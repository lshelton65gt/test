//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/DynBitVector.h"
#  undef __BITS_PER_WORDT
#  undef __BITVECTOR_WORDS
#  undef BitVector
#include "util/BitVector.h"

void DynBitVector::anytoany(const UInt32* src, UInt32* dst,
                            UInt32 srcStart, UInt32 numBits, UInt32 dstStart)
{
  const BVref<false> srcRef(const_cast<UInt32*>(src), srcStart, numBits);
  BVref<false> dstRef(dst, dstStart, numBits);
  dstRef.anytoany(srcRef);
}
