// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtCachedFileSystem.h"
#include "util/UtList.h"
#include "util/UtHashSet.h"
#include "util/UtIOStream.h"

//! ctor
UtCachedFileSystem::UtCachedFileSystem(SInt32 numPhysicalFiles)
{
  INFO_ASSERT(numPhysicalFiles > 0, "Invalid number of files specified.");
  mNumPhysicalFiles = numPhysicalFiles;
  mNumOpenFiles = 0;
  mOpenFiles = CARBON_ALLOC_VEC(UtIOStreamBase*, mNumPhysicalFiles);
  mAllFiles = new FileSet;
  mTimestamp = 0;
  mOpenFiles[mNumOpenFiles] = NULL; // keep this invariant as a sanity check
  mNumReopens = 0;
}

//! dtor
UtCachedFileSystem::~UtCachedFileSystem()
{
  // Not allowed to destruct system till all the files are closed
  INFO_ASSERT(mNumOpenFiles == 0, "Files are still open.");
  INFO_ASSERT(mAllFiles->empty(), "File objects have not been cleared.");

  CARBON_FREE_VEC(mOpenFiles, UtIOStreamBase*, mNumPhysicalFiles);
  delete mAllFiles;
}

//! Open a file -- return NULL and fill errmsg on failure
void UtCachedFileSystem::registerFile(UtIOStreamBase* f)
{
  mAllFiles->insert(f);
}

//! declare that a cached-file has been permanently closed
void UtCachedFileSystem::unregisterFile(UtIOStreamBase* f)
{
  if (f->hasActiveFD())
  {
    bool found = false;
    for (SInt32 i = 0; ! found && (i < mNumOpenFiles); ++i)
    {
      if (mOpenFiles[i] == f)
      {
        found = true;
        markInactive(i);
      }
    }
    INFO_ASSERT(found, "Stream not found in open-file registry.");
  }
  
  // Note that set semantics imply f can only be in there once,
  // and the fact that it is put there from the UtIOStreamBase ctor
  // also guarantees this.  BTW, mAllFiles has no functional
  // purpose except to enforce that the user closes all cached
  // files before deleting the file system.  Potentially someone
  // could iterate over the set and figure out which files are
  // still open.  NYI.
  size_t erased = mAllFiles->erase(f);
  INFO_ASSERT(erased == 1, "Stream not found in all-files registry.");
}

void UtCachedFileSystem::activate(UtIOStreamBase* f)
{
  INFO_ASSERT(mNumOpenFiles < mNumPhysicalFiles, "Consistency check failed.");
  INFO_ASSERT(mOpenFiles[mNumOpenFiles] == NULL, "Consistency check failed.");
  mOpenFiles[mNumOpenFiles] = f;
  ++mNumOpenFiles;
  if (mNumOpenFiles < mNumPhysicalFiles)
    mOpenFiles[mNumOpenFiles] = NULL;
}

void UtCachedFileSystem::maybeRelease()
{
  INFO_ASSERT(mNumOpenFiles <= mNumPhysicalFiles, "Consistency check failed.");
  if (mNumOpenFiles == mNumPhysicalFiles)
  {
    ++mNumReopens;

    // Figure out which one is least recently used.
    UInt32 lruTimestamp = mOpenFiles[0]->getTimestamp();
    SInt32 lruIndex = 0;

    // looping starts at 1 because we have already initialized based
    // on the object at 0.  We also know that there is at least one
    // open file because mNumPhysicalFiles is guaranteed by the ctor
    // to be >0.
    for (SInt32 i = 1; i < mNumPhysicalFiles; ++i)
    {
      UInt32 timestamp = mOpenFiles[i]->getTimestamp();
      if (timestamp < lruTimestamp)
      {
        lruTimestamp = timestamp;
        lruIndex = i;
      }
    }
    mOpenFiles[lruIndex]->releaseFD();
    markInactive(lruIndex);
  }
}

void UtCachedFileSystem::markInactive(SInt32 index)
{
  --mNumOpenFiles;
  if (index != mNumOpenFiles)
  {
    // Swap the one we closed with the last one so that
    // we can append the next one we open at the end
    // rather than at a hole we just created
    mOpenFiles[index] = mOpenFiles[mNumOpenFiles];
  }
  mOpenFiles[mNumOpenFiles] = NULL;
}

UInt32 UtCachedFileSystem::bumpTimestamp()
{
  // The timestamp is only used for predicting an optimal file to
  // lose its file descriptor using LRU or MRU or something.  So
  // if it overflows 32-bits that isn't going to harm functionality.
  return mTimestamp++;
}
