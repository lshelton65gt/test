// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdlib.h>
#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/OSWrapper.h"

#if pfWINDOWS

// This function looks at the path passed in, and if it looks like it
// could be a CARBON_HOME directory, it sets the environment variable
// CARBON_HOME for this process to path, prepends necessary
// directories in CARBON_HOME to the PATH environment variable, and
// returns true.  If it doesn't look like a CARBON_HOME, it returns
// false.
bool
SetCarbonHome(const char* path)
{
  // If this really is a CARBON_HOME, it will have a bin/modelstudio.
  UtString cmsPath = UtString(path) + "/bin/modelstudio";
  UtString unused;
  if (OSStatFile(cmsPath.c_str(), "e", &unused)) {
    // It looks like path is a reasonable CARBON_HOME, so we'll use it.

    #define CARBON_HOME_EQUALS "CARBON_HOME="
    // Since some implementations of putenv() put the actual string
    // passed into the environment (rather than a copy), we can't pass
    // an automatic variable to putenv().  Instead we create a memory
    // leak.

    int pathlen = strlen(path);
    // sizeof includes terminating \0.
    char* env = static_cast<char*>(malloc(sizeof CARBON_HOME_EQUALS + pathlen));
    strncpy(env, CARBON_HOME_EQUALS, sizeof CARBON_HOME_EQUALS - 1); // Don't copy the \0.
    strncpy(env + sizeof CARBON_HOME_EQUALS - 1, path, pathlen + 1);
    putenv(env);

    // Now prepend Carbon directories to PATH.  This same set of
    // directories is used in src/windowsInstall/carbonate.xslt.
    char* previousPath = getenv("PATH");
    UtString newEnv = UtString("PATH=") + path + "\\Win\\bin;" + path + "\\Win\\lib;" + path + "\\Win\\lib\\winx\\shared;" + previousPath;
    putenv(newEnv.c_str());
    return true;
  }
  else
    return false;               // We were passed something that's not
                                // a valid CARBON_HOME.
}

#endif // pfWINDOWS
