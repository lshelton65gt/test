// -*-C++-*-
/******************************************************************************
 Copyright (c) 2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/TimebombHelper.h"
#include "util/UtLicense.h"
#include "util/UtLicenseMsg.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/CarbonAssert.h"

//! Dummy license callback class, simply because UtLicense requires it.
class TimebombHelper::LicenseCB : public UtLicense::MsgCB
{
public:
  CARBONMEM_OVERRIDES

  LicenseCB() {}
  virtual ~LicenseCB() {}

  virtual void waitingForLicense(const char*) {}

  virtual void queuedLicenseObtained(const char*) {}

  virtual void requeueLicense(const char*) {}

  virtual void relinquishLicense(const char*) {}

  virtual void exitNow(const char*)
  {
    exit(1);
  }
};


TimebombHelper::TimebombHelper()
  : mOwnLicense(true)
{
  mCB = new LicenseCB;
  mLicense = new UtLicense(mCB, false);
}

TimebombHelper::TimebombHelper(UtLicense* licenseObj)
  : mLicense(licenseObj), mCB(NULL), mOwnLicense(false)
{
}

TimebombHelper::~TimebombHelper()
{
  if (mOwnLicense) {
    delete mCB;
    delete mLicense;
  }
}

SInt64 TimebombHelper::getLatestTimebomb(UtString* timebombString, UtString* featureName)
{
  // The special timebomb license has encoded in its feature name the
  // latest possible end date for the timebomb.
  //
  // The format is
  //
  // <feature prefix>_<date>
  //
  // where <feature prefix> is the string corresponding to
  // UtLicense::eTimebombBase, and <date> is the date in the
  // format YYYYMMDD
  //
  // Look for all possible timebomb-enabling licenses.  If more
  // than one exist, we want to choose the one that allows the
  // longest timebomb.
  SInt64 latestValidBomb = 0;
  UtString timebombPrefix;
  mLicense->getFeatureName(&timebombPrefix, UtLicense::eTimebombBase);
  // Add the underscore.  We don't want to match (for instance)
  // crbn_timebomb or crbn_timebomb123.
  timebombPrefix << "_";
  UtStringArray timebombFeatures;
  mLicense->getFeaturesMatchingPrefix(timebombPrefix.c_str(), &timebombFeatures);
  for (UtStringArray::iterator i = timebombFeatures.begin(); i != timebombFeatures.end(); ++i) {
    // Extract the date from the feature name and convert
    const char* encodedDate = *i;
    UtString thisFeature = encodedDate;
    encodedDate += timebombPrefix.size();     // move past prefix
    UtString thisBombStr;
    SInt64 thisBomb;
    sConvertTimeBombLicenseDate(encodedDate, &thisBombStr, &thisBomb);
    if (thisBomb > latestValidBomb) {
      latestValidBomb = thisBomb;
      *timebombString = thisBombStr;
      *featureName = thisFeature;
    }
  }

  return latestValidBomb;
}

bool TimebombHelper::sCreate64BitTimebomb(const char* timeStr, SInt64* timebomb)
{
  // Create and parse the time string.  Timebombs expire at 11:59:59 PM.
  bool success = true;
  UtString fullTimeStr;
  tm timeStruct;
  fullTimeStr << timeStr << " 23:59:59";
  if (!OSParseTime(fullTimeStr.c_str(), &timeStruct)) {
    success = false;
  } else {
    *timebomb = mktime(&timeStruct);
    INFO_ASSERT(*timebomb > 0, "Invalid return from mktime()");
  }
  return success;
}

void TimebombHelper::sConvertTimeBombLicenseDate(const char* encodedDate, UtString* timebombStr, SInt64* timebomb)
{
  timebombStr->clear();
  // Convert YYYYMMDD to YYYY-MM-DD.
  INFO_ASSERT(strlen(encodedDate) == 8, "Invalid timebomb end date");
  timebombStr->append(encodedDate, 4);
  timebombStr->append("-");
  timebombStr->append(encodedDate + 4, 2);
  timebombStr->append("-");
  timebombStr->append(encodedDate + 6, 2);

  // This should always succeed, because the date format is correct.
  bool success = sCreate64BitTimebomb(timebombStr->c_str(), timebomb);
  INFO_ASSERT(success, "Timebomb end date conversion failed");
}

