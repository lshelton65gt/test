// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _MEM_HISTOGRAM_H_
#define _MEM_HISTOGRAM_H_

// We can run with memory debugging available even in product mode, so
// just do it all the time.  Previously, the roadblock was that for
// memory allocations >64k I have to use malloc and I don't know how
// to get the size when I free unless I add my own extra overhead,
// which I didn't want to do in product mode.

// But then I realized that I was already keeping a map from the
// pointer to the stack-frame that allocated it, there was not much
// extra overhead in making the map .second be a
// pair<stackframe,size>.  So now during a memory debugging replay, I
// can tell the size of an object being freed from the pointer.

// That means I can do memory debugging in product mode, with the
// slight cost of having to check (gMemHistogram!=NULL) on every
// allocation and free.
#define MEM_DEBUG_ENABLE 


// Memory debugging is always enabled when compiling with CDB
#ifdef CDB
#ifndef MEM_DEBUG_ENABLE
#define MEM_DEBUG_ENABLE
#endif
#endif

#include "util/UtString.h"
#include "util/UtArray.h"
#include "util/UtHashSet.h"
#include "util/UtHashMap.h"
#include "util/UtStackTrace.h"

struct MemStackTrace;
class ZostreamDB;

class MemHistogram
{
public: CARBONMEM_OVERRIDES
  //! constructor
  MemHistogram(const char* dumpFile, const char* executable, bool capture);

  //! destructor
  ~MemHistogram();

  //! record an allocation
  void record(void* allocedPtr, size_t size);
  
  //! erase an allocation
  void erase(void* allocedPtr);
  
  //! print a memory histogram to a file (stdout if NULL is passed for filename)
  bool print(const char* filename);

  //! print a memory histogram to a file (stdout if NULL is passed for filename)
  void checkpoint(const char* filename);

  //! are we currently allocating memory for the histogram itself?
  bool isActive() const {return mBusyDepth != 0;}

  //! flush the allocation records to disk -- prints error messages to stdout
  //! and returns status
  bool flush();

  //! replay memory activity from a dump file
  bool replay(int detail, bool listPointers);

  //! replay memory free activity from a dump file
  bool replayFree(void* ptr);

  //! replay memory allocation activity from a dump file
  void replayAlloc(void* ptr, MemStackTrace* trace, size_t size);

  //! Record that we've run out of memory, do a final checkpoint, & close files
  /*! returns the number of bytes that got released so exiting works.
   */
  UInt32 outOfMemory();

private:
  void flushCache();
  void bumpToken();
  void writeAlloc(void* ptr, size_t size, MemStackTrace* trace);
  void initCapture(const char* dumpfile, const char* executable);
  void initReplay(const char* dumpfile);

  // Before this change I was effectively keeping the count information by
  // copying bools into stack variables.  I found this cleaner.  The
  // count can be > 1 when, for example, CarbonMem::checkpoint() is called,
  // does a push and then calles flush(), which does a push, etc.
  void push();
  void pop();

  typedef UtHashSet<MemStackTrace*, HashPointerValue<MemStackTrace*> > TraceSet;
  typedef std::pair<MemStackTrace*,UInt32> TraceEntry;
#if pfLP64
  typedef UtHashMap<void*, TraceEntry, HashPointer<void*>,
                    HashObjectMgr<void*> > TraceMap;
#else
  typedef UtHashMap<void*, TraceEntry> TraceMap;
#endif
  typedef UtArray<MemStackTrace*> TraceVector;

  SInt32 mBusyDepth;            // 0 means idle
  TraceSet* mTraces;
  TraceVector* mTraceVec;
  TraceMap* mAllocs;            // NULL during execution, used during replay
  UtString* mExecutable;
  UtString* mDumpFilename;
  UtExeSymbolTable* mSymbolTable;
  unsigned long mTotalBytes;    // # bytes allocated known by histogram
                                // (excludes histogram overhead)
  ZostreamDB* mMemDump;         // Used during execution
  SInt32 mTokenCount;
  int mDetail;
  bool mListPointers;
  bool mMemExhausted;
  void* mMemReserve;

  // In order to make the size of the dump file not be insane,
  // keep a cache of  n  allocated pointers such that if they
  // are freed before the next checkpoint, we can just forget
  // about them rather than writing them to the file.  Maintain
  // a list (mFifo) of AllocRecords so if the cache is full when
  // inserting we sacrafice the oldest allocation.
  struct PtrCache;
  struct AllocRecord {
    CARBONMEM_OVERRIDES
    AllocRecord(void* ptr, size_t size, MemStackTrace* stackTrace,
		PtrCache& cache):
      mPtr(ptr), mSize(size), mStackTrace(stackTrace), mCache(cache),
      mNext(0)
    {
      // Add this node to end of mCache.mFifo list.

      if (mCache.mFifo == NULL) { // I'm the only node
	mCache.mFifo = mCache.mLast = mPrev = this;
      }
      else {			// Already a node there
	// only insert new record at end
        INFO_ASSERT(mCache.mLast->mNext == NULL, "Last record is not at end of list.");
	mPrev = mCache.mLast;	// My previous was last
	mCache.mLast->mNext = this;
	mCache.mLast = this;	// I'm new last node
      }
      mCache.mFifoSize++;
    }
    ~AllocRecord()
    {
      // Remove this node from list.

      if (mCache.mFifo == this) { // Deleting first node
	mCache.mFifo = mNext;
      }
      else {			// Not first node
	mPrev->mNext = mNext;
      }
      if (mNext == NULL) {	// This node was last
	mCache.mLast = mPrev;
      }
      else {			// Not last node
        mNext->mPrev = mPrev;
      }
      mCache.mFifoSize--;
    }
    void* mPtr;
    size_t mSize;
    MemStackTrace* mStackTrace;
  private:
    PtrCache& mCache;
    AllocRecord* mPrev;		// ==this if only one node
    AllocRecord* mNext;		// NULL for last node

    CARBON_FORBID_DEFAULT_CTORS(AllocRecord);
  };
  struct PtrCache {
    CARBONMEM_OVERRIDES
    // We use HashObjectMgr<> rather than the default HashPointerMgr<>
    // here, because HashObjectMgr<> assumes pointers are shrinkable, but
    // some of these pointers point to objects greater than 64kB and
    // therefore unshrinkable.  HashObjectMgr<> solves the problem by
    // allocating pointers for us.
    typedef UtHashMap<void*, AllocRecord*,
                      HashPointer<void*>,
                      HashObjectMgr<void*> > Map;
    typedef Map::iterator iterator;

    PtrCache(MemHistogram* mh): mFifo(0), mLast(0), mFifoSize(0),
				mMemHistogram(mh) {}

    void insert(void* ptr, size_t size, MemStackTrace* trace);
    void clear();
    bool erase(void* ptr);
    iterator begin() {return mMap.begin();}
    iterator end() {return mMap.end();}

    AllocRecord* mFifo;
    AllocRecord* mLast;
    size_t mFifoSize;

  private:
    PtrCache();			// Disallow default constructor
    PtrCache(const PtrCache&);	// Disallow copy constructor
    PtrCache& operator=(const PtrCache&); // Disallow assignment

    Map mMap;
    MemHistogram* mMemHistogram;
  };
  friend struct PtrCache;

  PtrCache* mPtrCache;
  char mHexBuf[20];
};

#endif
