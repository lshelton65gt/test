// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtStream.h"
#include "util/MemoryPool.h"
#include "util/UtIOStream.h"
#include "util/UtArray.h"

class MyClass
{
public:
  typedef MyClass* iterator;
  typedef MyClass& reference;

  MyClass(int i) { ptr = new int(); *ptr = i;}
  ~MyClass() { *ptr = -1; delete ptr; }

  bool isAllocated() const {return mIsAllocated;}
  void putIsAllocated(bool flag) {mIsAllocated = flag;}

  int *ptr;
  bool mIsAllocated;
};

typedef MemoryPool< MyClass, 2*sizeof(MyClass) > MyClassPool;

int memorypooltest(int /*argc*/, char* /*argv*/[])
{
  MyClassPool pool;
  UtArray< MyClass* > vect;

  for (int i = 0; i < 10; i++) {
    void *v = pool.malloc();
    MyClass *c = new (v) MyClass(i);
    vect.push_back(c);
  }

  pool.free(vect[1]);
  pool.free(vect[8]);

  int accum = 0;
  for (MyClassPool::iterator iter = pool.begin(); iter != pool.end(); ++iter) {
    accum += *(iter->ptr);
  }
  if (accum != (2 + 3 + 4 + 5 + 6 + 7 + 9)) {
    UtIO::cout() << "FAIL 1" << UtIO::endl;
  } else {
    UtIO::cout() << "PASS 1" << UtIO::endl;
  }

  for (int i = 10; i < 14; i++) {
    void *v = pool.malloc();
    MyClass *c = new (v) MyClass(i);
    vect.push_back(c);
  }

  accum = 0;
  for (MyClassPool::iterator iter = pool.begin(); iter != pool.end(); ++iter) {
    accum += *(iter->ptr);
  }
  if (accum != (2 + 3 + 4 + 5 + 6 + 7 + 9 + 10 + 11 + 12 + 13)) {
    UtIO::cout() << "FAIL 2" << UtIO::endl;
  } else {
    UtIO::cout() << "PASS 2" << UtIO::endl;
  }
  return 0;
}
