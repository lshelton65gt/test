// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/Zstream.h"
#include "util/UtIOStream.h"
#include "util/UtString.h"
#include "util/UtIStream.h"
#include <fstream>

int ztest(int argc, char* argv[])
{
  int stat = 0;
  if (argc < 2)
  {
    UtIO::cerr() << "Usage:\n  ztest <file>\n";
    exit(1);
  }

  // first test uncompressed input reading
  ZISTREAM(inNotZ, argv[1]);
  std::ofstream src2("src2");
  if (inNotZ)
  {
    char abuf[150];
    while (! inNotZ.eof() && ! inNotZ.fail())
    {
      int numRead = inNotZ.read(abuf, 150);
      src2.write(abuf, numRead);
    }

    if (inNotZ.fail())
    {
      UtIO::cerr() << inNotZ.getError() << '\n';
      stat = 1;
    }
    inNotZ.close();
    src2.close();
  }
  else
  {
    UtIO::cerr() << inNotZ.getError() << '\n';
    stat = 1;
  }
  

  ZOSTREAM(z, "out.gz");
  if (! z)
  {
    UtIO::cerr() << z.getError() << '\n';
    exit(1);
  }

  // resize to just over default to exercise cipher byte check
  z.resizeFileBuffer(66000);

  // exercise dynamic resize on reading
  z.resizeDataBuffer(100000);

  std::ifstream in(argv[1]);
  if (!in)
  {
    UtIO::cerr() << "Unable to open file: " << argv[1] << "\n";
    exit(1);
  }
  
  in.seekg(0, std::ios::end);
  int length = in.tellg();
  char* bufP = new char[length];
  in.seekg(0, std::ios::beg);
  in.read(bufP, length);
  in.close();

  z.write(bufP, static_cast<UInt32>(length));
  delete [] bufP;
  
  if (!z)
  {
    UtIO::cerr() << "zwrite problem: " << z.getError() << '\n';
    stat = 1;
  }

  z.close();
  
  ZISTREAM(inZ, "out.gz");
  std::ofstream src("src");
  if (inZ)
  {
    char buf[128];
    while (! inZ.eof() && ! inZ.fail())
    {
      int numRead = inZ.read(buf, 128);
      src.write(buf, numRead);
    }

    if (inZ.fail())
    {
      UtIO::cerr() << inZ.getError() << '\n';
      stat = 1;
    }
    inZ.close();
    src.close();
  }
  else
  {
    UtIO::cerr() << inZ.getError() << '\n';
    stat = 1;
  }

  ZOSTREAMDB(oDB, "db.gz"); 
  double a1 = 3.141592654, a2;
  UInt64 b1 = 1, b2;
  UInt32 c1 = 1, c2;
  UInt16 d1 = 1, d2;
  const char* e1 = "1";
  oDB << a1 << b1 << c1 << d1 << e1;
  oDB.close();

  ZISTREAMDB(iDB, "db.gz");
  UtString e2;
  if (!(iDB >> a2) || (a1 != a2) ||
      !(iDB >> b2) || (b1 != b2) ||
      !(iDB >> c2) || (c1 != c2) ||
      !(iDB >> d2) || (d1 != d2) ||
      !(iDB >> e2) || (strcmp(e1, e2.c_str()) != 0))
    ++stat;
  iDB.close();

  // Test Zi/oStringStreams

  UtString outBuf;
  ZOSTRINGSTREAM(zbuf, &outBuf);
  const char* phrase = "Ahardrainsagonnafall. ";
  zbuf << phrase;
  zbuf << 23;
  zbuf.flush();

  // prepare the validation string.
  UtString check(phrase);
  check << 23;

  UtIStringStream iss(outBuf);
  ZISTREAM(zinBuf, &iss);

  UtString inRead;
  zinBuf >> inRead;
  UInt32 val;
  zinBuf >> val;
  if (! zinBuf.eof())
  {
    UtIO::cerr() << "ZistringStream: in buf not fully read." << UtIO::endl;
    ++stat;
  }

  if (! zinBuf.fail())
  {
    inRead << " ";
    inRead << val;
  }

  if (check.compare(inRead) != 0)
  {
    UtIO::cerr() << "Zi/oStringStream failed.: check = '" << check << "'  inRead = '" << inRead << "'" << UtIO::endl;
    ++stat;
  }
  
  return stat;
}
