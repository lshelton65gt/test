// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Some testing of the graph merge code
*/

#include "util/UtVector.h"
#include "util/Graph.h"
#include "util/GenericBigraph.h"
#include "util/GraphMerge.h"
#include "util/GraphSCC.h"

#include "util/UtIOStream.h"

#include "intbigraph.h"

#define GRAPH_SIZE 32

IntGraph* copyGraph(IntGraph*);
bool mergeGraphDepth(IntGraph*, int mergeMask, const char*);
bool mergeGraphFanin(IntGraph*, int mergeMask, const char*);
void printGraph(IntGraph*, const char*);

int graphmergetest(int /* argc */, char** /* argv */)
{
  // Create a bigraph of integers
  int status = 0;
  IntGraph* intGraph1 = new IntGraph;

  // Start with a small graph
  IntNode* nodes[GRAPH_SIZE];
  for (int i = 0; i < 6; ++i) {
    nodes[i] = addNode(intGraph1, (1 << i));
  }
  addEdge(intGraph1, nodes[0], nodes[1], 0);
  addEdge(intGraph1, nodes[0], nodes[2], 1);
  addEdge(intGraph1, nodes[0], nodes[3], 2);
  addEdge(intGraph1, nodes[1], nodes[4], 3);
  addEdge(intGraph1, nodes[2], nodes[4], 4);
  addEdge(intGraph1, nodes[3], nodes[5], 5);
  addEdge(intGraph1, nodes[4], nodes[5], 6);
  
  // Print the small graph
  printGraph(intGraph1, "Small Graph");

  // Merge and print it again
  IntGraph* intGraph2 = copyGraph(intGraph1);
  if (!mergeGraphDepth(intGraph2, 0, "Small Fully Merged Graph By Depth")) {
    status = 1;
  }
  IntGraph* intGraph3 = copyGraph(intGraph1);
  if (!mergeGraphFanin(intGraph3, 0, "Small Fully Merged Graph By Fanin")) {
    status = 1;
  }
  delete intGraph1;
  delete intGraph2;
  delete intGraph3;

  // Create 32 nodes with a different bit set in the integer.
  intGraph1 = new IntGraph;
  for (int i = 0; i < GRAPH_SIZE; ++i) {
    nodes[i] = addNode(intGraph1, (1 << i));
  }

  // Connect up the edges
  addEdge(intGraph1, nodes[5], nodes[8], 0);
  addEdge(intGraph1, nodes[5], nodes[10], 1);
  addEdge(intGraph1, nodes[8], nodes[6], 2);
  addEdge(intGraph1, nodes[8], nodes[4], 3);
  addEdge(intGraph1, nodes[10], nodes[17], 4);
  addEdge(intGraph1, nodes[10], nodes[19], 5);
  addEdge(intGraph1, nodes[6], nodes[16], 6);
  addEdge(intGraph1, nodes[6], nodes[3], 7);
  addEdge(intGraph1, nodes[4], nodes[3], 8);
  addEdge(intGraph1, nodes[4], nodes[12], 9);
  addEdge(intGraph1, nodes[16], nodes[1], 10);
  addEdge(intGraph1, nodes[16], nodes[18], 11);
  addEdge(intGraph1, nodes[3], nodes[14], 12);
  addEdge(intGraph1, nodes[12], nodes[14], 13);
  addEdge(intGraph1, nodes[1], nodes[20], 14);
  addEdge(intGraph1, nodes[1], nodes[22], 15);
  addEdge(intGraph1, nodes[1], nodes[24], 16);
  addEdge(intGraph1, nodes[18], nodes[9], 17);
  addEdge(intGraph1, nodes[14], nodes[28], 18);
  addEdge(intGraph1, nodes[20], nodes[2], 19);
  addEdge(intGraph1, nodes[22], nodes[27], 20);
  addEdge(intGraph1, nodes[22], nodes[25], 21);
  addEdge(intGraph1, nodes[24], nodes[25], 22);
  addEdge(intGraph1, nodes[24], nodes[26], 23);
  addEdge(intGraph1, nodes[28], nodes[15], 24);
  addEdge(intGraph1, nodes[28], nodes[30], 25);
  addEdge(intGraph1, nodes[28], nodes[11], 26);
  addEdge(intGraph1, nodes[2], nodes[7], 27);
  addEdge(intGraph1, nodes[11], nodes[13], 28);
  addEdge(intGraph1, nodes[21], nodes[23], 29);
  addEdge(intGraph1, nodes[21], nodes[29], 30);
  addEdge(intGraph1, nodes[23], nodes[31], 31);
  addEdge(intGraph1, nodes[29], nodes[31], 32);
  addEdge(intGraph1, nodes[29], nodes[0], 33);

  // Print the graph in a sorted walk
  printGraph(intGraph1, "Original Large Graph");

  // Make a copy of the graph for merging and merge it assuming that
  // every multiple of 3 offset is not mergable
  intGraph2 = copyGraph(intGraph1);
  if (!mergeGraphDepth(intGraph2, 0x49249248,
                       "Merged Every Three Large Graph By Depth")) {
    status = 2;
  }
  intGraph3 = copyGraph(intGraph1);
  if (!mergeGraphFanin(intGraph3, 0x49249248,
                       "Merged Every Three Large Graph By Fanin")) {
    status = 2;
  }
  delete intGraph2;
  delete intGraph3;

  // Make a copy of the graph for merging and merge it assuming that
  // every multiple of 4 offset is not mergable
  intGraph2 = copyGraph(intGraph1);
  if (!mergeGraphDepth(intGraph2, 0x11111110,
                  "Merged Every Four Large Graph By Depth")) {
    status = 3;
  }
  intGraph3 = copyGraph(intGraph1);
  if (!mergeGraphFanin(intGraph3, 0x11111110,
                  "Merged Every Four Large Graph By Fanin")) {
    status = 3;
  }

  delete intGraph1;
  delete intGraph2;
  delete intGraph3;
  return status;
} // int graphmergetest

IntGraph* copyGraph(IntGraph* intGraph)
{
  // Walk all the nodes and put them in a keep set
  GraphNodeSet keepSet;
  for (Iter<GraphNode*> i = intGraph->nodes(); !i.atEnd(); ++i) {
    GraphNode* node = *i;
    keepSet.insert(node);
  }
  Graph* newGraph = intGraph->subgraph(keepSet, false);
  return dynamic_cast<IntGraph*>(newGraph);
}

class IntPrintWalker : public PrintWalker
{
public:
  IntPrintWalker(IntGraph* intGraph) : PrintWalker(intGraph) {}

  static void printNodeData(const IntNode* node)
  {
    int data = node->getData();
    const char* sep = "";
    for (int i = 0; i < GRAPH_SIZE; ++i) {
      if (((1 << i) & data) != 0) {
        UtIO::cout() << sep << i;
        sep = ",";
      }
    }
  }
protected:
  void printNode(const IntNode* node)
  {
    printNodeData(node);
  }
};

class IntMerge : public UtGraphMerge
{
public:
  //! constructor
  IntMerge(IntGraph* intGraph, int mergeMask) :
    mIntGraph(intGraph), mMergeMask(mergeMask), mSuccess(true)
  {}

  //! Get merge success
  bool getSuccess() const { return mSuccess; }

protected:
  //! Test if a node is a valid start node
  bool validStartNode(const GraphNode*) const
  {
    return true;
  }

  //! Test if a node is mergable
  bool nodeMergable(const GraphNode* graphNode) const
  {
    const IntNode* node = mIntGraph->castNode(graphNode);
    int value = node->getData();
    return (value & mMergeMask) == 0;
  }

  //! Test if two nodes are mergable 
  bool edgeMergable(const GraphEdge* edge) const
  {
    const GraphNode* gn1 = mIntGraph->startPointOf(edge);
    const GraphNode* gn2 = mIntGraph->endPointOf(edge);
    return nodeMergable(gn1) && nodeMergable(gn2);
  }

  //! Function to merge nodes
  void mergeNodes(const NodeVector& nodes)
  {
    // Get the last node to merge into
    NodeVector::const_reverse_iterator p = nodes.rbegin();
    GraphNode* node = *p++;
    IntNode* collectNode = mIntGraph->castNode(node);

    // Merge the others into it
    UtIO::cout() << "Merging: ";
    IntPrintWalker::printNodeData(collectNode);
    int newValue = collectNode->getData();
    for (; p != nodes.rend(); ++p) {
      // Merge the nodes
      GraphNode* node = *p;
      IntNode* intNode = mIntGraph->castNode(node);
      newValue |= intNode->getData();

      // Print info
      UtIO::cout() << ";";
      IntPrintWalker::printNodeData(intNode);
    }
    UtIO::cout() << "\n";
    mIntGraph->setData(collectNode, newValue);
    if ((newValue & mMergeMask) != 0) {
      UtIO::cout() << "Merging unmergable nodes with mask: "
                   << UtIO::hex << (newValue & mMergeMask) << "\n";
      mSuccess = false;
    }
  }

  //! Function to handle merging edge data
  void mergeEdges(GraphEdge*, const GraphEdge*) const {}

  //! Function to handle removed node (nothing to do)
  void removeMergedNode(GraphNode*)
  {
    return;
  }

  //! Function to handle encapsulated cyclic nodes
  /*! This should not happen
   */
  void encapsulateCycle(const NodeVector&)
  {
    UtIO::cout() << "Found an unexpected cycle in the graph\n";
  }

private:
  IntGraph* mIntGraph;
  int mMergeMask;
  bool mSuccess;
}; // class IntMerge : public UtGraphMerge


class NodeDataCmp : public IntMerge::NodeCmp
{
public:
  NodeDataCmp() {}

  bool operator()(const GraphNode*, const GraphNode*) const
  {
    return false;
  }

private:
  IntGraph* mGraph;
};

bool mergeGraphDepth(IntGraph* intGraph, int mergeMask, const char* title)
{
  // Merge by depth
  IntMerge intMerge(intGraph, mergeMask);
  intMerge.init(intGraph);
  NodeDataCmp nodeDataCmp;
  IntNodeCmp nodeOrderCmp(intGraph);
  intMerge.mergeByDepth(&nodeDataCmp, &nodeOrderCmp);
  printGraph(intGraph, title);
  bool success = intMerge.getSuccess();

  // Make sure the graph is acyclic
  GraphSCC graphSCC;
  graphSCC.compute(intGraph);
  if (graphSCC.hasCycle()) {
    UtIO::cout() << "Merging introduced a cycle\n";
    success = false;
  }
  return success;
}

bool mergeGraphFanin(IntGraph* intGraph, int mergeMask, const char* title)
{
  // Merge by fanin
  IntMerge intMerge(intGraph, mergeMask);
  IntNodeCmp nodeCmp(intGraph);
  IntEdgeCmp edgeCmp(intGraph);
  intMerge.init(intGraph);
  intMerge.mergeFanin(&nodeCmp, &edgeCmp);
  printGraph(intGraph, title);
  bool success = intMerge.getSuccess();

  // Make sure the graph is acyclic
  GraphSCC graphSCC;
  graphSCC.compute(intGraph);
  if (graphSCC.hasCycle()) {
    UtIO::cout() << "Merging introduced a cycle\n";
    success = false;
  }
  return success;
}

void printGraph(IntGraph* intGraph, const char* title)
{
  IntNodeCmp nodeCmp(intGraph);
  IntEdgeCmp edgeCmp(intGraph);
  IntPrintWalker printWalker(intGraph);
  UtIO::cout() << title << ":\n";
  printWalker.walk(intGraph, &nodeCmp, &edgeCmp);
  UtIO::cout() << "\n";
}
