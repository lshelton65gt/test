//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/OSWrapper.h"
#include "util/UtHashMapFastIter.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include <cstdio>
#include <iostream>

typedef unsigned int MyUint32;
typedef unsigned short MyUint16;

#define TABLE_SIZE 400

struct key
{
  MyUint32 one_ip; MyUint32 two_ip; MyUint16 one_port; MyUint16 two_port;
  unsigned hash() const 
  {
    return (((this->one_ip << 17) | (this->one_ip >> 15)) ^ this->two_ip) +
      (this->one_port * 17) + (this->two_port * 13 * 29);
  }

  bool operator<(const key& k) const
  {
    UInt32 myTot = one_ip + two_ip + one_port + two_port;
    UInt32 kTot = k.one_ip + k.two_ip + k.one_port + k.two_port;
    return myTot < kTot;
  }
  
  bool operator==(const key& k) const
  {
    return this->equal(&k);
  }
  
  bool equal(const key* k2) const
  {
    return (0 == memcmp(this,k2,sizeof(struct key)));
  }
};

struct value
{
  const char *id;
};
typedef UtHashMapFastIter<key*, value*, HashPointerValue<key*> > MyHash;
typedef UtHashMapFastIter<UInt32, UInt32> MyNumHash;
typedef UtHashMapFastIter<SInt64, UInt64> MyNum64HashMap;

static void doInsert(MyHash& theHash)
{
  int i;
  key* k;
  value* v;

  for (i = 0; i < TABLE_SIZE; i++)
  {
    k = new key;
    k->one_ip = 0xcfccee40 + i;
    k->two_ip = 0xcf0cee67 - (5 * i);
    k->one_port = 22 + (7 * i);
    k->two_port = 5522 - (3 * i);
    
    MyHash::iterator p = theHash.find(k);
    if (p != theHash.end()) {
      delete k;
    }
    else {
      v = new value;
      v->id = "a value";

      theHash[k] = v;
    }
  }
  printf("After insertion, hashtable contains " FormatSize_t " items.\n",
         theHash.size());
}

typedef UtHashMapFastIter<int, int> IntMap;

struct FourIntMaps2 {
  IntMap im1;
  IntMap im2;
  IntMap im3;
  IntMap im4;
};

static void doPointer()
{
  MyHash theHash;

  // First test simple case.
  IntMap int_map;
  int_map[1] = 42;
  IntMap::iterator p = int_map.find(1);
  assert(p != int_map.end());
  
  FourIntMaps2 fim;
#if !pfLP64
  assert(sizeof(fim) == 48);
#endif

  int i;
  int j;
  
  doInsert(theHash);

  j = 0;
  for (i = 0; i < TABLE_SIZE; i++)
  {
    key sk;
    sk.one_ip = 0xcfccee40 + i;
    sk.two_ip = 0xcf0cee67 - (5 * i);
    sk.one_port = 22 + (7 * i);
    sk.two_port = 5522 - (3 * i);
    
    MyHash::iterator p = theHash.find(&sk);
    if (p == theHash.end())
      ++j;
  }
  printf("%d not found(const)\n", j);

  j = 0;
  for (i = 0; i < TABLE_SIZE; i++)
  {
    key sk;
    sk.one_ip = 0xcfccee40 + i;
    sk.two_ip = 0xcf0cee67 - (5 * i);
    sk.one_port = 22 + (7 * i);
    sk.two_port = 5522 - (3 * i);
    
    MyHash::iterator p = theHash.find(&sk);
    if (p == theHash.end())
      ++j;
  }
  printf("%d not found(non-const)\n", j);

  j = 0;
  int m = 0;
  for (MyHash::UnsortedLoop q = theHash.loopUnsorted();
       ! q.atEnd(); ++q)
  {
    ++m;
    MyHash::iterator p = theHash.find(q.getKey());
    if (p == theHash.end())
      ++j;
  }
  printf("%d not found (loop non-const)\n", j);
  printf("%d iterated (loop non-const)\n", m);
  
  j = 0;
  m = 0;
  for (MyHash::UnsortedCLoop q = theHash.loopCUnsorted();
       ! q.atEnd(); ++q)
  {
    ++m;
    MyHash::iterator p = theHash.find(q.getKey());
    if (p == theHash.end())
      ++j;
  }
  printf("%d not found (loop const)\n", j);
  printf("%d iterated (loop const)\n", m);

  j = 0;
  m = 0;
  // Won't check sorting. Just want to make sure it compiles
  for (MyHash::SortedLoop q = theHash.loopSorted();
       ! q.atEnd(); ++q)
  {
    ++m;
    MyHash::iterator p = theHash.find(q.getKey());
    if (p == theHash.end())
      ++j;
  }
  printf("%d not found (loop sorted)\n", j);
  printf("%d iterated (loop sorted)\n", m);
  
  /*
   * UtHashMapFastIter does not support 'erase'
   *
   * for (i = 0; i < TABLE_SIZE; i++)
   * {
   *   key sk;
   *   sk.one_ip = 0xcfccee40 + i;
   *   sk.two_ip = 0xcf0cee67 - (5 * i);
   *   sk.one_port = 22 + (7 * i);
   *   sk.two_port = 5522 - (3 * i);
   *   
   *   MyHash::iterator p = theHash.find(&sk);
   *   delete p->first;
   *   delete p->second;
   *   //    key* tmpk = p->first;
   *   //    value* tmpv = p->second;
   *   theHash.erase(p);
   *   //    delete tmpk;
   *   //    delete tmpv;
   * }
   * printf("After removal by find/erase, hashtable contains " FormatSize_t " items.\n",
   *        theHash.size());


   *   doInsert(theHash);

   *  for (MyHash::iterator p = theHash.begin();
   *      p != theHash.end(); ++p)
   * {
   *   delete p->first;
   *   delete p->second;
   *   //    key* tmpk = p->first;
   *   //    value* tmpv = p->second;
   *   theHash.erase(p);
   *   //    delete tmpk;
   *   //    delete tmpv;
   * }
   * printf("After removal by iter, hashtable contains " FormatSize_t " items.\n",
   *        theHash.size());
   *
   * for (i = 0; i < TABLE_SIZE; i++)
   * {
   *   key sk;
   *   sk.one_ip = 0xcfccee40 + i;
   *   sk.two_ip = 0xcf0cee67 - (5 * i);
   *   sk.one_port = 22 + (7 * i);
   *   sk.two_port = 5522 - (3 * i);

   *    MyHash::iterator p = theHash.find(&sk);
   *   key* tmpk = p->first;
   *   value* tmpv = p->second;
   *   theHash.erase(&sk);
   *   delete tmpk;
   *   delete tmpv;
   * }
   * printf("After removal by erase(key), hashtable contains " FormatSize_t " items.\n",
   *        theHash.size());
   */
  
  for (MyHash::RemoveLoop p(theHash); !p.atEnd(); ++p) {
    key* tmpk = p.getKey();
    value* tmpv = p.getValue();
    delete tmpk;
    delete tmpv;
  }

  // instead just clear it before re-inserting
  theHash.clear();


  // now do a simple [] addition and replacement.
  {
    MyHash tmpHash;
    key* k = new key;
    k->one_ip = 0xcfccee40 + 1;
    k->two_ip = 0xcf0cee67 - (5 * 1);
    k->one_port = 22 + (7 * 1);
    k->two_port = 5522 - (3 * 1);
    
    value* v = new value;
    v->id = "nuther value";

    value* check = tmpHash[k];
    if (check != NULL)
    {
      fprintf(stderr, "null insertion with [] failed.\n");
      exit(1);
    }
    
    tmpHash[k] = v;

    MyHash::iterator q = tmpHash.find(k);
    if (q == tmpHash.end())
    {
      fprintf(stderr, "Operator [] failed.\n");
      exit(1);
    }
    else 
    {
      value* check = q->second;
      if (v != check)
      {
        fprintf(stderr, "Operator[] referencing failed.\n");
        exit(1);
      }
    }
    delete k;
    delete v;
  }

}

static void doNumberInsert(MyNumHash& theHash)
{
  UInt32 i;
  for (i = 0; i < TABLE_SIZE; i++)
    theHash[i] = i;
  printf("After insertion, hashtable contains " FormatSize_t " items.\n",
         theHash.size());
}

static void doNumber64()
{
  UInt64 chk;
  SInt64 schk;

  MyNum64HashMap theHash;

  UInt64 i;
  for (i = 0; i < TABLE_SIZE; i++)
  {
    SInt64 key = -(SInt64)i;
    theHash[key] = i;
  }
  printf("After insertion, hashtable contains " FormatSize_t " items.\n",
         theHash.size());
  
  
  for (MyNum64HashMap::SortedLoop q = theHash.loopSorted();
       ! q.atEnd(); ++q)
  {
    schk = q.getKey();
    chk = q.getValue();
    assert(schk == -(SInt64)(chk));
  }

  MyNum64HashMap::iterator p = theHash.find(0);
  assert(p != theHash.end());
  assert(p->first == 0);
  assert(p->second == 0);

  theHash.clear();
  assert(theHash.size() == 0);
}

static void doNumber()
{
  MyNumHash theHash;
  
  UInt32 i,j,c;

  doNumberInsert(theHash);
  
  
  j = 0;
  c = 0;
  for (i = 0; i < TABLE_SIZE; i++)
  {
    MyNumHash::iterator p = theHash.find(i);
    if (p == theHash.end())
      ++j;
    else if ((p->first != i) && (p->second != i))
      ++c;
  }
  printf("%u not found(const)\n", j);
  printf("%u entry conflicts(const)\n", c);

  j = 0;
  c = 0;
  for (i = 0; i < TABLE_SIZE; i++)
  {
    MyNumHash::iterator p = theHash.find(i);
    if (p == theHash.end())
      ++j;
    else if ((p->first != i) && (p->second != i))
      ++c;
  }
  printf("%u not found(non-const)\n", j);
  printf("%u entry conflicts(non-const)\n", c);

  /*
   * for (i = 0; i < TABLE_SIZE; i++)
   * {
   *   MyNumHash::iterator p = theHash.find(i);
   *   theHash.erase(p);
   * }
   * printf("After removal by find/erase, hashtable contains " FormatSize_t " items.\n",
   *        theHash.size());


   *   doNumberInsert(theHash);
   * 
   * for (MyNumHash::iterator p = theHash.begin();
   *      p != theHash.end(); ++p)
   *   theHash.erase(p);
   * 
   * printf("After removal by iter, hashtable contains " FormatSize_t " items.\n",
   *        theHash.size());
   * 
   * doNumberInsert(theHash);
   * 
   * for (i = 0; i < TABLE_SIZE; i++)
   * {
   *   theHash.erase(i);
   * }
   * printf("After removal by erase(key), hashtable contains " FormatSize_t " items.\n",
   *        theHash.size());
   */

  doNumberInsert(theHash);
  {
    MyNumHash tmpHash;
    theHash.swap(tmpHash);
  }
  printf("After removal by swap, hashtable contains " FormatSize_t " items.\n",
         theHash.size());

  doNumberInsert(theHash);
  theHash.freeMemory();
  printf("After removal by freeMemory, hashtable contains " FormatSize_t " items.\n",
         theHash.size());

  // now do a simple [] addition and replacement.
  {
    MyNumHash tmpHash;
    if (3 != tmpHash[4])
      tmpHash[4] = 3;

    MyNumHash::iterator q = tmpHash.find(4);
    if (q == tmpHash.end())
    {
      fprintf(stderr, "Operator [] failed.\n");
      exit(1);
    }
    else 
    {
      if (q->second != 3)
      {
        fprintf(stderr, "Operator[] referencing failed.\n");
        exit(1);
      }
    }
  }
}

// A dumb class.
struct dumb {
  char chars[10];
};

struct obj {
  struct dumb d;
  UtString str;
};

// Test a map from a "pointer" to a value managed with HashObjectMgr.
static void doObject()
{
  typedef UtHashMapFastIter<UInt32, obj> MyObjectHash;

  MyObjectHash theHash;
  
  obj o;
  o.str = "My value that should be copied";
  strncpy(o.d.chars, "Hello", sizeof o.d.chars);
  
  theHash[5] = o;

  printf("%s\n", theHash[5].str.c_str());

  // Test iterator
  for (MyObjectHash::iterator iter = theHash.begin();
       iter != theHash.end();
       ++iter)
  {
    printf("%s\n", iter->second.d.chars);
  }
}

typedef UtHashSet<int> MySet;

// Test mapping from integer to UtHashSet.
static void doMapOfSet()
{
  typedef UtHashMapFastIter<UInt32, MySet> MyHashMapOfSet;

  MyHashMapOfSet theHash;
  MySet theSet;
  // Create new entry in may with key 10, insert 42 into set stored there.
  theHash[10].insert(42);

  MySet::iterator iter = theHash[10].begin();
  printf("%d\n", *iter);	// Should be 42

  // call clear_and_resize, add one entry, and the world should still
  // be sane.  This targets a specific bug in that routine that
  // existed Sep-Nov 05.  It only occurs if there are a few entries
  // already in the hashtable
  for (UInt32 i = 11; i < 50; ++i) {
    theHash[i].insert(42);
  }
  theHash.clear_and_resize();
  theHash[10].insert(42);
  iter = theHash[10].begin();
  assert(*iter == 42);
}

static void doHashSet()
{
  UtHashSet<int> s;

  s.insert(2);
  s.insert(4);
  s.insert(6);
  s.insert(8);
  assert(s.size() == 4);
  assert(s.find(6) != s.end());
  assert(s.find(42) == s.end());
}


// Try a set with a value manager
    
class HashableInt		// A class with hash() and operator==() methods
{
public:
  HashableInt(int i) { val = i; }
  size_t hash() const {
    return val; } // I didn't say it was a good hash.
  bool operator==(const HashableInt& i) const {
    return i.val == val; }
  int squared() const { return val * val; }
  int val;
};

static void doHashSetWithObjectManager()
{
  // som :== Set with Object Manager
  UtHashSet<HashableInt> som;
  som.insert(3);
  som.insert(1);
  som.insert(4);
  som.insert(1);
  som.insert(5);
  som.insert(9);
  assert(som.size() == 5);
  assert(som.find(1) != som.end());
  assert(*som.find(1) == 1);	// Check the value
  assert(som.find(2) == som.end());
  assert(som.find(5)->squared() == 25);	// invoke a method
}

struct Dog {
  UtString mName;
  size_t hash() const { return mName.hash(); }
  bool operator==(const Dog& d) const { return mName == d.mName; }
};
static void doHashSetOfDogs()
{
  Dog d;
  d.mName = "Fido";
  typedef UtHashSet<Dog> DogSet;
  DogSet dogSet;
  dogSet.insert(d);
  DogSet::iterator it = dogSet.begin();
  std::cout << it->mName.c_str() << std::endl;
  // Test operator*()
  assert((*it).mName == it->mName);
}

// Test UtHashMapFastIter/UtHashSet
int testUtHashMap2()
{
  doPointer();
  doNumber();
  doNumber64();
  doObject();
  doMapOfSet();
  
  doHashSet();
  doHashSetWithObjectManager();
  doHashSetOfDogs();
  return 0;
}
