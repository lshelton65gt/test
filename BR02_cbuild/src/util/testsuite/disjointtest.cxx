//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/DisjointRange.h"
#include "util/UtIOStream.h"

struct RangeTest {SInt32 xmin; SInt32 xmax;};

static RangeTest ranges[] = {
  {5,9}, {3,8}, {11,20}, {0, 2}, {1, 3}, {0, 9}, {15, 17}
};

static RangeTest ranges2[] = {
  {2, 4}, {6, 8}, {1, 9}
};


static void test_ranges(RangeTest* ranges, size_t num_ranges) {
  DisjointRange dr;

  for (size_t i = 0; i < num_ranges; ++i) {
    dr.insert(ranges[i].xmin, ranges[i].xmax);
  }

  dr.check();
  UInt32 idx = 0;
  for (DisjointRange::SlotMapLoop p(dr.loopSlots()); !p.atEnd(); ++p, ++idx) {
    DisjointRange::Slot* slot = p.getValue();
    const ConstantRange& range = slot->getRange();
    UtIO::cout() << idx << ": ["
                 << range.getMsb() << ":" << range.getLsb() << "]\n";
  }

  for (size_t i = 0; i < num_ranges; ++i) {
    ConstantRange ir;
    assert(dr.construct(ConstantRange(ranges[i].xmax, ranges[i].xmin), &ir));
    UtIO::cout() << "[" << ranges[i].xmax << ":" << ranges[i].xmin<< "]\t--> ";
    UtIO::cout() << "[" << ir.getMsb() << ":" << ir.getLsb() << "]\n";
  }

  UtIO::cout() << UtIO::endl;
} // static void test_ranges

int disjointtest() {
  test_ranges(ranges, sizeof(ranges)/sizeof(ranges[0]));
  test_ranges(ranges2, sizeof(ranges2)/sizeof(ranges2[0]));
  return 0;
}

