// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtStream.h"
#include "util/CarbonTypes.h"
#include "util/UtIOStream.h"
#include "codegen/carbon_priv.h"

#include <iomanip>

const UInt32 val[3] = {0,0,1}; // can do 96 bits
const UInt32 ones[4] = {0xffffffff,0xffffffff,0xffffffff,0xffffffff};

#ifdef USE_TEMPLATES
# include "util/BitVector.h"
# include "util/BitVectorStream.h"
# define STYLE "TEMPLATE"
# define BITVECTOR0(size)                BitVector<size>
# define BITVECTOR0v(var, size)          BitVector<size> var(KUInt64(0))
# define BITVECTOR1(size, a1)            BitVector<size>(a1)
# define BITVECTOR1v(var, size, a1)      BitVector<size> var(a1)
# define BITVECTOR2(size, a1, a2)        BitVector<size>(a1, a2)
# define BITVECTOR2v(var, size, a1, a2)  BitVector<size> var(a1, a2)

# define SBITVECTOR0(size)                SBitVector<size>
# define SBITVECTOR0v(var, size)          SBitVector<size> var(KSInt64(0))
# define SBITVECTOR1(size, a1)            SBitVector<size>(a1)
# define SBITVECTOR1v(var, size, a1)      SBitVector<size> var(a1)
# define SBITVECTOR2(size, a1, a2)        SBitVector<size>(a1, a2)
# define SBITVECTOR2v(var, size, a1, a2)  SBitVector<size> var(a1, a2)

struct Statics {
  BitVector<65> two_exp_64;
  BitVector<65> two_exp_63;
  BitVector<128> allones;

  BitVector<128> bus;
  BitVector<65> alu;
  BitVector<65> mac;

  Statics():
    two_exp_64(val, 3),
    two_exp_63(KUInt64(0x8000000000000000)),
    allones(ones, 4),
    bus(KUInt64(0)),
    alu(KUInt64(0)),
    mac(KUInt64(0))
  {}

  int bvtest();
  int sbvtest();
  void s_rsh (SInt64 i);
  void s_do_flip (void);
  void signextend (void);
  void s_fieldarith (void);
  void s_concats (void);
  void uneg (void);
  void do_flip (void);
  void lsh (UInt64 i);
  void rsh (UInt64 i);
  void zeroextend (void);
  void s_tryfunny (void);
  void tryfindbit (void);
  void concats (void);
  void fieldarith (void);
  void tryfunny (void);
  void s_uneg (void);
  void s_lsh (SInt64 i);
};

#endif

#ifdef USE_DYNAMIC
# include "util/DynBitVector.h"
# define STYLE "DYNAMIC"
# define BITVECTOR0(size)                DynBitVector(size)
# define BITVECTOR0v(var, size)          DynBitVector var(size)
# define BITVECTOR1(size, a1)            DynBitVector(size, a1)
# define BITVECTOR1v(var, size, a1)      DynBitVector var(size, a1)
# define BITVECTOR2(size, a1, a2)        DynBitVector(size, a1, a2)
# define BITVECTOR2v(var, size, a1, a2)  DynBitVector var(size, a1, a2)

//# define SBITVECTOR0(size)                SDynBitVector(size)
//# define SBITVECTOR0v(var, size)          SDynBitVector var(size)
//# define SBITVECTOR1(size, a1)            SDynBitVector(size, a1)
//# define SBITVECTOR1v(var, size, a1)      SDynBitVector var(size, a1)
//# define SBITVECTOR2(size, a1, a2)        SDynBitVector(size, a1, a2)
//# define SBITVECTOR2v(var, size, a1, a2)  SDynBitVector var(size, a1, a2)

struct Statics {
  DynBitVector two_exp_64;
  DynBitVector two_exp_63;
  DynBitVector allones;

  DynBitVector bus;
  DynBitVector alu;
  DynBitVector mac;

  Statics():
    two_exp_64(65, val, 3),
    two_exp_63(65, KUInt64(0x8000000000000000)),
    allones(128, ones, 4),
    bus(128),
    alu(65),
    mac(65)
  {
  }


  int bvtest();
  int sbvtest();
  void s_rsh (SInt64 i);
  void s_do_flip (void);
  void signextend (void);
  void s_fieldarith (void);
  void uneg (void);
  void do_flip (void);
  void lsh (UInt64 i);
  void rsh (UInt64 i);
  void zeroextend (void);
  void s_tryfunny (void);
  void tryfindbit (void);
  void fieldarith (void);
  void tryfunny (void);
  void s_uneg (void);
  void s_lsh (SInt64 i);

  void testDBVRangeLoop();
  void test_setrange(UInt32 bvsize, UInt32 numBits);
};

#endif

static int errors = 0;

#define FAIL(stmt)	\
    do {UtIO::cout() << __LINE__ << ":" << stmt << UtIO::dec; errors++;} while (0)

#define CHECK(cond) \
    do {if (!(cond)) {UtIO::cout() << __LINE__ << " failed\n" << UtIO::dec; errors++;}} while (0)

#ifdef USE_TEMPLATES
void Statics::concats (void)
{
  static UInt32 raw[]={ 0x01234567, 0x89abcdef,0xfedcba98,0x76543210};
  BITVECTOR2v (updown, 128, raw,4);

  BITVECTOR0v (dest,128);
  BITVECTOR0v (realdest,128);
  BITVECTOR0v (allzero, 128);
  allzero = 0;

  realdest = *reinterpret_cast <const BitVector<128>*>(((
     ((dest.lpartsel (0,40)=updown.partsel (60,40))  /* edcba988, 0f*/
      .lpartsel_advance (60)=updown.partsel (64,60)) /* fedcba98, 6543210*/
     .lpartsel_advance (28)=updown.partsel (60,28))  /* edcba98 */
.base<BitVector<128> > (100)));

  if (realdest.partsel (0,64).llvalue () != KUInt64(0xdcba980fedcba988)
      || realdest.partsel (64,64).llvalue () != KUInt64(0xdcba9886543210fe))
    FAIL ("big chunk concat fail\n");

  // This is a good place to test BVref comparisons

  if (updown.partsel (0,128) != updown)
    FAIL ("A[lo:hi] == A fails\n");

  if (updown.partsel (12,72) == updown.partsel (12,68))
    FAIL ("differnet sizes compare wrong\n");

  // Test <, >, etc.
  if (updown.partsel (12,7) < updown.partsel (12,68))
    if (updown.partsel (12,68) > updown.partsel (12,7))
      ;                         // This is okay
    else
      FAIL ("a!=b, a<b -> b > a failed(1)\n");
  else
    if (updown.partsel (12,68) < updown.partsel (12,7))
      FAIL ("a!=b, a > b -> b < a failed(2)\n");

  if (updown.partsel (12,68) < updown.partsel (12,68))
    FAIL ("a < a\n");
  else if (updown.partsel (12,68) < allzero.partsel (12, 40))
    FAIL ("BV(a) < 0 - oops\n");
  else if (updown.partsel (12,40) >= allones.partsel (0, 40))
    FAIL ("BV(a) >= allones\n");

  // checked_test and test work

  if (updown.partsel (12, 72).test (1) != updown.partsel (12,72).checked_test (1))
    FAIL ("checked_test of partsel problems\n");

  BITVECTOR0v (scratch,128);
  BITVECTOR0v (itch,128);
  scratch.lpartsel (60,68) = updown;
  itch.lpartsel (30,68) = updown;

  if (scratch.partsel (60,68) != itch.partsel (30,68))
    FAIL ("unaligned compare fails\n");

  if (scratch.partsel (60,68) != itch.partsel (30,72))
    FAIL ("zero extend compare fails\n");

  // Now try at least one that SHOULD be different
  if (scratch.partsel (1,120) == itch.partsel (1,120))
    FAIL ("compare succeeds when different values. \n");

  // Test the all() method - only present for DynBitVector
  scratch.set ();               // all on
  scratch.set (64,0);           // clear one

#if 0 // bvref has no method all()
  if (scratch.partsel (1,120).all ())
    FAIL ("all() fails\n");

  // probe edges
  if (scratch.partsel (0,64).all () &&
      scratch.partsel (65,63).all ())
    ;
  else
    FAIL ("all() probe fails\n");

  if (scratch.partsel (48, 62).all ())
    FAIL ("random bounds .all() failed\n");
#endif

  BITVECTOR0v (A,64);
  BITVECTOR0v (B,64);
  BITVECTOR0v (C,64);

  dest=0;
  realdest=allones;

  A=KUInt64 (0xaaaaaaaaaaaaaaaa);
  B=KUInt64 (0xbbbbbbbbbbbbbbbb);
  C=KUInt64 (0xcccccccccccccccc);

  realdest = *reinterpret_cast<const BitVector<128> *>((((dest.lpartsel (0,20)=
		A.partsel (4,20)).lpartsel_advance (24)=
	       B.partsel (8,24)).lpartsel_advance (64)=
	      C.partsel (0,64)).base<const BitVector<128> >(44));

  if (realdest != dest)
    FAIL ("Assign from base()-ed BV\n");
  if (realdest.partsel (0,20).value () != 0xaaaaa
      || realdest.partsel (20,24).value () != 0xbbbbbb
      || realdest.partsel (44,64).llvalue () != KUInt64 (0xcccccccccccccccc)
      || realdest.partsel (108,20).value () != 0)
    FAIL ("concat broken\n");
}
#endif

void Statics::fieldarith (void)
{
  alu = allones;

  alu.lpartsel (1,63)+= KUInt64(1);
  if (alu.partsel (0,64).llvalue () != 1
      || alu.partsel (64,1).llvalue () != 1)
    FAIL ("partsel+=uint FAIL\n");

  alu.lpartsel (1,63) -= KUInt64(1);
  if (alu.count () != alu.size ())
    FAIL ("partsel-=uint FAIL\n");

  // Bug observed in BV op= UInt64
#ifdef USE_TEMPLATES
  // +=(UInt64) not defined for DynBitVector
  // -=(UInt64) not defined for DynBitVector

  alu = 0;
  alu.lpartsel (1,63) += KUInt64 (0x100000000);
  if (alu.partsel (0,64).llvalue () != KUInt64 (0x200000000))
    FAIL ("BV::operator+=(U64) broken\n");

  alu.lpartsel (1,63) -= KUInt64 (0x100000000);
  if (alu.partsel (0,64).llvalue () != 0)
    FAIL ("BV op= symmetry\n");
#endif
  
  // exhaustive test

  alu = 0;
  for(unsigned int i=0;i<65535;++i)
    {
      alu.lpartsel (27,16) |=i;	// Set bits
      alu.lpartsel (27,16) &=~i; // Clear them
      if (alu.partsel (26,18).any ())
	{
	  FAIL ("partsel and/or\n");
	  break;
	}
    }

  alu = 0;
  for(UInt32 i=0;i<65535;++i)
    {
      alu.lpartsel (27,16) = i;
      if (alu.partsel (26,18).value () != ((i<<1)))
	{
	  FAIL ("parsel deposit leaks\n");
	  break;
	}
    }

  alu = 0;
  for(unsigned int i=0;i<=65535;++i)
    {
      alu.lpartsel (27,16) += (unsigned int) 1;
      if (alu.partsel (26,18).value () != ((i+1)%65536)<<1)
	{
	  FAIL ("counter leaks\n");
	  break;
	}
    }

  // test down counters too
  alu = 0;
  for(int i=65535; i>=0;--i)
    {
      if (alu.partsel (26,18).value () != (((unsigned int)i+1)%65536)<<1)
	{
	  FAIL ("downcounter leaks\n");
	  break;
	}
      alu.lpartsel (27,16) -= 1U;
    }
  if (alu.count ())
    FAIL ("downcounter bugs\n");

  // Test xor bug
  bus = allones;

#ifdef USE_TEMPLATES
  bus.lpartsel (1,64) = BitVector<128>(allones) ^ bus.partsel (0,128);

  if (bus.count () != 64 || bus.llvalue (0,64) != 1 || bus.llvalue (64,64) != KUInt64 (0xFFFFFFFFFFFFFFFE))
    FAIL ("xor partsel bug\n");
#endif
}

// Tests for findfirst findlast
void Statics::tryfindbit (void)
{
#ifdef USE_DYNAMIC
#define _FFS findFirstOne
#define _FLS findLastOne
#else
#define _FFS FFO
#define _FLS FLO
#endif

  if (allones._FFS () != 0 || allones._FLS () != 127)
    FAIL ("FFS fails\n");

  if (two_exp_63._FFS () != 63
      || two_exp_64._FFS () != 64
      || two_exp_64._FLS () != 64)
    FAIL ("FF B\n");

#ifndef USE_DYNAMIC
  if (two_exp_64.FFZ () != 0
      || two_exp_64.FLZ () != 63) // Bet this fails because of ~ without mask
    FAIL ("FFZ fails\n");

  if (allones.FFZ () != -1 || allones.FLZ () != -1)
    FAIL ("FindZero B\n");

  bus = 0;
  if (bus.FFZ () != 0 || bus.FLZ () != 127
      || bus.FFO () != -1 || bus.FLO () != -1)
    FAIL ("FindO|Z C\n");

  // Do some partselect tests too
  if (two_exp_64.partsel (1,60).FFO () != -1
      || two_exp_64.partsel (1,64).FFO () != 63
      || two_exp_64.partsel (64,1).FFO () != 0)
    FAIL ("FFO partsel\n");

  if (two_exp_64.partsel (1,64).FFZ () != 0
      || two_exp_64.partsel (1,64).FLZ () != 62
      || two_exp_64.partsel (64,1).FFZ () != -1)
    FAIL ("FF partsel 2\n");

  // Try some exhaustive tests
  alu = 1;

  for (int i=0; i<65; ++i)
  {
    if (alu.FFO () != i)
      FAIL ("FFS alu\n");
    else if (alu.FFO () != alu.FLO ())
      FAIL ("FFO != FLO\n");

    mac = alu;
    mac.flip ();
    if (mac.FFZ () != i)
      FAIL ("FFZ alu\n");
    else if (mac.FFZ () != mac.FLZ ())
      FAIL ("FFZ != FLZ\n");

    alu <<= 1;
  }

  alu = 1;
  mac = alu;
  mac.flip ();

  // And for partselects too
  for (int j = 0; j < 65; ++j)
  {
    for (int i =0; i <65; ++i)
    {
      if (alu.partsel (i,65-i).FFO () != alu.partsel (i,65-i).FLO ())
        FAIL ("partsel.FFO != partsel.FLO\n");

      if (mac.partsel (i,65-i).FFZ () != mac.partsel (i,65-i).FLZ ())
        FAIL ("partsel.FFZ != partsel.FLZ\n");
    }

    alu <<= 1;
    mac = alu;
    mac.flip ();
  }

#endif
  
}

void Statics::tryfunny (void)
{
#ifdef USE_TEMPLATES
  // We need a method to return the WHOLE bitvector after an
  // assignment to the partsel.
  bus = *reinterpret_cast<const BITVECTOR0(128)*>(((BITVECTOR1(128, 65u)).lpartsel (64,64)
         = KUInt64(0xFFFFFFFFFFFFFFFF)).base<BITVECTOR0 (128) >(64));

  if (bus.llvalue (0,64) != 65
      || bus.llvalue (64,64) != KUInt64(0xFFFFFFFFFFFFFFFF))
    FAIL ("Lvalue temp hack fails (65 != " << bus.llvalue (0,64)
          << "), (MAXUInt != " << bus.llvalue (64,64) << "\n");

  bus = BitVector<65>(bus) << 32;
  bus = (BitVector<65>(bus)) >> 1;
#endif
}

// Test zero-extended bitvectors.
void Statics::zeroextend (void)
{
  BITVECTOR1v(words, 64, allones); // Initialized from larger object.

  if (words.count () != 64)
    FAIL ("copy constructor failed\n");

#define GCC_402_BUG 0
#if GCC_402_BUG && USE_TEMPLATES
  // All these prints cause gcc/product 402 to work.  In the absense of them,
  // bvtest fails with templates here.

  UtIO::cout() << "words.llvalue " << UtIO::hex << words.llvalue() << UtIO::endl;
  UtIO::cout() << "two_exp(64,1) " << UtIO::hex << two_exp_64.partsel(64, 1) << UtIO::endl;
  BVref<false> lref(words.lpartsel (12,32));
  BVref<false> rref(two_exp_64.partsel (64,1)); // one bit on...
  lref = rref;
  UtIO::cout() << "lref = {" << UtIO::dec << lref._M_bpos << "," << lref._M_bsiz << "}\n";
  UtIO::cout() << "rref = {" << rref._M_bpos << "," << rref._M_bsiz << "}\n";
  UtIO::cout() << "words.llvalue " << UtIO::hex << words.llvalue() << UtIO::endl;
#else
  words.lpartsel (12,32) = two_exp_64.partsel (64,1); // one bit on...
#endif

  if (words.llvalue () != KUInt64(0xFFFFF00000001FFF)) {
#if GCC_402_BUG && USE_TEMPLATES
    UtIO::cout() << "words.llvalue " << UtIO::hex << words.llvalue() << UtIO::endl;
#endif
    FAIL ("zero extended copy fails\n");
  }

  words.lpartsel (13,30) = allones; // Assign bigger field to smaller
  if (words.llvalue () != KUInt64 (0xfffff7ffffffffff))
    FAIL ("spilled over assignment\n");
}

#define S(n) v=(x<<n); if (v.llvalue() != ux<<n) FAIL(UtIO::hex << (ux << n) << "!=" << v.llvalue() << "\n")

void Statics::lsh (UInt64 i)
{
  BITVECTOR0v(v, 64);
  BITVECTOR1v(x, 64, i);
  UInt64 ux = i;
  S (0);
  S (1);
  S (2);
  S (3);
  S (4);
  S (5);
  S (6);
  S (7);
  S (8);
  S (9);
  S (10);
  S (11);
  S (12);
  S (13);
  S (14);
  S (15);
  S (16);
  S (17);
  S (18);
  S (19);
  S (20);
  S (21);
  S (22);
  S (23);
  S (24);
  S (25);
  S (26);
  S (27);
  S (28);
  S (29);
  S (30);
  S (31);
  S (32);
  S (33);
  S (34);
  S (35);
  S (36);
  S (37);
  S (38);
  S (39);
  S (40);
  S (41);
  S (42);
  S (43);
  S (44);
  S (45);
  S (46);
  S (47);
  S (48);
  S (49);
  S (50);
  S (51);
  S (52);
  S (53);
  S (54);
  S (55);
  S (56);
  S (57);
  S (58);
  S (59);
  S (60);
  S (61);
  S (62);
  S (63);
}
#undef S
#define S(n) v=(x>>n); if (v.llvalue() != ux>>n) FAIL(UtIO::hex << (ux << n) << "!=" << v.llvalue() << "\n")

void Statics::rsh (UInt64 i)
{

  BITVECTOR1v(x, 64, i);
  BITVECTOR0v(v, 64);

  UInt64 ux = i;
  S (0);
  S (1);
  S (2);
  S (3);
  S (4);
  S (5);
  S (6);
  S (7);
  S (8);
  S (9);
  S (10);
  S (11);
  S (12);
  S (13);
  S (14);
  S (15);
  S (16);
  S (17);
  S (18);
  S (19);
  S (20);
  S (21);
  S (22);
  S (23);
  S (24);
  S (25);
  S (26);
  S (27);
  S (28);
  S (29);
  S (30);
  S (31);
  S (32);
  S (33);
  S (34);
  S (35);
  S (36);
  S (37);
  S (38);
  S (39);
  S (40);
  S (41);
  S (42);
  S (43);
  S (44);
  S (45);
  S (46);
  S (47);
  S (48);
  S (49);
  S (50);
  S (51);
  S (52);
  S (53);
  S (54);
  S (55);
  S (56);
  S (57);
  S (58);
  S (59);
  S (60);
  S (61);
  S (62);
  S (63);
}
#undef S

#ifdef USE_DYNAMIC
static void signed_mult(UInt32 asize, UInt32 bsize, UInt32 csize) {
  // signed multiply of varying sizes
  DynBitVector a(asize), b(bsize);
  DynBitVector c(csize), neg_one(csize), one(csize);
  neg_one.set();                // -1
  one.set(0);                   // 1
  a.set();                      // -1
  b.set();                      // -1
  c.signedMultiply(a, b);       // 1
  if (c != one) {
    FAIL("c != 1\n");
  }
  a.copy(one);
  c.signedMultiply(a, b);       // -11
  if (c != neg_one) {
    FAIL("c != -1\n");
  }
}

static void signed_compare(UInt32 asize, UInt32 bsize) {
  // signed multiply of varying sizes
  DynBitVector a_n1(asize), a1(asize), a_small(asize), a0(asize), a_big(asize);
  DynBitVector b_n1(bsize), b1(bsize), b_small(bsize), b0(bsize), b_big(bsize);

  // On review of the initial implementation, Rich found a hole:
  //
  // a value     b value   a_hi_word  b_hi_word  a_hi_word_signEx  b_hi_word_signEx
  // ----------- -------   ---------  ---------  ----------------  ----------------
  // {1b1,66'b0} {1b1,65'b0}  3'b100     2'b10       32'hFFFC         32'hFFFE
  // {1b1,66'b0} {1b1,129'b0} 3'b100     2'b10       32'hFFFC         32'hFFFE
  
  UInt32 rich_asize = 32 * ((asize + 31) / 32) + 3;
  UInt32 rich_bsize = 32 * ((bsize + 31) / 32) + 2;
  DynBitVector a_rich(rich_asize);
  DynBitVector b_rich(rich_bsize);

  a_rich.set(rich_asize - 1);
  b_rich.set(rich_bsize - 1);

  if (rich_asize < rich_bsize) {
    // a_rich has less bits so it's negative number will be less negative,
    // therefore larger
    CHECK(a_rich.signedCompare(b_rich) > 0);
  }
  else if (rich_asize == rich_bsize) {
    CHECK(a_rich.signedCompare(b_rich) == 0);
  }
  else {
    CHECK(a_rich.signedCompare(b_rich) < 0);
  }

  a_n1.set();                   // -1
  a1.set(0);                    // 1
  b_n1.set();                   // -1
  b1.set(0);                    // 1
  a_small.set(asize - 1);       // most negative number
  b_small.set(bsize - 1);       // ditto
  a_big.setRange(0, asize - 1, 1);
  b_big.setRange(0, bsize - 1, 1);

  DynBitVector cpy;
  cpy = a_big;   CHECK(a_big.signedCompare(cpy) == 0);
  cpy = a1;      CHECK(a1.signedCompare(cpy) == 0);
  cpy = a0;      CHECK(a0.signedCompare(cpy) == 0);
  cpy = a_n1;    CHECK(a_n1.signedCompare(cpy) == 0);
  cpy = a_small; CHECK(a_small.signedCompare(cpy) == 0);
  CHECK(a_big.signedCompare(a_big) == 0);
  CHECK(a1.signedCompare(a1) == 0);
  CHECK(a0.signedCompare(a0) == 0);
  CHECK(a_n1.signedCompare(a_n1) == 0);
  CHECK(a_small.signedCompare(a_small) == 0);

  CHECK(a1.signedCompare(b1) == 0);
  CHECK(a_n1.signedCompare(b_n1) == 0);
  CHECK(a0.signedCompare(b0) == 0);

  CHECK(a_big.signedCompare(b1) > 0);   // assume bsize>1
  CHECK(a_big.signedCompare(b0) > 0);
  CHECK(a_big.signedCompare(b_n1) > 0);
  CHECK(a_big.signedCompare(b_small) > 0);
  CHECK(a1.signedCompare(b0) > 0);
  CHECK(a1.signedCompare(b_small) > 0);
  CHECK(a1.signedCompare(b_n1) > 0);
  CHECK(a0.signedCompare(b_small) > 0);
  CHECK(a0.signedCompare(b_n1) > 0);
  CHECK(a_n1.signedCompare(b_small) > 0);
}  
#endif

void Statics::do_flip (void)
{
  BITVECTOR0v(value, 128);

  if (!value[0])
    ;
  else
    FAIL ("!BVref fails\n");

  value.lpartsel (1,10) = ((UInt64) 0x3fful);	// set a complete field
  value.lpartsel (0,11).flip (); // toggle it (and the LSB)
  if (value.partsel (0,11).value () != 1u)
    FAIL ("do_flip of solid field\n");

  value.lpartsel(1,10).flip (); // turn them back on
  if (value.partsel (0,11).value () != 0x7ffu)
    FAIL ("do_flip assignment fails\n");
}

void Statics::uneg (void)
{
  BITVECTOR0v(value, 128);

  value = -value;
  if (value.any ()) FAIL ("-0 != 0\n");

  value = 1;
  value = -value;
  if (value.count () != 128) FAIL ("-(1) not propagated\n");

  BITVECTOR1v(v, 127, value);

  if (v.count () != 127)
    FAIL ("copy constructor sets extra bit\n");
  v = -v;
  if (v.count () != 1 || v.value () != 1)
    FAIL ("unary negate bugs\n");

  value = 10;
  value = -value;
  value = -value;
  if (value != UInt64(10)) FAIL ("double negation of 10 failed\n");

  value = 1;
  value.negate();
  if (value.count () != 128) FAIL ("DynBV negate() failed\n");
}


int bvtest (void) {
  Statics statics;
  return statics.bvtest();
}

int Statics::bvtest (void)
{
  UtIO::cout() << "Testing " << STYLE << " bit vectors..." << UtIO::endl;

  // Test for funny bitfield assignments
  zeroextend ();

  // Test multiple assignments to temp constructed BV
  tryfunny ();

  // Test bit searchs
  tryfindbit ();

#ifdef USE_TEMPLATES
  // test concatenations
  concats ();

  // Test partsel arithmetic
  fieldarith ();
#else
  testDBVRangeLoop();

  // Major aggressive bv testing.  Our inner loops run about 127^4
  // times, which is 260M.  Yow!
  for (UInt32 bvsize = 1; bvsize < 127; ++bvsize) {
    for (UInt32 numBits = 1; numBits <= bvsize; ++numBits) {
      test_setrange(bvsize, numBits);
    }
  }
#endif

  // test bit flipping
  do_flip ();

  // test unary negate
  uneg ();

  // Test left shifts;

  lsh (1);
  lsh (KUInt64(0xFFFFFFFFFFFFFFFE));
  lsh (255);

  // And right shifts too
  rsh (1);
  rsh (KUInt64(0x8000000000000000));
  rsh (KUInt64(255)<<56);

  // Bug discovered by Mark S.
  // right shift by a multiple of 32
  UInt32 lgArr[] = {0xd1e9a159, 0xd1e9a159, 0xa0381744, 0x725781b9, 
                    0x4c673942, 0x44abcb83, 0x3c19b60b, 0xe0c80696};
  BITVECTOR2v(lgrsBug, 256, lgArr, 8);
  lgrsBug >>= 224;
  if (lgrsBug.value() != lgArr[7])
    FAIL ("Shift right by multiple of 32 wrong!\n");    

  // Bug discovered by Mark S.
  BITVECTOR1v(f, 4, 0u);
  f += 1U;
  if (f.value () != 1)
    FAIL ("addition error\n");
  
  // Bug175
  BITVECTOR1v(m1, 128, allones);

#ifdef USE_TEMPLATES
  m1.lpartsel (31,4) = ((UInt64) 0);
  if (m1.partsel (0,32).value () != 0x7FFFFFFF
      || m1.partsel (32,32).value () != 0xFFFFFFF8)
    FAIL ("BUG175!\n");

  m1.lpartsel (64,64) <<= 16;   // put some zeros in the high word
  if (m1.partsel (64,32) != (UInt32) 0xffff0000u)
    FAIL ("<<= or partsel compare problems\n");

  m1.lpartsel (64,64) >>= 16;
  if (m1.partsel (64,32) != (UInt32) 0xffffffff
    ||m1.partsel (96,32) != (UInt32) 0x0000ffff)
    FAIL (">>= or partsel compare problems\n");


# if pfGCC3
  if (sizeof(two_exp_63) != 12
      || sizeof(allones) != 16)
    FAIL ("bitvector sizeof wrong\n");
# endif
# if pfGCC2
  if (sizeof(two_exp_63) != 16
      || sizeof(allones) != 20)
    FAIL ("bitvector sizeof wrong\n");
# endif
#endif

  if (two_exp_63 != two_exp_63)
    FAIL ("initialization or equality fails" << two_exp_63 << "\n");

  alu = two_exp_63;

  if (alu[0].value () || !alu[63].value ())
    FAIL ("one bit test fails\n");

  
  if (alu != two_exp_63)
    FAIL ("assignment fails " << alu << "\n");

  if (two_exp_64 != (two_exp_63 + two_exp_63))
    FAIL ("add fails == \n");

  if (two_exp_64 != (two_exp_63 << 1))
    FAIL ("shift fails\n");

  // Note that boost::operator produces complaints if we compare
  // BITVECTOR0(128> to BITVECTOR0(65>

#ifdef USE_DYNAMIC
  // operator= for DynBitVector transfers numBits from rhs to lhs.
  bus.copy(two_exp_64);
#else
  bus = two_exp_64;
#endif
  if (bus != BITVECTOR1(128, two_exp_64))
    FAIL ("BitV<128> != BitV<65> fails\n");

#ifdef USE_DYNAMIC
  // Templates give error:
  // ..../bvtest.cxx:545: error: no match for 'operator-=' in 'bus -= two_exp_63'
  bus -= two_exp_63;

  bus += two_exp_63;

  if (bus != BITVECTOR1(128, two_exp_64))
    FAIL ("subtract fails " << bus << "\n");

  bus <<= 1;			// 2**65


  if (bus <= BITVECTOR1(128, two_exp_64))
    FAIL ("unsigned compare fails " << bus << "\n");

  if (bus == allones)
    FAIL ("equality fails \n");

  alu.copy(bus);
#else
  bus <<= 1;			// 2**65
  alu = bus;			// Should result in zero after truncation
#endif

  if (0 != alu.value ())
    FAIL ("truncation fails expect zero, got " << alu << "\n");

#ifdef USE_DYNAMIC
  // operator= for DynBitVector transfers numBits from rhs to lhs.
  mac.copy(alu + BITVECTOR1(65, KUInt64 (1)));
#else
  mac = alu + BITVECTOR1(65, KUInt64 (1));
#endif
  if (mac <= alu)
    FAIL ("unsigned compare fails " << mac << "\n");

  if (mac.none ())
    FAIL ("check for zeros fails\n");

  if (mac.count () != 1)
    FAIL ("population count fails count(" << mac << ")=" << mac.count () << "\n");

  // Now some fancier stuff...

  mac[0] = false;		// Turn off only set bit
  if (mac.value () != (0))
    FAIL ("clearing 1 bit fails\n");

  mac.lpartsel (1,2) = ((UInt64) 3ul);		// Should set both bits

  if (mac.value () != (6))
    FAIL ("setting a range of bits fails, expect 6, " << mac << "\n");

  if (mac.partsel (1,2).value () != 3)
    FAIL ("reading a range of bits fails\n");


#ifdef USE_DYNAMIC
  alu.copy(allones);
#else
  alu = allones;		// Only copy 65 bits
#endif
  if (alu.partsel (64,64).llvalue () != 1)
    FAIL ("bitvector assign writes too much\n");

#ifdef USE_TEMPLATES
  // Stuff that probably won't compile for dynamic bitvectors..
  bus = allones;
  mac = 0;

  bus.lpartsel (16,64) &= mac.partsel (1,64);
  if (bus.partsel (0,64).llvalue () != 0xffff
      && bus.partsel (64,64).llvalue () != KUInt64 (0xffff000000000000))
    FAIL ("partsel &= bug\n");

  bus.lpartsel (16,64) |= allones;
  if (bus.count () != bus.size ())
    FAIL ("partsel|=bitvector fails\n");

  bus = 0;
  bus.lpartsel (0,33) |= allones.partsel (65,33);
  bus.lpartsel (33, 66) |= allones.partsel (1,66);
  bus.lpartsel (99, 29) |= allones.partsel (31,29);

  if (bus.count () != 128)
    FAIL ("lpartsel |= partsel fails\n");

  bus = 0;
  bus.lpartsel (1,127) = alu & allones.partsel (0,127);
  if (bus.partsel (0,64).llvalue () != KUInt64(0xfffffffffffffffe)
      || bus.partsel (64,64).llvalue () != ((UInt64) 3))
    FAIL ("lpartsel = bv & partsel fail \n");

  // Test op= UInt64
  bus = 0;
  UInt64 v = KUInt64(0x100000000);
  bus += v;
  if (bus != v)
    FAIL ("+= 64 bit fails\n");

  bus += KUInt64(1);
  bus &= KUInt64 (0x1FFFFFFFF);
  if (bus != KUInt64 (0x100000001))
    FAIL ("&= 64 bit fails\n");

  bus &= ~KUInt64(0x100000001);
  bus |= KUInt64 (0xFFFFFFFF00000000);
  bus ^= KUInt64 (0x5555555555555555);
  if (bus != KUInt64 (0xaaaaaaaa55555555))
    FAIL ("^= 64 bit fails\n");


  // BUG 1104 64 bit +/- not tested
  BITVECTOR0v (adc, 128);

  adc = KUInt64 (0x8000000000000000);
  adc += KUInt64 (0x8000000000000000); // Check that this carries

  if(adc.count () != 1 || !adc.test (64))
    FAIL ("carry add fails!\n");

  adc -= KUInt64 (0x4000000000000000); // won't propagate
  if (adc.llvalue () != KUInt64 (0xC000000000000000)
      || adc.lpartsel (64,64) != KUInt64(0x0))
    FAIL ("borrow subtract fails!\n");

  BITVECTOR0v(alu65, 65);
  alu65  = KUInt64 (0x8000000080000000);
  alu65 += KUInt64 (0x8000000080000000); // Carry up

  if (alu65.llvalue() != KUInt64(0x0000000100000000)
      || not alu65.test(64))
    FAIL ("Carry propagation fails!\n");
#endif

  // Copy some aligned bits
  alu = mac;
  if (alu != mac)
    FAIL ("assignment fails\n");

  bus ^= bus;			// clear all bits
  if (bus.llvalue () != (0))
    FAIL ("XOR failed " << bus << "\n");

  // Set a range
  bus.lpartsel (8,56) = allones.partsel (8,56);

  if (bus.partsel (8,8).value () != 255 ||
      bus.partsel (16,16).value () != 65535 ||
      bus.partsel (32,32).value () != 0xFFFFFFFF)
    FAIL ("aligned range assignment fails - expect 56 1'b " << bus << "\n");

  // Add unaligned moves and compares...

#ifdef USE_DYNAMIC
  // Templates give error:
  // .../bvtest.cxx:700: error: ambiguous 
  // overload for 'operator!=' in 'BitVector<_Nb>::partsel(short unsigned int, 
  // short unsigned int) const [with unsigned int _Nb = 128](8, 32) != 
  // BitVector<_Nb>::partsel(short unsigned int, short unsigned int) const [with 
  // unsigned int _Nb = 128](1, 32)'

  if (allones.partsel (8,32) != allones.partsel (1,32))
    FAIL ("unaligned compares fail\n");
#endif

  bus -= bus;			// zero
  if (bus.llvalue () != (0))
    FAIL ("subtract failed " << bus << "\n");

  if (bus.partsel (8,32).any() || !allones.partsel (34,62).any())
    FAIL ("any() on reference fails\n");

  if (bus.partsel (8,120).count() != 0 || allones.partsel (18,34).count() != 34)
    FAIL ("count() fails\n");

  bus.lpartsel (8,32) = allones.partsel (0,32);
  bus.lpartsel (0,8) = allones.partsel (8,8);
  bus.lpartsel (40,88) = allones; // A truncating assignment

  if (bus != allones)
    FAIL ("random field moves fails, expect 128'b1 " << bus << "\n");

  // Finally test for overlapping assignments.

  bus.lpartsel (8,8) = KUInt64(0);
  bus.lpartsel (0,32) = bus.partsel (8,32);
  if (bus.partsel (0,32).value () != 0xffffff00)
    FAIL ("overlap wrong\n");

  bus.lpartsel (8,32) = bus.partsel (0,32);
  if (bus.partsel (0,32).value () != 0xffff0000)
    FAIL ("overlap two wrong\n");

  // See if multiple references from RHS cause trouble
  static UInt32 checkvec[] = {0x76543210, 0xfedcba98, 0x01234567, 0x89abcdef};

  BITVECTOR2v(checkval, 128, checkvec, 4);

  bus.lpartsel (0,16) = checkval.partsel(0,16);
  bus.lpartsel (16,16) = checkval.partsel(0,16);
  bus.lpartsel (32,16) = checkval.partsel(0,16);
  bus.lpartsel (48,16) = checkval.partsel(0,16);
  bus.lpartsel (64,16) = checkval.partsel(0,16);
  bus.lpartsel (80,16) = checkval.partsel(0,16);

  if (bus.partsel(0,32).value () != 0x32103210
      || bus.partsel (32,32).value () != 0x32103210
      || bus.partsel (64,32).value () != 0x32103210
      || bus.partsel (96,32).value () != 0xffffffff)
    FAIL ("Expected repeat 0x3210 pattern " << bus << "\n");

  

  // Test field-select and arithmetic
  bus.lpartsel(0,16) += KUInt64(1);

  if (bus.partsel(0,16).value () != 0x3211)
    FAIL ("expected 0x3211, got " << bus.partsel(0,16).value () << "\n");

  int count = bus.partsel(0,32).value () + 1;

  if (count != 0x32103212)
    FAIL ("expected 0x32103212  got " << UtIO::hex << count << "\n");

  // Test for commutativity?
  count = (1<<16) + bus.partsel(0,32).value ();
  if (count != 0x32113211)
    FAIL ("expected 0x32113211, got " << UtIO::hex << count << UtIO::dec << "\n");

  // Need tests of all other operators, especially divide, multiply, modulus
  // and the reduction operators...

  bus = 1;
  if (bus.count () != 1)
    FAIL ("population count fails got " << bus.count () << "\n");

  if (not bus.redxor ())
    FAIL ("reduction xor fails\n");

  bus *= 2u;
  if (bus.partsel(0,16).value () != 2)
    FAIL ("multiply fails\n");

  if ((bus << 1) != BITVECTOR1(128, 4u))
    FAIL ("left shift fails got " << bus << "\n");

  if ((bus << 128) != BITVECTOR1(128, 0u))
    FAIL ("big left shift fails " << (bus << 128) << "\n");

  bus *= 0u;
  if (bus.count () != 0)
    FAIL ("multiply by zero fails got " << bus << "\n");
  
  if (bus.redxor ())
    FAIL ("redxor(0) fails\n");

  // Some more reference tests
  bus = 0;
  bus.lpartsel (8,8) |= KUInt64(255);

  if (bus.partsel(0,16).value () != (255<<8))
    FAIL ("reference::|= fails\n");

  bus.lpartsel (4,8) &= KUInt64(15);
  if (bus.partsel (8,8).value () != (15<<4))
    FAIL ("reference::&= fails\n");

  bus.lpartsel (4,4) ^= KUInt64(15);
  if (bus .partsel(0,32).value () != 0xf0f0)
    FAIL ("reference::^= fails\n");

  // Test possible implementation of catenation
  //
  bus = BITVECTOR1(128, mac) | (BITVECTOR1(128, alu)<<65) | (BITVECTOR1(128, 0u)<<126);

  bus = (allones << 64) & (allones >> 64);
  if (bus.count () != 0)
    FAIL ("shifting fails, expect 0, got " << bus << "\n");

  bus = BITVECTOR1(128, ~KUInt64(0)) << 64;
#ifdef USE_DYNAMIC
  // Templates error with this:
  // .../bvtest.cxx:827: error: no match 
  // for 'operator+=' in 'bus += BitVector<64>(ffffffffffffffff)'

  bus += BITVECTOR1(64, ~KUInt64(0));

  if (bus.count () != 128)
    FAIL ("mixed mode arithmetic fails, expected all ones, got "<<bus<<"\n");
#endif

  // Test of bounded fields...

  if (allones.partsel(128,1).value ())
    FAIL ("out-or-range partsel fails\n");

  if (allones.partsel (15,64).redxor () ||
      not allones.partsel (15,65).redxor ())
    FAIL ("redxor partsel fails\n");

  // Zero length assignment ought to be ignored..
  //
  allones.lpartsel(128,2) = KUInt64(0);

  //
  allones.lpartsel(0,0) = KUInt64(1);

  if (allones.count() != 128)
    FAIL ("out-or-range assignment fails\n");

  BITVECTOR1v(shiftAdd1, 66, KUInt64(0));
  shiftAdd1 = KUInt64(1);
  shiftAdd1 <<= KUInt64(1);
  shiftAdd1 += KUInt64(1);
  if (shiftAdd1.value() != 3)
    FAIL ("Shift-add of small vector failed\n");
  
  BITVECTOR1v(shiftAdd2, 68, KUInt64(0xffffffffffffffff));
  shiftAdd2 += KUInt64(1);
  if (shiftAdd2.llvalue() != 0)
    FAIL ("Shift-add-carry of 68 bit vector failed thru 64\n"); 
  const UInt32* theArray = shiftAdd2.getUIntArray();
  if (theArray[2] != 1)
    FAIL ("Shift-add-carry of 68 bit vector failed in last word\n"); 

#ifdef USE_DYNAMIC
  DynBitVector good;
#else
  BITVECTOR0v(good, 64);
#endif

  // Copy Constructor was wrong...
  {
    BITVECTOR0v(scoped, 64);
    scoped.lpartsel (16,32) = 0xffffffff;

    good = scoped;
    // should call destructor for scoped here
  }
  if (good.llvalue () != KUInt64 (0x0000ffffffff0000))
    FAIL ("copy constructor problems?\n");

  // subtraction of small value
  BITVECTOR1v(subTest, 64, KUInt64(0xFFFFFFFFFFFFFFFF));
  subTest -= KUInt64(1);
  if (subTest != BITVECTOR1(64, KUInt64(0xFFFFFFFFFFFFFFFE)))
    FAIL ("subtraction of small constant\n");
  subTest -= KUInt64(4);
  for (size_t i = 0; i < 64; ++i)
  {
    // bits 0 and 2 should be false, others should be true
    if ((i == 0) || (i == 2))
    {
      if (subTest.test(i))
        FAIL("testing bit that should be off");
    }
    else if (!subTest.test(i))
      FAIL("testing bit that should be on");
  }

#ifdef USE_DYNAMIC
  // resize test only makes sense for dynamic bvs

  subTest.resize(32);
  if (subTest != (DynBitVector(32, 0xFFFFFFFF) - 5))
    FAIL("resize from 64 to 32");
  subTest.resize(64);
  if (subTest != (DynBitVector(64, 0x00000000FFFFFFFF) - 5))
    FAIL("resize from 32 to 64");
#else
  subTest = 0x00000000FFFFFFFF - 5;
#endif

  subTest /= 4u;
  UInt32 divCheck = (0xFFFFFFFF - 5)/4;
  if (subTest != BITVECTOR1(64, divCheck))
    FAIL("divide-by-4 failed");
#ifdef USE_DYNAMIC
  DynBitVector flipCheck = ~subTest;
#else
  BitVector<64> flipCheck = ~subTest;
#endif
  for (size_t i = 0; i < flipCheck.size(); ++i)
    if (flipCheck.test(i) == subTest.test(i))
      FAIL("flipcheck");

  // reference subtraction.  yow!  here's one aligned to a byte
  subTest.lpartsel (24,8) -= KUInt64(3);
  if (subTest.llvalue () != divCheck - (3<<24))
    FAIL("reference subtraction");

  // set the whole mother again
  subTest.set();
  if (subTest != BITVECTOR1(64, KUInt64(0xFFFFFFFFFFFFFFFF)))
    FAIL("set whole 64-bit bv");

#ifdef USE_TEMPLATES
  // test fast method for 64-bit part selects.

  // First phase: use a 64-bit BitVector, and an UInt64,
  // go through a large number of combinations, and test that they
  // get the same answers.
  for (int size = 3; size < 64; size += 17)
  {
    for (int i = 0; i < 64; ++i)
    {
      UInt64 onehotLL = KUInt64(1) << i;
      BITVECTOR1v(onehotBV, 64, onehotLL);
      for (int j = 0; j < 64; ++j)
      {
        UInt64 nBitsBV = onehotBV.llvalue(j, size);
        UInt64 mask = (KUInt64(1) << ((UInt64) size)) - 1;
        UInt64 nBitsLL = (onehotLL >> j) & mask;
        if (nBitsBV != nBitsLL)
        {
          FAIL("llvalue-range failure\n");
          UtIO::cout() << "bv: " << onehotBV << ", ll: " << onehotLL << UtIO::endl;
          UtIO::cout() << "bv64(1<<" << i << ").llvalue(" << j << "," << size
                    << ")" << UtIO::endl;
          break;
        }
      }
    }
  }

  // Simple walking-ones tests to make sure that 64-bit BVref ^, |, & work properly
  // at various alignments.
  BitVector<128> bv128;
  for (int i = 0; i < 64; ++i) {
    UInt64 onehotLL = KUInt64(1) << i;
    for (int j = 0; j < 64; ++j) {
      bv128.reset();

      // test |= on a clean vector
      bv128.lpartsel_unchecked(j, 64) |= onehotLL;
      CHECK(bv128.count() == 1);
      CHECK(bv128.test(i + j));

      // test ^= to make sure it clears that bit and sets it
      bv128.lpartsel_unchecked(j, 64) ^= onehotLL;
      CHECK(bv128.count() == 0);
      bv128.lpartsel_unchecked(j, 64) ^= onehotLL;
      CHECK(bv128.count() == 1);
      CHECK(bv128.test(i + j));

      // set all ones and then clear using &= ~onehot
      bv128.set();
      bv128.lpartsel_unchecked(j, 64) &= ~onehotLL;
      CHECK(bv128.count() == 127);
      CHECK(!bv128.test(i + j));

      // test ^= to make sure it sets that bit and clears it
      bv128.lpartsel_unchecked(j, 64) ^= onehotLL;
      CHECK(bv128.test(i + j));
      CHECK(bv128.count() == 128);
      bv128.lpartsel_unchecked(j, 64) ^= onehotLL;
      CHECK(bv128.count() == 127);
      CHECK(! bv128.test(i + j));
    }
  }

  // Second phase: use a larger bit vector, and compare the results
  // to what's expected.  Do this just 1 bit at a time for human sanity
  BITVECTOR0v(oneHot100, 100);
  for (int i = 0; i < 100; ++i)
  {
    oneHot100.set(i, 1);
    for (int j = 0; j < 100; ++j)
    {
      if ((oneHot100.llvalue(j, 1) == 1) != (j == i))
        FAIL("bv100 llvalue mismatch 1\n");
    }
    if (i >= 63)
    {
      UInt64 val = oneHot100.llvalue(i - 63, 64);
      if (val != (KUInt64(1) << 63))
        FAIL("bv100 llvalue mismatch i\n");
    }
    oneHot100.set(i, 0);
  }

  // test fast method for 32-bit part selects.

  // First phase: use a 32-bit BitVector, and an UInt32,
  // go through a large number of combinations, and test that they
  // get the same answers.
  for (int size = 3; size < 32; size += 5)
  {
    for (int i = 0; i < 32; ++i)
    {
      UInt32 onehotL = 1UL << i;
      BITVECTOR1v(onehotBV, 32, onehotL);
      for (int j = 0; j < 32; ++j)
      {
        UInt32 nBitsBV = onehotBV.value(j, size);
        UInt32 mask = (1UL << size) - 1;
        UInt32 nBitsL = (onehotL >> j) & mask;
        if (nBitsBV != nBitsL)
        {
          FAIL("lvalue-range failure\n");
          UtIO::cout() << "bv: " << onehotBV << ", l: " << onehotL << UtIO::endl;
          UtIO::cout() << "bv64(1<<" << i << ").value(" << j << "," << size
                    << ")" << UtIO::endl;
          break;
        }
      }
    }
  }

  // Second phase: use a larger bit vector, and compare the results
  // to what's expected.  Do this just 1 bit at a time for human sanity
  // BITVECTOR0v(oneHot100, 100); declared above...
  for (int i = 0; i < 100; ++i)
  {
    oneHot100.set(i, 1);
    for (int j = 0; j < 100; ++j)
    {
      if ((oneHot100.value(j, 1) == 1) != (j == i))
        FAIL("bv100 value mismatch 1\n");
    }
    if (i >= 32)
    {
      UInt64 val = oneHot100.llvalue(i - 31, 32);
      if (val != (1UL << 31))
        FAIL("bv100 llvalue mismatch i\n");
    }
    oneHot100.set(i, 0);
  }
#endif

  // comparison functions
  BITVECTOR1v(cmp, 64, 381u);
  if (cmp < 380u)
    FAIL("bv(381) < 380 ");
  if (cmp > 382u)
    FAIL("bv(381) < 382 ");
  if (cmp <= 380u)
    FAIL("bv(381) <= 380 ");
  if (cmp >= 382u)
    FAIL("bv(381) <= 382 ");

  // I can't seem to get this to compile as operators due to some
  // strange interaction with the boost operators.  So I have to
  // use explicit method calls
#ifdef USE_DYNAMIC
  // We are going to compare to 64-bit values.  For dynamic bit
  // vectors we can compare them against 32-bit values but
  // for templatized ones we have to match sizes
  BITVECTOR1v(d380, 32, 380u);
  BITVECTOR1v(d381, 32, 381u);
  BITVECTOR1v(d382, 32, 382u);
#else
  BITVECTOR1v(d380, 64, 380u);
  BITVECTOR1v(d381, 64, 381u);
  BITVECTOR1v(d382, 64, 382u);
#endif
#if 0
  if (cmp < d380)
    FAIL("bv381 < d380 ");
  if (cmp > d382)
    FAIL("bv381 > d382 ");
  if (cmp <= d380)
    FAIL("bv381 <= d380 ");
  if (cmp >= d382)
    FAIL("bv381 <= 382 ");

  if (cmp < d381)
    FAIL("bv381 < d381 ");
  if (cmp > d381)
    FAIL("bv381 > d381 ");
  if (! (cmp <= d381))
    FAIL("! bv381 <= d381 ");
  if (! (cmp >= d381))
    FAIL("! bv381 <= d381 ");

  if (! (d380 < cmp))
    FAIL("! d380 < bv381 ");
  if (! (d382 > cmp))
    FAIL("! d382 > bv381 ");
  if (! (d380 <= cmp))
    FAIL("! d380 <= bv381 ");
  if (! (d382 >= cmp))
    FAIL("! 382 <= bv381 ");
#else
  if (cmp.isLessThan(d380))
    FAIL("bv381.isLessThan(d380 ");
  if (cmp.isGreaterThan(d382))
    FAIL("bv381.isGreaterThan(d382 ");
#endif
  if (cmp.compare(d380) != 1)
    FAIL("compare(381,380)!=1)");
  if (cmp.compare(d381) != 0)
    FAIL("compare(381,381)!=0)");
  if (cmp.compare(d382) != -1)
    FAIL("compare(381,381)!=-1)");

#if PERFTEST
  perftest();
#endif

  // Simple tests of divide
  BITVECTOR0v (num,127);
  num[126] = KUInt64(1);

  num /= KUInt64(2);
  if (! num[125] || num.count () != 1)
    FAIL ("divide by power of two fails\n");

  // BUG 407 - oob read/write of bitvectors..
#ifdef USE_DYNAMIC
  // Tepmlates give error:
  // .../bvtest.cxx:1116: error: no match 
  // for 'operator[]' in 'BitVector<_Nb>::operator[](short unsigned int) [with 
  // unsigned int _Nb = 127](120)[7]'

  num[120][7] |= 0xffffffffUL;     // Don't set excess bits above 127
  if (num.count () != 7)
    FAIL ("BUG407 relapse\n");
#endif

  // BUG 471 logical ! operator

  BITVECTOR0v (allzeros,64);
  if (!num)
    FAIL ("BUG471 relapse\n");

  if (!allzeros)
    ;
  else
    FAIL ("BUG471 relapse2\n");

#ifdef USE_DYNAMIC
  // Templates give:
  // .../bvtest.cxx:1143: error: `
  // integerMult' undeclared (first use this function)

  // integer multiplication
  
  BITVECTOR0v (multiplier, 64);
  BITVECTOR0v (multee, 64);
  
  multee.integerMult(multiplier);
  if (multee != 0) FAIL ("Multiply by 0 failed 1\n");

  multiplier = 10;
  multee.integerMult(multiplier);
  if (multee != 0) FAIL ("Multiply by 0 failed 2\n");

  multee = 8;
  multiplier = 0;
  multee.integerMult(multiplier);
  if (multee != 0) FAIL ("Multiply by 0 failed 3\n");

  multee = 10;
  multiplier = 1;
  multee.integerMult(multiplier);
  if (multee != 10) FAIL ("Multiply by 1 failed\n");
  
  // power of 2 multiply
  multee.reset();
  multee.set(62);
  multiplier = 2;
  multee.integerMult(multiplier);
  if ((multee.value() != 0) ||
      (multee.count() != 1) ||
      ! multee.test(63))
    FAIL ("Simple integer 1 multiplication failed\n");

  multee.reset();
  multee = 25;
  multiplier = 20;
  multee.integerMult(multiplier);
  if (multee != 500) FAIL ("Simple integer 2 multiplication failed\n");


  // integer division and mod
  multee.reset();
  multee = 25;
  multiplier = 20;
  BITVECTOR0v (rem, 64);
  multiplier.integerDiv(multee, &rem);
  if ((multiplier != 0) || (rem != 20)) FAIL ("Overflow integer div failed\n");
  
  multee = 25;
  multiplier = 20;
  multee.integerDiv(multiplier, &rem);
  if ((multee != 1) || (rem != 5)) FAIL ("Simple Integer div failed\n");
  
  multee = 5;
  multiplier = 1;
  multee.integerDiv(multiplier, &rem);
  if ((rem != 0) || (multee != 5)) FAIL ("Divide by 1 failed\n");

  // set range
  multee.reset();
  multee.setRange(31, 3, 1);
  if (! (multee.test(31) && multee.test(32) && multee.test(33)))
    FAIL ("setRange set failed\n");
  
  multee.setRange(31, 3, 0);
  if (multee.any()) FAIL ("setRange clear failed\n");
#endif

#ifdef USE_DYNAMIC
  // Templates give error:
  // .../bvtest.cxx:1217: error: ambiguous 
  // overload for 'operator!=' in 'BitVector<_Nb>::partsel(short unsigned int, 
  // short unsigned int) const [with unsigned int _Nb = 36](0, 32) != 286331153'

  // BUG1873 - dynbitvector partsel problems?

  const UInt32 B1873_DATA[] = {0x11111111, 0x22222222, 0x33};
  BITVECTOR2v (DATA_A, 72, B1873_DATA, 3);
  BITVECTOR0v (RES_A, 36);

  RES_A = DATA_A.partsel (0,36);
  // can't test more than 32 bits at a time...
  if (RES_A.partsel (0,32) != 0x11111111 || RES_A.partsel (32,4) != 0x2)
    FAIL ("partsel(0,36) assignment fails\n");

  RES_A = DATA_A.partsel (36,36);
  if (RES_A.partsel (0,32) != 0x32222222 || RES_A.partsel (32,4) != 0x3)
    FAIL ("partsel(36,36) assignment fails\n");
#endif

  bus = 100;
  if (bus > KUInt64(0x100000000)) {
    FAIL("bus > UInt64 failed\n");
  }
  if (KUInt64(0x100000000) < bus ) {
    FAIL("UInt64 < bus failed\n");
  }
  if (bus >= KUInt64(0x100000000)) {
    FAIL("bus > UInt64 failed\n");
  }
  if (KUInt64(0x100000000) <= bus ) {
    FAIL("UInt64 < bus failed\n");
  }

#ifdef USE_DYNAMIC
  // test three distinct-sized DBVs in a signed multiply
  DynBitVector a(71);
  DynBitVector b(135);
  DynBitVector c(200);
  a = 5;  a.negate();           // a = -5
  b = 8;  b.negate();           // b = -8
  c.signedMultiply(a, b);       // c = 40
  c -= 40;                      // c = 0
  if (c.any()) {
    FAIL("DBV::signedMultiply failure\n");
  }

  b.negate();                   // b = 8
  c.signedMultiply(a, b);       // c = -40
  c += 40;                      // c = 0;
  if (c.any()) {
    FAIL("DBV::signedMultiply failure\n");
  }

  signed_mult(35, 50, 70);
  signed_mult(50, 35, 70);
  signed_mult(35, 70, 70);
  signed_mult(70, 35, 70);
  signed_mult(35, 70, 170);
  signed_mult(70, 35, 170);

  // here is a cases that are explicitly optimized by
  // DynBitVector::signedMultiply
  signed_mult(96, 96, 96);      // can always use raw unsigned multiply
  signed_mult(96, 96, 192);     // can always use raw unsigned multiply

  signed_compare(35, 50);
  signed_compare(50, 35);
  signed_compare(70, 35);
  signed_compare(35, 70);
  signed_compare(12, 12);
  signed_compare(32, 32);
  signed_compare(32, 12);
  signed_compare(12, 32);
  signed_compare(5, 7);
  signed_compare(7, 5);
#endif

  return errors;
} // int Statics::bvtest

#ifdef USE_TEMPLATES
// For now, putting all tests under USE_TEMPLATES
static SBITVECTOR2v(s_two_exp_64, 65, val, 3u);
static SBITVECTOR1v(s_two_exp_63, 65, KSInt64(0x8000000000000000));
static SBITVECTOR2v(s_allones, 128, ones, 4u);
static SBITVECTOR2v(s_allzeros,64,val,2u);

static SBITVECTOR0v(s_bus, 128);
static SBITVECTOR0v(s_alu, 65);
static SBITVECTOR0v(s_mac, 65);

#ifdef USE_TEMPLATES
void Statics::s_concats (void)
{
  static UInt32 raw[]={ 0x01234567, 0x89abcdef,0xfedcba98,0x76543210};
  SBITVECTOR2v (updown, 128, raw,4);

  SBITVECTOR0v (dest,128);
  SBITVECTOR0v (realdest,128);

  realdest = *reinterpret_cast <const SBitVector<128>*>(((
     ((dest.lpartsel (0,40)=updown.partsel (60,40))  /* edcba988, 0f*/
      .lpartsel_advance (60)=updown.partsel (64,60)) /* fedcba98, 6543210*/
     .lpartsel_advance (28)=updown.partsel (60,28))  /* edcba98 */
    .base<const SBitVector<128> > (100)));

  if (realdest.partsel (0,64).llvalue () != KSInt64(0xdcba980fedcba988)
      || realdest.partsel (64,64).llvalue () != KSInt64(0xdcba9886543210fe))
    FAIL ("big chunk concat fail\n");

    // This is a good place to test BVref comparisons
  if (updown.partsel (0,128) != updown)
    FAIL ("A[lo:hi] == A fails\n");

  if (updown.partsel (12,72) == updown.partsel (12,68))
    FAIL ("differnet sizes compare wrong\n");
    
  SBITVECTOR0v (scratch,128);
  SBITVECTOR0v (itch,128);
  scratch.lpartsel (60,68) = updown;
  itch.lpartsel (30,68) = updown;

  if (scratch.partsel (60,68) != itch.partsel (30,68))
    FAIL ("unaligned compare fails\n");

  if (scratch.partsel (60,68) != itch.partsel (30,72))
    FAIL ("sign extend compare fails\n");

  // Now try at least one that SHOULD be different
  if (scratch.partsel (1,120) == itch.partsel (1,120))
    FAIL ("compare succeeds when different values. \n");

  SBITVECTOR0v (A,64);
  SBITVECTOR0v (B,64);
  SBITVECTOR0v (C,64);

  dest=0;
  realdest=s_allones;

  A=KSInt64 (0xaaaaaaaaaaaaaaaa);
  B=KSInt64 (0xbbbbbbbbbbbbbbbb);
  C=KSInt64 (0xcccccccccccccccc);

  realdest = *reinterpret_cast<const SBitVector<128> *>((((dest.lpartsel (0,20)=
                A.partsel (4,20)).lpartsel_advance (24)=
               B.partsel (8,24)).lpartsel_advance (64)=
              C.partsel (0,64)).base<const SBitVector<128> > (44));

  if (realdest != dest)
    FAIL ("Assign from base()-ed BV\n");
  if (realdest.partsel (0,20).value () != 0xaaaaa
      || realdest.partsel (20,24).value () != 0xbbbbbb
      || realdest.partsel (44,64).llvalue () != KSInt64 (0xcccccccccccccccc)
      || realdest.partsel (108,20).value () != 0)
    FAIL ("concat broken\n");
}

void Statics::s_fieldarith (void)
{
  alu = allones;
  s_alu = s_allones;
#ifdef USE_DYNAMIC
  s_alu.lpartsel (1,63)+= 1;
  if (s_alu.partsel (0,64).llvalue () != 1
      || s_alu.partsel (64,1).llvalue () != 1)
    FAIL ("partsel+=uint FAIL\n");

  s_alu.lpartsel (1,63) -= 1;
  if (s_alu.count () != s_alu.size ())
    FAIL ("partsel-=uint FAIL\n");

  // Bug observed in BV op= SInt64
  s_alu = 0;
  s_alu.lpartsel (1,63) += KSInt64 (0x100000000);
  if (s_alu.partsel (0,64).llvalue () != KUInt64 (0x200000000))
    FAIL ("BV::operator+=(U64) broken\n");

  s_alu.lpartsel (1,63) -= KSInt64 (0x100000000);
  if (s_alu.partsel (0,64).llvalue () != 0)
    FAIL ("BV op= symmetry\n");

#endif
  // exhaustive test
  s_alu = 0;
  for( SInt32 i=0;i<65535;++i)
    {
      s_alu.lpartsel (27,16) |=i; // Set bits
      s_alu.lpartsel (27,16) &=~i; // Clear them
      if (s_alu.partsel (26,18).any ())
        {
          FAIL ("partsel and/or\n");
          break;
        }
    }

  s_alu = 0;
  for(SInt32 i=0;i<65535;++i)
    {
      s_alu.lpartsel (27,16) = i;
      if (s_alu.partsel (26,18).value () != ((i<<1)))
        {
          FAIL ("parsel deposit leaks\n");
          break;
        }
    }

  s_alu = 0;
  for(int i=0;i<=65535;++i)
    {
      s_alu.lpartsel (27,16) += (int) 1;
      if (s_alu.partsel (26,18).value () != ((i+1)%65536)<<1)
        {
          FAIL ("counter leaks\n");
          break;
        }
    }

  // Test xor bug
  s_bus = s_allones;

  s_bus.lpartsel(1,64) = SBitVector<128>(s_allones) ^ s_bus.partsel (0,128);

  if (s_bus.count () != 64 || s_bus.llvalue (0,64) != 1 || s_bus.llvalue (64,64) != -2)
    FAIL ("xor partsel bug\n");

}
#endif

void Statics::s_tryfunny (void)
{
#ifdef USE_TEMPLATES
  // We use BVref::base() to return the WHOLE bitvector after an
  // assignment to the partsel.  Concat's can only be unsigned, so we construct
  // a normal bitvector by pieces and then assign it to a real signed bitvector.
  s_bus = *(((BITVECTOR1(128, 65u)).lpartsel (64,64)
         = KUInt64(0xffffffffffffffff)).base<const SBITVECTOR0 (128) >(64));

  if (s_bus.llvalue () != 65 || not (s_bus.llvalue (64,64) == KSInt64(0xFFFFFFFFFFFFFFFF)))
    FAIL ("Lvalue temp hack fails (65 != " 
          << s_bus.llvalue ()
          << "), (-1 != " << s_bus.llvalue (64,64) << "\n");

  s_bus = s_bus << 32; // 0xffffffff000000000000004100000000
  s_bus = SBitVector<65>(s_bus) << 32; //0xffffffffffffffff0000000000000000
  s_bus = (SBitVector<65>(s_bus).shiftRightArith (1)); //0xfffffffffffffff8000000000000000
  if (s_bus.partsel(0,64).llvalue() != KSInt64(0x8000000000000000) 
      ||  s_bus.partsel (64,64).llvalue() != KSInt64(0xffffffffffffffff))
    FAIL ("signed shift fails\n");

  SBitVector<10> two(2);
  // No longer supporting BV >> BV
  s_bus  >>= two.value ();
  if (s_bus.partsel(0,64).llvalue() != KSInt64(0xe000000000000000))
    FAIL("unsigned shift of SBV fails\n");

  // Test bug comparing a SBitVector and a S64
  s_bus.lpartsel (64,64) = 0;
  s_bus.lpartsel (0,64) = -1;

  if (s_bus == KSInt64 (0xffffffffffffffff))
    FAIL ("zero-extend signed compare bug\n");
  
  s_bus = -1;
  if (s_bus != KSInt64 (0xffffffffffffffff))
    FAIL ("sign-extend SBitVector compare bug\n");

#endif
}

// Test sign-extended bitvectors.
void Statics::signextend (void)
{

  SBITVECTOR1v(words, 128, s_allones); // Initialized from similar object.
  BITVECTOR1v(u_word, 128, allones);
  SBITVECTOR0v(s_word, 130);

  if (s_word.count() != 0)
    FAIL ("SBV not initialized to zero but to: " << s_word << "\n");
  
  s_word = u_word; // no sign extention
  if (s_word.count() != 128)
    FAIL ("assignment of BV to SBV fails\n");

  s_word = s_allones; // sign extension
  if (s_word.count() != 130)
    FAIL ("assignment of SBV to SBV fails\n");

  s_word = 0;
  s_word += -8;
  s_word += -8;
  if (s_word.count() != 126 || s_word.llvalue() != -16 || s_word.value() != -16)
    FAIL ("assignment BV<130> += SInt32 fails \n");

  s_word -= -10;
  if ( s_word.llvalue(0,64) != -6 || s_word.value(0,32) != -6)
    FAIL ("assignment BV<130> -= SInt32 fails \n");

  s_word -= KSInt64(0xfffffffffffffffe); // -= -2
  if ( s_word.llvalue(0,64) != -4 || s_word.value(0,32) != -4)
    FAIL ("assignment BV<130> -= SInt64 fails \n");

  s_word += KSInt64(0xfffffffffffffffc); // += -4
  if ( s_word.llvalue(0,64) != -8 || s_word.value(0,32) != -8)
    FAIL ("assignment BV<130> += SInt64 fails \n");


  BITVECTOR1v(u_words, 128, allones);
  SBITVECTOR1v(s_words, 130, u_words); // no sign ext
  if (s_words.count() != 128)
    FAIL ("copy constructor SBV(BV<k>&) fails \n");

  SBITVECTOR1v(ss_words, 130, s_allones); // sign ext
  if (ss_words.count() != 130)
    FAIL ("copy constructor SBV(SBV<k>&) fails \n");

  if (words.count () != 128) 
    FAIL ("copy constructor failed\n");

  SBITVECTOR1v(words1, 64, s_allones); // Initialized from larger object
  if (words1.count () != 64) 
    FAIL ("copy constructor failed\n");
  
  // Signed version. lhs should be signed extended on assignment
  words1.lpartsel (12,32) = s_two_exp_64.partsel (64,1);
  if (words1.llvalue () != -1)
    FAIL ("sign extended copy fails\n");

  words1.lpartsel (1,30) = s_allzeros; // Assign bigger field to smalle
  if (KSInt64 (0xffffffff80000001) != words1.llvalue ())
    FAIL ("spilled over assignment\n");

  words = -1; // all ones
  if (words.count () != 128)
    FAIL ("sign extend fails. expected count:128, got:" << words.count() << "\n");

  SBITVECTOR1v(words2, 128, SInt64(-1));
  if (words2.count () != 128)
    FAIL ("sign extend fails. expected count:128, got:" << words2.count() << "\n");
  words2 = 0;
  words2.lpartsel(5,88) = SInt32(-2);
  if  (words2.count () != 87)
    FAIL ("sign extend fails in BVref assignment of SInt32.\n");
 
  words2 = 0;
  words2.lpartsel(5,88) = SInt64(-4);
  if  (words2.count () != 86)
    FAIL ("sign extend fails in BVref assignment of SInt64 .\n");

  // Test BVref operator==(SInt64)
  if (!(words2.partsel(5,88) == SInt64(-4)))
    FAIL ("BVref comparision fails\n");

  // Test BVref operator==(UInt64)
  BITVECTOR1v(uwords2, 128, UInt64(-1));
  if (!(uwords2.partsel(4,88) == KUInt64 (0x0fffffffffffffff)))
    FAIL ("BVref comparision fails\n"); 

  uwords2 = UInt64(0);
  uwords2.lpartsel(4,88) += UInt32(5);
  if (uwords2.count() != 2)
     FAIL ("BVref += UInt32 fails\n");
    
  words2 = 0;
  words2.lpartsel(5,88) += SInt32(-1);
  if (words2.count() != 88)
    FAIL ("BVref += SInt32 fails\n");

    
   /* Need to fix BVref::operator=(T32/T64) when BVref size > 95
  BITVECTOR1v(uwords2, 128, UInt64(-1));
  uwords2.lpartsel(5,118) = UInt32(0);
  if (uwords2.count () != 5)
    FAIL ("...\n");

  words2 = 0;
  words2.lpartsel(5,118) = SInt64(-4);
  if  (words2.count () != 116)
     FAIL ("sign extend fails in BVref assignment of SInt64 .\n");
  */
  
}

void Statics::s_do_flip (void)
{
  SBITVECTOR0v(value, 128);

  if (!value)
    ;
  else
    FAIL ("do_flip of single bit fails\n");

  value.lpartsel (1,10) = ((UInt64) 0x3fful);   // set a complete field
  value.lpartsel (0,11).flip ();
  if (value.partsel (0,11).value () != 1)
    FAIL ("do_flip of solid field\n");

  value.lpartsel (1,10).flip ();
  if (value.count () != 11)
    FAIL ("do_flip assignment fails\n");

  // signed version
  value.lpartsel (1,10) = ((SInt64) 0x3fful);   // set a complete field
  if (value != SInt64 (0x7ff))
    FAIL ("signed partsel assignment\n");
}

void Statics::s_uneg (void)
{
  SBITVECTOR0v(value, 128);
  
  value = -value;
  if (value.any ()) FAIL ("-0 != 0\n");

  value = 1;
  value = -value;
  if (value.count () != 128) FAIL ("-(1) not propagated\n");

  SBITVECTOR1v(v, 127, value);

  if (v.count () != 127)
     FAIL ("count() counts  extra bit\n");

  if (!v.test(127))
     FAIL ("copy constructor doesnt sets extra bit\n");

  

  v = -v;
  if (v.count () != 1 || v.value () != 1)
    FAIL ("unary negate bugs\n");

  v = -v;      // all ones
  v = v & -2;  // only 1st bit is cleared.
  v.flip();    // only 1st bit remains set.
  if (v.count () != 1 || v.value () != 1)
    FAIL ("SInt32 is not sign-extended in operator '&'\n");

  value = 10;
  value = -value;
  value = -value;
  if (value != SInt64(10)) FAIL ("double negation of 10 failed\n");
  
#ifdef USE_DYNAMIC
  value = 1;
  value.negate();
  if (value.count () != 128) FAIL ("DynBV negate() failed\n");
#endif
  SBITVECTOR1v(small, 34, -1);
  SBITVECTOR1v(big, 134, 0);
  big = small;
  if (big.count() != 134)
    FAIL ("sign extension fails\n");

  BITVECTOR1v(g,64, UInt64(-1));
  g &= UInt32(7);
  if (g.count() != 3)
    FAIL (" BV<124> & UInt32  to sign extend fails\n");
  

}

#define S(n) v=(x<<n); if (v.llvalue() != ux<<n) FAIL(UtIO::hex << (ux << n) << "!=" << v.llvalue() << " while shifting\n")

void Statics::s_lsh (SInt64 i)
{
  SBITVECTOR0v(v, 64);
  SBITVECTOR1v(x, 64, i);
  SInt64 ux = i;
  S (0);
  S (1);
  S (2);
  S (3);
  S (4);
  S (5);
  S (6);
  S (7);
  S (8);
  S (9);
  S (10);
  S (11);
  S (12);
  S (13);
  S (14);
  S (15);
  S (16);
  S (17);
  S (18);
  S (19);
  S (20);
  S (21);
  S (22);
  S (23);
  S (24);
  S (25);
  S (26);
  S (27);
  S (28);
  S (29);
  S (30);
  S (31);
  S (32);
  S (33);
  S (34);
  S (35);
  S (36);
  S (37);
  S (38);
  S (39);
  S (40);
  S (41);
  S (42);
  S (43);
  S (44);
  S (45);
  S (46);
  S (47);
  S (48);
  S (49);
  S (50);
  S (51);
  S (52);
  S (53);
  S (54);
  S (55);
  S (56);
  S (57);
  S (58);
  S (59);
  S (60);
  S (61);
  S (62);
  S (63);
}
#undef S
#define S(n) v=(x.shiftRightArith(n)); if (v.llvalue() != ux>>n) FAIL(UtIO::hex << (ux << n) << "!=" << v.llvalue() << "\n")

void Statics::s_rsh (SInt64 i)
{

  SBITVECTOR1v(x, 64, i);
  SBITVECTOR0v(v, 64);

  SInt64 ux = i;
  S (0);
  S (1);
  S (2);
  S (3);
  S (4);
  S (5);
  S (6);
  S (7);
  S (8);
  S (9);
  S (10);
  S (11);
  S (12);
  S (13);
  S (14);
  S (15);
  S (16);
  S (17);
  S (18);
  S (19);
  S (20);
  S (21);
  S (22);
  S (23);
  S (24);
  S (25);
  S (26);
  S (27);
  S (28);
  S (29);
  S (30);
  S (31);
  S (32);
  S (33);
  S (34);
  S (35);
  S (36);
  S (37);
  S (38);
  S (39);
  S (40);
  S (41);
  S (42);
  S (43);
  S (44);
  S (45);
  S (46);
  S (47);
  S (48);
  S (49);
  S (50);
  S (51);
  S (52);
  S (53);
  S (54);
  S (55);
  S (56);
  S (57);
  S (58);
  S (59);
  S (60);
  S (61);
  S (62);
  S (63);
}


#endif


int sbvtest (void) {
  Statics statics;
  return statics.sbvtest();
}

#ifdef USE_DYNAMIC
static struct {int msb; int lsb;} sSequences[] = {
  {3, 0},   // 0
  {6, 4},   // 7
  {11, 7},  // 0
  {15, 12}, // f
  {19, 16}, // 0
  {20, 20}, // 1
  {27, 21}, // 0
  {29, 28}, // 3
  {31, 30}  // 0
};

static void sTestSequence(const DynBitVector& dbv, bool onesAndZeros,
                          bool expectState, int init)
{
  DynBitVector::RangeLoop loop(dbv, onesAndZeros);
  UInt32 inc = onesAndZeros ? 1 : 2;
  for (UInt32 i = init; i < 9; i += inc, ++loop) {
    CHECK(!loop.atEnd());
    CHECK(loop.getCurrentState() == expectState);
    if (onesAndZeros) {
      expectState = !expectState;
    }
    CHECK(*loop == ConstantRange(sSequences[i].msb, sSequences[i].lsb));
  }      
  CHECK(loop.atEnd());
}

void Statics::testDBVRangeLoop() {
  DynBitVector dbv(32);
  dbv = 0x3010f070;
  sTestSequence(dbv, true, false, 0);
  sTestSequence(dbv, false, true, 1);
  dbv.flip();
  sTestSequence(dbv, true, true, 0);
  sTestSequence(dbv, false, true, 0);
} // void Statics::testDBVRangeLoop

// DynBitVector has an aggressive setRange method implementation that
// must be tested aggressively, using a very conservative test.
void Statics::test_setrange(UInt32 bvsize, UInt32 numBits) {
  DynBitVector bv(bvsize);

  UInt32 numPositions = bvsize - numBits + 1;

  // Writes 1s into every position, and then undo that.
  for (UInt32 pos = 0; pos < numPositions; ++pos) {
    bv.setRange(pos, numBits, 1);
    for (UInt32 i = 0; i < numBits; ++i) {
      CHECK(bv.test(pos + i));
    }
    CHECK(bv.count() == numBits);

    bv.setRange(pos, numBits, 0);
    CHECK(!bv.any());
  }

  // Writes 0s into every position, and then undo that.
  bv.setRange(0, bvsize, 1);    // set all the bits
  CHECK(bv.count() == bvsize);
  for (UInt32 pos = 0; pos < numPositions; ++pos) {
    bv.setRange(pos, numBits, 0);
    for (UInt32 i = 0; i < numBits; ++i) {
      CHECK(!bv.test(pos + i));
    }
    CHECK(bv.count() == bvsize - numBits);
    bv.setRange(pos, numBits, 1);
    CHECK(bv.count() == bvsize);
  }
} // void Statics::test_setrange
#endif

int Statics::sbvtest (void)
{
#ifdef USE_TEMPLATES
  UtIO::cout() << "Testing " << STYLE << " signed bit vectors.." << UtIO::endl;
  
  SBITVECTOR1v(swords1, 128, allones);
  SBITVECTOR1v(swords2, 128, allones);

  swords1 = swords2 + swords2;
  if (swords1.partsel(0,64).llvalue() != KSInt64(0xfffffffffffffffe)
      || swords1.partsel(64,64).llvalue() != KSInt64(0xffffffffffffffff))
    FAIL("signed addition fails\n");
 
  swords1 = -swords1;
  if (2 != swords1.partsel(0,64).llvalue() )
    FAIL("signed negation fails\n");

  signextend ();
 
  s_uneg ();

  // Test for funny bitfield assignments
  s_tryfunny ();

  // test concatenations
  s_concats ();

  // Test partsel arithmetic
  s_fieldarith ();
 
  // test bit flipping
  s_do_flip ();

  // Test left shifts;
  s_lsh (1);
  s_lsh (KSInt64(0xFFFFFFFFFFFFFFFE));
  s_lsh (255);

  // And right shifts too
  s_rsh (1);
  s_rsh (-1);
  s_rsh (KSInt64(0x8000000000000000));
  s_rsh (KSInt64(255)<<56);
  
  // Bug175
  SBITVECTOR1v(m1, 128, s_allones);

  m1.lpartsel (31,4) = ((SInt64) 0);
  if (m1.partsel (0,32).value () != SInt32(0x7FFFFFFF)
      || m1.partsel (32,32).value () != (SInt32(0xFFFFFFF8)))
    FAIL ("BUG175!\n");

# if pfGCC3
  if (sizeof(s_two_exp_63) != 12
      || sizeof(s_allones) != 16)
    FAIL ("bitvector sizeof wrong\n");
# endif
# if pfGCC2
  if (sizeof(s_two_exp_63) != 16
      || sizeof(s_allones) != 20)
    FAIL ("bitvector sizeof wrong\n");
# endif
  
  if (s_two_exp_63 != s_two_exp_63)
    FAIL ("initialization or equality fails" << two_exp_63 << "\n");

  s_alu = s_two_exp_63;

  if (s_alu[0].value () || !s_alu[63].value ())
    FAIL ("one bit test fails\n");

  if (s_alu != s_two_exp_63)
    FAIL ("assignment fails " << alu << "\n");

  if (s_two_exp_64 != (s_two_exp_63 + s_two_exp_63))
    FAIL ("add fails == \n");


  if (s_two_exp_64 != (s_two_exp_63 << 1))
    FAIL ("shift fails\n");

  // Testing (SBitVector op Int) and (Int op SBitVector)
  if( s_allzeros != SInt32 (0))
     FAIL (" 'SBitVector op SInt32' fails\n");
  if(SInt32 (0) != s_allzeros)
     FAIL ("'SInt32 op SBitVector' fails\n");

  // Note that boost::operator produces complaints if we compare
  // BITVECTOR0(128> to BITVECTOR0(65>
  
  s_bus = s_two_exp_64;
  if (s_bus != SBITVECTOR1(128, s_two_exp_64))
    FAIL ("BitV<128> != BitV<65> fails\n");
    
  s_bus <<= 1;                    // 2**65
  s_alu = s_bus;                  // Should result in zero after truncation

  if (0 != s_alu.value ())
    FAIL ("truncation fails expect zero, got " << s_alu << "\n");

  s_mac = s_alu + SBITVECTOR1(65, KSInt64 (1));
  if (s_mac <= s_alu)
    FAIL ("signed compare fails " << s_mac << "\n");

  if (s_mac.none ())
    FAIL ("check for zeros fails\n");

  if (s_mac.count () != 1)
    FAIL ("population count fails count(" << s_mac << ")=" << s_mac.count () << "\n");

  // Now some fancier stuff...

  s_mac[0] = false;               // Turn off only set bit
  if (s_mac.value () != (0))
    FAIL ("clearing 1 bit fails\n");

  s_mac.lpartsel (1,2) = ((SInt64) 3l);          // Should set both bits

  if (s_mac.value () != (6))
    FAIL ("setting a range of bits fails, expect 6, " << mac << "\n");

  if (s_mac.partsel (1,2).value () != 3)
    FAIL ("reading a range of bits fails\n");

  
  s_alu = s_allones;                // Only copy 65 bits
  if (s_alu.partsel (64,64).llvalue () != 1)
    FAIL ("bitvector assign writes too much\n");
  
  // Stuff that probably won't compile for dynamic bitvectors..
  s_bus = s_allones;
  s_mac = 0; 
    
  s_bus.lpartsel (16,64) &= s_mac.partsel (1,64);
  if (s_bus.partsel (0,64).llvalue () != 0xffff
      && s_bus.partsel (64,64).llvalue () != KSInt64 (0xffff000000000000))
    FAIL ("partsel &= bug\n");

  s_bus.lpartsel (16,64) |= s_allones;
  if (s_bus.count () != s_bus.size ())
    FAIL ("partsel|=bitvector fails\n");

  s_bus = 0;
  s_bus.lpartsel (0,33) |= s_allones.partsel (65,33);
  s_bus.lpartsel (33, 66) |= s_allones.partsel (1,66);
  s_bus.lpartsel (99, 29) |= s_allones.partsel (31,29);

  if (s_bus.count () != 128)
    FAIL ("lpartsel |= partsel fails\n");

  s_bus = 0;
  s_bus.lpartsel (1,127) = s_alu & s_allones.partsel (0,127);
  if (s_bus.partsel (0,64).llvalue () != KSInt64(0xfffffffffffffffe)
      || s_bus.partsel (64,64).llvalue () != -1)
    FAIL ("lpartsel = bv & partsel fail \n");

  // Test op= SInt64
  s_bus = 0;

  SInt64 v = KSInt64(0x100000000);
  s_bus += v;
  if (s_bus != v)
    FAIL ("+= 64 bit fails\n");

  s_bus += KSInt64(1);
  s_bus &= KSInt64 (0x1FFFFFFFF);
  if (s_bus != KSInt64 (0x100000001))
    FAIL ("&= 64 bit fails\n");

  s_bus &= ~KSInt64(0x100000001);
  s_bus |= KSInt64 (0xFFFFFFFF00000000);
  s_bus ^= KSInt64 (0x5555555555555555);
  if (s_bus.partsel(0,64) != KSInt64 (0xaaaaaaaa55555555) ||
      s_bus.partsel(64,64) != KSInt64 (0xffffffffffffffff))
    FAIL ("^= 64 bit fails\n");

  s_bus &= 7;
  if (s_bus.count() != 2)
    FAIL (" SBV<128> &= 0x7 fails to sign extend\n"); // 0x5

  s_bus |= -4;
  if (s_bus.partsel(64,64) != KSInt64(0xffffffffffffffff) ||
      s_bus.partsel(0,64) != KSInt64 (0xfffffffffffffffd))
    FAIL (" SBV<128> |= -4 fails to sign extend\n");
    
  s_bus &= 7;

  s_bus |= SInt64(-4);
  if (s_bus.partsel(64,64) != KSInt64(0xffffffffffffffff))
    FAIL (" SBV<128> |= -4 fails to sign extend\n"); 

    // BUG 1104 64 bit +/- not tested
  SBITVECTOR0v (s_adc, 128);
 
  s_adc = KSInt64 (0x8000000000000000);
  s_adc += KSInt64 (0x8000000000000000); // Check that this carries

  if(s_adc.count () != 64 || !s_adc.test (64))
    FAIL ("carry add fails!\n");

  s_adc -= KSInt64 (0x4000000000000000); // won't propagate
  if (s_adc.llvalue () != KSInt64 (0xC000000000000000)
      || s_adc.lpartsel (64,64) != KSInt64(0xfffffffffffffffe))
    FAIL ("borrow subtract fails!\n");

  SBITVECTOR0v(s_alu65, 65);
  s_alu65  = KSInt64 (0x8000000080000000);
  s_alu65 += KSInt64 (0x8000000080000000); // Carry up
  if (s_alu65.llvalue() != KUInt64(0x0000000100000000)
      || not s_alu65.test(64))
    FAIL ("Carry propagation fails!\n");

  // Copy some aligned bits
  s_alu = s_mac;
  if (s_alu != s_mac)
    FAIL ("signed assignment fails\n");

  s_bus ^= s_bus;                   // clear all bits
  if (s_bus.llvalue () != (0))
    FAIL ("XOR failed " << bus << "\n");

  // Set a range
  s_bus.lpartsel (8,56) = s_allones.partsel (8,56);

  if (s_bus.partsel (8,8).value () != 255 ||
      s_bus.partsel (16,16).value () != 65535 ||
      s_bus.partsel (32,32).value () != SInt32(0xFFFFFFFF))
    FAIL ("aligned range assignment fails - expect 56 1'b " << bus << "\n");

  // Add unaligned moves and compares...


  s_bus -= s_bus;                   // zero
  if (s_bus.llvalue () != (0))
    FAIL ("subtract failed " << bus << "\n");

  if (s_bus.partsel (8,32).any() || !s_allones.partsel (34,62).any())
    FAIL ("any() on reference fails\n");

  if (s_bus.partsel (8,120).count() != 0 || s_allones.partsel (18,34).count() != 34)
    FAIL ("count() fails\n");

  s_bus.lpartsel (8,32) = s_allones.partsel (0,32);
  s_bus.lpartsel (0,8) = s_allones.partsel (8,8);
  s_bus.lpartsel (40,88) = s_allones; // A truncating assignment

  if (s_bus != s_allones)
    FAIL ("random field moves fails, expect 128'b1 " << bus << "\n");

  // Finally test for overlapping assignments.

  s_bus.lpartsel (8,8) = KSInt64(0);
  s_bus.lpartsel (0,32) = s_bus.partsel (8,32);
  if (s_bus.partsel (0,32).value () != SInt32(0xffffff00))
    FAIL ("overlap wrong\n");

  s_bus.lpartsel (8,32) = s_bus.partsel (0,32);
  if (s_bus.partsel (0,32).value () != SInt32(0xffff0000))
    FAIL ("overlap two wrong\n");

  SBITVECTOR0v(mbus,128);
  mbus = KSInt64(0); // clear all bits
  mbus.lpartsel(0,8) = KSInt64(-1); // set 8 bits
  if (mbus.lpartsel(0,32).value() != 0x000000ff)
    FAIL ("sign assignment on partsel fails\n");

  mbus.lpartsel(16,16) = mbus.partsel(0,8); // size mismatch
  if ( mbus.lpartsel(0,32).value() != SInt32(0xffff00ff))
    FAIL ("sign extention on partial BVref assignment fails! expected 0xffff00ff got " << mbus << " \n");

  // See if multiple references from RHS cause trouble
  static UInt32 checkvec[] = {0x76543210, 0xfedcba98, 0x01234567, 0x89abcdef};

  SBITVECTOR2v(checkval, 128, checkvec, 4);

  s_bus.lpartsel (0,16) = checkval.partsel(0,16);
  s_bus.lpartsel (16,16) = checkval.partsel(0,16);
  s_bus.lpartsel (32,16) = checkval.partsel(0,16);
  s_bus.lpartsel (48,16) = checkval.partsel(0,16);
  s_bus.lpartsel (64,16) = checkval.partsel(0,16);
  s_bus.lpartsel (80,16) = checkval.partsel(0,16);

  if (s_bus.partsel(0,32).value () != SInt32(0x32103210)
      || s_bus.partsel (32,32).value () != SInt32(0x32103210)
      || s_bus.partsel (64,32).value () != SInt32(0x32103210)
      || s_bus.partsel (96,32).value () != SInt32(0xffffffff))
    FAIL ("Expected repeat 0x3210 pattern " << s_bus << "\n");


  // Test field-select and arithmetic
  s_bus.lpartsel(0,16) += KSInt64(1);

  if (s_bus.partsel(0,16).value () != 0x3211)
    FAIL ("expected 0x3211, got " << s_bus.partsel(0,16).value () << "\n");

  int count = s_bus.partsel(0,32).value () + 1;

  if (count != 0x32103212)
    FAIL ("expected 0x32103212  got " << UtIO::hex << count << "\n");

  // Test for commutativity?
  count = (1<<16) + s_bus.partsel(0,32).value ();
  if (count != 0x32113211)
    FAIL ("expected 0x32113211, got " << UtIO::hex << count << UtIO::dec << "\n");


  s_bus = 1;
  if (s_bus.count () != 1)
    FAIL ("population count fails got " << s_bus.count () << "\n");

  if (not s_bus.redxor ())
    FAIL ("reduction xor fails\n");

  s_bus *= SInt32 (2);
  if (s_bus.partsel(0,16).value () != 2)
    FAIL ("multiply fails\n");

  if ((s_bus << 1) != SBITVECTOR1(128, 4))
    FAIL ("left shift fails got " << s_bus << "\n");

  if ((s_bus << 128) != SBITVECTOR1(128, 0))
    FAIL ("big left shift fails " << (s_bus << 128) << "\n");

  s_bus *= SInt32 (0);
  if (s_bus.count () != 0)
    FAIL ("multiply by zero fails got " << s_bus << "\n");

  if (s_bus.redxor ())
    FAIL ("redxor(0) fails\n");

  // Some more reference tests
  s_bus = 0;
  s_bus.lpartsel (8,8) |= KSInt64(255); 

  if (s_bus.partsel(0,16).value () != (255<<8))
    FAIL ("reference::|= fails\n");

  s_bus.lpartsel (4,8) &= KSInt64(15); 
  if (s_bus.partsel (8,8).value () != (15<<4))
    FAIL ("reference::&= fails\n");

  s_bus.lpartsel (4,4) ^= KSInt64(15); 
  if (s_bus .partsel(0,32).value () != 0xf0f0)
    FAIL ("reference::^= fails\n");

  s_bus = SBITVECTOR1(128, s_mac) | (SBITVECTOR1(128, s_alu)<<65) | (SBITVECTOR1(128, 0)<<126);

  s_bus = (s_allones << 64) & (s_allones.shiftRightArith (64u));
  if (s_bus.count () != 64 || s_bus.partsel(0,64).llvalue() != 0) 
    FAIL ("shifting fails, expect 0, got " << s_bus.partsel(0,64).llvalue() << "\n");

  s_bus = SBITVECTOR1(128, ~KSInt64(0)) << 64;
#ifdef USE_DYNAMIC
  s_bus += SBITVECTOR1(64, ~KSInt64(0));

  if (s_bus.count () != 128)
    FAIL ("mixed mode arithmetic fails, expected all ones, got "<<s_bus<<"\n");
#endif

  // Test of bounded fields...

  if (s_allones.partsel(128,1).value ())
    FAIL ("out-or-range partsel fails\n");

  if (s_allones.partsel (15,64).redxor () ||
      not s_allones.partsel (15,65).redxor ())
    FAIL ("redxor partsel fails\n");

  // Zero length assignment ought to be ignored..
  //
  s_allones.lpartsel(128,2) = KUInt64(0);
  s_allones.lpartsel(0,0) = KUInt64(1); 

  if (s_allones.count() != 128)
    FAIL ("out-or-range assignment fails\n");

  SBITVECTOR1v(shiftAdd1, 66, KSInt64(0));
  shiftAdd1 = KSInt64(1);
  shiftAdd1 <<= KSInt64(1);
  shiftAdd1 += KSInt64(1);
  if (shiftAdd1.value() != 3)
    FAIL ("Shift-add of small vector failed\n");

  SBITVECTOR1v(shiftAdd2, 68, KSInt64(0xffffffffffffffff));
  shiftAdd2 += KSInt64(1);
  if (shiftAdd2.llvalue() != 0)
    FAIL ("Shift-add-carry of 68 bit vector failed thru 64\n");
  const UInt32* theArray = shiftAdd2.getUIntArray();
  if (theArray[2] != 0)
    FAIL ("Shift-add-carry of 68 bit vector failed in last word\n");

  BITVECTOR0v(good, 64);

  // Copy Constructor was wrong...
  {
    BITVECTOR0v(scoped, 64);
    scoped.lpartsel (16,32) = (UInt64) -1;

    good = scoped;
    // should call destructor for scoped here
  }
  if (good.llvalue () != KUInt64 (0x0000ffffffff0000))
    FAIL ("copy constructor problems?\n");

  // subtraction of small value
  SBITVECTOR1v(subTest, 64, KSInt64(0xFFFFFFFFFFFFFFFF));
  subTest -= KSInt64(1);
  if (subTest != SBITVECTOR1(64, KSInt64(0xFFFFFFFFFFFFFFFE)))
    FAIL ("subtraction of small constant\n");
  subTest -= KSInt64(4);
  for (size_t i = 0; i < 64; ++i)
  {
    // bits 0 and 2 should be false, others should be true
    if ((i == 0) || (i == 2))
    {
      if (subTest.test(i))
        FAIL("testing bit that should be off");
    }
    else if (!subTest.test(i))
      FAIL("testing bit that should be on");
  }

  subTest = 0x00000000FFFFFFFF - 5;

  subTest /= 4;
  SInt32 divCheck = (0xFFFFFFFF - 5)/4;
  if (subTest != SBITVECTOR1(64, divCheck))
    FAIL("divide-by-4 failed");
  SBitVector<64> flipCheck = ~subTest; // flip()
  for (size_t i = 0; i < flipCheck.size(); ++i)
    if (flipCheck.test(i) == subTest.test(i))
      FAIL("signed flipcheck");

  // Test shiftRightArith on a bvref
  SBitVector<64> sbv (KSInt64(0x8000000000000000));
  sbv.lpartsel (56,8).shiftRightArith (6);
  if (sbv.llvalue () != KSInt64 (0xfe00000000000000))
    FAIL ("SBVref asr fails\n");

  // unsigned field
  sbv.set (31);
  sbv.lpartsel (28, 28) /= 8u;
  if (sbv.llvalue () != KSInt64 (0xfe00000010000000))
    FAIL ("SBVref asr unsigned fails\n");

  // Now try zapping the whole thing
  sbv.lpartsel (0,63) /= KUInt64 (0x8000000000000000);
  if (sbv != KSInt64 (-1))
    FAIL ("SBVref sign prop fail\n");

  // reference subtraction.  yow!  here's one aligned to a byte
  subTest.lpartsel (24,8) -= KSInt64(3); 
  if (subTest.llvalue () != divCheck - (3<<24))
    FAIL("reference subtraction");

  // set the whole mother again
  subTest.set();
  if (subTest != SBITVECTOR1(64, KSInt64(0xFFFFFFFFFFFFFFFF)))
    FAIL("set whole 64-bit bv");

  // test fast method for 64-bit part selects.

  // First phase: use a 64-bit SBitVector, and an SInt64,
  // go through a large number of combinations, and test that they
  // get the same answers.
  for (int size = 3; size < 64; size += 17)
  {
    for (int i = 0; i < 64; ++i)
    {
      SInt64 onehotLL = KSInt64(1) << i;
      SBITVECTOR1v(onehotBV, 64, onehotLL);
      for (int j = 0; j < 64; ++j)
      {
        if ((i != 63) || ((j + size) <= 64))
        {
          SInt64 nBitsBV = onehotBV.llvalue(j, size);
          SInt64 mask = (KSInt64(1) << ((UInt64) size)) - 1;
          SInt64 nBitsLL = (onehotLL >> j) & mask;
          if (nBitsBV != nBitsLL)
          {
            FAIL("llvalue-range failure\n");
            UtIO::cout() << "sbv: " << onehotBV << ", ll: " << onehotLL << UtIO::endl;
            UtIO::cout() << "sbv64(1<<" << i << ").llvalue(" << j << "," << size
                      << ")" << UtIO::endl;
            break;
          }
        }
      }
    }
  }

  // Second phase: use a larger bit vector, and compare the results
  // to what's expected.  Do this just 1 bit at a time for human sanity
  SBITVECTOR0v(oneHot100, 100);
  for (int i = 0; i < 100; ++i)
  {
    oneHot100.set(i, 1);
    for (int j = 0; j < 100; ++j)
    {
      if ((oneHot100.llvalue(j, 1) == 1) != (j == i))
        FAIL("bv100 llvalue mismatch 1\n");
    }
    if (i >= 63)
    {
      SInt64 val = oneHot100.llvalue(i - 63, 64);
      if (val != (KSInt64(1) << 63))
        FAIL("bv100 llvalue mismatch i\n");
    }
    oneHot100.set(i, 0);
  }

  // test fast method for 32-bit part selects.

  // First phase: use a 32-bit SBitVector, and an SInt32,
  // go through a large number of combinations, and test that they
  // get the same answers.
  for (int size = 3; size < 32; size += 5)
  {
    for (int i = 0; i < 32; ++i)
    {
      SInt32 onehotL = 1UL << i;
      SBITVECTOR1v(onehotBV, 32, onehotL);
      for (int j = 0; j < 32; ++j)
      {
        if ((i != 31) || ((j + size) <= 32))
        {
          SInt32 nBitsBV = onehotBV.value(j, size);
          SInt32 mask = (1UL << size) - 1;
          SInt32 nBitsL = (onehotL >> j) & mask;
          if (nBitsBV != nBitsL)
          {
            FAIL("lvalue-range failure\n");
            UtIO::cout() << "bv: " << onehotBV << ", l: " << onehotL << UtIO::endl;
            UtIO::cout() << "bv64(1<<" << i << ").value(" << j << "," << size
                      << ")" << UtIO::endl;
            break;
          }
        }
      }
    }
  }

  // Second phase: use a larger bit vector, and compare the results
  // to what's expected.  Do this just 1 bit at a time for human sanity
  // SBITVECTOR0v(oneHot100, 100); declared above...
  for (int i = 0; i < 100; ++i)
  {   
    oneHot100.set(i, 1);
    for (int j = 0; j < 100; ++j)
    {
      if ((oneHot100.value(j, 1) == 1) != (j == i))
        FAIL("bv100 value mismatch 1\n");
    } 
    if (i >= 32)
    {
      SInt64 val = oneHot100.llvalue(i - 31, 32);
      if (val != (1UL << 31))
        FAIL("bv100 llvalue mismatch i\n");
    }
    oneHot100.set(i, 0);
  }

  // comparison functions
  SBITVECTOR1v(cmp, 64, 381);
  if (cmp < 380)
    FAIL("bv(381) < 380\n");
  if (cmp > 382)
    FAIL("bv(381) > 382\n");
  if (cmp <= 380)
    FAIL("bv(381) <= 380\n");
  if (cmp >= 382)
    FAIL("bv(381) >= 382\n");
  


  SBITVECTOR1v(d380, 64, 380);
  SBITVECTOR1v(d381, 64, 381);
  SBITVECTOR1v(d382, 64, 382);

  if (cmp.isLessThan(d380))
    FAIL("bv381.isLessThan(d380\n");
  if (cmp.isGreaterThan(d382))
    FAIL("bv381.isGreaterThan(d382\n");

  if (cmp.compare(d380) != 1)
    FAIL("compare(381,380)!=1)\n");
  if (cmp.compare(d381) != 0)
    FAIL("compare(381,381)!=0)\n");
  if (cmp.compare(d382) != -1)
    FAIL("compare(381,381)!=-1)\n");

  SBITVECTOR1v(scmp, 64, -381);
  if (scmp < -382)
    FAIL("bv(-381) < -382 \n");
  if (scmp > -380)
    FAIL("bv(-381) > -380 \n");
  if (scmp <= -382)
    FAIL("bv(-381) <= -382 \n");
  if (scmp >= -380)
    FAIL("bv(-381) >= -380 \n");
  if (scmp > 380)
    FAIL("bv(-381) > 380 \n");
  if (scmp > 382)
    FAIL("bv(-381) > 382 \n");
  if (380 < scmp)
     FAIL("380 < bv(-381) \n");
  if (d380 < scmp)
     FAIL("d380 < bv(-381) \n");
  if (d382 < scmp)
    FAIL("d382 < bv(-381) \n");

  SBITVECTOR1v(sd380, 64, -380);
  SBITVECTOR1v(sd381, 64, -381);
  SBITVECTOR1v(sd382, 64, -382);
  SBITVECTOR1v(sd1, 64, -1);

  if (scmp.isLessThan(sd382))
    FAIL("bv-381.isLessThan(sd382) \n");
  if (scmp.isGreaterThan(sd380))
    FAIL("bv-381.isGreaterThan(sd380) \n");

  if (scmp.compare(sd382) != 1)
    FAIL("compare(-381,-382)!=1)");
  if (scmp.compare(sd381) != 0)
    FAIL("compare(-381,-381)!=0)");
  if (scmp.compare(sd380) != -1)
    FAIL("compare(-381,-380)!=-1)");

  if (scmp.isGreaterThan(d380))
    FAIL("bv-381.isGreaterThan(d380) fails\n");
  
  if (scmp.isGreaterThan(d382))
    FAIL("bv-381.isGreaterThan(d382) fails\n");

  if (scmp.compare(d380) != -1)
    FAIL("compare(-381,380)!=-1) fails\n");

  if (scmp.compare(sd1) != -1)
    FAIL("compare(-381,-1)!=-1) fails\n");
  
  if (scmp.compare(d382) != -1)
    FAIL("compare(-381,382)!=-1) fails\n");

  if (scmp.compare(sd381) != 0)
    FAIL("compare(-381,381)!=0) fails\n");

  UInt32 val1[3] = {0, 0xffffffff, 0xffffffff};  // 96 bit
  UInt32 val2[3] = {0xffffffff, 0xffffffff, 0xffffffff}; // 96 bit
  SBITVECTOR2v(big, 96, val2, 3);
  SBITVECTOR2v(small, 96, val1, 3);

  if (big.compare(small) != 1)
    FAIL ("compare {-1,-1,-1} compare {-1,-1,0}\n");

  if (big.isLessThan(small))
    FAIL ("isLessThan fails for signed vector\n");

   // Simple tests of divide
  SBITVECTOR0v (num,127);
  num.lpartsel(64,63) = KSInt64(0x4000000000000000); // last bit 1. no sanitize
  num >>= 1;
  num <<= 1;

  if (num.count() != 1 || !num.test(127)) // chk using unguarded test()
    FAIL ("sanitize of SBV after left shift failed \n");

  num /= SInt32 (2);
  if ( num._M_w[3] != 0xe0000000 // check that excess bit is set, and 2 msbs of value set
       || num.count () != 2)    // (but NO others!)
    FAIL ("divide by power of two of signed vector fails\n");

  // Check for 64 bit divide also

  num /= SInt64 (2);
  if (num._M_w[3] != 0xf0000000 // only 3 msbs and excess bit set
      || num.partsel (0,124).any ())
    FAIL ("divide SBV by S64 fails\n");
  
  // Check for correct rounding
  num = -1;
  num /= 2;
  if (num != 0)
    FAIL ("signed divide not rounding off\n");

  num = -11;
  num /= 4;
  if (num != ((-11)/4))
    FAIL ("signed -11/4 fails\n");

  // BUG 471 logical ! operator

  SBITVECTOR0v (allzeros,64);
  if (!num)
    FAIL ("BUG471 relapse\n");

  if (!allzeros)
    ;
  else
    FAIL ("BUG471 relapse2\n");

// Test NxM unsigned multiplier
  {
    UInt64 normal_product;

    for(int i=0; i<1000; ++i)
    {
      UInt64 a,b;

      a = ((UInt64) (std::rand ()) << 32) | (UInt64) std::rand ();
      b = ((UInt64) (std::rand ()) << 32) | (UInt64) std::rand ();

      BITVECTOR0v (av, 64);
      av = a;
      BITVECTOR0v (bv, 64);
      bv = b;

      BITVECTOR0v (prod,64);

      normal_product = a * b;
      prod = av * bv;

      if (normal_product != prod.llvalue ())
        FAIL ("NxM multiply fails " << UtIO::hex << normal_product
              << " != "  << prod.llvalue () << UtIO::dec << "\n");
    }
  }

#endif
  return errors;
} // int Statics::sbvtest
