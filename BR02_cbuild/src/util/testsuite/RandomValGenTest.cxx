// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/RandomValGen.h"
#include "util/OSWrapper.h"

#include "util/UtString.h"
#include "util/UtStream.h"
#include <iostream>
#include <algorithm>
#include "util/UtVector.h"
#include "util/UtIOStream.h"
#include <iterator>


// please do not use "using namespace std"


static SInt32 gTestNum = 0;


void OutputResult(bool passed, SInt32 test)
{  // Make sure it is fast?

  if (passed)
  {
    std::cout << "PASS " << test << std::endl;
  }
  else
  {
    std::cout << "FAIL " << test << std::endl;
  }
}


void UInt32Test(void)
{
  UtVector<UInt32> v1(5000000); 
  UtVector<UInt32> v2(5000000);

 
  // Make sure generating the same sequence.
  std::generate(v1.begin(), v1.end(), RandomGenUInt32(12345));
  std::generate(v2.begin(), v2.end(), RandomGenUInt32(12345));
  OutputResult(v1 == v2, gTestNum++);

  // Make sure everything in the sequence is unique.
  UtVector<UInt32>::iterator new_end;
  new_end = std::unique(v2.begin(), v2.end());
  std::fill(new_end, v2.end(), 0);
  OutputResult(v1 == v2, gTestNum++);

  // Make sure generating a different sequence.
  std::generate(v1.begin(), v1.end(), RandomGenUInt32(12345));
  std::generate(v2.begin(), v2.end(), RandomGenUInt32(67890));
  OutputResult(v1 != v2, gTestNum++);

  // Make sure generating within a range.
  std::generate(v1.begin(), v1.end(), RandomGenUInt32(12345, 19, 2000));
  std::generate(v2.begin(), v2.end(), RandomGenUInt32(12345, 19, 2000));
  OutputResult(v1 == v2, gTestNum++);

  // In the range?
  UtVector<UInt32>::iterator max;
  UtVector<UInt32>::iterator min;
  std::generate(v1.begin(), v1.end(), RandomGenUInt32(121212, 137, 3030));
  max = std::max_element(v1.begin(), v1.end());
  min = std::min_element(v1.begin(), v1.end());
  OutputResult((*min >= 137) && (*max <= 3030), gTestNum++);

  // Print some samples to make sure they are always the same.
  std::generate(v1.begin(), v1.end(), RandomGenUInt32(12345));
  std::copy(v1.begin(), v1.begin() + 25, std::ostream_iterator<UInt32>(std::cout, "\n"));
  std::generate(v1.begin(), v1.end(), RandomGenUInt32(12345, 50, 100));
  std::copy(v1.begin(), v1.begin() + 25, std::ostream_iterator<UInt32>(std::cout, "\n"));
}


void SInt32Test(void)
{
  UtVector<SInt32> v1(5000000); 
  UtVector<SInt32> v2(5000000);

 
  // Make sure generating the same sequence.
  std::generate(v1.begin(), v1.end(), RandomGenSInt32(12345));
  std::generate(v2.begin(), v2.end(), RandomGenSInt32(12345));
  OutputResult(v1 == v2, gTestNum++);

  // Make sure everything in the sequence is unique.
  UtVector<SInt32>::iterator new_end;
  new_end = std::unique(v2.begin(), v2.end());
  std::fill(new_end, v2.end(), 0);
  OutputResult(v1 == v2, gTestNum++);

  // Make sure generating a different sequence.
  std::generate(v1.begin(), v1.end(), RandomGenSInt32(12345));
  std::generate(v2.begin(), v2.end(), RandomGenSInt32(67890));
  OutputResult(v1 != v2, gTestNum++);

  // Make sure generating within a range.
  std::generate(v1.begin(), v1.end(), RandomGenSInt32(12345, 19, 2000));
  std::generate(v2.begin(), v2.end(), RandomGenSInt32(12345, 19, 2000));
  OutputResult(v1 == v2, gTestNum++);

  // In the range?
  UtVector<SInt32>::iterator max;
  UtVector<SInt32>::iterator min;
  std::generate(v1.begin(), v1.end(), RandomGenSInt32(121212, -137, 3030));
  max = std::max_element(v1.begin(), v1.end());
  min = std::min_element(v1.begin(), v1.end());
  OutputResult((*min >= -137) && (*max <= 3030), gTestNum++);

  // Print some samples to make sure they are always the same.
  std::generate(v1.begin(), v1.end(), RandomGenSInt32(12345));
  std::copy(v1.begin(), v1.begin() + 25, std::ostream_iterator<SInt32>(std::cout, "\n"));
  std::generate(v1.begin(), v1.end(), RandomGenSInt32(12345, -50, 10));
  std::copy(v1.begin(), v1.begin() + 25, std::ostream_iterator<SInt32>(std::cout, "\n"));
}


void DoubleTest(void)
{
  UtVector<double> v1(5000000); 
  UtVector<double> v2(5000000);

 
  // Make sure generating the same sequence.
  std::generate(v1.begin(), v1.end(), RandomGenDouble(12345));
  std::generate(v2.begin(), v2.end(), RandomGenDouble(12345));
  OutputResult(v1 == v2, gTestNum++);

  // Make sure everything in the sequence is unique.
  UtVector<double>::iterator new_end;
  new_end = std::unique(v2.begin(), v2.end());
  std::fill(new_end, v2.end(), 0);
  OutputResult(v1 == v2, gTestNum++);

  // Make sure generating a different sequence.
  std::generate(v1.begin(), v1.end(), RandomGenDouble(12345));
  std::generate(v2.begin(), v2.end(), RandomGenDouble(67890));
  OutputResult(v1 != v2, gTestNum++);

  // Make sure generating within a range.
  std::generate(v1.begin(), v1.end(), RandomGenDouble(12345, 19.0, 2000.0));
  std::generate(v2.begin(), v2.end(), RandomGenDouble(12345, 19.0, 2000.0));
  OutputResult(v1 == v2, gTestNum++);

  // In the range?
  UtVector<double>::iterator max;
  UtVector<double>::iterator min;
  std::generate(v1.begin(), v1.end(), RandomGenDouble(121212, -137.5, 3030.7));
  max = std::max_element(v1.begin(), v1.end());
  min = std::min_element(v1.begin(), v1.end());
  OutputResult((*min >= -137.5) && (*max <= 3030.7), gTestNum++);

  // Print some samples to make sure they are always the same.
  std::generate(v1.begin(), v1.end(), RandomGenDouble(12345));
  std::copy(v1.begin(), v1.begin() + 25, std::ostream_iterator<double>(std::cout, "\n"));
  std::generate(v1.begin(), v1.end(), RandomGenDouble(12345, -50.1, 10.0));
  std::copy(v1.begin(), v1.begin() + 25, std::ostream_iterator<double>(std::cout, "\n"));
}


//int main(int argc, char* argv[])
int randomvalgentest(int, char*[])
{
  UInt32Test();
  SInt32Test();
  DoubleTest();
  return 0;
}
