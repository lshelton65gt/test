// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/RandomValGen.h"
#include "util/Timing.h"
#include "util/UtIOStream.h"
#include "util/CarbonAssert.h"

template<class DType>
static int test(DType innerLimit)
{
  DType totalCountQC = 0;
  DType a;
  unsigned int outerLimit = 0xFFFFFFFF / innerLimit;
  UtIO::cout() << (8*sizeof(DType)) << "-bit ?: ";
  UtIO::cout().flush();
  Timing timing;
  timing.start();
  for (unsigned int i = 0; i < outerLimit; ++i)
  {
    bool ena = i & 1;
    for (DType iter = 0; iter < innerLimit; ++iter)
    {
      a = ena? iter: 0;
      totalCountQC += a;
    }
  }
  timing.end();
  timing.printDeltas();

  DType totalCountITE = 0;
  UtIO::cout() << (8*sizeof(DType)) << "-bit if ";
  UtIO::cout().flush();
  timing.start();
  for (unsigned int i = 0; i < outerLimit; ++i)
  {
    bool ena = i & 1;
    for (DType iter = 0; iter < innerLimit; ++iter)
    {
      if (ena)
        a = iter;
      else
        a = 0;
      totalCountITE += a;
    }
  }
  timing.end();
  timing.printDeltas();
  assert(totalCountITE == totalCountQC);
  return totalCountITE;
} // static int test


int miscperf()
  
{
  UInt64 total = 0;
  total += test<UInt8>(0xFE);
  total += test<UInt16>(0xFFFE);
  total += test<UInt32>(0xFFFE);
  total += test<UInt64>(0xFFFE);
  UtIO::cout() << total << UtIO::endl;
  return 0;
}
