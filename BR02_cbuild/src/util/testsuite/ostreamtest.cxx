// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <fstream>
#include <iomanip>
#include "util/UtIOStream.h"

#define WRITESTREAMS(phrase) \
  stdStream << phrase << std::endl; \
  utStream <<  phrase << UtIO::endl;

int ostreamtest()
{
  const char* stdName = "ostream.out";
  const char* utName = "utostream.out";
#if pfGCC_2
  std::ofstream stdStream(stdName, ios::out | ios::trunc);
#else
  std::ofstream stdStream(stdName, std::ios_base::out | std::ios_base::trunc);
#endif
    
  UtOFStream utStream(utName);

  // alphanumerics
  const char* phrase = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
  WRITESTREAMS(phrase);

  // reset the width to something smaller. Should ignore width in this
  // case
  stdStream << std::setw(4) << phrase << " " << phrase << std::endl;
  utStream <<  UtIO::setw(4) << phrase << " " << phrase << UtIO::endl;
  
  // non-alphanumeric characters
  phrase = "`~!@#$%^&*()_+-={}[]:;\"'<>,. ?/|\\";
  WRITESTREAMS(phrase);

  // empty string
  phrase = "";
  WRITESTREAMS(phrase);

  // change base
  stdStream <<  std::hex << 10 << " " << 11 << std::endl;
  utStream <<  UtIO::hex << 10 << " " << 11 << UtIO::endl;
  stdStream <<  std::oct << 10 << " " << 11 << std::endl;
  utStream <<  UtIO::oct << 10 << " " << 11 << UtIO::endl;
  stdStream <<  std::dec << 10 << " " << 11 << std::endl;
  utStream <<  UtIO::dec << 10 << " " << 11 << UtIO::endl;
  // difference here. We use dec for unsigned and sdec for signed
  stdStream <<  std::dec << -10 << " " << 11 << std::endl;
  utStream <<  UtIO::sdec << -10 << " " << 11 << UtIO::endl;
  


  // set width and fill and base
  stdStream <<  std::hex << std::setw(15) << std::setfill('0') << 13 << " " << 14 << std::endl;
  utStream <<  UtIO::hex << UtIO::setw(15) << UtIO::setfill('0') << 13 << " " << 14 << UtIO::endl;

  // set width and base, but fill with something wacky
  stdStream <<  std::hex << std::setw(15) << std::setfill('(') << 15 << " " << 10 << std::endl;
  utStream <<  UtIO::hex << UtIO::setw(15) << UtIO::setfill('(') << 15 << " " << 10 << UtIO::endl;
  // now with width == 0
  stdStream <<  std::hex << std::setw(0) << std::setfill('(') << 14 << " " << 10 << std::endl;
  utStream <<  UtIO::hex << UtIO::setw(0) << UtIO::setfill('(') << 14 << " " << 10 << UtIO::endl;

  // make sure hexness stays
  stdStream << 14 << std::endl;
  utStream << 14 << UtIO::endl;

  
  // set width to something smaller
  stdStream <<  std::oct << std::setw(1) << std::setfill('(') << 29 << std::endl;
  utStream <<  UtIO::oct << UtIO::setw(1) << UtIO::setfill('(') << 29 << UtIO::endl;
  
  phrase = "bah";
  // setfill should still be good here
  stdStream <<  std::setw(10) << phrase << std::endl;
  utStream <<  UtIO::setw(10) << phrase << UtIO::endl;
  
  // set width and fill and base with char stream
  phrase = "silly";
  stdStream <<  std::hex << std::setw(20) << std::setfill('$') << phrase << " " << phrase << std::endl;
  utStream <<  UtIO::hex << UtIO::setw(20) << UtIO::setfill('$') << phrase << " " << phrase << UtIO::endl;

  // double to hex?
  stdStream <<  std::hex << 15.189 << " " << 10 << " " << 11 << std::endl;
  utStream <<  UtIO::hex << 15.189 << " " << 10 << " " << 11 << UtIO::endl;

  // fixed double
  // I can't find a way to make this work with gcc 2
#if ! pfGCC_2
  stdStream <<  std::dec 
            << std::fixed 
            << std::setprecision(1) << 15.189 << " " << 10.567 << std::endl;
  utStream <<  UtIO::dec << UtIO::fixed << UtIO::setprecision(1) << 15.189 << " " << 10.567 << UtIO::endl;
#endif
  
  // The following has different results with std::ofstream between
  // gcc 2.95.3 and gcc 3.2.3

  // char with >1 width
#if pfGCC_2
  stdStream << std::setw(8) << std::setfill('&') << "choo" << std::endl;
#else
  stdStream << std::setw(5) << std::setfill('&') << 'c' << "hoo" << std::endl;
#endif
  utStream <<  UtIO::setw(5) << UtIO::setfill('&') << 'c' << "hoo" << UtIO::endl;

  phrase = "done";
  WRITESTREAMS(phrase);
  return 0;
}
