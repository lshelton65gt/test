// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtStream.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtIOStream.h"
#include <fstream>
#include "util/UtSet.h"
#include "util/UtHashSet.h"

// please do not use "using namespace std"

struct IntCmp {
  size_t hash(int var) const {return var;}
  bool lessThan(int v1, int v2) const {return v1 < v2;}
  bool lessThan1 (int v1, int v2) const { return v1 < v2; }
  bool equal(int v1, int v2) const {return v1 == v2;}
};

int intsettest()
{
  typedef UtHashSet<int, IntCmp> IntSet;
  IntSet hfib;
  typedef UtSet<int> IntStdSet;
  IntStdSet sfib;
  int x;
  x = 2;  hfib.insert(x);  sfib.insert(x);
  x = 5;  hfib.insert(x);  sfib.insert(x);
  x = 3;  hfib.insert(x);  sfib.insert(x);
  x = 13; hfib.insert(x);  sfib.insert(x);
  x = 8;  hfib.insert(x);  sfib.insert(x);
  x = 8;  hfib.insert(x);  sfib.insert(x);
  x = 8;  hfib.insert(x);  sfib.insert(x);
  x = 2;  hfib.insert(x);  sfib.insert(x);

  IntStdSet::iterator sp = sfib.begin();
  IntStdSet::iterator se = sfib.end();
  int status = 0;
  IntSet::SortedLoop hp = hfib.loopSorted();
  for (; !hp.atEnd() && (sp != se); ++hp, ++sp)
  {
    if (*hp != *sp)
    {
      UtIO::cout() << "Mismatch between set and UtHashSet: hash=" << *hp << ", set=" << *sp << UtIO::endl;
      status = 1;
    }
    UtIO::cout() << *hp << " ";
  }
  UtIO::cout() << UtIO::endl;
  if (!hp.atEnd())
  {
    UtIO::cout() << "Unexpected end of set" << UtIO::endl;
    status = 1;
  }
  else if (sp != se)
  {
    UtIO::cout() << "Unexpected end of UtHashSet" << UtIO::endl;
    status = 1;
  }
  return status;
} // int intsettest

static int complexCopies = 0;

// Test that we are able to loop through a set of
// complex numbers values without copy-constructing them.
class Complex
{
public:
  Complex(double real, double imag): mReal(real), mImag(imag) {}
  ~Complex() {}
  static int compare(const Complex* c1, const Complex* c2) {
    int cmp = 0;
    if (c1->mReal < c2->mReal)
      cmp = -1;
    else if (c1->mReal > c2->mReal)
      cmp = 1;
    else if (c1->mImag < c2->mImag)
      cmp = -1;
    else if (c1->mImag > c2->mImag)
      cmp = 1;
    return cmp;
  }
  bool operator==(const Complex& other) const {
    return ((mReal == other.mReal) && (mImag == other.mImag));
  }
  bool operator<(const Complex& other) const {
    return compare(this, &other) < 0;
  }


  Complex(const Complex& src): mReal(src.mReal), mImag(src.mImag) {
    ++complexCopies;
  }
  Complex& operator=(const Complex& src)
  {
    if (&src != this)
    {
      ++complexCopies;
      mReal = src.mReal;
      mImag = src.mImag;
    }
    return *this;
  }

  void print() const {
    UtIO::cout() << "(" << mReal << "," << mImag << ")" << UtIO::endl;
  }

  size_t hash() const {return ((int) mReal) + ((int) mImag);}
private:
  double mReal;
  double mImag;
}; // class Complex
      
typedef UtHashSet<Complex, HashValue<Complex> > UtComplexSet;

template<class CSet> int complexsettest(int factor) {
  CSet c;
  c.insert(Complex(1, 0));
  c.insert(Complex(1, 2));
  c.insert(Complex(2, 1));
  c.insert(Complex(2, 3));

  // This loop will use a copy-constructor & probably some temps
  int copies = complexCopies;
  for (typename CSet::SortedLoop loop = c.loopSorted(); !loop.atEnd(); ++loop)
  {
    const Complex& cx = *loop;
    cx.print();
  }
  assert(copies*factor == complexCopies);

  // This loop should use no copy constructors
  copies = complexCopies;
  typename CSet::SortedLoop loop = c.loopSorted();
  for (const Complex* cptr; loop(&cptr);)
    cptr->print();
  assert(copies == complexCopies);

  // This loop is unsorted, and should also use no copy ctors
  copies = complexCopies;
  typename CSet::UnsortedLoop uloop = c.loopUnsorted();
  for (const Complex* cptr; uloop(&cptr);)
    cptr->print();
  assert(copies == complexCopies);

  return 0;
}

int hashsettest(int /*argc*/, char* /*argv*/[])
{
  int status = intsettest();
  status += complexsettest<UtComplexSet>(1);  // no extra copy-ctors
  return status;
}
