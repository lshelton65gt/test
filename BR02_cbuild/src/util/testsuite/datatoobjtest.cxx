//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/UtIOStream.h"
#include "util/DataToObj.h"

int datatoobjtest(int argc, char** argv) {
  if (argc != 4) {
    return 1;
  }
  UtString errmsg;
  if (!UtConvertFileToCFunction(argv[1], argv[2], argv[3], &errmsg)) {
    UtIO::cout() << errmsg << "\n";
    return 1;
  }
  return 0;
}
