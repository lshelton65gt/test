// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _INTBIGRAPH_H_
#define _INTBIGRAPH_H_

typedef GenericBigraph<int, unsigned int> IntGraph;
typedef IntGraph::Node IntNode;
typedef IntGraph::Edge IntEdge;

class IntNodeCmp : public GraphSortedWalker::NodeCmp
{
public:
  //! constructor
  IntNodeCmp(IntGraph* graph) : mGraph(graph) {}

  //! override function to compare two nodes
  bool operator()(const GraphNode* gn1, const GraphNode* gn2) const
  {
    const IntNode* node1 = mGraph->castNode(gn1);
    const IntNode* node2 = mGraph->castNode(gn2);
    return node1->getData() < node2->getData();
  }
private:
  IntGraph* mGraph;
};

class IntEdgeCmp : public GraphSortedWalker::EdgeCmp
{
public:
  //! constructor
  IntEdgeCmp(IntGraph* graph) : mGraph(graph) {}

  //! override function to compare two edges
  bool operator()(const GraphEdge* ge1, const GraphEdge* ge2) const
  {
    const IntEdge* edge1 = mGraph->castEdge(ge1);
    const IntEdge* edge2 = mGraph->castEdge(ge2);
    return edge1->getData() < edge2->getData();
  }
private:
  IntGraph* mGraph;
};

class PrintWalker : public GraphSortedWalker
{
public:
  //! constructor
  PrintWalker(IntGraph* graph) : mGraph(graph), mIndent(0) {}

  //! Override visitNodeAfter to print in BFS
  Command visitNodeBefore(Graph*, GraphNode* gn)
  {
    // Print the node integer
    IntNode* node = mGraph->castNode(gn);
    printIndent();
    UtIO::cout() << "Start Node: ";
    printNode(node);
    UtIO::cout() << "\n";
    mIndent += 2;
    return GW_CONTINUE;
  }

  //! Override visitNodeAfter to print in DFS
  Command visitNodeAfter(Graph*, GraphNode* gn)
  {
    // Print the node integer
    IntNode* node = mGraph->castNode(gn);
    mIndent -= 2;
    printIndent();
    UtIO::cout() << "End Node  : ";
    printNode(node);
    UtIO::cout() << "\n";
    return GW_CONTINUE;
  }

  //! Override visitTreeEdge to print the edge
  Command visitTreeEdge(Graph*, GraphNode* start, GraphEdge* edge)
  {
    printEdge("Tree Edge ", start, edge);
    return GW_CONTINUE;
  }

  //! Override visitCrossEdge to print the edge
  Command visitCrossEdge(Graph*, GraphNode* start, GraphEdge* edge)
  {
    printEdge("Cross Edge", start, edge);
    return GW_CONTINUE;
  }

  //! Override visitBackEdge to print the edge that finishes the cycle
  Command visitBackEdge(Graph*, GraphNode* start, GraphEdge* edge)
  {
    printEdge("Back Edge ", start, edge);
    return GW_CONTINUE;
  }

protected:
  virtual void printNode(const IntNode* node)
  {
    UtIO::cout() << node->getData();
  }

  virtual void printEdge(const IntEdge* edge)
  {
    UtIO::cout() << edge->getData();
  }

private:
  void printIndent()
  {
    assert(mIndent >= 0);
    for (int i = 0; i < mIndent; ++i) {
      UtIO::cout() << " ";
    }
  }

  void printEdge(const char* type, GraphNode* graphNode, GraphEdge* graphEdge)
  {
    // Convert to the IntGraph types
    IntNode* start = mGraph->castNode(graphNode);
    IntEdge* edge = mGraph->castEdge(graphEdge);
    assert(start == edge->getStartPoint());

    // Print the information start -> to : edge value
    printIndent();
    UtIO::cout() << type << ": ";
    printNode(start);
    UtIO::cout() << " -> ";
    printNode(edge->getEndPoint());
    UtIO::cout() << ": ";
    printEdge(edge);
    UtIO::cout() << "\n";
  }
  IntGraph* mGraph;
  int mIndent;
}; // class PrintWalker : public GraphSortedWalker

static IntNode* addNode(IntGraph* intGraph, int i)
{
  IntNode* node = new IntNode(i);
  intGraph->addNode(node);
  return node;
}

static void addEdge(IntGraph* intGraph, IntNode* start, IntNode* to, int edgeData)
{
  IntEdge* edge = new IntEdge(start, to, edgeData);
  intGraph->addEdge(start, edge);
}



#endif // _INTBIGRAPH_H_
