// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtShellTok.h"
#include "util/UtString.h"
#include "util/UtVector.h"

int shelltoktest()
{
  UtString line;
  UtVector<UtString> lines;
  while (fgetline(stdin, &line))
    lines.push_back(line);

  // Test the parsing of command lines
  for (size_t i = 0; i < lines.size(); ++i)
  {
    line = lines[i];
    fprintf(stdout, "----------------------------------------\n");
    fprintf(stdout, "ORIG=%s", line.c_str());
    int i = 0;
    UtString requoted, buf;
    for (UtShellTok tok(line.c_str()); !tok.atEnd(); ++tok, ++i)
    {
      fprintf(stdout, "%d.\tTOKEN=%s\n", i, *tok);
      fprintf(stdout, "\tREST=%s", tok.curPos());
      requoted << UtShellTok::quote(*tok, &buf) << " ";
    }
    fprintf(stdout, "REQUOTED=%s\n", requoted.c_str());
    fprintf(stdout, "----------------------------------------\n");
  }
  return 0;
}
