/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include <cstdio>
#include "util/UtDLList.h"
#include "shell/carbon_shelltypes.h"

class test_class {
  UInt32 mVar;
public:
  test_class(int var) { mVar = var; }
  void setVar(int _var) {mVar = _var;}
  int  getVar() const {return mVar;}
};

int utdllisttest() {

  {
    UtDLList<int> test_list;
    
    printf("Testing Push Back\n");
    test_list.push_back(0);
    test_list.push_back(1);
    test_list.push_back(2);
    test_list.push_back(3);
    test_list.push_back(4);
    test_list.push_back(5);
    test_list.push_back(6);
    test_list.push_back(7);
    test_list.push_back(8);
    test_list.push_back(9);
    
    for(UtDLList<int>::iterator iter = test_list.begin(); iter != test_list.end(); ++iter) {
      printf("Val = %d\n", *iter);
    }
    
    printf("Testing pop_front\n");
    test_list.pop_front();
    for(UtDLList<int>::iterator iter = test_list.begin(); iter != test_list.end(); ++iter) {
      printf("Val = %d\n", *iter);
    }
    
    printf("Testing pop_back\n");
    test_list.pop_back();
    for(UtDLList<int>::iterator iter = test_list.begin(); iter != test_list.end(); ++iter) {
      printf("Val = %d\n", *iter);
    }
    
    printf("Testing Front\n");
    printf("Front: %d\n", test_list.front());
    
    printf("Testing Back\n");
    printf("Front: %d\n", test_list.back());
    
    printf("Testing Push Front\n");
    test_list.clear();
    test_list.push_front(0);
    test_list.push_front(1);
    test_list.push_front(2);
    test_list.push_front(3);
    test_list.push_front(4);
    test_list.push_front(5);
    test_list.push_front(6);
    test_list.push_front(7);
    test_list.push_front(8);
    test_list.push_front(9);
    
    for(UtDLList<int>::iterator iter = test_list.begin(); iter != test_list.end(); ++iter) {
      printf("Val = %d\n", *iter);
    }
    
    printf("Testing Front\n");
    printf("Front: %d\n", test_list.front());
    
    printf("Testing Back\n");
    printf("Front: %d\n", test_list.back());
    
    printf("Testing Insert.\n");
    UtDLList<int>::iterator insert_iter = test_list.begin();
    test_list.insert(insert_iter, -1);
    insert_iter++;
    insert_iter++;
    insert_iter++;
    insert_iter++;
    test_list.insert(insert_iter, 100);
    insert_iter--;
    --insert_iter;
    test_list.insert(insert_iter, 200);
    insert_iter = test_list.end();
    test_list.insert(insert_iter, 300);
    
    for(UtDLList<int>::iterator iter = test_list.begin(); iter != test_list.end(); ++iter) {
      printf("Val = %d\n", *iter);
    }
    
  }
  {
    printf("Testing Ranged Insert.\n");
    UtDLList<int> test_list1;
    UtDLList<int> test_list2;
    for(int i = 0; i < 10; i++) {
      test_list1.push_back(i);
      test_list2.push_back(i*10);
    }
    
    UtDLList<int>::iterator  iter1 = test_list1.begin();
    UtDLList<int>::iterator  iter2 = test_list2.begin();
    UtDLList<int>::iterator  iter3 = test_list2.begin();
    
    iter1++; iter1++; iter1++;
    iter2++; iter2++; iter2++; iter2++; iter2++;
    iter3++; iter3++; iter3++; iter3++; iter3++; iter3++; iter3++; iter3++;
    
    test_list1.insert(iter1, iter2, test_list2.end());
    
    for(UtDLList<int>::iterator iter = test_list1.begin(); iter != test_list1.end(); ++iter) {
      printf("Val = %d\n", *iter);
    }
  }
  {
    printf("Testing Splice\n");
    UtDLList<int> test_list1;
    UtDLList<int> test_list2;
    for(int i = 0; i < 10; i++) {
      test_list1.push_back(i);
      test_list2.push_back(i*10);
    }
    
    UtDLList<int>::iterator  iter1 = test_list1.begin();
    UtDLList<int>::iterator  iter2 = test_list2.begin();
    UtDLList<int>::iterator  iter3 = test_list2.begin();
    
    iter1++; iter1++; iter1++;
    iter2++; iter2++; iter2++; iter2++; iter2++;
    iter3++; iter3++; iter3++; iter3++; iter3++; iter3++; iter3++; iter3++;
    
    UInt32 size = test_list1.size();
    printf("List 1 size = %d\n", size);
    for(UtDLList<int>::iterator iter = test_list1.begin(); iter != test_list1.end(); ++iter) {
      printf("List 1: Val = %d\n", *iter);
    }

    size = test_list2.size();
    printf("List 2 size = %d\n", size);
    for(UtDLList<int>::iterator iter = test_list2.begin(); iter != test_list2.end(); ++iter) {
      printf("List 2: Val = %d\n", *iter);
    }
    
    printf("Performing Splice.\n");
    test_list1.splice(iter1, test_list2);
    
    for(UtDLList<int>::iterator iter = test_list1.begin(); iter != test_list1.end(); ++iter) {
      printf("List 1: Val = %d\n", *iter);
    }

    for(UtDLList<int>::iterator iter = test_list2.begin(); iter != test_list2.end(); ++iter) {
      printf("List 2: Val = %d\n", *iter);
    }
  }
  
  {
    printf("Testing const_iterator\n");
    
    UtDLList<test_class *> test_list;    
    for(int i = 0; i < 10; i++) {
      test_list.push_back(new test_class(i));
    }

    UtDLList<test_class *>::const_iterator iter;
    iter = test_list.begin();
    while(iter != test_list.end()) {
      printf("Val = %d\n", (*iter)->getVar());
      iter++;
    }
    
    // Clean up
    for(UtDLList<test_class *>::iterator iter = test_list.begin(); iter != test_list.end(); ++iter) {
      delete *iter;
    }

    printf("Testing const_iterator with constant values in container\n");
    
    UtDLList<const test_class *> test_list1;    
    for(int i = 0; i < 10; i++) {
      test_list1.push_back(new test_class(i));
    }

    UtDLList<const test_class *>::const_iterator iter1;
    iter1 = test_list1.begin();
    while(iter1 != test_list1.end()) {
      printf("Val = %d\n", (*iter1)->getVar());
      iter1++;
    }

    // Clean up
    for(UtDLList<const test_class *>::iterator iter = test_list1.begin(); iter != test_list1.end(); ++iter) {
      delete *iter;
    }

  }
  return 0;
}
  
