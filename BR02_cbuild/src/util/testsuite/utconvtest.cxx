// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtConv.h"
#include "util/UtString.h"
#include "util/UtVector.h"
#include "util/OSWrapper.h"
#include "util/UtIOStream.h"
#include "util/DynBitVector.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"

static int sTestValToFormatStr()
{
  UtVector<UtString> input;
  UtVector<UtString> octGold;
  UtVector<UtString> decGold;
  
  int stat = 0;
  UtOStream& io_cerr = UtIO::cerr();

  {
    UtString inBuf;
    {
      UtString filename;
      filename += "convtest.in";
      io_cerr << filename << UtIO::endl;
      UtIBStream in(filename.c_str());
      if (!in)
      {
        io_cerr << "Cannot open input file " << filename << UtIO::endl;
        return 1;
      }
      
      while (in >> inBuf) {
        input.push_back(inBuf);
        inBuf.clear();
      }
      in.close();

      size_t size = input.size();
      octGold.reserve(size);
      decGold.reserve(size);
    }
    
    {
      UtString filename;
      filename += "octal.result.gold";
      UtIBStream in(filename.c_str());
      if (!in)
      {
        io_cerr << "Cannot open input file " << filename << UtIO::endl;
        return 1;
      }
      
      inBuf.clear();
      while (in >> inBuf) {
        octGold.push_back(inBuf);
        inBuf.clear();
      }
      in.close();
      assert(octGold.size() == input.size());
    }

    
    {
      UtString filename;
      filename += "decimal.result.gold";
      UtIBStream in(filename.c_str());
      if (!in)
      {
        io_cerr << "Cannot open input file " << filename << UtIO::endl;
        return 1;
      }
      
      inBuf.clear();
      while (in >> inBuf) {
        decGold.push_back(inBuf);
        inBuf.clear();
      }
      in.close();
      assert(decGold.size() == input.size());
    }
  }
  
  UtVector<UInt32*> inputNumbers;
  inputNumbers.reserve(input.size());
  UtVector<size_t> inputWidths;
  inputWidths.reserve(input.size());
  for (UtVector<UtString>::const_iterator s = input.begin();
       s != input.end(); ++s)
  {
    const UtString& in = *s;
    
    size_t dataArrSize = ((in.size() * 4) + 31)/32;
    UInt32* inputArray = new UInt32[dataArrSize];
    inputWidths.push_back(in.size() * 4);
    UtString tmp = in;
    size_t numToInsert = 0;
    if ((sizeof(UInt32) * 2) - in.size() > 0)
      numToInsert = ((sizeof(UInt32) * 2) - in.size()) % (sizeof(UInt32) * 2);
    if (numToInsert > 0)
      tmp.insert(0, numToInsert, '0');
    UtConv::HexStringToUInt32(tmp.c_str(), inputArray, NULL,
                              inputWidths.back());
    /*
      UtIStringStream convStream(tmp.c_str());
      UInt32 tmpVal;
      for (int j = dataArrSize - 1; j >= 0; --j)
      {
      convStream >> tmpVal;
      inputArray[j] = tmpVal;
      }
    */
    inputNumbers.push_back(inputArray);
  }
  assert(inputNumbers.size() == input.size());
  assert(inputWidths.size() == input.size());
  
  // do octal
  size_t numIns = input.size();
  size_t i;
  const size_t maxResultSize = 10000;
  char convResult[maxResultSize];
  for (i = 0; i < numIns; ++i)
  {
    int checkSize = CarbonValRW::writeOctValToStr(convResult, maxResultSize, inputNumbers[i], inputWidths[i]);
    if (checkSize != (signed)(inputWidths[i] + 2)/3)
    {
      io_cerr << "Incorrect size usage return by writeOctValToStr\n";
      stat = 3;
    }

    if (octGold[i].compare(convResult) != 0)
    {
      io_cerr << "Octal value " << i << " is wrong.";
      io_cerr << "  Expected " << octGold[i] << ", got " << convResult
                << UtIO::endl;
      stat = 3;
    }
  }

  // do decimal
  for (i = 0; i < numIns; ++i)
  {
    // The second number is an integer, and must be treated as signed
    if (i == 1)
      UtConv::BinaryToSignedDec(inputNumbers[i], inputWidths[i], false, convResult, maxResultSize);
    else
      UtConv::BinaryToUnsignedDec(inputNumbers[i], inputWidths[i], convResult,
                                  maxResultSize);
    
    const char* goldStr = decGold[i].c_str();
    if (strcmp(goldStr, convResult) != 0)
    {
      io_cerr << "Decimal " << i << " expected " << goldStr
              << " got " << convResult << UtIO::endl;
      stat = 3;
    }
  }

  // do decimal through the writeDecValToStr functions
  
  // UInt8
  UInt8 val8 = 0xfa;
  // numbits = 7 (8 - 1), bufsize = (7+2)/3 + 1 = 4.
  char buf8[4];
  if (CarbonValRW::writeDecValToStr(buf8, 4, &val8, false, 7) == -1)
  {
    io_cerr << "WriteToDec - uint8 failed: Buffer size not accepted.\n";
    stat = 3;
  }
  else if (strcmp(buf8, "122") != 0)
  {
    io_cerr << "WriteToDec - uint8 failed: Expected 122, got " << buf8 << "\n";
    stat = 3;
  }

  // UInt16
  UInt16 val16 = 0xfaa5;
  // numbits = 15 (16 - 1), bufsize = (15+2)/3 + 1 = 6.
  char buf16[6];
  if (CarbonValRW::writeDecValToStr(buf16, 6, &val16, false, 15) == -1)
  {
    io_cerr << "WriteToDec - uint16 failed: Buffer size not accepted.\n";
    stat = 3;
  }
  else if (strcmp(buf16, "31397") != 0)
  {
    io_cerr << "WriteToDec - uint16 failed: Expected 31397, got " << buf16 << "\n";
    stat = 3;
  }

  // UInt32
  // unsigned buffer check (worst case was being calculated the same
  // as hex. It now uses octal worst case).
  UInt32 val32 = 1000000;
  const int decWrongSize = (32 + 3)/4 + 1;
  char buf32wrong[decWrongSize];
  if (CarbonValRW::writeDecValToStr(buf32wrong, decWrongSize, &val32, false, 32) != -1)
  {
    // most likely crashed!
    io_cerr << "writeDecValToStr = Invalid buffer sent but excepted.";
    stat = 3;
  }

  // these attempts to write should fail because buffer is too small
  char buftemp[6];
  val32 = 8190;
  if (CarbonValRW::writeDecValToStr(buftemp, 4, &val32, false, 12) != -1)
  {
    io_cerr << "WriteToDec - buftemp failed: buffer size 4 is too small to hold 8190.\n";
    stat = 3;
  }
  
  val32 = 8191;
  if (CarbonValRW::writeDecValToStr(buftemp, 5, &val32, false, 13) != -1)
  {
    io_cerr << "WriteToDec - buftemp failed: buffer is large enough to hold the value 8191 but test in code is pessimistic so this attempt to write a Dec value should have failed.\n";
    stat = 3;
  }
  if (CarbonValRW::writeDecValToStr(buftemp, 6, &val32, false, 13) == -1)
  {
    io_cerr << "WriteToDec - buftemp failed: Sufficient buffer size was not accepted.\n";
    stat = 3;
  }

  val32 = 0xfaa50f21;
  // unsigned truncate
  // numbits = 31 (32 - 1), bufsize = (31+2)/3 + 1 = 12.
  char buf32u[12];
  if (CarbonValRW::writeDecValToStr(buf32u, 10, &val32, false, 31) != -1)
  {
    io_cerr << "WriteToDec - uint32u failed: Incorret buffer size should not have been accepted(unsigned).\n";
    stat = 3;
  }
  if (CarbonValRW::writeDecValToStr(buf32u, 12, &val32, false, 31) == -1)
  {
    io_cerr << "WriteToDec - uint32u failed: Buffer size not accepted.\n";
    stat = 3;
  }
  else if (strcmp(buf32u, "2057637665") != 0)
  {
    io_cerr << "WriteToDec - uint32u failed: Expected 2057637665, got " << buf32u << "\n";
    stat = 3;
  }
  // signed
  char buf32s[13]; // adding 1 for the -
  if (CarbonValRW::writeDecValToStr(buf32s, 9, &val32, true, 32) != -1)
  {
    io_cerr << "WriteToDec - uint32s failed: Incorrect buffer size should not have been accepted(signed).\n";
    stat = 3;
  }
  buf32s[0] = '\0';
  if (CarbonValRW::writeDecValToStr(buf32s, 13, &val32, true, 32) == -1)
  {
    io_cerr << "WriteToDec - uint32s failed: Buffer size not accepted.\n";
    stat = 3;
  }
  else if (strcmp(buf32s, "-89845983") != 0)
  {
    io_cerr << "WriteToDec - uint32s failed: Expected -89845983, got " << buf32s << "\n";
    stat = 3;
  }
  
  // UInt64
  // numbits = 63 (64 - 1), bufsize = (63+2)/3 + 1 = 22.
  // needs 22
  UInt64 val64 = KUInt64(0xf000ffffaaaa5555);
  char buf64[23];
  // unsigned
  if (CarbonValRW::writeDecValToStr(buf64, 19, &val64, false, 63) != -1)
  {
    io_cerr << "WriteToDec - uint64 failed: Incorrect buffer size should not have been accepted (unsigned).\n";
    stat = 3;
  }
  buf64[0] = '\0';
  if (CarbonValRW::writeDecValToStr(buf64, 22, &val64, false, 63) == -1)
  {
    io_cerr << "WriteToDec - uint64 failed: Buffer size not accepted.\n";
    stat = 3;
  }
  else if (strcmp(buf64, "8070732005792961877") != 0)
  {
    io_cerr << "WriteToDec - uint32s failed: Expected 8070732005792961877, got " << buf64 << "\n";
    stat = 3;
  }
  // signed
  if (CarbonValRW::writeDecValToStr(buf64, 22, &val64, true, 63) != -1)
  {
    // note that the necessary buf size test is slightly pessimistic for
    // some values, (this particular value would fit into a buffer of 21 bytes)
    io_cerr << "WriteToDec - uint64s failed: Insufficient buffer size should not have been accepted (signed).\n";
    stat = 3;
  }
  buf64[0] = '\0';
  if (CarbonValRW::writeDecValToStr(buf64, 23, &val64, true, 63) == -1)
  {
    // this particular value actually only needs 20+1 bytes but the size
    // test is based on octal conversion and 63 bits signed could use
    // upt to 22+1 bytes
    io_cerr << "WriteToDec - uint64s failed: Sufficient buffer size was not accepted.\n";
    stat = 3;
  }
  else if (strcmp(buf64, "-1152640031061813931") != 0)
  {
    io_cerr << "WriteToDec - uint64s failed: Expected -1152640031061813931, got " << buf64 << "\n";
    stat = 3;
  }
  
  // do a non-trivial signed UtIO::dec (no verilog results for this)
  UInt32 arr[2];
  arr[0] = 0x00000f01;
  arr[1] = 0x800000f0;

  UtConv::BinaryToSignedDec(arr, 64, true, convResult, maxResultSize); 
  UtString signedGold;
  signedGold.assign("-9223371006062620927");
  if (signedGold.compare(convResult) != 0)
  {
    io_cerr << "Non-trivial signed decimal conversion failed" << UtIO::endl;
    stat = 3;
  }
  
  for(UtVector<UInt32*>::iterator sm = inputNumbers.begin();
      sm != inputNumbers.end(); ++sm)
  {
    UInt32* arr = *sm;
    delete [] arr;
  }
  
  // A simple mask/value format
  // 33 bit z0..1x
  DynBitVector value(33);
  DynBitVector mask(33);
  mask.set(32);
  mask.set(0);
  value.set(1);
  value.set(0);
  UtString expect("z0000000000000000000000000000001x");
  UtString dynValBuf;
  CarbonValRW::writeBin4ToStr(&dynValBuf, &value, &mask);
  if ( dynValBuf.compare(expect) != 0 )
  {
    io_cerr << "writeBin4ToStr failed." << UtIO::endl;
    stat = 3;
  }
  
  return stat;
}

static int sTestValXZToFormatStr()
{
  UtOStream& io_cerr = UtIO::cerr();
  int stat;
  UInt8 val8, xdrive8, idrive8, forceMask8;
  UInt16 val16, xdrive16, idrive16, forceMask16;
  UInt32 val32, xdrive32, idrive32, forceMask32;
  UInt64 val64, xdrive64, idrive64, forceMask64;
  UInt32 valA[3];
  UInt32 xdriveA[3];
  UInt32 idriveA[3];
  UInt32 forceA[3];

  UInt32 control1[3];
  UInt32 control64[6];
  UInt32 controlA[9];
  char valueStr[100];
  char auxStr[100];

  val8 = xdrive8 = forceMask8 = 0;
  val16 = xdrive16 = forceMask16 = 0;
  val32 = xdrive32 = forceMask32 = 0;
  val64 = xdrive64 = forceMask64 = 0;
  
  idrive64 = UInt64(~0);
  idrive32 = UInt32(~0);
  idrive16 = UInt16(~0);
  idrive8 = UInt8(~0);

  CarbonValRW::setToZero(valA, 3);
  CarbonValRW::setToZero(xdriveA, 3);
  CarbonValRW::setToOnes(idriveA, 3);
  CarbonValRW::setToZero(forceA, 3);
  CarbonValRW::setToZero(control1, 3);
  CarbonValRW::setToZero(control64, 6);
  CarbonValRW::setToZero(controlA, 9);

  stat = 0;
  // First, try formatting without drives, controlmask, or forceMask
  if (CarbonValRW::writeBinXZValToStr(valueStr, 100, &val8, NULL, NULL, NULL, NULL, false, 5) != 5)
  {
    io_cerr << "writeBinXZValToStr (8, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "00000") != 0)
  {
    io_cerr << "writeBinXZValToStr (8, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeBinXZValToStr(valueStr, 100, &val16, NULL, NULL, NULL, NULL, false, 15) != 15)
  {
    io_cerr << "writeBinXZValToStr (16, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "000000000000000") != 0)
  {
    io_cerr << "writeBinXZValToStr (16, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeBinXZValToStr(valueStr, 100, &val32, NULL, NULL, NULL, NULL, false, 29) != 29)
  {
    io_cerr << "writeBinXZValToStr (32, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  auxStr[29] = '\0';
  for (int i = 0; i < 29; ++i)
    auxStr[i] = '0';

  if (strcmp(valueStr, auxStr) != 0)
  {
    io_cerr << "writeBinXZValToStr (32, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }


  if (CarbonValRW::writeBinXZValToStr(valueStr, 100, &val64, NULL, NULL, NULL, NULL, false, 51) != 51)
  {
    io_cerr << "writeBinXZValToStr (64, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  auxStr[51] = '\0';
  for (int i = 29; i < 51; ++i)
    auxStr[i] = '0';

  if (strcmp(valueStr, auxStr) != 0)
  {
    io_cerr << "writeBinXZValToStr (64, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeBinXZValToStr(valueStr, 100, valA, NULL, NULL, NULL, NULL, false, 80) != 80)
  {
    io_cerr << "writeBinXZValToStr (A, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  auxStr[80] = '\0';
  for (int i = 51; i < 80; ++i)
    auxStr[i] = '0';

  if (strcmp(valueStr, auxStr) != 0)
  {
    io_cerr << "writeBinXZValToStr (A, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }


  if (CarbonValRW::writeHexXZValToStr(valueStr, 100, &val8, NULL, NULL, NULL, NULL, false, 5) != 2)
  {
    io_cerr << "writeHexXZValToStr (8, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "00") != 0)
  {
    io_cerr << "writeHexXZValToStr (8, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeHexXZValToStr(valueStr, 100, &val16, NULL, NULL, NULL, NULL, false, 15) != 4)
  {
    io_cerr << "writeHexXZValToStr (16, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "0000") != 0)
  {
    io_cerr << "writeHexXZValToStr (16, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeHexXZValToStr(valueStr, 100, &val32, NULL, NULL, NULL, NULL, false, 29) != 8)
  {
    io_cerr << "writeHexXZValToStr (32, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  auxStr[8] = '\0';
  for (int i = 0; i < 8; ++i)
    auxStr[i] = '0';

  if (strcmp(valueStr, auxStr) != 0)
  {
    io_cerr << "writeHexXZValToStr (32, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }


  if (CarbonValRW::writeHexXZValToStr(valueStr, 100, &val64, NULL, NULL, NULL, NULL, false, 51) != 13)
  {
    io_cerr << "writeHexXZValToStr (64, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  auxStr[13] = '\0';
  for (int i = 8; i < 13; ++i)
    auxStr[i] = '0';

  if (strcmp(valueStr, auxStr) != 0)
  {
    io_cerr << "writeHexXZValToStr (64, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeHexXZValToStr(valueStr, 100, valA, NULL, NULL, NULL, NULL, false, 80) != 20)
  {
    io_cerr << "writeHexXZValToStr (A, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  auxStr[20] = '\0';
  for (int i = 13; i < 20; ++i)
    auxStr[i] = '0';

  if (strcmp(valueStr, auxStr) != 0)
  {
    io_cerr << "writeHexXZValToStr (A, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeOctXZValToStr(valueStr, 100, &val8, NULL, NULL, NULL, NULL, false, 5) != 2)
  {
    io_cerr << "writeOctXZValToStr (8, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "00") != 0)
  {
    io_cerr << "writeOctXZValToStr (8, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeOctXZValToStr(valueStr, 100, &val16, NULL, NULL, NULL, NULL, false, 15) != 5)
  {
    io_cerr << "writeOctXZValToStr (16, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "00000") != 0)
  {
    io_cerr << "writeOctXZValToStr (16, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeOctXZValToStr(valueStr, 100, &val32, NULL, NULL, NULL, NULL, false, 29) != 10)
  {
    io_cerr << "writeOctXZValToStr (32, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  auxStr[10] = '\0';
  for (int i = 0; i < 10; ++i)
    auxStr[i] = '0';

  if (strcmp(valueStr, auxStr) != 0)
  {
    io_cerr << "writeOctXZValToStr (32, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }


  if (CarbonValRW::writeOctXZValToStr(valueStr, 100, &val64, NULL, NULL, NULL, NULL, false, 51) != 17)
  {
    io_cerr << "writeOctXZValToStr (64, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  auxStr[17] = '\0';
  for (int i = 10; i < 17; ++i)
    auxStr[i] = '0';

  if (strcmp(valueStr, auxStr) != 0)
  {
    io_cerr << "writeOctXZValToStr (64, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeOctXZValToStr(valueStr, 100, valA, NULL, NULL, NULL, NULL, false, 80) != 27)
  {
    io_cerr << "writeOctXZValToStr (A, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  auxStr[27] = '\0';
  for (int i = 17; i < 27; ++i)
    auxStr[i] = '0';

  if (strcmp(valueStr, auxStr) != 0)
  {
    io_cerr << "writeOctXZValToStr (A, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeDecXZValToStr(valueStr, 100, &val8, NULL, NULL, NULL, NULL, false, false, 5) != 1)
  {
    io_cerr << "writeDecXZValToStr (8, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "0") != 0)
  {
    io_cerr << "writeDecXZValToStr (8, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeDecXZValToStr(valueStr, 100, &val16, NULL, NULL, NULL, NULL, false, false, 15) != 1)
  {
    io_cerr << "writeDecXZValToStr (16, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "0") != 0)
  {
    io_cerr << "writeDecXZValToStr (16, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeDecXZValToStr(valueStr, 100, &val32, NULL, NULL, NULL, NULL, false, false, 29) != 1)
  {
    io_cerr << "writeDecXZValToStr (32, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "0") != 0)
  {
    io_cerr << "writeDecXZValToStr (32, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }


  if (CarbonValRW::writeDecXZValToStr(valueStr, 100, &val64, NULL, NULL, NULL, NULL, false, false, 51) != 1)
  {
    io_cerr << "writeDecXZValToStr (64, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "0") != 0)
  {
    io_cerr << "writeDecXZValToStr (64, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  if (CarbonValRW::writeDecXZValToStr(valueStr, 100, valA, NULL, NULL, NULL, NULL, false, false, 80) != 1)
  {
    io_cerr << "writeDecXZValToStr (A, NULL = drives = control = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "0") != 0)
  {
    io_cerr << "writeDecXZValToStr (A, NULL = drives = control = force), format wrong\n";
    stat = 1;
  }

  // test controlmask, no force, no drive
  // changing test for each type, to get more overall coverage without
  // making this test gi-hugic
  val8 = 0x30;
  // add an x to bit 3
  control1[0] = 0x8;
  control1[1] = 0x8;
  control1[2] = 0x8;
  if (CarbonValRW::writeBinXZValToStr(valueStr, 100, &val8, NULL, NULL, NULL, control1, false, 6) != 6)
  {
    io_cerr << "writeBinXZValToStr (8, control, NULL = drives = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "11x000") != 0)
  {
    io_cerr << "writeBinXZValToStr (8, control, NULL = drives = force), format wrong\n";
    stat = 1;

  }

  val16 = 0x3010;
  // add all z's to bits 0-3 for z
  // add a z to bit 6 for Z
  // add all x's to bits 8-11 for x
  // add an x to bit 12 for X
  control1[0] = 0x1f4f;
  control1[1] = 0x1f00;
  control1[2] = 0x1f4f;
  if (CarbonValRW::writeHexXZValToStr(valueStr, 100, &val16, NULL, NULL, NULL, control1, false, 16) != 4)
  {
    io_cerr << "writeHexXZValToStr (16, control, NULL = drives = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "XxZz") != 0)
  {
    io_cerr << "writeHexXZValToStr (16, control, NULL = drives = force), format wrong\n";
    stat = 1;

  }

  val32 = 0x03003003;
  // add all z to bit 3-5, z
  // add a z to bit 6, Z
  // add an x to bit 15, X
  // add z to bit 18-20, x
  control1[0] = 0x1c8078;
  control1[1] = 0x1c8000;
  control1[2] = 0x1c8078;
  if (CarbonValRW::writeOctXZValToStr(valueStr, 100, &val32, NULL, NULL, NULL, control1, false, 32) != 11)
  {
    io_cerr << "writeOctXZValToStr (16, control, NULL = drives = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "0030xX30Zz3") != 0)
  {
    io_cerr << "writeOctXZValToStr (16, control, NULL = drives = force), format wrong\n";
    stat = 1;

  }

  // make sure x overrides z
  control64[0] = 0x0;
  control64[1] = 0x3;
  control64[2] = 0x0;
  control64[3] = 0x1;
  control64[4] = 0x0;
  control64[5] = 0x3;
  if (CarbonValRW::writeDecXZValToStr(valueStr, 100, &val64, NULL, NULL, NULL, control64, false, false, 64) != 1)
  {
    io_cerr << "writeDecXZValToStr (64, control, NULL = drives = force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "X") != 0)
  {
    io_cerr << "writeDecXZValToStr (64, control, NULL = drives = force), format wrong\n";
    stat = 1;
  }

  // now, add drives and force into the mix
  // force bit 0, and make the xdrive and idrive undriven for that bit
  // force bit 1 and have idrive drive
  // bit 2 will be a constant x, but the value bit will be 1
  // bit 3 will be a 1.
  // bit 4 will be a 0
  // bit 5 will be a drive-calculated z, pulled up
  // bit 6 constant z, with both xdrive and idrive driving, and val 1
  // bit 7 will be forced to 1, and xdrive is driving.
  // bit 8 will be forced to 0, and constant x, and driven, force wins.
  // bit 9 will be 1
  controlA[0] = controlA[1] = controlA[2] = 0x144;
  controlA[3] = controlA[4] = controlA[5] = 0x104;
  controlA[6] = controlA[7] = controlA[8] = 0x144;
  xdriveA[0] = xdriveA[1] = xdriveA[2]    = 0x1a3;
  idriveA[0] = idriveA[1] = idriveA[2]    = 0x142;
  forceA[0] = forceA[1] = forceA[2]       = 0x183;
  valA[0] = valA[1] = valA[2]             = 0x2ae;
  // 101z101x10
  if (CarbonValRW::writeBinXZValToStr(valueStr, 100, valA, xdriveA, idriveA, forceA, controlA, true, 81) != 81)
  {
    io_cerr << "writeBinXZValToStr (A, control, drives, force), failed\n";
    stat = 1;
  }

  auxStr[81] = '\0';
  for (int i = 0; i < 81; ++i)
  {
    int wordbit = i % 32;
    switch(wordbit)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 9:
      auxStr[80 - i] = '1';
      break;
    case 2:
      auxStr[80 - i] = 'x';
      break;
    case 6:
      auxStr[80 - i] = 'z';
      break;
    default:
      auxStr[80 - i] = '0';
      break;
    }

  }

  
  if (strcmp(valueStr, auxStr) != 0)
  {
    io_cerr << "writeBinXZValToStr (A, control, drives, force), format wrong\n";
    stat = 1;
    
  }

  // All constants. Have the controlmask == all constants (the value
  // would already be set by the carbonnet creation
  // drives are all undriven. no force
  val8 = 0xf0;
  forceMask8 = 0;
  xdrive8 = 0xff;
  idrive8 = 0;
  control1[0] = 0xff;
  control1[1] = 0xf0;
  control1[2] = 0;
  if (CarbonValRW::writeHexXZValToStr(valueStr, 100, &val8, &xdrive8, &idrive8, &forceMask8, control1, false, 8) != 2)
  {
    io_cerr << "writeHexXZValToStr (8, control, drives, force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "f0") != 0)
  {
    io_cerr << "writeHexXZValToStr (8, control, drives, force), format wrong\n";
    stat = 1;
  }

  // controlMask/forceMask NULL, xdrive and idrive undriven, all z's.
  val16 = 0;
  xdrive16 = 0x1ff;
  idrive16 = 0;
  if (CarbonValRW::writeOctXZValToStr(valueStr, 100, &val16, &xdrive16, &idrive16, NULL, NULL, false, 9) != 3)
  {
    io_cerr << "writeOctXZValToStr (16, control, drives, force), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "zzz") != 0)
  {
    io_cerr << "writeOctXZValToStr (16, control, drives, force), format wrong\n";
    stat = 1;
  }

  // undriven, pulled, 1 constant bit, 1 forced, signed
  val32 = 0xffffffff;
  forceMask32 = 0x80000000;
  xdrive32 = 0xffffffff;
  idrive32 = 0;
  control1[0] = 0xffffffff;
  control1[1] = 0xffffffff;
  control1[2] = 0;
  if (CarbonValRW::writeDecXZValToStr(valueStr, 100, &val32, &xdrive32, &idrive32, &forceMask32, control1, true, true, 32) != 2)
  {
    io_cerr << "writeDecXZValToStr (32, control, drives, force), failed\n";
    stat = 1;
  }
  
  if (strcmp(valueStr, "-1") != 0)
  {
    io_cerr << "writeOctXZValToStr (32, control, drives, force), format wrong\n";
    stat = 1;
  }

  // test UInt32 usage with octal and bitwidth < 32 (used by constants
  // in the shell)
  control1[0] = 0x3ff;
  control1[1] = 0x3ff;
  control1[2] = 0x3ff;
  val32 = 0;
  if (CarbonValRW::writeOctXZValToStr(valueStr, 100, &val32, NULL, NULL, NULL, control1, false, 10) != 4)
  {
    io_cerr << "writeOctXZValToStr (10, control, 32 bit val), failed\n";
    stat = 1;
  }

  if (strcmp(valueStr, "xxxx") != 0)
  {
    io_cerr << "writeOctXZValToStr (16, control, drives, force), format wrong\n";
    stat = 1;
  }

  return stat;
}

static int sSetConstantBits()
{
  UtOStream& io_cerr = UtIO::cerr();
  int stat;

  UInt8 val8;
  UInt16 val16;
  UInt32 val32;
  UInt64 val64;
  UInt32 valA[3];
  UInt8 idrive8;
  UInt16 idrive16;
  UInt32 idrive32;
  UInt64 idrive64;
  UInt32 idriveA[3];
  UInt32 control[3];
  UInt32 control64[6];
  UInt32 controlA[9];

  CarbonValRW::setToZero(control, 3);
  CarbonValRW::setToZero(control64, 6);
  CarbonValRW::setToZero(controlA, 9);
  CarbonValRW::setToZero(idriveA, 3);
  CarbonValRW::setToZero(valA, 3);
  
  val8 = idrive8 = 0;
  val16 = idrive16 = 0;
  val32 = idrive32 = 0;
  val64 = idrive64 = 0;

  stat = 0;

  // set val8 to all 1s
  control[0] = 0xff;
  control[1] = 0xff;
  control[2] = 0;
  CarbonValRW::setConstantBits(&val8, &idrive8, control, 8);
  if ((val8 != 0xff) || (idrive8 != 0xff))
  {
    io_cerr << "setConstantBits (8) wrong\n";
    stat = 1;
  }
  

  // set val16 to have just 1 in bit 14, the last bit
  // set the idrive to have other driving bits
  control[0] = 0x4000;
  control[1] = 0x4000;
  control[2] = 0;
  idrive16 = 0x3fff;
  CarbonValRW::setConstantBits(&val16, &idrive16, control, 15);
  if ((val16 != 0x4000) || (idrive16 != 0x7fff))
  {
    io_cerr << "setConstantBits (16) wrong\n";
    stat = 1;
  }

  // set val32 to have no set bits, so put some x/z's in control
  control[0] = 0xffff0000;
  control[1] = 0xf00f0000;
  control[2] = 0xffff0000;
  CarbonValRW::setConstantBits(&val32, &idrive32, control, 32);
  if ((val32 != 0) || (idrive32 != 0))
  {
    io_cerr << "setConstantBits (32) wrong\n";
    stat = 1;
  }

  // clear all bits in the upper half of the 64 bit - 1 (use only 63
  // bits). 
  val64 = KUInt64(0x7fffffff00000000);
  control64[0] = 0;
  control64[1] = 0x7fffffff;
  CarbonValRW::setConstantBits(&val64, &idrive64, control64, 63);
  if ((val64 != 0) || (idrive64 != KUInt64(0x7fffffff00000000)))
  {
    io_cerr << "setConstantBits (64) wrong\n";
    stat = 1;
  }
  
  // set all the bits except 3, 1 is z, another is x, and another is
  // live, 77 bits
  controlA[0] = 0xfffffffe;
  controlA[1] = 0xffffffff;
  controlA[2] = 0x1fff;
  controlA[3] = 0xfffffffe;
  controlA[4] = 0xefffffff;
  controlA[5] = 0x1fff;
  controlA[6] = 0x0;
  controlA[7] = 0x10000000;
  controlA[8] = 0x1000;
  valA[0] = 1; // live bit
  idriveA[0] = 1;
  CarbonValRW::setConstantBits(valA, idriveA, controlA, 77);

  bool isGood = valA[0] == 0xffffffff;
  isGood &= valA[1] == 0xefffffff;
  isGood &= valA[2] == 0xfff;
  isGood &= idriveA[0] == 0xffffffff;
  isGood &= idriveA[1] == 0xefffffff;
  isGood &= idriveA[2] == 0xfff;
  if (! isGood)
  {
    io_cerr << "setConstantBits (A) wrong\n";
    stat = 1;
  }
  
  return stat;
}

static int sTestStrToVal()
{
  const char* str1 = "x";
  UInt32 data[2];
  UInt32 mask[2];
  memset(data, 0xff, sizeof(UInt32) * 2);
  memset(mask, 0xff, sizeof(UInt32) * 2);
  
  int stat = 0;
  UtOStream& io_cerr = UtIO::cerr();
  int truncStat = -2;
  if (! UtConv::BinStrToUInt32Fit(str1, data, mask, 33, &truncStat) ||
      (truncStat != 1)) {
    io_cerr << "Failed BinStrToUInt32Fit call" << UtIO::endl;
    stat = 1;
  }
  
  if (stat == 0)
  {
    if ((data[0] != 0) || (data[1] != 0) || 
        (mask[0] != 0) || (mask[1] != 0))
    {
      io_cerr << "Failed BinStrToUInt32Fit x-extend" << UtIO::endl;
      stat = 1;
    }
    
  }
  
  const char* str2 = "z";
  memset(data, 0xff, sizeof(UInt32) * 2);
  memset(mask, 0xff, sizeof(UInt32) * 2);

  truncStat = -2;
  if (! UtConv::BinStrToUInt32Fit(str2, data, mask, 33, &truncStat) ||
      (truncStat != 1)) {
    io_cerr << "Failed BinStrToUInt32Fit call 2" << UtIO::endl;
    stat = 1;
  }


  if (stat == 0) 
  {
    if ((data[0] != 0) || (data[1] != 0) || 
        (mask[0] != 0) || (mask[1] != 0))
    {
      io_cerr << "Failed BinStrToUInt32Fit z-extend" << UtIO::endl;
      stat = 1;
    }
  }

  // truncate
  memset(data, 0, sizeof(UInt32) * 2);
  memset(mask, 0, sizeof(UInt32) * 2);
  
  const char* str3 = "1111_0000";
  truncStat = -2;
  if (! UtConv::BinStrToUInt32Fit(str3, data, mask, 4, &truncStat) || (truncStat != -1)) {
    io_cerr << "Failed BinStrToUInt32Fit call 3" << UtIO::endl;
    stat = 1;
  }
  

  if (stat == 0) 
  {
    if ((data[0] != 0) || (mask[0] != 0xf))
    {
      io_cerr << "Failed BinStrToUInt32Fit truncate" << UtIO::endl;
      stat = 1;
    }
  }

  memset(data, 0xff, sizeof(UInt32) * 2);
  memset(mask, 0xff, sizeof(UInt32) * 2);
  truncStat = -2;
  if (! UtConv::HexStrToUInt32Fit(str1, data, mask, 33, &truncStat) ||
      (truncStat != 1)) {
    io_cerr << "Failed HexStrToUInt32Fit call" << UtIO::endl;
    stat = 1;
  }
  
  if (stat == 0)
  {
    if ((data[0] != 0) || (data[1] != 0) || 
        (mask[0] != 0) || (mask[1] != 0))
    {
      io_cerr << "Failed HexStrToUInt32Fit x-extend" << UtIO::endl;
      stat = 1;
    }
    
  }
  
  memset(data, 0xff, sizeof(UInt32) * 2);
  memset(mask, 0xff, sizeof(UInt32) * 2);

  truncStat = -2;
  if (! UtConv::HexStrToUInt32Fit(str2, data, mask, 33, &truncStat) ||
      (truncStat != 1)) {
    io_cerr << "Failed HexStrToUInt32Fit call 2" << UtIO::endl;
    stat = 1;
  }


  if (stat == 0) 
  {
    if ((data[0] != 0) || (data[1] != 0) || 
        (mask[0] != 0) || (mask[1] != 0))
    {
      io_cerr << "Failed HexStrToUInt32Fit z-extend" << UtIO::endl;
      stat = 1;
    }
  }

  // truncate
  memset(data, 0, sizeof(UInt32) * 2);
  memset(mask, 0, sizeof(UInt32) * 2);
  
  str3 = "F_0";
  truncStat = -2;
  if (! UtConv::HexStrToUInt32Fit(str3, data, mask, 4, &truncStat) || (truncStat != -1)) {
    io_cerr << "Failed HexStrToUInt32Fit call 3" << UtIO::endl;
    stat = 1;
  }
  

  if (stat == 0) 
  {
    if ((data[0] != 0) || (mask[0] != 0xf))
    {
      io_cerr << "Failed HexStrToUInt32Fit truncate" << UtIO::endl;
      stat = 1;
    }
  }
  
  return stat;
}

static int sTestRangeCp()
{
  UtOStream& io_cerr = UtIO::cerr();

  int stat = 0;
  UInt32 src[3];
  UInt32 dest[3];
  dest[0] = 0xf0f0f0f0;
  src[2] = 0xf0f0f0f0;
  dest[1] = 0xffffffff;
  src[0] = 0xffffffff;
  dest[2] = 0;
  src[1] = 0;

  CarbonValRW::copyRange(dest, src, 28, 4);
  if ((dest[0] != 0xf0f0f0f0) ||
      (dest[1] != 0xffffffff) ||
      (dest[2] != 0))
  {
    io_cerr << "Copy Range 1 failed.\n";
    stat = 1;
  }

  CarbonValRW::copyRange(dest, src, 32, 16);
  if ((dest[0] != 0xf0f0f0f0) ||
      (dest[1] != 0xffff0000) ||
      (dest[2] != 0))
  {
    io_cerr << "Copy Range 2 failed.\n";
    stat = 1;
  }

  CarbonValRW::copyRange(dest, src, 48, 1);
  if ((dest[0] != 0xf0f0f0f0) ||
      (dest[1] != 0xfffe0000) ||
      (dest[2] != 0x0))
  {
    io_cerr << "Copy Range 3 failed.\n";
    stat = 1;
  }

  CarbonValRW::copyRange(dest, src, 62, 10);
  if ((dest[0] != 0xf0f0f0f0) ||
      (dest[1] != 0x3ffe0000) ||
      (dest[2] != 0xf0))
  {
    io_cerr << "Copy Range 4 failed.\n";
    stat = 1;
  }

  dest[0] = 0xffffffff;
  dest[1] = 0xffffffff;
  dest[2] = 0xffffffff;
  src[0] = 0;
  src[1] = 0;
  src[2] = 0;

  CarbonValRW::copyRange(dest, src, 1, 94);
  if ((dest[0] != 1) ||
      (dest[1] != 0) ||
      (dest[2] != 0x80000000))
  {
    io_cerr << "Copy Range 5 failed.\n";
    stat = 1;
  }

  // cpSrcToDestRange
  UInt32 in32v = 0xff;
  UInt32 in32 = 0;
  CarbonValRW::cpSrcToDestRange(&in32, &in32v, 10, 9);
  if (in32 != 0x3fc00)
  {
    io_cerr << "cpSrcToDestRange 1 failed.\n";
    stat = 1;
  }

  UInt32 in72v[3];
  UInt32 in72[3];
  in72v[2] = 0;
  in72v[1] = 0x0f0f0f0f;
  in72v[0] = 0xf;
  in72[2] = 0;
  in72[1] = 0x0f0f0f0f;
  in72[0] = 0;
  CarbonValRW::cpSrcToDestRange(in72, in72v, 28, 4);
  if ((in72[0] != 0xf0000000) || (in72[1] != 0x0f0f0f0f) ||
      (in72[2] != 0))
  {
    io_cerr << "cpSrcToDestRange 2 failed.\n";
    stat = 1;
  }
  
  UInt32 tmp = 1;
  CarbonValRW::cpSrcToDestRange(in72, &tmp, 71, 1);
  if ((in72[2] != 0x80) || (in72[1] != 0x0f0f0f0f) ||
      (in72[0] != 0xf0000000))
  {
    io_cerr << "cpSrcToDestRange 3 failed.\n";
    stat = 1;
  }
  
  UInt32 en72[3];
  UInt32 en72v[3];
  en72v[0] = 0xffffffff;
  en72v[1] = 0xffffffff;
  en72v[2] = 0x9f;
  memset (en72, 0, 3 * sizeof(UInt32));
  CarbonValRW::cpSrcToDestRange(en72, en72v, 0, 72);
  if ((en72[0] != en72v[0]) ||
      (en72[1] != en72v[1]) ||
      (en72[2] != en72v[2]))
  {
    io_cerr << "cpSrcToDestRange 4 failed.\n";
    stat = 1;
  }

  return stat;
}

static int sTestBinaryStrToHex()
{
  int stat = 0;
  UtOStream& io_cerr = UtIO::cerr();

  const char* str1 = "1";
  const char* str2 = "x";
  const char* str3 = "x1";
  const char* str4 = "0z";
  const char* str5 = "11010001";
  const char* str6 = "00001111";
  const char* str7 = "101010101";
  
  UtString tmp = str1;
  if (! UtConv::BinaryStrToHex(&tmp) || (tmp.compare(str1) != 0))
  {
    io_cerr << "UtConv::BinaryStrToHex str1 failed\n";
    stat = 1;
  }
  
  // should fail
  tmp = str2;
  if (UtConv::BinaryStrToHex(&tmp) || (tmp.compare(str2) != 0))
  {
    io_cerr << "UtConv::BinaryStrToHex str2 failed\n";
    stat = 1;
  }
  
  tmp = str3;
  if (UtConv::BinaryStrToHex(&tmp) || (tmp.compare(str3) != 0))
  {
    io_cerr << "UtConv::BinaryStrToHex str3 failed\n";
    stat = 1;
  }

  tmp = str4;
  if (UtConv::BinaryStrToHex(&tmp) || (tmp.compare(str4) != 0))
  {
    io_cerr << "UtConv::BinaryStrToHex str4 failed\n";
    stat = 1;
  }

  // should pass
  tmp = str5;
  if (! UtConv::BinaryStrToHex(&tmp) || (tmp.compare("d1") != 0))
  {
    io_cerr << "UtConv::BinaryStrToHex str5 failed\n";
    stat = 1;
  }

  tmp = str6;
  if (! UtConv::BinaryStrToHex(&tmp) || (tmp.compare("0f") != 0))
  {
    io_cerr << "UtConv::BinaryStrToHex str6 failed\n";
    stat = 1;
  }

  tmp = str7;
  if (! UtConv::BinaryStrToHex(&tmp) || (tmp.compare("155") != 0))
  {
    io_cerr << "UtConv::BinaryStrToHex str7 failed\n";
    stat = 1;
  }
  
  return stat;
}

#define FAIL(stmt) \
  do {io_cerr << stmt; stat = 1;} while (0)
 
static int sTestRangeToRangeCp()
{
  int stat = 0;
  UtOStream& io_cerr = UtIO::cerr();

  DynBitVector allones(160);
  allones.set();
  
  DynBitVector dst1(160);
  dst1 = 0;

  UInt32* dst1Arr = dst1.getUIntArray();
  UInt32* allonesArr = allones.getUIntArray();

  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 33, allonesArr, 28, 65);

  if (dst1.partsel(33, 65) != allones.partsel(28, 65))
    FAIL("cpSrcRangeToDestRange failed 1\n");

  dst1 = 0;
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 62, allonesArr, 1, 70);
  if (dst1.partsel(62,70) != allones.partsel(1, 70))
    FAIL("cpSrcRangeToDestRange failed 2\n");

  dst1 = 0;
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 62, allonesArr, 0, 70);
  if (dst1.partsel(62,70) != allones.partsel(0, 70))
    FAIL("cpSrcRangeToDestRange failed 3\n");

  dst1 = allones;

  DynBitVector src1(160);
  src1 = 0;
  UInt32* src1Arr = src1.getUIntArray();
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 33, src1Arr, 28, 65);
  if (dst1.partsel(33, 65) != src1.partsel(28, 65))
    FAIL("cpSrcRangeToDestRange failed 4\n");
  
  dst1 = allones;
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 62, src1Arr, 1, 70);
  if (dst1.partsel(62,70) != src1.partsel(1, 70))
    FAIL("cpSrcRangeToDestRange failed 5\n");

  dst1 = allones;
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 62, src1Arr, 0, 70);
  if (dst1.partsel(62,70) != src1.partsel(0, 70))
    FAIL("cpSrcRangeToDestRange failed 6\n");

  dst1 = 0;
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 62, allonesArr, 1, 5);
  if (dst1.partsel(62,5) != allones.partsel(1, 5))
    FAIL("cpSrcRangeToDestRange failed 7\n");
  
  dst1 = 0;
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 33, allonesArr, 28, 5);
  if (dst1.partsel(33,5) != allones.partsel(28, 5))
    FAIL("cpSrcRangeToDestRange failed 8\n");
  
  dst1 = allones;
  src1 = 0;
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 62, src1Arr, 1, 5);
  if (dst1.partsel(62,5) != src1.partsel(1, 5))
    FAIL("cpSrcRangeToDestRange failed 9\n");
  
  dst1 = allones;
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 33, src1Arr, 28, 5);
  if (dst1.partsel(33,5) != src1.partsel(28, 5))
    FAIL("cpSrcRangeToDestRange failed 10\n");
  
  
  dst1 = 0;
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 33, allonesArr, 65, 5);
  if (dst1.partsel(33,5) != allones.partsel(65, 5))
    FAIL("cpSrcRangeToDestRange failed 11\n");

  dst1 = allones;
  src1 = 0;
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 62, src1Arr, 62, 5);
  if (dst1.partsel(62,5) != src1.partsel(62, 5))
    FAIL("cpSrcRangeToDestRange failed 12\n");

  dst1 = allones;
  src1 = 0;
  src1.setRange(44, 100, 1);
  dst1.setRange(28, 100, 0);
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 28, src1Arr, 44, 100);
  // dst1 should be all ones now.
  if (dst1 != allones)
    FAIL("cpSrcRangeToDestRange failed 13\n");
  
  dst1 = allones;
  src1 = 0; 

  src1.setRange(50, 100, 1);
  dst1.setRange(17, 100, 0);
  CarbonValRW::cpSrcRangeToDestRange(dst1Arr, 17, src1Arr, 50, 100);
  // dst1 should be all ones now.
  if (dst1 != allones)
    FAIL("cpSrcRangeToDestRange failed 14\n");
 
  return stat;
}

static void test(size_t nbytes, const char* msg) {
  if (nbytes != 0) {
    UtIO::cout() << msg << ' ' << nbytes << UtIO::endl;
    assert(0);
  }
}

extern int utconvtest(void)
{
  // Do things that cause memory to be allocated.
  UtIO::cout();
  UtIO::cerr();
  size_t start = CarbonMem::getBytesAllocated();

  int stat = 0;
  stat = sTestValToFormatStr();
  test(CarbonMem::getBytesAllocated() - start, "sTestValToFormatStr");
  stat |= sTestValXZToFormatStr();
  test(CarbonMem::getBytesAllocated() - start, "sTestValXZToFormatStr");
  stat |= sSetConstantBits();
  test(CarbonMem::getBytesAllocated() - start, "sSetConstantBits");
  stat |= sTestStrToVal();
  test(CarbonMem::getBytesAllocated() - start, "sTestStrToVal");
  stat |= sTestBinaryStrToHex();
  test(CarbonMem::getBytesAllocated() - start, "sTestBinaryStrToHex");
  stat |= sTestRangeCp();
  test(CarbonMem::getBytesAllocated() - start, "sTestRangeCp");
  stat |= sTestRangeToRangeCp();
  test(CarbonMem::getBytesAllocated() - start, "sTestRangeToRangeCp");
  return stat;
}
