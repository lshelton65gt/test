// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
// test the memory management subsystem

#include "util/UtWildcard.h"
#include "util/UtIOStream.h"

static int err(const char* msg)
{
  UtIO::cout() << msg << UtIO::endl;
  return 1;
}

int wildcardtest()
{
  int ret = 0;
  UtWildcard w1("abc*");
  if (w1.isMatch("def"))
    ret += err("abc* matches def");
  if (!w1.isMatch("abcdef"))
    ret += err("abc* !matches abcdef");
  if (!w1.isMatch("abc"))
    ret += err("abc* !matches abc");

  UtWildcard w2("*abc");
  if (w2.isMatch("def"))
    ret += err("*abc matches def");
  if (w2.isMatch("abcdef"))
    ret += err("*abc matches abcdef");
  if (!w2.isMatch("abc"))
    ret += err("*abc !matches abc");
  if (!w2.isMatch("yomama_abc"))
    ret += err("*abc !matches yomama_abc");

  UtWildcard w3("abc*?");
  if (w3.isMatch("abc"))
    ret += err("abc*? matches abc");
  if (!w3.isMatch("abcd"))
    ret += err("abc*? !matches abcd");
  if (!w3.isMatch("abcdefghifhiojads"))
    ret += err("abc*? !matches abcdoijasdioj");

  UtWildcard w4("abc*?def");
  if (w4.isMatch("abc"))
    ret += err("abc*?def matches abc");
  if (w4.isMatch("abcd"))
    ret += err("abc*?def matches abcd");
  if (w4.isMatch("abcdef"))
    ret += err("abc*?def matches abcdef");
  if (!w4.isMatch("abcXdef"))
    ret += err("abc*?def !matches abcXdef");
  if (!w4.isMatch("abcX09870987def"))
    ret += err("abc*?def !matches abcX09870987def");
  if (!w4.isMatch("abcX098def70987def"))
    ret += err("abc*?def !matches abcX098def70987def");
  if (!w4.isMatch("abcX098de70987def"))
    ret += err("abc*?def !matches abcX098de70987def");

  return ret;
} // int wildcardtest
