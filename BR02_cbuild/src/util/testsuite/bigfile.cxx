//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// Write a 5 gig file to make sure that works

#include "util/UtIOStream.h"
#include "util/UtIStream.h"

int bigfiletest(int argc, char** argv) {
  UInt64 five_billion = 5*1000*1000;
  five_billion *= 1000;

  static const char buf[] = "Now is the time for all good men to come to the aid of their country\n";
  static const int len = sizeof(buf) - 1;        // 70

  UInt64 write_bytes = 0;
  SInt32 log_interval = 500*1000*1000;

  UtOStream& outfile = UtIO::cout();

  for (int i = 1; i < argc; ++i) {
    UtOBStream bigfile(argv[i], "w", 10*1000*1000);

    SInt32 log_count = log_interval;
    while (write_bytes < five_billion) {
      if (!bigfile.write((char*) buf, len)) {
        outfile << bigfile.getErrmsg() << "\n";
        return 1;
      }
      write_bytes += len;
      log_count -= len;

      if (log_count <= 0) {
        outfile << "Wrote " << write_bytes << " to " << argv[i] << "\n";
        outfile.flush();
        log_count = log_interval;
      }
    }

    if (!bigfile.close()) {
      outfile << bigfile.getErrmsg() << "\n";
      return 1;
    }
  }

  for (int i = 1; i < argc; ++i) {
    UtIBStream bigfile(argv[i], 10*1000*1000);

    char inbuf[len + 1];
    inbuf[len] = '\0';

    UInt32 bytes;
    UInt64 read_bytes = 0;
    SInt32 log_count = log_interval;
    while ((bytes = bigfile.read(inbuf, len)) > 0) {
      read_bytes += bytes;

      log_count -= bytes;
      if (log_count <= 0) {
        outfile << "Read " << read_bytes << " from " << argv[i] << "\n";
        outfile.flush();
        log_count = log_interval;
      }

      if (strcmp(inbuf, buf) != 0) {
        outfile << "wrong data: " << buf << " != " << inbuf << "\n";
        return 1;
      }
    }

    if (read_bytes != write_bytes) {
      outfile << "wrong byte-count: " << write_bytes << " != "
                   << read_bytes << "\n";
      return 1;
    }

    if (!bigfile.close()) {
      outfile << bigfile.getErrmsg() << "\n";
      return 1;
    }
  }
  return 0;
} // int bigfiletest
