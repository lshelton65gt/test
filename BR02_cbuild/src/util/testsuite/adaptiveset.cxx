// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtStream.h"
#include "util/UtString.h"
#include "util/UtIOStream.h"
#include "util/UtSet.h"
#include "util/UtHashSet.h"

// please do not use "using namespace std"


// Compares absolute value
struct AbsIntCmp {
  size_t hash(int var) const {return std::abs(var);}
  bool lessThan(int v1, int v2) const {return std::abs(v1) < std::abs(v2);}
  bool lessThan1(int v1, int v2) const {return std::abs(v1) < std::abs(v2);}
  bool equal(int v1, int v2) const {
    return (v1 == v2) || (-v1 == v2);
  }
};

struct StdIntLessThan {
  bool operator()(int i1, int i2) const {return std::abs(i1) < std::abs(i2);}
};

typedef UtHashSet<int, AbsIntCmp> AbsIntSet;
typedef UtSet<int, StdIntLessThan> AbsIntStdSet;


// checks to see that AbsIntSet and AbsIntStdSet contain the same values.
static bool adaptiveIntSetTestConsistent( AbsIntSet hfib, AbsIntStdSet sfib, int call )
{
  int status = 0;
  AbsIntStdSet::iterator sp = sfib.begin();
  AbsIntStdSet::iterator se = sfib.end();
  AbsIntSet::SortedLoop hp = hfib.loopSorted();
  for (; !hp.atEnd() && (sp != se); ++hp, ++sp)
  {
    int hashVal = *hp;
    if (hashVal != *sp)
    {
      UtIO::cout() << "call " << call << ": Mismatch between set and UtHashSet: hash=" << hashVal << ", set=" << *sp << UtIO::endl;
      status = 1;
    }
    UtIO::cout() << *hp << " ";
  }
  UtIO::cout() << UtIO::endl;
  if (!hp.atEnd())
  {
    UtIO::cout() << "call " << call << ": Unexpected end of set" << UtIO::endl;
    status = 1;
  }
  else if (sp != se)
  {
    UtIO::cout() << "call " << call << ": Unexpected end of UtHashSet" << UtIO::endl;
    status = 1;
  }
  return (status ? true : false );
}

// check to see that the insertWithCheck method works fine with AbsIntSet, returns true if there was an error
// existVal must be in the set already, newVal must not be in the set
static bool adaptiveIntSetTestInsertWithCheckTest ( AbsIntSet* phfib, AbsIntStdSet* psfib, int existVal, int newVal, int call )
{
  int status = 0;

  // now check to see if insertWithCheck works

  // attempt to add something that should already be in set
  if ( phfib->insertWithCheck(existVal) )
  {
    UtIO::cout() << "call " << call << ": insertWithCheck returned true (expected false) when set already contained inserted value:" << existVal << UtIO::endl;
    status = 1;
  }

  // attempt to add something that is not in set (using insertWithCheck)
  if ( ! phfib->insertWithCheck(newVal) )
  {
    UtIO::cout() << "call " << call << ": insertWithCheck returned false (expected true) when set did not contain inserted value" << UtIO::endl;
    status = 1;
  }
  psfib->insert(newVal);		// keep *phfib and *psfib in sync

  // try to add a value that was inserted with insertWithCheck, should fail.
  if ( phfib->insertWithCheck(newVal) )
  {
    UtIO::cout() << "call " << call << ": insertWithCheck returned true (expected false) when set should already contain inserted value" << UtIO::endl;
    status = 1;
  }
  return ( status ? true : false );
}
static int adaptiveIntSetTestSizeTest ( AbsIntSet* phfib, AbsIntStdSet* psfib, size_t desiredSize, int call )
{
  int status = 0;

  if ( ( phfib->size() != desiredSize ) || ( psfib->size() != desiredSize ) )
  {
    UtIO::cout() << "call " << call << ": Size test failed, hfib.size: " << phfib->size() << " and sfib.size: " << psfib->size() << UtIO::endl;
    status = 1;
  }
  return ( status ? true : false );
}


static int adaptiveIntSetTest()
{
  int status = 0;
  AbsIntSet hfib;
  AbsIntStdSet sfib;
  int x;
  int xprev;
  int xtemp;

  x = -2;  hfib.insert(x);  sfib.insert(x);
  x = 5;  hfib.insert(x);  sfib.insert(x);
  x = -3;  hfib.insert(x);  sfib.insert(x);
  x = 13; hfib.insert(x);  sfib.insert(x);
  x = 8;  hfib.insert(x);  sfib.insert(x);
  x = 8;  hfib.insert(x);  sfib.insert(x); // duplicate
  x = -8;  hfib.insert(x);  sfib.insert(x); // duplicate a second time
  x = 2;  hfib.insert(x);  sfib.insert(x); // duplicate


  if ( adaptiveIntSetTestSizeTest ( &hfib, &sfib, 5, 1 ) )
  {
    status = 1;
  }

  //  why is this not AbsIntStdSet, if we  use AbsIntSet then we could
  // miss an error due to common mode problem
  AbsIntSet cpy;
  // 
  cpy.insert(hfib.begin(), hfib.end());
  if (cpy != hfib)
  {
    UtIO::cout() << "Mismatch between UtHashSet and copy" << UtIO::endl;
    status = 1;
  }
  cpy.insert(sfib.begin(), sfib.end()); // should do nothing.
  if (cpy != hfib)
  {
    UtIO::cout() << "Mismatch between UtHashSet and copy" << UtIO::endl;
    status = 1;
  }

  if ( adaptiveIntSetTestConsistent( hfib, sfib, 1 ) )
  {
    status = 1;
  }

  if ( adaptiveIntSetTestInsertWithCheckTest ( &hfib, &sfib, 2, 200, 1 ) )
  {
    status = 1;
  }

  // add enough to the adaptive set so that it converts over to a hash table
  for ( xprev = 8, x = 13; x < 50000; xtemp = xprev + x, xprev = x, x = xtemp )
  {
    hfib.insert(x);  sfib.insert(x);
  }


  if ( adaptiveIntSetTestInsertWithCheckTest ( &hfib, &sfib, 200, 201, 2 ) )
  {
    status = 1;
  }

  if ( adaptiveIntSetTestSizeTest ( &hfib, &sfib, 24, 2 ) )
  {
    status = 1;
  }

  if ( adaptiveIntSetTestConsistent( hfib, sfib, 2 ) )
  {
    status = 1;
  }

  return status;
} // static int adaptiveIntSetTest

static int adaptiveComplexCopies = 0;

// Test that we are able to loop through a set of
// complex numbers values without copy-constructing them.
class AdaptiveComplex
{
public:
  AdaptiveComplex(double real, double imag): mReal(real), mImag(imag) {}
  ~AdaptiveComplex() {}
  static ptrdiff_t compare(const AdaptiveComplex* c1, const AdaptiveComplex* c2) {
    int cmp = 0;
    if (c1->mReal < c2->mReal)
      cmp = -1;
    else if (c1->mReal > c2->mReal)
      cmp = 1;
    else if (c1->mImag < c2->mImag)
      cmp = -1;
    else if (c1->mImag > c2->mImag)
      cmp = 1;
    return cmp;
  }
  bool operator==(const AdaptiveComplex& other) const {
    return ((mReal == other.mReal) && (mImag == other.mImag));
  }
  bool operator<(const AdaptiveComplex& other) const {
    return compare(this, &other) < 0;
  }


  AdaptiveComplex(const AdaptiveComplex& src): mReal(src.mReal), mImag(src.mImag) {
    ++adaptiveComplexCopies;
  }
  AdaptiveComplex& operator=(const AdaptiveComplex& src)
  {
    if (&src != this)
    {
      ++adaptiveComplexCopies;
      mReal = src.mReal;
      mImag = src.mImag;
    }
    return *this;
  }

  void print() const {
    UtIO::cout() << "(" << mReal << "," << mImag << ")" << UtIO::endl;
  }

  size_t hash() const {return ((int) mReal) + ((int) mImag);}
private:
  double mReal;
  double mImag;
}; // class AdaptiveComplex
      
typedef UtHashSet<AdaptiveComplex, HashValue<AdaptiveComplex> > AdaptiveComplexSet;

static int adaptiveComplexSetTest() {
  AdaptiveComplexSet c;
  c.insert(AdaptiveComplex(1, 0));
  c.insert(AdaptiveComplex(1, 2));
  c.insert(AdaptiveComplex(2, 1));
  c.insert(AdaptiveComplex(2, 3));

  // This loop will use a copy-constructor & probably some temps
  int copies = adaptiveComplexCopies;
  int ret = 0;
  for (AdaptiveComplexSet::SortedLoop loop = c.loopSorted();
       !loop.atEnd(); ++loop)
  {
    const AdaptiveComplex& cx = *loop;
    cx.print();
  }
  if (copies != adaptiveComplexCopies)
  {
    ++ret;
    UtIO::cout() << "Complex loop1: copies = " << copies << ", adaptiveComplexCopies = " << adaptiveComplexCopies << UtIO::endl;
  }

  // This loop should use no copy constructors
  copies = adaptiveComplexCopies = 0;
  AdaptiveComplexSet::SortedLoop loop = c.loopSorted();
  for (const AdaptiveComplex* cptr; loop(&cptr);)
    cptr->print();
  if (copies != adaptiveComplexCopies)
  {
    ++ret;
    UtIO::cout() << "Complex loop2: Expected no copy constructors, got " << adaptiveComplexCopies << UtIO::endl;
  }


  // This loop is unsorted, and should also use no copy ctors
  copies = adaptiveComplexCopies = 0;
  AdaptiveComplexSet::UnsortedLoop uloop = c.loopUnsorted();
  for (const AdaptiveComplex* cptr; uloop(&cptr);)
    cptr->print();

  if (copies != adaptiveComplexCopies)
  {
    ++ret;
    UtIO::cout() << "Complex loop3: Expected no copy constructors, got " << adaptiveComplexCopies << UtIO::endl;
  }

  return ret;
}

int adaptivesettest(int /*argc*/, char* /*argv*/[])
{
  int status = adaptiveIntSetTest();
  status += adaptiveComplexSetTest();
  return status;
}
