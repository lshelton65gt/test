// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtString.h"
#include "util/UtIOStream.h"
#include "util/MemManager.h"


extern int argproctest(int argc, char* argv[]);
extern int ztest(int argc, char* argv[]);
extern int memorypooltest(int /*argc*/, char* /*argv*/[]);
extern int randomvalgentest(int /*argc*/, char* /*argv*/[]);
extern int hashsettest(int /*argc*/, char* /*argv*/[]);
extern int hashsetfastlooptest(int /*argc*/, char* /*argv*/[]);
extern int adaptivesettest(int /*argc*/, char* /*argv*/[]);
extern int testStrings(int /*argc*/, char* /*argv*/[]);
extern int bvperf();
extern int miscperf();
extern int memtest(int argc, char** argv, bool);
extern int memhist(int argc, char** argv);
extern int utconvtest();
extern int wildcardtest();
extern int oswraptest();
extern int testZstreamZip();
extern int shelltoktest();
extern int testStreamTokenizer();
extern int bigfiletest(int argc, char** argv);
extern int streamtest();
extern int istreamtest();
extern int iseektest();
extern int istreamerr();
extern int testarray();
extern int dbvdesctest();
extern int testUtHashMap();
extern int testUtHashMap2();
extern int testsourceloc();
extern int printdec(int argc, char** argv);
extern int bigraphtest(int argc, char** argv);
extern int graphmergetest(int argc, char** argv);
extern int disjointtest();
extern int queuetest();
extern int datatoobjtest(int argc, char** argv);
extern int utdllisttest();
extern int xfertest();
extern int serialize();
extern int oswrap64BitModels();

// This test always returns ok, but it creates 2 file that need to be
// diffed.
extern int ostreamtest();

#ifdef USE_TEMPLATES
extern int bvtest(void);
extern int sbvtest(void);
# else
# ifdef USE_DYNAMIC
extern int bvtest(void);
extern int sbvtest(void);
# else
extern int dbvtest(void);
extern int tbvtest(void);
extern int dsbvtest(void);
extern int tsbvtest(void);
# define BVTEST_BOTH
# endif
#endif

#include "codegen/carbon_priv.h"
UInt32 gCarbonMemoryBitbucket[1] COMMON;

// Driver program that does all the utils tests
int main(int argc, char** argv)
{
  CarbonMem memContext(&argc, argv, getenv("CARBON_UTILTEST_DUMPFILE"));

  int ret = 1;
  if (argc > 1)
  {
    for (int i = 1; i < argc; ++i)
      argv[i - 1] = argv[i];
    --argc;

    if (strcmp(argv[0], "argproctest") == 0)
      ret = argproctest(argc, argv);
    else if (strcmp(argv[0], "ztest") == 0)
      ret = ztest(argc, argv);
    else if (strcmp(argv[0], "memorypooltest") == 0)
      ret = memorypooltest(argc, argv);
    else if (strcmp(argv[0], "randomvalgentest") == 0)
      ret = randomvalgentest(argc, argv);
    else if (strcmp(argv[0], "hashsettest") == 0)
      ret = hashsettest(argc, argv);
    else if (strcmp(argv[0], "hashsetfastlooptest") == 0)
      ret = hashsetfastlooptest(argc, argv);
    else if (strcmp(argv[0], "adaptivesettest") == 0)
      ret = adaptivesettest(argc, argv);
    else if (strcmp(argv[0], "strings") == 0)
      ret = testStrings(argc, argv);
    else if (strcmp(argv[0], "memtest") == 0)
      ret = memtest(argc, argv, true);
    else if (strcmp(argv[0], "memhist") == 0)
      ret = memhist(argc, argv);
    else if (strcmp(argv[0], "wildcardtest") == 0)
      ret = wildcardtest();
    else if (strcmp(argv[0], "utconvtest") == 0)
      ret = utconvtest();
    else if (strcmp(argv[0], "bvperf") == 0)
      ret = bvperf();
    else if (strcmp(argv[0], "miscperf") == 0)
      ret = miscperf();
    else if (strcmp(argv[0], "oswraptest") == 0)
      ret = oswraptest();
    else if (strcmp(argv[0], "ostreamtest") == 0)
      ret = ostreamtest();
    else if (strcmp(argv[0], "zstreamziptest") == 0)
      ret = testZstreamZip();
    else if (strcmp(argv[0], "shelltoktest") == 0)
      ret = shelltoktest();
    else if (strcmp(argv[0], "testStreamTokenizer") == 0)
      ret = testStreamTokenizer();
    else if (strcmp(argv[0], "streamtest") == 0)
      ret = streamtest();
    else if (strcmp(argv[0], "bigfiletest") == 0)
      ret = bigfiletest(argc, argv);
    else if (strcmp(argv[0], "istreamtest") == 0)
      ret = istreamtest();
    else if (strcmp(argv[0], "iseektest") == 0)
      ret = iseektest();
    else if (strcmp(argv[0], "istreamerr") == 0)
      ret = istreamerr();
    else if (strcmp(argv[0], "arraytest") == 0)
      ret = testarray();
    else if (strcmp(argv[0], "sourceloc") == 0)
      ret = testsourceloc();
    else if (strcmp(argv[0], "dbvdesctest") == 0)
      ret = dbvdesctest();
    else if (strcmp(argv[0], "uthashmap") == 0)
      ret = testUtHashMap();
    else if (strcmp(argv[0], "uthashmap2") == 0)
      ret = testUtHashMap2();
    else if (strcmp(argv[0], "printdec") == 0)
      ret = printdec(argc, argv);
    else if (strcmp(argv[0], "bigraphtest") == 0)
      return bigraphtest(argc, argv);
    else if (strcmp(argv[0], "graphmergetest") == 0)
      return graphmergetest(argc, argv);
    else if (strcmp(argv[0], "disjointtest") == 0)
      return disjointtest();
    else if (strcmp(argv[0], "queuetest") == 0)
      return queuetest();
    else if (strcmp(argv[0], "datatoobjtest") == 0)
      return datatoobjtest(argc, argv);
    else if (strcmp(argv[0], "utdllisttest") == 0)
      ret = utdllisttest();
    else if (strcmp(argv[0], "xfertest") == 0)
      ret = xfertest();
    else if (strcmp(argv[0], "serialize") == 0)
      ret = serialize();
    else if (strcmp(argv[0], "oswrap64BitModels") == 0)
      ret = oswrap64BitModels();

#ifdef BVTEST_BOTH
    else if (strcmp(argv[0], "bvtest") == 0)
      ret = dbvtest() + tbvtest() + dsbvtest() + tsbvtest();
#else
    else if (strcmp(argv[0], "bvtest") == 0)
      ret = bvtest() + sbvtest();
#endif
  } // if
  else
    UtIO::cout() << "Usage: " << argv[0] << " argproctest|ztest|memorypooltest|randomvalgentest ..." << UtIO::endl;
  return ret;
} // int main
