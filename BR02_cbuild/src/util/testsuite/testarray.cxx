//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <stdio.h>
#include "util/UtArray.h"
#include "util/UtStringArray.h"
#include <algorithm>

template<class Iterator, class Val>
void test_iterators(Iterator first, int delta,
                    Val val1, Val val2)
{
  Iterator second = first + delta;
  assert(second - delta == first);
  assert((*first) == val1);
  assert((*second) == val2);
  assert(first < second);
  assert(first <= second);
  assert(! (second <= first));
  assert(second >= first);
  assert(! (first >= second));
  assert(second > first);
  assert(first != second);
  second -= delta;
  assert(first == second);

  // test pre-increment, pre-dec
  second++;
  assert(first + 1 == second--);
  assert(first++ == second);
}


// Used in test_base_vec() testing.  GCC 2.95 has problems
// with classes inside function scope.

struct Pred: public UtPtrArray::Predicate {
  bool operator()(const void* ptr) const {
    return *((const char*)ptr) == 'l';
  }
};

static void test_base_vec()
{
  // test push-back
  UtPtrArray a;
  static const char hw[] = "Hello, world!";
  for (const char* p = hw; *p != '\0'; ++p)
  {
    a.push_back((void*) p);
    //a.printStrings();
    //fprintf(stdout, "------------------------------------------\n");
  }

  // test indexing
  for (UInt32 i = 0; i < a.size(); ++i)
  {
    const char* c = (const char*) a[i];
    printf("%c", *c);
  }
  printf("\n");

  // test forward iteration
  for (UtPtrArray::iterator q = a.begin(); q != a.end(); ++q)
  {
    const char* c = (const char*) *q;
    printf("%c", *c);
  }
  printf("\n");

  // test const iteration
  const UtPtrArray& b = a;
  for (UtPtrArray::const_iterator q = b.begin(); q != b.end(); ++q)
  {
    const char* c = (const char*) *q;
    printf("%c", *c);
  }
  printf("\n");

  // test reverse iteration
  for (UtPtrArray::reverse_iterator q = a.rbegin(); q != a.rend(); ++q)
  {
    const char* c = (const char*) *q;
    printf("%c", *c);
  }
  printf("\n");

  // test reverse const iteration
  for (UtPtrArray::const_reverse_iterator q = b.rbegin(); q != b.rend(); ++q)
  {
    const char* c = (const char*) *q;
    printf("%c", *c);
  }
  printf("\n");


  // test array removal
  assert(a.remove(hw + 1) == 1); // removes the 'e'
  for (UtPtrArray::iterator p = a.begin(); p != a.end(); ++p)
  {
    const char* c = (const char*) *p;
    printf("%c", *c);
  }
  printf("\n");

  // test array removal
  assert(a.removeMatching(Pred()) == 3); // removes all the l's
  for (UtPtrArray::iterator p = a.begin(); p != a.end(); ++p)
  {
    const char* c = (const char*) *p;
    printf("%c", *c);
  }
  printf("\n");

  // Now test some big arrays.  Cast from integers to pointers for
  // ease of testing
  a.clear();
  for (UIntPtr i = 0; i < 100000; ++i)
    a.push_back((void*) i);
  for (UIntPtr i = 0; i < 100000; ++i)
    assert(a[i] == (void*) i);

  // test UtPtrArray::iterator
  test_iterators(a.begin(), 1, (void*) 0, (void*) 1);

  // test UtPtrArray::reverse_iterator
  test_iterators(a.rbegin(), 2, (void*) 99999, (void*) 99997);

  // test UtPtrArray::const_iterator
  test_iterators(b.begin(), 3, (void*) 0, (void*) 3);

  // test UtPtrArray::const_reverse_iterator
  test_iterators(b.rbegin(), 4, (void*) 99999, (void*) 99995);
} // static void test_base_vec

struct CharCompare {
  bool operator()(const char* a, const char* b) const {return *a < *b;}
};

struct CompareToChar : std::unary_function<const char*, bool>{
  CompareToChar(char c): mChar(c) {}
  result_type operator()(argument_type p) const {
    return *p == mChar;
  }
  char mChar;
};

static void test_str_vec()
{
  // test push-back
  UtArray<const char*> a, remTest;
  const char hw[] = "Hello, world!";
  for (const char* p = hw; *p != '\0'; ++p)
    a.push_back(p);
  remTest = a;

  // test indexing
  for (UInt32 i = 0; i < a.size(); ++i)
    printf("%c", *a[i]);
  printf("\n");

  // test forward iteration
  for (UtArray<const char*>::iterator q = a.begin(); q != a.end(); ++q)
    printf("%c", **q);
  printf("\n");

  // test const iteration
  const UtArray<const char*>& b = a;
  for (UtArray<const char*>::const_iterator q = b.begin(); q != b.end(); ++q)
    printf("%c", **q);
  printf("\n");

  // test reverse iteration
  for (UtArray<const char*>::reverse_iterator q = a.rbegin(); q != a.rend(); ++q)
    printf("%c", **q);
  printf("\n");

  // test const iteration
  for (UtArray<const char*>::const_reverse_iterator q = b.rbegin(); q != b.rend(); ++q)
    printf("%c", **q);
  printf("\n");

  // test UtArray::iterator
  test_iterators(a.begin(), 1, a[0], a[1]);

  // test UtArray::reverse_iterator
  test_iterators(a.rbegin(), 2, a[a.size() - 1], a[a.size() - 3]);

  // test UtArray::const_iterator
  test_iterators(b.begin(), 3, b[0], b[3]);

  // test UtArray::const_reverse_iterator
  test_iterators(b.rbegin(), 4, b[b.size() - 1], b[b.size() - 5]);

  // Sort with forward & backward iterators & print
  std::sort(a.begin(), a.end(), CharCompare());
  printf("sorted forward: ");
  for (UtArray<const char*>::iterator p = a.begin(); p != a.end(); ++p)
    printf("%c", **p);
  printf("\nsorted reverse: ");
  std::sort(a.rbegin(), a.rend(), CharCompare()
);
  for (UtArray<const char*>::iterator p = a.begin(); p != a.end(); ++p)
    printf("%c", **p);
  printf("\n");

  // test array removal
  assert(remTest.remove(hw + 1) == 1); // removes the 'e'
  for (UtArray<const char*>::iterator p = remTest.begin(); p != remTest.end();
       ++p)
  {
    printf("%c", **p);
  }
  printf("\n");

  assert(remTest.removeMatching(CompareToChar('l')) == 3);
  for (UtArray<const char*>::iterator p = remTest.begin(); p != remTest.end();
       ++p)
  {
    printf("%c", **p);
  }
  printf("\n");
} // static void test_str_vec

static void test_utstringarray()
{
  // test push-back
  UtStringArray a, remTest;
  const char hw[] = "Hello, world!";
  for (const char* p = hw; *p != '\0'; ++p)
    a.push_back(p);
  remTest = a;

  // test indexing
  for (UInt32 i = 0; i < a.size(); ++i)
    printf("%c", *a[i]);
  printf("\n");

  // test forward iteration
  for (UtStringArray::iterator q = a.begin(); q != a.end(); ++q)
    printf("%c", **q);
  printf("\n");

  // test const iteration
  const UtStringArray& b = a;
  for (UtStringArray::const_iterator q = b.begin(); q != b.end(); ++q)
    printf("%c", **q);
  printf("\n");

  // test reverse iteration
  for (UtStringArray::reverse_iterator q = a.rbegin(); q != a.rend(); ++q)
    printf("%c", **q);
  printf("\n");

  // test const iteration
  for (UtStringArray::const_reverse_iterator q = b.rbegin(); q != b.rend(); ++q)
    printf("%c", **q);
  printf("\n");

  // test UtArray::iterator
  test_iterators(a.begin(), 1, a[0], a[1]);

  // test UtArray::reverse_iterator
  test_iterators(a.rbegin(), 2, a[a.size() - 1], a[a.size() - 3]);

  // test UtArray::const_iterator
  test_iterators(b.begin(), 3, b[0], b[3]);

  // test UtArray::const_reverse_iterator
  test_iterators(b.rbegin(), 4, b[b.size() - 1], b[b.size() - 5]);

  // Sort with forward & backward iterators & print
  std::sort(a.begin(), a.end(), CharCompare());
  printf("sorted forward: ");
  for (UtStringArray::iterator p = a.begin(); p != a.end(); ++p)
    printf("%c", **p);
  printf("\nsorted reverse: ");
  std::sort(a.rbegin(), a.rend(), CharCompare()
);
  for (UtStringArray::iterator p = a.begin(); p != a.end(); ++p)
    printf("%c", **p);
  printf("\n");

  // test array removal
  assert(remTest.remove(hw + 1) == 1); // removes the 'e'
  for (UtStringArray::iterator p = remTest.begin(); p != remTest.end();
       ++p)
  {
    printf("%c", **p);
  }
  printf("\n");

  UInt32 curSize = remTest.size();
  remTest.erase(remTest.begin());
  assert(remTest.size() == curSize - 1);
}

static void test_int_vec()
{
  UtArray<UIntPtr> a;

  // Now test some big arrays.  Cast from integers to pointers for
  // ease of testing
  a.clear();
  for (UIntPtr i = 0; i < 100000; ++i)
    a.push_back(i);
  for (UIntPtr i = 0; i < 100000; ++i)
    assert(a[i] == i);
}


static void test_insert() {
  UtArray<UInt32> src, dest;

  // Test inserting at the beginning
  src.push_back(0);
  src.push_back(1);
  src.push_back(2);
  dest.push_back(3);
  dest.push_back(4);
  dest.insert(dest.begin(), src.begin(), src.end());
  assert(dest.size() == 5);
  for (UInt32 i = 0; i < 5; ++i) {
    assert(dest[i] == i);
  }

  // test inserting at the end
  src.clear();
  src.push_back(5);
  src.push_back(6);
  dest.insert(dest.end(), src.begin(), src.end());
  assert(dest.size() == 7);
  for (UInt32 i = 0; i < 7; ++i) {
    assert(dest[i] == i);
  }

  // test inserting in the middle
  dest.clear();
  src.clear();
  dest.push_back(0);
  dest.push_back(1);
  dest.push_back(2);
  src.push_back(3);
  src.push_back(4);
  src.push_back(5);
  src.push_back(6);
  src.push_back(7);
  dest.push_back(8);
  dest.push_back(9);
  UtArray<UInt32>::iterator i = dest.begin() + 3;
  dest.insert(i, src.begin(), src.end());
  assert(dest.size() == 10);
  for (UInt32 i = 0; i < 10; ++i) {
    assert(dest[i] == i);
  }
}    

int testarray()
{
  // Test the power-of-2 calculation
  for (UInt32 exp = 0; exp < 32; ++exp)
  {
    UInt32 x = 1 << exp;
    for (SInt32 i = -1; i <= 1; ++i)
    {
      UInt32 y = x + i;
      printf("%x -> %x\n", y, UtPtrArray::nextPowerOfTwo(y));
    }
  }

  test_base_vec();
  test_str_vec();
  test_int_vec();
  test_utstringarray();
  test_insert();
  return 0;
}

