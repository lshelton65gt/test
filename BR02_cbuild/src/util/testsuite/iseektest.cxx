//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// Test seaking on input streams

// The input stream contains decimal numbers equal to the seak value
// at every location divisible by 11, up to the size of the file.
// E.g. at location 121, it contains the string "121".

#include "util/RandomValGen.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/CarbonAssert.h"

static int test_seek(UtIStream* is, UInt32 file_size, UInt32 iters) {
  RandomValGen rand;

  for (UInt32 i = 0; i < iters; ++i) {
    // Get a random number divisible by 11
    UInt32 r = rand.URRandom(0, file_size / 11 - 1) * 11;
    is->seek(r, UtIO::seek_set);
    UInt32 token;
    if (! (*is >> token)) {
      UtIO::cout() << is->getErrmsg() << "\n";
      return 1;
    }
    if (token != r) {
      UtIO::cout() << "iseek mismatch: " << token << "!=" << r << "\n";
      return 1;
    }
    UtString buf;
    buf << r;
    UInt32 expectedPos = buf.size() + r;
    if (is->tell() != expectedPos) {
      UtIO::cout() << "tell mismatch: " << is->tell()
                   << "!=" << expectedPos << "\n";
    }
  }
  return 0;
}

// Write a file with the characters indicating the file-position at
// every 11th character
static void write_file(UtOStream* os, UInt32 file_size) {
  UtString buf;
  for (UInt32 numChars = 0; numChars < file_size; numChars += 11) {
    os->setwidth(11);
    *os << UtIO::left << numChars;
  }
}

int iseektest() {
  int file_size = 500*11;
  {
    UtOBStream os("numbers.txt");
    INFO_ASSERT(os.is_open(), "cannot write numbers.txt");
    write_file(&os, file_size);
  }

  int ret = 0;

  {
    UtIBStream is("numbers.txt");
    INFO_ASSERT(is.is_open(), "cannot read numbers.txt");
    ret |= test_seek(&is, file_size, 500);
  }

  {
    UtString buf;
    UtOStringStream os(&buf);
    write_file(&os, file_size);
    UtIStringStream is(buf.c_str());
    ret |= test_seek(&is, file_size, 500);
  }
  return ret;
}
