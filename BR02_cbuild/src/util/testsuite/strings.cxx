// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtString.h"
#include "util/UtStream.h"
#include "util/UtIOStream.h"
#include "util/MemManager.h"
#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "util/CarbonAssert.h"

// Uncomment next 2 lines to regression-test SGI strings (with mem leaks)
// #include <string>
// #define UtString std::string

int testStrings(int, char*[])
{
  {
    UtString bug4499("12345678");
    const char* s = bug4499.c_str() + 1;
    bug4499.assign(s);
    assert(bug4499 == "2345678");

    s = bug4499.c_str() + 1;
    bug4499 = s;
    assert(bug4499 == "345678");

    s = bug4499.c_str() + 1;
    bug4499.insert(1, s + 1);
    assert(bug4499 == "3567845678");

    s = bug4499.c_str() + 1;
    bug4499 += s;
    assert(bug4499 == "3567845678567845678");

    s = bug4499.c_str() + 1;
    bug4499 << s;
    assert(bug4499 == "3567845678567845678567845678567845678");

    bug4499 << bug4499;
    assert(bug4499 == "35678456785678456785678456785678456783567845678567845678567845678567845678");
  }

  StrToken tokIter("   Now    is\tthe\n\ntime for all good men     to come to the aid    of their country.\n\n\n");
  const char* tok;
  while (tokIter(&tok))
    UtIO::cout() << tok << " ";
  UtIO::cout() << UtIO::endl;

  int stat = 0;
  const char* firstStr = "2k;lkjf";
  const char* there = StringUtil::findFirstNonAlphanumeric(firstStr);
  if (! there)
  {
    UtIO::cerr() << "findFirstNonAlphanumeric failed to find token" << UtIO::endl;
    stat = 1;
  }
  else if (there != firstStr + 2)
  {
    UtIO::cerr() << "findFirstNonAlphanumeric returned incorrect position" << UtIO::endl;
    stat = 1;
  }
  
  there = StringUtil::findFirstNonAlphanumeric("abc123");
  if (there != NULL)
  {
    UtIO::cerr() << "findFirstNonAlphanumeric returned non null for alphanumeric string" << UtIO::endl;
    stat = 1;
  }

  for (size_t i = 65000; i < 200000; ++i) {
    UtString* s = new UtString;
    s->reserve(65537);
    delete s;
  }

  // Constructor, operator==, operator!= tests
  UtString a;
  assert(a.empty());
  a = "Hello world!";
  assert(!a.empty());
  UtString b("Hello world!");
  UtString c("Hello world!xxx", 12);
  UtString d("H", 1);
  assert(a == b);
  assert(a == c);
  assert(a != d);

  // += tests
  d += "ello";
  d += ' ';
  d += UtString("world!");
  assert(a == d);

  UInt32 i = 0;
  const char* s = a.c_str();
  for (UtString::iterator p = a.begin(); p != a.end(); ++p, ++i) {
    assert(*p == a[i]);
    assert(a[i] == s[i]);
  }

  // insertion, compare tests
  a.insert(a.begin(), '*');
  assert(a.compare("*Hello world!") == 0);
  a.insert(2, "xxx");
  assert(a.compare("*Hxxxello world!") == 0);
  a.insert(10, UtString("12345"));
  assert(a.compare("*Hxxxello 12345world!") == 0);
  a.insert(10, "ABCD", 3);
  assert(a.compare("*Hxxxello ABC12345world!") == 0);
  a.insert(10, 5, 'y');
  assert(a.compare("*Hxxxello yyyyyABC12345world!") == 0);
  UtString::iterator bs = b.begin() + 1;
  UtString::iterator be = b.end() - 1;
  a.insert(a.end(), bs, be);
  assert(a.compare("*Hxxxello yyyyyABC12345world!ello world") == 0);

  // erase tests
  a.erase(a.begin());
  assert(a.compare("Hxxxello yyyyyABC12345world!ello world") == 0);
  {
    UtString::iterator p = a.begin();
    ++p;                        // first x;
    UtString::iterator q = p + 3; // 'e'
    a.erase(p, q);
  }
  assert(a.compare("Hello yyyyyABC12345world!ello world") == 0);
  a.erase(6, 24);
  assert(a.compare("Hello world") == 0);

  // compare tests with differing lengths
  assert(a.compare("GHello world") > 0);
  assert(a.compare("IHello world") < 0);
  assert(a.compare("Hello world!") < 0);
  assert(a.compare("Hello worl") > 0);

  // compare tests with embedded nuls
  {
    UtString x(a), y(a);
    x += '\0';
    y += '\0';
    x += 'a';
    y += 'a';
    assert(x.compare(y) == 0);
    x += '\0';
    y += '\0';
    x += 'a';
    y += 'b';
    assert(x.compare(y) < 0);
    assert(x.size() == a.size() + 4);
    assert(y.size() == a.size() + 4);
  }    

  // append tests
  a.append(b);
  assert(a.compare("Hello worldHello world!") == 0);
  a.append(b, 1, 4);
  assert(a.compare("Hello worldHello world!ello") == 0);
  a.append("xxx");
  assert(a.compare("Hello worldHello world!elloxxx") == 0);
  a.append("yyyyy", 3);
  assert(a.compare("Hello worldHello world!elloxxxyyy") == 0);
  a.append(10, '-');
  assert(a.compare("Hello worldHello world!elloxxxyyy----------") == 0);

  


  assert(b.find_first_of("H", 1, 1) == UtString::npos);  // '1' misses the H
  assert(b.find_first_of("H", 0, 1) == 0);
  assert(b.find_first_not_of("Helo wrd!", 0, 9) == UtString::npos);
  assert(b.find_first_not_of("Hlo wrd!", 0, 8) == 1); // finds 'e'
  assert(b.find_first_of("H", 1) == UtString::npos);
  assert(b.find_first_of("H") == 0);
  assert(b.find_first_not_of("Helo wrd!") == UtString::npos);
  assert(b.find_first_not_of("Hlo wrd!") == 1); // finds 'e'

  assert(b.find_first_of('H', 1) == UtString::npos);  // '1' misses the H
  assert(b.find_first_of('e') == 1);
  assert(b.find_first_not_of('H') == 1); // finds 'e'

  assert(b.find_first_of(UtString("H"), 1) == UtString::npos);  // '1' misses the H
  assert(b.find_first_of(UtString("H")) == 0);
  assert(b.find_first_not_of(UtString("Helo wrd!")) == UtString::npos);
  assert(b.find_first_not_of(UtString("Hlo wrd!")) == 1); // finds 'e'
  assert(b.find_first_of(UtString("H"), 1) == UtString::npos);
  assert(b.find_first_of(UtString("H")) == 0);
  assert(b.find_first_not_of(UtString("Helo wrd!")) == UtString::npos);
  assert(b.find_first_not_of(UtString("Hlo wrd!")) == 1); // finds 'e'

  // reserve enough space that we know that memory will not need to be
  // reallocated
  {
    UtString x;
    x.reserve(1000);
    const char* p = x.data();
    for (int i = 0; i < 1000; ++i) {
      x += ' ';
      assert(x.data() == p);
    }
  }

  {
    UtString x("foo");
    x.resize(10, ' ');
    assert(x.compare("foo       ") == 0);
  }

  assert(b.rfind('!') == 11);
  assert(b.rfind('!', 10) == UtString::npos);
  assert(b.rfind('l') == 9); // Hello WorLd!
  assert(b.find('l') == 2);  // HeLlo World!
  assert(b.find('z') == UtString::npos);
  assert(b.find("llo") == 2);
  assert(b.find("llox") == UtString::npos);
  assert(b.find("llox", 0, 3) == 2);

  // Test StringUtil:: methods alloc, dup, dup, free, free.
  char* stra = StringUtil::dup("my string");
  assert(strlen(stra) == strlen("my string"));
  StringUtil::free(stra);	// Frees strlen() + 1 bytes
  char* strb = StringUtil::dup(stra, 4); // Only 4 characters, no null
  StringUtil::free(strb, 4);
  strb = StringUtil::alloc(9);
  StringUtil::free(strb, 9);

  return stat;
}
