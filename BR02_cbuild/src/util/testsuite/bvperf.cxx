// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/BitVector.h"
#include "util/Timing.h"
#include "util/UtIOStream.h"

int bvperf()
{
  BitVector<96> v;
  int totalCount = 0;
  Timing timing;
  timing.start();
  for (unsigned int iter = 0; iter < 80000000; ++iter)
  {
    v[37] = iter & 1;
    v[94] = !(iter & 1);
    totalCount += v[37].value();
    totalCount += v[94].value();
  }
  timing.end();
  UtIO::cout() << "constant  [] indexing: ";
  timing.printDeltas();
  UtIO::cout() << totalCount << UtIO::endl;

  totalCount = 0;
  timing.start();
  for (int iter = 0; iter < 80000000; ++iter)
  {
    v.set(37, iter & 1);
    v.set(94, !(iter & 1));
    totalCount += (int) v.test(37);
    totalCount += (int) v.test(94);
  }
  timing.end();
  UtIO::cout() << "constant method indexing: ";
  timing.printDeltas();
  UtIO::cout() << totalCount << UtIO::endl;

  totalCount = 0;
  timing.start();
  for (SInt32 iter = 0; iter < 2000000; ++iter)
  {
    for (UInt32 i = 0; i < 96; ++i)
      v[i] = (i ^ iter) & 1;
    for (int i = 0; i < 96; ++i)
      totalCount += v[i].value();
  }
  timing.end();
  UtIO::cout() << "variable [] indexing: ";
  timing.printDeltas();
  UtIO::cout() << totalCount << UtIO::endl;
  
  totalCount = 0;
  timing.start();
  for (int iter = 0; iter < 2000000; ++iter)
  {
    for (int i = 0; i < 96; ++i)
      v.set(i, (i ^ iter) & 1);
    for (int i = 0; i < 96; ++i)
      totalCount += (int) v.test(i);
  }
  timing.end();
  UtIO::cout() << "variable method indexing: ";
  timing.printDeltas();
  UtIO::cout() << totalCount << UtIO::endl;

  return 0;
} // int bvperf
