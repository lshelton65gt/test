//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/CarbonAssert.h"

int testStreamTokenizer() {
  UtString buf, ibuf;
  UtOStringStream os(&buf);
  const char* toks[] = {"abc", "abc def", "\\abc\t\ndef\"", "", "'", NULL};
  const char** p = toks;
  os << UtIO::slashify;
  for (; *p != NULL; ++p) {
    os << *p;
  }

  UtIStringStream is(buf);
  is >> UtIO::slashify;
  for (p = toks; is >> ibuf; ibuf.clear(), ++p) {
    INFO_ASSERT(strcmp(ibuf.c_str(), *p) == 0, "mismatch");
  }
  INFO_ASSERT(*p == NULL, "end of tokens not reached");

#if 0
  // The tokenizer should be able to take any file that's
  // human-readable, make a still-readable tokenized version
  // of it, and then read that tokenized version back in.
  // The tokenized version will change the whitespace relative
  // to the original, but if we do:
  //
  //  ORIG->slashify->A->tokenize->B->slashify->C->tokenize->D
  // 
  // then A==C and B==D.

  UtString buf;
  UtIStream& cin = UtIO::cin();
  while (cin.fgetline(&buf)) {
    
  }
#endif
  return 0;
} // int testTokenizer
