//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/DynBitVecDesc.h"
#include "util/RandomValGen.h"

int dbvdesctest() {
  DynBitVectorFactory factory(false);

  // Generate some random numbers of varying sizes, make
  // DynBitVectors out of them, store them as descriptors,
  // and in their original form, and make sure the values
  // are retained.

  RandomValGen rand(100);
  DynBitVector buf;
  for (SInt32 i = 0; i < 10000; ++i) {
    SInt32 numBits = rand.URRandom(1, 70);
    DynBitVector bv(numBits);
    bv.resize(numBits);
    for (SInt32 j = 0; j < numBits; ++j) {
      if ((rand.URandom() & 0x8000) != 0)
        bv.set(j);
    }

    DynBitVecDesc desc(bv, &factory);
    desc.getBitVec(&buf, numBits);
    assert(buf == bv);
  }

  // Probably most of the bitvecs tested above are not contiguous.
  // Explicitly make some contiguous ones.
  for (SInt32 i = 0; i < 10000; ++i) {
    // Make a large number of small ones, plus some large ones that
    // exceed the 2^16 limits of the size/startBit and force us to
    // test the ALL_ONES case, plus some large ones with a single
    // bit set that force us to test SINGLE_BIT.
    UInt32 isSmall = rand.URRandom(0,1) == 0;
    UInt32 isSingleBit = rand.URRandom(0,1) == 0;
    SInt32 numBits = rand.URRandom(1, isSmall? 500: 1000000);
    DynBitVector bv(numBits);
    UInt32 startBit = rand.URRandom(0, isSmall? numBits - 1: 1);
    UInt32 sz = isSingleBit? 1: rand.URRandom(0, numBits - startBit);

    bv.resize(numBits);
    bv.setRange(startBit, sz, 1);
    DynBitVecDesc desc(bv, &factory);
    desc.getBitVec(&buf, numBits);
    assert(buf == bv);
    assert(desc.all(sz) == ((startBit == 0) || (sz == 0)));
    assert(desc.count() == sz);
    
    UInt32 gotStartBit, gotSize;
    assert(desc.getContiguousRange(&gotStartBit, &gotSize));
    assert((gotStartBit == startBit) || (sz == 0));
    assert(gotSize == sz);
  } // for

  return 0;
} // int dbvdesctest
