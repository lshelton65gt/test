//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <cstdio>
#include "util/DynBitVector.h"
#include "util/UtConv.h"
#include "util/RandomValGen.h"

int printdec(int, char**) {
  // Generate large numbers of random decimal integers
  RandomValGen random(0);
# define MAX_DIGITS 2000
# define BUFSIZE MAX_DIGITS + 1000 // needed because UtConv requires octal sizes
  char number[BUFSIZE];
  char conversion[BUFSIZE];

  for (int i = 0; i < 1000; ++i) {
    UInt32 numDigits = random.URRandom(1, MAX_DIGITS);
    bool seenNonZero = false;
    for (UInt32 j = 0; j < numDigits; ++j) {
      number[j] = random.URRandom('0', '9');
      if (!seenNonZero) {
        if (number[j] == '0') {
          --j;                    // don't put in leading zeros
        }
        else {
          seenNonZero = true;
        }
      }
    }
    number[numDigits] = '\0';

    // Convert this into a DynBitVector so we can convert it back to a string
    int len = strlen(number);
    int maxBitSize = len*4;
    DynBitVector bv(maxBitSize), tmp(maxBitSize);
    //DynBitVector ten(4, 10);
    for (char* p = number; *p != '\0'; ++p) {
      int digit = *p - '0';
      bv.multBy10();
      //bv *= 10;
      //bv.integerMult(ten);
      bv += digit;
    }

    UInt32* p = const_cast<UInt32*>(bv.getUIntArray());
    int cvt = UtConv::BinaryToUnsignedDec(p, maxBitSize, conversion, BUFSIZE);
    assert(cvt != -1);
    assert(strcmp(conversion, number) == 0);

    // Do this again but now give a better prediction for the number
    // of characters so that we don't have to do the memmove (test
    // both paths through the conversion)
    int bufsiz = (maxBitSize + 2)/3 + 1;
    char* buf = new char[bufsiz];
    cvt = UtConv::BinaryToUnsignedDec(p, maxBitSize, buf, bufsiz);
    assert(cvt != -1);
    assert(strcmp(conversion, number) == 0);
    assert(strcmp(conversion, buf) == 0);
    delete [] buf;
  } // for
  return 0;
} // int printdec
