//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/SourceLocator.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"

int testsourceloc() {
  SourceLocatorFactory factory;

  // Test 10k...no...100 files with 400k lines each -- 4B source locators
  UtString fname;
  for (SInt32 i = 0; i < 100 /*10000*/; ++i) {
    fname.clear();
    fname << "file_" << i;
    const char* fstr = fname.c_str();
    if ((i % 20) == 0)
      fprintf(stderr, "Checking %s\n", fstr);
    for (UInt32 j = 1; j < 400000; ++j) {
      SourceLocator loc = factory.create(fstr, j);
      if ((strcmp(loc.getFile(), fstr) != 0) ||
          (loc.getLine() != j))
      {
        fprintf(stderr, "Expected `%s:%lu', got `%s:%lu'\n",
                fstr, (unsigned long) j,
                loc.getFile(), (unsigned long) loc.getLine());
        return 1;
      }
    }
  }
  return 0;
}
