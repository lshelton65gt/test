//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/DynBitVector.h"
#include "util/CarbonAssert.h"
#include "shell/carbon_shelltypes.h"
#include <cstring>

int xfertest() {
  UInt32 src[100], dst[100];

  memset(src, '\0', 100*sizeof(UInt32));
  memset(dst, '\0', 100*sizeof(UInt32));

  src[0] = 0x11223344;
  src[1] = 0x55667788;
  src[2] = 0x99aabbcc;
  src[3] = 0xddeeff00;

  DynBitVector::anytoany(src, dst, 8, 64, 8);
  INFO_ASSERT(dst[0] == (src[0] & 0xffffff00), "w0");
  INFO_ASSERT(dst[1] == src[1], "w1");
  INFO_ASSERT(dst[2] == (src[2] & 0x000000ff), "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 8, 64, 0);
  INFO_ASSERT(dst[0] == 0x88112233, "w0");
  INFO_ASSERT(dst[1] == 0xcc556677, "w1");
  INFO_ASSERT(dst[2] == 0, "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 0, 64, 8);
  INFO_ASSERT(dst[0] == 0x22334400, "w0");
  INFO_ASSERT(dst[1] == 0x66778811, "w1");
  INFO_ASSERT(dst[2] == 0x55, "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 8, 8, 16);
  INFO_ASSERT(dst[0] == 0x00330000, "w0");
  INFO_ASSERT(dst[1] == 0x0, "w1");
  INFO_ASSERT(dst[2] == 0x0, "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 8, 16, 16);
  INFO_ASSERT(dst[0] == 0x22330000, "w0");
  INFO_ASSERT(dst[1] == 0x0, "w1");
  INFO_ASSERT(dst[2] == 0x0, "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 8, 24, 16);
  INFO_ASSERT(dst[0] == 0x22330000, "w0");
  INFO_ASSERT(dst[1] == 0x11, "w1");
  INFO_ASSERT(dst[2] == 0x0, "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 8, 32, 16);
  INFO_ASSERT(dst[0] == 0x22330000, "w0");
  INFO_ASSERT(dst[1] == 0x8811, "w1");
  INFO_ASSERT(dst[2] == 0x0, "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 8, 48, 16);
  INFO_ASSERT(dst[0] == 0x22330000, "w0");
  INFO_ASSERT(dst[1] == 0x66778811, "w1");
  INFO_ASSERT(dst[2] == 0x0, "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 8, 64, 16);
  INFO_ASSERT(dst[0] == 0x22330000, "w0");
  INFO_ASSERT(dst[1] == 0x66778811, "w1");
  INFO_ASSERT(dst[2] == 0xcc55, "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 8, 8, 24);
  INFO_ASSERT(dst[0] == 0x33000000, "w0");
  INFO_ASSERT(dst[1] == 0x0, "w1");
  INFO_ASSERT(dst[2] == 0x0, "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 8, 16, 24);
  INFO_ASSERT(dst[0] == 0x33000000, "w0");
  INFO_ASSERT(dst[1] == 0x22, "w1");
  INFO_ASSERT(dst[2] == 0x0, "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 8, 24, 24);
  INFO_ASSERT(dst[0] == 0x33000000, "w0");
  INFO_ASSERT(dst[1] == 0x1122, "w1");
  INFO_ASSERT(dst[2] == 0x0, "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 8, 32, 24);
  INFO_ASSERT(dst[0] == 0x33000000, "w0");
  INFO_ASSERT(dst[1] == 0x881122, "w1");
  INFO_ASSERT(dst[2] == 0x0, "w2");

  memset(dst, '\0', 100*sizeof(UInt32));
  DynBitVector::anytoany(src, dst, 8, 64, 24);
  INFO_ASSERT(dst[0] == 0x33000000, "w0");
  INFO_ASSERT(dst[1] == 0x77881122, "w1");
  INFO_ASSERT(dst[2] == 0xcc5566, "w2");

  return 0;
}
