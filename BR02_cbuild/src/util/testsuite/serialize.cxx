//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// This tests some basic serialization code.  Here is the expected output
// from this test:

/*
{
  'c1'  'mc_1' 
  'c2'  'mc_2' 
  'ia'  [
    1 
    3 
    5 
    7 
  ]
  'sa'  [
    'now' 
    'is' 
    'the' 
    'time' 
    'for two words in one token' 
    'and embedded " and \'' 
  ]
  'sub_class'  {
    'c1'  'msc_1' 
  }
}
*/

// note that arrays are enclosed with [] and structures are enclosed with {},
// and that we indent the output for legibility.
// 
// we are dependent on the slashification feature of Ut*Stream to make sure that
// we can read strings as tokens.  I expect this code will be converted to use
// XML, at which point different mechanisms will be employed for 

#include "util/UtSerialize.h"
#include "util/UtString.h"
#include "util/UtVector.h"

class MySerializableSubClass {
public:
  ~MySerializableSubClass() {}

  void stuff() {
    mC1 = "msc_1";
  }

  void serialize(UtSerialStruct* ss) {
    ss->addAtomic("c1", &mC1);
  }

  UtString mC1;
};

class MySerializableClass {
public:
  ~MySerializableClass() {}
  void stuff() {
    mC1 = "mc_1";
    mC2 = "mc_2";
    mSubClass.stuff();
    mIntArray.push_back(1);
    mIntArray.push_back(3);
    mIntArray.push_back(5);
    mIntArray.push_back(7);
    mStrArray.push_back("now");
    mStrArray.push_back("is");
    mStrArray.push_back("the");
    mStrArray.push_back("time");
    mStrArray.push_back("for two words in one token");
    mStrArray.push_back("and embedded \" and '");
  }

  void serialize(UtSerialStruct* ss) {
    ss->addAtomic("c1", &mC1);
    ss->addStruct("sub_class", &mSubClass);
    ss->addAtomic("c2", &mC2);
    ss->addAtomicContainer("ia", &mIntArray);
    ss->addAtomicContainer("sa", &mStrArray);
  }

  UtString mC1;
  MySerializableSubClass mSubClass;
  UtString mC2;
  UtArray<UInt32> mIntArray;
  UtVector<UtString> mStrArray;
};
  
int serialize() {
  MySerializableClass c1, c2;
  c1.stuff();

  {
    UtSerialStruct serializer;
    c1.serialize(&serializer);
    UtOBStream os("serial.out");
    serializer.write(&os, 0);
  }
  {
    UtSerialStruct serializer;
    c2.serialize(&serializer);
    {
      UtIBStream is("serial.out");
      if (!serializer.read(&is)) {
        UtIO::cerr() << is.getErrmsg() << "\n";
        return 1;
      }
    }
    {
      UtOBStream os("serial.out2");
      serializer.write(&os, 0);
    }
  }
  return 0;
}
