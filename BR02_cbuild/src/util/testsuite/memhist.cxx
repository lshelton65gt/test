//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/MemManager.h"
#include <cstdio>               // for sprintf
#include <cstring>              // for strcmp

static void* sAlloc(size_t sz, bool overhead) {
  if (overhead) {
    return carbonmem_malloc(sz);
  }
  return carbonmem_alloc(sz);
}

static void* sRealloc(void* ptr, size_t old_sz, size_t sz, bool overhead) {
  if (overhead) {
    return carbonmem_realloc(ptr, sz);
  }
  return carbonmem_reallocate(ptr, old_sz, sz);
}

static void sFree(void* ptr, size_t sz, bool overhead) {
  if (overhead) {
    carbonmem_free(ptr);
  }
  else {
    carbonmem_dealloc(ptr, sz);
  }
}

void* deepMalloc(int depth, bool overhead) {
  if (depth == 1) {
    return sAlloc(20000, overhead);
  }

  // This code should be compiled with -O0, otherwise gcc will optimize
  // the tail-recursion and not show the stack I want.

  return deepMalloc(depth - 1, overhead);
}

int memhist(int argc, char** argv) {
  bool overhead = (argc == 2) && (strcmp(argv[1], "overhead") == 0);

  CarbonMem::checkpoint("memhist.start");
  void* ptr = sAlloc(40000, overhead);
  CarbonMem::checkpoint("memhist.small_block");
  sFree(ptr, 40000, overhead);
  CarbonMem::checkpoint("memhist.small_block_freed");
  ptr = sAlloc(100000, overhead);
  CarbonMem::checkpoint("memhist.large_block");
  sFree(ptr, 100000, overhead);
  CarbonMem::checkpoint("memhist.large_block_freed");
  ptr = sAlloc(20000, overhead);
  ptr = sRealloc(ptr, 20000, 21000, overhead);
  CarbonMem::checkpoint("memhist.realloc_same_block");
  ptr = sRealloc(ptr, 21000, 40000, overhead);
  CarbonMem::checkpoint("memhist.realloc_bigger_block");
  ptr = sRealloc(ptr, 40000, 10000, overhead);
  CarbonMem::checkpoint("memhist.realloc_smaller_block");
  ptr = sRealloc(ptr, 10000, 80000, overhead);
  CarbonMem::checkpoint("memhist.realloc_large_block");
  ptr = sRealloc(ptr, 80000, 20000, overhead);
  CarbonMem::checkpoint("memhist.realloc_larger_block");
  sFree(ptr, 20000, overhead);
  CarbonMem::checkpoint("memhist.realloc_freed");
  for (UInt32 i = 5; i < 15; ++i) {
    ptr = deepMalloc(i, overhead);
    char buf[100];
    sprintf(buf, "memhist.deep%d", (int) i);
    CarbonMem::checkpoint(buf);
    sFree(ptr, 20000, overhead);
  }
  return 0;
} // int memhist

