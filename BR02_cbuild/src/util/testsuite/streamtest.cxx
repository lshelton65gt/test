// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtIOStream.h"
#include "util/CarbonTypes.h"
#include "util/UtCachedFileSystem.h"
#include "util/OSWrapper.h"
#include "util/CarbonAssert.h"
#include "util/DynBitVector.h"

// This test routine facilitates testing cached files by interleaving
// the writing of numerous indepdent files by specifing whihc phase
// to run.
static void test_stream(UtOStream& f, int which = -1)
{
# define DO_IT(n, expr) if ((which == -1) || (which == n)) expr

  DO_IT(0, f << UtString("UtString\n"));
  DO_IT(1, f << "const char*\n");

  // 10 billion, portably, without using ULL
  UInt64 tenBillion = ((UInt64)10000*((UInt64)1000*1000)); 
  DO_IT(2, f << tenBillion << "\n");

  SInt64 negTenBillion = -((SInt64) tenBillion);
  DO_IT(3, f << negTenBillion << "\n");

  SInt32 neg10k = -10000;
  DO_IT(4, f << neg10k << "\n");

  UInt32 fourBillion = 4UL*1000UL*1000UL*1000UL;
  DO_IT(5, f << fourBillion << "\n");

  UInt16 sixtyFourK = 0xFFFF;
  DO_IT(6, f << sixtyFourK << "\n");

  SInt32 neg32K = -32000;
  DO_IT(7, f << neg32K << "\n");

  DO_IT(8, f << 3.14592654 << "\n");

  const char c = 'c';
  DO_IT(9, f << c << '\n');

  DO_IT(10, f << UtIO::endl);

#if pfWINDOWS
  // The NULL to "(nil)" trick doesn't work on Windows.  I'm not sure
  // why, because this is really a g++ operator<< overload issue, not
  // a platform issue.  Fake it out for now so the rest of the test can
  // pass.
  DO_IT(11, f << "(nil)\n");
  DO_IT(12, f << "(nil)\n");
#else
  void* ptr = NULL;
  DO_IT(11, f << ptr << '\n');

  const void* cptr = NULL;
  DO_IT(12, f << cptr << '\n');
#endif
  
  DO_IT(13, f << "hex " << UtIO::hex << 20 << " " << 20 << "\n");
  DO_IT(14, f << "oct " << UtIO::oct << 20 << " " << 20 << "\n");
  DO_IT(15, f << "dec " << UtIO::dec << 20 << " " << 20 << "\n");
  DO_IT(16, f << "sdec " << UtIO::sdec << 20 << " " << 20 << "\n");
  DO_IT(17, f << "general "  << UtIO::general << 3.141592654 << " " << 3.141592654 << "\n");
  DO_IT(18, f << "fixed "    << UtIO::fixed   << 3.141592654 << " " << 3.141592654 << "\n");
  DO_IT(19, f << "exponent " << UtIO::exponent << 3.141592654E+00 << " " << 3.141592654E+00 << "\n");

  DO_IT(20, f << " Col1 Col2 Col3 Col4 Col5\n");
  DO_IT(21, f << UtIO::setw(5) << 1
        << UtIO::setw(5) << 2
        << UtIO::setw(5) << 3
        << UtIO::setw(5) << 4
        << UtIO::setw(5) << 5 << "\n");
  DO_IT(22, f << 1 << " " << 2 << " " << 3 << " " << 4 << " " << 5 << "\n");
  DO_IT(23, f << UtIO::setw(5) << UtIO::setfill('_') << 1
        << UtIO::setw(5) << 2
        << UtIO::setw(5) << 3
        << UtIO::setw(5) << 4
        << UtIO::setw(5) << 5 << "\n");
  DO_IT(24, f << 1 << 2 << 3 << 4 << 5 << "\n");

  DO_IT(25, f << "HEX " << UtIO::HEX << 20 << " " << 20 << "\n");
  DO_IT(26, f << "bin " << UtIO::bin << 20 << " " << 20 << "\n");

  // Big numbers
  DynBitVector bv(300);         // That's right, baby, 300 bits.

  // First, test an easy-to-verify decimal number
  // Generate 10^50, and see how it prints in various radices.
  bv = 1000000000;              // 10^9
  for (int i = 0; i < 41; ++i)  // 41+9 = 50
    bv.multBy10();
  DO_IT(27, f << "hex " << UtIO::hex << bv << "\n");
  DO_IT(31, f << "HEX " << UtIO::HEX << bv << "\n");
  DO_IT(28, f << "oct " << UtIO::oct << bv << "\n");
  DO_IT(29, f << "dec " << UtIO::dec << bv << "\n");
  DO_IT(30, f << "sdec " << UtIO::sdec << bv << "\n");
  DO_IT(32, f << "bin " << UtIO::bin << bv << "\n");
  f << UtIO::dec;
  
  // Now, test an easy-to-verify hex number
  // Think of all the hex numbers that make words and shift them in
  bv  = 0xbeefcafe;   bv <<= 36;
  bv |= 0xfeedface;   bv <<= 36;
  bv |= 0xdeadbeef;   bv <<= 36;
  bv |= 0xdeadface;   bv <<= 36;
  bv |= 0xcafedead;   bv <<= 36;
  
  DO_IT(27, f << "hex " << UtIO::hex << bv << "\n");
  DO_IT(31, f << "HEX " << UtIO::HEX << bv << "\n");
  DO_IT(28, f << "oct " << UtIO::oct << bv << "\n");
  DO_IT(29, f << "dec " << UtIO::dec << bv << "\n");
  DO_IT(30, f << "sdec " << UtIO::sdec << bv << "\n");
  DO_IT(32, f << "bin " << UtIO::bin << bv << "\n");
  f << UtIO::dec;

# define NUM_STATEMENTS 33
} // static void test_stream

int streamtest(void)
{
  UtString buf;
  UtOStringStream ss(&buf);
  test_stream(ss);
  fprintf(stdout, "UtOStringStream:\n%s\n\n", buf.c_str());
  fprintf(stdout, "_____________________\nUtOStream:\n");
  test_stream(UtIO::cout());

  // Test buffered file output (very small buffer)
  {
    UtOBStream file("small_buffer.txt", "w", 5);
    test_stream(file);
    file.close();
    UtIO::cout() << "[small_buffer required " << file.getNumWrites()
                 << " writes]" << UtIO::endl;
  }

  // Test buffered file output (very small buffer)
  {
    UtOBStream file("default_buffer.txt");
    test_stream(file);
    file.close();
    UtIO::cout() << "[default_buffer required " << file.getNumWrites()
                 << " writes]" << UtIO::endl;
  }

  // Now test a whole lot of open files, buffer sizes varying from 1 to 100
  // (pity the filesystem).  We are going to do this twice.  Once with small
  // buffers, which will cause a lot of thrashing, and once with default
  // buffers, which should just cause 64k writes & reopens
  for (int mode = 0; mode < 2; ++mode)
  {
    UtCachedFileSystem* fs = new UtCachedFileSystem(5);
    const int numFiles = 2000;
    UtOCStream* files[numFiles];

    if (mode == 0)
      UtIO::cout() << "\n\n____________\n[Small buffers]\n";
    else
      UtIO::cout() << "\n\n____________\n[Default buffers]\n";

    // Open all the 2000 files
    for (int i = 0; i < numFiles; ++i)
    {
      UtString filename;
      filename << "cfile" << mode << "_" << i << ".txt";
      UInt32 bufferSize = UtOBStream::cDefaultBufferSize;
      if (mode == 0)
        bufferSize = 1 + (i % 99);
      UtOCStream* f = new UtOCStream(filename.c_str(), fs, "w", bufferSize);
      if (!f->is_open())
      {
        fprintf(stderr, "%s\n", f->getErrmsg());
        return 1;
      }
      files[i] = f;
    }

    // Write each statement to the 2000 files round-robin through the files
    for (int which = 0; which < NUM_STATEMENTS; ++which)
    {
      for (int i = 0; i < numFiles; ++i)
      {
        test_stream(*files[i], which);
      }
    }

    // Close all the files and record statistics
    for (int i = 0; i < numFiles; ++i)
    {
      UtOCStream* file = files[i];
      if (!file->close())
      {
        fprintf(stderr, "%s\n", file->getErrmsg());
        return 1;
      }
      else
        UtIO::cout() << "[cached-file " << i << " required "
                     << file->getNumWrites()
                     << " writes]" << UtIO::endl;

      delete file;
    }

    UtIO::cout() << "[Cached File System required " <<
      fs->getNumReopens() << " reopens]" << UtIO::endl;
    delete fs;
  } // for

  // negative testing.  try to write UtOBStream to /etc
  {
#if pfWINDOWS
    // In our Windows test environment, T: is /tools, which shouldn't be writable.
    UtOBStream no_open("T:/cannot_write.txt");
#else
    UtOBStream no_open("/etc/cannot_write.txt");
#endif
    if (!no_open.is_open())
      UtIO::cout() << no_open.getErrmsg() << UtIO::endl;
  }

  // Make directory disappear for a re-open
  {
    UtString errmsg, path;
    if (OSMkdir("cfile_subdir", 0770, &errmsg) != 0)
    {
      UtIO::cout() << errmsg << UtIO::endl;
      return 1;
    }
    else
    {
      UtCachedFileSystem fs(1); // that's right, just allow 1 descriptor
      OSConstructFilePath(&path, "cfile_subdir", "file1.txt");
      UtOCStream f1(path.c_str(), &fs, "w");
      f1 << "asdf";
      f1.flush();
      if (f1.bad()) {
        UtIO::cout() << f1.getErrmsg() << UtIO::endl;
      }
      
      UtOCStream f2("file2.txt", &fs, "w"); // not in subdir
      f2 << "asdf";
      f2.flush();               // this should close f1, but leave f2 open
      if (f2.bad()) {
        UtIO::cout() << f2.getErrmsg() << UtIO::endl;
      }
      assert(!f1.UtOBStream::is_open());
      assert(f1.is_open());
      // Now, remove that subdir.  The file can't be re-opened
      if (OSDeleteRecursive("cfile_subdir", &errmsg) != 0)
      {
        UtIO::cout() << errmsg << UtIO::endl;
        return 1;
      }
      f1 << "1234";
      f1.flush();
      assert(f1.bad());
      UtIO::cout() << f1.getErrmsg() << UtIO::endl;
    } // else
  } // Make directory disappear for a re-open

  return 0;
} // int streamtest
