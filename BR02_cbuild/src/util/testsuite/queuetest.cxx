//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/UtQueue.h"
#include "util/CarbonAssert.h"

int queuetest() {
  UtQueue<int> d;

  assert(d.empty());
  d.push_back(4);               // Physically queue = {[4],r=0,w=1}
  d.push_back(5);               // Physically queue = {[4,5],r=0,w=2}
  d.push_back(6);               // Physically queue = {[4,5,6],r=0,w=3}
  d.push_back(7);               // Physically queue = {[4,5,6,7],r=0,w=4}

  assert(d.size() == 4);

  assert(d.front() == 4);
  d.pop_front();               // Physically queue = {[x,5,6,7],r=1,w=4}
  assert(d.front() == 5);
  d.pop_front();               // Physically queue = {[x,x,6,7],r=2,w=4}

  assert(d.size() == 2);

  // Check the internal capacity here, because we want to test whether
  // we are using excessive space in the queue -- we want to make sure
  // we are wrapping.
  assert(d.capacity() == 4);

  d.push_back(8);              // Physically queue = {[8,x,6,7],r=2,w=1}
  assert(d.size() == 3);
  assert(d.capacity() == 4);   // if we failed to wrap around it would be 8

  // The next push_back will force the queue to re-arrange itself so it
  // has room to grow, although it will not actually grow until needed
  d.push_back(9);              // Physically queue = {[6,7,8,9],r=0,w=4}
  assert(d.size() == 4);
  assert(d.capacity() == 4);

  assert(d.front() == 6);
  d.pop_front();               // Physically queue = {[x,7,8,9],r=1,w=4}
  assert(d.size() == 3);
  assert(d.front() == 7);
  d.pop_front();               // Physically queue = {[x,x,8,9],r=2,w=4}
  assert(d.size() == 2);
  assert(d.capacity() == 4);

  // The next push-back could have wrapped around to array[0], but
  // because the array's capacity is 8, we might as well use that capacity
  // before wrapping around.  I believe this strategy can give us less
  // frequent re-shuffles
  d.push_back(10);             // Physically queue = {[x,x,8,9,10],r=2,w=5}
  assert(d.capacity() == 5);

  assert(d.front() == 8);
  d.pop_front();               // Physically queue = {[x,x,x,9,10],r=3,w=5}

  assert(d.size() == 2);
  assert(d.front() == 9);
  d.pop_front();               // Physically queue = {[x,x,x,x,10],r=4,w=5}
  assert(d.size() == 1);
  assert(d.front() == 10);
  d.pop_front();               // Physically queue = {[x,x,x,x,x],r=0,w=0}
  assert(d.size() == 0);
  assert(d.empty());
  assert(d.capacity() == 5);

  // Chase r/w around the queue.  make sure that even inserting and
  // removing 20 items does not change the capacity from 5.
  for (int i = 100; i < 120; ++i) {
    d.push_back(i);
    assert(d.front() == i);
    assert(d.size() == 1);
    d.pop_front();
  }
  assert(d.capacity() == 5);

  d.clear();
  assert(d.capacity() == 0);


  d.push_back(0x428eaa80);
  d.push_back(0x428ea9c0);
  d.push_back(0x428ea9a0);
  d.push_back(0x428ea980);
  d.pop_front();
  d.pop_front();
  d.pop_front();
  d.pop_front();
  assert(d.empty());
  d.push_back(0x428ea980);
  d.push_back(0x428ea9c0);
  d.push_back(0x428eaa80);
  d.push_back(0x428ea960);
  d.push_back(0x428ea9a0);
  d.push_back(0x428ea940);
  d.push_back(0x428ea900);
  d.push_back(0x428ea8c0);
  d.push_back(0x428ea880);
  d.push_back(0x428ea860);
  d.push_back(0x428ea820);
  d.push_back(0x428ea140);
  d.push_back(0x4223dd20);
  d.push_back(0x428ea7e0);
  d.push_back(0x428ea800);
  d.push_back(0x428ea760);
  assert(d.front() == 0x428ea980);
/*
front transaction: 0x428eaa80
front transaction: 0x428eaa80
The write data at CMR (0x0005) is: 0xe800.
pop   transaction
front transaction: 0x428ea9c0
front transaction: 0x428ea9c0
The write data at ADDR (0x0006) is: 0.
pop   transaction
front transaction: 0x428ea9a0
front transaction: 0x428ea9a0
front transaction: 0x428ea9a0
The read data at GSRH (0x0001) is: 0x8000.
pop   transaction
front transaction: 0x428ea980
front transaction: 0x428ea980
The write data at ADDR (0x0006) is: 0xe.
pop   transaction
front transaction: 0x428ea980
*/

  return 0;
} // int queuetest
