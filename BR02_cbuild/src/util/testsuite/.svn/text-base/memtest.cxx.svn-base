// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
// test the memory management subsystem

#include "util/MemManager.h"
#include "util/UtStream.h"
#include "util/UtIOStream.h"
#include "util/UtVector.h"
//#include "util/UtString.h"
//#include <fstream>
#include "util/UtString.h"
#include "util/UtVector.h"
#include "util/Zstream.h"
#include "../util/MemSystem.h"
#include <cassert>
#include "util/Stats.h"
#include "util/UtHashMap.h"
#include "util/Timing.h"
#include "util/RandomValGen.h"

static void doStuff(bool doIO)
{
  // Allocate & free all the strings in /usr/dict & validate
  // that we have no trapped memory afterwards
  if (doIO)
    CarbonMem::printStats();

  Zistream* ZISTREAM_ALLOC(f, "words");
  assert(f);
  UtString* buf = new UtString;
  UtVector<char*> strs;

  if (doIO)
  {
    UtIO::cout() << "before file-read() ..." << UtIO::endl;
    CarbonMem::printStats();
  }

  UInt64 accum = 0;
  while (f->readline(buf))
  {
    UInt64 trapped = CarbonMem::getBytesTrapped();
    accum += trapped;
    char* str = new char[buf->size() + 1];
    strcpy(str, buf->c_str());
    strs.push_back(str);
    buf->clear();
  }
  delete f;

  if (doIO)
  {
    UtIO::cout() << "accumulated trapped " << accum << UtIO::endl;
    UtIO::cout() << "after file-read() ..." << UtIO::endl;
    CarbonMem::printStats();
  }

  for (UInt32 i = 0; i < strs.size(); ++i)
  {
    char* str = strs[i];
    delete [] str;
  }
  if (doIO)
  {
    UtIO::cout() << "after deletions ..." << UtIO::endl;
    CarbonMem::printStats();
  }
  delete buf;
} // static void doStuff



// Replay memory accesses that were harvested by running cbuild with
// a MemSystem.cxx that was compiled by LOG_MEM_OPS.  That produces
// a script like this:
/*
    CarbonMalloc 20 0x403638f4
    CarbonFree 0x40190e18 0
    CarbonFree 0x403638f4 20
    CarbonFree 0x40190e20 0
    CarbonFree 0x401d6270 20
    CarbonFree 0x40190d68, 0
    CarbonMalloc 4 0x40190d68
    CarbonMalloc 4 0x40190e20
    ...
*/
extern void MemCheckFreeList(int);

static int debug_line = -1;

static void replayMemScript(const char* filename, bool doIO)
{
  if (doIO)
    UtIO::cout() << "Replaying memory allocs from " << filename << UtIO::endl;
  Zistream* ZISTREAM_ALLOC(f, filename);

  // Note that we may not get the same pointers as the original
  // cbuild run.  So we need to keep a mapping of all the pointers
  // so we free them in the same order.  Of course we need memory
  // for that
  typedef UtHashMap<size_t, std::pair<void*,UInt32> > AllocMap;
  AllocMap allocs;
  UtString buf;
  int line = 0;
  while (f->readline(&buf))
  {
    ++line;
    if (line == debug_line)
    {
      fprintf(stdout, "Stop!\n");
    }

    union {void* v;
      UIntPtr p; } cbuildPtr;

    UInt32 size;
    if (sscanf(buf.c_str(), "CarbonMalloc %u " FormatPtr, &size, &cbuildPtr.v) == 2)
    {
      void* ptr = CarbonMem::allocate(size);
      assert(allocs.find(cbuildPtr.p) == allocs.end());
      allocs[cbuildPtr.p] = std::pair<void*,UInt32>(ptr,size);
    }
    else if (sscanf(buf.c_str(), "CarbonFree " FormatPtr " %u", &cbuildPtr.v, &size)
             == 2)
    {
      AllocMap::iterator p = allocs.find(cbuildPtr.p);
      assert(p != allocs.end());
      void* ptr = p->second.first;
      allocs.erase(p);
/*
      if (size <= 65536)
      {
        void* fixedPtr;
        assert(CarbonGetSize(ptr, &fixedPtr, false) == size);
      }
*/
      assert(ptr);
      CarbonMem::deallocate(ptr, size);
    }
    MemCheckFreeList(1);
  }

  // The memory playback file might not have been from a clean run, so clean
  // it all up now
  while (!allocs.empty())
  {
    AllocMap::iterator p = allocs.begin();
    CarbonMem::deallocate(p->second.first, p->second.second);
    allocs.erase(p);
  }
  delete f;
} // static void replayMemScript

static void testBlockRelease(bool testTotalBytes, int numMeg)
{
  Stats stats;
  double startBytesAlloced = stats.getMemAlloced();

  // Allocate 240M of 32k chunks and then release them all.  Hopefully
  // the memory will be released to the system almost entirely.
  const int K = 1024;
  const int M = K*K;
  const int size = 32*K;
  int numDesired = numMeg * M;
  const int numAllocs = numDesired / size;
  UtVector<char*> allocs(numAllocs);
  for (int i = 0; i < numAllocs; ++i)
  {
    allocs[i] = new char[size];
    memset(allocs[i], 0, size);
  }
  if (testTotalBytes)
  {
    double maxBytesAlloced = stats.getMemAlloced();
    assert(maxBytesAlloced - startBytesAlloced > 200);
  }

  for (int i = 0; i < numAllocs; ++i)
  {
    delete [] allocs[i];
  }
  CarbonMem::releaseBlocks();

  if (testTotalBytes && !CarbonMem::isValgrindRunning() &&
      !CarbonMem::isPurifyRunning())
  {
#if pfLINUX || pfLINUX64
    // RH 7.3 at least can release memory back to the OS.  Solaris cannot,
    // at least not via free(), which is the method employed by
    // CarbonMem::releaseBlocks.
    double endBytesAlloced = stats.getMemAlloced();
    assert(endBytesAlloced - startBytesAlloced < 30);
#endif
  }
} // static void testBlockRelease

// In this one, we test the allocating and freeing of a very large number
// of like-size items.  We are concerned this might be n^2 in the number
// of free items
static void largeNumAllocsTest() {
  const size_t mem_to_alloc = 100*1000*1000;
  const size_t alloc_size = 4;
  const size_t num_allocs = mem_to_alloc / alloc_size;
  const size_t num_skip = 1000;

  static char* allocs[num_allocs];

  // try a few different patterns of memory allocation and freeing

  Timing timing;

  // 1. free in same order as alloc
  timing.start();
  for (size_t i = 0; i < num_allocs; ++i) {
    allocs[i] = new char[alloc_size];
  }
  for (size_t i = 0; i < num_allocs; ++i) {
    delete [] allocs[i];
  }
  timing.printAndReset("\nfree in same order as alloc");
  CarbonMem::printStats();
  timing.printAndReset("stats");

  // 2. free in opposite order as alloc
  for (int i = num_allocs - 1; i >= 0; --i) {
    allocs[i] = new char[alloc_size];
  }
  for (size_t i = 0; i < num_allocs; ++i) {
    delete [] allocs[i];
  }
  timing.printAndReset("\nfree in opposite order as alloc");
  CarbonMem::printStats();
  timing.printAndReset("stats");

  // 3. free interleaved in same order as alloc
  for (size_t i = 0; i < num_allocs; ++i) {
    allocs[i] = new char[alloc_size];
  }
  for (size_t skip = 0; skip < num_skip; ++skip) {
    for (size_t i = skip; i < num_allocs; i += num_skip) {
      delete [] allocs[i];
    }
  }
  timing.printAndReset("\nfree interleaved in same order as alloc");
  CarbonMem::printStats();
  timing.printAndReset("stats");

  // 4. free interleaved in opposite order as alloc
  for (int i = num_allocs - 1; i >= 0; --i) {
    allocs[i] = new char[alloc_size];
  }
  for (size_t skip = 0; skip < num_skip; ++skip) {
    for (size_t i = skip; i < num_allocs; i += num_skip) {
      delete [] allocs[i];
    }
  }
  timing.printAndReset("\nfree interleaved in opposite order as alloc");
  CarbonMem::printStats();
  timing.printAndReset("stats");

  // 5. randomly shuffle order
  for (size_t i = 0; i < num_allocs; ++i) {
    allocs[i] = new char[alloc_size];
  }
  RandomValGen rand(0);
  for (size_t i = 0; i < num_allocs; ++i) {
    size_t j = rand.URRandom(0, num_allocs - 1);
    char* tmp = allocs[j];
    allocs[j] = allocs[i];
    allocs[i] = tmp;
  }
  for (size_t i = 0; i < num_allocs; ++i) {
    delete [] allocs[i];
  }
  timing.printAndReset("\nrandomly shuffle order");
  CarbonMem::printStats();
  timing.printAndReset("stats");
} // static void largeNumAllocsTest

static void alloc_track(bool small) {
  RandomValGen rand(0);

  // allocate/deallocate
  UInt32 bytesAlloced = CarbonMem::getBytesAllocated();

  int iters = small ? 1000 : 100000;
  int range = small ? 10000 : 200000;

  for (int i = 0; i < iters; ++i) {
    size_t sz = rand.URRandom(0, range);
    void* ptr = CarbonMem::allocate(sz);
    CarbonMem::deallocate(ptr, sz);
    assert(bytesAlloced == CarbonMem::getBytesAllocated());
  }

  // new/delete
  for (int i = 0; i < iters; ++i) {
    size_t sz = rand.URRandom(0, range);
    char* ptr = new char[sz];
    delete [] ptr;
    assert(bytesAlloced == CarbonMem::getBytesAllocated());
  }

  // malloc/free
  for (int i = 0; i < iters; ++i) {
    size_t sz = rand.URRandom(0, range);
    void* ptr = CarbonMem::malloc(sz);
    CarbonMem::free(ptr);
    assert(bytesAlloced == CarbonMem::getBytesAllocated());
  }
}

static void random_fill_bytes(unsigned char* buf, UInt32 numBytes, UInt32 seed) {
  RandomValGen rand(seed);
  for (UInt32 i = 0; i < numBytes; ++i) {
    UInt32 c = rand.URRandom(0, 255);
    buf[i] = c;
  }
}

static void random_test_bytes(const unsigned char* buf, UInt32 numBytes, UInt32 seed) {
  RandomValGen rand(seed);
  for (UInt32 i = 0; i < numBytes; ++i) {
    UInt32 c = rand.URRandom(0, 255);
    assert((UInt32) buf[i] == (UInt32) c);
  }
}

static unsigned char* test_realloc_once(unsigned char* buf,
                                        UInt32 old_size, UInt32 new_size,
                                        UInt32 old_seed, UInt32 new_seed,
                                        bool overhead)
{
  if (overhead) {
    buf = (unsigned char*) carbonmem_realloc(buf, new_size);
  }
  else {
    buf = (unsigned char*) CarbonMem::reallocate(buf, old_size, new_size);
  }
  random_test_bytes(buf, std::min(old_size, new_size), old_seed);
  random_fill_bytes(buf, new_size, new_seed);
  return buf;
}

static void test_realloc_random(bool overhead, bool small) {
  UInt32 orig_bytes = CarbonMem::getBytesAllocated();

  UInt32 range = 200000;
  UInt32 iters = 1000;
  if (small) {
    range = 100;
    iters = 10;
  }

  RandomValGen rand(600);
  UInt32 prev_size = rand.URRandom(1, range);
  unsigned char* buf = overhead
    ? (unsigned char*) carbonmem_malloc(prev_size)
    : (unsigned char*) CarbonMem::allocate(prev_size);
  random_fill_bytes(buf, prev_size, 0);
  for (UInt32 i = 1; i < iters; ++i) {
    UInt32 next_size = rand.URRandom(1, range);
    buf = test_realloc_once(buf, prev_size, next_size, i - 1, i, overhead);
    prev_size = next_size;
  }
  if (overhead) {
    carbonmem_free(buf);
  }
  else {
    CarbonMem::deallocate(buf, prev_size);
  }
  UInt32 final_bytes = CarbonMem::getBytesAllocated();
  assert(orig_bytes == final_bytes);
}

void test_realloc_directed(bool overhead) {
  UInt32 orig_bytes = CarbonMem::getBytesAllocated();

  // Ensure that the pointer does not change when we realloc within a known
  // size-bucket.  This is dependent on knowledge of the memory system
  // implementation.
  unsigned char* buf = overhead
    ? (unsigned char*) carbonmem_malloc(50000)
    : (unsigned char*) CarbonMem::allocate(50000);
  random_fill_bytes(buf, 50000, 0);
  unsigned char* buf2 = test_realloc_once(buf, 50000, 60000, 0, 1, overhead);
  assert(buf == buf2);

  // test to make sure we can go to a new bucket size and that
  // our memory usage goes down
  UInt32 old_bytes = CarbonMem::getBytesAllocated();
  buf = test_realloc_once(buf, 60000, 10000, 1, 2, overhead);
  assert(buf != buf2);
  UInt32 new_bytes = CarbonMem::getBytesAllocated();
  assert(new_bytes < old_bytes);
    
  // test that we can realloc from <64k to >64k, where we have to
  // use the system malloc
  buf = test_realloc_once(buf, 10000, 70000, 2, 3, overhead);
  buf = test_realloc_once(buf, 70000, 200000, 3, 4, overhead);
  buf = test_realloc_once(buf, 200000, 30000, 4, 5, overhead);

  if (overhead) {
    carbonmem_free(buf);
  }
  else {
    CarbonMem::deallocate(buf, 30000);
  }

  UInt32 final_bytes = CarbonMem::getBytesAllocated();
  assert(orig_bytes == final_bytes);
} // void test_realloc_directed

int memtest(int argc, char** argv, bool doIO)
{
  int ret = 0;
  if ( doIO ) {
    // before we get the baseline allocCount and trapCount we access the
    // three streams that have memory allocated for them upon their first
    // access (this memory is owned by UtIO but is released by the
    // memory system just before the final memory cleanup)
    UtOStream *unused1 = &UtIO::cout();
    UtOStream *unused2 = &UtIO::cerr();
    UtIStream *unused3 = &UtIO::cin();

    // use the values to keep them live even in the smartest compilers
    if ( ( unused1 == NULL ) or ( unused2 == NULL ) or ( unused3 == NULL )){
      UtIO::cout() << "Stream setup error ";
      ++ret;
    }
  }
  
  UInt32 allocCount = doIO? CarbonMem::getBytesAllocated(): 0;
  UInt32 trapCount = doIO? CarbonMem::getBytesTrapped(): 0;

  //CarbonMem::overrideNewDelete();

  if (doIO)
    CarbonMem::printStats();
#if pfWINDOWS || pfLP64
  // Stats::getMemAlloced() currently returns 0 on Windows.
  // The test needs to be calibrated better on Linux64
  bool testTotalBytes = false;
#else
  bool testTotalBytes = doIO;
#endif
  testBlockRelease(testTotalBytes, doIO? 240: 50); // multi-threading - just do 50M
  if (doIO)
    CarbonMem::printStats();
  for (int i = 1; i < argc; ++i)
    replayMemScript(argv[i], doIO);
  if (doIO)
    CarbonMem::printStats();
  doStuff(doIO);
  if (doIO) {
    CarbonMem::printStats();
    UtIO::cout() << "Random realloc...\n";
  }
  bool small = ! doIO;              // in thread test, only do a few iters
  test_realloc_random(false, small);
  test_realloc_random(true, small);
  if (doIO) {
    CarbonMem::printStats();
    UtIO::cout() << "Directed realloc...\n";
  }
  test_realloc_directed(false);
  test_realloc_directed(true);
  if (doIO)
    CarbonMem::printStats();

  if (doIO)
  {
    if (CarbonMem::getBytesAllocated() != allocCount)
    {
      UtIO::cout() << (UInt32) CarbonMem::getBytesAllocated()
                << " bytes allocated, expected " << allocCount << UtIO::endl;
      ++ret;
    }

    // we will trap memory when doing memory debugging.  allow that.
    if ((CarbonMem::getBytesTrapped() != trapCount) &&
        (getenv("CARBON_MEM_DEBUG") == NULL))
    {
      UtIO::cout() << (UInt32)CarbonMem::getBytesTrapped()
                << " bytes trapped, expected " << trapCount << UtIO::endl;
      ++ret;
    }
  }

  alloc_track(small);

  Timing timing;
  if (doIO) {
    timing.start();
    largeNumAllocsTest();
    UtIO::cout() << "Total alloc stress-test time: ";
    timing.end();
    timing.printDeltas();
  }

  return ret;
} // int memtest
