// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtIZStream.h"
#include "util/UtIOStream.h"
#include "util/CarbonTypes.h"
#include "util/UtCachedFileSystem.h"
#include "util/OSWrapper.h"
#include "util/CarbonAssert.h"
#include "util/DynBitVector.h"
#include "util/BitVector.h"

// This test framework is base on reading istreamtest.in, which is
// similar to the expected results from "streamtest", which really
// just tests output streams.

// This test routine facilitates testing cached files by interleaving
// the reading of numerous independent files by specifing which phase
// to run.
#define ALL_PHASES -1
static int test_stream(UtIStream& f, int which = ALL_PHASES)
{
  int ret = 0;

# define DO_IT_CMP(n, expr, cmp) if ((which == ALL_PHASES) || (which == n)) { \
  expr; \
  if (f.bad()) {UtIO::cerr() << f.getErrmsg() << "\n"; assert(0); } \
  assert(cmp); \
}
# define DO_IT_STR(n, var, expect) if ((which == ALL_PHASES) || (which == n)) { \
  f >> var; \
  if (f.bad()) {UtIO::cerr() << f.getErrmsg() << "\n"; assert(0); } \
  assert(strcmp(var.c_str(), expect) == 0); \
}
# define DO_IT(n, var, expect) if ((which == ALL_PHASES) || (which == n)) { \
  f >> var; \
  if (f.bad()) {UtIO::cerr() << f.getErrmsg() << "\n"; assert(0); } \
  assert(var == expect); \
}

  UtString strBuf;

  // Token1 512_byte_tok... )(*&*(&	1298374.9821734e5
  DO_IT_STR(0, strBuf, "Token1");
  DO_IT_CMP(0, f >> strBuf,
            ((strncmp(strBuf.c_str(), "512_byte_tok", 12) == 0) &&
             (strBuf.size() == 512)));
  DO_IT_STR(1, strBuf, ")(*&*(&");
  DO_IT_STR(2, strBuf, "1298374.9821734e5");
  char c;
  DO_IT(3, c, '\n');

  // A line of text
  DO_IT_CMP(4, f.getline(&strBuf),
            strcmp(strBuf.c_str(), "A line of text\n") == 0);

  // 10 billion, portably, without using ULL
  UInt64 tenBillion;
  DO_IT(5, tenBillion, ((UInt64)10000*((UInt64)1000*1000)));

  SInt64 negTenBillion;
  DO_IT(6, negTenBillion, -(((SInt64) 10000)*(1000*1000)));

  SInt32 neg100k;
  DO_IT(7, neg100k, -100000);

  SInt16 neg10k;
  DO_IT(7, neg10k, -10000);

  UInt32 fourBillion;
  DO_IT(7, fourBillion, 4UL*1000UL*1000UL*1000UL); 

  UInt16 sixtyFourK;
  DO_IT(8, sixtyFourK, 0xFFFF);

  SInt32 neg32K;
  DO_IT(9, neg32K, -32000);

  // note -- double-precision comparisons are risky!
  double pi;
  //DO_IT_CMP(8, f >> pi, std::fabs(pi - 3.14592654) < .0001);
  DO_IT(10, pi, 3.14592654);

  DO_IT(11, c, '\n');            // left over from pi
  DO_IT(12, c, 'c');

  // blank line
  DO_IT_CMP(13, f.getline(&strBuf),
            strcmp(strBuf.c_str(), "\n") == 0);

  void* ptr;
  DO_IT(14, ptr, NULL);
  
  UInt32 i1, i2;
# define DO_IT_VAL(n, radix, radixStr, val) \
  DO_IT_CMP(n, f >> strBuf >> UtIO::radix >> i1 >> i2, \
            (i1 == val) && (i2 == val) && (strcmp(strBuf.c_str(), radixStr) == 0))


  DO_IT_VAL(15, hex, "hex", 20);
  DO_IT_VAL(16, oct, "oct", 20);
  DO_IT_VAL(17, dec, "dec", 20);
  DO_IT_VAL(18, sdec, "sdec", 20);

  double d1, d2;
# define DO_IT_FVAL(n, radix, radixStr, val) \
  DO_IT_CMP(n, f >> strBuf >> UtIO::radix >> d1 >> d2, \
            (d1 == val) && (d2 == val) && (strcmp(strBuf.c_str(), radixStr) == 0))
  DO_IT_FVAL(19, general, "general", 3.14159);
  DO_IT_FVAL(20, fixed, "fixed", 3.141593);
  DO_IT_FVAL(21, exponent, "exponent", 3.141593e+00);

  DO_IT(22, c, '\n');            // left over from pi
  DO_IT_CMP(23, f.getline(&strBuf),
            strcmp(strBuf.c_str(), " Col1 Col2 Col3 Col4 Col5\n") == 0);

  //  1    2    3    4   5 
  DO_IT_CMP(24, f >> UtIO::setw(5) >> i1, i1 == 1);
  DO_IT_CMP(25, f >> UtIO::setw(5) >> i1, i1 == 2);
  DO_IT_CMP(26, f >> UtIO::setw(5) >> i1, i1 == 3);
  DO_IT_CMP(27, f >> UtIO::setw(5) >> i1, i1 == 4);
  DO_IT_CMP(28, f >> UtIO::setw(5) >> i1, i1 == 5);

  // 1 2 3 4 5
  DO_IT_CMP(29, f >> i1, i1 == 1);
  DO_IT_CMP(30, f >> i1, i1 == 2);
  DO_IT_CMP(31, f >> i1, i1 == 3);
  DO_IT_CMP(32, f >> i1, i1 == 4);
  DO_IT_CMP(33, f >> i1, i1 == 5);

  // 12345
  DO_IT_CMP(34, f >> UtIO::setw(1) >> i1, i1 == 1);
  DO_IT_CMP(35, f >> UtIO::setw(1) >> i1, i1 == 2);
  DO_IT_CMP(36, f >> UtIO::setw(1) >> i1, i1 == 3);
  DO_IT_CMP(37, f >> UtIO::setw(1) >> i1, i1 == 4);
  DO_IT_CMP(38, f >> UtIO::setw(1) >> i1, i1 == 5);

  // HEX 14 14
  DO_IT_CMP(39, f >> "HEX" >> UtIO::HEX >> i1 >> i2,
            (i1 == 0x14) && (i2 == 0x14));

  // bin 00000000000000000000000000010100 00000000000000000000000000010100
  DO_IT_CMP(40, f >> "bin" >> UtIO::bin >> i1  >> i2,
            (i1 == 0x14) && (i2 == 0x14));

  // Big numbers
  DynBitVector dbv(300), tdbv(300);       // That's right, baby, 300 bits.

  // First, test an easy-to-verify decimal number
  // Generate 10^50, and see how it prints in various radices.
  dbv = 1000000000;              // 10^9
  for (int i = 0; i < 41; ++i)  // 41+9 = 50
    dbv.multBy10();

  DO_IT_CMP(41, f >> "hex" >> UtIO::hex >> tdbv, tdbv == dbv);
  DO_IT_CMP(42, f >> "HEX" >> UtIO::HEX >> tdbv, tdbv == dbv);
  DO_IT_CMP(43, f >> "oct" >> UtIO::oct >> tdbv, tdbv == dbv);
  DO_IT_CMP(44, f >> "dec" >> UtIO::dec >> tdbv, tdbv == dbv);
  DO_IT_CMP(45, f >> "sdec" >> UtIO::sdec >> tdbv, tdbv == dbv);
  DO_IT_CMP(46, f >> "bin" >> UtIO::bin >> tdbv, tdbv == dbv);
  f >> UtIO::dec;
  
  // Now, test an easy-to-verify hex number
  // Think of all the hex numbers that make words and shift them in
  dbv  = 0xbeefcafe;   dbv <<= 36;
  dbv |= 0xfeedface;   dbv <<= 36;
  dbv |= 0xdeadbeef;   dbv <<= 36;
  dbv |= 0xdeadface;   dbv <<= 36;
  dbv |= 0xcafedead;   dbv <<= 36;
  
  DO_IT_CMP(47, f >> "hex" >> UtIO::hex >> tdbv, tdbv == dbv);
  DO_IT_CMP(48, f >> "HEX" >> UtIO::HEX >> tdbv, tdbv == dbv);
  DO_IT_CMP(49, f >> "oct" >> UtIO::oct >> tdbv, tdbv == dbv);
  DO_IT_CMP(50, f >> "dec" >> UtIO::dec >> tdbv, tdbv == dbv);
  DO_IT_CMP(51, f >> "sdec" >> UtIO::sdec >> tdbv, tdbv == dbv);
  DO_IT_CMP(52, f >> "bin" >> UtIO::bin >> tdbv, tdbv == dbv);
  f >> UtIO::dec;
  
  // ParseThreeWords
  DO_IT_CMP(53, f >> UtIO::setw(5) >> strBuf,
            strcmp(strBuf.c_str(), "Parse") == 0);
  DO_IT_CMP(54, f >> UtIO::setw(5) >> strBuf,
            strcmp(strBuf.c_str(), "Three") == 0);
  DO_IT_CMP(55, f >> UtIO::setw(5) >> strBuf,
            strcmp(strBuf.c_str(), "Words") == 0);

  // parse three words
  DO_IT_CMP(56, f >> UtIO::setw(17) >> strBuf,
            strcmp(strBuf.c_str(), "parse") == 0);
  DO_IT_CMP(57, f >> UtIO::setw(17) >> strBuf,
            strcmp(strBuf.c_str(), "three") == 0);
  DO_IT_CMP(58, f >> UtIO::setw(17) >> strBuf,
            strcmp(strBuf.c_str(), "words") == 0);

  DO_IT_CMP(59, f >> strBuf,
            ((strncmp(strBuf.c_str(), "512_byte_tok", 12) == 0) &&
             (strBuf.size() == 512)));
  DO_IT_CMP(60, f >> strBuf,
            ((strncmp(strBuf.c_str(), "100_byte_tok", 12) == 0) &&
             (strBuf.size() == 100)));
  DO_IT_CMP(61, f >> strBuf,
            ((strncmp(strBuf.c_str(), "99_byte_tok", 11) == 0) &&
             (strBuf.size() == 99)));

  DO_IT(62, c, '\n');
  DO_IT_CMP(63, f.getline(&strBuf),
            ((strncmp(strBuf.c_str(), "A line of text with ", 20) == 0) &&
             (strBuf.size() == 98)));
  DO_IT_CMP(64, f.getline(&strBuf),
            ((strncmp(strBuf.c_str(), "A line of text with ", 20) == 0) &&
             (strBuf.size() == 99)));
  DO_IT_CMP(65, f.getline(&strBuf),
            ((strncmp(strBuf.c_str(), "A line of text with ", 20) == 0) &&
             (strBuf.size() == 100)));
  DO_IT_CMP(66, f.getline(&strBuf),
            ((strncmp(strBuf.c_str(), "A line of text with ", 20) == 0) &&
             (strBuf.size() == 101)));
  DO_IT_CMP(68, f.getline(&strBuf),
            ((strncmp(strBuf.c_str(), "A line of text with ", 20) == 0) &&
             (strBuf.size() == 102)));
  DO_IT_CMP(69, f.getline(&strBuf),
            ((strncmp(strBuf.c_str(), "A line of text with ", 20) == 0) &&
             (strBuf.size() == 198)));
  DO_IT_CMP(70, f.getline(&strBuf),
            ((strncmp(strBuf.c_str(), "A line of text with ", 20) == 0) &&
             (strBuf.size() == 199)));
  DO_IT_CMP(71, f.getline(&strBuf),
            ((strncmp(strBuf.c_str(), "A line of text with ", 20) == 0) &&
             (strBuf.size() == 200)));
  DO_IT_CMP(72, f.getline(&strBuf),
            ((strncmp(strBuf.c_str(), "A line of text with ", 20) == 0) &&
             (strBuf.size() == 201)));

  // 100x100x100x100x
  DO_IT_CMP(73, f >> UtIO::bin >> i1, i1 == 4);
  DO_IT(74, c, 'x');
  DO_IT_CMP(75, f >> UtIO::oct >> i1, i1 == 64);
  DO_IT(76, c, 'x');
  DO_IT_CMP(77, f >> UtIO::dec >> i1, i1 == 100);
  DO_IT(78, c, 'x');
  DO_IT_CMP(79, f >> UtIO::hex >> i1, i1 == 256);

  // 10000000000000000000000000000000000000000000000x10000000000000000000000000000000000000000000000x10000000000000000000000000000000000000000000000x10000000000000000000000000000000000000000000000
  dbv.reset();
  dbv.set(46);
  DO_IT_CMP(80, f >> UtIO::bin >> tdbv, tdbv == dbv);
  DO_IT(81, c, 'x');
  dbv.reset();

  // an 1 with 46*3 0s is next (oct)
  dbv.set(46*3);
  DO_IT_CMP(82, f >> UtIO::oct >> tdbv, tdbv == dbv);
  DO_IT(83, c, 'x');

  // next we have 10^46
  dbv.reset();
  dbv.set(0);
  for (int i = 0; i < 46; ++i)
    dbv.multBy10();
  DO_IT_CMP(84, f >> UtIO::dec >> tdbv, tdbv == dbv);
  DO_IT(85, c, 'x');

  // Finally we have a 1 with 46*4 0s (hex)
  dbv.reset();
  dbv.set(46*4);
  DO_IT_CMP(86, f >> UtIO::hex >> tdbv, tdbv == dbv);


  // Now do all the same bitvector tests with a static bitvector instead of a dynamic one
  // Big numbers
  BitVector<300> sbv, tsbv;       // That's right, baby, 300 bits.

  // First, test an easy-to-verify decimal number
  // Generate 10^50, and see how it prints in various radices.
  sbv = 1000000000;              // 10^9
  for (int i = 0; i < 41; ++i)  // 41+9 = 50
    sbv.multBy10();

  DO_IT_CMP(87, f >> "hex" >> UtIO::hex; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT_CMP(88, f >> "HEX" >> UtIO::HEX; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT_CMP(89, f >> "oct" >> UtIO::oct; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT_CMP(90, f >> "dec" >> UtIO::dec; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT_CMP(91, f >> "sdec" >> UtIO::sdec; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT_CMP(92, f >> "bin" >> UtIO::bin; f.readBitVec(&tsbv), tsbv == sbv);
  f >> UtIO::dec;
  
  // Now, test an easy-to-verify hex number
  // Think of all the hex numbers that make words and shift them in
  sbv  = 0xbeefcafe;   sbv <<= 36;
  sbv |= 0xfeedface;   sbv <<= 36;
  sbv |= 0xdeadbeef;   sbv <<= 36;
  sbv |= 0xdeadface;   sbv <<= 36;
  sbv |= 0xcafedead;   sbv <<= 36;
  
  DO_IT_CMP(93, f >> "hex" >> UtIO::hex; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT_CMP(94, f >> "HEX" >> UtIO::HEX; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT_CMP(95, f >> "oct" >> UtIO::oct; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT_CMP(96, f >> "dec" >> UtIO::dec; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT_CMP(97, f >> "sdec" >> UtIO::sdec; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT_CMP(98, f >> "bin" >> UtIO::bin; f.readBitVec(&tsbv), tsbv == sbv);
  f >> UtIO::dec;
  
  // 10000000000000000000000000000000000000000000000x10000000000000000000000000000000000000000000000x10000000000000000000000000000000000000000000000x10000000000000000000000000000000000000000000000
  sbv.reset();
  sbv.set(46);
  DO_IT_CMP(99, f >> UtIO::bin; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT(100, c, 'x');
  sbv.reset();

  // an 1 with 46*3 0s is next (oct)
  sbv.set(46*3);
  DO_IT_CMP(101, f >> UtIO::oct; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT(102, c, 'x');

  // next we have 10^46
  sbv.reset();
  sbv.set(0);
  for (int i = 0; i < 46; ++i)
    sbv.multBy10();
  DO_IT_CMP(103, f >> UtIO::dec; f.readBitVec(&tsbv), tsbv == sbv);
  DO_IT(104, c, 'x');

  // Finally we have a 1 with 46*4 0s (hex)
  sbv.reset();
  sbv.set(46*4);
  DO_IT_CMP(105, f >> UtIO::hex; f.readBitVec(&tsbv), tsbv == sbv);
  //NOTE if you add a new test, you must update NUM_STATEMENTS
# define NUM_STATEMENTS 107
  
  // Reset to dec unconditionally
  f >> UtIO::dec;

  if ((which == ALL_PHASES) || (which == (NUM_STATEMENTS-1))) {
    // Read past the end of the buffer
    f >> i1;
    assert(f.eof());
  }

  return ret;
} // static int test_stream

static const char testFilename[] = "istreamtest.in";

int istreamtest(void)
{
  int ret = 0;

  // Prepare to test IStringStream by reading the entire
  // file into a string buffer
  UtString buf;
  {
    UtString infileContents;

    UtIO::cout() << "Reading input file...\n";
    UtIBStream infile(testFilename);
    if (!infile.is_open()) {
      UtIO::cerr() << infile.getErrmsg() << UtIO::endl;
      ret = 1;
    }
    else {
      while (infile.getline(&buf)){
        infileContents << buf;
      }

      UtIO::cout() << "Testing UtIStringStream...\n";
      UtIStringStream ss(infileContents);
      ret |= test_stream(ss);

      // Test the unget functionality.  Note that we know what
      // the minimum buffer size is.  It's 200, and it is easier to
      // hit corner cases knowing this.
      UtIBStream file(testFilename, 200);

      // Read some of the file and then put it back, using various mechanisms.  We
      // may not put back precisely what we grabbed, depending on the mechanism, but
      // it should always parse the same

      // Grab a character and put it back.  That should be the easiest case,
      // requiring no extra memmoves and no memory allocations
      UtIO::cout() << "Testing unget character...\n";
      char c;
      file >> c;
      file.unget(&c, 1);
      assert(file.tell() == 0);
      ret |= test_stream(file);

      // Now we've gotten to EOF, so put the entire file contents back.  Our
      // buffer-manipulation strategies will not yield enough free store and we
      // will have to allocate auxiliary storage
      UtIO::cout() << "Testing unget entire file contents...\n";
      UInt32 fileCount = infileContents.size();
      const char* fileContents = infileContents.c_str();
      file.unget(fileContents, fileCount);
      assert(file.tell() == 0);
      file >> UtIO::dec;        // reset to default
      ret |= test_stream(file);

      for (int i = 1; i < NUM_STATEMENTS; i += 10) {
        file.unget(fileContents, fileCount);
        // Read bytes for the first i statements
        for (int j = 0; j < i; ++j) {
          test_stream(file, j);
        }

        // Now, based on the current file position, unget the characters
        // and re-parse the entire file
        file.unget(fileContents, file.tell());
        test_stream(file);
      }

      // For various sizes, try to hit all corner cases by:
      //   reading some bytes (preRead)
      //   ungetting some of those bytes (ungetCount)
      //   re-reading the rest of the file with various byte counts
      char* fileBuf = new char[fileCount];
      for (UInt32 preRead = 1; preRead < 420; ++preRead) {
        for (UInt32 ungetCount = 1; ungetCount <= preRead; ++ungetCount) {
          file.unget(fileContents, file.tell());
          UInt32 count = file.read(fileBuf, preRead);
          assert(count == preRead);
          UInt32 fileOffset = preRead - ungetCount;
          char* ungetStart = fileBuf + fileOffset;
          file.unget(ungetStart, ungetCount);
          count = file.read(fileBuf, fileCount - fileOffset);
          assert(memcmp(fileBuf, fileContents + fileOffset, fileCount - fileOffset) == 0);
        }
        assert(file.tell() == fileCount);
      }
      delete [] fileBuf;
    } // else
  }

  {
    UtIO::cout() << "Testing UtIBStream...\n";
    UtIBStream infile(testFilename);
    ret |= test_stream(infile);
  }

  {
    UtString errmsg;
    FILE* f = OSFOpen(testFilename, "r", &errmsg);
    if (f == NULL) {
      UtIO::cerr() << errmsg << UtIO::endl;
      return 1;
    }
    UtIFileStream ifstr(f);
    UtIO::cout() << "Testing UtIFileStream...\n";
    test_stream(ifstr);
    fclose(f);
  }

  {
    UtString errmsg;
    UTIZSTREAM(f, testFilename);
    if (!f.is_open()) {
      UtIO::cerr() << f.getErrmsg() << UtIO::endl;
      return 1;
    }
    UtIO::cout() << "Testing UtIZStream...\n";
    test_stream(f);
  }

  // Now test a whole lot of open files, buffer sizes varying from 1 to 100
  // (pity the filesystem).  We are going to do this twice.  Once with small
  // buffers, which will cause a lot of thrashing, and once with default
  // buffers, which should just cause 64k writes & reopens
  for (int mode = 0; mode < 2; ++mode)
  {
    UtCachedFileSystem* fs = new UtCachedFileSystem(5);
    const int numFiles = 250;
    UtICStream* files[numFiles];

    if (mode == 0)
      UtIO::cout() << "\n\n____________\n[Small buffers]\n";
    else
      UtIO::cout() << "\n\n____________\n[Default buffers]\n";

    // Open all the 2000 files
    for (int i = 0; i < numFiles; ++i)
    {
      UInt32 bufferSize = UtOBStream::cDefaultBufferSize;
      if (mode == 0)
        bufferSize = 200 + (i % 99);
      UtICStream* f = new UtICStream(testFilename, fs, bufferSize);
      if (!f->is_open())
      {
        fprintf(stderr, "%s\n", f->getErrmsg());
        return 1;
      }
      files[i] = f;
    }

    // Write each statement to the 2000 files round-robin through the files
    for (int which = 0; which < NUM_STATEMENTS; ++which)
    {
      for (int i = 0; i < numFiles; ++i)
      {
        test_stream(*files[i], which);
      }
    }

    // Close all the files and record statistics
    for (int i = 0; i < numFiles; ++i)
    {
      UtICStream* file = files[i];
      if (!file->close())
      {
        fprintf(stderr, "%s\n", file->getErrmsg());
        return 1;
      }
      else
        UtIO::cout() << "[cached-file " << i << " required "
                     << file->getNumReads()
                     << " reads]" << UtIO::endl;

      delete file;
    }

    UtIO::cout() << "[Cached File System required " <<
      fs->getNumReopens() << " reopens]" << UtIO::endl;
    delete fs;
  } // for

  // negative testing.  try to read UtOBStream from /etc
  {
    UtIBStream no_open("/etc/cannot_read.txt");
    if (!no_open.is_open())
      UtIO::cout() << no_open.getErrmsg() << UtIO::endl;
  }

  // Make directory disappear for a re-open
  {
    UtString errmsg, path;
    (void) OSDeleteRecursive("cfile_subdir", &errmsg);
    OSConstructFilePath(&path, "cfile_subdir", "istreamtest.in");

    if ((OSMkdir("cfile_subdir", 0770, &errmsg) != 0) ||
        ! OSCopyFile("istreamtest.in", path.c_str(), &errmsg))
    {
      UtIO::cout() << errmsg << UtIO::endl;
      return 1;
    }
    else
    {
      UtCachedFileSystem fs(1); // that's right, just allow 1 descriptor

      // buffer size of 200 is there so we don't just suck in the
      // whole file on the first read, destroying the interesting
      // aspect of the test.
      UtICStream f1("cfile_subdir/istreamtest.in", &fs, 200);
      f1 >> "Token1";
      if (f1.bad()) {
        UtIO::cout() << f1.getErrmsg() << "\n";
        return 1;
      }
      UtICStream f2(testFilename, &fs, 200); // not in subdir
      f2 >> "Token1";            // this should close f1, but leave f2 open
      if (f2.bad()) {
        UtIO::cout() << f2.getErrmsg() << "\n";
        return 1;
      }
      assert(!f1.UtIBStream::is_open());
      assert(f1.is_open());
      // Now, remove that subdir.  The file can't be re-opened
      UtString errmsg;
      if (OSDeleteRecursive("cfile_subdir", &errmsg) != 0) {
        UtIO::cout() << errmsg << UtIO::endl;
        return 1;
      }
      char buf[10000];
      UInt32 f2Chars = f2.read(buf, 10000); // read the rest of the file
      assert(!f2.bad());
      assert(f2Chars > 200);

      UInt32 f1Chars = f1.read(buf, 10000); // re-open will fail because...
      assert(f1.bad());                     // ...the file is gone
      assert(f1Chars < 200);
      UtIO::cout() << f1.getErrmsg() << UtIO::endl;
    } // else
  } // Make directory disappear for a re-open


  return 0;
} // int istreamtest
