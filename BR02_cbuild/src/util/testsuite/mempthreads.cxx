// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "util/OSWrapper.h"
#include "util/MemManager.h"
#include "util/UtList.h"
#include "util/UtString.h"

int memtest(int argc, char** argv, bool);

struct ArgcArgv
{
  int argc;
  char** argv;
};

static pthread_mutex_t sMutex = PTHREAD_MUTEX_INITIALIZER;
static int activeThreads = 0;
static int statusAccum = 0;
static bool thread_started = false;

#define check_status(pt_status) {if ( (pt_status) != 0 ) {perror("pthread_status not zero"); exit(1);}}

static void* start_thread(void* p)
{
  ArgcArgv* aa = (ArgcArgv*) p;
  int pthread_status = pthread_mutex_lock(&sMutex);
  check_status(pthread_status);

  ++activeThreads;
  fprintf(stdout, "increment active thread count, value now is: %d\n", activeThreads);    fflush(stdout);
  thread_started = true;
  pthread_status = pthread_mutex_unlock(&sMutex);
  check_status(pthread_status);

  int status = memtest(aa->argc, aa->argv, false);
  pthread_status = pthread_mutex_lock(&sMutex);
  check_status(pthread_status);

  --activeThreads;
  statusAccum += status;
  fprintf(stdout, "decrement active thread count, value now is: %d\n", activeThreads);    fflush(stdout);
  pthread_status = pthread_mutex_unlock(&sMutex);
  check_status(pthread_status);

  pthread_exit(NULL);
  return NULL;
}

typedef UtList<UtString*> StringList;

static void* serial_thread(void* p)
{
  StringList* sl = (StringList*) p;
  int pthread_status = pthread_mutex_lock(&sMutex);
  check_status(pthread_status);

  ++activeThreads;
  fprintf(stdout, "increment active thread count, value now is: %d\n", activeThreads);    fflush(stdout);
  sl->push_back(new UtString("hello, world!"));
  pthread_status = pthread_mutex_unlock(&sMutex);
  check_status(pthread_status);

  pthread_exit(NULL);
  return NULL;
}

extern void MemPrintStatsAllThreads();

// this is the main for test/unit-util (subtest
// util/testsuite/pthreadtest.  This checks the thread safe code in
// the carbon memory system

int main(int argc, char** argv)
{
#define NUM_THREADS 10
#define TIMEOUT_LIMIT 200;

  pthread_t threads[NUM_THREADS];
  ArgcArgv aa;
  aa.argc = argc;
  aa.argv = argv;

  unsigned long allocCount = CarbonMem::getBytesAllocated();
  unsigned long trapCount = CarbonMem::getBytesTrapped();
  CarbonMem::printStats();

  pthread_mutex_init(&sMutex, NULL);
  int pthread_status = pthread_mutex_lock(&sMutex);
  check_status(pthread_status);


  // Create all the threads.  They will all wait for our mutex
  for (int i = 0; i < NUM_THREADS; ++i)
  {
    int status = pthread_create (&threads[i],
                                 NULL, // attr
                                 start_thread,
                                 &aa);
    if (status != 0)
    {
      perror("pthread_create");
      exit(1);
    }
  }

  int activation_timer  = TIMEOUT_LIMIT;
  int termination_timer = TIMEOUT_LIMIT;
  int previous_activeThreads = -1;

  // Here is a busy-wait loop, that first checks that at least one
  // thread starts, then waits until all to finish.  Two watchdog
  // timers are used to make sure this loop does not become infinite.
  while (!thread_started || (activeThreads > 0))
  {
    fprintf(stdout, "Active threads: %d\n", activeThreads);
    fflush(stdout);

    if ( previous_activeThreads == activeThreads ) {
      // no progress since last iteration of this loop
      fprintf(stdout, "timers: activation: %d, termination: %d\n", activation_timer, termination_timer);
      fflush(stdout);

      if ( !thread_started ){
        if ( --activation_timer == 0 ){
          perror("TIMEOUT test1 during pthread creation");
          exit(1);
        }
      } else {
        if ( --termination_timer == 0 ){
          perror("TIMEOUT test1 during pthread shutdown");
          exit(1);
        }
      }
    } else {
      // progress was seen since last iteration, reset the watchdog timers
      activation_timer = TIMEOUT_LIMIT;
      termination_timer = TIMEOUT_LIMIT;
    }
    previous_activeThreads = activeThreads;

    // wake up the threads to do some work
    pthread_status = pthread_mutex_unlock(&sMutex);
    check_status(pthread_status);

    OSSleep(4); // Let 'em run for a quad of seconds (2 seconds was not enough time)

    pthread_status = pthread_mutex_lock(&sMutex);
    check_status(pthread_status);

  }

  MemPrintStatsAllThreads();

  // Now do another test where we create some non-concurrent threads,
  // have them allocate memory independently, and then free them all
  // in mainline
  StringList* sl = new StringList;
  for (int i = 0; i < NUM_THREADS; ++i)
  {
    int status = pthread_create (&threads[i],
                                 NULL, // attr
                                 serial_thread,
                                 sl);
    if (status != 0)
    {
      perror("pthread_create");
      exit(1);
    }
  }

  // Wait for the threads to finish
  termination_timer = TIMEOUT_LIMIT;
  while (activeThreads < NUM_THREADS)
  {
    fprintf(stdout, "Active threads: %d\n", activeThreads);
    fflush(stdout);

    if ( --termination_timer == 0 ){
      perror("TIMEOUT test2 waiting for pthreads to finish");
      exit(1);
    }

    // wake up the threads
    pthread_status = pthread_mutex_unlock(&sMutex);
    check_status(pthread_status);


    // Let 'em run for a couple of seconds
    OSSleep(2);
    pthread_status = pthread_mutex_lock(&sMutex);
    check_status(pthread_status);
  }
  for (StringList::iterator p = sl->begin(); p != sl->end(); ++p)
    delete *p;
  sl->clear();
  delete sl;

  MemPrintStatsAllThreads();

  if (CarbonMem::getBytesAllocated() != allocCount)
  {
    fprintf(stdout, "%lu bytes allocated, expected %lu\n",
            (unsigned long) CarbonMem::getBytesAllocated(),
            allocCount);
    ++statusAccum;
  }

  // we will trap memory when doing memory debugging.  allow that.
  if ((CarbonMem::getBytesTrapped() != trapCount) &&
      (getenv("CARBON_MEM_DEBUG") == NULL))
  {
    fprintf(stdout, "%lu bytes allocated, expected %lu\n",
            (unsigned long) CarbonMem::getBytesTrapped(),
            trapCount);
    ++statusAccum;
  }

  return statusAccum;
} // int main
