// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Some testing of the bigraph template utility
*/

#include "util/GenericBigraph.h"
#include "util/GraphSCC.h"
#include "util/UtIOStream.h"

#include "intbigraph.h"

#define INIT_GRAPH_SIZE 8
#define MAX_GRAPH_SIZE (INIT_GRAPH_SIZE + 2)
static IntNode* nodes[MAX_GRAPH_SIZE];

int bigraphtest(int /* argc */, char** /* argv */)
{
  // Create a bigraph of integers
  int status = 0;
  IntGraph* intGraph = new IntGraph;

  // Populate it with INIT_GRAPH_SIZE nodes starting at integer 0
  for (int i = 0; i < INIT_GRAPH_SIZE; ++i) {
    nodes[i] = addNode(intGraph, i);
  }

  // Create a graph with three cycles, one self cycle for node 1, one
  // simple cycle between 2,3, and one complex cycle with sub cycle
  // 4,5,6 and extra edges 5,7,4.
  //
  // The graph starts at 0 and goes to 1 and 7. Then 1 goes to
  // 2. These edges are acyclic.
  addEdge(intGraph, nodes[0], nodes[1], 0);
  addEdge(intGraph, nodes[0], nodes[7], 1);
  addEdge(intGraph, nodes[1], nodes[1], 2);
  addEdge(intGraph, nodes[1], nodes[2], 3);
  addEdge(intGraph, nodes[2], nodes[3], 4);
  addEdge(intGraph, nodes[3], nodes[2], 5);
  addEdge(intGraph, nodes[3], nodes[4], 6);
  addEdge(intGraph, nodes[4], nodes[5], 7);
  addEdge(intGraph, nodes[5], nodes[6], 8);
  addEdge(intGraph, nodes[5], nodes[7], 9);
  addEdge(intGraph, nodes[6], nodes[4], 10);
  addEdge(intGraph, nodes[7], nodes[4], 11);

  // Walk the graph in a sorted manner and print it
  IntNodeCmp nodeCmp(intGraph);
  IntEdgeCmp edgeCmp(intGraph);
  PrintWalker printWalker(intGraph);
  UtIO::cout() << "Cyclic Graph:\n";
  printWalker.walk(intGraph, &nodeCmp, &edgeCmp);

  // Run strongly connected components on it. It should have 4
  // components and some of them should be cyclic.
  GraphSCC graphSCC;
  graphSCC.compute(intGraph);
  if (graphSCC.numComponents() != 4) {
    UtIO::cout() << "Found " << graphSCC.numComponents()
                 << " components, should have found 4.\n";
    status = 1;
  }
  if (graphSCC.numCycles() != 3) {
    UtIO::cout() << "Found " << graphSCC.numCycles()
                 << " cycles, should have found 3.\n";
    status = 1;
  }

  // Walk the components and extract sub graphs of them
  for (GraphSCC::ComponentLoop l = graphSCC.loopComponents(); !l.atEnd(); ++l) {
    // If this is cyclic, it should either be a cycle of 1, or 2,3, or
    // 4,5,6,7. Otherwise it should be a single node of 0.
    GraphSCC::Component* comp = *l;
    Graph* extractGraph = graphSCC.extractComponent(comp);
    IntGraph* subGraph = dynamic_cast<IntGraph*>(extractGraph);
    // Cyclic first
    if (comp->isCyclic()) {
      // Make sure it is one of the three cycles
      if (subGraph->size() == 1) {
        // Make sure the node is 1
        IntNode* node = NULL;
        for (Iter<GraphNode*> i = subGraph->nodes(); !i.atEnd(); ++i) {
          GraphNode* graphNode = *i;
          node = subGraph->castNode(graphNode);
          if (node->getData() != 1) {
            UtIO::cout() << "Found an unexpected singleton cycle with node "
                         << node->getData() << "\n";
            status = 1;
          }
        }

        // Make sure the edge is number 2
        if (node == NULL) {
          UtIO::cout() << "Singleton cycle without a node in it!\n";
          status = 1;
        } else {
          bool foundEdge = false; // Coverity notes this is never set to true
          for (Iter<GraphEdge*> i = subGraph->edges(node); !i.atEnd(); ++i) {
            GraphEdge* graphEdge = *i;
            IntEdge* edge = subGraph->castEdge(graphEdge);
            if (foundEdge) {
              // Coverity says this branch is dead code
              UtIO::cout() << "Found an extra edge in the singleton cycle: "
                           << edge->getData() << "\n";
              status = 1;
            } else if (edge->getData() != 2) {
              UtIO::cout() << "Found invalid edge value " << edge->getData()
                           << " in singleton cycle.\n";
              status = 1;
            }
          }
        }
          
      } else if (subGraph->size() == 2) {
        // Make sure the nodes are 2 and 3
        bool foundNodes[MAX_GRAPH_SIZE];
        foundNodes[2] = foundNodes[3] = false;
        for (Iter<GraphNode*> i = subGraph->nodes(); !i.atEnd(); ++i) {
          GraphNode* graphNode = *i;
          IntNode* node = subGraph->castNode(graphNode);
          if (node->getData() == 2) {
            if (foundNodes[2]) {
              UtIO::cout() << "Duplicate node 2 found in subgraph.\n";
              status = 1;
            } else {
              foundNodes[2] = true;
            }
          } else if (node->getData() == 3) {
            if (foundNodes[3]) {
              UtIO::cout() << "Duplicate node 3 found in subgraph.\n";
              status = 1;
            } else {
              foundNodes[3] = true;
            }
          } else {
            UtIO::cout() << "Found unexpected node " << node->getData()
                         << " in 2,3 subgraph.\n";
            status = 1;
          }
        } // for

      } else if (subGraph->size() == 4) {
        // Mkae sure the nodes are 4, 5, 6, 7
        bool foundNodes[MAX_GRAPH_SIZE];
        foundNodes[4] = foundNodes[5] = foundNodes[6] = foundNodes[7] = false;
        for (Iter<GraphNode*> i = subGraph->nodes(); !i.atEnd(); ++i) {
          GraphNode* graphNode = *i;
          IntNode* node = subGraph->castNode(graphNode);
          if (node->getData() == 4) {
            if (foundNodes[4]) {
              UtIO::cout() << "Duplicate node 4 found in subgraph.\n";
              status = 1;
            } else {
              foundNodes[4] = true;
            }
          } else if (node->getData() == 5) {
            if (foundNodes[5]) {
              UtIO::cout() << "Duplicate node 5 found in subgraph.\n";
              status = 1;
            } else {
              foundNodes[5] = true;
            }
          } else if (node->getData() == 6) {
            if (foundNodes[6]) {
              UtIO::cout() << "Duplicate node 6 found in subgraph.\n";
              status = 1;
            } else {
              foundNodes[6] = true;
            }
          } else if (node->getData() == 7) {
            if (foundNodes[7]) {
              UtIO::cout() << "Duplicate node 7 found in subgraph.\n";
              status = 1;
            } else {
              foundNodes[7] = true;
            }
          } else {
            UtIO::cout() << "Found unexpected node " << node->getData()
                         << " in 4,5,6,7 subgraph.\n";
            status = 1;
          }
        } // for

      } else {
        // Strange, cyclic sub component
        UtIO::cout() << "Found a cycle of size " << subGraph->size()
                     << ", should be size 1, 2, or 4.\n";
        status = 1;
      }

    } else {
      // Must be acyclic with node 0 in it
      IntNode* node = NULL;
      for (Iter<GraphNode*> i = subGraph->nodes(); !i.atEnd(); ++i) {
        GraphNode* graphNode = *i;
        node = subGraph->castNode(graphNode);
        if (node->getData() != 0) {
          UtIO::cout() << "Found an unexpected acyclic node with value "
                       << node->getData() << "\n";
          status = 1;
        }
      }
    }

    // Done with this subgraph
    delete subGraph;

  } // for

  // Make the graph acylic by removing nodes, 1, 3, and 4.
  UtSet<GraphNode*> removeSet;
  removeSet.insert(nodes[1]);
  removeSet.insert(nodes[3]);
  removeSet.insert(nodes[4]);
  intGraph->removeNodes(removeSet);

  // Print it again
  UtIO::cout() << "\nPruned Graph:\n";
  printWalker.walk(intGraph, &nodeCmp, &edgeCmp);
  
  // Put the nodes back in. Add new nodes so that all edges are still present
  nodes[1] = addNode(intGraph, 1);
  nodes[3] = addNode(intGraph, 3);
  nodes[4] = addNode(intGraph, 4);
  nodes[8] = addNode(intGraph, 8);
  nodes[9] = addNode(intGraph, 9);
  addEdge(intGraph, nodes[0], nodes[1], 0);
  addEdge(intGraph, nodes[1], nodes[2], 3);
  addEdge(intGraph, nodes[2], nodes[3], 4);
  addEdge(intGraph, nodes[8], nodes[2], 5);
  addEdge(intGraph, nodes[3], nodes[4], 6);
  addEdge(intGraph, nodes[4], nodes[5], 7);
  addEdge(intGraph, nodes[6], nodes[9], 10);
  addEdge(intGraph, nodes[7], nodes[9], 11);

  // Check if it has cycles again
  graphSCC.compute(intGraph);
  if (graphSCC.numCycles() > 0) {
    UtIO::cout() << "Found " << graphSCC.numCycles()
                 << " cycles in the graph; should be acylic.\n";
    status = 1;
  }

  // Print it again
  UtIO::cout() << "\nAcyclic Graph:\n";
  printWalker.walk(intGraph, &nodeCmp, &edgeCmp);
  
  delete intGraph;
  return status;
} // int bigraphtest
