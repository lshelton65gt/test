// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/Zstream.h"
#include "util/ZstreamZip.h"
#include "util/OSWrapper.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"
#include <stdio.h>
#include <assert.h>

  // small file, small archive test
static int sTestSFileSArc()
{
  int status = 0;
  ZOSTREAM(afile, "a");
  if (afile.fail())
  {
    fprintf(stderr, "%s\n", afile.getError());
    return 1;
  }
  
  afile << "A random line of text\n";
  afile.close();
  
  ZOSTREAMZIP(arFile, "test1.czip");
  if (arFile.fail())
  {
    status = 1;
    fprintf(stderr, "%s\n", arFile.getFileError());
  }
  else if (! arFile.addFile("a"))
  {
    status = 1;
    fprintf(stderr, "%s\n", arFile.getFileError());
  }
  else if (! arFile.finalize())
  {
    status = 1;
    fprintf(stderr, "%s\n", arFile.getFileError());
  }

  if (status == 0)
  {
    ZISTREAMZIP(arRead, "test1.czip");

    // Get all entry names (== 1)
    UtStringArray ids;
    if (! arRead.getAllEntryIds(&ids))
    {
      status = 1;
      fprintf(stderr, "%s\n", arRead.getFileError());
    }

    assert(ids.size() == 1) ;
    assert(strcmp(ids[0], "a") == 0);
      
    // just get the 1 entry
    ZistreamEntry* a_entry = arRead.getEntry("a");
    if (! a_entry)
    {
      // should have been there
      assert(arRead.fail());
      status = 1;
      fprintf(stderr, "%s\n", arRead.getFileError());
    }
    else
    {
      // call getEntry again and make sure that the pointers are the
      // same.
      {
        ZistreamEntry* chkEntry = arRead.getEntry("a");
        assert(chkEntry == a_entry);
        
        // now close the a_entry and reopen it.
        arRead.finishEntry(&chkEntry);
        assert(chkEntry == NULL);
        a_entry = arRead.getEntry("a");
        assert(a_entry);
      }
      
      Zistream* ain = a_entry->castZistream();
      FILE* out = OSFOpen("a2", "w", NULL);
      assert(out);
      while (! ain->eof() && ! ain->fail())
      {
        char abuf[65536];
        UInt32 numRead = ain->read(abuf, 65536);
        fwrite(abuf, 1, numRead, out);
      }
      fclose(out);
      if (ain->fail())
      {
        status = 1;
        fprintf(stderr, "%s\n", ain->getError());
      }
      
    }
  }
  return status;
}

static int error(const char* errMsg)
{
  fprintf(stderr, "%s\n", errMsg);
  return 1;
}

// Multiple uncompressed files and a buffer in one archive
static const char* file1 = "ufile1";

static int sTestMUFileArc()
{
  int status = 0;

  const char* file2 = "ufile2";
  const char* stuff = "abcdefghijklmnopqrstuvwxyz0123456789";
  size_t stuffSize = strlen(stuff);
  const char* saveBufName = "saveBuf";

  FILE* out;
  for (int i = 0; i < 2; ++i)
  {
    const char* name = file1;
    if (i == 1)
      name = file2;
    out = OSFOpen(name, "w", NULL);
    assert(out);
    for (int j = 0; j < 10000; ++j)
      fwrite(stuff, 1, stuffSize, out);
    fclose(out);
    ++stuff; // change stuff
    --stuffSize;
  }

  UtString saveBuffer;
  saveBuffer << "The plain rain falls mainly in Istanbul";
  
  // add a file, then the buffer, then the last file
  ZOSTREAMZIP(outAr, "multifileandbuf"); 
  if (outAr.fail())
    status = error(outAr.getFileError());
  else if (! outAr.addFile(file1))
    status = error(outAr.getFileError());
  else if (! outAr.addBuffer(saveBufName, saveBuffer))
    status = error(outAr.getFileError());
  else if (! outAr.addFile(file2))
    status = error(outAr.getFileError());
  else if (! outAr.finalize())  
    status = error(outAr.getFileError());
  
  // This used to crash
  {
    ZistreamZip* ZISTREAMZIP_ALLOC(tmpzip, "nonexistentfile");
    delete tmpzip;
  }

  ZISTREAMZIP (inAr, "multifileandbuf");
  ZistreamEntry* zfile1 = NULL;
  ZistreamEntry* zfile2 = NULL;
  ZistreamEntry* zbuf = NULL;
  
  if (status == 0)
  {
    if (inAr.fail())
      status = error(inAr.getFileError());
    else if ((zfile1 = inAr.getEntry(file1)) == NULL)
      status = error(inAr.getFileError());
    else if ((zfile2 = inAr.getEntry(file2)) == NULL)
      status = error(inAr.getFileError());
    else if ((zbuf = inAr.getEntry(saveBufName)) == NULL)
      status = error(inAr.getFileError());
  }
  
  Zistream* zstr1 = NULL;
  Zistream* zstr2 = NULL;
  Zistream* zstr3 = NULL;
  
  if (status == 0)
  {
    zstr1 = zfile1->castZistream();
    zstr2 = zfile2->castZistream();
    zstr3 = zbuf->castZistream();
    
    FILE* chk1 = OSFOpen("zfile1", "w", NULL);
    FILE* chk2 = OSFOpen("zfile2", "w", NULL);
    assert(chk1);
    assert(chk2);
    
    // read a little from 1 then another...
    char buf[10000];
    bool done = false;
    UInt32 numRead = 0;
    UtString saveBufRead;

    while (!done && (status == 0))
    {
      int numDone = 0;
      inAr.switchEntry(zfile1);

      if (! zstr1->eof())
      {
        numRead = zstr1->read(buf, 10000);
        fwrite(buf, 1, numRead, chk1);
      }
      else
        ++numDone;

      inAr.switchEntry(zfile2);
      if (! zstr2->eof())
      {
        numRead = zstr2->read(buf, 10000);
        fwrite(buf, 1, numRead, chk2);
      }
      else 
        ++numDone;

      inAr.switchEntry(zbuf);
      if (! zstr3->eof())
      {
        numRead = zstr3->read(buf, 10000);
        if (numRead > 0)
          saveBufRead.append(buf, numRead);
      }
      else
        ++numDone;
      
      if (numDone == 3)
        done = true;
      
      if (zstr1->fail())
        status = error(zstr1->getError());

      if (zstr2->fail())
        status = error(zstr2->getError());

      if (zstr3->fail())
        status = error(zstr3->getError());
    }

    fclose(chk1);
    fclose(chk2);

    if ((status == 0) && (saveBufRead.compare(saveBuffer) != 0))
    {
      fprintf(stderr, "Buffer restore failed.\n");
      status = 1;
    }
  }
  
  return status;
}

static int sTestDBFileArc()
{
  int status = 0;

  // This will test mixed database and non-database entries using both
  // the direct db entry and the by-file entry

  // We're using file1 from sTestMUFileArc. If that doesn't
  // exist. return 1 now.
  {
    UtString result;
    if (OSStatFile(file1, "fr", &result) != 1)
    {
      fprintf(stderr, "Error: %s does not exist or is not readable.\n", file1);
      return 1;
    }
  }

  // create a database file
  {
    ZOSTREAMDB (db1, "db1");
    UInt32 val = 0;
    db1 << val;
    val += 10;
    db1 << val;
    db1.close();
  }


  // create a zstream file
  {
    ZOSTREAM (z, "zstream_type");
    UInt32 val = 1;
    z << val << " ";
    val += 10;
    z << val;
    z.close();
  }

  ZOSTREAMZIP(outAr, "dbmixed"); 
  if (outAr.fail())
    status = error(outAr.getFileError());
  else
  {
    // first write a database via file 
    if (! outAr.addFile("db1", ZstreamZip::eFileDB))
      status = error(outAr.getFileError());
    // and now a text file
    else if (! outAr.addFile(file1, ZstreamZip::eFileTxt))
      status = error(outAr.getFileError());
    else
    {
      // now write a database file using direct db entry
      ZostreamDB* curDB = outAr.addDatabaseEntry("miles");
      assert(curDB);
      (*curDB) << "workin";
      (*curDB) << UInt32(5);
      outAr.endDatabaseEntry();

      // and now a zstream text file;
      if (! outAr.addFile("zstream_type", ZstreamZip::eFileZstream))
        status = error(outAr.getFileError());
    }
  }
  
  if ((status == 0) &&
      ! outAr.finalize())
    status = error(outAr.getFileError());
  
  ZISTREAMZIP (inAr, "dbmixed");
  ZistreamEntry* zfile1 = NULL;
  ZistreamEntry* zfiledb1 = NULL;
  ZistreamEntry* zfiledb2 = NULL;
  ZistreamEntry* zfilez = NULL;
  
  if (status == 0)
  {
    if (inAr.fail())
      status = error(inAr.getFileError());
    else if ((zfile1 = inAr.getEntry(file1)) == NULL)
      status = error(inAr.getFileError());
    else if ((zfiledb1 = inAr.getEntry("db1")) == NULL)
      status = error(inAr.getFileError());
    else if ((zfiledb2 = inAr.getEntry("miles")) == NULL)
      status = error(inAr.getFileError());
    else if ((zfilez = inAr.getEntry("zstream_type")) == NULL)
      status = error(inAr.getFileError());
  }
  
  Zistream* zstr1 = NULL;
  ZistreamDB* zstr2 = NULL;
  ZistreamDB* zstr3 = NULL;
  Zistream* zstr4 = NULL;  

  if (status == 0)
  {
    assert(zfile1->castZistreamDB() == NULL);
    zstr1 = zfile1->castZistream();

    zstr2 = zfiledb1->castZistreamDB();
    assert(zfiledb1->castZistream() != NULL);

    zstr3 = zfiledb2->castZistreamDB();
    assert(zfiledb2->castZistream() != NULL);

    zstr4 = zfilez->castZistream();
    assert(zfilez->castZistreamDB() == NULL);
    
    FILE* chk1 = OSFOpen("zfile1_mixed", "w", NULL);
    assert(chk1);
    
    inAr.switchEntry(zfile1);
    while (! zstr1->eof() && (status == 0))
    {
      char buf[10000];
      UInt32 numRead = 0;
      numRead = zstr1->read(buf, 10000);
      fwrite(buf, 1, numRead, chk1);
      
      if (zstr1->fail())
        status = error(zstr1->getError());
    }
    
    fclose(chk1);
  }

  // Check the 2 databases
  if (status == 0)
  {
    inAr.switchEntry(zfiledb1);
    UInt32 val;
    (*zstr2) >> val;
    if (val != 0)
      status = error("Incorrect value in db1 file, expected 0");
    (*zstr2) >> val;
    if (val != 10)
      status = error("Incorrect value in db1 file, expected 10");
  }

  if (status == 0)
  {
    inAr.switchEntry(zfiledb2);
    UtString tmp;
    (*zstr3) >> tmp;
    if (tmp.compare("workin") != 0)
      status = error("Incorrect string in db file 2");

    UInt32 val;
    (*zstr3) >> val;
    if (val != 5)
      status = error("Incorrect value in db file 2");
  }

  // and check the textual zstream
  if (status == 0)
  {
    inAr.switchEntry(zfilez);
    UInt32 val;
    (*zstr4) >> val;
    if (val != 1)
      status = error("Incorrect value in z file, expected 1");
    (*zstr4) >> val;
    if (val != 11)
      status = error("Incorrect value in z file, expected 11");
  }
  
  return status;
}

static void test_encode_decode(const char* str) {
  UtString encrypted, decrypted;
  ZENCRYPT_STRING(str, &encrypted);
  ZDECRYPT_STRING(encrypted, &decrypted);
  INFO_ASSERT(decrypted == str, "decryption failure");
}

int testZstreamZip()
{
  int status = sTestSFileSArc();
  
  status += sTestMUFileArc();
  status += sTestDBFileArc();

  test_encode_decode("Now is the time");

  return status;
}
