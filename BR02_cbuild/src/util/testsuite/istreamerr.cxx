// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/CarbonTypes.h"
#include "util/UtCachedFileSystem.h"
#include "util/OSWrapper.h"
#include "util/CarbonAssert.h"

static const char testFilename[] = "istreamerr.in";

int istreamerr(void)
{
  int ret = 0;

  UtIBStream infile(testFilename);
  UtString buf;

  if (!infile.is_open()) {
    UtIO::cerr() << infile.getErrmsg() << UtIO::endl;
    ret = 1;
  }

  if (! ((infile >> "illegal") && (infile >> "octal:")))
  {
    UtIO::cerr() << infile.getErrmsg() << UtIO::endl;
    ret = 1;
  }

  UInt32 num;
  if (infile >> UtIO::oct >> num) {
    UtIO::cerr() << "FAIL: expected error on illegal octal\n";
    ret = 1;
  }
  else {
    // Expect this failure:
    UtIO::cout() << "Success: "
                 << infile.getErrmsg() << UtIO::endl;
  }
  if (!infile.getline(&buf)) {
    UtIO::cerr() << "FAIL: expected newline\n";
    ret = 1;
  }

  if (! ((infile >> "illegal") && (infile >> "binary:")))
  {
    UtIO::cerr() << infile.getErrmsg() << UtIO::endl;
    ret = 1;
  }

  if (infile >> UtIO::bin >> num) {
    UtIO::cerr() << "FAIL: expected error on illegal binary\n";
    ret = 1;
  }
  else {
    // Expect this failure:
    UtIO::cout() << "Success: "
                 << infile.getErrmsg() << UtIO::endl;
  }
  if (!infile.getline(&buf)) {
    UtIO::cerr() << "FAIL: expected newline\n";
    ret = 1;
  }

  if (!(infile >> "expected")) {
    UtIO::cout() << "Success: "
                 << infile.getErrmsg() << UtIO::endl;
  }

  // Try to parse negative numbers into an unsigned number
  if (! (infile >> UtIO::dec >> num)) {
    UtIO::cout() << "Success: "
                 << infile.getErrmsg() << UtIO::endl;
  }
  else {
    UtIO::cerr() << "FAIL: expected error parsing -5 into a UInt32\n";
    ret = 1;
  }    
  
  if (!infile.getline(&buf)) {
    UtIO::cerr() << "FAIL: expected newline\n";
    ret = 1;
  }

  SInt16 s16;
  if (! (infile >> UtIO::dec >> s16)) {
    UtIO::cout() << "Success: "
                 << infile.getErrmsg() << UtIO::endl;
  }
  else {
    UtIO::cerr() << "FAIL: expected error parsing -50000 into a SInt16\n";
    ret = 1;
  }    

  if (!infile.getline(&buf)) {
    UtIO::cerr() << "FAIL: expected newline\n";
    ret = 1;
  }

  if (! (infile >> UtIO::dec >> s16)) {
    UtIO::cout() << "Success: "
                 << infile.getErrmsg() << UtIO::endl;
  }
  else {
    UtIO::cerr() << "FAIL: expected error parsing 50000 into a SInt16\n";
    ret = 1;
  }    

  if (!infile.getline(&buf)) {
    UtIO::cerr() << "FAIL: expected newline\n";
    ret = 1;
  }

  SInt32 s32;
  if (! (infile >> UtIO::dec >> s32)) {
    UtIO::cout() << "Success: "
                 << infile.getErrmsg() << UtIO::endl;
  }
  else {
    UtIO::cerr() << "FAIL: expected error parsing -5000000000 into a SInt32\n";
    ret = 1;
  }    

  if (!infile.getline(&buf)) {
    UtIO::cerr() << "FAIL: expected newline\n";
    ret = 1;
  }

  UInt16 u16;
  if (! (infile >> UtIO::dec >> u16)) {
    UtIO::cout() << "Success: "
                 << infile.getErrmsg() << UtIO::endl;
  }
  else {
    UtIO::cerr() << "FAIL: expected error parsing 70000 into a UInt16\n";
    ret = 1;
  }    

  if (!infile.getline(&buf)) {
    UtIO::cerr() << "FAIL: expected newline\n";
    ret = 1;
  }

  UInt32 u32;
  if (! (infile >> UtIO::dec >> u32)) {
    UtIO::cout() << "Success: "
                 << infile.getErrmsg() << UtIO::endl;
  }
  else {
    UtIO::cerr() << "FAIL: expected error parsing 5000000000 into a UInt32\n";
    ret = 1;
  }    

  return ret;
} // int istreamerr
