// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/ArgProc.h"
#include "util/OSWrapper.h"

#include "util/UtString.h"
#include "util/UtStream.h"
#include "util/UtIOStream.h"

int argproctest(int argc, char* argv[])
{
  const char* cDblKey = "-dbl";
  const char* cIntKey = "-int";
  const char* cDblChar = "-O";
  const char* cStrKey = "-str";
  const char* cInFileKey = "-in";
  const char* cOutFileKey = "-out";
  const char* cOnKey = "-on"; // boolean
  const char* cSomeBool = "-somebool"; // boolean
  const char* cHelpKey = "-h";
  const char* cDeprecated = "-deprecated"; // boolean

  UtOStream& io_cout = UtIO::cout();
  const char* cOffKey = "-off"; // boolean override
  const char* cRestricted = "-restricted"; // boolean not allowed to
                                          // use
  const char* cOkay = "-allowed"; // okay to use, but not public
  const char* cWildPlus = "+mom*";
  const char* cNeg = "-neg";
  const char* cPos = "-pos";

  const char* sect1 = "Oreo Cookies";
  const char* sect2 = "Milk";
  
  ArgProc argProc;
  {
    UtString tmpStr;
    UtString docHeader;

    UtString path;
    UtString thisExec;
    OSParseFileName(argv[0], &path, &thisExec);
    const char* title = " - Simple Test Program";
    tmpStr << thisExec << title;
    docHeader = thisExec;

    argProc.setDescription(docHeader.c_str(), tmpStr.c_str(), "A nifty test program to solve the world's problems, including world peace.");

    argProc.createSection(sect1);
    argProc.createSection(sect2);
    
    tmpStr = thisExec + " [" + cHelpKey + "] [" + cOnKey + "] [" + cStrKey + 
      " <string> ] [" + cIntKey + " <integer>] [" + cDblKey +
      " <real>] [" + cInFileKey + " <infile>] [" + cOutFileKey + " <outfile>]";
    argProc.addSynopsis(tmpStr);
    
    argProc.addString(cHelpKey, "Prints documentation on switch usage and exits. If an option name is provided, help will be given on that option only; otherwise, general help and usage will be printed.<verbatim>T\n  h\n   i\n    s\n          is supposed to look\nstupid\n</verbatim> Not bad eh?", NULL, true, false, 1);
    
    argProc.addToSection(sect1, cHelpKey);

    argProc.addInputArgFile("-f", "File that contains options for this app.");
    argProc.addToSection(sect1, "-f");

    argProc.addBool(cOnKey, "Generic boolean", true, 1);
    argProc.addToSection(sect2, cOnKey);
    argProc.addSynonym(cOnKey, "-notOff", false);

    argProc.addBool(cRestricted, "Not allowed to use this", false, 1);
    argProc.disallowUsage(cRestricted);

    argProc.addBool(cOkay, "Can use this, but will not show in public documentation", false, 1);
    argProc.allowUsage(cOkay);
    argProc.addSynonym(cOkay, "-otay", true);
    
    argProc.addBool(cDeprecated, "This is a valid deprecated option.", false, 1);
    // to test non-printing of deprecated options add to section.
    argProc.addToSection(sect2, cDeprecated); 
    argProc.putIsDeprecated(cDeprecated, true);
    
    argProc.createUnprocessedGroup("Plus");
    argProc.addUnprocessedBoolWildcard("Plus", cWildPlus, "+mom[ma]", "Stupid momma plusarg", 1);
    argProc.addToSection(sect1, cWildPlus);

    argProc.addBool(cSomeBool, "Another Generic boolean, but this one is not overridden", false, 1);
    argProc.addToSection(sect1, cSomeBool);

    argProc.addBoolOverride(cOffKey, cOnKey);
    argProc.addToSection(sect1, cOffKey);

    argProc.addInt(cIntKey, "An integer argument", -1, false, false, 1);
    argProc.addToSection(sect1, cIntKey);
    
    argProc.addDouble(cDblKey, "A real number argument", 1.1, false, false, 1);
    argProc.addToSection(sect2, cDblKey);

    argProc.addDouble(cDblChar, "A single character double option", 0.034, false, false, 1);
    argProc.addToSection(sect2, cDblChar);

    argProc.addBool(cNeg, "Negative.", false, 1);
    argProc.addToSection(sect2, cNeg);
    argProc.addBoolOverride(cPos, cNeg, "Positive override of negative.");
    argProc.addToSection(sect2, cPos);
    
    argProc.addString(cStrKey, "A general string argument", "Yippee-ki-yay", false, false, 1);
    argProc.addToSection(sect1, cStrKey);
    
    argProc.addInputFile(cInFileKey, "Input file", NULL, true, false, 1);
    argProc.addToSection(sect2, cInFileKey);

    argProc.addOutputFile(cOutFileKey, "Output file", NULL, true, ArgProc::eRegFile, false, 1);
    argProc.addToSection(sect1, cOutFileKey);
  }

  UtString errMsg;
  UtStringArgv argvBuffer;
  ArgProc::ParseStatusT cmdLineStat =
    argProc.preParseCommandLine(argc, argv, &argvBuffer, &errMsg);

  if (cmdLineStat == ArgProc::eParseWarning) {
    io_cout << errMsg << '\n';
  }

  if (cmdLineStat == ArgProc::eParseError)
  {
    io_cout << errMsg << '\n';
    return 1;
  }
  
  char** procArgv = argvBuffer.getArgv();
  int procArgc = argvBuffer.getArgc();

  int numOptions;
  cmdLineStat =
    argProc.parseCommandLine(&procArgc, procArgv, &numOptions, &errMsg);
  
  if (cmdLineStat == ArgProc::eParseWarning) {
    io_cout << errMsg;
    cmdLineStat = ArgProc::eParsed;
  }

  ArgProc::ParseStatusT helpArg = argProc.isParsed(cHelpKey);
  if (cmdLineStat == ArgProc::eParseError)
  {
    int exitStat = 1;
    if ((helpArg == ArgProc::eNotParsed) ||
        (numOptions > 1))
      io_cout << errMsg << '\n';
    else
    {
      switch (helpArg)
      {
      case ArgProc::eParsed:
        break;
      case ArgProc::eNotParsed:
        break;
      case ArgProc::eParseError:
      case ArgProc::eParseWarning: // will not be this
      case ArgProc::eParseNote:
        argProc.getUsageVerbose(&errMsg);
        io_cout << errMsg << '\n';
        exitStat = 0;
        break;
      }
    }
    exit(exitStat);
  }
  else if (helpArg == ArgProc::eParsed)
  {
    ArgProc::StrIter iter;
    
    argProc.getStrIter(cHelpKey, &iter);
    while (! iter.atEnd())
    {
      const UtString& str = *iter;
      if (argProc.getUsage(str.c_str(), &errMsg) == ArgProc::eKnown)
        io_cout << errMsg << '\n';          
      else
        io_cout << "Unknown option: " << *iter << '\n';
      ++iter;
    }
  }

  if (argProc.isParsed(cIntKey) == ArgProc::eParsed)
  {
    ArgProc::IntIter iter;
    argProc.getIntIter(cIntKey, &iter);

    while (! iter.atEnd())
    {
      io_cout << "Int argument: " << *iter << '\n';
      ++iter;
    }
  }

  if (argProc.isParsed(cDblKey) == ArgProc::eParsed)
  {
    ArgProc::DblIter iter;
    argProc.getDoubleIter(cDblKey, &iter);

    while (!iter.atEnd())
    {
      io_cout << "Dbl argument: " << *iter << '\n';
      ++iter;
    }
  }
  

  if (argProc.isParsed(cDblChar) == ArgProc::eParsed)
  {
    ArgProc::DblIter iter;
    argProc.getDoubleIter(cDblChar, &iter);

    while (!iter.atEnd())
    {
      io_cout << "Single character Dbl argument: " << *iter << '\n';
      ++iter;
    }
  }
  

  if (argProc.isParsed(cStrKey) == ArgProc::eParsed)
  {
    ArgProc::StrIter iter;
    argProc.getStrIter(cStrKey, &iter);

    while (! iter.atEnd())
    {
      io_cout << "Str argument: " << *iter << '\n';
      ++iter;
    }
  }
  else
  {
    const char* strArg = argProc.getStrValue(cStrKey);
    if (strArg != NULL)
      io_cout << "Single string argument: " << strArg << '\n';
  }

  if (argProc.isParsed(cInFileKey) == ArgProc::eParsed)
  {
    ArgProc::StrIter iter;
    argProc.getStrIter(cInFileKey, &iter);
    
    while (! iter.atEnd())
    {
      io_cout << "InFile argument: " << *iter << '\n';
      ++iter;
    }
  }

  if (argProc.isParsed(cOutFileKey) == ArgProc::eParsed)
  {
    ArgProc::StrIter iter;
    argProc.getStrIter(cOutFileKey, &iter);
    
    while (! iter.atEnd())
    {
      io_cout << "OutFile argument: " << *iter << '\n';
      ++iter;
    }
  }

  bool checkBool = false;
  if (argProc.isParsed(cOffKey) == ArgProc::eParsed)
  {
    io_cout << "Bool Override argument: " << cOffKey << " found\n";
    checkBool = true;
  }
  if (argProc.isParsed(cOnKey) == ArgProc::eParsed)
  {
    io_cout << "Bool argument: " << cOnKey << " found\n";
    checkBool = true;
  }

  if (checkBool)
  {
    io_cout << "Final bool argument value: ";
    if (argProc.getBoolValue(cOnKey))
      io_cout << "true";
    else
      io_cout << "false";
    io_cout << "\n";
  }

  if (argProc.isParsed(cSomeBool) == ArgProc::eParsed)
  {
    io_cout << "Bool argument: " << cSomeBool << " found\n";
    assert(argProc.getBoolValue(cSomeBool));
  }

  if (argProc.isParsed(cOkay) == ArgProc::eParsed)
  {
    io_cout << "Bool argument: " << cOkay << " found\n";
    assert(argProc.getBoolValue(cOkay));
  }

  if (argProc.isParsed(cDeprecated) == ArgProc::eParsed)
  {
    io_cout << "Bool argument: " << cDeprecated << " found\n";
    assert(argProc.getBoolValue(cDeprecated));
  }

  if ((argProc.isParsed(cNeg) == ArgProc::eParsed) ||
      (argProc.isParsed(cPos) == ArgProc::eParsed))
  {
    io_cout << "-neg is ";
    if (argProc.getBoolValue(cNeg))
      io_cout << "true\n";
    else
      io_cout << "false\n";
  }
  

    
  if (argProc.isParsed(cWildPlus) == ArgProc::eParsed)
  {
    ArgProc::StrIter matched = argProc.getMatchedArgs(cWildPlus);
    io_cout << "Wildcard Bool argument: " << *matched << " from " << cWildPlus << "\n";
  }

  if (procArgc != 1)
  {
    io_cout << "Error: The entire command line was not processed.\n";
    return 1;
  }

  // Check arg re-initialization here
  argProc.reInitSection(sect2);
  assert(argProc.isParsed(cOnKey) == ArgProc::eNotParsed);
  assert(argProc.isParsed(cDeprecated) == ArgProc::eNotParsed);
  assert(argProc.isParsed(cDblKey) == ArgProc::eNotParsed);
  assert(argProc.isParsed(cDblChar) == ArgProc::eNotParsed);
  assert(argProc.isParsed(cNeg) == ArgProc::eNotParsed);
  assert(argProc.isParsed(cPos) == ArgProc::eNotParsed);
  assert(argProc.isParsed(cInFileKey) == ArgProc::eNotParsed);

  argProc.reInit(cSomeBool);
  assert(argProc.isParsed(cSomeBool) == ArgProc::eNotParsed);

  return 0;
}
