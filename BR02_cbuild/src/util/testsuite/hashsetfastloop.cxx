#if 0

// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtStream.h"
#include "util/HashSetFastLoop.h"
#include "util/UtString.h"
#include "util/UtIOStream.h"
#include <fstream>
#include "util/UtSet.h"

// please do not use "using namespace std"

struct IntCmp {
  size_t hash(int var) const {return var;}
  bool lessThan(int v1, int v2) const {return v1 < v2;}
  bool lessThan1(int v1, int v2) const {return v1 < v2;}
  bool equal(int v1, int v2) const {return v1 == v2;}
};

static int intsettest()
{
  typedef HashSetFastLoop<int, IntCmp> IntSet;
  IntSet hfib;
  UtSet<int> sfib;
  int x;
  x = 2;  hfib.insert(x);  sfib.insert(x);
  x = 5;  hfib.insert(x);  sfib.insert(x);
  x = 3;  hfib.insert(x);  sfib.insert(x);
  x = 13; hfib.insert(x);  sfib.insert(x);
  x = 8;  hfib.insert(x);  sfib.insert(x);
  x = 8;  hfib.insert(x);  sfib.insert(x);
  x = 8;  hfib.insert(x);  sfib.insert(x);
  x = 2;  hfib.insert(x);  sfib.insert(x);

  UtSet<int>::iterator sp = sfib.begin();
  UtSet<int>::iterator se = sfib.end();
  int status = 0;
  IntSet::SortedLoop hp = hfib.loopSorted();
  for (; !hp.atEnd() && (sp != se); ++hp, ++sp)
  {
    if (*hp != *sp)
    {
      UtIO::cout() << "Mismatch between set and HashSetFastLoop: hash=" << *hp << ", set=" << *sp << UtIO::endl;
      status = 1;
    }
    UtIO::cout() << *hp << " ";
  }
  UtIO::cout() << UtIO::endl;
  if (!hp.atEnd())
  {
    UtIO::cout() << "Unexpected end of set" << UtIO::endl;
    status = 1;
  }
  else if (sp != se)
  {
    UtIO::cout() << "Unexpected end of HashSetFastLoop" << UtIO::endl;
    status = 1;
  }

  int numEntries = 0;
  IntSet::UnsortedLoop ul = hfib.loopUnsorted();
  for (; !ul.atEnd(); ++ul)
    ++numEntries;
  if (numEntries != 5)
  {
    UtIO::cout() << "Wrong number of entries: " << numEntries << UtIO::endl;
    status = 1;
  }

  return status;
} // int intsettest

static int complexCopies = 0;

// Test that we are able to loop through a set of
// complex numbers values without copy-constructing them.
class HSFLComplex
{
public:
  HSFLComplex(double real, double imag): mReal(real), mImag(imag) {}
  ~HSFLComplex() {}
  static int compare(const HSFLComplex* c1, const HSFLComplex* c2) {
    int cmp = 0;
    if (c1->mReal < c2->mReal)
      cmp = -1;
    else if (c1->mReal > c2->mReal)
      cmp = 1;
    else if (c1->mImag < c2->mImag)
      cmp = -1;
    else if (c1->mImag > c2->mImag)
      cmp = 1;
    return cmp;
  }
  bool operator==(const HSFLComplex& other) const {
    return ((mReal == other.mReal) && (mImag == other.mImag));
  }
  bool operator<(const HSFLComplex& other) const {
    return compare(this, &other) < 0;
  }


  HSFLComplex(const HSFLComplex& src): mReal(src.mReal), mImag(src.mImag) {
    ++complexCopies;
  }
  HSFLComplex& operator=(const HSFLComplex& src)
  {
    if (&src != this)
    {
      ++complexCopies;
      mReal = src.mReal;
      mImag = src.mImag;
    }
    return *this;
  }

  void print() const {
    UtIO::cout() << "(" << mReal << "," << mImag << ")" << UtIO::endl;
  }

  size_t hash() const {return ((int) mReal) + ((int) mImag);}
private:
  double mReal;
  double mImag;
}; // class HSFLComplex
      
typedef HashSetFastLoop<HSFLComplex, HashValue<HSFLComplex> > HSFLComplexSet;

static int complexsettest() {
  HSFLComplexSet c;
  c.insert(HSFLComplex(1, 0));
  c.insert(HSFLComplex(1, 2));
  c.insert(HSFLComplex(2, 1));
  c.insert(HSFLComplex(2, 3));

  // This loop will use a copy-constructor & probably some temps
  int copies = complexCopies;
  for (HSFLComplexSet::SortedLoop loop = c.loopSorted(); !loop.atEnd(); ++loop)
  {
    const HSFLComplex& cx = *loop;
    cx.print();
  }
  assert(copies < complexCopies);

  // This loop should use no copy constructors
  copies = complexCopies;
  HSFLComplexSet::SortedLoop loop = c.loopSorted();
  for (const HSFLComplex* cptr; loop(&cptr);)
    cptr->print();
  assert(copies == complexCopies);

  // This loop is unsorted, and should also use no copy ctors
  copies = complexCopies;
  HSFLComplexSet::UnsortedLoop uloop = c.loopUnsorted();
  for (const HSFLComplex* cptr; uloop(&cptr);)
    cptr->print();
  assert(copies == complexCopies);

  return 0;
}
#endif

int hashsetfastlooptest(int /*argc*/, char* /*argv*/[])
{
#if 0
  int status = intsettest();
  status += complexsettest();
  return status;
#else
  return 0;
#endif
}
