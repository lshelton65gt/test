// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/OSWrapper.h"
#include "util/UtIOStream.h"
#include "util/UtHashSet.h"
#include "util/UtFileEntries.h"
#include "util/UtIStream.h"

static int testOSDirLoop()
{
  typedef UtHashSet<UtString, HashValue<UtString> > StringHash;
  StringHash knownEntries;
  UtString buf;
  {
    buf.assign("s_one");
    knownEntries.insert(buf);
    buf.assign("s_two");
    knownEntries.insert(buf);
    buf.assign("f_file");
    knownEntries.insert(buf);
    buf.assign("f_one.c");
    knownEntries.insert(buf);
    buf.assign("f_two.c");
    knownEntries.insert(buf);
    buf.assign("f_link");
    knownEntries.insert(buf);
    buf.clear();
  }

  size_t knownSize = knownEntries.size();

  int ret = 0;
  {
    UtFileEntries uniqueEntries;
    UtFileEntries::Status uniqStat;
    UtString entryErr;
    // test that we get all files/directories
    OSDirLoop dirLoop("osdirloop", "*");
    if (dirLoop.isError())
    {
      UtIO::cout() << "Cannot loop directory: " << dirLoop.getError() << UtIO::endl;
      return 1;
    }
    StringHash foundEntries;
    size_t numFound = 0;
    for (; ! dirLoop.atEnd(); ++dirLoop)
    {
      ++numFound;
      buf.assign(*dirLoop);
      UtString fullPath;
      
      uniqStat = uniqueEntries.addFile(dirLoop.getFullPath(&fullPath),
                                       &entryErr);
      switch (uniqStat)
      {
      case UtFileEntries::eOK:
        break;
      case UtFileEntries::eDup:
#if pfWINDOWS
        UtIO::cout() << "Duplicate file with different path found. Duplication on Windows is not possible: " << fullPath << UtIO::endl;
        ret = 1;
#else
        if ((strcmp(buf.c_str(), "f_link") != 0) &&
            // just in case we get a weird filesystem that reverses
            // the iteration
            (strcmp(buf.c_str(), "f_file") != 0))
        {
          UtIO::cout() << "Incorrect file duplication found: " << entryErr << UtIO::endl;
          ret = 1;
        }
#endif
        break;
      case UtFileEntries::eStatProb:
        UtIO::cout() << "Problem stating file: " << entryErr << UtIO::endl;
        ret = 1;
        break;
      }

      if (knownEntries.find(buf) == knownEntries.end())
      {
        UtIO::cout() << "Unexpected Entry: " << buf << " found in osdirloop." << UtIO::endl;
        ret = 1;
      }
      else
      {
        UtString tmp;
        dirLoop.getFullPath(&tmp);
        foundEntries.insert(tmp);
      }      
    }

    if ((numFound != knownSize) &&
        (foundEntries.size() != knownSize))
    {
      UtIO::cout() << "Osdirloop did not find all entries." << UtIO::endl;
      ret = 1;
    }
    
    const UtFileEntries::Entry* uniqueEntry = uniqueEntries.getFileEntry("osdirloop/f_link", &uniqStat);
    if (! uniqueEntry)
    {
      UtIO::cout() << "getFileEntry returned unexpected NULL";
      if (uniqStat != UtFileEntries::eOK)
        UtIO::cout() << ": Bad Status";
      UtIO::cout() << UtIO::endl;
      ret = 1;
    }
    
    if (! uniqueEntries.removeFile("osdirloop/f_file", &uniqStat))
    {
      UtIO::cout() << "removeFile failed unexpectedly";
      if (uniqStat != UtFileEntries::eOK)
        UtIO::cout() << ": Bad Status";
      UtIO::cout() << UtIO::endl;
      ret = 1;
    }
  }
  
  {
    // Loop only getting the directores (s_*)
    OSDirLoop dirLoop("osdirloop", "s_*");
    if (dirLoop.isError())
    {
      UtIO::cout() << "Cannot loop directory: " << dirLoop.getError() << UtIO::endl;
      return 1;
    }
    size_t numFound = 0;
    for (; ! dirLoop.atEnd(); ++dirLoop)
    {
      ++numFound;
      buf.assign(*dirLoop);
      if (knownEntries.find(buf) == knownEntries.end())
      {
        UtIO::cout() << "Unexpected Entry: " << buf << " found in osdirloop." << UtIO::endl;
        ret = 1;
      }
      else
      {
        dirLoop.getFullPath(&buf);
        OSStatEntry entry;
        UtString errMsg;
        if (OSStatFileEntry(buf.c_str(), &entry, &errMsg) != 0)
        {
          ret = 1;
          UtIO::cout() << "Could not stat: " << errMsg.c_str() << UtIO::endl;
        }
        else if (! entry.isDirectory() && entry.isExecutable())
        {
          ret = 1;
          UtIO::cout() << buf << " is not a directory or it doesn't have execution permission." << UtIO::endl;
        }
      }
    }

    if (numFound != 2)
    {
      UtIO::cout() << "Osdirloop did not find all s_* entries." << UtIO::endl;
      ret = 1;
    }
  } 
  
  {
    // Loop only getting the .c files (f_*.c)
    OSDirLoop dirLoop("osdirloop", "f_*.c");
    if (dirLoop.isError())
    {
      UtIO::cout() << "Cannot loop directory: " << dirLoop.getError() << UtIO::endl;
      return 1;
    }
    size_t numFound = 0;
    for (; ! dirLoop.atEnd(); ++dirLoop)
    {
      ++numFound;

      buf.assign(*dirLoop);
      if (knownEntries.find(buf) == knownEntries.end())
      {
        UtIO::cout() << "Unexpected Entry: " << buf << " found in osdirloop." << UtIO::endl;
        ret = 1;
      }
      else
      {
        dirLoop.getFullPath(&buf);
        OSStatEntry entry;
        UtString errMsg;
        if (OSStatFileEntry(buf.c_str(), &entry, &errMsg) != 0)
        {
          ret = 1;
          UtIO::cout() << "Could not stat: " << errMsg.c_str() << UtIO::endl;
        }
        else if (! entry.isReadable() && entry.isRegularFile())
        {
          ret = 1;
          UtIO::cout() << buf << " is not a readable regular file." << UtIO::endl;
        }
        
      }
    }
    
    if (numFound != 2)
    {
      UtIO::cout() << "Osdirloop did not find all s_* entries." << UtIO::endl;
      ret = 1;
    }
  } 
  
  return ret;
}

static int testOSFileOps() {
  UtString err;

  // Delete junk if it was left behind from a previous run
  (void) OSDeleteRecursive("junk", &err);
  err.clear();

  if ((OSMkdir("junk", 0770, &err) != 0) ||
      (OSMkdir("junk/a", 0770, &err) != 0) ||
      (OSMkdir("junk/a/b", 0770, &err) != 0) ||
      // There is no /etc/passwd on Windows. Just use a known file in
      // unit-util
      (!OSCopyFile("./comment.f", "junk/a/passwd", &err)) ||
      (!OSCopyFile("junk/a/passwd", "junk/a/b/passwd", &err)) ||
      (!OSCopyFile("junk/a/b/passwd", "junk/passwd", &err)) ||
      (OSDeleteRecursive("junk/a", &err) != 0))
  {
    fprintf(stderr, "%s\n", err.c_str());
    return 1;
  }

  // Make sure that the file junk/a got removed -- passwd should no longer
  // be in there
  {
    UtIBStream junk_a_passwd("junk/a/passwd");
    if (junk_a_passwd.is_open()) {
      fprintf(stderr, "delete junk/a must have failed");
      return 1;
    }
  }

  // Make sure an exact copy of /etc/passwd made it to junk/passwd
  if (OSCompareFiles("./comment.f", "junk/passwd", &err) != OSCompareSame) {
    fprintf(stderr, "files did not compare\n");
    return 1;
  }

  if (OSDeleteRecursive("junk", &err) != 0) {
    fprintf(stderr, "final delete failed: %s\n", err.c_str());
    return 1;
  }
  return 0;
} // static int testOSFileOps

int oswraptest()
{
  int ret = 0;
  char* endptr;
  SInt64  ll;
  UInt64 ull;

  struct test {
    SInt64 val;
    const char* str;
    int base;
  } tests[] =
  {
    {KSInt64(10000000000000), "10000000000000", 10},
    {KSInt64(0x10000000000000), "10000000000000", 16},
    {KSInt64(-10000000000000), "-10000000000000", 10},
    {0, "0", 10},
    {8, "1000", 2},
    {0, NULL, 0}
  };

  for (test* p = tests; p->str != NULL; ++p)
  {
    ll = strtoll(p->str, &endptr, p->base);
    if (ll != p->val)
    {
      ++ret;
      UtIO::cout() << "strtoll(" << p->str << ") yields " << ((double) ll)
                << UtIO::endl;
    }
    else if (*endptr != '\0')
    {
      ++ret;
      UtIO::cout() << "strtoll(" << p->str << ") end-of-token wrong" << UtIO::endl;
      UtIO::cout() << "end-of-token is" << endptr << UtIO::endl;
    }
    if (p->val >= 0)
    {
      ull = strtoull(p->str, &endptr, p->base);
      UInt64 uval = p->val;
      if (ull != uval)
      {
        ++ret;
        UtIO::cout() << "strtoull(" << p->str << ") yields" << ((double) ull)
                  << UtIO::endl;
      }
      else if (*endptr != '\0')
      {
        ++ret;
        UtIO::cout() << "strtoull(" << p->str << ") end-of-token wrong" << UtIO::endl;
        UtIO::cout() << "end-of-token is" << endptr << UtIO::endl;
      }
    }
  } // for

  UtString fname, errmsg;
#if ! pfWINDOWS
  if (!OSExpandFilename(&fname, "$CARBON_HOME/foo", &errmsg))
  {
    UtIO::cout() << "$CARBON_HOME/foo failed: " << errmsg.c_str()
                 << UtIO::endl;
    ret = 1;
  }

  if (!OSExpandFilename(&fname, "$(CARBON_HOME)/foo", &errmsg))
  {
    UtIO::cout() << "$(CARBON_HOME)/foo failed: " << errmsg.c_str()
                 << UtIO::endl;
    ret = 1;
  }

  if (!OSExpandFilename(&fname, "${CARBON_HOME}/foo", &errmsg))
  {
    UtIO::cout() << "${CARBON_HOME}/foo failed: " << errmsg.c_str()
                 << UtIO::endl;
    ret = 1;
  }

  if (OSExpandFilename(&fname, "$(CARBON_HOME}/foo", &errmsg))
  {
    UtIO::cout() << "$(CARBON_HOME}/foo succeeded (EXPECTED FAILURE): " << errmsg.c_str()
                 << UtIO::endl;
    ret = 1;
  }

  if (OSExpandFilename(&fname, "${CARBON_HOME)/foo", &errmsg))
  {
    UtIO::cout() << "${CARBON_HOME)/foo succeeded (EXPECTED FAILURE): " << errmsg.c_str()
                 << UtIO::endl;
    ret = 1;
  }

  if (!OSExpandFilename(&fname, "$CARBON_HOME", &errmsg))
  {
    UtIO::cout() << "$CARBON_HOME failed: " << errmsg.c_str()
                 << UtIO::endl;
    ret = 1;
  }

  if (!OSExpandFilename(&fname, "foo/$CARBON_HOME", &errmsg))
  {
    UtIO::cout() << "foo/$CARBON_HOME failed: " << errmsg.c_str()
                 << UtIO::endl;
    ret = 1;
  }

  fname.clear();
  if (!OSExpandFilename(&fname, "a\\$CARBON_HOME", &errmsg))
  {
    UtIO::cout() << "a\\$CARBON_HOME failed: " << errmsg.c_str()
                 << UtIO::endl;
    ret = 1;
  }
  else if (strcmp(fname.c_str(), "a$CARBON_HOME") != 0)
  {
    UtIO::cout() << "a$CARBON_HOME != " << fname.c_str()
                 << UtIO::endl;
    ret = 1;
  }

  if (!OSExpandFilename(&fname, "$C\\ARBON_HOME", &errmsg))
  {
    UtIO::cout() << "$CA\\RBON_HOME failed: " << errmsg.c_str()
                 << UtIO::endl;
    ret = 1;
  }
  
  if (OSExpandFilename(&fname, "$CARBON_HOME\\", &errmsg))
  {
    UtIO::cout() << "$CARBON_HOME\\ did not fail!"
                 << UtIO::endl;
    ret = 1;
  }

  if (!OSExpandFilename(&fname, "~root/foo", &errmsg))
  {
    UtIO::cout() << "~root/foo failed!" << errmsg
                 << UtIO::endl;
    ret = 1;
  }

  if (!OSExpandFilename(&fname, "~root", &errmsg))
  {
    UtIO::cout() << "~root failed!" << errmsg
                 << UtIO::endl;
    ret = 1;
  }

  if (OSExpandFilename(&fname, "~no_way_this_is_a_real_user", &errmsg))
  {
    UtIO::cout() << "~no_way_this_is_a_real_user did not fail!"
                 << UtIO::endl;
    ret = 1;
  }
#endif

  if (OSIsDirectoryDelimiter('-') || ! OSIsDirectoryDelimiter('/'))
  {
    UtIO::cout() << "OSIsDirectoryDelimiter (1) failed"
                 << UtIO::endl;
    ret = 1;
  }

  fname.clear();
  UtString path;

  OSParseFileName("/good/nice/file", &path, &fname);
  if ((path.compare("/good/nice") != 0) ||
      (fname.compare("file") != 0))
  {
    UtIO::cout() << "Unix file path not parsed properly" << UtIO::endl;
    ret = 1;
  }
  
  path = "/good/nice/file";
  OSReplaceDirectoryDelim(&path, '+');
  if (path.compare("+good+nice+file") != 0)
  {
    UtIO::cout() << "ReplaceDirectoryDelim on Unix file path not parsed properly" << UtIO::endl;
    ret = 1;
  }

  OSConstructFilePath(&path, "this/dir/", "gr");
  if (path.compare("this/dir/gr") != 0)
  {    
    UtIO::cout() << "OSConstructFilePath on Unix file path (1) incorrect" << UtIO::endl;
    ret = 1;

  }

  OSConstructFilePath(&path, "this/dir", "gr");
  if (path.compare("this/dir/gr") != 0)
  {    
    UtIO::cout() << "OSConstructFilePath on Unix file path (2) incorrect" << UtIO::endl;
    ret = 1;

  }

  // Should work on both windows and unix but only in the unit-util
  // test itself. We expect argproc.in to exist
  OSGetCurrentDir(&path);
  UtString result;
  if (OSIsDirectoryDelimiter(path[path.size() - 1]))
    path.erase(path.size() - 1);
  OSConstructFilePath(&result, path.c_str(), "argproc.in");

  UtString flags;
  if (! OSStatFile(result.c_str(), "e", &flags))
  {
    UtIO::cout() << "Could not find " << result << UtIO::endl;
    ret = 1;
  }

  // will only work on windows
#if pfWINDOWS

  if (OSIsDirectoryDelimiter('-') || ! OSIsDirectoryDelimiter('\\'))
  {
    UtIO::cout() << "OSIsDirectoryDelimiter (2) failed"
                 << UtIO::endl;
    ret = 1;
  }


  OSParseFileName("\\good\\nice\\file", &path, &fname);
  if ((path.compare("\\good\\nice") != 0) ||
      (fname.compare("file") != 0))
  {
    UtIO::cout() << "Windows file path not parsed properly" << UtIO::endl;
    ret = 1;
  }

  OSParseFileName("\\good\\nice/file", &path, &fname);
  if ((path.compare("\\good\\nice") != 0) ||
      (fname.compare("file") != 0))
  {
    UtIO::cout() << "Windows/Unix-mixed file path not parsed properly" << UtIO::endl;
    ret = 1;
  }

  path = "/good\\nice/file";
  OSReplaceDirectoryDelim(&path, '+');
  if (path.compare("+good+nice+file") != 0)
  {
    UtIO::cout() << "ReplaceDirectoryDelim on Windows file path not parsed properly" << UtIO::endl;
    ret = 1;
  }

  OSConstructFilePath(&path, "this\\dir/", "gr");
  if (path.compare("this\\dir/gr") != 0)
  {    
    UtIO::cout() << "OSConstructFilePath on Windows file path (1) incorrect" << UtIO::endl;
    ret = 1;

  }

  OSConstructFilePath(&path, "this/dir\\", "gr");
  if (path.compare("this/dir\\gr") != 0)
  {    
    UtIO::cout() << "OSConstructFilePath on Windows file path (2) incorrect" << UtIO::endl;
    ret = 1;

  }

#endif

  ret |= testOSDirLoop();

  ret |= testOSFileOps();

  return ret;
} // int oswraptest

// This test always returns success.  Pass/fail is determined by
// diffing the output with platform-specific gold files.
int oswrap64BitModels()
{
  if (OSSupports64BitModels()) {
    UtIO::cout() << "Can generate 64-bit models" << UtIO::endl;
  } else {
    UtIO::cout() << "Can not generate 64-bit models" << UtIO::endl;
  }

  return 0;
}
