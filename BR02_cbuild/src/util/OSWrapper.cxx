// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/OSWrapper.h"

#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "util/UtWildcard.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/c_util.h"

#include "sys/types.h"
#include <ctime>
#include <cstdio>

#include <cstring>
#include <cerrno>
#include <cstdlib>
#include <limits.h>
#include <float.h>
#include <math.h>
#include <signal.h>

//! Generic Directory delimiter
/*!
  Works for path creation on every platform.
  However, you cannot parse with this because Windows can have either
  / or \\
*/
#  define DIRECTORYDELIMITER '/'

#if pfUNIX
//! Unix-based directory-delimiters
#  define DIRECTORYDELIMITER_STR "/"
#  include <dirent.h> // for opendir, etc.
#elif pfWINDOWS
// Windows can handle forward-slash as well as backslash
#  define DIRECTORYDELIMITER_STR "/\\"
#  include "dirent-MinGW.h" // for opendir, etc.
#else
#  error "For what OS are we compiling?"
#endif

#include <sys/stat.h>

#if !pfWINDOWS
#include <sys/wait.h>
#endif

#include <ctype.h>              // isalnum

#include <fcntl.h>

#if pfUNIX
#include <unistd.h>
#include <pwd.h>
#include <stdlib.h>
#endif

#if pfWINDOWS
#include <windows.h>
#include <process.h>
#if pfGCC
#include <unistd.h>
#endif

// Windows API has extra leading "_"
#if !pfCYGWIN // perhaps this should be pfMSVC_8
#define stat _stat
#define getpid _getpid
#endif
#endif

// Static data member definitions
// These are pointers to functions, initialized with the functions
// this module is linked with.  These values will be replaced with
// functions that the Carbon Model is linked with when it initializes.
// Pedantic note:
// If any of these initializers (e.g., stdout) were to be a macro that
// resolved to a function call, this initialization would be compiled
// into a static initializer function and it wouldn't be called if the
// main program is compiled with a version of C++ incompatible with
// the one that compiled this module.  That would be bad.  Hopefully
// these will remain link-time resolvable values.
OSStdio::fwrite_typedef OSStdio::pfwrite = fwrite;
OSStdio::fflush_typedef OSStdio::pfflush = fflush;
FILE* OSStdio::mstdout = stdout;
FILE* OSStdio::mstderr = stderr;

static int sWrapStat(const char* path, struct stat* buf);

//! Closure for OSDirLoop
class OSDirLoop::DirClosure
{
public: CARBONMEM_OVERRIDES
  DirClosure(const char* directory, const char* wildcardPattern) :
    mDirDesc(NULL), mDirEntry(NULL), mDir(directory), 
    mWildcard(wildcardPattern)
  {}
  
  ~DirClosure()
  {
    if (mDirDesc)
      closedir(mDirDesc);
    mDirDesc = NULL;
    mDirEntry = NULL;
  }
  
  void openDir()
  {
    mDirDesc = opendir(mDir.c_str());
    if (mDirDesc == NULL)
    {
      UtString errBuf;
      mErrMsg << mDir << ": " << OSGetLastErrmsg(&errBuf);
    }
    else
      internalNext();
  }
  
  bool isError() const
  {
    return ! mErrMsg.empty();
  }
  
  const char* getError() const
  {
    const char* ret = NULL;
    if (! mErrMsg.empty())
      ret = mErrMsg.c_str();
    return ret;
  }
  
  bool atEnd() const
  {
    return ((mDirDesc == NULL) || (mDirEntry == NULL) || ! mErrMsg.empty());
  }

  const char* deReference() const
  {
    const char* ret = NULL;
    if (! atEnd())
      ret = mDirEntry->d_name;
    return ret;
  }

  const char* getFullPath(UtString* buf) const
  {
    const char* ret = NULL;
    buf->clear();
    if (! atEnd())
    {
      OSConstructFilePath(buf, mDir.c_str(), mDirEntry->d_name);
      ret = buf->c_str();
    }
    return ret;
  }

  void next()
  {
    if (! atEnd())
      internalNext();
  }
  
private:  
  DIR* mDirDesc;
  struct dirent* mDirEntry;
  UtString mDir;
  UtString mErrMsg;
  UtWildcard mWildcard;

  // Assumes we are not at the end
  void internalNext()
  {
    do {
      mDirEntry = readdir(mDirDesc);
    } while (mDirEntry && 
             (! mWildcard.isMatch(mDirEntry->d_name) ||
              (strcmp(".", mDirEntry->d_name) == 0) ||
              (strcmp("..", mDirEntry->d_name) == 0)));
  }
};




// OSDirLoop
OSDirLoop::OSDirLoop(const char* directory, const char* wildcardPattern)
{
  mDirClosure = new DirClosure(directory, wildcardPattern);
  mDirClosure->openDir();
}

OSDirLoop::~OSDirLoop()
{
  delete mDirClosure;
}

bool OSDirLoop::atEnd() const
{
  return mDirClosure->atEnd();
}

const char* OSDirLoop::operator*() const
{
  return mDirClosure->deReference();
}

const char* OSDirLoop::getFullPath(UtString* buf) const
{
  return mDirClosure->getFullPath(buf);
}

OSDirLoop& OSDirLoop::operator++()
{
  mDirClosure->next();
  return *this;
}

bool OSDirLoop::isError() const
{
  return mDirClosure->isError();
}

const char* OSDirLoop::getError() const
{
  return mDirClosure->getError();
}

//! Closure for OSStatEntry
struct OSStatEntry::StatClosure
{
  CARBONMEM_OVERRIDES

  StatClosure() : mNoExist(false), mNoRead(false)
#if pfUNIX
                  , mIsOwner(false), mIsGrp(false)
#endif
  {}
  
  ~StatClosure() {}
  
  int callStat(const char* path)
  {
    int status = sWrapStat(path, &mBuf);
    if (status == -1)
    {
      UtString errBuf;
      mErrorMsg << path << ": " << OSGetLastErrmsg(&errBuf);
      
      mNoRead = true;
      if (errno == ENOENT)
        mNoExist = true;
      else if (errno == EACCES)
        mNoRead = true; // redundant, but documentative 
    }
    else
    {
#if pfUNIX
      mIsOwner = (getuid() == mBuf.st_uid);
      mIsGrp = (getgid() == mBuf.st_gid);

      // To determine if group matches, need to iterate over all groups the user
      // belongs to.
      int ngroups = getgroups(0, NULL);
      mIsGrp = false;
      if (ngroups < 0) {
        status = ngroups;
      } else {
        gid_t *groups = CARBON_ALLOC_VEC(gid_t, ngroups);
        // callStat should return -1 on error or 0 if succeeds, so status is set based on
        // the return value from getgroups.
        int getgroups_status = getgroups(ngroups, groups);
        if (getgroups_status >= 0) {
          for (int count = 0; (count < ngroups) and not mIsGrp; ++count) {
            mIsGrp = (groups[count] == mBuf.st_gid);
          }
          status = 0;
        } else {
          status = getgroups_status;
        }
        CARBON_FREE_VEC(groups, gid_t, ngroups);
      }
#endif
    }
    return status;
  }
  
  bool isE() const {
    return ! mNoExist;
  }

  bool isR() const {
    bool isReadable = ! mNoRead;
    if (isReadable)
    {
#if pfUNIX
      isReadable = (mIsOwner && ((mBuf.st_mode & S_IRUSR) != 0));
      isReadable |= (mIsGrp && ((mBuf.st_mode & S_IRGRP) != 0));
      isReadable |= ((mBuf.st_mode & S_IROTH) != 0);
#elif pfWINDOWS
      isReadable = ((mBuf.st_mode & S_IREAD) != 0);
#endif
    }
    return isReadable;
  }
  
  bool isW() const {
    bool isWritable = ! isError();
    if (isWritable)
    {
#if pfUNIX
      isWritable = (mIsOwner && ((mBuf.st_mode & S_IWUSR) != 0));
      isWritable |= (mIsGrp && ((mBuf.st_mode & S_IWGRP) != 0));
      isWritable |= ((mBuf.st_mode & S_IWOTH) != 0);
#elif pfWINDOWS
      isWritable = ((mBuf.st_mode & S_IWRITE) != 0);
#endif
    }
    return isWritable;
  }
  
  bool isF() const {
    bool isRegFile = ! isError();
    if (isRegFile)
      isRegFile = ((mBuf.st_mode & S_IFREG) != 0);
    return isRegFile;
  }

  bool isD() const {
    bool isDirectory = ! isError();
    if (isDirectory)
      isDirectory = ((mBuf.st_mode & S_IFDIR) != 0);
    return isDirectory;
  }

  bool isX() const {
    bool isExec = ! isError();
    if (isExec)
    {
#if pfUNIX
      isExec = (mIsOwner && ((mBuf.st_mode & S_IXUSR) != 0));
      isExec |= (mIsGrp && ((mBuf.st_mode & S_IXGRP) != 0));
      isExec |= ((mBuf.st_mode & S_IXOTH) != 0);
#endif
#if pfWINDOWS
      isExec = ((mBuf.st_mode & S_IEXEC) != 0);
#endif
    }
    return isExec;
  }
  
  SInt64 getFileSize() const
  {
    SInt64 ret = -1;
    if (! isError())
      ret = SInt64(mBuf.st_size);
    return ret;
  }

  UInt64 getModTime() const {
    return (UInt64) mBuf.st_mtime;
  }

  const UtString& getErrorMsg() const {
    return mErrorMsg;
  }

  bool isError() const {
    return ! mErrorMsg.empty();
  }

  void getDevInodePair(UInt64* device, UInt64* inode) const
  {
    if (! isError())
    {
      *device = mBuf.st_dev;
      *inode = mBuf.st_ino;
    }
    else
    {
      *device = 0;
      *inode = 0;
    }
  }

  UtString mErrorMsg;
  struct stat mBuf;
  bool mNoExist;
  bool mNoRead;
  bool mIsOwner;
  bool mIsGrp;
  UInt64 mModTime;
};

// OSStatEntry
OSStatEntry::OSStatEntry()
{
  mStatClosure = new StatClosure;
}

OSStatEntry::~OSStatEntry()
{
  delete mStatClosure;
}

bool OSStatEntry::exists() const
{
  return mStatClosure->isE();
}

bool OSStatEntry::isReadable() const
{
  return mStatClosure->isR();
}

bool OSStatEntry::isWritable() const
{
  return mStatClosure->isW();
}

bool OSStatEntry::isRegularFile() const
{
  return mStatClosure->isF();
}

bool OSStatEntry::isDirectory() const
{
  return mStatClosure->isD();
}

bool OSStatEntry::isExecutable() const
{
  return mStatClosure->isX();
}

bool OSStatEntry::isError() const
{
  return mStatClosure->isError();
}

void OSStatEntry::getDevInodePair(UInt64* device, UInt64* inode) const
{
  mStatClosure->getDevInodePair(device, inode);
}

SInt64 OSStatEntry::getFileSize() const
{
  return mStatClosure->getFileSize();
}

UInt64 OSStatEntry::getModTime() const
{
  return mStatClosure->getModTime();
}

static int sWrapStat(const char* path, struct stat* buf)
{
  int status = 0;
  do
  {
    status = stat(path, buf);
  } while ((status == -1) && (errno == EINTR));
  
  return status;
}

void OSGetRealPath(const char* relativePath, UtString* absolutePath)
{
  absolutePath->erase();
#if pfWINDOWS
  char actualPath[MAX_PATH+1];
  actualPath[0] = '\0';
  GetFullPathName(relativePath, MAX_PATH+1, actualPath, NULL);
#else
  char actualPath[PATH_MAX+1];
  actualPath[0] = '\0';
  realpath(relativePath, actualPath);
#endif
  absolutePath->assign(actualPath);
}

void OSParseFileName(const char* pathName, UtString* path, UtString* file)
{
  UtString pathCp = pathName;
  UtString::size_type lastSlash = pathCp.find_last_of(DIRECTORYDELIMITER_STR);
  path->erase();
  file->erase();
  if (lastSlash != UtString::npos)
  {
    UtString::size_type fileStart = lastSlash + 1;
    if (lastSlash > 0) // top-level dir/file check.
      path->assign(pathCp.c_str(), lastSlash);
    else 
      fileStart = 0;

    file->assign(pathCp.c_str() + fileStart, pathCp.size() - fileStart);
  }
  else
    *file = pathCp;
}

const char* OSGetTimeStr(const char* format, UtString* str)
{
  time_t t;
  char timeStr[256];
  std::time(&t);
  size_t numChars = strftime(timeStr, 256, format, localtime(&t));
  str->assign(timeStr, numChars);
  return str->c_str();
}

bool OSParseTime(const char* timeString, struct tm* tm)
{
#if pfWINDOWS
  unsigned int year, mon, mday, hour, min, sec;
  if (scanf("%u-%u-%u %u:%u:%u", 
            &year, &mon, &mday, &hour, &min, &sec) < 6
      || year < 1970 || mon < 1 || mon > 12 || mday < 1 || mday > 31
      || hour > 23 || min > 59 || sec > 60) {
    UtIO::cout() << "Invalid timestring: '" << timeString << "'. Required format: yyyy-mm-dd 24h:min:sec" << UtIO::endl;
    return false;	// failure
  }
  tm->tm_year = year;
  tm->tm_mon = mon;
  tm->tm_mday = mday;
  tm->tm_hour = hour;
  tm->tm_min = min;
  tm->tm_sec = sec;

  tm->tm_wday = tm->tm_yday = 0;	// ignored
  tm->tm_isdst = -1;	// Allow mktime to compute correct value
  return true;	// success
#else
  return strptime(timeString, "%Y-%m-%d %H:%M:%S", tm) != NULL;
#endif
}

// You might think you could use the PWD environment variable here, but
// Sun's /bin/sh doesn't maintain PWD leading to truely wacko behavior
// in test/shell where the 'cd api' doesn't get tracked.
//
void OSGetCurrentDir(UtString* cwd)
{
  cwd->clear();
  char cwdBuf[65000];
  if (getcwd(cwdBuf, 65000))
    cwd->assign(cwdBuf);
  else
    *cwd = "./";
}

int OSStatFileEntry(const char* path, OSStatEntry* entry, UtString* errMsg)
{
  errMsg->clear();
  int status = entry->mStatClosure->callStat(path);
  if (status == -1)
    *errMsg = entry->mStatClosure->getErrorMsg();
  return status;
}

int OSStatFile(const char* path, const char* options, 
                UtString* result)
{
  OSStatEntry entry;
  UtString errMsg;

  int status = OSStatFileEntry(path, &entry, &errMsg);
  
  UtString opts(options);
  int retStatus = 0;
  bool allFlagsPass = true;

  result->clear();
  result->resize(strlen(options), '0');
  
  if (status == 0)
  {
    UtString& resultRef = *result;
    // need the user and group ids for the caller in order to
    // determine permissions from file owner.
    
    int index = 0;
    for (UtString::const_iterator p = opts.begin(); p != opts.end(); ++p)
    {
      bool hasPrivilege = false; // general helper
      switch (*p)
      {
      case 'e':
        // if we got here, the file exists
        hasPrivilege = true;
        break;
      case 'f':
        hasPrivilege = entry.isRegularFile();
        break;
      case 'r':
        hasPrivilege = entry.isReadable();
        break;
      case 'w':
        hasPrivilege = entry.isWritable();
        break;
      case 'x':
        hasPrivilege = entry.isExecutable();
        break;
      case 'd':
        hasPrivilege = entry.isDirectory();
        break;
      } // switch
      allFlagsPass &= hasPrivilege;
      resultRef[index] = (hasPrivilege) ? '1' : '0';
      ++index;
    } // for
  } // if
  else {

    allFlagsPass = false;    

    bool testExist = false;
    if (! entry.exists())
      testExist = (opts.find_first_of('e') != UtString::npos);

    
    
    bool testAccess = false;
    if (entry.exists() && ! entry.isReadable())
      testAccess = (opts.find_first_of("r") != UtString::npos);
    
    if (! testExist && ! testAccess)
    {
      result->clear();
      *result = errMsg;
      retStatus = -1;
    }
  }
  
  if (retStatus != -1)
  {
    if (allFlagsPass)
      retStatus = 1;
    else
      retStatus = 0;
  }
  return retStatus;
}

bool OSIsDirectoryDelimiter(const char s)
{
  return strchr(DIRECTORYDELIMITER_STR, s) != NULL;
}

void OSConstructFilePath(UtString* result, 
                         const char* dir,
                         const char* file)
{
  // in case file and *result are the same
  UtString fileCp (file);
  // ditto with dir
  UtString dirCp(dir);
  result->clear();
  *result += dirCp;
  if (! OSIsDirectoryDelimiter((*result)[result->size() - 1]))
    *result += DIRECTORYDELIMITER;
  *result += fileCp;
}

bool OSGetFileSize(const char* filename, SInt64* fileSize, UtString* errMsg)
{
  bool isGood = true;
  OSStatEntry entry;

  int status = OSStatFileEntry(filename, &entry, errMsg);
  if (status != -1)
    *fileSize = entry.getFileSize();
  else
    isGood = false;
  return isGood;
}

int OSSystemBackground(const char* cmd, UtString* nonExitMsg) {
#if pfUNIX
  UtString buf;
  buf << cmd << " &";
  return OSSystem(buf.c_str(), nonExitMsg);
#endif
#if pfWINDOWS
  STARTUPINFO startup_info;
  PROCESS_INFORMATION proc_info;
  UtString buf;
  memset(&startup_info, 0, sizeof(startup_info));
  startup_info.cb = sizeof(startup_info);
  startup_info.dwFlags = STARTF_USESHOWWINDOW;
  startup_info.wShowWindow = SW_HIDE;
  buf << "cmd.exe /c " << cmd;
  nonExitMsg->clear();
  if (!CreateProcess(NULL, (char*) buf.c_str(), NULL, NULL, FALSE,
                     CREATE_NEW_CONSOLE, NULL, NULL,
                     &startup_info, &proc_info))
  {
    *nonExitMsg << "CreateProcess Error: " << (UInt32) GetLastError();
    return -1;
  }
  return 0;
#endif
} 

int OSSystem(const char* cmd, UtString* nonExitMsg)
{
#if 1
  nonExitMsg->clear();
  return system(cmd);
#else
  nonExitMsg->clear();
  int status = 1;
  FILE* sysCmd = popen(cmd, "w");
  if (sysCmd == NULL)
  {
    UtString errBuf;
    *nonExitMsg << cmd << ": " << OSGetLastErrmsg(&errBuf);
    return -1;
  }
  int sysRet = pclose(sysCmd);
  if (sysRet == -1)
  {
    UtString errBuf;
    *nonExitMsg << cmd << ": " << OSGetLastErrmsg(&errBuf);
    return -1;
  }

  /*
    do {
    sysRet = system (cmd);
    } while ((sysRet < 0) && (errno == EINTR));
  */
#if pfWINDOWS
  status = errno;
  *nonExitMsg << "pclose() returned errno: " << status;
#else
  if (WIFEXITED (sysRet))
    status = WEXITSTATUS(sysRet);
  else if (WIFSIGNALED(sysRet))
  {
    int termSig = WTERMSIG(status);
    *nonExitMsg << "terminated with signal: " << termSig;
    status = -1;
  }
  else
    // this can't happen, but let's be safe
    INFO_ASSERT(0, "Invalid status returned from system call.");
#endif
  return status;
#endif
}

bool OSExpandFilename(UtString* buf, const char* filename, UtString* errmsg)
{
  buf->clear();
  UtString envBuf, result, current;
  bool env = false;
  bool env_paren = false;  // $(ENV)
  bool env_brace = false;  // ${ENV}
  bool username = false;
  bool changed = true;
  
  // Run this up to 1000 times until the string settles
  current << filename;
  for (int count = 0; (count < 1000) && changed; ++count) {
    // Replace any expansions
    result.clear();
    for (const char* p = current.c_str(); (*p != '\0') || env || username; ++p)
    {
      bool slashed = (*p == '\\');
      if (slashed)
      {
        result << *p;
        ++p;
        if (*p == '\0')
        {
          *errmsg << "Backslash at end of token: " << filename;
          return false;
        }
      } 

      if (env || username)
      {
        if (!isalnum(*p) && (*p != '_') && !slashed)
        {
          if (env) {
            if ((*p == '(') && !env_paren && !env_brace) {
              env_paren = true;
            }
            else if ((*p == '{') && !env_paren && !env_brace) {
              env_brace = true;
            }
            else if ((*p != ')') && env_paren) {
              *errmsg << "Environment variable " << envBuf << " malformed, expected ).";
              return false;
            }
            else if ((*p != '}') && env_brace) {
              *errmsg << "Environment variable " << envBuf << " malformed, expected }.";
              return false;
            }
            else {
              if (env_paren || env_brace) {
                ++p;              // skip closer
              }
              const char* val = getenv(envBuf.c_str());
              if (val == NULL)
              {
                *errmsg << "Environment variable " << envBuf << " not found.";
                return false;
              }
              envBuf.clear();
              result << val;
              env = false;
            }
          }
#if !pfWINDOWS
          else if (username)
          {
            // tilde expansion should go here
            struct passwd* pswd = NULL;

            if (! envBuf.empty())
              do {
                pswd = getpwnam(envBuf.c_str());
              } while ((pswd == NULL) && (errno == EINTR));
            else
            {
              uid_t uid = getuid();
              do {
                pswd = getpwuid(uid);
              } while ((pswd == NULL) && (errno == EINTR));
              // pswd cannot be null here
              INFO_ASSERT(pswd, filename);
            }
          
            if (pswd == NULL)
            {
              *errmsg << "User name " << envBuf << " not found.";
              return false;
            }
            envBuf.clear();
            INFO_ASSERT(pswd->pw_dir, filename);
            result << pswd->pw_dir;
            username = false;
          }
#endif
        } // if
        else
          envBuf << *p;
      }

      if (!env && !username && (*p != '\0'))
      {
        if ((*p == '$') && !slashed)
          env = true;
#if !pfWINDOWS
        else if ((*p == '~') && !slashed)
          username = true;
#endif
        else
          result << *p;
      }
      // could happen if env or username is true and we are at the end
      // of the string.  ~username, for example
      if (*p == '\0')
      {
        --p;
        // if either of these are true, we have ourselves an infinite
        // loop
        INFO_ASSERT(!env, filename);
        INFO_ASSERT(!username, filename);
      }
    } // while

    // Check if it changed
    changed = (result != current);
    current = result;
  } // for

  // Return the result and remove any backslashes now
  buf->clear();
  for (const char* p = result.c_str(); (*p != '\0'); ++p) {
    if (strncmp(p,"\\$",2) != 0) {
      *buf << *p;
    }
  }

  return true;
} // bool OSExpandFilename


void OSContractFilename(UtString* buf, const char* filename)
{
  UtString cwd;
  OSGetCurrentDir(&cwd);
  if (strncmp(cwd.c_str(), filename, cwd.size()) == 0)
  {
    filename += cwd.size();
    if (OSIsDirectoryDelimiter(*filename))
      ++filename;
  }
  *buf = filename;
}

int OSChdir(const char* dir, UtString* reason)
{
  int stat;
  INFO_ASSERT(dir, "NULL dirname passed as parameter.");
  do {
    stat = chdir(dir);
  } while ((stat < 0) && (errno == EINTR));

  if (reason && (stat != 0))
  {
    *reason << dir << ": ";
    UtString errBuf;
    *reason << OSGetLastErrmsg(&errBuf);
  }
  // In case there is an os difference in error status
  if (stat != 0)
    stat = -1;
  return stat;
}

int OSMkdir(const char* dir, UInt32 mode, UtString* reason)
{
  (void) mode;			// dummy reference
  int stat = 0;
  INFO_ASSERT(dir, "NULL dirname passed as parameter.");
  if (mkdir (dir
#if !pfWINDOWS || pfCYGWIN
	     , mode_t(mode)
#endif
	     ) != 0)
    stat = -1;

  if (reason && (stat != 0))
  {
    *reason << dir << ": ";
    UtString errBuf;
    *reason << OSGetLastErrmsg(&errBuf);
  }
  return stat;
}


int OSRenameFile(const char *oldpath, const char *newpath, UtString* reason) {
  int stat = 0;

#if pfWINDOWS  // must clear out destination on windows first
  (void) OSUnlink(newpath, reason);
#endif

  if (rename (oldpath, newpath) != 0) {
    stat = -1;
    if (reason) {
      *reason << "rename(" << oldpath << ", " << newpath << "): ";
      UtString errBuf;
      *reason << OSGetLastErrmsg(&errBuf);
    }
  }
  return stat;
}

int OSRmdir(const char* pathname, UtString* reason) {
  int unlinkStat = 0;
  do {
    unlinkStat = rmdir(pathname);
  }  while ((unlinkStat < 0) && (errno == EINTR));
  if ((unlinkStat != 0) && reason) {
    UtString errBuf;
    *reason << pathname << ": " << OSGetLastErrmsg(&errBuf);
  }
  
  return unlinkStat;
}

// Recursively remove the contents of the specified directory
/*!
  Works similar to the "rm -r" shell command, but blocks interrupts.
  \param pathname directory to remove
  \param reason If delete fails and reason is not NULL the strerror is
  put here in the form of perror, ie., pathname: strerror. 

  \sa OSRmdir, OSUnlink

  \returns 0 if successful, -1 otherwise.
*/
int OSDeleteRecursive(const char* filespec, UtString* reason) {
  int ret = 0;

  // Is this a file or a directory
  UtString result;
  int status = OSStatFile(filespec, "wdf", &result);
  if ((status == 0) && (result[0] == '1') && (result.size() == 3) &&
      (result[1] == '1'))
  {
    // is a directory
    UtString file;
    for (OSDirLoop files(filespec, "*"); !files.atEnd(); ++files) {
      file.clear();
      files.getFullPath(&file);
      ret |= OSDeleteRecursive(file.c_str(), reason);
    }
    ret |= OSRmdir(filespec, reason);
  }
  else {
    ret |= OSUnlink(filespec, reason);
  }
  return ret;
} // int OSDeleteRecursive

int OSUnlink(const char* pathname, UtString* reason)
{
  int unlinkStat = 0;
  do {
    unlinkStat = unlink(pathname);
  }  while ((unlinkStat < 0) && (errno == EINTR));
  if ((unlinkStat != 0) && reason)
  {
    UtString errBuf;
    *reason << pathname << ": " << OSGetLastErrmsg(&errBuf);
  }
  
  return unlinkStat;
}

bool OSCopyFile(const char* srcName, const char* dstName, UtString* errmsg) {
  // This implementation is pretty slow.  Probably should use unbuffered
  // File I/O, but we haven't built UtIOStream wrappers for those yet.

  UtIBStream srcFile(srcName);
  if (!srcFile.is_open()) {
    *errmsg = srcFile.getErrmsg();
    return false;
  }

  UtOBStream dstFile(dstName);
  if (!dstFile.is_open()) {
    *errmsg = dstFile.getErrmsg();
    return false;
  }

# define MYBUFSIZ 100000
  char buf[MYBUFSIZ];
  UInt32 numRead;
  while ((numRead = srcFile.read(buf, MYBUFSIZ)) != 0) {
    if (!dstFile.writeStr(buf, numRead)) {
      *errmsg = dstFile.getErrmsg();
      return false;
    }
  }
  return true;
} // bool OSCopyFile

//! Compare two files.  If failure, return OSCompareFailed and
//! populate errmsg
OSCompareFileStatus OSCompareFiles(const char* file1,
                                   const char* file2,
                                   UtString* errmsg)
{
  UtIBStream f1(file1);
  if (!f1.is_open()) {
    *errmsg = f1.getErrmsg();
    return OSCompareFailed;
  }
  UtIBStream f2(file2);
  if (!f2.is_open()) {
    *errmsg = f2.getErrmsg();
    return OSCompareFailed;
  }

  UInt32 numRead1, numRead2;
  char buf1[MYBUFSIZ], buf2[MYBUFSIZ];
  while ((numRead1 = f1.read(buf1, MYBUFSIZ)) != 0) {
    numRead2 = f2.read(buf2, MYBUFSIZ);
    if (numRead1 != numRead2) {
      return OSCompareDifferent;
    }
    if (memcmp(buf1, buf2, numRead1) != 0) {
      return OSCompareDifferent;
    }      
  }
  // See if there are bytes left over in file2
  if (f2.read(buf2, 1) != 0) {
    return OSCompareDifferent;    
  }
  return OSCompareSame;
} // OSCompareFileStatus OSCompareFiles


#if !pfWINDOWS
SInt32 OSFork(UtString* reason)
{
  pid_t pid = 0;
  do
  {
    pid = fork();
  }
  while ((pid < 0) && (errno == EINTR));
  if ((pid < 0) && reason)
  {
    UtString errBuf;
    *reason << "Unable to fork: " << OSGetLastErrmsg(&errBuf);
  }

  return SInt32(pid);
}

int OSExecv(const char* program, char *const argv[], UtString* reason)
{
  int execStat = 0;
  do {
    execStat = execv(program, argv);
  } while ((execStat < 0) && (errno == EINTR));
  
  // will only get here if execv failed
  if ((execStat < 0) && reason)
  {
    UtString errBuf;
    *reason << "Could not exec: " << OSGetLastErrmsg(&errBuf);
  }
  return execStat;
}

int OSWaitPid(SInt32 pid, UtString* reason)
{
  INFO_ASSERT(reason, "NULL reason string passed in as a parameter.");
  int ret = 0;
  int wstat = 0;
  do {
    wstat = waitpid(pid_t(pid), &ret, 0);
  } while ((wstat == -1) && (errno == EINTR));
  if (wstat == -1)
  {
    UtString errBuf;
    *reason << "Error waiting for child process: " << OSGetLastErrmsg(&errBuf);
    ret = -1;
  }
  else
  {
    if (WIFSIGNALED(ret))
    {
      ret = WTERMSIG(ret);
      if (ret == 0)
        ret = 127;
      *reason << "Process terminated with signal: " << ret;
    }
    else if (WIFEXITED(ret))
      ret = WEXITSTATUS(ret);
    else
    {
      *reason << "Internal error: Problem evaluating status of child process.";
      ret = -1;
    }
  }
  return ret;
}
#endif

FILE* OSFOpen(const char* fname, const char* mode, UtString* reason)
{
  FILE* fd = NULL;
  do {
    fd = fopen(fname, mode);
  } while ((fd == NULL) && (errno == EINTR));

  if (! fd && reason)
  {
    UtString errBuf;
    *reason << fname << ": " << OSGetLastErrmsg(&errBuf);
  }
  return fd;
}

int OSSysOpen(const char* fname, int flags, mode_t mode, UtString* reason)
{
#if pfLINUX
  flags |= O_LARGEFILE;
#endif

  int fd;
  do {
    fd = open(fname, flags, mode);
  } while ((fd  < 0) && (errno == EINTR));

  if (fd < 0)
  {
    if (reason == NULL) {
      // do not pass an error message to the caller
    } else if (fname [0] == '\0') {
      // The error message looked kind of weird if there is an empty filename,
      // so use quotes in the name.
      UtString errBuf;
      *reason << "\"\": " << OSGetLastErrmsg(&errBuf);
    } else {
      UtString errBuf;
      *reason << fname << ": " << OSGetLastErrmsg(&errBuf);
    }
    
    fd = -1;   
  }
  return fd;
}

int OSSysClose(int fd, UtString* reason)
{
  int stat = 0;
  do {
    stat = close(fd);
  } while ((stat  < 0) && (errno == EINTR));

  if (stat < 0)
  {
    stat = -1;
    if (reason) {
      UtString errBuf;
      *reason << OSGetLastErrmsg(&errBuf);
    }
  }
  return stat;
}

const char* OSLegalizeFilename(const char* filename, UtString* buf)
{
  buf->clear();

  // 50 characters is used as a practical maximum for filenames, even though
  // more could be supported by many OSs.  Usually, in our system, long filenames
  // are a result of pathological synthesized names from parameter expansion,
  // and only the first 20 or so characters are interesting.
  for (const char* p = filename; (*p != '\0') && (buf->size() < 50); ++p)
  {
    // change illegal characters, max out at 50 chars
    if (!isalnum(*p) && (*p != '_'))
      *buf << "_";
    else
      *buf << *p;
  }
  return buf->c_str();
}

const char* OSFindExecutableInPath(const char* exe, UtString* buf)
{
  UtString result;
  const char* pathname = NULL;
  if (OSStatFile(exe, "x", &result) == 1)
  {
    if (OSExpandFilename(buf, exe, &result))
      pathname = buf->c_str();
    else
      *buf = result;            // leave error message in buf
  }
  else
  {
    // Look in the path for the directory
    for (StrToken tok(getenv("PATH"), ENVLIST_SEPARATOR); !tok.atEnd(); ++tok) {
      const char* dir = *tok;
      buf->clear();
      OSConstructFilePath(buf, dir, exe);
      pathname = buf->c_str();
      if (OSStatFile(pathname, "x", &result) == 1)
        break;
      else
        pathname = NULL;
    }
  }
  return pathname;
} // const char* OSFindExecutableInPath


#if !pfWINDOWS
UInt64 OSGetRusageTime(bool user, bool self)
{
  struct rusage myUsage;
  int status;

  // note that the man pages for linux limits the
  // fields the rusage structure that are actually maintined.
  // it appears that only the timevals are maintined on both linux and solaris
  status = getrusage( (self? RUSAGE_SELF: RUSAGE_CHILDREN), &myUsage);
  INFO_ASSERT(status == 0, "OSGetRusageTime: getrusage() returned non-zero");
  if ( user )
    return static_cast<UInt64>(myUsage.ru_utime.tv_sec) * 1000000
      + myUsage.ru_utime.tv_usec; // in microseconds
  else
    return static_cast<UInt64>(myUsage.ru_stime.tv_sec) * 1000000
      + myUsage.ru_stime.tv_usec; // in microseconds
}
#else
UInt64 OSGetRusageTime(bool user, bool /* self --unused */)
{
  static HANDLE me = 0;
  if (me == 0)
    me = GetCurrentProcess();   // Returns -1
  FILETIME dummy;
  FILETIME kernelTime;
  FILETIME userTime;
  BOOL status = GetProcessTimes(me, &dummy, &dummy, &kernelTime, &userTime);
  if (status == 0) {            // failed
    UtString msg;
    OSGetLastErrmsg(&msg);
    UtIO::cerr() << "GetProcessTimes(): " << msg;
  }

  UInt64 result;
  if (user) {
    ULARGE_INTEGER
      user = {{userTime.dwLowDateTime, userTime.dwHighDateTime}};
    result = user.QuadPart / 10; // Convert 100ns to us
  }
  else {
    ULARGE_INTEGER
      kernel = {{kernelTime.dwLowDateTime, kernelTime.dwHighDateTime}};
    result = kernel.QuadPart / 10; // Convert 100ns to us
  }
  
  return result;
}
#endif


template <typename T, typename U> void strToNumCheck(T* value, 
                                         const char* nptr, 
                                         char** endptr, 
                                         int errNo, 
                                         U maxVal,
                                         U minVal,
                                         int base,
                                         UtString* errMsg,
                                         bool forceError)
{
  bool converted = true;

  if (forceError   // used to catch parsing negative numbers into unsigned vars
      || ((errNo == ERANGE) && ((*value == minVal) || (*value == maxVal)))
      || (*value < minVal)
      || (*value > maxVal))
  {
    if (errMsg) {
      *errMsg << "Converted value `";
      errMsg->append(nptr, *endptr - nptr);
      *errMsg << "' is out of range [" << minVal << "," << maxVal << "]";
    }
    converted = false;
  }
  
  if (*endptr == nptr)
  {
    if (errMsg)
      *errMsg << "`" << *nptr << "' does not begin with a space, base-"
              << base << " digit, or +/-";
    converted = false;
  }
  
  if (! converted)
  {
    *value = 0;
    *endptr = const_cast<char*>(nptr);
  }
}

static void sCheckBase(int base)
{
  switch (base)
  {
  case 16:
  case 10:
  case 8:
  case 2:
  case 0:
    break;                      // all supported
  default:
    INFO_ASSERT((base == 0) || (base == 2) || (base == 8) || (base == 10) || (base == 16), "Unsupported radix.");
    break;
  }
}

SInt32 OSStrToS32(const char* nptr, char **endptr, int base, UtString* errMsg)
{
  sCheckBase(base);

  long value = strtol(nptr, endptr, base);
  strToNumCheck<long,SInt64>(&value, nptr, endptr, errno,
                             (SInt64) UtSINT32_MAX, 
                             (SInt64) UtSINT32_MIN,
                             base, errMsg, false);

  return (SInt32) value;
}

UInt32 OSStrToU32(const char* nptr, char **endptr, int base, UtString* errMsg)
{
  sCheckBase(base);

  unsigned long value = strtoul(nptr, endptr, base);

  // Unsigned values don't have a minimum return for strtoul and ull
  strToNumCheck<unsigned long, UInt64>(&value, nptr, endptr, errno,
                                       (UInt64) UtUINT32_MAX,
                                       (UInt64) UtUINT32_MIN,
                                       base, errMsg, (*nptr == '-'));

  return (UInt32) value;
}

SInt64 OSStrToS64(const char* nptr, char **endptr, int base, UtString* errMsg)
{
  sCheckBase(base);

// libg++ for the windows cross-compiler lacks this function  <-<- Is this still true?
#if pfWINDOWS && !pfGCC
  bool negate = false;
  SInt64 value = 0;
  if (*nptr == '-')
  {
    negate = true;
    ++nptr;
  }
  else if (*nptr == '+')
    ++nptr;

  value = strtoull(nptr, endptr, base);
  strToNumCheck(&value, nptr, endptr, errno, UtSINT64_MAX, 
                (SInt64) 0, base, errMsg, false);

  if (negate)
    value = -value;
  return value;
#else
  
  SInt64 value = strtoll(nptr, endptr, base);
  strToNumCheck(&value, nptr, endptr, errno, UtSINT64_MAX, 
                UtSINT64_MIN, base, errMsg, false);
  return value;
#endif
}

UInt64 OSStrToU64(const char* nptr, char **endptr, int base, UtString* errMsg)
{
  sCheckBase(base);

// libg++ for the windows cross-compiler lacks this function
#if pfWINDOWS && !pfGCC
  UInt64 result = 0;

  nptr = StringUtil::skip(nptr);

  if ( base == 0 ) {
    base = 10;    // assume 10, but this may be adjusted below if we find other radix indicators
    if ( strlen(nptr) > 1 ) {
      if ( *nptr == '0' ){
        base = 8; // either octal or hex, will be changed to hex below if it passes the hex test
        ++nptr;
        if ( strlen(nptr) > 1 ) {
          if ( ( *nptr == 'x' ) || ( *nptr == 'X' ) ){
            base = 16;
            ++nptr;
          }
        }
      }
    }
  }

  unsigned char char_map[256];
  const unsigned char cIllegal = 255;
  memset(char_map, cIllegal, 256);
  switch (base)
  {
  case 16:
    for (char* p = "abcdefABCDEF"; *p != '\0'; ++p)
      char_map[*p] = (unsigned char) (10 + tolower(*p) - 'a');
    // no break
  case 10:
    char_map['9'] = 9;
    char_map['8'] = 8;
    // no break
  case 8:
    for (char* p = "765432"; *p != '\0'; ++p)
      char_map[*p] = *p - '0';
    // no break
  case 2:
    char_map['0'] = 0;
    char_map['1'] = 1;
    break;
  default:
    INFO_ASSERT(0, "Unsupported radix.");
    break;
  };

  // Should do a range check like strtoull does!
  for (; char_map[*nptr] != cIllegal; ++nptr)
    result = base*result + char_map[*nptr];
  *endptr = const_cast<char*>(nptr);
  return result;

#else

  UInt64 value = strtoull(nptr, endptr, base);
  // Unsigned values don't have a minimum return for strtoul and ull
  strToNumCheck(&value, nptr, endptr, errno, UtUINT64_MAX, 
                UtUINT64_MIN, base, errMsg, (*nptr == '-'));
  return value;
 
#endif
}

double OSStrToD(const char* nptr, char **endptr, UtString* errMsg)
{
  double value = strtod(nptr, endptr);
  strToNumCheck(&value, nptr, endptr, errno, DBL_MAX, -DBL_MAX,
                10, errMsg, false);
  return value;
}


int OSSysRead(int fd, char* buf, UInt32 size, UtString* errmsg) {
  int result;
  int numRead = 0;
  // Read more from the file
  do
  {
    result = ::read(fd, buf, size);
    if (result > 0)
    {
      buf += result;
      numRead += result;
      size -= result;
    }
    // check for signal interruption.
  } while ((result == -1) && (errno == EINTR));
  
  if ((numRead == 0) && (result < 0))
  {
    numRead = -1;
    UtString errBuf;
    *errmsg << "OSSysRead failed: " << OSGetLastErrmsg(&errBuf);
  }

  return numRead;
} // int OSSysRead


int OSSysSeek(int fd, SInt64 offset, int whence, UtString* errmsg)
{
  int result;
  result = ::lseek(fd, (off_t) offset, whence);
  if (result == -1) {
    UtString errBuf;
    *errmsg << "OSSysSeek failed: " << OSGetLastErrmsg(&errBuf);
  }
  return result;
}

SInt64 OSSysTell(int fd, UtString* errmsg)
{
  SInt64 offset;
  offset = ::lseek(fd, off_t(0), SEEK_CUR);
  if (offset == -1) {
    UtString errBuf;
    *errmsg << "OSSysTell failed: " << OSGetLastErrmsg(&errBuf);
  }
  return offset;
}

 // not available on Windows Visual C++, but available with cross-compiler
int OSSysFTruncate(int fd, SInt64 size, UtString* errmsg)
{
#if pfGCC || pfLINUX || pfUNIX
  int result = ::ftruncate(fd, size);
#endif
#if pfMSVC
  int result = _chsize(fd, size);
#endif
  if (result == -1) {
    UtString errBuf;
    *errmsg << "OSSysFTruncate failed: " << OSGetLastErrmsg(&errBuf);
  }
  return result;
}

UInt32 OSGetPid()
{
  return (UInt32) getpid();
}

void OSGetPidStr(UtString* name) {
  name->clear();
  *name << OSGetPid();
}

void OSSleep(UInt32 numSeconds)
{
#if pfUNIX
  sleep(numSeconds);
#elif pfWINDOWS
  // specified as number of milliseconds
  Sleep(numSeconds * 1000);
#else
  INFO_ASSERT(0, "Not supported on this platform.");
#endif
}

#if ! pfWINDOWS
#include <signal.h>

void OSSigaction(int sigType, OSSigHandler sigFn)
{
  struct sigaction act;
  act.sa_handler = sigFn;
  act.sa_flags = SA_RESETHAND | SA_NODEFER;

  memset(&act.sa_mask, 0, sizeof(act.sa_mask));
  int sigactstat = sigaction(sigType, &act, NULL);
  INFO_ASSERT(sigactstat != -1, "Unrecognized status returned from sigaction.");
}

void OSSigSegvaction(int sigType, OSSigHandler sigFn)
{
  struct  sigaction act;
  stack_t sigstk;

  act.sa_handler = sigFn;
  act.sa_flags = SA_ONSTACK;

  // Setup Alternate Stack to be used after SegFault Signal is Received
  if((sigstk.ss_sp = malloc(SIGSTKSZ)) == NULL)
    UtIO::cerr() << "Cannot Setup Alternate Stack.\n";    
  sigstk.ss_size = SIGSTKSZ;
  sigstk.ss_flags = 0;
  if(sigaltstack(&sigstk,(stack_t *)0) < 0)
    UtIO::cerr() << "Error in Alternate Stack.\n"; 

  memset(&act.sa_mask, 0, sizeof(act.sa_mask));
  int sigactstat = sigaction(sigType, &act, NULL);
  INFO_ASSERT(sigactstat != -1, "Unrecognized status returned from sigaction.");
}

void OSKillProcess(int pid, OSKillSeverity sev)
{
  int signalType = SIGTERM;
  switch (sev)
  {
  case eKillInterrupt:
    signalType = SIGINT;
  case eKillTerminate:
    signalType = SIGTERM;
  case eKillAssasinate:
    signalType = SIGKILL;
  }
  kill((int) pid, signalType);
}

void OSKillMe(int sigType)
{
  kill((int) OSGetPid(), sigType);
}

#else
// windows only supports signal(), and not completely.
void OSSigaction(int, OSSigHandler)
{
  UtIO::cerr() << "OSSigAction not implemented on Windows." << UtIO::endl;
}

void OSKillProcess(int, OSKillSeverity)
{
  INFO_ASSERT(0, "Unsupported on windows.");
}

// no kill() in standard windows.
void OSKillMe(int)
{
  INFO_ASSERT(0, "Unsupported on windows.");
}
#endif

const char*
OSGetLastErrmsg(UtString* errmsg)
{
#define ERRORLEN_MAX 1024

  char buf[ERRORLEN_MAX];
  *errmsg = carbon_lasterror(buf, sizeof(buf));
  return errmsg->c_str();
}

bool
OSIsWine()
{
  // There is no supported method for determining if we're running on
  // Wine, but the Wine registry keys are likely to be there if we're
  // running on Wine, and unlikely to be there if we're not.  Of
  // course the key names could change in future versions.
#if pfWINDOWS
  HKEY hKey;
  LONG result = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\Wine",
                             0, KEY_QUERY_VALUE, &hKey);
  if (result == ERROR_SUCCESS) {  // Found the key, assume we're Wining
    RegCloseKey(hKey);
    return true;
  }
  else
    return false;
#else  // pfWINDOWS
  return false;                 // Not Wine if not compiled for Windows
#endif
}

void OSReplaceDirectoryDelim(UtString* path, const char c)
{
  UtString::size_type curPos = path->find_first_of(DIRECTORYDELIMITER_STR);
  while (curPos != UtString::npos)
  {
    (*path)[curPos] = c;
    curPos = path->find_first_of(DIRECTORYDELIMITER_STR, curPos);
  }
}

const char* OSTimeToString(UInt64 time, UtString* buf) {
  time_t t = time;
#if pfUNIX
  char cbuf[100];                // man page says "at least 26"
  ctime_r(&t, cbuf);
  *buf = cbuf;
#else
  *buf = ctime(&t);             // ctime_r not avail on windows?
#endif

  return buf->c_str();
}

#if pfWINDOWS
// I found this implementation at
//     http://www.openasthra.com/wp-content/uploads/gettimeofday.c
// happy happy joy joy!
//
// I found a bug in this package which is that it wasn't giving the same
// absolute time numbers as Linux for tv_usec, for operations that were
// as coincident as I could manage.  I experimentally determined a correction
// of 1890091644.  I didn't attempt to determine where that number might come
// from.
#include <time.h>

#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif

struct timezone 
{
  int  tz_minuteswest; /* minutes W of Greenwich */
  int  tz_dsttime;     /* type of dst correction */
};

static int gettimeofday(struct timeval *tv, struct timezone *tz)
{
  FILETIME ft;
  UInt64 tmpres = 0;
  static int tzflag;

  if (NULL != tv)
  {
    GetSystemTimeAsFileTime(&ft);

    tmpres |= ft.dwHighDateTime;
    tmpres <<= 32;
    tmpres |= ft.dwLowDateTime;

    /*converting file time to unix epoch*/
    tmpres -= DELTA_EPOCH_IN_MICROSECS; 
    tmpres /= 10;  /*convert into microseconds*/

#define MEASURED_CORRECTION 1890091644
    tv->tv_sec = (UInt32)(tmpres / 1000000UL) - MEASURED_CORRECTION;
    tv->tv_usec = (UInt32)(tmpres % 1000000UL);
  }

  if (NULL != tz)
  {
    if (!tzflag)
    {
      _tzset();
      tzflag++;
    }
#if _MSC_VER >= 0x0800
    long seconds = 0;
    _get_timezone(&seconds);
    tz->tz_minuteswest = seconds / 60;
    _get_daylight(&tz->tz_dsttime);
#else
    tz->tz_minuteswest = _timezone / 60;
    tz->tz_dsttime = _daylight;
#endif
  }

  return 0;
}

#else

#include <sys/time.h>

#endif

UInt64 OSGetSecondsSince1970() {
  UInt64 time = 0;
  struct timeval tv;
  if (gettimeofday(&tv, NULL) == 0) {
    time = static_cast<UInt64>((UInt32) tv.tv_sec);
  }
  return time;
}

bool OSSupports64BitModels()
{
// We only support 64-bit models on Linux
#if pfLINUX || pfLINUX64
  // The hardware must support it.  uname should return x86_64
  UtString msg;
  bool hardware64Bit = (OSSystem("uname -i | grep -q x86_64", &msg) == 0);
  // The Linux64 area must exist.  Check for cbuild.
  bool hasLinux64 = false;
  const char* carbonHome = getenv("CARBON_HOME");
  if (carbonHome != NULL) {
    UtString cbuildPath = carbonHome;
    cbuildPath += "/Linux64/bin/cbuild";
    OSStatEntry statEntry;
    OSStatFileEntry(cbuildPath.c_str(), &statEntry, &msg);
    hasLinux64 = statEntry.exists();
  }

  return hardware64Bit && hasLinux64;
#else
  return false;
#endif
}
