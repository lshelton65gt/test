// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file

  Implements ordering nodes in a directed graph. If there are cycles a
  virtual function is called to find spots to break them.
*/

#include "util/GraphQueue.h"

UtGraphQueue::UtGraphQueue() :
  mGraph(NULL), mLiveEdges(NULL), mLiveNodes(NULL), mNumRemaining(0),
  mReady(NULL), mNewReady(NULL), mDepth(0)
{
}

UtGraphQueue::~UtGraphQueue()
{
  if (mReady != NULL) {
    delete mReady;
  }
  if (mNewReady != NULL) {
    delete mNewReady;
  }
  if (mDeferredReady != NULL) {
    delete mDeferredReady;
  }
  if (mLiveEdges != NULL) {
    INFO_ASSERT(mLiveEdges->empty(), "Live edges not freed in destructor.");
    delete mLiveEdges;
  }
  if (mLiveNodes != NULL) {
    INFO_ASSERT(mLiveNodes->empty(), "Live nodes not freed in destructor.");
    delete mLiveNodes;
  }
}

void 
UtGraphQueue::init(Graph* graph, NodeCmp* graphNodeCmp, EdgeCmp* graphEdgeCmp)
{
  // Make sure we only init once
  INFO_ASSERT(mGraph == NULL, "init called more than once.");
  mGraph = graph;

  // Allocate various data
  NodeCmpWrapper ncw(graphNodeCmp);
  mReady = new NodeSet(ncw);
  mNewReady = new NodeSet(ncw);
  mDeferredReady = new NodeSet(ncw);
  EdgeCmpWrapper ecw(graphEdgeCmp);
  mLiveEdges = new LiveEdges(ecw);
  mLiveNodes = new NodeSet(ncw);

  // Initialize all graph nodes. As part of this we set its fanin
  // count to 0. Wel also assume all nodes have no fanouts. Both of
  // these bits of information get updated to reflect reality in the
  // next loop.
  typedef UtSet<GraphNode*> NoFanoutNodes;
  NoFanoutNodes noFanoutNodes;
  mNumRemaining = 0;
  for (Iter<GraphNode*> loop = mGraph->nodes(); !loop.atEnd(); ++loop) {
    GraphNode* node = *loop;
    initGraphNode(node);
    putFaninCount(node, (SIntPtr)0);
    noFanoutNodes.insert(node);
    mLiveNodes->insert(node);
    ++mNumRemaining;
  }

  // Compute the fanin count and find nodes with no fanout
  for (Iter<GraphNode*> n = mGraph->nodes(); !n.atEnd(); ++n) {
    // Iterate over all the edges for this node
    GraphNode* from = *n;
    for (Iter<GraphEdge*> e = mGraph->edges(from); !e.atEnd(); ++e) {
      // Get the end point of this edge
      GraphEdge* edge = *e;
      GraphNode* to = mGraph->endPointOf(edge);

      // Initialize various data for the from, to, and edge
      if (initGraphEdge(from, edge, to)) {
        incrFaninCount(to);
        noFanoutNodes.erase(from);
        mLiveEdges->insert(edge);
      }
    }
  }

  // Add nodes with no fanin to the ready set and schedule any nodes
  // with no fanout at the end.
#ifdef PRE_SECHEDULE_ZERO_FANOUT_NODES
  int size = mNumRemaining;
  for (Iter<GraphNode*> loop = mGraph->nodes(); !loop.atEnd(); ++loop) {
    // Check if the node has no fanout. We can schedule those
    // immediately which takes precedence over putting it in the ready
    // queue.
    GraphNode* node = *loop;
    if (noFanoutNodes.find(node) != noFanoutNodes.end()) {
      scheduleNode(node, size);
      int count = mLiveNodes->erase(node);
      INFO_ASSERT(count == 1, "GraphNode not live.");
      --mNumRemaining;
    } else {
      // Check if the node has no fanin. If so at it to the ready
      // queue
      if (getFaninCount(node) == 0) {
        mReady->insert(node);
      }
    }
  }
#else
  for (Iter<GraphNode*> loop = mGraph->nodes(); !loop.atEnd(); ++loop) {
    GraphNode* node = *loop;
    // Check if the node has no fanin. If so at it to the ready
    // queue
    if (getFaninCount(node) == 0) {
      mReady->insert(node);
    }
  }
#endif
} // void UtGraphQueue::init

void UtGraphQueue::scheduleWalk()
{
  // In a loop process the graph nodes to be scheduled
  while (mNumRemaining > 0) {
    // If there are no ready nodes, try to break the cycle
    while (mReady->empty()) {
      // Break the cycle and potential update the ready queue
      breakCycle();
      updateReadyQueue();
    }

    // Ask our subclass which of the current ready nodes should be
    // scheduled in the current iteration. At least one ready node
    // must be scheduled.
    SInt32 ready_cnt = mReady->size();
    UtArray<GraphNode*> ready(ready_cnt); // pre-size.
    SInt32 pos=0;
    for (NodeSet::iterator iter=mReady->begin();
         iter!=mReady->end();
         ++iter) {
      ready[pos++] = *iter;
    }

    UtSet<GraphNode*> deferred;
    deferReadyNodes(ready,&deferred);
    ready.clear();

    // Schedule the ready nodes now. This removes edges and
    // potentially adds more nodes to the ready queue. But we can't
    // insert while we iterate so we have a temporary set for that.
    ++mDepth;
    bool scheduled_node = false;
    for (NodeSetLoop l(*mReady); !l.atEnd(); ++l) {
      // Schedule the node
      GraphNode* node = *l;
      if (deferred.find(node) == deferred.end()) {
        scheduled_node = true;
        scheduleNode(node, mDepth);
        --mNumRemaining;

        // Updated the edge information for this node
        breakNode(node);
        int count = mLiveNodes->erase(node);
        INFO_ASSERT(count == 1, "GraphNode not live.");
      } else {
        rememberDeferredReady(node);
      }
    } // for

    INFO_ASSERT(scheduled_node, "All ready nodes deferred.");

    // Clear the ready queue and then add the new ready nodes
    mReady->clear();
    updateReadyQueue();
  } // while
} // void UtGraphQueue::scheduleWalk

void UtGraphQueue::putNodeData(GraphNode* node, SIntPtr data)
{
  mGraph->setScratch(node, data);
}

SIntPtr UtGraphQueue::getNodeData(GraphNode* node) const
{
  return mGraph->getScratch(node);
}

void UtGraphQueue::initGraphNode(GraphNode*)
{
  return;
}

bool UtGraphQueue::initGraphEdge(GraphNode*, GraphEdge*, GraphNode*)
{
  // Assume we should use this edge
  return true;
}

void UtGraphQueue::breakEdge(GraphEdge* edge)
{
  // This must be a live edge
  if (mLiveEdges->find(edge) == mLiveEdges->end()) {
    return;
  }

  // Check if the to node has already been scheduled. This can occur
  // because we schedule nodes with no fanout at maximum depth.
  GraphNode* to = mGraph->endPointOf(edge);
  if (mLiveNodes->find(to) != mLiveNodes->end()) {
    // Decrement the fanin count for any edges on this node
    if (decrFaninCount(to) == 0) {
      // This node is now ready. Note that we don't insert it into the
      // ready queue because this routine could be called while
      // iterating over the ready queue. Instead it gets put into the
      // new ready queue. It is up to the caller to update the ready
      // queue at the appropriate times.
      mNewReady->insert(to);
    }
  }

  // This is now a broken edge
  int count = mLiveEdges->erase(edge);
  INFO_ASSERT(count == 1, "GraphEdge not live.");
}

void UtGraphQueue::breakNode(GraphNode* node)
{
  // This must be a live node
  INFO_ASSERT(mLiveNodes->find(node) != mLiveNodes->end(), "GraphNode not live.");

  // Break all the edges for this node
  for (Iter<GraphEdge*> i = mGraph->edges(node); !i.atEnd(); ++i) {
    // If the edge was already broken, don't process it again
    GraphEdge* edge = *i;
    breakEdge(edge);
  }
}

void UtGraphQueue::deferReadyNodes(const UtArray<GraphNode*> & /*ready*/, UtSet<GraphNode*> * /*deferred*/)
{
  // default implementation: do not defer any nodes.
}

void UtGraphQueue::rememberDeferredReady(GraphNode * node)
{
  mDeferredReady->insert(node);
}

void UtGraphQueue::updateReadyQueue()
{
  INFO_ASSERT(mReady->empty(), "Ready queue has not been cleared.");
  mReady->swap(*mNewReady);
  mReady->insert(mDeferredReady->begin(),mDeferredReady->end());
  mDeferredReady->clear();
}

bool
UtGraphQueue::NodeCmp::operator()(const GraphNode* gn1, const GraphNode* gn2)
  const
{
  int cmp = carbonPtrCompare(gn1, gn2);
  return (cmp < 0);
}

bool
UtGraphQueue::EdgeCmp::operator()(const GraphEdge* ge1, const GraphEdge* ge2)
  const
{
  int cmp = carbonPtrCompare(ge1, ge2);
  return (cmp < 0);
}

void UtGraphQueue::incrFaninCount(GraphNode* node)
{
  SIntPtr faninCount = getFaninCount(node);
  ++faninCount;
  putFaninCount(node, faninCount);
}

SIntPtr UtGraphQueue::decrFaninCount(GraphNode* node)
{
  SIntPtr faninCount = getFaninCount(node);
  --faninCount;
  putFaninCount(node, faninCount);
  return faninCount;
}

SIntPtr UtGraphQueue::getFaninCount(GraphNode* node)
{
  return getNodeData(node);
}

void UtGraphQueue::putFaninCount(GraphNode* node, SIntPtr faninCount)
{
  putNodeData(node, faninCount);
}

