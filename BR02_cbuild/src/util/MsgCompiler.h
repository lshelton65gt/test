// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// please do not use "using namespace std"

#include <sstream>
#include <fstream>
#include <cassert>
#include <set>
#include <stdio.h>
#include <string>
#include <vector>

class MsgElement
{
public: // no CARBONMEM_OVERRIDES
  MsgElement() {}
  virtual ~MsgElement() {}
  virtual void writeValue(std::ofstream& of, const char* var) = 0;
  virtual void writeHelpAnnotation(std::string* str) = 0;
};
  
class MsgString: public MsgElement
{
public: // no CARBONMEM_OVERRIDES
  MsgString(const char* str) {
    mString = str;
  }

  virtual void writeValue(std::ofstream& of, const char* var)
  {
    of << "  " << var << " << \"";

    // Need to backslash any backslashes or double-quotes found
    // in mString...
    for (const char* s = mString.c_str(); *s != '\0'; ++s) {
      char c = *s;
      switch (c) {
      case '"':  of << "\\\""; break;
      case '\n': of << "\\n";  break;
      case '\t': of << "\\t";  break;
      case '\\': of << "\\";   break;
      default:   of << c;      break;
      }
    }

    of << "\";\n";
  }
    
  virtual void writeHelpAnnotation(std::string* str) {
    *str += mString;
  }

private:
  std::string mString;
};

class MsgArg: public MsgElement
{
public: // no CARBONMEM_OVERRIDES
  MsgArg()
  {
  }

  ~MsgArg()
  {
  }

  virtual void writeValue(std::ofstream& of, const char* var)
  {
    of << "  " << var << " << " << mVar << ";\n";
  }    
    
  void addType(const std::string& type)
  {
    mTypes.push_back(type);
/*
    if (strcmp(type.c_str(), "int") == 0)
      mFormat += "d";
    else if (strcmp(type.c_str(), "long") == 0)
      mFormat += "l";
    else if (strcmp(type.c_str(), "unsigned") == 0)
      mFormat += "u";
    else if (mFormat.size() == 1)
        mFormat += "s";
*/
  }

  void putIsPointer()
  {
    mPtrRef += "*";
  }

  void putIsReference()
  {
    mPtrRef += "&";
  }

  const char* getPtrRef() const {return mPtrRef.c_str();}

  void putVariableName(const std::string& var)
  {
    mVar = var;
  }

  const char* getVariableName() {return mVar.c_str();}

//  const std::string& getFormat()
//  {
//    return mFormat;
//  }

  void print()
  {
    std::vector<std::string>::iterator p;
    int prefix=2;
    for (p = mTypes.begin(); p != mTypes.end(); ++p)
    {      
      printf("%*s%s", prefix, "  ", p->c_str());
      prefix = 1;
    }
    printf("%s %s\n", mPtrRef.c_str(), mVar.c_str());
  }

  void write(std::ofstream& of)
  {
    std::vector<std::string>::iterator p;
    for (p = mTypes.begin(); p != mTypes.end(); ++p)
    {      
      of << *p << " ";
    }
    of << mPtrRef << " " << mVar;
  }

  typedef std::vector<std::string>::iterator TypesIterator;
  TypesIterator getTypesBegin() {return mTypes.begin();}
  TypesIterator getTypesEnd() {return mTypes.end();}

  virtual void writeHelpAnnotation(std::string* str) {
    *str += '[';
#if 0
    for (int i = 0; i < mTypes.size(); ++i) {
      *str += mTypes[i];
      *str += ' ';
    }
#endif
    *str += mVar;
    *str += ']';
  }

private:
  std::vector<std::string> mTypes;
  std::string mVar;
  std::string mPtrRef;
  //string mFormat;
}; // class MsgArg: public MsgElement


enum MsgSeverity {eStatus, eNote, eWarning, eAlert, eError, eFatal, eContinue,
                  eSuppress};
static MsgSeverity sNextSeverity;

class Msg
{
public:  // no CARBONMEM_OVERRIDES
  Msg(int id, const char* symbolicName)
  {
    mSeverity = sNextSeverity;
    mId = id;
    mName = symbolicName;
    //printf("Msg %d %s\n", id, symbolicName);
    mCurrentArg = NULL;
    mLocation = NULL;
  }

  ~Msg()
  {
    std::vector<MsgElement*>::iterator p;
    for (p = mElements.begin(); p != mElements.end(); ++p)
      delete *p;
  }

  void addString(const char* str)
  {
    mElements.push_back(new MsgString(str));
  }

  MsgArg* getArg()
  {
    if (mCurrentArg == NULL) {
      mCurrentArg = new MsgArg;
      mArgs.push_back(mCurrentArg);
      mElements.push_back(mCurrentArg);
    }
    return mCurrentArg;
  }

  void finalizeArg() {
    //mFormat += mCurrentArg->getFormat();
    //mCurrentArg->print();
    mCurrentArg = NULL;
  }

  void addHelp(const char* text)
  {
    //printf("  format %s\n", mFormat.c_str());
    mHelp += text;
    //printf("  help %s\n", text);
  }

  MsgArg* getLocation() const {return mLocation;}

  void putHasLocation(bool hasLoc)
  {
    if (hasLoc)
    {
      assert(mArgs.size() == 1); // this_assert_OK
      mLocation = mArgs[0];
      mElements.pop_back();     // location is processed separately
      mFormat += ": ";
    }
    else
    {
      assert(mArgs.empty());    // this_assert_OK
    }      
  }

  static void putNextSeverity(MsgSeverity sev) {
    sNextSeverity = sev;
  }

  void write(std::ofstream& cfile, std::ofstream& hfile);

  bool hasHelp() const {return ! mHelp.empty();}
  const char* getHelp() const {return mHelp.c_str();}
  int getId() const {return mId;}
  const char* getName() const {return mName.c_str();}
  MsgSeverity getSeverity() {return mSeverity;}

  typedef std::vector<MsgArg*>::iterator ArgIterator;
  ArgIterator getArgsBegin() {return mArgs.begin();}
  ArgIterator getArgsEnd() {return mArgs.end();}

  std::vector<MsgElement*> mElements;

private:
  MsgArg* mCurrentArg;
  MsgArg* mLocation;
  MsgSeverity mSeverity;
  int mId;
  std::string mName;
  std::string mFormat;
  std::string mHelp;
  std::vector<MsgArg*> mArgs;
};

class MsgCompiler
{
public: // no CARBONMEM_OVERRIDES
  MsgCompiler();
  ~MsgCompiler();
  void parse(const char* file);
  void append(Msg* msg) {mMsgList.push_back(msg);}
  void bumpErrorCount() {++mErrorCount;}
  int getErrorCount() {return mErrorCount;}
  int numMsgs() {return mMsgList.size();}
  Msg* getMsg(int i) {return mMsgList[i];}
  bool sortMsgs();
  void write(std::ofstream& cfile, std::ofstream& hfile);
  void writeHelp(std::ofstream& helpfile);
  void addInclude(const char* path);

private:
  int mErrorCount;
  Msg* mCurrentMsg;
  std::vector<Msg*> mMsgList;
  std::set<std::string> mIncludeFiles;
  //std::ostringstream mErrorMsg;
};

int yylex();
int yyerror(const char* text);

