// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/Graph.h"
#include "util/GraphDot.h"
#include "util/UtIOStream.h"

GraphDotWriter::GraphDotWriter(UtOStream& os)
  : mIsFileStream(false), mOut(&os)
{
}

GraphDotWriter::GraphDotWriter(const UtString& filename)
  : mIsFileStream(true), mOut(new UtOFStream(filename.c_str()))
{
}

GraphDotWriter::~GraphDotWriter()
{
  if (mIsFileStream)
    delete mOut;
}

GraphWalker::Command GraphDotWriter::visitTreeEdge(Graph* g, GraphNode* from, GraphEdge* edge)
{
  return printEdge(g, from, edge);
}

GraphWalker::Command GraphDotWriter::visitCrossEdge(Graph* g, GraphNode* from, GraphEdge* edge)
{
  return printEdge(g, from, edge);
}

GraphWalker::Command GraphDotWriter::visitBackEdge(Graph* g, GraphNode* from, GraphEdge* edge)
{
  return printEdge(g, from, edge);
}

void GraphDotWriter::writeName(Graph*, GraphNode* node)
{
  *mOut << '"' << node << '"';
}

void GraphDotWriter::writeLabel(Graph*, GraphNode*)
{
  // no special labels by default
  return;
}

void GraphDotWriter::writeLabel(Graph*, GraphEdge*)
{
  // no special labels by default
  return;
}

GraphWalker::Command GraphDotWriter::printEdge(Graph* g, GraphNode* from, GraphEdge* edge)
{
  writeName(g, from);
  *mOut << " -> ";
  writeName(g, g->endPointOf(edge));
  *mOut << " ";
  writeLabel(g, edge);
  *mOut << "\n";
  return GW_CONTINUE;
}

void GraphDotWriter::printNode(Graph* g, GraphNode* node)
{
  writeName(g, node);
  *mOut << " ";
  writeLabel(g, node);
  *mOut << "\n";
}

void GraphDotWriter::output(Graph* g, const char* name)
{
  *mOut << "digraph \"" << name << "\" {\n";

  *mOut << "  // node listing\n";  
  for (Iter<GraphNode*> n = g->nodes(); !n.atEnd(); ++n)
    printNode(g, *n);

  *mOut << "  // edge listing\n";  
  walk(g);

  *mOut << "}\n";
}
