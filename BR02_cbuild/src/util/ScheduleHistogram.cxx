// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/ScheduleHistogram.h"
#include <string.h>
#include <algorithm>

// We display a histogram of CPU time spent in calls to
// carbonSchedule.  We show a bar graph with five bars, one for each
// quintile.  Storing the time spent in each call would take lots of
// memory if there are lots of calls, so we'd rather store the number
// of entries in each bin rather than storing the actual times.  But
// we don't know the partitioning for the bins a priori, so we store
// the timings of the first few thousand calls.  At that point we
// determine the partioning of the bins, and from then on we just
// store the number of entries in each bin.  If there are longer calls
// after we do the partitioning, the partitions won't be ideal.  The
// largest partition will include everything greater than 80% of the
// largest time encoutered before we partitioned.  But that's good
// enough.

SPScheduleHistogram::SPScheduleHistogram()
  : mTimeArraySize(0), mMaxTime(0), mPartitioned(false)
{
  memset(mBins, 0, sizeof mBins);
}

void
SPScheduleHistogram::add(double time)
{
  if (time > mMaxTime)
    mMaxTime = time;

  if (!mPartitioned) {
    if (mTimeArraySize < cmMaxTimes)
      // Haven't filled array yet -- just store the time in the array
      mTimeArray[mTimeArraySize++] = time;
    else
      // Array is full -- time to partition
      partition();
  }
  if (mPartitioned)             // Possibly just became true
    // Increment the appropriate bin directly
    mBins[std::min(static_cast<unsigned>(time/mBinSpan), cmNumBins-1)]++;
}

// We've been storing the time for each call to carbonSchedule in an
// array.  That array is now large (or we're done), so determine the
// bins, count the number of entries that belong in each bin, and be
// done with the array.
void
SPScheduleHistogram::partition()
{
  if (mPartitioned)
    return;
  mBinSpan = mMaxTime / cmNumBins;
  //  printf("mBinSpan = %f, mMaxTime = %f, cmNumBins = %d\n",
  //         mBinSpan, mMaxTime, cmNumBins);
  // Put all stored times in their appropriate bins
  for (unsigned i = 0; i < mTimeArraySize; i++) {
    unsigned bin = static_cast<unsigned>(mTimeArray[i]/mBinSpan);
    mBins[std::min(bin, cmNumBins-1)]++;
  }
        
  // We could allocate mTimeArray on the heap, and free it here.

  mPartitioned = true;
}

void
SPScheduleHistogram::print(UtOStream& out)
{
  partition();
  unsigned fullestBinSize = 0;
  for (unsigned i = 0; i < cmNumBins; i++)
    if (mBins[i] > fullestBinSize)
      fullestBinSize = mBins[i];

  for (unsigned i = 0; i < cmNumBins; i++) {
    // Output e.g., "0.000 - 0.002"
    out << UtIO::Precision(3);
    if (i != cmNumBins - 1)
      out << i*mBinSpan << " - " << (i+1)*mBinSpan;
    else
      // Last bin will have entries greater than cmNumBins*mBinSpan if
      // the largest entry was found after partitioning, so we don't
      // specify the upper bound of the last bin.
      out << "      > " << i*mBinSpan;
    // Display the bar of the bar graph
    const unsigned cMaxHashes = 52;
    out << "s  "                // s for seconds
        << UtIO::left
        << UtIO::Width(cMaxHashes)
        << UtString(static_cast<size_t>(cMaxHashes * mBins[i]
                                        / static_cast<double>(fullestBinSize)
                                        + .5),
                    '#')
        << UtIO::right
        << UtIO::Width(9)
        << mBins[i]
        << UtIO::endl;
  }
}
