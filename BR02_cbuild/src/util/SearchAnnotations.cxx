// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CodeStream.h"
#include "util/UtIOStream.h"
#include "util/ArgProc.h"
#include "util/AnnotationStoreMsgContext.h"
#include "util/FileCollector.h"
#include "util/UtList.h"
#include <climits>

/* \file The annotation store reader utility opens annotation stores for
 *  reading. It can then either search for specific locations from the emitted
 *  code, or just dump the entire store to stdout.
 */
static const char *scLibName = "-libName";
static const char *scOutput = "-o";
static const char *scFaultContextSize = "-faultContextSize";

class Context;       // forward declaration

//! \class Reader
class Reader {
public:

  //! ctor for Reader
  Reader (MsgContext &msg_context, Context &reader_context, ArgProc &args) : 
    mMsgContext (msg_context), mReaderContext (reader_context), mErrorSink (msg_context), mArgs (args),
    mHdlStore (CodeAnnotationStore::cREADING, &mErrorSink),
    mImplStore (CodeAnnotationStore::cREADING, &mErrorSink),
    mFileRoot ("libdesign"), mOutput (&UtIO::cout ()), mOwnOutput (false)
  { processArgs (args); }

  virtual ~Reader ()
  {
    if (mOwnOutput) {
      mOutput->flush ();
      delete mOutput;
      mOwnOutput = false;
    }
  }

  // Enumeration of the different reader modes
  enum Mode {
    cNONE,                              //!< no mode specified
    cDUMP_DB,                           //!< just dump the database
    cFAULTS,                            //!< lookup compiler faults in the database
    cERROR                              //!< an erroneous mode specification
  };

  //! Instantiate a concrete reader class
  /*! \return the class instance if mode is a valid mode or NULL otherwise
   */
  static Reader *instantiate (const Mode mode, MsgContext &, Context &, ArgProc &);

  //! Run the reader
  virtual bool operator () () = 0;

  //! \return the fileroot gleaned from the -libName command line argument
  const char *getFileRoot () const { return mFileRoot; }

protected:

  // When CodeAnnotationStore instances are opened for reading an error sink is
  // required. This error sink outputs the error messages as ParseErrors to the
  // AnnotationStore-MsgContext
  struct ErrorSink : public CodeAnnotationStore::ErrorSink {
    ErrorSink (MsgContext &msg_context) : mMsgContext (msg_context) {}
    virtual void operator () (const char *filename, const UInt32 lineno, const char *errmsg)
    { mMsgContext.ParseError (filename, lineno, errmsg); }
  private:
    MsgContext &mMsgContext;
  };

  MsgContext &mMsgContext;              //!< whining stream
  Context &mReaderContext;              //!< context for this run
  ErrorSink mErrorSink;                 //!< error sink for the annotation stores
  ArgProc &mArgs;                       //!< argument vector
  CodeAnnotationStore mHdlStore;        //!< hdl-source annotation store
  CodeAnnotationStore mImplStore;       //!< implementation source annotation store

  //! Open both the HDL and the implementation annotation stores
  bool openStores ()
  {
    UtString errmsg;
    if (!mHdlStore.open (mFileRoot, "hdl", 0, &errmsg)) {
      mMsgContext.OpenStoreError ("HDL", errmsg.c_str ());
      return false;
    } else if (!mImplStore.open (mFileRoot, "impl", 0, &errmsg)) {
      mMsgContext.OpenStoreError ("implementation", errmsg.c_str ());
      return false;
    } else {
      return true;
    }
  }

  const char *stripName (const char *name) const
  {
    // for implementation annotation strip out the directory part... we will
    // always be able to suss out which file in src/codegen from the filename
    // part
    unsigned int slash = 0;
    unsigned int n = 0;
    for (n = 0; name [n] != '\0'; n++) {
      if (name [n] == '/') {
        slash = n;
      }
    }
    return (slash > 0 && slash < n - 1) ? name + slash + 1 : name;
  }

  //! \return the reader output stream
  UtOStream &output () const { return *mOutput; }

private:

  const char *mFileRoot;                //!< file root from -libName argument
  UtOStream *mOutput;                   //!< the output stream
  bool mOwnOutput;                      //!< set when this owns the stream

  //! Process the generic Reader arguments
  void processArgs (ArgProc &args)
  {
    if (args.getStrValue (scLibName, &mFileRoot) != ArgProc::eKnown) {
      mMsgContext.CmdLineError2 ("%s: unable to infer library name from command line", scLibName);
    }
    const char *filename;
    UtOStream *output;
    if (args.getStrValue (scOutput, &filename) != ArgProc::eKnown) {
      // no -o options specified
    } else if ((output = new UtOFStream (filename)) == NULL) {
      // allocation of the object failed... this is bad
      mMsgContext.CannotOpenOutputFile (filename, "allocation failed");
    } else if (!output->is_open ()) {
      // unable to open the output file
      UtString oserror;
      OSGetLastErrmsg (&oserror);
      mMsgContext.CannotOpenOutputFile (filename, oserror.c_str ());
      delete output;
    } else {
      // opened an output file successfully
      mOutput = output;
      mOwnOutput = true;
    }
  }
};

//! \class Context
/*! Context wraps all the processing for the annotation store reader. It sets
 *   up argument process and then runs a Reader instance.
 */

class Context {
public:

  //! ctor for class Context
  Context (MsgContext &msg_context) : 
    mMsgContext (msg_context), mCollector (msg_context)
  { configureArgs (); }

  //! Run the reader
  /*! \return the 
   */
  bool operator () (int *argc, char **argv)
  {
    if (!processArgs (argc, argv)) {
      return false;                     // failure
    }
    Reader *reader = Reader::instantiate (mCollector.getMode (), mMsgContext, *this, mArgProc);
    if (reader == NULL) {
      return false;                     // no reader was created so return failure
    } else if ((*reader) ()) {
      delete reader;
      return true;                      // success
    } else {
      delete reader;
      return false;                     // failure
    }
  }

  //! Simple representation of locations read from the command line
  struct Location {
    Location (UtString filename, const UInt32 lineno) : mFilename (filename), mLineno (lineno) {}
    Location () : mFilename (), mLineno (0) {}
    UtString mFilename;
    UInt32 mLineno;
    typedef UtList <Location> List;
  };


  //! Access the locations specified on the command line
  Location::List::const_iterator beginLocation () const { return mCollector.beginLocation (); }
  Location::List::const_iterator endLocation () const { return mCollector.endLocation (); }
  UInt32 numLocations () const { return mCollector.numLocations (); }

private:

  MsgContext &mMsgContext;              //!< whining stream
  ArgProc mArgProc;                     //!< argument vector handler

  //! Configure the argument processor
  void configureArgs ()
  {
    mArgProc.setDescription ("Annotation Store Utility", "readannotationstore",
      "Utility to read annotation store information.");
    mArgProc.addString (scLibName, "Change the default library name", "libdesign", false, false, 1);
    mArgProc.addString (scOutput, "Send the output to a file instead of stdout", NULL, true, false, 1);
    mArgProc.addBool ("-h", "Print the documentation for annotation store reader options and exit.", 
      false, 1);
    mArgProc.addSynonym ("-h", "-help", false);
    mArgProc.addInt (scFaultContextSize, "Number of locations before the fault to print",
      5, false, false, 1);
    mArgProc.addSynonym (scFaultContextSize, "-N", false);
  }

  //! Process the command line arguments
  bool processArgs (int *argc, char **argv)
  {
    mArgProc.accessHiddenOptions ();
    mArgProc.allowUsageAll ();
    // run the parser
    UtString errMsg;
    int numOptions;
    if (mArgProc.parseCommandLine (argc, argv, &numOptions, &errMsg, &mCollector) != ArgProc::eParsed) {
      mMsgContext.CmdLineError (errMsg.c_str ());
      return false;
    }
    UtString errmsg;
    if (!mCollector.isOK (&errMsg)) {
      mMsgContext.CmdLineError (errMsg.c_str ());
      return false;
    }
    // grab arguments
    if (mArgProc.getBoolValue ("-h")) {
      printUsage ();
      exit(0);
    }
    return true;
  }

  //! Print annotation store reader usage instructions
  void printUsage ()
  {
    UtString usage;
    mArgProc.getUsageVerbose(&usage, false);
    UtIO::cout() << usage << UtIO::endl;
  }

  //! \class Context::Collector
  class Collector : public FileCollector {
  public:

    Collector (MsgContext &msg_context) : 
      FileCollector (), mMsgContext (msg_context), mMode (Reader::cNONE), mExpectingLineno (false),
      mLocations ()
    {}

    virtual bool scanArgument (const char *, const char *)
    { 
      return false;
    }

    bool isOK (UtString *errmsg)
    {
      if (mExpectingLineno) {
        *errmsg << "expected a line number after filename: " << mLastFilename.c_str ();
        return false;
      } else {
        return true;
      }
    }

    Context::Location::List::const_iterator beginLocation () const { return mLocations.begin (); }
    Context::Location::List::const_iterator endLocation () const { return mLocations.end (); }
    UInt32 numLocations () const { return mLocations.size (); }

    virtual void addArgument (const char *arg)
    {
      Lexer scan (mMsgContext);
      Lexer::Tokens token = scan (arg);
      if (mExpectingLineno && token != Lexer::cINTEGER) {
        mMsgContext.CmdLineError2 ("expected a line number after filename", mLastFilename.c_str ());
        mExpectingLineno = false;
      } else if (!mExpectingLineno && token == Lexer::cINTEGER) {
        mMsgContext.CmdLineError ("line number specified without a filename");
      }
#if 1
#define RETURNING(_token_, _extra_)
#else
#define RETURNING(_token_, _extra_) UtIO::cout () << "arg scanner returned " << #_token_ << _extra_ << "\n";
#endif
      switch (token) {
      case Lexer::cFAULTS:
        if (mMode != Reader::cNONE) {
          mMsgContext.MultipleModeSpecificationError (arg);
        } else {
          mMode = Reader::cFAULTS;
        }
        RETURNING (cFAULT, "");
        break;
      case Lexer::cDUMP:
        if (mMode != Reader::cNONE) {
          mMsgContext.MultipleModeSpecificationError (arg);
        } else {
          mMode = Reader::cDUMP_DB;
        }
        RETURNING (cFAULT, "");
        break;
      case Lexer::cFILENAME:
        if (mMode == Reader::cNONE) {
          // default it to faults
          mMode = Reader::cFAULTS;
        } else if (mMode == Reader::cFAULTS) {
          // already set to faults
        } else {
          mMsgContext.CmdLineError ("locations are not expected in this mode");
        }
        mLastFilename = scan.getFilename ();
        mExpectingLineno = true;      // need the line number on the next call
        RETURNING (cFILENAME, ": " << scan.getFilename ());
        break;
      case Lexer::cINTEGER:
        if (mMode == Reader::cNONE) {
          // default it to faults
          mMode = Reader::cFAULTS;
        } else if (mMode == Reader::cFAULTS) {
          // already set to faults
        } else {
          mMsgContext.CmdLineError ("locations are not expected in this mode");
        }
        if (mExpectingLineno) {
          mLocations.push_back (Location (mLastFilename, scan.getInteger ()));
        }
        mExpectingLineno = false;
        RETURNING (cINTEGER, ": " << scan.getInteger ());
        break;
      case Lexer::cLOCATION:
        if (mMode == Reader::cNONE) {
          // default it to faults
          mMode = Reader::cFAULTS;
        } else if (mMode == Reader::cFAULTS) {
          // already set to faults
        } else {
          mMsgContext.CmdLineError ("locations are not expected in this mode");
        }
        mLocations.push_back (Location (scan.getFilename (), scan.getInteger ()));
        RETURNING (cLOCATION, ": " << scan.getFilename () << ": " << scan.getInteger ());
        break;
      case Lexer::cERROR:
        RETURNING (cERROR, "");
        mMode = Reader::cERROR;
        break;
      }
#undef RETURNING
    }

    //! \return the mode inferred from arguments
    Reader::Mode getMode () const { return mMode; }

  private:

    MsgContext &mMsgContext;            //!< for whining
    Reader::Mode mMode;                 //!< inferred mode
    bool mExpectingLineno;              //!< set when a lineno is expected 
    UtString mLastFilename;             //!< the last filename seen
    Location::List mLocations;            //!< list of locations discovered

    class Lexer {
    public:

      enum Tokens {
        cFAULTS,
        cDUMP,
        cFILENAME,
        cINTEGER,
        cLOCATION,
        cERROR
      };

      Lexer (MsgContext &msg_context) : mMsgContext (msg_context) {}

      const char *getFilename () const { return mFilename; }
      UInt32 getInteger () const { return mInteger; }

      Tokens operator () (const char *s)
      {
        int state = 0;
        unsigned int n = 0;             // index into s
        unsigned int n0 = 0;            // index of first character in an integer
        unsigned int m = 0;             // index into mFilename
        while (1) {
          switch (state) {
          case 0:
            if (s [n] == '\0') {
              mMsgContext.CmdLineError ("empty argument");
              return cERROR;              // no token found
            } else if (isalpha (s [n]) || s [n] == '/' || s [n] == '.' || s [n] == '_') {
              state = 10;                 // looking for filename (or keyword)
            } else if (isdigit (s [n])) {
              n0 = n;
              mInteger = 0;
              state = 20;                 // looking for integer
            } else if (isspace (s [n])) {
              n++;                        // consume whitespace
            } else {
              mMsgContext.CmdLineError2 (s, "unhandled argument");
              return cERROR;
            }
            break;
          case 10:
            // scanning filename or filename:location
            if (s [n] == ':') {
              n++;                        // consume the colon
              state = 12;                 // it is a location
              mFilename [m] = '\0';      // terminate the filename part
            } else if (!isalnum (s [n]) && s [n] != '.' && s [n] != '_' && s [n] != '/') {
              mFilename [m] = '\0';
              state = 11;
            } else if (m >= sizeof (mFilename) - 1) {
              mMsgContext.CmdLineError2 ("filename is too long", s);
              return cERROR;
            } else {
              mFilename [m++] = s [n++];  // acrete the character into the filename
            }
            break;
          case 11:
            // completed scanning the filename, but it may be a keyword
            if (!strcmp (mFilename, "fault") || !strcmp (mFilename, "faults")) {
              return cFAULTS;
            } else if (!strcmp (mFilename, "dump")) {
              return cDUMP;
            } else {
              return cFILENAME;
            }
            break;
          case 12:
            // consume whitespace after the ':' of a filename:lineno
            if (isdigit (s [n])) {
              mInteger = 0;
              state = 20;                 // go for the digit
            } else if (isspace (s [n])) {
              n++;                        // consume the white space
            } else {
              mMsgContext.CmdLineError2 (s, "expected an integer after ':'");
              return cERROR;
            }
            break;            
          case 20:
            // scanning an integer, or the integer part of filename:lineno
            if (isdigit (s [n])) {
              mInteger = 10 * mInteger + (UInt32) s [n++] - (UInt32) '0';
            } else {
              state = 21;                 // end of the integer
            }
            break;
          case 21:
            // reached the end of the digits
            if (s [n] == '\0') {
              // end of the string
              return m > 0 ? cLOCATION : cINTEGER;
            } else if (isspace (s [n])) {
              n++;                      // consume trailing space
            } else if (m > 0) {
              // It is not a digit and it is not space, but we are already in
              // the lineno part of filename:lineno, so it's an error.
              mMsgContext.CmdLineError2 (s, "invalid location specification");
              return cERROR;
            } else if (n - n0 >= sizeof (mFilename)) {
              mMsgContext.CmdLineError2 ("filename is too long", s);
              return cERROR;
            } else {
              // It is a filename that starts with digits
              memcpy (mFilename, s + n0, n - n0);
              m = n - n0;
              state = 10;
            }
            break;
          default:
            ASSERT (0);
            break;
          }
        }
      }
    private:
      MsgContext &mMsgContext;
      char mFilename [PATH_MAX];
      UInt32 mInteger;
    };

  };

  Collector mCollector;                 //!< for gather arguments not associated with options

};

/***************
 * entry point *
 ***************/

int main (int argc, char **argv)
{
  MsgStreamIO msg_stream (stdout, true);
  MsgContext msg_context;
  msg_context.addReportStream (&msg_stream);
  Context context (msg_context);
  return context (&argc, argv) ? 0 : 1;
}

/****************
 * DumpDbTokens *
 ****************/

//! \class DumpDbTokens
/*! This reader mode just calls the store methods for printing the store
 *  configurations and dumping the database contents.
 */
class DumpDbTokens : public Reader {
public:

  DumpDbTokens (MsgContext &msg_context, Context &reader_context, ArgProc &args) :
    Reader (msg_context, reader_context, args)
  {}

  virtual ~DumpDbTokens () {}

  virtual bool operator () ()
  {
    UtString errmsg;
    if (!openStores ()) {
      return false;
    }
    mHdlStore.print (output ());
    mHdlStore.dumpDB (output ());
    mImplStore.print (output ());
    mImplStore.dumpDB (output ());
    return true;
  }

};

/**************
 * FindFaults *
 **************/

class FindFaults : public Reader {
public:
  FindFaults (MsgContext &msg_context, Context &reader_context, ArgProc &args) :
    Reader (msg_context, reader_context, args), mContextSize (1)
  {}

  virtual ~FindFaults () {}

  virtual bool operator () ()
  {
    // parse the fault search specific options
    if (!parseCommandLine ()) {
      return false;
    }
    // for now, just allowing a single location to be specified
    if (mReaderContext.numLocations () == 0) {
      mMsgContext.CmdLineError ("no fault location is specified on the command line");
      return false;
    } else if (mReaderContext.numLocations () > 1) {
      mMsgContext.CmdLineError ("only a single fault location may current be specified on the command line");
      return false;
    }
    // grab that single location
    ASSERT (mReaderContext.beginLocation () != mReaderContext.endLocation ());
    Context::Location fault_location = *mReaderContext.beginLocation ();
    // open the store
    if (!openStores ()) {
      return false;
    }
    // now search both the stores
    bool retval = true;
    if (!searchStore (mHdlStore, fault_location, false)) {
      retval = false;
    }
    if (!searchStore (mImplStore, fault_location, true)) {
      retval = false;
    }
    return retval;
  }

private:

  UInt32 mContextSize;                  //!< number of locations to print for each fault

  enum {
    cWINDOW_SIZE = 100                  //!< size of the location window sucked from the file
  };

  //! Circular buffer for reading entries from the annotation database.
  CodeAnnotationStore::DatabaseEntry mWindow [cWINDOW_SIZE];
  UInt32 mNext;                         //!< next location in the window to fill
  UInt32 mLength;                       //!< number of valid entries (mLength <= cWINDOW_SIZE)

  //! Grab the FindFaults-specific command line options
  bool parseCommandLine ()
  {
    SInt32 value;
    if (mArgs.getIntFirst (scFaultContextSize, &value) != ArgProc::eKnown) {
      mMsgContext.CmdLineError2 (scFaultContextSize, "no value found; this should be defaulted");
      return false;
    } else if (value <= 0) {
      mMsgContext.CmdLineError2 (scFaultContextSize, "value must be positive");
      return false;
    } else if (value > cWINDOW_SIZE) {
      mMsgContext.FaultContextTooLarge (scFaultContextSize, value, cWINDOW_SIZE);
      return false;
    } else {
      mContextSize = value;
    }
    return true;
  }
    
  //! Search the store for HDL and implementation annotation "near" to a location
  /*! \param store The store to search
   *  \param location The candidate location
   *  \param short_name if true only the filename part of the domain is printed (this
   *  is usefull for implementation locations, just the emitFoo.cxx part gets shown.
   *
   *  \note The list of annotations preceding the candidate location is
   *  output. More than likely, especially with HDL annotations there will not
   *  be an exact match to the generated code location. The number of
   *  annotations displayed is set by the -faultContext (aliased to -N) command
   *  line argument.
   */
  bool searchStore (CodeAnnotationStore &store, const Context::Location location, bool short_names)
  {
    // find the file index corresponding to the filename
    UInt32 file_index;
    if (!store.findFileIndex (location.mFilename, &file_index)) {
      mMsgContext.FileNameNotInStoreIndex (location.mFilename.c_str (), store.getName ().c_str ());
      return false;
    }
    // Construct a map from file index to filename
    UtHashMap <UInt32, UtString> filemap;
    store.reverseFileMap (&filemap);
    // The mWindow array is used as a circular buffer that store a window of
    // the last cWINDOW_SIZE entries for the appropriate file from the
    // annotation database.
    CodeAnnotationStore::DatabaseEntry entry;
    mNext = 0;
    mLength = 0;
    UInt32 n_entries = 0;
    while (store.read (&entry)
      && !(entry.mRangeFileIndex == file_index && entry.mRangeLineno > location.mLineno)) {
      if (entry.mRangeFileIndex != file_index) {
        continue;                       // this is not the file we are interested in
      }
      // Update the circular buffer
      mWindow [mNext] = entry;
      mNext = (mNext + 1) % cWINDOW_SIZE;
      if (mLength < cWINDOW_SIZE) {
        mLength++;
      }
      n_entries++;
    }
    // Output the requested number of locations before the fault
    output () << "Locations in the " << store.getName () << " source near the fault at "
              << location.mFilename << ": " << location.mLineno << "\n";
    UInt32 m = mLength > mContextSize ? mContextSize : mLength;
                                        // number of entries left to print
    UInt32 n;                           // index into the circular buffer
    // Note that we cannot just use the % operator here because it had the same
    // sign as the quotient.
    if (mNext < m) {
      n = cWINDOW_SIZE - (m - mNext);
    } else {
      n = mNext - m;
    }
    while (m > 0) {
      CodeAnnotationStore::DatabaseEntry &entry = mWindow [n];
      ASSERT (mWindow [n].mRangeFileIndex == file_index);
      // find the domain file name
      UtHashMap <UInt32, UtString>::const_iterator domain_found;
      if ((domain_found = filemap.find (mWindow [n].mDomainFileIndex)) == filemap.end ()) {
        // That's odd, the index is not in the reversed map. The annotation
        // configuration file must be corrupted.
        mMsgContext.FileIndexNotInStoreIndex (mWindow [n].mDomainFileIndex, store.getName ().c_str ());
        break;
      }
      const char *name_to_print;
      if (short_names) {
        // Just print the filename part. This should be used for implementation
        // annotations. Rather than printing
        // /blah/blah/blah/src/codegen/emitFoo.cxx stripping gives us just
        // emitFoo.cxx.
        name_to_print = stripName (domain_found->second.c_str ());
      } else {
        // Print the entire path name. This should be used for HDL
        // annotations. They can be scattered all about the file system so the
        // entire path is needed.
        name_to_print = domain_found->second.c_str ();
      }
      const char *leaders = (m == 1) ? "*** " : "    ";
      if (!write (store, location, entry, filemap, short_names, leaders)) {
        break;
      }
      n = (n + 1) % cWINDOW_SIZE;
      m--;
    }
    // output locations after the fault
    if (n_entries > 0) {
      write (store, location, entry, filemap, short_names, "    ");
      m++;
    }
    while (m < mContextSize && store.read (&entry)) {
      if (!write (store, location, entry, filemap, short_names, "    ")) {
        break;
      }
      m++;
    }
    return true;
  }

  bool write (CodeAnnotationStore &store, const Context::Location location, 
    CodeAnnotationStore::DatabaseEntry &entry, 
    UtHashMap <UInt32, UtString> &filemap, bool short_names, const char *leaders) const
  {
    // find the domain file name
    UtHashMap <UInt32, UtString>::const_iterator domain_found;
    if ((domain_found = filemap.find (entry.mDomainFileIndex)) == filemap.end ()) {
      // That's odd, the index is not in the reversed map. The annotation
      // configuration file must be corrupted.
      mMsgContext.FileIndexNotInStoreIndex (entry.mDomainFileIndex, store.getName ().c_str ());
      return false;
    }
    const char *name_to_print;
    if (short_names) {
      // Just print the filename part. This should be used for implementation
      // annotations. Rather than printing
      // /blah/blah/blah/src/codegen/emitFoo.cxx stripping gives us just
      // emitFoo.cxx.
      name_to_print = stripName (domain_found->second.c_str ());
    } else {
      // Print the entire path name. This should be used for HDL
      // annotations. They can be scattered all about the file system so the
      // entire path is needed.
      name_to_print = domain_found->second.c_str ();
    }
    output () << leaders << name_to_print << " line " << entry.mDomainLineno << " ("
      << location.mFilename << ": " << entry.mRangeLineno
      << ": " << entry.mRangeColumn << ")\n";
    return true;
  }
};

// Now all the reader flavours are declared, define the instantiate method.
/*static*/ Reader *Reader::instantiate (const Mode mode, MsgContext &msg_context,
  Context &reader_context, ArgProc &arg_proc)
{
  switch (mode) {
  case cNONE:
    msg_context.NoModeSpecified ();
    return NULL;
    break;
  case cERROR:
    return NULL;
    break;
  case cDUMP_DB:
    return new DumpDbTokens (msg_context, reader_context, arg_proc);
    break;
  case cFAULTS:
    return new FindFaults (msg_context, reader_context, arg_proc);
    break;
  default:
    ASSERT (0);
    return NULL;
    break;
  }
}
