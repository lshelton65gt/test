// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdarg.h>
#include "nucleus/Nucleus.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUAssign.h"
#include "codegen/codegen.h"
#include "cg/LIR.h"

#if CG_IBURG_ENABLE
#include "cg/LIRMatch.h"
#endif

//! Map an LIR operator into a string
/*static*/ const char *LIR::cvt (enum Op x)
{
  switch (x) {
#define LIROP(_n_, _arity_, _name_) case e##_name_: return "e" #_name_;
#include "cg/LIR.def"
  default: return "LIR::Op###error###";
  }
}

LIR::LIR (const SourceLocator loc, const Op irop, LIR *left, LIR *right) :
  mLoc (loc), mOp (irop), mLeft (left), mRight (right), mActualBinding (0xdeadbeef),
  mLabel (cNOT_A_STATE), mState (NULL), mBitNet (NULL), mVectorNet (NULL),
  mBits (NULL)
{
}

LIR::LIR (const SourceLocator loc, const Op irop, const DynBitVector *bits) :
  mLoc (loc), mOp (irop), mLeft (NULL), mRight (NULL), mActualBinding (0xdeadbeef),
  mLabel (cNOT_A_STATE), mState (NULL), mBitNet (NULL), mVectorNet (NULL),
  mBits (bits)
{
}

LIR::LIR (const SourceLocator loc, const Op irop, NUNet *net) :
  mLoc (loc), mOp (irop), mLeft (NULL), mRight (NULL), mActualBinding (0xdeadbeef),
  mLabel (cNOT_A_STATE), mState (NULL), mBitNet (NULL), mVectorNet (NULL),
  mBits (NULL)
{
  switch (irop) {
  case ebitnet:
    mBitNet = dynamic_cast <NUBitNet *> (net);
    // Check that this really is a bitnet. If this assertion fails, then the
    // tree transformation rule from the low-level code generator definition is
    // bogus.
    NU_ASSERT (net->isBitNet () && mBitNet != NULL, net);
    break;
  case evectornet8:
  case evectornet16:
  case evectornet32:
  case evectornet64:
    mVectorNet = dynamic_cast <NUVectorNet *> (net);
    // Check that this really is a vector net. If this assertion fails, then
    // the tree transform rule is bogus.
    NU_ASSERT (net->isVectorNet () && mVectorNet != NULL, net);
    // Check that this is a vector net that is shorter than the biggest word
    // size. If this transformation fails, then the transform rule should have
    // created a dynvectornet for this node.
    NU_ASSERT (mVectorNet->getBitSize () < LLONG_BIT, mVectorNet);
    break;
  case ebigvectornet:
    mVectorNet = dynamic_cast <NUVectorNet *> (net);
    // Check that this really is a vector net. If this assertion fails, then
    // the tree transform rule is bogus.
    NU_ASSERT (net->isVectorNet () && mVectorNet != NULL, net);
    // Check that this is a vector net that is shorter than the biggest word
    // size. If this transformation fails, then the transform rule should have
    // created a vectornet for this node.
    NU_ASSERT (mVectorNet->getBitSize () >= LLONG_BIT, mVectorNet);
    break;
  default:
    // The intermediate representation is corrupted.
    NU_ASSERT (false, net);
    break;
  }
}

LIR::~LIR ()
{
#if CG_IBURG_ENABLE
  if (mState == NULL) {
    // no state vector to delete
  } else if (mState->left == NULL && mState->right == NULL) {
    // The state vector for leaf nodes is statically allocated in the
    // iburg-generated tree pattern matcher.
  } else {
    delete mState;
  }
#endif
  if (mLeft != NULL) {
    delete mLeft;
  }
  if (mRight != NULL) {
    delete mRight;
  }
}

//! Write LIR to a stream
void LIR::print (UtOStream &out) const
{
  IndentedStream <UtOStream> sink (out);
  print (sink);
}

//! Write LIR to standard output
void LIR::pr () const
{
  print (UtIO::cout ());
}

#if CG_IBURG_ENABLE
extern char *LIRMatch_string[];
extern short *LIRMatch_nts[];
extern int LIRMatch_rule (struct LIRMatch_state *state, int goalnt);
extern LIR *LIRMatch_kids (LIR *p, int eruleno, LIR *kids []);

//! Dump the cover of a LIR
void LIR::printCover (IndentedStream <UtOStream> &sink, const UInt32 goalnt) const
{
  if (mState == NULL) {
    sink << "### no state ###\n";
    return;
  }
  UInt32 eruleno = LIRMatch_rule (mState, goalnt);
  short *nts = LIRMatch_nts [eruleno];
  sink << cvt (mOp);
  if (mBitNet != NULL) {
    sink << "\t(" << mBitNet->getName ()->str () << ")";
  } else if (mVectorNet != NULL) {
    sink << "\t(" << mVectorNet->getName ()->str () << ")";
  }
  sink << ":\t[" << LIRMatch_string [eruleno] << "]\t";
  LIR *kids [2];
  LIRMatch_kids (const_cast <LIR *> (this), eruleno, kids);
  int n_kids = 0;
  for (int i = 0; nts [i]; i++) {
    if (n_kids++ == 0) {
      sink << "(\n";
    }
    kids [i]->printCover (sink, nts [i]);
  }
  if (n_kids > 0) {
    sink << ")\n";
  } else {
    sink << "\n";
  }
}
#endif

//! Helper method for LIR output
void LIR::print (IndentedStream <UtOStream> &sink) const
{
  sink << cvt (mOp);
  if (mBitNet != NULL) {
    sink << "\t(" << mBitNet->getName ()->str () << ")";
  } else if (mVectorNet != NULL) {
    sink << "\t(" << mVectorNet->getName ()->str () << ")";
  } else if (mLeft == NULL && mRight == NULL) {
    // no operands
  } else if (mLeft != NULL && mRight == NULL) {
    // one operand
    sink << "\t(";
    mLeft->print (sink);
    sink << ")";
  } else if (mLeft != NULL && mRight != NULL) {
    // two operands
    sink << "\t(";
    mLeft->print (sink);
    sink << ",\t";
    mRight->print (sink);
    sink << ")";
  } else {
    INFO_ASSERT (false, "corrupt LIR tree");
  }
}

class DumpLIRCallback : public NUDesignCallback {
public:
  DumpLIRCallback (UtOStream &out) : NUDesignCallback (), mOut (out) {}
  virtual Status operator () (Phase, NUBase *) { return eNormal; }
  virtual Status operator () (Phase phase, NUAssign *node)
  {
    if (phase == ePre) {
      node->print (0, 0);
      LIR *lir = node->generateLIR ();
      if (lir == NULL) {
        UtIO::cout () << "Unable to construct LIR for statement: ";
        node->print (1, 2);
        return eNormal;
      }
      UtString buffer;
      node->compose (&buffer, NULL, 2);
      mOut << "Statement:\n";
      mOut << buffer;
      {
        mOut << "LIR tree:\n";
        IndentedStream <UtOStream> sink (mOut, 2);
        lir->print (sink);
      }
#if CG_IBURG_ENABLE
      if (!lir->match ()) {
        UtIO::cout () << "iburg match failed\n";
      } else {
        IndentedStream <UtOStream> sink (mOut, 2);
        mOut << "Cover of LIR tree:\n";
        lir->printCover (sink, 1);
        mOut << "\n";
        mOut.flush ();
        mOut << "Iburg-generated code:\n";
        SimpleCodeStream code (&mOut, CodeStream::cDO_NOT_CLOSE);
        code++;
        lir->emit (code, 1);
        mOut << "\n";
      }
#endif
#if CG_BURS_ENABLE
      // Now try the BURS matcher
      if (!lir->label ()) {
        UtIO::cout () << "BURS labeling failed\n";
      } else {
        mOut << "BURS-generated code:\n";
        SimpleCodeStream code (&mOut, CodeStream::cDO_NOT_CLOSE);
        code++;
        lir->emitCode (code, LIR::getGoal ());
        mOut << "\n";
      }
#endif
      delete lir;
    }
    return eNormal;
  }

private:
  UtOStream &mOut;

};

//! Write LIR for a design into a file
void LIR::dumpLIR (NUDesign *design, UtString filename)
{
  UtOFStream out (filename.c_str ());
  DumpLIRCallback callback (out);
  NUDesignWalker dump (callback, false, false);
  dump.design (design);
}

#if CG_IBURG_ENABLE
bool LIR::match ()
{
  if (LIRMatch_label (this) == NULL) {
    return false;
  } else {
    return true;
  }
}
#endif

//! Panic method for the BURS matcher.
/*static*/ void LIR::Panic (const char *fmt, ...)
{
  char buffer [256];
  va_list ap;
  va_start (ap, fmt);
  vsnprintf (buffer, sizeof (buffer), fmt, ap);
  va_end (ap);
  INFO_ASSERT (false, buffer);
}
