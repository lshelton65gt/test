// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdarg.h>
#include "compiler_driver/CarbonContext.h"
#include "util/ArgProc.h"
#include "util/IndentedStream.h"
#include "util/UtIOStream.h"
#include "util/OSWrapper.h"
#include "codegen/codegen.h"
#include "cg/CG.h"
#include "cg/LIR.h"

// command line options
const char *scVerboseCG = NULL;
const char *scCG = NULL;

//! CG ctor
CG::CG (CarbonContext *context, MsgContext *msg_context, const UInt32 options) :
  mFlags (options), mContext (context), mMsgContext (msg_context)
{}

/*virtual*/ CG::~CG () {}

//! Configure the CG-specific options
/*static*/ void CG::addCommandLineArgs (ArgProc *args)
{
  scVerboseCG = CRYPT ("-verboseCG");
  scCG = CRYPT ("-cg");
  args->addBool (scVerboseCG, CRYPT ("Dump analysis from the new code generator"), 
    false, CarbonContext::ePassCarbon);
  args->addString (scCG, CRYPT ("Diagnostic switches for the new code generator (experimental use only)"),
    NULL, true, true, CarbonContext::ePassCarbon);
}

//! Generate the C/C++ model
bool CG::generate (NUDesign *design)
{
  configure (*mContext->getArgs ());
  // setup the build directory
  if (!configureModelDirectory ()) {
    return false;
  }
  // dump LIR into a file
  UtString lirfilename;
  makeModelFilename ("lir.dump", &lirfilename);
  LIR::dumpLIR (design, lirfilename);
  return true;
}

//! Set configuration options from the command line
void CG::configure (const ArgProc &args)
{
  // look for -verboseCG
  if (args.getBoolValue (scVerboseCG)) {
    mFlags |= eVERBOSE;
  }
  // look for -cg argument
  ArgProc::StrIter it;
  if (args.getStrIter (scCG, &it) == ArgProc::eKnown) {
    while (!it.atEnd ()) {
      configure (*it);
      ++it;
    }
  }
}

//! Set configuration options form a string
void CG::configure (const char *s)
{
  // process the string with a simple state machine
  char ident [64];
  UInt32 n = 0;
  UInt32 m = 0;
  UInt32 state = 0;
  while (state < 100) {
    switch (state) {
    case 0:
      // initial state - scanning for the start of an identifier
      if (s [n] == '\0') {
        // the end of the string
        state = 100;                // final state
      } else if (isalnum (s [n])) {
        // the start of an identifier
        m = 0;
        state = 10;                 // start scanning an identifier
      } else if (isspace (s [n]) || s [n] == ',' || s [n] == '\t') {
        // whitespace or a separator
        n++;                        // skip space and separators
      } else {
        // bogus text
        mMsgContext->CGCmdLineError (scCG, s, "badly formed option");
        state = 100;                // final state
      }
      break;
    case 10:
      // accumulating an identifier into ident
      if (m >= sizeof (ident) - 1) {
        // run off the end of the buffer
        ident [sizeof (ident) - 1] = '\0';
        mMsgContext->CGCmdLineError (scCG, ident, "argument is too long");
        state = 100;
      } else if (isalnum (s [n])) {
        // accumulate another character
        ident [m++] = s [n++];
      } else {
        // hit the end of the identifier
        ident [m] = '\0';               // null terminate the identifier
        state++;
      }
      break;
    case 11:
      // recognise configuration options from the identifier
      if (!strcasecmp (ident, "verbose")) {
        mFlags |= eVERBOSE;
      } else if (!strcasecmp (ident, "dumplir")) {
        mFlags |= eDUMPLIR;
      } else {
        mMsgContext->CGCmdLineError (scCG, ident, "unrecognised codegen option");
      }
      state = 0;
      break;
    default:
      INFO_ASSERT (false, "state machine jammed");
      break;
    }
  }
}

//! Configure the model directory
bool CG::configureModelDirectory ()
{
  if (!configureModelPaths (*mContext->getArgs ())) {
    return false;
  }

  // Delete an existing build directory
  int status;
  UtString statMsg (mModelDirectory);
  UtString deleteMsg;
  if ((status = OSStatFile (mModelDirectory.c_str (), "edrw", &statMsg)) == 0) {
    // directory does not exist... that's ok, we're about to create it
  } else if (status != 1) {
    // something... and unrecoverable... happened
    mMsgContext->CGFail (statMsg.c_str ());
    exit (1);
  } else if ((status = OSDeleteRecursive (mModelDirectory.c_str (), &deleteMsg)) != 0) {
    // something bad happened when deleting the directory
    mMsgContext->CGFail (deleteMsg.c_str ());
    exit (1);
  } else {
    // successfully deleted the existing directory
  }
  
  // Create a new directory
  UtString createMsg;
  if (OSMkdir (mModelDirectory.c_str (), 0770, &createMsg) != 0) {
    mMsgContext->CGFail (createMsg.c_str ());
    exit (1);
  }    

  return true;
}

//! Infer all the various paths
bool CG::configureModelPaths (const ArgProc &args)
{

  // Remember our current working directory
  UtString cwd;                         //!< current working directory
  OSGetCurrentDir (&cwd);

  // Get the target name from the command line
  const char *targetlib;
  if (args.getStrValue ("-o", &targetlib) != ArgProc::eKnown) {
    mMsgContext->CGCmdLineError ("-o", "", "missing argument");
    return false;
  }

  // Pull apart the pathname of the target
  OSParseFileName (targetlib, &mTargetDirectory, &mTargetFile);

  // Find the filename without any extension.
  mTargetName.assign (mTargetFile, 0, mTargetFile.rfind ('.'));

  // Construct a simulation name to tag the external interface with.  If we
  // said "-o libdesign.a", then the tag should be 'design'.
  INFO_ASSERT (mTargetName.substr (0,3) == "lib", mTargetName.c_str ());
  mTargetStem = mTargetName.substr (3);

  // The interface-tag must contain valid characters for a C++ identifier So
  // replace anything but [A-Za-z0-9_$] by legal chars.
  UInt32 n_changed = 0;
  for (UtString::size_type i = 0; i < mTargetStem.size (); ++i) {
    char c = mTargetStem [i];
    if (isalnum (c) || (c == '_') || (c == '$')) {
      // valid character
    } else {
      // replace the bogus character with an underscore.
      mTargetStem [i] = '_';
      n_changed++;
    }
  }
  if (n_changed > 0) {
    mMsgContext->CGFileNameChanged (mTargetName.c_str (), mTargetStem.c_str ());
  }

  // Change any non-absolute path into an absolute one!
  if (!mTargetDirectory.empty () && OSIsDirectoryDelimiter (mTargetDirectory [0])) {
    // The target directory is an absolute path
  } else {
    OSConstructFilePath (&mTargetDirectory, cwd.c_str (), mTargetDirectory.c_str ());
  }

  // Construct the path of the build directory
  UtString dirname;
  dirname << "newcarbon." << mTargetName;
  OSConstructFilePath (&mModelDirectory, mTargetDirectory.c_str (), dirname.c_str ());

#if 0
  if (isVerbose ()) {
#define SHOW(_x_) UtIO::cout () << #_x_ << ": " << m##_x_ << "\n";
    SHOW (TargetDirectory);
    SHOW (TargetFile);
    SHOW (TargetStem);
    SHOW (ModelDirectory);
#undef SHOW
  }
#endif

  return true;
}

//! Construct an absolute path name into the model directory
const char *CG::makeModelFilename (UtString filename, UtString *pathname) const
{
  *pathname << mModelDirectory << "/" << filename;
  return pathname->c_str ();
}
