// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/* \file
 *
 * This file contains stubs for the generatorLIR methods in the nucleus and
 * schedule classes. It allows compilation of cbuild with all the new codegen
 * stuff disabled.
 */

#include "cg/CGConfig.h"

#if !CG_ENABLE

#include "nucleus/Nucleus.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"

// declare a degenerate class LIR
class LIR {
};

#define NUGENERATELIR(_class_) \
  LIR *_class_::generateLIR () const { NU_ASSERT (false, this); return NULL; }

NUGENERATELIR (NUBlockingAssign)
NUGENERATELIR (NUAssign)
NUGENERATELIR (NUIdentRvalue)
NUGENERATELIR (NUIdentLvalue)
NUGENERATELIR (NUBinaryOp)
NUGENERATELIR (NUUnaryOp)

#endif
