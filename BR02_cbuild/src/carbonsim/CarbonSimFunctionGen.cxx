// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimFunctionGen.h"
#include "carbonsim/CarbonSimWrappedNet.h"
#include "carbonsim/CarbonSimTransactor.h"
#include "carbonsim/CarbonSimMaster.h"
#include "shell/carbon_capi.h"
#include "util/UtArray.h"
#include "util/Loop.h"

typedef UtArray<CarbonSimNet *> SignalList;
typedef Loop<SignalList> SignalListLoop;
typedef UtArray<CarbonSimTransactor *> TransactorList;
typedef Loop<TransactorList> TransactorListLoop;

class CarbonSimFunctionGenI
{
public: CARBONMEM_OVERRIDES
  SignalList mSignals;
  TransactorList mTransactors;
};

CarbonSimFunctionGen::CarbonSimFunctionGen(CarbonTime offset, UInt32 initial_value)
  : mOffset(offset), mInitialValue(initial_value), mCurrentValue(initial_value), mCurrentTick(0), mMaster(NULL)
{
  mPrivate = new CarbonSimFunctionGenI;
}


CarbonSimFunctionGen::~CarbonSimFunctionGen(void)
{
#if HACK
  for (SignalListLoop i = SignalListLoop(mPrivate->mSignals);
       not i.atEnd(); ++i)
  {
    CarbonSimNet * signal = *i;
    delete signal;
  }
#endif
  delete mPrivate;
}


void CarbonSimFunctionGen::setTick(CarbonTime tick)
{
  INFO_ASSERT(tick >= mCurrentTick, "CarbonSimFunctionGen::setTick: new tick cannot be less than current.");

  CarbonTime transition_tick = getNextTransitionTick();
  mCurrentTick = tick;

  if ((tick == 0) or (tick == transition_tick))
  {
    mCurrentValue = getValue(tick);

    for (SignalListLoop i = SignalListLoop(mPrivate->mSignals); not i.atEnd(); ++i)
    {
      CarbonSimNet * signal = *i;
      signal->deposit(&mCurrentValue);
    }

    for (TransactorListLoop i = TransactorListLoop(mPrivate->mTransactors); not i.atEnd(); ++i)
    {
      CarbonSimTransactor * transactor = *i;
      transactor->step(tick);
    }
  }
}


bool CarbonSimFunctionGen::bindSignal(CarbonSimNet * net)
{
  bool ret = true;
  if (net)
    mPrivate->mSignals.push_back(net);
  else 
    ret = false;
  return ret;
}

bool CarbonSimFunctionGen::bindTransactor(CarbonSimTransactor * transactor)
{
  INFO_ASSERT(transactor, "CarbonSimFunctionGen::bindTransactor: transactor must not be NULL.");
  mPrivate->mTransactors.push_back(transactor);
  // If we already bound the generator to the master, add the transactor
  if (mMaster)
    mMaster->addTransactor(transactor);
  // else the transactor will get added when the generator is bound.
  return true;
}


void CarbonSimFunctionGen::print(bool /* verbose */, UInt32 /* indent */)
{
  ;
}

// Gets called when the generator gets bound.
void CarbonSimFunctionGen::putMaster(CarbonSimMaster* simMaster)
{
  INFO_ASSERT(mMaster == NULL, "CarbonSimFunctionGen::putMaster: simMaster must not be NULL.");
  mMaster = simMaster;
  for (TransactorListLoop i = TransactorListLoop(mPrivate->mTransactors); ! i.atEnd(); ++i)
  {
    CarbonSimTransactor * transactor = *i;
    mMaster->addTransactor(transactor);
  }
}
