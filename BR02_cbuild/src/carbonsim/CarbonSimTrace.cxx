// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include <string.h>
#include "carbonsim/CarbonSimTrace.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbonsim/CarbonSimMaster.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"
#include "util/UtIOStream.h"


CarbonSimTrace::CarbonSimTrace(CarbonSimMaster * master, CarbonSimNet * signal, UInt32 id)
  : mMaster(master), mSignal(signal), mHitCount(0), mID(id)
{
  mSignal->addNotifyOnValueChange(this);
}


CarbonSimTrace::~CarbonSimTrace(void)
{
  mSignal->removeNotifyOnValueChange(this);
  delete mSignal;
}


void CarbonSimTrace::trace(void)
{
  mHitCount++;
  print();
}


void CarbonSimTrace::print(bool verbose, UInt32 indent)
{
  UtString indent_buffer;

  for (UInt32 i = 0; i < indent; i++)
  {
    indent_buffer = indent_buffer + " ";
  }

  SInt32 num_bytes = (mSignal->getNumUInt32s() * 4 * 2) + 1;
  char * b = CARBON_ALLOC_VEC(char, num_bytes);
  mSignal->format(b, num_bytes, eCarbonHex);

  //printf("%sTrace %d\n", indent_buffer.c_str(), mID);
  UtIO::cout() << indent_buffer << "Trace " << mID << UtIO::endl;
  //printf("%s  Tick:   %lld\n", indent_buffer.c_str(),
  //mMaster->getTick());
  UtIO::cout() << indent_buffer << "  Tick:   " << mMaster->getTick() << UtIO::endl;
  if (verbose)
  {
    // printf("%s  Signal: %s\n", indent_buffer.c_str(), mSignal->getName());
    UtIO::cout() << indent_buffer << "  Signal: " << mSignal->getName() << UtIO::endl;
    //printf("%s  Value:  %s\n", indent_buffer.c_str(), b);
    UtIO::cout() << indent_buffer << "  Value:  " << b << UtIO::endl;
    //printf("%s  Hits:   %d\n", indent_buffer.c_str(), mHitCount);
    UtIO::cout() << indent_buffer << "  Hits:   " << mHitCount << UtIO::endl;
  }

  CARBON_FREE_VEC(b, char, num_bytes);
}


void CarbonSimTrace::update(CarbonSimNet * /* net */)
{
  trace();
}
