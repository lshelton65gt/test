// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimOneToOneWire.h"
#include "carbonsim/CarbonSimNet.h"
#include "util/CarbonTypes.h"
#include "util/CarbonAssert.h"


CarbonSimOneToOneWire::CarbonSimOneToOneWire(CarbonSimNet * src, CarbonSimNet * dst, CarbonSimWire::PullType pull)
  : CarbonSimWire(pull), mSrc(src), mDst(dst)
{
  INFO_ASSERT(mSrc, "CarbonSimOneToOneWire::CarbonSimOneToOneWire: source must not be NULL.");
  INFO_ASSERT(mDst, "CarbonSimOneToOneWire::CarbonSimOneToOneWire: destination mus not be NULL.");
  INFO_ASSERT(mSrc->getBitWidth() == mDst->getBitWidth(),
              "CarbonSimOneToOneWire::CarbonSimOneToOneWire: source and destination must have the same width.");  
  allocBuffers(mSrc);
}


CarbonSimOneToOneWire::~CarbonSimOneToOneWire(void)
{
}


void CarbonSimOneToOneWire::print(bool /* verbose */, UInt32 /* indent */)
{
}


void CarbonSimOneToOneWire::flow(void)
{
  UInt32 * copy_data  = 0;
  UInt32 * copy_drive = 0;

  if ((mPull == eCarbonSimWirePullNone) or (mSrc->isDriving()))
  {
    mSrc->examine(mDriveData, mDriveDrive);
    copy_data  = mDriveData;
    copy_drive = mDriveDrive;
  }
  else
  {
    copy_data  = mPullData;
    copy_drive = mPullDrive;
  }

  INFO_ASSERT(copy_data, "CarbonSimOneToOneWire::flow: no data value to deposit.");

  if (mDst->isTristate())
  {
    mDst->deposit(copy_data, copy_drive);
  }
  else
  {
    mDst->deposit(copy_data);
  }
}
