// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimTransactorAdaptor.h"


CarbonSimTransactorAdaptor::CarbonSimTransactorAdaptor(CarbonSimTransactor * transactor)
  : mTransactor(transactor)
{
}


CarbonSimTransactorAdaptor::~CarbonSimTransactorAdaptor(void)
{
}


bool CarbonSimTransactorAdaptor::dispatch(const UtString * /* command */, BitVectorArgList * /* bits_list */)
{
  return true;
}
