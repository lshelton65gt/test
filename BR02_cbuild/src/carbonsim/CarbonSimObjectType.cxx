// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimObjectType.h"
#include "carbonsim/CarbonSimObjectInstance.h"
#include "shell/carbon_capi.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"


//! CarbonSimObjectTypeI class
/*!
  Implementation class for CaebonSimObjectType.  It hides data types
  that we do not want to appear in the CarbonSimObjectType header
  file.
*/
class CarbonSimObjectTypeI
{
public: CARBONMEM_OVERRIDES
  //! Constructor.
  CarbonSimObjectTypeI(const char * type_name, CarbonSimObjectType::InstanceCreationFunction creation_function, CarbonDBType db_type, CarbonInitFlags init_flags)
    : mName(type_name), mCreationFunction(creation_function), mDefaultDBType(db_type), mDefaultInitFlags(init_flags)
  {
  }

public:
  UtString mName;
  CarbonSimObjectType::InstanceCreationFunction mCreationFunction; 
  CarbonDBType mDefaultDBType;
  CarbonInitFlags mDefaultInitFlags;

 private:
  CarbonSimObjectTypeI(void);
  CarbonSimObjectTypeI(const CarbonSimObjectTypeI&);  
  CarbonSimObjectTypeI& operator=(const CarbonSimObjectTypeI&);
};



 CarbonSimObjectType::CarbonSimObjectType(const char * type_name, InstanceCreationFunction creation_function, CarbonDBType db_flags, CarbonInitFlags init_flags)
  : mPrivate (new CarbonSimObjectTypeI(type_name, creation_function, db_flags, init_flags))
{
  INFO_ASSERT(type_name, "CarbonSimObjectType::CarbonSimObjectType: type_name cannot be NULL.");
  INFO_ASSERT(creation_function, "CarbonSimObjectType::CarbonSimObjectType: creation_function cannot be NULL.");
}



CarbonSimObjectType::~CarbonSimObjectType(void)
{
  delete mPrivate;
}



const char * CarbonSimObjectType::getName(void) const
{
  return mPrivate->mName.c_str();
}



CarbonSimObjectType::InstanceCreationFunction CarbonSimObjectType::getCreationFunction(void) const
{
  return mPrivate->mCreationFunction;
}



CarbonDBType CarbonSimObjectType::getDefaultDBType(void) const
{
  return mPrivate->mDefaultDBType;
}



CarbonInitFlags CarbonSimObjectType::getDefaultInitFlags(void) const
{
  return mPrivate->mDefaultInitFlags;
}
