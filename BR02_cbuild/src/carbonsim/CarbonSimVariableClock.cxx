// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimVariableClock.h"
#include "util/CarbonTypes.h"
#include "util/UtIOStream.h"


CarbonSimVariableClock::CarbonSimVariableClock(CarbonTime period, CarbonTime duty_cycle, UInt32 initial_value, CarbonTime offset)
  : CarbonSimClock(period, duty_cycle, initial_value, offset)
{
}


CarbonSimVariableClock::~CarbonSimVariableClock(void)
{
}


void CarbonSimVariableClock::setSpeed(CarbonTime period, CarbonTime duty_cycle)
{
   if (period == 0) {
      UtIO::cerr() << "Error: Clock period must be greater than zero in CarbonSimVariableClock::setSpeed(" << period << ", " << duty_cycle << ")" << UtIO::endl;
   } else if (duty_cycle == 0) {
      UtIO::cerr() << "Error: Clock duty cycle must be greater than zero in CarbonSimVariableClock::setSpeed(" << period << ", " << duty_cycle << ")" << UtIO::endl;
   } else {
      mPeriod = period;
      mDutyCycle = duty_cycle;
      mNonDutyCycle = period - duty_cycle;
   }
   return;
}
