// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimClock.h"
#include "carbonsim/CarbonSimNet.h"
#include "util/UtIOStream.h"
#include "util/CarbonAssert.h"

CarbonSimClock::CarbonSimClock(CarbonTime period, CarbonTime duty_cycle, UInt32 initial_value, CarbonTime offset)
  : CarbonSimFunctionGen(offset, initial_value), mPeriod(period), mDutyCycle(duty_cycle), mNonDutyCycle(mPeriod - mDutyCycle)
{
  if (mPeriod == 0)
  {
    UtIO::cerr() << "!!! PERIOD MUST BE NON-ZERO !!!" << UtIO::endl;
    // assert here so the user will know that it is a CarbonSimClock
    // instantiation problem
    INFO_ASSERT(mPeriod != 0, "CarbonSimClock must have non-zero period.");
  }
}


CarbonSimClock::~CarbonSimClock(void)
{
}


UInt32 CarbonSimClock::getValue(CarbonTime tick)
{
  UInt32 value;


  if (tick < mOffset)
  {
    value = mInitialValue;
  }
  else
  {
    CarbonTime ticks = tick - mOffset;
    CarbonTime remainder = ticks % mPeriod;    

    if ((0 == remainder) || (remainder < mDutyCycle))
    {
      value = mInitialValue;
    }
    else
    {
      value = !mInitialValue;
    }
  }

  return value;
}


CarbonTime CarbonSimClock::getNextTransitionTick(void)
{
  CarbonTime transition_tick = 0;

  if ((mOffset != 0) && ((0 == mCurrentTick) && (mCurrentTick < mOffset)))
  {
    transition_tick = mOffset;
  }
  else
  {
    CarbonTime ticks = mCurrentTick - mOffset;
    CarbonTime remainder = ticks % mPeriod;    

    if ((0 == remainder) || (remainder < mDutyCycle))
    {
      transition_tick = mCurrentTick + (mDutyCycle - remainder);
    }
    else
    {
      transition_tick = mCurrentTick + ((mDutyCycle + mNonDutyCycle) - remainder);
    }
  }

  return transition_tick;
}
