// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMONETOONESOCKETWIRE_H_
#define __CARBONSIMONETOONESOCKETWIRE_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimOneToOneSocketWire class.
*/
#endif

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef __CARBONSIMONETOONEWIRE_H_
#include "carbonsim/CarbonSimOneToOneWire.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonSimNet;



//! CarbonSimOneToOneSocketWire class
/*!
  CarbonSimOneToOneSocketWire provides an interface for connecting multiple 
  Carbon Models to each other over socket connections. This class is
  bound to the ports on the Carbon Models to be connected, and must be
  registered with the CarbonSimMaster. If the Carbon Models are distributed
  across multiple processes or machines, CarbonSimOneToOneSocketWire will manage
  the connection without any change in the interface that is called by the
  user. The abstraction that CarbonSimOneToOneSocketWire provides creates a highly 
  flexible and portable environment for linking to Carbon Models.
*/
class CarbonSimOneToOneSocketWire : public CarbonSimOneToOneWire
{
 public:   CARBONMEM_OVERRIDES
//! Constructor.for outgoing socket
/*! Declares a wire between one net and a socket connection

    \param src The source net of the wire.
    \param port The socket port number to be written.
    \param buffer_size The number of entries to buffer before writing
    to the socket.
*/

  CarbonSimOneToOneSocketWire(CarbonSimNet * src, UInt32 port, UInt32 buffer_size=1);

//! Constructor.for incoming socket
/*! Declares a wire between one net and a socket connection

    \param ipaddr The IP address of the socket to be read.
    \param port The socket port number to be read.
    \param dst The destination net of the wire. This may be any legal 
    net defined in the system that can be driven.
    \param buffer_size The number of entries that have been buffered to
    be read from the socket.
*/

  CarbonSimOneToOneSocketWire(const char * ipaddr, UInt32 port, CarbonSimNet * dst, UInt32 buffer_size=1);

  //! Destructor.
  virtual ~CarbonSimOneToOneSocketWire(void);


#ifndef CARBON_EXTERNAL_DOC
  // Causes the data to move.  
  virtual void flow(void);

  //! Prints this to UtIO::cout().
  virtual void print(bool verbose = true, UInt32 indent = 0);
#endif

 private:
  CarbonSimNet * mSrc;
  CarbonSimNet * mDst;
  UInt32 mPort;
  int mSockFD, mMsgFD;

  UInt32 mSocketBufferEntries;
  char* mSocketBuffer;
  UInt32 mSocketBufferIdx;

  UInt32 mDriveDataSize;

 private:
  bool examineSocket(void);
  bool depositSocket(void);

  void init();
  int closeSocket(int sd);

  CarbonSimOneToOneSocketWire(void);
  CarbonSimOneToOneSocketWire(const CarbonSimOneToOneSocketWire&);  
  CarbonSimOneToOneSocketWire& operator=(const CarbonSimOneToOneSocketWire&);
};


#endif
