// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimAllocatedInputNet.h"



CarbonSimAllocatedInputNet::CarbonSimAllocatedInputNet(const char * name, SInt32 msb, SInt32 lsb)
  : CarbonSimAllocatedNet(name, msb, lsb)
{
}



CarbonSimAllocatedInputNet::~CarbonSimAllocatedInputNet(void)
{
}

bool CarbonSimAllocatedInputNet::isInput(void) const { return true; }

bool CarbonSimAllocatedInputNet::isOutput(void) const { return false; }

bool CarbonSimAllocatedInputNet::isBidirect(void) const { return false; }

bool CarbonSimAllocatedInputNet::isTristate(void) const { return false; }

bool CarbonSimAllocatedInputNet::isDriving(void) const { return false; }

bool CarbonSimAllocatedInputNet::isDepositable(void) const
{
  return true;
}

