// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimAllocatedNet.h"
#include "shell/carbon_capi.h"
#include "util/UtConv.h"
#include "util/UtString.h"
#include "util/UtSubject.h"

class CarbonSimAllocatedNetI
{
public: CARBONMEM_OVERRIDES
  CarbonSimAllocatedNetI(const char* name): mName(name) {}
  UtString mName;
  UtSubject<CarbonSimNet> mValueSubject;
};


CarbonSimAllocatedNet::CarbonSimAllocatedNet(const char * name, SInt32 msb, SInt32 lsb)
  : CarbonSimNet(), mMSB(msb), mLSB(lsb), mForced(false), mValueChanged(false)
{
  mPrivate = new CarbonSimAllocatedNetI(name);
  mPrivate->mValueSubject.putSubject(this);

  mCurrentDrive  = allocateBuffer();
  mCurrentData   = allocateBuffer();
  mPreviousDrive = allocateBuffer();
  mPreviousData  = allocateBuffer();

  setBuffer(mCurrentDrive, getNumUInt32s());
}


CarbonSimAllocatedNet::~CarbonSimAllocatedNet(void)
{
  UInt32 num_words = getNumUInt32s();

  CARBON_FREE_VEC(mCurrentDrive, UInt32, num_words);
  CARBON_FREE_VEC(mCurrentData, UInt32, num_words);
  CARBON_FREE_VEC(mPreviousDrive, UInt32, num_words);
  CARBON_FREE_VEC(mPreviousData, UInt32, num_words);

  delete mPrivate;
}

const CarbonSimAllocatedNet* CarbonSimAllocatedNet::castAllocatedNet() const
{
  return this;
}

const char * CarbonSimAllocatedNet::getName() const
{
  return mPrivate->mName.c_str();
}


SInt32 CarbonSimAllocatedNet::getNumUInt32s(void) const
{
  return (getBitWidth() + 31) / 32;
}


SInt32 CarbonSimAllocatedNet::getBitWidth(void) const
{
  UInt32 sub = mMSB - mLSB;
  return static_cast<SInt32>(std::abs(static_cast<int>(sub)) + 1);
}


CarbonStatus CarbonSimAllocatedNet::deposit(const UInt32 * buf, const UInt32 * drive)
{
  if (not mForced)
  {
    UInt32 num_words = getNumUInt32s();

    if(isObserved())
    {
      mValueChanged = compareBuffer(buf, mCurrentData, num_words);
    }

    copyBuffer(buf, mCurrentData, num_words);
    if (drive)
    {
      copyBuffer(drive, mCurrentDrive, num_words);
    }
    else
    {
      clearBuffer(mCurrentDrive, num_words);
    }      
  }

  return eCarbon_OK;
}


CarbonStatus CarbonSimAllocatedNet::examine(UInt32 * value, UInt32 * drive) const
{  
  UInt32 num_words = getNumUInt32s();

  copyBuffer(mCurrentData, value, num_words);
  copyBuffer(mCurrentDrive, drive, num_words);

  return eCarbon_ERROR;
}


CarbonStatus CarbonSimAllocatedNet::force(const UInt32 * value)
{
  deposit(value);
  mForced = true;
  return eCarbon_OK;
}


CarbonStatus CarbonSimAllocatedNet::release(void)
{  
  mForced = false;
  return eCarbon_OK;
}


CarbonStatus CarbonSimAllocatedNet::format(char * value, int len, CarbonRadix radix) const
{
  int ret = 0;
  size_t num_bits = static_cast<unsigned int>(getBitWidth());
  
  switch(radix)
  {
  case eCarbonBin:
    ret = CarbonValRW::writeBinValToStr(value, len, mCurrentData, num_bits);
    break;
  case eCarbonHex:
    ret = CarbonValRW::writeHexValToStr(value, len, mCurrentData, num_bits);
    break;
  case eCarbonOct:
    ret = CarbonValRW::writeOctValToStr(value, len, mCurrentData, num_bits);
    break;
  case eCarbonDec:
    ret = CarbonValRW::writeDecValToStr(value, len, mCurrentData, true, num_bits);
    break;
  default:
    ret = -1;
    break;
  }

  return (ret == -1) ? eCarbon_ERROR : eCarbon_OK;
}


void CarbonSimAllocatedNet::addNotifyOnValueChange(UtObserver<CarbonSimNet> * observer)
{
  INFO_ASSERT(observer, "Attempt to register NULL for value change notification.");
  mPrivate->mValueSubject.attach(observer);
}


void CarbonSimAllocatedNet::removeNotifyOnValueChange(UtObserver<CarbonSimNet> * observer)
{
  INFO_ASSERT(observer, "Attempt to unregister NULL for value change notification.");
  mPrivate->mValueSubject.detach(observer);
}


void CarbonSimAllocatedNet::notifyValueChange(void)
{
  mPrivate->mValueSubject.notify();
}


void CarbonSimAllocatedNet::schedule(CarbonTime /* tick_time */)
{
  if (isObserved() and mValueChanged)
  {
    mPrivate->mValueSubject.notify();
    mValueChanged = false;
  }
}


UInt32 * CarbonSimAllocatedNet::allocateBuffer(void)
{
  UInt32 num_words = getNumUInt32s();
  UInt32 * b = CARBON_ALLOC_VEC(UInt32, num_words);

  CarbonValRW::setToZero(b, num_words);

  return b;
}


void CarbonSimAllocatedNet::setBuffer(UInt32 * x, UInt32 num_words) const
{
  CarbonValRW::setToOnes(x, num_words);
}


void CarbonSimAllocatedNet::clearBuffer(UInt32 * x, UInt32 num_words) const
{
  CarbonValRW::setToZero(x, num_words);
}


void CarbonSimAllocatedNet::copyBuffer(const UInt32 * src, UInt32 * dst, UInt32 num_words) const
{
  if (src and dst)
  {
    memcpy (dst, src, num_words * sizeof(UInt32));
  }
}


bool CarbonSimAllocatedNet::compareBuffer(const UInt32 * x, const UInt32 * y, UInt32 num_words) const
{
  bool result = false;

  if (x and y)
  {
    result = (memcmp (x, y, num_words * sizeof(UInt32)) != 0);
  }

  return result;
}


bool CarbonSimAllocatedNet::isObserved(void)
{
  return mPrivate->mValueSubject.isObserved();
} 

bool CarbonSimAllocatedNet::isScalar(void) const 
{ 
  return getBitWidth() == 1;
}

bool CarbonSimAllocatedNet::isVector(void) const 
{ 
  return getBitWidth() > 1;
}
