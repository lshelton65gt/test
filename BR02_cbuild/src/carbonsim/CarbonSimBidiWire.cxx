// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include <cstdio>
#include "carbonsim/CarbonSimBidiWire.h"
#include "carbonsim/CarbonSimNet.h"
#include "util/Loop.h"
#include "util/UtSubject.h"
#include "util/UtArray.h"


typedef UtArray<CarbonSimNet *> NetList;
typedef Loop<NetList> NetListLoop;


class CarbonSimBidiWireI
{
public: CARBONMEM_OVERRIDES
  NetList mDrivers;
  UtSubject<CarbonSimBidiWire> mConflictSubject;
};



CarbonSimBidiWire::CarbonSimBidiWire(void)
  : CarbonSimWire(eCarbonSimWirePullNone)
{					       
  init();
}


CarbonSimBidiWire::CarbonSimBidiWire(CarbonSimWire::PullType pull)
  : CarbonSimWire(pull)
{					       
  init();
}


void CarbonSimBidiWire::init(void)
{
  mPrivate = new CarbonSimBidiWireI;
}


CarbonSimBidiWire::~CarbonSimBidiWire(void)
{
}


void CarbonSimBidiWire::print(bool /* verbose */, UInt32 /* indent */)
{
}


void CarbonSimBidiWire::addDriver(CarbonSimNet * driver)
{
  if ( mPrivate->mDrivers.empty() )
  {
    allocBuffers(driver);
  }
  else
  {
    INFO_ASSERT(mPrivate->mDrivers[0]->getBitWidth() == driver->getBitWidth(),
                "CarbonSimBidiWire::addDriver: cannot add driver, bit widths do not match.");
  }

  mPrivate->mDrivers.push_back(driver);
}


void CarbonSimBidiWire::flow(void)
{
  UInt32 * copy_data = 0;
  UInt32 * copy_drive = 0;
  CarbonSimNet * driver = 0;


  /*
  ** Find a driver.  For now we will go through the whole list to
  ** detect conflicts.  If this is too slow, we can instead bail once
  ** we find a driver.
  */
  for (NetListLoop i = NetListLoop(mPrivate->mDrivers); not i.atEnd(); ++i)
  {
    CarbonSimNet * net = *i;

    if (net->isDriving())
    {
      if (driver != 0)
      {
        /* 
           Hey, this thing could be forced, in which case there could
           be a false positive as to a drive conflict. We need an api
           function that tells us what bits are forced.
        */
	if (isObserved())
	{
	  notifyDriveConflict();
	}
      }
      driver = net;
    }
  }


  /*
  ** Figure out what data to propogate.
  */
  if (driver)
  {
    driver->examine(mDriveData, mDriveDrive);

    copy_data  = mDriveData;
    copy_drive = mDriveDrive;
  }
  else
  {
    switch (mPull)
    {
    case eCarbonSimWirePullUp:
    case eCarbonSimWirePullDown:
      copy_data  = mPullData;
      copy_drive = 0;
      break;

    case eCarbonSimWirePullNone:
      copy_data  = 0;
      copy_drive = mUndriveDrive;
      break;

    default:
      INFO_ASSERT(0, "Unexpected pull value.");
    }
  }


  /*
  ** Move it.
  */
  for (NetListLoop i = NetListLoop(mPrivate->mDrivers); not i.atEnd(); ++i)
  {
    CarbonSimNet * dst = *i;

    if ((driver != dst) and (not dst->isOutput()))
    {
      bool doPull = (copy_drive == mUndriveDrive) and (dst->isTristate());
      bool doPropogateDrive = (driver and (driver->isTristate()) and (dst->isTristate()));

      if (doPull || doPropogateDrive)
      {
	dst->deposit(copy_data, copy_drive);
      }
      else
      {
	dst->deposit(copy_data);
      }
    }
  }
}


void CarbonSimBidiWire::addNotifyOnDriveConflict(UtObserver<CarbonSimBidiWire> * observer)
{
  INFO_ASSERT(observer, "Attempt to register NULL for value change notification.");
  mPrivate->mConflictSubject.attach(observer);
}


void CarbonSimBidiWire::removeNotifyOnDriveConflict(UtObserver<CarbonSimBidiWire> * observer)
{
  INFO_ASSERT(observer, "Attempt to unregister NULL for value change notification.");
  mPrivate->mConflictSubject.detach(observer);
}


void CarbonSimBidiWire::notifyDriveConflict(void)
{
  mPrivate->mConflictSubject.notify();
}


bool CarbonSimBidiWire::isObserved(void)
{
  return mPrivate->mConflictSubject.isObserved();
} 
