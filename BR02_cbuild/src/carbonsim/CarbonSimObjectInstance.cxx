// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "CarbonSimObjectInstanceI.h"
#include "carbonsim/CarbonSimObjectInstance.h"
#include "carbonsim/CarbonSimObjectType.h"
#include "shell/carbon_capi.h"




CarbonSimObjectInstance::CarbonSimObjectInstance(const char * instance_name, CarbonSimObjectType * object_type, CarbonDBType db_flags, CarbonInitFlags init_flags)
  : mType(object_type)
  , mCarbonObject ((mType->getCreationFunction())(db_flags, init_flags))
  , mPrivate( new CarbonSimObjectInstanceI(instance_name))
{}



CarbonSimObjectInstance::~CarbonSimObjectInstance(void)
{
  carbonDestroy(&mCarbonObject);
  delete mPrivate;
}



const char * CarbonSimObjectInstance::getName(void) const
{
  return mPrivate->mName.c_str();
}



CarbonStatus CarbonSimObjectInstance::schedule(CarbonTime tick_time)
{
  return carbonSchedule(mCarbonObject, tick_time);
}
