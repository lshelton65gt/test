// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "CarbonSimTestApplication.h"
#include "carbonsim/CarbonSimMaster.h"
#include "carbonsim/CarbonSimClock.h"
#include "util/UtIOStream.h"


CarbonSimTestApplication::CarbonSimTestApplication(int* argc, char **argv)
  : UtApplication(argc, argv)
{
}


CarbonSimTestApplication::~CarbonSimTestApplication(void)
{
}


SInt32 CarbonSimTestApplication::run(void)
{
  UtOStream& out = UtIO::cout();
  CarbonSimMaster * master = CarbonSimMaster::create();

  CarbonSimClock clk_a(10000, 4000);
  //clk_addCallback(edge, model, "net name", function);

  CarbonSimClock clk_b(5000, 3000);
  CarbonSimClock clk_c(3000, 1500, 0, 1000);

  master->addClock(&clk_a);
  master->addClock(&clk_b);
  master->addClock(&clk_c);

  out << "Step Test" << UtIO::endl;

  out << master->getTick() << UtIO::endl;
  for (UInt32 i = 0; i < 20; i ++)
  {
    master->step();
    out << master->getTick() << UtIO::endl;
  }


  out << "Run Test" << UtIO::endl;
  master->run(50000);
  out << master->getTick() << UtIO::endl;


  return 0;
}
