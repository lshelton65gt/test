// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __CARBONSIMTESTAPPLICATION_H_
#define __CARBONSIMTESTAPPLICATION_H_


#include "util/UtApplication.h"


class CarbonSimTestApplication : public UtApplication
{
 public:
  //! Constructor.
  CarbonSimTestApplication (int* argc, char **argv);

  //! Destructor.
  virtual ~CarbonSimTestApplication(void);

  //! Run the application.
  virtual SInt32 run(void);


private:
  CarbonSimTestApplication(void);
  CarbonSimTestApplication(const CarbonSimTestApplication&);
  CarbonSimTestApplication& operator=(const CarbonSimTestApplication&);
};


#endif
