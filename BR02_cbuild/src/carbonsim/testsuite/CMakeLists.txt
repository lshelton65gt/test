# No carbonsim for Windows
if(${WIN32})
  return()
endif()

add_definitions(${DISABLE_DYNAMIC_CAST}
                ${DISABLE_EXCEPTIONS})

set(carbonsimtest_sources
    CarbonSimTestMain.cxx
    CarbonSimTestApplication.cxx
    )

add_executable(carbonsimtest ${carbonsimtest_sources})
target_link_libraries(carbonsimtest
                      ${libCarbonSim}
                      ${libCarbonStatic}
                      ${XTRA_LIBS}
                      ${FLEXLM_APP_LIBFLAGS}
                      ${ZLIB}
                      )

add_dependencies(carbonsimtest
                 carbonsim
                 libCarbonTarget
                 )

set_target_properties(carbonsimtest PROPERTIES
                      RUNTIME_OUTPUT_DIRECTORY ${CARBON_OBJ}/carbonsim/testsuite
                      )
