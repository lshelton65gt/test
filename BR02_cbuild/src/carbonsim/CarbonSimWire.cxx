// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include <string.h>
#include "carbonsim/CarbonSimWire.h"
#include "carbonsim/CarbonSimNet.h"
#include "shell/carbon_capi.h"
#include "util/UtConv.h"
#include "util/CarbonAssert.h"


CarbonSimWire::CarbonSimWire(CarbonSimWire::PullType pull)
  : mPull(pull)
  , mDriveData (0), mDriveDrive (0)
  , mPullData (0), mPullDrive (0)
  , mUndriveDrive (0)
{
}


CarbonSimWire::~CarbonSimWire(void)
{
  deleteBuffers();
}


void CarbonSimWire::print(bool /* verbose */, UInt32 /* indent */)
{
}


void CarbonSimWire::allocBuffers(CarbonSimNet * net)
{
  mDriveData     = CarbonSimWire::allocCopyBuffer(net);
  mDriveDrive    = CarbonSimWire::allocCopyBuffer(net);
  mPullData      = CarbonSimWire::allocPullDataBuffer(net, mPull);
  mPullDrive     = CarbonSimWire::allocPullDriveBuffer(net, mPull);
  mUndriveDrive  = CarbonSimWire::allocUndriveDriveBuffer(net);
}


void CarbonSimWire::deleteBuffers(void)
{
  if (mDriveData)    carbonmem_free(mDriveData);
  if (mDriveDrive)   carbonmem_free(mDriveDrive);
  if (mPullData)     carbonmem_free(mPullData);
  if (mPullDrive)    carbonmem_free(mPullDrive);
  if (mUndriveDrive) carbonmem_free(mUndriveDrive);
}


UInt32 * CarbonSimWire::allocCopyBuffer(CarbonSimNet * net)
{
  UInt32 num_words = net->getNumUInt32s();
  UInt32 * b = (UInt32*) carbonmem_malloc((sizeof(UInt32)) * num_words);

  clearBits(b, num_words);
  
  return b;
}


UInt32 * CarbonSimWire::allocPullDataBuffer(CarbonSimNet * net, CarbonSimWire::PullType pull)
{
  UInt32 num_words = net->getNumUInt32s();
  UInt32 * b = 0;

  switch (pull)
  {
  case eCarbonSimWirePullNone:
    b = 0;
    break;
  case eCarbonSimWirePullUp:
    b = CarbonSimWire::allocCopyBuffer(net);
    setBits(b, num_words, net->getBitWidth());
    break;
  case eCarbonSimWirePullDown:
    b = CarbonSimWire::allocCopyBuffer(net);
    clearBits(b, num_words);
    break;
  default:
    INFO_ASSERT(0, "CarbonSimWire::allocPullDataBuffer: unknown pull type.");
    break;
  }

  return b;
}


UInt32 * CarbonSimWire::allocPullDriveBuffer(CarbonSimNet * net, CarbonSimWire::PullType pull)
{
  UInt32 * b = 0;

  switch (pull)
  {
  case eCarbonSimWirePullNone:
    b = 0;
    break;
  case eCarbonSimWirePullUp:
  case eCarbonSimWirePullDown:
    b = CarbonSimWire::allocCopyBuffer(net);
    setBits(b, net->getNumUInt32s(), net->getBitWidth());
    break;
  default:
    INFO_ASSERT(0, "CarbonSimWire::allocPullDriveBuffer: unknown pull type.");
    break;
  }

  return b;
}


UInt32 * CarbonSimWire::allocUndriveDriveBuffer(CarbonSimNet * net)
{
  UInt32 * b = CarbonSimWire::allocCopyBuffer(net);
  setBits(b, net->getNumUInt32s(), net->getBitWidth());
  return b;
}


void CarbonSimWire::clearBits(UInt32 * b, UInt32 num_words)
{
  CarbonValRW::setToZero(b, num_words);
}


void CarbonSimWire::setBits(UInt32 * b, UInt32 num_words, UInt32 num_set_bits)
{
  clearBits(b, num_words);

  for (UInt32 i = 0; i < num_words; i++)
  {
    UInt32 value = 0xffffffff;
    UInt32 shift = num_set_bits % 32;


    if (num_set_bits == 0)
    {
      value = 0;
    }
    else if (shift > 0)
    {
      value = ~(0xffffffff << shift);
    }
    else
    {
      value = 0xffffffff;
    }


    b[i] = value;


    if (num_set_bits >= 32)
    {
      num_set_bits -= 32;
    }
    else
    {
      num_set_bits = 0;
    }
  }
}
