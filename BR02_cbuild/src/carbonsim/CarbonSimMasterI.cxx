// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/




#include "CarbonSimMasterI.h"
#include "carbonsim/CarbonSimMemory.h"
#include "carbonsim/CarbonSimNet.h"
#include "util/UtStringArray.h"

CarbonSimMasterI::CarbonSimMasterI(void)
{
}



CarbonSimMasterI::~CarbonSimMasterI(void)
{
  for (NetCache::UnsortedLoop i = mNetCache.loopUnsorted(); not i.atEnd(); ++i)
  {
    CarbonSimNet * signal = i.getValue();
    delete signal;
  }

  for (MemoryCache::UnsortedLoop i = mMemoryCache.loopUnsorted(); not i.atEnd(); ++i)
  {
    CarbonSimMemory * signal = i.getValue();
    delete signal;
  }

  for (InstanceList::iterator i = mInstanceList.begin(),
         e = mInstanceList.end();
       i != e;
       ++i)
  {
    CarbonSimObjectInstance * object = *i;
    delete object;
  }  
}


CarbonSimObjectInstance * CarbonSimMasterI::findInstance(UtStringArray * name_comps, UtStringArray::iterator * last_name_cmp)
{
  InstanceSet::iterator longest_match = mInstances.end();
  UtString current_path = "";

  for (UtStringArray::iterator i = name_comps->begin(); i != name_comps->end(); ++i)
  {
    current_path += *i;
    
    InstanceSet::iterator m = mInstances.find(current_path);
    if (m != mInstances.end())
    {
      longest_match = m;
      if (last_name_cmp)
      {
	*last_name_cmp = i;
      }
    }

    current_path += ".";
  }

  if (last_name_cmp)
  {
    (*last_name_cmp)++;
  }

  return (longest_match != mInstances.end()) ?
    (*longest_match).second : static_cast<CarbonSimObjectInstance*>(NULL);
}
