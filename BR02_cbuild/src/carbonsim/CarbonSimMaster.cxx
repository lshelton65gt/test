// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimMaster.h"
#include "CarbonSimObjectFactory.h"
#include "CarbonSimMasterI.h"
#include "carbonsim/CarbonSimClock.h"
#include "carbonsim/CarbonSimFunctionGen.h"
#include "carbonsim/CarbonSimMemory.h"
#include "carbonsim/CarbonSimAllocatedInputNet.h"
#include "carbonsim/CarbonSimAllocatedOutputNet.h"
#include "carbonsim/CarbonSimWrappedNet.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbonsim/CarbonSimObjectInstance.h"
#include "carbonsim/CarbonSimStep.h"
#include "carbonsim/CarbonSimWire.h"
#include "carbonsim/CarbonSimTransactor.h"
#include "hdl/HdlId.h"
#include "hdl/HdlVerilogPath.h"
#include "shell/carbon_capi.h"
#include "util/UtString.h"
#include <algorithm>




CarbonSimMaster::CarbonSimMaster(void)
  : mCurrentTick(0), mMaxTick(UtUINT64_MAX), mInterrupted(false), mFinished(false), mIsInitialStep(true)
{
  mPrivate = new CarbonSimMasterI;
  mPrivate->mFinishedSubject.putSubject(this);
}


CarbonSimMaster::~CarbonSimMaster(void)
{
  delete mPrivate;
}



CarbonSimObjectType * CarbonSimMaster::addObjectType(const char * type_name, CarbonSimObjectType::InstanceCreationFunction creation_function)
{
  return mPrivate->mCarbonObjectFactory.addObjectType(type_name, creation_function);
}



CarbonSimObjectInstance * CarbonSimMaster::addInstance(CarbonSimObjectType * object_type, const char * instance_name, CarbonDBType db_type, CarbonInitFlags init_flags)
{
  CarbonSimObjectInstance * object_instance = mPrivate->mCarbonObjectFactory.createNewInstance(object_type, instance_name, db_type, init_flags);

  mPrivate->mInstances[UtString(instance_name)] = object_instance;
  mPrivate->mInstanceList.push_back(object_instance);

  return object_instance;
}


CarbonSimObjectInstance * CarbonSimMaster::findInstance(const char * instance_name) const
{
  HdlId info;
  HdlVerilogPath vp;
  UtStringArray name_comps;

  vp.decompPath(instance_name, &name_comps, &info);
  return mPrivate->findInstance(&name_comps, 0);
}



void CarbonSimMaster::addAlias(const char * alias, CarbonSimNet * signal)
{
  INFO_ASSERT(alias, "CarbonSimMaster::addAlias: alias cannot be NULL.");
  INFO_ASSERT(signal, "CarbonSimMaster::addAlias: signal cannot be NULL.");
  mPrivate->mAliases[UtString(alias)] = signal;
}



void CarbonSimMaster::addClock(CarbonSimClock * clk)
{
  INFO_ASSERT(clk, "CarbonSimMaster::addClock: clk cannot be NULL.");
  mPrivate->mFunctionGens.push_back(clk);
  clk->putMaster(this);
}



void CarbonSimMaster::addStep(CarbonSimStep * stp)
{
  INFO_ASSERT(stp, "CarbonSimMaster::addStep: stp cannot be NULL.");
  mPrivate->mFunctionGens.push_back(stp);
  stp->putMaster(this);
}



void CarbonSimMaster::addWire(CarbonSimWire * clk)
{
  INFO_ASSERT(clk, "CarbonSimMaster::addWire: wire cannot be NULL.");
  mPrivate->mWires.push_back(clk);
}



void CarbonSimMaster::addTransactor(CarbonSimTransactor * transactor)
{
  INFO_ASSERT(transactor, "CarbonSimMaster::addTransactor: transactor cannot be NULL.");
  if (transactor->getId() < 0)
  {
    // If the id is non-negative, we already have the transactor in
    // our list. It is being added by another CarbonSimFuncGen.
    SInt32 id = mPrivate->mTransactors.size();
    transactor->putId(id);
    mPrivate->mTransactors.push_back(transactor);
  }
}



void CarbonSimMaster::interrupt(void)
{
  mInterrupted = true;
}

void CarbonSimMaster::tallyInterrupt(CarbonSimTransactor* transactor)
{
  mPrivate->mTransactorTally.set(transactor->getId());
  mInterrupted = true;
}

void CarbonSimMaster::finish(void)
{
  mFinished = true;
  notifyFinish();
}



bool CarbonSimMaster::isFinished(void)
{
  return mFinished;
}



void CarbonSimMaster::addNotifyOnFinish(UtObserver<CarbonSimMaster> * observer)
{
  INFO_ASSERT(observer, "CarbonSimMaster::addNotifyOnFinish: cannot register NULL for notification.");
  mPrivate->mFinishedSubject.attach(observer);
}



void CarbonSimMaster::removeNotifyOnFinish(UtObserver<CarbonSimMaster> * observer)
{
  INFO_ASSERT(observer, "CarbonSimMaster::removeNotifyOnFinish: cannot unregister NULL for notification.");
  mPrivate->mFinishedSubject.detach(observer);
}



void CarbonSimMaster::notifyFinish(void)
{
  mPrivate->mFinishedSubject.notify();
}



void CarbonSimMaster::run(void)
{
  while ((not mFinished) and (not mInterrupted) and (mCurrentTick < mMaxTick))
  {
    step(mMaxTick - mCurrentTick);
  }
  mInterrupted = false;
}

void CarbonSimMaster::runTransactors(bool cumulative)
{
  DynBitVector* tally = &(mPrivate->mTransactorTally);
  tally->resize(mPrivate->mTransactors.size());
  tally->reset();
  while ((not mFinished) and (! tally->all()) and (mCurrentTick < mMaxTick))
  {
    if (! cumulative)
      // all transactors must fire on same step
      tally->reset();

    step(mMaxTick - mCurrentTick);
  }
  mInterrupted = false;
}


void CarbonSimMaster::run(CarbonTime delta_ticks, bool ignoreInterrupts)
{
  CarbonTime end_tick = mCurrentTick + delta_ticks;

  while ((not mFinished) and (not mInterrupted) and (mCurrentTick < end_tick))
  {
    step(end_tick - mCurrentTick);
    if (ignoreInterrupts)
      mInterrupted = false;
  }
  mInterrupted = false;
}



void CarbonSimMaster::step(CarbonTime delta_ticks)
{
  if (not mFinished)
  {
    CarbonTime next_edge_tick = 0;
    CarbonTime end_tick = mCurrentTick + delta_ticks;
    bool is_edge = findNextFunctionEdgeTick(&next_edge_tick);

    if (mIsInitialStep)
    {
      schedule(0);
      mIsInitialStep = 0;
    }
    else if (is_edge and (next_edge_tick <= end_tick))
    {
      schedule(next_edge_tick);
    }
    else
    {
      schedule(end_tick);
    }
  }
}



void CarbonSimMaster::step(void)
{
  if (not mFinished)
  {
    CarbonTime next_edge_tick = 0;
    bool is_edge = findNextFunctionEdgeTick(&next_edge_tick);

    if (mIsInitialStep)
    {
      schedule(0);
      mIsInitialStep = 0;
    }
    else if (is_edge)
    {
      schedule(next_edge_tick);
    }
  }
}



bool CarbonSimMaster::findNextFunctionEdgeTick(CarbonTime * next_tick)
{
  bool has_tick = false;

  *next_tick = mMaxTick;

  for (CarbonSimMasterI::FunctionGenListLoop i = CarbonSimMasterI::FunctionGenListLoop(mPrivate->mFunctionGens); not i.atEnd(); ++i)
  {
    CarbonSimFunctionGen * fg = *i;

    if (fg->hasNextTransitionTick())
    {
      has_tick = true;

      CarbonTime new_tick = fg->getNextTransitionTick();
      if (new_tick < *next_tick)
      {
	*next_tick = new_tick;
      }
    }
  }

  return has_tick;
}



void CarbonSimMaster::schedule(CarbonTime tick_time)
{
  mCurrentTick = tick_time;

  /*
  ** Tell each of the function generators where we are in time.
  */
  for (CarbonSimMasterI::FunctionGenListLoop i = CarbonSimMasterI::FunctionGenListLoop(mPrivate->mFunctionGens); not i.atEnd(); ++i)
  {
    CarbonSimFunctionGen * fg = *i;
    fg->setTick(tick_time);
  }

  /*
  **
  */
  for (CarbonSimMasterI::NetCache::UnsortedLoop i = mPrivate->mAllocatedNetCache.loopUnsorted(); not i.atEnd(); ++i)
  {
    CarbonSimAllocatedNet * signal = (CarbonSimAllocatedNet*)(CarbonSimNet*)(i.getValue());
    INFO_ASSERT(signal, "CarbonSimMaster::schedule: NULL signal in cache.");
    signal->schedule(tick_time);
  }


  /*
  ** Run each object.
  */
  for (CarbonSimMasterI::InstanceList::const_iterator i = mPrivate->mInstanceList.begin(),
         e = mPrivate->mInstanceList.end();
       i != e;
       ++i)
  {
    CarbonSimObjectInstance * object = *i;
    object->schedule(tick_time);
  }  

  /*
  ** Propogate data through wires.
  */
  for (CarbonSimMasterI::WireListLoop i = CarbonSimMasterI::WireListLoop(mPrivate->mWires); not i.atEnd(); ++i)
  {
    CarbonSimWire * wire = *i;
    wire->flow();
  }
}



CarbonSimNet * CarbonSimMaster::findNet(const char * path_name)
{
  CarbonSimNet * signal = findCachedNet(path_name);

  if (not signal)
  {
    signal = createNet(path_name);
    if (signal)
    {
      storeCachedNet(signal);
    }
  }

  return signal;
}



CarbonSimNet * CarbonSimMaster::allocInputNet(const char * path_name, SInt32 msb, SInt32 lsb)
{
  CarbonSimNet * signal = findCachedNet(path_name);
  INFO_ASSERT(signal == 0, "CarbonSimMaster::allocInputNet: signal already allocated.");

  signal = new CarbonSimAllocatedInputNet(path_name, msb, lsb);
  storeCachedNet(signal);
  mPrivate->mAllocatedNetCache[signal->getName()] = signal;

  return signal;
}



CarbonSimNet * CarbonSimMaster::allocOutputNet(const char * path_name, SInt32 msb, SInt32 lsb)
{
  CarbonSimNet * signal = findCachedNet(path_name);
  INFO_ASSERT(signal == 0, "CarbonSimMaster::allocOutputNet: signal already allocated.");

  signal = new CarbonSimAllocatedOutputNet(path_name, msb, lsb);
  storeCachedNet(signal);
  mPrivate->mAllocatedNetCache[signal->getName()] = signal;

  return signal;
}



CarbonSimNet * CarbonSimMaster::findNetByAlias(const char * alias) const
{
  CarbonSimMasterI::AliasSet::const_iterator m =
    mPrivate->mAliases.find(UtString(alias));
  return (m != mPrivate->mAliases.end()) ?
    (*m).second : static_cast<CarbonSimNet*>(NULL);
}



CarbonSimMemory * CarbonSimMaster::findMemory(const char * path_name)
{
  CarbonSimMemory * signal = findCachedMemory(path_name);

  if (not signal)
  {
    signal = createMemory(path_name);
    if (signal)
    {
      storeCachedMemory(signal);
    }
  }

  return signal;
}



CarbonSimNet * CarbonSimMaster::findCachedNet(const char * path_name) const
{
  CarbonSimNet * signal = 0;
  CarbonSimMasterI::NetCache::const_iterator i = mPrivate->mNetCache.find(path_name);

  if (i != mPrivate->mNetCache.end())
  {
    signal = (*i).second;
  }

  return signal;
}



void CarbonSimMaster::storeCachedNet(CarbonSimNet * signal)
{
  mPrivate->mNetCache[signal->getName()] = signal;
}



CarbonSimNet * CarbonSimMaster::createNet(const char * path_name)
{
  CarbonSimObjectInstance * object;
  UtString shell_signal_name;


  if (not convertCarbonSimNameToShellName(path_name, &object, &shell_signal_name))
  {
    return 0;
  }

  CarbonNetID * signal = carbonFindNet(object->getCarbonObject(), shell_signal_name.c_str());
  if (signal == 0)
  {
    return 0;
  }


  return new CarbonSimWrappedNet(object, signal, path_name);
}



CarbonSimMemory * CarbonSimMaster::findCachedMemory(const char * path_name) const
{
  CarbonSimMemory * signal = 0;
  CarbonSimMasterI::MemoryCache::const_iterator i = mPrivate->mMemoryCache.find(path_name);

  if (i != mPrivate->mMemoryCache.end())
  {
    signal = (*i).second;
  }

  return signal;
}



void CarbonSimMaster::storeCachedMemory(CarbonSimMemory * signal)
{
  mPrivate->mMemoryCache[signal->getName()] = signal;
}



CarbonSimMemory * CarbonSimMaster::createMemory(const char * path_name)
{
  CarbonSimObjectInstance * object;
  UtString shell_signal_name;


  if (not convertCarbonSimNameToShellName(path_name, &object, &shell_signal_name))
  {
    return 0;
  }

  CarbonMemoryID * signal = carbonFindMemory(object->getCarbonObject(), shell_signal_name.c_str());
  if (signal == 0)
  {
    return 0;
  }

  return new CarbonSimMemory(object, signal, path_name);
}



bool CarbonSimMaster::convertCarbonSimNameToShellName(const char * path_name, CarbonSimObjectInstance ** object, UtString * shell_name)
{
  HdlId info;
  HdlVerilogPath vp;
  UtStringArray name_comps;
  UtStringArray::iterator last_name_cmp;
  UtString object_inst_name;
  UtString object_def_name;


  vp.decompPath(path_name, &name_comps, &info);

  if (name_comps.size() < 2)
  {
    return 0;
  }


  /*
  ** Look up the object instance.
  */
  last_name_cmp = name_comps.end();
  *object = mPrivate->findInstance(&name_comps, &last_name_cmp);
  if (*object == 0)
  {
    return false;
  }



  /*
  ** Patch together the real signal name.
  */
  UtStringArray signal_name_comps;
  signal_name_comps.push_back((*object)->getType()->getName());
  
  for (UtStringArray::UnsortedLoop p(last_name_cmp, name_comps.end()); ! p.atEnd(); ++p)
    signal_name_comps.push_back(*p);
  
#if 0
  UtStringArray::iterator nc_e = name_comps.end();
  while (last_name_cmp != nc_e)
  {
    signal_name_comps.push_back(*last_name_cmp);
    ++last_name_cmp;
  }
#endif
  //  std::copy (last_name_cmp, name_comps.end(), std::back_inserter
  //  (signal_name_comps));

  vp.compPath (signal_name_comps, shell_name, &info);

  return true;
}



CarbonSimMaster * CarbonSimMaster::create(void)
{
  return new CarbonSimMaster;
}



void CarbonSimMaster::storeWire(CarbonSimWire * wire, const char * wire_signal_name)
{
  mPrivate->mWireCache[wire_signal_name] = wire;
}



CarbonSimWire * CarbonSimMaster::findWire(const char * wire_signal_name) const
{
  CarbonSimWire * wire = 0;
  CarbonSimMasterI::WireCache::const_iterator i = mPrivate->mWireCache.find(wire_signal_name);

  if (i != mPrivate->mWireCache.end())
  {
    wire = (*i).second;
  }

  return wire;
}
