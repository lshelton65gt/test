// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include <string.h>
#include "carbonsim/CarbonSimBreakpoint.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbonsim/CarbonSimMaster.h"
#include "shell/carbon_capi.h"
#include "util/UtConv.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"
#include "util/UtIOStream.h"

CarbonSimBreakpoint::CarbonSimBreakpoint(CarbonSimMaster * master, CarbonSimNet * signal, UInt32 * value, UInt32 id)
  : UtObserver<CarbonSimNet>()
  , mMaster(master)
  , mSignal(signal)
  , mNumWords (signal->getNumUInt32s ())
  , mBreakValue (CARBON_ALLOC_VEC(UInt32, mNumWords))
  , mCurrentValue (CARBON_ALLOC_VEC(UInt32, mNumWords))
  , mHitCount(0)
  , mID(id)
{
  CarbonValRW::setToZero(mCurrentValue, mNumWords);
  memcpy(static_cast<void *>(mBreakValue), static_cast<void *>(value), mNumWords * 4);

  mSignal->addNotifyOnValueChange(this);
}


CarbonSimBreakpoint::~CarbonSimBreakpoint(void)
{
  mSignal->removeNotifyOnValueChange(this);

  //delete mSignal; ???
  CARBON_FREE_VEC(mBreakValue, UInt32, mNumWords);
  CARBON_FREE_VEC(mCurrentValue, UInt32, mNumWords);
}


void CarbonSimBreakpoint::check(void)
{
  CarbonValRW::setToZero(mCurrentValue, mNumWords);
  mSignal->examine(mCurrentValue);

  if ((memcmp(static_cast<void *>(mBreakValue), static_cast<void *>(mCurrentValue), mNumWords * 4)) == 0)
  {
    mHitCount++;
    mMaster->interrupt();
    print();
  }
}


void CarbonSimBreakpoint::print(bool verbose, UInt32 indent)
{
  UtString indent_buffer;

  for (UInt32 i = 0; i < indent; i++)
  {
    indent_buffer = indent_buffer + " ";
  }

  SInt32 num_bytes = (mSignal->getNumUInt32s() * 4 * 2) + 1;
  char * b = CARBON_ALLOC_VEC(char, num_bytes);
  mSignal->format(b, num_bytes, eCarbonHex);

  //printf("%sBreakpoint %d\n", indent_buffer.c_str(), mID);
  UtIO::cout() << indent_buffer << "Breakpoint " << mID << UtIO::endl;
  //printf("%s  Tick:   %lld\n", indent_buffer.c_str(),
  //mMaster->getTick());
  UtIO::cout() << indent_buffer << "  Tick:   " << mMaster->getTick() << UtIO::endl;
  if (verbose)
  {
    // printf("%s  Signal: %s\n", indent_buffer.c_str(), mSignal->getName());
    UtIO::cout() << indent_buffer << "  Signal: " << mSignal->getName() << UtIO::endl;
    //printf("%s  Value:  %s\n", indent_buffer.c_str(), b);
    UtIO::cout() << indent_buffer << "  Value:  " << b << UtIO::endl;
    //printf("%s  Hits:   %d\n", indent_buffer.c_str(), mHitCount);
    UtIO::cout() << indent_buffer << "  Hits:   " << mHitCount << UtIO::endl;
  }
  
  CARBON_FREE_VEC(b, char, num_bytes);
}


void CarbonSimBreakpoint::update(CarbonSimNet * /* net */)
{
  check();
}
