// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimOneToManyWire.h"
#include "carbonsim/CarbonSimNet.h"
#include "util/Loop.h"
#include "util/UtArray.h"


typedef UtArray<CarbonSimNet *> NetList;
typedef Loop<NetList> NetListLoop;


class CarbonSimOneToManyWireI
{
public: CARBONMEM_OVERRIDES
  NetList mDsts;
};


CarbonSimOneToManyWire::CarbonSimOneToManyWire(CarbonSimNet * src, CarbonSimWire::PullType pull)
  : CarbonSimWire(pull), mSrc(src)
{					       
  INFO_ASSERT(mSrc, "CarbonSimOneToManyWire::CarbonSimOneToManyWire: src cannot be NULL.");
  
  mPrivate = new CarbonSimOneToManyWireI;
  allocBuffers(mSrc);
}


void CarbonSimOneToManyWire::addDestination(CarbonSimNet * dst)
{
  INFO_ASSERT(mSrc->getBitWidth() == dst->getBitWidth(),
              "CarbonSimOneToManyWire::addDestination: destination width must be the same as the source width.");
  mPrivate->mDsts.push_back(dst);
}


CarbonSimOneToManyWire::~CarbonSimOneToManyWire(void)
{
  delete mPrivate;
}


void CarbonSimOneToManyWire::print(bool /* verbose */, UInt32 /* indent */)
{
}


void CarbonSimOneToManyWire::flow(void)
{
  UInt32 * copy_data  = 0;
  UInt32 * copy_drive = 0;

  if ((mPull == eCarbonSimWirePullNone) or (mSrc->isDriving()))
  {
    mSrc->examine(mDriveData);
    copy_data  = mDriveData;
    copy_drive = mDriveDrive;
  }
  else
  {
    copy_data  = mPullData;
    copy_drive = mPullDrive;
  }

  INFO_ASSERT(copy_data, "CarbonSimOneToManyWire::flow: no data value to deposit.");

  for (NetListLoop i = NetListLoop(mPrivate->mDsts); not i.atEnd(); ++i)
  {
    CarbonSimNet * dst = *i;

    if (dst->isTristate())
    {
      dst->deposit(copy_data, copy_drive);
    }
    else
    {
      dst->deposit(copy_data);
    }
  }
}
