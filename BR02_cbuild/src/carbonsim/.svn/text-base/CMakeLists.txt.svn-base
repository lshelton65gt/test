# Carbonsim is only built with gcc
if(${MSVC})
  return()
endif()

include_directories(${CMAKE_SOURCE_DIR}/inc/carbonsim
                    ${CMAKE_CURRENT_SOURCE_DIR}
                    )

add_definitions(${DISABLE_DYNAMIC_CAST}
                ${DISABLE_EXCEPTIONS})

set(carbonsim_sources
    CarbonSimBreakpoint.cxx
    CarbonSimDebugger.cxx
    CarbonSimMaster.cxx
    CarbonSimMasterI.cxx
    CarbonSimGatedClock.cxx
    CarbonSimVariableClock.cxx
    CarbonSimClock.cxx
    CarbonSimMemory.cxx
    CarbonSimObjectInstance.cxx
    CarbonSimObjectType.cxx
    CarbonSimObjectFactory.cxx
    CarbonSimNet.cxx
    CarbonSimAllocatedNet.cxx
    CarbonSimAllocatedInputNet.cxx
    CarbonSimAllocatedOutputNet.cxx
    CarbonSimWrappedNet.cxx
    CarbonSimStep.cxx
    CarbonSimTrace.cxx
    CarbonSimTransactor.cxx
    CarbonSimTransactorAdaptor.cxx
    CarbonSimOneToOneWire.cxx
    CarbonSimOneToManyWire.cxx
    CarbonSimBidiWire.cxx
    CarbonSimWire.cxx
    CarbonSimFunctionGen.cxx
   )        

# The carbonsim library is all of libcarbon plus the carbonsim code.
# Since cmake doesn't directly support combining libraries into a
# larger archive, we'll have to do it ourselves, like we do for
# libcarbon.
add_static_library_pure_virtual_hack(carbonsimbase ${CMAKE_CURRENT_BINARY_DIR} ${carbonsim_sources})

# I can't seem to add a dependency on libcarbon for libcarbonsim
# (probably because both are custom targets), so instead add it as a
# dependency of the base library
add_dependencies(carbonsimbase libCarbonTarget)

get_property(libcarbonsimbase TARGET carbonsimbase PROPERTY LOCATION)
add_custom_command(OUTPUT ${libCarbonSim}
                   COMMAND ${CMAKE_COMMAND} -E make_directory ${CARBON_OBJ}/carbonsim
                   COMMAND ${CMAKE_AR} cr ${libCarbonSim}
                   COMMAND ${MERGELIB} ${libCarbonSim} ${libcarbonsimbase} ${libCarbonStatic}
                   DEPENDS carbonsimbase ${libCarbonStatic})

# Add it as a target so it gets built
add_custom_target(carbonsim ALL DEPENDS ${libCarbonSim})

# The socket stuff goes into its own library
add_static_library_pure_virtual_hack(carbonsocket ${CARBON_OBJ}/carbonsim CarbonSimOneToOneSocketWire.cxx)

add_dependencies(carbonsimbase PrepCarbonHome)
add_dependencies(carbonsocket PrepCarbonHome)
