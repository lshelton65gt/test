%{
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#if pfWINDOWS
#define YY_NO_UNISTD_H
#define isatty _isatty
extern "C" int isatty(int);
#endif
#include <stdio.h>
#include "util/UtString.h"
#include "util/UtStringUtil.h"

/*
** This is a hack to prevent icc from complaining about mismatched
** declarations of isatty() [flex doesn't append a throw() clause]
*/
#if pfICC
#define YY_NEVER_INTERACTIVE 1
#endif

// We use "%option stack" but we don't use yy_top_state(), so don't define it.
#define YY_NO_TOP_STATE 1

#include "CarbonSimParserPriv.h"
#include "CarbonSimBisonParser.hxx"

#define lexer_value (static_cast<YYSTYPE *>(yylval))
#define debugger (static_cast<CarbonSimDebugger *>(interpreter))

#define DEBUGGING 0
#if DEBUGGING
#else
#define debug(x)
#endif

%}


a             [a-zA-Z]
d             [[:digit:]]
s             [[:space:]]
w             [[:alnum:]]

ident         [a-zA-Z][a-zA-Z0-9_$]*
numhex        [a-fA-F0-9]+
numdec        [1-9][0-9]*
numoct        [0-7]+

radhex        \'(h|H)
raddec        \'(d|D)
radoct        \'(o|O)

alias         (a|A)(l|L)(i|I)(a|A)(s|S)
breakpoint    (b|B)(r|R)(e|E)(a|A)(k|K)(p|P)(o|O)(i|I)(n|N)(t|T)
continue      (c|C)(o|O)(n|N)(t|T)(i|I)(n|N)(u|U)(e|E)
peek          (p|P)(e|E)(e|E)(k|K)
poke          (p|P)(o|O)(k|K)(e|E)
quit          ((q|Q)(u|U)(i|I)(t|T))
run           ((r|R)(u|U)(n|N))
step          ((s|S)(t|T)(e|E)(p|P))
time          (t|T)(i|I)(m|M)(e|E)
trace         (t|T)(r|R)(a|A)(c|C)(e|E)
trans_post    (t|T)(r|R)(a|A)(n|N)(s|S)_(p|P)(o|O)(s|S)(t|T)


%x numhex
%x numdec
%x numoct

%pointer
%option noyywrap
%option nounput
%option interactive
%option stack
%option bison-bridge


%%

\n                      {
                          return eEnd;
                        }

{peek}                  {
                          return ePeek;
                        }

{poke}                  {
                          return ePoke;
                        }

{alias}                 {
                          return eAlias;
                        }

{breakpoint}            {
                          return eBreakpoint;
                        }

{continue}              {
                          return eContinue;
                        }

{quit}                  {
                          return eQuit;
                        }

{run}                   {
                          return eRun;
                        }

{step}                  {
                          return eStep;
                        }

{time}                  {
                          return eTime;
                        }

{trace}                 {
                          return eTrace;
                        }

{trans_post}            {
                          return eTransPost;
                        }

{ident}                 {
                          lexer_value->string = StringUtil::dup(yytext);
                          return eIdent;
                        }

{ident}(\.{ident})+     {
                          lexer_value->string = StringUtil::dup(yytext);
                          return eSignalIdent;                        
			}


\:{ident}               {
                          lexer_value->string = StringUtil::dup(yytext+1);
                          return eAliasIdent;
                        }

{numdec}                {
			  lexer_value->number = strtoul(yytext, NULL, 10);
                          return eNumber;
                        }

{numdec}/{radoct}       {
			  lexer_value->number = strtoul(yytext, NULL, 10);
                          yy_push_state(numoct);
                          return eBitWidth;
                        }


<numoct>{radoct}{numoct} {
                          lexer_value->string = StringUtil::dup(yytext+2);
                          yy_pop_state();
                          return eBitsOct;
                        }

{numdec}/{raddec}       {
			  lexer_value->number = strtoul(yytext, NULL, 10);
                          yy_push_state(numdec);
                          return eBitWidth;
                        }


<numdec>{raddec}{numdec} {
                          lexer_value->string = StringUtil::dup(yytext+2);
                          yy_pop_state();
                          return eBitsDec;
                        }

{numdec}/{radhex}       {
			  lexer_value->number = strtoul(yytext, NULL, 10);
                          yy_push_state(numhex);
                          return eBitWidth;
                        }


<numhex>{radhex}{numhex} {
                          lexer_value->string = StringUtil::dup(yytext+2);
                          yy_pop_state();
                          return eBitsHex;
                        }

.

%%
