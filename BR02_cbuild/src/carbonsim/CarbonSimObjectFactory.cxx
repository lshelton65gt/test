// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "CarbonSimObjectFactory.h"



CarbonSimObjectFactory::CarbonSimObjectFactory(void)
{
}



CarbonSimObjectFactory::~CarbonSimObjectFactory(void)
{
  for (CarbonSimObjectFactory::ObjectTypeMap::UnsortedLoop i = mObjectTypes.loopUnsorted(); not i.atEnd(); ++i)
  {
    CarbonSimObjectType * ot = i.getValue();
    delete ot;
  }
}



CarbonSimObjectType * CarbonSimObjectFactory::findObjectType(const char * type_name) const
{
  ObjectTypeMap::const_iterator i = mObjectTypes.find(UtString(type_name));
  return (i != mObjectTypes.end()) ?
    (*i).second : static_cast<CarbonSimObjectType*>(NULL);
}



CarbonSimObjectType * CarbonSimObjectFactory::addObjectType(const char * type_name, CarbonSimObjectType::InstanceCreationFunction creation_function,
							    CarbonDBType default_db_type, CarbonInitFlags default_init_flags)
{
  INFO_ASSERT(type_name, "CarbonSimObjectFactory::addObjectType: type_name cannot be NULL.");
  INFO_ASSERT(creation_function, "CarbonSimObjectFactory::addObjectType: creation_function cannot be NULL.");

  CarbonSimObjectType * type_handle = findObjectType(type_name);
  INFO_ASSERT(type_handle == 0, "CarbonSimObjectFactory::addObjectType: attempt to add duplicate object type.");

  type_handle = new CarbonSimObjectType(type_name, creation_function, default_db_type, default_init_flags);
  mObjectTypes[UtString(type_name)] = type_handle;

  return type_handle;
}



CarbonSimObjectInstance * CarbonSimObjectFactory::createNewInstance(CarbonSimObjectType * object_type, const char * instance_name) const
{
  INFO_ASSERT(object_type, "CarbonSimObjectFactory::createNewInstance: object_type cannot be NULL.");
  return createNewInstance(object_type, instance_name, object_type->getDefaultDBType(), object_type->getDefaultInitFlags());
}



CarbonSimObjectInstance * CarbonSimObjectFactory::createNewInstance(CarbonSimObjectType * object_type, const char * instance_name, CarbonDBType db_type, CarbonInitFlags init_flags) const
{
  INFO_ASSERT(object_type, "CarbonSimObjectFactory::createNewInstance: object_type cannot be NULL.");
  return new CarbonSimObjectInstance (instance_name, object_type, db_type, init_flags);
}
