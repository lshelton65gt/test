// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef __CARBONSIMPARSERPRIV_H_
#define __CARBONSIMPARSERPRIV_H_


#include "util/CarbonTypes.h"
#include "carbonsim/CarbonSimTransactorAdaptor.h"


class CarbonSimNet;
class DynBitVector;
class CarbonSimDebugger;

#define YYPARSE_PARAM interpreter
#define parser_actions ((static_cast<CarbonSimParser *> (interpreter))->getDebugger())


typedef union
{
  CarbonSimNet * signal;
  CarbonObjectID * model;
  CarbonTime tick;
  UInt32 number;
  DynBitVector * bits;
  CarbonSimTransactorAdaptor::BitVectorArgList * bits_list;
  CarbonSimTransactorAdaptor * transactor;
  char *string;
} yystype;
# define YYSTYPE yystype


extern YYSTYPE CarbonSim_lval;
extern "C" SInt32 CarbonSim_error(const char *s);
extern "C" SInt32 CarbonSim_lex(YYSTYPE *ignore);
int CarbonSim_parse(void *compiler);


#endif
