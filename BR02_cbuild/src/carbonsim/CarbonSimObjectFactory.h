// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimObjectFactory class.
*/
#endif

#ifndef __CARBONSIMOBJECTFACTORY_H_
#define __CARBONSIMOBJECTFACTORY_H_


#include "shell/carbon_shelltypes.h"
#include "carbonsim/CarbonSimObjectInstance.h"
#include "carbonsim/CarbonSimObjectType.h"
#include "util/Loop.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"


class CarbonSimObjectFactoryI;


//! CarbonSimObjectFactory class
/*!
*/
class CarbonSimObjectFactory
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  CarbonSimObjectFactory(void);

  //! Destructor.
  virtual ~CarbonSimObjectFactory(void);


  //! Add a new object type.
  CarbonSimObjectType * addObjectType(const char * type_name, CarbonSimObjectType::InstanceCreationFunction creation_function,
				      CarbonDBType default_db_type = eCarbonFullDB, CarbonInitFlags default_init_flags = eCarbon_NoFlags);
 

  //! Find an object type.
  CarbonSimObjectType * findObjectType(const char * type_name) const;

  //! Create a new object instance.
  CarbonSimObjectInstance * createNewInstance(CarbonSimObjectType * object_type, const char * instance_name) const;

  //! Create a new object instance.
  CarbonSimObjectInstance * createNewInstance(CarbonSimObjectType * object_type, const char * instance_name, CarbonDBType db_type, CarbonInitFlags init_flags) const;


 private:
  typedef UtHashMap<UtString, CarbonSimObjectType *> ObjectTypeMap;

 private:
  ObjectTypeMap mObjectTypes;
  
 private:
  CarbonSimObjectFactory(const CarbonSimObjectFactory&);  
  CarbonSimObjectFactory& operator=(const CarbonSimObjectFactory&);
};


#endif
