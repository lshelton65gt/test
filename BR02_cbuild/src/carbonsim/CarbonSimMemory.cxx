// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimMemory.h"
#include "shell/carbon_capi.h"
#include "util/UtString.h"


CarbonSimMemory::CarbonSimMemory(CarbonSimObjectInstance * object, CarbonMemory * memory, const char *name)
  : mObject(object), mMemory(memory), mName (new UtString(name))
{}


CarbonSimMemory::~CarbonSimMemory(void)
{
  delete mName;
}


SInt64 CarbonSimMemory::getLowAddress(void) const
{
  SInt64 leftAddr = carbonGetLeftAddr(mMemory);
  SInt64 rightAddr = carbonGetRightAddr(mMemory);
  return ((leftAddr < rightAddr) ? leftAddr : rightAddr);
}


SInt64 CarbonSimMemory::getHighAddress(void) const
{
  SInt64 leftAddr = carbonGetLeftAddr(mMemory);
  SInt64 rightAddr = carbonGetRightAddr(mMemory);
  return ((leftAddr > rightAddr) ? leftAddr : rightAddr);
}


UInt32 CarbonSimMemory::getRowBitWidth (void) const
{
  return carbonMemoryRowWidth(mMemory);
}


UInt32 CarbonSimMemory::getRowNumUInt32s (void) const
{
  return carbonMemoryRowNumUInt32s(mMemory);
}


SInt32 CarbonSimMemory::getRowLSB (void) const
{
  return carbonGetMemoryRowLSB(mMemory);
}


SInt32 CarbonSimMemory::getRowMSB (void) const
{
  return carbonGetMemoryRowMSB(mMemory);
}


CarbonStatus CarbonSimMemory::deposit (SInt64 address, const UInt32 *buf)
{
  return carbonDepositMemory(mMemory, address, buf);
}


CarbonStatus CarbonSimMemory::examine (SInt64 address, UInt32 * buf) const
{
  return carbonExamineMemory(mMemory, address, buf);
}


CarbonStatus CarbonSimMemory::format(char * buff, int len, CarbonRadix radix, SInt64 address) const
{
  return carbonFormatMemory (mMemory, buff, len, radix, address);
}
 

CarbonStatus CarbonSimMemory::readmemh(const char * filename)
{
  return carbonReadmemh(mMemory, filename);
}
 

CarbonStatus CarbonSimMemory::readmemb(const char * filename)
{
  return carbonReadmemb(mMemory, filename);
}

const char* CarbonSimMemory::getName()
  const
{
  return mName->c_str();
}
