/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

%{

#include <stdlib.h>
#include <stdio.h>
#include "CarbonSimParserPriv.h"
#include "carbonsim/CarbonSimDebugger.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbonsim/CarbonSimMaster.h"
#include "carbonsim/CarbonSimParser.h"
#include "carbonsim/CarbonSimTransactorAdaptor.h"
#include "util/DynBitVector.h"
#include "util/UtConv.h"
#include "util/UtVector.h"
#include "util/UtStringUtil.h"


#define DEBUGGING 0
#if DEBUGGING
#define debug(x) printf("parse: %s\n", (x))
#else
#define debug(x)
#endif


static DynBitVector * convert_to_bitvector(const char * buf, SInt32 bit_width, CarbonRadix radix);


%}


%token  eAlias
%token  <string>eAliasIdent
%token  eBreakpoint
%token  eContinue
%token  eEnd
%token  <string>eIdent
%token  <number>eBitWidth
%token  <number>eNumber
%token  <string>eBitsDec
%token  <string>eBitsHex
%token  <string>eBitsOct
%token  ePeek
%token  ePoke
%token  eQuit
%token  eRun
%token  <string>eSignalIdent
%token  eStep
%token  eTime
%token  eTrace
%token  eTransPost

%type <signal>alias
%type <bits>bits
%type <bits_list>bits_list
%type <signal>signal
%type <transactor>transactor

%pure_parser
%name-prefix="CarbonSim_"

%%


debugger_commands:
        alias_command
      | breakpoint_command
      | continue_command
      | empty_command
      | peek_command
      | poke_command
      | quit_command
      | run_command
      | step_command
      | time_command
      | trace_command
      | transactor_post_command
        ;


alias_command:
        eAlias eAliasIdent signal eEnd
        {
          debug("alias");
	  parser_actions->doAddAlias($2, $3);
	  YYACCEPT;
        }
        ;


breakpoint_command:
        eBreakpoint signal bits eEnd
        {
          debug("breakpoint (signal)");

	  CarbonSimNet * signal = $2;
	  DynBitVector * bits = $3;

	  parser_actions->doAddBreakpoint(signal, bits->getUIntArray());

	  // delete signal;  ??? Why does the cause a crash??
	  delete bits;

	  YYACCEPT;
        }
      | eBreakpoint alias bits eEnd
        {
          debug("breakpoint (alias)");

	  CarbonSimNet * signal = $2;
	  DynBitVector * bits = $3;

	  parser_actions->doAddBreakpoint(signal, bits->getUIntArray());

	  // delete signal;  ??? Why does the cause a crash ???
	  delete bits;

	  YYACCEPT;
        }
        ;


continue_command:
        eContinue eEnd
        {
          debug("continue");
	  parser_actions->doContinue();
	  YYACCEPT;
        }
        ;


empty_command:
        eEnd
        {
          debug("empty");
	  YYACCEPT;
        }
        ;


peek_command:
        ePeek signal eEnd
        {
          debug("peek (signal)");
	  parser_actions->doPeek($2);
	  //delete $2;
	  YYACCEPT;
        }
      | ePeek alias eEnd
        {
          debug("peek (alias)");
	  parser_actions->doPeek($2);
	  //delete $2;
	  YYACCEPT;
        }
        ;


poke_command:
        ePoke signal bits eEnd
        {
          debug("poke (signal)");

	  CarbonSimNet * signal = $2;
	  DynBitVector * bits = $3;

	  parser_actions->doPoke(signal, bits->getUIntArray());

	  // delete signal;  ??? Why does the cause a crash??
	  delete bits;

	  YYACCEPT;
        }
      | ePoke alias bits eEnd
        {
          debug("poke (alias)");

	  CarbonSimNet * signal = $2;
	  DynBitVector * bits = $3;

	  parser_actions->doPoke(signal, bits->getUIntArray());

	  // delete signal;  ??? Why does the cause a crash??
	  delete bits;

	  YYACCEPT;
        }
        ;


quit_command:
        eQuit eEnd
        {
          debug("quit");
	  parser_actions->doQuit();
	  YYACCEPT;
        }
        ;


run_command:
        eRun eEnd
        {
          debug("run (1)");
	  parser_actions->doRun();
	  YYACCEPT;
        }

      | eRun eNumber eEnd
        {
          debug("run (2)");
	  parser_actions->doRun($2);
	  YYACCEPT;
        }
        ;


step_command:
        eStep eEnd
        {
          debug("step");
	  parser_actions->doStep();
	  YYACCEPT;
        }
        ;


time_command:
        eTime eEnd
        {
          debug("time");
	  parser_actions->doShowTick();
	  YYACCEPT;
        }
        ;


trace_command:
        eTrace signal eEnd
        {
          debug("trace (signal)");
	  parser_actions->doAddTrace($2);
	  //delete $2;
	  YYACCEPT;
        }
      | eTrace alias eEnd
        {
          debug("trace (alias)");
	  parser_actions->doAddTrace($2);
	  //delete $2;
	  YYACCEPT;
        }
        ;


transactor_post_command:
        eTransPost transactor eIdent bits_list eEnd
        { 
          debug("trans_post");

	  UtString command($3);
	  CarbonSimTransactorAdaptor::BitVectorArgList * bits_list = $4;

	  bool result = $2->dispatch(&command, bits_list);

	  for (CarbonSimTransactorAdaptor::BitVectorArgListLoop i = CarbonSimTransactorAdaptor::BitVectorArgListLoop(*bits_list); not i.atEnd(); ++i)
	  {
	    delete *i;
	  }
	  delete bits_list;
	  StringUtil::free($3);

	  if (result)
	  {
	    YYACCEPT;
	  }
	  else
	  {
	    YYERROR;
	  }
        }
        ;


alias:
        eAliasIdent
        {
          debug("alias ident");

	  CarbonSimNet * signal = parser_actions->getSimMaster()->findNetByAlias($1);

	  if (signal != 0)
	  {
	    $$ = signal;
	    StringUtil::free($1);
	  }
	  else
	  {
	    printf("Could not find the alias \'%s\'.\n", $1);
	    StringUtil::free($1);
	    YYERROR;
	  }
        }
        ;


bits:
        eBitWidth eBitsDec
        {
          debug("bits (dec)");
	  StringUtil::free($2);
	  YYERROR;
        }
      | eBitWidth eBitsHex
        {
          debug("bits (hex)");

	  DynBitVector * bits = convert_to_bitvector($2, $1, eCarbonHex);
	  StringUtil::free($2);

	  if (not bits)
	  {
	    YYERROR;
	  }

	  $$ = bits;
        }
      | eBitWidth eBitsOct
        {
          debug("bits (oct)");

	  DynBitVector * bits = convert_to_bitvector($2, $1, eCarbonOct);
	  StringUtil::free($2);

	  if (not bits)
	  {
	    YYERROR;
	  }

	  $$ = bits;
        }
        ;


bits_list:
        bits_list bits
        {
	  debug("bits list (recurse)");

	  $1->push_back($2);
	  $$ = $1;
        }
      | bits
        {
	  debug("bits list (terminate)");

	  CarbonSimTransactorAdaptor::BitVectorArgList * bits_list = new CarbonSimTransactorAdaptor::BitVectorArgList();

	  bits_list->push_back($1);
	  
	  $$ = bits_list;
        }
        ;


signal:
        eSignalIdent
        {
          debug("signal");

	  CarbonSimNet * signal = parser_actions->getSimMaster()->findNet($1);

	  if (signal != 0)
	  {
	    $$ = signal;
	    StringUtil::free($1);
	  }
	  else
	  {
	    printf("Could not find the signal \'%s\'.\n", $1);
	    StringUtil::free($1);
	    YYERROR;
	  }
        }
        ;


transactor:
        eIdent
        {
          debug("transactor");

	  CarbonSimTransactorAdaptor * transactor = parser_actions->findTransactorAdaptor($1);;

	  if (transactor != 0)
	  {
	    StringUtil::free($1);
	  }
	  else
	  {
	    printf("Could not find the transactor \'%s\'.\n", $1);
	    StringUtil::free($1);
	    YYERROR;
	  }

	  $$ = transactor;
        }
        ;


%%



static DynBitVector * convert_to_bitvector(const char * buf, SInt32 bit_width, CarbonRadix radix)
{
  bool converted = false;
  DynBitVector * bits = new DynBitVector(bit_width);
  UtString * extended_buf = UtConv::ZeroExtendValue(buf, bit_width, radix);
  INFO_ASSERT(extended_buf, "ZeroExtendValue allocation failed.");

  switch (radix)
  {
  case eCarbonHex:
    converted = UtConv::HexStringToUInt32(extended_buf->c_str(), bits->getUIntArray(), NULL, bit_width);
    break;
  default:
    printf("Unsupported radix.\n");
    converted = false;
    break;
  }

  delete extended_buf;

  if (!converted)
  {
    printf("Could not convert the value \'%s\'.\n", buf);
    delete bits;
    bits = 0;
  }


  return bits;
}

