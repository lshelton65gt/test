// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef __CARBONSIMMASTERI_H_
#define __CARBONSIMMASTERI_H_


#include "CarbonSimObjectFactory.h"
#include "util/Loop.h"
#include "util/UtHashMap.h"
#include "util/UtSubject.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/DynBitVector.h"

//class CarbonObject;
class CarbonSimFunctionGen;
class CarbonSimMaster;
class CarbonSimMemory;
class CarbonSimNet;
class CarbonSimObjectInstance;
class CarbonSimTransactor;
class CarbonSimWire;
class UtStringArray;

class CarbonSimMasterI
{
public: CARBONMEM_OVERRIDES
  CarbonSimMasterI(void);
  ~CarbonSimMasterI(void);

  CarbonSimObjectInstance * findInstance(UtStringArray * name_comps, UtStringArray::iterator * last_name_cmp);


  typedef UtArray<CarbonSimFunctionGen *> FunctionGenList;
  typedef Loop<FunctionGenList> FunctionGenListLoop;
  typedef UtArray<CarbonSimWire *> WireList;
  typedef Loop<WireList> WireListLoop;
  typedef UtArray<CarbonSimTransactor *> TransactorList;
  typedef Loop<TransactorList> TransactorListLoop;
  typedef UtHashMap<UtString, CarbonSimNet *> AliasSet;
  typedef UtHashMap<UtString, CarbonSimNet *> NetCache;
  typedef UtHashMap<UtString, CarbonSimWire *> WireCache;
  typedef UtHashMap<UtString, CarbonSimMemory *> MemoryCache;
  typedef UtHashMap<UtString, CarbonSimObjectInstance *> InstanceSet; // For lookup
  typedef UtArray<CarbonSimObjectInstance *> InstanceList; // For iteration

  FunctionGenList mFunctionGens;
  WireList mWires;
  TransactorList mTransactors;
  AliasSet mAliases;
  NetCache mNetCache;
  NetCache mAllocatedNetCache;
  MemoryCache mMemoryCache;
  UtSubject<CarbonSimMaster> mFinishedSubject;
  CarbonSimObjectFactory mCarbonObjectFactory;
  InstanceSet mInstances;
  InstanceList mInstanceList;
  WireCache mWireCache;
  DynBitVector mTransactorTally;
};


#endif
