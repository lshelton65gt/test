// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimStep.h"
#include "carbonsim/CarbonSimNet.h"
#include "util/CarbonTypes.h"

CarbonSimStep::CarbonSimStep(CarbonTime offset, UInt32 initial_value)
  : CarbonSimFunctionGen(offset, initial_value)
{
}


CarbonSimStep::~CarbonSimStep(void)
{
}


UInt32 CarbonSimStep::getValue(CarbonTime tick)
{
  UInt32 value;

  if (tick < mOffset)
  {
    value = mInitialValue;
  }
  else
  {
    value = !mInitialValue;
  }

  return value;
}


bool CarbonSimStep::hasNextTransitionTick(void)
{
  return (mCurrentTick < mOffset);
}


CarbonTime CarbonSimStep::getNextTransitionTick(void)
{
  CarbonTime transition_tick = 0;

  if (mCurrentTick < mOffset)
  {
    transition_tick = mOffset;
  }
  else
  { 
    transition_tick = UtUINT64_MAX;
  }

  return transition_tick;
}
