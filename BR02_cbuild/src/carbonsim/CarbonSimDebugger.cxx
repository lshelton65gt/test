// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimDebugger.h"
#include "carbonsim/CarbonSimBreakpoint.h"
#include "carbonsim/CarbonSimMaster.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbonsim/CarbonSimTrace.h"
#include "util/CarbonTypes.h"
#include "util/Util.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtArray.h"
#include "util/Loop.h"
#include "util/UtIOStream.h"
#include <stdio.h>


typedef UtArray<CarbonSimBreakpoint *> BreakpointList;
typedef Loop<BreakpointList> BreakpointListLoop;
typedef UtArray<CarbonSimTrace *> TraceList;
typedef Loop<TraceList> TraceListLoop;
typedef UtHashMap<UtString, CarbonSimTransactorAdaptor*> TransactorAdaptorSet;
typedef Loop<TransactorAdaptorSet> TransactorAdaptorSetLoop;

class CarbonSimDebuggerI
{
public: CARBONMEM_OVERRIDES
  BreakpointList mBreakpoints;
  TraceList mTraces;
  TransactorAdaptorSet mTransactorAdaptors;
};

CarbonSimDebugger::CarbonSimDebugger(CarbonSimMaster * master)
  : mMaster(master), mBreakpointIDCount(0), mTraceIDCount(0), mRunning(false)
{
  mPrivate = new CarbonSimDebuggerI;
}


CarbonSimDebugger::~CarbonSimDebugger(void)
{
  for (BreakpointListLoop i = BreakpointListLoop(mPrivate->mBreakpoints);
       not i.atEnd(); ++i)
  {
    CarbonSimBreakpoint * breakpoint = *i;
    delete breakpoint;
  }
  delete mPrivate;
}


void CarbonSimDebugger::addTransactorAdaptor(const char * adaptor_name, CarbonSimTransactorAdaptor * adaptor)
{
  INFO_ASSERT(adaptor_name, "CarbonSimDebugger::addTransactorAdaptor: adaptor_name must be non-NULL.");
  INFO_ASSERT(adaptor, "CarbonSimDebugger::addTransactorAdaptor: adaptor must be non-NULL.");

  if (findTransactorAdaptor(adaptor_name) == 0)
  {
    mPrivate->mTransactorAdaptors[UtString(adaptor_name)] = adaptor;
  }    
}


CarbonSimTransactorAdaptor * CarbonSimDebugger::findTransactorAdaptor(const char * adaptor_name) const
{
  TransactorAdaptorSet::const_iterator m =
    mPrivate->mTransactorAdaptors.find(UtString(adaptor_name));
  return (m != mPrivate->mTransactorAdaptors.end()) ?
    (*m).second : static_cast<CarbonSimTransactorAdaptor*>(NULL);
}


void CarbonSimDebugger::doQuit(void)
{
  stop();
}


void CarbonSimDebugger::doStep(void)
{
  mMaster->step();
}


void CarbonSimDebugger::doRun(void)
{
  mMaster->run();
}


void CarbonSimDebugger::doRun(CarbonTime end_tick)
{
  mMaster->run(end_tick);
}


void CarbonSimDebugger::doContinue(void)
{
  mMaster->run();
}


void CarbonSimDebugger::doPeek(CarbonSimNet * signal)
{
  SInt32 num_bytes = (signal->getNumUInt32s() * 4 * 2) + 1;
  char * b = CARBON_ALLOC_VEC(char, num_bytes);

  signal->format(b, num_bytes, eCarbonHex);
  UtIO::cout() << b << UtIO::endl;
  //printf("%s\n", b);

  CARBON_FREE_VEC(b, char, num_bytes);
}


void CarbonSimDebugger::doPoke(CarbonSimNet * signal, UInt32 *new_value)
{
  signal->deposit(new_value);
  doPeek(signal);
}


void CarbonSimDebugger::doAddBreakpoint(CarbonSimNet * signal, UInt32 *break_value)
{
  CarbonSimBreakpoint * breakpoint;

  breakpoint = new CarbonSimBreakpoint(mMaster, signal, break_value, ++mBreakpointIDCount);
  mPrivate->mBreakpoints.push_back(breakpoint);
}


void CarbonSimDebugger::doAddTrace(CarbonSimNet * signal)
{
  CarbonSimTrace * trace;

  trace = new CarbonSimTrace(mMaster, signal, ++mTraceIDCount);
  mPrivate->mTraces.push_back(trace);
}


void CarbonSimDebugger::doAddAlias(const char * alias, CarbonSimNet * signal)
{
  INFO_ASSERT(alias, "CarbonSimDebugger::doAddAlias: alias must be non-NULL.");
  INFO_ASSERT(signal, "CarbonSimDebugger::doAddAlias: signal must be non-NULL.");
  mMaster->addAlias(alias, signal);
}


void CarbonSimDebugger::doShowTick(void)
{
  UtIO::cout() << "Tick: " << mMaster->getTick() << UtIO::endl;
  //printf("Tick: %lld\n", mMaster->getTick());
}

