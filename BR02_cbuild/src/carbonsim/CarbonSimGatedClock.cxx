// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimGatedClock.h"
#include "util/CarbonTypes.h"


CarbonSimGatedClock::CarbonSimGatedClock(CarbonTime period, CarbonTime duty_cycle, UInt32 initial_value, CarbonTime offset)
  : CarbonSimClock(period, duty_cycle, initial_value, offset), mEnabled(true)
{
}


CarbonSimGatedClock::~CarbonSimGatedClock(void)
{
}


CarbonTime CarbonSimGatedClock::getNextTransitionTick(void)
{
  return mEnabled ? CarbonSimClock::getNextTransitionTick() : mCurrentTick;
}
