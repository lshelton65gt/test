// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimWrappedNet.h"
#include "shell/carbon_capi.h"
#include "shell/carbon_dbapi.h"
#include "util/UtConv.h"
#include "util/UtString.h"
#include "util/UtSubject.h"


class CarbonSimWrappedNetI
{
public: CARBONMEM_OVERRIDES
  CarbonSimWrappedNetI(const char* name): mName(name) {}
  UtString mName;
  UtSubject<CarbonSimNet> mValueSubject;
};


CarbonSimWrappedNet::CarbonSimWrappedNet(CarbonSimObjectInstance * object, CarbonNet * net, const char *name)
  : CarbonSimNet(), mObject(object), mNet(net)
{
  mPrivate = new CarbonSimWrappedNetI(name);
  mPrivate->mValueSubject.putSubject(this);
  mCallback = 0;

  UInt32 num_words = carbonGetNumUInt32s(mNet);
  mDrive = CARBON_ALLOC_VEC(UInt32, num_words);
}


CarbonSimWrappedNet::~CarbonSimWrappedNet(void)
{
  if (mCallback)
    carbonDisableNetCB(mObject->getCarbonObject(), mCallback);
  UInt32 num_words = carbonGetNumUInt32s(mNet);
  carbonFreeHandle(mObject->getCarbonObject(), &mNet);
  delete mPrivate;
  CARBON_FREE_VEC(mDrive, UInt32, num_words);
}


SInt32 CarbonSimWrappedNet::getNumUInt32s(void) const
{
  return SInt32(carbonGetNumUInt32s(mNet));
}


SInt32 CarbonSimWrappedNet::getBitWidth(void) const
{
  CarbonObjectID* obj = mObject->getCarbonObject();
  CarbonDB* db = carbonGetDB(obj);
  const CarbonDBNode* node = carbonNetGetDBNode(obj, mNet);
  return SInt32(carbonDBGetWidth(db, node));
}


SInt32 CarbonSimWrappedNet::getLSB(void) const
{
  CarbonObjectID* obj = mObject->getCarbonObject();
  CarbonDB* db = carbonGetDB(obj);
  const CarbonDBNode* node = carbonNetGetDBNode(obj, mNet);
  return carbonDBGetLSB(db, node);
}


SInt32 CarbonSimWrappedNet::getMSB(void) const
{
  CarbonObjectID* obj = mObject->getCarbonObject();
  CarbonDB* db = carbonGetDB(obj);
  const CarbonDBNode* node = carbonNetGetDBNode(obj, mNet);
  return carbonDBGetMSB(db, node);
}


bool CarbonSimWrappedNet::isScalar(void) const
{
  CarbonObjectID* obj = mObject->getCarbonObject();
  CarbonDB* db = carbonGetDB(obj);
  const CarbonDBNode* node = carbonNetGetDBNode(obj, mNet);
  return carbonDBIsScalar(db, node);
}


bool CarbonSimWrappedNet::isVector(void) const
{
  CarbonObjectID* obj = mObject->getCarbonObject();
  CarbonDB* db = carbonGetDB(obj);
  const CarbonDBNode* node = carbonNetGetDBNode(obj, mNet);
  return carbonDBIsVector(db, node);
}


bool CarbonSimWrappedNet::isInput(void) const
{
  return carbonIsInput(mObject->getCarbonObject(), mNet);
}


bool CarbonSimWrappedNet::isOutput(void) const
{
  return carbonIsOutput(mObject->getCarbonObject(), mNet);
}


bool CarbonSimWrappedNet::isBidirect(void) const
{
  return carbonIsBidirect(mObject->getCarbonObject(), mNet);
}


bool CarbonSimWrappedNet::isTristate(void) const
{
  return carbonIsTristate(mNet);
}

bool CarbonSimWrappedNet::isDepositable() const
{
  return carbonIsDepositable(mObject->getCarbonObject(), mNet);
}


bool CarbonSimWrappedNet::isDriving(void) const
{
  bool driving = false;
  UInt32 num_words = carbonGetNumUInt32s(mNet);

  CarbonValRW::setToZero(mDrive, num_words);

  INFO_ASSERT(carbonExamine(mObject->getCarbonObject(), mNet, 0, mDrive) == eCarbon_OK,
              "CarbonSimWrappedNet::isDriving: carbonExamine failed.");

  for (UInt32 i = 0; i < num_words; i++)
  {
    if (mDrive[i] == 0x00000000)
    {
      driving = true;
      break;
    }
  }

  return driving;
}


CarbonStatus CarbonSimWrappedNet::deposit(const UInt32* buf, const UInt32* drive)
{
  return carbonDeposit(mObject->getCarbonObject(), mNet, buf, drive);
}


CarbonStatus CarbonSimWrappedNet::examine(UInt32* value, UInt32* drive) const
{  
  return carbonExamine(mObject->getCarbonObject(), mNet, value, drive);
}


CarbonStatus CarbonSimWrappedNet::force(const UInt32 * value)
{
  return carbonForce(mObject->getCarbonObject(), mNet, value);
}


CarbonStatus CarbonSimWrappedNet::release(void)
{  
  return carbonRelease(mObject->getCarbonObject(), mNet);
}



CarbonStatus CarbonSimWrappedNet::format(char* value, int len, CarbonRadix radix) const
{
  return carbonFormat(mObject->getCarbonObject(), mNet, value, len, radix);
}


void CarbonSimWrappedNet::addNotifyOnValueChange(UtObserver<CarbonSimNet> * observer)
{
  if (!mCallback)	// Register net change callback if none is active
    mCallback = carbonAddNetValueChangeCB(mObject->getCarbonObject(), CarbonSimWrappedNet::valueChange, static_cast<CarbonClientData>(this), mNet);
  INFO_ASSERT(observer, "CarbonSimWrappedNet::addNotifyOnValueChange: cannot register NULL for value change notify");
  mPrivate->mValueSubject.attach(observer);
}


void CarbonSimWrappedNet::removeNotifyOnValueChange(UtObserver<CarbonSimNet> * observer)
{
  INFO_ASSERT(observer, "CarbonSimWrappedNet::removeNotifyOnValueChange: cannot unregister NULL for value change notify");
  mPrivate->mValueSubject.detach(observer);
  if (!(mPrivate->mValueSubject.isObserved())) {
    // Remove change callback if there are no more observers
    carbonDisableNetCB(mObject->getCarbonObject(), mCallback);
    mCallback = 0;
  }
}


void CarbonSimWrappedNet::notifyValueChange(void)
{
  mPrivate->mValueSubject.notify();
}


void CarbonSimWrappedNet::valueChange(CarbonObject * /* object */, CarbonNet * /* net*/, CarbonClientData signal, UInt32* /* value */, UInt32* /* drive */)
{
  static_cast<CarbonSimNet *>(signal)->notifyValueChange();
}


const char* CarbonSimWrappedNet::getName() const
{
  return mPrivate->mName.c_str();
}

const CarbonSimWrappedNet* CarbonSimWrappedNet::castWrappedNet() const
{
  return this;
}
