// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "carbonsim/CarbonSimWrappedNet.h"
#include "carbonsim/CarbonSimTransactor.h"
#include "shell/carbon_capi.h"
#include "util/Loop.h"
#include "util/UtQueue.h"
#include "util/CarbonAssert.h"
//#include "util/UtIOStream.h"

class CarbonSimTransaction;

typedef UtQueue<CarbonSimTransaction *> TransactionList;

class CarbonSimTransactorI
{
public: CARBONMEM_OVERRIDES
  TransactionList mTransactions;
};

CarbonSimTransactorI* CarbonSimTransactorCreate() {
  return new CarbonSimTransactorI;
}
void CarbonSimTransactorDestroy(CarbonSimTransactorI* csti) {
  delete csti;
}
void CarbonSimTransactorSubmit(CarbonSimTransactorI* csti,
                               CarbonSimTransaction* transaction)
{
  INFO_ASSERT(transaction, "CarbonSimTransactorSubmit: transaction cannot be NULL.");
  //UtIO::cout() << "push  transaction: " << transaction << UtIO::endl;
  csti->mTransactions.push_back(transaction);
}
SInt32 CarbonSimTransactorNumTransactions(CarbonSimTransactorI* csti) {
  return csti->mTransactions.size();
}
CarbonSimTransaction* CarbonSimTransactorGetFirstTransaction(CarbonSimTransactorI* csti) {
  CarbonSimTransaction* t = csti->mTransactions.front();
  //UtIO::cout() << "front transaction: " << t << UtIO::endl;
  return t;
}
void CarbonSimTransactorPopFirstTransaction(CarbonSimTransactorI* csti) {
  //UtIO::cout() << "pop   transaction\n";
  csti->mTransactions.pop_front();
}  

void CarbonSimTransactor::putId(SInt32 id)
{
  INFO_ASSERT(mId == -1, "CarbonSimTransactor::putId: transactor already has a valid ID.");
  mId = id;
}
