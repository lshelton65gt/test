// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2003-2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "carbonsim/CarbonSimNet.h"
#include "util/CarbonTypes.h"

CarbonSimNet::CarbonSimNet(void)
{
}

CarbonSimNet::~CarbonSimNet(void)
{
}

const CarbonSimWrappedNet* CarbonSimNet::castWrappedNet() const
{
  return NULL;
}

const CarbonSimAllocatedNet* CarbonSimNet::castAllocatedNet() const
{
  return NULL;
}
