// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "CarbonSimOneToOneSocketWire.h"
#include "carbonsim/CarbonSimNet.h"
#include "util/CarbonTypes.h"
#include "util/UtIOStream.h"
#include "util/OSWrapper.h"
#include "util/CarbonAssert.h"

// For Linux & Solaris
// Solaris needs -lresolv on the link line.
#if pfUNIX
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <poll.h>
#elif pfWINDOWS
#include <winsock2.h>
#define socklen_t int
#endif

#include <fcntl.h>
#include <errno.h>
#include <string.h>

CarbonSimOneToOneSocketWire::CarbonSimOneToOneSocketWire(CarbonSimNet *src, UInt32 port, UInt32 buffer_size)
   : CarbonSimOneToOneWire(src,src)
   , mSrc(src)
   , mDst(0)
   , mPort(port)
   , mSocketBufferEntries(buffer_size)
{

  init();

  // Structure with some socket info
  sockaddr_in addr;
  sockaddr_in client_addr;
  socklen_t slen;

  /*
  ** Lookup signals.
  */
  INFO_ASSERT(mSrc, "CarbonSimOneToOneSocketWire::CarbonSimOneToOneSocketWire: source net cannot be NULL");
  allocBuffers(mSrc);
  mDriveDataSize = mSrc->getNumUInt32s() * 4;  // Data size in bytes

  /*
  ** Alloc the buffer.
  */
  mSocketBuffer = (char*) carbonmem_malloc(mDriveDataSize * mSocketBufferEntries);
  memset(static_cast<void *>(mSocketBuffer), 0,  mDriveDataSize * mSocketBufferEntries);
  mSocketBufferIdx = mSocketBufferEntries - 1;  // so we can call deposit() at the end of this function and have it transmit

  // Open the socket.
  // By convention, the sender will act as the server
  memset(&addr, 0, sizeof(sockaddr_in));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = INADDR_ANY;	// uses the current machine's IP
  addr.sin_port = htons(mPort);

  // Create the socket
  mSockFD = socket(AF_INET, SOCK_STREAM, 0 /* tcp */);
  UtString errBuf;
  if (mSockFD == -1)
     UtIO::cerr() << "CSOTOSW: Error creating socket: " << OSGetLastErrmsg(&errBuf) << UtIO::endl;

  if (bind(mSockFD, reinterpret_cast<sockaddr *>(&addr), sizeof(sockaddr_in)) == -1)
     UtIO::cerr() << "CSOTOSW: Error on bind: " << OSGetLastErrmsg(&errBuf) << UtIO::endl;
  listen(mSockFD, 1);	// There is only one client connecting

  // Block until the client connects
  slen = sizeof(sockaddr_in);
  mMsgFD = accept(mSockFD, reinterpret_cast<sockaddr *>(&client_addr), &slen);
  if (mMsgFD == -1)
     UtIO::cerr() << "CSOTOSW: Error on accept: " << OSGetLastErrmsg(&errBuf) << UtIO::endl;

  // Send dummy data, because the wire flow needs to read before it can write
  depositSocket();

}

CarbonSimOneToOneSocketWire::CarbonSimOneToOneSocketWire(const char *ipaddr, UInt32 port, CarbonSimNet *dst, UInt32 buffer_size)
   : CarbonSimOneToOneWire(dst,dst)
   , mSrc(0)
   , mDst(dst)
   , mPort(port)
   , mSocketBufferEntries(buffer_size)
{

  init();

  sockaddr_in addr;

  const int max_retries = 50;
  int retries = 0;
  int stat = 0;

  /*
  ** Lookup signals.
  */
  INFO_ASSERT(mDst, "CarbonSimOneToOneSocketWire::CarbonSimOneToOneSocketWire: destination net cannot be NULL.");
  allocBuffers(mDst);
  mDriveDataSize = mDst->getNumUInt32s() * 4;  // Data size in bytes

  /*
  ** Alloc the buffer.
  */
  mSocketBuffer = (char*) carbonmem_malloc(mDriveDataSize * mSocketBufferEntries);
  memset(static_cast<void *>(mSocketBuffer), 0,  mDriveDataSize * mSocketBufferEntries);
  mSocketBufferIdx = 0;

  // Connect to the socket.
  // By convention, the receiver will act as the client
  memset(&addr, 0, sizeof(sockaddr_in));
  addr.sin_family = AF_INET;
  //  memcpy(&addr.sin_addr.s_addr, hostip, 4);
#if pfUNIX
  inet_aton(ipaddr, &addr.sin_addr);
#elif pfWINDOWS
  addr.sin_addr.s_addr = inet_addr(ipaddr);
#endif

  addr.sin_port = htons(mPort);

  mMsgFD = socket(AF_INET, SOCK_STREAM, 0 /* tcp */);
  if (mMsgFD == -1) {
    UtString errBuf;
     UtIO::cerr() << "CSOTOSW: Unable to create socket: " 
                  << OSGetLastErrmsg(&errBuf) << UtIO::endl;
     abort();
  }

  // We can't just make one attempt at connecting, because the server may not have opened
  // the socket yet!  (ECONNREFUSED is returned if connecting program has not started yet)
  stat = connect(mMsgFD, reinterpret_cast<sockaddr *>(&addr), sizeof(sockaddr_in));
#if pfWINDOWS
  while ((stat == -1) && (++retries < max_retries) && (WSAGetLastError() == WSAECONNREFUSED)) {
#else
  while ((stat == -1) && (++retries < max_retries) && (errno == ECONNREFUSED)) {
#endif
     OSSleep(1);
     stat = connect(mMsgFD, reinterpret_cast<sockaddr *>(&addr), sizeof(sockaddr_in)); 
  }

  if (stat == -1) {
    UtString errBuf;
     UtIO::cerr() << "CSOTOSW: Connect failed after " << retries << " attempts: " << OSGetLastErrmsg(&errBuf) << UtIO::endl;
  }
  // Don't read anything from the socket.  That'll happen when we do a flow()

}


void CarbonSimOneToOneSocketWire::init()
{
  mSockFD = -1;
  mMsgFD = -1;
  if (mSocketBufferEntries == 0)
     mSocketBufferEntries = 1;
}

CarbonSimOneToOneSocketWire::~CarbonSimOneToOneSocketWire(void)
{
  if (mMsgFD != -1)
    closeSocket(mMsgFD);
  if (mSockFD != -1)
    closeSocket(mSockFD);
  if (mSocketBuffer) carbonmem_free(mSocketBuffer);
}


void CarbonSimOneToOneSocketWire::print(bool /* verbose */, UInt32 /* indent */)
{
}


void CarbonSimOneToOneSocketWire::flow(void)
{
  if (mSrc)
    mSrc->examine(mDriveData, mDriveDrive);
  else
    examineSocket();

  if (mDst)
    mDst->deposit(mDriveData);
  else
    depositSocket();
}


bool CarbonSimOneToOneSocketWire::examineSocket()
{

  UInt32 drvDataSize = mDriveDataSize * mSocketBufferEntries;
  int readStat;
  UtString err;

  if (mSocketBufferIdx == 0) {
     readStat = OSSysRead(mMsgFD, mSocketBuffer, drvDataSize, &err);
	// On Linux the socket does block, 0 is only returned if the socket
	// is really closed.
     if (readStat == 0) {
	UtIO::cerr() << "CSOTOSW: Error reading from socket, connection closed." << UtIO::endl;
	return false;         // Socket connection closed
     }
     // No return status???? flow() doesn't care about status. BAD! BAD!
     if (readStat == -1) {
	UtIO::cerr() << "CSOTOSW: Error reading socket: " << err << UtIO::endl;
	return false;
     } else {
	UInt32 dat_rcv = readStat;
	if(dat_rcv != drvDataSize) {
	   UtIO::cerr() << "CSOTOSW: Error reading socket: Bytes received lower that expected, received " << dat_rcv << UtIO::endl;
	}
     }
  }

  memcpy(mDriveData, &(mSocketBuffer[mSocketBufferIdx*mDriveDataSize]), mDriveDataSize);
  if (++mSocketBufferIdx == mSocketBufferEntries)
     mSocketBufferIdx = 0;

  return true;
}


bool CarbonSimOneToOneSocketWire::depositSocket()
{
  int result;
  UInt32 drvDataSize = mDriveDataSize * mSocketBufferEntries;
  char* driveDataBuf = mSocketBuffer;

  memcpy(&(mSocketBuffer[mSocketBufferIdx*mDriveDataSize]), mDriveData, mDriveDataSize);

  if (++mSocketBufferIdx == mSocketBufferEntries) {
     mSocketBufferIdx = 0;

     do {
	result = write(mMsgFD, driveDataBuf, drvDataSize);
	if (result > 0) {
	   driveDataBuf += result;
	   drvDataSize -= result;
	}
	else if ((result == -1) && (errno != EINTR))
	   drvDataSize = 0;
     } while (drvDataSize > 0); 

     if (result == -1) {
       UtString errBuf;
	UtIO::cerr() << "CSOTOSW: Error writing socket: " 
                     << OSGetLastErrmsg(&errBuf) << UtIO::endl;
	return false;
     }
  }

  return true;
}

int CarbonSimOneToOneSocketWire::closeSocket(int sd)
{
#if pfWINDOWS
  return closesocket(sd);
#else
  return OSSysClose(sd, NULL);
#endif
}

CarbonSimOneToOneWire* carbonSimOneToOneSocketWireOutCreate(CarbonSimNet* src, UInt32 port, UInt32 buffer_size)
{
  return new CarbonSimOneToOneSocketWire(src, port, buffer_size);
}

CarbonSimOneToOneWire* carbonSimOneToOneSocketWireInCreate(const char * ipaddr, UInt32 port, CarbonSimNet * dst, UInt32 buffer_size)
{
  return new CarbonSimOneToOneSocketWire(ipaddr, port, dst, buffer_size);
}
