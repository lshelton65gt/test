// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "carbonsim/CarbonSimAllocatedOutputNet.h"



CarbonSimAllocatedOutputNet::CarbonSimAllocatedOutputNet(const char * name, SInt32 msb, SInt32 lsb)
  : CarbonSimAllocatedNet(name, msb, lsb)
{
}



CarbonSimAllocatedOutputNet::~CarbonSimAllocatedOutputNet(void)
{
}

bool CarbonSimAllocatedOutputNet::isInput(void) const { return false; }

bool CarbonSimAllocatedOutputNet::isOutput(void) const { return true; }

bool CarbonSimAllocatedOutputNet::isBidirect(void) const { return false; }

bool CarbonSimAllocatedOutputNet::isTristate(void) const { return false; }


bool CarbonSimAllocatedOutputNet::isDriving(void) const { return true; }

bool CarbonSimAllocatedOutputNet::isDepositable(void) const
{
  return false;
}
