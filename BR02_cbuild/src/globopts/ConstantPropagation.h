// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _CONSTANTPROPAGATION_H_
#define _CONSTANTPROPAGATION_H_

class NUDesign;
class NUNetRefFactory;
class FLNodeElabFactory;
class IODBNucleus;
class STSymbolTable;
class DynBitVector;
class ExprResynth;
class RETieNets;
class Fold;
class REAlias;
class RENetWarning;

#include "exprsynth/ESPopulateExpr.h"
#include "exprsynth/ExprCallbacks.h"

//! GOConstantPropagation class
/*! This class manages the process of running constant propagation on
 *  a design.  It is a global optimization in that it visits the
 *  elaborated view of a design but then it applies the optimizations
 *  to the modules locally.
 */
class GOConstantPropagation
{
public:
  //! constructor
  GOConstantPropagation(NUDesign* design, NUNetRefFactory* netRefFactory,
                        FLNodeElabFactory* flowElabFactory,
                        IODBNucleus* iodbNucleus, STSymbolTable* symbolTable,
                        REAlias* mAlias, 
                        ArgProc* args, MsgContext* msgContext,
                        AtomicCache* cache, Stats* stats,
                        bool undrivenConstants, SInt32 verbose,
                        RENetWarning*);
  
  //! destructor
  ~GOConstantPropagation();

  //! Run the optimization on the given design.
  /*!
   *! Returns 'true' if anything was optimized
   */
  bool run();

private:
  // Hide copy and assign constructors.
  GOConstantPropagation(const GOConstantPropagation&);
  GOConstantPropagation& operator=(const GOConstantPropagation&);

  // Private types for passing information between the private functions.
  typedef UtMap<NUUseDefNode*, NUNetReplacementMap> DefReplacements;
  typedef LoopMap<DefReplacements> DefReplacementsLoop;
  typedef UtSet<NUPortConnection*> Ports;
  typedef Loop<Ports> PortsLoop;
  typedef UtMap<NUPortConnection*, DynBitVector*> ConstPorts;

  // Private types for managing constant values. There can be three
  // types of constants stored in our data, non-constants, constants,
  // and inactive logic.
  enum ConstType {
    eConstUninit,       //!< Hasn't been computed yet
    eConstNone,         //!< Not a constant
    eConstValue,        //!< The flow node represents a constant value
    eConstInactive,     //!< The flow node represents an inactive statement
    eConstPartial,      //!< Some of the bits are constant
    eConstZ             //!< It drives z or is undriven
  };
  class ConstValue
  {
  public:
    //! normal constructor
    ConstValue(ConstType type, DynBitVector* value) :
      mConstType(type), mValue(value), mMask(NULL)
    {}

    //! Partial const value constructor
    ConstValue(DynBitVector* value, DynBitVector* mask) :
      mConstType(eConstPartial), mValue(value), mMask(mask)
    {}

    //! Empty constructor
    ConstValue() : mConstType(eConstNone), mValue(NULL), mMask(NULL) {}

    //! destructor
    virtual ~ConstValue() {}

    //! Test if this is a full constant
    bool isConstant(void) const { return mConstType == eConstValue; }

    //! Test if this is a partial constant
    bool isPartialConstant(void) const { return mConstType == eConstPartial; }

    //! Test if this is an inactive device (e.g. flop with constant clock)
    bool isInactive(void) const { return mConstType == eConstInactive; }

    //! Test if this is a non-constant
    bool isNonConstant(void) const { return mConstType == eConstNone; }

    //! Test if we have not yet determined if this is a constant or not
    bool isUnknown(void) const { return mConstType == eConstUninit; }

    //! Test if this is an undriven constant
    bool isUndriven(void) const { return mConstType == eConstZ; }

    //! Get the constant value (only valid for constant values)
    DynBitVector* getValue() const
    {
      INFO_ASSERT(mConstType == eConstValue,
                  "Get value of non constant type during constant propagation");
      return mValue;
    }

    //! Get the value and mask if it is a partial constant
    void getPartialValue(DynBitVector** value, DynBitVector** mask) const
    {
      INFO_ASSERT(mConstType == eConstPartial,
                  "Get partial const value called on invalid type");
      *value = mValue;
      *mask = mMask;
    }

    //! Compare const values
    virtual bool operator==(const ConstValue& other) const
    {
      if (mConstType != other.mConstType) {
        return false;
      }
      bool result = false;
      switch (mConstType) {
        case eConstValue:
        {  
          INFO_ASSERT((mValue != NULL) && (other.mValue != NULL),
                      "Corrupted constant during constant proapgation");
          result = (*mValue == *other.mValue);
        }
        break;

        case eConstPartial:
        {
          INFO_ASSERT((mValue != NULL) && (other.mValue != NULL) &&
                      (mMask != NULL) && (other.mMask != NULL),
                      "Corrupted partial constant during constant proapgation");
          result = (*mValue == *other.mValue) && (*mMask == *other.mMask);
        }
        break;

        case eConstZ:
          result = true;

        case eConstUninit:
        case eConstNone:
        case eConstInactive:
        {
          INFO_ASSERT((mValue == NULL) && (other.mValue == NULL),
                      "Corrupted non-constant during constant propation");
          result = true;
        }
      } // switch
      return result;
    }

    //! Not sure why we need both an == and != operator
    bool operator!=(const ConstValue& other) const
    {
      return !(*this == other);
    }

  protected:
    ConstType           mConstType;
    DynBitVector*       mValue;
    DynBitVector*       mMask;
  };

  // Private functions to perform the optimization
  bool hasMultiplyDrivenBits(NUNetElab* netElab, FLNodeElab* flow,
                             const ConstValue& constValue);
  void markElaboratedConstants();
  void recordConstant(FLNodeElab* flowElab, FLNode* flow, DynBitVector* value);
  void markUnelaboratedConstants();
  void markConstantPortConnections();
  void optimizeDesign();
  NUExpr* createConst(DynBitVector*, bool, UInt32, const SourceLocator&);
  bool qualifyAlwaysBlock(NUNet* net, NUUseDefNode* ud);
  void optimizeConstantNets();
  void optimizeUseDef(FLNode*, ConstValue, DefReplacements*);
  void createConstContAssign(NUModule*, const NUNetRefHdl&, DynBitVector*);
  void optimizeConstantAssign(FLNode* flow, DynBitVector* value);
  void replaceDefs(DefReplacements& defReplacements);
  void applyLocalConstants() const;

  // Utility functions
  bool isPortConn(FLNodeElab* flowElab);
  bool isActiveDriver(FLNodeElab* flow) const;
  bool isValidDriver(FLNodeElab* flow, NUNetElab*) const;
  bool isSequential(FLNodeElab* flow) const;
  bool isInitial(FLNodeElab* flow) const;
  bool isLocallyUndriven(FLNodeElab* flow) const;
  bool invalidLvaluesRecurse(FLNodeElab* flow) const;
  bool invalidLvalues(FLNodeElab* flow) const;
  bool canPropagateConst(FLNodeElab* flow) const;
  bool getTieValue(FLNodeElab* flowElab, DynBitVector& tieValue);
  void resizeConstant(const NUNetRefHdl& constNetRef,
                      const NUNetRefHdl& subNetRef,
                      const DynBitVector& value,
                      DynBitVector* subValue) const;
  DynBitVector* isGloballyUndriven(FLNodeElab* flowElab);
  bool isLocallyUndriven(NUNetElab* netElab, FLNodeElabSet* initFlows);
  bool undrivenAliases(NUNetElab* netElab, FLNodeElabSet* initFlows);
  void gatherOptimizedPortFlows(Ports&, FLNodeVector* flows);
  bool isConstPort(DynBitVector** value, ConstPorts& constPorts,
                   NUPortConnection* port) const;
  bool isInputPort(FLNode* flow) const;
  ConstValue extractConstant(const NUExpr* expr, UInt32 size);
  ConstValue extractPartialConstant(const NUExpr* expr, UInt32 size);

  // Types and functions for doing a depth first search of the design
  typedef UtSet<const FLNodeElab*> Covered;
  typedef UtVector<FLNodeElab*> Stack;
  typedef std::pair<Covered, Stack> FlowStack;
  void depthFirstSearch(FLNodeElab*, Covered&, FlowStack&);
  void addSpecialNets(FlowStack& flowStack);
  void addNet(NUNetElab* netElab, FlowStack& flowStack);
  void addFlow(FLNodeElab* flowElab, FlowStack& flowStack);
  FLNodeElab* nextFlow(FlowStack& flowStack);
  void sortFlowStack(FlowStack* flowStack);

  // Types, data, and functions for determining constants
  typedef UtMap<FLNodeElab*, ConstValue> ConstCache;
  typedef LoopMap<ConstCache> ConstCacheLoop;
  ConstCache* mConstCache;
  ConstCache* mInitialCache;
  void markConstant(FLNodeElab* flow);
  ConstValue computeConstant(FLNodeElab*);
  ConstValue getConstant(FLNodeElab*) const;
  ConstValue getConstant(FLNodeElabVector* flowElabs);
  ConstValue getConstantValue(FLNodeElab* flowElab) const;
  void addCachedConst(ConstCache* constCache, FLNodeElab* flow,
                      ConstValue constValue);
  void addInactiveFlow(ConstCache* constCache, FLNodeElab* flow);
  ConstValue findInCache(ConstCache* constCache, FLNodeElab* flow) const;
  void clearConstCache(ConstCache* constCache);

  // Types and data for storing unelaborated constants
  typedef UtHashMap<FLNode*, ConstValue> UnelabConsts;
  typedef UnelabConsts::UnsortedLoop UnelabConstsUnsortedLoop;
  typedef UnelabConsts::SortedLoop UnelabConstsSortedLoop;
  UnelabConsts* mUnelabConsts;

  // Types and data to remember which useDefs we have optimized into a
  // continous assign and the temp we created to replace all LHS of
  // the inactive always blocks.
  typedef UtMap<const NUNetRefHdl, NUNet*> OptimizedUseDefs;
  OptimizedUseDefs* mOptimizedUseDefs;

  // Data to remember what flops we have tried to mark as
  // inactive. Since flops can have multiple drivers, we will visit it
  // multiple times, but we really only have to mark them once since
  // we do all the work in that call.
  FLNodeElabSet* mCoveredFlops;

  // Data to remember elaborated flows that are considered locally
  // undriven.
  typedef UtMap<FLNodeElab*, bool> LocallyUndriven;
  LocallyUndriven* mLocallyUndriven;

  // Type for sorting flops according to their priority
  typedef UtMap<NUUseDefNode*, int> BlockPriority;

  // local struct to sort flows according to always block priority
  struct SortFlops
  {
    SortFlops(const BlockPriority& blockPriority) :
      mBlockPriority(blockPriority)
    {}
    bool operator()(const FLNodeElab* f1, const FLNodeElab* f2) const
    {
      // Get the priority for the first flow
      NUUseDefNode* useDef1 = f1->getUseDefNode();
      BlockPriority::const_iterator pos = mBlockPriority.find(useDef1);
      FLN_ELAB_ASSERT(pos != mBlockPriority.end(), f1);
      int priority1 = pos->second;

      // And the second
      NUUseDefNode* useDef2 = f2->getUseDefNode();
      pos = mBlockPriority.find(useDef2);
      FLN_ELAB_ASSERT(pos != mBlockPriority.end(), f2);
      int priority2 = pos->second;

      // The lower number means it is lower priority. We sort high to
      // low
      return priority1 > priority2;
    }
    const BlockPriority& mBlockPriority;
  }; // struct SortFlops

  // Data and Functions to test if a net elab is mutable
  typedef UtMap<NUNetElab*, bool> MutableNets;
  mutable MutableNets* mMutableNets;
  bool isMutable(NUNetElab* netElab) const;

  // Types and Data to keep track of undriven nets and their init flow
  typedef std::pair<bool, FLNodeElabVector*> UndrivenAndInitFlows;
  typedef UtMap<NUNetElab*, UndrivenAndInitFlows> UndrivenNets;
  UndrivenNets* mUndrivenNets;

  // Functions to mark inactive flops as constants
  ConstType gatherOverlappingDrivers(FLNodeElab*, FLNodeElabSet*,
                                     FLNodeElabSet*) const;
  NUNet* getClock(FLNode* flow) const;
  DynBitVector* propagateConstantClocks(FLNodeElab*, const FLNodeElabSet&,
                                        const FLNodeElabSet&, Covered&,
                                        FlowStack&);
  void markConstantFlops(FLNodeElab*, Covered&, FlowStack&);
  void sortPriorityFlops(FLNodeElab*, const FLNodeElabSet&, FLNodeElabVector*);
  FLNodeElab* findHighestPriorityActiveFlop(const FLNodeElabVector&,
                                            DynBitVector**,
                                            Covered&, FlowStack&);
  DynBitVector* getConstantFlopData(FLNodeElab* thisFlop,
                                    const FLNodeElabSet& flops,
                                    const FLNodeElabSet& initials);
  DynBitVector* getInitialValue(FLNodeElab*, const FLNodeElabSet&);
  DynBitVector* getConstantExpr(FLNodeElab*, const NUNetRefHdl&);
  NUNetElab* getFlopClock(FLNodeElab* flop, ClockEdge* edge) const;
  NUAlwaysBlock* getClockBlock(FLNodeElab* flop) const;
  NUAlwaysBlock* getAlwaysBlock(FLNode* flow) const;
  NUAlwaysBlock* getAlwaysBlock(FLNodeElab* flowElab) const;

  //! Callback class for expression synthesis
  class SynthCallback : public ESCachedCallbackElab
  {
  public:
    //! constructor
    SynthCallback(GOConstantPropagation* constProp, FLNodeElab* startFlow,
		  ESPopulateExpr* populateExpr) :
      ESCachedCallbackElab(populateExpr), mConstantPropagation(constProp),
      mStartFlow(startFlow), mOtherFlowSeen(false), mHasConstantLeaf(false)
    {}

    //! destructor
    ~SynthCallback() {}

    //! Routine to indicate when to stop translation
    bool stop(FLNodeElabVector* flows, const NUNetRefHdl& ref, bool isContDriver);

    //! Routine to create the identity expression for a flow node
    ESExprHndl createIdentExpr(FLNodeElabVector* flowElabs, UInt32 bitSize,
			       bool isSigned, bool hardStop);

    //! Check if we ever saw a constant leaf in the expression
    bool hasConstantLeaf(void) const { return mHasConstantLeaf; }

  private:
    // Hide copy and assign constructors.
    SynthCallback(const SynthCallback&);
    SynthCallback& operator=(const SynthCallback&);

    GOConstantPropagation* mConstantPropagation;
    FLNodeElab* mStartFlow;
    bool mOtherFlowSeen;
    bool mHasConstantLeaf;
  };
  friend class SynthCallback;

  // Global flags
  SInt32 mVerbose;
  bool mUndrivenConstants;

  //! Should we print summary statistics about local propagation?
  bool mVerboseLocalPropagation;

  //! was any modification done to the nucleus structure?
  bool mModified;

  // Classes to find and optimize constants
  ESPopulateExpr* mPopulateExpr;
  ExprResynth* mExprResynth;
  Fold* mFold;
  RETieNets* mTieNets;

  // Utility classes passed in during construction
  NUDesign* mDesign;
  NUNetRefFactory* mNetRefFactory;
  FLNodeElabFactory* mFlowElabFactory;
  IODBNucleus* mIODBNucleus;
  STSymbolTable* mSymbolTable;
  REAlias* mAlias;
  Stats* mStats;
  AtomicCache* mAtomicCache;
  MsgContext* mMsgContext;
  RENetWarning* mNetWarning;
}; // class GOConstantPropagation

#endif // _CONSTANTPROPAGATION_H_
