// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "util/Stats.h"
#include "util/DynBitVector.h"
#include "util/StringAtom.h"

#include "iodb/IODBNucleus.h"

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUInstanceWalker.h"
#include "nucleus/NUSysTaskNet.h"
#include "nucleus/NUControlFlowNet.h"
#include "nucleus/NUDesignWalkerCallbacks.h"

#include "flow/FLFactory.h"
#include "flow/FLIter.h"

#include "reduce/TieNets.h"
#include "reduce/RewriteUtil.h"
#include "reduce/Fold.h"
#include "reduce/REAlias.h"
#include "reduce/RENetWarning.h"
#include "reduce/LocalConstProp.h"

#include "exprsynth/Expr.h"
#include "exprsynth/ExprFactory.h"
#include "exprsynth/ESPopulateExpr.h"
#include "exprsynth/ExprResynth.h"

#include "compiler_driver/CarbonContext.h" // for CRYPT

#include "ConstantPropagation.h"

GOConstantPropagation::GOConstantPropagation(NUDesign* design,
                                             NUNetRefFactory* netRefFactory,
                                             FLNodeElabFactory* factory,
                                             IODBNucleus* iodbNucleus,
                                             STSymbolTable* symbolTable,
                                             REAlias* alias,
                                             ArgProc* args,
                                             MsgContext* msgContext,
                                             AtomicCache* cache,
                                             Stats* stats,
                                             bool undrivenConstants,
                                             SInt32 verbose,
                                             RENetWarning* netWarning) :
  mVerbose(verbose), mUndrivenConstants(undrivenConstants), mDesign(design),
  mNetRefFactory(netRefFactory), mFlowElabFactory(factory),
  mIODBNucleus(iodbNucleus), mSymbolTable(symbolTable), mAlias(alias),
  mStats(stats), mAtomicCache(cache), mMsgContext(msgContext),
  mNetWarning(netWarning)
{
  mModified = false;
  mConstCache = new ConstCache;
  mInitialCache = new ConstCache;
  mUnelabConsts = new UnelabConsts;
  ESFactory* exprFactory = new ESFactory;
  mPopulateExpr = new ESPopulateExpr(exprFactory, netRefFactory);
  mExprResynth = new ExprResynth(ExprResynth::eStopAtState, mPopulateExpr,
                                 netRefFactory, args, msgContext, cache,
                                 iodbNucleus);
  bool verbose_fold = args->getBoolValue(CRYPT("-verboseFold"));
  mFold = new Fold(netRefFactory, args, msgContext, cache, iodbNucleus, 
                   verbose_fold, eFoldUDKiller);
  mTieNets = new RETieNets(mDesign, mAtomicCache, mIODBNucleus, msgContext,
                           mFold, verbose, false, NULL);
  mCoveredFlops = new FLNodeElabSet;
  mOptimizedUseDefs = new OptimizedUseDefs;
  mLocallyUndriven = new LocallyUndriven;
  mMutableNets = new MutableNets;
  mUndrivenNets = new UndrivenNets;
  mVerboseLocalPropagation = args->getBoolValue(CRYPT("-verboseLocalPropagation"));
}

void GOConstantPropagation::clearConstCache(ConstCache* constCache)
{
  for (ConstCacheLoop l(*constCache); !l.atEnd(); ++l) {
    ConstValue constValue = l.getValue();
    if (constValue.isConstant()) {
      DynBitVector* value = constValue.getValue();
      delete value;
    } else if (constValue.isPartialConstant()) {
      DynBitVector* value;
      DynBitVector* mask;
      constValue.getPartialValue(&value, &mask);
      delete value;
      delete mask;
    }
  }
}

GOConstantPropagation::~GOConstantPropagation()
{
  clearConstCache(mConstCache);
  delete mConstCache;
  clearConstCache(mInitialCache);
  delete mInitialCache;
  delete mUnelabConsts;
  delete mExprResynth;
  delete mFold;
  delete mPopulateExpr->getFactory();
  delete mPopulateExpr;
  delete mTieNets;
  delete mCoveredFlops;
  delete mOptimizedUseDefs;
  delete mLocallyUndriven;
  delete mMutableNets;
  for (UndrivenNets::iterator i = mUndrivenNets->begin();
       i != mUndrivenNets->end(); ++i) {
    FLNodeElabVector* nodes = i->second.second;
    delete nodes;
  }
  delete mUndrivenNets;
}

bool GOConstantPropagation::run()
{
  // Start a new timer so we can see the sub pieces of this phase
  mStats->pushIntervalTimer();

  // Visit all combinational elaborated flow nodes in the design using
  // depth first search and test if it is a constant. If so, add it to
  // the known constants set. This results in a map from elaborated
  // flow nodes to constants.
  markElaboratedConstants();
  mStats->printIntervalStatistics("ElabConsts");

  // Determine if all instances of an elaborated flow node have the
  // same constant value. If so, mark it for propagation. This results
  // in a map of unelaborated flow nodes to constants.
  markUnelaboratedConstants();
  mStats->printIntervalStatistics("UnelabConsts");

  // Find all the port connections for instances of a module where the
  // constant is the same. We have to do this because there are
  // different unelaborated flow nodes for every port connection
  // instance. This results in a map from port nets to constants for
  // every module.
  markConstantPortConnections();
  mStats->printIntervalStatistics("PortConsts");

  // TBD: Uniquify blocks/modules if it helps propagate constants

  // Do a nucleus design walk and modify the nucleus statements.
  optimizeDesign();
  mStats->printIntervalStatistics("OptConsts");
  mStats->popIntervalTimer();

  return mModified;
}

void GOConstantPropagation::markElaboratedConstants()
{
  // Find all the design output ports to start the depth first search
  FlowStack flowStack;
  for (NUDesign::PortLoop p = mDesign->loopOutputPorts(); !p.atEnd(); ++p)
  {
    NUNet* net = *p;
    NUModuleElab* mod = net->getScope()->getModule()->lookupElab(mSymbolTable);
    NUNetElab* netElab = net->lookupElab(mod->getHier());
    addNet(netElab, flowStack);
  }
  for (NUDesign::PortLoop p = mDesign->loopBidPorts(); !p.atEnd(); ++p)
  {
    NUNet* net = *p;
    NUModuleElab* mod = net->getScope()->getModule()->lookupElab(mSymbolTable);
    NUNetElab* netElab = net->lookupElab(mod->getHier());
    addNet(netElab, flowStack);
  }
  for (IODB::NameSetLoop p = mIODBNucleus->loopObserved(); !p.atEnd(); ++p)
  {
    // The IODB uses the persistent symbol table. But we elaborated
    // using the global optimizations temporary symbol table. So we
    // have to map from the IODB symbol table node to the one in the
    // temporary symbol table so that we can find the elaborated net.
    STSymbolTableNode* origNode = *p;
    STSymbolTableNode* newNode = mSymbolTable->lookup(origNode);
    NUNetElab* netElab = mIODBNucleus->getNet(newNode);
    addNet(netElab, flowStack);
  }

  // Find all the special observable nets. Today this includes the
  // output systask net and the control flow net. They are used to
  // keep some user defined and system tasks (like $display) live in
  // the design.
  //
  // We want to add any net here that must be used to determine if
  // logic is live.
  addSpecialNets(flowStack);

  // Sort the flow stack so that we get predictable results
  sortFlowStack(&flowStack);

  // Do a depth first search of the design. State devices will be
  // added to the flow stack.
  Covered covered;
  FLNodeElab* flow;
  while ((flow = nextFlow(flowStack)) != NULL)
    depthFirstSearch(flow, covered, flowStack);
} // GOConstantPropagation::markElaboratedConstants

void
GOConstantPropagation::depthFirstSearch(FLNodeElab* flow, Covered& covered,
					FlowStack& flowStack)
{
  // Make sure we don't hit any cycles
  if (covered.find(flow) != covered.end()) {
    return;
  }
  covered.insert(flow);

  // If this is a sequential, we got here from the flow stack. It
  // means we should check if this sequential can be treated as a
  // constant.
  if (isSequential(flow)) {
    markConstantFlops(flow, covered, flowStack);
  }

  // Visit the fanin for this set of flows
  for (Fanin p(mFlowElabFactory, flow, eFlowOverCycles, FLIterFlags::eAll);
       !p.atEnd(); ++p) {
    // For sequentials, add them to the stack to process their level
    // fanin. But also recurse and find constants down the clock
    // pin. This will allow us to mark inactive flops as constants.
    //
    // For combinationals, recurse and mark constants
    FLNodeElab* fanin = *p;
    if (isSequential(fanin)) {
      // Add to the to do list
      addFlow(fanin, flowStack);

      // Mark any inactive flops as constants. This can cause a
      // recursive call to this routine on the clock pin.
      markConstantFlops(fanin, covered, flowStack);
    } else {
      // visit their fanin to propagate and mark constants
      depthFirstSearch(fanin, covered, flowStack);
    }
  }

  markConstant(flow);
} // GOConstantPropagation::depthFirstSearch

void
GOConstantPropagation::addFlow(FLNodeElab* flow, FlowStack& flowStack)
{
  Covered& covered = flowStack.first;
  if (covered.find(flow) == covered.end())
  {
    // Mark it covered
    covered.insert(flow);

    // Only add this to the stack if it has active drivers
    if (isActiveDriver(flow))
      flowStack.second.push_back(flow);
  }
}

bool GOConstantPropagation::isActiveDriver(FLNodeElab* flow) const
{
  NUUseDefNode* useDef = flow->getUseDefNode();
  return ((useDef == NULL) || (useDef->getStrength() != eStrPull));
}

bool GOConstantPropagation::isSequential(FLNodeElab* flow) const
{
  NUUseDefNode* useDef = flow->getUseDefNode();
  return ((useDef != NULL) && useDef->isSequential());
}

bool GOConstantPropagation::isInitial(FLNodeElab* flow) const
{
  NUUseDefNode* useDef = flow->getUseDefNode();
  return ((useDef != NULL) && useDef->isInitial());
}

bool GOConstantPropagation::isLocallyUndriven(FLNodeElab* flowElab) const
{
  // Check the look-aside cache. We use a look-aside cache because we
  // may make things undriven if we see that a potentially complex
  // driver only drives z. In that case we can consider it a
  // non-driver. See computeConstant() for the other writer of
  // mLocallyUndriven.
  //
  // It is also a cache in that the result of this function is added
  // to the look-aside cache.
  LocallyUndriven::iterator pos = mLocallyUndriven->find(flowElab);
  if (pos != mLocallyUndriven->end()) {
    return pos->second;
  }

  // Undriven bound nodes are locally undriven
  bool locallyUndriven = false;
  if (flowElab->getType() == eFLBoundUndriven) {
    locallyUndriven = true;

  } else {

    // Bid ports representing the input flow are locally undriven if
    // they don't have fanin. This does not apply to input ports because
    // they can be driven by constants.
    NUUseDefNode* useDef = flowElab->getUseDefNode();
    if ((useDef != NULL) && useDef->isPortConnBid() &&
        isInputPort(flowElab->getFLNode()) && !flowElab->hasFanin()) {
      locallyUndriven = true;
    }
  }

  // Remember if this is undriven
  (*mLocallyUndriven)[flowElab] = locallyUndriven;
  return locallyUndriven;
}

class GOSpecialNets : public NUInstanceCallback
{
public:
  //! constructor
  GOSpecialNets(STSymbolTable* symbolTable, NUNetElabVector* nets) :
    NUInstanceCallback(symbolTable), mNets(nets)
  {}

protected:
  //! Function to find the sys task/control flow nets for this instance
  virtual void handleModule(STBranchNode* node, NUModule* mod)
  {
    NUNet* sysTaskNet = mod->getOutputSysTaskNet();
    NUNetElab* sysTaskNetElab = sysTaskNet->lookupElab(node, false);
    NU_ASSERT(sysTaskNetElab != NULL, sysTaskNet);
    mNets->push_back(sysTaskNetElab);

    NUNet* controlFlowNet = mod->getControlFlowNet();
    NUNetElab* controlFlowNetElab = controlFlowNet->lookupElab(node, false);
    NU_ASSERT(controlFlowNetElab != NULL, controlFlowNet);
    mNets->push_back(controlFlowNetElab);
  }
  virtual void handleDeclScope(STBranchNode*,NUNamedDeclarationScope*) {}
  virtual void handleInstance(STBranchNode*,NUModuleInstance*) {}
private:
  NUNetElabVector* mNets;
}; // class GOSpecialNets : public NUInstanceCallback
  

void
GOConstantPropagation::addSpecialNets(FlowStack& flowStack)
{
  // Walk the design gathering the system task nets
  NUNetElabVector nets;
  GOSpecialNets findSpecialNets(mSymbolTable, &nets);
  NUDesignWalker walker(findSpecialNets, false, false);
  walker.design(mDesign);

  // Add them to our start list
  for (NUNetElabVectorLoop l(nets); !l.atEnd(); ++l) {
    NUNetElab* netElab = *l;
    addNet(netElab, flowStack);
  }
}

void
GOConstantPropagation::addNet(NUNetElab* netElab, FlowStack& flowStack)
{
  for (NUNetElab::DriverLoop l = netElab->loopContinuousDrivers(); !l.atEnd(); ++l)
  {
    FLNodeElab* flow = *l;
    addFlow(flow, flowStack);
  }
}

FLNodeElab*
GOConstantPropagation::nextFlow(FlowStack& flowStack)
{
  if (flowStack.second.empty())
    return NULL;
  else
  {
    FLNodeElab* flow = flowStack.second.back();
    flowStack.second.pop_back();
    return flow;
  }
}

GOConstantPropagation::ConstValue
GOConstantPropagation::getConstant(FLNodeElabVector* flowElabs)
{
  // Gather the complete net ref for this set of flows. The problem is
  // that if the initial and driving flows are disjoint, we are not
  // sure what to do yet.
  NUNetRefHdl fullNetRef = mNetRefFactory->createEmptyNetRef();
  for (FLNodeElabVectorLoop l(*flowElabs); !l.atEnd() && !fullNetRef->all(); ++l) {
    // Combine the net refs, we can stop when we get to a full net ref
    FLNodeElab* flowElab = *l;
    NUNetRefHdl netRef = flowElab->getDefNetRef(mNetRefFactory);
    fullNetRef = mNetRefFactory->merge(fullNetRef, netRef);
  }

  // Start by gathering the initial and non-initial drivers.
  //
  // There are two scenarios under which this set of elaborated flows
  // can be considered a constant. The first is it is not actively
  // driven. The second is that the drivers of the elaborated net were
  // constant.
  //
  // We consider this set of flowElabs constant if all of them have
  // the same constant value for the bits in question. But there is a
  // caveat.
  //
  // In the undriven case we should enter this code with either a set
  // of initial blocks or an undriven bound node. In the constant case
  // we end up in here with a set of active constant drivers. But it
  // is also possible to have initial blocks on the drivers. Those are
  // not marked as constant unless the net is considered undriven. So
  // we should prune the initial block drivers if the net is driven
  // (the caveat).
  FLNodeElabVector initialFlows;
  FLNodeElabVector drivingFlows;
  for (FLNodeElabVectorLoop l(*flowElabs); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    if (isInitial(flowElab)) {
      initialFlows.push_back(flowElab);
    } else {
      drivingFlows.push_back(flowElab);
    }
  }

  // If there are driving flows, use that to determine the
  // constant. Otherwise use the initial flows.
  FLNodeElabVector* checkFlows;
  if (drivingFlows.empty()) {
    checkFlows = &initialFlows;
  } else {
    checkFlows = &drivingFlows;
  }

  // Determine if all the elaborated flows are the same constant and
  // the same net ref as the full net ref
  bool isConstant = true;
  bool first = true;
  ConstValue constValue(eConstUninit, NULL);
  for (FLNodeElabVectorLoop l(*checkFlows); !l.atEnd() && isConstant; ++l) {
    // Make sure we have the same net ref as the full net ref
    FLNodeElab* flowElab = *l;
    NUNetRefHdl netRef = flowElab->getDefNetRef(mNetRefFactory);
    isConstant = (netRef == fullNetRef);
    if (isConstant) {
      // Get this flows value. Initial drivers are only marked as
      // constants if the elaborated net is otherwise undriven and all
      // initial drivers result in the same constant.
      ConstValue thisConstValue = getConstant(flowElab);

      // Make sure this value matches any other values for all flows. If
      // it is undriven then we can ignore it (and it should not be
      // treated as the first item).
      if (thisConstValue.isNonConstant() || thisConstValue.isUnknown()) {
        // Not a constant or uninitialized. It can be ignored if it is
        // locally undriven. 
        if (!isLocallyUndriven(flowElab)) {
          isConstant = false;
        }

      } else if (first) {
        // First flow in the loop and it is some sort of constant. Store
        // the constant value and net ref to compare against the other
        // flows.
        constValue = thisConstValue;
        first = false;
      } else {
        // Not the first flow, make sure we have the same value
        isConstant = (constValue == thisConstValue);
      }
    } // if
  } // for

  // If we didn't find a driver, then validate that the flows are
  // locally undriven and treat them as z.
  if (isConstant && constValue.isUnknown()) {
    for (FLNodeElabVectorLoop l(*checkFlows); !l.atEnd() && isConstant; ++l) {
      FLNodeElab* flowElab = *l;
      FLN_ELAB_ASSERT(isLocallyUndriven(flowElab), flowElab);
    }
    constValue = ConstValue(eConstZ, NULL);
  }

  // If this is not a constant, clear any stale value
  if (!isConstant) {
    constValue = ConstValue();
  }
  return constValue;
} // GOConstantPropagation::getConstant

GOConstantPropagation::ConstValue
GOConstantPropagation::getConstant(FLNodeElab* flowElab) const
{
  ConstValue constValue = findInCache(mConstCache, flowElab);
  return constValue;
}

void GOConstantPropagation::markConstant(FLNodeElab* flow)
{
  // Check if this is a constant, if so, we don't need to recompute
  // whether it is a constant. We should never store the drives z
  // value. This is a local view only, not a global view. We test
  // globally undriven below.
  ConstValue constValue = getConstantValue(flow);
  FLN_ELAB_ASSERT(!constValue.isUndriven(), flow);
  if (!constValue.isUnknown()) {
    return;
  }

  // If this is a sequential, memory or a non active driver, then we
  // just insert it as a non-constant flow. Otherwise we compute
  // whether it is a constant or not.
  //
  // Note sequentials are done in a different piece of the code
  // because they are so different.
  if (!isSequential(flow) && canPropagateConst(flow)) {
    // Check if this elaborated flow is a locally undriven or an
    // initial block. We do the analysis on all aliases of the
    // elaborated flow. That is because if the locally undriven driver
    // or initial is the only driver, then it can be a constant. If it
    // is normal logic then we can compute its constant with
    // computeConstant that does it locally.
    if (isLocallyUndriven(flow) || isInitial(flow)) {
      DynBitVector* value = isGloballyUndriven(flow);
      if (value != NULL) {
        constValue = ConstValue(eConstValue, value);
      }
    } else if (flow->getUseDefNode() != NULL) {
      constValue = computeConstant(flow);
    }
  }

  // Insert this flow into our cache. It shouldn't exist yet
  addCachedConst(mConstCache, flow, constValue);
} // void GOConstantPropagation::markConstant

GOConstantPropagation::ConstValue
GOConstantPropagation::computeConstant(FLNodeElab* flow)
{
  // Check for the virtual nets. They can have a large number of
  // drivers and today they can't be a constant. So avoid the compile
  // performance problems it introduces.
  if (flow->getDefNet()->getNet()->isVirtualNet()) {
    return ConstValue();
  }

  // Convert the flow node to an expression using our constant cache
  // to replace FLNodeElab's with constants when appropriate.
  //
  // Note, the nested block here. This is because we want to clear the
  // expression cache at the end of this funtion. But we can't do that
  // unless the expression handle has been destructed.
  ConstValue constValue;
  {
    GOConstantPropagation::SynthCallback callback(this, flow, mPopulateExpr);
    ESExprHndl hndl = mPopulateExpr->create(flow, &callback);

    // If we got a valid expression and either the expression had some
    // leaf nodes replaced with constants or the number of operations is
    // less than 100, then check if the expression is a constant.
    //
    // We put in the test for expression size and constant because when
    // expression synthesis was improved to support cases, expression
    // re-synthesis got very expensive. So we are eliminating cases
    // where it is not likely that constants will propagate. The issue
    // was found by the AMCC oc48 test case.
    CarbonExpr* cExpr = hndl.getExpr();
    if ((cExpr != NULL) && 
        (callback.hasConstantLeaf() || !hndl.numOpsGreaterThan(100)) &&
        !hndl.numOpsGreaterThan(10000))
    {
      // Convert the expression back to a nucleus expression so that we
      // can fold it and test for constant.
      const SourceLocator& loc = flow->getUseDefNode()->getLoc();
      CopyContext cc(0,0);
      const NUExpr* expr = mExprResynth->exprToNucleus(cExpr, NULL, loc);

      // Resize the expression to the constant value we want. For this
      // we need to make a copy. Note this should be correctly sized
      // in most cases, but for some reason port connections are not
      // yet correctly sized. If that gets fixed, this code can be
      // removed.
      UInt32 size = flow->getFLNode()->getDefNetRef()->getNumBits();
      NUExpr* exprCopy = expr->copy(cc);
      exprCopy = exprCopy->makeSizeExplicit(size);

      // If the user asked for it, print the details
      if (mVerbose >= 2) {
        UtString buf;
        UtIO::cout() << "Flow: ";
        flow->compose(&buf, NULL, 4, true);
        UtIO::cout() << buf << "\n";

        // Print out the flow and the expression it collapsed to, if the
        // expression is not too big.  But don't print huge expressions,
        // such as the one resulting from test/constprop/crc.v
        NUCostContext costs;
        NUCost cost;
        costs.calcExpr(exprCopy, &cost);
        UtIO::cout() << "Expr: ";
        if ((cost.mAsicGates < 1000) && (cost.mInstructions < 1000)) {
          buf.clear();
          exprCopy->compose(&buf, NULL);
          UtIO::cout() << buf << "\n";
        }
        else
          UtIO::cout() << "exceeded maximum cost\n";
      } // if

      // Currently don't support real numbers. We should support this at
      // some point.
      //
      // Note that we check for z on the originally sized expression
      // because we don't want the 0 extension to mess things up. At
      // some point we need to revamp undriven in constant
      // propagation. It is not consistently handled. Sometimes we
      // treat it like constant 0 and sometimes like undriven.
      if ((expr != NULL) && expr->drivesOnlyZ()) {
        // It's not a constant, but we can treat it as undriven
        (*mLocallyUndriven)[flow] = true;
      } else if (exprCopy->getType() == NUExpr::eNUConstNoXZ) {
        // It is a pure constant, so get the value for our cache
        constValue = extractConstant(exprCopy, size);

      } else if (exprCopy->getType() == NUExpr::eNUConcatOp) {
        // Check if it is a partial constant
        constValue = extractPartialConstant(exprCopy, size);
          
      }
      delete exprCopy;

      // Print the result if it is a constant, partial constant or undriven
      if (mVerbose >= 2) {
        if (constValue.isConstant()) {
          UtIO::cout() << "Result: constant found\n";
        } else if (constValue.isPartialConstant()) {
          UtIO::cout() << "Result: partial constant found\n";
        } else if ((*mLocallyUndriven)[flow]) {
          UtIO::cout() << "Result: z-driven/undriven net found\n";
        }
        UtIO::cout() << "\n";
      }
    } // if
  }

  // We never re-use expressions because we only create expressions
  // between continuous driver flow nodes. So clear the factory
  // expressions cache
  mExprResynth->clearExprCache();
  mPopulateExpr->clearExprCache();
  return constValue;
} // GOConstantPropagation::computeConstant

DynBitVector*
GOConstantPropagation::isGloballyUndriven(FLNodeElab* flowElab)
{
  // Check if this elaborated flow is locally undriven or an initial
  // block. We do the analysis on that elaborated flow. That is
  // because if the locally undriven node or initial is the only
  // driver, then it can be a constant.
  FLN_ELAB_ASSERT(isLocallyUndriven(flowElab)||isInitial(flowElab), flowElab);

  // Check if all the aliases of this net are undriven.
  FLNodeElabSet initialFlows;
  if (!undrivenAliases(flowElab->getDefNet(), &initialFlows)) {
    return NULL;
  }

  // Ok, it must be undriven, if there is an initial block or net
  // flags that indicate a value, compute the value from
  // that. Otherwise, use zero.
  DynBitVector* value = getInitialValue(flowElab, initialFlows);
  return value;
}

bool
GOConstantPropagation::undrivenAliases(NUNetElab* netElab,
                                       FLNodeElabSet* initFlows)
{
  // If undriven constants are disabled, stop now
  if (!mUndrivenConstants) {
    return false;
  }

  // Check the cache first, we use the master to look up the
  // information.
  NUNetElab* master = mAlias->getMaster(netElab);
  UndrivenNets::iterator pos = mUndrivenNets->find(master);
  if (pos != mUndrivenNets->end()) {
    FLNodeElabVector* nodes = pos->second.second;
    initFlows->insert(nodes->begin(), nodes->end());
    return pos->second.first;
  }

  // Check if all the aliases are undriven and non primary
  // ports.
  NUNetElabVector netElabs;
  mAlias->getAliases(netElab, &netElabs);
  bool undriven = true;
  for (NUNetElabVectorLoop l(netElabs); !l.atEnd() && undriven; ++l) {
    NUNetElab* curNetElab = *l;

    // I experimented with considering all primary ports as driven
    // during this analysis. Port analysis would then be free to
    // convert undriven outputs into inouts as needed. There didn't
    // seem to be any side-effect besides a few regolds.

#undef CONSIDER_ALL_PRIMARY_PORTS_DRIVEN
#ifdef CONSIDER_ALL_PRIMARY_PORTS_DRIVEN
    if (curNetElab->isPrimaryPort()) {
      // Treat all primary ports as driven
#else
    NUNet* net = curNetElab->getStorageNet();
    if (net->isPrimaryInput() || net->isPrimaryBid()) {
      // Treat a primary input/bid port as driven
#endif
      undriven = false;
    } else {
      undriven = isLocallyUndriven(curNetElab, initFlows);
    }
#ifdef CONSIDER_ALL_PRIMARY_PORTS_DRIVEN
    }
#endif
  }

  // Store it in the cache and return
  FLNodeElabVector* nodes = new FLNodeElabVector;
  for (FLNodeElabSetCLoop l(*initFlows); !l.atEnd(); ++l) {
    FLNodeElab* initFlow = *l;
    nodes->push_back(initFlow);
  }
  UndrivenNets::value_type value(master, UndrivenAndInitFlows(undriven, nodes));
  mUndrivenNets->insert(value);
  return undriven;
} // GOConstantPropagation::undrivenAliases

bool GOConstantPropagation::isValidDriver(FLNodeElab* flowElab,
                                          NUNetElab* netElab)
  const
{
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  if (useDef == NULL) {
    return true;                // primary input
  }
  else if (!useDef->isInitial()) {
    if (useDef->isContDriver()) {
      return true;
    }
    else {
      bool drives, driverFound;
      mNetWarning->isValidDriver(flowElab, netElab, &drives, &driverFound);
      if (drives && driverFound) {
        return true;
      }
    }
  }
  return false;
}

bool 
GOConstantPropagation::isLocallyUndriven(NUNetElab* netElab,
                                         FLNodeElabSet* initFlows)
{
  // The netElab is driven if there is a continous driver on one of
  // the elaborated flows. We also only look at active drivers (ignore
  // initial and pull drivers).
  bool driven = false;
  for (NUNetElab::DriverLoop l = netElab->loopContinuousDrivers(); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    if (!isLocallyUndriven(flowElab) && isActiveDriver(flowElab) &&
        isValidDriver(flowElab, netElab))
    {
      driven = true;
    }

    // Gather the initial flows
    NUUseDefNode* useDef = flowElab->getUseDefNode();
    if ((useDef != NULL) && useDef->isInitial()) {
      initFlows->insert(flowElab);
    }
  }

  // If the net has any collapse clock directives on it, then we
  // consider it driven.
  if (mIODBNucleus->isCollapseSubordinate(netElab)) {
    driven = true;
  }

  return !driven;
}

bool GOConstantPropagation::isPortConn(FLNodeElab* flowElab)
{
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  return (useDef != NULL) && useDef->isPortConn();
}

// Determine whether any other full-time drivers on the net overlap
// with the bits def'd by the given flow.
bool GOConstantPropagation::hasMultiplyDrivenBits(NUNetElab* netElab,
                                                  FLNodeElab* flow,
                                                  const ConstValue& constValue)
{
  NUNetRefHdl flowBits = flow->getDefNetRef(mNetRefFactory);
  FLNodeElabSet covered;
  NUNetElabVector netElabs;
  mAlias->getAliases(netElab, &netElabs);
  int netElabSize = netElab->getNet()->getBitSize();
  for (Loop<NUNetElabVector> l(netElabs); !l.atEnd(); ++l) {
    NUNetElab* curNetElab = *l;
    for (NUNetElab::DriverLoop p = curNetElab->loopContinuousDrivers();
         !p.atEnd(); ++p) {
      FLNodeElab* driver = *p;
      if ((driver != flow) && !isPortConn(driver) &&
          isValidDriver(driver, curNetElab)) {
        // Test if this is a possible conflict. There are a number of
        // characteristics to check.
        //
        // 1. If the passed in net elab and this drivers net elab
        //    are different sizes, then we give up and assume there is
        //    a conflict
        //
        // 2. If the net sizes are the same and the two driving
        //    flows drive the same bits and constants, this is not
        //    a multiple driver situation
        //
        // 3. If the net sizes are the same and the flow bits
        //    overlap, then it is a multi driver situation
        int curNetElabSize = curNetElab->getNet()->getBitSize();
        NUNetRefHdl driverBits = driver->getDefNetRef(mNetRefFactory);
        if (curNetElabSize != netElabSize) {
          // Net sizes are different, give up
          return true;

        } else if (!flowBits->sameBits(*driverBits) ||
                   (constValue != getConstantValue(driver))) {
          // They are not the same bits or constant, so check for
          // overlap
          if (driverBits->overlapsBits(*flowBits)) {
            // There is overlap, it is a multi driver situation
            return true;
          }
        }
      } // isValidDriver
    } // !p.atEnd
  } // for

  // No overlap found
  return false;
} // bool GOConstantPropagation::hasMultiplyDrivenBits

void GOConstantPropagation::markUnelaboratedConstants()
{
  // Create a map from unelaborated flows to elaborated flows
  typedef UtMap<FLNode*, FLNodeElabSet> AllFlows;
  typedef LoopMap<AllFlows> AllFlowsLoop;
  AllFlows allFlows;
  FLNodeElabFactory::FlowLoop l;
  FLNodeElab* flowElab = NULL;
  for (l = mFlowElabFactory->loopFlows(); l(&flowElab);)
  {
    FLNode* flow = flowElab->getFLNode();
    if (flow != NULL)
    {
      FLNodeElabSet& flowElabs = allFlows[flow];
      flowElabs.insert(flowElab);
    }
  }

  // Gather all the Elaborated flows with constant values, and mark those
  // constants in the IODB (as long as they are not multiply driven).
  AllFlows constFlows;
  for (ConstCacheLoop c(*mConstCache); !c.atEnd(); ++c) {
    ConstValue constValue = c.getValue();
    if (constValue.isConstant() || constValue.isInactive()) {
      FLNodeElab* flowElab = c.getKey();
      FLNodeElabSet initialFlows;
      NUNetElab* netElab = flowElab->getDefNet();
      bool isUndriven = undrivenAliases(netElab, &initialFlows);
      if (!isInitial(flowElab) || isUndriven) {
        FLNode* flow = flowElab->getFLNode();
        FLN_ELAB_ASSERT(flow != NULL, flowElab);
        if (constValue.isConstant() && !isUndriven &&
            !hasMultiplyDrivenBits(netElab, flowElab, constValue)) {
          DynBitVector* value = constValue.getValue();
          recordConstant(flowElab, flow, value);
        }
        FLNodeElabSet& flowElabs = constFlows[flow];
        flowElabs.insert(flowElab);
      }
    }
  } // for

  // Find all the unelaborated flow nodes with the same constant in
  // all instances
  for (AllFlowsLoop f(constFlows); !f.atEnd(); ++f) {
    FLNode* flow = f.getKey();
    const FLNodeElabSet& elabFlows = f.getValue();
    FLNodeElabSet& allElabFlows = allFlows[flow];
    FLN_ASSERT(!allElabFlows.empty(), flow);
    if (allElabFlows.size() == elabFlows.size()) {
      ConstValue value;
      bool allInstConst = true;
      for (FLNodeElabSet::const_iterator i = elabFlows.begin();
	   i != elabFlows.end() && allInstConst;
	   ++i) {
	FLNodeElab* flowElab = *i;
        ConstValue nextValue = getConstantValue(flowElab);
	FLN_ELAB_ASSERT(!nextValue.isNonConstant(), flowElab);
	if (value.isNonConstant()) {
	  value = nextValue;
	} else if (value != nextValue) {
	  allInstConst = false;
        }
      }

      // If this is a valid unelaborated constant, mark it as such
      if (allInstConst) {
	FLN_ASSERT(mUnelabConsts->find(flow) == mUnelabConsts->end(), flow);
	mUnelabConsts->insert(UnelabConsts::value_type(flow, value));
      }
    } // if
  } // for
} // void GOConstantPropagation::markUnelaboratedConstants

// Record constant values we've computed in the IO Database, so that
// we can show dead constants in waveforms.  Note that this is somewhat
// optimistic -- we may compute a forcible net to be constant.  After
// elaboration, forcible net resolution, and flag propagation, we need
// to be sure to remove innappropriate constants from the IODB.  See
// IODBNucleus::cleanupMutableConstants
void GOConstantPropagation::recordConstant(FLNodeElab* flowElab,
                                           FLNode* flow,
                                           DynBitVector* value)
{
  // Mark the constant in the IODB so it gets reflected in waveforms
  NUNetElab* netElab = flowElab->getDefNet();
  NUNet* net = netElab->getStorageNet();
  if (net->is2DAnything()) {
    // do not declare memories to be constant -- the waveform system
    // cannot handle that right now
    return;
  }

  STAliasedLeafNode* node = netElab->getSymNode();

  // This flow-node may only def a subset of the bits of the vector.
  // Let's say the vector is [7:0] and we are now defing bits [5:3]
  // as 101.  We want to generate a mask 00111000, and allbits 00101000.
  // But the value coming out of the flow-node will be 101 -- just 3 bits.
  // So we need to copy those 3 bits into all bits, to get 00000101.
  // Below, we will loop over the ranges, and left-shift the bits to get
  // them into the right position.
  DynBitVector allbits(net->getBitSize(), *value);
  DynBitVector mask(net->getBitSize());

  // We can only handle this if there is exactly one contiguous range.
  bool first = true, valid = false;
  NUNetRefHdl ref = flow->getDefNetRef();
  for (NUNetRefRangeLoop p = flow->getDefNetRef()->loopRanges(mNetRefFactory);
       !p.atEnd(); ++p)
  {
    ConstantRange range = *p;
    if (first) {
      first = false;
      valid = true;

      allbits <<= range.getLsb();
      mask.setRange(range.getLsb(), range.getLength(), 1);
    }
    else {
      valid = false;
      break;
    }
  }

  // Now we have to conform to the declareConstBits interface, which
  // takes a mask and a value -- it's designed so you can delcare bits
  // as X/Z as well, so it's a 4-state enum.  So we can mask off the
  // bits we want and set them to 1, then invert the whole vector, remask,
  // and set the 0-bits.
  if (valid) {
    allbits &= mask;
    mIODBNucleus->declareConstBits(node, allbits, eValue1);
    allbits.flip();
    allbits &= mask;
    mIODBNucleus->declareConstBits(node, allbits, eValue0);
  }
} // void GOConstantPropagation::recordConstant

void GOConstantPropagation::markConstantPortConnections()
{
  // Gather all the constant input/output port connections
  ConstPorts constInputPorts;
  ConstPorts constOutputPorts;
  for (UnelabConstsUnsortedLoop l = mUnelabConsts->loopUnsorted();
       !l.atEnd(); ++l) {
    FLNode* flow = l.getKey();
    ConstValue constValue = l.getValue();
    NUUseDefNode* useDef = flow->getUseDefNode();
    if ((useDef != NULL) && constValue.isConstant() &&
	((useDef->getType() == eNUPortConnectionInput) ||
	 (useDef->getType() == eNUPortConnectionOutput) ||
         (useDef->getType() == eNUPortConnectionBid))) {
      // Get the port and constant value
      NUPortConnection* port;
      port = dynamic_cast<NUPortConnection*>(useDef);
      DynBitVector* value = constValue.getValue();

      // Put the port and constant value in the appropriate set,
      // inputs in inputs, outputs in outputs, and bids into the
      // direction they represent. The flow indicates the direction.
      if (isInputPort(flow)) {
        FLN_ASSERT(constInputPorts.find(port) == constInputPorts.end(), flow);
        constInputPorts.insert(ConstPorts::value_type(port, value));
      } else {
        FLN_ASSERT(constOutputPorts.find(port) == constOutputPorts.end(),flow);
        constOutputPorts.insert(ConstPorts::value_type(port, value));
      }
    }
  }

  // Gather all the module instances in the design
  NUDesign::ModuleInstances moduleInstances;
  mDesign->findModuleInstances(&moduleInstances);

  // Visit all the modules in the design and gather all the  port
  // connections.
  typedef UtMap<NUNet*, Ports> NetPorts;
  typedef LoopMap<NetPorts> NetPortsLoop;
  typedef UtMap<NUModule*, NetPorts> ModulePorts;
  typedef LoopMap<ModulePorts> ModulePortsLoop;
  ModulePorts modulePorts;
  for (NUDesign::ModuleInstancesLoop m(moduleInstances); !m.atEnd(); ++m)
  {
    NUModule* module = m.getKey();
    NUDesign::Instances* instances = m.getValue();
    NetPorts& netPorts = modulePorts[module];
    for (NUDesign::InstancesLoop i(*instances); !i.atEnd(); ++i)
    {
      NUModuleInstance* moduleInstance = *i;
      NUPortConnectionLoop p;
      for (p = moduleInstance->loopPortConnections(); !p.atEnd(); ++p)
      {
	NUPortConnection* port = *p;
	NUNet* net = port->getFormal();
	Ports& ports = netPorts[net];
	ports.insert(port);
      }
    }
  }

  // All done with the module instances memory
  for (NUDesign::ModuleInstancesLoop m(moduleInstances); !m.atEnd(); ++m)
  {
    NUDesign::Instances* instances = m.getValue();
    delete instances;
  }

  // Visit the complete set of  port connections for each net and
  // check if they are all constants. If so, we can modify the port
  // connections.
  //
  // TBD: This code does not handle wacky ports!!
  Ports optimizedPorts;
  for (ModulePortsLoop m(modulePorts); !m.atEnd(); ++m)
  {
    NUModule* module = m.getKey();
    NetPorts& netPorts = const_cast<NetPorts&>(m.getValue());
    for (NetPortsLoop n(netPorts); !n.atEnd(); ++n)
    {
      // Check if all instances of this net's ports are constants
      NUNet* net = n.getKey();
      Ports& ports = const_cast<Ports&>(n.getValue());
      bool allConstPort = true;
      DynBitVector* value = NULL;
      for (PortsLoop p(ports); !p.atEnd() && allConstPort; ++p)
      {
        // For inputs check the constant input ports, for outputs,
        // check the constant output ports, for bids check both.
	NUPortConnection* port = *p;
        if (port->isPortConnInput() || port->isPortConnBid()) {
          if (!isConstPort(&value, constInputPorts, port)) {
            allConstPort = false;
          }
        }
        if (port->isPortConnOutput() || port->isPortConnBid()) {
          if (!isConstPort(&value, constOutputPorts, port)) {
            allConstPort = false;
          }
        }
      }

      // If they are all constants, store that fact.
      if (allConstPort) {
	mTieNets->addModuleNet(module, net, RETieNets::NetValue(value));
        optimizedPorts.insert(ports.begin(), ports.end());
      }
    } // for
  } // for

  // Gather all the flow nodes that should not be optimized further
  // and delete them. These are all the input port flows (we can only
  // optimize them if all instances are the same constant) and any
  // output port flows where we optimized it here (not need to
  // optimize per instance).
  FLNodeVector deleteFlows;
  gatherOptimizedPortFlows(optimizedPorts, &deleteFlows);

  // Clean up the map, remove the fully optimized ports
  for (FLNodeVectorIter i = deleteFlows.begin(); i != deleteFlows.end(); ++i) {
    FLNode* flow = *i;
    mUnelabConsts->erase(flow);
  }
} // void GOConstantPropagation::markConstantPortConnections

bool
GOConstantPropagation::isConstPort(DynBitVector** value,
                                   ConstPorts& constPorts,
                                   NUPortConnection* port) const
{
  bool allConstPort = true;
  ConstPorts::iterator pos = constPorts.find(port);
  if (pos == constPorts.end()) {
    // Not a constant, give up
    allConstPort = false;

  } else {
    // make sure the values are the same
    DynBitVector* nextValue = pos->second;
    if (*value == NULL) {
      // First port, just store the constant
      *value = nextValue;

    } else if (**value != *nextValue) {
      // Differring port values for multiple instances
      allConstPort = false;
    }
  }
  return allConstPort;
} // GOConstantPropagation::isConstPort

void 
GOConstantPropagation::gatherOptimizedPortFlows(Ports& optimizedPorts,
                                                FLNodeVector* flows)
{
  for (UnelabConstsUnsortedLoop l = mUnelabConsts->loopUnsorted();
       !l.atEnd(); ++l) {
    FLNode* flow = l.getKey();
    ConstValue value = l.getValue();
    NUUseDefNode* useDef = flow->getUseDefNode();
    if ((useDef != NULL) && value.isConstant() &&
	((useDef->getType() == eNUPortConnectionInput) ||
	 (useDef->getType() == eNUPortConnectionOutput) ||
         (useDef->getType() == eNUPortConnectionBid))) {
      // We clean up input ports and any bid ports that represent the
      // input flow since we can only optimize the formal if all
      // instances are the same constant. We don't do this for outputs
      // and bids that represent the outputs because we can still
      // optimize the output net ref. But if all the output and
      // bid/output ports got optimized then there is nothing left to
      // do so remove them.
      //
      // We can tell the difference between a bid input vs output port
      // because a bid input port's def net is the same as the formal.
      bool isInput = isInputPort(flow);

      // Clean up input ports and outputs that have been optimized
      NUPortConnection* port;
      port = dynamic_cast<NUPortConnection*>(useDef);
      if (isInput) {
        flows->push_back(flow);
      } else if (optimizedPorts.find(port) != optimizedPorts.end()) {
        flows->push_back(flow);
      }
    }
  } // for
} // void GOConstantPropagation::gatherOptimizedFlowsPortFlows

bool GOConstantPropagation::isInputPort(FLNode* flow) const
{
  // Make sure this is a port connection
  NUUseDefNode* useDef = flow->getUseDefNode();
  NU_ASSERT((useDef != NULL) && useDef->isPortConn(), useDef);

  // Input ports are easy, bid ports have to flows. One that
  // represents the input component and one that represents the output
  // component.
  bool isInput = false;
  if (useDef->getType() == eNUPortConnectionInput) {
    isInput = true;

  } else if (useDef->getType() == eNUPortConnectionBid) {
    // For bids, the input component defs the formal
    NUPortConnectionBid* bid = dynamic_cast<NUPortConnectionBid*>(useDef);
    if (bid->getFormal() == flow->getDefNet()) {
      isInput = true;
    }
  }
  return isInput;
} // bool GOConstantPropagation::isInputPort

void GOConstantPropagation::optimizeDesign()
{
  // Convert nets to assignments to constants
  mTieNets->optimize();
  optimizeConstantNets();

  // Try to do some local propagation
  applyLocalConstants();
}

bool
GOConstantPropagation::SynthCallback::stop(FLNodeElabVector* flows,
                                           const NUNetRefHdl&,
                                           bool isContDriver)
{
  // We should only be called for continous drivers
  FLN_ELAB_ASSERT(isContDriver, *(flows->begin()));

  // If we hit the start node, continue, it means we are hitting a
  // nested version of this elaborated flow node.
  bool startFlow = false;
  for (FLNodeElabVectorIter i = flows->begin(); i != flows->end(); ++i)
  {
    FLNodeElab* flow = *i;
    startFlow |= (flow == mStartFlow);
  }
  if (startFlow && !mOtherFlowSeen)
    return false;
  else
    mOtherFlowSeen = true;

  // Check if this is a single port connection, if it is, keep going
  if (flows->size() == 1) {
    FLNodeElab* flowElab = *(flows->begin());
    NUUseDefNode* useDef = flowElab->getUseDefNode();
    if ((useDef != NULL) && useDef->isPortConn()) {
      return false;
    }
  }

  // Stop, it must be a fanin of our start flow
  return true;
}

ESExprHndl 
GOConstantPropagation::SynthCallback::createIdentExpr(
  FLNodeElabVector* flowElabs,
  UInt32 bitSize, bool isSigned,
  bool hardStop)
{
  // Get a net to represent this ident. Any flow Elab is fine because
  // they should all be for the same net.
  FLNodeElab* flowElab = *(flowElabs->begin());
  NUNetRefHdl netRef = flowElab->getFLNode()->getDefNetRef();
  const NUNet* net = netRef->getNet ();

  // Figure out the sign for any constant
  CarbonExpr::SignT sign = CarbonConst::eUnsigned;
  if (net->isSigned() and not net->isUnsignedSubrange ()) {
    sign = CarbonConst::ePos;
  }

  // Ask the constant propagation class if this set of elaborated
  // flows represent a constant. If so, use that constant as the
  // expression. Otherwise create an ident expression.
  ESFactory* factory = mPopulateExpr->getFactory();
  ConstValue constValue = mConstantPropagation->getConstant(flowElabs);
  if (constValue.isConstant()) {
    // Create a constant expression
    DynBitVector* value = constValue.getValue();    
    int size = net->isIntegerSubrange () ? 32 : bitSize;
    CarbonExpr* expr = factory->createConst(*value, sign, size);

    // We remember that we found a constant because it is not worth
    // doing a translation from Carbon expressions to Nucleus
    // expressions and running fold unless we found some constants in
    // the expression tree. This is done for compile performance
    // reasons. If we ever need to find constants for expressions like
    // (a & ~a) then we should consider other ways to improve compile
    // performance.
    mHasConstantLeaf = true;
    return ESExprHndl(expr);

  } else if (constValue.isPartialConstant() && !net->isIntegerSubrange()) {
    // Note: we give up on integer sub-ranges because they have funny
    // size issues and I'm not sure I can handle them with a partial
    // constant. Hopefully that does not matter.

    // Create a concat operator with parts from the constant and
    // non-constant expressions. Create an ident for the non-constant
    // parts and get the partial constant
    ESExprHndl ident = ESCallbackElab::createIdentExpr(flowElabs, bitSize,
                                                       isSigned, hardStop);
    DynBitVector* value;
    DynBitVector* mask;
    constValue.getPartialValue(&value, &mask);

    // Create the elements of the concat by walking the mask
    CarbonExprVector exprs;
    FLN_ELAB_ASSERT(bitSize == value->size(), flowElab);
    UInt32 endRange = bitSize - 1;
    bool isConst = (*mask)[endRange];
    for (UInt32 i = bitSize; i > 0; --i) {
      // Figure out if the next bit is a constant if we are not at the
      // end of the range.
      bool nextIsConst = false;
      if (i > 1) {
        nextIsConst = ((*mask)[i - 2] != 0);
      }

      // Check if the const/non-const attribute changes or if we are
      // at the end of the range. In that case we copy over the range
      // of bits.
      if ((isConst != nextIsConst) || (i == 1)) {
        // Figure out the range (i-1:endRange) and size
        UInt32 startRange = i - 1;
        ConstantRange range(endRange, startRange);
        UInt32 length = range.getLength();

        // Grab the constant or ident
        CarbonExpr* subExpr = NULL;
        if (isConst) {
          DynBitVector subValue(length);
          subValue = value->partsel(range.getLsb(), length);
          subExpr = factory->createConst(subValue, sign, length);
        } else {
          CarbonExpr* identExpr = ident.getExpr();
          subExpr = factory->createPartsel(identExpr, range, length, isSigned);
        }

        // Add this to the vector to create and update the end range
        exprs.push_back(subExpr);
        endRange = startRange - 1;
        isConst = nextIsConst;
      }
    } // for

    // Create the concat
    CarbonExpr* retExpr = factory->createConcatOp(&exprs, 1, bitSize, sign != CarbonConst::eUnsigned);
    mHasConstantLeaf = true;
    return ESExprHndl(retExpr);

  } else if (constValue.isUndriven()) {
    // It must be undriven, return an expression of z
    int size = net->isIntegerSubrange () ? 32 : bitSize;
    CarbonExpr* expr = factory->createConstZ(size, sign != CarbonConst::eUnsigned);
    mHasConstantLeaf = true;
    return ESExprHndl(expr);

  } else {
    // Give up and create an ident
    return ESCallbackElab::createIdentExpr(flowElabs, bitSize, isSigned, hardStop);
  }
} // GOConstantPropagation::SynthCallback::createIdentExpr

NUExpr* GOConstantPropagation::createConst(DynBitVector* value,
                                           bool sign,
					   UInt32 size,
					   const SourceLocator& loc)
{
  NUExpr* expr;
  expr = NUConst::create(sign, *value, value->size(), loc);
  expr->resize(size);
  return expr;
}

bool GOConstantPropagation::qualifyAlwaysBlock(NUNet* net, NUUseDefNode* ud) {
  if (ud->isSequential()) {
    return true;
  }
  if (net->isPort() || net->isMultiplyDriven()) {
    return false;
  }
  return true;
}
  

void GOConstantPropagation::optimizeConstantNets()
{
  // Go through the unelab constant assigns
  DefReplacements defReplacements;
  for (UnelabConstsSortedLoop l = mUnelabConsts->loopSorted(); !l.atEnd(); ++l)
  {
    // We only support optimizing assignment statement and always
    // blocks right now. Note that input/output port connections are
    // handled in a different routine.
    //
    // We don't support optimizing anything that defs the special
    // nets. They need to be done differently than by replacing the
    // LHS with nets. I think the best spot to handle those is in
    // fold.
    FLNode* flow = l.getKey();
    NUUseDefNode* useDef = flow->getUseDefNode();
    NUNet* net = flow->getDefNet();

    if ( net->isSysTaskNet() || net->isControlFlowNet() ) {
      continue; // this kind of net is not optimized here
    }

    if ((useDef == NULL) ||
	((useDef->getType() == eNUContAssign) ||
         (useDef->getType() == eNUPortConnectionOutput) ||
         (useDef->getType() == eNUPortConnectionBid) ||
         (useDef->getType() == eNUInitialBlock) ||
         (useDef->getType() == eNUAlwaysBlock)) )
    {
      // Optimize the nucleus data structures depending on whether it
      // is combinational or sequential or a port connection. For
      // sequential or port connection, we want to get rid of the
      // use def contribution and add a continuous assign statement.
      // We also are willing to optimize combinational always blocks
      // driving nets with no other continuous drivers.
      ConstValue constValue = l.getValue();
      if ((useDef == NULL) ||
          ((useDef->getType() == eNUAlwaysBlock) && qualifyAlwaysBlock(net, useDef)) ||
          (useDef->getType() == eNUPortConnectionOutput) ||
          (useDef->getType() == eNUPortConnectionBid) ||
          (useDef->getType() == eNUInitialBlock)) {
        // For sequential, port connection, initial block we replace
        // all the defs of the net with a temp and assign both the
        // temp and the net with the constant. But we delay doing the
        // def replacement so we can do it more efficiently. So this
        // routine creates the temps and the assignments, but gathers
        // the info for the def replacement.
        optimizeUseDef(flow, constValue, &defReplacements);
      } else if (constValue.isConstant()) {
        // Optimize a combinational assign inside an always block
        DynBitVector* value = constValue.getValue();
        optimizeConstantAssign(flow, value);
      }
    }
  }

  // Now we can more efficiently replace all the defs with the temps
  // gathered in optimizeUseDef
  replaceDefs(defReplacements);
} // void GOConstantPropagation::optimizeConstantNets

void
GOConstantPropagation::optimizeUseDef(FLNode* flow, ConstValue constValue,
                                      DefReplacements* defReplacements)
{
  // Gather the pertinent information for this optimization
  NUNetRefHdl netRef = flow->getDefNetRef();
  NUNet* net = netRef->getNet();

  // There are two tasks to optimize this useDef. One is to replace all
  // LHS of the use def with a temp for the flow's net (this
  // makes the logic dead which causes it to get optimized later). The
  // other is to create an assignment statement so that the net has a
  // constant value. The latter is done once for a given netref. We
  // also only create one temp for any given net ref.
  //
  // This block creates or finds a temp for this net ref and
  // optionally creates and assignment statement to a constant.
  NUNet* tempNet;
  OptimizedUseDefs::iterator pos = mOptimizedUseDefs->find(netRef);
  if (pos == mOptimizedUseDefs->end())
  {
    // Create a new temp based on the net
    NUModule* module = net->getScope()->getModule();
    tempNet = module->createTempNetFromImage(net, "inactive");

    // Add it to our cache
    mOptimizedUseDefs->insert(OptimizedUseDefs::value_type(netRef, tempNet));

    // Create the constant continous assign for the real
    // net. Otherwise print that we remove the driver for inactive
    // flops.
    if (constValue.isConstant()) {
      DynBitVector* value = constValue.getValue();
      createConstContAssign(module, netRef, value);
    } else {
      FLN_ASSERT(constValue.isInactive(), flow);
      NUNet* clkNet = getClock(flow);
      StringAtom* clkName = clkNet->getName();
      mMsgContext->GOInactiveFlop(flow, clkName->str());
    }
  }
  else
  {
    // We have created a temp for this before
    tempNet = pos->second;
  }

  // Gather the replacements we have to do for this use def so that we
  // can replace all LHS's for the use def node with the temp. This
  // makes any assignments dead. We delay doing this so we can run
  // replaceLeaves once per use def node. Note that if we have a NULL
  // use def node we are optimizing an undriven net.
  NUUseDefNode* useDef = flow->getUseDefNode();
  if (useDef != NULL) {
    NUNetReplacementMap& netReplacements = (*defReplacements)[useDef];
    netReplacements.insert(NUNetReplacementMap::value_type(net, tempNet));
  }
} // GOConstantPropagation::optimizeConstantUseDef

void GOConstantPropagation::replaceDefs(DefReplacements& defReplacements)
{
  // Unused replacements for replaceLeaves
  NULvalueReplacementMap unusedLvalueReplacements;
  NUExprReplacementMap unusedRvalueReplacements;
  NUTFReplacementMap unusedTFReplacements;
  NUCModelReplacementMap unusedCModelReplacements;
  NUAlwaysBlockReplacementMap unusedAlwaysReplacements;

  // Replace all the LHS's for the use def node with the temp created
  // in optimizeConstantUseDef.
  for (DefReplacementsLoop l(defReplacements); !l.atEnd(); ++l) {
    NUUseDefNode* useDef = l.getKey();
    const NUNetReplacementMap& netReplacementsConst = l.getValue();
    NUNetReplacementMap& netReplacements = 
      const_cast<NUNetReplacementMap&>(netReplacementsConst);
    RewriteLeaves translator(netReplacements, unusedLvalueReplacements,
                             unusedRvalueReplacements, unusedTFReplacements,
                             unusedCModelReplacements,
                             unusedAlwaysReplacements, NULL);
    mModified |= useDef->replaceLeaves(translator);
  }
} // void GOConstantPropagation::replaceDefs

NUNet* GOConstantPropagation::getClock(FLNode* flow) const
{
  NUUseDefNode* useDef = flow->getUseDefNode();
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  NU_ASSERT(always != NULL, useDef);
  NU_ASSERT(always->isSequential(), useDef);
  NUEdgeExpr* edgeExpr = always->getEdgeExpr();
  return edgeExpr->getNet();
}

void
GOConstantPropagation::createConstContAssign(NUModule* module,
                                             const NUNetRefHdl& netRef,
                                             DynBitVector* val)
{
  // Create the assign(s). We create more than one if the net ref has
  // holes in it because it is easier than creating LHS concats. LHS
  // concats are a pain in many parts of the code so creating more of
  // them doesn't make sense.
  NUNet* net = netRef->getNet();
  const SourceLocator& loc = net->getLoc();
  if (!net->isBitNet()) {
    // Create an assign per contigous range
    for (NUNetRefRangeLoop l = netRef->loopRanges(mNetRefFactory);
         !l.atEnd(); ++l)
    {
      // Create the sub-constant
      const ConstantRange& range = *l;
      const NUNetRefHdl& subRef = mNetRefFactory->createVectorNetRef(net,
                                                                     range);
      DynBitVector subValue;
      resizeConstant(netRef, subRef, *val, &subValue);

      // Create the lvalue.  Be a little careful here to avoid creating
      // a whole-variable part-select that Fold will then transform, which
      // would trigger re-running constant propagation if we are trying
      // to "settle" out all the changes, as we try to do in PV.
      NULvalue* lvalue = NULL;
      NUVectorNet* vn = net->castVectorNet();
      if ((vn == NULL) || (*(vn->getRange()) == range)) {
        lvalue = new NUIdentLvalue(net, loc);
      }
      else {
        lvalue = new NUVarselLvalue(net, range, loc);
      }

      // Create the assign
      mTieNets->createConstContAssign(net, lvalue, module, &subValue, false);
      mModified = true;

      // Print verbose information
      if (mVerbose >= 1)
      {
        UtString str;
        loc.compose(&str);
        UtIO::cout() << str << ": Optimized ";
        lvalue->printVerilog(false, 0, true);
        UtIO::cout() << " = ";
        val->printHex();
      }
    }
  }
  else
  {
    // The lvalue is simpler for bits
    NULvalue* lvalue = new NUIdentLvalue(net, loc);
    mTieNets->createConstContAssign(net, lvalue, module, val, false);
    mModified = true;

    // Print verbose information
    if (mVerbose >= 1)
    {
      UtString str;
      loc.compose(&str);
      UtIO::cout() << str << ": Optimized ";
      lvalue->printVerilog(false, 0, true);
      UtIO::cout() << " = ";
      val->printHex();
    }
  }
} // GOConstantPropagation::createConstContAssign

void
GOConstantPropagation::optimizeConstantAssign(FLNode* flow, DynBitVector* val)
{
  // Recursively visit the nested flow until we get to pure
  // assignment statement(s).
  if (flow->hasNestedBlock())
  {
    // we need to resize appropriately for each nested flow
    for (Iter<FLNode*> loop = flow->loopNested(); !loop.atEnd(); ++loop)
    {
      FLNode* nested = *loop;
      DynBitVector subVal;
      NUNetRefHdl flowNetRef = flow->getDefNetRef();
      NUNetRefHdl nestedNetRef = nested->getDefNetRef();
      resizeConstant(flowNetRef, nestedNetRef, *val, &subVal);
      optimizeConstantAssign(nested, &subVal);
    }
  }
  else
  {
    // Optimize based on the use def type. There are three options
    // here:
    //
    // 1. This flow represents an assign; optimize it.
    //
    // 2. This flow represents a statement we don't know how to
    //    optimize yet (Stick new types here if you don't know what to do).
    //
    // 3. This flow represents a statement type we don't expect to hit
    //    (bug).
    NUUseDefNode* useDef = flow->getUseDefNode();
    switch (useDef->getType())
    {
      case eNUAssign:
      case eNUContAssign:
      case eNUBlockingAssign:
      case eNUNonBlockingAssign:
      {
        // This is where the work can be done
        NUAssign* assign = dynamic_cast<NUAssign*>(useDef);
        UInt32 size = assign->getLvalue()->getBitSize();
        NUExpr* oldExpr = assign->getRvalue();
        NUExpr* expr = createConst(val, oldExpr->isSignedResult (), size, assign->getLoc());

        assign->replaceRvalue(expr);
        delete oldExpr;

        // Print that we optimized this port
        if (mVerbose >= 1)
        {
          // Don't print messages about temps
          NUNetSet nets;
          assign->getLvalue()->getDefs(&nets);
          bool tmp = false;
          for (NUNetSetIter i = nets.begin(); i != nets.end() && !tmp; ++i)
          {
            NUNet* net = *i;
            tmp = net->isTemp();
          }
          if (!tmp)
          {
            UtString str;
            assign->getLoc().compose(&str);
            UtIO::cout() << str << ": Optimized ";
            NULvalue* lvalue = assign->getLvalue();
            lvalue->printVerilog(false, 0, true);
            UtIO::cout() << " = ";
            val->printHex();
          }
        }
        break;
      }

      case eNUContEnabledDriver:
      case eNUBlockingEnabledDriver:
      case eNUTaskEnable:
      case eNUFor:
      case eNUControlSysTask:
      case eNUFCloseSysTask:
      case eNUFFlushSysTask:
      case eNUFOpenSysTask:
      case eNUCModelCall:
      case eNUOutputSysTask:
      case eNUInputSysTask:
        // Don't know how to handle these yet
        break;

      case eNUCase:
      case eNUIf:
      case eNUBlock:
        // These should be handled in the nesting area
        // If they have no nesting, they can be ignored
        FLN_ASSERT(!flow->hasNestedBlock(), flow);
        break;

      case eNUAlwaysBlock:
      case eNUStructuredProc:
      case eNUVarselLvalue:
      case eNUCModel:
      case eNUCModelArgConnectionInput:
      case eNUCModelArgConnectionOutput:
      case eNUCModelArgConnectionBid:
      case eNUConcatLvalue:
      case eNUCycle:
      case eNUIdentLvalue:
      case eNUInitialBlock:
      case eNULvalue:
      case eNUMemselLvalue:
      case eNUModule:
      case eNUModuleInstance:
      case eNUPortConnectionBid:
      case eNUPortConnectionInput:
      case eNUPortConnectionOutput:
      case eNUTF:
      case eNUTFArgConnectionBid:
      case eNUTFArgConnectionInput:
      case eNUTFArgConnectionOutput:
      case eNUTFElab:
      case eNUTask:
      case eNUTriRegInit:
      case eNUPartselIndexLvalue:
      case eNUReadmemX:
      case eNUSysRandom:
      default:
        // These are unexpected objects. We should not hit them in
        // this traversal.
        FLN_ASSERT("Unexpected object type in constant optimizations" == NULL,
                   flow);
        break;
    } // switch
  } // else
} // GOConstantPropagation::optimizeConstantAssign

bool GOConstantPropagation::invalidLvaluesRecurse(FLNodeElab* flow) const
{
  // Either recurse until we find leaf level flow or analyze the flow
  // if this is a leaf level flow.
  bool found = false;
  if (flow->hasNestedBlock()) {
    // Recurse until we get to leaf level flow or we find a statment
    // that has a dynamic bit select.
    for (FLNodeElabLoop l = flow->loopNested(); !l.atEnd() && !found; ++l) {
      FLNodeElab* subFlow = *l;
      found = invalidLvaluesRecurse(subFlow);
    }

  } else {
    // This is a leaf level flow, it must be a statement.
    NUUseDefNode* useDef = flow->getUseDefNode();
    NUStmt* stmt = dynamic_cast<NUStmt*>(useDef);
    NU_ASSERT(stmt != NULL, useDef);
    NUFindDynamicLvalues findInvalidLvalues;
    NUDesignWalker walker(findInvalidLvalues, false);
    if (walker.stmt(stmt) == NUDesignCallback::eStop) {
      found = true;
    }
  }

  return found;
} // bool GOConstantPropagation::invalidLvaluesRecurse

// Find the statements for the provided top level flow that 
// def the net and check if it uses a dynamic bit select.
// This routine uses invalidLvaluesRecurse to find
// the leaf level flow and analyze it.
bool GOConstantPropagation::invalidLvalues(FLNodeElab* flow) const
{
  // If the flow doesn't have a use def node it means it is a bound,
  // ignore it. (Could it be something else?
  NUUseDefNode* useDef = flow->getUseDefNode();
  if (useDef == NULL) {
    return false;
  }

  // If this is not a continous driver, it can't have a dyn bitsel lhs
  // (eg port connections etc).
  if (!useDef->isContDriver()) {
    return false;
  }

  return invalidLvaluesRecurse(flow);
}

bool GOConstantPropagation::canPropagateConst(FLNodeElab* flow) const
{
  // We don't propagate non-active drivers of a net (initial or pull
  // strength). They really aren't constants
  if (!isActiveDriver(flow))
    return false;

  // Don't propagate through 2D arrays because we have expression
  // re-synthesis issues with it. And there really is no benefit
  // anyway.
  NUNet* net = flow->getDefNet()->getStorageNet();
  if (net->is2DAnything())
    return false;

  // Don't propagate if this is mutable. This tests this netelab and
  // all its potential aliases.
  if (isMutable(flow->getDefNet())) {
    return false;
  }

  // Test if there are invalid lvalues. These include dynamic selects
  // as well as concat lvalues. A constant write to a dynamic bit
  // select is not the same as writing the whole net with a constant,
  // which is what it looks like with flow. Also, constant assigns to
  // concat lvalues can reorder the values.
  if (invalidLvalues(flow)) {
    return false;
  }

  // In all other case we can propagate constants through this flow
  return true;
} // bool GOConstantPropagation::canPropagateConst

GOConstantPropagation::ConstValue
GOConstantPropagation::getConstantValue(FLNodeElab* flowElab) const
{
  ConstValue constValue(eConstUninit, NULL);
  ConstCache::iterator pos = mConstCache->find(flowElab);
  if (pos != mConstCache->end()) {
    constValue = pos->second;
  }
  return constValue;
}

GOConstantPropagation::ConstValue
GOConstantPropagation::findInCache(ConstCache* constCache,
                                   FLNodeElab* flowElab) const
{
  ConstValue constValue;
  ConstCache::iterator pos = constCache->find(flowElab);
  if (pos != constCache->end()) {
    constValue = pos->second;
  }
  return constValue;
}

void
GOConstantPropagation::addCachedConst(ConstCache* constCache,
                                      FLNodeElab* flow,
                                      ConstValue constValue)
{
  FLN_ELAB_ASSERT(constCache->find(flow) == constCache->end(), flow);
  if (!constValue.isUnknown() && !constValue.isUndriven()) {
    constCache->insert(ConstCache::value_type(flow, constValue));
  }
}

void
GOConstantPropagation::resizeConstant(const NUNetRefHdl& constNetRef,
                                       const NUNetRefHdl& subNetRef,
                                       const DynBitVector& value,
                                       DynBitVector* subValue) const
{
  // Check if the net refs are the same, if so, we can use the same
  // constant
  if (constNetRef == subNetRef)
  {
    subValue->resize(value.size());
    *subValue = value;
    return;
  }

  // They are not the same size, we need a smaller constant. At least
  // that is the assumption so lets make sure. If so, we can get the
  // masks that describe what the constant is on and where we want to
  // put the constant.
  NU_ASSERT(constNetRef->covers(*subNetRef), constNetRef->getNet());
  DynBitVector constMask;
  constNetRef->getUsageMask(&constMask);
  DynBitVector subConstMask;
  subNetRef->getUsageMask(&subConstMask);
  NU_ASSERT(subConstMask.size() == constMask.size(), constNetRef->getNet());

  // So now lets make the constant smaller. This is a complex loop
  // because the usage masks for both the original constant mask and
  // the subset constant mask can have holes. For example the constant
  // could be on net[7-6,2-1] and the sub bet mask could be
  // net[6,1]. This means we need the constant value from offsets 2
  // and 0.
  //
  // Start by looping over the constants mask. Wherever it is a 1, it
  // represents a bit in the constant.
  UInt32 dstIndex = 0;
  UInt32 srcIndex = 0;
  for (UInt32 i = 0; i < constMask.size(); ++i)
  {
    if (constMask.test(i))
    {
      // This is a bit in the constant. Check if it should be a bit in
      // the sub constant too. If so, copy it over. Note that we can
      // assume that the subConstMask and the constMask are the same
      // size due to the assert above.
      if (subConstMask.test(i))
      {
        // This is a bit in the fanin ref that should be a constant
        subValue->resize(dstIndex+1);
        subValue->set(dstIndex, value[srcIndex]);
        ++dstIndex;
      }
      ++srcIndex;
    }
  }
} // GOConstantPropagation::resizeConstant

void
GOConstantPropagation::markConstantFlops(FLNodeElab* flow, Covered& covered,
                                         FlowStack& flowStack)
{
  // Check if we have tried to mark this before
  if (mCoveredFlops->find(flow) != mCoveredFlops->end())
    return;
  mCoveredFlops->insert(flow);

  // Check if this net can be propagated
  if (!canPropagateConst(flow))
    return;

  // Gather all the edge flops and initial blocks on this flops net
  // ref. If it fails, it means we gave up on treating this flop as
  // inactive.
  FLNodeElabSet flops;
  FLNodeElabSet initials;
  ConstType type = gatherOverlappingDrivers(flow, &flops, &initials);
  if (type == eConstNone)
    return;

  // Also mark the other flops covered to reduce how often this
  // routine is called.
  for (FLNodeElabSetIter i = flops.begin(); i != flops.end(); ++i)
  {
    FLNodeElab* flop = *i;
    mCoveredFlops->insert(flop);
  }

  // Recursively determine if all the flops attached to this netref
  // are constant or possibly inactive
  DynBitVector* value;
  value = propagateConstantClocks(flow, flops, initials, covered, flowStack);

  // If we still don't have a constant value, check if all drivers of
  // the flop have the same constant value. If so, we can optimize
  // it. This includes the initial value since we can't guarantee that
  // a clk trigger will ever occur. This can only occur if the driver
  // can be constant (inactive is not good enough)
  if ((type == eConstValue) && (value == NULL)) {
    value = getConstantFlopData(flow, flops, initials);
  }
  if (value == NULL) {
    return;
  }

  // Mark all the flops as constants
  for (FLNodeElabSetIter i = flops.begin(); i != flops.end(); ++i) {
    FLNodeElab* flop = *i;
    ConstValue constValue;
    if (type == eConstValue) {
      constValue = ConstValue(eConstValue, new DynBitVector(*value));
    } else {
      FLN_ELAB_ASSERT(type == eConstInactive, flop);
      constValue = ConstValue(eConstInactive, NULL);
    }
    addCachedConst(mConstCache, flop, constValue);
  }
  delete value;
}

// This routine returns all the elaborated flow for a given sequential
// flow node in two sets. The flops are returned in the flops set, and
// the initial blocks are returned in the initials set.
//
// The return value of the function indicates one of three things:
//
// 1. The flop cannot be a constant
//
// 2. The flop can be a constant
//
// 3. The flop has overlapping combinational logic so at best it can
//    be inactive but not constant.
GOConstantPropagation::ConstType
GOConstantPropagation::gatherOverlappingDrivers(FLNodeElab* flow,
                                                FLNodeElabSet* flops,
                                                FLNodeElabSet* initials) const
{
  // Gather all the flow nodes for this flop. We put them in two
  // buckets: initials and flop components. We can then use the
  // initial value on constant clocked flops. But only if all the
  // flops are inactive (have constant clocks).
  //
  // There are two cases where we currently give up:
  //
  // 1. If the flops are driving subsets of the nets then we can have
  //    cases where the various drivers on the net are different
  //    sizes. This is more difficult to deal with so we are delaying
  //    implementing it. The current check is that all edge drivers that
  //    overlap with the starting flow node have the same net ref.
  //
  // 2. The initial driver is for the entire net. In the future, we
  //    can handle subsets.
  ConstType type = eConstValue;
  NUNetRefHdl netRef = flow->getFLNode()->getDefNetRef();
  NUNetElab* netElab = flow->getDefNet();
  for (NUNetElab::DriverLoop l = netElab->loopContinuousDrivers();
       !l.atEnd() && (type != eConstNone); ++l) {
    // Only use the drivers that overlap with the starting flow node
    FLNodeElab* driver = *l;
    NUNetRefHdl thisNetRef = driver->getFLNode()->getDefNetRef();
    if (thisNetRef->overlaps(*netRef)) {
      if (isSequential(driver)) {
        // We only support an exact match for normal drivers
        if (thisNetRef == netRef)
          flops->insert(driver);
        else
          // Give up
          type = eConstNone;

      } else {
        // For initials, we support it if it writes to all the bits
        NUUseDefNode* useDef = driver->getUseDefNode();
        if ((useDef != NULL) && useDef->isInitial() && thisNetRef->all()) {
          initials->insert(driver);
        } else {
          // Unexpected use def, give up, it can't be a constant
          // (although it could be inactive).
          type = eConstInactive;
        }
      }
    }
  } // for
  return type;
} // GOConstantPropagation::gatherOverlappingDrivers

// This documentation is also copied in the function spec
// doc/functional-specs/state-const-prop.txt. If you update this
// documentation, please update the spec.
//
// There are three possible actions on every flop in the design:
// 
// 1. Check if an async reset is always active and produces a constant
//    value. The test for this is as follows:
// 
//    a. Traverse the broken up blocks from highest priority to lowest
//       priority while the async reset is deemed to be off (inactive).
// 
//    b. We now have the highest priority active clock. If it is
//       always active and the right hand side on the assignment is a
//       constant, mark the flop as having that constant.
// 
// 2. If the following conditions are true, the flop will be treated
//    as having a constant value:
// 
//    a. The net is not depositable or forcible.
// 
//    b. The total number of initial drivers for the flop cannot be
//       greater than one.
// 
//    c. If there is an initial driver the right-hand side is a
//       constant without x's or z's.
// 
//    d. All the drivers of the flop are writing to the same set of
//       bits (supporting cases where the bits are different is possible
//       but more complex).
// 
//    e. All the edge drivers of the flop are determined to have a
//       constant clock.
// 
//    The constant value for the flop will either be the initial value
//    if there is one or constant 0.
// 
// 3. No optimizations will occur
DynBitVector*
GOConstantPropagation::propagateConstantClocks(FLNodeElab* thisFlop,
                                               const FLNodeElabSet& flops,
                                               const FLNodeElabSet& initials,
                                               Covered& covered,
                                               FlowStack& flowStack)
{
  // Flops with multiple edges get broken up into multiple flops.
  // Sort the flow nodes for this set of flops in priority order from
  // highest to lowest. If the highest active priority is a constant,
  // it dominates the value of the flop. See below for details
  FLNodeElabVector sortedFlops;
  sortPriorityFlops(thisFlop, flops, &sortedFlops);

  // Find the highest priority active clock. An active clock is either
  // a non-constant clock, or a constant always on async reset. If
  // there are no active clocks, this routine will return NULL.
  FLNodeElab* activeFlop;
  DynBitVector* clkValue = NULL;
  activeFlop = findHighestPriorityActiveFlop(sortedFlops, &clkValue,
                                             covered, flowStack);

  // If we have an active flop, check if it is a constant async reset
  DynBitVector* value = NULL;
  if ((activeFlop != NULL) && (clkValue != NULL)) {
    // Check if the RHS is a constant, if so we can use that
    NUNetRefHdl netRef = activeFlop->getFLNode()->getDefNetRef();
    value = getConstantExpr(activeFlop, netRef);
  }

  // If we have no active flops, then use the initial block value.
  if (activeFlop == NULL) {
    value = getInitialValue(thisFlop, initials);
  }

  // Return if this flop has a constant value
  return value;
} // GOConstantPropagation::propagateConstantClocks

void
GOConstantPropagation::sortPriorityFlops(FLNodeElab* flow,
                                         const FLNodeElabSet& flops,
                                         FLNodeElabVector* sortedFlops)
{
  // If there is only one flop (which is true pretty often) there is
  // nothing to sort.
  if (flops.size() == 1)
  {
    sortedFlops->push_back(*flops.begin());
    return;
  }

  // Gather the always blocks for this flop. Note that we will not
  // find the funky blocks (see bug 238) because they are not
  // reachable from the clock block. That is ok, because these are
  // just repeats of an edge flop that we will find. They are there
  // for when the async reset goes away.
  //
  // The map we create assigns integer values to the blocks. The lower
  // the value, the higher the priority.
  BlockPriority blockPriority;
  int priority = 0;
  for (NUAlwaysBlock* always = getClockBlock(flow);
       always != NULL;
       always = always->getPriorityBlock(), ++priority)
  {
    blockPriority.insert(BlockPriority::value_type(always, priority));
  }

  // Copy over the flops that are in our always block map
  for (FLNodeElabSet::const_iterator i = flops.begin(); i != flops.end(); ++i)
  {
    FLNodeElab* flop = *i;
    NUUseDefNode* useDef = flop->getUseDefNode();
    if (blockPriority.find(useDef) != blockPriority.end())
      sortedFlops->push_back(flop);
  }

  // Sort the flops using the priority
  SortFlops sortFlops(blockPriority);
  std::sort(sortedFlops->begin(), sortedFlops->end(), sortFlops);
}

FLNodeElab*
GOConstantPropagation::findHighestPriorityActiveFlop(
  const FLNodeElabVector& flops,
  DynBitVector** clkValue,
  Covered& covered,
  FlowStack& flowStack)
{
  // Visit all the flops associated with this net ref and compute if
  // they are inactive. This code will recursively call the constant
  // propagation code for the clock path. We don't think this will
  // overuse the stack because clock paths tend not to be as deep as
  // data paths.
  FLNodeElab* activeFlop = NULL;
  for (FLNodeElabVectorCLoop l(flops); !l.atEnd() && (activeFlop == NULL); ++l)
  {
    // Get the clock for this flop
    FLNodeElab* flop = *l;
    ClockEdge edge;
    NUNetElab* clkElab = getFlopClock(flop, &edge);

    // Iterate over the drivers of the flop and propagate on the clock drivers
    FLNodeElabSet clockFlows;
    for (Fanin p(mFlowElabFactory, flop, eFlowOverCycles, FLIterFlags::eAll);
         !p.atEnd(); ++p)
    {
      FLNodeElab* fanin = *p;
      if (fanin->getDefNet() == clkElab)
      {
        clockFlows.insert(fanin);
        if (isSequential(fanin))
          markConstantFlops(fanin, covered, flowStack);
        else
          depthFirstSearch(fanin, covered, flowStack);
      }
    }

    // Today we only support optimizing a flop if it has one driver on
    // the clock pin and it is a scalar. In the future, we should
    // improve this.
    activeFlop = flop;
    if (clockFlows.size() == 1)
    {
      // Get the clock value
      FLNodeElab* clockFlow = *(clockFlows.begin());
      ConstValue constValue = getConstant(clockFlow);

      // If there is a clock value and it is a scalar, check if it is
      // an active flop
      UInt32 bitSize = clockFlow->getDefNet()->getStorageNet()->getBitSize();
      if (constValue.isConstant() && (bitSize == 1))
      {
        // Check if it is an async reset or clock.
        NUAlwaysBlock* always = getAlwaysBlock(flop);
        bool isClockBlock = always->getClockBlock() == NULL;

        // Constant clock blocks are always inactive, async resets are
        // active if the polarity of the edge matches the constant
        // value.
        if (isClockBlock)
          activeFlop = NULL;
        else
        {
          DynBitVector* value = constValue.getValue();
          if (((edge == eClockPosedge) && (*value == 1)) ||
              ((edge == eClockNegedge) && (*value == 0)))
            // This is an always active async reset
            *clkValue = value;
          else
            // If the clock is a constant always off then go to the
            // next flop in the priority list.
            activeFlop = NULL;
        }
      }
    } // if
  } // for

  return activeFlop;
} // GOConstantPropagation::findHighestPriorityActiveFlop

DynBitVector*
GOConstantPropagation::getInitialValue(FLNodeElab* flow,
                                       const FLNodeElabSet& initials)
{
  // flow is essentially undriven, if we have a constant value from
  // the initial block use that. If it is a tri1 or supply1 then use
  // constant 1. Otherwise use constant 0.
  DynBitVector* value = NULL;
  if (!initials.empty()) {
    // Compute the constants for all the initial values if they
    // haven't already. We do this first for the entire constant.
    for (FLNodeElabSet::UnsortedCLoop l = initials.loopCUnsorted();
         !l.atEnd(); ++l)
    {
      FLNodeElab* driver = *l;
      ConstValue curValue = findInCache(mInitialCache, driver);
      if (curValue.isNonConstant()) {
        // Not yet computed
        curValue = computeConstant(driver);
        if (!curValue.isNonConstant()) {
          addCachedConst(mInitialCache, driver, curValue);
        }
      }
    }

    // Make sure all initials have the same value.  I can't imagine
    // what and event-based simulator does if nets that are connected
    // through ports have different valued initial blocks. But we'd
    // better be safe and test for it.
    bool valid = true;
    for (FLNodeElabSet::UnsortedCLoop l = initials.loopCUnsorted(); !l.atEnd() && valid; ++l) {
      // We want the constant for the bits written by the given flow
      // node. But we could have initial drivers in different parts of
      // the hierarchy. So we might need to make a net ref image.
      FLNodeElab* driver = *l;
      NUNetRefHdl flowNetRef = flow->getFLNode()->getDefNetRef();
      NUNet* net = driver->getDefNet()->getStorageNet();
      NUNetRefHdl netRef = mNetRefFactory->createNetRefImage(net, flowNetRef);

      // Get and size the constant if this initial driver overlaps the
      // requestd elaborated flow net ref
      DynBitVector* curValue = NULL;
      NUNetRefHdl driverNetRef = driver->getFLNode()->getDefNetRef();
      if (driverNetRef->overlaps(*flowNetRef)) {
        ConstValue initialConstValue = findInCache(mInitialCache, driver);
        if (initialConstValue.isConstant()) {
          curValue = new DynBitVector(netRef->getNumBits());
          DynBitVector* initValue = initialConstValue.getValue();
          resizeConstant(driverNetRef, netRef, *initValue, curValue);
        }
      }

      // We fail if this initial block can't compute a value or if one
      // of the initial blocks has a different value.
      if (curValue == NULL) {
        valid = false;

      } else if (value == NULL) {
        // First time, just use the current value
        value = curValue;

      } else if (*curValue == *value) {
        // Not the first time and they values are equal, but we have
        // to delete the allocated space.
        delete curValue;

      } else {
        // Not the same value, give up
        delete curValue;
        valid = false;
      }
    } // for

    // If we failed for some reason, make sure we clean up
    if (!valid) {
      delete value;
      value = NULL;
    }

  } else {
    NUNetElab* netElab = flow->getDefNet();
    NUNet* net = netElab->getStorageNet();
    int netSize = net->getBitSize();
    value = new DynBitVector(netSize);
    if (net->isDefault1()) {
      value->flip();
    }
  }

  return value;
} // GOConstantPropagation::getInitialValue

DynBitVector*
GOConstantPropagation::getConstantExpr(FLNodeElab* flow,
                                       const NUNetRefHdl& netRef)
{
  // Do a recursive call to see if this flow is a constant
  ConstValue constValue = computeConstant(flow);

  // Size the constant appropriately if we got one
  DynBitVector* subValue = NULL;
  if (constValue.isConstant())
  {
    NUNetRefHdl thisNetRef = flow->getFLNode()->getDefNetRef();
    subValue = new DynBitVector(netRef->getNumBits());
    DynBitVector* value = constValue.getValue();
    resizeConstant(thisNetRef, netRef, *value, subValue);
    delete value;
  } else if (constValue.isPartialConstant()) {
    DynBitVector* value;
    DynBitVector* mask;
    constValue.getPartialValue(&value, &mask);
    delete value;
    delete mask;
  }
  return subValue;
} // GOConstantPropagation::getConstantExpr

DynBitVector*
GOConstantPropagation::getConstantFlopData(FLNodeElab* thisFlop,
                                           const FLNodeElabSet& flops,
                                           const FLNodeElabSet& initials)
{
  // See if all the flops have the same constant value
  DynBitVector* value = NULL;
  bool constant = true;
  typedef CLoop<FLNodeElabSet> FLNodeElabSetCLoop;
  for (FLNodeElabSetCLoop l(flops); !l.atEnd() && constant; ++l) {
    // Get the constant value for this flop
    FLNodeElab* flowElab = *l;
    NUNetRefHdl netRef = flowElab->getFLNode()->getDefNetRef();
    DynBitVector* thisValue = getConstantExpr(flowElab, netRef);

    // Check if it is not a constant or conflicts with another value
    if (thisValue == NULL) {
      constant = false;
    } else if (value == NULL) {
      // This is the first node in the vector
      value = thisValue;
    } else if (*value == *thisValue) {
      // They are the same so we can continue, but clean up since
      // getConstantExpr allocates space.
      delete thisValue;
    } else {
      // Must not be the same constant, give up
      constant = false;
      delete thisValue;
    }
  } // for

  // If it is a constant value, make sure it is the same as the
  // initial value if there is one or that the value is a constant 0
  // if there are no initial values.
  if (constant && (value != NULL)) {
    if (!initials.empty()) {
      // There are initial block(s), use their value if we can
      DynBitVector* initValue = getInitialValue(thisFlop, initials);
      if (initValue == NULL) {
        // Could not determine a value, give up
        constant = false;
      } else if (*initValue != *value) {
        // Different values, give up
        constant = false;
      }

      // We don't need the init value space anymore
      delete initValue;

    }

    else if (value->any()) {
      // There are no initial blocks so we init to 0, but the constant
      // value is not zero.
      constant = false;
    }
  }

  // Delete value space if this is not a constant
  if (!constant) {
    delete value;
    return NULL;
  } else {
    return value;
  }
} // GOConstantPropagation::getConstantFlopData

NUNetElab*
GOConstantPropagation::getFlopClock(FLNodeElab* flop, ClockEdge* edge) const
{
  // Find the clock pin for this flop. There is a copy of this code
  // in schedule/Util.cxx. I wanted to put this routine in an area
  // common to both but didn't really know of a good place. We
  // should re-organize this at some point.
  NUAlwaysBlock* always = getAlwaysBlock(flop);
  FLN_ELAB_ASSERT(always && always->isSequential(), flop);
  NUEdgeExpr *edge_expr = always->getEdgeExpr();
  NU_ASSERT(edge_expr, always);
  NUNet* clockNet = edge_expr->getNet();
  if (edge != NULL)
    *edge = edge_expr->getEdge();
  return clockNet->lookupElab(flop->getHier());
}

NUAlwaysBlock* GOConstantPropagation::getClockBlock(FLNodeElab* flop) const
{
  NUAlwaysBlock* always = getAlwaysBlock(flop);
  FLN_ELAB_ASSERT(always && always->isSequential(), flop);
  if (always->getClockBlock() != NULL)
    return always->getClockBlock();
  else
    return always;
}

NUAlwaysBlock* GOConstantPropagation::getAlwaysBlock(FLNodeElab* flop) const
{
  return getAlwaysBlock(flop->getFLNode());
}

NUAlwaysBlock* GOConstantPropagation::getAlwaysBlock(FLNode* flow) const
{
  NUUseDefNode* useDef = flow->getUseDefNode();
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  return always;
}

void GOConstantPropagation::applyLocalConstants() const
{
  // Walk all the modules and apply constants
  LocalPropagationStatistics propagation_statistics;
  RELocalConstProp localConstProp(mIODBNucleus, 
                                  NULL, 
                                  mFold, 
                                  &propagation_statistics,
                                  mVerbose >= 2,
                                  false); // do not use flow order
  NUModuleList modules;
  mDesign->getModulesBottomUp(&modules);
  typedef Loop<NUModuleList> ModuleLoop;
  for (ModuleLoop l(modules); !l.atEnd(); ++l) {
    NUModule* module = *l;
    localConstProp.module(module);
  }
  if (mVerboseLocalPropagation) {
    propagation_statistics.print("Constant Propagation");
  }
}

static bool lessFlows(const FLNodeElab* f1, const FLNodeElab* f2)
{
  return FLNodeElab::compare(f1, f2) < 0;
}

void GOConstantPropagation::sortFlowStack(FlowStack* flowStack)
{
  Stack& flows = flowStack->second;
  std::sort(flows.begin(), flows.end(), lessFlows);
}

GOConstantPropagation::ConstValue
GOConstantPropagation::extractConstant(const NUExpr* expr, UInt32 size)
{
  // Get the value for the constant
  const NUConst* constExpr = expr->castConst();
  DynBitVector* value = new DynBitVector(size);
  constExpr->getSignedValue(value);

  // All done with the copy, return the constant
  return ConstValue(eConstValue, value);
}

GOConstantPropagation::ConstValue
GOConstantPropagation::extractPartialConstant(const NUExpr* expr, UInt32 size)
{
  // Check if any of the items in the concat are constants. We only
  // need to go one level. If there it isn't a constant at the top
  // level then it must be non-constant. We are assuming that fold
  // does not created nested concats and does not let them through.
  NU_ASSERT(expr->getType() == NUExpr::eNUConcatOp, expr);
  const NUConcatOp* concatOp = dynamic_cast<const NUConcatOp*>(expr);
  bool constFound = false;
  int numArgs = concatOp->getNumArgs();
  for (int i = 0; !constFound && i < numArgs; ++i) {
    const NUExpr* subExpr = concatOp->getArg(i);
    constFound = (subExpr->getType() == NUExpr::eNUConstNoXZ);
  }

  // Use the max size so that we populate it correctly and then truncate below.
  UInt32 exprSize = expr->getBitSize();
  UInt32 maxSize = (size > exprSize) ? size : exprSize;

  // If we found a constant, create the partial constant value
  ConstValue constValue;
  if (constFound) {
    DynBitVector* value = new DynBitVector(maxSize);
    DynBitVector* mask = new DynBitVector(maxSize);
    int offset = 0;
    for (UInt32 i = 0; i < concatOp->getRepeatCount(); ++i) {
      for (UInt32 j = concatOp->getNumArgs(); j > 0 ; --j) {
        const NUExpr* subExpr = concatOp->getArg(j-1);
        int subSize = subExpr->getBitSize();
        if (subExpr->getType() == NUExpr::eNUConstNoXZ) {
          // We found a constant, get its value
          DynBitVector subValue(subSize);
          const NUConst* constExpr = subExpr->castConst();
          constExpr->getSignedValue(&subValue);

          // Copy this value to the current offset for the partial
          // constant. And set the mask bits for that range
          value->lpartsel(offset, subSize) = subValue;
          mask->setRange(offset, subSize, 1);
        }

        // Move to the next location in the partial constants
        offset += subSize;
      }
    }

    // Now size it to the LHS
    value->resize(size);
    mask->resize(size);
    constValue = ConstValue(value, mask);
  }

  // All done with the expression
  return constValue;
} // GOConstantPropagation::extractPartialConstant

bool GOConstantPropagation::isMutable(NUNetElab* netElab) const
{
  // test the cache first, we use the master to store information so
  // that we use less memory where possible.
  NUNetElab* master = mAlias->getMaster(netElab);
  MutableNets::iterator pos = mMutableNets->find(master);
  if (pos != mMutableNets->end()) {
    return pos->second;
  }

  // Not there, check the net and all its aliases
  NUNetElabVector netElabs;
  mAlias->getAliases(netElab, &netElabs);
  bool isMutable = false;
  for (NUNetElabVectorLoop l(netElabs); !isMutable && !l.atEnd(); ++l) {
    NUNetElab* aliasElab = *l;
    isMutable = aliasElab->getStorageNet()->isProtectedMutable(mIODBNucleus);
  }

  // Store it and return
  mMutableNets->insert(MutableNets::value_type(master, isMutable));
  return isMutable;
} // bool GOConstantPropagation::isMutable

