// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "util/Stats.h"
#include "util/Zstream.h"

#include "iodb/IODBNucleus.h"

#include "nucleus/NUNetRef.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUDesign.h"

#include "flow/FLFactory.h"

#include "localflow/UpdateUD.h"

#include "localflow/LocalAnalysis.h"
#include "localflow/Elaborate.h"

#include "reduce/RemoveElabFlow.h"
#include "reduce/RemoveUnelabFlow.h"
#include "reduce/REAlias.h"
#include "reduce/RENetWarning.h"
#include "reduce/ReachableAliases.h"
#include "reduce/UseBeforeKill.h"
#include "reduce/CleanElabFlow.h"

#include "compiler_driver/CarbonDBWrite.h"
#include "compiler_driver/CarbonContext.h"

#include "hdl/HdlVerilogPath.h"

#include "globopts/GlobOpts.h"

#include "ConstantPropagation.h"
#include "nucleus/NUTF.h"
#include "iodb/CGraphFlow.h"

// Switches storage (due to hiding of strings)
static const char* scConstantPropagation = NULL;
static const char* scNoConstantPropagation = NULL;
static const char* scVerboseConstantPropagation = NULL;
static const char* scNoUndrivenConstants = NULL;

class GOGlobOpts::TFOutUseBeforeKill : public NUDesignCallback
{
public:
  TFOutUseBeforeKill(NUNetRefFactory* refFactory, MsgContext* msgContext) :
    mNetRefFactory(refFactory), mMsgContext(msgContext)
  {}

  virtual ~TFOutUseBeforeKill() {}

  virtual Status operator()(Phase, NUBase*) 
  {
    return eNormal;
  }

  virtual Status operator()(Phase phase, NUTF* tf) { 
    if (phase == ePre)
    {
      UseBeforeKill ubk(mNetRefFactory);
      ubk.tf(tf);
      
      if (ubk.hasUBKNets())
      {
        NUNetSet taskOutNets;
        // only output nets can be read uninitialized inside a task (until
        // we lower it). Collect these for comparisons against what is
        // used before killed.
        for (NUNetFilterLoop tfArgLoop = tf->loopArgs(eOutputNet);
             ! tfArgLoop.atEnd(); ++tfArgLoop)
        {
          NUNet* tfParam = *tfArgLoop;
          bool addToSet = true;
          // This check is here in case we ever change from the current
          // practice of inlining vhdl tasks that contain signals.
          int argIndex = tf->getArgIndex (tfParam);
          NU_ASSERT (argIndex >= 0, tf);
          NUTF::CallByMode mode = tf->getArgMode (argIndex);
          switch (mode) {
          case NUTF::eCallByCopyOut:
          case NUTF::eCallByCopyInOut:
          case NUTF::eCallByValue:
            break;
            
          case NUTF::eCallByAny:
          case NUTF::eCallByReference:
            // Don't care about these nets
            // Typically, vhdl signals
            addToSet = false;
            break;
          }
          if (addToSet)
            taskOutNets.insert(tfParam);
        
        }
        
        UseBeforeKill::NetToLoc ubkNetLocs;
        ubk.intersectNetLocs(&ubkNetLocs, taskOutNets);
        for (UseBeforeKill::SortedNetLocLoop q = ubkNetLocs.loopSorted(); 
             ! q.atEnd(); ++q)
        {
          NUNet* net = q.getKey();
          const SourceLocator* loc = q.getValue();
          mMsgContext->RETaskUninitializedRead(loc, net->getName()->str());
        }
      }
    }
    return eNormal;
  }

private:
  NUNetRefFactory* mNetRefFactory;
  MsgContext* mMsgContext;
};


GOGlobOpts::GOGlobOpts(AtomicCache* atomicCache, IODBNucleus* iodbNucleus,
                       Stats* stats, ArgProc* args, MsgContext* msgContext,
                       NUNetRefFactory* netRefFactory,
		       SourceLocatorFactory* sourceLocatorFactory,
                       const char* congruenceInfoFilename,
                       WiredNetResynth* wiredNetResynth,
                       TristateModeT tristateMode)
{
  mAtomicCache = atomicCache;
  mSymTabBOM = NULL;
  mSymbolTable = NULL;
  mOldSymbolTable = NULL;
  mIODBNucleus = iodbNucleus;
  mStats = stats;
  mMsgContext = msgContext;
  mSourceLocatorFactory = sourceLocatorFactory;
  mNetRefFactory = netRefFactory;
  mArgs = args;
  mDesign = NULL;
  mFlowFactory = NULL;
  mFlowElabFactory = NULL;
  mElaborate = NULL;
  mCongruenceInfoFilename = congruenceInfoFilename;
  mWiredNetResynth = wiredNetResynth;
  mTristateMode = tristateMode;
  mModified = false;
}

void GOGlobOpts::addCommandlineArgs(ArgProc * arg)
{
  // Create switches to enable/disable portions of global optimizations
  scConstantPropagation = CRYPT("-constantPropagation");
  scNoConstantPropagation = CRYPT("-noConstantPropagation");
  scVerboseConstantPropagation = CRYPT("-verboseConstantPropagation");
  scNoUndrivenConstants = CRYPT("-noUndrivenConstants");

  arg->addBool(scConstantPropagation, CRYPT("Enables global constant propagation"), true, CarbonContext::ePassCarbon);
  arg->addBoolOverride(scNoConstantPropagation, scConstantPropagation, CRYPT("By default the Carbon compiler globally propagates constants through the design. This option disables that optimization."));
  arg->addInt(scVerboseConstantPropagation, CRYPT("Prints all the assignment statements and port connections that were optimized"), 0, false, false, CarbonContext::ePassCarbon);
  arg->addBool(scNoUndrivenConstants, CRYPT("Disables treating undriven nets as constants."), false, CarbonContext::ePassCarbon);
}

GOGlobOpts::~GOGlobOpts()
{
  mIODBNucleus->clearCollapseNets();
}

bool GOGlobOpts::optimize(NUDesign* design, const char* fileRoot,
                          bool print_warnings)
{
  mStats->pushIntervalTimer();

  // Get any switches that control parts of these global optimizations
  bool doConstProp = mArgs->getBoolValue(scConstantPropagation);
  // Create the framework for optimizations
  if (mDesign == NULL) {
    createFramework(design);
  }
  else {
    INFO_ASSERT(design == mDesign, "invalid calling sequence");
  }

  mStats->printIntervalStatistics("GO Prep");

  {
    /*
      There is no good place to do this, and we already have removed
      tasks that we have inlined, but here since hierrefs have been
      resolved and UD has been built, we can warn a user on a poorly
      constructed task. Unfortunately, inlining a task removes this
      analysis for that task.
      This could be moved before/during TFRewrite if
      -multiLevelHierTasks didn't exist. That switch screws up UD
      calculation.

      The reason behind this analysis is that a simulator will
      propagate an x into and out of a task if a task output is used
      uninitialized. Carbon only propagates a 0, leaving waveform
      miscompares that have to be traced back. At least, this way we
      can warn a user about it.
    */
    TFOutUseBeforeKill taskCB(mNetRefFactory, mMsgContext);
    NUDesignWalker walker(taskCB, false);
    walker.design(mDesign);
  }

  // Create an aliases class for figuring out which nets are
  // driven. It can't use the pessimistic version because RENetWarning
  // can't handle aliases of different sizes.
  REAlias* alias = createAliases();
  alias->findAliases(mStats, true);

  RENetWarning netWarnings(mNetRefFactory, mSymbolTable, mIODBNucleus,
                           mMsgContext, mReachableAliases, alias,
                           mDesign, mArgs, mFlowElabFactory);

  // Do undriven net warnings.
  if (print_warnings) {
    netWarnings.walkDesign();
    netWarnings.printWarnings(RENetWarning::eWarnUndriven);
    mStats->printIntervalStatistics(CRYPT("NetWarnings"));
  }

  // write out the .drivers file for the old tree-based connectivity browser
  {
    UtString driversFile, errmsg;
    driversFile << fileRoot << ".drivers";
    ZOSTREAMDB(drivers, driversFile.c_str());
    bool ok = drivers.is_open();
    if (ok) {
      netWarnings.dumpDrivers(&drivers);
      ok = drivers.close();
    }
    if (!ok) {
      fprintf(stdout, "%s\n", drivers.getErrmsg());
    }
  }

  // write out the .craph file for the new graphical connectivity browser
#ifdef NEW_CBROWSE
  {
    UtString cgraphFile, errmsg;
    CGraphFlow cgraph(mNetRefFactory, alias, mSourceLocatorFactory,
                      mAtomicCache);
    cgraph.design(mDesign);
    cgraphFile << fileRoot << ".cgraph";
    if (!cgraph.write(cgraphFile.c_str(), &errmsg)) {
      fprintf(stdout, "%s\n", errmsg.c_str());
    }
  }
#endif

  // All done with the alias class. We don't want to store this data
  // too long because it takes quite a bit of space. And constant
  // propagation creates it's own class.
  delete alias;
  netWarnings.putREAlias(NULL);

  // Run constant propagation
  if (doConstProp)
  {
    ArgProc::OptionStateT status;
    SInt32 verbose;
    status = mArgs->getIntLast(scVerboseConstantPropagation, &verbose);
    bool undrivenConstants = !mArgs->getBoolValue(scNoUndrivenConstants);
    propagateConstants(undrivenConstants, verbose, &netWarnings);
    mStats->printIntervalStatistics("GO ConstProp");
  }

  // All done so we can delete the temporary framework
  destroyFramework();
  mStats->printIntervalStatistics("GO Cleanup");

  mStats->popIntervalTimer();
  return true;
}

REAlias* GOGlobOpts::createAliases() {
  REAlias* alias = new REAlias(mDesign, mSymbolTable, mFlowElabFactory,
                               mMsgContext, mNetRefFactory, false, NULL, false);
  return alias;
}

void GOGlobOpts::createFramework(NUDesign* design)
{
  mDesign = design;
  mStats->pushIntervalTimer();

  // We create a temporary BOM and symbol table for the global
  // optimizations
  mSymTabBOM = new CbuildSymTabBOM;
  mSymbolTable = new STSymbolTable(mSymTabBOM, mAtomicCache);
  mSymbolTable->setHdlHier(new HdlVerilogPath);

  mFlowFactory = new FLNodeFactory();
  mFlowElabFactory = new FLNodeElabFactory();
  mElaborate = new Elaborate(mSymbolTable,
			     mAtomicCache,
			     mSourceLocatorFactory,
			     mFlowFactory,
			     mFlowElabFactory,
			     mNetRefFactory,
			     mMsgContext,
			     mArgs,
                             mIODBNucleus);

  // Update the NUDesign symbol table and record the old one so we can
  // restore it.
  mOldSymbolTable = mDesign->putSymtab(mSymbolTable);

  // Run local analysis which includes at least UD and local flow
  mStats->pushIntervalTimer();
  LocalAnalysis local_analysis(mArgs->getBoolValue(CRYPT("-phaseStats")),
			       mArgs->getBoolValue(CRYPT("-modulePhaseStats")),
			       mStats,
			       mAtomicCache,
			       mNetRefFactory,
			       mFlowFactory,
			       mMsgContext,
			       mIODBNucleus,
			       mArgs,
			       mSymbolTable,
			       mSourceLocatorFactory,
			       mFlowElabFactory,
                               mCongruenceInfoFilename.c_str(),
                               mWiredNetResynth,
                               mTristateMode);

  local_analysis.analyzeDesign(mDesign, LocalAnalysis::eAnalysisModeMinimal);
  mStats->popIntervalTimer();
  mStats->printIntervalStatistics("MinAnalysis");

  // Run the elaboration
  mElaborate->design(mDesign, true);
  mStats->printIntervalStatistics("DesElab");

  // Initialise mCheckedDirectives so that if a net is not found during
  // subsequent directives processing we can distinguish nets that never
  // existed from nets that have been eliminated by the compiler.
  mIODBNucleus->checkDirectives (mSymbolTable);

  // Populate the design's additional outputs. This includes data from
  // the IODB and module internal nets that need to be made live.
  mIODBNucleus->populateDesignOutputs(mDesign, mSymTabBOM, mSymbolTable);
  mDesign->handleElabInternalNets();
  mStats->printIntervalStatistics(CRYPT("DesOutputs"));

  // remove dead elab flow.
  CleanElabFlow cleanup(mFlowElabFactory, mSymbolTable, false);
  cleanup.design(mDesign);

  // Compute set of reachable symtab nodes.
  // Must come before hierarchical aliasing but after elaboration.
  mReachableAliases = new ReachableAliases();
  mReachableAliases->compute(mSymbolTable);
  mStats->printIntervalStatistics("Reachable");

  // Mark the depositable unelaborated nets
  mIODBNucleus->resolveDepositable(mSymTabBOM, mReachableAliases,
                                   mSymbolTable, false);

  // Find the elaborated collapse clocks
  mIODBNucleus->findCollapseNets(mSymbolTable);

  mStats->popIntervalTimer();
}

void GOGlobOpts::destroyFramework()
{
  mStats->pushIntervalTimer();

  // Traverse the design and clean the UD information. We don't need
  // it and it will get re-created by the later local analysis pass.
  ClearUDCallback callback(true); // recurse
  NUDesignWalker walker(callback, false);
  walker.design(mDesign);

  // Remove the elaborated flow
  RemoveElabFlow removeElab(mFlowElabFactory, mSymbolTable);
  removeElab.remove();
  mStats->printIntervalStatistics("RemEFlow");

  // Remove the unelaborated flow
  RemoveUnelabFlow removeUnelab(mFlowFactory);
  removeUnelab.remove();
  mStats->printIntervalStatistics("RemUEFlow");

  // Clear the additional design outputs
  mDesign->clearDesignOutputs();

  // Don't need the memory anymore
  delete mFlowElabFactory;
  delete mFlowFactory;
  delete mElaborate;
  delete mReachableAliases;
  NUElabBase::deleteObjects(mSymbolTable);
  delete mSymbolTable->getHdlHier();
  delete mSymbolTable;
  delete mSymTabBOM;

  // Restore the NUDesign symbol table 
  (void)mDesign->putSymtab(mOldSymbolTable);
  mDesign = NULL;

  mStats->popIntervalTimer();
}

bool GOGlobOpts::propagateConstants(bool undrivenConstants, SInt32 verbose,
                                    RENetWarning* netWarnings)
{
  // Create an aliases class for figuring out which nets are
  // driven. We use a pessimistic version of the aliases because
  // otherwise we may treat something as undriven when it really is
  // driven.
  REAlias* alias = createAliases();
  netWarnings->putREAlias(alias);
  alias->findPessimisticAliases();
  mStats->printIntervalStatistics("Aliases");

  // Run constant propagation
  GOConstantPropagation constantPropagation(mDesign, mNetRefFactory,
					    mFlowElabFactory, mIODBNucleus,
					    mSymbolTable, alias, 
                                            mArgs,
                                            mMsgContext, mAtomicCache, mStats,
                                            undrivenConstants, verbose,
                                            netWarnings);
  mModified = constantPropagation.run();

  // All done with the alias class. We don't want to store this data
  // too long because it takes quite a bit of space.
  netWarnings->putREAlias(NULL);
  delete alias;
  return true;
}
