// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "exprsynth/ExprCallbacks.h"
#include "flow/FLNode.h"
#include "nucleus/NUUseDefNode.h"

ContinuousDriverSynthCallback::ContinuousDriverSynthCallback(ESPopulateExpr * populateExpr,
                                                             FLNode * start_flow) :
  ESCallback(populateExpr),
  mStartFlow(start_flow),
  mStartFlowEncountered(false)
{
}

//! Destructor
ContinuousDriverSynthCallback::~ContinuousDriverSynthCallback() {}

//! Determine when to stop the expansion.
/*!
  Stop the expression expansion whenever we encounter a continuous
  driver or encounter the starting flow more than once (meaning
  we're in a cycle).

  \param flow         The current flow node.
  \param isContDriver Is this a continuous driver?

  \return true when we reach a stopping point; either continuous
          driver or multiple visits to the starting points.
 */
bool ContinuousDriverSynthCallback::stop(FLNode * flow, bool isContDriver) {
  if (flow==mStartFlow) {
    if (mStartFlowEncountered) {
      // if we've already seen our starting point, subsequent visits
      // mean we're in a cycle.
      return true;
    } else {
      // remember that we've seen the starting point.
      mStartFlowEncountered = true;
      return false;
    }
  } else {
    // continuous drivers are stopping points.
    return isContDriver;
  }
}

//! overload
CarbonExpr*
ContinuousDriverSynthCallback::createIdentExpr(const FLNodeVector& flows,
                                               UInt32 bitSize, bool isSigned,
                                               bool hardStop,
                                               NUNetRefHdl* resNetRef)
{
  // verify that every driver is continuous
  for (UInt32 i = 0, n = flows.size(); i < n; ++i) {
    FLNode* flow = flows[i];
    NUUseDefNode* ud = flow->getUseDefNode();
    if ((ud != NULL) && !ud->isContDriver()) {
      return false;
    }
  }

  return ESCallback::createIdentExpr(flows, bitSize, isSigned, hardStop, resNetRef);
}
