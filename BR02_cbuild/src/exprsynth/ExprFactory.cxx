// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "exprsynth/Expr.h"
#include "exprsynth/ExprFactory.h"
#include "util/Zstream.h"
#include "symtab/STSymbolTable.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"

static const char* cExprDBContextSig = "ExprDBContext";
static const UInt32 cExprDBContextVersion = 0;

class ExprWalkerDB : public CarbonTransformWalker
{
public:
  CARBONMEM_OVERRIDES

  //! constructor
  ExprWalkerDB(ESFactory* factory, ExprDBContext* dbContext, bool inclusiveNodes,
    STSymbolTable::LeafAssoc *leafAssoc) 
    : mFactory(factory), mDBExprContext(dbContext), mInclusiveNodes(inclusiveNodes),
      mLeafAssoc (leafAssoc)
  {}

  //! destructor
  ~ExprWalkerDB() {}

  virtual CarbonExpr* transformPartsel(CarbonExpr* /*expr*/, CarbonPartsel* partselOrig)
  {
    munchExpr(partselOrig);
    return partselOrig;
  }
  
  virtual CarbonExpr* transformUnaryOp(CarbonExpr* /*expr*/, CarbonUnaryOp* unaryOrig)
  {
    munchExpr(unaryOrig);
    return unaryOrig;
  }
  
  virtual CarbonExpr* transformBinaryOp(CarbonExpr* /*expr1*/, CarbonExpr* /*expr2*/,
                                        CarbonBinaryOp* binaryOrig)
  {
    munchExpr(binaryOrig);
    return binaryOrig;
  }
  
  virtual CarbonExpr* transformTernaryOp(CarbonExpr* /*cond*/,
                                         CarbonExpr* /*exprTrue*/, 
                                         CarbonExpr* /*exprFalse*/,
                                         CarbonTernaryOp* ternaryOrig)
  {
    munchExpr(ternaryOrig);
    return ternaryOrig;
  }
  
  virtual CarbonExpr* transformConcatOp(CarbonExprVector* /*exprVec*/, 
                                        UInt32 /*repeatCount*/,
                                        CarbonConcatOp* concatOrig)
  {
    munchExpr(concatOrig);
    return concatOrig;
  }

  virtual CarbonExpr* transformEdge(CarbonExpr* /*expr*/, CarbonEdge* edgeOrig) 
  {
    munchExpr(edgeOrig);
    return edgeOrig;
  }

  virtual CarbonExpr* transformIdent(CarbonIdent* ident)
  {
    return ident;
  }
  
  virtual void visitIdent(CarbonIdent* ident) { 

    // allow the ident to add any nested expressions first
    /*
      This has to be done first for ordering purposes. The unique
      expression vector needs to be setup so that any nested
      expressions are written first so the ident can reference those
      expressions as mapped pointers.
    */
    ident->nestedDBSetup(mFactory, mDBExprContext, mInclusiveNodes, mLeafAssoc);

    CarbonTransformWalker::visitIdent(ident);
    DynBitVector usageVec;
    const STAliasedLeafNode* name = ident->getNode(&usageVec);

    mDBExprContext->reserveBV(usageVec);

    STSymbolTable* localSymTab = mDBExprContext->getLocalSymTab();
    if (mInclusiveNodes)
      CE_ASSERT(localSymTab->lookup(name) != NULL, ident);
    else if (localSymTab->safeLookup(name) == NULL)
      localSymTab->translateLeaf(name, mLeafAssoc);
    munchExpr(ident);
  }

  virtual void visitConst(CarbonConst* cConst) {
    CarbonTransformWalker::visitConst(cConst);

    DynBitVector valueCp;
    cConst->getValue(&valueCp);
    mDBExprContext->reserveBV(valueCp);

    munchExpr(cConst);
  }

  virtual void visitConstXZ(CarbonConstXZ* cConstXZ) { 
    CarbonTransformWalker::visitConstXZ(cConstXZ);

    DynBitVector driveCp;
    DynBitVector valueCp;    
    cConstXZ->getValue(&valueCp, &driveCp);
    
    mDBExprContext->reserveBV(valueCp);
    mDBExprContext->reserveBV(driveCp);
    
    munchExpr(cConstXZ);
  }
  
  void finalize()
  {
    munchExpr(getResult());
  }
  
  
private:
  ESFactory* mFactory;
  ExprDBContext* mDBExprContext;
  /*
    If this is true then the local symtab in the ExprDBContext owns
    all the nodes in the CarbonIdents. Therefore, a symnode transform
    does not have to be performed.
  */
  bool mInclusiveNodes;

  STSymbolTable::LeafAssoc *mLeafAssoc;
  
  void munchExpr(CarbonExpr* expr)
  {
    mDBExprContext->addIfUnique(expr);
  }
};

ESFactory::ESFactory()
{
  mExprCache = new ExprCache;
}

void ESFactory::clear()
{
  UtArray<CarbonExpr*> tmp;
  for (ExprCacheIter p = mExprCache->begin(); p != mExprCache->end(); ++p)
    tmp.push_back(*p);
  mExprCache->clear();
  for (UtArray<CarbonExpr*>::iterator p = tmp.begin(); p != tmp.end(); ++p)
    delete *p;
}

ESFactory::~ESFactory()
{
  clear();
  delete mExprCache;
}

CarbonIdent*
ESFactory::createIdent(CarbonIdent* ident, bool& added)
{
  // Check if this expression already exists in the cache
  ExprCache::iterator pos = mExprCache->find(ident);
  CarbonIdent* expr;
  if (pos == mExprCache->end())
  {
    // Add it
    expr = ident;
    mExprCache->insert(ident);
    added = true;
  }
  else
  {
    CarbonExpr* cur = *pos;
    expr = cur->castIdent();
    added = false;
  }

  // Update the reference count and return it
  expr->incRefCnt();
  return expr;
}

CarbonUnaryOp*
ESFactory::createUnaryOp(CarbonExpr::ExprT type, CarbonExpr* subExpr,
			 UInt32 bitSize, bool isSigned)
{
  // Check if this expression already exists in the cache
  CarbonUnaryOp unaryOp(type, subExpr, bitSize, isSigned);
  CarbonUnaryOp* expr;
  ExprCache::iterator pos = mExprCache->find(&unaryOp);
  if (pos == mExprCache->end())
  {
    // Create it
    expr = new CarbonUnaryOp(type, subExpr, bitSize, isSigned);
    mExprCache->insert(expr);
  }
  else
  {
    // We found it.
    CarbonExpr* cur = *pos;
    expr = cur->castUnary();
  }

  // Update the reference count and return it
  expr->incRefCnt();
  return expr;
}

CarbonBinaryOp*
ESFactory::createBinaryOp(CarbonExpr::ExprT type, CarbonExpr* subExpr1,
			  CarbonExpr* subExpr2, UInt32 bitSize,
                          bool isSigned, bool computeSigned)
{
  // Check if this expression already exists in the cache
  CarbonBinaryOp binaryOp(type, subExpr1, subExpr2, bitSize,
                          isSigned, computeSigned);
  CarbonBinaryOp* expr;
  ExprCache::iterator pos = mExprCache->find(&binaryOp);
  if (pos == mExprCache->end())
  {
    // Create it
    expr = new CarbonBinaryOp(type, subExpr1, subExpr2, bitSize,
                              isSigned, computeSigned);
    mExprCache->insert(expr);
  }
  else
  {
    // We found it.
    CarbonExpr* cur = *pos;
    expr = cur->castBinary();
  }

  // Update the reference count and return it
  expr->incRefCnt();
  return expr;
}

CarbonTernaryOp*
ESFactory::createTernaryOp(CarbonExpr::ExprT type, CarbonExpr* subExpr1,
			   CarbonExpr* subExpr2, CarbonExpr* subExpr3,
			   UInt32 bitSize, bool isSigned, bool computeSigned)
{
  // Check if this expression already exists in the cache
  CarbonTernaryOp ternaryOp(type, subExpr1, subExpr2, subExpr3, bitSize,
                            isSigned, computeSigned);
  CarbonTernaryOp* expr;
  ExprCache::iterator pos = mExprCache->find(&ternaryOp);
  if (pos == mExprCache->end())
  {
    // Create it
    expr = new CarbonTernaryOp(type, subExpr1, subExpr2, subExpr3, bitSize,
                               isSigned, computeSigned);
    mExprCache->insert(expr);
  }
  else
  {
    // We found it.
    CarbonExpr* cur = *pos;
    expr = cur->castTernary();
  }

  // Update the reference count and return it
  expr->incRefCnt();
  return expr;
}

CarbonEdge*
ESFactory::createEdge(ClockEdge clockEdge, CarbonExpr * subExpr, UInt32 bitSize)
{
  // Check if this expression already exists in the cache
  CarbonEdge edgeCExpr(clockEdge, subExpr, bitSize);
  CarbonEdge* expr;
  ExprCache::iterator pos = mExprCache->find(&edgeCExpr);
  if (pos == mExprCache->end())
  {
    // Create it
    expr = new CarbonEdge(clockEdge, subExpr, bitSize);
    mExprCache->insert(expr);
  }
  else
  {
    // We found it.
    CarbonExpr* cur = *pos;
    expr = cur->castEdge();
  }

  // Update the reference count and return it
  expr->incRefCnt();
  return expr;
}

CarbonConst*
ESFactory::createConst(SInt64 val, UInt32 bitSize, bool isSigned)
{
  // Convert the signed value to an unsigned value and a sign bit
  CarbonConst::SignT sign = CarbonExpr::eUnsigned;
  UInt64 uVal = val;
  if (isSigned) {
    if (val < 0)
    {
      sign = CarbonConst::eNeg;
      uVal = -val;
    }
    else
    {
      sign = CarbonConst::ePos;
    }
  }

  // Create the constant from a dynamic bit vector
  DynBitVector dynVal(bitSize, uVal);
  return createConst(dynVal, sign, bitSize);
}

CarbonConst*
ESFactory::createConst(DynBitVector& val, CarbonConst::SignT sign,
		       UInt32 bitSize)
{
  // Check if this expression already exists in the cache
  CarbonConst cConst(val, sign, bitSize);
  CarbonConst* expr;
  ExprCache::iterator pos = mExprCache->find(&cConst);
  if (pos == mExprCache->end())
  {
    // Create it
    expr = new CarbonConst(val, sign, bitSize);
    mExprCache->insert(expr);
  }
  else
  {
    CarbonExpr* cur = *pos;
    expr = cur->castConst();
  }

  // Update the reference count and return it
  expr->incRefCnt();
  return expr;
}

CarbonConstReal*
ESFactory::createConstReal(double val, CarbonConst::SignT sign)
{
  // Check if this expression already exists in the cache
  CarbonConstReal cConst(val, sign);
  CarbonConstReal* expr;
  ExprCache::iterator pos = mExprCache->find(&cConst);
  if (pos == mExprCache->end())
  {
    // Create it
    expr = new CarbonConstReal(val, sign);
    mExprCache->insert(expr);
  }
  else
  {
    CarbonExpr* cur = *pos;
    expr = cur->castConstReal();
  }

  // Update the reference count and return it
  expr->incRefCnt();
  return expr;
}


CarbonConstXZ*
ESFactory::createConstZ(UInt32 bitSize, bool isSigned)
{
  // Create the constant values
  DynBitVector val(bitSize, 0);
  DynBitVector drive(bitSize, 0);
  drive.flip();
  CarbonExpr::SignT sign = isSigned? CarbonConst::ePos: CarbonConst::eUnsigned;
  return createConstXZ(val, drive, sign, bitSize);
}

CarbonConstXZ*
ESFactory::createConstX(UInt32 bitSize, bool isSigned)
{
  // Create the constant values
  DynBitVector val(bitSize, 0);
  DynBitVector drive(bitSize, 0);
  val.flip();
  drive.flip();
  CarbonExpr::SignT sign = isSigned? CarbonConst::ePos: CarbonConst::eUnsigned;
  return createConstXZ(val, drive, sign, bitSize);
}

CarbonConst*
ESFactory::createConstAllOnes(UInt32 bitSize, bool isSigned)
{
  // Create the constant values
  DynBitVector val(bitSize, 0);
  val.flip();
  CarbonExpr::SignT sign = isSigned? CarbonConst::ePos: CarbonConst::eUnsigned;
  return createConst(val, sign, bitSize);
}

CarbonConstXZ*
ESFactory::createConstXZ(DynBitVector& val, DynBitVector& drive,
			 CarbonConst::SignT sign, UInt32 bitSize)
{
  // Check if this expression already exists in the cache
  CarbonConstXZ cConst(val, drive, sign, bitSize);
  CarbonConstXZ* expr;
  ExprCache::iterator pos = mExprCache->find(&cConst);
  if (pos == mExprCache->end())
  {
    // Create it
    expr = new CarbonConstXZ(val, drive, sign, bitSize);
    mExprCache->insert(expr);
  }
  else
  {
    CarbonExpr* cur = *pos;
    expr = cur->castConstXZ();
  }

  // Update the reference count and return it
  expr->incRefCnt();
  return expr;
}

CarbonConcatOp*
ESFactory::createConcatOp(const CarbonExprVector* exprs, UInt32 repeatCount,
			  UInt32 bitSize, bool isSigned)
{
  // Check if this expression already exists in the cache
  CarbonConcatOp concat(exprs, repeatCount, bitSize, isSigned);
  CarbonConcatOp* expr;
  ExprCache::iterator pos = mExprCache->find(&concat);
  if (pos == mExprCache->end())
  {
    // Create it
    expr = new CarbonConcatOp(exprs, repeatCount, bitSize, isSigned);
    mExprCache->insert(expr);
  }
  else
  {
    CarbonExpr* cur = *pos;
    expr = cur->castConcat();
  }

  // Update the reference count and return it
  expr->incRefCnt();
  return expr;
}

CarbonPartsel*
ESFactory::createPartsel(CarbonExpr* expr, const ConstantRange& range,
			 UInt32 bitSize, bool isSigned)
{
  CarbonPartsel partsel(expr, range, bitSize, isSigned);
  CarbonPartsel* retExpr;
  ExprCache::iterator pos = mExprCache->find(&partsel);
  if (pos == mExprCache->end())
  {
    // Create it
    retExpr = new CarbonPartsel(expr, range, bitSize, isSigned);
    mExprCache->insert(retExpr);
  }
  else
  {
    CarbonExpr* cur = *pos;
    retExpr = cur->castPartsel();
  }

  // Update the reference count and return it
  retExpr->incRefCnt();
  return retExpr;
}

CarbonExpr* ESFactory::createZeroPad (CarbonExpr* expr, UInt32 bitSize)
{
  SInt32 padLen = bitSize - expr->getBitSize ();
  INFO_ASSERT (padLen >= 0, "CarbonExpr with negative width pad.");

  if (padLen == 0)
    return expr;                // Already there.

  CarbonExprVector conc;
  CarbonConst* zero = createConst (0, padLen, false);
  conc.push_back (zero);
  conc.push_back (expr);
  return createConcatOp (&conc, 1, bitSize, false);
}

CarbonExpr* ESFactory::createNeqZero(CarbonExpr* expr, UInt32 bitSize)
{
  CarbonExpr* beqz = createEqZero(expr, 1);
  CarbonExpr* bnez = createUnaryOp(CarbonExpr::eUnBitNeg, beqz, 1, false);  // not signed

  return createZeroPad (bnez, bitSize);
}

CarbonExpr* ESFactory::createEqZero(CarbonExpr* expr, UInt32 bitSize)
{
  CarbonConst* zero = createConst(0, expr->getBitSize(), expr->isSigned());
  CarbonExpr* beqZero = createBinaryOp(CarbonExpr::eBiEq, expr, zero, 1,
                                       false, false); // not signed
  return createZeroPad (beqZero, bitSize);
}

CarbonExpr* ESFactory::createNeqAllOnes(CarbonExpr* expr, UInt32 bitSize)
{
  CarbonExpr* beqOnes = createEqAllOnes(expr, 1);
  CarbonExpr* bneOnes = createUnaryOp(CarbonExpr::eUnBitNeg, beqOnes, 1,
                                      false);  // not signed
  return createZeroPad (bneOnes, bitSize);
}

CarbonExpr* ESFactory::createEqAllOnes(CarbonExpr* expr, UInt32 bitSize)
{
  CarbonConst* ones = createConstAllOnes(expr->getBitSize(), expr->isSigned());
  CarbonExpr* beqOnes = createBinaryOp(CarbonExpr::eBiEq, expr, ones, 1,
                                       false, false); // not signed
  return createZeroPad (beqOnes, bitSize);
}

static bool sameSign(CarbonConst::SignT sign, bool isSigned)
{
  return (((sign == CarbonConst::eUnsigned) && !isSigned) ||
          ((sign != CarbonConst::eUnsigned) && isSigned));
}

CarbonConst::SignT
ESFactory::convertSign(CarbonConst::SignT sign, bool isSigned,
                       const DynBitVector& val)
{
  // Check if we are converting at all
  if (sameSign(sign, isSigned)) {
    return sign;
  }

  // We must be converting it
  CarbonConst::SignT newSign;
  if (isSigned) {
    // Convert from unsigned to signed
    size_t size = val.size();
    if (val.test(size-1)) {
      newSign = CarbonConst::eNeg;
    } else {
      newSign = CarbonConst::ePos;
    }
  } else {
    // Convert from signed to unsigned
    newSign = CarbonConst::eUnsigned;
  }
  return newSign;
}

CarbonConst::SignT
ESFactory::convertSign(CarbonConst::SignT sign, bool isSigned,
                       double val)
{
  // Check if we are converting at all
  if (sameSign(sign, isSigned)) {
    return sign;
  }

  // We must be converting it
  CarbonConst::SignT newSign;
  if (isSigned) {
    // Convert from unsigned to signed
    if (val < 0) {
      newSign = CarbonConst::eNeg;
    } else {
      newSign = CarbonConst::ePos;
    }
  } else {
    // Convert from signed to unsigned
    newSign = CarbonConst::eUnsigned;
  }
  return newSign;
}


void ESFactory::setupDBContext(ExprDBContext* dbWriteContext,
                               const CarbonExpr* expr,
                               bool inclusiveNodes,
                               STSymbolTable::LeafAssoc *leafAssoc)
{
  ExprWalkerDB walk(this, dbWriteContext, inclusiveNodes, leafAssoc);
  walk.visitExpr(const_cast<CarbonExpr*>(expr));
  walk.finalize();
}

/* ExprDBContext definition */

ExprDBContext::ExprDBContext(STSymbolTable* symtab, DynBitVectorFactory* bvPool) : 
  mSymTab(symtab),
  mBVPool(bvPool)
{}

ExprDBContext::~ExprDBContext()
{
  postDBClean();
}

void ExprDBContext::addIfUnique(CarbonExpr* expr)
{
  if (mUniqueExprs.find(expr) == mUniqueExprs.end())
  {
    mUniqueExprs[expr] = mUniqExprVec.size();
    mUniqExprVec.push_back(expr);
  }
}


UInt32 ExprDBContext::getIndex(const CarbonExpr* expr) const
{
  ExprMap::const_iterator p = mUniqueExprs.find(const_cast<CarbonExpr*>(expr));
  CE_ASSERT(p != mUniqueExprs.end(), expr);
  return p->second;
}

CarbonExpr* ExprDBContext::getExpr(UInt32 index)
{
  INFO_ASSERT(index < mUniqExprVec.size(),
              "Retrieving a CarbonExpr that is not in the database");
  return mUniqExprVec[index];
}

void ExprDBContext::writeExprs(ZostreamDB& db) const
{
  db << cExprDBContextSig;
  db << cExprDBContextVersion;

  size_t numExprs = mUniqueExprs.size();
  db << UInt32(numExprs);
  for (size_t i = 0; i < numExprs; ++i)
  {
    CarbonExpr* expr = mUniqExprVec[i];
    writeCommon(expr, db);
      
    CarbonIdent* ident;
    CarbonConst* constExpr;
    CarbonUnaryOp* unary;
    CarbonBinaryOp* binary;
    CarbonTernaryOp* ternary;
    CarbonConcatOp* concat;
    if ((ident = expr->castIdent()))
      writeIdent(ident, i, db);
    else if ((constExpr = expr->castConst()))
      writeConst(constExpr, db);
    else if ((unary = expr->castUnary()))
      writeUnary(unary, i, db);
    else if ((binary = expr->castBinary()))
      writeBinary(binary, i, db);
    else if ((ternary = expr->castTernary()))
      writeTernary(ternary, i, db);
    else if ((concat = expr->castConcat()))
      writeConcat(concat, i, db);
    else 
      CE_ASSERT("Unexpected CarbonExpr type", expr);
  }
}

void ExprDBContext::readExprs(ZistreamDB& db, ESFactory* factory)
{
  UtString signature;
  if (! (db >> signature))
    return;

  if (signature.compare(cExprDBContextSig) != 0)
  {
    UtString buf;
    buf << "Invalid DB Context signature: " << signature;
    db.setError(buf.c_str());
    return;
  }

  UInt32 version;
  db >> version;
  if (db.fail())
    // the caller checks the db status.
    return;
  
  if (version > cExprDBContextVersion)
  {
    UtString buf;
    buf << "Unsupported expression context version: " << version;
    db.setError(buf.c_str());
    return;
  }
  
  UInt32 numExprs;
  db >> numExprs;
  mUniqExprVec.reserve(numExprs);
  for (UInt32 i = 0; i < numExprs; ++i)
  {
    ReadClosure closure(db, factory);
    readCommon(closure);

    INFO_ASSERT(CarbonExpr::isValidOp(closure.mOp),
                "Invalid op during file readback.");
      
    if (CarbonExpr::isIdent(closure.mOp))
      readIdent(closure);
    else if (CarbonExpr::isConst(closure.mOp) ||
	     CarbonExpr::isConstXZ(closure.mOp))
      readConst(closure);
    else if (CarbonExpr::isUnaryOp(closure.mOp))
      readUnary(closure);
    else if (CarbonExpr::isBinaryOp(closure.mOp))
      readBinary(closure);
    else if (CarbonExpr::isTernaryOp(closure.mOp))
      readTernary(closure);
    else if (CarbonExpr::isConcatOp(closure.mOp))
      readConcat(closure);
    else
      INFO_ASSERT(0, "Unexpected CarbonExpr type");
  }
}

STSymbolTable* ExprDBContext::getLocalSymTab()
{
  return mSymTab;
}

UInt32 ExprDBContext::checkExpr(CarbonExpr* expr, size_t i) const
{
  UInt32 index = getIndex(expr);
  CE_ASSERT(index < i, expr);
  return index;
}

void ExprDBContext::finalizeExpr(ReadClosure& /*closure*/, CarbonExpr* newExpr)
{
  addIfUnique(newExpr);
}

void ExprDBContext::writeCommon(const CarbonExpr* expr, ZostreamDB& db) const
{
  SInt32 type = static_cast<SInt32>(expr->getType());
  db << type;
  db << expr->getBitSize();
  db << expr->isSigned();
}

void ExprDBContext::readCommon(ReadClosure& closure)
{
  SInt32 type;
  closure.mDB >> type;
  closure.mOp = static_cast<CarbonExpr::ExprT>(type);
  INFO_ASSERT(CarbonExpr::isValidOp(closure.mOp),
              "Invalid expression op during file readback.");
  closure.mDB >> closure.mBitSize;
  closure.mDB >> closure.mIsSigned;
}

ConstantRange* ExprDBContext::createConstantRange() const
{
  ConstantRange* ret = new ConstantRange;
  mRanges.push_back(ret);
  return ret;
}

void ExprDBContext::writeIdent(const CarbonIdent* ident, size_t i, ZostreamDB& db) const
{
  // Write the node type and any nested expression pointers
  {
    CarbonExprVector addPointers;
    db << (SInt32) ident->addNestedToDB(&addPointers);
    SInt32 numAddPtrs = addPointers.size();
    db << numAddPtrs;
    for (SInt32 j = 0; j < numAddPtrs; ++j)
      db << checkExpr(addPointers[j], i);
  }
  
  // Get the leaf node and usage vector
  DynBitVector usageVecCp;
  const STAliasedLeafNode* node = ident->getNode(&usageVecCp);
  CE_ASSERT(node != NULL, ident); // must be an elaborated ident
  const DynBitVector* usageVec =  mBVPool->find(usageVecCp);
  CE_ASSERT(usageVec, ident);

  const STSymbolTableNode* translatedNameNode = mSymTab->safeLookup(node);
  // This could only happen if somehow the expression walker screwed
  // up and didn't populate the symtab with this name
  ST_ASSERT(translatedNameNode, node);
  const STAliasedLeafNode* translatedName = translatedNameNode->castLeaf();
  // This could only happen if somebody changed the symboltable's code
  // for the check for branches v. leaves when doing a translation
  ST_ASSERT(translatedName, translatedNameNode);
  db.writePointer(translatedName);
  db.writePointer(usageVec);

  ConstantRange declRange;
  if (ident->getDeclaredRange(&declRange))
    db << SInt32(1) << declRange.getMsb() << declRange.getLsb();
  else
    db << SInt32(0);
} // void ExprDBContext::writeIdent

void ExprDBContext::readIdent(ReadClosure& closure)
{
  CbuildShellDB::CarbonIdentType identType = CbuildShellDB::eSymTabIdent;
  {
    SInt32 identTypeCast;
    closure.mDB >> identTypeCast;
    identType = (CbuildShellDB::CarbonIdentType) identTypeCast;
    INFO_ASSERT(identType >= 0, 
                "Invalid indent expression type during file readback");
    INFO_ASSERT(identType <= CbuildShellDB::eSymTabIdentBP,
                "Invalid indent expression type during file readback");
  }
  
  CarbonExprVector exprVec;
  {

    SInt32 numNested = 0;
    closure.mDB >> numNested;
    INFO_ASSERT((numNested >= 0) && (numNested < 2),
                "Invalid nesting during file readback");
    for (SInt32 i = 0; i < numNested; ++i)
    {
      CarbonExpr* tmpExpr = NULL;
      UInt32 exprIndex;
      closure.mDB >> exprIndex;
      tmpExpr = getExpr(exprIndex);
      exprVec.push_back(tmpExpr);
    } 
  }
  
  STAliasedLeafNode* node = NULL; // silence GCC
  DynBitVector* usageVec = NULL;
  closure.mDB.readPointer(&node);
  closure.mDB.readPointer(&usageVec);

  // There must be a node associated with the expr
  INFO_ASSERT(node, "Could not find a node that matches an expression leaf");

  CarbonIdent* ident = createIdent(node, closure.mBitSize, usageVec, 
                                   identType, &exprVec);

  ST_ASSERT(ident, node);
  bool added;
  CarbonIdent* retExpr = closure.mFactory->createIdent(ident, added);
  if (! added)
    destroyIdent(ident);
  
  SInt32 hasRange;
  bool success = (closure.mDB >> hasRange);
  ST_ASSERT(success, node);
  if (hasRange == 1)
  {
    SInt32 msb, lsb;
    if ((closure.mDB >> msb) && (closure.mDB >> lsb))
    {
      ConstantRange declRange(msb, lsb);
      retExpr->putDeclaredRange(&declRange);
    }
    else
      ST_ASSERT(0, node);
  }

  finalizeExpr(closure, retExpr);
}
  
void ExprDBContext::writeConst(const CarbonConst* constExpr, ZostreamDB& db) const
{
  // write the sign information
  CarbonConst::SignT signInfo = constExpr->getSignInfo();
  db << static_cast<SInt32>(signInfo);
  
  
  // if this is a constxz than we have to write the drive out as
  // well
  const CarbonConstXZ* constxz = constExpr->castConstXZ();
  if (constxz)
  {
    DynBitVector driveCp;
    DynBitVector valueCp;    
    constxz->getValue(&valueCp, &driveCp);
    
    const DynBitVector* value = mBVPool->find(valueCp);
    const DynBitVector* drive = mBVPool->find(driveCp);
    CE_ASSERT(value, constExpr);
    CE_ASSERT(drive, constExpr);
    db.writePointer(value);
    db.writePointer(drive);
  }
  else
  {
    DynBitVector valueCp;
    constExpr->getValue(&valueCp);
    const DynBitVector* value = mBVPool->find(valueCp);
    CE_ASSERT(value, constExpr);
    db.writePointer(value);
  }
}

void ExprDBContext::readConst(ReadClosure& closure)
{
  // read the sign
  SInt32 signTmp;
  closure.mDB >> signTmp;
  CarbonConst::SignT signInfo = static_cast<CarbonConst::SignT>(signTmp);
  DynBitVector* value = NULL;   // silence gcc
  closure.mDB.readPointer(&value);
  bool hasUnknowns = CarbonExpr::isConstXZ(closure.mOp);
  CarbonExpr* expr = NULL;
  if (hasUnknowns)
  {
    DynBitVector* drive = NULL;
    closure.mDB.readPointer(&drive);
    expr = 
      closure.mFactory->createConstXZ(*value, *drive, signInfo,
                                      closure.mBitSize);
  }
  else
    expr = closure.mFactory->createConst(*value, signInfo, closure.mBitSize);
  
  finalizeExpr(closure, expr);
}

void ExprDBContext::writeUnary(const CarbonUnaryOp* unary, size_t i, ZostreamDB& db) const
{
  CarbonExpr* expr = unary->getArg(0);
  UInt32 exprIndex = checkExpr(expr, i);
  db << exprIndex;
  // if this is a partsel then write the range as well
  const CarbonPartsel* partsel = unary->castPartsel();
  if (partsel)
  {
    const ConstantRange& range = partsel->getRange();
    db.writeObject(range);
  }
}

void ExprDBContext::readUnary(ReadClosure& closure)
{
  // Get the expression index
  UInt32 exprIndex = 0;
  closure.mDB >> exprIndex;
  // Get the expression operated on
  CarbonExpr* subExpr = getExpr(exprIndex);

  // If this is a partsel, read the range
  bool isPartsel = CarbonExpr::isPartsel(closure.mOp);
  CarbonExpr* unary;
  if (isPartsel)
  {
    ConstantRange* range = createConstantRange();
    closure.mDB.readObject(range);
    unary = closure.mFactory->createPartsel(subExpr, *range, closure.mBitSize,
                                            closure.mIsSigned);
  }
  else
    unary = closure.mFactory->createUnaryOp(closure.mOp, subExpr,
					    closure.mBitSize,
                                            closure.mIsSigned);

  finalizeExpr(closure, unary);
}

void ExprDBContext::writeBinary(const CarbonBinaryOp* binary, size_t i, ZostreamDB& db) const
{
  UInt32 exprIndex = checkExpr(binary->getArg(0), i);
  db << exprIndex;
  exprIndex = checkExpr(binary->getArg(1), i);
  db << exprIndex;
}

void ExprDBContext::readBinary(ReadClosure& closure)
{
  // Get the expression index for expr1
  UInt32 exprIndex = 0;
  closure.mDB >> exprIndex;
  CarbonExpr* subExpr1 = getExpr(exprIndex);
  closure.mDB >> exprIndex;
  CarbonExpr* subExpr2 = getExpr(exprIndex);

  CarbonExpr* binary = 
    closure.mFactory->createBinaryOp(closure.mOp, subExpr1, subExpr2,
				     closure.mBitSize,
                                     closure.mIsSigned,
                                     false);

  finalizeExpr(closure, binary);
}

void ExprDBContext::writeTernary(const CarbonTernaryOp* ternary, size_t i, 
                                 ZostreamDB& db) const
{
  UInt32 exprIndex = checkExpr(ternary->getArg(0), i);
  db << exprIndex;
  exprIndex = checkExpr(ternary->getArg(1), i);
  db << exprIndex;
  exprIndex = checkExpr(ternary->getArg(2), i);
  db << exprIndex;
}

void ExprDBContext::readTernary(ReadClosure& closure)
{
  // Get the expression index for expr1
  UInt32 exprIndex = 0;
  closure.mDB >> exprIndex;
  CarbonExpr* subExpr1 = getExpr(exprIndex);
  closure.mDB >> exprIndex;
  CarbonExpr* subExpr2 = getExpr(exprIndex);
  closure.mDB >> exprIndex;
  CarbonExpr* subExpr3 = getExpr(exprIndex);

  CarbonExpr* ternary = 
    closure.mFactory->createTernaryOp(closure.mOp, subExpr1, subExpr2,
                                      subExpr3, closure.mBitSize,
                                      closure.mIsSigned, false);
  finalizeExpr(closure, ternary);
}

void ExprDBContext::writeConcat(const CarbonConcatOp* concat, size_t i, ZostreamDB& db) const
{
  db << concat->getRepeatCount();
  UInt32 numArgs = concat->getNumArgs();
  db << numArgs;
  for (UInt32 j = 0; j < numArgs; ++j)
  {
    UInt32 exprIndex = checkExpr(concat->getArg(j), i);
    db << exprIndex;
  }
}

void ExprDBContext::readConcat(ReadClosure& closure)
{
  UInt32 repeatCount = 0;
  closure.mDB >> repeatCount;

  UInt32 numArgs = 0;
  closure.mDB >> numArgs;
  CarbonExprVector vec;
  for (UInt32 j = 0; j < numArgs; ++j)
  {
    UInt32 exprIndex;
    closure.mDB >> exprIndex;
    CarbonExpr* subExpr = getExpr(exprIndex);
    vec.push_back(subExpr);
  }

  CarbonExpr* concat = closure.mFactory->createConcatOp(&vec, repeatCount,
							closure.mBitSize,
                                                        closure.mIsSigned);
  finalizeExpr(closure, concat);
}

CarbonIdent* ExprDBContext::createIdent(STAliasedLeafNode*, 
                                        UInt32,
                                        const DynBitVector*,
                                        CbuildShellDB::CarbonIdentType,
                                        const CarbonExprVector*)
{
  return NULL;
}

void ExprDBContext::destroyIdent(CarbonIdent*) 
{
}

void ExprDBContext::postDBClean()
{
  for (RangeVec::iterator p = mRanges.begin(); 
       p != mRanges.end(); ++p)
    delete *p;
  mRanges.clear();
}

void ExprDBContext::clear()
{
  postDBClean();
  mUniqueExprs.clear();
  mUniqExprVec.clear();
}

void ExprDBContext::reserveBV(const DynBitVector& bv)
{
  mBVPool->insert(bv);
}


void CarbonIdentExprMap::mapExpr(CarbonIdent* ident, CarbonExpr* expr)
{
  mExprMap[ident] = expr;
}

void CarbonIdentExprMap::removeMappedExpr(CarbonIdent* ident)
{
  mExprMap.erase(ident);
}


CarbonExpr* CarbonIdentExprMap::getExpr(CarbonIdent* ident)
{
  const CarbonIdentExprMap* me = const_cast<const CarbonIdentExprMap*>(this);
  return const_cast<CarbonExpr*>(me->getExpr(ident));
}

const CarbonExpr* CarbonIdentExprMap::getExpr(const CarbonIdent* ident) const
{
  IdentExprMap::const_iterator p = mExprMap.find(const_cast<CarbonIdent*>(ident));
  const CarbonExpr* ret = NULL;
  if (p != mExprMap.end())
    ret = p->second;
  return ret;
}

CarbonIdentExprMap::UnsortedCLoop 
CarbonIdentExprMap::loopCUnsorted() const
{
  return mExprMap.loopCUnsorted();
}

CarbonIdentExprMap::UnsortedLoop 
CarbonIdentExprMap::loopUnsorted() 
{
  return mExprMap.loopUnsorted();
}

void CarbonIdentExprMap::swap(CarbonIdentExprMap& other)
{
  mExprMap.swap(other.mExprMap);
}
