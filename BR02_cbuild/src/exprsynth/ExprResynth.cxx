// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "exprsynth/ExprFactory.h"
#include "exprsynth/ExprResynth.h"
#include "exprsynth/ESPopulateExpr.h"

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"

#include "localflow/ElabStateTest.h"

#include "nucleus/NUAssign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUExprFactory.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNetElabRef.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetSet.h"

#include "reduce/Fold.h"

#include "symtab/STSymbolTable.h"

class ExprResynthCallback: public ESCallbackElab
{
public:
  ExprResynthCallback(ESPopulateExpr* populateExpr, LFElabStateTest* elabStateTest,
                      ExprResynth::Mode mode)
    : ESCallbackElab(populateExpr), mMode(mode), mStartFlow(0)
  {
    mNetRefFactory = populateExpr->getNetRefFactory();
    mElabStateTest = elabStateTest;
  }

  bool isConstant(FLNodeElab* flow)
  {
//
// For the moment, this does not work -- it gets the assertion:
//
// exprsynth/ESPopulateExpr.cxx:884: CarbonExpr* ESPopulateExpr::wrappedRecursiveCreate(FLNode*, ESCallback*, bool): Assertion `"Unsupported construct" == __null' failed.
//
//    while (flow->hasNestedBlock())
//      flow = flow->getNested();
//
// Leaving the comment in so we can get more aggressive later on as 
// expression-synthesis matures
//
    NUUseDefNode* node = flow->getUseDefNode();
    if ((node != NULL) && !node->isInitial())
    {
      NUAssign* assign = dynamic_cast<NUAssign*>(node);
      if (assign != NULL)
      {
        NUExpr* rhs = assign->getRvalue();
        if (rhs->castConst() != NULL)
          return true;
      }
    }
    return false;
  }

  virtual bool stop(FLNodeElabVector* flows, const NUNetRefHdl& /* netRef */, 
                    bool /* isContDriver*/ ) {
    // For multi-drivers, stop here. Otherwise, grab the flow node
    if (flows->size() > 1)
      return true;
    INFO_ASSERT(!flows->empty(),
                "CarbonExpr creation stop called with empty data");
    FLNodeElab* flow = *(flows->begin());

    // Never stop at constants
    if (isConstant(flow))
      return false;

    // Check for sequentials, they are stopping points because their
    // RHS cannot be treated as combinational.
    NUUseDefNode* useDef = flow->getUseDefNode();
    if ((useDef != NULL) && useDef->isSequential())
      return true;

    if (mCovered.find(flow) != mCovered.end())
      return true;
    mCovered.insert(flow);

    if (mMode == ExprResynth::eStopAfterFirst)
    {
      if (flow == mStartFlow)
      {
        mStartFlow = NULL;        // avoid infinite loops for cycles
        return false;
      }
      return true;
    }

    NUNetRefHdl netRef = flow->getDefNetRef(mNetRefFactory);
    NUNet* net = flow->getFLNode()->getDefNet();

    if (((mMode & ExprResynth::eStopAtState) != 0) &&
        mElabStateTest->isState(flow))
      return true;

    if (mMode == ExprResynth::eStopAtScalar)
      return (netRef->all() && (netRef->getNumBits() == 1));
    return (mMode == ExprResynth::eStopAtContDriver) && !net->isTemp();
  }

  void putMode(ExprResynth::Mode mode) {mMode = mode;}
  void reset(FLNodeElab* flow) {
    mCovered.clear();
    mStartFlow = flow;
  }


  ExprResynth::Mode mMode;
  FLNodeElab* mStartFlow;
  FLNodeElabSet mCovered;
  NUNetRefFactory* mNetRefFactory;
  LFElabStateTest* mElabStateTest;
};

ExprResynth::ExprResynth(Mode mode, ESPopulateExpr* populateExpr,
			 NUNetRefFactory* netRefFactory, ArgProc* args,
			 MsgContext* msgContext, AtomicCache* cache, IODBNucleus *iodb)
  : mCopyContext(0, 0)
{
  mPopulateExpr = populateExpr;
  mExprCache = new ExprCache;
  mFold = new Fold(netRefFactory, args, msgContext, cache, iodb,
                   false,       // no verbose output
                   /*eFoldUDValid |*/ eFoldPreserveReset | eFoldBottomUp
                   | eFoldKeepExprs);

  mElabStateTest = new LFElabStateTest;
  mCallback = new ExprResynthCallback(mPopulateExpr, mElabStateTest, mode);
  mOwnExprFactory = true;
  mExprFactory = new NUExprFactory;
  mFold->shareExprFactory(mExprFactory);
  mNetRefFactory = netRefFactory;
}

ExprResynth::~ExprResynth()
{
  // fold's destructor navigates expressions, so it must precede
  // deletion of mExprFactory
  delete mFold;
  if (mOwnExprFactory)
    delete mExprFactory;
  delete mExprCache;
  delete mCallback;
  delete mElabStateTest;
}

void ExprResynth::putMode(Mode mode)
{
  mCallback->mMode = mode;
}

void ExprResynth::putExprFactory(NUExprFactory* factory)
{
  INFO_ASSERT(mOwnExprFactory, "Cannot change expression factory if it is owned");
  mOwnExprFactory = false;
  mFold->shareExprFactory(factory);
  delete mExprFactory;
  mExprFactory = factory;
}

const NUExpr* ExprResynth::record(NUExpr* expr)
{
  const NUExpr* cexpr = mExprFactory->insert(expr);
  cexpr = mFold->foldFactoryExpr(cexpr);
  return cexpr;
}

//! Callback to find non-dynamic partsels and idents to substitute
class ExprResynthIdentFinder : public NUDesignCallback {
public:
  ExprResynthIdentFinder(NUNetElabRefSet2* nets, STBranchNode* scope)
    : mNets(nets),
      mScope(scope)
  {
  }

  Status operator()(Phase phase, NUIdentRvalue* id) {
    if (phase == ePre) {
      mNets->insert(id->getNetElab(mScope));
      return eSkip;
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUMemselRvalue* id) {
    if (phase == ePre) {
      mNets->insert(id->getNetElab(mScope));
      return eSkip;
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUVarselRvalue* ps) {
    if (phase == ePre) {
      NUIdentRvalue* id = dynamic_cast<NUIdentRvalue*>(ps->getIdentExpr());
      if (ps->isConstIndex() && (id != NULL)) {
        NUNetRefFactory* nrf = mNets->getFactory();
        NUNet* net = id->getIdent();
        const ConstantRange* range = ps->getRange();
        NUNetRefHdl netRef = nrf->createVectorNetRef(net, *range);
        mNets->insert(id->getNetElab(mScope), netRef);
        return eSkip;
      }
    }
    return eNormal;
  }
  Status operator()(Phase, NUBase*) {
    return eNormal;
  }


  NUNetElabRefSet2* mNets;
  STBranchNode* mScope;
}; // class ExprResynthIdentFinder : public NUDesignCallback


// ExprResynth helper class to assist in doing those substitutions
// found by performing expression synthesis on a flow-node that
// defs a net, or part of a net.  When we defing part of a net,
// we can only replace a part-select.  When defing the whole net,
// we can replace any identifier.  We know what part of the net
// is def'd by the flow-node because we record it in the replace()
// method in this class.
class ExprReplace: public NuToNuFn
{
public:
  typedef std::pair<const NUExpr*, NUNetRefHdl> ExprNetRef;
  typedef UtHashMap<NUNetElab*,ExprNetRef> ReplacementMap;

  ExprReplace(CopyContext& copyContext, STBranchNode* scope,
              ExprResynth::NetMap& netMap)
    : mCopyContext(copyContext),
      mScope(scope),
      mNetMap(netMap)
  {}
  virtual NUExpr * operator()(NUExpr* expr, Phase phase)
  {
    if (isPre (phase))
      return 0;
    NUExpr* replacement = find(expr);
    if (replacement != NULL)
      delete expr;
    return replacement;
  }

  void replace(NUNetElab* netElab,
               const NUExpr* expr,
               const NUNetRefHdl& provided)
  {
    ExprNetRef exprNetRef(expr, provided);
    mReplacementMap[netElab] = exprNetRef;

    // Also map netElab's master to expr, if netElab has a master
    ExprResynth::NetMap::iterator p = mNetMap.find(netElab);
    if (p != mNetMap.end())
    {
      netElab = p->second;
      mReplacementMap[netElab] = exprNetRef;
    }
  }

  NUExpr* find(const NUExpr* expr)
  {
    const NUIdentRvalue* rv = dynamic_cast<const NUIdentRvalue*>(expr);
    if (rv != NULL) {
      NU_ASSERT(rv->isWholeIdentifier(), expr);
      return netElabToExpr(rv->getNetElab(mScope), ePrePost);
    }

    const NUVarselRvalue* pv = dynamic_cast<const NUVarselRvalue*>(expr);
    if ((pv != NULL) && pv->isConstIndex()) {
      const NUExpr* id = pv->getIdentExpr();
      rv = dynamic_cast<const NUIdentRvalue*>(id);
      if (rv != NULL) {
        NUNetElab* net = rv->getNetElab(mScope);
        ReplacementMap::iterator p = mReplacementMap.find(net);
        if (p != mReplacementMap.end()) {
          // We found an expression that defs some set of bits -- it
          // might def a superset of the bits needed for the part-select,
          // in which case we'll need to partselect the expression itself
          ExprNetRef exprNetRef = p->second;
          NUNetRefHdl bits = exprNetRef.second;
          const NUExpr* origRep = exprNetRef.first;
          NUExpr* rep = origRep->copy(mCopyContext);
          rep->resize(origRep->getBitSize());
          ConstantRange exprRange;
          NU_ASSERT2(bits->getRange(exprRange), expr, net);
          const ConstantRange* requiredRange = pv->getRange();
          NU_ASSERT2(exprRange.contains(*requiredRange), expr, net);

          // If the range is exact, we can substitute the mapped
          // expression for the partsel
          if (*requiredRange == exprRange) {
            return rep;
          }

          // Otherwise we will have to construct a new partselect
          // from the one we started with
          UInt32 sz = requiredRange->getLength();
          SInt32 lsb = requiredRange->getLsb() - exprRange.getLsb();
          SInt32 msb = lsb + sz - 1;
          NU_ASSERT2(lsb >= 0, expr, net);
          NU_ASSERT2((UInt32) msb < rep->getBitSize(), expr, net);
          ConstantRange nRange(msb, lsb);
          rep = new NUVarselRvalue(rep, nRange, pv->getLoc());
          rep->resize(sz);
          return rep; 
        } // if
      }
    }
    return NULL;
  } // NUExpr* find

  NUExpr* netToExpr(NUNet* net, Phase phase)
  {
    return netElabToExpr(net->lookupElab(mScope), phase);
  }

  NUExpr* netElabToExpr(NUNetElab* netElab, Phase phase)
  {
    if (!isPost(phase))
      return 0;
    ReplacementMap::iterator p = mReplacementMap.find(netElab);
    if (p != mReplacementMap.end())
    {
      ExprNetRef exprNetRef = p->second;
      NUNetRefHdl bits = exprNetRef.second;
      const NUExpr* rep = exprNetRef.first;
      if (bits->all()) {
        return rep->copy(mCopyContext);
      }
    }
    ExprResynth::NetMap::iterator q = mNetMap.find(netElab);
    if (q != mNetMap.end())
    {
      netElab = q->second;
      return new NUIdentRvalueElab(netElab, netElab->getNet()->getLoc());
    }
    return 0;
  }

private:
  ReplacementMap mReplacementMap;
  CopyContext& mCopyContext;
  STBranchNode* mScope;
  ExprResynth::NetMap& mNetMap;
};

FLNodeElab* ExprResynth::findFlowForNet(FLNodeElab* flow, NUNetElab* net)
{
  NUUseDefNode* node = flow->getUseDefNode();
  if ((flow->getDefNet() == net) and
      ((node == NULL) or (not node->isInitial()))) {
    return flow;
  }

  bool cont = true;
  while (cont) {
    FLNodeElab* ret = NULL;

    // Find the flow-node that defs this temp net based on the fanin
    for (Iter<FLNodeElab*> i = flow->loopFanin(); !i.atEnd(); ++i)
    {
      FLNodeElab* fanin = *i;
      node = fanin->getUseDefNode();
      if ((fanin->getDefNet() == net) and
          ((node == NULL) or (not node->isInitial()))) {
        if (ret == NULL) {
          ret = fanin;
        } else {
          return NULL;          // reject multiple drivers
        }
      }
    }
    if (ret != NULL) {
      return ret;               // a single flownode for net was found
    }
    if (flow->hasNestedBlock() && (flow->numNestedFlows() == 1)) {
      flow = flow->getSingleNested();
    } else {
      cont = false;
    }
  }
  return NULL;
} // FLNodeElab* ExprResynth::findFlowForNet

const NUExpr* ExprResynth::synthFlow(FLNodeElab* flow)
{
  const NUExpr* ret = NULL;
  mCallback->reset(flow);
  ESExprHndl hndl = mPopulateExpr->create(flow, mCallback);
  CarbonExpr* cexpr = hndl.getExpr();
  if (cexpr != NULL)
    ret = exprToNucleus(cexpr, NULL, flow->getUseDefNode()->getLoc());
  return ret;
}

/*! \brief transform an expression that may contain "temp" variables into one
 * that only contains continously driven variables
 *
 * The returned NUExpr* may be the same as the one passed in, or
 * it may be resynthesized to follow through temps.  The lifetime
 * of the returned NUExpr* is the lifetime of the ExprSynth instance.
 * if \a last_flow_used is not null and the returned expression was
 * resynthesized (through temps) then the last flow used for the
 * expansion is returned there, or it is unchanged if there was no expansion.
 */
const NUExpr* ExprResynth::buildContinuousExpr(const NUExpr* expr,
                                               FLNodeElab* flow,
                                               NUModule* mod,
                                               FLNodeElab **last_flow_used)
{
  // First, see if this expression has any temps in it.  If not, then
  // the passed-in expression can be returned.
  CopyContext copyContext(mod, NULL);
  STBranchNode* scope = flow->getHier();
  ExprReplace exprReplace(copyContext, scope, mNetMap);

  // Get all the elaborated nets used by this expression
  NUNetElabRefSet2 selectUses(mNetRefFactory);
  NUNetElabSet exprUses;
  //expr->getUsesElab(scope, &selectUses);
  ExprResynthIdentFinder idFinder(&selectUses, scope);
  NUDesignWalker walker(idFinder, false);
  walker.expr(const_cast<NUExpr*>(expr));

  UInt32 size = expr->getBitSize();

  UtVector<NUExpr*> delExprs;
  bool cont = true;

  for (NUNetElabRefSet2::iterator p = selectUses.begin();
       cont && (p != selectUses.end()); ++p)
  {
    NUNetElab* net = p->first;
    NUNetRefHdl needed = p->second;
    NUNet* localNet = net->getNet();
    FLNodeElab* nflow = findFlowForNet(flow, net);
    NUNetRefHdl provided = mNetRefFactory->createEmptyNetRef();

    if (nflow) {
      provided = nflow->getDefNetRef(mNetRefFactory);
      NU_ASSERT(!provided->empty(), net);
      NUNetRefHdl needed = selectUses.getNetRef(net);
      NU_ASSERT(!needed->empty(), net);
      ConstantRange exprRange;

      if (! provided->covers(*needed) || ! provided->getRange(exprRange)) {
        // We need the flow to continuously cover all the bits of
        // needed by the expression.  Otherwise, if we are only
        // deffing some of the required bits, then we can't do a
        // proper substitutition into the expression.
        cont = false;
        break;
      }
    }

    // do not do perform substitutions through memories
    if (localNet->isMemoryNet())
    {
      cont = false;
      break;
    }

    if ( nflow == NULL ) {
      // we got no flow, either because multiple driver, or no driver or some other (unknown) reason
      if ( net->isContinuouslyDriven() ) {
        // if continuously driven then make up a temp expression that will be used as a boundary
        NUExpr* tempExpr = new NUIdentRvalueElab(net, localNet->getLoc());
        provided = mNetRefFactory->createNetRef(net->getNet());
        exprReplace.replace(net, tempExpr, provided);
        delExprs.push_back(tempExpr);
      }
      else{
        // no flow, and there is nothing else to use in it's place
        cont = false; 
        break;
      }
    }

    if ( cont and ( nflow != NULL ) ) {
      mCallback->reset(nflow);
      ESExprHndl hndl = mPopulateExpr->create(nflow, mCallback);
      CarbonExpr* cexpr = hndl.getExpr();
      if ((cexpr == NULL) || hndl.numOpsGreaterThan(1000)) {
        cont = false;
      } else {
        const NUExpr* tempExpr = exprToNucleus(cexpr, mod, localNet->getLoc());

        // Do not subsitute an expression that includes the net
        // we are going to replace.  That will give us wrong answers
        // during clock-divider calculation.  See test/clock-aliasing/clkdiv.v
        exprUses.clear();
        tempExpr->getUsesElab(nflow->getHier(), &exprUses);
        if ((exprUses.find(net) != exprUses.end()) && provided->all()) {
          // Override the cache that was written in exprToNucleus with
          // the original variable.  Now we are just doing an elaborated
          // substition.
          tempExpr = record(new NUIdentRvalueElab(net, localNet->getLoc()));
        } else {
          // we will use this expansion, save flow if possible
          if ( last_flow_used ){
            (*last_flow_used) = nflow;
          }
        }
        
        // Should be careful here to avoid over-caching...
        (*mExprCache)[hndl] = tempExpr;

         // build a new version of 'expr', with the temp variable
         // substituted with tempExpr
        exprReplace.replace(net, tempExpr, provided);
      } // else
    } // else
  } // for

  const NUExpr* ret = NULL;
  if (cont)
  {
    NUExpr* nexpr = exprReplace.find(expr);
    if (nexpr == NULL)
    {
      nexpr = expr->copy(copyContext);
      nexpr->replaceLeaves(exprReplace);
    }

    nexpr->resize(size);
    ret = record(nexpr);
  }
    
  for (size_t i = 0; i < delExprs.size(); ++i)
    delete delExprs[i];

  return ret;
} // NUExpr* ExprResynth::buildContinuousExpr

NUExpr* ExprResynth::binOp(CarbonExpr* ce, NUModule* mod,
                           const SourceLocator& loc, NUOp::OpT op)
{
  const NUExpr* a1 = exprToNucleus(ce->getArg(0), mod, loc);
  if (a1 == NULL)
    return NULL;
  const NUExpr* a2 = exprToNucleus(ce->getArg(1), mod, loc);
  if (a2 == NULL)
  {
    delete a1;
    return NULL;
  }
  return new NUBinaryOp(op, a1, a2, loc);
}
  
NUExpr* ExprResynth::unOp(CarbonExpr* ce, NUModule* mod,
                          const SourceLocator& loc, NUOp::OpT op)
{
  const NUExpr* a1 = exprToNucleus(ce->getArg(0), mod, loc);
  if (a1 == NULL)
    return NULL;
  return new NUUnaryOp(op, a1, loc);
}
  
NUExpr* ExprResynth::ternOp(CarbonExpr* ce, NUModule* mod,
                          const SourceLocator& loc, NUOp::OpT op)
{
  const NUExpr* a1 = exprToNucleus(ce->getArg(0), mod, loc);
  if (a1 == NULL)
    return NULL;
  const NUExpr* a2 = exprToNucleus(ce->getArg(1), mod, loc);
  if (a2 == NULL)
    return NULL;
  const NUExpr* a3 = exprToNucleus(ce->getArg(2), mod, loc);
  if (a3 == NULL)
    return NULL;
  return new NUTernaryOp(op, a1, a2, a3, loc);
}
  
  
NUExpr* ExprResynth::nOp(CarbonExpr* ce, NUModule* mod,
                         const SourceLocator& loc, NUOp::OpT op)
{
  UInt32 num = ce->getNumArgs();
  NUCExprVector v(num);
  for (UInt32 i = 0; i < num; ++i)
  {
    const NUExpr* expr = exprToNucleus(ce->getArg(i), mod, loc);
    if (expr == NULL)
    {
      return NULL;
    }
    v[i] = expr;
  }
  CE_ASSERT(op == NUOp::eNaConcat, ce);
  const CarbonConcatOp* concat = ce->castConcat();
  CE_ASSERT(concat != NULL, ce);
  return new NUConcatOp(v, concat->getRepeatCount(), loc);
}
  
const NUExpr* ExprResynth::exprToNucleus(CarbonExpr* ce, NUModule* mod,
                                         const SourceLocator& loc)
{
  ESExprHndl hndl(ce);
  ExprCache::iterator p = mExprCache->find(hndl);
  if (p != mExprCache->end())
    return p->second;
  NUExpr* expr = e2n(ce, mod, loc);
  UInt32 bitSize = ce->getBitSize();
  bool is_signed = ce->isSigned();

  if (expr->isManaged()) {
    if ((expr->getBitSize() != bitSize) ||
        (expr->isSignedResult() != is_signed))
    {
      CopyContext cc(NULL, NULL);
      expr = expr->copy(cc);
      expr->setSignedResult(is_signed);
      expr = expr->makeSizeExplicit(bitSize);
    }
  }
  else {
    expr->setSignedResult(is_signed);
    expr = expr->makeSizeExplicit(bitSize);
  }
  const NUExpr* cexpr = record(expr);
  (*mExprCache)[hndl] = cexpr;
  return cexpr;
}

// A CarbonExpr identifier may in fact be a part-select or a
// bit-select or potentially even a concat of part-selects &
// bit-selects.  We have to look at the NUNetRef inside the flow
// to find out.
NUExpr* ExprResynth::translateIdent(CarbonExpr* ce, const SourceLocator& loc)
{
  // Get the net ref for these flows
  NUExpr * ne = NULL;

  ESFlowIdent* unelab_ident = ce->castESFlowIdent();  
  if (unelab_ident) {
    NUNetRefHdl netRef = unelab_ident->getNetRef();
    NUNet * net = netRef->getNet();
    CE_ASSERT(net, ce);

    if (netRef->all()) {
      ne = new NUIdentRvalue(net, loc);
    } else if (net->isVectorNet()) {
      // generate a part select
      const NUExpr* id = mExprFactory->insert(new NUIdentRvalue(net, loc));
      NUExprVector exprs;
      for (NUNetRefRangeLoop p = netRef->loopRanges(mNetRefFactory);
           !p.atEnd(); ++p)
      {
        const ConstantRange& r = *p;
        exprs.push_back(mFold->createPartsel(id, r, loc));
      }
      CE_ASSERT(!exprs.empty(), ce);    // something has to be set!
      if (exprs.size() > 1) {
        NUExprVector concat;
        for (NUExprVector::reverse_iterator i = exprs.rbegin();
             i != exprs.rend();
             ++i) {
          NUExpr* expr = *i;
          concat.push_back(expr);
        }
        ne = new NUConcatOp(concat, 1, loc);
      } else {
        ne = exprs[0];
      }
    }
    else {
      NUMemoryNet* mem = net->getMemoryNet();
      NU_ASSERT(mem && mem->isResynthesized(), net);

      // generate a mem select.  Need to denormalize the row
      const ConstantRange* depthRange = mem->getDepthRange();

      // What do we do if we get more than one memory row in this netref?
      UInt32 numMemRows = 0;
      for (NUNetRefRangeLoop p = netRef->loopRanges(mNetRefFactory);
           !p.atEnd(); ++p)
      {
        const ConstantRange& memRows = *p;
        NU_ASSERT(memRows.getLength() == 1, net);
        SInt32 index = depthRange->index(memRows.getLsb());
        NUConst* idx = NUConst::create(index < 0, std::abs(index), 32, loc);
        NUMemselRvalue *msel = new NUMemselRvalue(net, idx, loc);
        msel->putResynthesized( true ); // resynth has already happened
        ne = msel;
        ++numMemRows;
      }
      NU_ASSERT(numMemRows == 1, mem);
    } // else
  } else {
    ESFlowElabIdent* ident = ce->castESFlowElabIdent();
    CE_ASSERT(ident, ce);
    NUNetRefHdl netRef = ident->getNetRef();
    FLNodeElab* flow = ident->getFlowElab();
    NUNetElab* net = flow->getDefNet();
    NUNetElab* master = NULL;

    // See if this net was aliased by the user via mapNet() method
    NetMap::iterator p = mNetMap.find(net);
    if (p != mNetMap.end())
      master = p->second;

    if (netRef->all())
    {
      if (master != NULL)
        net = master;
      ne = new NUIdentRvalueElab(net, loc);
    }
    else
    {
      NUVectorNet* vec = dynamic_cast<NUVectorNet*>(net->getNet());
      NU_ASSERT(vec, net);

      // Generate a part-select.  The master/alias ranges better be
      // consistent.  We should probably handle if they are not, but at
      // present, mapNet() is only for clocks which are not vectors.
      if (master != NULL)
      {
        NUVectorNet* masterVec = dynamic_cast<NUVectorNet*>(master->getNet());
        NU_ASSERT(masterVec, master);
        NU_ASSERT(*vec->getRange() == *masterVec->getRange(), master);
        vec = masterVec;
        net = master;
      }

      // Is this a part-select, a bit-select, or a concat?
      const NUExpr* id = mExprFactory->insert(new NUIdentRvalueElab(net, loc));
      NUExprVector exprs;
      for (NUNetRefRangeLoop p = netRef->loopRanges(mNetRefFactory);
           !p.atEnd(); ++p) {
        const ConstantRange& r = *p;
        exprs.push_back(mFold->createPartsel(id, r, loc));
      }
      NU_ASSERT(!exprs.empty(), id);    // something has to be set!
      if (exprs.size() > 1) {
        NUExprVector concat;
        for (NUExprVector::reverse_iterator i = exprs.rbegin();
             i != exprs.rend();
             ++i) {
          NUExpr* expr = *i;
          concat.push_back(expr);
        }
        ne = new NUConcatOp(concat, 1, loc);
      } else {
        ne = exprs[0];
      }
    }
  }
  CE_ASSERT(ne, ce);
  return ne;
} // NUExpr* ExprResynth::translateIdent

NUExpr* ExprResynth::e2n(CarbonExpr* ce, NUModule* mod,
                         const SourceLocator& loc)
{
  NUExpr* ne = NULL;

  switch (ce->getType())
  {
  case CarbonExpr::eIdent:
    ne = translateIdent(ce, loc);
    break;
  case CarbonExpr::eConst:
  {
    DynBitVector v;
    ce->getValue(&v);
    ne = NUConst::create (false, v, v.size(), loc);
    break;
  }
  case CarbonExpr::eConstXZ:
  {
    DynBitVector v, k;
    ce->getValue(&v, &k);
    ne = NUConst::createXZ (false, v, k, v.size(), loc);
    break;
  }
  case CarbonExpr::eConstReal:
  {
    CarbonConstReal *constReal = ce->castConstReal();
    CE_ASSERT( constReal, ce );

    double value;
    constReal->getValue(&value);
    return  NUConst::create(value, loc);
    break;
  }

  // Make buffers disappear from expressions
  case CarbonExpr::eUnBuf:
    ne = const_cast<NUExpr*>(exprToNucleus(ce->getArg(0), mod, loc));
    break;
  case CarbonExpr::eUnMinus:  ne = unOp(ce, mod, loc, NUOp::eUnMinus);   break;
  case CarbonExpr::eUnVhdlNot: ne = unOp(ce, mod, loc, NUOp::eUnVhdlNot);  break;
  case CarbonExpr::eUnBitNeg: ne = unOp(ce, mod, loc, NUOp::eUnBitNeg);  break;
  case CarbonExpr::eUnChange: ne = unOp(ce, mod, loc, NUOp::eUnChange);  break;

  case CarbonExpr::eBiPlus:   ne = binOp(ce, mod, loc, NUOp::eBiPlus);   break;
  case CarbonExpr::eBiMinus:  ne = binOp(ce, mod, loc, NUOp::eBiMinus);  break;
  case CarbonExpr::eBiSMult:   ne = binOp(ce, mod, loc, NUOp::eBiSMult);   break;
  case CarbonExpr::eBiUMult:   ne = binOp(ce, mod, loc, NUOp::eBiUMult);   break;
  case CarbonExpr::eBiSDiv:    ne = binOp(ce, mod, loc, NUOp::eBiSDiv);    break;
  case CarbonExpr::eBiSMod:    ne = binOp(ce, mod, loc, NUOp::eBiSMod);    break;
  case CarbonExpr::eBiUDiv:    ne = binOp(ce, mod, loc, NUOp::eBiUDiv);    break;
  case CarbonExpr::eBiUMod:    ne = binOp(ce, mod, loc, NUOp::eBiUMod);    break;
  case CarbonExpr::eBiEq:     ne = binOp(ce, mod, loc, NUOp::eBiEq);     break;
  case CarbonExpr::eBiSLt:     ne = binOp(ce, mod, loc, NUOp::eBiSLt);     break;
  case CarbonExpr::eBiULt:     ne = binOp(ce, mod, loc, NUOp::eBiULt);     break;
  case CarbonExpr::eBiBitAnd: ne = binOp(ce, mod, loc, NUOp::eBiBitAnd); break;
  case CarbonExpr::eBiBitOr:  ne = binOp(ce, mod, loc, NUOp::eBiBitOr);  break;
  case CarbonExpr::eBiBitXor: ne = binOp(ce, mod, loc, NUOp::eBiBitXor); break;
  case CarbonExpr::eBiRshift: ne = binOp(ce, mod, loc, NUOp::eBiRshift); break;
  case CarbonExpr::eBiLshift: ne = binOp(ce, mod, loc, NUOp::eBiLshift); break;
  case CarbonExpr::eUnPartSel:
  case CarbonExpr::eBiBitSel:
  case CarbonExpr::eBiMemSel:
  case CarbonExpr::eBiDownTo:
  {
    const NUExpr* netExpr = exprToNucleus(ce->getArg(0), mod, loc);
    CarbonPartsel* partsel = ce->castPartsel();
    if (partsel != NULL)
      return mFold->createPartsel(netExpr, partsel->getRange(), loc);
    const NUExpr* sel = exprToNucleus(ce->getArg(1), mod, loc);
    if (sel == NULL)
      return NULL;
    if (ce->getType() == CarbonExpr::eBiBitSel)
      return mFold->createBitsel(netExpr, sel, loc);
    NUNet *net = netExpr->getWholeIdentifier();
    NU_ASSERT( net && net->getMemoryNet()->isResynthesized(), netExpr );
    NUMemselRvalue *msel = new NUMemselRvalue(netExpr, sel, loc);
    msel->putResynthesized( true );
    return msel;
  }

  case CarbonExpr::eUnSignExt: {
    NUExpr* a0 = exprToNucleus(ce->getArg(0), mod, loc)->copy(mCopyContext);
    a0->setSignedResult (true);
    NUExpr* a1 = NUConst::create(false, ce->getBitSize(), 32, loc);
    ne = new NUBinaryOp(NUOp::eBiVhExt, a0, a1, loc);
    ne->resize(ce->getBitSize());
    break;
  }

  case CarbonExpr::eTeCond:   ne = ternOp(ce, mod, loc, NUOp::eTeCond);  break;
    
  case CarbonExpr::eNaConcat: ne = nOp(ce, mod, loc, NUOp::eNaConcat);   break;

  case CarbonExpr::eEdge: {
    CarbonEdge * edgeCE = ce->castEdge();
    const NUExpr * expr = exprToNucleus(edgeCE->getArg(),mod,loc);
    ne = new NUEdgeExpr(edgeCE->getEdge(),expr,loc);
    break;
  }

  case CarbonExpr::eNaMultiDriver: return NULL;
  case CarbonExpr::eStart:
  case CarbonExpr::eInvalid:
    CE_ASSERT("Unexpected CarbonExpr type" == NULL, ce);
  } // switch
  
  return ne;
} // NUExpr* ExprResynth::e2n

void ExprResynth::mapNet(NUNetElab* alias, NUNetElab* master)
{
  mNetMap[alias] = master;
}

void ExprResynth::clearExprCache()
{
  mExprCache->clear();
  mFold->clearExprFactory();    // test/latches/latch45 fails without this
}
