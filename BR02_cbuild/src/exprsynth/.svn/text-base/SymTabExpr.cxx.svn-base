// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
 * \file
 * Implementation of CarbonIdent subclass using SymbolTable nodes as identifiers.
 */

#include <cstddef>
#include "exprsynth/SymTabExpr.h"
#include "symtab/STAliasedLeafNode.h"
#include "exprsynth/ExprFactory.h"
#include "util/UtOrder.h"       // For carbonPtrCompare()
#include "util/UtIndent.h"

SymTabIdent::SymTabIdent(STAliasedLeafNode * node, UInt32 bitSize) :
  CarbonIdent(eUnsigned, bitSize),
  mNode(node), mUsageMask(NULL)
{
}

SymTabIdent::SymTabIdent(STAliasedLeafNode * node, const DynBitVector& usageMask, UInt32 bitSize) :
  CarbonIdent(eUnsigned, bitSize),
  mNode(node)
{
  mUsageMask = new DynBitVector(usageMask);
}


SymTabIdent::~SymTabIdent()
{
  delete mUsageMask;
}


const char* SymTabIdent::typeStr() const
{
  return "SymTabIdent";
}


CarbonExpr::SignT SymTabIdent::evaluate(ExprEvalContext*) const
{
  return eBadSign;
}


CarbonExpr::AssignStat SymTabIdent::assign(ExprAssignContext * /*context --unused*/)
{
  return eReadOnly;
}


CarbonExpr::AssignStat SymTabIdent::assignRange(ExprAssignContext * /*context --unused*/, 
                                                const ConstantRange& /*range --unused*/)
{
  return eReadOnly;
}


ptrdiff_t SymTabIdent::compare(const CarbonExpr * other) const
{
  // pointer-compare; identical names will exist in multiple unelaborated symbol tables.
  const SymTabIdent* otherIdent = other->castSymTabIdent();  
  if (! otherIdent)
    return -1;
  
  ptrdiff_t ret = carbonPtrCompare(mNode, otherIdent->mNode);
  if (ret == 0)
  {
    if ((mUsageMask && ! otherIdent->mUsageMask) ||
        (! mUsageMask && otherIdent->mUsageMask))
      ret = carbonPtrCompare(mUsageMask, otherIdent->mUsageMask);
    else if (mUsageMask && otherIdent->mUsageMask)
    {
      if (*mUsageMask < *otherIdent->mUsageMask)
        ret = -1;
      else if (*mUsageMask > *otherIdent->mUsageMask)
        ret = 1;
      else 
        ret = 0;
    }
  }
  return ret;
}

size_t SymTabIdent::hash() const
{
  size_t dynHash = 0;
  if (mUsageMask)
    dynHash = mUsageMask->hash();
  return ((size_t)mNode >> 2) + dynHash * 17;
}


const STAliasedLeafNode * SymTabIdent::getNode(DynBitVector * usageMask) const
{
  if (mUsageMask == NULL)
  {
    // The usageMask must correspond to the node's bitsize, or bad
    // transforms can happen. We assume with a SymTabIdent that all bits
    // are used. Therefore, we just need to set the usageMask to the
    // bitSize and make it all ones.
    usageMask->resize(getBitSize());
    usageMask->set();
  }
  else 
    *usageMask = *mUsageMask;
  
  return mNode;
}


const STAliasedLeafNode * SymTabIdent::getNode() const
{
  return mNode;
}

void SymTabIdent::print(bool /*recurse --unused*/, int indent) const
{
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < indent; i++) {
    screen << " ";
  }
  screen << typeStr() << '(' << this << ") ";
  CarbonExpr::printSize(screen);
  screen << " : ";
  UtString name;
  mNode->compose(&name);
  
  screen << name << UtIO::endl;
}


void SymTabIdent::composeIdent(ComposeContext* context) const
{
  UtString* buf = context->getBuffer();
  switch(context->getMode())
  {
  case eComposeNormal:
  case eComposeC:
    mNode->verilogCompose(buf);
    break;
  case eComposeLeaf:
    mNode->verilogComposeLeaf(buf);
    break;
  }
}

const SymTabIdent* SymTabIdent::castSymTabIdent() const {
  return this;
}

const SymTabIdentBP* SymTabIdent::castSymTabIdentBP() const
{
  return NULL;
}

const ShellSymNodeIdent* SymTabIdent::castShellSymNodeIdent() const
{
  return NULL;
}

const ShellSymNodeIdentBP* SymTabIdent::castShellSymNodeIdentBP() const
{
  return NULL;
}

bool SymTabIdent::isWholeIdentifier() const 
{ 
  return true; 
}

SymTabIdentBP::SymTabIdentBP(STAliasedLeafNode* name, UInt32 bitSize,
                             CarbonExpr* orig) 
  : SymTabIdent(name, bitSize), mBackPointer(orig)
{}

SymTabIdentBP::SymTabIdentBP(STAliasedLeafNode* name, 
                             const DynBitVector& usageMask,
                             UInt32 bitSize,
                             CarbonExpr* orig) 
  : SymTabIdent(name, usageMask, bitSize), mBackPointer(orig)
{}

SymTabIdentBP::~SymTabIdentBP()
{}
   
ptrdiff_t SymTabIdentBP::compare(const CarbonExpr* other) const
{
  ptrdiff_t cmp = SymTabIdent::compare(other);
  if (cmp == 0)
  {
    const SymTabIdentBP* otherCast = (const SymTabIdentBP*) other;
    cmp = carbonPtrCompare(mBackPointer, otherCast->mBackPointer);
  }
  return cmp;
}

const char* SymTabIdentBP::typeStr() const
{
  return "SymTabIdentBP";
}
 
//! hash method
size_t SymTabIdentBP::hash() const
{
  size_t firstHash = SymTabIdent::hash();
  firstHash = (firstHash * 17) + ((size_t)(mBackPointer) >> 2);
  return firstHash;
}

void SymTabIdentBP::print(bool recurse, int indent) const
{
  SymTabIdent::print(recurse, indent);
  
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < indent; i++) {
    screen << " ";
  }
  
  screen << "BACKPOINTER:";
  mBackPointer->print(recurse, indent);
}

void SymTabIdentBP::composeIdent(ComposeContext* context) const
{
  mBackPointer->composeHelper(context);
}
  
const SymTabIdentBP* SymTabIdentBP::castSymTabIdentBP() const 
{ 
  return this; 
}

void SymTabIdentBP::nestedDBSetup(ESFactory* factory, ExprDBContext* dbContext, bool inclusiveNodes,
  STSymbolTable::LeafAssoc *leafAssoc)
{
  factory->setupDBContext(dbContext, mBackPointer, inclusiveNodes, leafAssoc);
}

CbuildShellDB::CarbonIdentType SymTabIdentBP::addNestedToDB(CarbonExprVector* exprVec) const
{
  exprVec->push_back(mBackPointer);
  return CbuildShellDB::eSymTabIdentBP;
}
