// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _TRANSLATEEXPR_H_
#define _TRANSLATEEXPR_H_

#include "nucleus/Nucleus.h"
#include "util/ConstantRange.h"
#include "exprsynth/Expr.h"

class CarbonExpr;
class ESFactory;
class NUMemselRvalue;

class ESTranslateExpr
{
public:
  //! constructor
  ESTranslateExpr(ESFactory* factory);

  //! destructor
  virtual ~ESTranslateExpr();

  //! Virtual function to translate a net
  virtual CarbonExpr* translateNet(NUNet* net, const ConstantRange& range,
				   UInt32 bitSize, bool isSigned) = 0;

  //! Function to translate a nucleus expression into a carbon expression
  CarbonExpr* recursiveTranslateExpr(NUExpr* expr);

private:
  // A set of functions to traverse the various nucleus expression
  // types and translate them
  CarbonExpr* translateVarsel(NUVarselRvalue*);
  CarbonExpr* translateConstXZ(NUConst*);
  CarbonConst::SignT getSign(NUConst* k);
  CarbonExpr* translateConst(NUConst*);
  CarbonExpr* translateIdent(NUIdentRvalue*);
  CarbonExpr* translateMemsel(NUMemselRvalue*);
  CarbonExpr* translateUnaryOp(NUUnaryOp*);
  CarbonExpr* translateBinaryOp(NUBinaryOp*);
  CarbonExpr* translateTernaryOp(NUTernaryOp*);
  CarbonExpr* translateConcat(NUConcatOp*);
  CarbonExpr* translateEdge(NUEdgeExpr*);
  CarbonExpr* translateReductionOp(CarbonExpr::ExprT, CarbonExpr*, UInt32);
  CarbonExpr* multiDriver(CarbonExprVector&);

  // Helper functions
  ConstantRange getNetRange(NUNet* net);

  // Local data
  ESFactory* mFactory;
}; // class ESTranslateExpr


#endif // _TRANSLATEEXPR_H_
