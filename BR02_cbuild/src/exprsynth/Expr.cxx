// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Implementation of CarbonExpr and derived classes.
*/

#include "exprsynth/Expr.h"
#include "util/UtConv.h"
#include <algorithm>
#include "iodb/CbuildShellDB.h"
#include "util/UtIndent.h"
#include "util/UtOrder.h"       // For carbonPtrCompare()

#define POLY_MULT	7

static bool sAssertOK = true;   // used to limit recursive asserts in CE_ASSERT

/*!
 * Local helper to return a character UtString for the given operator.
 */
static const char* helperGetOpChar(CarbonExpr::ExprT type)
{
  static const char* names[CarbonExpr::eInvalid] =
    {
      "<ident>",
      "<const>",
      "<constxz>",
      "<constreal>",
      "<edge>",
      "buf",
      "-",
      "~",
      "not",
      "Partsel",
      "SignExtend",
      "Change",
      "+",
      "-",
      "*","*",
      "/","/",
      "%","%",
      "==",
      "<","<",
      "&",
      "|",
      "^",
      ">>",
      "<<",
      "Bitsel",
      "Memsel",
      "downto",
      "Cond",
      "Concat",
      "MultiDriver",
      "*INVALID*"
    };
  INFO_ASSERT(names[type-1], "Added a new CarbonExpr type without a name");
  return names[type-1];
}

CarbonExpr::CarbonExpr(UInt32 bitSize, bool isSigned) :
  mBitSize(bitSize), mIsSigned(isSigned), mRefCount(0)
{}

CarbonExpr* CarbonExpr::getEnable(bool&) const { return NULL; }

bool CarbonExpr::drivesZ() const { return false; }

bool CarbonExpr::isNonZero() const { return false; }

bool CarbonExpr::isZero() const { return false; }


bool CarbonExpr::getDeclaredRange(ConstantRange*)
  const
{
  return false;
}

CarbonExpr::~CarbonExpr()
{
}

void CarbonExpr::printVerilog() const
{
  UtString buf;
  compose(&buf);
  UtIO::cout() << buf;
  UtIO::cout().flush();
}

void CarbonExpr::compose(UtString* buf) const
{
  ComposeContext context(buf, eComposeNormal);
  composeHelper(&context);
}

static void sCheckLineLength(UtIndent* bufObject)
{
  // Cap the size of a line at about 64k (2^16)
  if (bufObject->getCurLineLength() >= (1 << 16))
    bufObject->newline();
}

void CarbonExpr::composeC(UtIndent* bufObject) const
{
  sCheckLineLength(bufObject);
  ComposeContext context(bufObject, eComposeC);
  composeHelper(&context);
}

void CarbonExpr::composeLeaf(UtString* buf) const
{
  ComposeContext context(buf, eComposeLeaf);
  composeHelper(&context);
}

size_t CarbonExpr::hash() const
{
  return mBitSize * POLY_MULT;
}

UInt32 CarbonExpr::getBitSize() const
{
  CE_ASSERT(mBitSize != 0, this);
  return mBitSize;
}

bool CarbonExpr::operator==(const CarbonExpr& e2) const
{
  // Make sure they are the same type first
  int cmp = getType() - e2.getType();
  if (cmp == 0)
  {
    // Make sure the sign and size are the same
    cmp = ((int) mBitSize) - ((int) e2.mBitSize);
    if (cmp == 0)
    {
      cmp = (int) isSigned() - (int) e2.isSigned();
      if (cmp == 0)
	// At this point everything appears the same, ask the derived
	// class to compare itself
	cmp = compare(&e2);
    } // if
  } // if
  return cmp == 0;
} // bool CarbonExpr::operator==

void CarbonExpr::printSize(UtOStream& stream) const
{
  stream << "[size=" << getBitSize()
	 << (isSigned() ? ", signed" : "")
	 << "] ";
  stream << " ref=" << mRefCount << " ";
}

bool CarbonExpr::munchUnknown(DynBitVector* value, DynBitVector* drive)
{
  bool known = true;
  if (drive->any())
  {
    // make the value all z (x's are not expressed in carbon)
    value->reset();
    drive->set();
    known = false;
  }
  return known;
}

//! Static method to enable CE_ASSERT
void CarbonExpr::enableAsserts(){sAssertOK = true;}
  
//! Static method to disable CE_ASSERT (used to prevent recursive/nested asserts)
void CarbonExpr::disableAsserts(){sAssertOK = false;}

//! Test if NU_ASSERT is OK (used in CE_ASSERT macros)
bool CarbonExpr::assertOK() {return sAssertOK;}


static bool sAssertStarted = false;

void CarbonExpr::printAssertHeader(const char* file, int line, const char* exprStr)
{
  // Print the header information for an assert. This is broken up so
  // that information about multiple expressions can be printed.
  //
  // This assert prints all the info we need about an object in order
  // tor the user to find the problem. We'd like to give human
  // readable data if possible, which is different depending on the
  // type of object
  UtIO::cerr() << "\n\n******CARBON INTERNAL ERROR*******\n\n";
  UtIO::cerr() << file << ':' << line << " CE_ASSERT(" << exprStr
               << ") failed\n" << UtIO::flush;
  if ( sAssertStarted ) printAssertTrailer(); // bail out, we are in a recursive assert situation
  sAssertStarted = true;
}

void CarbonExpr::printAssertInfo(void) const
{
  if (this == NULL) {
    UtIO::cerr() << "(null object)\n";
  } else {
    printVerilog();
  }
}

void CarbonExpr::printAssertTrailer()
{
  // print stack trace ???
  UtIO::cerr() << UtIO::flush;
  abort();
}

CarbonIdent::CarbonIdent(bool isSigned, UInt32 bitSize)
  : CarbonExpr(bitSize, isSigned),
    mRange(NULL)
{
}

CarbonIdent::~CarbonIdent()
{
  if (mRange != NULL)
    delete mRange;
}

CarbonExpr::ExprT CarbonIdent::getType() const { return CarbonExpr::eIdent; }

void CarbonIdent::putDeclaredRange(const ConstantRange* r)
{
  if (mRange != NULL)
    delete mRange;
  mRange = new ConstantRange(*r);
}

void CarbonIdent::visit(CarbonExprWalker* exprWalker)
{
  if (! exprWalker->preVisitIdent(this))
    return;

  exprWalker->visitIdent(this);
}

bool CarbonIdent::getDeclaredRange(ConstantRange* range)
  const
{
  if (mRange == NULL)
    return false;
  *range = *mRange;
  return true;
}

void CarbonIdent::composeHelper(ComposeContext* context) const
{
  if (context->getMode() == eComposeC)
  {
    UtIndent* bufObject = context->getIndentObject();
    sCheckLineLength(bufObject);
  }
  
  composeIdent(context);
}

CarbonConst::CarbonConst(const DynBitVector& val, SignT signInfo, UInt32 bitSize) :
  CarbonExpr(bitSize, signInfo != eUnsigned), mValue(val), mSignInfo(signInfo)
{
}

CarbonConst::~CarbonConst()
{
}

CarbonExpr::ExprT CarbonConst::getType() const { return eConst; }

CarbonExpr::SignT CarbonConst::getSignInfo() const { return mSignInfo; }

const char* CarbonConst::typeStr() const { return "CarbonConst"; }

bool CarbonConst::isZero (void) const { return mValue == 0; }

bool CarbonConst::isNonZero () const { return mValue != 0; }

bool CarbonConst::getULL(UInt64* val) const {
    *val = mValue.llvalue();
    return true;
  }

bool CarbonConst::getLL(SInt64* val) const {
  UInt64 tmp = mValue.llvalue();
  SInt64 tmpCast = static_cast<SInt64>(tmp);
  if (mSignInfo == eNeg)
    tmpCast *= -1;
  *val = tmpCast;
  return true;
}

bool CarbonConst::getUL(UInt32* val) const
{
  *val = mValue.value();
  return true;
}

bool CarbonConst::getL(SInt32* val) const
{
  UInt32 uVal = mValue.value();
  SInt32 sVal = static_cast<SInt32>(uVal);
  if (mSignInfo == eNeg)
    sVal *= -1;
  *val = sVal;
  return true;
}

void CarbonConst::getValue(DynBitVector* value,  DynBitVector* drive) const
{
  getValue(value);
  drive->resize(getBitSize());
  drive->reset();
}

void CarbonConst::getValue(DynBitVector* value) const {
  *value = mValue;
  value->resize(getBitSize()); 
}

size_t CarbonConst::hash() const
{
  return (((CarbonExpr::hash() * POLY_MULT + mSignInfo) * POLY_MULT) +
	  mValue.hash());
}

ptrdiff_t CarbonConst::compare(const CarbonExpr* expr) const
{
  const CarbonConst* other = expr->castConst();
  int cmp = mSignInfo - other->mSignInfo;
  if (cmp == 0)
    cmp = mValue.compare(other->mValue);
  return cmp;
}

void CarbonConst::visit(CarbonExprWalker* exprWalker)
{
  exprWalker->visitConst(this);
}

void CarbonConst::print(bool /* recurse -- unused */, int indent) const
{
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < indent; i++) {
    screen << " ";
  }
  screen << typeStr() << '(' << this << ") ";
  CarbonExpr::printSize(screen);
  screen << " : ";

  switch (mSignInfo) {
  case eUnsigned: break;
  case ePos: screen << "+"; break;
  case eNeg: screen << "-"; break;
  default: CE_ASSERT("Unknown constant sign" == NULL, this); break;
  }

  printVal();
  screen << UtIO::endl;
}

void CarbonConst::printVal() const
{
  UtString buf;
  composeVal(&buf);
  UtIO::cout() << buf;
}

void CarbonConst::composeVal(UtString* buf)
  const
{
  *buf << "val="; 
  composeValueByType(buf, mValue);
}

static void sCalcValue(UtString* buf, const DynBitVector& value, CarbonRadix radix)
{
  UtString tmp;
  value.format(&tmp, radix);
  const char* str = tmp.c_str();
  while ((*str != '\0') && (*(str+1) != '\0') && (*str == '0'))
    ++str;
  *buf << str;
}

void CarbonConst::composeHelper(ComposeContext* context) const
{
  UtString* buf = context->getBuffer();
  
  switch (context->getMode())
  {
  case eComposeNormal:
  case eComposeLeaf:
    {
      switch (mSignInfo) {
      case eUnsigned: break;
      case ePos: *buf << "+"; break;
      case eNeg: *buf << "-"; break;
      default: CE_ASSERT("Unknown constant sign" == NULL, this); break;
      }
      if (castConstXZ() != NULL)
        composeVal(buf);
      else
        sCalcValue(buf, mValue, eCarbonHex);
    }
    break;
  case eComposeC:
    {      
      switch (mSignInfo) {
      case eUnsigned: break;
      case ePos: break;
      case eNeg: *buf << "-"; break;
      default: CE_ASSERT("Unknown constant sign" == NULL, this); break;
      }
      
      if (castConstXZ() != NULL)
        CE_ASSERT(0,  this);
      else
        sCalcValue(buf, mValue, eCarbonDec);
    }
    break;
  }
}

CarbonExpr::SignT 
CarbonConst::evaluate(ExprEvalContext* evalContext) const
{
  DynBitVector* value = evalContext->getValue();
  DynBitVector* drive = evalContext->getDrive();
  getValue(value);
  drive->resize(value->size());
  drive->reset();
  return mSignInfo;
}

CarbonExpr::AssignStat
CarbonConst::assign(ExprAssignContext* context)
{
  context->rshift(getBitSize());
  return eReadOnly;
}




CarbonConstReal::CarbonConstReal(const double val, SignT sign) :
  CarbonExpr(64, sign != eUnsigned), mValue(val), mSignInfo(sign)
{
}

CarbonConstReal::~CarbonConstReal()
{
}

CarbonExpr::ExprT CarbonConstReal::getType() const { return eConstReal; }

CarbonExpr::SignT CarbonConstReal::getSignInfo() const { return mSignInfo; }

const char* CarbonConstReal::typeStr() const { return "CarbonConstReal"; }

bool CarbonConstReal::getULL(UInt64* val) const {
  *val = (UInt64)mValue;
  return true;
}

bool CarbonConstReal::getLL(SInt64* val) const {
  *val = (SInt64)mValue;
  return true;
}

bool CarbonConstReal::getUL(UInt32* val) const
{
  *val = (UInt32)mValue;
  return true;
}

bool CarbonConstReal::getL(SInt32* val) const
{
  *val = (SInt32)mValue;
  return true;
}

void CarbonConstReal::getValue(DynBitVector*,  DynBitVector*) const
{
  CE_ASSERT("Unsupported getValue() function on CarbonReal" == NULL, this);
}

void CarbonConstReal::getValue(DynBitVector*) const
{
  CE_ASSERT("Unsupported getValue() function on CarbonReal" == NULL, this);
}

bool
CarbonConstReal::isZero() const { return mValue == 0.0; }

bool
CarbonConstReal::isNonZero() const { return mValue != 0.0; }

size_t CarbonConstReal::hash() const
{
  return (((CarbonExpr::hash() * POLY_MULT + mSignInfo) * POLY_MULT) +
	  (UInt64)mValue);
}

ptrdiff_t CarbonConstReal::compare(const CarbonExpr* expr) const
{
  const CarbonConstReal* other = expr->castConstReal();
  return (int)(mValue - other->mValue);
}

void CarbonConstReal::visit(CarbonExprWalker* exprWalker)
{
  exprWalker->visitConstReal(this);
}

void CarbonConstReal::print(bool /* recurse -- unused */, int indent) const
{
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < indent; i++) {
    screen << " ";
  }
  screen << typeStr() << '(' << this << ") ";
  CarbonExpr::printSize(screen);
  screen << " : ";

  switch (mSignInfo) {
  case eUnsigned: break;
  case ePos: screen << "+"; break;
  case eNeg: screen << "-"; break;
  default: CE_ASSERT("Unknown constant sign" == NULL, this); break;
  }
  printVal();
  screen << UtIO::endl;
}

void CarbonConstReal::printVal() const
{
  UtString buf;
  composeVal(&buf);
  UtIO::cout() << buf;
}

void CarbonConstReal::composeVal(UtString* buf)
  const
{
  *buf << "val=" << mValue; 
}

void CarbonConstReal::composeHelper(ComposeContext* context) const
{
  UtString* buf = context->getBuffer();
  *buf << mValue;
}

CarbonExpr::SignT 
CarbonConstReal::evaluate(ExprEvalContext* /*evalContext*/ ) const
{
  double value;
//  value = evalContext->getRealValue();
  // TJM How do I handle the real value from here into ExprEvalContext?
  CE_ASSERT("Unsupported evaluate() function on CarbonReal" == NULL, this);
  getValue(&value);
  return mSignInfo;
}

CarbonExpr::AssignStat
CarbonConstReal::assign(ExprAssignContext* context)
{
  context->rshift(64);
  return eReadOnly;
}


CarbonConstXZ::CarbonConstXZ(const DynBitVector& val, const DynBitVector& drive,
              SignT sign, UInt32 bitSize) :
  CarbonConst(val, sign, bitSize), mDrive(drive)
{}

CarbonConstXZ::~CarbonConstXZ() {}

CarbonExpr::ExprT CarbonConstXZ::getType() const { return eConstXZ; }

void CarbonConstXZ::getValue(DynBitVector* value,  DynBitVector* drive) const {
  *value = mValue;
  *drive = mDrive;
  UInt32 size = getBitSize();
  value->resize(size);
  drive->resize(size);
}

void CarbonConstXZ::getValue(DynBitVector* value) const {
  *value = mValue & ~(mDrive);
  value->resize(getBitSize());
}

bool CarbonConstXZ::drivesZ() const
{
  // Mask must be all 1's (for the bit size), and value must be all 0's
  // (for the bit size).
  UInt32 size = getBitSize();
  DynBitVector mask(size);
  mask.set ();
  DynBitVector valmask(size,mValue);
  valmask.flip ();
  return ((mask == mDrive) and (mask == valmask));
}

bool CarbonConstXZ::getULL(UInt64* val) const {
  DynBitVector cp(mValue);
  cp &= ~(mDrive);            // mask so cp contains 1's for known bits, 0 for unknown bits
  *val = cp.llvalue();
  return true;
}

bool CarbonConstXZ::getLL(SInt64* val) const {
  DynBitVector cp(mValue);
  cp &= ~(mDrive);            // mask so cp only contains the known bits (no X or Z)
  UInt64 tmp = cp.llvalue();
  SInt64 tmpCast = static_cast<SInt64>(tmp);
  if (mSignInfo == eNeg)
    tmpCast = -tmpCast;
  *val = tmpCast;
  return true;
}

bool CarbonConstXZ::getUL(UInt32* val) const
{
  DynBitVector cp(mValue);
  cp &= ~(mDrive);
  *val = cp.value();
  return true;
}

const char* CarbonConstXZ::typeStr() const { return "CarbonConstXZ"; }



size_t CarbonConstXZ::hash() const
{
  return (CarbonConst::hash() * POLY_MULT) + mDrive.hash();
}

ptrdiff_t CarbonConstXZ::compare(const CarbonExpr* expr) const
{
  const CarbonConstXZ* other = expr->castConstXZ();
  int cmp = CarbonConst::compare(other);
  if (cmp == 0)
    cmp = mDrive.compare(other->mDrive);
  return cmp;
}

void CarbonConstXZ::visit(CarbonExprWalker* exprWalker)
{
  exprWalker->visitConstXZ(this);
}

CarbonExpr::SignT 
CarbonConstXZ::evaluate(ExprEvalContext* evalContext) const
{
  getValue(evalContext->getValue(), 
           evalContext->getDrive());
  return mSignInfo;
}

CarbonExpr::AssignStat
CarbonConstXZ::assign(ExprAssignContext* context)
{
  context->rshift(getBitSize());
  return eReadOnly;
}

//! Print the value and the drive
void CarbonConstXZ::composeVal(UtString* buf) const {
  *buf << "val="; 
  composeValueByType(buf, mValue);
  *buf << ", drive=";
  composeValueByType(buf, mDrive);
}

CarbonOp::CarbonOp(CarbonExpr::ExprT op, UInt32 bitSize, bool isSigned) :
  CarbonExpr(bitSize, isSigned), mOp(op)
{}


CarbonOp::~CarbonOp()
{
}

CarbonExpr::ExprT CarbonOp::getType() const { return mOp; }

const char* CarbonOp::attrStr(UtString*) const { return ""; }

size_t CarbonOp::hash() const
{
  return (CarbonExpr::hash() * POLY_MULT) + (size_t)mOp;
}

void CarbonOp::print(bool recurse, int indent) const
{
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < indent; i++) {
    screen << " ";
  }
  screen << typeStr() << "(" << this << ") ";
  printSize(screen);
  UtString attr;
  screen << " : " << helperGetOpChar(mOp) << attrStr(&attr) << UtIO::endl;
  if (recurse) {
    for (UInt32 i = 0; i < getNumArgs(); i++) {
      getArg(i)->print(recurse, indent+2);
    }
  }
}


CarbonUnaryOp::CarbonUnaryOp(CarbonExpr::ExprT op, CarbonExpr *expr,
			     UInt32 bitSize, bool isSigned) :
  CarbonOp(op, bitSize, isSigned), mExpr(expr)
{
  CE_ASSERT((op >= eUnStart) && (op <= eUnEnd), this);
}


CarbonUnaryOp::~CarbonUnaryOp()
{
}

size_t CarbonUnaryOp::hash() const 
{
  return (CarbonOp::hash() * POLY_MULT) + (size_t)mExpr;
}

ptrdiff_t CarbonUnaryOp::compare(const CarbonExpr* expr) const
{
  const CarbonUnaryOp* other = expr->castUnary();
  ptrdiff_t cmp = carbonPtrCompare(mExpr, other->mExpr);
  return cmp;
}


CarbonExpr* CarbonUnaryOp::getArg(UInt32 index) const
{
  switch (index) {
  case 0:
    return mExpr;
    break;

  default:
    CE_ASSERT("Invalid index into CarbonUnaryOp greater than 0" == NULL, this);
    break;
  }
  return 0;
}

void CarbonUnaryOp::visit(CarbonExprWalker* exprWalker)
{
  // Pre-visit the unary op
  if (!exprWalker->preVisitUnaryOp(this))
    return;

  // Visit the sub expression first
  exprWalker->visitExpr(getArg(0));

  // Visit this expression
  exprWalker->visitUnaryOp(this);
}

const char* CarbonUnaryOp::typeStr() const
{
  return "UnaryOp";
}

CarbonExpr::SignT
CarbonUnaryOp::evaluate(ExprEvalContext* evalContext) const
{
  SignT sign = mExpr->evaluate(evalContext);
  DynBitVector* value = evalContext->getValue();
  DynBitVector* drive = evalContext->getDrive();
  UInt32 mySize = getBitSize();
  if (value->size() < mySize)
  {
    value->resize(mySize);
    drive->resize(mySize);
  }
  SignT ret = sign;
  bool known = munchUnknown(value, drive);
  if ((sign != eBadSign) && known)
  {
    switch(mOp)
    {
    case eUnBuf:
      // do nothing
      break; 
    case eUnMinus:
      if (sign == eNeg)
        ret = ePos;
      else if (sign == ePos)
        ret = eNeg;
      else // unsigned
        value->negate();
      break;
    case eUnBitNeg:
    case eUnVhdlNot:
      // catch 0 not positive mistakes
      if ((*value == 0) && (sign == eNeg))
        sign = ePos;
      if (sign == eNeg)
      {
        *value -= 1;
        ret = ePos;
      }
      else if (sign == eUnsigned)
        value->flip();
      else 
      {
        *value += 1;
        ret = eNeg;
      }
      break;
    case eUnPartSel:
      // partsel has its own eval method
      CE_ASSERT(mOp != eUnPartSel, this);
      break;

    case eUnSignExt:
    {
      // sign extend from MSB of operand into result size
      UInt32 signBit = mExpr->getArg (0)->getBitSize ();
      value->setRange (signBit, mySize - signBit , value->test (signBit-1));
    }
      break;

    default:
      CE_ASSERT(isUnaryOp(mOp), this);
      CE_ASSERT("Unknown CarbonUnaryOp type" == NULL, this); // fail-safe
      break;
    }
  }
  return ret;
}

CarbonExpr::AssignStat
CarbonUnaryOp::assign(ExprAssignContext* context)
{
  CarbonExpr* arg = getArg(0);
  AssignStat stat = eFull;
  ExprAssignContext* tmpContext = NULL;
  UInt32 size = getBitSize();

  switch(mOp)
  {
  case eUnSignExt:
  case eUnBuf:
    stat = arg->assign(context);
    break;
  case eUnMinus:
    tmpContext = context->copy();
    tmpContext->resize(size);
    tmpContext->negate();
    stat = arg->assign(tmpContext);
    context->rshift(size);
    break;
  case eUnBitNeg:
  case eUnVhdlNot:
    tmpContext = context->copy();
    tmpContext->resize(size);
    tmpContext->flip();
    stat = arg->assign(tmpContext);
    context->rshift(size);
    break;
  case eUnPartSel:
    // handled by CarbonPartSel
    CE_ASSERT(mOp != eUnPartSel, this);
    break;
  default:
    CE_ASSERT(isUnaryOp(mOp), this);
    break;
  }

  if (tmpContext)
    delete tmpContext;

  return stat;
}

void
CarbonUnaryOp::composePre(UtString* buf) const
{
  switch (getType())
  {
    case eUnBuf:
      break;

    case eUnMinus:
      *buf << "-";
      break;

    case eUnBitNeg:
    case eUnVhdlNot:
      *buf << "~";
      break;

    case eUnPartSel:
      break;

    case eUnSignExt:
      *buf << "$signed(";
      break;

    case eUnChange:
      *buf << "change(";
      break;

    default:
      CE_ASSERT("Unknown CarbonUnaryOp type" == NULL, this);
      break;
  }
}

void CarbonUnaryOp::composePost(UtString* buf) const
{
  switch(getType()) {
    case eUnPartSel:
    {
      const CarbonPartsel* partsel = castPartsel();
      ConstantRange range = partsel->getRange();

      // Ranges only have to be denormalized for whole identifiers
      CarbonIdent* ident = mExpr->castIdent();
      if ((ident != NULL) && ident->isWholeIdentifier()) {
        ConstantRange declaredRange;
        if (ident->getDeclaredRange(&declaredRange))
          range.denormalize(&declaredRange);
      }

      range.format(buf);
      break;
    }

    case eUnChange:
    case eUnSignExt:
      *buf << ")";
      break;

    default:
      // Missing types are caught above
      break;
  }
}

void
CarbonUnaryOp::composeHelper(ComposeContext* context) const
{
  UtString* buf = context->getBuffer();
  composePre(buf);

  mExpr->composeHelper(context);

  composePost(buf);
} // CarbonUnaryOp::composeHelper

CarbonPartsel::CarbonPartsel(CarbonExpr* expr, const ConstantRange& range,
			     UInt32 bitSize, bool isSigned) : 
  CarbonUnaryOp(eUnPartSel, expr, bitSize, isSigned)
{
  mRange = new ConstantRange(range);
}

CarbonPartsel::~CarbonPartsel()
{
  delete mRange;
}

size_t CarbonPartsel::hash() const 
{
  return (((CarbonUnaryOp::hash() * POLY_MULT) + mRange->getMsb()) * POLY_MULT
	  + mRange->getLsb());
}

ptrdiff_t CarbonPartsel::compare(const CarbonExpr* expr) const
{
  const CarbonPartsel* other = expr->castPartsel();
  int cmp = mRange->getMsb() - other->mRange->getMsb();
  if (cmp == 0)
  {
    cmp = mRange->getLsb() - other->mRange->getLsb();
    if (cmp == 0)
      cmp = CarbonUnaryOp::compare(other);
  }
  return cmp;
}

void CarbonPartsel::visit(CarbonExprWalker* exprWalker)
{
  // Pre-visit the part select
  if (!exprWalker->preVisitPartsel(this))
    return;

  // Visit the sub expression first
  exprWalker->visitExpr(getArg(0));

  // Visit this expression
  exprWalker->visitPartsel(this);
}

CarbonExpr::SignT CarbonPartsel::evaluate(ExprEvalContext* evalContext) const
{

  ExprEvalContext* tmpContext = evalContext->copy();
  SignT sign = mExpr->evaluate(tmpContext);
  // don't munch the unknown here

  DynBitVector* exprVal = tmpContext->getValue();
  DynBitVector* exprDrv = tmpContext->getDrive();
  DynBitVector* value = evalContext->getValue();
  DynBitVector* drive = evalContext->getDrive();
  
  if (sign != eBadSign)
  {

    value->resize(getBitSize());
    drive->resize(value->size());
    value->reset();
    drive->reset();
    
    if (sign == eNeg)
      exprVal->negate();
    
    UInt32 largest = mRange->leftmost();
    UInt32 smallest = mRange->rightmost();
    for (UInt32 i = smallest; i <= largest; ++i)
    {
      if (exprDrv->test(i))
        drive->set(i - smallest);
      else if (exprVal->test(i))
        value->set(i - smallest);
    }
  }

  delete tmpContext;
  return eUnsigned;
}

CarbonExpr::AssignStat
CarbonPartsel::assign(ExprAssignContext* context)
{
  AssignStat stat = eFull;
  CarbonIdent* arg = getArg(0)->castIdent();
  if (arg)
  {
    ConstantRange actualRange;
    CE_ASSERT(arg->getDeclaredRange(&actualRange), this);
    ConstantRange partRange = getRange();
    bool doFlip = false;
    if (partRange.isFlipped(actualRange))
    {
      partRange.switchSBs();
      doFlip = true;
    }
    partRange.denormalize(&actualRange);

    UInt32 size = getBitSize();
    CE_ASSERT(size == partRange.getLength(), this);
    
    CE_ASSERT(actualRange.contains(partRange), this);
    
    if (doFlip)
    {
      ConstantRange assignmentRange(partRange);
      assignmentRange.switchSBs();
      
      ExprAssignContext* tmpContext = context->copy();
      tmpContext->resize(size);
      tmpContext->flipDirection();
      stat = arg->assignRange(tmpContext, assignmentRange);
      delete tmpContext;
      context->rshift(size);    
    }
    else
      stat = arg->assignRange(context, partRange);
  }
  else
  {
    stat = eReadOnly;
    context->rshift(getBitSize());
  }

  return stat;
}

const char* CarbonPartsel::typeStr() const
{
  return "Partsel";
}

const char* CarbonPartsel::attrStr(UtString* str) const
{
  ConstantRange r = *mRange;
  ConstantRange declaredRange;
  if (mExpr->getDeclaredRange(&declaredRange))
    r.denormalize (&declaredRange);
  r.format(str);
  return str->c_str();
}

CarbonBinaryOp::CarbonBinaryOp(CarbonExpr::ExprT op, CarbonExpr *expr1,
			       CarbonExpr *expr2, UInt32 bitSize,
                               bool isSigned, bool computeSigned) :
  CarbonOp(op, bitSize, isSigned),
  mExpr1(expr1), mExpr2(expr2)
{
  if (!computeSigned)
    return;
  mIsSigned = false;
  CE_ASSERT((op >= eBiStart) && (op <= eBiEnd), this);
  switch(mOp)
  {
  case eBiEq:
  case eBiSLt:
  case eBiULt:
  case eBiBitSel:
  case eBiMemSel:
    break;
  case eBiPlus:
  case eBiSMult:
  case eBiSDiv:
  case eBiSMod:
  case eBiUMult:
  case eBiUDiv:
  case eBiUMod:
  case eBiBitAnd:
  case eBiBitOr:
  case eBiBitXor:
  case eBiRshift:
  case eBiLshift:
  case eBiMinus:
  case eBiDownTo:
    if (expr1->isSigned() && expr2->isSigned())
      mIsSigned = true;
    break;
  case eIdent:
  case eConst:
  case eConstXZ:
  case eConstReal:
  case eUnBuf:
  case eUnMinus:
  case eUnBitNeg:
  case eUnVhdlNot:
  case eUnPartSel:
  case eUnChange:
  case eUnSignExt:
  case eTeCond:
  case eNaConcat:
  case eNaMultiDriver:
  case eStart:
  case eInvalid:
  case eEdge:
    CE_ASSERT(isBinaryOp(mOp), this);
    CE_ASSERT("Unknown CarbonBinaryOp type" == NULL, this);
    break;
  } // switch
} // CarbonBinaryOp::CarbonBinaryOp


CarbonBinaryOp::~CarbonBinaryOp()
{
}

size_t CarbonBinaryOp::hash() const 
{
  return ((((CarbonOp::hash() * POLY_MULT) + (size_t)mExpr1) * POLY_MULT) +
	  (size_t)mExpr2);
}

ptrdiff_t CarbonBinaryOp::compare(const CarbonExpr* expr) const
{
  const CarbonBinaryOp* other = expr->castBinary();
  ptrdiff_t cmp = carbonPtrCompare(mExpr1, other->mExpr1);
  if (cmp == 0)
    cmp = carbonPtrCompare(mExpr2, other->mExpr2);
  return cmp;
}


CarbonExpr* CarbonBinaryOp::getArg(UInt32 index) const
{
  switch (index) {
  case 0:
    return mExpr1;
    break;

  case 1:
    return mExpr2;
    break;

  default:
    CE_ASSERT("Invalid index into CarbonBinaryOp greater than 1" == NULL, this);
    break;
  }
  return 0;
}


void CarbonBinaryOp::visit(CarbonExprWalker* exprWalker)
{
  // Pre-visit the binary op
  if (!exprWalker->preVisitBinaryOp(this))
    return;

  // Visit the sub expressions first
  exprWalker->visitExpr(getArg(0));
  exprWalker->visitExpr(getArg(1));

  // Visit this expression
  exprWalker->visitBinaryOp(this);
}

const char* CarbonBinaryOp::typeStr() const
{
  return "BinaryOp";
}

static CarbonExpr::SignT sCalcMultSign(CarbonExpr::SignT sign1, CarbonExpr::SignT sign2,
                                       bool isZero, bool* override)
{
  // All multiplication is done with absolute values. So, we need to
  // figure out the sign. If one is unsigned and one is negative, the
  // resulting value needs to be negated so that the absolute value is
  // correct, hence the override.
  
  CarbonExpr::SignT ret;
  if ((sign1 == CarbonExpr::eUnsigned) || (sign2 == CarbonExpr::eUnsigned))
    *override = true;
  if ((sign1 == CarbonExpr::eNeg) && (sign2 == CarbonExpr::eNeg))
    ret = CarbonExpr::ePos;
  else if ((sign1 == CarbonExpr::eNeg) || (sign2 == CarbonExpr::eNeg))
    ret = CarbonExpr::eNeg;
  else
    ret = CarbonExpr::ePos;

  if (isZero && (ret != CarbonExpr::eUnsigned))
    ret = CarbonExpr::ePos;
  return ret;
}

static void sBitOpValuate(DynBitVector& lhs, CarbonExpr::SignT sign1, 
                          DynBitVector& rhs, CarbonExpr::SignT sign2, 
                          UInt32 bitSize, bool lhsOnly = false)
{
  // Drives get resized at the end of evaluation, so don't worry about
  // it here. We already know that there are no z's at this point.
  if (lhs.size() < bitSize)
    lhs.resize(bitSize);
  if (!lhsOnly && (rhs.size() < bitSize))
    rhs.resize(bitSize);
  
  if (sign1 == CarbonExpr::eNeg)
    lhs.negate();
  if (sign2 == CarbonExpr::eNeg) 
    rhs.negate();
}

static CarbonExpr::SignT sBitOpResolve(CarbonExpr::SignT sign1, CarbonExpr::SignT sign2, DynBitVector* value)
{
  CarbonExpr::SignT ret = CarbonExpr::ePos;
  if ((sign1 == CarbonExpr::eUnsigned) ||
      (sign2 == CarbonExpr::eUnsigned))
    ret = CarbonExpr::eUnsigned;
  else if (value->test(value->size() - 1))
  {
    ret = CarbonExpr::eNeg;
    value->negate();
  }
  return ret;
}

static CarbonExpr::SignT sShiftResolve(CarbonExpr::SignT sign1, DynBitVector* value)
{
  CarbonExpr::SignT ret = CarbonExpr::ePos;
  if (sign1 == CarbonExpr::eUnsigned)
    ret = CarbonExpr::eUnsigned;
  else if (value->test(value->size() - 1))
  {
    ret = CarbonExpr::eNeg;
    value->negate();
  }
  return ret;
}

CarbonExpr::SignT 
CarbonBinaryOp::evaluate(ExprEvalContext* evalContext) const
{
  ExprEvalContext* lhsContext = evalContext->copy();
  ExprEvalContext* rhsContext = evalContext->copy();
  DynBitVector& lhs = lhsContext->getValueRef();
  DynBitVector& rhs = rhsContext->getValueRef();
  DynBitVector& lhD = lhsContext->getDriveRef();
  DynBitVector& rhD = rhsContext->getDriveRef();
  SignT sign1 = getArg(0)->evaluate(lhsContext);
  SignT sign2 = getArg(1)->evaluate(rhsContext);
  if ((sign1 == eBadSign) || (sign2 == eBadSign))
    return eBadSign;
  
  DynBitVector* value = evalContext->getValue();
  DynBitVector* drive = evalContext->getDrive();
  SignT ret = eBadSign;

  // inquire about z's
  bool known1 = munchUnknown(&lhs, &lhD);
  bool known2 = munchUnknown(&rhs, &rhD);
  value->resize(getBitSize());
  drive->resize(value->size());
  if (!known1 || !known2)
  {
    drive->set();
    // Do this for centralization of unknown handling
    munchUnknown(value,drive);
    switch (mOp)
    {
    case eBiEq:
    case eBiSLt:
    case eBiULt:
      ret = eUnsigned;
      value->reset();
      drive->reset();
      break;
    case eBiRshift:
    case eBiLshift:
      if (sign1 != eUnsigned)
        ret = ePos;
      else
        ret = eUnsigned;
      break;
    default:
      if ((sign1 == eUnsigned) || (sign2 == eUnsigned))
        ret = eUnsigned;
      else 
        ret = ePos;
      break;
    }
  }
  else
  {
    drive->reset();
    value->reset();

    bool overrideToUnsigned = false;
    ExprT op = mOp;
    // transform minus into plus
    if (mOp == eBiMinus)
    {
      op = eBiPlus;
      switch (sign2)
      {
      case eUnsigned:
        rhs.negate();
        break;
      case ePos:
        sign2 = eNeg;
        break;
      case eNeg:
        sign2 = ePos;
        break;
      case eBadSign:
        CE_ASSERT("Unknown constant sign" == NULL, this);
        break;
      }
    }
    
    /*
      NC does not follow the verilog standard for signed right shifts
      int fillVal = 0;
      size_t num;
    */
    size_t size;


    switch(op)
    {
    case eBiPlus:
      if (sign1 == eNeg)
        lhs.negate();
      if (sign2 == eNeg)
        rhs.negate();

      value->copy(lhs + rhs);

      if ((sign1 == eUnsigned) || (sign2 == eUnsigned))
        ret = eUnsigned;
      else
      {
        bool isNeg = value->test(value->size() - 1);
        if (isNeg)
        {
          ret = eNeg;
          value->negate();
        }
        else
          ret = ePos;
      }
    case eBiMinus:
      // minus gets transformed to plus
      CE_ASSERT(op != eBiMinus, this);
      break;
    case eBiSMult:
    case eBiUMult:
      lhs.integerMult(rhs);
      value->copy(lhs);
      ret = sCalcMultSign(sign1, sign2, value->none(), &overrideToUnsigned);
      break;
    case eBiSDiv:
    case eBiUDiv:
      lhs.integerDiv(rhs, &lhD);
      value->copy(lhs);
      ret = sCalcMultSign(sign1, sign2, value->none(), &overrideToUnsigned);
      break;
    case eBiSMod:
    case eBiUMod:
      lhs.integerDiv(rhs, value);
      // result takes the sign of the first operand
      ret = sign1;
      if (value->none() && (ret != eUnsigned))
        ret = ePos;
      break;
    case eBiEq:
      if (lhs == rhs)
      {
        if ((sign1 == sign2) ||
            ((sign1 != eNeg) && (sign2 != eNeg)))
          value->set(0);
      }
      ret = eUnsigned;
      break;
    case eBiSLt:
    case eBiULt:
      ret = eUnsigned;
      if ((sign1 != eNeg) &&
          (sign2 != eNeg))
      {
        if (lhs < rhs)
          value->set(0);
      }
      else if ((sign1 == eNeg) && 
               (sign2 == eNeg))
      {
        if (lhs.isGreaterThan(rhs))
          value->set(1);
      }
      else if (sign1 == eNeg)
        value->set(0);
      break;
    case eBiBitAnd:
      sBitOpValuate(lhs, sign1, rhs, sign2, getBitSize());
      value->copy(lhs & rhs);
      ret = sBitOpResolve(sign1, sign2, value);
      break;
    case eBiBitOr:
      sBitOpValuate(lhs, sign1, rhs, sign2, getBitSize());
      value->copy(lhs | rhs);
      ret = sBitOpResolve(sign1, sign2, value);
      break;
    case eBiBitXor:
      sBitOpValuate(lhs, sign1, rhs, sign2, getBitSize());
      value->copy(lhs ^ rhs);
      ret = sBitOpResolve(sign1, sign2, value);
      break;
    case eBiRshift:
      sBitOpValuate(lhs, sign1, rhs, sign2, getBitSize(), true);
      size = lhs.size();
      /*
        if ((sign1 != eUnsigned) && lhs.test(size - 1))
        fillVal = 1;
      */
      if (rhs >= size)
      {
        value->reset();
        /*
          NC does not follow the verilog standard for signed right
          shifts. Always 0 fills.
          
          if (fillVal != 0)
          value->set();
          else
          value->reset();
        */
      }
      else
      {
        value->copy(lhs >> rhs.value());
        /*
          if (fillVal != 0)
          {
          num = size - rhs.value();
          value->setRange(size - 1, num, fillVal);
          }
        */
      }
      ret = sShiftResolve(sign1, value);
      break;
    case eBiLshift:
      sBitOpValuate(lhs, sign1, rhs, sign2, getBitSize(), true);
      if (rhs >= lhs.size())
        value->reset();
      else
        value->copy(lhs << rhs.value());
      ret = sShiftResolve(sign1, value);
      break;
    case eBiBitSel:
      ret = eUnsigned;
      if (rhs >= lhs.size())
      {
        // out of bounds
        drive->set();
        munchUnknown(value, drive);
      }
      else
      {
        if (lhs.test(rhs.value()))
          value->set(0);
      }
      break;
    case eBiMemSel:
      // This only works on CarbonIdents that implement evaluateRange().
      {
        CarbonIdent *ident = getArg(0)->castIdent();
        CarbonConst *constant = getArg(1)->castConst();
        CE_ASSERT(ident && constant, this);
        SInt32 val;
        constant->getL(&val);
        // This select is normalized to the LSB.  However, memory
        // expressions are normalized to the lowest bit of the range.
        ConstantRange declaredRange;
        CE_ASSERT(ident->getDeclaredRange(&declaredRange), this);
        val = declaredRange.index(val);
        ConstantRange range(val, val);
        ret = ident->evaluateRange(evalContext, range);
      }
      break;
    default:
      CE_ASSERT(isBinaryOp(mOp), this);
      CE_ASSERT("Unknown CarbonBinaryOp type" == NULL, this);
      break;
    }

    if (overrideToUnsigned)
    {
      if (ret == eNeg)
        value->negate();
      ret = eUnsigned;
    }
  }

  // Resize the value in case it was extended.
  value->resize(getBitSize());
  drive->resize(value->size());

  delete lhsContext;
  delete rhsContext;
  return ret;
}

CarbonExpr::AssignStat
CarbonBinaryOp::assign(ExprAssignContext* context)
{
  AssignStat stat = eFull;
  if ((mOp == eBiBitSel) ||
      (mOp == eBiMemSel))
  {
    CarbonExpr* bitSel = getArg(1);
    CarbonConst* constSel = bitSel->castConst();
    CarbonIdent* ident = getArg(0)->castIdent();
    if (constSel && ident && ! bitSel->castConstXZ())
    {
      SInt32 bitVal;
      constSel->getL(&bitVal);
      
      CE_ASSERT(bitVal >= 0, this);
      
      ConstantRange declaredRange;
      if (ident->getDeclaredRange(&declaredRange))
        bitVal = declaredRange.index(bitVal);
      ConstantRange bit(bitVal, bitVal);
      stat = ident->assignRange(context, bit);
    }
    else 
    {
      stat = eReadOnly;
      context->rshift(getBitSize());
    }
  }
  else 
  {
    stat = eReadOnly;
    context->rshift(getBitSize());
  }

  return stat;
}
                               
void
CarbonBinaryOp::composeBitSel(ComposeContext* context) const
{
  CE_ASSERT(getType() == eBiBitSel, this);
  CarbonExpr* identExpr = getArg(0);
  CarbonExpr* index = getArg(1);
  
  identExpr->composeHelper(context);

  UtString* buf = context->getBuffer();

  *buf << "[";
  
  // Ranges only have to be denormalized for whole identifiers
  CarbonIdent* ident = identExpr->castIdent();
  if ((ident != NULL) && ident->isWholeIdentifier()) {
    ConstantRange declaredRange;
    bool hasDeclaredRange = ident->getDeclaredRange(&declaredRange);
    CE_ASSERT(hasDeclaredRange, this);
    CarbonConst* k = index->castConst ();
    if (k != NULL)
    {
      DynBitVector val;
      k->getValue(&val);
      CE_ASSERT(val.size() <= 32, this);
      UInt32 offset = *val.getUIntArray();
      SInt32 idx = declaredRange.index(offset);
      *buf << idx;
    }
    else
    {
      // Have to write out the corrective arithmetic
      if (declaredRange.getMsb() >= declaredRange.getLsb())
      {
        index->composeHelper(context);
        *buf << "-" << declaredRange.getLsb();
      }
      else
      {
        *buf << declaredRange.getLsb() << "-";
        index->composeHelper(context);
      }
    }
  }
  else
    index->composeHelper(context);
  *buf << "]";
}

void CarbonBinaryOp::composePre(UtString* buf) const
{
  *buf << "(";
}

void CarbonBinaryOp::composePost(UtString* buf, const char* postStr) const
{
  *buf << postStr;
}

const char* CarbonBinaryOp::composeOp(UtString* buf) const
{
  const char* postStr = ")";
  switch (getType())
  {
    case eBiPlus:
      *buf << " + ";
      break;

    case eBiMinus:
      *buf << " - ";
      break;

    case eBiSMult:
    case eBiUMult:
      *buf << " * ";
      break;

    case eBiSDiv:
    case eBiUDiv:
      *buf << " / ";
      break;

    case eBiSMod:
    case eBiUMod:
      *buf << " % ";
      break;

    case eBiEq:
      *buf << " == ";
      break;

    case eBiSLt:
    case eBiULt:
      *buf << " < ";
      break;
      
    case eBiBitAnd:
      *buf << " & ";
      break;

    case eBiBitOr:
      *buf << " | ";
      break;

    case eBiBitXor:
      *buf << " ^ ";
      break;

    case eBiRshift:
      *buf << " >> ";
      break;

    case eBiLshift:
      *buf << " << ";
      break;

    case eBiBitSel:
    case eBiMemSel:
      *buf << ")[";
      postStr="]";
      break;
      
    default:
      CE_ASSERT("Unknown CarbonBinaryOp type" == NULL, this);
      break;
  }
  return postStr;
}

void
CarbonBinaryOp::composeHelper(ComposeContext* context) const
{
  if (getType() == eBiBitSel)
  {
    composeBitSel(context);
    return;
  } // if
  

  UtString* buf = context->getBuffer();
  composePre(buf);

  getArg(0)->composeHelper(context);
  const char* postStr = composeOp(buf);

  getArg(1)->composeHelper(context);
  composePost(buf, postStr);
} // CarbonBinaryOp::composeHelper

CarbonTernaryOp::CarbonTernaryOp(CarbonExpr::ExprT op, CarbonExpr *expr1,
				 CarbonExpr *expr2, CarbonExpr *expr3,
				 UInt32 bitSize, bool isSigned, bool computeSigned) :
  CarbonOp(op, bitSize, isSigned), mExpr1(expr1), mExpr2(expr2), mExpr3(expr3)
{
  CE_ASSERT((op >= eTeStart) && (op <= eTeEnd), this);
  if (computeSigned) {
    mIsSigned = getArg(1)->isSigned() && getArg(2)->isSigned();
  }
}


CarbonTernaryOp::~CarbonTernaryOp()
{
}


size_t CarbonTernaryOp::hash() const 
{
  return (((((CarbonOp::hash() * POLY_MULT) + (size_t)mExpr1) * POLY_MULT) +
	   (size_t)mExpr2) * POLY_MULT) + (size_t)mExpr3;
}

ptrdiff_t CarbonTernaryOp::compare(const CarbonExpr* expr) const
{
  const CarbonTernaryOp* other = expr->castTernary();
  ptrdiff_t cmp = carbonPtrCompare(mExpr1, other->mExpr1);
  if (cmp == 0)
  {
    cmp = carbonPtrCompare(mExpr2, other->mExpr2);
    if (cmp == 0)
      cmp = carbonPtrCompare(mExpr3, other->mExpr3);
  }
  return cmp;
}

CarbonExpr* CarbonTernaryOp::getArg(UInt32 index) const
{
  switch (index) {
  case 0:
    return mExpr1;
    break;

  case 1:
    return mExpr2;
    break;

  case 2:
    return mExpr3;
    break;

  default:
    CE_ASSERT("Invalid index into CarbonTernaryOp greater than 2" == NULL, this);
    break;
  }
  return 0;
}

void CarbonTernaryOp::visit(CarbonExprWalker* exprWalker)
{
  // Pre-visit the ternary op
  if (!exprWalker->preVisitTernaryOp(this))
    return;

  // Visit the sub expressions first
  exprWalker->visitExpr(getArg(0));
  exprWalker->visitExpr(getArg(1));
  exprWalker->visitExpr(getArg(2));

  // Visit this expression
  exprWalker->visitTernaryOp(this);
}

CarbonExpr* CarbonTernaryOp::getEnable(bool& cond) const
{
  // Only conditional expresions can have an enable. Check both the
  // then and else legs of the conditional. If they drive a Z, then
  // the other leg is enabled. If so, return the conditional enable
  // and whether it is the then leg.
  CarbonExpr* enable = NULL;

  // cond must be initialized
  cond = false;

  if (getType() == eTeCond)
  {
    enable = getArg(0);
    if (getArg(1)->drivesZ())
      cond = false;
    else if (getArg(2)->drivesZ())
      cond = true;
    else
      enable = NULL;
  }
  return enable;
}

const char* CarbonTernaryOp::typeStr() const
{
  return "TernaryOp";
}

CarbonExpr::SignT 
CarbonTernaryOp::evaluate(ExprEvalContext* evalContext) const
{
  getArg(0)->evaluate(evalContext);

  DynBitVector* value = evalContext->getValue();
  DynBitVector* drive = evalContext->getDrive();
  CE_ASSERT(drive->none(), this); // conditionals return 1 or 0 only
  const CarbonExpr* exprTrue = getArg(1);
  const CarbonExpr* exprFalse = getArg(2);
  
  SignT ret = eBadSign;
  if (value->any())
    ret = exprTrue->evaluate(evalContext);
  else
    ret = exprFalse->evaluate(evalContext);

  UInt32 mySize = getBitSize();
  value->resize(mySize);
  drive->resize(mySize);
  if (!exprTrue->isSigned() || !exprFalse->isSigned()) {
    if (ret == eNeg)          // JDM: this seems really dubious!
      value->negate();
    ret = eUnsigned;
  }
  
  return ret;
}

CarbonExpr::AssignStat
CarbonTernaryOp::assign(ExprAssignContext* context)
{
  context->rshift(getBitSize());
  return eReadOnly;
}

void CarbonTernaryOp::composeHelper(ComposeContext* context) const
{
  CE_ASSERT(getType() == eTeCond, this);
  UtString* buf = context->getBuffer();
  getArg(0)->composeHelper(context);
  *buf << " ? ";
  getArg(1)->composeHelper(context);
  *buf << " : ";
  getArg(2)->composeHelper(context);
}

void CarbonConst::composeValueByType(UtString* buf, const DynBitVector& val)
  const
{
  val.format(buf, eCarbonHex);
}


CarbonNaryOp::CarbonNaryOp(CarbonExpr::ExprT op, const CarbonExprVector* exprs,
			   UInt32 bitSize, bool isSigned) :
  CarbonOp(op, bitSize, isSigned)
{
  mExprs = new CarbonExprVector(*exprs);
}

CarbonNaryOp::~CarbonNaryOp()
{
  delete mExprs;
}

size_t CarbonNaryOp::hash() const
{
  size_t hashval = 0;
  for (CarbonExprVectorIter i = mExprs->begin(); i != mExprs->end(); ++i)
  {
    CarbonExpr* expr = *i;
    hashval = (hashval * POLY_MULT) + (size_t)expr;
  }
  return hashval;
}

ptrdiff_t CarbonNaryOp::compare(const CarbonExpr* expr) const
{
  // If they are not the same size, then they must be different
  const CarbonNaryOp* other = expr->castNary();
  int size1 = getNumArgs();
  int size2 = other->getNumArgs();
  ptrdiff_t cmp = size1 - size2;
  if (cmp == 0)
  {
    // Compare the two expression vectors
    CarbonExprVectorIter i1 = mExprs->begin();
    CarbonExprVectorIter e1 = mExprs->end();
    CarbonExprVectorIter i2 = other->mExprs->begin();
    for (; i1 != e1 && (cmp == 0); ++i1, ++i2)
      cmp = carbonPtrCompare(*i1, *i2);
  }

  return cmp;
} // int CarbonNaryOp::compare

UInt32 CarbonNaryOp::getNumArgs() const
{
  return mExprs->size();
}


CarbonExpr* CarbonNaryOp::getArg(UInt32 index) const
{
  CE_ASSERT(index < mExprs->size(), this);
  return (*mExprs)[index];
}

void CarbonNaryOp::visit(CarbonExprWalker* exprWalker)
{
  // Pre-visit the nary op
  if (!exprWalker->preVisitNaryOp(this))
    return;

  // Visit the sub expressions first
  for (UInt32 i = 0; i < getNumArgs(); ++i)
    exprWalker->visitExpr(getArg(i));

  // Visit this expression
  exprWalker->visitNaryOp(this);
}

CarbonExpr::AssignStat
CarbonNaryOp::assign(ExprAssignContext* context)
{
  context->rshift(getBitSize());
  return eReadOnly;
}

CarbonConcatOp::CarbonConcatOp(const CarbonExprVector *exprs,
			       UInt32 repeat_count, UInt32 bitSize,
                               bool isSigned) :
  CarbonNaryOp(eNaConcat, exprs, bitSize, isSigned),
  mRepeatCount(repeat_count)
{
  CE_ASSERT(mRepeatCount >= 1, this);
}


CarbonConcatOp::~CarbonConcatOp()
{}

size_t CarbonConcatOp::hash() const
{
  return (CarbonNaryOp::hash() * POLY_MULT) + mRepeatCount;
}

ptrdiff_t CarbonConcatOp::compare(const CarbonExpr* expr) const
{
  const CarbonConcatOp* other = expr->castConcat();
  ptrdiff_t cmp = mRepeatCount - other->mRepeatCount;
  if (cmp == 0)
    cmp = CarbonNaryOp::compare(other);
  return cmp;
}

void CarbonConcatOp::visit(CarbonExprWalker* exprWalker)
{
  // Pre-visit the concat op
  if (!exprWalker->preVisitConcatOp(this))
    return;

  // Visit the sub expressions first
  for (UInt32 i = 0; i < getNumArgs(); ++i)
    exprWalker->visitExpr(getArg(i));

  // Visit this expression
  exprWalker->visitConcatOp(this);
}

const char* CarbonConcatOp::typeStr() const
{
  return "CarbonConcatOp";
}

const char* CarbonConcatOp::attrStr(UtString* str) const
{
  *str << " [RepeatCount=" << mRepeatCount << "]";
  return str->c_str();
}

CarbonExpr::SignT 
CarbonConcatOp::evaluate(ExprEvalContext* evalContext) const
{
  DynBitVector* value = evalContext->getValue();
  DynBitVector* drive = evalContext->getDrive();
  
  SignT ret = eUnsigned;
  value->resize(getBitSize());
  drive->resize(value->size());
  value->reset();
  drive->reset();

  UInt32 atomicSize = 0;
  ExprEvalContext* subContext = evalContext->copy();;
  
  for (CarbonExprVectorCIter i = mExprs->begin(); 
       i != mExprs->end(); ++i)
  {
    const CarbonExpr* expr = *i;
    DynBitVector& subv = subContext->getValueRef();
    DynBitVector& subd = subContext->getDriveRef();
    subv.resize(expr->getBitSize());
    subd.resize(expr->getBitSize());
    
    SignT exprSign = expr->evaluate(subContext);
    if (exprSign == CarbonExpr::eBadSign)
      ret = eBadSign;
    else
    {
      UInt32 detBitSize = expr->getBitSize(); 
      atomicSize += detBitSize;
      *value <<= detBitSize;
      *drive <<= detBitSize;
      if (exprSign == eNeg)
        subv.negate();
      *value += subv;
      *drive += subd;
    }
  }
  delete subContext;

  if (mRepeatCount > 1)
  {
    DynBitVector tmpval = *value;
    DynBitVector tmpdrv = *drive;
    
    for (UInt32 j = 1; j < mRepeatCount; ++j)
    {
      *value <<= atomicSize;
      *value += tmpval;
      *drive <<= atomicSize;
      *drive += tmpdrv;
    }
  }
  
  return ret;
}

CarbonExpr::AssignStat
CarbonConcatOp::assign(ExprAssignContext* context)
{
  AssignStat stat = eFull;
  UInt32 numReps = getRepeatCount();
  if (numReps == 1)
  {
    SInt32 numArgs = getNumArgs();
    // assign from right to left
    for (SInt32 n = numArgs - 1; n >= 0; --n)
    {
      CarbonExpr* arg = getArg(n);
      AssignStat localStat = arg->assign(context);
      switch(localStat)
      {
      case eFull:
        if (stat != eFull)
          stat = ePartial;
        break;
      case ePartial:
        stat = ePartial;
        break;
      case eReadOnly:
        /*
          If we have assigned to some parts of the concat and the stat
          is still full, the stat needs to be downgraded to
          partial. But, if this is the first concat argument then the
          current stat must be change to readonly. This ensures that
          the stat can never be more than ePartial.
        */
        if ((stat == eFull) && (n == numArgs - 1))
          stat = eReadOnly;
        else if (stat != eReadOnly)
          stat = ePartial;
        break;
      }
    }
  }
  else
  {
    context->rshift(getBitSize());
    stat = eReadOnly;
  }
  return stat;
}

void CarbonConcatOp::composeHelper(ComposeContext* context) const
{
  UtString* buf = context->getBuffer();
  switch (context->getMode())
  {
  case eComposeNormal:
  case eComposeLeaf:
  {
    if (getRepeatCount() != 1) {
      *buf << getRepeatCount();
    }
    *buf << "{";
    if ( 0 == getNumArgs() ){
      *buf << "<!!CONCAT_WITH_NO_ELEMENTS!!!>";
    } else {
      for (UInt32 i = 0; i < getNumArgs(); ++i)
      {
        if ( i ) {
          *buf << ", ";
        }
        if ( getArg(i) == (void*)0xdeadbeef ) {
          *buf << "<!!RECENTLY_RELEASED_MEMORY!!>";
        } else {
          getArg(i)->composeHelper(context);
        }
      }
    }
    *buf << "}";
    break;
  }
  case eComposeC: {
    CE_ASSERT(0, this);
    break;
  }
  }
}

CarbonEdge::CarbonEdge(ClockEdge clockEdge, 
                       CarbonExpr *expr,
                       UInt32 bitSize) :
  CarbonExpr(bitSize, false),   // edges are unsigned
  mExpr(expr),
  mClockEdge(clockEdge)
{
}


CarbonEdge::~CarbonEdge()
{
}

CarbonExpr::ExprT CarbonEdge::getType() const { return CarbonExpr::eEdge; }

size_t CarbonEdge::hash() const 
{
  return (((CarbonExpr::hash() * POLY_MULT) + (size_t)mExpr) * POLY_MULT) + (size_t)mClockEdge;
}

CarbonExpr* CarbonEdge::getArg(UInt32 index) const {
  return CarbonExpr::getArg(index);
}

ptrdiff_t CarbonEdge::compare(const CarbonExpr* expr) const
{
  const CarbonEdge* other = expr->castEdge();
  ptrdiff_t cmp = carbonPtrCompare(mExpr, other->mExpr);
  if (cmp == 0) {
    cmp = ((SIntPtr) mClockEdge) - ((SIntPtr) other->mClockEdge);
  }
  return cmp;
}


CarbonExpr* CarbonEdge::getArg() const
{
  return mExpr;
}

ClockEdge CarbonEdge::getEdge() const
{
  return mClockEdge;
}

void CarbonEdge::visit(CarbonExprWalker* exprWalker)
{
  // Pre-visit the unary op
  if (!exprWalker->preVisitEdge(this))
    return;

  // Visit the sub expression first
  exprWalker->visitExpr(getArg());

  // Visit this expression
  exprWalker->visitEdge(this);
}

const char* CarbonEdge::typeStr() const
{
  return "Edge";
}

CarbonExpr::SignT
CarbonEdge::evaluate(ExprEvalContext*) const
{
  // TBD: Edge evaluation.
  CE_ASSERT("Unsupported evaluate() function on CarbonEdge" == NULL, this);
  return eUnsigned;
}

CarbonExpr::AssignStat
CarbonEdge::assign(ExprAssignContext* context)
{
  CarbonExpr* arg = getArg();

  AssignStat stat = arg->assign(context);
  return stat;
}

void
CarbonEdge::composeHelper(ComposeContext* context) const
{
  UtString* buf = context->getBuffer();
  switch (context->getMode())
  {
  case eComposeNormal:
  case eComposeLeaf:
    switch(getEdge()) {
    case eClockPosedge:
      *buf << "posedge";
      break;
    case eClockNegedge:
      *buf << "negedge";
      break;
    case eLevelHigh:
    case eLevelLow:
      break;
    }
    
    mExpr->composeHelper(context);
    break;
  case eComposeC:
    CE_ASSERT(0, this);
    break;
  }
}

void CarbonEdge::print(bool recurse, int indent) const
{
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < indent; i++) {
    screen << " ";
  }
  screen << typeStr() << "(" << this << ") ";
  printSize(screen);
  screen << " : " << helperGetOpChar(getType());
  switch(getEdge()) {
  case eClockPosedge:
    screen << "posedge";
    break;
  case eClockNegedge:
    screen << "negedge";
    break;
  case eLevelHigh:
  case eLevelLow:
    break;
  }
  screen << UtIO::endl;
  if (recurse) {
    getArg()->print(recurse, indent+2);
  }
}


const STAliasedLeafNode* CarbonExpr::getNode(DynBitVector*) const
{
  CE_ASSERT("Unsupported getNode() function", this);
  return NULL;
}

FLNode* CarbonExpr::getFlow()
  const
{
  CE_ASSERT("Unsupported getFlow() function", this);
  return NULL;
}

FLNodeElab* CarbonExpr::getFlowElab()
  const
{
  CE_ASSERT("Unsupported getFlowElab() function", this);
  return NULL;
}

CarbonExpr* CarbonExpr::getArg(UInt32) const
{
  CE_ASSERT("Unsupported getArg() function", this);
  return NULL;
}

FLNode* CarbonIdent::getFlow() const
{
  CE_ASSERT("Unsupported getFlow() function", this);
  return NULL;
}

int CarbonIdent::numFlows() const
{
  CE_ASSERT("Unsupported numFlows() function", this);
  return 0;
}

CarbonExpr::SignT CarbonIdent::evaluateRange(ExprEvalContext*, const ConstantRange&)
{
  CE_ASSERT(0, this);
  return eBadSign;
}

void CarbonIdent::nestedDBSetup(ESFactory*, ExprDBContext*, 
                                bool, STSymbolTable::LeafAssoc *)
{
}

CbuildShellDB::CarbonIdentType 
CarbonIdent::addNestedToDB(CarbonExprVector*) const
{
  return CbuildShellDB::eSymTabIdent;
}

// Return number of arguments to this operator.
UInt32 CarbonExpr::getNumArgs() const
{
  return 0;
}

// return value and drive
void CarbonExpr::getValue(DynBitVector*, DynBitVector*) const
{
  CE_ASSERT("Unsupported getGetValue() function", this);
}

// Return just the value
void CarbonExpr::getValue(DynBitVector*) const
{
  CE_ASSERT("Unsupported getGetValue() function", this);
}

ExprAssignContext::ExprAssignContext() :
  mBitIndex(0)
{}

ExprAssignContext::~ExprAssignContext()
{}
  
void ExprAssignContext::putAssigns(const UInt32* val,
                                   const UInt32* drv,
                                   size_t numWords,
                                   size_t bitWidth)
{
  mValue.resize(bitWidth);
  mDrive.resize(bitWidth);

  UInt32* valArr = mValue.getUIntArray();
  UInt32* drvArr = mDrive.getUIntArray();
  CarbonValRW::cpSrcToDest(valArr, val, numWords);
  CarbonValRW::cpSrcToDest(drvArr, drv, numWords);
}

void ExprAssignContext::putAssigns(const DynBitVector& val,
                                   const DynBitVector& drv)
{
  mValue = val;
  mDrive = drv;
}

DynBitVector& ExprAssignContext::getValue()
{
  return mValue;
}

DynBitVector& ExprAssignContext::getDrive()
{
  return mDrive;
}

UInt32* ExprAssignContext::getValueArr()
{
  return mValue.getUIntArray();
}

UInt32* ExprAssignContext::getDriveArr()
{
  return mDrive.getUIntArray();
}

void ExprAssignContext::rshift(size_t shift)
{
  mValue >>= shift;
  mDrive >>= shift;
  mBitIndex += shift;
}

void ExprAssignContext::resize(size_t bitSize)
{
  mValue.resize(bitSize);
  mDrive.resize(bitSize);
}

void ExprAssignContext::negate()
{
  mValue.negate();
  cleanValue();
}

void ExprAssignContext::flip()
{
  mValue.flip();
  cleanValue();
}

void ExprAssignContext::cleanValue()
{
  mDrive.flip();
  mValue &= mDrive;
  mDrive.flip();
}

void ExprAssignContext::flipDirection()
{
  size_t bitWidth = mValue.size();

  DynBitVector tmpVal = mValue;
  DynBitVector tmpDrv = mDrive;

  for (size_t i = 0; i < bitWidth; ++i)
  {
    mValue[(bitWidth - 1) - i] = tmpVal[i];
    mDrive[(bitWidth - 1) - i] = tmpDrv[i];
  }
}

ExprAssignContext* ExprAssignContext::copy() const
{
  ExprAssignContext* copyMe = new ExprAssignContext;
  internalCopy(copyMe);
  return copyMe;
}

void ExprAssignContext::internalCopy(ExprAssignContext* copy) const
{
  copy->mValue = mValue;
  copy->mDrive = mDrive;
  copy->mBitIndex = mBitIndex;
}

UInt32 ExprAssignContext::curBitIndex() const
{
  return mBitIndex;
}

ExprEvalContext::ExprEvalContext()
{}

ExprEvalContext::~ExprEvalContext()
{}
  
DynBitVector* ExprEvalContext::getValue()
{
  return &mValue;
}

DynBitVector& ExprEvalContext::getValueRef()
{
  return mValue;
}


DynBitVector* ExprEvalContext::getDrive()
{
  return &mDrive;
}

DynBitVector& ExprEvalContext::getDriveRef()
{
  return mDrive;
}

ExprEvalContext* ExprEvalContext::copy() const
{
  ExprEvalContext* copyMe = new ExprEvalContext;
  internalCopy(copyMe);
  return copyMe;
}

void ExprEvalContext::internalCopy(ExprEvalContext* copy) const
{
  copy->getValueRef() = mValue;
  copy->getDriveRef() = mDrive;
}


void CarbonExpr::incRefCnt() {
  ++mRefCount;
}

UInt32 CarbonExpr::decRefCnt() {
  return --mRefCount;
}

const CarbonIdent* CarbonExpr::castIdent() const { return NULL; }
const CarbonConst* CarbonExpr::castConst() const { return NULL; }
const CarbonConstXZ* CarbonExpr::castConstXZ() const { return NULL; }
const CarbonConstReal* CarbonExpr::castConstReal() const { return NULL; }
const CarbonUnaryOp* CarbonExpr::castUnary() const { return NULL; }
const CarbonBinaryOp* CarbonExpr::castBinary() const { return NULL; }
const CarbonTernaryOp* CarbonExpr::castTernary() const { return NULL; }
const CarbonNaryOp* CarbonExpr::castNary() const { return NULL; }
const CarbonConcatOp* CarbonExpr::castConcat() const { return NULL; }
const CarbonPartsel* CarbonExpr::castPartsel() const { return NULL; }
const CarbonEdge* CarbonExpr::castEdge() const { return NULL; }
const SymTabIdent* CarbonExpr::castSymTabIdent() const { return NULL; }
const ESFlowIdent* CarbonExpr::castESFlowIdent() const { return NULL; }
const ESFlowElabIdent* CarbonExpr::castESFlowElabIdent() const { return NULL; }
const CarbonNetIdent* CarbonExpr::castCarbonNetIdent() const { return NULL; }
const CarbonMemIdent* CarbonExpr::castCarbonMemIdent() const { return NULL; }
const CarbonMemWordIdent* CarbonExpr::castCarbonMemWordIdent() const { return NULL; }


  //! CarbonExpr::castIdent()
const CarbonIdent* CarbonIdent::castIdent() const {
  return this;
}
const CarbonConst* CarbonConst::castConst() const {
  return this;
}
const CarbonConstXZ* CarbonConstXZ::castConstXZ() const {
  return this;
}
const CarbonConstReal* CarbonConstReal::castConstReal() const {
  return this;
}
const CarbonUnaryOp* CarbonUnaryOp::castUnary() const {
  return this;
}
const CarbonPartsel* CarbonPartsel::castPartsel() const {
  return this;
}
const CarbonBinaryOp* CarbonBinaryOp::castBinary() const {
  return this;
}
const CarbonTernaryOp* CarbonTernaryOp::castTernary() const {
  return this;
}
CarbonNaryOp* CarbonNaryOp::castNary() {
  return this;
}
const CarbonNaryOp* CarbonNaryOp::castNary() const {
  return this;
}
const CarbonConcatOp* CarbonConcatOp::castConcat() const {
  return this;
}
const CarbonEdge* CarbonEdge::castEdge() const {
  return this;
}
