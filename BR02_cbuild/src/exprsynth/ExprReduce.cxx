// -*-C++-*-
/******************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "exprsynth/ExprReduce.h"
#include "exprsynth/SymTabExpr.h"
#include "exprsynth/ExprFactory.h"

class ExprReduce::IdentCycleDetect: public CarbonExprWalker
{
public:
  CARBONMEM_OVERRIDES

  IdentCycleDetect(CarbonIdent* srcIdent) : 
    mSrcIdent(srcIdent)
  {}
  
  virtual ~IdentCycleDetect() {}
  
  virtual void visitIdent(CarbonIdent* ident) {
    CE_ASSERT(ident != mSrcIdent, ident);
  }
  
private:
  CarbonIdent* mSrcIdent;

  IdentCycleDetect();
  IdentCycleDetect(const IdentCycleDetect&);
  IdentCycleDetect& operator=(const IdentCycleDetect&);
};

ExprReduce::ExprReduce(ESFactory* exprFactory, 
                       CarbonIdent *ident)
  : mExprFactory(exprFactory), mTopIdent(ident)
{
  INFO_ASSERT( exprFactory != NULL,
               "Cannot create ExprReduce class with NULL expression factory");
}

ExprReduce::~ExprReduce()
{}

CarbonExpr* ExprReduce::replaceIdent(CarbonIdent* ident)
{
  return ident;
}

CarbonExpr* ExprReduce::reduce(CarbonExpr* reduceMe)
{
  visitExpr(reduceMe);
  return getResult();
}

CarbonExpr*
ExprReduce::transformPartsel(CarbonExpr* expr, CarbonPartsel* partselOrig) 
{
  // if expr is a partsel, bitsel, concat, const, or ident, it can be
  // reduced.
  
  const ConstantRange& range = partselOrig->getRange();
  if (range.getLength() == expr->getBitSize())
  {
    // not a partsel, but a whole identifier recurse through expr.
    // Note that this handles the only interesting ident case as well;
    // ie., if expr is an ident and the range covers the whole ident.
    ExprReduce recurse(mExprFactory, mTopIdent);
    return recurse.reduce(expr);
  }
  else if (CarbonConst* constExpr = expr->castConst())
  {
    // partsel of a constant. Just get the bits and return
    DynBitVector val;
    if (isKnownConstant(constExpr, &val))
    {
      // no oob. range must be normalized
      ConstantRange valBounds(val.size() - 1, 0);
      CE_ASSERT(valBounds.contains(range), partselOrig);
      
      // Shift the range over to the beginning of the word
      val >>= range.getLsb();
      
      // Resize the value
      val.resize(range.getLength());
      return mExprFactory->createConst(val, CarbonConst::eUnsigned, range.getLength());
    }
  }
  else if (CarbonPartsel* partsel = expr->castPartsel())
  {
    // A partsel of a partsel.
    // First, make sure these are normalized ranges
    const ConstantRange& nestedRange = partsel->getRange();
    CE_ASSERT(nestedRange.getMsb() >= nestedRange.getLsb(), partselOrig);
    CE_ASSERT(range.getMsb() >= range.getLsb(), partselOrig);

    // adjust the range to the nestedrange.
    ConstantRange adjustedRange(range);
    adjustedRange.adjust(nestedRange.getLsb());
    CE_ASSERT(nestedRange.contains(adjustedRange), partselOrig);
    // Create the simplified partsel and recurse
    CarbonExpr* simplified = 
      mExprFactory->createPartsel(expr->getArg(0), 
                                  adjustedRange,
                                  adjustedRange.getLength(),
                                  expr->isSigned());

    ExprReduce recurse(mExprFactory, mTopIdent);
    return recurse.reduce(simplified);
  }
  else if (CarbonBinaryOp* binOp = expr->castBinary())
  {
    if (binOp->getType() == CarbonExpr::eBiBitSel)
    {
      // A partsel of a bitsel better be 0:0
      ConstantRange nil_nil(0,0);
      CE_ASSERT(range == nil_nil, partselOrig);
      
      // The partsel is meaningless, recurse the bitsel.
      ExprReduce recurse(mExprFactory, mTopIdent);
      return recurse.reduce(binOp);
    }
  }
  else if (CarbonConcatOp* concat = expr->castConcat())
  {
    // Technically, this is tricky because with repeat counts the
    // partsel could be more than the core bit size. If that is the
    // case, we need to just fall through
    UInt32 rangeLength = range.getLength();
    UInt32 coreBitSize = 0;
    if (concat->getRepeatCount() == 1)
      coreBitSize = concat->getBitSize();
    else
    {
      for (SInt32 i = concat->getNumArgs() - 1;
           (rangeLength > 0) && (i >= 0); --i)
      {
        CarbonExpr* arg = concat->getArg(i);
        UInt32 bitSize = arg->getBitSize();
        coreBitSize += bitSize;
      }
    }
 
    // Don't do any processing if the range length + the lsb is
    // greater than the core size. We could have rangelength < the
    // core size but it may cross a repeat boundary.
    UInt32 lsb = range.getLsb();
    if (rangeLength + lsb <= coreBitSize)
    {
      CarbonExpr* beginExpr = NULL;
      CarbonExprVector concatExprs;
      
      for (UInt32 rpt = 0; (rangeLength > 0) && (rpt < concat->getRepeatCount()); ++rpt)
      {
        
        for (SInt32 i = concat->getNumArgs() - 1;
             (rangeLength > 0) && (i >= 0); --i)
        {
          CarbonExpr* arg = concat->getArg(i);
          UInt32 bitSize = arg->getBitSize();

          // keep running through the concat until we find an argument
          // that contains the lsb of the partsel range.
          if (beginExpr == NULL)
          {
            if (bitSize > lsb)
            {
              CarbonExpr* beginExprTmp = NULL;
              // is it an entire identifier?
              if ((lsb == 0) && (rangeLength == bitSize))
                // just return the concat arg
                return arg;
              else if (bitSize - lsb == 1)
              {
                // The beginning of the reduced concat is a single
                // bit.
                if (bitSize == 1)
                  // Current arg is a scalar or 1-bit vec
                  beginExprTmp = arg;
                else
                {
                  // We need to bitsel the current arg.

                  CarbonConst* bitIndex = mExprFactory->createConst(lsb, 32, false);
                  beginExprTmp = 
                    mExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, 
                                                 arg, bitIndex, 1, 
                                                 partselOrig->isSigned(), 
                                                 false);
                }
              }
              else
              {
                // the beginning of the concat is a partsel
                ConstantRange beginRange(bitSize - 1, lsb);
                beginExprTmp = mExprFactory->createPartsel(arg, beginRange, beginRange.getLength(), 
                                                        partselOrig->isSigned());
              }
              
              CE_ASSERT(beginExprTmp, partselOrig);
              // Further simplify the bitsel/partsel we just created
              ExprReduce recurse(mExprFactory, mTopIdent);
              beginExpr = recurse.reduce(beginExprTmp);
              
              rangeLength -= (bitSize - lsb);
              concatExprs.push_back(beginExpr);
            }
            else
              lsb -= bitSize;
          }
          else
          {
            if (bitSize >= rangeLength)
            {
              // end of the road. final expression here
              CarbonExpr* finalExprTmp = NULL;
              if (bitSize == rangeLength)
                // Full expression length
                finalExprTmp = arg;
              else if (rangeLength == 1)
              {
                // we only need a bitsel
                CarbonConst* bitIndex = mExprFactory->createConst(0, 32, false);
                finalExprTmp = mExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, arg, bitIndex, 1, 
                                                         partselOrig->isSigned(), false);
              }
              else
              {
                // we need a partsel
                ConstantRange endRange(rangeLength - 1, 0); 
                finalExprTmp = mExprFactory->createPartsel(arg, endRange, endRange.getLength(), 
                                                           partselOrig->isSigned());
              }
              
              CE_ASSERT(finalExprTmp, partselOrig);
              
              // Further simplify the bitsel/partsel we may have created
              ExprReduce recurse(mExprFactory, mTopIdent);
              CarbonExpr* finalExpr = recurse.reduce(finalExprTmp);
              
              concatExprs.push_back(finalExpr);
              rangeLength = 0;
            }
            else 
            {
              concatExprs.push_back(arg);
              rangeLength -= bitSize;
            }
          } // else
        } // for
      } // for

      CE_ASSERT(! concatExprs.empty(), partselOrig);

      // reverse the contents because we iterated in reverse
      std::reverse(concatExprs.begin(), concatExprs.end());

      // We do not need to recurse here because there is no
      // chance of a malformed expressioned at this point. The args of
      // the concat were visited before the concat transform, which
      // was done before the partsel transform.
      return mExprFactory->createConcatOp(&concatExprs, 1, range.getLength(),
                                          partselOrig->isSigned());
    } // if
  } // if concat
  
  return mExprFactory->createPartsel(expr,
                                     partselOrig->getRange(),
                                     partselOrig->getBitSize(),
                                     partselOrig->isSigned());
}

CarbonExpr*
ExprReduce::transformUnaryOp(CarbonExpr* expr, CarbonUnaryOp* unaryOrig)
{
  return mExprFactory->createUnaryOp(unaryOrig->getType(), expr, 
                                     unaryOrig->getBitSize(), 
                                     unaryOrig->isSigned());
}

CarbonExpr*
ExprReduce::transformBinaryOp(CarbonExpr* expr1, CarbonExpr* expr2,
                               CarbonBinaryOp* binaryOrig)
{
  // we can only reduce bitsel binary operators
  if (binaryOrig->getType() == CarbonExpr::eBiBitSel)
  {
    
    // expr1 can be an ident, concat, partsel, bitsel, or constant for
    // reduction
    if (CarbonIdent* ident = expr1->castIdent())
    {
      // The only thing that we can do here is reduce binaryOrig to an
      // ident if expr2 is a constant 0 (normalized lsb) and expr1 is
      // a scalar.
      DynBitVector val;
      if (ident->getBitSize() == 1)
      {
        // If the length is one, the value better be consistent
        // because we don't expect OOB accesses.
        CE_ASSERT(isKnownConstant(expr2, &val) 
                  && (val == 0), 
                  binaryOrig);
        // valid bitsel of a scalar
        return expr1;
      }
    }
    else if (CarbonBinaryOp* binOp = expr1->castBinary())
    {
      // If the expression is not a bitsel don't do anything
      if (binOp->getType() == CarbonExpr::eBiBitSel)
      {
        // Bitsel of a bitsel. expr2 better be constant 0.
        DynBitVector val;
        CE_ASSERT(isKnownConstant(expr2, &val), binaryOrig);
        
        // We could assume a result of x if it isn't 0, but we don't
        // expect this.
        CE_ASSERT(val == 0, binaryOrig);
        
        // At this point, expr1 replaces binaryOrig. We can
        // recursively reduce expr1 only and return. 
        ExprReduce recurse(mExprFactory, mTopIdent);
        return recurse.reduce(expr1);
      }
      
    }
    else if (CarbonPartsel* partSel = expr1->castPartsel())
    {
      // bitsel of a partsel. If expr2 is a known constant turn this
      // into a bitsel
      DynBitVector val;
      if (isKnownConstant(expr2, &val))
      {
        // no oob accesses. I'm not asserting here if expr2 is not
        // constant simply because we might actually synthesize such a
        // thing (unlike a bitsel of a bitsel)
        CE_ASSERT(val < partSel->getBitSize(), binaryOrig);

        // fix the absolute offset into the partsel
        const ConstantRange& partRange = partSel->getRange();
        UInt32 offset = val.value();
        UInt32 index = partRange.index(offset);
        
        CarbonConst* subIndex = mExprFactory->createConst(index, 32, false);
        
        // Get the expression from the partsel and bitsel that.
        CarbonExpr* partSelExpr = partSel->getArg(0);
        CarbonExpr* simplified = 
          mExprFactory->createBinaryOp(CarbonExpr::eBiBitSel,
                                       partSelExpr, subIndex,
                                       1, binaryOrig->isSigned(), false);
        
        // Recurse the reducer to deal with the bitsel of expr1.
        ExprReduce recurse(mExprFactory, mTopIdent);
        return recurse.reduce(simplified);
      }
    }
    else if (CarbonConcatOp* concat = expr1->castConcat())
    {
      // bitsel of a concat. If expr2 is a known constant, we need to
      // find the bit in the concat that will represent this. We'll
      // make a bitsel and recurse the reducer to deal with a
      // potential bitsel of a scalar
      DynBitVector val;
      if (isKnownConstant(expr2, &val))
      {
        CE_ASSERT(val < concat->getBitSize(), binaryOrig);
        UInt32 valInt = val.value();
        // if the repeat count is > 1, we could make this more
        // efficient, but that's a micro optimization for another day.

        CarbonExpr* targetExpr = NULL;
        for (UInt32 rpt = 0; (targetExpr == NULL) && (rpt < concat->getRepeatCount()); ++rpt)
        {
          for (SInt32 i = concat->getNumArgs() - 1;
               (targetExpr == NULL) && (i >= 0); --i)
            
          {
            CarbonExpr* arg = concat->getArg(i);
            UInt32 bitSize = arg->getBitSize();
            // Run through the concat until we find an argument that
            // corresponds with the bit select.
            if (bitSize > valInt)
              targetExpr = arg;
            else
              valInt -= bitSize;
          }
        }

        // We better have stopped; otherwise oob access.
        CE_ASSERT(targetExpr, binaryOrig);

        // At this point valInt is the index that we want for the
        // bitsel
        CarbonConst* bitIndex = mExprFactory->createConst(valInt, 32, false);
        CarbonExpr* simplified = 
          mExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, 
                                       targetExpr, bitIndex, 1,
                                       binaryOrig->isSigned(), false);

        // Reduce the possibly malformed bitsel.
        ExprReduce recurse(mExprFactory, mTopIdent);
        return recurse.reduce(simplified);
      }
    }
    else if (CarbonConst* constExpr = expr1->castConst())
    {
      // bitsel of a constant. Figure out the bit and return a
      // constant if both expressions are known.
      DynBitVector val1, val2;
      if (isKnownConstant(constExpr, &val1) && isKnownConstant(expr2, &val2))
      {
        // no oob
        CE_ASSERT(val2 < val1.size(), binaryOrig);
        
        DynBitVector resultBit(1);
        if (val1.test(val2.value()))
          resultBit.set();
        return mExprFactory->createConst(resultBit, CarbonConst::eUnsigned, 1);
      }
    }
  }
  
  return mExprFactory->createBinaryOp(binaryOrig->getType(),
                                      expr1, expr2,
                                      binaryOrig->getBitSize(),
                                      binaryOrig->isSigned(), false);
  
}

CarbonExpr*
ExprReduce::transformTernaryOp(CarbonExpr* expr1, CarbonExpr* expr2, 
                               CarbonExpr* expr3, 
                               CarbonTernaryOp* ternaryOrig)
{
  CarbonExpr::ExprT type = ternaryOrig->getType();
  UInt32 bitSize = ternaryOrig->getBitSize();
  return mExprFactory->createTernaryOp(type, expr1, expr2, expr3, bitSize,
                                       ternaryOrig->isSigned(), false);
}


CarbonExpr*
ExprReduce::transformEdge(CarbonExpr* expr, CarbonEdge* edgeOrig)
{
  ClockEdge clockEdge = edgeOrig->getEdge();
  UInt32 bitSize = edgeOrig->getBitSize();
  return mExprFactory->createEdge(clockEdge, expr, bitSize);
}

static void sExpandConcat(CarbonConcatOp* concat, CarbonExprVector* exprVec)
{
  UInt32 numArgs = concat->getNumArgs();
  UInt32 repeat = concat->getRepeatCount();
  for (UInt32 j = 0; j < repeat; ++j)
  {
    for (UInt32 i = 0; i < numArgs; ++i)
    {
      CarbonExpr* expr = concat->getArg(i);
      CarbonConcatOp* nested = expr->castConcat();
      if (nested)
        sExpandConcat(nested, exprVec);
      else
        exprVec->push_back(expr);
    }
  }
}

CarbonExpr*
ExprReduce::transformConcatOp(CarbonExprVector* exprVec, UInt32 repeatCnt,
                              CarbonConcatOp *concatOrig)
{
  CarbonExprVector concatExprs;
  // look for a nested concat
  for (CarbonExprVector::iterator p = exprVec->begin(), e = exprVec->end();
       p != e; ++p)
  {
    CarbonExpr* expr = *p;
    CarbonConcatOp* concatOp = expr->castConcat();
    if (concatOp)
      sExpandConcat(concatOp, &concatExprs);
    else
      concatExprs.push_back(expr);
  }
  return mExprFactory->createConcatOp(&concatExprs, repeatCnt, concatOrig->getBitSize(), concatOrig->isSigned());
}


CarbonExpr*
ExprReduce::transformIdent(CarbonIdent* ident)
{
  // Ensure there are no ident loops present.  If we ever find the
  // parent ident when reducing an expression, we're screwed.
  CE_ASSERT(ident != mTopIdent, ident);
  
  CarbonExpr* ret = replaceIdent(ident);
  CE_ASSERT(ret, ident);
  
  if (ret != ident)
  {
    // Check for a cycle on the ident
    IdentCycleDetect cycleDetect(ident);
    cycleDetect.visitExpr(ret);
    // aborted if a cycle has been detected.
  }
  return ret;
}


bool ExprReduce::isKnownConstant(const CarbonExpr* expr, DynBitVector* val)
{
  const CarbonConst* constExpr = expr->castConst();

  if (constExpr == NULL)
    return false;

  // Get the value of expr and make sure it is 0.
  
  // If not fully known assert here, since we don't currently
  // expect this.
  if (constExpr->castConstXZ() != NULL)
    return false;
  constExpr->getValue(val);
  return true;
}



// ExprReduceSymTab implementation 

ExprReduceSymTab::ExprReduceSymTab(CarbonIdentExprMap* identExprMap, 
                                   ESFactory* exprFactory, 
                                   CarbonIdent *ident, Mode mode, 
                                   IdentSet* replacedIdents) :
  ExprReduce(exprFactory, ident), 
  mIdentExprMap(identExprMap), 
  mReplacedIdents(replacedIdents), 
  mMode(mode)
{}
  
ExprReduceSymTab::~ExprReduceSymTab() 
{}

  
CarbonExpr* ExprReduceSymTab::replaceIdent(CarbonIdent* ident)
{
  CarbonExpr* replacedExpr = mIdentExprMap->getExpr(ident);
  if (replacedExpr == NULL)
  {
    // The replaced expr is the ident passed in
    replacedExpr = ident;
      
    // unless we are going through a backpointer and this ident has
    // a backpointer (it may be a bp leaf already and not have a bp).
    switch(mMode)
    {
    case eStandard:
      break;
    case eThruBackPointer:
      {
        SymTabIdent* symTabIdent = ident->castSymTabIdent();
        CE_ASSERT(symTabIdent, ident);
        SymTabIdentBP* identBP = symTabIdent->castSymTabIdentBP();
        if (identBP)
          replacedExpr = identBP->getBackPointer();
      }
      break;
    }
  }
  else
  {
    if (mReplacedIdents)
      mReplacedIdents->insert(ident);
      
    // We could have nested exprs.
    ExprReduceSymTab recurseAnalyze(mIdentExprMap, mExprFactory, mTopIdent, mMode, mReplacedIdents);
    replacedExpr = recurseAnalyze.reduce(replacedExpr);
  }
  return replacedExpr;
    
}
