/***************************************************************************************
  Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#include "exprsynth/ExprFlatten.h"
#include "compiler_driver/CarbonDBWrite.h"

ExprFlatten::ExprFlatten(CbuildSymTabBOM* bomManager,
                         CarbonIdent *ident,
                         CarbonConcatOp *expr) :
  mBOMManager( bomManager ),
  mExpr( expr )
{
  INFO_ASSERT( mBOMManager != NULL,
               "Cannot create ExprFlatten class with NULL BOM Manager");
  INFO_ASSERT( ident != NULL,
               "Cannot create ExprFlatten class with NULL ident");
  INFO_ASSERT( expr != NULL,
               "Cannot create ExprFlatten class with NULL expression");
  mIdentSet.insert( ident );
}

CarbonExpr*
ExprFlatten::transformPartsel(CarbonExpr*, CarbonPartsel* partselOrig) 
{
  return partselOrig;
}

CarbonExpr*
ExprFlatten::transformUnaryOp(CarbonExpr*, CarbonUnaryOp* unaryOrig)
{
  return unaryOrig;
}


CarbonExpr*
ExprFlatten::transformBinaryOp(CarbonExpr* , CarbonExpr* ,
                               CarbonBinaryOp* binaryOrig)
{
  return binaryOrig;
}
  
CarbonExpr*
ExprFlatten::transformTernaryOp(CarbonExpr* , CarbonExpr* , 
                                CarbonExpr* , CarbonTernaryOp* ternaryOrig)
{
  return ternaryOrig;
}


CarbonExpr*
ExprFlatten::transformEdge(CarbonExpr*, CarbonEdge* edgeOrig)
{
  return edgeOrig;
}


//! Recursively descend through any nested CarbonConcatOps and gather up
// the flattened and expanded set of idents.
void
ExprFlatten::collectConcatArgs( CarbonConcatOp *concat,
                                CarbonExprVector &flattenedConcatArgs )
{
  CarbonExprVector localVec;

  // populate localvec with the set of idents in the concatOp's args.
  // If an ident has a concatOp expression, recursively expand it.
  for ( UInt32 i = 0; i < concat->getNumArgs(); ++i )
  {
    CarbonExpr *subExpr = concat->getArg(i);
    CarbonIdent *ident = subExpr->castIdent();

    if (ident == NULL)
    {
      // Can only handle idents and constants
      CE_ASSERT(subExpr->castConst() != NULL, concat);
      localVec.push_back( subExpr );
    }
    else if (ident)
    {
      // Make sure we don't have a node that's one of our parent nodes!
      CE_ASSERT(mIdentSet.find(ident) == mIdentSet.end(), concat);
      
      CarbonExpr* subIdentExpr = mBOMManager->getExpr(ident);
      if ( subIdentExpr == NULL || (subIdentExpr->castConst() != NULL) )
      {
        // Idents without expressions or with constant expressions go
        // into the vector
        localVec.push_back( subExpr );
      }
      else
      {
        // remember this ident to check for loops
        mIdentSet.insert( ident );
        // Idents with concats are recursively expanded
        CarbonConcatOp *subConcat = subIdentExpr->castConcat();

        // Only handling nested concats for now
        CE_ASSERT(subConcat != NULL, concat);

        collectConcatArgs( subConcat, localVec );
        // done with this expr expansion; remove the ident from checking
        mIdentSet.erase( ident );
      }
    }
  }

  // Add the (possibly repeated) localVec into the expanded arg vector
  for ( UInt32 c = 0; c < concat->getRepeatCount(); ++c )
  {
    for ( UInt32 i = 0; i < localVec.size(); ++i )
    {
      flattenedConcatArgs.push_back( localVec[i] );
    }
  }
}


//! If a CarbonConcatOp has any nested concatOps, create a new
// CarbonConcatOp with all the netsted concat args flattened into it.
CarbonExpr*
ExprFlatten::transformConcatOp(CarbonExprVector *, UInt32,
                               CarbonConcatOp *concatOrig)
{
  CarbonExpr *retExpr;
  collectConcatArgs( concatOrig, mFlattenedConcatArgs );

  if ( mFlattenedConcatArgs.size() > concatOrig->getNumArgs() )
  { 
    // Something was expanded, so let's create a new concatOp
    ESFactory *exprFactory = mBOMManager->getExprFactory();
    retExpr = exprFactory->createConcatOp( &mFlattenedConcatArgs, // args
                                           1, // repeats have been flattened
                                           concatOrig->getBitSize(),// same size
                                           concatOrig->isSigned());
  }
  else
  {
    retExpr = concatOrig;
  }

  // Remove the refcount to the original expression
  concatOrig->decRefCnt();
  return retExpr;
}


// Make sure there ar eno ident loops in an expression
CarbonExpr*
ExprFlatten::transformIdent(CarbonIdent* ident)
{
  // Ensure there are no ident loops present.  If we ever find the
  // parent ident when flattening an expression, we're screwed.
  CE_ASSERT(mIdentSet.find(ident) == mIdentSet.end(), ident);
  return ident;
}


