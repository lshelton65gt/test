// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include <cstddef>
#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "util/StringAtom.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUPortConnection.h"
#include "exprsynth/ExprFactory.h"
#include "TranslateExpr.h"
#include "flow/FlowClassifier.h"
#include "exprsynth/ESPopulateExpr.h"


// The following are the set of expression handle class functions

ESExprHndl::ESExprHndl(CarbonExpr* expr)
{
  mExpr = expr;
  if (mExpr != NULL)
    mExpr->incRefCnt();
}

ESExprHndl::~ESExprHndl()
{
  if (mExpr != NULL)
    mExpr->decRefCnt();
}

ESExprHndl::ESExprHndl(const ESExprHndl& exprHndl)
{
  mExpr = exprHndl.mExpr;
  if (mExpr != NULL)
    mExpr->incRefCnt();
}

ESExprHndl& ESExprHndl::operator=(const ESExprHndl& exprHndl)
{
  if (&exprHndl != this)
  {
    if (mExpr != NULL)
      mExpr->decRefCnt();
    mExpr = exprHndl.mExpr;
    if (mExpr != NULL)
      mExpr->incRefCnt();
  }
  return *this;
}

class SizeWalker : public CarbonExprWalker
{
public:
  //! constructor
  SizeWalker(int maxSize) : mMaxSize(maxSize), mSize(0) {}

  //! Function to visit a part select
  virtual bool preVisitPartsel(CarbonPartsel*) { return countOperator(); }

  //! Function to visit a unary op
  virtual bool preVisitUnaryOp(CarbonUnaryOp*) { return countOperator(); }

  //! Function to visit a binary op
  virtual bool preVisitBinaryOp(CarbonBinaryOp*) { return countOperator(); }

  //! Function to visit a ternary op
  virtual bool preVisitTernaryOp(CarbonTernaryOp*) { return countOperator(); }

  //! Function to visit a nary op
  virtual bool preVisitNaryOp(CarbonNaryOp*) { return countOperator(); }

  //! Function to visit a concat op
  virtual bool preVisitConcatOp(CarbonConcatOp*) { return countOperator(); }

  //! Returns true if the expression exceeds the max size
  bool numOpsGreaterThan(CarbonExpr* expr)
  {
    visitExpr(expr);
    return mSize > mMaxSize;
  }

private:
  bool countOperator(void)
  {
    ++mSize;
    return mSize <= mMaxSize;
  }

  int mMaxSize;
  int mSize;
}; // class SizeWalker : public CarbonExprWalker


bool ESExprHndl::numOpsGreaterThan(int size)
{
  SizeWalker walker(size);
  return walker.numOpsGreaterThan(mExpr);
}

void ESExprHndl::print(bool recurse, int indent) const
{
  INFO_ASSERT(mExpr != NULL, "Invalid CarbonExpr encountered");
  mExpr->print(recurse, indent);
}

void ESExprHndl::printVerilog(int indent) const
{
  INFO_ASSERT(mExpr != NULL, "Invalid CarbonExpr encountered");
  for (int i = 0; i < indent; ++i)
    UtIO::cout() << " ";
  mExpr->printVerilog();
  UtIO::cout() << "\n";
}





// The following class is used to create Carbon expressions from
// nucleus expressions. It is derived from the ESTranslateExpr class.
class TranslateExpr : public ESTranslateExpr
{
public:
  //! constructor
  TranslateExpr(ESFactory* factory, ESPopulateExpr* populateExpr) :
    ESTranslateExpr(factory), mFactory(factory), mPopulateExpr(populateExpr),
    mCallback(NULL)
  {
    mInputSetStack = new InputSetStack;
  }

  //! destructor
  ~TranslateExpr()
  {
    delete mInputSetStack;
  }

  //! Function to set the stop call back and user data for all traversals
  /*!
   *  \return The previous value of the callback variable
   */
  ESCallback* putCallback(ESCallback* callback)
  {
    ESCallback* prevCallback = mCallback;
    mCallback = callback;
    return prevCallback;
  }

  //! Main function to recursively a nucleus expression
  CarbonExpr* translateExpr(NUExpr* expr, ESPopulateExpr::InputSet* inputSet)
  {
    // Push the input set on the stack so that the translateNet
    // callback can get the data.
    mInputSetStack->push_back(inputSet);

    // Call the base class to start the conversion
    CarbonExpr* retExpr = recursiveTranslateExpr(expr);
    mInputSetStack->pop_back();
    return retExpr;
  }

private:
  // Function to translate a net (overloaded from base class)
  CarbonExpr* translateNet(NUNet* net, const ConstantRange& range,
                           UInt32 bitSize, bool isSigned)
  {
    ESPopulateExpr::InputSet* inputSet = mInputSetStack->back();
    return mPopulateExpr->createNetExpr(net, range, bitSize, isSigned,
                                        inputSet, mCallback);
  }

  // Local data for use in traversal
  typedef UtVector<ESPopulateExpr::InputSet*> InputSetStack;
  InputSetStack* mInputSetStack;

  // Local utilities
  ESFactory* mFactory;
  ESPopulateExpr* mPopulateExpr;
  ESCallback* mCallback;
}; // class TranslateExpr : public ESTranslateExpr


// The following are the set of populate expression class functions

ESPopulateExpr::ESPopulateExpr(ESFactory* factory,
			       NUNetRefFactory* netRefFactory) :
  mFactory(factory), mNetRefFactory(netRefFactory)
{
  mTranslateExpr = new TranslateExpr(factory, this);
  mSeenFlows = new SeenFlows;
  mIgnoreMultiDrivenStoppers = false;
  mCaseFlowCache = new CaseFlowCache;
}

ESPopulateExpr::~ESPopulateExpr()
{
  delete mTranslateExpr;
  INFO_ASSERT(mSeenFlows->empty(),
              "Potentially premature Destruction of CarbonExpr process");
  delete mSeenFlows;
  clearCaseFlowCache();
  delete mCaseFlowCache;
}

ESFactory* ESPopulateExpr::getFactory()
{
  return mFactory;
}

ESExprHndl ESPopulateExpr::create(FLNode* flow, ESCallback* callback,
				  bool newCovered)
{
  mCache.clear();


  // Store the stop call back in the expression translation
  // class. There has to be a better way to do this
  ESCallback* prevCallback = mTranslateExpr->putCallback(callback);

  // Recursively visit this unelaborated flow creating an expression.
  // Start a new covered set so that we handle cycles cleanly.
  pushCovered(newCovered);
  CarbonExpr* expr = recursiveCreate(flow, callback);
  popCovered(newCovered);

  // We are not storing this expression anywhere but the handle so
  // decrement the reference count. Creating the handle will
  // increment it so that it never really hits zero.
  ESExprHndl exprHndl(expr);
  if (expr != NULL)
    expr->decRefCnt();
  mTranslateExpr->putCallback(prevCallback);
  return exprHndl;
}


ESExprHndl ESPopulateExpr::createWithEdges(FLNode* flow, ESCallback* callback,
                                           bool newCovered)
{
  ESExprHndl functional_expr_handle = create(flow,callback,newCovered);
  CarbonExpr * functional_expr = functional_expr_handle.getExpr();
  if (not functional_expr) {
    return functional_expr_handle;
  }

  NUUseDefNode * node = flow->getUseDefNode();
  if (node->isSequential()) {
    mCache.clear();

    // Store the stop call back in the expression translation
    // class. There has to be a better way to do this
    ESCallback* prevCallback = mTranslateExpr->putCallback(callback);

    NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(node);
    UInt32 bit_size = functional_expr->getBitSize();
    CarbonExprVector edge_components;
    edge_components.push_back(functional_expr);
    for (NUAlwaysBlock::EdgeExprLoop loop = always->loopEdgeExprs();
         not loop.atEnd();
         ++loop) {
      NUEdgeExpr * edge = (*loop);
      pushCovered(newCovered);
      CarbonExpr * edgeCExpr = translateEdgeExpr(flow, edge);
      popCovered(newCovered);
      FLN_ASSERT(edgeCExpr, flow); // tbd. error handling
      edge_components.push_back(edgeCExpr);
      bit_size += edgeCExpr->getBitSize();
    }  
    CarbonExpr * concat = mFactory->createConcatOp(&edge_components,
                                                   1, bit_size,
                                                   false); // edges are unsigned
    functional_expr->decRefCnt();
    functional_expr = concat;

    mTranslateExpr->putCallback(prevCallback);
  }    
  ESExprHndl exprHndl(functional_expr);
  functional_expr->decRefCnt();

  return exprHndl;
}

ESExprHndl ESPopulateExpr::createIfCondition(FLNode* flow, ESCallback* callback)
{
  NUIf* ifStmt = dynamic_cast<NUIf*>(flow->getUseDefNode());
  if (!ifStmt)
  {
    ESExprHndl invalid;
    return invalid;
  }

  mCache.clear();

  // Store the stop call back in the expression translation
  // class. There has to be a better way to do this
  ESCallback* prevCallback = mTranslateExpr->putCallback(callback);

  // Create the conditional expression
  pushCovered(true);
  CarbonExpr* condExpr = translateExpr(flow, ifStmt->getCond());
  ESExprHndl hndl(condExpr);
  popCovered(true);

  // Restore the stop call back
  mTranslateExpr->putCallback(prevCallback);

  return hndl;
}


ESExprHndl ESPopulateExpr::createSingleEdge(FLNode* flow, ESCallback* callback)
{
  NUUseDefNode* useDef = flow->getUseDefNode();
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  if (!always || !useDef->isSequential())
  {
    ESExprHndl invalid;
    return invalid;
  }

  mCache.clear();

  // Store the stop call back in the expression translation
  // class. There has to be a better way to do this
  ESCallback* prevCallback = mTranslateExpr->putCallback(callback);

  // Get the edge expression and translate it
  pushCovered(true);
  NUEdgeExpr* edge = always->getEdgeExpr();
  CarbonExpr* condExpr = translateEdgeExpr(flow, edge->getExpr());
  popCovered(true);

  // If it was a negedge then the expressions needs to be inverted
  if ((condExpr != NULL) && (edge->getEdge() == eClockNegedge)) {
    condExpr = mFactory->createUnaryOp(CarbonExpr::eUnBitNeg, condExpr, 1, false);
  }

  // Restore the stop call back
  mTranslateExpr->putCallback(prevCallback);

  ESExprHndl hndl(condExpr);
  return hndl;
}

class ElabCallback : public ESCallback
{
public:
  //! constructor
  ElabCallback(FLNodeElab* flow, ESCallbackElab* callbackElab,
	       ESPopulateExpr* populateExpr, ESFactory* factory,
               NUNetRefFactory* netRefFactory) :
    ESCallback(populateExpr), mFlow(flow), mCallbackElab(callbackElab),
    mFactory(factory), mNetRefFactory(netRefFactory)
  {}

  //! Function to test whether we should stop at this flow node or not
  bool stop(FLNode* flow, bool isContDriver)
  {
    // If we have not hit a continous driver, then we haven't left the
    // block yet, so keep going.
    if (! isContDriver)
      return false;

    // Make sure the net has changed. If it hasn't then we want to
    // continue. Otherwise we stop and let the createIdentExpr
    // function below deal with it.
    STBranchNode* hier = mFlow->getHier();
    NUNetElab* netElab = flow->getDefNet()->lookupElab(hier);
    if (netElab == mFlow->getDefNet())
    {
      // The net hasn't changed, but the user may want to stop and
      // create an ident expression. So ask them just to be sure
      FLNodeElabVector flows;
      flows.push_back(mFlow);
      NUNetRefHdl netRef = mFlow->getDefNetRef(mNetRefFactory);
      return mCallbackElab->stop(&flows, netRef, true);      
    }
    else
    {
      // The net has changed. Let the createIdentExpr function deal
      // with it
      return true;
    }
  }

  //! Function to create an identifier (free variable) from a unelab flow
  /*! This function converts unelaborated flow to elaborated flow and
   *  then uses the elaborated version of the function. It converts
   *  the unelaborated flow to elaborated flow using the hierarchy for
   *  the elaborated flow stored in this class.
   */
  virtual CarbonExpr* createIdentExpr(const FLNodeVector& flows,
                                      UInt32 bitSize, bool isSigned,
                                      bool hardStop, NUNetRefHdl* resNetRef);

  //! Function to create an identifier (free variable) from elaborated flow
  virtual CarbonExpr* createIdentExpr(const FLNodeElabVector& elabFlows,
                                      const NUNetRefHdl& reqNetRef,
                                      UInt32 bitSize, bool isSigned,
                                      bool hardStop);

private:
  // Type to organize FLNodeElab's to create a concat
  typedef UtMap<NUNetRefHdl, FLNodeElabVector> NetRefFlowElabMap;
  typedef NetRefFlowElabMap::iterator NetRefFlowElabMapIter;

  // Functor class to get a normalized net ref from a FLNode and an
  // NUNetElab
  class ElabNetRefFunctor : public ESPopulateExpr::NetRefFunctor
  {
  public:
    //! constructor
    ElabNetRefFunctor(NUNetElab* netElab) : mNetElab(netElab)
    {}

    //! destructor
    ~ElabNetRefFunctor() {}

    //! Routine to get a normalized net ref
    NUNetRefHdl getNetRef(FLNode* flow, NUNetRefFactory*);

  private:
    //! Hide copy and assign constructors.
    ElabNetRefFunctor(const ElabNetRefFunctor&);
    ElabNetRefFunctor& operator=(const ElabNetRefFunctor&);

    // Local data
    NUNetElab* mNetElab;
  }; // class ElabNetRefFunctor : public ESPopulateExpr::NetRefFunctor

  // Functions to do the parts of createIdentExpr
  bool gatherElaboratedFlows(const FLNodeVector& flows,
                             FLNodeElabVector* elabFlows,
                             NUNetRefHdl* retNetRef);
  bool isHardStop(const FLNodeElabVector& elabFlows);
  bool createConcatMap(const FLNodeElabVector& elabFlows,
                       NetRefFlowElabMap* netRefFlowElabMap,
                       const NUNetRefHdl& reqNetRef);

  // Checks if flow is a nested flow of visitFlow
  bool matchFlow(FLNode* flow, FLNode* visitFlow) const;

  //! Checks if this elaborated flow matches an unelaborated flow
  bool matchElabFlow(FLNode* flow, FLNodeElab* flowElab) const;

  // Checks if this flow represents a hierarchy change (port connection etc)
  bool isHierFlow(FLNode* flow) const;

  FLNodeElab* mFlow;
  ESCallbackElab* mCallbackElab;
  ESFactory* mFactory;
  NUNetRefFactory* mNetRefFactory;
}; // class ElabCallback : public ESCallback

CarbonExpr*
ElabCallback::createIdentExpr(const FLNodeVector& flows, UInt32 bitSize,
                              bool isSigned, bool hardStop, NUNetRefHdl* resNetRef)
{
  // Gather the flow nodes for this identity based on type. We gather
  // the initial block, pull driver, and normal drivers in different
  // structures
  NUNetRefHdl netRef;
  FLNodeElabVector elabFlows;
  bool isContDriver = gatherElaboratedFlows(flows, &elabFlows, &netRef);
  *resNetRef = netRef;

  // If we have not hit a continous driver, then we haven't left the
  // block yet, so we can't create a leaf node for this. We have to
  // give up.
  if (!isContDriver)
    return NULL;

  return createIdentExpr(elabFlows, netRef, bitSize, isSigned, hardStop);
}

CarbonExpr*
ElabCallback::createIdentExpr(const FLNodeElabVector& elabFlows,
                              const NUNetRefHdl& reqNetRef,
                              UInt32 bitSize, bool isSigned, bool hardStop)
{
  // Break this set of elaborated flows into buckets. If we are
  // succesful, we can create a concat operator over the
  // bucket. Otherwise, we will have all the FLNodeElab's in one
  // bucket and we create an expression of that.
  NetRefFlowElabMap netRefFlowElabMap;
  bool isValidConcat;
  isValidConcat = createConcatMap(elabFlows, &netRefFlowElabMap, reqNetRef);

  // Go over all the buckets and create expressions from them.
  ESSortedExprMap exprMap;
  bool failed = false;
  for (NetRefFlowElabMapIter i = netRefFlowElabMap.begin();
       i != netRefFlowElabMap.end() && !failed;
       ++i)
  {
    // Check if ther are no elaborated flows. This can happen when the
    // elaborated flow is dead even though the unelaborated flow
    // isn't. If so, return a constant 0 for that part
    CarbonExpr* expr = NULL;
    FLNodeElabVector& bucketFlows = i->second;
    const NUNetRefHdl& netRef = i->first;
    if (bucketFlows.empty()) {
      NUNet* net = netRef->getNet();
      expr = mFactory->createConstZ(netRef->getNumBits(), net->isSigned());

    } else {
      // Check if this is a hard stop. The reasons for a hard stop are
      // described in this sub-routine.  overlap between the flows within
      // one group.
      if (isHardStop(bucketFlows))
        hardStop = true;

      // If this isn't a hard stop, check if the user wants us to stop
      bool userStop = false;
      if (!hardStop && mCallbackElab->stop(&bucketFlows, netRef, true))
        userStop = true;

      // Callbacks may need to do cycle detection.  This provides the
      // information they need to do it.
      FLNodeElabVector addedFlows;
      mCallbackElab->addFlowsInProgress(bucketFlows, &addedFlows);

      // If we shouldn't stop, and we have a single bucket, recursively
      // create an expression for this flow.
      if (!hardStop && !userStop) {
        FLNodeElab* elabFlow = *(bucketFlows.begin());
        FLN_ELAB_ASSERT(bucketFlows.size() == 1, elabFlow);
        ESExprHndl hndl = mCallbackElab->create(elabFlow);
        expr = hndl.getExpr();
      }

      // If it fails, or we have more than one flow then create an
      // ident here.
      if (expr == NULL) {
        ESExprHndl hndl;
        UInt32 subBitSize = netRef->getNumBits();
        hndl = mCallbackElab->createIdentExpr(&bucketFlows, subBitSize,
                                              isSigned, true);
        expr = hndl.getExpr();
      }

      // Callbacks may need to do cycle detection.  This provides the
      // information they need to do it.
      mCallbackElab->removeFlowsInProgress(addedFlows);
    } // } else

    // If creating an ident fails, then give up.
    if (expr == NULL) {
      failed = true;
    } else {
      exprMap.insert(ESSortedExprMap::value_type(netRef, expr));
    }
  } // for

  // If any of the above failed, give up
  if (failed)
    return NULL;

  // If we get here, we have expression(s) we can return to our
  // caller.
  return mPopulateExpr->createMultiDriver(isValidConcat, reqNetRef, bitSize,
                                          isSigned, exprMap);
} // CarbonExpr* ElabCallback::createIdentExpr

NUNetRefHdl
ElabCallback::ElabNetRefFunctor::getNetRef(FLNode* flow,
                                           NUNetRefFactory* factory)
{
  NUNet* net = mNetElab->getNet();
  NUNetRefHdl netRef = flow->getDefNetRef();
  return factory->createNetRefImage(net, netRef);
}

bool ElabCallback::matchElabFlow(FLNode* flow, FLNodeElab* flowElab) const
{
  // Check the simple case where the elab flow points to the unelab flow
  if (flowElab->getFLNode() == flow) {
    return true;
  }

  // Another simple case, check if we have the same net refs
  FLNode* thisFlow = flowElab->getFLNode();
  if (matchFlow(flow, thisFlow)) {
    // They are the same net, compare net refs
    NUNetRefHdl netRef = flow->getDefNetRef();
    NUNetRefHdl elabNetRef = flowElab->getDefNetRef(mNetRefFactory);
    return netRef->overlaps(*elabNetRef);
  }

  // Otherwise, if we cross a hierarchy, then be pessimitic and match
  // (Can we do better here?)
  NUNet* flowNet = flow->getDefNet();
  NUNet* flowElabNet = flowElab->getDefNet()->getNet();
  if ((flowNet != flowElabNet) || isHierFlow(flow)) {
    return true;
  }

  // Must not be a match
  return false;
}

bool ElabCallback::gatherElaboratedFlows(const FLNodeVector& flows, 
                                         FLNodeElabVector* elabFlows,
                                         NUNetRefHdl* retNetRef)
{
  // We accumulate the unelaborated net ref to see what we are
  // required to fill in. This helps us make sure we found everything
  // in the elaborated world.
  NUNetRefHdl netRef = mNetRefFactory->createEmptyNetRef();

  // Visit all the elaborated and unelaborated flows and match them
  // up. This way we can find all the elaborated flows that represent
  // the unelaborated flows.
  bool isContDriver = true;
  STBranchNode* hier = mFlow->getHier();
  FLNodeVectorIter p;
  FLNodeElabSet covered;
  FLNodeElabVector initialFlows;
  for (p = flows.begin(); (p != flows.end()) && isContDriver; ++p) {
    // Accumulate the unelaborated net refs
    FLNode* flow = *p;
    netRef = mNetRefFactory->merge(netRef, flow->getDefNetRef());

    // If we have not hit a continous driver, then we haven't left the
    // block yet, so we can't create a leaf node for this. We have to
    // give up.
    NUUseDefNode* useDef = flow->getUseDefNode();
    if ((useDef != NULL) && !useDef->isContDriver()) {
      isContDriver = false;
    } else {
      // Find the elaborated net that represent this unelaborated net
      NUNet* flowNet = flow->getDefNet();
      NUNetElab* netElab = flowNet->lookupElab(hier);
      
      // Find the elaborated flow node(s) that represent this
      // unelaborated flow.
      for (NUNetElab::DriverLoop l = netElab->loopContinuousDrivers();
           !l.atEnd(); ++l) {
        FLNodeElab* elabFlow = *l;
        if (covered.find(elabFlow) == covered.end()) {
          // Make sure this flow node matches the reference we are
          // interested in. But if we cross hierarchy (nets are
          // different) then we have to be pessimistic.
          if (matchElabFlow(flow, elabFlow)) {
            // Put initials in different buckets. We ignore initials
            // unless they are the only drivers.
            NUUseDefNode* useDef = elabFlow->getUseDefNode();
            if ((useDef == NULL) || !useDef->isInitial()) {
              // Add this to our elaborated flows. We use the covered
              // set to make sure we don't get repeats.
              covered.insert(elabFlow);
              elabFlows->push_back(elabFlow);
            } else {
              initialFlows.push_back(elabFlow);
            }
          }
        } // if
      } // for
    } // else
  } // for

  // If we only found initial flows, then use that
  if (elabFlows->empty() && !initialFlows.empty())
    *elabFlows = initialFlows;

  // At this point we can sometimes still have no flows. Look at test
  // case test/langcov/one_write_one_not.v. For some reason there are
  // cases where we have undriven nets without elaborated bound nodes.
  if (elabFlows->empty())
    isContDriver = false;

  // Make sure we covered the entire required net ref
  if (isContDriver)
  {
    NUNetRefHdl elabNetRef = mNetRefFactory->createEmptyNetRef();
    NUNet* net = NULL;
    for (FLNodeElabVectorIter i = elabFlows->begin(); i != elabFlows->end(); ++i)
    {
      FLNodeElab* elabFlow = *i;
      NUNetRefHdl netRef = elabFlow->getDefNetRef(mNetRefFactory);
      net = elabFlow->getDefNet()->getNet();
      NUNetRefHdl normNetRef = mNetRefFactory->createNetRefImage(net, netRef);
      elabNetRef = mNetRefFactory->merge(elabNetRef, normNetRef);
    }

    // If the elaborated net name is not the same size as the net ref
    // give up. This means the net we picked to be the name is
    // smaller/bigger than what we need. In the future we could zero
    // extend/truncate.
    if (net->getBitSize() != netRef->getNet()->getBitSize()) {
      isContDriver = false;
    } else {
      // Make sure we covered the entire net ref
      NUNetRefHdl normNetRef = mNetRefFactory->createNetRefImage(net, netRef);

      // Allow for some bits of the net to be dead.
      // This used to check that the net refs are equivalent, but that was
      // relaxed when we created non-pessimistic elaborated flow.
      if (not normNetRef->covers(*elabNetRef))
        isContDriver = false;
    }

    // Return the net bits represented by the unelaborated flow
    if (isContDriver) {
      *retNetRef = mNetRefFactory->createNetRefImage(netRef->getNet(), elabNetRef);
    }

  } // if
  
  return isContDriver;
} // bool ElabCallback::gatherElaboratedFlows

// Find if the flow is a nested version of visitFlow. This is a
// recursive routine that walks the visitFlow's nesting to see if
// there is a match.
//
// This is a worker function to check if an elaborated flow node for
// an always block or continous assignment matches an unelaborated
// flow for a statement within the continous driver. This is routine
// is really meant for always blocks which have lots of nesting.
bool ElabCallback::matchFlow(FLNode* flow, FLNode* visitFlow) const
{
  // Check if we have an immediate match
  if (flow == visitFlow) {
    return true;
  }

  // Check if it matches in our nested flow
  for (Iter<FLNode*> l = visitFlow->loopNested(); !l.atEnd(); ++l) {
    FLNode* nested = *l;
    if (matchFlow(flow,nested))
      return true;
  }

  return false;
} // bool ElabCallback::matchFlow

// This routine determines if there is a hierarchical change through
// this flow. This occurs for module instance and port connection
// flows.
bool ElabCallback::isHierFlow(FLNode* flow) const
{
  // Port bound nodes are hierarchy changes
  if (flow->getType() == eFLBoundPort) {
    return true;
  }

  // Check if the use def is a port connection or module instance. Those are
  // hierarchy changes.
  NUUseDefNode* useDef = flow->getUseDefNode();
  if ((useDef != NULL) &&
      ((useDef->getType() == eNUModuleInstance) ||
       (useDef->getType() == eNUPortConnectionInput) ||
       (useDef->getType() == eNUPortConnectionOutput) ||
       (useDef->getType() == eNUPortConnectionBid))) {
    return true;
  }

  return false;
}

bool ElabCallback::isHardStop(const FLNodeElabVector& elabFlows)
{
  // Check the simple cases where we hit a cycle or we hit a pi
  bool hardStop = false;
  bool allPI = true;
  int numDrivers = 0;
  for (FLNodeElabVectorIter i = elabFlows.begin();
       (i != elabFlows.end()) && !hardStop;
       ++i)
  {
    // Check if we hit a cycle
    FLNodeElab* flowElab = *i;
    ++numDrivers;
    if (flowElab == mFlow)
      hardStop = true;

    // Check if we hit a primary input
    if (flowElab->getUseDefNode() != NULL)
      allPI = false;
  }

  // If it is a PI or the number of drivers is more than one stop
  if (allPI || (numDrivers > 1)) hardStop = true;

  return hardStop;
} // bool ElabCallback::isHardStop

bool
ElabCallback::createConcatMap(const FLNodeElabVector& elabFlows,
                              NetRefFlowElabMap* netRefFlowElabMap,
                              const NUNetRefHdl& reqNetRef)
{
  // Create a reverse map from unelaborated flows to elaborated
  // flows. The routines we need to create concats work off
  // unelaborated flows. I thought about using a template function but
  // I haven't been happy dealing with them.
  //
  // Also get a net elab to normalize net refs in isConcat. All
  // elaborated flows should have the same net elab.
  //
  // Also create a vector of unelaborated flows since that is what we
  // use to process concats.
  typedef UtMap<FLNode*, FLNodeElabVector> ReverseMap;
  ReverseMap reverseMap;
  FLNodeVector flows;
  NUNetElab* netElab = NULL;
  for (FLNodeElabVectorIter i = elabFlows.begin(); i != elabFlows.end(); ++i)
  {
    // Insert it into the reverse map
    FLNodeElab* elabFlow = *i;
    FLNode* flow = elabFlow->getFLNode();
    FLNodeElabVector& reverseFlowElabs = reverseMap[flow];
    reverseFlowElabs.push_back(elabFlow);

    // Get the net elab
    if (netElab == NULL)
      netElab = elabFlow->getDefNet();
    else
      NU_ASSERT(netElab == elabFlow->getDefNet(), netElab);

    // Add it to the unelaborated flows for processing concats
    flows.push_back(flow);
  }

  // Check if it is a concat
  ESFlowBuckets flowBuckets;
  ElabNetRefFunctor fnctr(netElab);
  NUNetRefHdl elabNetRef;
  bool isValidConcat = mPopulateExpr->isConcat(flows, reqNetRef, fnctr,
                                               &flowBuckets, &elabNetRef);

  // Convert the map from net refs to unelaborated flows to a map from
  // net refs to elaborated flows. This is where the reverse map comes
  // in. Convert everything to the requested net ref
  for (ESFlowBucketsIter i = flowBuckets.begin(); i != flowBuckets.end(); ++i) {
    // Get the spot to put these elaborated flows
    const NUNetRefHdl& netRef = i->first;
    NUNet* reqNet = reqNetRef->getNet();
    NUNetRefHdl normNetRef = mNetRefFactory->createNetRefImage(reqNet, netRef);
    FLNodeElabVector& elabFlows = (*netRefFlowElabMap)[normNetRef];

    // Process all the unelaborated flows for this bucket
    const FLNodeVector& flows = i->second;
    for (FLNodeVectorIter f = flows.begin(); f != flows.end(); ++f) {
      // Find the elaborated flow for this unelaborated flow
      FLNode* flow = *f;
      ReverseMap::iterator pos = reverseMap.find(flow);
      FLN_ASSERT(pos != reverseMap.end(), flow);
      FLNodeElabVector& reverseFlowElabs = pos->second;
      FLNodeElabVectorIter e;
      for (e = reverseFlowElabs.begin(); e != reverseFlowElabs.end(); ++e) {
        FLNodeElab* elabFlow = *e;
        elabFlows.push_back(elabFlow);
      }
    }
  } // for

  return isValidConcat;
} // ElabCallback::createConcatMap

ESExprHndl
ESPopulateExpr::create(FLNodeElab* flow, ESCallbackElab* callbackElab)
{
  // For port connections, we want to walk elaboratedly to the
  // flows fanin.
  mCache.clear();
  ESExprHndl hndl;
  if (translatePortConnection(flow, callbackElab, hndl))
    return hndl;

  // Use the unelaborated traversal to create the flow
  ElabCallback elabCallback(flow, callbackElab, this, mFactory,
                            mNetRefFactory);
  return create(flow->getFLNode(), &elabCallback);
}

ESExprHndl
ESPopulateExpr::createIfCondition(FLNodeElab* flow, ESCallbackElab* callbackElab)
{
  // Use the unelaborated traversal to create the flow
  ElabCallback elabCallback(flow, callbackElab, this, mFactory,
                            mNetRefFactory);
  return createIfCondition(flow->getFLNode(), &elabCallback);
}

ESExprHndl
ESPopulateExpr::createSingleEdge(FLNodeElab* flow, ESCallbackElab* callbackElab)
{
  // Use the unelaborated traversal to create the flow
  ElabCallback elabCallback(flow, callbackElab, this, mFactory,
                            mNetRefFactory);
  return createSingleEdge(flow->getFLNode(), &elabCallback);
}

ESExprHndl ESPopulateExpr::getEnable(ESExprHndl exprHndl)
{
  // Ask the expression for its enable
  bool cond;
  CarbonExpr* expr = exprHndl.getExpr();
  CarbonExpr* enable = expr->getEnable(cond);

  // The expression can either always drive (the enable is NULL), or
  // it conditionally drives and the enable is returned. If it
  // conditionally drives, cond is true if it drives when the enable
  // is true and false when it drives when enable is false.
  CarbonExpr* retExpr;
  if (enable == NULL)
    // It always drives so return a constant 1 enable
    retExpr = mFactory->createConst(1, 1, false); // not signed
  else if (cond)
    // It drives when the expression is true, just return it
    retExpr = enable;
  else
    // It drives when the expression is false, negate it
    retExpr = mFactory->createUnaryOp(CarbonExpr::eUnBitNeg, enable, 1,
                                      false); // enables are unsigned

  ESExprHndl retHndl(retExpr);
  return retHndl;
} // ESExprHndl ESPopulateExpr::getEnable


// The following are private routines used in creation of synthesized
// expressions.

CarbonExpr*
ESPopulateExpr::recursiveCreate(FLNode* flow, ESCallback* callback,
				bool nested)
{
  CreateCacheKey cache_key(flow,callback,nested);
  CarbonExprCache::iterator location = mCache.find(cache_key);
  if (location != mCache.end()) {
    return (*location).second;
  } else {
    CarbonExpr * result = wrappedRecursiveCreate(flow,callback,nested);
    mCache.insert(CarbonExprCache::value_type(cache_key,result));
    return result;
  }
}


CarbonExpr*
ESPopulateExpr::wrappedRecursiveCreate(FLNode* flow, ESCallback* callback,
                                       bool nested)
{
  const NUNetRefHdl& netRef = flow->getDefNetRef();
  UInt32 bitSize = netRef->getNumBits();
  bool isSigned = flow->getDefNet()->isSigned();

  // Check if this is a stopping point. We stop at nodes with no
  // drivers, if it is a sequential node or when the caller tells us
  // to. The sequential node case is a must stop. The NULL usedef or
  // caller stop is a soft stop.
  bool softStop = false;
  bool hardStop = false;
  NUUseDefNode* useDef = flow->getUseDefNode();
  if (useDef == NULL)
    softStop = true;
  else if ((useDef->getType() == eNUCycle) ||
	   (useDef->getStrength() == eStrPull))
    hardStop = true;
  else if (useDef->getType() == eNUModuleInstance)
    softStop = true;
  else if (callback->stop(flow, useDef->isContDriver()))
    softStop = true;
  else if (isCovered(flow))
    hardStop = true;
  if (softStop || hardStop)
    // Create an expression for this flow node as an identifier
    return createIdent(flow, callback, isSigned, hardStop);

  // Remember that we have been here
  addCovered(flow);

  // Create the expression from this node
  NUType type = useDef->getType();
  CarbonExpr* expr = NULL;
  switch (type)
  {
    case eNUInitialBlock:
    case eNUAlwaysBlock:
      // Get the nested named block and recurse
      FLN_ASSERT(flow->hasNestedBlock(), flow);
      expr = recursiveCreate(flow->getSingleNested(), callback, true);
      break;

    case eNUBlock:
    {
      // If this named block flow node has nesting then go use
      // that.
      FLNodeVector flows;
      for (Iter<FLNode*> l = flow->loopNested(); !l.atEnd(); ++l) {
        FLNode* nested = *l;
        flows.push_back(nested);
      }

      // Create the multidriver expression
      expr = multiDriver(flows, netRef, isSigned, callback, true);
      break;
    }

    case eNUContAssign:
    case eNUBlockingAssign:
    case eNUNonBlockingAssign:
    {
      // We don't support LHS concats
      NUAssign* assign = dynamic_cast<NUAssign*>(useDef);
      NULvalue* lvalue = assign->getLvalue();
      if (lvalue->getType() == eNUConcatLvalue)
        expr = createIdent(flow, callback, isSigned, true);
      else
        expr = translateExpr(flow, assign->getRvalue());
      break;
    }

    case eNUIf:
    {
      NUIf* ifStmt = dynamic_cast<NUIf*>(useDef);
      expr = translateIf(flow, ifStmt, callback);
      break;
    }

    case eNUCase:
    {
      NUCase* caseStmt = dynamic_cast<NUCase*>(useDef);
      expr = translateCase(flow, caseStmt, callback);
      break;
    }

    case eNUBlockingEnabledDriver:
    case eNUContEnabledDriver:
    {
      NUEnabledDriver* driver = dynamic_cast<NUEnabledDriver*>(useDef);
      expr = translateEnabledDriver(flow, driver);
      break;
    }

    case eNUTask:
    case eNUTaskEnable:
    case eNUReadmemX:
    case eNUTFArgConnectionBid:
    case eNUTFArgConnectionInput:
    case eNUTFArgConnectionOutput:
    case eNUTriRegInit:
    case eNUFor:
    case eNUInputSysTask:
    case eNUOutputSysTask:
    case eNUFOpenSysTask:
    case eNUControlSysTask:
    case eNUFCloseSysTask:
    case eNUFFlushSysTask:
    case eNUSysRandom:
      // Not yet implemented
      expr = NULL;
      break;

    case eNUModuleInstance:
    case eNUCycle:
    case eNUPortConnectionOutput:
    case eNUPortConnectionBid:
    case eNUPortConnectionInput:
    case eNUCModelCall:
      // Terminating points
      expr = createIdent(flow, callback, isSigned, true);
      break;

    case eNUAssign:
    case eNUStructuredProc:
    case eNUTFElab:
    case eNUTF:
      // We should find derived versions of these types of use def nodes
      FLN_ASSERT("Unexpected construct" == NULL, flow);
      break;

    case eNUModule:
    case eNULvalue:
    case eNUVarselLvalue:
    case eNUMemselLvalue:
    case eNUIdentLvalue:
    case eNUConcatLvalue:
    default:
      // Unsupported constructs because we should never encounter them
      FLN_ASSERT("Unsupported construct" == NULL, flow);
      break;
  } // switch
  
  // If we did not support the underlying expression, make this an ident
  if ((expr == NULL) && !nested)
    expr = createIdent(flow, callback, isSigned, true);

  // Make sure the expression is sized correctly
  if (expr != NULL) {
    // We only treat this as signed if it isn't an integer unsigned
    // sub range.
    bool lclIsSigned = isSigned & !flow->getDefNet()->isUnsignedSubrange();
    expr = resize(expr, bitSize, lclIsSigned);
  }    

  // Remove this flow node from the seen list
  removeCovered(flow);
  return expr;
} // ESPopulateExpr::recursiveCreate

bool
ESPopulateExpr::isConcat(const FLNodeVector& flows, const NUNetRefHdl& reqNetRef,
                         NetRefFunctor& functor, ESFlowBuckets* flowBuckets,
                         NUNetRefHdl* retNetRef)
{
  // Put all the flow nodes into buckets. Check if there are any
  // non-contigous range flow nodes. We don't handle them yet.
  bool isValidConcat = true;
  ESFlowBuckets lclFlowBuckets;
  for (FLNodeVectorIter i = flows.begin(); i != flows.end(); ++i) {
    FLNode* flow = *i;
    const NUNetRefHdl& netRef = functor.getNetRef(flow, mNetRefFactory);

    // Use getRange() to determine if the net ref is contiguous
    ConstantRange range;
    if (!netRef->getRange(range)) {
      // It is not contiguous, we can't handle this
      isValidConcat = false;
    }

    // Add this to the bucket even if it is not a valid concat. We
    // want to create the accumulated net ref below.
    FLNodeVector& bucketFlows = lclFlowBuckets[netRef];
    bucketFlows.push_back(flow);
  }

  // Check if all the flows don't intersect. We can't make a concat if
  // there are intersections.
  NUNetRefHdl accumNetRef = mNetRefFactory->createEmptyNetRef();
  ESFlowBucketsIter p;
  for (p = lclFlowBuckets.begin(); p != lclFlowBuckets.end(); ++p) {
    const NUNetRefHdl& netRef = p->first;
    if (accumNetRef->overlaps(*netRef)) {
      // We have overlap because these bits were removed before
      isValidConcat = false;
    }

    // We haven't seen these bits before, add them
    accumNetRef = mNetRefFactory->merge(accumNetRef, netRef);
  }

  // If the requested net ref doesn't have any holes, we can make a
  // concat out of the it. Otherwise, it is not clear how to put it
  // together. Use getRange() to determine if the net ref is
  // contiguous.
  ConstantRange range;
  if (isValidConcat && !reqNetRef->getRange(range)) {
    isValidConcat = false;
  }

  // Fill any holes with an empty set of flows. This can be filled in
  // by the caller with constants if they wish. We know we can create
  // a concat because the requested net ref doesn't have holes.
  if (isValidConcat && !accumNetRef->getRange(range)) {
    // Convert the requested net ref into an image of the accumulated
    // net. We can do this because all vector nets are now zero based.
    NUNet* net = accumNetRef->getNet();
    NUNetRefHdl normNetRef = mNetRefFactory->createNetRefImage(net, reqNetRef);
    
    // Figure out if we have any holes and then loop through them to
    // create additional entries in the net ref to flows map. We will
    // leave the flows empty. That will tell the caller that those
    // bits are undriven.
    NUNetRefHdl holesNetRef = mNetRefFactory->subtract(normNetRef, accumNetRef);
    for (NUNetRefRangeLoop l = holesNetRef->loopRanges(mNetRefFactory);
         !l.atEnd(); ++l) {
      const ConstantRange& range = *l;
      const NUNetRefHdl& holeRef = mNetRefFactory->sliceNetRef(normNetRef, range);
      lclFlowBuckets[holeRef];
      accumNetRef = mNetRefFactory->merge(accumNetRef, holeRef);
    }
  }

  // If we found any issues, create a single bucket. Otherwise, copy
  // the data over.
  if (!isValidConcat) {
    FLNodeVector& bucketFlows = (*flowBuckets)[accumNetRef];
    bucketFlows = flows;
  } else {
    for (p = lclFlowBuckets.begin(); p != lclFlowBuckets.end(); ++p) {
      const NUNetRefHdl& netRef = p->first;
      const FLNodeVector& bucketFlows = p->second;
      (*flowBuckets)[netRef] = bucketFlows;
    }
  }

  // Return the net ref for the driver we are going to create
  *retNetRef = accumNetRef;
  return isValidConcat;
} // ESPopulateExpr::isConcat

bool
ESPopulateExpr::createMultiExpr(const ESFlowBuckets& flowBuckets,
                                ESCallback* callback, ESSortedExprMap* exprMap,
                                bool nested, NUNetRefHdl* resNetRef)
{
  bool failed = false;
  ESFlowBucketsIter i;
  *resNetRef = mNetRefFactory->createEmptyNetRef();
  for (i = flowBuckets.begin(); (i != flowBuckets.end()) && !failed; ++i) {
    // If this bucket is a single flow node, we can continue
    // recursively. If there are no flow nodes it is undriven so let's
    // assume a constant z.
    const NUNetRefHdl& netRef = i->first;
    const FLNodeVector& flows = i->second;
    CarbonExpr* expr = NULL;
    if (flows.size() == 0) {
      // No drivers
      NUNet* net = netRef->getNet();
      expr = mFactory->createConstZ(netRef->getNumBits(), net->isSigned());

    } else if (flows.size() == 1) {
      // Single driver
      FLNode* flow = *(flows.begin());
      expr = recursiveCreate(flow, callback, nested);
    }

    // If it fails, or we have more than one flow then create an
    // ident here.
    if (expr == NULL) {
      NUNetRefHdl subNetRef;
      expr = callback->createIdentExpr(flows, netRef->getNumBits(),
                                       netRef->getNet()->isSigned(), true,
                                       &subNetRef);
      if (expr != NULL) {
        *resNetRef = mNetRefFactory->merge(*resNetRef, subNetRef);
      }
    } else {
      *resNetRef = mNetRefFactory->merge(*resNetRef, netRef);
    }

    // If creating an ident fails, then give up.
    if (expr == NULL)
      failed = true;
    else
      exprMap->insert(ESSortedExprMap::value_type(netRef, expr));
  }
  return !failed;
} // ESPopulateExpr::createMultiExpr

CarbonExpr*
ESPopulateExpr::multiDriver(FLNodeVector& flows, const NUNetRefHdl& reqNetRef,
                            bool isSigned, ESCallback* callback, 
                            bool nested, NUNetRefHdl* resNetRef)
{
  // Make sure we have a set of flow nodes we can handle
  ESFlowBuckets flowBuckets;
  NetRefFunctor functor;
  NUNetRefHdl netRef;
  bool isValidConcat = isConcat(flows, reqNetRef, functor, &flowBuckets, &netRef);

  // Create expressions for the various flows in the buckets no matter
  // what. If it is not a valid concat, we will have just one
  // bucket. If this routine fails for any reason, we give up.
  ESSortedExprMap exprMap;
  NUNetRefHdl exprNetRef;
  if (!createMultiExpr(flowBuckets, callback, &exprMap, nested, &exprNetRef))
    return NULL;

  // If we get here, we have expression(s) we can return to our
  // caller.
  if (resNetRef != NULL) {
    *resNetRef = exprNetRef;
  }
  UInt32 bitSize = reqNetRef->getNumBits();
  return createMultiDriver(isValidConcat, exprNetRef, bitSize, isSigned, exprMap);
} // ESPopulateExpr::multiDriver

CarbonExpr*
ESPopulateExpr::createMultiDriver(bool isValidConcat,
                                  const NUNetRefHdl& resultNetRef,
                                  UInt32 bitSize, bool isSigned,
                                  const ESSortedExprMap& exprMap)
{
  // Only create a concat if it is valid and we have more than one
  // expression
  if (isValidConcat && (exprMap.size() > 1)) {
    // It must be a set of expressions that form a contigous range,
    // get it
    ConstantRange range;
    bool status = resultNetRef->getRange(range);
    NU_ASSERT(status == true, resultNetRef->getNet());

    // Return the concat to our caller
    return createMultiDriverConcat(exprMap, range, bitSize, isSigned);

  } else {
    // It must be a single expression either because it is not a valid
    // concat or there was only one expression in the valid concat.
    CarbonExpr* expr = NULL;
    for (ESSortedExprMapIter i = exprMap.begin(); i != exprMap.end(); ++i)
    {
      CE_ASSERT(expr == NULL, expr);
      expr = i->second;
    }
    NU_ASSERT(expr != NULL, resultNetRef->getNet());
    return expr;
  }
} // ESPopulateExpr::createMultiDriver

CarbonExpr*
ESPopulateExpr::createNetRefMultiDriverConcat(const NUNetRefHdl& resultNetRef,
                                              const NUNetRefHdl& accumNetRef,
                                              UInt32 bitSize, bool isSigned,
                                              ESSortedExprMap* exprMap,
                                              Carbon4StateVal filler)
{
  // Find the holes in the resultNetRef that are not covered by the
  // accumNetRef.
  NUNetRefHdl holesNetRef = mNetRefFactory->subtract(resultNetRef, accumNetRef);
  for (NUNetRefRangeLoop l = holesNetRef->loopRanges(mNetRefFactory);
       !l.atEnd(); ++l) {
    const ConstantRange& range = *l;
    const NUNetRefHdl& subNetRef = 
      mNetRefFactory->sliceNetRef(resultNetRef, range);
    CarbonConst* constExpr = NULL;
    switch (filler)
    {
    case eValue0:
      constExpr = mFactory->createConst(0, range.getLength(), false);
      break;
    case eValueX: 
      constExpr = mFactory->createConstX(range.getLength(), false);
      break;
    case eValueZ:
      constExpr = mFactory->createConstZ(range.getLength(), false);
      break;
    case eValue1:
      NU_ASSERT(filler != eValue1, resultNetRef->getNet());
      break;
    }
    
    NU_ASSERT(constExpr, resultNetRef->getNet());
    
    (*exprMap)[subNetRef] = constExpr;
  }
  
  return createMultiDriver(true, resultNetRef, bitSize, isSigned, *exprMap);
} // ESPopulateExpr::createNetRefMultiDriverConcat

CarbonExpr*
ESPopulateExpr::createMultiDriverConcat(const ESSortedExprMap& exprMap,
                                        const ConstantRange& range,
                                        UInt32 bitSize, bool isSigned)
{
  // If the desired size is bigger, then fill in the concat with 0-bits
  // in the MSBs, which are the low index numbers
  CarbonExprVector sortedExprs;
  SInt32 exprSize = (SInt32)range.getLength();
  SInt32 sbSize = (SInt32) bitSize;
  for (SInt32 i = 0; i < (sbSize - exprSize); ++i)
    sortedExprs.push_back(mFactory->createConst(0, 1, false)); // not signed

  // Go through the map in order and create the sorted vector of
  // expressions. The create concat op requires a vector.
  for (ESSortedExprMapIter i = exprMap.begin(); i != exprMap.end(); ++i)
  {
    CarbonExpr* expr = i->second;
    sortedExprs.push_back(expr);
  }

  // Create the concat operation
  //
  // size the concat operation with the natural size of all the
  // expressions in the concatenation, unless the user wants it to
  // be bigger, in which case take into account the padding constants
  // added above.  This allows test/constprop/crc.v and bitflow.v
  // to avoid a mismatch between the concatenation size and what's
  // computed based on its elements.  The consequence of mismatching
  // those is that in expression resynthesis, Fold must rewrite the
  // concat, which in the case of crc.v is a fatal out-of-memory
  // problem, because the concat-rewriting blows out a complex graph
  // into a tree.
  CarbonExpr* retExpr;
  SInt32 maxSize = std::max(exprSize, sbSize);
  retExpr = mFactory->createConcatOp(&sortedExprs, 1, maxSize, isSigned);
  return retExpr;
} // CarbonExpr:: ESPopulateExpr::createMultiDriverConcat


CarbonExpr*
ESPopulateExpr::translateExpr(FLNode* flow, NUExpr* expr)
{
  // Get the input nets and flows
  InputSet* inputSet = new InputSet(mNetRefFactory);
  UtSet<FLNode*> covered;
  for (Iter<FLNode*> l = flow->loopFanin(); !l.atEnd(); ++l)
  {
    FLNode* fanin = *l;
    if (covered.find(fanin) == covered.end())
    {
      // Add this to the map
      NUNetRefHdl netRefHdl = fanin->getDefNetRef();
      inputSet->insert(netRefHdl, fanin);

      // Remember that we have seen this flow node
      covered.insert(fanin);
    }
  }

  // Create the expression by recursively visiting the nucleus objects
  CarbonExpr* retExpr = mTranslateExpr->translateExpr(expr, inputSet);
  delete inputSet;
  return retExpr;
}


CarbonExpr*
ESPopulateExpr::translateEdgeExpr(FLNode* flow, NUExpr* expr)
{
  // Get the input nets and flows
  InputSet* inputSet = new InputSet(mNetRefFactory);
  UtSet<FLNode*> covered;
  for (Iter<FLNode*> l = flow->loopEdgeFanin(); !l.atEnd(); ++l)
  {
    FLNode* fanin = *l;
    if (covered.find(fanin) == covered.end())
    {
      // Add this to the map
      NUNetRefHdl netRefHdl = fanin->getDefNetRef();
      inputSet->insert(netRefHdl, fanin);

      // Remember that we have seen this flow node
      covered.insert(fanin);
    }
  }

  // Create the expression by recursively visiting the nucleus objects
  CarbonExpr* retExpr = mTranslateExpr->translateExpr(expr, inputSet);
  delete inputSet;
  return retExpr;
}


CarbonExpr*
ESPopulateExpr::translateIf(FLNode* flow, NUIf* ifStmt, ESCallback* callback)
{
  if (!flow->hasNestedBlock())
    return NULL; // no point looking any further

  // Find the flow nodes for the then and else statements, and
  // accumulate which bits are being written to in each clause.
  FLNodeVector thenFlows;
  FLNodeVector elseFlows;
  NUNetRefHdl thenNetRef = mNetRefFactory->createEmptyNetRef();
  NUNetRefHdl elseNetRef = mNetRefFactory->createEmptyNetRef();

  // Classify the flow nodes
  FLIfClassifier<FLNode> ifClassifier(flow);

  // Process the 'then' statements
  for (FLIfClassifier<FLNode>::FlowLoop l = ifClassifier.loopThen(flow);
       !l.atEnd(); ++l)
  {
    FLNode* fanin = *l;
    thenFlows.push_back(fanin);
    NUNetRefHdl netRef = fanin->getDefNetRef();
    thenNetRef = mNetRefFactory->merge(thenNetRef, netRef);
  }

  // Process the 'else' statements
  for (FLIfClassifier<FLNode>::FlowLoop l = ifClassifier.loopElse(flow);
       !l.atEnd(); ++l)
  {
    FLNode* fanin = *l;
    elseFlows.push_back(fanin);
    NUNetRefHdl netRef = fanin->getDefNetRef();
    elseNetRef = mNetRefFactory->merge(elseNetRef, netRef);
  }

  // Make sure we found something
  FLN_ASSERT(!thenFlows.empty() || !elseFlows.empty(), flow);

  // Make sure we have complete net refs for both the then and else
  // clauses.  If the net ref is not assigned in both clauses, then
  // this is really a latch, which we don't handle.
  NUNetRefHdl fullNetRef = flow->getDefNetRef();
  if ((fullNetRef != thenNetRef) || (fullNetRef != elseNetRef))
    return NULL;

  // Create the conditional expression
  CarbonExpr* condExpr = translateExpr(flow, ifStmt->getCond());
  if (condExpr == NULL)
    return NULL;

  // Create the then expression
  const NUNetRefHdl& netRef = flow->getDefNetRef();
  UInt32 bitSize = netRef->getNumBits();
  bool isSigned = flow->getDefNet()->isSigned();
  CarbonExpr* thenExpr = multiDriver(thenFlows, netRef, isSigned, callback);
  if (thenExpr == NULL)
  {
    condExpr->decRefCnt();
    return NULL;
  }

  // Create the else expression
  CarbonExpr* elseExpr = multiDriver(elseFlows, netRef, isSigned, callback);
  if (elseExpr == NULL)
  {
    condExpr->decRefCnt();
    thenExpr->decRefCnt();
    return NULL;
  }

  // We have everything we need. Crete the conditional
  CarbonExpr* retExpr;
  retExpr = mFactory->createTernaryOp(CarbonExpr::eTeCond, condExpr, thenExpr,
				      elseExpr, bitSize, isSigned, false);
  return retExpr;
}

CarbonExpr*
ESPopulateExpr::translateCase(FLNode* flow, NUCase* caseStmt,
                              ESCallback* callback)
{
  // If this is not a fully specified case statement, give up
  if (!caseStmt->isFullySpecified() && !caseStmt->hasDefault())
    return NULL;

  if (!flow->hasNestedBlock())
    // Unexpected, give up for now
    return NULL;

  // Classify all the flow nodes for the various case items.
  FLNodeCaseClassifier* caseClassifier = classifyCaseFlow(flow);

  // Process the statements for each case item.  We need to make sure
  // that all bits are written in every case item so we can build up
  // the right expression. We also need to match up every case item
  // condition with the appropriate assignment statement.
  typedef UtVector<FLNodeVector> CaseItemFlows;
  typedef UtVector<NUNetRefHdl> CaseItemNetRefs;
  CaseItemFlows caseItemFlows;
  CaseItemNetRefs caseItemNetRefs;
  NUCaseItemVector caseItems;
  int index = 0;
  for (NUCase::ItemLoop l = caseStmt->loopItems(); !l.atEnd(); ++l) {
    // Allocate space for our data. Vectors don't resize themselves
    caseItemFlows.resize(index+1);
    FLNodeVector& flows = caseItemFlows[index];
    caseItemNetRefs.resize(index+1);
    NUNetRefHdl itemRef = mNetRefFactory->createEmptyNetRef();

    // Store the case item in a vector so that we can do a reverse
    // iteration below. We have to create the expression from the
    // default/last case back.
    NUCaseItem* caseItem = *l;
    caseItems.push_back(caseItem);

    // Get the flow nodes for this case item
    for (FLNodeCaseClassifier::FlowLoop s = caseClassifier->loopCaseItem(caseItem, flow);
         !s.atEnd();
         ++s)
    {
      FLNode* stmtFlow = *s;
      flows.push_back(stmtFlow);

      // Update our covered net ref
      NUNetRefHdl netRef = stmtFlow->getDefNetRef();
      itemRef = mNetRefFactory->merge(itemRef, netRef);
    }

    // Store the accumulated net ref
    caseItemNetRefs[index] = itemRef;
    ++index;
  } // for
  int numCaseItems = index;

  // Not sure if we can have an empty set of case items but we assume
  // we don't below so check for it.
  if (numCaseItems == 0)
    return NULL;

  // Make sure we have complete net refs for all case items
  NUNetRefHdl fullNetRef = flow->getDefNetRef();
  bool validNetRef = true;
  for (int i = 0; (i < numCaseItems) && validNetRef; ++i) {
    validNetRef = (fullNetRef == caseItemNetRefs[i]);
  }
  if (!validNetRef)
    return NULL;
  caseItemNetRefs.clear();

  // We have a valid case, create the select expression
  CarbonExpr* selExpr = translateExpr(flow, caseStmt->getSelect());
  if (selExpr == NULL)
    return NULL;

  // Create expressions for all the case items flows
  CarbonExprVector itemExprs;
  itemExprs.resize(numCaseItems);
  const NUNetRefHdl& netRef = flow->getDefNetRef();
  UInt32 bitSize = netRef->getNumBits();
  bool isSigned = flow->getDefNet()->isSigned();
  bool validExprs = true;
  for (int i = 0; (i < numCaseItems) && validExprs; ++i) {
    // Translate the expression
    FLNodeVector& flows = caseItemFlows[i];
    CarbonExpr* itemExpr = multiDriver(flows, netRef, isSigned, callback);
    if (itemExpr != NULL) {
      itemExprs[i] = itemExpr;
    } else {
      validExprs = false;
    }
  }
  if (!validExprs)
    return NULL;

  // Create the default or last case item as the trailing else for the
  // nested conditionals. We assume that if there is a default it is
  // the last one.
  NUCaseItemVectorRevIter i = caseItems.rbegin();
  index = numCaseItems - 1;
  CarbonExpr* retExpr = itemExprs[index];
  if (retExpr == NULL)
    return NULL;

  // Go through the rest of the case items in reverse creating the
  // conditionals
  for (++i; (i != caseItems.rend()) && validExprs; ++i) {
    // Create the conditional from the case item conditions
    NUCaseItem* caseItem = *i;
    NUExpr* nucCondExpr = caseItem->buildConditional(caseStmt->getSelect());
    CarbonExpr* condExpr = translateExpr(flow, nucCondExpr);
    validExprs = (condExpr != NULL);
    delete nucCondExpr;

    // Create the conditional statement
    if (validExprs)
    {
      CarbonExpr* thenExpr = itemExprs[--index];
      retExpr = mFactory->createTernaryOp(CarbonExpr::eTeCond, condExpr,
                                          thenExpr, retExpr, bitSize, isSigned,
                                          false);
      validExprs = (retExpr != NULL);
    }
    else
      retExpr = NULL;
  }

  return retExpr;
}

CarbonExpr*
ESPopulateExpr::translateEnabledDriver(FLNode* flow, NUEnabledDriver* driver)
{
  // Translate the enable and input expressions
  CarbonExpr* enExpr = translateExpr(flow, driver->getEnable());
  if (enExpr == NULL)
    return NULL;
  CarbonExpr* inExpr = translateExpr(flow, driver->getDriver());
  if (inExpr == NULL)
  {
    enExpr->decRefCnt();
    return NULL;
  }

  // Create the conditional operator
  CarbonExpr* retExpr;
  UInt32 bitSize = driver->getLvalue()->getBitSize();
  bool isSigned = flow->getDefNet()->isSigned();
  CarbonExpr* constZ = mFactory->createConstZ(bitSize, isSigned);
  retExpr = mFactory->createTernaryOp(CarbonExpr::eTeCond, enExpr, inExpr,
				      constZ, bitSize,
                                      isSigned, false);
  return retExpr;
}

CarbonExpr*
ESCallback::createIdentExpr(const FLNodeVector& flows, UInt32 bitSize,
                            bool isSigned, bool /* hardStop */,
                            NUNetRefHdl* resNetRef)
{
  INFO_ASSERT(not flows.empty(), "Cannot create ident expression with empty data");

  // Figure out the bits defined by this ident. It is cheaper and
  // easier to do it here and copy it around than compute this on the
  // fly because then we would need to store the factory.
  NUNetRefFactory* factory = mPopulateExpr->getNetRefFactory();
  NUNetRefHdl allNetRef = factory->createEmptyNetRef();
  for (FLNodeVectorCLoop l(flows); !l.atEnd(); ++l) {
    FLNode* flow = *l;
    NUNetRefHdl netRef = flow->getDefNetRef();
    allNetRef = factory->merge(allNetRef, netRef);
  }
  *resNetRef = allNetRef;

  ESFlowIdent* newIdent = new ESFlowIdent(flows, allNetRef, bitSize, isSigned);
  bool added = false;
  CarbonExpr* retExpr = mPopulateExpr->mFactory->createIdent(newIdent, added);
  if (!added)
    delete newIdent;
  return retExpr;
}

ESExprHndl ESCallbackElab::createIdentExpr(FLNodeElabVector* flowElabs,
					   UInt32 bitSize, bool isSigned, bool)
{
  INFO_ASSERT(flowElabs && ! flowElabs->empty(),
              "Cannot create ident expression with empty data");

  // Figure out the bits defined by this ident. It is cheaper and
  // easier to do it here and copy it around than compute this on the
  // fly because then we would need to store the factory.
  NUNetRefFactory* factory = mPopulateExpr->getNetRefFactory();
  NUNetRefHdl allNetRef = factory->createEmptyNetRef();
  for (FLNodeElabVectorLoop l(*flowElabs); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    NUNetRefHdl netRef = flowElab->getDefNetRef(factory);
    allNetRef = factory->merge(allNetRef, netRef);
  }

  ESFlowElabIdent* newIdent = new ESFlowElabIdent(flowElabs, allNetRef,
                                                  bitSize, isSigned);
  bool added = false;
  CarbonExpr* retExpr;
  retExpr = mPopulateExpr->mFactory->createIdent(newIdent, added);
  if (!added)
    delete newIdent;
  return ESExprHndl(retExpr);
}

ESExprHndl ESCallbackElab::create(FLNodeElab* flowElab)
{
  // By default call recursively
  return mPopulateExpr->create(flowElab, this);
}

CarbonExpr*
ESPopulateExpr::createIdent(FLNode* flow, ESCallback* callback,
                            bool isSigned, bool hardStop)
{
  UInt32 bitSize = flow->getDefNetRef()->getNumBits();
  FLNodeVector flows;
  flows.push_back(flow);
  NUNetRefHdl netRef;
  CarbonExpr* retExpr = callback->createIdentExpr(flows, bitSize, isSigned,
                                                  hardStop, &netRef);
  return retExpr;
}

CarbonExpr*
ESPopulateExpr::createNetExpr(NUNet* net, const ConstantRange& range,
                              UInt32 bitSize, bool extIsSigned,
			      InputSet* inputSet, ESCallback* callback)
{
  // We only treat this as signed if it isn't an integer unsigned sub
  // range
  bool isSigned = extIsSigned && !net->isUnsignedSubrange();

  // Count the number of flow nodes to see if it is a multi driver or
  // not. While we are there gather the flow and compute the reference
  // for this ident.
  CarbonExpr* retExpr = NULL;
  FLNode* flow = NULL;
  NUNetRefHdl netRef = mNetRefFactory->createEmptyNetRef();
  FLNodeVector flows;
  for (InputSet::CondNetLoop i = inputSet->loop(net); !i.atEnd(); ++i)
  {
    flow = (*i).second;
    flows.push_back(flow);
    netRef = mNetRefFactory->merge(netRef, flow->getDefNetRef());
  }

  // Check for now flows. This is unexpected but seems to happen due
  // to bugs in buffer insertion. I will file a bug for this.
  if (flows.empty())
    return NULL;

  // For a single driver we can keep going, for a multi-driver we need
  // to do more work. The result is an expression and the net ref that
  // covers that expression.
  SInt32 driverCount = flows.size();
  if (driverCount == 1)
  {
    // Create an expression for the entire netref
    NU_ASSERT(flow != NULL, net);
    retExpr = recursiveCreate(flow, callback);
  }
  else
  {
    // Either create a concatenation of all the flow-nodes in the
    // appropriate order or create a multi-flow ident.
    NUNetRefHdl resNetRef;
    retExpr = multiDriver(flows, netRef, isSigned, callback, false, &resNetRef);
    netRef = resNetRef;
  }

  // Make sure the net ref we create covers the entire requested
  // length. If not we give up. This can occur if part of the net is
  // not actively driven. We can do better here in the future.
  if ((net->isVectorNet()) && (retExpr != NULL))
  {
    NUNetRefHdl reqRef = mNetRefFactory->createVectorNetRef(net, range);
    if (!netRef->covers(*reqRef))
      retExpr = NULL;
  }

  // If we were succesful, create the sub range requested by our caller
  if (retExpr != NULL)
  {
    // Create an expression for the sub range requested. For vectors
    // we use a helper function, for scalars the range better be
    // [0:0].
    NUVectorNet* vecNet = dynamic_cast<NUVectorNet*>(net);
    if (vecNet != NULL) {
      retExpr = createVecNetRange(retExpr, flow, netRef, vecNet, range,
				  bitSize, isSigned);
    } else if ((range.getMsb() != 0) && (range.getLsb() != 0)) {
      // out of range select for a scalar? Why are we getting this?
      // Well, return 0 for now.
      retExpr = mFactory->createConst(0, range.getLength(), net->isSigned());
    }

    // Make sure the net expression is the right size.
    if (retExpr != NULL) {
      retExpr = resize(retExpr, bitSize, isSigned);
    }
  } // if

  return retExpr;
} // ESPopulateExpr::createNetExpr

CarbonExpr*
ESPopulateExpr::createVecNetRange(CarbonExpr* expr, FLNode* flow,
				  const NUNetRefHdl& netRef,
				  NUVectorNet* vecNet,
                                  const ConstantRange& range,
				  UInt32 bitSize, bool isSigned)
{
  // Don't care about lvals if we are stopping
  if (expr->castIdent() == NULL)
  {
    // We also don't support 2D array lvalues
    if (vecNet->is2DAnything())
      return NULL;
    
    // Check if there are any LHS concats on this flow node. If so, we
    // can't support it because they may reverse bits.
    if (!legalLvalues(flow))
      return NULL;
  }

  // Check if the net range requested matches the range defined. If
  // so, this flow node represents the right range.
  NUNetRefHdl reqRef = mNetRefFactory->createVectorNetRef(vecNet, range);
  if (reqRef == netRef)
    return expr;

  // Loop the ranges and create sub expressions for every
  // sub-range. We will concat these later.
  CarbonExprVector exprs;
  SInt32 seen_bits = 0;
  for (NUNetRefRangeLoop l = netRef->loopRanges(mNetRefFactory);
       !l.atEnd(); ++l) {
    // Get the overlap between the two ranges. That is the range for
    // this part select. If there is no overlap, we don't need this in
    // the concat
    const ConstantRange& vecRange = *l;
    if (vecRange.overlaps(range)) {
      ConstantRange subRange = vecRange.overlap(range);

      // The resulting range we use depends on whether the input
      // expression is an identifier or expression. For expressions we
      // normalize the range, for identifiers there are two cases: If the
      // identifier defs the entire net, then we use the range as
      // specified by the net. If the identifier defs a subset of the
      // range then we use normalized ranges.
      ConstantRange selRange = subRange;
      if (!expr->isIdent(expr->getType()) || !netRef->all()) {
        // Treat the input as an expression, so normalize to the range
        selRange = ConstantRange(labs(subRange.getMsb() - vecRange.getLsb()) + 
                                 seen_bits,
                                 labs(subRange.getLsb() - vecRange.getLsb()) + 
                                 seen_bits);
      }

      // Now we can create the part/bi select since range has
      // been normalized if necessary.
      CarbonExpr* subExpr;
      if (selRange.getMsb() == selRange.getLsb()) {
        CarbonExpr* indexExpr = mFactory->createConst(selRange.getMsb(), 32, 
                                                      false); // not signed
        subExpr = mFactory->createBinaryOp(CarbonExpr::eBiBitSel, expr,
                                           indexExpr, bitSize,
                                           isSigned, false);
      } else {
        subExpr = mFactory->createPartsel(expr, selRange, bitSize, isSigned);
      }

      // Add this expression to the vector
      exprs.push_back(subExpr);
    } // if

    seen_bits += vecRange.getLength();
  } // for

  // For a single entry, just return that. Otherwise create a concat op
  CarbonExpr* retExpr = NULL;
  NU_ASSERT(!exprs.empty(), vecNet);
  if (exprs.size() == 1) {
    retExpr = exprs.back();
  } else {
    retExpr = mFactory->createConcatOp(&exprs, 1, bitSize, isSigned);
  }
  return retExpr;
}

bool ESPopulateExpr::legalLvalues(FLNode* flow)
{
  // Make sure we don't have a flow without a use def (PI?). If we do,
  // it can't have illegal lvalues.
  NUUseDefNode* useDef = flow->getUseDefNode();
  if (useDef == NULL)
    return true;

  // Do a recursive test based on the usedef type
  NUType type = useDef->getType();
  bool legal = true;
  switch (type)
  {
    case eNUAlwaysBlock:
    case eNUInitialBlock:
      // Get the nested block and recurse
      FLN_ASSERT(flow->hasNestedBlock(), flow);
      legal = legalLvalues(flow->getSingleNested());
      break;

    case eNUIf:
    case eNUBlock:
    case eNUCase:
      // Recurse into nesting
      for (Iter<FLNode*> l = flow->loopNested(); !l.atEnd() && legal; ++l)
        legal = legalLvalues(*l);
      break;

    case eNUContAssign:
    case eNUBlockingAssign:
    case eNUNonBlockingAssign:
    {
      // For assignments we can test the lvalue
      NUAssign* assign = dynamic_cast<NUAssign*>(useDef);
      NULvalue* lvalue = assign->getLvalue();
      NUType lvalueType = lvalue->getType();
      legal = (lvalueType != eNUConcatLvalue);
      break;
    }

    case eNUContEnabledDriver:
    case eNUBlockingEnabledDriver:
    {
      NUEnabledDriver* driver = dynamic_cast<NUEnabledDriver*>(useDef);
      NULvalue* lvalue = driver->getLvalue();
      NUType lvalueType = lvalue->getType();
      legal = (lvalueType != eNUConcatLvalue);
      break;
    }

    case eNUModuleInstance:
      // Can't have a concat lvalue in a sub-module. The RHS might be
      // a concat, but the LHS is always a whole ordered object
      legal = true;
      break;

  default:
    // Need to add more support for other constructs such as module
    // instances, cycles, etc.
    legal = false;
    break;
  } // switch

  return legal;
} // bool ESPopulateExpr::legalLvalues


void ESPopulateExpr::pushCovered(bool newCovered)
{
  if (newCovered)
  {
    FLNodeSet* covered = new FLNodeSet;
    mSeenFlows->push_back(covered);
  }
  else
    INFO_ASSERT(!mSeenFlows->empty(),
                "Problem in call stack (push) with recursive CarbonExpr creation");
}

void ESPopulateExpr::popCovered(bool newCovered)
{
  if (newCovered)
  {
    INFO_ASSERT(!mSeenFlows->empty(),
                "Problem in call stack (pop) with recursive CarbonExpr creation");
    FLNodeSet* covered = mSeenFlows->back();
    mSeenFlows->pop_back();
    INFO_ASSERT(covered->empty(),
                "Problem in call stack (pop) with recursive CarbonExpr creation");
    delete covered;
  }
}

bool ESPopulateExpr::isCovered(FLNode* flow) const
{
  FLN_ASSERT(!mSeenFlows->empty(), flow);
  FLNodeSet* covered = mSeenFlows->back();
  return (covered->find(flow) != covered->end());
}

void ESPopulateExpr::addCovered(FLNode* flow)
{
  FLN_ASSERT(!mSeenFlows->empty(), flow);
  FLNodeSet* covered = mSeenFlows->back();
  covered->insert(flow);
}

void ESPopulateExpr::removeCovered(FLNode* flow)
{
  FLN_ASSERT(!mSeenFlows->empty(), flow);
  FLNodeSet* covered = mSeenFlows->back();
  FLN_ASSERT(covered->find(flow) != covered->end(), flow);
  covered->erase(flow);
}

bool
ESPopulateExpr::translatePortConnection(FLNodeElab* flow,
					ESCallbackElab* callbackElab,
					ESExprHndl& hndl)
{
  // Check for cycles set
  FLNodeElabSet covered;

  // We traverse through port connections going in and out of modules
  // before deciding to translate an unelaborated flow node.
  FLNodeElab* curFlow = flow;
  FLNodeElabVector flows;
  NUUseDefNode* useDef = flow->getUseDefNode();
  bool validExpression = false;
  int numDrivers = 0;
  bool cycleFound = false;
  while ((numDrivers < 2) && (curFlow != NULL) && !validExpression &&
         (useDef != NULL) &&
         (!(cycleFound = (covered.find(curFlow) != covered.end()))) &&
	 ((useDef->getType() == eNUPortConnectionOutput) ||
	  (useDef->getType() == eNUPortConnectionBid) ||
	  (useDef->getType() == eNUPortConnectionInput))) {
    // For input port connections we use the actual as a RHS to the
    // flow node.
    covered.insert(curFlow);
    if (useDef->getType() == eNUPortConnectionInput) {
      // May be a constant port, check it out
      NUPortConnectionInput* port;
      port = dynamic_cast<NUPortConnectionInput*>(useDef);
      NUExpr* expr = port->getActual();
      if (expr != NULL) {
	ElabCallback elabCallback(curFlow, callbackElab, this, mFactory,
                                  mNetRefFactory);
	ESCallback* prevCallback = mTranslateExpr->putCallback(&elabCallback);
	CarbonExpr* cExpr = translateExpr(curFlow->getFLNode(), expr);
	mTranslateExpr->putCallback(prevCallback);
	if (cExpr != NULL) {
	  // All done, use this expression
	  hndl = ESExprHndl(cExpr);
	  validExpression = true;
	}
      }

      // If we didn't get a valid expression, we want to error out
      if (!validExpression)
	curFlow = NULL;
    } else {
      // Check if this is a LHS concat port connection. Can't handle
      // them yet.
      if (illegalPortLvalue(useDef)) {
	// Give up
	curFlow = NULL;
      } else {
	// It must be an output or bid port. We use the fanin to create
	// an expression. Gather the fanin flow nodes
	FLNodeElab* faninElab = NULL;
        flows.clear();
        numDrivers = 0;
	for (FLNodeElabLoop l = curFlow->loopFanin(); !l.atEnd(); ++l) {
	  faninElab = *l;
	  ++numDrivers;
          flows.push_back(faninElab);
	}

	// Create the expression
	if (numDrivers == 1) {
	  // Use this flow to translate
	  curFlow = faninElab;
	  useDef = curFlow->getUseDefNode();
	}
        else if (numDrivers == 0)
          curFlow = NULL;
      } // else
    } // else
  } // while

  // If we found a cycle, give up. If we found new flow node(s) to
  // process do so now. If numDrivers > 1 then use the
  // ElabCallback::createIdentExpr function that can deal with
  // multiple drivers. If numDrivers == 1 then do a recursive call.
  if (cycleFound) {
    validExpression = false;

  } else if (numDrivers > 1) {
    // We have multiple drivers. We need a net ref that matches the
    // driving net but uses the range from the original net
    //
    // Get the net for the fanin drivers. We only support one net for
    // now.
    NUNet* faninNet = NULL;
    NUNetRefHdl faninNetRef = mNetRefFactory->createEmptyNetRef();
    validExpression = true;
    for (FLNodeElabVectorLoop l(flows); !l.atEnd() && validExpression; ++l) {
      FLNodeElab* elabFlow = *l;
      NUNet* thisNet = elabFlow->getFLNode()->getDefNet();
      const NUNetRefHdl& netRef = elabFlow->getFLNode()->getDefNetRef();
      if (faninNet == NULL) {
        // First time through the loop
        faninNet = thisNet;
        faninNetRef = mNetRefFactory->merge(faninNetRef, netRef);

      } else if (faninNet == thisNet) {
        // We can keep going
        faninNetRef = mNetRefFactory->merge(faninNetRef, netRef);

      } else {
        // give up
        validExpression = false;
      }
    }

    // Create a net ref for the fanin net from the current net. We use
    // the size from the current flow as well. But note that, we have
    // to do this in a complex way because of all the possible complex
    // port connection scenarios. This is because we have run before
    // port lowering.
    //
    // We currently handle two scenarios:
    //
    // 1. The requested range is different than the driven range. In
    //    this case we have to use createNetRefImage that requires
    //    that both fanout/fanin nets be the same size.
    //
    // 2. The requested range is the same as the driven range, but one
    //    side is a scalar an the other is a bit select of a vector.
    NUNetRefHdl netRef;
    if (validExpression) {
      NUNetRefHdl curNetRef = curFlow->getFLNode()->getDefNetRef();
      if (curNetRef->getNet()->getNetRefWidth() == faninNet->getNetRefWidth()) {
        // create image will work
        netRef = mNetRefFactory->createNetRefImage(faninNet, curNetRef);

      } else if (curNetRef->getNumBits() == faninNetRef->getNumBits()) {
        // Ok we can't usage create image but the two requested sizes
        // are the same. This means it is some soft of same sized
        // assignment from nets that are different sizes. Use the
        // fanin net ref instead.
        //
        // One example of this is a bit select passed to a scalar
        // port.
        netRef = faninNetRef;

      } else {
        // Other scenarios: give up for now
        validExpression = false;
      }
    }

    // If we can create a valid expression, recurse
    if (validExpression) {
      // Create an elab call back for this new start point. We use any
      // elaborated flow as the starting point since all we really care
      // about is the hierarchy and net. We get that curFlow from the
      // loop at the beginning of this routine.
      bool isSigned = netRef->getNet()->isSigned();
      UInt32 bitSize = netRef->getNumBits();
      ElabCallback elabCallback(curFlow, callbackElab, this, mFactory,
                                mNetRefFactory);

      // Recursively create the expression for this set of flows. This
      // code will check to see if we can make this ident with a concat
      // of flows. If we end up with singleton elab flows in each item
      // of the concat, it may recurse further.
      CarbonExpr* expr = elabCallback.createIdentExpr(flows, netRef, bitSize,
                                                      isSigned, false);
      if (expr != NULL) {
        expr = resize(expr, bitSize, isSigned);
        hndl = ESExprHndl(expr);
        validExpression = true;
      }
    } // if

  } else if ((curFlow != NULL) && (curFlow != flow) && !validExpression) {
    ElabCallback elabCallback(curFlow, callbackElab, this, mFactory,
                              mNetRefFactory);
    hndl = create(curFlow->getFLNode(), &elabCallback);
    validExpression = true;
  }

  return validExpression;
} // ESPopulateExpr::translatePortConnection

bool ESPopulateExpr::illegalPortLvalue(NUUseDefNode* useDef) const
{
  // Get the actual for output or inout ports.
  NULvalue* lvalue = NULL;
  NUPortConnectionOutput* outPort;
  outPort = dynamic_cast<NUPortConnectionOutput*>(useDef);
  if (outPort != NULL)
    lvalue = outPort->getActual();
  else
  {
    NUPortConnectionBid* bidPort;
    bidPort = dynamic_cast<NUPortConnectionBid*>(useDef);
    if (bidPort != NULL)
      lvalue = bidPort->getActual();
    else
      NU_ASSERT("Routine should not be called on input ports" == NULL, useDef);
  }

  // Make sure this is an actual we can create an expression
  // for. Today we don't support NULL ports or complex ports such as
  // concat lvalues.
  return ((lvalue == NULL) ||
          ((lvalue->getType() != eNUIdentLvalue) &&
           (lvalue->getType() != eNUVarselLvalue)));
           
} // bool ESPopulateExpr::illegalPortLvalue

ESExprHndl 
ESPopulateExpr::resize(ESExprHndl exprHndl, UInt32 bitSize, bool isSigned)
{
  CarbonExpr* expr = exprHndl.getExpr();
  return ESExprHndl(resize(expr, bitSize, isSigned));
}

CarbonExpr* 
ESPopulateExpr::resize(CarbonExpr* expr, UInt32 bitSize, bool isSigned)
{
  UInt32 esize = expr->getBitSize();
  if (bitSize != esize) {
    // If we are making it bigger we better protect it with
    // a concat.  Consider this case:
    //    wire a;
    //    wire b = ~a;
    //    wire [2:0] c = b;
    // We want to avoid creating
    //    wire [2:0] c = ~a;
    // because the funny verilog sizing rules are liable to extend
    // a to 3 bits before applying the ~, and we will get 3'b111
    // if a=0.  Also be careful of sign extension.
    if (bitSize > esize) {
      if (isSigned) {
        expr = mFactory->createUnaryOp(CarbonExpr::eUnSignExt,
                                       expr, bitSize, true);
      }
      else {
        CarbonExprVector v;
        v.push_back(mFactory->createConst(0, bitSize - esize, false));
        v.push_back(expr);
        expr = mFactory->createConcatOp(&v, 1, bitSize, false);
      }

    } else {
      // Create a part select for the smaller size
      ConstantRange range(bitSize - 1, 0);
      expr = mFactory->createPartsel(expr, range, bitSize, expr->isSigned());
    }
  } else {
    if ( isSigned != expr->isSigned() ) {
      // caller requested only a signed'ness change
      expr = mFactory->createUnaryOp(isSigned? CarbonExpr::eUnSignExt : CarbonExpr::eUnBuf,
                                     expr, bitSize, isSigned);
    }
  }
  return expr;
} // CarbonExpr* ESPopulateExpr::resize

FLNodeVectorLoop ESFlowIdent::loopFlows()
{
  return FLNodeVectorLoop(*mFlows);
}

int ESFlowIdent::numFlows() const
{
  return mFlows->size();
}

const NUNetRefHdl& ESFlowIdent::getNetRef() const { return mNetRef; }

bool ESFlowIdent::isWholeIdentifier() const
{
  return mNetRef->all();
}

FLNodeElabVectorLoop ESFlowElabIdent::loopFlows()
{
  return FLNodeElabVectorLoop(*mFlowElabs);
}

int ESFlowElabIdent::numFlows() const
{
  return mFlowElabs->size();
}

const NUNetRefHdl& ESFlowElabIdent::getNetRef() const { return mNetRef; }

bool ESFlowElabIdent::isWholeIdentifier() const
{
  return mNetRef->all();
}


NUNetRefHdl
ESPopulateExpr::NetRefFunctor::getNetRef(FLNode* flow, NUNetRefFactory*)
{
  return flow->getDefNetRef();
}

ESPopulateExpr::FLNodeCaseClassifier*
ESPopulateExpr::classifyCaseFlow(FLNode* flow)
{
  // Check if we have classified this flow before
  NUUseDefNode* useDef = flow->getUseDefNode();
  CaseFlowCache::iterator pos = mCaseFlowCache->find(useDef);
  FLNodeCaseClassifier* classifier = NULL;
  if (pos == mCaseFlowCache->end()) {
    // Create and remember it
    classifier = new FLNodeCaseClassifier(useDef);
    mCaseFlowCache->insert(CaseFlowCache::value_type(useDef, classifier));
  } else {
    classifier = pos->second;
  }
  classifier->classifyFlow(flow);
  return classifier;
}

void ESPopulateExpr::clearCaseFlowCache(void)
{
  CaseFlowCache::iterator i;
  for (i = mCaseFlowCache->begin(); i != mCaseFlowCache->end(); ++i) {
    FLNodeCaseClassifier* classifier = i->second;
    delete classifier;
  }
  mCaseFlowCache->clear();
}

void ESPopulateExpr::clearExprCache(void)
{
  mFactory->clear();
}

const ESFlowIdent* ESFlowIdent::castESFlowIdent() const {
  return this;
}

const ESFlowElabIdent* ESFlowElabIdent::castESFlowElabIdent() const {
  return this;
}
