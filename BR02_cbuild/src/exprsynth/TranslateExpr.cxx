// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "TranslateExpr.h"
#include "nucleus/NUExpr.h"
#include "exprsynth/ExprFactory.h"

ESTranslateExpr::ESTranslateExpr(ESFactory* factory) : mFactory(factory)
{}

ESTranslateExpr::~ESTranslateExpr() {}

CarbonExpr*
ESTranslateExpr::recursiveTranslateExpr(NUExpr* expr)
{
  NUExpr::Type type = expr->getType();
  CarbonExpr* resExpr = NULL;
  switch(type)
  {
  case NUExpr::eNUVarselRvalue:
  {
    NUVarselRvalue* varsel = dynamic_cast<NUVarselRvalue*>(expr);
    resExpr = translateVarsel(varsel);
    break;
  }

  case NUExpr::eNUConstXZ:
  {
    NUConst* constXZ = expr->castConst ();
    resExpr = translateConstXZ(constXZ);
    break;
  }

  case NUExpr::eNUConstNoXZ:
  {
    // Currently don't support real numbers, we should add support
    // here and to global constant propagation.
    NUConst* constNoXZ = expr->castConst ();
    if (constNoXZ->isReal()) {
      resExpr = NULL;
    } else {
      resExpr = translateConst(constNoXZ);
    }
    break;
  }

  case NUExpr::eNUEdgeExpr:
  {
    NUEdgeExpr * edge = dynamic_cast<NUEdgeExpr*>(expr);
    resExpr = translateEdge(edge);
    break;
  }
    
  case NUExpr::eNUSysFunctionCall:
    resExpr = NULL;
    break;
    
  case NUExpr::eNUIdentRvalue:
  {
    NUIdentRvalue* ident = dynamic_cast<NUIdentRvalue*>(expr);
    resExpr = translateIdent(ident);
    break;
  }

  case NUExpr::eNUMemselRvalue:
  {
    NUMemselRvalue* memsel = dynamic_cast<NUMemselRvalue*>(expr);
    resExpr = translateMemsel(memsel);
    break;
  }

  case NUExpr::eNUUnaryOp:
  {
    NUUnaryOp* unaryOp = dynamic_cast<NUUnaryOp*>(expr);
    resExpr = translateUnaryOp(unaryOp);
    break;
  }

  case NUExpr::eNUBinaryOp:
  {
    NUBinaryOp* binaryOp = dynamic_cast<NUBinaryOp*>(expr);
    resExpr = translateBinaryOp(binaryOp);
    break;
  }

  case NUExpr::eNUTernaryOp:
  {
    NUTernaryOp* ternaryOp = dynamic_cast<NUTernaryOp*>(expr);
    resExpr = translateTernaryOp(ternaryOp);
    break;
  }

  case NUExpr::eNUConcatOp:
  {
    NUConcatOp* concat = dynamic_cast<NUConcatOp*>(expr);
    resExpr = translateConcat(concat);
    break;
  }

  case NUExpr::eNULut:
  {
    // we should be able to handle LUTs, but for now give up
    resExpr = NULL;
    break;
  }

  default:
    NU_ASSERT("Unknown nucleus expression type" == NULL, expr);
  } // switch

  // Make sure we created the correct size expression. All the
  // translateXXX functions above are responsible for doing this.
  if (resExpr != NULL) {
    NU_ASSERT(expr->getBitSize() == resExpr->getBitSize(), expr);
  }

  // Do we need to treat the result of this expression as if it were
  // sign-extended?  (Constants correctly retain their signedness and
  // shouldn't need an explicit extension operator.
  bool signMismatch = (resExpr != NULL) &&
    (resExpr->isSigned() != expr->isSignedResult());

  if (signMismatch ||
      (resExpr && expr->isSignedResult() &&
       (resExpr->castConst() == NULL)))
  {
    CarbonUnaryOp* u = resExpr->castUnary();
    if (signMismatch || (u == NULL) ||
        (u->getType() != CarbonExpr::eUnSignExt))
    {
      resExpr = mFactory->createUnaryOp(CarbonExpr::eUnSignExt, resExpr,
                                        expr->getBitSize (),
                                        expr->isSignedResult());
    }
  }

  return resExpr;
}

CarbonExpr*
ESTranslateExpr::translateConstXZ(NUConst* constXZ)
{
  // Get the data from the NU constant. We may want to redo how sizing
  // works.
  DynBitVector val;
  DynBitVector drive;
  constXZ->getValueDrive(&val, &drive);
  UInt32 bitSize = constXZ->getBitSize();
  CarbonConst::SignT sign = getSign(constXZ);

  // Create the expression style constant
  return mFactory->createConstXZ(val, drive, sign, bitSize);
}

CarbonConst::SignT ESTranslateExpr::getSign(NUConst* k) {
  CarbonConst::SignT sign = CarbonConst::eBadSign;
  if (!k->isSignedResult()) {
    sign = CarbonConst::eUnsigned;
  }
  else {
    sign = CarbonConst::ePos;
  }
  return sign;
}

CarbonExpr*
ESTranslateExpr::translateConst(NUConst* constNoXZ)
{
  // Get the data from the NU constant. We may want to redo how sizing
  // works.
  DynBitVector val;
  constNoXZ->getSignedValue(&val);
  UInt32 bitSize = constNoXZ->getBitSize();
  CarbonConst::SignT sign = constNoXZ->isSignedResult () ? CarbonConst::ePos
                                                         : CarbonConst::eUnsigned;

  // Create the expression style constant
  return mFactory->createConst(val, sign, bitSize);
}


CarbonExpr*
ESTranslateExpr::translateIdent(NUIdentRvalue* ident)
{
  // Match the input net to the input flow nodes so we can create the
  // right type of expression. There may be more than one flow net so
  // we provide what bits we want. Since this is an ident, we want all
  // of them.
  NUNet* net = ident->getIdent();
  const ConstantRange& range = getNetRange(net);

  // make sure this is not a memory. We don't handle these yet.
  if (net->is2DAnything()) {
    return NULL;
  }

  // Now create the identity expressions
  CarbonExpr *id = translateNet(net, range, ident->getBitSize(),
                                ident->isSignedResult());

  return id;
}

CarbonExpr*
ESTranslateExpr::translateMemsel(NUMemselRvalue* /* memsel */)
{
  return NULL;
}

CarbonExpr*
ESTranslateExpr::translateUnaryOp(NUUnaryOp* unaryOp)
{
  // Translate the RHS
  NUExpr* rhs = unaryOp->getArg(0);
  CarbonExpr* subExpr = recursiveTranslateExpr(rhs);
  if (subExpr == NULL)
    return subExpr;

  // Create the new carbon expression for this type of nucleus operator
  CarbonExpr* retExpr;
  NUOp::OpT type = unaryOp->getOp();
  UInt32 bitSize = unaryOp->getBitSize();
  bool isSigned = unaryOp->isSignedResult();
  switch(type)
  {
    case NUOp::eUnBuf:
      retExpr = mFactory->createUnaryOp(CarbonExpr::eUnBuf, subExpr, bitSize,
                                        isSigned);
      break;

    case NUOp::eUnBitNeg:
      retExpr = mFactory->createUnaryOp(CarbonExpr::eUnBitNeg, subExpr,
					bitSize, isSigned);
      break;
    case NUOp::eUnVhdlNot:
      retExpr = mFactory->createUnaryOp(CarbonExpr::eUnVhdlNot, subExpr,
					bitSize, isSigned);
      break;

    case NUOp::eUnChange:
      retExpr = mFactory->createUnaryOp(CarbonExpr::eUnChange, subExpr,
					bitSize, isSigned);
      break;

    case NUOp::eUnRedAnd:
        retExpr = mFactory->createEqAllOnes(subExpr, bitSize);
        break;

    case NUOp::eUnRedOr:
        retExpr = mFactory->createNeqZero(subExpr, bitSize);
        break;

    case NUOp::eUnLogNot:
        retExpr = mFactory->createEqZero(subExpr, bitSize);
        break;

    case NUOp::eUnCount:
        retExpr = translateReductionOp (CarbonExpr::eBiPlus, subExpr, bitSize);
        break;

    case NUOp::eUnRedXor:
    {
      // The best method for this is to break up the bits and xor them
      retExpr = translateReductionOp(CarbonExpr::eBiBitXor, subExpr, bitSize);
      break;
    }

    case NUOp::eUnFFZ:
    case NUOp::eUnFFO:
    case NUOp::eUnFLZ:
    case NUOp::eUnFLO:
    case NUOp::eUnPlus:
    case NUOp::eUnMinus:
    case NUOp::eUnAbs:
    case NUOp::eUnRound:
    default:
      retExpr = NULL;
      break;
  } // switch

  if (retExpr == NULL)
    subExpr->decRefCnt();

  return retExpr;
}

CarbonExpr*
ESTranslateExpr::translateBinaryOp(NUBinaryOp* binaryOp)
{
  // Translate the two arguments
  NUExpr* nExpr1 = binaryOp->getArg(0);
  CarbonExpr* expr1 = recursiveTranslateExpr(nExpr1);
  if (expr1 == NULL)
    return NULL;
  NUExpr* nExpr2 = binaryOp->getArg(1);
  CarbonExpr* expr2 = recursiveTranslateExpr(nExpr2);
  if (expr2 == NULL)
  {
    expr1->decRefCnt();
    return NULL;
  }

  // Create the new Carbon expression for this type of nucleus operator
  NUOp::OpT type = binaryOp->getOp();
  UInt32 bitSize = binaryOp->getBitSize();
  bool isSigned = binaryOp->isSignedResult();
  CarbonExpr* retExpr;
  switch(type)
  {
    case NUOp::eBiPlus:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiPlus, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiMinus:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiMinus, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiSMult:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiSMult, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiSDiv:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiSDiv, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiSMod:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiSMod, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiUMult:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiUMult, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiUDiv:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiUDiv, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiUMod:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiUMod, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiTrieq:
    case NUOp::eBiEq:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiEq, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiTrineq:
    case NUOp::eBiNeq:
      // Translate (a != b) to ~(a == b)
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiEq, expr1, expr2, 1,
                                         false, false);
      retExpr = mFactory->createUnaryOp(CarbonExpr::eUnBitNeg, retExpr,
					1, isSigned);
      retExpr = mFactory->createZeroPad (retExpr, bitSize);
      break;

    case NUOp::eBiLogAnd:
    {
      // Translate (a && b) to (a != 0) & (b != 0)
      CarbonExpr* and1;
      CarbonExpr* and2;
      and1 = mFactory->createNeqZero(expr1, 1);
      and2 = mFactory->createNeqZero(expr2, 1);
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiBitAnd, and1, and2,
					 1, isSigned, false);
      retExpr = mFactory->createZeroPad (retExpr, bitSize);
      break;
    }
      
    case NUOp::eBiLogOr:
    {
      // Translate (a || b) to (a != 0) | (b != 0)
      CarbonExpr* orLeg1;
      CarbonExpr* orLeg2;
      orLeg1 = mFactory->createNeqZero(expr1, 1);
      orLeg2 = mFactory->createNeqZero(expr2, 1);
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiBitOr, orLeg1, orLeg2,
					 1, isSigned, false);
      retExpr = mFactory->createZeroPad (retExpr, bitSize);
      break;
    }

    case NUOp::eBiULt:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiULt, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiSLt:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiSLt, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiSLte:
    {
      // Translate (a <= b) to (a < b) | (a == b)
      CarbonExpr* orLeg1;
      CarbonExpr* orLeg2;
      orLeg1 = mFactory->createBinaryOp(CarbonExpr::eBiSLt, expr1, expr2, 1, false, false);
      orLeg2 = mFactory->createBinaryOp(CarbonExpr::eBiEq, expr1, expr2, 1, false, false);
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiBitOr, orLeg1, orLeg2,
					 bitSize, isSigned, false);
      break;
    }
      
    case NUOp::eBiSGtr:
    {
      // Translate (a > b) to ~((a < b) | (a == b))
      CarbonExpr* orLeg1;
      CarbonExpr* orLeg2;
      orLeg1 = mFactory->createBinaryOp(CarbonExpr::eBiSLt, expr1, expr2, 1, false, false);
      orLeg2 = mFactory->createBinaryOp(CarbonExpr::eBiEq, expr1, expr2, 1, false, false);
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiBitOr, orLeg1, orLeg2,
					 1, false, false);
      retExpr = mFactory->createUnaryOp(CarbonExpr::eUnBitNeg, retExpr,
					1, isSigned);
      retExpr = mFactory->createZeroPad (retExpr, bitSize);
      break;
    }
      
    case NUOp::eBiSGtre:
      // Translate (a >= b) to ~(a < b)
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiSLt, expr1, expr2, 1, false, false);
      retExpr = mFactory->createUnaryOp(CarbonExpr::eUnBitNeg, retExpr,
					1, isSigned);
      retExpr = mFactory->createZeroPad (retExpr, bitSize);
      break;

    case NUOp::eBiULte:
    {
      // Translate (a <= b) to (a < b) | (a == b)
      CarbonExpr* orLeg1;
      CarbonExpr* orLeg2;
      orLeg1 = mFactory->createBinaryOp(CarbonExpr::eBiULt, expr1, expr2, 1, false, false);
      orLeg2 = mFactory->createBinaryOp(CarbonExpr::eBiEq, expr1, expr2, 1, false, false);
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiBitOr, orLeg1, orLeg2,
					 bitSize, isSigned, false);
      break;
    }
      
    case NUOp::eBiUGtr:
    {
      // Translate (a > b) to ~((a < b) | (a == b))
      CarbonExpr* orLeg1;
      CarbonExpr* orLeg2;
      orLeg1 = mFactory->createBinaryOp(CarbonExpr::eBiULt, expr1, expr2, 1, false, false);
      orLeg2 = mFactory->createBinaryOp(CarbonExpr::eBiEq, expr1, expr2, 1, false, false);
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiBitOr, orLeg1, orLeg2,
					 1, false, false);
      retExpr = mFactory->createUnaryOp(CarbonExpr::eUnBitNeg, retExpr,
					1, isSigned);
      retExpr = mFactory->createZeroPad (retExpr, bitSize);
      break;
    }
      
    case NUOp::eBiUGtre:
      // Translate (a >= b) to ~(a < b)
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiULt, expr1, expr2, 1, false, false);
      retExpr = mFactory->createUnaryOp(CarbonExpr::eUnBitNeg, retExpr,
					1, isSigned);
      retExpr = mFactory->createZeroPad (retExpr, bitSize);
      break;

    case NUOp::eBiBitAnd:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiBitAnd, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiBitOr:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiBitOr, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiBitXor:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiBitXor, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiRshift:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiRshift, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiLshift:
      retExpr = mFactory->createBinaryOp(CarbonExpr::eBiLshift, expr1, expr2,
					 bitSize, isSigned, false);
      break;

    case NUOp::eBiDownTo:
      retExpr = mFactory->createBinaryOp (CarbonExpr::eBiDownTo, expr1, expr2,
                                          bitSize, isSigned, false);

    case NUOp::eBiVhdlMod:
    case NUOp::eBiExp:
    case NUOp::eBiDExp:
    case NUOp::eBiVhExt:
    case NUOp::eBiVhZxt:
    case NUOp::eBiVhLshift:
    case NUOp::eBiVhRshift:
    case NUOp::eBiLshiftArith:
    case NUOp::eBiRshiftArith:
    case NUOp::eBiVhRshiftArith:
    case NUOp::eBiVhLshiftArith:

    case NUOp::eBiRoR:
    case NUOp::eBiRoL:
      retExpr = NULL;
      return retExpr;

    default:
      NU_ASSERT("Unknown nucleus op type" == NULL, binaryOp);
      retExpr = NULL;
      break;
  } // switch

  NU_ASSERT(retExpr != NULL, binaryOp);

  return retExpr;
}

CarbonExpr*
ESTranslateExpr::translateTernaryOp(NUTernaryOp* ternaryOp)
{
  // Translate the three arguments
  NUExpr* nExpr1 = ternaryOp->getArg(0);
  CarbonExpr* expr1 = recursiveTranslateExpr(nExpr1);
  if (expr1 == NULL)
    return NULL;
  NUExpr* nExpr2 = ternaryOp->getArg(1);
  CarbonExpr* expr2 = recursiveTranslateExpr(nExpr2);
  if (expr2 == NULL)
  {
    expr1->decRefCnt();
    return NULL;
  }
  NUExpr* nExpr3 = ternaryOp->getArg(2);
  CarbonExpr* expr3 = recursiveTranslateExpr(nExpr3);
  if (expr3 == NULL)
  {
    expr1->decRefCnt();
    expr2->decRefCnt();
    return NULL;
  }

  // Create the new Carbon expression for this type of nucleus operator
  NUOp::OpT type = ternaryOp->getOp();
  NU_ASSERT(type == NUOp::eTeCond, ternaryOp);
  CarbonExpr* retExpr;
  retExpr = mFactory->createTernaryOp(CarbonExpr::eTeCond, expr1, expr2,
				      expr3, ternaryOp->getBitSize(),
                                      ternaryOp->isSignedResult(), false);

  return retExpr;
}

CarbonExpr*
ESTranslateExpr::translateConcat(NUConcatOp* concat)
{
  // Translate the expressions for each segement of the concat
  CarbonExprVector exprs;
  bool fail = false;
  for (UInt32 index = 0; index < concat->getNumArgs() && !fail; ++index)
  {
    NUExpr* expr = concat->getArg(index);
    CarbonExpr* subExpr = recursiveTranslateExpr(expr);
    if (subExpr == NULL)
      fail = true;
    else
      exprs.push_back(subExpr);
  }

  // Create the concat op if we have all the sub expressions
  if (!fail)
  {
    CarbonExpr* retExpr;
    retExpr = mFactory->createConcatOp(&exprs, concat->getRepeatCount(),
				       concat->getBitSize(),
                                       concat->isSignedResult());
    return retExpr;
  }
  else
  {
    // Don't need the sub expressions anymore
    for (CarbonExprVectorIter i = exprs.begin(); i != exprs.end(); ++i)
    {
      CarbonExpr* subExpr = *i;
      subExpr->decRefCnt();
    }
    return NULL;
  }
}

CarbonExpr*
ESTranslateExpr::translateVarsel(NUVarselRvalue* varsel)
{
  // Don't translate array selects
  if (varsel->isArraySelect()) {
    return NULL;
  }

  // Check if this is a part select or bit select. The best way to do
  // this is to get the constant range and test if it is [0:0], then
  // we have a bit select which is stored in the index. We also want
  // to know if the index is a constant 0, if so this is a simple
  // partsel.
  const ConstantRange* range = varsel->getRange();
  bool isPartsel = (range->getMsb() != 0) || (range->getLsb() != 0);
  bool const0Index = varsel->isConstIndex() && (varsel->getConstIndex() == 0);

  // We don't support variable indicies with part sels. We need a new
  // op type in Carbon expressions to support them. We also don't
  // support part selects with a non const 0 index
  if (isPartsel && !const0Index) {
    return NULL;
  }

  // Translate the index first. This can be a constant or variable index
  NUExpr* expr = varsel->getIndex();
  CarbonExpr* indexExpr = recursiveTranslateExpr(expr);
  if (indexExpr == NULL) {
    return NULL;
  }

  // If this is a constant index, then we can push the bit select down
  // to the identifier. This allows the creation of the identifier to
  // pick out the right drivers if we only want a subset. For variable
  // indices or varsels on expressions, we get the entire identifier
  bool isSigned = varsel->isSignedResult();
  UInt32 bitSize = varsel->getBitSize();
  NUNet* net = varsel->getIdentNet();
  CarbonExpr* netExpr = NULL;
  if ((net != NULL) && varsel->isConstIndex()) {
    // Create the bit/part select range
    SInt32 val;
    CarbonConst* cConst = indexExpr->castConst();
    cConst->getL(&val);
    ConstantRange netRange(range->getMsb()+val, range->getLsb()+val);

    // Create the identity for the requested range
    netExpr = translateNet(net, netRange, bitSize, isSigned);

  } else {
    // Translate the identity expression
    if (net != NULL) {
      // Create the identity for the entire net
      const ConstantRange* partRange = varsel->getRange ();
      ConstantRange netRange = getNetRange(net);

      if (partRange->getMsb () != 0 || partRange->getLsb () != 0) {
        // Can't represent indexed-partsel in CarbonExprs.
        return NULL;
      }

      netExpr = translateNet(net, netRange, netRange.getLength(), isSigned);

    } else {
      // Translate it as an expression
      netExpr = recursiveTranslateExpr(varsel->getIdentExpr());
    }
    
    // Create the bit/part select if we have a valid expression
    if (netExpr != NULL) {
      if (isPartsel) {
        netExpr = mFactory->createPartsel(netExpr, *range, bitSize, isSigned);
      } else {
        netExpr = mFactory->createBinaryOp(CarbonExpr::eBiBitSel, netExpr,
                                           indexExpr, varsel->getBitSize(),
                                           varsel->isSignedResult(), false);
      }
    } else {

      // Won't be using this expression
      indexExpr->decRefCnt();
    }
  }

  return netExpr;
}

CarbonExpr*
ESTranslateExpr::translateEdge(NUEdgeExpr * edge)
{
  NUExpr * edgeExpr = edge->getExpr();
  ClockEdge clockEdge = edge->getEdge();

  CarbonExpr * edgeCExpr = recursiveTranslateExpr(edgeExpr);

  CarbonExpr * retCExpr = mFactory->createEdge(clockEdge,
                                               edgeCExpr, 
                                               edge->getBitSize());

  return retCExpr;
}


CarbonExpr*
ESTranslateExpr::translateReductionOp(CarbonExpr::ExprT /* type */,
				      CarbonExpr* /* subExpr */,
				      UInt32 /* bitSize */)
{
  // Go through the vector range and create the bit select operators
  CarbonExpr* retExpr = NULL;
  return retExpr;
}

ConstantRange
ESTranslateExpr::getNetRange(NUNet* net)
{
  ConstantRange range(0,0);
  NUVectorNet* vecNet = dynamic_cast<NUVectorNet*>(net);
  if (vecNet != NULL)
    range = *(vecNet->getRange());
  return range;
}

