// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <cstddef>
#include "util/CarbonTypes.h"
#include "util/UtOrder.h"       // For carbonPtrCompare()
#include "exprsynth/ESPopulateExpr.h"
#include "nucleus/NUNet.h"
#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "symtab/STBranchNode.h"

ESFlowIdent::ESFlowIdent(const FLNodeVector& flows, const NUNetRefHdl& netRef,
                         UInt32 bitSize, bool isSigned) :
  CarbonIdent(isSigned, bitSize), mNetRef(netRef)
{
  if (!flows.empty())
  {
    FLNode* flow = flows[0];
    NUNet* net = flow->getDefNet();
    if (net != NULL)
    {
      NUVectorNet* vn = net->castVectorNet();
      if (vn != NULL)
        putDeclaredRange(vn->getDeclaredRange());
    }
  }
  mFlows = new FLNodeVector(flows);
}

ESFlowIdent::~ESFlowIdent() {
  delete mFlows;
}

// The following are the set of Flow Identifier class functions

size_t ESFlowIdent::hash() const
{
  // In the future we may want to improve this based on the full
  // hierarchical name
  size_t hashVal = 0;
  for (FLNodeVector::iterator p = mFlows->begin(); p != mFlows->end(); ++p)
  {
    FLNode* flow = *p;
    hashVal += (size_t) flow;
  }
  return hashVal;
}

ptrdiff_t ESFlowIdent::compare(const CarbonExpr* expr) const
{
  // Make sure the other type is the same as this
  const ESFlowIdent* otherFlowIdent = expr->castESFlowIdent();
  if (otherFlowIdent == NULL)
    return -1;

  // Both are flow idents, compare them. There is no good compare
  // routine yet. TBD - write one and use it.
  size_t numFlowsMe = mFlows->size();
  size_t numFlowsOther = otherFlowIdent->mFlows->size();
  
  ptrdiff_t ret = (SIntPtr)numFlowsMe - (SIntPtr)numFlowsOther;
  for (size_t i = 0; (i < numFlowsMe) && (ret == 0); ++i)
  {
    FLNode* flowMe = (*mFlows)[i];
    FLNode* flowOther = (*(otherFlowIdent->mFlows))[i];
    ret = carbonPtrCompare(flowMe, flowOther);
  }
  return ret;
}

UInt32 ESFlowIdent::determineBitSize() const
{
  return mNetRef->getNumBits();
}

void ESFlowIdent::print(bool /* recurse */, int indent) const
{
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < indent; i++) {
    screen << " ";
  }
  screen << typeStr() << '(' << this << ") ";
  CarbonExpr::printSize(screen);
  screen << " : ";

  for (FLNodeVector::iterator p = mFlows->begin(); p != mFlows->end(); ++p)
  {
    FLNode* flow = *p;
    screen << flow->getDefNetRef()->getNet()->getName()->str() << UtIO::endl;
  }
}  

static void composeNetBits(UtString* buf, const NUNetRefHdl& netRef,
                           NUNet* namedNet)
{
  if (!netRef->all())
  {
    *buf << "[";
    NUVectorNet* vecNet = namedNet->castVectorNet();
    const ConstantRange* namedRange = NULL;
    if (vecNet != NULL) {
      namedRange = vecNet->getDeclaredRange();
    }
    else {
      NUMemoryNet* memNet = namedNet->getMemoryNet();
      NU_ASSERT(memNet, namedNet);

      // the bits in the memory netRef correspond to rows in a memory
      // net, and those are *not* normalized
    }
    
    ConstantRange netRange;
    if (netRef->getRange(netRange))
    {
      if (namedRange != NULL) {
        netRange.denormalize(namedRange);
      }

      SInt32 lsb, msb;
      lsb = netRange.getLsb();
      msb = netRange.getMsb();
      if (lsb == msb)
	*buf << lsb;
      else
	*buf << msb << ":" << lsb;
    }
    else
    {
      DynBitVector bits(netRef->getNumBits());
      netRef->getUsageMask(&bits);
      bool first = true;
      for (UInt32 i = 0; i < bits.size(); ++i)
      {
        if (bits.test(i))
        {
          SInt32 index = i;
          if (namedRange != NULL) {
            index = namedRange->index(i);
          }
          if (first)
            first = false;
          else
            *buf << ", ";
          *buf << index;
        }
      }
    }
    *buf << "]";
  } // if
} // static void composeNetBits

void ESFlowIdent::composeIdent(ComposeContext* context) const
{
  UtString* buf = context->getBuffer();
  const NUNetRefHdl& netRef = getNetRef();
  NUNet* namedNet = netRef->getNet();
  *buf << namedNet->getName()->str();
  composeNetBits(buf, netRef, netRef->getNet());
}  

CarbonExpr::SignT ESFlowIdent::evaluate(ExprEvalContext*) const
{
  return eBadSign;
}

CarbonExpr::AssignStat 
ESFlowIdent::assign(ExprAssignContext*)
{
  CE_ASSERT("Assign not valid" == NULL, this);
  return eReadOnly;
}

CarbonExpr::AssignStat 
ESFlowIdent::assignRange(ExprAssignContext*, const ConstantRange&)
{
  CE_ASSERT("Assign not valid" == NULL, this);
  return eReadOnly;
}

FLNode* ESFlowIdent::getFlow() const
{ 
  return (*mFlows)[0]; 
}

static void sGetUsageMask(const FLNode* flnode, DynBitVector* usageMask)
{
  NUNetRefHdl netRef = flnode->getDefNetRef();
  netRef->getUsageMask(usageMask);
}

const STAliasedLeafNode* 
ESFlowIdent::getNode(DynBitVector* usageMask) const
{
  FLNode* flow = (*mFlows)[0];
  sGetUsageMask(flow, usageMask);
  return NULL;
}



ESFlowElabIdent::ESFlowElabIdent(const FLNodeElabVector* flowElabs,
                                 const NUNetRefHdl& netRef,
                                 UInt32 bitSize,  bool isSigned) :
  CarbonIdent(isSigned, bitSize), mNetRef(netRef)
{
  if (!flowElabs->empty())
  {
    FLNodeElab* flow = (*flowElabs)[0];
    NUNetElab* netElab = flow->getDefNet();
    if (netElab != NULL)
    {
      NUVectorNet* vn = netElab->getNet()->castVectorNet();
      if (vn != NULL)
        putDeclaredRange(vn->getDeclaredRange());
    }
  }
  mFlowElabs = new FLNodeElabVector(*flowElabs);
}

ESFlowElabIdent::~ESFlowElabIdent() {
  delete mFlowElabs;
}

FLNodeElab* ESFlowElabIdent::getFlowElab() const 
{ 
  return (*mFlowElabs)[0]; 
}

//! Get the unelaborated flow associated with this identifier
FLNode* ESFlowElabIdent::getFlow() const 
{ 
  return getFlowElab()->getFLNode(); 
}

// The following are the set of FlowElab Identifier class functions

size_t ESFlowElabIdent::hash() const
{
  // In the future we may want to improve this based on the full
  // hierarchical name
  size_t hashVal = 0;
  for (FLNodeElabVector::iterator p = mFlowElabs->begin(); p != mFlowElabs->end(); ++p)
  {
    FLNodeElab* elab = *p;
    hashVal += (size_t) elab;
  }
  return hashVal;
}

ptrdiff_t ESFlowElabIdent::compare(const CarbonExpr* expr) const
{
  // Make sure the other type is the same as this
  const ESFlowElabIdent* otherFlowElabIdent;
  otherFlowElabIdent = expr->castESFlowElabIdent();
  if (otherFlowElabIdent == NULL)
    return -1;

  // Both are flowElab idents, compare them. There is no good compare
  // routine yet. TBD - write one and use it.
  //  return (int)mFlowElab - (int)otherFlowElabIdent->mFlowElab;
  size_t numFlowsMe = mFlowElabs->size();
  size_t numFlowsOther = otherFlowElabIdent->mFlowElabs->size();
  
  ptrdiff_t ret = (SIntPtr)numFlowsMe - (SIntPtr)numFlowsOther;
  for (size_t i = 0; (i < numFlowsMe) && (ret == 0); ++i)
  {
    FLNodeElab* elabMe = (*mFlowElabs)[i];
    FLNodeElab* elabOther = (*(otherFlowElabIdent->mFlowElabs))[i];
    ret = carbonPtrCompare(elabMe, elabOther);
  }
  return ret;
}

CarbonExpr::SignT ESFlowElabIdent::evaluate(ExprEvalContext*) const
{
  return eBadSign;
}

CarbonExpr::AssignStat 
ESFlowElabIdent::assign(ExprAssignContext*)
{
  CE_ASSERT("Assign not valid" == NULL, this);
  return eReadOnly;
}

CarbonExpr::AssignStat 
ESFlowElabIdent::assignRange(ExprAssignContext*, const ConstantRange&)
{
  CE_ASSERT("Assign not valid" == NULL, this);
  return eReadOnly;
}

const STAliasedLeafNode* 
ESFlowElabIdent::getNode(DynBitVector* usageMask) const
{
  FLNodeElab* flElab = (*mFlowElabs)[0];
  const NUNetElab* elab = flElab->getDefNet();
  const STAliasedLeafNode* node = elab->getSymNode();
  sGetUsageMask(flElab->getFLNode(), usageMask);
  return node;
}

UInt32 ESFlowElabIdent::determineBitSize() const
{
  return mNetRef->getNumBits();
}

void ESFlowElabIdent::print(bool /* recurse */, int indent) const
{
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < indent; i++) {
    screen << " ";
  }
  screen << typeStr() << '(' << this << ") ";
  CarbonExpr::printSize(screen);
  screen << " : ";

  for (FLNodeElabVector::iterator p = mFlowElabs->begin(); p != mFlowElabs->end(); ++p)
  {
    FLNodeElab* elab = *p;
    NUNetElab* netElab = elab->getDefNet();
    screen << netElab->getHier()->str() << "." 
           << netElab->getNet()->getName()->str() << UtIO::endl;
  }
}  

void ESFlowElabIdent::composeIdent(ComposeContext* context) const
{
  UtString* buf = context->getBuffer();

  // For now we are picking the first flow node. We should fix this.
  FLNodeElabVector::iterator p = mFlowElabs->begin();
  FLNodeElab* flow = *p;

  // Print the net and bits
  *buf << flow->getName()->str();
  composeNetBits(buf, getNetRef(), flow->getDefNet()->getNet());
}  
