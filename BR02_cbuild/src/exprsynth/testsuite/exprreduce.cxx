// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "exprsynth/ExprFactory.h"
#include "exprsynth/ExprReduce.h"
#include "exprsynth/SymTabExpr.h"

#include "symtab/STSymbolTable.h"
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STFieldBOM.h"

#include "hdl/HdlVerilogPath.h"
#include "hdl/HdlId.h"
#include "util/AtomicCache.h"
#include "util/ConstantRange.h"

// This tests the ExprReduce class in exprsynth

static int errors = 0;
static ESFactory* sExprFactory = NULL;
static STSymbolTable* sSymTab = NULL;

#define FAIL(stmt)	\
    do {UtIO::cout() << stmt << UtIO::endl; errors++;} while (0)

static STAliasedLeafNode* sGetNode(const char* path)
{
  HdlHierPath::Status stat;
  HdlId info;
  STSymbolTableNode* node = sSymTab->getNode(path, &stat, &info);
  INFO_ASSERT(node, path);
  STAliasedLeafNode* ret = node->castLeaf();
  ST_ASSERT(ret, node);
  return ret;
}

static CarbonIdent* sGetIdent(const char* path, UInt32 width)
{
  SymTabIdent* ident = new SymTabIdent(sGetNode(path), width);
  if (width > 1) {
    ConstantRange range(width - 1, 0);
    ident->putDeclaredRange(&range);
  }
  
  bool added;
  CarbonIdent* identF = sExprFactory->createIdent(ident, added);
  if (! added)
    delete ident;
  return identF;
}

static void sTestNonReduce()
{
  ExprReduce reducer (sExprFactory, NULL);

  // Simple Ident
  CarbonIdent* a = sGetIdent("top.a", 1);
  CarbonExpr* aReduce = reducer.reduce(a);
  if (aReduce != a)
    FAIL("Reduced simple ident.");

  // simple bitsel
  CarbonIdent* b = sGetIdent("top.b", 4);
  CarbonConst* one = sExprFactory->createConst(1, 32, false);
  CarbonBinaryOp* b1= sExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, 
                                                   b, one, 1, false, false);
  CarbonExpr* b1Reduce = reducer.reduce(b1);
  if (b1Reduce != b1)
    FAIL("Reduced simple bitsel");


  // simple partsel
  ConstantRange simpleRange(3, 2);
  CarbonPartsel* b3_2= sExprFactory->createPartsel(b, simpleRange, 2, false);
  CarbonExpr* b3_2Reduce = reducer.reduce(b3_2);
  if (b3_2Reduce != b3_2)
    FAIL("Reduced simple partsel");
  
  // simple concat 
  CarbonExprVector concatExprs;
  concatExprs.push_back(b);
  concatExprs.push_back(a);

  CarbonConcatOp* b_a = sExprFactory->createConcatOp(&concatExprs, 1, 5, false);
  CarbonExpr* b_aReduce = reducer.reduce(b_a);
  if (b_aReduce != b_a)
    FAIL("Reduced simple concat");
  
}


static void sTestBinary()
{
  ExprReduce reducer (sExprFactory, NULL);

  // bitsel of bitsel scalar
  CarbonIdent* a = sGetIdent("top.a", 1);  
  CarbonConst* zero = sExprFactory->createConst(0, 32, false);
  CarbonBinaryOp* a0 = sExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, 
                                                    a, zero, 1, false, false);
  CarbonExpr* a0Reduce = reducer.reduce(a0);
  if (a0Reduce != a)
    FAIL("Failed to reduce bitsel of bitsel scalar");

  // bitsel of a constant
  CarbonConst* two = sExprFactory->createConst(2, 32, false);
  CarbonConst* one = sExprFactory->createConst(1, 32, false);
  CarbonBinaryOp* two_1 = sExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, 
                                                       two, one, 1, false, false);
  CarbonConst* oneBit = sExprFactory->createConst(1, 1, false);
  CarbonExpr* two_1Reduce = reducer.reduce(two_1);
  if (two_1Reduce != oneBit)
    FAIL("Failed to reduce bitsel of constant");
  

  // bitsel of bitsel vector
  CarbonIdent* b = sGetIdent("top.b", 4);
  CarbonBinaryOp* b1= sExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, 
                                                   b, one, 1, false, false);
  CarbonBinaryOp* b1bit = sExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, 
                                                       b1, zero, 1, false, false);
  CarbonExpr* b1bitReduce = reducer.reduce(b1bit);
  if (b1bitReduce != b1)
    FAIL("Failed to reduce bitsel of bitsel vector");
  
  // bitsel of partsel
  CarbonBinaryOp* b2 = sExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, 
                                                     b, two, 1, false, false);
  
  ConstantRange simpleRange(3, 1);
  CarbonPartsel* b3_1= sExprFactory->createPartsel(b, simpleRange, 3, false);
  CarbonBinaryOp* b3_1_1 = sExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, 
                                                        b3_1, one, 1, false, false);
  CarbonExpr* b3_1_1Reduce = reducer.reduce(b3_1_1);
  if (b3_1_1Reduce != b2)
    FAIL("Failed to reduce b[3:1][1]");
  
  
  // bitsel of concat
  CarbonExprVector concatExprs;
  concatExprs.push_back(b3_1);
  concatExprs.push_back(a);
  CarbonConcatOp* b3_1_a = sExprFactory->createConcatOp(&concatExprs, 1, 4, false);
  CarbonBinaryOp* b3_1_a_2 = sExprFactory->createBinaryOp(CarbonExpr::eBiBitSel,
                                                          b3_1_a, two, 1, false, false);
  CarbonExpr* b3_1_a_2Reduce = reducer.reduce(b3_1_a_2);
  if (b3_1_a_2Reduce != b2)
    FAIL("Failed to reduce {b[3:1],a}[2]");
}

static void sTestPartsel()
{
  ExprReduce reducer (sExprFactory, NULL);

  // whole identifier partsel
  CarbonIdent* b = sGetIdent("top.b", 4);
  ConstantRange bRange(3, 0);
  CarbonPartsel* b3_0= sExprFactory->createPartsel(b, bRange, 4, false);
  CarbonExpr* b3_0Reduce = reducer.reduce(b3_0);
  if (b3_0Reduce != b)
    FAIL("Failed to reduce b[3:0]");
  
  // Partsel of a constant
  CarbonConst* fourteen = sExprFactory->createConst(14, 32, false);
  ConstantRange range1(3,1);
  CarbonPartsel* fourteen3_1= sExprFactory->createPartsel(fourteen, range1, 3, false);
  CarbonConst* sevenMin = sExprFactory->createConst(7, 3, false);
  CarbonExpr* fourteen3_1Reduce = reducer.reduce(fourteen3_1);
  if (fourteen3_1Reduce != sevenMin)
    FAIL("Failed to reduce 'd14[3:1]");
  
  // Partsel of a bitsel
  CarbonConst* two = sExprFactory->createConst(2, 32, false);
  CarbonBinaryOp* b2 = sExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, 
                                                    b, two, 1, false, false);
  ConstantRange zeroRange(0,0);
  CarbonPartsel* b2__00 = sExprFactory->createPartsel(b2, zeroRange, 1, false);
  CarbonExpr* b2__00Reduce = reducer.reduce(b2__00);
  if (b2__00Reduce != b2)
    FAIL("Failed to reduce b[2][0:0]");

  // Partsel of a partsel
  ConstantRange offsetRange(3, 1);
  CarbonPartsel* b3_1 = sExprFactory->createPartsel(b, offsetRange,  3, false);
  ConstantRange subOffset(1,0);
  CarbonPartsel* b3_1__1_0 = sExprFactory->createPartsel(b3_1, subOffset, 2, false);
  CarbonExpr* b3_1__1_0Reduce = reducer.reduce(b3_1__1_0);
  ConstantRange range2_1(2, 1);
  CarbonPartsel* b2_1 = sExprFactory->createPartsel(b, range2_1, 2, false);
  if (b3_1__1_0Reduce != b2_1)
    FAIL("Failed to reducde b[3:1][1:0]");

  // Partsel of a concat
  // First, a whole partsel of a concat
  ConstantRange simpleRange(3, 2);
  CarbonPartsel* b3_2= sExprFactory->createPartsel(b, simpleRange, 2, false);
  CarbonIdent* a = sGetIdent("top.a", 1);
  CarbonExprVector concatExprs;
  concatExprs.push_back(b3_2);
  concatExprs.push_back(a);
  ConstantRange fullRange(2, 0);
  CarbonConcatOp* b3_2_a = sExprFactory->createConcatOp(&concatExprs, 1, 3, false);
  CarbonPartsel* b3_2_a__2_0 = sExprFactory->createPartsel(b3_2_a, fullRange, 3, false);
  CarbonExpr* b3_2_a__2_0Reduce = reducer.reduce(b3_2_a__2_0);
  if (b3_2_a__2_0Reduce != b3_2_a)
    FAIL("Failed to reduce {b[3:2], a}[2:0]");
  
  // partsel of a concat
  concatExprs.clear();
  CarbonIdent* c = sGetIdent("top.c", 7);
  concatExprs.push_back(c);
  concatExprs.push_back(b3_2);
  concatExprs.push_back(a);
  CarbonConcatOp* c_b3_2_a = sExprFactory->createConcatOp(&concatExprs, 1, 10, false);
  ConstantRange range2(4, 2);
  CarbonPartsel* c_b3_2_a__4_2 = sExprFactory->createPartsel(c_b3_2_a, range2, 3, false);

  CarbonExpr* c_b3_2_a__4_2Reduce = reducer.reduce(c_b3_2_a__4_2);

  ConstantRange range3(1, 0);
  CarbonPartsel* c1_0 = sExprFactory->createPartsel(c, range3, 2, false);
  CarbonConst* three = sExprFactory->createConst(3, 32, false);
  CarbonBinaryOp* b3 = sExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, b, three, 1, false, false);
  concatExprs.clear();
  concatExprs.push_back(c1_0);
  concatExprs.push_back(b3);
  CarbonConcatOp* c1_0_b3 = sExprFactory->createConcatOp(&concatExprs, 1, 3, false);
  
  if (c_b3_2_a__4_2Reduce != c1_0_b3)
    FAIL("Failed to reduce {c, b[3:2], a}[4:2]");
  
}

int main()
{
  // setup
  ESFactory exprFactory;
  sExprFactory = &exprFactory;

  AtomicCache atomicCache;
  STEmptyFieldBOM emptyBOM;
  HdlVerilogPath vlogPath;
  STSymbolTable symTab(&emptyBOM, &atomicCache);
  symTab.setHdlHier(&vlogPath);
  
  sSymTab = &symTab;

  // Setup some identifiers
  STBranchNode* top = symTab.createBranch(atomicCache.intern("top"),
                                          NULL);
  symTab.createLeaf(atomicCache.intern("a"), top);
  symTab.createLeaf(atomicCache.intern("b"), top);
  symTab.createLeaf(atomicCache.intern("c"), top);
  
  // run
  sTestNonReduce();
  sTestBinary();
  sTestPartsel();
  return errors;
}
