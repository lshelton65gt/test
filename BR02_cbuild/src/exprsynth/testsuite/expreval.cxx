// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "exprsynth/Expr.h"

static int errors = 0;

#define FAIL(stmt)	\
    do {UtIO::cout() << stmt << UtIO::endl; errors++;} while (0)

#define UNARYOP(var, type, expr, size)        CarbonUnaryOp var(type, expr, size, false)
#define PARTSEL(var, expr, range, size)       CarbonPartsel var(expr, range, size, false)
#define BINARYOP(var, type, expr1, expr2, size)   CarbonBinaryOp var(type, expr1, expr2, size, false, false)
#define SBINARYOP(var, type, expr1, expr2, size)   CarbonBinaryOp var(type, expr1, expr2, size, false, true)
#define TERNARYOP(var, expr1, expr2, expr3, size)   CarbonTernaryOp var(CarbonExpr::eTeCond, expr1, expr2, expr3, size, false, false)
#define CONCATOP(var, exprvec, count, size)   CarbonConcatOp var(exprvec, count, size, false)

class MyIdent : public CarbonIdent
{
public:
  MyIdent(const DynBitVector& value, const DynBitVector& drive, SignT sign, 
          UInt32 size) :
    CarbonIdent(sign != eUnsigned, size), mVal(value), mDrv(drive), mSign(sign)
  {
  }

  virtual ~MyIdent() {}
  
  virtual const STAliasedLeafNode* getNode(DynBitVector*) const 
  {
    return NULL;
  }

  virtual ptrdiff_t compare(const CarbonExpr* other) const {
    const MyIdent* otherId = static_cast<const MyIdent*>(other);
    assert(otherId);
    if (mVal < otherId->mVal)
      return(-1);
    else if (mVal.isGreaterThan(otherId->mVal))
      return (1);
    else 
    {
      if (mDrv < otherId->mDrv)
        return (-1);
      else if (mDrv.isGreaterThan(otherId->mDrv))
        return (1);
      
      return (0);
    }
  }

  virtual const char* typeStr() const {
    return "MyIdent";
  }
  
  virtual UInt32 determineBitSize() const
  {
    return mVal.size();
  }
  
  virtual void print(bool, int) const
  {
    UtOStream& screen = UtIO::cout();
    screen << typeStr() << '(' << this << ") ";
    CarbonExpr::printSize(screen);
    screen << UtIO::endl;
  }

  virtual void composeIdent(ComposeContext* context) const
  {
    UtString* buf = context->getBuffer();
    UtOStringStream ss(buf);
    ss << typeStr() << '(' << this << ')';
  }
  
  virtual SignT evaluate(ExprEvalContext* evalContext) const
  {
    DynBitVector* value = evalContext->getValue();
    DynBitVector* drive = evalContext->getDrive();
    UInt32 size = getBitSize();
    value->resize(size);
    drive->resize(size);
    *value = mVal;
    *drive = mDrv;
    value->resize(size);
    drive->resize(size);
    return mSign;
  }

  virtual AssignStat assign(ExprAssignContext*)
  {
    return eReadOnly;
  }

  virtual AssignStat assignRange(ExprAssignContext*, const ConstantRange&)
  {
    return eReadOnly;
  }

  virtual bool isWholeIdentifier() const { return true; }
  
  DynBitVector mVal;
  DynBitVector mDrv;
  SignT mSign;
};



static void sTestIdentAndConst()
{
  // don't test the ident too much, since it is
  // implementation-specific
  ExprEvalContext evalContext;
  DynBitVector& val = evalContext.getValueRef();
  DynBitVector& drv = evalContext.getDriveRef();
  val.resize(32);
  drv.resize(32);
  val = 1;
  drv = 0;
  MyIdent a(val, drv, CarbonExpr::eNeg, 16);

  val.reset();
  drv.set();
  CarbonExpr* b = &a;
  CarbonExpr::SignT result = b->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) ||
      (val != 1) || (drv != 0) ||
      (val.size() != 16) || (drv.size() != 16))
    FAIL ("Ident eval failed.\n");
  
  // Consts are simple, just like the ident
  val = 10;
  drv = 1;
  CarbonConstXZ c (val, drv, CarbonExpr::eUnsigned, 5);
  b = &c;
  val.set();
  drv.reset();
  result = b->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) ||
      (val != 10) || (drv != 1) ||
      (val.size() != 5) || (drv.size() != 5))
    FAIL ("ConstXZ eval failed.\n");

  val = 5;
  CarbonConst d (val, CarbonExpr::ePos, 20);
  b = &d;
  result = b->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) ||
      (val != 5) || (val.size() != 20) ||
      (drv != 0) || (drv.size() != 20))
    FAIL ("Const eval failed.\n");
  
}

static void sTestUnary()
{
  ExprEvalContext evalContext;
  DynBitVector& val = evalContext.getValueRef();
  val.resize(20);
  DynBitVector& drv = evalContext.getDriveRef();
  drv.resize(20);

  val = 100;
  drv = 0;
  MyIdent a(val, drv, CarbonExpr::eNeg, 20);
  
  MyIdent b(val, drv, CarbonExpr::ePos, 20);

  MyIdent c(val, drv, CarbonExpr::eUnsigned, 20);

  UNARYOP(opBufA, CarbonExpr::eUnBuf, &a, a.getBitSize());
  UNARYOP(opBufB, CarbonExpr::eUnBuf, &b, b.getBitSize());
  UNARYOP(opBufC, CarbonExpr::eUnBuf, &c, c.getBitSize());
  UNARYOP(opBitNegA, CarbonExpr::eUnBitNeg, &opBufA, opBufA.getBitSize());
  UNARYOP(opBitNegB, CarbonExpr::eUnBitNeg, &opBufB, opBufB.getBitSize());
  UNARYOP(opBitNegC, CarbonExpr::eUnBitNeg, &opBufC, opBufC.getBitSize());
  UNARYOP(opBitNegD, CarbonExpr::eUnBitNeg, &opBufA, 64);
  // starting with a negative, ends up positive
  CarbonExpr* expr = &opBitNegA;
  CarbonExpr::SignT result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 99) || (drv != 0) ||
      (val.size() != 20) || (drv.size() != 20))
    FAIL ("BitNeg with UnaryBuf neg failed.\n");

  // Starting with a positive, ends up negative
  expr = &opBitNegB;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) ||
      (val != 101) || (drv != 0) ||
      (val.size() != 20) || (drv.size() != 20))
    FAIL ("BitNeg with UnaryBuf pos failed.\n");

  // Starting with unsigned, ends up unsigned
  expr = &opBitNegC;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) ||
      (val != 0xfff9b) || (drv != 0) ||
      (val.size() != 20) || (drv.size() != 20))
    FAIL ("BitNeg with UnaryBuf unsigned failed.\n");
  
  UNARYOP(opUnMinusA, CarbonExpr::eUnMinus, &opBitNegA, opBitNegA.getBitSize());
  expr = &opUnMinusA;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) ||
      (val != 99) || (drv != 0) ||
      (val.size() != 20) || (drv.size() != 20))
    FAIL ("UnMinus pos failed.\n");

  val.resize(32);
  val = 1;
  CarbonConst ca(val, CarbonExpr::eUnsigned, 32);
  //ca.resize(32);

  UNARYOP(opUnMinusCA, CarbonExpr::eUnMinus, &ca, 32);
  expr = &opUnMinusCA;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) ||
      (val != 0xffffffff) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("UnMinus unsigned failed.\n");
  
  UNARYOP(opUnMinusB, CarbonExpr::eUnMinus, &opBitNegB, opBitNegB.getBitSize());
  
  expr = &opUnMinusB;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) ||
      (val != 101) || (drv != 0) ||
      (val.size() != 20) || (drv.size() != 20))
    FAIL ("UnMinus signed failed.\n");

  // Try some z's
  a.mDrv.set(0);
  b.mDrv.set(0);
  c.mDrv.set(0);
  

  expr = &opBitNegA;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 0) || (drv != 0xfffff) ||
      (val.size() != 20) || (drv.size() != 20))
    FAIL ("BitNeg with UnaryBuf neg unknown failed.\n");

  // Starting with a positive, ends up negative
  expr = &opBitNegB;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) ||
      (val != 0) || (drv != 0xfffff) ||
      (val.size() != 20) || (drv.size() != 20))
    FAIL ("BitNeg with UnaryBuf pos unknown failed.\n");

  // Starting with unsigned, ends up unsigned
  expr = &opBitNegC;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) ||
      (val != 0) || (drv != 0xfffff) ||
      (val.size() != 20) || (drv.size() != 20))
    FAIL ("BitNeg with UnaryBuf unsigned failed.\n");

  // Try a partsel
  a.mVal = 10;
  ConstantRange range(4, 0);
  PARTSEL(ps, &a, range, range.getLength());

  expr = &ps;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) ||
      (val != 0x16) || (drv != 1) ||
      (val.size() != 5) || (drv.size() != 5))
    FAIL ("Partsel w/ unknown failed.\n");

  a.mDrv = 0;
  expr = &ps;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) ||
      (val != 0x16) || (drv != 0) ||
      (val.size() != 5) || (drv.size() != 5))
    FAIL ("Partsel known failed.\n");

  PARTSEL(ps2, &b, range, range.getLength());

  b.mDrv = 0;
  b.mVal = 10;

  expr = &ps2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) ||
      (val != 10) || (drv != 0) ||
      (val.size() != 5) || (drv.size() != 5))
    FAIL ("Partsel pos failed.\n");

  // special case negative 0 for unary ~, should end up -1
  // This should never happen in practice, but no reason not to catch
  // a mistake such as this and correct it.
  b.mVal = 0;
  b.mDrv = 0;
  expr = &opBitNegB;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) ||
      (val != 1) || (drv != 0) ||
      (val.size() != 20) || (drv.size() != 20))
    FAIL ("BitNeg with UnaryBuf pos unknown failed.\n");

  expr = &opBitNegD;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) ||
      (val != 9) || (drv != 0) ||
      (val.size() != 64) || (drv.size() != 64))
    FAIL ("BitNeg with UnaryBuf pos resize failed.\n");

}

static void sTestAdd()
{
  ExprEvalContext evalContext;
  
  DynBitVector& val = evalContext.getValueRef();
  val.resize(32);
  DynBitVector& drv = evalContext.getDriveRef();
  drv.resize(32);
  val = 1;
  drv = 0;
  MyIdent a (val, drv, CarbonExpr::ePos, 32);
  MyIdent b (val,  drv,  CarbonExpr::eNeg, 32);

  BINARYOP(opPlus1, CarbonExpr::eBiPlus, &b, &a, 32);

  CarbonExpr* expr = &opPlus1;
  CarbonExpr::SignT result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiPlus eval 0 failed\n");
  
  val = 1;
  val.negate();
  drv = 0;
  MyIdent c(val,  drv,  CarbonExpr::eUnsigned, 32);

  BINARYOP(opPlus2, CarbonExpr::eBiPlus, &a, &c, 32);

  expr = &opPlus2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiPlus eval 1 failed\n");

  val = 1;
  drv = 0;
  MyIdent d (val, drv, CarbonExpr::eNeg, 32);

  BINARYOP(opPlus3, CarbonExpr::eBiPlus, &b, &d, 32);

  expr = &opPlus3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 2) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiPlus eval 2 failed\n");

  MyIdent e (val, drv, CarbonExpr::ePos, 32);

  BINARYOP(opPlus4, CarbonExpr::eBiPlus, &a, &e, 32);
  
  expr = &opPlus4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 3) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiPlus eval 3 failed\n");

  // unknown check
  // signed
  e.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiPlus eval 4 failed\n");

  // unsigned
  a.mDrv = 1;
  expr = &opPlus2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiPlus eval 5 failed\n");
  
  // overflow check - make sure size is correct
  val = 7;
  drv = 0;
  MyIdent f (val, drv, CarbonExpr::eUnsigned, 32);

  BINARYOP(opPlus5, CarbonExpr::eBiPlus, &a, &f, 3);

  expr = &opPlus5;
  a.mDrv = 0;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 3) || (drv.size() != 3))
    FAIL ("BiPlus eval 6 failed\n");


  BINARYOP(opMinus1, CarbonExpr::eBiMinus, &a, &b, 32);
  expr = &opMinus1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 2) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiMinus eval 0 failed\n");

  BINARYOP(opMinus2, CarbonExpr::eBiMinus, &b, &a, 32);
  expr = &opMinus2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 2) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiMinus eval 1 failed\n");

  
  BINARYOP(opMinus3, CarbonExpr::eBiMinus, &f, &a, 32);
  expr = &opMinus3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 6) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiMinus eval 2 failed\n");

  BINARYOP(opMinus4, CarbonExpr::eBiMinus, &a, &f, 16);
  expr = &opMinus4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0xfffa) || (drv != 0) ||
      (val.size() != 16) || (drv.size() != 16))
    FAIL ("BiMinus eval 3 failed\n");
  
  a.mDrv = 1;
  b.mDrv = 1;
  expr = &opMinus2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiMinus eval unknown 0 failed\n");

}

static void sTestMult()
{
  ExprEvalContext evalContext;
  DynBitVector& val = evalContext.getValueRef();
  val.resize(8);
  DynBitVector& drv = evalContext.getDriveRef();
  drv.resize(8);

  val = 1;
  drv = 0;
  MyIdent a(val, drv, CarbonExpr::ePos, 8);
  
  BINARYOP(opMult1, CarbonExpr::eBiUMult, &a, &a, 32);

  CarbonExpr* expr = &opMult1;
  CarbonExpr::SignT result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUMult eval 0 failed\n");

  MyIdent b (val, drv, CarbonExpr::eNeg, 8);
  BINARYOP(opMult2, CarbonExpr::eBiUMult, &a, &b, 32);
  expr = &opMult2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUMult eval 1 failed\n");

  val = 0;
  MyIdent c (drv, val, CarbonExpr::ePos, 8);
  
  BINARYOP(opMult3, CarbonExpr::eBiUMult, &b, &c, 32);
  expr = &opMult3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUMult eval 2 failed\n");

  val = 2;
  drv = 0;
  MyIdent d (val, drv, CarbonExpr::eUnsigned, 8);
  BINARYOP(opMult4, CarbonExpr::eBiUMult, &d, &c, 32);
  expr = &opMult4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUMult eval 3 failed\n");
  

  BINARYOP(opMult5, CarbonExpr::eBiUMult, &d, &b, 32);
  expr = &opMult5;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0xfffffffe) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUMult eval 4 failed\n");
  
  d.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUMult eval 5 failed\n");

  b.mDrv = 1;
  expr = &opMult2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUMult eval 6 failed\n");
}

static void sTestSignedMult()
{
  ExprEvalContext evalContext;
  DynBitVector& val = evalContext.getValueRef();
  val.resize(8);
  DynBitVector& drv = evalContext.getDriveRef();
  drv.resize(8);

  val = 1;
  drv = 0;
  MyIdent a(val, drv, CarbonExpr::ePos, 8);
  
  SBINARYOP(opMult1, CarbonExpr::eBiSMult, &a, &a, 32);

  CarbonExpr* expr = &opMult1;
  CarbonExpr::SignT result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSMult eval 0 failed\n");

  MyIdent b (val, drv, CarbonExpr::eNeg, 8);
  SBINARYOP(opMult2, CarbonExpr::eBiSMult, &a, &b, 32);
  expr = &opMult2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSMult eval 1 failed\n");

  val = 0;
  MyIdent c (drv, val, CarbonExpr::ePos, 8);
  
  SBINARYOP(opMult3, CarbonExpr::eBiSMult, &b, &c, 32);
  expr = &opMult3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSMult eval 2 failed\n");

  val = 2;
  drv = 0;
  MyIdent d (val, drv, CarbonExpr::eUnsigned, 8);
  SBINARYOP(opMult4, CarbonExpr::eBiSMult, &d, &c, 32);
  expr = &opMult4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSMult eval 3 failed\n");
  

  SBINARYOP(opMult5, CarbonExpr::eBiSMult, &d, &b, 32);
  expr = &opMult5;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0xfffffffe) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSMult eval 4 failed\n");
  
  d.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSMult eval 5 failed\n");

  b.mDrv = 1;
  expr = &opMult2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSMult eval 6 failed\n");
}

static void sTestDiv()
{
  ExprEvalContext evalContext;
  DynBitVector& val = evalContext.getValueRef();
  val.resize(10);
  DynBitVector& drv = evalContext.getDriveRef();
  drv.resize(10);

  val = 1;
  drv = 0;
  MyIdent a(val, drv, CarbonExpr::ePos, 8);
  
  BINARYOP(opDiv1, CarbonExpr::eBiUDiv, &a, &a, 32);

  CarbonExpr* expr = &opDiv1;
  CarbonExpr::SignT result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUDiv eval 0 failed\n");

  MyIdent b(val, drv, CarbonExpr::eNeg, 8);
  BINARYOP(opDiv2, CarbonExpr::eBiUDiv, &a, &b, 32);
  expr = &opDiv2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUDiv eval 1 failed\n");

  BINARYOP(opDiv3, CarbonExpr::eBiUDiv, &b, &b, 32);
  expr = &opDiv3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUDiv eval 2 failed\n");
  

  a.mVal = 0;
  expr = &opDiv2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUDiv eval 3 failed\n");
  
  a.mVal = 10;
  b.mVal = 3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 3) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUDiv eval 4 failed\n");

  val = 2;
  MyIdent c(val, drv, CarbonExpr::eUnsigned, 8);
  BINARYOP(opDiv4, CarbonExpr::eBiUDiv, &b, &c, 32);  
  expr = &opDiv4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0xffffffff) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUDiv eval 5 failed\n");
  
  b.mDrv = 1;
  expr = &opDiv2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUDiv eval 6 failed\n");

  b.mDrv = 0;

  // test mods
  BINARYOP(opMod1, CarbonExpr::eBiUMod, &b, &c, 32);
  expr = &opMod1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUMod eval 0 failed\n");

  BINARYOP(opMod2, CarbonExpr::eBiUMod, &b, &b, 32);
  expr = &opMod2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUMod eval 1 failed\n");

  // 10%-3 = 1
  BINARYOP(opMod3, CarbonExpr::eBiUMod, &a, &b, 32);
  expr = &opMod3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUMod eval 2 failed\n");
  

  a.mVal = 1;
  BINARYOP(opMod4, CarbonExpr::eBiUMod, &c, &a, 32);
  expr = &opMod4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUMod eval 3 failed\n");
  
  a.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiUMod eval 4 failed\n");
  
}

static void sTestSignedDiv()
{
  ExprEvalContext evalContext;
  DynBitVector& val = evalContext.getValueRef();
  val.resize(10);
  DynBitVector& drv = evalContext.getDriveRef();
  drv.resize(10);

  val = 1;
  drv = 0;
  MyIdent a(val, drv, CarbonExpr::ePos, 8);
  
  SBINARYOP(opDiv1, CarbonExpr::eBiSDiv, &a, &a, 32);

  CarbonExpr* expr = &opDiv1;
  CarbonExpr::SignT result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSDiv eval 0 failed\n");

  MyIdent b(val, drv, CarbonExpr::eNeg, 8);
  SBINARYOP(opDiv2, CarbonExpr::eBiSDiv, &a, &b, 32);
  expr = &opDiv2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSDiv eval 1 failed\n");

  SBINARYOP(opDiv3, CarbonExpr::eBiSDiv, &b, &b, 32);
  expr = &opDiv3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSDiv eval 2 failed\n");
  

  a.mVal = 0;
  expr = &opDiv2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSDiv eval 3 failed\n");
  
  a.mVal = 10;
  b.mVal = 3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 3) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSDiv eval 4 failed\n");

  val = 2;
  MyIdent c(val, drv, CarbonExpr::eUnsigned, 8);
  SBINARYOP(opDiv4, CarbonExpr::eBiSDiv, &b, &c, 32);  
  expr = &opDiv4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0xffffffff) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSDiv eval 5 failed\n");
  
  b.mDrv = 1;
  expr = &opDiv2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSDiv eval 6 failed\n");

  b.mDrv = 0;

  // test mods
  SBINARYOP(opMod1, CarbonExpr::eBiSMod, &b, &c, 32);
  expr = &opMod1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSMod eval 0 failed\n");

  SBINARYOP(opMod2, CarbonExpr::eBiSMod, &b, &b, 32);
  expr = &opMod2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSMod eval 1 failed\n");

  // 10%-3 = 1
  SBINARYOP(opMod3, CarbonExpr::eBiSMod, &a, &b, 32);
  expr = &opMod3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSMod eval 2 failed\n");
  

  a.mVal = 1;
  SBINARYOP(opMod4, CarbonExpr::eBiSMod, &c, &a, 32);
  expr = &opMod4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSMod eval 3 failed\n");
  
  a.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSMod eval 4 failed\n");
  
}

static void sTestArithmetic()
{
  sTestAdd();
  sTestMult();
  sTestSignedMult ();
  sTestDiv();
  sTestSignedDiv ();
}

static void sTestLogical()
{
  ExprEvalContext evalContext;
  DynBitVector& val = evalContext.getValueRef();
  val.resize(10);
  DynBitVector& drv = evalContext.getDriveRef();
  drv.resize(10);
  val = 1;
  drv = 0;
  MyIdent a(val, drv, CarbonExpr::ePos, 8);
  MyIdent b(val, drv, CarbonExpr::eNeg, 8);
  val = 2;
  MyIdent c(val, drv, CarbonExpr::eUnsigned, 8);
  
  BINARYOP(opEq1, CarbonExpr::eBiEq, &a, &a, 32);

  CarbonExpr* expr = &opEq1;
  CarbonExpr::SignT result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiEq eval 0 failed\n");

  BINARYOP(opEq2, CarbonExpr::eBiEq, &a, &b, 32); 
  expr = &opEq2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiEq eval 1 failed\n");

  
  BINARYOP(opEq3, CarbonExpr::eBiEq, &a, &c, 1); 
  expr = &opEq3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BiEq eval 2 failed\n");
  
  a.mDrv = 1;
  expr = &opEq1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiEq eval 3 failed\n");

  a.mDrv = 0;
  
  SBINARYOP(opLt1, CarbonExpr::eBiSLt, &a, &c, 1);
  expr = &opLt1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BiSLt eval 0 failed\n");

  BINARYOP(opULt1, CarbonExpr::eBiULt, &a, &c, 1);
  expr = &opULt1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BiULt eval 0 failed\n");

  SBINARYOP(opLt2, CarbonExpr::eBiSLt, &c, &a, 1);
  expr = &opLt2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BiSLt eval 1 failed\n");

  BINARYOP(opULt2, CarbonExpr::eBiULt, &c, &a, 1);
  expr = &opULt2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BiULt eval 1 failed\n");

  SBINARYOP(opLt3, CarbonExpr::eBiSLt, &c, &c, 32);
  expr = &opLt3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSLt eval 2 failed\n");
  
  SBINARYOP(opLt4, CarbonExpr::eBiSLt, &b, &a, 32);
  expr = &opLt4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSLt eval 3 failed\n");
  
  SBINARYOP(opLt6, CarbonExpr::eBiSLt, &b, &a, 1);
  expr = &opLt6;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BiSLt eval 4 failed\n");
  
  SBINARYOP(opLt5, CarbonExpr::eBiSLt, &a, &b, 1);
  expr = &opLt5;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BiSLt eval 5 failed\n");
  
  b.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BiSLt eval 6 failed\n");

  a.mDrv = 1;
  expr = &opLt4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiSLt eval 7 failed\n");

  BINARYOP(opULt3, CarbonExpr::eBiULt, &c, &c, 32);
  expr = &opULt3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiULt eval 2 failed\n");
  
  BINARYOP(opULt4, CarbonExpr::eBiULt, &b, &a, 32);
  expr = &opULt4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiULt eval 3 failed\n");
  
  BINARYOP(opULt6, CarbonExpr::eBiULt, &b, &a, 1);
  expr = &opULt6;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BiULt eval 4 failed\n");
  
  BINARYOP(opULt5, CarbonExpr::eBiULt, &a, &b, 1);
  expr = &opULt5;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BiULt eval 5 failed\n");
  
  b.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BiULt eval 6 failed\n");

  a.mDrv = 1;
  expr = &opLt4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiULt eval 7 failed\n");
}

static void sTestBitOps()
{
  ExprEvalContext evalContext;
  DynBitVector& val = evalContext.getValueRef();
  val.resize(10);
  DynBitVector& drv = evalContext.getDriveRef();
  drv.resize(10);
  val = 1;
  drv = 0;
  MyIdent a(val, drv, CarbonExpr::ePos, 8);
  MyIdent b(val, drv, CarbonExpr::eNeg, 8);
  val = 2;
  MyIdent c(val, drv, CarbonExpr::eUnsigned, 8);
  
  BINARYOP(opAnd1, CarbonExpr::eBiBitAnd, &a, &a, 32);
  CarbonExpr* expr = &opAnd1;
  CarbonExpr::SignT result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiAnd eval 0 failed\n");

  BINARYOP(opAnd2, CarbonExpr::eBiBitAnd, &b, &b, 32);
  expr = &opAnd2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiAnd eval 1 failed\n");

  BINARYOP(opAnd3, CarbonExpr::eBiBitAnd, &a, &b, 32);
  expr = &opAnd3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiAnd eval 2 failed\n");

  a.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiAnd eval 3 failed\n");
  a.mDrv = 0;


  BINARYOP(opAnd4, CarbonExpr::eBiBitAnd, &a, &c, 32);
  expr = &opAnd4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiAnd eval 4 failed\n");
  
  BINARYOP(opOr1, CarbonExpr::eBiBitOr, &a, &a, 32);
  expr = &opOr1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != a.mVal) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiOr eval 0 failed\n");

  BINARYOP(opOr2, CarbonExpr::eBiBitOr, &a, &b, 32);
  expr = &opOr2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiOr eval 1 failed\n");

  BINARYOP(opOr3, CarbonExpr::eBiBitOr, &b, &c, 32);
  expr = &opOr3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0xffffffff) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiOr eval 2 failed\n");

  c.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiOr eval 3 failed\n");
  c.mDrv = 0;

  BINARYOP(opXor1, CarbonExpr::eBiBitXor, &a, &a, 32);
  expr = &opXor1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiXor eval 0 failed\n");
  
  BINARYOP(opXor2, CarbonExpr::eBiBitXor, &a, &b, 32);
  expr = &opXor2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 2) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiXor eval 1 failed\n");
  
  BINARYOP(opXor3, CarbonExpr::eBiBitXor, &b, &c, 32);
  expr = &opXor3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0xfffffffd) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiXor eval 2 failed\n");
  
  // shifts
  BINARYOP(opRshift1, CarbonExpr::eBiRshift, &a, &c, 32);
  expr = &opRshift1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiRshift eval 0 failed\n");

  BINARYOP(opRshift2, CarbonExpr::eBiRshift, &c, &a, 32);
  expr = &opRshift2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiRshift eval 1 failed\n");

  BINARYOP(opRshift3, CarbonExpr::eBiRshift, &b, &c, 32);
  expr = &opRshift3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0x3fffffff) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiRshift eval 2 failed\n");

  b.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiRsshift eval 3 failed\n");
  b.mDrv = 0;
  c.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiRshift eval 4 failed\n");
  c.mDrv = 0;

  // no sign extension of right shift. (NC 0 fills)
  BINARYOP(opRshift4, CarbonExpr::eBiRshift, &b, &b, 32);
  expr = &opRshift4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiRshift eval 5 failed\n");
  
  // lshifts
  BINARYOP(opLshift1, CarbonExpr::eBiLshift, &b, &b, 32);
  expr = &opLshift1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiLshift eval 0 failed\n");

  BINARYOP(opLshift2, CarbonExpr::eBiLshift, &b, &c, 32);
  expr = &opLshift2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 4) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiLshift eval 1 failed\n");

  BINARYOP(opLshift3, CarbonExpr::eBiLshift, &a, &c, 32);
  expr = &opLshift3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 4) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiLshift eval 2 failed\n");
  
  a.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiLshift eval 3 failed\n");
  a.mDrv = 0;
  b.mDrv = 1;
  expr = &opLshift2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 0) || (drv != 0xffffffff) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiLshift eval 4 failed\n");

  BINARYOP(opLshift4, CarbonExpr::eBiLshift, &c, &a, 32);
  expr = &opLshift4;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 4) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("BiLshift eval 5 failed\n");
  
}

static void sTestBinary()
{
  sTestArithmetic();
  sTestLogical();
  sTestBitOps();

  ExprEvalContext evalContext;

  // Here, test bitselect
  DynBitVector& val = evalContext.getValueRef();
  val.resize(2);
  DynBitVector& drv = evalContext.getDriveRef();
  drv.resize(2);
  val = 0;
  drv = 0;
  MyIdent a (val, drv, CarbonExpr::ePos, 2);

  val.resize(8);
  drv.resize(8);
  val = 10;
  drv = 0;
  MyIdent b(val,  drv,  CarbonExpr::eUnsigned, 8);

  BINARYOP(opBitSelB, CarbonExpr::eBiBitSel, &b, &a, 1);

  CarbonExpr* expr = &opBitSelB;
  CarbonExpr::SignT result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BitSel eval 0 failed\n");

  a.mVal = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BitSel eval 1 failed\n");

  // unknown test on bitselect expr
  a.mDrv = 1; 
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 1) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BitSel eval Z bs failed\n");

  a.mDrv = 0;
  a.mVal = 0;
  b.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0) || (drv != 1) ||
      (val.size() != 1) || (drv.size() != 1))
    FAIL ("BitSel eval Z val failed\n");
}

static void sTestTernary()
{
  ExprEvalContext evalContext;
  DynBitVector& val = evalContext.getValueRef();
  val.resize(20);
  DynBitVector& drv = evalContext.getDriveRef();
  drv.resize(20);
  val = 0;
  drv = 0;
  MyIdent a (val, drv, CarbonExpr::ePos, 20);
  MyIdent zero (val, drv, CarbonExpr::ePos, 20);
  MyIdent b (val, drv, CarbonExpr::ePos, 5);
  MyIdent c (val, drv, CarbonExpr::eNeg, 10);
  MyIdent d (val, drv, CarbonExpr::eUnsigned, 20);
  b.mVal = 1;
  c.mVal = 2;
  d.mVal = 3;
  
  BINARYOP(opBiEq, CarbonExpr::eBiEq, &a, &zero, 1);
  TERNARYOP(opTern1, &opBiEq, &b, &c, 15);
  TERNARYOP(opTern2, &opBiEq, &c, &d, 19);
  TERNARYOP(opTern3, &opBiEq, &d, &b, 20);

  CarbonExpr* expr = &opTern1;
  CarbonExpr::SignT result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::ePos) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 15) || (drv.size() != 15))
    FAIL ("Ternary eval 0 failed\n");

  a.mVal = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 2) || (drv != 0) ||
      (val.size() != 15) || (drv.size() != 15))
    FAIL ("Ternary eval 1 failed\n");

  a.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eNeg) || 
      (val != 2) || (drv != 0) ||
      (val.size() != 15) || (drv.size() != 15))
    FAIL ("Ternary eval 2 failed\n");
  
  a.mDrv = 0;
  a.mVal = 0;
  expr = &opTern2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0x7fffe) || (drv != 0) ||
      (val.size() != 19) || (drv.size() != 19))
    FAIL ("Ternary eval 3 failed\n");

  a.mVal = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 3) || (drv != 0) ||
      (val.size() != 19) || (drv.size() != 19))
    FAIL ("Ternary eval 4 failed\n");

  a.mVal = 0;
  a.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 3) || (drv != 0) ||
      (val.size() != 19) || (drv.size() != 19))
    FAIL ("Ternary eval 5 failed\n");

  a.mDrv = 0;
  a.mVal = 0;
  expr = &opTern3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 3) || (drv != 0) ||
      (val.size() != 20) || (drv.size() != 20))
    FAIL ("Ternary eval 6 failed\n");

  a.mVal = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 20) || (drv.size() != 20))
    FAIL ("Ternary eval 7 failed\n");
  
  a.mVal = 0;
  a.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 1) || (drv != 0) ||
      (val.size() != 20) || (drv.size() != 20))
    FAIL ("Ternary eval 8 failed\n");
}

static void sTestConcat()
{
  ExprEvalContext evalContext;
  DynBitVector& val = evalContext.getValueRef();
  val.resize(20);
  DynBitVector& drv = evalContext.getDriveRef();
  drv.resize(20);
  val = 0;
  drv = 0;
  MyIdent a (val, drv, CarbonExpr::ePos, 20);
  MyIdent zero (val, drv, CarbonExpr::ePos, 20);
  MyIdent b (val, drv, CarbonExpr::ePos, 5);
  MyIdent c (val, drv, CarbonExpr::eNeg, 12);
  MyIdent d (val, drv, CarbonExpr::eUnsigned, 4);
  a.mVal = 1;
  b.mVal = 1;
  c.mVal = 2;
  d.mVal = 3;

  CarbonExprVector vec;
  vec.push_back(&a);
  vec.push_back(&b);
  CONCATOP(opConcat1, &vec, 1, 32);
  
  CarbonExpr* expr = &opConcat1;
  CarbonExpr::SignT result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 33) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("Concat eval 0 failed\n");

  a.mDrv = 0xf;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 33) || (drv != 0x1e0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("Concat eval 1 failed\n");

  vec.clear();
  vec.push_back(&c);
  vec.push_back(&d);
  CONCATOP(opConcat2, &vec, 1, 32);
  expr = &opConcat2;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0xffe3) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("Concat eval 2 failed\n");

  vec.clear();
  vec.push_back(&d);
  CONCATOP(opConcat3, &vec, 4, 32);
  expr = &opConcat3;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0x3333) || (drv != 0) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("Concat eval 3 failed\n");

  d.mDrv = 1;
  result = expr->evaluate(&evalContext);
  if ((result != CarbonExpr::eUnsigned) || 
      (val != 0x3333) || (drv != 0x1111) ||
      (val.size() != 32) || (drv.size() != 32))
    FAIL ("Concat eval 4 failed\n");
}

int main()
{
  sTestIdentAndConst();
  sTestUnary();
  sTestBinary();
  sTestTernary();
  sTestConcat();
  return errors;
}
