# Rules to make libcarbonaux.dll.

project(CarbonAux)

add_library(carbonaux SHARED
            ${CMAKE_SOURCE_DIR}/shell/libcarbonaux.def
            ${FLEXLM_LIBDIR}/lm_new.c
            )

target_link_libraries(carbonaux
                      ${CARBON_LICENSE_LIBS}
                      ${FSDB_LIB}
                      wsock32.lib
                      comctl32.lib
                      netapi32.lib
                      )

set_target_properties(carbonaux PROPERTIES
                      PREFIX ../lib
                      IMPORT_PREFIX ../lib
                      IMPORT_SUFFIX .dll.lib
                      RUNTIME_OUTPUT_DIRECTORY ${CARBON_OBJ}/shell
                      ARCHIVE_OUTPUT_DIRECTORY ${CARBON_OBJ}/shell
                      )

# We need to make sure the shell directory exists before the library is built
add_custom_command(TARGET carbonaux PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -E make_directory ${CARBON_OBJ}/shell
                   )
