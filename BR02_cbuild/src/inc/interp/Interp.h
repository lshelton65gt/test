// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include <cstddef>	// For NULL
#include "util/CarbonTypes.h"

// #define INTERP_PROFILE 1

// #define INTERP_BYTE_STREAM 1

#ifdef INTERP_BYTE_STREAM
typedef UInt8 InterpInstruction;
#else
typedef UInt32 InterpInstruction;
#endif

class UtOStream;
class UtString;
class ConstantRange;
class NUDesign;
class STSymbolTable;
class SCHSchedule;
class MsgContext;
class IODBNucleus;
class AtomicCache;
class ArgProc;
class NUNetRefFactory;
class SourceLocatorFactory;

#if 0
class InterpreterMemory
{
public:
  Memory() {}
  virtual ~Memory() {}

  virtual UInt32 read32(UInt32) = 0;
  virtual UInt64 read64(UInt32) = 0;
  virtual UInt32 read32(UInt64) = 0;
  virtual UInt64 read64(UInt64) = 0;

  virtual void write(UInt32, UInt32) = 0;
  virtual void write(UInt64, UInt32) = 0;
  virtual void write(UInt32, UInt64) = 0;
  virtual void write(UInt64, UInt64) = 0;
};
#endif

class Interpreter
{
public:
  enum Register {
    eRegA32,
    eRegA64,
    eRegB32,
    eRegB64,
    eRegMax = eRegB64
  };

  enum NetStorageMode {
    eNetBlockLocal,
    eNetReference,
    eNetStorage
  };

  enum OpCode {
    eInvalid,                   // invalidate op-code 0 to catch codegen errors

    eFetch8A32,
    eFetch16A32,
    eFetch32A32,
    eFetch64A32,

    eFetchRef8A32,
    eFetchRef16A32,
    eFetchRef32A32,
    eFetchRef64A32,

    eFetch8B32,
    eFetch16B32,
    eFetch32B32,
    eFetch64B32,

    eFetchRef8B32,
    eFetchRef16B32,
    eFetchRef32B32,
    eFetchRef64B32,

    eFetch8A64,
    eFetch16A64,
    eFetch32A64,
    eFetch64A64,

    eFetchRef8A64,
    eFetchRef16A64,
    eFetchRef32A64,
    eFetchRef64A64,

    eFetch8B64,
    eFetch16B64,
    eFetch32B64,
    eFetch64B64,

    eFetchRef8B64,
    eFetchRef16B64,
    eFetchRef32B64,
    eFetchRef64B64,

    eFetchLocal32A32,
    eFetchLocal32A64,
    eFetchLocal32B32,
    eFetchLocal32B64,

    eFetchLocal64A32,
    eFetchLocal64A64,
    eFetchLocal64B32,
    eFetchLocal64B64,

      // Load constants.  Usually these wind up in the B register,
      // except for a few arithmetic operations like (1 << x)
    eConstA32,
    eConstA64,
    eConstB32,
    eConstB64,

      // We need those same operations for storing and writing
      // references, but we don't generally need to store
      // from B because our operators never write into B.

    eStore8A32,
    eStore16A32,
    eStore32A32,
    eStore64A32,
    eStore8A64,
    eStore16A64,
    eStore32A64,
    eStore64A64,

    eStoreRef8A32,
    eStoreRef16A32,
    eStoreRef32A32,
    eStoreRef64A32,
    eStoreRef8A64,
    eStoreRef16A64,
    eStoreRef32A64,
    eStoreRef64A64,

    eStoreLocal32A32,
    eStoreLocal32A64,
    eStoreLocal64A32,
    eStoreLocal64A64,


    // Binary operators range from cAdd to cPartAssign.  All arithmetic
    // and bitwise operators are in the form 'Asz = Asz op Bsz', where 'sz'
    // is 0,1,2,3, correspending to 1-byte, 2-byte, 4-byte, 8-byte
    // operands.   Boolean operators are in the form 'A0 = Asz op Bsz'.
    eAdd32,
    eSub32,
    eMod32,
    eMul32,
    eDiv32,
    eLShift32,
    eRShift32,
    eBitOr32,
    eBitAnd32,
    eBitXor32,
    eLogOr32,
    eLogAnd32,
    eLogEq32,
    eLogLt32,
    eLogLte32,
    eLogNot32,
    eBitNot32,
    eNegate32,

    eAdd64,
    eSub64,
    eMod64,
    eMul64,
    eDiv64,
    eLShift64,
    eRShift64,
    eBitOr64,
    eBitAnd64,
    eBitXor64,
    eLogOr64,
    eLogAnd64,
    eLogEq64,
    eLogLt64,
    eLogLte64,
    eLogNot64,
    eBitNot64,
    eNegate64,

    eMoveA32A64,
    eMoveA32B32,
    eMoveA32B64,
    eMoveA64A32,
    eMoveA64B32,
    eMoveA64B64,
    eMoveB32B64,
    eMoveB32A32,
    eMoveB32A64,
    eMoveB64B32,
    eMoveB64A32,
    eMoveB64A64,

    eConcat32,
    eConcat64,
    eIf32,
    eIf64,
    eIfChanged,
    eRelativeJump,
    eSetInstance,
    eReturn,
    eCall,

    ePushA32,
    ePushA64,
    ePushB32,
    ePushB64,
    ePopA32,
    ePopA64,
    ePopB32,
    ePopB64,

    eBitSel32,
    eBitSel64,

    eKBitSel32,
    eKBitSel64,

    eBitSet8,
    eBitSet16,
    eBitSet32,
    eBitSet64,
    eBitSetRef8,
    eBitSetRef16,
    eBitSetRef32,
    eBitSetRef64,
    eBitSetLocal32,
    eBitSetLocal64,

    ePartSel32,
    ePartSel64,
    ePartSelBA32,
    ePartSelBA64,

    ePartSet8A32,
    ePartSet16A32,
    ePartSet32A32,
    ePartSet64A32,
    ePartSetRef8A32,
    ePartSetRef16A32,
    ePartSetRef32A32,
    ePartSetRef64A32,
    ePartSetLocal32A32,
    ePartSetLocal64A32,

    ePartSet8A64,
    ePartSet16A64,
    ePartSet32A64,
    ePartSet64A64,
    ePartSetRef8A64,
    ePartSetRef16A64,
    ePartSetRef32A64,
    ePartSetRef64A64,
    ePartSetLocal32A64,
    ePartSetLocal64A64,

    eReductOr32,
    eReductOr64,
    eReductNor32,
    eReductNor64,
    eReductXor32,
    eReductXor64,
    eReductXnor32,
    eReductXnor64,

#ifdef INTERP_PROFILE
    eProfile,
#endif
    eEnd,
  };

  //static bool isNul(OpCode op) {return op == eNul;}
  //static bool isBinary(OpCode op) {return !isNul(op) && (op <= eMemAssign);}
  //static bool isUnary(OpCode op) {return (op >= eAssign) && (op <= eResize3);}
  //static bool isTernary(OpCode op) {return op == eTernary;}
  //static bool isControl(OpCode op) {return op >= eIf;}

  static bool isLogical(OpCode op);

  Interpreter();
  ~Interpreter();

  void run();

  // Population routines
  void fetch(Register reg, UInt32 offset, UInt32 netSize, NetStorageMode,
             const char* comment);
  void store(Register reg, UInt32 offset, UInt32 netSize, NetStorageMode,
             const char* comment);
  void partselAssign(Register reg, UInt32 offset, UInt32 netSize,
                     NetStorageMode, const ConstantRange&, 
                     const char* comment);
  void move(Register src, Register dst);
  void push(Register reg);
  void pop(Register reg);
  void opcode(OpCode, const char* comment = NULL);
  void const64(UInt64, Register dst);
  void const32(UInt32, Register dst);
  void instance(UInt32);
  void call(UInt32 dataOffset, UInt32 address);
  void insertReturn();
  
  static bool needsMask(OpCode);
  static bool isCommutative(OpCode);
  static UInt32 getSizeCode(UInt32 numBits); // 0: 1 byte, 1: 2 bytes, 2: 4 bytes, 3: 8 bytes
  void addTernaryOp(OpCode, UInt32 numBits, UInt32 dst,
                    UInt32 op1, UInt32 op2, UInt32 op3);

  void putCodeFile(UtOStream*);
  void putCommentFile(UtOStream*);
  void putModelData(UInt8* model, bool* changeArray);

  static bool is64BitReg(Register reg) {return (reg == eRegA64) || (reg == eRegB64);}

  UInt32 getInstructionCount() {return mInstructionCount;}

  bool loadCode(const char* filename, UtString* errmsg);

  void partselRvalue(OpCode op, const ConstantRange& range);

  class Block;

  //! To add an if statement, you must compile the then- and else- clauses
  //! into nested blocks, which are created with pushBlock.
  /*! Note -- the instruction-count will be updated as you add code to
   *! a block, but will not reach the instruction stream until you use
   *! the block in an If or Case statement, so things are temporarily
   *! inconsistent during block population.  Therefore, any entry-points
   *! established when a block has been allocated and not yet added will
   *! be invalid.
   */
  Block* pushBlock();

  //! Pop a block off the stack.
  /*! This does *not* cause the contents to be added to the instruction
   *! stream.  See addIf.
   */
  void popBlock(Block*);

  //! add an If statement to the instruction stream, using predefined
  //! Then and Else blocks.
  void addIf(bool is64Bit, Block* thenClause, Block* elseClause);

  void addIfChanged(SInt32 changedIndex, Block* clockBlock);

  void addCommentLine(const char* commentLine);
  void blockComment(const char* commentLine);
  void addInstruction(OpCode inst, UInt32 data, const char* comment = NULL,
                      bool showData = true);

#ifdef INTERP_PROFILE
  void printProfileData();
#endif

  void pushInt(UInt64 val, SInt32 numBytes, const char* label);

  static void Populate(NUDesign*, STSymbolTable* symtab,
                       SCHSchedule* sched, const char* fileRoot,
                       MsgContext* msgContext, IODBNucleus* iodb,
                       AtomicCache* atomicCache, ArgProc* args,
                       NUNetRefFactory* netRefFactory,
                       SourceLocatorFactory* sourceLocatorFactory);


private:
  void writeInstruction(InterpInstruction data);
  void bumpInstructionCount(UInt32 increment);
  void finalizeBlock(Block* block);

  UInt32* mStack32;
  UInt64* mStack64;
  UInt32* mLocal32;
  UInt64* mLocal64;
  InterpInstruction* mReturnAddress;
  InterpInstruction* mInstructions;
  UInt8* mInstance;
  UInt8* mModel;
  bool* mChangeArray;
  UtOStream* mCodeFile;
  UtOStream* mCommentFile;
  UInt32 mInstructionCount;
  UInt32 mScheduleEntry;
  Block* mCurrentBlock;
  SInt32 mCommentAddress;
  SInt32 mCommentIndent;

#ifdef INTERP_PROFILE
  UInt32* mProfileBuckets;
  UInt32 mNumProfileBuckets;
#endif
};
