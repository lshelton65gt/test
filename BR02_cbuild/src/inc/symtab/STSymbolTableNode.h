// -*- C++ -*-                
/*****************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __STSYMBOLTABLENODE_H_
#define __STSYMBOLTABLENODE_H_


#ifndef __HierName_h_
#include "util/HierName.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef __UtString_h_
#include "util/UtString.h"
#endif

/*!
  \file
  The node abstraction for the SymbolTable.
*/

class STAliasedLeafNode;
class STBranchNode;
class StringAtom;

//! Nodes of the symbol table
/*!
  This is a base class to describe symbol table nodes which are
  branches or leaves.
*/
class STSymbolTableNode : public HierName
{
public: CARBONMEM_OVERRIDES
  //! Abstraction for object data
  typedef void* BOMData;
  
  //! constructor
  STSymbolTableNode(StringAtom* name, BOMData bomData);
  
  //! virtual destructor
  virtual ~STSymbolTableNode();

  /* Specialization */
  
  //! Place the parent
  /*!
    \param parent Parents can only be branches. If NULL it is
    considered a root.
  */
  void putParent(STBranchNode* parent);

  //! Return the Parent Node read-only
  /*!
    Returns NULL if there is no parent
  */
  const STBranchNode* getParent() const;
  
  //! Return the Parent read-write
  /*!
    Returns NULL if there is no parent
  */
  STBranchNode* getParent();

  //! Return the root of this node.
  /*!
    Return the earliest non-NULL ancestor for this symbol table node.
    Returns NULL if there is no parent.
   */
  const STBranchNode* getRoot() const;

  //! Return the root of this node read-write.
  /*!
    Return the earliest non-NULL ancestor for this symbol table node.
    Returns NULL if there is no parent.
   */
  STBranchNode* getRoot();

  //! test to see if the specified node is somewhere in the parent chain
  /*!
    Returns false if 'this' has no parent or if the specified node is
    not above 'this' in the hierarchy.  Asserts if the specified node is
    the same as 'this'. Will return false if 'this' is above the specified
    node.
  */
  bool isAncestor(const STSymbolTableNode* );

  //! Cast this node as a BranchNode*.
  /*!
    \returns NULL if it is not a branch node.
  */
  STBranchNode* castBranch()
  {
    const STSymbolTableNode* me = const_cast<const STSymbolTableNode*>(this);
    return const_cast<STBranchNode*>(me->castBranch());
  }

  //! cast to constant branch node
  virtual const STBranchNode* castBranch() const;

  //! Cast this node to an STAliasedLeafNode
  /*!
    \returns NULL if it is not a leaf node
  */
  STAliasedLeafNode* castLeaf() {
    const STSymbolTableNode* me = const_cast<const STSymbolTableNode*>(this);
    return const_cast<STAliasedLeafNode*>(me->castLeaf());
  }

  //! Cast to constant leaf node
  virtual const STAliasedLeafNode* castLeaf() const;
  //! Return index of Node
  /*!
    Child nodes (branches and leaves) all have indices into arrays
    stored in branches. Every child has only 1 parent, so there is
    only 1 index per child. The index is set by the parent node.
    Therefore, every node upon creation will have a bogus index of -1.

    \returns Index of node or -1 if the index is not yet set.
  */
  SInt32 getIndex() const;

  //! Get the associated BOM data with this node
  inline BOMData getBOMData()
  {
    return mBOMData;
  }
  
  //! const version of getField()
  inline BOMData getBOMData() const
  {
    return mBOMData;
  }
  
  /* Implementation */
  
  //! Return this name string
  virtual const char* str() const;
  
  //! Return the name object
  virtual StringAtom* strObject() const {return mName;}
  
  //! Return the Parent's Name read-only
  /*!
    \returns NULL if there is no parent
  */
  virtual const HierName* getParentName() const;
  
  //! Print this path to a FILE* (by default to stdout). Used only for debugging and assertions.
  virtual void print() const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param includeRoot if true then top of hierarchy is included
           if generating a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
    \param escapeIfMultiLevel if true then resulting name will be
           escaped if a hierarchical name was generated
  */
  virtual void composeHelper(UtString*, bool includeRoot, bool hierName,
                             const char* separator, bool escapeIfMultiLevel) const;

  //! Compose the full name as a verilog path
  /*! 
    Any illegal parts of the name will be escaped.
  */
  void verilogCompose(UtString* buffer) const;

  //! Compose only the leaf of this name as a verilog identifier
  /*!
    If the identifer is an illegal name, it will be escaped.
  */
  void verilogComposeLeaf(UtString* buffer) const;

  //! Replace the current BOM data with the one passed in here.
  void putBOMData(BOMData data);

protected:
  //! The field alloced by the SymbolTable
  BOMData mBOMData;
  
  //! Index into the SymbolTable
  SInt32 mIndex;                // this index is of questionable value, and could be removed with just a bit of work
  
  //! Sets the index given by the SymbolTable
  void setIndex(STSymbolTableNode* child, SInt32 index) {
    child->mIndex = index;
  }

protected:
  //! Print node-specific description to stderr
  virtual void printInfo() const;

private:
  StringAtom* mName;
  STBranchNode* mParent;
  
  // forbid
  STSymbolTableNode(const STSymbolTableNode&);
  STSymbolTableNode& operator=(const STSymbolTableNode&);
};

#endif

