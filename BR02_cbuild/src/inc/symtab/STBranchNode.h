// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __STBRANCHNODE_H_
#define __STBRANCHNODE_H_


#ifndef __CarbonPlatform_h_
#include "util/CarbonPlatform.h"
#endif
#ifndef __STSYMBOLTABLENODE_H_
#include "symtab/STSymbolTableNode.h"
#endif
#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif

/*!
  \file
  A specialization of a STSymbolTableNode that denotes branches of a
  tree. Each full name has scopes, each scope is a branch.
*/

//! STBranchNode class to describe a scope and give access to its children
/*!
  A branch has leaves and other branches hanging off it. Leaves would
  be childless names, such as nets.

  This class allows both associative and random access to the
  children.
*/ 
class STBranchNode : public STSymbolTableNode
{
public: CARBONMEM_OVERRIDES
  //! constructor
  STBranchNode(const StringAtom* name, BOMData bomdata);

  //! virtual destructor
  virtual ~STBranchNode();

  virtual const STBranchNode* castBranch() const;

  /* specialization */

  //! Check child existence on this branch
  /*!
    \param child Leaf or branch
    \returns true if the child exists on this branch
  */
  bool isChild(const STSymbolTableNode* child) const;

  //! Check index existence on this branch
  /*!
    \param index Index of potential child in this branch's child table
    \returns true if a child exists in that table position.
  */
  bool hasChild(SInt32 index) const;
  
  //! Random access into the child table
  /*!
    \param index Index of existing child in this branch's child table
    \returns Child at index. 
  */
  STSymbolTableNode* getChild(SInt32 index);

  //! const version of getChild
  const STSymbolTableNode* getChild(SInt32 index) const;

  //! Number of children currently in the branch
  SInt32 numChildren() const;

protected:
  //! Iterator for this class
  friend class STBranchNodeIter;
  //! SymbolTable access
  /*!
    SymbolTable actually adds and removes the children, since the
    SymbolTable is the context of the hierarchy.
  */
  friend class STSymbolTable;

  //! Add a child to this branch
  /*!
    \param child Leaf or branch 
    \returns true if the child was added
  */
  void addChild(STSymbolTableNode* child);

  //! Add a child with index to this branch 
  /*!
    \param child Leaf or branch
    \param index Index into array this child should be placed
  */
  void addChildByIndex(STSymbolTableNode* child, SInt32 index);

  //! Remove a child from the parent, set the entry to NULL.
  /*!
    \param child Leaf or branch
  */
  void removeChild(STSymbolTableNode *child);

private:
  //! Print node-specific description to stderr
  virtual void printInfo() const;
  
private:
  UtArray<STSymbolTableNode*> mChildArray;
  
  // forbid
  STBranchNode(const STBranchNode&);
  STBranchNode& operator=(const STBranchNode&);
};

//! Iterator for STBranchNode class
/*!
  Allows access to the list of children. Consistent order is
  guaranteed.
*/
class STBranchNodeIter
{
public: CARBONMEM_OVERRIDES
  //! constructor with branch to be iterated
  /*!
    Since modifying access is granted STBranchNode is not const
  */
  STBranchNodeIter(STBranchNode* branch);
  
  //! Start over
  /*!
    This does \b not have to be called to start iteration. This is
    provided in case there is a need to re-start the iteration.
  */
  void begin();
  
  //! Return the next child - read-write
  /*!
    \param index Pointer to integer. Modified to reflect the index of
    the returned child.
    \returns The next child in the list or NULL if finished
  */
  STSymbolTableNode* next(SInt32* index);
  
  //! Return the next child - read-only
  const STSymbolTableNode* next(SInt32* index) const;

private:
  //! struct to encapsulate the iterator 
  struct ChildIter
  {
    UtArray<STSymbolTableNode*>::const_iterator mIter;
    const STSymbolTableNode* mKey;
  };
  
  STBranchNode* mBranch;
  mutable ChildIter mChildIter;

  // forbid
  STBranchNodeIter();
  STBranchNodeIter(const STBranchNodeIter&);
  STBranchNodeIter& operator=(const STBranchNodeIter&);
};
#endif
