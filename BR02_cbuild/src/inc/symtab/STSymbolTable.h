// -*- C++ -*-                
/*****************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __STSYMBOLTABLE_H_
#define __STSYMBOLTABLE_H_


#include "symtab/STFieldBOM.h"
#include "symtab/STSymbolTableNode.h"
#include "symtab/STBranchNode.h"
#include "symtab/STAliasedLeafNode.h"
#include "hdl/HdlHierPath.h"

#include "util/UtHashSet.h"
#include "util/UtHashMap.h"
#include "util/LoopFunctor.h"


class UtXmlWriter;

/*!
  \file
  Carbon STSymbolTable class declaration.
*/

class HierStringName;
class AtomicCache;
class HdlId;
class STNodeSelectDB;
class MsgContext;
class ZistreamDB;
class ZostreamDB;
class Zostream;
class Zistream;
class UtString;

//! Hash of symbol table nodes.
typedef UtHashSet< HierName*, HashPointerValue<HierName*> > STSymbolTableNodeSet;

//! STSymbolTable class to store name hierarchy
/*!
  "The STSymbolTable is the context that contains all the hierarchy,
  string-cache and field assignments" - JDM

  The STSymbolTable stores STSymbolTableNodes, each STSymbolTableNode
  holds a Bill Of Materials object that can be used to write and read
  to and from a database, respectively.

  The data fields (data BOMs) are only accessible through the nodes.
  However, the BOMS are owned by the STSymbolTable mainly
  because the STSymbolTable needs to interact with a BOM manager, an
  STFieldBOM in order to alloc and free each node's BOM data.

  For example, if we want to store a UInt32 and a Foo* object into
  each node, then we would have an object (data BOM object) that
  contains as members a UInt32 and a Foo*. That object will be
  allocated and freed  by the STFieldBOM that is passed in to the
  STSymbolTable upon construction. The data BOM object is opaque to
  the symboltable and to the STFieldBOM interface, but isn't to its
  implementation. This will give a very weak guarantee that there is
  type safety.

  Iterating the symboltable is done using the getNodeLoopSorted(),
  getNodeLoop(), etc. methods. The loop iterator returns an
  STSymbolTableNode* and thus there is never any reason to dynamic_cast
  an STSymbolTableNode. Once you have an STSymbolTableNode* you can
  get the leaf or the branch by calling castLeaf() or castBranch on it
  explicitly.
  At one point the iterator was returning pointers to the HierName
  base class. This has been fixed by employing a LoopFunctor.
*/
class STSymbolTable 
{
protected:
  // debug helpers
  
  //! Return the node associated with path
  /*!
    \returns the STSymbolTableNode* if the path exists or NULL
    \warning DO NOT USE IN CODE
  */
  STSymbolTableNode* dbgFindNode(const char* hierPath);

  //! Return the node associated with path, even if it is an internal node
  /*!
    This will return a node for an internally generated name. For
    example top.$flatten_data_tmp. It will also work for legal names,
    but there is no check for legality or errors. So, if you are
    looking for a legal name, use dbgFindNode().

    This function will not work if the name is escaped.

    \returns the STSymbolTableNode* if the path exists or NULL
    \warning DO NOT USE IN CODE
  */
  STSymbolTableNode* dbgFindInternal(const char* hierPath);

public: 
  CARBONMEM_OVERRIDES

  //! Type definition for a set of root nodes
  typedef UtHashSet<STSymbolTableNode*> RootSet;
  //! Iterator for Root Nodes
  typedef RootSet::UnsortedLoop RootIter;
  typedef RootSet::SortedLoop SortedRootIter;
  
  //! constructor
  /*!
    \param fieldBom Structure that allocs and frees the symboltable fields
    \param stringCache The cache of interned strings
    \warning The user of this class must supply a HdlHierPath using
    setHdlHier()
  */
  STSymbolTable(STFieldBOM* fieldBom, AtomicCache* stringCache);

  //! virtual destructor
  virtual ~STSymbolTable();

  //! Set the hdl compose/decompose object
  void setHdlHier(const HdlHierPath* hdlPath);

  //! Get the hdl path object
  const HdlHierPath* getHdlHier() const;

  //! Number of nodes in the STSymbolTable.
  SInt32 numNodes() const;
  
  //! With a name, index, and a parent create a STBranchNode
  /*!
    Creates a new STSymbolTableNode with indexed name and parent. 
    The index allows the user to add a child to the parent in a known
    order. Most apps should use the createLeaf and createBranch
    methods. This is provided for apps that must dynamically create
    nodes and the particular type of node is not known.
    
    \param childName Name of the child of parent. This function is hdl
    independent and thus any attributes of the child should not be
    included in the name; e.g., a bus, out[31:0], should be made up of
    only its identifier, out, and the attribute, [31:0] should be
    stripped.

    \param parent Parent node of child. If NULL, childName is root and
    childIndex is ignored
    \param isLeaf True if the child has no children
    \param childIndex Index into array of children in parent. If
    childIndex is -1 \b (default), then the index is set internally.
    \returns The newly created STSymbolTableNode. 
    
    \warning For the first implementation, duplicity will not be
    checked. Once a hash is established this will change.
    
    \sa createBranch, createLeaf
  */
  STSymbolTableNode* createNode(StringAtom* childName, 
                                STBranchNode* parent,
                                bool isLeaf,
                                SInt32 childIndex = -1);
  
  //! With a name, index, and a parent create a STBranchNode
  /*!
    Creates a new STBranchNode with indexed name and parent. 
    The index allows the user to add a child to the parent in a known
    order.
    
    \param childName Name of the child of parent. This function is hdl
    independent and thus any attributes of the child should not be
    included in the name; e.g., a bus, out[31:0], should be made up of
    only its identifier, out, and the attribute, [31:0] should be
    stripped.

    \param parent Parent node of child. If NULL, childName is root and
    childIndex is ignored
    \param childIndex Index into array of children in parent. If
    childIndex is -1 \b (default), then the index is set internally.
    \returns The newly created STSymbolTableNode. 
    
    \warning For the first implementation, duplicity will not be
    checked. Once a hash is established this will change.
  */
  STBranchNode* createBranch(StringAtom* childName, 
                             STBranchNode* parent,
                             SInt32 childIndex = -1);
  
  //! With a name, index, and a parent create a STSymbolTableNode
  /*!
    Creates a new STAliasedLeafNode with indexed name and parent. 
    The index allows the user to add a child to the parent in a known
    order.
    
    \param childName Name of the child of parent. This function is hdl
    independent and thus any attributes of the child should not be
    included in the name; e.g., a bus, out[31:0], should be made up of
    only its identifier, out, and the attribute, [31:0] should be
    stripped.

    \param childName Name of child node
    \param parent Parent node of child. If NULL, childName is root and
    childIndex is ignored
    \param childIndex Index into array of children in parent. If
    childIndex is -1 \b (default), then the index is set internally.
    \returns The newly created STSymbolTableNode. 
    
    \warning For the first implementation, duplicity will not be
    checked. Once a hash is established this will change.
  */
  STAliasedLeafNode* createLeaf(StringAtom* childName, 
                                STBranchNode* parent,
                                SInt32 childIndex = -1,
                                bool hasAllocation = true);
 
  //! Remove the leaf and free the memory for it.
  void destroyLeaf(STAliasedLeafNode *leaf);

  //! Given a full hierarchical path, create a leaf node
  /*!
    Duplicity checks are done.
    The path is decomposed and the necessary branches and eventual
    leaf are created.
    \warning Indexing cannot be done with this method. Indexing
    defaults to -1.
    \warning Will assert if the path points to a branch
  */
  STAliasedLeafNode* createLeafFromPath(const char* path, HdlHierPath::Status* parseStatus, HdlId* info);
                                        
  //! Translate a branch node from another table to this table
  /*!
    Duplicity checks are done.
    \warning Indexing cannot be done with this method. Indexing
    defaults to -1.
    \sa createBranch
  */
  STBranchNode* translateBranch(const STBranchNode* otherTableNode, STBranchNode *root = NULL);

  //! Translate a leaf node from another table to this table
  /*!
    Works just like translateBranch except that a leaf is created.
  */
  STAliasedLeafNode* translateLeaf(const STAliasedLeafNode* leaf, STBranchNode *root= NULL);

  //! \class LeafAssoc
  /*! When copying symbol tables for writing the various symbol databases
   *  post-copy processing of leaves is necessary. This class maintains a list
   *  of pairs of leaves. While it would be easier to use UtVector for the list
   *  operations and iterator this is not permitted because the symbol table
   *  code is used in the runtime. The ordering of the pairs matters so a hash
   *  map from src leaf to new leaf cannot be used.
   */
  class LeafAssoc {
  public:
    CARBONMEM_OVERRIDES

    LeafAssoc () : mFirst (NULL), mLast (NULL) {}
    virtual ~LeafAssoc () { clear (); }
    void clear ();

    //! Append a pair to the list
    void append (const STAliasedLeafNode *, STAliasedLeafNode *);

    class iterator;                     // forward declaration

    //! \class pair
    /*! This class represents a pair of leaf nodes. The classs is public so
      that the first and second members can be accessed via the iterator as
      it->first and it->second.
     */

    class pair {
    public:
      CARBONMEM_OVERRIDES

      // these classes need to see mNext
      friend class LeafAssoc::iterator;
      friend class LeafAssoc;

      pair (const STAliasedLeafNode *src_leaf, STAliasedLeafNode *new_leaf) :
        first (src_leaf), second (new_leaf), mNext (NULL)
      {}

      const STAliasedLeafNode *first;   //!< the first element of the pair
      STAliasedLeafNode *second;        //!< the second element of the pair

    private:
      struct pair *mNext;               //!< for chaining into lists
    };

    struct pair *mFirst;                //!< first pair in the assoc list
    struct pair *mLast;                 //!< last pair in the assoc list

  public:

    friend class iterator;              //!< needs to see mFirst and mLast

    //! \class iterator
    /*! Iteration over LeafAssoc instances.
     */

    class iterator {
    public:

      //! initialise an iterator for a LeafAssoc instance
      iterator (LeafAssoc &list) { mCurrent = list.mFirst; }

      //! \return iff we are at the end of iteration
      bool atEnd () { return mCurrent == NULL; }

      //! advance the iterator
      void operator ++ () { mCurrent = mCurrent->mNext; }

      //! \return the current member.
      /*! \note this allows access to the leaves with it->first and it->second
       */
      struct LeafAssoc::pair *operator -> () { return mCurrent; }

    private:

      struct LeafAssoc::pair *mCurrent; //!< current iteration point in the list

    };

    //! \return an iterator over this
    iterator loop () { return iterator (*this); }

  };

  //! Translate a leaf node from another table to this table
  /*! When a new leaf is translated the (old_leaf, new_leaf) pair is appended
   *  to the LeafAssoc instance.
   */

  STAliasedLeafNode* translateLeaf(const STAliasedLeafNode* leaf, LeafAssoc *,
    STBranchNode *root= NULL);
  
  //! Caster for HierNames to STSymbolTableNodes
  /*!
    This should never use a dynamic_cast the because the container
    is privately owned, and is guaranteed to be populated with
    STSymbolTableNodes. 
  */
  struct NameToNode
  {
    typedef STSymbolTableNode* value_type;
    typedef STSymbolTableNode* reference;
    
    STSymbolTableNode* operator() (HierName* name) const { 
      return (STSymbolTableNode*)(void*) name;
    }

    const STSymbolTableNode* operator() (const HierName* name) const 
    {
      return (const STSymbolTableNode*)(const void*) name;
    }

  };


  // helpers
  typedef STSymbolTableNodeSet::UnsortedLoop HierNameLoop;
  typedef STSymbolTableNodeSet::UnsortedCLoop HierNameCLoop;
  typedef STSymbolTableNodeSet::SortedLoop HierNameLoopSorted;

  //! Loop supporting iteration over symbol table nodes.
  typedef LoopFunctor<HierNameLoop, NameToNode> NodeLoop;
  //! CLoop supporting iteration over symbol table nodes.
  typedef LoopFunctor<HierNameCLoop, NameToNode> CNodeLoop;
  
  //! Sorted loop iteration over symbol tablel nodes
  typedef LoopFunctor<HierNameLoopSorted, NameToNode> NodeLoopSorted;

  //! Iterate through the nodes using the Loop interface.
  /*!
    \warning The symbol table cannot be changed while looping over the
    nodes. If it is, the iterator will become invalid and the behavior
    is undefined.
  */
  CNodeLoop getCNodeLoop() const;

  //! Loop over modifiable symboltable nodes
  NodeLoop getNodeLoop ();


  //! Sorted iteration through the nodes using the LoopSorted interface.
  /*!
    \warning The symbol table cannot be changed while looping over the
    nodes. If it is, the iterator will become invalid and the behavior
    is undefined.
  */
  NodeLoopSorted getNodeLoopSorted() const;
  
  //! Iterate through the roots. 
  /*!
    \warning The symbol table cannot be changed while iterating the
    roots. If it is, the iterator will become invalid and the behavior
    is undefined.
    \warning The iteration due to the implementation of Loop is not
    currently const hip.
  */
  RootIter getRootIter();
  
  //! Sorted iteration through the roots. 
  /*!
    \warning The symbol table cannot be changed while iterating the
    roots. If it is, the iterator will become invalid and the behavior
    is undefined.
    \warning The iteration due to the implementation of Loop is not
    currently const hip.
  */
  SortedRootIter getSortedRootIter();
  
  //! Query the table using an HDL path name
  /*!
    \param path HDL path name to lookup. The path here may have
    attributes on its identifier unlike createNode(). Attributes get
    stripped and the node associated with the identifier is returned.
    \param parseStatus Legality of path name
    \param info The identifiers information will be in this
    structure. Must \e NOT be NULL
    \param start_parent Optional, if given, is start node for lookup.
    \returns STSymbolTableNode associated with path or NULL if the path
    does not exist in the STSymbolTable or if the path is not legal.
  */
  STSymbolTableNode* getNode(const char* path, HdlHierPath::Status* parseStatus, HdlId* info, HierName *start_parent = 0);

  //! Const version of getNode
  const STSymbolTableNode* getNode(const char* path, HdlHierPath::Status* parseStatus, HdlId* info, HierName *start_parent = 0) const;

  //! Query the table using an HDL name and parent node
  /*!
    \param parent hierarchical node for the parent
    \param name name of the child you are looking for
    \returns NULL if unsuccessful, the table node if successful
  */
  STSymbolTableNode* find(const HierName* parent, StringAtom* name);
  
  //! Query the table using an HDL name and parent node
  /*!
    \param parent hierarchical node for the parent
    \param name name of the child you are looking for
    \returns NULL if unsuccessful, the table node if successful
  */
  const STSymbolTableNode* find(const HierName* parent, StringAtom* name)
    const;

  //! Query the table for a particular node name
  STSymbolTableNode* lookup(const HierName* name);

  //! Const version of a lookup
  const STSymbolTableNode* lookup(const HierName* name) const;
  
  //! A safe lookup for HierName
  /*!
    In other lookup functions based on HierName's they all assume that
    the HierName has StringAtoms that are in this SymbolTable's atomic
    cache. This function does not assume that (just as the translate
    functions do not). This function is slower than other lookups.
  */
  const STSymbolTableNode* safeLookup(const HierName* name) const;
  
  //! non-const version of a safeLookup
  STSymbolTableNode* safeLookup(const HierName* name);
  
  //! Get the atomic UtString cache
  /*!
    This is needed by apps that do not own the AtomicCache but need
    access in order to create and lookup StringAtoms.
  */
  AtomicCache* getAtomicCache();
  
  //! Write the table contents
  /*!
    The outFile database will map leaf pointers internally so that the
    indices can be used by a post leaf-write operation. Currently,
    branches are not mapped internally. That will require a ZostreamDB
    change, and it is currently not needed.

    \param outFile Valid Zostream.
    \param nodeSelect Object that contains methods to decide whether
    or not a symbol table node is to be written. If NULL, everything
    will be written
    \param writeData If true will write the data fields of the
    nodes. If false, only the node hierarchy gets written.
    \warning The caller is responsible for checking the status of the
    write.
  */
  void writeDB(ZostreamDB& outFile, STNodeSelectDB* nodeSelect,
               bool writeData);
  
  //! Read the db file.
  /*!
    The STFieldBOM passed in on construction must be compatible with
    the STFieldBOM that wrote the database. The branches and leaves
    will be read in first. Then, the preFieldRead will be done and
    finally the fields of each leaf and branch will be read in. 
    
    This function can be additive to an existing symboltable with
    nodes already in it. So, any db read by a STFieldBOM on a field
    that could already have been allocated needs to take that into
    account.  
    
    \param in Valid Zistream
    \param msgContext Error message context
    \returns eReadOK if the DB is read in successfully.
  */
  STFieldBOM::ReadStatus readDB(ZistreamDB& in,
                                MsgContext* msgContext);
  
  //! Print out contents of table - Debugging only
  void print();
  
  //! Return the STFieldBOM associated with this symtab
  const STFieldBOM* getFieldBOM() const;

  //! Modifiable form
  STFieldBOM* getFieldBOM();
  
  //! find the number of roots
  UInt32 numRoots() const;
  void setName(UtString& name);
  void writeXml(UtXmlWriter* writer);
  void writeXml(UtXmlWriter* writer, UtString& fileName);

  static void writeXml(UtXmlWriter* writer,UtArray<STSymbolTable*>& tables);
  static void writeXml(UtXmlWriter* writer,UtArray<STSymbolTable*>& tables,UtString& fileName);

private:

  UtString    mName;
  STFieldBOM* mFieldBOM;
  const HdlHierPath* mHdlPath;
  

  RootSet mRoots;
  STSymbolTableNodeSet mNodes;
  
  AtomicCache* mAtomCache;
  
  // helpers
  void writeXmlTable(UtXmlWriter* writer);
  void writeXmlBranches(UtArray<STBranchNode*>& sourceBranches,UtXmlWriter* xmlWriter);
  void writeXmlLeaves(UtArray<STAliasedLeafNode*>& sourceLeaves,UtXmlWriter* xmlWriter); 
  void writeXmlBranchNode(const STBranchNode* branchNode,UtXmlWriter* writer);
  void writeXmlBranchData(const STBranchNode* branchNode,UtXmlWriter* writer);
  void writeXmlLeafNode(const STAliasedLeafNode* leafNode,UtXmlWriter* writer);
  void writeXmlLeafData(const STAliasedLeafNode*  sourceLeaf,UtXmlWriter* xmlWriter);

  void setBOM(STFieldBOM* fieldBOM);
  void addRoot(STSymbolTableNode* root);
  STSymbolTableNode* allocNode(bool isLeaf, StringAtom* name, 
                               STBranchNode* parent);
  
  void printLeaf(STAliasedLeafNode* node, UtString& indent);
  void printBranch(STBranchNode* node, UtString& indent);
  
  STAliasedLeafNode* allocLeaf(StringAtom* name,
                               STBranchNode* parent,
                               bool hasAllocation, 
                               bool* added);
  STBranchNode* allocBranch(StringAtom* name,
                            STBranchNode* parent,
                            bool* added);
  
  void registerNode(STSymbolTableNode* node, STBranchNode* parent);
  
  void addNodeToParent(STSymbolTableNode* child, 
                       STBranchNode* parent, 
                       SInt32 childIndex);
  
  // forbid
  STSymbolTable();
  STSymbolTable(const STSymbolTable&);
  STSymbolTable& operator=(const STSymbolTable&);
  
private:
  // DB-related stuff
  
  //! DBLeaf, helper for write closure
  class DBLeaf;
  //! Write closure
  class DBWriteClosure;
  friend class DBWriteClosure;
  //! Read Closure
  class DBReadClosure;

  void writeAtomicCache(DBWriteClosure& fc);
  void writeBranchToDB(const STBranchNode* branch,
                       DBWriteClosure& fc);

  void writeLeafToDB(const STAliasedLeafNode* leaf, 
                     DBWriteClosure& fc);

  void writeAliases(DBWriteClosure& fc);
  void writeFields(DBWriteClosure& fc);

  void readSigAndVersion(DBReadClosure& rc);
  void readDate(DBReadClosure& rc);
  void readAtomicCache(DBReadClosure& rc);
  void readNodes(DBReadClosure& rc);
  
  void readCommon(DBReadClosure& rc);
  void readBranchDescr(DBReadClosure& rc);
  void readLeafDescr(DBReadClosure& rc);
  void readAliases(DBReadClosure& rc);
  void readFields(DBReadClosure& rc);
};

//! Interface for selecting particular symtab nodes
/*!
  When writing the DB, the symboltable method will call the select
  method in this interface for each aliased leaf node.
*/
class STNodeSelectDB
{
public: 
  CARBONMEM_OVERRIDES

  //! constructor
  STNodeSelectDB() {}

  //! virtual destructor
  virtual ~STNodeSelectDB() {}

  //! Returns true if the leaf is selected (for write)
  /*!
    This checks the always-select set before calling
    computeSelectLeaf().
  
    \param leaf The particular leaf node in question.
    \param hdlPath Gives the caller the ability to parse the node name
    \returns true if the leaf is selected (for writing, usually) or
    false if the node is to be skipped.

    \warning If the caller selects \e not to write out a master alias
    of a true alias ring, the caller is responsible for dealing with
    its aliases as well. Currently, if the master is not to be
    written, but all or some of its aliases are to be written, the
    next master-related alias that is iterated to will be the
    master. So, it is strongly suggested that a caller make sure that
    all slaves are not written out if the master is not going to be.
  */
  bool selectLeaf(const STAliasedLeafNode* leaf, const HdlHierPath* hdlPath);

  //! Returns true if the branch is selected (for write)
  /*!
    This checks the always-select set before calling
    computeSelectBranch()
  
    \param branch The particular branch node in question
    \param hdlPath Gives the caller the ability to parse the node name
    \returns true if the branch is selected (for writing, usually) or
    false if the branch and all its children are to be skipped.
  */
  bool selectBranch(const STBranchNode* branch, const HdlHierPath* hdlPath);

  //! Always select the given leaf.
  /*!
    Requests that true is returned from selectLeaf for this leaf.
    The selector may apply constraints that override this request.
  */
  void requestAlwaysSelectLeaf(const STAliasedLeafNode* leaf);

  //! Always select the given branch
  /*!
    Requests that true is returned from selectBranch for this branch.
    The selector may apply constraints that override this request.
  */
  void requestAlwaysSelectBranch(const STBranchNode* branch);

protected:

  //! Returns true if the leaf is selected (for write)
  /*!
    \param leaf The particular leaf node in question.
    \param hdlPath Gives the caller the ability to parse the node name
    \returns true if the leaf is selected (for writing, usually) or
    false if the node is to be skipped.

    \warning If the caller selects \e not to write out a master alias
    of a true alias ring, the caller is responsible for dealing with
    its aliases as well. Currently, if the master is not to be
    written, but all or some of its aliases are to be written, the
    next master-related alias that is iterated to will be the
    master. So, it is strongly suggested that a caller make sure that
    all slaves are not written out if the master is not going to be.
  */
  virtual bool computeSelectLeaf(const STAliasedLeafNode* leaf, const HdlHierPath* hdlPath) = 0;

  //! Returns true if the branch is selected (for write)
  /*!
    \param branch The particular branch node in question
    \param hdlPath Gives the caller the ability to parse the node name
    \returns true if the branch is computeSelected (for writing, usually) or
    false if the branch and all its children are to be skipped.
  */
  virtual bool computeSelectBranch(const STBranchNode* branch, const HdlHierPath* hdlPath) = 0;

  //! Requests that the node be inserted into the always-selected set
  /*!
    The implementation may choose to apply constraints to nodes before
    adding them to the mAlwaysSelect set.
   */
  virtual void requestAlwaysSelect(const STSymbolTableNode* node) = 0;

  typedef UtHashSet<const STSymbolTableNode*> CNodeSet;
  CNodeSet mAlwaysSelect;

private:
  bool isAlwaysSelected(const STSymbolTableNode* node) const;

  // forbid
  STNodeSelectDB(const STNodeSelectDB&);
  STNodeSelectDB& operator=(const STNodeSelectDB&);
};

// Note: this macro is written this way so that it can be invoked like a
// function, with the final semicolon supplied by the user.  A brace-delimited
// statement is no good, because it leads to syntax errors with code like:
// if (cond)
//   ST_ASSERT(predicate,node);
// else
//   ...

#define ST_ASSERT(exp, symtabObject) \
  do { \
    if (!(exp)) { \
      (symtabObject)->printAssertInfo(__FILE__, __LINE__, __STRING(exp)); \
    } \
  } while (0) /* no trailing semicolon -- it is supplied by the caller */

#endif
