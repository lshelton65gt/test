// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __STALIASEDLEAFNODENOALLOC_H_
#define __STALIASEDLEAFNODENOALLOC_H_


#include "symtab/STAliasedLeafNode.h"

/*!
  \file
  A specialization of a STAliasedLeafNode that represents a node
  for which no allocation is necessary.  This is used when creating
  aliases nodes for nets that have been flattened away -- we need
  to know about their names but do not need to know about their allocations.
*/

//! STAliasedLeafNodeNoAlloc class to describe a name and its aliases
/*!
  A name in a design can have many different working names due to
  synthesis and replication. This class correlates aliases and names
  while in itself is a name.
*/ 
class STAliasedLeafNodeNoAlloc : public STAliasedLeafNode
{
public: CARBONMEM_OVERRIDES
  //! constructor
  STAliasedLeafNodeNoAlloc(const StringAtom* name, BOMData bomdata);

  //! virtual destructor
  virtual ~STAliasedLeafNodeNoAlloc();

  virtual bool isAllocated() const;

private:
  // forbid
  STAliasedLeafNodeNoAlloc(const STAliasedLeafNodeNoAlloc&);
  STAliasedLeafNodeNoAlloc& operator=(const STAliasedLeafNodeNoAlloc&);
};

#endif
