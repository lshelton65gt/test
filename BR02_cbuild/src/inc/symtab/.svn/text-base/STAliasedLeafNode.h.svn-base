// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __STALIASEDLEAFNODE_H_
#define __STALIASEDLEAFNODE_H_


#ifndef __STSYMBOLTABLENODE_H_
#include "symtab/STSymbolTableNode.h"
#endif

/*!
  \file
  A specialization of a STSymbolTableNode that denotes the leaves of a
  branch. Each full name has scopes, each scope is a branch, each leaf
  on the scope is an STAliasedLeafNode. Aliased signals point to each
  other for easy iteration.
*/

//! STAliasedLeafNode class to describe a name and its aliases
/*!
  A name in a design can have many different working names due to
  synthesis and replication. This class correlates aliases and names
  while in itself is a name.
*/ 
class STAliasedLeafNode : public STSymbolTableNode
{
public: CARBONMEM_OVERRIDES
  //! constructor
  STAliasedLeafNode(const StringAtom* name, BOMData bomdata);

  //! virtual destructor
  virtual ~STAliasedLeafNode();

  virtual const STAliasedLeafNode* castLeaf() const;
  virtual STAliasedLeafNode* castLeaf();

  /* Specialization */

  //! Return the size of the aliasRing
  /*!
    This is an O(n) method. Only use this if you really need to know
    the size of the ring. If you just need to know if there are aliases
    use hasAliases().
  */
  SInt32 getRingLen() const;
  
  //! Return true if this node has aliases
  /*!
    This is a very efficient method that tells you whether or not
    there are aliases.
  */
  bool hasAliases() const;
  
  //! Link a node as an alias
  /*!
    \warning If the alias rings overlap the resulting alias ring will
    be corrupt. Both alias rings must be mutually exclusive. The only
    way that a node could potentially overlap is if the symboltable is
    being appended via a database read and the symboltable that is
    being read has leaves that already exist in this symboltable but
    have an alias that is also already in this symboltable. To avoid
    this issue the reader uses the private function appendAliases().

    This adds the aliasName to the AliasRing. This also resolves the
    storage alias, if applicable, and the master alias.

    In all cases, the master for the joined ring is the master of
    'this'.
   
    The storage for the joined ring is the storage for 'this' if
    non-NULL. Otherwise, the storage for 'aliasName' is used. If the
    storage for both rings is NULL, storage ptrs are untouched,
    remaining NULL.

    \param aliasName Name object that is aliased to this

    \warning Duplicity checks are \b NOT done.
    \sa setThisMaster(), setThisStorage(), transferAliases()
  */
  void linkAlias(STAliasedLeafNode* aliasName);
  
  //! Return the alias to which this is linked - read/write 
  /*!
    \returns the "next" alias in the ring. To traverse the ring, you
    call this method on this then call this method on the returned
    ring, etc. If no aliases exist, this is returned.
    
  */
  STAliasedLeafNode* getAlias();

  //! Return the alias to which this is linked - read-only
  const STAliasedLeafNode* getAlias() const;

  //! set this node as the master alias
  /*!
    To make it possible for the symbol table to do true aliasing with
    some notion of what the common net should be in the alias ring,
    this method will have all the aliases in the ring point to the
    common net. 
    So, this method does 2 things essentially:
    \li It goes through the ring and updates all the master node
    pointers to point to this node.
    \li It will make all new aliases that are added point to this
    node as their master.

    If this is not called all aliases point to the STAliasedLeafNodes
    that aliased them. For example, if node a aliased node b, then
    node b will point to a. If a was not aliased previously, it will
    point to itself, and it still will since b is aliased by a. It 
    \b is strange, but it is a speed hack. By doing it this way,
    adding aliases is a constant time algorithm. It is assumed that
    all practical symboltable usages will set master aliases.

    \sa linkAlias()
    \sa setThisStorage()
  */
  void setThisMaster();


  //! set this node as the storage alias
  /*!
    This method allows for aliases to have separate a master and a
    separate storage alias. The master is most likely the highest
    level name in the hierarchy, while the storage for the net could
    be deep in the hierarchy.

    This method does 2 things:
    \li It goes through the ring and updates all the storage node
    pointers to point to this node.
    \li It will attempt to make all new aliases that are added point to this
    node for storage. The caveat here is that if two nodes have
    already been set to different storages and then are aliased
    together, the node that aliases (the node that calls the
    linkAlias() method) will have priority and its storage alias will
    be used.

    If this is not called on an alias ring, the storage alias will be
    NULL.
    
    \sa getStorage(), linkAlias(), setThisMaster()
  */
  void setThisStorage();


  //! Clear all storage information from this alias ring.
  void clearStorage();
  
  //! Return the master alias
  /*!
    This returns the master AliasedLeafNode or \b this if it was never
    set.

    \sa setThisMaster()
  */
  STAliasedLeafNode* getMaster();

  //! Return the master alias - read only
  const STAliasedLeafNode* getMaster() const;

  //! Return the non-null storage alias
  /*!
    If this node was never aliased it will return this node.
    If the internal pointer is needed, which can be NULL, then use
    getInternalStorage()

    \sa setThisStorage(), getInternalStorage()
  */
  STAliasedLeafNode* getStorage();

  //! Get the storage node pointer
  /*!
    This returns the AliasedLeafNode which is marked for storage or it
    returns \b NULL if it was never set. This is needed for apps or
    internals that need to know if storage is really null or not. Use
    getStorage() if the actual non-null storage node is needed.

    \sa setThisStorage(), getStorage()
   */
  STAliasedLeafNode* getInternalStorage();

  //! Return the storage alias - read only
  const STAliasedLeafNode* getStorage() const;

  //! Get storage node pointer - read-only
  const STAliasedLeafNode* getInternalStorage() const;

  //! Transfer the aliases of other onto this (excluding other).
  void transferAliases(STAliasedLeafNode *other);

  //! Print the alias ring.  By default, prints to stdout.
  void printAliases() const;

  //! Print the alias ring and storage elements.  By default, prints to stdout.
  void printAliasStorage() const;

  //! Does node's NUNet require allocation
  virtual bool isAllocated() const;

  //! Used by the db-reader
  typedef UtArray<STAliasedLeafNode*> LeafNodeVec;

  //! Add set of nodes as aliases. 
  /*!
    If any of these nodes already appear in the current alias ring
    then those nodes are simply ignored/untouched.

    Note that the only way that alias rings could overlap is if you
    are translating aliases from one symboltable to another. 

    This is an O(n) function, so all the nodes that need to be
    appended to this leaf should be in the vector, and this should not
    be called repeatedly.

    This is used by the db reader. 

    \param nodes List of nodes to append as aliases to this node. It
    is a non-const reference because the vector itself will not be
    modified, but the nodes inside of it will indirectly.
  */
  void appendAliases(LeafNodeVec& nodes);

  //! class to loop over aliases
  class AliasLoop
  {
  public: 
    CARBONMEM_OVERRIDES

    typedef STAliasedLeafNode* value_type;
    typedef STAliasedLeafNode* reference;

    //! ctor
    AliasLoop(STAliasedLeafNode* node): mFirst(node), mCurrent(node) {}

    //! increment
    void operator++() {
      mCurrent = mCurrent->getAlias();
      if (mCurrent == mFirst)
        mCurrent = NULL;
    }

    //! get current value
    STAliasedLeafNode* operator*() const {return mCurrent;}

    //! at the end?
    bool atEnd() {return mCurrent == NULL;}
  private:
    STAliasedLeafNode* mFirst;
    STAliasedLeafNode* mCurrent;
  };

  //! class to loop over aliases of a const leaf
  class ConstAliasLoop
  {
  public: 
    CARBONMEM_OVERRIDES

    typedef const STAliasedLeafNode* value_type;
    typedef const STAliasedLeafNode* reference;

    //! ctor
    ConstAliasLoop(const STAliasedLeafNode* node): mFirst(node), mCurrent(node) {}

    //! increment
    void operator++() {
      mCurrent = mCurrent->getAlias();
      if (mCurrent == mFirst)
        mCurrent = NULL;
    }

    //! get current value
    const STAliasedLeafNode* operator*() const {return mCurrent;}

    //! at the end?
    bool atEnd() {return mCurrent == NULL;}
  private:
    const STAliasedLeafNode* mFirst;
    const STAliasedLeafNode* mCurrent;
  };

private:
  //! Print node-specific description to stderr
  virtual void printInfo() const;
  
private:
  STAliasedLeafNode* mNextAlias;
  STAliasedLeafNode* mMasterAlias;
  STAliasedLeafNode* mStorageAlias;

  // forbid
  STAliasedLeafNode(const STAliasedLeafNode&);
  STAliasedLeafNode& operator=(const STAliasedLeafNode&);
};

//! Abstraction for a vector of leaf nodes
typedef UtArray<const STAliasedLeafNode*> STAliasedLeafNodeVector;

#endif
