// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __STFIELDBOM_H_
#define __STFIELDBOM_H_

#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

class UtXmlWriter;
class STAliasedLeafNode;
class STBranchNode;
class ZostreamDB;
class ZistreamDB;
class MsgContext;
class UtString;
class AtomicCache;

//! BOMData container
/*!
  This object allocs and frees all BOMDatas that are placed in the
  SymbolTable.
*/
class STFieldBOM
{
public: CARBONMEM_OVERRIDES
  //! DB Read enum
  /*!
    This enum holds all the reasons why a DB read may fail (as well as
    a successful eReadOK). Many non-eReadOK statuses will have
    information associated with them. They are notated in the
    description, with the word \e info.
  */
  enum ReadStatus {
    eReadOK, //!< Read/Parse successful
    eReadFileError, //!< Read/Parse failed due to file problem, info
    eReadIncompatible, //!< File is incompatible with the software
    eReadCorruptFile //! The file is corrupt
  };

  //! virtual destructor
  virtual ~STFieldBOM() {}
  
  //! Abstraction for void* for bom data
  typedef void* Data;

  //! Write the BOM's signature to ensure consistency with the reader
  virtual void writeBOMSignature(ZostreamDB& out) const = 0;
  
  //! Read the BOM signature.
  /*!
    This checks the signature for compatibility and will return 
    eReadIncompatible if the signature is not compatible with the
    writer's signature. If there is an error, the errMsg will contain
    the reason
  */
  virtual ReadStatus readBOMSignature(ZistreamDB& in, UtString* errMsg) = 0;

  //! Allocate an BOMData to be placed in the symbol table branch.
  virtual Data allocBranchData() = 0;

  //! Allocate an BOMData to be placed in the symbol table leaf.
  virtual Data allocLeafData() = 0;
  
  //! Delete the BOMdata on a branch
  /*!
    This will NULLify the reference (*bomdata = NULL).
    If bomdata is NULL then no action is taken.
    \warning Undefined behaviour if *bomdata is not an object created
    by this object's allocBranchData() method.

    \param branch Branch from which the bomdata originates
    \param bomdata A pointer to the bomdata in the branch

  */
  virtual void freeBranchData(const STBranchNode* branch, Data* bomdata) = 0;
  
  //! Delete the BOMdata on a leaf
  /*!
    This will NULLify the reference (*bomdata = NULL).
    \warning Undefined behaviour if *bomdata is not an object created
    by this object's allocLeafData() method.
    
    \param leaf Leaf from which the bomdata originates
    \param bomdata A pointer to the bomdata in the leaf
  */
  virtual void freeLeafData(const STAliasedLeafNode* leaf, Data* bomdata) = 0;

  //! Sets atomic cache for Uset Type class
  virtual void setAtomicCache(AtomicCache* /*atomCache*/){};

  //! Copy string Atoms into current Atomic cache
  virtual void copyStringsIntoSymTable() {};

  //! Pre-field write call.
  /*!
    This is only called \e once. Before all the fields are written by
    the symbol table this is called to do any cleanups or object writes
    that are needed across all fields. This method is called after
    preAtomicCacheWrite method.
  */
  virtual void preFieldWrite(ZostreamDB& out) = 0;

  //! Write the BOMData for a leaf node
  virtual void writeLeafData(const STAliasedLeafNode* leaf, ZostreamDB& out) const  = 0;
  
  //! Write the BOMData for a branch node
  virtual void writeBranchData(const STBranchNode* branch, ZostreamDB& out,
                               AtomicCache* atomicCache) const = 0;

  //! Pre-field read
  /*!
    This is only called \e once. This is called before reading in any
    of the fields.
  */
  virtual ReadStatus preFieldRead(ZistreamDB& in) = 0;
  
  //! Read the BOMData for a leaf node
  virtual ReadStatus readLeafData(STAliasedLeafNode* leaf, ZistreamDB& in, 
                                  MsgContext* msg) = 0;
  
  //! Read the BOMData for a branch node
  virtual ReadStatus readBranchData(STBranchNode* branch, ZistreamDB& in, 
                                    MsgContext* msg) = 0;
  
  //! Print the contents of the branch structure to stdout
  /*!
    Prints the memory pointers of the bomdata structure on a branch to
    stdout. Unless the data needs to wrap for structural reasons keep the
    printed structures on 1 line.
  */
  virtual void printBranch(const STBranchNode* branch) const = 0;

  //! Print the contents of the leaf structure to stdout
  /*!
    Prints the memory pointers of the bomdata structure on a branch to
    stdout. Unless the data needs to wrap for structural reasons keep the
    printed structures on 1 line.
  */
  virtual void printLeaf(const STAliasedLeafNode* leaf) const = 0;

  virtual const char* getClassName() const = 0;

  //! Write the BOMData for a branch node
  virtual void xmlWriteBranchData(const STBranchNode* branch,UtXmlWriter* writer) const = 0;

  //! Write the BOMData for a leaf node
  virtual void xmlWriteLeafData(const STAliasedLeafNode* leaf,UtXmlWriter* writer) const = 0;

};

//! BOM manager for empty fields
/*!
  This can be used for symbol tables that do not need any fields. All
  node BOMs will have NULL in them.
*/
class STEmptyFieldBOM : public STFieldBOM
{
public: CARBONMEM_OVERRIDES
  //! constructor
  STEmptyFieldBOM();
  //! virtual destructor
  virtual ~STEmptyFieldBOM();

  //! STFieldBOM::writeBOMSignature()
  virtual void writeBOMSignature(ZostreamDB& out) const;

  //! STFieldBOM::readBOMSignature()
  virtual ReadStatus readBOMSignature(ZistreamDB& in, UtString* errMsg);

  //! Returns NULL
  virtual Data allocBranchData();

  //! Returns NULL
  virtual Data allocLeafData();

  //! does nothing
  virtual void freeBranchData(const STBranchNode*, Data* bomdata);

  //! does nothing
  virtual void freeLeafData(const STAliasedLeafNode*, Data* bomdata);

  //! Does nothing
  virtual void preFieldWrite(ZostreamDB& out);

  //! does nothing
  virtual void writeLeafData(const STAliasedLeafNode*, ZostreamDB&) const;

  //! does nothing
  virtual void writeBranchData(const STBranchNode* branch, ZostreamDB& out,
                               AtomicCache*) const;

  //! Returns eReadOK
  virtual ReadStatus preFieldRead(ZistreamDB& in);

  //! Returns eReadIncompatible
  virtual ReadStatus readLeafData(STAliasedLeafNode*, ZistreamDB&, MsgContext*);

  //! Returns eReadIncompatible
  virtual ReadStatus readBranchData(STBranchNode*, ZistreamDB&, MsgContext*);

  //! Just prints a null (0x0)
  virtual void printBranch(const STBranchNode*) const;

  //! Just prints a null (0x0)
  virtual void printLeaf(const STAliasedLeafNode*) const;

  virtual const char* getClassName() const;

  //! Write the BOMData for a branch node
  virtual void xmlWriteBranchData(const STBranchNode* branch,UtXmlWriter* writer) const;

  //! Write the BOMData for a leaf node
  virtual void xmlWriteLeafData(const STAliasedLeafNode* leaf,UtXmlWriter* writer) const;


};

#endif
