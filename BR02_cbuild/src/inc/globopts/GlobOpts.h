// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _GLOBOPTS_H_
#define _GLOBOPTS_H_

#include "flow/Flow.h"
#include "nucleus/Nucleus.h"
#include "util/CarbonTypes.h"
#include "util/UtString.h"

class ArgProc;
class AtomicCache;
class CbuildSymTabBOM;
class Elaborate;
class IODBNucleus;
class MsgContext;
class RENetWarning;
class ReachableAliases;
class STSymbolTable;
class SourceLocatorFactory;
class Stats;
class WiredNetResynth;
class REAlias;

//! GOGlobOpts class
/*! This class is used to create a framework in which to do global
 *  optimizations. There are interesting challenges because:
 *
 *  1. All optimizations must either be the same for every instance or
 *  the instances must be replicated.
 *
 *  2. To do global optimizations, elaboration must have been
 *  done. However, the optimizations may cause us to re-do a lot of
 *  the nucleus structures making the elaboration and elaborated
 *  flow invalid.
 *
 *  So this class creates a frame work by doing a simplified local and
 *  elaborated analysis such that optimizations can occur. It then
 *  deletes that framework. The caller can then start the true local
 *  and elaborated analysis.
 */
class GOGlobOpts
{
public:
  //! constructor
  GOGlobOpts(AtomicCache* atomicCache, IODBNucleus* iodbNucleus,
             Stats* stats, ArgProc* args, MsgContext* msgContext,
             NUNetRefFactory* netRefFactory,
             SourceLocatorFactory* sourceLocatoryFactory,
             const char* congruenceInfoFilename,
             WiredNetResynth* wiredNetResynth,
             TristateModeT tristateMode);

  //! destructor
  ~GOGlobOpts();

  //! Call only once - adds the relevant command line args
  static void addCommandlineArgs(ArgProc *arg);

  //! Run the global optimizations
  bool optimize(NUDesign* design, const char* fileRoot, bool print_warnings);

  //! Create the framework (e.g. elaborated flow graph)
  void createFramework(NUDesign*);

  //! Create an alias map.  This must be deleted by the caller.
  REAlias* createAliases();

  //! did an optimization occur?
  bool optimizationOccurred() {return mModified;}

private:
  GOGlobOpts(const GOGlobOpts&);
  GOGlobOpts& operator=(const GOGlobOpts&);

  // Private functions to run the optimizations
  bool propagateConstants(bool undrivenConstants, SInt32 verbose, RENetWarning*);
  void destroyFramework();

  // Classes to create the optimization framework
  STSymbolTable* mSymbolTable;
  CbuildSymTabBOM* mSymTabBOM;
  STSymbolTable* mOldSymbolTable;
  NUNetRefFactory* mNetRefFactory;
  FLNodeFactory* mFlowFactory;
  FLNodeElabFactory* mFlowElabFactory;
  Elaborate* mElaborate;
  ReachableAliases* mReachableAliases;

  // helper class declares
  class TFOutUseBeforeKill;
  
  // Utility classes
  IODBNucleus* mIODBNucleus;
  MsgContext* mMsgContext;
  AtomicCache* mAtomicCache;
  Stats* mStats;
  NUDesign* mDesign;
  ArgProc* mArgs;
  SourceLocatorFactory* mSourceLocatorFactory;
  WiredNetResynth* mWiredNetResynth;
  TristateModeT mTristateMode;
  UtString mCongruenceInfoFilename;
  bool mModified;
}; // class GOGlobOpts


#endif // _GLOBOPTS_H_
