// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*! \file
 * Human readable persistent form for cbuild internal data structures.
 */

#ifndef _NEXPR_H_
#define _NEXPR_H_

//! Set to enable N-expressions
#define NEXPR_ENABLE 0

#include <stdarg.h>
#include "util/UtIOStream.h"
#include "util/CarbonTypes.h"
#include "util/SourceLocator.h"
#include "util/StringAtom.h"

class MsgContext;
class NUDesign;                         // extern decl for NExpr::writeNucleus
class NUModule;                         // extern decl for NExpr::writeNets
class NUNetRefFactory;
class CarbonContext;

//! \class NExpr
/*! The internal cbuild data structures are written to text files containing
 *  lists of N-expressions. An N-expressions is a nested parenthesised form
 *  that looks a little like Lisp S-expressions. An N-expression is represented
 *  as an instance of class NExpr.
 * 
 *  Data is written through an NExpr::Writer object.
 *
 *  Objects are written by calling a write method on the object. For example,
 *  NUBlockingAssign is equipped with a ::write (NExpr::Writer &, ...)
 *  method. These write methods are not be written directly. Instead they are
 *  generated from a schema by the N-expression schema compiler.
 *
 *  N-expressions are read from a file by an NExpr::Reader object. The reader
 *  creates an NExpr instance for each N-expression in the file. Object are
 *  created from N-expressions with the NExpr::build method.
 */

class NExpr {
public:

  CARBONMEM_OVERRIDES

  //! ctor for NExpr
  /*! \param loc the location of the N-expression.
   *  \param type a symbol denoting the type string.
   */
  NExpr (const SourceLocator loc, const StringAtom *type);
  NExpr (const SourceLocator loc, const StringAtom *type, const UInt32 type_index);

  virtual ~NExpr ();

  //! \return the location of the N-expression
  const SourceLocator getLoc () const { return mLoc; }

  //! \return the type string for the NExpr
  /*! This method returns the type symbol read from the N-expression. For
   *  example, if the N-expressions if (NUVectorNet ...) then this method returns
   *  a symbol denoting the string "NUVectorNet".
   */
  const StringAtom *getType () const { return mType; }

  //! \return the index of the type
  UInt32 getTypeIndex () const { return mTypeIndex; }

  //! Lookup the type identifier for the N-expression
  /*! This method looks up the type table and maps the type symbol to the
   *  integer type identifier. If the type symbol denotes a valid type then the
   *  type parameter is set to the integer type identifier.
   *  \return iff the type symbol denotes a valid type
   */
  bool getType (UInt32 *type) const 
  { return lookupTypeIndex (getType ()->str (), type); }

  //! Lookup an enum identifier
  static bool lookupEnum (const UInt32 type, const char *ident, UInt32 *value);

  //! \return iff this is a particular type or a subclass of that type
  /*! This method checks whether an N-expression is an instance of a given type
   *  or a subclass of that type.
   */
  bool isType (const UInt32 type) const;

  //! \return iff the type is an enumeration
  static bool isEnum (const UInt32 type) { return (sTypeFlags [type] & eENUM) != 0; }

  //! \return iff the type is a choice enumeration
  static bool isChoiceEnum (const UInt32 type) { return (sTypeFlags [type] & eCHOICE) != 0; }

  //! \return iff the type is a mask enumeration
  bool static isMaskEnum (const UInt32 type) { return (sTypeFlags [type] & eMASK) != 0; }

  //! Set the integer label
  void setLabel (const UInt32 label) { mLabel = label; }

  //! \return iff this N-expression is labelled
  bool isLabelled () const { return mLabel > 0; }

  //! \return the label of this N-expression
  UInt32 getLabel () const { return mLabel; }

  class LabelMap;                       // maps integer labels to void *
  class ObjectMap;                      // maps void * to integer labels
  class ForwardReferenceList;           // list of pointers that need to be fixed

  //! \class Writer
  /*! Objects are written as N-expressions via an NExpr::Writer.
   */
  class Writer {
  public:

    enum {
      cDEBUG = 1<<0,                    //!< enable debugging noise
      cOWNS_STREAM = 1<<1,              //!< iff this owns the stream
      cWIDTH = 100                      //!< width of the output
    };

    // When cCLOSURE is set, a non-hierarchical object is written immediately
    // the label is allocated.

    //! ctor that wraps an existing stream.
    Writer (UtOStream &, const UInt32 flags = 0);

    //! ctor that opens and writes to a file.
    Writer (const char *filename, const UInt32 flags = 0);

    virtual ~Writer ();

    //! \class Object
    /*! The Object class is used by write methods to track labels, parenthesis
     *  and blank lines. An instance should be created on entry to a write
     *  method and the done method called on exit.
     *
     */
    friend class Object;

    class Object {
    public:
      Object (Writer &, const char *tag, const void *object, bool hierarchical, bool isTop);
      ~Object ();
    private:
      Writer &mDump;                    //!< output sream
      bool mParenthesised;              //!< set if close parentheses are needed
    };

    //! Output horizontal or vertical glue
    void doGlue (const char next_char);

    //! Add an object to the writer object table and return the label
    UInt32 intern (const void *object);

    //! Intern an object and write the label to the output file as %label.
    bool writeLabel (const void *object);

    //! Templated write method for base types
    template <typename _T>
    bool write (const _T);

    //! Templated write method for pointer base types
    template <typename _T>
    bool write (const _T *);

    //! Write a single character to the output file.
    Writer &operator << (const char c);

    //! Write a string to the output file.
    Writer &operator << (const char *s);

    //! Write a Carbon string to the output file.
    Writer &operator << (const UtString &);

    //! Write an integer to the output file.
    Writer &operator << (const UInt32);

    //! Write the nucleus from a design
    bool writeNucleus (NUDesign *, UtString *errmsg);

    //! Write all the nets in a design
    bool writeNets (NUDesign *, UtString *errmsg);

    //! Write all the nets in a module
    bool writeNets (NUModule *, UtString *errmsg);

    //! Is the stream configured for debug output
    bool isDebug () const { return mFlags & cDEBUG; }

  private:
    
    UInt32 mFlags;                      //!< options and status
    UInt32 mDepth;                      //!< current nesting depth
    bool mIsStartOfLine;                //!< set at the start of the line
    bool mNeedsNewline;                 //!< write a newline before next output
    bool mNeedsSpace;                   //!< write a space before next output
    UInt32 mWidth;                      //!< width of the current line

    // Label management.
    mutable LabelMap *mLabels;          //!< maps objects to labels

    //! The output stream
    UtOStream *mOut;                    //!< output sink
    UtString mFilename;                 //!< name of the output sink

    //! Primitive output method
    void output (const char *);

  };

  class BaseValue;                      // forward decl

  //! \class Reader
  /*! N-expression are read from an input file via Reader objects.
   */
  class Reader {
  public:
    CARBONMEM_OVERRIDES

    enum {
      cLLDEBUG = 1<<0,                  //!< enable N-expression scanner debugging
      cECHO    = 1<<1                   //!< echo N-expressions after they are read
    };

    //! ctor that opens a file.
    /*! \param msg_context the error message stream.
     *  \param strings the symbol obarray
     *  \param sourcelocators the global source locator factory
     *  \param options option mask over the above enum constants
     */
    Reader (CarbonContext *ctx, const UInt32 options);

    virtual ~Reader ();

    //! Parse a file of N-expressions.
    bool parse (const char *filename, UtString *errmsg);

    //! Read the next N-expression from the input stream
    bool read (NExpr **);

    //! Create a source locator
    /*! This is used by the NExpr scanner */
    void create (const int lineno, SourceLocator *loc)
    {   *loc = mSourceLocators->create (mFilename, lineno);     }

    //! Intern a symbol
    StringAtom *intern (const char *s);

    //! Write out the expected field type table
    static void dumpFieldTypes (UtOStream &);

    //! Build an object from the N-expression
    /*! This method constructs a cbuild object from the NExpr instance.
     *  \param nx the NExpr instance representing a cbuild object.
     *  \param object set to point to the new object.
     *  \param expected_type identifier of the expected type.
     *  \param errmsg a string in which error messages are written.
     *  \return iff an object of the expected type was constructed.
     *
     *  \note If an object could not be constructed, then a suitable error
     *  message is returned in errmsg.
     *
     *  \note This method is generated by the schema compiler.
     */
    bool build (const NExpr &nx, void **object, const UInt32 expected_type, 
      UtString *errmsg) const;

    //! Templated wrapper around build.
    /*! This method wraps the raw build method with templated code that returns
     *  the object as a suitably typed pointer. This templated method is used
     *  from within the generated build and Foo::read methods. The integer type
     *  identifiers are allocated by the schema compiler and are absolutely
     *  guarenteed to change as the NExpr schema is developed therefore this
     *  method should only be called from within the schema-compiler-generated
     *  code. If a build is needed from hand-written code then get the correct
     *  type index using the NExpr::lookupTypeIndex method to map the type
     *  symbol into an integer identifier.
     */
    template <typename _T, const UInt32 type_index>
    bool build (const NExpr &nx, _T **object, UtString *errmsg) const;

    //! Lookup a label in the object map
    bool lookupLabel (const UInt32 label, void **object) const;

    //! Patch all the labels
    bool patchLabels ();

    //! Enter a label into map
    /*! \param label the label of the new object
     *  \param object the object denoted by the label
     *  \param errmsg error message written by addLabel if it fails
     *  \return iff the object was added to the map
     */
    bool addLabel (const UInt32 label, void *object, UtString *errmsg) const;

    //! Add a pointer to the patch worklist
    void needsPatch (const SourceLocator loc, void *object) const;

    //! Read a nested object from an N-expression
    /*! \param nx the NExpr instance that denotes the N-expression
     *  \param name the name of the field containing the nested object
     *  \param type_index the required type of the nested object
     *  \param label_ok iff a forward reference is permitted
     *  \param field the addres of the field in the object
     *  \param errmsg returned error message
     *  \return iff an object or allowed forward reference was found
     *
     *  \note This method looked for nested objects in the NExpr instance and
     *  writes either the address of the constructed object, or the label
     *  number of a forward reference into the field. If an allowed forward reference is
     *  found, then it is added to the readers worklist of forward references
     *  to patch.
     */
    bool readObject (const NExpr &nx, const char *name, 
      const UInt32 type_index, bool forward_is_ok, void **field, UtString *errmsg) const;

    //! Templated wrapper around readObject
    /*! \note This method is called by the read methods generated by the schema
     *  compiler.
     */
    template <typename _T, const UInt32 _type_index, bool _forward_is_ok>
    bool readObject (const NExpr &nx, const char *name, _T **field, UtString *errmsg) const
    {
      return readObject (nx, name, _type_index, _forward_is_ok, 
        reinterpret_cast <void **> (field), errmsg);
    }
    
    /*! Macros to get GCC to check the printf format string of the various
     * warning and error methods.
     */
#ifdef __GNUC__
#define PRINTF(_m_, _n_) __attribute__ ((__format__ (printf, _m_, _n_)))
#else
#define PRINTF(_m_, _n_)
#endif

    //! Parser and scanner error reporting
    void YYError (const SourceLocator, const char *fmt, ...) PRINTF (3,4);
    void YYWarn (const SourceLocator, const char *fmt, ...) PRINTF (3,4);
    void YYInfo (const SourceLocator, const char *fmt, ...) PRINTF (3,4);
    void vYYError (const SourceLocator, const char *fmt, va_list);
    void vYYWarn (const SourceLocator, const char *fmt, va_list);
    void vYYInfo (const SourceLocator, const char *fmt, va_list);

#undef PRINTF

  private:
    
    const UInt32 mFlags;                //!< reader options
    const char *mFilename;              //!< name of the input file
    UInt32 mNErrors;                    //!< number of errors
    MsgContext *mMsgContext;            //!< error stream
    SourceLocatorFactory *mSourceLocators;
                                        //!< source location stuff
    AtomicCache *mStrings;              //!< string table
    NUNetRefFactory *mNetRefFactory;    //!< factory for netrefs
    ObjectMap *mLabels;                 //!< maps integer labels to objects
    mutable ForwardReferenceList *mForward; 
                                        //!< list of void * that need patching

    /*! When building objects from NExpr instances track nesting depth. This is
     *  to implement "post top build" callbacks.
     */
    mutable UInt32 mBuildDepth;

    //! Get the name of a token returned by the scanner for a terminal
    static const char *tokenName (const UInt32 token);

    //! \return iff type is a vector type
    /*! \note the element type is returned in element
     *  \note currently, vectors are indicated by setting the top bit of the
     *  type word. The remaining bits are the type of the element. This
     *  approach precludes vectors of vectors... if it turns out that we use
     *  them, then something else will be needed.
     */
    static bool isVector (const UInt32 type, UInt32 *element)
    { *element = type; return false; /*not yet implemented*/ }

    //! \return iff a type is a terminal
    static bool isTerminal (const UInt32 type)
    { return type >= NExpr::sNClasses; }

    //! \return the scanner token for a given terminal type
    static bool expectedToken (const UInt32 terminal, UInt32 *state);


    //! \struct Lexeme
    /*! Lexeme is used to return tokens from the scanner.
     */

    class Lexeme {
    public:

      Lexeme () : mLoc (), mToken (0), mSymbol (NULL), mInteger (0), mValue (NULL) {}

      //! Clear all the payload fields
      void clear ();

      //! Set the source location of the lexeme
      void set (const SourceLocator loc) { mLoc = loc; }

      //! Set the symbol value of the lexeme
      void set (StringAtom *symbol) { mSymbol = symbol; }

      //! Return a keyword or an operator from the scanner
      void Return (const UInt32 token)
      { mToken = token; }

      //! Return a symbol from the scanner
      void Return (const UInt32 token, StringAtom *symbol)
      { mToken = token; mSymbol = symbol; }

      //! Return a base type value from the scanner
      void Return (const UInt32 token, BaseValue *value)
      { mToken = token; mValue = value; }

      //! Return an unsigned integer value from the scanner.
      void Return (const UInt32 token, UInt32 value)
      { mToken = token; mInteger = value; }

      //! \return the location of the token in the source file
      const SourceLocator getLoc () const { return mLoc; }

      //! \return the symbol
      const StringAtom *getSymbol () const { return mSymbol; }

      //! \return the integer value
      UInt32 getInteger () const { return mInteger; }

      //! \return iff the token matches a given value.
      bool is (const UInt32 token) const { return mToken == token; }

      //! \return iff the token matches a given value.
      /*! If the token matches then the symbol value is return in symbol */
      bool is (const UInt32 token, StringAtom **symbol)
      {
        if (mToken == token) {
          *symbol = mSymbol;
        }
        return (mToken == token);
      }

      //! \return iff the token matches a given value.
      /*! If the token matches then the base type value is return in value */
      bool is (const UInt32 token, BaseValue **value)
      {
        if (mToken == token) {
          *value = mValue;
        }
        return (mToken == token);
      }

      //! \return iff the token matches a given value.
      /*! If the token matches then the integer type value is return in value */
      bool is (const UInt32 token, UInt32 *value)
      {
        if (mToken == token) {
          *value = mInteger;
        }
        return (mToken == token);
      }

      //! Format an error message string of the form "expect foo, found foobar"
      /*! \return s->c_str () */
      const char *expected (const UInt32 expected, UtString *s) const;

      //! Convert a token into a string
      static const char *compose (const UInt32 token, UtString *);

      //! Dump the lexeme to standard out
      void pr () const;

    private:

      SourceLocator mLoc;               //!< location of the token
      UInt32 mToken;                    //!< the integer value of the token
      StringAtom *mSymbol;              //!< value of a string lexeme
      UInt32 mInteger;                  //!< value of an integer lexeme
      BaseValue *mValue;                //!< value of a lexeme for a base type

    };

    /*! \note this is generated by flex but the scanner function declaration is
     *  defined into a member of the Reader.
     * \param expecting the literal token type expected, or 0 if no specific type
     * is expected. This should be a value from enum NExprTokens.
     * \return a token which is either a character (<128) or an NExprTokens enum
     * value (defined in NExprLex.h that is generated by the schema compiler).
     */
    int lex (int expecting, Lexeme *);

    //! Error functions for the generated scanner
    void NExpr_error (const char *fmt, ...);

    // recursive descent parser methods
    Lexeme mLexeme;
    UInt32 mLexToken;
    bool parse_typename (Lexeme *value, StringAtom **);
    bool parse_object (Lexeme *value, NExpr **);
    bool parse_label (Lexeme *value, UInt32 *);
    bool parse_attribute (Lexeme *token, const UInt32 classtype, NExpr *nexpr);
    bool parse_nested (Lexeme *token, const UInt32 classtype, const UInt32 field, 
      const UInt32 fieldtype, NExpr **);
    bool parse_value (Lexeme *token, const UInt32 classtype, const UInt32 field,
      const UInt32 fieldtype, BaseValue **);
    void skip (const UInt32 expected, Lexeme *token);

  };                                    // class Reader

  //! \return the type expected
  static bool getType (const UInt32 classtype, const UInt32 field, UInt32 *fieldtype);

  //! Construct an object from this NExpr instance
  bool build (const NExpr::Reader &, void **object, UtString *errmsg) const;

  //! Templated get method for base type
  template <typename _T>
  bool get (const char *tag, _T *, UtString *errmsg) const;

  //! Templated get method for base type
  template <typename _T>
  bool get (const char *tag, _T **, UtString *errmsg) const;

  //! Check that the actual type is consistent with the expected type
  /*! \return iff actual either denotes the same type as expected or it denotes
   *  a subtype of the type denoted by expected.
   */
  static bool typeCheck (const UInt32 expected, const UInt32 actual, UtString *errmsg);

  //! The tripwire function breaks if the nucleus has been broken beneath us.
  static void tripWire ();

  //! Write the trip-wire function for detecting when class have been changed
  static void writeTripwire (const char *filename);

  //! Test whether a class is a subclass of another
  static bool isSubclass (const UInt32 superclass, const UInt32 subclass);

  //! \return iff an enumeration is declared as a mask
  static bool isMask (const UInt32 type);

  //! Map a symbol of a given enumeration type into its value
  /*! If the name is a valid enumeration identifier for the type then value is
   *  set to the appropriate value and the function returns true. If not, then
   *  errmsg is written appropriately and the function returns false.
   */
  static bool getEnumValue (StringAtom *, UInt32 *value, UtString *errmsg);

  //! Test a type against the field
  bool checkType (const UInt32 field_index, const UInt32 actual_type, UtString *errmsg) const;

  //! Prepare an error message string for more text.
  /*! Multiple error messages may be tacked together in a single message
   *  string. This method prepares the string by adding a continuation
   *  backslash and a new line if the message string is non empty.
   */
  static void prepare (UtString *);

  //! Append a message to an error message.
  /*! \return false
   */
  static bool Error (UtString *errmsg, const char *fmt, ...);

  //! \class BaseValue
  /*! When an N-expression is read, a map from identifiers to field values is
   *  constructed. Field values are constructed out of the BaseValue hierarchy.
   */
  class BaseValue {
  public:
    CARBONMEM_OVERRIDES

    //! ctor for BaseValue
    BaseValue (const SourceLocator loc) : mLoc (loc) {}

    // We need a virtual destructor in order to force BaseValue to be polymorphic.
    virtual ~BaseValue () {}
    
    //! \return a string identifying the types of the value
    virtual const char *getType () const = 0;

    //! \return the source location of the value
    const SourceLocator getLoc () const { return mLoc; }

    //! Form a human readable representation of the value
    virtual void compose (UtString *) const = 0;

    //! Print to an output stream
    virtual void print (UtOStream &s, const UInt32) const;

    //! Print to standard output
    void pr () const;

  private:
    const SourceLocator mLoc;           //!< location in the N-expression file
  };

  //! \class NestedValue
  /*! Value that is a nested N-expression.
   */
  class NestedValue : public BaseValue {
  public:

    NestedValue (const SourceLocator loc, NExpr *nested) : 
      BaseValue (loc), mNested (nested) {}

    virtual ~NestedValue () { delete mNested; }

    //! \return a string identifying the types of the value
    virtual const char *getType () const { return mNested->getType ()->str (); }

    //! \return the nested N-expression
    NExpr *getNExpr () const { return mNested; }

    //! Form a human readable representation of the value
    virtual void compose (UtString *) const {}

    //! Print to an output stream
    virtual void print (UtOStream &s, const UInt32 indent) const
    { mNested->print (s, indent); }

  private:
    NExpr *mNested;                     //!< the nested N-expression
  };
  
  //! \class Value
  /*! Templated class for non-pointer type values.
   */
  template <typename _T>
  class Value : public BaseValue {
  public:
    Value (const SourceLocator loc, _T x) :
      BaseValue (loc), mX (x)
    {}

    //! \return the payload
    _T getValue () const { return mX; }

    //! \return a string identifying the type of this value
    /*! \note The definition of this method is generated by the schema
     *  compiler.
     */
    virtual const char *getType () const;

    //! Form a human readable representation of this
    virtual void compose (UtString *) const;

  private:
    _T mX;                              //!< the value
  };

  //! \class PointerValue
  /*! Templated class for pointer type values.
   */
  template <typename _T>
  class PointerValue : public BaseValue {
  public:

    PointerValue (const SourceLocator loc, _T *x) :
      BaseValue (loc), mX (x)
    {}

    //! \return the payload
    _T *getValue () const { return mX; }

    //! \return a string identifying the type of this value
    /*! \note The definition of this method is generated by the schema
     *  compiler.
     */
    virtual const char *getType () const;

    //! Form a human readable representation of this
    virtual void compose (UtString *) const;

  private:
    _T *mX;
  };

  //! Add a field to the N-expression.
  bool addField (SourceLocator loc, StringAtom *name, BaseValue *value, UtString *errmsg);

  //! Lists of N-expressions
  typedef UtVector <NExpr *> List;

  //! Dump an NExpr to a stream
  void print (UtOStream &, const UInt32 indent = 0) const;
  void pr () const;

private:

  enum {
    eNOTYPE = 0xFFFFFFFF
  };

  const SourceLocator mLoc;             //!< source location of the N-expression
  const StringAtom *mType;              //!< the type string
  UInt32 mTypeIndex;                    //!< the type index
  UInt32 mLabel;                        //!< if non-zero, the label

  // The FieldMap is really a hash map from names to fields. However, including
  // UtHashMap into NExpr.h turns out to be a bad idea. It seems to break the
  // compile when it gets sucked into other headers. So, definition of the type
  // is delegated to NExpr.cxx and the NExpr holds a pointer to the field map.
  class FieldMap;
  friend class FieldMap;

  // Classes and base types are mapped to an integer code at run time. Classes
  // are allocated codes form 0 to sNClasses - 1, base types are allocated
  // codes from sNClasses to sNClasses + sNBaseTypes - 1. The sTypeName table
  // is used to map codes into the names. The class part of the table is sorted
  // so that we can map from name to code with a binary search. This is used
  // when identifying the class type of an N-expressions.

  //! Lookup the sTypeNames table for the index of a name.
  static bool lookupTypeIndex (const char *name, UInt32 *label);

  //! Lookup the sFieldNames table for the index of a field
  static bool lookupFieldIndex (const char *name, UInt32 *label);
  
  //! Get the name of a type.
  static const char *typeName (UInt32 index);

  //! Get the name of a field
  static const char *fieldName (UInt32 index);

  static const char *sTypeNames [];     //!< table of type names
  static const UInt32 sNClasses;        //!< number of abstract types
  static const UInt32 sNBaseTypes;      //!< number of base types

  static const char *sFieldNames [];    //!< table of field names
  static const UInt32 sNFieldNames;     //!< number of value field names

  enum {
    eBASETYPE = 1<<0,
    eCLASS = 1<<1,
    ePOINTER = 1<<2,
    eVECTOR = 1<<3,
    eMASK = 1<<4,
    eCHOICE = 1<<5,
    eENUM = eMASK|eCHOICE
  };

  static const UInt32 sTypeFlags [];    //!< bitmask of types

  //! Test the lookup functions
  static void checkTypeLookup ();
  static void checkFieldLookup ();

  FieldMap *mFields;                    //!< map of the fields

  //! \class Field
  /*! This class manages a (name, value) pair.
   *  \note The Field class instance takes ownership of the value object. The
   *  value storage is freed in the Field destructor.
   */
  class Field {
  public:
    CARBONMEM_OVERRIDES

    Field (SourceLocator loc, const StringAtom *name, BaseValue *value) :
      mLoc (loc), mName (name), mValue (value)
    {}
    virtual ~Field () { delete mValue; }

    //! \return the source location of the field.
    const SourceLocator getLoc () const { return mLoc; }

    //! \return the field value
    const BaseValue *getValue () const { return mValue; }

    //! \return the field name
    const StringAtom *getName () const { return mName; }

    //! Print to an output stream
    void print (UtOStream &) const;

    //! Print to standard output
    void pr () const;

  private:
    SourceLocator mLoc;
    const StringAtom *mName;
    const BaseValue *mValue;
  };

  //! Lookup a named field in the field map.
  bool get (const char *name, Field **field, UtString *errmsg) const;

  struct EnumValue {
    EnumValue (const UInt32 type, const char *name, UInt32 value) : 
      mType (type), mName (name), mValue (value) {}
    const UInt32 mType;                 //!< the type index for the enum
    const char *mName;                  //!< identifier for the enum value
    const UInt32 mValue;                //!< the associated value
  };

  static UInt32 sNEnumTypes;
  static UInt32 sNEnumValues;
  static EnumValue sEnumValues [];

  static void getEnumBounds (const UInt32 key, UInt32 *lo, UInt32 *size);
  static const char *getEnumName (const UInt32 key);
};

/*! \note N-expression enabled classes must be equipped with write and read
 *  methods. These must be declared as public methods of the class. It is
 *  strongly recommended that these methods be implemented using the
 *  N-expression schema compiler rather than hand written. Furthermore,
 *  concrete classes require a parameterless constructor so that the
 *  NExpr::build method can instantiate an object and then call its read
 *  method.
 */

#if NEXPR_ENABLE

#define NEXPR_SUPPORT2(_class_) \
public: \
  virtual bool write (NExpr::Writer &, bool isTop, UtString *errmsg) const; \
  virtual bool read (const NExpr::Reader &, const NExpr &, UtString *errmsg); \
protected: \
  friend class NExpr::Reader; \
  friend class NExpr; \
  _class_ () {}

#define NEXPR_SUPPORT \
public: \
  virtual bool write (NExpr::Writer &, bool isTop, UtString *errmsg) const; \
  virtual bool read (const NExpr::Reader &, const NExpr &, UtString *errmsg); \
protected: \
  friend class NExpr; \
  friend class NExpr::Reader;

#else

#define NEXPR_SUPPORT2(_class_)
#define NEXPR_SUPPORT

#endif
#endif

