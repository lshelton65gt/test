// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*! \file
 *  A small class hierarchy for testing the NExpr stuff. This is intended for
 *  use in a set of test cases for N-expression reading and writing.
 */

#ifndef _NEXPR_TEST_H
#define _NEXPR_TEST_H

#include "nexpr/NExpr.h"

#if NEXPR_ENABLE

#include "util/NXS.h"

namespace NExprTest {

  //! \class NExprTest::Base
  /*! Base class for the NExprTest hierarchy.
   */
  class Base {
  public:
    Base () : mDepth (0), mNext (NULL) { }
    virtual ~Base () {}

    enum Choice {
      eTestChoice1,
      eTestChoice2,
      eTestChoice3
    } NXSCHOICE;

    enum Mask {
      eTestMask1 = 1<<0,
      eTestMask2 = 1<<4,
      eTestMask3 = 1<<6,
      eTestMask13 = eTestMask1|eTestMask3
    } NXSMASK;

    NEXPR_SUPPORT

    //! \return the name of the type
    virtual const char *getType () const = 0;

    //! Visualisation
    void print (UtOStream &) const;
    void pr () const;

    //! Clean up all the crap
    static void postmortem ();

    //! Set the N-expression depth of this test object
    void setDepth (const UInt32 depth) { mDepth = depth; }

    //! Add this object to the list of test objects
    void addToList ();
    
  private:

    UInt32 mBaseX;
    UInt32 mDepth;                      //!< record the N-expression nesting depth
    Base *mNext;                        //!< link to the next in the test object list

    //! Keep a list of all the test objects constructed
    static Base *sHead, *sTail;

  protected:

    // The print methods write text used in regression gold files. This is a
    // set of methods to help output readably formatted text.

    class PrintContext {
    public:
      PrintContext (UtOStream &, const Base *, PrintContext *parent = NULL);
      virtual ~PrintContext ();
      void operator () (const char *name, const UInt32 value);
      void operator () (const char *name, const Base *value);
    private:
      UtOStream &mOut;
      const Base *mObject;
      PrintContext *mParent;
      UInt32 mNFields;
      void separate ();
    };

    //! Visualisation
    virtual void print (PrintContext &) const;

  } NXSDISTILL;

  class A : public Base {
  public:
    A () {}
    virtual ~A () {}
    
    NEXPR_SUPPORT

    //! \return the name of the type
    virtual const char *getType () const { return "A"; }

  protected:

    //! Visualisation
    virtual void print (PrintContext &) const;

  private:
    UInt32 mAX;
  };

  class B : public Base {
  public:
    B (const UInt32 x) : Base (), mBX (x) {}
    virtual ~B () {}

    NEXPR_SUPPORT

    //! \return the name of the type
    virtual const char *getType () const { return "B"; }

  protected:

    //! Visualisation
    virtual void print (PrintContext &) const;

  private:
    UInt32 mBX;
  };

  class C : public B {
  public:
    C (NExprTest::Base *b, UInt32 x) : B (x), mBase (b) {}
    virtual ~C () { }

    NEXPR_SUPPORT

    //! \return the name of the type
    virtual const char *getType () const { return "C"; }

  protected:

    //! Visualisation
    virtual void print (PrintContext &) const;

  private:
    Base *mBase;
    UInt32 mCX;
    UInt32 mCY;
  };

  class D : public B {
  public:
    D (UInt32 x) : B (x), mBase (NULL) {}
    virtual ~D () { }

    NEXPR_SUPPORT

    //! \return the name of the type
    virtual const char *getType () const { return "D"; }

  protected:

    //! Visualisation
    virtual void print (PrintContext &) const;

  private:
    Base *mBase;
    UInt32 mDX;
  };

}

#endif
#endif
