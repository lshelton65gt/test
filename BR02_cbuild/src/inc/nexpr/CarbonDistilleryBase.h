// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _CARBONDISTILLEYBASE_H_
#define _CARBONDISTILLEYBASE_H_

class NExpr;
class MsgContext;
class AtomicCache;

class CarbonDistilleryBase {
public:

  CarbonDistilleryBase (AtomicCache *, MsgContext *);
  virtual ~CarbonDistilleryBase () {}

  //! Construct cbuild internal data structures from the accumulated N-expressions
  bool distill ();

  //! Add an N-expression to the set of N-expressions for distillation
  void operator () (NExpr *nexpr);

protected:

  AtomicCache *mAtomicCache;
  MsgContext *mMsgContext;

};

#endif
