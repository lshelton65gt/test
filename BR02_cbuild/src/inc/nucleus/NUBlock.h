// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUBLOCK_H_
#define NUBLOCK_H_

#include "nucleus/NUScope.h"
#include "nucleus/NUBlockStmt.h"

/*!
  \file
  Declaration of the Nucleus block class.
 */

//! NUBlock class
/*!
 * Model the code-containing aspect of a Verilog block. Blocks can be
 * arbitrarily nested.
 *
 * Verilog named blocks are separated into declaration and definition
 * components. Variable declarations are added to the
 * NUNamedDeclarationScope hierarchy, while code is owned by the
 * NUBlock.
 *
 * All user variables must be declared within an
 * NUNamedDeclarationScope. NUBlock objects may declare nets; only
 * non-static declarations are allowed. Nets declared within an
 * NUBlock will never be visible to the user.
 *
 * NUBlock objects are not elaborated nor emitted into the symbol
 * table.
 *
 * The block is responsible for memory management of its declared nets
 * and contained statements.
 * 
 * \sa NUNamedDeclarationScope
 */
class NUBlock : public NUBlockStmt, public NUBlockScope
{
public:
  //! Constructor for an NUBlock.
  /*!
   * \param stmts  List of statements this block will contain. List is
   *               copied. May be NULL.
   *
   * \param parent Parent scope containing this block. Usually, this
   *               is a module, task, or another block.
   *
   * \param iodb   IODB to store intrinsic type information.
   *
   * \param netref_factory Factory used to manage NetRefs; needed for
   *               u/d containers.
   *
   * \param uses_cf_net True when the block needs the $cfnet in its
   *               use set.
   *
   * \param loc    Source location.
   */
  NUBlock(const NUStmtList * stmts,
          NUScope * parent,
          IODBNucleus * iodb,
          NUNetRefFactory * netref_factory,
          bool uses_cf_net,
          const SourceLocator & loc);

  //! Destructor for an NUBlock.
  virtual ~NUBlock();

  //! Blocks are not named and therefore do not have a symtab index.
  SInt32 getSymtabIndex() const { NU_ASSERT(0, this); return -1; }

  //! Blocks are not named.
  virtual bool isNamed() { return false; }

  //! Blocks are anonymous; they are not named.
  StringAtom * getName() const { return NULL; }

  //! From NUScope; return this as the base object.
  NUBase* getBase() { return this; }
  const NUBase* getBase() const { return this; }

  //! Realise NUScope::getSourceLocation virtual method
  virtual const SourceLocator getSourceLocation () const;

  //! Add the given block as a child
  /*!
   * \param block  The block to add as a child.
   */
  void addBlock(NUBlock * block) APIDISTILLMACRO2(APIEMITBIND(mBlockList),
                                                  APIEMITCODE(mBlockList.push_back(block)));

  //! Remove the given block as a child.
  /*!
   * \param block  The block to remove as a child.
   */
  void removeBlock(NUBlock * block);

  //! Loop over nested blocks
  NUBlockCLoop loopBlocks() const;

  //! Loop over nested blocks
  NUBlockLoop loopBlocks();

  //! Illegal to call; blocks cannot contain declaration scopes.
  void addDeclarationScope(NUNamedDeclarationScope *scope);

  //! Illegal to call; blocks cannot contain declaration scopes.
  void removeDeclarationScope(NUNamedDeclarationScope *scope);

  //! Will always loop over an empty set of declaration scopes.
  NUNamedDeclarationScopeCLoop loopDeclarationScopes() const;

  //! Will always loop over an empty set of declaration scopes.
  NUNamedDeclarationScopeLoop loopDeclarationScopes();

  //! Return the type of this class.
  NUType getType() const { return eNUBlock; }

  //! Return the scope in which this block is defined.
  NUScope* getParentScope() const { return mParent; }

  //! Change the parent scope for this block.
  void putParentScope (NUScope* scope) { mParent = scope; }

  //! Return the type of scoping unit.
  ScopeT getScopeType() const { return NUScope::eBlock; }

  //! Add a net to this block's locally-declared nets.
  /*! 
   * \param net  The net to add. 
   *
   * It is asserted that this net must be non-static, temporary, and
   * block-local.
   */
  void addLocal(NUNet* net) APIDISTILLMACRO3(APIEMITBIND(mLocalNetList),
                                             APIEMITCODE(mLocalNetList.push_back(net)),
                                             APIEMITCODE(mIODB->addTypeIntrinsic(net)));
  //! Remove a net from this block's set of locally-defined nets.
  /*!
   * \param net  The net to remove.
   *
   * \param remove_references If true, references to 'net' are removed
   *             from the NetRef factory. If live NetRef references
   *             still exist, this will cause a reference-count
   *             assertion.
   */
  void removeLocal(NUNet* net, bool remove_references=true);

  //! remove a set of nets (more efficient than calling removeLocal N times)
  void removeLocals(const NUNetSet& nets, bool remove_references);

  //! Move all the local variables to a target block
  void moveLocals(NUScope* target);

  //! Loop over the locally-declared nets.
  NUNetCLoop loopLocals() const { return NUNetCLoop(mLocalNetList); }

  //! Loop over the locally-declared nets.
  NUNetLoop loopLocals() { return NUNetLoop(mLocalNetList); }

  //! For a block, same as getAllNets.
  void getAllNonTFNets(NUNetList *net_list) const;

  //! Add all nets local to this scope to the given list, in no particular order.
  /*!
   * Nets declared local to blocks will be added also; this list has
   * all nets declared within this scope.
   */
  void getAllNets(NUNetList *net_list) const;

  //! Add the nets from the nested named blocks to the given list
  void getAllSubNets(NUNetList *net_list) const;

  //! Blocks are not emitted in the symbol table; return false.
  bool emitInSymtab() const { return false; }

  //! For objects contained within this block, return their position within the parent's symbol table entry.
  /*!
   * Blocks are not emitted in the symbol table. Therefore, all
   * requests for symbol table position revert to the parent.
   */
  SInt32 reserveSymtabIndex();

  //! Loop through all the statements.
  NUStmtLoop loopStmts() { return NUStmtLoop(mStmts); }

  //! Constant loop through all the statements.
  NUStmtCLoop loopStmts() const { return NUStmtCLoop(mStmts); }

  //! Return the statements for the tf body.
  /*!
   * The list must be freed by the caller.
   */
  NUStmtList * getStmts() const;

  //! Remove the given statement from the statement list
  /*!
   * The caller is responsible for memory management of the removed
   * statement. The block is responsible for memory management of the
   * replacement statement, if non-NULL.
   *
   * \param stmt The statment being removed.
   *
   * \param replacement If non-NULL, replace the original statement
   *             with this one.
   *
   * If the original statement is not found as a member of the block,
   * the replacement is not inserted.
   *
   * ACA: We should assert if the original is not present or at least
   * return a boolean. If we give no indication of success, it is
   * unclear to the caller who owns the replacement statement.
   */
  void removeStmt(NUStmt *stmt, NUStmt *replacement=0);

  //! Replace the old statement with the new statement.
  /*!
   * \param old_stmt The statment being removed. It must exist as a
   *                 member of this block.
   *
   * \param new_stmt Replace the original statement with this one.
   *
   * The caller is responsible for memory management of the original
   * statement.
   */
  void replaceStmt(NUStmt *old_stmt, NUStmt *new_stmt);

  //! Replace the old statement with the new list of statements.
  /*!
   * \param old_stmt The statement being removed. It must exist as a
   *                 member of this block. 
   *
   * \param new_stmts Replace the original statement with this list of
   *                 statements. This list is copied.
   *
   * The caller is responsible for memory management of the original
   * statement.
   */
  void replaceStmt(NUStmt *old_stmt, NUStmtList *new_stmts);

  //! Replace the current statement list with the given statement list.
  /*!
   * \param new_stmts The new statements.
   *
   * The caller is responsible for memory management of the original
   * set of statements.
   */
  void replaceStmtList(const NUStmtList& new_stmts);

  //! Add the given statement to the end of the block.
  void addStmt(NUStmt *stmt) APIDISTILLMACRO2(APIEMITBIND(mStmts),
                                              APIEMITCODE(mStmts.push_back(stmt)));

  //! Add the given statements to the end of the block.
  void addStmts(NUStmtList *stmts);

  //! Add the given statement to the start of the block.
  void addStmtStart(NUStmt *stmt);

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
   * Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! CodeGen
  CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! Return class name
  const char* typeStr() const;

  //! Generate a symbol for a new net or module.  See NUScope::gensym
  virtual StringAtom* gensym(const char* prefix,
                             const char* name = NULL,
                             const NUBase* proxy = NULL,
                             bool isAlreadyUnique = false);

  //! Blocks are never at the top-level.
  bool atTopLevel() const { return false; }

  //! Generate a BDD based on the def of a net
  /*!
    \param net The net.
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  BDD bdd(NUNet* net, BDDContext *ctx, STBranchNode *scope = 0) const;

  //! calculate cost, add it to NUCost* argument
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! Return a copy of this statement; recurses to copy nested statements.
  virtual NUStmt* copy(CopyContext &copy_context) const;

  //! Moves the local statements and local nets to a new block
  /*!
   * \param dstBlock The target for all contained statements and nets.
   */
  void moveStmts(NUBlock* dstBlock);

  //! Is this block the target of a NUBreak?
  bool isBreakTarget() const { return mIsBreakTarget; }
  //! set the value of the NUBreak target flag
  void putIsBreakTarget( bool isBreakTarget );

  //! Is this block marked as separated
  virtual bool isSeparated () const { return false; }

protected:
  //! Return the net flags to use when creating a locally-scoped temporary
  NetFlags getTempFlags() const { return NetFlags(eTempNet|eAllocatedNet|eBlockLocalNet|eNonStaticNet); }

  //! Netref factory
  NUNetRefFactory *mNetRefFactory APIOMITTEDFIELD;


private:
  //! Hide copy and assign constructors.
  NUBlock(const NUBlock&);
  NUBlock& operator=(const NUBlock&);

  //! Helper to block copy function.
  virtual NUBlock* createNewBlock(CopyContext& copy_context) const;

  //! Statements
  NUStmtList mStmts APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Locally-declared nets in no particular order.
  NUNetList mLocalNetList APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Nested blocks in no particular order.
  NUBlockList mBlockList APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Parent scope
  NUScope *mParent;

  //! Are we the target of a NUBreak?
  bool mIsBreakTarget;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUBlock : public NUBlockStmt, public NUBlockScope

//! \class NUSeparatedBlock
/*! A separated block is identical to a block, except that code is generated 
 *  in a separate function. This is used, for example, to break apart otherwise
 *  too-large generated functions.
 *
 *  A separated block differs from an NUBlock only in that is requires a name,
 *  for generating the function name and the code generation method is
 *  replaced.
 */

class NUSeparatedBlock : public NUBlock {
public:

  //! Constructor for an NUSeparatedBlock.
  /*!
   * \param name   Name of the block, used to construct a function name.
   *
   * \param stmts  List of statements this block will contain. List is
   *               copied. May be NULL.
   *
   * \param parent Parent scope containing this block. Usually, this
   *               is a module, task, or another block.
   *
   * \param iodb   IODB to store intrinsic type information.
   *
   * \param netref_factory Factory used to manage NetRefs; needed for
   *               u/d containers.
   *
   * \param uses_cf_net True when the block needs the $cfnet in its
   *               use set.
   *
   * \param loc    Source location.
   */
  NUSeparatedBlock(StringAtom *name,
    const NUStmtList * stmts,
    NUScope * parent,
    IODBNucleus * iodb,
    NUNetRefFactory * netref_factory,
    bool uses_cf_net,
    const SourceLocator & loc);

  //! Destructor for an NUSeparatedBlock
  virtual ~NUSeparatedBlock();

  //! Separated block are named
  virtual bool isNamed() { return true; }

  //! Blocks are anonymous; they are not named.
  virtual StringAtom * getName() const { return mName; }

  //! CodeGen
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Is this block marked as separated
  virtual bool isSeparated () const { return true; }
  
  //! Get the temporary nets accessed by the block
  NUNetSetByName &getTmpNets () const;

  //! Add a net to the closure set
  /*! \note mClosure is mutable, and this method const, so that the closure can
   *  be from emitCode methods without futzing about with const_cast.
   */
  void addNetToClosure (NUNet *net) const { mClosure.insert (net); }

  //! \return the closure of the block
  NUNetSetByName &getClosure () const { return mClosure; }

  //! Compute the closure of the statements in the block
  /*! The temp nets in the closure must be passed as parameters to the C++
   *  function that implements the block.
   */

  class Callback {
  public:
    Callback (const NUSeparatedBlock *block) : mBlock (block) {}
    virtual ~Callback () {}
    virtual bool operator () (NUNet *) = 0;
  protected:
    const NUSeparatedBlock *mBlock;
  };

  void computeClosure (Callback &, NUNetSet &, bool force = false) const;


private:

  //! Helper to block copy function.
  virtual NUBlock* createNewBlock(CopyContext& copy_context) const;
  //! Helper to createNewBlock function. Uniquifies the name it has
  //! copied from another block.
  void uniquifyCopiedName();

  StringAtom *mName;                    //!< name of the block

  //! Set of all the nets imported into the block
  mutable NUNetSetByName mClosure;      //! set of nets that are passed as parameters
  mutable bool mHaveClosure;            //! set when the closure is computed

  // The closure members are marked as mutable because it is computed in the
  // const NUSeparatedBlock::emitCode method. The closure is not computed until
  // just before it is required because the nucleus may be transformed right up
  // until code generation.

} APIOMITTEDCLASS; //class NUSeparatedBlock : public NUBlock {

#endif
