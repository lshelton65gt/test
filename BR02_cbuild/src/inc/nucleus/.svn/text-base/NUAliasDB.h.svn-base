// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NUALIASDB_H_
#define NUALIASDB_H_

#include "symtab/STFieldBOM.h"
#include "exprsynth/ExprFactory.h"
#include "util/UtHashMap.h"
#include "nucleus/GCCXMLDistillMacros.h"
#include "nucleus/NucleusClassFriendsMacros.h"

// forward declarations.
class NUNet;
class NUScope;
class NUNetRefHdl;
class STSymbolTable;
class STNodeSelectDB;
class ZostreamDB;
class ZistreamDB;
class UtXmlWriter;

//! Bill of Materials for a module's alias db
class NUAliasDataBOM
{
public:
  //! constructor
  NUAliasDataBOM();

  //! destructor
  ~NUAliasDataBOM();

  //! print contents to stdout
  void print() const;
  
  //! Get modifiable underlying net.
  NUNet * getNet();

  //! Get const underlying net.
  const NUNet * getNet() const;

  //! Do we have a valid net?
  bool hasValidNet() const;

  //! Set the underlying net.
  void setNet(NUNet * net);

  //! Get modifiable underlying scope.
  NUScope * getScope();

  //! Get const underlying scope.
  const NUScope * getScope() const;
  
  //! Set the underlying scope.
  void setScope(NUScope * scope);

  //! Determine the appropriate index 
  SInt32 getIndex() const;

  //! Set the appropriate index
  void setIndex(SInt32 index);

  //! Set the node's Ident
  void putIdent(CarbonIdent* ident);

  //! Get the node's ident
  CarbonIdent* getIdent();

  //! const version of getIdent
  const CarbonIdent* getIdent() const;

  void writeXml(UtXmlWriter* xmlWriter) const;

private:
  union {
    NUNet * mNet;
    NUScope * mScope;
  };
  SInt32 mMySymtabIndex;
  CarbonIdent* mNodeIdent APIOMITTEDFIELD;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUAliasDataBOM;

//! BOM Manager for a module's alias db.
class NUAliasBOM : public virtual STFieldBOM
{
public:
  NUAliasBOM();

  //! virtual destructor
  virtual ~NUAliasBOM();

  //! Allocate a Data object to be placed in the symbol table.
  virtual Data allocBranchData();

  //! Allocate a Data object to be placed in the symbol table.
  virtual Data allocLeafData();

  //! Delete the Data object
  virtual void freeBranchData(const STBranchNode*, Data * bomdata);

  //! Delete the Data object
  virtual void freeLeafData(const STAliasedLeafNode*, Data * bomdata);

  //! does nothing
  virtual void preFieldWrite(ZostreamDB&);

  //! Returns eReadOK
  virtual ReadStatus preFieldRead(ZistreamDB&);

  //! Write the BOMData for a leaf node
  virtual void writeLeafData(const STAliasedLeafNode* leaf, ZostreamDB& out) const;

  //! Write the BOMData for a branch node
  virtual void writeBranchData(const STBranchNode* branch, ZostreamDB& out,
                               AtomicCache*) const;

  //! print contents to stdout
  virtual void printLeaf(const STAliasedLeafNode* leaf) const;

  //! print contents to stdout
  virtual void printBranch(const STBranchNode* branch) const;

  //! Currently asserts - not legal
  virtual ReadStatus readLeafData(STAliasedLeafNode* leaf, ZistreamDB& in, 
                                  MsgContext* msg);
  
  //! Currently asserts - not legal
  virtual ReadStatus readBranchData(STBranchNode* branch, ZistreamDB& in, 
                                    MsgContext* msg);

  //! This calls the symboltable's writeDB method 
  //  bool writeData(ZostreamDB& out, STSymbolTable* symtab, STNodeSelectDB* selector) const;

  //! Write the r/w interface signature
  virtual void writeBOMSignature(ZostreamDB& out) const;
  
  //! Read the r/w interface signature
  virtual ReadStatus readBOMSignature(ZistreamDB& in, UtString*);

  virtual const char* getClassName() const;

  //! Write the BOMData for a branch node
  virtual void xmlWriteBranchData(const STBranchNode* branch,UtXmlWriter* writer) const;

  //! Write the BOMData for a leaf node
  virtual void xmlWriteLeafData(const STAliasedLeafNode* leaf,UtXmlWriter* writer) const;



  //! Get the expression factory
  ESFactory* getExprFactory();
  
  //! Map an identifier to an expression
  void mapExpr(CarbonIdent* ident, CarbonExpr* expr);

  //! Given an existing ident, get the describing expression
  /*!
    Returns NULL if the ident is not found or if there is a NULL
    expression describing the ident
  */
  CarbonExpr* getExpr(CarbonIdent* ident);
  //! const version of getExpr
  const CarbonExpr* getExpr(CarbonIdent* ident) const;

  //! This casts the opaque bomdata pointer to a NUAliasDataBOM *
  static NUAliasDataBOM * castBOM(Data bomdata);

  //! Create a CarbonExpr using the net's symbol-table node as an identifier.
  CarbonIdent * createCarbonIdent(NUNet * net);

  //! Put given identifier into the expr factory, and update net
  /*!
   * This places my_symtab_ident into the expr_factory, and puts the
   * resultant CarbonIdent* into the net. 
   */
  CarbonIdent * storeCarbonIdent(NUNet * net,
                                 CarbonIdent * my_symtab_ident);

  //! Creates a backpointer CarbonExpr using the net's symbol-table node as an ident.
  /*!
    \param net The net to create an ident for. The ident will contain
    a backpointer expression (bitsel or partsel) of the net in
    origNetRefHdl
    \param origNetRefHdl The reference to a bitsel or partsel of the
    net that is represented by the parameter 'net'.

    For example, 'net' may be $portsplit_d_2_2, the backpointer
    expression that will be created, assuming origNetRefHdl contains a
    reference to the 2nd bit in d would be d[2].
  */
  CarbonIdent * createCarbonIdentBP(NUNet * net,
                                    const NUNetRefHdl* origNetRefHdl);

  //! Creates a backpointer ident given the backpointer expression.
  /*!
    \param net The net to create an ident for. The ident will contain
    the backpointer expression that is passed in
    \param backPointer The expression representing the parameter
    'net'.

    For example, 'net' may be $vec_b_c_a. The backpointer expression
    should be {b,c,a}.
  */
  CarbonIdent * createCarbonIdentBP(NUNet * net,
                                    CarbonExpr* backPointer);

  //! Return the ident->expression map
  CarbonIdentExprMap* getIdentExprMap() {
    return &mExprMap;
  }

  //! Sanitize the expression map
  /*!
    This removes any nested expressions, and remaps idents if any
    expressions are nested.
  */
  void sanitizeExpressions();
  
private:

  ESFactory mExprFactory APIOMITTEDFIELD;
  CarbonIdentExprMap mExprMap APIOMITTEDFIELD;

  void freeData(Data * bomdata);
  Data allocData();
  void printData(const Data bomdata) const;  

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS;//class NUAliasBOM : public virtual STFieldBOM

#endif // NUALIASDB_H_
