// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef NUFOR_H_
#define NUFOR_H_

#include "nucleus/NUBlockStmt.h"

//! NUFor class
/*!
 * A for loop, while loop, or repeat.
 *
 * Since we use NUFor to cover a variety of statement types, we allow
 * the initial statements to be empty, and also allow the advance statements
 * to be empty (more like a C for loop than a Verilog for loop).
 *
 * This will also cover a forever, once forever is implemented.
 */
class NUFor : public NUBlockStmt
{
public:
  //! constructor
  /*!
    \param initial Statements for Initial assignment.  May be empty.
    \param condition Conditional expression indicating loop termination.
    \param advance Statements to step assignment for next iteration.  May be empty.
    \param body Statements for the loop body.
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param usesCFNet true when this needs $cfnet in it's use list
    \param loc Source location.
  */
  NUFor(const NUStmtList & initial,
	NUExpr *           condition,
        const NUStmtList & advance,
	const NUStmtList & body,
	NUNetRefFactory *netref_factory,
        bool usesCFNet,
	const SourceLocator & loc);
  

  //! Return the statements for the initial section 
  /*!
   * If the arg is true then a copy of the statements is made, else
   * just a new list is provided.
   * In all cases the list must be freed by the caller.
   */
  NUStmtList * getInitial(bool copy_statements) const;
  
  //! Remove the given statement from the for loop initial section
  void removeInitialStmt(NUStmt *stmt, NUStmt *replacement=0);

  //! Replace the current statement list for the initial section with the given statement list.
  /*!
   * The given list is copied.
   * This is used to reorder non-blocking temporaries.
   * The caller is responsible for making sure that statements are not leaked.
   */
  void replaceInitial(const NUStmtList& new_stmts);

  //! Loop through all the statements in the initial section
  NUStmtLoop loopInitial() {return NUStmtLoop(mInitialStmts);}
  
  //! loop through all the statements in the initial section
  NUStmtCLoop loopInitial() const {return NUStmtCLoop(mInitialStmts);}
  

  //! Return the conditional expression.
  NUExpr * getCondition() const { return mCondition; }
  
  //! Set the conditional expression.
  void setCondition (NUExpr*condition) {mCondition = condition;}

  //! Return the statements for the advance section 
  /*!
   * If the arg is true then a copy of the statements is made, else
   * just a new list is provided.
   * In all cases the list must be freed by the caller.
   */
  NUStmtList * getAdvance(bool copy_statements) const;

  //! Remove the given statement from the loop advance section
  void removeAdvanceStmt(NUStmt *stmt, NUStmt *replacement=0);

  //! Replace the current statement list for the advance section with the given statement list.
  /*!
   * The given list is copied.
   * This is used to reorder non-blocking temporaries.
   * The caller is responsible for making sure that statements are not leaked.
   */
  void replaceAdvance(const NUStmtList& new_stmts);

  //! Loop through all the statements in the advance section
  NUStmtLoop loopAdvance() {return NUStmtLoop(mAdvanceStmts);}
  
  //! loop through all the statements in the advance section
  NUStmtCLoop loopAdvance() const {return NUStmtCLoop(mAdvanceStmts);}
  

  //! Loop through all the statements in the loop body
  NUStmtLoop loopBody() {return NUStmtLoop(mBodyStmts);}
  
  //! loop through all the statements in the loop body
  NUStmtCLoop loopBody() const {return NUStmtCLoop(mBodyStmts);}

  //! Attempt to estimate the number of times a loop will execute, or return false if it's too complex
  bool estimateLoopCount(SInt32* count) const;

  //! Return the statements for the loop body (note this is a copy of the statement list)
  /*!
   * The list must be freed by the caller.
   */
  NUStmtList * getBody() const;

  //! Remove the given statement from the loop body
  void removeBodyStmt(NUStmt *stmt, NUStmt *replacement=0);

  //! Replace the current statement list for the body with the given statement list.
  /*!
   * The given list is copied.
   * This is used to reorder non-blocking temporaries.
   * The caller is responsible for making sure that statements are not leaked.
   */
  void replaceBody(const NUStmtList& new_stmts);

  //! Add the given statement to the end of the block
  void addBodyStmt(NUStmt *stmt);

  //! Add the given statement to the start of the block
  void addBodyStmtStart(NUStmt *stmt);

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet *new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose (UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! Return class name
  const char* typeStr() const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! Return a copy of this statement; recurses to copy nested statements.
  NUStmt* copy(CopyContext &copy_context) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! Return the type of this class
  virtual NUType getType() const { return eNUFor; }

  //! destructor
  ~NUFor();

private:
  //! Hide copy and assign constructors.
  NUFor(const NUFor&);
  NUFor& operator=(const NUFor&);
  
  //! Initial assignment.
  // NUBlockingAssign * mInitial;
  NUStmtList  mInitialStmts;

  //! Conditional expression indicating loop termination.
  NUExpr *           mCondition;

  //! Statements that implement the assignment for the next iteration
  NUStmtList mAdvanceStmts;

  //! Statements for the loop body.
  NUStmtList mBodyStmts;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUFor : public NUBlockStmt

#endif // NUFOR_H_
