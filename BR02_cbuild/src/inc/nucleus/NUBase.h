// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NUBASE_H_
#define NUBASE_H_

#include "nucleus/GCCXMLDistillMacros.h"
#include "nucleus/NucleusClassFriendsMacros.h"

/*!
  \file
  Declaration of the base Nucleus class from which all Nucleus classes are derived.
*/
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif


class NUDesign;
class SourceLocator;
class UtString;

//!Encapsulate context information passed down for code generation
typedef UInt64 CGContext_t;

class STSymbolTable;

//! NUBase class
/*!
 * Abstract base class for all Nucleus objects.
 */
class NUBase
{
public:
  //! constructor
  NUBase();

  //! debug help: print routine
  virtual void print(bool recurse = true, int indent = 0) const;

  //! debug help: non-recursing print routine (quick to type, no accidents)
  void pr() const;              // print(false,0); not inlined.

  //! debug help: return the type of what I'm looking at
  virtual const char* typeStr() const = 0;

  //! debug help: print elaborated object
  virtual void  printElab(STSymbolTable*, bool recurse = true, int indent = 0)
    const;

  //!Code Generator
  /*!Generate equivalent C++ code for the verilog object.
   * \returns CGContext_t providing information about code generated.
   */
  virtual CGContext_t emitCode(CGContext_t context) const = 0;

  //! Is this an initial block.
  /*!
   * Assume it is not an initial block and let derived classes
   * override it.
   */
  virtual bool isInitial() const { return false; }

  //! Does this node represent state (e.g. sequential always, latch...)
  /*!
   * Assume it is not a sequential block and let derived classes
   * override it.
   */
  virtual bool isSequential() const { return false; }

  //! Does this node represent a priority block (e.g. async pre/clear)
  /*!
   * Assume it is not a priority block and let derived classes
   * override it.
   */
  virtual bool isPriorityBlock() const { return false; }

  //! Does this node represent a cycle (asynchronous loop)
  /*!
   * Assume it is not a cycle and let derived classes override it.
   */
  virtual bool isCycle() const { return false; }

  //! Return true if this object represents a cross-hierarchicy reference
  /*!
   * Assume it is not a hier ref and let derived classes override it.
   */
  virtual bool isHierRef() const { return false; }

  //! Add the given object as a possible hierarchical reference to this obj.
  /*! In most cases this operation is illegal. In the cases that it
   *  isn't, the function should be overriden.
   */
  virtual void addHierRef(NUBase*);

  //! Remove the given object as a possible hierarchical reference
  /*! In most cases this operation is illegal. In the cases that it
   *  isn't, the function should be overriden.
   */
  virtual void removeHierRef(NUBase*);

  //! Query state of marker
  bool getMark() { return baseFlags.mMark; }

  //! Set state of marker
  void putMark(bool flag) { baseFlags.mMark = flag; }

  //! destructor
  virtual ~NUBase();

  //! Static method to enable NU_ASSERT
  static void enableAsserts();
  
  //! Static method to disable NU_ASSERT (used to prevent recursive/nested asserts)
  static void disableAsserts();

  //! Test if NU_ASSERT is OK (used in NU_ASSERT macros)
  static bool assertOK();

  //! Utility function to print N spaces for indenting debug print code
  static void indent(int numSpaces);

  //! Print header information about an assertion.
  static void printAssertInfoHeader();

  //! Print information about a single object associated with an assertion.
  void printAssertInfo() const;

  //! Print detailed footer information about an assertion.
  static void printAssertInfoFooter(const char* file, int line, const char* exprStr);

  //! Print design object information about an assertion
  virtual void printAssertInfoHelper() const;

  //! Make sure an NUDesignWalker hits all nucleus objects
  static void checkDesignWalker(NUDesign*, const char* phase);

  //! Compose location information for a protected object
  static void composeProtectedNUObject(SourceLocator const &loc, UtString *buf);

  //! increment a count of the number of active NUDesigns
  static void incDesignCount();

  //! decrement a count of the number of active NUDesigns
  static void decDesignCount();


private:
  //! Hide copy and assign constructors.
  NUBase(const NUBase&);
  NUBase& operator=(const NUBase&);

  struct
  {
  /*!
   * Marker for doing aliveness analysis.  State is not guaranteed, you must set/clear it
   * before you use it.
   */
    bool mMark:1;
  } baseFlags APIOMITTEDFIELD;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUBase

#ifndef __STRING
//! Stringify expression
#define __STRING(exp) #exp
#endif

// Note: these macros are written this way so that they can be invoked like
// functions, with the final semicolon supplied by the user.  A brace-delimited
// statement is no good, because it leads to syntax errors with code like:
// if (cond)
//   NU_ASSERT(predicate,net);
// else
//   ...

#define NU_ASSERT(exp, nucleusObj) \
  do { \
    if (!(exp) && NUBase::assertOK()) { \
      NUBase::disableAsserts(); \
      NUBase::printAssertInfoHeader(); \
      (nucleusObj)->printAssertInfo(); \
      NUBase::printAssertInfoFooter(__FILE__, __LINE__, __STRING(exp)); \
      NUBase::enableAsserts(); \
    } \
  } while (0) /* no trailing semicolon -- it is supplied by the caller */

#define NU_ASSERT2(exp, nucleusObj1, nucleusObj2) \
  do { \
    if (!(exp) && NUBase::assertOK()) { \
      NUBase::disableAsserts(); \
      NUBase::printAssertInfoHeader(); \
      (nucleusObj1)->printAssertInfo(); \
      (nucleusObj2)->printAssertInfo(); \
      NUBase::printAssertInfoFooter(__FILE__, __LINE__, __STRING(exp)); \
      NUBase::enableAsserts(); \
    } \
  } while (0) /* no trailing semicolon -- it is supplied by the caller */

#endif
