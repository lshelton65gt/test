// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUMODULEINSTANCE_H_
#define NUMODULEINSTANCE_H_

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUNetRefMap.h"

#include "util/SourceLocator.h"

#include "util/LoopFilter.h"
#include "util/LoopFunctor.h"

class STBranchNode;
class StringAtom;

/*!
  \file
  Declaration of the Nucleus module instance class.
*/

//! NUModuleInstance class
/*!
 * Instantiation of a module, macromodule, or UDP.
 *
 */
class NUModuleInstance : public NUUseDefNode
{
public:
  //! constructor
  /*!
    \param name Local instance name, not expanded hierarchically.
    \param parent NUModule/NamedDeclarationScope which is performing the instantiation.
    \param module Module which is being instantiated.
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param loc Source location of the instantiation.
   */
  NUModuleInstance(
    StringAtom* name,
    NUScope* parent,
    NUModule* module,
    NUNetRefFactory *netref_factory,
    const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUModuleInstance; }

  //! Return the source location of the instantiation.
  const SourceLocator& getLoc() const { return mLoc; }

  //! Instance name
  StringAtom* getName() const { return mName; }

  //! puts into buf, the name of this instance including any
  //! namedDeclarationScope prefixes that might exist in the
  //! instantiating entity 
  void getScopedName(UtString* buf, bool* previous_was_protected);

  //! Return the module/declaration scope performing the instantiation.
  NUScope* getParent() const { return mParent; }
  //! Return the module/declaration scope base object performing the instantiation.
  NUBase* getParentBase() const;

  //! If the parent scope is module, return it. If it is declaration
  //! scope, iterate down the parent hierarchy till module is found
  //! and return it.
  NUModule* getParentModule() const;

  //! Return the module being instantiated.
  NUModule* getModule() const { return mModule; }

  //! Returns map of formals to connection, the caller is responsible for calling delete on the returned map.
  NUPortConnectionMap *getPortMap() const;

  //! loop over all port connections
  NUPortConnectionLoop loopPortConnections() {
    return NUPortConnectionLoop(mPortConnections);
  }

  //! loop over all port connections
  NUPortConnectionCLoop loopPortConnections() const {
    return NUPortConnectionCLoop(mPortConnections);
  }

  //! Class to filter port connections so we can generate loops for them.
  class PortConnectionFilter
  {
  public:
    typedef bool (NUUseDefNode::* FilterFn)() const;
    PortConnectionFilter() : mFn(0) {}
    PortConnectionFilter(FilterFn fn) : mFn(fn) {}
    bool operator()(NUPortConnection* conn) { return (conn->*mFn)(); }
  private:
    FilterFn mFn;
  };

  typedef LoopFilter<NUPortConnectionLoop,PortConnectionFilter> PortConnectionLoopFilter;
  typedef LoopFilter<NUPortConnectionCLoop,PortConnectionFilter> PortConnectionCLoopFilter;

  class CoercePortConnectionToInput {
  public: 
    typedef NUPortConnectionInput* value_type;
    typedef NUPortConnectionInput* reference;
    NUPortConnectionInput * operator()(NUPortConnection* conn) const;
    const NUPortConnectionInput * operator()(const NUPortConnection* conn) const;
  };
  class CoercePortConnectionToOutput {
  public: 
    typedef NUPortConnectionOutput* value_type;
    typedef NUPortConnectionOutput* reference;
    NUPortConnectionOutput * operator()(NUPortConnection* conn) const;
    const NUPortConnectionOutput * operator()(const NUPortConnection* conn) const;
  };
  class CoercePortConnectionToBid {
  public: 
    typedef NUPortConnectionBid* value_type;
    typedef NUPortConnectionBid* reference;
    NUPortConnectionBid * operator()(NUPortConnection* conn) const;
    const NUPortConnectionBid * operator()(const NUPortConnection* conn) const;
  };

  typedef LoopFunctor<PortConnectionLoopFilter,CoercePortConnectionToInput>   PortConnectionInputLoop;
  typedef LoopFunctor<PortConnectionCLoopFilter,CoercePortConnectionToInput>  PortConnectionInputCLoop;
  typedef LoopFunctor<PortConnectionLoopFilter,CoercePortConnectionToOutput>  PortConnectionOutputLoop;
  typedef LoopFunctor<PortConnectionCLoopFilter,CoercePortConnectionToOutput> PortConnectionOutputCLoop;
  typedef LoopFunctor<PortConnectionLoopFilter,CoercePortConnectionToBid>     PortConnectionBidLoop;
  typedef LoopFunctor<PortConnectionCLoopFilter,CoercePortConnectionToBid>    PortConnectionBidCLoop;

  PortConnectionInputLoop loopInputPortConnections() {
    NUPortConnectionLoop outerloop = loopPortConnections();
    PortConnectionFilter filterfunc(&NUUseDefNode::isPortConnInput);
    PortConnectionLoopFilter loopfilter(outerloop,filterfunc);
    return PortConnectionInputLoop(loopfilter);
  }

  PortConnectionInputCLoop loopInputPortConnections() const {
    NUPortConnectionCLoop outerloop = loopPortConnections();
    PortConnectionFilter filterfunc(&NUUseDefNode::isPortConnInput);
    PortConnectionCLoopFilter loopfilter(outerloop,filterfunc);
    return PortConnectionInputCLoop(loopfilter);
  }

  PortConnectionOutputLoop loopOutputPortConnections() {
    NUPortConnectionLoop outerloop = loopPortConnections();
    PortConnectionFilter filterfunc(&NUUseDefNode::isPortConnOutput);
    PortConnectionLoopFilter loopfilter(outerloop,filterfunc);
    return PortConnectionOutputLoop(loopfilter);
  }

  PortConnectionOutputCLoop loopOutputPortConnections() const {
    NUPortConnectionCLoop outerloop = loopPortConnections();
    PortConnectionFilter filterfunc(&NUUseDefNode::isPortConnOutput);
    PortConnectionCLoopFilter loopfilter(outerloop,filterfunc);
    return PortConnectionOutputCLoop(loopfilter);
  }

  PortConnectionBidLoop loopBidPortConnections() {
    NUPortConnectionLoop outerloop = loopPortConnections();
    PortConnectionFilter filterfunc(&NUUseDefNode::isPortConnBid);
    PortConnectionLoopFilter loopfilter(outerloop,filterfunc);
    return PortConnectionBidLoop(loopfilter);
  }

  PortConnectionBidCLoop loopBidPortConnections() const {
    NUPortConnectionCLoop outerloop = loopPortConnections();
    PortConnectionFilter filterfunc(&NUUseDefNode::isPortConnBid);
    PortConnectionCLoopFilter loopfilter(outerloop,filterfunc);
    return PortConnectionBidCLoop(loopfilter);
  }

  //! Add all port connections to the given list, in module port list order.
  void getPortConnections(NUPortConnectionList &conn_list) const;

  //! Return the port connection for the given 0-based port index.
  NUPortConnection* getPortConnectionByIndex(int index) const;

  //! Add all input port connections to the given list, in module port list order.
  void getInputPortConnections(NUPortConnectionInputList &conn_list) const;

  //! Add all output port connections to the given list, in module port list order.
  void getOutputPortConnections(NUPortConnectionOutputList &conn_list) const;

  //! Add all bid port connections to the given list, in module port list order.
  void getBidPortConnections(NUPortConnectionBidList &conn_list) const;

  //! Add the nets this instantiation uses to the given set.
  /*!
   * A module instance uses anything connected to formal inputs or bids
   * (ie, the actual expressions connected to them)
   */
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this instantiation uses to def net to the given set.
  /*!
   * The given net must be a def of the instantiation (so, it must be connected
   * to a formal output or bid).
   */
  void getUses(NUNet *net, NUNetSet *uses) const;
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the set of nets this instantiation defs to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Clear the use/def information stored in the object, if any.
  void clearUseDef();

  //! Fixup the use/def information; remove all nets in the given list from the sets.
  void fixupUseDef(NUNetList *remove_list);

  //! Set the given port connections as the port connections for this instantiation.
  void setPortConnections(NUPortConnectionVector& connections); 

  //! Add the given port connection to the port  connections for this instantiation.
  void addPortConnection(NUPortConnection* connection) APIDISTILLMACRO2(APIEMITBIND(mPortConnections),
                                                                        APIEMITCODE(mPortConnections.push_back(connection)));



  //! Add the given net to the set of nets being defined by this node.
  void addDef(const NUNetRefHdl &def_net_ref);

  //! Add the use_net as being used by the given def_net.
  void addUse(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);

  //! Add the use_set as being used by the given def_net.
  void addUses(const NUNetRefHdl &def_net_hdl, NUNetRefSet *use_set);

  //! Return the elaborated module for this instantiation in the given hierarchy.
  NUModuleElab *lookupElab(STBranchNode *hier) const;

  //! Return my index in the symbol table.
  SInt32 getSymtabIndex() const APIDISTILLMACRO1(APIEMITCODE(return mMySymtabIndex ));

  //!Generate C++ code
  virtual CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int ident) const;

  //! Return a UtString identifying this type.
  virtual const char* typeStr() const;

  //! add the cost of this into the NUCost*.
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! Is this node a continuous driver?
  virtual bool isContDriver() const { return true; }

  //! Return the parent module (illegal)
  const NUModule* findParentModule() const { NU_ASSERT(0=="module not scoped", this); return NULL; }
  NUModule* findParentModule() { NU_ASSERT (0=="module not scoped", this); return NULL; }

  //! destructor
  ~NUModuleInstance();

  //! Used by the congruency analyzer to replace a module with its
  //! congruent parameterized version
  void putModule(NUModule*);

  //! Used by the congruency analyzer to add input parameter to
  //! share module implementations that differ only by constants
  void addConnection(NUPortConnection*);

  //! Compare routine for module instances.
  static int compare(const NUModuleInstance* i1, const NUModuleInstance* i2);


private:
  //! Hide copy and assign constructors.
  NUModuleInstance(const NUModuleInstance&);
  NUModuleInstance& operator=(const NUModuleInstance&);

  //! Index of me in the symbol table
  SInt32 mMySymtabIndex APIDISTILLMACRO2(APICONSTRUCTORFALSE,
                                         APIEMITCODE(mMySymtabIndex = mParent->reserveSymtabIndex()) );


  //! NUModule/NUNamedDeclarationScope doing the instantiating.
  NUScope* mParent;

  //! Module being instantiated.
  NUModule *mModule;

  //! Local instance name.
  StringAtom *mName;

  //! Source location of this instance.
  SourceLocator mLoc;

  //! Ordered container of port connections, in module port list order.
  NUPortConnectionVector mPortConnections APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Map of port defs to their "use"s; uses net refs
  NUNetRefNetRefMultiMap mPortNetRefDefMap APIOMITTEDFIELD;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory APIOMITTEDFIELD;

  DECLARENUCLEUSCLASSFRIENDS();

}  APIDISTILLCLASS; // class NUModuleInstance

//! compare a module instance by name
struct NUModuleInstanceCmp
{
  bool operator()(const NUModuleInstance* m1, const NUModuleInstance* m2) const
  {
    return NUModuleInstance::compare(m1, m2) < 0;
  }      
};

#endif
