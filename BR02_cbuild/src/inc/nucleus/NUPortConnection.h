// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUPORTCONNECTION_H_
#define NUPORTCONNECTION_H_

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUCopyContext.h"
#include "util/SourceLocator.h"

/*!
  \file
  Declaration of the Nucleus port connection class.
*/

//! NUPortConnection class
/*!
 * Abstract base class representing a port connection.
 */
class NUPortConnection : public NUUseDefNode
{
public:
  //! constructor
  /*!
    \param loc Source location.
  */
  NUPortConnection(NUNet* formal, const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const = 0;

  //! Return the direction of the port connection
  virtual PortDirectionT getDir() const = 0;

  //! Return the formal net
  NUNet* getFormal() const { return mFormal; }

  //! Change the formal net
  void putFormal(NUNet* formal) { mFormal = formal; }

  //! Return the actual net (asserts if complete port lowering has not occurred)
  virtual NUNet* getActualNet() const = 0;

  //! Return the set of actual nets in the provided set
  virtual void getActuals(NUNetSet* actuals) const = 0;

  //! Return true if there is no actual connected to the port.
  virtual bool isUnconnectedActual() const = 0;

  //! Get source location
  const SourceLocator& getLoc() const { return mLoc; }

  //! Return the module instance to which this port connection belongs.
  NUModuleInstance* getModuleInstance() const;

  //! Set the module instance to which this port connection belongs.
  //! Can only be called once.
  void setModuleInstance(NUModuleInstance *inst);

  //! Return the set of nets this node reads from
  virtual void getUses(NUNetSet *uses) const = 0;
  virtual void getUses(NUNetRefSet *uses) const = 0;
  virtual bool queryUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const = 0;

  //! Return the set of nets this node reads from when defining the given net
  virtual void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const = 0;
  virtual bool queryUses(const NUNetRefHdl &def_net_ref,
			 const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const = 0;

  //! Return the set of nets for which this node defines a value
  virtual void getDefs(NUNetSet *defs) const = 0;
  virtual void getDefs(NUNetRefSet *defs) const = 0;
  virtual bool queryDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const = 0;

  //! Change this node to define new_net instead of old_net.
  virtual void replaceDef(NUNet *old_net, NUNet* new_net) = 0;
  
  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net) = 0;

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const = 0;

  //! Return class name
  virtual const char* typeStr() const = 0;

  //! Return a copy of this port connection.
  virtual NUPortConnection* copy(CopyContext & copy_context) const = 0;

  //! Return the parent module
  const NUModule* findParentModule() const;
  NUModule* findParentModule();

  //! symbolic printing
  virtual void compose(UtString*, const STBranchNode* /* scope */, int indent, bool recurse) const;

  //! code generation
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Check if the port connection can be aliased
  /*! This routine checks if we have whole identifiers and compatiable
   *  sizes. This is different for the various port connections.
   */
  virtual bool isAliasable() const = 0;

  //! Return true for a port connection
  virtual bool isPortConn() const;

  //! destructor
  ~NUPortConnection();

protected:
  //! Source location of port connection.
  SourceLocator mLoc;

  //! Instance for which this is a port connection.
  NUModuleInstance *mInst;

  //! The formal of the connection.
  NUNet *mFormal;

private:
  //! Hide copy and assign constructors.
  NUPortConnection(const NUPortConnection&);
  NUPortConnection& operator=(const NUPortConnection&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUPortConnection : public NUUseDefNode


//! NUPortConnectionInput class
/*!
 * Port connection to formals which are inputs.
 * The actual is an NUExpr and the formal is an NUNet.
 *
 * TBD: Note, most likely the formal will be changed from NUNet to NULvalue.
 */
class NUPortConnectionInput : public NUPortConnection
{
public:
  //! constructor
  /*!
    \param actual Actual expression, may be 0 if port is unconnected.
    \param formal Formal net
    \param loc Source location.
  */
  NUPortConnectionInput(NUExpr *actual,
			NUNet *formal,
			const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUPortConnectionInput; }

  //! Return the direction of the port connection
  PortDirectionT getDir() const { return eInput; }

  //! Return the actual expression
  NUExpr* getActual() const { return mActual; }

  //! Return the actual net (assumes port lowering has occurred)
  virtual NUNet* getActualNet() const;

  //! Return the set of actual nets in the provided set
  virtual void getActuals(NUNetSet* actuals) const;

  //! Updates the actual expression -- caller is responsible for memory management.
  void setActual(NUExpr *actual) { mActual = actual; }

  //! Return true if there is no actual connected to the port.
  bool isUnconnectedActual() const { return (mActual == 0); }

  //! Return true if the actual is a simple expression.
  bool isSimple() const;

  //! Return the set of nets this node reads from
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Return the set of nets this node reads from when defining the given net
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Return the set of nets for which this node defines a value
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Does this node represent an input port connection.
  bool isPortConnInput() const { return true; }

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const;

  //! Return class name
  virtual const char* typeStr() const;

  //! Return a copy of this port connection.
  virtual NUPortConnection* copy(CopyContext & copy_context) const;

  //! add the cost of this into the NUCost*.
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! Check if the port connection can be aliased
  virtual bool isAliasable() const;

  //! destructor
  ~NUPortConnectionInput();

private:
  //! Hide copy and assign constructors.
  NUPortConnectionInput(const NUPortConnectionInput&);
  NUPortConnectionInput& operator=(const NUPortConnectionInput&);

  //! The actual of the connection.
  NUExpr *mActual;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUPortConnectionInput : public NUPortConnection


//! NUPortConnectionOutput class
/*!
 * Port connection to formals which are outputs.
 * The actual is an NULvalue and the formal is an NUNet.
 *
 * TBD: Note, most likely the formal will be changed from NUNet to NUExpr.
 */
class NUPortConnectionOutput : public NUPortConnection
{
public:
  //! constructor
  /*!
    \param actual Actual Lvalue, may be 0 if port is unconnected.
    \param formal Formal net
    \param loc Source location.
  */
  NUPortConnectionOutput(NULvalue *actual,
			 NUNet *formal,
			 const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUPortConnectionOutput; }

  //! Return the direction of the port connection
  PortDirectionT getDir() const { return eOutput; }

  //! Return the actual Lvalue
  NULvalue* getActual() const { return mActual; }

  //! Return the actual net (assumes port lowering has occurred)
  virtual NUNet* getActualNet() const;

  //! Updates the actual expression -- caller is responsible for memory management.
  void setActual(NULvalue *actual) { mActual = actual; }

  //! Return the set of actual nets in the provided set
  virtual void getActuals(NUNetSet* actuals) const;

  //! Return true if there is no actual connected to the port.
  bool isUnconnectedActual() const { return (mActual == 0); }

  //! Return the set of nets this node reads from
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Return the set of nets this node reads from when defining the given net
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Return the set of nets for which this node defines a value
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Does this node represent an output port connection.
  bool isPortConnOutput() const { return true; }

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const;

  //! Return class name
  virtual const char* typeStr() const;

  //! Return a copy of this port connection.
  virtual NUPortConnection* copy(CopyContext & copy_context) const;

  //! add the cost of this into the NUCost*.
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! Check if the port connection can be aliased
  virtual bool isAliasable() const;

  //! destructor
  ~NUPortConnectionOutput();

private:
  //! Hide copy and assign constructors.
  NUPortConnectionOutput(const NUPortConnectionOutput&);
  NUPortConnectionOutput& operator=(const NUPortConnectionOutput&);

  //! The actual of the port connection.
  NULvalue *mActual;

  DECLARENUCLEUSCLASSFRIENDS();

}APIDISTILLCLASS; //class NUPortConnectionOutput : public NUPortConnection


//! NUPortConnectionBid class
/*!
 * Port connection to formals which are bidirects.
 * The actual is an NULvalue and the formal is an NUNet.
 *
 * TBD: Note, most likely the formal will be changed from NUNet to NULvalue.
 */
class NUPortConnectionBid : public NUPortConnection
{
public:
  //! constructor
  /*!
    \param actual Actual Lvalue, may be 0 if port is unconnected.
    \param formal Formal net
    \param loc Source location.
  */
  NUPortConnectionBid(NULvalue *actual,
		      NUNet *formal,
		      const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUPortConnectionBid; }

  //! Return the direction of the port connection
  PortDirectionT getDir() const { return eBid; }

  //! Return the actual Lvalue
  NULvalue* getActual() const { return mActual; }

  //! Return the actual net (assumes port lowering has occurred)
  virtual NUNet* getActualNet() const;

  //! Updates the actual expression -- caller is responsible for memory management.
  void setActual(NULvalue *actual) { mActual = actual; }

  //! Return the set of actual nets in the provided set
  virtual void getActuals(NUNetSet* actuals) const;

  //! Return true if there is no actual connected to the port.
  bool isUnconnectedActual() const { return (mActual == 0); }

  //! Return the set of nets this node reads from
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Return the set of nets this node reads from when defining the given net
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Return the set of nets for which this node defines a value
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Does this node represent a bid port connection.
  bool isPortConnBid() const { return true; }

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const;

  //! Return class name
  virtual const char* typeStr() const;

  //! Return a copy of this port connection.
  virtual NUPortConnection* copy(CopyContext & copy_context) const;

  //! add the cost of this into the NUCost*.
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! Check if the port connection can be aliased
  virtual bool isAliasable() const;

  //! destructor
  ~NUPortConnectionBid();

private:
  //! Hide copy and assign constructors.
  NUPortConnectionBid(const NUPortConnectionBid&);
  NUPortConnectionBid& operator=(const NUPortConnectionBid&);

  NULvalue *mActual;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUPortConnectionBid : public NUPortConnection


#endif
