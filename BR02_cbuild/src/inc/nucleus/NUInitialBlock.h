// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Declaration of the Nucleus initial block class.
*/

#ifndef NUINITIALBLOCK_H_
#define NUINITIALBLOCK_H_

#include "nucleus/NUStructuredProc.h"

class SourceLocator;
class CopyContext;

//! NUInitialBlock class
/*!
 *  Model an initial block.  Note that an initial block really just has one
 *  statement, but a lot of the time that statement is a begin/end block.
 *  So, we just collapse that begin/end block into this initial block, for
 *  convenience.
*/
class NUInitialBlock : public NUStructuredProc
{
public:
  //! constructor
  /*!
    \param name  Name used for code generation.
    \param block Block which will contain the local variables and statements.
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param loc The source location of the initial block.
   */
  NUInitialBlock(StringAtom * name,
                 NUBlock * block,
		 NUNetRefFactory *netref_factory,
		 const SourceLocator& loc);
  
  //! Return the type of this class
  virtual NUType getType() const { return eNUInitialBlock; }

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose (UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const;

  //! Return a UtString identifying this type.
  virtual const char* typeStr() const;

  //! Does this node represent an initial block or statement
  virtual bool isInitial() const { return true; }

  //! Is this block similar to another one in terms of i/os & behavior?
  virtual bool isSimilar(const NUStructuredProc& other) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUInitialBlock();

  //! Make a copy of this initial-block
  NUInitialBlock* clone(CopyContext & copy_context) const;

private:
  //! Hide copy and assign constructors.
  NUInitialBlock(const NUInitialBlock&);
  NUInitialBlock& operator=(const NUInitialBlock&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS ; //class NUInitialBlock : public NUStructuredProc

#endif
