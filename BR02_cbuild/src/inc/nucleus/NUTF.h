// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef NUTF_H_
#define NUTF_H_

#include "nucleus/NUScope.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlockingMaps.h"

class CopyContext;

/*!
  \file
  Declaration of the Nucleus task (and extinct function) classes.
*/

//! NUTF class
/*!
 * Abstract base class for tasks, and the now extinct, functions.
 */
class NUTF : public NUBlockingMaps, public NUBlockScope
{
public:

//! Argument Passing Conventions for NUTFArgConnection and NUTF
  enum CallByMode {
    eCallByAny = 0,               //!< No required conventions - backend chooses
    eCallByValue,                 //!< C-style, copy-in
    eCallByReference,             //!< Pass a pointer to argument
    eCallByCopyOut,               //!< Output parameter by reference copy
    eCallByCopyInOut              //!< TFRewrite could replace this as it lowers arguments
  };

  typedef UtVector<CallByMode> CallByModeVector;

  //! constructor
  /*!
    \param parent Module in which declared
    \param name_branch The symbol table node for the task/function
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param loc The source location of the task/function
    \param iodb the IODB for this scope
   */
  NUTF(NUModule *parent,
       STBranchNode* name_branch,
       NUNetRefFactory *netref_factory,
       const SourceLocator& loc,
       IODBNucleus *iodb);

  //! constructor
  /*!
    \param parent Module in which declared
    \param name The name of the task/function
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param loc The source location of the task/function
    \param iodb the IODB for this scope
   */
  NUTF(NUModule *parent,
       StringAtom* name,
       StringAtom* originalName,
       NUNetRefFactory *netref_factory,
       const SourceLocator& loc,
       IODBNucleus *iodb);

  //! Return the type of this class
  virtual NUType getType() const { return eNUTF; }

  //! Return the locally relative name.
  StringAtom* getName() const;

  //! Return the original name of the module.
  StringAtom* getOriginalName() const;

  //! Return the branch node.
  STBranchNode * getNameBranch() const;

  //! From NUScope; return this as the base object.
  NUBase* getBase() { return this; }
  const NUBase* getBase() const { return this; }

  //! Where is this node defined in the verilog source?
  const SourceLocator& getLoc() const { return mLoc; }

  //! Realisation of NUScope::getSourceLocation abstract virtual method
  virtual const SourceLocator getSourceLocation () const { return getLoc (); }

  //! Get the parent module for this use def node
  const NUModule* findParentModule() const { return getParentModule(); }
  NUModule* findParentModule() { return getParentModule(); }

  //! Return the parent scope
  NUScope *getParentScope() const { return mParent; }

  /*! Return the parent scope as specified in the HDL, if not correctly
    represented in nucleus.
  */
  virtual NUScope *getHdlParentScope() const { return mHdlParent; }
  void setHdlParentScope( NUScope *scope );

  //! Add the net to the locally-defined nets.
  //! \param net The net to add
  void addLocal(NUNet *net) APIDISTILLMACRO2(APIEMITBIND(mLocalNetList),
                                             APIEMITCODE(mLocalNetList.push_back(net)));

  //! Remove the net from this module's set of locally-defined nets.
  void removeLocal(NUNet* net, bool remove_references=true);

  //! remove a set of nets (more efficient than calling removeLocal N times)
  void removeLocals(const NUNetSet& nets, bool remove_references);

  //! Add the given block as a child
  void addBlock(NUBlock * block) APIDISTILLMACRO2(APIEMITBIND(mBlockList),
                                                  APIEMITCODE(mBlockList.push_back(block)));

  //! Remove the given block from being a child
  void removeBlock(NUBlock * block);

  //! Add the given declaration scope as a child
  void addDeclarationScope(NUNamedDeclarationScope * scope) APIDISTILLMACRO2(APIEMITBIND(mDeclarationScopeList),
                                                                             APIEMITCODE(mDeclarationScopeList.push_back(scope)));

  //! Remove the given sub-declaration scope.
  void removeDeclarationScope(NUNamedDeclarationScope * scope);

  //! Loop over sub-blocks
  NUBlockCLoop loopBlocks() const;

  //! Loop over sub-blocks
  NUBlockLoop loopBlocks();

  //! Loop over sub-declaration scopes.
  NUNamedDeclarationScopeCLoop loopDeclarationScopes() const;

  //! Loop over sub-declaration scopes.
  NUNamedDeclarationScopeLoop loopDeclarationScopes();

  //! Add the arg to the arg list.
  /*!
   * The order in which these are added matters; it should be in arg
   * list order.  If this is a task that is replacing a function then
   * the first arg is the output of the function, and is the size of
   * the function result.
   */
  void addArg(NUNet *net, CallByMode mode) APIDISTILLMACRO4(APIEMITBIND(mArgs),
                                                            APIEMITBIND(mArgsMode),
                                                            APIEMITCODE(mArgs.push_back(net)),
                                                            APIEMITCODE(mArgsMode.push_back (mode)));

  //! add the arg to the arg list, inserting it at the beginning of the list.  NOTE this is a expensive (slow) method and you should use addArg instead if at all possible.
  void addArgFirst(NUNet *net, CallByMode mode) APIDISTILLMACRO4(APIEMITBIND(mArgs),
                                                                 APIEMITBIND(mArgsMode),
                                                                 APIEMITCODE(mArgs.insert(mArgs.begin(),net)),
                                                                 APIEMITCODE(mArgsMode.insert(mArgsMode.begin(),mode)));

  //! Return the number of arguments
  int getNumArgs() const;

  //! Return the argument corresponding to the 0-based index
  NUNet* getArg(int idx) const;

  //! Return the parameter-passing mode for the argument
  CallByMode getArgMode (int idx) const;

  //! Alter the parameter-passing mode for the argument
  void putArgMode (int idx, CallByMode mode);

  //! Return the 0-based index corresponding to which position this net appears in the
  //! arg list, -1 if it doesn't exist in the list.
  int getArgIndex(const NUNet *net) const;

  //! Replace the current argument with the new argument.
  void replaceArg(NUNet *cur_arg, NUNet *new_arg);

  //! Replace one port net with a a set of new ports.
  void replaceArg(NUNet* cur_arg, NUNetVector& new_args);

  //! Get unique name for names inside this scope
  StringAtom* uniquify(const char* prefix, const char* name);

  //! A task or function can never be at the top level
  bool atTopLevel() const { return false; }

  //! For objects declared locally, return an index for them to be lookup-up by, which
  //! will be consistent across different instantiations of this scope.
  SInt32 reserveSymtabIndex() APIDISTILLMACRO1(APIEMITCODE(return mCurSymtabIndex++));

  //! Create an elaboration of this task/function at the given hierarchy and put it into
  //! the symbol table.
  NUTFElab *createElab(STBranchNode *hier, STSymbolTable *symtab);

  //! Return this task/function elaborated at the given hierarchy
  virtual NUScopeElab* lookupElabScope(const STBranchNode *hier) const;

  //! Return my index in the symbol table
  SInt32 getSymtabIndex() const APIDISTILLMACRO1(APIEMITCODE(return mMySymtabIndex ));


  //! Loop over all local nets
  NUNetCLoop loopLocals() const { return NUNetCLoop(mLocalNetList); }

  //! Loop over all local nets
  NUNetLoop loopLocals() { return NUNetLoop(mLocalNetList); }

  //! nop for TF.
  void getAllNonTFNets(NUNetList *net_list) const;

  //! Add all nets local to this scope to the given list, in no particular order.
  /*!
   * Nets declared local to named blocks will be added also; this list has all nets
   * declared within this scope.
   */
  void getAllNets(NUNetList *net_list) const;

  //! Loop over all arguments of specified direction, in argument order
  NUNetFilterCLoop loopArgs(NetFlags flags) const { return NUNetFilterCLoop(NUNetVectorCLoop(mArgs), NUNetFilter(flags)); }

  //! Loop over all arguments of specified direction, in argument order
  NUNetFilterLoop loopArgs(NetFlags flags) { return NUNetFilterLoop(NUNetVectorLoop(mArgs), NUNetFilter(flags)); }

  //! Loop over all arguments, in argument order
  NUNetVectorCLoop loopArgs() const { return NUNetVectorCLoop(mArgs); }

  //! Loop over all arguments, in argument order
  NUNetVectorLoop loopArgs() { return NUNetVectorLoop(mArgs); }

  //! Loop over the statements
  NUStmtCLoop loopStmts() const { return NUStmtCLoop(mStmts); }

  //! Loop over the statements
  NUStmtLoop loopStmts() { return NUStmtLoop(mStmts); }

  //! Return the statements for the tf body.
  /*!
   * The list must be freed by the caller.
   */
  NUStmtList * getStmts() const;
  NUStmt* getStmt() const;

  //! Replace the current statement list with the given statement list.
  /*!
   * The given list is copied.
   * This is used to reorder non-blocking temporaries.
   * The caller is responsible to make sure that statements are not leaked.
   */
  void replaceStmtList(const NUStmtList& new_stmts);

  //! Add the given statement to the end of the block
  void addStmt(NUStmt *stmt) APIDISTILLMACRO2(APIEMITBIND(mStmts),
                                              APIEMITCODE(mStmts.push_back(stmt)));

  //! Add the given statements to the end of the block
  void addStmts(NUStmtList *stmts);

  //! Add the given statement to the start of the block
  void addStmtStart(NUStmt *stmt);

  //! Remove the given statement from the block
  void removeStmt(NUStmt *stmt, NUStmt *replacement=0);

  //! Clear the use/def information stored in the object, if any.
  virtual void clearUseDef();

  //! Fixup the use/def information; remove all nets in the given list from the sets.
  virtual void fixupUseDef(NUNetList *remove_list);

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;
  
  //! Iterate over the hierarchical referenced nets read by this task.
  NUNetCLoop loopNetHierRefReads() const { return NUNetCLoop(mNetHierRefReads); }
  NUNetLoop loopNetHierRefReads() { return NUNetLoop(mNetHierRefReads); }

  void addNetHierRefRead( NUNet* net ) APIDISTILLMACRO2(APIEMITBIND(mNetHierRefReads),
                                                        APIEMITCODE(mNetHierRefReads.push_back( net )));
  
  //! Iterate over the hierarchical referenced nets written by this task.
  NUNetCLoop loopNetHierRefWrites() const { return NUNetCLoop(mNetHierRefWrites); }
  NUNetLoop loopNetHierRefWrites() { return NUNetLoop(mNetHierRefWrites); }
  void addNetHierRefWrite( NUNet* net ) APIDISTILLMACRO2(APIEMITBIND(mNetHierRefWrites),
                                                         APIEMITCODE(mNetHierRefWrites.push_back( net )));
  
  //! Iterate over the hierarchical tasks called with in this task
  NUTaskEnableCLoop loopTaskEnableHierRefs() const { return NUTaskEnableCLoop(mTaskEnableHierRefs); }
  NUTaskEnableLoop loopTaskEnableHierRefs() { return NUTaskEnableLoop(mTaskEnableHierRefs); }
  void addTaskEnableHierRef( NUTaskEnable* te ) APIDISTILLMACRO2(APIEMITBIND(mTaskEnableHierRefs),
                                                                 APIEMITCODE(mTaskEnableHierRefs.push_back( te )));

  //! destructor
  virtual ~NUTF();

  //! clear out the body of the task, leave the nets
  void removeBody();

  //! copy the contents of one TF into this
  static void clone(NUTF* dst, const NUTF& src, CopyContext&,
                    bool xformNonLocals);



protected:
  static void cloneScopeLocals(const NUScope * original, 
                               NUScope * target,
                               CopyContext & copy_context);

  static void cloneDeclarationScope(NUNamedDeclarationScope * original,
                                    NUScope * parent,
                                    CopyContext & copy_context);

  //! Return the net flags to use when creating a locally-scoped temporary
  NetFlags getTempFlags() const { return NetFlags(eTempNet|eAllocatedNet|eBlockLocalNet
                                                  |eNonStaticNet); }

  //! Helper function to print the contents of a task/function.
  void printContents(int indent) const;

  //! Task/function symbol-table element.
  STBranchNode * mNameBranch APIDISTILLMACRO1(APIEMITTYPE(StringAtom*));

  //! Task/function name; may include hierarchy when flattening
  StringAtom * mRelativeName APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Original task/function  name -- this may not be unique
  StringAtom *mOriginalName;

  //! Task/function source location.
  SourceLocator mLoc;

  //! Local nets
  NUNetList mLocalNetList APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! List of sub-declaration scopes.
  NUNamedDeclarationScopeList mDeclarationScopeList APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Arguments, in argument order
  NUNetVector mArgs APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Parameter-passing mode associated with each argument
  CallByModeVector mArgsMode APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Statement list
  NUStmtList mStmts APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Nested blocks in no particular order.
  NUBlockList mBlockList APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Parent module
  NUModule *mParent;

  /*! \brief Parent scope, if the module is not the actual parent.
   *
   * VHDL allows subprograms to be declared inside an process (always
   * block) or another subprogram (task/function).  This differs from
   * Verilog and from the parent scoping rules implemented in Nucleus
   * via mParent.  This pointer allows walking up the true HDL
   * hierarchy, not the Nucleus hierarchy.
   * \sa mParent
   */
  NUScope *mHdlParent APIDISTILLMACRO2(APICONSTRUCTORFALSE,
                                       APIEMITCODE(mHdlParent = NULL) );
  //! Currently-available symtab index
  SInt32 mCurSymtabIndex APIDISTILLMACRO2(APICONSTRUCTORFALSE,
                                          APIEMITCODE(mCurSymtabIndex=0) );

  //! Index of me in the symbol table.
  SInt32 mMySymtabIndex APIDISTILLMACRO2(APICONSTRUCTORFALSE,
                                         APIEMITCODE(mParent->reserveSymtabIndex()));
  
  //! HierRefs read by this task.
  NUNetList mNetHierRefReads APIDISTILLMACRO1(APICONSTRUCTORFALSE);
  
  //! HierRefs written by this task.
  NUNetList mNetHierRefWrites APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Hierachical tasks called from this task.
  NUTaskEnableVector mTaskEnableHierRefs APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Has anyone requested that we inline this task, either for correctness,
  //! performance, or compliance with a user "inline" directive.
  bool mRequestInline APIDISTILLMACRO2(APICONSTRUCTORFALSE,
                                       APIEMITCODE(mRequestInline = false) );
  //! Is this task recursive?
  /*!
   *! If so we will not be able to inline it, and there may be other
   *! downstream restrictions
   */
  bool mRecursive APIDISTILLMACRO2(APICONSTRUCTORFALSE,
                                   APIEMITCODE(mRecursive = false) );

private:
  //! share common member-var initialization between two ctors
  void commonInit(NUModule* parent);

  //! Hide copy and assign constructors.
  NUTF(const NUTF&);
  NUTF& operator=(const NUTF&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUTF : public NUBlockingMaps, public NUBlockScope


//! NUTask class
/*!
 * Task
 */
class NUTask : public NUTF
{
public:
  //! constructor
  /*!
    \param parent Module in which declared
    \param name_branch The name of the task
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param loc The source location of the task
    \param iodb The IODB for intrinsic type info to be stored in
   */
  NUTask(NUModule *parent,
	 STBranchNode * name_branch,
	 NUNetRefFactory *netref_factory,
	 const SourceLocator& loc,
         IODBNucleus *iodb);

  //! constructor
  /*!
    \param parent Module in which declared
    \param name The name of the task
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param loc The source location of the task
    \param iodb The IODB for intrinsic type info to be stored in
   */
  NUTask(NUModule *parent,
	 StringAtom* name,
	 StringAtom* originalName,
	 NUNetRefFactory *netref_factory,
	 const SourceLocator& loc,
         IODBNucleus *iodb);

  //! Return the type of this class
  virtual NUType getType() const { return eNUTask; }

  //! Return the type of scoping unit
  NUScope::ScopeT getScopeType() const { return NUScope::eTask; }

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Dump myself to UtIO::cout().
  void print(bool verbose, int indent) const;

  //! Return a string identifying this type.
  const char* typeStr() const;

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);

  //! Add the use_net as being used by the non-blocking def of def_net.
  void addNonBlockingUse(NUNet *def_net, NUNet *use_net);
  void addNonBlockingUse(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);

  //! Add the net to the set of nets this stmt non-blockingly-defs.
  void addNonBlockingDef(NUNet *net);
  void addNonBlockingDef(const NUNetRefHdl &net_ref);

  //! Add the net to the set of non-blocking kills
  void addNonBlockingKill(NUNet *net);
  void addNonBlockingKill(const NUNetRefHdl &net_ref);

  //! Add the nets this node non-blockingly kills to the given set.
  void getNonBlockingKills(NUNetSet *kills) const;
  void getNonBlockingKills(NUNetRefSet *kills) const;
  bool queryNonBlockingKills(const NUNetRefHdl &def_net_ref,
			     NUNetRefCompareFunction fn,
			     NUNetRefFactory *factory) const;

  //! Add the nets this node non-blockingly defs to the given set.
  void getNonBlockingDefs(NUNetSet *defs) const;
  void getNonBlockingDefs(NUNetRefSet *defs) const;
  bool queryNonBlockingDefs(const NUNetRefHdl &def_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Add the nets this node uses for non-blocking defs to the given set.
  void getNonBlockingUses(NUNetSet *uses) const;
  void getNonBlockingUses(NUNetRefSet *uses) const;
  bool queryNonBlockingUses(const NUNetRefHdl &use_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Add the nets this node uses to non-blockingly def the given net to the given set.
  void getNonBlockingUses(NUNet *net, NUNetSet *uses) const;
  void getNonBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryNonBlockingUses(const NUNetRefHdl &def_net_ref,
			    const NUNetRefHdl &use_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Clear the use/def information stored in the object, if any.
  void clearUseDef();

  //! Fixup the use/def information; remove all nets in the given list from the sets.
  void fixupUseDef(NUNetList *remove_list);

  //! Indicate preference for the inlining of current task (false == do-not-inline)
  void putRequestInline(bool flag);

  //! Has anyone requested that this task be inlined (via putRequestInline)?
  bool isRequestInline() const {return mRequestInline;}

  //! Should this task be inlined, taking into account requests
  //! and constraints?
  bool shouldInline() const {return mRequestInline && canInline();}

  //! Return true if we can inline the task (independent of whether it's desired)
  /*!
   *! We cannot inline recursive or hierarchical tasks, so false
   *! is returned, even if someone requested that the task be inlined
   *!
   *! We cannot inline hier-ref tasks because their resolutions are
   *! only known elaboratedly. Further, the inlining code does not
   *! take care of resolving any hier-refs (tasks or nets) contained
   *! within the task being inlined.
   */
  bool canInline() const { return !mRecursive && !hasHierRef(); }

  //! Set the must-inline flag
  void putRecursive(bool flag);

  //! Return true if we must inline this task
  bool isRecursive() const { return mRecursive; }

  //! Return true if there are hierarchical references to this task.
  bool hasHierRef() const;

  //! Iterate over the hierarchical references to this task, if any.
  NUTaskEnableCLoop loopHierRefTaskEnables() const
  {
    return mTaskEnableVector ? NUTaskEnableCLoop(*mTaskEnableVector) : NUTaskEnableCLoop();
  }
  NUTaskEnableLoop loopHierRefTaskEnables()
  {
    return mTaskEnableVector ? NUTaskEnableLoop(*mTaskEnableVector) : NUTaskEnableLoop();
  }

  //! Add an out-of-scope reference to this task
  void addHierRef(NUBase *enable);

  //! Remove an out-of-scope reference ot this task
  void removeHierRef(NUBase *task_enable_hier_ref);

  //! destructor
  ~NUTask();


private:
  //! Hide copy and assign constructors.
  NUTask(const NUTask&);
  NUTask& operator=(const NUTask&);

  //! Cross-hierarchy task enables pointing to this.  If NULL, there are none.
  NUTaskEnableVector *mTaskEnableVector APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Set of nets which this stmt non-blockingly-kills defs.
  NUNetRefSet mNonBlockingNetRefKillSet APIOMITTEDFIELD;

  //! Map of non-blocking defs to their "use"s
  NUNetRefNetRefMultiMap mNonBlockNetRefDefMap APIOMITTEDFIELD;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUTask : public NUTF


//! NUTFElab class
/*!
 * Model the elaboration of a task/function
 */
class NUTFElab : public NUScopeElab
{
public:
  //! constructor
  /*!
    \param block The correspdoning unelaborated task/function
    \param hier The hierarchy node in the symbol table for this elaboration
   */
  NUTFElab(NUTF *tf, STBranchNode *hier);

  //! Return the type of this class
  virtual NUType getType() const { return eNUTFElab; }

  //! Return the task/function of which this is an elaboration
  NUTF *getTF() const;

  //! Realise NUScopeElab::getSourceLocation virtual method
  virtual const SourceLocator getSourceLocation () const;

  //! Realise NUElabBase::findUnelaborated virtual method
  virtual bool findUnelaborated (STSymbolTable *stab, STSymbolTableNode **unelab) const;

  //! Dump myself to UtIO::cout().
  void print(bool recurse, int indent) const;

  //! Dump the elaboration to cout
  void printElab(STSymbolTable *symtab, bool recurse, int indent) const;

  //! Return a UtString identifying this type.
  const char* typeStr() const;

  //! Lookup the NUTFElab object for the given hierarchy branch node
  static NUTFElab *lookup(STBranchNode *hier);

  //! destructor
  ~NUTFElab();


private:
  //! Hide copy and assign constructors.
  NUTFElab(const NUTFElab&);
  NUTFElab& operator=(const NUTFElab&);
} APIOMITTEDCLASS; //class NUTFElab : public NUScopeElab

#endif
