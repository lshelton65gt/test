// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUBLOCKSTMT_H_
#define NUBLOCKSTMT_H_

/*!
  \file
  Declaration of the Nucleus block statement class.
*/

#include "nucleus/NUStmt.h"

//! NUBlockStmt class
/*!
 * Abstract class, models a block of statements (if, case, named block).
 *
 * Provides management of use/def information for blocks of statements.
 */
class NUBlockStmt : public NUStmt
{
public:
  //! constructor
  /*!
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param usesCFNet true when this needs $cfnet in it's use list
    \param loc Source location.
  */
  NUBlockStmt(NUNetRefFactory *netref_factory,
              bool usesCFNet,
	      const SourceLocator& loc) :
    NUStmt(usesCFNet, loc),
    mBlockNetRefDefMap(netref_factory),
    mNonBlockNetRefDefMap(netref_factory),
    mBlockingNetRefKillSet(netref_factory),
    mNonBlockingNetRefKillSet(netref_factory),
    mNetRefFactory(netref_factory)
  {}

  //! Clear the use/def information stored.
  void clearUseDef();

  //! Fixup the use/def information; remove all nets in the given list from the sets.
  void fixupUseDef(NUNetList *remove_list);

  //! Add the nets this stmt uses for blocking defs to the given set.
  void getBlockingUses(NUNetSet *uses) const;
  void getBlockingUses(NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the nets this stmt uses to blockingly def the given net to the given set.
  void getBlockingUses(NUNet *net, NUNetSet *uses) const;
  void getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &def_net_ref,
			 const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the nets this stmt uses for non-blocking defs to the given set.
  void getNonBlockingUses(NUNetSet *uses) const;
  void getNonBlockingUses(NUNetRefSet *uses) const;
  bool queryNonBlockingUses(const NUNetRefHdl &use_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Add the nets this stmt uses to non-blockingly def the given net to the given set.
  void getNonBlockingUses(NUNet *net, NUNetSet *uses) const;
  void getNonBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryNonBlockingUses(const NUNetRefHdl &def_net_ref,
			    const NUNetRefHdl &use_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Add the nets this stmt blockingly defines to the given set.
  void getBlockingDefs(NUNetSet *defs) const;
  void getBlockingDefs(NUNetRefSet *defs) const;
  bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the nets this stmt non-blockingly defines to the given set.
  void getNonBlockingDefs(NUNetSet *defs) const;
  void getNonBlockingDefs(NUNetRefSet *defs) const;
  bool queryNonBlockingDefs(const NUNetRefHdl &def_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Add the nets this stmt blockingly kills to the given set.
  void getBlockingKills(NUNetSet *kills) const;
  void getBlockingKills(NUNetRefSet *kills) const;
  bool queryBlockingKills(const NUNetRefHdl &def_net_ref,
			  NUNetRefCompareFunction fn,
			  NUNetRefFactory *factory) const;

  //! Add the nets this stmt non-blockingly kills to the given set.
  void getNonBlockingKills(NUNetSet *kills) const;
  void getNonBlockingKills(NUNetRefSet *kills) const;
  bool queryNonBlockingKills(const NUNetRefHdl &def_net_ref,
			     NUNetRefCompareFunction fn,
			     NUNetRefFactory *factory) const;

  //! Add the use_net as being used by the blocking def of def_net.
  void addBlockingUse(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);

  //! Add the use_net as being used by the non-blocking def of def_net.
  void addNonBlockingUse(NUNet *def_net, NUNet *use_net);
  void addNonBlockingUse(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);

  //! Add the net to the set of nets this stmt blockingly-kills.
  void addBlockingKill(const NUNetRefHdl &net_ref);

  //! Add the net to the set of nets this stmt non-blockingly-kills.
  void addNonBlockingKill(NUNet *net);
  void addNonBlockingKill(const NUNetRefHdl &net_ref);

  //! Add the net to the set of nets this stmt blockingly-defs.
  void addBlockingDef(const NUNetRefHdl &net_ref);

  //! Add the net to the set of nets this stmt non-blockingly-defs.
  void addNonBlockingDef(NUNet *net);
  void addNonBlockingDef(const NUNetRefHdl &net_ref);

  //! Find the set of visible uses for a given statement list.
  /*!
    Kills can satisfy local uses, causing them to not be externally
    visible.
   */
  static void getVisibleBlockingUses(NUStmtLoop loop, NUNetRefSet *uses);

  //! destructor
  virtual ~NUBlockStmt();


protected:
  //! Map of blocking defs to their "use"s
  NUNetRefNetRefMultiMap mBlockNetRefDefMap APIOMITTEDFIELD;

  //! Map of non-blocking defs to their "use"s
  NUNetRefNetRefMultiMap mNonBlockNetRefDefMap APIOMITTEDFIELD;

  //! Set of nets which this stmt blockingly-kills defs.
  NUNetRefSet mBlockingNetRefKillSet APIOMITTEDFIELD;

  //! Set of nets which this stmt non-blockingly-kills defs.
  NUNetRefSet mNonBlockingNetRefKillSet APIOMITTEDFIELD;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory APIOMITTEDFIELD;

private:
  //! Hide copy and assign constructors.
  NUBlockStmt(const NUBlockStmt&);
  NUBlockStmt& operator=(const NUBlockStmt&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUBlockStmt : public NUStmt
#endif
