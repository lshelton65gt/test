// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _NU_NET_REF_SET_H_
#define _NU_NET_REF_SET_H_

#include "util/LoopSorted.h"

#include "nucleus/NUNetRef.h"

//! Maintain a set of NUNetRef
/*!
 * Wrapper on top of UtSet<NUNetRefHdl>
 *
 * There is only one entry per NUNet in the underlying set.
 *
 * \sa NUNetRefHashSet
 */
class NUNetRefSet
{
private:
  class NetRefSetCompare
  {
  public:
    bool operator()(const NUNetRefHdl& one, const NUNetRefHdl& two) const
    {
      return (one->getNet() < two->getNet());
    }
  };
  typedef UtSet<NUNetRefHdl,NetRefSetCompare> NetRefSet;

public:
  typedef NUNetRefHdl value_type;
  typedef NetRefSet::iterator iterator;
  typedef NetRefSet::const_iterator const_iterator;

  //! constructor
  NUNetRefSet(NUNetRefFactory *factory) : mFactory(factory)
  {}

  //! default constructor is intentionally omitted because if you
  //! find yourself with a default-contructed NUNetRefSet, it will crash
  //! the first time you use it
  //NUNetRefSet() : mFactory(0)
  //{}

  //! Copy
  NUNetRefSet(const NUNetRefSet& src) : mFactory(src.mFactory)
  {
    copyHelper(src);
  }

  //! Assign
  NUNetRefSet& operator=(const NUNetRefSet& src)
  {
    if (&src != this) {
      clear();
      mFactory = src.mFactory;
      copyHelper(src);
    }
    return *this;
  }

  //! Get the factory this is using
  NUNetRefFactory *getFactory() const { return mFactory; }

  //! Insert the ref into the set
  void insert(const NUNetRefHdl& ref);

  //! Insert the given range into the set
  void insert(const_iterator start, const_iterator end);

  //! Find the reference associated with a net
  NUNetRefHdl findNet(NUNet*);

  //! Return the iterator pointing to the net ref which satisfies (ref.*fn)(set_net_ref)
  const_iterator find(const NUNetRefHdl& ref, NUNetRefCompareFunction fn) const;
  //! Overloaded
  iterator find (const NUNetRefHdl& ref, NUNetRefCompareFunction fn);

  //! Remove the net ref which satisfies (ref.*fn)(set_net_ref)
  void erase(NUNetRefHdl& ref, NUNetRefCompareFunction fn);

  //! Remove the bits represented by the given net ref
  void erase(NUNetRefHdl& ref);

  //! Erase by iterator
  /*! Note that this is not safe if the iterator will then be used for
   *  more iteration. A find iterator should be issued to call this
   *  function.
   */
  void erase(iterator pos) { mSet.erase(pos); }

  //! STL begin semantics
  const_iterator begin() const { return mSet.begin(); }

  //! STL begin semantics
  iterator begin() { return mSet.begin(); }

  //! STL end semantics
  const_iterator end() const { return mSet.end(); }

  //! STL end semantics
  iterator end() { return mSet.end(); }

  //! STL clear semantics
  void clear() { mSet.clear(); }

  //! STL empty semantics
  bool empty() const { return mSet.empty(); }

  //! STL size semantics. Number of nets (not bits).
  int size() const { return mSet.size(); }

  //! Number of bits present in the set (not set size).
  UInt32 getNumBits() const;

  //! debug help: print routine
  void print(int indent = 0) const;

  //! Return an iterator for a netref chosen in a stable manner from all net refs in the set
  const_iterator stableChoice() const;

  //! Are the two sets the same?
  bool operator==(const NUNetRefSet & other) const;

  //! Are they different?
  bool operator!=(const NUNetRefSet & other) const { return not((*this)==other); }

  //! Does one set intersect with the other?
  static bool set_has_intersection(const NUNetRefSet & one, const NUNetRefSet & two);

  //! Set intersection algorithm: out = (one intersect two)
  /*!
   * The output set must be different from the input set.
   */
  static void set_intersection(const NUNetRefSet &one, const NUNetRefSet &two, NUNetRefSet &out);

  //! Set difference algorithm: out = (one minus two)
  /*!
   * The output set must be different from the input set.
   */
  static void set_difference(const NUNetRefSet &one, const NUNetRefSet &two, NUNetRefSet &out);

  //! Set subtraction (in-place difference): out -= one
  static void set_subtract(const NUNetRefSet & one, NUNetRefSet & out);

  class NetRefSetFilter;
  friend class NetRefSetFilter;

  //! Class for filtering nets out of the set during looping.
  class NetRefSetFilter
  {
  public:
    NetRefSetFilter() {}
    NetRefSetFilter(const NUNetRefHdl & net_ref, NUNetRefCompareFunction fn) :
      mNetRef(net_ref), mCompareFunction(fn)
    {}
    bool operator()(const NetRefSet::value_type & v) const
    {
      return ((*mNetRef).*mCompareFunction)(*v);
    }
  private:
    NUNetRefHdl mNetRef;
    NUNetRefCompareFunction mCompareFunction;
  };
  
  //! Typedefs to support looping.
  typedef CLoop<NetRefSet> NetRefSetLoop;
  typedef LoopFilter<NetRefSetLoop, NetRefSetFilter> CondLoop;
  NetRefSetLoop loop() const { return NetRefSetLoop(mSet.begin(),mSet.end()); }

  CondLoop loop(const NUNetRefHdl & ref, NUNetRefCompareFunction fn) const {
    const_iterator iter = mSet.find(ref);
    return CondLoop(NetRefSetLoop(iter,mSet.end()), NetRefSetFilter(ref, fn));
  }

  //! Iterator wrapper so we don't have to copy construct loops
  typedef Iter<NUNetRefHdl> SortedLoop;

  //! Return a sorted iterator over the net refs
  SortedLoop loopSorted() const
  {
    return SortedLoop(new LoopSorted<NUNetRefHdl>(loop()));
  }

  //! destructor
  ~NUNetRefSet() {}

private:
  //! Copy all the data from the source set, knowing that we have nothing so 
  //! far in this
  void copyHelper(const NUNetRefSet& src);

  //! The set.
  NetRefSet mSet;

  //! Factory to use to create net refs
  NUNetRefFactory *mFactory;
};

typedef NUNetRefSet::const_iterator NUNetRefSetIter;
typedef NUNetRefSet::iterator NUNetRefSetNonconstIter;

#endif 
