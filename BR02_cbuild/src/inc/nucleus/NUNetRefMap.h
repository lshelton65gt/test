// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef _NU_NET_REF_MAP_H_
#define _NU_NET_REF_MAP_H_


#include "nucleus/NUNetRefSet.h"
#include "util/UtHashSet.h"
#include "util/LoopThunk.h"

//! NUNetAndNetRefQuery
/*!
 * Abstract base class used to determine which nets and net ref mappings are desired
 * in a traversal of a NUNetRefMultiMap.
 *
 * There are two operator() methods:
 *  1) operator()(const NUNetRef&, const NUNetRef&)
 *     This operator() determines whether mappings from a specific NUNetRef are desired,
 *     through comparison with another NUNetRef.  Useful for when you want to walk all
 *     mappings where a specific relationship holds between NUNetRefs.
 *
 *  2) operator()(const NUNet*, const NUNetRef&)
 *     This operator() determines whether to consider any NUNetRefs of the first arg
 *     when walking.  The second arg is the NUNetRef which is being matched during the
 *     traversal.
 */
class NUNetAndNetRefQuery
{
public:
  NUNetAndNetRefQuery() {}
  virtual ~NUNetAndNetRefQuery() {}
  virtual bool operator()(const NUNetRef& first, const NUNetRef& second) = 0;
  virtual bool operator()(const NUNet* net, const NUNetRef &ref) = 0;
};

//! Maintain a mapping of NUNetRef -> many T
/*!
 * implemented by: ( map of NUNet -> ( map of NUNetRef -> set<T> ) )
 */
template <class T, class H = HashPointer<T, 2>, class NetCompare = NUNetCmpPtr>
class NUNetRefMultiMap
{
private:
  //! Convenience typedefs for the internal map
  class NetRefHdlCompare
  {
  public:
    bool operator()(const NUNetRefHdl& one, const NUNetRefHdl& two) const
    {
      return ((*one) < (*two));
    }
  };


public:
  typedef T data_type;
  typedef UtHashSet<T, H, HashMgr<T> > TSet;
  typedef typename TSet::UnsortedLoop TSetLoop;
  typedef UtMap<const NUNetRefHdl,TSet,NetRefHdlCompare> NetRefTMultiMap;

private:
  typedef UtMap<NUNet*,NetRefTMultiMap,NetCompare> NetNetRefTMap;

public:
  //! constructor
  NUNetRefMultiMap(NUNetRefFactory *factory) :
    mFactory(factory)
  {}

  //! Add a mapping to nothing
  /*!
   * This can be used, for instance, to model something which is def'd by a constant.
   */
  void insert(const NUNetRefHdl& ref) {
    insert(ref, nullT());
  }

  //! Add the mapping if it does not exist yet.
  void insert(const NUNetRefHdl& ref, T t) {
    // Do nothing if the ref is empty
    if ((*ref).empty()) {
      return;
    }

    NetRefTMultiMap &m = mMap[ref->getNet()];
    TSet & tset = m[ref];
    tset.insertWithCheck(t);
  } 

  //! Remove any mappings of the given net ref according to the function
  /*!
   * No new net refs will be created to satisfy this.
   */
  void erase(const NUNetRefHdl& ref, NUNetRefCompareFunction fn) {

    typename NetNetRefTMap::iterator map_iter = mMap.find(ref->getNet());
    if (map_iter == mMap.end()) {
      return;
    }

    NetRefTMultiMap &m = map_iter->second;
    for (typename NetRefTMultiMap::iterator iter = m.begin();
         iter != m.end(); 
         /* no advance */) {
      // determine our next element before the potential deletion
      typename NetRefTMultiMap::iterator next=iter;
      ++next;

      const NUNetRef& netRef = *ref;
      if ((netRef.*fn)(*(iter->first))) {
        m.erase(iter);
      }

      // update to next element
      iter = next;
    }
    
    cleanup(ref->getNet());
  }

  //! Remove any mappings of the bits represented by the given net ref
  /*!
   * This does a bit-level erase; potentially new net refs will be created to
   * satisfy this.
   */
  void erase(const NUNetRefHdl& ref) {

    typename NetNetRefTMap::iterator map_iter = mMap.find(ref->getNet());
    if (map_iter == mMap.end()) {
      return;
    }

    NetRefTMultiMap &m = map_iter->second;
    for (typename NetRefTMultiMap::iterator iter = m.begin();
         iter != m.end(); 
         /* no advance */) {
      // determine our next element before the potential deletion
      typename NetRefTMultiMap::iterator next=iter;
      ++next;

      NUNetRefHdl this_ref = iter->first;
      if (ref->overlaps(*this_ref)) {
        // If they are equivalent, just delete; the subtraction would just create an
        // empty net ref.
        if (not ref->covers(*this_ref)) {
          NUNetRefHdl diff = mFactory->subtract(this_ref, ref);

          // Do not use SortedLoop here because order does not matter;
          TSet & tset = iter->second;
          TSet & diff_tset = m[diff];
          for (typename TSet::iterator p = tset.begin(); p != tset.end(); ++p) {
            // Insert directly into the tset; do not go through top-level ::insert.
            diff_tset.insertWithCheck(*p);
          }
        }
        m.erase(iter);
      }

      // update to next element
      iter = next;
    }

    cleanup(ref->getNet());
  } // void erase


  void erase(NUNet * net) {
    mMap.erase(net);
  }

  //! Remove all mappings.
  void clear() {
    mMap.clear();
  }


  //! Number of elements.
  int size() const {
    int s = 0;
    for (typename NetNetRefTMap::const_iterator map_iter = mMap.begin();
	 map_iter != mMap.end();
	 ++map_iter)
    {
      const NetRefTMultiMap &m = map_iter->second;
      for (typename NetRefTMultiMap::const_iterator iter = m.begin();
           iter != m.end(); ++iter)
      {
        const TSet & tset = iter->second;
        s += tset.size();
      }
    }
    return s;
  }


  //! True if container is empty
  bool empty() const {
    return mMap.empty();
  }


  //! Get the factory this is using
  NUNetRefFactory *getFactory() const { return mFactory; }

  //! Typedefs to support loopings.
  typedef LoopMap<const NetRefTMultiMap, typename NetRefTMultiMap::const_iterator> NetRefTMultiMapCLoop;

  //! Class for supporting iterating over filtered netref->flow mappings
  class CondLoop
  {
  public:
    typedef std::pair<NUNetRefHdl, T> reference;
    typedef reference value_type;

    //! Constructor which takes a NUNetRef member function to use for the net ref comparison
    CondLoop(const NUNetRefHdl& hdl, NUNetRefCompareFunction fn,
             NetRefTMultiMapCLoop loop)
      : mEnable(true),
        mOuterLoop(loop),
        mFilter(fn),
        mFilterClass(0),
        mRefHandle(hdl)
    {
      if (not loop.atEnd()) {
        TSet& tset = const_cast<TSet&>((*mOuterLoop).second);
        mInnerLoop = tset.loopUnsorted();
	findValid();
      }
    }

    //! Constructor which takes a NUNetAndNetRefQuery class implementation to use for the net ref comparison
    CondLoop(const NUNetRefHdl& hdl, NUNetAndNetRefQuery *filter,
             NetRefTMultiMapCLoop loop)
      : mEnable(true),
        mOuterLoop(loop),
        mFilter(0),
        mFilterClass(filter),
        mRefHandle(hdl)
    {
      if (not loop.atEnd()) {
        TSet& tset = const_cast<TSet&>((*mOuterLoop).second);
        mInnerLoop = tset.loopUnsorted();
	findValid();
      }
    }

    CondLoop(): mEnable(false) {}

    //! Assignment
    CondLoop& operator=(const CondLoop& other)
    {
      if (&other != this) {
        mEnable = other.mEnable;
        mOuterLoop = other.mOuterLoop;
        mFilter = other.mFilter;
        mFilterClass = other.mFilterClass;
        mRefHandle = other.mRefHandle;
        mLoopRef = other.mLoopRef;
        mInnerLoop = other.mInnerLoop;
      }
      return *this;
    }

    bool atEnd() const
    {
      return !mEnable || mOuterLoop.atEnd();
    }

    void operator++()
    {
      INFO_ASSERT(not mInnerLoop.atEnd(), "incrementing exhausted inner loop iterator");
      INFO_ASSERT(not mOuterLoop.atEnd(), "incrementing exhausted outer loop iterator");
      ++mInnerLoop;
      findValid();
    }

    reference operator*() const
    {
      return reference(mLoopRef, *mInnerLoop);
    }

  private:
    //! If the current inner iter is not valid, try to find the next valid one.
    void findValid()
    {
      const NUNetRef& myRef = *mRefHandle;
      const NUNetRef* loopRef = &*((*mOuterLoop).first);
      while (mInnerLoop.atEnd() || !test(myRef, *loopRef))
      {
        ++mOuterLoop;
        if (mOuterLoop.atEnd()) {
          break;
        }
        loopRef = &*((*mOuterLoop).first);
        if (test(myRef, *loopRef))
        {
          TSet& tset = const_cast<TSet&>((*mOuterLoop).second);
          mInnerLoop = tset.loopUnsorted();
        }
      }
      mLoopRef = NUNetRefHdl(const_cast<NUNetRef*>(loopRef));
    }

    //! Test two net refs with either the query function or the query class
    bool test(const NUNetRef& first, const NUNetRef& second)
    {
      if (mFilter) {
        return (first.*mFilter)(second);
      } else if (mFilterClass) {
        return (*mFilterClass)(first, second);
      } else {
        NU_ASSERT2("Bad CondLoop, no filter function or class" == 0, &first, &second);
        return false;
      }
    }

    bool mEnable;
    NetRefTMultiMapCLoop mOuterLoop;
    NUNetRefCompareFunction mFilter;
    NUNetAndNetRefQuery *mFilterClass;
    NUNetRefHdl mRefHandle;
    NUNetRefHdl mLoopRef;
    TSetLoop mInnerLoop;
  };

  typedef LoopMap<const NetNetRefTMap, typename NetNetRefTMap::const_iterator> NetNetRefTMapCLoop;

  //! Class for supporting iterating over all netref->flow mappings
  /*!
   * Ultimately, this should probably become LoopMapThunk in util, once it is generalized.
   */
  class MapLoop
  {
  public:
    typedef std::pair<typename NetRefTMultiMap::key_type, T> reference;
    typedef reference value_type;

    MapLoop(NetNetRefTMapCLoop loop) : mOuterLoop(loop)
    {
      if (not loop.atEnd()) {
        mMiddleLoop = NetRefTMultiMapCLoop(loop.getValue());
        if (!mMiddleLoop.atEnd()) {
	  TSet & middleSet = const_cast<TSet&>((*mMiddleLoop).second);
          mInnerLoop = middleSet.loopUnsorted();
	}
	findValid();
      }
    }

    bool atEnd()
    {
      return mOuterLoop.atEnd();
    }

    void operator++()
    {
      INFO_ASSERT(not mInnerLoop.atEnd(),  "incrementing exhausted iterator");
      INFO_ASSERT(not mMiddleLoop.atEnd(), "incrementing exhausted iterator");
      INFO_ASSERT(not mOuterLoop.atEnd(),  "incrementing exhausted iterator");
      ++mInnerLoop;
      findValid();
    }

    reference operator*() const
    {
      return reference((*mMiddleLoop).first, *mInnerLoop);
    }

  private:
    //! If the current inner iter is not valid, try to find the next valid one.
    void findValid()
    {
      while (mInnerLoop.atEnd()) {
        ++mMiddleLoop;
        if (mMiddleLoop.atEnd())
        {
          ++mOuterLoop;
          if (mOuterLoop.atEnd()) {
            break;
          }
          mMiddleLoop = NetRefTMultiMapCLoop(mOuterLoop.getValue());
        }
	TSet & middleSet = const_cast<TSet&>((*mMiddleLoop).second);
        mInnerLoop = middleSet.loopUnsorted();
      }
    }

    NetNetRefTMapCLoop mOuterLoop;
    NetRefTMultiMapCLoop mMiddleLoop;
    TSetLoop mInnerLoop;
  };

  //! Return an iterator through all the mappings
  MapLoop loop() const { return MapLoop(NetNetRefTMapCLoop(mMap)); }

  class NetRefNetCondFilter 
  {
  public:
    NetRefNetCondFilter() : 
      mQueryFunction(0),
      mNet(0)
    {}
    NetRefNetCondFilter(NUNet::QueryFunction fn) : 
      mQueryFunction(fn),
      mNet(0)
    {}
    NetRefNetCondFilter(NUNet* net) : 
      mQueryFunction(0),
      mNet(net)
    {}
    bool operator()(typename MapLoop::value_type cur) const {
      NUNetRefHdl net_ref = cur.first;
      NUNet * net = net_ref->getNet();
      if (net == NULL)
	return false;
      else if (mNet != NULL)
        return mNet == net;
      return (net->*mQueryFunction)();
    }
  private:
    NUNet::QueryFunction mQueryFunction;
    NUNet* mNet;
  };
  typedef LoopFilter<MapLoop, NetRefNetCondFilter> CondNetLoop;

  //! Loop over all entries in the map whose net matches fn
  CondNetLoop loop(NUNet::QueryFunction fn) const { 
    NetRefNetCondFilter filter(fn);
    return CondNetLoop(loop(),filter);
  }

  //! Loop over all entries in the map for the specified net
  CondNetLoop loop(NUNet* net) const { 
    NetRefNetCondFilter filter(net);
    return CondNetLoop(loop(),filter);
  }

  bool containsNet(NUNet* net)
    const
  {
    typename NetNetRefTMap::const_iterator p = mMap.find(net);
    if (p != mMap.end())
    {
      const NetRefTMultiMap& mmap = p->second;
      return !mmap.empty();
    }
    return false;
  }

  //! Return an iterator through the mapped-to T for the given net ref
  /*!
   * T are traversed where ((*ref)->*fn)(mapped_net_ref) returns true.
   *
   * NOTE: this will only consider netrefs which have the same NUNet as
   * the passed-in ref.
   */
  CondLoop loop(const NUNetRefHdl& ref, NUNetRefCompareFunction fn) const {
    typename NetNetRefTMap::const_iterator map_iter = mMap.find((*ref).getNet());
    if (map_iter == mMap.end()) {
      // Empty loop
      return CondLoop();
    } else {
      return CondLoop(ref, fn, NetRefTMultiMapCLoop(map_iter->second));
    }
  }


  //! LoopGen for LoopThunk which allows query-based iteration over the whole map.
  struct CondLoopGen
  {
  public:
    //! LoopThunk requires default constructor, but do not use this.
    CondLoopGen() {}

    //! constructor
    CondLoopGen(const NUNetRefHdl& ref, NUNetAndNetRefQuery *query) :
      mQuery(query),
      mRef(ref)
    {}

    //! copy constructor required by LoopThunk
    CondLoopGen(const CondLoopGen& other) :
      mQuery(other.mQuery),
      mRef(other.mRef)
    {
    }

    //! assign operator required by LoopThunk
    CondLoopGen& operator=(const CondLoopGen& other)
    {
      if (&other != this) {
        mQuery = other.mQuery;
        mRef = other.mRef;
      }
      return *this;
    }

    //! Return the CondLoop for the given net's mappings, if applicable.
    /*!
     * This applies the query object to the passed-in net and if the net is a
     * net we want to iterate over, then create a CondLoop for its mappings.
     * Otherwise, create an empty loop.
     */
    CondLoop operator()(const typename NetNetRefTMapCLoop::value_type &value)
    {
      NUNet *net = const_cast<NUNet*>(value.first);
      NetRefTMultiMap &mappings = const_cast<NetRefTMultiMap&>(value.second);

      if (not (*mQuery)(net, *mRef)) {
        return CondLoop();
      } else {
        return CondLoop(mRef, mQuery, NetRefTMultiMapCLoop(mappings));
      }
    }

  private:
    NUNetAndNetRefQuery *mQuery;
    NUNetRefHdl mRef;
  };

  //! Loop type to allow query-based iteration over all mappings.
  typedef LoopThunk< NetNetRefTMapCLoop, CondLoop, CondLoopGen > CondMapLoop;

  //! Return an iterator through the mapped-to T based on the query object
  /*!
   * All mappings for a net are traversed where (*query)(net, mapped_net_ref) returns true.
   * Then, all T for that net are traversed where (*query)(ref, mapped_net_ref) returns true.
   *
   * NOTE: this will allow consideration of netrefs which have different NUNet
   * as the passed-in ref.
   */
  CondMapLoop loop(const NUNetRefHdl& ref, NUNetAndNetRefQuery *query) const {
    return CondMapLoop(NetNetRefTMapCLoop(mMap), CondLoopGen(ref, query));
  }


  //! Populate the given set with net refs that are mapped-from (keys) which are not empty
  void populateMappedFrom(NUNetRefSet *s) const {
    for (MapLoop l = loop(); not l.atEnd(); ++l) {
      s->insert((*l).first);
    }
  }


  //! Return true if the given net ref is mapped-from (a key)
  bool queryMappedFrom(const NUNetRefHdl &net_ref, NUNetRefCompareFunction fn) const {
    typename NetNetRefTMap::const_iterator map_iter = mMap.find(net_ref->getNet());
    if (map_iter != mMap.end()) {
      const NetRefTMultiMap &m = map_iter->second;
      const NUNetRef& netRef = *net_ref;
      for (typename NetRefTMultiMap::const_iterator iter = m.begin(); iter != m.end(); ++iter) {
        const NUNetRef& nr = *(iter->first);
	if ((nr.*fn)(netRef)) {
	  return true;
	}
      }
    }
    return false;
  }


  //! Helper functions to facilitate printing of templatized value in (key,value) map
  virtual void helperTPrint(const T &v, int indent) const;

  //! debug help: print routine
  void print(int indent = 0, bool printT = true) const {
    for (typename NetNetRefTMap::const_iterator iter = mMap.begin(); iter != mMap.end(); ++iter) {
      NUNet *net = iter->first;
      const NetRefTMultiMap& m = iter->second;

      if (net) {
	net->print(0, indent+2);
      } else {
	for (int i = 0; i < indent+2; i++) {
	  UtIO::cout() << " ";
	}
	UtIO::cout() << "NUNet(0)" << UtIO::endl;
      }

      if (printT)
      {
        for (typename NetRefTMultiMap::const_iterator iter2 = m.begin(); iter2 != m.end();
             ++iter2)
        {
          NUNetRefHdl ref = iter2->first;
          ref->print(indent+4);
          TSet & t = const_cast<TSet&>(iter2->second);
          for (TSetLoop p = t.loopUnsorted(); !p.atEnd(); ++p)
          {
            helperTPrint(*p, indent+6);
            UtIO::cout() << UtIO::endl;
          }
        }
      }
      else
        UtIO::cout() << "   " << m.size() << " map entries" << UtIO::endl;
    } // for
  } // void print


  //! destructor
  virtual ~NUNetRefMultiMap() {
    clear();
  }


private:
  //! Hide copy and assign constructors.
  NUNetRefMultiMap(const NUNetRefMultiMap&);
  NUNetRefMultiMap& operator=(const NUNetRefMultiMap&);

  //! Create a null T
  T nullT() const;

  //! If the given net has an empty netref->flnode map, remove it from the map.
  void cleanup(NUNet *net) {
    typename NetNetRefTMap::iterator map_iter = mMap.find(net);
    if (map_iter == mMap.end()) {
      return;
    }

    NetRefTMultiMap &m = map_iter->second;
    if (m.empty()) {
      mMap.erase(map_iter);
    }
  }

  //! The map.
  NetNetRefTMap mMap;

  //! Factory to use to create net refs
  NUNetRefFactory *mFactory;
};


//! Mapping of NUNetRef -> many FLNode*
typedef NUNetRefMultiMap<FLNode*> NUNetRefFLNodeMultiMap;


// We must override the operator== for NUNetRefHdl because we want shallow equality,
// not deep equality, which is what we get with NUNetRefHdl::operator==
#if 0
class NUNetRefMapHash: public HashValue<NUNetRefHdl>
{
public:
  bool equal(const NUNetRefHdl& v1, const NUNetRefHdl& v2) const {
    const NUNetRef& nr1 = *v1;
    const NUNetRef& nr2 = *v2;
    return &nr1 == &nr2;
  }

  size_t hash(const NUNetRefHdl& var) const {
    const NUNetRef& nr = *var;
    return ((size_t) &nr) - 2;
  }
};
#else
typedef HashValue<NUNetRefHdl> NUNetRefMapHash;
#endif

// Cannot use a typedef for the base because then we cannot instantiate it in
// NUNetRef.cxx
#define NUNetRefNetRefMultiMapB NUNetRefMultiMap<NUNetRefHdl, NUNetRefMapHash>

//! Mapping of NUNetRef -> many NUNetRef
class NUNetRefNetRefMultiMap : public NUNetRefNetRefMultiMapB
{
public:
  //! constructor
  NUNetRefNetRefMultiMap(NUNetRefFactory *factory) :
    NUNetRefNetRefMultiMapB(factory)
  {}

  //! Return true if the given net ref is mapped-to
  bool queryMappedTo(const NUNetRefHdl &use_net_ref, NUNetRefCompareFunction fn) const;

  //! Return true if the given net ref is mapped-to for the given def
  bool queryMappedTo(const NUNetRefHdl &def_net_ref,
		     NUNetRefCompareFunction def_fn,
		     const NUNetRefHdl &use_net_ref,
		     NUNetRefCompareFunction use_fn) const;

  //! Populate the given set with net refs that are mapped-to which are not empty
  void populateMappedToNonEmpty(NUNetRefSet *s) const;

  //! Populate the given set with net refs that are mapped-to the given net ref which are not empty
  void populateMappedToNonEmpty(NUNetRefSet *s,
				const NUNetRefHdl &r,
				NUNetRefCompareFunction fn) const;

  //! destructor
  ~NUNetRefNetRefMultiMap() {}

private:
  //! Hide copy and assign constructors.
  NUNetRefNetRefMultiMap(const NUNetRefNetRefMultiMap&);
  NUNetRefNetRefMultiMap& operator=(const NUNetRefNetRefMultiMap&);
} APIOMITTEDCLASS ;//class NUNetRefNetRefMultiMap : public NUNetRefNetRefMultiMapB


#endif
