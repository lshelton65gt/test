// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Declaration of the Nucleus task/function argument connection class.
*/

#ifndef _NUCMODELARGCONNECTION_H_
#define _NUCMODELARGCONNECTION_H_

#include "nucleus/NUUseDefNode.h"
#include "util/SourceLocator.h"
#include "nucleus/NUCopyContext.h"

class NUCModelArgConnectionInput;
class NUCModelArgConnectionOutput;


//! NUCModelArgConnection class
/*!
 * Abstract base class representing a task/function argument connection.
 */
class NUCModelArgConnection : public NUBase
{
public:
  //! constructor
  /*!
    \param formal Formal argument
    \param cm C-model being called
    \param loc Source location.
  */
  NUCModelArgConnection(NUCModelPort* formal, NUCModelCall *cm,
			const SourceLocator& loc) :
    mFormal(formal), mCModelCall(cm), mLoc(loc)
  {}

  //! Return the type of this class
  virtual NUType getType() const = 0;

  //! Where is this node defined in the verilog source?
  const SourceLocator& getLoc() const { return mLoc; }

  //! Return the formal
  const NUCModelPort* getFormal() const { return mFormal; }

  virtual UInt32 getBitSize() const;

  //! Return the cmodel being called
  NUCModelCall *getCModelCall() const { return mCModelCall; }

  //! Replace the cmodel call
  /*! This routine is used to update the cmodel call when we make
   *  copies of the c-model call. We copy the arg connections and the
   *  c-model call needs to be updated.
   */
  void updateCModelCall(NUCModelCall* cmodelCall);

  //! A compare operator to sort the arg connections by formal name
  int compare(const NUCModelArgConnection* other) const;

  //! Return the direction of the connection
  virtual PortDirectionT getDir() const = 0;

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const = 0;

  //! for -dumpVerilog
  virtual void compose (UtString*, const STBranchNode* scope, int indent, bool recurse) const = 0;

  //! Return class name
  virtual const char* typeStr() const = 0;

  //! calculate costs, add to NUCost arg
  virtual void calcCost(NUCost*, NUCostContext*) const = 0;

  //! Return a copy of this
  virtual NUCModelArgConnection* copy(CopyContext &copy_context) const = 0;

  //! Replace all net references based on a translation functor.
  virtual bool replaceLeaves(NuToNuFn & translator) = 0;

  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net) = 0;

  //! destructor
  virtual ~NUCModelArgConnection() {}

  //! Returns false
  bool isSigned() const { return false; }

  //! Cast to an input connection if it is one
  virtual NUCModelArgConnectionInput* castInput() { return NULL; }

  //! Cast to an input connection if it is one
  virtual NUCModelArgConnectionOutput* castOutput() { return NULL; }

protected:
  //! Formal argument
  NUCModelPort* mFormal;

  //! C-Model being called
  NUCModelCall *mCModelCall;

  //! Source location of arg connection
  SourceLocator mLoc;

private:
  //! Hide copy and assign constructors.
  NUCModelArgConnection(const NUCModelArgConnection&);
  NUCModelArgConnection& operator=(const NUCModelArgConnection&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCModelArgConnection : public NUBase

//! NUCModelArgConnectionInput class
/*!
 * Class representing an input task/function argument connection.
 */
class NUCModelArgConnectionInput : public NUCModelArgConnection
{
public:
  //! constructor
  /*!
    \param formal Formal argument
    \param actual Actual argument
    \param cm C-model being called
    \param loc Source location.
  */
  NUCModelArgConnectionInput(NUCModelPort* formal, NUExpr *actual,
			     NUCModelCall* cm, const SourceLocator& loc) :
    NUCModelArgConnection(formal, cm, loc), mActual(actual)
  {}

  //! Return the type of this class
  virtual NUType getType() const { return eNUCModelArgConnectionInput; }

  //! Return the direction of the connection
  PortDirectionT getDir() const { return eInput; }

  //! Return the actual expression
  NUExpr *getActual() const { return mActual; }

  //! Set the actual expression
  void setActual(NUExpr * actual) { mActual = actual; }

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(NUNet *net, NUNetSet *uses) const;
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! print arg connection for -dumpVerilog
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return class name
  const char* typeStr() const;

  //! Return a copy of this
  NUCModelArgConnection* copy(CopyContext &copy_context) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! Codegen
  virtual CGContext_t emitCode (CGContext_t) const;

  //! destructor
  virtual ~NUCModelArgConnectionInput();

  //! Cast to an input connection if it is one
  virtual NUCModelArgConnectionInput* castInput() { return this; }

private:
  //! Hide copy and assign constructors.
  NUCModelArgConnectionInput(const NUCModelArgConnectionInput&);
  NUCModelArgConnectionInput& operator=(const NUCModelArgConnectionInput&);

protected:
  //! Actual expression
  NUExpr *mActual;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCModelArgConnectionInput : public NUCModelArgConnection

//! NUCModelArgConnectionOutput class
/*!
 * Class representing an output task/function argument connection.
 */
class NUCModelArgConnectionOutput : public NUCModelArgConnection
{
public:
  //! constructor
  /*!
    \param formal Formal argument
    \param actual Actual argument
    \param cm C-Model being called
    \param loc Source location.
  */
  NUCModelArgConnectionOutput(NUCModelPort* formal, NULvalue *actual,
			      NUCModelCall* cm, const SourceLocator& loc) :
    NUCModelArgConnection(formal, cm, loc), mActual(actual)
  {}

  //! Return the type of this class
  virtual NUType getType() const { return eNUCModelArgConnectionOutput; }

  //! Return the direction of the connection
  PortDirectionT getDir() const { return eOutput; }

  //! Return the actual l-value
  NULvalue *getActual() const { return mActual; }

  //! Set the lvalue
  void setActual(NULvalue * actual) { mActual = actual; }

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(NUNet *net, NUNetSet *uses) const;
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! print arg connection for -dumpVerilog
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return class name
  const char* typeStr() const;

  //! Return a copy of this
  NUCModelArgConnection* copy(CopyContext &copy_context) const;

  //! Codegen
  virtual CGContext_t emitCode (CGContext_t) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUCModelArgConnectionOutput();

  //! Cast to an input connection if it is one
  virtual NUCModelArgConnectionOutput* castOutput() { return this; }

private:
  //! Hide copy and assign constructors.
  NUCModelArgConnectionOutput(const NUCModelArgConnectionOutput&);
  NUCModelArgConnectionOutput& operator=(const NUCModelArgConnectionOutput&);

  //! Actual l-value
  NULvalue *mActual;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCModelArgConnectionOutput : public NUCModelArgConnection

//! NUCModelArgConnectionBid class
/*!
 * Class representing an bid task/function argument connection.
 */
class NUCModelArgConnectionBid : public NUCModelArgConnection
{
public:
  //! constructor
  /*!
    \param formal Formal argument
    \param actual Actual argument
    \param cm C-Model being called
    \param loc Source location.
  */
  NUCModelArgConnectionBid(NUCModelPort* formal, NULvalue *actual,
			   NUCModelCall* cm, const SourceLocator& loc) :
    NUCModelArgConnection(formal, cm, loc), mActual(actual)
  {}

  //! Return the type of this class
  virtual NUType getType() const { return eNUCModelArgConnectionBid; }

  //! Return the direction of the connection
  PortDirectionT getDir() const { return eBid; }

  //! Return the actual l-value
  NULvalue *getActual() const { return mActual; }

  //! Set the lvalue
  void setActual(NULvalue * actual) { mActual = actual; }

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(NUNet *net, NUNetSet *uses) const;
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! print arg connection for -dumpVerilog
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return class name
  const char* typeStr() const;

  //! Return a copy of this
  NUCModelArgConnection* copy(CopyContext &copy_context) const;

  //! Codegen
  virtual CGContext_t emitCode (CGContext_t) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  virtual ~NUCModelArgConnectionBid();

private:
  //! Hide copy and assign constructors.
  NUCModelArgConnectionBid(const NUCModelArgConnectionBid&);
  NUCModelArgConnectionBid& operator=(const NUCModelArgConnectionBid&);

protected:
  //! Actual l-value
  NULvalue *mActual;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCModelArgConnectionBid : public NUCModelArgConnection

#endif // _NUCMODELARGCONNECTION_H_
