// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "nucleus/Nucleus.h"

//! class to bucketize the levels (height) of modules within the design hierarchy
/*!
 *! The height of a module, the distance from lowest leaf, differs from its depth,
 *! the distance from the root.  The height of a leaf module is 0, independent of
 *! where it is instantiated relative to the root
 */
class NUInstanceLevels {
public:
  //! ctor
  NUInstanceLevels();

  //! dtor
  ~NUInstanceLevels();

  //! compute the instance levelization for the design
  void design(NUDesign*);

  //! how many levels of instance hierarchy are there in the design?
  UInt32 numInstanceLevels() const;

  typedef Loop<NUModuleList> ModuleLevelLoop;

  //! loop over the modules at a particular level (must be < numInstanceLevels())
  ModuleLevelLoop loopLevel(UInt32 level) const;

  //! Get the height of a module in the instance-hierarchy tree (distinct from depth)
  UInt32 getModuleLevel(NUModule* mod) const;

private:
  typedef UtVector<NUModuleList*> ModuleBuckets;
  typedef UtHashMap<NUModule*, UInt32> ModuleLevelMap;

  ModuleBuckets* mLevelBuckets;
  ModuleLevelMap* mModuleLevelMap;
};
