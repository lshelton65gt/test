// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Declaration of the Nucleus cycle (asynchronous loop)
*/

#ifndef _NUCYCLE_H_
#define _NUCYCLE_H_

#include "nucleus/NUUseDefNode.h"
#include "util/SourceLocator.h"
#include "util/UtVector.h"
#include "util/UtMap.h"
#include "flow/Flow.h"
#include "flow/FLNodeElab.h"

class UtOStream;
class NUCycleScheduleBlock;
class ScheduleBucket;

typedef UtMap<const NUNetElab*,const UtString*> NameMap;

typedef UtVector<NUCycleScheduleBlock*> CycleBlockVector;

//! Abstract base class representing any element that can be in a cycle schedule
class NUCycleScheduleElement
{
public:
  //! Fill a vector with the scheduled flow nodes for this element
  virtual void fill(FLNodeElabVector* vec) const = 0;
  //! Build a string representation of this element
  virtual void compose(UtString* buf, UInt32 indent) const = 0;
  //! Emit a C++ implementation of this element
  virtual void emit(UtOStream& buf, UInt32 indent) const = 0;
  //! Free the resources used by this element
  virtual ~NUCycleScheduleElement() {};

  //! size metric (codegen complexity)
  virtual int size () const = 0;

  //! Identify the cycle blocks that should be out-of-line
  virtual void identifyOutlineCandidates (int limit, CycleBlockVector*, bool) = 0;
};

//! The simplest schedulable element: a single elaborated flow node
class NUCycleSimple : public NUCycleScheduleElement
{
public:
  NUCycleSimple(FLNodeElab* f) : mFlow(f) {};
  FLNodeElab* getFlow() const { return mFlow; }
  virtual void fill(FLNodeElabVector* vec) const;
  virtual void compose(UtString* buf, UInt32 indent) const;
  virtual void emit(UtOStream& buf, UInt32 indent) const;
  friend class SettleLoop;
  virtual int size () const;
  virtual void identifyOutlineCandidates (int, CycleBlockVector*, bool);

private:
  //! The elaborated flow node being scheduled
  FLNodeElab* mFlow;
};

//! A sequence of scheduleable elements is a schedulable element
class NUCycleScheduleBlock : public NUCycleScheduleElement
{
public:
  typedef UtVector<NUCycleScheduleElement*>::iterator iterator;
  typedef UtVector<NUCycleScheduleElement*>::const_iterator const_iterator;
  NUCycleScheduleBlock() : mSubFunction (0) {}
  void append(NUCycleScheduleElement* elem) { mSequence.push_back(elem); }
  iterator begin() { return mSequence.begin(); }
  iterator end() { return mSequence.end(); }
  const_iterator begin() const { return mSequence.begin(); }
  const_iterator end() const { return mSequence.end(); }
  virtual void fill(FLNodeElabVector* vec) const;
  virtual void compose(UtString* buf, UInt32 indent) const;
  virtual void emit(UtOStream& buf, UInt32 indent) const;
  ~NUCycleScheduleBlock()
  {
    for (const_iterator p = mSequence.begin(); p != mSequence.end(); ++p)
      delete *p;
  }

  // Sum over all the sizes in the sequence
  virtual int size () const;

  //! Make this block out-of-line
  void putOutline () {
    static int subfun=0;
    mSubFunction = ++subfun;
  }

  //! Return function uniquifier values for out-of-line implementation.
  int getOutline () const { return mSubFunction;}
  bool isOutline () const { return mSubFunction != 0; }

  virtual void identifyOutlineCandidates (int limit, CycleBlockVector*, bool);
                        
private:
  //! Nonzero if out-of line
  int mSubFunction;

  //! The sequence of schedulable elements
  UtVector<NUCycleScheduleElement*> mSequence;
};

//! A schedule element that branches on some value to determine the schedule sequence
class NUCycleBranch : public NUCycleScheduleElement
{
public:
  NUCycleBranch(NUNetElab* net, SInt32 index) : mNetElab(net), mBitIndex(index) {};
  virtual void fill(FLNodeElabVector* vec) const;
  virtual void compose(UtString* buf, UInt32 indent) const;
  virtual void emit(UtOStream& buf, UInt32 indent) const;
  friend class NUCycle;
  virtual int size () const;
  virtual void identifyOutlineCandidates (int limit, CycleBlockVector*, bool);

private:
  NUNetElab*           mNetElab;    //!< The elaborated net to branch on
  SInt32               mBitIndex;   //!< The bit index to branch on
  NUCycleScheduleBlock mTrueBlock;  //!< The schedule when the value is true
  NUCycleScheduleBlock mFalseBlock; //!< The schedule when the value is false
};

//! A schedule element that runs multiple times until its value settles
class NUCycleSettleLoop : public NUCycleScheduleElement
{
public:
  virtual void fill(FLNodeElabVector* vec) const;
  virtual void compose(UtString* buf, UInt32 indent) const;
  virtual void emit(UtOStream& buf, UInt32 indent) const;
  virtual int size() const;
  virtual void identifyOutlineCandidates(int limit, CycleBlockVector*, bool);

  static bool isCheckable(FLNodeElab* flow);

  //! Get the set of flow nodes from which stale values may be used
  FLNodeElabSet* getCutPoints() const;

  void addCutPoint(FLNodeElab* flow) { mCutPoints.insert(flow); }

  //! Can this loop generate settle logic?
  bool canSettleLoop() const;

  friend class NUCycle;

private:
  //! The schedule element to loop over until it settles
  NUCycleScheduleBlock mBody;
  FLNodeElabSet        mCutPoints;
};

//! NUCycle class
/*!
 *  Model a cycle - this is mostly a dummy structure currently. It
 *  serves to make each cycle unique. In the future, we may add
 *  additional information to make it a first class citizen.
 */
class NUCycle : public NUUseDefNode
{
public:
  //! constructor
  NUCycle(const SourceLocator& loc, UInt32 id);

  //! destructor
  ~NUCycle();

  //! Return the type of this class
  virtual NUType getType() const { return eNUCycle; }

  //! Test if this is a cycle
  virtual bool isCycle() const { return true; }

  //! Get the unique id
  UInt32 getID() const { return mId; }

  //! set the unique id
  void putID(UInt32 id) {mId = id;}

  //! debug help: return the type of what I'm looking at
  const char* typeStr() const;

  //!Code Generator
  CGContext_t emitCode(CGContext_t /* context */ ) const {
    NU_ASSERT(0=="unimplemented", this); return 0;}

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Emit a method definition for evaluating a cycle at runtime
  void emitDefinition(UtOStream&) const;

  //! Emit a method declaration for evaluating a cycle at runtime
  void emitDeclaration(UtOStream&, int limit);

  //! Emit functions that are coded out-of-line
  void emitSubFunctions (UtOStream&) const;

  //! Emit a call to evaluating a cycle at runtime
  ScheduleBucket* emitCall(UtOStream&) const;

  //! Add a flow-node to the cycle
  void addFlow(FLNodeElab* flow);

  //! Schedule a flow-node within the cycle (order is fanin-to-fanout)
  void scheduleFlow(FLNodeElab* flow);

  //! Start a branch in the cycle schedule
  void scheduleBranch(NUNetElab* netElab, SInt32 bitIndex);

  //! Start a loop in the cycle schedule
  void scheduleLoop();

  //! End a branch or loop segment
  void scheduleJoin();

  //! Add a cut point in the looping cycle schedule
  void addCutPoint(FLNodeElab* flow);

  //! Get a vector of flows in an allowed execution order
  /*! Because cycle schedules may execute flow nodes in different orders
   *  depending on values determined at run-time, there is not a single
   *  schedule order.  For stability, this routine returns the order
   *  of flow node execution if all branches are not taken.
   */
  const FLNodeElabVector* getOrderedFlowVector() const;

  //! type for looping through all the flow nodes
  typedef FLNodeElabHashSet::UnsortedCLoop FlowLoop;

  //! Loop over all the original FLNodeElabs that were merged into the cycle
  FlowLoop loopFlows() const {return mFlowNodeSet.loopCUnsorted();}

  //! Loop over all fanin to the cycle
  FLNodeElabLoop loopFanin() const { updateFaninCache(); return FLNodeElabLoop::create(CLoop<FLNodeElabHashSet>(mFanin)); }

  //! type for looping through all the flow nodes in a canonical order by name
  typedef FLNodeElabHashSet::SortedLoop SortedLoop;

  //! Loop over all the original FLNodeElabs in canonical order
  SortedLoop loopSorted() const {return mFlowNodeSet.loopSorted();}

  //! Loop over all fanin to the cycle
  SortedLoop loopFaninSorted() const { updateFaninCache(); return mFanin.loopSorted();}

  // Get the number of FLNodeElabs in the cycle
  size_t numNodes() const {return mFlowNodeSet.size();}

  // Get the number of FLNodeElabs in the cycle
  size_t numFanins() const {updateFaninCache(); return mFanin.size();}

  typedef UtSet<const FLNodeElab*> NodeElabSet;

  //! dump encapsulated cycle info
  virtual void dumpCycle(NodeElabSet* covered, Sensitivity sense) const;

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet* /* uses */) const { NU_ASSERT(0=="unimplemented", this); }
  void getUses(NUNetRefSet* /* uses */) const { NU_ASSERT(0=="unimplemented", this); }
  bool queryUses(const NUNetRefHdl & /* use_net_ref -- unused */,
		 NUNetRefCompareFunction /* fn -- unused */,
		 NUNetRefFactory * /* factory -- unused */) const
  {
    NU_ASSERT(0=="unimplemented", this);
    return false;
  }

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(NUNet* /* net */, NUNetSet* /* uses */) const { NU_ASSERT(0=="unimplemented", this); }
  void getUses(const NUNetRefHdl& /* net_ref */, NUNetRefSet* /* uses */) const { NU_ASSERT(0=="unimplemented", this); }
  bool queryUses(const NUNetRefHdl & /* def_net_ref -- unused */,
		 const NUNetRefHdl & /* use_net_ref -- unused */,
		 NUNetRefCompareFunction /* fn -- unused */,
		 NUNetRefFactory * /* factory -- unused */) const
  {
    NU_ASSERT(0=="unimplemented", this);
    return false;
  }

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet* /* defs */) const { NU_ASSERT(0=="unimplemented", this); }
  void getDefs(NUNetRefSet* /* defs */) const { NU_ASSERT(0=="unimplemented", this); }
  bool queryDefs(const NUNetRefHdl & /* def_net_ref -- unused */,
		 NUNetRefCompareFunction /* fn -- unused */,
		 NUNetRefFactory * /* factory -- unused */) const
  {
    NU_ASSERT(0=="unimplemented", this);
    return false;
  }

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet* /* old_net */, NUNet* /* new_net */) { NU_ASSERT(0=="unimplemented", this); }

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * /* old_net */, NUNet * /* new_net */) { NU_ASSERT(0=="unimplemented", this); return false; }

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & /*translator*/) { NU_ASSERT(0=="unimplemented", this); return false; }

  //! Where is this node defined in the verilog source?
  const SourceLocator& getLoc() const { return mLoc; }

  //! Get the partent module for this use def node
  /*! Get the parent module for a statement or structured proc use def
   *  node. This should not be called on modules.
   */
  const NUModule* findParentModule() const { NU_ASSERT(0=="unimplemented", this); return 0;}
  NUModule* findParentModule() { NU_ASSERT(0=="unimplemented", this); return 0;}

  //! add the cost of this into the NUCost*.
  /*!
   * This caches the result into the context.  Everyone but context should
   * use this one.
   */
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! Compute the cost; expensive; only should be called by CostContext.
  void calcCostFromContext(NUCost*, NUCostContext*) const;

  //! Is a flow-node encapsulated in this cycle?
  bool isNodeEncapsulated(FLNodeElab* flow) const;

  //! debug print routine
  virtual void print(bool recurse, int indent) const;

  //! Does this net fanin to the cycle?
  bool isFaninNet(NUNetElab* net) const;

  //! Is this net driven by the cycle?
  bool isCycleNet(NUNetElab* net) const;

  //! Invalidate the fanin cache
  /*! This will cause the fanin to be recomputed before its next use. */
  void invalidateFaninCache() const { mFaninCacheIsStale = true; }

  //! How many branches are in the schedule for this cycle?
  UInt32 getBranchCount() const { return mBranchCount; }

  //! How many nodes are inside loops in the schedule for this cycle?
  size_t getSettledFlowCount() const { return mSettledFlowSet.size(); }

  //! Loop over the nodes that are scheduled inside of loops
  SortedLoop loopSettledFlows() const {return mSettledFlowSet.loopSorted();}
  
  //! Comparison operator works by cycle ID number
  bool operator<(const NUCycle& other) const { return mId < other.mId; }

  bool isScheduled () const;

  void putScheduled (bool f);

private:
  //! Hide copy and assign constructors.
  NUCycle(const NUCycle&);
  NUCycle& operator=(const NUCycle&);

  //! Routine to add the appopriate fanin for a given cycle flow node
  void addFlowFanin(FLNodeElab* flow, const FLNodeElabSet& cycleFlows) const;

  //! Get all of the leaf nodes of the cycle
  void gatherCycleFlows(FLNodeElabSet* cycleFlows) const;

  //! Rebuild the fanin cache
  void updateFaninCache() const;

  // A helper function that fills a vector with flow nodes in schedule order
  void fillFlowVector(FLNodeElabVector* vec, const NUCycleScheduleBlock* block) const;

private:
  //! Source location of this block.
  SourceLocator mLoc;

  //! A unique id
  UInt32 mId;

  FLNodeElabHashSet mFlowNodeSet;           //!< all flow nodes in the cycle
  NUNetElabSet mDefNets;                    //!< nets defed in the cycle

  NUCycleSettleLoop* mInLoop;               //!< points to enclosing loop while scheduling
  FLNodeElabHashSet mSettledFlowSet;        //!< all flow nodes in SettleLoop schedules
  UInt32 mBranchCount;                      //!< the number of branches in the schedule

  /* The fanin sets here act as the definitive fanin information for the cycle.
     This fanin information is used by all FLNodeElabCycles in the cycle,
     and it is computed entirely from the fanin of leaf nodes of the cycle.
     It is marked mutable so that the logical constness of the NUCycle methods
     is not affected by the state of the cache.
  */
  mutable FLNodeElabHashSet mFanin;         //!< cache of fanin to the cycle
  mutable NUNetElabSet mFaninNets;          //!< cache of fanin nets to the cycle
  mutable bool mFaninCacheIsStale;          //!< records the state of the fanin cache

  mutable bool mScheduled;      //!< Is this cycle ever scheduled?
  NUCycleScheduleBlock mSchedule; //!< A vector of schedule elements that defines the scheduled order
  UtStack<NUCycleScheduleBlock*> mScheduleStack; //!< A stack of partially-built schedules
  CycleBlockVector mOutlined;   //!< Holds outlined cycle blocks.
};

#endif // _NUCYCLE_H_
