// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUEXPR_H_
#define NUEXPR_H_

#include "nucleus/NUCopyContext.h"
#include "nucleus/NUUseNode.h"
#include "nucleus/NUNet.h"
#include "util/SourceLocator.h"
#include "util/ConstantRange.h"
#include "util/UtIOStream.h"
#include "util/DynBitVector.h"

class STBranchNode;
class DynBitVector;
class BDD;
class BDDContext;
class NUConst;
class MsgContext;

/*!
  \file
  NUExpr and derived class declarations.
*/

/*
std::ostream& operator<<(std::ostream& s, const DynBitVector& bv)
{
  unsigned long numWords = bv.getUIntArraySize();
  if (numWords > 2)
    s << "0x" << UtIO::hex << UtIO::setfill('0') << UtIO::setw(sizeof(numWords));
  unsigned long* bvArr = bv.getUIntArray();
  for (unsigned long i = numWords - 1; i > 0; ++i)
    s << bvArr[i];
  
  return s << bvArr[0];
}
*/

//! NUExpr class
/*!
 * Abstract base class, an expression.
 */
class NUExpr : public NUUseNode
{
public:
  //! constructor
  /*!
    \param loc Source location.
   */
  NUExpr(const SourceLocator& loc);

  //! Enumerated type so we can tell which expression type this is
  enum Type
  {
    eNUVarselRvalue,
    eNUConstXZ,
    eNUConstNoXZ,
    eNUEdgeExpr,
    eNUSysFunctionCall,
    eNUIdentRvalue,
    eNUIdentRvalueElab,
    eNUMemselRvalue,
    eNUUnaryOp,
    eNUBinaryOp,
    eNUTernaryOp,
    eNUConcatOp,
    eNULut,
    eNUNullExpr,  //!< created by double commas in an expression list ",,"
    eNUAttribute,
    eNUCompositeExpr,
    eNUCompositeIdentRvalue,
    eNUCompositeSelExpr,
    eNUCompositeFieldExpr
  };

  //! Function to return the expression type
  virtual Type getType() const = 0;

  //! Is this a constant expression?
  virtual bool isConstant() const { return (getType() == eNUConstXZ) or (getType() == eNUConstNoXZ); }

  //! Get source location
  const SourceLocator& getLoc() const { return mLoc; }

  //! Return the self-determined size of this expression.
  UInt32 getBitSize() const;

  //! Clear the cached value of the bit size
  /*!
   *! With the new sizing methodology, an expression's size is always the
   *! self-determined size.  But we still keep that value in mBitSize, because
   *! determineBitSize() is too expensive to run every time you want to
   *! examine the bit size.
   *!
   *! This size-caching is troublesome when an expression is mutated in some
   *! fashion.  When an expression is mutated via an NUExpr method, then of
   *! course it will update the cache if needed.  But when an expression is
   *! mutated by (say) changing the bit-size of an underlying net, then the
   *! system must inform the NUExpr that it's size-cache is no longer valid.
   *! This routine provides a mechanism to do that.
   *!
   *! This routine has no effect with the old sizing methodology (-newSizing
   *! not specified on cbuild command line)
   */
  void clearBitSizeCache();

  //! make a 1-bit condition-expression out of an arbitrary sized expression
  /*!
   *! This differs from makeSizeExplicit(1) because it takes into account
   *! the upper bits (e.g. (wide_val != 0)), where makeSizeExplicit would
   *! generate wide_val[0]
   */
  NUExpr* makeConditionExpr();

  //! Return the size this expression is "naturally".
  /*!
   * The size returned is the size the expression would be if the size was
   * self-determined.  Note that Verilog sizing is context sensitive in general,
   * so this will not necessary be the same value as getBitSize().
   */
  UInt32 determineBitSize() const;

  //! Resize this expression to be the given size.
  /*!
   * After this is called, getBitSize() will return this value.
   *
   * If you have a managed expression, then it is illegal call resize().
   * It will assert.  You should instead call resizeSubHelper(),
   * which returns an NUExpr* to replace this.
   */
  void resize(UInt32 size);

  //! return an expression such that the self-determined size matches the context size
  /*!
   * If the context size does not match the self-determined size
   * then a new expression is built so it will have the context size.  The
   * necessary partselect or extension (sign or zero padding) will be
   * done by the new expression.  The new expression will include the
   * original expression. If the sizes match then the original expression
   * may be  returned.
   *
   * NOTE: The implementation for NUExpr does not modify the
   * operand(s) of the expression, but some objects derived from NUExpr
   * must also make * the size of their operands explicit
   * (NUUnaryOp, NUBinaryOp. NUTernaryOp, NULut)
   *
   * default value for desired_size is the mBitSize
   */
  NUExpr* makeSizeExplicit(UInt32 desired_size = 0);

  //! Is this expression dynamically sized?
  virtual bool sizeVaries () const;

  //! Is this expression a real-number value?
  virtual bool isReal() const;

  //! Returns this as an NUConst. NULL if this isn't a constant
  virtual const NUConst* castConst() const;

  //! modifiable version
  NUConst* castConst() { 
    const NUExpr* me = const_cast<const NUExpr*>(this);
    return const_cast<NUConst*>(me->castConst());
  }
  
  //! Return this as an NUConst, if this is a pure constant with no strength
  virtual const NUConst* castConstNoXZ() const;
  
  //! modifiable version
  NUConst* castConstNoXZ() {
    const NUExpr* me = const_cast<const NUExpr*>(this);
    return const_cast<NUConst*>(me->castConstNoXZ());
  }
  
  //! Return this as an NUConst if this is a constant with strength
  virtual const NUConst* castConstXZ() const;

  //! modifiable version
  virtual NUConst* castConstXZ() {
    const NUExpr* me = const_cast<const NUExpr*>(this);
    return const_cast<NUConst*>(me->castConstXZ());
  }

  typedef UtHashMap<const NUExpr*, UInt32> ExprIntMap;

  //! How many bits of non-zero value are there?
  /*!
   * Compute the width of a value large enough to contain the leftmost
   * possible 1'b1 in the expression.
   *
   * A net's effectiveBitSize() is the same as its width.
   *
   * A constant's effectiveBitSize() is the width of the constant that
   * has a 1 in its leftmost bit position.  Thus the constant
   *  8'b00100000' has an effectiveBitSize() of 6.
   *
   * A constant zero has an effectiveBitSize() of ZERO, but a
   * signed value has an effectiveBitSize() equal to its getBitSize(), because sign
   * extension will yield sign bits propagated to whatever width you choose.
   *
   * Similarly, an unsigned expression right shifted by a constant amount
   * has its effectiveBitSize() reduced by the amount of the right-shift.
   * 
   * Default to safe contextually sized value if we don't have a better guess.
   *
   * Note that effectiveBitSize will search the entire expression tree.
   * If you are going to be calling this at every node of a large expression
   * then there are potential n^2 issues.  These can be resolved by passing
   * in an NUExpr::ExprIntMap* to cache values.  The caller is responsible
   * for ensuring that the cache is cleared if expressions are mutated.
   */
  UInt32 effectiveBitSize (ExprIntMap* ebit_cache = NULL) const;

  //! Helper function for derived expression classes to compute their effective bit size
  virtual UInt32 effectiveBitSizeHelper (ExprIntMap* ebit_cache) const;

  // Return NULL if not constant-evaluatable, otherwise return the value
  //virtual Constant* evaluate() = 0;

  //! Return true if this expression access a single, complete, identifier, false otherwise.
  virtual bool isWholeIdentifier() const { return false; }

  //! Return true if this is a simple expression accessing a hierref
  /*! Will return true for Ident, PartSel, and BitSel, where the net
   *  being accessed is a hierref, or when isWholeIdentifier() is true
   *  and the identifier is a hierref.
   */ 
  virtual bool isHierRef() const;

  //! Get the NUNet that is on the right hand side.
  /*!
   * This call is only valid if you know that you have a whole identifier
   */
  virtual NUNet* getWholeIdentifier() const;

  //! Construct LHS matching RHS, if possible
  virtual NULvalue* Lvalue(const SourceLocator &loc) const;

  //! Return the set of nets this node reads from
  virtual void getUses(NUNetSet *uses) const = 0;
  virtual void getUses(NUNetRefSet *uses) const = 0;
  virtual void getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const = 0;
  virtual void getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses) const = 0;
  virtual bool queryUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const = 0;

  //! Try to isolate the Z-causing condition, return the enable and driver.
  /*!
    \param enable Enable expression which will be populated if isolation was succesful.
    \param driver Drive expression which will be populated if isolation was succesful.
    \return true if able to isolate the z condition, false otherwise.

    It is possible that this expression drives Z unconditionally.  In that case, true
    is returned but the enable and driver are set to 0.

    If unable to isolate the z-causing condition, or if the expression does
    not drive Z, then enable and driver will not be updated.
   */
  virtual bool isolateZ(NUExpr ** /* enable -- unused */,
			NUExpr ** /* driver -- unused */) const
  {
    return false;
  }

  //! Return true if this expression can drive a Z value, false otherwise.
  /*! 
   *!  \param bits Optional bit-vector to receive a 1 for every bit that is
   *!              driven by Z
   */
  virtual bool drivesZ(DynBitVector* bits = NULL) const;

  //! Return true if this expression only drives a Z value, false otherwise.
  virtual bool drivesOnlyZ() const;

  //! Return true if this expression ought to preserve any Z values found
  //! on the specified net. 
  /*!
   *! Note that this does not imply (at this time) that
   *! Carbon will preserve the Z properly -- only that Verilog
   *! semantics require it.  This routine is for use in Nucleus
   *! transformation code, or in code that warns about potential
   *! mismatches with simulation
   */
  virtual bool shouldPreserveZ(NUNet*) const;

  //! Return a copy of this expression tree.
  NUExpr* copy(CopyContext &copy_context) const;

  //! destructor
  virtual ~NUExpr();

  //! Determine if two expression trees represent the same function.
  /*! Note: this disregards the source-locator field, but DOES require
   *! that the mSignedResult and bitsize be identical
   */
  bool operator==(const NUExpr & otherExpr) const {
    return compare(otherExpr, false, false, true) == 0;
  }

  //! Are two expressions equivalent (avoids sign comparisons and bitsize
  //! except for inside of concats.  This is looser than NUExpr::operator==(), and
  //! evaluates $SIGNED(0) == $UNSIGNED(0) as true.
  bool equal (const NUExpr& otherExpr) const {
    return compare (otherExpr, false, false, false) == 0;
  }

  //! Compare if two expression trees
  ptrdiff_t compare(const NUExpr& otherExpr, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const;

  //! Helper virtual function, which will always get an expr of the same type to cast
  virtual ptrdiff_t compareHelper(const NUExpr* otherExpr, bool compareLocator,
                                  bool comparePointer, bool compareSizeOfConsts) const = 0;

  //! shallow equality comparison -- used for NUExprFactory
  bool shallowEq(const NUExpr & otherExpr) const;

  //! Determine if two expression trees represent the different functions.
  bool operator!=(const NUExpr & otherExpr) const {
    return !(*this == otherExpr);
  }

  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  virtual BDD bdd(BDDContext *ctx, STBranchNode *scope=0) const = 0;

  //! Can we build a BDD for this expression?
  /*!
    Determines whether we can build a valid BDD based on pre-assigned
    leaves.  It does this without using the BDD package, so it's good
    to call this on expressions before consuming BDD space.

    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return true if we can generate a useful BDD
   */
  virtual bool hasBdd(BDDContext *ctx, STBranchNode *scope=0) const;

  //! Return whether the expression is signed or unsigned
  bool isSignedResult () const {return mSignedResult;}

  //! Set the sign
  /*!
   *! Per infrastructure spec, this method should be named putSignedResult(),
   *! but I'm leaving this one here to avoid the large diff for now
   */
  void setSignedResult(bool v) {putSignedResult(v);}

  //! Put the sign flag for the expression
  void putSignedResult(bool v);

  //! Compose a string giving verilog source code for the expression.
  //! Optionally generate a full hierarchical name with option to include 
  //! the top of hierarchy.  Note that bitselect and partselect indices
  //! will be given relative to the original verilog declarations, rather
  //! than showing the normalized indices in the nucleus representation.
  /*!
    \param buf composed results are appended to this string 
    \param scope if not null then this scope is used to create a hierarchical name
    \param includeRoot if true then the top of the hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  void compose(UtString* buf, const STBranchNode* scope, bool includeRoot = true,
               bool hierName = true, const char* separator = "." ) const 
  {
    if ( mLoc.isTicProtected() ){
      composeProtectedNUObject(mLoc, buf);
      return;
    }

    // call the class specific helper method, we define a single compose method on 
    // the root class so that every reference to this method will see the default arguments.
    composeHelper( buf, scope, includeRoot, hierName, separator );
  }

  //! special form of compose that avoids checking for protected regions
  /*! mostly used for strings within $display) be careful if
   * you use this because it can make `protected regions visible to the user
   */
  void composeNoProtected(UtString* buf, const STBranchNode* scope, bool includeRoot = true,
                          bool hierName = true, const char* separator = "." ) const 
  {
    // call the class specific helper method, we define a single compose method on 
    // the root class so that every reference to this method will see the default arguments.
    composeHelper( buf, scope, includeRoot, hierName, separator );
  }

  virtual void composeHelper(UtString* buf, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const = 0; 

  //! compose verilog name and when necessary size any constants (this is the default implementation)
  virtual void composeSized(UtString* pbuf, const STBranchNode* scope, CarbonRadix /* radix */) const
  {
    compose ( pbuf, scope );
  }

  //! print verilog-syntax
  void printVerilog(bool addNewline = false) const;

  //! return elaborated identifier, if found, else return NULL
  virtual NUNetElab* getNetElab(STBranchNode* scope) const;

  //! hash an expression
  virtual size_t hash() const = 0;

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const = 0;

  //! manage this expression, and all its children, in a factory
  /*! Expressions managed by a factory do not own their sub-expressions.
   *! All sub-expressions are managed by the factory, and can therefore
   *! be shared.
   */
  virtual void manage(NUExprFactory*);

  //! is this expression managed by a factory?
  bool isManaged() const {return mIsManaged;}

  //! does this expression own its sub-expressions?
  bool ownsSubExprs() const {return !mIsManaged;}

  //! return a simplified catenated expression if one exists
  /*! Bit/part selects are combined.  a[2:1],a[0] => a[2:0]
   */
  virtual NUExpr* catenate(const NUExpr*, CopyContext &) const;

  //! convenience method for constructing an Op based on managed const exprs
  inline NUExpr* constCast() const;

  //! Print design object information about an assertion
  virtual void printAssertInfoHelper() const;

  //! compare two expressions for stable printing order
  static ptrdiff_t compare(const NUExpr* e1, const NUExpr* e2,
                           bool compareLocator,
                           bool cmpPointer,
                           bool cmpSizeOfConsts)
  {
    return e1->compare(*e2, compareLocator, cmpPointer, cmpSizeOfConsts);
  }

  //! Easier interface to call replaceLeaves on an expression, where the
  //! expression itself may need to change into a different type
  NUExpr* translate(NuToNuFn & translator);

  //! for the current expression (a memory index expression), if wider than 32 bits then return a truncated version and tell user
  /*! returns truncated version, or original expression if already
   * small enough.
   * the bool pointed to by was_truncated is set to true if truncation performed
   */
  NUExpr* limitMemselIndexExpr(bool *was_truncated, MsgContext* msg_context, const SourceLocator &loc);


  /*! \brief Replace an expression, including one that represents an
   * identifier, using the netToExpr callback.
   *
   * This can only be done if the identifier is an NUIdentRvalue, but it
   * might get replaced with something that is not an NUIdentRvalue.  This
   * is intended to be used with bitsels and partsels.  So it returns
   * information about how to normalize the bitsel/partsel offset to 0 in
   * &lsb.
   */
  static bool replaceExpr( NUExpr** ident, NuToNuFn::Phase pahse, NuToNuFn& translator );

  //! Helper function used by NUExpr subclass resize methods to resize sub expressions
  /*!
   *! This avoids mutating managed expressions by copying them if they need
   *! to change size
   *!
   *! This should only be called from an NUExpr-derived class, but I was not
   *! able to make that work putting it in the 'protected' section, because
   *! subclasses of NUExpr call this on their NUExpr* member variables.  I'm
   *! not clear why that should not be allowed.
   */
  NUExpr* resizeSubHelper(UInt32 size);

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  NUExpr* getArg(UInt32 index) {
    const NUExpr* me = this;
    return const_cast<NUExpr*>(me->getArgHelper(index));
  }

  //! Return number of arguments to this operator.
  virtual UInt32 getNumArgs() const;

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  const NUExpr* getArg(UInt32 index) const {
    return getArgHelper(index);
  }

  //! Mutate the argument -- assumes ownership of new_arg
  /*!
   *! The caller is responsible for disposal of the old arg, which is
   *! returned
   */
  NUExpr* putArg(UInt32 index, NUExpr* new_arg);

  //! coerce the non-self determined operands to real
  virtual NUExpr* coerceOperandsToReal( );

protected:
  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual const NUExpr* getArgHelper(UInt32 index) const;

  //! Mutate the argument
  virtual NUExpr* putArgHelper(UInt32 index, NUExpr*);

  //! recursion helper for determineBitSize
  virtual UInt32 determineBitSizeHelper() const = 0;

  //! Transform this expression into one whose determineBitSize is desired_size
  /*!
   *! The base-class implementation of this method uses:
   *!   - part-selects to reduce the size of expressions we want to shrink
   *!   - concats 0s onto unsigned expressions we we want to grow
   *!   - uses SXT to for signed expressions we want to grow
   *!
   *! Subclasses of NUExpr can override makeSizeExplicitHelper for a
   *! more optimal implementation.  For example, if we want 16 bits
   *! of a 32-bit constant, we can just shrink the constant directly
   *! rather than part-selecting it.  We can part-select operands of
   *! + rather than part-selecting the result of the +, propagating
   *! the explicit-size down the expression tree.  But a fallback
   *! is always for a subclass to call this base class helper to
   *! complete the size adjustment, if necessary.
   */
  virtual NUExpr* makeSizeExplicitHelper(UInt32 desired_size);

  //! Helper function to copy expr tree
  virtual NUExpr* copyHelper(CopyContext &copy_context) const = 0;

  //! shallow equality helpers
  virtual bool shallowEqHelper (const NUExpr& otherExpr) const;

  //! Helper method for resizing an expression to be the given size.
  virtual void resizeHelper(UInt32 size) = 0;

  void printSize(UtOStream&) const;

  //! Source location of this expression.
  SourceLocator mLoc;

  //! Sign-extension of result
  bool mSignedResult;

  //! Is this expression managed in a factory, and therefore does
  //! not own its sub-expressions?
  bool mIsManaged APIOMITTEDFIELD;

  //! Has makeSizeExplicit been called on this expr?
  bool mSizeMadeExplicit APIOMITTEDFIELD;

  //! Verilog-sized bit size
  /*!
   *! Even NUExpr-derived classes must call getBitSize() to read the
   *! value, because this value is computed as needed by calling
   *! determineBitSize() or getBitSize(), when using the new sizing
   *! methodology (gNewSizingMethdology).  In that methdology, mBitSize
   *! can also be changed by other mutating methods, such as
   *! makeSizeExplicit, and NUConcatOp::putRepeatCount.  When altering
   *! OP args, we clear the mBitSize so it gets recomputed with the
   *! new args.
   */
  mutable UInt32 mBitSize;


private:
  //! Hide copy and assign constructors.
  NUExpr(const NUExpr&);
  NUExpr& operator=(const NUExpr&);

  DECLARENUCLEUSCLASSFRIENDS();

}  APIDISTILLCLASS; //class NUExpr : public NUUseNode



//! NUEdgeExpr class
/*!
 * Models an edge event expression, for example "@ posedge clk"
 */
class NUEdgeExpr : public NUExpr
{
public:

  //! constructor
  /*!
    \param edge What type of edge
    \param expr The expression of this event
    \param loc Source location.
   */
  NUEdgeExpr(ClockEdge edge, NUExpr *expr, const SourceLocator& loc);

  //! constructor based on managed operators
  /*!
    \param edge What type of edge
    \param expr The expression of this event
    \param loc Source location.
   */
  NUEdgeExpr(ClockEdge edge, const NUExpr *expr, const SourceLocator& loc);

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUEdgeExpr; }

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! Return the expression for the event
  const NUExpr *getExpr() const { return mExpr; }

  //! Return the expression for the event
  NUExpr *getExpr() { return mExpr; }

  //! Return the set of nets this node reads from
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  void getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const;
  void getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Return the type of edge
  ClockEdge getEdge() const { return mEdge; }
  
  //! Return the net which is the edge.  Only valid for trivial expressions.
  //! TBD: what type of expressions are allowed as edge expressions?
  NUNet* getNet() const;

  //! Set this edge expression to be a clock.
  void setIsClock();

  //! Set this edge expression to be a reset.
  void setIsReset();

  //! Return true if this is a clock.
  bool isClock() const { return mClock; }

  //! Return true if this is a reset.
  bool isReset() const { return mReset; }

  //! Generate code for this expression (no-op to codegen?)
  virtual CGContext_t emitCode (CGContext_t) const {return 0;}

  //! Dump myself to cout
  void print(bool recurse, int ident) const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param buf location to hold the result
    \param scope Build name upto and including specified scope,
     if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString* buf, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  //! Return class name
  const char* typeStr() const;

  //! Determine if two expression trees represent the same function.
  virtual ptrdiff_t compareHelper(const NUExpr* otherExpr, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const;

  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  BDD bdd(BDDContext *ctx, STBranchNode *scope=0) const;

  //! Return a copy of this expression tree, but with opposite edge sensitivity.
  NUExpr* copyOpposite() const;

  //! calculate cost, add it to NUCost* argument
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUEdgeExpr();

  //! hash an expression
  virtual size_t hash() const;

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const;

  //! manage this expression and all sub-expressions in a factory
  virtual void manage(NUExprFactory*);

  //! copy an edge-expr without without upcasting
  NUEdgeExpr* copyEdgeExpr(CopyContext& cc) const;

  virtual UInt32 getNumArgs() const;


protected:
  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual const NUExpr* getArgHelper(UInt32 index) const;

  //! Mutate the argument
  virtual NUExpr* putArgHelper(UInt32 index, NUExpr*);

  //! Transform this expression into one whose determineBitSize is desired_size
  virtual NUExpr* makeSizeExplicitHelper(UInt32 desired_size);

  //! coerce the non-self determined operands to real
  virtual NUExpr* coerceOperandsToReal( );

  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;
  
  //! shallow equality comparison -- used for NUExprFactory
  virtual bool shallowEqHelper(const NUExpr & otherExpr) const;

  //! Resize this expression to be the given size.
  void resizeHelper(UInt32 size);

private:
  //! Hide copy and assign constructors.
  NUEdgeExpr(const NUEdgeExpr&);
  NUEdgeExpr& operator=(const NUEdgeExpr&);

  //! Which edge this expression is active.
  ClockEdge mEdge;

  //! The subexpression.
  NUExpr *mExpr;

  //! True if this is a clock.
  bool mClock;

  //! True if this is a reset.
  bool mReset;

  DECLARENUCLEUSCLASSFRIENDS();

}  APIDISTILLCLASS; //class NUEdgeExpr : public NUExpr


//! NUIdentRvalue class
/*!
 * Whole identifier used in an expression.
 */
class NUIdentRvalue : public NUExpr
{
public:
  //! constructor
  /*!
    \param net The identifier
    \param loc Source location.
   */
  NUIdentRvalue(NUNet *net, const SourceLocator& loc);

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUIdentRvalue; }

  //! Return the identifier
  NUNet* getIdent() const { return mNet; }

  //! Return the elaborated identifier
  virtual NUNetElab* getNetElab(STBranchNode* scope) const;

  //! Return true if this expression access a single, complete, identifier, false otherwise.
  bool isWholeIdentifier() const { return true; }

  //! Return true if this is a simple expression accessing a hierref
  /*! Returns true if the net is a hier ref
   */
  virtual bool isHierRef() const;

  //! Examine ident to see if it's a real-number reference
  virtual bool isReal() const;

  //! Construct LHS matching RHS
  NULvalue* Lvalue(const SourceLocator &loc) const;

  //! Get the NUNet that is on the right hand side.
  /*!
   * This call is only valid if you know that you have a whole identifier
   */
  NUNet* getWholeIdentifier() const { return getIdent(); }

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! How many bits of effective value are there?
  virtual UInt32 effectiveBitSizeHelper (ExprIntMap* ebit_cache) const;

  //! Return the set of nets this node reads from
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  void getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const;
  void getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int ident) const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope if not null then this scope is prepended to name (if hierarchical name is created)
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  //! Return class name
  const char* typeStr() const;

  //! destructor
  ~NUIdentRvalue();

  //! Determine if two expression trees represent the same function.
  virtual ptrdiff_t compareHelper(const NUExpr* otherExpr, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const;

  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  virtual BDD bdd(BDDContext *ctx, STBranchNode *scope=0) const;

  //! Can we build a BDD for this expression?
  /*!
    Determines whether we can build a valid BDD based on pre-assigned
    leaves.  It does this without using the BDD package, so it's good
    to call this on expressions before consuming BDD space.

    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return true if we can generate a useful BDD
   */
  virtual bool hasBdd(BDDContext *ctx, STBranchNode *scope) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! hash an expression
  virtual size_t hash() const;

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const;

  //! Return true if this expression ought to preserve any Z values found
  //! on the specified net. 
  /*!
   *! Note that this does not imply (at this time) that
   *! Carbon will preserve the Z properly -- only that Verilog
   *! semantics require it.  This routine is for use in Nucleus
   *! transformation code, or in code that warns about potential
   *! mismatches with simulation
   */
  virtual bool shouldPreserveZ(NUNet*) const;

protected:
  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;

  //! Resize this expression to be the given size.
  void resizeHelper(UInt32 size);


private:
  //! Hide copy and assign constructors.
  NUIdentRvalue(const NUIdentRvalue&);
  NUIdentRvalue& operator=(const NUIdentRvalue&);

  //! The identifier
  NUNet* mNet;

  DECLARENUCLEUSCLASSFRIENDS();

}  APIDISTILLCLASS; //class NUIdentRvalue : public NUExpr

//! NUIdentRvalueElab class
/*!
 * Whole identifier used in an expression.
 */
class NUIdentRvalueElab : public NUIdentRvalue
{
public:
  //! constructor
  /*!
    \param net The identifier
    \param loc Source location.
   */
  NUIdentRvalueElab(NUNetElab *net, const SourceLocator& loc);

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUIdentRvalueElab; }

  //! Return the identifier
  virtual NUNetElab* getNetElab(STBranchNode*) const { return mNetElab; }

  //! Return class name
  const char* typeStr() const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name upto and including specified scope,
           if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  CGContext_t emitCode (CGContext_t context) const;

  //! destructor
  ~NUIdentRvalueElab();

  //! Determine if two expression trees represent the same function.
  virtual ptrdiff_t compareHelper(const NUExpr* otherExpr, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int ident) const;

  //! Return the set of nets this node reads from
  void getUsesElab(STBranchNode*, NUNetElabSet *uses) const;
  void getUsesElab(STBranchNode*, NUNetElabRefSet2 *uses) const;

  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  virtual BDD bdd(BDDContext *ctx, STBranchNode *scope) const;

  //! Can we build a BDD for this expression?
  /*!
    Determines whether we can build a valid BDD based on pre-assigned
    leaves.  It does this without using the BDD package, so it's good
    to call this on expressions before consuming BDD space.

    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return true if we can generate a useful BDD
   */
  virtual bool hasBdd(BDDContext *ctx, STBranchNode *scope) const;

  //! hash an expression
  virtual size_t hash() const;

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const;


protected:
  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;

private:
  //! Hide copy and assign constructors.
  NUIdentRvalueElab(const NUIdentRvalue&);
  NUIdentRvalueElab& operator=(const NUIdentRvalue&);

  //! The elaborated identifier.
  NUNetElab *mNetElab;

  DECLARENUCLEUSCLASSFRIENDS();

} APIOMITTEDCLASS; // class NUIdentRvalueElab : public NUIdentRvalue



//! NUVarselRvalue class
/*!
 * Variable Part select of a vector or expression.
 *
 * Varsel's provide the semantic representation for Verilog part-selects,
 * bit-selects and variable part-selects (the x[i+:width] syntax).
 *
 */
class NUVarselRvalue : public NUExpr
{
public:
  //! constructor (variable part-select)
  /*!
    \param net The identifier.
    \param idx Variable bit offset expression.
    \param range Range of the select.
    \param loc Source location.
   */
  NUVarselRvalue (NUNet *net,
                  NUExpr *idx,
                  const ConstantRange& range,
                  const SourceLocator& loc);

  //! constructor (variable part-select)
  /*!
    \param net The identifier.
    \param idx Variable bit offset expression.
    \param range Range of the select.
    \param loc Source location.
   */
  NUVarselRvalue (const NUNet *net,
                  const NUExpr *idx,
                  const ConstantRange& range,
                  const SourceLocator& loc);

  //! constructor (variable part-select)
  /*!
    \param net The identifier.
    \param idx Variable bit offset expression.
    \param range Range of the select.
    \param loc Source location.
   */
  NUVarselRvalue (const NUExpr *net,
                  const NUExpr *idx,
                  const ConstantRange& range,
                  const SourceLocator& loc);

  //! constructor (variable part-select)
  /*!
    \param base base object expression
    \param idx Variable bit offset expression.
    \param range Range of the select.
    \param loc Source location.
   */
  NUVarselRvalue (NUExpr *base,
                  NUExpr *idx,
                  const ConstantRange& range,
                  const SourceLocator& loc);

  
  //! constructor (part-select)
  /*!
    \param net The identifier
    \param range Range of the select
    \param loc Source location.
   */
  NUVarselRvalue(NUNet *net,
                 const ConstantRange& range,
                 const SourceLocator& loc);

  //! constructor (part-select of an expression)
  /*!
    \param expr The source expression
    \param range Range of the desired select
    \param loc Source location.
   */
  NUVarselRvalue(NUExpr *expr,
                 const ConstantRange& range,
                 const SourceLocator& loc);

  //! constructor (part-select of a const expression)
  /*!
    \param expr The source expression
    \param range Range of the desired select
    \param loc Source location.
   */
  NUVarselRvalue(const NUExpr *expr,
                 const ConstantRange& range,
                 const SourceLocator& loc);

  //! constructor (bit-select)
  /*!
    \param net The identifier
    \param expr The index expression
    \param loc Source location.
   */
  NUVarselRvalue(NUNet *net,
                 NUExpr *expr,
                 const SourceLocator& loc);

  //! constructor (bit-select)
  /*!
    \param net The identifier
    \param expr The index expression
    \param loc Source location.
   */
  NUVarselRvalue(NUExpr *net,
                 NUExpr *expr,
                 const SourceLocator& loc);

  //! constructor (bit-select)
  /*!
    \param net The identifier
    \param expr The index expression
    \param loc Source location.
   */
  NUVarselRvalue(const NUExpr *net,
                 const NUExpr *expr,
                 const SourceLocator& loc);

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUVarselRvalue; }

  //! Return the identifier
  NUNet* getIdent() const { return mIdent->getWholeIdentifier(); }

  //! Return the identifier net
  /*! \param noArray defaults to true.  Set it to false to allow
   *                 the routine to return a pointer to the NUMemoryNet
   *                 for an array select.  When noArray=true, an assert
   *                 is generated when getIdentNet() is called on an
   *                 array select.
   */
  NUNet* getIdentNet(bool noArray=true) const;

  //! Return the identifier
  const NUExpr* getIdentExpr() const { return mIdent; }

  //! Return the identifier
  NUExpr* getIdentExpr() { return mIdent; }

  //! Return true if this is a select of a row of an array
  bool isArraySelect() const;

  //! Return the elaborated identifier, if there is one
  NUNetElab* getNetElab(STBranchNode* scope) const;

  //! Return the index expression
  const NUExpr* getIndex() const { return mExpr; }
  
  //! Return the index expression
  NUExpr* getIndex() { return mExpr; }
  
  // Return true if the index is constant and the identifier is whole.
  bool isSimpleConstantVarsel() const;

  //! Return the whether the index is constant
  bool isConstIndex() const;
  
  //! Return the constant index.  Only valid if isConstIndex()
  SInt32 getConstIndex() const;

  //! Return the select range without applying any variable index
  const ConstantRange* getRange() const { return &mRange; }

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! How many bits of effective value are there?
  virtual UInt32 effectiveBitSizeHelper(ExprIntMap* ebit_cache) const;

  //! Return true if size of partselect can vary dynamically (no larger than determineBitSize())
  virtual bool sizeVaries () const;

  //! Return true if this expression access a single, complete, identifier, false otherwise.
  bool isWholeIdentifier() const;

  //! Return true if this is a simple expression accessing a hierref
  /*! Returns true if the ident is a hier ref
   */
  virtual bool isHierRef() const;

  //! Get the NUNet that is on the right hand side.
  /*!
   * This call is only valid if you know that you have a whole identifier
   */
  NUNet* getWholeIdentifier() const;

  //! Construct LHS matching RHS, if possible
  NULvalue* Lvalue(const SourceLocator &loc) const;

  //! Return the set of nets this node reads from
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  void getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const;
  void getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int ident) const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name upto and including specified scope,
           if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  //! Return class name
  const char* typeStr() const;

  //! Determine if two expression trees represent the same function.
  virtual ptrdiff_t compareHelper(const NUExpr* otherExpr, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const;

  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  BDD bdd(BDDContext *ctx, STBranchNode *scope=0) const;

  //! Can we build a BDD for this expression?
  /*!
    Determines whether we can build a valid BDD based on pre-assigned
    leaves.  It does this without using the BDD package, so it's good
    to call this on expressions before consuming BDD space.

    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return true if we can generate a useful BDD
   */
  virtual bool hasBdd(BDDContext *ctx, STBranchNode *scope) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUVarselRvalue();

  //! hash an expression
  virtual size_t hash() const;

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const;

  //! manage this expression and all sub-expressions in a factory
  virtual void manage(NUExprFactory*);

  //! return a simplified catenated expression if one exists
  /*! Bit/part selects are combined.  a[2:1],a[0] => a[2:0]
   */
  virtual NUExpr* catenate(const NUExpr* rop, CopyContext &cc) const;


  //! Return true if this expression ought to preserve any Z values found
  //! on the specified net. 
  /*!
   *! Note that this does not imply (at this time) that
   *! Carbon will preserve the Z properly -- only that Verilog
   *! semantics require it.  This routine is for use in Nucleus
   *! transformation code, or in code that warns about potential
   *! mismatches with simulation
   */
  virtual bool shouldPreserveZ(NUNet*) const;

  //! Is indexing in bounds?
  bool isIndexInBounds () const;

  //! modify boundedness flag
  void putIndexInBounds (bool b);

  virtual UInt32 getNumArgs() const;


private:
  //! Helper function to canonicalize constant indexing into the range
  bool fixConstExpr (void);

protected:
  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual const NUExpr* getArgHelper(UInt32 index) const;

  //! Mutate the argument
  virtual NUExpr* putArgHelper(UInt32 index, NUExpr*);

  //! Hide copy and assign constructors.
  NUVarselRvalue(const NUVarselRvalue&);
  NUVarselRvalue& operator=(const NUVarselRvalue&);

  //! shallow equality comparison -- used for NUExprFactory
  virtual bool shallowEqHelper(const NUExpr & otherExpr) const;

  //! Resize this expression to be the given size.
  void resizeHelper(UInt32 size);

  //! Transform this expression into one whose determineBitSize is desired_size
  virtual NUExpr* makeSizeExplicitHelper(UInt32 desired_size);

  //! coerce the non-self determined operands to real
  virtual NUExpr* coerceOperandsToReal( );

  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;

  //! The identifier.
  NUExpr* mIdent;

  //! The variable bit offset
  NUExpr* mExpr;

  //! The range of bits selected.
  ConstantRange mRange;
  
  //! Varsel indexing expression is guaranteed to be inbounds
  bool mIndexInBounds;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUVarselRvalue : public NUExpr

//! NUMemselRvalue class
/*!
 * Select a location of a memory, select a word (or slice?) of a memory
 */
class NUMemselRvalue : public NUExpr
{
public:
  //! constructor
  /*!
    \param net The identifier
    \param expr Range of the select
    \param loc Source location.
   */
  NUMemselRvalue(NUNet *net,
                 NUExpr *expr,
                 const SourceLocator& loc);

  //! constructor
  /*!
    \param net The identifier
    \param exprs Vector of ranges of the select
    \param loc Source location.
   */
  NUMemselRvalue(NUNet *net,
                 NUExprVector *exprs,
                 const SourceLocator& loc);

  //! constructor
  /*!
    \param netElab The identifier
    \param expr Range of the select
    \param loc Source location.
   */
  NUMemselRvalue(NUExpr *netElab,
                 NUExpr *expr,
                 const SourceLocator& loc);

  //! constructor
  /*!
    \param netElab The identifier
    \param exprs Vector of ranges of the select
    \param loc Source location.
   */
  NUMemselRvalue(NUExpr *netElab,
                 NUExprVector *exprs,
                 const SourceLocator& loc);

  //! constructor
  /*!
    \param netElab The identifier
    \param expr Range of the select
    \param loc Source location.
   */
  NUMemselRvalue(const NUExpr *netElab,
                 const NUExpr *expr,
                 const SourceLocator& loc);

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUMemselRvalue; }

  //! Construct LHS matching RHS
  NULvalue* Lvalue(const SourceLocator &loc) const;

  //! Return the identifier
  NUNet* getIdent() const { return mIdent->getWholeIdentifier(); }

  //! Return the identifier
  const NUExpr* getIdentExpr() const { return mIdent; }

  //! Return the identifier
  NUExpr* getIdentExpr() { return mIdent; }

  //! Return the identifier
  NUNetElab* getNetElab(STBranchNode* scope) const;

  //! Return the index expression
  const NUExpr* getIndex(UInt32 dim) const { return mExprs[dim]; }

  //! Return the index expression
  NUExpr* getIndex(UInt32 dim) { return mExprs[dim]; }

  //! prepend another index expression onto the memsel's expression vector
  void prependIndexExpr( NUExpr *expr );

  //! Push another index expression onto the memsel's expression vector
  void addIndexExpr( NUExpr *expr );

  //! Return the number index expressions this memsel has
  UInt32 getNumDims() const { return mExprs.size(); }

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! How many bits of effective value are there?
  virtual UInt32 effectiveBitSizeHelper(ExprIntMap* ebit_cache) const;

  //! Return the set of nets this node reads from
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  void getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const;
  void getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
                 NUNetRefCompareFunction fn,
                 NUNetRefFactory *factory) const;

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int ident) const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name upto and including specified scope,
           if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  //! Return class name
  const char* typeStr() const;

  //! Determine if two expression trees represent the same function.
  virtual ptrdiff_t compareHelper(const NUExpr* otherExpr, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const;

  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  BDD bdd(BDDContext *ctx, STBranchNode *scope=0) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUMemselRvalue();

  //! hash an expression
  virtual size_t hash() const;

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const;

  //! manage this expression and all sub-expressions in a factory
  virtual void manage(NUExprFactory*);

  //! Return true if this is a simple expression accessing a hierref
  /*! Returns true if the ident is a hier ref
   */
  virtual bool isHierRef() const;

  //! Has this memsel been through multidimensional resynthesis?
  bool isResynthesized() const;

  //! set the resynthesis flag
  void putResynthesized( bool resynth );

  virtual UInt32 getNumArgs() const;


protected:
  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual const NUExpr* getArgHelper(UInt32 index) const;

  //! Mutate the argument
  virtual NUExpr* putArgHelper(UInt32 index, NUExpr*);

  //! shallow equality comparison -- used for NUExprFactory
  virtual bool shallowEqHelper(const NUExpr & otherExpr) const;

  //! Resize this expression to be the given size.
  void resizeHelper(UInt32 size);

  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;

private:
  //! Hide copy and assign constructors.
  NUMemselRvalue(const NUMemselRvalue&);
  NUMemselRvalue& operator=(const NUMemselRvalue&);

  //! The identifier, expected to only be a full identifier of a memory net.
  NUExpr* mIdent;

  //! The vector or index selections into the memory
  NUExprVector mExprs;

  //! flag indicating memory resynthesis has dealt with this node
  bool mResynthesized;
} APIDISTILLCLASS; //class NUMemselRvalue : public NUExpr

// forward decls
class DynBitVector;

//! NUConst class
/*!
 * A constant accessed via virtual methods to get the size, representation, drive
 * and a host of other HDL properties.
 */
class NUConst : public NUExpr
{
public:

  //! constructor
  /*!
    \param bitSize Natural size of this constant.
    \param loc Source location.
   */
  NUConst(UInt32 bitSize, const SourceLocator& loc);

  //! How many bits of effective value are there? Pass this thru to subclasses
  //virtual UInt32 effectiveBitSizeHelper(ExprIntMap* ebit_cache) const;

  //! Return the set of nets this node reads from, will always be the empty set
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  void getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const;
  void getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Emit C++ Code
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Dump myself to cout
  void print(bool recurse = true, int indent = 0) const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name upto and including specified scope,
           if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  //! compose verilog-syntax, sizing operands when necessary
  virtual void composeSized(UtString*, const STBranchNode*, CarbonRadix) const;

  //! Return class name
  virtual const char* typeStr() const = 0;

  //! Create a copy with a different size and sign
  virtual NUConst* rebuild(bool isSignedResult, UInt32 bitSize) const = 0;

  //! destructor
  virtual ~NUConst();

  virtual size_t hash() const;

  virtual size_t shallowHash() const;

  //! return this  
  virtual const NUConst* castConst() const;
    
  
  //! 
  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  BDD bdd(BDDContext *ctx, STBranchNode *scope=0) const;

  //! Can we build a BDD for this expression?
  /*!
    Determines whether we can build a valid BDD based on pre-assigned
    leaves.  It does this without using the BDD package, so it's good
    to call this on expressions before consuming BDD space.

    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return true if we can generate a useful BDD
   */
  virtual bool hasBdd(BDDContext *ctx, STBranchNode *scope) const;

  //! put the unsigned value of the const into val (a ULL), always returns true
  /*!
    This only gets the first 64 bits of a value. So, if a value is
    more than that the value is truncated. 
  */
  virtual bool getULL(UInt64* val) const = 0;

  //! put the signed value of the const into val (a LL), always returns true
  /*!
    This only gets the first 64 bits of a value. So, if a value is
    more than that the value is truncated. 
  */
  virtual bool getLL(SInt64* val) const = 0;

  //! puts the first 32 bits of the unsigned constant into val, always returns true
  virtual bool getUL(UInt32* val) const = 0;

  //! puts the signed value of a const into val, always returns true
  /*!
    This only gets the first 32 bits of a value. So, if a value is
    more than that the value is truncated. 
   */
  virtual bool getL(SInt32* val) const = 0;

  //! Get a CarbonReal
  virtual bool getCarbonReal (CarbonReal* val) const = 0;

  //! Get the value of the const regardless of drive 
  /*!
    This will only return the value and will set any x's and z's to 0. So,
    without the drive the value is not obvious as to what is really
    is, but codegen doesn't care about drive, except maybe in rare
    cases.
    For known constants, this will negate the constant if the sign is
    negative. Currently, unknowns will always be returned as positive
    regardless of sign. Would a value with x's and z's ever be
    negative?
  */
  virtual void getSignedValue(DynBitVector* value) const = 0;

  //! Get the value and the drive, returns false if not yet resized
  virtual void getValueDrive(DynBitVector* value, DynBitVector* drive,
                             bool allowUnsized = false) const;

  //! Is constant a zero?
  virtual bool isZero (void) const = 0;

  //! Is constant all ones?
  virtual bool isOnes (void) const = 0;

  //! Is constant a possible negative value
  virtual bool isNegative () const = 0;

  //! Does constant have X or Z?
  virtual bool hasXZ() const = 0;

  //! Does this have 0/1/X and Z's?
  virtual bool isPartiallyDriven() const = 0;

  //! returns true if this constant was defined by a string value.
  virtual bool isDefinedByString () const = 0;

  //! Create a constant that is known to have no x's or z's.
  static NUConst* createKnownConst(const UtString& val,
                                   UInt32 bit_width, const SourceLocator& loc);
  //! Create a constant that is known to have no x's or z's. and was defined by a string e.g. "foo"
  static NUConst* createKnownConst(const UtString& val,
                                   UInt32 bit_width, bool definedByString,
                                   const SourceLocator& loc);

  //! Create a constant that is known to have x's and z's
  static NUConst* createXZ(const UtString& val, bool isSignedResult,
                           UInt32 bit_width, const SourceLocator& loc);

  //! Create a known constant that is less than 65 bits, result is unsigned
  static NUConst* createKnownIntConst(UInt64 val,
                                      UInt32 bit_width,
                                      bool definedByString,
                                      const SourceLocator& loc);

  //! Create a constant from a UtString of 1,0,x and z  
  /*!
    This will look for an unknown in the UtString and create the
    appropriate constant type

    \warning this is \e not for use in creating a constant from a verilog
    string, see one of the variants of  createKnownConst to do that.
  */
  static NUConst* createConstFromStr(const UtString& val,
                                     UInt32 bit_width, 
                                     const SourceLocator& loc);

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  /*! \brief Returns the minimum number of bits required to represent val
   *
   *  \param val the value in question
   *  \param isSigned set to true if \a val is signed, false if unsigned
   *  returns 1 for the value 0
   */
  static UInt32 determineOptimalBitSize(const UtString* val, bool isSigned);
  /*! \brief Returns the minimum number of bits required to represent val
   *
   * \param val the value in question
   * \param isSigned set to true if \a val is signed, false if unsigned
   * \param bitSize is the width that should be used to interpret the
   *      bit pattern in \a val.  If larger than 64 (the maximum that
   *      is possible to represent in the UInt64 arg \a val) then the
   *      following assumption is made.
   *      All bits beyond the 64 in val are insignificant, this means that
   *      if isSigned is true then they are assumed to be identical to the
   *      most significant bit of val, if isSigned is false then they are
   *      assumed to be all zeros.
   */
  static UInt32 determineOptimalBitSize(UInt64 val, bool isSigned, UInt32 bitSize);
  

  //! Construct a proper constant for bit-vector
  static NUConst* create(bool resultsign, const DynBitVector& result,
                         UInt32 width, const SourceLocator& loc);

  static NUConst* createXZ(bool sign, const DynBitVector& value, const DynBitVector& drive,
                         UInt32 width, const SourceLocator& loc);

  //! Construct a proper constant for small integer
  static NUConst* create(bool resultsign, UInt64 result, UInt32 width,
                         const SourceLocator& loc);

  //! Construct a proper constant for small integer with XZ
  static NUConst* createXZ(bool sign, UInt64 value, UInt64 drive, UInt32 width,
                         const SourceLocator& loc);

  //! Construct a proper constant for a double
  static NUConst* create ( CarbonReal value, const SourceLocator& loc);


protected:
  //! Hide copy and assign constructors.
  NUConst(const NUConst&);
  NUConst& operator=(const NUConst&);

  //! Resize this expression to be the given size.
  void resizeHelper(UInt32 size);

  //! Clear the bits in the representation that are outside the bounds
  //! of the mBitSize.
  /*! Note that if you resize a 32-bit constant,
   *! smaller and then larger, we will zero the high order bits.  For
   *! example, if you have 32'hff000000, and you resize it to 16 bits,
   *! and then resize it back to 32 bits, you will be left with
   *! 32'h00000000.  So don't call resize spuriously
   */
  virtual void clearDirtyBits(UInt32 size) = 0;

  //! Natural size of the constant; the size specified in the Verilog source.
  UInt32 mNaturalSize;

  //! Print the value to stdout \a radix defines the desired radix
  virtual void printVal(UtString*, CarbonRadix radix) const = 0;

  //! Value printing for a DynBitVector...
  void printValueByType(UtString*, const DynBitVector& val, CarbonRadix ) const;
  //! The radix is ignored for reals--decimal only
  void printValueByType(UtString*, double val, CarbonRadix ) const;
  void printAsciiValueByType(UtString* buf, const DynBitVector& val ) const;
  //! Asserts--reals can't be printed as ASCII
  void printAsciiValueByType(UtString*buf, const double val ) const;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUConst : public NUExpr

class NUConstNoXZ : public NUConst
{
public:
  //! constructor
  NUConstNoXZ(const DynBitVector& value, UInt32 bitSize, const SourceLocator& loc);

  //! Allows string constants
  NUConstNoXZ(const DynBitVector& value, UInt32 bitSize, bool definedByString, const SourceLocator& loc);

  NUConstNoXZ(UInt32 value, UInt32 bitSize, const SourceLocator& loc);
  //! Constructor from a real
  NUConstNoXZ (CarbonReal r, const SourceLocator& loc);

  //! Copy Constructor
  NUConstNoXZ (const NUConstNoXZ& src);

  //! virtual constructor
  virtual ~NUConstNoXZ();

  //! overload virtual
  UInt32 determineBitSizeHelper () const { return mValue.size (); }

  //! overload virtual
  UInt32 effectiveBitSizeHelper (ExprIntMap*) const;

  //! Returns this
  virtual const NUConst* castConstNoXZ() const;

  // Get the properly signed 2's complement representation.
  virtual void getSignedValue(DynBitVector* value) const;

   virtual bool getULL(UInt64* val) const;
  
  //! puts the 64 bit value into val -- Josh sez this routine is busted for signed \#s.  Use with care!
  virtual bool getLL(SInt64* val) const;

  //! puts 32-bit unsigned value into val
  virtual bool getUL(UInt32* val) const;

  //! puts 32-bit signed value into val
  virtual bool getL(SInt32* val) const;

  //! get CarbonReal
  virtual bool getCarbonReal (CarbonReal*) const {return false;}

  //! Create a copy with a different size and sign
  virtual NUConst* rebuild(bool isSignedResult, UInt32 bitSize) const;

  virtual bool isZero (void) const;

  virtual bool isOnes (void) const;

  virtual bool isNegative (void) const;

  //! Is this a real number?
  virtual bool isReal( void ) const;

  //! Emit C++ Code
  virtual CGContext_t emitCode (CGContext_t c) const;

  //! Returns this class name
  virtual const char* typeStr() const;

  virtual void clearDirtyBits(UInt32 size);

  virtual Type getType () const;

  //! returns true if this constant was defined by a string value.
  virtual bool isDefinedByString() const;

  //! Does constant have X or Z?
  virtual bool hasXZ() const;

  //! Partially driven?
  virtual bool isPartiallyDriven() const;

  virtual void composeHelper (UtString* buf, const STBranchNode* scope,
                                  bool includeRoot, bool HierName, const char * sep) const;

  //! Helper virtual function, which will always get an expr of the same type to cast
  virtual ptrdiff_t compareHelper(const NUExpr* otherExpr,
                                  bool compareLocator,
                                  bool comparePointer,
                                  bool compareSizeOfConsts) const;


protected:
  //! return an expression such that the self-determined size matches the context size
  virtual NUExpr* makeSizeExplicitHelper(UInt32 desired_size);

  //! coerce the non-self determined operands to real
  virtual NUExpr* coerceOperandsToReal( );

  //! copy
  virtual NUExpr* copyHelper(CopyContext &/*copy_context*/) const;
  
  //! value print
  virtual void printVal(UtString* buf, CarbonRadix radix) const;

  //! compose this constant (string of ascii chars) as a string
  virtual void composeAsciiString(UtString* buf) const;

private:
  bool mDefinedByString;        // if true then this constant was defined by a string

protected:
  DynBitVector mValue;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUConstNoXZ : public NUConst

//! Real numbers
class NUConstReal : public NUConstNoXZ
{
public:
  // all the same constructors and methods as parent EXCEPT for these
  virtual bool hasXZ () const;
  virtual bool isReal () const;
  virtual bool getCarbonReal (CarbonReal* val) const;
  NUConstReal (CarbonReal value, const SourceLocator& loc);

  virtual ptrdiff_t compareHelper(const NUExpr* otherExpr,
                                  bool compareLocator,
                                  bool comparePointer,
                                  bool compareSizeOfConsts) const;
  // override - always 64 bits wide
  virtual UInt32 determineBitSizeHelper () const { return 64; }
  // override - never resize a real
  virtual void resizeHelper (UInt32 /*size*/);
  // override - never mess with real's effective size either...
  virtual UInt32 effectiveBitSizeHelper (ExprIntMap*) const { return 64; }

  virtual NUConst* rebuild (bool isSignedResult, UInt32 bitSize) const;


protected:
  virtual NUExpr* makeSizeExplicitHelper(UInt32 desired_size);

  //! this is a no-op for NUConstReal
  virtual NUExpr* coerceOperandsToReal( );

  virtual NUExpr* copyHelper (CopyContext&) const;

  //! value print
  virtual void printVal(UtString* buf, CarbonRadix radix) const;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUConstReal : public NUConstNoXZ

//! Overall class for constants that contain x's and z's
class NUConstXZ : public NUConst
{
public:
  NUConstXZ (const DynBitVector& value, const DynBitVector& drive, UInt32 size, const SourceLocator& loc);

  //! overload
  NUConstXZ (UInt64 value, UInt64 drive, UInt32 size, const SourceLocator& loc);

  //! virtual destructor
  virtual ~NUConstXZ();

  //! overload
  virtual UInt32 determineBitSizeHelper () const;
  //! overload
  virtual UInt32 effectiveBitSizeHelper (ExprIntMap*) const;
  //! Does constant have X or Z?
  virtual bool hasXZ() const;

  //! Returns this;
  virtual const NUConst* castConstXZ() const;

  //! returns true if this constant was defined by a string value.
  virtual bool isDefinedByString() const;

  //! Return the value and the drive
  virtual void getValueDrive(DynBitVector* value, DynBitVector* drive,
                             bool allowUnsized = false) const;

  //! Try to isolate the Z-causing condition, return the enable and driver.
  bool isolateZ(NUExpr **enable, NUExpr **driver) const;

  //! Return just the unsigned value
  virtual void getSignedValue(DynBitVector* value) const;
  
  /*! 
   *!    \param bits Optional bit-vector to receive a 1 for every bit that is driven by Z
   */
  virtual bool drivesZ(DynBitVector* bits = NULL) const;

  //! Return true if this expression only drives a Z value, false otherwise.
  virtual bool drivesOnlyZ() const;

  //! puts the known part of 64 bit value into (unsigned)val
  virtual bool getULL(UInt64* val) const;

  //! puts the known part of 64 bit value into (signed)val
  virtual bool getLL(SInt64* val) const;
  
  //! puts the known part of 32 bit value into (unsigned)val
  virtual bool getUL(UInt32* val) const;

  //! puts the known part of 32 bit value into (signed)val
  virtual bool getL(SInt32* val) const;

  //! No X|Z in reals
  virtual bool getCarbonReal (CarbonReal*) const {return false;}

  //! Create a copy with a different size and sign
  virtual NUConst* rebuild(bool isSignedResult, UInt32 bitSize) const;

  //! returns class name
  virtual const char* typeStr() const;

  //! All zero? This probably should consider ZX and tristate flag?
  virtual bool isZero (void) const;

  //! All ones? 
  virtual bool isOnes (void) const;

  virtual bool isNegative (void) const;

  virtual bool isPartiallyDriven() const;
  
  virtual void clearDirtyBits(UInt32 size);

  NUExpr::Type getType () const;

  virtual ptrdiff_t compareHelper(const NUExpr* otherExpr,
                                  bool compareLocator,
                                  bool comparePointer,
                                  bool compareSizeOfConsts) const;


protected:
  //! return an expression such that the self-determined size matches the context size
  virtual NUExpr* makeSizeExplicitHelper(UInt32 desired_size);

  //! coerce the type of constant to real
  virtual NUExpr* coerceOperandsToReal( );

  //! Print the value and the drive
  virtual void printVal(UtString* buf, CarbonRadix radix) const;

  //! copy
  virtual NUExpr* copyHelper(CopyContext &/*copy_context*/) const;

private:
  //! Numeric value of constant
  DynBitVector mValue;



  //! The known/unknown bit mask
  /*!
    For each bit in mValue there is a corresponding bit in mDrive
    that, if set, means that the corresponding bit in mValue is an
    unknown, and if the bit in mValue is 0 it is a 'z' and if it is a 1
    it is an 'x'.
  */
  DynBitVector mDrive;

  void getDrive(DynBitVector* drive) const;

  // forbid
  NUConstXZ(const NUConstXZ&);
  NUConstXZ& operator=(const NUConstXZ&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUConstXZ : public NUConst

//! NUOp class
/*!
 * Abstract base class for operators.
 */
class NUOp : public NUExpr
{
public:
  //! Operators
  enum OpT
  {
    eStart = 0,             //!< Unused
    eUnBuf,                 //!< Unary buffer pass-through
    eUnPlus,                //!< Unary +
    eUnMinus,               //!< Unary -
    eUnLogNot,              //!< Unary Logical !
    eUnBitNeg,              //!< Unary Bitwise ~
    eUnVhdlNot,             //!< Unary VHDL Bitwise not
    eUnRedAnd,              //!< Unary Reduction &
    eUnRedOr,               //!< Unary Reduction |
    eUnRedXor,              //!< Unary Reduction ^
    eUnCount,		    //!< Population Count
    eUnAbs,                 //!< Absolute value
    eUnFFZ,		    //!< Find First Zero
    eUnFFO,		    //!< Find First One
    eUnFLZ,                 //!< Find Last Zero 
    eUnFLO,		    //!< Find Last One
    eUnRound,               //!< Round real to integer value (away from zero) 
    eUnItoR,                //!< integer to real
    eUnRtoI,                //!< real to integer (by truncation)
    eUnRealtoBits,          //!< real to 64 bits
    eUnBitstoReal,          //!< 64 bits to real
    eUnChange,              //!< Detect if the input expression changed
    eUnStart = eUnBuf,      //!< Start of unary operators
    eUnEnd = eUnChange,	    //!< End of unary operators

    eBiPlus,                //!< Binary +
    eBiMinus,               //!< Binary -
    eBiSMult,               //!< Binary Signed *
    eBiUMult,                //!< Binary Unsigned *
    eBiSDiv,                //!< Binary Signed /
    eBiUDiv,                 //!< Binary Unsigned /
    eBiSMod,                 //!< Binary Unsigned %
    eBiUMod,                 //!< Binary Unsigned %
    eBiVhdlMod,             //!< VHDL modulus
    eFirstRelational,       //!< First relational operator
    eBiEq=eFirstRelational, //!< Binary ==
    eBiNeq,                 //!< Binary !=
    eBiTrieq,               //!< Binary ===
    eBiTrineq,              //!< Binary !==
    eBiLogAnd,              //!< Binary Logical &&
    eBiLogOr,               //!< Binary Logical ||
    eBiSLt,                 //!< Signed <
    eBiSLte,                //!< Signed <=
    eBiSGtr,                //!< Signed >
    eBiSGtre,               //!< Signed >=
    eBiULt,                  //!< Binary <
    eBiULte,                 //!< Binary <=
    eBiUGtr,                 //!< Binary >
    eBiUGtre,                //!< Binary >=
    eLastRelational=eBiUGtre,//!< Last Relational operator
    eBiBitAnd,              //!< Binary Bitwise &
    eBiBitOr,               //!< Binary Bitwise |
    eBiBitXor,              //!< Binary Bitwise ^
    eBiRoR,                 //!< Binary Rotate Right
    eBiRoL,                 //!< Binary Rotate Left
    eBiExp,                 //!< Binary **
    eBiDExp,                //!< Double-precision Binary**
    eBiVhExt,               //!< Binary extension, padding with the sign bit (See Arch Spec)
    eBiVhZxt,               //!< Binary extension, zero-padding              ( "   "    "
    eBiVhLshift,            //!< Binary vhdl shift left logical, 
                            //!< unlike eBiLshift, it can take negative count
    eBiVhRshift,            //!< Binary vhdl shift right logical
                            //!< unlike eBiRshift, it can take negative count
    eBiVhLshiftArith,       //!< Binary shift left arith (VHDL)
    eBiVhRshiftArith,       //!< Binary shift right arith (VHDL)
    eBiRshiftArith,         //!< Binary shift right arith (Verilog)
    eBiLshiftArith,         //!< Binary shift left arith (Verilog)
    eBiRshift,              //!< Binary >>
    eBiLshift,              //!< Binary <<
    eBiDownTo,              //!< Binary VHDL 'downto'
    eBiStart = eBiPlus,     //!< Start of binary operators
    eBiEnd = eBiDownTo,     //!< End of binary operators

    eTeCond,                //!< Ternary ?:
    eTeStart = eTeCond,     //!< Start of ternary operators
    eTeEnd = eTeCond,       //!< End of ternary operators

    eNaConcat,              //!< N-ary {}
    eNaStart = eNaConcat,   //!< Start of n-ary operators
    eNaFopen,               //!< verilog f open("filename") or f open("filename",type)
    eNaLut,                 //!< Lookup table
    eNaEnd = eNaLut,        //!< End of n-ary operators

    eZEndFile,              //!< ENDFILE
    eZSysTime,              //!< $time
    eZSysStime,             //!< $stime
    eZSysRealTime,          //!< $realtime

    eInvalid                //!< Bad operator
  };

  //! Is this operator a relational operator
  static bool isRelationalOperator (OpT op);

  //! Is this operator one that treats *both* operands as signed values
  /*!
   *! Beware that this function returns false for eBiRshiftArith and
   *! eBiVhRshiftArith, but the lop should be sign-extended in those.
   *! Not the rop though.
   *!
   *! Note that a 'true' result from this function does not imply that
   *! the result is signed.  For that, \sa isSignedResult().
   */
  static bool isSignedOperator (OpT op);

  //! constructor
  /*!
    \param op The operator
    \param loc Source location.
   */
  NUOp(OpT op, const SourceLocator& loc);

  //! Return the operator
  OpT getOp() const { return mOp; }

  //! Return if the operator is a boolean relational operator
  static bool isRelational (OpT op) { return (op >= eFirstRelational) && (op <= eLastRelational); }

  //! Return the set of nets this node reads from
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  void getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const;
  void getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;
  
  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net) = 0;

  //! Emit C++ Code
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Return true if the given operator is a unary operator.
  static bool isUnaryOp(OpT op)
  {
    return ((op >= eUnStart) && (op <= eUnEnd));
  }

  //! Return true if the given operator is a binary operator.
  static bool isBinaryOp(OpT op)
  {
    return ((op >= eBiStart) && (op <= eBiEnd));
  }

  //! Return true if the given operator is a ternary operator.
  static bool isTernaryOp(OpT op)
  {
    return ((op >= eTeStart) && (op <= eTeEnd));
  }

  //! Return true if the given operator is an n-ary operator.
  static bool isNaryOp(OpT op)
  {
    return ((op >= eNaStart) && (op <= eNaEnd));
  }

  //! Determine if two expression trees represent the same function.
  virtual ptrdiff_t compareHelper(const NUExpr* otherExpr, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const;

  //! Dump myself to cout
  void print(bool recurse = true, int indent = 0) const;

  //! Return class name
  virtual const char* typeStr() const = 0;

  //! Return the char* string for this operator, by default return c-style strings (vs verilog style)
  const char* getOpChar(bool cStyle = true) const;

  //! Return the string for \a op (a specific operator, not necessarily for 'this')
  /*
    \param op the opcode you need the string for
    \param cStyle if true then a c operator, if false a verilog operator
   */
  static const char* convertOpToChar(const OpT op, bool cStyle = true);

  //! destructor
  virtual ~NUOp();

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

protected:
  //! shallow equality comparison -- used for NUExprFactory
  virtual bool shallowEqHelper(const NUExpr & otherExpr) const;

  OpT mOp;


private:
  //! Hide copy and assign constructors.
  NUOp(const NUOp&);
  NUOp& operator=(const NUOp&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUOp : public NUExpr


//! NUUnaryOp class
/*!
 * Unary operation.
 */
class NUUnaryOp : public NUOp
{
public:
  //! constructor
  /*!
    \param op The operator
    \param expr Expression argument
    \param loc Source location.
   */
  NUUnaryOp(OpT op,
	    NUExpr *expr,
	    const SourceLocator& loc);

  //! constructor based on managed ops
  /*!
    \param op The operator
    \param expr Expression argument
    \param loc Source location.
   */
  NUUnaryOp(OpT op,
	    const NUExpr *expr,
	    const SourceLocator& loc);

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUUnaryOp; }

  //! Return number of arguments to this operator.
  virtual UInt32 getNumArgs() const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name upto and including specified scope,
           if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  //! Return class name
  const char* typeStr() const;

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! How many bits of effective value are there?
  virtual UInt32 effectiveBitSizeHelper(ExprIntMap* ebit_cache) const;

  //! Return true if the expression will evaluate to a real number
  virtual bool isReal() const;

  //! hash an expression
  virtual size_t hash() const;

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const;

  //! Does this unary operator implement a size-preserving bit-flip of a full identifier?
  virtual bool isIdentifierBitFlip() const;

  //! destructor
  ~NUUnaryOp();

  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  BDD bdd(BDDContext *ctx, STBranchNode *scope=0) const;

  //! Can we build a BDD for this expression?
  /*!
    Determines whether we can build a valid BDD based on pre-assigned
    leaves.  It does this without using the BDD package, so it's good
    to call this on expressions before consuming BDD space.

    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return true if we can generate a useful BDD
   */
  virtual bool hasBdd(BDDContext *ctx, STBranchNode *scope) const;

  //! Codegen
  virtual CGContext_t emitCode (CGContext_t) const;

  //! manage this expression and all sub-expressions in a factory
  virtual void manage(NUExprFactory*);


protected:
  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual const NUExpr *getArgHelper(UInt32 index) const;

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual NUExpr* putArgHelper(UInt32 index, NUExpr *);

  //! Transform this expression into one whose determineBitSize is desired_size
  virtual NUExpr* makeSizeExplicitHelper(UInt32 desired_size);

  //! coerce the non-self determined operands to real
  virtual NUExpr* coerceOperandsToReal( );

  //! Resize this expression to be the given size.
  void resizeHelper(UInt32 size);

  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;

private:
  //! Hide copy and assign constructors.
  NUUnaryOp(const NUUnaryOp&);
  NUUnaryOp& operator=(const NUUnaryOp&);

  //! The subexpression.
  NUExpr *mExpr;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUUnaryOp : public NUOp


//! NUBinaryOp class
/*!
 * Binary operation.
 */
class NUBinaryOp : public NUOp
{
public:
  //! constructor
  /*!
    \param op The operator
    \param expr1 First expression argument
    \param expr2 Second expression argument
    \param loc Source location.
   */
  NUBinaryOp(OpT op,
	     NUExpr *expr1,
	     NUExpr *expr2,
	     const SourceLocator& loc);

  //! constructor based on managed operators
  /*!
    \param op The operator
    \param expr1 First expression argument
    \param expr2 Second expression argument
    \param loc Source location.
   */
  NUBinaryOp(OpT op,
	     const NUExpr *expr1,
	     const NUExpr *expr2,
	     const SourceLocator& loc);

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUBinaryOp; }

  //! Return number of arguments to this operator.
  virtual UInt32 getNumArgs() const;

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! Resize this expression to be the given size.
  void resizeHelper(UInt32 size);

  //! How many bits of effective value are there?
  virtual UInt32 effectiveBitSizeHelper(ExprIntMap* ebit_cache) const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name upto and including specified scope,
           if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  //! Return class name
  const char* typeStr() const;

  //! Return true if the op evaluates to a real number
  virtual bool isReal() const;

  //! hash an expression
  virtual size_t hash() const;

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const;

  //! destructor
  ~NUBinaryOp();

  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  BDD bdd(BDDContext *ctx, STBranchNode *scope=0) const;

  //! Can we build a BDD for this expression?
  /*!
    Determines whether we can build a valid BDD based on pre-assigned
    leaves.  It does this without using the BDD package, so it's good
    to call this on expressions before consuming BDD space.

    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return true if we can generate a useful BDD
   */
  virtual bool hasBdd(BDDContext *ctx, STBranchNode *scope) const;

  //! Codegen
  virtual CGContext_t emitCode (CGContext_t) const;

  //! manage this expression and all sub-expressions in a factory
  virtual void manage(NUExprFactory*);

protected:
  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual const NUExpr *getArgHelper(UInt32 index) const;

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual NUExpr* putArgHelper(UInt32 index, NUExpr *);


  //! Transform this expression into one whose determineBitSize is desired_size
  virtual NUExpr* makeSizeExplicitHelper(UInt32 desired_size);

  //! coerce the non-self determined operands to real
  virtual NUExpr* coerceOperandsToReal( );

  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;


private:
  //! Hide copy and assign constructors.
  NUBinaryOp(const NUBinaryOp&);
  NUBinaryOp& operator=(const NUBinaryOp&);

  //! The left subexpression.
  NUExpr *mExpr1;

  //! The right subexpression.
  NUExpr *mExpr2;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUBinaryOp : public NUOp


//! NUTernaryOp class
/*!
 * Ternary operation.
 */
class NUTernaryOp : public NUOp
{
public:
  //! constructor
  /*!
    \param op The operator
    \param expr1 First expression argument
    \param expr2 Second expression argument
    \param expr3 Third expression argument
    \param loc Source location.
   */
  NUTernaryOp(OpT op,
	      NUExpr *expr1,
	      NUExpr *expr2,
	      NUExpr *expr3,
	      const SourceLocator& loc);

  //! constructor for managed operands
  /*!
    \param op The operator
    \param expr1 First expression argument
    \param expr2 Second expression argument
    \param expr3 Third expression argument
    \param loc Source location.
   */
  NUTernaryOp(OpT op,
	      const NUExpr *expr1,
	      const NUExpr *expr2,
	      const NUExpr *expr3,
	      const SourceLocator& loc);

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUTernaryOp; }

  //! Return number of arguments to this operator.
  virtual UInt32 getNumArgs() const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name upto and including specified scope,
           if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  //! Return class name
  const char* typeStr() const;

  //! Return true if the expression will evaluate to a real number
  virtual bool isReal() const;

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! Resize this expression to be the given size.
  void resizeHelper(UInt32 size);

  //! How many bits of effective value are there?
  virtual UInt32 effectiveBitSizeHelper(ExprIntMap* ebit_cache) const;

  //! hash an expression
  virtual size_t hash() const;

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const;

  //! destructor
  ~NUTernaryOp();

  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  BDD bdd(BDDContext *ctx, STBranchNode *scope=0) const;

  //! Can we build a BDD for this expression?
  /*!
    Determines whether we can build a valid BDD based on pre-assigned
    leaves.  It does this without using the BDD package, so it's good
    to call this on expressions before consuming BDD space.

    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return true if we can generate a useful BDD
   */
  virtual bool hasBdd(BDDContext *ctx, STBranchNode *scope) const;

  //! Try to isolate the Z-causing condition, return the enable and driver.
  bool isolateZ(NUExpr **enable, NUExpr **driver) const;

  //! Return true if this expression can drive a Z value, false otherwise.
  virtual bool drivesZ(DynBitVector* zbits = NULL) const;

  //! Return true if this expression can only drive a Z value, false otherwise.
  virtual bool drivesOnlyZ() const;

  //! CodeGen
  virtual CGContext_t emitCode (CGContext_t) const;

  //! manage this expression and all sub-expressions in a factory
  virtual void manage(NUExprFactory*);


  //! Return true if this expression ought to preserve any Z values found
  //! on the specified net. 
  /*!
   *! Note that this does not imply (at this time) that
   *! Carbon will preserve the Z properly -- only that Verilog
   *! semantics require it.  This routine is for use in Nucleus
   *! transformation code, or in code that warns about potential
   *! mismatches with simulation
   */
  virtual bool shouldPreserveZ(NUNet*) const;


protected:
  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual const NUExpr *getArgHelper(UInt32 index) const;

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual NUExpr* putArgHelper(UInt32 index, NUExpr *);

  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;

  //! Transform this expression into one whose determineBitSize is desired_size
  virtual NUExpr* makeSizeExplicitHelper(UInt32 desired_size);

  //! coerce the non-self determined operands to real
  virtual NUExpr* coerceOperandsToReal( );

private:
  //! Hide copy and assign constructors.
  NUTernaryOp(const NUTernaryOp&);
  NUTernaryOp& operator=(const NUTernaryOp&);

  //! The left subexpression.
  NUExpr *mExpr1;

  //! The middle subexpression.
  NUExpr *mExpr2;

  //! The right subexpression.
  NUExpr *mExpr3;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS ;//class NUTernaryOp : public NUOp


//! NUNaryOp class
/*!
 * N-ary operation, this is an abstract class.
 */
class NUNaryOp : public NUOp
{
public:
  //! constructor
  /*!
    \param op The operator
    \param exprs The expressions, the vector will be copied.  0 is leftmost.
    \param loc Source location.
   */
  NUNaryOp(OpT op,
	   const NUExprVector& exprs,
	   const SourceLocator& loc);

  //! constructor for managed operands
  /*!
    \param op The operator
    \param exprs The expressions, the vector will be copied.  0 is leftmost.
    \param loc Source location.
   */
  NUNaryOp(OpT op,
	   const NUCExprVector& exprs,
	   const SourceLocator& loc);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Return number of arguments to this operator.
  virtual UInt32 getNumArgs() const;

  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  BDD bdd(BDDContext *ctx, STBranchNode *scope=0) const;

  //! hash an expression
  virtual size_t hash() const;

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const;

  //! destructor
  ~NUNaryOp();

  //! manage this expression and all sub-expressions in a factory
  virtual void manage(NUExprFactory*);

  //! loop thru all the expressions
  NUExprCLoop loopExprs () const {return NUExprCLoop (mExprs); }

  //! overload
  NUExprLoop loopExprs () {return NUExprLoop (mExprs); }


protected:
  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual const NUExpr *getArgHelper(UInt32 index) const;

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual NUExpr* putArgHelper(UInt32 index, NUExpr *);

  //! constructor for use by derived classes only
  NUNaryOp(OpT op, const SourceLocator &loc);

  //! Vector of expressions, 0 is leftmost.
  NUExprVector mExprs;

private:
  //! Hide copy and assign constructors.
  NUNaryOp(const NUNaryOp&);
  NUNaryOp& operator=(const NUNaryOp&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS ; //class NUNaryOp : public NUOp


//! NUConcatOp class
/*!
 * Concatenation operation.
 */
class NUConcatOp : public NUNaryOp
{
public:
  //! constructor
  /*!
    \param exprs The expressions, the vector will be copied.  0 is leftmost.
    \param repeat_count The number of times to repeat the expression.  Must be at least 1.
    \param loc Source location.
   */
  NUConcatOp(const NUExprVector& exprs,
	     UInt32 repeat_count,
	     const SourceLocator& loc);

  //! constructor for managed operands
  /*!
    \param exprs The expressions, the vector will be copied.  0 is leftmost.
    \param repeat_count The number of times to repeat the expression.  Must be at least 1.
    \param loc Source location.
   */
  NUConcatOp(const NUCExprVector& exprs,
	     UInt32 repeat_count,
	     const SourceLocator& loc);

  //! constructor for simple duplication of an expression
  /*!
    \param expr The expression to be used, (NOTE no copy is made of this expression)
    \param repeat_count The number of times to repeat the expression.  Must be at least 1.
    \param loc Source location.
   */
  NUConcatOp(NUExpr* expr,
	     UInt32 repeat_count,
	     const SourceLocator& loc);


  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUConcatOp; }

  //! Return the number of times to repeat the expression, is at least 1.
  UInt32 getRepeatCount() const { return mRepeatCount; }

  //! Change the repeat count
  void putRepeatCount (UInt32 count);

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! How many bits of effective value are there?
  virtual UInt32 effectiveBitSizeHelper(ExprIntMap* ebit_cache) const;

  //! Return true if this expression access a single, complete, identifier, false otherwise.
  bool isWholeIdentifier() const;

  //! Return true if this is a simple expression accessing a hierref
  /*! Returns true if this is a whole identifier and it is a hier ref
   */
  virtual bool isHierRef() const;

  //! Get the NUNet that is on the right hand side.
  /*!
   * This call is only valid if you know that you have a whole identifier
   */
  NUNet* getWholeIdentifier() const;

  //! Construct LHS matching RHS, if possible
  virtual NULvalue* Lvalue(const SourceLocator &loc) const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name upto and including specified scope, if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  //! Return class name
  const char* typeStr() const;

  //! Emit C++ Code
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Equivalence
  virtual ptrdiff_t compareHelper(const NUExpr*, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const;

  //! Dump myself to cout
  void print(bool recurse = true, int indent = 0) const;

  //! hash an expression
  virtual size_t hash() const;

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const;

  //! Return true if this expression can drive a Z value, false otherwise.
  bool drivesZ(DynBitVector* zbits = NULL) const;

  //! Return true if this expression only drives a Z value, false otherwise.
  bool drivesOnlyZ() const;

  //! destructor
  ~NUConcatOp();

  //! Concat contains variable-sized members, viz.   x(j downto 0) + x(10 downto j)
  bool sizeVaries () const;


  //! Return true if this expression ought to preserve any Z values found
  //! on the specified net. 
  /*!
   *! Note that this does not imply (at this time) that
   *! Carbon will preserve the Z properly -- only that Verilog
   *! semantics require it.  This routine is for use in Nucleus
   *! transformation code, or in code that warns about potential
   *! mismatches with simulation
   */
  virtual bool shouldPreserveZ(NUNet*) const;

  //! copy a concat without without upcasting
  NUConcatOp* copyConcatOp(CopyContext& cc) const;


protected:
  //! Transform this expression into one whose determineBitSize is desired_size
  virtual NUExpr* makeSizeExplicitHelper(UInt32 desired_size);

  //! for a concat this is a no-op
  virtual NUExpr* coerceOperandsToReal( );

  //! shallow equality comparison -- used for NUExprFactory
  virtual bool shallowEqHelper(const NUExpr&) const;

  //! Resize this expression to be the given size.
  void resizeHelper(UInt32 size);

  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;

private:
  //! Hide copy and assign constructors.
  NUConcatOp(const NUConcatOp&);
  NUConcatOp& operator=(const NUConcatOp&);

  //! Number of repetitions.
  UInt32 mRepeatCount;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUConcatOp : public NUNaryOp


//! NUNullExpr class
/*!
 * a class to represent the empty expression, created by double
 * commas in an expression list (but not a port list).
 */
class NUNullExpr : public NUExpr
{
public:
  //! constructor
  /*!
    \param loc Source location.
   */
  NUNullExpr(const SourceLocator& loc);

  //! destructor
  ~NUNullExpr();

  //! Function to return the expression type
  virtual Type getType() const;

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * /* old_net -- unused*/ ,
               NUNet * /* new_net --unused */);

  //! Replace all net references based on a translation functor.
  bool replaceLeaves(NuToNuFn & /* translator --unused*/);

  // no cost for NUNullExpr
  void calcCost(NUCost*, NUCostContext*) const;

  //! Return the set of nets this node reads from, always empty
  void getUses(NUNetSet * /*uses -- unused */) const;
  void getUses(NUNetRefSet * /* uses -- unused */) const;
  void getUsesElab(STBranchNode* /* scope -- unused */, NUNetElabSet * /* uses -- unused */) const;
  void getUsesElab(STBranchNode* /* scope -- unused */, NUNetElabRefSet2 * /* uses -- unused */) const;
  bool queryUses(const NUNetRefHdl &/* use_net_ref -- unused */,
                 NUNetRefCompareFunction /* fn -- unused */,
                 NUNetRefFactory * /* factory -- unused */) const;

  //! return true if both are NUNullExpr since any two are equivalent.
  ptrdiff_t compareHelper(const NUExpr* otherExpr, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const;

  //! Generate a BDD for this expression (TBD)
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  BDD bdd(BDDContext * ctx, STBranchNode * /* scope -- unused*/) const ;

  //! Dump myself to cout
  void print(bool recurse = true, int indent = 0) const;

  //! Compose for a NUNullExpr generates a single space
  void composeHelper(UtString* buf, const STBranchNode* /* scope --unused */,
                     bool /* includeRoot --unused */,
                     bool /* hierName --unused */,
                     const char* /* separator --unused */ ) const;

  //! Return class name
  const char* typeStr() const;

  //! Emit C++ Code 
  CGContext_t emitCode (CGContext_t) const;

  //! hash an expression
  size_t hash() const;

  //! shallow-hash an expression -- used for NUExprFactory
  size_t shallowHash() const;


protected:
  //! Resize this expression to be the given size.  This is a no-op
  void resizeHelper(UInt32 /* size -- unused */);

  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;

private:
  //! Hide copy and assign constructors.
  NUNullExpr(const NUNullExpr&);
  NUNullExpr& operator=(const NUNullExpr&);

  //! unique index for this null expr
  UInt32 mIndex;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUNullExpr : public NUExpr

//! NUChangeDetect class
/*! Detects changes on an input expression. This is implemented with a
 *  class because we need to store a temporary net to hold the old
 *  value.
 *
 *  Note that the temp name should not be set until codegen. This is
 *  so that we don't have to deal with managing the temp names. The
 *  copy routine will assert if it is called after the temp name has
 *  been set.
 */
class NUChangeDetect : public NUUnaryOp
{
public:
  //! constructor
  //! constructor
  /*!
    \param expr Expression argument
   */
  NUChangeDetect(NUExpr *expr);

  //! constructor based on managed ops
  /*!
    \param expr Expression argument
   */
  NUChangeDetect(const NUExpr *expr);

  //! destructor
  ~NUChangeDetect();

  //! Codegen
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Set the temp net to store the old value
  /*! Set the temp net. This temp net stores the old value of the
   *  expression so that change detection can be done. This is not
   *  populated until code generation to avoid issues with
   *  optimizations moving these change detects around.
   *
   *  Note: this routine can only be called once on each unique change
   *        detection expression.
   *
   *  \param tempNet - a pointer to a module level temporary net
   *
   */
  void putTempNet(NUNet* tempNet);

  //! Get the temp net to store the old value
  /*! Get the temp net. This temp net stores the old value of the
   *  expression so that change detection can be done. This is not
   *  populated until code generation to avoid issues with
   *  optimizations moving these change detects around. Calling it
   *  earlier will just return NULL.
   */
  NUNet* getTempNet() const { return mTempNet; }

  //! Dump myself to cout
  void print(bool recurse = true, int indent = 0) const;


protected:
  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;

private:
  //! Hide copy and assign constructors.
  NUChangeDetect(const NUChangeDetect&);
  NUChangeDetect& operator=(const NUChangeDetect&);

  // Temporary net. This is not populated until code generation to
  // avoid issues with optimizations moving these change detects
  // around.
  NUNet* mTempNet;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUChangeDetect : public NUUnaryOp


//! NULut Class
/*!
 * Look up table, fully populated, with constant outputs.
 *
 * This is considered an n-ary op:  arg(0) is the select, and the rest of
 * the args are the constant rows in the index order (0 ... numTableRows() - 1).
 */
class NULut : public NUNaryOp
{
public:
  //! constructor
  /*!
    \param expr Expression argument
    \param table Constant output entries per input expression value.
    \param loc Source location.
   */
  NULut(NUExpr *expr,
        const NUConstVector &table,
        const SourceLocator& loc);

  //! constructor based on managed ops
  /*!
    \param expr Expression argument
    \param table Constant output entries per input expression value.
    \param loc Source location.
   */
  NULut(const NUExpr *expr,
        const NUCConstVector &table,
        const SourceLocator& loc);

  //! destructor
  ~NULut();

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! How many bits of effective value are there?
  virtual UInt32 effectiveBitSizeHelper(ExprIntMap* ebit_cache) const;

  //! Return the number of rows in the table.
  UInt32 numTableRows() const;

  //! Set the table name.
  /*!
   * Name is only used by codegen, so codegen is where this is
   * populated.
   */
  void putTableName(StringAtom *name);

  //! Return the table name.
  StringAtom* getTableName() const;

  //! Function to return the expression type
  Type getType() const;

  //! Return class name
  const char* typeStr() const;

  //! Codegen
  CGContext_t emitCode (CGContext_t) const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name upto and including specified scope,
           if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  //! Can we build a BDD for this expression?
  /*!
    Determines whether we can build a valid BDD based on pre-assigned
    leaves.  It does this without using the BDD package, so it's good
    to call this on expressions before consuming BDD space.

    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return true if we can generate a useful BDD
   */
  virtual bool hasBdd(BDDContext *ctx, STBranchNode *scope) const;

  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  BDD bdd(BDDContext *ctx, STBranchNode *scope=0) const;


protected:

  //! Transform this expression into one whose determineBitSize is desired_size
  virtual NUExpr* makeSizeExplicitHelper(UInt32 desired_size);

  //! coerce the non self-determined operands to real
  virtual NUExpr* coerceOperandsToReal( );

  //! Resize this expression to be the given size.
  /*!
   * After this is called, getBitSize() will return this value.
   */
  void resizeHelper(UInt32 size);

  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;

private:
  //! Hide copy and assign constructors.
  NULut(const NULut&);
  NULut& operator=(const NULut&);

  //! Table name.
  StringAtom *mName;

  DECLARENUCLEUSCLASSFRIENDS();

} APIOMITTEDCLASS; //class NULut : public NUNaryOp

#endif
