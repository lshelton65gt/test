// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NUNET_H_
#define NUNET_H_

#include "util/SourceLocator.h"
#include "flow/Flow.h"
#include "util/ConstantRange.h"
#include "nucleus/NUElabBase.h"
#include "nucleus/NUHierRef.h"
#include "symtab/STAliasedLeafNode.h"
#include "flow/FLNodeElabSet.h"
#include "flow/FLNodeSet.h"
#include "util/UtHashSet.h"
#include "iodb/IODBNucleus.h"

class CGAuxInfo;		// Forward declare.
class STSymbolTable;
class StringAtom;
class STAliasedLeafNode;
class STBranchNode;
class IODBNucleus;
class CarbonIdent;

// Currently it looks like it's a performance win to store netrefs for
// NUBitNets directly in the NUBitNet rather than mapping them in the
// factory.  But I am still experimenting with this so I want to leave
// the #if in.
#define NUBITNETREF_STORED_IN_NUBITNET 1

/*!
  \file
  Declaration of the Nucleus net class.
*/

//! NUNet class
/*!
 * Abstract base class.
 * A net represents a connection; may be a port, reg, wire, temporary, etc.
 */
class NUNet : public NUBase
{
public:
  //! constructor
  /*!
    \param name Name of the net
    \param flags Attribute flags
    \param scope Scope in which net is declared
    \param loc Source location of declaration of the net
   */
  NUNet(StringAtom* name,
        NetFlags flags,
        NUScope* scope,
        const SourceLocator& loc);

  //! Constructor
  /*!
    \param name_leaf Name of the net from the module's symbol table.
    \param flags Attribute flags
    \param scope Scope in which net is declared
    \param loc Source location of declaration of the net
   */
  NUNet(STAliasedLeafNode * name_leaf,
        NetFlags flags,
        NUScope* scope,
        const SourceLocator& loc);

  //! potentially normalize \a expr the range select \a toRange to match the \a fromRange. (helper for select expressions on nets)
  static NUExpr* sNormalizeExpr(NUExpr* expr, const ConstantRange& fromRange, const ConstantRange& toRange, SInt32 adjust, bool retNullForOutOfRange);


  //! Return the declared bit size
  virtual UInt32 getBitSize() const = 0;

  //! Get the number of bits this memory net needs in a netref
  //! (depth for memories, width for vectors)
  virtual UInt32 getNetRefWidth() const = 0;

  //! Return the name
  StringAtom* getName() const APIDISTILLMACRO1(APIEMITCODE(return const_cast<StringAtom*>(mNameLeaf->strObject()) ));

  //! Change the name
  /*!
   * Only supported early in the compile.  Will assert if
   * this net has aliases.
   */
  void changeName(StringAtom *name);

  //! Return the symbol table entry for the name.
  STAliasedLeafNode * getNameLeaf() const { return mNameLeaf; }

  //! Return the master symbol table entry for the name.
  STAliasedLeafNode * getMasterNameLeaf() const;

  //! Return the master net.
  NUNet * getMaster() const;

  //! Return the storage symbol table entry for the name.
  STAliasedLeafNode * getStorageNameLeaf() const;

  //! Return the storage net.
  NUNet * getStorage() const;

  //! Remember that CarbonIdent is associated with this unelaborated net.
  void putIdent(CarbonIdent * ident);

  //! Return the CarbonIdent associated with this unelaborated net.
  CarbonIdent * getIdent() const;

  //! Alias this net (as master) with another.
  void alias(NUNet * other);

  //! Transfer aliases of other onto this (excluding other).
  /*!
   * This is used by assignment aliasing when we don't want to keep other
   * (because it's a temp), but we do want to keep all the names associated with other.
   * So, this allows us to keep all the names of other but still delete other.
   */
  void transferAliases(NUNet *other);

  //! Is this net observable or mutable outside its declaration scope?
  /*!
   * Hierarchical references and user directives cause a net to be
   * protected.
   *
   * \sa isProtectedObservable, isProtectedMutable
   */
  bool isProtected(const IODBNucleus* iodb) const {
    return (isProtectedObservable(iodb) or isProtectedMutable(iodb));
  }

  //! Is this net observable outside its declaration scope?
  /*!
   * Hierarchical references and user directives cause a net to be
   * protected.
   *
   * \sa IODBNucleus::isProtectedObservable
   */
  virtual bool isProtectedObservable(const IODBNucleus* iodb) const;

  //! Is this net protected mutable due to something other than hierarchical references?
  /*!
   * This returns true for subset of when isProtectedObservable
   * returns true, and should only be used when you specifically don't
   * want hierrefs to make a net protected.
   *
   * \returns true if this net is protected observable due to
   *     something other than hierarchical references (user
   *     directives, etc).
   *
   * \sa isProtectedObservable
   */
  virtual bool isProtectedObservableNonHierref(const IODBNucleus* iodb) const;

  //! Is this net written outside its declaration scope?
  /*!
   * Hierarchical references and user directives cause a net to be
   * protected.
   *
   * \sa IODBNucleus::isProtectedMutable
   */
  virtual bool isProtectedMutable(const IODBNucleus* iodb) const;

  //! Is this net protected mutable due to something other than hierarchical references?
  /*!
   * This returns true for subset of when isProtectedMutable returns
   * true, and should only be used when you specifically don't want
   * hierrefs to make a net protected.
   *
   * \returns true if this net is protected mutable due to something
   *     other than hierarchical references (user directives, etc).
   *
   * \sa isProtectedMutable
   */
  virtual bool isProtectedMutableNonHierref(const IODBNucleus* iodb,
                                            bool checkPrimary = true) const;

  //! Mark a net a protected observable (not due to hierrefs).
  virtual void putIsProtectedObservableNonHierref(IODBNucleus *iodb);

  //! Remove the net from the list of protected observable nets
  virtual void removeIsProtectedObservableNonHierref(IODBNucleus *iodb);

  //! Mark a net a protected mutable (not due to hierrefs).
  virtual void putIsProtectedMutableNonHierref(IODBNucleus *iodb);

  //! Remove the net from the list of protected mutable nets
  virtual void removeIsProtectedMutableNonHierref(IODBNucleus *iodb);

  //! Mark a net depositable within the IODB.
  virtual void putIsDepositableIODB(IODBNucleus *iodb);

  //! Return the source location
  const SourceLocator& getLoc() const { return mLoc; }

  //! Datatype to abstract looping over driver flow nodes
  typedef FLNodeSet::UnsortedLoop DriverLoop;

  //! Datatype to abstract looping over driver flow nodes
  typedef FLNodeSet::UnsortedCLoop ConstDriverLoop;

  //! Loop over the continuous driver flow nodes
  /*!
   * Not guaranteed to have one; only driven nets have these, and then only
   * if driven continuously.
   */
  DriverLoop loopContinuousDrivers() {
    return mContinuousDrivers.loopUnsorted();
  }
  //! Loop over the continuous driver flow nodes -- const version
  ConstDriverLoop loopContinuousDrivers() const {
    return mContinuousDrivers.loopCUnsorted();
  }

  //! Return the single driver for this net; isMultiplyDriven() must be false
  FLNode* getContinuousDriver() const;

  //! Replace the scope (moved named blocks)
  void replaceScope(NUScope* oldScope, NUScope* newScope);

  //! Is this flow a continuous driver?
  /*!
   * Returns true if \param possibleDriver is a continuous driver of 'this'
   */
  bool hasContinuousDriver(FLNode *possibleDriver) const;

  //! Is this net continuously driven by a flow node with overlapping type?
  bool hasContinuousDriver(FLTypeT flow_type) const;

  //! Return true if this net has multiple continuous drivers
  bool isMultiplyDriven() const;

  //! Return true if this net has at least one continuous driver
  inline bool isContinuouslyDriven() const {
    return (not mContinuousDrivers.empty());
  }

  //! Add a continuous driver flow node for this net
  void addContinuousDriver(FLNode *driver);

  //! Replace all the continuous drivers with the given single driver
  void replaceContinuousDrivers(FLNode *driver);

  //! Remove the given node, if it is a continuous driver.
  void removeContinuousDriver(FLNode *driver);

  //! Remove all continuous drivers associated with this net.
  void removeContinuousDrivers();

  //! Trim excess capacity from the driver array
  void trimExcessCapacity();

  //! Return the scope in which the net is declared
  NUScope* getScope() const { return mScope; }

  //! Typedef for any of the "is..." functions
  typedef bool (NUNet::* QueryFunction)() const;

  //! Return true if this net is an input
  bool isInput() const { return ((mFlags & ePortMask) == eInputNet); }

  //! Return true if this net is an output
  bool isOutput() const { return ((mFlags & ePortMask) == eOutputNet); }

  //! Return true if this net is a bid
  bool isBid() const { return ((mFlags & ePortMask) == eBidNet); }

  //! Return true if this net is an input, output, or bid
  bool isPort() const { return ((mFlags & ePortMask) != 0); }

  //! Return true if this net is a flop output
  /*! Note that the flop flag is not propagated to all its aliases so
   *  make sure the right unelaborated net is tested. (The one that is
   *  driven by an always block). To do a flop test on an elaborated
   *  construct see LFElabStateTest.
   */
  bool isFlop() const { return ((mFlags & eFlopNet) != 0); }

  //! Return true if this net is a primary input
  bool isPrimaryInput() const { return (isPrimary() and isInput()); }

  //! Return true if this net is a primary output
  bool isPrimaryOutput() const { return (isPrimary() and isOutput()); }

  //! Return true if this net is a primary bid
  bool isPrimaryBid() const { return (isPrimary() and isBid()); }

  //! Return true if this net is a primary input, output, or bid
  bool isPrimaryPort() const { return (isPrimary() and isPort()); }

  //! Alias for atTopLevel
  bool isPrimary() const { return atTopLevel(); }

  //! Return true if this net is a temporary created by the compiler
  bool isTemp() const { return (mFlags & eTempNet) != 0; }

  //! Set original name of the net
  void setOriginalName(StringAtom* name)  APIDISTILLMACRO1(APIEMITCODE(mOrigName = name));

  //! Return original name of the net
  StringAtom* getOriginalName()  APIDISTILLMACRO1(APIEMITCODE(return mOrigName));

  //! Return true if this net's name was generated by the compiler
  bool isGensym() const;

  // ! Return true if this net is implicitly declared
  //bool isImplicit() const { return NetIsDeclaredAs(mFlags, eDMImplicitNet); }

  //! Returns true if isWor() or isTrior()
  bool isWiredOr() const { return isWor() || isTrior(); }

  //! Returns true if isWand() or isTriand()
  bool isWiredAnd() const { return isWand() || isTriand(); }
  
  //! Return true if isWor(), isWand(), isTriOr(), or isTriand()
  bool isWiredNet() const { return isWiredOr() || isWiredAnd(); }


  //! Return true if this net is declared as a wire
  bool isWire() const { return NetIsDeclaredAs(mFlags, eDMWireNet); }

  //! Return true if this net is declared as a tri
  bool isTri() const { return NetIsDeclaredAs(mFlags, eDMTriNet); }

  //! Return true if this net is declared as a type which defaults to 1.
  bool isDefault1() const { return (isTri1() or isPullUp() or isWand() or isTriand()); }

  //! Return true if this net is declared as a type which defaults to 0.
  bool isDefault0() const { return (isTri0() or isPullDown() or isWor() or isTrior()); }

  //! Return true if this net is declared as a tri1
  bool isTri1() const { return NetIsDeclaredAs(mFlags, eDMTri1Net); }

  //! Return true if this net is declared as a supply0
  bool isSupply0() const { return NetIsDeclaredAs(mFlags, eDMSupply0Net); }

  //! Return true if this net is declared as a wand
  bool isWand() const { return NetIsDeclaredAs(mFlags, eDMWandNet); }

  //! Return true if this net is declared as a triand
  bool isTriand() const { return NetIsDeclaredAs(mFlags, eDMTriandNet); }

  //! Return true if this net is declared as a tri0
  bool isTri0() const { return NetIsDeclaredAs(mFlags, eDMTri0Net); }

  //! Return true if this net is declared as a supply1
  bool isSupply1() const { return NetIsDeclaredAs(mFlags, eDMSupply1Net); }

  //! Return true if this net is declared as a wor
  bool isWor() const { return NetIsDeclaredAs(mFlags, eDMWorNet); }

  //! Return true if this net is declared as a trior
  bool isTrior() const { return NetIsDeclaredAs(mFlags, eDMTriorNet); }

  //! Return true if this net is forcible
  bool isForcible() const { return ((mFlags & eForcibleNet) != 0); }

  //! Return true if this net is depositable
  bool isDepositable() const { return ((mFlags & eDepositNet) != 0); }

  //! Return true if this net was a VHDL record member passed via a port.
  bool isRecordPort () const { return ((mFlags & eRecordPort) != 0); }

  //! Return true if this net is declared as a trireg
  bool isTrireg() const { return NetIsDeclaredAs(mFlags, eDMTriregNet); }

  //! Return true if this net is declared as a reg
  bool isReg() const { return NetIsDeclaredAs(mFlags, eDMRegNet); }

  //! Return true if this net is aliased
  bool isAliased() const { return ((mFlags & eAliasedNet) != 0); }

  //! Return true if this net needs space allocated
  bool isAllocated() const { return ((mFlags & eAllocatedNet) != 0); }

  //! Return true if this net (or anything in alias ring) cannot and willnot be waveformed
  bool isInaccurate() const { return ((mFlags & eInaccurateNet) != 0); }

  //! Is this net elaboratedly dead?
  /*!
   *! This will always return false if -newAllocator is not specified
   *! on the cbuild command line.  If -newAllocator is specified, then
   *! after REAlloc runs it will return true for any NUNet that cannot
   *! be reached in an elaborated fanin walk from primary outputs &
   *! observable nets.  Note that NUNets can be unelaborated live (can
   *! influence module outputs) but elaboratedly dead.  This routine
   *! helps find those.  The benefit is that those nets don't have to
   *! connected through constructors in generated code.  They still
   *! have to be created, because of examples like this:
   *! 
   *!   {dead[3:0], live[3:0]} = {a[4:0], b[2:0]};
   */
  bool isDead() const { return ((mFlags & eDeadNet) != 0); }

  //! Return true if this net is double buffered
  virtual bool isDoubleBuffered() const;

  //! Return true if this net is declared as a 2D collection of anything
  bool is2DAnything() const;

  //! Return true if this net is declared as a memory (a 2D array of reg) (should be called is2DReg)
  bool isMem() const { return is2DReg(); }
  bool is2DReg() const { return NetIsDeclaredAs(mFlags, eDMReg2DNet); }

  //! Return true if this net is declared as a 2D array of wire
  bool is2DWire() const { return NetIsDeclaredAs(mFlags, eDMWire2DNet); }

  //! Return true if this net is declared as a 2D array of time variable
  bool is2DTime() const { return NetIsDeclaredAs(mFlags, eDMTime2DNet); }

  //! Return true if this net is declared as a 2D array of rtime variable
  bool is2DRTime() const { return NetIsDeclaredAs(mFlags, eDMRTime2DNet); }

  //! Return true if this net is declared as a 2D array of integer variable
  bool is2DInteger() const { return NetIsDeclaredAs(mFlags, eDMInteger2DNet); }

  //! Return true if this net is declared as an integer.
  bool isInteger() const { return NetIsDeclaredAs(mFlags, eDMIntegerNet); }

  //! Return true if this net is declared as a time.
  bool isTime() const { return NetIsDeclaredAs(mFlags, eDMTimeNet); }

  //! Return true if this net is declared as a real.
  bool isReal() const { return NetIsDeclaredAs(mFlags, eDMRealNet); }

  //! Return true if this net is of a type that would show the 'signed' keyword in a verilog declaration, see also isSigned()
  bool showSignedInVerilogDeclaration() const;

  //! Return true if this net is connected to tristate-driver
  /*! Note: this call is not valid until tristate analysis is complete */
  bool isTriWritten() const { return ((mFlags & eTriWritten) != 0); }

  //! Return true if this net is connected to tristate-driver
  bool isConstZ() const { return ((mFlags & eConstZ) != 0); }

  //! Return true if this net is locally undriven (computed)
  bool isUndriven() const;

  //! Return true if this net is block-local
  bool isBlockLocal() const { return ((mFlags & eBlockLocalNet) != 0); }

  //! Return true if this net has non-static semantics
  bool isNonStatic() const { return ((mFlags & eNonStaticNet) != 0); }

  //! Return true if this net possibly behaves like a latch
  /*! Note that the latch flag is not propagated to all its aliases so
   *  make sure the right unelaborated net is tested. (The one that is
   *  driven by an always block). To do a latch test on an elaborated
   *  construct see LFElabStateTest.
   */
  bool isLatch() const { return ((mFlags & eLatch) != 0); }

  //! Return true if this net is pulled-up
  bool isPullUp() const { return ((mFlags & ePullUp) != 0); }

  //! Return true if this net is pulled-down
  bool isPullDown() const { return ((mFlags & ePullDown) != 0); }

  //! Return true if this net is a signed net
  bool isSigned() const { return ((mFlags & eSigned) != 0); }

  //! Return true if this net is a reset net
  bool isReset() const { return ((mFlags & eReset) != 0); }

  //! Return true if this net should be cleared at the end of the schedule
  bool isClearAtEnd() const { return ((mFlags & eClearAtEnd) != 0); }

  //! Return if this net is pulled up, pulled down, or is declared
  //! trireg, tri0, tri1
  bool isPulled() const;

  //! Return if this net is driven by any sort of tri-state device
  /*! Any net connected to a pullup, pulldown, z-driver, primary bidirect,
      or trireg needs to be analyzed as a tristate bus, and scheduled
      appropriately.  This does not imply that the net can show up
      as a Z in a waveform.  E.g. nets that have tristate drivers
      and pullups cannot be Z.  However, primary-bidirects with
      internal pullups still need strength information in order
      to communicate to the outside world that whether there is
      a strong internal driver.
  */
  bool isTristate() const;

  //! Return if this net can take on a Z value
  /*! Nets that have tristate drivers or are connected to primary bidirects,
      and have no pullups or triregs, can take on a value of Z.  Internal
      undriven nets can also take on a value of Z.
  */
  bool isZ() const;

  //! Return true if this net is primary tristate
  bool isPrimaryZ() const { return ((mFlags & ePrimaryZ) != 0); }

  //! Return true if this net is temp,blockLocal,nonstatic and its scope matches matchScope
  bool isTempBlockLocalNonStaticOfScope(NUScope *matchScope) const {
    return ( isTempBlockLocalNonStatic() && ( getScope() == matchScope ));
  }

  //! Return true if this net is temp,blockLocal,nonstatic
  bool isTempBlockLocalNonStatic() const {
    return isTemp() && isBlockLocal() && isNonStatic();
  }

  //! Return true if this net is local (immediate child or some sort of grandchild) to the given scope.
  bool isChildOf(const NUScope *scope) const;

  //! Identify signed subrange of integer (viz.  -5 to 5)
  virtual bool isSignedSubrange () const {return false;}

  //! Identify unsigned subrange of integer (viz 0 to 5)
  virtual bool isUnsignedSubrange () const {return false;}

  //! Some sort of subrange either signed or unsigned.
  virtual bool isIntegerSubrange () const {return false; }

  //! Was this net ever a VHDL record field?
  /*!
    This is identified by looking at the net's scope. If the scope is a named declaration scope
    and it's hier flags has eRecordMask bit set, it returns true.
  */
  bool isRecordField() const;

  //! Return the flags for this net
  NetFlags getFlags() const { return mFlags; }

  //! Return the set of declaration flags for this net.
  NetFlags getDeclarationFlags() const { return NetFlags(mFlags & eDeclareMask); }

  //! Change the set of flags on this net.
  void setFlags(NetFlags flags);

  //! Return true if this net is declared in a top-level module.
  bool atTopLevel() const;

  //! true iff this net is bound as output to instantiated module
  bool isOutputConnectionAlias () const;

  //! Return true if this net is a dataflow token for an external net
  virtual bool isExtNet() const { return false; }

  //! Return true if this net is a ControlFlowNet, this is the default implementation so it returns false.
  virtual bool isControlFlowNet() const { return false; }

  //! Return true if this net is local to task/function or within a named block embedded in a task/function.
  virtual bool isTFNet() const;

  //! Return true if this net is referenced cross-hierarchically
  bool hasHierRef() const;

  //! Return a NUNetHierRef if this net is a hierref, 0 otherwise.
  virtual NUNetHierRef* getHierRef() { return 0; }
  virtual const NUNetHierRef* getHierRef() const { return 0; }

  //! Loop over the hierarchical references to this net, if any
  NUNetVectorCLoop loopHierRefs() const
  {
    return mHierRefVector ? NUNetVectorCLoop(*mHierRefVector) : NUNetVectorCLoop();
  }
  NUNetVectorLoop loopHierRefs()
  {
    return mHierRefVector ? NUNetVectorLoop(*mHierRefVector) : NUNetVectorLoop();
  }

  //! Populate a list with the aliases of this net (this net will be included)
  void getAliases(NUNetList *aliases) const;

  //! Returns whether this net feeds the clock pin of a sequential node
  /*!
   * This flag is used to determine whether we need the new value of
   * the old value of the net when computing logic and we are running
   * without input flow. When we are computing clock logic we need the
   * new value, when we are computing sequential logic we need the old
   * value.
   *
   * This mark indicates the net is used by clock logic.
   *
   * The isInDataPath function can be used to see if the net is used
   * by a data pin of a sequential block.
   *
   * This flag is currently only put on PIs. In the future we will try
   * to find a solution for derived clocks with this issue.
   */
  bool isInClkPath() const { return ((mFlags & eInClkPath) != 0); }

  //! Returns whether this net feeds the data pin of a sequential node.
  /*!
   * See the documentation for puInClkPath for why this is needed.
   *
   * This mark indicates the net is used by the data path of a
   * sequential block. This can be because it feeds a sequential node
   * directly or because it feeds a sequential node through a sample
   * based scheduled combinational block which feeds a sequential
   * node.
   */
  bool isInDataPath() const { return ((mFlags & eInDataPath) != 0); }

  //! Returns true if this net is used as an edge trigger.
  bool isEdgeTrigger() const { return ((mFlags & eEdgeTrigger) != 0); }

  //! Mark this net as block-local.
  void putIsBlockLocal(bool);

  //! Mark this net as non-static.
  void putIsNonStatic(bool);

  //! Mark this net as a flop
  void putIsFlop(bool);

  //! Mark this node as being aliased.
  void putIsAliased(bool);

  //! Mark this node as being a latch
  void putIsLatch(bool);

  //! Mark this node as being a pull-up
  void putIsPullUp(bool);

  //! Mark this node as being a pull-down
  void putIsPullDown(bool);

  //! Mark this node as being allocated.
  void putIsAllocated(bool);

  //! Mark this node cannot be waveformed
  void putIsInaccurate(bool);

  //! Mark this node as being elaboratedly dead.
  void putIsDead(bool);

  //! Mark this node as being double buffered
  virtual void putIsDoubleBuffered(bool);

  //! Mark this node as being temporary
  void putIsTemp(bool);

  //! Mark this node as being tristatable by the model
  void putIsTriWritten(bool flag);

  //! Mark this node as being tristatable by the model
  void putIsConstZ(bool flag);

  //! Mark this node as being primary tristate
  void putIsPrimaryZ(bool flag);

  //! Mark this node as being an input
  void putIsInput(bool flag);

  //! Mark this node as being an output
  void putIsOutput(bool flag);

  //! Mark this node as being a bidirect
  void putIsBid(bool flag);

  //! Mark this net as a trireg
  void putIsTrireg(bool flag);

  //! Mark this net as a tri1
  void putIsTri1(bool flag);

  //! Mark this net as a tri0
  void putIsTri0(bool flag);

  //! Mark this net as a wand
  void putIsWand(bool flag);

  //! Mark this net as a wor
  void putIsWor(bool flag);

  //! Mark this net as a triand
  void putIsTriand(bool flag);

  //! Mark this net as a trior
  void putIsTrior(bool flag);

  //! Mark this net as forcible
  void putIsForcible(bool flag);

  //! Mark this net as being a VHDL record member passed via a port
  void putIsRecordPort(bool flag);

  //! Mark this net as depositable
  void putIsDepositable(bool flag);

  //! Mark this node as a net that feeds clock pin of a sequential block
  /*! See the documentation for isInClkPath for details.
   */
  void putInClkPath(bool flag);

  //! Mark this node as a net that feeds the data pin of a sequential block
  /*! See the documentation for isInDataPath for details.
   */
  void putInDataPath(bool flag);

  //! Mark this node as a net that is used as an edge trigger.
  void putIsEdgeTrigger(bool flag);

  //! Mark this net as being read within the net's scope
  void putIsRead(bool flag);

  //! Mark this net as being written within the net's scope
  void putIsWritten(bool flag);

  //! Mark this net as needing to be cleared at the end of the schedule
  void putIsClearAtEnd(bool flag);

  //! Return true if this net is read within the net's scope
  /*! Note: this call is not valid until design R/W analysis is complete */
  bool isRead() const { return ((mFlags & eReadNet) != 0); }

  //! Return true if this net is written within the net's scope
  /*! Note: this call is not valid until design R/W analysis is complete */
  bool isWritten() const { return ((mFlags & eWrittenNet) != 0); }

  //! Return true if this net is read outside the net's scope
  virtual bool isHierRefRead() const;

  //! Return true if this net is written outside the net's scope by
  //! *any* of its references.  \sa isHierRefAlwaysWritten
  /*! Note: this call is not valid until design R/W analysis is complete */
  virtual bool isHierRefWritten() const;

  //! Propagate flags to this net because it is aliased with the given net.
  /*!
   * Call this for port aliasing, where we do not change the underlying nucleus, we only change
   * the flow.
   */
  void aliasSetFlags(const NUNet *alias);

  //! Propagate flags to this net because it is assign-aliased with the given net.
  /*!
   * Call this for assign aliasing, where we do change the underlying nucleus, the big thing this
   * does is to get the initial driver set up correctly.
   */
  void assignAliasSetFlags(const NUNet *alias);

  STAliasedLeafNode * lookupElabSymNode(const STBranchNode *hier) const;

  //! Return the NUNetElab object for this net resolved local to the given hierarchy.
  /*!
   * If this node is aliased, this will return the alias master NUNetElab,
   * which means this may return a net in another hierarchy.
   *
   * This does the right thing for named blocks: it elaborates the net to the
   * scope which is an ancestor of a module hierarchy, where that module
   * hierarchy is either the passed-in hier or the module hierarchy containing
   * the passed-in hier.
   */
  NUNetElab *lookupElab(const STBranchNode *hier, bool allowNULL = false) const;

  //! Create an elaboration of this net at the given hierarchy and put it into
  //! the symbol table.
  void createElab(STBranchNode *hier, STSymbolTable *symtab);

  //! Return the index for this net in the symbol table
  SInt32 getSymtabIndex() const APIDISTILLMACRO1(APIEMITCODE(return mMySymtabIndex ));



  //! Create a fanin iterator.  Must be deleted by the caller.
  FLNetIter* makeFaninIter(FLIterFlags::TraversalT visit,
			   FLIterFlags::TraversalT stop,
			   FLIterFlags::NestingT nesting,
			   FLIterFlags::IterationT iteration=FLIterFlags::eIterNodeOnce);

  //! Create a fanin iterator.  Must be deleted by the caller.
  FLNetIter* makeFaninIter(int visit,
			   int stop,
			   int nesting,
			   FLIterFlags::IterationT iteration=FLIterFlags::eIterNodeOnce)
  {
    return makeFaninIter((FLIterFlags::TraversalT) visit,
			 (FLIterFlags::TraversalT) stop,
			 (FLIterFlags::NestingT) nesting,
			 iteration);
  }

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name based on specified scope,
    \param includeRoot if true then top of hierarchy (or specified scope) is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
    \param escapeIfMultiLevel if true then resulting name will be escaped if a hierarchical name was generated
  */
  void compose(UtString*, const STBranchNode* scope=NULL, 
               bool includeRoot = true,
               bool hierName = true, 
               const char* separator = ".",
               bool escapeIfMultiLevel = false ) const;

  //! Compose the unelaborated net name
  /*! Compose an unelaborated name for the net, based on the original module name and full path
   *  from the module to the net.
   */
  void composeUnelaboratedName(UtString* buf) const;
  //! Compose the list of StringAtom* for unelaborated name if the net name is not
  //! protected. Otherwise return the source location of protected name.
  const SourceLocator* composeUnelaboratedName(UtList<StringAtom*>* names) const;

  //! Compose a declaration string for the net, e.g. "reg q;" or "wire [7:0] z;" or "input clk;"
  virtual void composeDeclaration(UtString*) const = 0;

  //! Compose a declaration prefix a vector or memory net.  This is the part of
  //! the declaration that precedes the net name, e.g. [7:0] for a vector net. 
  virtual void composeDeclarePrefix(UtString*) const;

  //! Compose a declaration suffix for a memory net.  This is the part of
  //! the declaration that comes after the net name -- the depth range.
  virtual void composeDeclareSuffix(UtString*) const;

  //! Put the net type into dir, e.g. "reg", "input", "wire", etc.
  void composeType(UtString* dir) const;

  //! Put all the net flags (e.g. tristate, etc) plus the type, into buf
  virtual void composeFlags(UtString* buf) const;

  //! Alphabetic comparison of names.  -1 is <; 0 is ==; 1 is >
  static int compare(const NUNet* n1, const NUNet* n2);

  bool operator<(const NUNet& other) const
  {
    return compare(this, &other) < 0;
  }

  //! Dump myself to cout
  void print(bool verbose=false, int indent=0) const;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Auxilliary information for CodeGen
  CGAuxInfo *getCGOp() const;

  //! Add the structure containing CodeGen auxilliary information.
  void setCGOp(CGAuxInfo * cgOp) const;

  //! calculate costs, add to NUCost arg
  virtual void calcCost(NUCost*, NUCostContext* = 0) const;

  //! destructor
  virtual ~NUNet();

  //! Add the given net as a possible hierarchical reference to this net.  Illegal to call if this is a hier ref.
  virtual void addHierRef(NUBase *net_hier_ref);

  //! Remove the given net as a possible hierarchical reference to this net.  Illegal to call if this is a hier ref.
  virtual void removeHierRef(NUBase *net_hier_ref);

  //! Create a hierarchical reference to this net.  Illegal to call if this is a hier ref.
  virtual NUNet* createHierRef(const AtomArray& name, NUScope *scope, NetFlags flags, const SourceLocator &loc) = 0;

  //! Is the net seen as a bidirect net from codegen's perspective?
  static bool isCodegenBidirect(const NUNet* net);

  //! find an NUNet given an ELABORATED symbol table node, or return NULL if there is none
  /*!
   * \sa findUnelab
   */
  static NUNet* find(const STSymbolTableNode* node);

  //! Return the storage NUNet given an ELABORATED symbol table node, or return NULL if there is none
  /*!
   * \sa findStorageUnelab
   */
  static NUNet* findStorage(const STSymbolTableNode* node);

  //! find an NUNet given an UNELABORATED symbol table node, or return NULL if there is none
  /*!
   * \sa find
   */
  static NUNet* findUnelab(const STSymbolTableNode* node);

  //! Return the storage NUNet given an UNELABORATED symbol table node, or return NULL if there is none
  /*!
   * \sa findStorage
   */
  static NUNet* findStorageUnelab(const STSymbolTableNode* node);

  //! Does this net contain the specified range?
  virtual bool containsRange(const ConstantRange& range) const = 0;

  //! Does this net contain the specified bit?
  virtual bool containsBit(SInt32 index) const = 0;

  //! enumeration for NUNet derivations
  enum NetType {
    eBit,
    eVector,
    eMemory,
    eTempMemory,
    eReal,
    eVirtual,
    eComposite,
    eArray
  };

  //! What type is this net?
  //virtual NetType getType() const = 0;

#if NUBITNETREF_STORED_IN_NUBITNET
  //! Return the sole netref associated with this net (bitnet or virtualnet), or NULL
  virtual const NUNetRef* getNetRef() const;
#endif

  //! Is this a bit net?  (much faster than (NULL != dynamic_cast<NUBitNet*>(net)))
  virtual bool isBitNet() const;

  //! Is this a vector net? (much faster than (NULL != dynamic_cast<NUVectorNet*>(net)))
  virtual bool isVectorNet() const;

  //! Is this a memory net? (much faster than (NULL != dynamic_cast<NUMemoryNet*>(net)))
  virtual bool isMemoryNet() const;

  //! Is this a virtual net?
  virtual bool isVirtualNet() const;

  //! Is this a system task net?
  virtual bool isSysTaskNet() const;

  //! Is this a vhdl line net?  The info for this is stored in the VectorNetFlags.
  virtual bool isVHDLLineNet() const;

  //! Is this a composite net?
  virtual bool isCompositeNet() const;

  //! Is this an array net?
  virtual bool isArrayNet() const;

  //! If this net is a memory net, return it as a memory net (tempMems return their master)
  virtual NUMemoryNet* getMemoryNet();

  //! If this net is a memory net, return it as a memory net (tempMems return their master)
  const NUMemoryNet* getMemoryNet() const {
    return const_cast<NUNet*>(this)->getMemoryNet();
  }

  //! If this net is a composite net, return it as a composite net
  virtual NUCompositeNet* getCompositeNet();

  //! If this net is a composite net, return it as a composite net
  const NUCompositeNet* getCompositeNet() const {
    return const_cast<NUNet*>(this)->getCompositeNet();
  }

  //! If this net is a vector net, return it as a vector net
  virtual NUVectorNet* castVectorNet();

  //! const version of castVectorNet()
  const NUVectorNet* castVectorNet() const
  {
    return const_cast<NUNet*>(this)->castVectorNet();
  }

  //! flag properties exclusive to vector nets - scalar nets have no vector flags
  virtual VectorNetFlags getVectorNetFlags() const { return VectorNetFlags (0);}

  virtual void putVectorNetFlags (VectorNetFlags /*f*/) {}


  //! Clone a net into a new scope
  NUNet * clone ( NUScope * scope,
                  NUNetReplacementMap & net_replacements,
                  StringAtom* name,
                  NetFlags flags);

  //! Clone hierarchical reference with new name
  NUNet* cloneHierRef(NUModule* module, const AtomArray& path);

  //! Build hierarchical reference from local net, given a path
  NUNet* buildHierRef(NUModule* module, const AtomArray& path);

  //! Get the current number of times any net has been deleted
  static UInt32 getNetDeleteCounter();

  //!This function should be used during population to get the declared
  //number of dimensions which may not be the number of dimensions of
  // the net. For both two and three dimensional array nets "is2DAnything" flag
  // is set. So, to know whether a net is 3D this function is useful.
  UInt32 getDeclaredDimensions() const;

  //! construct the range for a net given the normalised offsets
  ConstantRange makeRange (const ConstantRange &sliced_offsets) const;

  //! Get the source language for this net
  /*! Each net gets it's source language from its parent
   *  module/architecture. Mixed language designs are divided by
   *  hierarchical components like modules and architectures (Verilog
   *  and VHDL).
   *
   *  Note that if a net gets flattened from one language into
   *  another, the new temp net created will be for the parent
   *  hierarchical component. This is ok because the temp net is not
   *  directly visible by the user. If the original net survives as an
   *  alias in the original scope, it will retain the correct
   *  language.
   */
  HierFlags getSourceLanguage() const;


protected:
  //! Hide copy and assign constructors.
  NUNet(const NUNet&);
  NUNet& operator=(const NUNet&);

  //! Helper function for print, prints the name and size information
  virtual void printNameSize() const = 0;

  //! Scope in which this net is declared.
  NUScope *mScope;

  //! Name of this net.
  //StringAtom *mName;
  STAliasedLeafNode *mNameLeaf APIDISTILLMACRO1(APIEMITTYPE(StringAtom*));

  //! Original name of the net as it's declared by RTL. 
  // It's used for temporary nets only.
  StringAtom*  mOrigName APIDISTILLMACRO2(APICONSTRUCTORFALSE,
                                          APIEMITCODE(mOrigName = NULL) );

  //! Source location of this net.
  SourceLocator mLoc;

  //! Continuous drivers of this net.
  FLNodeSet mContinuousDrivers APIOMITTEDFIELD;

  //! Flags of this net.
  NetFlags mFlags;

  //! Index of me in the symbol table
  SInt32 mMySymtabIndex APIDISTILLMACRO2(APICONSTRUCTORFALSE,
                                         APIEMITCODE(mMySymtabIndex = mScope->reserveSymtabIndex()) );

  //! Cross-hierarchy net references pointing to this.  If NULL, there are none.
  NUNetVector *mHierRefVector APIDISTILLMACRO2(APICONSTRUCTORFALSE,
                                               APIEMITCODE(mHierRefVector = NULL) );
  

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS ; // class NUNet : public NUBase


//! NUNetElab class
/*!
 * Model the elaboration of a net (the net existing at some elaborated
 * point in the hierarchy).
 */
class NUNetElab : public NUElabBase
{
public:
  //! constructor
  /*!
    \param net The correspdoning unelaborated net
    \param hier The hierarchy in which this net resides
   */
  //  NUNetElab(NUNet *net, STBranchNode *hier);
  NUNetElab(NUNet *net, STAliasedLeafNode *myName);

  //! Return the corresponding unelaborated net
  NUNet *getNet() const { return mNet; }

  //! Return the hierarchy in which this net resides
  STBranchNode *getHier() const;
  
  //! Return the symbol table entry for this elaboration
  STAliasedLeafNode *getSymNode() const;

  //! Return the storage symbol table entry.
  STAliasedLeafNode * getStorageSymNode() const;

  //! Return the unelaborated net associated with the storage element in the symbol table.
  NUNet * getStorageNet() const;

  //! Return the hierarchy for the associated storage element.
  STBranchNode * getStorageHier() const;

  //! Realise NUElabBase::getSourceLocation virtual method
  virtual const SourceLocator getSourceLocation () const;

  //! Realise NUElabBase::findUnelaborated virtual method
  virtual bool findUnelaborated (STSymbolTable *stab, STSymbolTableNode **unelab) const;

  //! Return TRUE if an alias of this elaborated net is a primary port.
  bool isPrimaryPort() const;

  //! Return TRUE if an alias of this elaborated net is a primary input.
  bool isPrimaryInput() const;

  //! Return TRUE if an alias of this elaborated net is a primary inout.
  bool isPrimaryBid() const;

  //! Return TRUE if an alias of this elaborated net is a primary output.
  bool isPrimaryOutput() const;

  //! Return TRUE if an alias of this elaborated net is at the top level.
  bool atTopLevel() const;

  //! Put whether or not this elab causes a combinational schedule run
  /*!
   * This gets set by the scheduler to mark alias rings that require a
   * combinational schedule run when the value is changed. This
   * includes depositable and forcible nets that drive a combo
   * schedule.
   */
  void putIsComboRun(bool isComboRun);

  //! Get whether or not a deposit/force causes a combo schedule run
  bool isComboRun() const;

  //! Datatype to abstract looping over driver flow nodes
  typedef FLNodeElabSet::UnsortedLoop DriverLoop;

  //! Datatype to abstract looping over driver flow nodes
  typedef FLNodeElabSet::UnsortedCLoop ConstDriverLoop;

  //! Datatype to abstract sorted looping over driver flow nodes
  typedef FLNodeElabSet::SortedLoop SortedDriverLoop;

  //! Functor to skip hierarchical references and null nets
  struct NoHierRef
  {
    //! Returns true if node has a non-null net and is not a hierref
    bool operator() (const STAliasedLeafNode* node) const;
  };

  //! Datatype to loop over node aliases skipping hierrefs and null nets
  typedef LoopFilter<STAliasedLeafNode::AliasLoop, NoHierRef> NoHierRefAliasLoop;

  //! Loop over aliases skipping hierrefs and null nets
  NoHierRefAliasLoop loopNoHierRefAliases();

  //! Loop over the continuous driver flow nodes
  /*!
   * Not guaranteed to have one; only driven nets have these, and then only
   * if driven continuously.
   */
  DriverLoop loopContinuousDrivers() {
    return mContinuousDrivers.loopUnsorted();
  }

  //! Loop over the continuous driver flow nodes -- const version
  ConstDriverLoop loopContinuousDrivers() const {
    return mContinuousDrivers.loopCUnsorted();
  }

  //! Loop over the continuous driver flow nodes
  /*!
   * Not guaranteed to have one; only driven nets have these, and then only
   * if driven continuously.
   */
  SortedDriverLoop loopSortedContinuousDrivers() const;


  //! Return the single continuous driver for this net; isMultiplyDriven() must be false
  FLNodeElab* getContinuousDriver() const;

  //! Return the first driver of the specified type; NULL if one doesn't exist.
  FLNodeElab* getFirstContinuousDriverOfType(FLTypeT flow_type) const;

  //! Is this flow a continuous driver?
  /*!
   * Returns true if \param possibleDriver is a continuous driver of 'this'
   */
  bool hasContinuousDriver(FLNodeElab *possibleDriver) const;

  //! Is this net continuously driven by a flow node with overlapping type?
  bool hasContinuousDriver(FLTypeT flow_type) const;

  //! Return true if this net has multiple continuous drivers
  inline bool isMultiplyDriven() const {
    ConstDriverLoop loop = loopContinuousDrivers();
    if (!loop.atEnd())
    {
      ++loop;
      return !loop.atEnd();
    }
    return false;
  }

  //! Return true if this net has at least one continuous driver
  inline bool isContinuouslyDriven() const {
    return (not mContinuousDrivers.empty());
  }
  
  //! Add the driver flow node for this net.
  void addContinuousDriver(FLNodeElab *driver);

  //! Replace all the drivers with the given single driver
  void replaceContinuousDrivers(FLNodeElab *driver);

  //! Remove the given node, if it is a driver.
  void removeContinuousDriver(FLNodeElab *driver);

  //! Remove all drivers associated with this net.
  void removeContinuousDrivers();

  //! Remove any drivers associated with this net that are in the set
  //! \returns the number of drivers removed
  UInt32 removeContinuousDriverSet(const FLNodeElabSet& flowSet);

  //! Trim excess capacity from the driver array
  void trimExcessCapacity();

  //! Set this net as the master net of its alias ring
  void setIsMaster();

  //! Mark this node as allocated in the symbol table.
  void setAllocated();

  //! Make this net be an alias of the given master.
  /*!
   \param master Net which will be the alias master.
   \param fixStorage leave this as default ordinarily, it can
                     be set to false for system nets, which
                     do not need runtime storage, and for
                     which it is expensive (n^2) elaborate
                     and maintain storage.
   * This NUNetElab will be deleted soon.
   *
   * 1. Erase myself from the symbol table.
   * 2. Put myself in the alias ring of the master.
   */
  void alias(NUNetElab *master, bool fixStorage = true);

  //! Get the set of nets that are aliases of a given elaborated symbol node
  /*! This routine traverses the alias ring and retrieves the NUNet*
   *  for every alias. It puts this in a NUNetVector provided by the
   *  caller.
   *
   *  \param leaf The elaborated symbol table node for which to
   *  retrieve the aliases.
   *
   *  \param netVector a place to put all the nets from the alias
   *  ring.
   *
   *  \param parentFilter if non-NULL it filters the nets based on
   *  hierarchy
   *
   *  \param includeUnallocated indicates whether to include NUNet* which
   *  will not be allocated at codegen time, such as flattened or
   *  assignment-aliased nets.
   */
  static void getAliasNets(const STAliasedLeafNode* leaf,
                           NUNetVector* netVector,
                           bool includeUnallocated,
                           const STBranchNode* parentFilter = NULL);

  //! Get the set of nets that are aliases of a given net elab
  /*! This routine traverses the alias ring and retrieves the NUNet*
   *  for every alias. It puts this in a NUNetVector provided by the
   *  caller.
   *
   *  \param netVector a place to put all the nets from the alias
   *  ring.
   *
   *  \param includeUnallocated indicates whether to include NUNet* which
   *  will not be allocated at codegen time, such as flattened or
   *  assignment-aliased nets.
   *
   *  \param parentFilter if non-NULL it filters the nets based on
   *  hierarchy
   */
  void getAliasNets(NUNetVector* netVector,
                    bool includeUnallocated,
                    const STBranchNode* parentFilter = NULL) const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Create a fanin iterator.  Must be deleted by the caller.
  FLNetElabIter* makeFaninIter(FLIterFlags::TraversalT visit,
                               FLIterFlags::TraversalT stop,
                               FLIterFlags::NestingT nesting,
                               FLIterFlags::IterationT iteration=FLIterFlags::eIterNodeOnce);

  //! Create a fanin iterator.  Must be deleted by the caller.
  FLNetElabIter* makeFaninIter(int visit,
                               int stop,
                               int nesting,
                               FLIterFlags::IterationT iteration=FLIterFlags::eIterNodeOnce)
  {
    return makeFaninIter((FLIterFlags::TraversalT) visit,
                         (FLIterFlags::TraversalT) stop,
                         (FLIterFlags::NestingT) nesting,
                         iteration);
  }

  //! Return true if any of the unelaborated nets in the alias ring match
  bool queryAliases(NUNet::QueryFunction fn) const;

  //! Dump myself to cout
  void print(bool verbose, int indent) const;

  //! Return class name
  const char* typeStr() const;

  //! destructor
  ~NUNetElab();

  //! find an NUNetElab given a symbol table node, or return NULL if there is none
  static NUNetElab* find(const STSymbolTableNode* node);

  //! Get all the alias nets and the net flags for primary ports
  /*! A primary port may be represented by a single symbol table node
   *  or multiple symbol table nodes. This routine takes a vector of
   *  symbol table nodes and gathers all the net aliases and net flags
   *  for the primary ports.
   */
  static void getPrimaryNetsAndFlags(const STAliasedLeafNodeVector& leafNodes,
                                     NetFlags* netFlags, NUNetSet* primNets);


  //! Get all the alias nets and the net flags for primary ports
  /*! A primary port may be represented by a single symbol table node
   *  or multiple symbol table nodes. This routine takes a vector of
   *  symbol table nodes and gathers all the net aliases and net flags
   *  for the primary ports.
   */
  static NetFlags getPrimaryNetsAndFlags(STSymbolTable* symbolTable,
                                         NUNetElab* netElab,
                                         NUNetSet* primNets);


protected:
  //! Helper function for print, prints the name and size information
  virtual void printNameSize() const;

private:
  //! Hide copy and assign constructors.
  NUNetElab(const NUNetElab&);
  NUNetElab& operator=(const NUNetElab&);

  //! Corresponding unelaborated net.
  NUNet *mNet;

  //! Continuous drivers of this net.
  FLNodeElabSet mContinuousDrivers;
} APIOMITTEDCLASS; //class NUNetElab : public NUElabBase

//! needs documentation
struct NUNetElabCmp
{
  bool operator()(const NUNetElab* n1, const NUNetElab* n2) const
  {
    return NUElabBase::compare(n1, n2) < 0;
  }      
};

//! compare a net by name & scope
struct NUNetCmp
{
  bool operator()(const NUNet* n1, const NUNet* n2) const
  {
    return NUNet::compare(n1, n2) < 0;
  }      
};

//! comparison a net by pointer
struct NUNetCmpPtr
{
  bool operator()(const NUNet* n1, const NUNet* n2) const
  {
    return n1 < n2;
  }      
};

//! needs documentation
typedef UtSet<NUNet*, NUNetCmp> NUNetOrderedSet;

//! const NUNet* key'ed ordered set
typedef UtSet<const NUNet*, NUNetCmp> ConstNUNetOrderedSet;

//! needs documentation
typedef UtSet<NUNetElab*, NUNetElabCmp> NUNetElabOrderedSet;

//! NUVectorNet class
/*!
 * A net which is a vector.
 */
class NUVectorNet : public NUNet
{
public:
  //! Constructor to use when net is declared in a module
  /*!
    \param name Name of the net
    \param range Declared range of this net
    \param flags Attribute flags
    \param scope Scope in which net is declared
    \param loc Source location of declaration of the net
   */
  NUVectorNet(StringAtom *name,
	      const ConstantRange& range,
	      NetFlags flags,
              VectorNetFlags vflags,
	      NUScope *scope,
	      const SourceLocator& loc);

  //! Legacy constructor that takes ownership of range by ptr
  /*!
    \param name Name of the net
    \param range Declared range of this net
    \param flags Attribute flags
    \param scope Scope in which net is declared
    \param loc Source location of declaration of the net
   */
  NUVectorNet(StringAtom *name,
	      ConstantRange* range,
	      NetFlags flags,
              VectorNetFlags vflags,
	      NUScope *scope,
	      const SourceLocator& loc);

  //! Return the declared bit size
  virtual UInt32 getBitSize() const;

  //! Get the number of bits this memory net needs in a netref
  //! for vectors this is the width of the vector \sa NUNetRef
  virtual UInt32 getNetRefWidth() const;

  //! Return operational range -- always size-1:0
  const ConstantRange *getRange () const {return &mOperationalRange;}

  //! Return declared range
  const ConstantRange *getDeclaredRange () const {return &mDeclaredRange;}

  //! Compose a declaration string for the net, e.g. "reg q;" or "wire [7:0] z;" or "input clk;"
  void composeDeclaration(UtString*) const;

  //! Given a selection expression, normalize it for range size-1:0
  /*! sel parameter will either be used in return value or deleted by
   *! this routine.  If the bit is a constant expression, and is out
   *! of range for the net's declared range, then either NULL is
   *! returned, or an expression for constant 0.  This is controlled
   *! by parameter retNullForOutOfRange.
   *!
   *! \p adjust provides additional offset for partselects involving little-endian
   *! vectors or -: indexing.  Canonically, the index expression refers to the lsb.  This
   *! handles situations where the user's index expression refers to the MSB of
   *! a fixed-width range.  The adjust value is the amount to reduce the index expression
   *! by so that at simulation-time we are computing an index to the LSB of the range.
   *!
   *! The intent here is to allow out-of-range LHS bitselects to cause
   *! an error, but to return 0 for out-of-range RHS bitselects.
   *! 
   */
  NUExpr* normalizeSelectExpr(NUExpr* sel, SInt32 adjust, bool retNullForOutOfRange);

  //! Given a normalized selection expression, denormalize it for
  /*! the vector-net's declared range.  sel parameter will either
   *! be used in return value or deleted by
   *! this routine.
   */
  NUExpr* denormalizeSelectExpr(NUExpr* sel);

  //! Put all the vector net flags into buffer
  virtual void composeFlags(UtString* buf) const;

  //! Compose a declaration prefix a vector or memory net.  This is the part of
  //! the declaration that precedes the net name, e.g. [7:0] for a vector net. 
  virtual void composeDeclarePrefix(UtString*) const;

  //! Return class name
  const char* typeStr() const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! Create a hierarchical reference to this net.
  NUNet* createHierRef(const AtomArray& name, NUScope *scope, NetFlags flags, const SourceLocator &loc);

  //! destructor
  ~NUVectorNet();

  //! Does this net contain the specified range?
  virtual bool containsRange(const ConstantRange& range) const;

  //! Does this net contain the specified bit?
  virtual bool containsBit(SInt32 index) const;

  //! Is this a vector net?
  virtual bool isVectorNet() const;

  //! Is this a vhdl line net?
  virtual bool isVHDLLineNet() const;

  //! If this net is a vector net, return it as a vector net
  virtual NUVectorNet* castVectorNet();

  //! flag properties exclusive to vector nets
  VectorNetFlags getVectorNetFlags() const;

  //! Identify signed subrange of integer (viz.  -5 to 5)
  bool isSignedSubrange () const;

  //! Identify unsigned subrange of integer (viz 0 to 5)
  bool isUnsignedSubrange () const;

  //! Some sort of subrange either signed or unsigned.
  bool isIntegerSubrange () const;

  //! set vector properties
  void putVectorNetFlags (VectorNetFlags f);


protected:
  //! Helper function for print, prints the name and size information
  void printNameSize() const;

  //! Declared range -- always size-1:0
  ConstantRange mDeclaredRange;

  //! Operational range.
  ConstantRange mOperationalRange APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Flags meaninful only to vectors.
  VectorNetFlags mVectorNetFlags;

private:
  //! Hide copy and assign constructors.
  NUVectorNet(const NUVectorNet&);
  NUVectorNet& operator=(const NUVectorNet&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS ; //class NUVectorNet : public NUNet

//! NUMemoryNet class
/*!
 * A net which is a N-dimensional object.
 */
class NUMemoryNet : public NUNet
{
public:
    //! Constructor to use when a 2-D memory is declared in a module
    // This is the simplest possible NUMemoryNet -- e.g. reg [7:0] mem [1:100]
    /*!
      \param name Name of the memory
      \param width_range Declared width range of this memory
      \param depth_range Declared depth range of this memory
      \param flags Attribute flags
      \param vflags VectorNet attribute flags
      \param scope Scope in which net is declared
      \param loc Source location of declaration of the net
     */
    NUMemoryNet(StringAtom *name,
                ConstantRange *width_range,
                ConstantRange *depth_range,
                NetFlags flags,
                VectorNetFlags vflags,
                NUScope *scope,
                const SourceLocator& loc);

  //! Constructor to use when a N-dim memory is declared in a module
  /*!
    \param name Name of the memory
    \param firstUnpackedDimension is the index into range_array for the first unpacked dimension, in a simple verilog memory this is 1
    \param range_array Array of ranges for this memory.  range(0) is for the fastest changing dimension, in a simple verilog memory it is the width
    \param flags Attribute flags
    \param vflags VectorNet attribute flags
    \param scope Scope in which net is declared
    \param loc Source location of declaration of the net
   */
  NUMemoryNet(StringAtom *name,
              UInt32 firstUnpackedDimension,
              ConstantRangeArray *range_array,
              NetFlags flags,
              VectorNetFlags vflags,
              NUScope *scope,
              const SourceLocator& loc);

  //! Copy the Range information from \a src to this.  Also includes mFirstUnpackedDimensionIndex which must remain consistent with range information
  void copyRanges( NUMemoryNet *src );

  //! copy some flags and variables from \a src to this. Eventually we would like to have all flags handled by this method.
  void copyAuxInformation( NUMemoryNet *src);

  //! Return the declared total bit size.
  /*! This will return 0 if the calculated size exceeds 2^32.  This zero
   *  value must be detected by population, to ensure that it never
   *  propagates into the backend of cbuild.
   */
  virtual UInt32 getBitSize() const;

  /*! \brief Return the number of bits in a word of the resynthesized memory
   */
  UInt32 getRowSize () const { return mOperationalWidthRange.getLength(); }

  /*! \brief Return the width of the given memory dimension.
   */
  UInt32 getRangeSize (UInt32 dimension) const;

  /* \brief Returns the nth memory range
   */
  const ConstantRange *getRange( UInt32 dimension ) const
  { return &mRanges[dimension]; }

  /* \brief Returns the nth normalized memory range
   */
  const ConstantRange &getOperationalRange( UInt32 dimension ) const
  { return mOperationalRanges[dimension]; }

  /* \brief Set the depth of the memory
     NOTE: only intended to be called from memory resynthesis!
   */
  void putMemoryDepth( SInt32 msb, SInt32 lsb );

  /* \brief Set the width of the memory
     NOTE: only intended to be called from memory resynthesis!
   */
  void putMemoryWidth( SInt32 msb, SInt32 lsb );

  //! Get the number of bits this memory net needs in a netref
  /*! netrefs for memories use the top level dimension, so for
   *  a 2D memory it is the size of the depth.  \sa NUNetRef
   */
  virtual UInt32 getNetRefWidth() const;

  //! Get the range that memory netrefs work on
  const ConstantRange *getNetRefRange() const;

  //! Return the number of words in this memory. Only works if isResynthesized() is true for this memory.
  UInt32 getDepth() const;

  //! Get the number of declared dimensions in the original HDL.  Does not consider the fact that the memory may have been resynthesized.
  UInt32 getNumDims() const { return mRanges.size(); }

  //! Return true, this is a memory net (much faster than (NULL != dynamic_cast<NUMemoryNet*>(net)))
  bool isMemoryNet() const;

  //! Return "declared" range for the fastest changing dimension. Only works if isResynthesized() is true for this memory.
  // If the memory in the HDL was declared with 1 packed dimension then this is that range.
  // If the memory was declared with more than 1 packed dimensions then this is the resynthesized width.
  const ConstantRange *getDeclaredWidthRange() const;

  //! Return "declared" range for the slowest changing dimension.  Only works if isResynthesized() is true for this memory.
  // If the memory in the HDL was declared with 1 unpacked dimension then this is that range.
  // If the memory was declared with more than 1 unpacked dimension then this is the resynthesized depth.
  const ConstantRange *getDeclaredDepthRange() const;

  //! Return operational width range (this is normalized form of getDeclaredWidthRange())
  const ConstantRange *getWidthRange() const;

  //! returns the depth range. NOTE this not not normalized and is identical to getDeclaredDepthRange() 
  const ConstantRange *getDepthRange() const;


  //! Convert an expr relative to the declared range to be relative to the operational range
  NUExpr* normalizeSelectExpr(NUExpr* sel, UInt32 dim, SInt32 adjust,
                              bool retNullForOutOfRange);

  //! Convert an expr relative to the operational range to be relative to the declared range
  NUExpr* denormalizeSelectExpr(NUExpr* sel, UInt32 dim);

  //! Return class name
  const char* typeStr() const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! destructor
  ~NUMemoryNet();

  //! Increment the number of write ports
  void incrementWritePorts(UInt32 write_ports) { mNumWritePorts+=write_ports; }

  //! Get the number of write ports (accurate after populate is done)
  UInt32 getNumWritePorts() const { return mNumWritePorts; }

  //! Set the number of write ports
  void putNumWritePorts (UInt32 num) { mNumWritePorts = num; }

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! Put that this memory is written in a for loop
  void putWrittenInForLoop(bool writtenInForLoop)
  {
    mWrittenInForLoop = writtenInForLoop;
  }

  //! Check if this memory is written in a for loop
  bool getIsWrittenInForLoop() const { return mWrittenInForLoop; }

  //! Does this memory need to be dynamic for StateUpdate and TempMemory?
  bool getIsDynamic() const { return (getIsWrittenInForLoop() or hasHierRef() or isHierRef()); }

  //! Has this memory been through multidimensional resynthesis?
  bool isResynthesized() const { return mResynthesized; }

  //! set the resynthesis flag,
  /*! setting this to true means that:
   *   all references to this memory are compatibile with a single packed and a single unpacked dimension
   *   it also means that the depth and width member variables of this NUMemoryNet are now vaild and represent the single packed and unpacked ranges
   *    the declared ranges can still be found in the mRanges array
   */
  void putResynthesized( bool resynth );

  //! Put the memory-specific net flags plus the type, into buf
  virtual void composeFlags(UtString* buf) const;

  // Compose a declaration string for the net, e.g. "reg q;" or "wire [7:0] z;" or "input clk;"
  void composeDeclaration(UtString*) const;

  //! Compose a declaration prefix a vector or memory net.  This is the part of
  //! the declaration that precedes the net name, e.g. [7:0] for a vector net. 
  virtual void composeDeclarePrefix(UtString*) const;

  //! Compose a declaration suffix for a memory net.  This is the part of
  //! the declaration that comes after the net name -- the depth range.
  virtual void composeDeclareSuffix(UtString*) const;

  //! Create a hierarchical reference to this net.
  NUNet* createHierRef(const AtomArray& name, NUScope *scope, NetFlags flags, const SourceLocator &loc);

  //! Does this memory (net) contain the specified range? (specified range is expected to be normalized) 
  virtual bool containsRange(const ConstantRange& range) const;

  //! Does this net contain the specified bit?
  virtual bool containsBit(SInt32 index) const;

  //! If this net is a memory net, return it as a memory net
  virtual NUMemoryNet* getMemoryNet();

  //! Mark this node as being double buffered
  virtual void putIsDoubleBuffered(bool);

  //! Mark this node as being double buffered
  virtual bool isDoubleBuffered() const;

  //! flag properties exclusive to vector nets (or memories of vectors)
  VectorNetFlags getVectorNetFlags() const;

  //! Identify signed subrange of integer (viz.  -5 to 5)
  bool isSignedSubrange () const;

  //! Identify unsigned subrange of integer (viz 0 to 5)
  bool isUnsignedSubrange () const;

  //! Some sort of subrange either signed or unsigned.
  bool isIntegerSubrange () const;

  //! set vector properties
  void putVectorNetFlags (VectorNetFlags f);

  //! return true if any declared range of memory contains a negative number
  bool isDeclaredWithNegativeIndex() const;

  //! get the index (into mRanges) of the first unpacked dimension, all ranges at indices below this number are packed dimensions (note 0-based)
  UInt32 getFirstUnpackedDimensionIndex() const {return mFirstUnpackedDimensionIndex;}

  //! returns true if this NUMemoryNet had the implicit [0:0] range added as the only unpacked range (this is done only if it had multiple packed ranges and no packed ranges)
  bool isMemHaveImplicitUnpackedRange() const { return mMemHadImplicitUnpackedRangeAdded; }
  void setImplicitUnpackedRangeAdded(bool val) { mMemHadImplicitUnpackedRangeAdded = val; }

  //! add the specified range to the end of mRanges, and the normalized equivalent to mOperationalRanges
  //   NOTE: only intended to be used by memory resynthesis process
  void appendRange(const ConstantRange& range) {
    mRanges.push_back(range);
    ConstantRange normalized(range.getLength()-1, 0);
    mOperationalRanges.push_back(normalized);
  }


protected:
  //! Helper function for print, prints the name and size information
  void printNameSize() const;

  //! An array to hold the declared ranges.
  /*! the entry at index 0 is the innermost range, the least significant, it is the index that changes the fastest
   *  The entry at (mRanges.size()-1) is the outermost, the index that changes the slowest, the index that selects the largest chunk
   * e.g. reg [7:0] two_d [100:101]; // two words of 8 bits each
   *   mRanges[0] = <7:0>
   *   mRanges[1] = <100:101>
   * 
   * e.g. reg [7:0] [15:8] four_d [16:31] [100:101]; // a 4 d array, 2 packed dimensions, 2 unpacked
   *   mRanges[0] = <15:8>
   *   mRanges[1] = <7:0>
   *   mRanges[2] = <100:101>
   *   mRanges[3] = <16:31>
   *
   * All packed dimensions have indices lower than any unpacked dimension. \sa mFirstUnpackedDimensionIndex
   *  This is the same ordering that is used in composite nets.
   */
  ConstantRangeVector mRanges;

  //! holds the index (into mRanges) of the first dimension that is unpacked.  
  /*! If mFirstUnpackedDimensionIndex is 0 then there are no packed dimensions
   * if mFirstUnpackedDimensionIndex >= mRanges.size() then there are no unpacked dimensions
   * \sa mRanges
   */ 
  UInt32 mFirstUnpackedDimensionIndex;

  //! is true if this NUMemoryNet have the implicit [0:0] added as the only unpacked range (because it had multiple packed ranges)
  bool mMemHadImplicitUnpackedRangeAdded;

  //! Operational (i.e. normalized) ranges, this is defined in parallel to the mRanges Vector, except here each entry is a ranges of the form [n:0]
  // perhaps this should have been called mNormalizedRanges
  // do not confuse with the mOperationalWidthRange below which is the normalized range for the simplified memory
  ConstantRangeVector mOperationalRanges APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Declared width range, un-normalized, only valid for memories marked as being resynthesized
  // the width (number of bits in the packed dimension(s)), if isResynthesized() is true then  this is the memory word size
  // perhaps this should be called the mResynthesizedWidthRange? or mSimplifiedMemoryWidthRange, \sa mOperationalWidthRange
  ConstantRange mDeclaredWidthRange;

  //! Implementation depth range, is always normalized (<<<< original comment was wrong, this is not normalized)
  // The number of words in the unpacked dimension(s), only valid if memory marked as resynthesized
  // perhaps this should be called the mSimplifiedMemoryDepthRange
  ConstantRange mDeclaredDepthRange APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Implementation width range, normalized, only valid after memory is resynthesized
  // should be equivalent to the mDeclaredWidthRange, but normalized 
  // perhaps this should be called the mSimplifiedMemoryWidthRange
  ConstantRange mOperationalWidthRange APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  // There is no need for a normalized version of the DepthRange, this comment is here to show that this kind of symmertry is unnecessary
  // ConstantRange mOperationalDepthRange APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Vector Properties
  VectorNetFlags mVectorNetFlags;

private:
  //! Hide copy and assign constructors.
  NUMemoryNet(const NUMemoryNet&);
  NUMemoryNet& operator=(const NUMemoryNet&);

  //! The number of write ports for this memory (how many LHS's it is on)
  /*!
   * This number is the number of defined write ports, not the number
   * of instances. If there are multiple instances of a module, the
   * replication of the data should take care of the number of ports.
  */
  UInt32 mNumWritePorts APIOMITTEDFIELD;

  // If set, this memory is written in a for loop. The number of write
  // ports is inacurate if we are written in a for loop.
  bool mWrittenInForLoop APIOMITTEDFIELD;

  //! is this net double buffered?
  bool mIsDoubleBuffered APIOMITTEDFIELD;

  //! Has this memory been through resynthesis?
  bool mResynthesized APIOMITTEDFIELD;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUMemoryNet : public NUNet

//! NUTempMemoryNet class
/*!
 * A net which is a temporary storage for non-blocking memory writes
 */
class NUTempMemoryNet : public NUNet
{
public:
  //! Constructor to use when memory is declared in a module
  /*!
    \param name Name of the memory
    \param master net this is temping for
    \param flags Attribute flags
    \param scope Scope in which net is declared
    \param loc Source location of declaration of the net
   */
  NUTempMemoryNet(StringAtom *name,
                  NUMemoryNet* master,
                  NetFlags flags,
                  bool isDynamic,
                  NUScope*);

  //! Destructor
  virtual ~NUTempMemoryNet();

  //! Return the declared bit size
  virtual UInt32 getBitSize() const;

  //! Get the number of bits this memory net needs in a netref
  /*! netrefs for memories use the top level dimension, so for
   *  a 2D memory it is the size of the depth.  \sa NUNetRef
   */
  virtual UInt32 getNetRefWidth() const;

  //! Return the declared bit size
  UInt32 getDepth() const { return mMaster->getDepth(); }

  //! Return declared width range
  const ConstantRange *getDeclaredWidthRange () const {
    return mMaster->getDeclaredWidthRange();
  }

  //! Return operational width range
  const ConstantRange *getWidthRange () const {
    return mMaster->getWidthRange();
  }

  //! Return declared depth range
  const ConstantRange *getDepthRange () const {
    return mMaster->getDepthRange();
  }

  //! Return class name
  const char* typeStr() const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Increment the number of write ports.
  void incrementWritePorts(UInt32 write_ports);

  //! Get the number of write ports (accurate after loop unrolling and MemoryLoop is done)
  UInt32 getNumWritePorts() const { return mNumWritePorts;}

  //! Set the number of write ports
  void putNumWritePorts (UInt32 num) { mNumWritePorts = num; }

  //! clear out the master field -- to aids in destruction
  void clearMaster() {mMaster = NULL;}

  //! Get the master memory this is temping for
  const NUMemoryNet* getMaster() const {return mMaster;}

  //! Get the master memory this is temping for
  NUMemoryNet* getMaster() {return mMaster;}

  //! Change the master memory this is temping for
  void putMaster(NUMemoryNet *newMaster) {mMaster=newMaster;}

 //! Does this temp need a dynamically expandable # of write records?
  bool isDynamic() const { return (mIsDynamic or mMaster->getIsDynamic() or isHierRef () or hasHierRef ()); }

  //! Mark this temporary memory as dynamic.
  void putIsDynamic() { mIsDynamic = true; }

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  // Compose a declaration string for the net, e.g. "reg q;" or "wire [7:0] z;" or "input clk;"
  void composeDeclaration(UtString*) const;

  //! Compose a declaration prefix a vector or memory net.  This is the part of
  //! the declaration that precedes the net name, e.g. [7:0] for a vector net. 
  virtual void composeDeclarePrefix(UtString*) const;

  //! Compose a declaration suffix for a memory net.  This is the part of
  //! the declaration that comes after the net name -- the depth range.
  virtual void composeDeclareSuffix(UtString*) const;

  //! Illegal to call; cannot create hierrefs to temp memories.
  NUNet* createHierRef(const AtomArray& name, NUScope *scope, NetFlags flags, const SourceLocator &loc);

  //! Does this net contain the specified range?
  virtual bool containsRange(const ConstantRange& range) const;

  //! Does this net contain the specified bit?
  virtual bool containsBit(SInt32 index) const;

  //! If this net is a memory net, return it as a memory net
  virtual NUMemoryNet* getMemoryNet();

  //! flag properties exclusive to vector nets
  VectorNetFlags getVectorNetFlags() const;

  //! Identify signed subrange of integer (viz.  -5 to 5)
  bool isSignedSubrange () const;

  //! Identify unsigned subrange of integer (viz 0 to 5)
  bool isUnsignedSubrange () const;

  //! Some sort of subrange either signed or unsigned.
  bool isIntegerSubrange () const;

  //! set vector properties
  void putVectorNetFlags (VectorNetFlags ) {}


protected:
  //! Helper function for print, prints the name and size information
  void printNameSize() const;

private:
  //! Hide copy and assign constructors.
  NUTempMemoryNet(const NUTempMemoryNet&);
  NUTempMemoryNet& operator=(const NUTempMemoryNet&);

  NUMemoryNet* mMaster;
  SInt32 mNumWritePorts;
  bool mIsDynamic;
};

#endif
