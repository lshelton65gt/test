// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#ifndef NUEXTNET_H_
#define NUEXTNET_H_

#ifndef NUVIRTUALNET_H_
#include "nucleus/NUVirtualNet.h"
#endif


//! NUExtNet class
/*!
 * Dataflow token to represent a reference to a net outside this module's
 * scope.  For example, if we have an observable net in a child module,
 * we create an NUExtNet in the parent module to represent this.
 *
 \code
  module top(i1, i2);
    input i1, i2;
    wire w = i1 & i2;
   child child (w);
  endmodule
  module child(i);
    input i;
    wire r;
    assign r = i;
  endmodule
  observeSignal child.r
 \endcode
 *
 * If we don't represent the data dependency of child.r on top.w, we may
 * think that top.w is dead and optimize it away.  To represent this data
 * dependency, we use an NUExtNet in top.
 *
 * To aid in the analysis, we also use an NUExtNet in child which
 * depends on all the observable nets in child.  Even if there are multiple
 * observable nets, we only create one NUExtNet (which will depend upon
 * each observable net).
 */
class NUExtNet : public NUVirtualNet
{
public:
  //! Constructor
  /*!
    \param name Name of the net
    \param flags Attribute flags
    \param scope Scope in which net is declared
    \param loc Source location of declaration of the net
   */
  NUExtNet(StringAtom *name,
	   NetFlags flags,
	   NUScope *scope,
	   const SourceLocator& loc);

  bool isExtNet() const { return true; }

  //! destructor
  ~NUExtNet();

  //! Return class name
  const char* typeStr() const;

  //! Compose a declaration string for the net, e.g. "reg q;" or "wire [7:0] z;" or "input clk;"
  void composeDeclaration(UtString*) const;

protected:
  //! Helper function for print, prints the name and size information
  virtual void printNameSize() const;

private:
  //! Hide copy and assign constructors.
  NUExtNet(const NUExtNet&);
  NUExtNet& operator=(const NUExtNet&);
} APIOMITTEDCLASS; //class NUExtNet : public NUVirtualNet


#endif // NUEXTNET_H_
