// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _NUINSTANCEWALKER_H_
#define _NUINSTANCEWALKER_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUDesignWalker.h"

class STSymbolTable;
class STBranchNode;

//! A class to walk the design looking for all module instances.
/*! There are two types of module instances. The first is the top
 *  modules which don't have an NUModuleInstance. The second is all
 *  the NUModuleInstances. This walk is similar to elaboration in that
 *  it finds all instances of a module and can provide a path to that
 *  instance from the top.
 *
 *  This class is a base class for module instance walks. The abstract
 *  virtual function must be overriden in the derived class.
*/
class NUInstanceCallback : public NUDesignCallback
{
private:
  //! Abstraction for a stack of symbol table nodes
  typedef UtVector<STBranchNode*> BranchStack;

public:
  //! constructor
  NUInstanceCallback(STSymbolTable* symTab, bool inc_named_decl_scope = false);

  //! destructor
  ~NUInstanceCallback();

  //! By default, skip everything.
  Status operator()(Phase, NUBase *) { return NUDesignCallback::eSkip; }

  //! Walk through the design.
  Status operator()(Phase, NUDesign *) { return NUDesignCallback::eNormal; }

  //! Module callback (we only care about top modules here)
  NUDesignCallback::Status operator()(Phase phase, NUModule* mod);

  //! Module instance callback
  NUDesignCallback::Status operator()(Phase phase, NUModuleInstance* inst);

  //! Named declaration scope callback
  NUDesignCallback::Status operator()(Phase phase, NUNamedDeclarationScope* declScope);

protected:
  //! Function to be called on every module 
  virtual void handleModule(STBranchNode* branch, NUModule* module) = 0;

  //! Function to be called on every named declaration scope
  virtual void handleDeclScope(STBranchNode* branch, NUNamedDeclarationScope* declScope) = 0;

  //! Function to be called on every module instance
  virtual void handleInstance(STBranchNode* branch, NUModuleInstance* instance) = 0;

  //! Access function for the symbol table
  STSymbolTable* getSymtab() const { return mSymTab; }

  //! Access function for the top node on the stack
  STBranchNode* getCurrentNode() const;

private:
  // Local data
  STSymbolTable* mSymTab;
  BranchStack* mBranchStack;
  bool         mIncludeNamedDeclScope; // This flag is used in NUInstanceWalker.
                                       // If this flag is true the walker visits 
                                       // named blocks and generate blocks.
}; // class NUInstanceCallback : public NUDesignCallback


#endif // _NUINSTANCEWALKER_H_
