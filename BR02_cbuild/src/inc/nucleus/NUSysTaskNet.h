// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#ifndef NUSYSTASKNET_H_
#define NUSYSTASKNET_H_

#ifndef NUVIRTUALNET_H_
#include "nucleus/NUVirtualNet.h"
#endif


//! NUSysTaskNet class
/*!
 * A artificial net used to represent the "output" of system tasks
 * like $display and $write
 */
class NUSysTaskNet : public NUVirtualNet
{
public:
  //! Constructor
  /*!
    \param name The name to be used for the net
    \param flags Attribute flags
    \param scope Scope in which net is declared
    \param loc Source location of declaration of the net
   */
  NUSysTaskNet(StringAtom *name,
               NetFlags flags,
               NUScope *scope,
               const SourceLocator& loc);

  //! Destructor
  virtual ~NUSysTaskNet();

  //! Return class name
  const char* typeStr() const;

  // Compose a declaration string for the net, e.g. "reg q;" or "wire [7:0] z;" or "input clk;"
  void composeDeclaration(UtString*) const;

  //! Helper function for print, prints the name and size information
  void printNameSize() const;

  //! return true because a NUSysTaskNet is always observable
  bool isProtectedObservable(const IODBNucleus* /* iodb */ ) const { return true; };

  //! Is this a system task net?
  virtual bool isSysTaskNet() const;

private:
  //! Hide copy and assign constructors.
  NUSysTaskNet(const NUSysTaskNet&);
  NUSysTaskNet& operator=(const NUSysTaskNet&);

}  APIOMITTEDCLASS ; //class NUSysTaskNet : public NUVirtualNet

#endif // NUSYSTASKNET_H_
