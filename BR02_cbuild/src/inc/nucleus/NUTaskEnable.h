// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUTASKENABLE_H_
#define NUTASKENABLE_H_

#include "nucleus/NUStmt.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUDesignWalker.h"

/*!
  \file
  Declaration of the Nucleus task enable class.
*/

//! NUTaskEnable class
/*!
 * A task enable
 */
class NUTaskEnable : public NUStmt
{
public:
  //! constructor
  /*!
    \param task The task being called
    \param parentModule The module containing the task enable (not the task)
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param usesCFNet true when this needs $cfnet in it's use list
    \param loc The source location of the call
   */
  NUTaskEnable(NUTask *task,
               NUModule* parentModule,
	       NUNetRefFactory *netref_factory,
               bool usesCFNet,
	       const SourceLocator& loc);

  //! Return the task being called
  virtual NUTask* getTask() const { return mTask; }

  //! Return the type of this class
  virtual NUType getType() const { return eNUTaskEnable; }

  //! Set the arg connections for this task enable
  void setArgConnections(const NUTFArgConnectionVector& connections);

  //! Add the arg connection to the end of the connections.
  void addArgConnection(NUTFArgConnection *conn);

  //! Get the vector of arg connections for this task enable
  void getArgConnections(NUTFArgConnectionVector& connections) const;

  //! Loop over the arg connections
  NUTFArgConnectionCLoop loopArgConnections() const { return NUTFArgConnectionCLoop(mArgs); }

  //! Loop over the arg connections
  NUTFArgConnectionLoop loopArgConnections() { return NUTFArgConnectionLoop(mArgs); }

  //! Return the arg connection for the given 0-based port index.
  NUTFArgConnection* getArgConnectionByIndex(int idx) const;

  //! Code Generator
  CGContext_t emitCode(CGContext_t) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! Return class name
  const char* typeStr() const;

  //! Return a copy of this statement
  NUStmt* copy(CopyContext &copy_context) const;

  //! Add the use_net as being used by the blocking def of def_net.
  void addBlockingUse(const NUNetRefHdl &def_net_ref,
		      const NUNetRefHdl &use_net_ref);

  //! Add the uses set as being used by the blocking def of def_net.
  void addBlockingUses(NUNet *def_net, NUNetSet *uses);
  void addBlockingUses(const NUNetRefHdl &def_net_ref, NUNetRefSet *uses);

  //! Add the net to the set of nets this stmt blockingly-defs.
  void addBlockingDef(const NUNetRefHdl &net_ref);

  //! Add the nets this node uses for blocking defs to the given set.
  void getBlockingUses(NUNetSet *uses) const;
  void getBlockingUses(NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to blockingly def the given net to the given set.
  void getBlockingUses(NUNet *def_net, NUNetSet *uses) const;
  void getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &def_net_ref,
			 const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! No-op
  void getNonBlockingUses(NUNetSet * /* uses -- unused */) const {}
  void getNonBlockingUses(NUNetRefSet * /* uses */) const {}
  bool queryNonBlockingUses(const NUNetRefHdl & /* use_net_ref -- unused */,
			    NUNetRefCompareFunction /* fn -- unused */,
			    NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! No-op
  void getNonBlockingUses(NUNet * /* def_net -- unused */, NUNetSet * /* uses -- unused */) const {}
  void getNonBlockingUses(const NUNetRefHdl & /* net_ref */, NUNetRefSet * /* uses */) const {}
  bool queryNonBlockingUses(const NUNetRefHdl & /* def_net_ref -- unused */,
			    const NUNetRefHdl & /* use_net_ref -- unused */,
			    NUNetRefCompareFunction /* fn -- unused */,
			    NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this node blockingly defines to the given set.
  void getBlockingDefs(NUNetSet *defs) const;
  void getBlockingDefs(NUNetRefSet *defs) const;
  bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! No-op
  void getNonBlockingDefs(NUNetSet * /* uses -- unused */) const {}
  void getNonBlockingDefs(NUNetRefSet * /* defs */) const {}
  bool queryNonBlockingDefs(const NUNetRefHdl & /* def_net_ref -- unused */,
			    NUNetRefCompareFunction /* fn -- unused */,
			    NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this node blockingly kills to the given set.
  void getBlockingKills(NUNetSet *kills) const;
  void getBlockingKills(NUNetRefSet *kills) const;
  bool queryBlockingKills(const NUNetRefHdl &def_net_ref,
			  NUNetRefCompareFunction fn,
			  NUNetRefFactory *factory) const;

  //! No-op
  void getNonBlockingKills(NUNetSet * /* kills -- unused */) const {}
  void getNonBlockingKills(NUNetRefSet * /* kills */) const {}
  bool queryNonBlockingKills(const NUNetRefHdl & /* def_net_ref -- unused */,
			     NUNetRefCompareFunction /* fn -- unused */,
			     NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Clear the use/def information
  void clearUseDef();

  //! Fixup the use/def information; remove all nets in the given list from the sets.
  void fixupUseDef(NUNetList *remove_list);

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! Return the NUTaskHierRef if this is a hierref, 0 otherwise.
  virtual NUTaskHierRef *getHierRef() { return 0; }
  virtual const NUTaskHierRef *getHierRef() const { return 0; }

  //! Return the net ref factory
  NUNetRefFactory *getNetRefFactory() const { return mNetRefFactory; }

  //! destructor
  ~NUTaskEnable();

  //! create in HDL form
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Replace the task and fix up arg connections
  /*! If connectionSrc is specified, look to that task-enable for the TFArgs to
   *! transform for this
   */
  void replaceTask(NUTask* newTask, CopyContext& cc, NUTaskEnable* connectionSrc = 0);

  //! Get the parent module for this use def node
  const NUModule* findParentModule() const;
  NUModule* findParentModule();

protected:
  //! Helper function for print, prints some identifying information
  virtual void printInfo() const;

  //! The task being called
  NUTask *mTask;

  //! Argument connections
  NUTFArgConnectionVector mArgs;

  //! Map of arg defs to their "use"s; uses net refs
  NUNetRefNetRefMultiMap mNetRefArgDefMap APIOMITTEDFIELD;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory APIOMITTEDFIELD;

  //! module in which NUTaskEnable resides
  NUModule* mParentModule;

private:
  //! Hide copy and assign constructors.
  NUTaskEnable(const NUTaskEnable&);
  NUTaskEnable& operator=(const NUTaskEnable&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUTaskEnable


//! Hierarchical task enable
class NUTaskEnableHierRef : public NUTaskEnable
{
public:
  //! constructor
  NUTaskEnableHierRef(const AtomArray& path,
                      StringAtom *taskName,
                      NUScope *scope,
		      NUNetRefFactory *netref_factory,
                      bool usesCFNet,
		      const SourceLocator &loc);
  ~NUTaskEnableHierRef();

  //! Return true if this enable represents a cross-hierarchicy reference
  bool isHierRef() const { return true; }

  //! Return the task being called.
  /*!
   * An assertion is triggered if there is more than one resolution.
   */
  NUTask * getTask() const;

  //! Return one of the potential resolutions as representative.
  /*!
   * If this task enable has multiple resolutions but you just want
   * one to treat as a representative, this method will choose one
   * potential resolution and return it. This is useful for
   * use/def-type tasks.
   *
   * Generally, you want to get the unique elaborated resolution. See
   * NUTaskHierRef::isLocallyRelative and related methods.
   *
   * If you want all resolutions, see NUTaskHierRef::loopResolutions.
   *
   * \return One (of potentially) resolution task.
   */
  NUTask * getRepresentativeResolution() const;

  //! Return the module in which this hierref appears.
  NUModule *getModule() const { return mModule; }

  //! Return the NUTaskHierRef for this hierref.
  NUTaskHierRef *getHierRef() { return &mTaskHierRef; }
  const NUTaskHierRef *getHierRef() const { return &mTaskHierRef; }

  //! Return a copy of this statement
  NUStmt* copy(CopyContext &copy_context) const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Add the non-task-local def'd nets to the passed-in set.
  /*!
   * Tasks may def non-task-local nets.  Collect them and localize them
   * with respect to where this hierarchical ref occurs.
   */
  void getNonLocalDefs(NUNetSet *defs);

  //! Add the non-task-local used nets to the passed-in set.
  /*!
   * Tasks may use non-task-local nets.  Collect them and localize them
   * with respect to where this hierarchical ref occurs.
   */
  void getNonLocalUses(NUNetSet *uses);

protected:
  //! Helper function for print, prints some identifying information
  void printInfo() const;

private:
  //! Hide copy and assign constructors.
  NUTaskEnableHierRef(const NUTaskEnableHierRef&);
  NUTaskEnableHierRef& operator=(const NUTaskEnableHierRef&);

  //! Hierref resolution object.
  NUTaskHierRef mTaskHierRef;

  //! Module in which task enable appears
  NUModule * mModule;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUTaskEnableHierRef : public NUTaskEnable


//! Callback to find task enables in a Nucleus tree
class NUTaskEnableCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  NUTaskEnableCallback(NUTaskEnableVector* task_enables) :
    mTaskEnables(task_enables), mCovered(new NUTaskEnableSet)
  {}
  ~NUTaskEnableCallback() { delete mCovered; }

  //! For everything unknown, keep going
  Status operator()(Phase, NUBase*) { return eNormal; }

  //! Don't visit sub intances
  Status operator()(Phase, NUModuleInstance*) { return eSkip; }

  //! For task enables, process the data
  Status operator()(Phase phase, NUTaskEnable* task_enable)
  {
    // We only care about hier refs to tasks in other modules
    if ((phase == ePre) && task_enable->isHierRef())
    {
      if (mCovered->find(task_enable) == mCovered->end())
      {
        mTaskEnables->push_back(task_enable);
        mCovered->insert(task_enable);
      }
    }

    // Can we have any task enables inside task enables?
    return eNormal;
  }
private:
  NUTaskEnableVector* mTaskEnables;
  NUTaskEnableSet* mCovered;
}; // class NUTaskEnableCallback : public NUDesignCallback

#endif
