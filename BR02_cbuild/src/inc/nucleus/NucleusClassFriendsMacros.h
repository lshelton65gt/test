// -*-C++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __NUCLEUSCLASSFRIENDSMACROS_H__
#define __NUCLEUSCLASSFRIENDSMACROS_H__

//This files is used to declare friends of distilled nucleus classes



//Forward declare of the friendly classes at global scope

class xNucleusConverter;
class NucleusXmlWriter;


//Use this macro to decare the friends of a class

#define DECLARENUCLEUSCLASSFRIENDS()    \
private: \
  friend class xNucleusConverter;       \
  friend class  NucleusXmlWriter;       \







#endif
