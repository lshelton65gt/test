// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUCOMPOSITENET_H_
#define NUCOMPOSITENET_H_

#include "nucleus/NUNet.h"

//! NUCompositeNet class
/*!
 * A net which is a record or struct
 */
class NUCompositeNet : public NUNet
{
public:
    //! Constructor to use when a composite net is declared in a module
    /*!
      \param name Name of the net
      \param flags Attribute flags
      \param fields Array of child fields contained in the net. The elements of array will be owned by constructed class.
      \param ranges Vector of index ranges in the unpacked dimensions if any.
      \param packed Boolean flag indicated the net was declared packed
      \param scope Scope in which net is declared
      \param loc Source location of declaration of the net
     */

  NUCompositeNet( const StringAtom *typeMark,
                  const NUNetVector& fields,
                  const ConstantRangeVector& ranges,
                  bool packed,
                  NUScope *scope,
                  StringAtom *name,
                  const SourceLocator& loc,
                  NetFlags flags);

  // Steve Lim 2013-11-23: Added a variant of the constructor that takes a
  // ConstantRangeArray ranges from which it will construct a ConstantRangeVector.
  // This facilitates the common use of VerificVhdlDesignWalker::extractArrayDimensions
  // implemented for processing of multi-dimensional arrays targetting a NUMemoryNet
  // which takes a ConstantRangeArray for its index ranges.
  NUCompositeNet( const StringAtom *typeMark,
                  const NUNetVector& fields,
                  ConstantRangeArray& ranges,
                  bool packed,
                  NUScope *scope,
                  StringAtom *name,
                  const SourceLocator& loc,
                  NetFlags flags);

  //! destructor
  virtual ~NUCompositeNet();

  //! Return the declared total bit size.
  /*! This will return 0 if the calculated size exceeds 2^32.  This zero
   *  value must be detected by population, to ensure that it never
   *  propagates into the backend of cbuild.
   */
  virtual UInt32 getBitSize() const;

  //! This makes no sense for a composite net; always false
  virtual bool containsRange( const ConstantRange& ) const 
  { return false; }

  //! This makes no sense for a composite net; always false
  virtual bool containsBit( SInt32 ) const 
  { return false; }

  /*! \brief Return the number of fields in the composite net
   */
  UInt32 getNumFields () const { return mFields.size(); }

  //! Return the Nth field net pointer
  NUNet *getField( UInt32 fieldIndex ) const;

  //! Set the Nth field net pointer.  Does no memory management.
  void putField( NUNet *new_net, UInt32 fieldIndex );

  //! Overload from NUNet
  virtual NUCompositeNet *getCompositeNet() { return this; }

  //! Get the size of this composite.  
  UInt32 getCompositeSize() const { return getBitSize(); }

  //! Get the number of bits this composite net needs in a netref.
  /*! The number of bits in the netref is not necessarily the number of
   *  bits in the composite net.  In this case we used the number of fields.
   */
  virtual UInt32 getNetRefWidth() const { return getNumFields(); }

  //! Return true, this is a composite net (much faster than (NULL !=
  //dynamic_cast<NUCompositeNet*>(net)))
  bool isCompositeNet() const { return true; }

  //! Return class name
  const char* typeStr() const;

  //! Return the original name of composite net.
  StringAtom* getOriginalName() const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const
  { NU_ASSERT( false, this ); }

  //! Was this net declared as a packed struct?
  bool isPacked() const { return mPacked; }

  //! Has this met been through composite net resynthesis?
  bool isResynthesized() const { return mResynthesized; }

  //! set the resynthesis flag
  void putResynthesized( bool resynth ) { mResynthesized = resynth; };

  virtual NUNet* createHierRef(const AtomArray&, NUScope*, NetFlags,
                               const SourceLocator&);

  // Compose a declaration string for the net, e.g. "reg q;" or "wire
  // [7:0] z;" or "input clk;"
  void composeDeclaration(UtString*) const;

  //! Compose a declaration prefix for a composite net.  This is the
  //part of the declaration that precedes the net name.
  virtual void composeDeclarePrefix(UtString*) const;
  //! Compose a declaration suffix for a composite net.  This is the
  //part of the declaration that follows the net name, containing the
  //dimensions of the composite net.
  virtual void composeDeclareSuffix(UtString*) const;

  const StringAtom *getTypeMark() const { return mTypeMark; }

  //! Fill in the argument with an expanded array of NUIdentRvalues, in
  //order, for the CompositeNet, dealing with nested composites if
  //necessary.  This will allocate new NUExprs for each composite field.
  void createExprVector( NUExprVector* exprVector ) const;
  //! Fill in the argument with an expanded array of NUIdentLvalues, in
  //order, for the CompositeNet, dealing with nested composites if
  //necessary.  This will allocate new NULvalues for each composite field.
  void createLvalueVector( NULvalueVector* lvalueVector ) const;

  //! Fill in the argument with an expanded array of NUNets, in order
  //for the CompositeNet, dealing with nested composites if necessary.
  //No memory will be allocated.
  void createNetVector( NUNetVector *netVector ) const;

  //! Get the number of declared dimensions
  virtual UInt32 getNumDims() const { return mRanges.size(); }

  /* \brief Returns the nth array range
   */
  virtual const ConstantRange* getRange( const UInt32 index ) const;

  //! Get ranges of this net.
  const ConstantRangeVector *getRanges() { return &mRanges; }

  //! Clear all ranges of this net.
  void clearRanges() { mRanges.clear(); }

  //! Given a selection expression, normalize it for range size-1:0
  NUExpr* normalizeSelectExpr(NUExpr* sel, UInt32 dim, SInt32 adjust,
                              bool retNullForOutOfRange);

  //! get the index (into mRanges) of the first unpacked dimension, all ranges at indices below this number are packed dimensions
  UInt32 getFirstUnpackedDimensionIndex() const {return mFirstUnpackedDimensionIndex;}

protected:
  //! Helper function for print, prints the name and size information
  void printNameSize() const;
  //! Get the named declaration scope corresponding to this composite.
  NUScope* getDeclScopeForComposite() const;

private:
  //! Hide copy and assign constructors.
  NUCompositeNet(const NUCompositeNet&);
  NUCompositeNet& operator=(const NUCompositeNet&);

  const StringAtom *mTypeMark;

  //! The vector of record fields
  NUNetVector mFields;

  //! The vector of declared array ranges.  Index 0 is the innermost
  //range; N-1 is the outer range.  This matches memories.
  ConstantRangeVector mRanges;

  //! holds the index (into mRanges) of the first dimension that is unpacked.  
  /*!
   * For NUCompositeNet this index is currently ALWAYS 0 as there is no packed dimension.  
   * Do not confuse this with the isPacked() function that indicates if the struct was declared as packed
   * \sa mRanges
   */ 
  UInt32 mFirstUnpackedDimensionIndex;


  //! packed struct flag
  bool mPacked;

  //! Has this net been through resynthesis?
  bool mResynthesized APIOMITTEDFIELD;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCompositeNet : public NUNet
#endif
