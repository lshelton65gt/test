// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUCLEUS_H_
#define NUCLEUS_H_

#include "util/CarbonTypes.h"
#include "nucleus/NUBase.h"
#include "util/Iter.h"
#include "util/Loop.h"
#include "util/LoopFilter.h"
#include "util/LoopSorted.h"
#include "util/LoopMulti.h"

#include "util/UtList.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtMap.h"
#include "util/UtMultiMap.h"
#include "util/UtSet.h"
#include "util/UtStack.h"
#include "util/UtVector.h"
#include "util/UtString.h"
#include "nucleus/NucleusClassFriendsMacros.h"

// please do not use "using namespace std"

/*!
  \file
  Forward declarations of Nucleus classes and convenience typedefs for users
  of Nucleus.
  This file exists for two reasons:
  1 - If you are simply passing Nucleus objects around, just include this file and
      you will be all set.
  2 - Some classes depend on forward declarations of other classes.  This file
      helps with the circularity.
*/

class SourceLocator;
class NUAlwaysBlock;
class NUAssign;
class NUAttribute;
class NUBinaryOp;
class NUBitNet;
class NUBlock;
class NUBlockingAssign;
class NUBlockingEnabledDriver;
class NUBlockScope;
class NUBlockStmt;
class NUBreak;
class NUCase;
class NUCaseItem;
class NUCaseCondition;
class NUChangeDetect;
class NUCModelInterface;
class NUCModelFn;
class NUCModelCall;
class NUCModelPort;
class NUCModelArgConnection;
class NUCModelArgConnectionInput;
class NUCModelArgConnectionOutput;
class NUCModelArgConnectionBid;
class NUCModel;
class NUConcatOp;
class NUConcatLvalue;
class NUConst;
class NUContAssign;
class NUContEnabledDriver;
class NUControlFlowNet;
class NUControlSysTask;
class NUCopyContext;
class NUCost;
class NUCostContext;
class NUDesign;
class NUEdgeExpr;
class NUEnabledDriver;
class NUExpr;
class NUExprFactory;
class NUExtNet;
class NUFCloseSysTask;
class NUFFlushSysTask;
class NUFOpenSysTask;
class NUIdentLvalue;
class NUIdentRvalue;
class NUIf;
class NUFor;
class NUHierRef;
class NUInitialBlock;
class NUInputSysTask;
class NUInstanceLevels;
class NULut;
class NULvalue;
class NUMemoryNet;
class NUMemselLvalue;
class NUMemselRvalue;
class NUTempMemoryNet;
class NUModule;
struct NUModuleCmp;
class NUModuleElab;
class NUModuleInstance;
class NUNamedDeclarationScope;
class NUNamedDeclarationScopeElab;
class NUNaryOp;
class NUNet;
class NUNetHierRef;
class NUNetElab;
class NUNetElabRefSet2;
class NUExtRefNet;
class NUNetFilter;
class NUNetRef;
class NUNullExpr;
class NUNetRefHdl;
class NUNetRefFactory;
class NUNetRefNetRefMultiMap;
class NUNetRefSet;
class NUNonBlockingAssign;
class NUOp;
class NUOutputSysTask;
class NUPortConnection;
class NUPortConnectionBid;
class NUPortConnectionInput;
class NUPortConnectionOutput;
class NUReadmemX;
class NUScope;
class NUScopeElab;
class NUSeparatedBlock;
class NUStmt;
class NUStructuredProc;
class NUSysFunctionCall;
class NUSysRandom;
class NUSysTask;
class NUSysTaskNet;
class NUTask;
class NUTaskEnable;
class NUTaskEnableHierRef;
class NUTaskLevels;
class NUTernaryOp;
class NUTF;
class NUTFElab;
class NUTFArgConnection;
class NUTFArgConnectionInput;
class NUTFArgConnectionOutput;
class NUTFArgConnectionBid;
class NUTriRegInit;
class NUUnaryOp;
class NUUseDefNode;
class NUUseDefStmtNode;
class NUUseNode;
class NUVectorNet;
class NUCycle;
class NUVarselRvalue;
class NUVarselLvalue;
class NUVirtualNet;
class NUCompositeNet;
class NUCompositeSelExpr;
class NUCompositeExpr;
class NUCompositeFieldExpr;
class NUCompositeSelLvalue;
class NUCompositeLvalue;
class NUCompositeFieldLvalue;
class NUCompositeIdentLvalue;
class NUCompositeIdentRvalue;
class NUBitNetHierRef;
class NUCompositeInterfaceExpr;
class NUCompositeInterfaceLvalue;
class NUCompositeNetHierRef;
class NUConstNoXZ;
class NUConstReal;
class NUConstXZ;
class NUMemoryNetHierRef;
class NUTaskHierRef;
class NUVectorNetHierRef;


typedef UtHashSet<NUNet*, HashHelper<NUNet*>, HashMgr<NUNet*> > NUNetSet;
typedef Loop<NUNetSet> NUNetSetLoop;
typedef UtHashSet<const NUNet*, HashHelper<const NUNet*>,
                  HashMgr<const NUNet*> > NUConstNetSet;
typedef UtHashSet<NUAssign*, HashHelper<NUAssign*>,
                  HashMgr<NUAssign*> > NUAssignSet;

//! NUNetFilter class
/*!
 * Class to filter out nets according to flag matches so that a consistent iteration
 * interface can be presented to nucleus users.
 */
class NUNetFilter
{
public:
  NUNetFilter() : mFlags(NetFlags(0)) {}
  NUNetFilter(NetFlags flags) : mFlags(flags) {}

  bool operator() (NUNet *net);

private:
  NetFlags mFlags;
};

// Include the Nucleus translator after type predeclarations.
#include "nucleus/NuToNu.h"

struct NUNetHashByName;

//! Typedef for any of the comparison with other net ref functions
typedef bool (NUNetRef::* NUNetRefCompareFunction)(const NUNetRef &other) const;

typedef UtMap<NUNet *, NUNet *> NUNetReplacementMapNonConst;
typedef UtMap<const NUNet *, NUNet *> NUNetReplacementMap;
typedef UtMap<const NUTF *, NUTF *> NUTFReplacementMap;
typedef UtMap<const NUNet *, NULvalue*> NULvalueReplacementMap;
typedef UtMap<const NUNet *, NUExpr*> NUExprReplacementMap;
typedef UtMap<const NUCModel *, NUCModel *> NUCModelReplacementMap;
typedef UtMap<const NUAlwaysBlock *, NUAlwaysBlock *> NUAlwaysBlockReplacementMap;

typedef UtHashMap<const NUExpr*, const NUExpr*> NUExprExprHashMap;
typedef UtHashMap<const NUExpr*, const NUExpr*,
                  HashPointerValue<const NUExpr*> > NUCExprCExprDeepHashMap;
typedef UtHashMap<NUExpr*, NUExpr*,
                  HashPointerValue<NUExpr*> > NUExprExprDeepHashMap;

typedef UtHashMap<NULvalue*, NULvalue*,
                  HashPointerValue<NULvalue*> > NULvalLvalDeepHashMap;
typedef UtHashMap<NUExpr*, NUNet*,
                  HashPointerValue<NUExpr*> > NUEdgeNetMap;

typedef UtVector<NUBase*> NUBaseVector;
typedef NUBaseVector::iterator NUBaseVectorNonconstIter;
typedef NUBaseVector::const_iterator NUBaseVectorIter;
typedef Loop<NUBaseVector> NUBaseVectorLoop;
typedef CLoop<NUBaseVector> NUBaseVectorCLoop;
typedef UtHashSet<NUBase*> NUBaseSet;
typedef UtHashSet<NUCycle*> NUCycleSet;
typedef UtList<NUAlwaysBlock*> NUAlwaysBlockList;
typedef UtList<NUAlwaysBlock*>::const_iterator NUAlwaysBlockListIter;
typedef UtList<NUAlwaysBlock*>::iterator NUAlwaysBlockListNonconstIter;
typedef UtSet<NUAlwaysBlock*> NUAlwaysBlockSet;
typedef UtVector<NUAlwaysBlock*> NUAlwaysBlockVector;
typedef UtMap<NUAlwaysBlock*, NUNetRefSet> NUAlwaysBlockNetRefSetMap;
typedef UtList<NUAssign*> NUAssignList;
typedef Loop<NUAlwaysBlockVector> NUAlwaysBlockVectorLoop;
typedef CLoop<NUAlwaysBlockVector> NUAlwaysBlockVectorCLoop;
typedef Loop<NUAlwaysBlockSet> NUAlwaysBlockSetLoop;

// NUBlock types
typedef UtList<NUBlock*> NUBlockList;
typedef CLoop<NUBlockList> NUBlockCLoop;
typedef Loop<NUBlockList> NUBlockLoop;
typedef UtSet<NUBlock*> NUBlockSet;

typedef UtList<NUContAssign*> NUContAssignList;
typedef UtList<NUContAssign*>::const_iterator NUContAssignListIter;
typedef UtList<NUContAssign*>::iterator NUContAssignListNonconstIter;
typedef UtVector<NUContAssign*> NUContAssignVector;
typedef UtList<NUEdgeExpr*> NUEdgeExprList;
typedef UtList<NUEdgeExpr*>::const_iterator NUEdgeExprListIter;
typedef UtList<NUEdgeExpr*>::iterator NUEdgeExprListNonconstIter;
typedef UtSet<NUEdgeExpr*> NUEdgeExprSet;
typedef NUEdgeExprSet::const_iterator NUEdgeExprSetIter;
typedef NUEdgeExprSet::iterator NUEdgeExprSetNonconstIter;
typedef UtList<NUEnabledDriver*> NUEnabledDriverList;
typedef UtList<NUEnabledDriver*>::const_iterator NUEnabledDriverListIter;
typedef UtList<NUEnabledDriver*>::iterator NUEnabledDriverListNonconstIter;
typedef UtList<NUInitialBlock*> NUInitialBlockList;
typedef UtList<NUInitialBlock*>::const_iterator NUInitialBlockListIter;
typedef UtList<NUInitialBlock*>::iterator NUInitialBlockListNonconstIter;
typedef UtHashSet<NUEnabledDriver*> NUEnabledDriverSet;
typedef UtSet<NULvalue*> NULvalueSet;
typedef UtVector<NULvalue*> NULvalueVector;
typedef UtVector<NULvalue*>::const_iterator NULvalueVectorIter;
typedef UtVector<NULvalue*>::iterator NULvalueVectorNonconstIter;
typedef Loop<NULvalueVector> NULvalueLoop;
typedef CLoop<NULvalueVector> NULvalueCLoop;
typedef UtMap<const NULvalue*,NULvalue*> NULvalueLvalueMap;
typedef Iter<NUModule*> NUModuleIter;
typedef UtList<NUModule*> NUModuleList;
typedef UtHashSet<NUModule*> NUModuleSet;
typedef NUModuleSet::const_iterator NUModuleSetIter;
typedef NUModuleSet::iterator NUModuleSetNonconstIter;
typedef UtHashMap<NUModule*,NUModule*> NUModuleMap;
typedef UtList<NUModule*>::const_iterator NUModuleListIter;
typedef UtList<NUModuleInstance*> NUModuleInstanceList;
typedef UtVector<NUModuleInstance*> NUModuleInstanceVector;
typedef Loop<NUModuleInstanceVector> NUModuleInstanceVectorLoop;
typedef UtList<NUModuleInstance*>::const_iterator NUModuleInstanceListIter;
typedef UtList<NUModuleInstance*>::iterator NUModuleInstanceListNonconstIter;
typedef Loop<NUModuleInstanceList> NUModuleInstanceListLoop;
typedef CLoop<NUModuleInstanceList> NUModuleInstanceListCLoop;
typedef LoopMulti<NUModuleInstanceListLoop> NUModuleInstanceMultiLoop;
typedef LoopMulti<NUModuleInstanceListCLoop> NUModuleInstanceMultiCLoop;
typedef UtSet<NUModuleInstance*> NUModuleInstanceSet;
typedef UtVector<NUModule*> NUModuleVector;
typedef UtVector<NUCModelPort*> NUCModelPortVector; 
typedef Loop<NUModuleVector> NUModuleVectorLoop;


// NUNamedDeclarationScope types.
typedef UtList<NUNamedDeclarationScope*> NUNamedDeclarationScopeList;
typedef CLoop<NUNamedDeclarationScopeList> NUNamedDeclarationScopeCLoop;
typedef Loop<NUNamedDeclarationScopeList> NUNamedDeclarationScopeLoop;

typedef Iter<NUNet*> NUNetIter;
typedef UtList<NUNet*> NUNetList;
typedef UtVector<NUNet*> NUNetVector;
typedef UtVector<NUNetElab*> NUNetElabVector;
typedef Loop<NUNetElabVector> NUNetElabVectorLoop;
typedef UtVector<NUNet*>::const_iterator NUNetVectorIter;
typedef UtVector<NUNet*>::iterator NUNetVectorNonconstIter;
typedef UtVector<const NUNet*> NUNetCVector;
typedef Loop<NUNetVector> NUNetVectorLoop;
typedef CLoop<NUNetVector> NUNetVectorCLoop;
typedef LoopFilter<NUNetVectorLoop, NUNetFilter> NUNetFilterLoop;
typedef LoopFilter<NUNetVectorCLoop, NUNetFilter> NUNetFilterCLoop;
typedef UtList<NUNet*>::const_iterator NUNetListIter;
typedef UtList<NUNet*>::iterator NUNetListNonconstIter;
typedef UtList<NUNetElab*> NUNetElabList;
typedef UtList<NUNetElab*>::const_iterator NUNetElabListIter;
typedef UtList<NUNetElab*>::iterator NUNetElabListNonconstIter;
typedef Iter<NUNetElab*> NUNetElabIter;
typedef UtHashSet<NUNetElab*> NUNetElabSet;
typedef UtHashSet<const NUNetElab*> NUCNetElabSet;
typedef UtHashSet<const NUNet*> NUCNetSet;
typedef Loop<NUNetElabSet> NUNetElabLoop;
typedef CLoop<NUNetElabSet> NUNetElabCLoop;
typedef Loop<NUCNetElabSet> NUCNetElabLoop;
typedef NUNetElabSet::const_iterator NUNetElabSetIter;
typedef NUNetElabSet::iterator NUNetElabSetNonconstIter;
typedef UtMultiMap<NUNetElab*,NUNetElab*> NUNetElabMultiMap;
typedef UtHashMap<NUNetElab*,NUNetElab*> NUNetElabMap;
typedef UtMap<const NUNetElab*,NUNetElab*> NUCNetElabMap;
// NUNetSet is defined in NUNetSet.h which is created by the makefile,
//   look for it in $CARBON_HOME/obj/.../nucleus/NUNetSet.h
//typedef UtSet<NUNet*> NUNetSet;
typedef UtHashSet<NUNet*, NUNetHashByName> NUNetSetByName;

typedef UtMap<NUNetElab*, NUNetRefHdl> NUNetElabBitRefs;

typedef Loop<NUNetList> NUNetLoop;
typedef CLoop<NUNetList> NUNetCLoop;
typedef LoopSorted<NUNet*> NUNetLoopSorted;
#define NUNetSetIter NUNetSet::const_iterator
#define NUNetSetNonconstIter NUNetSet::iterator
typedef UtHashMap<NUNet*,NUNetSet*> NUNetSetMap;
typedef UtHashMap<NUNet*,NUNetSet*>::const_iterator NUNetSetMapIter;
typedef UtHashMap<NUNet*,NUNetSet*>::iterator NUNetSetMapNonconstIter;
typedef UtVector<NUPortConnection*> NUPortConnectionVector;
typedef NUPortConnectionVector::const_iterator NUPortConnectionVectorIter;
typedef CLoop<NUPortConnectionVector> NUPortConnectionCLoop;
typedef Loop<NUPortConnectionVector> NUPortConnectionLoop;
typedef UtList<NUPortConnection*> NUPortConnectionList;
typedef UtList<NUPortConnection*>::const_iterator NUPortConnectionListIter;
typedef UtList<NUPortConnectionInput*> NUPortConnectionInputList;
typedef UtList<NUPortConnectionInput*>::const_iterator NUPortConnectionInputListIter;
typedef UtList<NUPortConnectionOutput*> NUPortConnectionOutputList;
typedef UtList<NUPortConnectionOutput*>::const_iterator NUPortConnectionOutputListIter;
typedef UtList<NUPortConnectionBid*> NUPortConnectionBidList;
typedef UtList<NUPortConnectionBid*>::const_iterator NUPortConnectionBidListIter;
typedef UtMap<NUNet*,NUPortConnection*>  NUPortConnectionMap;
typedef UtHashSet<NUStmt*> NUStmtSet;
typedef UtMap<NUStmt*,NUStmt*> NUStmtStmtMap;
typedef UtList<NUStmt*> NUStmtList;
typedef UtList<NUStmt*>::const_iterator NUStmtListIter;
//! abstraction for looping through statements
typedef Loop<NUStmtList> NUStmtLoop;
typedef CLoop<NUStmtList> NUStmtCLoop;
typedef UtVector<NUStmt*> NUStmtVector;
typedef UtList<NUStmt*>::iterator NUStmtListNonconstIter;
typedef UtList<NUStructuredProc*> NUStructuredProcList;
typedef UtList<NUStructuredProc*>::iterator NUStructuredProcListIter;
typedef UtSet<NUExpr*> NUExprSet;
typedef UtHashSet<const NUExpr*> NUCExprHashSet;
typedef UtList<NUExpr*> NUExprList;
typedef std::pair<NUExpr*,SourceLocator> NUExprLocPair;
typedef UtList<NUExprLocPair> NUExprLocList; // use this instead of NUExprList when you need a more accurate loc for each expr
typedef UtList<NUExpr*>::const_iterator NUExprListIter;
typedef UtList<NUExprLocPair>::const_iterator NUExprLocListIter;
typedef UtList<NUExpr*>::iterator NUExprListNonconstIter;
typedef UtVector<const NUExpr*> NUCExprVector;
typedef UtVector<NUExpr*> NUExprVector;
typedef UtVector<NUExpr*>::const_iterator NUExprVectorIter;
typedef UtVector<NUExpr*>::iterator NUExprVectorNonconstIter;
typedef UtVector<const NUExpr*> NUCExprVector;
typedef UtMap<const NUExpr*,NUExpr*> NUExprExprMap;
typedef UtMap<const NUExpr*,NUNet*> NUExprNetMap;
typedef Loop<NUExprVector> NUExprLoop;
typedef CLoop<NUExprVector> NUExprCLoop;
typedef UtList<NUUseDefNode*> NUUseDefList;
typedef UtList<NUUseDefNode*>::const_iterator NUUseDefListIter;
typedef UtList<NUUseDefNode*>::iterator NUUseDefListNonconstIter;
typedef UtSet<NUUseDefNode*> NUUseDefSet;
typedef UtSet<const NUUseDefNode*> NUCUseDefSet;
typedef NUUseDefSet::const_iterator NUUseDefSetIter;
typedef NUUseDefSet::iterator NUUseDefSetNonconstIter;
typedef UtVector<NUUseDefNode*> NUUseDefVector;
typedef NUUseDefVector::iterator NUUseDefVectorIter;
typedef UtList<NUTriRegInit*> NUTriRegInitList;
typedef UtList<NUTriRegInit*>::const_iterator NUTriRegInitListIter;
typedef UtList<NUTriRegInit*>::iterator NUTriRegInitListNonconstIter;
typedef UtVector<NUCaseCondition*> NUCaseConditionVector;
typedef UtList<NUCaseItem*> NUCaseItemList;
typedef UtList<NUCaseItem*>::const_iterator NUCaseItemListIter;
typedef UtList<NUCaseItem*>::iterator NUCaseItemListNonconstIter;
typedef UtList<NUCaseItem*>::const_reverse_iterator NUCaseItemListRevIter;
typedef UtVector<const NUCaseItem*> NUCaseItemCVector;
typedef UtVector<NUCaseItem*> NUCaseItemVector;
typedef UtVector<NUCaseItem*>::const_iterator NUCaseItemVectorIter;
typedef UtVector<NUCaseItem*>::iterator NUCaseItemVectorNonconstIter;
typedef UtVector<NUCaseItem*>::reverse_iterator NUCaseItemVectorRevIter;
typedef UtList<NUTask*> NUTaskList;
typedef UtVector<NUTask*> NUTaskVector;
typedef UtVector<NUTask*>::iterator NUTaskVectorNonconstIter;
typedef UtVector<NUTask*>::const_iterator NUTaskVectorIter;
typedef UtSet<NUTask*> NUTaskSet;
typedef UtSet<NUTask*>::iterator NUTaskSetNonconstIter;
typedef UtSet<NUTask*>::const_iterator NUTaskSetIter;
typedef Iter<NUTask*> NUTaskLoop;
typedef Iter<NUTask*> NUTaskCLoop;
typedef UtVector<NUTaskEnable*> NUTaskEnableVector;
typedef UtVector<NUTaskEnable*>::iterator NUTaskEnableVectorNonconstIter;
typedef UtVector<NUTaskEnable*>::const_iterator NUTaskEnableVectorIter;
typedef Loop<NUTaskEnableVector> NUTaskEnableLoop;
typedef CLoop<NUTaskEnableVector> NUTaskEnableCLoop;
typedef UtSet<NUTaskEnable*> NUTaskEnableSet;
typedef UtVector<NUTFArgConnection*> NUTFArgConnectionVector;
typedef UtVector<NUTFArgConnection*>::iterator NUTFArgConnectionVectorNonconstIter;
typedef UtVector<NUTFArgConnection*>::const_iterator NUTFArgConnectionVectorIter;
typedef Loop<NUTFArgConnectionVector> NUTFArgConnectionLoop;
typedef CLoop<NUTFArgConnectionVector> NUTFArgConnectionCLoop;
typedef UtVector<NUCModelInterface*> NUCModelInterfaceVector;
typedef NUCModelInterfaceVector::iterator NUCModelInterfaceVectorNonconstIter;
typedef NUCModelInterfaceVector::const_iterator NUCModelInterfaceVectorIter;
typedef Loop<NUCModelInterfaceVector> NUCModelInterfaceLoop;
typedef CLoop<NUCModelInterfaceVector> NUCModelInterfaceCLoop;
typedef UtVector<NUCModelFn*> NUCModelFnVector;
typedef NUCModelFnVector::iterator NUCModelFnVectorNonconstIter;
typedef NUCModelFnVector::const_iterator NUCModelFnVectorIter;
typedef Loop<NUCModelFnVector> NUCModelFnLoop;
typedef CLoop<NUCModelFnVector> NUCModelFnCLoop;
typedef UtVector<NUCModel*> NUCModelVector;
typedef NUCModelVector::iterator NUCModelVectorNonconstIter;
typedef NUCModelVector::const_iterator NUCModelVectorIter;
typedef Loop<NUCModelVector> NUCModelLoop;
typedef CLoop<NUCModelVector> NUCModelCLoop;
typedef UtVector<NUCModelArgConnection*> NUCModelArgConnectionVector;
typedef UtVector<NUCModelArgConnection*>::iterator NUCModelArgConnectionVectorNonconstIter;
typedef UtVector<NUCModelArgConnection*>::const_iterator NUCModelArgConnectionVectorIter;
typedef Loop<NUCModelArgConnectionVector> NUCModelArgConnectionLoop;
typedef CLoop<NUCModelArgConnectionVector> NUCModelArgConnectionCLoop;
typedef UtVector<NUCModelCall*> NUCModelCallVector;
typedef Loop<NUCModelCallVector> NUCModelCallVectorLoop;
typedef UtMap<const NUCModel*, NUCModelCallVector> NUCModelCalls;
typedef UtVector<NUChangeDetect*> NUChangeDetectVector;
typedef Loop<NUChangeDetectVector> NUChangeDetectVectorLoop;
typedef CLoop<NUChangeDetectVector> NUChangeDetectVectorCLoop;

// NUScope types
typedef UtSet<NUScope*> NUScopeSet;
typedef NUScopeSet::const_iterator NUScopeSetIter;
typedef NUScopeSet::iterator NUScopeSetNonconstIter;
typedef UtList<NUScope*> NUScopeList;
typedef UtStack<NUScope*> NUScopeStack;

// NUConst types
typedef UtVector<NUConst*> NUConstVector;
typedef NUConstVector::const_iterator NUConstVectorIter;
typedef NUConstVector::iterator NUConstVectorNonconstIter;
typedef Loop<NUConstVector> NUConstVectorLoop;
typedef CLoop<NUConstVector> NUConstVectorCLoop;
typedef UtVector<const NUConst*> NUCConstVector;

// NUCModelInterface types
typedef UtMap<UtString, NUCModelInterface*> CModelInterfaceMap;
typedef CModelInterfaceMap::iterator CModelInterfaceMapIter;
typedef CModelInterfaceMap::const_iterator CModelInterfaceMapConstIter;
typedef CModelInterfaceMap::value_type CModelInterfaceMapValue;


// This structure is used in postDesignFixup of verilog always blocks.
// It combines info for the seguential block.
typedef struct {
  NUAlwaysBlock* always;      // 
  NUExprLocList* exprLocList; // list of level expressions in the always block
  NUModule* module;           // the module the always block is in
  UtString reasons;           // It's not used now
} SensitivityInfo;
typedef UtList<SensitivityInfo> SensitivityInfoList;


//! Closure class; performs the set closure operation over sets of NUNetSets.
typedef SetClosure<NUNet*,NUNetSet> NUNetClosure;

//! Closure class; performs the set closure operation over sets of NUNetElabSets.
typedef SetClosure<NUNetElab*,NUNetElabSet> NUNetElabClosure;

// Template class for object-equivalence testing
template <class T>struct NUObjEqual{
  NUObjEqual() {}
  bool operator()(const T* a, const T* b) const
  {
    return *a==*b;
  }
};

template <class LoopClass> bool ObjListEqual (LoopClass a, LoopClass b)
{
  // Can't use std::equal because it won't handle lists of different lengths
  // and loop classes don't export size()
  //
  for(; !a.atEnd () && !b.atEnd (); ++a, ++b)
  {
    // compare the two objects, NOT their pointers!!
    if (not (**a == **b))
      return false;
  }

  // Did we reach the ends of the loop without a mismatch
  return (a.atEnd () && b.atEnd ());
}

template <class LoopClass> bool NUPtrListEqual (LoopClass a, LoopClass b)
{
  // Can't use std::equal because it won't handle lists of different lengths
  // and loop classes don't export size()
  //
  for(; !a.atEnd () && !b.atEnd (); ++a, ++b)
    // compare the two pointers, NOT their objects!!
    if (not (*a == *b))
      return false;

  // Did we reach the ends of the loop without a mismatch
  return (a.atEnd () && b.atEnd ());
}

#endif
