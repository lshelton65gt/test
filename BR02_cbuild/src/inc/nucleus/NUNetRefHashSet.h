// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _NU_NET_REF_HASH_SET_H_
#define _NU_NET_REF_HASH_SET_H_

#include "util/LoopSorted.h"
#include "util/UtHashMapFastIter.h"

#include "nucleus/NUNetRef.h"

//! Maintain a set of NUNetRef
/*!
 * This is an incomplete re-implementation of NUNetRefSet, based on a
 * UtHashMap<NUNet*,DynBitVecDesc>.  It has a subset of the functionality,
 * at the moment, but should perform better.
 *
 * Replacing NUNetRefSet's implementation with this one is not practical
 * at this time due to the dependence of many users of NUNetRefSet on
 * ordered traversal through standard STL iterators.  But new users of
 * NUNetRefSet can consider this implementation as an alternative if they
 * are not depending on ordered traversal.
 *
 * Any new user of NUNetRefHashSet may face having to fill in and implement
 * any missing methods they need.  They will find out which methods are
 * missing by searching NUNetRefHashSet.cxx or by letting the Linker tell
 * them.
 *
 * \sa NUNetRefSet
 */
class NUNetRefHashSet
{
private:
  typedef UtHashMapFastIter<NUNet*,DynBitVecDesc> NetRefMap;

public:
  typedef NUNetRefHdl value_type;
  typedef NetRefMap::iterator iterator;
  typedef NetRefMap::const_iterator const_iterator;

  //! constructor
  NUNetRefHashSet(NUNetRefFactory *factory) : mFactory(factory)
  {}

  //! default constructor is intentionally omitted because if you
  //! find yourself with a default-contructed NUNetRefHashSet, it will crash
  //! the first time you use it
  //NUNetRefHashSet() : mFactory(0)
  //{}

  //! Copy
  NUNetRefHashSet(const NUNetRefHashSet& src) : mFactory(src.mFactory)
  {
    copyHelper(src);
  }

  //! Assign
  NUNetRefHashSet& operator=(const NUNetRefHashSet& src)
  {
    if (&src != this) {
      clear();
      mFactory = src.mFactory;
      copyHelper(src);
    }
    return *this;
  }

  //! Get the factory this is using
  NUNetRefFactory *getFactory() const { return mFactory; }

  //! Insert the ref into the set
  bool insert(const NUNetRefHdl& ref);

  //! Insert the ref into the set
  bool insert(NUNet* net, DynBitVecDesc bits);

  //! Insert the ref into the set
  bool insert(NUNet* net, const DynBitVector& bits);

  //! Insert the given range into the set
  bool insert(const NUNetRefHashSet& src);

  //! Find the reference associated with a net
  NUNetRefHdl findNet(NUNet*);

  //! If the specified net is in the set, return true and update bits
  bool findNet(NUNet* net, DynBitVecDesc* bits);

  //! Return the iterator pointing to the net ref which satisfies (ref.*fn)(set_net_ref)
  const_iterator find(const NUNetRefHdl& ref, NUNetRefCompareFunction fn) const;
  //! Overloaded
  iterator find (const NUNetRefHdl& ref, NUNetRefCompareFunction fn);

  //! Remove the net ref which satisfies (ref.*fn)(set_net_ref)
  void erase(NUNetRefHdl& ref, NUNetRefCompareFunction fn);

  //! Remove the bits represented by the given net ref
  void erase(NUNetRefHdl& ref);

  //! Erase by iterator
  /*! Note that this is not safe if the iterator will then be used for
   *  more iteration. A find iterator should be issued to call this
   *  function.
   */
  //void erase(iterator pos) { mSet.erase(pos); }

  //! STL begin semantics
  const_iterator begin() const { return mMap.begin(); }

  //! STL begin semantics
  iterator begin() { return mMap.begin(); }

  //! STL end semantics
  const_iterator end() const { return mMap.end(); }

  //! STL end semantics
  iterator end() { return mMap.end(); }

  //! STL clear semantics
  void clear() { mMap.clear(); }

  //! STL empty semantics
  bool empty() const { return mMap.empty(); }

  //! STL size semantics. Number of nets (not bits).
  int size() const { return mMap.size(); }

  //! Number of bits present in the set (not set size).
  UInt32 getNumBits() const;

  //! debug help: print routine
  void print(int indent = 0) const;

  //! Return an iterator for a netref chosen in a stable manner from all net refs in the set
  iterator stableChoice() const;

  //! Are the two sets the same?
  bool operator==(const NUNetRefHashSet & other) const;

  //! Are they different?
  bool operator!=(const NUNetRefHashSet & other) const { return not((*this)==other); }

  //! Does one set intersect with the other?
  static bool set_has_intersection(const NUNetRefHashSet & one, const NUNetRefHashSet & two);

  //! Set intersection algorithm: out = (one intersect two)
  /*!
   * The output set must be different from the input set.
   */
  static void set_intersection(const NUNetRefHashSet &one, const NUNetRefHashSet &two, NUNetRefHashSet &out);

  //! Set difference algorithm: out = (one minus two)
  /*!
   * The output set must be different from the input set.
   */
  static void set_difference(const NUNetRefHashSet &one, const NUNetRefHashSet &two, NUNetRefHashSet &out);

  //! Set subtraction (in-place difference): out -= one
  static void set_subtract(const NUNetRefHashSet & one, NUNetRefHashSet & out);

#if 0
  class NetRefSetFilter;
  friend class NetRefSetFilter;

  //! Class for filtering nets out of the set during looping.
  class NetRefSetFilter
  {
  public:
    NetRefSetFilter() {}
    NetRefSetFilter(const NUNetRefHdl & net_ref, NUNetRefCompareFunction fn) :
      mNetRef(net_ref), mCompareFunction(fn)
    {}
    bool operator()(const NetRefSet::value_type & v) const
    {
      return ((*mNetRef).*mCompareFunction)(*v);
    }
  private:
    NUNetRefHdl mNetRef;
    NUNetRefCompareFunction mCompareFunction;
  };
  
  //! Typedefs to support looping.
  typedef CLoop<NetRefSet> NetRefSetLoop;
  typedef LoopFilter<NetRefSetLoop, NetRefSetFilter> CondLoop;
  NetRefSetLoop loop() const { return NetRefSetLoop(mMap.begin(),mMap.end()); }

  CondLoop loop(const NUNetRefHdl & ref, NUNetRefCompareFunction fn) const {
    const_iterator iter = mMap.find(ref);
    return CondLoop(NetRefSetLoop(iter,mMap.end()), NetRefSetFilter(ref, fn));
  }

  //! Iterator wrapper so we don't have to copy construct loops
  typedef Iter<NUNetRefHdl> SortedLoop;

  //! Return a sorted iterator over the net refs
  SortedLoop loopSorted() const
  {
    return SortedLoop(new LoopSorted<NUNetRefHdl>(loop()));
  }
#endif

  //! destructor
  ~NUNetRefHashSet() {}

private:
  //! Copy all the data from the source set, knowing that we have nothing so 
  //! far in this
  void copyHelper(const NUNetRefHashSet& src);

  //! The set.
  NetRefMap mMap;

  //! Factory to use to create net refs
  NUNetRefFactory *mFactory;
};

#endif 
