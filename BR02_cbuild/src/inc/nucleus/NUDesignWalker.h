// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUDESIGNWALKER_H_
#define NUDESIGNWALKER_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUModule.h"   // need all the loop declarations

/*!\file
 * Definition of Design Walker and Design Callback classes.
 */


//! Class which handles the callback from the design walker.
/*!
 * The callback is handled through overloading of operator() .
 * By default, all nucleus objects go to NUBase.
 *
 * When you create a derived class from this, you will need to at least
 * create an NUBase handler.
 *
 * Which operator() to call is chosen by the C++ compiler, when it performs
 * overload resolution.
 */
class NUDesignCallback
{
public:
  //! Status of callback
  enum Status {
    eNormal,       //!< Normal status, continue walking
    eDelete,       //!< Delete the current node after done walking it
    eStop,         //!< Stop the walk
    eSkip,         //!< Skip the current node
    eInvalid       //!< Invalid status
  };

  //! Phase of callback
  enum Phase {
    ePre,          //!< Pre-node walk
    ePost          //!< Post-node walk
  };

  //! constructor
  NUDesignCallback() {}

  //! All walking action is performed through operator()
  virtual Status operator()(Phase phase, NUBase *node) = 0;
  virtual Status operator()(Phase phase, NUUseNode *node) { return operator()(phase, (NUBase*) node); }
  virtual Status operator()(Phase phase, NUNet *node) { return operator()(phase, (NUBase*) node); }
  virtual Status operator()(Phase phase, NUUseDefNode *node) { return operator()(phase, (NUBase*) node); }
  virtual Status operator()(Phase phase, NUNamedDeclarationScope *node) { return operator()(phase, (NUBase*) node); }
  virtual Status operator()(Phase phase, NUPortConnection *node) { return operator()(phase, (NUUseDefNode*) node); }
  virtual Status operator()(Phase phase, NUPortConnectionInput *node) { return operator()(phase, (NUPortConnection*) node); }
  virtual Status operator()(Phase phase, NUPortConnectionOutput *node) { return operator()(phase, (NUPortConnection*) node); }
  virtual Status operator()(Phase phase, NUPortConnectionBid *node) { return operator()(phase, (NUPortConnection*) node); }
  virtual Status operator()(Phase phase, NUUseDefStmtNode *node) { return operator()(phase, (NUUseDefNode*) node); }
  virtual Status operator()(Phase phase, NUDesign *node) { return operator()(phase, (NUBase*) node); }
  virtual Status operator()(Phase phase, NUModule *node) { return operator()(phase, (NUUseDefNode*) node); }
  virtual Status operator()(Phase phase, NUModuleInstance *node) { return operator()(phase, (NUUseDefNode*) node); }
  virtual Status operator()(Phase phase, NUEnabledDriver *node) { return operator()(phase, (NUStmt*) node); }
  virtual Status operator()(Phase phase, NUTriRegInit *node) { return operator()(phase, (NUUseDefNode*) node); }
  virtual Status operator()(Phase phase, NUContAssign *node) { return operator()(phase, (NUAssign*) node); }
  virtual Status operator()(Phase phase, NUStructuredProc *node) { return operator()(phase, (NUUseDefNode*) node); }
  virtual Status operator()(Phase phase, NUAlwaysBlock *node) { return operator()(phase, (NUStructuredProc*) node); }
  virtual Status operator()(Phase phase, NUInitialBlock *node) { return operator()(phase, (NUStructuredProc*) node); }
  virtual Status operator()(Phase phase, NUTF *node) { return operator()(phase, (NUUseDefNode*) node); }
  virtual Status operator()(Phase phase, NUTask *node) { return operator()(phase, (NUTF*) node); }
  virtual Status operator()(Phase phase, NUStmt *node) { return operator()(phase, (NUUseDefStmtNode*) node); }
  virtual Status operator()(Phase phase, NUBreak *node) { return operator()(phase, (NUStmt*) node); }
  virtual Status operator()(Phase phase, NUSysTask *node) { return operator()(phase, (NUStmt*) node); }
  virtual Status operator()(Phase phase, NUReadmemX *node) { return operator()(phase, (NUSysTask*) node); }
  virtual Status operator()(Phase phase, NUSysRandom *node)      { return operator()(phase, (NUSysTask*) node); }
  virtual Status operator()(Phase phase, NUFCloseSysTask *node)  { return operator()(phase, (NUSysTask*) node); }
  virtual Status operator()(Phase phase, NUFFlushSysTask *node)  { return operator()(phase, (NUSysTask*) node); }
  virtual Status operator()(Phase phase, NUFOpenSysTask *node)   { return operator()(phase, (NUSysTask*) node); }
  virtual Status operator()(Phase phase, NUOutputSysTask *node)  { return operator()(phase, (NUSysTask*) node); }
  virtual Status operator()(Phase phase, NUControlSysTask *node) { return operator()(phase, (NUSysTask*) node); }
  virtual Status operator()(Phase phase, NUInputSysTask *node)  { return operator()(phase, (NUSysTask*) node); }
  virtual Status operator()(Phase phase, NUBlockStmt *node) { return operator()(phase, (NUStmt*) node); }
  virtual Status operator()(Phase phase, NUCase *node) { return operator()(phase, (NUBlockStmt*) node); }
  virtual Status operator()(Phase phase, NUCaseCondition *node) { return operator()(phase, (NUBase*) node); }
  virtual Status operator()(Phase phase, NUCaseItem *node) { return operator()(phase, (NUBase*) node); }
  virtual Status operator()(Phase phase, NUIf *node) { return operator()(phase, (NUBlockStmt*) node); }
  virtual Status operator()(Phase phase, NUFor *node) { return operator()(phase, (NUBlockStmt*) node); }
  virtual Status operator()(Phase phase, NUBlock *node) { return operator()(phase, (NUBlockStmt*) node); }
  virtual Status operator()(Phase phase, NUAssign *node) { return operator()(phase, (NUStmt*) node); }
  virtual Status operator()(Phase phase, NUNonBlockingAssign *node) { return operator()(phase, (NUAssign*) node); }
  virtual Status operator()(Phase phase, NUBlockingAssign *node) { return operator()(phase, (NUAssign*) node); }
  virtual Status operator()(Phase phase, NUTaskEnable *node) { return operator()(phase, (NUStmt*) node); }
  virtual Status operator()(Phase phase, NUCModel *node) { return operator()(phase, (NUBase*) node); }
  virtual Status operator()(Phase phase, NUCModelCall *node) { return operator()(phase, (NUBlockStmt*) node); }
  virtual Status operator()(Phase phase, NUCModelPort *node) { return operator()(phase, (NUBase*) node); }
  virtual Status operator()(Phase phase, NUCModelFn *node) { return operator()(phase, (NUBase*) node); }
  virtual Status operator()(Phase phase, NUCModelInterface *node) { return operator()(phase, (NUBase*) node); }
  virtual Status operator()(Phase phase, NUCModelArgConnection* node) { return operator()(phase, (NUBase*) node); }
  virtual Status operator()(Phase phase, NUCModelArgConnectionInput* node) { return operator()(phase, (NUCModelArgConnection*) node); }
  virtual Status operator()(Phase phase, NUCModelArgConnectionOutput* node) { return operator()(phase, (NUCModelArgConnection*) node); }
  virtual Status operator()(Phase phase, NUCModelArgConnectionBid* node) { return operator()(phase, (NUCModelArgConnection*) node); }
  virtual Status operator()(Phase phase, NULvalue *node) { return operator()(phase, (NUUseDefNode*) node); }
  virtual Status operator()(Phase phase, NUIdentLvalue *node) { return operator()(phase, (NULvalue*) node); }
  virtual Status operator()(Phase phase, NUConcatLvalue *node) { return operator()(phase, (NULvalue*) node); }
  virtual Status operator()(Phase phase, NUVarselLvalue *node) { return operator()(phase, (NULvalue*) node); }
  virtual Status operator()(Phase phase, NUMemselLvalue *node) { return operator()(phase, (NULvalue*) node); }
  virtual Status operator()(Phase phase, NUExpr *node) { return operator()(phase, (NUUseNode*) node); }
  virtual Status operator()(Phase phase, NUConst *node) { return operator()(phase, (NUExpr*) node); }
  virtual Status operator()(Phase phase, NUEdgeExpr *node) { return operator()(phase, (NUExpr*) node); }
  virtual Status operator()(Phase phase, NUIdentRvalue *node) { return operator()(phase, (NUExpr*) node); }
  virtual Status operator()(Phase phase, NUVarselRvalue *node) { return operator()(phase, (NUExpr*) node); }
  virtual Status operator()(Phase phase, NUMemselRvalue *node) { return operator()(phase, (NUExpr*) node); }
  virtual Status operator()(Phase phase, NUConcatOp *node) { return operator()(phase, (NUOp*) node); }
  virtual Status operator()(Phase phase, NUSysFunctionCall *node) { return operator()(phase, (NUExpr*) node); }
  virtual Status operator()(Phase phase, NUOp *node) { return operator()(phase, (NUExpr*) node); }
  virtual Status operator()(Phase phase, NUNullExpr *node) { return operator()(phase, (NUExpr*) node); }
  virtual Status operator()(Phase phase, NUTFArgConnection *node) { return operator()(phase, (NUUseDefNode*) node); }
  virtual Status operator()(Phase phase, NUTFArgConnectionInput *node) { return operator()(phase, (NUTFArgConnection*) node); }
  virtual Status operator()(Phase phase, NUTFArgConnectionOutput *node) { return operator()(phase, (NUTFArgConnection*) node); }
  virtual Status operator()(Phase phase, NUTFArgConnectionBid *node) { return operator()(phase, (NUTFArgConnection*) node); }
  virtual Status operator()(Phase phase, NUAttribute *node) { return operator()(phase, (NUExpr*) node); }
  virtual Status operator()(Phase phase, NUCompositeSelLvalue *node) { return operator()(phase, (NULvalue*) node); }
  virtual Status operator()(Phase phase, NUCompositeIdentLvalue *node) { return operator()(phase, (NULvalue*) node); }
  virtual Status operator()(Phase phase, NUCompositeLvalue *node) { return operator()(phase, (NULvalue*) node); }
  virtual Status operator()(Phase phase, NUCompositeFieldLvalue *node) { return operator()(phase, (NULvalue*) node); }
  virtual Status operator()(Phase phase, NUCompositeSelExpr *node) { return operator()(phase, (NUExpr*) node); }
  virtual Status operator()(Phase phase, NUCompositeIdentRvalue *node) { return operator()(phase, (NUExpr*) node); }
  virtual Status operator()(Phase phase, NUCompositeExpr *node) { return operator()(phase, (NUExpr*) node); }
  virtual Status operator()(Phase phase, NUCompositeFieldExpr *node) { return operator()(phase, (NUExpr*) node); }

  virtual Status stmtList(Phase, NUBase*) {return eNormal;}

  //! For things which can be deleted, you may supply a replacement.  0 means no replacement.
  /*!
   * Note that there is a lot of NYI here... this was created to allow stmt
   * replacement first, so other things might assert.  Please add the implementation
   * as you require it.
   */
  virtual NUAlwaysBlock *replacement(NUAlwaysBlock *) { return 0; }
  virtual NUCModel *replacement(NUCModel *) { return 0; }
  virtual NUCModelArgConnection *replacement(NUCModelArgConnection *) { return 0; }
  virtual NUCModelPort *replacement(NUCModelPort *) { return 0; }
  virtual NUCModelFn *replacement(NUCModelFn *) { return 0; }
  virtual NUCModelInterface *replacement(NUCModelInterface *) { return 0; }
  virtual NUCaseCondition *replacement(NUCaseCondition *) { return 0; }
  virtual NUCaseItem *replacement(NUCaseItem *) { return 0; }
  virtual NUContAssign *replacement(NUContAssign *) { return 0; }
  virtual NUEnabledDriver *replacement(NUEnabledDriver *) { return 0; }
  virtual NUExpr *replacement(NUExpr *) { return 0; }
  virtual NUInitialBlock *replacement(NUInitialBlock *) { return 0; }
  virtual NULvalue *replacement(NULvalue *) { return 0; }
  virtual NUModuleInstance *replacement(NUModuleInstance *) { return 0; }
  virtual NUNamedDeclarationScope *replacement(NUNamedDeclarationScope *) { return 0; }
  virtual NUNet *replacement(NUNet *) { return 0; }
  virtual NUPortConnectionBid *replacement(NUPortConnectionBid *) { return 0; }
  virtual NUPortConnectionInput *replacement(NUPortConnectionInput *) { return 0; }
  virtual NUPortConnectionOutput *replacement(NUPortConnectionOutput *) { return 0; }
  virtual NUStmt *replacement(NUStmt *) { return 0; }
  virtual NUTFArgConnection *replacement(NUTFArgConnection *) { return 0; }
  virtual NUTask *replacement(NUTask *) { return 0; }
  virtual NUTriRegInit *replacement(NUTriRegInit *) { return 0; }

  //! destructor
  virtual ~NUDesignCallback() {}

private:
  //! Hide copy and assign constructors.
  NUDesignCallback(const NUDesignCallback&);
  NUDesignCallback& operator=(const NUDesignCallback&);
};


//! Callback-based generic design traversal.
/*!
 * Traverse a design, invoking a callback for each nucleus node visited.
 */
class NUDesignWalker
{
public:
  //! constructor
  /*!
   * \param rememberVisited  By default we visit modules only once. Set this
   *                         to false to visit modules for every instance.
   */
  NUDesignWalker(NUDesignCallback &callback, bool verbose,
		 bool rememberVisited = true);

  //! Tell the walker if it should walk over replacements for deleted constructs.
  void putWalkReplacements(bool walk_replacements);
  
  //! Tell the walker if it should recurse into task bodies from task-enables
  //! (default true)
  void putWalkTasksFromTaskEnables(bool flag);

  //! Tell the walker to forget all of the visited items
  void resetRememberVisited();

  //! Tell the walker to walk over net declarations (default false)
  void putWalkDeclaredNets(bool flag);
  
  //! Walk a design
  /*!
   * \param design           The design.
   *
   * \param walk_all_modules Walk all modules -- even if they are no
   *                         longer instantiated.
   */
  NUDesignCallback::Status design(NUDesign *design, bool walk_all_modules=false);

  //! Walk a module
  NUDesignCallback::Status module(NUModule *module);

  //! Walk a declaration scope (only if putWalkDeclaredNets(true) is called)
  NUDesignCallback::Status declarationScope(NUNamedDeclarationScope *dscope);

  //! Walk a module isntance
  NUDesignCallback::Status moduleInstance(NUModuleInstance *this_module_inst);

  //! Walk an input port connection.
  NUDesignCallback::Status inputPortConnection(NUPortConnectionInput *this_conn);

  //! Walk an output port connection.
  NUDesignCallback::Status outputPortConnection(NUPortConnectionOutput *this_conn);

  //! Walk an bid port connection.
  NUDesignCallback::Status bidPortConnection(NUPortConnectionBid *this_conn);

  //! Walk a task/function
  NUDesignCallback::Status tf(NUTF *tf);

  //! Walk a task
  NUDesignCallback::Status task(NUTask *task);

  //! Walk a continuous assign
  NUDesignCallback::Status contAssign(NUContAssign *assign);

  //! Walk an enabled driver
  NUDesignCallback::Status enabledDriver(NUEnabledDriver *driver);

  //! Walk a trireg init node
  NUDesignCallback::Status triRegInit(NUTriRegInit *init);

  //! Walk an always block
  NUDesignCallback::Status alwaysBlock(NUAlwaysBlock *block);

  //! Walk an initial block
  NUDesignCallback::Status initialBlock(NUInitialBlock *block);

  //! Walk a structured procedure
  NUDesignCallback::Status structuredProc(NUStructuredProc *proc);

  //! Walk a statement
  NUDesignCallback::Status stmt(NUStmt *stmt);

  //! Walk an if statement
  NUDesignCallback::Status ifStmt(NUIf *if_stmt);

  //! Walk a case statement
  NUDesignCallback::Status caseStmt(NUCase *case_stmt);

  //! Walk a case item
  NUDesignCallback::Status caseItem(NUCaseItem *case_item);

  //! Walk a case condition
  NUDesignCallback::Status caseCondition(NUCaseCondition *case_condition);

  //! Walk a block scope (NUTask/NUBlock)
  NUDesignCallback::Status blockScope(NUBlockScope *block_scope);

  //! Walk a block (NUBlock, NUIf, NUCase, NUFor, etc.)
  NUDesignCallback::Status blockStmt(NUBlock *block);

  //! Walk a for stmt
  NUDesignCallback::Status forStmt(NUFor *for_stmt);

  //! Walk an assign
  NUDesignCallback::Status assign(NUAssign *assign);

  //! Walk an blocking assign
  NUDesignCallback::Status blockingAssign(NUBlockingAssign *blocking_assign);

  //! Walk a non-blocking assign
  NUDesignCallback::Status nonBlockingAssign(NUNonBlockingAssign *non_blocking_assign);

  //! Walk a task enable
  NUDesignCallback::Status taskEnable(NUTaskEnable *task_enable);

  //! Walk an l-value
  NUDesignCallback::Status lvalue(NULvalue *lvalue);

  //! Walk an ident l-value
  NUDesignCallback::Status identLvalue(NUIdentLvalue *ident_lvalue);

  //! Walk a concat l-value
  NUDesignCallback::Status concatLvalue(NUConcatLvalue *concat_lvalue);

  //! Walk a composite l-value
  NUDesignCallback::Status compositeLvalue(NUCompositeLvalue *composite_lvalue);

  //! Walk a composite l-value select
  NUDesignCallback::Status compositeSelLvalue(NUCompositeSelLvalue *compositesel_lvalue);

  //! Walk a composite l-value field
  NUDesignCallback::Status compositeFieldLvalue(NUCompositeFieldLvalue *compositefield_lvalue);

  //! Walk a composite l-value ident
  NUDesignCallback::Status compositeIdentLvalue(NUCompositeIdentLvalue *compositeident_lvalue);

  //! Walk a bitsel l-value
  NUDesignCallback::Status varselLvalue(NUVarselLvalue *varsel_lvalue);

  //! Walk a memsel l-value
  NUDesignCallback::Status memselLvalue(NUMemselLvalue *memsel_lvalue);

  //! Walk a net
  NUDesignCallback::Status net(NUNet* net);

  //! Walk an expression
  NUDesignCallback::Status expr(NUExpr *expr);

  //! Expression dispatch helper
  NUDesignCallback::Status exprDispatch(NUExpr* e,
                                        NUDesignCallback::Phase phase);

  //! Walk a NUCompositeSelExpr
  NUDesignCallback::Status compositeSelExpr(NUCompositeSelExpr *this_op);

  //! Walk a composite r-value field
  NUDesignCallback::Status compositeFieldExpr(NUCompositeFieldExpr *this_op);

  //! Walk a tf arg connection
  NUDesignCallback::Status tfArgConnection(NUTFArgConnection *conn);

  //! Walk a c-model call
  NUDesignCallback::Status cmodelCall(NUCModelCall* cmodel_call);

  //! Walk a c-model function
  NUDesignCallback::Status cmodelFn(NUCModelFn* cmodel_fn);

  //! Walk a c-model interface
  NUDesignCallback::Status cmodelInterface(NUCModelInterface* cmodel_interface);

  //! Walk a c-model port
  NUDesignCallback::Status cmodelPort(NUCModelPort* cmodel_port);

  //! Walk a c-model arg connection
  NUDesignCallback::Status cmodelArgConnection(NUCModelArgConnection* conn);

  //! Walk a c-model 
  NUDesignCallback::Status cmodel(NUCModel* cmodel);

  //! Walk a break stmt
  NUDesignCallback::Status breakStmt(NUBreak* sysTask);

  //! Walk a random task
  NUDesignCallback::Status sysRandom(NUSysRandom* sysRand);

  //! Walk a readMexX task
  NUDesignCallback::Status sysReadmemX(NUReadmemX* sysMemXTask);

  //! Walk a FClose task
  NUDesignCallback::Status sysFClose(NUFCloseSysTask* sysFCloseTask);

  //! Walk a FFlush task
  NUDesignCallback::Status sysFFlush(NUFFlushSysTask* sysFFlushTask);

  //! Walk a FOpen task
  NUDesignCallback::Status sysFOpen(NUFOpenSysTask* sysFOpenTask);

  //! Walk a OutputSys  task
  NUDesignCallback::Status sysOutputSys(NUOutputSysTask* sysOutputTask);

  //! Walk a ControlSys  task
  NUDesignCallback::Status sysControlSys(NUControlSysTask* sysControlTask);

  //! Walk a InputSys  task
  NUDesignCallback::Status sysInputSys(NUInputSysTask* sysInputTask);

  //! Walk an attrbiute
  NUDesignCallback::Status attribute(NUAttribute* attribute);
  //! destructor
  ~NUDesignWalker();

  //! Walk a continuous driver use def node
  NUDesignCallback::Status contDriver(NUUseDefNode* useDef);

  //! Walk nested nucleus
  /*!
   * \param this_container Container holding the nested nucleus
   * \param loop Loop over the nested nucleus
   * \param traverseFn Member function of DeadDesignWalker to call for each nested nucleus
   * \param removeFn Member function of container to call when removing nested nucleus
   *
   * \note removeFn is optional, but you always need an implementation
   *  for the method NUDesignCallback::replacement(NestedT*) even if
   *  you do not plan on removing anything.
   *
   * TraverseT is distinct from NestedT so that modules can loop over always
   * blocks and call the NUStructuredProc callback on them.
   */
  template<class ContainerT, class LoopT, class ReplaceT, class TraverseT>
  NUDesignCallback::Status nested(ContainerT *this_container,
				  LoopT (ContainerT::* loopFn)(),
				  NUDesignCallback::Status (NUDesignWalker::* traverseFn)(TraverseT*),
				  void (ContainerT::* replaceFn)(const ReplaceT&) = NULL);

private:
  //! Hide copy and assign constructors.
  NUDesignWalker(const NUDesignWalker&);
  NUDesignWalker& operator=(const NUDesignWalker&);

  //! Helper to walk a task/function
  /*!
   * This is private because it is a helper function, and should not be called
   * by users; it won't behave the way they think it should.
   */
  NUDesignCallback::Status helperTF(NUTF *this_tf);

  //! Return true if have visited this module/scope.
  bool haveVisited(NUScope *scope) const;

  //! Remember that have visited this module/scope.
  void rememberVisited(NUScope *scope);

  //! Callback to use
  NUDesignCallback &mCallback;

  //! Set of modules/scopes which we have visited so far.
  NUScopeSet &mScopeSet;

  //! Recurse over replacements for deleted nodes?
  bool mWalkReplacements;

  //! Be verbose?
  bool mVerbose;

  //! Remember if we have visited a module, task, etc?
  bool mRememberVisited;

  //! Walk into task bodies from task enables
  bool mWalkTasksFromTaskEnables;

  //! Walk over declared nets?
  bool mWalkDeclaredNets;

  //! Tasks that we are currently visiting.  This is not the same
  //! as the tasks that we have ever visited.
  NUTaskSet* mCurrentTasks;
};

#endif
