// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#ifndef NUCOMPOSITENETHIERREF_H_
#define NUCOMPOSITENETHIERREF_H_


//! Hierarchical reference to a composit net
class NUCompositeNetHierRef : public NUCompositeNet
{
public:
  //! constructor
  /*!
    \param name Name of the net (includes path)
    \param flags The declared flags for the net
    \param scope Scope in which hierarchical ref occurs.
    \param loc Source location
  */
  NUCompositeNetHierRef(const AtomArray& path,
			const StringAtom *typeMark, NetFlags flags, NUNetVector *fields, 
			const ConstantRangeVector *ranges, bool packed,
			NUScope *scope, const SourceLocator &loc);

  //! destructor
  ~NUCompositeNetHierRef();

  bool isHierRef() const { return true; }

  //! Illegal to call.
  void addHierRef(NUBase *net_hier_ref);

  //! Illegal to call.
  void removeHierRef(NUBase *net_hier_ref);

  //! Return true if this net is read outside the net's scope
  bool isHierRefRead() const;

  //! Return true if this net is written outside the net's scope
  bool isHierRefWritten() const;

  //! Are any of the possible resolutions protected mutable due to non-hierref reasons?
  /*!
   * This returns true for subset of when isProtectedMutable returns
   * true, and should only be used when you specifically don't
   * want hierrefs to make a net protected.
   */
  bool isProtectedMutableNonHierref(const IODBNucleus* iodb,
                                    bool checkPrimary = true) const;

  //! Return the NUNetHierRef for this hierref.
  NUNetHierRef* getHierRef() { return &mNetHierRef; }
  const NUNetHierRef* getHierRef() const { return &mNetHierRef; }

  //! Code Generator
  // Should exist !!
  //CGContext_t emitCode (CGContext_t) const;

  //! Illegal to call; cannot create hierrefs to hierrefs.
  NUNet* createHierRef(const AtomArray& name, NUScope *scope, NetFlags flags, const SourceLocator &loc);

protected:
  //! Helper function for print, prints the name and size information
  void printNameSize() const;

private:
  //! Hide copy and assign constructors.
  NUCompositeNetHierRef(const NUCompositeNetHierRef&);
  NUCompositeNetHierRef& operator=(const NUCompositeNetHierRef&);

  //! Hierref resolution object.
  NUNetHierRef mNetHierRef;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCompositeNetHierRef : public NUCompositeNet


#endif // NUBITNETHIERREF_H_
