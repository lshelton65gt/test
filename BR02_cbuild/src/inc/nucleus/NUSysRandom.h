// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef _NU_SYS_RANDOM_H_
#define _NU_SYS_RANDOM_H_

#include "nucleus/NUSysTask.h"

/*!
  \file
  Declaration of the Nucleus random system tasks.  Syntactically, $random
  and friends are functions, and can be used anywhere in expressions.  But
  because they modify their first arg, a net, it is more convenient to
  "hoist" them, treat them like a system task, and substitute a temp net
  in the expression.
*/

//! NUSysRandom class
class NUSysRandom: public NUSysTask
{
public:
  //! which of the random functions is it;?
  enum Function
  {
    eRandom,           //!< $random
    eDistUniform,      //!< $dist_uniform
    eDistNormal,       //!< $dist_normal
    eDistExponential,  //!< $dist_exponential
    eDistPoisson,      //!< $dist_poisson
    eDistChiSquare,    //!< $dist_chi_square
    eDistT,            //!< $dist_t
    eDistErlang,       //!< $dist_errlang
    eInvalidRandom     //!< not a function related to random
  };


  //! ctor
  NUSysRandom(Function func,
              StringAtom* name,
              NULvalue* outLvalue,
              NULvalue* seedLvalue,
              NUExprVector& exprs,
              const NUModule* module,
              NUNetRefFactory* netRefFactory,
              bool usesCFNet,
              const SourceLocator& loc);

  //! dtor
  virtual ~NUSysRandom();

  // first define methods that override those of NUSysTask

  //! Return the type of this class
  virtual NUType getType() const;

  //! Return class name
  virtual const char* typeStr() const;

  //! returns true if \a net is def'd by this NUSysRandom task
  virtual bool isDefNet(NUNet* net) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Dump myself to cout, shows distribution function name
  virtual void print(bool recurse, int indent) const;

  //! Return a copy of this statement; recurses to copy nested statements.
  virtual NUStmt* copy(CopyContext &copy_context) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;


  //! Add the nets this stmt blockingly defines to the given set.
  virtual void getBlockingDefs(NUNetSet *defs) const;
  virtual void getBlockingDefs(NUNetRefSet *defs) const;
  virtual bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
				 NUNetRefCompareFunction fn,
				 NUNetRefFactory *factory) const;


  //! Add the nets this stmt blockingly kills to the given set.
  virtual void getBlockingKills(NUNetSet *kills) const;
  virtual void getBlockingKills(NUNetRefSet *kills) const;
  virtual bool queryBlockingKills(const NUNetRefHdl &def_net_ref,
				  NUNetRefCompareFunction fn,
				  NUNetRefFactory *factory) const;


  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net);

  //! replace any net's def'd by this system call ($random modifies its first arg)
  void replaceDef(NUNet *old_net, NUNet *new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);


  // now declare methods that are specific to NUSysRandom

  //! Return the seed net
  NUNet* getSeedNet() const;

  //! Validate legal arguments for the system call
  bool isValid(UtString* errmsg);

  //! get the lvalue for the output net
  NULvalue* getOutLvalue() const;

  //! get the lvalue for the seed net
  NULvalue* getSeedLvalue() const;

  //! Look up a Function code based on the verilog system function name
  static bool lookup(const char* sysFunc, Function*);



private:
  Function mFunction;

  // This is the LHS in the original HDL that might have looked like:
  //
  //   always @(*)
  //      out = $random(seed);
  //
  NULvalue* mOutLvalue;

  // The seed (first optional argument) is really an inout. We model
  // this by making it both an lvalue and an rvalue for the task. This
  // field represents only the lvalue. The rvalue is represented in
  // the expression array that is part of NUSysTask.
  //
  // Since the seed is an optional argument this pointer can be NULL.
  NULvalue* mSeedLvalue;

  //! Hide copy and assign constructors.
  NUSysRandom(const NUSysRandom&);
  NUSysRandom& operator=(const NUSysRandom&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUSysRandom: public NUSysTask

#endif // _NU_SYS_RANDOM_H_
