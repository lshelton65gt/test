// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Declaration of the Nucleus system task class
*/

#ifndef _NUSYSTASK_H_
#define _NUSYSTASK_H_

#include "nucleus/NUStmt.h"
#include "shell/carbon_capi.h"
#include "util/SourceLocator.h"

class NULvalue;

//! NUSysTask class
/*!
 * Base class for system task statements.
 * Provides the common structures and methods for system tasks
 * If you derive from this class you should at least redefine the
 * methods:
 *   getType, typeStr, isDefNet,compose, print, operator==,
 *   calcCost, emitCode, copy
 * if your derived class contains a new def'ed net then you probably
 * need to redefine the methods:
 *  replace, replaceDef, replaceLeaves, getBlockingDefs(2),
 *  queryBlockingDefs, getBlockingKills(2), queryBlockingKills, 
 *
 * and if you add any member variables you must consider the
 * re-implementation of most other methods that are virtual (the
 * non-virtual class are probably ok as defined)
 * to simplify your work you should leave all arguments in the
 * \a exprs vector.  If a specific position has a special meaning you
 * can provide a method to extract that entry.  For simplicity the
 * methods in this class operate on all entries of the vector.
 */
class NUSysTask : public NUStmt
{
public:
  //! constructor
  /*!
   * Creates a system task,
   *
   * \param name A module unique name for this task
   * \param exprs arguments to this system task
   * \param module is the module where this system task appeared
   * \param netRefFactory Factory to manage netref's - needed for u/d containers
   * \param usesCFNet true when this needs $cfnet in it's use list
   * \param isVerilogTask false when this is created by VHDL.
   * \param loc The location in the source code for this task.
   */
  NUSysTask(StringAtom* name,
            NUExprVector& exprs,
            const NUModule* module,
            NUNetRefFactory* netRefFactory,
            bool usesCFNet,
            bool isVerilogTask,
            const SourceLocator& loc);

  //! destructor
  ~NUSysTask();


  //! Return class name
  virtual const char* typeStr() const;

  //! returns true if \a net is def'd by this NUSysTask,
  /*! this is pure virtual to force any derived classes to define this
   * function for correct operation
   */
  virtual bool isDefNet(NUNet* net) const = 0;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! put a print version of this object's flags into buffer
  virtual void printFlagsToBuf(UtString* buf) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! No-op
  void clearUseDef() {}

  //! Add the nets this stmt uses for blocking defs to the given set.
  virtual void getBlockingUses(NUNetSet* uses) const;
  virtual void getBlockingUses(NUNetRefSet* uses) const;
  virtual bool queryBlockingUses(const NUNetRefHdl& use_net_ref,
				 NUNetRefCompareFunction fn,
				 NUNetRefFactory* factory) const;

  //! Add the nets this stmt uses to blockingly def the given net to the given set.
  virtual void getBlockingUses(NUNet *net, NUNetSet* uses) const;
  virtual void getBlockingUses(const NUNetRefHdl& net_ref, NUNetRefSet* uses) const;
  virtual bool queryBlockingUses(const NUNetRefHdl &def_net_ref,
				 const NUNetRefHdl &use_net_ref,
				 NUNetRefCompareFunction fn,
				 NUNetRefFactory* factory) const;

  //! No-op
  void getNonBlockingUses(NUNetSet * /* uses -- unused */) const {}
  void getNonBlockingUses(NUNetRefSet * /* uses -- unused */) const {}
  bool queryNonBlockingUses(const NUNetRefHdl & /* use_net_ref -- unused */,
			    NUNetRefCompareFunction /* fn -- unused */,
			    NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! No-op
  void getNonBlockingUses(NUNet * /* net -- unused */,
			  NUNetSet * /* uses -- unused */) const {}
  void getNonBlockingUses(const NUNetRefHdl & /* net_ref -- unused */,
			  NUNetRefSet * /* uses -- unused */) const {}
  bool queryNonBlockingUses(const NUNetRefHdl & /* def_net_ref -- unused */,
			    const NUNetRefHdl & /* use_net_ref -- unused */,
			    NUNetRefCompareFunction /* fn -- unused */,
			    NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this stmt blockingly defines to the given set.
  virtual void getBlockingDefs(NUNetSet* uses) const;
  virtual void getBlockingDefs(NUNetRefSet* uses) const;
  virtual bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
				 NUNetRefCompareFunction fn,
				 NUNetRefFactory* factory) const;

  //! No-op
  void getNonBlockingDefs(NUNetSet * /* uses -- unused */) const {}
  void getNonBlockingDefs(NUNetRefSet * /* uses -- unused */) const {}
  bool queryNonBlockingDefs(const NUNetRefHdl & /* def_net_ref -- unused */,
			    NUNetRefCompareFunction /* fn -- unused */,
			    NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this stmt blockingly kills to the given set.
  virtual void getBlockingKills(NUNetSet* kills) const;
  virtual void getBlockingKills(NUNetRefSet* kills) const;
  virtual bool queryBlockingKills(const NUNetRefHdl &def_net_ref,
				  NUNetRefCompareFunction fn,
				  NUNetRefFactory* factory) const;

  //! No-op
  void getNonBlockingKills(NUNetSet * /* kills -- unused */) const {}
  void getNonBlockingKills(NUNetRefSet * /* kills -- unused */) const {}
  bool queryNonBlockingKills(const NUNetRefHdl & /* def_net_ref -- unused */,
			     NUNetRefCompareFunction /* fn -- unused */,
			     NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net);

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);


  //! If this has a name, return it, else return NULL.
  StringAtom* getName() const { return mName; }

  //! Returns false if populated from VHDL source.
  bool isVerilogTask() const { return mIsVerilogTask; }

  //! Return the module for this NUSysTask
  const NUModule* getModule() const { return mModule; }

  //! Returns a pointer to the list of expressions for this system task
  NUExprVector getValueExprs() const {return mValueExprVector;}

  //! loop thru all the expressions
  NUExprCLoop loopCArgs () const {return NUExprCLoop (mValueExprVector); }
  //! overload for non-constant
  NUExprLoop loopArgs ()        {return NUExprLoop (mValueExprVector); }


protected:
  //! make a copy of the expressions in the \a source expression vector, put results in \a dest
  void copyExprVector(NUExprVector& dest, const NUExprVector& source, CopyContext& copy_context) const;
  
  NUNetRefFactory* mNetRefFactory APIOMITTEDFIELD;
  StringAtom* mName;
  NUExprVector mValueExprVector;     // arguments as expressions
  const NUModule* mModule;
  bool  mIsVerilogTask;

private:
  //! Hide copy and assign constructors.
  NUSysTask(const NUSysTask&);
  NUSysTask& operator=(const NUSysTask&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUSysTask : public NUStmt

//! NUReadmemX class
class NUReadmemX : public NUSysTask
{
public:
  //! constructor
  /*!
   * Creates a readmemX system task
   *
   * \param name A module unique name for this task
   *
   * \param exprs expressions (currently just a place holder)
   *
   * \param module is the module where this system task appeared
   *
   * \param lvalue An lvalue for the memory
   *
   * \param file The file that the task reads
   *
   * \param hexFormat If true, then the file is in UtIO::hex format,
   * otherwise it is in binary format.
   *
   * \param startAddress where to start reading into the memory. This
   * is either specified in the HDL system task or the left address of
   * the memory range should be used.
   *
   * \param endAddress Where to stop reading into the memory. This is
   * either specified in the HDL system task or the right address of
   * the memory range should be used.
   *
   * \param endSpecified If set the endAddress was specified in the
   * system task. In this case the task should print a warning if the
   * entire range is not loaded.
   *
   * \param usesCFNet true when this needs $cfnet in it's use list
   *
   * \param loc The location in the source code for this task.
   */
  NUReadmemX(StringAtom* name,
             NUExprVector& exprs,
             const NUModule* module,
             NULvalue *lvalue, StringAtom* file,
	     bool hexFormat, SInt64 startAddress, SInt64 endAddress,
	     bool endSpecified,
             NUNetRefFactory *netRefFactory,
             bool usesCFNet,
             const SourceLocator& loc);

  //! destructor
  ~NUReadmemX();

  //! No-op
  void getBlockingUses(NUNetSet * /* uses -- unused */) const {}
  void getBlockingUses(NUNetRefSet * /* uses -- unused */) const {}
  bool queryBlockingUses(const NUNetRefHdl & /* use_net_ref -- unused */,
			 NUNetRefCompareFunction /* fn -- unused */,
			 NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! No-op
  void getBlockingUses(NUNet * /* net -- unused  */,
		       NUNetSet * /* uses -- unused */) const {}
  void getBlockingUses(const NUNetRefHdl & /* net_ref -- unused  */,
		       NUNetRefSet * /* uses -- unused */) const {}
  bool queryBlockingUses(const NUNetRefHdl & /* def_net_ref -- unused */,
			 const NUNetRefHdl & /* use_net_ref -- unused */,
			 NUNetRefCompareFunction /* fn -- unused */,
			 NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this stmt blockingly defines to the given set.
  void getBlockingDefs(NUNetSet *uses) const;
  void getBlockingDefs(NUNetRefSet *uses) const;
  bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the nets this stmt blockingly kills to the given set.
  void getBlockingKills(NUNetSet *kills) const;
  void getBlockingKills(NUNetRefSet *kills) const;
  bool queryBlockingKills(const NUNetRefHdl &def_net_ref,
			  NUNetRefCompareFunction fn,
			  NUNetRefFactory *factory) const;

  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! is the specified net def'd by this NUReadmemX task?
  bool isDefNet(NUNet* net) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const;

  //! Return class name
  virtual const char* typeStr() const;

  //! Return a copy of this statement; recurses to copy nested statements.
  NUStmt* copy(CopyContext &copy_context) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! Accessors
  NULvalue* getLvalue ()const {return mLvalue;} 

  StringAtom* getFileName ()const {return mFileName;}

  SInt64 getStartAddress ()const { return mStartAddress; }

  SInt64 getEndAddress ()const { return mEndAddress; }

  bool getHexFormat ()const { return mHexFormat; }

  bool getEndSpecified ()const { return mEndSpecified; }

  //! Return the type of this class
  virtual NUType getType() const { return eNUReadmemX; }

private:
  NULvalue* mLvalue;
  StringAtom* mFileName;
  SInt64 mStartAddress;
  SInt64 mEndAddress;
  bool mHexFormat;
  bool mEndSpecified;

  //! Hide copy and assign constructors.
  NUReadmemX(const NUReadmemX&);
  NUReadmemX& operator=(const NUReadmemX&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUReadmemX : public NUSysTask

//! NUOutputSysTask class 
/*!
     used to represent $display, $fdisplay, $write, $fwrite, $monitor,
     $fmonitor, $strobe, $fstrobe
*/
class NUOutputSysTask : public NUSysTask
{
public:
  //! constructor
  /*!
   * Creates a $Display type system task
   *
   * \param name A module unique name for this task
   *
   * \param exprs vector of expressions to be displayed
   *
   * \param module is the module where this system task appeared
   *
   * \param hasFileSpec if true then first arg is the file specification
   *
   * \param appendNewline if true then a newline is appended to format
   * string on output
   *
   * \param timeUnit value of time unit from `timescale directive (used for %t)
   *
   * \param timePrecision time precision from `timescale directive
   *
   * \param netRefFactory Factory to manage netref's - needed for u/d containers
   *
   * \param usesCFNet true when this needs $cfnet in it's use list
   *
   * \param isVerilogTask false when this is created by VHDL.
   *
   * \param isWriteLineTask true when representing WRITELINE of VHDL.
   *
   * \param hasInstanceName true when this is a display or variant
   * that uses %m
   *
   * \param where is the location in the unelabHier where the $display
   * occurs, should be null if initial scope of $display is a module,
   * else should be the namedDeclarationScope of $display
   *
   * \param loc The location in the source code for this task.
   */
  NUOutputSysTask(StringAtom* name,
                  NUExprVector& exprs,
                  const NUModule* module,
                  bool hasFileSpec,
                  bool appendNewline,
                  SInt8 timeUnit,
                  SInt8 timePrecision,
                  NUNetRefFactory *netRefFactory,
                  bool usesCFNet,
                  bool isVerilogTask,
                  bool isWriteLineTask,
                  bool hasInstanceName,
                  STBranchNode* where,
                  const SourceLocator& loc);

  //! destructor
  ~NUOutputSysTask();

  // first define methods that override those of NUSysTask

  //! Return class name
  virtual const char* typeStr() const;

  //! returns true if \a net is def'd by this NUOutputSysTask
  virtual bool isDefNet(NUNet* net) const;

  //! put a print version of this object's flags into buffer
  virtual void printFlagsToBuf(UtString* buf) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;
private:
  // helper for emitCode, returns true if argument found false if none available
  bool useAnotherOutputSysTaskArgument(NUExprList *localExprList, UInt32* argCounter, char formatchar, NUExpr** expr) const;
public:

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;


  // Accessors

  //! Return the type of this class
  virtual NUType getType() const;

  //! Return a copy of this statement; recurses to copy nested statements.
  NUStmt* copy(CopyContext &copy_context) const;

  //! returns the file spec expression if defined, else null
  NUExpr* getFileSpec () const;

  //! returns true if this has a file specification
  bool hasFileSpec () const { return mHasFileSpec; }

  //! returns true if a newline is to be added at end of output
  bool needsNewline () const { return mAddNewline; }

  //! returns true if this is representing VHDL's WRITELINE procedure.
  bool isWriteLineTask() const { return mWriteLineTask; }

  //! Return the value of the timeUnit that was defined for this task
  SInt8 getTimeUnit() const { return mTimeUnit; };

  //! returns true if this is a display variant that prints %m
  bool hasHierarchicalName() const { return mHasHierarchicalName; }

  //! Get the sub instance(s) this system task was flattened from
  STBranchNode* getNameBranch() const { return mNameBranch; }

  //! Update the sub instance(s) this system task was flattened from
  void putNameBranch(STBranchNode* nameBranch) { mNameBranch = nameBranch; }

  //! Set the original pathname to retain it after promotion to the root
  void putPromotePath(const char* path);

private:
  bool mAddNewline;          // true if a newline is appended to each line of output
  bool mHasFileSpec;         // true if this system task has a file spec

  bool mWriteLineTask;       // true is this is representing WRITELINE

  bool mHasHierarchicalName; // true if this is a display variant that
                             // contains a %m format in it.
  SInt8 mTimeUnit;           //! time unit from `timescale directive (used for %t)
  SInt8 mTimePrecision;      //! time precision from `timescale directive

  // A place to store a branch node that represents the sub
  // instance(s) this system task was flattend from. This is used in
  // figuring out the %m instance name for $display and its variants.
  STBranchNode* mNameBranch;

  // If this system-task call got promoted, and it's got a %m in it,
  // then TFPromote needs to save the original task hierarchy
  char* mPromotePath;

  //! Hide copy and assign constructors.
  NUOutputSysTask(const NUOutputSysTask&);
  NUOutputSysTask& operator=(const NUOutputSysTask&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUOutputSysTask : public NUSysTask


//! NUFCloseSysTask class 
/*!
    for $fclose
*/
class NUFCloseSysTask : public NUSysTask
{
public:
  //! constructor
  /*!
   * Creates a $fclose type system task
   *
   * \param name A module unique name for this task
   *
   * \param exprs, a vector (with a single entry) verilog specifier
   * for the file to be, (the expression must evaluate to an integer when used.)
   *
   * \param module is the module where this system task appeared
   *
   * \param netRefFactory Factory to manage netref's - needed for u/d containers
   *
   * \param usesCFNet true when this needs $cfnet in it's use list
   *
   * \param isVerilogTask false when this is created by VHDL.
   *
   * \param loc The location in the source code for this task.
   */
  NUFCloseSysTask(StringAtom* name,
                  NUExprVector& exprs,
                  const NUModule* module,
                  NUNetRefFactory *netRefFactory,
                  bool usesCFNet,
                  bool isVerilogTask,
                  bool useInputFileSystem,
                  const SourceLocator& loc);

  //! destructor
  ~NUFCloseSysTask();

  // first define methods that override those of NUSysTask

  //! Return class name
  virtual const char* typeStr() const;

  //! returns true if \a net is def'd by this NUFCloseSysTask
  virtual bool isDefNet(NUNet* net) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Return the type of this class
  virtual NUType getType() const;

  //! Return a copy of this statement; recurses to copy nested statements.
  NUStmt* copy(CopyContext &copy_context) const;

  // now declare methods that are specific to NUFCloseSysTask

  //! returns the file spec expression (always element 0 of expression vector)
  NUExpr* getFileSpec () const;

  //! check for the proper number of arguments
  bool isValid(UtString* msg);


private:
  bool mUseInputFileSystem;

  //! Hide copy and assign constructors.
  NUFCloseSysTask(const NUFCloseSysTask&);
  NUFCloseSysTask& operator=(const NUFCloseSysTask&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUFCloseSysTask : public NUSysTask



//! NUFOpenSysTask class 
/*!
    for $fopen
    Declaration of the Nucleus object for fopen.  Syntactically,
    $fopen is a function, and can be used anywhere in expressions.  But
    because we need a temp variable to contain the filename for
    conversion  it is more convenient to "hoist" it and treat it like
    a system task, and create a temp net that is used in its place in
    the expression.
*/
class NUFOpenSysTask : public NUSysTask
{
public:
  //! constructor
  /*!
   * Creates a $fopen type system task
   *
   * \param name A module unique name for this task
   *
   * \param outNet A temp net that allows this function to act like a task
   *
   * \param statusLvalue lvalue for receiving the status of open systask in VHDL
   *
   * \param exprs arguments to fopen
   *
   * \param module is the module where this system task appeared
   *
   * \param netRefFactory Factory to manage netref's - needed for u/d containers
   *
   * \param usesCFNet true when this needs $cfnet in it's use list
   *
   * \param isVerilogTask false when this is created by VHDL.
   *
   * \param loc The location in the source code for this task.
   */
  NUFOpenSysTask(StringAtom* name,
                 NUNet* outNet,
                 NUIdentLvalue* statusLvalue,
                 NUExprVector& exprs,
                 const NUModule* module,
                 NUNetRefFactory *netRefFactory,
                 bool usesCFNet,
                 bool isVerilogTask,
                 bool useInputFileSystem,
                 const SourceLocator& loc);

  //! destructor
  ~NUFOpenSysTask();

  // first define methods that override those of NUSysTask

  //! Return class name
  virtual const char* typeStr() const;

  //! returns true if \a net is def'd by this NUFOpenSysTask
  virtual bool isDefNet(NUNet* net) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Add the nets this stmt blockingly defines to the given set.
  void getBlockingDefs(NUNetSet *defs) const;
  void getBlockingDefs(NUNetRefSet *defs) const;
  bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! a NUFOpenSysTask kills the one temp net defined for it
  void getBlockingKills(NUNetSet * kills) const;
  void getBlockingKills(NUNetRefSet* kills) const;
  bool queryBlockingKills(const NUNetRefHdl & def_net_ref,
			  NUNetRefCompareFunction fn,
			  NUNetRefFactory* factory) const;

  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net);

  //! replace any net's def'd by this system call (a temp net defined to allow this system function act like a system task)
  virtual void replaceDef(NUNet *old_net, NUNet *new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);

  //! Return the type of this class
  virtual NUType getType() const;

  //! Return a copy of this statement; recurses to copy nested statements.
  virtual NUStmt* copy(CopyContext &copy_context) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;


  // now declare methods that are specific to NUFOpenSysTask

  //! return the output net
  NUNet* getOutNet() const;

  //! return the status net
  NUNet* getStatusNet() const;

  //! check for the proper number of arguments
  bool isValid(UtString* errmsg);

  //! get the lvalue for the output net
  NULvalue* getLvalue() const;

  //! get the lvalue for status net in VHDL
  NULvalue* getStatusLvalue() const;

private:
  NUIdentLvalue* mLvalue;
  /*! \brief The Status of VHDLs file_open procedure.
   *
   * One version of vhdl file_open() procedure uses its first arguement to
   * retrieve the status of file_open(). There are four different status 
   * defined by FILE_OPEN_STATUS type.
   */
  NUIdentLvalue* mStatusLvalue;
  bool mUseInputFileSystem;

  //! Hide copy and assign constructors.
  NUFOpenSysTask(const NUFOpenSysTask&);
  NUFOpenSysTask& operator=(const NUFOpenSysTask&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUFOpenSysTask : public NUSysTask

//! NUInputSysTask class 
/*!
    Nucleus object for Verilogs $fread $fscanf and VHDLs READ READLINE. 
    Syntactically, $fread $fscanf are  functions, and can be used anywhere in 
    expressions .  But because we need a temp variable to contain the filename 
    for conversion  it is more convenient to "hoist" it and treat it like
    a system task, and create a temp net that is used in its place in
    the expression. 
    But VHDLs READ and READLINE are Tasks (Procedures).
*/
class NUInputSysTask : public NUSysTask
{
public:
  //! constructor
  /*!
   * Creates a $fread ,  READ type system task
   *
   * \param name A module unique name for this task
   *
   * \param exprs arguments to Input SysTask
   *
   * \param lvalue An lvalue for the receiving net
   *
   * \param statusLvalue For receiving the status of READ task at runtime.
   *
   * \param module is the module where this system task appeared
   *
   * \param netRefFactory Factory to manage netref's - needed for u/d containers
   *
   * \param usesCFNet true when this needs $cfnet in it's use list
   *
   * \param isVerilogTask false when this is created by VHDL.
   *
   * \param isReadLineTask true when representing READLINE of VHDL.
   *
   * \param loc The location in the source code for this task.
   */
  NUInputSysTask(StringAtom* name,
                 NUExprVector& exprs,
                 NULvalue *lvalue,
                 NULvalue *statusLvalue,
                 const NUModule* module,
                 NUNetRefFactory *netRefFactory,
                 bool usesCFNet,
                 bool isVerilogTask,
                 bool isReadLineTask,
                 const SourceLocator& loc);

  //! destructor
  ~NUInputSysTask();

  // first define methods that override those of NUSysTask

  //! Return class name
  virtual const char* typeStr() const;

  //! returns true if \a net is def'd by this NUInputSysTask
  virtual bool isDefNet(NUNet* net) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Add the nets this stmt blockingly defines to the given set.
  void getBlockingDefs(NUNetSet *defs) const;
  void getBlockingDefs(NUNetRefSet *defs) const;
  bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  void getBlockingKills(NUNetSet * kills) const;
  void getBlockingKills(NUNetRefSet* kills) const;
  bool queryBlockingKills(const NUNetRefHdl & def_net_ref,
			  NUNetRefCompareFunction fn,
			  NUNetRefFactory* factory) const;
  
  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);

  //! Return the type of this class
  virtual NUType getType() const;

  //! Return a copy of this statement; recurses to copy nested statements.
  virtual NUStmt* copy(CopyContext &copy_context) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;


  // now declare methods that are specific to NUInputSysTask

  //! returns the file spec expression
  NUExpr* getFileSpec () const;

  //! check for the proper number of arguments
  bool isValid(UtString* errmsg);

  //! get the lvalue for the net to be written to.
  NULvalue* getReadLvalue() const { return mReadLvalue; }

  //! get the lvalue for the status net to be written to.
  NULvalue* getStatusLvalue() const { return mStatusLvalue; }

  //! returns true if representing READLINE of VHDL
  bool isReadLineTask() const { return mReadLineTask; }

private:
  NULvalue* mReadLvalue;   // For receiving the read value.

  NULvalue* mStatusLvalue; // For receiving the status of read operation.

  bool mReadLineTask;   // true when representing READLINE of VHDL 

  //! Hide copy and assign constructors.
  NUInputSysTask(const NUInputSysTask&);
  NUInputSysTask& operator=(const NUInputSysTask&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUInputSysTask : public NUSysTask

//! NUFFlushSysTask class 
/*!
    for $fflush
*/
class NUFFlushSysTask : public NUSysTask
{
public:
  //! constructor
  /*!
   * Creates a $fflush type system task
   *
   * \param name A module unique name for this task
   *
   * \param exprs, a vector (with a single entry) verilog specifier
   * for the file to be flushed, (the expression must evaluate to an integer when used.)
   *
   * \param module is the module where this system task appeared
   *
   * \param netRefFactory Factory to manage netref's - needed for u/d containers
   *
   * \param usesCFNet true when this needs $cfnet in it's use list
   *
   * \param loc The location in the source code for this task.
   */
  NUFFlushSysTask(StringAtom* name,
                  NUExprVector& exprs,
                  const NUModule* module,
                  NUNetRefFactory *netRefFactory,
                  bool usesCFNet,
                  const SourceLocator& loc);

  //! destructor
  ~NUFFlushSysTask();

  // first define methods that override those of NUSysTask

  //! Return class name
  virtual const char* typeStr() const;

  //! returns true if \a net is def'd by this NUFFlushSysTask
  virtual bool isDefNet(NUNet* net) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Return the type of this class
  virtual NUType getType() const;

  //! Return a copy of this statement; recurses to copy nested statements.
  NUStmt* copy(CopyContext &copy_context) const;

  // now declare methods that are specific to NUFFlushSysTask

  //! returns the file spec expression (always element 0 of expression vector)
  NUExpr* getFileSpec () const;

  //! check for the proper number of arguments
  bool isValid(UtString* msg);

private:

  //! Hide copy and assign constructors.
  NUFFlushSysTask(const NUFFlushSysTask&);
  NUFFlushSysTask& operator=(const NUFFlushSysTask&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUFFlushSysTask : public NUSysTask



//! NUControlSysTask class 
/*!
    for $stop and $finish
    Nucleus object for either of these control tasks.

    This class stores it's own U/D info since it cannot be calculated
    on the fly.
*/
class NUControlSysTask : public NUSysTask
{
public:

  //! constructor
  /*!
   * Creates a control type system task
   *
   * \param name A module unique name for this task
   *
   * \param controlType selects control type (one of: eCarbonStop or eCarbonFinish)
   *
   * \param exprs arguments to the task (or a default value of 1), it
   * must contain a single entry.
   *
   * \param module is the module where this system task appeared
   *
   * \param netRefFactory Factory to manage netref's - needed for u/d containers
   *
   * \param usesCFNet true when this needs $cfnet in it's use list
   *
   * \param loc The location in the source code for this task.
   */
  NUControlSysTask(StringAtom* name,
                   CarbonControlType controlType,
                   NUExprVector& exprs,
                   const NUModule* module,
                   NUNetRefFactory *netRefFactory,
                   bool usesCFNet,
                   const SourceLocator& loc);

  //! destructor
  ~NUControlSysTask();

  // first define methods that override those of NUSysTask

  //! Return the type of this class
  virtual NUType getType() const;

  //! Return class name
  virtual const char* typeStr() const;

  //! returns true if \a net is def'd by this NUControlSysTask
  virtual bool isDefNet(NUNet* net) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! Return a copy of this statement; recurses to copy nested statements.
  virtual NUStmt* copy(CopyContext &copy_context) const;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Add the nets this stmt uses for blocking defs to the given set.
  void getBlockingUses(NUNetSet *uses) const;
  void getBlockingUses(NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the nets this stmt uses to blockingly def the given net to the given set.
  void getBlockingUses(NUNet *net, NUNetSet *uses) const;
  void getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &def_net_ref,
			 const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the nets this stmt blockingly defines to the given set. (simply the $cfnet)
  void getBlockingDefs(NUNetSet *defs) const;
  void getBlockingDefs(NUNetRefSet *defs) const;
  bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the use_net as being used by the blocking def of def_net.
  void addBlockingUse(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);

  //! Add the net to the set of nets this stmt blockingly-defs.
  void addBlockingDef(const NUNetRefHdl &net_ref);

  //! Clear the use/def information stored.
  void clearUseDef();
  
  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;


  // now declare methods that are specific to NUControlSysTask

  //! get the value for the single argument of this sys task
  NUExpr* getVerbosity() const;

  //! get the reason for this control task
  CarbonControlType getReasonForControlOperation() const;

private:

  //! source of this control operation.
  CarbonControlType mControlType;

  // currently there is no need to maintain specific kill information
  // for  NUControlSysTask.

  /*! \brief Map of blocking defs to their uses
   *
   * Control flow operations need to maintain their own U/D
   * information.  $stop and $finish def the $cfnet with a use of all
   * 'live' values up to the $stop or finish.
   */
  NUNetRefNetRefMultiMap mBlockNetRefDefMap APIOMITTEDFIELD;

  //! Hide copy and assign constructors.
  NUControlSysTask(const NUControlSysTask&);
  NUControlSysTask& operator=(const NUControlSysTask&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUControlSysTask : public NUSysTask

#endif // _NUSYSTASK_H_
