// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _NU_EXPR_WALKER_H_
#define _NU_EXPR_WALKER_H_

#include "nucleus/Nucleus.h"

//! Class to help mutate top-level expressions in a design or module.
/*!
 *! This is a helper class to be used with Nucleus replaceLeaves and
 *! NuToNuFn functor.  This is for when you need to mutate nucleus
 *! expressions wherever they appear in the design, but only the
 *! top level expressions.  The custom mutator will manage the recursion
 *! into the expression tree.
 *!
 *! This is harder to do than it sounds.  NUDesignWalker can be used to
 *! find all the top level expressions, because it has eSkip.  But it is
 *! not good for replacing expressions in whatever nucleus object points
 *! to them (e.g. NUIf conditions, NUFor conditions, NUCase selects,
 *! RHS of assigns, etc).
 *!
 *! replaceLeaves is the only infrastructure that knows how to do these
 *! replacements, but it does not provide a mechanism to avoid recursing,
 *! or even a reliable mechanism to know when you are at the "top level".
 *! Maintaining a depth count via ePre and ePost does not work, because
 *! of the breadth-first style of the Stmt replaceLeaves functions --
 *! they tend to call Pre on all their member vars, and then Post on all
 *! their member vars.  See NUEnabledDriver::replaceLeaves for an example.
 *!
 *! This infrastructure provides a robust mechanism for walking, and
 *! optionally replacing, top level expressions only.  It was originally
 *! developed for factory-based expression Folding but was then transformed
 *! into a re-usable library when I decided I needed it for ExplicitSize.
 *!
 *! A user of this class should provide a processExpr() method.
 */

class NUExprReplace : public NuToNuFn {
public:
  //! ctor
  NUExprReplace();

  //! dtor
  ~NUExprReplace();

  //! The user of NUExprMutator must override this method.
  /*!
   *! He may also override non-Expr operator() methods of NuToNuFn,
   *! but must not override operator()(NUExpr*), as this class must
   *! handle that method.
   */
  virtual NUExpr* processExpr(NUExpr*) = 0;

  //! The user of this class should not override this method
  NUExpr* operator() (NUExpr*, Phase);

private:
  NUCExprHashSet* mTopLevelExprSet;
  NUCExprHashSet* mSubExprSet;
};

#endif //  _NU_EXPR_WALKER_H_
