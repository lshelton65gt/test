// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _NU_CASE_H_
#define _NU_CASE_H_

#include "nucleus/NUBlockStmt.h"
class Fold;

//! NUCaseCondition class
class NUCaseCondition : public NUBase
{
public:
  //! constructor
  NUCaseCondition(const SourceLocator& loc,
                  NUExpr* expr,        // ownership transfer
                  NUExpr* mask = NULL, // ownership transfer
                  bool rangeCond = false)
    : mLoc(loc),
      mExpr(expr),
      mMask(mask),
      mIsRange(rangeCond)
  {
  }

  //! destructor
  ~NUCaseCondition();

  //! Equivalence
  bool operator==(const NUCaseCondition &) const;

  //! Build a boolean expression indicating whether this condition is true.
  //! The caller must free this expression when finished with it.
  NUExpr* buildConditional(const NUExpr* sel) const;

  //! Is this condition appropriate for code-generating into a C switch
  bool isSwitchable() const {return (mMask == NULL) && (!mIsRange);}

  //! Get all the nets used in the condition expressions
  void getUses(NUNetSet* uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! get the unmasked expression used for comparisons
  NUExpr* getExpr() const {return mExpr;}

  //! Change this condition's expression
  /*!
   * The caller is responsible to make sure that old expression is not leaked.
   */
  void replaceExpr(NUExpr* expr) { mExpr = expr; }
  
  //! get the mask used for comparisons
  NUExpr* getMask() const {return mMask;}

  //! Change this condition's mask expression
  /*!
   * The caller is responsible to make sure that old mask expression is not leaked.
   */
  void replaceMask(NUExpr* mask) { mMask = mask; }
  
  //! get the location
  const SourceLocator& getLoc() const {return mLoc;}

  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! make a new copy of this condition
  NUCaseCondition* copy(CopyContext &copy_context) const;

  //! print condition expressions to cout
  void print(bool recurse, int indent) const;

  //! Return class name
  const char* typeStr() const;

  //!Code Generator
  CGContext_t emitCode(CGContext_t context) const;

  //! print human friendly
  virtual void compose (UtString*, const STBranchNode* scope, int indent, bool recurse) const;

private:
  SourceLocator mLoc;
  NUExpr* mExpr;
  NUExpr* mMask;
  bool    mIsRange;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCaseCondition : public NUBase

//! NUCaseItem class
/*!
 * A single item in a case statement. This is made up of an expression
 * list and statement list pair.
 */
class NUCaseItem : public NUBase
{
public:
  typedef UtVector<NUCaseCondition*> ConditionVector;

  //! constructor
  /*!
   * \param loc source locator
   */
  NUCaseItem(const SourceLocator& loc) :
    mLoc(loc)
  {
  }

  //! destructor
  ~NUCaseItem();

  //! Get the statement list
  NUStmtCLoop loopStmts() const {return NUStmtCLoop(mStmts);}
  NUStmtLoop loopStmts() {return NUStmtLoop(mStmts);}

  //! add a statement to the case item
  void addStmt(NUStmt* stmt) {mStmts.push_back(stmt);}

  void addStmts(NUStmtList *stmts) {
    mStmts.insert(mStmts.end(), stmts->begin(), stmts->end());
  }

  void addStmtStart(NUStmt *stmt) {
    mStmts.push_front(stmt);
  }

  //! Remove the given statement from the item
  void removeStmt(NUStmt *stmt, NUStmt *replacement=0);

  //! return a copy of the statement list to be freed by the caller
  NUStmtList* getStmts() const;

  //! add a condition for the case item
  void addCondition(NUCaseCondition* cond)
  {
    mConditions.push_back(cond);
  }

  //! return true if this is a default item (ie., has no associated condition)
  bool isDefault() const
  {
    return mConditions.empty();
  }

  typedef CLoop<ConditionVector> ConditionCLoop;
  typedef Loop<ConditionVector> ConditionLoop;

  //! Loop over all conditions
  ConditionLoop loopConditions() {return ConditionLoop(mConditions);}

  //! Loop over all conditions
  ConditionCLoop loopConditions() const {return ConditionCLoop(mConditions);}

  //! Create a single expression that evaluates the OR of all conditions
  NUExpr* buildConditional(const NUExpr* sel) const;

  //! Does this item have any conditions?
  bool hasConditions() const { return (not mConditions.empty()); }

  //! equivalence
  bool operator==(const NUCaseItem&) const;

  //! get the location
  const SourceLocator& getLoc() const {return mLoc;}

  //! Replace the case conditions with the given conditions
  /*!
   * The given vector is copied.  The user of this must make
   * sure that no conditions leak.
   */
  void replaceConditions(ConditionVector &new_conds);

  //! Replace the current statement list with the given statement list.
  /*!
   * The given list is copied. This is used to reorder non-blocking
   * temporaries. The caller is responsibe to make sure that
   * statements passed it are not leaked.
   *
   * \param new_stmts A list of statements in a new order.
   */
  void replaceStmts(const NUStmtList& new_stmts);

  void replaceDef(NUNet * old_net, NUNet * new_net);

  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! debug print routine
  void print(bool recurse, int indent) const;

  //! Return a copy of this.
  NUCaseItem* copy(CopyContext &copy_context) const;

  //! Return class name
  const char* typeStr() const;

  //!Code Generator
  CGContext_t emitCode(CGContext_t context) const;

  //! print human friendly
  virtual void compose (UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Clear all the condition expressions, deleting them if desired
  void clearConditions(bool deleteConditions);

private:
  //! Hide copy and assign constructors.
  NUCaseItem(const NUCaseItem&);
  NUCaseItem& operator=(const NUCaseItem&);

  SourceLocator mLoc;

  //! List of case match expressions
  ConditionVector mConditions;

  //! List of case statements
  NUStmtList mStmts;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCaseItem : public NUBase

//! NUCase class
/*!
 * A case statement
 */
class NUCase : public NUBlockStmt
{
public:
  enum CaseType {
    eCtypeCase,                      //!< defined as case stmt (x,z,? must match exaxtly)
    eCtypeCasez,                     //!< defined as casez (z and ? match anything)
    eCtypeCasex                      //!< defined as casex (x,z,? match anything)
  };
  
  //! constructor
  /*!
   * \param sel The case selector expression
   * \param netref_factory Factory to manage netref's - needed for u/d containers
   * \param usesCFNet true when this needs $cfnet in it's use list
   * \param loc Source location.
  */
  NUCase(NUExpr* sel,
	 NUNetRefFactory *netref_factory,
         CaseType type,
         bool usesCFNet,
	 const SourceLocator& loc);
  
  //! Return the type of this class
  virtual NUType getType() const { return eNUCase; }


  //! Return the case selector
  NUExpr* getSelect() const { return mSel; }

  //  typedef Loop<const NUCaseItemList,
  //  NUCaseItemList::const_iterator> ItemCLoop;
  typedef CLoop<NUCaseItemList> ItemCLoop;
  typedef Loop<NUCaseItemList> ItemLoop;

  typedef CRLoop<NUCaseItemList> ItemCRLoop;
  typedef RLoop<NUCaseItemList> ItemRLoop;
  
  //! add a new case item, transferring ownership
  void addItem(NUCaseItem* item) {mCaseItems.push_back(item);}

  //! remove a case item
  void removeItem (NUCaseItem* item) {
    mCaseItems.erase (std::remove (mCaseItems.begin (), mCaseItems.end (), item),
		      mCaseItems.end ());
  }

  //! replace case items
  void replaceItems(const NUCaseItemList& newList);

  //! Iterate over the case item list without messing with it
  ItemCLoop loopItems() const {return ItemCLoop(mCaseItems);}

  //! Return the case item list, allowing mutation to each case item
  ItemLoop loopItems() {return ItemLoop(mCaseItems);}

  //! Iterate over the case item list without messing with it
  ItemCRLoop loopItemsReverse() const {return ItemCRLoop(mCaseItems);}

  //! Return the case item list, allowing mutation to each case item
  ItemRLoop loopItemsReverse() {return ItemRLoop(mCaseItems);}

  //! Returns whether the user specified a full case directive
  bool isUserFullCase() const { return mUserFullCase; }

  //! Put whether the user specified a full case directive
  void putUserFullCase(bool flag) { mUserFullCase = flag; }

  //! Returns whether the case is fully enumerated (without using a default)
  bool isFullySpecified() const { return mFullySpecified; }

  //! Put whether the case is fully enumerated (without using a default)
  void putFullySpecified(bool flag) { mFullySpecified = flag; }

  //! Returns whether this case has a default
  bool hasDefault() const { return mHasDefault; }

  //! Put whether this case-statement has a default.
  void putHasDefault(bool flag) { mHasDefault = flag; }

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace the select
  /*!
   * The caller is responsible to make sure that old select is not leaked.
   */
  void replaceSelect(NUExpr* select);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose (UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! Return class name
  const char* typeStr() const;

  //! returns the type of case: case, casez, casex
  CaseType getCaseType() const;

  //! Return a copy of this statement; recurses to copy nested statements.
  NUStmt* copy(CopyContext &copy_context) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! should this case be emitted as a switch? (ignoreMask == true allows don't cares)
  bool canEmitSwitch (bool ignoreMask) const;
  
  //! Equivalence
  bool operator==(const NUStmt&) const;

  bool isConsistent() const;

  //! examine a the case statement and reduce the size of the case selector and branch labels.
  //! This is only possible if the selector is a variable with an
  //! effective bit size smaller than actual size, and all case labels
  //! are constants.  If a case label (NUCaseCondition) is found to
  //! have a value that is unselectable then that NUCaseCondition is
  //! removed, and the case branch if the removed condtion was the
  //! last condition  for that branch.
  void eliminateUnnecessaryCaseBits(Fold* fold);


  //! destructor
  ~NUCase();

private:
  //! Hide copy and assign constructors.
  NUCase(const NUCase&);
  NUCase& operator=(const NUCase&);

  //! Case select
  NUExpr* mSel;

  //! Case item list
  NUCaseItemList mCaseItems;

  /*! \brief If set, either the user specified a full case directive in Verilog, or the design is in VHDL.
   * 
   * This flag means that the user has stated, either explicitly via an
   * inline directive or implicitly via language choice, that every
   * possible value of the case selector is covered by one of the case
   * branches, possibly requiring the default case item.
   */
  bool mUserFullCase;

  /*! \brief If set, the case is fully enumerated WITHOUT the need of a default case item.
   *
   * This is a computed value.  Computed for verilog and VHDL during
   * population.  If mFullySpecified is true then the NUCase will not
   * have a default branch and mHasDefault flag will be false. 
   * Setting mFullySpecified is favored over mHasDefault
   * except during aggressive folding of case stmts where the default is
   * used to handle one of the case branches.
   * See bug3608 for more discussion.
   */
  bool mFullySpecified;

  /*! \brief If set, the NUcase has a default item.
   *
   * Will not be set if mFullySpecified is set.  Note that the
   * original HDL for a case stmt may have included a default but
   * analysis of the NUCase removed it (and set mFullySpecified)
   * because mFullySpecified is favored over mHasDefault.
   */
  bool mHasDefault;

  /*! identifies which style of case stmt this is */
  CaseType mCaseType;

  DECLARENUCLEUSCLASSFRIENDS();
  
} APIDISTILLCLASS; //class NUCase : public NUBlockStmt


#endif // _NU_CASE_H_
