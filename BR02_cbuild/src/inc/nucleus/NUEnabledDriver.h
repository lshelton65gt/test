// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUENABLEDDRIVER_H_
#define NUENABLEDDRIVER_H_

#include "nucleus/NUStmt.h"
#include "util/SourceLocator.h"

class Fold;

/*!
 \file
 NUEnabledDriver class declaration.
*/


//! NUEnabledDriver class
/*!
 * This class models a driver which can drive a Z onto a net.
 *
 * This class behaves like a module-level continous driver.
 */
class NUEnabledDriver : public NUStmt
{
public:
  //! constructor
  /*!
    \param lvalue The lvalue being driven.
    \param enable The enable expression.
    \param driver The expression being driven when enabled.
    \param strength The strength of the drive, when enabled.
    \param usesCFNet true when this needs $cfnet in it's use list
    \param loc The source location of this initialization.
  */
  NUEnabledDriver(NULvalue *lvalue,
		  NUExpr *enable,
		  NUExpr *driver,
		  Strength strength,
                  bool usesCFNet,
		  const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUEnabledDriver; }

  //! Return the LHS of the assign.
  NULvalue* getLvalue() const { return mLvalue; }

  //! Return the enable.
  NUExpr* getEnable() const { return mEnable; }

  //! Return the enabled driver.
  NUExpr* getDriver() const { return mDriver; }

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose (UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Equivalence test
  bool operator==(const NUStmt&) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! Print the name for this enabled driver if it exists
  virtual void printName() const;

  //! Return class name
  const char* typeStr() const;

  //! Return the strength of the drive of this node.
  Strength getStrength() const { return mStrength; }

  //! Return the parent module
  const NUModule* findParentModule() const;
  NUModule* findParentModule();

  //! add the cost of this into the NUCost*.
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUEnabledDriver();

  //! Can this node drive a Z on the specified net?
  virtual bool drivesZ(NUNet*) const;

protected:
  //! Hide copy and assign constructors.
  NUEnabledDriver(const NUEnabledDriver&);
  NUEnabledDriver& operator=(const NUEnabledDriver&);

  //! LHS
  NULvalue *mLvalue;

  //! Enable expression
  NUExpr *mEnable;

  //! Driving expression
  NUExpr *mDriver;

  //! Strength of driver
  Strength mStrength;
} APIOMITTEDCLASS ; //class NUEnabledDriver : public NUStmt

//! NUBlockingEnabledDriver class
/*!
 * An enabled driver in an always block.
 */
class NUBlockingEnabledDriver : public NUEnabledDriver
{
public:
  //! constructor
  /*!
    \param lvalue The lvalue being driven.
    \param enable The enable expression.
    \param driver The expression being driven when enabled.
    \param strength The strength of the drive, when enabled.
    \param usesCFNet true when this needs $cfnet in it's use list
    \param loc The source location of this initialization.
  */
  NUBlockingEnabledDriver(NULvalue *lvalue,
                          NUExpr *enable,
                          NUExpr *driver,
                          Strength strength,
                          bool usesCFNet,
                          const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUBlockingEnabledDriver; }

  //! Return class name
  const char* typeStr() const;

  //! Return a copy of this statement; recurses to copy nested statements.
  virtual NUStmt* copy(CopyContext &copy_context) const;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Add the nets this node uses for blocking defs to the given set.
  void getBlockingUses(NUNetSet *uses) const;
  void getBlockingUses(NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to blockingly def the given net to the given set.
  void getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &def_net_ref,
			 const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! No-op
  void getNonBlockingUses(NUNetSet * /* uses -- unused */) const {}
  void getNonBlockingUses(NUNetRefSet * /* uses -- unused */) const {}
  bool queryNonBlockingUses(const NUNetRefHdl & /* use_net_ref -- unused */,
			    NUNetRefCompareFunction /* fn -- unused */,
			    NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! No-op
  void getNonBlockingUses(const NUNetRefHdl & /* net_ref -- unused */,
			  NUNetRefSet * /* uses -- unused */) const {}
  bool queryNonBlockingUses(const NUNetRefHdl & /* def_net_ref -- unused */,
			    const NUNetRefHdl & /* use_net_ref -- unused */,
			    NUNetRefCompareFunction /* fn -- unused */,
			    NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this node blockingly defines to the given set.
  void getBlockingDefs(NUNetSet *uses) const;
  void getBlockingDefs(NUNetRefSet *uses) const;
  bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! No-op
  void getNonBlockingDefs(NUNetSet * /* uses -- unused */) const {}
  void getNonBlockingDefs(NUNetRefSet * /* uses -- unused */) const {}
  bool queryNonBlockingDefs(const NUNetRefHdl & /* def_net_ref -- unused */,
			    NUNetRefCompareFunction /* fn -- unused */,
			    NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this node blockingly kills to the given set.
  void getBlockingKills(NUNetSet * kills) const;
  void getBlockingKills(NUNetRefSet * kills) const;
  bool queryBlockingKills(const NUNetRefHdl &def_net_ref,
			  NUNetRefCompareFunction fn,
			  NUNetRefFactory *factory) const;

  //! No-op
  void getNonBlockingKills(NUNetSet * /* kills -- unused */) const {}
  void getNonBlockingKills(NUNetRefSet * /* kills -- unused */) const {}
  bool queryNonBlockingKills(const NUNetRefHdl & /* def_net_ref -- unused */,
			     NUNetRefCompareFunction /* fn -- unused */,
			     NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

private:
  //! Hide copy and assign constructors.
  NUBlockingEnabledDriver(const NUBlockingEnabledDriver&);
  NUBlockingEnabledDriver& operator=(const NUBlockingEnabledDriver&);
} APIOMITTEDCLASS; // class NUBlockingEnabledDriver : public NUEnabledDriver

//! NUContEnabledDriver class
/*!
 * A continous version of the enabled driver. This is the original
 * type of enabled driver created by population.
 *
 * Note: these are not really statements, but deriving from NUStmt to
 * get code reuse. So we need to define the various blocking and
 * non-blocking use/def/kill functions, even though they can never be
 * called.
 */
class NUContEnabledDriver : public NUEnabledDriver
{
public:
  //! constructor
  /*!
    \param name The name of this node, for code generation.
    \param lvalue The lvalue being driven.
    \param enable The enable expression.
    \param driver The expression being driven when enabled.
    \param strength The strength of the drive, when enabled.
    \param usesCFNet true when this needs $cfnet in it's use list
    \param loc The source location of this initialization.
  */
  NUContEnabledDriver(StringAtom *name,
                      NULvalue *lvalue,
                      NUExpr *enable,
                      NUExpr *driver,
                      Strength strength,
                      bool usesCFNet,
                      const SourceLocator& loc);

  //! destructor
  ~NUContEnabledDriver() {}

  //! Return the type of this class
  virtual NUType getType() const { return eNUContEnabledDriver; }

  //! Return a string identifying this type.
  virtual const char* typeStr() const;
  
  //! Return a copy of this statement; recurses to copy nested statements.
  virtual NUStmt* copy(CopyContext &copy_context) const;

  //! Return the name of this node.
  /*!
   * We name these for code generation; there is no scoping use of the name.
   */
  StringAtom* getName() const { return mName; }

  //! Print the name for this enabled driver if it exists
  virtual void printName() const;

  //! Returns a blocking enabled driver copy of this continous enabled driver
  /*! This is used when we merge enabled drivers to make bigger blocks.
   */
  NUStmt* copyBlocking() const;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Is this node a continuous driver?
  virtual bool isContDriver() const { return true; }

  //! Invalid to call.
  void getBlockingUses(NUNetSet *uses) const;
  void getBlockingUses(NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &def_net_ref,
			 const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getNonBlockingUses(NUNetSet *uses) const;
  void getNonBlockingUses(NUNetRefSet *uses) const;
  bool queryNonBlockingUses(const NUNetRefHdl &use_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getNonBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryNonBlockingUses(const NUNetRefHdl &def_net_ref,
			    const NUNetRefHdl &use_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getBlockingDefs(NUNetSet *uses) const;
  void getBlockingDefs(NUNetRefSet *uses) const;
  bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getNonBlockingDefs(NUNetSet *uses) const;
  void getNonBlockingDefs(NUNetRefSet *uses) const;
  bool queryNonBlockingDefs(const NUNetRefHdl &def_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getBlockingKills(NUNetSet *kills) const;
  void getBlockingKills(NUNetRefSet *kills) const;
  bool queryBlockingKills(const NUNetRefHdl &def_net_ref,
			  NUNetRefCompareFunction fn,
			  NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getNonBlockingKills(NUNetSet *kills) const;
  void getNonBlockingKills(NUNetRefSet *kills) const;
  bool queryNonBlockingKills(const NUNetRefHdl &def_net_ref,
			     NUNetRefCompareFunction fn,
			     NUNetRefFactory *factory) const;

  //! Use this method to decide whether to call the 'getBlocking*' or 'get*' methods
  virtual bool useBlockingMethods() const;

  //! Is this driver scheduled?
  bool isScheduled (void) const;

  //! Mark the scheduled state.
  void putScheduled (bool f);

private:
  //! Hide copy and assign constructors.
  NUContEnabledDriver(const NUContEnabledDriver&);
  NUContEnabledDriver& operator=(const NUContEnabledDriver&);

  //! Name of this node.
  StringAtom* mName;
  //! Scheduled flag
  bool mScheduled;
} APIOMITTEDCLASS; // class NUContEnabledDriver : public NUEnabledDriver

#endif
