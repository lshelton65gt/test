// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#ifndef NUCONTROLFLOWNET_H_
#define NUCONTROLFLOWNET_H_

#ifndef NUVIRTUALNET_H_
#include "nucleus/NUVirtualNet.h"
#endif

//! NUControlFlowNet class
/*!
 * This class is an artificial net used to maintain proper control
 * flow between system tasks like $stop or $finish and other
 * operations.  Each module has one of these nets declared in it, but
 * all such nets in a design are aliased together (to reduce nets)
 * \sa NUOutputControlFlow for a artificial net used to keep things alive/visible
 */
class NUControlFlowNet : public NUVirtualNet
{
public:
  //! Constructor
  /*!
    \param name The name to be used for the net
    \param flags Attribute flags
    \param scope Scope in which net is declared
    \param loc Source location of declaration of the net
   */
  NUControlFlowNet(StringAtom *name,
                   NetFlags flags,
                   NUScope *scope,
                   const SourceLocator& loc);

  //! Destructor
  virtual ~NUControlFlowNet();

  //! Return class name
  const char* typeStr() const;

  //! easy way to tell if this is a ControlFlowNet
  bool isControlFlowNet() const { return true; }

  // Compose a declaration string for the net, e.g. "reg q;" or "wire [7:0] z;" or "input clk;"
  void composeDeclaration(UtString*) const;

  //! Helper function for print, prints the name and size information
  void printNameSize() const;

  //! return true because we want any NUControlFlowNet to always be there
  bool isProtectedObservable(const IODBNucleus* /* iodb */ ) const { return true; };


private:
  //! Hide copy and assign constructors.
  NUControlFlowNet(const NUControlFlowNet&);
  NUControlFlowNet& operator=(const NUControlFlowNet&);

} APIOMITTEDCLASS; //class NUControlFlowNet : public NUVirtualNet

#endif // NUCONTROLFLOWNET_H_
