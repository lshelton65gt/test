// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NU_TO_NU_H_
#define NU_TO_NU_H_


//! Provide an interface for leaf translation as we walk Nucleus.
/*!
 * This callback functor is used by all NUUseNode::replaceLeaves calls.
 */
class NuToNuFn
//  : public std::binary_function<const NUBase *, NUBase *, enum TOrder>
{
public:
  virtual ~NuToNuFn() {}

  /*!
   * Does the current visit come before or after recursing through the
   * current node?
   *
   * Note: ePre and ePost are not always mutually exclusive. Some
   * leaf-level visits use the ePrePost flag, which covers both types
   * of visit with a single call.
   */
  enum Phase {
    ePre=1,               //!< Pre-node walk.
    ePost=2,              //!< Post-node walk.
    ePrePost = ePre|ePost //!< Single call covering both pre and post walk (leaf nodes)
  };

  //! Does this visit come before recursing through the current node?
  static bool isPre(Phase phase) { return (phase & ePre)!=0; }

  //! Does this visit come after recursing through the current node?
  static bool isPost(Phase phase) { return (phase & ePost)!=0; }

  //! Convert references from one net to another.
  /*!
   * Memory management of the incoming net is not required.
   *
   * \return non-NULL iff the current reference to the incoming net
   * should be replaced.
   */
  virtual NUNet * operator()(NUNet *, Phase) { return NULL; }

  //! Convert references from one task to another.
  /*!
   * Memory management of the incoming tf is not required.
   *
   * \return non-NULL iff the current reference to the incoming tf
   * should be replaced.
   */
  virtual NUTF * operator()(NUTF *, Phase) { return NULL; }

  //! Convert references from one cmodel to another.
  /*!
   * This callback allows an implementation to rewrite NUCModel
   * references contained within NUCModelCall objects.
   *
   * Memory management of the incoming cmodel is not required.
   *
   * \return non-NULL iff the current reference to the incoming cmodel
   * should be replaced.
   */
  virtual NUCModel * operator()(NUCModel *, Phase) { return NULL; }

  //! Convert references from one always block to another.
  /*!
   * This callback allows an implementation to rewrite NUAlwaysBlock
   * references contained within other NUAlwaysBlock objects. In
   * particular, this callback is used to update clock and priority
   * pointers.
   *
   * Memory management of the incoming always block is not required.
   *
   * \return non-NULL iff the current reference to the incoming always
   * block should be replaced.
   */
  virtual NUAlwaysBlock * operator()(NUAlwaysBlock *, Phase) { return NULL; }

  //! Convert references from one LHS to another.
  /*!
   * If a replacement is requested, the current reference to the
   * incoming LHS will be eliminated. The implemetation is responsible
   * for memory management of discarded objects. The incoming LHS
   * should be deleted or otherwise managed to avoid a memory leak.
   *
   * \return non-NULL iff the current reference to the incoming LHS
   * should be replaced.
   */
  virtual NULvalue * operator()(NULvalue *, Phase) { return NULL; }


  //! Convert references from one expression to another.
  /*!
   * If a replacement is requested, the current reference to the
   * incoming expression will be eliminated. The implemetation is
   * responsible for memory management of discarded objects. The
   * incoming expression should be deleted or otherwise managed to
   * avoid a memory leak.
   *
   * \return non-NULL iff the current reference to the incoming
   * expression should be replaced.
   */
  virtual NUExpr * operator()(NUExpr *, Phase) { return NULL; }

  //! Convert references from a net to an expression.
  /*! 
   * This is intended for use when creating elaborated expressions
   * from unelaborated ones, usually during expression synthesis.
   *
   * Memory management of the incoming net is not required.
   *
   * \return non-NULL iff the current reference to the incoming net
   * should be replaced with an expression.
   */
  virtual NUExpr * netToExpr(NUNet*, Phase) { return NULL; }

  //! Convert references from an elaborated net to an expression.
  /*! 
   * This is intended for use during expression synthesis when
   * expressions are created in terms of elaborated nets.
   *
   * Memory management of the incoming net is not required.
   *
   * \return non-NULL iff the current reference to the incoming net
   * should be replaced with an expression.
   */
  virtual NUExpr* netElabToExpr(NUNetElab*, Phase) { return NULL; }

#if 0
  // seems like eDepthFirst <==> ePost and eBreadthFirst <==> ePre
  enum TOrder { eDepthFirst=1,    //!< Recurse before translating (bottom-up)
                eBreadthFirst=2,	//!< Recurse after translating
                ePrePost=3 };	//!< both pre-and-post (leaf node)
  inline bool isDepthFirst (TOrder order) { return (order & eDepthFirst)!=0;}
  inline bool isBreadthFirst (TOrder order){ return (order & eBreadthFirst)!=0;}
#endif
};


#endif
