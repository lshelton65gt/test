// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NU_MEMORYNETHIERREF_H_
#define NU_MEMORYNETHIERREF_H_

#include "nucleus/NUNet.h"

//! Hierarchical reference to a memory net
class NUMemoryNetHierRef : public NUMemoryNet
{
public:
  //! constructor
  /*!
    \param name Name of the net (includes path)
    \param width_range Declared width range of this memory
    \param depth_range Declared depth range of this memory
    \param flags The declared flags for the net
    \param flags The declared vectorflags for the net
    \param ref Hierarchical path to the net
    \param scope Scope in which hierarchical ref occurs.
    \param resynth should this memory be considered resynthesized?
    \param loc Source location
  */
  NUMemoryNetHierRef(const AtomArray& path,
		     ConstantRange *width_range,
		     ConstantRange *depth_range,
                     NetFlags flags,
                     VectorNetFlags vflags,
		     NUScope *scope,
                     bool resynth,
		     const SourceLocator &loc);

  //! constructor
  /*!
    \param name Name of the net (includes path)
    \param rangeArray Array of dimension ranges for this memory
    \param flags The declared flags for the net
    \param vflags The declared vectornetflags for the net
    \param ref Hierarchical path to the net
    \param scope Scope in which hierarchical ref occurs.
    \param resynth should this memory be considered resynthesized?
    \param loc Source location
  */
  NUMemoryNetHierRef(const AtomArray& path,
		     ConstantRangeArray *rangeArray,
                     NetFlags flags,
                     VectorNetFlags vflags,
		     NUScope *scope,
                     bool resynth,
		     const SourceLocator &loc);

  //! destructor
  ~NUMemoryNetHierRef();

  bool isHierRef() const { return true; }

  //! Illegal to call.
  void addHierRef(NUBase *net_hier_ref);

  //! Illegal to call.
  void removeHierRef(NUBase *net_hier_ref);

  //! Return true if this net is read outside the net's scope
  bool isHierRefRead() const;

  //! Return true if this net is written outside the net's scope
  bool isHierRefWritten() const;

  //! Are any of the possible resolutions protected mutable due to non-hierref reasons?
  /*!
   * This returns true for subset of when isProtectedMutable returns
   * true, and should only be used when you specifically don't
   * want hierrefs to make a net protected.
   */
  bool isProtectedMutableNonHierref(const IODBNucleus* iodb,
                                    bool checkPrimary = true) const;

  //! Return the NUNetHierRef for this hierref.
  NUNetHierRef* getHierRef() { return &mNetHierRef; }
  const NUNetHierRef* getHierRef() const { return &mNetHierRef; }

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Illegal to call; cannot create hierrefs to hierrefs.
  NUNet* createHierRef(const AtomArray& name, NUScope *scope, NetFlags flags, const SourceLocator &loc);

protected:
  //! Helper function for print, prints the name and size information
  void printNameSize() const;

private:
  //! Hide copy and assign constructors.
  NUMemoryNetHierRef(const NUMemoryNetHierRef&);
  NUMemoryNetHierRef& operator=(const NUMemoryNetHierRef&);

  //! Hierref resolution object.
  NUNetHierRef mNetHierRef;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUMemoryNetHierRef : public NUMemoryNet

#endif
