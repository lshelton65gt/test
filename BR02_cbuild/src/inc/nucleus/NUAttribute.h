// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUATTRIBUTE_H_
#define NUATTRIBUTE_H_

#include "nucleus/NUCopyContext.h"
#include "nucleus/NUExpr.h"
#include "util/SourceLocator.h"
#include "bdd/BDD.h"

class NUNet;

//! NUAttribute class
/*!
 * This represents any VHDL or SystemVerilog attributes that may need to
 * be temporarily populated.  These probably won't ever exist past the
 * population phase; they currently are required not to via a
 * NucleusSanity check.
 */
class NUAttribute : public NUExpr
{
public:
  //! Attributes.  This list is taken from the Jaguar attribute list
  enum AttribT
  {
    eStart = 0,             //!< Unused
    eLeft,
    eRight,
    eHigh,
    eLow,
    eLength,
    eStructure,
    eBehavior,
    eValue,
    ePos,
    eVal,
    eSucc,
    ePred,
    eLeftof,
    eRightof,
    eEvent,
    eActive,
    eLastEvent,
    eLastValue,
    eLastActive,
    eDelayed,
    eStable,
    eQuiet,
    eTransaction,
    eBase,
    eRange,
    eReverseRange,
    eAscending,
    eImage,
    eDriving,
    eDrivingValue,
    eSimpleName,
    eInstanceName,
    ePathName,
    eUserDefAttrib,
    eVhStart = eLeft,        //!< Start of VHDL attributes
    eVhEnd = eUserDefAttrib, //!< End of VHDL attributes
    eInvalid                 //!< Bad operator
  };

  //! constructor
  /*!
    \param net The target net of the attribute
    \param attrib The attribute type
    \param loc Source location.
   */
  NUAttribute(NUExpr *net, const AttribT attrib, const SourceLocator& loc);

  //! Return the attribute
  AttribT getAttrib() const { return mAttrib; }

  //! Get source location
  const SourceLocator& getLoc() const { return mLoc; }

  //! Return if the operator is a VHDL attribute
  static bool isVhdlAttribute(AttribT attrib) { 
    return (attrib >= eVhStart) && (attrib <= eUserDefAttrib); }

  NUExpr *getExpr() const { return mExpr; }

  //! Return class name
  virtual const char* typeStr() const;

  //! Return the char* string for this attribute
  const char* getAttribChar() const;

  //! Dump myself to cout
  void print(bool recurse = true, int indent = 0) const;

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUAttribute; }

  virtual UInt32 determineBitSizeHelper() const { return 1; } //good enough for event & stable

  virtual void resizeHelper(UInt32 /*size*/);

  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet* /*old_net*/, NUNet* /*new_net*/)
    { NU_ASSERT(0, this); return false; }

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves( NuToNuFn & translator );

  //! Cost estimation function
  virtual void calcCost(NUCost*, NUCostContext* = 0) const
    { NU_ASSERT(0, this); }

  virtual void getUses(NUNetSet *uses) const;
  virtual void getUses(NUNetRefSet *uses) const;
  virtual void getUsesElab(STBranchNode* /*scope*/, NUNetElabSet* /*uses*/) const
    { NU_ASSERT(0, this); }
  virtual void getUsesElab(STBranchNode* /*scope*/, NUNetElabRefSet2* /*uses*/) const
    { NU_ASSERT(0, this); }
  virtual bool queryUses(const NUNetRefHdl &/*use_net_ref*/,
			 NUNetRefCompareFunction /*fn*/,
			 NUNetRefFactory* /*factory*/) const {
    NU_ASSERT(0, this);
    return false;
  }

  //! Return the number of arguments to this operator.
  virtual UInt32 getNumArgs() const;

  //! Helper virtual function, which will always get an expr of the same type to cast
  virtual ptrdiff_t compareHelper(const NUExpr* /*otherExpr*/, bool /*compareLocator*/,
                                  bool /*comparePointer*/, bool /*compareSizeOfConsts*/) const 
    {
      NU_ASSERT(0, this);
      return (ptrdiff_t)0;
    }

  //! Generate a BDD for this expression
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  virtual BDD bdd(BDDContext *ctx, STBranchNode* /*scope=0*/) const
    { return ctx->invalid(); }

  virtual void composeHelper(UtString* buf, const STBranchNode* /*scope*/,
                             bool /*includeRoot*/, bool /*hierName*/,
                             const char* /*separator*/ ) const;

  //! hash an expression
  virtual size_t hash() const {
    NU_ASSERT(0, this);
    return (size_t)0;
  }

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const {
    NU_ASSERT(0, this);
    return (size_t)0;
  }

  //! destructor
  virtual ~NUAttribute();

protected:
  //! Return a copy of this expression tree.
  virtual NUExpr* copyHelper(CopyContext &/*copy_context*/) const;

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual const NUExpr* getArgHelper(UInt32 index) const;

  //! Mutate the argument
  virtual NUExpr* putArgHelper(UInt32 index, NUExpr*);

private:
  //! Hide copy and assign constructors.
  NUAttribute(const NUAttribute&);
  NUAttribute& operator=(const NUAttribute&);

  NUExpr *mExpr;       //! Net this is an attribute of
  AttribT mAttrib;     //! The attribute type
  SourceLocator mLoc;  //! SourceLocator

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUAttribute : public NUExpr
#endif
