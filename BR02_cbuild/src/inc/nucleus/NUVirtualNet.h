// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/



#ifndef NUVIRTUALNET_H_
#define NUVIRTUALNET_H_

#ifndef NUNET_H_
#include "nucleus/NUNet.h"
#endif

#if NUBITNETREF_STORED_IN_NUBITNET
#ifndef NUNETREF_H_
#include "nucleus/NUNetRef.h"
#endif
#endif

//! NUVirtualNet class
/*!
 * A base class for a net that does not exist in the design but must
 * appear in the carbon_model and must act like a net
 */
class NUVirtualNet : public NUNet
{
public:
  //! Constructor
  /*!
    \param name Name of the net
    \param flags Attribute flags
    \param scope Scope in which net is declared
    \param loc Source location of declaration of the net
   */
  NUVirtualNet(StringAtom *name,
               NetFlags flags,
               NUScope *scope,
               const SourceLocator& loc);

  //! Return true so that these dataflow tokens always remain observable.
  bool isProtectedObservable(const IODBNucleus*) const { return true; }

  //! Destructor
  virtual ~NUVirtualNet();

  //! Return class name
  const char* typeStr() const;

  //! Bit size does not really matter, just treat like a 1 bit net.
  virtual UInt32 getBitSize() const;

  //! Bit size does not really matter, just treat like a 1 bit net.
  virtual UInt32 getNetRefWidth() const;

  //! Illegal to call; cannot create hierrefs to temp virtual nets.
  NUNet* createHierRef(const AtomArray& name, NUScope *scope, NetFlags flags, const SourceLocator &loc);

  //! Does this net contain the specified range?
  virtual bool containsRange(const ConstantRange& range) const;

  //! Does this net contain the specified bit?
  virtual bool containsBit(SInt32 index) const;

#if NUBITNETREF_STORED_IN_NUBITNET
  //! Return a pointer to the one legal netref for this bit net
  virtual const NUNetRef* getNetRef() const;
#endif

  //! Is this a virtual net?
  virtual bool isVirtualNet() const;

private:
  //! Hide copy and assign constructors.
  NUVirtualNet(const NUVirtualNet&);
  NUVirtualNet& operator=(const NUVirtualNet&);

#if NUBITNETREF_STORED_IN_NUBITNET
  //! Every bit-net has exactly one legal netref, so we might as well store
  //! it here and not in the factory
  NUNetRef mNetRef;
#endif
} APIOMITTEDCLASS; //class NUVirtualNet : public NUNet


#endif // NUVIRTUALNET_H_
