// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _NUCMODELCALL_H_
#define _NUCMODELCALL_H_

#include "nucleus/NUBlockStmt.h"

//! NUCModelCall class
/*! Abstract class that models a call to a c-model as a statement.
 */
class NUCModelCall : public NUBlockStmt
{
public:
  //! constructor
  NUCModelCall(NUCModel* cmodel,
               NUCModelFn* cModelFn,
               const SourceLocator& loc,
               NUNetRefFactory* netRefFactory,
               bool usesCFNet);

  //! destructor
  virtual ~NUCModelCall();

  //! Return the type of this class
  NUType getType() const { return eNUCModelCall; }

  //! debug help: return the type of what I'm looking at
  const char* typeStr() const { return "NUCModelCall"; }

  //! Get the cmodel function for this call
  const NUCModelFn* getFunction() const { return mCModelFn; }

  //! Get the c-model for this call
  NUCModelInterface* getCModelInterface() const;

  //! Get the c-model instance for this call
  NUCModel* getCModel() const { return mCModel; }

  //! Add the arg connection to the end of the connections
  void addArgConnection(NUCModelArgConnection* conn);

  //! Loop over the arg connections
  NUCModelArgConnectionLoop loopArgConnections();

  //! Loop over the arg connections
  NUCModelArgConnectionCLoop loopArgConnections() const;

  //! Count the number of UInt32 words to represent the c-model inputs
  UInt32 inputsWordCount() const;

  //! Count the number of UInt32 words to represent the c-model outputs
  UInt32 outputsWordCount() const;

  //! If this has a name, return it, else return NULL.
  StringAtom* getName() const;

  //! Get the context for calling this function
  const char* getContext() const;

  //! Cost estimation function
  void calcCost(NUCost*, NUCostContext* = 0) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet* old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet* old_net, NUNet* new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn& translator);

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose (UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! Return a copy of this statement; recurses to copy nested statements.
  NUStmt* copy(CopyContext& copy_context) const;

  //! debug help: print routine
  void print(bool recurse = true, int indent = 0) const;

private:
  //! Hide copy and assign constructors.
  NUCModelCall(const NUCModelCall&);
  NUCModelCall& operator=(const NUCModelCall&);

protected:
  // Local data for this call
  NUCModel* mCModel;
  NUCModelFn* mCModelFn;

private:
  NUCModelArgConnectionVector* mArgs;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUCModelCall : public NUBlockStmt

#endif // _NUCMODELCALL_H_
