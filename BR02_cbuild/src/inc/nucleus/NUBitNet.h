// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/




#ifndef NUBITNET_H_
#define NUBITNET_H_

#ifndef NUNETREF_H_
#include "nucleus/NUNetRef.h"
#endif

//! NUBitNet class
/*!
 * A net which is a single bit
 */
class NUBitNet : public NUNet
{
public:
  //! Constructor to use when net is declared in a module
  /*!
    \param name Name of the net
    \param flags Attribute flags
    \param scope Scope in which net is declared
    \param loc Source location of declaration of the net
   */
  NUBitNet(StringAtom *name,
	   NetFlags flags,
	   NUScope *scope,
	   const SourceLocator& loc);

  //! Return the declared bit size
  virtual UInt32 getBitSize() const;

  //! Return the declared bit size
  virtual UInt32 getNetRefWidth() const;

  //! Return class name
  const char* typeStr() const;

  // Compose a declaration string for the net, e.g. "reg q;" or "wire [7:0] z;" or "input clk;"
  void composeDeclaration(UtString*) const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Create a hierarchical reference to this net.
  NUNet* createHierRef(const AtomArray& name, NUScope *scope, NetFlags flags,
                       const SourceLocator &loc);

  //! destructor
  ~NUBitNet();

  //! Does this net contain the specified range?
  virtual bool containsRange(const ConstantRange& range) const;

  //! Does this net contain the specified bit?
  virtual bool containsBit(SInt32 index) const;

#if NUBITNETREF_STORED_IN_NUBITNET
  //! Return a pointer to the one legal netref for this bit net
  virtual const NUNetRef* getNetRef() const;
#endif

  //! Is this a bit net?
  virtual bool isBitNet() const;

protected:
  //! Helper function for print, prints the name and size information
  void printNameSize() const;


private:
  //! Hide copy and assign constructors.
  NUBitNet(const NUBitNet&);
  NUBitNet& operator=(const NUBitNet&);

#if NUBITNETREF_STORED_IN_NUBITNET
  //! Every bit-net has exactly one legal netref, so we might as well store
  //! it here and not in the factory
  NUNetRef mNetRef APIOMITTEDFIELD;
#endif

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUBitNet : public NUNet

#endif // NUBITNET_H_
