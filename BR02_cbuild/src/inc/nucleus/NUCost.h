// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __NU_COST_H__
#define __NU_COST_H__

#include "util/CarbonTypes.h"
#include "util/UtHashMap.h"
#include "nucleus/Nucleus.h"
#include <stdio.h>              // for FILE

//! NUCost class
/*! Container for various statistics we'd like to measure about a design,
 *  and Carbon's interpetation of it.
 */
class NUCost
{
public:
  //! ctor
  NUCost();

  //! dtor
  ~NUCost();

  //! zero out the cost structure
  void clear();

  //! print this cost as part of an ascii table
  void print(FILE*, bool csv) const;

  //! print the header line for the ascii table
  static void printHeader(FILE*, bool csv);

  //! add the another cost to 'this'.  
  void addCost(const NUCost& other, bool addIOs = false, bool addSyntactic = true, UInt32 executionCount = 1);

  struct NetInfo {
    NetInfo() {mBits = 0; mVars = 0;}
    SInt32 mBits;       // a[31:0] is 32 bits, 1 variable
    SInt32 mVars;
    void addNet(SInt32 width) {mBits += width; ++mVars;}
    void format(UtString* buf, bool csv, int cols = 5, char delim = ' ') const;
    void operator+=(const NetInfo& src)
    {
      mBits += src.mBits;
      mVars += src.mVars;
    }
  };

  struct MemoryInfo {
    SInt64 mBits;
    SInt32 mVars;
    SInt32 mAddrs;
//    SInt32 mReadPorts;  not figured out at the moment
    SInt32 mWritePorts;

    void operator+=(const MemoryInfo& src)
    {
      mBits += src.mBits;
      mVars += src.mVars;
      mAddrs += src.mAddrs;
      mWritePorts += src.mWritePorts;
    }

    void format(UtString* buf, bool csv, int cols = 5, char delim = ' ') const;
  };

  //! Return the total number of assigns
  SInt32 totalAssigns() const;

  //! Return a complexity measure:  \#instrs/\#assigns
  double complexity() const;

  // Countable attributes of design (independent of Carbon's interpreation)
  SInt32  mPrimGates;     // Counts primitive instantiations of nand,bufif0, etc
  NetInfo mFlops;
  NetInfo mLatches;
  NetInfo mTristates;
  NetInfo mInputs;  
  NetInfo mOutputs;  
  NetInfo mBidirects;
  NetInfo mTemps;
  NetInfo mDoubleBuffered;
  NetInfo mClkPath;
  NetInfo mIntegers;
  NetInfo mBufAssigns;
  NetInfo mLocalNets;
  MemoryInfo mMemories;
  //! In an elaborated view of the design, how many times is this module instantiated?
  SInt32 mInstances;
  //! In an unelaborated view of the design, how many times is this module instantiated?
  //! Note that the unelab instance count is only computed for modules (not computed for tasks or functions).
  SInt32 mUnelabInstances;
  SInt32 mSubmodules;    // how many modules does this module instantiate?
  SInt32 mUDPs;
  SInt32 mSequentialBlocks;
  SInt32 mCombinationalBlocks;
  SInt32 mResetBlocks;
  SInt32 mInitialBlocks;
  SInt32 mTasks;
  SInt32 mBlocks;
  SInt32 mFors;
  SInt32 mIfs;
  SInt32 mCases;
  SInt32 mContAssigns;
  SInt32 mBlockingAssigns;
  SInt32 mNonBlockingAssigns;
  SInt32 mCModels;
  SInt32 mCModelCalls;
  SInt32 mCModelArgs;
  // \var the number of system tasks in design (only tasks that generate output like $display, or $fclose, are included)
  SInt32 mSysTasks;

  SInt32 mSysFunctions;

  // Estimates, subject to interpretation
  SInt32 mAsicGates;     // The usual DRE guestimate of 2-input nand count

  // Carbon-specific estimates for modeling cost
  SInt32 mOps;           // (a + b - c) would be 2 ops even for 32-bit a,b,c

  // mInstructionRun only counts for the worst case number of instructions
  // that would get run during an execution, which is the longest path
  // through a case/if branch, not the total number of instructions in
  // all branches.
  SInt32 mInstructionsRun;  // On 32-bit arch a[63:0]&b[63:0] takes 2 instructions

  // mInstructions includes the total number of instructions in all branches
  SInt32 mInstructions;    // On 32-bit arch a[63:0]&b[63:0] takes 2 instructions

  SInt32 mHierNets;
  SInt32 mHierTasks;

/*
  // Global costs that must be figured out post-scheduling
  SInt32 mClocks;
  SInt32 mPrimaryClocks;
  SInt32 mDerivedClocks;
  SInt32 mAsyncResets;
  SInt32 mSequentialSchedules;
  SInt32 mCombinationalSchedules;
*/
};


//! NUCostContext class
/*!
 * This class keeps a mapping from nucleus objects to NUCost* objects, and
 * manages the memory for NUCost*.  At any time after nucleus creation, you
 * can instantiate an NUCostContext and compute the cost of any object in
 * the unelaborated nucleus hierarchy.  Elaborated Cost is computed for
 * designs and modules, but the NU*Elab objects are not needed.
 */
class NUCostContext
{
  struct HashModFunc: public HashPointer<const NUBase*>
  {
    //! lessThan operator -- for sorted iterations
    bool lessThan(const NUBase* v1, const NUBase* v2) const;
    //UtString mBuf1;
    //UtString mBuf2;
  };

  typedef UtHashMap<const NUBase*, NUCost*, HashModFunc> CostMap;
public:
  //! ctor
  NUCostContext();

  //! dtor
  ~NUCostContext();

  //! compute the cost of a design, add it to the passed-in cost, if any
  void calcDesign(const NUDesign*, NUCost* = NULL);

  //! compute the cost of a module, add it to the passed-in cost
  /*! Note that calling this on a module will change that module's
   *  mInstances count.  To get the cost of a module in a read-only
   *  fashion, call getModule()
   */
  void calcModule(const NUModule*, NUCost* cost, bool addIOCounts);

  //! compute the cost of a task
  void calcTF(const NUTF*);

  //! re-compute the cost of a task
  void recalcTF(const NUTF*);

  //! Compute the cost of a list of statements.
  void calcStmtList(const NUStmtList & stmts, NUCost * cost);

  //! Calculate or lookup the cost of an expression.  Lookup on occurs on managed expressions
  /*! See NUExprFactory for info on managed expressions */
  void calcExpr(const NUExpr* expr, NUCost* cost);

  //! lookup the cost of a cycle, returning NULL if not yet known
  NUCost* getCycleCost(const NUCycle*);

  //! Establish the cost of a cycle
  void putCycleCost(const NUCycle*, const NUCost&);

  //! get the cost of a module, task.  the cost must have already
  //! been calculated.  'deep' only makes sense for modules
  const NUCost* findCost(const NUBase*, bool deep = false) const;

  //! get the cost of a module, task.  the cost must have already
  //! been calculated.   'deep' only makes sense for modules
  NUCost* findCost(const NUBase*, bool deep = false);

  //! print a table of all the costs
  bool printCostTable(const char* filename, bool deep, bool csv, bool estimateCPS) const;

  //! Recalculate the cost of a module.  This will not update its instantiations
  void recalcModule(const NUModule*);

  //! Should we be updating instance counts?  Default is true.
  bool getUpdateInstances() const { return mUpdateInstances; }

  //! Set the updating-instances behavior
  void putUpdateInstances(bool flag) { mUpdateInstances = flag; }

  //! Clear all the memorized costs in the context
  void clear();

  //! Does cost-computation for task enables include the cost of the called task?
  bool getTaskEnablesHaveTaskCost() const { return mTaskEnablesHaveTaskCost; }

  //! Enable/disable cost-computation for task enables including the cost of the called task?
  void putTaskEnablesHaveTaskCost(bool enabled) { mTaskEnablesHaveTaskCost = enabled; }
private:
  NUCost* allocCost(const NUBase* obj, CostMap*);
  NUCost* allocDeepCost(const NUModule* obj);

  //! Count elaborated instances; this recurses through the whole design.
  void countInstances(const NUModule* mod);
  void countInstances(const NUNamedDeclarationScope* declScope);

  //! Count unelaborated instances; this just tallys immediate
  //! instantiations of the given module/declaration scope.
  void countUnelabSubinstances(const NUModule* mod);
  void countUnelabSubinstances(const NUNamedDeclarationScope* declScope);

  //! Increment the unelaborated instance count for this module.
  void incrementUnelabInstances(const NUModule* mod);

  CostMap mSelfCostMap;
  CostMap mDeepCostMap;
  NUCost mDesignCost;
  bool mUpdateInstances;

  //! Does cost-computation for task enables include the cost of the called task?
  bool mTaskEnablesHaveTaskCost;
};

#endif
