// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _NUALWAYSWALKER_H_
#define _NUALWAYSWALKER_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUDesignWalker.h"

//! Callback to determine which of a group of always blocks are still in the Nucleus tree.
class NUAlwaysBlockDiscoveryCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  //! Determine which always blocks still exist in the design.
  /*!
   * \param potential_blocks Set of pointers to always blocks which
   *        may or may not still exist in the design. (input)
   *
   * \param found_blocks Set of pointers to always blocks from
   *        'potential_blocks' which were found to still exist in the
   *        design.
   */
  NUAlwaysBlockDiscoveryCallback(const NUAlwaysBlockSet & potential_blocks,
                   NUAlwaysBlockSet & found_blocks) :
    mPotentialBlocks(potential_blocks),
    mFoundBlocks(found_blocks)
  {}
  
  Status operator()(Phase /* phase */, NUBase * /* node */) { return eNormal; }
  Status operator()(Phase phase, NUAlwaysBlock * block)
  {
    if (phase == ePre) {
      if (mPotentialBlocks.find(block) != mPotentialBlocks.end()) {
        mFoundBlocks.insert(block);
      }
    }
    return eSkip; // do not continue to walk.
  }

private:
  const NUAlwaysBlockSet & mPotentialBlocks;
  NUAlwaysBlockSet & mFoundBlocks;
};


#endif
