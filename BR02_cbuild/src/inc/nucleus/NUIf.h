// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUIF_H_
#define NUIF_H_

/*!
  \file
  Declaration of the Nucleus if class.
*/

#include "nucleus/NUBlockStmt.h"

//! NUIf class
/*!
 * An if statement
 */
class NUIf : public NUBlockStmt
{
public:
  //! constructor
  /*!
    \param cond Conditional expression.
    \param then_stmts Statements for then branch.
    \param else_stmts Statements for else branch.
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param usesCFNet true when this needs $cfnet in it's use list
    \param loc Source location.
  */
  NUIf(NUExpr *cond,
       const NUStmtList& then_stmts,
       const NUStmtList& else_stmts,
       NUNetRefFactory *netref_factory,
       bool usesCFNet,
       const SourceLocator& loc);

  //! constructor
  /*!
    Constructor without then/else statement specification.

    \param cond Conditional expression.
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param usesCFNet true when this needs $cfnet in it's use list
    \param loc Source location.
  */
  NUIf(NUExpr *cond,
       NUNetRefFactory *netref_factory,
       bool usesCFNet,
       const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUIf; }

  //! Return the conditional expression.
  NUExpr *getCond() const { return mCond; }

  //! loop through all the statements in the 'then' clause
  NUStmtLoop loopThen() {return NUStmtLoop(mThenStmts);}

  //! const loop through all the statements in the 'then' clause
  NUStmtCLoop loopThen() const {return NUStmtCLoop(mThenStmts);}

  //! loop through all the statements in the 'else' clause
  NUStmtLoop loopElse() {return NUStmtLoop(mElseStmts);}

  //! loop through all the statements in the 'else' clause
  NUStmtCLoop loopElse() const {return NUStmtCLoop(mElseStmts);}

  //! Return the statements for the then branch.
  /*!
   * The list must be freed by the caller.
   */
  NUStmtList *getThen() const;

  //! Return the statements for the else branch.
  /*!
   * The list must be freed by the caller.
   */
  NUStmtList *getElse() const;

  //! Return the pointer to the Then stmt List
  NUStmtList *getThenList();
  //! Return the pointer to the Else stmt List
  NUStmtList *getElseList();


  //! Check if the then clause is empty
  bool emptyThen() const;

  //! Check if the else clause is empty
  bool emptyElse() const;

  //! Add the given statement to the end of the block
  void addThenStmt(NUStmt *stmt);

  //! Add the given statement to the end of the block
  void addElseStmt(NUStmt *stmt);

  //! Add the given statements to the end of the block
  void addThenStmts(NUStmtList *stmts);

  //! Add the given statements to the end of the block
  void addElseStmts(NUStmtList *stmts);

  //! Add the given statement to the start of the block
  void addThenStmtStart(NUStmt *stmt);

  //! Add the given statement to the start of the block
  void addElseStmtStart(NUStmt *stmt);

  //! Replace the condition with the given new condition
  /*!
   * The caller is responsible to make sure that old cond is not leaked.
   */
  void replaceCond(NUExpr* cond);

  //! Replace the current statement list for the 'then' branch with the given statement list.
  /*!
   * The given list is copied.
   * This is used to reorder non-blocking temporaries.
   * The caller is responsible to make sure that statements are not leaked.
   */
  void replaceThen(const NUStmtList& new_stmts);

  //! Replace the current statement list for the 'else' branch with the given statement list.
  /*!
   * The given list is copied.
   * This is used to reorder non-blocking temporaries.
   * The caller is responsible to make sure that statements are not leaked.
   */
  void replaceElse(const NUStmtList& new_stmts);

  //! Remove the given statement from the then clause
  void removeThenStmt(NUStmt *stmt, NUStmt *replacement=0);

  //! Remove the given statement from the else clause
  void removeElseStmt(NUStmt *stmt, NUStmt *replacement=0);

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Equivalence
  virtual bool operator==(const NUStmt&) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! Return class name
  const char* typeStr() const;

  //! Return a copy of this statement; recurses to copy nested statements.
  NUStmt* copy(CopyContext &copy_context) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUIf();

private:
  //! Hide copy and assign constructors.
  NUIf(const NUIf&);
  NUIf& operator=(const NUIf&);

  //! Conditional expression.
  NUExpr *mCond;

  //! Statements for the then branch.
  NUStmtList mThenStmts;

  //! Statements for the else branch.
  NUStmtList mElseStmts;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS ; //class NUIf : public NUBlockStmt


#endif
