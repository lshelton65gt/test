// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NUMODULE_H_
#define NUMODULE_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUScope.h"

#include "util/UtHashSet.h"
#include "util/LoopPair.h"      // to loop over all internals & ports at once
#include "util/LoopMulti.h"

// please do not use "using namespace std"

class AtomicCache;
class STBranchNode;
class ConstantRange;
class IODBNucleus;
class STSymbolTable;
class STAliasedLeafNode;
class CopyContext;

/*!
  \file
  Declaration of the Nucleus module and elaborated module classes.
*/

//! NUModule class
/*!
 * Module, Macromodule, or UDP
 *
 */
class NUModule : public NUUseDefNode, public NUScope
{
#if !pfGCC_2
  using NUUseDefNode::operator<;
#endif

public:
  //! constructor for top-level modules
  /*!
    \param design The design
    \param isTopLevel true if this is a top level module
    \param name The name of the module.
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param string_cache String cache
    \param loc The source location of the module.
    \param iodb The IODB
    \param timeUnit The `timescale time unit
    \param timePrecision The `timescale time precision
    \param timescaleSpecified Was the timescale user-specified?
   */
  NUModule(NUDesign *design,
           bool isTopLevel,
           StringAtom* name,
           STSymbolTable * aliasDB,
           NUNetRefFactory *netref_factory,
           AtomicCache *string_cache,
           const SourceLocator& loc,
           IODBNucleus *iodb,
           SInt8 timeUnit,
           SInt8 timePrecision,
           bool timescaleSpecified,
           HierFlags language);

  //! Return the type of this class
  virtual NUType getType() const { return eNUModule; }

  //! Return the unique name of the module.
  StringAtom* getName() const;

  //! Return the original name of the module.
  StringAtom* getOriginalName() const;

  //! Set the original name of the module.
  void putOriginalName(StringAtom*);

  //! From NUScope; return this as the base object.
  NUBase* getBase() { return this; }
  const NUBase* getBase() const { return this; }

  //! Return the source location of the module.
  const SourceLocator& getLoc() const;

  //! Realise NUScope::getSourceLocation virtual method
  virtual const SourceLocator getSourceLocation () const { return getLoc (); }

  //! Iterate over every module instance, including the ones in declaration scopes.
  NUModuleInstanceMultiLoop loopInstances();

  //! Iterate over every module instance, including the ones in declaration scopes.
  NUModuleInstanceMultiCLoop loopInstances() const;

  //! Does this module or one of it's declaration scopes have submodules?
  bool hasInstances() const;

  //! Iterate over instantiations within module, excluding the ones in declaration scopes.
  NUModuleInstanceMultiLoop loopModuleInstances();

  //! Iterate over instantiations within module, excluding the ones in declaration scopes.
  NUModuleInstanceMultiCLoop loopModuleInstances() const;

  //! Loop over the top-level sub-blocks.
  /*!
   * Note that these blocks can also be reached by traversing the
   * tasks, always blocks and initial blocks in this module.
   */
  NUBlockCLoop loopBlocks() const;

  //! Loop over the top-level sub-blocks.
  /*!
   * Note that these blocks can also be reached by traversing the
   * tasks, always blocks and initial blocks in this module.
   */
  NUBlockLoop loopBlocks();

  //! Loop over the top-level declaration scopes.
  NUNamedDeclarationScopeCLoop loopDeclarationScopes() const;

  //! Loop over the top-level declaration scopes.
  NUNamedDeclarationScopeLoop loopDeclarationScopes();

  typedef Loop<NUContAssignList> ContAssignLoop;
  typedef CLoop<NUContAssignList> ContAssignCLoop;

  //! Loop over all continuous assigns
  ContAssignLoop loopContAssigns() {return ContAssignLoop(mContAssignList);}
  ContAssignCLoop loopContAssigns() const { return ContAssignCLoop(mContAssignList); }

  typedef Loop<NUEnabledDriverList> ContEnabledDriverLoop;
  typedef CLoop<NUEnabledDriverList> ContEnabledDriverCLoop;

  //! Loop over all enabled drivers
  ContEnabledDriverLoop loopContEnabledDrivers()
  {
    return ContEnabledDriverLoop(mContEnabledDriverList);
  }

  //! Loop over all enabled drivers
  ContEnabledDriverCLoop loopContEnabledDrivers() const
  {
    return ContEnabledDriverCLoop(mContEnabledDriverList);
  }

  typedef Loop<NUTriRegInitList> TriRegInitLoop;
  typedef CLoop<NUTriRegInitList> TriRegInitCLoop;

  //! replace all the trireginits
  void replaceTriRegInits(const NUTriRegInitList& tl) {mTriRegInitList = tl;}

  //! Loop over all trireg inits
  TriRegInitLoop loopTriRegInit() {return TriRegInitLoop(mTriRegInitList);}
  TriRegInitCLoop loopTriRegInit() const { return TriRegInitCLoop(mTriRegInitList); }

  //! Add the continuous assignments to the given list.
  void getContAssigns(NUContAssignList* assign_list) const;

  //! Add the enabled drivers to the given list.
  void getContEnabledDrivers(NUEnabledDriverList* driver_list) const;

  //! Add the tri-reg init nodes to the given list
  void getTriRegInit(NUTriRegInitList* init_list) const;

  //! Iterate over the module's tasks in an unpredictable order
  NUTaskLoop loopTasks();

  //! Iterate over the module's tasks in an unpredictable order
  NUTaskCLoop loopTasks() const;

  //! Iterate over the module's tasks in a (1) levelized (2) lexical order
  /*!
   *! This iteration mechanism levelizes all the tasks in a module,
   *! and iterates over them from bottom to top.  In any given level,
   *! the tasks are given in alphabetic order
   */
  NUTaskLoop loopTasksLevelized() const;

  //! Iterate over the module's cModels instances
  NUCModelCLoop loopCModels() const
  {
    return NUCModelCLoop(mCModelVector);
  }

  //! Iterate over the module's cModels instances
  NUCModelLoop loopCModels()
  {
    return NUCModelLoop(mCModelVector);
  }

  //! replace all the c-models
  void replaceCModels(const NUCModelVector& cv) {mCModelVector = cv;}

  typedef CLoop<NUAlwaysBlockList> AlwaysBlockCLoop;
  typedef Loop<NUAlwaysBlockList> AlwaysBlockLoop;

  //! Loop over the always blocks
  AlwaysBlockLoop loopAlwaysBlocks() {
    return AlwaysBlockLoop(mAlwaysBlockList);
  }

  //! Loop over the always blocks
  AlwaysBlockCLoop loopAlwaysBlocks() const {
    return AlwaysBlockCLoop(mAlwaysBlockList);
  }

  typedef CLoop<NUInitialBlockList> InitialBlockCLoop;
  typedef Loop<NUInitialBlockList> InitialBlockLoop;

   //! Loop over the initial blocks
  InitialBlockCLoop loopInitialBlocks() const {
    return InitialBlockCLoop(mInitialBlockList);
  }

  InitialBlockLoop loopInitialBlocks() {
    return InitialBlockLoop(mInitialBlockList);
  }

  //! Add the always blocks to the given list.
  void getAlwaysBlocks(NUAlwaysBlockList* always_list) const;

  //! Add the initial blocks to the given list.
  void getInitialBlocks(NUInitialBlockList* initial_list) const;

  //! Add structured procs
  void getStructuredProcs(NUStructuredProcList * struct_list) const;

  //! Add the sub-blocks to the given list.
  void getBlocks(NUBlockList* block_list) const;

  //! Add the sub-declaration scopes to the given list.
  void getDeclarationScopes(NUNamedDeclarationScopeList * scope_list) const;

  //! Add all nets local to the top-scope of this module to the given list, in no particular order.
  /*!
   * Only nets declared at the module scope will be added; nets inside
   * sub-blocks and sub-declaration scopes will not be added.
   */
  void getAllTopNets(NUNetList *net_list) const;

  //! Add all nets local to the top-scope of this module to the given list, inserting all ports before other locals.
  /*!
   * Only nets declared at the module scope will be added; nets inside
   * sub-blocks and sub-declaration scopes will not be added.
   */
  void getAllTopNetsPortsFirst(NUNetList *net_list) const;

  //! Add all nets local to sub-blocks and sub-declaration scopes within this module to the given list, in no particular order
  /*!
   * Only nets declared local to sub-blocks and sub-declaration scopes will be added.
   */
  void getAllNestedNets(NUNetList *net_list) const;

  //! Add all hierrefs within this module to the given list, in no particular order
  void getAllHierRefNets(NUNetList *net_list) const;

  //! getAllTopNets + getAllNestedNets + getAllHierRefNets + VirtualNets
  /*!
   * This is intended to explicitly NOT return task
   * internals/outputs because we do not want to elaborate the flow
   * graph within tasks/
   */
  void getAllNonTFNets(NUNetList *net_list) const;

  //! Equivalent to:  getAllNonTFNets + TF nets.
  void getAllNets(NUNetList *net_list) const;

  //! Populate the given start_list with nets to use as walking starting points.
  /*!
   * These nets added to start_list define the terminal points of the
   * graph for the current module (on the output side only), so this
   * list is suitable for using as starting points in a fanin walk.
   * The following kinds of points are included: module outputs,
   * module bids, protectedobservable points
   */
  void getFaninStartNets(NUNetList *start_list, IODBNucleus *iodb);

  //! Populates a vector with the hier refs to tasks in this module
  /*! This function does a module walk to find all task enable
   *  statements that are hierarchical references. It fills the
   *  provided vector with this.
   *
   *  \param task_enables - a vector provided by the caller to be
   *  populated.
   */
  void getTaskEnableHierRefs(NUTaskEnableVector* task_enables) const;

  //! Add all non-port-nets local to this module to the given list, in no particular order.
  NUNetCLoop loopLocals() const {return NUNetCLoop(mLocalNetList);}

  //! Add all non-port-nets local to this module to the given list, in no particular order.
  NUNetLoop loopLocals() {return NUNetLoop(mLocalNetList);}

  //! Iterate over the hierarchical referenced nets, in no particular order
  NUNetCLoop loopNetHierRefs() const {return NUNetCLoop(mNetHierRefList);}
  NUNetLoop loopNetHierRefs() {return NUNetLoop(mNetHierRefList);}

  //! Does this module define tasks with hierarchical references?
  bool hasHierRefTasks() const;
  
  //! Add all nets which are ports to this module to the given list, in module port list order.
  void getPorts(NUNetList *net_list) const;

  /*! \brief Return the 0-based index corresponding to which position
   * this net appears in the port list, -1 if it doesn't exist in the
   * list.
   */
  int getPortIndex(NUNet *net) const;

  //! Return the net corresponding to the given 0-based port index.
  NUNet *getPortByIndex(int idx) const;

  //! Return the number of ports in the port list.
  int getNumPorts() const;

  //! Return the ExtNet for this module
  NUNet *getExtNet() const;

  //! Return the OutputSysTaskNet for this module
  NUSysTaskNet* getOutputSysTaskNet() const { return mOutputSysTaskNet; }

  /*! \brief  Return the OutputSysTaskNet for this module as an NUNet, 
   *
   * (used when we need a function pointer to pass into another
   * function)
   */
  NUNet* getOutputSysTaskNetAsNUNet() const;

  //! Return the ControlFlowNet for this module
  NUControlFlowNet* getControlFlowNet() const { return mControlFlowNet; }

  /*! \brief  Return the ControlFlowNet for this module as an NUNet, 
   *
   * (used when we need a function pointer to pass into another
   * function)
   */
  NUNet* getControlFlowNetAsNUNet() const;

public:

  //! Add all nets which are input ports to this module to the given list, in module port list order.
  NUNetFilterCLoop loopInputPorts() const { return loopPortMask(eInputNet); }
  NUNetFilterLoop loopInputPorts() { return loopPortMask(eInputNet); }
  NUNetFilterCLoop loopOutputPorts() const { return loopPortMask(eOutputNet); }
  NUNetFilterLoop loopOutputPorts() { return loopPortMask(eOutputNet); }
  NUNetFilterCLoop loopBidPorts() const { return loopPortMask(eBidNet); }
  NUNetFilterLoop loopBidPorts() { return loopPortMask(eBidNet); }
  NUNetFilterCLoop loopPortMask(NetFlags flags) const {
    return NUNetFilterCLoop(NUNetVectorCLoop(mPorts), NUNetFilter(flags));
  }
  NUNetFilterLoop loopPortMask(NetFlags flags) {
    return NUNetFilterLoop(NUNetVectorLoop(mPorts), NUNetFilter(flags));
  }

  //! Loop over all ports
  NUNetVectorLoop loopPorts() {return NUNetVectorLoop(mPorts);}
  NUNetVectorCLoop loopPorts() const {return NUNetVectorCLoop(mPorts);}

  //! Abstraction for an iterator over the port and local nets
  /*! Note that there is probably no use for this type on its own. It
   *  is just an intermediate type to define NetLoop in two stages.
   */
  typedef LoopPair<NUNetVectorLoop, NUNetLoop> LclNetLoop;

  //! Abstraction for an iterator over the port and local nets
  /*! Note that there is probably no use for this type on its own. It
   *  is just an intermediate type to define NetLoop in two stages.
   */
  typedef LoopPair<NUNetVectorCLoop, NUNetCLoop> LclNetCLoop;

  //! Abstraction for an iterator over the port, local nets, and hier refs
  typedef LoopPair<LclNetLoop, NUNetLoop> NetLoop;

  //! Abstraction for an iterator over the port, local nets, and hier refs
  typedef LoopPair<LclNetCLoop, NUNetCLoop> NetCLoop;

  //! Loop over all nets, internal and external
  NetLoop loopNets();

  //! const-loop over all nets, internal and external
  NetCLoop loopNets() const;

  //! Add the modules which this module instantiates and the declaration scopes in
  //! in this module instantiates to the given list.
  void getInstantiatedModules(NUModuleList *module_list) const;

  //! Add the nets this module uses to the given set.
  /*!
   * This is from the point of view of looking-in, from outside the module.
   * From that vantage point, inputs and bids are used within the module.
   */
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this module uses to def net to the given set.
  /*!
   * This is from the point of view of looking-in, from outside the module.
   * From that vantage point, outputs and bids have defs relative to inputs
   * and bids.
   */
  void getUses(NUNet *net, NUNetSet *uses) const;
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the set of nets this module defs to the given set.
  /*!
   * This is from the point of view of looking-in, from outside the module.
   * From that vantage point, outputs and bids are def'd within the module.
   */
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the given net to the set of nets being defined by this node.
  void addDef(const NUNetRefHdl &net_ref);

  //! Add the use_net as being used by the given def_net.
  void addUse(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);

  //! Add the use_set as being used by the given def_net.
  void addUses(const NUNetRefHdl &def_net_hdl, NUNetRefSet *use_set);

  //! Invalid to call
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Update all temp memory masters based on a translation functor.
  void updateTempMemoryMasters(NuToNuFn & translator);

  //! Clear the use/def information stored in the object, if any.
  void clearUseDef();

  //! Fixup the use/def information; remove all nets in the given list from the sets.
  void fixupUseDef(NUNetList *remove_list);

  //! Add the port to this module's port list.
  /*!
   * The order in which these are added matters; it should be in port list order.
   * \param net The net to add
   */
  void addPort(NUNet* net) APIDISTILLMACRO4(APIEMITBIND(mPorts),
                                            APIEMITCODE(mPorts.push_back(net)),
                                            APIEMITCODE(addNetName(net)),
                                            APIEMITCODE(mIODB->addTypeIntrinsic(net)));

  //! Turn a port into a local net
  /*!
   * Constant-propagation can discover that an input port is tied to
   * the same constant for every instance of a module. In that case,
   * we tie the net to a constant locally, and demote it from "port"
   * status
   *
   * NOTE: This net must no longer be in the port list (via
   * ::replacePorts or a similar call) prior to calling
   * ::localizePort. The net-name must no longer be present in the
   * mNameMap (this maintenance is performed by ::replacePorts).
   */
  void localizePort(NUNet* portNet);

  //! Replace the port list with a new list (after optimizations)
  void replacePorts(const NUNetVector& ports);

  //! Replace one port net with a a set of new ports.
  void replacePort(NUNet* net, NUNetVector& ports);

  //! Add the net to this module's locally-defined nets.
  //! \param net The net to add
  void addLocal(NUNet* net)  APIDISTILLMACRO4(APIEMITBIND(mLocalNetList),
                                              APIEMITCODE(mLocalNetList.push_back(net)),
                                              APIEMITCODE(addNetName(net)),
                                              APIEMITCODE(mIODB->addTypeIntrinsic(net)));

  

  //! Add the net to this module's hierarchical references.
  void addNetHierRef(NUNet* net) APIDISTILLMACRO3(APIEMITBIND(mNetHierRefList),
                                                  APIEMITCODE(mNetHierRefList.push_back(net)),
                                                  APIEMITCODE(addNetName(net)));



  //! Remove the net from this module's set of locally-defined nets.
  void removeLocal(NUNet* net, bool remove_references=true);

  //! remove a set of nets (more efficient than calling removeLocal N times)
  void removeLocals(const NUNetSet& nets, bool remove_references);

  //! Remove the net from this module's set of hierarchical reference nets.
  void removeNetHierRef(NUNet* net, bool remove_references=true);

  //! Add the module instance to this module's instantiations.
  void addModuleInstance(NUModuleInstance* instance)  APIDISTILLMACRO2(APIEMITBIND(mModuleInstanceList),
                                                                       APIEMITCODE(mModuleInstanceList.push_back(instance)));

  //! Add the assign to this module's continuous assigns.
  void addContAssign(NUContAssign* assign)  APIDISTILLMACRO2(APIEMITBIND(mContAssignList),
                                                             APIEMITCODE(mContAssignList.push_back(assign)));

  //! Add the init to this module's tri-reg inits.
  void addTriRegInit(NUTriRegInit* init);

  //! Add the always block to this module's always blocks.
  void addAlwaysBlock(NUAlwaysBlock* block) APIDISTILLMACRO2(APIEMITBIND(mAlwaysBlockList),
                                                             APIEMITCODE(mAlwaysBlockList.push_back(block)));

  //! Add the initial block to this module's initial blocks.
  void addInitialBlock(NUInitialBlock* block) APIDISTILLMACRO2(APIEMITBIND(mInitialBlockList),
                                                               APIEMITCODE(mInitialBlockList.push_back(block)));

  //! Add the given enabled driver list to this module's enabled driver list.
  void addContEnabledDrivers(NUEnabledDriverList* driver_list);

  //! Add the given block as a child
  void addBlock(NUBlock * block) APIDISTILLMACRO2(APIEMITBIND(mBlockList),
                                                  APIEMITCODE(mBlockList.push_back(block)));

  //! Add the given declaration scope as a child
  void addDeclarationScope(NUNamedDeclarationScope * block) APIDISTILLMACRO2(APIEMITBIND(mDeclarationScopeList),
                                                                             APIEMITCODE(mDeclarationScopeList.push_back(block)));

  //! Add the given task to this module's tasks.
  void addTask(NUTask *task)  APIDISTILLMACRO2(APIEMITBIND(mTaskVector),
                                               APIEMITCODE(mTaskVector.push_back(task)));

  //! Add the given c-model to this module's c-models
  void addCModel(NUCModel* cmodel)  APIDISTILLMACRO2(APIEMITBIND(mCModelVector),
                                                     APIEMITCODE(mCModelVector.push_back(cmodel)));

  //! Remove this instance from this module's instantiations.
  void removeModuleInstance(NUModuleInstance* instance, NUModuleInstance *replacement=0);

  //! replace all the module instances that are at module level, not the one's
  //! inside it's child declaration scopes.
  void replaceModuleInstances(const NUModuleInstanceList& new_mods) {
    mModuleInstanceList = new_mods;
  }

  //! replace all the module and it's child declaration scope instances.
  void replaceModuleAndDeclScopeInstances(const NUModuleInstanceList& new_mods);

  //! Get the list of declaration scopes that are empty i.e scopes that have
  //! no module instances, no local nets, and all nested declaration scopes are empty.
  void getEmptyDeclScopes(NUNamedDeclarationScopeList& emptyDeclScopes);

  //! Remove the assign from this module's continuous assigns.
  void removeContAssign(NUContAssign* assign, NUContAssign *replacement=0);

  //! Remove the init from this module's tri-reg inits.
  void removeTriRegInit(NUTriRegInit* init, NUTriRegInit *replacement=0);

  //! Remove the always block from this module's always blocks.
  void removeAlwaysBlock(NUAlwaysBlock* block, NUAlwaysBlock *replacement=0);

  //! Remove the initial block from this module's initial blocks.
  void removeInitialBlock(NUInitialBlock* block, NUInitialBlock *replacement=0);

  //! Remove the given enabled driver list from this module's enabled driver list.
  void removeContEnabledDriver(NUEnabledDriver* driver, NUEnabledDriver *replacement=0);

  //! Remove the given sub-block
  void removeBlock(NUBlock * block);

  //! Remove the given sub-declaration scope.
  void removeDeclarationScope(NUNamedDeclarationScope * scope);

  //! Remove the given task from this module's tasks.
  void removeTask(NUTask *task, NUTask *replacement=0);

  //! replace all the tasks
  void replaceTasks(const NUTaskVector& tv) {mTaskVector = tv;}

  //! Remove the given c-model from this module's c-models
  void removeCModel(NUCModel* cmodel, NUCModel* replacement=0);

  //! Remove all the blocks -- usually because this module has been flattened away
  void removeBlocks();

  //! Replace the list of initial blocks with the given list.
  /*!
   * The given list is copied.
   * The caller is responsible to make sure that intial blocks are not leaked.
   */
  void replaceInitialBlocks(const NUInitialBlockList& block_list);

  //! Replace the list of always blocks with the given list.
  /*!
   * The given list is copied.
   * This is used in asynchronous reset rewriting.
   * The caller is responsible to make sure that always blocks are not leaked.
   */
  void replaceAlwaysBlocks(const NUAlwaysBlockList& block_list);

  //! Replace the list of blocks with the given list.
  /*!
   * The given list is copied.
   * The caller is responsible to make sure that blocks are not leaked.
   */
  void replaceBlocks(const NUBlockList& block_list);

  //! Replace the list of declaration scopes with the given list.
  /*!
   * The given list is copied.
   * The caller is responsible to make sure that scopes are not leaked.
   */
  void replaceDeclarationScopes(const NUNamedDeclarationScopeList& scope_list);

  //! Replace the list of continuous assigns with the given list.
  /*!
   * The given list is copied.
   * The caller is responsible to make sure that continuous assigns are not leaked.
   */
  void replaceContAssigns(const NUContAssignList& assign_list);

  //! Replace the list of continuous enabled drivers with the given list.
  /*!
   * The given list is copied.
   *
   * This is used in block merging
   *
   * The caller is responsible to make sure that continuous enabled
   * drivers are not leaked.
   */
  void replaceContEnabledDrivers(const NUEnabledDriverList& driver_list);

  //! Generate C++ code
  virtual CGContext_t emitCode (CGContext_t) const;

  //! generate verilog code for module
  virtual void compose(UtString* buf, const STBranchNode* scope, int indent, bool recurse) const;

  //! For objects declared locally, return an index for them to be lookup-up by, which
  //! will be consistent across different instantiations of this scope.
  SInt32 reserveSymtabIndex() APIDISTILLMACRO1(APIEMITCODE(return mCurSymtabIndex++));


  //! Return the NUModuleElab object for this module at the top-level.
  //! TBD: really should support multiple of these?
  NUModuleElab *lookupElab(STSymbolTable *symtab) const;

  //! Return the NUModule object for the given hierarchy branch node, or 0 if there isn't one.
  /*!
    If lookupParent is set, look through hierarchy for a parent that is module.
  */
  static NUModule* lookup(const STBranchNode *node, bool lookupParent = false);
  static NUModule* lookup(STBranchNode *node, bool lookupParent = false);

  //! Return the STBranchNode for the module parent of the supplied node
  static const STBranchNode *getParentModuleBranch( const STBranchNode *branch );

  //! Return my index in the symbol table
  SInt32 getSymtabIndex() const APIDISTILLMACRO1(APIEMITCODE(return mMySymtabIndex ));




  //! Return true if this module is at the top-level
  bool atTopLevel() const { return mAtTopLevel; }

  //! Return the type of scoping unit
  NUScope::ScopeT getScopeType() const { return NUScope::eModule; }

  //! Modules do not have a parent scope
  NUScope* getParentScope() const { return 0; }

  //! create a printable form of the flags of NUModule
  void printFlagsToBuf(UtString* buf) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const;

  //! Return a UtString identifying this type.
  virtual const char* typeStr() const;

  //! Get storage allocation for this module's class
  size_t getClassDataSize() const { return mClassDataSize; }

  //! Set storage allocation for this module's class
  void setClassDataSize (UInt32 alloc) const { mClassDataSize = alloc; }

  //! Get maximal alignment required by this module's class
  size_t getClassDataAlignment() const { return mClassDataAlignment; }

  //! Set alignment required by an instance of this class
  void setClassDataAlignment (UInt32 align) const {
    mClassDataAlignment = align;
  }

  //! Get storage allocation for data + instances
  size_t getClassInstDataSize() const { return mClassInstDataSize; }

  //! Set storage allocation for data + instances
  void setClassInstDataSize (UInt32 alloc) const { mClassInstDataSize = alloc; }

  //! Return the parent module (illegal)
  const NUModule* findParentModule() const {
    NU_ASSERT(0=="modules not scoped", this); return NULL;
  }

  NUModule* findParentModule() { NU_ASSERT (0=="modules not scoped", this); return NULL; }

  //! Function to compare two modules
  static int compare(const NUModule* m1, const NUModule* m2);

  //! sorting helper function so you can make UtHashSets and UtHashMaps of modules
  bool operator<(const NUModule& other) const;

  //! add the cost of this into the NUCost*.
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! Get the symbol table used to store the assignment aliases and hierarchical structure of flattened modules
  STSymbolTable* getAliasDB() const { return mAliasDB; }

  //! Given a hierref name, try to resolve it locally.
  /*!
   * By resolving locally, we mean that the instantiated modules are
   * not traversed, only this module is searched.
   *
   * \returns the symbol table node if succesful, 0 otherwise
   */
  STSymbolTableNode* resolveHierRefLocally(const AtomArray& path,
                                           UInt32 pathIndex) const;

  //! Given a hierref name, try to resolve it locally, recursively.
  /*!
   * By resolving locally recursively, we mean that the instantiated
   * modules are also traversed, and at each recursion we do a local
   * search.
   *
   * \param hier_name The path to the hierarchical reference
   *
   * \param instPath Optional vector will be populated with the
   * instances in the path from the current module to the hier ref. If
   * the hier ref is not found, the vector will be empty.
   *
   * \returns Return the symbol table node if successful, 0 otherwise.
   */
  STSymbolTableNode* resolveHierRefRecurse(const AtomArray& path,
                                           UInt32 pathIndex,
                                           NUModuleInstanceVector* instPath=NULL) const;

  //! Given a hierarchical name of a net, return a hierarchical reference which covers it, 0 if none.
  NUNet* findNetHierRef(const AtomArray& path) const;

  //! Return the hierref for the net in this module, if it exists, 0 otherwise.
  /*!
   * The passed-in net may either be a hierarchical reference or a normal net.
   * If it is a hierarchical reference, try to find a hierarchical reference in this
   * module which resolves similarly.
   */
  NUNet* findNetHierRef(NUNet *net) const;

  //! declare that this module runs a system function from the "Time" group
  void putRunsSystemFunctionsOfTimeGroup(bool flag);

  //! declare that this module runs a system task from the "Random" group
  void putRunsSystemTasksOfRandomGroup(bool flag);

  //! declare that this module runs a system task from the "Output" group, if verilog_flag is true then this is from Verilog
  void putRunsSystemTasksOfOutputGroup(bool flag, bool verilog_flag);

  //! declare that this module runs a system task from the "Input" group
  void putRunsSystemTasksOfInputGroup(bool flag);

  //! declare that this module runs a system task from the "Control" group
  void putRunsSystemTasksOfControlGroup(bool flag);

  //! does this module run ANY system tasks/fucns directly?
  // if mRunsVerilogSystemTasksOfOutputGroup is true then
  //mRunsSystemTasksOfOutputGroup is true
  bool runsSystemTasks() const {return (mRunsSystemFunctionsOfTimeGroup or
                                        mRunsSystemTasksOfRandomGroup or
                                        mRunsSystemTasksOfOutputGroup or
                                        mRunsSystemTasksOfInputGroup or
                                        mRunsSystemTasksOfControlGroup   );}

  //! does this module run any system tasks/fucns from the "Time" group directly?
  bool runsSystemFunctionsOfTimeGroup() const {return (mRunsSystemFunctionsOfTimeGroup);}

  //! does this module run any system tasks/fucns from the "Random" group directly?
  bool runsSystemTasksOfRandomGroup() const {return (mRunsSystemTasksOfRandomGroup);}

  //! does this module run any system tasks/fucns from the "Output" group directly?
  bool runsSystemTasksOfOutputGroup() const {return (mRunsSystemTasksOfOutputGroup);}

  //! does this module run any Verilog system tasks/fucns from the "Output" group directly?
  bool runsVerilogSystemTasksOfOutputGroup() const {return (mRunsVerilogSystemTasksOfOutputGroup);}

  //! does this module run any system tasks/fucns from the "Input" group directly?
  bool runsSystemTasksOfInputGroup() const {return (mRunsSystemTasksOfInputGroup);}
  //! does this module run any system tasks/fucns from the "Control" group directly?
  bool runsSystemTasksOfControlGroup() const {return (mRunsSystemTasksOfControlGroup);}

  //! Retrieve the `timescale time unit for this module
  SInt8 getTimeUnit() const { return mTimeUnit; }
  //! Set the `timescale time unit (or timeunit) for this module
  void setTimeUnit(SInt8 val) { mTimeUnit = val; }
  //! Retrieve the `timescale time precision for this module
  SInt8 getTimePrecision() const { return mTimePrecision; }
  //! Set the `timescale time precision (or timeprecision) for this module
  void setTimePrecision(SInt8 val) { mTimePrecision = val; }

  //! Retrieve whether the design specified a timescale (or timeunit or timeprecision) for this module
  bool isTimescaleSpecified() const { return (mFlags & eTimescaleSpecified) != 0; }
  void setTimescaleSpecified() { mFlags |= eTimescaleSpecified; }

  //! get all the change detect operations in this module
  /*!
    If scheduled_only is true, only return the change detects which
    have been scheduled (polling the ::isScheduled method on various
    Nucleus types).
   */
  void getChangeDetects(NUChangeDetectVector* changeDetects, bool scheduled_only) const;

  //! destructor
  ~NUModule();

  //! Request this module to be flattened or not
  /*! Note that prior flattening flags are erased */
  void setFlattenContents();

  //! Request this module to be flattened or not
  /*! Note that prior flattening flags are erased */
  void setFlattenModule();

  //! Request this module to be flattened or not
  /*! Note that prior flattening flags are erased */
  void setFlattenDisallow();

  //! Request this module to not be flattened
  /*! Note that prior flattening flags are erased */
  void setFlattenAllow();

  //! Request this module be hidden
  void setHidden();

  //! Request that this module be optimized.
  void setTernaryGateOptimization();

  //! Request this module have no async resets
  void setNoAsyncResets();

  //! Request that outputSysTasks are to be ignored in this module
  void setDisableOutputSysTasks();
  //! Request that outputSysTasks are NOT to be ignored in this module
  void setEnableOutputSysTasks();

  //! is flattening requested?
  bool isFlattenModule() const {
    return ((mFlags & eFlattenMask) == eFlattenModule);
  }

  //! is flattening requested?
  bool isFlattenContents() const {
    return ((mFlags & eFlattenMask) == eFlattenContents);
  }

  //! is flattening disallowed for the tree?
  bool isFlattenDisallow() const {
    return ((mFlags & eFlattenMask) == eFlattenDisallow);
  }

  //! is flattening allowed for the tree?
  bool isFlattenAllow() const {
    return ((mFlags & eFlattenMask) == eFlattenAllow);
  }

  //! is sop-transformation requested?
  bool isTernaryGateOptimization() const {
    return ((mFlags & eTernaryGateOptimization) == eTernaryGateOptimization);
  }

  //! is this module supposed to be hidden?
  bool isHidden() const {
    return ((mFlags & eHidden) != 0);
  }

  //! is this module supposed to have no async resets?
  bool isNoAsyncResets() const {
    return ((mFlags & eNoAsyncResets) != 0);
  }

  //! return true if the user specified this module in a disableOutputSysTask directive.
  bool isDisableOutputSysTasks() const {
    return ((mFlags & eDisableOutputSysTasks) != 0);
  }
  //! return true if the user specified this module in a enableOutputSysTask directive.
  bool isEnableOutputSysTasks() const {
    return ((mFlags & eEnableOutputSysTasks) != 0);
  }

  //! Returns true if this module has any displays that use a %m format string
  bool hasDisplaysWithHierarchicalName() const;

  //! Determine whether this module can be used for Congruence with another
  //! by comparing the directive flags.  Any new fields added to NUModule
  //! that would affect a modules equivalence with another one should be
  //! incorporated into this routine.
  bool areFlagsCongruent(const NUModule* other) const;

  //! get the design
  NUDesign* getDesign() const {return mDesign;}

  typedef UtHashSet<STSymbolTableNode*> NodeSet;

  //! loop over all nets in this module that were declared asyncReset
  /*!
   *! This routine is not legal to call after flattening.
   */
  NodeSet::SortedLoop loopAsyncResetNets() const;

  //! Does this module have any asyncReset nets declared?
  bool hasAsyncResetNets() const {return mAsyncResetNets != NULL;}

  //! Add an async reset net
  void addAsyncResetNet(STSymbolTableNode* sym);

  //! Get the source language for this module/architecture
  HierFlags getSourceLanguage() const
  {
    return HierFlags(getHierFlags() & eLanguageMask);
  }

  //! Run sanity check for all the always-block priority chains in the module
  /*!
   *! Schedule-based block-merging a little sloppy with reset chains,
   *! and we need to relax the checking a little bit -- just make sure
   *! the signature is as expected
   */
  void sanityCheckAlwaysBlockPrioChain() const;

  //! get the netref factory
  NUNetRefFactory* getNetRefFactory() const {return mNetRefFactory; }

  // experimental code for Programmer's View:
#ifdef CARBON_PV
  //! Copy this module into a new design.  The map is for instances.
  /*!
   *! The user should copy modules bottom-up so that the module-map
   *! is updated before any module that instantiates it is copied.
   *! The newly created module in new_design is returned.
   *!
   *! NOTE:  THIS IS EXPERIMENTAL CODE AND NOT ALL THE MEMBER VARIABLES
   *! OF THE MODULE ARE TRANSFERRED YET.
   */
  NUModule* copy(NUDesign* new_design, NUModuleMap* mod_map);

  //! copy always blocks from this module to the dst
  void copyAlwaysBlocks(NUModule* dst_module, CopyContext& cc,
                        NuToNuFn* xlate);

  //! copy module instances from this module to the dst
  void copyModuleInstances(NUModule* dst_module, CopyContext& cc,
                           NUModuleMap* mod_map);
#endif


protected:
  //! Return the net flags to use when creating a locally-scoped temporary
  NetFlags getTempFlags() const { return NetFlags(eTempNet|eAllocatedNet); }

private:
  //! Hide copy and assign constructors.
  NUModule(const NUModule&);
  NUModule& operator=(const NUModule&);

  //! Common initialization code called from all constructors
  void commonInit(AtomicCache *string_cache, bool timescaleSpecified);

  //! Port nets in their port list order.
  NUNetVector mPorts APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Map of port nets to their port number.  Speeds up getPortIndex().
  UtHashMap<NUNet*,int> mPortIndices APIOMITTEDFIELD;

  //! Locally-declared nets in no particular order.
  NUNetList mLocalNetList  APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Hierarchical reference nets in no particular order.
  NUNetList mNetHierRefList APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Map of port defs to their "use"s
  NUNetRefNetRefMultiMap* mPortNetRefDefMap APIOMITTEDFIELD;

  //! List of continuous assigns.
  NUContAssignList mContAssignList APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! List of tri-reg init nodes
  NUTriRegInitList mTriRegInitList APIOMITTEDFIELD;

  //! List of instantiations.
  NUModuleInstanceList mModuleInstanceList  APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! List of always blocks.
  NUAlwaysBlockList mAlwaysBlockList  APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! List of initial blocks.
  NUInitialBlockList mInitialBlockList  APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! List of enabled drivers.
  NUEnabledDriverList mContEnabledDriverList APIOMITTEDFIELD;

  //! List of sub-blocks.
  NUBlockList mBlockList APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! List of sub-declaration scopes.
  NUNamedDeclarationScopeList mDeclarationScopeList  APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Vector of tasks
  NUTaskVector mTaskVector APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Vector of c-models in this module
  NUCModelVector mCModelVector APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Unique Module name -- this is uniquified based on paramters and uselib
  StringAtom *mName;

  //! Original module name -- this may not be unique
  StringAtom *mOriginalName;

  //! Alias symbol table.
  STSymbolTable * mAliasDB;

  //! Module source location.
  SourceLocator mLoc;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory APIOMITTEDFIELD;

  //! Currently-available symtab index
  SInt32 mCurSymtabIndex APIDISTILLMACRO2(APICONSTRUCTORFALSE,
                                          APIEMITCODE(mCurSymtabIndex = 0));

  //! Index of me in the symbol table.
  SInt32 mMySymtabIndex APIDISTILLMACRO2(APICONSTRUCTORFALSE,
                                         APIEMITCODE(mMySymtabIndex= mAtTopLevel? mDesign->reserveSymtabIndex(): -1));



  //! Dataflow token for references to nets outside this module
  NUExtNet *mExtNet APIOMITTEDFIELD;

  //! true if this module is at the top level of the design.
  bool mAtTopLevel;

  //! Names of nets in this module specified in asyncReset directives
  /*!
   *! These are kept in here, and not just the IODB, so that it's easy
   *! for congruence to tell that two modules are not equivalent because
   *! either one of them has asyncReset directives on its nets.  
   *!
   *! Note that, unlike the noAsyncResets directive, specific nets are
   *! identified.
   */
  NodeSet* mAsyncResetNets APIDISTILLMACRO1(APICONSTRUCTORFALSE);


  /*! \brief true if this module executes system tasks/functions, of "Time"
   * group and therefore needs to include header of carbonGlobal
   */
  bool mRunsSystemFunctionsOfTimeGroup APIOMITTEDFIELD;

  /*! \brief true if this module executes system tasks/functions, of "Random"
   * group and therefore needs to allocate a runtime system pointer
   */
  bool mRunsSystemTasksOfRandomGroup APIOMITTEDFIELD;

  /*! \brief true if this module executes system tasks/functions, of "Output"
   * group and therefore needs to allocate a Verilog/VHDL File system
   */
  bool mRunsSystemTasksOfOutputGroup APIOMITTEDFIELD;

  /*! \brief true if this module executes Verilog system tasks/functions, of "Output"
   * group and therefore needs to allocate a Verilog File system
   */
  bool mRunsVerilogSystemTasksOfOutputGroup APIOMITTEDFIELD;

  /*! \brief true if this module executes system tasks/functions, of "Input"
   * group and therefore needs to allocate a Verilog/VHDL File system
   */
  bool mRunsSystemTasksOfInputGroup APIOMITTEDFIELD;

  /*! \brief true if this module executes system tasks/functions, of "Control"
   * group and therefore needs to maintain callback information
   */
  bool mRunsSystemTasksOfControlGroup APIOMITTEDFIELD;

  //! Data size of this module's class object
  mutable size_t mClassDataSize APIOMITTEDFIELD;

  //! Data alignment required by this class
  mutable size_t mClassDataAlignment APIOMITTEDFIELD;

  //! Data size of this module's data + instances
  mutable size_t mClassInstDataSize APIOMITTEDFIELD;

  /*! a single net mOutputSysTaskNet is used as the target for all
   * output system tasks so that those tasks can have data dependencies
  */
  NUSysTaskNet* mOutputSysTaskNet APIOMITTEDFIELD;

  /*! \brief a single net mControlFlowNet is used as the target for all
   * output system tasks so that those tasks can have data dependencies
   */
  NUControlFlowNet* mControlFlowNet APIOMITTEDFIELD;
  /*! \brief Integer that represents the `timescale timeUnit for this module.
   * Its value is as specified in Tables 75 and 150 of the Verilog LRM,
   * ranging from 2 to -15.  The value is the power of 10 for the
   * timescale.  The default value is 0 == 1 sec.
   */
  SInt8 mTimeUnit;
  /*! \brief Integer representing the `timescale timePrecision for this module.
   * Its value is as specified in Tables 75 and 150 of the Verilog LRM,
   * ranging from 2 to -15.  The value is the power of 10 for the
   * timescale.  The default value is 0 == 1 sec.
   */
  SInt8 mTimePrecision;

  /*! \brief Flags to indicate if the module had a `timescale specified,
   *  plus flattening directives. this is a mask of FlagT, sized so that
   *  it and the two SInt8's above all fit in 32-bits.
   */
  UInt16 mFlags;

  enum FlagT {
    eFlattenMask              = 0xf, // the 4 flattening options are mutexed
    eFlattenDisallow          = 0x1,
    eFlattenModule            = 0x2,
    eFlattenContents          = 0x3,
    eFlattenAllow             = 0x4,

    eTimescaleSpecified       = 0x10,
    eHidden                   = 0x20,
    eNoAsyncResets            = 0x40,
    eDisableOutputSysTasks    = 0x80,
    eEnableOutputSysTasks     = 0x100,
    eTernaryGateOptimization  = 0x200,
  } ;

  NUDesign* mDesign;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUModule : public NUUseDefNode, public NUScope


//! NUModuleElab class
/*!
 * Model the elaboration of a module (the module existing at some elaborated
 * point in the hierarchy).
 */
class NUModuleElab : public NUScopeElab
{
public:
  //! constructor
  /*!
   \param module The module of which this is an elaboration.
   \param inst The module instantiation which created this elaboration.
               0 if at top-level or if a flattened instantiation.
   \param hier The hierarchy node in the symbol table for this elaboration.
   */
  NUModuleElab(NUModule *module, NUModuleInstance *inst, STBranchNode *hier);

  //! Return the module of which this is an elaboration
  NUModule *getModule() const;

  //! Return the module instance, will be 0 if at top-level
  NUModuleInstance *getModuleInstance() const { return mInst; }

  //! Return true if this module is at the top-level
  bool atTopLevel() const;

  //! Realise NUElabBase::getSourceLocation virtual method
  virtual const SourceLocator getSourceLocation () const;

  //! Realise NUElabBase::findUnelaborated virtual method
  virtual bool findUnelaborated (STSymbolTable *stab, STSymbolTableNode **unelab) const;

  //! Dump the module's elaborated flow to UtIO::cout().
  void printElab(STSymbolTable *symtab, bool verbose, int indent) const;

  //! Dump myself to UtIO::cout().
  void print(bool verbose, int indent) const;

  //! Return a UtString identifying this type.
  virtual const char* typeStr() const;

  //! Return the NUModuleElab object for the given hierarchy branch node, or 0 if there isn't one.
  static NUModuleElab *lookup(const STBranchNode *hier);
  static NUModuleElab *lookup(STBranchNode *hier);

  //! destructor
  ~NUModuleElab();

  static NUModuleElab* find(const STSymbolTableNode* node);


private:
  //! Hide copy and assign constructors.
  NUModuleElab(const NUModuleElab&);
  NUModuleElab& operator=(const NUModuleElab&);

  //! The module instantiation which created this elaboration.  0 if at top-level.
  NUModuleInstance *mInst;
} APIOMITTEDCLASS; //class NUModuleElab : public NUScopeElab

//! compare a module by name
struct NUModuleCmp
{
  bool operator()(const NUModule* m1, const NUModule* m2) const
  {
    return NUModule::compare(m1, m2) < 0;
  }      
};

#endif
