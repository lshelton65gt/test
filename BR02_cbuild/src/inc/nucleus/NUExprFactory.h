// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUEXPRFACTORY_H_
#define NUEXPRFACTORY_H_
#include "util/Util.h"
#include "util/UtHashSet.h"
#include "nucleus/NUExpr.h"

//! Special hash methodology for NUExprFactory which assumes
//! all sub-expressions are already in the factory 

//// WARNING WARNING WARNING
////
////  This code needs to be replaced by a correct shallow hash.
////
//// WARNING WARNING WARNING

struct NUExprShallowHash
{
public:
  //! hash operator -- relies on pointer value with optional shift
  size_t hash(const NUExpr* var) const {
    return var->shallowHash();
  }

  //! lessThan operator -- for sorted iterations -- relies on class operator<
  bool lessThan(const NUExpr* v1, const NUExpr* v2) const {
    return v1 < v2;
  }

  //! shallow less-than function -- for Microsoft hash tables
  bool lessThan1(const NUExpr* v1, const NUExpr* v2) const {
    return v1 < v2;
  }

  //! equal operator -- for hashing on pointer value
  bool equal(const NUExpr* v1, const NUExpr* v2) const {
    return (v1 == v2) || v1->shallowEq(*v2);
  }
};

//! NUExprFactory class
class NUExprFactory
{
public:
  //! constructor
  NUExprFactory();

  //! destructor
  ~NUExprFactory();

  //! insert an expression into the factory
  /*!
   *! After an insertion, it will be owned by the factory.  A
   *! read-only copy of it will be returned.  It is read-only so
   *! we can create only one copy of identical expressions, and
   *! they can be shared without fear of mutation.
   *!
   *! Factory expressions cannot be deleted individually.  They
   *! are reclaimed only by deleting the factory.
   *!
   *! If 'copy' is specified as true, then the expression will
   *! be copied before inserting into the factory, if necessary.
   *! This is handy if you are trying to put an expression into
   *! the factory that may be in one of 3 states:
   *!
   *!   - not factory-managed (copy it before inserting)
   *!   - already managed by this factory (return it without copying)
   *!   - already managed by a different factory (copy it before inserting)
   */
  const NUExpr* insert(NUExpr*, bool copy = false);

  //! Remove all the expressions from the factory
  void clear();

  //! Is the factory empty?
  bool empty() const;

  //! Create a resized version of a factory-managed expression
  /*!
   *! size defaults to the determineBitSize of expr
   */
  const NUExpr* resize(const NUExpr* expr, UInt32 size = 0);

  //! Create a new signed version of a factory-managed expression
  const NUExpr* changeSign(const NUExpr*, bool sign);

  //! Resize & change the sign of a factory-managed expression
  const NUExpr* resizeSign(const NUExpr*, UInt32 size, bool sign);

  //! Make a new version of the expression with explicit size as given
  /*!
   *! size defaults to the getBitSize() of expr
   */
  const NUExpr* makeSizeExplicit(const NUExpr*, UInt32 size = 0);

private:
  //! To detect when stale nets might be in the expression cache, we keep
  //! track of the net-delete-count and reset it whenever the expr-cache
  //! is initialized or cleared
  UInt32 mNetDeleteCount;

  typedef UtHashSet<NUExpr*, NUExprShallowHash> ExprHashSet;
  ExprHashSet* mExprSet;
};
#endif
