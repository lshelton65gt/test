// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#ifndef NUBITNETHIERREF_H_
#define NUBITNETHIERREF_H_

#ifndef NUBITNET_H_
#include "nucleus/NUBitNet.h"
#endif

//! Hierarchical reference to a bit net
class NUBitNetHierRef : public NUBitNet
{
public:
  //! constructor
  /*!
    \param name Name of the net (includes path)
    \param flags The declared flags for the net
    \param scope Scope in which hierarchical ref occurs.
    \param loc Source location
  */
  NUBitNetHierRef(const AtomArray& name,
                  NetFlags flags,
		  NUScope *scope,
		  const SourceLocator &loc);

  //! destructor
  ~NUBitNetHierRef();

  bool isHierRef() const { return true; }

  //! Illegal to call.
  void addHierRef(NUBase *net_hier_ref);

  //! Illegal to call.
  void removeHierRef(NUBase *net_hier_ref);

  //! Return true if this net is read outside the net's scope
  bool isHierRefRead() const;

  //! Return true if this net is written outside the net's scope
  bool isHierRefWritten() const;

  //! Are any of the possible resolutions protected mutable due to non-hierref reasons?
  /*!
   * This returns true for subset of when isProtectedMutable returns
   * true, and should only be used when you specifically don't
   * want hierrefs to make a net protected.
   */
  bool isProtectedMutableNonHierref(const IODBNucleus* iodb,
                                    bool checkPrimary = true) const;

  //! Return the NUNetHierRef for this hierref.
  NUNetHierRef* getHierRef() { return &mNetHierRef; }
  const NUNetHierRef* getHierRef() const { return &mNetHierRef; }

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Illegal to call; cannot create hierrefs to hierrefs.
  NUNet* createHierRef(const AtomArray& name, NUScope *scope, NetFlags flags, const SourceLocator &loc);

protected:
  //! Helper function for print, prints the name and size information
  void printNameSize() const;

private:
  //! Hide copy and assign constructors.
  NUBitNetHierRef(const NUBitNetHierRef&);
  NUBitNetHierRef& operator=(const NUBitNetHierRef&);

  //! Hierref resolution object.
  NUNetHierRef mNetHierRef;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUBitNetHierRef : public NUBitNet


#endif // NUBITNETHIERREF_H_
