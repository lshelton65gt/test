// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUASSIGN_H_
#define NUASSIGN_H_

#include "nucleus/NUStmt.h"

class IODBNucleus;

/*!
  \file
  Declaration of the Nucleus always block class.
*/

//! NUAssign class
/*!
 *  Pure virtual class, models an assignment.
 */
class NUAssign : public NUStmt
{
 public:
  //! constructor
  /*!
    \param lvalue The LHS of the assign.
    \param rvalue The RHS of the assign.
    \param usesCFNet If true then this has $cfnet in it's use list
    \param loc The source location of the assign.
    \param do_resize Do an explicit resize?  You will want this to be true
                     for the most part, unless you have already set the size
                     of the lhs and rhs appropriately.  For example, the copy
                     method uses false.
   */
  NUAssign(NULvalue *lvalue,
	   NUExpr *rvalue,
           bool usesCFNet,
	   const SourceLocator& loc,
           bool do_resize = true);
  
  //! Return the type of this class
  virtual NUType getType() const { return eNUAssign; }

  //! Create an assignment using properties from this
  /*!
   *! Inheriting strength, cfnet, loc from this.  The 'name' and
   *! 'scope' parameters are only used if this is a continuous
   *! assign, which requires a name.
   *!
   *! This routine is written to be called from code that doesn't
   *! care whether it's working with blocking or continuous assigns,
   *! and it's easier to provide name and scope even when they are
   *! not needed
   */
  virtual NUAssign* create(NULvalue *lvalue, NUExpr *rvalue,
                           NUScope* scope, const char* name) = 0;

  //! Return the LHS of the assign.
  NULvalue* getLvalue() const { return mLvalue; }

  //! Return the RHS of the assign.
  NUExpr* getRvalue() const { return mRvalue; }
  
  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Change this node to define new_net instead of old_net
  void replaceLvalue(NULvalue* lvalue, bool deleteOld = true);

  //! Change this node to assign a different Rvalue
  /*!
   * The caller is responsible to make sure that old rvalue is not leaked.
   */
  void replaceRvalue(NUExpr *rvalue) { mRvalue = rvalue; }
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Does this node represent a simple buffer or assignment of one net to another.
  bool isSimpleBufOrAssign() const;

  //! Check if this assignment is the temp memory initialization
  /*! The following conditions need to be true for this to be the temp
   *  mem assign:
   *
   *  1. The LHS and RHS have to be whole identifiers
   *
   *  2. The LHS has to be a temp memory
   *
   *  3. The RHS has to be the master memory for the temp memory.
   *
   *  In addition, callers of this function should check if this is
   *  the first assignment in the always block. That cannot be tested
   *  in this routine. The temp memory is returned to do that checking
   *  if isTempMemInit returns true.
   */
  bool isTempMemInit(NUTempMemoryNet** tmem) const;

  //! Size the assignment
  /*!
   * Will be called by constructor.
   *
   * 1 - determine maximum size seen in LHS and RHS
   * 2 - resize to that size
   */
  void resize();

  /*! \brief if LHS and RHS are same size, LHS whole identifier of
   *  unsigned, RHS is signed constant, then coerce RHS to unsigend.
   *
   * This simplification is done to minimize regolds required for
   * turning on newSizing, and to reduce checks required in later phases 
   */
  void adjustRHSSign();

  //! Equivalence test
  bool operator==(const NUStmt&) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const;
  
  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! destructor
  virtual ~NUAssign();

  //! In Verilog semantics, does this node preserve a Z on src
  //! when it drives dest?
  /*!
   *! Note that this does not imply (at this time) that
   *! Carbon will preserve the Z properly -- only that Verilog
   *! semantics require it.  This routine is for use in Nucleus
   *! transformation code, or in code that warns about potential
   *! mismatches with simulation
   */
  virtual bool shouldPreserveZ(NUNet* src, NUNet* dest) const;

  //! Source and Destination overlap in this assignment.  noCover=> 1:1 overlap not allowed
  virtual bool overlaps (NUNetRefFactory* factory, bool noCover) const = 0;

protected:
  //! Return the specific type of assign for debug printing.
  virtual const char *getPrintName() const = 0;

  //! The LHS.
  NULvalue *mLvalue;

  //! The RHS.
  NUExpr *mRvalue;


private:
  //! Hide copy and assign constructors.
  NUAssign(const NUAssign&);
  NUAssign& operator=(const NUAssign&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUAssign : public NUStmt


//! NUBlockingAssign class
/*!
 * A blocking assignment.
 */
class NUBlockingAssign : public NUAssign
{
public:
  //! constructor
  /*!
    \param lvalue The LHS of the assign.
    \param rvalue The RHS of the assign.
    \param usesCFNet true when this needs $cfnet in it's use list
    \param loc The source location of the assign.
    \param do_resize Do an explicit resize?  You will want this to be true
                     for the most part, unless you have already set the size
                     of the lhs and rhs appropriately.  For example, the copy
                     method uses false.
   */
  NUBlockingAssign(NULvalue *lvalue,
		   NUExpr *rvalue,
                   bool usesCFNet,
		   const SourceLocator& loc,
                   bool do_resize = true) : 
    NUAssign (lvalue, rvalue, usesCFNet, loc, do_resize)
  {}
  
  //! Return the type of this class
  virtual NUType getType() const { return eNUBlockingAssign; }

  //! Create an assignment of the same subclass as this, also
  //! inheriting strength, cfnet, loc
  virtual NUAssign* create(NULvalue *lvalue, NUExpr *rvalue,
                           NUScope* scope, const char* name);

  //! Add the nets this node uses for blocking defs to the given set.
  void getBlockingUses(NUNetSet *uses) const;
  void getBlockingUses(NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to blockingly def the given net to the given set.
  void getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &def_net_ref,
			 const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! No-op
  void getNonBlockingUses(NUNetSet * /* uses -- unused */) const {}
  void getNonBlockingUses(NUNetRefSet * /* uses -- unused */) const {}
  bool queryNonBlockingUses(const NUNetRefHdl & /* use_net_ref -- unused */,
			    NUNetRefCompareFunction /* fn -- unused */,
			    NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! No-op
  void getNonBlockingUses(const NUNetRefHdl & /* net_ref -- unused */,
			  NUNetRefSet * /* uses -- unused */) const {}
  bool queryNonBlockingUses(const NUNetRefHdl & /* def_net_ref -- unused */,
			    const NUNetRefHdl & /* use_net_ref -- unused */,
			    NUNetRefCompareFunction /* fn -- unused */,
			    NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this node blockingly defines to the given set.
  void getBlockingDefs(NUNetSet *uses) const;
  void getBlockingDefs(NUNetRefSet *uses) const;
  bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! No-op
  void getNonBlockingDefs(NUNetSet * /* uses -- unused */) const {}
  void getNonBlockingDefs(NUNetRefSet * /* uses -- unused */) const {}
  bool queryNonBlockingDefs(const NUNetRefHdl & /* def_net_ref -- unused */,
			    NUNetRefCompareFunction /* fn -- unused */,
			    NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this node blockingly kills to the given set.
  void getBlockingKills(NUNetSet * kills) const;
  void getBlockingKills(NUNetRefSet * kills) const;
  bool queryBlockingKills(const NUNetRefHdl &def_net_ref,
			  NUNetRefCompareFunction fn,
			  NUNetRefFactory *factory) const;

  //! No-op
  void getNonBlockingKills(NUNetSet * /* kills -- unused */) const {}
  void getNonBlockingKills(NUNetRefSet * /* kills -- unused */) const {}
  bool queryNonBlockingKills(const NUNetRefHdl & /* def_net_ref -- unused */,
			     NUNetRefCompareFunction /* fn -- unused */,
			     NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose (UtString*, const STBranchNode* scope, int indent, bool recurse) const;
  
  //! Return a string identifying this type.
  virtual const char* typeStr() const;
  
  //! Generate a BDD based on the def of a net
  /*!
    \param net The net
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  virtual BDD bdd(NUNet* net, BDDContext *ctx, STBranchNode *scope = 0) const;

  //! calculate cost, add it to NUCost* argument
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! Return a copy of this statement; recurses to copy nested statements.
  NUStmt* copy(CopyContext &copy_context) const;

  //! destructor
  ~NUBlockingAssign();

  //! Equivalence
  bool operator==(const NUStmt&) const;

  //! Source and Destination overlap in this assignment.  noCover=> 1:1 overlap not allowed
  virtual bool overlaps (NUNetRefFactory* factory, bool noCover) const;

protected:
  //! Return the specific type of assign for debug printing.
  const char *getPrintName() const { return "BlockingAssign"; }
  

private:
  //! Hide copy and assign constructors.
  NUBlockingAssign(const NUBlockingAssign&);
  NUBlockingAssign& operator=(const NUBlockingAssign&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUBlockingAssign : public NUAssign


//! NUNonBlockingAssign class
/*!
 * A non-blocking assignment.
 *
 * These can make it to code gen; they should be treated just like non-blocking
 * assigns at that point.
 */
class NUNonBlockingAssign : public NUAssign
{
public:
  //! constructor
  /*!
    \param lvalue The LHS of the assign.
    \param rvalue The RHS of the assign.
    \param usesCFNet true when this needs $cfnet in it's use list
    \param loc The source location of the assign.
    \param do_resize Do an explicit resize?  You will want this to be true
                     for the most part, unless you have already set the size
                     of the lhs and rhs appropriately.  For example, the copy
                     method uses false.
   */
  NUNonBlockingAssign(NULvalue *lvalue,
		      NUExpr *rvalue,
                      bool usesCFNet,
		      const SourceLocator& loc,
                      bool do_resize = true) : 
    NUAssign (lvalue, rvalue, usesCFNet, loc, do_resize)
  {}

  //! Create an assignment of the same subclass as this, also
  //! inheriting strength, cfnet, loc
  virtual NUAssign* create(NULvalue *lvalue, NUExpr *rvalue,
                           NUScope* scope, const char* name);

  //! Return the type of this class
  virtual NUType getType() const { return eNUNonBlockingAssign; }

  //! No-op
  void getBlockingUses(NUNetSet * /* uses -- unused */) const {}
  void getBlockingUses(NUNetRefSet * /* uses -- unused */) const {}
  bool queryBlockingUses(const NUNetRefHdl & /* use_net_ref -- unused */,
			 NUNetRefCompareFunction /* fn -- unused */,
			 NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! No-op
  void getBlockingUses(const NUNetRefHdl & /* net_ref -- unused */,
		       NUNetRefSet * /* uses -- unused */) const {}
  bool queryBlockingUses(const NUNetRefHdl & /* def_net_ref -- unused */,
			 const NUNetRefHdl & /* use_net_ref -- unused */,
			 NUNetRefCompareFunction /* fn -- unused */,
			 NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this node uses for non-blocking defs to the given set.
  void getNonBlockingUses(NUNetSet *uses) const;
  void getNonBlockingUses(NUNetRefSet *uses) const;
  bool queryNonBlockingUses(const NUNetRefHdl &use_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Add the nets this node uses to blockingly def the given net to the given set.
  void getNonBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryNonBlockingUses(const NUNetRefHdl &def_net_ref,
			    const NUNetRefHdl &use_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! No-op
  void getBlockingDefs(NUNetSet * /* uses -- unused */) const {}
  void getBlockingDefs(NUNetRefSet * /* uses -- unused */) const {}
  bool queryBlockingDefs(const NUNetRefHdl & /* def_net_ref -- unused */,
			 NUNetRefCompareFunction /* fn -- unused */,
			 NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this node non-blockingly defines to the given set.
  void getNonBlockingDefs(NUNetSet *uses) const;
  void getNonBlockingDefs(NUNetRefSet *uses) const;
  bool queryNonBlockingDefs(const NUNetRefHdl &def_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! No-op
  void getBlockingKills(NUNetSet * /* kills -- unused */) const {}
  void getBlockingKills(NUNetRefSet * /* kills -- unused */) const {}
  bool queryBlockingKills(const NUNetRefHdl & /* def_net_ref -- unused */,
			  NUNetRefCompareFunction /* fn -- unused */,
			  NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this node non-blockingly kills to the given set.
  void getNonBlockingKills(NUNetSet *kills) const;
  void getNonBlockingKills(NUNetRefSet *kills) const;
  bool queryNonBlockingKills(const NUNetRefHdl &def_net_ref,
			     NUNetRefCompareFunction fn,
			     NUNetRefFactory *factory) const;

  //! Return a blocking assign to use in place of this.
  /*!
   * Used by non-blocking reordering; this assign should be immediately
   * deleted as it is no longer useful.
   */
  NUBlockingAssign *makeBlocking();

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose (UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return a string identifying this type.
  virtual const char* typeStr() const;
  
  //! Return a copy of this statement; recurses to copy nested statements.
  NUStmt* copy(CopyContext &copy_context) const;

  //! calculate cost, add it to NUCost* argument
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUNonBlockingAssign();

  //! Equivalence
  bool operator==(const NUStmt&) const;

  //! Source and Destination overlap in this assignment.  noCover=> 1:1 overlap not allowed
  virtual bool overlaps (NUNetRefFactory* factory, bool noCover) const;


protected:
  //! Return the specific type of assign for debug printing.
  const char *getPrintName() const { return "NonBlockingAssign"; }

private:
  //! Hide copy and assign constructors.
  NUNonBlockingAssign(const NUNonBlockingAssign&);
  NUNonBlockingAssign& operator=(const NUNonBlockingAssign&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUNonBlockingAssign : public NUAssign

//! NUContAssign class
/*!
 * A continuous assignment.
 *
 * Note: these are not really statements, but deriving from NUStmt to
 * get code reuse.  So, we need to define the various blocking and
 * non-blocking use/def/kill functions, even though they can never be
 * called.
 */
class NUContAssign : public NUAssign
{
public:
  //! constructor
  /*!
    \param name The name of this continuous assign, for code generation.
    \param lvalue The LHS of the assign.
    \param rvalue The RHS of the assign.
    \param loc The source location of the assign.
    \param do_resize Do an explicit resize?  You will want this to be true
                     for the most part, unless you have already set the size
                     of the lhs and rhs appropriately.  For example, the copy
                     method uses false.
   */
  NUContAssign(StringAtom *name,
	       NULvalue *lvalue,
	       NUExpr *rvalue,
	       const SourceLocator& loc,
	       Strength strength=eStrDrive,
               bool do_resize = true) :
    NUAssign(lvalue, rvalue, false, loc, do_resize), // a contAssign can never use a cfNet
    mName(name), 
    mStrength(strength),
    mScheduled (false)
  {}
  
  //! Create an assignment of the same subclass as this, also
  //! inheriting strength, cfnet, loc
  virtual NUAssign* create(NULvalue *lvalue, NUExpr *rvalue,
                           NUScope* scope, const char* name);

  //! Return the type of this class
  virtual NUType getType() const { return eNUContAssign; }

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose (UtString*, const STBranchNode* scope, int indent, bool recurse) const;
  
  //! Return a string identifying this type.
  virtual const char* typeStr() const;
  
  //! Return the LHS of the assign.
  NULvalue* getLvalue() const { return mLvalue; }

  //! Return the RHS of the assign.
  NUExpr* getRvalue() const { return mRvalue; }
  
  //! Add the nets this assign uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this assign uses to def net to the given set.
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the set of nets this assign defs to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! A complete def means that all bits are guaranteed to have a def
  /*! See NULvalue::getCompleteDefs for details.
   */
  void getCompleteDefs(NUNetSet* defs) const;

  //! A complete def means that all bits are guaranteed to have a def
  /*! See NULvalue::getCompleteDefs for details
   */
  void getCompleteDefs(NUNetRefSet* defs) const;

  //! Invalid to call.
  void getBlockingUses(NUNetSet *uses) const;
  void getBlockingUses(NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getBlockingUses(NUNet *net, NUNetSet *uses) const;
  void getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &def_net_ref,
			 const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getNonBlockingUses(NUNetSet *uses) const;
  void getNonBlockingUses(NUNetRefSet *uses) const;
  bool queryNonBlockingUses(const NUNetRefHdl &use_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getNonBlockingUses(NUNet *net, NUNetSet *uses) const;
  void getNonBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryNonBlockingUses(const NUNetRefHdl &def_net_ref,
			    const NUNetRefHdl &use_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getBlockingDefs(NUNetSet *uses) const;
  void getBlockingDefs(NUNetRefSet *uses) const;
  bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getNonBlockingDefs(NUNetSet *uses) const;
  void getNonBlockingDefs(NUNetRefSet *uses) const;
  bool queryNonBlockingDefs(const NUNetRefHdl &def_net_ref,
			    NUNetRefCompareFunction fn,
			    NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getBlockingKills(NUNetSet *kills) const;
  void getBlockingKills(NUNetRefSet *kills) const;
  bool queryBlockingKills(const NUNetRefHdl &def_net_ref,
			  NUNetRefCompareFunction fn,
			  NUNetRefFactory *factory) const;

  //! Invalid to call.
  void getNonBlockingKills(NUNetSet *kills) const;
  void getNonBlockingKills(NUNetRefSet *kills) const;
  bool queryNonBlockingKills(const NUNetRefHdl &def_net_ref,
			     NUNetRefCompareFunction fn,
			     NUNetRefFactory *factory) const;

  //! Try to construct an enabled driver from this assign.
  /*!
    \param new_driver Will be updated to point to the created enabled driver.
    \returns true if successful in creating an enabled driver, false otherwise.

    If true is returned, this assign must be deleted, as it is no longer valid.

    Note that true may be returned, and new_driver may be set to 0, if this assign
    drives Z unconditionally.
  */
  bool makeEnabledDriver(NUEnabledDriver** new_driver);

  //! Return the name of the continuous assign.
  /*!
   * We name continuous assignments for code generation; there is no scoping use
   * of the name.
   */
  StringAtom* getName() const { return mName; }

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! Print the u/d information to cout
  /*!
   * For U/D stuff, this behaves like a NUUseDefNode and not a NUUseDefStmtNode,
   * so override this to do the right thing.
   */
  void printUDSets(NUNetRefFactory *netref_factory) const;

  //! Return the strength of the drive of this node.
  Strength getStrength() const { return mStrength; }

  //! Return the parent module
  const NUModule* findParentModule() const;
  NUModule* findParentModule();

  //! Return a copy of this statement; recurses to copy nested statements.
  NUStmt* copy(CopyContext &copy_context) const;

  //! Returns a blocking assignment copy of this continous assignment
  /*! This is used when we merge assignments to make bigger blocks.
   */
  NUStmt* copyBlocking() const;

  //! calculate cost, add it to NUCost* argument
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! Check if this is a constant continous assign
  /*! A constant continous assign is of the form assign x =
   *  \<constant\>; We optimized these types of assigns so we should not
   *  perform other optimizations on this construct
   */
  bool isConstantAssign(const IODBNucleus* iodb) const;

  //! Is this node a continuous driver?
  virtual bool isContDriver() const { return true; }

  //! destructor
  ~NUContAssign() {}

  //! Equivalence
  bool operator==(const NUStmt&) const;

  //! Use this method to decide whether to call the 'getBlocking*' or 'get*' methods
  virtual bool useBlockingMethods() const;

  //! Can this node drive a Z on the specified net?
  virtual bool drivesZ(NUNet*) const;

  bool isScheduled (void) const;

  void putScheduled (bool);

  //! Source and Destination overlap in this assignment.  noCover=> 1:1 overlap not allowed
  virtual bool overlaps (NUNetRefFactory* factory, bool noCover) const;

protected:
  //! Return the specific type of assign for debug printing.
  const char *getPrintName() const { return "ContAssign"; }


private:
  //! Hide copy and assign constructors.
  NUContAssign(const NUContAssign&);
  NUContAssign& operator=(const NUContAssign&);

  //! Name of this continuous assign
  StringAtom* mName;

  //! Strength of this drive
  Strength mStrength;

  //! This continuous assign has been scheduled for execution
  bool mScheduled;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUContAssign : public NUAssign

#endif
