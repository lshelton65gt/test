// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004, 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUSTMTREPLACE_H_
#define NUSTMTREPLACE_H_

#include "nucleus/NUDesignWalker.h"

//! NUStmtReplaceCallback class
/*!
 * This class replaces statements with other statements.
 *
 * Be careful when using this class, because it deletes the
 * old statements immediately when doing the replace, and you
 * could get in trouble with a pointer-to-pointer map if the
 * old NUStmt you freed gets re-allocated.
 */
class NUStmtReplaceCallback : public NUDesignCallback
{
public:
  NUStmtReplaceCallback();
  ~NUStmtReplaceCallback();

  Status operator()(Phase, NUBase*);

  //! Do not go into submodules; just do this module.
  Status operator()(Phase, NUModuleInstance*);

  //! Callback to walk task enables, return eDelete if we want to replace it.
  Status operator()(Phase, NUStmt* stmt);

  //! Callback to give a replacement statement for the given statement.
  virtual NUStmt *replacement(NUStmt *stmt);

  //! Add a replacement to the internal map
  void addReplacement(NUStmt* old, NUStmt* newStmt);

private:
  // forbid copy ctor and assign op
  NUStmtReplaceCallback& operator=(const NUStmtReplaceCallback& src);
  NUStmtReplaceCallback(const NUStmtReplaceCallback& src);

  //! Map of task enable to be replaced with its replacement.
  NUStmtStmtMap* mReplacements;
};

#endif // NUSTMTREPLACE_H_
