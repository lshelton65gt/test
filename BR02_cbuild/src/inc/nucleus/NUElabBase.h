// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _NU_ELAB_BASE_
#define _NU_ELAB_BASE_
#include "nucleus/NUBase.h"

class STSymbolTableNode;
class STBranchNode;
class UtString;

//! NUElabBase class
class NUElabBase: public NUBase
{
public:
  NUElabBase(STSymbolTableNode*);
  ~NUElabBase();

  //! Get the symbol table node for this this object
  STSymbolTableNode* getSymNode() {return mSymNode;}

  //! Get the symbol table node for this this object (const version)
  const STSymbolTableNode* getSymNode() const {return mSymNode;}

  //! print the hierarchical name to cout
  void pname() const;

  //! \return the source location for this object
  virtual const SourceLocator getSourceLocation () const = 0;

  //! locate the unelaborated symbol for an elaborated object
  /*! \arg stab the unelaborated symbol table
   *  \arg unelab returns the unelaborated symbol from stab for this
   *  \return iff an unelaborated symbol was located in stab
   */
  virtual bool findUnelaborated (STSymbolTable *stab, STSymbolTableNode **unelab) const = 0;
  
  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope if not null and hierarchical name being generated then name is generated within this hierarchy
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  void compose(UtString*, const STBranchNode* scope, bool includeRoot = true, bool hierName = true, const char* separator = ".") const;

  //! Alphabetic comparison of name hierarchies.  -1 is <; 0 is ==; 1 is >
  static int compare(const NUElabBase* n1, const NUElabBase* n2);

  //! comparison
  bool operator<(const NUElabBase& n1) const {
    return compare(this, &n1) < 0;
  }
  
  //! find an NUElabBase given a symbol table node, or return NULL if there is none
  static NUElabBase* find(const STSymbolTableNode* node);

  /*! 
   * Some of the elaborated nucleus objects are not "owned" by anyone
   * except the symbol table, so delete whatever we need to.
   */
  static void deleteObjects(STSymbolTable * symtab);

  //! Print design object information about an assertion
  virtual void printAssertInfoHelper() const;


protected:
  STSymbolTableNode* mSymNode;
} APIOMITTEDCLASS; //class NUElabBase: public NUBase
#endif
