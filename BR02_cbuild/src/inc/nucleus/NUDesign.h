// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NUDESIGN_H_
#define NUDESIGN_H_

#include "nucleus/NUModule.h"
#include "util/CarbonTypes.h"
#include "util/LoopThunk.h"
#include "util/UtHashSet.h"


class STSymbolTableNode;
class NUAliasBOM;
class MsgContext;
class SCHScheduleFactory;
class IODBNucleus;
class STFieldBOM;

/*!
  \file
  Declaration of the Nucleus design class.
*/

//! NUDesign class
/*! 
 *  The whole design.
 */
class NUDesign : public NUBase
{
#if ! pfGCC_2
  using NUBase::printElab;
#endif

public:
  //! constructor
  NUDesign(STSymbolTable* pSTSymbolTable,
           IODBNucleus*   pIODBNucleus,
           SourceLocatorFactory*  pSourceLocatorFactory,
           SCHScheduleFactory*    pSCHScheduleFactory,
           MsgContext* pMsgContext,
           ArgProc* pArgProc,
           NUNetRefFactory* pNUNetRefFactory);
 

  //! Add the top-level modules to the given list.
  void getTopLevelModules(NUModuleList *module_list) const;

  //! Add all modules to the given list.
  void getAllModules(NUModuleList *module_list) const;

  typedef NUModuleSet::SortedLoop ModuleLoop;

private:
  struct PortExtractor {
    PortExtractor() {mFlags = (NetFlags) 0;}
    PortExtractor(NetFlags flags): mFlags(flags) {}
    NUNetFilterLoop operator()(NUModule* mod) {
      return mod->loopPortMask(mFlags);
    }
    NetFlags mFlags;
  };

public:
  typedef UtHashSet<STSymbolTableNode*, HashPointerValue<STSymbolTableNode*> >
    NameSet;
  typedef NameSet::SortedLoop NameSetLoop;

  //! Loop over all the top level modules in the design
  ModuleLoop loopTopLevelModules() const;

  //! Loop over all the modules in the design
  ModuleLoop loopAllModules() const;

  //! Get the modules in bottom-up order
  void getModulesBottomUp(NUModuleList*) const;

  //! add all unprotected modules to the \a unprotected_modules list
  /*! Note only verilog modules can be protected with `protected. A module
   * instantiated within a protected region is also considered
   * protected for this method and will not be included in the return list.
   */
  void getUnprotectedModules(NUModuleList* unprotected_modules);
  
  typedef LoopThunk<ModuleLoop, NUNetFilterLoop, PortExtractor> PortLoop;

  //! Iterate over top-level module input ports
  PortLoop loopInputPorts() const {return PortLoop(loopTopLevelModules(),
                                                   PortExtractor(eInputNet));}

  //! Iterate over top-level module output ports
  PortLoop loopOutputPorts() const {return PortLoop(loopTopLevelModules(),
                                                   PortExtractor(eOutputNet));}

  //! Iterate over top-level module bidirectional ports
  PortLoop loopBidPorts() const {return PortLoop(loopTopLevelModules(),
                                                 PortExtractor(eBidNet));}

  struct NetLoopGen {NUNetCLoop operator()(const NUModule* mod) {
    return mod->loopLocals();
  }};
  typedef LoopThunk<ModuleLoop, NUNetCLoop, NetLoopGen> NetLoop;

  //! Loop over all top-level module locals
  NetLoop loopLocals() const;

  //! Loop over all nets declared observable by the user
  NameSetLoop loopObservable() {return mObservable.loopSorted();}

  //! Loop over all nets declared as clock-masters by the user
  NameSetLoop loopClockMasters() {return mClockMasters.loopSorted();}

  //! Reset the set of clock masters.
  void resetClockMasters() { mClockMasters.clear(); }

  //! Loop over all nets declared observable by the user
  //NUNetElabCLoop loopObservable() const {return NUNetElabCLoop(mObservable);}
  
  //! Mark a design net as observable
  void observeNet(NUNetElab* net);

  //! Clear the set of additional elaborated design outputs
  /*  Elaboration occurs more than once in the compiler. This is done
   *  with different symbol tables. So some of the additional
   *  elaborated design outputs that was accumulated can become
   *  invalid. This function clears the data so that it can be
   *  repopulated with data from a newer elaboration pass.
   *
   *  This could have been done at the time of repopulation, but we
   *  get a better memory footprint for the compiler if we clear the
   *  data as soon as it has been deleted.
   *
   *  The current set of elaborated design outputs are:
   *
   *   - observed elaborated nets (from directives and internal nets)
   *
   *   - clock masters
   */
  void clearDesignOutputs();

  /* \brief this should be called after the final elaboration of
      design, to process elaborated virtual nets 
  
    Specifically this method: adds the virtual nets for: {the output
    system task, and the control flow net} to the output nets in
    the design.  Other special handling of nets could be placed here.
  */
  void handleElabInternalNets();

  //! see handleElabInternalNets for info
  void handleElabInternalNetsHelper(NUNet* net, StringAtom* modName);

  //! Mark a design net as a clock-master
  void addClockMaster(NUNetElab* net);

  //! Make the given module be a top-level module
  void addTopLevelModule(NUModule* module)
    APIDISTILLMACRO3(APIEMITBIND(mAllModules),
                     APIEMITCODE(mAllModules.insert(module)),
                     APIEMITCODE(mTopLevelModules.insert(module)));

  //! Remember the given module as a module in the design.
  /*!
   * Keep track of all modules in the design, top-level or not, as a matter
   * of book-keeping.
   */

  void addModule(NUModule* module) APIDISTILLMACRO2(APIEMITBIND(mAllModules),
                                                    APIEMITCODE(mAllModules.insert(module)));

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! For top level modules, return an index for them to be lookup-up by.
  SInt32 reserveSymtabIndex()
    APIDISTILLMACRO1(APIEMITCODE(return mCurSymtabIndex++));




  //! Dump myself to UtIO::cout().
  virtual void print(bool recurse, int indent) const;

  //! Dump the design's elaborated flow to UtIO::cout().
  virtual void printElab(bool recurse=true, int indent=0)
    const;

  //! Return a UtString identifying this type.
  virtual const char* typeStr() const;

  //! Debug function to find a NetElab by hierchical name
  NUNetElab* findNetElab(const char* name);

  //! Debug function to find a Net by hierchical name or module.name
  NUNet* findNet(const char* name);

  //! Debug function to find a ModuleElab by hierchical name
  NUModuleElab* findMod(const char* name);

  //! Add a c-model to this design
  void addCModelInterface(UtString& name, NUCModelInterface* cmodelInterface) 
    APIDISTILLMACRO5(APIEMITBIND(mAllCModelInterfaces),
                     APIEMITBIND(mCModelInterfaceVector),
                     APIEMITCODE(typedef xCModelInterfaceMap::value_type xCModelInterfaceMapValue),
                     APIEMITCODE(mAllCModelInterfaces.insert(xCModelInterfaceMapValue(name, cmodelInterface))),
                     APIEMITCODE(mCModelInterfaceVector.push_back(cmodelInterface)));


  //! Find a c-model in this design
  NUCModelInterface* findCModelInterface(UtString& name) const;

  //! Iterator over the c-model interfaces in a design
  NUCModelInterfaceLoop loopCModelInterfaces()
  {
    return NUCModelInterfaceLoop(mCModelInterfaceVector);
  }

  //! Iterator over the c-model interfaces in a design
  NUCModelInterfaceCLoop loopCModelInterfaces() const
  {
    return NUCModelInterfaceCLoop(mCModelInterfaceVector);
  }

  //! Print verilog for the entire design into the specified file
  void printVerilog(const char* filename);

  //! Dump the design verilog to a directory
  void dumpVerilog(const char* fileRoot, const char* dirName,
                   MsgContext* msgContext);

  //! destructor
  ~NUDesign();

  NUAliasBOM * getAliasBOM() { return mAliasBOM; }

  //! Swap out the symbol table with a new one
  /*! This is used by global optimizations which uses a temporary
   *  symbol table.
   *
   *  \param newSymTab The new symbol table to use.
   *
   *  \returns the previous value for the symbol table so that it can
   *  be restored.
   */ 
  STSymbolTable* putSymtab(STSymbolTable* newSymTab);

  //! Abstraction for a set of instances
  typedef UtSet<NUModuleInstance*> Instances;

  //! Abstraction to iterate over a set of instances
  typedef Loop<Instances> InstancesLoop;

  //! Abstraction for all the instances of a given module
  typedef UtMap<NUModule*, Instances*> ModuleInstances;

  //! Abstraction to iterate over all modules and their instances
  typedef LoopMap<ModuleInstances> ModuleInstancesLoop;

  //! Find all module instances
  /*! Uses the elaborated symbol table to walk the design and populate
   *  the provied map with all the module instances. This is not valid
   *  before elaboration.
   *
   *  The caller should free the data created by this routine
   */
  void findModuleInstances(ModuleInstances* moduleInstances);

  //! Set the default time precision value for the simulation.
  /*! This is the value of each clock tick, and also the default
   *  $timeformat units to use.  The stored unit is updated only if the
   *  supplied value is smaller.
   */
  void setGlobalTimePrecision( SInt8 value );
  //! Get the default time precision value for the simulation
  SInt8 getGlobalTimePrecision() const;

  //! Get the design symbol table
  STSymbolTable* getSymbolTable() const {return mSymtab;}

  //! Get the atomic cache
  AtomicCache* getAtomicCache() const;

  //! Get a pointer to a set of temp nets created due to the tie net directive
  /*! This function can be used both for population and
   *  iteration. Note that this set should not be used for
   *  iteration. This is because we don't clean it up when nets are
   *  deleted. It should only be used to query whether a net is in the
   *  set or not.
   */
  NUNetSet* getTieNetTemps() { return mTieNets; }

  //! Get a pointer to the onDemand state net set
  NUConstNetSet* getOnDemandNets() { return mOnDemandNets; }

  //! Get a pointer to the map of buffered nets.  Used by OnDemand.
  NUNetElabMap* getBufferedNetMap() { return mBufferedNetMap; }

  // Experimental code for Programmer's View
#ifdef CARBON_PV
  //! copy the entire design, creating a new empty symbol table, which
  //! will be owned by the new design.  This differs from the original design,
  //! whose symbol table is owned by the CarbonContext.
  NUDesign* copy();
#endif

  NUNetRefFactory* getNetRefFactory() const {return mNetRefFactory;}
  IODBNucleus* getIODB() const {return mIODB;}
  SCHScheduleFactory* getScheduleFactory() const {return mScheduleFactory;}
  ArgProc* getArgProc() const {return mArgProc;}
  MsgContext* getMsgContext() const {return mMsgContext;}
  SourceLocatorFactory* getSourceLocatorFactory() const {
    return mSourceLocatorFactory;
  }

private:
  //! Cleanup elaboration memory.
  void cleanupElab();

  //! Hide copy and assign constructors.
  NUDesign(const NUDesign&);
  NUDesign& operator=(const NUDesign&);

  //! Symbol table to use.
  STSymbolTable *mSymtab;

  //! Currently-available symtab index for top-level modules
  SInt32 mCurSymtabIndex APIDISTILLMACRO2(APICONSTRUCTORFALSE,
                                          APIEMITCODE(mCurSymtabIndex = 0));



  //! Set of top-level modules
  NUModuleSet mTopLevelModules  APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Set of all modules
  NUModuleSet mAllModules APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  typedef ModuleInstances::value_type ModuleInstancesValue;

  // Map of the c-models in this design
  CModelInterfaceMap mAllCModelInterfaces APIDISTILLMACRO1(APICONSTRUCTORFALSE);
  NUCModelInterfaceVector mCModelInterfaceVector APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Set of all observable nets pathnames
  NameSet mObservable APIOMITTEDFIELD;

  //! Set of all clock master pathnames
  NameSet mClockMasters APIOMITTEDFIELD;

  NUAliasBOM * mAliasBOM APIOMITTEDFIELD;

  //! The global time precision (aka simulatin time unit)
  /*! This is defined as the smallest `timescale time precision
   *  specified in the design.  Each clock tick represents this amount
   *  of time.  When printing $time/$stime/$realtime in a $display
   *  statement, the default $timeformat system task supplies the
   *  smallest time precision unit specified anywhere in the design's
   *  `timescale directives.  (Verilog LRM p.300) We don't yet have
   *  $timeformat implemented, but this variable is used to collect this
   *  default information. It will be used appropriately in the $display
   *  of $time/$stime/$realtime.
   */
  SInt8 mGlobalTimePrecision;

  // A set of temp nets created due to tie directives
  NUNetSet* mTieNets APIOMITTEDFIELD;

  //! Set of all NUNets that are sequential or transition scheduled - needed for onDemand
  NUConstNetSet* mOnDemandNets APIOMITTEDFIELD;

  //! Map of all buffered NUNets to their source nets
  NUNetElabMap* mBufferedNetMap APIOMITTEDFIELD;

  NUNetRefFactory* mNetRefFactory APIOMITTEDFIELD;
  SCHScheduleFactory* mScheduleFactory APIOMITTEDFIELD;
  IODBNucleus* mIODB;
  SourceLocatorFactory* mSourceLocatorFactory APIOMITTEDFIELD;
  MsgContext* mMsgContext APIOMITTEDFIELD;
  ArgProc* mArgProc APIOMITTEDFIELD;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUDesign : public NUBase

#endif
