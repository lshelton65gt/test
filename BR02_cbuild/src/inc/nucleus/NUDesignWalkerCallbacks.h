// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file

  This file contains some generally useful design walker callbacks so
  that they can be shared across optimizations.
*/

#ifndef _NUDESIGNWALKERCALLBACKS_H_
#define _NUDESIGNWALKERCALLBACKS_H_

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUNetRef.h"

//! Class to gather all the leaf level lvalues in a node that overlap a net ref
class NUGatherLvaluesCallback : public NUDesignCallback
{
public:
  //! constructor
  NUGatherLvaluesCallback(NULvalueVector* lvalues, const NUNetRefHdl& netRef,
                          NUNetRefFactory* netRefFactory) :
    mNetRef(netRef), mNetRefFactory(netRefFactory), mLvalues(lvalues) {}

  //! Override base function to do nothing
  Status operator()(Phase, NUBase*);

  //! Override the memsel lvalue to gather that
  Status operator()(Phase phase, NUMemselLvalue* memsel);

  //! Override the varsel lvalue to gather that
  Status operator()(Phase phase, NUVarselLvalue* varsel);

  //! Override the ident lvalue to gather that
  Status operator()(Phase phase, NUIdentLvalue* ident);

private:
  //! Handle all the leaf lvalues the same way
  Status handleLvalue(Phase phase, NULvalue* lvalue);

  //! The overlaping net ref
  NUNetRefHdl mNetRef;

  //! Factory for overlap testing
  NUNetRefFactory* mNetRefFactory;

  //! Place to store the lvalues
  NULvalueVector* mLvalues;
}; // class NUGatherLvaluesCallback : public NUDesignCallback

//! Class to find dynamic lvalues
/*! Dynamic bit selects on the left hand side do not write to all bits
 *  of the net. However, flow must be pessimistic and so the graph
 *  looks like it does. This results in invalid optimizations.
 *
 *  Concat lvalues can reorder the bits and it makes it difficult to
 *  apply the optimization. So we give up.
 *
 *  This walker visits all defs from any given point. So if one def
 *  needs to be tested, it should be called on the statements for leaf
 *  level flow.
 */
class NUFindDynamicLvalues : public NUDesignCallback
{
public:
  //! Override the base callback to just continue looking
  Status operator()(Phase, NUBase*);

  //! Override the varsel lvalue callback to test for it
  Status operator()(Phase phase, NUVarselLvalue* varselLvalue);

  //! Override the memsel lvalue callback to test for dynamic selects
  Status operator()(Phase phase, NUMemselLvalue* memselLvalue);

  //! Override concat lvalues since we should not process them
  Status operator()(Phase, NUConcatLvalue*);
};

#endif // _NUDESIGNWALKERCALLBACKS_H_
