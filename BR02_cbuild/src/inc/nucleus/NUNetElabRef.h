/***************************************************************************************
  Copyright (c) 2002-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef NUNETELABREF_H_
#define NUNETELABREF_H_

#include "nucleus/NUNetRef.h"

#include "util/UtSet.h"
#include "util/UtList.h"
#include "util/UtMultiMap.h"

#include "util/UtHashSet.h"
#include "util/UtHashMap.h"

/*!
  \file 
  Declaration of elaborated net ref representation.  
 */
class NUNetElabRef
{
public:

  //! Return the unelaborated net associated with this elab ref.
  NUNet * getNet() const { 
    return mNetRef->getNet(); 
  }

  //! Return the unelaborated net ref.
  NUNetRefHdl getNetRef() const {
    return mNetRef;
  }

  //! Return the elaborated net associated with this elab ref.
  NUNetElab * getNetElab() const {
    return mNetRef->getNet()->lookupElab(mScope);
  }

  //! Return the hierarchy
  STBranchNode * getScope() const { 
    return mScope;
  }

  //! Ordering comparison for containers
  bool operator<(const NUNetElabRef& other) const {
    return compare(this, &other) < 0;
  }

  //! Hashing for containers.
  size_t hash() const { return (((size_t)mScope) + mNetRef->hash()); }

  //! Are they the same ref?
  bool operator==(const NUNetElabRef & other) const { 
    return compare(this,&other) == 0;
  }

  //! Are they different refs?
  bool operator!=(const NUNetElabRef & other) const { 
    return compare(this,&other) != 0;
  }

  //! Ordering comparison for containers
  static int compare(const NUNetElabRef* ref1, const NUNetElabRef* ref2);

  //! debug help: return the type of what I'm looking at
  const char* typeStr() const { return "NUNetElabRef"; }

  //! debug help: print routine
  void print(int indent = 0) const;

protected:
  friend class NUNetElabRefFactory;
  friend class NUNetElabRefHdl;

  //! Constructor
  NUNetElabRef(STBranchNode * scope, NUNetRefHdl & netRef) :
    mScope(scope),
    mNetRef(netRef),
    mRefCnt(0)
  {}

  //! Destructor
  ~NUNetElabRef();

  //! Increment reference count. (used by NUNetElabRefHdl)
  void incCount() { ++mRefCnt; }

  //! Decrement reference count. (used by NUNetElabRefHdl)
  void decCount() { --mRefCnt; }
private:
  //! Hide copy and assign constructors.
  NUNetElabRef(const NUNetElabRef&);
  NUNetElabRef& operator=(const NUNetElabRef&);

  //! Branch node defining the elaborated scope for this elab ref.
  STBranchNode * mScope;

  //! Unelaborated net-ref
  NUNetRefHdl mNetRef;

  //! The number of references to this.
  SInt32 mRefCnt;
};


class NUNetElabRefHdl
{
public:
  //! constructor
  NUNetElabRefHdl(NUNetElabRef *ref) : 
    mNetRef(ref) 
  {
    mNetRef->incCount();
  }

  //! destructor
  ~NUNetElabRefHdl() {
    mNetRef->decCount();
  }

  //! copy
  NUNetElabRefHdl(const NUNetElabRefHdl& src) :
    mNetRef(src.mNetRef)
  {
    mNetRef->incCount();
  }

  //! assign
  NUNetElabRefHdl& operator=(const NUNetElabRefHdl& src) {
    if (&src != this) {
      mNetRef->decCount();
      mNetRef = src.mNetRef;
      mNetRef->incCount();
    }
    return *this;
  }

  //! dereference - return the underlying object
  const NUNetElabRef* operator*() const { return mNetRef; }

  //! dereference - return the underlying object
  const NUNetElabRef* operator->() const { return mNetRef; }

  //! Comparison - wrap the comparison of the ref.
  bool operator<(const NUNetElabRefHdl& ref) const {
    return (*mNetRef) < (*(ref.mNetRef));
  }

  //! Hashing for containers.
  size_t hash() const {return mNetRef->hash();}

  //! Do two handles point to the same ref?
  bool operator==(const NUNetElabRefHdl & other) const { 
    return (*mNetRef==*other.mNetRef);
  }

  //! Do two handles point to different refs?
  bool operator!=(const NUNetElabRefHdl & other) const { 
    return (*mNetRef!=*other.mNetRef);
  }

private:
  //! Underlying ref
  NUNetElabRef *mNetRef;
};

//! Comparison functor
class NUNetElabRefHdlCompare
{
public:
  bool operator()(const NUNetElabRefHdl & one, const NUNetElabRefHdl & two) const
  {
    return NUNetElabRef::compare(*one, *two) < 0;
  }
};

typedef UtSet<NUNetElabRefHdl,NUNetElabRefHdlCompare> NUNetElabRefSet;
typedef UtMultiMap<NUNetElabRefHdl,NUNetElabRefHdl> NUNetElabRefMultiMap;
typedef UtList<NUNetElabRefHdl> NUNetElabRefList;

typedef UtHashSet<NUNetElabRefHdl,HashValue<NUNetElabRefHdl> > NUNetElabRefHashSet;
typedef UtHashMap<NUNetElabRefHdl,NUNetElabRefHashSet,HashValue<NUNetElabRefHdl> > NUNetElabRefToSetHashMap;


//! Factory to manage net elab refs.
class NUNetElabRefFactory
{
public:
  //! Constructor
  NUNetElabRefFactory(NUNetRefFactory * netRefFactory);
  
  //! Destructor
  ~NUNetElabRefFactory();

  //! Create an elaborated reference to a net ref
  NUNetElabRefHdl createNetElabRef(STBranchNode * branch, NUNetRefHdl & netRef);
  
  //! Merge:  result = (one union two)
  NUNetElabRefHdl merge(const NUNetElabRefHdl& one, const NUNetElabRefHdl& two);

  //! Intersect:  result = (one intersect two)
  NUNetElabRefHdl intersect(const NUNetElabRefHdl& one, const NUNetElabRefHdl& two);

  //! Subtract:  result = (one minus two)
  NUNetElabRefHdl subtract(const NUNetElabRefHdl& one, const NUNetElabRefHdl& two);

  //! printStats
  void printStats(int indent_arg=0);

private:
  //! Hide copy and assign constructors.
  NUNetElabRefFactory(const NUNetElabRefFactory&);
  NUNetElabRefFactory& operator=(const NUNetElabRefFactory&);

  //! Insert the given net ref into the pool.
  /*!
   * If the given net ref is already represented by another in the pool, then the
   * given will be deleted.
   */
  NUNetElabRefHdl insert(NUNetElabRef* ref);

  //! Comparison so we can use set to manage the pool
  class SetCompare
  {
  public:
    bool operator()(NUNetElabRef* one, NUNetElabRef* two) const
    {
      return NUNetElabRef::compare(one, two) < 0;
    }
  };

  typedef UtHashSet<NUNetElabRef*,HashPointerValue<NUNetElabRef*> > InternalPool;
  InternalPool mPool;

  //! The unelaborated net-ref factory.
  NUNetRefFactory * mNetRefFactory;
};



//! Alternative implementation of NUNetElabRefSet
/*!
 *! The original NUNetElabRefSet above doesn't meet the needs
 *! of RENetWarning (where this was originally implemented) or
 *! ExprResynth.cxx.  In particular, NUNetElabRefSet will not see that 
 *! a.b.c[1] is a subset of a.b.c[3:0].
 *!
 *! At some point these 2 implementations should be merged.  
 *!
 *! This one uses a UtHashMap<NUNetElab*,NUNetRefHdl> and that way it can
 *! keep track of what bits of what nets are covered.
 */
class NUNetElabRefSet2 {
  typedef UtHashMap<NUNetElab*, NUNetRefHdl> ElabRefMap;
public:
  typedef ElabRefMap::iterator iterator;
  typedef ElabRefMap::const_iterator const_iterator;

  //! ctor
  NUNetElabRefSet2(NUNetRefFactory* nrf);

  //! dtor
  ~NUNetElabRefSet2();

  //! Insert the elaborated net ref into the set
  void insert(NUNetElab* netElab, const NUNetRefHdl& netRef);

  //! Insert the elaborated net into the set (all bits)
  void insert(NUNetElab* netElab);

  //! Find the parts of the elaborated net ref that are not covered
  //! by the set
  NUNetRefHdl getUncovered(NUNetElab* netElab, const NUNetRefHdl& netRef)
    const;

  NUNetRefHdl getNetRef(NUNetElab* netElab) const;

  //! Loop over all the net-refs, sorted by NUNetElab
  typedef ElabRefMap::SortedLoop RefLoop;
  RefLoop loopRefs();

  //! get the factory
  NUNetRefFactory* getFactory() const {return mNetRefFactory;}

  iterator begin() {return mElabRefMap.begin();}
  iterator end() {return mElabRefMap.end();}
  const_iterator begin() const {return mElabRefMap.begin();}
  const_iterator end() const {return mElabRefMap.end();}

private:
  NUNetRefFactory* mNetRefFactory;
  ElabRefMap mElabRefMap;
};


#endif
