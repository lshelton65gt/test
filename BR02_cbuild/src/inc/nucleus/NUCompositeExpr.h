// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUCOMPOSITEEXPR_H_
#define NUCOMPOSITEEXPR_H_

#include "nucleus/NUExpr.h"
#include "nucleus/NUCompositeNet.h"


class NUCompositeExpr: public NUNaryOp
{
public:
  NUCompositeExpr( const NUExprVector& exprs, const SourceLocator& loc );

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUCompositeExpr; }

  //! Return class name
  const char* typeStr() const;

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! Indicate whether this is an array concat expr
  void setIsCompositeArray(bool isCompositeArray);

  //! Is this an array concat expr
  bool isCompositeArray() const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name upto and including specified scope, if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

protected:
  //! Return a copy of this expression tree.
  NUExpr* copyHelper(CopyContext &copy_context) const;

  //! Resize this expression to be the given size.
  void resizeHelper(UInt32 size);

private:
  bool mIsCompositeArray;

  //! Hide copy and assign constructors.
  NUCompositeExpr(const NUCompositeExpr&);
  NUCompositeExpr& operator=(const NUCompositeExpr&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCompositeExpr: public NUNaryOp


//! NUCompositeInterfaceExpr class
/*!
 * This class defines the interface to access the contents of anything
 * that represents a record.  NUCompositeSelExpr, and
 * NUCompositeFieldExpr and NUCompositeIdentRvalue are all derived from
 * this class.
 */

class NUCompositeInterfaceExpr : public NUExpr
{
public:
  NUCompositeInterfaceExpr( const SourceLocator& loc );
  virtual ~NUCompositeInterfaceExpr();

  enum CompositeType
  {
    eCompositeIdentLvalue,
    eCompositeSelLvalue, 
    eCompositeFieldLvalue,
    eCompositeIdentRvalue,
    eCompositeSelExpr,
    eCompositeFieldExpr
  };

  virtual CompositeType getCompositeType() const = 0;

  /*! \brief Return the number of fields in the composite object
   */
  virtual UInt32 getNumFields () const = 0;

  //! Return the Nth field net pointer
  virtual NUNet *getField( UInt32 fieldIndex ) const = 0;

  //! Set the Nth field net pointer.  Does no memory management.
  //virtual void putField( NUNet *new_net, UInt32 fieldIndex ) = 0;

  //! Get the underlying NUComposite expression for this object
  virtual NUCompositeInterfaceExpr *getIdentExpr() const = 0;

  //! Get the number of ranges stored in this object
  virtual UInt32 getNumDims() const = 0;

  //! Get the Nth range stored in this object
  virtual const ConstantRange *getRange( const UInt32 index ) const = 0;

  //! Get the size of the base object of this composite.
  virtual UInt32 getCompositeSize() const = 0;

  /* \brief get the NUNet corresponding to this composite expression
   *
   * This routine must be used with caution; what it actually returns is
   * NOT the precise net that will be referenced.  That net cannot be
   * determined, and will not even exist, until during post-population
   * resynthesis.  Hovever, there are several cases where it is very
   * useful to know what kind of net, at least whether it is a
   * composite/noncomposite net, a composite structure refers to.  In
   * particular, we need this information during population of a
   * bit/part select of a vector inside a record.
   */
  virtual NUNet *getNet() const = 0;

  // NUUseNode virtual functions
  //! Add the set of nets this construct uses to the given set.
  virtual void getUses(NUNetSet *uses) const;
  virtual void getUses(NUNetRefSet *uses) const;
  virtual bool queryUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
                         NUNetRefFactory *factory) const;
  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net);
  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
  */
  virtual bool replaceLeaves(NuToNuFn & translator);
  virtual void calcCost(NUCost*, NUCostContext* = 0) const;

  // NUExpr virtual functions
  virtual void getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const;
  virtual void getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses) const;

  //! Helper virtual function, which will always get an expr of the same type to cast
  virtual ptrdiff_t compareHelper(const NUExpr* otherExpr, bool compareLocator,
                                  bool comparePointer, bool compareSizeOfConsts) const;
  virtual BDD bdd(BDDContext *ctx, STBranchNode *scope=0) const;
  //! hash an expression
  virtual size_t hash() const;
  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const;

  //! Returns true if the expression or one of it's parent composites is indexed.
  /*!
    Populates the selVector with indices if provided.
  */
  bool hasSelects(NUExprVector* selVector = NULL) const;

protected:
  //! Return a copy of this expression tree.
  virtual NUExpr* copyHelper(CopyContext &copy_context) const;

private:
  //! Hide copy and assign constructors.
  NUCompositeInterfaceExpr(const NUCompositeInterfaceExpr&);
  NUCompositeInterfaceExpr& operator=(const NUCompositeInterfaceExpr&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCompositeInterfaceExpr : public NUExpr


// The sign of the NUCompositeSelExpr is not used in the population phase.
// In composite re-synthesis phase the sign of the re-synthesized expr is set based 
// on the net, it points to.
class NUCompositeSelExpr: public NUCompositeInterfaceExpr
{
public:
  //! constructor
  /*!
    \param net The identifier
    \param exprs Vector of ranges of the select
    \param loc Source location.
   */
  NUCompositeSelExpr(NUCompositeInterfaceExpr *object,
                     NUExprVector *exprs,
                     const SourceLocator& loc);

  //! constructor
  /*!
    \param net The identifier.
    \param exprs Vectors of ranges of the select. The last expression is the variable/constant bit offset.
    \param range The range of select over the bit offset.
    \param isDummyOffset Is the bit offset for part-select a dummy
                         offset that should be ignored.
    \param loc Source location.
   */
  NUCompositeSelExpr(NUCompositeInterfaceExpr *object,
                     NUExprVector* exprs, const ConstantRange range,
                     bool isDummyOffset, const SourceLocator& loc);

  virtual ~NUCompositeSelExpr();

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUCompositeSelExpr; }

  //! Return class name
  const char* typeStr() const;

  //! Get the number of indices in this select expression
  UInt32 getNumIndices() const { return mExprs.size(); }

  //! Return the index expression
  NUExpr* getIndex(UInt32 dim) const { return mExprs[dim]; }

  //! Construct LHS matching RHS
  NULvalue* Lvalue(const SourceLocator &loc) const;

  //! Push another index expression onto the composite's expression vector
  void addIndexExpr( NUExpr *expr );

  virtual CompositeType getCompositeType() const { return eCompositeSelExpr; }

  //! Get the number of composite fields from the referenced net
  virtual UInt32 getNumFields () const { return mComposite->getNumFields(); }

  //! Get the ith field from the referenced NUCompositeNet
  virtual NUNet* getField(UInt32 index) const { return mComposite->getField( index ); }

  //! Return the object the field is being selected from.
  virtual NUCompositeInterfaceExpr *getIdentExpr() const { return mComposite; }

  /* \brief get the NUNet corresponding to this composite expression
   *
   * This routine must be used with caution; what it actually returns is
   * NOT the precise net that will be referenced.  That net cannot be
   * determined, and will not even exist, until during post-population
   * resynthesis.  Hovever, there are several cases where it is very
   * useful to know what kind of net, at least whether it is a
   * composite/noncomposite net, a composite structure refers to.  In
   * particular, we need this information during population of a
   * bit/part select of a vector inside a record.
   */
  virtual NUNet *getNet() const;

  //! Get the size of the base object of this composite.
  virtual UInt32 getCompositeSize() const;

  //! Helper to allow calling the primary compose method
  virtual void compose( UtString* buf, const STBranchNode* scope, bool showRoot ) {
    NUExpr::compose( buf, scope, showRoot );
  }

  //! Return the number index expressions this memsel has
  virtual UInt32 getNumDims() const { return mComposite->getNumDims(); }

  //! Get the Nth range stored in this object
  virtual const ConstantRange *getRange( const UInt32 index ) const;

  //! Get range of the part select on the last dimension.
  const ConstantRange getPartSelRange() const;

  //! Set the range of part select on the last indexed dimension.
  void setPartSelRange(const ConstantRange& psRange);

  //! Set part select indicator as true
  void setPartSelect() { mIsPartSelect = true; }
    
  //! Is the select on the innermost dimension a part select?
  bool isPartSelect() const {
    return mIsPartSelect;
  }

  //! Is the bit offset on part-select a dummy one that should be ignored?
  /*!
    For part selects like x(3 to 5), that have a range but no index, returns true.
    For part selects like x(3+x to 5+x), that have a range and variable index, returns false.
    For selects like x(3), return false since it's not a part select.
  */
  bool isPartSelectIndexDummy() const {
    return (mIsPartSelect && mIsDummyOffset);
  }

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! Resize this expression to be the given size.
  void resizeHelper(UInt32 size);

  //! How many bits of effective value are there?
  virtual UInt32 effectiveBitSizeHelper(ExprIntMap* ebit_cache) const;

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);

  //! Dump myself to cout
  virtual void print(bool recurse, int ident) const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name upto and including specified scope,
           if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  virtual ptrdiff_t compareHelper(const NUExpr* expr, bool compareLocator,
                                  bool comparePointer, bool compareSizeOfConsts) const
  {
    const NUCompositeSelExpr* se = dynamic_cast<const NUCompositeSelExpr*>(expr);
    NU_ASSERT(se, expr);
    ptrdiff_t cmp = mComposite->compare(*(se->mComposite), compareLocator, comparePointer,
                                        compareSizeOfConsts);
    if (cmp == 0) {
      cmp = mRange.compare(se->mRange);
      if (cmp == 0) {
        UInt32 size = mExprs.size();
        for (UInt32 i = 0; (cmp == 0) && (i < size); ++i) {
          cmp = mExprs[i]->compare(*(se->mExprs[i]), compareLocator, comparePointer,
                                   compareSizeOfConsts);
        }
      }
    }
    return cmp;
  }
  virtual size_t hash() const;
  virtual size_t shallowHash() const;

  virtual UInt32 getNumArgs() const;

  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;

protected:
  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual const NUExpr *getArgHelper(UInt32 index) const;

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual NUExpr* putArgHelper(UInt32 index, NUExpr *);

  virtual NUExpr* copyHelper(CopyContext&) const;

  virtual bool shallowEqHelper (const NUExpr& otherExpr) const;

private:
  //! Hide copy and assign constructors.
  NUCompositeSelExpr(const NUCompositeSelExpr&);
  NUCompositeSelExpr& operator=(const NUCompositeSelExpr&);

  //! The net being selected on.
  NUCompositeInterfaceExpr *mComposite;

  //! The vector of index selections into the memory
  NUExprVector mExprs;

  //! The range to be applied over index (bit offset) expression. Only one
  //! index expression is supported in this case.
  ConstantRange mRange;

  //! Indicates whether the bit offset on part select is a dummy offset that should be ignored.
  bool mIsDummyOffset;

  //! Indicates whether this is a part select.
  bool mIsPartSelect;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUCompositeSelExpr: public NUCompositeInterfaceExpr


// The sign of the NUCompositeFieldExpr is not used in the population phase.
// In composite re-synthesis phase the sign of the re-synthesized expr is set based 
// on the net, it points to.
class NUCompositeFieldExpr: public NUCompositeInterfaceExpr
{
public:
  //! constructor
  /*!
    \param object A composite object, non-arrayed, that is having a field chosen from it
    \param index The numeric index of the field.
    \param loc Source location.
   */
  NUCompositeFieldExpr(NUCompositeInterfaceExpr *object,
                       UInt32 index,
                       const SourceLocator& loc);

  ~NUCompositeFieldExpr();

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUCompositeFieldExpr; }

  //! Return class name
  const char* typeStr() const;

  virtual CompositeType getCompositeType() const { return eCompositeFieldExpr; }

  //! Get the number of fields from the referenced NUCompositeNet
  virtual UInt32 getNumFields () const;

  //! Get the ith field from the referenced NUCompositeNet
  virtual NUNet* getField(UInt32 index) const;

  //! Return the object the field is being selected from.
  virtual NUCompositeInterfaceExpr *getIdentExpr() const { return mComposite; }

  /* \brief get the NUNet corresponding to this composite expression
   *
   * This routine must be used with caution; what it actually returns is
   * NOT the precise net that will be referenced.  That net cannot be
   * determined, and will not even exist, until during post-population
   * resynthesis.  Hovever, there are several cases where it is very
   * useful to know what kind of net, at least whether it is a
   * composite/noncomposite net, a composite structure refers to.  In
   * particular, we need this information during population of a
   * bit/part select of a vector inside a record.
   */
  virtual NUNet *getNet() const;

  //! Return the number index expressions this composite has
  virtual UInt32 getNumDims() const;

  //! Get the Nth range stored in this object
  virtual const ConstantRange *getRange( const UInt32 index ) const;

  //! Get the size of the base object of this composite.
  virtual UInt32 getCompositeSize() const;

  //! Construct LHS matching RHS
  NULvalue* Lvalue(const SourceLocator &loc) const;

  virtual void compose( UtString* buf, const STBranchNode* scope, bool showRoot );

  //! Return the field index
  UInt32 getFieldIndex() const { return mIndex; }

  //! Set the field index
  void setFieldIndex( UInt32 index ) { mIndex = index; }

  //! Return the size this expression "naturally" is.
  UInt32 determineBitSizeHelper() const;

  //! Resize this expression to be the given size.
  void resizeHelper(UInt32 size);

  //! How many bits of effective value are there?
  virtual UInt32 effectiveBitSizeHelper(ExprIntMap* ebit_cache) const;

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);

  //! Dump myself to cout
  virtual void print(bool recurse, int ident) const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name upto and including specified scope,
           if null (or not a parent) then only the name of this NUNet is created
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
  */
  virtual void composeHelper(UtString*, const STBranchNode* scope, bool includeRoot,
                             bool hierName, const char* separator ) const;

  virtual void calcCost(NUCost*, NUCostContext*) const;

  virtual ptrdiff_t compareHelper(const NUExpr* expr, bool compareLocator,
                                  bool comparePointer, bool compareSizeOfConsts) const
  {
    const NUCompositeFieldExpr* fe = dynamic_cast<const NUCompositeFieldExpr*>(expr);
    NU_ASSERT(fe, expr);
    ptrdiff_t cmp = (ptrdiff_t)mIndex - (ptrdiff_t)fe->mIndex;
    if (cmp == 0) {
      cmp = mComposite->compare(*(fe->mComposite), compareLocator, comparePointer,
                                compareSizeOfConsts);
    }
    return cmp;
  }
  virtual BDD bdd(BDDContext* ctx, STBranchNode*) const;
  virtual size_t hash() const;
  virtual size_t shallowHash() const;

protected:
  virtual NUExpr* copyHelper(CopyContext&) const;
  virtual bool shallowEqHelper (const NUExpr& otherExpr) const;

private:
  //! Hide copy and assign constructors.
  NUCompositeFieldExpr(const NUCompositeFieldExpr&);
  NUCompositeFieldExpr& operator=(const NUCompositeFieldExpr&);

  /*! The object having a field chosen from.  This object is expected to
   * evaluate to a single, non-arrayed record.
   */
  NUCompositeInterfaceExpr *mComposite;

  //! The index of the field being selected
  UInt32 mIndex;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCompositeFieldExpr: public NUCompositeInterfaceExpr


class NUCompositeIdentRvalue : public NUCompositeInterfaceExpr
{
public:
  NUCompositeIdentRvalue( NUCompositeNet *net, const SourceLocator &loc );
  virtual ~NUCompositeIdentRvalue();

  //! Function to return the expression type
  virtual Type getType() const { return NUExpr::eNUCompositeIdentRvalue; }

  //! Return the identifier
  NUCompositeNet* getIdent() const { return mNet; }

  // virtual functions
  const char *typeStr() const { return "NUCompositeIdentRvalue"; }

  virtual UInt32 determineBitSizeHelper() const;
  virtual void resizeHelper( UInt32 size );
  virtual void composeHelper(UtString*, const STBranchNode*, bool, bool, const char*) const;
  virtual void print(bool recurse, int ident) const;
  virtual bool replaceLeaves( NuToNuFn & translator );
  virtual bool isWholeIdentifier() const { return true; }
  virtual NUNet* getWholeIdentifier() const { return (NUNet*)mNet; }
  //! Add the set of nets this construct uses to the given set.
  virtual void getUses(NUNetSet *uses) const;
  virtual void getUses(NUNetRefSet *uses) const;

  virtual CompositeType getCompositeType() const { return eCompositeIdentRvalue; }

  /*! \brief Return the number of fields in the composite object
   */
  virtual UInt32 getNumFields () const;

  //! Return the Nth field net pointer
  virtual NUNet *getField( UInt32 fieldIndex ) const;

  //! Get the underlying NUComposite expression for this object
  virtual NUCompositeInterfaceExpr *getIdentExpr() const;

  //! Get the number of ranges stored in this object
  virtual UInt32 getNumDims() const;

  //! Get the Nth range stored in this object
  virtual const ConstantRange *getRange( const UInt32 index ) const;

  //! Get the size of the base object of this composite.
  virtual UInt32 getCompositeSize() const;

  /* \brief get the NUNet corresponding to this composite expression
   *
   * This routine must be used with caution; what it actually returns is
   * NOT the precise net that will be referenced.  That net cannot be
   * determined, and will not even exist, until during post-population
   * resynthesis.  Hovever, there are several cases where it is very
   * useful to know what kind of net, at least whether it is a
   * composite/noncomposite net, a composite structure refers to.  In
   * particular, we need this information during population of a
   * bit/part select of a vector inside a record.
   */
  virtual NUNet *getNet() const;

  //! Construct LHS matching RHS
  NULvalue* Lvalue(const SourceLocator &loc) const;

  virtual ptrdiff_t compareHelper(const NUExpr* expr, bool, bool, bool) const
  {
    const NUCompositeIdentRvalue* ir = dynamic_cast<const NUCompositeIdentRvalue*>(expr);
    NU_ASSERT(ir, expr);
    return NUNet::compare(mNet, ir->mNet);
  }

  //! shallow-hash an expression -- used for NUExprFactory
  virtual size_t shallowHash() const;

protected:
  virtual NUExpr* copyHelper( CopyContext &copy_context ) const;

private:
  //! Hide copy and assign constructors.
  NUCompositeIdentRvalue(const NUCompositeIdentRvalue&);
  NUCompositeIdentRvalue& operator=(const NUCompositeIdentRvalue&);

  NUCompositeNet *mNet;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCompositeIdentRvalue : public NUCompositeInterfaceExpr
#endif

