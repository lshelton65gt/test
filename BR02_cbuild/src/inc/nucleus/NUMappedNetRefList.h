//! -*-C++-*-

/***************************************************************************************
  Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef NUMAPPEDNETREFLIST_H_
#define NUMAPPEDNETREFLIST_H_

#include "nucleus/NUNetRef.h"

class DisjointRange;

//! List of NUNetRef
/*!
 * We use a list to enforce ordering, prevent NUNetRef collapsing
 * (as in NUNetRefSet), and to allow inserts/removals throughout.
 */
typedef UtList<NUNetRefHdl> NUNetRefList;

//! Loop over NUNetRefList; used by interface to formal/actual lists.
typedef CLoop<NUNetRefList> NUNetRefListLoop;

//! A list of iterators used for collecting overlapping netrefs
typedef UtList<NUNetRefList::iterator> NUNetRefListIterList;

//! Maintain an NUNetRefList with maps to arbitrary subranges
/*
 *! This structure is used wtih

 *! This supercedes earlier use of a list to enforce ordering, prevent
 *! NUNetRef collapsing (as in NUNetRefSet), and to allow
 *! inserts/removals throughout, while still offering speed (log n)
 */
class NUMappedNetRefList {
public:
  CARBONMEM_OVERRIDES

  //! helper function for keying off a ConstantRange for an STL set
  struct CmpRanges {
    bool operator()(const ConstantRange& p, const ConstantRange& q) const {
      SInt32 cmp = p.getLsb() - q.getLsb();
      if (cmp == 0) {
        cmp = p.getMsb() - q.getMsb();
      }
      return cmp < 0;
    }
  };

  // We need to keep track of list iterators pointing to cells with
  // NUNetRefHdls, and the same NUNetRefHdl may appear more than
  // once in the list.  This is basically a multi-map.
  
  // I haven't decided yet whether it's better to represent these
  // multi-maps as UtMultiMap<ConstantRange,NUNetRefList::iterator>,
  // or as UtMap<ConstantRange,UtHashSet<NUNetRefList::iterator> >.
  //
  // The problem with the MultiMap is that we need a linear search
  // to erase a specific NUNetRefList::iterator, so I'd prefer
  // to use a map to a HashSet.  Unfortunately, I don't know how
  // to portably hash a NUNetRefList::iterator so that I get a
  // distinct hash value for multiple cells of the same NUNetRefHdl.
  // list::iterator does not have a hash() method to my knowledge.
  //
  // This means that ultimately the hash-table would wind up doing
  // a linear search as well.  So for now I am going to use the UtMultiMap
  // approach.  If we figure out how to write a hash function for
  // list::iterator (e.g. find the cell pointer) then it would be
  // faster to switch to the hash.
  //
  // I have tested both settings for NU_USE_HASH_TO_SETS and they
  // both work fine.

# define NU_USE_HASH_TO_SETS 0
# if NU_USE_HASH_TO_SETS
  struct HashListIter {
    //! hash operator -- requires class method hash()
    size_t hash(const NUNetRefList::iterator& iter) const {
      NUNetRefHdl hdl = *iter;
      return hdl->hash();
    }

    //! equal function
    bool equal(const NUNetRefList::iterator& i1,
               const NUNetRefList::iterator& i2)
      const
    {
      INFO_ASSERT(*i1 == *i2, "list-hash unexpected different values");
      bool ret = i1 == i2;
      return ret;
    }
  };

  typedef UtHashSet<NUNetRefList::iterator,HashListIter> ListIterSet;
  typedef UtMap<ConstantRange, ListIterSet,
                CmpRanges> RangeIterMap;
#else
  typedef UtMultiMap<ConstantRange, NUNetRefList::iterator,
                     CmpRanges> RangeIterMap;
#endif

  typedef std::pair<DisjointRange, RangeIterMap> NetInfo;
  typedef UtHashMap<NUNet*,NetInfo> NetInfoMap;

  //! ctor
  NUMappedNetRefList(NUNetRefFactory* nrf) :
    mNetRefFactory(nrf),
    mNumBits(0)
  {}

  //! copy ctor
  NUMappedNetRefList(const NUMappedNetRefList&);

  //! dtor
  ~NUMappedNetRefList() {}

  //! push back a new element onto the list, but maintain ConstantRange map
  /*!
   *! The net-ref passed in must have a contiguous range.
   */
  void push_back(const NUNetRefHdl& hdl) {insert(end(), hdl);}

  //! insert an element at a specified position in the list
  /*!
   *! The net-ref passed in must have a contiguous range.
   */
  NUNetRefList::iterator insert(NUNetRefList::iterator pos,
                                const NUNetRefHdl& hdl);

  //! erase an element in the list
  NUNetRefList::iterator erase(NUNetRefList::iterator);

  //! quickly find all the elements in the list that overlap the specified reference
  /*!
   *! This routine takes into account the net referenced by 'ref', and finds
   *! all the elements in the list that overlap ref's range, with the same net.
   *!
   *! The net-ref passed in must have a contiguous range.
   */
  void findOverlap(const NUNetRefHdl& ref, NUNetRefListIterList* iter_list);

  //! Insert all the elements in the iteration range
  template<class Iter> void insert(NUNetRefList::iterator pos, Iter start, Iter finish) {
    for (; start != finish; ++start) {
      insert(pos, *start);
    }
  }

  //! Loop over the entire netref list
  NUNetRefListLoop loopAllRefs() const {return NUNetRefListLoop(mList);}

  //! return the size of the list
  UInt32 size() const {return mList.size();}

  //! return if the list is empty
  bool empty() const {return mList.empty();}

  //! return the total number of bits (multi-counting overlaps)
  UInt32 numBits() const {return mNumBits;}

  //! iterate through the list
  NUNetRefList::iterator begin() {return mList.begin();}
  NUNetRefList::iterator end() {return mList.end();}
  NUNetRefList::const_iterator begin() const {return mList.begin();}
  NUNetRefList::const_iterator end() const {return mList.end();}

  //! debug print function
  void print();

  //! clear all entries in list (and map)
  void clear();

private:
  NetInfoMap mNetInfoMap;
  NUNetRefList mList;
  NUNetRefFactory* mNetRefFactory;
  UInt32 mNumBits;
};

typedef UtHashMap<NUNet*,NUMappedNetRefList*> NUNetToNetRefListMap;

#endif // NUMAPPEDNETREFLIST_H_
