// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUTFWALKER_H_
#define NUTFWALKER_H_

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUAssign.h"

//! Callback to traverse tasks and functions.
/*!
 * Find all the out-of-scope uses and defs within the task/function.
 * Keep them in a set, also remember whether a task must be inlined
 * (must be inlined when there is a non-blocking assign to an out-of-scope net).
 */
class NUTFWalkerCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  //! Constructor
  NUTFWalkerCallback() :
    mMustInline(false),
    mHasTaskHierRefs(false),
    mWalker(NULL)
  {}

  //! Destructor
  ~NUTFWalkerCallback() {}

  //! Set our walker; allows restarting walks when we encounter nested task calls.
  void setWalker(NUDesignWalker * walker) { 
    INFO_ASSERT(mWalker==NULL, "Restarting active design walker");
    mWalker = walker;
  }

  //! By default, walk through everything.
  Status operator() (Phase , NUBase * )  { return eNormal; }

  Status operator() (Phase phase, NUNonBlockingAssign *assign)
  {
    if (phase == ePre) {
      NUNetSet defs;
      assign->getNonBlockingDefs(&defs);

      NUNetSet non_local_defs;

      NUScope * scope = mScopes.top();
      scope->filterNonLocals(&defs, &non_local_defs);

      if (not non_local_defs.empty()) {
	mMustInline = true;
      }
    }
    return eNormal;
  }

  Status operator() (Phase phase, NUTaskEnable *enable)
  {
    if (phase == ePre) {
      if (enable->isHierRef()) {
        mHasTaskHierRefs = true;
      } else {
        // Walk all non-hierref tasks; 
        // If a called task has non-local references, we have non-local references.
        // If a called task has hier-ref task calls, we have hier-ref task calls.
        NUTask * task = enable->getTask();
        if (mWalker != NULL) {
          // TFPromote does not want to recurse into task bodies from
          // task enables, because it is using NUTFWalker to discover rewrite
          // requirements when moving tasks from one module to another,
          // rather than to discover usedef information for a task-enable
          // that references a task that makes non-local references
          mWalker->task(task);
        }
      }
    }
    return eNormal;
  }

  Status operator() (Phase phase, NUTask * task) {
    if (phase==ePre) {
      mScopes.push(task); // record our descent.
    } else {
      mScopes.pop();
    }
    return eNormal;
  }

  Status operator() (Phase /* phase */, NUExpr *expr)
  {
    NUNetSet uses;
    expr->getUses(&uses);

    NUScope * scope = mScopes.top();
    scope->filterNonLocals(&uses, &mNonLocalUses);

    // No need to traverse into the expression.
    return eSkip;
  }

  Status operator() (Phase /* phase */, NULvalue *lvalue)
  {
    NUNetSet uses;
    lvalue->getUses(&uses);

    NUNetSet defs;
    lvalue->getDefs(&defs);

    NUScope * scope = mScopes.top();
    scope->filterNonLocals(&uses, &mNonLocalUses);
    scope->filterNonLocals(&defs, &mNonLocalDefs);

    // No need to traverse into the lvalue
    return eSkip;
  }

  NUNetSet mNonLocalUses;
  NUNetSet mNonLocalDefs;

  //! Must this be inlined?
  bool mMustInline;

  //! Does this have hierarchical task enables in it?
  bool mHasTaskHierRefs;

private:
  //! Stack of tasks we are traversing.
  NUScopeStack mScopes;

  //! Our walker; used to begin walks from task enables.
  NUDesignWalker * mWalker;
};

//! Callback to determine if any task enables are called from some Nucleus tree. 
/*!
 * Note that this callback will recurse through enabled tasks.
 */
class NUHierRefTaskDiscoveryCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  //! Constructor
  NUHierRefTaskDiscoveryCallback() :
    NUDesignCallback(),
    mWalker(NULL)
  {}

  //! Destructor
  ~NUHierRefTaskDiscoveryCallback() {}

  //! Set our walker.
  /*!
   * Setting the walker allows single-walking over tasks, since the
   * walker will not repeatedly walk over a task.
   */
  void setWalker(NUDesignWalker * walker) {
    INFO_ASSERT(mWalker==NULL, "restarting active design walker");
    mWalker = walker;
  }

  Status operator()(Phase /*phase*/, NUBase * /*node*/) { return eNormal; }
  Status operator()(Phase /*phase*/, NUExpr * /*node*/) { return eSkip; }
  Status operator()(Phase /*phase*/, NULvalue * /*node*/) { return eSkip; }
  Status operator()(Phase phase, NUTaskEnable * enable) {
    if (phase != ePre) {
      return eNormal;
    }

    if (enable->isHierRef()) {
      return eStop;
    }
    INFO_ASSERT(mWalker, "walking design without a callback");
    return mWalker->task(enable->getTask());
  }
private:
  //! The walker.
  NUDesignWalker * mWalker;
};

#endif
