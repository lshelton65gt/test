// -*-C++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __GCCXMLDISTILLMACROS_H__
#define __GCCXMLDISTILLMACROS_H__

//This file defines various macros used to attach gccxml attributes to classes.


#ifdef __GCCXML__
//These macros are active only when gccxml is run.



#define APIDISTILLCLASS                          __attribute((gccxml("distill")))
#define APIOMITTEDCLASS                          __attribute((gccxml("omitted")))
#define APIDISTILLFIELD                          __attribute((gccxml("distill")))
#define APIOMITTEDFIELD                          __attribute((gccxml("omitted")))

#define APIEMITCODE(_a_)                           "$emitCode(" #_a_ ")"
#define APIEMITINIT(_a_)                           "$emitInit(" #_a_ ")"
#define APIEMITTYPE(_a_)                           "$emitType(" #_a_ ")"
#define APIEMITBIND(_a_)                           "$bindField(" #_a_ ")"
#define APIEMITPURE(  _)                           "$emitPure("")"
#define APICONSTRUCTORFALSE                        "$constructorFalse()"


#define APIDISTILLMACRO1(_a_)                          __attribute((gccxml(_a_ )))
#define APIDISTILLMACRO2(_a_,_b_)                      __attribute((gccxml(_a_,_b_ )))
#define APIDISTILLMACRO3(_a_,_b_,_c_)                  __attribute((gccxml(_a_,_b_,_c_ )))
#define APIDISTILLMACRO4(_a_,_b_,_c_,_d_)              __attribute((gccxml(_a_,_b_,_c_,_d_ )))
#define APIDISTILLMACRO5(_a_,_b_,_c_,_d_,_e_)          __attribute((gccxml(_a_,_b_,_c_,_d_,_e_ )))
#define APIDISTILLMACRO6(_a_,_b_,_c_,_d_,_e_,_f_)      __attribute((gccxml(_a_,_b_,_c_,_d_,_e_ ,_f_)))
#define APIDISTILLMACRO7(_a_,_b_,_c_,_d_,_e_,_f_,_g_)  __attribute((gccxml(_a_,_b_,_c_,_d_,_e_ ,_f_,_g_)))







#else

//The above macros are defined as noop.



#define APIDISTILLCLASS
#define APIOMITTEDCLASS
#define APIDISTILLFIELD
#define APIOMITTEDFIELD

#define APIEMITCODE(_a_)
#define APIEMITINIT(_a_)
#define APIEMITTYPE(_a_)
#define APIEMITBIND(_a_)
#define APIEMITPURE(   )
#define APICONSTRUCTORFALSE

#define APIDISTILLMACRO1(_a_)
#define APIDISTILLMACRO2(_a_,_b_)
#define APIDISTILLMACRO3(_a_,_b_,_c_)
#define APIDISTILLMACRO4(_a_,_b_,_c_,_d_)
#define APIDISTILLMACRO5(_a_,_b_,_c_,_d_,_e_)










#endif

#endif
