// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _NUCMODELINTERFACE_H_
#define _NUCMODELINTERFACE_H_

#include "util/SourceLocator.h"
#include "nucleus/NUCModelFn.h"
#include "nucleus/NUCModelPort.h"

//! NUCModelInterface class
/*! There is a single instance of this class for each CModel defined
    in the directives file.  All instances of the CModel in the design
    share the NUCModelInterface, which tracks the formal parameters.
 */
class NUCModelInterface : public NUBase
{
private:
  //! Filter to skip the NULL port for iteration
  class NullPortFilter
  {
  public:
    //! constructor
    NullPortFilter() {};

    //! Comparison routine to see if we should visit this port
    bool operator()(NUCModelPort* port) const;
  };

  // Structure to compare two strings so we don't insert duplicate contexts
  struct CompareStrings
  {
    bool operator()(const UtString* s1, const UtString* s2) const;
  };

public:
  //! constructor
  NUCModelInterface(const SourceLocator&, StringAtom*, bool isCallOnPlayback);

  //! destructor
  ~NUCModelInterface();

  //! Add a c-model function
  void addFunction(NUCModelFn* fn);

  //! Iterate over the module's cModels
  NUCModelFnCLoop loopFunctions() const;

  //! Iterate over the module's cModelFns
  NUCModelFnLoop loopFunctions();

  //! Add a port (should be added in order
  void addPort(NUCModelPort* port);

  //! Iterator for the ports
  typedef Loop<NUCModelPortVector> PortsLoop;

  //! Iterator for the ports
  //  typedef Loop<const Ports, Ports::const_iterator> PortsCLoop;
  typedef CLoop<NUCModelPortVector> PortsCLoop;

  //! Iterator over the ports
  PortsLoop loopPorts();

  //! Iterator over the ports
  PortsCLoop loopPorts() const;

  //! Get the number of ports defined so far
  UInt32 numPorts() const;

  //! Iterator for ports except the null port
  typedef LoopFilter<PortsLoop, NullPortFilter> PortsLoopNoNull;

  //! Iterator for ports except the null port
  typedef LoopFilter<PortsCLoop, NullPortFilter> PortsCLoopNoNull;

  //! Iterator over the ports but skip the null port
  PortsLoopNoNull loopPortsNoNull();

  //! Iterator over the ports but skip the null port
  PortsCLoopNoNull loopPortsNoNull() const;

  //! Get a port by name
  NUCModelPort* findPort(const UtString* name) const;

  //! Returns true if there are context strings
  bool hasContextStrings() const;

  //! Abstraction for the context strings of this c-models
  typedef UtSet<const UtString*, CompareStrings> ContextStrings;

  //! Loop for the context strings of this c-model
  typedef Loop<ContextStrings> ContextStringsLoop;

  //! Constant iterator for the context strings
  typedef ContextStrings::const_iterator ContextStringsCIter;

  //! Loop for the context strings of this c-model
  typedef CLoop<ContextStrings> ContextStringsCLoop;

  //! Iterate over the context strings for the various c-model calls
  ContextStringsCLoop loopContextStrings() const;

  //! Iterate over the context strings for the various c-model calls
  ContextStringsLoop loopContextStrings();

  //! debug help: return the type of what I'm looking at
  virtual const char* typeStr() const { return "NUCModelInterface"; }

  //! If this has a name, return it, else return NULL.
  StringAtom* getName() const { return mName; }

  //! Get the next variant index
  UInt32 nextVariant() { return mNumVariants++; }

  //!Code Generator
  /*!Generate equivalent C++ code for the verilog object.
   * \returns CGContext_t providing information about code generated.
   */
  CGContext_t emitCode(CGContext_t context) const;

  //! Get the location for this c-model
  const SourceLocator& getLoc() const { return mLoc; }

  //! Make a new unique name from this c-model
  UtString* makeName();

  //! Check if this cmodel has the null port
  bool hasNullPort() const;

  //! Get the null port (or NULL if not found)
  NUCModelPort *getNullPort() const;

  //! Is this a c-model that should be called during replay playback
  bool isCallOnPlayback() const { return mIsCallOnPlayback; }

private:
  // Hide copy and assign constructors.
  NUCModelInterface(const NUCModelInterface&);
  NUCModelInterface& operator=(const NUCModelInterface&);

  // Need to add parameter information and access functions above

  // The list of c-model functions
  NUCModelFnVector* mCModelFnVector;

  // The list of c-model context strings
  ContextStrings* mContextStrings;

  // The list of output and input ports in the order defined by the user
  NUCModelPortVector* mPorts;

  // A list of ports sorted by name so that we can find them
  struct ComparePorts
  {
    bool operator()(const UtString* s1, const UtString* s2) const;
  };
  typedef UtMap<const UtString*, NUCModelPort*, ComparePorts> SortedPorts;
  SortedPorts* mSortedPorts APIOMITTEDFIELD;

  // Local data for this function
  SourceLocator mLoc;
  StringAtom* mName;
  UInt32 mNameIndex;
  UInt32 mNumVariants;
  bool mIsCallOnPlayback; // true if the c-model should be called during playback

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUCModelInterface : public NUBase

#endif // _NUCMODELINTERFACE_H_
