// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUSCOPE_H_
#define NUSCOPE_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUElabBase.h"
#include "util/SourceLocator.h"
#include "util/LoopPair.h"
#include "nucleus/GCCXMLDistillMacros.h"
#include "nucleus/NucleusClassFriendsMacros.h"


/*!
  \file
  Declaration of the Nucleus scope class.
*/

class AtomicCache;
class StringAtom;
class ConstantRange;
class STBranchNode;
class IODBNucleus;
class STAliasedLeafNode;

//! Helper class for NUNetSetByName
struct NUNetHashByName
{
  //! hash operator -- requires class method hash()
  size_t hash(const NUNet* var) const;

  //! equal function
  bool equal(const NUNet* v1, const NUNet* v2) const;

  //! shallow less-than function -- for Microsoft hash tables
  bool lessThan1(const NUNet* v1, const NUNet* v2) const;

  //! lessThan operator -- for sorted iterations
  bool lessThan(const NUNet* v1, const NUNet* v2) const;
}; // struct NUNetHashByName


//! NUScope class
/*!
 * Abstract interface for an unelaborated scope of some sort:
 *  module, task, function, named block
 */
class NUScope
{
public:
  //! Scope types
  enum ScopeT { 
    eBlock,
    eModule, 
    eTask, 
    eNamedDeclarationScope,
    eUserTask };

  //! constructor
  NUScope(IODBNucleus *iodb, HierFlags hierFlags) :
    mIODB(iodb),
    mSymNumMap(NULL),
    mScheduled (false),
    mHierFlags(hierFlags)
  {}

  //! Return the type of scoping unit
  virtual ScopeT getScopeType() const = 0;

  //! Return the type of scoping unit, as a string
  const char *getScopeTypeStr() const;

  //! Is this block named?
  virtual bool isNamed() { return true; }

  //! Return the name; valid to call only if isNamed() is true.
  virtual StringAtom *getName() const = 0;

  //! Return the original name of the module.
  /*!
    Only valid if isNamed() is true. Defaults to calling getName().
  */
  virtual StringAtom* getOriginalName() const;

  //! Return a printable name even if the scope isn't named.
  virtual const char * getPrintableName() const;

  //! Return the parent scoping unit, if any.  Modules never have a parent (will return 0).
  virtual NUScope* getParentScope() const = 0;

  /*! Return the HDL scope; only non-null if different from the Nucleus
   *  scope.  This is only non-null in a NUTF generated from VHDL.
   */
  virtual NUScope *getHdlParentScope() const { return NULL; }

  //! Return the module in which this scope resides.  Modules never have a parent (will return 0).
  NUModule* getParentModule() const;

  //! Return the module in which this scope resides.  If this scope is a module, it will return itself.
  NUModule* getModule() const;

  //! Return the task or function in which this scope resides.  If this scope is a TF, it will return itself.  If it is not in a TF scope, it will return NULL.
  NUTF* getTFScope() const;

  //! Return the NUBase object which this represents.
  virtual NUBase* getBase() = 0;
  virtual const NUBase* getBase() const = 0;

  //! \return the source location for this object
  /*! \note Some of the subclasses of NUScope also declare a getLoc method. The
   *  realisation of this method in those classes will just wrap the getLoc
   *  call. It is usefull when writing the GUI database to have a method that
   *  provides the source location for scope objects without having to probe
   *  the object for its type.
   */
  virtual const SourceLocator getSourceLocation () const = 0;

  //! Return true if this scope is a parent of the passed-in scope, false otherwise.
  bool isParentOf(NUScope *scope) const;

  //! Return true if this scope is inside a task or function, or is a task or function
  bool inTFHier() const;

  //! Return true if this named block should be emitted in the symbol table, false if not.
  virtual bool emitInSymtab() const { return true; }

  //! For objects declared locally, return an index for them to be lookup-up by, which
  //! will be consistent across different instantiations of this scope.
  virtual SInt32 reserveSymtabIndex()  APIDISTILLMACRO1(APIEMITPURE()) = 0;

  //! Return my index in the symbol table
  virtual SInt32 getSymtabIndex() const = 0;

  //! Add the given block as a child
  virtual void addBlock(NUBlock * block) = 0;

  //! Remove the given block from being a child
  virtual void removeBlock(NUBlock * block) = 0;

  //! Add the given declaration scope as a child
  virtual void addDeclarationScope(NUNamedDeclarationScope *scope) = 0;

  //! Remove the given declaration scope from being a child
  virtual void removeDeclarationScope(NUNamedDeclarationScope *scope) = 0;

  //! Loop over sub-blocks
  virtual NUBlockCLoop loopBlocks() const = 0;

  //! Loop over sub-blocks
  virtual NUBlockLoop loopBlocks() = 0;

  //! Loop over sub-declaration scopes
  virtual NUNamedDeclarationScopeCLoop loopDeclarationScopes() const = 0;

  //! Loop over sub-declaration scopes
  virtual NUNamedDeclarationScopeLoop loopDeclarationScopes() = 0;

  //! Add the net to the locally-declared nets.
  //! \param net The net to add
  virtual void addLocal(NUNet *net) = 0;
  
  //! Remove the net from the set of locally-declared nets.
  virtual void removeLocal(NUNet *net, bool remove_references=true) = 0;

  //! Remove the nets from the set of locally-declared nets.
  virtual void removeLocals(const NUNetSet& net, bool remove_references=true) = 0;

  //! Remove a net from this scope's set of locally-defined nets.
  /*!
   * \param net  The net to remove.
   *
   * When removing the net from the locals list, do not clear from the
   * NetRef factory references.
   *
   * Calling this method is equivalent to calling:
   \code
     removeLocal(net,false);
   \endcode
   *
   * \sa removeLocal
   */
  virtual void disconnectLocal(NUNet *net);

  //! Loop over all local nets
  virtual NUNetCLoop loopLocals() const = 0;

  //! Loop over all local nets
  virtual NUNetLoop loopLocals() = 0;

  //! Return contained non-tf nets.
  virtual void getAllNonTFNets(NUNetList *net_list) const = 0;

  //! Add all nets local to this scope to the given list, in no particular order.
  /*!
   * Nets declared local to sub blocks will be added also; this list
   * has all nets declared within this scope.
   */
  virtual void getAllNets(NUNetList *net_list) const = 0;

  //! Create a temporary net scoped locally preserving sizing and
  //! other information from a source net.
  /*!
   * \param duplicate_memories If true, a new NUMemoryNet is created.
   *        Otherwise, an NUTempMemoryNet is created with the original
   *        net as * its master.
   * \param suppressGenSym if true, suppresses GenSym on name creation,
   *        if false, GenSym is used for name creation.
   */
  NUNet * createTempNetFromImage(NUNet * original,
                                 const char* prefix,
                                 bool duplicate_memories=false,
				 bool suppressGenSym = false);

  //! Create a subrange temporary net scoped locally
  /*! Create a temp net which is a subrange of the original net. This
   *  function preserves the flags.
   *
   *  Note that this function currently only supports vector nets. If
   *  it is needed for other uses, it should be extended.
   */
  NUNet* createTempSubRangeNetFromImage(NUNet* original,
                                        UInt32 bit_size,
                                        const char* prefix);

  //! Create a temporary net scoped locally, with a generated name unique
  //! to this scope.
  /*!
   * If bit_size is 1, will create a bit net, otherwise will create a vector net
   * with range [bit_size-1:0].  bit_size must be at least 1.
   */
  NUNet* createTempNet(StringAtom* name, UInt32 bit_size, bool isSigned,
                       const SourceLocator& loc, StringAtom* origName=NULL);

  //! Create a temporary bit net scoped locally, with a generated name unique
  //! to this scope.
  NUNet* createTempBitNet(StringAtom* name, bool is_signed,
			  const SourceLocator& loc, StringAtom* origName=NULL);

  //! Create a temporary vector net scoped locally, with a generated name unique
  //! to this scope.
  NUNet* createTempVectorNet(StringAtom* name,
			     const ConstantRange& range,
                             bool isSigned,
			     const SourceLocator& loc,
			     NetFlags flags=eNoneNet,
                             VectorNetFlags vflags=eNoneVectorNet,
                             StringAtom* origName=NULL);

  //! Create a temporary memory net scoped locally, with a generated name unique
  //! to this scope.
  NUTempMemoryNet* createTempMemoryNet(StringAtom* name, 
                                       NUMemoryNet* master,
                                       bool isDynamic,
                                       NetFlags flags=eNoneNet,
                                       StringAtom* origName=NULL);

  //! Create a memory net in the image of another memory.
  /*! The temp memory is not automatically initialized from the master
   *  memory. It only uses the master memory to get the width, range,
   *  and VectorNet flags for the temp image memory.
   */
  NUMemoryNet* createTempMemoryNetFromImage(StringAtom *name,
                                            NUMemoryNet* master,
                                            NetFlags flags);

  //! Create a composite net in the image of another composite..
  /*! The temp net is not automatically initialized from the master
   *  net. It only uses the master composite to get the shape/structure
   *  for the temp composite net image.
   *
   *  NOTE: use createTempNetFromImage do not use this routine.
   *  This routine is used only by the Interra flow which is going away, and it is not being fixed.
   */
  NUCompositeNet* createTempCompositeNetFromImageForInterraFlowUseOnly(StringAtom *name,
                                                                       NUCompositeNet* master,
                                                                       NetFlags flags,
                                                                       NUNetRefFactory *netRefFactory);

  //! Create a unique block name internal to this block (for unnamed blocks)
  // ACA: this should be removed
  StringAtom* newBlockName(AtomicCache* string_cache,
                           const SourceLocator& loc);

  //! Generate a symbol for a new net or module.  
  /*! \param prefix specifies a descripted common prefix
   *         (e.g. "flatten", "rescope")
   *  \param name specifies an instance-specific name -- if NULL
   *         then one will be generated
   *  \param proxy specifies a nucleus object that this new name is
   *         used in place of.  If simple, gensym may compose this name as
   *         text and use it to make a descriptive name
   *  \param isAlreadyUnique specifies whether the caller guarantees 
   *         that the combination of prefix and name are unique within
   *         the scope.  If false, then a ;num will be appended for
   *         some "num" that is chosen by this routine to guarantee
   *         uniqueness.
   */
  virtual StringAtom* gensym(const char* prefix,
                             const char* name = NULL,
                             const NUBase* proxy = NULL,
                             bool isAlreadyUnique = false);

  //! Generate a unique name based on a list of strings
  /*! \param prefix specifies are descriptive prefix for the name
   *  \param names list of names that are massaged into a new name
   *  \param name distinguishing string if a name cannot be made from names
   */
  StringAtom *gensymFromList (const char *prefix,
    UtList <const char *> names,
    const char *name = NULL);
    
  //! Generate a symbol for a new hierarchical reference.
  /*!
   * Transform a hierarchical name "name" into "$PREFIX_name_POSTFIX".
   *
   * This hierarchical-reference version of gensym does not offer the
   * flexibility nor the uniqueness checks found in NUScope::gensym.
   */
  virtual StringAtom * gensymHierRef(const char * prefix,
                                     const char * leaf_name,
                                     const char * postfix);

  //! Was sym created by gensym()?
  static bool isGensym(StringAtom* sym);

  //! Find the best symbol in a name-ring
  /*! The best symbol is
   *!    1. not a gensym
   *!    2. shallowest in depth
   *!    3. lexically earliest
   *! If all the aliases are genyms, then it returns the one passed in.
   *!
   *! This is intended to be used by routines that want to pick
   *! the best node for printing, or for master-name selection.
   */
  static STAliasedLeafNode* findBestAlias(STAliasedLeafNode*);

  //! return this scope elaborated at the given hierarchy
  virtual NUScopeElab* lookupElabScope(const STBranchNode *hier) const;

  //! Return true if this scope is at the top-level
  virtual bool atTopLevel() const = 0;

  //! Dump unelaborated flow of all nets to cout
  void printFlow(int indent) const;

  static int compare(const NUScope* s1, const NUScope* s2);

  //! Determine the depth of a symbol table node.
  /*!
    Hierarchy elements not corresponding to modules do not contribute
    to this depth calculation.

    Must be an elaborated symbol table node, as the BOM is analyzed.
   */
  static SInt32 determineModuleDepth(STSymbolTableNode * node);
  
  //! Add any non locals to the output set.
  virtual void filterNonLocals(const NUNetSet * in, NUNetSet * out);

  //! destructor
  virtual ~NUScope();

  //! get iodb
  IODBNucleus* getIODB() const {return mIODB;}

#ifdef CDB
  bool hasNetName(StringAtom* sym) const;
#endif

  //! declare an NUNet* inside the parent scope.  Do not allow two nets
  //! of the same name in the same module, even in different scopes
  void addNetName(NUNet* net) APIDISTILLMACRO2(APIEMITCODE(StringAtom* name = net->getName()),
                                               APIEMITCODE(mNetHash.insert(name)));

  //! remove the declaration of an NUNet* inside the parent module.
  void removeNetName(NUNet*);

  //! Return the net flags to use when creating a locally-scoped temporary
  virtual NetFlags getTempFlags() const = 0;

  //! Mark this block as being scheduled at lesat once
  void putScheduled (bool);

  //! Has this block been added to some schedule?
  bool isScheduled (void) const;

  //! Get the hierarchical flags
  HierFlags getHierFlags() const { return mHierFlags; }

  //! fix up the IODB (needed for PV's multi-design mutations)
  void putIODB(IODBNucleus* iodb) {mIODB = iodb;}


protected:
  //! helper function for NUBlock & NUModule to implement removeLocals
  void removeNetsHelper(const NUNetSet& nets, bool remove_references,
                        NUNetList* net_list);

  //! Pointer to the IODB
  /*!
    This allows scopes to add the intrinsic type
    information of their nets to the type dictionary
  */
  IODBNucleus *mIODB;

  //! Hide default, copy and assign constructors.
protected:
  NUScope();
private:
  NUScope(const NUScope&);
  NUScope& operator=(const NUScope&);

  //! Helper function to create a new temporaries flags from an original net
  NetFlags createTempNetFlags(NUNet* original);

  //! Get unique name for names inside this module-level scope
  /*! Given a string created by uniquify, remove the unique prefix
   *! and suffix.  E.g.
   *!    uniquify("foo", "bar") --> "$foo_bar;1"
   *!    stripUniquify(buf, "$foo_bar;1") --> "foo_bar"
   *! parameter 'buf' is appended to without clearing it.
   */
  static void stripUniquify(UtString* buf, const char* uniquifiedString);

  //! string-to-number map, used to generate unique names.
  typedef UtHashMap<StringAtom*,UInt32> SymNumMap;
  SymNumMap* mSymNumMap APIOMITTEDFIELD;

#ifdef CDB
  // Keep track of all nets in a module to avoid adding ones with clashing names
  AtomSet mNetHash APIDISTILLMACRO1(APICONSTRUCTORFALSE);
#endif
  //! Has this scope been scheduled or called from a scheduled flow node.
  bool mScheduled APIOMITTEDFIELD;

  // helper methods for gensymFromList

  enum {
    cMAX_NAME_LENGTH = 128              // maximum size of constructed name
  };

  //! Try to make a clever name if all the nets share a common prefix and/or suffix
  bool makeCleverName (const char *leader, const UtList <const char *> &, StringAtom **);

  //! Try to make a name by concating all the strings in the list
  bool makeLongestName (const char *leader, const UtList <const char *> &, StringAtom **);

  //! Try to make a name from the first and last strings in the list
  bool makeMediumName (const char *leader, const UtList <const char *> &, StringAtom **);

#if 0
  //! Strip extraneous $bumf from the name
  bool strip (const char *s, unsigned int *n) const;
#endif

  //! The source language for this module/architecture
  HierFlags mHierFlags;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUScope


//! NUBlockScope class
/*!
 * Abstract interface for a stmt-block scope:  function, task, block
 */
class NUBlockScope : public NUScope
{
public:
  //! constructor
  NUBlockScope(IODBNucleus *iodb, HierFlags hierFlags) :
    NUScope(iodb, hierFlags)
  {}

  //! Replace the current statement list with the given statement list.
  /*!
   * The given list is copied.
   * This is used to reorder non-blocking temporaries.
   * The caller is responsible to make sure that statements are not leaked.
   */
  virtual void replaceStmtList(const NUStmtList& new_stmts) = 0;

  //! Add the given statement to the end of the block
  virtual void addStmt(NUStmt *stmt) = 0;

  //! Add the given statements to the end of the block
  virtual void addStmts(NUStmtList *stmts) = 0;

  //! Add the given statement to the start of the block
  virtual void addStmtStart(NUStmt *stmt) = 0;

  //! Loop through all the statements
  virtual NUStmtLoop loopStmts() = 0;

  //! Loop through all the statements
  virtual NUStmtCLoop loopStmts() const = 0;

  //! Return the statements for the tf body.
  /*!
   * The list must be freed by the caller.
   */
  virtual NUStmtList * getStmts() const = 0;

  //! Loop over all local nets
  virtual NUNetCLoop loopLocals() const = 0;

  //! Loop over all local nets
  virtual NUNetLoop loopLocals() = 0;

  //! Reparent any subordinate named blocks into a newly inserted named block
  void adoptChildren (void);
  //! Reparent any subordinate named blocks in given statements into this block
  void adoptChildren (NUStmtList* stmts);
  //! destructor
  virtual ~NUBlockScope() {}


protected:
  //! Hide default, copy and assign constructors.
  NUBlockScope();
private:
  NUBlockScope(const NUBlockScope&);
  NUBlockScope& operator=(const NUBlockScope&);
  //! This logic block appears in one-or-more schedules

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUBlockScope : public NUScope


//! NUScopeElab class
/*!
 * Abstract base class to model an elaborated scope.
 */
class NUScopeElab : public NUElabBase
{
public:
  //! constructor
  NUScopeElab(NUScope *scope, STSymbolTableNode *node);

  //! Return the symbol table entry for this elaboration
  STBranchNode *getHier() const;

  //! Return unelaborated nucleus scope
  NUScope* getScope() const { return mScope; }
  
  //! Dump the scope's elaborated flow to UtIO::cout().
  virtual void printElab(STSymbolTable *symtab, bool verbose, int indent) const = 0;

  //! Dump myself to UtIO::cout().
  virtual void print(bool verbose, int indent) const = 0;

  //! Generate C++ code
  CGContext_t emitCode (CGContext_t) const;

  //! Lookup the NUScopeElab object for the given hierarchy branch node
  static NUScopeElab* lookupScope(STBranchNode *hier);

  //! Get the hierarchical flags
  HierFlags getHierFlags() const { return mScope->getHierFlags(); }

  //! destructor
  virtual ~NUScopeElab() {}


protected:
  //! Unelaborated scope
  NUScope *mScope;

private:
  //! Hide copy and assign constructors.
  NUScopeElab(const NUScopeElab&);
  NUScopeElab& operator=(const NUScopeElab&);
};

#endif
