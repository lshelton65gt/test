// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NUCOPYCONTEXT_H_
#define NUCOPYCONTEXT_H_

#include "nucleus/Nucleus.h"

class AtomicCache;

//! Class to keep context when copying statements.
class CopyContext
{
public:
  //! Constructor
  /*
    \param scope     The current scope for our copy (ie. named block,
                     function, module). If the copy passes through an
                     NUScope, a new CopyContext will be created using
                     that scope.

    \param str_cache The string cache.

    \param copy_scope_variables Should we create copies of any locally
                     scoped nets?

    \param prefix    Prefix to prepend to the original net name when
                     copying locally scoped variables.
  */
  CopyContext(NUScope* scope, AtomicCache* str_cache,
	      bool copy_scope_variables=false,
              const char * prefix=NULL) :
    mScope(scope),
    mStrCache(str_cache),
    mCopyScopeVariables(copy_scope_variables),
    mPrefix(prefix)
  {}

  //! Translates a net into its copy equivalent.
  /*
    \param net The net we are attempting to translate.

    If mCopyScopeVariables is false or if there is no available
    translation, the provided net is returned unmodified.
   */
  NUNet * lookup( NUNet * net ) {
    NUNet * replacement = net;
    if ( mCopyScopeVariables ) {
      NUNetReplacementMap::iterator location = mNetReplacements.find(net);
      if ( location != mNetReplacements.end() ) {
	replacement = location->second;
      }
    }
    return replacement;
  }

  //! Translates a block into its copy equivalent.
  /*
    \param block [in] The block we are attempting to translate.

    If there is no available translation, the provided block is returned
    unmodified.
   */
  NUBlock* lookup( NUBlock *block ) {
    NUBlock *replacement = block;
    NUBlockReplacementMap::iterator location = mBlockReplacements.find( block );
    if ( location != mBlockReplacements.end() ) {
      replacement = location->second;
    }
    return replacement;
  }

  //! Current scope for copy
  NUScope *mScope;
  
  //! String cache; used when creating new names.
  AtomicCache *mStrCache;

  //! Should we generate copies of locally scoped nets?
  bool mCopyScopeVariables;

  //! Prefix for any copied locally scoped nets.
  const char * mPrefix;

  //! How have we rewritten locally scoped nets?
  NUNetReplacementMap mNetReplacements;

  typedef UtMap<const NUBlock *, NUBlock *> NUBlockReplacementMap;
  //! How have we rewritten blocks that are NUBreak targets?
  NUBlockReplacementMap mBlockReplacements;
};

#endif
