// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _NUCMODELPORT_H_
#define _NUCMODELPORT_H_

#include "util/CarbonTypes.h"
#include "nucleus/NUBase.h"

//! class NUCModelPort
/*! This class mirrors the NUNet class.
 */
class NUCModelPort : public NUBase
{
public:
  //! constructor
  NUCModelPort(const char* name, bool isNullPort, PortDirectionT dir,
               UInt32 size, UInt32 index);

  //! destructor
  ~NUCModelPort();

  //! Get the string that represents this port
  const UtString* getName() const { return mName; }

  //! Get the direction for this port
  PortDirectionT getDirection() const { return mDirection; }

  //! Get the size of this port in the number of bits.
  UInt32 getBitSize(UInt32 variant) const;

  //! Get the size of this port in the number of UInt32 words
  UInt32 getSize(UInt32 variant) const;

  //! Get the maximum bit size for this port
  UInt32 getMaxBitSize() const;

  //! Get the maximum size (number of words) for this port
  UInt32 getMaxSize() const;

  //! Get the index into the c-model ports
  UInt32 getIndex() const { return mIndex; }

  //! Add a new variant size
  void addSize(UInt32 variant, UInt32 size);

  //! Compare function so that we can sort by port name
  int compare(const NUCModelPort* other) const;

  //! Check if this is a NULL port
  bool isNullPort() const { return mIsNullPort; }

  //! debug help: return the type of what I'm looking at
  virtual const char* typeStr() const { return "NUCModelPort"; }

  //!Code Generator
  /*!Generate equivalent C++ code for the verilog object.
   * \returns CGContext_t providing information about code generated.
   */
  CGContext_t emitCode(CGContext_t /*context */) const;

  //! \return iff the port is 2-dimensional
  /*! Memories are not permitted as CModel ports so this will always return
   * false.
   *
   * \note NUCModelPort is used as one of the type template parameters to
   * LowerPortConnection::call. This requires an is2DAnything method.
   */
  bool is2DAnything () const { return false; }

private:
  // The local data
  UtString* mName;
  bool mIsNullPort;
  PortDirectionT mDirection;
  typedef UtVector<UInt32> Sizes;
  Sizes* mSizes;
  UInt32 mIndex;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUCModelPort : public NUBase

#endif // _NUCMODELPORT_H_
