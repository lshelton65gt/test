// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NUNETCLOSURE_H_
#define NUNETCLOSURE_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUNetSet.h"
#include "util/SetClosure.h"

//! Closure class; performs the set closure operation over sets of NUNetSets.
typedef SetClosure<NUNet*,NUNetSet> NUNetClosure;

//! Closure class; performs the set closure operation over sets of NUNetElabSets.
typedef SetClosure<NUNetElab*,NUNetElabSet> NUNetElabClosure;


#endif
