// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004, 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NUELAB_HIER_HELPER_H_
#define NUELAB_HIER_HELPER_H_

class STSymbolTable;
class STBranchNode;
class STAliasedLeafNode;

class NUModule;
class NUScope;
class NUNet;

//! Interface class for elaboration helpers.
/*!
 * Encapsulate a number of methods which help translate from the
 * unelaborated Nucleus constructs and symbol table entries to entries
 * in the elaborated symbol table.
 */
class NUElabHierHelper
{
public:
  //! Recursively find an unelaborated path within a module to some nested scope.
  /*!
   * Example:
   \verbatim
     module top();
       always 
       begin :outer 
         begin :inner
         end
       end
     endmodule
   \endverbatim
   *
   * The call:
   * 
   \code
     findUnelabBranchForScope( NUModule(top), NUScope(inner) ) 
   \endcode
   *
   * will return a STBranchNode representing the relative hierarchy "outer.inner".
   *
   * \param module The containing module.
   * \param scope  A scope contained within 'module'.
   *
   * \returns Symbol table branch representing path to 'scope' within
   *     'module'. NULL is returned if the scope is not user-visible.
   */
  static STBranchNode * findUnelabBranchForScope(NUModule * module, NUScope * scope);

  //! Return the STBranchNode for a task rooted at the hierarchy of its containing module.
  /*!
   * Most tasks/functions will appear in the symbol table as immediate
   * children of the containing NUModule. This is not the case when
   * flattening occurs. Hierarchy for tasks/functions corresponds to
   * their declaration hierarchy; this hierarchy is stored until
   * elaboration in the unelaborated symbol table contained within
   * each NUModule.
   *
   * This method is used during elaboration to recursively discover
   * the true containing hierarchy for flattened tasks/functions.
   *
   * For example, if task 't' is declared in module 'sub' and module
   * 'sub' is flattened into 'top', this method would return the
   * symbol table node for 'top.sub'.
   *
   * TBD: Use this method to preserve named block hierarchy during
   * flattening.
   *
   * \param unelab_parent The parent of a task/function unelaborated symbol table node.
   *
   * \param module_hier The hierarchy of the actual containing module (post-flattening).
   *
   * \returns the elaborated symbol table node corresponding to the
   * hierarchical (declaration) parent of a task/function.
   */
  static const STBranchNode * resolveElabHierForTF(const STBranchNode * unelab_parent, 
                                                   const STBranchNode * module_hier);

  /*!
   * Return the STBranchNode for the declaration context of given net elaborated inside the
   * given hierarchy.
   *
   * Find the STBranchNode corresponding to the module in which hier resides.
   * Then, resolve the elaboration of net along the path to the module.
   */
  static const STBranchNode * resolveElabHierForNet(const NUNet *net, const STBranchNode *hier);

  //! Find the real symtab branch node associated with a branch node from the alias db.
  /*!
   * \param name_node The AliasDB branch (may or may not have an
   *                  associated scope)
   * \param root      The current, known symtab branch.
   * \param symtab    The symbol table.
   *
   * \returns a branch node from the symtab which corresponds to
   * name_node from the alias db.
   */
  static STBranchNode * findRealHier(const STBranchNode * name_node, 
                                     STBranchNode * root, 
                                     STSymbolTable * symtab);

  //! Find the real symtab leaf node associated with a leaf node from the alias db.
  /*!
   * \param name_leaf The AliasDB leaf.
   * \param root      The current, known symtab branch.
   * \param symtab    The symbol table.
   *
   * \returns a branch node from the symtab which corresponds to
   * name_node from the alias db.
   */
  static STAliasedLeafNode * findRealLeaf(const STAliasedLeafNode * name_leaf, 
                                          STBranchNode * root, 
                                          STSymbolTable * symtab);

  /*!
   * Recursively find the module hierarchy node containing the given hier.
   */
  static const STBranchNode * findModuleInHier(const STBranchNode *hier);

private:
  /*!
   * Return the STBranchNode for the given block elaborated inside the given hierarchy.
   *
   * Recursively traverse upward until find the module parent, then return the
   * elaboration path as unwind.
   */
  static const STBranchNode * resolveElabHierForScope(const NUScope *scope, const STBranchNode *hier);

  //! Follow a branch node back until we find a module.
  static STBranchNode * findModuleRoot(STBranchNode * branch);

};
#endif
