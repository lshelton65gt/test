// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUTFARGCONNECTION_H_
#define NUTFARGCONNECTION_H_

#include "nucleus/NUUseDefNode.h"
#include "util/SourceLocator.h"
#include "nucleus/NUCopyContext.h"

/*!
  \file
  Declaration of the Nucleus task/function argument connection class.
*/

//! NUTFArgConnection class
/*!
 * Abstract base class representing a task/function argument connection.
 */
class NUTFArgConnection : public NUUseDefNode
{
public:
  //! constructor
  /*!
    \param formal Formal argument
    \param tf Task/Function being called
    \param loc Source location.
  */
  NUTFArgConnection(NUNet *formal, NUTF *tf, const SourceLocator& loc) :
    mFormal(formal), mTF(tf), mLoc(loc)
  {}

  //! Return the type of this class
  virtual NUType getType() const = 0;

  //! Where is this node defined in the verilog source?
  const SourceLocator& getLoc() const { return mLoc; }

  //! Return the formal net
  NUNet *getFormal() const { return mFormal; }

  //! Return the set of actual nets in the provided set
  virtual void getActuals(NUNetSet* actuals) const = 0;

  //! Return the bit size of this connection
  UInt32 getBitSize() const;

  //! Return the task/function being called
  NUTF *getTF() const { return mTF; }

  //! Replace the task/function and fixes up the formal
  void replaceTF(NUTF* newTF);

  //! Get the parent module for this use def node
  const NUModule* findParentModule() const;
  NUModule* findParentModule();

  //! Return the direction of the connection
  virtual PortDirectionT getDir() const = 0;

  //! Equivalence
  virtual bool operator==(const NUTFArgConnection&) const = 0;

  //! Codegen
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Compose a name
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const = 0;

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const = 0;

  //! Return class name
  virtual const char* typeStr() const = 0;

  //! Return a copy of this
  virtual NUTFArgConnection* copy(CopyContext &copy_context) const = 0;

  //! destructor
  virtual ~NUTFArgConnection() {}

  //! Get the elaborated uses of this connection
  virtual void getUsesElab(STBranchNode*, NUNetElabSet*) const = 0;

  //! manage any sub-expressions in a factory
  virtual void manage(NUExprFactory*);

  //! Is the formal signed?
  bool isSigned() const;

protected:
  //! Formal argument
  NUNet *mFormal;

  //! Task/function being called
  NUTF *mTF;

  //! Source location of arg connection
  SourceLocator mLoc;

private:
  //! Hide copy and assign constructors.
  NUTFArgConnection(const NUTFArgConnection&);
  NUTFArgConnection& operator=(const NUTFArgConnection&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUTFArgConnection : public NUUseDefNode

//! NUTFArgConnectionInput class
/*!
 * Class representing an input task/function argument connection.
 */
class NUTFArgConnectionInput : public NUTFArgConnection
{
public:
  //! constructor
  /*!
    \param actual Actual argument
    \param formal Formal argument
    \param tf Task/Function being called
    \param loc Source location.
  */
  NUTFArgConnectionInput(NUExpr *actual, NUNet *formal, NUTF *tf, const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUTFArgConnectionInput; }

  //! Return the direction of the connection
  PortDirectionT getDir() const { return eInput; }

  //! Return the actual expression
  NUExpr *getActual() const { return mActual; }

  //! Set the actual expression
  void setActual(NUExpr * actual) { mActual = actual; }

  //! Return the set of actual nets in the provided set
  void getActuals(NUNetSet* actuals) const;

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  void getUsesElab(STBranchNode*, NUNetElabSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(NUNet *net, NUNetSet *uses) const;
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Equivalence
  virtual bool operator==(const NUTFArgConnection&) const;

  //! Codegen
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! symbolic printing
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return class name
  const char* typeStr() const;

  //! Return a copy of this
  NUTFArgConnection* copy(CopyContext &copy_context) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUTFArgConnectionInput();

  //! manage any sub-expressions in a factory
  virtual void manage(NUExprFactory*);

private:
  //! Hide copy and assign constructors.
  NUTFArgConnectionInput(const NUTFArgConnectionInput&);
  NUTFArgConnectionInput& operator=(const NUTFArgConnectionInput&);

  //! Actual expression
  NUExpr *mActual;

  //! keep track of whether we own this actual.
  /*! We could hide in bottom bit of mActual if we want to trim memory */
  bool mOwnActual APIOMITTEDFIELD;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUTFArgConnectionInput : public NUTFArgConnection

//! NUTFArgConnectionOutput class
/*!
 * Class representing an output task/function argument connection.
 */
class NUTFArgConnectionOutput : public NUTFArgConnection
{
public:
  //! constructor
  /*!
    \param actual Actual argument
    \param formal Formal argument
    \param tf Task/Function being called
    \param loc Source location.
  */
  NUTFArgConnectionOutput(NULvalue *actual, NUNet *formal, NUTF *tf, const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUTFArgConnectionOutput; }

  //! Return the direction of the connection
  PortDirectionT getDir() const { return eOutput; }

  //! Return the actual l-value
  NULvalue *getActual() const { return mActual; }

  //! Set the actual l-value
  void setActual(NULvalue * actual) { mActual = actual; }

  //! Return the set of actual nets in the provided set
  void getActuals(NUNetSet* actuals) const;

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  void getUsesElab(STBranchNode*, NUNetElabSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(NUNet *net, NUNetSet *uses) const;
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Equivalence
  virtual bool operator==(const NUTFArgConnection&) const;

  //! Codegen
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! symbolic printing
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return class name
  const char* typeStr() const;

  //! Return a copy of this
  NUTFArgConnection* copy(CopyContext &copy_context) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUTFArgConnectionOutput();

private:
  //! Hide copy and assign constructors.
  NUTFArgConnectionOutput(const NUTFArgConnectionOutput&);
  NUTFArgConnectionOutput& operator=(const NUTFArgConnectionOutput&);

  //! Actual l-value
  NULvalue *mActual;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUTFArgConnectionOutput : public NUTFArgConnection

//! NUTFArgConnectionBid class
/*!
 * Class representing a bidirect task/function argument connection.
 */
class NUTFArgConnectionBid : public NUTFArgConnection
{
public:
  //! constructor
  /*!
    \param actual Actual argument
    \param formal Formal argument
    \param tf Task/Function being called
    \param loc Source location.
  */
  NUTFArgConnectionBid(NULvalue *actual, NUNet *formal, NUTF *tf, const SourceLocator& loc) :
    NUTFArgConnection(formal, tf, loc),
    mActual(actual)
  {}

  //! Return the type of this class
  virtual NUType getType() const { return eNUTFArgConnectionBid; }

  //! Return the direction of the connection
  PortDirectionT getDir() const { return eBid; }

  //! Return the actual l-value
  NULvalue *getActual() const { return mActual; }

  //! Set the actual l-value
  void setActual(NULvalue * actual) { mActual = actual; }

  //! Return the set of actual nets in the provided set
  void getActuals(NUNetSet* actuals) const;

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  void getUsesElab(STBranchNode*, NUNetElabSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(NUNet *net, NUNetSet *uses) const;
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Equivalence
  virtual bool operator==(const NUTFArgConnection&) const;

  //! Codegen
  virtual CGContext_t emitCode (CGContext_t) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! symbolic printing
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return class name
  const char* typeStr() const;

  //! Return a copy of this
  NUTFArgConnection* copy(CopyContext &copy_context) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUTFArgConnectionBid();

private:
  //! Hide copy and assign constructors.
  NUTFArgConnectionBid(const NUTFArgConnectionBid&);
  NUTFArgConnectionBid& operator=(const NUTFArgConnectionBid&);

  //! Actual l-value
  NULvalue *mActual;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUTFArgConnectionBid : public NUTFArgConnection

#endif
