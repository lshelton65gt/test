// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUNAMEDDECLARATIONSCOPE_H_
#define NUNAMEDDECLARATIONSCOPE_H_

#include "nucleus/NUModule.h"

class CGAuxInfo;

/*!
  \file
  Declaration of the Nucleus declaration scope and elaborated
  declaration scope classes.
 */

//! NUNamedDeclarationScope class
/*!
 * Model the variable-declaring aspect of a Verilog named block.
 * Declaration scopes can be arbitrarily nested.
 *
 * NUNamedDeclarationScope are also used in VHDL
 *
 * NUNamedDeclarationScope can also contain NUModuleInstance because 
 * named or unnamed generate blocks can contain module instantiations.
 *
 * Verilog named blocks are separated into declaration and definition
 * components. Variable declarations are added to the
 * NUNamedDeclarationScope hierarchy, while code is owned by the
 * NUBlock.
 *
 * All user variables must be declared within an
 * NUNamedDeclarationScope. NUBlock objects may declare nets; only
 * non-static declarations are allowed. Nets declared within an
 * NUBlock will never be visible to the user.
 *
 * NUNamedDeclarationScope objects are elaborated and emitted into the
 * symbol table. An NUNamedDeclarationScope is not allowed to contain
 * statements. It is only for declaration.
 *
 * The block is responsible for memory management of its declared nets.
 * 
 * \sa NUBlock
 */
class NUNamedDeclarationScope : public NUBase, public NUScope
{
public:
  //! Constructor for an NUNamedDeclarationScope.
  /*!
   * \param name   The user-specified name for this declaration scope.
   *
   * \param parent Parent scope containing this one. Usually, this ist
   *               a module, task, or another declaration scope.
   *
   * \param iodb   IODB to store intrinsic type information.
   *
   * \param netref_factory Factory used to manage NetRefs; needed for
   *               u/d containers.
   *
   * \param loc    Source location.
   */
  NUNamedDeclarationScope(StringAtom * name,
                          NUScope * parent,
                          IODBNucleus * iodb,
                          NUNetRefFactory * netref_factory,
                          const SourceLocator & loc,
			  HierFlags hierFlag = eNoFlag);

  //! Destructor for an NUNamedDeclarationScope.
  ~NUNamedDeclarationScope();

  //! Return the type of scoping unit.
  ScopeT getScopeType() const { return NUScope::eNamedDeclarationScope; }

  //! From NUScope; return this as the base object.
  NUBase* getBase() { return this; }
  const NUBase* getBase() const { return this; }

  //! Get source location
  const SourceLocator& getLoc() const { return mLoc; }

  //! Realise NUScope::getSourceLocation virtual method
  virtual const SourceLocator getSourceLocation () const { return getLoc (); }

  //! Mutate the location field
  void putLoc(const SourceLocator& src) { mLoc = src;}

  //! Return the name of this declaration scope.
  /*!
   * \return The local name of this declaration scope. If this scope
   * is embedded within another declaration scope, this method does
   * not return a hierarchical path to the scope; only the local name
   * is returned.
   */
  StringAtom *getName() const { return mName; }

  //! Return the scope in which this scope is defined.
  NUScope* getParentScope() const { return mParent; }

  //! All declaration scopes are emitted in the symbol table; return true;
  bool emitInSymtab() const { return true; }

  //! For objects contained within this block, return their position within this scope's symbol table entry.
  SInt32 reserveSymtabIndex();

  //! Return my index in my parent's symbol table entry.
  SInt32 getSymtabIndex() const;

  //! Return the elaborated declaration scope for this scope in the given hierarchy.
  NUNamedDeclarationScopeElab *lookupElab(STBranchNode *hier) const;

  //! Create an elaboration of this declaration at the given hierarchy and put it into the symbol table.
  NUNamedDeclarationScopeElab *createElab(STBranchNode *hier, STSymbolTable *symtab);

  //! Illegal to call; declaration scopes cannot contain blocks.
  void addBlock(NUBlock * block);

  //! Illegal to call; declaration scopes cannot contain blocks.
  void removeBlock(NUBlock * block);

  //! Will always loop over an empty set of blocks.
  NUBlockCLoop loopBlocks() const;

  //! Will always loop over an empty set of blocks.
  NUBlockLoop loopBlocks();

  //! Add the given declaration scope as a child.
  void addDeclarationScope(NUNamedDeclarationScope *scope);

  //! Remove the given declaration scope from being a child.
  void removeDeclarationScope(NUNamedDeclarationScope *scope);

  //! Loop over sub-declaration scopes.
  NUNamedDeclarationScopeCLoop loopDeclarationScopes() const;

  //! Loop over sub-declaration scopes.
  NUNamedDeclarationScopeLoop loopDeclarationScopes();

  //! Add a net to this block's locally-declared nets.
  /*! 
   * \param net  The net to add. 
   *
   * It is asserted that this net must be non-static, temporary, and
   * block-local.
   */
  void addLocal(NUNet* net) APIDISTILLMACRO4(APIEMITBIND(mLocalNetList),
                                             APIEMITCODE(mLocalNetList.push_back(net)),
                                             APIEMITCODE(addNetName(net)),
                                             APIEMITCODE(mIODB->addTypeIntrinsic(net)));



  //! Remove a net from this scope's set of locally-defined nets.
  /*!
   * \param net  The net to remove.
   *
   * \param remove_references If true, references to 'net' are removed
   *             from the NetRef factory. If live NetRef references
   *             still exist, this will cause a reference-count
   *             assertion.
   */
  void removeLocal(NUNet *net, bool remove_references=true);

  //! remove a set of nets (more efficient than calling removeLocal N times)
  void removeLocals(const NUNetSet& nets, bool remove_references);

  //! Loop over the locally-declared nets.
  NUNetCLoop loopLocals() const { return NUNetCLoop(mLocalNetList); }

  //! Loop over the locally-declared nets.
  NUNetLoop loopLocals() { return NUNetLoop(mLocalNetList); }

  //! For a declaration scope, same as getAllNets.
  void getAllNonTFNets(NUNetList *net_list) const;

  //! Add all nets local to this scope to the given list, in no particular order.
  /*!
   * Nets declared local to blocks will be added also; this list has
   * all nets declared within this scope.
   */
  void getAllNets(NUNetList *net_list) const;

  //! Add the nets from the nested named blocks to the given list
  void getAllSubNets(NUNetList *net_list) const;

  //! Iterate over every all nested declaration scope instances
  NUModuleInstanceMultiLoop loopInstances();

  //! Iterate over every all nested declaration scope instances
  NUModuleInstanceMultiCLoop loopInstances() const;

  //! Does this declaration scope or it's child declaration scopes have submodules?
  bool hasInstances() const;

  //! Iterate over this declaration scope's instances and not the nested ones.
  NUModuleInstanceMultiLoop loopDeclScopeInstances();
  
  //! Iterate over this declaration scope's instances and not the nested ones.
  NUModuleInstanceMultiCLoop loopDeclScopeInstances() const;

  //! Add the module instance to this NamedDeclarationScope's instantiations.
  void addModuleInstance(NUModuleInstance* instance);
  
  //! Remove this instance from this NamedDeclarationScope's instantiations.
  void removeModuleInstance(NUModuleInstance* instance, NUModuleInstance *replacement=0);

  //! replace all the module instances
  void replaceModuleInstances(const NUModuleInstanceList& new_mods) {
    mInstanceList = new_mods;
  }

  //! Declaration scopes are never at the top-level.
  bool atTopLevel() const { return false; }

  //! calculate cost, add it to NUCost* argument
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! Return class name
  const char* typeStr() const;

  //! CodeGen
  CGContext_t emitCode (CGContext_t) const;
  
  CGAuxInfo* getCGOp() const;

  void setCGOp(CGAuxInfo* cgOp) const;

  //! print verilog-syntax (default printer if not overridden)
  void printVerilog(bool addNewline = false,
                    int indent = 0,
                    bool recurse = true) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! Get the netref factory
  NUNetRefFactory* getNetRefFactory() const {return mNetRefFactory;}

  //! Remove all instances in this scope and nested scopes.
  void removeAllInstances();

  //! Used by parent to clean up instances in this scope and nested scopes,
  //! prior to deleting the scopes themselves.
  void deleteAllInstances();

  //! Is the declaration scope empty?
  /*!
    It is empty if it has no instances, no local nets and all the nested declaration
    scopes that are empty. If a list is given, it adds itself to the list.
  */
  bool isEmpty(NUNamedDeclarationScopeList* emptyDeclScopes);


protected:
  //! Invalid to call.
  NetFlags getTempFlags() const;

private:
  //! Hide copy and assign constructors.
  NUNamedDeclarationScope(const NUNamedDeclarationScope&);
  NUNamedDeclarationScope& operator=(const NUNamedDeclarationScope&);

  //! Locally-declared nets in no particular order.
  NUNetList mLocalNetList;

  //! Nested declaration scopes in no particular order.
  NUNamedDeclarationScopeList mDeclarationScopeList;

  //! List of instantiations.
  NUModuleInstanceList mInstanceList;

  //! Name
  StringAtom *mName;

  //! Parent scope
  NUScope *mParent;

  //! Index of me in the symbol table.
  SInt32 mMySymtabIndex;

  //! Currently-available symtab index
  SInt32 mCurSymtabIndex;

  //! Currently-available name number, used to generate unique names.
  UInt32 mCurNameIndex;

  //! Netref factory
  NUNetRefFactory *mNetRefFactory APIOMITTEDFIELD;
  
  //! Source location of the statement.
  SourceLocator mLoc;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUNamedDeclarationScope : public NUBase, public NUScope

//! NUNamedDeclarationScopeElab class
/*!
 * Model the elaboration of a named declaration scope.
 */
class NUNamedDeclarationScopeElab : public NUScopeElab
{
public:
  //! Constructor
  /*!
   * \param scope The corresponding unelaborated declaration scope.
   *
   * \param hier  The hierarchy node in the symbol table for this
   *              elaboration
   */
  NUNamedDeclarationScopeElab(NUNamedDeclarationScope * scope, STBranchNode *hier);

  //! Return a UtString identifying this type.
  const char* typeStr() const;

  //! Return the block of which this is an elaboration
  NUNamedDeclarationScope * getScope() const;

  //! Realise NUElabBase::getSourceLocation virtual method
  virtual const SourceLocator getSourceLocation () const;

  //! Realise NUElabBase::findUnelaborated virtual method
  virtual bool findUnelaborated (STSymbolTable *stab, STSymbolTableNode **unelab) const;

  //! Dump myself to UtIO::cout().
  void print(bool recurse, int indent) const;

  //! Dump the elaboration to cout
  void printElab(STSymbolTable *symtab, bool recurse, int indent) const;

  //! Lookup the NUNamedDeclarationScopeElab object for the given hierarchy branch node
  static NUNamedDeclarationScopeElab * lookup(STBranchNode *hier);

  //! Find the NUNamedDeclarationScopeElab for given hierarchy branch node.
  static NUNamedDeclarationScopeElab* find(const STSymbolTableNode* node);

  //! Destructor
  ~NUNamedDeclarationScopeElab();


private:
  //! Hide copy and assign constructors.
  NUNamedDeclarationScopeElab(const NUNamedDeclarationScopeElab&);
  NUNamedDeclarationScopeElab& operator=(const NUNamedDeclarationScopeElab&);
} APIOMITTEDCLASS ; //class NUNamedDeclarationScopeElab : public NUScopeElab

#endif
