// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _NUCMODELFN_H_
#define _NUCMODELFN_H_

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUBlockingMaps.h"

class NUCModelInteface;

//! NUCModelFn class
/*! Abstract class that models the c-model function. A c-model may
 *  have different c-model functions because each function represents
 *  a different schedule type and may have different flow
 *  characteristics. The schedule and flow information is provided by
 *  the user.
 *
 *  Note: unlike other nucleus objects, this object stores UD
 *  information based on NUCModelPort classes.
 */
class NUCModelFn : public NUBase
{
public:
  //! constructor
  NUCModelFn(NUCModelInterface* cmodelInterface, UtString* ctx);

  //! destructor
  virtual ~NUCModelFn();

  //! debug help: return the type of what I'm looking at
  const char* typeStr() const { return "NUCModelFn"; }

  //! Get the c-model for this function
  NUCModelInterface* getCModelInterface() const { return mCModelInterface; }

  //! Abstraction for a set of defps
  typedef UtSet<NUCModelPort*> Defs;

  //! Abstraction for an iterator over the set of defs
  typedef Loop<Defs> DefsLoop;

  //! Abstraction for an iterator over the set of defs
  typedef CLoop<Defs> DefsCLoop;

  //! add a def
  void addDef(NUCModelPort* out);

  //! Returns an iterator over the defs
  DefsLoop loopDefs();

  //! Returns an iterator over the defs
  DefsCLoop loopDefs() const;

  //! Abstraction for UD information on NUCModelPort clases
  typedef UtMultiMap<NUCModelPort*, NUCModelPort*> DefUseMap;
  
  //! Iterator over the NUCModelPort UD information
  typedef LoopMap<DefUseMap> DefUseMapLoop;

  //! Iterator over the NUCModelPort UD information
  typedef LoopMap<const DefUseMap, DefUseMap::const_iterator> DefUseMapCLoop;

  //! Add a mapping from a def to its use (def must exist)
  void addUse(NUCModelPort* out, NUCModelPort* in);

  //! Iterator over the UD information
  DefUseMapLoop loopUD();

  //! Iterator over the UD information
  DefUseMapCLoop loopUD() const;

  //! Where is this node defined in the verilog source?
  const SourceLocator& getLoc() const;

  //! If this has a name, return it, else return NULL.
  StringAtom* getName() const;

  //! Get the context under which this c-model should be called
  const UtString* getContext() const { return mContext; }

  //! debug help: print routine
  void print(bool verbose, int indent) const;

  //! debug help: composeroutine
  void compose(UtString*, const STBranchNode* scope, int numSpaces, bool recurse) const;

  //!Code Generator
  /*!Generate equivalent C++ code for the verilog object.
   * \returns CGContext_t providing information about code generated.
   */
  CGContext_t emitCode(CGContext_t context) const;

private:
  //! Hide copy and assign constructors.
  NUCModelFn(const NUCModelFn&);
  NUCModelFn& operator=(const NUCModelFn&);

  // The NUCModelInterface this function is for. The NUCModelInterface
  // contains all the general information that is common to all
  // functions for a given c-model.
  NUCModelInterface* mCModelInterface;
  UtString* mContext;

  // UD Information in terms of NUCModelPorts. Note that we need both
  // a def map and a def to use map because we could have some defs
  // with no level sensitive uses. (Edge sensitive uses are handled by
  // the always block around this cmodel).
  Defs* mDefs;
  DefUseMap* mDefUseMap;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUCModelFn : public NUUseDefNode

#endif // _NUCMODELFN_H_
