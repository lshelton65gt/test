// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _NUCMODEL_H_
#define _NUCMODEL_H_

#include "nucleus/Nucleus.h"

class STBranchNode;             // extern ref

//! NUCModel class
/*! For a given design, there is only one copy of a c-model. This is
 *  because a c-model replaces a module, task, function, user-defined
 *  task or function it replaces so multiple instances of those
 *  constructs handle the multiple instances.
 *
 *  However, with flattening, we may end up moving two instances of a
 *  c-model module into its parent. This will mean there are two
 *  copies of the same c-model in a module. We don't want to replicate
 *  all of the c-model because most of the data can be shared. But we
 *  need a unique name to create storage for the c-model handle.
 *
 *  This is where the NUCModel comes in. It can be replicated
 *  and gets a new unique name.
 *
 *  The other benefit of having a c-model is we can have multiple
 *  parameterized modules as c-models. We want only one routine for
 *  the user so there is only one NUCModelInterface. We show all the
 *  parameter variants with multiple NUCModels, each with a different
 *  variant index.
 */
class NUCModel : public NUBase
{
public:
  //! constructor
  NUCModel(NUCModelInterface* cmodelInterface, 
           NUModule * parent_module,
           UInt32 variant,
           UtString* origName = NULL);

  //! destructor
  virtual ~NUCModel();

  //! Get the unique name for this c-model
  const UtString* getName() const { return mName; }

  //! Get the original sub hierarchy for this cmodel in case it got flattened
  const UtString* getOrigName() const { return mOrigName; }

  //! Get the c-model
  NUCModelInterface* getCModelInterface() const { return mCModelInterface; }

  //! Get the variant
  UInt32 getVariant() const { return mVariant; }

  //! debug help: return the type of what I'm looking at
  const char* typeStr() const { return "NUCModel"; }

  //! Abstraction for a parameter name and value (string pair)
  typedef std::pair<UtString, UtString> Param;

  //! Add a parameter name and value for this c-model
  void addParam(Param& param);

  //! Get the number of parameter name/value pairs
  UInt32 getNumParams() const;

  //! Abstraction for an iterator over the parameter names and values
  typedef Loop<UtVector<Param*> > ParamLoop;

  //! Get the parent module for this CModel
  NUModule* getParentModule() const { return mModule; }

  //! Iterator over the parameter name/value pairs
  ParamLoop loopParams() const;

  //!Code Generator
  /*!Generate equivalent C++ code for the verilog object.
   * \returns CGContext_t providing information about code generated.
   */
  CGContext_t emitCode(CGContext_t context) const;

  //! generate a verilog string for the object and place in \a buf
  virtual void compose(UtString* buf, const STBranchNode* scope, int indent, bool recurse) const;

private:
  //! Hide copy and assign constructors.
  NUCModel(const NUCModel&);
  NUCModel& operator=(const NUCModel&);

  // Local data
  NUCModelInterface* mCModelInterface;
  NUModule* mModule; // parent module
  UInt32 mVariant;
  UtString* mName;
  UtString* mOrigName;
  typedef UtVector<Param*> Params;
  Params* mParams;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUCModel : public NUBase

#endif // _NUCMODEL_H_
