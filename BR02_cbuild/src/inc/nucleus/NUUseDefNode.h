// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUUSEDEFNODE_H_
#define NUUSEDEFNODE_H_

#include "nucleus/NUUseNode.h"

class SourceLocator;
class StringAtom;
class BDD;
class BDDContext;
class STBranchNode;

/*!
  \file
  Declaration of the Nucleus use-def-node class.
*/

//! NUType enum
enum NUType
{
  eNUAlwaysBlock,
  eNUAssign,
  eNUVarselLvalue,
  eNUBlock,
  eNUBlockingAssign,
  eNUBlockingEnabledDriver,
  eNUBreak,
  eNUCase,
  eNUCModelCall,
  eNUCModel,
  eNUCModelArgConnectionInput,
  eNUCModelArgConnectionOutput,
  eNUCModelArgConnectionBid,
  eNUConcatLvalue,
  eNUContAssign,
  eNUContEnabledDriver,
  eNUControlSysTask,
  eNUCycle,
  eNUEnabledDriver,
  eNUIdentLvalue,
  eNUIf,
  eNUInitialBlock,
  eNUInputSysTask,
  eNULvalue,
  eNUMemselLvalue,
  eNUModule,
  eNUModuleInstance,
  eNUNamedDeclarationScope,
  eNUNonBlockingAssign,
  eNUOutputSysTask,
  eNUFOpenSysTask,
  eNUFCloseSysTask,
  eNUFFlushSysTask,
  eNUPortConnectionBid,
  eNUPortConnectionInput,
  eNUPortConnectionOutput,
  eNUStructuredProc,
  eNUTF,
  eNUTFArgConnectionBid,
  eNUTFArgConnectionInput,
  eNUTFArgConnectionOutput,
  eNUTFElab,
  eNUTask,
  eNUTaskEnable,
  eNUTriRegInit,
  eNUPartselIndexLvalue,
  eNUReadmemX,
  eNUSysRandom,
  eNUFor,
  eNUCompositeLvalue,
  eNUCompositeIdentLvalue,
  eNUCompositeSelLvalue,
  eNUCompositeFieldLvalue
};

//! NUUseDefNode class
/*!
 * Interface class, models a Nucleus construct which uses and/or defines net values.
 *
 * Some derived classes will be able to compute their own use/def values as needed.
 * For those classes, the following member functions will not be used:
 *  . addDef()
 *  . addUse()
 *  . clearUseDef()
 *
 * For other derived classes, though, the computation of use/def is non-trivial,
 * and those functions will be used.
 */
class NUUseDefNode : public NUUseNode
{
public:
  //! constructor
  NUUseDefNode() {}

  //! Get the type of this use def
  virtual NUType getType() const = 0;

  //! Add the nets this node uses to the given set.
  virtual void getUses(NUNetSet *uses) const = 0;
  virtual void getUses(NUNetRefSet *uses) const = 0;
  virtual bool queryUses(const NUNetRefHdl &use_net_ref,
                         NUNetRefCompareFunction fn,
                         NUNetRefFactory *factory) const = 0;

  //! Add the nets this node uses to def the given net to the given set.
  virtual void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const = 0;
  virtual bool queryUses(const NUNetRefHdl &def_net_ref,
                         const NUNetRefHdl &use_net_ref,
                         NUNetRefCompareFunction fn,
                         NUNetRefFactory *factory) const = 0;

  //! Add the nets this node defines to the given set.
  virtual void getDefs(NUNetSet *defs) const = 0;
  virtual void getDefs(NUNetRefSet *defs) const = 0;
  virtual bool queryDefs(const NUNetRefHdl &def_net_ref,
                         NUNetRefCompareFunction fn,
                         NUNetRefFactory *factory) const = 0;

  //! Does this node represent a simple buffer or assignment of one net to another.
  virtual bool isSimpleBufOrAssign() const;

  //! Does this node represent an output port connection.
  virtual bool isPortConnOutput() const;

  //! Does this node represent an input port connection.
  virtual bool isPortConnInput() const;

  //! Does this node represent a bid port connection.
  virtual bool isPortConnBid() const;

  //! Does this node represent a port connection
  virtual bool isPortConn() const;

  //! Is this node a continuous driver?
  virtual bool isContDriver() const { return false; }

  //! Add the given net to the set of nets being defined by this node.
  /*!
    Note: not all UseDefNode's will need this, so a default empty
    implementation is provided.
  */
  virtual void addDef(const NUNetRefHdl & /* net_ref --unused */) {}
  void addDef1(const NUNetRefHdl &net_ref) {addDef(net_ref);}

  //! Add the given use_net as being used by the given def_net.
  /*!
    Note: not all UseDefNode's will need this, so a default empty
    implementation is provided.
  */
  virtual void addUse(const NUNetRefHdl & /* def_net_ref -- unused */,
		      const NUNetRefHdl & /* use_net_ref -- unused */) {}
  void addUse1(const NUNetRefHdl & def_net_ref,
	       const NUNetRefHdl & use_net_ref) {addUse(def_net_ref,use_net_ref);}

  //! Add the given use_set as being used by the given def_net.
  /*!
    Note: not all UseDefNode's will need this, so a default empty
    implementation is provided.
  */
  virtual void addUses(const NUNetRefHdl & /* def_net_hdl -- unused */,
		       NUNetRefSet * /* use_set -- unused */) {}

  //! Change this node to define new_net instead of old_net.
  virtual void replaceDef(NUNet *old_net, NUNet* new_net) = 0;

  //! Clear the use/def information stored in the object, if any.
  /*!
    Note: not all UseDefNode's will need this, so a default empty
    implementation is provided.
   */
  virtual void clearUseDef() {}

  //! Fixup the use/def information; remove all nets in the given list from the sets.
  /*!
    Note: not all UseDefNode's will need this, so a default empty
    implementation is provided.
   */
  virtual void fixupUseDef(NUNetList * /* remove_list -- unused */) {}

  //! Reusable implementation of fixup
  static void helperFixupUseDef(NUNetRefNetRefMultiMap *m, NUNetList *remove_list);
  static void helperFixupUseDef(NUNetRefSet *s, NUNetList *remove_list);

  //! Emit code to begin a profiling bucket.
  void emitStartBucket() const;

  //! Where is this node defined in the verilog source?
  virtual const SourceLocator& getLoc() const = 0;

  //! If this has a name, return it, else return NULL.
  virtual StringAtom* getName() const;

  //! Print the u/d information to cout
  virtual void printUDSets(NUNetRefFactory *factory) const;

  virtual bool stopsFlow(NUNet* inputNet, bool* val);

  //! Return the strength of the drive of this node.
  virtual Strength getStrength() const { return eStrDrive; }

  //! Generate a BDD based on the def of a net
  /*!
    \param ctx The context which is managing the BDD's
    \param scope If non-0, this expression will be elaborated to the given scope.
    \return BDD representation of this expression
   */
  virtual BDD bdd(NUNet* net, BDDContext *ctx, STBranchNode *scope = 0) const;

  //! destructor
  virtual ~NUUseDefNode() {}

  //! Get the parent module for this use def node
  /*! Get the parent module for a statement or structured proc use def
   *  node. This should not be called on modules.
   *
   * Returns NULL if the parent module could not be found.  This can happen
   * because the parent module is found by looking at UseDef information,
   * and if that information is only stored via a UD pass, then it is not
   * available immediately after population.
   */
  virtual const NUModule* findParentModule() const = 0;
  virtual NUModule* findParentModule() = 0;

  //! compose verilog-syntax
  virtual void compose(UtString*,
                       const STBranchNode* scope, 
                       int indent = 0,
                       bool recurse = true) const = 0; // recurse needed for test/fold/foldcase.v

  //! print verilog-syntax (default printer if not overridden)
  void printVerilog(bool addNewline = false,
                    int indent = 0,
                    bool recurse = true) const;

  //! Compare routine to order by use def
  static int compare(const NUUseDefNode* ud1, const NUUseDefNode* ud2);
  bool operator<(const NUUseDefNode& other) const
  {
    return compare(this, &other) < 0;
  }

  //! Print design object information about an assertion
  virtual void printAssertInfoHelper() const;

  //! Can this node drive a Z on the specified net?
  virtual bool drivesZ(NUNet*) const;

  //! In Verilog semantics, does this node preserve a Z on src
  //! when it drives dest?
  /*!
   *! Note that this does not imply (at this time) that
   *! Carbon will preserve the Z properly -- only that Verilog
   *! semantics require it.  This routine is for use in Nucleus
   *! transformation code, or in code that warns about potential
   *! mismatches with simulation
   */
  virtual bool shouldPreserveZ(NUNet* src, NUNet* dest) const;


private:
  //! Hide copy and assign constructors.
  NUUseDefNode(const NUUseDefNode&);
  NUUseDefNode& operator=(const NUUseDefNode&);
} APIOMITTEDCLASS; //class NUUseDefNode : public NUUseNode

//! NUUseDefStmtNode class
/*!
 * Interface class which models a statement which uses, defines, and
 * kills net values.
 *
 * A kill is when a statement redefines a net value completely, thus
 * killing any prior definition:
 \code
   s1:  a = b;
   s2:  a = c;
 \endcode
 * s2 kills the value defined by s1
 *
 \code
   s1:  a = b;
   s2:  a[foo] = c;
 \endcode
 * s2 does not kill s1
 *
 * Statements can have blocking and/or non-blocking definitions in
 * them; these definitions are used by the structured proc in which
 * they appear.  At the statement level, however, we need to keep them
 * separate.
 */
class NUUseDefStmtNode : public NUUseDefNode
{
public:
  //! constructor
  NUUseDefStmtNode() {}

  //! Use this method to decide whether to call the 'getBlocking*' or 'get*' methods
  virtual bool useBlockingMethods() const; 

  //! Invalid to call - use blocking or non-blocking form.
  virtual void getUses(NUNetSet *uses) const;
  virtual void getUses(NUNetRefSet *uses) const;
  virtual bool queryUses(const NUNetRefHdl &use_net_ref,
                         NUNetRefCompareFunction fn,
                         NUNetRefFactory *factory) const;

  //! Invalid to call - use blocking or non-blocking form.
  virtual void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  virtual bool queryUses(const NUNetRefHdl &def_net_ref,
                         const NUNetRefHdl &use_net_ref,
                         NUNetRefCompareFunction fn,
                         NUNetRefFactory *factory) const;
  
  //! Invalid to call - use blocking or non-blocking form.
  virtual void getDefs(NUNetSet *defs) const;
  virtual void getDefs(NUNetRefSet *defs) const;
  virtual bool queryDefs(const NUNetRefHdl &def_net_ref,
                         NUNetRefCompareFunction fn,
                         NUNetRefFactory *factory) const;

  //! Add the nets this node uses for blocking defs to the given set.
  virtual void getBlockingUses(NUNetSet *uses) const = 0;
  virtual void getBlockingUses(NUNetRefSet *uses) const = 0;
  virtual bool queryBlockingUses(const NUNetRefHdl &use_net_ref,
                                 NUNetRefCompareFunction fn,
                                 NUNetRefFactory *factory) const = 0;

  //! Add the nets this node uses to blockingly def the given net to the given set.
  virtual void getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const = 0;
  void getBlockingUses1(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const {getBlockingUses(net_ref,uses);}
  virtual bool queryBlockingUses(const NUNetRefHdl &def_net_ref,
                                 const NUNetRefHdl &use_net_ref,
                                 NUNetRefCompareFunction fn,
                                 NUNetRefFactory *factory) const = 0;
  
  //! Add the nets this node uses for non-blocking defs to the given set.
  virtual void getNonBlockingUses(NUNetSet *uses) const = 0;
  virtual void getNonBlockingUses(NUNetRefSet *uses) const = 0;
  virtual bool queryNonBlockingUses(const NUNetRefHdl &use_net_ref,
                                    NUNetRefCompareFunction fn,
                                    NUNetRefFactory *factory) const = 0;

  //! Add the nets this node uses to non-blockingly def the given net to the given set.
  virtual void getNonBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const = 0;
  void getNonBlockingUses1(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const {getNonBlockingUses(net_ref,uses);}
  virtual bool queryNonBlockingUses(const NUNetRefHdl &def_net_ref,
                                    const NUNetRefHdl &use_net_ref,
                                    NUNetRefCompareFunction fn,
                                    NUNetRefFactory *factory) const = 0;
  
  //! Add the nets this node blockingly defines to the given set.
  virtual void getBlockingDefs(NUNetSet *defs) const = 0;
  virtual void getBlockingDefs(NUNetRefSet *defs) const = 0;
  void getBlockingDefs1(NUNetRefSet *defs) const {getBlockingDefs(defs);}
  virtual bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
                                 NUNetRefCompareFunction fn,
                                 NUNetRefFactory *factory) const = 0;

  //! Add the nets this node non-blockingly defines to the given set.
  virtual void getNonBlockingDefs(NUNetSet *defs) const = 0;
  virtual void getNonBlockingDefs(NUNetRefSet *defs) const = 0;
  void getNonBlockingDefs1(NUNetRefSet *defs) const {getNonBlockingDefs(defs);}
  virtual bool queryNonBlockingDefs(const NUNetRefHdl &def_net_ref,
                                    NUNetRefCompareFunction fn,
                                    NUNetRefFactory *factory) const = 0;

  //! Add the nets this node blockingly kills to the given set.
  virtual void getBlockingKills(NUNetSet *kills) const = 0;
  virtual void getBlockingKills(NUNetRefSet *kills) const = 0;
  virtual bool queryBlockingKills(const NUNetRefHdl &def_net_ref,
                                  NUNetRefCompareFunction fn,
                                  NUNetRefFactory *factory) const = 0;

  //! Add the nets this node non-blockingly kills to the given set.
  virtual void getNonBlockingKills(NUNetSet *kills) const = 0;
  virtual void getNonBlockingKills(NUNetRefSet *kills) const = 0;
  virtual bool queryNonBlockingKills(const NUNetRefHdl &def_net_ref,
                                     NUNetRefCompareFunction fn,
                                     NUNetRefFactory *factory) const = 0;

  //! Add the use_net as being used by the blocking def of def_net.
  /*!
    Note: not all UseDefNode's will need this, so a default empty
    implementation is provided.
   */
  virtual void addBlockingUse(const NUNetRefHdl & /* def_net_ref -- unused */,
                              const NUNetRefHdl & /* use_net_ref -- unused */) {}

  //! Add the use_net as being used by the non-blocking def of def_net.
  /*!
    Note: not all UseDefNode's will need this, so a default empty
    implementation is provided.
   */
  virtual void addNonBlockingUse(NUNet * /* def_net -- unused */,
                                 NUNet * /* use_net -- unused */) {}
  virtual void addNonBlockingUse(const NUNetRefHdl & /* def_net_ref -- unused */,
                                 const NUNetRefHdl & /* use_net_ref -- unused */) {}

  //! Add the net to the set of nets this stmt blockingly-kills.
  /*!
    Note: not all UseDefNode's will need this, so a default empty
    implementation is provided.
   */
  virtual void addBlockingKill(const NUNetRefHdl & /* net_ref -- unused */) {}

  //! Add the net to the set of nets this stmt non-blockingly-kills.
  /*!
    Note: not all UseDefNode's will need this, so a default empty
    implementation is provided.
   */
  virtual void addNonBlockingKill(NUNet * /* net -- unused */) {}
  virtual void addNonBlockingKill(const NUNetRefHdl & /* net_ref -- unused */) {}

  //! Add the net to the set of nets this stmt blockingly-defs.
  /*!
    Note: not all UseDefNode's will need this, so a default empty
    implementation is provided.
   */
  virtual void addBlockingDef(const NUNetRefHdl & /* net_ref -- unused */) {}

  //! Add the net to the set of nets this stmt non-blockingly-defs.
  /*!
    Note: not all UseDefNode's will need this, so a default empty
    implementation is provided.
   */
  virtual void addNonBlockingDef(NUNet * /* net -- unused */) {}
  virtual void addNonBlockingDef(const NUNetRefHdl & /* net_ref -- unused */) {}
  //! Print the u/d information to cout
  virtual void printUDSets(NUNetRefFactory *factory) const;

  //! Return the parent module
  const NUModule* findParentModule() const;
  virtual NUModule* findParentModule();

  //! destructor
  virtual ~NUUseDefStmtNode() {}


private:
  //! Hide copy and assign constructors.
  NUUseDefStmtNode(const NUUseDefStmtNode&);
  NUUseDefStmtNode& operator=(const NUUseDefStmtNode&);
} APIOMITTEDCLASS; //class NUUseDefStmtNode : public NUUseDefNode

//! Comparator for value-based NUUseDefNode comparisons.
struct NUUseDefNodeCmp
{
  bool operator()(const NUUseDefNode* ud1, const NUUseDefNode* ud2) const
  {
    return NUUseDefNode::compare(ud1,ud2) < 0;
  }
} APIOMITTEDCLASS; //struct NUUseDefNodeCmp

typedef UtSet<NUUseDefNode*,NUUseDefNodeCmp> NUUseDefOrderedSet;

#endif
