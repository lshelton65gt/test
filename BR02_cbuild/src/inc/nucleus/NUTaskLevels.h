// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "nucleus/Nucleus.h"

//! class to bucketize the levels (height) of tasks within a module callgraph
/*!
 *! The height of a task, the distance from lowest leaf, differs from its
 *! depth, the distance from the root.  The height of a leaf task is 0,
 *! independent of where it is instantiated relative to the root.
 *!
 *! This depth computation is strictly within a module -- hierarchical task
 *! calls are excluded.
 *!
 *! With mutually recursive tasks, an arbitrary point in the task cycle
 *! will be picked as the "leaf" and the other tasks in the cycle will get
 *! height based on that leaf.
 */
class NUTaskLevels {
public:
  //! ctor
  NUTaskLevels();

  //! dtor
  ~NUTaskLevels();

  //! compute the instance levelization for the design
  void module(const NUModule*);

  //! how many levels of task hierarchy are there in the module?
  UInt32 numTaskLevels() const;

  typedef Loop<NUTaskList> TaskLevelLoop;

  //! loop over the tasks at a particular level (must be < numInstanceLevels())
  TaskLevelLoop loopLevel(UInt32 level) const;

  //! Get the height of a task in the task-enable tree (distinct from depth)
  UInt32 getTaskLevel(NUTask* mod) const;

private:
  typedef UtVector<NUTaskList*> TaskBuckets;
  typedef UtHashMap<NUTask*, UInt32> TaskLevelMap;

  TaskBuckets* mLevelBuckets;
  TaskLevelMap* mTaskLevelMap;
};
