// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003, 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  Utility functions and classes for working with Nucleus structures
  as abstract graphs.
*/

#ifndef _NUCLEUSGRAPH_H_
#define _NUCLEUSGRAPH_H_

#include "util/Graph.h"
#include "util/GenericDigraph.h"
#include "util/GraphDot.h"

// forward class declarations
class NUDesign;
class STSymbolTable;
class STBranchNode;
class UtOStream;
class UtString;

typedef GenericDigraph<void*, STBranchNode*> ModuleGraph;

//! Build a graph representing the elaborated module hierarcy
ModuleGraph* extractModuleGraph(NUDesign* design, STSymbolTable* symtab);

//! Dump a graph of the elaborated hierarchy
void dumpModuleGraph(NUDesign* design, STSymbolTable* symtab,
                     const UtString& fileName, const UtString& graphName);

//! label each node with its elaborated name
//! A derived GraphDotWriter for writing a module graph in dot format
class ModuleGraphDotWriter : public GraphDotWriter
{
 public:
  ModuleGraphDotWriter(UtOStream& os) : GraphDotWriter(os) {};
  ModuleGraphDotWriter(const UtString& filename) : GraphDotWriter(filename) {};
 private:
  void writeLabel(Graph* g, GraphNode* node);
  void writeLabel(Graph* g, GraphEdge* edge);
};

#endif // _NUCLEUSGRAPH_H_
