// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUTRIREGINIT_H_
#define NUTRIREGINIT_H_

#include "nucleus/NUUseDefNode.h"
#include "util/SourceLocator.h"

/*!
 \file
 NUTriRegInit class declaration.
*/


//! NUTriRegInit class
/*!
 * This class models initialization of a Trireg net.
 *
 * This class behaves like a module-level continous driver.
 */
class NUTriRegInit : public NUUseDefNode
{
public:
  //! constructor
  /*!
    \param name The name of this node, for code generation.
    \param lvalue The lvalue being initialized.
    \param loc The source location of this initialization.
   */
  NUTriRegInit(StringAtom *name,
	       NULvalue *lvalue,
	       const SourceLocator& loc) :
    mName(name),
    mLvalue(lvalue),
    mLoc(loc),
    mScheduled (false)
  {}

  //! Return the type of this class
  virtual NUType getType() const { return eNUTriRegInit; }

  //! Return the Trireg.
  /*!
   * For now, this is an l-value because it is expected that bit
   * individualization may be necessary to support trireg's, and that is not
   * designed yet.
   */
  NULvalue* getLvalue() const { return mLvalue; }

  //! Add the nets this node uses to the given set.
  /*!
   * This is a no-op.
   */
  void getUses(NUNetSet * /* uses -- unused */) const {}
  void getUses(NUNetRefSet * /* uses -- unused */) const {}
  bool queryUses(const NUNetRefHdl & /* use_net_ref -- unused */,
		 NUNetRefCompareFunction /* fn -- unused */,
		 NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this node uses to def the given net to the given set.
  /*!
   * This is a no-op.
   */
  void getUses(NUNet * /* net -- unused */,
	       NUNetSet * /* uses -- unused */) const {}
  void getUses(const NUNetRefHdl & /* net_ref -- unused */,
	       NUNetRefSet * /* uses -- unused */) const {}
  bool queryUses(const NUNetRefHdl & /* def_net_ref -- unused */,
		 const NUNetRefHdl & /* use_net_ref -- unused */,
		 NUNetRefCompareFunction /* fn -- unused */,
		 NUNetRefFactory * /* factory -- unused */) const
  {
    return false;
  }

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Where is this node defined in the verilog source?
  const SourceLocator& getLoc() const { return mLoc; }

  //! Return the name of this node.
  /*!
   * We name these for code generation; there is no scoping use of the name.
   */
  StringAtom* getName() const { return mName; }

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! Return class name
  const char* typeStr() const;

  //! Return the strength of the drive of this node.
  virtual Strength getStrength() const { return eStrPull; }

  //! Code Gen
  virtual CGContext_t emitCode(CGContext_t context) const;

  //! symbolic printing
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return the parent module
  const NUModule* findParentModule() const;
  NUModule* findParentModule();

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! Is this node a continuous driver?
  virtual bool isContDriver() const { return true; }

  //! destructor
  ~NUTriRegInit();

  bool isScheduled (void) const;

  void putScheduled (bool);

private:
  //! Hide copy and assign constructors.
  NUTriRegInit(const NUTriRegInit&);
  NUTriRegInit& operator=(const NUTriRegInit&);

  //! Name of this node.
  StringAtom* mName;

  //! LHS
  NULvalue *mLvalue;

  //! Source location
  SourceLocator mLoc;

  bool mScheduled;
};

#endif
