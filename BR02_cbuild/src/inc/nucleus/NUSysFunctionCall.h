// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef _NU_SYS_FUNCTION_CALL_H_
#define _NU_SYS_FUNCTION_CALL_H_

#include "nucleus/NUExpr.h"

/*!
  \file
  Declaration of the Nucleus system function call class.
*/

//! NUSysFunctionCall class
/*!
 * This does not inherit from, and is not a sibling of, the now
 * non-existent class NUFunctionCall,
 * because that class treats function-call args similar to module
 * ports, with a formal NUNet*.  We don't have such a thing for
 * system tasks, so we just have an expression-vector.   That's
 * similar enough to an NUNaryOp that we will inherit from it,
 * despite this not being an Op.
 */
class NUSysFunctionCall: public NUNaryOp
{
public:
  //! ctor
  NUSysFunctionCall(NUOp::OpT op,
                    SInt8 timeUnit,
                    SInt8 timePrecision,
                    const NUExprVector& exprs,
                    const SourceLocator& loc);

  //! dtor
  virtual ~NUSysFunctionCall();

  //! Function to return the expression type
  virtual Type getType() const;

  //! Return the size this expression "naturally" is.
  virtual UInt32 determineBitSizeHelper() const;

  //! Return true if this expression returns a real value
  virtual bool isReal() const;

  //! calculate cost
  void calcCost(NUCost*, NUCostContext*) const;

  //! return true if both are NUNullExpr since any two are equivalent.
  bool operator==(const NUExpr & otherExpr) const;

  //! shallow equality comparison -- used for NUExprFactory
  bool shallowEq(const NUExpr & otherExpr) const;

  virtual BDD bdd(BDDContext* ctx, STBranchNode* scope) const;

  //! Compose verilog syntax for this expression
  virtual void composeHelper(UtString*, const STBranchNode* scope,
                             bool includeRoot,
                             bool hierName,
                             const char* separator)
    const;

  //! Return class name
  virtual const char* typeStr() const;

  //! Emit C++ Code 
  virtual CGContext_t emitCode (CGContext_t) const;

  //! returns the op-code for a verilog system function name, also a flag if from the converter group of sys functions
  static NUOp::OpT lookupOp(const char* sysFunc, bool * is_conveter_function);

  //! Validate legal arguments for the system call
  bool isValid(UtString* errmsg) const;

  //! Return the value of the syscall's timeUnit
  SInt8 getTimeUnit() const { return mTimeUnit; };

  //! Return the value of the syscall's timeUnit
  SInt8 getTimePrecision() const { return mTimePrecision; };

protected:
  //! shallow equality comparison -- used for NUExprFactory
  virtual bool shallowEqHelper(const NUExpr & otherExpr) const;

  //! Return a copy of this expression tree.
  virtual NUExpr* copyHelper(CopyContext &copy_context) const;

  //! resize the expression based on context
  virtual void resizeHelper(UInt32 size);

private:
  SInt8 mTimeUnit;               //! time unit from `timescale directive
  SInt8 mTimePrecision;          //! time precision from `timescale directive

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUSysFunctionCall: public NUNaryOp

#endif // _NU_SYS_FUNCTION_CALL_H_
