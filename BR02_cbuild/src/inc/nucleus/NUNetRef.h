// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NUNETREF_H_
#define NUNETREF_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUNet.h"
#include "flow/Flow.h"
#include "util/CarbonAssert.h"
#include "util/LoopFilter.h"
#include "util/UtSet.h"
#include "util/UtIOStream.h"
#include "util/DynBitVecDesc.h"
#include <cstddef>

#ifdef CDB
//#define NETREF_DEBUG
#endif

/*!
  \file
  Declaration of Nucleus net reference class.
 */

class DynBitVector;
class NUNetRefRangeLoop;
class NUNetRefHdl;
class NULvalue;

//! NUNetRef class
/*!
 * A NUNetRef is used to track and manage subranges of bits for a NUNet object.
 * NOTE however that the preciseness of this tracking may not be at
 * the level of individual bits. Depending on the type of the NUNet
 * object, the granularity changes:
 *
 *  in a NUBitNet there is only a single bit to keep track of.
 *  in a NUVectorNet the NUNetRef tracks individual bits within the declared range of the vector.
 *  in a NUMemoryNet the NUNetRef has a granularity based on the top level dimension of the memory.  Thus
 *   the NUNetRef tracks whole words for a 2D memory.  For a 3D memory it tracks squares of memory. For a
 *   4D it tracks cubes....
 *  in a NUCompositeNet the NUNetRef has a granularity of the fields.
 *
 * As you can see as you move down this list the preciseness of the tracking is reduced.
 * 
 * When this class was written, the granularity was for individual bits so some comments may 
 * imply that the reference is for individual bits.
 *
 * NUNetRefs are reference counted, so you should not be creating or deleting these, that
 * is handled by the NUNetRefHdl class.
 *
 * Need unelaborated and elaborated versions of this.  May end up being templatized based
 * on NUNet/NUNetElab, may end up having NUNetElab point to a NUNetRef, this is undecided.
 */
class NUNetRef
{
public:
  //! Return the NUNet for this ref.
  NUNet* getNet() const { return mNet; }

  //! Class for filtering drivers.
  class DriverFilter
  {
  public:
    DriverFilter(const NUNetRef *net_ref, NUNetRefCompareFunction fn);
    bool operator() (FLNode *flow) const;

  private:
    const NUNetRef *mNetRef;
    NUNetRefCompareFunction mCompareFunction;
  };

  //! Datatype to abstract looping over driver flow nodes
  typedef LoopFilter<NUNet::DriverLoop,DriverFilter> DriverLoop;

  //! Loop over the drivers which affect this net ref
  /*!
   * All drivers which affect this net ref will be walked.  Note that the drivers may
   * also affect bits not represented in this net ref:
   *   wire [2:0] b;
   *   assign b[1:0] = foo;
   *   assign b[2:0] = bar;
   *
   * If you loopInclusiveDrivers for 'b[0]' you will visit both continuous assigns.
   */
  DriverLoop loopInclusiveDrivers() const
  {
    return loopContinuousDrivers(&NUNetRef::overlapsSameNet);
  }

  //! Loop over the drivers which affect only this net ref
  /*!
   * Drivers which only affect this net ref will be walked:
   *   wire [2:0] b;
   *   assign b[1:0] = foo;
   *   assign b[2:0] = bar;
   *
   * If you loopExclusiveDrivers for 'b[0]' you will visit nothing, for 'b[1:0]' you
   * will visit the assign of 'foo'.
   */
  DriverLoop loopExclusiveDrivers() const
  {
    return loopContinuousDrivers(&NUNetRef::operator==);
  }

  //! Loop over the drivers for which the compare function holds true.
  DriverLoop loopContinuousDrivers(NUNetRefCompareFunction fn) const;

  //! Ordering comparison for containers
  bool operator<(const NUNetRef& other) const;

  //! Ordering comparison for containers.  deepNetCompare is set to 
  //! true if you want to order the NUNetRefs alphabetically by net,
  //! but that should only be done if the nets will not be rescoped
  //! during an ordered containers lifetime.
  static int compare(const NUNetRef& ref1, const NUNetRef& ref2,
                     bool deepNetCompare);

  //! Hashing for hash'd containers
  size_t hash() const {
    return (((size_t)mNet) >> 2) + mBits.hash();
  }
  
  //! Return true if this net ref covers no part of the net.
  bool empty() const { return (mNet == NULL); /*mBits.empty(); */}

  //! Return true if this net ref covers all parts of the net.
  bool all() const { return !empty() && mBits.all(mNet->getNetRefWidth());}

  //! If the range is contiguous return it.
  bool getRange(ConstantRange& range) const;

  //! Return the first contiguous range covered by this net ref.
  bool getFirstContiguousRange(ConstantRange& range) const;

  //! Return the number of bits represented by this net ref
  /*! The number of bits is the count of the bits of this net that are
   *  referenced. So if this reference has holes, it does not count
   *  them.
   *
   *  See the various derived classes for details on how this is
   *  computed.
   */
  UInt32 getNumBits() const;

  //! Return true if this net ref overlaps another.
  /*!
   * This net ref overlaps another if any bit which the other net ref
   * represents is also represented in this net ref. In this version,
   * the two netrefs do not have to represent the same net, just have
   * overlapping bits.  \sa overlaps
   */
  bool overlapsBits(const NUNetRef& other) const;

  //! Return true if this net ref covers the specified bit offset
  bool overlapsBit(UInt32 offset) const;

  //! Return true if this net ref overlaps another.
  /*!
   * This net ref overlaps another if any bit which the other net ref
   * represents is also represented in this net ref. In this version,
   * the two netrefs must represent the same net.  \sa overlapBits
   */
  bool overlapsSameNet(const NUNetRef& other) const;

  //! Return true if this net ref overlaps another.
  /*!
   * This net ref overlaps another if any bit which the other net ref
   * represents is also represented in this net ref.  Giving a false
   * value for the requireSameNet argument allows you to test for
   * overlap of net refs that may not refer to the same net (because
   * of elaborated aliasing, for instance).
   */
  bool overlaps(const NUNetRef& other, bool requireSameNet = true) const {
    return requireSameNet? overlapsSameNet(other): overlapsBits(other);
  }

  //! Return true if this net ref represents the other net ref exactly.
  bool operator==(const NUNetRef& other) const;

  //! Return false if this net ref represents the other net ref exactly.
  bool operator!=(const NUNetRef& other) const {return !(*this == other);}

  //! Return true if this net ref covers another.
  /*!
   * This net ref covers another if every bit which the other net ref represents is
   * also represented in this net ref.
   *
   * \sa covered
   */
  bool covers(const NUNetRef& other) const;

  //! Return true if this net ref is covered by another.
  /*!
   * This net ref is covered by another if every bit which this net
   * ref represents is also represented in the other net ref.
   *
   * \sa covers
   */
  bool covered(const NUNetRef& other) const;

  //! Return true if this net ref is adjacent (in bits) to another.
  bool adjacent(const NUNetRef& other) const;

  //! Return true if this net ref represents the same range of bits as another
  /*!
   * Note that the nets referenced do not have to be equivalent, just that the bits
   * have to line up:
   *   wire [4:0] a;
   *   wire [0:4] b;
   * Then:
   *   a[4:3] is aligned with b[0:1]
   * but,
   *   a[0] is not aligned with b[0]
   */
  bool aligned(const NUNetRef& other) const;

  //! Create a list of lvalues suitable for assignment to the elements of this net ref
  void createLvalues(NUNetRefFactory* factory, const SourceLocator& loc,
                     UtList<NULvalue*>* lvalueList, bool blastVectors=false) const;

  //! debug help: return the type of what I'm looking at
  const char* typeStr() const;

  //! debug help: print routine
  void print(int indent = 0) const;

  //! Assert helper routine.
  void printAssertInfo() const;

  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param scope Build name based on specified scope,
    \param includeRoot if true then top of hierarchy (or specified scope) is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
    \param escapeIfMultiLevel if true then resulting name will be escaped if a hierarchical name was generated
    \param denormalize if true then returns the name of the net with denormalized indices.
  */
  void compose(UtString*, const STBranchNode* scope=NULL, bool includeRoot = true,
               bool hierName = true, const char* separator = ".",
               bool escapeIfMultiLevel = false,
               bool denormalize = false) const;

  //! Compose the unelaborated net name
  /*! Compose an unelaborated name for the net, based on the original
   *  module name and full path from the module to the net. This calls
   *  NUNet::composeUnelaboratedName and appends the usage bits for
   *  part selected vectors.
   */
  void composeUnelaboratedName(UtString* buf) const;

  //! Get the bitmask of the used bits of the net
  /*!
    \param usageMask This is modified to have a mask of all the used
    bits in the net. For each bit that is used a 1 is in the
    normalized position. For example, for completely used reg [3:6] d,
    the dynamic bit vector will be resized to 4, with its first 4 bits
    set to 1.
   */
  void getUsageMask(DynBitVector* usageMask) const;

  //! Append the bits used in this net-ref to the provided buffer.
  /*!
    If this net-ref is complete, nothing is added to the buffer.
    
    If this net-ref is incomplete, a string of the following form is
    appended to the provided buffer:

    [0, 5, 14:12, ..., 128:126]

    Each bit present is added to the buffer; this can get quite large
    for large sparse net-refs.

    \param buf    Buffer for appended result.
    \param indent Number of spaces to pre-pend on the buffer.
    \param normalizeVects  For vector nets, denormalize bit indices to
                  declared range.  Memory addresses are always denormalized

    \return true if buffer was modified.
   */
  bool printBits(UtString * buf, int indent, bool normalizeVects) const;

  //! loop over the contiguous ranges in the netRef
  NUNetRefRangeLoop loopRanges(NUNetRefFactory* factory) const;

  //! copy ctor must be public for HashValue to see it for factory management
  NUNetRef(const NUNetRef& src): mBits(src.mBits) {initNet(src.mNet);}

  //! Tests if two nets refs (with possibly different nets) have the same bits
  /*! If the two nets have different sizes then it returns
   *  false. Otherwise it matches the bits set and returns true if
   *  they are the same in both.
   */
  bool sameBits(const NUNetRef& other) const;

protected:
  //! Ensure that creation and deletion can only go through NUNetRefFactory and NUNetRefHdl.
  friend class NUNetRefFactory;
  friend class NUNetRefHdl;
  friend class NUBitNet;
  friend class NUVirtualNet;


  //! constructors
  NUNetRef() : mNet(0), mRefCnt(0) {}
  NUNetRef(NUNet *net, DynBitVectorFactory* factory);
  NUNetRef(NUNet *net, const DynBitVector& bv, DynBitVectorFactory* factory);
  NUNetRef(NUNet *net, SInt32 idx, DynBitVectorFactory* factory);
  NUNetRef(NUNet* net, const ConstantRange& r, DynBitVectorFactory* factory);
  //! create a NUNetRef for net using the subrange(s) specified in image.
  NUNetRef(NUNet* net, const NUNetRef& image);

  SInt32 getRefCnt() const { return mRefCnt; }

public:
  //! destructor
  /*!
   * The destructor needs to be public because we delete the net-refs
   * using the ::clearPointers method of our pool container.
   */
  ~NUNetRef();

private:
  //! disallow assign op
  NUNetRef& operator=(const NUNetRef&);

  //! Manipulate reference counts
  /*!
   * Protected so that only NUNetRefHdl can call.
   *
   * If NETREF_DEBUG is defined, then we keep around unique tokens per call to incCount(),
   * which allows tracing of unmatched decCount().
   */
#ifdef NETREF_DEBUG
  void printAliveTokens();
  void incCount(int *token) const;
  void decCount(int token) const;
  int obtainToken() const;
  void freeToken(int token) const;
  static void printAllAliveTokens();
  static void createTokenContainer();
  static void deleteTokenContainer();
  static bool queryToken(int token);
#else
  void incCount() const {++mRefCnt;}
  void decCount() const {--mRefCnt;}
#endif

  //! Merge this net ref with another.
  NUNetRef merge(const NUNetRef& other) const;

  //! Intersect this net ref with another.
  NUNetRef intersect(const NUNetRef& other) const;

  //! Subtract the parts of this net ref which overlap other
  NUNetRef subtract(const NUNetRef& other) const;

private:
  inline void initNet(NUNet* net) {
    mRefCnt = 0;
    if (mBits.empty())
      mNet = NULL;
    else
      mNet = net;
  }
  
  NUNetRef combine(DynBitVecDesc::CombineFn combine, 
                   const NUNetRef& other,
                   DynBitVectorFactory* factory) const;


  //! The net being referenced.
  NUNet *mNet;

  //! The bits that are set
  DynBitVecDesc mBits;

  //! The number of handles with references to this net ref.
  mutable SInt32 mRefCnt APIOMITTEDFIELD;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS ;  // class NUNetRef


//! Handle class for net refs
/*!
 * Net refs are reference counted, this is the handle class to use to access net refs.
 */
class NUNetRefHdl
{
public:
  //! constructor
  NUNetRefHdl(const NUNetRef* ref) : mNetRef(ref)
  {
#ifdef NETREF_DEBUG
    mNetRef->incCount(&mToken);
#else
    mNetRef->incCount();
#endif
  }

  //! default constructor
  NUNetRefHdl() : mNetRef(NULL)
  {
#ifdef NETREF_DEBUG
    mToken = 0;
#endif
  }

  //! destructor
  ~NUNetRefHdl()
  {
    if (mNetRef) {
#ifdef NETREF_DEBUG
      mNetRef->decCount(mToken);
#else
      mNetRef->decCount();
#endif
    }
  }

  //! copy
  NUNetRefHdl(const NUNetRefHdl& src)
  {
    if (src.mNetRef) {
#ifdef NETREF_DEBUG
      src.mNetRef->incCount(&mToken);
#else
      src.mNetRef->incCount();
#endif
    }
    mNetRef = src.mNetRef;
  }

  //! assign
  NUNetRefHdl& operator=(const NUNetRefHdl& src)
  {
    if (&src != this) {
      if (mNetRef) {
#ifdef NETREF_DEBUG
	mNetRef->decCount(mToken);
#else
	mNetRef->decCount();
#endif
      }
      if (src.mNetRef) {
#ifdef NETREF_DEBUG
	src.mNetRef->incCount(&mToken);
#else
	src.mNetRef->incCount();
#endif
      }
      mNetRef = src.mNetRef;
    }
    return *this;
  }

  //! dereference - return the underlying object
  const NUNetRef& operator*() const { return *mNetRef; }

  //! dereference - return the underlying object
  const NUNetRef* operator->() const { return mNetRef; }

  bool operator<(const NUNetRefHdl& ref) const
  {
    return (*mNetRef) < (*ref.mNetRef);
  }

  size_t hash() const {return mNetRef->hash();}

  //! Do to handles point to the same netref?
  bool operator==(const NUNetRefHdl & other) const { return mNetRef==other.mNetRef; }
  
  //! Are they different?
  bool operator!=(const NUNetRefHdl & other) const { return not(mNetRef==other.mNetRef); }

  //! Is this handle pointing to a valid netref?
  bool isValid () const { return mNetRef != 0; }

private:
  //! Underlying net ref
  const NUNetRef* mNetRef;

#ifdef NETREF_DEBUG
  //! To help debug, keep around tokens
  int mToken;
#endif

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS ; //class NUNetRefHdl


//! Factory to manage net refs
class NUNetRefFactory
{
public:
  //! constructor
  NUNetRefFactory();

  //! Create a reference to a whole net
  NUNetRefHdl createNetRef(NUNet *net);

  //! Create a reference to a bit net
  NUNetRefHdl createBitNetRef(NUNet *net);

  //! Create a reference to a whole vector net
  NUNetRefHdl createVectorNetRef(NUNet *net);

  //! Create a reference to a bit of a vector.
  /*!
   * The given idx is absolute, based on the net's declaration:
   *  wire [3:-6] a;
   *  VectorNetRef(a, -6) will represent the least significant bit
   */
  NUNetRefHdl createVectorNetRef(NUNet *net, SInt32 idx);

  //! Create a reference to a slice of a vector
  NUNetRefHdl createVectorNetRef(NUNet *net, const ConstantRange &r);

  //! Create a reference to a range of addresses in a memory
  NUNetRefHdl createMemoryNetRef(NUNet *net, const ConstantRange &r);

  //! Create a reference to a bit-slice of an already sliced vector.
  /*!
   * The specified index must specify a bit contained within the
   * original reference.
   *
   * \verbatim
   * result = original[idx]
   * \endverbatim
   */
  NUNetRefHdl sliceNetRef(const NUNetRefHdl& original, SInt32 idx) {
    return sliceNetRef(original, ConstantRange(idx,idx));
  }

  //! Create a reference to a slice of an already sliced vector.
  /*!
   * The specified range must specify a proper subset of the original
   * reference.
   *
   * \verbatim
   * result = original[range.msb,range.lsb];
   * \endverbatim
   */
  NUNetRefHdl sliceNetRef(const NUNetRefHdl& original, const ConstantRange& r);

  //! Create a reference to a whole memory
  NUNetRefHdl createMemoryNetRef(NUNet *net);

  //! Create a reference to one row of a memory
  /*!
   * The given idx is absolute, based on the net's declaration:
   *  reg [31:0] a [3:-6];
   *  MemoryNetRef(a, -6) will represent the least significant word
   */
  NUNetRefHdl createMemoryNetRef(NUNet *net, SInt32 idx);

  //! Create a reference to part of a net based on a netref for another net.
  NUNetRefHdl createNetRefImage(NUNet * net, const NUNetRefHdl & source);

  //! Create an empty reference
  NUNetRefHdl createEmptyNetRef() {return NUNetRefHdl(&mEmptyNetRef);}

  //! Trigger garbage collection; anything with ref count of 0 in the pool is deleted.
  void cleanup();

  //! Merge:  result = (one union two)
  NUNetRefHdl merge(const NUNetRefHdl& one, const NUNetRefHdl& two);

  //! Intersect:  result = (one intersect two)
  NUNetRefHdl intersect(const NUNetRefHdl& one, const NUNetRefHdl& two);

  //! Subtract:  result = (one minus two)
  NUNetRefHdl subtract(const NUNetRefHdl& one, const NUNetRefHdl& two);

  //! Erase any netrefs for the given NUNet
  /*!
   * Use this when a net is being deleted.
   */
  void erase(NUNet *net);

  //! debug help: print routine
  void print(int indent = 0) const;

  //! Apply usage mask -- usageMask typically comes from getUsageMask
  /*! This is only callable by the factory */
  NUNetRefHdl applyUsageMask(const NUNetRefHdl& netRef,
                             const DynBitVector& usageMask);

  //! destructor
  ~NUNetRefFactory();

  //! Debug routine to analyze all the netrefs in the factory to
  //! get a sense of how many have contiguous ranges
  void countContiguousRanges();

  //! Dump out a summary of the netrefs owned by the factory
  void printStats(int indent_arg=0);

  //! Get the bit-vector factory
  DynBitVectorFactory* getDynBitVectorFactory() {return &mBVFactory;}

private:
  //! Hide copy and assign constructors.
  NUNetRefFactory(const NUNetRefFactory&);
  NUNetRefFactory& operator=(const NUNetRefFactory&);

  //! Insert the given net ref into the pool.
  /*!
   * If the given net ref is already represented by another in the pool, then the
   * given will be deleted.
   */
  NUNetRefHdl insert(const NUNetRef& ref);

#if 0
  //! Create the least net ref possible for this net.
  /*!
   * The net ref returned will not be inserted into the pool.
   * Caller is responsible for deleting.
   */
  NUNetRef *createNetRefLower(NUNet *net);

  //! Create the greatest net ref possible for this net.
  /*!
   * The net ref returned will not be inserted into the pool.
   * Caller is responsible for deleting.
   */
  NUNetRef *createNetRefUpper(NUNet *net);
#endif

  DynBitVectorFactory mBVFactory;

  //! Comparison so we can use set to manage the pool
  class SetCompare
  {
  public:
    bool operator()(NUNetRef* one, NUNetRef* two) const
    {
      return NUNetRef::compare(*one, *two, false) < 0;
    }
  };

  typedef UtHashSet<NUNetRef,HashValue<NUNetRef> > RefSet;
  typedef UtHashMap<NUNet*,RefSet> NetRefMap;
  NetRefMap* mNetRefMap;

  NUNetRef mEmptyNetRef;
} ; // class NUNetRefFactory


//! class for iterating over the contiguous ranges of an NUNetRef
class NUNetRefRangeLoop {
public:
  //! constructor
  NUNetRefRangeLoop();

  //! constructor given a netRef
  NUNetRefRangeLoop(const NUNetRef* netRef, NUNetRefFactory* factory);

  //! destructor
  ~NUNetRefRangeLoop();

  //! copy ctor
  NUNetRefRangeLoop(const NUNetRefRangeLoop& src);

  //! assign op
  NUNetRefRangeLoop& operator=(const NUNetRefRangeLoop& src);

  //! increment
  void operator++();

  //! gets current range
  const ConstantRange& operator*() {return mCurrentRange;}

  // are we at the end of the iteration?
  bool atEnd() const {return !mValid;}

  //! gets a net-ref corresponding to the current range
  NUNetRefHdl getCurrentNetRef() {return mCurrentNetRef;}

private:
  void setup();

  NUNetRefHdl mNetRef;
  ConstantRange mCurrentRange;
  NUNetRefHdl mCurrentNetRef;
  bool mValid;
  NUNetRefFactory* mFactory;
}; // class NUNetRefRangeLoop

#endif
