// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _NUBLOCKINGMAPS_H_
#define _NUBLOCKINGMAPS_H_

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUNetRefMap.h"


//! NUBlockingMaps class
/*! This is an intermediate helper class to keep all the blocking
 *  use/def maps code in one place. It allows the derived classes to
 *  derive off this instead of NUUseDefNode if they need to manage
 *  only blocking use/def maps.
 */
class NUBlockingMaps : public NUUseDefNode
{
public:
  //! constructor
  NUBlockingMaps(NUNetRefFactory* netRefFactory);

  //! destructor
  ~NUBlockingMaps();

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(NUNet *net, NUNetSet *uses) const;
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Invalid to call; must use blocking or non-blocking interface
  void addDef(const NUNetRefHdl &net_ref);

  //! Invalid to call; must use blocking or non-blocking interface
  void addUse(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);

  //! Invalid to call; must use blocking or non-blocking interface
  void addUses(const NUNetRefHdl &def_net_hdl, NUNetRefSet *use_set);

  //! Invalid to call
  void replaceDef(NUNet *old_net, NUNet* new_net);

  //! Invalid to call
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Add the nets this node uses for blocking defs to the given set.
  void getBlockingUses(NUNetSet *uses) const;
  void getBlockingUses(NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to blockingly def the given net to the given set.
  void getBlockingUses(NUNet *net, NUNetSet *uses) const;
  void getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryBlockingUses(const NUNetRefHdl &def_net_ref,
			 const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the use_net as being used by the blocking def of def_net.
  void addBlockingUse(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);

  //! Add the net to the set of nets this stmt blockingly-defs.
  void addBlockingDef(const NUNetRefHdl &net_ref);

  //! Add the nets this node blockingly defs to the given set.
  void getBlockingDefs(NUNetSet *defs) const;
  void getBlockingDefs(NUNetRefSet *defs) const;
  bool queryBlockingDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const;

  //! Add the net to the set of blocking kills
  /*!
   * Keep track of kills for latch recognition
   */
  void addBlockingKill(const NUNetRefHdl &net_ref);

  //! Add the nets this node blockingly kills to the given set.
  void getBlockingKills(NUNetSet *kills) const;
  void getBlockingKills(NUNetRefSet *kills) const;
  bool queryBlockingKills(const NUNetRefHdl &def_net_ref,
			  NUNetRefCompareFunction fn,
			  NUNetRefFactory *factory) const;

  //! Add the nets this node uses for non-blocking defs to the given set.
  virtual void getNonBlockingUses(NUNetSet *uses) const = 0;
  virtual void getNonBlockingUses(NUNetRefSet *uses) const = 0;
  virtual bool queryNonBlockingUses(const NUNetRefHdl &use_net_ref,
				    NUNetRefCompareFunction fn,
				    NUNetRefFactory *factory) const = 0;

  //! Add the nets this node uses to non-blockingly def the given net to the given set.
  virtual void getNonBlockingUses(NUNet *net, NUNetSet *uses) const = 0;
  virtual void getNonBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const = 0;
  virtual bool queryNonBlockingUses(const NUNetRefHdl &def_net_ref,
				    const NUNetRefHdl &use_net_ref,
				    NUNetRefCompareFunction fn,
				    NUNetRefFactory *factory) const = 0;

  //! Add the net to the set of nets this stmt non-blockingly-defs.
  virtual void addNonBlockingDef(NUNet *net) = 0;
  virtual void addNonBlockingDef(const NUNetRefHdl &net_ref) = 0;

  //! Add the use_net as being used by the non-blocking def of def_net.
  virtual void addNonBlockingUse(NUNet *def_net, NUNet *use_net) = 0;
  virtual void addNonBlockingUse(const NUNetRefHdl &def_net_ref,
				 const NUNetRefHdl &use_net_ref) = 0;

  //! Add the nets this node non-blockingly defs to the given set.
  virtual void getNonBlockingDefs(NUNetSet *defs) const = 0;
  virtual void getNonBlockingDefs(NUNetRefSet *defs) const = 0;
  virtual bool queryNonBlockingDefs(const NUNetRefHdl &def_net_ref,
				    NUNetRefCompareFunction fn,
				    NUNetRefFactory *factory) const = 0;

  //! Add the net to the set of non-blocking kills
  virtual void addNonBlockingKill(NUNet *net) = 0;
  virtual void addNonBlockingKill(const NUNetRefHdl &net_ref) = 0;

  //! Add the nets this node non-blockingly kills to the given set.
  virtual void getNonBlockingKills(NUNetSet *kills) const = 0;
  virtual void getNonBlockingKills(NUNetRefSet *kills) const = 0;
  virtual bool queryNonBlockingKills(const NUNetRefHdl &def_net_ref,
				     NUNetRefCompareFunction fn,
				     NUNetRefFactory *factory) const = 0;


protected:
  //! Set of nets which this stmt blockingly-kills defs.
  NUNetRefSet mBlockingNetRefKillSet APIOMITTEDFIELD;

  //! Map of blocking defs to their "use"s
  NUNetRefNetRefMultiMap mBlockNetRefDefMap APIOMITTEDFIELD;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

private:
  //! Hide copy and assign constructors.
  NUBlockingMaps(const NUBlockingMaps&);
  NUBlockingMaps& operator=(const NUBlockingMaps&);
} APIOMITTEDCLASS; // class NUBlockingMaps : public NUUseDefNode

  

#endif // _NUBLOCKINGMAPS_H_
