// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUSTMT_H_
#define NUSTMT_H_

#include "nucleus/NUUseDefNode.h"
#include "util/SourceLocator.h"
#include "nucleus/NUElabBase.h"
#include "nucleus/NUCopyContext.h"
#include "nucleus/NUNetRefMap.h"

/*!
  \file
  Declaration of the Nucleus statement class.
*/

//! NUStmt class
/*!
 * Abstract class, models a statement.
 */
class NUStmt : public NUUseDefStmtNode
{
public:
  //! constructor
  /*!
    \param loc Source location.
  */
  NUStmt(bool usesCFNet, const SourceLocator& loc) :
    mLoc(loc),
    mUsesCFNet(usesCFNet)
  {}

  //! Get source location
  const SourceLocator& getLoc() const { return mLoc; }

  //! Mutate the location field
  void putLoc(const SourceLocator& src) {mLoc = src;}

  //! returns true if this needs the $cfnet in its use
  bool getUsesCFNet() const { return mUsesCFNet; }

  //! update the value of the usesCFNet flag
  void putUsesCFNet(bool new_val) { mUsesCFNet = new_val; }

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net) = 0;
  
  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net) = 0;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const {return 0;}

  //! Helper function to replace statements
  static void helperReplaceSubStmt(NUStmt*, NUStmt*, NUStmt*);

  //! Return a copy of this statement; recurses to copy nested statements.
  virtual NUStmt* copy(CopyContext &copy_context) const = 0;

  //! Is this statement identical to another
  virtual bool operator==(const NUStmt &) const = 0;

  //! destructor
  virtual ~NUStmt() {};

protected:
  //! Source location of the statement.
  SourceLocator mLoc;

  /*! \brief if true then this statement uses the $cfnet
   * 
   * This flag is used so that we can support the generation of
   * correct use information even when the use list is dynamically
   * caculated (such as for a simple assign).
   * The fact that a statement has the controlFlow net in it's "use"
   * list is a function of the context of the statement and not
   * necessarily the statement itself.  Thus this information must be
   * calculated during U/D generation, and is stored here.
   */
  bool mUsesCFNet APIOMITTEDFIELD;

private:
  //! Hide copy and assign constructors.
  NUStmt(const NUStmt&);
  NUStmt& operator=(const NUStmt&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUStmt : public NUUseDefStmtNode

//! Convenience function to remove a stmt and potentially replace with another
inline void RemoveReplaceStmtIfExists(NUStmtList &l,
				      NUStmt *stmt,
				      NUStmt *replacement=0)
{
  NUStmtList::iterator iter = l.begin();
  for (; iter != l.end(); ++iter) {
    if (*iter == stmt) break;
  }
  if (iter != l.end()) {
    if (replacement) {
      l.insert(iter, replacement);
    }
    l.erase(iter);
  }
}

//! make a copy of statements in \a source, append them to end of \a target
/*!
 * \param source source list
 * \param target destination list (this list is not cleared before statements are added to end)
 * \param copy_context context used for stmt copy
 * \param stmt_to_original optional arg, if not null then the mapping
 * of new stmts to original is added to this map.
*/
inline void deepCopyStmtList ( const NUStmtList & source, NUStmtList * target, 
                               CopyContext &copy_context,
                               NUStmtStmtMap * stmt_to_original=NULL )
{
  for ( NUStmtList::const_iterator iter = source.begin() ;
	iter != source.end() ;
	++iter ) {
    NUStmt* original = (*iter);
    NUStmt* stmt = original->copy(copy_context);
    target->push_back ( stmt );
    if ( stmt_to_original ) {
      (*stmt_to_original)[ stmt ] = original;
    }
  }
}

//! Delete the statements provided in the list and clear the list.
inline void deleteStmtList(NUStmtList& stmts)
{
  for (NUStmtList::iterator iter = stmts.begin();
       iter != stmts.end();
       ++iter) {
    delete (*iter);
  }
  stmts.clear();
}

#endif
