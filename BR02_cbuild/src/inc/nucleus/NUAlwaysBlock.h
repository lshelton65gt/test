// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUALWAYSBLOCK_H_
#define NUALWAYSBLOCK_H_

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUStructuredProc.h"
#include "util/Loop.h"

class SourceLocator;
class CopyContext;

/*!
  \file
  Declaration of the Nucleus always block class.
*/

//! NUAlwaysBlock class
/*!
 *  Model an always block.  Note that an always block really just has one
 *  statement, but a lot of the time that statement is a begin/end block.
 *  So, we just collapse that begin/end block into this always block, for
 *  convenience.
*/
class NUAlwaysBlock : public NUStructuredProc
{
public:
  enum NUAlwaysT {
    eAlways,                    // the default, plain ordinary always block (verilog, V2k, or SV)
    eAlwaysComb,                // SV only, from always_comb
    eAlwaysFF,                  // SV only, from always_ff
    eAlwaysLatch                // SV only, from always_latch
  };

  //! constructor
  /*!
    \param name  Name used for code generation.
    \param block Block which will contain the local variables and statements.
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param loc The source location of the always block.
    \param is_wild_card is true, when wild card is used in always block
    \param always_type distinguishes between a plain always (verilog, v2k, SV) or always_comb, always_ff, or always_latch (SV only)
   */
  NUAlwaysBlock(StringAtom * name,
                NUBlock *block,
		NUNetRefFactory *netref_factory,
		const SourceLocator& loc,
                bool is_wild_card,
                NUAlwaysT always_type = eAlways);
  
  //! Return the type of this class
  virtual NUType getType() const { return eNUAlwaysBlock; }

  //! Add the nets this always block uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this always block uses to def net to the given set.
  void getUses(NUNet *net, NUNetSet *uses) const;
  void getUses(const NUNetRefHdl& net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the edge uses from this block to the given set.
  void getEdgeUses(NUNetSet *uses) const;
  void getEdgeUses(NUNetRefSet *uses) const;
  bool queryEdgeUses(const NUNetRefHdl &use_net_ref,
                     NUNetRefCompareFunction fn,
                     NUNetRefFactory *factory) const;

  //! returns true if any edge exprs have been saved for this always block
  bool hasEdgeExprs();
  //! Add the given edge expression as something this always block is sensitive-to.
  void addEdgeExpr(NUEdgeExpr *expr) APIDISTILLMACRO2(APIEMITBIND(mEdgeExprs),
                                                      APIEMITCODE(mEdgeExprs.push_back(expr)));

  //! Add the given level (not edge) expression to the sensitivity list
  void addLevelExpr(NUExpr *expr)  APIDISTILLMACRO2(APIEMITBIND(mLevelExprs),
                                                    APIEMITCODE(mLevelExprs.push_back(expr)));

  //! Set the given block to be the priority block.
  void setPriorityBlock(NUAlwaysBlock *block);

  //! Return the priority block, may be 0.
  /*!
   * Priority block means the block which takes priority over this block.
   * This is used to model asynchronous resets; we split the always blocks,
   * but the scheduler must still order the execution correctly.
   */
  NUAlwaysBlock *getPriorityBlock() const;

  //! Set the given block to be the clock block.
  void setClockBlock(NUAlwaysBlock *block);

  //! Return the clock block, may be 0.
  /*!
   * Clock block is the lowest priority block for this block if this
   * is a priority block. This is essentially a back pointer for any
   * priority blocks.
   */
  NUAlwaysBlock *getClockBlock() const;
  
  //! Get the syncReset out of the edgeExprList
  NUEdgeExpr *getSyncReset();
  
  //! Remember that some always block has this always block as its clock ptr.
  void addClockBlockReferer(NUAlwaysBlock * referer);

  //! Remove the given always block from the referer set
  void removeClockBlockReferer(NUAlwaysBlock * referer);

  //! Get the set of always blocks referring to this one.
  void getClockBlockReferers(NUAlwaysBlockVector * referers) const;

  //! Is the specified block in the clock-block refererer set
  bool isClockBlockReferer(NUAlwaysBlock*) const;

  //! Does the specified block have a non-empty clock-block refererer set?
  bool hasClockBlockReferer() const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const;

  //! Return a UtString identifying this type.
  virtual const char* typeStr() const;

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);
  
  //! Is this block sequential (e.g. edge-triggered)
  virtual bool isSequential() const;

  //! Is this block similar to another one in terms of i/os & behavior?
  virtual bool isSimilar(const NUStructuredProc& other) const;

  //! Get the edge expression associated with this block.
  NUEdgeExpr* getEdgeExpr() const;

  //! Get the list of edge expressions associated with this block.
  /*!
   * After asynchronous reset handling, there will just be 1 element in this list.
   */
  void getEdgeExprList(NUEdgeExprList *edge_list) const;

  //! Remove the given edge expression from the internal list.
  /*!
   * The edge expression must exist in the list.
   * The given edge expression is not destroyed.
   */
  void removeEdgeExpr(NUEdgeExpr *edge);

  //! Replace the list of edge expressions with the given list.
  /*!
   * The given list is copied.
   * This is used in constant propagation
   * The caller is responsible to make sure that edge exprs are not leaked.
   */
  void replaceEdgeExprs(NUEdgeExprList* block_list);

  typedef Loop<NUEdgeExprList> EdgeExprLoop;
  /*
  typedef Loop<const NUEdgeExprList,
	       NUEdgeExprList::const_iterator> EdgeExprCLoop;
  */
  typedef CLoop<NUEdgeExprList> EdgeExprCLoop;
  
  //! Iterate over every edge expr.
  EdgeExprLoop loopEdgeExprs() { return EdgeExprLoop(mEdgeExprs); }

  //! Iterate over every edge expr.
  EdgeExprCLoop loopEdgeExprs() const { return EdgeExprCLoop(mEdgeExprs); }

  //! Can the specified input net stop flow through this always block, and withwhat boolean value?
  virtual bool stopsFlow(NUNet* inputNet, bool* val);

  //! is the block a priority block for another?
  virtual bool isPriorityBlock() const {return mClkBlock != NULL;}

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;
  
  //! Remove constants that are substituted into the edge list.
  //! Used for sync reset.  Disables blocks whose level guard signal is inactive.
  void removeConstantSyncReset();

  //! This block is being eliminated, so extract it out of the priority
  //! chain.  Returns new clock block (which might be changed by this routine).
  NUAlwaysBlock* removeFromPriorityChain();

  //! destructor
  ~NUAlwaysBlock();

  //! Make a copy of this always-block
  /*! Note: the original priority and clock-block pointers are retained
   *! as is, and must be fixed up by the caller, unless copy_prio_pointers is
   *! false.  In that case, the clock/prio pointers are left NULL in the clone.
   */
  NUAlwaysBlock* clone(CopyContext & copy_context, bool copy_prio_pointers)
    const;

  typedef Loop<NUExprList> ExprLoop;
  //! Iterate over every level expr.
  ExprLoop loopLevelExprs() { return ExprLoop(mLevelExprs); }
  //! Return the pointer to the level expression list
  NUExprList* getLevelExprList() {return &mLevelExprs; }

  //! Set the original flag for the level expressions
  void setLevelExprList(bool val) {mOrigLevelExprList = val;}
  //! Get the original flag for the level expressions
  bool isOrigLevelExprList() const {return mOrigLevelExprList;}

  //! Set the wild card flag for this always block
  void setWildCard() {mWildCardUsed = true; }
  //! Return the wild card flag of this always block
  bool isWildCard() const {return mWildCardUsed; }

  NUAlwaysT getAlwaysType() const {return mAlwaysType;}
  bool isAlwaysComb()  const {return mAlwaysType == eAlwaysComb;}
  bool isAlwaysFF()    const {return mAlwaysType == eAlwaysFF;}
  bool isAlwaysLatch() const {return mAlwaysType == eAlwaysLatch;}


private:
  //! Hide copy and assign constructors.
  NUAlwaysBlock(const NUAlwaysBlock&);
  NUAlwaysBlock& operator=(const NUAlwaysBlock&);

  //! List of sensitive edge expressions.
  NUEdgeExprList mEdgeExprs APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! List of level sensitive (not edge sensitive) expressions
  NUExprList     mLevelExprs APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  //! Priority block
  NUAlwaysBlock *mPrioBlock APIOMITTEDFIELD;

  //! Clock block
  /*! If this is a priority block of an always block then this pointer
   *  points back to the lowest priority block of the original
   *  block. This way we can get from any priority block to all the
   *  broken up pieces of the original sequential block. This block is
   *  the one that executes the clk condition statement.
   *
   *  For blocks that are not priority blocks, this pointer is NULL.
   */
  NUAlwaysBlock* mClkBlock APIOMITTEDFIELD;

  //! Pointers to always blocks with this always block as mClkBlock ptr.
  NUAlwaysBlockSet * mReverseClockBlock APIOMITTEDFIELD;

  //! This flag is used to prevent from processing level expression more than once.
  bool mOrigLevelExprList APIOMITTEDFIELD;

  //! This flag is true, if an always block uses wild card in the sensitivity list,
  //! like this, always &(*)
  bool mWildCardUsed;

  NUAlwaysT mAlwaysType;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUAlwaysBlock : public NUStructuredProc

// Handy sorting object.  Sort by source line, and disambiguate by
// name of always block to get a canonical order.

struct CmpAlwaysBlocks {
  bool operator()(const NUAlwaysBlock* a, const NUAlwaysBlock* b) const {
    SInt32 cmp = SourceLocator::compare(a->getLoc(), b->getLoc());
    if (cmp != 0)
      return cmp < 0;
    else
      return *(a->getName ()) < *(b->getName ()) ;
  }
};

#endif
