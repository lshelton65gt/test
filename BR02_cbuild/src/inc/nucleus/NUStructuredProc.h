// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Declaration of the Nucleus structured procedure (always, initial, ...)
*/

#ifndef _NUSTRUCTUREDPROC_H_
#define _NUSTRUCTUREDPROC_H_

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUNetRef.h"
#include "util/SourceLocator.h"

//! NUStructuredProc class
/*!
 *  Model a structured proc (initial block, always block).
 */
class NUStructuredProc : public NUUseDefNode
{
public:
  //! constructor
  /*!
    \param name  Name used for code generation.
    \param block Block which will contain the local variables and statements.
    \param netref_factory Factory to manage netref's - needed for u/d containers
    \param loc The source location of the block.
   */
  NUStructuredProc(StringAtom * name,
                   NUBlock *block,
		   NUNetRefFactory *netref_factory,
		   const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUStructuredProc; }

  //! Return the block for this structured procedure.
  NUBlock* getBlock() const { return mBlock; }

  //! Replace the block with a new one
  void replaceBlock(NUBlock* old_block, NUBlock* new_block)
  {
    NU_ASSERT(mBlock == old_block, this);
    mBlock = new_block;
  }
  
  //! Return the name of the structured procedure.
  /*!
   * A compiler-generated used for code generation.
   */
  StringAtom* getName() const;

  //! Return the source location of the structured procedure.
  const SourceLocator& getLoc() const;

  //! Mutate the location field
  void putLoc(const SourceLocator& src) {mLoc = src;}

  //! Clear the use/def information stored in the object, if any.
  void clearUseDef();

  //! Fixup the use/def information; remove all nets in the given list from the sets.
  void fixupUseDef(NUNetList *remove_list);

  //! Add the nets this structured procedure uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this structured procedure uses to def net to the given set.
  void getUses(NUNet *net, NUNetSet *uses) const;
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the set of nets this structured procedure defs to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the net as something this structured procedure defs.
  void addDef(const NUNetRefHdl &net_ref);

  //! Add the use_net as being used by the given def_net.
  void addUse(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);

  //! Add the use_set as being used by the given def_net.
  void addUses(const NUNetRefHdl &def_net_ref, NUNetRefSet *use_set);

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const;

  //! symbolic printing
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return a UtString identifying this type.
  virtual const char* typeStr() const;

  //! Is this block similar to another one in terms of i/os & behavior?
  virtual bool isSimilar(const NUStructuredProc& other) const = 0;

  //! Is this node a continuous driver?
  virtual bool isContDriver() const { return true; }

  //! Is c-model call (it has a c-model call statement)
  bool isCModelCall() const;

  //! Return the parent module
  const NUModule* findParentModule() const;
  NUModule* findParentModule();

  //! destructor
  virtual ~NUStructuredProc();

protected:
  //! Name of the block; used for code generation.
  StringAtom * mName;

  //! Block holding the statements and locally declared temporaries
  NUBlock * mBlock;

  //! Source location of this block.
  SourceLocator mLoc;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory APIOMITTEDFIELD;


private:
  //! Hide copy and assign constructors.
  NUStructuredProc(const NUStructuredProc&);
  NUStructuredProc& operator=(const NUStructuredProc&);

  //! Map of defs to their "use"s; uses net refs
  NUNetRefNetRefMultiMap* mNetRefDefMap APIOMITTEDFIELD;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUStructuredProc : public NUUseDefNode
#endif // _NUSTRUCTUREDPROC_H_
