// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NULVALUE_H_
#define NULVALUE_H_

#include "nucleus/NUCopyContext.h"
#include "nucleus/NUUseDefNode.h"
#include "util/SourceLocator.h"
#include "util/ConstantRange.h"
#include <cstddef>

class NUExpr;

/*!
  \file
  NULvalue and derived class declarations.
*/

//! NULvalue class
/*!
 * Abstract base class, models an expression which can be assigned a value;
 * either the LHS of some type of assignment or the receiving end of a port
 * connection.
 */
class NULvalue : public NUUseDefNode
{
public:
  //! constructor
  /*!
    \param loc Source location.
   */
  NULvalue(const SourceLocator& loc) { mLoc = loc; }

  //! Return the type of this class
  virtual NUType getType() const { return eNULvalue; }

  //! Get source location
  const SourceLocator& getLoc() const { return mLoc; }

  //! Return the verilog rule size of this lvalue.
  /*!
   * For lvalues, this will always be the natural size.
   */
  virtual UInt32 getBitSize() const { return determineBitSize(); }

  //! Is this a variable-sized lvalue?
  virtual bool sizeVaries () const;

  //! Return whether this lvalue is a real number or not
  /*! Only NUIdentLvalues have a chance of being real-valued.  The other
   *  deriviations can never be a real value.
   */
  virtual bool isReal() const { return false; }

  //! Return the size this lvalue "naturally" is.
  virtual UInt32 determineBitSize() const = 0;

  //! Resize this lvalue.
  /*!
   * Lvalues cannot be resized, but this sets the sizes on any expressions
   * in the lvalue (such as for dynamic bit selects).
   */
  virtual void resize() = 0;

  //! Return true if this lvalue access a single, complete, identifier, false otherwise.
  virtual bool isWholeIdentifier() const { return false; }

  //! Return true if this lvalue access involves a 2D net
  virtual bool is2DAnything () const { return false; }

  //! Return true if this is a simple expression accessing a hierref
  virtual bool isHierRef() const = 0;

  //! Get the NUNet that is on the left hand side.
  /*!
   * This call is only valid if you know that you have a whole identifier
   */
  virtual NUNet* getWholeIdentifier() const
  {
    NU_ASSERT("Not a whole identifier!" == NULL, this);
    return NULL;
  }

  //! Add the nets this node uses to the given set.
  virtual void getUses(NUNetSet *uses) const = 0;
  virtual void getUses(NUNetRefSet *uses) const = 0;
  virtual bool queryUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const = 0;

  //! Add the nets this node uses to def the given net to the given set.
  virtual void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const = 0;
  virtual bool queryUses(const NUNetRefHdl &def_net_ref,
			 const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const = 0;

  //! Add the nets this node defines to the given set.
  virtual void getDefs(NUNetSet *defs) const = 0;
  virtual void getDefs(NUNetRefSet *defs) const = 0;
  virtual bool queryDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const = 0;

  //! Add the nets this node completely defines to the given set.
  /*!
   * Complete def means that all bits of the net are guaranteed to have
   * a def:
   *  a = b;      // is a complete def
   *  a[foo] =b;  // not a complete def
   */
  virtual void getCompleteDefs(NUNetSet *defs) const = 0;
  virtual void getCompleteDefs(NUNetRefSet *defs) const = 0;
  virtual bool queryCompleteDefs(const NUNetRefHdl &def_net_ref,
				 NUNetRefFactory *factory) const = 0;

  //! Change this node to define new_net instead of old_net.
  virtual void replaceDef(NUNet *old_net, NUNet* new_net) = 0;

  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net) = 0;

  //! Create a RHS expression representing this LHS
  virtual NUExpr* NURvalue() const = 0;

  //! Does the RValue represent the same object
  virtual bool equalAddr(const NUExpr *rhs) const = 0;

  //! Return the parent module
  const NUModule* findParentModule() const;
  NUModule* findParentModule();

  //! destructor
  virtual ~NULvalue();

  //! hash an lvalue
  virtual size_t hash () const = 0;

  //! copy
  virtual NULvalue* copy(CopyContext &copy_context) const = 0;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const=0;
  //! Equivalence
  virtual bool operator==(const NULvalue&) const = 0;

  //! Compare two lvalues
  ptrdiff_t compare( const NULvalue &otherLvalue, bool cmpLocator,
                     bool cmpPointer ) const;

  //! Helper virtual function, which will always get an expr of the same type to cast
  virtual ptrdiff_t compareHelper(const NULvalue* otherLvalue, bool cmpLocator,
                                  bool cmpPointer ) const = 0;

protected:
  //! The source location of the lvalue.
  SourceLocator mLoc;

private:
  //! Hide copy and assign constructors.
  NULvalue(const NULvalue&);
  NULvalue& operator=(const NULvalue&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NULvalue : public NUUseDefNode

//! NUIdentLvalue class
/*!
 * Lvalue which is a simple Identifier
 */
class NUIdentLvalue : public NULvalue
{
public:
  //! constructor
  /*!
    \param net The identifier.
    \param loc Source location of the lvalue.
   */
  NUIdentLvalue(NUNet *net, const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUIdentLvalue; }

  //! Return the identifier.
  NUNet* getIdent() const { return mNet; }

  //! Return the size this lvalue "naturally" is.
  UInt32 determineBitSize() const;

  //! Resize this lvalue.
  void resize() {}

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node completely defines to the given set.
  void getCompleteDefs(NUNetSet *defs) const;
  void getCompleteDefs(NUNetRefSet *defs) const;
  bool queryCompleteDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Return true if this lvalue access a single, complete, identifier, false otherwise.
  bool isWholeIdentifier() const { return true; }

  //! Return true if this lvalue access involves a 2D net
  virtual bool is2DAnything () const;

  //! Return true if this is a simple expression accessing a hierref
  virtual bool isHierRef() const;

  //! Return true if the referenced net is a real number
  virtual bool isReal() const;

  //! Get the NUNet that is on the left hand side.
  /*!
   * This call is only valid if you know that you have a whole identifier
   */
  NUNet* getWholeIdentifier() const { return mNet; }

  //! Construct RHS corresponding to this LHS
  NUExpr* NURvalue() const;
  
  //! Does the RValue represent the same object
  bool equalAddr(const NUExpr *rhs) const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Equivalence
  virtual bool operator==(const NULvalue&) const;
  
  //! Helper virtual function, which will always get an expr of the same type to cast
  virtual ptrdiff_t compareHelper(const NULvalue* otherLvalue, bool compareLocator,
                                  bool comparePointer ) const;
  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return class name
  const char* typeStr() const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! copy
  virtual NULvalue* copy(CopyContext &copy_context) const;

  //! hash an lvalue
  virtual size_t hash () const;


  //! destructor
  ~NUIdentLvalue();


private:
  //! Hide copy and assign constructors.
  NUIdentLvalue(const NUIdentLvalue&);
  NUIdentLvalue& operator=(const NUIdentLvalue&);

  //! The identifier.
  NUNet *mNet;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUIdentLvalue : public NULvalue


//! NUVarselLvalue class
/*!
 * Lvalue which is a bit range select of a vector.  The bit range begins
 * at a bit offset specified by an expression which may be non-constant.
 *
 * This provides the semantic representation for bit-select, part-select
 * and variable part-select on the left-hand-side.
 */
class NUVarselLvalue : public NULvalue
{
public:
  //! constructor
  /*!
    \param net The identifier.
    \param expr The bit offset expression.
    \param range selection range
    \param loc Source location of the lvalue.
   */
  NUVarselLvalue(NUNet *net,
                 NUExpr *expr,
                 const ConstantRange& range,
                 const SourceLocator& loc);

  //! constructor
  /*!
    \param lvalue The lvalue to select from
    \param expr The bit offset expression.
    \param range selection range
    \param loc Source location of the lvalue.
   */
  NUVarselLvalue(NULvalue* lvalue,
                 NUExpr *expr,
                 const ConstantRange& range,
                 const SourceLocator& loc);

  /*! constructor (bit-select)
    \param net The identifier.
    \param expr The bit offset expression.
    \param loc Source location of the lvalue.
   */
  NUVarselLvalue(NUNet *net,
                 NUExpr *expr,
                 const SourceLocator& loc);

  /*! constructor (bit-select)
    \param lvalue Then lvalue to select from
    \param expr The bit offset expression.
    \param loc Source location of the lvalue.
   */
  NUVarselLvalue(NULvalue *lvalue,
                 NUExpr *expr,
                 const SourceLocator& loc);

  //! constructor (part-select)
  /*!
    \param net The identifier.
    \param range selection range
    \param loc Source location of the lvalue.
   */
  NUVarselLvalue(NUNet *net,
                 const ConstantRange& range,
                 const SourceLocator& loc);

  //! constructor (part-select)
  /*!
    \param lvalue Then lvalue to select from
    \param range selection range
    \param loc Source location of the lvalue.
   */
  NUVarselLvalue(NULvalue *lvalue,
                 const ConstantRange& range,
                 const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUVarselLvalue; }

  //! Get the identifier net for this select
  NUNet* getIdentNet(bool noArray=true) const;

  //! Return the identifier.
  NULvalue* getLvalue() const { return mIdent; }

  //! Return true if this is a select of a row of an array
  bool isArraySelect() const;

  //! Return the bitselect expression.
  NUExpr* getIndex() const { return mExpr; }

  //! Replace the index with a new one
  /*!
   * The caller is responsible to make sure that old cond is not leaked.
   */
  void replaceIndex(NUExpr* expr);

  //! Return the range of bits being selected.
  const ConstantRange* getRange() const { return &mRange; }

  //! Return the whether the index is constant
  bool isConstIndex() const;
  
  //! Return the constant index.  Only valid if isConstIndex()
  SInt32 getConstIndex() const;

  //! Return the size this lvalue "naturally" is.
  UInt32 determineBitSize() const;

  //! Resize this lvalue -- \sa putSize
  void resize();

  //! Resize this lvalue to the specified bit-size
  /*!
   *! When VHDL populates   v(a downto b) = x32;
   *! It doesn't know, from looking at the lvalue, how big it is.  But
   *! VHDL requires lvalue and rvalue to be the same size at runtime,
   *! otherwise a compliant simulator should raise an error.  Since Carbon
   *! is not a compliant simulator, we can improve our handling of this
   *! construct by assuming that a-b+1==32 at runtime, and sizing the lvalue
   *! to 32.  So VHDL population can call this code when constructing 
   *! assignments, when it knows the size of the rvalue and not the lvalue.
   *!
   *! This routine differs from resize(), which does not take an argument,
   *! and merely sets mExpr to its self-determined size.
   */
  void putSize(UInt32 bitSize);

  //! Is this virtually sized?
  virtual bool sizeVaries () const;

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Add the nets this node completely defines to the given set.
  void getCompleteDefs(NUNetSet *defs) const;
  void getCompleteDefs(NUNetRefSet *defs) const;
  bool queryCompleteDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefFactory *factory) const;

  //! Return true if this lvalue access a single, complete, identifier, false otherwise.
  bool isWholeIdentifier() const;

  //! Return true if this lvalue access involves a 2D net
  virtual bool is2DAnything () const { return mIdent->is2DAnything (); }

  //! Return true if this is a simple expression accessing a hierref
  virtual bool isHierRef() const;

  //! Construct a RHS representation of this LHS
  NUExpr * NURvalue() const;

  //! Does the RValue represent the same object
  bool equalAddr(const NUExpr *rhs) const;

  //! Get the NUNet that is on the left hand side.
  /*!
   * This call is only valid if you know that you have a whole identifier
   */
  NUNet* getWholeIdentifier() const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return class name
  const char* typeStr() const;

  //! destructor
  ~NUVarselLvalue();

  //! hash an lvalue
  virtual size_t hash () const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! copy
  virtual NULvalue* copy(CopyContext &copy_context) const;

  //! Equivalence
  virtual bool operator==(const NULvalue&) const;

  //! Helper virtual function, which will always get an expr of the same type to cast
  virtual ptrdiff_t compareHelper(const NULvalue* otherLvalue, bool compareLocator,
                                  bool comparePointer ) const;

  //! Merge adjacent destination bit references
  NULvalue* catenate (const NUVarselLvalue* v, CopyContext& cc) const;

  //! Is index expression guaranteed to be in bounds
  bool isIndexInBounds () const;

  //! set/clear bounds flag.
  void putIndexInBounds (bool b);

  //! Prints specified range into the buffer depending on whether the
  //! the select index is constant.
  static void printRange(UtString* buf, ConstantRange r, bool isConst);


private:
  //! Hide copy and assign constructors.
  NUVarselLvalue(const NUVarselLvalue&);
  NUVarselLvalue& operator=(const NUVarselLvalue&);

  bool fixConstExpr (void);

protected:

  //! The identifier.
  NULvalue* mIdent;

  //! The bitselect expression.
  NUExpr *mExpr;

  //! The constant range
  ConstantRange mRange;

  bool mIndexInBounds APIOMITTEDFIELD;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUVarselLvalue : public NULvalue


//! NUConcatLvalue class
/*!
 * Lvalue which is a concatenation.
 */
class NUConcatLvalue : public NULvalue
{
public:
  //! constructor
  /*!
    \param lvalues The lvalues, the vector will be copied.  0 is leftmost.
    \param loc Source location.
   */
  NUConcatLvalue(const NULvalueVector& lvalues,
		 const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUConcatLvalue; }

  //! Return the number of arguments to this concatenation.
  UInt32 getNumArgs() const;

  //! Return the specified argument; 0-based index, 0 is leftmost argument.
  NULvalue *getArg(UInt32 index) const;

  //! Update the specified index; 0-based index, 0 is leftmost argument
  /*!
   * Caller is responsible for memory management of old argument.
   * \return the old argument at the specified index.
   */
  NULvalue* setArg(UInt32 index, NULvalue * argument);

  //! Loop over the args
  NULvalueCLoop loopLvalues() const { return NULvalueCLoop(mLvalues); }
  NULvalueLoop loopLvalues() { return NULvalueLoop(mLvalues); }

  //! Return the size this lvalue "naturally" is.
  UInt32 determineBitSize() const;

  //! Resize this lvalue.
  void resize();

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Add the nets this node completely defines to the given set.
  void getCompleteDefs(NUNetSet *defs) const;
  void getCompleteDefs(NUNetRefSet *defs) const;
  bool queryCompleteDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefFactory *factory) const;

  //! Return true if this lvalue access a single, complete, identifier, false otherwise.
  bool isWholeIdentifier() const;

  //! Return true if this lvalue access involves a 2D net
  virtual bool is2DAnything () const;

  //! Return true if this is a simple expression accessing a hierref
  virtual bool isHierRef() const;

  //! Get the NUNet that is on the left hand side.
  /*!
   * This call is only valid if you know that you have a whole identifier
   */
  NUNet* getWholeIdentifier() const;

  //! Construct RHS matching LHS
  NUExpr* NURvalue() const;
  NUExpr* NURvalue(const SourceLocator &loc) const;

  //! Does the RValue represent the same object
  bool equalAddr(const NUExpr * /* rhs*/) const { return false; }

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return class name
  const char* typeStr() const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUConcatLvalue();

  //! hash an lvalue
  virtual size_t hash () const;

  //! copy
  virtual NULvalue* copy(CopyContext &copy_context) const;

  //! Equivalence
  virtual bool operator==(const NULvalue&) const;

  //! Helper virtual function, which will always get an expr of the same type to cast
  virtual ptrdiff_t compareHelper(const NULvalue* otherLvalue, bool compareLocator,
                                  bool comparePointer) const;


private:
  //! Hide copy and assign constructors.
  NUConcatLvalue(const NUConcatLvalue&);
  NUConcatLvalue& operator=(const NUConcatLvalue&);

protected:
  //! Vector of nested l-values.  0 is leftmost.
  NULvalueVector mLvalues;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUConcatLvalue : public NULvalue


//! NUMemselLvalue class
/*!
 * Lvalue which is an index into a memory.
 */
class NUMemselLvalue : public NULvalue
{
#if !pfGCC_2
  using NUUseDefNode::operator<;
#endif

public:
  //! constructor
  /*!
    \param net The identifier.
    \param expr The index into the memory.
    \param loc Source location of the lvalue.
   */
  NUMemselLvalue(NUNet *net,
                 NUExpr *expr,
                 const SourceLocator& loc);

  //! constructor
  /*!
    \param net The identifier.
    \param exprs Vector of ranges indexing into the memory.
    \param loc Source location of the lvalue.
   */
  NUMemselLvalue(NUNet *net,
                 NUExprVector *exprs,
                 const SourceLocator& loc);

  //! constructor
  /*!
    \param identifier The identifier
    \param expr The index into the memory.
    \param loc Source location.
   */
  NUMemselLvalue(NULvalue *identifier,
                 NUExpr *expr,
                 const SourceLocator& loc);

  //! constructor
  /*!
    \param identifier The identifier
    \param exprs Vector of ranges indexing into the memory.
    \param loc Source location.
   */
  NUMemselLvalue(NULvalue *identifier,
                 NUExprVector *exprs,
                 const SourceLocator& loc);

  //! Return the type of this class
  virtual NUType getType() const { return eNUMemselLvalue; }

  //! Return the identifier.
  NUNet* getIdent() const { return mIdent->getWholeIdentifier(); }

  //! Return the identifier.
  NULvalue * getIdentLvalue() const { return mIdent; }

  //! Return the mem select expression.
  NUExpr* getIndex(UInt32 dim) const { return mExprs[dim]; }

  //! Return the whether the index is constant
  bool isConstIndex(UInt32 dim) const;
  
  //! prepend another index expression onto the memsel's expression vector
  void prependIndexExpr( NUExpr *expr );

  //! Push another index expression onto the memsel's expression vector
  void addIndexExpr( NUExpr *expr );

  //! Return the number index expressions this memsel has
  UInt32 getNumDims() const { return mExprs.size(); }

  //! Replace the index with a new one
  /*!
   * The caller is responsible to make sure that old index is not leaked
   * and that the supplied index already exists.
   */
  void replaceIndex(UInt32 dim, NUExpr* expr);

  //! Return the size this lvalue "naturally" is.
  UInt32 determineBitSize() const;

  //! Resize this lvalue.
  void resize();

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node uses to def the given net to the given set.
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  bool replaceLeaves(NuToNuFn & translator);

  //! Add the nets this node completely defines to the given set.
  void getCompleteDefs(NUNetSet *defs) const;
  void getCompleteDefs(NUNetRefSet *defs) const;
  bool queryCompleteDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefFactory *factory) const;

  //! Return true if this lvalue access a single, complete, identifier, false otherwise.
  bool isWholeIdentifier() const;

  //! Return true if this lvalue access involves a 2D net
  /*! Memsels are always 2d */
  virtual bool is2DAnything () const { return true; }

  //! Return true if this is a simple expression accessing a hierref
  virtual bool isHierRef() const;

  //! Get the NUNet that is on the left hand side.
  /*!
   * This call is only valid if you know that you have a whole identifier
   */
  NUNet* getWholeIdentifier() const;

  //! Construct a RHS corresponding to this LHS
  NUExpr* NURvalue() const;

  //! Does the RValue represent the same object
  bool equalAddr(const NUExpr *rhs) const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Dump myself to cout
  void print(bool recurse, int indent) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Return class name
  const char* typeStr() const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! destructor
  ~NUMemselLvalue();

  //! copy
  virtual NULvalue* copy(CopyContext &copy_context) const;

  //! hash function for making canonical sets of these
  virtual size_t hash() const;

  //! ordered comparison of two memsellvalues.
  static ptrdiff_t compare(const NUMemselLvalue* m1, const NUMemselLvalue* m2);

  //! comparison function for making canonical sets of these
  bool operator==(const NUMemselLvalue&) const;

  //! Equivalence
  virtual bool operator==(const NULvalue&) const;

  //! Helper virtual function, which will always get an expr of the same type to cast
  virtual ptrdiff_t compareHelper(const NULvalue* otherLvalue, bool compareLocator,
                                  bool comparePointer) const;

  //! sorting comparision function
  bool operator<(const NUMemselLvalue&) const;

  //! Has this memsel been through multidimensional resynthesis?
  bool isResynthesized() const {return mResynthesized; }

  //! set the resynthesis flag
  void putResynthesized( bool resynth ) { mResynthesized = resynth; }

private:
  //! hide copy and assign constructors
  NUMemselLvalue(const NUMemselLvalue&);
  NUMemselLvalue& operator=(const NUMemselLvalue&);

  //! The identifier, expected to only be a full identifier of a memory net.
  NULvalue *mIdent;

  //! The array of select expressions
  NUExprVector mExprs;

  bool mResynthesized APIOMITTEDFIELD; // set to true when this NUMemselLvalue has been through MemoryResynthesis

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUMemselLvalue : public NULvalue


#endif
