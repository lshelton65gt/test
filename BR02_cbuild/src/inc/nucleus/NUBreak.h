// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef __NUBreak_h_
#define __NUBreak_h_

#include "nucleus/NUBlockStmt.h"

/*!
  \file
  Declaration of the Nucleus 'break' class.  This class is designed for
  population of VHDL exit/return/next, and Verilog disable.  The nucleus
  representation contains an NUBlock*, which points to the block that we
  want to break out of.

  It is up to population to associate the 'break' with the block we
  are breaking from.

  By the time constant propagation & TFRewrite occurs, there should be
  no NUBreak statements.  They should be resynthesized using 'enable'
  nets and 'if' statements, so that existing Nucleus transformations
  can deal with them appropriately.
*/

//! NUBreak class
/*!
 * Models a 'break' statement.
 */
class NUBreak : public NUBlockStmt
{
public:
  //! constructor
  /*!
    \param block the block we are breaking out of
    \param name the name of that block (NUBlock does not know the name)
    \param netRefFactory the NUNetRefFactory
    \param loc Source location.
  */
  NUBreak(NUBlock* block,
          StringAtom* keyword,  // disable, exit, return,...
          StringAtom* targetName,
          NUNetRefFactory* netRefFactory,
          const SourceLocator& loc);

  //! destructor
  virtual ~NUBreak();

  //! Change this node to define new_net instead of old_net.
  virtual void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net);

  //! Return a copy of this statement; recurses to copy nested statements.
  virtual NUBreak* copy(CopyContext &copy_context) const;

  //! Equivalence
  virtual bool operator==(const NUStmt &) const;

  //! Get the target block
  NUBlock* getTarget() {return mTarget;}

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  //! Dump myself to cout
  virtual void print(bool recurse, int indent) const;

  //! Return class name
  virtual const char* typeStr() const;

  //! calculate costs, add to NUCost arg
  virtual void calcCost(NUCost*, NUCostContext*) const;

  //! Get the type of this use def
  virtual NUType getType() const;

  //! Get the name of the block
  StringAtom* getTargetName() const {return mTargetName;}

  //! Get the name of the keyword
  StringAtom* getKeyword() const {return mKeyword;}

private:
  //! Hide copy and assign constructors.
  NUBreak(const NUBreak&);
  NUBreak& operator=(const NUBreak&);

  NUBlock* mTarget;
  StringAtom* mTargetName; // NUBlock doesn't store its name, so keep it here.
  StringAtom* mKeyword;    // disable, exit, next, return...

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUBreak;

#endif
