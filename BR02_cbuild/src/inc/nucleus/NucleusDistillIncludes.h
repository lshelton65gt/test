// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

//This file is used as input to gccxml
//This file is included by src/nucleus/NucleusDistilledClasses.cxx


#ifndef __NUCLEUSDISTILLINCLUDES_H__
#define __NUCLEUSDISTILLINCLUDES_H__ 


#include "nucleus/Nucleus.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUBase.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUBitNetHierRef.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUBlockingMaps.h"
#include "nucleus/NUBlockStmt.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUCModelArgConnection.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModelFn.h"
#include "nucleus/NUCModel.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUCModelPort.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUCompositeNetHierRef.h"
#include "nucleus/NUControlFlowNet.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUElabBase.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUExtNet.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetRefMap.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUSysFunctionCall.h"
#include "nucleus/NUSysRandom.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUSysTaskNet.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUUseNode.h"
#include "nucleus/NUVectorNetHierRef.h"
#include "nucleus/NUVirtualNet.h"

#endif
