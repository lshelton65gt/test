// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NUHIERREF_H_
#define NUHIERREF_H_

#include "util/StringAtom.h"
#include "nucleus/Nucleus.h"
#include "util/LoopFunctor.h"
#include "util/UtArray.h"

class STSymbolTableNode;
class STBranchNode;
class STSymbolTable;
class HdlHierPath;

//! NUHierRef class
/*!
 * This class contains path information for hierarchical references.
 *
 * For example, to represent the following hierarchical reference:

 module parent();
   wire w;
   assign w = child1.o;
   child child1();
 endmodule

 * The path is "child1".
 */
class NUHierRef
{
public:
  //! ctor
  NUHierRef(const AtomArray& path);

  //! dtor
  virtual ~NUHierRef();

#ifdef CDB
  // Debug hooks cause gcc 3.x makes it hard to break in constructors
  // and destructors
  void ctor();
  void dtor();
#endif

  //! Loop over possible resolutions
  NUBaseVectorLoop loopResolutions();

  //! Loop over possible resolutions (const version)
  NUBaseVectorCLoop loopResolutions() const;

  //! Return the number of resolutions
  int resolutionCount() const;

  //! Print possible resolutions
  void printPossibleResolutions(int indent=0) const;

  //! Return true if this hierref can be resolved locally
  /*! A hier ref is locally resolved if it is a reference into a
   *  child.
   *
   * \param instPath Optional, if given, will be populated with the
   *                 NUModuleInstances in the path to the local hier
   *                 ref, if this can be resolved locally.
   *
   * \param node Optional, if given, will be populated with the symbol
   *             table entry of the resolved object, if this can be
   *             resolved locally.
   *
   * \return true if this hierref can be resolved locally.
   */
  bool isLocallyRelative(NUModuleInstanceVector* instPath = NULL,
                         STSymbolTableNode** node = 0) const;

  //! Return true if this hierref can be resolved as local to the provided module
  /*! A hier ref is locally resolved if it is a reference into a
   *  child.
   *
   * \param module The module used as the root of the search.
   *
   * \param instPath Optional, if given, will be populated with the
   *                 NUModuleInstances in the path to the local hier
   *                 ref, if this can be resolved locally.
   *
   * \param node Optional, if given, will be populated with the symbol
   *             table entry of the resolved object, if this can be
   *             resolved locally.
   *
   * \return true if this hierref can be resolved locally.
   */
  bool isLocallyRelativeToModule(const NUModule * module,
                                 NUModuleInstanceVector* instPath = NULL,
                                 STSymbolTableNode** node = 0) const;

  //! Add the given object as a possible resolution to this hier ref.
  /*! The object can be either an NUNet* or NUTask*. We use NUBase* to
   *  generalize the code.
   */
  void addResolution(NUBase* obj) APIDISTILLMACRO2(APIEMITBIND(mResolutionVector),
                                                   APIEMITCODE(mResolutionVector.push_back(obj)));




  /*!
    \param old_resolution A current resolution
    \param old_instance Instance which the new resolution is replacing
    \returns true if the new resolution should be used, false otherwise.
    
    We have created a uniquified version of old_resolution as
    new_resolution. Two scenarios are possible:
    
    1) This hierref is locally relative. In this case, we remove the
       old resolution and expect the caller to properly hookup the new
       resolution. Returns 'true'.

    2) This hierref is not locally relative. In that case, keep the
       old resolution and expect the caller to properly hookup the new
       resolution. Since we are operating unelaboratedly, we cannot
       determine which one will really be used.
    */
  bool fixupResolution(NUBase* old_resolution,
                       NUModuleInstance* old_instance);

  //! Remove the given object from being a possible resolution to this hier ref.
  void removeResolution(NUBase* net);

  //! Replace the resolutions with a new set
  void replaceResolutions(const NUBaseVector& newResolutions);

  //! Return true if this hier ref and the other hier ref can resolve to the same object.
  /*!
   * Used during flattening when 'inst' is being flattened into
   * its parent module. This routine checks if the given child hierref
   * (which is inside the instance) is the same as this hierref.
   *
   * \param do_consistency_check Conditionally check if the resolutions
   *        for the two hierrefs are compatible. If this hierref is
   *        already part of the parent module, it should have a valid
   *        set of resolutions. If this hierref is part of the current
   *        flattening operation, its resolutions haven't been
   *        populated yet. Pass 'true' when the resolutions are valid
   *        and should be checked.
   */
  bool sameResolutionAsChild(const NUHierRef *child, 
                             NUModuleInstance *inst, 
                             bool do_consistency_check) const;

  //! Static function to return the symbol table node for a hier ref
  /*! This function uses the Verilog semantics to resolve a
   *  hierarchical reference to the correct net/task/etc. instance.
   *
   *  This function works for both unelaborated and elaborated
   *  resolutions.  When calling unelabortedly, the caller needs to
   *  specify a given_module parameter.
   *
   *  \param path The hierarchical path to the object.
   *
   *  \param symTab The elaborated symbol table to search.
   *
   *  \param startNode The starting scope to start the search.
   *  If 0, will start at symtab root.
   *
   *  \param given_module If non-zero, this module will be used to get
   *  child instance names when doing name lookups.  Only give this when
   *  doing unelaboration resolutions.
   *
   *  \param isParseTable If true, it is assumed that symTab is not
   *  the 'real' symbol table produced during design elaboration, but
   *  instead is the one created by iodb/IODBParse.cxx, which has a
   *  slightly different BOM format, due to the lack of Elab nucleus
   *  structures.
   *
   *  \returns an STSymbolTableNode that represents the object
   *  instance or 0 if it can't be resolved. Although a 0 return
   *  really is unexpected because Cheetah should catch these.
   */
  static STSymbolTableNode* resolveHierRef(const AtomArray& path,
                                           UInt32 pathIndex,
                                           STSymbolTable* symTab,
                                           STBranchNode* startNode,
                                           const NUModule *given_module = 0,
                                           bool isParseTable = false);
					   
  //! Localize the name of this hierref for use in the context of an immediate parent.
  /*!
    \param inst_name Instance name in parent
    \param localized_name Will be modified to have the localized name

    As an example:

    \code
    module parent();
      child c ();
    endmodule
    module child();
      wire w = l.bar;
      leaf l ();
    endmodule
    module leaf();
      reg bar;
    endmodule
    \endcode

    If child is flattened into parent, then this routine can be called with
    \code
    child_l_bar->localizeNameForParent("c", &name);
    \endcode
    And then name will have the string "c.l.bar"
   */
  void localizeNameForParent(NUScope* scope,
                             StringAtom* inst_name,
                             AtomArray* localized_name) const;

  //! Print info when an NU_ASSERT occurs
  virtual void printAssertInfo() const = 0;

  //! Get the path as specified by the user
  const AtomArray& getPath() const { return mPath; }

  //! Mutate existing hier-ref to have alternate path
  void putPath(const AtomArray& path);

  //! Get the path as specified by the user, as a string
  void getName(UtString*, HdlHierPath& hdl) const;

  //! Generate a suitable name to use for a nucleus obj that's a hier ref.
  /*! The name of an NUNet that is a hier-ref net is not the path
   *! of the hierarchical reference.  (It used to be).  It's a gensym
   *! name.  The path is kept separately as an array of StringAtom*.
   *! But we use the whole path for the gensym for user semi-friendliness.
   */
  static StringAtom* gensym(NUScope* scope, const AtomArray& path);

  //! Build an AtomArray based on a symbol table node
  /*! The AtomArray is not cleared by this routine.  This routine
   *! appends to an existing atomArray.
   */
  static void buildAtomArray(AtomArray* atomArray, STSymbolTableNode* node);

  //! Print the contents of the path
  void print() const;

  //! return the subtype name of this hier-ref
  virtual const char* typeStr() const = 0;

  //! generate a name suitable for emitting as a hier-ref name in C code
  void composeCPath(UtString* buf) const;

  //! Remove this as a reference to its possible resolutions.
  void cleanupResolutions();

protected:
  //! Get the parent scope for this hierarchical reference
  virtual const NUModule* getModule() const = 0;

  //! Return the object which uses this object
  /*! This function is abstract virtual because the data is stored in
   *  the derived class so that it can retain the true type.
   */
  virtual NUBase* getObject() const = 0;

  //! Path to the referenced task/net, including task/net name
  AtomArray mPath;

private:
  //! Hide copy and assign constructors.
  NUHierRef(const NUHierRef&);
  NUHierRef& operator=(const NUHierRef&);

  //! Possible resolutions of this reference.
  NUBaseVector mResolutionVector APIDISTILLMACRO1(APICONSTRUCTORFALSE);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; // class NUHierRef

//! Hierarchical reference to a task
class NUTaskHierRef : public NUHierRef
{
public:
  //! constructor
  NUTaskHierRef(NUTaskEnable *enable, const AtomArray& path, StringAtom *taskName);

  //! destructor
  ~NUTaskHierRef();

  //! Return the string version of the task name
  StringAtom *getTaskName() const { return mPath[0]; }

  /*!
   * Coercion functor from NUBase to NUTask so loopResolutions can
   * expose the correct type.
   */
  class CoerceBaseToTask {
  public: 
    typedef NUTask * value_type;
    typedef NUTask * reference;
    NUTask * operator()(NUBase * node) const; 
    const NUTask * operator()(const NUBase * node) const; 
  };
  typedef LoopFunctor<NUBaseVectorLoop,CoerceBaseToTask>  TaskVectorLoop;
  typedef LoopFunctor<NUBaseVectorCLoop,CoerceBaseToTask> TaskVectorCLoop;

  //! Loop over possible resolutions
  TaskVectorLoop loopResolutions();

  //! Loop over possible resolutions (const version)
  TaskVectorCLoop loopResolutions() const;

  //! Return true if this hierref can be resolved locally
  /*! A hier ref is locally resolved if it is a reference into a
   *  child.
   *
   * \param instPath Optional, if given, will be populated with the
   *                 NUModuleInstances in the path to the local hier
   *                 ref, if this can be resolved locally.
   *
   * \param resolved_task Optional, if given, will be populated with
   *                      the resolved task, if this can be resolved
   *                      locally.
   *
   * \return true if this hierref can be resolved locally.
   */
  bool isLocallyRelative(NUModuleInstanceVector* instPath = NULL,
                         NUTask **resolved_task = 0) const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Return the object which uses this object
  /*! This function is abstract virtual because the data is stored in
   *  the derived class so that it can retain the true type.
   */
  NUBase* getObject() const;

  //! Return one of the potential resolutions as representative.
  /*!
   * If this task enable has multiple resolutions but you just want
   * one to treat as a representative, this method will choose one
   * potential resolution and return it. This is useful for
   * use/def-type tasks.
   *
   * Generally, you want to get the unique elaborated resolution. See
   * isLocallyRelative and related methods.
   *
   * If you want all resolutions, see loopResolutions.
   *
   * \return One (of potentially) resolution task.
   */
  NUTask * getRepresentativeResolution() const;

  //! Print info when an NU_ASSERT occurs
  virtual void printAssertInfo() const;

  //! return the type of this object
  virtual const char* typeStr() const;

protected:
  //! Get the parent scope for this hierarchical reference
  const NUModule* getModule() const;

private:
  //! Hide copy and assign constructors.
  NUTaskHierRef(const NUTaskHierRef&);
  NUTaskHierRef& operator=(const NUTaskHierRef&);

  //! Get the task enable which uses this object
  const NUTaskEnable* getTaskEnable() const { return mTaskEnable; }

  //! Task enable which uses this object
  NUTaskEnable *mTaskEnable;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS ; // class NUTaskHierRef : public NUHierRef


//! Hierarchical reference to a net
/*!
 * This class exists so that we can get code reuse; it consolidates the
 * hierarchical resolution handling for nets.
 *
 * All the hierarchical net reference classes will contain one of these.
 */
class NUNetHierRef : public NUHierRef
{
public:
  //! Constructor
  /*!
    \param net Net which uses this object
   */
  NUNetHierRef(NUNet *net, const AtomArray& path) :
    NUHierRef(path),
    mNet(net)
  {}

  //! Destructor
  ~NUNetHierRef();

  /*!
   * Coercion functor from NUBase to NUNet so loopResolutions can
   * expose the correct type.
   */
  class CoerceBaseToNet {
  public: 
    typedef NUNet * value_type;
    typedef NUNet * reference;
    NUNet * operator()(NUBase * node) const;
    const NUNet * operator()(const NUBase * node) const;
  };
  typedef LoopFunctor<NUBaseVectorLoop,CoerceBaseToNet>  NetVectorLoop;
  typedef LoopFunctor<NUBaseVectorCLoop,CoerceBaseToNet> NetVectorCLoop;

  //! Loop over possible resolutions
  NetVectorLoop loopResolutions();

  //! Loop over possible resolutions (const version)
  NetVectorCLoop loopResolutions() const;

  //! Return true if this hierref can be resolved locally
  /*! A hier ref is locally resolved if it is a reference into a
   *  child.
   *
   * \param instPath Optional, if given, will be populated with the
   *                 NUModuleInstances in the path to the local hier
   *                 ref, if this can be resolved locally.
   *
   * \param resolved_net Optional, if given, will be populated with the
   *                     resolved net, if this can be resolved locally.
   *
   * \return true if this hierref can be resolved locally.
   */
  bool isLocallyRelative(NUModuleInstanceVector* instPath = NULL,
                         NUNet **resolved_net = 0) const;

  //! Return the NUNet which uses this object
  NUNet* getNet() { return mNet; }
  const NUNet* getNet() const { return mNet; }

  //! If this is locally relative, return the resolved net, otherwise return 0.
  NUNet *getLocallyResolvedNet() const;

  //! Code Generator
  CGContext_t emitCode (CGContext_t) const;

  //! Print info when an NU_ASSERT occurs
  virtual void printAssertInfo() const;

  //! return the type of this object
  virtual const char* typeStr() const;

protected:
  //! Get the parent scope for this hierarchical reference
  const NUModule* getModule() const;

  //! Return the object which uses this object
  /*! This function is abstract virtual because the data is stored in
   *  the derived class so that it can retain the true type.
   */
  NUBase* getObject() const;

private:
  //! Hide copy and assign
  NUNetHierRef(const NUNetHierRef&);
  NUNetHierRef& operator=(const NUNetHierRef&);

  //! Net which uses this object
  NUNet *mNet;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS ; //class NUNetHierRef : public NUHierRef

#endif
