// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUCOMPOSITELVALUE_H_
#define NUCOMPOSITELVALUE_H_

#include "nucleus/NULvalue.h"
#include "nucleus/NUCompositeNet.h"

//! NUCompositeLvalue class
/*!
 * This class represents an aggregate (like a concat) of a composite
 * type.  Its elements may be any valid object, including nested
 * NUCompositeLvalue nodes.
 */
class NUCompositeLvalue: public NUConcatLvalue
{
public:
  //! constructor
  /*!
    \param lvalues The lvalues, the vector will be copied.  0 is leftmost.
    \param loc Source location.
   */
  NUCompositeLvalue( const NULvalueVector& lvalues, const SourceLocator& loc );

  virtual NUType getType() const { return eNUCompositeLvalue; }

  //! Return class name
  const char* typeStr() const;

  //! Indicate whether this is an array concat expr
  void setIsCompositeArray(bool isCompositeArray);

  //! Is this an array concat expr
  bool isCompositeArray() const;

  //! Method used to make a copy of NUCompositeLvalue.
  NULvalue* copy(CopyContext &copy_context) const;

private:
  bool mIsCompositeArray;

  //! Hide copy and assign constructors.
  NUCompositeLvalue(const NUCompositeLvalue&);
  NUCompositeLvalue& operator=(const NUCompositeLvalue&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS ; //class NUCompositeLvalue: public NUConcatLvalue


//! NUCompositeInterfaceLvalue class
/*!
 * This class defines the interface to access the contents of anything
 * that represents a record.  NUCompositeSelLvalue, and
 * NUCompositeFieldLvalue and NUCompositeIdentLvalue are all derived from
 * this class.
 */

class NUCompositeInterfaceLvalue : public NULvalue
{
public:
  NUCompositeInterfaceLvalue( const SourceLocator& loc );
  virtual ~NUCompositeInterfaceLvalue();

  enum CompositeType
  {
    eCompositeIdentLvalue,
    eCompositeSelLvalue, 
    eCompositeFieldLvalue,
    eCompositeIdentRvalue,
    eCompositeSelExpr,
    eCompositeFieldExpr
  };

  virtual CompositeType getCompositeType() const = 0;

  /*! \brief Return the number of fields in the composite object
   */
  virtual UInt32 getNumFields () const = 0;

  //! Return the Nth field net pointer
  virtual NUNet *getField( UInt32 fieldIndex ) const = 0;

  //! Set the Nth field net pointer.  Does no memory management.
  //virtual void putField( NUNet *new_net, UInt32 fieldIndex ) = 0;

  //! Get the underlying NUComposite expression for this object
  virtual NUCompositeInterfaceLvalue *getIdentExpr() const = 0;

  //! Get the number of ranges stored in this object
  virtual UInt32 getNumDims() const = 0;

  //! Get the Nth range stored in this object
  virtual const ConstantRange *getRange( const UInt32 index ) const = 0;

  //! Get the size of the base object of this composite.
  virtual UInt32 getCompositeSize() const = 0;

  //! Get the underlying NUCompositeNet, no matter how deeply nested
  virtual NUCompositeNet *getCompositeNet() const = 0;

    /* \brief get the NUNet corresponding to this composite lvalue
   *
   * This routine must be used with caution; what it actually returns is
   * NOT the precise net that will be referenced.  That net cannot be
   * determined, and will not even exist, until during post-population
   * resynthesis.  Hovever, there are several cases where it is very
   * useful to know what kind of net, at least whether it is a
   * composite/noncomposite net, a composite structure refers to.  In
   * particular, we need this information during population of a
   * bit/part select of a vector inside a record.
   */
  virtual NUNet *getNet() const = 0;

  // NUUseNode virtual functions
  //! Add the set of nets this construct uses to the given set.
  virtual void getUses(NUNetSet *uses) const;
  virtual void getUses(NUNetRefSet *uses) const;
  virtual bool queryUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
                         NUNetRefFactory *factory) const;
  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net);
  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
  */
  virtual bool replaceLeaves(NuToNuFn & translator);
  virtual void calcCost(NUCost*, NUCostContext* = 0) const;

  // NUUseDef virtual functions
  //! Add the nets this node uses to def the given net to the given set.
  virtual void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  virtual bool queryUses(const NUNetRefHdl &def_net_ref,
                         const NUNetRefHdl &use_net_ref,
                         NUNetRefCompareFunction fn,
                         NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  virtual void getDefs(NUNetSet *defs) const;
  virtual void getDefs(NUNetRefSet *defs) const;
  virtual bool queryDefs(const NUNetRefHdl &def_net_ref,
                         NUNetRefCompareFunction fn,
                         NUNetRefFactory *factory) const;

  // NULvalue virtual functions
  //! Return true if this is a simple expression accessing a hierref
  virtual bool isHierRef() const;
  virtual void getCompleteDefs(NUNetSet *defs) const;
  virtual void getCompleteDefs(NUNetRefSet *defs) const;
  virtual bool queryCompleteDefs(const NUNetRefHdl &def_net_ref,
				 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  virtual void replaceDef(NUNet *old_net, NUNet* new_net);

  virtual size_t hash() const;
  virtual ptrdiff_t compareHelper(const NULvalue*, bool, bool) const;
  //! Create a RHS expression representing this LHS
  virtual NUExpr* NURvalue() const;

  //! Does the RValue represent the same object
  virtual bool equalAddr(const NUExpr *rhs) const;

  //! copy
  virtual NULvalue* copy(CopyContext &copy_context) const;

  //! Returns true if the lvalue or one of it's parent composites is indexed.
  /*!
    Populates the selVector with indices if provided.
  */
  bool hasSelects(NUExprVector* selVector = NULL) const;

private:
  //! Hide copy and assign constructors.
  NUCompositeInterfaceLvalue(const NUCompositeInterfaceLvalue&);
  NUCompositeInterfaceLvalue& operator=(const NUCompositeInterfaceLvalue&);

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCompositeInterfaceLvalue : public NULvalue


//! NUCompositeSelLvalue class
/*!
 * This class acts as a MemselLvalue over a NUCompositeNet
 * from the net it contains.
 */
class NUCompositeSelLvalue: public NUCompositeInterfaceLvalue
{ 
public:

  //! constructor
  /*!
    \param net The identifier
    \param exprs Vector of ranges of the select
    \param loc Source location.
   */
  NUCompositeSelLvalue( NUCompositeInterfaceLvalue *object, NUExprVector *exprs,
                        const SourceLocator& loc );

  //! constructor
  /*!
    \param net The identifier.
    \param exprs Vectors of ranges of the select. The last expression is the variable/constant bit offset.
    \param range The range of select over the bit offset.
    \param loc Source location.
   */
  NUCompositeSelLvalue( NUCompositeInterfaceLvalue *object, NUExprVector* exprs,
                        const ConstantRange range, bool isDummyOffset,
                        const SourceLocator& loc );

  virtual ~NUCompositeSelLvalue();

  virtual NUType getType() const { return eNUCompositeSelLvalue; }

  //! Return class name
  const char* typeStr() const;

  //! Get the number of indices in this select expression
  UInt32 getNumIndices() const { return mExprs.size(); }

  //! Return the referenced mem select expression.
  NUExpr* getIndex(UInt32 dim) const { return mExprs[dim]; }

  //! Push another index expression onto the composite's expression vector
  void addIndexExpr( NUExpr *expr );

  virtual CompositeType getCompositeType() const { return eCompositeSelLvalue; }

  //! Get the number of fields from the referenced NUCompositeNet
  virtual UInt32 getNumFields () const { return mComposite->getNumFields(); }

  //! Get the ith field from the referenced NUCompositeNet
  virtual NUNet* getField(UInt32 index) const { return mComposite->getField( index ); }

  //! Return the object being indexed
  virtual NUCompositeInterfaceLvalue* getIdentExpr() const { return mComposite; }

  //! Return the underlying NUCompositeNet
  virtual NUCompositeNet *getCompositeNet() const { return mComposite->getCompositeNet(); }

  /* \brief get the NUNet corresponding to this composite lvalue
   *
   * This routine must be used with caution; what it actually returns is
   * NOT the precise net that will be referenced.  That net cannot be
   * determined, and will not even exist, until during post-population
   * resynthesis.  Hovever, there are several cases where it is very
   * useful to know what kind of net, at least whether it is a
   * composite/noncomposite net, a composite structure refers to.  In
   * particular, we need this information during population of a
   * bit/part select of a vector inside a record.
   */
  virtual NUNet *getNet() const;

  //! Helper to allow calling the primary compose method
  virtual void compose( UtString* buf, const STBranchNode* scope, bool showRoot ) {
    compose( buf, scope, 0, showRoot );
  }

  //! Return the number index expressions this composite select has
  virtual UInt32 getNumDims() const { return mComposite->getNumDims(); }

  //! Get the Nth range stored in this object
  virtual const ConstantRange *getRange( const UInt32 index ) const;

  //! Get range of the part select on the last indexed dimension.
  const ConstantRange getPartSelRange() const;

  //! Set the range of part select on the last indexed dimension.
  void setPartSelRange(const ConstantRange& psRange);

  //! Is the select on the innermost dimension a part select?
  bool isPartSelect() const {
    return mIsPartSelect;
  }

  //! Is the bit offset on part-select a dummy one that should be ignored?
  /*!
    For part selects like x(3 to 5), that have a range but no index, returns true.
    For part selects like x(3+x to 5+x), that have a range and variable index, returns false.
    For selects like x(3), return false since it's not a part select.
  */
  bool isPartSelectIndexDummy() const {
    return (mIsPartSelect && mIsDummyOffset);
  }

  //! Get the size of the base object of this composite.
  virtual UInt32 getCompositeSize() const;

  //! Return true if this is a simple expression accessing a hierref
  bool isHierRef() const;

  virtual UInt32 getBitSize() const;  

  //! Add the nets this node uses to the given set.
  void getUses(NUNetSet *uses) const;
  void getUses(NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;
  //! Add the nets this node uses to def the given net to the given set.
  void getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const;
  bool queryUses(const NUNetRefHdl &def_net_ref,
		 const NUNetRefHdl &use_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Add the nets this node defines to the given set.
  void getDefs(NUNetSet *defs) const;
  void getDefs(NUNetRefSet *defs) const;
  bool queryDefs(const NUNetRefHdl &def_net_ref,
		 NUNetRefCompareFunction fn,
		 NUNetRefFactory *factory) const;

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);

  //! Add the nets this node completely defines to the given set.
  void getCompleteDefs(NUNetSet *defs) const;
  void getCompleteDefs(NUNetRefSet *defs) const;
  bool queryCompleteDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefFactory *factory) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  virtual void print( bool recurse = true, int ident = 0 ) const;

  //! Return the size this lvalue "naturally" is.
  UInt32 determineBitSize() const;

  //! Resize this lvalue.
  void resize();

  //! hash function for making canonical sets of these
  virtual size_t hash() const;

  //! Helper virtual function, which will always get an expr of the same type to cast
  virtual ptrdiff_t compareHelper(const NULvalue* otherLvalue, bool compareLocator,
                                  bool comparePointer) const;

  //! Construct a RHS corresponding to this LHS
  NUExpr* NURvalue() const;

  //! Does the RValue represent the same object
  bool equalAddr(const NUExpr *rhs) const;

  //! copy
  virtual NULvalue* copy(CopyContext &copy_context) const;

  //! Equivalence
  virtual bool operator==(const NULvalue&) const;

private:
  //! Hide copy and assign constructors.
  NUCompositeSelLvalue(const NUCompositeSelLvalue&);
  NUCompositeSelLvalue& operator=(const NUCompositeSelLvalue&);

  //! The composite object being indexed
  NUCompositeInterfaceLvalue* mComposite;

  //! The bitselect expression.
  NUExprVector mExprs;

  //! The range to be applied over index (bit offset) lvalue. Only one
  //! index expression is supported in this case.
  ConstantRange mRange;

  //! Indicates whether the bit offset on part select is a dummy offset that should be ignored.
  bool mIsDummyOffset;

  //! Indicates whether this is a part select.
  bool mIsPartSelect;

  DECLARENUCLEUSCLASSFRIENDS();

}APIDISTILLCLASS; //class NUCompositeSelLvalue: public NUCompositeInterfaceLvalue


//! NUCompositeFieldLvalue class
/*!
 * This class selects a single field, specified with an integer index,
 * out of the single nonarrayed record specified.
 */
class NUCompositeFieldLvalue: public NUCompositeInterfaceLvalue
{
public:
  NUCompositeFieldLvalue( NUCompositeInterfaceLvalue *object, UInt32 index,
                          const SourceLocator& loc );

  ~NUCompositeFieldLvalue();

  virtual NUType getType() const { return eNUCompositeFieldLvalue; }

  //! Return class name
  const char* typeStr() const;

  virtual CompositeType getCompositeType() const { return eCompositeFieldLvalue; }

  //! Get the number of fields from the referenced NUCompositeNet
  virtual UInt32 getNumFields () const;

  //! Get the ith field from the referenced NUCompositeNet
  virtual NUNet* getField(UInt32 index) const;

  //! Helper to allow calling the primary compose method
  virtual void compose( UtString* buf, const STBranchNode* scope, bool showRoot ) {
    compose( buf, scope, 0, showRoot );
  }

  //! Return the composite object from which a field is being selected
  NUCompositeInterfaceLvalue* getIdentExpr() const { return mComposite; }

  //! Return the underlying NUCompositeNet
  virtual NUCompositeNet *getCompositeNet() const { return mComposite->getCompositeNet(); }

  /* \brief get the NUNet corresponding to this composite lvalue
   *
   * This routine must be used with caution; what it actually returns is
   * NOT the precise net that will be referenced.  That net cannot be
   * determined, and will not even exist, until during post-population
   * resynthesis.  Hovever, there are several cases where it is very
   * useful to know what kind of net, at least whether it is a
   * composite/noncomposite net, a composite structure refers to.  In
   * particular, we need this information during population of a
   * bit/part select of a vector inside a record.
   */
  virtual NUNet *getNet() const;

  //! Return the field index
  UInt32 getFieldIndex() const { return mIndex; }

  //! Set the field index
  void setFieldIndex( UInt32 index ) { mIndex = index; }

  //! Return the number index expressions this composite field has
  virtual UInt32 getNumDims() const;

  //! Get the Nth range stored in this object
  virtual const ConstantRange *getRange( const UInt32 index ) const;

  //! Get the size of the base object of this composite.
  virtual UInt32 getCompositeSize() const;

  //! Return true if this is a simple expression accessing a hierref
  bool isHierRef() const;

  virtual UInt32 getBitSize() const;  

  //! Change this node to define new_net instead of old_net.
  void replaceDef(NUNet *old_net, NUNet* new_net);
  
  //! Replace all references to old_net with new_net.
  bool replace(NUNet * old_net, NUNet * new_net);

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator);

  /*! A field reference doesn't really def the entire net, but we record
   *  it as such because that's the smallest chunk that we can guarantee
   *  the field ref is actually in.
   */
  virtual void getDefs(NUNetSet *defs) const;
  /*! A field reference doesn't really def the entire net, but we record
   *  it as such because that's the smallest chunk that we can guarantee
   *  the field ref is actually in.
   */
  virtual void getDefs(NUNetRefSet *defs) const;
  //! Add the nets this node completely defines to the given set.
  void getCompleteDefs(NUNetSet *defs) const;
  void getCompleteDefs(NUNetRefSet *defs) const;
  bool queryCompleteDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefFactory *factory) const;

  //! calculate costs, add to NUCost arg
  void calcCost(NUCost*, NUCostContext*) const;

  //! print human friendly
  virtual void compose(UtString*, const STBranchNode* scope, int indent, bool recurse) const;

  virtual void print( bool recurse = true, int ident = 0 ) const;

  //! Return the size this lvalue "naturally" is.
  UInt32 determineBitSize() const;

  //! Resize this lvalue.
  void resize();

  //! hash function for making canonical sets of these
  virtual size_t hash() const;

  //! Helper virtual function, which will always get an expr of the same type to cast
  virtual ptrdiff_t compareHelper(const NULvalue* otherLvalue, bool compareLocator,
                                  bool comparePointer) const;

  //! Construct a RHS corresponding to this LHS
  NUExpr* NURvalue() const;

  //! Does the RValue represent the same object
  bool equalAddr(const NUExpr *rhs) const;

  //! copy
  virtual NULvalue* copy(CopyContext &copy_context) const;

  //! Equivalence
  virtual bool operator==(const NULvalue&) const;

private:
  //! Hide copy and assign constructors.
  NUCompositeFieldLvalue(const NUCompositeFieldLvalue&);
  NUCompositeFieldLvalue& operator=(const NUCompositeFieldLvalue&);

  /*! The object having a field chosen from.  This object is expected to
   * evaluate to a single, non-arrayed record. 
   */
  NUCompositeInterfaceLvalue* mComposite;

  //! The field index
  UInt32 mIndex;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCompositeFieldLvalue: public NUCompositeInterfaceLvalue


class NUCompositeIdentLvalue : public NUCompositeInterfaceLvalue
{
public:
  NUCompositeIdentLvalue( NUCompositeNet *net, const SourceLocator &loc );
  virtual ~NUCompositeIdentLvalue();

  //! Function to return the expression type
  virtual NUType getType() const { return eNUCompositeIdentLvalue; }

  //! Return the identifier
  NUCompositeNet* getIdent() const { return mNet; }

  // virtual functions
  const char *typeStr() const { return "NUCompositeIdentLvalue"; }

  virtual void compose( UtString*, const STBranchNode*, int, bool) const;
  virtual void print( bool recurse = true, int ident = 0 ) const;
  virtual void resize() {}
  virtual UInt32 determineBitSize() const;
  virtual bool operator==(const NULvalue&) const;
  virtual void getDefs( NUNetSet *defs) const;
  virtual void getDefs( NUNetRefSet *defs) const;
  virtual bool replaceLeaves(NuToNuFn & translator);
  virtual bool isWholeIdentifier() const { return true; }
  virtual NUNet* getWholeIdentifier() const { return mNet; }
  virtual NULvalue* copy(CopyContext &copy_context) const;

  virtual CompositeType getCompositeType() const { return eCompositeIdentLvalue; }

  /*! \brief Return the number of fields in the composite object
   */
  virtual UInt32 getNumFields () const;

  //! Return the Nth field net pointer
  virtual NUNet *getField( UInt32 fieldIndex ) const;

  //! Get the underlying NUComposite expression for this object
  virtual NUCompositeInterfaceLvalue *getIdentExpr() const;

  //! Get the number of ranges stored in this object
  virtual UInt32 getNumDims() const;

  //! Get the Nth range stored in this object
  virtual const ConstantRange *getRange( const UInt32 index ) const;

  //! Get the size of the base object of this composite.
  virtual UInt32 getCompositeSize() const;

  //! Get the underlying composite net
  NUCompositeNet *getCompositeNet() const {
    return mNet;
  }

  /* \brief get the NUNet corresponding to this composite lvalue
   *
   * This routine must be used with caution; what it actually returns is
   * NOT the precise net that will be referenced.  That net cannot be
   * determined, and will not even exist, until during post-population
   * resynthesis.  Hovever, there are several cases where it is very
   * useful to know what kind of net, at least whether it is a
   * composite/noncomposite net, a composite structure refers to.  In
   * particular, we need this information during population of a
   * bit/part select of a vector inside a record.
   */
  virtual NUNet *getNet() const;

private:
  //! Hide copy and assign constructors.
  NUCompositeIdentLvalue(const NUCompositeIdentLvalue&);
  NUCompositeIdentLvalue& operator=(const NUCompositeIdentLvalue&);

  NUCompositeNet *mNet;

  DECLARENUCLEUSCLASSFRIENDS();

} APIDISTILLCLASS; //class NUCompositeIdentLvalue : public NUCompositeInterfaceLvalue
#endif
