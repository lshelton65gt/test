// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUUSENODE_H_
#define NUUSENODE_H_

#include "nucleus/Nucleus.h"

/*!
  \file
  Declaration of the Nucleus use-node class.
*/

//! NUUseNode class
/*!
 * Abstract class, models a Nucleus construct which only uses values.
 */
class NUUseNode : public NUBase
{
public:
  //! constructor
  NUUseNode() {}

  //! Add the set of nets this construct uses to the given set.
  virtual void getUses(NUNetSet *uses) const = 0;
  virtual void getUses(NUNetRefSet *uses) const = 0;
  virtual bool queryUses(const NUNetRefHdl &use_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory *factory) const = 0;

  //! Replace all references to old_net with new_net.
  virtual bool replace(NUNet * old_net, NUNet * new_net) = 0;

  //! Replace all net references based on a translation functor.
  /*!
    Returns true if any lower net was successfully replaced.
   */
  virtual bool replaceLeaves(NuToNuFn & translator) = 0;

  //! Code Generator
  virtual CGContext_t emitCode (CGContext_t) const {return 0;}

  //! Cost estimation function
  virtual void calcCost(NUCost*, NUCostContext* = 0) const = 0;

  //! Use this method to decide whether to call the 'getBlocking*' or 'get*' methods
  /*! The blocking/nonblocking methods are only valid on some (but not all) statements.
   *  For these statements the simple getDefs and getUses methods are invalid.
   *  Conversely, for non-statement nodes there is no blocking/nonblocking method
   *  variant and for statements which do not support it, these methods exist but
   *  are not valid to call.  To determine which variant to use, this
   *  useBlockingMethods virtual method is provided.
   *
   *  A typical use would look like this:
   *  \code
   *  if (useDef->useBlockingMethods()) {
   *    NUUseDefStmtNode* stmtNode = dynamic_cast<NUUseDefStmtNode*>(useDef);
   *    stmtNode->getBlockingDefs(...);
   *    stmtNode->getNonBlockingDefs(...);  // only necessary before reordering
   *  } else {
   *    useDef->getDefs(...);
   *  }
   *  \endcode
   *  or if you have a pointer that is already an NUUseDefStmtNode but is not specific
   *  enough to be of a known persuasion (an NUAssign, for example) you might do this:
   *  \code
   *  if (assign->useBlockingMethods()) {
   *    assign->getBlockingDefs(...);
   *    assign->getNonBlockingDefs(...);  // only necessary before reordering
   *  } else {
   *    assign->getDefs(...);
   *  }
   *  \endcode
   */
  virtual bool useBlockingMethods() const { return false; } // overridden by NUUseDefStmtNode

  //! destructor
  virtual ~NUUseNode() {}


private:
  //! Hide copy and assign constructors.
  NUUseNode(const NUUseNode&);
  NUUseNode& operator=(const NUUseNode&);
} APIOMITTEDCLASS; //class NUUseNode : public NUBase

#endif
