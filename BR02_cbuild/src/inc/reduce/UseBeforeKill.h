// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __USE_BEFORE_KILL_H_
#define __USE_BEFORE_KILL_H_

#include "nucleus/Nucleus.h"
#include "util/UtHashMap.h"

//! Class to find uninitialized reads of outputs
/*!
  This gathers the outputs at each entry point and analyzes the entry
  point for nets in a bit-accurate fashion that have been used before
  they were killed, ie., read uninitialized.

  During the analysis, the collection of kills is kept, and could be
  used to find the parts of the outputs that are not killed at
  all. This is currently not done.
*/
class UseBeforeKill
{
public:
  //! Destructor
  UseBeforeKill(NUNetRefFactory* factory);
  
  //! Constructor
  ~UseBeforeKill();

  //! NUTF entry point
  void tf(NUTF* tf);

  //! Block entry point
  void block(NUBlock* block);

  //! Were any ubk nets found?
  bool hasUBKNets() const;

  //! Clear out the object, so this object can be reused
  void clear();
  
  //! NUNet* to SourceLocator* map
  typedef UtHashMap<NUNet*, const SourceLocator*> NetToLoc;
  //! Sorted Loop for NetToLoc
  typedef NetToLoc::SortedLoop SortedNetLocLoop;

  
  //! Gather ubk nets that are also members of the queryNets set.
  void intersectNetLocs(NetToLoc* result, const NUNetSet& queryNets) const;
  
private:
  void stmtList(NUStmtLoop stmts,
                NUNetRefSet* collectKills);


  void caseStmt(NUCase* caseStmt, NUNetRefSet* collectKills);

  void ifStmt(NUIf* ifStmt, NUNetRefSet* collectKills);

  void checkUses(NUNetRefSet* usedNetRefs, 
                 const NUNetRefSet& collectKills,
                 const SourceLocator& curLoc);
  
  void updateKillRefs(NUStmt* stmt, NUNetRefSet* killRefs);

  //void filterNetRefSet(NUNetRefSet* netRefs);

  void blockStmt(NUBlock* block, NUNetRefSet* collectKills);

  void forStmt(NUFor* forStmt, NUNetRefSet* collectKills);

  void addUninitReadLoc(const NUNetRefSet& ubkNets, 
                        const SourceLocator& loc);

  void addFullyKilled(NUNet* killedNet);

  NUNetRefFactory* mNetRefFactory;
  //! Bit-accurate ubk analysis ref set
  NUNetRefSet* mUseBeforeKillNetRefs;
  //! Bit-accurate kill analysis ref set
  NUNetRefSet* mKillNetRefs;

  //! Set of nets that are seen as used before killed
  NetToLoc* mUBKNets;
};

#endif // __USE_BEFORE_KILL_H_
