// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef LOGIC2LUT_H_
#define LOGIC2LUT_H_

/*!
  \file 
  Declaration of the Logic2LUT package.
*/

#include "nucleus/Nucleus.h"

class FLNodeFactory;
class ArgProc;
class MsgContext;
class IODBNucleus;
class ArgProc;
class AtomicCache;
class FLNode;
class Fold;

//! The Logic2LUT interface.
/*!
 *  Implementation of transform from general NUExpr to NULut (lookup table)
 *
 *  Examines all expressions in the unelaborated Nucleus tree.  For those
 *  with a sufficiently small number of fanin bits (limit 8), we exhaustively
 *  simulate the entire input space, and record the results in a truth table.
 *  If the evaluation of the LUT, including the concat of the input bits,
 *  is less than the cost of the original expression, then we substitute.
 *
 *  For now, we only do this substitution at the back end, because further
 *  simplifications will not be possible once we have computed the LUT.  At
 *  some point, we could add Folds to simplify LUTs with some constant input
 *  bits into smaller LUTs.
 */
class Logic2Lut
{
public:
  //! Constructor for Logic2LUT interface.
  /*!
   * \param netref_factory Factory used to manage NetRefs.
   *
   * \param dfg_factory Factory for the unelaborated data-flow graph.
   *
   * \param args    Command-line argument interface.
   *
   * \param msg_ctg The message context.
   *
   * \param iodb    The IODB.
   *
   * \param str_cache The string cache.
   *
   * \param verbose Should block resynthesis print debugging
   *                information?
   */
  Logic2Lut(NUNetRefFactory *netref_factory,
            Fold* fold,
            FLNodeFactory *dfg_factory,
            ArgProc* args,
            MsgContext *msg_ctx,
            IODBNucleus *iodb,
            AtomicCache *str_cache,
            bool verbose,
            bool force);
  
  //! dtor
  ~Logic2Lut();

  //! Fully convert the contents of the design 
  bool design(NUDesign * design);

  //! Try to transform one expression to a LUT, returning non-NULL if successful
  NUExpr* expr(NUExpr* expr);

  //! keep track of net-constant substitutions
  typedef UtHashMap<NUNet*, DynBitVector> NetConstantMap;

  //! vector of NUNetRefHdls
  typedef UtVector<NUNetRefHdl> NetRefVector;

private:
  //! Build a LUT for an expression, having already computed the
  //! select concatenation
  NUExpr* buildLut(NUConcatOp* concat, NUExpr* select, NUExpr* expr, UInt32 n);

  //! Build a vector of continuous-range netrefs from a netref set
  bool createConcatVector(const NUNetRefSet& bits, NetRefVector* v);

  //! Sort the concatenation to minimize shifting
  void sortConcatVector(NetRefVector* v);

  //! Build a NUConcatOp from a netref vector.  empty netrefs provide holes
  NUConcatOp* buildConcat(const NetRefVector& v, NUExpr* expr);

  //! Factory used to manage NetRefs.
  NUNetRefFactory * mNetRefFactory;

  //! Factory for the unelaborated data-flow graph.
  FLNodeFactory * mFlowFactory;

  //! args
  ArgProc* mArgs;

  //! The message context.
  MsgContext * mMsgContext;
  
  //! The IODB.
  IODBNucleus * mIODB;

  //! The string cache.
  AtomicCache * mStrCache;

  //! Should we print debug information?
  bool mVerbose;

  //! Should we do block resynthesis independent of whether it's a good idea?
  bool mForce;

  //! Context for calculating costs
  NUCostContext* mCostContext;

  Fold* mFold;

  //! Keep track of the total estimated cost benefit of this transformation
  SInt32 mTotalCostWin;

  //! Keep track of how many LUTs we successfully converted
  UInt32 mNumLuts;
};


#endif
