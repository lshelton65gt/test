// -*-C++-*-    $Revision: 1.6 $
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _OCCURRENCE_LOGGER_H
#define _OCCURRENCE_LOGGER_H

class NUUseDefNode;                     // external declaration
class NUExpr;                           // external declaration
class SourceLocator;                    // external declaration
class ArgProc;                          // external declaration
class NUCModelArgConnection;            // external declaration
class MsgContext;                       // external declaration

#if pfLINUX
#include <sys/param.h>
#endif

#ifndef MAXPATHLEN
#define MAXPATHLEN 1024
#endif

//! Manage a log file
/*! OccurrenceLogger writes a log file, one line per entry, where each entry is
 *  consists of a source code location, a tag and zero or more typed arguments.
 */
class OccurrenceLogger {
public:

  /*! \param msg_context error message handle
   *  \param root the design name used as the first part of the log filename
   *  \param ident the name of the log
   *  \param flags bitmask defined by OccurrenceLogger::Flags
   *  \param dirname optional directory path for the logfile
   *  \note The log filename is constructed as root.ident.log. For example,
   *        the common-concat log filename will usuall be libdesign.concat.log,
   *        with libdesign the root and concat the ident.
   */
  OccurrenceLogger (MsgContext &msg_context, const char *root, const char *ident, const UInt32 flags, 
    const char *dirname = NULL);
  virtual ~OccurrenceLogger ();

  //! Flag bit definitions for mFlags.
  enum Flags {
    cACTIVE  = 1<<0,                    //!< set when the log is active
    cSUMMARY = 1<<1,                    //!< output a summary of the items
    cVERBOSE = 1<<2,                    //!< trace log file opening and closing
    cTEE     = 1<<3,                    //!< tee log file to standard out
    cDEFAULT = 0
  };

  //! Set options flags
  void set (const UInt32 options) { mFlags |= options; }

  //! Write an entry to the log.
  /*! Write an entry to the log consisting of a source location, a tag string
   *  and all the arguments passed to this since the last entry.
   *  \param loc The source location.
   *  \param key The key for the entry. The tag string is given by keyToName (key)
   *  \note If any arguments are desired for the entry
   */
  void log (const SourceLocator &loc, const UInt32 key);
  void log (const char *file, const unsigned int line, const UInt32 key);

  //class Arg;
  //void arg (Arg *x) { mArg [mNArgs++] = x; }

  template <class T>
  void arg (T t, bool brief = false) { mArg [mNArgs++] = new ArgT <T> (t, brief); }

  bool isActive () const { return (mFlags & cACTIVE); }
  virtual const char *keyToName (const UInt32) const = 0;

  //! Output a summary of the number of entries per tag
  void summarise (UtOStream &) const;
  void summarise ();

  //! Wrap occurrence log open and close in a class.
  /*! Wrap a predefined set of occurrence logs so that logs may be opened in
   *  the constructor and closed in the desctructor. Creating an instance of
   *  this class on the stack automates the opening and closing of a number of
   *  occurrence logs.
  */
  class Wrapper {
  public:

    //! Constructor opens logs.
    /*! \param msg_context error message handle
     *  \param args the command line for cbuild
     *  \param root stem of the log file names
     *
     *  The constructor opens the following logs:
     *  \li the common concat log if -logCommonConcat is found 
     *  \li the port vectorisation log if -logPortVec is found
     *
     *  \note any logs opened by the constructor are closed in the destructor
     */

    Wrapper (MsgContext &msg_context, const ArgProc &args, const char *root);

    //! Closed any open occurrence logs
    ~Wrapper ();
  };

private:

  MsgContext &mMsgContext;              //!< error message handle
  UInt32 mFlags;                        //!< flags pass to c'tor and status bits
  const char *mIdent;                   //!< logfile identifier
  char mFilename [MAXPATHLEN];          //!< constructed filename
  
  enum {
    cMAXKEYS = 64,                      //!< maximum number of log keys
    cMAXARGS = 8                        //!< maximum args for an entry
  };

  enum {
    BUFFER_SIZE = 512};                 //!< initial size of the output buffer

  // BUFFER_SIZE bytes are initially allocated for the output buffer. It is
  // grown if required. For example, port vectorisation logging may output
  // graph snapshot. These can be long...

  char *mLineBuffer;                    //!< single line buffer
  UInt32 mSize;                         //!< size of the line buffer
  int mFD;                              //!< fd of the output file
  UInt32 mLength;                       //!< number of bytes written to the log

public:

  //! \class Visible
  /*! By subclassing of OccurrenceLogger::Visible, clients of the logger can
   *  provide a mechanism for private objects to appear in the log.
   */
  class Visible {
  public:
    Visible () {}
    virtual ~Visible () {}
    virtual void compose (UtString *, const UInt32) const = 0;
  };

  class Arg {
  public:
    Arg () {}
    virtual ~Arg () {}

    //! Write a human readable form of the argument to a buffer.
    /* \param s the buffer
     * \param size length of the buffer
     * \return if the buffer is big enough, then the number of bytes written
     * \return if the buffer is not big enough then either -1 * (number of
     *         bytes needed) or -1 to indicate "just grow the buffer and try again"
     */
    virtual SInt32 write (char *s, const UInt32 size) const = 0;

  };

  template <class T>
  class ArgT : public Arg {
  public:
    ArgT (const T t, bool brief = false) : mValue (t), mBrief (brief) {}
    virtual ~ArgT () {}
    virtual SInt32 write (char *s, const UInt32 size) const;
  private:
    const T mValue;
    const bool mBrief;
  };
  
private:

  Arg *mArg [cMAXARGS];                 //!< argument vector

  UInt32 mNArgs;                        //!< current number of args

  // running counters
  UInt32 mTotalCount;                   //!< total number entries
  UInt32 mCount [cMAXKEYS];             //!< entries by key

  //! Open the logfile.
  void openlog (const char *root, const char *ident, const char *dirname);

  //! Close the logfile
  void closelog ();

  //! Grow the buffer by at least delta
  bool grow (const UInt32 delta);

  //! Form the filename from the root, ident and optional directory.
  /*! \note The filename is written into mFilename.
   */
  bool makeFilename (const char *root, const char *ident, const char *dirname);

  //! Put a timestamp on the log.
  void stampLog ();

  //! Write an error message and close the logfile.
  void fail (const char *, ...);

  //! Flush the buffer to the logfile.
  void flush ();

  //! Write the buffer to a file descriptor.
  void writebuffer (int fd);

};

#endif
