// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004, 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*! \file 
  Mark memory-writes inside loop that may need temping.
 */
#ifndef _RE_MEMORYLOOP_
#define _RE_MEMORYLOOP_

#include "nucleus/Nucleus.h"

class MsgContext;

//! Walk a design marking any memory net that is written to inside a loop.
class REMemoryLoop
{
public:
  //! Constructor
  REMemoryLoop(MsgContext*);

  //! Destructor
  ~REMemoryLoop();

  void design (NUDesign * design, bool verbose);

private:
  // Hide copy constructor and assignment
  REMemoryLoop (const REMemoryLoop&);
  REMemoryLoop& operator=(const REMemoryLoop&);

  MsgContext* mMsgContext;

  //! Track nested loops
  UInt32 mLoopDepth;
};
  

#endif // _RE_MEMORYLOOP_

