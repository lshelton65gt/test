// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef DEADDESIGNWALKER_H_
#define DEADDESIGNWALKER_H_

#include "nucleus/NUDesignWalker.h"
#include "util/UtStack.h"

/*!
  \file
  Declaration of dead design walker package.
 */

class MsgContext;
class IODBNucleus;
class ArgProc;

class DeadDesignCallback
{
public:
  virtual ~DeadDesignCallback();

  //! Callback meant to process nets immediately prior to deletion.
  /*!
   * This callback is invoked each time the dead design walk
   * encounters a scope/module which contains nets considered dead.
   * 
   * This callback allows a way of performing cleanup tasks prior to
   * net deletion. Removing references to these nets from the symbol
   * table is a very common operation.
   *
   * \param dead_nets Nets deemed dead by the design walk. 
   *
   * \sa DeadNetCallback
   */
  virtual void operator()(NUNetList * dead_nets) = 0;
};

//! Walk the design or module, removing dead nucleus
/*!
 * The live nucleus is already assumed to have been marked.  This is the implementation of a
 * sweep pass which deletes any non-marked nucleus.
 *
 * This class utilizes the callback-based design walker to perform the walk.
 */
class DeadDesignWalker
{
public:
  /*!
   * constructor
   * \param netref_factory Netref factory
   * \param msg_ctx Message context
   * \param verbose If true will be verbose as delete nucleus
   */
  DeadDesignWalker(NUNetRefFactory *netref_factory,
                   MsgContext *msg_ctx,
                   IODBNucleus *iodb,
                   ArgProc* args,
                   DeadDesignCallback * callback,
                   bool verbose);

  //! Kickoff point for the walk
  void walk(NUDesign *this_design, bool delete_instances);

  //! Kickoff point for the walk, only the given module will be walked
  void walk(NUModule *this_module, bool delete_instances);

  ~DeadDesignWalker();

private:
  //! Hide copy and assign constructors.
  DeadDesignWalker(const DeadDesignWalker&);
  DeadDesignWalker& operator=(const DeadDesignWalker&);

  //! Allow the callback to access the object
  class NucleusCallback;
  friend class NucleusCallback;

  //! Nucleus walk callback, determines if a node needs to be deleted
  class NucleusCallback : public NUDesignCallback
  {
#if ! pfGCC_2
    using NUDesignCallback::operator();
#endif

  public:
    NucleusCallback() :
      mObj(0),
      mStartModule(0),
      mOnlyOneModule(false),
      mContinueModule(false)
    {}

    /*!
     * \param only_one_module If true, only the first module seen will
     *        be traversed. This is used for doing dead code
     *        elimination under one module.
     *
     * \param delete_instances If true, allow deletion of module
     *     instances. If performing dead code elimination after
     *     elaborated aliasing, deleting module instances could delete
     *     allocation points, leading to an invalid model.
     */
    NucleusCallback(DeadDesignWalker *obj, 
                    bool only_one_module,
                    bool delete_instances) :
      mObj(obj),
      mStartModule(0),
      mOnlyOneModule(only_one_module),
      mContinueModule(true),
      mDeleteInstances(delete_instances)
    {}

    Status operator()(Phase phase, NUBase *node);
    Status operator()(Phase phase, NULvalue *node);
    Status operator()(Phase phase, NUExpr *node);
    Status operator()(Phase phase, NUCaseItem *node);
    Status operator()(Phase phase, NUCaseCondition *node);
    Status operator()(Phase phase, NUTFArgConnection *node);
    Status operator()(Phase phase, NUPortConnection * node);
    Status operator()(Phase phase, NUDesign *node);
    Status operator()(Phase phase, NUModule *node);
    Status operator()(Phase phase, NUModuleInstance *node);
    Status operator()(Phase phase, NUStructuredProc *node);
    Status operator()(Phase phase, NUBlock *node);
    Status operator()(Phase phase, NUTask *node);
    Status operator()(Phase phase, NUCModelArgConnection* node);
    Status operator()(Phase phase, NUCModel* node);
    Status operator()(Phase phase, NUCModelPort* node);
    Status operator()(Phase phase, NUCModelInterface* node);
    Status operator()(Phase phase, NUCModelFn* node);
    Status operator()(Phase phase, NUNamedDeclarationScope* node);

    ~NucleusCallback() {}

  private:
    DeadDesignWalker* mObj;

    //! The following keep state when calling this for a single module.
    NUModule *mStartModule;
    bool mOnlyOneModule;
    bool mContinueModule;

    //! Deleting instances at late-stages will remove allocation points.
    bool mDeleteInstances;
  };

  //! Make sure walk cleanup is done
  void postWalk();

  //! Set up for module traversal
  void preModule(NUModule *module);

  //! Clean up from a module traversal
  void postModule(NUModule *module);

  //! Handle a dead block; add the nets to the removed list
  void deadBlock(NUBlock *block);

  //! Search a module's blocks for unused locals.
  void findDeadLocals(NUModule * module);

  //! Move dead nets from mRemoveNetListStack to mRemoveNetList
  void prepareDeadNets();

  //! Delete all the nets in mRemoveNetList
  void removeDeadNets();

  //! Mark that there has been a modification to the current module.
  void markModification();

  //! Put the net onto the remove list for later removal.  Warn if the net is protected mutable.
  void rememberDeadNet(NUNet *net);

  //! Stack to track if anything in a module has been deleted.
  UtStack< bool > mModuleModifiedStack;

  //! Stack of (per module) list of nets which are being deleted.
  UtStack< NUNetList* > mRemoveNetListStack;

  //! List of nets to delete (per design)
  NUNetList mRemoveNetList;

  //! Netref factory
  NUNetRefFactory *mNetRefFactory;

  //! Message context.
  MsgContext *mMsgContext;

  //! IODB
  IODBNucleus *mIODB;

  //! Args
  ArgProc* mArgs;

  //! Callback prior to deleting all nets.
  DeadDesignCallback * mCallback;

  //! Be verbose?
  bool mVerbose;
};

#endif
