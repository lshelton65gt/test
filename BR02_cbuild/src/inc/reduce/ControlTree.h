// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CONTROL_TREE_H_
#define CONTROL_TREE_H_

/*!
 * \file
 * Declaration of the ControlTree data structure and routines used
 * to construct and manipulate them.
 *
 * A ControlTree is a binary tree with an NUExpr associated with each node.
 * The leaves of the tree represent the RHS of an assignment, and each
 * branch node is the condition under which assignments below that branch
 * are controlled.
 *
 * For example, a Verilog if statement:
 * \verbatim
 * if (rst)
 *   l = 0;
 * else if (clk)
 *   l = in;
 * \endverbatim
 * would give rise to a ControlTree:
 * \verbatim
 *      rst
 *      / \
 *     /   \
 *   clk    0
 *     \
 *      \
 *       in
 * \endverbatim
 */

#include "util/UtPair.h"
#include "util/UtMap.h"

#include "nucleus/NUExpr.h"
#include "nucleus/NUDesignWalker.h"

#include "flow/FLNodeElab.h"

#include "bdd/BDD.h"

class STBranchNode;
class ControlTree;
class BDDSource;

typedef UtMap<NUUseDefNode*,UInt32> NucleusOrderMap;

//! A simple callback base class used to operate on ControlTrees
class ControlTreeCallback
{
public:
  //! Virtual destructor
  virtual ~ControlTreeCallback() {;}

  //! Visit routine called on applicable nodes
  virtual bool visit(ControlTree* /* node */) { return false; }
};

//! Represents the location of a particular node within a tree
class ControlTreeNodeAddress
{
public:
  //! Build the address for a node
  ControlTreeNodeAddress(ControlTree* node);

  //! Get the node in a tree that corresponds to this address, or NULL
  ControlTree* lookup(ControlTree* tree) const;

  //! Get the path length for this address
  UInt32 length() const { return mNumBranches; };

  //! Truncate the address to len branches
  void truncate(UInt32 len);

  //! Print an address to stdout
  void print() const;

  //! Test for equality
  bool operator==(const ControlTreeNodeAddress& other) const;

  //! Test for inequality
  bool operator!=(const ControlTreeNodeAddress& other) const { return (!((*this) == other)); }

  //! Comparison operator
  bool operator<(const ControlTreeNodeAddress& other) const;

private:
  UInt32         mNumBranches; //! How many branches are specified in the address
  UtList<UInt32> mRoute;       //! The branch values, packed into 32-bit words
};

/*! A ControlTree abstracts out the pattern of nested if-then control
  constructs.  It has branch nodes, each with a controlling condition
  and a combination of true and false sub-trees, and leaf nodes which
  have a right-hand-side expression which is used when all of the
  controlling conditions activate the path from the root to the leaf. 
 */
class ControlTree
{
public:
  //! Constructor for a leaf node with a single flow
  ControlTree(FLNodeElab* leafFlow)
    : mParent(NULL), mTrue(NULL), mFalse(NULL)
  {
    mFlowSet.insert(leafFlow);
  }

  //! Constructor for a leaf node with multiple flows
  ControlTree(FLNodeElabSet::const_iterator beg, FLNodeElabSet::const_iterator end)
    : mParent(NULL), mTrue(NULL), mFalse(NULL)
  {
    mFlowSet.insert(beg,end);
  }

  //! Constructor for a branch node
  ControlTree(FLNodeElab* ifFlow, ControlTree* trueLeg, ControlTree* falseLeg)
    : mParent(NULL), mTrue(trueLeg), mFalse(falseLeg)
  {
    mFlowSet.insert(ifFlow);
    if (trueLeg)
      trueLeg->setParent(this);
    if (falseLeg)
      falseLeg->setParent(this);
  }

  //! Destructor -- deletes entire tree
  ~ControlTree()
  {
    delete mTrue;
    if (mFalse != mTrue)
      delete mFalse;
  }

  //! Static utility method for building a ControlTree from an elaborated flow node
  static ControlTree* controlTreeFromFlow(FLNodeElab* flow, 
                                          NucleusOrderMap& orderMap,
                                          NUNetRefFactory* netRefFactory);

  //! Is this the root of the control tree?
  bool isRoot() const { return (mParent == NULL); }

  //! Is this a branch node?
  bool isBranch() const { return (mTrue || mFalse); }

  //! Is this a complete branch node? (Ie. has both true and false legs)
  bool isCompleteBranch() const { return (mTrue && mFalse); }

  //! Is this a leaf of the control tree?
  bool isLeaf() const { return !isBranch(); }

  //! Get the parent of this node in the tree, or NULL if this is the root
  ControlTree* getParent() const { return mParent; }

  //! Get the FLNodeElab for a branch (it will be an if flow node).
  FLNodeElab* getBranchFlow() const { INFO_ASSERT(isBranch() && (mFlowSet.size() == 1), "Not a valid branch");
                                      return *(mFlowSet.begin()); }

  //! Get the use-def node for a branch (it will be an NUIf).
  NUUseDefNode* getBranchUseDef() const { FLNodeElab* flow = getBranchFlow();
                                          return flow ? flow->getUseDefNode() : NULL; }

  //! Begin iterator for the set of flows associated with a leaf node
  FLNodeElabSet::const_iterator beginLeafFlows() const { INFO_ASSERT(isLeaf(), "Not a leaf");
                                                         return mFlowSet.begin(); }

  //! End iterator for the set of flows associated with a leaf node
  FLNodeElabSet::const_iterator endLeafFlows() const { INFO_ASSERT(isLeaf(), "Not a leaf");
                                                       return mFlowSet.end(); }

  //! Get the condition expression from a branch node if-statement
  /*! This call is only valid for branch nodes */
  NUExpr* getIfCondExpr() const;

  //! Get the sub-tree for the true condition, or NULL if there is none
  /*! This call is only valid for branch nodes */
  ControlTree* getTrueBranch() const { INFO_ASSERT(isBranch(), "Not a branch"); return mTrue; }

  //! Get the sub-tree for the false condition, or NULL if there is none
  /*! This call is only valid for branch nodes */
  ControlTree* getFalseBranch() const { return mFalse; }

  //! Make a deep copy of this control tree
  ControlTree* clone() const;

  //! Test for equality
  bool operator==(const ControlTree& other) const;

  //! Test for inequality
  bool operator!=(const ControlTree& other) const { return (!((*this) == other)); }

  //! compare for ordering
  static int compare(const ControlTree* first, const ControlTree* second);

  //! less than for ordering
  bool operator<(const ControlTree& other) const
  {
    return ControlTree::compare(this, &other) < 0;
  }

  //! Test for equal control structures (leaf expressions can differ)
  bool sameControlStructure(ControlTree* other) const;

  //! Delete the subtree at this node and remove it from its parent
  /*! Note: ONLY CALL THIS IF THE TREE IS ON THE HEAP.
   *        YOU CANNOT ACCESS THE NODE AFTER CALLING THIS METHOD.
   */
  void prune();

  //! Prune use of the given net from the tree
  /*! Note: ONLY CALL THIS IF THE TREE IS ON THE HEAP.
   *        IF THIS METHOD RETURNS FALSE, THE TREE HAS BEEN
   *        DELETED (PRUNED COMPLETELY) AND YOU CANNOT ACCESS
   *        THIS POINTER ANYMORE.
   */
  bool pruneUse(NUNet* net);

  //! Merge another control tree into this one (this one has priority)
  void merge(ControlTree* other);

  //! Reduce the control tree if there are any redundant branches
  ControlTree* reduce(BDDSource* bddSource);

  //! Convert a branch node into a leaf node
  void makeLeaf();

  //! Build a BDD representing the condition from the root to this node
  BDD getReadCondition(BDDSource* bddSource) const;

  //! Build a BDD representing the condition from this node to its leaves
  BDD getDrivingCondition(BDDSource* bddSource) const;

  //! Return true if the control tree has a single spine
  bool isSingleSpine() const;

  //! Return true if this tree's leaf drivers have only continuous flow fanin
  bool isContinuouslyDriven() const;

  //! Return true if this tree is fully populated (every branch has both true and false legs)
  bool isFullyPopulated() const;

  //! Return the node with the given flow, or NULL if none in this tree have it
  ControlTree* findNodeWithFlow(FLNodeElab* flow);

  //! Print a text representation of the control tree
  void print(int indent=0) const;

  //! Compose a text representation of the control tree
  void compose(UtString* buf, int indent=0) const;

  //! Write out the ControlTree in dot format
  void write(const char* filename) const;

  //! Execute a callback for each leaf in the tree
  bool forAllLeaves(ControlTreeCallback* callback);

  //! Execute a callback for each branch in the tree
  bool forAllBranches(ControlTreeCallback* callback);

  //! Execute a callback for each node in the tree.
  bool forAllNodes(ControlTreeCallback* callback);

  //! Is this particular node well-formed?
  /*!
    To perform a consistency check over an entire tree, the following is necessary:
    \code
      ControlTreeConsistencyCallback consistency_callback;
      bool consistent = tree->forAllNodes(&consistency_callback);
    \endcode
   */
  bool consistent();

private:
  //! Set the parent of this node
  void setParent(ControlTree* parent) { INFO_ASSERT(!mParent, "Overwriting parent pointer!");
                                        mParent = parent; }

  //! Replace this node with the given replacement tree
  void replace(ControlTree* replacement);

  //! Replace or prune this node if it's redundant according to the condition
  ControlTree* reduceHelper(BDDSource* bddSource, BDD& condition);

  //! Helper method for writing tree in dot format
  void writeHelper(UtOStream& out) const;

  //! Helper method for writing node name to file
  void writeName(UtOStream& out) const;

  //! Helper method foe isSingleSpine()
  int singleSpineHelper() const;

  typedef UtPair<UInt32,ControlTree*> OrderedTree;
  typedef UtVector<OrderedTree>       OrderedTreeVector;

  //! class Result
  /*! This class encapsulates the result from the control tree
   *  creation. This is because as we create it, we have to keep track
   *  of whether the leaf assignments kill the def or not. If they
   *  don't (memories and LHS dynamic bit selects) then we can't merge
   *  two parallel control trees. See the code in
   *  buildTreeForMultipleFlows for details.
   *
   *  Note that the kills flag is not looking to make sure that the
   *  def's are killed at all levels. That is never the case for
   *  latches. It is used to find any pessimism at the leaf statements
   *  in the nucleus tree. These invalidate some of the control
   *  merging code in buildTreeForMultipleFlows.
   *
   */
  class Result
  {
  public:
    //! constructor
    Result(ControlTree* tree, bool kills) : mTree(tree), mKills(kills) {}

    //! Get the control tree
    ControlTree* getTree() const { return mTree; }

    //! Get the kill flag
    bool allAssignsKill() const { return mKills; }

  private:
    ControlTree* mTree;
    bool mKills;
  };

  //! Static helper function to create a control tree from a single flow
  static Result buildTreeForSingleFlow(FLNodeElab* flowElab,
                                       NucleusOrderMap& orderMap,
                                       NUNetRefFactory* netRefFactory);

  //! Static private utility function for merging multiple control trees
  static Result buildTreeForMultipleFlows(FLNodeElab* parent,
                                          FLNodeElabSet& flows,
                                          NucleusOrderMap& orderMap,
                                          NUNetRefFactory* netRefFactory);

  //! Static helper function to see if all leaf statements kill
  /*! This code finds UD pessimism if we are not going to walk down
   *  and create parts of the control tree.
   */
  static bool allLeafAssignmentsKill(FLNodeElab* flowElab, 
                                     NUNetRefFactory* netRefFactory);

private:
  ControlTree*  mParent;  //! the parent for a non-root node
  ControlTree*  mTrue;    //! the true sub-tree for a branch node
  ControlTree*  mFalse;   //! the false sub-tree for a branch node

  // This will be a single if flow for a branch or one or more flows for a leaf.
  FLNodeElabSet mFlowSet; //! set of flows at this node
};


//! Perform consistency checks on each node of a control tree.
class ControlTreeConsistencyCallback : public ControlTreeCallback
{
public:
  ControlTreeConsistencyCallback() :
    ControlTreeCallback() 
  {}
  virtual ~ControlTreeConsistencyCallback() {}
  virtual bool visit(ControlTree * node) {
    return node->consistent(); // call the per-node consistency checker.
  }
};

//! A design walker which builds a NucleusOrderMap for an always block
class NucleusOrderCallback : public NUDesignCallback
{
public:
  //! Constructor
  NucleusOrderCallback()
    : NUDesignCallback()
  {
    mOrderMap = new NucleusOrderMap;
    mNext = 1;
  }

  //! Destructor
  ~NucleusOrderCallback()
  {
    delete mOrderMap;
  }

  //! Return the order map constructed by the callback
  NucleusOrderMap* getOrderMap()
  {
    NucleusOrderMap* old = mOrderMap;
    mOrderMap = new NucleusOrderMap;
    mNext = 1;
    return old;
  }

  // Callback overrides
  NUDesignCallback::Status operator()(Phase, NUBase*) { return eNormal; }

  NUDesignCallback::Status operator()(Phase phase, NUUseDefStmtNode* stmt)
  {
    if (phase == ePre)
      mOrderMap->insert(NucleusOrderMap::value_type(stmt,mNext++));
    return eNormal;
  }

private:
  UInt32           mNext;     //! The next index to assign
  NucleusOrderMap* mOrderMap; //! The map from statement to order
};

#endif // CONTROL_TREE_H_

