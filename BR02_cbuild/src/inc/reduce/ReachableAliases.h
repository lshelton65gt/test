// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef REACHABLEALIASES_H_
#define REACHABLEALIASES_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUNet.h"
#include "symtab/STAliasedLeafNode.h"
#include "util/UtHashSet.h"

//! ReachableAliases class
/*!
 * This class keeps track of which unelaborated aliases are reachable.
 *
 * This is needed because there are some aliases, such as created by
 * flattening, which are just used for naming purposes, and do not
 * have any semantic impact on the net.
 */
class ReachableAliases
{
public:
  //! constructor
  ReachableAliases();

  //! destructor
  ~ReachableAliases();

  //! Populate the set of reachable aliases from the given symbol table
  /*!
   * Call this routine after elaboration but before aliasing.
   *
   * It disqualifies any STAliasedLeafNodes that are not masters
   * and also storage nodes.
   */
  void compute(STSymbolTable *symtab);

  //! Return true if the given node is a reachable alias, false otherwise
  bool query(STAliasedLeafNode *node) const;

  //! Apply the given function to all reachable aliases, return true if any match, false otherwise.
  bool queryAliases(NUNetElab *net_elab, NUNet::QueryFunction fn) const;
  bool queryAliases(STAliasedLeafNode *node, NUNet::QueryFunction fn) const;

  //! Remember the given elaborated net as being depositable.
  void rememberDepositable(NUNetElab *net);

  //! Query if the given elaborated net is depositable.
  bool queryDepositable(NUNetElab *net) const;

  //
  // NOTE: the following query routines are here because they are used by
  // multiple analyses and they utilize the computed reachable set.
  //

  //! Return true if the given net elab requires tristate initialization, false otherwise.
  bool requiresTriInit(NUNetElab *net_elab) const;

  //! Is this net attached to a pullup or pulldown?
  /*!
   * Only create initializations for tristate nets or nets which have pullups or pulldowns.
   */
  bool isAttachedToPull(NUNetElab *net_elab) const;

  //! Does this net get read or written?
  /*!
   * test/hier_ref/test10.v -- no readers/writers --> no init!
   */
  bool isReadOrWritten(NUNetElab *net_elab) const;

  //! Is this net effectively depositable or a primary input?
  /*!
   * If a net is depositable but also has an explicit pull, what
   * we want to do is error out, because we there is no way to
   * let the pull take effect if the user does not do an explicit
   * deposit.
   */
  bool isDepositOrInput(NUNetElab *net_elab) const;

private:
  //! Hide copy and assign constructors.
  ReachableAliases(const ReachableAliases&);
  ReachableAliases& operator=(const ReachableAliases&);

  //! Set of reachable nodes
  typedef UtHashSet<STAliasedLeafNode*> STALNSet;
  STALNSet mReachables;

  //! Set of depositable elaborated nets
  NUNetElabSet mDepositables;
};

#endif
