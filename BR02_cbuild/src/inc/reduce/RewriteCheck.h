// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef REWRITE_CHECK_H_
#define REWRITE_CHECK_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUNetSet.h"

/*!
  \file
  Declaration of rewrite consistency package.
 */

//! Make sure all nets we expected to rewrite have been removed from Nucleus.
class RewriteCheck : public NuToNuFn
{
public:
  //! Constructor
  RewriteCheck ( const NUNetSet & check_nets ) :
    mCheckNets(check_nets)
  {}

  //! Check that no rescope candidate is still referenced by Nucleus.
  NUNet * operator()(NUNet * net, Phase /*phase*/) { 
    NU_ASSERT(mCheckNets.find(net) == mCheckNets.end(),net);
    return NULL;
  }
private:  
  const NUNetSet & mCheckNets;
};

#endif
