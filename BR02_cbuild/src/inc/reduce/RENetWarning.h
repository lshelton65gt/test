// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef RENETWARNING_H_
#define RENETWARNING_H_

#include "nucleus/Nucleus.h"
#include "util/Util.h"
#include "flow/Flow.h"

class MsgContext;
class IODBNucleus;
class FLNodeElab;
class ReachableAliases;
class REAlias;
class ArgProc;
class NUNetElabRefSet2;
class FLNodeElabFactory;
class STBranchNode;
class STSymbolTableNode;

/*!
  \file
  Declaration of class used to scan a design for undriven nets
*/

//! RENetWarning
/*!
 * Class which elaborated finds undriven bits of nets
 */
class RENetWarning
{
public:
  enum WarningFlags {
    eWarnUndriven = 0x1,
    eWarnDead     = 0x2
  };

  //! constructor
  RENetWarning(NUNetRefFactory *netref_factory,
               STSymbolTable* symtab,
               IODBNucleus* iodb,
               MsgContext *msg_context,
               ReachableAliases* reachableAliases,
               REAlias* alias,
               NUDesign* design,
               ArgProc*,
               FLNodeElabFactory*);

  //! dtor
  ~RENetWarning();

  //! Design walk to find unitialized nets
  void walkDesign();

  //! print warnings for the nets according to the mask of WarningFlags
  void printWarnings(UInt32 warningFlagsMask);

  //! declare the command line arguments required by RENetWarning
  static void addCommandlineArgs(ArgProc* arg);

  //! Consider test/constprop/partial_const.v.  There is a live
  //! wire ("temp") with one constant bit.  It gets rescoped into
  //! the always-block but constant propagation finds the constant
  //! bit and annotates it.  The Shell does not know how to deal
  //! with partial constants on dead nets, so make all the bits we
  //! don't know about be "X"
  void fixPartialDeadConstants();
 
  // Function to determine if this driver is a valid driver. The
  // invalid drivers are the port connections which are already
  // represented by aliasing.
  //
  // The drive return value indicates the net is driven
  //
  // The driverFound indicates that this flow node is not an aliased
  // port connection and should be considered when computing
  // undriven/dead nets. All others should be ignored.
  // If checkForConstant is true, then for the case of input ports,
  // only if they are driven by a constant will they be considered as driven.
  // This is used by constant propagation. If checkForConstant is false, then
  // any actual expression is considered as the driver for the input port,
  // even if that actual expression is undriven itself. See bug 12540 for more
  // details.
  void isValidDriver(FLNodeElab* drive, NUNetElab* driveNet,
                     bool* driven, bool* driverFound, bool checkForConstant = true);

  //! Update the alias class with a new one
  void putREAlias(REAlias* alias);

  //! Print nets that are driven both by tristate and non-tristate drivers
  //! Assumes that walkDesignZ() has been called already
  void printZConflictWarnings(); // must happen after LFTristate

  //! map to remember whether a flow-node has a z-loss
  typedef UtHashMap<FLNodeElab*,bool> ZLossMap;

  //! walk the fanin of a flow-node defining a non-z driver for a net
  //! that also has Z drivers, alerting the user on any Z drivers that
  //! have been lost through NUUseDefNodes that ought to preserve Z according
  //! to Verilog semantics
  /*!
   *! \param drive the flow-node to walk
   *! \param printIt whether to print a warning or just return whether
   *!        a z-loss was found
   *! \param zlossMap a hash-table to memo-ize whether a flow-node has already
   *!        been traversed.  This is needed to avoid infinite recursion on
   *!        cycles
   */
  bool walkZLoss(FLNodeElab* drive, bool printIt, ZLossMap* zlossMap);

  //! Walk the design looking for nets that are driven by both Z-drivers
  //! and non-Z drivers
  void walkDesignZ();

  //! Determine if any of the local nets in this netelab's alias ring
  //! have been marked as protected-mutable
  bool isProtectedMutable(NUNetElab* netElab) const;

  //! Dump the design drivers to a file //rjc this can probably be removed see bug 8133
  void dumpDrivers(ZostreamDB* file);

private:
  //! the sorts of warnings we can print on a range
  enum Warning {
    eUndriven,
    eWeaklyDriven,
    eDrivenBothZAndNonZ,
    eMultipleStrongDrivers,
    eDead
  };

  //! print a single net range warning
  void printRangeWarning(NUNetElab* netElab,
                         const NUNetRefHdl& undriven,
                         Warning);

  //! Helper function for printRangeWarning
  void printSliceWarning(NUNetElab* netElab, Warning warning,
                         const char* slice);

  void printUndrivenWarnings();

  //! Design walk to find & print undriven output ports
  void printUndrivenLocalOuts();

  //! Compute the bits of live vector nets that have no live drivers and no loads.
  /*!
   *! We don't necessarily need to warn about them (maybe we will) but we do need
   *! to mark them in the IODB file so the shell can display those bits correctly.
   */
  void computeDeadBits();

  // helper functions to compute various things on the potentially
  // aliased nets.
  bool isProtectedObservable(NUNetElab* netElab) const;
  bool isPrimaryOutput(NUNetElab* netElab) const;

  //! Get the aliases if there are any
  void getAliases(NUNetElab* netElab, NUNetElabVector* netElabs) const;

  //! Determine if netRef intersects netElabRefSet[netElab]
  bool intersects(NUNetElab* netElab,
                  const NUNetRefHdl& netRef,
                  NUNetElabRefSet2* netElabRefSet);

  void writeString(ZostreamDB* db, const char* str);
  void writeHierarchy(ZostreamDB* db, STSymbolTableNode* node);
  void dumpDriver(ZostreamDB* db, FLNodeElab*, UInt32 depth);

private:
  //! Hide copy and assign constructors.
  RENetWarning(const RENetWarning&);
  RENetWarning& operator=(const RENetWarning&);

  typedef bool (RENetWarning::* NetQueryFunction)(NUNetElab*) const;
  bool queryProtectedMutable(NUNetElab* netElab) const;
  bool queryProtectedObservable(NUNetElab* netElab) const;
  bool queryPrimaryOutput(NUNetElab* netElab) const;
  typedef UtHashMap<NUNetElab*,bool> NetBoolMap;
  bool checkNetFlag(NUNetElab* netElab,
                    NetBoolMap* cache,
                    NetQueryFunction queryFn) const;
  bool testEnableConstant0(NUUseDefNode* ud, STBranchNode* hier);

  //! Determine which bits that are killed by the given flow, return true if
  //! any were killed.
  /*! \param flow The flow to check
   *! \param localKill is filled in with the netref for the local
   *!        net driven by the given flow, not the master net.
   *!        For the purposes of this method, all continuous drivers kill.
   *!        Perhaps a better name should be found.
   *! \param globalKill localKill's image projected on the canonical
   *!        NUNetElab's NUNet
   */
  bool findKilledBits(FLNodeElab* flow, NUNetRefHdl* localKill,
                      NUNetRefHdl* globalKill);

  //! Test to see whether this flow-node is part of a Z/Non-Z conflict
  bool testConflict(FLNodeElab*, const NUNetRefHdl& killedBits,
                    NUNetElabSet* tieNets);

  NUNetRefFactory* mNetRefFactory;
  MsgContext* mMsgContext;
  IODBNucleus* mIODB;
  STSymbolTable* mSymbolTable;
  NUNetElabRefSet2* mReadBits;
  NUNetElabRefSet2* mWrittenBits;
  NUNetElabRefSet2* mWeakBits;
  NUNetElabRefSet2* mEnabledBits;
  NUNetElabRefSet2* mFullTimeBits;
  ReachableAliases* mReachableAliases;
  REAlias* mAlias;

  NUDesign* mDesign;

  //! Define a FLNodeElab set where the comparator will show
  //! two different FLNodeElabs that print the same (same net,
  //! same usedef location) as being the same, and will only admit
  //! one.  This is used to reduce redundant message printing.
  struct FLNodeElabPrintCmp {
    bool operator()(FLNodeElab* f1, FLNodeElab* f2) const;
  };
  typedef UtSet<FLNodeElab*,FLNodeElabPrintCmp> FLNodeElabPrintSet;

  typedef UtHashMap<NUNetElab*, FLNodeElabPrintSet> ElabNetRefFlowSetMap;

  //! Keep track of all the driving flow-nodes for a net that has driver
  //! conflicts.
  /*! During the discovery of the conflicted nets, mConflictFlowMap
   *! gets initialized with empty FLNodeElabPrintSets when a net has
   *! been discovered to hae conflicts, marking that we are going to
   *! need to record of all the drivers for that net in a second pass.
   */   
  ElabNetRefFlowSetMap mConflictFlowMap;

  NetBoolMap* mProtectedMutableCache;
  NetBoolMap* mProtectedObservableCache;
  NetBoolMap* mPrimaryOutputCache;

  FLNodeElabFactory* mFLNodeElabFactory;

  ArgProc* mArgs;
}; // class RENetWarning


#endif
