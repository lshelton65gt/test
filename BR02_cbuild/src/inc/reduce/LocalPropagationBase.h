// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _LOCALPROPAGATIONBASE_H_
#define _LOCALPROPAGATIONBASE_H_

#include "nucleus/Nucleus.h"
#include "reduce/LocalPropagationStatistics.h"

class Fold;

/*!
  The base class for stack-based propagation approaches. 
  This helps maintain the context stacks and maps.
*/
class LocalPropagationBase
{
public:
  //! Constructor
  /*!
    fold                  - A Fold object used for expression simplification.
    statistics            - Statistics object for noting successful replacements.
    storing_non_constants - Must be true if the stored propagations may be non-constant.
    dont_check_prop_vecs  - Do not check if constant propgation vectors are empty prior
                            to deletion of an object of this class.
   */
  LocalPropagationBase(Fold* fold,
                       LocalPropagationStatistics * statistics,
                       bool storing_non_constants, bool dont_check_prop_vecs = false);

  //! Destructor
  ~LocalPropagationBase();

  //! Pushes a new context onto the stack.
  void pushContext();

  //! Removes the constant context from the top of the stack.
  /*! 
    If the current context is within a nesting, the propagation
    expressions are saved aside so they can be applied to the parent
    context when we leave the nested object.
   */
  void popContext();

  //! Add a propagation expression to the current context.
  /*! 
    The provided expression is added to the top propagation stack. A
    copy of the expression is saved; the original expression may be
    deleted if needed.

    If the provided expression is NULL, any cached expression is
    cleared.
   */
  void addNet(NUNet* net, NUExpr * expr);

  //! Function to optimize an expression in the nucleus tree.
  /*!
    Given the current contents of the propagation cache, the provided
    expression has all propagation opportunities replaced. The result
    is simplified upon replacement.

    If the result is non-NULL, an optimization has been performed.
   */
  NUExpr* optimizeExpr(NUExpr* expr, bool* expr_changed = NULL,
                       NUNetSet* replacedNets = NULL);

  //! Return the Fold object.
  Fold * getFold() const { return mFold; }

  //! Return the propagation statistics.
  LocalPropagationStatistics* getStatistics() const { return mStatistics; }

  //! Note entry into a nested context.
  void pushNesting();

  //! Note exit from a nested context.
  /*!
    If the preserve_non_statics flag is false, do not promote
    non-static nets to the parent context.
   */
  void popNesting(bool preserve_non_statics);

  //! Clear the propagation cache for all defs in the given statement.
  void applyNonPropagatedDefs(NUNetSet& defs);

private:
  void cleanupExprUseMap(NUExpr** pexpr);
  void populateExprUseMap(NUExpr** pexpr);

  //! Hide copy and assign constructors.
  LocalPropagationBase(const LocalPropagationBase&);
  LocalPropagationBase& operator=(const LocalPropagationBase&);

  // Types and data for a stack of propagation contexts.
  typedef UtHashMap<NUNet*, NUExpr*> NetValues;
  typedef LoopMap<NetValues> NetValuesLoop;
  typedef UtVector<NetValues*> NetValuesVector;
  typedef Loop<NetValuesVector> NetValuesVectorLoop;
  typedef RLoop<NetValuesVector> NetValuesVectorRLoop;
  NetValuesVector* mNetValues;

  // to accelerate disqualifyUses, maintain a map from each used
  // NUNets to the NUExpr** which is a pointer to the 'value' field
  // in the NetValues map
  typedef UtHashSet<NUExpr**> ExprPtrSet;
  typedef UtHashMap<NUNet*,ExprPtrSet> UseMap;
  UseMap mUseMap;

  // Functions to manage a constant context
  NUExpr * getNetValue(NUNet* net) const;

  // Types and data for a stack of nestings. Each nesting contains a
  // vector of constant caches. Each cache represents a different
  // leg of the nested construct. For example for if statements
  // there are two contexts, one for the then and one for the else.
  typedef UtVector<NetValuesVector*> NestedNetValues;
  NestedNetValues * mNestedNetValues;

  //! Clear the propagation cache based on a set of defs.
  void disqualifyDefs(NUNetSet & defs);

  //! Clear the propagation cache based on a set of uses.
  void disqualifyUses(NUNetSet & defs);

  //! Eliminate all of the local, cached expression copies.
  void cleanupNetValues(NetValues * netValues,
                        bool delete_expr, bool clean_use_map);

  //! Fold.
  Fold* mFold;

  //! Statistics object.
  LocalPropagationStatistics * mStatistics;

  //! Must be true if the stored propagations may be non-constant.
  bool mStoringNonConstants;

  //! Population code uses this code too. It never exits cleanly on an error
  //! Avoid asserting if constant propagation vectors are not empty.
  bool mDontCheckPropVecs;

}; // class LocalPropagationBase : public NUDesignCallback


#endif
