// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef ATOMIZE_H_
#define ATOMIZE_H_

#include "nucleus/Nucleus.h"

/*!
  \file
  Declaration of atomization package.
*/

class AtomicCache;
class NUNetRefFactory;
class IODBNucleus;
class MsgContext;
class ArgProc;
class AllocAlias;

class AtomizeStatistics;

//! Map used to maintain the original statement ordering
typedef UtMap<const NUStmt *,SInt32> StmtPositionMap;

//! Map from net to the statements which def that statement.
typedef UtMultiMap<NUNet*,NUStmt*> StmtDefMap;

//! Pair of StmtDefMap iterators
typedef std::pair<StmtDefMap::iterator,StmtDefMap::iterator> StmtDefMapRange;

//! Atomize always blocks into independent components.
/*! 
  A class used to break up Verilog always blocks. The result of this
  breaking will be independent components (either new always blocks or
  continuous assignments).
 */
class Atomize
{
public:
  //! Constructor
  /*!
    \param str_cache       The string cache.
    \param net_ref_factory The NUNetRef factory.
    \param iodb            The IODB.
    \param msg_ctx         The message context.
    \param alloc_alias     Allocation alias query object.
    \param verbose         Print debugging information?
    \param stats           Statistics object.
    \param update_ud       If true, UD will be recalculated upon change.
   */
  Atomize(AtomicCache * str_cache,
          NUNetRefFactory * net_ref_factory,
          IODBNucleus * iodb,
          ArgProc* args,
          MsgContext * msg_ctx,
          AllocAlias * alloc_alias,
          bool verbose,
          AtomizeStatistics * stats,
          bool update_ud);

  //! Destructor
  ~Atomize();

  //! Process all always blocks contained within this module.
  /*!
    Two operations are performed:
   
    1. Multi-statement always blocks are separated into independent
       components.

    2. Single-assignment combinational blocks are converted into
       continuous assignments.

    UD is recomputed upon any change.

    \param module The module for consideration.
    \return true if any modification was made.

    \sa atomizeBlocks
    \sa convertBlocks
   */
  bool module(NUModule * this_module);
private:

  //! Atomize always blocks contained within this module.
  /*!
    For each always block contained in this module, attempt to split
    it into independent components via ::atomizeBlock().

    The set of always blocks contained within this module will be
    altered upon success.

    UD information is inaccurate if this method returns true. It is
    expected that ::module() will fully recompute module UD.

    \param module The module under consideration.
    \return true if any modification was made.
    \sa atomizeBlock
   */
  bool atomizeBlocks(NUModule * this_module);

  //! Convert single-assignment combinational blocks into continuous assignments.
  /*!
    For each always block contained in this module, attempt to convert
    it into a continuous assignment via ::convertBlock().

    The set of always blocks and continuous assignments contained
    within this module will be altered upon success.

    UD information is inaccurate if this method returns true. It is
    expected that ::module() will fully recompute module UD.

    \param module The module under consideration.
    \return true if any transformation was made.
    \sa convertBlock
   */
  bool convertBlocks(NUModule * this_module);

  //! Split an always blocks into disjoint parts.
  /*!
    If this method returns true, the provided always block no longer
    contains any statements. The generated always blocks have been
    added to the 'replacements' list. The caller must delete the
    original always block and add the generated blocks to the parent
    module.

    UD information is inaccurate if this method returns true. It is
    expected that ::module() will fully recompute module UD.

    \param always The always block under consideration.
    \param replacements Populated with the generated replacement
               always blocks upon success; empty otherwise.
    \param possible_aliases Seed set for net closure, contains the nets in the
               module which are potentially elaboratedly aliased.

    \return true if any modification was made
    \sa validAtomizationBlock
    \sa buildStmtDependencyInfo
    \sa createIndependentBlocks
   */
  bool atomizeBlock(NUAlwaysBlock * always,
                    NUAlwaysBlockList * replacements,
                    NUNetSet * possible_aliases);

  //! Convert single-assignment always blocks into continuous assignments.
  /*! 
    If an always block is combinational and contains a single
    assignment, convert the contained statement into a continuous
    assignment.

    If this method returns true, a continuous assignment has been
    generated. The caller add the new continuous assignment to the
    module. Further, the original always block must be deleted and
    removed from the module.

    \param always The always block under consideration.
    \param replacement Populated with the generated replacement
               continuous assignment; NULL otherwise.

    \return true if the always block has been converted into a continuous assignment.
    \sa validConversionBlock
   */
  bool convertBlock(NUAlwaysBlock * always,
                    NUContAssign  ** replacement);

  //! Perform simple checks to determine if the block can be atomized.
  /*!
    Blocks with any of the following characteristics are not
    considered valid for atomization:
    
    1. Part of an async-reset clock/priority tree
    2. Contains locals

    \param always The always block under consideration.
    \return true if valid for atomization
   */
  bool validAtomizationBlock(NUAlwaysBlock * always);

  //! Build up dependency information for the statements contained within an always block.
  /*!
    Process the statements contained within the always block in turn. 

    For each statement:
    
        1. Populate position_map with the statement's position within
           the block.

        2. Remember which nets are defined by the statements

        3. Add this statement's set of dependent nets to the closure.

           The set of dependent nets is the union of:
           A. Nets defined by the statement.

           B. Nets used by the statement defined by a previous
              statement.

    The set closure operation is used to create independent sets for
    unrelated nets and combine dependencies if there is any set
    overlap. The results are available in 'closure'.

    NOTE: This analysis is pessimistic in the sense that it assumes that all nets
          which are potentially elaboratedly aliased, can be elaboratedly aliased
          together.  This is not strictly true, as port sizes must match.  Currently,
          (6/10/05) Atomize is run only before port sizes have been rewritten, so
          nothing is being missed.  If, in the future, cbuild is modifed to run
          Atomize after port splitting, this routine should be modified to do pairwise
          net comparisions, so that size can be taken into effect.

    \param always       The always block under consideration.
    \param position_map Map from statement to its position within the always block.
    \param def_map      Multi-map from net to statements which define that net.
    \param closure      Net closure for interdependent nets.
    \param possible_aliases Nets in the module which are potentially elaboratedly aliased.

    \return true if the dependency information was successfully built.
   */
  bool buildStmtDependencyInfo(NUAlwaysBlock   * always,
                               StmtPositionMap & position_map,
                               StmtDefMap      & def_map,
                               NUNetClosure    * closure,
                               NUNetSet        * possible_aliases);

  //! Generate independent blocks from dependency information.
  /*!
    For each independent set defined by the set closure:

    1. Locate the statements which define nets in the set.

    2. Sort these statements according to their original position.

    3. Add these statements to a new always block (copying edge
       expressions if necessary).

    \param always       The always block under consideration.
    \param position_map Map from statement to its position within the always block.
    \param def_map      Multi-map from net to statements which define that net.
    \param closure      Net closure for interdependent nets.
    \param replacements Populated with the generated replacement
                        always blocks upon success; empty otherwise.
   */
  void createIndependentBlocks(NUAlwaysBlock     * always,
                               StmtPositionMap   & position_map,
                               StmtDefMap        & def_map,
                               NUNetClosure      * closure,
                               NUAlwaysBlockList * replacements);

  //! Perform simple checks to determine if the block can be converted to a continuous assign.
  /*!
    Blocks with any of the following characteristics are not
    considered valid for convertion to continuous assignment:
    
    1. Sequential
    2. Contains locals

    \param always The always block for consideration.
    \return true if valid for conversion.
   */
  bool validConversionBlock(NUAlwaysBlock * always);

  //! Add the module nets which are potential elaborated aliases of each other.
  /*!
    This is pessimistic; it assumes that ports, hierrefs, and hierarchically-referenced
    nets may all be aliased.  This is a necessary view in the absence of elaborated
    information about exactly which aliases may occur.
   */
  void populatePossibleAliasSet(NUModule * this_module,
                                NUNetSet * possible_aliases);

  //! The string cache
  AtomicCache * mStrCache;
  
  //! The NUNetRef factory.
  NUNetRefFactory * mNetRefFactory;
  
  //! IODB
  IODBNucleus * mIODB;

  //! Argument processing
  ArgProc * mArgs;

  //! Message context
  MsgContext * mMsgContext;

  //! Alloc alias query object
  AllocAlias * mAllocAlias;

  //! Should we print/collect debugging information?
  bool mVerbose;

  //! Statistics object.
  AtomizeStatistics * mStatistics;

  //! Do we need to update UD information?
  bool mUpdateUD;
};



//! Functor used to sort based on original statement order.
/*!
  Given a statement position map, order statements based on the stored
  position of each statement.
 */
struct StmtPositionCmp
{
public:
  //! Constructor
  /*!
    \param position_map Stores relation between statements and original position.
   */
  StmtPositionCmp(StmtPositionMap & position_map);

  //! Comparator: Returns true if s1 has a position earlier in the current always block than s2.
  bool operator()(const NUStmt * s1, const NUStmt * s2);
private:
  //! Relation between statements and original position.
  StmtPositionMap & mPositionMap;
};


//! Atomization statistics class
/*!
  Collect information about atomization success.
 */
class AtomizeStatistics
{
public:
  //! Constructor
  AtomizeStatistics();

  //! Destructor
  ~AtomizeStatistics();

  //! Modified a module
  void module();

  //! Atomized an always block
  void source();

  //! Increment the number of decompositions.
  void target();

  //! Converted an always block to cont assign.
  void assign();

  //! Dump the collected statistics.
  void print() const;
private:
  //! Clear the collected statistics.
  void clear();

  //! The number of affected modules.
  SInt32 mModules;

  //! The number of atomized blocks.
  SInt32 mAtomizeSources;

  //! The number of decompositions.
  SInt32 mAtomizeTargets;

  //! The number of generated cont assigns
  SInt32 mAssigns;
};

#endif
