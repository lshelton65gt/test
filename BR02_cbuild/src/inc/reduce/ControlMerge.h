// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CONTROL_MERGE_H_
#define CONTROL_MERGE_H_

#include "nucleus/Nucleus.h"

/*!
  \file
  Declaration of control merge package.
 */

class NUNetRefFactory;
class MsgContext;
class AtomicCache;
class ArgProc;
class IODBNucleus;

class Inference;
class InferenceStatistics;
class Fold;
class UD;
class ReduceUtility;
class ControlMergeStatistics;

/*!
  Chunk: Sorted set of always blocks which define adjacent parts of
  the same net. Sorted by defined netref.
*/
class Chunk
{
public:
  Chunk(NUNetRefFactory * netref_factory) :
    mNetRefFactory(netref_factory)
  {}

  ~Chunk() {}

  typedef NUAlwaysBlockVector::iterator iterator;
  typedef NUAlwaysBlockVector::const_iterator const_iterator;
  typedef NUAlwaysBlockVector::reverse_iterator reverse_iterator;
  typedef NUAlwaysBlockVector::const_reverse_iterator const_reverse_iterator;
  typedef NUAlwaysBlockVector::value_type value_type;

  iterator begin() { return mElements.begin(); }
  iterator end() { return mElements.end(); }

  const_iterator begin() const { return mElements.begin(); }
  const_iterator end() const { return mElements.end(); }

  reverse_iterator rbegin() { return mElements.rbegin(); }
  reverse_iterator rend() { return mElements.rend(); }

  const_reverse_iterator rbegin() const { return mElements.rbegin(); }
  const_reverse_iterator rend() const { return mElements.rend(); }

  void push_back(value_type element) { mElements.push_back(element); }
  void extend(Chunk & other) { extend(other.mElements); }
  void extend(NUAlwaysBlockVector & v) {
    mElements.insert(mElements.end(),v.begin(),v.end());
  }

  void clear() { mElements.clear(); }

  int size() const { return mElements.size(); }
  bool empty() const { return mElements.empty(); }

  NUAlwaysBlock * collate();
  NUAlwaysBlock * collate(NUAlwaysBlock * a, NUAlwaysBlock * b);
  NUStmt * collate(NUStmt *a, NUStmt *b);
  NUBlock * collate(NUBlock *a, NUBlock *b);
  NUIf * collate(NUIf *a, NUIf *b);
  NUCase * collate(NUCase *a, NUCase *b);

  //! Determine which stmts from b we preserve and which will be deleted.
  void collate(NUStmtLoop aLoop,
	       NUStmtLoop bLoop,
	       NUStmtList &savStmts,
	       NUStmtList &delStmts);

  void print() const;

private:
  NUNetRefFactory * mNetRefFactory;
  NUAlwaysBlockVector mElements;
};
typedef UtList<Chunk> Chunks;

/*!
  Traverse a design searching for always blocks with identical control
  structures.

  Given two always blocks with identical control structures, create a
  new always block with a single control structure and joined data
  paths.

  In the simplest example, the two always blocks:

    always begin
      a[0] = b[0];
    end

    always begin
      a[1] = b[1];
    end

  will be merged into the single always block:

    always begin
      a[0] = b[0];
      a[1] = b[1];
    end

  With a proper ordering of data path, we can vectorize assignments:

    always begin
      a[0:1] = b[0:1];
    end

  Of course, this becomes more interesting when processing always
  blocks with complex control structures. For example:

    always begin
      if (rst) begin
        a[0] = 1'b0;
      end else if (read_b) begin
        a[0] = b[0];
      end else if (read_c) begin
        a[0] = c[0];
      end else begin
        a[0] = d[0];
      end
    end

  If this always block were replicated for each bit of 'a', this
  object will collapse all of those blocks into a single always block.
  The combined block will contain one control tree and data
  assignments in the proper location.

    always begin
      if (rst) begin
        a[0] = 1'b0;
        a[1] = 1'b0;
        a[2] = 1'b0;
      end else if (read_b) begin
        a[0] = b[0];
        a[1] = b[1];
        a[2] = b[2];
      end else if (read_c) begin
        a[0] = c[0];
        a[1] = c[1];
        a[2] = c[2];
      end else begin
        a[0] = d[0];
        a[1] = d[1];
        a[2] = d[2];
      end
    end

  After vectorization, this becomes:

    always begin
      if (rst) begin
        a[0:2] = {3{1'b0}};
      end else if (read_b) begin
        a[0:2] = b[0:2];
      end else if (read_c) begin
        a[0:2] = c[0:2];
      end else begin
        a[0:2] = d[0:2];
      end
    end

  The initial implementation will focus on merging blocks which define
  different bits of the same vector, but this is not a firm
  constraint. Future extensions could successfully merge blocks
  defining different nets, different control structures, etc.

  Example (different def nets):

    always
      if (sel) x = y;
    always 
      if (sel) z = w;

  becomes:

    always 
      if (sel) begin
        x = y;
        z = w;
      end

  Example (partial control structure overlap):

    always
      if (sel) 
        if (read)
          x = y;
    always
      if (sel)
        z = w;

  becomes:

    always
      if (sel) begin
        if (read)
	  x = y;
        z = w;
      end

  Limitations:

  1) Always blocks must define one net.
  2) Always blocks must define non-overlapping net-refs.
  3) Defined net cannot be:
     a) used in the same block
     b) port of containing module
     c) port of submodule
     This constraint prevents the self-cyclic error described in
     Bug662.
  4) Always block must contain one statement.
  5) Each control branch must contain one statement.
  6) Each assignment must define the same netref as defined by the
     entire always block.

  Note: Constraints 1,4-6 may be relaxed.
 */
class ControlMerge
{
public:
  //! Constructor
  ControlMerge(AtomicCache * str_cache,
	       NUNetRefFactory * netref_factory,
	       MsgContext * msg_context, 
	       IODBNucleus * iodb,
	       ArgProc *args,
	       bool verbose,
	       ControlMergeStatistics *stats,
	       InferenceStatistics *inference_stats);

  //! Destructor
  ~ControlMerge();

  //! Walk a module and its instantiations.
  void module(NUModule * one);

private:
  AtomicCache     * mStrCache;
  NUNetRefFactory * mNetRefFactory;
  MsgContext      * mMsgContext;
  ArgProc         * mArgs;

  UD        * mUD;
  Fold      * mFold;
  Inference * mInference;

  ControlMergeStatistics * mStatistics;

  //! Be verbose as processing occurs?
  bool mVerbose;
};

//! Perform control-based merging over the always blocks in one module.
/*!
  Maintains state and operates over each block in a single module.
 */
class ControlMergeModule
{
public:
  //! Constructor
  ControlMergeModule(NUModule* module,
		     NUNetRefFactory * netref_factory,
		     ControlMergeStatistics * statistics,
		     bool verbose);
  
  //! Destructor
  ~ControlMergeModule();

  //! Perform the search over all always blocks
  /*!
   */
  bool merge();

private:
  typedef UtMap<NUNet*,NUAlwaysBlockVector> ValidNetDrivers;
  ValidNetDrivers mValidNetDrivers;

  //! Find the set of always blocks which match the criteria defined by validate().
  void findCandidates();

  //! Determine if a single always block is a valid merge candidate.
  /*!
    Current constraints:
    1) always block defines one net.
    2) named block contains one statement.
    3) that one statement is one of:
       a) blocking assign
       b) branching stmt (case or if)

    Matching blocks are added to the mValidNetDrivers map.
   */
  void validate(NUAlwaysBlock * always);

  //! Ensure that an always block defines the same netref in each stmt.
  bool checkDefConsistency(NUAlwaysBlock * always,
			   NUNetRefHdl & def_net_ref);

  //! Create always block chunks.
  void filterCandidates();

  //! Per def net, create always block chunks.
  void filterCandidates(NUAlwaysBlockVector & blocks);

  //! Create isomorphic always block chunks.
  void filterIsoChunks();

  //! From always block chunks, create isomorphic always block chunks.
  void filterIsoChunks(Chunk & chunk);

  //! Determine if two always blocks have the same control structure.
  bool iso(NUAlwaysBlock * a, NUAlwaysBlock * b);

  //! Determine if two stmts have the same control structure.
  bool iso(NUStmt *a, NUStmt *b);

  //! Determine if two named blocks have the same control structure.
  bool iso(NUBlock *a, NUBlock *b);

  //! Determine if two if stmts have the same control structure.
  bool iso(NUIf *a, NUIf *b);

  //! Determine if two case stmts have the same control structure.
  bool iso(NUCase *a, NUCase *b);

  //! Determine if statement loops have the same control structure.
  bool iso(NUStmtLoop aLoop, NUStmtLoop bLoop);

  //! Merge all ISO always block chunks into combined blocks.
  bool mergeIsoChunks();

  //! Merge single ISO always block chunk into a new block.
  bool mergeIsoChunk(Chunk & chunk);

  //! Print a set of chunks.
  void printChunks(Chunks * chunks) const;

  //! Groups of adjacent always blocks.
  Chunks mChunks;

  //! Groups of adjacent, isomorphic always blocks.
  Chunks mIsoChunks;

  //! Groups of successfully merged chunks.
  Chunks mMergedChunks;

  NUModule * mModule;
  NUNetRefFactory * mNetRefFactory;
  ControlMergeStatistics * mStatistics;
  ReduceUtility * mUtility;
  bool mVerbose;
};

//! Maintain a tally of affected blocks/modules.
class ControlMergeStatistics
{
public:
  //! Constructor
  ControlMergeStatistics() { clear(); }

  //! Destructor
  ~ControlMergeStatistics() {}

  void module() { ++mModules; }
  void source() { ++mSources; }
  void target() { ++mTargets; }
  void clear() { mModules=0; mSources=0; mTargets=0; }
  void print() const;

private:
  SInt64 mModules;
  SInt64 mSources;
  SInt64 mTargets;

};
#endif
