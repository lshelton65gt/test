// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef REMOVEELABFLOW_H_
#define REMOVEELABFLOW_H_

#include "flow/FLFactory.h"

/*!
  \file
  Declaration of flow removal package.
 */

//! Remove elab flow class
/*!
 * This class gets rid of elaborated flow in the factory.
 * Nucleus is not deleted, but the drivers are removed from the NUNet's.
 */
class RemoveElabFlow
{
public:
  //! constructor
  RemoveElabFlow(FLNodeElabFactory *dfg_factory, STSymbolTable* symtab) :
    mFlowFactory(dfg_factory), 
    mSymbolTable(symtab)
  {}

  //! destructor
  ~RemoveElabFlow() {}

  //! Remove the flow
  void remove();

private:
  //! Hide copy and assign constructors.
  RemoveElabFlow(const RemoveElabFlow&);
  RemoveElabFlow& operator=(const RemoveElabFlow&);

  //! DFG Factory
  FLNodeElabFactory *mFlowFactory;
  STSymbolTable* mSymbolTable;
};

#endif
