// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef FLATTEN_H_
#define FLATTEN_H_

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUCopyContext.h"

#include "reduce/FlattenAnalyzer.h"

/*!
  \file
  Declaration of flattening package.
 */

class AtomicCache;
class MsgContext;
class IODBNucleus;
class ArgProc;

class RewriteLeaves;
class LeafConsistency;
class FlattenStatistics;

class STBranchNode;
class STAliasedLeafNode;

class Fold;

class FlattenHierHelper;

class Flatten
{
public:
  //! Constructor
  /*!
    \param design the design
    \param str_cache String cache (unused)
    \param msg_ctx The message context
    \param design_cost_ctx Design cost context, in which the design cost is assumed to have been computed
    \param inline_limit Maximum size (in # of assigns) of modules being flattened.
    \param flatten_only_leaves Flatten only leaf modules (not hierarchy with submodules).
    \param flatten_single_instances Flatten singly-instantiated modules, regardless of cost.
    \param early_flattening_pass True during the early flattening pass; disallows all port lowering operations.
    \param debugging Are we running with -g?
    \param verbose If 0, silent. If != 1, will be verbose.
    \param stats Statistics object to keep tallys in
   */
  Flatten(NUDesign * design,
          AtomicCache *str_cache,
          NUNetRefFactory *netref_factory,
          MsgContext *msg_ctx,
          IODBNucleus * iodb,
          ArgProc* args,
          NUCostContext * design_cost_ctx,
          UtOStream * trace_file,
          SInt32 inline_limit,
          bool flatten_only_leaves,
          bool flatten_single_instances,
          bool flatten_hier_ref_tasks,
          bool early_flattening_pass,
          bool debugging,
          bool verbose,
          UInt32 tiny_threshold,
          UInt32 parent_threshold,
          FlattenStatistics *stats);

  //! Destructor
  ~Flatten();

  //! Walk a module and conditionally flatten its submodules.
  /*!
    \param module  The module to transform.
    \param flattenMode

    \return true if any flattening occurred.
   */
  bool module(NUModule * this_module, FlattenAnalyzer::Mode flattenMode);

private:

  //! Debug display routine.
  void dumpFlattening(UtOStream & out,
                      NUModule * this_module,
                      NUModuleInstance * instance, 
                      FlattenAnalyzer::Status status, 
                      FlattenAnalyzer::FlattenSize * size);

  //! The design.
  NUDesign * mDesign;

  //! String cache
  AtomicCache *mStrCache;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! Message context.
  MsgContext *mMsgContext;
  
  //! IODB
  IODBNucleus * mIODB;

  //! args
  ArgProc* mArgs;

  //! Precomputed design cost context
  NUCostContext * mDesignCostCtx;

  //! Trace file.
  UtOStream * mTraceFile;

  //! Maximum size (in # of assigns) of modules being flattened.
  SInt32 mInlineLimit;

  //! Cost context.
  NUCostContext * mCosts;

  //! Fold context
  Fold* mFold;
  
  FlattenStatistics * mStatistics;

  //! Only allow flattening of leaf instances.
  bool mFlattenOnlyLeaves;

  //! Flatten singly-instantiated modules regardless of size.
  bool mFlattenSingleInstances;

  //! Allow flattening of modules containing tasks with hierarchical references.
  /*!
   * This is controlled separately because flattening can cause
   * situations where, unelaboratedly, a task enable will have
   * multiple resolutions.
   */
  bool mFlattenHierRefTasks;

  //! True during the early flattening pass; disallows all port lowering operations.
  bool mEarlyFlatteningPass;

  bool mDebugging;

  //! If true, will be verbose about skipped inlining opportunities.
  bool mVerbose;

  //! how big can a module be and still ignore ParentSizeLimit?
  UInt32 mTinyThreshold;

  //! avoid flattening modules into parents of this size unless they are tiny
  //! (see mTinyThreshold)
  UInt32 mParentSizeLimit;
};


class FlattenFixupSysTasks : public NUDesignCallback
{
public:
  //! constructor
  FlattenFixupSysTasks(FlattenHierHelper* hierHelper) : 
    mHierHelper(hierHelper)
  {}

  //! destructor
  ~FlattenFixupSysTasks() {}

  //! Ignore everthing else
  Status operator()(Phase, NUBase*) { return eNormal; }

  //! We are manually walking tasks; don't visit them through task enables.
  Status operator()(Phase, NUTaskEnable*) { return eSkip; }

  //! Overload the systask callback to fixup it up
  Status operator()(Phase phase, NUOutputSysTask* sysTask);

private:
  FlattenHierHelper* mHierHelper;
};

class FlattenModuleInstance 
{
public:
  FlattenModuleInstance(NUDesign * design,
                        NUModule * parent,
                        NUModuleInstance * instance,
                        AtomicCache * string_cache,
                        NUNetRefFactory *netref_factory,
                        IODBNucleus * iodb,
                        MsgContext * msg_context,
                        Fold* fold,
                        bool early_flattening_pass,
                        bool debugging);

  ~FlattenModuleInstance();

  FlattenAnalyzer::Status flatten();

  static FlattenAnalyzer::Status qualifyConcatInputPortConnection(NUConcatOp *concat,
                                                                  NUNet *formal,
                                                                  NUNetSet &no_scalarize_nets);
  static FlattenAnalyzer::Status qualifyConcatOutputPortConnection(NUConcatLvalue *concat,
                                                                   NUNet *formal,
                                                                   NUNetSet &no_scalarize_nets);

  //! Alias upper and lower.
  /*!
    \param first Net which will be used in the tree
    \param second Net which will be replaced by first in the tree
   */
  void alias(NUNet * first, NUNet * second);

private:
  //! Create copies of module constructs
  FlattenAnalyzer::Status cloneModule();

  //! Fixup hierref resolutions due to aliasing.
  void fixupResolutions(NUNet * first, NUNet * second);

  //! Rewrite the cloned constructs.
  /*!
   * Replace all references of sub-module nets with references to
   * parent-module nets or generated temporaries.
   */
  void rewriteConstructs();

  //! Move all the cloned/rewritten constructs into the parent module.
  void promoteConstructs();

  //! Clone the Nucleus tree for the submodule.
  void cloneConstructs();

  //! Add the memories from the net replacement map to the copy context
  void addMemoriesToRewriteMap(CopyContext & copy_context);

  //! Add the memories from one of our (lhs/rhs) replacement maps to the copy context.
  template<typename MapType> 
  void extractMemoriesFromMap(CopyContext & copy_context,
                              MapType & replacement_map);

  //! Clone locals within the submodule.
  void cloneLocals();

  //! Clone locals within a given scope.
  void cloneScopeLocals(NUScope * scope);

  //! Clone a single local net.
  void cloneOneLocal(NUNet * local);

  bool isTiedNet(NUNet * net) const;

  /*!
   * Try to find a hierarchical reference in the parent module or the
   * set of already encountered hier-refs which refers to the same net
   * as the passed in hierarchical reference. Return the match, if
   * found. Return 0 if not found.
   */
  NUNet* findMatchingHierref(NUNet *net);

  /*!
   * Try to find a hierarchical reference from a set of known hierrefs.
   */
  void findMatchingHierrefFromLoop(NUNetLoop loop, 
                                   NUNet ** parent_net, 
                                   NUNet * net, 
                                   bool do_consistency_check);

  /*!
   * See if the given hierarchical reference is resolved at the parent level.
   * Return the parent net it resolves to, or return 0 if there are no parent
   * resolutions.
   */
  NUNet* findLocalResolution(NUNet *net);
  
  //! Clone all hierarchical references of the child into the parent.
  void cloneHierRefs();

  //! Clone a hierref in the submodule into the parent module.
  /*!
   * 2 cases:
   *  1 - The hierref already exists in the parent module (ie, both child and
   *      parent are referencing the same hierarchical net).  Alias the existing
   *      parent hierref to the child hierref.
   *  2 - The hierref does not exist in the parent module.  Create a hierref
   *      in the parent, alias that hierref to the child hierref.
   */
  void cloneHierRef(NUNet *net);

  //! Copy all hierarchical references from the original task to its flattened replacement.
  /*!
   * FlattenResolveHierRefs::resolveTasks will later come and remove
   * any excessive hierarchical references.
   */
  void cloneTaskHierRefs(NUTask *original, NUTask *replacement);

  void cloneTasks(CopyContext & copy_context);

  void cloneInstances(CopyContext & copy_context);

  void cloneAssigns(CopyContext & copy_context);
  void cloneAlways(CopyContext & copy_context);
  void cloneInitials(CopyContext & copy_context);
  void cloneCModels(CopyContext & copy_context);
  void cloneTriRegInits(CopyContext & copy_context);
  void cloneRandomInits(CopyContext & copy_context);
  void cloneEnabledDrivers(CopyContext & copy_context);

  //! Create a locally named version of the original
  NUNet * clone ( NUNet * original,
		  NUScope * scope,
		  NUNetReplacementMap & net_replacements,
		  bool preserve_true_name=false );
  NUModuleInstance * clone ( const NUModuleInstance * original,
			     CopyContext & copy_context ) ;
  NUAlwaysBlock * clone ( const NUAlwaysBlock * original,
			  CopyContext & copy_context );
  NUInitialBlock * clone ( const NUInitialBlock * original,
			   CopyContext & copy_context );
  NUTask * clone ( NUTask * original,
		   CopyContext & copy_context );
  NUCModel * clone ( NUCModel * original,
			     CopyContext & copy_context );
  NUTriRegInit * clone ( NUTriRegInit * original,
			 CopyContext & copy_context );
  NUContAssign * clone ( NUEnabledDriver * original,
			 CopyContext & copy_context );

  FlattenAnalyzer::Status addPortConnections(NUNetSet &no_scalarize_nets);
  FlattenAnalyzer::Status addInputPortConnection(NUExpr * actual, NUNet * formal, NUNetSet &no_scalarize_nets);
  FlattenAnalyzer::Status addOutputPortConnection(NULvalue * actual, NUNet * formal, NUNetSet &no_scalarize_nets);

  void promoteNetFlags(NUNet * under, NUNet * net);
  void promoteNetFlagsConcatRvalue(NUConcatOp *concat, NUNet *formal);
  void promoteNetFlagsConcatLvalue(NUConcatLvalue *concat, NUNet *formal);

  void rememberAlias(NUNet * upper, NUNet * lower);

  bool checkDimensions(const NUNet *formal, const NULvalue *actual);
  bool checkDimensions(const NUNet *formal, const NUExpr   *actual);

  void rewriteTasks(RewriteLeaves & translator, FlattenFixupSysTasks*);
  void rewriteAssigns(RewriteLeaves & translator);
  void rewriteInstances(RewriteLeaves & translator);
  void rewriteAlways(RewriteLeaves & translator, FlattenFixupSysTasks*);
  void rewriteInitials(RewriteLeaves & translator, FlattenFixupSysTasks*);
  void rewriteTriRegInits(RewriteLeaves & translator);
  void rewriteRandomInits(RewriteLeaves & translator);

  //! Review all constructs and ensure nothing dangles into the inlined scope.
  void inlineConsistency();
  void localConsistency();
  void taskConsistency();
  void instanceConsistency(LeafConsistency & consistency);
  void assignConsistency(LeafConsistency & consistency);
  void alwaysConsistency(LeafConsistency & consistency);
  void initialConsistency(LeafConsistency & consistency);

  void promoteLocals();
  void promoteAliases();

  void promoteTasks();
  void promoteInstances();
  void promoteAssigns();
  void promoteAlways();
  void promoteInitials();
  void promoteCModels();
  void promoteTriRegInits();
  void promoteRandomInits();
  void promoteEnabledDrivers();

  void destroyLocals();

  //! Locally-declared nets in no particular order.
  NUNetList mLocalNetList;

  //! Locally-declared hierref nets in no particular order.
  NUNetList mLocalNetHierRefList;

  //! Pending alias relationships.
  typedef UtMultiMap<NUNet*,NUNet*> MasterToSlave;
  MasterToSlave mPendingAliases;

  //! List of continuous assigns.
  NUContAssignList mContAssignList;

  //! List of instantiations.
  NUModuleInstanceList mInstanceList;

  //! List of always blocks.
  NUAlwaysBlockList mAlwaysBlockList;

  //! List of initial blocks.
  NUInitialBlockList mInitialBlockList;

  // ACA: NEED DECLARATIONSCOPE LIST

  //! Vector of tasks
  NUTaskVector mTaskVector;

  //! Vector of cmodels
  NUCModelVector mCModelVector;

  //! List of tri reg inits
  NUTriRegInitList mTriRegInitList;

  //! The design.
  NUDesign * mDesign;

  //! my module
  NUModule * mModule;

  NUModule * mSubmodule;
  NUModuleInstance * mInstance;

  //! String cache
  AtomicCache *mStrCache;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! IODB
  IODBNucleus * mIODB;

  //! Message context.
  MsgContext *mMsgContext;

  //! Fold context, used for folding flattened constructs
  Fold * mFold;

  //! True during the early flattening pass; disallows all port lowering operations.
  bool mEarlyFlatteningPass;
  
  bool mDebugging;

  FlattenHierHelper * mHierHelper;

  //! Replacements for nets (generated temporaries or exact port matches).
  NUNetReplacementMap     mNetReplacements;

  //! Replacements based on LHS expressions.
  NULvalueReplacementMap  mLvalueReplacements;

  //! Replacements based on RHS expressions.
  NUExprReplacementMap    mRvalueReplacements;

  //! Replacements specifically for uses of input ports connected to tie nets.
  NUExprReplacementMap    mTieNetRvalueRHSReplacements;

  //! Replacements specifically for uses of output/bid ports connected to tie nets.
  NULvalueReplacementMap  mTieNetLvalueRHSReplacements;

  //! Replacements for tasks.
  NUTFReplacementMap      mTFReplacements;

  //! Replacements for cmodels.
  NUCModelReplacementMap  mCModelReplacements;
  
  //! Replacements for always blocks (used to fixup clk/priority ptrs)
  NUAlwaysBlockReplacementMap  mAlwaysReplacements;
};


//! Interface class for relative elaboration (during flattening) helpers.
/*!
 * Encapsulate a number of methods which help translate from the
 * Nucleus constructs and unelaborated symbol table entries of a child
 * module to entries in the unelaborated symbol table of a parent
 * module.
 */
class FlattenHierHelper
{
public:
  //! Constructor
  /*!
   * \param parent   The current parent module.
   * \param instance A module instance within the current parent
   *     module; references the submodule we are flattening into the
   *     parent.
   */
  FlattenHierHelper(NUModule * parent,
                    NUModuleInstance * instance);

  //! Destructor
  ~FlattenHierHelper();

  //! Create leaf entries associated with a flattened net within a parent module's unelaborated symbol table.
  /*!
   * \param lower A net declared within an instantiated submodule.
   *     Symbol table entries will be created within the parent module
   *     for * this net and all its aliases. These entries will all be
   *     prefixed * by the instantiation path. Net 'q' will have
   *     symbol-table entry * 'sub.q' within the parent module.
   *
   * \returns The symbol-table entry for 'lower' within the current
   *     parent module.
   *
   * \sa branchForInstance, cloneLeafWithRoot
   */
  STAliasedLeafNode * leafForInstanceNet(NUNet * lower);

  //! Create branch entries associated with a flattened task/function within a parent module's unelaborated symbol table.
  /*!
   * The BOM NUScope entry is NULL upon return; the caller is expected
   * to update the scope information after creating the new T/F.
   *
   \code
       STBranchNode branch = branchForInstanceTF(originalTF);
       NUTF * tf = new NUTask (...,branch,...);
       NUAliasDataBOM * bom = NUAliasBOM::castBOM(branch->getBOMData());
       bom->setScope(tf);
   \endcode
   *
   * \param original A task/function declared within an instantiated
   *     submodule. Symbol table entries will be created within the
   *     parent module for this task/function. These entries will all
   *     be prefixed by the instantiation path. Task 't' will have
   *     symbol-table entry 'sub.t' within the parent module.
   *
   * \returns The symbol-table entry for 'original' within the current
   *     parent module.
   *
   * \sa branchForInstance, cloneBranchWithRoot
   */
  STBranchNode * branchForInstanceTF(NUTF * original);

  //! Create an systask branch node with a parent module's unelaborated sym tab
  void branchForInstanceOutSysTask(NUOutputSysTask* outSysTask);

  //! Create a branch node as a path to nets in the submodule (corresponds to the name of the module instance).
  STBranchNode * branchForInstance();

  //! Create a branch node as a path to nets in the submodule (corresponds to the name of the declaration/module scope).
  STBranchNode* branchForInstanceScope(NUScope* instScope);

  //! Find a branch node as an unelaborated path for the declaration scope in the given unelaborated symbol table.
  static STBranchNode* findBranchForDeclScope(NUNamedDeclarationScope* declScope,
                                              STSymbolTable* symtab);

protected:
  /*! 
   * Direct access to leaf creation is only allowed for the
   * reconstruction data rewriter. All other creation attempts should
   * go through ::leafForInstanceNet().
   */
  friend class FlattenTransformWalker;
  /*!
   * Class that transforms original net back pointers to their
   * flattened equivalents.
   */
  friend class BackPointerWalker;
  
  //! Create rooted entries within a parent module's unelaborated symbol table.
  /*!
   * The source leaf-node and all its aliases are copied into the
   * paren't module's unelaborated symbol table using the specified
   * root.
   *
   * If 'source' references 'q' in the following alias ring:
   \verbatim
     { q, y.q, y.z.q }
   \endverbatim
   * 
   * and 'root' is the hierarchy 'x', we will create a new alias ring:
   \verbatim
     { x.q, x.y.q, x.y.z.q }
   \endverbatim
   *
   * \param source A symbol table leaf declared within an instantiated
   *     submodule.
   * \param root The symbol table node that will root the copy of
   *     'source' and its alias ring.
   *
   * \returns A symbol table leaf node copied from 'source' rooted at
   *     'root'.
   *
   * \sa cloneLeafWithRoot
   */
  STAliasedLeafNode * createLeafWithRoot(const STAliasedLeafNode * source, STBranchNode * root);

private:

  //! Create a branch node for a flattened object given its original branch
  STBranchNode* branchForInstance(STBranchNode* original_branch);

  //! Create rooted, populated symbol table entries.
  /*!
   * Create a symbol-table entry with ::createLeafWithRoot() and
   * ensure its BOM fields are populated.
   *
   * \param source A symbol table leaf declared within an instantiated
   *     submodule.
   * \param root The symbol table node that will root the copy of
   *     'source' and its alias ring.
   *
   * \returns A populated symbol table leaf node copied from 'source'
   *     rooted at 'root'.
   */
  STAliasedLeafNode * cloneLeafWithRoot(const STAliasedLeafNode * source, STBranchNode * root);

  //! Translate the reconstruction data for this symbol-table node.
  void cloneReconData(const STAliasedLeafNode * source_leaf, STBranchNode * root);

  //! Recursively copy a source branch node and base it at the named root.
  STBranchNode * cloneBranchWithRoot(const STBranchNode * source, STBranchNode * root);

  //! The parent module/flattening target
  NUModule * mModule;

  //! Module instance inside our parent module referring to submodule.
  NUModuleInstance * mInstance;

  //! Submodule of parent module instantiated through the module instance.
  NUModule * mSubmodule;
};


//! Resolve hierrefs for a module which have been exposed by flattening.
/*!
 * If a module has a hierarchical reference into a child, and the child is
 * flattened into it, then the hierarchical reference into the child can be
 * replaced by the actual net.
 */
class FlattenResolveHierRefs
{
public:
  //! Constructor
  FlattenResolveHierRefs(NUModule *module, 
                         NUNetRefFactory * netref_factory,
                         Fold* fold);

  //! Destructor
  ~FlattenResolveHierRefs();

  /*!
   * Do the work; see if any of the module's hierrefs are resolvable.  If so,
   * resolve them by replacing the hierrefs with the actual object.
   */
  void resolve();

  /*!
   * Do the work; see if any of the module's hierrefs are resolvable.  If so,
   * resolve them by replacing the hierrefs with the actual nets.
   */
  void resolveNets();

  /*!
   * Do the work; see if any of the module's hierrefs are resolvable.  If so,
   * resolve them by replacing the hierrefs with the actual tasks.
   */
  void resolveTasks();

private:
  //! Hide copy and assign constructors.
  FlattenResolveHierRefs(const FlattenResolveHierRefs&);
  FlattenResolveHierRefs& operator=(const FlattenResolveHierRefs&);

  //! The module to process
  NUModule *mModule;

  //! NetRef factory.
  NUNetRefFactory * mNetRefFactory;

  //! Fold
  Fold* mFold;
};


//! TBD
class FlattenTaskEnableFixupCallback : public NUDesignCallback
{
public:
  //! Constructor
  FlattenTaskEnableFixupCallback(NUModule * the_module,
                                 NUNetRefFactory * netref_factory);

  //! Destructor
  ~FlattenTaskEnableFixupCallback();

  //! By default, walk through everything
  Status operator()(Phase /*phase*/, NUBase * /*node*/) { return eNormal; }

  //! Do not walk through module instances; we are only processing one module
  Status operator()(Phase /*phase*/, NUModuleInstance * /*node*/) { return eSkip; }

  //! Do not walk through expressions; we are only concerned with stmts.
  /*!
   * Note: If we ever handle function hier-references, we will need to
   * process expressions.
   */
  Status operator()(Phase /*phase*/, NUExpr * /*node*/) { return eSkip; }

  //! Walk into and locally process the child statements of task/functions.
  Status operator()(Phase phase, NUTF * node);

  //! Walk into and locally process the child statements of blocks.
  Status operator()(Phase phase, NUBlock *node);

  //! Walk into and locally process the child statements of case statements.
  Status operator()(Phase phase, NUCase *node);

  //! Walk into and locally process the child statements of if() statements.
  Status operator()(Phase phase, NUIf *node);

  //! Walk into and locally process the child statements of for() statements.
  Status operator()(Phase phase, NUFor *node);

protected:
  //! Resolve hierarchical references of this task enable
  virtual NUStmt * fixupTaskEnable(NUStmt * enable_stmt);

  //! Netref factory.
  NUNetRefFactory * mNetRefFactory;

private:
  //! Process a list of statements.
  bool stmts(NUStmtLoop loop, NUStmtList & replacements);

  //! The parent module.
  NUModule * mModule;
  
};

class FlattenStatistics
{
public:
  FlattenStatistics() { clear(); }

  ~FlattenStatistics() {}

  void parent() { ++mParents; }
  void child()  { ++mChildren; }
  void clear() { mParents=0; mChildren=0; }
  void print(UtOStream & out) const;
  
private:
  SInt64 mParents;
  SInt64 mChildren;
};


//! This class qualifies a port connection as being handle-able by flattening.
class FlattenQualifyPort
{
public:
  //! constructor
  FlattenQualifyPort(NUModule *submodule);

  //! destructor
  ~FlattenQualifyPort();

  //! Query whether a port connection can be handled by flattening.
  bool qualify(NUExpr *actual, NUNet *formal) const;
  bool qualify(NULvalue *lformal, NUNet *nformal) const;

private:
  //! The module being flattened
  NUModule *mSubmodule;

  //! Set of nets in the submodule which cannot be scalarized.
  NUNetSet *mNoScalarizeNets;
};


#endif // FLATTEN_H
