// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _CLEANTASKRESOLUTIONS_H_
#define _CLEANTASKRESOLUTIONS_H_

//! class CleanTaskResolutions
/*! Walks the design to see if there are any hier ref task resolutions
 *  which are not valid elaboratedly. These can be created by
 *  flattening because it can't determine that a flattened task is not
 *  longer used. It is easier to determine this after elaboration.
 */
class CleanTaskResolutions
{
public:
  //! constructor
  CleanTaskResolutions() {}

  //! destructor
  ~CleanTaskResolutions() {}

  //! Function to clean up the task resolutions for a design
  void design(NUDesign* design, STSymbolTable* symtab);

private:
  //! Hide copy and assign constructors
  CleanTaskResolutions(const CleanTaskResolutions&);
  CleanTaskResolutions& operator=(const CleanTaskResolutions&);
};

#endif // _CLEANTASKRESOLUTIONS_H_
