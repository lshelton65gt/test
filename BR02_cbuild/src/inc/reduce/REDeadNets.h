// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef REDEADNETS_H_
#define REDEADNETS_H_

#include "reduce/Reduce.h"

class NUDesign;
class STSymbolTable;
class FLNodeElabFactory;
class IODBNucleus;

/*!
  \file 
  Declaration of class used to clear/set the dead flag on nets.
*/

//! REDeadNets
/*!
 * Class which performs design and data-flow walks to determine which
 * unelaborated nets are dead/alive. The NUNet::isDead() interface
 * will be accurate after this analysis.
 */
class REDeadNets
{
public:
  //! Constructor
  REDeadNets(STSymbolTable * symbol_table,
             FLNodeElabFactory * flow_elab_factory,
             IODBNucleus * iodb);

  //! Analyze the design.
  void design(NUDesign * the_design);
private:

  //! Walk the design and mark all nets as dead.
  /*!
   * All nets in the design are marked as dead during this call. We
   * wipe the slate clean before performing a liveness walk.
   */
  void markDead(NUDesign * the_design);

  //! Walk the data-flow graph and mark referenced nets as live.
  /*!
   * All nets encountered during an elaborated data-flow walk are
   * considered live. For each live elaborated net, every unelaborated
   * alias is marked live.
   */
  void markLive(NUDesign * the_design);

  //! Hide copy and assign constructors.
  REDeadNets(const REDeadNets&);
  REDeadNets& operator=(const REDeadNets&);

  //! Symbol table.
  STSymbolTable * mSymbolTable;
  //! Elaborated data-flow factory.
  FLNodeElabFactory * mFlowElabFactory;
  //! IODB
  IODBNucleus * mIODB;
};

#endif // REDEADNETS_H_
