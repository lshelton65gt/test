// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef INFERENCE_H_
#define INFERENCE_H_

#include "nucleus/Nucleus.h"
#include "util/ConstantRange.h"

/*!
  \file
  Declaration of vector inference package.
*/

class ArgProc;
class MsgContext;
class AtomicCache;
class Fold;
class NUNetRefFactory;
class InferenceStatistics;
class InferenceDiscoveryCallback;
class IODBNucleus;
class CommonConcat;
class Collapser;
class LessThanUseDef;

//! Collapse bit-level operations into vectorized equivalents.
class Inference
{
public:

  /*! Inference has a number of options and parameters that control behaviour
      and verbosity. These are packaged into a Params instance and passed into
      the Inference constructor. The options are specified in the mOptions
      member as a bitmask. The values are defined by enum Options.

      An instance my be explicitly configured by passing the options bitmask
      and the parameters to the constructor, or by passing an ArgProc
      reference.
  */

  struct Params {
    //! Explicit initialisation of options
    Params (const UInt32 options, MsgContext *msg_context, const SInt32 threshold = 100 /*percent*/) :
      mMsgContext (msg_context), mOptions (options), mCollapserThreshold (threshold)
    {}

    //! Initialisation of options from the command line.
    Params (const ArgProc *, MsgContext *msg_context, const UInt32 options = 0);

    bool operator [] (const UInt32 options) const { return (mOptions & options) == options; }
    bool any (const UInt32 options) const { return (mOptions & options); }
    void set (const UInt32 options) { mOptions |= options; }

    bool doLeftConcatCollapse () const { return mOptions & eLeftConcatCollapse; }
    bool doRightConcatCollapse () const { return mOptions & eRightConcatCollapse; }
    bool doCommonConcat () const { return mOptions & eCommonConcat; }
    bool doConcatNetElimination () const { return mOptions & eEliminateConcatNets; }
    bool doProcConcatNetElimination () const 
    { return doConcatNetElimination () && (mOptions & eEliminateProcConcatNets); }

    //! Is case inferencing enabled?
    /*!
     Currently, case collapsing is not turned on as it is known to be buggy.
     */
    bool doCaseCollapsing() const { return mOptions & eCollapseCaseStmts_DONOTUSE; }

    MsgContext *mMsgContext;            //!< message context

    UInt32 mOptions;                    //!< bitmask of Options (see below)
    SInt32 mCollapserThreshold;         //!< threshold for accepting a collapse

    // The collapser threshold is expressed as a percentage. A collapse is
    // allowed if the replacement complexity * 100 is less than or equal to the
    // aggregate complexity * mThreshold.

    bool isVerboseCommonConcat () const
    { return (mOptions & eVerboseCommonConcat); }

    bool isVerboseCollapse () const
    { return (mOptions & eVerboseCollapse); }

    bool isVerboseInference () const
    { return (mOptions & eVerboseInference); }

    bool isVerboseAlerts () const
    { return (mOptions & eVerboseAlerts); }

#ifdef CDB
    const char *cvt (char *s, const unsigned int size) const;
    void pr () const;
#endif

  };

  //! Options bits for the mOptions field of struct Params.
  enum {
    eVerboseInference        = 1<<0,    //!< verbose while analysis occurs
    eVerboseCollapse         = 1<<1,    //!< verbose listing of collapse discovery
    eVerboseAlerts           = 1<<2,    //!< dump state along with alerts
    eEnableMetrics           = 1<<3,    //!< use NUCost metrics to test possible collapses
    eVerboseVectorMetrics    = 1<<4,    //!< output the complexity number for collapse candidates
    eVerboseCommonConcat     = 1<<5,    //!< verbose listing of common concat discovery
    eLeftConcatCollapse      = 1<<6,    //!< allow collapse into left-side concats
    eRightConcatCollapse     = 1<<7,    //!< allow collapse into right-side concats
    eCommonConcat            = 1<<8,    //!< do common concat elimination
    eEliminateConcatNets     = 1<<9,    //!< replace local wires with concat temporary bitsels
    eEliminateProcConcatNets = 1<<10,   //!< replace local in behavioural code
    eNoFoldCollapsedVector   = 1<<11,   //!< do not fold the collapsed vector - for debugging
    eModuleWideCommonConcat  = 1<<12,   //!< module-wide common-concat analysis
    eAlwaysWideCommonConcat  = 1<<13,   //!< always-block-wide common-concat analysis
    eCollapseCaseStmts_DONOTUSE = 1<<14,   //!< Optimize case statements.  Known to be buggy, do not use.
    //! mask that forces collapser to build left and right concats
    eConcatCollapse = eLeftConcatCollapse|eRightConcatCollapse,
    //! mask that forces concat construction and common concat analysis
    eConcatAnalysis = eConcatCollapse|eCommonConcat|eEliminateConcatNets|eEliminateProcConcatNets
  };

  //! Constructor
  /*!
    \param fold_ctx A Fold context; used to simplify generated statements.
    \param msg_ctx The message context
    \param params Inference options
    \param stats Statistics object to keep tallys in
  */

  Inference(Fold* fold_ctx,
	    NUNetRefFactory * net_ref_factory,
            MsgContext *,
            AtomicCache *,
            IODBNucleus *,
            ArgProc *,
            Params params,
	    InferenceStatistics *stats);
    
  //! Destructor
  ~Inference();

  // queries

  const Params &getParams () const { return mParams; }
  IODBNucleus *getIODB () const { return mIODB; }
  NUNetRefFactory *getNetRefFactory () const { return mNetRefFactory; }
  MsgContext *getMsgContext () const { return mMsgContext; }

  //! Walk a module and perform vector inference operations.
  /*!
    \param module  The module to transform.
    \return true if anything was modified.
  */
  bool module(NUModule * this_module);

  //! Walk a single always block and perform vector inference operations.
  /*!
    \param always  The block to transform.
    \return true if anything was modified.
  */
  bool always(NUAlwaysBlock * always);

  //! Perform inference on a user-supplied list of statements.
  /*!
    The stmtlist is modified in-place.

    \param scope Scope used when creating case-select temporaries.

    \param stmtlist Statement list for processing.

    \param recurse If true, the statements are both locally and
    recursively processed.

    \return true if any statements were successfully collapsed.

    Note: There is no distinction between collapsing of the provided
    statement list and collapsing of statements below the provided
    statement list (provided that recursion is allowed).
  */
  bool stmts(NUScope * scope, NUStmtList & stmtlist, bool recurse=true);

  //! Specialized for post-block-merging.
  bool mergedBlocks(NUModule * this_module,
		    NUUseDefVector & blocks);

  //! These are used for diagnostics and debugging of statement collapsing

  enum {
    ePosition = 1<<16,                  //!< output linenumbers
    eDumpBlockingDefs = 1<<17,          //!< output the blocking defs of the statement
    eDumpBlockingKills = 1<<18,         //!< output the blocking kills of the statement
    eDumpBlocks = 1<<19,                //!< dump blocks in dump (...NUModule *...)
    eDumpContAssigns = 1<<20,           //!< dump cont assigns in dump (...NUModule *...)
    eDumpContAssignOrder = 1<<21,       //!< dump cont assigns line numbers in dump (...NUModule *...)
    eIndentMask = 0xFF,                 //!< mask for indent amount
    eCountMask = 0x00FF};               //!< mask for the count

  static void dump (UtOStream &f, const NULvalue *stmt);
  static void dump (UtOStream &f, const NUStmt *stmt, const UInt32 options = 0);
  static void dump (UtOStream &f, const NUAssignList &list, const UInt32 options = 0);
  static void dump (UtOStream &f, NUStmtList::const_iterator it, const UInt32 N);
  static void dump (UtOStream &f, NUStmtList::iterator it, const UInt32 N);
  static void dump (UtOStream &f, const NUStmtList &stmts, const UInt32 options = 0);
  static void dump (UtOStream &f, const NUContAssignList &stmts, const UInt32 options = 0);
  static void dump (UtOStream &f, const NUContAssignVector &stmts, const UInt32 options = 0);
  static void dump (UtOStream &f, const NUModule *, const UInt32 options = 0);

protected:
  //! Allow our discovery walker to push/pop scopes.
  friend class InferenceDiscoveryCallback;

  //! Push a scope onto the stack.
  void pushScope(NUScope * scope) { mScopes.push(scope); }

  //! Pop a scope off the stack.
  void popScope() { mScopes.pop(); }

  //! Get the top of the stack.
  NUScope * getScope() const { return mScopes.top(); }

private:

  //! Consider the set of continuous assigns for vectorization.
  /*!
    All cont assigns are considered together as a single vectorization
    opportunity. Currently, there is no reordering performed, though
    that may be considered in the future.

    The set of cont assigns are transformed into a generic stmtlist
    and then operated on as any other stmtlist. If any changes occur,
    they are transformed back into cont assigns and replace the
    original set. 

    Deletion of replaced assignments happens far below.

    Returns true if a single vectorization occured.
  */
  bool collapseContAssigns(NUModule * this_module, CommonConcat *);

  //! Takes a list of stmts and performs vectorization over the set.
  /*!
    The statement list is modified in-place. Any vectorized operations
    take the place of the original bit-level assignments.

    Does not recursively process the list.

    Returns true if a single vectorization occured.
  */
  bool collapseStmts(NUStmtList & stmts, CommonConcat *concat);

  //! Given a net and offset, compute its index.
  /*!
   * Ensures that 'net' is a vector, finds its range and uses the
   * offset to derive index.  When printing, use the declared range
   * of the net to compute the index, otherwise use the operational
   * range.
   */
  UInt32 computeIndexForNet(const NUNet * net, SInt32 offset) const;

  void dumpAssignList(const NULvalue * sliced,
    const NUStmtList::iterator begin, const NUStmtList::iterator end) const;

  void dumpAssignList(const NULvalue * sliced,
                      const NUAssignList & equiv_assigns) const;

  void dumpStmt(const NUStmt * stmt) const;

  //! collapse a qualified case statement into a single assignment statement.
  /*!
    conditions checked are:
    1. the case statement is fully specified with no defaults.
    2. each case item has one single assignment and one single constant 
       condition.
    3. consistency and isomorphic check for assignments in case items.
       -- for each case item
        a. compare the lhs with condition/index, record the differences.
	b. compare the rhs with condition/index, record the differences.
	c. save the 1st case item lhs/rhs differences as the base for comparison. 
	d. compare all other case items with the first one for consistency in the
	   lhs/rhs differences.
    4. call copySelector to create an assignment to assign the selector to a temp, 

    \param case_stmt the case statement to be operated on.
    \param new_stmt the new assignment to replace the case statement.
    \param sel_assign the new assignment to save the selector to a temp.

    \returns true if the case statement is to be replaced by the assignment statement.
  */

  bool collapseCaseToAssign ( NUCase * case_stmt, 
                              NUAssign ** new_stmt,
                              NUAssign ** sel_assign,
                              CommonConcat *concats);
  //! Trace
  /*!
   */
  
  void dumpAssignmentEndpoints(const NULvalue * sliced,
    const NUAssign * first, 
    const NUAssign * last,
    const ConstantRange &first_range,
    const ConstantRange &last_range) const;

  void dumpAssignmentEndpoints(const NULvalue * sliced,
    const NUAssign * first, 
    const NUAssign * last,
    const NUExpr *first_index,
    const NUExpr *last_index) const;

  //! Trace
  /*!
   */
  void dumpEndpoint(const NULvalue * sliced, 
                    const NUAssign * assign, 
                    const ConstantRange &range) const;
  void dumpEndpoint(const NULvalue * sliced, 
                    const NUAssign * assign, 
                    const NUExpr *index) const;
  void dumpEndpoint(const NULvalue * sliced, 
                    const NUAssign * assign, 
                    SInt32 index) const;

  //! Constant expression folding and simplification
  Fold *mFoldContext;

  //! NetRefFactory
  NUNetRefFactory * mNetRefFactory;

  MsgContext *mMsgContext;
  AtomicCache *mStrCache;

  //! IODBNucleus is needed in the collapser
  IODBNucleus *mIODB;

  ArgProc *mArgs;

  InferenceStatistics * mStatistics;

  //! Noise
  bool mVerbose;                        //!< enumerate inference transformations
  bool mVerboseCollapse;                //!< dump extra collapser information
  bool mVerboseVectorMetrics;           //!< dump summary of collapser metric

  //! 
  //! Parameters passed to instantiation
  Params mParams;

  //! after Elaboration? 
  //  The flag is used to disable case inferencing which 
  //  might create temp net too late for elaboration.
  bool mPostElab;

  //! the current scope
  NUScopeStack mScopes;

};


//! Statistics
class InferenceStatistics
{
public:
  InferenceStatistics() { clear(); }
  ~InferenceStatistics() {}

  void module() { ++mModules; }
  void incoming() { ++mIncoming; }
  void outgoing() { ++mOutgoing; }
  void caseIncoming() { ++mCaseIncoming; }
  void caseIncoming(UInt32 count) { mCaseIncoming += count; }
  void caseOutgoing() { ++mCaseOutgoing; }
  void clear() { mModules=0; mIncoming=0; mOutgoing=0; mCaseIncoming=0; mCaseOutgoing=0;}
  void print() const;

private:
  SInt64 mModules;
  SInt64 mIncoming;
  SInt64 mOutgoing;
  SInt64 mCaseIncoming;
  SInt64 mCaseOutgoing;
};

#endif
