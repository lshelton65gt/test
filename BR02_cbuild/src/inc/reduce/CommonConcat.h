// -*-C++-*-    $Revision: 1.10 $
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _COMMON_CONCAT_H
#define _COMMON_CONCAT_H

#include "util/UtMultiMap.h"
#include "util/UtMap.h"
#include "reduce/Inference.h"

// external declarations
class OccurrenceLogger;
class MsgContext;

//! class CommonConcat
/*! CommonConcat identifies left-side concats in assignment statements that
  occur in subsequent expressions. Right-side occurrences of the same concat
  may be replaced with a single temporary net.

  It must be run after the collapser, but before the application of concat rewriting.
*/

class CommonConcat {
public:

  /*!
   * \param scope Enclosing scope of the statement list
   * \param iodb Used only for the NUNet::isProtected actual parameter
   * \param params User options, such as -noConcatNetElimination as well as
   *        context dependent information such as Inference::eInSchedule
   */
  CommonConcat (NUScope *scope, MsgContext *msg_context, const IODBNucleus *iodb, 
                const Inference::Params &params, const CommonConcat *parent);

  virtual ~CommonConcat ();

  //! Module-wide common-concat application
  /*! \note A common-concat object must be used either with this entry point or
   *   with the discover/apply methods but not both.
   */
  bool module (NUModule *module);

  //! Always-block-wide common-concat application
  bool always (NUModule *module);
  bool always (NUAlwaysBlock *block);

  //! Parse the options word from the command line
  static void parseOptions (const ArgProc *, UInt32 *, MsgContext *msg_context);

  //! Look for left-side concats in a list of statements.
  /*! Scans list looking for concats on the left-side of assignment statements
   * \param list The list of statements to scan
   * \return iff any left-side concats were found in list
   */
  bool leftSideDiscover (NUStmtList &list);

  //! Walk a module looking for nets that must not be touched by common concat
  /*! \note Looks for nets that are used in actual parameters and makes an
   *         entry in mProtected. Thus isProtected will return true for these
   *         nets.
   */
  void winnowProtectedNets (NUModule *module);

  //! Test whether any left-side concats were found by discovery
  bool isDegenerate () const { return mLhsOccurrences.size () == 0; }

  //! Search for right-side occurrence of concats
  /*! Searches for concats in a statement list
   * \param list The list of statements scanned for occurrences of concats that
   *        match the left-side concats found by discover.
   * \return The number of right-side occurrences found
   * \pre CommonConcat::discover must have been called
   * \note list need not be the same list as that passed to the discover
   *       method. Indeed, the top-level CommonConcat instance for a module
   *       performs concat discover on the continuous assigns and is then
   *       applied to the statement lists of any behavioral blocks.
   */
  bool rightSideDiscover (NUStmtList &list);

  //! Replace right-side concat occurrences
  /*! \param module Module in which to replace expressions with concat temporary references
   *  \note rightSideDiscover must have been called to build the list of replacements.
   */
  bool rightSideReplace (NUModule *module);

  //! Replace right-side concat occurrences
  /*! \param list List of statements in which to replace expressions with concat temporary references
   *  \note rightSideDiscover must have been called to build the list of replacements.
   */
  bool rightSideReplace (NUStmtList *list);

  //! Replace the left-side of any applied common concats
  /*! Replace the NUConcatLvalue of the eliminated concats with an
   * NUIdentLvalue that denotes the elimation net for that concat.
   * \arg list the list of statements
   * \pre list must contain the NUAssign instances from which
   *      CommonConcat::discover gleaned the left-side concats for this
   *      CommonConcat instance.
   * \note This method should be called for behavioral
   *       code. The NUModule variant of replaceConcatLvalues must be used for
   *       the continuous assignments of a module.
   */
  UInt32 leftSideReplace (NUStmtList &list);

  //! Replace the left-side of any applied common concats from the continuous assigns of a module
  /*! Replace the NUConcatLvalue of the eliminated concats in the continuous
   *  assigns of module with an NUIdentLvalue that denotes the elimation net
   *  for that concat.
   * \arg module the module containing the continuous assigns to modify
   * \pre The module must contain the continuous assigns from which discover
   *      gleaned the left-side concats for this CommonConcat instance.
   * \note This method only looks at the lvalues of the continuous
   *       assignments of module. Of course, if discovery was performed over
   *       the continuous assigns of module, then all the left-side concats are
   *       in the continuous assigns anyway.
   */
  UInt32 leftSideReplace (NUModule *module);

  //! Eliminate any local nets in a module that were part of an applied common concat.
  /*! For local nets that are part of an identified common concat replace
   *  all occurrences of net with a reference to the appropriate bit(s) of the
   *  common-concat net
   */
  bool eliminateLocalNets (NUModule *);

  //! Eliminate any local nets in a statement list that were part of an applied common concat.
  bool eliminateLocalNets (NUStmtList &);
  
  /*! \enum Counters
   *  Identifiers for the assorted counts maintained during concat analysis.
   *  \note The identifiers are constructed by a CPP macro and read from CommonConcat.def
   */
  enum Counters {
#define CONCAT_COUNTER(_tag_, _label_, _comment_) c##_tag_,
#include "reduce/CommonConcat.def"
    cNCounters                          //!< number of counters
  };

  //! Increment a common-concat counter
  void count (const CommonConcat::Counters tag, const UInt32 delta = 1)
  { mCount.count (tag, delta); }

  //! Return a common-concat counter value
  UInt32 getCount (const Counters x) const { return mCount [x]; }

  //! \enum Qualifiers
  enum Qualifiers {
    cALLOW_PORT = 1<<0,
    cNO_QUALIFIERS = 0
  };

  //! Can a net be safely eliminated for common-concat replacement.
  /*! \arg net the net in question
   *  \return iff the net is a candidate for elimination by local net elimination.
   *  \note This method tests whether a net is a candidate for elimination by
   *        common concat. It is also called by the collapser when considering
   *        a tristate left-side net for collapsing. Tristate nets can be
   *        common-concat transformed only if all instances of the net can be
   *        replaced by a common-concat reference. This method determines
   *        whether that is possible.
   */
  bool canBeEliminated (const NUNet *net, const UInt32 qualifiers = cNO_QUALIFIERS) const; 
  bool canBeEliminated (const NUExpr *expr, const UInt32 qualifiers = cNO_QUALIFIERS) const; 

  //! Can an lvalue be safely eliminated for common-concat replacement.
  /*! \arg lvalue the lvalue under consideration.
   *  \note This method pulls apart NUIdentLvalue and NUConcatLvalue instances
   *        and call canBeEliminated on the encapsulated nets. If returns true
   *        if all these encapsulated nets can be eliminated.
   */
  bool canBeEliminated (const NULvalue *lvalue, const UInt32 qualifiers = cNO_QUALIFIERS) const; 

  //! Can a net be safely eliminated for replacement by a common-concat temporary reference
  bool canBeReplaced (const NUNet *, const UInt32 qualifiers = cNO_QUALIFIERS) const; 
  bool canBeReplaced (const NUExpr *, const UInt32 qualifiers = cNO_QUALIFIERS) const;

  //! mark a net as protected against replacement or elimination.
  void protect (const NUNet *);
  
  void protect (const NUExpr *);

  //! mark a net as protected against replacement or elimination.
  void protectActual (const NUNet *, const NUUseDefNode *);
  void protectActual (const NUNet *, const NUCModelArgConnection *);

  //! mark an lvalue as protected against common concat elimination
  void protect (const NULvalue *);

  //! mark the lvalues in a concat as protected
  void protect (const NUConcatLvalue *);

  //! test whether is instance is local to a scope
  bool isLocal (const NUScope *scope) const { return mScope == scope; }

  // CommonConcat can write a log file for tracking applications of common
  // concat replacement and net elimination.

  //! Open the concat log file building the filename from root
  static void openOccurrenceLog (MsgContext &, const char *root);

  //! Close the concat log file
  static void closeOccurrenceLog ();

  //! print a human readable representation of the common concat instance to stdout
  /*! \arg deep print extra information.
   *  \note This method is intended for use from the debugger.
   */
  void pr (bool deep = false) const;

  //! print a human readable representation of the common concat instance to a stream
  /*! \arg s the output stream.
   *  \arg deep print extra information.
   *  \note This method is intended for use from the debugger
   */
  void pr (UtOStream &s, bool deep = false) const;

  //! output the verbose statistics for a module
  void statistics (UtOStream &s, const NUModule *module) const;

  //! dump the statistics with a comment
  void statistics (UtOStream &s, const char *fmt, ...) const;

private:
  
  /*! \class Count
   *  Encapsulate a set of counters that gather statistics about the concat
   *  analysis.
   */

  class Count {
  public:

    Count ();

    //! Increment a count
    void count (const Counters key, const UInt32 delta = 1) 
    { mCount [key] += delta; mNonZero |= delta > 0; }

    //! \return a counter value
    UInt32 operator [] (const UInt32 key) const { return mCount [key]; }

    //! Merge a counter with another counter.
    void merge (const Count &);

    //! \return iff there is a non-zerocounter
    bool isNonZero () const { return mNonZero; }

    //! Diagnostic dump
    void pr () const;

    //! Options for statistics output
    enum Options {
      cBRIEF = 1<<8,                    //!< just the brief key
      cINDENT_MASK = 0xFF,              //!< mask the indent amount bits
      cALL = 0xFFFFFFFF
    };
    
    //! Output statistics from the counter
    void statistics (UtOStream &, const UInt32 options = cBRIEF) const;

  private:

    bool mNonZero;                      //!< set when a counter goes non-zero
    UInt32 mCount [cNCounters];         //!< the counter values

    //! Map a counter tag to a short identifier
    static const char *getLabel (const UInt32);

    //! Map a counter tag to a longer description
    static const char *getDescription (const UInt32);

  };

  //! Encapsulate a left-side concat found by CommonConcat::discover.
  /*! Instances of CommonConcat::Concat represent an NUConcatLvalue instance
   *  that is a candidate for common concat replacement. Discovery constructs a
   *  list of Concat instances in CommonConcat::mDescriptors.
   */
  friend class CommonConcatRightReplacement;

  class Concat {
  public:

    /*!
     * \param rank The rank of the concat is used in a pessimistic test for
     *             killing of the concat assignment. Left-side concats from
     *             continuous assigns are given rank equal to zero. For blocking
     *             or non-blocking assigns the rank in the ordinal in the
     *             statement list containing the assign. A concat is live at a
     *             given statement in the statement list of the candidate
     *             position is greater than the rank and less than the killing
     *             rank of the Concat, kept in mKilled.
     * \param lvalue The NUConcatLvalue instance.
     * \param assign The assignment that induces the Concat.
3     * \param nets The list of nets in the concat. While the constructor could
     *             just pluck these out of the NUConcatLvalue instance,
     *             CommonConcat::addConcat is the only method that creates
     *             Concat instances and it needs to walk through the nets
     *             anyway, passing them into the ctor saves traversing the
     *             NUConcatLvalue again.
     */
    
    enum Flavour {
#define CONCAT_FLAVOUR(_tag_, _descr_) c##_tag_,
#include "reduce/CommonConcat.def"
      cNFlavours
    };

    Concat (CommonConcat &concat, const Flavour flavour,
            const UInt32 rank, NUConcatLvalue *lvalue, NUAssign *assign, 
            NUNetList &nets);

    ~Concat ();

    static const char *cvt (const Flavour);

    // The first pass constructs a mapping from expressions to a concat
    // descriptor that may replace that expression. For concats that are
    // selected for replacement on the second pass, this mapping sees a
    // replaceLeaves transformation.

    //! \class Replacement
    /*! Denotes an occurrence of a width-bit wide prefix of a concat in an
     *  expression.
     */

    struct Replacement {
      /*! \param expr The expression that is replaced by a partsel of the
       *              common concat
       *  \param concat Descriptor of the concat
       *  \param width Number of bits of the concat used in expr.
       *  \note Currently on prefix matches are implemented so the 
       *  partition that replaces expr is from 0 to width-1.
       */

      Replacement (NUExpr *expr, Concat *concat, const UInt32 width) : 
      mExpr (expr), mConcat (concat), mWidth (width) {}

      NUExpr *mExpr;                    //!< express that may be replaced
      Concat *mConcat;                  //!< the concat that replaces expr
      const UInt32 mWidth;              //!< concat prefix length
    };

    //! \class ReplacementMap
    /*! A mapping from expressions to the concat that may replace it. */

    class ReplacementMap : public UtMap <NUExpr *, Replacement> {
    public:
      ReplacementMap () : UtMap <NUExpr *, Replacement> () {}
      virtual ~ReplacementMap () {}

      //! Note that expr may be replaced by a concat
      void insert (NUExpr *expr, Concat *descr, const UInt32 width)
      {
        UtMap <NUExpr *, Replacement>::insert (
          std::pair <NUExpr *, Replacement> (expr, Replacement (expr, descr, width)));
      }

    };

    //! Note the occurrence of this concat in a right-side concat.
    void foundRightSideOccurrence (NUExpr *expr, const UInt32 width);

    //! \return the number of right-side occurrences of this concat.
    UInt32 getNumRightSides () const { return mNRightSides; }

    void foundReplacement () { mNNetReplacements++; }

    //! \return iff this concat is an orphan concat.
    /*! \note An orphan concat is a concat that is defined by a left side but
     *  has only a single right-side occurrence and induces no replacements.
     */
    bool isOrphan () const { return mNRightSides < 2 && mNNetReplacements == 0; }

    //! Wrapper around UtList <Concat *>.
    /*! This just adds a pr method to UtList <Concat *> that is useful for the
     *  CommonConcat diagnostic methods.
     */

    class List : public UtList <Concat *> {
    public:

      List () : UtList <Concat *> () {}

      //! Write a human readable representation of this to s.
      void pr (UtOStream &s, bool deep = false) const;

    };

    //! \return the number of items in the concat.
    UInt32 getLength () const     { return mNets.size (); }

    //! \return the type of the inducing statement.
    /*! This will be one of the assignment flavours */
    NUType getType () const       { return mType; }

    //! \return the strength of the inducing assignment.
    Strength getStrength () const { return mStrength; }

    //! \return the inducing assignment.
    NUAssign *getAssign () const        { return mAssign; }

    //! \return the lvalue that induced the concat.
    NUConcatLvalue *getLvalue () const  { return mLvalue; }
    
    //! \return iff the concat was applied to a right-side concat,
    bool isEliminated () const          { return mEliminator != NULL; }

    //! \return the net of the first item in the left-side concat.
    NUNet *getLeader () const           { return *mNets.begin (); }

    //! Set the killing point of the concat.
    /*! The left-side concat must is killed whenever the inducing assignment is
     *  killed. The initial implementation just pessimistically notes the index
     *  of any statement in the list that may kill the assign.
     * \note A finer grained implementation could track the killing points of
     *       the individual nets in the common concat left-side. This
     *       implementation is quite simplistic...
     */
    void killAt (const UInt32 position) 
    { 
      if (position < mKilled) {
        mKilled = position; 
      }
    }

    //! \return iff the concat is guarenteed to be live at the given statement index.
    bool isLive (const UInt32 position) const
    { return mRank <= position && position < mKilled; }

    //! Build the defining assignment for the common concat.
    /*! The defining assign is the assignment of the common concat net back to
     *  the original left-side of the inducing assignment. If any local nets
     *  were eliminated then a list of assignments is constructed. Only
     *  assignments to non-eliminated parts of the original left-side concat
     *  are synthesised.
     */
    bool buildDefiningAssign (NUStmtList *);

    //! Match and expression against a concat instance.
    /*! Encapsulate the position in a candidate Concat instance for matching
     *  against an item in a right-side concat
     */
    struct Matcher {
      Matcher (Concat *descr) : mIt (descr->begin ()), mMatched (true) {}

      /*! \param expr expression to match against current position in the
       *         candidate
       *  \param cumulative_width if expr matches then cumulative_width is
       *         incremented by the width of the matched net.
       *  \return iff the expression matches a prefix of the descriptor
       *  \note The matcher object is used by initialising with a potentially
       *        matching Concat instance and then calling this operator, in
       *        order, for each component expression of the right-side
       *        concat. \sa CommonConcat::Concat::Occurrences::match
       */
      bool operator () (const NUExpr *expr, UInt32 *cumulative_width);

    private:

      // When the Matcher instance is intialised, mMatched is true and mIt is
      // initialised to the start of the list of nets that makes up the
      // concat. Each time the matching operator () is called, if the the
      // expression matches the iterator is advance, else mMatched is reset.

      NUNetList::const_iterator mIt;    //!< current position in the match
      bool mMatched;                    //!< reset when a match fails
      
    };                                  // struct Matcher

    //! Grab an iterator over the nets in the concat.
    NUNetList::const_iterator begin () { return mNets.begin (); }
    NUNetList::const_iterator end () { return mNets.end (); }
    

    //! Return the common-concat temporary net used to replace this concat
    /*! \param scope Scope in which to create temporary nets.
     *  \note The first time getReplacementNet is called for a concat
     *        descriptor the temporary net is created with
     *        NUScope::createTempNet. Subsequent calls for a given concat
     *        return this same net.
     */
    NUNet *getReplacementNet (NUScope *scope = NULL);


    //! Return the "replaced" counter
    /*! \sa CommonConcat::Concat::count */
    UInt32 getCount (const CommonConcat::Counters x) const { return mCount [x]; }

    //! Concat::Map adds a pr method to UtMultiMap <NUNet *, Concat *>
    class Map : public UtMultiMap <NUNet *, Concat *, NUNetCmp> {
    public:
      Map () : UtMultiMap <NUNet *, Concat *, NUNetCmp> () {}
      void pr (UtOStream &, bool deep = false) const;
    };


    class Eliminator;                   // forward declaration

    //! Searchable map of all Concat instances.
    /*! When a left-side concat is located it is added to
     *  Concat::mLhsOccurrences. When a common-concat instance is applied to a
     *  module or statement list, NURvalueConcat instances are matched against
     *  mLhsOccurrences.
     */
    class Occurrences : public Concat::Map {
    public:

      Occurrences (CommonConcat &x);
      ~Occurrences () {}

      //! Add a concat to the map.
      void add (Concat *);

      //! Search for a concat match.
      /*! \param concat The right-side concat to match.
       *  \param position The position in the statement list.
       *  \note position is used for the pessimistic concat liveness test.
       *  \param descr If a match is found then descr returns the appropriate
       *         Concat instance.
       *  \param width If a match is found then width returns the length of the
       *         matched prefix.
       *  \param flavour NUType of the assign that contains concat on its rhs.
       *  \param strength Strength of a continuous assign.
       *  \note A future enhancement is to make a more sophisticated matcher
       *        that will match a concat againsts arbitrary substrings of a
       *        left-side concat rather than just a prefix.
       */
      bool match (const NUConcatOp *concat, const UInt32 position,
                  Concat **descr, UInt32 *width, const NUType flavour, 
                  const Strength strength = eStrWeakest) const;

      bool match (const NUConcatLvalue *concat, const UInt32 position,
                  Concat **descr, UInt32 *width, const NUType flavour,
                  const Strength strength = eStrWeakest) const;

      //! Add an entry for each applied concat to a local net eliminator.
      /*! If a local net is used in an applied concat then it can be totally
       *  replaced by the common-concat temporary. This method calls the
       *  Concat::build (Eliminator *) method for each concat that was applied
       *  to a right-side.
       */
      void build (Eliminator *);

      //! Dump a human readable representation.
      void pr (UtOStream &, bool deep = false) const;

    private:

      CommonConcat &mCommonConcat;      //!< context
      UInt32 mMaxDegree;                //!< maximum number of concat with the same first net
    };                                  // class Occurrences

    //! Local net elimination functor.
    /*! This replaceLeaves functor is used to replace references to local nets
     *  with references to partitions of a common concat temporary.
    */

    class Eliminator : public NuToNuFn {
    public:

      //! \param concat the enclosing CommonConcat instance
      Eliminator (CommonConcat &concat) : 
        mCommonConcat (concat), mMap (), mLine (0), mNReplaced (0) {}

      ~Eliminator ();

      //! Define an eliminable net.
      /*! \param concat The concat descriptor for the common-concat temporary
       *  \param net The local net that can be replaced.
       *  \param replacement The replacement net.
       *  \param range The partition of the replacement net that replaces net.
       */
      void define (Concat *concat, const NUNet *net, NUNet *replacement, ConstantRange range);

      //! Match NUIdentRvalue or NUVarselRvalues against the eliminable nets.
      virtual NUExpr *operator () (NUExpr *, Phase);

      //! Match NUIdentLvalue or NUVarselLvalue against the eliminablenets.
      virtual NULvalue *operator () (NULvalue *, Phase);

      //! Return the number of replacable nets.
      UInt32 getSize () const { return mMap.size (); }

      //! return the number of local nets eliminated
      UInt32 getCount () const { return mNReplaced; }

      //! Set the position of the current statement for liveness testing.
      void setLine (const UInt32 n) { mLine = n; }

      //! Diagnostics.
      void pr (UtOStream &, bool deep) const;

    private:

      CommonConcat &mCommonConcat;      //! enclosing context

      //! Replacement entry for a replacable net.
      struct Replacement {
        /*! \param concat The concat descriptor for the replacement net.
         *  \param net The replacement net.
         *  \param range The partition of the replacement net that corresponds
         *         the eliminated net.
         *  \note The concat instance is used for liveness testing.
         */
        Replacement (Concat *concat, NUNet *net, ConstantRange range) : 
          mConcat (concat), mNet (net), mRange (range), mCount (0) {}

        Concat *mConcat;                //!< the descriptor for the replacement net
        NUNet *mNet;                    //!< the replacement net.
        ConstantRange mRange;           //!< the partition in the replacement net.
        UInt32 mCount;                  //!< number of applications
      };

      typedef UtMap <const NUNet *, struct Replacement, NUNetCmp> Map;

      Map mMap;                         //!< map from nets to the replacement information
      UInt32 mLine;                     //!< line number in the statement list
      UInt32 mNReplaced;                //!< number replaced

    };                                  // class Eliminator

    //! Initialise an eliminator functor with this concat.
    void build (Eliminator *);

    //! Design walk for killing left-side concat assignment.
    /*! Replacement of a right-side concat is invalid if the assignment that
     *  induced the left-side concat descriptor is not live. This walker looks
     *  for any assignments to a net in a statement and kills all the left-side
     *  concats containing that net. This is a pessimistic killing that is
     *  valid within a block. This walker must be called for each statement in
     *  the list that was passed to CommonConcat::discovery and is subsequently
     *  manipulated with apply.
    */

    class AssignKiller : public NUDesignCallback {
    public:

      /*! \param killing the map from nets to the left-side descriptor
       *  \param position the position of the statement in the list
       */

      AssignKiller (Map &killing, const UInt32 position) : 
        mKilling (killing), mPosition (position) {}

    private:

      Map &mKilling;                    //!< handle on the CommonConcat::mKilling
      const UInt32 mPosition;           //!< position of the containing statement in the list

      //! Base operator does nothing
      virtual Status operator () (Phase, NUBase *) { return eNormal; }

      //! Called for each assignment to lvalue at statement mPostion
      /*! \note mPosition must be set to the index of the statement containing
       *  lvalue before the walk of 
       */
      virtual Status operator () (Phase, NUIdentLvalue *lvalue);
    };                                  // class ConcatAssignKiller

    //! diagnostics
    void pr (UtOStream &, bool deep = false) const;
    void pr (bool deep = false) const;

    //! counting
    void count (const Counters tag, const UInt32 delta = 1) { mCount.count (tag, delta); }

  private:                              // CommonConcat::Concat

    CommonConcat &mCommonConcat;
    Flavour mFlavour;                   //!< the type of this concat
    const UInt32 mRank;                 //!< 0 for ContAssign, abstract posn for proc assign
    UInt32 mKilled;                     //!< position that common concat is killed
    const NUType mType;                 //!< the flavour of the assign
    const Strength mStrength;           //!< strength of cont assign
    NUConcatLvalue *mLvalue;            //!< the lhs concat expression
    NUAssign *mAssign;                  //!< the enclosing assignment statement
    NUNetList mNets;                    //!< vector of nets in the concat
    NUNet *mEliminator;                 //!< the elimination net
    UtSet <NUNet *> mRemoved;           //!< totally removed local nets
    CommonConcat::Count mCount;         //!< statistics
    UInt32 mNRightSides;
    UInt32 mNNetReplacements;
  };                                    // class Concat

  //! Right-hand side concat elimination function
  /*! This replaceLeaves functor matches NUConcatRvalue expressions against the
   *  left-side concats. Those that match all of, or a prefix of, a live
   *  left-side concat are replaced with the appropriate common-concat temporary.
   */

  class RightSide : public NuToNuFn {
  public:

    RightSide (CommonConcat &concat, const Concat::Occurrences &map, const bool verbose) :
      NuToNuFn (),  mCommonConcat (concat), mVerbose (verbose), mMap (map), 
      mLowCount (mCommonConcat.getCount (cREPLACE_RVALUE)),
      mFoundReplacement (false)
    {}

    virtual ~RightSide () {}

    //! Traverse a statement list calling replaceLeaves for each statement.
    bool operator () (NUStmtList *);

    //! Replace NUConcatRvalue instances that match a left-side concat.
    virtual NUExpr *operator () (NUExpr *, Phase);

  private:

    CommonConcat &mCommonConcat;        //!< enclosing common concat
    const bool mVerbose;                //!< be noisy or quiet
    const Concat::Occurrences &mMap;    //!< the map of 
    UInt32 mPosition;
    NUStmt *mStmt;
    NUType mType;
    Strength mStrength;
    const UInt32 mLowCount;
    bool mFoundReplacement;
  };                                    // class RightSide

  const bool mVerbose;                  //!< -verboseCommonConcat
  const bool mDoLocalNetElimination;    //!< set when elimination net replaces local scalars
  UtSet <const NUBase *> mProtected;    //!< items that cannot be touched
  const CommonConcat *mParent;          //!< parent for inheriting isProtected

  NUScope *mScope;                      //!< scope of this instance
  MsgContext *mMsgContext;              //!< message context
  const IODBNucleus *mIODB;             //!< IODB for isProtected
  const Inference::Params &mParams;     //!< parameters passed down from inference
  Concat::List mDescriptors;            //!< list of left-side concats in statement order
  Concat::Occurrences mLhsOccurrences;  //!< map of left-side concats
  Concat::Map mKilling;                 //!<
  Concat::ReplacementMap mReplacements; //!< map of nets that may be replaced by a concat temp bitsel
  bool mAppliedToStatements;            //!< set when applied to mStatements

  mutable Count mCount;                 //!< statistics gathering

  friend class CommonConcatLogger;

  static OccurrenceLogger *sLog;        //!< common concat occurrence logger

  //! track number of nets created
  /*! Each common concat net is given a name of CommonConcat_N where N is the
   *  value of sNNets when the net was created. N is unique across the common
   *  concat nets. Breakpoints in common concat code can be made conditional
   *  for a specific net by testing the value of sNNets.
   */
  static UInt32 sNNets;

  //! identifiers in the CommonConcat log file
  /*! These are used as the tag argument to the CommonConcat::log call
   */

  enum OccurrenceKey {
#define CONCAT_LOG(_tag_, _descr_) c##_tag_,
#include "reduce/CommonConcat.def"
    cNKeys
  };

  //! Is a net of lvalue protected from common-concat manipulation
  /*! \return iff the item is protected
   *  \note item is, probably a net or an lvalue
   *  \sa CommonConcat::protect
   */

  bool isProtected (const NUBase *item) const 
  { 
    return mProtected.find (item) != mProtected.end ()
      || (mParent != NULL && mParent->isProtected (item));
  }

  //! Pessimistically infer 
  void killConcats (NUStmt *stmt, const UInt32 position);

  friend class RightSide;

  //! Access the replacement net for a concat.
  NUNet *getReplacementNet (Concat *descriptor) { return descriptor->getReplacementNet (mScope); }

  //! Add a left side concat to the concat occurrence map.
  bool addConcat (const Concat::Flavour, const UInt32 rank, NUConcatLvalue *, NUAssign *);

  //! Add a right side concat to the concat occurrence map
  bool addConcat (const Concat::Flavour, const UInt32 rank, NUConcatOp *);

  void foundRightSideOccurrence (NUExpr *expr, Concat *descr, const UInt32 width);

  /*! Diagnostics
   */
  void dumpConcatList (UtOStream &, bool deep = false) const;
  //!< outputs the list of NUConcatLvalue instances found during lhs discovery

};

#endif
