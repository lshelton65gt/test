// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Package of miscellaneous transformations (mostly called from the scheduler).
*/

#ifndef _RETRANSFORM_H_
#define _RETRANSFORM_H_

#include "flow/FLFactory.h"

class STSymbolTable;
class MsgContext;
class SourceLocatorFactory;
class IODBNucleus;
class AtomicCache;
class ConstantRange;
class FLNodeFactory;
class FLNodeElabFactory;
class UD;
class ArgProc;
class ClockAndDriver;
class CopyContext;
class Stats;
class FLElabUseCache;

typedef UtMap<NUUseDefNode*,NUUseDefNode*> NUUseDefOldToNew;

//! Reduce Transformations class
/*! This class contains routines to do various transformations to the
 *  nucleus data structures. These transformations are simple
 *  operations on the unelaborated and elaborated data structures.
 */
class RETransform
{
public:
  //! constructor
  RETransform(STSymbolTable*,
	      AtomicCache*,
	      SourceLocatorFactory *loc_factory,
	      FLNodeFactory*,
	      FLNodeElabFactory*,
	      NUNetRefFactory *netref_factory,
	      MsgContext*,
	      IODBNucleus*,
	      ArgProc * args);

  //! destructor
  ~RETransform();

  //! Abstraction for sorted FLNodeElab's
  struct CompareFLNodeElabs
  {
    bool operator()(const FLNodeElab* f1, const FLNodeElab* f2) const;
  };

  //! Abstraction for keeping track of new elaborated flows created
  typedef UtSet<FLNodeElab*, CompareFLNodeElabs> NewFlows;

  //! Adds an intervening buffer between a block and a set of input nets
  /*! This routine gets all the elaborated flow nodes for all
   *  instances of a block of code (always block or continous assign)
   *  and a set of nets that need to be delayed (buffered). It
   *  attempts to create a new always block that buffers those nets
   *  before using them in this block.
   *
   *  \param outFlows The set of elaborated flow nodes for all
   *  instances of a given use def node.
   *
   *  \param inNets The set of nets that must be delayed or buffered.
   *
   *  \param replaceEdges If set, we replace inputs in the edge
   *  expression with the buffered nets.
   */
  void addBuffer( FLNodeElabSet& outFlows, NUNetRefSet& inNetRefs,
                  bool replaceEdges );

  //! Fixes up the UD and Flow information after adding buffers
  /*! The addBuffer routine stores all the new blocks created as well
   *  as the output flows. This routine fixes up the UD information
   *  and the flow. It is done as a followup cleanup pass because UD
   *  needs to be run on each module.
   *
   *  \param design A design pointer.
   *
   *  \param newFlows A place to put all the elaborated flow nodes
   *  that were created by this routine.
   *
   *  \param clockAndDriver Information about clocks in the design
   */
  void fixupBufferFlow(NUDesign* design, NewFlows* newFlows,
                       FLNodeElabSet* deletedElabFlows,
                       ClockAndDriver* clockAndDriver,
                       Stats* stats);

  //! Creates a new usedef from two sets of flow node's use defs
  /*! This function is used to merge code together to reduce call
   *  overhead and increase the ability to make nets into local
   *  variables.
   *
   *  The function receives two vectors of flow nodes. Each vector is
   *  the set of flow nodes for a given use def node. The order of the
   *  two usedef nodes is first f1, then f2.
   *  
   *  The first step is two merge the use def nodes for the two flow
   *  nodes. It creates a new usedef node that executes the old use
   *  def nodes in the order this flow/usedef then f2 flow/usedef. We
   *  fix up this flow node and the f2 flow node to now point to the
   *  new usedef node.
   */
  void mergeFlows(FLNodeElabVectors&);

  //! Function to clean up after merging flows
  void cleanUpMergedFlows();

  //! Function to try and optimize the resulting blocks created by merging.
  void optimizeMergedFlows(NUDesign *design, bool verbose);

  //! cleanup internal flow.
  void repairMergedFlow(NUDesign * design, ClockAndDriver * clock_and_driver, FLNodeElabSet*, Stats* stats);

  //! provide access to the flow-elab-factory for iteration skipping cycles
  FLNodeElabFactory* getFlowElabFactory() {return mFlowElabFactory;}

  //! Create a buffer for a reset net.
  NUNetElab* addResetBuffer( NUNetElab *netElab, FLNodeElab** returnElabFlow);

  //! Record a clock for an upcoming latch conversion
  /*! The latch conversion needs to account for all the clocks that
   *  will be created for a given latch. This is because it has to
   *  make flops that are a function of the clocks and not any local
   *  expressions that are potentially made up of block local temps
   *  (block local temps get duplicated in the copy process).
   *
   *  So to create flops from a latch, first all this routine on all
   *  conversions and then call convertLatchToFlop on each one again.
   */
  void recordLatchClock(NUAlwaysBlock* latch, NUExpr* clk, NUExpr* origClk,
                        bool inverted);

  //! Create a flop from a latch (combinational block)
  NUAlwaysBlock* convertLatchToFlop(NUAlwaysBlock* latch, ClockEdge edge,
                                    NUExpr* clk, NUAlwaysBlock* priority);

  //! Fixup the nucleus and flow after converting latches to flops
  /*! This pass does a number of operations on the design as a result
   *  of creating the new always blocks. These include:
   *
   *  - Fixup the clock block pointers for the new always blocks.
   *
   *  - Replace the clock expressions for all the flops with constants.
   *
   *  - Delete the old always blocks and clean up the module always
   *    blocks list.
   *
   *  - Repair the flow (both unelaborated and elaborated).
   */
  void fixupNewFlops(NUDesign* design, Stats* stats);

  void repairBlockFlows(NUDesign* design,
                        NUAlwaysBlockSet* blockSet,
                        FLNodeElabSet* deletedElabFlow,
                        ClockAndDriver* clockAndDriver,
                        Stats* stats);

private:
  //! REUpdateTempNetCallback class needs to be a friend to access createAssigns
  friend class REUpdateTempNetCallback;

  void discoverBoundary(FLNodeSet & boundary_flow);

  // add a buffer between block and a bunch of source nets
  void addBufferBlock(NUUseDefNode*, NUNetRefSet&, NUAlwaysBlockVector*,
                      bool replaceEdges);
  void replaceInputNet(NUModule* module, NUAlwaysBlock* always,
                       const NUNetRefHdl& netRef, NUNet* newNet,
                       bool replaceEdges);
  void createDelays(NUNet* newNet, const NUNetRefHdl& netRef,
                    const SourceLocator& loc, NUStmtList* stmtList);
  NUNet* createTempNet(NUScope* scope, NUNet* net, const char* prefix);
  void replaceNetUses(NUAlwaysBlock* always, NUNet* net, NUNet* newNet,
                      bool replaceEdges);

  // Types and data to keep track of created temp nets (for elaboration)
  typedef UtMap<NUScope*, NUNetVector*> TempNets;
  typedef LoopMap<TempNets> TempNetsLoop;
  TempNets* mTempNets;
  void addTempNet(NUScope* scope, NUNet* tempNet);
  TempNetsLoop loopTempNets();
  void clearTempNets();

  // Type for a vector of use def nodes to merge
  typedef UtVector<NUUseDefNode*> NUUseDefVector;
  typedef NUUseDefVector::iterator NUUseDefVectorIter;

  // The set of created use defs
  NUUseDefSet mCreatedUseDefs;

  // The set of deleted use defs
  NUUseDefOldToNew mDeletedUseDefs;

  // Types and data for dealing with updating UD and flow when we add
  // a buffer block between two use defs.
  typedef UtMap<NUAlwaysBlock*, FLNodeElabSet*> BufferAndFlow;
  typedef BufferAndFlow::value_type BufferAndFlowValue;
  typedef LoopMap<BufferAndFlow> BufferAndFlowLoop;
  BufferAndFlow* mBufferAndFlow;

  // Remember the always blocks that had inputs buffered as well as
  // what inputs were buffered. That way we can verify that the buffer
  // insertion worked correctly.
  typedef UtMap<NUAlwaysBlock*, NUNetRefSet*> BufferedInputs;
  typedef BufferedInputs::value_type BufferedInputsValue;
  typedef LoopMap<BufferedInputs> BufferedInputsLoop;
  BufferedInputs* mBufferedInputs;

  // The set of always blocks that need their flow repaired after
  // buffer insertion or latch to flop conversion. Only the ones where
  // the delayed net is also locally defined need this done.
  NUAlwaysBlockSet* mRepairBlocks;
  void repairFlow(NUDesign* design, FLNodeElabSet* deletedElabFlows,
                  ClockAndDriver* clockAndDriver, Stats* stats);

  // Types to find all modules and instances for the objects we are
  // transforming.
  typedef UtSet<STBranchNode*> InstanceSet;
  typedef InstanceSet::iterator InstanceSetIter;
  typedef UtMap<NUModule*, InstanceSet> ModuleHierarchies;
  typedef ModuleHierarchies::iterator ModuleHierarchiesIter;
  typedef ModuleHierarchies::value_type ModuleHierarchiesValue;

  // Types and data to cache all the buffered nets we have created
  typedef std::pair<NUAlwaysBlock*, NUNet*> BlockNetPair;
  typedef UtMap<NUNetRefHdl, BlockNetPair> NetDelayBlocks;
  typedef UtMap<NUModule*, NetDelayBlocks*> ModNetDelayBlocks;
  typedef ModNetDelayBlocks::value_type ModNetDelayBlocksValue;
  ModNetDelayBlocks* mModNetDelayBlocks;
  NetDelayBlocks* getNetDelayBlocks(NUModule* module);
  void addNetDelayBlock(NUModule*, const NUNetRefHdl&, NUAlwaysBlock*, NUNet*);
  BlockNetPair findNetDelayBlock(NUModule*, const NUNetRefHdl&);
  typedef LoopMap<ModNetDelayBlocks> ModNetDelayBlocksLoop;
  ModNetDelayBlocksLoop loopModNetDelayBlocks();

  // Create a new use def node from two use def nodes
  NUUseDefNode* mergeUseDefNodes(NUUseDefVector& useDefVector);

  // Update the clock/priority block pointers for all always blocks in module
  void updateClockPriorityPointers(NUModule* module);

  // Find the replacement always block for a merged block
  NUAlwaysBlock* findNewBlock(NUAlwaysBlock* oldAlways);

  void moveStmts(NUBlock* block, NUUseDefNode* ud);

  // Create unelaborated and elaborated data for the new buffer
  void createElaboratedData(FLNodeElabSet*, NUAlwaysBlock*, NUNet*, FLNode*,
			    NewFlows*);
  FLNode* createUnelaboratedData(FLNodeElabSet*, NUAlwaysBlock*, NUStmt*,
				 const NUNetRefHdl&);
				 
  //! Elaborate each temp net in every elaboration of a set of modules
  void elaborateTempNets(ModuleHierarchies& moduleHierarchies,
                         NUNetElabSet* newElabNets);

  //! Elaborate each net in every elaboration of module
  void elaborateNets(NUScope* scope, NUNetVector* newNets,
		     ModuleHierarchies& moduleHierarchies,
                     NUNetElabSet* newElabNets);

  void updateFlowMaps(FLNodeElabVectors & nodeVectors);
  FLNodeConstElabMap mMergedElabToFlow;
  FLNodeSet mMergedFlow;

  //! Map from module to vector of NUUseDefNodes
  /*!
   * Group NUUseDefNodes from the same module together. Used so
   * optimizations can operate per-module.
   */
  typedef UtMap<NUModule*, NUUseDefVector> ModuleCreatedUseDefs;

  //! Group NUUseDefNodes from the same module together.
  /*!
   * Iterate over the mCreatedUseDefs set and group the NUUseDefNodes
   * by containing module.
   */
  void groupBlocksByModule(ModuleCreatedUseDefs & moduleCreatedUseDefs);

  //! Cleanup symtab references for rescoped nets.
  /*!
   * Eliminate signatures and storage ptrs from the symbol table. We
   * have unelaboratedly determined that nets in 'rescoped_nets' are
   * local in scope and can be converted into temporaries. They are
   * now block-local and need no module-level storage (removing the
   * storage ptr) and do not need to be scheduled (removing the
   * signature).
   */
  void cleanupSymtabForNets(NUNetSet & rescoped_nets);

  //! Type to keep track of new sequential always blocks created
  /*! When we convert from latches to flops, we create one or more
   *  sequential always blocks. Once they are all created then we go
   *  through the process of fixing up all the Nucleus and flow for
   *  the changes. This type is used to keep track of the new to old
   *  always blocks so that the process can run once over all the
   *  changes.
   */
  typedef UtMap<NUAlwaysBlock*, NUAlwaysBlockVector> OldToNewAlwaysBlocks;

  //! Iterator over map from old to new always blocks in latch conversion
  typedef LoopMap<OldToNewAlwaysBlocks> OldToNewAlwaysBlocksLoop;

  //! Data for the new always blocks (see OldToNewAlwaysBlocks)
  OldToNewAlwaysBlocks* mOldToNewAlwaysBlocks;

  //! Type for clock constant replacements for every new always block
  typedef UtMap<NUAlwaysBlock*, NUExprExprDeepHashMap*> ClkConstReplacements;

  //! Data for clock constant replacements
  /*! This data is gathered during flop creation process. It is then
   *  used in figuring otu the clock constant replacements during the
   *  fixupNewFlops pass.
   */
  ClkConstReplacements* mClkConstReplacements;

  //! Gather all the constant replacements for this always and priority blocks
  void findClockValues(NUAlwaysBlock* newAlways);

  //! Figure out a constant for a clock or priority block
  void addConstExpr(NUAlwaysBlock* newAlways, NUAlwaysBlock* clkAlways,
                    bool invertValue);


  //! Add a new always block to our old to new always blocks map
  void addNewAlways(NUAlwaysBlock* oldAlways, NUAlwaysBlock* newAlways);

  //! Connect the clock block pointers for all converted flops
  void connectClockPointers();

  //! A depth, always block pair used to find an appropriate clock block
  typedef std::pair<int, NUAlwaysBlock*> DepthBlockPair;

  //! A map to record the current best clock block for every block
  typedef UtMap<NUAlwaysBlock*, DepthBlockPair> DepthRootMap;

  //! Iterator over the blocks and their best clock block
  typedef LoopMap<DepthRootMap> DepthRootMapLoop;

  //! Recursively determine the block depth for any block
  DepthBlockPair determineBlockDepth(NUAlwaysBlock* always,
                                     DepthRootMap* depthRootMap);

  //! Replace clock expressions with constants
  void replaceConstantClocks();

  //! When we are all done with clock replacement map, delete it
  void deleteClkConstReplacementMap();

  //! Abstraction to keep track of the new unelaborated flows created
  /*! During latch to flop conversion we can create more than one flop
   *  to represent the latch. This occurs when there are multiple
   *  clocks (async resets) involved. This means we need to create new
   *  unelaborated flow. The original unelaborated flow for the latch
   *  is fixed to point to the first always block. The remaining flops
   *  have unelaborated flow created for it.
   *
   *  This type maps the fixed unelaborated flow to the new
   *  unelaborated flows created.
   */
  typedef UtMap<FLNode*, FLNodeVector> NewFlowsMap;

  //! Abstraction to keep track of the new elaborated flows created
  /*! This map is identical to the unelaborated flow map above except
   *  for the elaborated flow graph.
   */
  typedef UtMap<FLNodeElab*, FLNodeElabVector> NewElabFlowsMap;

  //! Function to repair the flow after latch to flop conversion
  void repairAlwaysFlow(NUDesign* design, Stats* stats);

  //! Create elaborated nets for all temporary clocks created
  void elaboratedClockNets();

  //! Function to create new unelaborated flow for converted latches
  void createUnelaboratedAlwaysFlow(NewFlowsMap* newFlowsMap);

  //! Function to create new elaborated flow for converted latches
  void createElaboratedAlwaysFlow(NewFlowsMap& newFlowsMap);

  //! Function to reconnect any fanout flow for converted latches
  void connectNewFanin(FLNode* flow, Iter<FLNode*> l,
                       const NewFlowsMap& newFlowsMap,
                       void (FLNode::* connector)(FLNode*),
                       void (FLNode::* clear_fanin)(),
                       bool (FLNode::* overlap_check)(const FLNode *, NUNetRefFactory*));

  //! Function to reconnect any elaborated fanout flow for converted latches
  void connectNewFanin(FLNodeElab* flowElab,
                       FLNodeElabLoop loop,
                       NewElabFlowsMap& newElabFlowsMap,
                       FLElabUseCache* cache,
                       void (FLNodeElab::* connector)(FLNodeElab*),
                       void (FLNodeElab::* clear_fanin)(),
                       bool (FLNodeElab::* check_overlap)(const FLNodeElab *, NUNetRefFactory*, FLElabUseCache*));

  //! Create a new block assignment to be put into an always block
  static NUBlockingAssign* createAssign(NUNet* defNet, NUExpr* rvalue,
                                        const SourceLocator& loc,
                                        NUStmtList* stmtList);

  //! Create a new block assignment to be put into an always block
  static NUBlockingAssign* createAssign(NULvalue* lvalue, NUExpr* rvalue,
                                        const SourceLocator& loc,
                                        NUStmtList* stmtList);

  //! Create a new always block from a list of blocking assignments
  NUAlwaysBlock* createAlways(NUModule* module, NUStmtList& stmtList,
                              const SourceLocator& loc);

  //! Find all the hierarchies for a set of modules
  void findModuleHierarchies(NUModuleSet& moduleSet,
                             ModuleHierarchies* moduleHierarchies);

  //! Verify that all buffered nets were buffered correctly
  void verifyBufferedNets(NUAlwaysBlock* always, const NUNetRefSet& inNets);

  //! Abstraction for the cache of temp clocks created
  typedef UtMap<NUModule*, NUExprExprDeepHashMap*> TempClkExprs;

  //! Cache all the temporary clocks created so we don't create duplicates
  TempClkExprs* mTempClkExprs;

  //! Function to create or get a cached temp clock from an existing expression
  NUExpr* createTempClk(NUModule* module, NUExpr* clk, NUAlwaysBlock* latch);

  //! Function to get a cached temp clock for an existing clock expression
  /*! This function asserts if the clock expression is not found
   */
  NUExpr* getTempClk(NUModule* module, NUExpr* clk);

  //! Delete the temporary clock map data (done creating temporary clocks)
  void deleteTempClkExprs();

  // Helper classes
  STSymbolTable *mSymtab;
  AtomicCache *mStrCache;
  SourceLocatorFactory * mLocFactory;
  FLNodeFactory *mFlowFactory;
  FLNodeElabFactory *mFlowElabFactory;
  NUNetRefFactory *mNetRefFactory;
  MsgContext *mMsgContext;
  IODBNucleus * mIODB;
  ArgProc * mArgs;
}; // class RETransform


#endif // _RETRANSFORM_H_
