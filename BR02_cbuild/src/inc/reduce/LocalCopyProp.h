// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _LOCALCOPYPROP_H_
#define _LOCALCOPYPROP_H_

#include "reduce/LocalPropagationStatistics.h"

class IODBNucleus;
class UD;
class Fold;

//! class RELocalCopyProp
/*! 
  Simple local copy propagator.
 */
class RELocalCopyProp
{
public:
  //! Constructor.
  RELocalCopyProp(IODBNucleus* iodbNucleus, UD* ud, Fold* fold, 
                  LocalPropagationStatistics * statistics,
                  bool verbose);

  //! Destructor.
  ~RELocalCopyProp();

  //! Propagate within a module.
  void module(NUModule* module);

  //! Propagate within an always block.
  void alwaysBlock(NUAlwaysBlock* always);

private:
  //! Hide copy and assign constructors.
  RELocalCopyProp(const RELocalCopyProp&);
  RELocalCopyProp& operator=(const RELocalCopyProp&);

  // If true, print the blocks that were optimized
  bool mVerbose;

  //! IODB object.
  IODBNucleus* mIODBNucleus;

  //! UD
  UD* mUD;

  //! Fold object
  Fold* mFold;

  //! Statistics object.
  LocalPropagationStatistics * mStatistics;
}; // class RELocalCopyProp

#endif // _LOCALCOPYPROP_H_
