// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef SPLIT_FLOW_H_
#define SPLIT_FLOW_H_

#include "nucleus/Nucleus.h"
#include "flow/Flow.h"
#include "flow/FlowUtil.h"

/*!
  \file
  Declaration of flow recomputation package.
 */

class STSymbolTable;
class AtomicCache;
class SourceLocatorFactory;
class FLNodeFactory;
class FLNodeElabFactory;
class NUNetRefFactory;
class MsgContext;
class IODBNucleus;
class ArgProc;
class Stats;

class STBranchNode;
class Elaborate;

class ClockAndDriver;

class SplitFlowFactory;
class SplitFlowElabFactory;

//! Flow recomputation interface.
class SplitFlow
{
public:
  //! Constructor.
  SplitFlow(STSymbolTable *symtab,
	    AtomicCache * str_cache,
	    SourceLocatorFactory *loc_factory,
	    FLNodeFactory * flow_factory,
	    FLNodeElabFactory * flow_elab_factory,
	    NUNetRefFactory *netref_factory,
	    MsgContext * msg_ctx,
            IODBNucleus * iodb,
	    ArgProc *args) :
    mSymtab(symtab),
    mStrCache(str_cache),
    mLocFactory(loc_factory),
    mFlowFactory(flow_factory),
    mFlowElabFactory(flow_elab_factory),
    mNetRefFactory(netref_factory),
    mMsgContext(msg_ctx),
    mIODB(iodb),
    mArgs(args)
  {}

  //! destructor
  ~SplitFlow() {}

  //! Re-compute the flow graphs for a set of always blocks.
  /*!
   * \param design The design. Used to perform dead code removal.
   *
   * \param replacement_blocks The outgoing blocks. Unelaborated and
   *               elaborated flow is recomputed within these blocks.
   *               The limit to flow recomputation is defined by the
   *               continuous * inputs and outputs of the blocks.
   *
   * \param create_declarations Do the replacement blocks contain
   *               declarations which need elaborating? This is
   *               currently an all or nothing decision. If an
   *               elaborated * declaration for any nets already
   *               exists, re-elaborating the * declarations may
   *               trigger an error.
   *
   * TBD: Determine if we can always always attempt to re-declare the
   * replacement blocks. Recent changes to elaboration may allow
   * multiple calls to NUNet::createElab without issue.
   */
  void repair(NUDesign * design,
	      NUAlwaysBlockSet & replacement_blocks,
	      FLNodeSet & boundary_flow,
	      FLNodeConstElabMap & flow_to_elab,
	      FLNodeElabSet* deleted_flow_elabs,
              ClockAndDriver * clock_and_driver,
	      bool create_declarations,
              Stats* stats);

  //! Map from module to the modified flow nodes within that module.
  typedef UtMap<NUModule*, FLNodeSet> ModuleToFlow;

  //! Set of hierarchical paths.
  typedef UtSet<STBranchNode*> HierSet;

private:
  //! Re-compute the flow graphs for a set of always blocks.
  /*!
   * \param replacement_blocks The set of modified blocks needing
   *                           re-elaboration.
   *
   * \param boundary_flow      Unelaborated flow that models the 
   *                           outputs of the 'replacement_blocks'.
   *
   * \param flow_to_elab       Map from unelaborated flow node to 
   *                           all of its elaborated versions.
   *
   */
  void fixupFlow(NUAlwaysBlockSet & replacement_blocks,
                 FLNodeSet & boundary_flow,
                 FLNodeConstElabMap & flow_to_elab,
                 ClockAndDriver * clock_and_driver);

  //! Run unelaborated and elaborated dead code/flow elimination.
  void cleanupFlow(NUDesign * design, FLNodeElabSet * deleted_flow_elabs,
                   Stats* stats);

  //! Run unelaborated and elaborated code/flow sanity checks.
  void sanityChecks(NUDesign * design, Stats* stats);

  //! Given a set of blocks, make sure our boundary set is complete.
  /*!
   * Iterate over the unelaborated flow factory and ensure that each
   * FLNode referencing an always block in _blocks_ is present in
   * _boundary_.
   */
  void completeBoundary(NUAlwaysBlockSet & blocks,
                        FLNodeSet & boundary);

  //! Given an unelaborated boundary, make sure we have all associated elaborated flow.
  /*!
   * Iterate over the elaborated flow factory and ensure that each
   * FLNodeElab referencing a FLNode in _boundary_ is present in the
   * _flow_to_elab_ entry for that FLNode.
   */
  void completeElabFlowMap(FLNodeSet & boundary,
                           FLNodeConstElabMap & flow_to_elab);

  //! Populate the unelaborated map of pre-existing flow for a specific net.
  void setupDriverLookup(NUNetNodeToFlowMap & flow_lookup,
                         NUNet * net);
  
  //! Populate the elaborated map of pre-existing flow for a specific net elab.
  void setupDriverLookup(FlowNetHierToFlowElabMap & flow_lookup,
                         NUNetElab * net);

  //! Populate the unelaborated map of pre-existing flow.
  void setupFlowLookup(NUNetNodeToFlowMap & flow_lookup,
                       FLNodeSet & boundary_flow);

  //! Populate the unelaborated map of pre-existing flow for a specific flow node.
  void setupOneFlowLookup(NUNetNodeToFlowMap & flow_lookup,
                          FLNode * flow);

  //! Populate the elaborated map of pre-existing flow for the specified boundary.
  void setupFlowElabLookup(FlowNetHierToFlowElabMap & flow_elab_lookup,
                           FLNodeSet & boundary_flow,
                           FLNodeConstElabMap & flow_to_elab);

  //! Populate the elaborated map of pre-existing flow for a specific flow node.
  void setupOneFlowElabLookup(FlowNetHierToFlowElabMap & flow_elab_lookup,
                              FLNodeElab * flow_elab);

  //! Map from module to the modified always blocks within that module.
  typedef UtMap<NUModule*, NUAlwaysBlockSet> ModuleCreatedAlways;

  //! Recreate the unelaborated flow for all of the modified blocks.
  /*!
   * Using some set of pre-existing unelaborated flow nodes, generate
   * the remainder of the flow graph for a specified set of blocks.
   *
   * \param split_flow_factory    An unelaborated flow factory with a
   *                              cache of pre-existing nodes.
   *
   * \param module_created_always A map from module to modified always
   *                              blocks within that module. Flow is
   *                              recalculated on a per-module basis;
   *                              blocks in the same module are
   *                              processed at the same time.
   */
  void doUnelaboration(SplitFlowFactory * split_flow_factory,
                       ModuleCreatedAlways & module_created_always);

  //! Recreate the elaborated flow for all of the modified blocks.
  void doElaboration(SplitFlowFactory * split_flow_factory,
		     SplitFlowElabFactory * split_flow_elab_factory,
                     ModuleCreatedAlways & module_created_always,
		     FLNodeSet & boundary_flow,
		     FLNodeConstElabMap & flow_to_elab,
                     ClockAndDriver * clock_and_driver);

  //! Elaborated declaration for all the modified blocks within a given piece of hierarchy.
  void setupElaboration(Elaborate * elaborate,
			STBranchNode * hier,
                        NUAlwaysBlockSet & blocks_for_hierarchy);

  //! Mark the continuous fanin of the modified flow as stopping points for the re-elaboration.
  /*!
   * \param split_flow_set Unelaborated drivers for the modified
   *                 blocks. These unelaborated flow nodes are the
   *                 starting points for the re-elaboration.
   *
   * \param flow_elab_set  Set of elaborated flow nodes associated with the modified blocks. 
   */
  void markStoppingPoints(FLNodeSet & starting,
			  FLNodeSet & stoppers);

  //! Mark drivers of a net as stopping points for the re-elaboration.
  /*!
   * For each driver of a net, add that driver as a re-elaboration
   * stopping point unless the driver is already considered a starting
   * point.
   *
   * \param net       Mark the drivers for this net.
   * \param starting  Starting points for re-elaboration.
   * \param stoppers  Stopping points for re-elaboration.
   */
  void markStoppingDrivers(NUNet * net,
			   FLNodeSet & starting,
			   FLNodeSet & stoppers);

  //! Container for (net,node) pair. 
  /*!
   * Used to determine if a net has multiple drivers contained within
   * a use-def node.
   */
  class MultiDriverNetNode {
  public:
    MultiDriverNetNode(const NUNet * net, NUUseDefNode * node) :
      mNet(net),mNode(node) {}
    bool operator<(const MultiDriverNetNode &other) const {
      return (mNode==other.mNode) ? (*mNet)<(*other.mNet) : mNode<other.mNode;
    }
  private:
    const NUNet * mNet;
    NUUseDefNode * mNode;
  };

  //! Container for (net-elab,hierarchy,node) triple. 
  /*!
   * Used to determine if an unelaborated net has multiple elaborated
   * drivers contained within a use-def node.
   *
   * We do not try and determine if a single elaborated net has
   * multiple drivers within a single use-def node, as different
   * unelaborated nets may become aliased through hierarchy. These
   * multiple driver situations are not interesting.
   */
  class MultiDriverNetHierNode {
  public:
    MultiDriverNetHierNode(NUNet*net,STBranchNode*hier,NUUseDefNode*node) :
      mNet(net),mHier(hier),mNode(node) {}
    bool operator<(const MultiDriverNetHierNode & other) const {
      return (mNode==other.mNode) ? (mHier==other.mHier) ? (*mNet)<(*other.mNet) : mHier<other.mHier : mNode<other.mNode;
    }
  private:
    NUNet * mNet;
    STBranchNode * mHier;
    NUUseDefNode * mNode;
  };

  //! Map of (net,node) information to flow node representing that driver.
  typedef UtMap<MultiDriverNetNode,FLNode*> MultiDriverFlowLookup;

  //! Map of (net-elab,hierarchy,node) information to elaborated flow node representing that driver.
  typedef UtMap<MultiDriverNetHierNode,FLNodeElab*> MultiDriverFlowElabLookup;

  //! Resolve multiple drivers.
  /*!
   * Determine if we have merged multiple drivers of a net into a
   * single always block. In this situation, only one of these drivers
   * will persist. Choose a canonical driver and rewrite fanin sets as
   * appropriate.
   */
  void findMultipleDrivers(FLNodeSet & flow_set,
			   FLNodeConstElabMap & flow_to_elab);

  //! Use the mFlowTranslate map to eliminate boundary flow for multiply driven nets.
  /*!
   * findMultipleDrivers has already discovered all of the drivers for
   * a given net and merged all their driving net-refs into one,
   * representative flow node. The translate map dictates which flows
   * have been consumed by the representative and are fit to remove.
   */
  void removeMultiDriversFromBoundary(FLNodeSet & flow_set);

  /*! 
   * When merging blocks, not all net definitions are reflected in the
   * continuous flow graph. For each boundary flow node, be sure that
   * its def-net-ref includes all defined bits.
   * 
   * For example, if we merge the following two blocks:
   \code
     assign x[0] = a & b;
     always begin
       x[1] = c & d;
       y = x[1];
     end
   \endcode
   *
   * and there are other, unmerged readers of x[0] but no other
   * readers of x[1], there will be no continuous flow node
   * representing the driver for x[1]. 
   *
   * When these two blocks are merged, we need to produce one
   * continuous flow node for all of x.
   */
  void addInternalDefsToFlow(FLNodeSet & flow_set);

  //! Update fanin pointers to reflect eliminated multi-driver flow.
  void updateFaninForMultiDrivers();

  /*!
   * Update unelaborated fanin sets based on multiple-driver resolution.
   */
  void updateFanin(FLNode * flow);
  /*!
   * Update elaborated fanin sets based on multiple-driver resolution.
   */
  void updateElabFanin(FLNodeElab * flow);

  //! Get the set of nets which need re-elaboration.
  void getStartingNets(NUAlwaysBlockSet & blocks_for_hierarchy,
                       NUNetList & starting_nets);

  //! Create a map from module to modified block.
  /*!
   * This is used so we are able to process all blocks within a module
   * at once.
   */
  void mapAlwaysToModule(NUAlwaysBlockSet & replacement_blocks,
                         ModuleCreatedAlways & module_created_always);

  void populateElaborationData(FLNodeSet & boundary_flow,
                               FLNodeConstElabMap & flow_to_elab,
                               HierSet & hier_set,
                               ModuleToFlow & module_to_flow);

  //! Find all instantiations of the referenced modules.
  /*!
   * Find all instantiations of the referenced modules, even if they
   * weren't discovered in the elaborated flow we were handed. This
   * helps us make sure that we don't miss elaborating any generated
   * temporary nets.
   */
  void findAllModuleHierarchy(const ModuleToFlow & module_to_flow,
                              HierSet & hier_set);

  void populateElaborationDataElabFlowSet(FLNodeElabSet& elab_set,
                                          HierSet & hier_set,
                                          ModuleToFlow & module_to_flow);

  void prepareNestedFlowForDestruction(FLNode * flow);
  void prepareNestedFlowForDestruction(FLNodeElab * flow);


  // Translation table for multiple driver situations -- they become
  // single-drivers after flow fixup.
  FLNodeToFLNodeMap mFlowTranslate;
  FLNodeElabToFLNodeElabMap mFlowElabTranslate;

  //! The design;
  NUDesign * mDesign;

  //! Symbol table.
  STSymbolTable *mSymtab;

  //! String cache.
  AtomicCache * mStrCache;

  //! Source locator factory
  SourceLocatorFactory *mLocFactory;

  //! Unelaborated flow factory.
  FLNodeFactory * mFlowFactory;

  //! Elaborated flow factory.
  FLNodeElabFactory * mFlowElabFactory;

  //! NetRef factory.
  NUNetRefFactory * mNetRefFactory;

  //! Message context.
  MsgContext * mMsgContext;

  //! IODB.
  IODBNucleus* mIODB;

  //! Command-line interface.
  ArgProc *mArgs;

  //! Should nets contained within input blocks be elaboratedly declared?
  /*!
   * TBD: Try to always re-declare nets (see constructor comment)
   */
  bool mCreateDeclarations;
};

#endif
