// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef FLATTEN_ANALYZER_H_
#define FLATTEN_ANALYZER_H_

#include "util/CarbonTypes.h"

#include "nucleus/Nucleus.h"

class MsgContext;

//! Interface to determine which module instances are valid flattening candidates
class FlattenAnalyzer
{
public:
  enum Mode {
    eModeEnable,                //!< Flattening is allowed below the current module
    eModeDisable,               //!< Flattening is not allowed below the current module
    eModeForce,                 //!< Flattening is forced below and including the current module.
    eModeForceContents,         //!< Flattening is forced below (not including) the current module.
    eModeUnknown                //!< Unknown mode.
  };

  //! Track why the flattening may have failed.
  enum Status {
    eStatusSuccess,             //!< Inlining is allowed.
    eStatusForced,              //!< Inlining forced by directive (internal state).
    eStatusLargeCandidates,     //!< Both parent and child are larger than mInlineLimit.
    eStatusParentGrowth,        //!< Parent module gained too much complexity.
    eStatusNonLeaf,             //!< Child is not a leaf module (has submodules).
    eStatusUnknownPort,         //!< Instance has an unhandled expression type as its port.
    eStatusPortNoScalarize,     //!< Concat actual connected to a non-scalarizable formal.
    eStatusDirective,           //!< Flattening disabled by user directive.
    eStatusHierRefTasks,        //!< Flattening not permitted because of tasks with hierarchical references.
    eStatusError,               //!< An error occurred (directive inconsistency).
    eStatusUnknown 		//!< Some other failure.
  };

  //! Track the sizes of a flattening.
  class FlattenSize {
  public:
    FlattenSize() : parent(0), child(0), combined(0) {}
    SInt32 parent;
    SInt32 child;
    SInt32 combined;
  };

  //! Constructor.
  FlattenAnalyzer(MsgContext * msg_context,
                  NUCostContext * design_costs,
                  NUCostContext * costs,
                  NUModule * module,
                  Mode flatten_mode,
                  SInt32 inline_limit,
                  bool flatten_only_leaves,
                  bool flatten_single_instances,
                  bool flatten_hier_ref_tasks,
                  UInt32 tiny_threshold,
                  UInt32 parent_size_limit);

  //! Destructor.
  virtual ~FlattenAnalyzer();

  //! Determine the new flattening mode.
  /*! 
    Determine the new flattening mode based on the current mode and
    directives applied to the current module.

    \param module      The current module.

    \param msgContext  Message context - used to print warnings about
                       directive inconsistencies.

    \param currentMode Incoming flattening mode; inherited from the
                       parent module.

    \return Mode derived from incoming mode and directives.
   */
  static Mode determineFlattenMode(const NUModule * module, MsgContext * msgContext, Mode currentMode);

  //! Qualify a submodule based on cost metrics.
  /*!
    \param submodule The submodule under consideration.
    
    \param size (output) Size information for the submodule.

    \return If eStatusSuccess, flattening is allowed. All other values
            disqualify flattening of the submodule.
   */
  Status qualified ( const NUModule * submodule, FlattenSize * size ) const;

private:

  //! Analyze all submodules under the current module.
  /*!
    All submodules of a given module are analyzed before any
    flattening operations begin. The analysis considers all instances
    of a single module at the same time.

    The -flattenSingleInstances switch is now on by default. It no
    longer blindly flattens singly-instantiated modules. Instead, if
    those modules are too large (a multiple of the flattening
    threshold), they are disqualified.

    Modules slightly larger than the threshold are also flattened,
    provided that the cumulative complexity of all instances in the
    design is small (compared to a multiple of the flattening
    threshold).

    Parent modules stop allowing flattening of child modules if they
    grow too large (hard threshold of 10k).
  */
  void analyze();

  //! Analyze one submodule of the current module.
  Status analyze(NUModule * submodule) ;

  //! Compute the complexity metric for the profived module.
  /*!
    Complexity of a module is no longer purely computed by the number
    of assignments in that module. The assignment count is reduced by
    the ratio of runtime instructions to instructions. We use the
    ratio of runinstr/instr as a predictor of optimization
    opportunity. We do not allow this ratio to increase the complexity
    of a module (runtime instructions could be greater in the case of
    a for loop, for example). We also do not allow the ratio to reduce
    the complexity of the module by more than half (large case
    statements, for example).
   */
  SInt32 computeComplexity(const NUModule * module) const;

  //! Map from a submodule to all of its instances within the current module
  typedef UtMap<NUModule*,NUModuleInstanceSet> NUModuleToModuleInstanceMap;

  //! Map of module complexity to a set of submodules with that complexity.
  typedef UtMap<SInt32,NUModuleSet> ModuleSizeHistogram;

  //! State of flattening for the provided module.
  typedef UtMap<const NUModule*,FlattenAnalyzer::Status> ModuleStatusMap;

  ModuleStatusMap mSubmoduleStatus;

  //! Message context.
  MsgContext * mMsgContext;

  //! Design-wide cost context. Used to determine instantiation counts.
  NUCostContext * mDesignCostCtx;

  //! Module-level cost context.
  NUCostContext * mModuleCostCtx;

  //! Parent module for flattening candidates.
  NUModule * mModule;

  //! Flattening mode for the current (parent) module.
  Mode mFlattenMode;

  //! Maximum size (in # of assigns) of modules being flattened.
  SInt32 mInlineLimit;

  //! Only allow flattening of leaf instances.
  bool mFlattenOnlyLeaves;

  //! Flatten singly-instantiated modules regardless of size.
  bool mFlattenSingleInstances;

  //! Allow flattening of modules containing tasks with hierarchical references.
  /*!
   * This is controlled separately because flattening can cause
   * situations where, unelaboratedly, a task enable will have
   * multiple resolutions.
   */
  bool mFlattenHierRefTasks;

  //! Threshold below which we will flatten a module into its parent
  //! independent of how big the parent already is, on the grounds that
  //! the contents of the module are no more costly than the module
  //! instance
  SInt32 mTinyThreshold;

  //! Threshold for parent-size above which we will avoid flattening
  //! a child module into its parent, unless the child module is really
  //! tiny (see mTinyTreshold)
  SInt32 mParentSizeLimit;
};

#endif
