// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION

  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING AND/OR
  DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN
  CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _TIENETS_H_
#define _TIENETS_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUDesign.h"   // for InstancesLoop

class Fold;

//! Class to perform tie nets with module based tie net directives
/*! This class is used to tie a module net to a constant therby
 *  removing all previously driving logic and potentially optimizing
 *  any logic that uses the net.
 *
 *  The routines are called to implement the tieNet directive and to
 *  do some of the global constant propagation optimizations (when the
 *  net being optimized is the entire net and it is a port). The code
 *  is shared here because the operations are similar.
 */
class RETieNets
{
public:
  //! constructor
  RETieNets(NUDesign* design, AtomicCache* strCache,
            IODBNucleus* iodbNucleus, MsgContext* msgContext,
            Fold* fold,
            bool verbose, bool tying, NUNetSet* tieNetTemps);

  //! destructor
  ~RETieNets();

  //! Process the tie net directives (records tie nets)
  /*! Visits all module nets in the design and queries the IODB
   *  whether it has been tied or not. If it has been tied, it uses
   *  addModuleNet to record that fact.
   */ 
  void processDirectives();

  //! NetValue class
  /*! This class stores information about a tie net value. It contains
   *  the constant value as well as the source node and whether it was
   *  the result of a user tieNet directive or constant propagation.
   */
  class NetValue
  {
  public:
    //! constructor for tieNet Directive
    NetValue(const DynBitVector* value, const STSymbolTableNode* node) :
      mValue(value), mNode(node)
    {}

    //! constructor for constant
    NetValue(const DynBitVector* value) : mValue(value), mNode(NULL) {}

    //! Get the constant value
    const DynBitVector* getConstant() const { return mValue; }

    //! Get the original node if this was from a tie directive
    const STSymbolTableNode* getSourceNode() const { return mNode; }

  private:
    const DynBitVector* mValue;
    const STSymbolTableNode* mNode;
  }; // class NetValue

  //! Add a tied net
  /*! Records a module and net with a constant value. This indicates
   *  the net can be safely tied to that value.
   *
   *  \param module The module in which the net resides.
   *
   *  \param net The net which is tied to a constant.
   *
   *  It is illegal to call this routine twice on the same module and
   *  net. An assert will fire if this is done.
   *
   *  Once all the nets have been tied, use the optimize() function to
   *  modify the nucleus data structures.
   */
  void addModuleNet(NUModule* module, NUNet* net, const NetValue& netValue);

  //! Optimize the tie nets (modifies nucleus)
  /*! This routine performs all the modifications accumulated with the
   *  addModuleNet function. The modifications are:
   *
   *  1. It removes all port connections in the parent of modules that
   *     have nets that are tied to constants.
   * 
   *  2. It adds a continuous constant assign in the parent of all output
   *     ports.
   *
   *  3. It replaces all lvalues for constant nets with a temp.
   *
   *  4. It replaces all rvalues for constant nets with the constant
   *     except for edge expressions.
   *
   *  5. It removes all port declarations for constant nets and modifies
   *     the flags to be a local net.
   *
   *  6. It adds a continuous assign to all nets that are tied to
   *     constants in the module (needed for edge expressions).
   *
   * \param tieStrength if true, the added continous assigns are added
   * at tie strength which override drive strength.
   */
  void optimize(bool tieStrength = false);

  //! Helper function to create a continous assign from a constant
  /*! Use this function to create a new continous assign in a module
   *  to a provided lvalue.
   *
   *  \param net - The net that is being assigned
   *
   *  \param lvalue - An lvalue that represents the part of the net
   *  that is being assigned.
   *
   *  \param parentModule - The module this net belongs to
   *
   *  \param value - A dynamic bit vector with the constant value to
   *  assign
   *
   *  \param tieStrength - if set the assign is created at tie
   *  strength which overrides drive strength.
   */
  void createConstContAssign(NUNet* net, NULvalue* lvalue,
                             NUModule* parentModule, const DynBitVector* value,
                             bool tieStrength);

  //! Walk the design looking for possible tienet issues
  /*! Tienet directives are applied locally which means they cannot
   *  remove all drivers of a net since some of them can be driving
   *  aliases of a tied net.
   *
   *  This is solved by making the tienet assignment statement run at
   *  a higher strength. However, it does not solve all
   *  issues. Consider the following case where there are multiple
   *  drivers in different modules:
   *
   *  \verbatim
   *     module1:
   *
   *            assign data = in1 & in2;
   *
   *     module2:
   *
   *            always @ (in3 or in4)
   *              begin
   *                data = in3 & in4;
   *                out = ~data;
   *              end
   *  \endverbatim
   *
   *  Assume that module1.data is tied to constant 1 and that
   *  module1.data and module2.data are aliased. In this case the
   *  tie-strength driver in module1 will run second and therefore
   *  override module2.data's assignment. However, module2.out will
   *  see the untied value because it is scheduled together with the
   *  data = in3 & in4 assignment. Also remember that because there
   *  can be multiple instances of module2 we cannot always just
   *  delete the code.
   *
   *  Today this routine simply warns when this condition occurs. In
   *  the future this can be fixed. The current thinking is to mark
   *  the net forcible and force the instances that are tied. The
   *  problem with this is that the forcible flag is applied using a
   *  hierarchical closure algorithm which can cause a performance
   *  degradation.
   */
  void findTienetIssues(void);

private:
  // Helper functions
  NUDesign::InstancesLoop loopInstances(NUDesign::ModuleInstances*, NUModule*);

  // Types, data, and functions for storing constant nets. We keep
  // them sorted so that the print order is consistent across
  // compiles.
  typedef UtMap<NUNet*, NetValue, NUNetCmp> NetValues;
  typedef LoopMap<NetValues> NetValuesLoop;
  typedef UtMap<NUModule*, NetValues*, NUModuleCmp> ConstModuleNets;
  typedef LoopMap<ConstModuleNets> ConstModuleNetsLoop;
  ConstModuleNets* mConstModuleNets;

  // Private optimization routines
  void optimizeInstancePorts(NUModuleInstance* modInst, NetValues*, bool);
  void optimizeModuleNets(NUModule* module, NetValues* netValues, bool);
  void replaceLvalues(NUModule* module, NetValues* netValues, bool);
  void replaceRvalues(NUModule* module, NetValues* netValues);
  void removePorts(NUModule* module, NetValues* netvalues);
  void printOptimizedNets(NetValues* netValues);
  void addContAssigns(NUModule* module, NetValues* netValues, bool);
  void optimizePort(NUModule* parentModule, NUNet* formal,
                    NULvalue* lvalue, NetValues* netValues,
                    bool tieStrength);
  const char* createValueString(NUNet* net, const DynBitVector* value,
                                UtString* buf);

  // Private tie net issues analysis routines
  void analyzeOtherDriver(NUNet* net);

  // If set, we print messages about optimizations
  bool mVerbose;

  // If true, print messages about "Tied" nets; "Optimized" otherwise.
  bool mTying;

  //! Utility classes
  NUDesign* mDesign;
  AtomicCache* mStrCache;
  IODBNucleus* mIODBNucleus;
  MsgContext* mMsgContext;
  Fold* mFold;

  // Place to store tieNet temps created
  NUNetSet* mTieNetTemps;
};

#endif // _TIENETS_H_
