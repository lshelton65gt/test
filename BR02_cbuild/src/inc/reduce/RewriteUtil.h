// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef REWRITE_UTILITY_H_
#define REWRITE_UTILITY_H_

#include "nucleus/Nucleus.h"

class Fold;

/*!
  \file
  Declaration of rewrite utility package.
 */

//! Kitchen sink functor for performing Nucleus rewrites.
class RewriteLeaves : public NuToNuFn
{
public:
  //! Constructor
  /*!
    \param net_replacements Map specifying net -> net transformation
        rules. These rules can be applied to both RHS and LHS net
        references.

    \param lvalue_replacements Map of net -> LHS Nucleus elements to
        stipulate rewrites. These rewrites can be applied to both RHS
        and LHS references involving the source net. All lvalue types
        are convertible to RHS expressions, so there are no rewriting
        constraints, unlike the rvalue replacement map.
    
    \param rvalue_replacements Map of net -> RHS Nucleus elements to
        stipulate rewrites. These rewrites can be applied to both RHS
        and LHS references involving the source net. Since this
        rewrite can occur on the LHS, only convertible expressions
        should be used as map values.

        Examples of good rewrite specifications include:
            net_a -> net_b
            net_a -> net_b[10:0]
            net_a -> {net_b,net_c}

        Examples of poor rewrite specifications include:
            net_a -> net_b + net_c
            net_a -> en ? net_b : net_c

        These rewrite requirements are weakly enforced. An ill-formed
        replacement will probably not fire, though worse things are
        possible.

    \param tf_replacements Map specifying tf -> tf transformation
        rules. These rules will modify the NUTF reference in
        NUTaskEnable and NUTFArgConnection objects.

    \param cmodel_replacements Map specifying cmodel -> cmodel
        transformation rules. These rules will modify the NUCModel
        reference in NUCModelCall objects.

    \param always_replacements Map specifying always -> always
        transformation rules. These rules will model the clock and
        priority pointers within visited always blocks.
   */
  RewriteLeaves ( NUNetReplacementMap & net_replacements,
                  NULvalueReplacementMap & lvalue_replacements,
                  NUExprReplacementMap & rvalue_replacements,
                  NUTFReplacementMap  & tf_replacements,
                  NUCModelReplacementMap & cmodel_replacements,
                  NUAlwaysBlockReplacementMap & always_replacements,
                  Fold* fold,
                  bool postReplacement = false
    ) :
    mNetReplacements(net_replacements),
    mLvalueReplacements(lvalue_replacements),
    mRvalueReplacements(rvalue_replacements),
    mTFReplacements(tf_replacements),
    mCModelReplacements(cmodel_replacements),
    mAlwaysReplacements(always_replacements),
    mFold(fold),
    mPostReplacement(postReplacement)
  {}

  //! Translate one net to another.
  virtual NUNet * operator()(NUNet * net, Phase phase);

  //! Translate one tf to another.
  virtual NUTF * operator()(NUTF * tf, Phase phase);

  //! Translate one always block to another.
  virtual NUAlwaysBlock * operator()(NUAlwaysBlock * always, Phase phase);

  //! Translate one cmodel to another.
  virtual NUCModel * operator()(NUCModel * cmodel, Phase phase);

  //! Translate one lhs element to another.
  virtual NULvalue * operator()(NULvalue * lvalue, Phase phase);

  //! Translate one rhs expression to another.
  virtual NUExpr * operator()(NUExpr * rvalue, Phase phase);

private:
  NULvalue * makeIdentLvalue(const NUIdentLvalue * ident);

  NUExpr * makeIdentRvalue(const NUIdentRvalue * ident);

  NULvalue * makeIdentLvalueFromLvalue(const NUIdentLvalue * ident);

  NUExpr * makeIdentRvalueFromLvalue(const NUIdentRvalue * ident);

  NULvalue * makeIdentLvalueFromRvalue(const NUIdentLvalue * ident);

  NUExpr * makeIdentRvalueFromRvalue(const NUIdentRvalue * ident);

  NULvalue * makeIdentLvalueFromNet(const NUIdentLvalue * ident);

  NUExpr * makeIdentRvalueFromNet(const NUIdentRvalue * ident);

  const NULvalue * getReplacementLvalue(const NUNet * net);

  const NUExpr * getReplacementRvalue(const NUNet * net);

  NUNet * getReplacementNet(const NUNet * net);

  //! Our lookup structures.
  NUNetReplacementMap & mNetReplacements;
  NULvalueReplacementMap & mLvalueReplacements;
  NUExprReplacementMap & mRvalueReplacements;
  NUTFReplacementMap  & mTFReplacements;
  NUCModelReplacementMap & mCModelReplacements;
  NUAlwaysBlockReplacementMap & mAlwaysReplacements;

  //! A Fold object for cleaning up after transformations
  Fold* mFold;

  //! If true, do the replacement in the post pass
  bool mPostReplacement;
};

//! A version of the RewriteLeaves class that only replaces lvalues.
class REReplaceLvalues : public RewriteLeaves
{
public:
  //! Constructor
  /*!
    \sa RewriteLeaves for all parameter uses.

    The only difference is that no RHS rewrites are permitted. The
    description of allowed rewrites can still come from either RHS or
    LHS sources.
   */
  REReplaceLvalues ( NUNetReplacementMap & net_replacements,
                     NULvalueReplacementMap & lvalue_replacements,
                     NUExprReplacementMap & rvalue_replacements,
                     NUTFReplacementMap  & tf_replacements,
                     NUCModelReplacementMap & cmodel_replacements,
                     NUAlwaysBlockReplacementMap & always_replacements,
                     Fold* fold = NULL
    ) : RewriteLeaves(net_replacements, lvalue_replacements,
                      rvalue_replacements, tf_replacements,
                      cmodel_replacements, always_replacements,
                      fold)
  {}

  //! Do not permit expression (RHS) rewrites.
  NUExpr * operator()(NUExpr*, Phase) { return NULL; }

  //! Do not permit direct net rewrites.
  /*! 
    Nets are present below both LHS and RHS elements. Because we do
    not know the current context, do not directly perform a net to net
    transformation. All nets should be encapsulated within
    NUIdentLvalue or NUIdentRvalue objects; these are appropriately
    processed or skipped.
   */
  NUNet * operator()(NUNet *, Phase) { return NULL; }
};

//! A version of the RewriteLeaves class that only replaces rvalues.
class REReplaceRvalues : public RewriteLeaves
{
public:
  //! Constructor
  /*!
    \sa RewriteLeaves for all parameter uses.

    The only difference is that no LHS rewrites are permitted. The
    description of allowed rewrites can still come from either RHS or
    LHS sources.
   */
  REReplaceRvalues ( NUNetReplacementMap & net_replacements,
                     NULvalueReplacementMap & lvalue_replacements,
                     NUExprReplacementMap & rvalue_replacements,
                     NUTFReplacementMap  & tf_replacements,
                     NUCModelReplacementMap & cmodel_replacements,
                     NUAlwaysBlockReplacementMap & always_replacements,
                     Fold* fold = NULL
    ) : RewriteLeaves(net_replacements, lvalue_replacements,
                      rvalue_replacements, tf_replacements,
                      cmodel_replacements, always_replacements,
                      fold)
  {}

  //! Do not permit expression (RHS) rewrites.
  virtual NULvalue * operator()(NULvalue*, Phase) { return NULL; }

  //! Do not permit direct net rewrites.
  /*! 
    Nets are present below both LHS and RHS elements. Because we do
    not know the current context, do not directly perform a net to net
    transformation. All nets should be encapsulated within
    NUIdentLvalue or NUIdentRvalue objects; these are appropriately
    processed or skipped.
   */
  NUNet * operator()(NUNet *, Phase) { return NULL; }
};

//! replaceLeaves expr -> expr translator class that matches deep expressions.
class REReplaceDeepExprExpr : public NuToNuFn
{
public:
  //! constructor
  REReplaceDeepExprExpr(const NUExprExprDeepHashMap& exprMap) :
    mExprMap(exprMap)
  {}

  //! destructor
  virtual ~REReplaceDeepExprExpr() {}

  //! Overload the expr to expr call back
  NUExpr* operator()(NUExpr* expr, Phase phase);

protected:
  const NUExprExprDeepHashMap& mExprMap;

}; // class REReplaceDeepExprExpr : public NuToNuFn

//! replaceLeaves expr -> expr  or lvalue -> lvalue
class REReplaceDeep : public REReplaceDeepExprExpr
{
public:
  //! constructor
  REReplaceDeep(const NUExprExprDeepHashMap& exprMap, const NULvalLvalDeepHashMap& lvalMap) :
    REReplaceDeepExprExpr(exprMap), mLvalMap (lvalMap)
  {}

  //! destructor
  virtual ~REReplaceDeep() {}

  //! Overload the lvalue callback
  NULvalue* operator() (NULvalue* lvalue, Phase phase);

protected:
  const NULvalLvalDeepHashMap& mLvalMap;
}; // class REReplaceDeep : public REReplaceDeepExprExpr

#endif
