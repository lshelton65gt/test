// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef RETRIINIT_H_
#define RETRIINIT_H_

#include "reduce/Reduce.h"
#include "nucleus/Nucleus.h"
#include "flow/Flow.h"
#include "util/SourceLocator.h"
#include "util/UtMap.h"

class MsgContext;
class Stats;
class IODBNucleus;
class ReachableAliases;
class STAliasedLeafNode;
class ArgProc;

// please do not use "using namespace std"

/*!
  \file
  Declaration of class used to generate tristate initialization functions.
*/

//! RETriInit
/*!
 * Class which generates tristate initialization functions for a design.
 *
 * This should be invoked after alias creation occurs, to get the initialization
 * functions placed at the correct hierarchy point.
 */
class RETriInit
{
public:
  //! constructor
  /*!
    \param design The design to walk
    \param symtab The symbol table to use to look up elaboration
    \param flow_factory The factory used to manage unelaborated flow nodes
    \param flow_elab_factory The factory used to manage elaborated flow nodes
    \param netref_factory Factory to manage net refs
    \param msg_context Used to emit messages.
    \param iodb IO Database
    \param init_mode Net initialization mode
    \param verbose If true, will print out initialization generation as it occurs.
   */
  RETriInit(NUDesign *design,
	    STSymbolTable *symtab,
            AtomicCache* atomicCache,
	    FLNodeFactory *flow_factory,
	    FLNodeElabFactory *flow_elab_factory,
	    NUNetRefFactory *netref_factory,
	    MsgContext *msg_context,
	    IODBNucleus *iodb,
            ArgProc * args,
            SourceLocatorFactory* sourceLocatorFactory,
            ReachableAliases *reachables,
	    TristateModeT init_mode,
	    bool verbose=false) :
    mDesign(design),
    mSymtab(symtab),
    mAtomicCache(atomicCache),
    mFlowFactory(flow_factory),
    mFlowElabFactory(flow_elab_factory),
    mNetRefFactory(netref_factory),
    mMsgContext(msg_context),
    mIODB(iodb),
    mArgs(args),
    mSourceLocatorFactory(sourceLocatorFactory),
    mReachableAliases(reachables),
    mInitMode(init_mode),
    mVerbose(verbose)
  {}

  //! Design walk to create initialization functions.
  /*!
   * Walk the design, collecting nets which are tristatable,
   * and the fanout for those nets.
   *
   * Then, create initialization functions based on the type of net, and make
   * the initialization fanout to everything the net fans-out to.
   *
   * And finally after the new nucleus has been created, the class
   * will destroy and re-create flow (unelaborated and elaborated).
   */
  void triInitDesign(Stats *stats, bool phase_stats);

  //! destructor
  ~RETriInit();

private:
  //! Hide copy and assign constructors.
  RETriInit(const RETriInit&);
  RETriInit& operator=(const RETriInit&);

  enum TriInitValue {
    eTriInitUnknown,            //!< Unknown initialization.
    eTriInitZero,               //!< Pulldown initialization.
    eTriInitOne,                //!< Pullup initialization.
    eTriInitTrireg,             //!< Trireg initialization.
    eTriInitConflict            //!< Conflicting initialization requirements; code initializer with hierarchical reference.
  };

  //! Map from unelaborated net to its known initial value.
  /*!
   * If different elaborations of this net require different initial
   * values, a value of eTriInitConflict will be used.
   */
  typedef UtMap<NUNet*,TriInitValue> InitialDriverValueMap;

  //! Map from elaborated net to its known initial value.
  typedef UtMap<NUNetElab*,TriInitValue> InitialDriverElabValueMap;

  // TBD:  This FlowSense (etc) stuff is replicated across REAlias, REResolution,
  // and here, it should be cleaned up.  Maybe have a base class for these
  // analysis passes?

  //! Keep track of a flow-node and how we got there
  typedef std::pair< FLNodeElab*, Sensitivity > FlowSense;

  //! Set of flow-nodes and how we got there
  typedef UtSet<FlowSense> FlowSenseSet;

  //! Convenience typedef for mapping elaborated net to set of elaborated flow.
  typedef UtMap< NUNetElab*, FlowSenseSet* > NetNodeElabSetMap;

  //! Way of storing the location for the triinit for a given net-elab.
  typedef UtMap<NUNetElab*,STAliasedLeafNode*> NetElabNameMap;

  //! Convenience typedef for iterating over a mapping of elaborated net to set
  //! of elaborated flow.
  typedef NetNodeElabSetMap::iterator NetNodeElabSetMapIter;

  //! Update all unelaborated aliases of _net_elab_ with the needed initialization type.
  /*!
   * If any of these unelaborated aliases have already been marked
   * with a conflicting initialization type, mark that alias as
   * eTriInitConflict. A conflict means that the initializer will be
   * coded with a hierarchical reference.
   */
  void updateAliasesWithValue(NUNetElab * net_elab, TriInitValue tri_init_value);

  //! Store the fact that this net was multiply-driven and fans out to the
  //! given flow node.
  void rememberFanout(NUNetElab *net, FLNodeElab *fanout, Sensitivity);

  //! Walk the design finding tristatable nets.
  void walkDesign();

  /*! 
   * Mark all unelaborated aliases of net-elabs not requiring tristate
   * initialization as having tristate conflict. If a shared alias
   * between a tristate net-elab and a non-tristate net-elab is chosen
   * as a tristate initialization location, the conflict marking
   * forces the initializer to use a hierarchical reference.
   */
  void markNonTriInit();

  //! Consistency check that no non-triinit net has an initial driver in its alias ring.
  /*!
   * The only allowable initial drivers are hier-ref bound nodes,
   * which we use in conflict situations. The presence of a hier-ref
   * bound does not imply an initial driver in all elaborations -- it
   * implies an initial driver in _some_ elaboration.
   */
  void checkNonTriInit();

  //! Walk all the tristatable nets we found, add an initial driver for the nets.
  /*!
   * Go through all nets which need initial drivers and make them, populate
   * the mInitDriverMap.
   */
  void makeAllInit();

  //! Determine which alias is an appropriate location for the initial driver.
  /*!
   * When creating initial drivers, we need to place them in a
   * location where the unelaborated connectivity will keep the driver
   * live. We cannot, for example, place the driver at a dead reader.
   */
  STAliasedLeafNode * determineInitLocation(NUNetElab * net_elab);

  //! Determine the type of initialization required for this net elab.
  TriInitValue determineInitValue(NUNetElab * net_elab);

  /*! 
   * Create a hierarchical reference to use instead of an unelaborated
   * alias with conflicting initialization requirements.
   */
  NUNet * createHierRefForInit(NUNetElab * net_elab, STAliasedLeafNode * alias_for_init);

  //! If multiple aliases for a net elab have initial drivers, print them.
  void dumpMultipleInitialDrivers(NUNetElab * net_elab, 
                                  FLNodeElabVector * initial_drivers);

  //! Make an initial, unelaborated, driver for the net.
  /*!
   * May return 0 if it is determined that an initial driver is not needed.
   */
  void makeInit(NUNet * alias_for_init, 
                TriInitValue tri_init_value);

  //! Make an initial, unelaborated, constant-valued driver for the net.
  NUUseDefNode* makeInitConstant(NUModule *module,
				 NUNet *net,
				 const UtString &val,
				 const SourceLocator &loc);

  //! Make an initial, unelaborated, 1-valued driver for the net.
  NUUseDefNode* makeInit1(NUModule *module,
			  NUNet *net,
			  const SourceLocator &loc);

  //! Make an initial, unelaborated, 0-valued driver for the net.
  NUUseDefNode* makeInit0(NUModule *module,
			  NUNet *net,
			  const SourceLocator &loc);

  //! Make an initial, unelaborated, random-valued driver for the net.
  NUUseDefNode* makeInitR(NUModule *module,
			  NUNet *net,
			  const SourceLocator &loc);

  //! Make an initial, unelaborated, trireg driver for the net.
  NUUseDefNode* makeInitTrireg(NUModule *module,
			       NUNet *net,
			       const SourceLocator &loc);

  //! Is this net attached to a pullup or pulldown?
  bool isAttachedToPull(NUNetElab* netElab);

  //! Does this net get read or written?
  bool isReadOrWritten(NUNet*);

  //! Is this net effectively depositable or a primary input?
  bool isDepositOrInput(NUNetElab* driver_net);

  //! Check that all the initial drivers created are consistent
  void checkInitialDriverConsistency();

  //! Check that all pull drivers for a single net elab have the same value.
  bool checkSingleInitialDriverConsistency(const FLNodeElabVector& drivers);

  //! The design
  NUDesign *mDesign;

  //! The symbol table to lookup elaboration with
  STSymbolTable *mSymtab;

  //! String cache
  AtomicCache* mAtomicCache;

  //! Unelaborated flow node manager.
  FLNodeFactory *mFlowFactory;

  //! Elaboarted flow node manager.
  FLNodeElabFactory *mFlowElabFactory;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! Where to emit messages
  MsgContext *mMsgContext;

  //! IO database
  IODBNucleus *mIODB;

  //! Argument processor
  ArgProc * mArgs;

  //! Source locator factory for elaboration
  SourceLocatorFactory* mSourceLocatorFactory;

  //! Set of reachable aliases
  ReachableAliases *mReachableAliases;

  //! Set of net elabs which have been shown to not require tristate initialization.
  NUNetElabSet mNonTriInit;

  //! Map of nets which are going to have initial drivers created to their fanouts.
  NetNodeElabSetMap mFanouts;

  //! Abstraction for the net to init driver map
  typedef UtMap<NUNet*, NUUseDefNode*> InitDriverMap;

  //! Map of nets to their newly-created initial drivers
  InitDriverMap mInitDriverMap;

  //! Map of unelaborated nets to known initialization values.
  InitialDriverValueMap mInitialDriverValue;

  //! Map of elaborated nets to known initialization values.
  InitialDriverElabValueMap mInitialDriverElabValue;

  //! Net initialization mode
  TristateModeT mInitMode;

  //! Flag to print out resolution function generation as it occurs
  bool mVerbose;
};

#endif
