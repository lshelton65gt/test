// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef SANITY_ELAB_H_
#define SANITY_ELAB_H_

#include "flow/Flow.h"

class NUNetRefFactory;
class MsgContext;
class SanityElabStatus;
class SanityElabNetStatus;
class FLElabUseCache;

/*!
  \file
  Declaration of elaborated sanity package.
 */

class SanityElab 
{
public:
  SanityElab(MsgContext        * msg_context,
             STSymbolTable     * symbol_table,
             NUNetRefFactory   * net_ref_factory,
             FLNodeFactory     * dfg_factory,
             FLNodeElabFactory * dfg_elab_factory,
             bool checkFaninOverlap, // requires accurate UD
             bool verbose);

  ~SanityElab();

  void design(NUDesign * design);

private:
  //! Hide copy and assign constructors.
  SanityElab(const SanityElab&);
  SanityElab& operator=(const SanityElab&);

  //! Sanity check the flow factory.
  void factory();

  //! Sanity check of all elaborated nets.
  void nets();

  bool flow(SanityElabStatus * status, FLNodeElab * flnode_elab);

  //! Sanity check for one elaborated net.
  bool net(SanityElabNetStatus * status, NUNetElab * net_elab);

  bool allocFlowCheck(FLNode *flnode);

  bool allocFlowCheck(FLNodeElab *flnode);

  bool defNetHierarchyCheck(SanityElabStatus * status, FLNodeElab *flnode);

  bool faninOverlapCheck(FLNodeElab *flnode, FLNodeElab *fanin);

  //! Messages
  MsgContext * mMsgContext;

  //! Symbol table
  STSymbolTable * mSymbolTable;

  //! NetRef Factory
  NUNetRefFactory   * mNetRefFactory;

  //! DFG Factory
  FLNodeFactory     * mFlowFactory;

  //! DFG Elab Factory
  FLNodeElabFactory * mFlowElabFactory;

  //! Free set
  MemoryPoolPtrSet mFreeSet;

  //! Free Elab set
  MemoryPoolPtrSet mFreeElabSet;

  //! Number of errors found.
  UInt32 mNumErrors;

  //! Cache to improve overlap checks
  FLElabUseCache* mLevelCache;
  FLElabUseCache* mEdgeCache;

  //! Check that all fanins' defs overlap with the flow's uses?
  bool mCheckOverlap;

  //! Verbose during sanity check?
  bool mVerbose;
};

#endif
