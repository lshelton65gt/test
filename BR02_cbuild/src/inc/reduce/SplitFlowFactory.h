// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef SPLIT_FLOW_FACTORY_H_
#define SPLIT_FLOW_FACTORY_H_

#include "flow/FLFactory.h"

/*!
  \file
  Declaration of cache-aware factory package.
 */

//! Cache-aware unelaborated flow factory.
/*!
 *
 * TBD: Really, we want this class to support the same interface as
 * the FLNodeFactory, but not contain all of the data members. For
 * now, just subclass FLNodeFactory. In the fullness of time, we
 * should separate the FLNodeFactory interface and create a new class
 * for the actual flow factory (with data members in all their glory).
 *
 * The problem by directly subclassing FLNodeFactory is that we are
 * not explicitly protected from interface changes. If a new interface
 * method is added to FLNodeFactory without updating SplitFlowFactory,
 * that call will be made on the data members of the parent class
 * (which is not how SplitFlowFactory is intended).
 *
 * The proposal:
 *
 * FLNodeFactory - abstract base class. All factory consumers use
 *   |             objects of this type.
 *   +- FLNodeFactoryImplementation - Implementation of the
 *   |             general-purpose flow factory.
 *   +- SplitFlowFactory - Implementation of the splitting factory.
 *                 Supporting the same interface as the above
 *                 implementation; handing off all requests as
 *                 necessary.
 *
 * With this separation, changes to FLNodeFactory must always be
 * reflected in both the general implementation and SplitFlowFactory.
 * Interface changes cannot only be made to the general implementation
 * because factory consumers will always interact with the abstract
 * base class FLNodeFactory.
 */
class SplitFlowFactory : public FLNodeFactory
{
public:
  //! Constructor.
  /*!
   * \param flow_factory The actual, unelaborated flow factory.
   *     Creation requests fall back to this factory if there is not
   *     an equivalent node in the cache.
   *
   * \param flow_lookup The flow cache. A map from (net-ref,node)
   *     pairs to unelaborated flow.
   */
  SplitFlowFactory(FLNodeFactory * flow_factory,
		   NUNetNodeToFlowMap & flow_lookup) :
    mFlowFactory(flow_factory),
    mFlowLookup(flow_lookup)
  {}

  //! Destructor.
  ~SplitFlowFactory(){}

  //! Destroy the provided flow node.
  /*!
   * This request is sent directly to the underlying factory.
   */
  void destroy(FLNode *flnode) { mFlowFactory->destroy(flnode); }

  //! The beginning of active flow.
  /*!
   * This request is sent directly to the underlying factory.
   */
  iterator begin() { return mFlowFactory->begin(); }

  //! The end of active flow.
  /*!
   * This request is sent directly to the underlying factory.
   */
  iterator end() { return mFlowFactory->end(); }

  //! Create an unelaborated flow based on the provided net-ref and node.
  /*!
   * If the pair (net-ref,node) is cached, return the associated flow.
   * If the pair is not cached, send the creation request to the
   * underlying flow factory.
   */
  virtual FLNode * create (const NUNetRefHdl &net_ref, NUUseDefNode *node) {
    NUNetNode net_node(net_ref,node);
    NUNetNodeToFlowMap::iterator location = mFlowLookup.find(net_node);
    if ( location != mFlowLookup.end() ) {
      return location->second;
    } else {
      return mFlowFactory->create(net_ref,node);
    }
  }

  //! Create an unelaborated bound node based on the provided net-ref.
  /*!
   * If the pair (net-ref,NULL) is cached, return the associated flow.
   * If the pair is not cached, send the creation request to the
   * underlying flow factory.
   *
   * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   * NOTE: This ::createBound method isn't completely correct. With
   * the addition of flow-type information, we should have
   * incorporated the type into our caching strategy. The code below
   * will return _any_ existing bound, even if it is of a different
   * type.
   *
   * The NUNetNode tuple should be changed to a triple: 
   *    (net-ref, node, flow-type)
   *
   * This probably doesn't matter much in the current application of
   * this class, but we can't be sure.
   * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   */
  virtual FLNodeBound * createBound(const NUNetRefHdl &net_ref, FLTypeT type) {
    NUNetNode net_node(net_ref,0);
    NUNetNodeToFlowMap::iterator location = mFlowLookup.find(net_node);
    if ( location != mFlowLookup.end() ) {
      FLNode * created = location->second;
      FLNodeBound * bound_created = dynamic_cast<FLNodeBound*>(created);
      NU_ASSERT(bound_created,net_ref);
      return bound_created;
    } else {
      return mFlowFactory->createBound(net_ref, type);
    }
  }
  
private:
  //! The underlying flow factory. 
  /*!
   * Any explicit allocation/deallocation requests are passed here.
   */
  FLNodeFactory * mFlowFactory;
  //! The flow cache.
  /*!
   * Any creation requests will first check here for a pre-existing
   * element before falling back to the underlying flow factory.
   */
  NUNetNodeToFlowMap & mFlowLookup;
};

//! Cache-aware unelaborated flow factory.
/*!
 * TBD: Separate FLNodeElabFactory interface from implementation (see
 * SplitFlowFactory comment).
 */
class SplitFlowElabFactory : public FLNodeElabFactory
{
#if !pfGCC_2
  using FLNodeElabFactory::createCycle;
#endif

public:
  //! Constructor.
  /*!
   * \param flow_factory The actual, elaborated flow factory. Creation
   *     requests fall back to this factory if there is not an
   *     equivalent node in the cache.
   *
   * \param flow_lookup The flow cache. A map from
   *     (flow,net-elab,hierarchy) triples to elaborated flow.
   */
  SplitFlowElabFactory(FLNodeElabFactory * flow_factory,
		       FlowNetHierToFlowElabMap & flow_lookup) :
    mFlowFactory(flow_factory),
    mFlowLookup(flow_lookup)
  {}

  //! Destructor.
  ~SplitFlowElabFactory(){}


  //! Destroy the provided elaborated flow node.
  /*!
   * This request is sent directly to the underlying factory.
   */
  void destroy(FLNodeElab *flnode) { mFlowFactory->destroy(flnode); }
  //  iterator begin() { return mFlowFactory->begin(); }
  //  iterator end() { return mFlowFactory->end(); }

  //! Create an elaborated flow based on the provided flow,net-elab and hierarchy.
  /*!
   * If the triple (flow,net-elab,hier) is cached, return the
   * associated elaborated flow. If the pair is not cached, send the
   * creation request to the underlying elaborated flow factory.
   */
  virtual FLNodeElab *create(FLNode *flnode,
			     NUNetElab *net,
			     STBranchNode *hier) {
    FlowNetHier flow_net_hier(flnode,net,hier);
    FlowNetHierToFlowElabMap::iterator location = mFlowLookup.find(flow_net_hier);
    if ( location != mFlowLookup.end() ) {
      return location->second;
    } else {
      return mFlowFactory->create(flnode,net,hier);
    }
  }

  //! Create a cycle; this is disallowed from the cached factory.
  virtual FLNodeElabCycle *createCycle(NUUseDefNode* /*node*/) {
    // This should never happen under block splitting.
    INFO_ASSERT(0,"Unexpected cycle creation.");
    return 0;
  }

  //! Create an elaborated bound node based on the provided flow, net-elab and hierarchy
  /*!
   * If the triple (flow,net-elab,hier) is cached, return the
   * associated elaborated flow. If the pair is not cached, send the
   * creation request to the underlying elaborated flow factory.
   */
  virtual FLNodeBoundElab *createBound(FLNode *flnode,
				       NUNetElab *net,
				       STBranchNode *hier) {
    FlowNetHier flow_net_hier(flnode,net,hier);
    FlowNetHierToFlowElabMap::iterator location = mFlowLookup.find(flow_net_hier);
    if ( location != mFlowLookup.end() ) {
      FLNodeElab * created = location->second;
      FLNodeBoundElab * bound_created = dynamic_cast<FLNodeBoundElab*>(created);
      FLN_ASSERT(bound_created,flnode);
      return bound_created;
    } else {
      // This should never happen under block splitting.
      return mFlowFactory->createBound(flnode,net,hier);
      //FLN_ASSERT(0, flnode);
      //return 0;
    }
  }

private:
  //! The underlying elaborated flow factory. 
  /*!
   * Any explicit allocation/deallocation requests are passed here.
   */
  FLNodeElabFactory * mFlowFactory;
  //! The elaborated flow cache.
  /*!
   * Any creation requests will first check here for a pre-existing
   * element before falling back to the underlying elaborated flow
   * factory.
   */
  FlowNetHierToFlowElabMap & mFlowLookup;
};

#endif
