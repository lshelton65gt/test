// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implements a transformation to reduce the number of clocks in the design.
*/

#ifndef _CLOCKGATE_H_
#define _CLOCKGATE_H_

class NUNetRefFactory;
class FLNodeFactory;
class BDDContext;
class AtomicCache;
class MsgContext;
class IODBNucleus;
class NUModule;
class NUNet;
class ExprResynth;
class ArgProc;
class REClockExclusivity;
class Fold;

#include "util/CarbonAssert.h"
#include "util/CarbonTypes.h"
#include "util/UtMap.h"

#include "nucleus/Nucleus.h"

#include "flow/Flow.h"

//! Class to maintain statistics for the clock gate transformation
class REClockGateStats
{
public:
  //! constructor
  REClockGateStats() :
    mNumFlops(0), mNumTransformedFlops(0), mNumInvalidClk(0), mNumNonExcl(0),
    mNumConflEnables(0), mNumInvalidClkEdges(0), mNumInvalidEnables(0),
    mNumInvalidRootClks(0)
  {}

  //! Increment the number of flops found
  void incrFlops(void) { ++mNumFlops; }

  //! Increment the number of transformed flops
  void incrTransformedFlops(void) { ++mNumTransformedFlops; }

  //! Increment the number of invalid gated clocks
  void incrInvalidClk(void) { ++mNumInvalidClk; }

  //! Increment the number of non exclusive clocks
  void incrNonExcl(void) { ++mNumNonExcl; }

  //! Increment the number of conflicting enables found
  void incrConflEnables(void) { ++mNumConflEnables; }

  //! Increment the number of invalid clock edges
  void incrInvalidClkEdge(void) { ++mNumInvalidClkEdges; }

  //! Increment the number of invalid enables
  void incrInvalidEnables(void) { ++mNumInvalidEnables; }

  //! Increment the number of invalid root clocks
  void incrInvalidRootClk(void) { ++mNumInvalidRootClks; }

  //! Print statistics
  void print(void) const;

private:
  int mNumFlops;
  int mNumTransformedFlops;
  int mNumInvalidClk;
  int mNumNonExcl;
  int mNumConflEnables;
  int mNumInvalidClkEdges;
  int mNumInvalidEnables;
  int mNumInvalidRootClks;
}; // class REClockGateStats

//! Class to find and transform gated clocks into flop enables
/*! Gated clocks can lead to performance problems for three reasons:
 *
 * 1. The schedule overhead is higher with more clocks.
 *
 * 2. There is a potential for DCL cycles because the gate condition
 *    is often computed with a latch.
 *
 * 3. It reduces the amount of block merging because logic ends up in
 *    buckets with different clocks.
 *
 * An example transformation is as follows:
 *
 * \verbatim
 *      always @ (clk or gate0)
 *        if (~clk)
 *          gate1 <= gate0;
 *
 *      assign gclk = clk & gate1;
 *
 *      always @ (posedge gclk)         ==>     always @ (posedge clk)
 *        q <= d;                                 if (gate1)
 *                                                  q <= d;
 * \endverbatim
 *
 * See the document docs/functional-specs/clkgate.txt for more details.
 */
class REClockGate
{
public:
  //! constructor
  REClockGate(FLNodeFactory* flowFactory, NUNetRefFactory* netRefFactory,
              AtomicCache* strCache, MsgContext* msgContext,
              IODBNucleus* iodb, ArgProc* args, UD* ud, 
              REClockGateStats* stats, bool verbose);

  //! destructor
  ~REClockGate();

  //! Process a module
  void module(NUModule* mod);

private:
  //! Hide copy constructor
  REClockGate(const REClockGate&);

  //! Hide assignment constructor
  REClockGate& operator=(const REClockGate&);

  //! Look for candidates and convert gated clock flops
  void transformFlops(const FLNodeVector& flops);

  //! Test if a clock has gating logic and get it
  bool isValidTransform(FLNode* flow, ClockEdge* newEdge, NUNetRefHdl* newClkRef,
                        NUExprVector* enableExprs);

  //! Invert a clock edge
  ClockEdge invert(ClockEdge edge);

  //! Modify a flop by bringing in the enable.
  void transformFlop(FLNode* flow, const NUNetRefHdl& newClkRef, ClockEdge newEdge,
                     const NUExprVector& enableExprs);

  //! Function to possibly print information about a transformed flop
  void printTransformedFlop(FLNode* flow, const NUNetRefHdl& clkRef, NUNet* newClk,
                            const NUExprVector& enExprs) const;

  //! Function to possibly print information about non exclusive enables
  void printNonExcl(FLNode* flopFlow, const NUExpr* clkExpr, FLNode* enFlow,
                    const NUExpr* enableClkExpr) const;

  //! Function to possibly print information about an invalid flop clock expr
  void printInvalidClock(FLNode* flow, const NUExpr* flopClkExpr,
                         const char* reason) const;

  //! Function to print common part of failed transform message
  void printFailedTransform(FLNode* flopFlow) const;

  //! Utility function to simplify an clock/enable expression
  NUExpr* replaceClockWithConst(const NUExpr* expr, const NUNetRefHdl& clkRef,
                                UInt64 clkVal);

  //! Utility function to create an expression from a clk net ref
  NUExpr* createClkExpr(const NUNetRefHdl& clkRef, const SourceLocator& loc);

  //! Utility function to find the edge for a transformed flop
  bool findNewClockEdge(const NUExpr* clkExpr, const NUNetRefHdl& clkRef,
                        ClockEdge* edge);

  //! Utility function to create the new enable condition for the ungated flop
  void createFlopEnableExpr(const NUExpr* flopClkExpr,
                            const NUNetRefHdl& clkRef, ClockEdge edge,
                            const FLNodeVector& enables,
                            NUExprVector* enableExprs);

  //! Abstraction to accumualte the enable expressions by clock tree depth
  typedef UtMap<int, NUCExprVector> EnableExprMap;

  //! Function to create a vector of enable expressions
  void createEnableExprs(const NUExpr* clkExpr, const NUNetRefHdl& clkRef,
                         UInt64 clkVal, NUExprVector* enExprs);

  //! Function to sort a set of enable expressions by depth
  void createEnableExprsRecurse(const NUExpr* expr, EnableExprMap* enableExprMap,
                                int depth);

  //! Utility function to create nested if statements from enables
  void createIfStatements(const NUExprVector& enExprs, const NUStmtList& stmts,
                          const SourceLocator& loc, NUStmtList* blockStmts);

  //! Class to find flops and test exclusivity
  REClockExclusivity* mClockExclusivity;

  //! Net Ref factory for analyzing defs and uses
  NUNetRefFactory* mNetRefFactory;

  //! Fold class to simplfy newly created expressions
  Fold* mFold;

  //! UD class to recompute UD of modified blocks
  UD* mUD;

  //! Keep track of statistics
  REClockGateStats* mStats;

  //! If set, information should be printed about converted latches
  bool mVerbose;
}; // class REClockGate

#endif // _CLOCKGATE_H_
