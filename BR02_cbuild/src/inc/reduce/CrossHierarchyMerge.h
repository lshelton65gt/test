/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef CROSSHIERARCHYMERGE_H_
#define CROSSHIERARCHYMERGE_H_

#include "nucleus/Nucleus.h"

class STSymbolTable;

//! Determine cross-hierarchy merge opportunities
/*!
 * This class analyzes the design and tries to determine
 * cross-hierarchy merge opportunities.  The mergeable
 * nodes are printed.
 *
 * Currently, it looks for merges of the sort:
 * . combinational -> combinational or
 *   combinational -> sequential
 * . blocks must be in immediate parent/child hierarchy.
 *   no sibling hierarchy is allowed, or grandparent
 *   hierarchy.
 * . for block A to be mergeable into block B, it must
 *   be mergeable in all elaborations.
 *
 */
class CrossHierarchyMerge
{
public:
  //! constructor
  CrossHierarchyMerge();

  //! destructor
  ~CrossHierarchyMerge();

  //! Analyze the design
  void design(NUDesign *design, STSymbolTable *symtab);

private:
};

#endif
