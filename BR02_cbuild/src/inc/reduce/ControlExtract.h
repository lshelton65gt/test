// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CONTROL_EXTRACT_H_
#define CONTROL_EXTRACT_H_

#include "nucleus/NUDesignWalker.h"

/*!
  \file
  Declaration of control extraction package.
 */

class AtomicCache;
class NUNetRefFactory;
class MsgContext;
class UD;
class IODBNucleus;
class ArgProc;

class RedundantAssigns;
class ControlExtractStatistics;

//! Combine sequential statements having identical control structure with a single statement.
/*!
 * I. Combination of if-then-else stmts and ternary-friendly assignments
 *
 * Traverse a module searching for sequential ternary-friendly
 * assignments and if-then-else statements with a shared select.
 * Rewrite these statements using a single if-then-else statement.
 * Ternary assignments are decomposed into then/else components and
 * included in this combined if-then-else statement.
 *
 * Note: Ternary-friendly assignments are assignments with a ternary
 * expression (the ?: operator) or some easily transformed expression
 * form for the RHS.
 *
 * Some example ternary-friendly statements:
 *
 * out = sel ? a & b;
 * out = en & data;   <=> out = en ? data : 1'b0;
 * out = hold | data; <=> out = hold ? 1'b1 : data;
 *
 * Statements with a top-level ternary assignment are transformed into
 * the if-then-else form even when it is not combined with another
 * statement. This allows more downstream code-motion opportunities.
 *
 * Statements with either the & or | form are transformed into
 * if-then-else only when there are other statements with matching
 * control structure.
 *
 * Examples of control extraction:
 *
 * ORIGINAL: 
 *   always @(..) begin
 *     x = sel ? a : b;
 *     if (sel) 
 *       y = c;
 *     else
 *       y = d;
 *     z = sel ? e : f;
 *   end
 *
 * OPTIMIZED:
 *   always @(..) begin
 *     if (sel) begin
 *       x = a;
 *       y = c;
 *       z = e;
 *     end else begin
 *       x = b;
 *       y = d;
 *       z = f;
 *     end
 *   end
 * 
 * Statements contained within the generated if-then-else statement
 * will be processed recursively. Nested ternary expressions may
 * become top-level expressions.
 *
 * Example:
 * ORIGINAL: 
 *   always @(..) begin
 *     x = sel1 ? sel2 ? a : b : c;
 *     z = sel1 ? sel2 ? d : e : f;
 *   end
 *
 * STEP 1:
 *   always @(..) begin
 *     if (sel1) begin
 *       x = sel2 ? a : b;
 *       y = sel2 ? d : e;
 *     end else begin
 *       x = c;
 *       y = f;
 *     end
 *  end
 *
 * OPTIMIZED:
 *   always @(..) begin
 *     if (sel1) begin
 *       if (sel2) begin
 *         x = a;
 *         y = d;
 *       end else begin
 *         x = b;
 *         y = e;
 *       end
 *     end else begin
 *       x = c;
 *       y = f;
 *     end
 *  end
 *
 * Limitations:
 *
 * 1. We currently do not search the RHS of an assignment for a buried
 *    ternary expression.
 *
 * 2. Selects must be isomorphic to be considered for inclusion. This
 *    means we miss opportunities with simple transformations such as:
 *      x = sel ? a : b;
 *      z = (sel|0) ? e : f;
 *
 *    We also do not handle inversions of the select:
 *      y = (!sel) ? c : d;
 *
 * II. Combination of case statements
 *
 * As the module is traversed, search for sequential case statements
 * sharing all control elements. The selector and all case conditions
 * must match for two case statements to be considered.
 *
 * Example:
 * ORIGINAL:
 *   always @(sel1 or sel2 or sel3) begin
 *     case({sel1,sel2})
 *       {1'b0,sel3}  : c2 = 2'b11;
 *       {sel3,~sel3} : c2 = 2'b01;
 *       default      : c2 = 2'b00;
 *     endcase
 *     case({sel1,sel2})
 *       {1'b0,sel3}  : c3 = 2'b10;
 *       {sel3,~sel3} : c3 = 2'b01;
 *       default      : c3 = 2'b11;
 *     endcase
 *   end
 *
 * OPTIMIZED:
 *   always @(sel1 or sel2 or sel3) begin
 *     case({sel1,sel2})
 *       {1'b0,sel3}  : begin
 *         c2 = 2'b11;
 *         c3 = 2'b10;
 *       end
 *       {sel3,~sel3} : begin
 *         c2 = 2'b01;
 *         c3 = 2'b01;
 *       end
 *       default      : begin
 *         c2 = 2'b00;
 *         c3 = 2'b11;
 *       end
 *     endcase
 *   end
 *
 * Limitations:
 *
 * 1. Selects and controls must be isomorphic to be considered for
 *    inclusion. As with if/ternary statements, this means we will
 *    miss opportunities with simple transformation.
 *
 */
class ControlExtract
{
public:
  //! Constructor
  ControlExtract(AtomicCache * str_cache,
		 NUNetRefFactory * netref_factory,
		 MsgContext * msg_context, 
		 IODBNucleus * iodb,
                 ArgProc* args,
		 bool verbose,
		 ControlExtractStatistics *stats);

  //! Destructor
  ~ControlExtract();

  //! Walk a module and its components.
  /*!
   * Use a NUDesignWalker to traverse all constructs within a module.
   * Constructs which contain sequential statements (named blocks, if
   * stmts, case stmts, for loops, etc) are searched for sequential
   * statements which can be optimized by combining if-then-else
   * statements, ternary assignstatements, and case statements.
   *
   * \param one    Traverse this module.
   * \returns true If any rewrites were successful.
   */
  bool module(NUModule * one);

  //! Walk a single always block.
  bool always(NUAlwaysBlock * alwaysBlock);

  //! Walk a list of merged use-def nodes.
  /*!
   * Use a NUDesignWalker to traverse all constructs within the
   * provided set of always blocks. Search for and optimize sequential
   * statements with a shared control.
   *
   * \param one    The module containing all the NUUseDefNode 
   *               in 'blocks'.
   * \param blocks A vector of NUUseDefNode generated by block
   *               merging; these must all be always blocks.
   * \returns true If any rewrites were successful.
   */
  bool mergedBlocks(NUModule * one, NUUseDefVector & blocks);

private:
  AtomicCache     * mStrCache;
  NUNetRefFactory * mNetRefFactory;
  MsgContext      * mMsgContext;

  //! Object for re-computing the use-def information for any stmt.
  UD        * mUD;

  //! Statistics to track magnitude of success.
  ControlExtractStatistics * mStatistics;

  //! Be verbose as processing occurs?
  bool mVerbose;
};


//! Perform the search/replacement.
/*!
 * Callback for NUDesignWalker.
 *
 * Control the traversal over Nucleus constructs as we search for
 * optimization opportunities.
 */
class ControlExtractCallback : public NUDesignCallback
{
public:
  //! Constructor
  /*!
   * \param remove_redundancies

    \param convert_boolean_to_ternary If true, attempt to convert the
        following boolean expression forms:
    \code
          out = (en & data);  <=> out = en ? data : 1'b0;
          out = (nen | data); <=> out = nen ? 1'b1 : data;
    \endcode
        This transformation occurs only if multiple statements share a
        control expression.
   */
  ControlExtractCallback(NUNetRefFactory * netref_factory, 
			 UD * ud,
			 ControlExtractStatistics * stats,
                         bool remove_redundancies,
                         bool convert_boolean_to_ternary);

  //! Destructor
  ~ControlExtractCallback();

  //! By default, skip over all constructs.
  Status operator()(Phase /*phase*/, NUBase * /*node*/) { return eSkip; }

  //! Walk into a module (not module instances, though)
  Status operator()(Phase /*phase*/, NUModule * /*node*/) { return eNormal; }

  //! Walk into always/initial blocks.
  Status operator()(Phase /*phase*/, NUStructuredProc * /*node*/) { return eNormal; }

  //! Walk into tasks/functions.
  Status operator()(Phase /*phase*/, NUTF * /*node*/) { return eNormal; }

  //! Walk into and locally process the child statements of named blocks.
  Status operator()(Phase phase, NUBlock *node);

  //! Walk into and locally process the child statements of case statements.
  Status operator()(Phase phase, NUCase *node);

  //! Walk into and locally process the child statements of if() statements.
  Status operator()(Phase phase, NUIf *node);

  //! Walk into and locally process the child statements of for() statements.
  Status operator()(Phase phase, NUFor *node);

  //! Did we find any assignments and convert them into if-then-else?
  bool didWork() const { return mDidWork; }

  //! Process a list of statements.
  /*!
   * From a series a statements, search for if() and case()
   * combination opportunities. Unmodified statements and generated
   * statements are added to the 'replacements' list. Statements used
   * to generate these statements are deleted.
   *
   * Caller *must* use 'replacements' to update a statement body if
   * this method returns true, as statements from the original list
   * have been deleted.
   *
   * UD is updated on the generated statements; the caller does
   * not need to recalculate UD from a parent.
   *
   * \param loop Search this loop for optimization opportunities.
   * \param replacements Results of operation.
   * \return true if any replacements were made.
   *
   * \sa searchForAdjacentIfStmts, searchForAdjacentCaseStmts
   */
  bool stmts(NUStmtLoop loop, NUStmtList & replacements);
private:

  //! Look for if-combination opportunities within a list of statements.
  /*!
   * From a series a statements, find adjacent conditionals
   * (if/ternary) and combine those with shared select. Unmodified
   * statements and the generated if() statements are added to the
   * 'replacements' list. Statements used to generate these if()
   * statements are deleted.
   *
   * Caller *must* use 'replacements' to update a statement body if
   * this method returns true, as statements from the original list
   * have been deleted.
   *
   * UD is updated on the generated if() statement; the caller does
   * not need to recalculate UD from a parent.
   *
   * Updates 'mDidWork' if any modifications were performed.
   *
   * \param loop Search this loop for if() candidates.
   * \param replacements Results of operation.
   * \return true if any replacements were made.
   *
   * \sa stmts
   */
  bool searchForAdjacentIfStmts(NUStmtLoop loop, NUStmtList & replacements);

  //! Look for case-combination opportunities within a list of statements.
  /*!
   * From a series a statements, find adjacent case statements and
   * combine those with identical control structure. Unmodified
   * statements and the generated case statements are added to the
   * 'replacements' list. Statements used to generate these case
   * statements are deleted.
   *
   * Caller *must* use 'replacements' to update a statement body if
   * this method returns true, as statements from the original list
   * have been deleted.
   *
   * UD is updated on the generated case() statement; the caller does
   * not need to recalculate UD from a parent.
   *
   * Updates 'mDidWork' if any modifications were performed.
   *
   * \param loop Search this loop for case() candidates.
   * \param replacements Results of operation.
   * \return true if any replacements were made.
   *
   * \sa stmts
   */
  bool searchForAdjacentCaseStmts(NUStmtLoop loop, NUStmtList & replacements);

  //! Get the select from an if stmt or assign with ternary-friendly form.
  /*!
   * If the select uses nets defined by this statement, the select is
   * considered invalid and NULL is returned.
   *
   * We are trying to avoid combining statements like:
   *   always @(..) begin
   *     sel = sel ? a : b;
   *     out = sel ? c : d;
   *   end
   * These two assignments use the same select but the definition of
   * that sel is different for the two uses.
   *
   * Examples of supported statements and the returned select:
   *
   * STATEMENT               SELECT
   * if ( sel ) out = a;     sel
   * else       out = b;
   *
   * out = sel ? a & b;      sel
   *
   * out = en & data;        en
   *
   * out = hold | data;      hold
   *
   * The returned expression is the actual select, not a copy.
   *
   * \param stmt The statement which could be if() or assign with top-level ternary.
   * \param saw_ternary_assignment Was this a ternary assign?
   * \return The select if discovered and valid, NULL otherwise.
   */
  NUExpr * getRhsSelect(NUStmt * stmt, bool * saw_ternary_assignment);

  //! Create single if() stmt from multiple stmts sharing a select
  /*! 
   * From a series of statements (if() or top-level ternary) sharing
   * the same select, generate a single if() statement with the proper
   * then/else clauses.
   *
   * Assumes that 'stmts' contains statements with a shared select;
   * consistency checking is not performed.
   *
   * The bodies of any if-then-else statements in 'stmts' are emptied
   * and used to create the combined if-then-else statement.
   *
   * \param select The shared select that will be copied for the new if() stmt.
   * \param stmts  List of statements which will be decomposed into then/else parts.
   * \return The generated if() statement.
   */
  NUIf * createCombinedIf(NUExpr * select, NUStmtList & stmts);

  //! Check if the control structure of a case statement is independent from its def set.
  /*!
   * This is similar to the if-control situation that ::getRhsSelect()
   * is trying to avoid. If a case statement redefines a net used in
   * its control (either selector or conditions), the reaching
   * definition for subsequent case statements will be different. 
   * 
   * As a result, merging this sort of case statement is incorrect.
   *
   * \param case_stmt A case statement.
   * \return true if 'case_stmt' does not re-define any control nets.
   */
  bool hasValidControl(const NUCase * case_stmt) const;

  //! Check if the control structure of two case statements is identical.
  /*!
   * Consider the selector as well as all the conditions of the two
   * case statements. If they are all identical, consider the control
   * structures to be identical.
   *
   * If either case statement contains more items or conditions, they
   * are not considered identical.
   *
   * If the ordering of items and conditions is not identical, the
   * case statements are not considered identical.
   * 
   * \param a One case statement.
   * \param b The other case statement.
   * \return true if 'a' and 'b' have identical control structure.
   */
  bool haveSharedControl(const NUCase * a, const NUCase * b) const;

  //! Construct a single case statement from a number sharing control structure.
  /*!
   * From a series of case statements sharing control structure,
   * append all contents into a single case statement.
   *
   * Assumes that 'a' and the case stmts in 'others' share control
   * structure. An assertion is made that the number of case items in
   * 'a' is the same as all case statements in 'others'. No other
   * consistency checks are performed.
   *
   * The bodies of 'others' case statements are emptied and moved into
   * the appropriate item within 'a'.
   *
   * \param a The first (and target) case statement.
   * \param others An ordered list of case statements; their contents are added to 'a'.
   */
  void createCombinedCase(NUCase * a, NUStmtList & others) const;

  //! Remove redundant assignments from if statements.
  RedundantAssigns * mRedundantAssigns;

  NUNetRefFactory * mNetRefFactory;
  UD * mUD;
  ControlExtractStatistics * mStatistics;

  //! Should we try and remove redundant statements?
  bool mRemoveRedundancies;

  //! Should we convert boolean forms into ternary statements?
  bool mConvertBooleanToTernary;
 
  //! Did we find any assignments and convert them into if-then-else?
  bool mDidWork;
};


//! Maintain statistics about the magnitude of success
/*!
 * ControlExtract is responsible for informing the statistics object
 * when it is successful in optimization some module/statement.
 */
class ControlExtractStatistics
{
public:
  //! Constructor
  ControlExtractStatistics() { clear(); }

  //! Destructor
  ~ControlExtractStatistics() {}

  void module() { ++mModules; }
  void ifSources(SInt32 num) { mIfSources+=num; }
  void ifTarget() { ++mIfTargets; }

  void caseSources(SInt32 num) { mCaseSources+=num; }
  void caseTarget() { ++mCaseTargets; }

  //! Reset all statistics.
  void clear() { 
    mModules     = 0; 
    mIfSources   = 0; 
    mIfTargets   = 0; 
    mCaseSources = 0;
    mCaseTargets = 0;
  }

  //! Print statistics.
  void print() const;

private:
  //! The number of modules with successful optimization (either if or case).
  SInt32 mModules;

  //! The number of statements used to create the combined if-then-else statements.
  SInt32 mIfSources;

 // The number of combined if-then-else statements.
  SInt32 mIfTargets;

  //! The number of statements used to create the combined case statements.
  SInt32 mCaseSources;

 // The number of combined case statements.
  SInt32 mCaseTargets;
};

#endif
