// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef ASSIGNALIAS_H_
#define ASSIGNALIAS_H_

#include "nucleus/Nucleus.h"
#include "flow/Flow.h"

/*!\file
 * Declaration of assignment and functional aliasing analysis.
 * AssignAlias is the analysis portion of the aliasing code --
 * the mechanics of aliasing are handled by the AliasManager
 * class.
 */

class IODBNucleus;
class ArgProc;
class MsgContext;
class ESPopulateExpr;
class AliasManager;
class AssignAliasCallback;
class FunctionCache;
class NetComponentMapPopulator;
class AssignAliasStatistics;
class DefAfterUse;
class Stats;

//! AssignAlias class
/*!
 * Perform assignment aliasing.
 *
 * Note that currently this is expected to be called before elaboration; only unelaborated
 * flow is fixed up.
 */
class AssignAlias
{
public:
  typedef UtSet<NUNet*> NetSet;

  //! constuctor
  /*
    \param netref_factory NetRef Factory
    \param dfg_factory DFG Factory
    \param iodb IODB
    \param msg_context Msg context
    \param verbose If 1, will be very verbose, 2 is minimal output
   */
  AssignAlias(NUNetRefFactory *netref_factory,
              FLNodeFactory *dfg_factory,
              IODBNucleus *iodb,
              ArgProc* args,
              MsgContext *msg_ctx,
              Stats * stats,
              bool phase_stats,
              bool find_functional_aliases,
              bool cont_assigns_only,
              SInt32 verbosity,
              AssignAliasStatistics * statistics);

  //! Handle this module
  void module(NUModule *this_module);

  ~AssignAlias();

private:
  //! Hide copy and assign constructors.
  AssignAlias(const AssignAlias&);
  AssignAlias& operator=(const AssignAlias&);

  //! Allow the callback access to this object
  friend class AssignAliasCallback;
  friend class NetComponentMapPopulator;

  //! Find aliases for this module
  void findAliases(NUModule *this_module);

  //! Create aliases by: doing a fanin walk of \a walk_net, finding the safe to alias net-pairs, creating the alias
  void walkNetFanin(NUNet *walk_net);

  //! Look for assignment aliasing opportunity
  bool addFaninAlias(FLNode * driver);

  //! Look for drivers with LHS dynamic indicies (can't do functional aliasing)
  bool invalidLvaluesRecurse(FLNode* flow) const;

  //! Look for functional aliasing opprtunities
  bool addFunctionalAlias(FLNode * driver);

  //! Used by both assignment and functional aliasing to attempt to alias two nets
  bool addAlias(NUNet * fanin_net, 
                NUNet * driven_net,
                FLNode * driver);

  //! Return true if the nets can be aliased
  bool testNetPair(NUNet *driver_net, NUNet *node_net);

  //! Return true if net2 is not used in any nodes where net1 is def'd
  bool testUseDefRelationship(NUNet *net1, NUNet *net2, FLNode *exclude_driver);

  //! Return true if the net flags do not conflict.
  bool testNetFlags(NUNet *net1, NUNet *net2);

  //! Check that two potential non-module-scoped aliases are in the same scope.
  bool testScopeConsistency(NUNet * net1, NUNet * net2);

  //! Return the fanin net if the assign is one we can alias-through, 0 otherwise
  NUNet* testAssign(NUAssign *assign);

  //! Return the fanin net if the always block is one we can alias-through, 0 otherwise
  NUNet* testAlways(NUAlwaysBlock *always, FLNode *driver);

  //! Return the fanin net if the driver is one we can alias-through, 0 otherwise
  NUNet* testDriver(FLNode *driver);

  /*!
   * Test to see if we can alias; we determined that the assignment is in the
   * same always block as the fanin driver.
   * We need to make sure that there are no defs which reach the always block
   * but don't fan into the assign:
   *
   * always
   *   begin
   *     b = c & d;
   *     a = b;
   *     b = e;
   *   end
   *
   * Here, we cannot alias "a = b;" because "b = e;" reaches the always block
   * but does not reach the assign.
   *
   * Return the fanin_net if we can alias, 0 otherwise.
   */
  NUNet *testValidSingleAlways(NUNet *fanin_net,
                               FLNode *always_driver,
                               FLNode *assign_driver);

  //! Test that a net has only a single definition within an always block.
  bool testSingleDefinition(NUNet * net, NUAlwaysBlock * always);

  //! Test that a net has no reads within an always block
  bool testSequentialRead(NUNet * net, NUAlwaysBlock * always);

  /*!
   * Test to see if we will alias fanin_net, where we have determined that
   * fanin_net has mutiple drivers.  The given always block must thus be
   * combinational and fanin_net must not be driven inside the given always
   * block.
   *
   * Return the fanin_net if we can alias, 0 otherwise.
   */
  NUNet *testValidCombAlways(NUNet *fanin_net, NUAlwaysBlock *always);

  //! Handles aliasing mechanisms
  AliasManager* mAliasManager;

  typedef UtMap<NUAlwaysBlock*,NetSet> DefinitionMap;

  //! Tracks single-definition characteristics of always blocks.
  DefinitionMap mSingleDefinitionMap;
  
  //! Tracks sequential-read characteristics of always blocks.
  DefinitionMap mSequentialReadMap;

  //! Netref factory
  NUNetRefFactory *mNetRefFactory;

  //! IODB
  IODBNucleus *mIODB;

  //! Expression factory.
  ESPopulateExpr * mPopulateExpr;

  //! Function cache.
  FunctionCache * mFunctionCache;

  //! Performance statistics object.
  Stats * mStats;

  //! Are we computing performance statistics?
  bool mPhaseStats;

  //! Should functionally equivalent nets become aliases?
  bool mFindFunctionalAliases;

  //! Are we looking at just continuous assigns, and allowing multiple drivers?
  bool mContAssignsOnly;

  //! Be verbose?
  SInt32 mVerbosity;

  //! Statistics object
  AssignAliasStatistics * mStatistics;

  //! Keep track of nets that are redefined after being used
  DefAfterUse* mDefAfterUse;
};


//! Track aliasing statistics 
class AssignAliasStatistics 
{
public:
  //! Constructor.
  AssignAliasStatistics() { clear(); }

  //! Destructor
  ~AssignAliasStatistics() {}

  //! Mark that an assignment alias was found.
  void assign() { ++mAssignAliases; }

  //! Mark that a functional alias was found.
  void functional() { ++mFunctionalAliases; }

  //! Reset all statistics.
  void clear() { mAssignAliases=0; mFunctionalAliases=0; }

  //! Dump the collected statistics.
  void print() const;

private:
  //! The number of discovered assignment aliases.
  SInt64 mAssignAliases;

  //! The number of discovered functional aliases.
  SInt64 mFunctionalAliases;
};

#endif
