// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef TERNARY_GATE_OPTIMIZATION_H_
#define TERNARY_GATE_OPTIMIZATION_H_

#include "nucleus/Nucleus.h"

class AtomicCache;
class MsgContext;
class IODBNucleus;
class ArgProc;
class NUNetRefFactory;

class BDD;
class BDDContext;

class ControlExtractStatistics;

typedef UtMap<const NUExpr*,BDD> NUExprToBDD;

//! Use BDDs to resynthesize the logic within a single module.
/*!
  Selectively use BDDs to rewrite logic.
 */
class TernaryGateOptimization
{
public:
  TernaryGateOptimization(AtomicCache * str_cache,
                          NUNetRefFactory * netref_factory,
                          MsgContext * msg_context,
                          IODBNucleus * iodb, 
                          ArgProc * args,
                          bool verbose,
                          ControlExtractStatistics * control_extract_statistics);

  ~TernaryGateOptimization();

  //! Apply BDD transformation to the specified module.
  /*!
    This was originally written to address performance concerns with bug
    5250, a dastardly testcase using a sum-of-products form. All
    continuous assigns within the module are converted into a canonical
    ?: form using BDDs and placed into a single always block.

    This always block is then optimized using ControlExtract. The hope
    is that ?: statements involving the same control will be combined.
    This is likely because the BDDs used a uniform order across all
    expressions.

    This optimization will only trigger if the 'ternaryGateOptimization'
    directive is specified:
    \code
        ternaryGateOptimization chksum_module
    \endcode

    Note: Moving all continuous assigns into a single always block may
    have correctness issues. For this reason, only use the
    'ternaryGateOptimization' directive when dealing with a simple,
    combinational module without cyclic or timing conflicts.
  */
  void module(NUModule * one_module);

  //! Apply BDD transformation to the specified blocks.
  /*! 
    This is the block-merging interface. Assignments are selectively
    transformed into a canonical form using BDDs.
   */
  void mergedBlocks(NUModule * module, NUUseDefVector & use_defs);

private:
  //! Hide copy and assign constructors.
  TernaryGateOptimization(const TernaryGateOptimization&);
  TernaryGateOptimization& operator=(const TernaryGateOptimization&);

  //! Apply BDD transformation to one always block.
  /*!
    Part of the block-merging call style.
   */
  bool always(NUAlwaysBlock * always);

  //! Are the uses similar enough to run through BDD with the same, incoming context?
  bool qualifyUseDelta(NUNetRefSet * context_uses, NUNetRefSet * stmt_uses);

  //! Map from net to the continuous assigns associated with that net.
  /*!
    Part of the module-specific call style. We apply BDD
    transformations with a uniform ordering to all assigns associated
    with a single net.
   */
  typedef UtMap<NUNet*,NUContAssignList> NetAssignMap;

  enum BDDEnrollmentEnum {
    eDoNotEnroll,
    eEnrollNewContext,
    eEnrollSameContext
  };

  typedef UtMap<NUStmt*,BDDEnrollmentEnum> NUStmtToBDDEnrollment;


  /*! 
    Determine if subsequent statements should be enrolled in a new BDD
    context or included in the same context as previous statements.
  */
  void determineContextForAssigns(NUAlwaysBlock * always,
                                  NUStmtToBDDEnrollment & enrollment);

  /*!
    Mark statements in an isolated context as not needing to be
    BDD-enrolled.
  */
  void eliminateSmallAssignContexts(NUAlwaysBlock * always, 
                                    NUStmtToBDDEnrollment & enrollment);

  //! Does the module contain only continuous assigns?
  /*!
    Part of the module-specific call style. We apply BDD
    transformations only if a module is completely combinational.
  */  
  bool onlyContAssignDrivers(const NUModule * mod) const;

  //! Are the continuous assigns in this module simple?
  /*!
    Part of the module-specific call style. Return true if all
    continuous assigns are of drive strength, define a single bit, and
    do not contain Z expressions.
   */
  bool simpleContAssigns(const NUModule * mod) const;

  //! Partition all continuous assigns by def net.
  /*!
    Part of the module-specific call style.
  */
  void segmentContAssigns(NUModule * mod, NetAssignMap * net_assigns);

  //! Are the continuous assigns consistent?
  /*!
    Part of the module-specific call style. We transform only if there
    are no overlaps between defs and uses -- this is an attempt to be
    more pessimistic. We will merge all these assigns into one block
    and want to avoid cycles where possible.
  */
  bool consistentAssigns(const NUContAssignList & assigns);

  //! Transform a BDD into an ?: expression.
  /*!
    Returns NULL if the BDD is invalid, if BDD -> NUExpr conversion
    fails, or if it appears that the conversion from factory NUExpr to
    serial NUExpr will blow up.
   */
  NUExpr * makeTernaryExpr(const NUExpr * rhs, NUExprToBDD * expr_to_bdd);

  //! Transform a BDD into a blocking assign with a ?: RHS.
  /*!
    Returns NULL if the BDD -> NUExpr conversion failed for some reason.

    \sa makeTernaryExpr
   */
  NUBlockingAssign * makeTernaryAssign(const NUAssign * assign, NUExprToBDD * expr_to_bdd);

  //! Will this expression explode?
  /*!
    Return true if it appears that converting the factory-enrolled
    NUExpr into a normal NUExpr will cause size growth by more than a
    factor of 100.

    This growth is possible because a factory-enrolled NUExpr shares
    memory for CSEs, while a normal NUExpr is a tree; no sharing
    occurs.
  */
  bool blowsup(const NUExpr * bddExpr);

  //! Does the expression contain wide sub-expressions?
  /*!
    Avoid creating BDDs when there are wide operations under a
    single-bit expression. We don't want to bit-blast things like
    comparators; we can't always reassemble the pieces.
   */
  bool wideExpressions(const NUExpr * expr);

  //! The BDD context; may be cleared from time to time.
  BDDContext * mBDDContext;

  //! String cache.
  AtomicCache * mStrCache;

  //! Net ref factory.
  NUNetRefFactory * mNetRefFactory;

  //! Message context.
  MsgContext * mMsgContext;

  //! IODB.
  IODBNucleus * mIODB;

  //! Argument processing.
  ArgProc * mArgs;

  //! Verbose? Currently unused.
  bool mVerbose;

  //! Control extract statistics. Used by module interface.
  ControlExtractStatistics * mControlExtractStatistics;
};

#endif
