// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef ALLOCALIAS_H_
#define ALLOCALIAS_H_

#include "util/UtMap.h"
#include "util/UnionFind.h"

#include "nucleus/Nucleus.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetRefMap.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUPortConnection.h"

#include "symtab/STSymbolTable.h"

//! Abstract interface for allocation alias queries
/*!
 * This is an abstract class which provides an interface to query if two nets
 * can be allocation aliased.
 *
 * If two nets are "allocation aliased" that means they have the same run-time
 * storage location.
 */
class AllocAlias
{
public:
  //! Enumeration to describe what the status of the ports of the design.
  enum PortStatus {
    eMatchedPortSizes,     //!< Ports have been rewritten such that formal and actual port sizes are guaranteed to match.
    eMismatchedPortSizes   //!< Ports have not been rewritten, formal and actual port sizes may not match.
  };

  //! constructor
  AllocAlias(PortStatus port_status) : 
    mPortStatus(port_status), mNetAliases(new NetAliases)
  {}

  //! destructor
  virtual ~AllocAlias() { delete mNetAliases; }

  //! Return true if the net can possible be allocation aliased to another net in the same module.
  bool query(NUNet *net) const;

  //! Return true if the nets are possibly allocation aliased.
  bool query(NUNet *net1, NUNet *net2) const;

  //! Return the status the ports assumed to be in.
  PortStatus getPortStatus() const { return mPortStatus; }

  //! Return a comparison between two nets for created buckets by aliases
  /*! This function first does a query to see if they are potential
   *  aliases. If so, it returns 0. Otherwise it returns the result of
   *  NUNet::compare.
   */
  int compare(NUNet* net1, NUNet* net2) const;

  //! Return a representative net for a given possibly aliased net
  NUNet* getRepresentativeNet(NUNet* net) const { return mNetAliases->find(net); }

  //! Add a net to the set. It will not be aliased
  void add(NUNet* net);

protected:
  //! Helper function to equate nets
  void equate(const NUNetSet& nets);
  //! Return the ranges for given net in all dimensions added to
  //! vector in higher to lower dimension order.
  void getRangeVectorForNet(NUNet* net, ConstantRangeVector& crv);

private:
  //! Hide copy and assign constructors.
  AllocAlias(const AllocAlias&);
  AllocAlias& operator=(const AllocAlias&);

  //! What status are the ports in?
  PortStatus mPortStatus;

  //! Abstraction for a union find set of aliases
  typedef UnionFind<NUNet*, NUNetSet> NetAliases;

  //! Storage for the computed net aliases
  NetAliases* mNetAliases;
};

//! Unelaborated allocation alias queries
/*!
 * This class understands which unelaborated nets could possibly be allocation
 * aliased, before elaboration has occurred.
 *
 * This is pessimistic; once elaboration has occurred, use the ElabAllocAlias
 * class to get less pessimistic results (that is, less nets will be considered
 * to be aliased and query() will return true less often).
 *
 * Since this class is unelaborated, it is used per-module.
 *
 * The following is a table that describes when two nets could
 * possibly be aliases. Note that the transitive property applies.
 *
 *              hierref   hieracc   formal   actual
 *
 *  hierref       x
 *
 *  hieracc       x         x
 *
 *  formal        x         x         x
 *
 *  actual        x         x         x        x
 *
 *  The blank part of the table is just the mirror image so it is left
 *  out for readability.
 *
 *  Legend:
 *
 *  hierref   - the net is a hier ref to another net
 *  hieracc   - the net is hier ref accessed from some other module.
 *  formal    - the net is a formal for a non-primary port
 *  actual    - the net is an actual to a sub-instance.
 *
 *  This means alias sets are constructed as follows:
 *
 * 1. If port_status is matched then only nets of the same size are
 *    considered. This breaks the alias sets up by size. Otherwise, all
 *    the nets are considered aliasable.
 *
 * 2. For each aliasable set, find the hier refs, hier refed, formals
      and actuals and make them a single aliasable set.
 */
class UnelabAllocAlias : public AllocAlias
{
public:
  //! constructor
  /*!
   * This walks the module and populates the mActuals set of nets.
   */
  UnelabAllocAlias(NUModule *module, PortStatus port_status);

  //! destructor
  ~UnelabAllocAlias() {}

private:
  //! Hide copy and assign constructors.
  UnelabAllocAlias(const UnelabAllocAlias&);
  UnelabAllocAlias& operator=(const UnelabAllocAlias&);

  //! Abstraction to keep a set of buckets of aliasable nets by size
  //! in each dimension.
  typedef UtMap<ConstantRangeVector, NUNetSet,
                ConstantRangeVectorCompare> NetBuckets;

  //! Populate the mActuals set of nets.
  void computeActuals();

  //! Walk the module nets and gather them into buckets
  void gatherAliasableNets(NetBuckets* net_buckets);

  //! Nets which appear as actuals to sub-instances of the module.
  NUNetSet mActuals;

  //! Module being analyzed.
  NUModule *mModule;
};


//! Elaborated allocation alias queries
/*!
 * This class understands which unelaborated nets can be allocation aliased, after
 * elaboration has occurred.
 *
 * Understanding this allows us to perform some optimizations such as code movement
 * and CSE.
 *
 * As an example, consider:
 * \code
 *  module foo(in1, in2, o1, o2);
 *    input in1, in2;
 *    output o1;
 *    always @(in1 or in2)
 *    begin
 *      o1 = in1 & in2;
 *      o2 = in1 & in2;
 *    end
 *  endmodule
 * \endcode
 *
 * In this example, in1 and in2 will be codegen'd as references, and
 * the C++ compiler won't be able to tell if they point to o1 and/or
 * o2, or not.  So, the C++ compiler won't be able to perform any
 * optimization in this case.
 *
 * However, we can know this because we know the alias rings in the
 * symbol table.
 *
 * Note that there is some pessimism in this algorithm. It works by
 * walking every alias ring and finding any rings that have more than
 * one net in the same scope. Those nets aliased and must be treated
 * as such unelaboratedly.
 *
 * But the pessimism is introduced by the transitive property of
 * UnionFind. Say we have two alias rings, one with m.n1 and m.n2 as
 * aliases. The other has m.n2 and m.n3 as aliases. Through the
 * transitive property m.n1 and m.n3 will be treated as aliases, even
 * though they aren't.
 *
 * If this turns out to be a problem we will need a more complex
 * representation.
 */
class ElabAllocAlias : public AllocAlias
{
public:
  //! constructor
  ElabAllocAlias(PortStatus port_status);

  //! destructor
  ~ElabAllocAlias() {}

  //! Compute the aliases for the entire design
  void compute(NUDesign* design, STSymbolTable* symtab);

private:
  //! Hide copy and assign constructors.
  ElabAllocAlias(const ElabAllocAlias&);
  ElabAllocAlias& operator=(const ElabAllocAlias&);

  //! Walk the symbol table and gather unelaborated aliases.
  void gatherAliases(STSymbolTable* symtab);

  //! Add the unelaborated aliases for one alias ring
  void compute(STAliasedLeafNode* leaf);
}; // class ElabAllocAlias : public AllocAlias

//! Design allocation alias queries
/*! This class is a hybrid between the unelaborated allocated alias
 *  and elaborated allocated alias classes. It tries to reduce the
 *  pessimism introduced with the UnelabAllocAlias class by walking
 *  the design. The problem is that it counts on the design port
 *  connections staying consistent which is not true during the local
 *  analysis passes where flattening is occuring.
 *
 *  This class takes an NUDesign and walks it looking for all port
 *  connections and hierarchical references. It uses that information
 *  to create a graph from actuals to formals (referer to resolution
 *  for hier refs). This graph can then be used to find all the
 *  possible aliases per scope.
 *
 *  That in turn can be used to have the reorder code introduce less
 *  temporaries.
 */
class DesignAllocAlias : public AllocAlias
{
public:
  //! constructor
  DesignAllocAlias(NUDesign* design, PortStatus port_status);

  //! destructor
  ~DesignAllocAlias() {}

private:
  //! Hide the copy constructor
  DesignAllocAlias(const DesignAllocAlias&);

  //! Hide the assign constructor
  DesignAllocAlias& operator=(const DesignAllocAlias&);

  //! Helper function to place a set of possible aliases in the NetAliases storage
  void processAliases(const NUNetVector& aliases);

  typedef std::pair<ConstantRangeVector, NUScope*> SizeAndScope;

  //! Provides the SizeAndScope less-than function for a UtMap with
  //! SizeAndScope as key.
  class SizeAndScopeCompare :
    public std::binary_function<SizeAndScope&, SizeAndScope&, bool>
  {
  public:
    //! Returns true only if sas1 < sas2
    bool operator()(const SizeAndScope& sas1, const SizeAndScope& sas2) const;
  };

  typedef UtMap<SizeAndScope, NUNetSet, SizeAndScopeCompare> ScopeAliases;

  //! Design being analyzed
  NUDesign* mDesign;
}; // class DesignAllocAlias : public AllocAlias

//! AllocAliasBucketNetRefMap
/*! This class organizes the netref multi maps by possible alloc
 *  aliases. By doing this up front we avoid a linear search to find
 *  all possible aliases in the map.
 */
template <class NetRefMapType>
class AllocAliasBucketNetRefMap
{
public:
  typedef typename NetRefMapType::data_type data_type;

  //! constructor
  AllocAliasBucketNetRefMap(AllocAlias* query, NUNetRefFactory* netRefFactory) :
    mCompare(query), mNetRefFactory(netRefFactory)
  {
    mAliasBuckets = new AliasBuckets(mCompare);
  }

  //! destructor
  ~AllocAliasBucketNetRefMap()
  {
    for (LoopMap<AliasBuckets> l(*mAliasBuckets); !l.atEnd(); ++l) {
      NetRefMapType * netRefMap = l.getValue();
      delete netRefMap;
    }
    delete mAliasBuckets;
  }

  //! Insert a new statement node for a net ref
  void insert(const NUNetRefHdl& netRef, data_type node)
  {
    NetRefMapType * netRefMap = getNetRefMap(netRef);
    netRefMap->insert(netRef, node);
  }

  //! Erase a net ref from the right map
  void erase(const NUNetRefHdl& netRef)
  {
    NetRefMapType * netRefMap = getNetRefMap(netRef);
    netRefMap->erase(netRef);
  }

  //! Return the net ref to statement node multi map for this alias bucket
  NetRefMapType * getNetRefMap(const NUNetRefHdl& netRef)
  {
    typename AliasBuckets::iterator pos = mAliasBuckets->find(netRef);
    NetRefMapType* netRefMap;
    if (pos == mAliasBuckets->end()) {
      netRefMap = new NetRefMapType(mNetRefFactory);
      mAliasBuckets->insert(typename AliasBuckets::value_type(netRef, netRefMap));
    } else {
      netRefMap = pos->second;
    }
    return netRefMap;
  }

private:
  //! Compare class to bucketize net refs
  class Compare
  {
  public:
    //! Constructor
    Compare(AllocAlias* query) : mQuery(query) {}

    //! compare operator
    bool operator()(const NUNetRefHdl& netRef1, const NUNetRefHdl& netRef2) const
    {
      NUNet* net1 = netRef1->getNet();
      NUNet* net2 = netRef2->getNet();
      return mQuery->compare(net1, net2) < 0;
    }

  private:
    AllocAlias* mQuery;
  };

  //! Compare function to create buckets
  Compare mCompare;

  //! NetRef factory for the multi map
  NUNetRefFactory* mNetRefFactory;

  //! Abstraction for alias bucketized net ref maps
  typedef UtMap<NUNetRefHdl, NetRefMapType *, Compare> AliasBuckets;

  //! Storage for the alias bucketized net ref maps
  AliasBuckets* mAliasBuckets;
}; // class AllocAliasBucketNetRefMap

//! NetRefOverlapsQuery
/*!
 * Class used for conditional loop query function when determining overlap.
 */
class NetRefOverlapsQuery : public NUNetAndNetRefQuery
{
public:
  NetRefOverlapsQuery(AllocAlias *alias_query) : NUNetAndNetRefQuery(), mAliasQuery(alias_query) {}
  ~NetRefOverlapsQuery() {}

  /*!
   * When formal and actual ports are guaranteed to match in size, there is a match if the netrefs
   * overlap.
   *
   * When formal and actual ports are not guaranteed to match in size, there is always a match.
   *
   * This does not query for potentially aliased, because the operator()(const NUNet*, const NUNetRef&)
   * has already weeded that out.  So, this only needs to check for overlap.
   */
  bool operator()(const NUNetRef& first, const NUNetRef& second)
  {
    if ((mAliasQuery->getPortStatus() == AllocAlias::eMatchedPortSizes) or
        (first.getNet() == second.getNet())) {
      return first.overlapsBits(second);
    } else {
      return true;
    }
  }

  /*!
   * Include the given net in the walk if either it is the same net as the netref we
   * are searching for, or it is possibly aliased to the net of the given netref.
   */
  bool operator()(const NUNet *net, const NUNetRef &ref)
  {
    return ((ref.getNet() == net) or
            mAliasQuery->query(const_cast<NUNet*>(net), ref.getNet()));
  }

private:
  AllocAlias *mAliasQuery;
};


//! \class ProtectedAliases
/*! At port vectorisation time, all various protected attributes have not been
  * propagated around the design. Thus, while a net may be marked, its aliases
  * may not be. This is a problem because port vectorisation really must not
  * touch protected nets. This class builds on DesignAllocAlias to implement a
  * protected operator that returns true if a net or any of its aliases are
  * protected.
  */
class ProtectedAlias : public DesignAllocAlias {
public:

  ProtectedAlias (IODBNucleus *iodb, NUDesign *design);
  virtual ~ProtectedAlias () {}

  //! \return iff a net or any of its aliases are protected
  bool operator () (NUNet *net) const
  { return mProtect.find (getRepresentativeNet (net)) != mProtect.end (); }

  //! \return iff there are no protected nets
  bool empty () const { return mProtect.empty (); }

  //! mark a net and its aliases as protected
  inline void protect (NUNet *net) { mProtect.insert (getRepresentativeNet (net)); }

  //! construct a human readable representation of the protected st
  /*! \param flags options for compose
   *  \note the bottom 8 bits of flags are used as the indent amount
   */
  void compose (UtString *s, const UInt32 flags) const;

private:
  
  NUNetSet mProtect;                    //! representative of all protected nets

};

#endif
