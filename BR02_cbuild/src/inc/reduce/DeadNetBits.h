// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Optimization to remove any dead net bits
*/

#ifndef _DEADNETBITS_H_
#define _DEADNETBITS_H_

class NUNetRefFactory;
class NUModule;
class NUNetRefSet;
class IODBNucleus;
class AtomicCache;
class ArgProc;
class MsgContext;
class Fold;

//! class REDeadNetBits
class REDeadNetBits
{
public:
  //! constructor
  REDeadNetBits(NUNetRefFactory* netRefFactory,
                IODBNucleus* iodb,
                AtomicCache* strCache,
                ArgProc* args,
                MsgContext* msgContext);

  //! destructor
  ~REDeadNetBits();

  //! process a module, returns TRUE if changes were made
  bool module(NUModule* module);

private:
  //! Helper function to find the used bits of this module
  void findUsedBits(NUModule* module, NUNetRefSet* usedBits);

  //! Helper to mark nets that are part of nucleus that cannot be optimized
  void addInvalidOptNets(NUModule* module, NUNetRefSet* usedBits);

  //! Helper function to prune non-optimizable nets
  bool canOptimize(NUNet* net);

  //! Helper function to remove dead bits (by temping them)
  bool removeDeadBits(NUModule* module, const NUNetRefSet& usedBits);

  //! Helper function to create replacment concats
  void createDeadBitsReplacements(const NUNetRefHdl& deadNetRef,
                                  NULvalueReplacementMap* lvalReplacements);

  //! Helper function to free allocated temporary memory
  void deleteDeadBitsReplacements(const NULvalueReplacementMap& lvalues);

  //! Net ref factory to create net ref sets
  NUNetRefFactory* mNetRefFactory;
  
  //! Used to flow walking
  IODBNucleus* mIODB;

  //! Used to create new nets
  AtomicCache* mStrCache;

  //! Helper class to optimize replacements
  Fold* mFold;
}; // class REDeadNetBits

#endif // _DEADNETBITS_H_
