// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef REMOVEDEADCODE_H_
#define REMOVEDEADCODE_H_

#include "flow/Flow.h"
#include "nucleus/Nucleus.h"
#include "reduce/UnelabMarkSweep.h"
#include "nucleus/NUDesignWalker.h"
#include "util/UtMap.h"
#include "nucleus/NUNetRefSet.h"

/*!
  \file
  Declaration of dead code removal package.
 */

class IODBNucleus;
class ArgProc;
class MsgContext;
class DeadDesignCallback;

//! RemoveDeadCode class
/*!
 * Class to find any dead code and remove it.  Both Nucleus and unelaborated flow
 * are removed.
 */
class RemoveDeadCode
{
public:
  //! constuctor
  /*
    \param netref_factory NetRef Factory
    \param dfg_factory DFG Factory
    \param iodb IODB
    \param msg_context Msg context
    \param delete_dead_code If true, dead code will be removed, if false just unelab flow is modified
    \param delete_instances If true, module instances will be removed while deleting dead code
                            If performing dead code elimination after elaborated aliasing,
                            deleting module instances could delete allocation points, leading to
                            an invalid model.
    \param verbose If true, will be verbose about dead code removal
   */
  RemoveDeadCode(NUNetRefFactory *netref_factory,
                 FLNodeFactory *dfg_factory,
                 IODBNucleus *iodb,
                 ArgProc* args,
                 MsgContext *msg_ctx,
                 bool start_at_all_nets,
                 bool delete_dead_code,
                 bool delete_instances,
                 bool verbose) :
    mNetRefFactory(netref_factory),
    mFlowFactory(dfg_factory),
    mIODB(iodb),
    mArgs(args),
    mMsgContext(msg_ctx),
    mStartAtAllNets(start_at_all_nets),
    mDeleteDeadCode(delete_dead_code),
    mDeleteInstances(delete_instances),
    mVerbose(verbose)
  {}

  //! Walk this whole design.
  /*!
    \param design The design
    \param callback User-callback called prior to deleting nets;
        allows the user to cleanup elaborated references, the symbol
        table, etc.
  */
  void design(NUDesign *design, DeadDesignCallback * callback);

  //! Walk only this module
  void module(NUModule *module, DeadDesignCallback * callback=NULL);

  //! destructor
  ~RemoveDeadCode() {}

private:
  //! Hide copy and assign constructors.
  RemoveDeadCode(const RemoveDeadCode&);
  RemoveDeadCode& operator=(const RemoveDeadCode&);

  //! Allow the callbacks to access the object
  class FlowCallback;
  friend class FlowCallback;

  //! Flow callback class for sweep
  class FlowCallback : public UnelabMarkSweep::SweepCallback
  {
  public:
    FlowCallback(RemoveDeadCode *obj) : mObj(obj) {}
    void operator()(FLNode *flnode, bool is_dead) { mObj->sweepCallback(flnode, is_dead); }
    ~FlowCallback() {}

  private:
    RemoveDeadCode* mObj;
  };

  //! Flow callback class for nucleus walk
  class NucleusTouchCallback : public NUDesignCallback
  {
#if ! pfGCC_2
    using NUDesignCallback::operator();
#endif

  public:
    NucleusTouchCallback() : mObj(0) {}

    /*!
     * If only_one_module is true, only the first module seen will be
     * traversed.  This is used for doing dead code elimination under
     * one module.
     */
    NucleusTouchCallback(RemoveDeadCode *obj, bool only_one_module=false) :
      mObj(obj),
      mOnlyOneModule(only_one_module),
      mContinueModule(true)
    {}

    //! Clear the mark on the nucleus node
    Status operator()(Phase phase, NUBase *node);
    Status operator()(Phase phase, NUModule *node);

    ~NucleusTouchCallback() {}

  private:
    RemoveDeadCode* mObj;
    bool mOnlyOneModule;
    bool mContinueModule;
  };

  //! Callback used on the sweep
  void sweepCallback(FLNode *flnode, bool is_dead);

  //! Perform the mark pass over the whole design
  void markLiveNucleus(NUDesign *this_design);

  //! Perform the mark pass over a module
  void markLiveNucleus(NUModule *this_module);

  //! Remove all the dead nucleus we have found for the design
  void deleteDeadNucleus(NUDesign *this_design,
                         DeadDesignCallback * callback);

  //! Remove the dead nucleus we have found for the module
  void deleteDeadNucleus(NUModule *this_module,
                         DeadDesignCallback * callback);

  //! Callback for deletion of dead nucleus
  void nucleusDeleteCallback(NUBase *node);

  //! NetRef Factory
  NUNetRefFactory *mNetRefFactory;

  //! DFG Factory
  FLNodeFactory *mFlowFactory;

  //! IODB
  IODBNucleus *mIODB;

  //! args
  ArgProc* mArgs;

  //! Message context
  MsgContext *mMsgContext;

  //! Set of nucleus constructs which will be removed
  NUUseDefSet mRemoveNodes;

  //! Map of u/d nodes -> set of netrefs
  typedef UtMap<NUUseDefNode*,NUNetRefSet> UDNetRefSetMap;

  //! Keep a map of nodes we want to remove, to the set of dead defs we have seen.
  //! Once all the defs are seen as dead, the node will be moved to the mRemoveNodes
  UDNetRefSetMap mPartiallyCoveredMap;

  //! Use all nets or outputs/observable as starting points?
  bool mStartAtAllNets;

  //! Delete dead code?
  bool mDeleteDeadCode;

  //! Delete module instances while deleting dead code?
  bool mDeleteInstances;

  //! Be verbose?
  bool mVerbose;
};

#endif
