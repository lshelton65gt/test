// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef REDUCE_UTIL_
#define REDUCE_UTIL_

/*!
  \file
  Declaration of reduce utility package.
 */

#include "nucleus/Nucleus.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUNetRefMap.h"

typedef NUNetRefMultiMap<NUUseDefSet*> NUNetRefToNodeSet;

class ReduceUtility 
{
public:
  ReduceUtility(NUModule * module,
		NUNetRefFactory * netref_factory) : 
    mModule(module),
    mNetRefFactory(netref_factory),
    mValid(false),
    mModuleDefs(netref_factory),
    mModuleUses(netref_factory),
    mInitialBlockDefs(netref_factory),
    mTaskDefs(netref_factory),
    mNetRefDefs(netref_factory)
  {}

  ~ReduceUtility() { clear(); }

  void update();
  void clear();

  bool inInitialBlock(const NUNet * net) const;
  bool inInitialBlock(const NUNetRefHdl & net_ref) const;

  bool inTask(const NUNet * net) const;
  bool inTask(const NUNetRefHdl & net_ref) const;

  bool hasModuleUse(const NUNet * net) const;
  bool hasModuleUse(const NUNetRefHdl & net_ref) const;

  bool hasModuleDef(const NUNet * net) const;
  bool hasModuleDef(const NUNetRefHdl & net_ref) const;

  SInt32 getDefCount(const NUNet * net) const;
  SInt32 getDefCount(const NUNetRefHdl & net_ref) const;

  void getDefs(const NUNet * net, NUUseDefSet & use_def_set) const;
  void getDefs(const NUNetRefHdl & net_ref, NUUseDefSet & use_def_set) const;

private:
  void getModuleDefs();
  void getModuleUses();

  //! Keep track (by netref) of def location (countable).
  void addDefs(NUUseDefNode * node, NUNetRefSet & net_set);

  NUModule * mModule;

  NUNetRefFactory * mNetRefFactory;

  bool mValid;

  NUNetRefSet mModuleDefs;
  NUNetRefSet mModuleUses;

  NUNetRefSet mInitialBlockDefs;
  NUNetRefSet mTaskDefs;

  NUNetRefToNodeSet mNetRefDefs;
};

//! SubstituteNets class
/*!
 * Used for fixup of aliased nets.  This is called over the module to substitute
 * subordinates for master.
 */
class SubstituteNets : public NuToNuFn
{
public:
  SubstituteNets(NUNetReplacementMapNonConst * replacements) :
    mReplacements(replacements)
  {}

  ~SubstituteNets() {}

  NUNet *operator()(NUNet *net, Phase phase)
  {
    if (not isPre (phase))
      return 0;

    NUNetReplacementMapNonConst::iterator iter = mReplacements->find(net);
    if (iter != mReplacements->end()) {
      return iter->second;
    }
    return 0;
  }

private:
  NUNetReplacementMapNonConst * mReplacements;
};

class LessThanUseDef : public std::binary_function<NUUseDefNode*, NUUseDefNode*, bool>
{
public:
  LessThanUseDef(NUNetRefFactory * netref_factory) :
    mNetRefFactory(netref_factory)
  {}

  bool operator()(const NUUseDefNode * a, const NUUseDefNode * b) const;
protected:
  NUNetRefFactory * mNetRefFactory;
};

class LessThanAssign : public LessThanUseDef {
 public:

  // Use a comparison that looks into the right-side if both left sides are
  // idents. This is a little more expensive than LessThanUseDef, but will
  // allow discovery of more vectorisation opportunities in which the
  // left-side is a bare identifier.

  LessThanAssign (NUNetRefFactory *netref_factory) : LessThanUseDef (netref_factory)
  {}

  bool operator () (const NUUseDefNode *, const NUUseDefNode *) const;

};

#endif // REDUCE_UTIL_
