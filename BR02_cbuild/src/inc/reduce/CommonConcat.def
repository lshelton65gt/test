// -*-C++-*-    $Revision: 1.2 $
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

//! CommonConcat.def preprocessor abuse file.

// Define the counter tags for common concat statistics gathering.

#ifndef CONCAT_COUNTER
#define CONCAT_COUNTER(_tag_, _label_, _description_)
#endif

CONCAT_COUNTER (LEFT_CONCATS,  "discover-left", "discovery of left-side concats")
CONCAT_COUNTER (RIGHT_CONCATS, "discover-right", "discovery of right-side concats")
CONCAT_COUNTER (REPLACE_RVALUE,"replace-right", "replacement of right-side concat")
CONCAT_COUNTER (REPLACE_LVALUE,"replace-left", "replacement of left-side concat")
CONCAT_COUNTER (REPLACE_LOCAL_NET, "replace-local", "replacement of a local net")
CONCAT_COUNTER (ORPHAN_RVALUE_CONCAT, "orphan-right", "an orphan right-side concat")
CONCAT_COUNTER (BAD_ORPHAN_RVALUE_CONCAT, "bad-orphan-right", "an orphan right-side concat with no elimination")

#undef CONCAT_COUNTER

// Define the logfile tags for common concat

#ifndef CONCAT_LOG
#define CONCAT_LOG(_tag_, _descr_)
#endif

CONCAT_LOG (BUILD_ASSIGN, "constructed assignment statement")
CONCAT_LOG (LVALUE_CONCAT, "a discovered left-side concat")
CONCAT_LOG (REPLACED_LOCAL_NET_LVALUE, "replacement of a left-side local net occurrence")
CONCAT_LOG (REPLACED_LOCAL_NET_RVALUE, "replacement of a right-side local net occurrence")
CONCAT_LOG (REPLACED_LVALUE_CONCAT, "replacement of a left-side concat")
CONCAT_LOG (REPLACED_RVALUE_CONCAT, "replacement of a right-side concat")
CONCAT_LOG (ORPHAN_RIGHT_SIDE,   "replacement of a single right-side concat")
CONCAT_LOG (BAD_ORPHAN_RIGHT_SIDE,   "replacement of a single right-side concat with no eliminations")
CONCAT_LOG (ORPHAN_RIGHT_SIDE_IGNORED,   "replacement of a single right-side concat ignored")
CONCAT_LOG (LVALUE_CONCAT_REJECTED, "a rejected left-side concat")
CONCAT_LOG (ELIMINATED, "an eliminated net with no write-back")

#undef CONCAT_LOG

// Define the different types of common concat.
#ifndef CONCAT_FLAVOUR
#define CONCAT_FLAVOUR(_tag_, _descr_)
#endif

CONCAT_FLAVOUR (UNSPECIFIED, "unspecified flavour")
CONCAT_FLAVOUR (CONT_LEFT,   "appears on a continuous assign left-side")
CONCAT_FLAVOUR (CONT_RIGHT,  "appears on continuous assign right-side")
CONCAT_FLAVOUR (PROC_LEFT,   "appears on a procedural left-side")
CONCAT_FLAVOUR (PROC_RIGHT,  "appears on a procedural right-side")

#undef CONCAT_FLAVOUR
