// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef REALIAS_H_
#define REALIAS_H_

#include "reduce/Reduce.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUNet.h"
#include "flow/Flow.h"
#include "util/UtSet.h"
#include "util/UtHashSet.h"
#include "nucleus/NUNetSet.h"

class MsgContext;
class STSymbolTable;
class Stats;
class NUNetRefFactory;

// please do not use "using namespace std"

/*!
  \file
  Declaration of class used to generate aliasing.
*/

//! REAlias
/*!
 * Class which generates aliasing for a design.
 *
 * As aliasing occurs, we pick one of the aliases to be allocated:
 * . try to pick the causally earliest, this will cause
 *    inputs to be allocated at the node highest in hierarchy
 *    outputs to be allocated at the node lowest in hierarchy
 * . except, if there are multiple drivers, then allocate outputs to
 *   the highest hierarchy point with multiple drivers
 */
class REAlias
{
public:
  class Predicate
  {
  public:
    Predicate();
    virtual ~Predicate();

    //! if flow is aliasable, then return net to be aliased
    virtual NUNetElab* findDriverNet(FLNodeElab* flow) = 0;

    //! find master net for most recent alias
    virtual NUNetElab* findMaster() = 0;

    //! get iteration visit flags
    virtual FLIterFlags::TraversalT getVisitFlags() = 0;

    //! get iteration nesting flags
    virtual FLIterFlags::NestingT getNestingFlags() = 0;

    //! should the alias code absolutely obey causality?
    virtual bool enforceCausalFirst() const = 0;
  };

  //! constructor
  /*!
    \param design The design to walk
    \param symtab The symbol table to use to look up elaboration
    \param flow_factory Factory used to manage elaborated flow nodes.
    \param msg_context Used to emit messages.
    \param verbose If true, will print out aliasing as it occurs.
   */
  REAlias(NUDesign *design,
	  STSymbolTable *symtab,
	  FLNodeElabFactory *flow_factory,
	  MsgContext *msg_context,
          NUNetRefFactory *netref_factory,
	  bool verbose,
          Predicate* predicate = 0,
          bool doAllocation = true,
          bool keepDeadFlows = false):
    mDesign(design),
    mSymtab(symtab),
    mFlowFactory(flow_factory),
    mMsgContext(msg_context),
    mNetRefFactory(netref_factory),
    mVerbose(verbose),
    mPredicate(predicate),
    mDoAllocation(doAllocation),
    mKeepDeadFlows(keepDeadFlows),
    mBadAllocation(false)
  {}

  //! Design walk to generate aliasing for a design. 
  void aliasDesign(Stats *stats, bool phase_stats);

  //! Find aliases in the design
  /*! This routine is called as part of aliasDesign() below. If you
   *  need to perform the actual aliasing pass, then call that. This
   *  routine is useful to get alias marking without actually doing
   *  the hierarchical aliasing.
   *
   *  The default value for createAliases is false because that is how
   *  it should be called if we don't want to create the aliases. This
   *  function is called with createAliases set to true by
   *  aliasDesign().
   */
  void findAliases(Stats* stats, bool phase_stats, bool createAliases = false);

  //! Find all possible aliases in the design
  /*! This pass assumes that all nets connected through ports will
   *  become aliases. It ignores any size or varselect issues that the
   *  real aliasing has to deal with. This is because optimiations or
   *  port splitting may cause the alias to actually happen.
   */
  void findPessimisticAliases();

  //! Get master and all the aliases for a given elaborated net
  /*! This routine gets all potential aliases in the alias ring
   *  including the master and this net.
   */
  void getAliases(NUNetElab* netElab, NUNetElabVector* netElabs);

  //! Get the computed master for a net elab
  /*! Either returns the master for the given netElab or the netElab
   *  if it is the master or there isn't one.
   */
  NUNetElab* getMaster(NUNetElab* netElab);

  //! Test if two elaborated nets can be aliased
  bool isAlias(NUNetElab* netElab1, NUNetElab* netElab2);

  //! Determine the master alias, and return the pair(master,subordinate)
  /*!
    /param causal_first is the driving net (causally earliest)
    /param causal_second is the driven net (causally latest)
    /return pair(master,subordinate)
  */
  static std::pair<NUNetElab*, NUNetElab*>
  determineMasterAlias(NUNetElab *causal_first, NUNetElab *causal_second);

  //! destructor
  ~REAlias();

private:
  //! Hide copy and assign constructors.
  REAlias(const REAlias&);
  REAlias& operator=(const REAlias&);

  //! Convenience typedef for a set of net elabs
  typedef UtHashSet<NUNetElab*> NetElabSet;
  typedef NetElabSet::const_iterator NetElabSetIter;
  typedef NetElabSet::UnsortedLoop NetElabSetLoop;

  //! Make the subordinate nets be aliases of the master
  /*!
    /param master_net Master of the alias ring.
    /param subord_nets Aliases of the master.

    Create an alias out of the given nets, delete the subordinate
    elaborated flow node and elaborated net, fixup the fanouts of the
    subordinate, mark allocation.
   */
  void createAliases(NUNetElab *master_net,
		     NetElabSet *subord_nets);

  //! Sanity check (bug 442) - make sure that the data structures are consistent.
  void sanityCheckAliases();

  //! Make sure no nets in the given set are alias masters
  void sanityCheckSubordSet(NUNetElab *master, const NetElabSet *subord_nets);

  //! Make sure the given master is not a subordinate of something else
  void sanityCheckMaster(NUNetElab *master);

  //! Make sure there is one and only one net allocated in an alias ring.
  void sanityCheckAllocation(NUNetElab *master, const NetElabSet *subord_nets);

  //! Iterate over the symbol table and make sure that all storage elements are marked allocated.
  /*!
    This is a separate test from sanityCheckAllocation() because there
    may be aliases in the symbol table which were not discovered by
    REAlias.
   */
  void sanityCheckSTAllocation();

  //! Fixup the fanout of the given net by replacing the old drivers with the new
  //! driver.
  void fixupFanout(NUNetElab *net,
		   const FLNodeElabList& old_driver_list,
		   NUNetElab::DriverLoop new_drivers);

  //! Merge any existing aliases of subord_alias into master_alias.
  void mergeAlias(NUNetElab *master_alias, NUNetElab *subord_alias);

  //! Remember the given nodes as alias candidates
  /*!
    /param causal_first is the driving net (causally earliest)
    /param causal_second is the driven net (causally latest)
    /param driver is the first->second driver which will be eliminated.
    
    We store these up instead of creating the alias as we go, because creating
    the alias as we go will mess up the iterator we use for traversal.
  */
  void rememberAlias(NUNetElab *causal_first,
		     NUNetElab *causal_second,
		     FLNodeElab *driver,
                     bool enforceCausalFirst);

  //! Remember an alias, specifying master & subord, not taking into
  //! acount flow.  This is called by rememberAlias, and also by
  //! the symbol table based walk that picks up dead flow
  void rememberAliasHelper(NUNetElab* master, NUNetElab* subord,
                           NUNetElab* causal_first);

  //! Remember the given node as a fanout of the given net.
  void rememberFanout(NUNetElab *net, FLNodeElab *fanout);

  //! Remember the given node as a driver of the given net.
  /*!
    \return true if have seen this driver of this net before.

   * Need to do this because drivers may be nested inside sequential blocks, and
   * need to fixup all the drivers when doing aliasing, not just the top-level
   * continuous drivers.
   */
  bool rememberDriver(NUNetElab *net, FLNodeElab *driver);

  //! Walk the nodes which we have remembered and create aliases.
  void walkAliases();

  //! Walk the design, looking for potential aliases.
  /*!
   * For now, these are the cases we alias:
   *  . neither the source nor the sink can be multiply-driven
   *  . through buffers and assigns
   *  . through output port connections
   *  . through input port connections
   */
  void walkDesign(bool createAliases);

  //! Walk the symbol table, looking for aliases missed by the design walk
  void walkSymtab();

  //! Walk a branch node looking for missed aliases
  void fixMissedAliases(STBranchNode* parent);

  // Remember the aliasing for a dead port if it is not already remembered
  void aliasDeadPort(NUNetElab* master, NUNetElab* alias);

  //! Perform allocation of unelaborated nets.
  /*!
   * Walk mAllocated and mark all keys as being unallocated, unless the
   * value the key is mapped-to is in mCannotAllocate.
   *
   * Also mark all nets in mCannotAllocate as begin unallocated.
   */
  void performAllocation();

  //! Set the first net as being allocated to the second net.
  /*!
   * If the first net is already allocated to something else, find the end point
   * of that chain and make that net allocated to the second net.
   */
  void pairwiseAllocation(NUNetElab *net_being_allocated,
			  NUNetElab *net_allocated_to);

  //! Compute allocation between the nets given and save that information.
  void rememberAllocation(NUNetElab *causal_first, NUNetElab *causal_second);

  //! Convenience typedef of set of net-sets, used to collect unelaborated aliases.
  typedef UtSet<NUNetSet*> SetOfNetSet;

  //! Given a net, if it already has an alias set in net_alias_map, then merge the
  //! existing set into new_alis_set, and put the existing set into delete_sets.
  void mergeAliasSets(NUNet *net,
		      NUNetSet &new_alias_set,
		      NUNetSetMap &net_alias_map,
		      SetOfNetSet &delete_sets);

  //! Walk over the design, propagate the primary_z and z flags.  Iterate until a fixed point is reached.
  void deadPortFixup();

  //! Clear the driving flow for a dead net
  void clearDrivers(NUNetElab* net);

  //! Eliminate the eliminated-drivers for the master and subordinates
  /*!
    \param master_net Alias master
    \param subord_nets Alias subordinates
    \param old_driver_list List to which all eliminated drivers will be added.
   */
  void processElimDrivers(NUNetElab *master_net,
			  NetElabSet *subord_nets,
			  FLNodeElabSet &old_driver_list);

  //! Eliminate multiple bound nodes on the master and subordinates.
  /*!
    \param master_net Alias master
    \param subord_nets Alias subordinates
    \param old_driver_list List to which all eliminated drivers will be added.
   */
  void processBoundDrivers(NUNetElab *master_net,
                           NetElabSet *subord_nets,
                           FLNodeElabSet &old_driver_list);

  //! Change the drivers to the subords to be drivers to the master.
  void processDrivers(NUNetElab *master_net,
		      NetElabSet *subord_nets);

  //! Traverse the flags on the aliases and issue warnings.
  /*!
   * Note: must be called after the eliminate-drivers have been eliminated.

    \param master_net Alias master
    \param subord_nets Alias subordinates
    \param is_primary_tristate Set to true if these aliases should be marked primary-tristate, false otherwise.
   */
  void flagAliases(NUNetElab *master_net,
		   NetElabSet *subord_nets,
		   bool &is_primary_tristate);

  //! Having gathered up all the tristate mismatch errors, print them
  //! in sorted order
  void reportTriMismatches();

  //! The design
  NUDesign *mDesign;

  //! The symbol table to lookup elaboration with
  STSymbolTable *mSymtab;

  //! Factory used to manage elaborated flow nodes.
  FLNodeElabFactory *mFlowFactory;

  //! Where to emit messages
  MsgContext *mMsgContext;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! Convenience typedef for mapping elaborated net to set of elaborated net.
  typedef UtMap<NUNetElab*, NetElabSet> NetElabSetMap;

  //! Convenience typedef for iterating over a mapping of elaborated net to set
  //! of elaborated net.
  typedef NetElabSetMap::iterator NetElabSetMapIter;
  typedef NetElabSetMap::const_iterator NetElabSetMapConstIter;

  //! Convenience typedef for mapping elaborated net to set of elaborated flow.
  typedef UtMap< NUNetElab*, FLNodeElabSet > NetNodeElabSetMap;

  //! Convenience typedef for iterating over a mapping of elaborated net to set
  //! of elaborated flow.
  typedef NetNodeElabSetMap::iterator NetNodeElabSetMapIter;

  void fixupOneNetFanouts(NUNetElab *net,
                          FLNodeElabSet & one_net_fanouts,
                          const FLNodeElabList& old_driver_list,
                          NUNetElab::DriverLoop new_drivers);

  bool addNetFlow(NetNodeElabSetMap & net_flow,
                  NUNetElab * net,
                  FLNodeElab * fanout);

  //! Convenience typedef for mapping elaborated net to elaborated flow.
  typedef UtMultiMap< NUNetElab*, FLNodeElab*> NetNodeElabMap;

  //! Convenience typedef for iterating over a mapping of elaborated net to
  //! elaborated flow.
  typedef NetNodeElabMap::iterator NetNodeElabMapIter;
  typedef std::pair<NetNodeElabMapIter,NetNodeElabMapIter> NetNodeElabMapRange;

  //! Convenience typedef for mapping elaborated net to elaborated net.
  typedef UtMap< NUNetElab*, NUNetElab* > NetElabMap;

  //! Convenience typedef for iterating over a mapping of elaborated net to
  //! elaborated net.
  typedef NetElabMap::iterator NetElabMapIter;
  typedef NetElabMap::const_iterator NetElabMapConstIter;

  //! Debug routine to dump sizes of various alias rings.
  void dumpAliasStats();

  //! Map of alias masters to set of subordinate aliases.
  NetElabSetMap mAliases;

  //! Map of nets which are going to be aliased to their fanouts.
  NetNodeElabSetMap mFanouts;

  //! Map of nets which are going to be aliased to their drivers.
  NetNodeElabSetMap mDrivers;

  //! Map of net to their driver(s) which will be eliminated due to aliasing.
  NetNodeElabMap mElimDrivers;

  //! Map of nets which are aliased subordinates to masters.
  NetElabMap mSubords;

  //! Map of nets to their allocation, recursively.
  /*!
   * If a net is a key, then the net is not allocated, but the allocation
   * is defined by the value.  Unless the value is a member of mCannotAllocate.
   */
  NetElabMap mAllocated;

  //! Set of unelaborated nets which cannot possibly be marked as allocated.
  NUNetSet mCannotAllocate;

  //! Flag to print out aliasing as it occurs
  bool mVerbose;

  //! Method for determining whether a given node is a master, returning subbordinate
  Predicate* mPredicate;

  //! Structure to record a tri-mismatch warning we will print after we
  //! collect all of them
  typedef std::pair<STAliasedLeafNode*,UtString> TriMismatch;

  //! Functor to compare tri-mismatch warnings to order them consistently
  struct TriMismatchCmp {
    bool operator()(const TriMismatch& p1, const TriMismatch& p2) {
      int cmp = STAliasedLeafNode::compare(p1.first, p2.first);
      if (cmp == 0)
        cmp = p1.second.compare(p2.second);
      return cmp < 0;
    }
  };

  //! sorted set of tristate mismatches
  typedef UtSet<TriMismatch, TriMismatchCmp> SortedTriMismatchSet;
  SortedTriMismatchSet mTriMismatches;

  typedef UtHashSet<FLNodeElab*> FlowHashSet;
  FlowHashSet mDeadFlows;

  //! The FindPortConnections class must be a friend to add aliases
  friend class FindPortConnections;

  bool mDoAllocation;
  bool mKeepDeadFlows;

  //! Was there a bad allocation found in the sanity check?
  bool mBadAllocation;
};

#endif
