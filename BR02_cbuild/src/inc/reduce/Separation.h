// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef SEPARATION_H_
#define SEPARATION_H_

#include "nucleus/NUDesignWalker.h"

/*!
  \file
  Declaration of vector separation package.
 */

#include "nucleus/NUNetRefMap.h"
typedef NUNetRefMultiMap<SInt32> NUNetRefCntMap;

#include "util/UnionFind.h"
typedef UnionFind<NUNetRefHdl> NUNetRefUnionFind;

class MsgContext;
class SeparationStatistics;
class Fold;

//! Split a vector into multiple, independent parts.
class Separation 
{
public:
  typedef enum { 
    eBlastNets = 1,             /*!< Apply blastNet directive on
                                 * specified nets.*/

    eSeparateVectors = 2,       /*!< Analyze Nucleus to determine
                                 * independent vector components.*/

    eCreateContAssigns = 4      /*!< Create a continuous assign to
                                 * preserve observability for the
                                 * original vector. */
  } ModeFlags;


  //! Constructor
  /*!
   * \param threshold Only separate vectors larger than this value.
   * \param str_cache String cache.
   * \param netref_factory NetRef factory.
   * \param msg_ctx Message context.
   * \param fold Fold object.
   * \param separation_mode Flags determining our operating mode.
   * \param verbose Print out details of successful splits.
   * \param stats Statistics object for summary prints.
   */
  Separation(UInt32            threshold,
             AtomicCache     * str_cache,
             NUNetRefFactory * netref_factory,
             MsgContext      * msg_ctx,
             IODBNucleus     * iodb,
             Fold            * fold,
             UInt32            separation_mode,
             bool              verbose,
             SeparationStatistics * stats);

  //! Destructor
  ~Separation();

  //! Search within this module for separation opportunities.
  void module(NUModule * this_module);

private:
  //! Populate the netref cache from equivalence classes.
  void populateCache();
  void addToCache(NUNetRefSet & net_refs);

  //! Bit-blast nets that are targets of the blastNet directive
  /*! Modifies the netref cache to blast nets in this module.
   *  Returns true if any nets in this module are blasted.
   */
  bool blast(NUModule* this_module);

  //! Determine which nets in mCache are valid candidates.
  void qualify(NUModule* this_module, NUNetSet & nets) const;

  //! Is this net a valid candidate?
  bool qualify(NUModule* this_module, const NUNet * net) const;

  //! Create net_refs to fill in holes for each candidate net.
  void complete(NUNetSet & nets);

  //! Create temporaries and update module.
  void separate(NUModule * this_module, NUNetSet & nets);

  //! Equivalence classes of vector uses.
  NUNetRefUnionFind    * mUnionFind;

  //! Independent uses
  NUNetRefCntMap       * mCache;

  AtomicCache          * mStrCache;
  NUNetRefFactory      * mNetRefFactory;
  MsgContext           * mMsgContext;
  IODBNucleus          * mIODB;
  Fold                 * mFold;

  SeparationStatistics * mStatistics;
  UInt32                 mSeparationMode;
  bool                   mVerbose;

  //! Smaller vectors are discarded as separation candidates.
  UInt32 mSizeThreshold;
};


//! NUDesignWalker callback - Track how specific nets are used.
class SeparationUseCallback : public NUDesignCallback
{
public:
  //! Constructor
  SeparationUseCallback(NUNetRefFactory * netref_factory,
			IODBNucleus * iodb,
                        NUModule * module,
			NUNetRefUnionFind * union_find);

  //! Destructor
  ~SeparationUseCallback();

  //! By default, walk through everything.
  Status operator()(Phase /*phase*/, NUBase * /*node*/) { return eNormal; }

  //! Process only top-level module.
  Status operator()(Phase /*phase*/, NUModule * module);

  //! Process port-connection outputs.
  Status operator()(Phase phase, NUPortConnectionOutput * output);

  //! Process port-connection inputs.
  Status operator()(Phase phase, NUPortConnectionInput * input);

  //! Process port-connection bids.
  Status operator()(Phase phase, NUPortConnectionBid * bid);

  //! Process enabled drivers.
  Status operator()(Phase phase, NUEnabledDriver * driver);

  //! Process tri-init.
  Status operator()(Phase phase, NUTriRegInit * tri_init);

  //! Process assigns
  Status operator()(Phase phase, NUAssign * assign);

  //! Process continuous assigns
  Status operator()(Phase phase, NUContAssign * assign);

  //! Process ops.
  /*!
   * The special case of (identifier==constant) is skipped for
   * purposes of separation. A comparison to a constant should not
   * force all bits into a single vector.
   */
  Status operator()(Phase phase, NUOp * op);

  //! Process rvalue expressions.
  Status operator()(Phase phase, NUExpr * expr);
  
  //! Process lvalues.
  Status operator()(Phase phase, NULvalue * lvalue);

  //! overload
  Status operator()(Phase phase, NUVarselLvalue * lvalue);

  //! Who is walking us?
  void setWalker(NUDesignWalker * walker) { mWalker=walker; }
private:
  //! Remember that these parts were used.
  void remember(NUNetRefSet & net_refs);

  //! Remember that this part was used.
  /*! 
    If some overlapping parts of this net were previously seen,
    combine them into one entry.
  */
  void remember(const NUNetRefHdl & net_ref);

  /*! 
    Take a LHS which is known to be a whole identifier and return the
    single NUNetRefHdl.
  */
  NUNetRefHdl prepareLhs(NULvalue * lhs);

  /*! 
    Take a RHS which is known to be a whole identifier and return the
    single NUNetRefHdl.
  */
  NUNetRefHdl prepareRhs(NUExpr * rhs);

  /*! 
    Take two net refs known to have the same range and perform
    pairwise equate operations.
  */
  void equateBits(const ConstantRange range, 
                  const NUNetRefHdl & a, 
                  const NUNetRefHdl & b);

  //! Do we want to consider this net?
  bool qualify(NUNet * net);

  //! NetRef factory.
  NUNetRefFactory * mNetRefFactory;

  //! IODB
  IODBNucleus * mIODB;

  //! The entry point.
  NUModule * mModule;

  //! Equivalence classes of vector uses.
  NUNetRefUnionFind * mUnionFind;

  //! The walker that will call us -- we need to force walkthrough of certain sub-structures.
  NUDesignWalker * mWalker;
};


//! Collect statistics about vector separation.
class SeparationStatistics
{
public:
  //! Constructor
  SeparationStatistics() { clear(); }

  //! Destructor
  ~SeparationStatistics() {}

  //! Reset all information
  void clear() { mModules=0; mNets=0; mParts=0; }

  //! Increment the number of affected modules.
  void module() { ++mModules; }
  //! Increment the number of affected nets.
  void net() { ++mNets; }
  //! Increment the number of separated parts.
  void part() { ++mParts; }
  //! Print collected information
  void print() const;
private:
  //! The number of modules affected by vector separation.
  SInt64 mModules;
  //! The number of nets we have separated.
  SInt64 mNets;
  //! The number of separated parts.
  SInt64 mParts;
};

#endif
