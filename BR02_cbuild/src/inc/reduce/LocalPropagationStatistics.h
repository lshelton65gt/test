// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _LOCAL_PROPAGATION_STATISTICS_H_
#define _LOCAL_PROPAGATION_STATISTICS_H_

/*!
  Statistics class for propagation optimizations.
 */
class LocalPropagationStatistics 
{
public:
  //! Constructor.
  LocalPropagationStatistics() :
    mModules(0),
    mTasks(0),
    mAlwaysBlocks(0),
    mContAssigns(0),
    mReplacements(0)
  {}

  //! Destructor.
  ~LocalPropagationStatistics() {}

  //! Note that a module has been optimized in some way.
  void module()      { ++mModules; }
  //! Note that a task has been optimized in some way.
  void task()        { ++mTasks; }
  //! Note that an always block has been optimized in some way.
  void alwaysBlock() { ++mAlwaysBlocks; }
  //! Note that a continuous assignment has been optimized in some way.
  void contAssign()  { ++mContAssigns; }
  //! Note that an expression has been rewritten.
  void replacement() { ++mReplacements; }

  //! Print summary information about the optimization.
  void print(const char * optimization_name) const;

private:

  UInt32 mModules;      //!< Number of modules containing optimized constructs.
  UInt32 mTasks;        //!< Number of optimized tasks.
  UInt32 mAlwaysBlocks; //!< Number of optimized always blocks.
  UInt32 mContAssigns;  //!< Number of optimized continuous assignments.
  UInt32 mReplacements; //!< Number of rewritten RHS expressions.
};

#endif
