// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _LOCALCONSTPROP_H_
#define _LOCALCONSTPROP_H_

#include "reduce/LocalPropagationStatistics.h"

class IODBNucleus;
class UD;
class Fold;

//! class RELocalConstProp
/*! This class does local constant propagation within blocks and tasks
 *  in a module. It does not propagate constants between blocks or
 *  into and out of task enables. It may be possible to improve on
 *  some of these restrictions. But the spot to do global constant
 *  propagation is in the global optimizations pass.
 */
class RELocalConstProp
{
public:
  //! constructor
  RELocalConstProp(IODBNucleus* iodbNucleus, UD* ud, Fold* fold, 
                   LocalPropagationStatistics * statistics,
                   bool verbose, bool use_flow_order);

  //! destructor
  ~RELocalConstProp();

  //! propagate constants in a module
  void module(NUModule* module);

  //! propagate constants in an always block
  void alwaysBlock(NUAlwaysBlock* always);

private:
  //! Hide copy and assign constructors.
  RELocalConstProp(const RELocalConstProp&);
  RELocalConstProp& operator=(const RELocalConstProp&);

  // If true, print the blocks that were optimized
  bool mVerbose;

  // requires unelab flow graph
  bool mUseFlowOrder;

  // Helper classes
  IODBNucleus* mIODBNucleus;
  UD* mUD;
  Fold* mFold;

  //! Statistics object.
  LocalPropagationStatistics * mStatistics;
}; // class RELocalConstProp

#endif // _LOCALCONSTPROP_H_
