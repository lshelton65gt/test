// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#ifndef _CONGRUENT_H_
#define _CONGRUENT_H_

#include "util/c_memmanager.h"
#include "util/Util.h"
#include "nucleus/Nucleus.h"

class IODBNucleus;
class CongruentObject;

//! congruence class, used to find modules that have identical, or very similar
//! contents, and eliminate references to all but one of them.
class Congruent {
public:
  CARBONMEM_OVERRIDES

  //! ctor
  Congruent(IODBNucleus*, bool verbose, UInt32 mParamCostFactor,
            const char* congruencyInfoFilename, MsgContext*,
            bool factorConstants);

  //! dtor
  ~Congruent();

  //! compute congruence on a design, substitute instances of
  //! redundant modules, factoring out mismatching constants
  //! as input ports.  Returns true if any changes were made
  bool designModules(NUDesign*);

  //! compute congruence on a module, removing tasks with identical
  //! bodies to another task.  Returns true if any changes were made
  bool moduleTasks(NUModule*);

  //! remember a statement list, and return a canonical statement list
  //! from the ones previously seen
  NUStmtList* insertStmtList(NUStmtList*);

  //! Remember an expression, and return a canonical expressoin from the
  //! ones previously seen.  In the case of expressions, we consider two
  //! nets to be equivalent if they appear in the expressions in the same
  //! order
  const NUExpr* insertExpr(const NUExpr*);

  friend class CongruentObject;
  typedef UtHashMap<NUBase*,CongruentObject*> NucleusObjMap;

  //! Clear out the contents of the Congruent context
  void clear();

private:
  typedef UtVector<CongruentObject*> ObjVec;

  //! Determine the canonical module-groups based on congruence, at the
  //! given hierarchical level.
  /*! The resultant set of canonical modules is placed in mCanonicalObjVec.
   *! The subordinate modules are in the CongruentObject, in the vector mEquivMods.
   *! Return true if any changes were made to the nucleus tree
   */
  bool computeCanonicalMods(const NUInstanceLevels&, UInt32 level);

  //! Determine the canonical task-groups based on congruence, at the
  //! given hierarchical level.
  /*! The resultant set of canonical tasks is placed in mCanonicalObjVec.
   *! The subordinate modules are in the CongruentObject, in the vector mEquivMods.
   *! Return true if any changes were made to the nucleus tree
   */
  bool computeCanonicalTasks(const NUTaskLevels&, UInt32 level);

  //! Subdivide the congruence groups of cm and it's equivs, based on parameter
  //! cost factors, placing the new set of canonical modules into newCanonicalMods.
  void findBestCongruenceGroups(ObjVec* newCanonicalMods, CongruentObject* cm);

  //! Given the canonical modules in mCanonicalObjVec, substitute mismatching constants
  //! with input port nets in the master NUModule
  void convergeCanonicalMods();

  IODBNucleus* mIODB;
  NucleusObjMap* mNucleusObjMap;
  ObjVec* mCanonicalObjVec;
  ObjVec* mAllObjVec;
  bool mVerbose;
  bool mFactorConstants;
  UInt32 mParamCostFactor;
  NUCostContext* mCostContext;

  UtOBStream* mInfoFile;
  MsgContext* mMsgContext;


  struct CongruentCompare {
    CongruentCompare(bool factorConstants)
      : mFactorConstants(factorConstants)
    {
    }

    bool operator()(const CongruentObject* c1, const CongruentObject* c2)
      const;

    bool mFactorConstants;
  };

  // I don't ordinarily like RB-trees, but in this case, there are
  // usually at most thousands of modules, not millions, and the
  // comparator method is complex.  If we need hash/eq methods, then
  // we would need to maintain two complex methods and it would
  // be very easy to think you got them both consistent and correct
  // based on small testcases, and have them fall apart with
  // real world examples.
  typedef UtSet<CongruentObject*,CongruentCompare> CongruentSet;
  CongruentSet* mCongruentSet;

};

#endif
