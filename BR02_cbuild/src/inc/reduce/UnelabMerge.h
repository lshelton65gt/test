// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef UNELABMERGE_H_
#define UNELABMERGE_H_

#include "nucleus/NUNetRefMap.h"

/*!
  \file
  Declaration of unelaborated block merge package.
 */

class AtomicCache;
class UD;
class NUNetRefFactory;
class MsgContext;
class IODBNucleus;
class ArgProc;
class UnelabFanout;
class ReduceUtility;
class UnelabMergeStatistics;

typedef NUUseDefList Chain;
typedef UtVector<Chain> Chains;

typedef NUNetRefMultiMap<UnelabFanout*> NUNetRefFanout;
typedef NUNetRefMultiMap<NUUseDefNode*, HashPointer<NUUseDefNode*, 2>,
                         NUNetCmp> NUNetRefNodeMap;

class UnelabMerge
{
public:
  //! Constructor
  UnelabMerge(AtomicCache * string_cache,
              NUNetRefFactory *netref_factory,
              MsgContext * msg_ctx,
              IODBNucleus * iodb,
              ArgProc* args,
              bool verbose,
              UnelabMergeStatistics *stats);

  //! Destructor
  ~UnelabMerge();

  //! Walk a module and perform unelaborated merging.
  void module(NUModule * this_module);

private:
  //! String cache
  AtomicCache *mStrCache;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! Message context.
  MsgContext *mMsgContext;

  //! IODB.
  IODBNucleus * mIODB;

  UD * mUD;

  UnelabMergeStatistics * mStatistics;

  //! Be verbose as processing occurs?
  bool mVerbose;
};

class UnelabModuleMerge 
{
public:
  UnelabModuleMerge(NUModule * this_module,
                    AtomicCache * string_cache,
                    NUNetRefFactory *netref_factory,
                    MsgContext * msg_ctx,
                    IODBNucleus * iodb,
                    UnelabMergeStatistics * statistics,
                    bool verbose);
  
  ~UnelabModuleMerge();

  enum ChainMergeType {
    eUnknownMerge = 0x0,
    eFanoutMerge  = 0x1,
    eFaninMerge   = 0x2,
    eCreateMerge  = 0x4,
    eFanoutOrCreateMerge = eFanoutMerge|eCreateMerge,
    eFaninOrCreateMerge = eFaninMerge|eCreateMerge,
    eAnyMerge = eFanoutMerge|eFaninMerge|eCreateMerge
  };

  bool merge(ChainMergeType merge_type);
private:

  void buildFanout();
  void addFanout(NUUseDefNode * use_def, NUNetRefSet & uses);
  void addFanout(const NUNetRefHdl & net_ref, NUUseDefNode * use_def);
  UnelabFanout * getFanout(const NUNetRefHdl & net_ref);
  void getFanout(const NUNetRefHdl & net_ref, NUUseDefSet & fanout);
  void clearFanout();
  void printFanout();

  void findChains();
  void findChainCandidates();
  void buildChains(NUNet::QueryFunction queryFn);

  void gatherFanoutChain(const NUNetRefHdl & starting_net_ref,
			 Chain & chain);
  void gatherFaninChain(Chain & chain);
  void clearChains();
  void printChains();
  void printChain(Chain & chain, ChainMergeType merge_type=eUnknownMerge);

  bool mergeChains();
  NUAlwaysBlock * findFanoutAlways(Chain & chain, bool no_sequential);
  NUAlwaysBlock * findFaninAlways(Chain & chain, bool no_sequential);

  //! Return true if the chain is considered dangerous for merging into sequential blocks.
  /*!
   * If any element in the chain defines either an output net or an edge, return true.
   */
  bool isDangerousChain(const Chain & chain) const;

  //! Return true if the chain defines an output/inout.
  bool definesOutputNet(const Chain & chain) const;

  //! Return true if the chain defines an observable net.
  bool definesProtectedObservableNet(const Chain & chain) const;
  
  //! Return true if the chain defines a depositable net.
  bool definesProtectedMutableNet(const Chain & chain) const;

  //! Return true if the chain defines an edge net.
  bool definesEdgeNet(const Chain & chain) const;

  //! Return true if the chain uses an edge net.
  bool usesEdgeNet(const Chain & chain) const;

  //! Does this always block define any 2-dimensional net?
  bool alwaysDefinesMemory(NUAlwaysBlock * always);

  //! Does the chain contain any incomplete defs?
  /*! 
    This usually means that we've got some dynamic select on the LHS
    and shouldn't merge into a sequential block.
   */
  bool hasIncompleteDef(const Chain & chain) const;

  bool createsCycle(Chain & chain, NUAlwaysBlock * always);

  void markChainInaccurate(Chain & chain);
  void addChainToBlock(NUAlwaysBlock * always,
		       Chain & chain,
		       ChainMergeType merge_type);
  void promoteUD(Chain & chain, NUAlwaysBlock * always);
  void promoteUD(NUUseDefNode * node, NUAlwaysBlock * always);

  NUBlockingAssign * transformAssign(NUUseDefNode * use_def);
  void markChainForRemoval(Chain & chain);

  void update();

  //! Current module
  NUModule * mModule;

  //! String cache
  AtomicCache *mStrCache;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! Message context.
  MsgContext *mMsgContext;

  //! IODB.
  IODBNucleus * mIODB;

  UnelabMergeStatistics * mStatistics;

  //! Noisy?
  bool mVerbose;

  ChainMergeType mMergeType;

  NUNetRefFanout mFanoutMap;
  
  ReduceUtility * mUtility;

  NUNetRefNodeMap mStartingPoints;

  Chains mChains;

  NUUseDefSet mRemovedUseDefs;
};


class UnelabMergeStatistics
{
public:
  UnelabMergeStatistics() :
    mIterations(0) 
  {}
  ~UnelabMergeStatistics() {}

  void mark(SInt32 size);
  void print(const char * label) const;
  void add(UnelabMergeStatistics & other);
  void iteration() { ++mIterations; }
  void clear();
  
private:
  UInt64 mIterations;
  typedef UtMap<SInt32,SInt64> Histogram;
  Histogram mHistogram;
};

//! Generic structure used to model single-module unelaborated fanout relationships.
class UnelabFanout
{
public:
  //! Constructor.
  UnelabFanout(const NUNetRefHdl & net_ref) :
    mNetRef(net_ref)
  {}

  //! Destructor.
  ~UnelabFanout() {};

  //! Return the net.
  const NUNetRefHdl & getNetRef() const { return mNetRef; }

  //! Add a new usedef as a fanout of mNet.
  void addFanout(NUUseDefNode * useDef) { mFanout.insert(useDef); }

  //! Add all fanout to _useDefSet_
  void getFanout(NUUseDefSet & useDefSet) { useDefSet.insert(mFanout.begin(),mFanout.end()); }

  //! How many fanout?
  SInt32 fanoutCount() const { return mFanout.size(); }

  //! Display the net and its fanout.
  void print();

  //! compare two UnelabFanouts for sorted printing in NUNetRefMultiMap
  bool operator<(const UnelabFanout&) const;

private:
  //! The net in question -- a fanin to all use def nodes in _mFanout_
  NUNetRefHdl mNetRef;

  //! The fanout set of _mNet_
  NUUseDefSet mFanout;
};

#endif
