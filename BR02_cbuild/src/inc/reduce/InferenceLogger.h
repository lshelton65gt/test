// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef INFERENCE_LOGGER_H_
#define INFERENCE_LOGGER_H_

#include "reduce/OccurrenceLogger.h"
#include "reduce/Inference.h"
#include "vectorise/ConcatAnalysis.h"

class NUAssign;
class NUNet;
class NULvalue;
class NUModule;

//! \class InferenceLogger
/*! Log file interface for inference */

class InferenceLogger {
public:

  InferenceLogger () : mLog (NULL) {}
  ~InferenceLogger () {}

  //! bind this instance to a log file
  void bind (OccurrenceLogger *log = NULL) { mLog = log; }

  //! set options on the log
  void set (const UInt32 options);

  //! If the log file is active, then output an entry.
  void operator () (const UInt32, const NUAssign *);
  void operator () (const UInt32, const NUAssign *, const ConcatAnalysis::NetMap *);
  void operator () (const UInt32, const NUAssign *, const NUAssign *, const UInt32, const ConcatAnalysis::NetMap *);
  void operator () (const UInt32, const NUAssign *, const NULvalue *);
  void operator () (const UInt32, const NUAssign *, const UInt32);
  void operator () (const UInt32, const NUAssign *, const UInt32, const UInt32);
  void operator () (const UInt32, const NUExpr *);
  void operator () (const UInt32, const NUModule *);
  void operator () (const UInt32, const NUModule *, const UInt32, const UInt32);
  void operator () (const UInt32, const NUModule *, const UInt32);
  void operator () (const UInt32, const NUModule *, const UInt32, const ConcatAnalysis::NetMap *);
  void operator () (const UInt32, const NUModule *, const ConcatAnalysis::NetMap *);
  void operator () (const UInt32, const NUModule *, const NUModule *, const ConcatAnalysis::NetMap::Edge *, const NUNet *, const NUNet *);
  void operator () (const UInt32, const NUModule *, const NUModule *, const ConcatAnalysis::NetMap::Edge *, const OccurrenceLogger::Visible *, const OccurrenceLogger::Visible *);
  void operator () (const UInt32, const NUModule *, const ConcatAnalysis::NetMap::Node *);
  void operator () (const UInt32, const NUModule *, const NUNet *);
  void operator () (const UInt32, const NUModule *, const NUNet *, SourceLocator &loc);
  void operator () (const UInt32, const NUModuleInstance *);
  void operator () (const UInt32, const NUModuleInstance *, const NUPortConnection *);
  void operator () (const UInt32, const NUModuleInstance *, const UInt32, const UInt32);
  void operator () (const UInt32, const NUModuleInstance *, const UInt32);
  void operator () (const UInt32, const NUNet *, const NUExpr *, const NUExpr * = NULL);
  void operator () (const UInt32, const NUNet *, const NULvalue *, const NULvalue * = NULL);
  void operator () (const UInt32, const NUNet *, const NUNet *);
  void operator () (const UInt32, const SourceLocator &loc, const NUModule *, const NUNet *);
  void operator () (const UInt32, const UInt32);
  void operator () (const UInt32, const UInt32, const NUModule *);

private:
  OccurrenceLogger *mLog;
};

#endif
