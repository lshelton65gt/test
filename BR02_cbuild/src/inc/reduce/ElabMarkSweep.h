// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef ELABMARKSWEEP_H_
#define ELABMARKSWEEP_H_

#include "flow/Flow.h"

/*!
  \file 
  Declaration of the elaborated flow mark/sweep package.
*/

class STSymbolTable;

//! ElabMarkSweep class
/*!
 * Class to perform a mark/sweep over elaborated flow.
 */
class ElabMarkSweep 
{
public:
  //! Constructor
  /*!
    \param dfg_factory DFG Elab Factory
    \param symtab Symbol table
    \param iodb IODB
   */
  ElabMarkSweep(FLNodeElabFactory * dfg_factory,
                STSymbolTable *symtab) :
    mFlowFactory(dfg_factory),
    mSymtab(symtab)
  {}

  //! Callback for sweep
  class SweepCallback
  {
  public:
    SweepCallback() {}
    virtual void operator()(FLNodeElab * flnode, bool is_dead) = 0;
    virtual ~SweepCallback() {}
  };

  //! Mark the reachable elaborated flow for the design
  void mark(NUDesign * this_design);

  //! Sweep the flow, callback for all flow
  void sweep(SweepCallback &callback);

  //! Destructor
  ~ElabMarkSweep() {}

  //! Clear the mark on all flow
  static void clearMarkAllFlow(FLNodeElabFactory * flow_factory);

private:
  //! Hide copy and assign constructors.
  ElabMarkSweep(const ElabMarkSweep&);
  ElabMarkSweep& operator=(const ElabMarkSweep&);

  //! Place a mark on all reachable flow.
  void markDesignFlow(NUDesign *design);

  //! DFG Elab Factory
  FLNodeElabFactory *mFlowFactory;

  //! Symbol table
  STSymbolTable *mSymtab;
};

#endif
