// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Definition of concat rewrite phase.
*/

#ifndef CONCATREWRITE_H_
#define CONCATREWRITE_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetRefMap.h"
#include "util/ConstantRange.h"

class MsgContext;
class ConcatStatistics;
class AtomicCache;
class IODBNucleus;
class Fold;
class ArgProc;
class AllocAlias;

// Forward declare
template<typename UserEdgeData, typename UserNodeData> class GenericDigraph;

//! Class to rewrite concats to simpler forms
/*!
 * This class tries to rewrite assigns of the form:
 *  {a, b, c} = {d, e, f};
 * Into:
 *  a = d;
 *  b = e;
 *  c = f;
 *
 * This rewrite is lower cost because we don't have to do the shifting
 * involed with a concat.
 */
class ConcatRewrite
{
public:
  //! constructor
  ConcatRewrite(NUNetRefFactory *netref_factory,
		MsgContext *msg_context,
		AtomicCache *str_cache,
		IODBNucleus *iodb,
                ArgProc * args,
		ConcatStatistics *stats,
                AllocAlias* alias_query,
		bool updateUD);

  //! destructor
  ~ConcatRewrite();

  //! Traverse the module, rewriting assigns
  void module(NUModule *this_module);

  //! Traverse the proc, rewriting assigns.
  bool structuredProc(NUStructuredProc * proc);

  //! Helper class to keep track of the parts of an assign:{assign_type,lhs,rhs}
  class AssignComponents
  {
  public:
    AssignComponents(NULvalue* lhs, NUExpr* rhs, NUType assign_type):
      mLhs(lhs),
      mRhs(rhs),
      mAssignType(assign_type)
    {
      NU_ASSERT( ((assign_type == eNUBlockingAssign ) or
                  (assign_type == eNUNonBlockingAssign ) or
                  (assign_type == eNUContAssign ) ), lhs );
    }

    ~AssignComponents() {}

    NULvalue* getLhs() const { return mLhs; }
    NUExpr* getRhs() const { return mRhs; }
    NUType getAssignType() const { return mAssignType; }

  private:
    NULvalue* mLhs;
    NUExpr* mRhs;
    // supported types: eNUBlockingAssign eNUNonBlockingAssign eNUContAssign
    NUType mAssignType;
  };

  typedef UtList<AssignComponents> AssignComponentsList;
  typedef GenericDigraph<void*,AssignComponents*> ConcatPartGraph;
  typedef NUNetRefMultiMap<AssignComponents*> ConcatPartDefs;

private:
  //! Hide copy and assign constructors.
  ConcatRewrite(const ConcatRewrite&);
  ConcatRewrite& operator=(const ConcatRewrite&);

  //! Try to rewrite assigns in tasks and functions.  Return true if anything was done.
  bool tfs(NUModule *module);

  //! Try to rewrite continuous assigns.  Return true if anything was done.
  bool contAssigns(NUModule *module);

  //! Try to rewrite the given continuous assign.  Return true if anything was done.
  bool contAssign(NUModule *module, NUContAssign *assign);

  //! Return true if this assign is a candidate for concat rewriting.
  bool testAssign(NUAssign *assign);

  //! Find the NUExpr that is not a simple wrapper concat.
  NUExpr * findRealRvalue(NUExpr * rhs);

  //! Find the NULvalue that is not a simple wrapper concat.
  NULvalue * findRealLvalue(NULvalue * lhs);

  //! Return true if this assign has a vector lhs which is a candidate for rewriting
  bool testAssignVectorLhs(NUAssign *assign);

  //! Return true if this assign has a concat lhs which is a candidate for rewriting
  bool testAssignConcatLhs(NUAssign *assign);

  //! Return true if the RHS expression is a valid rewrite candidate with the given LHS.
  bool testConcatLhsExprRhs(const NUConcatLvalue * lhs_concat,
                            NUExpr * rhs,
                            bool zExpr);

  //! Return true if the RHS op is a valid rewrite candidate with the given LHS.
  bool testConcatLhsBinaryOpRhs(const NUConcatLvalue * lhs_concat,
                                NUBinaryOp * op,
                                bool zExpr);

  //! Return true if the RHS concat is a valid rewrite candidate with the given LHS.
  bool testConcatLhsConcatRhs(const NUConcatLvalue * lhs_concat,
                              NUConcatOp * rhs_concat,
                              bool zExpr);

  //! Return true if the def set overlaps the use set of the given assign.
  bool testAssignOverlap(NUAssign *assign);

  //! Rewrite the given continuous assign, place new assigns into the assign list.
  bool rewriteContAssign(NUModule *module,
			 NUContAssign *assign,
			 NUContAssignList & replacements);

  //! Try to rewrite assigns in structured procs.  Return true if anything was done.
  bool structuredProcs(NUModule *module);

  //! Try to rewrite assigns in the block.  Return true if anything was done.
  bool blockScope(NUBlockScope *block);

  //! Try to rewrite assigns in the stmt list.  Return true if anything was done.
  bool stmtList(NUStmtList &stmts, bool looping_context);

  //! Try to rewrite assigns at/under the given stmt.  Return true if anything was done.
  bool stmt(NUStmt *stmt, bool looping_context, NUStmtList & replacements);

  //! Try to rewrite assigns in the if stmt.  Return true if anything was done.
  bool ifStmt(NUIf *stmt);

  //! Try to rewrite assigns in the case stmt.  Return true if anything was done.
  bool caseStmt(NUCase *stmt);

  //! Try to rewrite assigns in the for stmt.  Return true if anything was done.
  bool forStmt(NUFor *stmt);

  //! Rewrite the given continuous assign, place new assigns into the assign list.
  bool rewriteAssign(NUAssign *assign, bool looping_context, NUStmtList &replacements);

  //! Return a usable rvalue which is the wanted_part of the given rhs
  NUExpr *getAbsoluteIndexedRvalueCopy(NUExpr *rhs, const ConstantRange &wanted_part);

  //! Return a usable lvalue which is the wanted_part of the given lhs
  /*!
   * Currently only lhs which are whole identifiers are supported.
   */
  NULvalue *getAbsoluteIndexedLvalueCopy(NULvalue *lhs, const ConstantRange &wanted_part);

  //! Break up the given assign, create new lhs, rhs pairs
  bool createNewParts(NUAssign *assign, bool looping_context,
                      AssignComponentsList &new_parts_list);

  void cleanupNewParts(AssignComponentsList & new_parts_list);

  bool checkNewParts(AssignComponentsList & new_parts_list);
  bool noNewPartsOverlap(AssignComponentsList & new_parts_list);
  bool orderNewParts(AssignComponentsList & new_parts_list);

  void createNewPartsConcatLvalues(AssignComponentsList &new_parts_list);

  //! Break up the assign, the given assign must have a lhs which is a concat
  bool createNewPartsConcatLvalue(NUType assign_type, NULvalue * lhs, NUExpr * rhs, 
                                  AssignComponentsList &new_parts_list);

  void createNewPartsConcatRvalues(AssignComponentsList &new_parts_list);

  //! Break up the assign, the given assign must have a rhs which is a concat
  bool createNewPartsConcatRvalue(NUType assign_type, NULvalue * lhs, NUExpr * rhs,
                                  AssignComponentsList &new_parts_list);

  void createNewPartsVarselLvalue(NUType assign_type, NULvalue * lhs, NUExpr * rhs, 
                                  bool looping_context, AssignComponentsList &new_parts_list);

  void createNewContAssigns(NUModule * module,
                            NUContAssign * assign,
                            AssignComponentsList & new_parts_list,
                            NUContAssignList & replacements);

  void createNewAssigns(NUAssign * assign, 
                        AssignComponentsList & new_parts_list,
                        NUStmtList & replacements);

  //! Netref factory
  NUNetRefFactory *mNetRefFactory;

  //! Message context
  MsgContext *mMsgContext;

  //! String cache
  AtomicCache *mStrCache;

  //! IODB
  IODBNucleus *mIODB;

  //! args
  ArgProc* mArgs;

  //! Stats keeper
  ConcatStatistics *mStats;

  //! Do we need to update UD information?
  bool mUpdateUD;

  //! Fold object; used for simplifying nested partsels.
  Fold * mFold;

  //! Current scope (needed for temp variables)
  NUScope* mScope;

  //! Used to test for overlap between nets
  AllocAlias* mAliasQuery;
};

//! Statistics for concat rewrite phase
class ConcatStatistics
{
public:
  ConcatStatistics() :
    mModules(0),
    mBlockingAssigns(0),
    mNonblockingAssigns(0),
    mContAssigns(0)
  {}

  ~ConcatStatistics() {}

  void module() {++mModules; }
  void blockingAssign() {++mBlockingAssigns;}
  void nonblockingAssign() {++mNonblockingAssigns;}
  void contAssign() {++mContAssigns;}
  void print() const;

private:
  UInt64 mModules;
  UInt64 mBlockingAssigns;
  UInt64 mNonblockingAssigns;
  UInt64 mContAssigns;
};

#endif
