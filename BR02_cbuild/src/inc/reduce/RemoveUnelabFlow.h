// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef REMOVEUNELABFLOW_H_
#define REMOVEUNELABFLOW_H_

#include "flow/FLFactory.h"

/*!
  \file
  Declaration of flow removal package.
 */

//! Remove unelab flow class
/*!
 * This class gets rid of unelaborated flow in the factory.
 * Nucleus is not deleted, but the drivers are removed from the NUNet's.
 */
class RemoveUnelabFlow
{
public:
  //! constructor
  RemoveUnelabFlow(FLNodeFactory *dfg_factory) :
    mFlowFactory(dfg_factory)
  {}

  //! destructor
  ~RemoveUnelabFlow() {}

  //! Remove the flow
  void remove();

private:
  //! Hide copy and assign constructors.
  RemoveUnelabFlow(const RemoveUnelabFlow&);
  RemoveUnelabFlow& operator=(const RemoveUnelabFlow&);

  //! DFG Factory
  FLNodeFactory *mFlowFactory;
};

#endif
