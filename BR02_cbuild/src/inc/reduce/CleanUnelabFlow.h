// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef CLEANUNELABFLOW_H_
#define CLEANUNELABFLOW_H_

#include "flow/Flow.h"
#include "nucleus/Nucleus.h"
#include "reduce/UnelabMarkSweep.h"

/*!
  \file
  Declaration of unelaborated flow cleanup package.
 */

class IODBNucleus;

//! CleanUnelabFlow class
/*!
 * Class to find any dead flow and remove it.
 */
class CleanUnelabFlow
{
public:
  //! constuctor
  /*
    \param dfg_factory DFG Factory
    \param iodb IODB
    \param verbose If true, will be verbose about flow cleanup.
   */
  CleanUnelabFlow(FLNodeFactory *dfg_factory,
		  IODBNucleus *iodb,
		  bool start_at_all_nets,
		  bool verbose) :
    mFlowFactory(dfg_factory),
    mIODB(iodb),
    mStartAtAllNets(start_at_all_nets),
    mVerbose(verbose),
    mDeadFlowFound(false)
  {}

  //! Walk this design, mark/sweep, remove dead flow.
  void design(NUDesign *this_design);

  //! Delete dead flow for the design, assumed that mark has already occurred.
  void deleteDeadFlow();

  //! destructor
  ~CleanUnelabFlow() {}

private:
  //! Hide copy and assign constructors.
  CleanUnelabFlow(const CleanUnelabFlow&);
  CleanUnelabFlow& operator=(const CleanUnelabFlow&);

  //! Allow the callback to use the removeFlow function.
  class Callback;
  friend class Callback;

  //! Callback class for sweep
  class Callback : public UnelabMarkSweep::SweepCallback
  {
  public:
    Callback(CleanUnelabFlow *obj) : mObj(obj) {}
    void operator()(FLNode *flnode, bool is_dead) { mObj->removeFlow(flnode, is_dead); }
    ~Callback() {}

  private:
    CleanUnelabFlow* mObj;
  };

  //! Callback from sweep
  void removeFlow(FLNode *flnode, bool is_dead);

  //! DFG Factory
  FLNodeFactory *mFlowFactory;

  //! IODB
  IODBNucleus *mIODB;

  //! Use all nets or outputs/observable as starting points?
  bool mStartAtAllNets;

  //! Be verbose?
  bool mVerbose;

  //! Any dead flow found?
  bool mDeadFlowFound;
};

#endif
