// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Constant fold expression trees.
 */

#ifndef FOLD_H_
#define FOLD_H_

class ArgProc;
class AtomicCache;
class ConstantRange;
class ESPopulateExpr;
class FoldI;
class IODBNucleus;
class MsgContext;
class SourceLocator;

#include "nucleus/Nucleus.h"

enum FoldFlags {
  eFoldUDValid = 1,    /*!< UD information has been computed and can
                        be used for constant propagation. */

  eFoldPreserveReset = 2, /*!< Avoid any fold operations that would hurt
                        reset detection.  This is only set for Folding
                        during populate before reset recognition. */

  eFoldUDKiller = 4,    /*!< Statement folding that destroys UD is
                        permitted.  Particularly if-then-else and case
                        statements can be transformed. */

  eFoldComputeUD = 8,   /*!< Fold is responsible for recomputing UD if
                        it changes anything when doing a call to
                        Flow::module().  If the Fold caller calls
                        lower level entrypoints, they are still responsible
                        for recomputing UD if anything is changed. */

  eFoldIgnoreXZ = 16,  /*!< Treat XZ as 0 and fold constant expressions
                        containing X or Z. */

  eFoldBottomUp = 32,   /*!< This Fold pass is being used to transform
                          expressions in a bottom-up order. e.g. we
                          are processing the leaves and folding them
                          before we process the root of a statement or
                          expression.

                          This flag keeps associative reordering from
                          being applied.  We only want to do it on
                          top-down walks so that we fold all the terms
                          at once.  This is critically important when
                          we're folding bit-field operations
                        */

  eFoldAggressive = 64, /*!< Do transforms that would cause cycle
                        detection to get confused.  This is probably
                        only useful in codegen.  Any earlier phase
                        might think there were dependencies that don't
                        really exist if Fold transforms something like
                        
	                        if (x[3:2]) ....
                        into
				if (x & 6) ...

                        because UD will think that there is now a use
                        of ALL of 'x', not just bits [3:2]
                      */

  eFoldBDDMux = 128,   /*! Allow BDD transformations to generate MUXes
                           rather than ?:, if they cost less estimated
                           instructions.  Note MUXes reference their
                           select twice, but ?: has an 8-instruction
                           estimated overhead for branch mispredict.
                           There are some optimizations that
                           prefer ?:, such as control merging
                        */

  eFoldCaseEquivalent = 256, /*! Do the fold of equivalent case items,
			        N-square/2 algorithm, applied once in 
				early localAnalysis.
			     */
  eFoldNoStmtDelete = 512,  /*! Do not delete any statements that could
                              participate in cycles after schedule has run. */
  eFoldKeepExprs = 1024,    /*! Do not clear expr cache */
  eFoldSimplifyArithmetic = 2048, /*! Simplify arithmetic expressions */
  eFoldNoNetConstants = 4096  /*! Do not replace nets with continously
                                assigned constants, which is bad for port exprs  */
};

//! Class to drive the walker
/*!
 * This class is able to walk the whole design and visit every NUExpr
 * for the whole design.
 */
class Fold
{
public:
  //! constructor
  Fold(NUNetRefFactory *netref_factory,
       ArgProc* args,
       MsgContext *msg_ctx,
       AtomicCache *cache,
       IODBNucleus *iodb,
       bool verbose,
       UInt32 flags             // set of FoldFlags
       );

  //! destructor
  ~Fold();

  //! Generalized expression folder
  /*!
   * \return constant-folded and canonicalized expression tree.
   */
  NUExpr* fold(NUExpr*, UInt32 flagsOn=0, UInt32 flagsOff=0);

  //! Generalized lhs folder
  /*!
   * \return constant-folded lhs.
   */
  NULvalue* fold(NULvalue*, UInt32 flagsOn=0, UInt32 flagsOff=0);

  //! Clear the expression factory that is owned by this Fold
  void clearExprFactory();

  //! Make the Fold system share this expression factory.
  /*!
   *! The user must ensure that this expression factory lasts longer than
   *! the Fold object.
   */
  void shareExprFactory(NUExprFactory*);

  //! Fold an expression that's owned by a shared expression factory
  /*!
   *! You must call shareExprFactory before calling this method.  And
   *! the expression passed in must be a member of the shared factory.
   */
  const NUExpr* foldFactoryExpr(const NUExpr*, UInt32 flagsOn=0, UInt32 flagsOff=0);

  //! Constant fold the module
  /*!
   * Iterate through all the different top-level drivers which exist
   * in a module.  For each driver, visit the statements and fold expression.
   *
   * Recurse to handle submodules if recurse is true.
   */
  void module(NUModule *module, bool recurse = true);

  //! Walk a design
  void design(NUDesign *design, UInt32 flagsOn=0, UInt32 flagsOff=0);

  //! Walk a structuredProc and fold it
  void structuredProc (NUStructuredProc *proc);

  //! Walk a block and fold it
  void block(NUBlock* the_block);

  //! Walk a task and fold it.
  void tf (NUTF *tf);

  //! Fold a statement, returning the new statement
  NUStmt* stmt(NUStmt *stmt, UInt32 flagsOn=0, UInt32 flagsOff=0);

  //! Test
  bool isChanged () const;

  //! Reset
  void clearChanged();

  //! Push the scope  (Visible to make it possible to fold less than a design...)
  void pushScope(NUBlockScope*);

  //! pop a scope
  void popScope();

  //! create a pre-folded bit-select of a constant
  NUExpr* createBitsel(const NUExpr* ident, const NUExpr* sel,
                       const SourceLocator& loc);

  //! create a pre-folded part-select
  NUExpr* createPartsel(const NUExpr* ident, const ConstantRange& range,
                        const SourceLocator& loc);

  //! change whether verbosity is desired, return old value
  bool putVerbose(bool flag);

  //! change whether tracing is desired, return previous value
  bool putTraceFold(bool flag);

  //! specify value indicating if tracing of BDDs used by fold is desired, returns previous value
  UInt32 putTraceBDDFold(UInt32 verboseFlags);

private:
  // hide these operators
  Fold (const Fold&);
  Fold& operator=(const Fold&);

  FoldI* mFold;
};

#endif
