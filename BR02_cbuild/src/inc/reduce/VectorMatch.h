// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef VECTOR_MATCH_H_
#define VECTOR_MATCH_H_

#include "util/UtString.h"
#include "nucleus/Nucleus.h"

/*!
  \file
  Declaration of vector match package.
 */

#ifndef REGEX_GROUP_BUG
// Harry Spencer's regex package and certain versions of regex
// included in the GNU glibc library do not produce the expected match
// sets when multiple groups are used.

// I believe this is documented in the GNU gnats bug database as bug
// 1570. It appears to be fixed in later glibc versions (post RH7.3).
// The Spencer package has not fixed this bug.

// http://bugs.gnu.org/cgi-bin/gnatsweb.pl/full?database=glibc&cmd=view+audit-trail&pr=1570
#define REGEX_GROUP_BUG 1
#endif

class NUNetRefFactory;
class MsgContext;
class AtomicCache;
class IODBNucleus;
class ArgProc;

class VectorMatchStatistics;

extern "C" {
  // The first declaration is visible to VectorMatch.cxx, where a
  // valid definition of regmatch_t is available. The second
  // declaration is visible to other consumers of this header file (a
  // definition of regmatch_t is not available).
#ifdef _REGEX_H_
  typedef regmatch_t REMatch;
#else
  struct REMatch;
#endif
}

//! Find non-vector nets which appear to be bit-blasted and reconstitute a vector.
/*!
  This work is driven by Bug 490, which notes two situations. 

  \verbatim
  The first testcase contains bit-blasted nets at the module boundary.
  Solving this problem is secondary because it requires a bit more
  effort to properly cleanup -- if we change the port connections, we
  need to update every instantiation. Any name-destruction will be
  more apparant, as the boundaries will completely change (maintaining
  no tie to the original names).

  Example:
    module mem(addr3,addr2,addr1,addr0);
      input addr3,addr2,addr1,addr0;
      wire [3:0] wa = {addr3,addr2,addr1,addr0};

  The second testcase contains a number of wires which appear to be
  easily vectorized. They are local to the module, have obvious name
  and index parts, etc.

  Example:
    wire load_0, load_1, load_2;
    assign load_0 = in_0 | eq_0;
    assign load_1 = in_1 | eq_1;
    assign load_2 = in_2 | eq_2;

  Approach:

  This optimization will be applied very early in the Cbuild process. 

  1. Discovery

    Use a regular expression engine to separate the part of the name
    which appears to designate the name from the part which appears to
    be an index. Something like the following seems appropriate:

      ^([a-zA-Z0-9_]+?)([0-9]+)$

    Something a little less aggressive is also as it looks more
    explicitly like a vector:

      ^([a-zA-Z0-9_]+?)_([0-9]+)$

    The first group is coded to match alpha-numeric sequences in a
    non-greedy fashion. This allows the second group to match all
    trailing numeric characters; none of them will be sucked into the
    first group.

    We will also need to handle flattened names, like:
      $temp_INST_0_VARIABLE

    For now, this search process will be limited to module locals.
    There is nothing preventing us from recursing through named
    blocks, functions, tasks, or other scoping structures.

  2. Grouping

    When the name is distinguished from the index, group all
    like-named nets togther.

  3. Validation

    Once all nets have been grouped into their name groups, create
    appropriately sized vectors if the following conditions hold:

    A. All bits between the max and min index are represented
    B. All bits share the same declaration mask
    C. No bit is marked by the user as protected (observeNet or
       otherwise)

    Question: Should we worry about situations where part or all the
    vector contains '1'bz' in its definition? If only some bits are
    driven by z, creating a vector will cause all bits to be
    tristatable.

    Question: Should there be a lower bound where we do not create
    vectors? Is the runtime cost of a multi-bit construct ever higher
    than multiple single-bit constructs?

    Question: Should we look at their driving functions and determine
    if creating vectors will do any good before going ahead and
    generating them? If different driving functions can cause us not to
    create a vector, what qualifies that driving function as different?
    What percentage of bits are allowed to have "different" driving
    functions before we decide to not create the vector?

  4. Update

    All references to the single-bits will be replaced with the
    appropriate NUBitselLvalue or NUBitselRvalue. We need to support
    the following situations:

      wire a_0;
      a_0 = in;       // NUIdentLvalue; becomes NUBitselLvalue
      a_0[0] = in;    // NUBitselLvalue; remains NUBitselLvalue
      a_0[0:0] = in;  // NUPartselLvalue; becomes NUBitselLvalue.

      out = a_0;      // NUIdentRvalue; becomes NUBitselRvalue
      out = a_0[0];   // NUBitselRvalue; remains NUBitselRvalue
      out = a_0[0:0]; // NUPartselRvalue; becomes NUBitselRvalue.

    These transformation rules are valid if the net appears in a
    statement, function call, task enable, module instance, etc.

  5. Cleanup

    All single-bit nets need to be removed from their scope.

  ------------------------------------------------------------

  For example, if I name my nets

      bit0_regA, bit1_regA, bit2_regA.... 

  ...

  These would easily be swallowed by the hand-crafted parser I used
  "in the past", which would break these down to:

      base    = "bit"
      suffix  = "_regA"
      indices = (0, 1, 2)

  I never saw that specific case, although I saw plenty of a_0_ and
  \a[0] , which are both generated by Synopsys using some switch or
  another.

  Of course "in the past" I saw a lot of gate level netlists.  But at
  Carbon we are going to see them too.  We will only see a gate level
  netlists for a highly sensitive DVD core at ATI, VGA graphics cores,
  etc.

  ------------------------------------------------------------

  One case to be aware of is that all the unnamed synthesized nets
  generated by Synopsys (e.g. expression intermediates) get names like
  n2782.  In the past we made a huge synthetic vector (e.g. n<10738:1>)
  for all those, but that didn't have any real drawback, as users would
  generally never want to look at those in waveforms anyway.  But if
  that has a drawback for us we might want to set an upper bound on the
  size of vector
  \endverbatim
 */
class VectorMatch
{
public:
  //! Constructor
  VectorMatch(AtomicCache * str_cache,
	      NUNetRefFactory * netref_factory,
	      MsgContext * msg_context, 
	      IODBNucleus * iodb,
	      ArgProc *args,
	      bool verbose,
	      bool portVerbose,
	      VectorMatchStatistics *stats);

  //! Destructor
  ~VectorMatch();

  //! Walk a module
  void module(NUModule * one);

private:  
  AtomicCache     * mStrCache;
  NUNetRefFactory * mNetRefFactory;
  MsgContext      * mMsgContext;
  IODBNucleus            * mIODB;
  ArgProc         * mArgs;

  VectorMatchStatistics * mStatistics;
  bool mVerbose;
  bool mPortVerbose;
};


//! Utility class to hold the name and indices of one candidate vector.
class VectorCandidate
{
public:
  //! Constructor
  VectorCandidate(UtString & name) :
    mName(name),
    mNet(NULL),
    mPort(false)
  {}

  //! Destructor
  ~VectorCandidate() {}

  //! Get the name of this vector candidate
  const UtString & getName() const { return mName; }

  typedef UtMap<SInt32,NUNet*> IndexMap;

  typedef IndexMap::iterator iterator;
  typedef IndexMap::const_iterator const_iterator;
  typedef IndexMap::value_type value_type;

  //! Add an index as associated with this name
  void insert(SInt32 index, NUNet *net);

  //! Clear all indices.
  void clear() { mIndices.clear(); }

  int size() const { return mIndices.size(); }
  bool empty() const { return mIndices.empty(); }

  iterator begin() { return mIndices.begin(); }
  iterator end() { return mIndices.end(); }

  const_iterator begin() const { return mIndices.begin(); }
  const_iterator end() const { return mIndices.end(); }

  std::pair<SInt32,SInt32> getBounds() const;

  //! Set the vectorized net.
  void setNet(NUNet * net) { mNet=net; }

  //! Return the vectorized net.
  NUNet * getNet() const { return mNet; }

  bool isPort() const { return mPort; }

  //! Qualify this candidate.
  bool qualify(SInt32 size_limit) const;
  
  //! Print this candidate and if present, its vectorized net.
  void print() const;
private:
  UtString mName;

  IndexMap mIndices;

  NUNet * mNet;

  bool mPort;
};


class VectorMatchModule
{
public:
  //! Constructor
  VectorMatchModule(NUModule *one,
		    AtomicCache * str_cache,
		    NUNetRefFactory * netref_factory,
		    IODBNucleus * iodb,
		    VectorMatchStatistics * statistics,
		    bool verbose,
		    bool portVerbose) :
    mModule(one),
    mStrCache(str_cache),
    mNetRefFactory(netref_factory),
    mIODB(iodb),
    mStatistics(statistics),
    mVerbose(verbose),
    mPortVerbose(portVerbose)
  {}

  //! Destructor
  ~VectorMatchModule() {}

  //! Search for optimization opportunities.
  bool reconstruct();

private:
  //! name comparison functor
  class VectorNameCompare 
  {
  public:
    bool operator()(const UtString & a, const UtString & b) const {
      return (a.compare(b)<0);
    }
  };

  //! Map of names to candidate structures.
  typedef UtMap<const UtString, VectorCandidate*, VectorNameCompare> CandidateMap;

  //! Set of vectorization candidates.
  typedef UtSet<VectorCandidate*> CandidateSet;

  //! Set of names.
  typedef UtSet<UtString, VectorNameCompare> CandidateNameSet;

  class RE {
  public:
#if REGEX_GROUP_BUG
    RE (const char * re_v, const char * re_r) :
      re_vector(re_v),
      re_remainder(re_r)
    {}
    const char * re_vector;
    const char * re_remainder;
#else // REGEX_GROUP_BUG
    RE (const char * re_v) :
      re_vector(re_v)
    {}
    const char * re_vector;
#endif // REGEX_GROUP_BUG
  };

  //! Search for names/nets which resemble bit-blasted vectors.
  bool findCandidates(const RE & patterns);

  //! Search for ports which resemble bit-blasted vectors.
  bool findPortCandidates(const RE & patterns);

  //! Determine if a net is a valid vectorization candidate based on its characteristics
  bool qualify(const NUNet * net);

  //! Find vectorization candidates from flattened nets.
  bool findCandidates(NUNet * net, const RE & patterns);

  //! Generalized wrapper to find vectorization candidates.
#if REGEX_GROUP_BUG
  bool findNameMatchCandidates(NUNet * net,
			       const char * re_vector,
			       const char * re_remainder);
#else // REGEX_GROUP_BUG
  bool findNameMatchCandidates(NUNet * net,
			       const char * re_vector);
#endif // REGEX_GROUP_BUG

  //! Convert a name into one which doesn't already exist within the module.
  UtString getLegalName(UtString & name);

  //! Fetch/create a candidate for this name.
  VectorCandidate * getCandidate(UtString & name);

  //! Transform valid vector candidates into vectors.
  bool createVectors(SInt32 size_limit);

  //! Create a vector net from the candidate information.
  void createVector(VectorCandidate * candidate);

  //! Setup replacement maps used for module rewrites.
  void setupReplacements();

  //! Setup replacement maps for one candidate
  void setupReplacements(VectorCandidate * candidate);

  //! Update the module to use the vectors instead of the single-bit alternatives.
  void useVectors();

  //! Destroy intermediate structures
  void cleanup();

  //! Delete the source nets
  void cleanup(VectorCandidate * candidate);

  //! Print the current set of vector candidates.
  void printCandidates(bool filter) const;

  //! Print the port candidates.
  void printPortCandidates() const;

  void printMatches(const char * cname, int max_matches, REMatch * matches);

  NUModule * mModule;
  AtomicCache * mStrCache;
  NUNetRefFactory * mNetRefFactory;
  IODBNucleus            * mIODB;
  VectorMatchStatistics * mStatistics;

  //! Be noisy?
  bool mVerbose;

  //! Give information about port inference opportunities
  bool mPortVerbose;

  //! The current set of vector candidates.
  CandidateMap mCandidates;

  //! The set of successfully vectorized candidates.
  CandidateSet mVectors;

  //! The set of names we have already handed out to vectorization candidates.
  CandidateNameSet mLegalNames;

  NULvalueReplacementMap mLvalueReplacements;
  NUExprReplacementMap   mRvalueReplacements;
};


class VectorMatchStatistics
{
public:
  //! Constructor
  VectorMatchStatistics() { clear(); }
  //! Destructor
  ~VectorMatchStatistics() {}

  void module() { ++mModules; }
  void in()    { ++mBits; }
  void out() { ++mVectors; }
  void clear() { mModules=0; mBits=0; mVectors=0; }
  void print() const;

private:
  SInt64 mModules;
  SInt64 mBits;
  SInt64 mVectors;
};

#endif
