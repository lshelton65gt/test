// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __MARK_SWEEP_UNUSED_TASKS_H_
#define __MARK_SWEEP_UNUSED_TASKS_H_

#include "util/Util.h"
#include "nucleus/Nucleus.h"

class NUDesign;
class NUTask;
class NUModule;
class MsgContext;

//! Class to remove unused tasks from a design
class MarkSweepUnusedTasks
{
public:
  //! Constructor
  /*!
    \returns true if tasks were removed
    \param msg The message context.
    \param verbose  If true then print messages about tasks deleted
    \param allow_recursion Report error messages when recursive tasks are discovered
   */
  MarkSweepUnusedTasks(MsgContext* msg, bool verbose,
                       bool allow_recursion = true);

  //! Constructor
  ~MarkSweepUnusedTasks();

  //! Mark all unused tasks and remove them from the design.
  /*! returns true if any tasks were removed */
  bool walkAndRemove(NUDesign* design, bool* recursion_found);

private:
  typedef UtHashSet<NUTask*> TaskSet;
  TaskSet* mUsedTasks;
  MsgContext* mMsgContext;
  bool mReportRemovals;
  bool mAllowRecursion;
  TaskSet* mActiveTasks;
  NUTaskEnableVector* mActiveTaskEnableStack;
  
  class MarkCallback;
  class SweepCallback;

  // walk the design, marking active tasks, and looking for recursive
  // calls, return true if recursion found
  bool mark(NUDesign* design);

  //! Walk over the design, removing unused tasks. returns true if
  //! there were any problems.
  bool sweep(NUDesign* design);

  // forbid
  MarkSweepUnusedTasks();
  MarkSweepUnusedTasks(const MarkSweepUnusedTasks&);
  MarkSweepUnusedTasks& operator=(const MarkSweepUnusedTasks&);
};

#endif // __MARK_SWEEP_UNUSED_TASKS_H_
