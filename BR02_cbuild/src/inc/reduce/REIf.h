// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
 * \file
 * Transform if-then-else if.... into case constructs in expectation that
 * C++ switch statements will generate better code.
 */

#ifndef _REIF_
#define _REIF_

#include <utility>
#include "util/CarbonTypes.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUDesignWalker.h"

class MsgContext;
class AtomicCache;
class StringAtom;
class NUAssign;
class NUNet;
class SourceLocator;
class IODBNucleus;
class ArgProc;

//! Transform if-then-else if ... to case statements
class REIf
{
public:
  //! Constructor
  REIf(NUNetRefFactory*, MsgContext*, AtomicCache*, IODBNucleus *, ArgProc* args,
       bool verbose);

  //! Destructor
  ~REIf();

  //! Chase thru each module
  void reduceModule(NUModule* mod);

  // Map from case indices to statement list.
  typedef UtSet<SInt32> CaseIndices;
  typedef std::pair<CaseIndices*, NUStmtList*> CaseAction;

  typedef UtVector<CaseAction> CaseActionList;

private:
  //! Find the potential case selector
  bool findSelector (const NUExpr *cond, CaseIndices& cases, const NUExpr** foundSelector, bool* reversed);

  //! Walk the if chain
  bool walkIf (NUStmtList* stmts, const NUExpr* caseSel, CaseActionList& actions,
	       NUStmtList&);

  //! Examine the statements in always blocks
  void reduceStatements(NUStmtLoop stmtLoop, NUStmtList& newStmts,
                        NUStmtList& killStmts);

  //! Check for duplicate indices and complain
  void checkDuplicateIndices (const NUExpr* selector, CaseActionList& actions,
                              CaseIndices* indices, const SourceLocator& loc);

  //! Evaluate and transform a candidate if-statement.
  bool xformcase(NUIf*, NUStmtList* newStmts);

  // Nucleus walk callback
  class Callback : public NUDesignCallback
  {
#if ! pfGCC_2
    using NUDesignCallback::operator();
#endif

  public:
    //! constructor
    Callback(REIf* reif);

    //! destructor
    ~Callback();

    //! Catchall routine
    Status operator()(Phase, NUBase*) { return eNormal; }

    //! Stay within this module - do not walk down sub-modules
    Status operator()(Phase, NUModuleInstance*) { return eSkip; }

    //! Routine to handle blocks (which contain statement lists)
    Status operator()(Phase phase, NUBlock* block);

    //! Routine to handle "ifs" (which contain then/else statement lists)
    Status operator()(Phase phase, NUIf* ifStmt);

    //! Routine to handle case items (which contain a statement list)
    Status operator()(Phase phase, NUCaseItem * caseItem);

    //! Specialize for task enable because do not want to traverse into hierref tasks
    //! (need to stay within this module).
    Status operator()(Phase phase, NUTaskEnable * enable);

    //! Routine to handle tasks and functions (which contain statement lists)
    Status operator()(Phase phase, NUTF* tf);

    //! Return if statements were modified
    bool statementsModified() const { return mStmtsModified; }

  private:
    REIf* mREIf;

    // Set if a block within the current module has been modified
    bool mStmtsModified;
  }; // class Callback : public NUDesignCallback

  NUNetRefFactory *mNetRefFactory;
  MsgContext* mMsgContext;
  AtomicCache* mAtomicCache;
  IODBNucleus* mIODB;
  ArgProc* mArgs;

  //! Print out debugging and progress messages
  bool mVerbose;
};

#endif // _REIF_

