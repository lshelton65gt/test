// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef UNELABMARKSWEEP_H_
#define UNELABMARKSWEEP_H_

#include "flow/Flow.h"
#include "nucleus/Nucleus.h"
#include "localflow/DesignWalker.h"

/*!
  \file
  Declaration of unelaborated flow mark/sweep package.
 */

class IODBNucleus;

//! UnelabMarkSweep class
/*!
 * Class to perform a mark/sweep over unelaborated flow.
 */
class UnelabMarkSweep : public DesignWalker
{
public:
  //! constuctor
  /*
    \param dfg_factory DFG Factory
    \param iodb IODB
   */
  UnelabMarkSweep(FLNodeFactory *dfg_factory,
		  IODBNucleus *iodb,
		  bool start_at_all_nets) :
    DesignWalker(false),
    mFlowFactory(dfg_factory),
    mIODB(iodb),
    mStartAtAllNets(start_at_all_nets)
  {}

  //! Callback for sweep
  class SweepCallback
  {
  public:
    SweepCallback() {}
    virtual void operator()(FLNode *flnode, bool is_dead) = 0;
    virtual ~SweepCallback() {}
  };

  //! Mark the reachable flow for the design
  void mark(NUDesign *this_design);

  //! Mark the reachable flow for the module, recurse if flag is set
  void mark(NUModule *this_module, bool recurse);

  //! Mark all live flow starting at the specified nets.
  static void markNetFlow(NUNetList * start_list);

  //! Sweep the flow, callback for all flow
  void sweep(SweepCallback &callback);

  //! destructor
  ~UnelabMarkSweep() {}

  //! Clear the mark on all flow
  static void clearMarkAllFlow(FLNodeFactory * flow_factory);

private:
  //! Hide copy and assign constructors.
  UnelabMarkSweep(const UnelabMarkSweep&);
  UnelabMarkSweep& operator=(const UnelabMarkSweep&);

  //! Walk this module and submodules
  void module(NUModule *module);

  //! Walk this module and submodules if recurse is set
  void module(NUModule *module, bool recurse);

  //! Mark all live flow inside this module.
  void markModuleFlow(NUModule *module);

  //! DFG Factory
  FLNodeFactory *mFlowFactory;

  //! IODB
  IODBNucleus *mIODB;

  //! Use all nets or outputs/observable as starting points?
  bool mStartAtAllNets;
};

#endif
