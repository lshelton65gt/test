// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef _RE_MUX_
#define _RE_MUX_

/*!
  \file
  Declaration of MUX rewrite package.
 */

#include "util/CarbonTypes.h"
#include "nucleus/Nucleus.h"

class MsgContext;
class AtomicCache;
class StringAtom;
class NUAssign;
class BDDContext;
class IODBNucleus;
class ArgProc;


//! context for reducing stargen-like mux structures to a more efficient form
class REMux
{
public:
  //! Constructor
  REMux(NUNetRefFactory * netref_factory, 
	MsgContext  * msg_context, 
	AtomicCache * str_cache,
	IODBNucleus * iodb,
	ArgProc * args,
	UInt32 min_bit_size,
	UInt32 min_term_count,
	bool processTernaryOps,
	bool updateUD,
	bool verbose);

  //! Destructor
  ~REMux();

  //! reduce the muxes in the module
  void module(NUModule* mod);

private:

  //! Recursively process task/function contents.
  /*!
    \return true if any sub-construct was modified.
  */
  bool reduce(NUTF * tf);

  //! Recursively process block contents.
  /*!
    \return true if any sub-construct was modified.
  */
  bool reduce(NUBlock * block);

  //! Recursively process if() contents.
  /*!
    \return true if any sub-construct was modified.
  */
  bool reduce(NUIf * stmt);

  //! Recursively process case() contents.
  /*!
    \return true if any sub-construct was modified.
  */
  bool reduce(NUCase * stmt);

  //! Recursively process for() contents.
  /*!
    \return true if any sub-construct was modified.
  */
  bool reduce(NUFor * stmt);

  //! Recursively process statements.
  /*!
    Dispatch routine to ::xformMuxAssign() and the other ::reduce()
    methods.

    \param stmt The current statement.
    \param replacements Ordered list of modified+unmodified statements.
    \param kills List of rewritten statements (now needing deletion).

    \return true if any sub-construct was modified.
  */
  bool reduce(NUStmt * stmt, 
	      NUStmtList & replacements,
	      NUStmtList & kills);

  //! Recursively process a statement list.
  /*!
    \param loop The loop over a statement list.
    \param replacements Ordered list of modified+unmodified statements.
    \param kills List of rewritten statements (now needing deletion).

    \return true if any sub-construct was modified.
   */
  bool reduce(NUStmtLoop loop, 
	      NUStmtList & replacements,
	      NUStmtList & kills);

  //! Perform the mux rewrite operation on a single assignment.
  /*!
    Generated if-then-else statements are appended to the
    'replacements' list.

    \param assign The current assignment.
    \param replacements Ordered list of modified+unmodified statements.

    \return true if the assignment was converted into an if-then-else.
   */
  bool xformMuxAssign(const NUAssign* assign, NUStmtList & replacements);

  typedef std::pair<NUExpr*, NUExpr*> Term;
  typedef UtVector<Term> TermVector;

  bool isComplete(const TermVector& terms);
  bool isExclusive(const TermVector& terms);
  bool isComplete(TermVector* terms);

  bool checkTerm(NUExpr* exp, TermVector* terms);
  NUExpr * isTeCond (NUExpr* enaExpr);
  NUExpr * isMultiplyConcat(NUExpr* enaExpr, NUExpr* expr);

  //! Does this assignment use some of the nets it defines?
  bool hasDefUseOverlap(const NUAssign * assign);

  //! Thin 'getUses' wrapper to handle both NUContAssign and NUBlockingAssign.
  void getUses(const NUAssign* assign, NUNetSet* uses);

  //! Thin 'getDefs' wrapper to handle both NUContAssign and NUBlockingAssign.
  void getDefs(const NUAssign* assign, NUNetSet* defs);

  //! Delete all members of an expression
  void deleteExprList(NUExprList & exprs);

  //! Helper to generate the negation of an enable.
  NUExpr * generateNot(NUExpr * enable);

  NUNetRefFactory * mNetRefFactory;
  MsgContext      * mMsgContext;
  AtomicCache     * mStrCache;
  IODBNucleus     * mIODB;
  ArgProc         * mArgs;
  BDDContext      * mBDDContext;
  
  //! The minimum bit-size before converting into if-then-else.
  UInt32 mMinBitSize;

  //! The minimum number of terms needed before converting into if-then-else.
  UInt32 mMinTermCount;

  //! Should we convert ?: into if-then-else?
  bool mProcessTernaryOps;

  // do we need to update UD information?
  bool mUpdateUD;

  //! Print diagnostics?
  bool mVerbose;

  //! Expressions generated during the resolution of muxes.
  /*!
    Its contents should not persist across calls to ::module()
   */
  NUExprList mTemporaryExprs;
};
  

#endif // _RE_MUX_

