// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef DYNIDXTOCASE_H_
#define DYNIDXTOCASE_H_

#include "util/CarbonTypes.h"

class NUNetRefFactory;
class NUModule;
class MsgContext;
class IODBNucleus;
class ArgProc;
class AtomicCache;
class Fold;

//! DynIdxToCase class
/*!
 * Transform dynamic vector indexing to case statements when the dynamic
 * vector index is alone on the RHS and the indexing variable is under
 * the size limit.
 *
 * This was implemented to regain the lost performance from the Agere
 * DSP16k/ks2 testcases when memory to bitvector conversion was implemented.
 * This allows the muxes in those Agere designs to be optimized again.
 */
class DynIdxToCase
{
public:
  //! constructor
  DynIdxToCase(NUNetRefFactory *factory,  //! Net ref factory
               MsgContext *msg_context,   //! Message context
               IODBNucleus *iodb,         //! IO DB
               ArgProc *args,             //! Arg processor
               AtomicCache *str_cache,    //! String cache
               UInt32 size_limit,         //! Bit size limit of the indexing variable.  0 disables any xforms.
               bool verbose);             //! If true, dump out information on what xforms occured.

  //! destructor
  ~DynIdxToCase();

  //! Run analysis over the given module.
  void module(NUModule *module);

private:
  //! Hide copy and assign constructors.
  DynIdxToCase(const DynIdxToCase&);
  DynIdxToCase& operator=(const DynIdxToCase&);

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! Message context
  MsgContext *mMsgContext;

  //! IO DB
  IODBNucleus *mIODB;

  //! Arg processor
  ArgProc *mArgs;

  //! Fold object used to simplify generated expressions.
  Fold *mFold;

  //! Bit size limit of the indexing variable.  0 disables any xforms.
  UInt32 mSizeLimit;

  //! If true, dump out information on what xforms occured.
  bool mVerbose;
};

#endif
