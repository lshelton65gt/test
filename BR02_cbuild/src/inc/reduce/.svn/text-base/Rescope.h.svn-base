// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef RESCOPE_H_
#define RESCOPE_H_

/*!
  \file
  Declaration of rescope package.
 */

#include "nucleus/Nucleus.h"
#include "nucleus/NUDesignWalker.h"
#include "util/UtStack.h"
#include "nucleus/NUExpr.h"     // should move method defs to Rescope.cxx
#include "nucleus/NULvalue.h"   // ditto

class MsgContext;
class AtomicCache;
class IODBNucleus;
class ArgProc;
class ReduceUtility;
class RescopeStatistics;
class RescopeTranslator;

class Rescope
{
public:
  Rescope(AtomicCache *str_cache,
	  NUNetRefFactory *netref_factory,
	  MsgContext *msg_ctx,
	  IODBNucleus * iodb,
          ArgProc* args,
          SInt32 memory_limit,
	  bool verbose,
	  RescopeStatistics *stats);
    
  //! destructor
  ~Rescope();

  //! Walk a module and attempt to rescope its local nets.
  /*!
    \param module  The module to transform.

    The UD is recomputed if anything contained in the module changed.
   */
  void module(NUModule * one);


  //! Post block-merging, try and rescope single-use variables.
  /*!
    \param module The module containing the target blocks.
    \param blocks The blocks to transform.
    \param rescoped_nets Set of rescoped nets. Caller is responsible for 
                  clearing related symbol-table storage information.

    This pass does not create new nets; it moves nets from module to
    block scope. As a result, UD and Flow do not need updating.
   */
  void mergedBlocks(NUModule * one, 
		    NUUseDefVector & blocks,
		    NUNetSet * rescoped_nets);

private:
  //! Process all structured procs under the provided module.
  /*!
    Each structured proc (always and initial blocks) is walked in turn
    and analyzed for rescope opportunities.

    \return true if any nets were rescoped.
   */
  bool rescopeAllBlocks(NUModule * this_module);

  //! Process all TFs under the provided module.
  /*!
    Each task is walked in turn and analyzed for rescope
    opportunities.

    \return true if any nets were rescoped.
   */
  bool rescopeTFs(NUModule * this_module);

  /*!
    \param always        Search this always block for rescoping opportunities.
    \param rescoped_nets Set of rescoped nets. Caller is responsible for 
    clearing related symbol-table storage information.
  */
  bool rescopeMergedBlock(NUAlwaysBlock * always,
                          NUNetSet * rescoped_nets);

  //! Look for partial rescopable defs within an always block.
  /*!
    For example, the bit x[0] is rescopable within the first always
    block; The entire net cannot be rescoped because the x[1] bit is
    continuously live (its previous value may reach the q1 read).

    \code
      always @(posedge clk) begin
        x[0] = a & b & c;
        q0 = en & x[0];
      end

      always @(posedge clk) begin
        if (en) x[1] = d & e;
        q1 = x[1] & f;
      end
    \endcode

    This search is only applied after block merging.

    \return true if any partial net has been rescoped.
   */
  bool rescopeUnwavableMergedBlock(NUAlwaysBlock * always);

  //! Qualifies the scope of each net defined in this block and demotes those with local scope
  /*!
    \param proc An always or initial block.

    \returns true if we replaced any net with a local.
  */
  bool rescope(NUStructuredProc * proc);

  //! Qualifies the scope of each memory defined in this task and demotes those with local scope
  /*!
    \param task The task

    In general, this only happens for memories declared in tasks. This
    is because we already declare all task nets on the stack. Memories
    can be further improved by declaring each memory row as a separate
    variable.

    \returns true if we replaced any net with a local.
  */
  bool rescope(NUTask * task);

  //! Qualifies a single net
  bool qualified(NUNet * net) const;

  //! Qualifies a single net
  /*! 
    Tasks only allow memory rescoping because all other task nets will
    already be declared on the stack.
   */
  bool qualifiedTFNet(NUNet * net) const;

  //! Qualifies a single netref.
  /*!
    Is the provided net valid for schedule-time partial rescope?
   */
  bool qualifiedNetRef(NUNetRefHdl & net_ref) const;

  //! Determine the flags when temping a given net.
  NetFlags determineFlags(const NUNet * net) const;

  //! Demotes a set of nets and rewrites the block using temporaries.
  template<class DeclarationType>
  void demote(DeclarationType * scope, const NUNetSet * pending_rescope);

  //! Generate a local net and store remember how that relates to the original (for use in rewriting).
  template<class DeclarationType>
  void demote(DeclarationType * scope, NUNet * original, RescopeTranslator * translator);

  //! Generate local nets for partially rescoped nets.
  void demoteNetRefs(NUBlock * block, const NUNetRefSet * pending_rescope);

  //! Use gensym to generate an appropriate name for a partial net.
  StringAtom * generateNameForNetRef(const char * prefix,
                                     NUBlock * block,
                                     NUNetRefHdl & net_ref);

  //! Rewrite dynamic memory references as static.
  void rewriteDynamicMemories(NUBlockScope * proc, const NUNetSet * pending_rescope);

  //! Move a net from module to block scope. Only valid if a single-def, non-use net.
  void lowerScope(NUNet * net, NUBlock * block);

  //! print the rescope header.
  void printHeader(const char * label,
                   const NUStructuredProc * proc) const;

  //! print out one rescope and optionally a header.
  void printRescope(const char * label,
                    const NUStructuredProc * proc,
                    const NUNet * net,
                    bool do_header) const;

  //! print out one rescope and optionally a header.
  void printRescope(const char * label,
                    const NUTask * task,
                    const NUNet * net,
                    bool do_header) const;

  //! print that a partial net has been rescoped.
  void printPartialRescope(const char * label,
                           const NUStructuredProc * proc,
                           const NUNetRefHdl & net_ref,
                           bool do_header) const;

  ReduceUtility * mUtility;

  //! String cache
  AtomicCache * mStrCache;
    
  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;
    
  //! Message context.
  MsgContext * mMsgContext;

  //! IODB
  IODBNucleus * mIODB;

  //! args
  ArgProc* mArgs;

  RescopeStatistics * mStatistics;

  //! Largest memory we allow for rescope.
  SInt32 mMemoryLimit;

  //! Be verbose as processing occurs?
  bool mVerbose;
};


//! Determine if a memory is used in an ident.
class RescopeMemoryIdentSearch : public NUDesignCallback
{
public:
  RescopeMemoryIdentSearch(const NUNet * net) :
    mNet(net)
  {}

  //! By default, walk through all Nucleus elements.
  Status operator()(Phase, NUBase *) { return eNormal; }

  Status operator()(Phase, NUIdentRvalue * node);
  Status operator()(Phase, NUIdentLvalue * node);
  Status operator()(Phase phase, NUMemselRvalue * node);
  Status operator()(Phase phase, NUMemselLvalue * node);

  void putWalker(NUDesignWalker*);

private:
  const NUNet * mNet;
  NUDesignWalker* mWalker;
};

//! Replace dynamic memory references with constant ones.
class RescopeDynamicMemoryHelper
{
public:
  RescopeDynamicMemoryHelper(NUNetRefFactory * netref_factory,
                             const NUNetSet * rescoped_nets) :
    mNetRefFactory(netref_factory),
    mRescopedNets(rescoped_nets)
  {}

  //! Process all statements contained within a block.
  bool blockScope(NUBlockScope * block);

private:
  //! Dispatch to handler for stmt type.
  /*!
   * If statement is one of:
   *   NUBlock
   *   NUIf
   *   NUCase
   *   NUBlockingAssign
   * A specialized handler is called for that stmt-type.
   *
   * Otherwise, we search for and decompose dynamic memory references.
   *
   * \return true if this statement was decomposed into multiple statements.
   */
  bool stmt(NUStmt * stmt, NUStmtList * replacements); 

  //! Process case stmt.
  bool caseStmt(NUCase * stmt, 
                NUStmtList * replacements);

  //! Process if stmt.
  bool ifStmt(NUIf * stmt, 
              NUStmtList * replacements);

  //! Process for stmt.
  bool forStmt(NUFor * stmt,
               NUStmtList * replacements);

  //! Process assignment.
  bool assignStmt(NUBlockingAssign * stmt,
                  NUStmtList * replacements);

  //! Iteratively process stmt list until all dynamic references are gone.
  /*!
   * \return true if any statement was decomposed.
   * \sa processStmtLoopWorker
   */
  bool processStmtLoop(NUStmtLoop loop,
                       NUStmtList * replacements);

  //! Perform one decomposition pass over a stmt list.
  /*!
   * \return true if any statement was decomposed.
   * \sa processStmtLoop
   */
  bool processStmtLoopWorker(NUStmtLoop loop,
                             NUStmtList * replacements);

  template<class SuperMemselType,class MemselType>
  void processSingleMemsel(SuperMemselType * super_memsel, 
                           NUStmt * node,
                           NUStmtList * replacements);

  void decompose(NUStmt * node, NUStmtList * replacements);

  NUNetRefFactory *   mNetRefFactory;
  const NUNetSet *    mRescopedNets;

  NUScopeStack  mScopes;
};


//! Class to discover the dynamic memory selects within a statement.
class RescopeDynamicMemoryDiscoveryCallback : public NUDesignCallback
{
public:
  RescopeDynamicMemoryDiscoveryCallback(const NUNetSet * rescoped_nets) :
    mRescopedNets(rescoped_nets) {}

  //! By default, iterate through all Nucleus elements.
  Status operator()(Phase, NUBase *) { return eNormal; }

  //! Collect dynamic NUMemselRvalue expressions.
  Status operator()(Phase, NUMemselRvalue * node);

  //! Collect dynamic NUMemselLvalue elements.
  Status operator()(Phase, NUMemselLvalue * node);

  //! Get the collected rvalue memsels.
  void getRvalues(NUExprSet * rvalues);

  //! Get the collected lvalue memsels.
  void getLvalues(NULvalueSet * lvalues);

  //! Reset our discovered memory selects.
  void clear();

  //! Are there any dynamic references?
  bool any();
private:
  NUExprSet        mDynamicMemselRvalues;
  NULvalueSet      mDynamicMemselLvalues;

  const NUNetSet * mRescopedNets;
};


//! Class to replace dynamic memory references with generated tmps.
/*!
 * \sa RescopeDynamicMemoryHelper::decompose 
 * \sa RescopeDynamicMemorySeparationCallback
 */
class RescopeDynamicMemoryRewriter : public NuToNuFn
{
public:
  //! Constructor.
  /*!
   * \param ptr_comparison Should we perform ptr or operator==()
   *                       comparisons?
   */
  RescopeDynamicMemoryRewriter(bool ptr_comparison) :
    mPtrComparison(ptr_comparison) 
  {}

  //! Return a replacement for this expression, NULL otherwise.
  NUExpr * operator()(NUExpr * node, Phase);

  //! Return a replacement for this lvalue, NULL otherwise.
  NULvalue * operator()(NULvalue * node, Phase);
  
  //! Add one expression to our lookup.
  void add(NUMemselRvalue * old_expr, NUExpr * new_expr);

  //! Add expr lookup information to our lookup.
  void add(NUExprExprMap & expr_replacements);

  //! Add one lvalue to our lookup.
  void add(NUMemselLvalue * old_lvalue, NULvalue * new_lvalue);

  //! Add lvalue lookup information to our lookup.
  void add(NULvalueLvalueMap & lvalue_replacements);

  //! Clear lookup information.
  void clear();

private:
  //! Map of expression replacements.
  NUExprExprMap mExprReplacements;
  
  //! Map of lvalue replacements.
  NULvalueLvalueMap mLvalueReplacements;

  //! Should we perform ptr or operator==() comparisons?
  bool mPtrComparison;
};


//! Class to separate out multiple dynamic memory references from a statement.
/*!
 * Single dynamic references are easy: we can wrap the statement in a
 * case statement where each branch statically chooses one of N
 * temporaries.
 *
 * This class attempts to transform statements with multiple dynamic
 * references into statements with at most one dynamic reference.
 *
 * A list of "pre" and "post" statements is generated; these go before
 * and after the original statement. Temporaries are generated as
 * needed.
 *
 * Example:
 *
 * The following Verilog:
 \code
   mem_a[idx] = mem_b[mem_c[idx]] | mem_d[idx];
 \endcode
 *
 * would be translated into:
 \code
   $tmpc = mem_c[idx];
   $tmpb = mem_b[$tmpc];
   $tmpd = mem_d[idx];
   $tmpa = $tmpb | $tmpd;
   mem_a[idx] = $tmpa;
 \endcode
 *
 * Temporaries $tmpa, $tmpb, $tmpc, $tmpd were generated. The
 * resulting list of statements contains at most one dynamic memory
 * reference.
 *
 * \sa RescopeDynamicMemoryRewriter
 */
class RescopeDynamicMemorySeparationCallback : public NUDesignCallback
{
public:
  //! Constructor.
  /*!
   * \param scope    The current containing scope (usually a 
   *                 block); used for temporary generation.
   *
   * \param rewriter Callback for ::replaceLeaves() calls; understands
   *                 how to replace a dynamic memsel with our
   *                 generated replacement.
   */
  RescopeDynamicMemorySeparationCallback(NUScope * scope,
                                         RescopeDynamicMemoryRewriter * rewriter) :
    mScope(scope),
    mRewriter(rewriter)
  {}

  //! By default, iterate through all Nucleus elements.
  Status operator()(Phase, NUBase *) { return eNormal; }

  //! Collect dynamic NUMemselRvalue expressions.
  Status operator()(Phase, NUMemselRvalue * node);

  //! Collect dynamic NUMemselLvalue elements.
  Status operator()(Phase, NUMemselLvalue * node);

  //! Get a list of stmts which must precede the current statement.
  void getPreStmts(NUStmtList * stmts);

  //! Get a list of stmts which must follow the current statement.
  void getPostStmts(NUStmtList * stmts);

private:
  //! Generate a temp-assign for the given expression.
  /*!
   * Once it has been determined that a memsel needs to be extracted,
   * this method generates a temporary and assigns to that temporary. 
   *
   * An NUIdentRvalue is generated referencing that temporary; we will
   * later replace the memsel with this identifier.
   *
   * Example: The memory-reference
   *
   \code
     mem[idx]
   \endcode
   *
   * Will generate a temporary $tmp_mem and the following "pre"
   * assignment initializing that temporary:
   * 
   \code
     $tmp_mem = mem[idx];
   \endcode
   *
   * References to the original mem[idx] memsel will be replaced by a
   * reference to $tmp_mem. This replacement does not occur here; we
   * save a mapping between the original and the replacement. 
   *
   * Replacement occurs via a call to ::replaceLeaves() from one of
   * two locations:
   *
   * 1. RescopeDynamicMemoryHelper::decompose() -- rewrite rooted at
   *    the containing statement.
   *
   * 2. RescopeDynamicMemorySeparationCallback::operator(Phase,NUExpr*)
   *    -- rewrite rooted at our parent expression.
   *
   * \sa RescopeDynamicMemoryRewriter
   */
  void hoistExpression(NUMemselRvalue * expr);

  //! Generate a temp-assign for the given lvalue.
  /*!
   * Once it has been determined that a memsel needs to be extracted,
   * this method generates a temporary and assigns to that temporary. 
   *
   * An NUIdentLvalue is generated referencing that temporary; we will
   * later replace the memsel with this identifier.
   *
   * Example: The memory-reference
   *
   \code
     mem[idx]
   \endcode
   *
   * Will generate a temporary $tmp_mem and the following "post"
   * assignment initializing that temporary:
   * 
   \code
     mem[idx] = $tmp_mem;
   \endcode
   *
   * References to the original mem[idx] memsel will be replaced by a
   * reference to $tmp_mem. This replacement does not occur here; we
   * save a mapping between the original and the replacement.
   *
   * \sa RescopeDynamicMemoryRewriter
   */
  void hoistLvalue(NUMemselLvalue * lvalue);

  //! Current containing scope; used for temp generation.
  NUScope         * mScope;

  /*! 
   * Callback for ::replaceLeaves() calls; understands how to replace
   * a dynamic memsel with our generated replacement.
   */
  RescopeDynamicMemoryRewriter * mRewriter;

  //! Statements to come before the current.
  NUStmtList mPreStmts;

  //! Statements to come after the current.
  NUStmtList mPostStmts;
};

//! Replace nets with block-local temporaries.
class RescopeTranslator : public NuToNuFn
{
public:
  //! Constructor
  RescopeTranslator ();

  //! Destructor
  ~RescopeTranslator ();

  //! If this is a valid (non-memory) rescoped net, perform the translation.
  NUNet * operator()(NUNet * net, Phase phase);

  //! Translate memory-addressing into the tempified equivalent.
  /*!
    Nop for lvalue types besides NUMemselLvalue
   */
  NULvalue * operator()(NULvalue * lvalue, Phase phase);

  //! Translate memory-addressing into the tempified equivalent.
  /*!
    Nop for expr types besides NUMemselRvalue
   */
  NUExpr * operator()(NUExpr * expr, Phase phase);

  //! Remember original->replacement (non-memories).
  void add(const NUNet * original, NUNet * replacement);

  //! Remember original->replacement (non-memories).
  void add(const NUNet * original, NUExpr * replacement);

  //! Remember (original,address)->replacement (only memories).
  void add(const NUMemoryNet * original, SInt32 address, NUNet * replacement);

  //! Return the vector-net if this is a valid rescoped memory/address pair.
  /*!
    \return NULL if this is not a rescoped memory.
   */
  NUNet * getAddressNet(const NUNet * memory_net, SInt32 address) const;
private:  
  //! Return the vector-net if this is a valid rescoped memory/address pair.
  /*!
    \return NULL if this is not a rescoped memory or if the address is non-constant
   */
  NUNet * getAddressNet(const NUNet * memory_net, const NUExpr * address_expr) const;

  //! Map from rescoped-net to temp-replacement (except memories).
  NUNetReplacementMap mNetReplacements;

  //! Map from partially rescoped net to concat of its replacements.
  /*! 
    Part of that concat may be the original net. As a result, we can
    only perform replacement during a post walk.
   */
  NUExprReplacementMap mExprReplacements;

  typedef UtMap<SInt32,NUNet*> AddressMap;
  typedef UtMap<const NUNet*,AddressMap> MemoryReplacementMap;
  MemoryReplacementMap mMemoryReplacements;
};


//! Track statistics
class RescopeStatistics
{
public:
  RescopeStatistics() { clear(); }

  ~RescopeStatistics() {}

  void module() { ++mModules; }
  void block() { ++mBlocks; }
  void task() { ++mTasks; }
  void net()  { ++mNets; }
  void net_ref()  { ++mNetRefs; }
  void clear() { mModules=0; mTasks=0; mBlocks=0; mNets=0; mNetRefs=0; }
  void print() const;
  
private:
  SInt64 mModules;
  SInt64 mTasks;
  SInt64 mBlocks;
  SInt64 mNets;
  SInt64 mNetRefs;
};

#endif
