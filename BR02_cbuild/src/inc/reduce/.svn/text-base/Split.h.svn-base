// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef SPLIT_H_
#define SPLIT_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUUseDefNode.h"

#include "flow/Flow.h"
#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FlowUtil.h"

#include "util/Util.h"

/*!
  \file
  Declaration of block splitting package.
 */

// Forward declarations.
class STSymbolTable;
class AtomicCache;
class SourceLocatorFactory;
class MsgContext;
class STBranchNode;
class IODBNucleus;
class ArgProc;
class ClockAndDriver;
class Stats;


//! Ordered set of FLNode*
typedef UtSet<FLNode*,FLNodeCmp> FLNodeOrderedSet;


//! Comparator for FLNodeOrderedSet.
/*!
 * This comparator allows us to order sets of sets. This is all just
 * so the order of splitting operations is fixed.
 */
struct FLNodeSetCmp
{
  bool operator()(const FLNodeOrderedSet* set1, const FLNodeOrderedSet * set2) const;
};


//! Ordered set of FLNodeOrderedSet*
/*!
  Set of FLNodeOrderedSet -- models the set of sets, each of which must
  be preserved during block splitting.
*/
typedef UtSet<FLNodeOrderedSet*, FLNodeSetCmp> BlockSplitSets;


//! Why is a split allowed/disallowed?
/*!
 * This enum models the status for the splitting operation of one
 * always block.
 *
 * See Split::Status to save status information across all splitting
 * operations.
 *
 * TBD: Resolve name similarity between SplitStatus and Split::Status.
 *
 * \sa Split::Status
 */
enum SplitStatus {
  eSuccess,                //!< Splitting may proceed.
  eNonAlwaysBlock,         //!< Unsupported split requested for non-always UseDef.
  eCModelAlways,           //!< Unsupported split requested for CModel always.
  eHierMismatch,           //!< Hierarchy for each FLNode does not match.
  eNonPartitionNetInvalid, //!< Non-partitioned net either is memory or partially defined.
  ePartitionNetInvalid,    //!< Partitioned net is block-local.
  eControlFlowPresent,     //!< The block defines the control-flow net; a $stop or $finish is present.
  eForcibleNetDefined,     //!< The block defines a forcible net; any temping is unsafe.
  eHierRefTask,            //!< A hierarchical task call was present in the block.
  eAsyncReset,             //!< Don't allow blocks with async reset
  eTaskInlining,           //!< A task would need to be inlined against the caller's wishes.
  eDuplicateSideEffects,   //!< A statement with side effects would need to be duplicated.
  eUnknown                 //!< Unknown status.
};


//! Instructions on how to split a given block.
/*!
 * The splitting request is represented as a set of groups. Each of
 * these groups is a non-overlapping subset of the definitions in the
 * block. A FLNode set represents a single group. These FLNode sets
 * are ordered to help fix the order of operations.
 *
 * The user is required to add a group for each distinct split
 * required. Definitions not covered by the user-requested splits will
 * be placed into a special, connectivity group. The connectivity
 * group contains definitions which were not visible to the scheduler,
 * usually because they were elaboratedly dead and unelaboratedly
 * live.
 *
 * It is possible that the instruction information could be rewritten
 * in terms of nets or net-refs. The cycle detection splitting has had
 * success operating at a much lower level.
 */
class SplitInstructions
{
public:
  //! Constructor.
  /*!
   * \param use_def The block being split. Currently, only the
   *                splitting of always blocks is supported. This
   *                object must match the NUUseDefNode associated with
   *                each FLNode in all our groups.
   */
  SplitInstructions(NUUseDefNode * use_def);

  //! Destructor.
  ~SplitInstructions();

  //! Return the block associated with these splitting instructions.
  NUUseDefNode * getUseDef() const;

  //! Update the associated block.
  /*!
   * Currently, this interface is only used internally to block
   * splitting.
   */
  void setUseDef(NUUseDefNode* useDef);

  //! Add a set of defs; these will end up in one split.
  void addGroup(FLNodeOrderedSet * group);

  //! Provide access to all of the split groups.
  /*!
   * TBD: This interface should probably be rewritten as providing an
   * iterator, rather than a pointer to the internal data. The caller
   * should not be able to mutate the splitting requests.
   */
  BlockSplitSets * getGroups();

  //! Is the provided flow in one of our groups?
  /*!
   * This method tests if the flow is present in one of the
   * user-provided groups. The connectivity group is not checked for
   * membership.
   *
   * Worst-case, this method will perform N set lookups (one for each
   * user-provided group).
   */
  bool isPartitioned(FLNode * flow) const;

  //! Add the provided flow to our connectivity group.
  void addConnectivityFlow(FLNode * flow);

  //! Return the connectivity group to the caller.
  /*!
   * TBD: This interface should probably be rewritten as providing an
   * iterator, rather than a pointer to the internal data. The caller
   * should not be able to directly mutate the connectivity group.
   */
  FLNodeOrderedSet * getConnectivityGroup();

  //! Add all flow from all groups to the provided set.
  void getFlowFromAllGroups(FLNodeSet * output) const;

  //! Print the status of a splitting operation.
  /*!
   * The contents of each group and the status of a splitting
   * operation (success, failure reason, etc) are displayed.
   *
   * \sa printGroup
   */
  void print(SplitStatus status);

private:
  //! Print the names in this particular splitting group.
  void printGroup(FLNodeOrderedSet* group) const;

  //! The set of splitting groups. 
  /*!
   * Each group describes one decomposition of the block.
   */
  BlockSplitSets   mGroups;

  //! Definitions from the block not provided by the caller.
  /*!
   * The connectivity group contains in all continuously live
   * definitions not present in mGroups.
   *
   * The sets in mGroups usually contain all elaboratedly live flow
   * (also unelaboratedly live), while the connectivity group contains
   * elaboratedly dead flow (also unelaboratedly live).
   *
   * Unelaboratedly dead flow should not be present in the flow graph.
   */
  FLNodeOrderedSet mConnectivityGroup;

  //! The always block associated with these splitting instructions.
  /*!
   * TBD: We eventually may want a technique for splitting non-always
   * blocks. See bug 619 for an example where splitting of continuous
   * assignments is requested.
   */
  NUUseDefNode   * mUseDef;
};

/*!
  Ordered map from a given block (NUUseDefNode) to its set of FLNode sets.
  Defines the allowed splits for that block.
*/
typedef UtMap<NUUseDefNode*, SplitInstructions*, NUUseDefNodeCmp> BlockToSplitInstructionsMap;

/*!
  Map from FLNode to the set of FLNodeElab derived from that FLNode.
  FLNodeConstElabMap
*/

/*!
  Map from the candidate split block to its split parts.
*/
typedef UtMap<NUAlwaysBlock*,NUAlwaysBlockSet*> BlockToReplacements;


//! Set of hierarchy nodes -- utility container.
/*!
 * TBD: Other places in the codebase create set of branch nodes. They
 * all should use the same type.
 */
typedef UtSet<STBranchNode*> HierarchySet;


//! Map from unelaborated flow to all hierarchy containing an elaboration of that flow.
typedef UtMap<FLNode*,HierarchySet> FlowToHierarchyMap;


//! Represents final partition using shallow stmt lists
typedef UtMap<FLNodeOrderedSet*,NUStmtList,FLNodeSetCmp> FlowSetToStmtGroupMap;


/*!
 * Split blocks into independent parts based on the provided
 * instructions.
 *
 * TBD: There is a mix of method_name and methodName style in this
 * code. Clean it up.
 */
class Split
{
public:
  //! Constructor.
  /*!
    \param symtab       The symbol table.

    \param str_cache    The string cache.

    \param loc_factory  The source locator factory; see comments for
                        mLocFactory.

    \param flow_factory The unelaborated flow factory.

    \param flow_elab_factory The elaborated flow factory.
    
    \param netref_factory The net-ref factory.

    \param msg_ctx      The message context.

    \param iodb         The IODB.

    \param args         Argument processor.
  */
  Split(STSymbolTable *symtab,
        AtomicCache * str_cache,
        SourceLocatorFactory *loc_factory,
        FLNodeFactory * flow_factory,
        FLNodeElabFactory * flow_elab_factory,
        NUNetRefFactory *netref_factory,
        MsgContext * msg_ctx,
        IODBNucleus * iodb,
        ArgProc *args);

  //! Destructor.
  ~Split() {}


  //! Abstraction for the status of a split request.
  /*!
   * This models the status across all splitting operations. To model
   * the status of one splitting operations, see SplitStatus.
   * 
   * A status of eSplitChanged does not mean that _every_ split was
   * successful. It means that at least one splitting operation was
   * successful and no fatal errors were encountered.
   *
   * Note: It does not appear that eSplitError is currently used.
   *
   * TBD: Resolve name similarity between SplitStatus and Split::Status.
   *
   * \sa SplitStatus
   */
  enum Status
  {
    eSplitNone,                 //!< No modifications were performed.
    eSplitChanged,              //!< The splitting which occurred was successful.
    eSplitError                 //!< An error occured.
  };

  //! Split a set of blocks into its specified partitions by creating
  //!   new blocks.
  /*!
    \param design The design.

    \param clock_and_driver An auxillary structure used to ensure that
            driving logic associated with clock masters remains live.

    \param block_map A map of NUUseDefNode to BlockSplitSets. The node
            models a block to split. The set contains groups of
            FLNodeSplitSet. Each group models a set of statements
            which should be preserved by the splitting. (input)

    \param flow_to_elabs A map of FLNode to FLNodeElabSet. This set
            includes each FLNodeElab derived from the FLNode. The
            block-split rewrites parts of FLNode and FLNodeElab
            objects so newly-created blocks are properly referenced.
            (input)

    \param split_elab_flow A set of FLNodeElab. The elaborated set of
            blocks which were successfully split. If initially empty,
            it will the FLNodeElab blocks associated with a subset of
            the FLNode blocks blocks originally provided via
            block_map. Newly created FLNodeElab objects will also be
            placed in this set. (output)

    \param deleted_elab_flow A set of FLNodeElab. The set of
            elaborated dfg nodes which were deleted because they
            became dead. (output)

    \param allowTaskInlining Should we allow splits that require task
            inlining?

    \param verbose Should we be noisy?

    Returns eSplitChanged if we successfully split any block in
    block_map, eSplitNone if there were not splits and eSplitError if
    an error occured during splitting.
  */
  Status blocks ( NUDesign * design,
                  ClockAndDriver * clock_and_driver,
                  BlockToSplitInstructionsMap & block_map, 
                  FLNodeConstElabMap & flow_to_elabs,
                  FLNodeElabSet * split_elab_flow,
                  FLNodeElabSet * deleted_elab_flow,
                  bool allowTaskInlining,
                  bool verbose,
                  Stats* stats);

private:

  //! Augment the provided partitions with boundary flow unreachable via an elaborated walk.
  /*!
   * The scheduler requests splits in terms of the available
   * elaborated information. This includes logic which is elaboratedly
   * live (and, by association, unelaboratedly live).
   *
   * The scheduler does not provide splitting instructions which
   * include logic which is elaboratedly dead, but still
   * unelaboratedly live.
   *
   * Perform a flow factory walk and qualify each unelaborated flow.
   * If we have splitting instructions for the NUUseDefNode associated
   * with a FLNode and that FLNode is not present in the
   * scheduler-specified groups, add the FLNode to the connectivity
   * group.
   *
   * From the scheduler's perspective, we need to preserve only the
   * elaborated liveness of the flow in the provided splitting
   * instructions. To maintain the correct connectivity internal to
   * the design, we also need to maintain the unelaborated liveness
   * for everything the scheduler didn't provide.
   */
  void discoverFullBoundary(BlockToSplitInstructionsMap & block_map,
			    FLNodeConstElabMap & flow_to_elab);

  //! Repair the flow and elab flow for a set of split blocks.
  /*!
   * Collect all flow from out split instruction sets as the
   * reconstruction boundary. Call SplitFlow::repair to perform the
   * actual repair.
   *
   * \sa SplitFlow::repair
   */
  void repair(NUDesign * design,
              ClockAndDriver * clock_and_driver,
              NUAlwaysBlockSet * split_blocks,
              NUAlwaysBlockSet * split_replacements,
              BlockToSplitInstructionsMap & block_map,
              FLNodeConstElabMap & flow_to_elab,
              FLNodeElabSet * deleted_elab_flow,
              Stats* stats);

  //! Split the specified block into some number of independent blocks.
  /*!
   * \param instructions The splitting instructions for one block.
   *         Each group contained within the set of instructions
   *         models a group of definitions to be preserved during
   *         splitting. * Definitions must be mentioned at most once
   *         in the set of groups. * Definitions not included in the
   *         instruction groups will be * grouped into partitions
   *         based on dependencies. (input)
   * 
   * \param split_blocks Set of original blocks which were
   *         successfully split. (output)
   *
   * \param split_replacements Set of resulting split blocks. (output)
   *
   * \return eSuccess if we successfully split this block into its
   * parts; otherwise, the appropriate failure status is returned.
  */
  SplitStatus one_block(SplitInstructions * instructions,
                        NUAlwaysBlockSet * split_blocks,
                        NUAlwaysBlockSet * split_replacements);

  SplitStatus get_final_partition(SplitInstructions * instructions,
                                  const NUNetSet& all_partition_nets,
                                  FlowSetToStmtGroupMap * partition,
                                  bool * modified);

  //! Consistency check for multi-instance blocks.
  /*!
   * For combinational blocks, disallow splitting if the it appears
   * that performing the requested split will alter the elaborated
   * liveness of one or more of the outputs of this block.
   *
   * See test/block-splitting/partially-dead2.v for an example.
   *
   * We do not need to worry about sequential blocks, as splitting is
   * not allowed to create fanin arcs across splits. In combinational
   * splitting, we try to reduce the amount of replicated logic.
   *
   * TBD: If this ends up disqualifying too many blocks, we can
   * probably revert to the sequential-style splitting for
   * combinational blocks (repliate more logic).
   */
  SplitStatus checkPartitionHierarchy(SplitInstructions * instructions,
                                      FLNodeConstElabMap & flow_to_elab);

  //! Collect the hierarchy for an entire partition by FLNode.
  /*!
   * \sa checkPartitionHierarchy
   */
  void getHierarchy(FLNodeOrderedSet & flow_set,
                    FlowToHierarchyMap * flow_to_hierarchy,
                    FLNodeConstElabMap & flow_to_elab);

  //! Collect the hierarchy from the FLNodeElabs associated with one FLNode.
  /*!
   * \sa checkPartitionHierarchy
   */
  void getHierarchy(FLNodeElabSet & elab_set,
                    HierarchySet * hierarchy);

  //! Collect the set of FLNode in a partition with inconsistent hierarchy.
  /*!
   * \sa checkPartitionHierarchy
   */
  void checkHierarchy(FLNodeOrderedSet & flow_set,
                      HierarchySet & representative_hierarchy,
                      FlowToHierarchyMap & flow_to_hierarchy,
                      FLNodeSet * incomplete_flow);

  //! Check that there is no cross-partition flow between incomplete flow and this partition.
  bool checkInternalDependencies(FLNodeOrderedSet & flow_set,
                                 FLNodeSet & incomplete_flow);

  //! Determine the set of nets used internally by a partition.
  /*!
   * \sa checkPartitionHierarchy
   * \sa checkInternalDependencies
   */
  void computeInternalDependencies(FLNodeOrderedSet & flow_set,
                                   NUNetSet * internal_dependencies);
  
  //! Consistency check that split requests are for valid always blocks.
  /*!
   * CModels: Always blocks which include CModel calls are not split.
   *          Splitting a block containing a CModel can end up
   *          duplicating the * CModel call, which is not expected by
   *          the user.
   *
   * AsyncBlocks: Any portion of an asynchronous-reset sequential
   *          block will not be split. This is because it proved
   *          impossible with our current infrastructure to split all
   *          of the * related blocks in a uniform way and then piece
   *          the clock/priority * structure back together.
   *
   * Tasks: The block is disqualified if task inlining is disabled and
   *          the current block invokes a task which need inlining for
   *          valid splitting. Usually, this means that the block
   *          invokes a task which has non-local defs and uses.
   *          Without inlining these tasks, we may replicate the task
   *          call in multiple splits, unexpectedly updating the
   *          non-local task-defined nets more than once.
   *
   * HierRef Tasks: Hierarchical references to tasks cannot be inlined
   *          with today's techniques and therefore prevent splitting
   *          for the same reason as a non-inlined task with non-local
   *          definitions prevents splitting.
   *
   * \return eSuccess if the block can be legally split, the
   * appropriate SplitStatus otherwise.
   */
  SplitStatus checkUseDef(NUUseDefNode * use_def, bool allowTaskInlining);

  //! Check the block for hier-ref tasks.
  /*!
   * This method always returns eSuccess if the -multiLevelHierTasks
   * switch was provided. See bug 2916 for details.
   *
   * \sa Split::checkUseDef
   */
  SplitStatus checkAlwaysForTaskHierRefs(NUAlwaysBlock * always_block);

  //! Consistency check for non-partitioned nets.
  /*! 
   * Disallow splitting when a block defines a memory and this memory
   * has not been assigned to a group.
   *
   * \param node The block being split.
   *
   * \param all_partition_nets The union of all nets present in all groups.
   *
   * \return eNonPartitionNetInvalid if invalid nets were not found,
   * eSuccess otherwise.
   */
  SplitStatus checkNonPartitionNets(NUUseDefNode * node,
                                    const NUNetSet & all_partition_nets);

  void add_to_final_partition ( NUAlwaysBlock * always,
                                const NUNetSet & all_block_nets,
                                const NUNetSet & all_partition_nets,
                                FLNodeOrderedSet & flow_set,
                                FlowSetToStmtGroupMap * partition );

  //! Create new always blocks from the provided statement groups.
  /*!
   * \param module The module containing the input block; target for
   *     resulting blocks.
   *
   * \param always The original, input always block. (input)
   *
   * \param partition Mapping from the user-provided flow groups to a
   *     set of statements which must be included in the block for the
   *     group. (input)
   *
   * \param split_blocks Set containing all always blocks which have
   *     been successfully split. (output)
   *
   * \param split_replacements Set containing all always blocks which
   *     are the result of a splitting operation. (output)
   *
   * Uses one_group to process each group.
   * 
   * \sa one_group
   */
  SplitStatus statement_partition(NUModule * module,
                                  NUAlwaysBlock * always,
                                  FlowSetToStmtGroupMap & partition,
                                  NUAlwaysBlockSet * split_blocks,
                                  NUAlwaysBlockSet * split_replacements);

  //! Create the always block corresponding to a single group.
  /*!
   * \param module The module containing the input block; target for
   *     resulting blocks.
   *
   * \param always The original, input always block. (input)
   *
   * \param group_stmts The statements which must be included in the
   *     block (as copies). (input).
   *
   * \param flow_set The group of definitions which must persist in
   *     the newly created always block.
   *
   * \sa statement_partition
   */
  void one_group ( NUModule * module,
                   NUAlwaysBlock * always,
                   const NUStmtList & group_stmts,
                   FLNodeOrderedSet & flow_set,
                   NUAlwaysBlockSet * split_replacements );

  /*! 
   * Determine which statements must be preserved in order to
   * correctly compute the given group. See method body for
   * information about the algorithm used.
   *
   * \param all_block_nets All defs in this group of statements. (input)
   *
   * \param all_partition_nets Nets computed by all partitions. (input)
   *
   * \param group_nets Nets that need to be computed by this group
   *                   of statements. Subset of all_block_nets. (input)
   *
   * \param stmts All statements in this group. (input)
   *
   * \param independent_stmts Minimum (or close) set of statements 
   *                          required to compute 'group_nets'. (output)
   *
   * \param is_sequential True if the containing always block is
   *        sequential in nature. The dependency analysis is more
   *        conservative for sequential blocks. When processing a
   *        sequential * block, the dependency algorithm cannot allow
   *        values to carry * across splits. To do so would incur a
   *        cycle delay. (input)
   */
  void isolate_dependencies ( const NUNetSet& all_block_nets,
                              const NUNetSet & all_partition_nets,
                              const NUNetSet & group_nets,
                              NUStmtList & stmts,
                              NUStmtSet * independent_stmts,
                              bool is_sequential );

  //! Create an always block from an existing block and a stmt group.
  /*!
   * Given a source always block a set of statements, create a new
   * always block with copies of only the provided statements.
   *
   * New temporary nets are created for any definitions present in
   * stmts which are not requested members of this group.
   *
   * \param always_block The original always block.
   *
   * \param group_nets Nets expected to have definitions in the new
   *        block.
   *
   * \param group_stmts The set of statements which belong in the
   *        created block. These statements are copied because
   *        multiple split blocks may require the same statement
   *        because of * dependency requirements.
   *
   * \return the newly constructed always block.
   */
  NUAlwaysBlock * create ( NUAlwaysBlock * always_block,
                           const NUNetSet & group_nets,
                           const NUStmtList & group_stmts );

  //! Determine which nets from the old block need to have temps.
  /*!
   * Nets defined in this split which are not present in the
   * caller-specified group (group_nets) have temp versions created.
   *
   * All block-local nets from the original block are temped.
   *
   * Initializers (based on the net values computed in separate split
   * groups) are added for all uses which are not resolved by locally
   * copied defs.
   * 
   * The new block is modified in-place.
   *
   * \param old_block The old, pre-split block.
   *
   * \param new_block The result of splitting one group.
   *
   * \param stmt_to_original A map from new statement pointer back to
   *        the original statement in the pre-split always block.
   *        Accurate UD information is not yet available for the
   *        copied statements, so we go back to the pre-split version
   *        of each statement.
   *
   * \param group_nets Nets which belong in this split. Other defs
   *        carried over (because of data dependencies or other
   *        reasons) will be temped.
   */
  void tmpify_nets( const NUBlock * old_block, 
                    NUBlock * new_block,
                    const NUNetSet & group_nets );

  //! Create a replacement net in the specified split block.
  /*!
   * Create a temp version of the specified net, declared within the
   * provided split block.
   *
   * At this point, the cloning process is pretty similar to
   * NUScope::createTempNetFromImage, but we handle memories in a
   * different manner.
   */
  NUNet * createReplacement ( NUBlock * block,
                              NUNet * original );

  //! Can we handle the net types in and outside the partitioning?
  /*!
   * \param instructions The user-specified (and inferred
   *     connectivity) splitting instructions. (input)
   *
   * \param all_partition_nets The set of all nets in all
   *     user-specified groups. Nets in the connectivity group are not
   *     included.
   *
   * \sa checkPartitionNets
   * \sa checkNonPartitionNets
   */
  SplitStatus checkPartition(SplitInstructions * instructions,
                             NUNetSet * all_partition_nets);

  SplitStatus checkFinalPartition(FlowSetToStmtGroupMap & partition,
                                  NUModule* module);
  
  //! Populate a set with all nets defined in a flow set.
  /*!
   * Pseudocode:
   * net_set = map(lambda flow : flow->getDefNet(), flow_set)
   */
  void getGroupNets(const FLNodeOrderedSet & flow_set, NUNetSet * net_set);

  //! Are nets in a user-specified group legal for splitting?
  /*!
   *  Test that none of a set of nets are of the following types:
   *
   * Forcible: Splitting a forcible net is hard because these nets
   *     cannot be temped. If the user chooses to force a value on
   *     the net, the model-computed value cannot take effect. By
   *     using temps, we would break this functional requirement.
   *
   * ControlFlow: Blocks defining the control flow net are not
   *     currently split because we could alter the order of execution
   *     of $stop/$finish statements. Alternatively, we would end up
   *     replicating entire blocks in each partition (the $cfnet
   *     causes LOTS of implicit dependencies).
   *
   * Block-Local: We check and disqualify if any block-local
   *     non-static are specified in the partitioning. This probably
   *     should be an invalid situation in our data model, but bug
   *     6618 proves that it occurs.
   *
   * \sa checkPartition
   */
  SplitStatus checkPartitionNets(const NUNetSet & net_set);

  //! Update a set of FLNode to now reference the new always block.
  void fixup_flow ( NUAlwaysBlock * new_always, 
                    FLNodeOrderedSet & flow_set );

  //! Add all statements to the group except those proven independent.
  /*!
   * Pseudocode:
   * group_stmts = filter(lambda s: not independent_stmts.contains(s), stmts)
   */
  void subtract_independents ( NUStmtList & stmts, 
                               NUStmtSet  & independent_stmts,
                               NUStmtList * group_stmts );

  //! Delete the elaborated nets for all block-locals in the source blocks.
  void deleteElaboratedNets(NUAlwaysBlockSet * split_blocks);

  //! Delete the source blocks which have been successfully split.
  void destroySplitBlocks(NUAlwaysBlockSet * split_blocks);

  //! Add the elaborated flow for split successes to a map.
  /*!
   * \sa rememberGroupElabFlow
   */
  void rememberSplitElabFlow(SplitInstructions * instructions,
                             FLNodeConstElabMap & flow_to_elab,
                             FLNodeElabSet * split_elab_flow);

  //! Add the elaborated flow for a single group to a map.
  /*!
   * \sa rememberSplitElabFlow
   */
  void rememberGroupElabFlow(FLNodeOrderedSet & flow_set,
                             FLNodeConstElabMap & flow_to_elab,
                             FLNodeElabSet * split_elab_flow);

  //! Test if a block would require task inlining
  bool wouldRequireTaskInlining(NUAlwaysBlock* always);

  //! Inline any tasks with references to non-local nets.
  bool inlineTasksWithNonLocals(NUAlwaysBlock * always);

  //! Debug method. Print all splitting requests.
  /*!
   * The reported status information is inaccurate. Everything is
   * assumed to have status eUnknown for printing.
   *
   * \sa SplitInstructions::print
   */
  void print(BlockToSplitInstructionsMap * block_map);

  //! This function prints out a NUNetSet; useful for debugging
  /*!
   * \sa Split::print
   */
  void print(const NUNetSet & s);

  //! Utility structures ----------------------------------------

  //! The symbol table.
  STSymbolTable *mSymtab;

  //! String cache.
  AtomicCache * mStrCache;

  //! Source locator factory.
  /*!
   * Passed to SplitFlow; used by the Elaboration constructor and
   * that's only because the LocalFlow context class is shared between
   * population and elaboration.
   */
  SourceLocatorFactory *mLocFactory;

  //! Unelaborated flow factory.
  FLNodeFactory * mFlowFactory;
  
  //! Elaborated flow factory.
  FLNodeElabFactory * mFlowElabFactory;

  //! Net-ref factory.
  NUNetRefFactory * mNetRefFactory;

  //! Message context.
  MsgContext * mMsgContext;

  //! IODB.
  IODBNucleus * mIODB;

  //! Argument processor.
  ArgProc * mArgs;

  //! True if the -multiLevelHierTasks switch was provided.
  /*!
   * The -multiLevelHierTasks switch was developed for a specific
   * Mindspeed design in an attempt to overcome some Verilog testbench
   * issues with high amounts of hierarchical references to both nets
   * and tasks. See bug 2916 for a little more history. There are
   * testcases in test/bugs/bug2916.
   *
   * Within block-splitting, the -multiLevelHierTasks switch disables
   * all task inlining prior to performing a splitting operation.
   * Further, it allows block splitting even in the presence of
   * hierarchical references to tasks.
   *
   * The -multiLevelHierTasks switch makes UD inaccurate for tasks
   * with hierarchical references. The concensus is that there are
   * situations where we will get functionally incorrect answers with
   * this strategy.
   */
  bool mMultiLevelTasks;
};

#endif
