// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef DEADNETS_H_
#define DEADNETS_H_

#include "reduce/DeadDesignWalker.h"

class STSymbolTable;

/*! 
 * Cleanup symbol table references for a list of nets during dead code elimination.
 * \sa DeadDesignCallback
 */
class DeadNetCallback : public DeadDesignCallback
{
public:
  DeadNetCallback(STSymbolTable *symtab) : mSymtab(symtab) {}

  virtual void operator()(NUNetList * deleted_nets);
private:
  STSymbolTable * mSymtab;
};


#endif
