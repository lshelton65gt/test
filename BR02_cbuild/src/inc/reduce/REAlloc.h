// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef REALLOC_H_
#define REALLOC_H_

#include "nucleus/Nucleus.h"
#include "util/Util.h"

class MsgContext;
class IODBNucleus;
class STAliasedLeafNode;
class STSymbolTable;
class STBranchNode;
class Stats;
class FLNodeElabFactory;
class FLNodeFactory;
class ArgProc;
class ReachableAliases;

/*!
  \file
  Declaration of class used to assign allocation & storage.

  Allocation refers to a bit kept in NUNet, which is used by the
  code generator to decide whether to allocate direct storage for
  that net in its module, or whether to make that net be a reference
  to storage in some other module.

  Storage refers property of an alias-ring, which is a circular
  linked list of STAliasedLeafNodes.  There is exactly one storage
  STAliasedLeafNode per alias ring.  The NUNet associated with that
  storage element must have allocation set, and none of the NUNets
  associated with the other STAliasedLeafNodes can have allocation
  set.

  The allocation implementation as of March 2004 decides allocation
  as it follows design flow.  This can lead to illegal situations
  where more than one NUNet on an alias ring thinks it has the
  allocation.  This is bad because values may not propagate properly
  across all the different names for a physical design wire.

  Today, cbuild will assert when this occurs.

  There is an algorithm proposed in the comments for bug832, which
  is roughly:

      map<NUNet*,int> netCounts;

      foreach (alias ring in design) {
        foreach NUNet* net in alias ring
          ++netCounts[net];
      }
      foreach (alias ring in design) {
        bool foundStorage = false;
        foreach NUNet* net in alias ring {
          if (netCounts[net] == 1) {
            // pick the first net we see, bug832 comment 8 says it can be
            // any net, it doesn't matter
            allocate storage for current alias
            foundStorage = true;
          }
        }
        assert(foundStorage);
      }

  I think this algorithm will fail (assert) with the following testcase:

      module top(out, in);
        output out;
        input in;

        mybuf u1(tmp, in);
        mybuf u1(out, tmp);
      endmodule

      module mybuf(out, in);
        output out;
        input in;
        invert u1(tmp, in);
        invert u2(out, tmp);
      endmodule

      module invert(out, in);
        output out;
        input in;
        assign out = ~in;
      endmodule

  The algorithm implemented here is a little different.  It tries
  to find an "optimal" allocation which might be illegal, and then
  iteratively changes the allocation until it finds a legal one
  (that might be less optimal).

  Here, "optimal" means the allocation should:

    a) minimize the propagation of net flags that imply less efficient
       storage, such as Forcible, Tristate, Bidirect, and StateUpdate.
       The NUNet that receives the allocation must generate a member
       variable declaration based on these flags
       (e.g. Bidirect<UInt1>).  All writers to the net must also know
       about these declarations.  However, an NUNet that is not
       allocated, and only reads the value of that net, can codegen a
       reference declaration based on the net's basic type
       (e.g. UInt1).

    b) minimize the number of reference variables that need to be created.

  A reference variable must be created when the allocation is at a
  higher level of hierarchy than a reference.  When the allocation is
  at a lower level, the higher levels of hierarchy can use scoped
  access to directly access the lower-level storage -- this is
  desirable.

  Phase 1:

  For each alias ring, find an allocation at the highest "writer"
  in each alias ring.  This can lead to duplicate allocations when two
  alias rings have intersecting but non-equivalent sets of NUNets.
  
  Phase 2:

  For each alias ring that has a conflict, move the allocation up
  in hierarchy until there is no more conflict.  This should rapidly
  converge on a correct answer, as long as all ports are connected.

  Phase 3:

  Propagate various net flags so that all modules that write to a net,
  or that instantiate other modules that write to a net, have the
  correct declaration of the net.  E.g. if a net is forcible, it would
  be very bad to allow a writer to see that net as non-forcible.  In
  the generated C++ code, an ugly cast would be required to get that
  to compile, and the integrity of the force could be compromised.
  Hence all writers need to have all the flags set on the net.

  Readers, on the other hand, do not need to know whether a net is
  forcible, or tristate, or bidirect.  A const references to the value
  field can be made available to readers, and so excess propagation of
  the flags to nets that are not written in their module or any module
  below, do not need those flags.
*/

//! REAlloc
class REAlloc
{
public:
  //! constructor
  REAlloc(STSymbolTable* symtab,
          IODBNucleus* iodb,
          ArgProc* args,
          MsgContext *msg_context,
          bool dump,
          Stats* stats,
          FLNodeElabFactory *flow_elab_factory,
          FLNodeFactory *flow_factory,
          ReachableAliases *reachables);

  //! dtor
  ~REAlloc();

  //! before REAlias occurs, collect the legal places to assign storage
  void collectLegalStorage();

  //! walk a design and figure allocation.  This must run after REAlias
  bool design(NUDesign*);

  //! Aggressivelly propagate flags to guarantee elaborated consistency
  void propagateFlags(NUDesign*);

private:
  //! Move the storage node in node's alias ring up a level hierarchy.
  /*! If there is no legal node at a level up, keep going up till we
   *! hit the design's top level
   */
  void moveStorageUpHierarchy(STAliasedLeafNode* node,
                              STAliasedLeafNode* storage);

  //! Record all the nets that require tristate initialization prior
  //! to temporarily clearing the net flags
  void recordTriInitNets();

  //! Using a symbol-table traversal, find all the elaborated nets in the design.
  void findNetElabs(NUDesign* design);

  //! Pick a good performing allocation
  /*! We want the allocation  as low in the hierarchy as
   *! possible to minimize reference variables.  Note that this allocation may
   *! not be legal -- it may produce two alocations in the same ring when
   *! rings in the elaborated design contain intersecting but non-equal
   *! sets of NUNets.
   */
  void greedyPickAlloc();

  //! Fix missing aliases to dead nets
  void fixHierarchicalAliases(NUDesign* design/*STBranchNode* parent*/);

  //! Invalidate certain nets as allocation candidates.
  /*!
   * 1. An unelaborated net appears multiple times in an alias ring.
   * 2. An unelaborated net is covered by a hierarchical reference in the port-connection graph.
   *
   * \return true on success, false on failure.
   */
  bool findDisqualifiedNets(NUDesign * design);

  /*! 
   * Any NUNet that appears in a live alias ring more than once due to
   * multiple instantiations is immediately disqualified
   */
  void disqualifyRepeatedUnelaboratedNets();

  /*!
   * Any NUNet that is below a hierarchical reference in the
   * port-connection graph cannot be considered for allocation.
   *
   * \return false if cyclic hierrefs are discovered, true otherwise.
   */
  bool disqualifyHierRefCoverage(NUDesign * design);

  //! Does this net qualify to receive allocation?
  bool isDisqualifiedNet(NUNet* net);

  //! Pick the best node for allocation in an alias ring
  void pickBestNodeInRing(STAliasedLeafNode* masterNode,
                          bool pickHighestNode);

  //! Report an error where we could not find a good allocation
  void reportError(STAliasedLeafNode* masterNode, bool dup);

  //! print detail about a ring
  void printRing(STAliasedLeafNode* node);

  //! Move storage nodes up in the hierarchy to legalize the allocation
  void moveAllocsUp();

  //! Validate that we have a legal allocation.
  void sanityCheck();

  //! Dump the allocation in a canonical order
  void dump();

  //! Hide copy and assign constructors.
  REAlloc(const REAlloc&);
  REAlloc& operator=(const REAlloc&);

  MsgContext* mMsgContext;
  IODBNucleus* mIODB;
  ArgProc* mArgs;
  STSymbolTable* mSymbolTable;

  NUNetSet* mDisqualifiedNets;
  NUNetElabVector* mLiveNetElabs;
  NUNetElabSet* mRequiresTriInit;

  bool mMoveUpSettled;
  bool mDump;
  Stats* mStats;
  FLNodeElabFactory* mFlowElabFactory;
  FLNodeFactory* mFlowFactory;

  //! Class which maintains reachable symbol table nodes.
  ReachableAliases *mReachableAliases;
};

#endif
