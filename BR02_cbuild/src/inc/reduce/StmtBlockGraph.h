// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef STMT_BLOCK_GRAPH_H_
#define STMT_BLOCK_GRAPH_H_

#include "util/GenericDigraph.h"
#include "util/GraphDot.h"

#include "reduce/AllocAlias.h"

#include "nucleus/Nucleus.h"
#include "nucleus/NUNetRefMap.h"

class StmtBlockStmtData;

// Added a full class for namespace.
// typedef GenericDigraph<void*,StmtBlockStmtData*> StmtBlockGraph;

/*!
  The statement block graph.

  The statement block graph is a local graph modeling the use/def
  dependencies between the statements in a given statement list. 

  Each edge has a marking which describes the relationship it is
  modeling. \sa StmtBlockGraph::EdgeFlag

  There is a unique graph node for each non-control statement in the
  input statement list. Control statements have multiple nodes.

  If statements are modeled using four separate nodes. There is a node
  associated with the select, the then clause and the else clause. The
  last node represents the entire if statement. 

  Case statements are modeled using four separate types of nodes. There
  is one node representing the entire case statement. Each case item has
  a node for its conditions and another node for its statements. All of
  the conditions for a single case item are represented with a single
  node; there is not a node for each distinct condition. The last node
  corresponds to the case select.

  Details: doc/functional-specs/statement_block_graph.html
 */
class StmtBlockGraph : public GenericDigraph<void*,StmtBlockStmtData*>
{
public:
  //! Edge flag bits - represents all dependency types
  typedef enum {
    NON_BLOCKING = 0x80,
    BLOCKING     = 0x00, // absence of flag
    USE_AFTER    = 0x40,
    DEF_AFTER    = 0x00, // absence of flag 
    USE          = 0x20,
    DEF          = 0x00, // absence of flag
    BROKEN       = 0x10
  } EdgeFlag;
private:
};


//! Generic data class for statement graph nodes.
/*!
  Compound (if and case) statements and their components have
  specialized data types.

  \sa StmtBlockIfData
  \sa StmtBlockCaseData
  \sa StmtBlockIfComponentData
  \sa StmtBlockCaseComponentData
 */
class StmtBlockStmtData
{
public:
  //! Constructor
  StmtBlockStmtData(NUStmt * stmt) :
    mStmt(stmt)
  {}

  //! Destructor
  virtual ~StmtBlockStmtData() {}

  //! Does the statement associated with this node have components?
  virtual bool isCompoundStatement() const { return false; }

  //! Does this node represent the component for another statement?
  virtual bool isStatementComponent() const { return false; }

  //! Return the statement associated with this node.
  virtual NUStmt * getStmt() const { return mStmt; }

private:
  NUStmt * mStmt;
};


//! Base class for all graph nodes which model a compound statement (if/case).
class StmtBlockCompoundStmtData : public StmtBlockStmtData
{
public:
  //! Constructor
  StmtBlockCompoundStmtData(NUStmt * stmt) :
    StmtBlockStmtData(stmt)
  {}

  //! Destructor
  virtual ~StmtBlockCompoundStmtData() {}

  //! Does the statement associated with this node have components?
  virtual bool isCompoundStatement() const { return true; }

  //! Is an if statement?
  virtual bool isIfStatement() const { return false; }

  //! Is a case statement?
  virtual bool isCaseStatement() const { return false; }

  //! List of nodes for all the components associated with this compound statement.
  typedef UtVector<StmtBlockGraph::Node*> ComponentVector;
  typedef CLoop<ComponentVector> ComponentCLoop;
  typedef Loop<ComponentVector> ComponentLoop;

  //! Return all the components associated with this compound statement.
  void getComponents(ComponentVector * components) const {
    components->insert(components->begin(),mComponents.begin(),mComponents.end());
  }

  //! Return a loop over all the components associated with this compound statement.
  ComponentCLoop loopComponents() const { return ComponentCLoop(mComponents); }
  ComponentLoop loopComponents() { return ComponentLoop(mComponents); }

  //! Add a new component to our set.
  void addComponent(StmtBlockGraph::Node* component) {
    mComponents.push_back(component);
  }
private:
  ComponentVector mComponents;
};


//! Base class for all graph nodes which model an individual component of an if/case statement.
class StmtBlockStmtComponentData : public StmtBlockStmtData
{
public:
  //! Constructor
  StmtBlockStmtComponentData(StmtBlockGraph::Node* compound_parent) :
    StmtBlockStmtData(compound_parent->getData()->getStmt()),
    mStmtNode(compound_parent)
  {}

  //! Destructor
  virtual ~StmtBlockStmtComponentData() {}

  //! Does this node represent the component for another statement?
  virtual bool isStatementComponent() const { return true; }

  //! Is this a component of an if statement?
  virtual bool isIfComponent() const { return false; }

  //! Is this a component of a case statement?
  virtual bool isCaseComponent() const { return false; }

  //! Return a printable string for this component type.
  virtual const char * getComponentTypeString() const = 0;

  //! Return the node for the parent compound statement which contains this component.
  StmtBlockGraph::Node* getStmtNode() const { return mStmtNode; }

private:
  //! The node for our parent compound statement.
  StmtBlockGraph::Node* mStmtNode;
};


//! Data class for graph nodes which model an individual component of an if statement.
class StmtBlockIfComponentData : public StmtBlockStmtComponentData
{
public:
  typedef enum {
    CONDITION,                  //!< The if condition.
    THEN_CLAUSE,                //!< Statements in the then clause.
    ELSE_CLAUSE                 //!< Statements in the else clause.
  } ComponentType;

  //! Constructor.
  StmtBlockIfComponentData(StmtBlockGraph::Node* compound_if_node,
                           ComponentType component_type) :
    StmtBlockStmtComponentData(compound_if_node),
    mComponentType(component_type)
  {}
  
  //! Destructor.
  virtual ~StmtBlockIfComponentData() {}

  //! Is this a component of an if statement?
  virtual bool isIfComponent() const { return true; }

  //! Return the type of component.
  ComponentType getComponentType() const { return mComponentType; }

  //! Return a printable string for this component type.
  virtual const char * getComponentTypeString() const {
    switch(mComponentType) {
    case CONDITION:   return "CONDITION";
    case THEN_CLAUSE: return "THEN_CLAUSE";
    case ELSE_CLAUSE: return "ELSE_CLAUSE";
    default:          return "UNKNOWN";
    }
  }
private:
  //! The component type represented by this node.
  ComponentType mComponentType;
};

//! Data class for graph nodes which model an individual component of a case statement.
class StmtBlockCaseComponentData : public StmtBlockStmtComponentData
{
public:
  typedef enum {
    SELECT,                     //!< The case selector.
    ITEM_CONDITION,             //!< The set of conditions for a particular item.
    ITEM_CLAUSE                 //!< The statements associated with a particular case item.
  } ComponentType;

  //! Constructor.
  StmtBlockCaseComponentData(StmtBlockGraph::Node* compound_case_node,
                             NUCaseItem * item,
                             ComponentType component_type) :
    StmtBlockStmtComponentData(compound_case_node),
    mItem(item),
    mComponentType(component_type)
  {
    // mItem is NULL iff this component is a select.
    INFO_ASSERT( (mItem==NULL) == (mComponentType==SELECT),
                 "Statement block graph consistency error." );
  }
  
  //! Destructor.
  virtual ~StmtBlockCaseComponentData() {}

  //! Is this a component of a case statement?
  virtual bool isCaseComponent() const { return true; }

  NUCaseItem * getItem() const { return mItem; }

  //! Return the type of component.
  ComponentType getComponentType() const { return mComponentType; }

  //! Return a printable string for this component type.
  virtual const char * getComponentTypeString() const {
    switch(mComponentType) {
    case SELECT:         return "SELECT";
    case ITEM_CONDITION: return "ITEM_CONDITION";
    case ITEM_CLAUSE:    return "ITEM_CLAUSE";
    default:          return "UNKNOWN";
    }
  }
private:
  //! The particular case item associated with this node.
  /*!
    Must be NULL if the component-type is SELECT.
   */
  NUCaseItem * mItem;

  //! The component type represented by this node.
  ComponentType mComponentType;
};

//! Data for if-statement graph nodes.
class StmtBlockIfData : public StmtBlockCompoundStmtData
{
public:
  //! Constructor
  StmtBlockIfData(NUIf * if_stmt);

  //! Destructor
  virtual ~StmtBlockIfData() {}

  //! Is an if statement?
  virtual bool isIfStatement() const { return true; }

private:
};

//! Data for case-statement graph nodes.
class StmtBlockCaseData : public StmtBlockCompoundStmtData
{
public:
  //! Constructor
  StmtBlockCaseData(NUCase * case_stmt);

  //! Destructor
  virtual ~StmtBlockCaseData() {}

  //! Is a case statement?
  virtual bool isCaseStatement() const { return true; }

private:
};


//! A class to write StmtBlockGraphs in Dot format
class StmtBlockGraphDotWriter : public GraphDotWriter
{
public:
  StmtBlockGraphDotWriter(UtOStream& os) : GraphDotWriter(os) {}
  StmtBlockGraphDotWriter(const UtString& filename) : GraphDotWriter(filename) {}

  //! Static all-in-one convenience function callable from the debugger
  static void writeGraphToFile(StmtBlockGraph* graph, const char* filename)
  {
    UtString fname(filename);
    StmtBlockGraphDotWriter sgdw(fname);
    sgdw.output(graph);
  }

  static void writeGraphToStream(StmtBlockGraph* graph, UtOStream & os)
  {
    StmtBlockGraphDotWriter sgdw(os);
    sgdw.output(graph);
  }

private:
  //! Write the text for a given node.
  /*!
    Nodes representing compound statements end up creating two graph
    DOT constructs. First, we write a cluster node for this node and
    add all of its component parts to the cluster. Second, we add a
    node representing the complete compound statement.

    Non-compound nodes will use the base class implementation.
   */
  virtual void printNode(Graph* g, GraphNode* node);

  //! A node's label is the Verilog text of the statement.
  /*! 

    If this node represents one component of a compound statement, a
    label is printed describing the part of the statement it models.
    An if-select, for example, will have the label of "SELECT".

    Component statements will be joined through a subgraph cluster.

    \sa printNode
   */
  virtual void writeLabel(Graph* graph, GraphNode* node);

  //! An edge's label describes the dependency reason encoded in the scratch field
  virtual void writeLabel(Graph* graph, GraphEdge* edge);
};


/*!
  Factory to encapsulate the creation and destruction of
  statement-block graphs. 

  These graphs are not currently cached.

  TBD: The StmtBlockGraph should replace all consumers of StmtGraph
  (see localflow/Reorder).
 */
class StmtBlockGraphFactory
{
public:
  //! Constructor
  StmtBlockGraphFactory(NUNetRefFactory * net_ref_factory,
                        AllocAlias * alias_query,
                        bool expect_all_blocking) : 
    mNetRefFactory(net_ref_factory),
    mAliasQuery(alias_query),
    mExpectAllBlocking(expect_all_blocking)
  {}
  
  //! Destructor
  virtual ~StmtBlockGraphFactory() {}

  //! Given a list of statements, create a statement-block graph.
  StmtBlockGraph * create(const NUStmtList * stmts);

  //! Destroy a statement-block graph.
  /*!
    The graph referenced by pgraph is deleted and the pgraph pointer
    is NULLed.

    All data embedded within the provided graph is properly destroyed
    before the graph itself is deleted.
   */
  void destroy(StmtBlockGraph ** pgraph);
private:
  //! Hide copy and assign constructors.
  StmtBlockGraphFactory(const StmtBlockGraphFactory&);
  StmtBlockGraphFactory& operator=(const StmtBlockGraphFactory&);

  typedef NUNetRefMultiMap<StmtBlockGraph::Node*>  NUNetRefStmtBlockGraphNodeMultiMap;

  typedef AllocAliasBucketNetRefMap<NUNetRefStmtBlockGraphNodeMultiMap> AliasBucketNetRefStmtBlockGraphMap;

  //! Create all the graph nodes associated with a single statement.
  /*!
    Compound statements, like if and case, will produce multiple graph
    nodes.
    
    \sa createIfComponentNode
    \sa createCaseComponentNode
   */
  StmtBlockGraph::Node* createNodeAndComponents(StmtBlockGraph * graph, 
                                                NUStmt * stmt);

  //! Create a graph node with the specified data and add to the graph.
  StmtBlockGraph::Node * createNodeWithData(StmtBlockGraph * graph,
                                            StmtBlockStmtData * node_data);

  //! Create the graph node and its associated data for one component of an if statement.
  /*!
    \sa StmtBlockIfComponentData
   */
  void createIfComponentNode(StmtBlockGraph * graph,
                             StmtBlockGraph::Node* stmt_node,
                             StmtBlockIfData *stmt_data,
                             StmtBlockIfComponentData::ComponentType component_type);

  //! Create the graph node and its associated data for one component of a case statement.
  /*!
    \sa StmtBlockCaseComponentData
   */
  void createCaseComponentNode(StmtBlockGraph * graph,
                               StmtBlockGraph::Node* stmt_node,
                               StmtBlockCaseData *stmt_data,
                               NUCaseItem * item,
                               StmtBlockCaseComponentData::ComponentType component_type);

  //! Given a graph node, determine and add all the necessary RAW, WAW, WAR dependencies.
  /*!
    If a node is an if or case statement, dependency arcs is added for
    each of the associated components.
   */
  void updateDependencies(StmtBlockGraph * graph,
                          StmtBlockGraph::Node* stmt_node,
                          AliasBucketNetRefStmtBlockGraphMap* block_def_map,
                          AliasBucketNetRefStmtBlockGraphMap* non_block_def_map,
                          AliasBucketNetRefStmtBlockGraphMap* live_use_map);

  //! Add all the RAW, WAR, WAW dependency arcs as specified in the provided use/def information.
  void updateDependenciesForNode(StmtBlockGraph * graph,
                                 StmtBlockGraph::Node* stmt_node,
                                 NUNetRefSet & defs,
                                 NUNetRefSet & uses,
                                 AliasBucketNetRefStmtBlockGraphMap* block_def_map,
                                 AliasBucketNetRefStmtBlockGraphMap* non_block_def_map,
                                 AliasBucketNetRefStmtBlockGraphMap* live_use_map);

  //! Update the dependency arcs for all sub-components of this if-statement node.
  void updateDependenciesForIfComponents(StmtBlockGraph * graph,
                                         StmtBlockGraph::Node* stmt_node,
                                         AliasBucketNetRefStmtBlockGraphMap* block_def_map,
                                         AliasBucketNetRefStmtBlockGraphMap* non_block_def_map,
                                         AliasBucketNetRefStmtBlockGraphMap* live_use_map);

  //! Update the dependency arcs for all sub-components of this case-statement node.
  void updateDependenciesForCaseComponents(StmtBlockGraph * graph,
                                           StmtBlockGraph::Node* stmt_node,
                                           AliasBucketNetRefStmtBlockGraphMap* block_def_map,
                                           AliasBucketNetRefStmtBlockGraphMap* non_block_def_map,
                                           AliasBucketNetRefStmtBlockGraphMap* live_use_map);

  //! Calculate the defs and visible uses for a given statement list.
  void calculateDefsAndUsesForClause(NUStmtLoop loop, 
                                     NUNetRefSet * defs,
                                     NUNetRefSet * uses);

  //! Calculate the dependency information and add all the relevant arcs to this node representing a statement-list.
  /*!
    Used by if and case-clause processing.
   */
  void updateDependenciesForClause(StmtBlockGraph * graph,
                                   StmtBlockGraph::Node* component_node,
                                   NUStmtLoop loop,
                                   AliasBucketNetRefStmtBlockGraphMap* block_def_map,
                                   AliasBucketNetRefStmtBlockGraphMap* non_block_def_map,
                                   AliasBucketNetRefStmtBlockGraphMap* live_use_map);


  //! Add dependencies to the graph for current net refs that intersect prior net refs
  void addDependencies(StmtBlockGraph* graph, StmtBlockGraph::Node* stmtNode, UInt32 edgeType,
                       NUNetRefSet& currentNetRefs,
                       AliasBucketNetRefStmtBlockGraphMap* priorNetRefMap,
                       AliasBucketNetRefStmtBlockGraphMap* currentNetRefMap);

  //! Update the net ref sets to reflect the defs and kills 
  void processDefsAndKills(StmtBlockGraph::Node* stmtNode, NUNetRefSet& defs, NUNetRefSet& kills,
                           AliasBucketNetRefStmtBlockGraphMap* defNetRefMap,
                           NUNetRefStmtBlockGraphNodeMultiMap* secondaryNetRefMap);

  //! Remove edges due to net ref pessimism
  void removePessimisticNetRefEdges(StmtBlockGraph* graph);

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! Allocation alias query.
  AllocAlias *mAliasQuery;

  //! Are we running after non-blocking reordering has occurred?
  bool mExpectAllBlocking;
};

#endif
