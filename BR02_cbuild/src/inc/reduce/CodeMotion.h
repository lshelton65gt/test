// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CODE_MOTION_H_
#define CODE_MOTION_H_

#include "nucleus/Nucleus.h"

#include "reduce/StmtBlockGraph.h"
typedef UtMap<StmtBlockGraph::Node*,StmtBlockGraph::Node*> StmtBlockGraphNodeToNodeMap;


/*!
  \file
  Declaration of code motion package.
 */

class AllocAlias;
class ArgProc;
class AtomicCache;
class CodeMotionStatistics;
class Fold;
class IODBNucleus;
class MsgContext;
class NUNetRefFactory;
class Reorder;

//! CodeMotion class
/*!
 * Class to determine which statements can be moved into adjacent
 * control structures.
 *
 * Currently we only move statements into following control structures
 * (If/Case) if the original statment can be removed from its original
 * location and it can be placed within the control structure.
 *
 * The following example can be transformed (assuming no other
 * consumers of 'x'):
 *
 \code
  begin
    x = a & b;
    if (sel) begin
      y = x | z;
    end
  end
 \endcode
 *
 * Into:
 *
 \code
  begin
    if (sel) begin
      x = a & b;
      y = x | z;
    end
  end
 \endcode
 * 
 */
class CodeMotion
{
public:

  enum {
    cDefExprMotionThreshold = 500 //!< default threshold to limit the cost of 
                                  //!< expression that can be moved.
  };

  //! Constructor
  /*!
    \param str_cache      The string cache.
    \param netref_factory The NUNetRef factory.
    \param msg_ctx        Message Context.
    \param iodb           The IODB.
    \param verbose        Print summary information?
    \param stats          Collection of summary information across calls, managed by caller.
  */
  CodeMotion(AtomicCache * str_cache,
             NUNetRefFactory *netref_factory,
	     MsgContext * msg_ctx,
	     IODBNucleus * iodb,
             ArgProc* args,
             AllocAlias * alloc_alias,
             Fold * fold,
             bool move_into_expressions,
             SInt32 expr_motion_threshold,
	     bool verbose,
	     CodeMotionStatistics *stats);

  //! Destructor
  ~CodeMotion();

  //! Walk a module and perform code motion over each embedded always block.
  /*!
    \param this_module The module for processing.
    \returns true If any rewrites were successful.
   */
  bool module(NUModule * this_module);

  //! Walk a vector of NUUseDefNode and perform code-motion.
  /*!
    This is a specialized for post-block-merging.

    \param this_module The module containing the always blocks in 'blocks'.
    \param blocks      Vector of NUUseDefNode, which must be NUAlwaysBlock.
    \returns true If any rewrites were successful.
  */
  bool mergedBlocks(NUModule * this_module,
		    NUUseDefVector & blocks);

private:
  //! Process one always block
  /*!
    \param always The always block for processing.
  */
  bool alwaysBlock(NUAlwaysBlock * always);

  //! Process one task.
  bool tf(NUTask * task);

  //! Recursively process one statement list.
  /*!
   * Recursively walk the given statement list, moving statements into nested
   * statements where possible.
   *
   * \param stmts The statement list for processing; modified in-place.
   *
   * \param uplevel_uses The set of nets which are used by subsequent
   *     statements at higher levels.
   *
   * \param replace_stmt Internally set to true if the given immediate
   *     statement list was modified, false otherwise.
   *
   * \return true if any stmt was moved, either in this list or in a
   *     nested statement.
   */
  bool stmtList(NUStmtList & stmts,
		const NUNetRefSet *uplevel_uses,
		bool &replace_stmts);

  //! Handle one-level of motion.
  /*!
   * \param stmts The statement list for processing; modified in-place.
   *
   * \param uplevel_uses The set of nets which are used by subsequent
   *     statements at higher levels.
   *
   * \return true if any stmt was moved in this local list.
   */
  bool stmtListLocal(NUStmtList & stmts,
                     const NUNetRefSet *uplevel_uses);
  
  //! Update all edges based on performed merges.
  void updateDestinations(StmtBlockGraph * graph, 
                          StmtBlockGraph::Node * node,
                          StmtBlockGraphNodeToNodeMap * destination_map);

  //! Remember a performed merge.
  void rememberDestination(StmtBlockGraph::Node * node,
                           StmtBlockGraph::Node * destination,
                           StmtBlockGraphNodeToNodeMap * destination_map);

  typedef UtMap<NUStmt*,UInt32> StmtPositionMap;

  //! Given a statement node, determine if its edges reference a mergable point.
  /*!

    The provided node is analyzed for a legal merge. First, we
    determine if there is a valid merge destination. If a destination
    is discovered and the defs for our candidate 'node' are legal for
    motion (no up-level use), the statement for 'node' is merged into
    its destination.

    The destination_map is updated if a successful merge is performed.

    \return true iff a merge occurred.
   */
  bool mergeNodes(StmtBlockGraph * graph, 
                  StmtBlockGraph::Node * node,
                  const NUNetRefSet & local_kills,
                  const NUNetRefSet & uplevel_uses,
                  StmtPositionMap * position,
                  StmtBlockGraphNodeToNodeMap * destination_map);

  //! Given a statement node, find its single mergable destination node.
  /*!
    We process the edge set twice. First, we check to see if all RAW
    edges terminate at the same destination. Given that destination,
    we check to see that no WAW and WAR dependency is invalidated by
    using this destination.

    \return destination node, if valid. NULL otherwise.
   */
  StmtBlockGraph::Node * findMergableDestination(StmtBlockGraph * graph, 
                                                 StmtBlockGraph::Node * node,
                                                 StmtPositionMap * position);

  //! Recursive processing of statement lists.
  bool stmtListRecurse(NUStmtList & stmts,
                       const NUNetRefSet *uplevel_uses);

  //! Determine if each referenced net is a valid candidate for motion.
  /*!
    \param defs A set of net references for qualification.
  */
  bool qualifyDefsForMotion(const NUNetRefSet & defs, const NUNetRefSet & kills) const;

  //! Attempt to move a candidate statement into a control statement.
  /*!
    \param destination The graph node for the target statement, potentially part of a control statement.
    \param candidate The statement under consideration for motion.
    \param dumped_header Have we printed the debug trace header for this statement?

    \return true if the candidate was successfully moved into the target statement.
   */
  bool moveInto(StmtBlockGraph::Node * destination, 
                NUStmt * candidate,
                bool dumped_header);

  //! Attempt to move a candidate statement into an if() statement.
  /*!
    \param if_component_data The graph node for the component of the target if() statement.
    \param candidate The statement under consideration for motion.
    \param dumped_header Have we printed the debug trace header for this statement?

    \return true if the candidate was successfully moved into the target statement.
   */
  bool moveIntoIf(StmtBlockIfComponentData * if_component_data,
                  NUStmt * candidate, 
                  bool dumped_header);

  //! Attempt to move a candidate statement into a case() statement.
  /*!
    \param case_component_data The graph node for the component of the target case() statement.
    \param candidate The statement under consideration for motion.
    \param dumped_header Have we printed the debug trace header for this statement?

    \return true if the candidate was successfully moved into the target statement.
   */
  bool moveIntoCase(StmtBlockCaseComponentData * case_component_data,
                    NUStmt * candidate,
                    bool dumped_header);

  //! Attempt to move a candidate statement into a block.
  /*!
    \param block_stmt The target block.
    \param candidate The statement under consideration for motion.
    \param dumped_header Have we printed the debug trace header for this statement?

    \return true if the candidate was successfully moved into the target statement.
   */
  bool moveIntoBlock(NUBlock * block_stmt,
                     NUStmt * candidate, 
                     bool dumped_header);

  //! Attempt to move the RHS of a candidate statement into an assignment.
  /*!
    This is single-use propagation.

    \param assign The target assignment.
    \param candidate The statement under consideration for motion.
    \param dumped_header Have we printed the debug trace header for this statement?

    \return true if the candidate was successfully moved into the target statement.
   */
  bool moveIntoAssign(NUBlockingAssign * assign, 
                      NUStmt * candidate, 
                      bool dumped_header);

  NUExpr * extendRHS(NULvalue * lhs, NUExpr * rhs);

  //! Determine if the given expression into which a new expression will get moved
  //! into has hit the maximum expression size threshold.
  bool qualifyTargetExpr(NUExpr* expr);

  //! Determine if this assignment can have its expression propagated.
  bool qualifyCandidateAssign(NUStmt * candidate);

  //! Propagate the RHS of our candidate statement into the target assignment.
  bool rewriteTargetAssign(NUAssign * target,
                           NUStmt * candidate);

  //! Propagate the RHS of our candidate expression into the target assignment.
  NUExpr * rewriteTargetExpr(NUExpr * target,
                             NUStmt * candidate);

  //! Recurse through statements and move nested statements where possible.
  /*!
   * \param stmt A statement which potentially contains nested statements.
   * \param uplevel_uses The set of nets used by subsequent statements
   *     at higher levels.
   *
   * \return true if any stmt was moved, false otherwise..
   */
  bool stmt(NUStmt * stmt, const NUNetRefSet *uplevel_uses);

  //! Recurse through blocks and move nested statements where possible.
  /*!
   * \param block The block for processing.
   * \param uplevel_uses The set of nets used by subsequent statements
   *     at higher levels.
   *
   * \return true if any stmt was moved, false otherwise..
   */
  bool block(NUBlock * block, const NUNetRefSet *uplevel_uses);

  //! Recurse through if() statements and move nested statements where possible.
  /*!
   * \param stmt The if() statement for processing.
   * \param uplevel_uses The set of nets used by subsequent statements
   *     at higher levels.
   *
   * \return true if any stmt was moved, false otherwise..
   */
  bool ifStmt(NUIf *stmt, const NUNetRefSet *uplevel_uses);

  //! Recurse through case() statements and move nested statements where possible.
  /*!
   * \param stmt the case() statement for processing.
   * \param uplevel_uses The set of nets used by subsequent statements
   *     at higher levels.
   *
   * \return true if any stmt was moved, false otherwise..
   */
  bool caseStmt(NUCase *stmt, const NUNetRefSet *uplevel_uses);

  //! Recurse through for() statements and move nested statements where possible.
  /*!
   * \param stmt the for() statement for processing.
   * \param uplevel_uses The set of nets used by subsequent statements
   *     at higher levels.
   *
   * \return true if any stmt was moved, false otherwise..
   */
  bool forStmt(NUFor *stmt, const NUNetRefSet *uplevel_uses);

  //! Does some expression use any NUNetRef in our set?
  /*!
   * \param net_refs A set of NUNetRefs
   * \param expr     The expression which may use one of the NUNetRefs.
   * 
   * \return true if the 'expr' uses any NUNetRef in 'net_refs'.
   */
  bool usedBy(NUNetRefSet & net_refs, NUExpr * expr);

  //! Does any expression under this LHS use any NUNetRef in our set?
  /*!
   * \param net_refs A set of NUNetRefs
   * \param lhs      The LHS which may use one of the NUNetRefs.
   * 
   * \return true if the 'expr' uses any NUNetRef in 'net_refs'.
   */
  bool usedBy(NUNetRefSet & net_refs, NULvalue * lhs);

  //! Update the Use/Def information to reflect a successful code motion.
  /*! 
    This is necessary because we need our uses to be close to accurate
    (in this case, a superset) for correct computation of
    uplevel_uses.

    For example, given the following case:

    always begin
      if (en1) begin
        t1 = a & b;
        if (en2) begin
          t2 = t1 & c;
        end
      end
      t3 = t1 & d;
      if (en3) begin
        t4 = t2 & t3;
      end else begin
        t4 = t2;
      end
    end

    We can move t3 into the first branch of the en3 if statement. This
    if statement now looks like:
      if (en3) begin
        t3 = t1 & d;
        t4 = t2 & t3;
      end else begin
        t4 = t2;
      end

    If getUses for this modified if statement does not return that t1
    is used, we will not recognize that t1 has a use at some higher
    level and could incorrectly move the t1 assignment into the en2 if
    statement.

    This testcase is test/motion/btop.v. The test/cust/star/ba/btop
    design had comparison failures when we did not update UD after
    motion.

    NOTE: This routine results in a pessimistic view of the use/def
    information for a particular block. Satisfied kills are not
    eliminated from the use sets. In the above example, the en3 if
    statement will still appear to have an unsatisfied use for t3.

    UD _MUST_ be recomputed after CodeMotion completes its pass over
    the current block.

    \param candidate
    \param parent
   */
  void promoteUD(NUStmt * parent,
                 NUNetRefSet & candidate_defs,
                 NUNetRefSet & candidate_uses);

  //! Dump information about a candidate statement.
  void dumpMotionHeader(NUStmt * candidate);

  //! Generic trace method.
  void dumpMotion(NUStmt * candidate, 
                  const SourceLocator & loc,
                  const char * nucleus_type_str, 
                  const char * location, 
                  bool dumped_header);

  //! Specific trace method to dump information about a successful move into a single case item.
  void dumpMotion(NUStmt * candidate, NUCase * case_stmt, NUCaseItem * item, bool dumped_header);

  NUStmtList mDeleteList;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;
    
  //! Message context
  MsgContext * mMsgContext;

  //! IODB.
  IODBNucleus * mIODB;

  //! args
  ArgProc* mArgs;

  //! Fold
  Fold * mFold;

  //! Reordering interface.
  StmtBlockGraphFactory * mGraphFactory;

  CodeMotionStatistics * mStatistics;

  //! Perform copy-propagation into expressions?
  bool mMoveIntoExpressions;

  //! The limit on the size of expressions that can be moved
  SInt32 mExprMotionThreshold;

  //! Be verbose as processing occurs?
  bool mVerbose;

  //! Debug the graph?
  bool mDebug;
};


//! The CodeMotionStatistics class.
class CodeMotionStatistics
{
public:
  //! Constructor
  CodeMotionStatistics() { clear(); }

  //! Destructor
  ~CodeMotionStatistics() {}

  //! Increment the number of affected modules.
  void module() { ++mModules; }

  //! Increment the number of moved statements.
  void statement() { ++mStatements; }

  //! Increment the number of statements copied into expressions.
  void expression() { ++mExpressions; }

  //! Clear any accumulated information.
  void clear() { mModules=0; mStatements=0; mExpressions=0; }

  //! Print all accumulated information.
  void print() const;

private:
  //! The number of affected modules.
  SInt64 mModules;

  //! The number of statements moved into other statements.
  SInt64 mStatements;

  //! The number of statements copied into expressions.
  SInt64 mExpressions;
};

#endif // CODE_MOTION_H_
