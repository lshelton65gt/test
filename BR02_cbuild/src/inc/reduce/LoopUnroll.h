// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef LOOP_UNROLL_H_
#define LOOP_UNROLL_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUExpr.h"

/*!
  \file
  Declaration of loop unrolling/optimization package.
 */

class NUNetRefFactory;
class MsgContext;
class AtomicCache;
class ArgProc;

class NUCostContext;
class UD;
class Inference;
class Ternary;
class Fold;

class InferenceStatistics;
class TernaryStatistics;
class LoopReplicator;
class LoopDistributor;
class InvariantExtractor;
class LoopUnrollStatistics;

//! Discover loops and replace them with their unrolled equivalents.
class LoopUnroll
{
public:
  //! Constructor
  LoopUnroll(AtomicCache * str_cache,
             NUNetRefFactory * netref_factory,
             MsgContext * msg_context,
             IODBNucleus * iodb,
             ArgProc * args,
             bool force,
             bool verbose,
             LoopUnrollStatistics *stats,
             InferenceStatistics *inference_stats,
             TernaryStatistics *ternary_stats);

  //! Destructor
  ~LoopUnroll();

  //! Walk a module and unroll loops.
  bool module(NUModule * one);
  //! Walk an always block and unroll loops.
  bool always(NUAlwaysBlock* one_always);

private:

  //! Does the body of any embedded for() loop redefine its index variable?
  /*!
    Only NUBlock and NUTF types are supported.
   */
  bool qualify(NUUseDefNode * scope);

  //! A precheck to make sure we find optimizable statements.
  bool estimateBeneficial(NUFor* stmt);

  //! Methods to perform loop unrolling.
  bool unroll(NUStmt * stmt, NUStmtList & replacements, NUStmtList & delete_stmts); 
  bool unroll(NUTF * tf, NUStmtList & delete_stmts);
  bool unroll(NUBlock * block, NUStmtList & delete_stmts);
  bool unroll(NUIf * stmt, NUStmtList & delete_stmts);
  bool unroll(NUCase * stmt, NUStmtList & delete_stmts);
  bool unroll(NUFor * stmt, NUStmtList & replacements, NUStmtList & delete_stmts);
  bool unroll(NUStmtLoop loop,
	      NUStmtList & replacements, 
	      NUStmtList & delete_stmts);

  //! Unroll both the innards of the loop and the loop itself.
  bool doubleUnroll(NUFor * stmt, 
                    NUStmtList & replacements,
                    NUStmtList & delete_stmts);

  //! Methods to perform other sorts of loop unrolling (distribution, hoisting, etc).
  bool optimize(NUStmt * stmt, NUStmtList & delete_stmts);
  bool optimize(NUTF * tf, NUStmtList & delete_stmts);
  bool optimize(NUBlock * block, NUStmtList & delete_stmts);
  bool optimize(NUIf * stmt, NUStmtList & delete_stmts);
  bool optimize(NUCase * stmt, NUStmtList & delete_stmts);
  bool optimize(NUFor * stmt, NUStmtList & delete_stmts);
  bool optimize(NUStmtLoop loop, NUStmtList & replacements, NUStmtList & delete_stmts);

  bool test(NUExpr * original_condition, 
	    NUNet  * net,
	    NUExpr * current);
  NUExpr * advance(NUExpr * original_target,
		   NUNet * net,
		   NUExpr * current);
  NUExpr * evaluate(NUExpr * original_target,
		    NUNet  * net,
		    NUExpr * current);

  //! Replicate control structures while isolating definitions of different nets.
  bool replicate(NUStmtLoop loop,
		 NUStmtList & replacements);

  //! Perform the loop distribution operation over a stmt loop.
  /*!
   * replacements is populated as we iterate, even if nothing is
   * distributed.
   *
   * return true if any distribution occurred.
   */
  bool distribute(NUStmtLoop loop,
		  NUStmtList & replacements,
		  NUStmtList & delete_stmts);

  bool extract(NUStmtLoop loop,
	       NUStmtList & replacement,
	       NUStmtList & delete_stmts);

  void copy(NUStmtLoop loop,
	    NUStmtList & target,
	    NUNet * net,
	    NUExpr * current);

  bool checkUses(NUExpr * fn, NUNet *net) const;
  void updateSignedness(NUExpr * fn);

  bool advantageous(NUFor * stmt, NUStmtList & unrolled, int iterations);

  //! Print that an unrolling operation failed because it was estimated to need too many iterations.
  void dumpEstimatedIterationFailure(const SourceLocator & loc, SInt32 estimated_loop_count);

  //! Print that unrolling estimated that the size was too high
  void dumpEstimatedUnrollSizeFailure(const SourceLocator& loc, SInt32 cost);

  //! Print that an unrolling operation failed because it needed too many iterations.
  void dumpIterationFailure(const SourceLocator & loc, const char * estimated=NULL);

  UtVector<NUBlockScope*> * mScopes;

  LoopReplicator       * mReplicator;
  LoopDistributor      * mDistributor;
  InvariantExtractor   * mExtractor;

  LoopUnrollStatistics * mStatistics;

  AtomicCache     * mStrCache;
  NUNetRefFactory * mNetRefFactory;
  MsgContext      * mMsgContext;
  ArgProc         * mArgs;

  NUCostContext * mCosts;
  UD        * mUD;
  Ternary   * mTernary;
  Inference * mInference;
  Fold      * mFold;

  NUStmtList mDeleteStmts;

  //! If set, unroll all loops whether or not we think it is advantageous
  bool mForceUnroll;

  //! Be verbose as processing occurs?
  bool mVerbose;

  //! Have we unrolled any loops?
  bool mUnrolled;

  //! Have we optimized any loops?
  bool mOptimized;

  //! Should we distribute loops?
  bool mAllowDistribution;

  //! Should we extract invariants?
  bool mAllowExtraction;

  //! Should we replicate control structures?
  bool mAllowReplication;

  //! Did some Fold optimization occur, requiring us to recompute UD?
  bool mForceUDUpdate;

  //! Depth of loop unrolling in action
  SInt32 mLoopDepth;
};


//! Copy functor for for() loops.
class LoopFactory
{
public:
  LoopFactory(AtomicCache * str_cache,
	      NUNetRefFactory * netref_factory) :
    mStrCache(str_cache),
    mNetRefFactory(netref_factory) 
  {}

  ~LoopFactory() {}

  //! Create a copy of the for() loop with the specified body.
  NUFor * operator()(NUFor * for_stmt, NUStmtList * body_stmts);
private:
  AtomicCache * mStrCache;
  NUNetRefFactory * mNetRefFactory;
};


//! Replicate loop control structures to make other optimizations simpler.
/*!
 * The general rule for this optimization is:
 *
 \code
   for (i=0; i<128; i=i+1) begin
     if (SELECT) begin
       STMT_A
       STMT_B
     end
   end
 \endcode
 *
 * This is transformed into:
 *
 \code
   for (i=0; i<128; i=i+1) begin
     if (SELECT) begin
       STMT_A
     end
     if (SELECT) begin
       STMT_B
     end
   end
 \endcode
 *
 */
class LoopReplicator
{
public:
  //! Constructor
  LoopReplicator(AtomicCache * str_cache,
		 NUNetRefFactory * netref_factory,
		 UD * ud,
		 bool verbose) :
    mStrCache(str_cache),
    mNetRefFactory(netref_factory),
    mUD(ud),
    mVerbose(verbose) {}

  //! Destructor
  ~LoopReplicator() {};
  
  //! Perform the replication operation within a for() loop.
  bool replicate(NUFor * for_stmt);
private:
  bool replicate(NUStmt * stmt, NUStmtList & my_replacements);
  bool qualify(NUStmt * stmt, NUNetRefSet & def_set);
  void reduce(NUStmt * stmt, NUNetRefHdl & def_net_ref);

  AtomicCache * mStrCache;
  NUNetRefFactory * mNetRefFactory;
  UD * mUD;
  bool mVerbose;
};


//! Perform the loop distribution operation.
/*!
 * The general rule for this optimization is:
 *
 \code
   for (i=0; i<128; i=i+1) begin
     STMT_A
     STMT_B
   end
 \endcode
 *
 * This is transformed into:
 *
 \code
   for (i=0; i<128; i=i+1) begin
     STMT_A
   end
   for (i=0; i<128; i=i+1) begin
     STMT_B
   end
 \endcode
 *
 */
class LoopDistributor
{
public:
  //! Constructor
  LoopDistributor(AtomicCache * str_cache,
		  NUNetRefFactory * netref_factory,
		  UD * ud,
		  bool verbose) : 
    mStrCache(str_cache),
    mNetRefFactory(netref_factory),
    mUD(ud),
    mVerbose(verbose) {}

  ~LoopDistributor() {}

  //! Perform the loop distribution operation if legal.
  /*!
   * results is populated and for_stmt is given an empty body if distribution occurs.
   */
  bool distribute(NUFor * for_stmt, NUStmtList & results);
private:
  //! Is distribution of this for() loop legal?
  bool qualify(const NUFor * for_stmt) const;

  //! Create a copy of a for() stmt with only a single body element.
  NUFor * copy(NUFor * for_stmt, NUStmt * body_stmt);

  AtomicCache * mStrCache;
  NUNetRefFactory * mNetRefFactory;
  UD * mUD;
  bool mVerbose;
};


//! Interface to rewrite invariant control statements within a loop.
/*!
 * A 'variant' expression depends on values computed within a loop.
 * Most commonly, these expressions use the index variable. The 'if'
 * statement in the following example has a variant control
 * expression:
 *
 \code
   for (i=0; i<128; i=i+1) begin
     if (sel[i]) begin
       out[i] = in[i];
     end
   end
 \endcode
 *
 *
 * An 'invariant' expression does not use nets defined within the
 * loop. The 'if' statement in the following example has an invariant
 * control expression.
 *
 \code
   for (i=0; i<128; i=i+1) begin
     if (sel) begin
       out[i] = in[i];
     end
   end
 \endcode
 *
 * This class rewrites loops which contain invariant control
 * structures. The invariant example above can be transformed into:
 *
 \code
   if (sel) begin
     for (i=0; i<128; i=i+1) begin
       out[i] = in[i];
     end
   end
 \endcode
 * 
 * The select remains unchanged as the loop iterates. As a result, the
 * code is functionally equivalent when the 'if' statement is moved
 * outside the loop.
 *
 * In general, the invariant transformation is:
 *
 \code
   for (i=0; i<128; i=i+1) begin
     if (INVARIANT) begin
       THEN_CLAUSE
     end else begin
       ELSE_CLAUSE
     end
   end
 \endcode
 *
 * This becomes:
 *
 \code
   if (INVARIANT) begin
     for (i=0; i<128; i=i+1) begin
       THEN_CLAUSE
     end
   end else begin
     for (i=0; i<128; i=i+1) begin
       ELSE_CLAUSE
     end
   end
 \endcode
 *
 * Invariant extraction can occur for both 'case' and 'if' statements.
 *
 * Other rewriting techniques are included to expose invariant
 * transformations.
 * \sa InvariantExtractor::splitIfAndStatements
 * \sa InvariantExtractor::splitIfOrStatements
 * \sa InvariantExtractor::flipIfStatements
 */
class InvariantExtractor
{
public:
  //! Constructor
  InvariantExtractor(AtomicCache * str_cache,
                     NUNetRefFactory * netref_factory,
                     UD * ud,
                     UtVector<NUBlockScope*> * scopes,
                     bool verbose) :
    mStrCache(str_cache),
    mNetRefFactory(netref_factory),
    mUD(ud),
    mScopes(scopes),
    mVerbose(verbose) {}

  //! Destructor
  ~InvariantExtractor() {}

  //! Perform invariant motion for the provided block stmt.
  /*!
   * Currently, this is only function when the block stmt is a for()
   * loop, but it can be extended to if() and case().
   * 
   * results is populated and stmt is given empty body/branches.
   */
  bool extract(NUBlockStmt * stmt, NUStmtList & results);
private:
  //! Perform invariant motion for the provided for() loop.
  bool extract(NUFor * for_stmt, NUStmtList & results);

  //! Extract an invariant case statement out of another block stmt.
  void extract(NUCase * case_stmt, NUBlockStmt * block_stmt);

  //! Extract an invariant if statement out of another block stmt.
  void extract(NUIf * if_stmt, NUBlockStmt * block_stmt);

  //! Is invariant motion of this for() loop legal?
  bool qualify(const NUFor * for_stmt) const;

  //! Split an embedded if statement into variant and invariant components.
  /*!
   * The general rule for this transformation is:
   *
   \code
     for (i=0; i<128; i=i+1) begin
       if (VARIANT && INVARIANT) begin
         THEN_CLAUSE
       else
         ELSE_CLAUSE
       end
     end
   \endcode
   *
   * This becomes:
   *
   \code
     for (i=0; i<128; i=i+1) begin
       if (INVARIANT) begin
         if (VARIANT) begin
           THEN_CLAUSE
         end else begin
           ELSE_CLAUSE
         end
       end else begin
         ELSE_CLAUSE (copy)
       end
     end
   \endcode
   *
   * If the ELSE_CLAUSE is non-empty, statement copies are created.
   * The resulting statement structure allows an invariant
   * transformation to occur.
   */
  void splitIfAndStatements(NUFor* for_stmt);

  //! Split an embedded if statement into variant and invariant components.
  /*!
   * The general rule for this transformation is:
   *
   \code
     for (i=0; i<128; i=i+1) begin
       if (VARIANT || INVARIANT) begin
         THEN_CLAUSE
       else
         ELSE_CLAUSE
       end
     end
   \endcode
   *
   * This becomes:
   *
   \code
     for (i=0; i<128; i=i+1) begin
       if (INVARIANT) begin
         THEN_CLAUSE (copy)
       end else begin
         if (VARIANT) begin
           THEN_CLAUSE
         end else begin
           ELSE_CLAUSE
         end
       end
     end
   \endcode
   *
   * If the THEN_CLAUSE is non-empty, statement copies are created.
   * The resulting statement structure allows an invariant
   * transformation to occur.
   */
  void splitIfOrStatements(NUFor* for_stmt);
  
  //! Split an expression into variant and invariant components.
  /*!
    \param condition The original expression.
    \param defs      Nets defined within the loop.
    \param eq_op_type  Operator such that: 
                       (a op b) == 1'b0
                       can be converted into:
                       (a == 1'b0) bitop (b == 1'b0)
                       If 'bitop_op_type' is &, 'eq_op_type' should be '|'.
    \param log_op_type   Logical operator (ie. &&)
    \param bitop_op_type Bitwise operator (ie. &)
    \param invariantCondition The invariant component of 'condition'.
    \param variantCondition The variant component of 'condition'.
    
    \return true if the expression was successfully split.
   */
  bool splitCondition(NUExpr * condition, NUNetSet & defs, 
                      NUOp::OpT eq_op_type,
                      NUOp::OpT log_op_type,
                      NUOp::OpT bit_op_type,
		      NUExpr *& invariantCondition,
		      NUExpr *& variantCondition);

  //! Recursively qualify an expression as valid for splitting.
  bool qualifyConditions(NUExpr * condition, NUOp::OpT op_type);

  //! Split a binary expression tree.
  void splitBinaryConditions(NUExpr * condition, 
                             NUOp::OpT op_type, 
			     NUNetSet& defs,
			     NUExprList& invariantConditions,
			     NUExprList& variantConditions);

  //! Create a binary tree for some operator.
  NUExpr * createBinaryOp(NUExprList & conditions, 
			  NUOp::OpT op_type,
			  const SourceLocator & loc);

  //! Create a bitwise-op/comparison tree for some conditions.
  NUExpr * createEqOp(NUExprList & conditions, 
                      NUOp::OpT op_type,
                      NUBinaryOp * old_condition);

  //! Create a binary-op tree for some conditions.
  NUExpr * createOp(NUExprList & conditions, 
                    NUOp::OpT op_type,
                    NUBinaryOp * old_condition);

  //! Move invariant if stmt out of variant stmt.
  /*!
   * \sa flipIfBalancedStatements
   * \sa flipIfThenStatements
   * \sa flipIfElseStatements
   */
  void flipIfStatements(NUFor* for_stmt);

  /*! 
   * Flip invariant if statements which occur within both the
   * then-clause and else-clause of a variant if statement.
   *
   * The general rule for this transformation is:
   * 
   \code
     for (i=0; i<128; i=i+1) begin
       if (VARIANT) begin
         if (INVARIANT) begin
           CLAUSE_A
         end else begin
           CLAUSE_B
         end
       end else begin
         if (INVARIANT) begin
           CLAUSE_C
         end else begin
           CLAUSE_D
         end
       end
     end
   \endcode
   *
   * If both invariant clauses are the same, this becomes:
   *
   \code
     for (i=0; i<128; i=i+1) begin
       if (INVARIANT) begin
         if (VARIANT) begin
           CLAUSE_A
         end else begin
           CLAUSE_C
         end
       end else begin
         if (VARIANT) begin
           CLAUSE_B
         end else begin
           CLAUSE_D
         end
       end
     end
   \endcode
   * 
   */
  void flipIfBalancedStatements(NUFor * for_stmt,
                                NUIf * if_stmt,
                                NUIf * then_if,
                                NUIf * else_if);

  /*! 
   * Flip an invariant if statement outside the then-clause of a
   * variant if statement.
   *
   * The general rule for this transformation is:
   *
   \code
     for (i=0; i<128; i=i+1) begin
       if (VARIANT) begin
         if (INVARIANT) begin
           CLAUSE_A
         end else begin
           CLAUSE_B
         end
       end else begin
         CLAUSE_C
       end
     end
   \endcode
   *
   * This becomes:
   *
   \code
     for (i=0; i<128; i=i+1) begin
       if (INVARIANT) begin
         if (VARIANT) begin
           CLAUSE_A
         end else begin
           CLAUSE_C
         end
       end else begin
         if (VARIANT) begin
           CLAUSE_B
         end else begin
           CLAUSE_C
         end
       end
     end
   \endcode
   */
  void flipIfThenStatements(NUFor * for_stmt,
                            NUIf * if_stmt,
                            NUIf * then_if);

  //! Flip an invariant if statement outside the else-clause of a variant if statement.
  /*!
   * The general rule for this transformation is:
   *
   \code
     for (i=0; i<128; i=i+1) begin
       if (VARIANT) begin
           CLAUSE_A
       end else begin
         if (INVARIANT) begin
           CLAUSE_B
         end else begin
           CLAUSE_C
         end
       end
     end
   \endcode
   *
   * This becomes:
   *
   \code
     for (i=0; i<128; i=i+1) begin
       if (INVARIANT) begin
         if (VARIANT) begin
           CLAUSE_A
         end else begin
           CLAUSE_B
         end
       end else begin
         if (VARIANT) begin
           CLAUSE_A
         end else begin
           CLAUSE_C
         end
       end
     end
   \endcode
   */
  void flipIfElseStatements(NUFor * for_stmt,
                            NUIf * if_stmt,
                            NUIf * else_if);

  //! Return the 'if' statement if the loop references a single-if-statement list.
  NUIf * getSingleIf(NUStmtLoop loop);

  //! Determine if an expression is invariant based on some set of definitions.
  bool isInvariant(NUExpr * expr, NUNetSet * defs, NUNetSet * other_uses=NULL) const;

  bool isConstantZero(NUExpr* expr);

  //! Copy an arbitrary block stmt with a specified list of body stmts.
  NUBlockStmt * copy(NUBlockStmt * block_stmt, NUStmtList * body_stmts);

  //! Create a copy of a for() stmt with a specified list of body stmts.
  NUFor * copy(NUFor * for_stmt, NUStmtList * body_stmts);

  AtomicCache * mStrCache;
  NUNetRefFactory * mNetRefFactory;
  UD * mUD;
  UtVector<NUBlockScope*> * mScopes;
  bool mVerbose;
};


class LoopUnrollStatistics
{
public:
  //! Constructor
  LoopUnrollStatistics() { clear(); }
  //! Destructor
  ~LoopUnrollStatistics() {}

  void module() { ++mModules; }
  void loop()   { ++mLoops; }
  void forced() { ++mForced; }
  void clear()  { mModules=0; mLoops=0; mForced=0; }
  void print() const;
private:
  SInt64 mModules;
  SInt64 mLoops;
  SInt64 mForced;
};
#endif
