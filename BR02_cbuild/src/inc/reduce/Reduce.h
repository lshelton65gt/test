// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef REDUCE_H_
#define REDUCE_H_

/*!
  \file
  Declaration file for the reduction package.
*/

#include "nucleus/Nucleus.h"
#include "flow/Flow.h"

class REAlias;
class REResolution;

class Inference;
class Rescope;
class Split;
class Stats;
class ReachableAliases;

class Reduce {
public:
  Reduce(Stats*, NUDesign*,
         bool verbose_cleanup,
         bool verbose_sanity,
         bool dump_x_hier,
         bool verbose_tri_init,
         bool dump_allocation,
         int internal_assert_test,
         FLNodeElabFactory* flow_elab_factory,
         FLNodeFactory* flow_factory,
         ReachableAliases* reachable_aliases,
         TristateModeT tristate_mode);
  
  bool runHierAlias();
  bool runReduce();

private:
  Stats* mStats;
  NUDesign* mDesign;
  bool mVerboseCleanup;
  bool mVerboseSanity;
  bool mDumpXHier;
  bool mVerboseTriInit;
  bool mDumpAllocation;
  int mInternalAssertTest;
  FLNodeElabFactory* mFlowElabFactory;
  FLNodeFactory* mFlowFactory;
  ReachableAliases* mReachableAliases;
  TristateModeT mTristateMode;
};

#endif
