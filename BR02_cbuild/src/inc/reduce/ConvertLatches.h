// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _CONVERTLATCHES_H_
#define _CONVERTLATCHES_H_

class FLNodeFactory;
class BDDContext;
class AtomicCache;
class MsgContext;
class IODBNucleus;
class UD;
class ArgProc;
class REClockExclusivity;

//! class to find and convert latches into flops
/*! This class converts a latch to a flop when the latch is used as a
 *  gate to a clock. The following is an example:
 *
 *    always @ (en or clk)
 *      if (~clk)
 *        ren <= en;
 *    assign gclk = clk & ren;
 *
 *    always @ (posedge gclk)
 *      q <= d;
 *
 *  Note that ren can only change when clk is zero and gclk is
 *  affected by ren when clk is one. Therefore any changes on the
 *  latch while it is open do not affect the simulation. The only
 *  change necessary is when clk rises. So the latch can be converted
 *  to a flop as follows:
 *
 *    always @ (posedge clk)
 *      ren <= en;
 *
 *  The new code looks for a continuous assign fed by a latch with the
 *  following conditions:
 * 
 *  1. The continuous assign only defines a single net of size 1.
 *
 *  2. The expression for the continuous is a logical AND of two
 *     expressions and those expressions are for one bit net references.
 *
 *  3. At least one of the continuous assign references is a latch
 *     that could be optimized (is not connected to a port or has been
 *     marked protected observable or mutable).
 *
 *  4. The latch is driven by one flow.
 * 
 *  5. The latch only defines a single net of size 1
 *
 *  6. The latch contains an if statement with at least one
 *     empty leg.
 *
 *  7. One leg of the and gate's expression and the latch
 *     enables expression can't both be high at the same time
 *     (proven using BDDs).
 *
 *  If all of the above is true, it converts the latch to a flop and
 *  marks the latch as non-wavable. That is because its value is no
 *  longer accurate at all times.
 */
class REConvertLatches
{
public:
  //! constructor
  REConvertLatches(FLNodeFactory* flowFactory, NUNetRefFactory* netRefFactory,
                   AtomicCache* strCache, MsgContext* msgContext,
                   IODBNucleus* iodb, UD* ud, ArgProc* args, 
                   bool convert, bool verbose);

  //! destructor
  ~REConvertLatches();

  //! process a module
  void module(NUModule* mod);

private:
  //! Function to test if this assign is fed by a latch and can be converted
  void processAssign(NUContAssign* contAssign);

  //! Abstraction to keep track of an a clock expression and the latch use
  /*! In the assignment statement that looks like:
   *
   *  \verbatim
   *    assign gclk = <expr1> & <expr2>;
   *  \endverbatim
   *
   *  One of the legs is expected to be the latch and the other the
   *  clock. This pair contains the expression for the clock and the
   *  net ref for the latch.
   */
  typedef std::pair<NUExpr*, NUNetRefHdl> ClkLatchUsesPair;

  //! Abstraction for a set of potential latches and how they are used
  typedef UtVector<ClkLatchUsesPair> ClkLatchUses;

  //! Abstraction to iterate over potential latch uses
  typedef CLoop<ClkLatchUses> ClkLatchUsesLoop;

  //! Check if NUContAssign is of the right type and return fanin latches
  bool isValidAssign(NUContAssign* contAssign, NUNetRefHdl* assignNetRef,
                     ClkLatchUses* clkLatchUses, bool* isOR);

  //! Check if a sub expression uses a scalar net and return it
  bool singleScalarUse(NUExpr* expr, NUNetRefHdl* netRef);

  //! Check if the latch is not viewed externally or by more than one construct
  bool isOptimizableLatch(const NUNetRefHdl& netRef) const;

  //! Find the candidate latch for conversion
  void findCandidateLatch(const NUNetRefHdl& assignNetRef,
                          const ClkLatchUses& clkLatchUses, bool isOR,
                          FLNode** latchFlow);

  //! Test if a flow node represents an initial block
  bool isInitialBlock(FLNode* flow) const;

  //! Test to make sure that a latch only defs the enable we care about
  bool singleDef(FLNode* flow, const NUNetRefHdl& netRef);

  //! Function to convert a latch
  NUNet* convertLatch(FLNode* flow);

  //! Class to find flops and test exclusivity
  REClockExclusivity* mClockExclusivity;

  //! Context to create BDDs
  BDDContext* mBDDContext;

  //! Flow factory to find drivers of nets
  FLNodeFactory* mFlowFactory;

  //! Net Ref factory for analyzing defs and uses
  NUNetRefFactory* mNetRefFactory;

  //! A cache to create names
  AtomicCache* mStrCache;

  //! Message context to print information about converted latches
  MsgContext* mMsgContext;

  //! IODB Context to test for protected
  IODBNucleus* mIODB;

  //! UD to fixup always blocks
  UD* mUD;

  //! If set, the conversion should happen. Otherwise we may just print
  bool mConvert;

  //! If set, information should be printed about converted latches
  bool mVerbose;

  //! Count of converted latches
  int mConvertCount;
};

#endif // _CONVERTLATCHES_H_
