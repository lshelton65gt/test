// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _PRIORITYENCODERREWRITE_H_
#define _PRIORITYENCODERREWRITE_H_

class EncoderStatistics;
class Reorder;
class ArgProc;
class AllocAlias;

//! PriorityEncoderRewrite
/*! Class to find all priority encoders that are made up of if
 *  statements and convert them to if/else if chains.
 *
 *  Some priority encoders are done with for loops containing an if
 *  statement. It is assumed that for loop unrolling will create the
 *  list of if statements in this case.
 *
 *  This class has two three main entry points. Two of them do walks
 *  looking for statement lists to be processed. The third is the
 *  routine to process a statement list. This allows this optimization
 *  to be called from another optimization such as for loop unrolling.
 */
class PriorityEncoderRewrite
{
public:
  //! constructor
  PriorityEncoderRewrite(EncoderStatistics* encoderStatistics,
			 NUNetRefFactory* netrefFactory,
			 MsgContext* msgContext,
                         AtomicCache * strCache,
			 IODBNucleus* iodb,
                         ArgProc* args,
                         AllocAlias* alloc_alias,
                         bool verbose);

  //! destructor
  ~PriorityEncoderRewrite();

  //! Walk a module and re-write any priority encoders found.
  /*! Note that this does not walk any sub modules. It counts on the
   *  caller to handle that.
   * \return true if any rewrite occurred.
   */
  bool module(NUModule* module);

  //! Re-write the given set of statements if there is a priority encoder
  /*! Analyzes the input statments and if it can, it will re-write any
   *  sequence of if statements that have the same def set, do not use
   *  the defs, and are missing either a then or else clause, into an
   *  if/else if statement.
   *
   *  \param The use-def node owning the statement list; used for assert messages.
   *  \param unprocessedStmtsLoop An iterator over statements to analyze and re-write
   *
   *  \param loc Location of the if statements body for verbosity
   *
   *  \param msg context information for verbosity.
   *
   *  \param newStmts A place to put the new statement list. This is
   *                  populated even if no rewrite occurs. Contents
   *                  should be used only when this method returns a
   *                  non-zero value.
   *
   *  \returns the number of if statements that were optimized
   *
   */
  SInt64 rewrite(NUBase * parent,
                 NUStmtLoop unprocessedStmtsLoop, 
                 const SourceLocator& loc,
		 const char* msg, 
                 NUStmtList & newStmts);

private:

  typedef UtList<BDD> BDDList;
  typedef Loop<BDDList> BDDLoop;

  //! Hide copy and assign constructors.
  PriorityEncoderRewrite(const PriorityEncoderRewrite&);
  PriorityEncoderRewrite& operator=(const PriorityEncoderRewrite&);

  // Abstraction for a vector of if statements we use to find groups
  // of if statements
  typedef UtVector<NUIf*> IfStmts;
  typedef IfStmts::iterator IfStmtsIter;

  // Local functions to qualify and convert a set of if statements
  NUIf* validIf(NUStmt* stmt, NUNetRefSet&, NUNetRefSet&, NUNetRefSet&);

  bool unrelatedStatement(NUStmt* stmt, NUNetRefSet&, NUNetRefSet&, NUNetRefSet&);

  //! Create a single if-statement from a list.
  /*!
   \return NULL if the list is empty.
   */
  NUStmt* convert(IfStmts& ifStmts);
  void gatherKills(NUStmtLoop stmtsLoop, NUNetRefSet& blockingKills,
		   NUNetRefSet& nonBlockingKills);

  //! Chain if statements into a priority encoded chain of if-else statements.
  SInt64 rewriteMatchingDefIfs(NUStmtList& inputStmts, NUStmtList& outputStmts,
                               NUBase * parent, const SourceLocator& loc,
                               const char* msg);

  //! Check if we have an ordered series of if statements with same uses and
  //! mutually exclusive conditions. Chain them into if-else statements. There
  //! could be multiple such groups in the if statements list.
  SInt64 rewriteMutexCondIfs(NUStmtList& inputStmts, NUStmtList& outputStmts);

  //! Add to the list of statements and return true if this if statement condition
  //! is mutually exclusive to all if statement conditions in the list and
  //! return true.
  bool maybeAddMutexIf(NUIf* ifStmt, IfStmts& mutexIfs, BDDContext& ctx,
                       BDDList& mutexIfBdds);

  // Nucleus walk callback
  class Callback : public NUDesignCallback
  {
#if ! pfGCC_2
    using NUDesignCallback::operator();
#endif

  public:
    //! constructor
    Callback(PriorityEncoderRewrite* rewritePriorityEncoder);

    //! destructor
    ~Callback();

    //! Catchall routine
    Status operator()(Phase, NUBase*) { return eNormal; }

    //! Stay within this module - do not walk down sub-modules
    Status operator()(Phase, NUModuleInstance*) { return eSkip; }

    //! Routine to handle blocks (which contain statement lists)
    Status operator()(Phase phase, NUBlock* block);

    //! Routine to handle "ifs" (which contain then/else statement lists)
    Status operator()(Phase phase, NUIf* ifStmt);

    //! Routine to handle case items (which contain a statement list)
    Status operator()(Phase phase, NUCaseItem * caseItem);

    //! Specialize for task enable because do not want to traverse into hierref tasks
    //! (need to stay within this module).
    Status operator()(Phase phase, NUTaskEnable * enable);

    //! Routine to handle tasks and functions (which contain statement lists)
    Status operator()(Phase phase, NUTF* tf);

    //! Return if statements were modified
    bool statementsModified() const { return mStmtsModified; }

  private:
    PriorityEncoderRewrite* mPriorityEncoderRewrite;

    // Set if a block within the current module has been modified
    bool mStmtsModified;
  }; // class Callback : public NUDesignCallback

  // Place to store accumulated stats
  EncoderStatistics* mEncoderStats;

  // Helper classes
  NUNetRefFactory *mNetRefFactory;
  UD* mUD;
  Reorder * mReorder;

  // Be verbose?
  bool mVerbose;
}; // class PriorityEncoderRewrite

//! class EncoderStatistics
class EncoderStatistics
{
public:
  //! constructor
  EncoderStatistics() { clear(); }

  //! destructor
  ~EncoderStatistics() {}

  void module() { ++mModules; }
  void ifStmt(SInt64 ifStmts) { mIfStmts += ifStmts; }
  void clear() { mModules = 0; mIfStmts = 0; }
  void print() const;
private:
  SInt64 mModules;
  SInt64 mIfStmts;
};

#endif // _PRIORITYENCODERREWRITE_H_
