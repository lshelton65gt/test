// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef CLEANELABFLOW_H_
#define CLEANELABFLOW_H_

#include "reduce/ElabMarkSweep.h"

class STSymbolTable;

/*!
  \file
  Declaration of elaborated flow cleanup package.
 */

//! CleanElabFlow class
/*!
 * Class to find any dead flow and remove it.
 */
class CleanElabFlow
{
public:
  //! constuctor
  /*
    \param dfg_elab_factory Elaborated DFG Factory
    \param symtab Symbol Table
    \param verbose If true, will be verbose about flow cleanup.
   */
  CleanElabFlow(FLNodeElabFactory *dfg_elab_factory,
		STSymbolTable *symtab,
		bool verbose) :
    mFlowFactory(dfg_elab_factory),
    mSymtab(symtab),
    mVerbose(verbose),
    mDeleteFlows(true)
  {}

  //! Walk this design.
  void design(NUDesign *design,FLNodeElabSet * deleted_flow_elabs=NULL,
              bool deleteFlows = true);

  //! Delete dead flow for the design, assumed that mark has already occurred.
  void deleteDeadFlow(FLNodeElabSet* deleted_flow_elabs = NULL);

  //! destructor
  ~CleanElabFlow() {}

private:
  //! Hide copy and assign constructors.
  CleanElabFlow(const CleanElabFlow&);
  CleanElabFlow& operator=(const CleanElabFlow&);

  //! Allow the callback to use the removeFlow function.
  class Callback;
  friend class Callback;

  //! Callback class for sweep
  class Callback : public ElabMarkSweep::SweepCallback
  {
  public:
    Callback(CleanElabFlow *obj,
             FLNodeElabSet *deleted_flow_elabs) : 
      mObj(obj),
      mDeletedFlowElabs(deleted_flow_elabs)
    {}
    void operator()(FLNodeElab *flnode, bool is_dead) { mObj->removeFlow(flnode, mDeletedFlowElabs, is_dead); }
    ~Callback() {}

  private:
    CleanElabFlow * mObj;
    FLNodeElabSet * mDeletedFlowElabs;
  };

  //! Callback from sweep.
  void removeFlow(FLNodeElab * flnode, 
                  FLNodeElabSet * deleted_flow_elabs,
                  bool is_dead);

  //! Remove all unmarked flow from the factory.
  void removeUnmarkedFlow(FLNodeElabSet * deleted_flow_elabs);

  //! DFG Factory
  FLNodeElabFactory *mFlowFactory;

  //! Symbol table
  STSymbolTable *mSymtab;

  //! Be verbose?
  bool mVerbose;

  //! Any dead flow found?
  bool mDeadFlowFound;

  //! Delete the flow?
  bool mDeleteFlows;
};

#endif
