// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef RETRIFLAGS_H_
#define RETRIFLAGS_H_

#include "reduce/Reduce.h"
#include "nucleus/Nucleus.h"

class Stats;
class STSymbolTable;
class ReachableAliases;

/*!
  \file 
  Declaration of class used to propagate tristate flags across the design.
 */

//! RETriFlags
/*!
 * Class which performs tristate flag propagation across all potential aliases.
 */
class RETriFlags
{
public:
  //! Constructor
  /*!
    \param symbol_table Symbol table.
    \param reachables Already-computed set of reachable aliases.
   */
  RETriFlags(STSymbolTable * symbol_table,
             ReachableAliases * reachables);

  //! Destructor
  ~RETriFlags();

  //! Propagate flags on unelaborated alias sets
  /*!
   * Create sets of unelaborated aliases, and propagate flags across those sets.
   */
  void propagate(Stats *stats, bool phase_stats);

private:
  //! Hide copy and assign constructors.
  RETriFlags(const RETriFlags&);
  RETriFlags& operator=(const RETriFlags&);

  //! Populate the given net vector with reachable aliases of the master.
  void getAliasNets(NUNetElab *master, NUNetSet *aliases);

  //! Collect the set of unelaborated aliases
  /*!
   * Iterate through mAliases (which contains elaborated aliases), and collect
   * unelaborated alias sets into alias_sets.
   */
  void collectUnelaboratedAliases(NUNetClosure* alias_sets);

  STSymbolTable * mSymbolTable;
  ReachableAliases * mReachableAliases;
};

#endif // RETRIFLAGS_H_
