// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __RE_TRI_SIMPLIFY_H_
#define __RE_TRI_SIMPLIFY_H_

#include "nucleus/Nucleus.h"

class Fold;
class IODBNucleus;

//! context for simplifying tristates (NUContEnabledDrivers) in
//! a module
/*!
  This class tries to find 'complete' tristates nets, where all the
  enable-drivers ORed together simplify to 1'b1.  In that case it
  makes a continuous assign.  We don't care whether the enables are
  mutually exclusive because a conflict would produce X in a 4-state
  simulator and therefore Carbon reserves the right to produce whatever
  2-state value is most convenient.

  We also consider a tristate 'complete' if there are no other possible
  writers on the net, and it's not pulled or a trireg, and we are in
  "-tristate x" mode, where we are allowed to produce whatever value we
  like on a net for Z conditions.
*/
class RETriSimplify {
public:
  //! ctor
  RETriSimplify(Fold* fold, bool verbose, MsgContext*, TristateModeT, IODBNucleus*);

  //! dtor
  ~RETriSimplify();

  //! simplify the tristates in a module
  bool module(NUModule* mod);

private:
  //! simplify the tristate drivers to a net
  bool processNet(NUNet* net);

  //! Determine whether a net is qualified to be tristate-simplified,
  //! and gather up the list of enabled drivers while we are figuring
  //! it out
  bool qualifyNet(NUNet* net,
                  NUEnabledDriverList* enabled_drivers,
                  NULvalue** lval,
                  Strength* strength);

  //! Having determined that a net is qualified for optimization,
  //! see if the optimization makes sense (determined by cost).  If
  //! so, optimize it and return true, if not return false
  bool optimizeNet(NUNet* net,
                   const NUEnabledDriverList& enabled_drivers,
                   NULvalue* lval,
                   Strength strength);

  //! Having changed the drivers for a net, repair the continuously
  //! driving flow-nodes for that net.  Note that we reject any net
  //! this is driven by anything other than an NUEnabledDriver*, so
  //! we don't have to worry about nested flow
  void repairFlow(NUNet* net, NUUseDefNode* replacementNode);

  //! after all the nets are examined, the enabled drivers are examined to
  //! see which must be eliminated because they have been transformed into
  //! something else.
  bool processEnabledDriver(NUEnabledDriver*);

  //! Based on a collected enable, can we treat a net as a strong continuous
  //! assign?  Or do we have to keep it as a Z-net?
  bool canEliminateTristate(NUExpr* ena);

  //! Walk through a list of enabled drivers, and collect an aggregate ena
  //! and drive-value.  E.g. if I have
  //!    assign a = e1 ? i1 : 1'bz;
  //!    assign a = e2 ? i2 : 1'bz;
  //! then the collected expressions are
  //!   *ena == e1 | e2;
  //!   *drv = e1 ? i1 : i2;
  void collectEnablesAndDrives(const NUEnabledDriverList& enabled_drivers,
                               NUExpr** ena, NUExpr** drv, NUCost*, UInt32);

  //! collected replacement drivers (including old driver that we are not
  //! modifying)
  NUEnabledDriverList* mNewDrivers;

  //! enabled drivers we want to delete 
  NUEnabledDriverSet* mKillDrivers;

  //! be noisy when optimizing?
  bool mVerbose;

  //! current module
  NUModule* mModule;

  //! what tristate mode has the user requested?
  TristateModeT mTristateMode;

  //! all the contexts we need for optimization
  Fold* mFold;
  NUCostContext* mCostContext;
  MsgContext* mMsgContext;
  IODBNucleus* mIODB;
};

#endif
