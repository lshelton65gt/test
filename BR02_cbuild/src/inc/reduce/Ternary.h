// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef TERNARY_H_
#define TERNARY_H_

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUNetRef.h"

/*!
  \file
  Declaration of ternary generation package.
 */

class NUNetRefFactory;
class MsgContext;
class AtomicCache;
class ArgProc;
class IODBNucleus;

class Inference;
class InferenceStatistics;
class Fold;
class TernaryStatistics;
class UD;

/*!

  Ternary conversion - Convert branch statements (if-then-else, case)
  into ternary ?: assignments. This conversion enables other
  optimizations (fold, infernence). Unsimplified situations are
  usually converted back into branch statements by ControlExtract.

  \sa ControlExtract

 */
class Ternary
{
public:
  //! Constructor
  /*!
    \param str_cache The string cache.
    \param netref_factory NetRef factory.
    \param msg_context Message context.
    \param iodb The iodb.
    \param args Argument-processor.

    \param only_bit_conditions If true, only convert bit-select conditions.

    \param only_balanced_conditions If true, only convert balanced branch statements.

    \param only_bit_definitions If true, only convert branch statements with a bit-select definition.
   */
  Ternary(AtomicCache * str_cache,
	  NUNetRefFactory * netref_factory,
	  MsgContext * msg_context, 
	  IODBNucleus * iodb,
	  ArgProc *args,
          bool only_bit_conditions,
          bool only_balanced_conditions,
          bool only_bit_definitions,
          bool fold_results,
	  bool verbose,
	  TernaryStatistics *tstats,
	  InferenceStatistics *istats);

  //! Destructor
  ~Ternary();

  //! Do the design
  /*! Note, that UD is called on the affected blocks
   */
  void design(NUDesign * design);

  //! Walk a module
  /*! Note, that UD is called on the affected blocks
   */
  void module(NUModule * one);

  //! Specialized for post-block-merging.
  /*! Note, that UD is called on the affected blocks
   */
  bool mergedBlocks(NUModule * module, NUUseDefVector & use_defs);

  //! Walk a stmtlist and modify in-place.
  /*! Note, that UD is not called. It is the callers responsibility to
   *  recompute UD when necessary.
   */
  bool stmts(NUStmtList & stmts);

private:
  AtomicCache     * mStrCache;
  NUNetRefFactory * mNetRefFactory;
  MsgContext      * mMsgContext;
  ArgProc         * mArgs;

  Fold      * mFold;
  Inference * mInference;
  UD        * mUD;

  TernaryStatistics * mStatistics;

  //! Only bit-select conditions.
  bool mOnlyBitConditions;

  //! Only allow balanced conditions.
  bool mOnlyBalancedConditions;

  //! Only allow bit-select definitions.
  bool mOnlyBitDefinitions;

  //! Should results be simplified?
  bool mFoldResults;

  //! Be verbose as processing occurs?
  bool mVerbose;
};


class TernaryBlock
{
public:
  TernaryBlock(NUNetRefFactory * netref_factory,
               Fold * folder,
	       TernaryStatistics * statistics,
	       bool only_bit_conditions,
               bool only_balanced_conditions,
               bool only_bit_definitions,
               bool fold_results,
	       bool verbose) :
    mNetRefFactory(netref_factory),
    mFold(folder),
    mStatistics(statistics),
    mOnlyBitConditions(only_bit_conditions),
    mOnlyBalancedConditions(only_balanced_conditions),
    mOnlyBitDefinitions(only_bit_definitions),
    mFoldResults(fold_results),
    mVerbose(verbose),
    mRecomputeUD(false)
  {
    reset();
  }

  ~TernaryBlock() {}

  //! Walk a stmtlist and modify in-place.
  bool translate(NUStmtLoop loop,
		 NUStmtList & stmts);

  NUStmt * translate(NUStmt * stmt);
  NUStmt * translate(NUBlock * block);
  NUStmt * translate(NUIf * stmt);
  NUStmt * translate(NUCase * stmt);
  NUStmt * translate(NUFor * stmt);

  //! Did a translate operation succeed?
  bool success() const { return mSuccess; }

  //! Clear success status.
  void reset() { mSuccess = false; }

  //! Test if changes occured that required UD recomputation
  bool getRecomputeUD() const { return mRecomputeUD; }

  //! Set whether we should recompute UD or not
  void putRecomputeUD(bool val) { mRecomputeUD = val; }

private:
  NUNetRefFactory * mNetRefFactory;
  Fold * mFold;
  TernaryStatistics * mStatistics;

  //! Only bit-select conditions.
  bool mOnlyBitConditions;

  //! Only allow balanced conditions..
  bool mOnlyBalancedConditions;

  //! Only allow bit-select definitions.
  bool mOnlyBitDefinitions;

  bool mFoldResults;
  bool mVerbose;
  bool mSuccess;
  bool mRecomputeUD;
};


//! Must be called from a NUDesignWalker rooted at an if() stmt.
class TernaryTranslateCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  //! Constructor
  TernaryTranslateCallback(NUNetRefFactory * netref_factory,
			   bool only_bit_conditions,
                           bool only_balanced_conditions,
                           bool only_bit_definitions) :
    mNetRefFactory(netref_factory),
    mOnlyBitConditions(only_bit_conditions),
    mOnlyBalancedConditions(only_balanced_conditions),
    mOnlyBitDefinitions(only_bit_definitions),
    mLvalue(NULL)
  {
    cleanup();
  }

  //! Destructor
  ~TernaryTranslateCallback() {
    cleanup();
  }

  //! Unhandled Nucleus types cause us to abort the search.
  Status operator()(Phase /*phase*/, NUBase * /*node*/) { return eStop; }

  //! Skip all expressions.
  Status operator()(Phase /*phase*/, NUExpr * /*node*/) { return eSkip; }

  //! Skip all LValues.
  Status operator()(Phase /*phase*/, NULvalue * /*node*/) { return eSkip; }

  //! Handle blocking assignments; make sure we consistently see blocking assignments.
  Status operator()(Phase phase, NUBlockingAssign *assign);

  //! Handle non-blocking assignments; make sure we consistently see non-blocking assignments.
  Status operator()(Phase phase, NUNonBlockingAssign *assign);

  //! Assignments push a copy of their rvalue on our expr stack.
  Status operator()(Phase phase, NUAssign *assign);

  //! Generate ternary operators from if() stmts.
  /*!
    As the recursion unwinds, if() stmts generate ternary operators
    from the contents of the expr stack.
  */
  Status operator()(Phase phase, NUIf *if_stmt);

  //! Generate ternary operators from case() stmts.
  /*!
    As the recursion unwinds, case() stmts generate ternary operators
    from the contents of the expr stack.
  */
  Status operator()(Phase phase, NUCase *case_stmt);

  //! Skip over case conditions
  Status operator()(Phase /*phase*/, NUCaseCondition* /*node*/) { return eSkip; }

  //! Pass through case items
  Status operator()(Phase /*phase*/, NUCaseItem* /*node*/) { return eNormal; }

  //! Generate the assign.
  /*!
    \return NULL if we are inconsistent.
    \sa ::consistent()
   */
  NUAssign * getAssign (const SourceLocator & loc);

private:
  //! Check that an assignment defines only one bit of a net.
  /*!
    If this if stmt is valid, its def netref is saved aside for later
    consistency checks.
  */
  bool setDefNetRef(NUAssign* stmt);

  //! Check that a statement defines only one netref and that netref is consistent with the approved def netref.
  bool checkDefNetRef(NUStmt* stmt) const;

  //! Return true if this if() stmt appears valid for this transformation. 
  /*
    Checks:
    1. If() stmt defines one bit of a vector.

    2. Conditional uses one bit of some vector (only checked for top if() stmt)

    Note: This is too conservative, as we will miss the following situation:
    if (a[0] and a[1]) begin
    ...
    end

    We will see that as a two-bit select; not matching our search
    pattern of single-bit references.

    3. Both the then and else bodies contain one stmt.

  */
  bool validIf(NUIf * if_stmt);

  //! Return true if this case() stmt appears valid for this transformation. 
  /*
    Checks:
    1. case() stmt kills one bit of a vector.

    2. Conditional uses one bit of some vector (only checked for top case() stmt)

    Note: This is too conservative, as we will miss the following situation:
    if (a[0] and a[1]) begin
    ...
    end

    We will see that as a two-bit select; not matching our search
    pattern of single-bit references.

    3. The case stmt is fully specified
    4. Each case item contains only one condition.
    5. Each case item contains one stmt.

  */
  bool validCase(NUCase * case_stmt);

  //! Generate an rValue from a lValue
  NUExpr * createRvalue(NULvalue * lvalue, const SourceLocator & loc) const;

  //! Determine if the result of the recursion is valid. 
  /*!
    Legal to call only after the walk of an NUIf tree is complete.

    Checks:
    1. The netref being defined has been found (non-empty).
    2. There is only one expression remaining on our stack.
    3. We know the assignment type.
  */
  bool consistent() const { 
    return ((not mDefNetRef->empty()) and
	    (mExprs.size()==1) and
	    (mAssignmentType!=eUnknown));
  }

  //! Return the generated expression.
  /*!
    Legal to call only after the walk of an NUIf tree is complete and consistent.

    The caller is responsible for memory management of the returned
    NUExpr object.

    This can only be called once.
  */
  NUExpr * popRvalue() {
    NUExpr * rvalue = mExprs.back();
    INFO_ASSERT(rvalue, "Ternary consistency error.");
    mExprs.pop_back();
    return rvalue;
  }

  //! Return the generated lvalue.
  /*!
    Legal to call only after the walk of an NUIf tree is complete and consistent.

    The caller is responsible for memory management of the returned
    NULvalue object.

    This can only be called once.
  */
  NULvalue * popLvalue() {
    INFO_ASSERT(mLvalue, "Ternary consistency error.");
    NULvalue * lvalue = mLvalue;
    mLvalue = NULL;
    return lvalue;
  }

  //! Return the defined NUNetRefHdl.
  /*!
    Legal to call only after the walk of an NUIf tree is complete and consistent.
  */
  NUNetRefHdl getDefNetRef() {
    return mDefNetRef;
  }

  //! Cleanup all internal data structures.
  /*!
    In the event of an aborted recursion, we need to delete any
    generated expressions. This also resets all internal state so this
    object can be used for multiple recursions.
  */
  void cleanup();

  NUNetRefFactory * mNetRefFactory;

  //! Only bit-select conditions.
  bool mOnlyBitConditions;

  //! Only allow balanced conditions..
  bool mOnlyBalancedConditions;

  //! Only allow bit-select definitions.
  bool mOnlyBitDefinitions;

  NUNetRefHdl mDefNetRef;

  NUExprVector mExprs;

  NULvalue * mLvalue;

  enum AssignmentType {
    eUnknown,
    eBlocking,
    eNonBlocking
  };
  AssignmentType mAssignmentType;

  //! Have we already checked our condition?
  bool mCheckedIfCondition;
};


class TernaryStatistics
{
public:
  //! Constructor
  TernaryStatistics() { clear(); }
  //! Destructor
  ~TernaryStatistics() {}

  void module()  { ++mModules; }
  void ternary() { ++mTernaryStmts; }
  void clear() { mModules=0; mTernaryStmts=0; }
  void print() const;

private:
  SInt64 mModules;
  SInt64 mTernaryStmts;
};

#endif
