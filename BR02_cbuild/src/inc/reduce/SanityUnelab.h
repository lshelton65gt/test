// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef SANITYUNELAB_H_
#define SANITYUNELAB_H_

#include "flow/Flow.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUDesignWalker.h"
#include "localflow/DesignWalker.h"

class NUNetRefFactory;
class MsgContext;

class SanityUnelabStatus;
class SanityUnelabNetStatus;

/*!
  \file
  Declaration of unelaborated sanity check package.
 */

//! SanityUnelab class
/*!
 * Class to sanity check unelaborated structures.  Make sure any reachable flow is valid:
 *  . check that every net's drivers are valid
 *  . check that all fanin and nested flow is valid
 *  NYI. check that use def pointers are valid
 */
class SanityUnelab : public DesignWalker
{
public:
  //! constuctor
  /*
    \param netref_factory NetRef Factory
    \param dfg_factory DFG Factory
    \param checkFaninOverlap If true, will check that flow fanin overlaps uses.
                             This requires accurate UD.
    \param verbose If true, will be verbose about flow cleanup.
   */
  SanityUnelab(MsgContext * msg_context,
               NUNetRefFactory * netref_factory,
               FLNodeFactory *dfg_factory,
               bool checkFaninOverlap,
               bool verbose);

  //! Sanity check the design
  void design(NUDesign *design);

  //! destructor
  ~SanityUnelab() {}

private:
  //! Hide copy and assign constructors.
  SanityUnelab(const SanityUnelab&);
  SanityUnelab& operator=(const SanityUnelab&);

  //! Sanity check all nets in this module and all sub-modules
  void module(NUModule *module);

  //! Sanity check all nets in the scope
  void scopeNets(NUScope * scope);

  //! Sanity check the net
  bool net(SanityUnelabNetStatus * status, NUNet *this_net);

  //! Sanity check the flow factory
  void factory();

  //! Check that the given flow is allocated.  Return true if it is, false otherwise.
  bool allocFlowCheck(FLNode *flnode);

  //! For each fanin of this flnode, check that its defs overlap the flnode's usedef node's uses.
  /*! 

    \param flnode The flow being tested.

    \param bad_fanin Fanin to flnode with known problems (deleted,
           etc.) These fanin will not be checked.

    \param unnecessary_fanin Fanin to flnode which do not pass the
           overlaps check. This will not include any fanin already
           present in bad_fanin.

    \return true if each checked fanin is valid.
   */
  bool faninOverlapCheck(FLNode* flnode, 
                         const FLNodeSet & bad_fanin,
                         FLNodeSet & unnecessary_fanin);

  //! Sanity check the flow node
  bool flow(SanityUnelabStatus * status, FLNode *flnode);

  //! Allow the callbacks to access the object.
  class SanityNucleusCallback;
  friend class SanityNucleusCallback;

  class SanityNucleusCallback : public NUDesignCallback
  {
#if ! pfGCC_2
    using NUDesignCallback::operator();
#endif
  public:
    SanityNucleusCallback() : mObj(0) {}
    SanityNucleusCallback(SanityUnelab *obj) : mObj(obj) {}

    //! Clear the mark on the nucleus node
    Status operator()(Phase phase, NUBase *node);

    ~SanityNucleusCallback() {}

  private:
    SanityUnelab* mObj;
  };

  //! Mark one usedef as valid (called from design walk)
  void mark(NUBase * node);

  //! Message context
  MsgContext * mMsgContext;

  //! NetRef Factory
  NUNetRefFactory * mNetRefFactory;

  //! DFG Factory
  FLNodeFactory *mFlowFactory;

  //! Free set
  MemoryPoolPtrSet mFreeSet;

  //! Valid usedef set
  UtHashSet<NUBase*> mValidUseDefs;

  //! Valid nets
  UtHashSet<NUNet*> mValidNets;

  //! Number of errors found
  UInt32 mNumErrors;

  //! Check that the fanin's defs overlap the flow usedef's uses?
  bool mCheckOverlap;
};

#endif
