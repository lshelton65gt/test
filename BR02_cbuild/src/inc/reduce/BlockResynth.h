// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef BLOCKRESYNTH_H_
#define BLOCKRESYNTH_H_

/*!
  \file 
  Declaration of the block resynthesis package.
*/

#include "nucleus/Nucleus.h"

class FLNodeFactory;
class ArgProc;
class MsgContext;
class IODBNucleus;
class ArgProc;
class AtomicCache;
class ESPopulateExpr;
class ExprResynth;
class FLNode;
class AllocAlias;

//! The block resynthesis interface.
/*!
 * Need to write this.
 */
class BlockResynth
{
public:
  //! Constructor for block resynthesis interface.
  /*!
   * \param netref_factory Factory used to manage NetRefs.
   *
   * \param dfg_factory Factory for the unelaborated data-flow graph.
   *
   * \param args    Command-line argument interface.
   *
   * \param msg_ctg The message context.
   *
   * \param iodb    The IODB.
   *
   * \param str_cache The string cache.
   *
   * \param verbose Should block resynthesis print debugging
   *                information?
   */
  BlockResynth(NUNetRefFactory *netref_factory,
               FLNodeFactory *dfg_factory,
               ArgProc* args,
               MsgContext *msg_ctx,
               IODBNucleus *iodb,
               AtomicCache *str_cache,
               bool verbose,
               bool force,
               AllocAlias* alloc_alias);
  
  //! Destructor for block resynthesis interface.
  ~BlockResynth();

  //! Fully resynthesize the contents of one module (not its submodules).
  /*!
   * \param one_module The module targetted for resynthesis.
   *
   * \return true if any rewrites are successful.
   */
  bool module(NUModule * one_module);

private:
  //! Resynthesize all continuous assigns contained within a module.
  /*
   * \param one_module  The module targetted for resynthesis.
   *
   * \param pending_deletion Set of NUUseDefNodes which have been
   *            obsoleted by resynthesized logic.
   *
   * \return true if any rewrites are successful.
   */
  bool contAssigns(NUModule * one_module, NUUseDefList * pending_deletion);

  //! Resynthesize all always blocks contained within a module.
  /*
   * \param one_module  The module targetted for resynthesis.
   *
   * \param pending_deletion Set of NUUseDefNodes which have been
   *            obsoleted by resynthesized logic.
   *
   * \return true if any rewrites are successful.
   */
  bool alwaysBlocks(NUModule * one_module, NUUseDefList * pending_deletion);

  //! Resynthesize one always block.
  /*
   * \param one_module  The module targetted for resynthesis.
   *
   * \param always      The always block targetted for resynthesis.
   *
   * \param replacements The resulting always blocks after
   *            resynthesis. If resynthesis of this block is
   *            successful, the new always block is appended to the
   *            replacement * list. If resynthesis fails, the original
   *            block is added.
   *
   * \return true if any rewrites are successful.
   */
  bool alwaysBlock(NUModule * one_module, NUAlwaysBlock * always,
                   NUAlwaysBlockList * replacements,
                   NUUseDefList * pending_deletion);

  //! Is this always block valid for block resynthesis?
  bool qualify(NUAlwaysBlock * always);

  //! Is this a valid output net of block resynthesis?
  bool qualify(NUNet * net);

  //! Print debug information.
  void print(NUNet * net, const char * driver_type);

  //! Determine which driver of a net is the output of a node.
  /*!
   * \param net  The net in question.
   *
   * \param node An NUUseDefNode which may be a driver for the net.
   *
   * \return A FLNode driver for 'net' that is the output of 'node';
   *         NULL if one does not exist.
   */
  FLNode* getDrivingNode(NUNet * net, NUUseDefNode * node);

  //! Make an lvalue from a flow-node
  NULvalue* constructLvalue(FLNode* flow);

  //! Factory used to manage NetRefs.
  NUNetRefFactory * mNetRefFactory;

  //! Factory for the unelaborated data-flow graph.
  FLNodeFactory * mFlowFactory;

  //! args
  ArgProc* mArgs;

  //! The message context.
  MsgContext * mMsgContext;
  
  //! The IODB.
  IODBNucleus * mIODB;

  //! The string cache.
  AtomicCache * mStrCache;

  //! Logic population interface (NUUseDefNode/NUExpr --> CarbonExpr).
  ESPopulateExpr * mPopulateExpr;

  //! Logic resynthesis interface (CarbonExpr --> NUExpr).
  ExprResynth * mExprResynth;

  //! Expression factory - used by resynthesis to create managed expressions.
  NUExprFactory * mExprFactory;

  //! Should we print debug information?
  bool mVerbose;

  //! Should we do block resynthesis independent of whether it's a good idea?
  bool mForce;

  //! Context for calculating costs
  NUCostContext* mCostContext;

  AllocAlias* mAllocAlias;
};


#endif
