// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef INTERRADESIGNWALKER_H_
#define INTERRADESIGNWALKER_H_

#include "compiler_driver/Interra.h"
#include "shell/carbon_shelltypes.h"
#include "util/UtString.h"

class TicProtectedNameManager;
class IODBNucleus;
class MsgContext;

//! Callback for InterraDesignWalk
/*!
  All callbacks return eNormal, unless otherwise noted.

  The callback mechanism is a way for the caller of the walk to look
  at particular parts of the design in a consistent way. One of the
  main problems with this approach, however, is that \e many functions
  are created as a result, and it kind of becomes a design borg of
  sorts. Some of these functions can probably be merged so that both
  vhdl and verilog can use the same callback. Some already have, but
  more can be done.
  But, in order to not change the current population code too much, I
  had to create more and more functions to keep each piece of
  functionality distinct.
*/
class InterraDesignWalkerCB
{
public:
  //! Virtual destructor
  virtual ~InterraDesignWalkerCB();

  //! Callback status
  enum Status {
    eNormal,  //!< Normal status, continue walking
    eStop,    //!< Stop the walk
    eSkip,    //!< Skip the current node
    eInvalid  //!< Invalid status
  };

  //! Callback phase
  enum Phase {
    ePre,          //!< Pre-node walk
    ePost          //!< Post-node walk
  };

  //! Design root callback
  virtual Status design(Phase phase, mvvNode entity, mvvNode architecture, mvvLanguageType langType);

  //! Entity (module) callback.
  /*!
    Entity is a vhdl term, but means "module declaration" for Verilog.

    This is a unifying function to allow for mixed language
    support. This calls the appropriate language construct underneath.

    isCmodel is defaulted to false. You should set isCmodel on the pre
    call, but are not required to. If isCmodel is true it will 
    only process what is needed for the cmodel, and then the callback
    for the cmodel will be called. In the post phase isCmodel
    will be what you set it to in the pre phase.
  */
  virtual Status hdlEntity(Phase phase, mvvNode node, mvvLanguageType langType, const char* entityName, bool* isCmodel);


  //! Component (Module) instance declaration callback.
  /*!
    This callback gets called before a component instance is
    populated. Verilog and Vhdl differ in how components can be
    instantiated. For example, in Verilog, you can vectorize an
    instantiation. So, the hdlComponentInstance() callback would be
    called several times for the same declaration. This callback only
    gets called twice in the vectorized case (once for pre and once
    for post).

    \param phase Phase of traversal
    \param node Instance that will be expanded for population
    \param langType Language type of the calling entity
  */
  virtual Status hdlComponentInstanceDeclaration(Phase phase, mvvNode node, mvvLanguageType langType);

  //! Component instance callback. Vhdl AND Verilog
  /*!
    Component instance is a vhdl term, but means "module instance" for Verilog.
    
    This is a unifying function to allow for mixed language
    support. This calls the appropriate language construct underneath.
    
    \param phase Phase of traversal
    \param inst Component instance 
    \param instName Name of component instance
    \param instMaster The master node of the inst (module in verilog,
    entity in vhdl)
    \param oldMaster The master node of instance prior to substitution.
    \param newMaster If Not NULL this the result of
    substituteEntity. If NULL, no entity substitution happened.
    \param entityName The resolved entity name for the component. If
    the entity was substituted this will be the substituted entity name.
    \param vectorIndex For languages that support vectorized instances
    such as verilog this will be non-null for a vectorized
    instantation. The value of this will be the current index into the
    instantiation.
    \param rangeOffset Normalized vectorIndex. 
    \param instLang Language type of the the instance
    \param scopeLang Language type of the the scope
  */
  virtual Status hdlComponentInstance(Phase phase, mvvNode inst, const char* instName, mvvNode instMaster, mvvNode oldMaster, mvvNode newMaster, const char* entityName, mvvNode arch, const SInt32* vectorIndex, const UInt32* rangeOffset, mvvLanguageType instLang, mvvLanguageType scopeLang);

  //! For-generate loop callback. Vhdl and verilog
  virtual Status forGenerateLoop(Phase phase, mvvNode node, mvvLanguageType langType);
  //! If-generate callback. Vhdl and verilog
  virtual Status ifGenerate(Phase phase, mvvNode node, mvvLanguageType langType);
  //! If-generate true path callback. Vhdl and verilog
  /*!
    This gets called with the true path of the if-else clause

    \param phase Phase of the traversal
    \param node if-generate true path node
    \param blockLabel NULL for verilog, the name of the if block for
    vhdl. For verilog, a generate block representing the ifgen block
    will be traversed and that will have the block name.
    \param langType Language type
  */
  virtual Status ifGenerateTrue(Phase phase, mvvNode node, const char* blockLabel, mvvLanguageType langType);

  //! Substitute an entity declaration
  /*!
    Currently only vlog, but could be applied to vhdl as well. May
    need to add architecture for this to work with vhdl. May need that
    with verilog too if configurations are supported.

    \param entityDecl The entity to possibly substitute
    \param entityLanguage Language of the entity
    \param entityDeclName The name of the entity
    \param instance The instance that is instantiating the entity
    \param isUDP True if this is a user defined module
    \param callingLanguage The language that is instantiating the
    entity
  */
  virtual mvvNode substituteEntity(mvvNode entityDecl, mvvLanguageType entityLanguage, const UtString& entityDeclName, mvvNode instance, bool isUDP, mvvLanguageType callingLanguage);

  //! Checks if a verilog module contains a foreign attribute which specifies the actual VHDL entity and architecture to use in place of the module
  virtual Status vlogForeignAttribute(mvvNode module, const UtString&, mvvNode* ent, mvvNode* arch);

  //! portList callback. Vhdl and verilog
  virtual Status portList(Phase phase, mvvNode node, mvvLanguageType langType);

  //! declarations callback. Vhdl and verilog
  /*!
    This gets called in vhdl to handle the declaration list. In
    Verilog, this handles the net and variable declarations of modules
    and net declarations of udps.
  */
  virtual Status declarations(Phase phase, mvvNode node, mvvLanguageType langType);
  
  //! Declaration callback. Vhdl ONLY.
  /*!
    This gets called in vhdl for every net and variable declaration inside
    package, architecture and process.
  */
  virtual Status declaration(Phase phase, mvvNode node, mvvLanguageType langType);
  
  // Following are the vhdl v. vlog callbacks. Not sure if some of
  // these should get merged somehow.

  /*!
    \defgroup InterraCallbackVerilog Verilog-only functions in the
    InterraCallback class.
    @{
  */

  //! For-Generate loop item with integer index. Needed for verilog
  /*!
    This gets called before traversing the current item within the
    for-generate loop.
  */
  virtual Status forGenerateIntIndex(Phase phase, mvvNode node, SInt32 index, mvvLanguageType langType);


  //! generate block item list.
  virtual Status generateBlockItemList(Phase phase, mvvNode node, mvvLanguageType langType);

  //! generate item
  virtual Status generateItem(Phase phase, mvvNode node, mvvLanguageType langType);

  //! User-defined procedure/process. Called from entity in the walk
  virtual Status udp(Phase phase, mvvNode node, const char* unit, const char* precision, mvvLanguageType langType);

  //! module callback. This gets called from entity in the walk.
  virtual Status module(Phase phase, mvvNode node, const char* unit, const char* precision, mvvLanguageType langType);

  //! cModel callback. This gets called after module port processing.
  virtual Status cModel(Phase phase, mvvNode node, mvvLanguageType langType);
  
  //! Case-generate callback
  virtual Status caseGenerate(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Case-generate true path callback. 
  /*!
    Called with the true case statement in a case-generate block.
  */
  virtual Status caseGenerateTrue(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Gate primitive callback
  virtual Status gatePrimitive(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Net callback
  virtual Status net(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Local Net callback
  virtual Status localNet(Phase phase, mvvNode node, mvvLanguageType langType);

  //! Port callback
  virtual Status port(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Continuous assign callback
  virtual Status contAssign(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Initial block callback
  virtual Status initialBlock(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Always block callback
  virtual Status alwaysBlock(Phase phase, mvvNode node, mvvLanguageType langType);

  //! Variable net callback.
  virtual Status variable(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Task/Function callback. Maybe this should be split into two?
  virtual Status taskFunction(Phase phase, mvvNode node, const char* tfName, mvvLanguageType langType);
  //! GenerateBlock callback.
  /*!
    \param blockName The name of the block. If the block is unnamed,
    this is assigned a name, like genblk1.
    \param genForIndex NULL if this block is not from a
    generate-for. Otherwise, it points to the value of the index.
  */
  virtual Status generateBlock(Phase phase,
			       mvvNode node,
			       const char* blockName,
			       mvvLanguageType langType, 
			       const SInt32* genForIndex);
  //! Local net list declarations
  virtual Status localNetList(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Local variable list declarations
  virtual Status localVariableList(Phase phase, mvvNode node, mvvLanguageType langType);
  //! scope variable list declarations (aka hierrefs)
  virtual Status scopeVariableList(Phase phase, mvvNode node, mvvLanguageType langType);
  //! generate block net declarations list
  virtual Status generateBlockNetList(Phase phase, mvvNode node, mvvLanguageType langType);
  //! task/function list callback
  virtual Status taskFunctionList(Phase phase, mvvNode node, mvvLanguageType langType);
  //! initial block list callback
  virtual Status initialBlockList(Phase phase, mvvNode node, mvvLanguageType langType);
  //! continuous assign list callback
  virtual Status contAssignList(Phase phase, mvvNode node, mvvLanguageType langType);
  //! declared net assignment list callback
  /*!
    A list of declared nets that are continuously assigned in their declaration.
    for example, in Verilog:
    wire net = 1'b0;

    \param node The module associated with the decl assign list. Would
    like to pass back the iterator, but there may be more than one
    iterator - variables and nets.
  */
  virtual Status declaredAssignList(Phase phase, mvvNode node, mvvLanguageType langType);

  //! always block list callback
  virtual Status alwaysBlockList(Phase phase, mvvNode node, mvvLanguageType langType);
  //! specify block list callback
  virtual Status specifyBlockList(Phase phase, mvvNode node, mvvLanguageType langType);

  //! Scope Variable callback (aka hierref)
  virtual Status scopeVariable(Phase phase, mvvNode node, mvvLanguageType langType);  

  //! declared assign callback.
  /*!
    Currently this is only used in the generateItem walk. Apl changed
    the module decl assign to deal with initial v. continuous
    assignments for declared assigns. I'm not sure if that also
    applies to generates. This needs further investigation. For now,
    the code that is checked in is being used which is a call to
    declassign assuming continuous assignments. - MS

    Also, this gets called on all declared nets. Currently, the
    callback has to handle whether or not their is a declared
    assign. This will get fixed up in a subsequent commit.
  */
  virtual Status declaredAssign(Phase phase, mvvNode node, mvvLanguageType langType);  

  //! Specify block callback
  virtual Status specifyBlock(Phase phase, mvvNode node, mvvLanguageType langType);  

  /*!
    @}

    \defgroup InterraCallbackVhdl Vhdl-only functions in the
    InterraCallback class.
    @{
  */
  
  // currently vhdl only

  //! For-Generate loop item with index expr. Used with vhdl
  /*!
    This gets called before traversing the current item within the
    for-generate loop.
  */
  virtual Status forGenerate(Phase phase, mvvNode node, mvvNode indexExpr, mvvLanguageType langType);

  
  //! Architecture callback. Called from entity in design walk.
  virtual Status architecture(Phase phase, mvvNode node, const char* archName, mvvLanguageType langType);
  //! Block statement callback
  virtual Status blockStmt(Phase phase, mvvNode node, const char* blockName, mvvLanguageType langType);
  //! Concurrent statement list callback
  virtual Status concurrentStmtList(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Concurrent statement callback
  virtual Status concurrentStmt(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Concurrent statement callback
  virtual Status concSigAssign(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Concurrent process callback
  virtual Status concProcess(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Conditional Signal Assign callback
  virtual Status conditionalSigAssign(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Select signal assign callback
  virtual Status selSigAssign(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Concurrent procedure callback
  virtual Status concProcCall(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Assert statement callback
  virtual Status assertStmt(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Use clause callback
  virtual Status useClause(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Generic clause callback (vhdl parameters)
  virtual Status genericClause(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Package use callback
  virtual Status package(Phase phase, mvvNode node, mvvLanguageType langType);
  //! Package declaration callback. Walked to from package use clause.
  /*!
    \param actualLib The library name internally used by cbuild
    \param logicalLib The original library name
    \param packName The original package name
    \param isStdLib Is the library a standard VHDL library
   */
  virtual Status packageDecl(Phase phase, mvvNode node, mvvLanguageType langType,
                             const char* libName, const char* packName,
                             bool isStdLib);
  //! Package body callback. Walked to from package use clause.
  virtual Status packageBody(Phase phase, mvvNode node, mvvLanguageType langType);
  //! file declaration callback
  virtual Status file(Phase phase, mvvNode node, mvvLanguageType langType);
  //! alias type callback
  virtual Status alias(Phase phase, mvvNode node, mvvLanguageType langType);
  
  /*!
    @}

    \defgroup InterraCallbackError Error functions in the
    InterraCallback class. Some of these are obviously
    language-specific, but it is best to define them anyway in your
    callback.
    \addtogroup InterraCallbackVhdl
    @{
  */

  // error cases, warnings, notes

  //! Report an unnamed generate block.
  /*!
    This doesn't return a status because this is just an informative
    note to the callback mechanism.
    Currently, this can only happen in Verilog, but the language is
    passed anyway.

    \param ve_node The generate block node.
    \param blockName The name that has been assigned to the unnamed generate block.
    (Unnamed generate blocks are assigned names like 'genblk1'.)
    \param langType (Currently only MVV_VERILOG is supported)
  */
  virtual void unnamedGenerateBlock(veNode ve_node, const char* blockName, mvvLanguageType langType);
  
  //! Empty Architecture callback
  /*!
    By default returns eNormal.
  */
  virtual Status emptyArchitecture(mvvNode node, const char* archName, mvvLanguageType langType);


  //! Unsupported concurrent statement callback
  /*!
    By default returns eStop;
  */
  virtual Status unsupportedConcStmt(mvvNode vh_conc, mvvLanguageType langType);

  //! Unsupported instance type callback
  /*!
    By default returns eStop;
  */
  virtual Status unsupportedInstanceType(mvvNode vh_conc, mvvLanguageType langType);
  
  //! Generate elaboration failure callback
  /*!
    By default returns eStop. The status here will be
    returned from the stmt list processing. No matter what is returned
    the current generate statement processing will not continue. If
    eSkip or eNormal is returned, eNormal status is used. eStop will
    stop all processing.
  */
  virtual Status generateElabFail(mvvNode node, mvvLanguageType langType);

  //! Generate elaboration error callback
  /*!
    By default returns eStop. The status here will be
    returned from the stmt list processing. No matter what is returned
    the current generate statement processing will not continue. If
    eSkip or eNormal is returned, eNormal status is used. eStop will
    stop all processing.
  */
  virtual Status generateElabError(mvvNode node, mvvLanguageType langType);

  //! Nested generate instance
  /*!
    By default returns eStop.

    This particular case where a generate is generating a
    generate cannot happen. This is just a failsafe.
  */
  virtual Status nestedGenerateInstance(mvvNode node, mvvLanguageType langType);

  //! Unsupported generate item callback
  /*!
    By default returns eStop.
  */
  virtual Status unsupportedGenerateItem(mvvNode node, mvvLanguageType langType);

  //! Component instance elaboration failure
  /*!
    By default returns eStop. The status here will be
    returned from the component process. No matter what is returned
    the current component traversal will not continue. If
    eSkip or eNormal is returned, eNormal status is used. eStop will
    stop all processing.
  */
  virtual Status componentElabFail(mvvNode component, mvvLanguageType lang, int elab_status);

  //! Unsupported declaration error
  /*!
    By default returns eStop.
  */
  virtual Status unsupportedDeclaration(mvvNode component, mvvLanguageType lang);

  //! Implicit Guard is NULL in Guarded Block
  /*!
    By default returns eStop.
  */
  virtual Status implicitGuardNullInGuardedBlock(mvvNode, mvvLanguageType lang);
  //! @}


  // append to str the location and type information about node/langType
  void addLocationAndType(mvvNode node, mvvLanguageType langType, UtString* str);

protected:

  mvvNode getSubstituteEntity(UtString parentEntityName,
			      mvvNode entityDecl, 
			      mvvLanguageType entityLanguage, 
			      const UtString& entityDeclName, 
			      const UtString& instanceName,
			      bool isUDP, 
			      mvvLanguageType callingLanguage,
			      TicProtectedNameManager* ticProtectedNameMgr,
			      IODBNucleus* iodb,
			      UtString* new_mod_name,
			      bool* found,
			      UtMap<UtString,UtString>** portMap);

  //! Determines if a Verilog module has a foreign attribute that specifies a VHDL entity and
  //! architecture in a specific library to use as a substitute using the OVI interface.
  /*!
    \param vlogNode - The Verilog module
    \param ent      - The VHDL Entity
    \param arch     - The VHDL Architecture
   */
  Status getEntArchPairFromForeignAttr(mvvNode vlogNode, mvvNode* ent, mvvNode* arch);

private:
  //! Gets the library, entity and architecture name from the supplied string
  /*!
    \param libEntArch - The original string
    \param sep        - The separators to be use to tokenize the string
    \param libName    - The library name
    \param entName    - The entity name
    \param archName   - The architecture name
   */
  void getLibEntArch(UtString libEntArch, UtString sep, UtString& libName, UtString& entName, UtString& archName);
};

//! Object to traverse a design parsed by Interra (cheetah/jaguar).
/*!
  The user supplies the callback. This walker does not remember
  visited nodes. It is up to the callback to decide whether to skip a
  node.

  The purpose of this walker is to have one consistent way of walking
  a design that interra parses. This is needed to allow for name-based
  associations with signals in unnamed blocks (generate blocks, for
  instance). This allows for population to be done in phases, so that
  uniquification can be done on modules/entities that cannot be
  analyzed until after they are traversed. Module uniquification based
  on defparams is a good example. A defparam may cross several modules
  that do not have parameters, which means that those modules that are
  instantiated more than once and are crossed by the defparam need to
  be uniquified. It is impossible to know that it needs to be
  uniquified unless you've already traversed the design.


  In an effort to make vhdl and verilog transitions be somewhat
  seamless, the hdlComponentInstance() and hdlEntity() methods are used for
  both languages. The hdlComponentInstance() method traverses a module
  instance in verilog and a component instance in vhdl. It also
  handles the language transition, and calls the hdlEntity() method with
  the master mvvNode. The entity method, in turn, traverses the
  language-specific node. 

  I'm sure several things will change in the interface of this
  walker. For one, I think that instance and module names need to be
  passed down to certain levels of traversals and used in the callback
  mechanism to avoid lots of similar code to get a name of a
  module/instance.
*/
class InterraDesignWalker
{
public:
  //! Constructor
  InterraDesignWalker(InterraDesignWalkerCB* callback, TicProtectedNameManager* tic_protected_nm_mgr);

  //! Destructor
  ~InterraDesignWalker();

  //! Walk the design
  /*!
    This begins the traversal of the design. If rootMod is NULL, this
    does nothing and returns eNormal.

    \param rootMod The primary design unit.
    \param architecture The architecture of the design unit. This is
    NULL for verilog designs.
    \param blk_cfg The configuration declaration. 
    \param logical_work The logical work library for VHDL designs.
  */
  InterraDesignWalkerCB::Status design(mvvNode rootMod, mvvNode architecture,
                                       mvvNode blk_cfg, const char* logical_work);

private:
  InterraDesignWalkerCB* mCallback;
  TicProtectedNameManager* mTicProtectedNameMgr;
  UtString mLogicalWorkLib; // Logical work library for VHDL designs.

  void getHdlEntityName(mvvNode entity, mvvLanguageType langType, UtString* nameBuf, bool* isUDP);

  //! Object to help with scoping generate blocks in verilog
  /*!
    The proposed Verilog LRM changes have generate blocks numbered as
    they appear in a scope. This can be modeled with a simple integer,
    but assuming extensions to generates will happen, it would be nice
    to keep the changes to the interface of genitems local to a closure.
    Also, I find it more pleasant on the eyes and fingers to pass
    object pointers as function parameters than modifiable PODs.
  */
  class VlogGenItemClosure
  {
  public:
    //! constructor
    VlogGenItemClosure() : mGenBlkCnt(0)
    {}
    
    //! destructor
    ~VlogGenItemClosure() {}
    
    //! Increment the generate block count number
    void incrGenBlkCnt() { ++mGenBlkCnt; }
    
    //! Get the generate block count number
    UInt32 getGenBlkCnt() const { return mGenBlkCnt; }
    
  private:
    UInt32 mGenBlkCnt;
  };
  
  //! Walk an entity of any language
  /*!
    The term, 'entity', is overloaded here. It means an entity for a
    vhdl design and a module for a verilog design. However, this
    handles the language aspect and calls the appropriate language
    traversal routine.

    \param node The entity to walk
    \param architecture The associated architecture of the
    entity. This is NULL for verilog.
    \param blk_cfg The block configuration. NULL for designs without
    configurations. Currently, not supported for verilog.
  */
  InterraDesignWalkerCB::Status hdlEntity(mvvNode node, mvvNode architecture, mvvNode blk_cfg);

  //! Walk a component instance of any language
  /*!
    The term, 'component', is overloaded here. It mean a component for
    a vhdl design and a module for a verilog design. So, this
    traverses a module instance in a verilog design and a component
    instance in a vhdl design.
  */
  InterraDesignWalkerCB::Status hdlComponentInstance(mvvNode component, mvvNode blk_cfg, mvvLanguageType lang);

  InterraDesignWalkerCB::Status localNet(mvvNode node, mvvLanguageType lang);
  InterraDesignWalkerCB::Status net(mvvNode node, mvvLanguageType lang);
  InterraDesignWalkerCB::Status variable(mvvNode node, mvvLanguageType lang);
  InterraDesignWalkerCB::Status port(mvvNode node, mvvLanguageType lang);
  InterraDesignWalkerCB::Status alias(mvvNode node, mvvLanguageType lang);
  InterraDesignWalkerCB::Status file(mvvNode node, mvvLanguageType lang);


  InterraDesignWalkerCB::Status architecture(vhNode vh_arch, mvvNode blk_cfg);
  InterraDesignWalkerCB::Status concurrentStmtList(vhNode vh_conc, mvvNode blk_cfg);
  InterraDesignWalkerCB::Status concurrentStmt(vhNode vh_conc, mvvNode blk_cfg);
  InterraDesignWalkerCB::Status vhdlBlockStmt(vhNode vh_conc_stmt, mvvNode blk_cfg);

  InterraDesignWalkerCB::Status vhdlAssertStmt(vhNode vh_node);
  InterraDesignWalkerCB::Status vhdlConcProcCall(vhNode vh_node);
  InterraDesignWalkerCB::Status vhdlSelSigAssign(vhNode vh_node);
  InterraDesignWalkerCB::Status vhdlConditionalSigAssign(vhNode vh_node);
  InterraDesignWalkerCB::Status vhdlConcProcess(vhNode vh_node);
  InterraDesignWalkerCB::Status vhdlConcSigAssign(vhNode vh_node);
  InterraDesignWalkerCB::Status vhdlForGenerate(vhNode vh_conc_stmt, mvvNode blk_cfg);
  InterraDesignWalkerCB::Status vhdlIfGenerate(vhNode vh_conc_stmt, mvvNode blk_cfg);
  InterraDesignWalkerCB::Status vhdlPackage(vhNode vh_package);
  InterraDesignWalkerCB::Status vhdlPackageDecl(const char* logical_lib, const char* pack_name);
  InterraDesignWalkerCB::Status vhdlPackageBody(const char* logical_lib, const char* pack_name);


  InterraDesignWalkerCB::Status processVhdlUseClause(vhNode vh_entity);
  InterraDesignWalkerCB::Status processVhdlPortClause(vhNode vh_entity);
  InterraDesignWalkerCB::Status processVhdlGenericClause(vhNode vh_entity);
  InterraDesignWalkerCB::Status processVhdlDeclarations(vhNode n_stm);
  


  InterraDesignWalkerCB::Status vlogTF(veNode ve_node);
  InterraDesignWalkerCB::Status vlogAlwaysBlock(veNode ve_node);
  InterraDesignWalkerCB::Status vlogInitialBlock(veNode ve_node);
  InterraDesignWalkerCB::Status vlogContAssign(veNode ve_node);
  InterraDesignWalkerCB::Status vlogScopeVariable(veNode ve_node);
  InterraDesignWalkerCB::Status vlogSpecifyBlock(veNode ve_node);
  InterraDesignWalkerCB::Status vlogDeclAssign(veNode ve_node);
  
  InterraDesignWalkerCB::Status vlogGatePrimitive(veNode ve_inst);

  InterraDesignWalkerCB::Status vlogGenerateStmtList(veNode ve_module, mvvNode blk_cfg);

  InterraDesignWalkerCB::Status vlogGenerate(veNode veGenerate, mvvNode blk_cfg, VlogGenItemClosure* genItemClosure);
  InterraDesignWalkerCB::Status vlogGenerateItem(veNode ve_item, mvvNode blk_cfg, const SInt32* genForIndex, VlogGenItemClosure* genItemClosure);
  InterraDesignWalkerCB::Status vlogCaseGenerate(veNode veGenerate, mvvNode blk_cfg, VlogGenItemClosure* genItemClosure);
  InterraDesignWalkerCB::Status vlogIfGenerate(veNode veGenerate, mvvNode blk_cfg, VlogGenItemClosure* genItemClosure);
  InterraDesignWalkerCB::Status vlogForGenerate(veNode veGenerate, mvvNode blk_cfg, VlogGenItemClosure* genItemClosure);
  InterraDesignWalkerCB::Status vlogGenerateBlock(veNode ve_node, mvvNode blk_cfg, const SInt32* genForIndex, VlogGenItemClosure* genItemClosure);

  InterraDesignWalkerCB::Status vlogModule(veNode module, mvvNode blk_cfg, bool isCmodel);
  InterraDesignWalkerCB::Status vlogUDP(veNode udp, bool isCmodel);

  InterraDesignWalkerCB::Status processVlogUdpDeclarations(veNode ve_udp);
  InterraDesignWalkerCB::Status processVlogModuleDeclarations(veNode ve_module);

  InterraDesignWalkerCB::Status processVlogPortList(veNode veModule, CheetahList& ve_port_iter);
  InterraDesignWalkerCB::Status processVlogLocalNetList(veNode ve_module, CheetahList& ve_net_iter);
  InterraDesignWalkerCB::Status processVlogLocalVarList(veNode ve_module, CheetahList& ve_var_iter);
  InterraDesignWalkerCB::Status processVlogScopeVarList(veNode ve_module, CheetahList& ve_var_iter);
  InterraDesignWalkerCB::Status processVlogGenerateBlockNetList(veNode ve_module, CheetahList& ve_var_iter);
  InterraDesignWalkerCB::Status processVlogTFList(veNode ve_module);
  InterraDesignWalkerCB::Status processVlogInitialBlockList(veNode ve_module);
  InterraDesignWalkerCB::Status processVlogContAssignList(veNode ve_module);
  InterraDesignWalkerCB::Status processVlogDeclAssignList(veNode ve_module);
  InterraDesignWalkerCB::Status processVlogAlwaysBlockList(veNode ve_module);
  InterraDesignWalkerCB::Status processVlogSpecifyBlockList(veNode ve_module);

  InterraDesignWalkerCB::Status 
  hdlComponentInstanceDeclaration(mvvNode component, mvvNode entity, 
                                  mvvNode old_entity, mvvNode new_entity, 
                                  const char* entityName, mvvNode archDecl, 
                                  mvvNode instance_cfg, mvvLanguageType lang,
                                  mvvLanguageType elabLang);

  InterraDesignWalkerCB::Status 
  processHdlComponentInstance(mvvNode component, 
                              mvvLanguageType formalLang, 
                              mvvLanguageType elabLang, 
                              mvvNode entityDecl, 
                              mvvNode oldEntity,
                              mvvNode newEntity,
                              const char* entityName,
                              mvvNode archDecl, 
                              mvvNode instance_config,
                              const SInt32* vectorIndex,
                              const UInt32* rangeOffset);
  
  InterraDesignWalkerCB::Status processVhdlBlockGuardSignal(vhNode vh_block);

  InterraDesignWalkerCB::Status processVlogGenerateBlockItemList(veNode veGenerate, 
								 mvvNode blk_cfg, 
								 const SInt32* genForIndex, 
								 VlogGenItemClosure* genItemClosure); 

  void addLocationAndType(mvvNode node, mvvLanguageType langType, UtString* str);
};

#endif
