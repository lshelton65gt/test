// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef ELABORATE_H_
#define ELABORATE_H_

#include "flow/FLFactory.h"
#include "util/SourceLocator.h"
#include "util/UtPair.h"

class MsgContext;
class LFContext;
class ArgProc;
class Stats;
class AtomicCache;
class ConstantRange;
class IODBNucleus;

class ClockAndDriver;

class Elaborate
{
public:
  //! Constructor
  Elaborate(STSymbolTable *symtab,
            AtomicCache *str_cache,
            SourceLocatorFactory *loc_factory,
            FLNodeFactory *flow_factory,
            FLNodeElabFactory *flow_elab_factory,
            NUNetRefFactory *net_ref_factory,
            MsgContext *msg_context,
            ArgProc *args,
            IODBNucleus* iodb) :
    mSymtab(symtab),
    mStrCache(str_cache),
    mLocFactory(loc_factory),
    mFlowFactory(flow_factory),
    mFlowElabFactory(flow_elab_factory),
    mNetRefFactory(net_ref_factory),
    mMsgContext(msg_context),
    mArgs(args),
    mIODB(iodb)
  {}

  //! Destructor
  ~Elaborate()
  {
    INFO_ASSERT(mDeferredUseMultiMap.empty(),     "Elaboration failed to apply some deferred connections!");    
    INFO_ASSERT(mDeferredEdgeUseMultiMap.empty(), "Elaboration failed to apply some deferred connections!");    
  };

  //! Enum to identify net elaboration passes
  /*!
   * We do elaboration in two passes.
   * First pass elaborates local nets.
   * Second pass resolves hierarchical references.
   */
  enum ElabPass {
    eElabLocals,     //!< Elaborate local nets, but not hierarchical refs
    eElabHierRefs,   //!< Elaborate hierarchical refs
    eElabReconData   //!< Elaborate reconstruction data
  };

  /*!
   * Map unelaborated flow to multiple elaborated flow nodes.
   */
  typedef UtMultiMap<FLNode*,FLNodeElab*> FlowElabMultiMap;
  typedef FlowElabMultiMap::iterator FlowElabMultiMapIter;
  typedef std::pair<FlowElabMultiMapIter,FlowElabMultiMapIter> FlowElabMultiMapRange;

  //! Elaborate the given design.
  /*!
   * Walk the design and elaborate it:
   * . create symbol table entries
   * . uniquify the flow nodes
   *
   * The declarations are processed first so that cross-scope references have a
   * chance to work, and to make port processing easy.
   */
  void design(NUDesign * design, bool elaboratePorts = true, bool elaborateFlow = true);

  void designDecl(NUDesign * design);

  void designFlow(NUDesign * design, bool elaboratePorts=true);

  void elabScopeDecl(STBranchNode * hier, NUBlock * block);

  void elabScopeFlow(STBranchNode * hier, 
		     FLNodeSet & starting_flow,
		     NUNetList & net_list,
		     FLNodeSet * stoppers=0,
                     ClockAndDriver * clock_and_driver=0,
                     bool elaboratePorts=true,
                     FLNodeElabSet* covered = NULL);

private:
  static NUNetElab* helperGetFormalElab(NUPortConnection *portconn,
                                        STBranchNode* hier);

  void elabConnectFlow(FlowElabMultiMap *flow_map, FLNodeSet * stoppers=0, ClockAndDriver* clock_and_driver=0, FLNodeElabSet* covered = NULL);
  void elabModuleVirtualNet(NUModule *module, NUNet *(NUModule::* getVirtualNetFn)() const, bool atTop, LFContext *context);
  void elabModuleVirtualNets(NUModule *module, bool atTop, LFContext *context);

  //! Elaborate nets in the given module and in the instantiated modules.
  /*!
   * Elaborate the declarations of the given module in the given context, and
   * recurse into its instances.
   */
  void elabModuleDecl(NUModule *module, LFContext *context, ElabPass pass);

  //! Elaborate the declarations of the given module instance in the given context.
  void elabModuleInstanceDecl(NUModuleInstance *module_instance,
                              LFContext *context,
                              ElabPass pass);

  void elabModuleFlow(NUModule *module, LFContext *context, bool elaboratePorts);
  void elabModuleInstanceFlow(NUModuleInstance *module_instance, LFContext *context, bool elaboratePorts);
  void elabBlock(NUBlock *block, LFContext *context, ElabPass);
  void elabDeclarationScope(NUNamedDeclarationScope * scope, LFContext *context, ElabPass);

  //! Elaborate the net in the given context.
  void elabNet(NUNet *net, LFContext *context);

  //! Elaborate the hierarchical net reference in the given context.
  void elabNetHierRef(NUNet *net, LFContext *context);

  //! Elaborate the reconstruction data for one net in the given context.
  void elabNetReconData(NUNet *net, LFContext *context);

  //! Elaborate the reconstruction data for a leaf node in the given context.
  void elabLeafReconData(STAliasedLeafNode * local_leaf, LFContext * context);

  void elabScopeLocalsFlow(NUScope *scope, FlowElabMultiMap *flow_map, LFContext *context, bool elaboratePorts);
  void elabScopeNetListFlow(STBranchNode * hier, NUNetList & net_list, FlowElabMultiMap *flow_map, LFContext *context,
                            FLNodeSet * stoppers=0, ClockAndDriver* clock_and_driver=0, bool elaboratePorts = true);
  void elabTF(NUTF *tf, LFContext *context, ElabPass pass);

  /*!
   * Elaborate a single driver for a net, and add the elaborated node to
   * the ongoing driver set.
   *
   * This is a helper function for ElabScopeNetListFlow.
   */
  void elabSingleNetDriverFlow(FLNode *flow_node,
                               FLNodeElabSet *driver_set,
                               FlowElabMultiMap *flow_map,
                               LFContext *context,
                               FLNodeSet *stoppers,
                               ClockAndDriver *clock_and_driver,
                               bool elaboratePorts);

  void elabScopeFlowSetFlow(FLNodeSet & starting_flow,
			    FlowElabMultiMap * flow_map,
			    LFContext * context,
			    FLNodeSet * stoppers,
                            ClockAndDriver * clock_and_driver,
                            bool elaboratePorts);

  //! Elaborate the flow for the fanin of the given flow_node.
  /*!
   * Keep a map of FLNode to FLNodeElab in flow_map.
   *
   * Note that we do not replicate module instance flow nodes.
   */
  void elabFlow(FLNode *flow_node, FlowElabMultiMap *flow_map, LFContext *context,
                FLNodeSet *stoppers=0, ClockAndDriver * clock_and_driver=0, bool elaboratePorts=true);

  //! Deferred connection description. 
  /*! 
    The FLNode is the fanout node needing an arc. If the bool is true,
    this is an edge-sensitive arc.
   */
  typedef UtPair<FLNode*,bool>                ConnectionDesc;

  //! Set of deferred connections.
  typedef UtSet<ConnectionDesc>               ConnectionDescSet;

  //! Map used to remember connections associated with module instances.
  typedef UtMap<FLNode*,ConnectionDescSet>    FlowConnectionMap;

  //! Map from nets to nodes needing a fanin arc.
  typedef UtMap<NUNetElab*,ConnectionDescSet> ConnectionMap;

  //! Add deferred connections for flow around module boundaries
  void addDeferredConnections(NUNetElab* net_elab,
                              const ConnectionDescSet& connections,
                              const FlowConnectionMap& moduleFanouts,
                              FlowElabMultiMap *flow_map);

  //! Connect drivers of net elabs to their fanout for hierarchically driven nets.
  /*!
   * The flow elaboration pass has set up the drivers onto the net elab, so
   * this connects all the drivers on the net elab to their fanouts.
   *
   * The edge parameter controls whether the fanin is added as an edge or level fanin.
   */
  void fixupDeferredFlows(FLNodeElabMultiMap &deferred_multimap, bool edge);

  void connectOneFlowFanin(FlowElabMultiMap *flow_map,
                           FLNodeSet * stoppers,
                           ClockAndDriver *clock_and_driver,
                           FLNodeElab * flow_node_elab, 
                           FLNode * unelab_fanin,
                           FLElabUseCache* cache,
                           void (Elaborate::* connectFaninFn)(FLNodeElab*,FLNodeElab*, FLElabUseCache*) );
  void connectOneElabFlowFanin(FlowElabMultiMap *flow_map,
                               ClockAndDriver * clock_and_driver,
                               FLNodeElab * flow_node_elab, 
                               FLNode * unelab_fanin,
                               FLElabUseCache* cache,
                               void (Elaborate::* connectFaninFn)(FLNodeElab*,FLNodeElab*,FLElabUseCache*) );

  //! Connect fanin to fanout.
  /*!
   * This may delay the connection for deferred flows; see connectFaninWorker.
   */
  void connectFanin(FLNodeElab *fanout, FLNodeElab *fanin, FLElabUseCache* cache);
  void connectEdgeFanin(FLNodeElab *fanout, FLNodeElab *fanin, 
                        FLElabUseCache* cache);

  //! Connect fanin to fanout.
  /*!
   * This will NOT delay the connection for deferred flows; see connectFaninWorker.
   */
  void connectFaninNoDefer(FLNodeElab *fanout, FLNodeElab *fanin,
                           FLElabUseCache* cache);
  void connectEdgeFaninNoDefer(FLNodeElab *fanout, FLNodeElab *fanin,
                               FLElabUseCache* cache);

  //! Worker function for connectFanin, connectEdgeFanin, connectFaninNoDefer, etc.
  /*!
   * If deferred_multimap is non-NULL and fanin is a continuous driver of a
   * deferred net, we delay the connection to fanout until all
   * continuous drivers of that net are seen.
   *
   * This routine will only add the fanin if its defs overlap the fanout's uses.
   */
  void connectFaninWorker(FLNodeElab *fanout,
                          FLNodeElab *fanin,
                          FLElabUseCache* cache,
                          FLNodeElabMultiMap *deferred_multimap,
                          void (FLNodeElab::* connectFaninFn)(FLNodeElab*));

  //! Add the driver to the net_elab
  void addContinuousDriver(NUNetElab *net_elab, FLNodeElab *driver);

  //! Pre-flow elaboration; must call before doing flow elaboration.
  /*!
   * Initialize the maps which keep track of hierarchically written nets to their
   * fanout.
   */
  void preFlowElaboration();

  //! Post-flow elaboration; must call after doing flow elaboration
  /*
   * Connect the flow nodes for nets which are hierarchically read
   * to their readers.
   */
  void postFlowElaboration();

  //! Remove any elaborated bound nodes which have type eFLBoundHierRef
  /*!
   * We elaborate the bound nodes for hierrefs because we need to remember the
   * elaborated fanout of hierarchically-referenced nets.
   * However, we do not actually use the elaborated hierref bound nodes in the
   * elaborated graph.  So, we need to clean them up once elaboration is complete.
   */
  void cleanupBoundHierRefs();

  STSymbolTable *mSymtab;
  AtomicCache *mStrCache;
  SourceLocatorFactory *mLocFactory;
  FLNodeFactory *mFlowFactory;
  FLNodeElabFactory *mFlowElabFactory;
  NUNetRefFactory *mNetRefFactory;
  MsgContext *mMsgContext;
  ArgProc *mArgs;
  IODBNucleus* mIODB;

  //! Map deferred-connection nets to their fanout.
  FLNodeElabMultiMap mDeferredUseMultiMap;
  FLNodeElabMultiMap mDeferredEdgeUseMultiMap;
};

#endif
