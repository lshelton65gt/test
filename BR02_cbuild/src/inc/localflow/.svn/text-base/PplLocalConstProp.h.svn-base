// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef PPLLOCALCONSTPROP_H
#define PPLLOCALCONSTPROP_H

#include "nucleus/Nucleus.h"
#include "localflow/VhdlPopulate.h"

class Fold;
class LocalPropagationBase;
class LocalPropagationStatistics;
class NUNetRefFactory;
class MsgContext;
class IODBNucleus;
class ArgProc;
class UD;

//! Local constant propagation during population.
/*!
  This class is used by population for constant propagation on the fly.
  Conventional constant propagation happens with all of the design
  populated in the nucleus in advance. This is not the case when
  nucleus is being populated. This class has constants propagated 
  upto the last populated statement. The new statement to be populated
  can use these constants and update them after it's population is
  complete. 

  The scope ..or localness, of constant propagation is limited to
  within the process and within the subprograms (functions/procedures).
  Constants do not flow from subprogram calls to inside subprograms.
  That is handled by population code which uses Jaguar elaboration
  to do that.

  The constant propagation is based on NUNets and not NUNetRefs.
  This makes is more pessimistic. 
*/
class PplLocalConstProp
{
public:
  
  //! Represents the types of sequential statement blocks that local constant
  //! propagation wants to know about.
  enum StmtBlockT {
    eBlock, eIfThen, eIfElse, eForInitial, eForBody, eForAdvance,
    eCase, eCaseItem, eTask, eStrucProc, eNotBlock
  };

  enum PhaseT {
    eBegin, eEnd, eUnknown
  };

  PplLocalConstProp(Fold* fold, IODBNucleus* iodb, VhdlPopulate::EntityAliasMap* alias_map);
  ~PplLocalConstProp();

  //! Unenroll nets def'ed by this statement and enroll new constants
  //! for this statement. This is called for every new populated statement.
  /*!
    For statements are expected to provide all their def nets in advance.
    Defs for all other statements ..except if and case, are computed.
  */
  void next(NUStmt* stmt, NUNetSet* forStmtDefs, NUScope* declScope);

  //! Indicate begin/end of a sequential statement block.
  /*!
    The next() should be called in the execution order of the statements. For
    example next() should be invoked for for-initial, then for-body and
    finally for-advance statements.
  */
  void next(PhaseT phase, NUBase* block, StmtBlockT blockType);

  //! Invalidate def'ed nets in the statement for constant propagation.
  void applyNonPropagatedDefs(NUStmt* stmt);

  //! Optimizes the given expression based on current propagated constant values.
  NUExpr* optimizeExpr(NUExpr* expr);

private:

  //! Helpers for constant propagation.
  //! Walk in/out of context depending on phase.
  void pushOrPop(PhaseT phase);

  //! If rvalue of the blocking assign statement is a constant, enroll it.
  void propagateConst(NUNet* net, NUBlockingAssign* assign,  NUScope* declScope);
  void enrollAssign(NUBlockingAssign* assign, NUScope* declScope);
  void enrollVhdlAliasAssign(NUBlockingAssign* assign, NUScope* declScope);
  NUConst* getConstantExpr(NUAssign* assign);
  NUConst* checkCopyConstantExpr(NUNet* net, NUConst* constExpr);

  //! Wrappers on LocalPropagationBase routines.
  void pushContext();
  void popContext();
  void pushNesting();
  void popNesting();
  void addNet(NUNet* net, NUExpr* constExpr);
  void applyNonPropagatedDefs(NUNetSet& defs);

  Fold* mFold;
  IODBNucleus* mIODB;
  LocalPropagationStatistics* mStats;
  LocalPropagationBase* mLPBase;
  VhdlPopulate::EntityAliasMap* mAliasMap;    // Map of vhdl Aliases
};

#endif
