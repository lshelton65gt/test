// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  A class to test if an elaborated net represents a latch or flop
*/

#ifndef _ELABSTATETEST_H_
#define _ELABSTATETEST_H_

class NUNetElab;
class FLNodeElab;

//! A class to test/cache if an elaborated net is a latch/flop
/*! The latch/flop flag is only stored on the elaborated net that is
 *  driven by an always block latch/flop. It is not propagated to its
 *  aliases. So to test if an elaborated net is a latch/flop it must
 *  walk all the aliases. But this can be expensive so it is cached by
 *  this class.
 *
 *  Note that doing a transitive closure over all the
 *  aliases would be too pessimistic.
 *
 *  Note that this is only valid after aliasing.
 */
class LFElabStateTest
{
public:
  //! constructor
  LFElabStateTest();

  //! destructor
  ~LFElabStateTest();

  //! Latch test function for an elaborated net.
  /*! This function queries the aliases
   */
  bool isLatch(const NUNetElab* netElab) const;

  //! Latch test function for an elaborated flow.
  /*! This function queries the unelaborated net for the unelaborated
   *  flow.
   */
  bool isLatch(FLNodeElab* flowElab) const;

  //! Flop test function for an elaborated net.
  /*! This function queries the aliases
   */
  bool isFlop(const NUNetElab* netElab) const;

  //! Flop test function for an elaborated flow.
  /*! This function queries the unelaborated net for the unelaborated
   *  flow.
   */
  bool isFlop(FLNodeElab* flowElab) const;

  //! State (latch or flop) test function for an elaborated net.
  /*! This function queries the aliases
   */
  bool isState(const NUNetElab* netElab) const;

  //! State (latch or flop) test function for an elaborated flow.
  /*! This function queries the unelaborated net for the unelaborated
   *  flow.
   */
  bool isState(FLNodeElab* flowElab) const;

private:
  //! Hide the copy constructor
  LFElabStateTest(const LFElabStateTest& src);

  //! Hide the assignment operator
  LFElabStateTest& operator=(const LFElabStateTest& src);

  //! Abstraction for state flags
  enum StateFlags {
    eSFNone  = 0,
    eSFLatch = (1 << 0),
    eSFFlop  = (1 << 1)
  };

  //! Helper function to get the cached state flags
  StateFlags getStateFlags(const NUNetElab* netElab) const;

  //! Abstraction for a cache of elaborated nets
  typedef UtMap<const NUNetElab*, StateFlags> ElabLatchFlag;

  //! Storage for the cache
  mutable ElabLatchFlag* mElabLatchFlag;
}; // class LFElabStateTest

#endif // _ELABSTATETEST_H_
