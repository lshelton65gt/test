// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef VHDLPOPULATE_H_
#define VHDLPOPULATE_H_

#include "localflow/Populate.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUTF.h"
#include "flow/Flow.h"
#include "compiler_driver/Interra.h"
#include "util/UtStackPOD.h"

class LFContext;
class ArgProc;
class Stats;
class AtomicCache;
class DynBitVector;
class DesignPopulate;
class CarbonContext;
class TicProtectedNameManager;

//! class to manipulate range information from the VHDL type system
/*! Note -- this should arguably be merged with HdlId class in
 *! hdl/HdlId.h, but I needed slightly different functionality
 *! and wanted to limit the scope of my change to VHDL only
 */ 
class VhdlRangeInfo {
public:
  //! ctor
  VhdlRangeInfo();

  //!  Overload
  VhdlRangeInfo (SInt32 msb, SInt32 lsb);

  //! Overload
  VhdlRangeInfo (NUExpr* msb, NUExpr* lsb);

  //! dtor
  ~VhdlRangeInfo();

  //! assignment
  VhdlRangeInfo& operator=(const VhdlRangeInfo& src);

  //! Make this be a vector
  void putVector(SInt32 msb, SInt32 lsb);

  //! Overload
  void putVector (NUExpr* msb, NUExpr* lsb);

  //! Make this be a scalar [0:0]
  void putScalar();

  //! Invalidate this range
  void putInvalid();

  //! Indicate that this is a null range
  void putNullRange(bool isNullRange);

  //! Get the LSB
  SInt32 getLsb(const SourceLocator& loc) const;

  //! Get the LSB expr
  NUExpr* getLsbExpr (const SourceLocator& loc) const;

  //! Get the MSB
  SInt32 getMsb(const SourceLocator& loc) const;

  //! Get the MSB expr
  NUExpr* getMsbExpr (const SourceLocator& loc) const;

  //! Is this a vector?
  bool isVector() const {return mIsVector;}

  //! Is anything initialized?
  bool isValid() const {return mIsValid;}

  //! Is this a non-constant range?
  bool isDynamic () const {return mMsbExpr || mLsbExpr; }

  //! Is the width initialized?
  bool isValidWidth() const {return mIsWidthValid;}

  //! Is the msb/lsb initialized?
  bool isValidMsbLsb() const {return mIsMsbLsbValid;}

  //! Is null range?
  bool isNullRange() const {return mIsNullRange;}

  //! Get the width of this
  UInt32 getWidth(const SourceLocator& loc) const;

  //! Put the width without validating msb/lsb -- this is used for
  //! dynamic part-selects with known width and unknown index
  void putWidth(UInt32 width);

  //! Switch the bounds
  void swap ();

private:
  bool mIsWidthValid;
  bool mIsVector;
  bool mIsMsbLsbValid;
  bool mIsValid;
  bool mIsNullRange;
  NUExpr* mMsbExpr;
  NUExpr* mLsbExpr;
  SInt32 mMsb;
  SInt32 mLsb;
};

// enum type corresponding to the predefined/userdefined STD/IEEE functions 
// having meta comment directive to builtin/mapped operator or feed thru. 

typedef enum
{
  eVHSYN_FEED_THRU,              // Feed thru function
  eVHSYN_S_TO_S,          // Integer to signed conversion function
  eVHSYN_S_TO_I,          // Signed to integer conversion function
  eVHSYN_S_TO_U,             // Integer to unsigned conversion function
  eVHSYN_U_TO_I,             // Unsigned to integer conversion function
  eVHSYN_S_XT,            // Sign extension function, i.e. signed resize
  eVHSYN_U_TO_U,            // Unsigned i.e 0 extension function
  eVHSYN_EQUAL,                  // EQ operator
  eVHSYN_NOT_EQUAL,              // Not equal to operator
  eVHSYN_AND,
  eVHSYN_NAND,
  eVHSYN_OR,
  eVHSYN_NOR,
  eVHSYN_XOR,
  eVHSYN_XNOR,
  eVHSYN_NOT,
  eVHSYN_SIGN_STAR, // SYN_SIGNED_MULT
  eVHSYN_SIGN_MULT,
  eVHSYN_UNS_STAR,
  eVHSYN_UNS_MULT,
  eVHSYN_NUMERIC_SIGN_DIV,
  eVHSYN_NUMERIC_UNS_DIV,
  eVHSYN_BINARY_MINUS,
  eVHSYN_NUMERIC_BINARY_MINUS,
  eVHSYN_NUMERIC_UNARY_MINUS,
  eVHSYN_BINARY_PLUS,
  eVHSYN_NUMERIC_BINARY_PLUS,
  eVHSYN_ABS,
  eVHSYN_UNS_LT,
  eVHSYN_UNS_GT,
  eVHSYN_GT,
  eVHSYN_LT,
  eVHSYN_UNS_GT_EQUAL,
  eVHSYN_UNS_LT_EQUAL,
  eVHSYN_GT_EQUAL,
  eVHSYN_LT_EQUAL,
  eVHSYN_REDUCT_AND,
  eVHSYN_REDUCT_OR,
  eVHSYN_SHL,
  eVHSYN_SHR,
  eVHSYN_SHRA,
  eVHSYN_SHLA,
  eVHSYN_ROL,
  eVHSYN_ROR,
  eVHSYN_REDUCT_XOR,
  eVHSYN_UNS_RESIZE,
  eVHSYN_RESIZE,
  eVHSYN_MOD,
  eVHFLATTEN,
  eVHSYN_NONE      // This should be last line. Do not add anything after this 
} vhBuiltInFuncOrMappedOperatorType;


typedef UtArray<NUExpr*> NUExprArray;
typedef UtArray<NULvalue*> NULvalueArray;
typedef UtArray<vhNode> VhNodeArray;
typedef UtArray<vhExpr> VhExprArray;
typedef UtVector<VhdlRangeInfo> VhdlRangeInfoVector;
typedef UtStack<NUIf* > NUIfstack;

//! This structure holds the position and the width of the
/*! aggregate element. It's used for gathering positions and widths
 *  of the aggregate elements for creation of concat expressions.
 */
typedef struct 
{
  int position;
  int width;
} aggr_elem;
typedef UtList<aggr_elem> AggElem;


//! VhdlPopulate class
/*!
 * Class to walk the Jaguar representation and populate Nucleus.
 *
 * All member functions which populate a Nucleus construct from Jaguar return
 * an error code.By default,the passed-in pointer to the construct is set to 0.
 * If success is returned, or if the compiler was able to create the construct
 * even when an error was encountered, then the passed-in pointer will be non-0
 */
class VhdlPopulate : public Populate
{
public:
  //! Expression signs
  enum SignType
  {
    eVHSIGNED = 1,
    eVHUNSIGNED,
    eVHUNKNOWN
  };

  //! Enum for assignment type
  enum assignType
  {
    eConcurrent,
    eSequentialNonBlocking,
    eSequentialBlocking,
    eInitialValue
  };

  class RecordIterator;
  class AggregateIterator;
  /*! \class JaguarIterator
    \brief Abstract base class for iterating over Jaguar lists
  */
  class JaguarIterator
  {
  public:
    JaguarIterator();
    JaguarIterator( const VhdlPopulate* vhdlPop, vhNode node );
    virtual ~JaguarIterator();
    virtual vhNode next() = 0;
    void rewind();
    UInt32 numScopesExited() const { return mNumScopesExited; }
    virtual RecordIterator *castRecordIterator() { return NULL; }
    virtual AggregateIterator *castAggregateIterator() { return NULL; }
    bool isBranch() const { return mIsBranch; }
  protected:
    const VhdlPopulate *mVhdlPopulate;
    vhNode mCurrentNode;
    UtStackPOD<JaguarList*> mVhListStack;
    UInt16 mNumScopesExited;
    bool mIsBranch;
  private:
    JaguarIterator& operator=(const JaguarIterator& src );
    JaguarIterator(const JaguarIterator& src);
  };

  
  /*! \class RecordIterator
    \brief Class for iterating over Jaguar records hierarchically
  */
  class RecordIterator : public JaguarIterator
  {
  public:
    RecordIterator( const VhdlPopulate* vhdlPop, vhNode recordNode );
    virtual vhNode next();
    virtual RecordIterator *castRecordIterator();
    void setRecurse( bool recurse ) { mRecurse = recurse; }
    bool getRecurse() const { return mRecurse; }
  private:
    RecordIterator();
    RecordIterator& operator=(const RecordIterator& src );
    RecordIterator(const RecordIterator& src);
    bool mRecurse;
  };

  /*! \class AggregateIterator
    \brief Class for iterating over Jaguar aggregates hierarchically
  */
  class AggregateIterator : public JaguarIterator
  {
  public:
    AggregateIterator( vhNode aggregateNode );
    virtual vhNode next();
    virtual AggregateIterator *castAggregateIterator();
  private:
    AggregateIterator();
    AggregateIterator& operator=(const AggregateIterator& src );
    AggregateIterator(const AggregateIterator& src);
  };

  //! constructor
  VhdlPopulate(DesignPopulate *design_populate,
               STSymbolTable *symtab,
               AtomicCache *str_cache,
               IODBNucleus * iodb,
               SourceLocatorFactory *loc_factory,
               FLNodeFactory *flow_factory,
               NUNetRefFactory *netref_factory,
               MsgContext *msg_context,
               ArgProc *arg,
               bool useLegacyGenerateHierarchyNames,
	       CarbonContext* carbonContext,
	       TicProtectedNameManager* tic_protected_nm_mgr);

  //! Populate an entity/arch.  Entry point for population.
  /*!
   \param vh_entity Jaguar node corresponding to an entity declaration.
   \param vh_arch Jaguar node corresponding to the architecture of the entity.
   \param blk_cfg Jaguar node corresponding to the block which has been configured in the configuration declaration/specification. This will be NULL if the block is not configured. In other words it is the current block configuration (if any) which enables the elaboration of instances within the elaborated instance. The jaguar object type of this node is VHBLOCKCONFIG.
   \param context The LFContext.
   \param uniquifiedName The module name, uniquified by generics.
   \param the_module The NUModule for this entity/architecture.
   */
  ErrorCode entityDecl( vhNode vh_entity,
                        LFContext *context, const char *uniquifiedName, 
                        NUModule **the_module, bool* seenAlready);

  //! Error unsupported declaration statement.
  ErrorCode unsupportedDeclaration(vhNode vh_decl);

  //! Error unsupported concurrent statement
  ErrorCode unsupportedConcStmt(vhNode vh_conc_stmt);

  //! Error implicit Guard signal is NULL in Guarded block
  ErrorCode implicitGuardNullInGuardedBlock(vhNode vh_block);

  //! We currently do not support vhdl assertions.
  ErrorCode assertStmt(vhNode vh_conc_stmt, LFContext*);

  //! Populate a for generate block
  /*!
    \param vh_conc_stmt The for-generate stmt
    \param label The block name of the for-generate
    \param context The LFContext
   */
  ErrorCode forGenerate(vhNode vh_conc_stmt, StringAtom* label, LFContext* context);

  //! Populate the true branch generate block of an if generate
  ErrorCode ifGenerateTrue(vhNode vh_conc_stmt, const char* blockLabel, LFContext* context);
  
  //! populate conditional sig assign
  /*!
    Called from NucleusInterraCB. There is also
    conditonalSigAsgn. Maybe these two should be merged?
  */
  ErrorCode concurrentConditionalSigAssign(vhNode vh_conc_stmt, 
                                           LFContext* context);
  
  //! populate sel sig assign
  /*!
    Called from NucleusInterraCB. There is also
    selectedSigAsgn. Maybe these two should be merged?
  */
  ErrorCode concurrentSelSigAssign(vhNode vh_conc_stmt, 
                                   LFContext* context);
  
  //! populate a concurrent signal assign that may be guarded
  /*!
    Called from NucleusInterraCB. There is also
    concurrentSignalAssign. Maybe these two should be merged?
  */
  ErrorCode maybeGuardedConcSigAssign(vhNode vh_conc_stmt, 
                                      LFContext* context);

  //! Is this type integer or subrange of integer?
  static bool isIntegerType (vhNode node);

  //! Is this type a subrange of integer?
  static bool isConstrainedIntegerType(vhNode node);

  //! Populate a NUModule from VHDL for instantiation in Verilog
  /*! \param ve_inst the Verilog module instance node
      \param primaryUnit The vhdl entity that is being instantiated
      \param offset Vectorized instantation offset. NULL if not vectorized.
      \param context Context of the module doing the instantiation
      \returns ErrorCode The status error code
   */
  ErrorCode VhdlModule( veNode ve_inst,
                        vhNode primaryUnit,
                        const UInt32 *offset,
                        LFContext *context );

  //! destructor
  ~VhdlPopulate();

  //! Create a source locator for the given Jaguar object
  SourceLocator locator(vhNode vh_node);
  //! Create a source locator for the given MVV object
  SourceLocator locator(mvvNode mvv_node);

  //! get the name of a vhNode
  /*! This method keys off the Jaguar object type and makes the correct
   *  API call to get the name of the object as a string, filling in 'buf'
     * \returns true on success, but in all cases buf will be set to something
   */
  static bool getVhdlObjName( vhNode node, UtString* buf);

  //! Determine whether a Jaguar node is of a VHDL record data type
  /*! This method handles both aggregates and net references of varying types.
   */
  static bool isRecordNet( vhNode net, LFContext *context );

  //! Determine whether a Jaguar expr is of type function_name.rec_field
  bool isFunctionRecField( vhExpr vh_expr, vhExpr *funcCallexpr);


  //! The actual library name is $lib_<user library name>. 
  static const char* getActualVhdlLibName(const char* libName, UtString& libStr);

  //! Populate a signal declaration to a NUNet.
  ErrorCode net(vhNode vh_net, LFContext* context, NUNet **the_net);

  //! port population routines.
  ErrorCode local(vhNode vh_net, LFContext* context, NUNet **the_net);
  ErrorCode port(vhNode vh_port, LFContext* context, NUNet **the_net);

  //! Process VHDL block statement
  /*!
   \param vh_block Jaguar node corresponding to the block statement.
   \param blockName This is the name of the block. The VHDL LRM states
   that all block statements must have a block name.
  */
  ErrorCode blockStmt( vhNode vh_block, const char* blockName, LFContext *context );
  
  //! Populate variable declaration to NUNet. Generate a unique name with prefix, if present.
  ErrorCode var(vhNode vh_var, LFContext* context, NUNet **the_net,
                const char* prefix = NULL, bool isDeclaredVar = true);

  //! Populate a file declaration.
  ErrorCode file(vhNode vh_decl, LFContext* context);

  //! Create dummy signal corresponding to alias object and then assign
  //! the value of the actual object into this dummy signal. This is required
  //! inorder to make the alias object visible to the user.
  ErrorCode createAndAssignSignalForAliasObject(vhNode alias, 
                                                LFContext *context);

  //! Method to create a declaration scope for package. packIgnored is set to false
  //! if package has already been seen and populated.
  ErrorCode package(vhNode vh_pack_decl, LFContext *context,
                    const char* libName, const char* packName, bool* packIgnored);

  //! Method to populate nucleus with the given c-model corresponding to a
  //! dummy/blackbox entity/architecture.
  /*!
    \param vh_entity The Jaguar node corresponding to the entity
    \param modName The name of the module 
    \param module The module 
    \param context The current localflow context
   */
  ErrorCode cModule( vhNode vh_entity, const char* modName, NUModule* module, 
                     LFContext* context );

  /*! \brief Populate a process statement in the traditional form to a NUAlwaysBlock
   *
   * These routines expect the vh_process to represent VHDL in the
   * canonical form.  Results are not guaranteed if this is not the
   * case. The population is a two step process. In first step ..pre-populate,
   * the always block and the block for it's body are created and the declaration
   * scope for the process is created. In the second step, the statements are
   * populated in the body block and the block is added to process along with
   * edge expressions.
   *
   */
  void prePopulateProcessStmt(vhNode vh_process, StringAtom* procDeclScope,
                              LFContext* context);
  ErrorCode postPopulateProcessStmt(vhNode vh_process, LFContext *context, 
                                    NUAlwaysBlock **the_process);

  //! Populate conc procedural call to NUAlwaysBlock with a NUTask within.
  ErrorCode concProcedureCall(vhNode vh_conc_proc_call, 
                              LFContext *context);

  /*! \brief Accelerate certain Synopsys GTECH components.
   *
   * Why bother running the simulation models for things we can do
   * faster and easier?  We map certain GTECh components to simpler
   * constructs, like mapping a buffer to a continuous assign.
   * \param [in] vh_conc_stmt The statement to try to accelerate
   * \param [in] context The LFContext
   * \returns eNotApplicable if it's not acceleratable, and the result of the acceleration population if it is.
   */
  ErrorCode accelerateGtechComponent( vhNode vh_conc_stmt, LFContext *context );
  //! Push the instance name onto the LFContext stack.
  /*!
    \param vh_inst Jaguar node corresponding to a instance.
    \param context   The local flow context
  */
  StringAtom* getInstanceName(vhNode vh_inst, LFContext *context);
  
  //! PreProcess the component instantiation 
  /*!
    This populates the NUModuleInstance. This gets called after the
    master entity has been populated/visited.
    
    \param vh_inst Jaguar node corresponding to a instance.
    \param n_entity_decl Master Jaguar node of instance
    \param old_entity_decl old Master Jaguar node of instance (in case of a substitution)
  */
  ErrorCode componentInstancePost( vhNode vh_inst,
                                   vhNode n_entity_decl,
                                   vhNode old_entity_decl,
                                   LFContext *context);

  //! Called if there is an empty architecture
  ErrorCode emptyArchitecture(vhNode vh_arch);

  //! Uniquify the module name based on the generic values
  ErrorCode uniquifyEntityName(vhNode vh_entity, UtString &uniquifiedModName);

  //! Member to get value(string or integer) of an expression.
  ErrorCode getVhExprValue(vhExpr vh_expr, int* the_value, UtString* str_val);

  //! Given a for-generate, calculate the block name
  /*!
    \param forgen The for-generate block
    \param vhIndexExpr The for index expression
    \param context The LFContext
    \param name a stringAtom ptr that is the name of the block
    \returns eSuccess if a name was returned, eFailNoCreate if there was a problem
  */
  ErrorCode calcForGenerateBlockName(vhNode forgen, 
                                     vhExpr vhIndexExpr,
                                     LFContext* context,
                                     StringAtom** name);

  //! Checks for attribute expression in the if/else clock block
  ErrorCode mustNotBeAttribute(NUExpr *cond_expr, LFContext *context, SourceLocator *loc);



  //! Builds a list of actuals passed to arguments of functions/procedures
  //! identifying default arguments and replacing missing actuals with defaults.
  bool getFuncProcActualArgList(vhNode func_or_proc_call,
                                JaguarList* actual_arg_list, UtList<vhNode>* output_actual_list);

  //!
  //! Find a substitute ENTITY if one exists.
  static mvvNode substituteEntity( mvvNode oldMod,
				   mvvLanguageType oldModLang,
				   bool isUDP,
				   UtString& new_mod_name,
				   TicProtectedNameManager* ticProtectedNameMgr);

  TicProtectedNameManager* getTicProtectedNameManager() { return mTicProtectedNameMgr; }

private:
  //! true if using old style of names for named generate blocks within modules
  bool mUseLegacyGenerateHierarchyNames;

  //! Support class
  TicProtectedNameManager* mTicProtectedNameMgr;

  //! Hide copy and assign constructors.
  VhdlPopulate(const VhdlPopulate&);
  VhdlPopulate& operator=(const VhdlPopulate&);

  //! Is this statement some kind of loop (e.g. for or while)?
  static bool isLoop(vhNode node);

  //! Detects if the expressions resized by this function. 
  static NUExpr* resizeIntegerRvalue(NUExpr* origExpr, bool isSigned);

  //! Maintain map of Jaguar package decl node to Nucleus scope
  void mapPackage(StringAtom *packName, NUNamedDeclarationScope *package, LFContext* context);
  void lookupPackage(StringAtom *packName, NUNamedDeclarationScope **the_package, LFContext* context) const;

  //! Map the blocks created for exiting out of a loop or returning from
  //! a function.  
  void mapBlock(vhNode vh_scope, NUBlock *block);

  //! Map the blocks created for 'next' inside a looop
  void mapNextBlock(vhNode vh_scope, NUBlock *block);

  void unmapBlock(vhNode vh_scope, NUBlock *block);
  void unmapNextBlock(vhNode vh_scope, NUBlock *block);

  //! Maintain map of subprogram signature to Nucleus task/function
  ErrorCode mapTask(NUModule *module, StringAtom *signature, NUTask *task);
  ErrorCode lookupTask(NUModule *module, StringAtom *signature, NUTask **the_task);

  void mapTaskConstantReturn(NUTask* task, NUConst* constExpr);
  NUConst* lookupConstantReturnForTask(NUTask* task);

  ErrorCode processVhdlModule( veNode ve_inst,
                               vhNode primaryUnit,
                               const UInt32 *offset,
                               LFContext *context );

  ErrorCode getIndexExprs(UtVector<vhExpr>& dimVector, const SourceLocator& loc,
                          int lWidth, LFContext* context, bool* was_truncated,
                          NUExprVector* idxVec);


public:
  //! Process the instance port connections
  ErrorCode portConnection(vhExpr vh_actual, LFContext *context, NUNet* formal,
                           NUPortConnection **the_conn);
  //! Wrapper function on the jaguar API vhIsAliasObject() . This function will
  //! take any jaguar node as input and return true if it is alias or if
  //! it is created out of an alias object, false otherwise.
  static bool isAliasObject(vhNode vh_obj);
  //! A wrapper function on the jaguar API vhEvaluateExpr() . This function 
  //! takes a jaguar expression node and evaluates that expression in a
  //! customised manner depending on the input flags returnNullForNonStaticExpr
  //! and staticnessCheckRequired .
  /*!
    \param vh_expr The jaguar expression node which has to evaluate
    \param returnNullForNonStaticExpr Flag to indicate if it should return NULL for the non-static input expression. Default value is false.
    \param staticnessCheckRequired Flag to indicate if staticness checking is required. Default value is false.
    \returns vhExpr The evaluated expression or the given input expresion if the expression evaluation fails. It will return NULL if the given expression is not static and the flag returnNullForNonStaticExpr is true.
   */
  static vhExpr evaluateJaguarExpr(vhExpr vh_expr, bool returnNullForNonStaticExpr = false, bool staticnessCheckRequired = false);
  /*! \brief Try to evaluate a Jaguar expression by populating Nucleus for it and then trying to fold it to a constant
    \param [in] vh_expr the expression to try to evaluate
    \param [out] value the integer value the expression evaluates to
    \param [in] context the LFContext
    \param [in] reportError Set this to false to avoid printing error messages
    \returns eSuccess when an integer value is reached
   */
  ErrorCode evaluateStaticExpr( vhExpr vh_expr, SInt32 *value, LFContext *context, bool reportError = true );

  //! Method to decompile a jaguar node into string to print that
  //! in the log message.
  static const char* decompileJaguarNode(vhNode jag_node, UtString* buf);
  //! Method to get the accelerated function type if it is falls into
  //! the accelerated function category. A function will fall in this
  //! category if the body of this function contains metacomment directive
  //! to builtin/mapped operator/function or feedthru.
  vhBuiltInFuncOrMappedOperatorType getBuiltInFuncOrMappedOperatorType(vhNode funcBody); 

  /*! \brief Extract the record typedef and potentially the array range
     for a net 

     This method takes as input a vhNode corresponding to a net of
     record type and returns two values: the typeDef for the type of the
     underlying record, and the other any array constraint that exists
     for the net.  It requires that isRecordNet() be true for the input net;
     this will likely crash otherwise.  If there is no arraying on the type,
     the constraint parameter returns NULL.
     \param[in] net The vhNode for the net to be processed
     \param[out] vh_typedef The vhNode for the record type
     \param[out] constraintVector The VhNodeArray for the set of array constraints on the net
   */
  static void getVhdlRecordType( vhNode net, vhNode *vh_typedef,
                                 VhNodeArray *constraintArray = NULL );
  //! Get the size that Jaguar thinks the record is.
  UInt32 getVhdlRecordSize( vhNode vh_record, LFContext *context );

  //! Process the unconnected instance ports
  ErrorCode unconnectedPort(mvvNode mvv_inst, NUNet *formal, LFContext *, 
                            NUPortConnection **the_conn);

  //! helper method to create sized SXT or ZXT nodes
  static NUExpr* buildMask (NUExpr* expr, UInt32 sz, bool signExtend = false);

  vhNode getVhIntegerType();

  //! Method to get the VH_RANGE of a jaguar object.
  /*!
    \param vh_obj The jaguar object whose range has to be retrieved.
    \param vh_range1 The reference to vhNode where to put the retrieved first VH_RANGE
    \param vh_range2 The reference to vhNode where to put the retrieved second VH_RANGE if any.
    \returns ErrorCode The status error code
   */
  ErrorCode getVhRangeNode( vhNode vh_obj, vhNode* vh_range1, vhNode *vh_range2);

  //! Returns true if the given expression operator is a overloaded with
  //! the user-defined operator function.
  static bool isOperatorOverloadedByUser(vhExpr vh_expr);

private:

  //! Returns the unique name for the given sequential block's declaration scope.
  StringAtom* getSeqBlockDeclScopeName(vhNode vh_block, LFContext* context);

  //! helper method for integer subrange recognition
  vhNode evaluateIntegerSubtype (vhNode node, VhdlRangeInfo*, LFContext*);

  //! helper to calculate signed net flags
  /*!
    This returns vector net flags representing the signedness of the
    net's range, if applicable.  It also updates the signed bit in the
    general net flags.
   */
  VectorNetFlags calcSignedNetFlags (vhNode node, NetFlags* flags, LFContext *context, VhdlRangeInfo* copyRange = NULL);

  //! determine mode of parameter
  static NUTF::CallByMode getParamMode (bool isSignal);

  //! Helper method for common assignment code
  ErrorCode helperAssign( NULvalue *the_lvalue, NUExpr *the_rvalue,
                          assignType assign_type, LFContext *context,
                          const SourceLocator &loc, bool &assignCreated );
  //! Populate concurrent assignment to NUContAssign
  ErrorCode concurrentSignalAssign(vhNode vh_net, LFContext* context, 
                                   NUContAssign **the_assign);
  /*! \brief Detect and flag a pullup or pulldown
   *
   * Detecting a pullup or pulldown will cause the driven net to be
   * annotated with either the pullup or pulldown flag.  This routine
   * does nothing if a pull is not detected.  A pullup or pulldown is a
   * concurrent assignment that drives a constant pattern of all "H" or
   * all "L" onto a complete net.
   *
   * \param[in] vh_lvalue The target of the assignment
   * \param[in] vh_rvalue The value being assigned
   * \param[in] context The LFContext
   * \returns ErrorCode eNotApplicable is returned if there is no pull
   */
  ErrorCode pull( vhNode vh_lvalue, vhExpr vh_rvalue, LFContext *context );
  //! Populate variable assignment to NUBlockingAssign
  ErrorCode variableAssign(vhNode vh_net, LFContext* context, 
                           NUBlockingAssign **the_assign);
  //! Populate signal assignment to NUNonBlockingAssign
  ErrorCode signalAssign(vhNode vh_net, LFContext* context, 
                         NUNonBlockingAssign **the_assign);
  /*! \brief Populate Memory signal assignment by an aggregate as rvalue 

  This method will create an assignment for each word of the memory
  \warning CAUTION: This routine will DELETE the concat argument, as the full concat is not actually used here or afterwards.
  \param[in] assign_type What cort of assignment to create
  \param[in] context The LFContext
  \param[in] mem The memory to be assigned to
  \param[in] lvalue The lvalue from the assign statement
  \param[in] concat The concat to be assigned to the memory
  \param[in] loc The source locator for the original assignment
   */
  ErrorCode memoryAssignByAggregate(assignType assign_type, LFContext* context,
                                    NUMemoryNet* mem, NULvalue* lvalue,
                                    NUConcatOp* concat, const SourceLocator& loc);
  /*! \brief Populate Memory slice assignment by a whole memory as rvalue 

  This method will create an assignment for each element of the memory
  slice in the lvalue from the whole memory given as the lvalue.
  \warning CAUTION: This routine will DELETE the concat argument, as the full concat is not actually used here or afterwards.
  \param[in] assign_type What cort of assignment to create
  \param[in] context The LFContext
  \param[in] concat The concat whose elements are to be assigned to
  \param[in] rvalue The rvalue from the assign statement
  \param[in] mem The memory to be assigned from
  \param[in] loc The source locator for the original assignment
   */
  ErrorCode memorySliceAssign(assignType assign_type, LFContext* context,
                              NUConcatLvalue* concat, NUExpr* rvalue, NUMemoryNet* mem,
                              const SourceLocator& loc);
  ErrorCode identLvalue(vhNode vh_named, LFContext* context, 
                        NULvalue **the_lvalue, const bool isAliasDecl);
  ErrorCode lvalue(vhNode vh_lvalue, LFContext* context, 
                   NULvalue **the_lvalue, bool isAliasDecl = false);
  //! Top-level function to process the indexed name lvalues 
  ErrorCode indexedNameLvalue( vhNode vh_lvalue, LFContext* context,
                               NULvalue **the_lvalue );
  /*! \brief Helper for indexed name lvalue population
   * \sa indexedNameLvalue
   */
  ErrorCode indexedNameLvalueHelper( NUNet *net, NUExpr *bitsel_expr,
                                     LFContext *context, const SourceLocator &loc,
                                     NULvalue **the_lvalue );
  /*! \brief Method that does the work in populating indexed name lvalues
   * \sa indexedNameLvalue
   */
  ErrorCode indexedNameLvalueHelper( NUNet *net, NUExprVector *indexVector,
                                     LFContext *context, const SourceLocator &loc,
                                     NULvalue **the_lvalue );
  //! Function to process  slice name lvalues
  ErrorCode sliceNameLvalue(vhNode vh_lvalue, LFContext *context, NULvalue **the_lvalue);
  //! Function to process  aggregate lvalues
  ErrorCode aggregateLvalue(vhExpr vh_lvalue, LFContext *context, 
                            NUConcatLvalue **the_lvalue);
  ErrorCode netFlags(vhNode vh_net, StringAtom* name, 
                     LFContext *context, NetFlags *flags);
  ErrorCode netRange( vhNode vh_net, ConstantRangeArray *rangeArray, LFContext *context);

  /*! \brief Extract the Jaguar record indices for a net reference

  This method takes a record field vhNode and the indexVector as input.
  For each indexed name in the selected name list, the indices are
  prepended into indexVector.  This method requires that isRecordNet() be
  true for the input net; this will likely crash otherwise.
  \param[in] vh_named the record expression
  \param[inout] indexVector the vector of array indices
  \param[in] context The LFContext
   */
  ErrorCode getVhdlRecordIndicies( vhExpr vh_named, NUExprVector *indexVector,
                                   LFContext *context );

  //! Allocate a NUBitNet, NUVectorNet, or NUMemoryNet, depending on node
  /*!  This method gathers information and calls the other signature of
    allocNet, which does the heavy lifting.
    \param[in] node The Jaguar node to create a NUNet for
    \param[in] name The name of the net to create
    \param[in] flags The flags to decorate the NUNet with
    \param[in] declaration_scope The scope to declare this net in
    \param[in] context the LFContext
    \param[out] the_net Parameter to return the net in
    \param[in] recConstraintArray Input constraints from a record array passed down to its elements; NULL otherwise
     \returns An ErrorCode
   */
  ErrorCode allocNet( vhNode node, StringAtom *name, NetFlags flags,
                      NUScope *declaration_scope, NUNet **the_net,
                      LFContext *context, UtArray<vhNode>* recConstraintArray = NULL );


  //! Allocate a net with the supplied name and bounds
  /*!
    This signature of allocNet does all the heavy lifting for creating nets.
    \param[in] name The name of the net to create
    \param[in] flags The flags to decorate the NUNet with
    \param[in] vflags Special flags for vector nets
    \param[in] dimArray A vector of dimensions for the net
    \param[in] declaration_scope The scope to declare this net in
    \param[in] loc The SourceLocator for this net
    \param[out] the_net Parameter to return the net in
    \param[in] context the LFContext
    \param[in] recConstraintArray Input constraints from a record array passed down to its elements; NULL otherwise
     \returns An ErrorCode
   */
  ErrorCode allocNet( StringAtom *name, NetFlags flags, VectorNetFlags vflags,
                      ConstantRangeArray *dimArray, NUScope *declaration_scope,
                      const SourceLocator &loc, NUNet **the_net,
                      LFContext *context, UtArray<vhNode>* recConstraintArray = NULL );


  //! Clean up various data structures upon exit from processStmt
  void processCleanup( LFContext *context );

  //! Create declaration scope for given block and push it into the context.
  void createDeclarationScope(vhNode vh_block, StringAtom* declScope,
                              LFContext* context, const SourceLocator* loc);
  //! Helper function for processes and concProcedureStmts
  void sequentialBlock(vhNode vh_block, StringAtom* declScope, LFContext *context, 
                       const SourceLocator *loc, NUBlock **the_block);
  //! populate the supplied suquential statement list into the current context
  ErrorCode sequentialStmtList( JaguarList *stmt_list, LFContext *context, bool is_wait_clock = false );

  //! rearange the list of sequential statements for processes with wait clocks.
  ErrorCode rearangeWaitClock(JaguarList *vh_seq_stmts, JaguarList *vh_new_list);


  //! Is the current scope an unrolled loop scope?
  bool isUnrolledLoopScope() const;
  //! Should the specified loop be unrolled?
  ErrorCode analyzeLoopForUnroll(vhNode vh_loop, ConstantRange& cr,
                                 vhDirType* direction, LFContext* context,
                                 bool* shouldUnrollLoop);
  //! Unroll and populate the for loop statements.
  ErrorCode unrollLoop(vhNode vh_loop, LFContext* context, bool* loopIsUnrollable,
                       NUStmt **the_stmt);

  /*! \brief Ensure the clocking constructs used are supported
   * cbuild can only support certain constructions of clocks and resets.
   * Each process statement is validated to conform with our support in
   * this method.
   * \param [in] vh_process The process statement's Jaguar node
   * \param [in] vh_clock_stmt The sequential statement to validate
   * \param [in] vh_clock The Jaguar clock node for this process
   * \returns The ErrorCode for the method
   * \sa clockAndResets, elaborateSeqStmtList
   */
  ErrorCode validateClock( vhNode vh_process, vhNode vh_clock_stmt, vhNode vh_clock );
  /*! \brief Perform static elaboration of a sequential statement list.
   *
   * Certain process bodies need to be statically elaborated before they
   * can have their clocks successfully validated.  This method detects
   * such processes and performs a static elaboration at the Jaguar
   * level by choosing the appropriate branch of the if statement, and
   * returning a statement list based on that list.  The elaborated
   * statement list is returned in elab_stmts.  If static elaboration is
   * not needed, a copy of the original list is returned.
   *
   * \param [in] orig_stmts The original statement list
   * \param [out] elab_stmts Output list containing the statically elaborated statements.  This list must be empty upon entering the method.
   * \returns true if elab_stmts is different from orig_stmts, false if not
   * \sa validateClock, clockAndResets
   */
  bool elaborateSeqStmtList( const JaguarList * const orig_stmts,
                             JaguarList *elab_stmts, const SourceLocator& loc );
  /*! \brief Find the reset nets/expressions for the process
   *
   * Walk through the process statement's sequential statements, extract
   * any resets found, and create edge expressions for them.  Any
   * complex resets will have intermediate nets created for them. This
   * method presumes the process statement has had its clocking
   * structure validated.
   * \param [in] seq_stmts The elaborated sequential statements for the process
   * \param [in] context The LFContext
   * \param [in] loc The SourceLocator for the process
   * \returns The ErrorCode for the method
   * \sa clockAndResets, ComplexResetHelper, validateClock
   */
  ErrorCode detectResets( JaguarList *seq_stmts, const SourceLocator &loc, LFContext *context );
  /*! \brief If a process uses a zero or more trailing resets, process its clock and reset(s).
   * 
   * Detect if the process is of the form using zero or more trailing resets.  This
   * is a process with multiple unconnected if statements.  The first if
   * condition must be a clock expression, with no elsif or else
   * clauses.  If there's one subsequent if statement, it defines a
   * priority-arranged series of async reset conditions.  If there are
   * multiple subsequent if statements, each statement defines a single
   * if condition; no elsif or else clauses are supported.  The ordering
   * of the reset condition if statements defines their priority.
   *
   * \param [in] vh_process The vhNode for the process
   * \param [in] seq_stmts The elaborated sequential statement list
   * \param [in] modified A boolean flag indicating if seq_stmts has been elaborated from the original list in vh_process
   * \param [in] context The LFContext
   * \param [out] has_trailing_reset A boolean output indicating trailing reset if-statements were found
   * \returns eNotApplicable if not of this form, eSuccess if of this form, an
   * appropriate condition otherwise
   *  \sa clockAndResets
   */
  ErrorCode clockWithMaybeTrailingReset( vhNode vh_process, JaguarList *seq_stmts,
                                         bool modified, LFContext *context,
                                         bool* has_trailing_reset );
  /*! \brief Process clock and reset(s) corresponding to a process statement
   * \sa validateClockConstruct, createClockExpression, detectResets, clockWithMaybeTrailingReset
   */
  ErrorCode clockAndResets(vhNode vh_process, JaguarList *seq_stmts, 
                           bool elaborated, LFContext *context, bool is_wait_clock);
  /*! \brief find the clock for a process and return the sequential stmt list to populate
   */
  ErrorCode findClock( vhNode vh_process, vhNode *vh_clock, ClockEdge *edge, 
                       JaguarList *seq_stmts, bool elaborated );
  /*! \brief Provide a wrapper around vhDetectClock
   *
   * Jaguar cannot detect a clock in a complex expression that we can
   * support, like ((clk'EVENT and clk='1') AND en='1').  This method
   * allows us to dissect the condition and locate the clock.  The
   * logical operand may only be an AND; any other operand will error
   * out.
   */
  vhNode detectClock( vhExpr vh_expr, int *vh_edge );
  /*! Locate a clock in this if statement, if it exists.  Return NULL and
   * vh_edge == -1 if there is no clock.  This does not handle nested if
   * statements, other than one nested as the first statement in the else
   * clause.
   */
  vhNode getIfStmtClock( vhNode vh_stmt, int *vh_edge );
  /*! Locate a clock in a wait statement, if it exists. Return NULL and
   * vh_edge == -1 if there is no clock. The wait statement must be 
   * the first statement in the process and there cannot be any other
   * wait statements in the process as well.
   */
  vhNode getWaitStmtClock( vhNode vh_process, JaguarList *seq_stmts, int *vh_edge );
  /*! This is a wrapper around getIfStmtClock that simply returns a
   *  boolean indicating if there is a clock present or not in the
   *  statement.
   * \sa ifStmtContainsClock
   */
  bool ifStmtContainsClock( vhNode vh_stmt );


  /*! This is used in maybeNormalizeClockStmt() to validate the reset.
   *  It returns true if second_rst == !first_rst
   */
  bool validateCustomReset( vhExpr first_if_conf, vhExpr second_if_conf );

  /*! \brief Normalize a single-element sequential statement list, if necessary.
   *
   * This routine accepts a Jaguar statement list with the form of the
   * example on the left and transforms it into one with the form of the
   * right example.
   *
   * \verbatim
   * if cond then               if not cond then
   *   if <clkedge> then          <reset stmts>
   *     <clock stmts>              else
   *   endif;                     if <clkedge> then
   * else                           <clock stmts>
   *   <reset stmts>              endif;
   * endif;                     endif;
   * \endverbatim
   *
   * The crucial difference here is that we cannot detect the clock in the
   * left example, but can detect the clock in the example on the right.
   * The current limitation is limited to only the form shown above.  For
   * example, if there are any statements preceding the if statement the
   * normalization will not occur.  Both the reset and clock statement
   * lists may have zero or more statements in them.
   */
  bool maybeNormalizeClockStmt( JaguarList *seq_stmts );
  ErrorCode maybeReplaceClockNet(NUExpr *cond_expr, LFContext *context);
  /*! \brief Create an edge expression for the supplied clock and add it to the context
   */
  ErrorCode createClockEdgeExpr( vhNode vh_clock, ClockEdge edge, 
                                 const SourceLocator &loc, LFContext *context );
  /*! Generate an NUExpr for if_cond, detect the edge of the reset
   *  expression, and add it into the edge sensitivity list.
   */
  ErrorCode resetEdge(vhExpr vh_cond, LFContext *context);
  //! Detect the edge of the reset expression and add it into the edge sensitivity list.
  ErrorCode resetEdge(vhExpr vh_cond, NUExpr *cond_expr, LFContext *context);
  /*! \brief processes non-trivial reset conditions
   *
   * This method will handle any reset conditions that are not simple
   * variations on \<net\> = \<level\>.  This includes, but is not limited
   * to, multiple nets in the reset expression and vector nets (whether
   * multiple or not) in the reset expression.  A properly handled reset
   * expression will be placed into the context's edge expression set.
   * \param [in] vh_cond The original Jaguar expression node
   * \param [in] cond_expr The NUExpr for the reset condition
   * \param [in] context The LFContext
   * \returns An ErrorCode indicating if the reset was created and
   *          populated into the context's edge expression list
   */
  ErrorCode ComplexResetHelper( vhNode vh_cond, NUExpr *cond_expr,
                                LFContext* context );
  //! Given the vh_stmt ( maybe compound or simple ) recursively check for 
  // any wait statement within, returns the first found wait stmt vhNode.
  vhNode hasWaitStmt(vhNode vh_stmt);
  //! Validate if a process uses a wait-based clock.
  ErrorCode validateWaitClock( vhNode vh_process, JaguarList *seq_stmts );
  //! helper for creating reset expressions
  ErrorCode resetExprHelper( vhExpr if_cond, LFContext *context,
                             NUExpr **the_cond_expr );
  //! Populate condition expression to NUExpr
  ErrorCode condExpr(vhExpr vh_cond, LFContext *context, NUExpr** cond);
  //! Create edge expression from the NUExpr and ClockEdge
  ErrorCode edgeExpr(NUExpr *clkExpr,ClockEdge edge,LFContext *context,
                     NUEdgeExpr **the_edge_expr);
  ErrorCode maybeFixConditionStmt(NUExpr** cond, const SourceLocator& loc);

  //! Process IF/ELSIF/ELSE sequential statements
  ErrorCode ifStmt(vhNode vh_if, LFContext *context, NUBlockStmt** the_if);

  enum branchToTake{eTakeThenOnly = 0, eTakeElseOnly = 1, eTakeBoth = 2 };
  //! This function gets called by ifStmt.
  //! It takes vh_cond_exp as input, checks it for staticness and
  //! returns optimized *cond_expr_rtn expression with additional 
  //! information br_to_take. If it is eTakeThenOnly or eTakeElseOnly,
  //! then the condition is static. Otherwise, it's not.
  ErrorCode getIfCondExpr(LFContext *context, vhExpr vh_cond_exp, 
                          NUExpr **cond_expr_rtn,  branchToTake *br_to_take);

  //! This function gets called by ifStmt for if/then, elseif and else parts
  //! of the if vhdl constracts. For if/then and elseif the vh_cond_exp is condition 
  //! expresion and vh_seq_stmt_list is the list of expression in the block.
  //! Depending on staticness of br_populated it populates either NUIf or NUBlock
  //! statement. If *the_if is not NULL, the new statements will be appended to else part
  //! of it.
  //! It takes vh_cond_exp, vh_seq_stmt_list,  *the_if, br_populated as input and returns 
  //! *the_if and br_populated.
  ErrorCode ifStmtHelper(LFContext *context, vhExpr vh_cond_exp, JaguarList* vh_seq_stmt_list,
                         NUBlockStmt** the_if, branchToTake* br_populated);



  //! Populate conc procedural call to NUAlwaysBlock with a NUTask within.
  ErrorCode concProcedureCall(vhNode vh_conc_proc_call, 
                              LFContext *context, NUAlwaysBlock **the_always);

  //! Populate return statement to NUBlockingAssign
  ErrorCode returnStmt(vhNode vh_return, LFContext *context, 
                       NUBlockingAssign **the_return);
  //! Populate FOR/WHILE/LOOP statement to NUFor
  ErrorCode loopStmt(vhNode vh_for, LFContext *context, NUNetSet& loopDefs,
                     NUStmt **the_stmt);
  ErrorCode createForStmtIndex(vhNode vh_for_index, LFContext* context,
                               NUNet** for_index_net);
  ErrorCode createForStmtControlObjects(vhNode loop_param_spec,
              LFContext *context, 
              NUExpr **the_condition, NUIdentLvalue** the_lvalue,
              NUExpr** the_advance_expr);
  //! Process an 'EXIT' stmt. As of now only for loop exit of the
  //! local loop is supported. In cbuild, we will create an internal
  //! net which will be assigned TRUE before the execution of the loop.
  //! Inside the body of the loop, when we hit an exit stmt, we assign FALSE
  //! to the cbuild-internal-net corresponding to the loop containing the EXIT.
  /*!
    \param vh_exit The jaguar node corresponding to the exit statement.
    \param context The local flow context.
    \param the_exit The assignment statement which has to be created for the vhdl EXIT statement. This assignment is like : internal_exit_net = FALSE . If it is a conditional exit stmt then we will create a stmt which will look like: if (EXIT_COND) internal_exit_net = FALSE .
   */
  ErrorCode exitStmt(vhNode vh_exit, LFContext *context, NUStmt **the_exit);

  //! Populate case statement to NUCase
  ErrorCode caseStmt(vhNode vh_case, LFContext *context, NUCase **the_case);
  //! Populate case-item statement to NUCaseItem
  ErrorCode caseItem(vhNode vh_case_item, LFContext* context,
                     bool* hasDefault, size_t maxSize, NUCaseItem **the_item,
                     NUExpr *caseSel);
  //! Populate sequential statements
  ErrorCode sequentialStmt(vhNode vh_stmt, LFContext *context);
  //! Populate procedure call to NUTaskEnable
  ErrorCode seqProcedureCall(vhNode vh_proc_call, LFContext *context);
  //! Method to check and populate file-io procedure calls.
  //! If the vhNode is not a file-io procedure call return eNotApplicable. 
  ErrorCode fileIOProcedureCall(vhNode vh_proc_call, LFContext *context);


  //! Method to process a concurrent statement.
  /*!
   \param vh_stmt Jaguar node corresponding to the concurrent statement.
   \param blk_cfg Jaguar node corresponding to current block which has been configured in the configuration declaration/specification. This will be NULL if the block is not configured. In other words it is the current block configuration (if any) which enables the elaboration of instances within the elaborated instance. The jaguar object type of this node is VHBLOCKCONFIG.
  */

  //! Create dummy always block to process the selected and the conditional 
  //! signal assignment statements.
  ErrorCode dummyAlwaysBlock(vhNode vh_sig_assgn, LFContext *context, 
                             NUAlwaysBlock **the_process);
  //! Process the conditional signal assignment statement
  ErrorCode conditionalSigAsgn(vhNode n_stm, LFContext *context, 
                               JaguarList& condWaveFrmList);
  //! Method to convert the waveform a conditional signal assignment into
  //! equivalent ternary expression.
  ErrorCode convertCondWaveFrmIntoTernaryExpr( LFContext *context, 
                                               JaguarList& condWaveFrmList,
                                               NUExpr** valueExpr,
                                               NUExpr** oredConds, 
                                               NUExpr** uncondRval,
                                               const bool drivesZ);
  //! Process the selected signal assignment statement
  ErrorCode selectedSigAsgn(vhNode n_stm, LFContext *context, 
                            JaguarList& selWaveFrmList, vhNode n_last_sel_wave, 
                            JaguarList& choiceList);
  //! Function that creates a NU bitwise equal expression with the select 
  //! and choice expression of a VHDL case alternative. 
  ErrorCode caseAlternativeCond(vhExpr n_select, vhExpr n_choice, 
                                LFContext *context, NUExpr** cond_expr);
  ErrorCode constantRange(vhNode vh_range, LFContext * context, 
                          ConstantRange **the_range);
  //! Populate constants to NUConst
  ErrorCode constant(vhExpr ve_expr, int lWidth, LFContext *context, 
                     NUExpr **the_const);
  //! Determine nucleus sign for binary expression
  ErrorCode chooseBinOp (vhExpr vh_expr, NUExpr* lop, NUExpr* rop, NUOp::OpT* op,
                         bool* invertExpr);

  /*! \brief Populate binary expressions.

   \param vh_expr  The Jaguar expression being converted
   \param context
   \param lWidth   (the current guess of) the width of the expression, or zero meaning unknown guess
   \param the_expr  pointer to the populated expression is placed here.
   \param propagateRvalue  if true then this routine is recursively called in an attempt to identify a constant value for the expression. Used for binary expressions that should be static, when finding initial values.
   \param  reportError Set this to false to avoid printing error messages
 */ 
  ErrorCode binaryExpr(vhExpr vh_expr, LFContext *context, int lWidth,
                       NUExpr **the_expr, bool propagateRvalue,
                       bool reportError);
  /*! \brief Adjust an array index it the index is of std_logic type
   *
   * When a std_[u]logic is used as an index of an array, we need to
   * adjust for the 9-valued enum that is std_ulogic.  We will populate
   * the index as an expression that returns a single bit, '0' or '1'.
   * However, since std_ulogic is a enum with values [UX01ZWLH-], we
   * really want to reference array indices 2 or 3.  So when an index
   * expression with an absolute base type of STD_ULOGIC is detected, we
   * add 2 to it to adjust it to fit the array supplied by the user.
   *
   * \param [in] index Jaguar node for he expression being processed
   * \param [inout] bitsel The nucleus expression to potentially transform
   * \param [in] loc the SourceLocator
   */
  void maybeAdjustIndex( vhNode vh_index, NUExpr **bitsel, const SourceLocator &loc );
  //! Top-level function to process the indexed name rvalues 
  ErrorCode indexedNameRvalue(vhExpr vh_expr, LFContext *context, 
                              int lWidth, NUExpr **the_expr);
  /*! \brief Helper for indexed name rvalue population
   * \sa indexedNameRvalue
   */
  ErrorCode indexedNameRvalueHelper( NUNet *net, NUExpr *bitsel_expr,
                                     const SourceLocator &loc, NUExpr **the_expr );
  /*! \brief Method that does the work in populating indexed name lvalues
   * \sa indexedNameLvalue
   */
  ErrorCode indexedNameRvalueHelper( NUNet *net, NUExprVector *indexVector,
                                     const SourceLocator &loc, NUExpr **the_expr );
  //! Method to populate slice name expressions in the Rvalue
  ErrorCode sliceNameRvalue(vhExpr vh_expr, LFContext *context, 
                            int lWidth, NUExpr **the_expr);
  //! Method to map bitselect index on an alias obj to actual obj's index
  /*!
    \param index The index expression of the indexed name alias object
    \param vh_bitsel The jaguar bit-select node
    \param alias bounds of this alias object
    \param actual The equivalent of alias on the actual object
    \param loc   The source code location
    \returns ErrorCode The status error code
   */
  ErrorCode resolveIndexAliasBitSel( NUExpr** index, vhNode vh_bitsel,
                                     VhdlRangeInfo& alias,
                                     VhdlRangeInfo& actual,
                                     const SourceLocator& loc );

  //! Method to resolve an alias object to get the actual NUNet and
  //! the translated left and the right bounds. 
  //! For the scalar object aliases, call this function with content of
  //! aliasSliceLeft and aliasSliceRight as 0. This function will put the
  //! actual bit index if the actual is indexed name in the content of
  //! actualSliceLeft and actualSliceRight .
  /*!
    \param vh_alias The jaguar alias node
    \param alias the bounds (MSB:LSB) of the jaguar alias node
    \param actual the translated (MSB:LSB) of the actual of this alias
    \param context The local flow context
    \param actualNet The NUNet corresponding to the original non-alias object .
    \returns ErrorCode The status error code
   */
  ErrorCode resolveAlias( vhNode vh_alias, 
                          VhdlRangeInfo *alias,
                          VhdlRangeInfo *actual,
                          LFContext *context, 
                          NUNet** actualNet );
  //! Method to create selected name with resolved actual object from a 
  //! alias object selected name.
  /*!
    \param alias_selname The selected name whose parent object is alias
    \returns The created selected name whose parent is resolved actual object
   */
  vhNode createActualObjSelNameFromAliasObjSelName(vhNode alias_selname);
  //! Method to populate aggregate name expressions 
  ErrorCode aggregate(vhExpr vh_expr, LFContext *context, int lWidth, NUExpr **the_expr);
  //! Map a multidimensional record array aggregate into an array of Jaguar nodes, ready for record assignment.
  ErrorCode recordArrayAggregate( vhExpr aggregate, const VhdlRangeInfoVector *dimVector,
                                  UtPtrArray &array, LFContext *context );

  //! Map a record array aggregate into an array of Jaguar nodes, ready for record assignment.
  /*! A record assignment creates a set of assigns in nucleus, one for
   * each record element.  When we associate individual elements of an
   * arrayed record through a port, Jaguar creates an array aggregate of
   * record aggregates for us.  We need to take this and create array
   * aggregates for each field in the record.  These aggregates get
   * placed in the UtPtrArray.
   \param[in] aggregate The Jaguar VHAGGREGATE node being expanded
   \param[in] arrayRange The array range of the aggregate
   \param[out] array The UtPtrArray to populate with NUExprs
   \param[in] context The LFContext
   \return ErrorCode The standard error status code
   \sa recordAssign, recordAggregate
   */
  ErrorCode recordArrayAggregate( vhExpr aggregate, const ConstantRange *arrayRange,
                                  UtPtrArray &array, LFContext *context );
  //! Map a record aggregate into an array of NUExprs or Jaguar nodes, ready for record assignment.
  /*! A record assignment creates a set of assigns in nucleus, one for
   * each record element.  This method takes an aggregate being
   * assigned to a record and creates a UtPtrArray of the aggregate
   * elements.  This is then used by recordAssign in creating each of
   * the individual record assignments.
   \param[in] aggregate The Jaguar VHAGGREGATE node being expanded
   \param[out] array The UtPtrArray to populate with NUExprs
   \param[in] context The LFContext
   \param[in] isLvalue is this an lvalue?
   \param[in] createNucleusObjs Boolean value indicating whether to create nucleus
   objects to put in the list or to put the Jaguar objects in the list
   \return ErrorCode The standard error status code
   \sa recordAssign, recordArrayAggregate
   */
  ErrorCode recordAggregate( vhNode aggregate, UtPtrArray &array,
                             LFContext *context, bool isLvalue = false,
                             bool createNucleusObjs = true );
  ErrorCode cleanupXZBinaryExpr(NUExpr *expr, int lWidth, 
                                const SourceLocator &loc, NUExpr **the_expr);
  //! Methods to create Binary String from character String.
  int composeBinStringFromString(const char* str, UtString* bin_str, bool replace_null_with_zero = false);
  void composeBinaryFromChar(UInt8 char_val, UtString *bin_str); 
  bool createFormatStringExpr(vhNode vh_node, const SourceLocator&, NUExpr **the_expr);

  /*! \brief Process a VHDL unary expression, creating a NUExpr
   *
   * \param [in] vh_expr  The Jaguar expression to be processed
   * \param [in] context The LFContext
   * \param [in] lWidth (the current guess of) the width of the expression, or zero meaning unknown guess
   * \param [in] propagateRvalue set this to true when the expression is expected to evaluate to a constant (static) value.  If true then extra effort is expended to determine the constant initial value.
   * \param [in] reportError Set this to false to avoid printing error messages
   * \param [out] the_expr pointer to NUExpr that is created

   */
  ErrorCode unaryExpr(vhExpr vh_expr, LFContext *context, int lWidth,
                      NUUnaryOp **the_expr, bool propagateRvalue,
                      bool reportError);

  /*! \brief Process a VHDL expression, creating a NUExpr
   *
   * \param [in] vh_expr  The Jaguar expression to be processed
   * \param [in] context The LFContext
   * \param [in] lWidth (the current guess of) the width of the expression, or zero meaning unknown guess
   * \param [in] propagateRvalue set this to true when the expression is expected to evaluate to a constant (static) value.  If true then extra effort is expended to determine the constant initial value.
   * \param [in] reportError Set this to false to avoid printing error messages
   * \param [out] the_expr pointer to NUExpr that is created

   */
  ErrorCode expr(vhExpr vh_expr, LFContext *context, 
                 int lWidth, NUExpr **the_expr,
                 bool propagateRvalue = false,
                 bool reportError = true);

  /*! \brief Create an expression for the constant node initializer
   *
   * \param [in] vh_named The Jaguar expression to be processed
   * \param [in] context 
   * \param [in] propagateRvalue set this to true when the expression is expected to evaluate to a constant (static) value. If true then extra effort is expended to determine the constant initial value.
   * \param [in] reportError Set this to false to avoid printing error messages
   * \param [out] the_expr pointer to NUExpr that is created
   */
  ErrorCode initialValueExpr(vhNode vh_named, LFContext* context,
                             NUExpr** the_expr, bool propagateRvalue,
                             bool reportError);

  /*! \brief Create an identifier expression for rvalue.
   *
   * \param [in] vh_named The Jaguar expression to be processed
   * \param [in] context 
   * \param [in] propagateRvalue set this to true when the expression is expected to evaluate to a constant (static) value. If true then extra effort is expended to determine the constant initial value.
   * \param [in] reportError Set this to false to avoid printing error messages
   * \param [out] the_expr pointer to NUExpr that is created

   */
  ErrorCode identRvalue(vhNode vh_named, LFContext *context, 
                        NUExpr **the_expr, bool propagateRvalue,
                        bool reportError);
  ErrorCode attribute( vhExpr vh_attribute, LFContext *context,
                       NUExpr **the_attribute );

  //! subprogram population routines.
  ErrorCode subProgCommon(NUTF* subp, vhNode vh_subp, LFContext* context);
  ErrorCode subprogram(vhNode vh_subp, LFContext *context, NUTF **the_subp);
  ErrorCode function(vhNode vh_func, vhExpr vh_func_call, StringAtom *name,
                     int returnWidth, vhExpr vh_size, LFContext *context,
                     const bool isRecursive, NUTask **the_task);
  ErrorCode procedure(vhNode vh_proc, StringAtom* name, LFContext *context, 
                                                           NUTask **the_proc);
  ErrorCode functionResult(vhNode vh_func, vhExpr vh_func_call, 
                           int returnWidth, vhExpr vh_size, LFContext *context,
                           NUNetVector *ret_vector);
  ErrorCode subProgArg(vhNode vh_arg, LFContext *context, NUNet **the_net);
  ErrorCode subProgLocal(vhNode vh_var, LFContext *context, NUNet **the_net);
  //! Function to internalize some of the possible library functions
  bool internalizeFuncCall(vhNode vh_function,vhExpr vh_func_call,
                           LFContext *context, int lhsWidth,
                           NUExpr **the_expr, ErrorCode *returnErrCode);
  /*! \brief Process a VHDL function call, returning a set of values as the result
   *
   * The reason we return a set of values is that a VHDL function may
   * return a record, which will likely have more than one field.
   * \param [in] vh_func_call The Jaguar function call node
   * \param [in] context The LFContext
   * \param [in] lhsWidth The context width of the function call result
   * \param [out] the_expr_array NUExprArray of return values
   * \sa functionResult
   */
  ErrorCode functionCall(vhExpr vh_func_call, LFContext *context, 
                         int lhsWidth, NUExprArray *the_expr_array);
  /*! \brief Transfer any user defined attributes between corresponding actual and formal ports
   * When a function recurses, the elaboration step creates new formal ports. If the actual port
   * has any user defined attributes associated with it, they should be passed on to the
   * corresponding formal.
   \param [in] vh_func_call The function call node
   \param [in] elaboratedFunctionBody The function body after it has been elaborated
   */
  ErrorCode transferUserDefinedAttributes(vhExpr vh_func_call,
					  vhNode elaboratedFunctionBody);
  /*! \brief Create a vector of argument associations for a subprogram.
   * This method accepts a Jaguar list of actual arguments for a
   * subprogram call and the NUTask created for the subprogram.  It
   * returns a NUTFArgConnectionVector, which contains the full
   * formal->actual association for a particular subprogram call.
   \param[in] vh_arg_iter The actual arg list from the function/procedure call
   \param[in] task The populated task; used to get the formal arg list
   \param[in] num_func_outputs The number of function output args to skip over
              in the formal arg list
   \param[in] context The LFContext
   \param[out] arg_conns The set of associations for the subprogram args, not
               counting any function output parameters.  The are handled separately
               in functionCall.
   \sa functionCall, seqProcedureCall
   */
  ErrorCode procArgConnections(JaguarList &vh_arg_iter, NUTask *task,
                               UInt32 num_func_outputs, LFContext *context, 
                               NUTFArgConnectionVector *arg_conns);
  /*! \brief Create an argument association for a single subprogram argument.
   */
  ErrorCode subProgArgConnection(vhNode vh_arg, NUNet *formal_net, 
                                 NUTF *tf, LFContext *context, 
                                 NUTFArgConnection **the_conn);
  //! Functions to support 'LINE' type variable
  static bool isLineVariable(vhNode vh_var);
  //! Create the entirly bogus $fopen that seems to be necessary
  ErrorCode initializeLineVar(LFContext *context, NUVectorNet *the_net,
                              const SourceLocator &loc);

  //! Function to process all the declaration in the scope.
  
  ErrorCode processDeclarations(vhNode vh_block, LFContext *context);
  //! Adds Statements for signal/variable declarations with initial values.
  ErrorCode initializeDeclaration(vhExpr vh_init, LFContext *context,
                                  NUNet *net);
  //! This function processes all concurrent statements inside the given block.
  /*!
   \param n_stm Jaguar node corresponding to the generate statement.
   \param blk_cfg Jaguar node corresponding to the block which has been configured via configuration declaration/specification. This will be NULL if the block is not configured. In other words it is the current block configuration (if any) which enables the elaboration of instances within the elaborated instance. The jaguar object type of this node is VHBLOCKCONFIG.
  */

  // Utility members on the top of the VHDL parser APIs

  /*! \brief Method to get width of an expression.
    This method is a wrapper over getVhExprRange().  It is to be used
    only at places where all we care about is the outermost dimension of
    the object.  THe common case for this is when there is only one
    dimension.
  */
  ErrorCode getVhExprWidth(vhExpr vh_expr, int* the_width,
                           const SourceLocator &loc,
                           LFContext *context,
                           bool isLval = false,
                           bool reportError = true);

  /*! \brief Method to get the total bitsize of an expression.
  This function is an wrapper over getVhExprRange() and is similar to
  getVhExprWidth().  The difference is that it will return the total bit
  size of the object, not simply the outermost dimension.
  */
  ErrorCode getVhExprBitSize( vhExpr vh_expr, int* the_size,
                              int *num_dims,
                              const SourceLocator &loc,
                              LFContext *context,
                              bool isLval = false,
                              bool reportError = true );

  //! Member to get the dimensional information of a Jaguar expression.
  /*!
   \param [in] vh_expr The Jaguar expression
   \param [out] dimVector A vector of VhdlRangeInfo containing all dimension info.
   \param [in] loc The VHDL source code location
   \param [in] isIndexConstraint Flag which is true if the given jaguar node corresponds to index constraint of an array object.
   \param [in] isLvalue is the object an lvalue?
   \param [in] reportError Boolean that can disable error reporting from this method.
  */
  ErrorCode getVhExprRange(vhExpr vh_expr,
                           VhdlRangeInfoVector &dimVector,
                           const SourceLocator &loc, 
                           bool isIndexConstraint, 
                           bool isLvalue,
                           bool reportError,
                           LFContext *context );

  //! Compares two return statements. If the current return stmt
  //! is larger than the largestReturnStmt, then the current return
  //! stmt becomes the largest return stmt. If the return stmt size
  //! cannot be determine, error out.
  /*!
   \param [in]  func_body         The function body
   \param [out] largestReturnStmt Handle to the current largest return stmt
  */				    
  ErrorCode compareReturnStmts(vhNode func_body,
			       vhNode* largestReturnStmt);

  //! Member to get the the range information of a Jaguar node for elaborated
  //! function body. We assume that all return stmts will all be the same
  //! size, and we just examine the first one.
  /*!
   \param [in] func_body The Jaguar node for elaborated function body.
   \param [out] dimVector A vector of VhdlRangeInfo containing all dimension info.
   \param [in] loc The VHDL source code location
   \param [in] isIndexConstraint Flag which is true if the given jaguar node corresponds to index constraint of an array object.
   \param [in] isLvalue is the object an lvalue?
   \param [in] reportError Boolean that can disable error reporting from this method.
  */
  ErrorCode getFunctionReturnRange(vhNode func_body,
                                   VhdlRangeInfoVector& dimVector,
                                   const SourceLocator& loc, bool isIndexConstraint,
                                   bool isLvalue, bool reportError,
                                   LFContext* context);

  /*! \brief determine the range(s) of an unconstrained array object,
   *  when also supplied with the size of the expression.
   *
   *  This is used for functions, binops, and unops where we know the
   *  elaborated size of an unconstrained array.  The parameters are
   *  identical to getVhExprRange, except for the first two.
   * \param [in] vh_type The unconstrained type.
   * \param [in] vh_exprsize The size associated with vh_type, constraining it.
   * \param [in] reportError Set this to false to avoid printing error messages
   * \sa getVhExprRange 
   */
  ErrorCode getUnconstrainedArrayRange( vhNode vh_type,
                                        vhNode vh_exprsize,
                                        VhdlRangeInfoVector &dimVector,
                                        const SourceLocator &loc, 
                                        bool isIndexConstraint, 
                                        bool isLvalue,
                                        bool reportError,
                                        LFContext *context );
  /*! Method to determine the of the expression corresponding to a
   function call where the function is builtin/mapped operator/function.
   The JAGUAR API vhGetExprSize() returns unconstrained array for such
   function calls. This happens because the entire body of such
   functions remains under translate/synthesis off/on region and we do
   not dump this OFF/ON region while we compile the design.
  */
  ErrorCode getWidthOfBuiltinFunc(vhExpr vh_expr,
                                  VhdlRangeInfoVector &dimVector,
                                  const SourceLocator& loc, LFContext *context);

  //! Member to get Max size of all case choice expressions
  int getMaxCaseExprSize(vhNode vh_case, LFContext *context);
  //! Member to get the sign of an expression
  SignType getVhExprSignType(vhExpr vh_expr);
  //! Get Width from ENUM_ENCODING attribute string, if specified
  bool      getWidthFromAttribute(vhExpr vh_expr, int* the_width);
  //! Get the index'd string value from ENUM_ENCODING string vector
  bool      getEncodedStringFromAttrib(UtString* str, vhExpr expr, 
                                       unsigned long idx);

  /*! \brief Function to calculate the left and right bound from a Jaguar range object.
   *
   * \param [in] vh_range The Jaguar range object
   * \param [in] bounds   The array of VhdlRangeInfo to which the range object bounds are to be added
   * \param [in] reportError Set this to false to avoid printing error messages
   * \param [in] checkStaticness Check staticness of bounds before evaluating them
   */
  ErrorCode getLeftAndRightBounds(vhNode vh_range, VhdlRangeInfo* bounds,
                                  LFContext *context, bool reportError = true,
                                  bool checkStaticness = true);

  //! Does this function sign-extend the operand?
  bool functionSignExtendsOperand (const char* package_name, const char* func_name, vhExpr arg);

  //! Populate NUSysFunctionCall for 'ENDFILE' calls.
  ErrorCode sysFunctionCall(vhExpr vh_func_call,
                            LFContext *context, NUExpr **the_expr);
  //! Create NUExpr corresponding to a predefined standard package function 
  //! call. This function will return eNotApplicable if the called VHDL function
  //! does not correspond to any standard package function .
  ErrorCode acceleratedExprForFunctionCall(vhExpr vh_func_call,
                                           LFContext *context, NUExpr **the_expr,
                                           UInt32 lhsWidth);

  //! Creates a conditional operands used for a NUTernaryOp to determine if 
  //! variable sized operands of a relational operator are of the same size or not.
  ErrorCode createConditionalOperand(vhExpr vh_expr, 
				     bool sizeVaries, 
				     NUExpr* orig_expr,
				     NUExpr** the_expr, 
				     LFContext* context,
				     const SourceLocator& loc);

/* --------------------------NEW RECORD METHODS----------------------------*/
  //! Allocate a NUCompositeNet and its elements for the node, along with the matching scope
  ErrorCode allocRecordNet( vhNode node, StringAtom *name,
                            VhNodeArray *recConstraintArray, NetFlags flags,
                            NUScope *declaration_scope, LFContext *context,
                            vhExpr vh_size, NUNet **the_net );
  //! Populate a record aggregate.
  /*! 
    Returns an expression that is concat of composites ..of type
    NUComposite[Expr|Lvalue], where each composite represents aggregate for
    a record or single/multi dimensional record arrays.
  !*/
  template <class NodeType>
  ErrorCode recordAggregate( vhNode aggregate, 
                             LFContext *context, NodeType **the_retval );

  //! Populate aggregate into a composite concat expression.
  template <class NodeType>
  ErrorCode createAggregateConcat(vhNode aggregate, ConstantRange& elemRange,
                                  bool isArrayAggregate, const SourceLocator& loc,
                                  vhNode typeDef, LFContext *context,
                                  NodeType **the_retval);
  //! Populate specified composite field/element positions and widths of aggregate with
  //! given expression.
  template <class NodeType>
  ErrorCode createElementExprOrLvalue(vhExpr expr_node, LFContext* context,
                                      AggElem& agg_elem, UtVector<NodeType*>& nodeVec);
  //! Converts the given choice into a list of composite field positions or composite
  //! array element positions.
  template <class NodeType>
  ErrorCode getElementPositions(vhExpr choice, vhNode typeDef,
                                bool isArrayAggregate, const SourceLocator& loc,
                                UtVector<NodeType*>& nodeVec, LFContext* context,
                                ConstantRange& elemRange, AggElem& agg_elem);

  //! Template function to populate either a L- or R-value record expression
  template<class NodeType> 
  ErrorCode exprOrLvalue( vhExpr elem, LFContext *context, NodeType **nuFieldNode, int lWidth = 0);

  //! Populate a NUCompositeFieldLvalue node for a field reference (not used right now?)
  ErrorCode fieldRefLvalue( vhNode field_ref, LFContext *context, 
                            NUCompositeFieldLvalue **the_lvalue, bool isAliasDecl );
/* ------------------------END NEW RECORD METHODS--------------------------*/

private:
  //! handles functions from std.stdio, returns eSuccess if a supported function found
  ErrorCode acceleratedExprForStdioFunctionCall(vhExpr vh_func_call,
                                                LFContext *context, NUExpr **the_expr,
                                                UInt32 lhsWidth);
  //! Create a NUIdentRvalue for population.  This may be wrapped in a
  //resize if the net is of an integer subrange type.  This should
  //always be used instead of calling "new NUIdentRvalue" from within
  //VHDL population.
  NUExpr *createIdentRvalue( NUNet *net, const SourceLocator &loc );
  //! Create a NUMemselRvalue for population.  This may be wrapped in a
  //resize if the memory element is of an integer subrange type.  This
  //should always be used instead of calling "new NUMemselRvalue" from
  //within VHDL population.
  NUExpr *createMemselRvalue( NUNet *net, NUExpr *expr, const SourceLocator &loc );
  //! Create a NUMemselRvalue for population. This method should be used during population instead of calling "new NUMemselRvalue".
  /*! If
    \param net the memory
    \param exprs the index expressions for memory
    \param resize_ranged_integers if true then the returned expression
           will be wrapped in a resize if the memory element is of an integer
           subrange type.
  */
  NUExpr *createMemselRvalue( NUNet *net, NUExprVector *exprs, bool resize_ranged_integers, const SourceLocator &loc );
  // Returns the jaguar node for given record field index.
  vhNode getRecordElement(vhNode recType, UInt32 fieldIndex);

public:

  typedef UtMap<NUModule*,NUExprReplacementMap> EntityAliasMap;

  //! Add to the map of alias net to the aliased expression for given scope.
  void mapVhdlAlias(NUModule* ent, NUNet* aliasNet, NUExpr* aliasedExpr);
  //! Return the scope-wise map of alias net to aliased expression.
  EntityAliasMap* getVhdlAliasMap();

  //! Method to process the function call to the predefined function RESIZE()
  NUExpr* createExprForResizeFunc( NUExpr* extdOrPartSelExpr, UInt32 lopWidth, 
                                   const SourceLocator& loc, UInt32 lhsWidth);
  //! Create expression corresponding to this functin call if this function
  //! corresponds to any built in function or mapped operator.
  ErrorCode processBuiltInFuncOrMappedOperator( vhExpr vh_func_call,
                                                LFContext *context, NUExpr **the_expr,
                                                UInt32 lhsWidth );
  //! Create subprogram signature by concatenating the width of each of the
  //! subprogram argument with the subprogram's PATHNAME. This is required
  //! to support subprograms with unconstrained array in it's signature.
  StringAtom* createSubprogSignature(LFContext *context, 
                                     vhNode vh_function_call); 
  //! Maps jaguar net/port with the given nucleus net in the declaration scope
  //! of the net.
  ErrorCode mapNet(NUScope* scope, mvvNode mvv_node, NUNet* net);
  //! Maps NUNet to a constraint range, if one was created
  ErrorCode mapConstraintRange(NUNet* net);
  /*! \brief Lookup a NUNet given the Jaguar node
   *
   * This function takes record nets into account.  If it is passed a VHSELECTEDNAME,
   * it will look the selected name up in the local symbol table.  If it's not, it
   * will look it up in the standard fashion in the DesignPopulate net map.
   *
   * \param [in] scope    The nucleus object for the scope of lookup
   * \param [in] node     The Jaguar node
   * \param [in] the_net  The pointer to pointer of the looked up net
   * \param [in] error_if_missing Set to false to avoid error reporting
  !*/
  ErrorCode lookupNet( NUScope *scope, vhNode node, LFContext *context,
                       NUNet **the_net, bool error_if_missing = true );
  //! Map a vhNode representing a port to a NUNet.
  /*!
    This function takes record nets into account.  If it is passed a VHSELECTEDNAME,
    it will look the selected name up in the local symbol table.  If it's not, it
    will look it up in the standard fashion in the DesignPopulate port map.
   */
  ErrorCode lookupPort( NUModule *module, vhNode node, LFContext *context,
                       NUNet **the_port );
  //! Count the total number of nested record elements.  Return 1 if not a record type.
  UInt32 numRecordElements( vhNode net, LFContext *context ) const;
  //! Create the name of a record element as a UtString
  /*! The created name is dot-separated, e.g. rec.subrec.field .  the
   *  vhExpr argument must be a node representing a relevant name in
   *  Jaguar.
   \param[in] node The node to make a name for
   \param[out] retval A UtString of the appropriate name
   */
  static void createRecordName( vhExpr node, UtString* retval );

  //! Expand a record into its component NUNets.
  /*! Take a Jaguar node corresponding to a record and expand it into an
    array of NUNet record elements, looking the NUNets up in the symtab.
    The result is placed in fieldArray.  This must only be called with a
    net for which isRecordNet( net ) is true.
    \param[in] net The Jaguar record net node.
    \param[out] fieldArray The ordered array of NUNets.
    \param[in] scope The scope to look in for the record. NULL means look in the current scope.
    \param[in] context The LFContext
  */
  ErrorCode expandRecord( vhNode net, UtPtrArray &fieldArray, NUModule *module,
                          LFContext *context);
  //! Expand a record into an array of vhNode record elements
  /*! Take a Jaguar node corresponding to a record or record aggregate
      and expand it into an array of vhNode record elements, creating
      new Jaguar nodes for each of the needed selected names.  The
      result is placed in fieldArray.  This must only be called with a
      net for which isRecordNet( net ) is true.
      \param[in] net The Jaguar record net node.
      \param[out] fieldArray Ordered array of manufactured Jaguar record element nodes.
      \param[in] context The LFContext
   */
  void expandRecordPortActuals( vhNode net, VhNodeArray &fieldArray,
                                LFContext *context );
  /*! \brief Use Jaguar to allocate names for each of the subelements of the record object.
   *
   * There are places where Jaguar supplies us with a record, and we
   * need each of the elements.  This set of elements does not exist in
   * Jaguar, so we must create it.  Jaguar's memory manager handles the
   * allocation and cleanup of these objects.
   * \param[in] net The net to create elements for.
   * \param[out] fieldArray An array to place the allocated strings into.
   * \param[in] context The LFContext.
   */
  void createRecordFieldNames( vhNode net, VhNodeArray &fieldArray,
                               LFContext *context );

  //! Take a record net declaration and create subnets for all its elements
  /*!
    \param vh_net    Jaguar node for the net in question
    \param net_name  The name of the net, pre-mangled for scope
    \param flags     The flags for net
    \param context   The local flow context
   */
  ErrorCode processRecordNetDecl( vhNode vh_net, StringAtom *net_name,
                                  NetFlags flags, LFContext *context );
  //! Take a record port declaration and create ports for all its elements
  /*! This method will use processRecordPortDecl to create the symtab
    hierarchy for the record.  It will also create a port net for each
    of the record elements.  The port net and the net in the symtab
    hierarchy are aliased together, with the port being the storage net,
    and the net in the symtab hierarchy being the master net.

    \param vh_net  Jaguar node for the net in question
    \param context The local flow context
   */
  ErrorCode processRecordPortDecl( vhNode vh_net, LFContext *context );
  //! Map a vhNode representing an element of a record signal to the net representing it.
  /*! \param module The module this selected name resides in
      \param node The node to look up.  It should be a selected name.
      \param the_net Parameter return for the NUNet mapped to.
   */
  ErrorCode lookupRecordElement( NUScope *scope, vhExpr node, vhExpr rec, LFContext *context, NUNet **the_net );
  //! what should this do?
  ErrorCode lookupRecordPort( LFContext *context, vhExpr node, NUNet **the_net );
  //! Map the supplied vhNode to the STSymbolTableNode representing the record.
  /*! Whole records are not stored in the standard net map, since they
   * have no NUNet corresponding to them.  This method starts with the
   * STBranchNode for the current declaration scope and walks up the
   * parent chain, examining each scope for a child branch of the
   * appropriate name.  This method will die if handed something that is
   * not either a record or record element that has already been added to
   * the symbol table.  This method handles both objects and selected names.
   \param node The node being looked up.  This must be of record type.
   \param parent The STNode that we will start looking in.
   \param context The LFContext.
   \return The STSymbolTableNode representing the record.
   \sa lookupSTNodeForSelName
  */
  static const STSymbolTableNode *lookupSTNodeForRecord( vhNode node,
                                                         const STSymbolTableNode *parent,
                                                         LFContext *context );
  //! Helper that looks up an object in the symbol table in the provided scope
  /*! Whole records are not stored in the standard net map, since they
   * have no NUNet corresponding to them.  This method starts with the
   * STBranchNode for the current declaration scope and walks up the
   * parent chain, examining each scope for a child branch with the
   * name of the root of the selected name.  This method will die if handed
   * something that is not either a record or record element that has already
   * been added to the symbol table.
   \param node The node being looked up.  This must be of record type and a VHSELECTEDNAME.
   \param parent The STNode that we will start looking in.
   \param context The LFContext.
   \param isInPackage It is true if the node was defined in the package.
   \return The STSymbolTableNode representing the record.
   \sa lookupSTNodeForRecord
  */
  static const STSymbolTableNode *lookupSTNodeHelper( vhNode node,
                                                      const STSymbolTableNode *parent,
                                                      LFContext *context, bool isInPackage );
  //! Create individual assignments for each element of a record assignment
  ErrorCode recordAssign( const assignType assignType, const vhExpr vh_lvalue_expr,
                          const vhExpr vh_rvalue_expr, LFContext *context,
                          const SourceLocator &loc );
  //! Construct expression for each field of the record lvalue from the given
  //! rvalue expression.
  ErrorCode getRvalueExprs(vhExpr vhRvalue, vhNode rvalRecIndex, LFContext* context,
                           const SourceLocator& loc, NUExprArray& rvalArray);
  //! Create an expression for a logical AND of each record field in the operands
  ErrorCode recordCompare( const NUOp::OpT op, const vhExpr vh_lvalue_expr,
                           const vhExpr vh_rvalue_expr, LFContext *context,
                           const SourceLocator &loc, NUExpr **the_expr );
  //! get the integer index of vh_element in vh_record (from Jaguar)
  UInt32 getVhRecordFieldIndex( vhNode vh_element, vhNode vh_record,
                                LFContext *context ) const;
  /*! \brief Create an expression for a selected name, dealing with any arrayed records.
   *
   * \param [in] vh_named The Jaguar expression to be processed
   * \param [in] context 
   * \param [in] propagateRvalue set this to true when the expression is expected to evaluate to a constant (static) value. If true then extra effort is expended to determine the constant initial value.
   * \param [in] reportError Set this to false to avoid printing error messages
   * \param [out] the_expr pointer to NUExpr that is created

   */
  ErrorCode selectedNameExpr( vhExpr vh_expr, LFContext *context, NUExpr **the_expr,
                              bool propagateRvalue, bool reportError );
  //! Create an lvalue for a selected name, dealing with any arrayed records
  ErrorCode selectedNameLvalue( vhNode vh_lvalue, LFContext *context,
                                NULvalue **the_lvalue, bool isAliasDecl );
  //! Helper method to find and populate all the indices involved in an arrayed record reference
  ErrorCode exprIndices( vhExpr vh_expr, LFContext *context,
                          NUExprVector *selExprVector );
  //! Helper method to find all the indices involved in complicated expressions
  /*
    leafOnly argument indicates that method should limit itself to collecting indices
    of leaf indexed name. For example: top.rec(0).f1.f2(3)(5). If leafOnly is set,
    the selExprVector would have indices 3, 5 ..just the leaf indices. If leafOnly is
    false, then selExprVector would have indices 0, 3, 5.
  */
  void exprIndices( vhExpr vh_expr, LFContext *context,
                     UtVector<vhExpr> *selExprVector, bool leafOnly = false );

  //! Checks if the width of a VHSLICENAME or VHRANGE is constant and if
  //! it's variable indexed. If so returns eSuccess and the constant
  //! width.
  /*!
   \param [in] vh_slice The jaguar node to operate on
   \param [out] constantWidth The constant width returned in this parameter. 0 is an invalid value.
   \param [out] isVariableIndexed Flag which has to set 'true' if the index is dynamic
   \returnval eSuccess  If the given slice is variable indexed with a fixed width,
   \returnval eNotApplicable  If the width is fixed, or if it is dynamic but we can't determine a fixed width, but we found no smoking gun
   \returnval eFailNoCreate If the width is dynamic and we encounter a construct we cannot support
   \returnval eFatal An error condition
  */
  ErrorCode getVariableRangeWidth(vhNode vh_slice,
                                  LFContext* context,
                                  int *constantWidth,
                                  bool *isVariableIndexed);

  //! Given a VHSLICENAME token, returns the net, index expression and range
  //! normalized to a [width-1:0] object.
  ErrorCode partsel ( vhExpr vh_slice, LFContext *context, int lWidth,
                      NUBase** the_node, NUExpr** index_expr, ConstantRange* range,
                      UtVector<vhExpr> *dimVector, bool* isConstIndex,
                      const bool isLvalue);

  //! Method to populate variable width slicename expression.
  /*!
   \param vh_slice The jaguar node corresponding to the variable width slice name expression.
   \param context Context of the current population.
   \param net Nucleus net object whose part select has to be created.
   \param the_expr Refrence to the nucleus part select expression which will be created out of the input 'vh_slice' .
   \param lWidth the context-derived width
   \return eSuccess on the successful creation of the nucleus expression from the jaguar expression 'vh_slice' otherwise eFailNoCreate or eFatal depending on the severity of the population error.
   */
  ErrorCode variableWidthSliceNameRvalue( vhExpr vh_slice, LFContext *context,
                                          NUNet *net, NUExpr **the_expr, int lWidth);

  //! Method to populate variable width slicename lvalue
  /*!
   \param vh_slice The jaguar node corresponding to the variable width slice name lvalue
   \param context Context of the current population.
   \param net Nucleus net object whose part select has to be created.
   \param the_lvalue Refrence to the nucleus part select lvalue which will be created out of the input 'vh_slice' .
   \param lWidth the context-derived width
   \return eSuccess on the successful creation of the nucleus expression from the jaguar expression 'vh_slice' otherwise eFailNoCreate or eFatal depending on the severity of the population error.
   */
  ErrorCode variableWidthSliceNameLvalue( vhExpr vh_slice, LFContext *context,
                                          NUNet *net, NULvalue **the_lvalue, int lWidth);

  //! Method to populate c-model with the generic information of the dummy
  //! entity.
  /*!
    \param cmodel The c-model where to put the generic information
    \param vh_entity The Jaguar node corresponding to the entity
   */
  ErrorCode cGenerics( NUCModel* cmodel, vhNode vh_entity);

  /*! \brief Create a unique identifier for a package, used to locate the scope for a package
   */
  StringAtom *createPackIdStr( const char* libName, const char *packName,
                               LFContext *context );
  /*! \brief Create NUScopes for a package and its library, if necessary
   */
  ErrorCode createPackageScope( vhNode vh_pack, LFContext *context,
                                NUNamedDeclarationScope **the_package );
  //! Method to create a hierref to a signal declared in a package
  ErrorCode packageNet( vhNode vh_net, LFContext *context, NUNet **the_net );

  NUExpr* createSizeExpr(NUExpr* val, NUExpr* sz,
                         const SourceLocator& loc,
                         UInt32 lhsWidth, bool sign);
  void emitValueIfConst(vhExpr, UtString*);

  //! Used to create a temporary memory copy if there's an overlap between 
  //! lvalue and rvalue of a mem <= aggregate or mem_slice <= mem kind of
  //! statement.
  NUNet* maybeCreateTempMemCopy(assignType assign_type, NUMemoryNet* mem,
                                NULvalue* lval, NUExpr* rval,
                                LFContext* context, const SourceLocator& loc);


  //! Used to populate attributes of a scalar type. 
  //! This method is called only if the argument of the attribute is dynamic
  //! In the static case the Jaguar evaluates the attribute and replaces it
  //! with constant
  ErrorCode attributeExpr(VhAttrbType attrType, vhExpr vh_expr, LFContext *context,
                          SourceLocator loc, NUExpr **the_expr);

  //! Is this type character or subrange of character?
  bool isCharacterType(vhNode node);

  //! This function breaks memory net into vectors. 
  ErrorCode breakMemoryNet(NUNet *netExpr, NUExprVector *expr_vector, const SourceLocator &loc );

  //! Class that allows collecting expressions returned by functions.
  /*!
    A function call to a function that returns a constant value can be replaced
    with the expression for the constant value. This class enables this 
    optimization by tracking all the return expressions from a function. These
    expressions are then analyzed by checkFunctionConstness() method to
    determine if function call can be replaced with a constant expression.
  */
  class FuncRetValCollector
  {
  public:
    FuncRetValCollector(VhdlPopulate* populator, bool enable);
    ~FuncRetValCollector();

    const NUExprList& getRetValExprs() const { return mFuncExprs; }

  private:
    VhdlPopulate* mPopulator;
    NUExprList mFuncExprs;
    NUExprList* mPrevFuncExprs;
  };

  bool checkFunctionConstness(NUTask* task, FuncRetValCollector& retExprs);

  //! Class that enables aggressive evaluation of expressions.
  /*!
    Use this only when you're absolutely sure that you need to statically know the value of an
    expression. This turns on aggressive evaluation of expressions where initial values of
    variable/signals are fair game. This typically would get used under following circumstances:
    a. Population of signal/variable/constant/file declarations. In such cases we know that 
       the bounds of such declarations have to be known at compile time for us to successfully
       populate them. The initial values should be used in such evaluation.
  */
  class AggressiveEvalEnabler
  {
  public:
    AggressiveEvalEnabler(VhdlPopulate* populator, bool enable = true);
    ~AggressiveEvalEnabler();

  private:
    VhdlPopulate* mPopulator;
    bool mPreviousValue;
  };

  //! Class that indicates population of statements for initialization of
  //! signal/variable/constant declarations. Such statements in processes and
  //! packages are usually added to initial block in module.
  class DeclInitIndicator
  {
  public:
    DeclInitIndicator(LFContext* context);
    ~DeclInitIndicator();

  private:
    LFContext* mContext;
  };

  //! Support classes
  bool mLowerCase;

  //! Population mappings
  typedef UtVector<UtString> StrVec; 
  typedef UtMap<StringAtom*, NUTask*> VhSignatureTaskMap;
  typedef UtMap<NUModule*, VhSignatureTaskMap > ModuleNodeTaskMap;
  typedef UtMap<NUTask*, NUConst*> TaskConstExprMap;
  typedef UtMap<vhExpr, StrVec> EnumEncodingMap; 
  //! Provides a VHDL package declaration to NUScope mapping
  typedef UtMap<StringAtom*, NUNamedDeclarationScope*> StringAtomPackageMap;
  typedef UtHashMap<vhNode, NUBlock*> VhNodeBlockMap;
  typedef UtHashMap<vhNode, UtStack<NUBlock*>* > VhNodeBlockStackMap;

  //! Provides Jaguar net/port to NUNet mapping
  typedef UtMap<vhNode, NUNet*> VhNodeNetMap;
  typedef UtMap<NUScope*, VhNodeNetMap> ScopeNodeNetMap;

  //! Provides complex Jaguar reset expr to NUExpr mapping
  typedef UtHashMap<vhNode, NUExpr*> VhNodeEdgeExprMap;

  //! Class to hold information for process being populated.
  class VhdlProcessInfo
  {
  public:
    VhdlProcessInfo();
    ~VhdlProcessInfo();

    //! Maps complex reset exprs to their replacements.
    VhNodeEdgeExprMap mComplexResetMap;
    //! Save a pointer to the current Jaguar clock expression.
    vhNode mJaguarClockExpr;
    //! Save a pointer to the current Jaguar clock edge specification.
    int mJaguarClockEdge;
    //! Save the copy of current clock expression.
    NUExpr* mClockExpr;
    //! Save a pointer to current always block.
    NUAlwaysBlock* mProcess;
    //! Save a pointer to current process body block.
    NUBlock* mBlock;
    //! Save the current process statement list.
    NUStmtList mStmts;
  };

  //! This is only valid for the population of a single sequential process.
  VhdlProcessInfo* mCurrentProcessInfo;

  StringAtomPackageMap mPackageMap;
  VhNodeBlockStackMap mBlockMap;     // used for exit & return
  VhNodeBlockMap mNextBlockMap; // used for next

  // A separate map for the exit net is required because we create a net
  // for the function in order to process the return value. 
  ScopeNodeNetMap mScopeExitNetMap; 
  ModuleNodeTaskMap mTaskMap;
  TaskConstExprMap mConstTaskMap;
  EnumEncodingMap mEnumMap;

  typedef UtHashMap<vhExpr,SignType> ExprSignMap;
  ExprSignMap mExprSignMap;

  // Map of alias nets to aliased expressions.
  EntityAliasMap mAliasMap;

  /*! mRecursionStack is used to provide a boolean value as to whether
   *  we are currently populating a recursive function or not.  This
   *  can't be a simple boolean because we wouldn't know when we are
   *  done unwinding the recursion.  We also can't set it just on if the
   *  current function is recursive; we still want to know we're inside
   *  recursion if we're populating a non-recursive function called from
   *  a recursive one.  When we start to populate a recursive function
   *  we push its jaguar pointer on this stack.  It gets popped off upon
   *  completion of its population.
   */
  UtStack<vhNode> mRecursionStack;

  UInt32 mUniqueFunctionIndex;

  //! Note if clock errors are being demoted to warnings/notes/suppressed or not
  bool mClockErrorsDemoted;

  //! Maximum number of recursions allowed for a recursive function.
  SInt32 mRecursionLimit;

  // Is > 0 when the current scope is a loop being unrolled.
  SInt32 mUnrolledLoopScopeCount;

  //! Is aggressive evaluation of expressions enabled?
  bool mIsAggressiveEvalEna;

  //! The list of return values for a given task.
  NUExprList* mReturnExprs;

  //! Is constant propgation of function that return constants enabled?
  bool mIsFuncConstPropEnabled;

  //! Maximum number of iterations allowed in an unrollable loop.
  static const UInt32 scMaxLoopUnrollIterations;

  //! Elaborated constraint range
  ConstantRange* mConstraintRange;
};
#endif
