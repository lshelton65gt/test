// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef PREPOPINTERRACB_H_
#define PREPOPINTERRACB_H_

#include "localflow/InterraDesignWalker.h"
#include "localflow/Populate.h"

class NUDesign;
class DesignPopulate;
class LFContext;
class STSymbolTable;

//! Pre-population callback structure for the InterraDesignWalker
/*!
  This simply fills a symboltable with the design hierarchy and any
  other information that will be needed to populate nucleus. This also
  provides a mechanism to uniquify design blocks using the elaborated
  symboltable.
*/
class PrePopInterraCB : public InterraDesignWalkerCB
{
public:
  
  //! Constructor
  PrePopInterraCB(DesignPopulate* populator, bool verbose);

  //! Virtual destructor
  virtual ~PrePopInterraCB();
  

  void print() const;

  virtual Status hdlEntity(Phase phase, mvvNode node, mvvLanguageType langType, const char* entityName, bool* isCmodel);

  
  virtual mvvNode substituteEntity(mvvNode entityDecl, mvvLanguageType entityLanguage, const UtString& entityDeclName, mvvNode instance, bool isUDP, mvvLanguageType callingLanguage);

  virtual Status vlogForeignAttribute(mvvNode module, const UtString& entityDeclName, mvvNode* ent, mvvNode* arch);

  virtual Status componentElabFail(mvvNode component, mvvLanguageType lang, int elab_status);

  virtual Status hdlComponentInstance(Phase phase, mvvNode inst, 
                                      const char* instName,
                                      mvvNode instMaster,
                                      mvvNode oldEntity,
                                      mvvNode newEntity,
                                      const char* entityName,
                                      mvvNode arch,
                                      const SInt32* vectorIndex, 
                                      const UInt32* rangeOffset,
                                      mvvLanguageType instLang, 
                                      mvvLanguageType scopeLang);

  virtual Status generateBlock(Phase phase, mvvNode node, const char* blockName, mvvLanguageType langType, const SInt32* genForIndex);

  virtual Status blockStmt(Phase phase, mvvNode node, const char* blockName, mvvLanguageType langType);
  virtual Status ifGenerateTrue(Phase phase, mvvNode node, const char* blockName, mvvLanguageType langType);
  virtual Status forGenerate(Phase phase, mvvNode node, mvvNode indexVal, mvvLanguageType langType);
  virtual Status unsupportedGenerateItem(mvvNode node, mvvLanguageType langType);


  // Put the tasks and functions in the elaborated symbol table.
  virtual Status taskFunction(Phase phase, mvvNode node, const char* tfName, mvvLanguageType langType);

  // Put concurrent VHDL process and VHDL library-package declaration scopes in
  // the elaborated and unelaborated symbol table.
  virtual Status concProcess(Phase phase, mvvNode node, mvvLanguageType langType);

  virtual Status packageDecl(Phase phase, mvvNode node, mvvLanguageType langType,
                             const char* libName, const char* packName, bool isStdLib);

  //! Using the elaborated symtab, uniquify modules as needed
  /*!
    The uniquification algorithm is rather simple-minded. It is a
    recursive, depth-first walk of the elaborated
    symboltable. At each level, we traverse into any branches before
    doing any calculation, so we are doing a bottom-up algorithm.
    
    As we walk back up the hierarchy, we calculate a signature for a
    module by creating a string that is the concatenation of the
    module names that are instanced by that module.

    For example, 
    \verbatim

    module top(...);
       defparam u1.a.p1 = 1;
       mid u1(...);
       mid u2(...)
    endmodule

    module mid(...);
       sub a (...);
       sub b (...);
    endmodule

    module sub(...)
       parameter p1 = 0;
       ....
    endmodule
    \endverbatim

    Here, we have two instantiations of mid. In one instance, its
    child instance is configured by a defparam. Therefore, we need mid
    to have two different definitions. The uniquification algorithm
    travels down to top.u1.a and top.u1.b and sees no other instances,
    so at top.u1 have a module signature of sub_#0_sub_#1
    (sub was uniquified already by parameter analysis during
    elaboration). Then, at top.u2, we have a module signature of
    sub_#1_sub_#1, because both instances of sub have the same
    parameter value. We see that mid has two different signatures, so
    we uniquify top.u2's blockname to be mid_#1, while top.u1's
    blockname is still mid.
    We keep track of the unique modules by keeping a map of unique
    module names to their respective module signatures. So, as we go
    up the design tree, we will be using the unique module names to
    create the signatures. Say, for example, that top was actually not
    the root of the design, its module signature would be
    mid_mid_#1. Therefore, as long as we are going bottom-up we are
    assured of unique names.
  */
  void uniquify();

  //! Find the elaborated scope given the current scope and instance
  STBranchNode* findElabScope(STBranchNode* parent, const char* instName, 
                              mvvLanguageType langType);
  //! Version of above function with StringAtom constructed for 
  //! the correct language type.
  STBranchNode* findElabScope(STBranchNode* parent, StringAtom* instAtom);
  
  //! Get the unelaborated declaration scope given an elaborated instance
  /*!
    The declaration scope is the original module/entity name in the
    design. To get the unique name of the module call
    getUniqueBlockName().
  */
  STBranchNode* getDeclarationScope(STBranchNode* instance);

  //! Given a scope, get the current unique block name for it.
  /*!
    Note that this may be 'wrong' if called before uniquify() is
    called.

    \param instance The elaborated instance of the module of which you
    want the uniquified name.
  */
  const char* getUniqueBlockName(const STBranchNode* instance) const;

  //! Given a jaguar node for vhdl process, get unique name for it.
  StringAtom* getUniqueVhdlProcessName(vhNode concProc) const;

  STSymbolTable* getDeclarationsSymTab();
  STSymbolTable* getElaborationsSymTab();
  
private:
  DesignPopulate* mPopulator;

  class Helper;
  Helper* mHelper;
  
  LFContext* mLFContext;
  
};

//! Manages stack of symbol table branch nodes.
/*!
  Has ability to backup current stack and work on a new stack. The new stack starts
  with root which is at the bottom of current stack. This can backup only
  one stack.
*/
class BranchStack
{
public:

  BranchStack();
  ~BranchStack();

  //! Empty stack
  bool empty();
  //! Push to top of stack.
  void push(STBranchNode* scope);
  //! Pop from top of the stack.
  void pop();
  //! The branch node at top of the stack.
  STBranchNode* top();

  //! Save current stack away and begin a new stack with root inserted in it.
  /*!
    Can be used to save only one stack. Call restoreStack before calling this again.
  */
  void saveStackAndBeginAtRoot();
  //! Restore the backed up stack and delete the current one.
  void restoreStack();

private:
  typedef UtList<STBranchNode*> STBStack;
  STBStack* mStack;
  STBStack* mSavedStack;
};

#endif
