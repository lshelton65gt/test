// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef VERILOGPOPULATE_H_
#define VERILOGPOPULATE_H_

#include "localflow/Populate.h"
#include "nucleus/Nucleus.h"
#include "compiler_driver/Interra.h"
#include "flow/Flow.h"
#include "nucleus/NUExpr.h"

class LFContext;
class ArgProc;
class Stats;
class AtomicCache;
class DynBitVector;
class DesignPopulate;
class CheetahContext;
class CarbonContext;
class HdlVerilogPath;
class TicProtectedNameManager;


//! VerilogPopulate class
/*!
 * Class to walk the Cheetah representation and populate Nucleus.
 *
 * All member functions which populate a Nucleus construct from Cheetah return
 * an error code.  By default, the passed-in pointer to the construct is set to 0.
 * If success is returned, or if the compiler was able to create the construct
 * even though an error was encountered, then the passed-in pointer will be non-0.
 */
class VerilogPopulate : public Populate
{
public:
  VerilogPopulate(DesignPopulate *design_populate,
                  STSymbolTable *symtab,
		  AtomicCache *str_cache,
		  IODBNucleus * iodb,
		  SourceLocatorFactory *loc_factory,
		  FLNodeFactory *flow_factory,
		  NUNetRefFactory *netref_factory,
		  MsgContext *msg_context,
		  ArgProc *arg,
                  TicProtectedNameManager* tic_protected_nm_mgr,
		  bool allow_hierrefs,
                  bool useLegacyGenerateHierarchyNames,
                  CarbonContext*);

  //! Populate a NUModule from the veNode.  Main entry point into Verilog population
  ErrorCode moduleOrUDP(veNode ve_module, LFContext *context, const char *uniquifiedName, const char* unit, const char* precision, NUModule **the_module, bool* seenAlready);
  ErrorCode createUDPModule(veNode ve_module, LFContext *context, NUModule **the_module);
  ErrorCode postDesignFixup(LFContext *context);

  ErrorCode topLevelMultiPortNet(NUNet* the_port);

  //! Check that a primary input is not written unless port coercion is enabled.
  static void checkCoercionLvalue(NUNet *net, MsgContext *msg_context, LFContext *context, const SourceLocator &loc);

  //! Populate a NUModule from Verilog for instantiation in VHDL
  /*!
    \param vh_inst The VHDL component instance
    \param old_module The original module that user instantiated.
    \param new_module The substituted module that replaces original module.
    \param context Context of the module doing the instantiation
    \returns ErrorCode The status error code
  */
  ErrorCode VlogEntity(vhNode vh_inst, mvvNode old_module,
                       veNode new_module, LFContext *context);

  /*!
   * This function will detect the design unit type i.e whether it is 
   * module or udp.
   */
  ErrorCode findIfModuleOrUDP(veNode ve_module, bool *isUDP);
  //! destructor
  ~VerilogPopulate();

  //! Run actual parameter analysis.
  /*!
    veModuleInstanceElaborate must have already been called.
  */
  ErrorCode analyzeInstanceParameters(veNode ve_module,
				      UtString* modInstName);
private:
  //! Hide copy and assign constructors.
  VerilogPopulate(const VerilogPopulate&);
  VerilogPopulate& operator=(const VerilogPopulate&);


  //! List of pairs: (Cheetah nodes, string)
  typedef std::pair<veNode,StringAtom*> VeNodeNamePair;
  typedef UtList<VeNodeNamePair> VeNodeNamePairList;

  //! Class which holds grabbed parameter values from Cheetah
  /*!
    Cheetah is used to evaluate the parameter expressions. This holds
    the result.
  */
  class ParamValue
  {
  public:
    //! trivial constructor
    ParamValue()
    {
      clear();
    }

    //! (Re)Initialize the object
    void clear()
    {
      mBuf.clear();
      mSize = 0;
      mType = VE_CONST;
      mParamVal = 0;
      mDblVal = 0.0;
      mDoNoValOpt = false;
      mConvertBasedNum = true;
      mValidDblVal = false;
      mValidParamVal = false;
      mValidSize = false;
    }
    
    //! Get the parameter value string - constant
    const UtString& getStr() const 
    {
      return mBuf;
    }

    //! Get the parameter value string - modifiable
    UtString& getStr() {
      return mBuf;
    }

    //! Does the value have an X or a Z?
    bool hasXZ() const {
      bool ret = false;
      if ((mType != VE_CONST) &&
          (mType != VE_STRING))
        ret =  (mBuf.find_first_of("xzXZ?") != UtString::npos);
      return ret;
    }

    //! Get the type of the parameter
    veObjType getType() const
    {
      return mType;
    }

    //! Get the effective width of the parameter
    UInt32 getSize() const {
      ASSERT(mValidSize);
      return mSize;
    }
    
    //! Get the integer value of the parameter 
    /*!
      This is only useful if hasValidParamVal() is true.
    */
    SInt32 getParamVal() const {
      ASSERT(hasValidParamVal());
      return mParamVal;
    }

    //! Get the double value of the parameter
    /*!
      This is only useful if hasValidDblVal() is true
    */
    double getDblValue() const {
      ASSERT(hasValidDblVal());
      return mDblVal;
    }

    //! False if the user does not want to convert based numbers
    /*!
      For parameter-based module uniquification, we want binary for
      most things to keep the uniquification consistent based on size
      and value. But for cmodels, we just want the based number as it
      was specified. 
      
      The default is true (unlike doNoValOpt(),
      mainly to stay away from a very confusing function name).
    */
    bool doConvertBasedNumber() const { return mConvertBasedNum; }


    //! Convert based numbers to binary?
    /*!
      See doConvertBasedNumber()
    */
    void putConvertBasedNumber(bool convertBased) { 
      mConvertBasedNum = convertBased;
    }
    
    //! Perform value optimization?
    /*!
      During getParamValueString, the value is analyzed to see if we
      can shorten it. If this is true, then we do not shorten it.
      Default is false.
    */
    bool doNoValOpt() const { 
      return mDoNoValOpt;
    }

    //! See doNoValOpt()
    void putDoNoValOpt(bool valOpt)
    {
      mDoNoValOpt = valOpt;
    }

    //! Setting for Cheetah type
    void putType(veObjType type) {
      mType = type;
    }

    //! Setting for integer value
    void putValue(SInt32 val) {
      ASSERT(not hasValidDblVal());
      mParamVal = val;
      mValidParamVal = true;
    }

    //! Setting for size
    void putSize(UInt32 sz) {
      mSize = sz;
      mValidSize = true;
    }

    //! Setting for double value
    void putDbl(double val)
    {
      ASSERT(not hasValidParamVal());
      mDblVal = val;
      mValidDblVal = true;
    }

    //! Return true if putValue() has been called, false otherwise.
    /*!
     * Must not call getParamVal() unless this routine returns true;
     */
    bool hasValidParamVal() const
    {
      return mValidParamVal;
    }

    //! Return true if putDbl() has been called, false otherwise.
    /*!
     * Must not call getDblVal() unless this routine returns true;
     */
    bool hasValidDblVal() const
    {
      return mValidDblVal;
    }

    //! Invalidate any dbl val that may be held.
    void invalidateDblVal()
    {
      mValidDblVal = false;
      mDblVal = 0.0;
    }

    //! Invalidate any param val that may be held.
    void invalidateParamVal()
    {
      mValidParamVal = false;
      mParamVal = 0;
    }

  private:
    UtString mBuf;
    UInt32 mSize;
    veObjType mType;
    SInt32 mParamVal;
    double mDblVal;
    bool mDoNoValOpt;
    bool mConvertBasedNum;

    // Note on the following booleans:  This class and its use is fairly messy, this is an attempt
    // to make sure that if someone queries a value, there is a valid value in place.
    bool mValidDblVal;    // true if putDbl was called, false otherwise.
    bool mValidParamVal;  // true if putValue was called, false otherwise.
    bool mValidSize;      // true if putSize was called, false otherwise.
  };

  //! Double evaluation of expression value.
  static void sDoEvalDouble(ParamValue* paramVal, veNode expr);


  //! Given left and right range exprs, get the constant range values
  ErrorCode getParameterRange(veNode leftExpr, veNode rightExpr, ConstantRange* range);

  //! Put the string representation of the value of expr into buf. Used by module naming
  /*!
   * If the parameter type is anything but a VE_CONST buf contains the value
   * If the parameter type is VE_CONST buf contains the binary value
   * and param_value is the integer value.
   */
  ErrorCode getParamValueString(ParamValue* paramVal,
				veNode expr);

  /*!
   * Helper function to map gate primitives to expression operators.
   *
   * \return NUOp::eInvalid when the primitive is not supported
   */
  NUOp::OpT getOpFromGatePrimitive(veNode ve_prim, LFContext *context, bool* invertResult);

  /*!
   * Handle pull primitives.
   * Just set the net flags; the initial drivers are created later (during Reduce).
   */
  ErrorCode pull(veNode ve_prim, LFContext *context);

  /*!
   * Local helper function to populate a continuous assign for a unary or
   * binary operator.
   */
  ErrorCode simpleGatePrimitive(veNode ve_prim,
				NUOp::OpT op,
				StringAtom *block_name,
                                UInt32 num_of_instances,
				const SourceLocator& loc,
				LFContext *context,
                                bool invert);

  /*!
   * helper function for simpleGatePrimitive, It prepares an
   * expression that can be used as an argument for a primitive gate, if
   * num_of_instances is greater than 1 then a concat will be returned.
   * In that case if the rhs is more than a simple expression then a temp
   * is used to reduce the duplication of expressions.
   */
  NUExpr* prepareGatePrimitiveArgument(NUExpr* rhs,
                                       UInt32 num_of_instances,
                                       NUModule* module,
                                       const SourceLocator& loc);

  /*!
   * helper function for simpleGatePrimitive, It prepares an
   * LValue that can be used as an argument for a primitive gate, if
   * num_of_instances is greater than 1 then a NUConcatLvalue will be returned.
   */
  NULvalue* prepareGatePrimitiveArgument(NULvalue* lhs,
                                         UInt32 num_of_instances,
                                         NUModule* module,
                                         const SourceLocator& loc);

  ErrorCode processVlogEntity(vhNode vh_inst, mvvNode old_module,
                              veNode new_module, LFContext *context);

public:
  //! Create a source locator for the given Cheetah object
  SourceLocator locator(veNode ve_node);
private:
  //! Create a source locator for the given MVV object
  SourceLocator locator(mvvNode mvv_node);

  //! Create Resistive/Non-Resistive device gate conditional
  ErrorCode deviceTernary(veBuiltinObjectType type,
			  UInt32 size,
			  NUExpr* driver,
			  NUExpr* enable,
			  const SourceLocator& loc,
			  LFContext *context,
			  NUExpr **the_expr);

  /*!
   * Local helper function to populate a continuous assign for a bufif/notif
   * primitive.
   */
  ErrorCode teCondGatePrimitive(veNode ve_prim,
				StringAtom *block_name,
                                UInt32 num_of_instances,
				const SourceLocator& loc,
				LFContext *context);

  //! build a single-bit expression for use as a condition (For, If, Case)
  NUExpr* buildCondition(NUExpr* expr) {return expr->makeConditionExpr();}

  /*!
   * Compute the size in UInt32s it would take to store this
   * net. Scalars would take one and vectors can take one or more.
   */
  UInt32 getSize(NUNet* net);

public:
  //! Return the canonical Cheetah node for this scope variable in this module.
  veNode getHierRefMapping(NUModule *module, veNode ve_scopevar);

private:
  /*!
   * Determine if this hierarchical reference has already been populated.
   * If it has, return the populated net in the_net.  Otherwise, the_net will be 0.
   *
   * Since Cheetah creates many scope variables for the same hierarchical reference,
   * we need to detect which scope variables are really the same.  We do this by
   * maintaining a mapping of hierarchical name to canonical scope variable node
   * (per each module).
   */
  ErrorCode makeHierRefMapping(NUModule *module, veNode ve_scopevar, NUNet **the_net);

  //! Remember possible net hier ref resolutions so we can set up pointers.
  ErrorCode rememberPossibleResolutions(NUNetHierRef *net_hier_ref,
                                        VeNodeNamePairList &resolutions,
                                        SourceLocator &loc);

  //! Maintain map of Cheetah node to Nucleus task
  ErrorCode lookupTask(NUModule *module, veNode node, NUTask **the_task);
  ErrorCode mapTask(NUModule *module, NUTask *task, veNode node);

  //! Populate pointers to/from hierarchical net references and their possible resolutions.
  ErrorCode allPossibleNetResolutions(LFContext *context);

  /*!
   * Find all NUNet's corresponding to the ve_possible_resolution node, and populate
   * the pointers to/from the hier ref and the resolutions.
   */
  ErrorCode possibleNetResolutions(NUNetHierRef *net_hier_ref,
                                   veNode ve_possible_resolution,
                                   StringAtom *module_name,
                                   LFContext *context);

  //! Populate pointer to/from hierarchical net reference and a possible resolution
  ErrorCode possibleNetResolution(NUNetHierRef *net_hier_ref, NUNet *net_resolution);

public:
  //! Populate a port connection.
  /*!
    \param ve_portconnectiosn_for_port  the list of ve_portconn for 
            this port. If multiple portconnectiosn then they are
            concated together to form a single actual. (at one
            time these were called wacky ports)
    \param context Context of the module doing the instantiation
    \param formal Net of formal (inside module being instantiated)
    \param instantiated The module being instantiated (where the formals are)
    \param offset For normal instances, this is NULL.
                  For ranged instantiations, this is the relative offset of the current instantiation.
                  This offset will be applied to all port actuals.
    \param the_conn If this routine is successful, this will point to the created port connection.

    \warning currently wacky ports are only supported if all bits of
   the vector are specified (in their declaration order, and are
   adjacent) in the formal port list, and sufficient bits are
   connected in the actual list.
   */
  ErrorCode portConnection(const VeNodeList & ve_portconnections_for_port,
                           LFContext *context,
                           NUNet* formal,
                           NUModule* instantiated,
                           const UInt32 *offset,
                           const SourceLocator& loc,
                           NUPortConnection **the_conn);

  //! Get the original name associated with a module.  Cheetah can create
  //! fake names for modules found via uselib, if that name clashes with
  //! some other module.  This routine gets us the original name, even
  //! though another module may also have that name.  Note that when reading
  //! a tokenized file generated with a uselib, we only get to know the
  //! original name via a comment meta-directive embedded doing tokenizing.
  StringAtom* getOriginalName(veNode mod);
  
  ErrorCode netHierRef(veNode ve_scopevar, LFContext* context, NUNet **the_net);
  ErrorCode port(veNode ve_port, LFContext* context, NUNet **the_net);
  ErrorCode var(veNode ve_var, LFContext* context, NUNet **the_net);
  ErrorCode net(veNode ve_net, LFContext* context, NUNet **the_net);
  ErrorCode local(veNode ve_net, LFContext* context, NUNet **the_net);
  ErrorCode declAssign(veNode ve_net, LFContext* context, NUContAssign **the_assign);
  ErrorCode contAssign(veNode ve_net, LFContext* context, NUContAssign **the_assign);
  ErrorCode addAlwaysBlock(veNode ve_always, LFContext *context, NUModule* module);
  ErrorCode specifyBlock(veNode ve_spec_block, LFContext* context);
  ErrorCode gatePrimitive(veNode ve_prim, LFContext *context);
  ErrorCode initialBlock(veNode ve_always, LFContext *context, NUInitialBlock **the_block);
  ErrorCode tf(veNode ve_tf, const char* tfName, LFContext *context, NUTask **the_tf);
  ErrorCode cModule(veNode ve_module, const char* modName, NUModule* module, LFContext* context);
  ErrorCode udpComponents(veNode ve_udp, NUModule* module, LFContext* context);

  //! Find a substitute module if one exists.
  static veNode substituteModule(mvvNode oldMod,
				 mvvLanguageType oldModLang,
				 bool isUDP,
				 UtString& new_mode_name,
				 TicProtectedNameManager* ticProtectedNameMgr);
  
  //! Handle the contents of a generate block.
  /*! Gets called once for each generated instance of the block.
    \param ve_generate The node to generate
    \param index the instance index of the ve_generate node.  If index is 
    NULL, this is a generate if or case block and does not have/use an
    index.
    \param genBlockCnt The current number of generated blocks in the
    scope.
    \param context The current LFContext
    \return ErrorCode Status of the call
   */
  ErrorCode generateBlock( veNode ve_generate, const SInt32* index, const char* blockName, LFContext *context );

  //! Get the instance name
  /*!
    \param ve_inst Cheetah node for the instantiation
    \param index If NULL, this is a non-ranged instantiation.
                 Otherwise, this points to the current index of the ranged instantiation.
    \param context Context of the module doing the instantiation
   */
  StringAtom* getInstanceName(veNode ve_inst, const SInt32 *index,
                              LFContext *context);

  //! Populate one module instance
  /*!
    \param ve_inst Cheetah node for the instantiation
    \param newModule If substituteModule was successful in finding a
    substitution, this will be non-NULL with the result of substituteModule
    \param offset If NULL, this is a non-ranged instantiation.
    Otherwise, this points to the current relative offset of the ranged instantiation.
    \param context Context of the module doing the instantiation
  */
  ErrorCode oneModuleInstancePost(veNode ve_inst, veNode newModule, const UInt32 *offset, LFContext *context);

  //! Nested generate block error.
  ErrorCode nestedGenerateInstance( veNode ve_item );

  //! Failure elaborating generate statement
  ErrorCode generateElabFail( veNode ve_item );

  //! unsupported instance type error
  ErrorCode unsupportedInstanceType(veNode ve_inst);

  //! Called from NucleusInterraCB from InterraDesignWalker. 
  /*!
    Handles declared assigns at the module level.
  */
  ErrorCode processModuleDeclAssigns(veNode ve_module,
                                     LFContext* context);

  //! Report an unnamed generate blockname with the assigned name
  void unnamedGenerateBlock(veNode ve_node, const char* blockName);

  //! Static function to determine if a veNode is a parameter
  /*!
    Returns true for parameters and localparams.
  */
  static bool sVeNodeIsParameter(veNode ve_ident);
  
  //! Static Function to get a parameter range, if available
  /*!
    Works for parameters,defparam and localparams.
    Returns true if the \a range was defined, false if no size could be determined
  */
  bool sGetParameterRange(veNode ve_ident, ConstantRange* range);

  bool getVisibleName( veNode ve_node, UtString* buf, bool get_orig_name = false);

  TicProtectedNameManager* getTicProtectedNameManager() { return mTicProtectedNameMgr; }

private:
  friend class DesignPopulate;
  //! Fixup an l-value actual for ranged instantiations.(instance arrays LRM 7.1.5)
  /*!
    \param lvalue The lvalue from the port connection, for the whole actual.
    \param formal_width The width of the formal argument.
    \param offset For normal instances, this is NULL.
                  For ranged instantiations, this is the relative offset of the current instantiation,
                  and this will be applied to the given actual lvalue.
    \param result_lvalue For normal instances, this will be the same as the passed-in lvalue.
                         For ranged instantiations, this will be bitselect of the passed-in lvalue.
   */
  ErrorCode lvalueActualFixup(NULvalue *lvalue,
                              UInt32 formal_width,
                              const UInt32 *offset,
                              NULvalue **result_lvalue);

  //! Fixup an r-value actual for ranged instantiations.(instance arrays, LRM 7.1.5)
  /*!
    \param lvalue The rvalue from the port connection, for the whole actual.
    \param formal_width The width of the formal argument.
    \param offset For normal instances, this is NULL.
                  For ranged instantiations, this is the relative offset of the current instantiation,
                  and this will be applied to the given actual lvalue.
    \param result_lvalue For normal instances, this will be the same as the passed-in rvalue.
                         For ranged instantiations, this will be bitselect of the passed-in rvalue.
   */
  ErrorCode rvalueActualFixup(NUExpr *rvalue,
                              UInt32 formal_width,
                              const UInt32 *offset,
                              NUExpr **result_rvalue);

  //! Populate a module instance
  /*!
    This handles ranged instantiations and may actually populate many instances.

    \param ve_inst Cheetah node for the instantiation
    \param context Context of the module doing the instantiation
   */
  //ErrorCode moduleInstance(veNode ve_inst, LFContext *context);

private:
  
  //! helper for processing actual port expressions of a module instance, collects the portconns that are to be connected to a single port 
  /*!
   * this is used to support wacky ports, where a vector is split into individual bits in the formal port list
   * this routine collects the cheetah portconnections that are to be connected to a collapsed wacky port
   * \param the_module pointer to the module that contains the actuals
   * \param ve_portconn pointer to the next element from \a ve_portconn_iter that is to be considered, this is
   *        updated before the return 
   * \param ve_portconn_iter iterator for a list of cheetah nodes that contains all portconnections, this is
   *        updated before the return
   * \param new_module substitute module if it exists
   * \param portMap portmap for \a new_module (if it exists)
   * \param portconns_for_this_port the cheetah veNodes are returned in this list
   * \param port upon return this contains the formal port that is to be used for the nodes in actual_exprs_for_this_port
   */
  ErrorCode gatherPortconnsForSinglePort(NUModule* the_module, veNode *ve_portconn, CheetahList *ve_portconn_iter,
                                         veNode new_module, NUPortMap* portMap, VeNodeList *portconns_for_this_port,
                                         NUNet** port);

  //! The rest of the population routines:
  ErrorCode resolveTaskHierRefs(LFContext *context);
  ErrorCode resolveTaskEnableOrFunctionCallHierRef(NUTaskEnableHierRef *task_enable, veNode ve_task_enable, LFContext *context, NUNet *function_output);
  //! save information about a hierarchical reference to a task or function that must be resolved at end of population
  ErrorCode addTaskHierRefToResolve(veNode ve_task_enable, veNode ve_function_call, NUTaskEnableHierRef *enable, NUScope *scope, NUScope *block_scope, NUNet *function_output);

  //ErrorCode moduleComponents(veNode ve_module, NUModule* module, LFContext* context);

  ErrorCode specifyBlockImplictConnection(veNode ve_sys_time_event, veNode ve_sys_time_delay, LFContext* context, const SourceLocator& loc);

  //! Helper function to create a continuous assign; code consolidation.
  NUContAssign* helperPopulateContAssign(NULvalue *lvalue, NUExpr *rvalue, const SourceLocator& loc,
					 LFContext *context);
  ErrorCode declAssign(veNode ve_net, LFContext* context, NUBlockingAssign **the_assign);

  ErrorCode blockingAssign(veNode ve_net, LFContext* context, NUBlockingAssign **the_assign);
  ErrorCode nonBlockingAssign(veNode ve_net, LFContext* context, NUNonBlockingAssign **the_assign);
  ErrorCode identLvalue(veNode ve_named, LFContext* context, NUModule *module, NUIdentLvalue **the_lvalue);
  ErrorCode bitselLvalue(veNode ve_lvalue, LFContext* context, NUModule* module, NULvalue **the_lvalue);
  ErrorCode partselLvalue(veNode ve_lvalue, LFContext *context, NUModule* module, NUVarselLvalue **the_lvalue);
  ErrorCode concatLvalue(veNode ve_lvalue, LFContext *context, NUModule* module, NUConcatLvalue **the_lvalue);
  ErrorCode lvalue(veNode ve_lvalue, LFContext* context, NUModule* module, NULvalue **the_lvalue);
  ErrorCode netFlags(veNode ve_net, StringAtom* name, LFContext *context, NetFlags *flags);
  ErrorCode netRange(veNode ve_net, ConstantRangeArray *rangeArray);
  ErrorCode arrayRangeDimensions(veNode ve_net, const SourceLocator &loc, ConstantRangeArray *rangeArray);
  ErrorCode findObjectResolution(  LFContext* context, CheetahList& scope_list, UtStack<veNode>& inst_stack, StringAtom*& parent_mod_name, veNode& resolvedObj, SourceLocator& loc );
  void undoElaboration( UtStack<veNode>& inst_stack );
  ErrorCode netHierRefResolve(veNode ve_scopevar, const AtomArray& path, LFContext *context, NUNet **the_net);


  ErrorCode unconnectedPort(veNode ve_inst, NUNet *formal, LFContext *context, NUPortConnection **the_conn);
  ErrorCode tfCommon(NUTF* tf, veNode ve_tf, LFContext* context);

  ErrorCode function(veNode ve_function, LFContext *context, NUTask **the_task_for_func);
  ErrorCode task(veNode ve_task, const char* tfName, LFContext *context, NUTask **the_task);
  ErrorCode functionResult(veNode ve_function, LFContext *context, NUNet **the_net);
  ErrorCode functionResultWidth(veNode ve_function, UInt32* the_width);
  ErrorCode tfArg(veNode ve_arg, LFContext *context, NUNet **the_net);
  ErrorCode tfLocal(veNode ve_var, LFContext *context, NUNet **the_net);
  ErrorCode alwaysBlock(veNode ve_always, LFContext *context, NUAlwaysBlock **the_always, NUExprLocList* expr_loc_list);


  ErrorCode addSensitivityEvents(LFContext* context, NUModule* module, NUAlwaysBlock* always, const NUExprLocList& expr_loc_list, const UtString& reasons);

  ErrorCode structuredProcBlock(veNode ve_stmt, LFContext *context, NUBlock **the_block, bool *isWildCard = NULL);
  ErrorCode sysTask(veNode ve_systask, LFContext* context, NUStmt **the_stmt);
  ErrorCode readMemX(veNode ve_systask, bool hexFormat, LFContext* context, const SourceLocator& loc, NUStmt **the_stmt);
  ErrorCode verilogOutputSysTask(veNode ve_systask, LFContext* context, const SourceLocator& loc, NUStmt **the_stmt);
  ErrorCode verilogFCloseOrFFlushSysTask(veNode ve_systask, bool isClose, LFContext* context, const SourceLocator& loc, NUStmt **the_stmt);
  ErrorCode verilogControlTask(veNode ve_systask, LFContext* context, const SourceLocator& loc, NUStmt **the_stmt);

  ErrorCode cModelModuleParameters(NUCModel* cmodel, veNode ve_module);
  ErrorCode userTask(veNode systask, StringAtom* name, LFContext* context, const SourceLocator& loc, NUStmt** stmt);
  ErrorCode taskCall(veNode ve_systask, NUCModel* cmodel, LFContext* context, const SourceLocator& loc, NUCModelCall** cmodelCall);
  ErrorCode sequentialBlock(veNode ve_block, LFContext *context, NUBlock*);
  ErrorCode caseStmt(veNode ve_case, LFContext *context, NUCase **the_stmt);
  ErrorCode caseItem(veNode ve_case_item, NUExpr* /*sel*/, LFContext* context, veObjType ve_obj_type, bool* hasDefault, size_t maxExprSize, NUCaseItem **the_item);
  ErrorCode disable(veNode ve_disable, LFContext *context, NUStmt **the_stmt);
  ErrorCode eventControl(veNode ve_control, LFContext *context, bool *isWildCard);
  ErrorCode delayOrEventControl(veNode ve_control, LFContext *context, bool *isWildCard);
  ErrorCode maybeFixConditionStmt(NUExpr** cond, SourceLocator& loc);
  ErrorCode ifStmt(veNode ve_if, LFContext *context, NUIf **the_if);
  ErrorCode forStmt(veNode ve_for, LFContext *context, NUFor **the_for);
  ErrorCode whileStmt(veNode ve_while, LFContext *context, NUFor **the_while);
  ErrorCode repeatStmt(veNode ve_while, LFContext *context, NUFor **the_repeat);
  ErrorCode stmt(veNode ve_stmt, LFContext *context, NUBlock* = NULL, bool *isWildCard = NULL);
  ErrorCode taskEnable(veNode ve_task_enable, LFContext *context);
  ErrorCode taskEnableHierRefResolve(veNode ve_scopevar, LFContext *context,
                                     bool looking_for_tasks, AtomArray *hier_path, StringAtom **task);

  ErrorCode partsel (veNode ve_partsel, LFContext * context, NUModule* module, NUNet** net, SInt32* value, NUExpr**row_expr, NUExpr**col_expr, NUExpr**index_expr, ConstantRange** range);
  ErrorCode constantRange(veNode ve_range, ConstantRange **the_range);
  ErrorCode constant(veNode ve_expr, NUConst **the_const);
  ErrorCode identRvalue(veNode ve_named, LFContext *context, NUModule* module, NUExpr **the_expr);
  ErrorCode bitselRvalue(veNode ve_expr, LFContext *context, NUModule* module, NUExpr **the_expr);
  ErrorCode partselRvalue(veNode ve_expr, LFContext *context, NUModule* module, NUExpr **the_expr);
  ErrorCode eventExpr(veNode ve_event, LFContext *context, const SourceLocator &loc, NUExpr **the_expr);
  ErrorCode binaryExpr(veNode ve_expr, LFContext *context, NUModule* module, int lWidth, NUExpr **the_expr);
  ErrorCode cleanupXZBinaryExpr(NUExpr *expr, int lWidth, const SourceLocator &loc, NUExpr **the_expr);
  ErrorCode unaryExpr(veNode ve_expr, LFContext *context, NUModule* module, int lWidth, NUUnaryOp **the_expr);
  ErrorCode condExpr(veNode ve_expr, LFContext *context, NUModule* module, int lWidth, NUTernaryOp **the_expr);
  ErrorCode concatExpr(veNode ve_expr, LFContext *context, NUModule* module, NUExpr **the_op);
  ErrorCode expr(veNode ve_expr, LFContext *context, NUModule* module, int lWidth, NUExpr **the_expr);
  //! A wrapper over expr method, used for mem selects which are either normalized or limited
  //! to 32 bits ..if limitIndex is true. Appropriate warnings are issued on truncation.
  ErrorCode getBitOrArrayBitSelExpr(veNode ve_expr, LFContext* context,
                                    NUModule* module, int lWidth,
                                    SourceLocator& loc, NUMemoryNet* mem,
                                    bool limitIndex, NUExpr** bitSelExpr);

  //! Limit the size of given expression to 32 bits and issue a warning if
  //! it was necessary.
  NUExpr* limitSelectIndexExpr(NUMemoryNet* mem, SourceLocator& loc,
                               NUExpr* selExpr);
  
  //! UDP population.
  enum udpEdgeType { udpPosedge, udpNegedge, udpLevelHigh, udpLevelLow, udpDontCare, udpX };
  ErrorCode udpMakeLhs( veNode ve_udp, LFContext* context, NUIdentLvalue** lhs );
  bool udpLevelListHasX( CheetahList& levelList );
  udpEdgeType udpGetEdgeType( veNode edge );
  ErrorCode udpAddTermToProduct( LFContext* context, NUExpr** product, 
                                 veNode port, bool invert );
  ErrorCode udpLevelEntryProduct( CheetahList& levelList, CheetahList& portList,
    LFContext* context, NUExpr** product );

  //! \return iff the level inputs contain an X
  bool udpLevelsContainX (veNode inputList) const;

  ErrorCode udpEdgeEntry( veNode entry, veNode inputList, CheetahList& portList,
                       LFContext* context, veNode* clkNode, NUExpr** clock,
		      udpEdgeType* edge, NUExpr** enable, bool* isValidEntry );


  bool udpIsBinaryAnd( NUExpr* expr, NUNet** net1, NUExpr** expr1,
		                     NUNet** net2, NUExpr** expr2 );

  bool udpAddLevelLatchStatement( veNode ve_udp, LFContext* context,
                           NUStmt** stmt, NUExpr* setExpr, NUExpr* clearExpr );

  NUExpr* udpAddLevelStatement( veNode ve_udp, LFContext* context,
                                NUStmt** stmt, NUExpr* expr, int value,
				const char* lhsName );
  ErrorCode udpAlways( veNode ve_udp, LFContext* context, NUExpr* clock,
                       udpEdgeType edge, NUExpr* edgeEnable, NUExpr* edgeData,
		       NUExpr* levelSet, NUExpr* levelClear  ); 
  ErrorCode udpSequentialComponents( veNode ve_udp, LFContext* context );
  ErrorCode udpContAssign( veNode ve_udp, LFContext* context,
                           NUContAssign **the_assign );

  //! Process a Verilog 2001 generate statement
  //  ErrorCode generate( veNode ve_generate, LFContext *context );

  //! Process a generate for statement
  //  ErrorCode generateFor( veNode ve_generate, LFContext *context );

  //! Process a generate if statement
  //  ErrorCode generateIf( veNode ve_generate, LFContext *context );

  //! Process a generate case statement
  //  ErrorCode generateCase( veNode ve_generate, LFContext *context );

  //! Figure out what this item is and populate it
  //  ErrorCode generateItem( veNode ve_item, LFContext *context );

  //! Populate all the hierarchical references on the given list, place them onto the current NUModule.
  ErrorCode scopeVariableList(CheetahList& ve_iter, LFContext *context);

  //! Helper method used to determine the repeat count of a concat.
  ErrorCode fetchConcatRepeatCount(veNode ve_expr, const SourceLocator & loc, UInt64 * repeat_count);

  //! Generate a constant expression that will mask away any don't-care bits.
  /*!
   * Process a case condition and generate a mask for don't-care bits.
   * All the real work is done by ::casexMaskRecurse(). After the mask
   * constant has been determined, convert that into an NUConst
   * object.
   *
   * \param ve_expr     The Cheetah node representing the case condition.
   * \param context     The LocalFlow context.
   * \param loc         SourceLocator for this case condition.
   * \param isCaseX     Are we generating a mask for a casex condition? If so, allow 'x' tokens.
   * \param maxExprSize The allowed size of our condition (comes from the select size).
   * \param the_expr    The generated mask, or NULL if one is not needed.
   *
   * \return success/failure status.
   *
   * \sa casexMaskRecurse
   */
  ErrorCode casexMask(veNode ve_expr, LFContext *context, const SourceLocator& loc, bool isCaseX, size_t maxExprSize, NUExpr **the_expr);

  //! Generate a constant expression that will mask away any don't-care bits.
  /*!
   * Recursively process the subexpressions of a case condition and
   * generate a mask for don't-care bits. Constant expressions are
   * analyzed for don't-care bits. Concats are recursively analyzed.
   * Any other expression is assumed to be care.
   *
   * \param ve_expr     The Cheetah node representing some sub-expression of the case condition.
   * \param context     The LocalFlow context.
   * \param loc         SourceLocator for this case condition.
   * \param isCaseX     Are we generating a mask for a casex condition? If so, allow 'x' tokens.
   * \param min_bit     The lowest bit this sub-expression controls.
   * \param max_bit     The highest bit this sub-expression controls.
   * \param mask        DynBitVector containing our mask; updated as masking is determined.
   *
   * \return success/failure status.
   *
   * \sa casexMask
   */
  ErrorCode casexMaskRecurse(veNode ve_expr, LFContext *context, const SourceLocator& loc, bool isCaseX, size_t min_bit, size_t max_bit, DynBitVector * mask);

  ErrorCode functionCall(veNode ve_function_call, LFContext *context, NUExpr **the_expr);
  ErrorCode addOutputVarForFunctionAsTask(veNode ve_function, LFContext *context, const SourceLocator& loc, NUNet **actual_net);

  ErrorCode sysFunctionCall(veNode ve_function_call, LFContext *context,
                            NUModule* module, int lWidth, NUExpr **the_expr);

  ErrorCode tfArgConnection(veNode ve_arg, NUNet *formal_net, NUTF *tf, LFContext *context, NUTFArgConnection **the_conn);
  // connect formals to actuals for 1) a task enable or 2 a function call converted to a task enable
  ErrorCode tfArgConnections(veNode ve_task_enable_or_function_call, NUTask *task,
                             LFContext *context, NUTFArgConnectionVector *arg_conns,
                             NUNet *actual_for_fct_output);

  /*!
    \brief convert \a ve_expr_iter into a vector of expressions

    utility function shared by concats and system function calls
    \warning Side effect: the list \a ve_expr_iter is cleared before
    return.

    \todo This function should be made aware of null arguments (such
   as found in instance argument lists, or $display arguments)
   */
  ErrorCode exprVec(CheetahList& ve_expr_iter, LFContext* context,
                    NUModule* module, int lWidth, NUExprVector* expr_vector);

  void createParamModuleMap(const char* fileRoot, MsgContext* msgContext);
  void destroyParamModuleMap();

  //! convert 
  ErrorCode scopeVariableToPath(veNode ve_scopevar, AtomArray* path);


  //! Support classes
  TicProtectedNameManager* mTicProtectedNameMgr;
  bool mAllowHierRefs;

  //! Population mappings
  typedef UtMap<UtString, veNode> StringVeNodeMap;
  typedef UtMap<NUModule*, StringVeNodeMap > ModuleStringNodeMap;
  typedef UtMap<veNode, NUTask*> VeNodeTaskMap;
  typedef UtMap<NUModule*, VeNodeTaskMap > ModuleNodeTaskMap;
  typedef UtMap<NUNetHierRef*,VeNodeNamePairList> NetHierRefVeNodeNamePairListMap;
  ModuleStringNodeMap mModuleHiernameNodeMap;
  ModuleNodeTaskMap mTaskMap;
  NetHierRefVeNodeNamePairListMap mNetPossibleResolutions;

  //! Keep track of hierarchical task enables which we must resolve post-population
  class TaskHierRefInfo
  {
  public:
    //TaskHierRefInfo() : mVeTaskEnable(0), mVeFunctionCall(0), mScope(0), mBlockScope(0), mFunctionOutput(0) {}
    TaskHierRefInfo(veNode task_enable, veNode function_call, NUScope *scope,
                    NUScope *block_scope, NUNet *function_output,
                    NUTaskEnableHierRef* ena)
      : mVeTaskEnable(task_enable), mVeFunctionCall(function_call),
        mScope(scope), mBlockScope(block_scope), mFunctionOutput(function_output),
        mEnable(ena)
    {}
    veNode mVeTaskEnable;     // if non-null then this is for a hier-ref for a task enable
    veNode mVeFunctionCall;   // if non-null then this is for a hier-ref for a function call
    NUScope *mScope;
    NUScope *mBlockScope;
    NUNet   *mFunctionOutput;   // used only when mVeFunctionCall is
                                // non-null, points to net that will
                                // hold the the output of the function
    NUTaskEnableHierRef* mEnable;
  };

  // We need both a vector and a set because as we fixup hier ref resolutions
  // for task arguments, we may encounter new resolutions.  We can iterate
  // over a vector as we are adding to it, but iterating over a set/map is hard.
  //
  // I used a vector of pointers because that has better performance than
  // a vector of TaskHierRefInfo objects, and also is not dependent on the
  // auto-generated copy ctor which really just does a memcpy.
  typedef UtHashSet<NUTaskEnableHierRef*> TaskHierRefSet;
  typedef UtVector<TaskHierRefInfo*> TaskHierRefVec;
  TaskHierRefSet mTaskHierRefSet;
  TaskHierRefVec mTaskHierRefVec;

  typedef UtHashMap<veNode,NUBlock*> BlockMap;
  BlockMap mBlockMap;


  CheetahContext* mCheetahContext;

  //! Structure for deferring sensitivity processing information.
  //! When populating combinational always blocks, we discover the need 
  //! to synthesize change-detect logic due to the presence in the always
  //! blocks of side-effecting c-model calls or system tasks.  To synthesize
  //! this logic we need to use NUDesignWalker on the always block, which
  //! cannot safely be done until the end of population, when hierarchical
  //! reference resolution and other cleanups have occurred.  The sensitivity
  //! list gets populated from VerilogPopulate::addAlwaysBlock, and it
  //! gets processed at the end of VerilogPopulate::postDesignFixup.
  SensitivityInfoList mSensitivityInfoList;

  class ParamModuleMap;
  friend class ParamModuleMap;
  ParamModuleMap* mParamModuleMap;

  HdlVerilogPath* mHdlVerilogPath;
  bool mUseLegacyGenerateHierarchyNames;

};

#endif
