// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef NUCLEUSSANITY_H_
#define NUCLEUSSANITY_H_

#include "compiler_driver/CarbonContext.h"

class NUDesign;
class NUModule;

//! NucleusSanity class
class NucleusSanity {
public:

  enum SanityStatus { eBeforeResynth, eAfterResynth, eAfterSchedule };

  //! ctor
  NucleusSanity();

  //! dtor
  ~NucleusSanity();

  //! perform sanity on a design
  void design(NUDesign*, SanityStatus);

  //! perform sanity on a module
  void module(NUModule*, SanityStatus);
private:
};

#endif
