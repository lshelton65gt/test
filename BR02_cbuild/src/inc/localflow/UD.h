// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef UD_H_
#define UD_H_

#include "flow/Flow.h"
#include "nucleus/NUNetRefMap.h"
#include "util/Graph.h"
#include "util/GenericDigraph.h"
#include "util/GraphSCC.h"

class MsgContext;
class IODBNucleus;
class ArgProc;
class SCCGraph;

/*!
  \file
  Declaration of use-def package.

  In this and related files the following definitions should be understood.

  \par Definition: use (n)
   A variable present in the definition of another.

  \par Definition: uses (n)
   The set of all variables present in the definition of a variable.

  \par
   Example1:

   \code
    A = B;
   \endcode

  \par
   The definition of 'A' contains a \b use of 'B'.

  \par
   Example2:

   \code
    A = B & C;
   \endcode

  \par
   The definition of 'A' contains \b uses for 'B' and 'C'.

  \note
   We do \b not mean:

  \par
  Definition: use (v)
  \par  
  Locations where a variable is referenced. 
 */

//! Class to perform Use/Def computation
/*!
 * This class is able to compute use/def information on a per-construct
 * basis, and is also able to walk whole modules.
 *
 * \sa The documentation for UD.h for definitions of the terms \b use,
 * \b uses and \b defs.
 */
class UD
{
public:
  //! constuctor
  /*
    \param verbose If true, will be verbose about UD creation.
   */
  UD(NUNetRefFactory *netref_factory,
     MsgContext *msg_ctx,
     IODBNucleus *iodb,
     ArgProc* args,
     bool verbose);

  //! destructor
  ~UD();

  //! Compute u/d information over this module.
  /*!
   * Iterate through all the different top-level drivers which exist
   * in a module.  For each driver, compute its u/d information, and
   * populate this module's u/d information based on that.
   *
   */
  void module(NUModule *module, bool computeModuleUD=true);

  //! Update UD information for the ext-net.
  /*!
   * This method is not part of the normal UD processing. It is meant
   * to update ext-net UD information late in a compile -- without
   * updating UD for the entire module.
   *
   * Generally, this method should be called after protected nets have
   * been added.
   *
   * \sa updateExtNet
   * \sa moduleInstanceUpdateExtNet
   */
  void moduleUpdateExtNet(NUModule * module);

  //! Compute u/d information over this task or function
  /*!
   * Perform syntax directed use-def analysis of the task or function.
   *
   * \param force If true, UD is locally computed even if this task
   * was previously encountered. The force flag is not applied to
   * tasks called within the provided task.
   */
  void tf(NUTF *tf,bool force=false);

  //! Compute u/d information for this structured proc.
  /*
   */
  void structuredProc(NUStructuredProc *proc);

  //! Compute u/d information for the block.
  void block(NUBlock *block);

  //! Compute u/d information for the given statement.
  /*!
   * Handle block statements.  All other statements are a no-op; they compute
   * u/d info when you ask them.
   */
  void stmt(NUStmt *stmt);

  // Types to create an acycle graph from the flow graph
  typedef GenericDigraph<void*, FLNode*> FlowGraph;
  typedef FlowGraph::Node FlowNode;
  typedef FlowGraph::Edge FlowEdge;
  typedef UtMap<FLNode*, FlowNode*> FlowNodeMap;
  typedef std::pair<FlowNode*,FLNode*> FlowFLNodeEdge;
  typedef UtList<FlowFLNodeEdge> EdgeQueue;
  typedef UtHashMap<FLNode*,NUNetRefSet> FlowRefMap;

  //! true if the UD for flowNode will be taken from the results of the the UDCreationWalker traversal
  bool isUDNeededForTransfer(FlowNode* flowNode);

private:
  //! Hide copy and assign constructors.
  UD(const UD&);
  UD& operator=(const UD&);

  //! Compute u/d information for this module instance.
  /*!
   * The module instance is a top-level continuous driver of nets; use
   * the instantiated module's u/d map, but translate formal nets to
   * actual nets.
   *
   */
  void moduleInstance(NUModuleInstance *inst);

  //! Compute u/d information for an output port connection of this module instance.
  /*!
   * An output port connection is any connection which drives a value out of
   * the module, so bidirect connections are handled here also.
   */
  void moduleInstanceOutput(NUModuleInstance *inst, NUPortConnection *conn);

  //! Compute u/d information for this continuous assign.
  /*!
   * For now, this is a no-op; assign statements can compute their own
   * u/d information when it is asked-for.
   */
  void contAssign(NUContAssign *assign);

  //! Compute u/d information for this enabled driver.
  /*!
   * For now, this is a no-op; enabled drivers can compute their own
   * u/d information when it is asked-for.
   */
  void enabledDriver(NUEnabledDriver *driver);

  //! Compute u/d information for this trireg initialization.
  /*!
   * For now, this is a no-op; trireg initializations can compute their own
   * u/d information when it is asked-for.
   */
  void triRegInit(NUTriRegInit *init);

  //! Compute u/d information for the given statements.
  /*!
    \param stmt_list The list of statements to analyze.
    \param overall_blocking_ud_map Map of def-nets to use-nets for blocking assigns
    \param overall_non_blocking_ud_map Map of def-nets to use-nets for non-blocking assigns.
    \param overall_blocking_kill_set Set of nets which have defs killing any preceeding blocking defs in this statement list.
    \param overall_non_blocking_kill_set Set of nets which have defs killing any preceeding non-blocking defs in this statement list.

   * Perform syntax directed use-def analysis of the list of statements.
   * Iterate through the statements:
   *  . compute u/d information for the individual statement
   *  . construct data flow graph nodes for each def of the statement,
   *    hooking up uses to defs encountered
   *  . the statement's def may either be a kill or a partial def.  A kill
   *    means that this new def overwrites all previous defs of that net.
   *  . a live set is maintained which contains the currently-live defs of
   *    all nets define so far.
   *
   * At the end, the live set defines the def set for this statement list.
   * Walk the data flow graph we constructed to determine u/d info for this
   * statement list.
   *
   * Note: I *think* we do *not* need to construct a control flow graph to
   * get this correct.  The difficult case is loops, but I think we can
   * do syntax directed analysis for that also.
   */
  void stmtList(NUStmtList *stmt_list,
		NUNetRefNetRefMultiMap *overall_blocking_ud_map,
		NUNetRefNetRefMultiMap *overall_non_blocking_ud_map,
		NUNetRefSet *overall_blocking_kill_set,
		NUNetRefSet *overall_non_blocking_kill_set);

  //! Compute u/d information for the case statement.
  void caseStmt(NUCase *stmt);

  //! Compute the dependency information for an if statement.
  /*! Defs:
   *  An if() statement defines a net if it is defined in either
   * branch. The set of definitions for an if() statement is the union
   * of definitions from both branches.
   *
   * Uses:
   *
   * A net is used to define another if it appears in the condition of
   * the if() statement or it is a use of that net in either
   * branch. For any given definition within the if() statement, its uses
   * are the union of uses from both branches and the condition. 
   *
   * Kills:
   *
   * A net is killed by an if() statement if it is killed by both
   * branches of the if() statement.
   */ 

  void ifStmt(NUIf *stmt);

  //! Compute u/d information for the for statement.
  /*!
   * The u/d information for a looping statement is computed by an
   * iterative algorithm (because outputs may feed back into a
   * subsequent loop).
   *
   * A 'for' statement only kills a def if we can prove that all paths
   * through the loop are kills. In practice, this means only the
   * indexing variables are killed.
   */
  void forStmt(NUFor *stmt);

  //! Compute u/d information for a c-model call
  void cmodelCall(NUCModelCall* cmodel_call);

  //! Compute u/d information for a system control task
  /*!
   * This also clears the controlflow live map and then adds just the $cfnet
   */
  void controlSysTask(NUControlSysTask* control_sys_task);

  //! Compute u/d information for the task enable statement.
  void taskEnable(NUTaskEnable *task_enable);

  //! manage local cache of FLNodeFactories, because creating them is slow
  FLNodeFactory* allocFlowNodeFactory();

  //! manage local cache of FLNodeFactories, because creating them is slow
  void freeFlowNodeFactory(FLNodeFactory*);


  //! Local typedefs to assist the local helper functions
  typedef void (NUUseDefStmtNode::* GetDefFunction)(NUNetRefSet *def_set) const;
  typedef void (NUUseDefStmtNode::* GetUsesFunction)(const NUNetRefHdl &def_net_ref, NUNetRefSet *use_set) const;
  typedef void (NUUseDefNode::* PutDefFunction)(const NUNetRefHdl &def_net_ref);
  typedef void (NUUseDefNode::* PutUseFunction)(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);
  typedef void (NUBlockStmt::* StmtPutDefFunction)(const NUNetRefHdl &def_net_ref);
  typedef void (NUBlockStmt::* StmtPutKillFunction)(const NUNetRefHdl &kill_net_ref);
  typedef void (NUBlockStmt::* StmtPutUseFunction)(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);
  typedef void (NUTF::* TFPutDefFunction)(const NUNetRefHdl &def_net_ref);
  typedef void (NUTF::* TFPutKillFunction)(const NUNetRefHdl &kill_net_ref);
  typedef void (NUTF::* TFPutUseFunction)(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);
  typedef void (NUBlock::* BlockPutDefFunction)(const NUNetRefHdl &def_net_ref);
  typedef void (NUBlock::* BlockPutKillFunction)(const NUNetRefHdl &kill_net_ref);
  typedef void (NUBlock::* BlockPutUseFunction)(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref);


  //! Class to walk the acyclic module flow and compute module UD
  /*! This class computes module UD by walking a strongly connected
   *  component (SCC) graph created from the module flow
   *  graph. Because it is an SCC graph it is acyclic. This makes the
   *  walk more predictable.
   *
   *  The walk gathers module uses (bound nodes) at the leaves of the
   *  graph and propogates them to the their fanout towards module
   *  outputs and observability points.
   *
   *  A basic premise to this algorithm is that if we have a strongtly
   *  connected component, then the use set for every flow node in
   *  that component is the same. That is because every flow node in
   *  the SCC is reachable from every other flow node in the SCC.
   *
   *  The general graph package provides various callback routines
   *  that are overridden to process the data during the graph
   *  walk. These are:
   *
   *  1. visitNodeBefore: this routine allows the walk to get
   *     initialized. In this case each graph node allocates an
   *     NUNetRefSet to store the uses for the node and its fanout.
   *
   *  2. visitNodeAfter: this routine gets called after all its
   *     fanin's visitNodeAfter has been called. It is responsible
   *     for merging in that last fanin's use data and updating the
   *     mLastChildNode so that the parent will be able to merge the
   *     data from its child (the node currently being visited)
   *
   *  3. visitNodeBetween: this routine gets called between two
   *     fanin's visitNodeAfter has been called. It is responsible
   *     for gathering the last visitNodeAfter's data and adding it to
   *     the current node.
   *
   *  4. visitCrossEdge: this routine gets called when we hit a node
   *     that has already been hit before so calling visitNodeAfter is
   *     not appropriate. It is responsible for updating the
   *     mLastChildNode pointer (see below)
   *
   *  5. visitBackEdge: there should not be any back edges because
   *     they indicate a cycle in the graph. This routine is overriden
   *     to guarantee that an SCC graph is passed in.
   *
   *  The mLastChildNode field is used to pass use set information
   *  between fanins and fanouts. It is set to point to the most
   *  recently visited node at various points in the graph walk. 
   *
   */
  class UDCreationWalker : public GraphWalker
  {
  public:
    // this walker has multiple phases, setup must come first, and teardown last.
    enum UDCreationWalkerPhase
    {
      eUDCreationWalkerSetup,   /*!< post-construction, but before any use*/
      eUDCreationWalkerWork,    /*!< this is where the work is done, currently there is only a single work phase */
      eUDCreationWalkerTeardown /*!< post-use, but before any destruction */
    };
    struct UDCreationWalkerAuxData
    {
      //! constructor
      UDCreationWalkerAuxData(NUNetRefSet* uses, SInt32 initial_count) :
        mNetRefSet(uses), mVisitsRemaining(initial_count)
      {}
      //! destructor
      UDCreationWalkerAuxData()
      {}                        // destructor is empty because memory is managed outside of this class
      
      void incrVisitsRemaining(){
        ++mVisitsRemaining;
      }
      void decrVisitsRemaining(){
        --mVisitsRemaining;
        INFO_ASSERT((mVisitsRemaining >= 0),"Consistency check failure in graph walker.");
      }
      SInt32 getVisitsRemaining(){
        return mVisitsRemaining;
      }
      NUNetRefSet* getNetRefSet(){
        return mNetRefSet;
      }
      void putNetRefSet(NUNetRefSet* netRefSet){
        mNetRefSet = netRefSet;
      }

      NUNetRefSet* mNetRefSet;
      SInt32       mVisitsRemaining;
    private:
      UDCreationWalkerAuxData(const UDCreationWalkerAuxData&);  // forbid -- copy constructor
      UDCreationWalkerAuxData& operator=(const UDCreationWalkerAuxData&);  // forbid -- copy assignment
    };

    //! constructor
    UDCreationWalker(GraphSCC* graphSCC, FlowGraph* flowGraph, NUModule* module,
                     UD* the_ud, NUNetRefFactory* netRefFactory) :
      mGraphSCC(graphSCC), mFlowGraph(flowGraph), 
      mUDCreationWalkerPhase(eUDCreationWalkerSetup), mModule(module),
      mTheUD(the_ud), mNetRefFactory(netRefFactory), mLastChildNode(NULL)
    {}

    //! Override the visitNodeBefore to set the initial data
    /*! This allocates space for this nodes use set and resets the
     *  mLastChildNode to NULL so that the fanins can start using it
     *  to pass data to their parents.
     */
    Command visitNodeBefore(Graph* graph, GraphNode* node);

    //! Override the visitNodeAfter to merge fanin data and copy to last child
    /*! Gather any uses from the last fanin that left a pointer to
     *  data in mLastChildNode and also update mLastChildNode so that
     *  it can be used by the parent of the current node.
     */
    Command visitNodeAfter(Graph* graph, GraphNode* node);

    //! Override the visitNodeBetween to merge fanin data
    /*! Gather any uses from the last fanin that stored its data in
     *  mLastChildNode.
     */
    Command visitNodeBetween(Graph* graph, GraphNode* node);

    //! Override the visitCrossEdge to copy to last child
    /*! Update the mLastChildNode so that the fanout of this node can
     *  use it later.
     */
    Command visitCrossEdge(Graph* graph, GraphNode* node, GraphEdge* edge);

    //! Override the visitBackEdge to make sure there are no cycles
    /*! Adds an assert that will fire if there are any back edges
     *  (cycles) in the graph.
     */
    Command visitBackEdge(Graph* graph, GraphNode* node, GraphEdge* edge);

    //! Override to start at outputs only so we only walk live part of graph.
    bool isValidStartNode(Graph* graph, GraphNode* node);

    //! advance the phase
    void advanceWalkerPhase();

    //! returns the netRefSet associated with node, or NULL if none avaliable
    NUNetRefSet* getUsesFromGraph(Graph* graph, GraphNode* node);

    //! decrement refcount and relase space if no longer needed
    void finishedWithUses(Graph* graph, GraphNode* node);

    UDCreationWalkerAuxData* getAuxGraphData(Graph* graph, GraphNode* node) {
      return reinterpret_cast<UDCreationWalkerAuxData*>(graph->getScratch(node));
    }

  private:

    GraphSCC* mGraphSCC;        // the graph walker that we are using
    FlowGraph* mFlowGraph;
    UDCreationWalkerPhase mUDCreationWalkerPhase; // the phase of the walker
    NUModule* mModule;
    UD* mTheUD;
    NUNetRefFactory* mNetRefFactory;

    // the graph node that was the last visited child, (its use set is up-to-date)
    GraphNode*   mLastChildNode;

    // A routine to take the last fanin data from mLastChildNode and
    // add it to the current graph nodes use set.
    NUNetRefSet* mergeFaninValue(Graph* graph, GraphNode* node);

    //! returns true if node represents a flownode for one or more output(s)
    bool isNodeForAnOutput(Graph* graph, GraphNode* node);
  };

  //! Create bound nodes for all nets in the module which are protected mutable.
  void helperCreateBoundsForProtected(NUModule *module,
				      NUNetRefFLNodeMultiMap &def_map,
				      FLNodeFactory* dfg_factory);

  //! Connect live defs of all protected observable nets to the module's ExtNet
  /*!
   * We collect the observable nets into the ExtNet so that we can find
   * all the fanin to observable nets when we handle an instantiation of
   * this module.
   * See NUExtNet class doc for more info.
   */
  void helperConnectExtNet(NUModule *module,
			   NUNetRefFLNodeMultiMap &use_map,
			   NUNetRefFLNodeMultiMap &def_map,
			   FLNodeFactory* dfg_factory);

  //! Connect fanin of \a child_virt_net (within \a child_inst) which is seen through port actuals, to the \a parent_virt_net
  /*!
   * This routine keeps any actuals which are connected to child_virt_net in the
   * child module alive in the parent module.
   * See NUExtNet class doc for more info.
   */
  void moduleInstanceConnectVirtNetFanin(NUModuleInstance *child_inst,
                                         NUNet* child_virt_net,
                                         NUNet* parent_virt_net);

  //! Promote ext-net uses from a module instance to parent module.
  /*!
   * \sa moduleUpdateExtNet
   */
  void moduleInstanceUpdateExtNet(NUModule * parent_module, NUModuleInstance * child_inst);
  
  //! Connect local protected nets as ext-net uses.
  /*!
   * \sa moduleUpdateExtNet
   */
  void updateExtNet(NUModule * module);


  //! Start at the formals, get the corresponding actuals, and add them as fanin to the given actual
  void addActualUsesFromFormals(NUModuleInstance *inst,
				NUNetRefHdl &actual_def_ref,
				NUNetRefSet &formal_uses);

  // process a statement within the context of a stmtlist or for().
  void oneStmt(NUStmt *one_stmt,
               NUNetRefSet *overall_blocking_kill_set,
               NUNetRefSet *overall_non_blocking_kill_set,
               FLNodeFactory* dfg_factory,
               NUNetRefFLNodeMultiMap &blocking_live_map,
	       NUNetRefFLNodeMultiMap &non_blocking_live_map,
               NUNetRefSet *additional_uses=NULL);

  //! Process a statement list without performing any map updates.
  void stmtListInternal(NUStmtList * stmt_list,
                        NUNetRefSet *overall_blocking_kill_set,
                        NUNetRefSet *overall_non_blocking_kill_set,
                        FLNodeFactory* dfg_factory,
                        NUNetRefFLNodeMultiMap &blocking_live_map,
                        NUNetRefFLNodeMultiMap &non_blocking_live_map,
                        NUNetRefSet * additional_uses=NULL);

  //! translate a livemap to UD information
  void helperStmtLiveToUD(NUNetRefNetRefMultiMap *overall_blocking_ud_map,
			  NUNetRefNetRefMultiMap *overall_non_blocking_ud_map,
			  NUNetRefFLNodeMultiMap &blocking_live_map,
			  NUNetRefFLNodeMultiMap &non_blocking_live_map);

  /*!
   * Construct the u/d information, put into the given u/d maps.
   * Every net in the live set represents a net which this statement
   * list defines.
   *
   * Place a 0 in the Use set for each net we def to cover those cases
   * where a net is def'd by a constant.
   *
   * TBD: when do block locals, don't put block locals into the u/d maps.
   *
   * TBD: refactor the blocking/non-blocking code into single f'n()
   */
  void helperLiveMapToUDMap(NUNetRefFLNodeMultiMap &live_map,
			    NUNetRefNetRefMultiMap &ud_map);

  //! Connect fanin for the given flow node
  /*!
   * The use_net_ref is the fanin for the flow node, connect defs of that which
   * are in the def_map to the flnode.
   *
   * If any bits of use_net_ref are not def'd, create a bound node for the
   * use_net.
   */
  void helperConnectFlowFanin(FLNode *flnode,
			      const NUNetRefHdl& use_net_ref,
			      NUNetRefFLNodeMultiMap &def_map,
 			      NUNetRefSet *kill_set,
			      FLNodeFactory* dfg_factory);

  //! Create flow nodes for the defs in def_set.  Used to process a single statement.
  void helperCreateDFGNodesForStmtDefs(NUNetRefFLNodeMultiMap &def_map,
				       const NUNetRefSet &def_set,
				       NUNetRefFLNodeMultiMap &live_map,
				       NUNetRefSet *kill_set,
				       NUStmt *stmt,
				       GetUsesFunction get_uses,
				       FLNodeFactory* dfg_factory,
                                       NUNetRefSet *additional_uses=NULL);

  //! Create flow nodes for defs in loop_def_map.  Used with for loops.
  void helperCreateDFGNodesForLoopDefs(NUNetRefFLNodeMultiMap &def_map,
				       const NUNetRefNetRefMultiMap &loop_def_map,
				       NUNetRefFLNodeMultiMap &live_map,
				       NUNetRefSet *kill_set,
				       NUStmt *stmt,
				       FLNodeFactory* dfg_factory);

  //! Transfer the use/def information from one use/def node to another using the given functions
  void helperTransferUD(NUUseDefStmtNode *from,
			GetDefFunction get_defs,
			GetUsesFunction get_uses,
			NUUseDefNode *to,
			PutDefFunction put_def,
			PutUseFunction put_use);

  //! Transfer the netref->flow infomation from one map to another.
  void helperTransferMapToMap(const NUNetRefFLNodeMultiMap &from,
                              NUNetRefFLNodeMultiMap &to);

  //! Create flow nodes for the given continuous driver node, and add to the u/d maps.
  void helperCont(NUNetRefFLNodeMultiMap &def_map,
		  NUNetRefFLNodeMultiMap &use_map,
		  NUUseDefNode *node,
		  FLNodeFactory* factory);

  //! Add the given kill information from the set to the specified blockstmt using the provided function
  void helperAddBlockStmtKills( const NUNetRefSet &from,
                                NUBlockStmt* to,
                                StmtPutKillFunction put_kill);

  //! Add the given use/def information from the map to the use/def node using the given functions
  void helperAddStmtUD(const NUNetRefNetRefMultiMap &from,
		       NUBlockStmt* to,
		       StmtPutDefFunction put_def,
		       StmtPutUseFunction put_use,
		       NUNetRefSet *other_uses = 0);

  //! Add the given kill information from the set to the specified TF using the provided function
  void helperAddTFKills( const NUNetRefSet &from,
                         NUTF* to,
                         TFPutKillFunction put_kill);

  //! Add the given use/def information from the map to the use/def node using the given functions
  void helperAddTFUD(const NUNetRefNetRefMultiMap &from,
		     NUTF* to,
		     TFPutDefFunction put_def,
		     TFPutUseFunction put_use);

  /*!
   * Determine what piece of the given bid is not killed by the tf, and create a
   * dependency of that bid to itself.
   */
  void helperTFBid(NUNet *bid,
		   NUTF *tf,
		   NUNetRefSet &kill_set,
		   TFPutDefFunction put_def,
		   TFPutUseFunction put_use);

  /*!
   * Handle bid task arguments - any bids which are not completely def'd depend on the
   * incoming value.
   */
  void helperTFBids(NUTF *tf);

  //! Add the given kill information from the set to the specified block using the provided function
  void helperAddBlockKills(const NUNetRefSet &from, NUBlock *to, BlockPutKillFunction put_kill);

  //! Add the given use/def information from the map to the use/def node using the given functions
  void helperAddBlockUD(const NUNetRefNetRefMultiMap &from,
                                 NUBlock *to,
                                 BlockPutDefFunction put_def,
                                 BlockPutUseFunction put_use);

  //! Compute the UD info for a task enable output.
  void taskEnableArgOutput(NUTaskEnable *task_enable, int conn_idx, NULvalue *actual);

  //! Add the given defs in actual_def_set to the def set for the task_enable and add their fanin to the uses
  /*!
   * This routine is used for both task arg connections and non-local task defs.
   *
   \param task_enable The task enable to add the def to.
   \param formal_net_ref The def to trace back in the task enable.
   \param actual_def_set The set of defs to apply to the task enable.
   \param additional_uses Used for arg connections, any uses on the formal (ie, 'foo' in 'bar[foo]').
   */
  void taskEnableDef(NUTaskEnable *task_enable,
                     NUNetRefHdl formal_net_ref,
                     const NUNetRefSet &actual_def_set,
                     const NUNetRefSet *additional_uses=0);

  //! Given a net ref which a task uses or defs, return the net ref to use for the task enable.
  /*!
   * If the task enable is a hierarchical reference, and the net ref is non-local
   * to the referred-to task, then we need to return the appropriate hierarchical
   * net reference.
   */
  NUNetRefHdl taskEnableGetNetRef(NUTaskEnable *task_enable,
                                  const NUNetRefHdl &net_ref);

  /*! Populate the module level UD. This creates a directed acyclic
   *  graph from the flow graph and then walks it to see what module
   *  inputs affect what module outputs.
   */
  void populateModuleUD(NUModule *module,
                        FLNodeFactory *dfg_factory,
                        NUNetRefFLNodeMultiMap &def_map);

  //! Create a directed graph from the flow graph
  void populateGraph(EdgeQueue* edge_queue, FlowGraph* flowGraph,
                     FLIterFlags::TraversalT stop = FLIterFlags::eAll);

  //! Apply the gathered module UD to the modules multimap
  void applyModuleUseDef(NUModule* module, Graph* graph, FlowGraph* flowGraph, GraphSCC* graphSCC,
                         UDCreationWalker* walker, GraphNode* node);

  //! Return true if have visited this scope.
  bool haveVisited(NUScope *scope) const;

  //! Remember that have visited this scope.
  void rememberVisited(NUScope *scope);

  //! Message context.
  MsgContext *mMsgContext;

  //! IODB (has protected information)
  IODBNucleus *mIODB;

  //! Netref factory
  NUNetRefFactory *mNetRefFactory;

  //! Be verbose as processing occurs?
  bool mVerbose;

  //! Set of scopes which we have visited so far.
  UtHashSet<NUScope*>* mScopeSet;

  /*! \brief the current control flow live set
   *
   * The netrefs in this set are considered live for control flow analysis
   *
   * A pointer is used here to allow the set to be replaced with a
   * temp set when necessary.
   */
  NUNetRefSet *mCurCFLiveSet;

  /*! \brief This is incremented each time a control flow operation is
   *  seen during UD analysis.
   *
   * Used to determine when to set the associated flag on the statement.
   *  \sa NUStmt::getUsesCFNet()
   */
  SInt32 mNumSeenControlFlowOperations;

  //! Arguments passed into cbuild
  ArgProc* mArgs;

  //! multiple level hierarchical tasks
  bool mMultiLevelHierTasks;

  //! cache of free FLNodeFactories, because creating & destroying them is slow
  typedef UtVector<FLNodeFactory*> FLNodeFactoryVector;

  //! cache of free FLNodeFactories, because creating & destroying them is slow
  FLNodeFactoryVector* mFreeFlowNodeFactories;
};

#endif
