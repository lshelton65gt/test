// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef DESIGNPOPULATE_H_
#define DESIGNPOPULATE_H_
//! \file DesignPopulate.h
/*! This file provides a place for data that needs to be shared between
  VerilogPopulate and VhdlPopulate.  There will be one DesignPopulate
  object created during population.  It contains a VerilogPopulate and a
  VhdlPopulate object.
*/
#include "localflow/VerilogPopulate.h"
#include "localflow/VhdlPopulate.h"
#include "compiler_driver/MsgCountMgr.h"

class Fold;
class ArgProc;
class TicProtectedNameManager;

//! Provides an Interra net to NUNet mapping
typedef UtMap<mvvNode, NUNet*> MvvNodeNetMap;

//! Provides an Interra port node to NULvalue mapping
typedef UtMap<mvvNode, NULvalue*> MvvNodeLvalueMap;

//! provides a NUNamedDeclarationScope to NUCompositeNet mapping for net lookup
typedef UtMap<NUNamedDeclarationScope*, NUCompositeNet*> NUScopeNetMap;

//! Maps a NUScope (NUModule or NUNamedDeclarationScope), to a map for  Interra port or net node, and a NUNet together
/*! The name ScopeToNodeNetMap should really be ScopeNodeNetMap but that name
 * is used in VhdlPopulate.h.  The plan is to eliminate the need for the VHDL
 * version of the map and only use the map that is defined here
 */
typedef UtMap<NUScope*, MvvNodeNetMap> ScopeToNodeNetMap;

//! Maps a NUScope (NUModule or NUNamedDeclarationScope), to a map of Interra ports -> NULvalues
typedef UtMap<NUScope*, MvvNodeLvalueMap> ScopeToNodeLvalueMap;

//! Maps a NUNet to a Constraint Range
typedef UtMap<NUNet*, ConstantRange> NUNetToConstraintRangeMap;

class InterraDesignWalker;
class NucleusInterraCB;

//! DesignPopulate class
/*!
 * Class to walk the Cheetah and/or Jaguar trees and populate Nucleus.
 *
 * In order to be able to populate a mixed-language design, information
 * needs to be shared between the Verilog and VHDL worlds.  In
 * particular this comes into play at the interface between languages.
 * This class contains the data that needs to be shared between the two
 * classes It also contains the instances of VerilogPopulate and
 * VhdlPopulate.
 */
class DesignPopulate
{
  friend Populate::ErrorCode
  VerilogPopulate::possibleNetResolutions(NUNetHierRef *net_hier_ref,
                                          veNode ve_possible_resolution,
                                          StringAtom *module_name,
                                          LFContext *context);


/*! \brief Get the name associated with any mvv_node without caring
 * whether the node is Verilog or VHDL.
 *
 * \return true if success,  false otherwise
 * \param mvv_node the node to take the name of
 * \param objName a UtString for the result
 */
  bool MVVNameAlloc( mvvNode mvv_node, UtString* objName );

public:
  DesignPopulate( STSymbolTable *symtab,
		  AtomicCache *str_cache,
		  IODBNucleus * iodb,
		  SourceLocatorFactory *loc_factory,
		  FLNodeFactory *flow_factory,
		  NUNetRefFactory *netref_factory,
		  MsgContext *msg_context,
		  ArgProc *arg,
                  TicProtectedNameManager* tic_protected_nm_mgr,
		  bool allow_hierrefs,
                  bool verbose_hierref_analysis,
                  bool useLegacyGenerateHierarchyNames,
                  VHDLCaseT caseMode,
                  CarbonContext* carbonContext);
  ~DesignPopulate();

  //! Create a source locator for the given MVV object
  SourceLocator locator( mvvNode node );
  //! Create a source locator for the given Cheetah object
  SourceLocator locator( veNode node );
  //! Create a source locator for the given Jaguar object
  SourceLocator locator( vhNode node );

  //! constructor init routine, allows for breakpoint
  void init();

  //! Put the InterraDesignWalker. Needed for integration.
  /*!
    This will be removed when the design walker is fully integrated
    into the population code.
  */
  void putDesignWalk(InterraDesignWalker* dw) { mInterraDesignWalker = dw; }

  //! Put the InterraDesignWalker callback object. Needed for integration.
  /*!
    This will be removed when the design walker is fully integrated
    into the population code.
  */
  void putWalkCB(NucleusInterraCB* cb) { mNucleusInterraCB = cb; }

  //! Get the InterraDesignWalker
  InterraDesignWalker* getInterraDesignWalker() { return mInterraDesignWalker; }

  //! Get the InterraDesignWalker callback
  NucleusInterraCB* getInterraCallback() { return mNucleusInterraCB; }

  //! Routine to get the LFContext. Needed by InterraDesignWalker.
  LFContext* getLFContext() { return mLFContext; }

  // Public entry point for populating a design with Verilog on top
  //Populate::ErrorCode design(veNode ve_topmod, NUDesign **the_design);

  // Public entry point for populating a design with VHDL on top
  /*
   \param vh_entity Jaguar node corresponding to the top level entity.
   \param vh_arch Jaguar node corresponding to the architecture of the top level entity.
   \param blk_cfg Jaguar node corresponding to the block which has been configured in the configuration declaration passed as the -vhdlTop argument of cbuild. This will be NULL if the top is an entity.
   \param the_design The reference to the design which has to be populated.
   \returns ErrorCode The status error code

  Populate::ErrorCode design( vhNode vh_entity, vhNode vh_arch, 
                              vhNode blk_cfg, NUDesign **the_design );
  */

  //! Create entry in map of MVV node to Nucleus net
  Populate::ErrorCode mapNet(NUScope* scope, mvvNode node, NUNet* net);
  //! Create entry in map of Nucleus net to Constraint Range
  Populate::ErrorCode mapConstraintRange(NUNet* net, ConstantRange* constraintRange);
  //! Lookup value in map of MVV node to Nucleus net, if not found print error msg when error_if_missing is true
  Populate::ErrorCode lookupNet(NUScope* scope, mvvNode node, NUNet **net, bool error_if_missing = true);
  //! Create entry in map of MVV node to NULvalue for a port
  Populate::ErrorCode mapPort(NUModule* module, mvvNode node, NULvalue* lvalue);
  //! Lookup value in map of MVV node to NULvalue port
  /*! \param partial_port will be set to true if \a node is only a part of
   * the formal connection. (Wacky ports).  The caller is responsible
   * for finding all the actuals needed to makup a portconnection.
   * \param range if partial_port is true then \a range will be set
   * with the the bit(s) of \a net that \a node represents
   * \param net will point to the net that was defind for the formal
   *  port, or will be NULL if an empty port appeared in the port declaration
   */
  Populate::ErrorCode lookupPort(NUModule *module, 
				 mvvNode node, 
				 mvvNode newModule,
                                 NUPortMap* portMap, 
				 NUNet **net,
                                 bool *partial_port, 
				 ConstantRange* range);

  void mapCompositeNet( NUNamedDeclarationScope *scope, NUCompositeNet *net );
  Populate::ErrorCode lookupCompositeNet( NUNamedDeclarationScope *scope,
                                          NUCompositeNet **net );

  //! The Verilog populator for the design
  VerilogPopulate mVlogPopulate;
  //! The VHDL populator for the design
  VhdlPopulate mVhdlPopulate;

  //! wrap up code, common to processing designs (used for verilog and VHDL)
  Populate::ErrorCode designFinish( LFContext *context, Populate::ErrorCode start_err_code);

  //! Print the stack of jaguar node source locations. This helps locate the
  //! source code that lead to the error message.
  void maybePrintJaguarStackSrcLoc();

  MsgContext* getMsgContext() { return mMsgContext; };

  //! Disable a message if it hits the limit of number of times it can be
  //! issued with the same text.
  eCarbonMsgCBStatus getMsgCBStatus(CarbonMsgSeverity severity,
                                    int msgNumber, const char* text,
                                    unsigned int len);

  //! Get the per scope jaguar node to NUNet map.
  const ScopeToNodeNetMap* getJaguarNodeNetMap() const {
    return &mNetMap;
  }

  const NUNetToConstraintRangeMap* getConstraintRangeMap() const {
    return &mConstraintRangeMap;
  }

  /*!
   * Creates a NUModuleInstance and assigns NUPortConnections between
   * the actual and formal ports. Takes into account module or entity
   * substitutions.
   */
  Populate::ErrorCode moduleInstance(mvvNode inst,
				     mvvNode oldEntity,
				     mvvNode newEntity,
				     const UInt32* offset,
				     LFContext* context); 

private:
  DesignPopulate( const DesignPopulate& );
  DesignPopulate& operator=( const DesignPopulate& );


  //! Return the Cheetah node to use to identify the net
  mvvNode getLookupNetNode(NUScope* scope, mvvNode node);

  //! Support classes--these are also stored in the language populators
  STSymbolTable *mSymtab;
  AtomicCache *mStrCache;
  IODBNucleus *mIODB;
  SourceLocatorFactory *mLocFactory;
  FLNodeFactory *mFlowFactory;
  NUNetRefFactory *mNetRefFactory;
  MsgContext *mMsgContext;
  ArgProc *mArg;
  bool mUseLegacyGenerateHierarchyNames;
  LFContext* mLFContext;
  bool mVerboseHierRefAnalysis;

  InterraDesignWalker* mInterraDesignWalker;
  NucleusInterraCB* mNucleusInterraCB;
  
  //! Map from a NUScope (NUModule or NUNamedDeclarationScope) to a map from an Interra node to a NULvalue (port)
  ScopeToNodeLvalueMap mPortMap;
  //! Maps connecting NUScope (NUModule or NUNamedDeclarationScope), and Interra node, and a NUNet
  ScopeToNodeNetMap mNetMap;
  NUScopeNetMap mCompositeNetMap;

  //! Maps a NUNet to a Constraint Range
  NUNetToConstraintRangeMap mConstraintRangeMap;

  //! What case should we force VHDL identifiers to be?
  VHDLCaseT mCaseMode;
  CarbonContext* mCarbonContext;

  //! Message callback to catch errors and print the jaguar stack.
  MsgCallback* mMsgCB;
  SInt32 mStackLimit; //! Maximum length of stack to print.

  //! Fold object used during population, this only does the simple stuff
  Fold *mFold;

public:
  //! Helper class to count messages, and suppress those that are emitted
  //  more than a user specified maximum
  MsgCountManager mMsgCountManager;
  
  //! Set of modules which need to be uniquified due to hierarchical references.
  UtSet<veNode> mUniquifyHierRefSet;
};

#endif

