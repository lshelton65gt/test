// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef REORDER_H_
#define REORDER_H_

#include "util/SourceLocator.h"
#include "util/UtHashSet.h"
#include "util/GenericDigraph.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUNetRefMap.h"
#include "reduce/AllocAlias.h"

class MsgContext;
class AtomicCache;
class IODBNucleus;
class NUNetRefSet;
class NUNetRefFactory;
class ArgProc;
class ReduceUtility;


/*!
  \file
  Declaration of reordering package.
 */

typedef GenericDigraph<void*,NUStmt*>      StmtGraph;
typedef NUNetRefMultiMap<StmtGraph::Node*> NUNetRefGraphNodeMultiMap;

typedef AllocAliasBucketNetRefMap<NUNetRefGraphNodeMultiMap> AliasBucketNetRefStmtGraphMap;

//! Class which reorders non-blocking assigns.
/*!
  Try to minimize the creation of temporaries for non-blocking assigns.
  As an example, if we had:
  \code
    a <= b;
    d <= a;
  \endcode

  If we don't reorder, we need to create a temporary for the first assign
  (because non-blocking assigns do not occur until right at the end of
  the containing structured procedure block).

  However, in this case we can reorder so that a temporary is not necessary:
  \code
    d = a;
    a = b;
  \endcode

  Note that it is not always possible to reorder:
  \code
    a <= b;
    b <= a;
  \endcode

  In this case, we need to create a temporary for a so that b sees the
  correct definition of a:
  \code
    temp = b;
    b = a;
    a = temp;
  \endcode
*/
class Reorder
{
public:
  //! constructor
  /*!
    \param str_cache
    \param netref_factory Net ref factory
    \param iodb
    \param args
    \param msg_ctx Context to issue error messages.
    \param alias_query Class used to query whether two nets may be allocation aliased
    \param expectAllBlocking Set to true to force non-blocking
                             assigns to raise an error
    \param groupConnected    When set, statements are order by
                             connected component before other ordering
    \param groupControlStatements When set, statements with shared
                             control bits are grouped together.
    \param dataOrderStatements when set, use a DFS walk (sorted by flow)
   */
  Reorder(AtomicCache *str_cache,
          NUNetRefFactory *netref_factory,
          IODBNucleus *iodb,
          ArgProc* args,
          MsgContext *msg_ctx,
          AllocAlias *alias_query,
          bool expectAllBlocking,
          bool groupConnected,
          bool groupControlStatements,
          bool dataOrderStatements);

  //! destructor
  ~Reorder();
  
  //! Perform non-blocking reordering over the design.
  void design(NUDesign *the_design);
  
  //! Perform non-blocking reordering over this module.
  void module(NUModule *module);

  //! Walk task and perform non-blocking reordering.
  void task(NUTask *task);

  //! Walk structured proc and perform non-blocking reordering.
  /*!
   * Need to reorder non-blocking assigns in the following cases:
   *   1 - If a net is used after a non-blocking assign occurs.
   *   2 - If a net has a blocking assign occuring after a non-blocking assign.
   *
   * The proc is walked, and a list of all non-blocking assigns is kept.
   * As the statements are walked, the preceeding conditions are checked.  If
   * found, they trigger reordering of assigns.
   *
   * Reordering occurs by creation of a temporary to hold the results of all
   * the non-blocking assignments to a net.  At the end of the proc,
   * the net will be assigned the value of the temporary.
   */
  void structuredProc(NUStructuredProc *structured_proc);

  //! Walk an unordered statement list for reordering, populate an ordered list to use in place of the given.
  void stmtList(const NUStmtList *unordered_stmts, NUStmtList * ordered_stmts);

  /*!
   * When non-blocking assigns are encountered, they are transformed to
   * blocking assigns.
   */
  void serialize(NUStmt *stmt);

  //! Walk the block scope for reordering.
  void blockScope(NUBlockScope *scope);

  //! Walk if statement for reordering.
  void ifStmt(NUIf *if_stmt);

  //! Walk for statement for reordering.
  void forStmt(NUFor *for_stmt);

  //! Walk case statement for reordering.
  void caseStmt(NUCase *case_stmt);

  //! Build a StmtGraph from an NUStmtList
  /*!
   \param stmts List of statements to build the graph for.
   \param cycles Optional.  The set of nets for which to place edges from their
                 final defs to their initial uses.  This is used by
                 SelfCycleReorder to try to eliminate self cycles in always blocks.
                 Can only use this when in "expectAllBlocking" mode (will assert otherwise).
   \param re_util Needs to be specified when cycles is specified.  Used to qualify
                  reordering feedback edges.
   *
   * This routine is made available to anyone who needs a stmt graph from a list
   * of stmts.
   */
  StmtGraph* createStmtGraph(const NUStmtList* stmts,
                             NUNetRefSet *cycles = 0,
                             ReduceUtility *re_util = 0);

  //! Create a new statement list, laid-out to minimize temporaries
  /*!
   * \param unordered_stmts (input) list of statements to build the graph for.
   * \param ordered_stmts   (output) list of ordered statements.
   * \param existing_graph  Optional. If given, this graph is used
   *                        instead of creating one. Note that this
   *                        will be deleted by this routine.
   *
   * This routine routine is made available to anyone who needs to layout stmts
   * from a graph.
   *
   * This method does not print the ordering operations performed. Use
   * dumpOrdering to obtain this information.
   *
   * \sa dumpOrdering
   */
  void layoutStmtList(const NUStmtList * unordered_stmts,
                      NUStmtList * ordered_stmts,
                      StmtGraph *existing_graph = 0);

  //! Free all memory associated with the given graph.
  void freeGraph(StmtGraph *graph);

  //! Dump a pre/post ordering report.
  /*!
   * This method is exposed so external callers can dump the changes
   * made by ::layoutStmtList. It is not used internally because
   * serialization (conversion of non-blocking to blocking assigns)
   * needs to occur between the print of unordered and ordered. 
   *
   * \sa layoutStmtList
   * \sa orderChanged
   * \sa dumpPreOrdering
   * \sa dumpPostOrdering
   */
  void dumpOrdering(const NUStmtList * unordered_stmts,
                    const NUStmtList * ordered_stmts);

private:
  //! Hide copy and assign constructors.
  Reorder(const Reorder&);
  Reorder& operator=(const Reorder&);

  //! Add dependencies to the graph for current net refs that intersect prior net refs
  void addDependencies(StmtGraph* graph, StmtGraph::Node* stmtNode, UInt32 edgeType,
                       NUNetRefSet& currentNetRefs,
                       AliasBucketNetRefStmtGraphMap* priorNetRefMap,
                       AliasBucketNetRefStmtGraphMap* currentNetRefMap);

  //! Update the net ref sets to reflect the defs and kills 
  void processDefsAndKills(StmtGraph::Node* stmtNode, NUNetRefSet& defs, NUNetRefSet& kills,
                           AliasBucketNetRefStmtGraphMap* defNetRefMap,
                           NUNetRefGraphNodeMultiMap* secondaryNetRefMap);
    
  //! Append the ordered statements from the graph to the given list
  void order(StmtGraph* graph, NUStmtList* ordered_stmts);

  //! Function to order a graph by data dependencies
  void dataOrder(StmtGraph* graph, NUStmtList* ordered_stmts);

  //! Convert this stmt to a blocking form using temporaries
  void requireTemp(NUStmt* stmt);

  //! Create temporary mem-entry and idx variables for non-blocking mem assigns
  void tempifyMemoryNet(NUMemoryNet* memNet, NUBlockScope* scope);

  //! Perform temporary creation.
  /*!
   * Go through the set of nets which need temps for their non-blocking assigns,
   * and create the temps.
   * Note we don't handle selectively creating temps for a net, this is because
   * we currently don't handle any sort of bit individualization.
   */
  void createTemporaries(NUBlockScope *scope, const SourceLocator& use_loc);

  //! Determine the full set of defs&uses in the current scope for the same net as this net ref
  NUNetRefHdl completeDefs(NUNetRefHdl & must_temp_net_ref);

  typedef UtHashMap<NUMemoryNet*, NUTempMemoryNet*> MemTempMap;

  //! Create a temporary from an original net.
  NUNet * createTemporary(NUNet * net, 
                          NUBlockScope * scope, 
                          MemTempMap * memTempMap,
                          bool * is_mem,
                          bool * is_bit);
	
  //! Replace references to the original net with a temporary.
  void replaceNetWithTemporary(NUNet * net, NUNet * temp);

  //! Add feedback edges for nets in feedback_use_map, break anti-dependencies.
  void createFeedbackEdges(NUNetRefGraphNodeMultiMap &feedback_use_map,
                           AliasBucketNetRefStmtGraphMap &block_def_map,
                           StmtGraph *graph,
                           ReduceUtility *re_util);

  //! Maintain feedback initial use information
  void feedbackMaintenance(StmtGraph::Node* node,
                           const NUNetRefSet &uses,
                           const NUNetRefSet &kills,
                           NUNetRefSet &open_feedback_uses,
                           NUNetRefGraphNodeMultiMap &feedback_use_map);

  //! Remove edges due to net ref pessimism
  void removePessimisticNetRefEdges(StmtGraph* graph);

  //! Map from statement to its position in the original, unordered list.
  typedef UtMap<NUStmt*,UInt32> StatementOrderMap;

  //! Is there any difference between the two statement lists?
  /*!
   * Used to determine if a debug print is necessary.
   */
  bool orderChanged(const NUStmtList * unordered_stmts, 
                    const NUStmtList * ordered_stmts);

  //! Print the incoming, unordered statements and populate a position map.
  /*!
   * \param unordered_stmts The unordered statements, for printing.
   * \param order           Map of statement to unordered position. Populated by this method.
   *
   * \sa dumpPostOrdering
   */
  void dumpPreOrdering(const NUStmtList * unordered_stmts,
                       StatementOrderMap & order);
  //! Print the outgoing, ordered statements.
  /*!
   * \param ordered_stmts The ordered statements, for printing.
   * \param order         Map of statement to unordered position.
   * 
   * \sa dumpPreOrdering
   */
  void dumpPostOrdering(const NUStmtList * ordered_stmts,
                        StatementOrderMap & order);

  //! String cache
  AtomicCache *mStrCache;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! IODB
  IODBNucleus *mIODB;

  //! args
  ArgProc* mArgs;

  //! Message context.
  MsgContext *mMsgContext;

  //! Allocation alias query.
  AllocAlias *mAliasQuery;

  //! Set of nets which we must temp out.
  NUNetRefSet * mMustTempSet;

  //! Convenience typedef to map a net to all its transformed non-blocking assigns.
  typedef UtMultiMap<NUNet*,NUBlockingAssign*> AssignMultiMap;

  //! Map of nets to transformed non-blocking assigns.
  AssignMultiMap mNonBlockMap;

  //! Set during the backend - Asserts if nonblocking defs are encountered.
  bool mExpectAllBlocking;

  //! Set to request that statements be grouped by weakly-connected component
  bool mGroupConnected;

  //! Set to request that control statements are scheduled togehter.
  bool mGroupControlStatements;

  //! Set to use a data order
  bool mDataOrderStatements;

  //! Set to trigger verbose reporting of reordering changes
  bool mVerbose;

  NUUseDefNode* mCurrentNode;
}; // class Reorder

#endif
