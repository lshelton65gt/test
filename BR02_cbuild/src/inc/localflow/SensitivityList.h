// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef SENSITIVITY_LIST_H_
#define SENSITIVITY_LIST_H_

#include "nucleus/Nucleus.h"

class MsgContext;
class NUDesign;
class NUAlwaysBlock;
class NUNetRefFactory;
class IODBNucleus;



//! Class to check the sensitivity list of verilog always blocks. We 
//! don't rely on Cheetah to check for sensitivity list of an always block.
//! This analysis may be expanded later on vhdl processes.
class SensitivityList
{
public:
  SensitivityList(IODBNucleus* iodb, MsgContext* msgCtx, NUNetRefFactory* factory);
  ~SensitivityList();

  // ! This function gathers all the nets that are defs in the always block.
  // | It doesn't check for missing nets.
  bool gatherInfo(NUModule* mod);

  //! This function checks the sensitivity list. It uses the info
  //! gathered in the gatherInfo function as well.
  bool check(NUModule* mod);

private:
  IODBNucleus*         mIODB;
  MsgContext*          mMsgContext;
  NUNetRefFactory*     mFactory;

  NUAlwaysBlockNetRefSetMap mAlwaysBlockNetRefSet;  // Map of always blocks and net references
  bool                      mIsGatherDone;          // Is used to confirm that Gather pahse is done
};


#endif
