// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/Nucleus.h"

class MsgContext;

//! Object to resynthesize wired net assigments.
/*!
  This object fixes assignments to wired nets by essentially changing
  = to &= for wand and |= for wor. 

  triand is treated as wand, and trior is treated wor.

  The propagate method is recallable. However, the module-based assign
  resynthesis is not recallable. This object will keep a list of
  visited modules so module() will not resynthesize the same one again.
*/
class WiredNetResynth
{
public:
  //! Constructor
  WiredNetResynth(MsgContext* msgContext);

  //! Destructor
  ~WiredNetResynth();

  //! Propagate wired net attributes and resynthesize assignments
  /*!
    If we come across an incompatible connection (wand->wor,
    wor->wand) alert.

    The algorithm for this is to keep doing a design walk until no
    flags are propagated. A wired net propagates through wires, and
    the act of changing a wire may mean we need to fix connections to
    the wire we changed. This can only be done in a cyclic manner.

    This will propagate wand/wor property across port connections to
    other wires. Propagation stops at registers.

    This will also, eventually, uniquify modules that have wired nets
    that are used in more than one context. For example, 
    
    \verbatim
    module sub(out, in);
      input in;
      output out;
      assign out = in;
    endmodule

    module top(out1, out2, in1, in2, d);
      input in1, in2, d;
      output out1, out2;

      wor out1;
      wand out2;

      sub s1(out1, in1);
      sub s2(out1, d);
      sub s3(out2, in2);
      sub s4(out2, d);
    endmodule
    \endverbatim

    In this case, sub needs to be uniquified for wand and for wor.

    \warning Uniquification is not yet implemented.
    \warning Wired nets through ports are not yet implemented. So, if
    a wired net is connected to anything else through a port it will
    cause an alert.

    This method is recallable.
  */
  void propagate(NUDesign* design);

  //! Recursively resynth continuous assigns to wired nets
  /*!
    This method is only recallable if you keep the same
    WiredNetResynth object around. A set of visited modules is kept
    and is used to skip. So, don't instantiate this class more than
    once when resynthesizing assigns.
    
    \param module Module in which to resynthesize assigns
    \param recurse If false, do not recurse into the module. Just do
    the assignments in the module definition.
  */
  bool module(NUModule* module, bool recurse = true);


private:
  //! Resynthesize NUAssigns to wired nets.
  void resynthAssigns(NUDesign* design);

  //! Lower wired net connections to unaliasable  ports
  /*!
    This lowers the following connections:

    -# Wired nets connected to ports that are registers

    Inouts cannot be registers. An alert will happen if that is
    encountered. That can only happen due to an earlier phase
    (if, for example, coercion was moved before this phase).
  */
  void lowerNonAliasablePorts();

  // forward decl
  class FlagPropagateCallback;
  class AssignmentCallback;

  friend class AssignmentCallback;

  // members
  MsgContext* mMsgContext;
  NUModuleSet* mVisitedModules;
  
  // forbid
  WiredNetResynth(const WiredNetResynth&);
  WiredNetResynth& operator=(const WiredNetResynth&);
};
