// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2002-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef UPDATEFLOW_H_
#define UPDATEFLOW_H_

#include "nucleus/NUDesignWalker.h"
#include "localflow/UnelabFlow.h"

/*!
  \file
  UD utility callbacks.
*/

class NUNetRefFactory;
class MsgContext;
class IODBNucleus;

//! Callback used to recompute all ext-net related unelab flow information.
/*!
 * This class can be used by a NUDesignWalker to compute ext-net
 * unelab flow information across an entire design. The unelab flow
 * interface calculates use/def information per-module and does not
 * perform recursion.
 */
class UpdateExtNetFlowCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:  
  //! Constructor
  UpdateExtNetFlowCallback(FLNodeFactory * flowFactory,
                           NUNetRefFactory * netRefFactory,
                           MsgContext * msgContext,
                           IODBNucleus * iodb) :
    NUDesignCallback()
  {
    mUnelabFlow = new UnelabFlow(flowFactory, netRefFactory, msgContext, iodb, false);
  }

  //! Destructor
  ~UpdateExtNetFlowCallback() { delete mUnelabFlow; }

  //! By default, skip everything
  Status operator()(Phase, NUBase*) { return eSkip; }

  //! Walk through designs.
  Status operator()(Phase, NUDesign*) { return eNormal; }

  //! Walk through module instances.
  Status operator()(Phase, NUModuleInstance *) { return eNormal; }

  //! There could be module instances inside named declaration scopes.
  Status operator()(Phase, NUNamedDeclarationScope*) { return eNormal; }

  //! Process modules once as we exit the recursive walk.
  Status operator()(Phase phase, NUModule * module) {
    if (phase==ePost) {
      mUnelabFlow->moduleUpdateExtNet(module);
    } 
    return eNormal;
  }
  
private:
  //! UnelabFlow interface.
  UnelabFlow * mUnelabFlow;
};

#endif
