// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CLOCK_AND_DRIVER_H_
#define CLOCK_AND_DRIVER_H_

/*!
  \file
  Declaration of clock driver package.
 */

#include "nucleus/Nucleus.h"
#include "flow/Flow.h"


//! Interface allowing elaboration to determine which nets are clocks.
class ClockAndDriver 
{
public:
  //! Constructor
  ClockAndDriver();

  //! Destructor
  ~ClockAndDriver();

  //! Add a net elab as a known clock; add all its drivers as known drivers.
  void addClock(const NUNetElab * net_elab);
  
  //! Is this net elab known to be a clock?
  bool isClock(const NUNetElab * net_elab) const;

  //! Is this flow elab known to be a clock driver?
  bool isClockDriver(const FLNodeElab * flow_elab) const;

private:
  //! Set of known clocks.
  NUCNetElabSet * mClocks;

  //! Set of known clock drivers.
  FLNodeElabSet * mClockDrivers;
};

#endif
