// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef REDUNDANTASSIGNS_H_
#define REDUNDANTASSIGNS_H_

#include "nucleus/Nucleus.h"
class IODBNucleus;
class MsgContext;

//! Get rid of redundant assignments
/*!
 * This currently only gets rid of continuous assigns of the form
 * "assign a = a;".  This is being done quickly for bug 2024.
 *
 * In the future, this should go away and be done better through
 * known compiler redundancy algorithms.
 *
 * NOTES:
 * 1 - this assumes that UD has not been computed yet; it does not
 *     attempt to maintain UD information.
 * 2 - this assumes that local flow graph has not been computed yet;
 *     it does not attempt to maintain that graph.
 */
class RedundantAssigns
{
public:
  //! constructor
  RedundantAssigns();

  //! destructor
  ~RedundantAssigns();

  //! Process the given module
  void module(NUModule *module);

  //! Process the given stmt
  void stmt(NUStmt *stmt);

private:
  //! Hide copy and assign constructors.
  RedundantAssigns(const RedundantAssigns&);
  RedundantAssigns& operator=(const RedundantAssigns&);
};

#endif
