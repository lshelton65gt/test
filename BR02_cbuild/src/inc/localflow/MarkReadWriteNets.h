// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef MARKREADWRITENETS_H_
#define MARKREADWRITENETS_H_

#include "nucleus/Nucleus.h"

class ArgProc;
class IODBNucleus;

/*!
  \file
  Declaration of read/write decoration package.
*/

//! Walk a piece of Nucleus and mark nets which are read or written.
/*!
 * This class does not use UD information, it just does a tree walk.
 * Therefore, it can be used prior to UD computation.
 *
 * Although the information computed by this class can be obtained
 * from the UD sets, this class is needed for 2 reasons:
 * 1. The information is needed so UD can work correctly in the
 *    presence of hierarchical references.
 * 2. By marking the nets we can have a faster lookup, rather than
 *    looking through UD sets.
 *
 * This gets called by UD whenever UD changes so that UD and
 * this information are in sync.
 */
class MarkReadWriteNets
{
public:
  //! constructor
  /*!
   * \param process_simple_connections If true, simple port
   * connections contribute to the read/write characteristics of a
   * net.
   */
  MarkReadWriteNets(IODBNucleus * iodb,
                    ArgProc* args,
                    bool process_simple_connections);

  //! destructor
  ~MarkReadWriteNets() {}

  //! Walk the whole design
  void design(NUDesign *design);

  //! Walk only the given module, and its children if recurse is true.
  void module(NUModule *module, bool recurse = false);

private:
  //! Hide copy and assign constructors.
  MarkReadWriteNets(const MarkReadWriteNets&);
  MarkReadWriteNets& operator=(const MarkReadWriteNets&);

  //! The IODB.
  IODBNucleus * mIODB;

  //! The command-line argument processor.
  ArgProc* mArgs;

  //! Should simple port connections contribute to the read/write characteristics?
  /*!
   * A simple port connection is one where hierarchical bit-wise
   * aliasing can occur. For example, connecting 'a' or '{a,b,c}' to
   * any port is simple. The expression 'a|b|c', however, is complex.
   */
  bool mProcessSimplePortConnections;
  bool mMultiLevelHierTasks;
};

#endif
