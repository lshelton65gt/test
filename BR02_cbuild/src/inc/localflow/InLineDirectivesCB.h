// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef INLINEDIRECTIVESCB_H_
#define INLINEDIRECTIVESCB_H_

#include "localflow/InterraDesignWalker.h"
#include "localflow/Populate.h"
#include "util/UtString.h"
#include "util/UtStack.h"

class CheetahContext;
class CarbonContext;
class TicProtectedNameManager;

// callback for InterraDesignWalker to process all in-line directives
class InLineDirectivesCB : public InterraDesignWalkerCB
{
public:

  InLineDirectivesCB(CarbonContext* context);
  ~InLineDirectivesCB();
  
  //! InterraDesignWalker callbacks.
  Status hdlEntity(Phase phase, mvvNode node, mvvLanguageType langType, const char* entityName, bool* isCmodel);

  Status generateBlock(Phase phase, mvvNode node, const char* blockName, mvvLanguageType langType, const SInt32* genForIndex);

  Status alwaysBlock(Phase phase, mvvNode node, mvvLanguageType);

  Status net(Phase phase, mvvNode node, mvvLanguageType langType);

  Status localNet(Phase phase, mvvNode node, mvvLanguageType langType);

  Status variable(Phase phase, mvvNode node, mvvLanguageType langType);

  mvvNode substituteEntity(mvvNode entityDecl, mvvLanguageType entityLanguage, const UtString& entityDeclName, mvvNode instance, bool isUDP, mvvLanguageType callingLanguage);


  Populate::ErrorCode getErrCode() const { return mErrCode; }
  void putErrCode(Populate::ErrorCode newCode) { mErrCode = newCode; }

private:

  InterraDesignWalkerCB::Status processVerilogNetLikeObject(mvvNode node);

  IODBNucleus* mIODB;
  CarbonContext* mCarbonContext;
  TicProtectedNameManager* mTicProtectedNameMgr;
  typedef UtStack<UtString> VisibleNameStack;
  VisibleNameStack mModuleNameStack;
  VisibleNameStack mBlockNameStack;
  Populate::ErrorCode mErrCode; 
};


#endif
