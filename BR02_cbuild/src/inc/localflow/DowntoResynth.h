// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __DowntoResynth_h_
#define __DowntoResynth_h_

#include "nucleus/Nucleus.h"

class ArgProc;
class MsgContext;
class AtomicCache;
class IODBNucleus;
class Fold;

//! class to eliminate "DownTo" exprs in concats
class DowntoResynth {
public:
  //! ctor
  DowntoResynth(NUNetRefFactory*, ArgProc*, MsgContext*, AtomicCache*,
                IODBNucleus*);

  //! dtor
  ~DowntoResynth();

  //! process entire design
  void design(NUDesign*);

private:
  Fold* mFold;
};

#endif 
