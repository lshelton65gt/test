// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef MEMORYRESYNTH_H_
#define MEMORYRESYNTH_H_

class NUDesign;
class NUMemoryNet;
class MsgContext;

//! class to resynthesize all memories to their normalized 2-d form
class MemoryResynth {
public:
  //! ctor
  MemoryResynth( MsgContext *msgContext );

  //! dtor
  ~MemoryResynth();

  //! perform memory resynthesis on a design, returns true if successful
  bool design(NUDesign* design);

private:
  MemoryResynth();
  MemoryResynth(const MemoryResynth&);
  MemoryResynth& operator=(const MemoryResynth&);
  MsgContext *mMsgContext;
};


//! class to provide multiple index iteration across a multidimensional
// NUMemoryNet
class MemoryIterator
{
public:
  /*! \brief Constructor
   *
   * A MemoryIterator provides the integer indicies to iterate across
   * the supplied number of dimensions of a multidimensional memory.
   * The iterator will walk from MSB to LSB across the highest 'dims'
   * dimensions.
   * \param mem [in] The memory to be iterated across.
   * \param dims [in] The number of dimensions to be iterated across.
   */
  MemoryIterator( const NUMemoryNet *mem, const int dims );
  ~MemoryIterator();

  //! reset the iterator to its beginning position
  void reset();
  //! Get the pointer to the array of integers containing the index values
  int *const *getIndexArray() const;
  //! iterate to the next set of indicies in the memory
  bool next();

private:
  //! disallowed 
  MemoryIterator();
  //! disallowed
  MemoryIterator( const MemoryIterator& );
  //! disallowed
  MemoryIterator& operator=( const MemoryIterator& );

  /*! \brief Perform the actual iteration
   *
   * This does the heavy lifting here.  This is a recursive function that
   * will move to the next value in the supplied dimension.  If the next
   * value is out of range, the current range is set to its MSB, and the
   * next higher range is incremented, recursively.  If we are at the
   * end of the top range, return false.
   * \param [in] dim The dimension to increment
   * \returns A boolean value indicating the validity of the iteration data
   */
  bool incrementDim( int dim );

  //! The memory being iterated over
  const NUMemoryNet *mMem;
  //! How many indicies to generate iterations for
  const int mDimsToIterate;
  //! Array, one entry per dimension, that provides the incrementer for the dimension
  int *mDimIncr;
  //! A pointer to the allocated integer array containing the current indicies
  int *mReturnedIndicies;
};
#endif
