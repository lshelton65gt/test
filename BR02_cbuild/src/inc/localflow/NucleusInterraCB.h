// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NUCLEUSINTERRACB_H_
#define NUCLEUSINTERRACB_H_

#include "localflow/InterraDesignWalker.h"
#include "localflow/Populate.h"

class NUDesign;
class DesignPopulate;
class LFContext;
class PrePopInterraCB;

//! Population callback structure for the InterraDesignWalker
/*!
  This differs from the old population methodology in several ways:

  -# Unlike the old population methodology, the execution stack of the
     walk cannot be unwound to a specific point and continue on. For
     example, if ports in a module cause a population problem in the
     old methodology, we returned eFailNoCreate and up above we check
     if there was a fatal error. Not having one meant going to the
     next thing to do. The best that we can do here is stop for fatal
     population errors only and always continue on otherwise. This has
     the side effect of more error spew, and a commitment to
     solidifying code to not crash in the face of errors. 

  -# ErrorCode analysis is no longer execution-stacked. Callbacks
     don't really allow for that type of thing, so we keep two
     errorcodes: a tmp error code (mPhaseErrCode) and the overall
     errorcode (mErrCode). This makes partial unwinding a non-starter
     as explained above. We could do better here, but to do so would
     be a major effort for little gain.

  -# The verilog and vhdl paths are basically combined in this
     callback, removing some of the nice object-oriented separation
     between the languages. Due to mixed-language support, that
     methodology doesn't work well with single language constructs
     through multi-language hierarchy. For example, a defparam in
     verilog can go through a vhdl module instantiation and touch a
     verilog module instantiated from the vhdl module.

  -# Having combined code allows for centralization of certain aspects
     of population. For example, module substitution was a verilog
     only thing. This new methodology allows for module substitution
     for vhdl as well. Someone just needs to write the vhdl part of
     the callback.

   Most of what you need to know about any particular callback
   function is documented in the InterraDesignWalkerCB class.
*/
class NucleusInterraCB : public InterraDesignWalkerCB
{
public:
  
  //! Constructor
  /*!
    \param populator The Cbuild design populator object
    \param nudesignptr A pointer to the NUDesign pointer. This
    callback object will create the NUDesign object.
    \param prePopCB The callback used for the prepopulation walk
    (which must have already been done). It contains the elaborated
    design and uniquification data.
  */
  NucleusInterraCB(DesignPopulate* populator, NUDesign* nudesign,
                   PrePopInterraCB* prePopCB);
  
  //! Populate the design.
  /*!
    This sets up the LFContext on the pre phase and calls the
    populator's designFinish as well as sets the top module on the
    post phase, if applicable.
  */
  virtual Status design(Phase phase, mvvNode node, mvvNode architecture, mvvLanguageType langType);
  
  virtual Status hdlEntity(Phase phase, mvvNode node, mvvLanguageType langType, const char* entityName, bool* isCmodel);

  virtual Status architecture(Phase phase, mvvNode node, const char* archName, mvvLanguageType langType);

  virtual Status udp(Phase phase, mvvNode node, const char* unit, const char* precision, mvvLanguageType langType);
  
  virtual Status module(Phase phase, mvvNode node, const char* unit, const char* precision, mvvLanguageType langType);

  virtual Status cModel(Phase phase, mvvNode node, mvvLanguageType langType);

  virtual mvvNode substituteEntity(mvvNode entityDecl, mvvLanguageType entityLanguage, const UtString& entityDeclName, mvvNode instance, bool isUDP, mvvLanguageType callingLanguage);
  virtual Status vlogForeignAttribute(mvvNode module, const UtString& entityDeclName, mvvNode* ent, mvvNode* arch);

  virtual Status componentElabFail(mvvNode node, mvvLanguageType lang, int elab_status);
  virtual Status generateElabFail(mvvNode node, mvvLanguageType lang);
  virtual Status generateElabError(mvvNode node, mvvLanguageType langType);

  virtual Status unsupportedInstanceType(mvvNode node, mvvLanguageType langType);
  virtual Status unsupportedConcStmt(mvvNode node, mvvLanguageType langType);
  virtual Status unsupportedDeclaration(mvvNode node, mvvLanguageType langType);
  virtual Status emptyArchitecture(mvvNode node, const char* archName, mvvLanguageType langType);
  virtual void unnamedGenerateBlock(veNode ve_node, const char* blockName, mvvLanguageType langType);

  virtual Status nestedGenerateInstance(mvvNode node, mvvLanguageType langType);
  virtual Status implicitGuardNullInGuardedBlock(mvvNode node, mvvLanguageType lang);

  virtual Status hdlComponentInstanceDeclaration(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status hdlComponentInstance(Phase phase, mvvNode inst, 
                                      const char* instName,
                                      mvvNode instMaster,
                                      mvvNode oldEntity,
                                      mvvNode newEntity,
                                      const char* entityName,
                                      mvvNode arch,
                                      const SInt32* vectorIndex, 
                                      const UInt32* rangeOffset,
                                      mvvLanguageType instLang, 
                                      mvvLanguageType scopeLang);

  virtual Status scopeVariable(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status generateBlock(Phase phase, mvvNode node, const char* blockName, mvvLanguageType langType, const SInt32* genForIndex);
  virtual Status taskFunction(Phase phase, mvvNode node, const char* tfName, mvvLanguageType langType);
  virtual Status initialBlock(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status gatePrimitive(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status specifyBlock(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status alwaysBlock(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status declaredAssign(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status declaredAssignList(Phase phase, mvvNode node, mvvLanguageType langType);

  virtual Status contAssign(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status assertStmt(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status ifGenerateTrue(Phase phase, mvvNode node, const char* blockName, mvvLanguageType langType);
  virtual Status forGenerate(Phase phase, mvvNode node, mvvNode indexVal, mvvLanguageType langType);
  virtual Status blockStmt(Phase phase, mvvNode node, const char* blockName, mvvLanguageType langType);
  virtual Status concProcCall(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status concProcess(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status selSigAssign(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status conditionalSigAssign(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status concSigAssign(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status file(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status alias(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status variable(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status localNet(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status net(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status package(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status packageDecl(Phase phase, mvvNode node, mvvLanguageType langType,
                             const char* logicalLib, const char* packName, bool isStdLib);
  virtual Status declaration(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status port(Phase phase, mvvNode node, mvvLanguageType langType);
  virtual Status portList(Phase phase, mvvNode node, mvvLanguageType langType);


    //! Get the population error code.
  Populate::ErrorCode getErrCode() const { return mErrCode; }


private:
  Populate::ErrorCode mErrCode;
  Populate::ErrorCode mPhaseErrCode;
  DesignPopulate* mPopulator;
  PrePopInterraCB* mPrePopInfo;
  STBranchNode* mElabScope; // Current scope
  LFContext* mLFContext;
  NUDesign* mDesign;
  UtSet<StringAtom*>* mPackageNets;
  
  Status processModuleOrUDP(mvvNode node, mvvLanguageType langType,
                            const char* unit, const char* precision);

};

#endif
