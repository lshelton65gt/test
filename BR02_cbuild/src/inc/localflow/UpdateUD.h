// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2002-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef UPDATEUD_H_
#define UPDATEUD_H_

#include "nucleus/NUDesignWalker.h"
#include "localflow/UD.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUModuleInstance.h"

/*!
  \file
  UD utility callbacks.
*/

class NUNetRefFactory;
class MsgContext;
class IODBNucleus;
class ArgProc;

//! Callback used to recompute all UD information.
/*!
 * This class can be used by a NUDesignWalker to compute use/def
 * information across an entire design. The UD interface calculates
 * use/def information per-module and does not perform recursion.
 */
class UDCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:  
  //! Constructor
  UDCallback(NUNetRefFactory * netRefFactory,
             MsgContext * msgContext,
             IODBNucleus * iodb,
             ArgProc* args) :
    NUDesignCallback()
  {
    mUD = new UD(netRefFactory, msgContext, iodb, args, false);
  }

  //! Destructor
  ~UDCallback() { delete mUD; }

  //! By default, skip everything
  Status operator()(Phase, NUBase*) { return eSkip; }

  //! Walk through designs.
  Status operator()(Phase, NUDesign*) { return eNormal; }

  //! Walk through module instances.
  Status operator()(Phase, NUModuleInstance *) { return eNormal; }

  //! There could be module instances inside named declaration scopes.
  Status operator()(Phase, NUNamedDeclarationScope*) { return eNormal; }

  //! Process modules once as we exit the recursive walk.
  Status operator()(Phase phase, NUModule * module) {
    if (phase==ePost) {
      mUD->module(module);
    } 
    return eNormal;
  }
  
private:
  //! UD interface.
  UD * mUD;
};


//! Callback used to recompute all ext-net related UD information.
/*!
 * This class can be used by a NUDesignWalker to compute ext-net
 * use/def information across an entire design. The UD interface
 * calculates use/def information per-module and does not perform
 * recursion.
 */
class UpdateExtNetUDCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:  
  //! Constructor
  UpdateExtNetUDCallback(NUNetRefFactory * netRefFactory,
                         MsgContext * msgContext,
                         IODBNucleus * iodb,
                         ArgProc* args) :
    NUDesignCallback()
  {
    mUD = new UD(netRefFactory, msgContext, iodb, args, false);
  }

  //! Destructor
  ~UpdateExtNetUDCallback() { delete mUD; }

  //! By default, skip everything
  Status operator()(Phase, NUBase*) { return eSkip; }

  //! Walk through designs.
  Status operator()(Phase, NUDesign*) { return eNormal; }

  //! Walk through module instances.
  Status operator()(Phase, NUModuleInstance *) { return eNormal; }

  //! Process modules once as we exit the recursive walk.
  Status operator()(Phase phase, NUModule * module) {
    if (phase==ePost) {
      mUD->moduleUpdateExtNet(module);
    } 
    return eNormal;
  }
  
private:
  //! UD interface.
  UD * mUD;
};


//! Callback used to clear all use-def information.
class ClearUDCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
    //! constructor
  ClearUDCallback(bool recurse): mRecurse(recurse) {}

  //! destructor
  ~ClearUDCallback() {}

  //! Catchall routine
  Status operator()(Phase, NUBase*) { return eNormal; }

  //! Routine to handle use def nodes
  Status operator()(Phase phase, NUUseDefNode* useDef)
  {
    if (phase == ePre)
      useDef->clearUseDef();
    return eNormal;
  }

  Status operator()(Phase phase, NUModuleInstance* inst)
  {
    if (phase == ePre)
      inst->clearUseDef();
    return mRecurse? eNormal: eSkip;
  }

private:
  bool mRecurse;
}; // class ClearUDCallback : public NUDesignCallback

#endif
