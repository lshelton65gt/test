// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef PROCESSRESYNTH_H_
#define PROCESSRESYNTH_H_

class MsgContext;
class NUDesign;
class NUNetRefFactory;
class IODBNucleus;
class AtomicCache;
class ArgProc;
class ResynthContext;

//! Class to look for VHDL processes populated as Verilog always blocks
//! and resynthesize them into one or more synthesiable Verilog always blocks.
class ProcessResynth
{
public:
  ProcessResynth(MsgContext* msgCtx, NUNetRefFactory* factory,
                 IODBNucleus* iodb, AtomicCache* atomicCache,
                 ArgProc* args, bool verbose);
  ~ProcessResynth();

  //! Design to look for VHDL processes that need resynthesis.
  bool design(NUDesign* design);

private:
  ResynthContext*  mContext;
};

#endif
