// -* -C++- *-
/******************************************************************************
 Copyright (c) 2004-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Includes required classes for "HDL/Verilog Parsing XML Generator"
*/

#ifndef DUMP_HIERARCHY_H
#define DUMP_HIERARCHY_H

#include "localflow/InterraDesignWalker.h"
#include "localflow/DesignHierarchy.h"

class IODBNucleus;

//! A class to dump the design hierarrchy in XML format
/*! Walks the design and dumps the instance hierarchies including
 *  the generics/parameters and ports for each instance inside the 
 *  top level instance in XML format.
 *
 *  Note that the 'instance hierarchy' includes generate labels
 *  and process labels. These are required by the modelstudio
 *  hierarchy browser to construct the full path names of
 *  signals inside processes, and underneath generates. Without
 *  these labels, the browser cannot find these signals in
 *  the Carbon database.
 */
class DumpHierarchyCB : public InterraDesignWalkerCB {

public:
  
  //! constructor
  DumpHierarchyCB(UtString fileName, 
		  TicProtectedNameManager* tic_protected_nm_mgr, 
		  VHDLCaseT vhdlCaseSpec,
		  IODBNucleus* iodb);
  //! destructor
  virtual ~DumpHierarchyCB();

  void dumpXml();

  //! Walks the design and dumps "ToplevelInterface" and "DesignHierarchy"
  virtual Status design(Phase           phase, 
			mvvNode         node,
			mvvNode         architecture,
			mvvLanguageType langType);

  //! Walks the entities/modules and adds them to the interface for dumping
  virtual Status hdlEntity(Phase           phase, 
			   mvvNode         node, 
			   mvvLanguageType langType, 
			   const char*     entityName, 
			   bool*           isCmodel);

  //! Return eSkip instead of eStop
  virtual Status unsupportedConcStmt(mvvNode, mvvLanguageType);
  virtual Status unsupportedInstanceType(mvvNode, mvvLanguageType);
  virtual Status generateElabFail(mvvNode, mvvLanguageType);
  virtual Status generateElabError(mvvNode, mvvLanguageType);
  virtual Status nestedGenerateInstance(mvvNode, mvvLanguageType);
  virtual Status unsupportedGenerateItem(mvvNode, mvvLanguageType);
  virtual Status componentElabFail(mvvNode, mvvLanguageType, int);
  virtual Status unsupportedDeclaration(mvvNode, mvvLanguageType);
  virtual Status implicitGuardNullInGuardedBlock(mvvNode, mvvLanguageType);

  //! Walks the components and dumps them
  virtual Status hdlComponentInstance(Phase           phase,    
                                      mvvNode         inst, 
                                      const char*     instName,
                                      mvvNode         instMaster,
                                      mvvNode         oldEntity,
                                      mvvNode         newEntity,
                                      const char*     entityName,
                                      mvvNode         arch,
                                      const SInt32*   vectorIndex, 
                                      const UInt32*   rangeOffset,
                                      mvvLanguageType instLang, 
                                      mvvLanguageType scopeLang);

  //! Walks the ports and adds them to the interface for dumping
  virtual Status port(Phase           phase, 
		      mvvNode         node, 
		      mvvLanguageType langType);
  //! Walks the generics and adds them to the interface for dumping
  virtual Status net(Phase           phase, 
		     mvvNode         node, 
		     mvvLanguageType langType);

  virtual Status packageDecl(Phase phase, mvvNode node, mvvLanguageType langType,
                             const char* libName, const char* packName,
                             bool isStdLib);

  //! Verilog generate block call back (to put generate labels in design hierarchy file)
  virtual Status generateBlock(Phase phase, mvvNode node, const char* blockName,
			       mvvLanguageType langType, const SInt32* genForIndex);

  //! vhdl for generate loop callback. 
  virtual Status forGenerate(Phase phase, mvvNode node, mvvNode indexExpr, mvvLanguageType langType);

  //! vhdl concurrent process callback
  virtual Status concProcess(Phase phase, mvvNode node, mvvLanguageType langType);

  //! if generate process callback
  virtual Status ifGenerate(Phase phase, mvvNode node, mvvLanguageType langType);

  //! Package body callback. Walked to from package use clause.
  virtual Status packageBody(Phase phase, mvvNode node, mvvLanguageType langType);

  //! Substitution of modules or entities with modules or entities
  virtual mvvNode substituteEntity(mvvNode entityDecl, 
				   mvvLanguageType entityLanguage, 
				   const UtString& entityDeclName, 
				   mvvNode instance, 
				   bool isUDP, 
				   mvvLanguageType callingLanguage);


  virtual Status vlogForeignAttribute(mvvNode module, const UtString&, mvvNode* ent, mvvNode* arch);

private:

public:
  //! Adds all the files used in the design
  void addFile(mvvNode node, 
	       UtString fileName, 
	       UtString fileType,
	       UtString language);
private:
  //! constructor
  DumpHierarchyCB();

  //! Check for `includes in the file. Only for VLOG.
  void checkForIncludes(veNode file);
  //! Check for `define in the file. Only for VLOG.
  void checkForDefines(veNode file, File* filePtr);

  DesignHierarchy*         mDesignHierarchy;
  TicProtectedNameManager* mTicProtectedNameMgr;
  bool                     mInProtectedRegion;
  VHDLCaseT                mVhdlCase;
  IODBNucleus*             mIODB;
};

#endif
