// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef USERTYPEPOPULATE_H_
#define USERTYPEPOPULATE_H_

#include "compiler_driver/Interra.h"
#include "nucleus/Nucleus.h"

class IODBNucleus;
class DesignPopulate;
class MsgContext;

class UserTypePopulate
{
public:
  UserTypePopulate(const DesignPopulate* populator, 
		   IODBNucleus* iodb,
                   bool verbose,
		   MsgContext* msgContext);
  bool populate(NUDesign* design);

private:

  const DesignPopulate* mPopulator;
  IODBNucleus* mIODB;
  bool mVerbose;
  MsgContext* mMsgContext;
};

#endif
