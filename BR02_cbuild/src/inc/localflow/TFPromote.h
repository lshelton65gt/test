// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004, 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef TFPROMOTE_H_
#define TFPROMOTE_H_

#include "nucleus/Nucleus.h"
#include "util/Util.h"

class MsgContext;
class AtomicCache;
class IODBNucleus;
class ArgProc;
class STBranchNode;
class CopyContext;

/*!
  \file
  Declaration of task/function rewrite package.
 */

//! Class to handle non-local references and non-blocking assigns inside
//! tasks and functions.
/*!
 * It is assumed that this is called very early in the compiler, before any
 * UD computation, etc, has been done -- this pass only modifies the Nucleus
 * tree.
 */
class TFPromote
{
public:
  //! constructor
  TFPromote(NUNetRefFactory *netref_factory,
	    MsgContext *msg_ctx,
	    AtomicCache *string_cache,
	    bool verbose,
            IODBNucleus *iodb,
            ArgProc* args,
            bool promoteAllTasks);

  //! destructor
  ~TFPromote();

  //! Perform rewrite over the whole design
  void design(NUDesign *design);

  class HierBlock;
  class HierTaskCall;
  typedef UtHashSet<HierBlock*, HashPointerValue<HierBlock*> > HierBlockSet;
  typedef UtArray<HierBlock*> HierBlockVec;
  typedef UtArray<HierTaskCall*> HierTaskCallVec;
  typedef UtHashMap<NUTaskEnable*,HierTaskCallVec*> TaskEnableInstanceMap;
  typedef UtMap<NUTaskEnable*,TFPromote::HierTaskCall*> TaskEnableHierCallMap;

  friend class TFPromoteCallback;
  class TaskEnableFixupCallback;

  //! internal helper function called from TaskEnableFixupCallback
  NUTaskEnable* rewriteTaskEnable(NUTaskEnable* taskEna);

private:
  //! Initial design walk to find elaborated TaskEnables and their owner blocks
  void findAllTasksAndCalls(NUDesign *this_design);

  //! Resolve the elaborated blocks for task-enable destinations
  void resolveCalls();

  //! Print the entire elaborated call-tree
  void print();

  //! Find all desirable block promotions
  void findPromotions();

  //! Determine if a particular block should be promoted
  void considerPromotingBlock(HierBlock*, SInt32 maxAllowableJumps);

  //! Look up an elaborated block, creating it if it does not yet exist
  HierBlock* getHierBlock(NUBase* block, STBranchNode* scope);

  //! Promote a task-enable
  void promoteCall(HierTaskCall*);

  //! Promote a net for a promoted block, and note its replacement
  void promoteNet(HierBlock*, NUNet* net, CopyContext*);

  //! Perform all the block promotions that were previously computed
  void performPromotions();

  //! Perform the promotion of a particular block
  void performBlockPromotion(HierBlock*);

  //! Clear all the memo-ized jump counts in preparation for another
  //! call-graph walk
  void clearJumpCount();

  //! Find the nucleus module associated with a scope
  NUModule* findModule(STBranchNode*);

  //! Perform all the task-enable promotions
  void promoteTaskEnables();

  //! Delete all original nucleus always- and initial-blocks after promotion
  /*! Note that anytime an always-block or initial-block is promoted, it must
   *! be promoted in every instance.  But that is not necessarily true for
   *! tasks, where there may be both local and hierarchical calls to the same
   *! nucleus task.  So we leave it to MarkSweepUnusedTasks to clean up the
   *! dead tasks for us, rather than presuming that every promoted NUTask*
   *! must be deleted.
   */
  void cleanup();

  //! When fixing up task-enables in a promoted block, we need to be
  //! able to associate the original NUTaskEnable* (stored in the
  //! HierTaskCall), with its promoted clone.
  void mapTaskEnables(HierBlock* block);

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! String cache
  AtomicCache *mStringCache;

  //! Message context
  MsgContext *mMsgContext;

  //! Be verbose as processing occurs?
  bool mVerbose;

  //! Promote all tasks, whether they are hier-refd or not
  bool mPromoteAllTasks;

  //! Set of scopes which we have visited so far.
  NUScopeSet mScopeSet;

  //! IODB to store intrinsic type information into
  IODBNucleus *mIODB;

  //! args
  ArgProc* mArgs;

  //! Keep track of elaborated blocks -- we elaborate on demand.
  HierBlockSet* mHierBlockSet;
  
  //! Same contents as mHierBlockSet, but in traversal order
  HierBlockVec* mHierBlockVec;

  //! Keep track of every elaboration of a task-enables for closure
  TaskEnableInstanceMap* mTaskEnableInstanceMap; // NEED TO DO ALWAYS BLOCKS TOO!

  //! Flag variable to know when to stop iterating (closure achieved)
  bool mPromotionOccurred;

  //! If the user threw -inlineTasks then any hierarchically ref'd
  //! task must be promoted
  bool mInlineTasks;

  //! Always block set to defer deletions
  typedef UtHashSet<NUAlwaysBlock*> AlwaysBlockSet;
  AlwaysBlockSet* mDeleteAlwaysBlocks;

  //! Initial block set to defer deletions
  typedef UtHashSet<NUInitialBlock*> InitialBlockSet;
  InitialBlockSet* mDeleteInitialBlocks;

  //! TaskEnable set to defer deletions
  typedef UtHashSet<NUTaskEnable*> TaskEnableSet;
  TaskEnableSet* mDeleteTaskEnables;

  //! Map promoted task-enable clones to HierTaskCall*
  TaskEnableHierCallMap* mTaskEnableHierCallMap;
};

#endif
