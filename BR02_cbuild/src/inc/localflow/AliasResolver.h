// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef PROCESSRESYNTH_H_
#define PROCESSRESYNTH_H_

#include "localflow/VhdlPopulate.h"

class MsgContext;
class NUDesign;
class AtomicCache;
class Fold;

//! Resolve VHDL aliases to originals. 
/*!
  VHDL alias declarations allow user to rename a net or part of it and
  use different range(s) for the aliased name from the net. The range 
  has to be of the same width. For example:

  signal vec : std_logic_vector(7 downto 2);
  alias myvec : std_logic_vector(4 to 8) is vec(7 downto 3);

  The myvec is an alias to vector net vec. The [7:3] part of the vector
  is being aliased. The range [7:3] of net vec is aliased to [4:8] of
  net myvec. Consider the following statement:

  myvec[5] := '1';

  This really means:

  vec[6] := '1';

  Ideally compiler should implement just one net, vec. The names myvec and
  range mappings should be saved in io/symtab db and shell should deal
  with it. However that is not what is implemented here.

  Compiler implements a net for every alias net. The alias net is def'ed
  only once in a continuous assign statement by the original net. All the
  other defs and uses of alias net are resolved at compile time to original net.
  Here's how the above example is implemented:

  reg [7:2] vec;
  reg [4:8] myvec;

  assign myvec = vec[7:3];

  ...
  vec[6] := '1';

  Population phase of compile does not resolve aliases. It however creates
  nets for aliases as well as originals. It also creates expressions for
  alias replacements and saves them into a (alias net => replacement expr)
  map.

  After population is complete, the AliasResolver does the following:

  a. Compute map of alias net ranges to original net ranges.
  b. Create statements that assign to alias nets from original expressions.
  c. Fix the offsets of indices based on alias net range map.
  d. Replace all alias nets with original nets. 
  
*/
class AliasResolver
{
public:
  AliasResolver(MsgContext* msgCtx, AtomicCache* strCache, Fold* fold);
  ~AliasResolver();

  //! Resolve aliases in the given design, given alias net to orig expr map.
  VhdlPopulate::ErrorCode design(NUDesign* design,
                                 VhdlPopulate::EntityAliasMap* aliasMap);

private:
  MsgContext* mMsgCtx;
  AtomicCache* mStrCache;
  Fold* mFold;
};


#endif
