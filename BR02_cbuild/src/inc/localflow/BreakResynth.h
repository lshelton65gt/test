// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __BreakResynth_h_
#define __BreakResynth_h_

#include "nucleus/Nucleus.h"
#include "util/CbuildMsgContext.h"

class MsgContext;

//! class to resynthesize 'break' statements into a enable-variables and
//! if statements.
class BreakResynth {

  //! Information associated with each block that we traverse.
  /*! This is kept on the stack as we recurse down the syntax tree.  If an
   *! enable net needs to be synthesized for a block, then it is kept
   *! in the BlockInfo so it can be re-used in case there are multiple
   *! disables to the same block.
   */
  struct BlockInfo;

  // We employ a two-pass strategy over each module.  In the first
  // pass, we:
  //    - re-arrange control structure (see optimization comments in ::ifStmt)
  //    - Add explicit NUBreak statements for every level of block being
  //      broken out of
  //    - Add "if (ena) ..." tests, and for (...; ena & ...; ...) conditions
  //      wherever required (where the if-statement optimization was not
  //      enough.
  //
  // After that, we know the exact set of ena variables we really need,
  // and we can avoid synthesizing those that no one looks at.
  enum Pass {
    eControlResynthesis,
    eEnableConstruction
  };

public:
  BreakResynth(NUNetRefFactory*, MsgContext*);
  ~BreakResynth();

  //! Process an always-block or initial-block
  void structuredProc(NUStructuredProc*);

  //! Process a design
  void design(NUDesign*);

private:
  //! Process a module
  void module(NUModule*, Pass);

  //! Process a task body
  void task(NUTask*);

  //! Process a task enable
  void taskEnable(NUTaskEnable*);

  //! Rewrite a statement list, copying the statements to newStmts,
  //! transforming NUBreaks into if/ena_var hacking, and removing
  //! nested NUBlocks.
  /*! 
   *! Returns true if one of the statements was a 'break'.  Return false
   *! if a break was buried below other syntax.
   */
  void stmtList(NUStmtLoop stmts,
                BlockInfo* blockInfo,
                NUStmtList* newStmts);

  //! Get the enable-net associated with a block, creating it if it
  //! doesn't yet exist.
  NUNet* getBlockEnableNet(BlockInfo* blockInfo, const SourceLocator& loc);

  //! Rewrite a case statement, handling breaks and nested blocks
  void caseStmt(NUCase*, BlockInfo* blockInfo);

  //! Process an if statement.
  /*!
   *! Returns whether the higher-level statement
   *! list should be continue to be processed.  We want to stop processing
   *! the statements after an "if", if the "then" branch contained a "break"
   *! and the remainder of the statements on the level of the "if" were
   *! moved into the else branch.  The StmtLoop is passed in so that the
   *! if statement can pull them into the "else" branch if needed
   */
  //
  bool ifStmt(NUIf*, BlockInfo*, NUStmtLoop stmts);

  //! Process a for statement
  void forStmt(NUFor*, BlockInfo*);

  //! Process a block, managing an explicit block-stack via mBlockVector and
  //! mBreakIndex.
  void blockStmt(NUBlock*, NUStmtList*);

  //! 1st pass: construct the block info structure for a block,
  //! 2nd pass: get it
  BlockInfo* pushBlockInfo(NUBlock*);

  //! 1st pass: remove the block info from the stack
  //! 2nd pass: also destroy it
  void popBlockInfo(BlockInfo*);

  //! Process a 'break' statement by adding 'enable' variables to all
  //! the blocks between the 'break' up to and including the target block
  void breakStmt(NUBreak* breakStmt, NUStmtList* stmts);

  //! Delete a block, without deleting the statements it owns, which
  //! have generally been moved up to the owner
  void deleteBlock(NUBlock*);

  //! Update the current minimum break index if it's higher level (lower)
  //! than the current one
  void updateBreakIndex(SInt32 breakIndex);

private:
#if pfLP64
  // Do not try to shrink BlockInfo pointers because we put
  // BlockInfo objects on the stack, so they don't have the
  // right pattern for us to shrink them.
  typedef UtHashMap<NUBlock*, BlockInfo*, HashPointer<void*>,
                    HashPointerMgr<NUBlock*>,
                    HashObjectMgr<BlockInfo*> > BlockMap;
#else
  typedef UtHashMap<NUBlock*,BlockInfo*> BlockMap;
#endif

  typedef UtVector<BlockInfo*> BlockVector;
  
  BlockMap* mBlockMap;          // helps us find the BlockInfo for a break
                                // target
  BlockVector* mBlockVector;    // helps us enumerate through all blocks 
                                // between the break stmt and its target
  SInt32 mBreakIndex;           // index of highest block we are breaking to
                                // This is 0-based, and -1 indicates that
                                // there is no active Break.

  // Which tasks have we covered so far/
  typedef UtHashSet<NUTask*> TaskSet;
  TaskSet* mModuleTasks;        // Only process each task once, even if
                                // it is enabled multiple times.
  TaskSet* mDesignTasksCovered;

  NUNetRefFactory* mNetRefFactory;
  MsgContext* mMsgContext;

  NUModule* mModule;
  
  Pass mPass;
};  

#endif
