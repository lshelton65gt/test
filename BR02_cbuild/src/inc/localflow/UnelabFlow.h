// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef UNELABFLOW_H_
#define UNELABFLOW_H_

#include "flow/Flow.h"
#include "flow/FlowUtil.h"
#include "nucleus/NUNetRef.h"
#include "localflow/DesignWalker.h"
#include "nucleus/NUNetRefMap.h"

class MsgContext;
class IODBNucleus;

/*!
  \file
  Declaration of unelaborated flow package.
 */

//! Class to create unelaborated DFG
class UnelabFlow : public DesignWalker
{
public:
  //! constuctor
  /*
    \param dfg_factory DFG Factory
    \param netref_factory Net ref factory
    \param msg_context Where to issue error messages
    \param iodb IODB
    \param verbose If true, will be verbose about DFG creation.
   */
  UnelabFlow(FLNodeFactory *dfg_factory,
	     NUNetRefFactory *netref_factory,
	     MsgContext *msg_context,
	     IODBNucleus *iodb,
	     bool verbose);

  //! Create DFG for this module.
  /*!
   * Iterate through all the different top-level drivers which exist
   * in a module.  For each driver, create a def node, and then stitch
   * all def nodes together.
   *
   * Recurse to handle submodules if recurse is true.
   */
  void module(NUModule *module, bool recurse);

  //! Always-recursive module walker
  void module(NUModule *module);

  //! stand-alone processing of a set of always blocks
  void alwaysBlockSet(NUAlwaysBlockSet *blocks);

  //! Update flow information for the ext-net.
  /*!
   * This method is not part of the normal flow creation. It is meant
   * to update ext-net flow information late in a compile -- without
   * updating/recreating flow for the entire module.
   *
   * Generally, this method should be called after protected nets have
   * been added.
   */
  void moduleUpdateExtNet(NUModule * module);

  //! destructor
  ~UnelabFlow();

private:
  //! Hide copy and assign constructors.
  UnelabFlow(const UnelabFlow&);
  UnelabFlow& operator=(const UnelabFlow&);

  //! Create DFG for this task or function
  void tf(NUTF *tf);

  //! Create DFG for this simple continuous driver.
  /*!
   * Simple continuous drivers are: continuous assign, trireg init, enabled driver.
   */
  void simpleContDriver(NUUseDefNode *node);

  //! Create DFG for this always block.
  void alwaysBlock(NUAlwaysBlock *block);

  //! Create DFG for this initial block.
  void initialBlock(NUInitialBlock *block);

  //! Create DFG for this block.
  void block(NUBlock *block);

  //! Create DFG for this list of stmts.
  /*!
   * Recurse to handle nested stmts.
   */
  void stmtList(NUStmtList *stmts);

  //! Create DFG for this stmt.
  /*!
   * Recurse to handle nested stmts.
   */
  void stmt(NUStmt *stmts);

  //! Create DFG for this if stmt.
  /*!
   * Recurse to handle nested stmts.
   */
  void ifStmt(NUIf *if_stmt);

  //! Create DFG for this case stmt.
  /*!
   * Recurse to handle nested stmts.
   */
  void caseStmt(NUCase *case_stmt);

  //! Create DFG for this for stmt.
  /*!
   * Recurse to handle nested stmts.
   */
  void forStmt(NUFor *for_stmt);

  //! Create DFG for this instance.
  /*!
   * Recurse to handle submodules if recurse is true.
   */
  void moduleInstance(NUModuleInstance *inst, bool recurse);

  //! Create a bound node for this undriven net, mark it as Z
  void undrivenNet(NUNet *net);

  //! Create a bound node for any undriven parts of a net
  void fixPartiallyDriven(NUNet* net);

  //! Create DFG for this bid port connection, input contribution
  void portConnectionBidInput(NUPortConnectionBid *portconn);

  //! Create DFG for this bid port connection, output contribution
  void portConnectionBidOutput(NUPortConnectionBid *portconn);

  //! Create DFG for this input port connection
  void portConnectionInput(NUPortConnectionInput *portconn);

  //! Create DFG for this output port connection
  void portConnectionOutput(NUPortConnectionOutput *portconn);

  //! Create DFG for a port input (input or bid)
  void portInput(NUPortConnection *portconn, NUNetRefSet *formal_defs);

  //! Create DFG for a port output (output or bid)
  void portOutput(NUPortConnection *portconn, NUNetRefSet *actual_defs);

  //! Start at the formals, get the corresponding actuals, and add them as fanin to the given flow node.
  void addActualUsesFromFormals(FLNode *flow_node,
				NUNetRefSet &formal_uses,
				NUNetRefFLNodeMultiMap &def_map);

  //! Create DFG for \a parent_virt_net of the instantiated module child_inst
  /*!
   * Promote the virtual net fanin from instantiated module if the fanin is seen
   * through port actuals.
   */
  void moduleInstanceConnectVirtNetFanin(NUModuleInstance *child_inst,
                                         NUNet* child_virt_net,
                                         NUNet* parent_virt_net,
                                         NUNetRefFLNodeMultiMap &def_map);

  //! Handle inputs, outputs, bids for modules, tasks, functions
  /*!
   * Handle arguments:
   *  . add flow bound nodes for inputs and bids
   *  . if an output is not driven, issue a message and create a bound node
   */
  void portsAndArgs(NUNetVectorLoop &loop);

  //! Add bound nodes for protected mutable nets (to represent the external driver).
  void protectedMutableNets(NUNetList &nets);

  //! Connect live defs of all protected observable nets to the module's ext net
  void extNet(NUModule *module, NUNetList &nets);

  //! Return true if the given net is really driving the given flow_node.
  /*!
   * The net is seen as a fanin to the given flow_node.  Make sure that
   * the net really drives the flow_node.
   */
  bool determineDriven(NUNet *net, FLNode *flow_node);

  //! Connect the fanin
  /*!
    \param use_map Map of nets to what FLNode's use them
    \param fanin_connector What connect fanin function to use (normal or edge)
    \param task_scope Optional, given when are connecting fanin for a task
   */
  void connectFanin(NUNetRefFLNodeMultiMap &use_map,
		    void (FLNode::* fanin_connector)(const NUNetRefHdl &net_ref,
						     NUNetRefCompareFunction fn),
                    NUScope *task_scope = 0);

  //! Create DFG for a node which represents some sort of nesting.
  /*!
    \param node UseDef node for which to create DFG
    \param internal_use_map Map of nets to flow nodes which use them, inside the given node
    \param internal_def_map Map of nets to flow nodes which def them, inside the given node
    \param edge_uses Optional, set of nets which are in the edge fanin to every def.

    Create flow nodes for all defs of this node.
   */
  void collect(NUUseDefNode *node,
               NUNetRefFLNodeMultiMap *internal_use_map,
               NUNetRefFLNodeMultiMap *internal_def_map,
               NUNetRefSet *edge_uses=0);


  //! Determine if flow has already been created & prevent re-creation.
  /*!
    Used during flow creation of loops.
  */
  FLNode * lookupFlow(const NUNetRefHdl & net_ref,
                      NUUseDefNode * stmt,
                      NUNetNodeToFlowMap * flow_map);

  void oneStmtHelper(NUNetRefSet &killing_defs,
                     NUNetRefSet &stmt_kills,
                     NUNetRefSet &stmt_defs,
                     NUNetRefFLNodeMultiMap &stmt_use_map,
                     NUNetRefFLNodeMultiMap &stmt_def_map,
                     NUNetRefFLNodeMultiMap *list_use_map,
                     NUNetRefFLNodeMultiMap *list_def_map,
                     bool allow_multiple_drivers=false);


  //! remove from use_map any refs to temp nets
  void pruneTempNets(NUBlock *block_stmt, NUNetRefFLNodeMultiMap *use_map);

  void moduleInstanceUpdateExtNet(NUModule * parent_module,
                                  NUModuleInstance * child_inst);

  void moduleInstanceConnectExtNetUpdates(NUModuleInstance * inst,
                                          NUNet* child_ext_net,
                                          NUNet* parent_ext_net,
                                          NUNetRefFLNodeMultiMap &def_map);

  void updateExtNet(NUModule * module, NUNetList & nets);


  //! DFG Factory
  FLNodeFactory *mFlowFactory;

  //! Netref factory
  NUNetRefFactory *mNetRefFactory;

  //! Message context
  MsgContext *mMsgContext;

  //! IODB
  IODBNucleus *mIODB;

  //! Stack of net-to-flow use mappings (fanin).
  UtStack<NUNetRefFLNodeMultiMap*> mUseMapStack;

  //! Stack of net-to-flow edge use mappings (edge fanin).
  UtStack<NUNetRefFLNodeMultiMap*> mEdgeUseMapStack;

  //! Stack of net-to-flow def mappings (fanout).
  UtStack<NUNetRefFLNodeMultiMap*> mDefMapStack;

  //! Stack of already-created flow (used during loop analysis)
  UtStack<NUNetNodeToFlowMap*> mFlowStack;
};


#endif
