// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __DesignHierarchy_h_
#define __DesignHierarchy_h_

#ifndef __UtString_h_
#include "util/UtString.h"
#endif
#ifndef __UtList_h_
#include "util/UtList.h"
#endif
#ifndef __UtPair_h_
#include "util/UtPair.h"
#endif
#ifndef __UtMap_h_
#include "util/UtMap.h"
#endif
#ifndef __UTXMLWRITER_H_
#include "util/UtXmlWriter.h"
#endif


class Architecture;
class Define;
class File;
class Instance;
class Interface;
class Library;
class Parameter;
class Port;
class Type;

class DesignHierarchy {
 public: CARBONMEM_OVERRIDES

  DesignHierarchy(UtString fileName);

  ~DesignHierarchy();

  typedef UtMap<UtString, UtString> SubstitutePortMap;

  //! Checks for interfaces
  bool interfaceExists(UtString searchName);
  //! Adds an interface
  void addInterface(UtString searchName, Interface* entity);
  //! Checks for libraries
  bool libraryExists(UtString libraryName);
  //! Adds a library
  void addLibrary(UtString libraryName, Library* library);
  //! Checks for files
  bool fileExists(UtString fileName);
  //! Adds all the files used in the design
  void addFile(UtString fileName, File* file);
  //! Check for types
  bool typeExists(UtString libraryNamae, UtString packageName, UtString typeName);
  //! Adds a type
  void addType(UtString libraryName, UtString packageName, UtString typeName, Type* type);
  //! Adds a top level module or entity
  void addTopLevelDesignUnit(Instance* instance);

  //! Pushes an instance into the stack. Becomes the current instance.
  void pushInstance(Instance* instance);
  //! Pops the topmost instance from the stack. Returns it.
  void popInstance();
  //! Gets the current topmost instance or NULL if the stack is empty.
  Instance* getCurrentInstance();

  //! Pushes an interface into the stack. Becomes the current interface.
  void setCurrentInterface(Interface* interface);
  //! Gets the current topmost instance or NULL if the stack is empty.
  Interface* getCurrentInterface();

  void dumpXml();
  //! Dumps the interface information
  void dumpInterfaces(UtXmlWriter* xmlWriter);
  //! Dumps the library information
  void dumpLibraries(UtXmlWriter* xmlWriter);
  //! Dumps the file information
  void dumpFiles(UtXmlWriter* xmlWriter);
  //! Dumps the type information
  void dumpTypes(UtXmlWriter* xmlWriter);
  //! Dumps the instance information
  void dumpInstances(UtXmlWriter* xmlWriter);

  bool hasSubstituteInterface();
  void setSubstituteInterfaceName(UtString substituteName);
  void clearSubstituteInterfaceName();
  UtString getSubstituteInterfaceName();

  void addSubstitutePortMap(UtString origName, UtString subsName);
  UtString getSubstitutePortMap(UtString origName);
  void clearSubstitutePortMap();

 private:
  typedef UtMap<UtString, Interface*> InterfaceMap;
  typedef InterfaceMap::iterator InterfaceIter;

  typedef UtMap<UtString, Library*> LibraryMap;
  typedef LibraryMap::iterator LibraryIter;

  typedef UtMap<UtString, File*> FileMap;
  typedef FileMap::iterator FileIter;

  typedef UtMap<UtString, Type*> TypeMap;
  typedef TypeMap::iterator TypeIter;

  typedef UtList<Instance*> Instances;
  typedef Instances::iterator InstancesIter;

  typedef UtList<Interface*> Interfaces;
  typedef Interfaces::iterator InterfacesIter;

 private:
  UtString          mFileName;
  InterfaceMap      mInterfaces;
  LibraryMap        mLibraries;
  FileMap           mFiles;
  TypeMap           mTypes;
  Instances         mTopLevelInstances;
  Instances         mInstanceStack;
  Interface*        mCurrentInterface;
  UtString          mSubstituteInterfaceName;
  SubstitutePortMap mSubstitutePortMap;
};

//! A class to store and dump the generics/parameters in XML format
/*! Stores the generics/parameters information, such as names, types, and 
 *  values for adding to the interface for dumping.
 */
class Parameter {
 public:  CARBONMEM_OVERRIDES

  Parameter(UtString name, UtString type, UtString value);

  ~Parameter();
  
  //! Returns the generic/parameter name
  UtString getName(void);
  //! Set the location of this parameter
  void setStart(UtString fileName, UInt32 lineNo);
  //! Dumps the generic/parameter information
  void     dumpParameter(UtXmlWriter* xmlWriter);
  
 private:
  UtString mName;
  UtString mType;
  UtString mValue;
  UtString mStart;
};

//! A class to store and dump the ports in XML format
/*! Stores the ports information, such as stores their names, types, directions, and 
 *  values for adding to the interface for dumping.
 */
class Port {
 public:  CARBONMEM_OVERRIDES

  Port(UtString name, 
       UtString type, 
       UtString direction, 
       UtString width);

  Port(UtString actual,
       UtString formal);

  ~Port();
  
  //! Returns the port name
  UtString getName(void);
  //! Set the location of this port
  void setStart(UtString fileName, UInt32 lineNo);
  //! Dumps the port information
  void dumpPort(UtXmlWriter* xmlWriter);
  
 private:
  UtString mName;
  UtString mType;
  UtString mDirection;
  UtString mWidth;
  UtString mActual;
  UtString mFormal;
  UtString mStart;
};

//! A class to store and dump the architecture in XML format
/*! Stores the starting and ending points of the architecture
 */
class Architecture {
 public:  CARBONMEM_OVERRIDES

  Architecture(UtString name, UtString libraryName);

  ~Architecture();
  
  //! Returns the port name
  UtString getName(void);
  //! Sets the start line number
  void setStart(UtString fileName, UInt32 lineNo);
  //! Sets the end line number
  void setEnd(UtString fileName, UInt32 lineNo);
  //! Dumps the port information
  void dumpArchitecture(UtXmlWriter* xmlWriter);
  
 private:
  UtString mName;
  UtString mLibraryName;
  UtString mStart;
  UtString mEnd;
};

//! A class to dump the libraries in XML format
class Library {
 public:  CARBONMEM_OVERRIDES

  Library(UtString name, UtString path, UtString language);

  ~Library();
  
  //! The name of this library
  UtString getName();
  //! Sets this library as the default library
  void setAsDefaultLibrary();
  //! Dumps the info in XML format
  void dumpLibrary(UtXmlWriter* xmlWriter);
  
 private:
  UtString        mName;
  UtString        mPath;
  UtString        mLanguage;
  bool            mIsDefault;
};

//! A class to store Verilog `defines
class Define {
 public:  CARBONMEM_OVERRIDES
  Define(UtString name, UtString value, UtString startLineNo);
  ~Define();
  
  void dumpDefine(UtXmlWriter* xmlWriter);
  
 private:
  UtString        mName;
  UtString        mValue;
  UtString        mStart;
};

//! Corresponds to all the files used in the design.
class File {
 public:  CARBONMEM_OVERRIDES
  typedef UtList<Define*> Defines;
  typedef Defines::iterator DefinesIter;

  typedef enum { 
    eEntity,
    eArchitecture,
    ePackageDeclaration,
    ePackageBody,
    eConfiguration,
    eModule,
    eVlogLibrary,
    eIncludedFile,
    eUnknown,
  } FileType;

  File(UtString name, UtString fileType, UtString language);
  ~File();
  
  void setFileType(UtString type);
  void addDefine(Define* define);  
  void dumpFile(UtXmlWriter* xmlWriter);

  static UtString getFileType(FileType fileType);
  
 private:
  UtString        mName;
  UtString        mFileType;
  UtString        mLanguage;
  Defines         mDefines;
};

//! A class to dump the type in XML format
/*
 */
class Type {
 public:
  Type(UtString libraryName, UtString packageName, UtString typeName);
  ~Type();

  void setStart(UtString fileName, UInt32 lineNo);

  void setEnd(UtString fileName, UInt32 lineNo);

  void dumpType(UtXmlWriter* xmlWriter);
 
 private:
  UtString mLibraryName;
  UtString mPackageName;
  UtString mTypeName;
  UtString mStart;
  UtString mEnd;
};

//! A class to dump the module or entity definitions in XML format
/*! Stores the added generics/parameters and ports information 
 *  for each definition for dumping.
 *  For VHDL, also stores the architecture.
 */
class Interface {
 public:  CARBONMEM_OVERRIDES
  //! constructor
  Interface(UtString name, UtString libraryName, UtString language);

  Interface(bool inProtectedRegion);

  //! destructor
  ~Interface();
  
  //! Get the name of the interface
  UtString getName() { return mName; }
  //! Sets the start line number
  void setStart(UtString fileName, UInt32 lineNo);
  //! Sets the end line number
  void setEnd(UtString fileName, UInt32 lineNo);
  //! Adds a generic/parameter 
  void addParameter(Parameter* param);
  //! Adds a port
  void addPort(Port *port);
  //! Adds an architecture
  void addArchitecture(Architecture* architecture);
  //! Dumps the interface information
  void dumpInterface(UtXmlWriter* xmlWriter);
  
 protected:

  typedef UtList<Parameter*> Parameters;
  typedef Parameters::iterator ParametersIter;

  typedef UtList<Port*> Ports;
  typedef Ports::iterator PortsIter;

  typedef UtMap<UtString, Architecture*> Architectures;
  typedef Architectures::iterator ArchitecturesIter;

  bool            mProtected;
  UtString        mName;
  UtString        mLibraryName;
  UtString        mLanguage;
  UtString        mStart;
  UtString        mEnd;
  Parameters      mParameters;
  Ports           mPorts;
  Architectures   mArchitectures;
};

//! Maintains the instance hierarchy of the design
class Instance : public Interface {
 public:  CARBONMEM_OVERRIDES

  Instance(UtString name,
	   UtString libraryName,
	   UtString interface, 
	   UtString language);

  Instance(bool inProtectedRegion);

  ~Instance();

  void isTopLevel();

  void addArchitectureName(UtString name);

  void addInstance(Instance*);

  void setOriginalInterface(UtString originalInterfaceName);
  
  void setInterfaceType(UtString type);

  void dumpInstance(UtXmlWriter* xmlWriter);
  
 private:

  typedef UtList<Instance*> Instances;
  typedef Instances::iterator InstancesIter;

  bool      mTopLevel;
  UtString  mInterface;
  UtString  mArchitecture;
  Instances mInstances;
  UtString  mOriginalInterfaceName;
  UtString  mInterfaceType;
};

#endif
