// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef MEMORYBVRESYNTH_H_
#define MEMORYBVRESYNTH_H_

#include "util/CarbonTypes.h"
#include "util/UnionFind.h"
#include "nucleus/Nucleus.h"

class MsgContext;
class IODBNucleus;
class Fold;

//! MemoryBVResynth class
/*!
 * This class converts memories to bitvectors, based on some size thresholds.
 *
 * This is done because the rest of the compiler is more efficient at handling
 * memories than bitvectors:
 *  . codegen of bitvectors is more efficient in some cases (especially when
 *    the memory is small)
 *  . use/def analysis is more precise with bitvectors
 *  . other misc things, such as buffering inserted by the scheduler can be
 *    high-overhead for memories
 *
 * It doesn't always make sense to convert memories to bitvectors, though:
 *  . If the memory is observable or a primary port, it will not be converted.
 *    In the future, this can possibly be added through shell smarts.
 *  . If the memory is used in a readmem call, it will not be converted.
 *  . Based on some size thresholds, the memory will not be converted.  This is
 *    mainly due to what codegen codes more efficiently:  very small memories
 *    make sense to be codegen'd as bitvectors.  But, very large memories do not.
 *
 * The thresholds available are:
 *  . size threshold:  the total bit size of the memory
 *  . dyn index threshold:  total bit size of the memory when the memory is used with
 *    a dynamic index.
 *  . width threshold:  the width of the memory.  For large widths, memories are
 *    more efficient.
 *
 * This analysis uses several passes:
 *  . Finds exclusions based on usage,see the code for these,
 *    as some are due to bugs in other phases of the compiler that will change over
 *    time.
 *  . Exclude based on size and being a primary port or protected.
 *  . UnionFind is used to construct equivalence classes for appearance of memories
 *    in pairwise operations:
 *    . ports, the formal and the actual will be in the same class
 *    . memory operations (such as ==), both operands will be in the same class
 *    If one fo the nets is excluded for some reason, then the whole class needs
 *    to be excluded.
 *  . Construct bitvector replacement for the memories and rewrite the
 *    nucleus tree, delete the old nets.
 *
 */
class MemoryBVResynth
{
public:
  //! constructor
  /*!
    \param size_threshold Maximum total bit size of memory to convert.
    \param dyn_index_threshold Maximum total bit size of memory which is used with
           dynamic indexing, to convert.
    \param width_threshold Maximum width bit size of memory to convert.
    \param dump_stats If true, will dump statistics at the end of analysis.
    \param fold Fold object to use for optimizing tree components.
    \param iodb IODB to use for determining protected nets.
    \param msg_context Message context to emit messages.
   */
  MemoryBVResynth(UInt32 size_threshold,
                  UInt32 dyn_index_threshold,
                  UInt32 width_threshold,
                  bool dump_stats,
                  Fold *fold,
                  IODBNucleus *iodb,
                  MsgContext *msg_context);

  //! destructor
  ~MemoryBVResynth();

  //! Perform the analysis on the design.
  void design(NUDesign *design);

  //! Reasons for net exclusions.
  enum ExclusionReason {
    eExclNone           = 0x000,  //! No reason to exclude the net
    eExclPrimaryProtect = 0x001,  //! Exclude because is primary port or protected
    eExclReadmem        = 0x008,  //! Exclude because used in readmem
    eExclWholeOp        = 0x010,  //! Exclude because used in whole operation
    eExclDynIndex       = 0x020,  //! Exclude because non-constant dynamic index
    eExclWidth          = 0x040,  //! Exclude because width is too large
    eExclTooBig         = 0x080,  //! Exclude because total size is too large
    eExclCmplxDynIndex  = 0x100,  //! Exclude because used in complex dynamic index
    eExclSensitivity    = 0x200   //! Exclude because it's in the sensitivity list
  };

  //! Map of net to its exclusion reason.
  typedef UtMap<const NUNet*,ExclusionReason> NetExclusionReason;

  //! Equivalence classes based on usage.
  typedef UnionFind<NUNet*> NetEquivClasses;

private:
  //! Hide default constructor, copy constructor, assign operator.
  MemoryBVResynth();
  MemoryBVResynth(const MemoryBVResynth&);
  MemoryBVResynth& operator=(const MemoryBVResynth&);

  //! Class to keep track of reasons for exclusion and total memories seen.
  /*!
   * See enum ExclusionReason for what these mean.
   *
   * The numbers are not mutually exclusive, a memory may fall into more than one
   * category.
   */
  class Stats
  {
  public:
    Stats() :
      mExclPrimaryProtect(0),
      mExclReadmem(0),
      mExclWholeOp(0),
      mExclDynIndex(0),
      mExclWidth(0),
      mExclTooBig(0),
      mExclCmplxDynIndex(0),
      mTotalExclusions(0),
      mTotalTries(0)
    {}
    ~Stats() {}
    UInt32 mExclPrimaryProtect;
    UInt32 mExclReadmem;
    UInt32 mExclWholeOp;
    UInt32 mExclDynIndex;
    UInt32 mExclWidth;
    UInt32 mExclTooBig;
    UInt32 mExclCmplxDynIndex;
    UInt32 mTotalExclusions;
    UInt32 mTotalTries;
  };

  //! Return true if the memory with the given exclusions should be made into a bitvector.
  /*!
   * Also updates the stats object with reasons for exclusions.
   */
  bool shouldMakeBV(NUMemoryNet *mem, ExclusionReason reason);

  /*!
   * Determine if the memory should be excluded due to size, primary, or protected.
   * Populate map with exclusion reason.
   */
  void determineSizeProtectedExclusion(NUMemoryNet *mem, NetExclusionReason &exclusions);

  //! Given a memory, make a bitvector to replace it.
  NUVectorNet *makeBV(NUMemoryNet *mem);

  //! Dump out statistics.
  void dumpStats();

  //! Statistics
  Stats stats;

  //! Maximum size to convert when only consideration is size.
  UInt32 mSizeThreshold;

  //! Maximum size to convert when memory has a dynamic index.
  UInt32 mDynIndexThreshold;

  //! Maximum size to allow the memory width to be, to convert it.
  UInt32 mWidthThreshold;

  //! If true, dump stats
  bool mDumpStats;

  //! Folder
  Fold *mFold;

  //! Message context
  MsgContext *mMsgContext;

  //! IODB
  IODBNucleus *mIODB;
};

#endif
