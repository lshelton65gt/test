// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _LFFINDUSES_H_
#define _LFFINDUSES_H_

//! A class to walk a use def node and find all use nets.
/*! Walks a use def node and finds expressions. It accumulates all the
 *  nets in the expressions.
 *
 *  This is built off the NUDesignCallback walker.
 *
 *  Note: this class counts on expressions UD being self
 *  determined. That is the UD pass need not run to call
 *  this. Otherwise, there would be no need to call this code.
 */
class LFFindUsesCallback : public NUDesignCallback
{
public:
  //! constructor - find nets
  LFFindUsesCallback(NUNetSet* netSet);

  //! constructor - find net refs
  LFFindUsesCallback(NUNetRefSet* netRefSet);

  //! destructor
  ~LFFindUsesCallback() {}

  //! Catch all -- keep looking
  Status operator()(Phase, NUBase*) { return eNormal; }

  //! Capture nets in expressions
  Status operator()(Phase phase, NUExpr* expr);

private:
  NUNetSet* mNets;
  NUNetRefSet* mNetRefs;
}; // class LFFindUsesCallback : public NUDesignCallback

#endif // _LFFINDUSES_H_
