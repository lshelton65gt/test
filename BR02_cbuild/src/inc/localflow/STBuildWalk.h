// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef DESIGNINFO_H_
#define DESIGNINFO_H_

#include "localflow/InterraDesignWalker.h"
#include "localflow/Populate.h"

class DesignPopulate;
class PrePopInterraCB;
class IODBDesignDataSymTabs;
class STBuildWalkHelper;

//! Walker to build elaborated and unelaborated design data symbol tables.
class STBuildWalkCB : public InterraDesignWalkerCB
{
public:

  STBuildWalkCB(DesignPopulate* populator, PrePopInterraCB* prePopCB,
                IODBDesignDataSymTabs* symtabs);
  ~STBuildWalkCB();
  
  //! InterraDesignWalker callbacks.
  Status hdlEntity(Phase phase, mvvNode node, mvvLanguageType langType,
                   const char* entityName, bool* isCmodel);

  Status blockStmt(Phase phase, mvvNode, const char* blockName,
                   mvvLanguageType langType);

  Status forGenerate(Phase phase, mvvNode node, mvvNode indexVal,
                     mvvLanguageType langType);

  Status ifGenerateTrue(Phase phase, mvvNode /*node*/, const char* blockLabel,
                        mvvLanguageType langType);

  Status generateBlock(Phase phase, mvvNode /*node*/, const char* blockName, 
                       mvvLanguageType langType, const SInt32* /*genForIndex*/);

  Status port(Phase phase, mvvNode node, mvvLanguageType langType);

  Status packageDecl(Phase phase, mvvNode node, mvvLanguageType langType,
                     const char* libName, const char* packName, bool isStdLib);

  Status net(Phase phase, mvvNode node, mvvLanguageType langType);

  Status localNet(Phase phase, mvvNode node, mvvLanguageType langType);

  Status variable(Phase phase, mvvNode node, mvvLanguageType langType);

  Status alias(Phase phase, mvvNode node, mvvLanguageType langType);

  Status file(Phase phase, mvvNode node, mvvLanguageType langType);

  Status concProcess(Phase phase, mvvNode node, mvvLanguageType langType);

  Status hdlComponentInstance(Phase phase, mvvNode inst,
                              const char* instName,
                              mvvNode instMaster,
                              mvvNode oldEntity,
                              mvvNode newEntity,
                              const char* /*entityName*/,
                              mvvNode /*arch*/,
                              const SInt32* vectorIndex, 
                              const UInt32* rangeOffset,
                              mvvLanguageType instLang, 
                              mvvLanguageType scopeLang);

  Status taskFunction(Phase phase, mvvNode /*node*/, 
                      const char* tfName, mvvLanguageType langType);

  mvvNode substituteEntity(mvvNode entityDecl, mvvLanguageType entityLanguage,
                           const UtString& entityDeclName, mvvNode instance,
                           bool isUDP, mvvLanguageType callingLanguage);

  Status vlogForeignAttribute(mvvNode module, const UtString& entityDeclName, mvvNode* ent, mvvNode* arch);

  //! Get the design info setup error code.
  Populate::ErrorCode getErrCode() const { return mErrCode; }

private:

  void addNet(Phase phase, mvvNode node, mvvLanguageType langType);
  void pushElabScope(const char* scope, mvvLanguageType lang);
  void pushElabScope(StringAtom* scopeAtom);
  void popElabScope();
  void pushElabAndUnelabScopes(const char* scope, SInt32 scopeType,
                               mvvLanguageType lang);
  void popElabAndUnelabScopes();

  Populate::ErrorCode mErrCode;
  DesignPopulate* mPopulator;
  PrePopInterraCB* mPrePopInfo;
  STBuildWalkHelper* mHelper;
  STBranchNode* mPrePopElabScope;
  LFContext* mLFContext;
};

#endif
