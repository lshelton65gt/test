// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __ExplicitSize_h_
#define __ExplicitSize_h_

#include "nucleus/Nucleus.h"

//! class to manage making sizes explicit throughout design
class ExplicitSize {
public:
  //! ctor
  ExplicitSize();

  //! dtor
  ~ExplicitSize();

  //! process entire design
  void mutateDesign(NUDesign*);
};

#endif 
