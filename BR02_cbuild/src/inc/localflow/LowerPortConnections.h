// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef LOWERPORTCONNECTIONS_H_
#define LOWERPORTCONNECTIONS_H_

#include "nucleus/Nucleus.h"
#include "reduce/Flatten.h"
#include "util/SourceLocator.h"

class LFContext;
class NUTFArgConnectionInput;
class NUTFArgConnectionOutput;
class NUTFArgConnectionBid;
class NUCModelArgConnectionInput;
class NUCModelArgConnectionOutput;
class NUCModelArgConnectionBid;

//! Class that creates nucleus statements to lower port connections
class LowerPortConnections
{
public:
  LowerPortConnections(AtomicCache * str_cache,
                       MsgContext * msg_context,
                       bool        reuse_assigns,
		       bool        lower_everything) :
    mStrCache(str_cache),
    mMsgContext(msg_context),
    mReuseAssigns(reuse_assigns),
    mLowerEverything(lower_everything),
    mModule(NULL)
  {}

  virtual ~LowerPortConnections() {}

  //! Process one module lowering necessary port connections.
  void module(NUModule * one);

  //! Process one structured proc lowering necessary port connections
  bool structuredProc(NUStructuredProc * proc);

  //! Process a task/function lowering necessaryt port connections
  bool tf(NUTF * tf);

  NUExpr * lower(NUModule * module, NUExpr * actual, UInt32 bits, const SourceLocator loc);
  NUExpr * lower(NUModule * module, NUExpr * actual, NUNet * formal_net, const SourceLocator loc);

  NULvalue * lower(NUModule * module, NULvalue * actual, UInt32 bits, const SourceLocator & loc);
  NULvalue * lower(NUModule * module, NULvalue * actual, NUNet * formal_net, const SourceLocator & loc);


protected:
  //! Called before we enter a task enable call
  virtual void preCallTaskEnable(NUTaskEnable* te);

  //! Should this input connection be lowered?
  virtual bool doLowerInput(NUTFArgConnectionInput* in_conn);
  //! Should this output connection be lowered?
  virtual bool doLowerOutput(NUTFArgConnectionOutput* out_conn);
  //! Should this bid connection be lowered?
  virtual bool doLowerBid(NUTFArgConnectionBid* bid_conn);

  //! Should this cmodel input connection be lowered?
  virtual bool doLowerInput(NUCModelArgConnectionInput* in_conn);
  //! Should this cmodel output connection be lowered?
  virtual bool doLowerOutput(NUCModelArgConnectionOutput* out_conn);
  //! Should this cmodel bid connection be lowered?
  virtual bool doLowerBid(NUCModelArgConnectionBid* bid_conn);

private:
  NUNet * reuseTempAssign(NUModule* m, NUExpr *actual, UInt32 bits);

  void instance(NUModuleInstance * inst);
  void port ( NUPortConnection * portconn, FlattenQualifyPort &qualifier );
  void input ( NUPortConnectionInput * inport, FlattenQualifyPort &qualifier );
  void output ( NUPortConnectionOutput * outport, FlattenQualifyPort &qualifier );
  void bid ( NUPortConnectionBid * biport, FlattenQualifyPort &qualifier );

  bool qualify(NUExpr * actual, NUNet *formal, FlattenQualifyPort &qualifier);
  bool qualify(NULvalue * actual, NUNet *formal, FlattenQualifyPort &qualifier);

  bool block(NUBlock * block);
  bool lower(NUStmt * stmt, 
	     NUScope * scope, 
	     NUStmtList & pre_stmts,
	     NUStmtList & post_stmts);
  bool ifStmt(NUIf * stmt, NUScope * scope);
  bool caseStmt(NUCase * stmt, NUScope * scope);
  bool forStmt(NUFor * stmt, NUScope * scope);

  template<class CallType,
	   class ArgLoopType,
           class ArgType,
	   class InputArgType,
	   class OutputArgType,
	   class BidArgType,
	   class FormalType,
	   bool LowerInputs>
  bool call(CallType * call, 
	    NUScope * scope, 
	    NUStmtList & pre_stmts,
	    NUStmtList & post_stmts);

  bool lower(NUStmtLoop loop, 
	     NUScope * scope, 
	     NUStmtList & stmts);

  //! String cache
  AtomicCache * mStrCache;

  //! Message Context.
  MsgContext * mMsgContext;

  //! Should we re-use generated assignments?
  bool mReuseAssigns;

  //! Should we lower everything or just port expressions?
  bool mLowerEverything;

  //! Allows us to remember the active scope.
  NUModule * mModule;
};

#endif
