// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef POPULATE_H_
#define POPULATE_H_

#include "iodb/IODBNucleus.h"
#include "compiler_driver/Interra.h"

class NUVectorNet;
class NUMemoryNet;
class NUExpr;
class SourceLocator;
class MsgContext;
class LFContext;
class IODBNucleus;
class Fold;
class DesignPopulate;
class FLNodeFactory;
class CarbonContext;
class NUExpr;
class NUUnaryOp;
class TicProtectedNameManager;

// The intend is that this macro will always  be used when the 
// code returns eFailPopulate. That enforces message 
// printing for every eFailPopulate error.
// Later code_review test in checkin will be modified to check for
// any eFailPopulate in rest of the localflow directory.
#define POPULATE_FAILURE(message, status) \
           message; \
           *(status) = Populate::eFailPopulate \
      

//! Populate class
/*!
 * Base class for Vhdl and Verilog populate classes
 *
 */
class Populate
{
public:
  //! Error codes
  enum ErrorCode
  {
    eSuccess = 0,    //! Successfully created the construct.
    eNotApplicable,  //! Not an error, but nothing created because function was not applicable to the construct
    eFailPopulate,   //! Did not create the construct, an error was found.
    eFatal           //! Must stop now.  The compiler is in an indeterminate state.
  };

  //! create an always block for each of the task_enable statements saved in the context
  /*!
   * \param context Context of the module doing the instantiation
   */
  ErrorCode extraTaskEnables(LFContext *context, IODBNucleus * iodb);

  /*! return true if tempCode has a value that means that we should
    stop processing now, returns false if processing can continue.  In the
    case where we can continue but there was an error, a code is
    stored in resultErrorCode for later use
    This might have been called mostCommonErrorCodeProcessing
  */
  static bool errorCodeSaysReturnNowWhenFailPopulate(ErrorCode tempCode,
                                                     ErrorCode *resultErrorCode);

  /*! return true if tempCode has a value that is eFatal otherwise
    returns false meaning processing can continue.  In the case where we
    can continue but there was an error (eFailCreate or eFailNoCreate),
    that code is stored in resultErrorCode for later use.
  */
  static bool errorCodeSaysReturnNowOnlyWithFatal(ErrorCode tempCode,
                                                  ErrorCode *resultErrorCode);

  /*!
   * This method walks up the scope stack and generates a mangled name
   * incorporating the scope hierarchy up to, but not including, the
   * enclosing module
   */
  static StringAtom *getInstanceMangledName(mvvNode instance, 
					    LFContext *context,
					    const SourceLocator& loc,
					    TicProtectedNameManager* ticProtectedNameMgr);


  /*!
   * These methods (composeInstanceName) returns the name and language type for mvvNode instance.
   * if LFContext is provided then the name is intern'ed in the cache of LFContext
   * if VHDLCaseT is provided then the name is returned and no caching is used.
   * if suffix is nonNull then that string is appended to the end of inst_name before the return
   */
  static mvvLanguageType composeInstanceName(mvvNode instance, 
                                             LFContext* context,
                                             UtString* inst_name,
                                             TicProtectedNameManager* ticProtectedNameMgr,
                                             const UtString* suffix = NULL);

  static mvvLanguageType composeInstanceName(mvvNode instance, 
					     VHDLCaseT vhdlCase,
					     UtString* inst_name,
					     TicProtectedNameManager* ticProtectedNameMgr);

  /*!
   * Given a nucleus expression, do a bitwise inversion of it
   */
  static NUUnaryOp* invert (NUExpr* e);

  //! Methods to create NUVarsel[Lvalue|Rvalue] and normalize it's select expr.
  /*!
    These methods should be used to create NUVarsel[Lvalue|Rvalue] instead of
    directly new'ing them.
  */

  //! Create NUVarselLvalue with given lvalue  and unnormalized select.
  static NUVarselLvalue* genVarselLvalue(NUNet* net, NULvalue* lval, NUExpr* selExpr,
                                         const SourceLocator& loc,
                                         bool retNullForOutOfRange = true,  SInt32 adjust = 0);

  //! Create NUVarselLvalue with given lvalue, unnormalized select and part select range.
  static NUVarselLvalue* genVarselLvalue(NUNet* net, NULvalue* lval, ConstantRange range,
                                         NUExpr* selExpr, const SourceLocator& loc,
                                         bool retNullForOutOfRange = true,  SInt32 adjust = 0);

  //! Create NUVarselLvalue with given net and unnormalized select.
  static NUVarselLvalue* genVarselLvalue(NUNet* net, NUExpr* selExpr,
                                         const SourceLocator& loc,
                                         bool retNullForOutOfRange = true, SInt32 adjust = 0);

  //! Create NUVarselLvalue with given range and unnormalized select.
  static NUVarselLvalue* genVarselLvalue(NUNet* net, ConstantRange range, NUExpr* selExpr,
                                         const SourceLocator& loc,
                                         bool retNullForOutOfRange = true, SInt32 adjust = 0);

  //! Create NUVarselRvalue with given rvalue  and unnormalized select.
  static NUVarselRvalue* genVarselRvalue(NUNet* net, NUExpr* rval, NUExpr* selExpr,
                                         const SourceLocator& loc,
                                         bool retNullForOutOfRange = true, SInt32 adjust = 0);

  //! Create NUVarselRvalue with given rvalue, unnormalized select and part select range.
  static NUVarselRvalue* genVarselRvalue(NUNet* net, NUExpr* rval,
                                         ConstantRange range, NUExpr* selExpr,
                                         const SourceLocator& loc,
                                         bool retNullForOutOfRange = true, SInt32 adjust = 0);

  //! Create NUVarselRvalue with given net and unnormalized select.
  static NUVarselRvalue* genVarselRvalue(NUNet* net, NUExpr* selExpr,
                                         const SourceLocator& loc,
                                         bool retNullForOutOfRange = true, SInt32 adjust = 0);

  //! Create NUVarselRvalue with given range and unnormalized select.
  static NUVarselRvalue* genVarselRvalue(NUNet* net, ConstantRange range, NUExpr* selExpr,
                                         const SourceLocator& loc,
                                         bool retNullForOutOfRange = true, SInt32 adjust = 0);
  
  //! List of Cheetah nodes
  typedef UtList<veNode> VeNodeList;

protected:

  //! Strings used do determine if a case stmt is fully specified.
  typedef UtStringSet CaseItemStrings;

  //! Types for the populate c-model functions
  typedef UtMap<UtString, NUNet*> NetLookup;
  typedef UtMap<NUNet*, IODBNucleus::CPort*> TimingFlowMap;
  typedef std::pair<ClockEdge, NUNet*> EdgeNet;
  struct CompareEdgeNet;
  friend struct CompareEdgeNet;
  typedef UtMap<EdgeNet*, NUCModelFn*, CompareEdgeNet> CModelFunctions;

  struct CompareEdgeNet
  {
    bool operator()(const EdgeNet* en1, const EdgeNet* en2) const;
  };
private:
  ErrorCode cModelPort(NUCModelInterface* cmodelInterface, UInt32 variant, const char* name, PortDirectionT dir, UInt32 size, UInt32 index, const SourceLocator& loc, bool isNullPort);
  ErrorCode cModelFunction(IODBNucleus::CPortTimingType type, NUNet* clkNet, NUCModelInterface* cmodelInterface, CModelFunctions& functions, NUCModelFn **the_fn);
  ErrorCode cModelArgConnection(NUCModelCall* cmodelCall, NUCModelPort* port, NUNet* net);
  ErrorCode cModelConnectAllPorts(NUCModelInterface * cmodelInterface, NUCModelFn * cmodelFn, NUCModelPort * outPort, NUNet * excludeThisNet, NetLookup & portNetMap);
  ErrorCode findCModelNet(NUCModelPort * port, NetLookup & primaryMap, const SourceLocator &loc, NUNet **the_net);

  //! Return true if all the select values are fully enumerated in the list.
  bool      reduceCaseItems(const CaseItemStrings& caseItemStrings);

  //! Normalize the given select expression based on the net's declared/operational range.
  static NUExpr* normalizeSelect(NUNet* net, NUExpr* selExpr,
                                 SInt32 adjust, bool retNullForOutOfRange);

protected:
  Populate( MsgContext* c, IODBNucleus* iodb,
            DesignPopulate *design_populate,
            STSymbolTable *symtab,
            AtomicCache *str_cache,
            SourceLocatorFactory *loc_factory,
            FLNodeFactory *flow_factory,
            NUNetRefFactory *netref_factory,
            ArgProc *arg, 
            CarbonContext* cc );
  

  /*! return true if tempCode has a value that indicates something is
    incomplete in population.  The tru values means we should stop
    processing now, if the code is not fatal then set resultErrorCode
    to eFailNoCreate.
  */
  static bool errorCodeSaysReturnNowIfIncomplete(ErrorCode tempCode,
                                                 ErrorCode *resultErrorCode);

  //! Compose and emit an error message when a bitsel is out of range of 
  //! the net that is either a vector or a memory.
  void VectOrMemExprRangeError(NUNet* net, NUExpr* bitsel_expr, const SourceLocator& loc);

  //! Compose and emit an error message when a bitsel is out of range of the vector
  void ExprRangeError( NUVectorNet *vn, NUExpr *bitsel_expr, const SourceLocator &loc);

  //! Compose and emit an error message when a bitsel is out of range of the memory width
  void ExprRangeError( NUMemoryNet *mem, NUExpr *bitsel_expr, const SourceLocator &loc);
  ErrorCode singleBitEdgeExpr(ClockEdge edge, NUExpr* clkExpr, LFContext* context, NUEdgeExpr** the_expr);


  ErrorCode cModel(const char* name, NUModule* module, LFContext* context, NUCModel **the_inst, bool isModule);
  ErrorCode cModulePorts(NUModule * module, NUCModel * cmodel, LFContext *, NetLookup & inputNetMap, NetLookup & outputNetMap);
  ErrorCode cModuleDirectives(const char* modName, NUModule * module, LFContext * context, NetLookup & inputNetMap, NetLookup & outputNetMap, TimingFlowMap & timingFlowMap);
  ErrorCode cModelFunctions(LFContext*, NUCModelInterface* cmodelInterface, NUUseDefNode* useDef, NetLookup& inputNetMap, NetLookup& outputNetMap, TimingFlowMap& timingFlowMap, CModelFunctions& functions);
  ErrorCode cModelAlwaysBlock(NUModule * module, LFContext * context, const EdgeNet * edgeNet, NUCModel * cmodel, NUCModelFn * cmodelFn, NetLookup & inputNetMap, NetLookup & outputNetMap);
  ErrorCode taskCModelFn(NUCModel* cmodel, const SourceLocator& loc, NUCModelFn** cmodelFn);
  ErrorCode taskPorts(NUCModel* cmodel, const char* name, LFContext* context,
                      const SourceLocator& loc);

  /*!
   * Compute the size in UInt32s it would take to store this
   * net. Scalars would take one and vectors can take one or more.
   */
  UInt32 getSize(NUNet* net);


  //! Method to create an InitialBlock if none in module & then add stmt to it.
  void addStmtToInitialBlock(LFContext *context, NUModule *module, 
                             NUStmt* stmt, const NUBase* proxy,
                             const SourceLocator &loc);

  //! Method to determine if a case statement is fully defined
  //! (without the need for a default). If found to be a full case
  //! then this method will also remove the default case since it is
  //! unnecessary.
  ErrorCode computeFullCase(NUCase** the_stmt, LFContext* context);

public:
  //! Add module instance to given parent scope which can be module or
  //! named declaration scope.
  void addModuleInstance(NUModuleInstance* inst, NUScope* parent);

public:
  //! Fold object used during population, this only does the simple stuff
  // also used to resolve constants properly (for sizing functions)
  Fold *mFold;
  CarbonContext* mCarbonContext;

protected:
  MsgContext *mMsgContext;
  IODBNucleus *mIODB;

  DesignPopulate *mPopulate;
  STSymbolTable *mSymtab;
  AtomicCache *mStrCache;
  SourceLocatorFactory *mLocFactory;
  FLNodeFactory *mFlowFactory;
  NUNetRefFactory *mNetRefFactory;
  ArgProc *mArg;

private:
  //! disallowed
  Populate();
  //! disallowed
  Populate( Populate &);
};
#endif
