// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef LOCALANALYSIS_H_
#define LOCALANALYSIS_H_

#include "nucleus/Nucleus.h"
#include "reduce/Flatten.h"
#include "reduce/FlattenAnalyzer.h"
#include "reduce/Inference.h"
#include "reduce/Ternary.h"
#include "reduce/ControlMerge.h"
#include "reduce/ControlExtract.h"
#include "reduce/UnelabMerge.h"
#include "reduce/Rescope.h"
#include "reduce/Separation.h"
#include "reduce/LoopUnroll.h"
#include "reduce/VectorMatch.h"
#include "reduce/ConcatRewrite.h"
#include "reduce/AssignAlias.h"
#include "reduce/Atomize.h"
#include "SensitivityList.h"

class Stats;
class AtomicCache;
class FLNodeFactory;
class MsgContext;
class IODBNucleus;
class ArgProc;
class EncoderStatistics;
class LFSplitAssigns;
class WiredNetResynth;
class REClockGateStats;
class LocalPropagationStatistics;
class AllocAlias;
class CodeMotionStatistics;

//! Analyze the unelaborated design in a bottom-up traversal
class LocalAnalysis
{
public:
  //! constructor
  LocalAnalysis(bool phase_stats,
		bool module_phase_stats,
		Stats *stats,
		AtomicCache *string_cache,
		NUNetRefFactory *netref_factory,
		FLNodeFactory *flow_factory,
		MsgContext *msg_context,
		IODBNucleus *iodb,
		ArgProc *arg,
		STSymbolTable *symtab,
		SourceLocatorFactory *loc_factory,
		FLNodeElabFactory *flow_elab_factory,
                const char* congruencyInfoFilename,
                WiredNetResynth* wiredNetResynth,
                TristateModeT tristateMode,
                const char *file_root = NULL);

  //! destructor
  ~LocalAnalysis();

  enum AnalysisMode {
    eAnalysisModeEarlyFlatten, //!< 1st pass; minimal flattening.
    eAnalysisModeMinimal,      //!< 2nd pass; prior to global opts (const prop)
    eAnalysisModePort,         //!< 3rd pass; port coercion & splitting
    eAnalysisModeNormal,       //!< 4th pass; flattening + optimizations
    eAnalysisModePostFlatten   //!< 5th pass; post-flattening optimizations
  };

  //! Initiate a bottom-up local analysis of the design
  /*!
   *  \param design The design to analyze
   *
   *  \param analysisMode Run the appropriate set of analysis. Minimal
   *  is used by Global Optimizations to get an early elaborated view
   *  of the design. Normal is used by Local Analysis for full
   *  analysis/optimization. 
   */
  void analyzeDesign(NUDesign *design, 
                     AnalysisMode analysisMode,
                     UtOStream * flattening_trace_file=NULL);

  //! Initiate port analysis to determine the correct direction of design ports.
  void analyzeDesignPorts(NUDesign *design);

  //! Call only once - adds the relevant command line args
  static void addCommandlineArgs(ArgProc *arg);

private:
  //! Abstraction for a module analysis phase
  typedef void (LocalAnalysis::*PhaseFn)(NUModule* module, UInt32 flags);

  //! Abstraction to test whether a module phase should execute or not
  typedef bool (LocalAnalysis::*PhaseEnabled)(UInt32 flags);

  //! Class that maintains information for a given phase
  class Phase
  {
  public:
    //! constructor
    /*! Create an analyze module phase. The name is used for debugging
     *  and phase stats. The phaseStat boolean indicates whether a
     *  phase stat line should be printed for this phase.
     */
    Phase(const char* name, UInt32 flags, bool printStats, 
          PhaseFn fn,
          PhaseEnabled enabled) :
      mName(name), mFlags(flags), mPrintStats(printStats),
      mFn(fn), mEnabled(enabled)
    {}

    //! Get the phase enable function
    PhaseEnabled getEnabledFn() const { return mEnabled; }

    //! Get the phase function
    PhaseFn getPhaseFn() const { return mFn; }

    //! Get the phase flags
    UInt32 getFlags() const { return mFlags; }

    //! Get the phase name
    const UtString& getName() const { return mName; }

    //! Get whether stats should be printed
    bool getPrintStats() const { return mPrintStats; }

  private:
    //! The phase name
    UtString mName;

    //! Flags to pass to the runPhase function
    UInt32 mFlags;

    //! If set, print phase stats
    bool mPrintStats;

    //! Function to execute a phase
    PhaseFn mFn;

    //! Function to test whether a phase should execute
    PhaseEnabled mEnabled;
  }; // class Phase

  //! A sequence of analyze module phases
  typedef UtVector<Phase> Phases;
    
  //! Function to run all the phases for a given analyze module pass
  void analyzeModule(NUModule* module, const Phases& phases);

  //! Analyze sub-modules before this module
  void analyzeModuleRecurse(NUModule *module, AnalysisMode analysisMode,
                            FlattenAnalyzer::Mode flattenMode,
                            const Phases& phases);

  //! Perform local analysis on the given module
  void setupAnalyzeModule(Phases* phases);

  //! Setup the optimizations for the post flattening local analysis
  void setupPostFlattenAnalyzeModule(Phases* phases);

  //! Perform minimum local analysis on the given module
  void setupMinimumAnalyzeModule(Phases* phases);

  //! Perform early flattening analysis on the given module
  void setupEarlyFlattenAnalyzeModule(Phases* phases);

  //! Returns true if analysis should prematurely exit.
  bool shouldExit();

  //! Returns true if early flattening is enabled
  bool earlyFlattenEnabled(UInt32);

  //! Run early flattening 
  void runEarlyFlattening(NUModule* module, UInt32 flags);

  //! Return true if we want to eliminate statements from modules that are no lnoger instantiated.
  bool cleanDeadModulesEnabled(UInt32);

  //! clean the blocks and statements from the modules that are no longer
  //! instantiated, leaving the net declarations
  void runCleanDeadModules(NUModule*,UInt32);

  //! Returns true if congruence is enabled.
  bool congruenceEnabled(UInt32);

  //! Run congruence over the tasks in a module.
  void runCongruence(NUModule * module, UInt32 flags);

  //! Returns true if port lowering is enabled
  bool lowerPortsEnabled(UInt32);

  //! Runs port lowering
  void runLowerPorts(NUModule* module, UInt32 flags);

  //! Returns true if flattening is enabled
  bool flattenModuleEnabled(UInt32);

  //! Runs flattening
  void runFlattenModule(NUModule* module, UInt32 flags);

  //! returns true if vector match is enabled
  bool vectorMatchEnabled(UInt32 flags);

  //! Runs vector matching (obsolete?)
  void runVectorMatch(NUModule* module, UInt32);

  //! Returns true if vector inferencing is enabled
  bool inferenceEnabled(UInt32);

  //! Run vector inferencing
  /*! \note Vector inference can leave behind suboptimal concat constructs. It
   *  must be followed by concat rewrite.
   */
  void runInference(NUModule* module, UInt32);

  //! Returns true if separation is enabled
  bool separationEnabled(UInt32);

  //! Run separation
  void runSeparation(NUModule* module, UInt32);

  //! Returns true if block resynthesis is enabled
  bool blockResynthesisEnabled(UInt32);

  //! Run block resynthesis
  void runBlockResynthesis(NUModule* module, UInt32);

  //! returns true if remux is enabled
  bool remuxEnabled(UInt32);

  //! Run remux - the flags indicate the first or second pass (0 or 1)
  void runRemux(NUModule* module, UInt32 flags);

  //! Returns true if rescoping nets is enabled
  bool rescopeEnabled(UInt32);

  //! Run rescope nets
  void runRescope(NUModule* module, UInt32);

  //! Return true if dynamic index to case conversion is enabled.
  bool dynIdxToCaseEnabled(UInt32);

  //! Convert dynamic indices to case statements.
  void runDynIdxToCase(NUModule* module, UInt32);

  //! Returns true if converting ifs to case statements is enabled
  bool ifCaseEnabled(UInt32);

  //! Convert ifs to case statements
  void runIfCase(NUModule* module, UInt32);

  //! returns true if redundant assign removal is enabled
  bool redundantAssignsEnabled(UInt32);

  //! Run remove redundant assigns
  void runRedundantAssigns(NUModule* module, UInt32);

  //! Returns true if constant folding is enabled
  bool foldEnabled(UInt32);

  //! Run fold - the flags are described by the FoldFlags enum
  void runFold(NUModule* module, UInt32 foldFlags);

  //! Returns true if priority enconder rewrite is enabled
  bool priorityEncoderRewriteEnabled(UInt32);

  //! Run priority encoder rewrite
  void runPriorityEncoderRewrite(NUModule* module, UInt32);

  //! Returns true if ternary creation is enabled
  bool ternaryEnabled(UInt32);

  //! Run ternary creation
  void runTernary(NUModule* module, UInt32);

  //! Returns true if control merge is enabled
  bool controlMergeEnabled(UInt32);

  //! Run control merge
  void runControlMerge(NUModule* module, UInt32);

  //! Aliasing options
  enum AliasingFlags
  {
    eAliasingBuildFlow = 1,     //!< Build flow before aliasing
    eAliasingDoFold = 2,        //!< Run fold after aliasing
    eAliasingRemoveFlow = 4,    //!< Remove flow after everything is done
    eAliasingClearDeadLogic = 8 //!< Clear dead logic before doing aliasing
  };

  //! returns true if assignment aliasing is enabled
  bool aliasingEnabled(UInt32);

  //! Run aliasing - see AliasingFlags for the aliasing options
  void runAliasing(NUModule* module, UInt32 flags);

  //! Returns true if break net cycles is enabled
  bool breakNetCyclesEnabled(UInt32);

  //! Run break net cycles to remove false cycles created by vectored nets
  void runBreakNetCycles(NUModule* module, UInt32);

  //! Returns true if latch cycles are removal is enabled
  bool removeLatchCyclesEnabled(UInt32);

  //! Run remove latch cycles
  void runRemoveLatchCycles(NUModule* module, UInt32);

  //! Return true if dead net bits should be removed
  bool deadNetBitsEnabled(UInt32);

  //! Optimize away any dead net bits
  void runDeadNetBits(NUModule* module, UInt32 flags);

  //! Returns true if latch recognition is enabled
  bool latchRecognitionEnabled(UInt32);

  //! Run latch recognition - when this is done the NUNet latch flag is set
  void runLatchRecognition(NUModule* module, UInt32);

  //! Returns true if the clock gate optimization is enabled.
  bool clockGateEnabled(UInt32);

  //! Run the clock gate optimization - moves enables from the clock to the flop
  void runClockGate(NUModule* module, UInt32);

  //! Returns true if the gated clock latch conversion is enabled
  bool convertLatchesEnabled(UInt32);

  //! Run the convert latches pass - currently converts latches in clock trees
  void runConvertLatches(NUModule* module, UInt32);

  //! Returns true if the unelaborated merge is enabled
  bool mergeUnelabEnabled(UInt32);

  //! Run merge unelab - merges cont assigns into always blocks
  void runMergeUnelab(NUModule* module, UInt32);

  //! Returns true if local constant propagation is enabled
  bool localConstPropEnabled(UInt32);

  //! Run local constant propagation - propagates constants within an always block
  void runLocalConstProp(NUModule* module, UInt32);

  //! Returns true if local copy propagation is enabled
  bool localCopyPropEnabled(UInt32);

  //! Run local copy propagation
  void runLocalCopyProp(NUModule* module, UInt32);

  //! Returns true if tristate analysis is enabled
  bool tristateAnalysisEnabled(UInt32);

  //! Run tristate analysis - determines tristates and resolves conflicts
  void runTristateAnalysis(NUModule* module, UInt32);

  //! Returns true if flow should be built
  bool buildFlowEnabled(UInt32);

  //! Runs the build flow pass
  void runBuildFlow(NUModule* module, UInt32 flags);

  //! Returns true if mark read/write nets is enabled
  bool markReadWriteNetsEnabled(UInt32);

  //! Run mark read write nets - computes if nets are read and written
  void runMarkReadWriteNets(NUModule* module, UInt32);

  //! Returns true if dead logic removal should be enabled
  bool clearDeadLogicEnabled(UInt32);

  //! Runs the dead logic removal pass
  void runClearDeadLogic(NUModule* module, UInt32 flags);

  //! Returns true if tristate warnings are enabled
  bool tristateWarningEnabled(UInt32);

  //! Run tristate warning
  void runTristateWarning(NUModule* module, UInt32);

  //! Returns true if the remove unelaborated flow pass is enabled;
  bool removeFlowEnabled(UInt32);

  //! Remove all unelaborated flow
  void runRemoveFlow(NUModule*, UInt32);

  //! Perform flattening, port lowering, inferencing.
  void lowerModule(NUModule * module, FlattenAnalyzer::Mode flattenMode);

  //! Returns true if ternary gate optimization should be enabled
  bool ternaryGateEnabled(UInt32);

  //! Optimize ternary gates
  void runTernaryGate(NUModule* module, UInt32);

  //! returns true if the unelaborated AllocAlias data should be created
  bool unelabAllocAliasEnabled(UInt32);

  //! Create and populate the unelabAllocAlias class (used by atomize etc..)
  /*! This pass populates the data in mAllocAlias. It should not be
   *  run if the data is already created.
   */
  void runUnelabAllocAlias(NUModule* module, UInt32 flags);

  //! Returns true if the unelaborated AllocAlias data should be removed
  bool deleteUnelabAllocAliasEnabled(UInt32);

  //! Delete the Unelab Alloc alias class
  /*! This deletes the data created by runUnelabAllocAlias and stored
   *  in mAllocAlias.
   */
  void runDeleteUnelabAllocAlias(NUModule*, UInt32);

  //! Returns true if the break Z constants pass is enabled
  bool breakZConstsEnabled(UInt32);

  //! Break Z constants
  void runBreakZConsts(NUModule* module, UInt32);

  //! Returns true if synthesis/simulation analysis pass is enabled
  bool synthEnabled(UInt32);

  //! Runs the check for synthesis/simulation analysis for verilog designs
  void runCheckSensitivity(NUModule* module, UInt32);

  //! Returns true if the async reset breakup pass is enabled
  bool asyncResetEnabled(UInt32);

  //! Break up async reset flops
  void runAsyncReset(NUModule* module, UInt32);

  //! returns true if the tristate resynthesis pass is enabled
  bool tristateResynthEnabled(UInt32);

  //! Run tristate resynthesis
  void runTristateResynth(NUModule* module, UInt32);

  //! Returns true if the reorder pass is enabled
  bool reorderEnabled(UInt32);

  //! Run reorder (non-blocking conversion and optimize statement order)
  void runReorder(NUModule* module, UInt32);

  //! Returns true if the self cycle reorder pass is enabled
  bool selfCycleReorderEnabled(UInt32);

  //! Remove combo block self cycles by reordering top level statements
  void runSelfCycleReorder(NUModule* module, UInt32);

  //! Returns true if the initial block cleanup is enabled
  bool initialBlockCleanupEnabled(UInt32);

  //! Remove initial block statements with non-const rhs
  void runInitialBlockCleanup(NUModule* module, UInt32);

  //! Wrapper around module flattening -- queries switch values and performs multiple flattening passes.
  void flattenModule(NUModule *module, bool early_flattening_pass);

  //! Add buffers for incomplete ports; helps codegen.
  void lowerPorts(NUModule * module, bool lower_everything);

  //! Returns true if the motion/extract pass is enabled
  bool motionAndExtractionEnabled(UInt32);

  //! Apply code motion/control extraction in an iterative fashion
  /*!
   * CodeMotion exposes optimization opportunities to ControlExtract
   * and vice versa. This method applies both these optimizations in
   * an iterative fashion.
   *
   * Reorder is used to reorder the statements within each always
   * block in the hope that further ControlExtract (and therefore
   * CodeMotion) opportunities are exposed.
   */
  void applyMotionAndExtraction(NUModule * module, UInt32);

  //! Mark this module as being traversed
  void rememberVisited(NUModule *module);

  //! Query if this module has been traversed already
  bool haveVisited(NUModule *module);

  //! Print global statistics for some passes
  void dumpGlobalStatistics() const;

  //! Clear the UD information
  void clearUD(NUModule* module);

  //! Traverse the design performing u/d computation.
  void buildUD(NUModule* module, bool computeModuleUD);

  //! build the unelaborated flow graph for a module
  void buildFlow(NUModule* module, bool recurse);

  //! remove the unelaborated flow graph
  void removeFlow();

  //! remove all the dead logic
  void clearDeadLogic(NUModule* module, bool startAtAllNets);

  //! Perform assignment aliasing, functional equivalence, and a new continuous
  //! assignment aliasing pass that handles multiple drivers to a net
  void performLocalAliasing(NUModule *module, bool doBuildFlow, bool doFold,
                            bool doRemoveFlow, bool doClearDeadLogic);

  //! Returns true if atomize is enabled
  bool atomizeEnabled(UInt32);

  //! Run the Atomize analysis.
  /*!
    \param module The target module.
    \param flags - non zero if UD needs recalculation upon change.
   */
  void runAtomize(NUModule * module, UInt32 flags);

  //! Returns true if concat-rewrite is enabled
  bool concatRewriteEnabled(UInt32);

  //! Run the concat-rewrite analysis on a single module.
  /*!
    \param module The target module.
    \param flags - non zero means recalculate ud on change
   */
  void runConcatRewrite(NUModule * module, UInt32 flags);

  //! Returns true if build UD is enabled
  bool buildUDEnabled(UInt32);

  //! Runs the pass that builds UD for a single module
  void runBuildUD(NUModule* module, UInt32 flags);

  //! returns true if loop unrolling is enabled
  bool loopUnrollEnabled(UInt32);

  //! Run the loop unrolling analysis on a single module.
  /*!
    \param module The target module.
   */
  void runLoopUnroll(NUModule * module, UInt32);

  //! Keep track of phase statistics?
  bool mPhaseStats;

  //! Keep track of module-level phase statistics?
  bool mModulePhaseStats;

  //! The flattening mode for the currently processed module
  FlattenAnalyzer::Mode mFlattenMode;

  //! Phase statistics
  Stats *mStats;

  //! String cache
  AtomicCache *mStrCache;

  //! Netref factory
  NUNetRefFactory *mNetRefFactory;

  //! Flow factory
  FLNodeFactory *mFlowFactory;

  //! Message Context
  MsgContext *mMsgContext;

  //! IODB
  IODBNucleus *mIODB;

  //! Cmdline argument info
  ArgProc *mArg;

  //! Symbol table
  STSymbolTable *mSymtab;

  //! Source locator factory
  SourceLocatorFactory *mLocFactory;

  //! Flow elab factory
  FLNodeElabFactory *mFlowElabFactory;

  //! Keep track of which modules have been visited
  NUModuleSet mVisitedModules;

  //! Global design cost
  NUCostContext *mDesignCostCtx;

  //! Unelab Alloc Alias class shared by multiple passes
  AllocAlias* mAllocAlias;

  //! Global statistics objects for some of the passes
  FlattenStatistics      mFlattenStats;
  InferenceStatistics    mInferenceStats;
  LoopUnrollStatistics   mLoopUnrollStats;
  EncoderStatistics    * mEncoderStats;
  TernaryStatistics      mTernaryStats;
  ControlMergeStatistics mControlMergeStats;
  ControlExtractStatistics mControlExtractStats;
  UnelabMergeStatistics  mUnelabMergeStats;
  RescopeStatistics      mRescopeStats;
  SeparationStatistics   mSeparationStats;
  CodeMotionStatistics * mCodeMotionStats;
  VectorMatchStatistics  mVectorMatchStats;
  ConcatStatistics mConcatRewriteStats;
  AtomizeStatistics      mAtomizeStats;
  LFSplitAssigns*        mSplitAssigns;
  AssignAliasStatistics  mAssignAliasStats;
  REClockGateStats*      mClockGateStats;
  LocalPropagationStatistics * mLocalConstantPropagationStatistics;
  LocalPropagationStatistics * mLocalCopyPropagationStatistics;

  //! Flags to indicate whether to print statistics or not
  bool mFlattenVerbosity;
  bool mInferenceVerbosity;
  bool mCollapseVerbosity;
  bool mLoopUnrollVerbosity;
  bool mEncoderVerbosity;
  bool mTernaryVerbosity;
  bool mControlMergeVerbosity;
  bool mControlExtractVerbosity;
  bool mUnelabMergeVerbosity;
  bool mRescopeVerbosity;
  bool mSeparationVerbosity;
  bool mCodeMotionVerbosity;
  bool mVectorMatchVerbosity;
  bool mVectorMatchPortVerbosity;
  bool mConcatRewriteVerbosity;
  bool mAtomizeVerbosity;
  bool mVerboseBreakNetCycles;
  bool mVerboseLocalPropagation;
  SInt32 mAliasingVerbosity;

  //! Flattening trace file
  UtOStream * mFlatteningTraceFile;

  //! The design.
  NUDesign * mDesign;

  //! Wired net resynthesizer
  WiredNetResynth* mWiredNetResynth;

  //! Filename for congruency info
  UtString mCongruencyInfoFilename;

  //! Are we keeping a Z state at runtime?
  TristateModeT mTristateMode;

  //! file name root from the carbon context
  const char *mFileRoot;

  //! Sensitivity list checker
  SensitivityList *mSensList;
};

#endif
