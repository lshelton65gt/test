// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef TFREWRITE_H_
#define TFREWRITE_H_

#include "nucleus/Nucleus.h"
#include "util/UtList.h"
#include "util/UtMap.h"

class MsgContext;
class AtomicCache;
class IODBNucleus;
class ArgProc;
class Fold;

/*!
  \file
  Declaration of task/function rewrite package.
 */

//! Class to handle non-local references and non-blocking assigns inside
//! tasks and functions.
/*!
 * It is assumed that this is called very early in the compiler, before any
 * UD computation, etc, has been done -- this pass only modifies the Nucleus
 * tree.
 */
class TFRewrite
{
public:
  //! constructor
  TFRewrite(NUNetRefFactory *netref_factory,
	    MsgContext *msg_ctx,
	    AtomicCache *string_cache,
            bool inlineSingleTaskCalls,
            bool inlineLowCostTaskCalls,
	    bool verbose,
            IODBNucleus *iodb,
            ArgProc* args);

  //! destructor
  ~TFRewrite();

  //! Perform rewrite over the whole design
  void design(NUDesign *design);

  //! Perform rewrite on the given module
  void module(NUModule *module);

  //! Perform rewrite on the given module.
  bool alwaysBlock(NUAlwaysBlock * always);

  //! Rewrite the given task/function, if necessary.
  /*!
   * Recurses to make sure all invoked tasks and functions are handled first.
   */
  void tf(NUTF *this_tf);

  //! Rewrite the given task enable
  /*!
   * If the task being invoked referenced non-local nets, then append the
   * extra arg connections for those nets.
   *
   * If the task was inlined, then the replacement statement is returned.
   */
  NUStmt* rewriteTaskEnable(NUTaskEnable *task_enable, NUScope *scope);

  //! Increment the call count for this particular task.
  void rememberTFCall(NUTask * this_tf);
private:
  //! Inline the given task enable, return the block created.
  NUBlock *inlineTask(NUTaskEnable *task_enable, NUScope *scope);

  //! Remove any local tasks which must be inlined
  void removeInlinedTasks(NUModule *module);

  //! Iterate over all local tasks and functions, trying to rewrite them
  void findTFToRewrite(NUModule *module);

  //! Analyze the given task/function, determine if it should be rewritten, and rewrite it
  void analyzeRewrite(NUTF *tf);

  //! Create net hierrefs for non-local net references for a task which is hierref'd
  /*!
   * The given task has hierarchical references to it.
   * If the task also uses or defs non-local nets, then the hierarchical referrers
   * of the task will also need to hierarchically refer to the non-local nets the
   * task uses or defs.
   *
   \verbatim
   module top();
     always
       child.mem_ref(data, addr);
     child child ();
   endmodule
   module child();
     reg [1:0] mem [1:0];
     task mem_ref;
       input [1:0] data;
       input addr;
       mem[addr] = data;
     endtask;
   endmodule;
   \endverbatim
   * In this example, the task child.mem_ref defs child.mem, which is non-local
   * to the task.  Since top hierarchically refers to child.mem_ref, we must
   * make sure top also has hierarchical references to child.mem.
   */
  void createTaskNonLocalHierrefs(NUTask *task,
                                  NUNetSet &non_local_uses,
                                  NUNetSet &non_local_defs);

  //! Worker for createTaskNonLocalHierrefs; creates a hierref to the given net for all hierrefs of the given task.
  void createTaskNonLocalHierref(NUTask *task, NUNet *non_local_net);

  //! Worker for createTaskNonLocalHierref; creates a hierref to the given net for the given hierarchical task enable
  /*!
    \param task_enable Task enable of the task containing the non-local reference
    \param non_local_net Net which is being non-locally referenced by the task
    \param base_name Name of the net in the task, this will be a hierarchical path if
                     the non-local reference is a hierarchial reference
   */
  void createTaskEnableNonLocalHierref(NUTaskEnableHierRef *task_enable,
                                       NUNet *non_local_net,
                                       const AtomArray& base_name);

  //! Return a clone of the given net_hier_ref in the given module
  NUNet *cloneHierRef(NUNet *net_hier_ref, NUModule *module);

  //! Mark tasks with only one caller as needing inlining.
  void markSingleInstanceTasks(NUModule * this_module);

  //! Return true if have visited this scope.
  bool haveVisited(NUScope *scope) const;

  //! Remember that have visited this scope.
  void rememberVisited(NUScope *scope);

  //! Map from task to the number of encountered calls.
  typedef UtMap<NUTask*,SInt32> TaskCallMap;
  TaskCallMap mTaskCalls;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! String cache
  AtomicCache *mStringCache;

  //! Message context
  MsgContext *mMsgContext;

  //! Inline tasks with non-local references?
  bool mInlineNonLocalReferences;

  //! Did we inline a task?
  bool mInlinedTask;

  //! Should we force the inlining of tasks with a single enabling call?
  bool mInlineSingleTaskCalls;

  //! Should we force the inlining of tasks with a single enabling call?
  bool mInlineLowCostTaskCalls;

  //! Be verbose as processing occurs?
  bool mVerbose;

  //! Set of scopes which we have visited so far.
  NUScopeSet mScopeSet;

  //! IODB to store intrinsic type information into
  IODBNucleus *mIODB;

  //! args
  ArgProc* mArgs;

  //! multiple level hierarchical tasks
  bool mMultiLevelHierTasks;

  //! fold context
  Fold* mFold;

  NUCostContext * mCost;

  //! deferred task deletions
  NUTaskList* mDeleteTasks;
};

#endif
