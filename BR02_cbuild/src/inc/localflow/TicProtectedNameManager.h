// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef __TICPROTECTEDNAMEMANAGER_H_
#define __TICPROTECTEDNAMEMANAGER_H_

#include "compiler_driver/Interra.h"
#include "util/CarbonAssert.h"
#include "util/SourceLocator.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"

class TicProtectedNameManager
{
public:
  TicProtectedNameManager(SourceLocatorFactory *loc_factory);
  ~TicProtectedNameManager();

  //! map for verilog protected objects
  typedef UtHashMap<veNode, UtString> VeNodeToVisibleNameMap;
  VeNodeToVisibleNameMap mVeNodeToVisibleNameMap;


  //! Create a source locator for the given Cheetah object
  SourceLocator locator(veNode ve_node);

  //! put the user visible name for \a ve_node into \a buf, return true if ve_node has a name
  /*! this routine is necessary to hide `protected regions from the user
   */
  bool getVisibleName( veNode ve_node, UtString* buf, bool get_orig_name = false);

  //! find the verilog module that has a visible name of \a name
  veNode CheetahFindModule(const char* name, veNode node);

  //! find the VHDL entity that has a visible name of \a name
  vhNode JaguarFindModule(const char* name);

private:
  TicProtectedNameManager( const TicProtectedNameManager& );
  TicProtectedNameManager& operator=( const TicProtectedNameManager& );
  
  SourceLocatorFactory *mSourceLocatorFactory;
};

#endif // __TICPROTECTEDNAMEMANAGER_H_
