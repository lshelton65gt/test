// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef DESIGNWALKER_H_
#define DESIGNWALKER_H_

#include "nucleus/Nucleus.h"

class IODB;

//! Generic design traversal.
/*!
 * Abstract base class for LocalFlow design-based analysis.
 *
 * Traverse a design calling a module() method to perform the
 * recursive traversal of all top-level modules. This module() method
 * can implement the recursion in any manner it chooses.
 * 
 * Methods for remembering the set of visited modules/scopes are
 * provided.
 */
class DesignWalker
{
public:
  //! constructor
  /*!
   * \param verbose If true, will be verbose while analysis occurs.
   */
  DesignWalker (bool verbose) :
    mVerbose(verbose)
  {}

  //! destructor
  virtual ~DesignWalker () {}

  //! Perform a walk over the specified design.
  virtual void design(NUDesign *design);

  //! Recursively walk the specified module.
  /*! 
   * Walk the specified module and recursively perform whatever
   * analysis is required.
   */
  virtual void module(NUModule *module) = 0;

  // the original intent was for the DesignWalker subclasses to use the
  // module() recursive traversal method and call an operate() method to
  // perform per-module analysis. unfortunately, many do not perform a
  // pre-recursion operation -- they have both pre and post-recursion
  // steps. Another pass of refactoring is required to pull the module
  // traversal into DesignWalker.
  //
  // virtual void operate(NUModule *module) = 0;

  //! was this operation configured to run verbosely?
  bool isVerbose() const { return mVerbose; }
protected:
  //! Return true if have visited this module/scope.
  bool haveVisited(NUScope *scope) const;

  //! Remember that have visited this module/scope.
  void rememberVisited(NUScope *scope);

  //! If true, be verbose as we do the computation.
  bool mVerbose;

private:
  //! Hide copy and assign constructors.
  DesignWalker(const DesignWalker&);
  DesignWalker& operator=(const DesignWalker&);

  //! Set of modules/scopes which we have visited so far.
  NUScopeSet mScopeSet;
};

#endif // DESIGNWALKER_H_
