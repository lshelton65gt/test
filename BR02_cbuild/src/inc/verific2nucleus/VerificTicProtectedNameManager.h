// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __VERIFICTICPROTECTEDNAMEMANAGER_H_
#define __VERIFICTICPROTECTEDNAMEMANAGER_H_

#include "util/CarbonAssert.h"
#include "util/SourceLocator.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtSet.h"
#include "LineFile.h"

namespace Verific {
    class VeriTreeNode;
    class VeriModule;
}

namespace verific2nucleus {

class VerificTicProtectedNameManager
{
public:
    VerificTicProtectedNameManager(SourceLocatorFactory *loc_factory);
    ~VerificTicProtectedNameManager();

    bool isTicProtected(Verific::linefile_type lf);
public:
    //! retun the user visible name for \a ve_node if it's from protected region, otherwise return original name
    /*! this routine is necessary to hide `protected regions from the user
     */
    UtString getVisibleName(Verific::VeriTreeNode* ve_node, const UtString& origName);

private:
    VerificTicProtectedNameManager( const VerificTicProtectedNameManager& );
    VerificTicProtectedNameManager& operator=( const VerificTicProtectedNameManager& );

private:
    //! map for verilog protected objects
    typedef UtHashMap<Verific::VeriTreeNode*, UtString> VeNodeToVisibleNameMap;
    VeNodeToVisibleNameMap mVeNodeToVisibleNameMap;
    //! location objects factory 
    SourceLocatorFactory *mSourceLocatorFactory;
}; // class VerificTicProtectedNameManager

} // namespace verific2nucleus

#endif // __VERIFICTICPROTECTEDNAMEMANAGER_H_
