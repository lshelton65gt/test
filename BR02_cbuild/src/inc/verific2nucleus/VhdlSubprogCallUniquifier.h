// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __VHDLSUBPROGCALLUNIQUIFIER_H_
#define __VHDLSUBPROGCALLUNIQUIFIER_H_

// Project headers
#include "verific2nucleus/VhdlCSElabVisitor.h"

namespace verific2nucleus 
{
  using namespace Verific;

  class VhdlSubprogCallUniquifier : public VhdlCSElabVisitor {
  public:
    VhdlSubprogCallUniquifier(CarbonConstraintManager *cm, CarbonStaticElaborator* se);

  public:
    // Vhdl Loop Unrolling
    virtual void VHDL_VISIT(VhdlLoopStatement, node);
    // Guard against walking a subprogram body from some point other than a call
    virtual void VHDL_VISIT(VhdlSubprogramBody, node);
    // This visitor processes overloaded operators.
    virtual void VHDL_VISIT(VhdlOperator, node);
    // This visitor processes function and procedure calls. 
    virtual void VHDL_VISIT(VhdlIndexedName, node) ;
    // This visitor saves statement ptrs for use by operator overload uniqification
    virtual void VHDL_VISIT(VhdlStatement, node);

  private:
    // Return true if calls within the subprogram body for this signature
    // have already been uniquified.
    bool AlreadyUniquified(UtString signature);
    // Replace an overloaded operator with a function call in a given statement
    bool ReplaceChildExpr(VhdlStatement* stmt, VhdlExpression* orig_expr, VhdlExpression* new_expr);
    // Returns true if the expression has an overloaded operator in it.
    bool HasOverloadedOperator(VhdlDiscreteRange* expr);
    // Create a uniquified function call, given an overloaded operator
    VhdlIndexedName* CreateFunctionCall(VhdlOperator* oper, VhdlSubprogramBody* sBody, VhdlDiscreteRange* left, VhdlDiscreteRange* right);
    // Walk subprogram for an overloaded operator
    void VisitOverloadedOperatorSubprogram(VhdlOperator* oper, VhdlSubprogramBody *sBody);
    // Return uniquified subprogram body for overloaded operator
    VhdlSubprogramBody* GetOverloadedOperatorSubprogram(VhdlOperator* oper, bool* alreadyUniquified);
    // Create a new expression with overloaded operators replaced by function calls.
    VhdlDiscreteRange* WalkExprAndReplaceOverloadedOperators(VhdlDiscreteRange* expr);

    virtual void reportFailure(VhdlTreeNode* node, const UtString& msg);

    
    UtSet<UtString> mUniquified;              // Set of signatures corresponding to already uniquified subprograms.
    VhdlStatement* mParentStatement;          // Ptr to statement that owns visited expressions.
  };
  
}; // namespace verific2nucleus

#endif // __VHDLSUBPROGCALLUNIQUIFIER_H_
