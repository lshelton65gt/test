// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*
 *
 * VerilogDesignWalker/VhdlDesignWalker classes
 * @TODO add description
 *
*/

#ifndef __VERIFIC_DESIGN_WALKER
#define __VERIFIC_DESIGN_WALKER

#include "VeriVisitor.h"    // Verilog Visitor base class definition
#include "VhdlVisitor.h"    // Vhdl Visitor base class definition

#include "util/UtString.h"

void createAssertMessage(Verific::VhdlTreeNode* node, UtString * str);
void createAssertMessage(Verific::VeriTreeNode* node, UtString * str);

//! Use this assert in lieu of INFO_ASSERT. This assert prints assert
//! message with source location. (this can be used in InterraDesignWalkerCB or InterraDesignWalker functions
#define CB_ISSUE_ASSERT(condition, node, msg) \
  if (!condition) { \
    UtString assert_msg; \
    createAssertMessage(&node, &assert_msg); \
    assert_msg << " " << msg; \
    INFO_ASSERT(condition, assert_msg.c_str()); \
  } /* caller adds semicolon */


// Ment to use in VerilogDesignWalker
#define VERI_CB_STATUS_CHK(node, status) \
  do { \
    switch ((status)) { \
    case VerilogDesignWalkerCB::NORMAL: break; \
    case VerilogDesignWalkerCB::STOP: return; \
    case VerilogDesignWalkerCB::SKIP: return; \
    case VerilogDesignWalkerCB::INVALID: CB_ISSUE_ASSERT(0, node, "Invalid callback status"); break; \
    } \
  } while (0) /* caller adds semicolon */

// Ment to use in VerilogDesignWalker (vhdl version)
#define VHDL_CB_STATUS_CHK(node, status) \
  do { \
    switch ((status)) { \
    case VhdlDesignWalkerCB::NORMAL: break; \
    case VhdlDesignWalkerCB::STOP: return; \
    case VhdlDesignWalkerCB::SKIP: return; \
    case VhdlDesignWalkerCB::INVALID: CB_ISSUE_ASSERT(0, node, "Invalid callback status"); break; \
    } \
  } while (0) /* caller adds semicolon */

// Ment to use in callback class
#define VERI_CB_STATUS_NESTED_CHK(node, status) \
  do { \
    switch ((status)) { \
    case VerilogDesignWalkerCB::NORMAL: break; \
    case VerilogDesignWalkerCB::STOP: \
    case VerilogDesignWalkerCB::SKIP: return status; \
    case VerilogDesignWalkerCB::INVALID: CB_ISSUE_ASSERT(0, node, "Invalid callback status"); break; \
    } \
  } while (0) /* caller adds semicolon */

// Ment to use in callback class (vhdl version)
#define VHDL_CB_STATUS_NESTED_CHK(node, status) \
  do { \
    switch ((status)) { \
    case VhdlDesignWalkerCB::NORMAL: break; \
    case VhdlDesignWalkerCB::STOP: \
    case VhdlDesignWalkerCB::SKIP: return status; \
    case VhdlDesignWalkerCB::INVALID: CB_ISSUE_ASSERT(0, node, "Invalid callback status"); break; \
    } \
  } while (0) /* caller adds semicolon */

// Extending Verific base interface
namespace Verific {

class VerilogDesignWalkerCB;
class VhdlDesignWalkerCB;

/* -------------------------------------------------------------------------- */

class VerilogDesignWalker : public VeriVisitor
{
public:
    ///@brief 
    VerilogDesignWalker() {}
    virtual ~VerilogDesignWalker() {}
    void sCallBack(VerilogDesignWalkerCB* cb) { mCallBack = cb; }
    // Make sure compile switches VHDL_SPECIALIZED_VISITORS and VERILOG_SPECIALIZED_VISITORS are turned on
    // See verilog/VeriCompileFlags.h vhdl/VhdlCompileFlags.h

/* ================================================================= */
/*                         VISIT VERILOG METHODS                     */
/* ================================================================= */
public:
    // The following class definitions can be found in VeriTreeNode.h
    virtual void VERI_VISIT(VeriTreeNode, node) ;
    virtual void VERI_VISIT(VeriCommentNode, node) ;

    ///// The following class definitions can be found in VeriModule.h
    virtual void VERI_VISIT(VeriModule, node) ;
    virtual void VERI_VISIT(VeriPrimitive, node) ;
    virtual void VERI_VISIT(VeriConfiguration, node) ;

    // The following class definitions can be found in VeriExpression.h
    virtual void VERI_VISIT(VeriExpression, node) ;
    virtual void VERI_VISIT(VeriName, node) ;
    virtual void VERI_VISIT(VeriIdRef, node) ;
    virtual void VERI_VISIT(VeriIndexedId, node) ;
    virtual void VERI_VISIT(VeriSelectedName, node) ;
    virtual void VERI_VISIT(VeriIndexedMemoryId, node) ;
    virtual void VERI_VISIT(VeriConcat, node) ;
    virtual void VERI_VISIT(VeriMultiConcat, node) ;
    virtual void VERI_VISIT(VeriFunctionCall, node) ;
    virtual void VERI_VISIT(VeriSystemFunctionCall, node) ;
    virtual void VERI_VISIT(VeriMinTypMaxExpr, node) ;
    virtual void VERI_VISIT(VeriUnaryOperator, node) ;
    virtual void VERI_VISIT(VeriBinaryOperator, node) ;
    virtual void VERI_VISIT(VeriQuestionColon, node) ;
    virtual void VERI_VISIT(VeriEventExpression, node) ;
    virtual void VERI_VISIT(VeriPortConnect, node) ;
    virtual void VERI_VISIT(VeriPortOpen, node) ;
    virtual void VERI_VISIT(VeriAnsiPortDecl, node) ;
    virtual void VERI_VISIT(VeriTimingCheckEvent, node) ;
    virtual void VERI_VISIT(VeriDataType, node) ;
    virtual void VERI_VISIT(VeriNetDataType, node) ; // VIPER #4896
    virtual void VERI_VISIT(VeriPathPulseVal, node) ;

    // The following class definitions can be found in VeriId.h
    virtual void VERI_VISIT(VeriIdDef, node) ;
    virtual void VERI_VISIT(VeriVariable, node) ;
    //virtual void VERI_VISIT(VeriMemoryId, node) ;
    virtual void VERI_VISIT(VeriInstId, node) ;
    virtual void VERI_VISIT(VeriModuleId, node) ;
    virtual void VERI_VISIT(VeriUdpId, node) ;
    virtual void VERI_VISIT(VeriConfigurationId, node) ;
    virtual void VERI_VISIT(VeriTaskId, node) ;
    virtual void VERI_VISIT(VeriFunctionId, node) ;
    virtual void VERI_VISIT(VeriGenVarId, node) ;
    virtual void VERI_VISIT(VeriParamId, node) ;
    virtual void VERI_VISIT(VeriBlockId, node) ;

    // The following class definitions can be found in VeriMisc.h
    virtual void VERI_VISIT(VeriRange, node) ;
    virtual void VERI_VISIT(VeriStrength, node) ;
    virtual void VERI_VISIT(VeriNetRegAssign, node) ;
    virtual void VERI_VISIT(VeriDefParamAssign, node) ;
    virtual void VERI_VISIT(VeriCaseItem, node) ;
    virtual void VERI_VISIT(VeriGenerateCaseItem, node) ;
    virtual void VERI_VISIT(VeriPath, node) ;
    virtual void VERI_VISIT(VeriDelayOrEventControl, node) ;
    virtual void VERI_VISIT(VeriConfigRule, node) ;
    virtual void VERI_VISIT(VeriInstanceConfig, node) ;
    virtual void VERI_VISIT(VeriCellConfig, node) ;
    virtual void VERI_VISIT(VeriDefaultConfig, node) ;
    virtual void VERI_VISIT(VeriUseClause, node) ;
    #ifdef VERILOG_PATHPULSE_PORTS
    virtual void VERI_VISIT(VeriPathPulseValPorts, node) ;
    #endif

    // The following class definitions can be found in VeriModuleItem.h
    virtual void VERI_VISIT(VeriModuleItem, node) ;
    virtual void VERI_VISIT(VeriDataDecl, node) ;
    virtual void VERI_VISIT(VeriNetDecl, node) ;
    virtual void VERI_VISIT(VeriFunctionDecl, node) ;
    virtual void VERI_VISIT(VeriTaskDecl, node) ;
    virtual void VERI_VISIT(VeriDefParam, node) ;
    virtual void VERI_VISIT(VeriContinuousAssign, node) ;
    virtual void VERI_VISIT(VeriGateInstantiation, node) ;
    virtual void VERI_VISIT(VeriModuleInstantiation, node) ;
    virtual void VERI_VISIT(VeriSpecifyBlock, node) ;
    virtual void VERI_VISIT(VeriPathDecl, node) ;
    virtual void VERI_VISIT(VeriSystemTimingCheck, node) ;
    virtual void VERI_VISIT(VeriInitialConstruct, node) ;
    virtual void VERI_VISIT(VeriAlwaysConstruct, node) ;
    virtual void VERI_VISIT(VeriGenerateConstruct, node) ;
    virtual void VERI_VISIT(VeriGenerateConditional, node) ;
    virtual void VERI_VISIT(VeriGenerateCase, node) ;
    virtual void VERI_VISIT(VeriGenerateFor, node) ;
    virtual void VERI_VISIT(VeriGenerateBlock, node) ;
    virtual void VERI_VISIT(VeriTable, node) ;
    virtual void VERI_VISIT(VeriPulseControl, node) ;

    // The following class definitions can be found in VeriStatement.h
    virtual void VERI_VISIT(VeriStatement, node) ;
    virtual void VERI_VISIT(VeriBlockingAssign, node) ;
    virtual void VERI_VISIT(VeriNonBlockingAssign, node) ;
    virtual void VERI_VISIT(VeriGenVarAssign, node) ;
    virtual void VERI_VISIT(VeriAssign, node) ;
    virtual void VERI_VISIT(VeriDeAssign, node) ;
    virtual void VERI_VISIT(VeriForce, node) ;
    virtual void VERI_VISIT(VeriRelease, node) ;
    virtual void VERI_VISIT(VeriTaskEnable, node) ;
    virtual void VERI_VISIT(VeriSystemTaskEnable, node) ;
    virtual void VERI_VISIT(VeriDelayControlStatement, node) ;
    virtual void VERI_VISIT(VeriEventControlStatement, node) ;
    virtual void VERI_VISIT(VeriConditionalStatement, node) ;
    virtual void VERI_VISIT(VeriCaseStatement, node) ;
    virtual void VERI_VISIT(VeriLoop, node) ;
    virtual void VERI_VISIT(VeriForever, node) ;
    virtual void VERI_VISIT(VeriRepeat, node) ;
    virtual void VERI_VISIT(VeriWhile, node) ;
    virtual void VERI_VISIT(VeriFor, node) ;
    virtual void VERI_VISIT(VeriWait, node) ;
    virtual void VERI_VISIT(VeriDisable, node) ;
    virtual void VERI_VISIT(VeriEventTrigger, node) ;
    virtual void VERI_VISIT(VeriSeqBlock, node) ;
    virtual void VERI_VISIT(VeriParBlock, node) ;

    // The following class definitions can be found in VeriConstVal.h
    virtual void VERI_VISIT(VeriConst, node) ;
    virtual void VERI_VISIT(VeriConstVal, node) ;
    virtual void VERI_VISIT(VeriIntVal, node) ;
    virtual void VERI_VISIT(VeriRealVal, node) ;

    // The following class definitions can be found in VeriLibrary.h
    virtual void VERI_VISIT(VeriLibraryDecl, node) ;

    // The System Verilog Class
    virtual void VERI_VISIT(VeriInterface, node) ;
    virtual void VERI_VISIT(VeriProgram, node) ;
    virtual void VERI_VISIT(VeriClass, node) ;
    virtual void VERI_VISIT(VeriPropertyDecl, node) ;
    virtual void VERI_VISIT(VeriSequenceDecl, node) ;
    virtual void VERI_VISIT(VeriModport, node) ;
    virtual void VERI_VISIT(VeriModportDecl, node) ;
    virtual void VERI_VISIT(VeriClockingDecl, node) ;
    virtual void VERI_VISIT(VeriConstraintDecl, node) ;
    virtual void VERI_VISIT(VeriBindDirective, node) ;
    virtual void VERI_VISIT(VeriPackage, node) ; // Std SV 1800 addition
    virtual void VERI_VISIT(VeriChecker, node) ; // Std SV 1800 addition
    virtual void VERI_VISIT(VeriSequentialInstantiation, node) ; // Std SV 1800 addition
    virtual void VERI_VISIT(VeriTypeRef, node) ;
    virtual void VERI_VISIT(VeriKeyword, node) ;
    virtual void VERI_VISIT(VeriStructUnion, node) ;
    virtual void VERI_VISIT(VeriEnum, node) ;
    virtual void VERI_VISIT(VeriDotStar, node) ;
    virtual void VERI_VISIT(VeriIfOperator, node) ;
    virtual void VERI_VISIT(VeriSequenceConcat, node) ;
    virtual void VERI_VISIT(VeriClockedSequence, node) ;
    virtual void VERI_VISIT(VeriAssignInSequence, node) ;
    virtual void VERI_VISIT(VeriDistOperator, node) ;
    virtual void VERI_VISIT(VeriSolveBefore, node) ;
    virtual void VERI_VISIT(VeriCast, node) ;
    virtual void VERI_VISIT(VeriNew, node) ;
    virtual void VERI_VISIT(VeriConcatItem, node) ;
    virtual void VERI_VISIT(VeriAssertion, node) ;
    virtual void VERI_VISIT(VeriJumpStatement, node) ;
    virtual void VERI_VISIT(VeriDoWhile, node) ;
    virtual void VERI_VISIT(VeriInterfaceId, node) ;
    virtual void VERI_VISIT(VeriProgramId, node) ;
    virtual void VERI_VISIT(VeriCheckerId, node) ;
    virtual void VERI_VISIT(VeriTypeId, node) ;
    virtual void VERI_VISIT(VeriModportId, node) ;
    virtual void VERI_VISIT(VeriNull, node) ;
    virtual void VERI_VISIT(VeriDollar, node) ;
    virtual void VERI_VISIT(VeriAssignmentPattern, node) ;
    virtual void VERI_VISIT(VeriMultiAssignmentPattern, node) ;
    virtual void VERI_VISIT(VeriTimeLiteral, node) ;
    virtual void VERI_VISIT(VeriOperatorId, node) ;
    virtual void VERI_VISIT(VeriOperatorBinding, node) ;
    virtual void VERI_VISIT(VeriForeachOperator, node) ;
    virtual void VERI_VISIT(VeriStreamingConcat, node) ;
    virtual void VERI_VISIT(VeriNetAlias, node) ;
    virtual void VERI_VISIT(VeriTimeUnit, node) ;
    virtual void VERI_VISIT(VeriForeach, node) ;
    virtual void VERI_VISIT(VeriWaitOrder, node) ;
    virtual void VERI_VISIT(VeriCondPredicate, node) ;
    virtual void VERI_VISIT(VeriDotName, node) ;
    virtual void VERI_VISIT(VeriTaggedUnion, node) ;
    virtual void VERI_VISIT(VeriClockingId, node) ;
    virtual void VERI_VISIT(VeriClockingDirection, node) ;
    virtual void VERI_VISIT(VeriClockingSigDecl, node) ;
    virtual void VERI_VISIT(VeriRandsequence, node) ;
    virtual void VERI_VISIT(VeriCodeBlock, node) ;
    virtual void VERI_VISIT(VeriProduction, node) ;
    virtual void VERI_VISIT(VeriProductionItem, node) ;
    virtual void VERI_VISIT(VeriProductionId, node) ;
    virtual void VERI_VISIT(VeriWith, node) ;
    virtual void VERI_VISIT(VeriWithExpr, node) ;
    virtual void VERI_VISIT(VeriInlineConstraint, node) ;
    virtual void VERI_VISIT(VeriWithStmt, node) ;
    virtual void VERI_VISIT(VeriArrayMethodCall, node) ;
    virtual void VERI_VISIT(VeriInlineConstraintStmt, node) ;
    virtual void VERI_VISIT(VeriCovergroup, node) ;
    virtual void VERI_VISIT(VeriCoverageOption, node) ;
    virtual void VERI_VISIT(VeriCoverageSpec, node) ;
    virtual void VERI_VISIT(VeriBinDecl, node) ;
    virtual void VERI_VISIT(VeriBinValue, node) ;
    virtual void VERI_VISIT(VeriOpenRangeBinValue, node) ;
    virtual void VERI_VISIT(VeriTransBinValue, node) ;
    virtual void VERI_VISIT(VeriDefaultBinValue, node) ;
    virtual void VERI_VISIT(VeriSelectBinValue, node) ;
    virtual void VERI_VISIT(VeriTransSet, node) ;
    virtual void VERI_VISIT(VeriTransRangeList, node) ;
    virtual void VERI_VISIT(VeriSelectCondition, node) ;
    virtual void VERI_VISIT(VeriTypeOperator, node) ;
    virtual void VERI_VISIT(VeriImportDecl, node) ;
    virtual void VERI_VISIT(VeriLetDecl, node) ;
    virtual void VERI_VISIT(VeriDefaultDisableIff, node) ;
    virtual void VERI_VISIT(VeriExportDecl, node) ;
    virtual void VERI_VISIT(VeriScopeName, node) ;
    virtual void VERI_VISIT(VeriNamedPort, node) ;
    virtual void VERI_VISIT(VeriPatternMatch, node) ;
    virtual void VERI_VISIT(VeriConstraintSet, node) ;
    virtual void VERI_VISIT(VeriCovgOptionId, node) ;
    virtual void VERI_VISIT(VeriSeqPropertyFormal, node) ;
    virtual void VERI_VISIT(VeriBinsId, node) ;
    virtual void VERI_VISIT(VeriLetId, node) ;
    virtual void VERI_VISIT(VeriCaseOperator, node) ;
    virtual void VERI_VISIT(VeriCaseOperatorItem, node) ;
    virtual void VERI_VISIT(VeriIndexedExpr, node) ;
    virtual void VERI_VISIT(VeriDPIFunctionDecl, node) ;
    virtual void VERI_VISIT(VeriDPITaskDecl, node) ;
    virtual void VERI_VISIT(VeriExternForkjoinTaskId, node) ;
    virtual void VERI_VISIT(VeriPrototypeId, node) ;

///@name Prevent the compiler from implementing the following
private:
    VerilogDesignWalker(VerilogDesignWalker &node);
    VerilogDesignWalker& operator=(const VerilogDesignWalker &rhs);
private:
    ///@brief The object represent pre/post general callback mechanism (template method)
    VerilogDesignWalkerCB* mCallBack;
};


class VhdlDesignWalker : public VhdlVisitor
{
public:
    ///@brief 
    VhdlDesignWalker() {}
    virtual ~VhdlDesignWalker() {}
    void sCallBack(VhdlDesignWalkerCB* cb) { mCallBack = cb; }
/* =///================================================================ */
/*  ///                       VISIT VHDL METHODS                     */
/* =///================================================================ */
public:

    ///// The following class definitions can be found in VhdlTreeNode.h
    virtual void VHDL_VISIT(VhdlTreeNode, node) ;
    virtual void VHDL_VISIT(VhdlCommentNode, node) ;

    ///// The following class definitions can be found in VhdlUnits.h
    virtual void VHDL_VISIT(VhdlDesignUnit, node) ;
    virtual void VHDL_VISIT(VhdlPrimaryUnit, node) ;
    virtual void VHDL_VISIT(VhdlSecondaryUnit, node) ;
    virtual void VHDL_VISIT(VhdlEntityDecl, node) ;
    virtual void VHDL_VISIT(VhdlConfigurationDecl, node) ;
    virtual void VHDL_VISIT(VhdlPackageDecl, node) ;
    virtual void VHDL_VISIT(VhdlContextDecl, node) ;
    virtual void VHDL_VISIT(VhdlPackageInstantiationDecl, node) ;
    virtual void VHDL_VISIT(VhdlArchitectureBody, node) ;
    virtual void VHDL_VISIT(VhdlPackageBody, node) ;

    // The following class definitions can be found in VhdlConfiguration.h
    virtual void VHDL_VISIT(VhdlConfigurationItem, node) ;
    virtual void VHDL_VISIT(VhdlBlockConfiguration, node) ;
    virtual void VHDL_VISIT(VhdlComponentConfiguration, node) ;

    // The following class definitions can be found in VhdlDeclaration.h
    virtual void VHDL_VISIT(VhdlDeclaration, node) ;
    virtual void VHDL_VISIT(VhdlTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlScalarTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlArrayTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlRecordTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlProtectedTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlProtectedTypeDefBody, node) ;
    virtual void VHDL_VISIT(VhdlAccessTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlFileTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlEnumerationTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlPhysicalTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlElementDecl, node) ;
    virtual void VHDL_VISIT(VhdlPhysicalUnitDecl, node) ;
    virtual void VHDL_VISIT(VhdlUseClause, node) ;
    virtual void VHDL_VISIT(VhdlContextReference, node) ;
    virtual void VHDL_VISIT(VhdlLibraryClause, node) ;
    virtual void VHDL_VISIT(VhdlInterfaceDecl, node) ;
    virtual void VHDL_VISIT(VhdlSubprogramDecl, node) ;
    virtual void VHDL_VISIT(VhdlSubprogramBody, node) ;
    virtual void VHDL_VISIT(VhdlSubtypeDecl, node) ;
    virtual void VHDL_VISIT(VhdlFullTypeDecl, node) ;
    virtual void VHDL_VISIT(VhdlIncompleteTypeDecl, node) ;
    virtual void VHDL_VISIT(VhdlConstantDecl, node) ;
    virtual void VHDL_VISIT(VhdlSignalDecl, node) ;
    virtual void VHDL_VISIT(VhdlVariableDecl, node) ;
    virtual void VHDL_VISIT(VhdlFileDecl, node) ;
    virtual void VHDL_VISIT(VhdlAliasDecl, node) ;
    virtual void VHDL_VISIT(VhdlComponentDecl, node) ;
    virtual void VHDL_VISIT(VhdlAttributeDecl, node) ;
    virtual void VHDL_VISIT(VhdlAttributeSpec, node) ;
    virtual void VHDL_VISIT(VhdlConfigurationSpec, node) ;
    virtual void VHDL_VISIT(VhdlDisconnectionSpec, node) ;
    virtual void VHDL_VISIT(VhdlGroupTemplateDecl, node) ;
    virtual void VHDL_VISIT(VhdlGroupDecl, node) ;
    virtual void VHDL_VISIT(VhdlSubprogInstantiationDecl, node) ;
    virtual void VHDL_VISIT(VhdlInterfaceTypeDecl, node) ;
    virtual void VHDL_VISIT(VhdlInterfaceSubprogDecl, node) ;
    virtual void VHDL_VISIT(VhdlInterfacePackageDecl, node) ;

    // The following class definitions can be found in VhdlExpression.h
    virtual void VHDL_VISIT(VhdlExpression, node) ;
    virtual void VHDL_VISIT(VhdlDiscreteRange, node) ;
    virtual void VHDL_VISIT(VhdlSubtypeIndication, node) ;
    virtual void VHDL_VISIT(VhdlExplicitSubtypeIndication, node) ;
    virtual void VHDL_VISIT(VhdlRange, node) ;
    virtual void VHDL_VISIT(VhdlBox, node) ;
    virtual void VHDL_VISIT(VhdlAssocElement, node) ;
    virtual void VHDL_VISIT(VhdlInertialElement, node) ;
    virtual void VHDL_VISIT(VhdlOperator, node) ;
    virtual void VHDL_VISIT(VhdlAllocator, node) ;
    virtual void VHDL_VISIT(VhdlAggregate, node) ;
    virtual void VHDL_VISIT(VhdlQualifiedExpression, node) ;
    virtual void VHDL_VISIT(VhdlElementAssoc, node) ;
    virtual void VHDL_VISIT(VhdlRecResFunctionElement, node) ;
    virtual void VHDL_VISIT(VhdlWaveformElement, node) ;
    virtual void VHDL_VISIT(VhdlDefault, node) ;

    // The following class definitions can be found in VhdlIdDef.h
    virtual void VHDL_VISIT(VhdlIdDef, node) ;
    virtual void VHDL_VISIT(VhdlLibraryId, node) ;
    virtual void VHDL_VISIT(VhdlGroupId, node) ;
    virtual void VHDL_VISIT(VhdlGroupTemplateId, node) ;
    virtual void VHDL_VISIT(VhdlAttributeId, node) ;
    virtual void VHDL_VISIT(VhdlComponentId, node) ;
    virtual void VHDL_VISIT(VhdlAliasId, node) ;
    virtual void VHDL_VISIT(VhdlFileId, node) ;
    virtual void VHDL_VISIT(VhdlVariableId, node) ;
    virtual void VHDL_VISIT(VhdlSignalId, node) ;
    virtual void VHDL_VISIT(VhdlConstantId, node) ;
    virtual void VHDL_VISIT(VhdlTypeId, node) ;
    virtual void VHDL_VISIT(VhdlGenericTypeId, node) ;
    virtual void VHDL_VISIT(VhdlUniversalInteger, node) ;
    virtual void VHDL_VISIT(VhdlUniversalReal, node) ;
    virtual void VHDL_VISIT(VhdlAnonymousType, node) ;
    virtual void VHDL_VISIT(VhdlSubtypeId, node) ;
    virtual void VHDL_VISIT(VhdlSubprogramId, node) ;
    virtual void VHDL_VISIT(VhdlOperatorId, node) ;
    virtual void VHDL_VISIT(VhdlInterfaceId, node) ;
    virtual void VHDL_VISIT(VhdlEnumerationId, node) ;
    virtual void VHDL_VISIT(VhdlElementId, node) ;
    virtual void VHDL_VISIT(VhdlPhysicalUnitId, node) ;
    virtual void VHDL_VISIT(VhdlEntityId, node) ;
    virtual void VHDL_VISIT(VhdlArchitectureId, node) ;
    virtual void VHDL_VISIT(VhdlConfigurationId, node) ;
    virtual void VHDL_VISIT(VhdlPackageId, node) ;
    virtual void VHDL_VISIT(VhdlPackageBodyId, node) ;
    virtual void VHDL_VISIT(VhdlContextId, node) ;
    virtual void VHDL_VISIT(VhdlLabelId, node) ;
    virtual void VHDL_VISIT(VhdlBlockId, node) ;
    virtual void VHDL_VISIT(VhdlProtectedTypeId, node) ;
    virtual void VHDL_VISIT(VhdlProtectedTypeBodyId, node) ;
    virtual void VHDL_VISIT(VhdlPackageInstElement, node) ;

    // The following class definitions can be found in VhdlMisc.h
    virtual void VHDL_VISIT(VhdlSignature, node) ;
    virtual void VHDL_VISIT(VhdlFileOpenInfo, node) ;
    virtual void VHDL_VISIT(VhdlBindingIndication, node) ;
    virtual void VHDL_VISIT(VhdlIterScheme, node) ;
    virtual void VHDL_VISIT(VhdlWhileScheme, node) ;
    virtual void VHDL_VISIT(VhdlForScheme, node) ;
    virtual void VHDL_VISIT(VhdlIfScheme, node) ;
    virtual void VHDL_VISIT(VhdlElsifElseScheme, node) ;
    virtual void VHDL_VISIT(VhdlCaseItemScheme, node) ;
    virtual void VHDL_VISIT(VhdlDelayMechanism, node) ;
    virtual void VHDL_VISIT(VhdlTransport, node) ;
    virtual void VHDL_VISIT(VhdlInertialDelay, node) ;
    virtual void VHDL_VISIT(VhdlOptions, node) ;
    virtual void VHDL_VISIT(VhdlConditionalWaveform, node) ;
    virtual void VHDL_VISIT(VhdlSelectedWaveform, node) ;
    virtual void VHDL_VISIT(VhdlConditionalExpression, node) ;
    virtual void VHDL_VISIT(VhdlSelectedExpression, node) ;
    virtual void VHDL_VISIT(VhdlBlockGenerics, node) ;
    virtual void VHDL_VISIT(VhdlBlockPorts, node) ;
    virtual void VHDL_VISIT(VhdlCaseStatementAlternative, node) ;
    virtual void VHDL_VISIT(VhdlElsif, node) ;
    virtual void VHDL_VISIT(VhdlEntityClassEntry, node) ;

    // The following class definitions can be found in VhdlName.h
    virtual void VHDL_VISIT(VhdlName, node) ;
    virtual void VHDL_VISIT(VhdlDesignator, node) ;
    virtual void VHDL_VISIT(VhdlArrayResFunction, node) ;
    virtual void VHDL_VISIT(VhdlRecordResFunction, node) ;
    virtual void VHDL_VISIT(VhdlLiteral, node) ;
    virtual void VHDL_VISIT(VhdlAll, node) ;
    virtual void VHDL_VISIT(VhdlOthers, node) ;
    virtual void VHDL_VISIT(VhdlUnaffected, node) ;
    virtual void VHDL_VISIT(VhdlInteger, node) ;
    virtual void VHDL_VISIT(VhdlReal, node) ;
    virtual void VHDL_VISIT(VhdlStringLiteral, node) ;
    virtual void VHDL_VISIT(VhdlBitStringLiteral, node) ;
    virtual void VHDL_VISIT(VhdlCharacterLiteral, node) ;
    virtual void VHDL_VISIT(VhdlPhysicalLiteral, node) ;
    virtual void VHDL_VISIT(VhdlNull, node) ;
    virtual void VHDL_VISIT(VhdlIdRef, node) ;
    virtual void VHDL_VISIT(VhdlSelectedName, node) ;
    virtual void VHDL_VISIT(VhdlIndexedName, node) ;
    virtual void VHDL_VISIT(VhdlSignaturedName, node) ;
    virtual void VHDL_VISIT(VhdlAttributeName, node) ;
    virtual void VHDL_VISIT(VhdlOpen, node) ;
    virtual void VHDL_VISIT(VhdlEntityAspect, node) ;
    virtual void VHDL_VISIT(VhdlInstantiatedUnit, node) ;
    virtual void VHDL_VISIT(VhdlExternalName, node) ;

    // The following class definitions can be found in VhdlSpecification.h
    virtual void VHDL_VISIT(VhdlSpecification, node) ;
    virtual void VHDL_VISIT(VhdlProcedureSpec, node) ;
    virtual void VHDL_VISIT(VhdlFunctionSpec, node) ;
    virtual void VHDL_VISIT(VhdlComponentSpec, node) ;
    virtual void VHDL_VISIT(VhdlGuardedSignalSpec, node) ;
    virtual void VHDL_VISIT(VhdlEntitySpec, node) ;

    // The following class definitions can be found in VhdlStatement.h
    virtual void VHDL_VISIT(VhdlStatement, node) ;
    virtual void VHDL_VISIT(VhdlNullStatement, node) ;
    virtual void VHDL_VISIT(VhdlReturnStatement, node) ;
    virtual void VHDL_VISIT(VhdlExitStatement, node) ;
    virtual void VHDL_VISIT(VhdlNextStatement, node) ;
    virtual void VHDL_VISIT(VhdlLoopStatement, node) ;
    virtual void VHDL_VISIT(VhdlCaseStatement, node) ;
    virtual void VHDL_VISIT(VhdlIfStatement, node) ;
    virtual void VHDL_VISIT(VhdlVariableAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlSignalAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlForceAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlReleaseAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlWaitStatement, node) ;
    virtual void VHDL_VISIT(VhdlReportStatement, node) ;
    virtual void VHDL_VISIT(VhdlAssertionStatement, node) ;
    virtual void VHDL_VISIT(VhdlProcessStatement, node) ;
    virtual void VHDL_VISIT(VhdlBlockStatement, node) ;
    virtual void VHDL_VISIT(VhdlGenerateStatement, node) ;
    virtual void VHDL_VISIT(VhdlProcedureCallStatement, node) ;
    virtual void VHDL_VISIT(VhdlSelectedSignalAssignment, node) ;
    virtual void VHDL_VISIT(VhdlConditionalSignalAssignment, node) ;
    virtual void VHDL_VISIT(VhdlConditionalForceAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlSelectedVariableAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlConditionalVariableAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlComponentInstantiationStatement, node) ;
    virtual void VHDL_VISIT(VhdlIfElsifGenerateStatement, node) ;
    virtual void VHDL_VISIT(VhdlCaseGenerateStatement, node) ;

///@name Prevent the compiler from implementing the following
private:
    VhdlDesignWalker(VhdlDesignWalker &node);
    VhdlDesignWalker& operator=(const VhdlDesignWalker &rhs);
private:
    ///@brief The object represent pre/post general callback mechanism (template method)
    VhdlDesignWalkerCB* mCallBack;
};

/* -------------------------------------------------------------------------- */

} // namespace Verific 

#endif // #ifndef __VERIFIC_DESIGN_WALKER

