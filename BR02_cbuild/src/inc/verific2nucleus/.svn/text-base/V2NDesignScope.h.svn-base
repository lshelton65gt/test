// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __VERIFIC_DESIGN_SCOPE
#define __VERIFIC_DESIGN_SCOPE

// Headers from this project
#include "util/UtPair.h"
#include "util/UtString.h"
#include "util/UtMap.h"
#include "util/UtVector.h"
#include "nucleus/Nucleus.h"
#include "symtab/STBranchNode.h"

//Headers from verific
#include "VhdlName.h"
#include "VhdlDataFlow_Elab.h"

// Headers from standart library
#include <utility>
#include <vector>

// Headers from Verific library

// Forward declaration


namespace verific2nucleus {

    using namespace Verific;

    // Forward declaration
    class VerificNucleusBuilder;



/**
 * @class V2NDesignScope
 *
 * @brief  Helper class for Nucleus DB generation. The class encapsulate information about current and parent scopes.
 *
 *      L1: BEGIN                  +-------+     +-------+
 *          L2: BEGIN              | L2 obj|     | L1 obj|
 *              ...                +-------+     +-------+
 *              END                |  ptr -|---->|  ptr -|---> NULL
 *          END                    +-------+     +-------+
 */
class V2NDesignScope {


    public:
        //add any contextual information here
        // RJC TODO, consider combining SUB_PROG_DECL and FOR_SCHEME to be AUTO vs STATIC scope (see comment for FOR_SCHEME in
        // VerificVerilogDesignWalker.cpp) and perhaps moving this flag out of the context scope and into a separate flag, it seems that
        // this is not orthogonal to the RVALUE and LVALUE enum values.
        typedef enum {UNKNOWN_CONTEXT, /*! unspecified context */
                      SUB_PROG_DECL,   /*! nets created in this context have eNonStaticNet flag set */
                      FOR_SCHEME,      /*! is set when processing a FOR loop, to allow the blocking assign(s) for initial/update for loop variable to
                                        * be placed so gValue will return it (so assignment can be put into higher context */
                      RVALUE,          /*! */
                      LVALUE           /*! */
       } ContextType;
       //possible types for signals, variables etc ...
        typedef enum { UNKNOWN_TYPE, INTEGER } TypeInfo;

        //possible type for case statements
        typedef enum {UNKNOWN_CASE_TYPE, CASE, CASE_Z, CASE_X, CASE_INSIDE, CASE_Z_INSIDE, CASE_X_INSIDE} CaseType;

        /// set, get methods
    public:
        ///Set return net
        void sReturnNet(NUNet* net);
        ///Get return net
        NUNet* gReturnNet() const;
        ///Set loop block 
        void sLoopBlock();
        ///Check marked loop block
        bool isLoopBlock() const;
        ///Check is current V2NDesignScope module scope (used when empty module scope is pushed)
        bool isModuleScope() const;
        ///Set current V2NDesignScope to be module scope (used when empty module scope is pushed)
        void setModuleScope();
        ///Set always block 
        void setAlwaysBlock(NUAlwaysBlock* alwaysBlock);
        ///get always block
        NUAlwaysBlock* getAlwaysBlock() const;
        ///Set module instance 
        void setModuleInstance(NUModuleInstance* moduleInstance);
        ///Get module instance 
        NUModuleInstance* getModuleInstance() const;
        ///TODO replace prefixes 'g' - with 'get', 's' - with 'set'
        ///set type info
        void sTypeInfo(TypeInfo i);
        ///get type info
        TypeInfo gTypeInfo() const;
        /// get owner scope (read-only)
        const NUScope* gOwner() const;
        /// get module 
        NUScope* gOwner() ;
        /// set declaration scope
        void sDeclarationScope(NUNamedDeclarationScope* s);
        /// set elaborated symbol table scope
        void sElabSTScope(STBranchNode* s);
        /// set unelaborated symbol table scope
        void sUnelabSTScope(STBranchNode* s);
        /// get elaborated symbol table scope (read-only)
        const STBranchNode* gElabSTScope() const;
        /// get elaborated symbol table scope
        STBranchNode* gElabSTScope();
        /// get unelaborated symbol table scope (read-only)
        const STBranchNode* gUnelabSTScope() const;
        /// get unelaborated symbol table scope
        STBranchNode* gUnelabSTScope();
        /// get declaration scope (read-only)
        const NUNamedDeclarationScope* gDeclarationScope() const;
        /// get declaration scope
        NUNamedDeclarationScope* gDeclarationScope();
        /// get parent scope (read-only)
        const V2NDesignScope* gParentScope() const;
        /// get scope type
        V2NDesignScope* gParentScope();
        /// set parent scope
        void sParentScope(V2NDesignScope* p);
        ///set owner block
        void sOwnerBlock(NUBlock*& b);
        ///get owner block (read-only)
        NUBlock*const & gOwnerBlock() const;
        ///get owner block
        NUBlock*& gOwnerBlock();

        //The Evaluation scope and the current return value.
        //Current return value:
        //Each parse tree node will resolve to a single value.
        //Rather than maintain a huge global variable data base
        //we simply pass back the value of the current parse tree
        //node through the gValue() to the caller. The parse tree
        //exactly mirrors the grammar so we are assured each unit
        //resolves to one value.

        //Evaluation scope. 
        //The context for the evaluation of a parse tree node can
        //be set by the parent.


        //On exit/entry from any Visit we must set the return value
        //and possibly restore the environment context.

        NUBase* gValue();
        void gValueVector(UtVector<NUBase*>& vec);
        void sValue(NUBase* v);
        void sValueVector(UtVector<NUBase*>& vec);
        /// Evaluation context, Can be set on entry by parent wihtout saving
        void sContext(ContextType ct);
        /// Get current context
        ContextType gContext() const;
        /// set case type 
        void sCaseType(CaseType ct);
        /// get case type 
        CaseType gCaseType() const;
	/// Set range for vector net 
	void sRange(const UtPair<int, int>& p);
	/// Get range for creating vector net
	const UtPair<int, int>& gRange() const;
        /// Set bitness for enum type
        void sBitness(unsigned n);
        /// Get bitness for enum type
        unsigned gBitness() const;
        /// Set type bitness
        void sTypeBitness(UtString s, unsigned n);
        /// Get type bitness
        unsigned gTypeBitness(UtString& s) const;
        ///Get enum value
        UtPair<unsigned, unsigned> gEnumValue(UtString& s) const;
        ///Set enum value
        void sEnumValue(UtString s, unsigned d, unsigned b);
        ///Get task
        NUTask* gTask(UtString& str);
        ///Set task
        void sTask(UtString str, NUTask*& t);
        /// set true as a variable decl
        void sVar() { mIsVar = true; }
        ///check if decl is var decl
        bool isVar() const { return mIsVar; }
        ///set as prefix
        void sPrefix(bool b) { mIsPrefix = b; }
        ///check if prefix
        bool isPrefix() { return mIsPrefix; }
        ///set mem net 
        void sMemNet(bool b) { mIsMemNet = b ; }
        ///check if mem net
        bool isMemNet() { return mIsMemNet; } 
        ///save left side size (using in others)
        void sSize(UInt32 s) { mSize = s; }
       //save left side range dir
        void sDir(UInt32 s) { mDir = s; }
        //get dir
        UInt32 gDir() const { return mDir; }
        ///get size
        UInt32 gSize() const { return mSize; } 
        ///set bit size for aggregate others
        void sAggrElemBit(UInt32 b) { mAggrElemBit = b; }
        ///get bit size for aggregate others
        UInt32 gAggrElemBit() const { return mAggrElemBit; }
        //if aggregate contain choices then return true , otherwise return false
        bool isChoiceExists() const { return mIsChoiceExists; }
        //set true if aggregate contain choices otherwise set false
        void setChoiceExists(bool b) { mIsChoiceExists = b; }
        //save if-statement NUStmtList
        void sIfStmtList(NUStmtList* l) { mStmtList = l; }
        //gets if-statement NUStmtList
        NUStmtList* gIfStmtList() { return mStmtList; }
        bool isSubprogramBlock() const;
        void sSubprogramBody();

        //! get the verific size/sign value to be used for the current context, if 0 then the value has not yet been calculated (usually when entering
        //a self-determined context or at the top of a construct where the size/sign needs to be calculated (such as an assignment).
        int getContextSizeSign() {return mContextSizeSign;}
        //! sets the saved contextSizeSign and returns the previous value.
        int updateContextSizeSign(int val) {int retVal = mContextSizeSign; mContextSizeSign = val; return retVal;}

        // Steve Lim: 2013-11-23: Added register/get functions for the current NUCompositeNet
        // corresponding to the top record object while we are processing a name such as A.B.C.
        // The NUCompositeNet for the object A is stored. If the field B is also a record, its
        // NUCompositeNet will not be stored because it is not the top record. Only the top
        // NUCompositeNet can be used to disambiguate between elements of different record
        // objects of the same type (and therefore elements have same names). For example: 
        //    type R1 is record A : bit; B : bit_vector(7 downto 0); end record;    
        //    type R6 is record A : bit; B : R1; end record;
        //    signal S1, S2 : R6;
        // The nets created for S1's elements A and B are stored in the walker's mObjectRegistry
        // keyed on the VhdlIdDef's from the type and are therefore not unique. The registry is
        // now a multimap and stores all nets keyed by the same VhdlIdDef. A lookup may therefore
        // return multiple entries from which the walker must disambiguate using the top record
        // net as context.
        void rRecordNet(NUCompositeNet* cNet) { mRecordNet = cNet; }
        NUCompositeNet* gRecordNet() { return mRecordNet; }
    // get/set number of assoc-element list
    unsigned gAggregateAssocListSize() const { return mAggregateAssocListSize; } // RJC thinks this variable is dead
    void sAggregateAssocListSize(unsigned s) { mAggregateAssocListSize = s; } // RJC thinks this variable is dead
    // get/set index (either an array index value or a record position) for element-assoc in the aggregate
    signed getAggregateIndex() const { return mAssocElementIndex; }
    void setAggregateIndex(signed s) { mAssocElementIndex = s; }
    // get/set check for aggregate type
    bool isAggregateType() const { return mIsAggregateType; } // RJC thinks this variable is dead
    void sAggregateType(bool b) { mIsAggregateType = b; } // RJC thinks this variable is dead
    // get/set range for choide element in aggregate
    void sAggregateRange(VhdlConstraint* r) { mAggregateRange = r; }
    VhdlConstraint* gAggregateRange() const { return mAggregateRange; }

  ///helper methods
private:
  bool isEnumValue(UtString& s) const;
  UtPair<unsigned, unsigned> gEnumMapValue(UtString& s) const;
  bool isTask(UtString& s) const;
  NUTask*& gTaskMapValue(UtString& s);
	
        /// constructors and destructors
    public:
        /**
         * @brief Only available constructor
         *
         * @param s - current scope
         * @param t - scope type
         * @param p - parent scope
         *
         * @note by default parent scope set to null and scope type set to MODULE (NUModule)
         */
        V2NDesignScope(NUScope* s, V2NDesignScope* p);
        ///destructor
        virtual ~V2NDesignScope();

    private:
        /// not copyable class
        V2NDesignScope(const V2NDesignScope& );
        /// not assignable class
        V2NDesignScope& operator = (const V2NDesignScope& );
        /// private members
    private:
        NUScope* mScope; ///< current scope. Set to the type of the scope owner
        NUNamedDeclarationScope* mDeclarationScope; ///< current named declaration scope
        STBranchNode* mElabSTScope; // current elaborated symbal table scope
        STBranchNode* mUnelabSTScope; // current unelaborated symbal table scope (keeping this for backward compatibility, probably needs to be removed)
        V2NDesignScope* mParentScope; ///< parent scope
        NUBase* mValue;
        ContextType mContext;
        CaseType mCaseType;
        UtPair<int, int> mRange;
        unsigned mBitness;
        UtMap<UtString, unsigned> mTypeBitness;
        UtMap<UtString, UtPair<unsigned, unsigned> > mEnumValue;
        NUBlock* mBlock; ///< owner's block
        UtMap<UtString, NUTask*> mTasks;
        TypeInfo mTypeInfo;
        UtVector<NUBase*> mValueVector;
        bool mIsLoopBlock;
        bool mIsModuleScope;
        NUAlwaysBlock* mAlwaysBlock;
        NUModuleInstance* mModuleInstance;
        NUNet* mReturnNet;
        bool mIsVar;
        bool mIsPrefix;
        bool mIsMemNet;
        UInt32 mSize;
        UInt32 mDir;
        UInt32 mAggrElemBit;
        bool mIsChoiceExists;
        NUStmtList* mStmtList;
        bool mSubprogramBlock;
        int mContextSizeSign;

        // Steve Lim: 2013-11-23: Added mRecordNet to store the net corresponding to the
        // current top record object while processing expressions. More details given in the
        // rRecordNet and gRecordNet functions above.
        NUCompositeNet* mRecordNet;
        //register aggreate assoc list size
        unsigned mAggregateAssocListSize; // RJC thinks: this variable looks dead
        // Index of an assoc element within an aggregate, either array index or record position
        signed mAssocElementIndex;
        //is aggregate cotext
        bool mIsAggregateType;
        VhdlConstraint* mAggregateRange;
///@name Static methods
public:
        ///@brief store verific nucleus builder
        static void sNucleusBuilder(VerificNucleusBuilder* nucleusBuilder);
private:
        /// nucleus builder reference
        static VerificNucleusBuilder* mNucleusBuilder;

}; //class V2NDesignScope

} // namespace verific2nucleus

#endif // __VERIFIC_DESIGN_SCOPE
