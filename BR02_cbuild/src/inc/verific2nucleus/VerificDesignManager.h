// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __VERIFIC_DESIGN_MANAGER
#define __VERIFIC_DESIGN_MANAGER

#include "util/UtString.h"
#include "util/UtVector.h"
#include "util/UtMap.h"
#include "util/UtPair.h"
#include "util/HdlFileCollector.h"
#include "util/CbuildMsgContext.h"
#include "util/UtIOStream.h"

// Verific library includes
#include "Set.h"            // Make class Set available
#include "Map.h"            // Make class Map available
#include "Message.h"        // Make message handlers available
#include "veri_file.h"      // Make verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definition of VeriName
#include "Array.h"          // Make class Array available
#include "VeriId.h"         // Definitions of all verilog identifier nodes
#include "VeriScope.h"      // Definition of VhdLibrary
#include "Strings.h"        // Definition of class to manipulate copy, concatenate, create etc...
#include "vhdl_file.h"      // Make verilog reader available
#include "VhdlIdDef.h"      // Make verilog reader available
#include "VhdlScope.h"      // Definition of VhdLibrary
#include "VhdlStatement.h"  // Make verilog reader available
#include "VhdlName.h"       // Definition of VhdlName
#include "VhdlUnits.h"      // Definition of VhdLibrary
#include "vhdl_sort.h"      // vhdl sorting

class CarbonContext;

namespace verific2nucleus {

class VerificDesignManager {
 public:
  VerificDesignManager();
  ~VerificDesignManager();
  ///@brief Uses verific's native Verilog XL processor, Analyze/Elaborate
  int CompileToParseTree(const UtString& f_listname, bool assumeVerilogFilesAreSV, const LangVer& langVer, 
                         UtMap<UtString,unsigned>& fileName2langver, bool autoCompile, const UtString& fileRoot, 
                         const UtVector<UtPair<UtString,UtString> > &libNameLibPath,UtMap<UtString, UtString> & fileName2LibName, 
                         bool compileOnly, bool hasVerilogLib, bool hasVhdlLib, bool hasLibmap, bool hasVhdlNoWriteLib);
  ///@Registers each file with name/type pair (where type is enum from LangVer)
  void RegisterFile(const UtString& f_name, const LangVer langVer, const UtString& l_name);
  void setVlogTop(const UtString& top_name);
  void setVhdlTop(const UtString& top_name);
  UtString& getVlogTop();
  UtString& getVhdlTop();
  void setVhdlExt(const UtString& str) { mVhdlExt.push_back(str); }
  void setTopLevelParameters(const UtMap<UtString,UtString>& paramValPairs);
  void setPragmaTriggers(const UtVector<UtString>& pragmaTriggers, bool isVerilog);
  void setIgnoreTranslatePragma(unsigned ignore);
  void freeAllocatedMemory(bool compileLibOnly);
  void setMsgContext(MsgContext* msg) { mMsgContext = msg; }
  MsgContext* getMsgContext() { return mMsgContext; }
  void setCarbonContext(CarbonContext* context) { mCarbonContext = context; }
  CarbonContext* getCarbonContext() { return mCarbonContext; }
  ArgProc* getArgs();
  bool setCompileLibOnly();
  unsigned convertToVerificLanguageVersion(LangVer langVer, bool* isVerilogGroup);

  void resetParamPairs();
private:
  int ElaborateHDL(const UtString& lastLib, bool* topFound);
  UtString GetHDLFile();
  bool StrEndWithStr(const char* str1, const char* str2);
  void handleExt(const Verific::Array* f_names, Verific::Array& vhdl_files, Verific::Array& vhdl_files);
  void checkForLoadVhdlStdLibraries();
  void loadVDBLibrarys();
  int elaborateVerilogModule(Verific::Array* all_top_modules, const UtString& lastLib);
  int elaborateVhdlModule(const UtString &lastLib);
  void createLibDesignOrderdFile(const UtString fileName, const UtString vhdlFileName) const;
  void printOrderedFileList(const UtString& fileRoot, const UtVector<UtPair<UtString,UtString> > &libNameLibPath,
                            UtMap<UtString,unsigned> &fileName2langver) const;
  void restoreLib(bool compileLibOnly, const UtVector<UtPair<UtString,UtString> > &libNameLibPath);
  void saveLib(bool compileLibOnly, bool hasLibmap, bool hasVhdlNoWriteLib, const Verific::Array& veri_files, 
               const Verific::Array& veri_files,
               const UtVector<UtPair<UtString,UtString> >& libNameLibPath);

  void copyAllVlogLibraryModulesToWork(const UtString& workLibName);
  void copyAllVhdlLibraryToWork(const UtString& workLibName);
  unsigned DetachVhdlPrimUnit(Verific::VhdlPrimaryUnit *unit,Verific::VhdlLibrary* lib);
  unsigned DetachVhdlSecondaryUnit(Verific::VhdlSecondaryUnit *unit, Verific::VhdlPrimaryUnit* primunit);
  ///@name Modules/Unit getting api's.
 public:
  //Verilog
  Verific::VeriModule* GetTopModule();
  Verific::VeriModule* GetModule(const char* module_name);
  Verific::Map* GetAllModules();
  //Vhdl
  Verific::VhdlPrimaryUnit* GetTopEntity();
  Verific::VhdlPrimaryUnit* GetPrimeryUnit(const char* primery_unit_name);
  Verific::Map* GetAllPrimeryUnits();
 
  void debugPrettyPrintVerilog(const UtString& libName);
  void debugPrettyPrint(const UtString& libName);
  /// debug prettyprint all libraries all entities all architectures
  void debugPrettyPrintVerilog();
  void debugPrettyPrint();
  void PrettyPrintVerilogDesign(const UtString& suffix);
  /// debug print out verilog
  void TraverseVerilog(Verific::VeriModule *module);
  /// debug print out vhdl
  void TraverseVhdl(Verific::VhdlPrimaryUnit *unit, const char *arch_name);
  UtString gArchName();
 private:
  UtVector<UtPair<UtString, unsigned> > mHdlFiles; // .first is the filename,  .second is either a value from: (Verific::veri_file::enum) or (Verific::vhdl_file::enum + mHdlFilesVHDLOffset)
  static const unsigned mHdlFilesVHDLOffset = 100; // defines a boundary between verilog and vhdl file LangVer values stored in mHdlFiles (see declaration for mHdlFiles for the use of this const)
  UtString mVlogTop; // User specified -vlogTop (empty if none). Note: This and mVhdlTop are mutually exclusive. Both cannot be set.
  UtString mVhdlTop; // User specified -vhdlTop (empty if none).
  Verific::veri_file mVerilogReader;
  Verific::vhdl_file mVhdlReader;
  Verific::VeriModule * mTopModule;  // The top level Verilog module in Verific parse tree. Note: This and mTopEntity are mutually exclusive. Both cannot be set.
  Verific::VhdlPrimaryUnit* mTopEntity; // The top level Vhdl entity in Verific parse tree. mTopModule and mTopEntity are set in elaborateVerilogModule and elaborateVhdlModule respectively.
  /// The top level module/entity parameter/generic mapping (const char* : const char*)
  Verific::Map* mParamValPairs;
  MsgContext* mMsgContext;
  CarbonContext* mCarbonContext;
    UtVector<UtString> mVhdlExt;
    UtString mArchName;
};

} // namespace verific2nucleus

#endif
