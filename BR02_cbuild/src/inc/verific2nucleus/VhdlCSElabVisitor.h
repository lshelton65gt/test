// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  This is the base class for the various visitor classes that are
  used in the CarbonStaticElaborator. It encapsulates shared 
  behavior between those classes.

  Specifically, it does the following:

  1) It maintains the VhdlDataFlow object - necessary to evaluate
     constants correctly,
  2) It traverses the design hierarchy (which the Verific VhdlVisitor
     base class does not do). I.E. when a component instantiation statement
     is visited, it finds the instantiated entity, and traverses it 
     (unless it has already been visited),
  3) It does 'live branch' analysis on conditional statements 
     (VhdlIfStatement, VhdlCaseStatement), and avoids visiting
     branches that will not be executed at run time ('pruning').
     These statements require special VhdlDataFlow management,
     also done in this class.
  4) It contains 'helper' methods used to walk subprogram
     bodies.

  WARNING: Changes to this class can have an impact on classes
  derived from it.

  Here are some rules to follow when adding members to this class,
  or when deriving new classes from this:

  1) When adding a new visitor to VhdlCSElabVisitor, look first in
     the base class visitor (to be found in VhdlVisitor.h,.cpp)
     and determine what behavior you want to implement.

     Then look at ALL derived classes, especially those that
     have the same visitor, and understand what impact
     your change will have on them. 

     If the derived class visitor makes a call to the
     base class visitor, perhaps like this:

         VhdlVisitor::VisitCaseStatement(node);

     Then you may want to change it to invoke the new VhdlCSElabVisitor,
     like this:

         VhdlCSElabVisitor::VisitCaseStatement(node);

  2) When creating a new derived class, decide what visitors
     you need to implement, and look at how they are implemented
     in VhdlCSElabVisitor, and in VhdlVisitor.
    
     You may need to copy some of the behavior there into
     the derived class implementation. Or, if you are
     can use the base class implementation directly,
     you can invoke it like this:

         VhdlCSElabVisitor::Visitxxx(node)
                   or
         VhdlVisitor::Visitxxx(node)

     where 'xxx' is the visitor method.

*/

#ifndef __CARBONSTATICELABORATOR_VISITORS_H_
#define __CARBONSTATICELABORATOR_VISITORS_H_

// Project headers
#include "verific2nucleus/CarbonConstraintManager.h"

// verific headers
#include "VeriVisitor.h"    // Visitor base class definition
#include "VhdlVisitor.h"    // Visitor base class definition

// Carbon Utilities
#include "util/UtString.h"
#include "util/UtSet.h"

class CarbonContext;

namespace verific2nucleus {

class CarbonStaticElaborator;

using namespace Verific;

  class VhdlCSElabVisitor : public VhdlVisitor 
  {
  public:
    VhdlCSElabVisitor(CarbonConstraintManager* cm, CarbonStaticElaborator* ce)
      : m_cm(cm)
      , mDataFlow(NULL)
      , mVisitedEntities()
      , mEnableSubprogramBodyWalk(false)
      , mCarbonStaticElaborator(ce)
      , mErrorDuringWalk(false)
      , mEnableElsifCondWalk(false)
      , mEnableElsifStmtWalk(false)
    {}

    // Return true if an error condition was encountered
    bool errorEncountered(void) {return mErrorDuringWalk;};

  public: // Visitor methods


    // These visitors primarily implement hierarchy walking
    virtual void VHDL_VISIT(VhdlEntityDecl, node);
    virtual void VHDL_VISIT(VhdlComponentInstantiationStatement, node);

    // These visitors manage the VhdlDataFlow object
    virtual void VHDL_VISIT(VhdlVariableAssignmentStatement, node);
    virtual void VHDL_VISIT(VhdlIfStatement, node);
    virtual void VHDL_VISIT(VhdlElsif, node);
    virtual void VHDL_VISIT(VhdlCaseStatement, node);
    
    // WARNING: Adding visitors can have an impact on the behavior of derived
    // classes, which may depend on the default VhdlVisitor behavior
    // of these visitors. For example, every statement visitor invokes
    // the VhdlVisitor::VhdlStatement first. This is behavior that VhdlSubprogCallUniquifier
    // depends on. If a statement visitor is added here that does not preserve
    // that funcitonality, then the VhdlSubprogCallUniquifier will fail.
    // See comments at the beginning of this file.

  protected: // Methods, used by derived classes

    // Return true if the indexed name is a 'walkable' subprogram
    bool isTraversableSubprog(VhdlIndexedName* call_inst);
    // Return true if function call evaluates to a constant integer
    bool evaluatesToConstantInt(VhdlIndexedName* call_inst, VhdlDataFlow* df);
    // Used for walking subprograms - push constraints, constants onto visitors
    void walkSubprogramBody(VhdlSubprogramBody* sBody, Array* arguments);
    // Used for walking loop statement after loop rewrite phase
    void walkLoopStatementAfterLoopRewrite(VhdlLoopStatement* node);

    virtual void reportFailure(VhdlTreeNode* node, const UtString& msg) = 0;

    CarbonStaticElaborator* getCarbonStaticElaborator() { return mCarbonStaticElaborator; }
    // Convenience function to retrieve carbon context
    CarbonContext* getCarbonContext();

  protected: // Data used by this class, and by derived classes

    CarbonConstraintManager* m_cm;             // Contains signature->constraint map, and access methods
    VhdlDataFlow* mDataFlow;                   // Used for constant and constraint evaluation by Verific 
    UtSet<VhdlEntityDecl*> mVisitedEntities;   // Set of all visited entities - to avoid multiple visits
    bool mEnableSubprogramBodyWalk;            // Dynamic variable used to control walking into subprogram bodies.
    CarbonStaticElaborator* mCarbonStaticElaborator;
    bool mErrorDuringWalk;                     // will be set to True if error encountered 
    // Variables to control elsif branch walk
    bool mEnableElsifCondWalk;
    bool mEnableElsifStmtWalk;
  }; // End of VhdlCSElabVisitor base class
  
}; // End of Verific2Nucleus namespace

#endif // __CARBONSTATICELABORATOR_VISITORS_H_
