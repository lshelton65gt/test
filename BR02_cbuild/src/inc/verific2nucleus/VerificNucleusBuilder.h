// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef __VERIFIC_NUCLEUS_BUILDER
#define __VERIFIC_NUCLEUS_BUILDER

#include "VhdlIdDef.h"
#include "VeriModule.h"
// Standard includes
//#include <string>
#include <vector>
#include <utility>
#include <map>

// Project includes
#include "util/CarbonTypes.h"
#include "util/SourceLocator.h"
#include "util/UtPair.h"
#include "util/UtMap.h"
#include "util/CbuildMsgContext.h"
#include "util/UtString.h"
#include "shell/carbon_internal_enums.h"
#include "nucleus/Nucleus.h" // for all forward declarations 
#include "nucleus/NUAlwaysBlock.h" // for NUAlwaysBlock::NUAlwaysT
#include "nucleus/NUExpr.h" // for NUOp::OpT
#include "nucleus/NUCase.h" // for NUCase::CaseType
#include "nucleus/NUTF.h" // for NUTF::CallByMode
//#include "nucleus/NULvalue.h"
#include "compiler_driver/MsgCountMgr.h"

// Forward Declarations
class STBranchNode;
class IODBDesignDataSymTabs;
class UserTypeFactory;
class Fold;
class CarbonContext;

#define NUCLEUS_CONSISTENCY(obj, loc, msg) \
do { \
    if (!(obj)) { \
        getMessageContext()->VerificConsistency(&loc, msg); \
        setError(true); \
        return 0; \
    } \
} while(0)

#define NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(obj, loc, msg) \
do { \
    if (!(obj)) { \
        getMessageContext()->VerificConsistency(&loc, msg); \
        setError(true); \
        return; \
    } \
} while(0)

#define NUCLEUS_CONSISTENCY_WITHOUT_RETURN(obj, loc, msg) \
do { \
    if (!(obj)) { \
        getMessageContext()->VerificConsistency(&loc, msg); \
        setError(true); \
    } \
} while(0)


/* Method prefixes - functionality
   a - add
   c - create
   f - find
   g - get
   i - init
   p - print
   r - replace
   s - set
   u - update

   "get vs find"
   "get" -  functions supposed to have constant time complexity
    "find" - functions supposed to have some searching inside and complexity for then constant time (O(logN), O(N)), and there
             should be notion in the comment aobut complexity of that particular function

   */

namespace verific2nucleus {

class VerificTicProtectedNameManager;
typedef UtStringSet CaseItemStrings;

///@class VerificNucleusBuilder
//Implemented as a drawing class pattern
class VerificNucleusBuilder {
public:
    VerificNucleusBuilder(NUDesign * mDesign,
                          AtomicCache * mAtomicCache,
                          SourceLocatorFactory& mSourceLocatorFactory,
                          STSymbolTable * mSymbolTable,
                          NUNetRefFactory * mNetRefFactory,
                          IODBNucleus * mIODB,
                          MsgContext* msgContext,
                          CarbonContext* carbonContext,
                          bool synthSpecified,
                          const char* modulePragmas [],
                          const char* netPragmas[],
                          const char* functionPragmas[],
                          IODBDesignDataSymTabs * symTabs,
                          ArgProc *arg,
                          VerificTicProtectedNameManager* nameManager);
                                
    ~VerificNucleusBuilder();
    AtomicCache* gCache();
    NUNetRefFactory* gNetRefFactory();
    MsgContext* getMessageContext();
    VerificTicProtectedNameManager* gTicProtectedNameMgr();
    MsgCountManager* gMsgCountManager(void) {return &mMsgCountManager;};
    SourceLocatorFactory& gSourceLocFactory(){return mSourceLocatorFactory;}
    bool IsTopModule(const UtString& name);
    // Retrieve top module (entity) in the design. Required for hierarchical reference construction.
    // TODO: This may render IsTopModule above no longer necessary.
    NUModule* getTopModule(void) {return mTopModule;};
    NUDesign* getDesign() { return mDesign;}

    IODBNucleus* gIODB();

    void setFold(Fold* folder) { mFold = folder; }
    Fold* getFold() { return mFold; }


    bool isKnownDirective(const UtString& keyword);
    bool isModuleDirective(const UtString& keyword);
    bool isNetDirective(const UtString& keyword);
    bool isFunctionDirective(const UtString& keyword);
    bool isFromDirectiveGroup(const char** directiveGroup, const UtString& keyword);
    CarbonContext* getCarbonContext() { return mCarbonContext; }
private:
    NUDesign* mDesign;
    AtomicCache* mAtomicCache;
    SourceLocatorFactory& mSourceLocatorFactory;
    STSymbolTable* mSymbolTable;
    NUNetRefFactory* mNetRefFactory; 
    IODBNucleus* mIODB;
    UserTypeFactory* mUserTypeFactory;
    ///@brief flag to keep whether current module is top level or not (only first module created is top-level)
    bool mIsTopLevel;
    ///@brief Pointer to top level module (NULL if no top level module yet established)
    NUModule* mTopModule;
    ///@brief keeps to module name string ("" emtpy string at the initialization)
    UtString mTopModuleName;
    ///@brief keep track of whether the STBranchNode* is a root of hierarchy (only first branch created is root)
    bool mIsRoot;
    ///@brief this flag is true if -synth specified on command line
    bool mSynthFlagWasSpecified;

    ///@name Public api to work with mModuleRegistry
    const char** mModulePragmas;
    const char** mNetPragmas;
    const char** mFunctionPragmas;
    IODBDesignDataSymTabs* mSymTabs;

public:
    //special sets gets method for populating
    //information about hier ref and record nets to symtab
    //generator
    UtVector<Verific::VhdlIdDef*>& gHierRefIdDefs();
    void sHierRefIdDef(Verific::VhdlIdDef* idDef);
    bool isHierRefIdDef(Verific::VhdlIdDef* idDef);
    //@brief Return true if synthesis checks are requested, false otherwise.
    bool getSynthFlag(void) const { return mSynthFlagWasSpecified; }

private:
    ///@name container for herRef nets used in PrePopInfo , sets in VerificVhdlIdDef.cpp
    UtVector<Verific::VhdlIdDef*> mHierRefIdDef;
public:
    ///@brief Get module by name, if module doesn't exist return NULL pointer
    NUBase* gModule(const UtString& name);
    NUBase* gModule(Verific::VeriTreeNode* ve_module);
    ///@brief Register Nucleus module with name
    void registerModule(Verific::VeriTreeNode* ve_module, Verific::VhdlTreeNode* vh_module, const UtString& name, NUModule* module);
private:
    ///@brief Keeping all nucleus created modules (name => NUModule* mapping)
    UtMap<UtString, NUModule*> mModuleRegistry;
    ///@brief Keeping all uniquified module names 
    UtMap<Verific::VeriModule*, UtString> mModuleNameRegistry;
    ///@brief Keeping all created module/NUModule mapping 
    UtMap<Verific::VeriTreeNode*, NUModule*> mVeModuleRegistry;
    UtMap<Verific::VhdlTreeNode*, NUModule*> mVhModuleRegistry;
    ///@brief API for handling multidimensional memories 
public:
    NUExpr* limitSelectIndexExpr(NUMemoryNet* mem, SourceLocator& loc, NUExpr* selExpr);
    void adjustSelExpr(SourceLocator& loc, NUMemoryNet* mem, bool limitIndex, NUExpr** bitSelExpr);
    NUVarselRvalue* genVarselRvalue(NUNet* net, NUExpr* rval, NUExpr* selExpr, const SourceLocator& loc, bool retNullForOutOfRange = true, SInt32 adjust = 0);
    NUVarselLvalue* genVarselLvalue(NUNet* net, NULvalue* lval, NUExpr* selExpr, const SourceLocator& loc, bool retNullForOutOfRange = true, SInt32 adjust = 0);
    NUExpr* normalizeSelect(NUNet* net, NUExpr* selExpr, SInt32 adjust, bool retNullForOutOfRange);
    ///@name error handling get/set
public:
    bool hasError();
    void setError(bool error);
    ArgProc* getArgProc() { return mArg; }
private:
    MsgContext* mMessageContext;
    CarbonContext* mCarbonContext;
    ArgProc* mArg;
    //! Keep track of message counts
    MsgCountManager mMsgCountManager;
    //! Message callback to catch and limit messages.
    MsgCallback* mMsgCB;
  
private:
    Fold* mFold;
private:
    ///@brief For error population keeping state if error already occurred
    bool mError;
private:
    NUEdgeNetMap mEdgeNetMap; 
private:
    VerificTicProtectedNameManager* mTicProtectedNameMgr;

///@name Nucleus DB objects construction API methods to be called from "Verific" verilog/vhdl walkers
public:
    NUExpr* cleanupXZBinaryExpr(NUBinaryOp *in_expr, int lWidth, const SourceLocator& loc);

    ///@name Set/Update/Print API
    void pDesign();
    void uDesign();
    void sUserTypesAtomicCache();
    void setNetFlag(NUNet* port, NetFlags flag);
    void sNetType(NUNet* net, unsigned netType);
    ///@name Creational API
    NUModule* cModule(Verific::VeriTreeNode* ve_module, Verific::VhdlTreeNode* vh_module, const UtString& name, const UtString& origModuleName, bool isVerilog, const SourceLocator& loc, bool timescaleSpecified=false, SInt8 timeUnit=1, SInt8 timePrecision=1);
    ///@brief create 1 bit net
    ///@param scope - declarations scope in which the net should be created
    ///@param netName - name of the net
    ///@param type - verific enumerated type for net
    ///@param isTemp - flag to indicate  whether the created net is temporary (e.g. function output net)
    ///@param isSigned - true if net is signed in this context
    ///@param loc- Source location object
    NUNet* cNet(NUScope* scope, const UtString& netName, unsigned type, bool isTemp, bool isSigned, const SourceLocator& loc);
    ///@brief create vector net
    ///@param scope - declarations scope in which the net should be created
    ///@param netName - name of the net
    ///@param type - verific enumerated type for net
    ///@param dim - range coordinates
    ///@param isTemp - flag to indicate  whether the created net is temporary (e.g. function output net)
    ///@param isSigned - true if net is signed in this context
    ///@param loc- Source location object
    NUNet* cNet(NUScope* scope, const UtString& netName, unsigned type, const UtPair<int,int>& dim, bool isTemp, bool isSigned, const SourceLocator& loc);
    ///@brief create vector net
    ///@param scope - declarations scope in which the net should be created
    ///@param netName - name of the net
    ///@param type - verific enumerated type for net
    ///@param dim1 - 1st dimension of range coordinates
    ///@param dim2 - 2nd dimension of range coordinates
    ///@param isTemp - flag to indicate  whether the created net is temporary (e.g. function output net)
    ///@param isSigned - true if net is signed in this context
    ///@param loc- Source location object
    ///@brief create memory net
    NUNet* cNet(NUScope* scope, const UtString& netName, unsigned type, const UtPair<int,int>& dim1, const UtPair<int,int>& dim2, bool isTemp, bool isSigned, const SourceLocator& loc);
    ///@brief create multidimensional memory net
    ///@param scope - declarations scope in which the net should be created
    ///@param netName - name of the net
    ///@param type - verific enumerated type for net
    ///@param firstUnpackedDim - index of first unpacked dimensions (for verilog 95/2001 type memories - this number is 1)
    ///@param dims - list of dimensions 
    ///@param isTemp - flag to indicate  whether the created net is temporary (e.g. function output net)
    ///@param isSigned - true if net is signed in this context
    ///@param loc- Source location object
    NUNet* cNet(NUScope* scope, const UtString& netName, unsigned type, unsigned firstUnpackedDim, const UtVector<UtPair<int,int> >& dims, bool isTemp, bool isSigned, const SourceLocator& loc);
    ///@brief Create scalar 1 bit port
    NUNet* cPort(NUScope* scope, const UtString& portName, unsigned direction, unsigned portType, bool isSigned, const SourceLocator& loc);
    ///@brief Create vector port with 'dim' dimensions
    NUNet* cPort(NUScope* scope, const UtString& portName, unsigned direction, const UtPair<int,int>& dim, unsigned portType, bool isSigned, const SourceLocator& loc);
    ///@brief Create 2-dim memory port with 'dim1' and 'dim2' dimensions
    NUNet* cPort(NUScope* scope, const UtString& portName, unsigned direction, const UtPair<int,int>& dim1, const UtPair<int,int>& dim2, unsigned portType, bool isSigned, const SourceLocator& loc);
    ///@brief Create multi-dim memory port with 'dims' array of dimensions
    NUNet* cPort(NUScope* scope, const UtString& portName, unsigned direction, unsigned firstUnpackedDim, const UtVector<UtPair<int,int> >& dims, unsigned portType, bool isSigned, const SourceLocator& loc);
    NUExpr* cUnaryOperation(NUExpr* expr1, unsigned opType, const SourceLocator& loc);
    NUExpr* cBinaryOperation(NUExpr* first, unsigned opType, NUExpr* second, bool exprIsSigned, bool isSignedOp, const SourceLocator& loc);
    NUExpr* cTernaryOperation(NUExpr* ifExpr, NUExpr* thenExpr, NUExpr* elseExpr, const SourceLocator& loc);
    NUContAssign* cContinuesAssign(NUScope* scope, NULvalue* lValue, NUExpr* rValue, const SourceLocator& loc);
    NUStmtList* cStmtList();
    NUBlockingAssign* cBlockingAssign(NULvalue* lValue, NUExpr* rValue, const SourceLocator& loc);
    NUNonBlockingAssign* cNonBlockingAssign(NULvalue* lValue, NUExpr* rValue, const SourceLocator& loc);
    NUAlwaysBlock* createAlwaysBlockInScope(NUScope* scope, NUBlock* top_block, bool isWildcard, NUAlwaysBlock::NUAlwaysT always_type, const SourceLocator& loc);
    NUBlock* cBlock(NUScope* scope, const SourceLocator& loc);
    NUExpr* cEventExpr(unsigned edge, NUExpr* edgeNetExpr, const SourceLocator& loc);
    NUExpr* cIdentRvalue(NUNet* net, const SourceLocator& loc);
    NULvalue* cIdentLvalue(NUNet* net, const SourceLocator& loc);
    NUExpr* createIdentRvalue(NUNet* net, const SourceLocator& loc);
    NULvalue* createIdentLvalue(NUNet* net, const SourceLocator& loc);
    NUConst* cConst(const UtString& val, unsigned bitWidth, bool hasXZ, bool isSigned, bool isString, const SourceLocator& loc);
    NUConst* cConst(unsigned val, unsigned bitWidth, bool isSigned, const SourceLocator& loc);
    NUConst* cConst(double val, const SourceLocator& loc);
    NUIf* cIf(NUExpr* cond, const NUStmtList& thenStmts, const NUStmtList& elseStmts, const SourceLocator& loc);
    NUTask* cTF(const UtString& name, const UtString& origName, NUModule* parentModule, const SourceLocator& loc);
    NUTaskEnable* createTaskEnable(NUTask* tf, NUModule* module, const SourceLocator& loc);
    NUTFArgConnection* cTFArgConnection(NUBase* actual, unsigned portIndex, NUTask* tf, const SourceLocator& loc);
    NUMemselRvalue* cMemselRvalue(NUBitNet* net, NUExpr* indexExpr, const SourceLocator& loc);
    NUMemselRvalue* cMemselRvalue(NUMemoryNet* net, NUExpr* indexExpr, const SourceLocator& loc);
    NUMemselRvalue* cMemselRvalue(NUExpr* net, NUExpr* indexExpr, const SourceLocator& loc);
    NUMemselRvalue* cMemselRvalue(NUBitNet* net, NUExprVector* indexExprs, const SourceLocator& loc);
    NUMemselRvalue* cMemselRvalue(NUMemoryNet* net, NUExprVector* indexExprs, const SourceLocator& loc);
    NUMemselRvalue* cMemselRvalue(NUExpr* net, NUExprVector* indexExprs, const SourceLocator& loc);
    NUMemselLvalue* cMemselLvalue(NUBitNet* net, NUExpr* indexExpr, const SourceLocator& loc);
    NUMemselLvalue* cMemselLvalue(NUMemoryNet* net, NUExpr* indexExpr, const SourceLocator& loc);
    NUMemselLvalue* cMemselLvalue(NULvalue* net, NUExpr* indexExpr, const SourceLocator& loc);
    NUMemselLvalue* cMemselLvalue(NUBitNet* net, NUExprVector* indexExprs, const SourceLocator& loc);
    NUMemselLvalue* cMemselLvalue(NUMemoryNet* net, NUExprVector* indexExprs, const SourceLocator& loc);
    NUMemselLvalue* cMemselLvalue(NULvalue* net, NUExprVector* indexExprs, const SourceLocator& loc);
    NUVarselRvalue* cVarselRvalue(NUMemoryNet* net, NUExpr* indexExpr, const SourceLocator& loc);
    NUVarselRvalue* cVarselRvalue(NUVectorNet* net, NUExpr* indexExpr, const SourceLocator& loc);
    NUVarselRvalue* cVarselRvalue(NUBitNet* net, NUExpr* indexExpr, const SourceLocator& loc);
    NUVarselRvalue* cVarselRvalue(NUExpr* expr, NUExpr* indexExpr, const SourceLocator& loc);
    NUVarselRvalue* cVarselRvalue(NUMemoryNet* net, const UtPair<int,int> & dim , const SourceLocator& loc);
    NUVarselRvalue* cVarselRvalue(NUVectorNet* net, const UtPair<int,int> & dim , const SourceLocator& loc);
    NUVarselRvalue* cVarselRvalue(NUBitNet* net, const UtPair<int,int> & dim , const SourceLocator& loc);
    NUVarselRvalue* cVarselRvalue(NUExpr* expr, const UtPair<int,int> & dim , const SourceLocator& loc);
    NUVarselRvalue* cVarselRvalue(NUMemoryNet* net, NUExpr* indexExpr, int adjust, const UtPair<int,int>& dim, const SourceLocator& loc);
    NUVarselRvalue* cVarselRvalue(NUVectorNet* net, NUExpr* indexExpr, int adjust, const UtPair<int,int>& dim, const SourceLocator& loc);
    NUVarselRvalue* cVarselRvalue(NUExpr* expr, NUExpr* indexExpr, const UtPair<int,int>& dim, const SourceLocator& loc);
    NUVarselLvalue* cVarselLvalue(NUMemoryNet* net, NUExpr* indexExpr, const SourceLocator& loc);
    NUVarselLvalue* cVarselLvalue(NUVectorNet* net, NUExpr* indexExpr, const SourceLocator& loc);
    NUVarselLvalue* cVarselLvalue(NUBitNet* net, NUExpr* indexExpr, const SourceLocator& loc);
    NUVarselLvalue* cVarselLvalue(NULvalue* lvalue, NUExpr* indexExpr, const SourceLocator& loc);
    NUVarselLvalue* cVarselLvalue(NUMemoryNet* net, const UtPair<int,int> & dim , const SourceLocator& loc);
    NUVarselLvalue* cVarselLvalue(NUVectorNet* net, const UtPair<int,int> & dim , const SourceLocator& loc);
    NUVarselLvalue* cVarselLvalue(NUBitNet* net, const UtPair<int,int> & dim , const SourceLocator& loc);
    NUVarselLvalue* cVarselLvalue(NULvalue* lvalue, const UtPair<int,int> & dim , const SourceLocator& loc);
    NUVarselLvalue* cVarselLvalue(NUMemoryNet* net, NUExpr* indexExpr, int adjust, const UtPair<int,int>& dim, const SourceLocator& loc);
    NUVarselLvalue* cVarselLvalue(NUVectorNet* net, NUExpr* indexExpr, int adjust, const UtPair<int,int>& dim, const SourceLocator& loc);
    NUVarselLvalue* cVarselLvalue(NULvalue* lvalue, NUExpr* indexExpr, const UtPair<int,int>& dim, const SourceLocator& loc);
    NUModuleInstance* cModuleInstance(const UtString& name, NUScope* parentScope, NUModule* masterModule, const SourceLocator& loc);
    NUPortConnection* cPortConnection(NUBase* actual, unsigned portIndex, NUModule* module, const SourceLocator& loc);
    NUPortConnection* cPortConnection(NUBase* actual, const UtString& name, NUModule* module, const SourceLocator& loc);
    NUPortConnection* cPortConnection(NUBase* actual, NUNet* formal, const SourceLocator& loc);
    // create a NUPortConnection for the specified \a formal port that is not connected to anything outside the module/entity (no actual)
    NUPortConnection* createUnconnectedPortConnection(NUNet *formal, const SourceLocator& loc);
    NULvalue* cConcatLvalue(NULvalueVector& lValues, const SourceLocator& loc);
    NUExpr* cConcatRvalue(NUExprVector& rValues, unsigned repeatCount, const SourceLocator& loc);
    NUCase* cCase(NUExpr* sel, unsigned caseStyle, unsigned maxExprSize, const SourceLocator& loc);
    NUCaseItem* cCaseItem(const SourceLocator& loc);
    NUCaseCondition* cCaseCondition(NUExpr* expr, NUConst* mask, unsigned maxExprSize, const SourceLocator& loc);
    NUConst* cCaseMask(NUExpr* expr, const UtString& condValue, bool isCaseZ, const SourceLocator& loc);
    NUConst* cCasexMask(NUExpr* expr, bool isCaseZ, size_t maxCaseExprSize, const SourceLocator& loc);
    void cCasexMaskRecurse(NUExpr* expr, bool isCaseZ, size_t min_bit, size_t max_bit, DynBitVector* mask);
    NUExpr* cSysFunctionCall(unsigned operation, const UtString& name, const NUExprVector& exprs, NUModule* module, const SourceLocator& loc);
    NUOutputSysTask* cOutputSysTask(const UtString& sysTaskName, NUExprVector& exprs, NUModule* module, bool hasFileSpec,
        bool appendNewLine, bool isVerilogTask, bool isWriteLineTask, bool hasInstanceName, STBranchNode* where, const SourceLocator& loc);
    NUControlSysTask* cControlSysTask(const UtString& sysTaskName, unsigned funcType, NUExprVector& exprs, NUModule* module, const SourceLocator& loc);
    NUReadmemX* cReadmemXSysTask(NUExprVector& exprs, NUModule* module, NULvalue* memNetLvalue, const UtString& filename, bool hexFormat,
        SInt64 startAddr, SInt64 endAddr, bool endSpecified,  const SourceLocator& loc);
    NUFFlushSysTask* cFFlushSysTask(const UtString& sysTaskName, NUExprVector& exprs, NUModule* module, const SourceLocator& loc);
    NUFCloseSysTask* cFCloseSysTask(const UtString& sysTaskName, NUExprVector& exprs, NUModule* module, const SourceLocator& loc);
    NUFOpenSysTask* cFOpenSysTask(StringAtom* name, NUNet* net, NUExprVector& exprs, NUModule* module, const SourceLocator& loc);
    NUSysRandom* cRandomSysTask(StringAtom* name, const UtString& funcName, NULvalue* lvalue, NUExprVector& exprs, NUModule* module, const SourceLocator& loc);
    NUNamedDeclarationScope* cNamedDeclarationScope(const UtString& name, NUScope* parentScope, const SourceLocator& loc);
    NUNet* cFunctionOutputNet(NUScope* scope, const UtString& netName, unsigned type, bool isTemp, bool isSigned, NUTask* f, const SourceLocator& loc);
    NUFor* cFor(const NUStmtList& initial, NUExpr* condition, const NUStmtList& advance, const NUStmtList& body, const SourceLocator& loc);
    NUNet* cTempNet(NUScope* scope, const UtString& netName, const SourceLocator& loc);
    NUBreak* cBreak(NUBlock* block, const UtString& keyword, const UtString& name, const SourceLocator& loc);
    NUChangeDetect* cChangeDetect(NUExpr* expr);
    NUInitialBlock* cInitialBlock(NUScope* scope, NUBlock* top_block, const SourceLocator& loc);
    NetFlags gNetHierRefFlags(NUNet* net, const SourceLocator& loc);
    NUNet* cNetHierRef(NUModule* masterModule, NUScope* parentScope, AtomArray* path, const UtString& netName, NUNet* localNet, const SourceLocator& loc);
    NUTaskEnable* cTaskEnableHierRef(NUScope* parentScope, NUModule* masterModule, NUTask* localTask, AtomArray* path,
        const UtString& taskName, const SourceLocator& loc);
    void cSTBranch(const UtString& name, STBranchNode* parentUnelabScope, STBranchNode* parentElabScope,
        STBranchNode** newUnelabScope, STBranchNode** newElabScope);
    STSymbolTableNode* cSTNode(const UtString& name, STBranchNode* currentUnelabScope, STBranchNode* currentElabScope);
    void aUserTypeInfo(NUNet* net, STSymbolTableNode* symNode, unsigned netType);
    const UserType* maybeCreateVerilogNet(NUNet* net, unsigned netType);
    CarbonVerilogType getVerilogNetType(unsigned netType);
    ///@name Adding to parent scope API
    void aTFToModule(NUTask* tf, NUModule* module);
    void aPortToScope(NUNet* port, NUScope* scope);
    void aNetToScope(NUNet* net, NUScope* scope);
    void aContinuesAssignToModule(NUContAssign* contAssign, NUModule* module);
    void aStmtToScope(NUStmt* stmt, NUScope* scope, const SourceLocator & loc);
    void aStmtToStmtList(NUStmt* stmt, NUStmtList* stmts);
    void aStmtToStmtListFront(NUStmt* stmt, NUStmtList* stmts);
    void addAlwaysBlockToModule(NUAlwaysBlock* alwaysBlock, NUModule* module);
    void aInitialBlockToModule(NUInitialBlock* initialBlock, NUModule* module);
    void aEventExprToAlwaysBlock(NUExpr* eventExpr, NUAlwaysBlock* alwaysBlock);
    void aTopLevelModule(NUModule* module);
    void aModule(NUModule* module);
    void aStmtToTF(NUStmt* stmt, NUTask* tf);
    void aStmtListToBlock(NUStmtList* stmts, NUBlock* block);
    void aTFArgConnectionToTaskEnable(NUTFArgConnection* argConnection, NUTaskEnable* taskEnable);
    void aTFArgConnectionToVector(NUTFArgConnection* argConnection, NUTFArgConnectionVector* argConnections);
    void aTFArgConnectionsToTaskEnable(NUTFArgConnectionVector* argConnections, NUTaskEnable* taskEnable);
    void aStmtToBlock(NUStmt* stmt, NUBlock* block);
    void aStmtToLoop(NUStmt* stmt, NUFor* loop);
    void aModuleInstanceToScope(NUModuleInstance* moduleInstance, NUScope* scope, const SourceLocator& loc);
    void aPortConnectionToModuleInstance(NUPortConnection* portConnection, NUModuleInstance* moduleInstance);
    void aUnconnectedPortsToModuleInstanceAndSort(NUModuleInstance* moduleInstance, NUModule* masterModule, const SourceLocator& loc);
    void aLvalueToLvalueVector(NULvalue* lValue, NULvalueVector& lValues);
    void aExprToExprVector(NUExpr* expr, NUExprVector& exprs);
    void aConditionToCaseItem(NUCaseCondition* caseCondition, NUCaseItem* caseItem);
    void aStmtToCaseItem(NUStmt* stmt, NUCaseItem* caseItem);
    void aStmtListToCaseItem(NUStmtList* stmts, NUCaseItem* caseItem);
    void aCaseItemToCase(NUCaseItem* caseItem, NUCase* caseStmt);
    void aNetToNetVector(NUNet* net, NUNetVector& nets);
    void aNameToPath(const UtString& name, AtomArray* path);
    void aNetHierRefToModule(NUNet* netHierRef, NUModule* module);
///@name Utilities
public:
    ///@brief report unsupported language construct 'msg' and set error state
    void reportUnsupportedLanguageConstruct(const SourceLocator& sourceLocation, const UtString& msg);
    ///@brief Verbose udp output if specified
    void verboseUdp(NUModule* module, const SourceLocator& loc);
    ///@brief adjust rvalue actual to offset (for ranged instances)
    void rvalueActualFixup(NUExpr *rvalue, UInt32 formal_width, const UInt32 *offset, NUExpr **result_rvalue);
    ///@brief adjust lvalue actual to offset (for ranged instances)
    void lvalueActualFixup(NULvalue *lvalue, UInt32 formal_width, const UInt32 *offset, NULvalue **result_lvalue);
    ///@brief check if scope is task/function
    bool isScopeTF(NUScope* scope);
    ///@brief Checks if there is redundant case items
    bool reduceCaseItems(const CaseItemStrings& caseItemStrings);
    ///@brief In case of full case removes redundant items
    void computeFullCase(NUCase** the_stmt, bool hasFullCaseDirective);
    ///@brief Checks if string has some special character inside (!@#$% etc.)
    static bool hasSpecialCharacter(const UtString& data);
    ///@brief Checks if string starts with a numeric/digit
    static bool startsWithNumeric(const UtString& data);
    ///@brief if 'data' contains special character create new data of the form:  "\\" + data + " "
    ///       else return orignal data
    static UtString createNucleusString(const UtString& data);
    static UtString gNameAsNucleusName(const UtString& data);
    static UtString gNameAsNucleusName(const StringAtom* data);

    void addDirective(const UtString& keyword, const UtString& rest, const UtString& moduleName, const UtString& netName, const SourceLocator& loc);
    // @brief Get port direction by index 
    unsigned gPortDirection(NUScope* scope, unsigned portIndex, const SourceLocator& loc);
    // @brief Find port direction by name (Complexity O(N), N -is number of ports the module has)
    unsigned fPortDirection(NUModule* module, const UtString& name);
    unsigned gPortDirection(NUNet* p);
    NUNet* gNetFromIdentRvalue(NUIdentRvalue* identRvalue);
    NUNet* gNetFromIdentLvalue(NUIdentLvalue* identLvalue);
    NUNet* gNetFromExpr(NUExpr* expr);

    // get vector net declaration length
    unsigned gNetLength(NUVectorNet* net);
    // For memory net get nth dimension declaration range Length
    unsigned gNetLength(NUMemoryNet* net, unsigned n);
    unsigned gExprWidth(NUExpr* expr);
    // get declaration range for vector
    UtPair<int,int> gNetDeclRange(NUVectorNet* net);
    // get nth dimension declaration range for memory net
    UtPair<int,int> gNetDeclRange(NUMemoryNet* net, unsigned n);
    NUExpr* gRvalue(NUBase* lvalueOrExpr);
    StringAtom* gNamedDeclScopeName(NUScope* scope);
    //RJC RC#16 the signature of this should be StringAtom* gTaskName(NUTF* task);
    //RJC RC#2013/12/04 (cloutier) This had been marked as addressed but the argument has a type of NUScope, do we not have a NUTF?  Or can you call gTaskName on things other than a task/function?

    StringAtom* gTaskName(NUScope* scope);
    StringAtom* gModuleName(NUScope* module);
    UtString gModuleName(Verific::VeriModule* name);
    void registerModuleName(const UtString& name, Verific::VeriModule* module);
    void createUniquifiedModuleName(Verific::VeriModule* ve_module, const UtString& name);
    StringAtom* gOriginalModuleName(NUScope* module);
    /// Uses scope->gensym(prefix)
    StringAtom* gNextUniqueName(NUScope* scope, const char* prefix);
    NUModule* gModule(NUScope* scope);
    NUScope* gParentDeclUnit(NUScope* scope);
    void dIfEmptyDeclScope(NUNamedDeclarationScope* declarationScope, NUScope* scope, bool isUserNamed);
    bool isMemoryNet(NUNet* net);
    ///@brief if stmt is block - return it, else create block, add stmt to block, return block
    NUBlock* blockify(NUStmt* stmt, NUScope* scope, const SourceLocator& loc);

    // gStmtsFromBlock can be called with second argument as either a block or a single statement.
    // If the seocnd arg is a single statement then that statement is added to the list named stmts.  
    // If the second argument is a block then the statements it contains are added to 'stmts'.
    void gStmtsFromBlock(NUStmtList* stmts, NUStmt* stmt, bool front);
    void gStmtsFromAlwaysBlock(NUStmtList* stmts, NUAlwaysBlock* alwaysBlock);
    NUExpr* cOrConditions(NUNetVector& nets);
    void rStmtList(NUAlwaysBlock* alwaysBlock, NUStmtList& stmts);
    NUExpr* copyExpr(NUExpr* expr);
    NULvalue* copyLval(NULvalue* lval);
    bool checkForPullConflicts();
    void sNetPullUpDown(NUNet* net, unsigned netType);
    NUConst* convertRealConstantToIntConstant( NUConst *the_const, unsigned width, const SourceLocator &loc );
    bool isModule(NUScope* scope);
    bool isTask(NUScope* scope);
    NUNet* fNetHierRef(NUModule* module, AtomArray* path);
    NUTask* fTaskByName(NUModule* module, const UtString& taskName);
    NUNet* getEdgeNet(NUExpr* expr);
    void putEdgeNet(NUExpr* expr, NUNet* net);
private:
    STBranchNode* cSTBranchUnelab(const UtString& name, STBranchNode* parent);
    STBranchNode* cSTBranchElab(const UtString& name, STBranchNode* parent);
public:
    NetFlags gNucleusNetType(unsigned type, bool isTemp, bool is2DNet, unsigned scope_type, bool isSigned, bool isParentTF, bool isPort, const SourceLocator &loc);
    NetFlags createPortFlags(NUScope* scope, StringAtom* name, unsigned dir, bool isSigned, const SourceLocator &loc);
    NUOp::OpT gNucleusBinOp(unsigned binOp, bool isSignedOp);
    NUOp::OpT gNucleusUnOp(unsigned unOp);
    HierFlags gNucleusLangType(bool isVerilog);
    ClockEdge gNucleusClockEdge(unsigned edge);
    NUCase::CaseType gNucleusCaseType(unsigned caseStyle);
    CarbonControlType gCarbonControlType(unsigned funcType);
    void fixSign (NUBinaryOp* expr);
    void fixSign (NUUnaryOp* expr);
    //! Convert IN, OUT or INOUT into appropriate mode for Verilog parameter
    NUTF::CallByMode getParamMode (const NUNet* net);
}; //class VerificNucleusBuilder

} // namespace verific2nucleus

#endif
