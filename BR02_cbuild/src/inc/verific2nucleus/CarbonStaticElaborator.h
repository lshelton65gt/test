// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONSTATICELABORATOR_H_
#define __CARBONSTATICELABORATOR_H_

#include "verific2nucleus/CarbonConstraintManager.h"
#include "util/UtString.h"

namespace Verific {
    class VhdlPrimaryUnit;
    class VeriModule;
    class Array;
    class VhdlIndexedName;
    class VhdlSubprogramId;
    class VhdlExpression;
    class VhdlConstraint;
    class VhdlDataFlow;
    class VhdlTreeNode;
}

class CarbonContext;

namespace verific2nucleus {

class CarbonConstraintManager;

using namespace Verific;

///@brief is an addition to Verific static elaborator to provide elaboration for parts needed by Carbon and unsupported by Verific
class CarbonStaticElaborator {
public:
    CarbonStaticElaborator(VeriModule* module, VhdlPrimaryUnit* entity, CarbonContext* carbonContext); // TODO refactor: we can get module, entity from CarbonContext (through VerificDesignManager member no need to pass it as an argument refactor
    ~CarbonStaticElaborator();
    ///@brief main routine which calls everything else, should be called from CarbonContext
    bool elaborate();
    //@name parse-tree modify series  (normally called by elaborate())

  CarbonContext* getCarbonContext() { return mCarbonContext; }
  void reportUnsupportedLanguageConstruct(VhdlTreeNode* node, const UtString& msg);
  void reportFailure(VhdlTreeNode* node, const char* subPhase, const UtString& msg);

private:
    bool vhdlElaborateConstraints();
    bool vhdlUniquifyFunctionDeclarations();
    bool vhdlRewriteLoop();
    bool vhdlDeadBranchRewrite();
    bool vhdlUniquifyFunctionCalls();
    bool vhdlSubstituteModule();

private:
    VeriModule* mTopModule; 
    VhdlPrimaryUnit* mTopEntity;
    CarbonContext* mCarbonContext;
    CarbonConstraintManager* mConstraintManager;
};

} // namespace verific2nucleus

#endif // __CARBONSTATICELABORATOR_H_
