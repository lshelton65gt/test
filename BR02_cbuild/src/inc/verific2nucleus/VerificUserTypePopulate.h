// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _VERIFIC_USERTYPEPOPULATE_H_
#define _VERIFIC_USERTYPEPOPULATE_H_

#include "nucleus/Nucleus.h"

class IODBNucleus;
class MsgContext;

namespace verific2nucleus {
    
class VerificVhdlDesignWalker;

/**
 * @class UserTypePopulate
 *
 * @brief Added user type information to the symbol table
 *        User type information related to declaration of nets
 *        -type - scalar , array, enum , ...
 *        -range - possible values
 *
 * example:
 *  entity pos_nat is
 *   port ( in1 : positive;
 *          in2 : natural;
 *          out1 : out natural
 *         );
 *  end;
 *
 * architecture pos_nat of pos_nat is
 * begin
 *  process(in1,in2)
 *  begin
 *   out1<= in1 + in2;
 *  end process;
 * end;
 *
 * Symbol table provided by symbol table builder
 * 
 * Num nodes: 5
 * AtomicCache: 0xb7800074
 *
 * Name: work_pos_nat_default_pos_nat (0xb7b893fc)
 * Parent: (nil)
 * BOM: 
 *   Name: in1 (0xb7be3f60)
 *   Parent: 0xb7b893fc
 *   Master: 0xb7be3f60
 *   Store: 0xb7be3f60
 *   BOM:   Aliases:
 *   Name: in2 (0xb7be3fa0)
 *   Parent: 0xb7b893fc
 *   Master: 0xb7be3fa0
 *   Store: 0xb7be3fa0
 *   BOM:   Aliases:
 *   Name: out1 (0xb7be3fe0)
 *   Parent: 0xb7b893fc
 *   Master: 0xb7be3fe0
 *   Store: 0xb7be3fe0
 *   BOM:   Aliases:
 *   Name: named_decl_lb (0xb7b89450)
 *   Parent: 0xb7b893fc
 *   BOM:
 *
 * Final symbol table
 *
 *Num nodes: 7
 * AtomicCache: 0xb7760074
 *
 * Name: work_pos_nat_default_pos_nat (0xb77c3410)
 * Parent: (nil)
 * BOM: 0xb7a0c56c
 *
 *
 *   Name: $extnet (0xb78624a0)
 *   Parent: 0xb77c3410
 *   Master: 0xb78624a0
 *   Store: 0xb78624a0
 *   BOM: 0xb7782688, 
 *
 * STORE: , ffffffff, ffffffff, (null) 
 *   Aliases:
 *   Name: $ostnet (0xb7863c60)
 *   Parent: 0xb77c3410
 *   Master: 0xb7863c60
 *   Store: 0xb7863c60
 *   BOM: 0xb77837c8, 
 *
 * STORE: , ffffffff, ffffffff, (null) 
 *   Aliases:
 *   Name: $cfnet (0xb7863d40)
 *   Parent: 0xb77c3410
 *   Master: 0xb7863d40
 *   Store: 0xb7863d40
 *   BOM: 0xb7783ac8, 
 *
 * STORE: , ffffffff, ffffffff, (null) 
 *   Aliases:
 *   Name: in1 (0xb7863e60)
 *   Parent: 0xb77c3410
 *   Master: 0xb7863e60
 *   Store: 0xb7863e60
 *   BOM: 0xb778e178, 
 * type positive is scalar [requires range when declared]  ranged 1 to 7fffffff
 * STORE: , ffffffff, ffffffff, (null) 
 *   Aliases:
 *   Name: in2 (0xb7863c40)
 *   Parent: 0xb77c3410
 *   Master: 0xb7863c40
 *   Store: 0xb7863c40
 *   BOM: 0xb77826a0, 
 * type natural is scalar [requires range when declared]  ranged 0 to 7fffffff
 * STORE: , ffffffff, ffffffff, (null) 
 *   Aliases:
 *   Name: out1 (0xb7863c80)
 *   Parent: 0xb77c3410
 *   Master: 0xb7863c80
 *   Store: 0xb7863c80
 *   BOM: 0xb7783570, 
 * type natural is scalar [requires range when declared]  ranged 0 to 7fffffff
 * STORE: , ffffffff, ffffffff, (null) 
 *   Aliases:
 */
class UserTypePopulate
{
public:
    UserTypePopulate(VerificVhdlDesignWalker* vhdlDesignWalker,
                     IODBNucleus* iodb,
                     bool verbose ,
                     MsgContext* msgContext);
    
    bool populate(NUDesign* design);
    
private:
    
    VerificVhdlDesignWalker* mVhdlDesignWalker;
    IODBNucleus* mIODB;
    bool mVerbose;
    MsgContext* mMsgContext;
};

}

#endif
