// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __VHDLLOOPREWRITER_H_
#define __VHDLLOOPREWRITER_H_

// Project headers
#include "verific2nucleus/VhdlCSElabVisitor.h"

namespace verific2nucleus {

class CarbonConstraintManager;

using namespace Verific;


struct LoopReplacements
{
    LoopReplacements() 
        : mLoopStatement(0), mParentNode(0), mNewBlkStmts(0), mNewBlkIds(0) {}
    LoopReplacements(VhdlLoopStatement* loopStatement, VhdlTreeNode* parentNode, Array* newBlkStmts, Map* newBlkIds) 
        : mLoopStatement(loopStatement), mParentNode(parentNode), mNewBlkStmts(newBlkStmts), mNewBlkIds(newBlkIds) {}
    void validateMembers(CarbonContext* cc);
    VhdlLoopStatement* mLoopStatement; // is the loop statement pointer in parse-tree which should be replaced
    VhdlTreeNode* mParentNode; // parent node in parse-tree, which contains loop statement
    Array* mNewBlkStmts; // is the list of statements under loop (already unrolled versions), which need to replace loop statement 
    Map* mNewBlkIds; // set of label id's in the loop - to be declared in loop container scope (which is mParentNode->LocalScope())
};

//Main class for replacing loop statement with unrolled version on parse-tree
class VhdlLoopRewriter : public VhdlCSElabVisitor {
///@name Constructors
public:
    VhdlLoopRewriter(CarbonConstraintManager* cm, CarbonStaticElaborator* se);
    ///@name utilties
public:
    bool replaceLoopStatements();

private:
    bool hasLoopStatement(Array* statementList);
    bool alreadyVisited(UtString signature);
    void replaceSingleLoopStatement(VhdlLoopStatement *loop_stmt, VhdlTreeNode* parent_node, Array *new_stmts, Map *new_ids);
    Array* prepThenCallStaticElaborate(Verific::VhdlLoopStatement* node, Map* new_blk_ids, VhdlIdDef* current_scope_id);
    bool primePathNames(VhdlNode* pnode, VhdlScope* current_scope);

    VhdlScope* getDeclarationScopeForLoopScope(Verific::VhdlScope* loop_scope);
    virtual void reportFailure(VhdlTreeNode* node, const UtString& msg);

    ///@name Visitors
public:
    //Annotate parent scope where loop (not generate loop) appears (TODO : add others if any)
    virtual void VHDL_VISIT(VhdlCaseStatementAlternative, node);
    virtual void VHDL_VISIT(VhdlIfStatement, node);
    virtual void VHDL_VISIT(VhdlElsif, node);
    virtual void VHDL_VISIT(VhdlProcessStatement, node);
    virtual void VHDL_VISIT(VhdlBlockStatement, node);
    virtual void VHDL_VISIT(VhdlSubprogramBody, node);
    // Below 2 visitors enable subprogram body walk (in opposite to base visitor approach of visiting subprograms from declaration place)
    virtual void VHDL_VISIT(VhdlIndexedName, node);
    virtual void VHDL_VISIT(VhdlOperator, node);
    // Annotate parent scope and rewrite unrolled loop
    virtual void VHDL_VISIT(VhdlLoopStatement, node);
private:
    VhdlTreeNode* mLoopParentNode;
private:
    // Return true if calls within the subprogram body for this signature
    // have already been visited.
    UtSet<UtString> mVisited;              // Set of signatures corresponding to already visited subprograms.
    // Each node in the vector represents LoopReplacements struct
    UtVector<LoopReplacements*> mLoopReplacements;
}; // class VhdlLoopRewriter

} // namespace verific2nucleus

#endif // __VHDLLOOPREWRITER_H_
