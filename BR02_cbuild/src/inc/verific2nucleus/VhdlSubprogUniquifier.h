// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __VHDLSUBPROGUNIQUIFIER_H_
#define __VHDLSUBPROGUNIQUIFIER_H_

// Project headers
#include "verific2nucleus/VhdlCSElabVisitor.h"
#include "util/UtList.h"

class CarbonStaticElaborator;

namespace verific2nucleus 
{
  using namespace Verific;

  class VhdlSubprogUniquifier : public VhdlCSElabVisitor {
  public:
    VhdlSubprogUniquifier(CarbonConstraintManager *cm, CarbonStaticElaborator* se);

  public:
    // Vhdl Loop Unrolling
    virtual void VHDL_VISIT(VhdlLoopStatement, node);
    // Guard against walking a subprogram body from some point other than a call
    virtual void VHDL_VISIT(VhdlSubprogramBody, node);
    // This visitor processes overloaded operators.
    virtual void VHDL_VISIT(VhdlOperator, node);
    // This visitor processes function and procedure calls. 
    virtual void VHDL_VISIT(VhdlIndexedName, node) ;

    // Declarations are saved until after exitting 'walkSubprogramBody'.
    bool ProcessDeferredDeclarations(void);

  private:
    // Uniquify subprogram body (functions, subprograms, overloaded operators)
    VhdlSubprogramBody* UniquifySubprogBody(VhdlSubprogramBody* sBody, UtString sig);
    // Return true if the subprogram for this signature has already been uniquified
    bool AlreadyUniquified(UtString sig);
    // Return a new uniquified name for a given entity.
    UtString UniquifiedName(const char* subprogram_name);
    // Return true if supplied subprogram declaration is nested within another.
    bool isNestedSubprogDecl(VhdlIdDef *sId);
    // Change subprogram call ids back to original (see comments in .cpp file)
    void ChangeRecursiveCallsBackToOriginal(VhdlSubprogramBody* sBody, VhdlIdDef* new_id, VhdlIdDef* orig_id);
    // First uniquify a subprogram declaration, then visit the uniquified body
    void UniquifyAndVisitSubprogramBody(VhdlSubprogramBody* sBody, Array* args);
    // Fund a subprogram declaration in the package decl area.
    VhdlSubprogramDecl* FindSubprogramDecl(VhdlPackageDecl* pkg_decl, VhdlIdDef* id);
    // Report an error, and set a flag to terminate remaining phases of Carbon Static Elaboration
    virtual void reportFailure(VhdlTreeNode* node, const UtString& msg);

  private:
    UtString mParentSignature;                // Signature of parent subprogram, for use in subprog body
    UtSet<UtString> mUniquified;              // Set of signatures corresponding to already uniquified subprograms.

    typedef struct PendingDecl
    {
      PendingDecl(VhdlIdDef* subprog_id, VhdlIdDef* subtype_id, VhdlScope* scope) 
      {
        SubprogId = subprog_id;
        SubtypeId = subtype_id;
        OwningScope = scope;
      }
      VhdlIdDef* SubprogId;       // Uniquified subprograms cannot be declared in a scope 
      VhdlIdDef* SubtypeId;       // while we're traversing the scope. Save them here until the
      VhdlScope* OwningScope;     // traversal is complete.
    } PendingDecl;
    UtList<PendingDecl> mPendingDecls;
    CarbonStaticElaborator* mCarbonStaticElaborator;
    bool mEnableUniquifySubprogramBody;            // Causes VhdlSubprogBody visitor to uniquify (vs. visit)
    VhdlSubprogramBody* mUniquifiedSubprogramBody; // Uniquified subprog body produced by VhdlSubprogBody visitor
  };
  
}; // namespace verific2nucleus

#endif // __VHDLSUBPROGUNIQUIFIER_H_
