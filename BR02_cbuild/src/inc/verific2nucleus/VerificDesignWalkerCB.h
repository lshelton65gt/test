// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*
 *
 * VerilogDesignWalkerCB/VhdlDesignWalkerCB classes
 * @Brief Classes to be inherited by custom walkers 
 *
*/

#ifndef __VERIFIC_DESIGN_WALKER_CB
#define __VERIFIC_DESIGN_WALKER_CB

// Just for types, we may do forward declaration as well
#include "VeriVisitor.h"    // Verilog Visitor base class definition
#include "VhdlVisitor.h"    // Vhdl Visitor base class definition

// Extending Verific base interface
namespace Verific {

// The macro 'VERI_WALK'/'VHDL_WALK' 
//     T is the type, ie, the class for which it is being defined
//     N is the name of the object of type T inside the visitor routine
//     P Phase (PRE,POST) each method will be called twice (one for each phase)
#define VERI_WALK(T,N,V)       VeriWalkCB##T(Verific::T &N, VisitPhase V)
#define VERI_WALK_CALL(T,N,V)    VeriWalkCB##T(N, V)
#define VERI_VISIT_CALL(T,N)     Visit##T(N)
#define VHDL_WALK(T,N,V)       VhdlWalkCB##T(Verific::T &N, VisitPhase V)
#define VHDL_WALK_CALL(T,N,V)    VhdlWalkCB##T(N, V)
#define VHDL_VISIT_CALL(T,N)     Visit##T(N)

class VerilogDesignWalkerCB {
public:
    typedef enum { VISITPRE, VISITPOST } VisitPhase;
    typedef enum {
        NORMAL,  //!< Normal status, continue walking
        STOP,    //!< Stop the walk
        SKIP,    //!< Skip the current node
        INVALID  //!< Invalid status
    } Status;

    
    ///@brief 
    VerilogDesignWalkerCB() {}
    virtual ~VerilogDesignWalkerCB() {}
    ///@name Walker methods tobe redefined (per need) in derived classses
    /// Please note that each of these functions called twice (PRE phase, POST phase, so phases needs to be handled properly
    /// So almost always we should have someething like a switch in their implementation 
    /// if (phase == VISITPRE) {
    ///     // PRE WORK
    /// } else if (phase == VISITPOST) {
    ///     // POST WORK
    /// }
public:
    // The following class definitions can be found in VeriTreeNode, phase.h
    virtual Status VERI_WALK(VeriTreeNode, node, phase) ;
    virtual Status VERI_WALK(VeriCommentNode, node, phase) ;

    // The following class definitions can be found in VeriModule.h
    virtual Status VERI_WALK(VeriModule, node, phase) ;
    virtual Status VERI_WALK(VeriPrimitive, node, phase) ;
    virtual Status VERI_WALK(VeriConfiguration, node, phase) ;

    // The following class definitions can be found in VeriExpression.h
    virtual Status VERI_WALK(VeriExpression, node, phase) ;
    virtual Status VERI_WALK(VeriName, node, phase) ;
    virtual Status VERI_WALK(VeriIdRef, node, phase) ;
    virtual Status VERI_WALK(VeriIndexedId, node, phase) ;
    virtual Status VERI_WALK(VeriSelectedName, node, phase) ;
    virtual Status VERI_WALK(VeriIndexedMemoryId, node, phase) ;
    virtual Status VERI_WALK(VeriConcat, node, phase) ;
    virtual Status VERI_WALK(VeriMultiConcat, node, phase) ;
    virtual Status VERI_WALK(VeriFunctionCall, node, phase) ;
    virtual Status VERI_WALK(VeriSystemFunctionCall, node, phase) ;
    virtual Status VERI_WALK(VeriMinTypMaxExpr, node, phase) ;
    virtual Status VERI_WALK(VeriUnaryOperator, node, phase) ;
    virtual Status VERI_WALK(VeriBinaryOperator, node, phase) ;
    virtual Status VERI_WALK(VeriQuestionColon, node, phase) ;
    virtual Status VERI_WALK(VeriEventExpression, node, phase) ;
    virtual Status VERI_WALK(VeriPortConnect, node, phase) ;
    virtual Status VERI_WALK(VeriPortOpen, node, phase) ;
    virtual Status VERI_WALK(VeriAnsiPortDecl, node, phase) ;
    virtual Status VERI_WALK(VeriTimingCheckEvent, node, phase) ;
    virtual Status VERI_WALK(VeriDataType, node, phase) ;
    virtual Status VERI_WALK(VeriNetDataType, node, phase) ; // VIPER #4896
    virtual Status VERI_WALK(VeriPathPulseVal, node, phase) ;

    // The following class definitions can be found in VeriId.h
    virtual Status VERI_WALK(VeriIdDef, node, phase) ;
    virtual Status VERI_WALK(VeriVariable, node, phase) ;
    //virtual void VERI_WALK(VeriMemoryId, node, phase) ;
    virtual Status VERI_WALK(VeriInstId, node, phase) ;
    virtual Status VERI_WALK(VeriModuleId, node, phase) ;
    virtual Status VERI_WALK(VeriUdpId, node, phase) ;
    virtual Status VERI_WALK(VeriConfigurationId, node, phase) ;
    virtual Status VERI_WALK(VeriTaskId, node, phase) ;
    virtual Status VERI_WALK(VeriFunctionId, node, phase) ;
    virtual Status VERI_WALK(VeriGenVarId, node, phase) ;
    virtual Status VERI_WALK(VeriParamId, node, phase) ;
    virtual Status VERI_WALK(VeriBlockId, node, phase) ;

    // The following class definitions can be found in VeriMisc.h
    virtual Status VERI_WALK(VeriRange, node, phase) ;
    virtual Status VERI_WALK(VeriStrength, node, phase) ;
    virtual Status VERI_WALK(VeriNetRegAssign, node, phase) ;
    virtual Status VERI_WALK(VeriDefParamAssign, node, phase) ;
    virtual Status VERI_WALK(VeriCaseItem, node, phase) ;
    virtual Status VERI_WALK(VeriGenerateCaseItem, node, phase) ;
    virtual Status VERI_WALK(VeriPath, node, phase) ;
    virtual Status VERI_WALK(VeriDelayOrEventControl, node, phase) ;
    virtual Status VERI_WALK(VeriConfigRule, node, phase) ;
    virtual Status VERI_WALK(VeriInstanceConfig, node, phase) ;
    virtual Status VERI_WALK(VeriCellConfig, node, phase) ;
    virtual Status VERI_WALK(VeriDefaultConfig, node, phase) ;
    virtual Status VERI_WALK(VeriUseClause, node, phase) ;
#ifdef VERILOG_PATHPULSE_PORTS
    virtual Status VERI_WALK(VeriPathPulseValPorts, node, phase) ;
#endif

    // The following class definitions can be found in VeriModuleItem.h
    virtual Status VERI_WALK(VeriModuleItem, node, phase) ;
    virtual Status VERI_WALK(VeriDataDecl, node, phase) ;
    virtual Status VERI_WALK(VeriNetDecl, node, phase) ;
    virtual Status VERI_WALK(VeriFunctionDecl, node, phase) ;
    virtual Status VERI_WALK(VeriTaskDecl, node, phase) ;
    virtual Status VERI_WALK(VeriDefParam, node, phase) ;
    virtual Status VERI_WALK(VeriContinuousAssign, node, phase) ;
    virtual Status VERI_WALK(VeriGateInstantiation, node, phase) ;
    virtual Status VERI_WALK(VeriModuleInstantiation, node, phase) ;
    virtual Status VERI_WALK(VeriSpecifyBlock, node, phase) ;
    virtual Status VERI_WALK(VeriPathDecl, node, phase) ;
    virtual Status VERI_WALK(VeriSystemTimingCheck, node, phase) ;
    virtual Status VERI_WALK(VeriInitialConstruct, node, phase) ;
    virtual Status VERI_WALK(VeriAlwaysConstruct, node, phase) ;
    virtual Status VERI_WALK(VeriGenerateConstruct, node, phase) ;
    virtual Status VERI_WALK(VeriGenerateConditional, node, phase) ;
    virtual Status VERI_WALK(VeriGenerateCase, node, phase) ;
    virtual Status VERI_WALK(VeriGenerateFor, node, phase) ;
    virtual Status VERI_WALK(VeriGenerateBlock, node, phase) ;
    virtual Status VERI_WALK(VeriTable, node, phase) ;
    virtual Status VERI_WALK(VeriPulseControl, node, phase) ;

    // The following class definitions can be found in VeriStatement.h
    virtual Status VERI_WALK(VeriStatement, node, phase) ;
    virtual Status VERI_WALK(VeriBlockingAssign, node, phase) ;
    virtual Status VERI_WALK(VeriNonBlockingAssign, node, phase) ;
    virtual Status VERI_WALK(VeriGenVarAssign, node, phase) ;
    virtual Status VERI_WALK(VeriAssign, node, phase) ;
    virtual Status VERI_WALK(VeriDeAssign, node, phase) ;
    virtual Status VERI_WALK(VeriForce, node, phase) ;
    virtual Status VERI_WALK(VeriRelease, node, phase) ;
    virtual Status VERI_WALK(VeriTaskEnable, node, phase) ;
    virtual Status VERI_WALK(VeriSystemTaskEnable, node, phase) ;
    virtual Status VERI_WALK(VeriDelayControlStatement, node, phase) ;
    virtual Status VERI_WALK(VeriEventControlStatement, node, phase) ;
    virtual Status VERI_WALK(VeriConditionalStatement, node, phase) ;
    virtual Status VERI_WALK(VeriCaseStatement, node, phase) ;
    virtual Status VERI_WALK(VeriLoop, node, phase) ;
    virtual Status VERI_WALK(VeriForever, node, phase) ;
    virtual Status VERI_WALK(VeriRepeat, node, phase) ;
    virtual Status VERI_WALK(VeriWhile, node, phase) ;
    virtual Status VERI_WALK(VeriFor, node, phase) ;
    virtual Status VERI_WALK(VeriWait, node, phase) ;
    virtual Status VERI_WALK(VeriDisable, node, phase) ;
    virtual Status VERI_WALK(VeriEventTrigger, node, phase) ;
    virtual Status VERI_WALK(VeriSeqBlock, node, phase) ;
    virtual Status VERI_WALK(VeriParBlock, node, phase) ;

    // The following class definitions can be found in VeriConstVal.h
    virtual Status VERI_WALK(VeriConst, node, phase) ;
    virtual Status VERI_WALK(VeriConstVal, node, phase) ;
    virtual Status VERI_WALK(VeriIntVal, node, phase) ;
    virtual Status VERI_WALK(VeriRealVal, node, phase) ;

    // The following class definitions can be found in VeriLibrary.h
    virtual Status VERI_WALK(VeriLibraryDecl, node, phase) ;

    // The System Verilog Class
    virtual Status VERI_WALK(VeriInterface, node, phase) ;
    virtual Status VERI_WALK(VeriProgram, node, phase) ;
    virtual Status VERI_WALK(VeriClass, node, phase) ;
    virtual Status VERI_WALK(VeriPropertyDecl, node, phase) ;
    virtual Status VERI_WALK(VeriSequenceDecl, node, phase) ;
    virtual Status VERI_WALK(VeriModport, node, phase) ;
    virtual Status VERI_WALK(VeriModportDecl, node, phase) ;
    virtual Status VERI_WALK(VeriClockingDecl, node, phase) ;
    virtual Status VERI_WALK(VeriConstraintDecl, node, phase) ;
    virtual Status VERI_WALK(VeriBindDirective, node, phase) ;
    virtual Status VERI_WALK(VeriPackage, node, phase) ; // Std SV 1800 addition
    virtual Status VERI_WALK(VeriChecker, node, phase) ; // Std SV 1800 addition
    virtual Status VERI_WALK(VeriSequentialInstantiation, node, phase) ; // Std SV 1800 addition
    virtual Status VERI_WALK(VeriTypeRef, node, phase) ;
    virtual Status VERI_WALK(VeriKeyword, node, phase) ;
    virtual Status VERI_WALK(VeriStructUnion, node, phase) ;
    virtual Status VERI_WALK(VeriEnum, node, phase) ;
    virtual Status VERI_WALK(VeriDotStar, node, phase) ;
    virtual Status VERI_WALK(VeriIfOperator, node, phase) ;
    virtual Status VERI_WALK(VeriSequenceConcat, node, phase) ;
    virtual Status VERI_WALK(VeriClockedSequence, node, phase) ;
    virtual Status VERI_WALK(VeriAssignInSequence, node, phase) ;
    virtual Status VERI_WALK(VeriDistOperator, node, phase) ;
    virtual Status VERI_WALK(VeriSolveBefore, node, phase) ;
    virtual Status VERI_WALK(VeriCast, node, phase) ;
    virtual Status VERI_WALK(VeriNew, node, phase) ;
    virtual Status VERI_WALK(VeriConcatItem, node, phase) ;
    virtual Status VERI_WALK(VeriAssertion, node, phase) ;
    virtual Status VERI_WALK(VeriJumpStatement, node, phase) ;
    virtual Status VERI_WALK(VeriDoWhile, node, phase) ;
    virtual Status VERI_WALK(VeriInterfaceId, node, phase) ;
    virtual Status VERI_WALK(VeriProgramId, node, phase) ;
    virtual Status VERI_WALK(VeriCheckerId, node, phase) ;
    virtual Status VERI_WALK(VeriTypeId, node, phase) ;
    virtual Status VERI_WALK(VeriModportId, node, phase) ;
    virtual Status VERI_WALK(VeriNull, node, phase) ;
    virtual Status VERI_WALK(VeriDollar, node, phase) ;
    virtual Status VERI_WALK(VeriAssignmentPattern, node, phase) ;
    virtual Status VERI_WALK(VeriMultiAssignmentPattern, node, phase) ;
    virtual Status VERI_WALK(VeriTimeLiteral, node, phase) ;
    virtual Status VERI_WALK(VeriOperatorId, node, phase) ;
    virtual Status VERI_WALK(VeriOperatorBinding, node, phase) ;
    virtual Status VERI_WALK(VeriForeachOperator, node, phase) ;
    virtual Status VERI_WALK(VeriStreamingConcat, node, phase) ;
    virtual Status VERI_WALK(VeriNetAlias, node, phase) ;
    virtual Status VERI_WALK(VeriTimeUnit, node, phase) ;
    virtual Status VERI_WALK(VeriForeach, node, phase) ;
    virtual Status VERI_WALK(VeriWaitOrder, node, phase) ;
    virtual Status VERI_WALK(VeriCondPredicate, node, phase) ;
    virtual Status VERI_WALK(VeriDotName, node, phase) ;
    virtual Status VERI_WALK(VeriTaggedUnion, node, phase) ;
    virtual Status VERI_WALK(VeriClockingId, node, phase) ;
    virtual Status VERI_WALK(VeriClockingDirection, node, phase) ;
    virtual Status VERI_WALK(VeriClockingSigDecl, node, phase) ;
    virtual Status VERI_WALK(VeriRandsequence, node, phase) ;
    virtual Status VERI_WALK(VeriCodeBlock, node, phase) ;
    virtual Status VERI_WALK(VeriProduction, node, phase) ;
    virtual Status VERI_WALK(VeriProductionItem, node, phase) ;
    virtual Status VERI_WALK(VeriProductionId, node, phase) ;
    virtual Status VERI_WALK(VeriWith, node, phase) ;
    virtual Status VERI_WALK(VeriWithExpr, node, phase) ;
    virtual Status VERI_WALK(VeriInlineConstraint, node, phase) ;
    virtual Status VERI_WALK(VeriWithStmt, node, phase) ;
    virtual Status VERI_WALK(VeriArrayMethodCall, node, phase) ;
    virtual Status VERI_WALK(VeriInlineConstraintStmt, node, phase) ;
    virtual Status VERI_WALK(VeriCovergroup, node, phase) ;
    virtual Status VERI_WALK(VeriCoverageOption, node, phase) ;
    virtual Status VERI_WALK(VeriCoverageSpec, node, phase) ;
    virtual Status VERI_WALK(VeriBinDecl, node, phase) ;
    virtual Status VERI_WALK(VeriBinValue, node, phase) ;
    virtual Status VERI_WALK(VeriOpenRangeBinValue, node, phase) ;
    virtual Status VERI_WALK(VeriTransBinValue, node, phase) ;
    virtual Status VERI_WALK(VeriDefaultBinValue, node, phase) ;
    virtual Status VERI_WALK(VeriSelectBinValue, node, phase) ;
    virtual Status VERI_WALK(VeriTransSet, node, phase) ;
    virtual Status VERI_WALK(VeriTransRangeList, node, phase) ;
    virtual Status VERI_WALK(VeriSelectCondition, node, phase) ;
    virtual Status VERI_WALK(VeriTypeOperator, node, phase) ;
    virtual Status VERI_WALK(VeriImportDecl, node, phase) ;
    virtual Status VERI_WALK(VeriLetDecl, node, phase) ;
    virtual Status VERI_WALK(VeriDefaultDisableIff, node, phase) ;
    virtual Status VERI_WALK(VeriExportDecl, node, phase) ;
    virtual Status VERI_WALK(VeriScopeName, node, phase) ;
    virtual Status VERI_WALK(VeriNamedPort, node, phase) ;
    virtual Status VERI_WALK(VeriPatternMatch, node, phase) ;
    virtual Status VERI_WALK(VeriConstraintSet, node, phase) ;
    virtual Status VERI_WALK(VeriCovgOptionId, node, phase) ;
    virtual Status VERI_WALK(VeriSeqPropertyFormal, node, phase) ;
    virtual Status VERI_WALK(VeriBinsId, node, phase) ;
    virtual Status VERI_WALK(VeriLetId, node, phase) ;
    virtual Status VERI_WALK(VeriCaseOperator, node, phase) ;
    virtual Status VERI_WALK(VeriCaseOperatorItem, node, phase) ;
    virtual Status VERI_WALK(VeriIndexedExpr, node, phase) ;
    virtual Status VERI_WALK(VeriDPIFunctionDecl, node, phase) ;
    virtual Status VERI_WALK(VeriDPITaskDecl, node, phase) ;
    virtual Status VERI_WALK(VeriExternForkjoinTaskId, node, phase) ;
    virtual Status VERI_WALK(VeriPrototypeId, node, phase) ;

  // returns value indicating if module contents should be processed.
  virtual bool doProcessModuleContents(bool val){(void)val; return true;} // default is to always process contents of declared modules


///@name Prevent the compiler from implementing the following
private:
    VerilogDesignWalkerCB(VerilogDesignWalkerCB &node);
    VerilogDesignWalkerCB& operator=(const VerilogDesignWalkerCB &rhs);
};

class VhdlDesignWalkerCB {
public:
    typedef enum { VISITPRE, VISITPOST } VisitPhase;
    typedef enum {
        NORMAL,  //!< Normal status, continue walking
        STOP,    //!< Stop the walk
        SKIP,    //!< Skip the current node
        INVALID  //!< Invalid status
    } Status;
    ///@brief 
    VhdlDesignWalkerCB() {}
    virtual ~VhdlDesignWalkerCB() {}
public:
////* ================================================================= */
////*                         VISIT VHDL METHODS                        */
////* ================================================================= */

    // The following class definitions can be found in VhdlTreeNode, phase.h
    virtual Status VHDL_WALK(VhdlTreeNode, node, phase) ;
    virtual Status VHDL_WALK(VhdlCommentNode, node, phase) ;

    // The following class definitions can be found in VhdlUnits.h
    virtual Status VHDL_WALK(VhdlDesignUnit, node, phase) ;
    virtual Status VHDL_WALK(VhdlPrimaryUnit, node, phase) ;
    virtual Status VHDL_WALK(VhdlSecondaryUnit, node, phase) ;
    virtual Status VHDL_WALK(VhdlEntityDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlConfigurationDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlPackageDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlContextDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlPackageInstantiationDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlArchitectureBody, node, phase) ;
    virtual Status VHDL_WALK(VhdlPackageBody, node, phase) ;

    // The following class definitions can be found in VhdlConfiguration.h
    virtual Status VHDL_WALK(VhdlConfigurationItem, node, phase) ;
    virtual Status VHDL_WALK(VhdlBlockConfiguration, node, phase) ;
    virtual Status VHDL_WALK(VhdlComponentConfiguration, node, phase) ;

    // The following class definitions can be found in VhdlDeclaration.h
    virtual Status VHDL_WALK(VhdlDeclaration, node, phase) ;
    virtual Status VHDL_WALK(VhdlTypeDef, node, phase) ;
    virtual Status VHDL_WALK(VhdlScalarTypeDef, node, phase) ;
    virtual Status VHDL_WALK(VhdlArrayTypeDef, node, phase) ;
    virtual Status VHDL_WALK(VhdlRecordTypeDef, node, phase) ;
    virtual Status VHDL_WALK(VhdlProtectedTypeDef, node, phase) ;
    virtual Status VHDL_WALK(VhdlProtectedTypeDefBody, node, phase) ;
    virtual Status VHDL_WALK(VhdlAccessTypeDef, node, phase) ;
    virtual Status VHDL_WALK(VhdlFileTypeDef, node, phase) ;
    virtual Status VHDL_WALK(VhdlEnumerationTypeDef, node, phase) ;
    virtual Status VHDL_WALK(VhdlPhysicalTypeDef, node, phase) ;
    virtual Status VHDL_WALK(VhdlElementDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlPhysicalUnitDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlUseClause, node, phase) ;
    virtual Status VHDL_WALK(VhdlContextReference, node, phase) ;
    virtual Status VHDL_WALK(VhdlLibraryClause, node, phase) ;
    virtual Status VHDL_WALK(VhdlInterfaceDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlSubprogramDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlSubprogramBody, node, phase) ;
    virtual Status VHDL_WALK(VhdlSubtypeDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlFullTypeDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlIncompleteTypeDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlConstantDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlSignalDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlVariableDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlFileDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlAliasDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlComponentDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlAttributeDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlAttributeSpec, node, phase) ;
    virtual Status VHDL_WALK(VhdlConfigurationSpec, node, phase) ;
    virtual Status VHDL_WALK(VhdlDisconnectionSpec, node, phase) ;
    virtual Status VHDL_WALK(VhdlGroupTemplateDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlGroupDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlSubprogInstantiationDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlInterfaceTypeDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlInterfaceSubprogDecl, node, phase) ;
    virtual Status VHDL_WALK(VhdlInterfacePackageDecl, node, phase) ;

    // The following class definitions can be found in VhdlExpression.h
    virtual Status VHDL_WALK(VhdlExpression, node, phase) ;
    virtual Status VHDL_WALK(VhdlDiscreteRange, node, phase) ;
    virtual Status VHDL_WALK(VhdlSubtypeIndication, node, phase) ;
    virtual Status VHDL_WALK(VhdlExplicitSubtypeIndication, node, phase) ;
    virtual Status VHDL_WALK(VhdlRange, node, phase) ;
    virtual Status VHDL_WALK(VhdlBox, node, phase) ;
    virtual Status VHDL_WALK(VhdlAssocElement, node, phase) ;
    virtual Status VHDL_WALK(VhdlInertialElement, node, phase) ;
    virtual Status VHDL_WALK(VhdlOperator, node, phase) ;
    virtual Status VHDL_WALK(VhdlAllocator, node, phase) ;
    virtual Status VHDL_WALK(VhdlAggregate, node, phase) ;
    virtual Status VHDL_WALK(VhdlQualifiedExpression, node, phase) ;
    virtual Status VHDL_WALK(VhdlElementAssoc, node, phase) ;
    virtual Status VHDL_WALK(VhdlRecResFunctionElement, node, phase) ;
    virtual Status VHDL_WALK(VhdlWaveformElement, node, phase) ;
    virtual Status VHDL_WALK(VhdlDefault, node, phase) ;

    // The following class definitions can be found in VhdlIdDef.h
    virtual Status VHDL_WALK(VhdlIdDef, node, phase) ;
    virtual Status VHDL_WALK(VhdlLibraryId, node, phase) ;
    virtual Status VHDL_WALK(VhdlGroupId, node, phase) ;
    virtual Status VHDL_WALK(VhdlGroupTemplateId, node, phase) ;
    virtual Status VHDL_WALK(VhdlAttributeId, node, phase) ;
    virtual Status VHDL_WALK(VhdlComponentId, node, phase) ;
    virtual Status VHDL_WALK(VhdlAliasId, node, phase) ;
    virtual Status VHDL_WALK(VhdlFileId, node, phase) ;
    virtual Status VHDL_WALK(VhdlVariableId, node, phase) ;
    virtual Status VHDL_WALK(VhdlSignalId, node, phase) ;
    virtual Status VHDL_WALK(VhdlConstantId, node, phase) ;
    virtual Status VHDL_WALK(VhdlTypeId, node, phase) ;
    virtual Status VHDL_WALK(VhdlGenericTypeId, node, phase) ;
    virtual Status VHDL_WALK(VhdlUniversalInteger, node, phase) ;
    virtual Status VHDL_WALK(VhdlUniversalReal, node, phase) ;
    virtual Status VHDL_WALK(VhdlAnonymousType, node, phase) ;
    virtual Status VHDL_WALK(VhdlSubtypeId, node, phase) ;
    virtual Status VHDL_WALK(VhdlSubprogramId, node, phase) ;
    virtual Status VHDL_WALK(VhdlOperatorId, node, phase) ;
    virtual Status VHDL_WALK(VhdlInterfaceId, node, phase) ;
    virtual Status VHDL_WALK(VhdlEnumerationId, node, phase) ;
    virtual Status VHDL_WALK(VhdlElementId, node, phase) ;
    virtual Status VHDL_WALK(VhdlPhysicalUnitId, node, phase) ;
    virtual Status VHDL_WALK(VhdlEntityId, node, phase) ;
    virtual Status VHDL_WALK(VhdlArchitectureId, node, phase) ;
    virtual Status VHDL_WALK(VhdlConfigurationId, node, phase) ;
    virtual Status VHDL_WALK(VhdlPackageId, node, phase) ;
    virtual Status VHDL_WALK(VhdlPackageBodyId, node, phase) ;
    virtual Status VHDL_WALK(VhdlContextId, node, phase) ;
    virtual Status VHDL_WALK(VhdlLabelId, node, phase) ;
    virtual Status VHDL_WALK(VhdlBlockId, node, phase) ;
    virtual Status VHDL_WALK(VhdlProtectedTypeId, node, phase) ;
    virtual Status VHDL_WALK(VhdlProtectedTypeBodyId, node, phase) ;
    virtual Status VHDL_WALK(VhdlPackageInstElement, node, phase) ;

    // The following class definitions can be found in VhdlMisc.h
    virtual Status VHDL_WALK(VhdlSignature, node, phase) ;
    virtual Status VHDL_WALK(VhdlFileOpenInfo, node, phase) ;
    virtual Status VHDL_WALK(VhdlBindingIndication, node, phase) ;
    virtual Status VHDL_WALK(VhdlIterScheme, node, phase) ;
    virtual Status VHDL_WALK(VhdlWhileScheme, node, phase) ;
    virtual Status VHDL_WALK(VhdlForScheme, node, phase) ;
    virtual Status VHDL_WALK(VhdlIfScheme, node, phase) ;
    virtual Status VHDL_WALK(VhdlElsifElseScheme, node, phase) ;
    virtual Status VHDL_WALK(VhdlCaseItemScheme, node, phase) ;
    virtual Status VHDL_WALK(VhdlDelayMechanism, node, phase) ;
    virtual Status VHDL_WALK(VhdlTransport, node, phase) ;
    virtual Status VHDL_WALK(VhdlInertialDelay, node, phase) ;
    virtual Status VHDL_WALK(VhdlOptions, node, phase) ;
    virtual Status VHDL_WALK(VhdlConditionalWaveform, node, phase) ;
    virtual Status VHDL_WALK(VhdlSelectedWaveform, node, phase) ;
    virtual Status VHDL_WALK(VhdlConditionalExpression, node, phase) ;
    virtual Status VHDL_WALK(VhdlSelectedExpression, node, phase) ;
    virtual Status VHDL_WALK(VhdlBlockGenerics, node, phase) ;
    virtual Status VHDL_WALK(VhdlBlockPorts, node, phase) ;
    virtual Status VHDL_WALK(VhdlCaseStatementAlternative, node, phase) ;
    virtual Status VHDL_WALK(VhdlElsif, node, phase) ;
    virtual Status VHDL_WALK(VhdlEntityClassEntry, node, phase) ;

    // The following class definitions can be found in VhdlName.h
    virtual Status VHDL_WALK(VhdlName, node, phase) ;
    virtual Status VHDL_WALK(VhdlDesignator, node, phase) ;
    virtual Status VHDL_WALK(VhdlArrayResFunction, node, phase) ;
    virtual Status VHDL_WALK(VhdlRecordResFunction, node, phase) ;
    virtual Status VHDL_WALK(VhdlLiteral, node, phase) ;
    virtual Status VHDL_WALK(VhdlAll, node, phase) ;
    virtual Status VHDL_WALK(VhdlOthers, node, phase) ;
    virtual Status VHDL_WALK(VhdlUnaffected, node, phase) ;
    virtual Status VHDL_WALK(VhdlInteger, node, phase) ;
    virtual Status VHDL_WALK(VhdlReal, node, phase) ;
    virtual Status VHDL_WALK(VhdlStringLiteral, node, phase) ;
    virtual Status VHDL_WALK(VhdlBitStringLiteral, node, phase) ;
    virtual Status VHDL_WALK(VhdlCharacterLiteral, node, phase) ;
    virtual Status VHDL_WALK(VhdlPhysicalLiteral, node, phase) ;
    virtual Status VHDL_WALK(VhdlNull, node, phase) ;
    virtual Status VHDL_WALK(VhdlIdRef, node, phase) ;
    virtual Status VHDL_WALK(VhdlSelectedName, node, phase) ;
    virtual Status VHDL_WALK(VhdlIndexedName, node, phase) ;
    virtual Status VHDL_WALK(VhdlSignaturedName, node, phase) ;
    virtual Status VHDL_WALK(VhdlAttributeName, node, phase) ;
    virtual Status VHDL_WALK(VhdlOpen, node, phase) ;
    virtual Status VHDL_WALK(VhdlEntityAspect, node, phase) ;
    virtual Status VHDL_WALK(VhdlInstantiatedUnit, node, phase) ;
    virtual Status VHDL_WALK(VhdlExternalName, node, phase) ;

    // The following class definitions can be found in VhdlSpecification.h
    virtual Status VHDL_WALK(VhdlSpecification, node, phase) ;
    virtual Status VHDL_WALK(VhdlProcedureSpec, node, phase) ;
    virtual Status VHDL_WALK(VhdlFunctionSpec, node, phase) ;
    virtual Status VHDL_WALK(VhdlComponentSpec, node, phase) ;
    virtual Status VHDL_WALK(VhdlGuardedSignalSpec, node, phase) ;
    virtual Status VHDL_WALK(VhdlEntitySpec, node, phase) ;

    // The following class definitions can be found in VhdlStatement.h
    virtual Status VHDL_WALK(VhdlStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlNullStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlReturnStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlExitStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlNextStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlLoopStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlCaseStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlIfStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlVariableAssignmentStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlSignalAssignmentStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlForceAssignmentStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlReleaseAssignmentStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlWaitStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlReportStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlAssertionStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlProcessStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlBlockStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlGenerateStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlProcedureCallStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlSelectedSignalAssignment, node, phase) ;
    virtual Status VHDL_WALK(VhdlConditionalSignalAssignment, node, phase) ;
    virtual Status VHDL_WALK(VhdlConditionalForceAssignmentStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlSelectedVariableAssignmentStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlConditionalVariableAssignmentStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlComponentInstantiationStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlIfElsifGenerateStatement, node, phase) ;
    virtual Status VHDL_WALK(VhdlCaseGenerateStatement, node, phase) ;

///@name Prevent the compiler from implementing the following
private:
    VhdlDesignWalkerCB(VhdlDesignWalkerCB &node);
    VhdlDesignWalkerCB& operator=(const VhdlDesignWalkerCB &rhs);
};

} // namespace Verific

#endif // __VERIFIC_DESIGN_WALKER_CB
