// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*
 *
 * VerificVerilogDesignWalker class
 * @TODO add description
 *
*/

#ifndef __VERIFIC_VERILOG_DESIGN_WALKER
#define __VERIFIC_VERILOG_DESIGN_WALKER

#include "VeriVisitor.h"    // Visitor base class definition
#include "Array.h"          // Make dynamic array class Array available

// Standard headers
#include <fstream>
#include <map>
#include <string>


// Project headers
#include "util/SourceLocator.h"
#include "util/UtPair.h"
#include "util/UtMap.h"
#include "util/UtHashSet.h"
#include "util/UtStack.h"
#include "util/UtString.h"
#include "nucleus/NUStmt.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"

class NUNet;
class NUModule;
class NUExpr;
class NULvalue;


#define GET_BIT(S,B) ((S)?(((S)[(B)/8]>>(B)%8)&1):0)

#define UNSUPPORTED_SV_DECLARATION(node, keyword) \
do { \
    SourceLocator MA_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,(node).Linefile()); \
    UtString MA_msg; \
    MA_msg << "SystemVerilog '" << VeriNode::PrintToken(keyword) << "' declaration"; \
    reportUnsupportedLanguageConstruct(MA_loc, MA_msg.c_str()); \
    return; \
} while(0)

namespace verific2nucleus {

class VerificNucleusBuilder;
class V2NDesignScope;
class VerificDesignManager;
class VerificVhdlDesignWalker;
class VerificTicProtectedNameManager;

using namespace Verific;
/* -------------------------------------------------------------------------- */

typedef UtMap<Verific::VeriTreeNode*,NULvalue*> PortMap;
typedef UtMap<NUScope*, PortMap> ScopePortMap; 




class VerificVerilogDesignWalker : public Verific::VeriVisitor
{
public:
    ///@brief the only available constructor
    VerificVerilogDesignWalker(VerificNucleusBuilder* verificNucleusBuilder, const char* fileRoot, MsgContext* msgContext,
        bool allow_hierrefs, bool inlineTasks, bool doingOutputSysTasks, bool warnForSysTask, int rjcDebugValue);
    virtual ~VerificVerilogDesignWalker();

/* ================================================================= */
/*                         VISIT METHODS                             */
/* ================================================================= */

    // The following class definitions can be found in VeriTreeNode.h
    virtual void VERI_VISIT(VeriTreeNode, node);
    virtual void VERI_VISIT(VeriCommentNode, node);

    // The following class definitions can be found in VeriModule.h
    virtual void VERI_VISIT(VeriModule, node);
    virtual void VERI_VISIT(VeriPrimitive, node);

    // The following class definitions can be found in VeriExpression.h
    virtual void VERI_VISIT(VeriExpression, node);
    virtual void VERI_VISIT(VeriIdRef, node);
    virtual void VERI_VISIT(VeriIndexedId, node);
    virtual void VERI_VISIT(VeriSelectedName, node);
    virtual void VERI_VISIT(VeriIndexedMemoryId, node);
    virtual void VERI_VISIT(VeriConcat, node);
    virtual void VERI_VISIT(VeriMultiConcat, node);
    virtual void VERI_VISIT(VeriFunctionCall, node);
    virtual void VERI_VISIT(VeriSystemFunctionCall, node);
    virtual void VERI_VISIT(VeriMinTypMaxExpr, node);
    virtual void VERI_VISIT(VeriUnaryOperator, node);
    virtual void VERI_VISIT(VeriBinaryOperator, node);
    virtual void VERI_VISIT(VeriQuestionColon, node);
    virtual void VERI_VISIT(VeriEventExpression, node);
    virtual void VERI_VISIT(VeriPortConnect, node);
    virtual void VERI_VISIT(VeriPortOpen, node);
    virtual void VERI_VISIT(VeriAnsiPortDecl, node);
    virtual void VERI_VISIT(VeriTimingCheckEvent, node);
    virtual void VERI_VISIT(VeriDataType, node);
    virtual void VERI_VISIT(VeriNew, node);
    virtual void VERI_VISIT(VeriCast, node);
    virtual void VERI_VISIT(VeriAssignmentPattern, node);
    virtual void VERI_VISIT(VeriMultiAssignmentPattern, node);
    virtual void VERI_VISIT(VeriPatternMatch, node);
    virtual void VERI_VISIT(VeriCondPredicate, node);

    // The following class definitions can be found in VeriId.h
    virtual void VERI_VISIT(VeriIdDef, node);
    virtual void VERI_VISIT(VeriVariable, node);
    virtual void VERI_VISIT(VeriInstId, node);
    virtual void VERI_VISIT(VeriModuleId, node);
    virtual void VERI_VISIT(VeriUdpId, node);
    virtual void VERI_VISIT(VeriTaskId, node);
    virtual void VERI_VISIT(VeriFunctionId, node);
    virtual void VERI_VISIT(VeriGenVarId, node);
    virtual void VERI_VISIT(VeriParamId, node);
    virtual void VERI_VISIT(VeriBlockId, node);

    // The following class definitions can be found in VeriMisc.h
    virtual void VERI_VISIT(VeriRange, node);
    virtual void VERI_VISIT(VeriStrength, node);
    virtual void VERI_VISIT(VeriNetRegAssign, node);
    virtual void VERI_VISIT(VeriCaseItem, node);
    virtual void VERI_VISIT(VeriGenerateCaseItem, node);
    virtual void VERI_VISIT(VeriPath, node);
    virtual void VERI_VISIT(VeriDelayOrEventControl, node);

    // The following class definitions can be found in VeriModuleItem.h
    virtual void VERI_VISIT(VeriModuleItem, node);
    virtual void VERI_VISIT(VeriDataDecl, node);
    virtual void VERI_VISIT(VeriNetDecl, node);
    virtual void VERI_VISIT(VeriFunctionDecl, node);
    virtual void VERI_VISIT(VeriTaskDecl, node);
    virtual void VERI_VISIT(VeriDefParam, node);
    virtual void VERI_VISIT(VeriContinuousAssign, node);
    virtual void VERI_VISIT(VeriGateInstantiation, node);
    virtual void VERI_VISIT(VeriModuleInstantiation, node);
    virtual void VERI_VISIT(VeriSpecifyBlock, node);
    virtual void VERI_VISIT(VeriPathDecl, node);
    virtual void VERI_VISIT(VeriSystemTimingCheck, node);
    virtual void VERI_VISIT(VeriInitialConstruct, node);
    virtual void VERI_VISIT(VeriAlwaysConstruct, node);
    virtual void VERI_VISIT(VeriGenerateConstruct, node);
    virtual void VERI_VISIT(VeriGenerateConditional, node);
    virtual void VERI_VISIT(VeriGenerateCase, node);
    virtual void VERI_VISIT(VeriGenerateFor, node);
    virtual void VERI_VISIT(VeriGenerateBlock, node);
    virtual void VERI_VISIT(VeriTable, node);
    virtual void VERI_VISIT(VeriTimeUnit, node); // SV only
    ////@name Unsupported SV items
    virtual void VERI_VISIT(VeriClass, node);
    virtual void VERI_VISIT(VeriImportDecl, node);
    virtual void VERI_VISIT(VeriPropertyDecl, node);
    virtual void VERI_VISIT(VeriSequenceDecl, node);
    virtual void VERI_VISIT(VeriModport, node);
    virtual void VERI_VISIT(VeriModportDecl, node);
    virtual void VERI_VISIT(VeriClockingDecl, node);
    virtual void VERI_VISIT(VeriConstraintDecl, node);
    virtual void VERI_VISIT(VeriBindDirective, node);
    virtual void VERI_VISIT(VeriOperatorBinding, node);
    virtual void VERI_VISIT(VeriForeachOperator, node);
    virtual void VERI_VISIT(VeriNetAlias, node);
    virtual void VERI_VISIT(VeriCovergroup, node);
    virtual void VERI_VISIT(VeriCoverageSpec, node);
    virtual void VERI_VISIT(VeriLetDecl, node);
    virtual void VERI_VISIT(VeriExportDecl, node);


    // The following class definitions can be found in VeriStatement.h
    virtual void VERI_VISIT(VeriStatement, node);
    virtual void VERI_VISIT(VeriBlockingAssign, node);
    virtual void VERI_VISIT(VeriNonBlockingAssign, node);
    virtual void VERI_VISIT(VeriGenVarAssign, node);
    virtual void VERI_VISIT(VeriAssign, node);
    virtual void VERI_VISIT(VeriDeAssign, node);
    virtual void VERI_VISIT(VeriForce, node);
    virtual void VERI_VISIT(VeriRelease, node);
    virtual void VERI_VISIT(VeriTaskEnable, node);
    virtual void VERI_VISIT(VeriSystemTaskEnable, node);
    virtual void VERI_VISIT(VeriDelayControlStatement, node);
    virtual void VERI_VISIT(VeriEventControlStatement, node);
    virtual void VERI_VISIT(VeriConditionalStatement, node);
    virtual void VERI_VISIT(VeriCaseStatement, node);
    virtual void VERI_VISIT(VeriForever, node);
    virtual void VERI_VISIT(VeriRepeat, node);
    virtual void VERI_VISIT(VeriWhile, node);
    virtual void VERI_VISIT(VeriFor, node);
    virtual void VERI_VISIT(VeriWait, node);
    virtual void VERI_VISIT(VeriDisable, node);
    virtual void VERI_VISIT(VeriEventTrigger, node);
    virtual void VERI_VISIT(VeriSeqBlock, node);
    virtual void VERI_VISIT(VeriParBlock, node);
    virtual void VERI_VISIT(VeriJumpStatement, node);
    virtual void VERI_VISIT(VeriAssertion, node) ;
    virtual void VERI_VISIT(VeriDoWhile, node);
    virtual void VERI_VISIT(VeriRandsequence, node);
    virtual void VERI_VISIT(VeriNullStatement, node);
  
    // The following class definitions can be found in VeriConstVal.h
    virtual void VERI_VISIT(VeriConst, node);
    virtual void VERI_VISIT(VeriConstVal, node);
    virtual void VERI_VISIT(VeriIntVal, node);
    virtual void VERI_VISIT(VeriRealVal, node);

///@name Enumerations
public:
    typedef enum {BIT, VECTOR, MEMORY} DimType;
    class ParamValue;
///@name UDP population helper utilties
private:
    ///@brief enumeration for udp edge types (for internal use)
    enum udpEdgeType { udpPosedge, udpNegedge, udpLevelHigh, udpLevelLow, udpDontCare, udpX };
    ///@brief find net and return rvalue expression for it
    NUExpr* identRvalue(VeriTreeNode* port, const SourceLocator& loc);
    ///@brief find net and return lvalue expression for it
    NULvalue* identLvalue(VeriTreeNode* port, const SourceLocator& loc);
    ///@brief find edgeIndex, if found return true else return false;
    bool udpInputListGetEdgeIndex(const UtString& inputList,unsigned* edgeIndex);
    ///@brief return edge corresponding to edgeIndex in input list
    char udpInputListGetEdge(const UtString& inputList, unsigned edgeIndex);
    ///@brief Get Left and Right side of edge input from given inputList
    /// (ex. inputList = "1(?1)11?" List1 = "1", List2="11?" edge=(?1)
    UtString udpEdgeInputListGetUdpLevelInputList1(const UtString& inputList, unsigned edgeIndex);
    UtString udpEdgeInputListGetUdpLevelInputList2(const UtString& inputList, unsigned edgeIndex);
    ///@brief Top level call to populate a sequential UDP.
    /// Analyze a sequential UDP table, and build the nucleus structures to
    /// represent it.
    void udpSequentialComponents(VeriTable* udpTable, NUModule* module, const SourceLocator& loc);
    ///@brief \return if the level inputs contain an X
    bool udpLevelsContainX (const UtString& inputList, unsigned edgeIndex);
    ///@brief Summarize the edge type for a UDP.
    /// UDP edges have two types: single symbol and two symbol.
    /// Analyze the edge symbols and summarize them into a single enum.
    udpEdgeType udpGetEdgeType(const UtString& inputList, unsigned edgeIndex);
    ///@brief Parse an edge entry of a sequential UDP and update the expressions.
    ///@param entry - The entry to be analyzed
    ///@param inputList - Input list for the entry
    ///@param portList - Portlist Verific array
    ///@param portIndex - current port index, keeping (queued to start at inputs)
    ///@param edgeIndex - edge change port index (there can be one for each entry)
    ///@param module - udp nucleus object
    ///@param loc - udp table source location 
    ///@param clkNode - Saved clock from a previous call to check consistency
    ///@param clock - Return a new clock expr if not created already
    ///@param edge - Saved edge from a previous call to check for consistency
    ///@param enable - Returned expr representing additional level conditions of the entry.
    ///@param isValidEntry - Returned true if no 'x' levels were found.
    void udpEdgeEntry(const UtString& entry, const UtString& inputList, Array* portList, unsigned portIndex,
        unsigned edgeIndex, NUModule* module, const SourceLocator& loc, VeriTreeNode** clkNode, NUExpr** clock,
        udpEdgeType* edge, NUExpr** enable, bool* isValidEntry);
    ///@brief Create an always block to represent the sequential UDP.
    void udpAlways(VeriTreeNode* outputPort, NUModule* module, NUExpr* clock, udpEdgeType edge,
        NUExpr* edgeEnable, NUExpr* edgeData, NUExpr* levelSet, NUExpr* levelClear);
    ///@brief Examine set/clear level expresssions.  Look to see if the expressions
    /// fit the following template:
    ///   set = data & enable
    ///   clear = !data & enable
    /// The template also matches commuted expressions and inversions of
    /// data and enable.
    /// If the template matches then create enable/data statements instead of
    /// set/clear.
    /// Stmt is updated to be:
    ///   if( enable )
    ///     udpOut = data;
    /// If set/clear do not match the template, return a null enable expression,
    /// and do not add any new statement.
    ///@param outputPort - output port verific tree node representation
    ///@param module - udp primitive nucleus representation
    ///@param stmt - Current statement.  Also the returned updated statement.
    ///@param setExpr - Set expression.
    ///@param clearExpr - Clear expression.
    bool udpAddLevelLatchStatement(VeriTreeNode* outputPort, NUModule* module, NUStmt** stmt, NUExpr* setExpr,
        NUExpr* clearExpr);
    ///@brief Determine if the expression is a binary and.  Return the sub-expressions.
    /// Also, the sub-expressions must be idents or inversions of idents.
    bool udpIsBinaryAnd(NUExpr* expr, NUNet** net1, NUExpr** expr1, NUNet** net2, NUExpr** expr2);
    ///@brief For a level set or clear expression, create a continuous assign and
    /// add a conditional statement over the current statement.
    /// Example:
    /// Cont. assign added to module:
    ///   assign lhsName = expr;
    ///
    /// Stmt is updated to be:
    ///   if (lhsName)
    ///     udpOut = value
    ///   else
    ///     stmt;
    ///@param outputPort - output port verific tree node representation
    ///@param module - udp primitive nucleus representation
    ///@param stmt - Current statement.  Also the returned updated statement.
    ///@param expr - Set or clear expression.
    ///@param value - Value to be assigned (1=set, 0=clear)
    ///@param lhsName - Name to use if the expression is not already an ident.
    NUExpr* udpAddLevelStatement(VeriTreeNode* outputPort, NUModule* module,
        NUStmt** stmt, NUExpr* expr, int value, const char* lhsName);
    ///@brief Return true if a level list has any 'x' entries.
    bool udpLevelListHasX(const UtString& levelList);
    ///@brief Get udp output symbol value from given level entry (ex. levelEntry="001:1"; return "1")
    UtString tableEntryGetOutputSymbol(const UtString& entry);
    ///@brief Get udp input list (keep in string) from given entry (ex. levelEntry="001:1"; return "001")
    UtString tableEntryGetInputList(const UtString& entry);
    UtString seqEntryGetCurrentState(const UtString& entry);
    ///@brief Get next character from given levelList (string) and increment index, if index out of bounds return empty string (ex. levelList="001"; index = 2; return "1")
    UtString levelListNextItem(const UtString& levelList, unsigned* index);
    /*! 
      Build a continuous assign to represent a combinational UDP table.
      Only entries with output=1 and no x values are counted towards the
      sum of products representation.

      \verbatim
    Example:
    table
        // a b c : out
        0 0 1 : 1
        x b ? : 0
        1 1 0 : x
        1 1 1 : 1
    endtable
    out = !a&!b&c | a&b&c;
    \endverbatim
     */
    ///@param udpTable - UDP table representation object in verific
    ///@param module - udp primitive nucleus object (represented as NUModule)
    ///@param loc - source location object
    ///@param the_assign - result continues assign nucleus object
    void udpContAssign(VeriTable* udpTable,  NUModule* module, const SourceLocator& loc,  NUContAssign** the_assign);
    ///@brief Return the product of level inputs in a UDP table.
    ///@param levelList - List of table entry levels (inputs)
    ///@param portList -  List of inputs queued to be in sync with the levelList
    ///@param portIndex - current port index
    ///@param loc - source location object
    ///@return product - computed/constructed result nucleus expression
    // TODO change portIndex to pointer
    void udpLevelEntryProduct(const UtString& levelList, Array* portList, unsigned* portIndex,
        const SourceLocator& loc, NUExpr** product);
    ///@brief Add a term to the current product.
    ///@param product - Product of literals from previous calls
    ///@param ve_port - port for the net to be used as a literal
    ///@param invert - whether to invert this literal before anding it in.
    ///@param loc - source location object
    ///@return product - Updated product nucleus expression
    void udpAddTermToProduct(NUExpr** product, VeriTreeNode* ve_port, bool invert, const SourceLocator& loc);

    void udpSequentialComponents(VeriTable* udpTable);
///@name Helper utilities
private:
    ConstantRange* pruneRangeToOverlappingRange(ConstantRange* declaredRange, ConstantRange* parsedRange, const UtString& netName, const SourceLocator* const loc);
    bool checkParsedRangeVsDeclaredRange(ConstantRange* declaredRange, ConstantRange* parsedRange, const UtString& netName, const SourceLocator* const loc);
    void createUniquifiedModuleName(VeriModule* ve_module);
    UtString gVisibleName(VeriTreeNode* ve_node, const UtString& origName);
    VerificTicProtectedNameManager* gTicProtectedNameMgr();
    MsgContext* getMessageContext();
    bool IsUnsupportedTypeRef(VeriDataType* dataType);
    void addCaseCondition(VeriCaseItem* ve_item, unsigned c, unsigned maxExprSize, NUExpr* condExpr, UtString* condValue, NUCaseItem* caseItem, const SourceLocator& sourceLocation);
    void genCaseConditions(VeriCaseItem* ve_item, unsigned c, unsigned maxExprSize, const UtPair<int,int>& dim, NUCaseItem* caseItem, const SourceLocator& sourceLocation);
    void reportUnsupportedLanguageConstruct(const SourceLocator& sourceLocation, const UtString& msg);
    bool allModulesHaveTimeScaleSpecified();
    bool allModulesDoNotHaveTimeScaleSpecified();
    bool isInverseNonConstRangeSelect(VeriRange* index, const UtPair<int,int>& declRange);
    NUBase* genMemoryPartSelect(VeriExpression* node, VeriExpression* index, NUExpr** indexExpr, const UtPair<int,int>& normalizedIndices, NUBase* selObject, unsigned contextType, bool inverse, bool isNonConstRangeSel, const SourceLocator& sourceLocation);
    void parseDimensions(VeriDataType* dataType, VeriIdDef* id, const SourceLocator& sourceLocation, DimType* dimType,
        unsigned* assignBitWidth, UtVector<UtPair<int,int> >* dims, unsigned* firstUnpackedDim);
    VeriRange* findNthRange(VeriIdDef* id, VeriDataType* dataType, unsigned n);
    void accumulatePackedUnpackedDimensions(VeriIdDef* id, VeriDataType* dataType, unsigned numberOfUnpackedDims,
        const SourceLocator& sourceLocation, unsigned * assignBitWidth, UtVector<UtPair<int,int> >* packedDims,  UtVector<UtPair<int,int> >* unpackedDims);
    bool useSignedBinaryOperator(unsigned opType, unsigned lhs_sign, unsigned rhs_sign);
    // TODO consider moving createPreIncDec, createPostIncDec to VerificNucleusBuilder
    NUExpr* createPreIncDec(unsigned operType, VeriExpression* iter, bool isSignedResult, bool isSignedOp,
        const SourceLocator& loc);
    NUExpr* createPostIncDec(unsigned operType, VeriExpression* iter, bool isSignedResult, bool isSignedOp,
        const SourceLocator& loc);
    ///@brief Handle SV specific assigments (+=, -=, etc.)
    ///@param node - blocking assign Verific parse-tree node
    ///@param context_size_sign - context size sign (same as for non-SV case)
    ///@param lhs_sign - lvalue expression sign
    ///@param rhs_sign - rvalue expression sign
    ///@param sourceLocation - location object for blocking assign
    ///@param left - output lvalue nucleus object left side of the assignment
    ///@param right - output expr nucleus object right side of the assignment
    void HandleSVBlockingAssign(VeriBlockingAssign* node, int context_size_sign, unsigned lhs_sign,
        unsigned rhs_sign, const SourceLocator& sourceLocation, NULvalue** left, NUExpr** right);
    // helper function to handle nested operation of compound operator, also special care for ++i/--i operators
    void HandleSVCompoundAssignOperator(unsigned oper_type, VeriExpression* ve_left, VeriExpression* ve_right, 
        int context_size_sign, unsigned lhs_sign, unsigned rhs_sign, const SourceLocator& sourceLocation,
        NULvalue** left, NUExpr** right);
    void HandleSVBinaryOperator(VeriBinaryOperator* node, int context_size_sign, unsigned lhs_sign, unsigned rhs_sign,
        const SourceLocator& loc, NUExpr** result);
    bool IsOpenPort(VeriTreeNode* node);

    ///@@brief arrayNextItem.  Get the item from \a array at position \a pos.
    /// If \a pos is in bounds then the item is returned in \a item and \a pos is incremented.
    /// If \a pos is out of bounds of \a array then set \a item to NULL and do not increment \a pos.
    template<class T> void arrayNextItem(Array* array, unsigned* pos, T **item)
    {
      if (array && array->Item( *pos, item ) ) {
        (*pos)++;
      } else {
        *item = NULL;
      }
    }

    bool getPCIndex(VeriExpression* ve_portconn, Array* ve_allPortConnections, unsigned * index);
    void createPortConnection(const UtVector<VeriExpression*>& ve_portconns, Array* ve_allPortConnections, NUNet* formal, NUModule* instantiated, NUModuleInstance *module_instance, VeriModule* ve_module, const UInt32 *offset, const SourceLocator& loc, NUPortConnection **the_conn);
    // old implementation leaved for mixed language backword compatability. TODO needs to be cleaned up and reduced to the VHDL only 
    void createOneModuleInstanceVhdl(VeriModuleInstantiation& node, VeriInstId* inst, NUScope* parent, NUModule* masterModule, const UInt32 * /*offset*/, const SourceLocator& loc);
    void createOneModuleInstance(VeriInstId* ve_inst, VeriModule* ve_module, NUScope* parent, NUModule* the_module, const UInt32 *offset, const SourceLocator& loc);
    void gatherPortconnsForSinglePort(NUModule* the_module,  VeriModule* ve_module, VeriExpression **ve_portconn, unsigned *ve_portconn_iter,
        Array* ve_allPortConnections, UtVector<VeriExpression*> *portconns_for_this_port, NUNet** port, const SourceLocator& loc);
    VeriTreeNode* portConnGetFormalExpr(VeriExpression* ve_portconn, Array* ve_allPortConnections, VeriModule* ve_module);
    void lookupPort(NUModule* module, VeriTreeNode* node, NUNet **the_net, bool *partial_port, ConstantRange* range, const SourceLocator& loc);
    void mapPort(NUScope* module, VeriTreeNode* node, NULvalue* lvalue, const SourceLocator& sourceLocation);
    NUScope* fParentNamedBlock(V2NDesignScope* cScope, const UtString& name);
    bool isNamedBlock(VeriStatement* stmt);
    void gRange(UtPair<int, int>* dim, const VeriRange& range, unsigned width, bool normalize, const UtString& netName, const UtPair<int,int>* const declRange, int* adjust=0, const SourceLocator* const = 0);
    VeriDataType* gMaxRangeDataType(VeriDataType* first, VeriDataType* second);
    VeriRange* gMaxRange(VeriRange* first, VeriRange* second);
    bool isDoubleOperator(unsigned oper);
    bool isIncDecOperator(unsigned oper);
    bool isCompoundAssignOperator(unsigned oper);
    bool isResistiveDevice(unsigned type);
    UtPair<unsigned,unsigned> getDoubleOperator(unsigned oper);
    void getGateFlags(unsigned oper, bool* isMultiOutputGate, bool* isMultiInputGate, bool* isTriStateGate);
    void addModuleItem(unsigned id, const SourceLocator & loc);
    // Converts an operator expressed as a string into a VERI_* token value.
    unsigned getVerificOperatorForUnaryDoubleOperator(const UtString& oper);
    unsigned getVerificOperatorForBinaryDoubleOperator(const UtString& oper);
    unsigned getVerificOperatorForAssignSubOperator(unsigned oper);
    bool isMixedEvent(VeriStatement* stmt);
    bool isCombAlways(VeriStatement* stmt);
    // by default some of the output sys tasks are disabled in Carbon (and can be enabled by -enableOutputSysTasks option) this function checks whether or not the system task is from that list  (as we haven't implemented yet all the sysTasks support in Verific the included list might be incomplete).
    bool isDisabledSysTask(unsigned funcType);
    bool isControlSysTask(unsigned funcType);
    bool isOutputSysTask(unsigned funcType);
    bool isFFlushOrFCloseSysTask(unsigned functType);
    bool isReadmemX(unsigned funcType);
    bool isRandomSysTask(unsigned funcType);

    ///@brief Returns true if _funcType_ is a system task supported by carbon
    bool isSupportedSysTask(unsigned funcType);
    ///@brief Returns true if _funcType_ is a system function that is supported by carbon
    bool isSupportedSysFunction(unsigned funcType);
    bool isNegativeNumber(unsigned oper, VeriExpression* expr);
    VeriExpression* evalConstExpr(int contextSizeSign, VeriExpression* rightExpr);
    bool evalIntValue(VeriExpression* rightExpr, int* intValue);
    unsigned gMaxCaseExprSize(const VeriCaseStatement& caseStatement);
    unsigned gMaxCaseItemExprSize(const VeriCaseItem& caseItem);
    ///@brief Function add carbon custom directives to IODB
    ///@param netName - the net name pragma to attach (can be empty)
    ///@param moduleName - the module name pragma to attach
    ///@param pragmas - list of pragmas to be attached to the current net 
    ///@param lineinfos - map of all pragmas to line number for current net 
    ///@param source_locator - source location nucleus object 
    void addPragmas(UtString* netName, UtString* moduleName, Map* pragmas, Map* lineinfos, SourceLocator* loc);
    ///@brief Compose Prefix name, using scope information
    ///@param moduleName - indicates endpoint for prefix name
    ///@return hierarchical name (module1.func1.block1)
    UtString composePrefixName(const UtString& moduleName);
    ///@brief Function finds and returns used expression in port connection actual 
    VeriExpression* fActualRefExpr(VeriExpression* vpc);
    ///@brief Function finds and returns used iddef in port connection actual 
    VeriIdDef* fActualRefId(VeriExpression* vpc);
    ///@brief Get Formal name from given port connection expression (can be iddef as well)
    /// if found formal name return true and formal name via 'name' argument
    /// if unable to find formal name return false ('name' argument value remains unchanged in this case) 
    bool gFormalName(VeriTreeNode* vpc, UtString* name);
    bool portIsRedeclared(VeriExpression* vpc, NUNet* port, UtMap<NUBase*, bool> alreadyAddedPorts);
    NUBase* findNet(VeriIdDef* netId);
    /// @brief Returns active NUScope* for current V2NDesignScope as well it's type:
    /// @detailed gNUScopeAndType function is nothing more than just same gNUScope() function with addition of correct scope type information in pair construct. Whenever current scope type information along with current scope  needs to be used - than this function should be called. gNUScope remains for cases when only current scope needed (not it's type).
    /// @note  Verific tree node info we use only for reporting inconsistency (V2N_INFO_ASSERT). In some places it's hard to get access to Verific parse tree node corresponding to NUScope (e.g. in place where we need recursive call to detect the parent declaration scope). So in those places we just pass NULL as node.
    /// @brief Returns active NUScope* for current V2NDesignScope 
    NUScope* getNUScope(VeriTreeNode* node);
    /// @brief Returns owner NUScope* of object for given V2NDesignScope
    NUScope* getNUScope(V2NDesignScope* currentScope, VeriTreeNode* node);
    ///@brief if 'data' contains special character create new data of the form:  "\\" + data + " "
    ///       else return orignal data
    UtString createNucleusString(const UtString& data);
    bool doingOutputSysTasks(NUModule* module);
    bool doInlineTasks();
    /// @brief Function creates an implicit continuous assign for $setuphold
    void SpecifyBlockImplicitConnection(VeriExpression* lhs, VeriExpression* rhs, VeriSystemTimingCheck& node);
    ///@brief returns true if operand of opType is self determined (for operand at position opIndex (index starts at 1))
    bool isOperandSelfDetermined(unsigned opType, bool isUnaryOperation, int opIndex);
    ///@brief Check for initial value on a wire or variable, and return NUExpr*, or NULL if no initial value
    NUExpr* gInitialValueExpr(VeriIdDef* id, VeriDataDecl& node, bool isVectorType, int width);
    ///@brief Create a continuous assign, given an initial value and a net, and put it in the module.
    void createInitialValueContinuousAssign(NUExpr* value, VeriNetDecl& node, NUNet* n);
    ///@brief Create a blocking assign, given an initial value and a NUNet
    NUBlockingAssign* createInitialValueBlockingAssign  (NUExpr* value, VeriDataDecl& node, NUNet* n);
    ///@brief Compose a UtString for VeriSelectedName
    UtString composeSelectedName(const VeriSelectedName* selName);
    //@brief Helper function used to prevent multiple error messages for a given line of RTL
    bool previouslyVisited(const char* name, const SourceLocator* loc);
    //@brief Create a block and scope for begin/end
    NUBlock* createBlockAndPushScope(VeriTreeNode* node, const SourceLocator& sourceLocation, const char* name);
    //@brief Delete block and pop the scope (used for temporary blocks to capture task enables generated
    // by VeriFunctionCall visitor (see comments there).
    void deleteBlockAndPopScope(NUBlock* block);
    //@brief Helper functions to identify the '1' and '0' strengths respectively,
    // and return them.
    unsigned getStrength1(unsigned lval, unsigned rval);
    unsigned getStrength0(unsigned lval, unsigned rval);
    //@brief Returns true if id is declared in global/compilation scope
    bool isInGlobalScope(VeriIdDef* id);
  
///@name Helper methods to dispatch functionality
private:
    /// Builder methods for system tasks
    void cOutputSysTask(VeriSystemTaskEnable* node, NUModule* module, const UtString& name, const SourceLocator& sourceLocation);
    void cControlSysTask(VeriSystemTaskEnable* node, NUModule* module, const UtString& name, const SourceLocator& sourceLocation);
    void cReadmemXSysTask(VeriSystemTaskEnable* node, NUModule* module, const UtString& name, const SourceLocator& sourceLocation);
    void cFFlushOrFCloseSysTask(VeriSystemTaskEnable* node, NUModule* module, const UtString& name, const SourceLocator& sourceLocation);
///@name Global Set Methods 
public:
  
    ///@brief Function updates Nucleus design object, by means of VerificNucleusBuilder API call
    ///should be callled at the end of visit work, as a last step for postprocessing
    void uDesign();

    ///@name Manipulations methods with current scope
    // s - set
    // g - get
    // a - add
    // i - init
    // u - update
    // c - create
///@name Members related to scope
private:
    ///@brief Get the current scope for construction.
    V2NDesignScope* gScope();
    ///@brief Push the current scope. Set current scope to new_scope, create new branch in symbol table, and set current branch scope newly created one
    // (both for elab and unelab branches)
    void PushScope(V2NDesignScope* s, const UtString& scopeName);
    ///@brief Pop the scope to what it was
    void PopScope();
    ///@brief The current scope for construction
    V2NDesignScope* mCurrentScope;
    ///@brief The scope stack (used during construction)
    UtStack<V2NDesignScope*> mScopeStack;
    ///@brief Table of NUScopes and their associated initializer blocks (if any).
    /// VeriNetDecl initializers are translated into blocking assigns, and placed in these
    /// initializer blocks. 
    UtMap<NUScope*, NUBlock*> mInitializerBlocks;
    ///@brief Table of lines of source code that have been visited, entries of the form:
    /// <idname>.<filename>.<linenumber>. This is used to detect multiple visits to the
    /// same line of source code, which in turn is used to avoid duplicate error messages.
    UtHashSet<UtString> mPreviouslyVisited;
    ///@name Utilities
private:
    ///@brief Utility function to get declaration scope and it's type,
    /// in case no named declaration scope the declaration scope will be module/task/function
    NUScope* getDeclarationScope();
    ///@brief Utility function to get declaration scope and it's type,
    /// in case no named declaration scope will check for NUBlock in case no NUBlock the declaration scope will be module/task/function
    NUScope* getDeclarationScope2();
    ///@brief Find initializer block (if any) for supplied scope. Consults mInitializerBlocks. Creates one if it doesn't exist
    NUBlock* gInitializerBlock(NUScope* scope, const SourceLocator& sourceLocation);
    //@brief Helper function, returns true if we are in a context where event controls are legal (inside an always, first stmt).
    bool gEventControlLegal(void) { return mEventControlLegal; };
    //@brief Helper function to set mEventControlLegal boolean
    void sEventControlLegal(bool value)  { mEventControlLegal = value; };
  
    // Get methods for register access
public:
    UtMap<VeriTreeNode*, NUNet*>* gDeclarationRegistry();
    UtMap<VeriTreeNode*, NUTask*>* gTFRegistry();
    UtMap<VeriTreeNode*, StringAtom*>* gNameRegistry();
    ///@name error handling get/set
public:
    bool hasError();
    void setError(bool error);
    ///@name ParamModuleMap create/delete
private:
    void createParamModuleMap(const char* fileRoot, MsgContext* msgContext);
    void destroyParamModuleMap();
public:
    ///@brief if module is not a parametric module 2 names are equal, otherwise we have origname name (HDL specified name) and uniquified name (after static elaboration)
    void gVerilogModuleNames(VeriModule* module, bool escaped, UtString* origModuleName, UtString* moduleName);
private:
    ///@brief Updated libdesign.parameters file
    void analyzeInstanceParameters(VeriModule* ve_module, UtString* modOrigName);
    void getParamValueString(ParamValue* paramVal, VeriExpression* expr);
    void doValOpt(UtString& buf, UInt32& size);
    void getParameterRange(VeriExpression* leftExpr, VeriExpression* rightExpr, ConstantRange* range);
private:
    ///@brief Reference to nuclues builder
    VerificNucleusBuilder* mNucleusBuilder;
    ///@brief Keep local nets verific and nucleus objects
    UtMap<VeriTreeNode*, NUNet*> mDeclarationRegistry;
    ///@brief Keep function/task declarations needed across modules 
    UtMap<VeriTreeNode*, NUTask*> mTFRegistry;
    ///@brief Keep generated names registered with verific objects, for using later in symbol table construction
    UtMap<VeriTreeNode*, StringAtom*> mNameRegistry;
    ///@brief enableOutputSysTasks switch will set this to true, if not specified value will be false
    bool mEnableOutputSysTasks;
    int mrjcDebug;
    bool mInlineTasks;
    ///@brief - keeps track of whether an event control is legitimate. It must be the first item within an always.
    bool mEventControlLegal;
    ///@brief -disallowHierRefs switch will set this to false, if not specified value will be true
    bool mAllowHierRefs;
    ///@brief -warnForSysTask switch will set this to true, if not specified value will be false
    bool mWarnForSysTask;
    ///@brief keeps state if the current always block has side effect (system tasks) statements
    bool mHasSideEffectStatement;
    ///@brief keeps state if the current always block is combinational (only level expressions)
    bool mIsCombAlways;
    ///@brief keeps verific/nucleus portmap for each net, the purpose is to keep track in order to be able correctly handle spilitted ports and indexed (bit, partselect) declarations. Lvalue used to keep nucleus object. Note that we don't populate this for nucleus db, it's just helper so walker destructor needs to delete it at the end
    ScopePortMap mPortMap;
    ///@brief keep last port direction (input, output, inout, ref (TODO)) to use for next ansi ports - for which direction isn't explicitely specified). This is applicable only for ansi-style port declarations.
    unsigned mPortDirectionToUseIfUndefined;
    ///@brief push true if timescale specified for module otherwise false
    ///This member needed to issue warning if some module misses timescale info, while others do have
    ///In normal case either all modules should have timescale specified or noone should have 
    ///for any other case - warning will be issues
    UtVector<bool> mTimeScaleRegistry;
    bool mIgnoreContextSign;

    class ParamModuleMap;
    friend class ParamModuleMap;
    ParamModuleMap* mParamModuleMap;
///@name Prevent the compiler from implementing the following
private:
    VerificVerilogDesignWalker(VerificVerilogDesignWalker &node);
    VerificVerilogDesignWalker& operator=(const VerificVerilogDesignWalker &rhs);
};

/* -------------------------------------------------------------------------- */

} // namespace verific2nucleus 

#endif // #ifndef __VERIFIC_VERILOG_DESIGN_WALKER

