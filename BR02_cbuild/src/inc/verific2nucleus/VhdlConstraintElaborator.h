// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __VHDLCONSTRAINTELABORATOR_H_
#define __VHDLCONSTRAINTELABORATOR_H_

// Project headers
#include "verific2nucleus/VhdlCSElabVisitor.h"

namespace verific2nucleus 
{
  using namespace Verific;

  // Main visitor class for constraint elaboration. This will eventually get too big to put here.
  class VhdlConstraintElaborator : public VhdlCSElabVisitor {
  public:
    VhdlConstraintElaborator(CarbonConstraintManager *cm, CarbonStaticElaborator* ce);

  public:
    // Vhdl Loop Unrolling
    virtual void VHDL_VISIT(VhdlLoopStatement, node);
    // Guard against walking a subprogram body from some point other than a call
    virtual void VHDL_VISIT(VhdlSubprogramBody, node);
    // This visitor processes overloaded operators.
    virtual void VHDL_VISIT(VhdlOperator, node);
    // This visitor processes function and procedure calls. 
    virtual void VHDL_VISIT(VhdlIndexedName, node) ;
    // Construct constraint for return expression, add to signature->constraint map
    virtual void VHDL_VISIT(VhdlReturnStatement, node);

  private:
    virtual void reportFailure(VhdlTreeNode* node, const UtString& msg);
    bool isUnsupportedPragmaFunction(VhdlIndexedName* call_inst);
    bool hasReturnStatement(VhdlSubprogramBody* subprog_body);

    UtString m_CallerSignature;               // Signature of subprogram - availble during walk of subprog body
  };
  
}; // namespace verific2nucleus

#endif // __VHDLCONSTRAINTELABORATOR_H_
