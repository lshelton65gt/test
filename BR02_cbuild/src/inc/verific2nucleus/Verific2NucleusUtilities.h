// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __VERIFIC2NUCLEUSUTILITIES
#define __VERIFIC2NUCLEUSUTILITIES


// Standard headers
#include <fstream>
#include <map>
#include <string>

// Project headers
#include "util/SourceLocator.h"
#include "util/UtSet.h"
#include "util/UtPair.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUScope.h"
#include "VeriModuleItem.h"

//Verific headers.
#include "LineFile.h"

// Verific Vhdl Headers
#include "VhdlStatement.h"
#include "VhdlIdDef.h"
#include "VhdlName.h"
#include "VhdlIdDef.h"
#include "VhdlUnits.h"

// Verific Verilog Headers
#include "VeriModuleItem.h"

class NUNet;
class NUModule;
class NUExpr;
class CarbonContext;

// this macros are used in place of INFO_ASSERT so that the error message will point to a HDL file and line number.  The do{} while(0) construct is used so that users of this macro can put a semicolon after the macro
// another usage can be prevention of future development errors
#define V2N_INFO_ASSERT(condition, verificNode, helpMsg) \
do {UtString msgString; msgString << "ERROR: " << (verificNode).Linefile()->GetFileName() << ":" <<  (verificNode).Linefile()->GetLeftLine() << ", " <<  helpMsg;\
  _do_HELPFUL_ASSERT((condition), __STRING(condition), __FILE__, __LINE__, ( msgString.c_str()));} while (0)

#define NUCLEUS_INFO_ASSERT(condition, loc, helpMsg) \
do {UtString msgString; msgString << "ERROR: " << loc.getFile() << ":" <<  loc.getLine() << ", " <<  helpMsg;\
  _do_HELPFUL_ASSERT((condition), __STRING(condition), __FILE__, __LINE__, ( msgString.c_str()));} while (0)

// VERIFIC_CONSISTENCY* macros should be called in a context where getMessageContext(), setError() API calls are available and so the purpose is just call it for VerificVerilogDesignWalker.h VerificVhdlDesignWalker.h main population walkers.
#define VERIFIC_CONSISTENCY(obj, node, msg) \
do { \
    if (!(obj)) { \
        SourceLocator MA_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,(node).Linefile()); \
        getMessageContext()->VerificConsistency(&MA_loc, msg); \
        setError(true); \
        return 0; \
    } \
} while(0)

#define VERIFIC_CONSISTENCY_NO_RETURN_VALUE(obj, node, msg) \
do { \
    if (!(obj)) { \
        SourceLocator MA_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,(node).Linefile()); \
        getMessageContext()->VerificConsistency(&MA_loc, msg); \
        setError(true); \
        return; \
    } \
} while(0)

#define VERIFIC_CONSISTENCY_WITHOUT_RETURN(obj, node, msg) \
do { \
    if (!(obj)) { \
        SourceLocator MA_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,(node).Linefile()); \
        getMessageContext()->VerificConsistency(&MA_loc, msg); \
        setError(true); \
    } \
} while(0)

namespace verific2nucleus {

  class VerificNucleusBuilder;
  class VerificDesignManager;
  class VerificVerilogDesignWalker;
  class VerificVhdlDesignWalker;
  class V2NDesignScope;

  /**
   * @class Verific2NucleusUtilities.
   *
   * @brief Utilities for verific to nucleus conversion
   */

  class Verific2NucleusUtilities {
  public:
    ///Construct a source locator.
    static SourceLocator gSourceLocator(VerificNucleusBuilder& nb, Verific::linefile_type lf);
    static SourceLocator getSourceLocator(CarbonContext* cc, Verific::linefile_type lf);
    static NUNet* gModuleNet(NUModule* m, UtString net_name, bool case_insensitive=false);
    static NUModule* gNUModule(const UtString& moduleName, const UtString& name, Verific::VhdlComponentInstantiationStatement* ve_stmt);
    static NUModule* gNUModule(Verific::VeriModuleInstantiation* inst);
    // The function intended to lookup only verilog modules (and actually takes VeriModule pointer, in case corresponding NUModule not found)
    static NUModule* gNUModule(Verific::VeriModule* mod);
    static void InitObjects(VerificNucleusBuilder* nb, VerificDesignManager* dm, VerificVerilogDesignWalker* veWalker,
        VerificVhdlDesignWalker* vhWalker);
    static VerificNucleusBuilder* smVerificNucleusBuilder;
    static void createLegalVerilogIdentifier(UtString& moduleName);
    // Return true if for loop has decls in the initializers
    static bool forLoopHasDecls(Verific::VeriFor& node);
    // Return true if the statement has side effects (see comment in .cpp file)
    static bool hasSideEffectStmt(const Verific::VeriStatement* s);
    // Return true if function has no return (either with 'return' keyword or with implicit assignment to function 'name' 
    static bool isVoidFunction(Verific::VeriFunctionDecl* node);
    // Return deuniquified name given a uniquified name string (with the standard suffix)
    static UtString getDeuniquifiedName(const char* unique_name);
    // Return true if the \a subprogram_name was uniquified (with the standard suffix)
    static bool isUniquifiedName(const UtString& subprogram_name);
    // Pretty prints whole parse-tree into single file (supposed to be used for debugging purposes)
    static void dumpParseTree(Verific::VeriModule* topModule, Verific::VhdlPrimaryUnit* topEntity, const UtString& suffix);
    // Debugging function to print Verific VhdlTreeNode::_path_name global variable
    static void dumpPathName(Verific::Array* path_name);
    // Debugging function to print all id's in a Verific Scope.
    static void dumpScope(Verific::VhdlScope *scope);
    // Debugging function to dump the contents of a VhdlDataFlow
    static void dumpDataFlow(Verific::VhdlDataFlow* df);
private:
    // Helper for dumpDataFlow
    static void dumpDataFlowInternal(Verific::VhdlDataFlow* df, UInt32 indent=0);
public:
    // print the whole design into a -dumpVerilog style output directory/file
    static void dumpVerilog(CarbonContext* cc, const char* fileRoot, const char* dirName, Verific::VeriModule* topModule, Verific::VhdlPrimaryUnit* topEntity);

    ///@name Scope get api's
public:
    ///@brief Return nearest declaration scope - named declaration scope or task/module scope (typically used to populate declarations)
    static NUScope* getDeclarationScope(V2NDesignScope* currentScope);
    ///@brief Return nearest block scope - block or task/module scope (typically used to populate statements)
    static NUScope* getBlockScope(V2NDesignScope* currentScope);
    ///@brief Return nearest unit scope - task or module scope
    static NUScope* getUnitScope(V2NDesignScope* currentScope);
    ///@brief Return nearest module scope
    static NUScope* getModuleScope(V2NDesignScope* currentScope);
    ///@brief Return nearest NUNamedDeclarationScope object, return NULL if not found
    static NUNamedDeclarationScope* getNamedDeclarationScope(V2NDesignScope* currentScope);
    ///@brief Return nearest NUBlock object, return NULL if not found
    static NUBlock* getBlock(V2NDesignScope* currentScope);
    ///@brief Return nearest NUTask object, return NULL if not found
    static NUTask* getTask(V2NDesignScope* currentScope);
    ///@brief Return nearest NUModule object, return NULL if not found 
    static NUModule* getModule(V2NDesignScope* currentScope);
public:
    // Return true if id was declared in an IEEE library
    static bool isIeeeDecl(Verific::VhdlIdDef* id);
    // Return true if id was declared in an STD library
    static bool isStdDecl(Verific::VhdlIdDef* id);
    ///@brief Check if an identifier is a subprogram declared in a package from the IEEE library
    static bool isIeeeSubprogram(Verific::VhdlIdDef* id);
    ///@brief Check if an operator id corresponds to an overloaded operator that is NOT part of the IEEE library
    static bool isNonIeeeOverloadedOperator(Verific::VhdlIdDef* opId);
    //@brief Check if this is one of the standard textio subprograms
    static bool isStdTextioSubprogram(Verific::VhdlIdDef* id);
    //@brief Check if this is one of the ieee std_logic_11164 edge detection functions
    static bool isIeeeEdgeDetectFunction(Verific::VhdlIdDef* id);
    //@brief This method collects arguments to a Vhdl subprogram in an array. Option to get default value
    //if argument is not specified.
    static void getSubprogramArgs(Verific::Array* array, Verific::VhdlIndexedName* call_inst, bool get_dflt);
    //@brief Given position of formal subprogram parameter, get initial assign expression (if any)
    static Verific::VhdlExpression* getInitialAssign(unsigned idx, Verific::VhdlSubprogramId* sid);
    //@brief Determine whether the bounds of a range are constant, fill in left, right bounds if arguments are supplied
    static bool isConstantRange(Verific::VhdlDiscreteRange* range, Verific::VhdlDataFlow* df, SInt32* l = NULL, SInt32* r = NULL);
    //@brief Return true if a Vhdl loop should be unrolled, see comments in .cpp file
    static bool loopShouldBeUnrolled(Verific::VhdlLoopStatement* loop, Verific::VhdlDataFlow* df, bool forceUnroll);
    //@brief set up sub dataflow for a loop that cannot be unrolled.
    static Verific::VhdlDataFlow* setupLoopDataFlow(Verific::Array* data_flows, Verific::VhdlDataFlow* parent, Verific::VhdlLoopStatement* loop);
    //@brief set up dataflow for branch (typically used for conditional statement branches)
    static Verific::VhdlDataFlow* setupBranchDataFlow(Verific::Array* data_flows, Verific::VhdlDataFlow* parent_df);
    //@brief Conditional statement live branch detection
    static bool branchIsLive(Verific::VhdlExpression *relational_expr, Verific::VhdlDataFlow* df, bool* short_circuited);
    //@brief Cleanup dataflow object
    static void cleanUpDataFlows(Verific::VhdlStatement* node, Verific::Array* data_flows, Verific::VhdlDataFlow* curDF);
    //@brief Deletes/Nullifies all elements in the \a statements array. NOTE: array size remains same with all elements being NULL
    static bool nullifyChildStmts(Verific::VhdlTreeNode* parent_node, Verific::Array* statements);
    //@brief Get subprogram name from VhdlIndexedName* (returns 'conv_integer' for 'ieee.std_logic_unsigned.conv_integer')
    static UtString getConvSubprogramName(const Verific::VhdlIndexedName* node);
    //@brief Get VhdlIdDef node from VhdlIndexedName*
    static Verific::VhdlIdDef* getConvSubprogramPrefix(const Verific::VhdlName& node);
    //@brief Return >0 if supplied name is a IEEE package function that we're treating as a builtin
    static unsigned isIEEEPackageBuiltinFunction(Verific::VhdlIndexedName* call_inst);
    //@brief Return instantiated entity or module, given VhdlComponentInstantiation*
    static void getInstantiatedEntityOrModule(Verific::VhdlComponentInstantiationStatement* inst, Verific::VhdlEntityDecl** e, Verific::VeriModule** m);
    //@brief Advanced utiltiy function to check for verific elaboration flow created empty (in opposite to Verific version this one looks deeply into blocks).
    /// Return true if this block is either elab created empty or is a hieararchy of VhdlBlockStatements which
    /// all contain elab created empties
    static bool isElabCreatedEmpty(Verific::VhdlStatement* stmt);
    //@brief Return a Verific "pretty print" of a node in a UtString.
    static UtString verific2String(const Verific::VhdlTreeNode& n);
    ///@brief replace single child statement with statements in \a new_statements array, caller maintains ownership of \a child and the Array \a new_statements, \a new_statements can be null which means delete \a child
    static bool replaceChildStmtBy(Verific::VhdlTreeNode* parent_node, Verific::VhdlStatement* child, const Verific::Array* new_statements);
    //@brief Find VHDL statement that owns supplied scope
    static Verific::VhdlStatement* findScopeStatement(Verific::Array* statements, Verific::VhdlScope* scope);
    //@brief Return true if this id is underneath a process or subprogram scope  
    static bool isInProcessOrSubprogram(Verific::VhdlIdDef* id);
    //@brief Return false if Verific array has any non-NULL entries.
    static bool isArrayEmpty(Verific::Array* a);
    //@brief Return the number of non-NULL entries in \a array
    static UInt32 countNonNullArrayItems(Verific::Array* array);
    //@brief returns true if lval is a composite (record)
    static bool isComposite(NULvalue* lval);
    //@brief Undeclare a for loop statement label in the label's owning scope.
    static bool undeclareLoopLabel(Verific::VhdlLoopStatement* loop_stmt);
    //@brief Helper function to return bounds of the prefix of an indexed name, given the prefix constraint
    static void getPrefixBounds(Verific::VhdlConstraint* prefix_constraint, SInt32* prefix_left, SInt32* prefix_right);
    //@brief Helper function to return the last non-null association of an indexed name
    static Verific::VhdlDiscreteRange* getLastAssoc(const Verific::VhdlIndexedName& node);
    //@brief Helper function to return true if an expression contains a slice with null ranges
    static bool hasNullRange(const Verific::VhdlDiscreteRange& expr, Verific::VhdlDataFlow* df);
    

  private: // methods, used internally
    //@brief Helper function to extract integer range values, returns true if both bounds are constant integers
    static bool getConstantRangeBounds(Verific::VhdlValue* left, Verific::VhdlValue* right, SInt32* left_bound, SInt32* right_bound);
    //@brief Helper function to find all ids on the left hand side of an assignment
    static void findAndInvalidateAssignedIdsInLoop(Verific::VhdlDataFlow* parent_df, Verific::VhdlLoopStatement* loop);
    //@brief Helper function to return the declaration scope given a loop scope
    static Verific::VhdlScope* getDeclarationScopeForLoopScope(Verific::VhdlScope* loop_scope);
    
  private:
    static VerificDesignManager* smVerificDesignManager;
    static VerificVerilogDesignWalker* smVerificVerilogDesignWalker;
    static VerificVhdlDesignWalker* smVerificVhdlDesignWalker;
  };

  
}




#endif
