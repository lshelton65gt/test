// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONCONSTRAINTMANAGER_H_
#define __CARBONCONSTRAINTMANAGER_H_

#include "util/UtString.h"
#include "util/UtMap.h"

namespace Verific {
    class VhdlPrimaryUnit;
    class VeriModule;
    class Array;
    class VhdlIndexedName;
    class VhdlSubprogramId;
    class VhdlExpression;
    class VhdlOperator;
    class VhdlConstraint;
    class VhdlDataFlow;
    class VhdlValue;
    class VhdlSubprogramId;
    class VhdlSubprogramBody;
    class VhdlIdDef;
    class VhdlScope;
}

namespace verific2nucleus {

using namespace Verific;

// This class is responsible for several things:
//
// 1) It encapsulates a mapping from function call signatures
//    onto return value constraints. This mapping is used during design traversal (via a
//    VhdlVisitor based class) to look up the return constraint of a function at the point
//    where it is called.
//
// 2) It has methods for building and accessing the constraint map.
//
// 3) It has methods for building constraints from expressions, including
//    exprssions that contain overloaded operators and function calls. For these,
//    it uses the constraint map.
//
// The constraint map is built during a walk of the Verific elaborated Vhdl parse
// tree. 
//
// Constraints added to this map are copied, and placed in the map. They are
// freed by the destructor.

class CarbonConstraintManager {
public:
  CarbonConstraintManager(void) ;
  ~CarbonConstraintManager(void);

public: // Methods

  // @brief Add an entry to the signature->constraint map. Returns true on success, 
  // false if a different constraint was already registered for this signature.
  // Caller is responsible for freeing the constraint.
  bool addSignatureConstraint(UtString signature, VhdlConstraint* constraint);
  
  // @brief Create an entry in the mSubprogramBodies map that maps a subprogram call
  // signature to a (already) uniquified subprogram body pointer. This is used later 
  // to determine which uniquified subprogram corresponds to a call instance.
  void addSignatureSubprogramBody(UtString signature, VhdlSubprogramBody* sBody);
  
  // @brief Construct a constraint from an expression, given target constraint. Can 
  // return an unconstrained or NULL constraint, which the caller is responsible for freeing.
  VhdlConstraint* createConstraintFromExpression(VhdlExpression *expr, VhdlConstraint* target_constraint, VhdlDataFlow* df);

  // @brief Get a signature, given an instance of a function of procedure call
  UtString getSubprogramSignature(VhdlIndexedName* call_inst, VhdlDataFlow* df);

  // @brief Get a signature, given an overloaded operator
  UtString getOverloadedOperatorSignature(VhdlOperator* oper, VhdlDataFlow* df);

  // @brief Lookup the return constraint of function with specified signature
  VhdlConstraint* lookupFunctionReturnConstraint(UtString signature);
  
  // @brief Lookup the uniquified subprogram body for a given signature. Returns
  // NULL if signature has no entry in the subprogram body table. (This is
  // an error.)
  VhdlSubprogramBody* lookupSubprogramBody(UtString signature);

  // @brief Create a uniquified name for a subprogram, given the original name
  // and the scope in which the name is to be declared. 
  UtString getUniquifiedName(VhdlScope* scope, const char* name);
  
  // @brief Get the original name, given a uniquified name.
  UtString getDeuniquifiedName(const char* name);

  // @brief Consistency check verifies that functions encountered during
  // constraint elaboration have been uniquified during subprog uniquification.
  bool checkConsistency(void);
  
private: // Methods 
 
  // @brief Get constraint on function return value, given VhdlIndexedName* ptr to function call
  VhdlConstraint* getFunctionCallReturnConstraint(VhdlIndexedName* call_inst, VhdlDataFlow* df);
  // @brief Get constraint on overloaded operator 
  VhdlConstraint* getOverloadedOperatorReturnConstraint(VhdlExpression* expr, VhdlDataFlow* df);
  // @brief Helper function for getSignature - computes signature of containing subprogram
  UtString getContainingSubprogramSignature(VhdlIdDef* containing_subprog);
  // @brief Create a unique signature for a subprogram (or overloaded operator) call, given args
  UtString getSignature(VhdlSubprogramId* sId, Array* args, VhdlDataFlow* df);
  // @brief Return true if two constraints require the same number of bits to represent in hardware 
  bool areConstraintsEqual(VhdlConstraint* constr1, VhdlConstraint* constr2);
  // @brief Creates VhdlConstraint for an expression that has an unconstrained target. The expression should not have operators.
  VhdlConstraint* createConstraintFromPrimary(VhdlExpression* vhdlExpr, VhdlConstraint* targetConstraint, VhdlDataFlow* df);
  // @brief Construct the return constraint for a type conversion instance
  VhdlConstraint* createConstraintFromTypeConversion(VhdlIndexedName* inst, VhdlDataFlow* df);
  // @brief Construct the return constraint for an IEEE package function that we're treating as a builtin
  VhdlConstraint* createConstraintFromBuiltinFunction(VhdlIndexedName* inst, VhdlDataFlow* df, unsigned op = 0);
  // @brief Create a constraint based on the results of an operation, given the left and right constraints, and the target constraint.
  VhdlConstraint* mergeConstraints(unsigned oper, VhdlConstraint* left, VhdlConstraint* right, VhdlConstraint* targetConstraint);
  // @brief Create a constraint for single bit of a vector, given the constraint on the vector.
  VhdlConstraint* createBitConstraint(VhdlConstraint* targetConstraint);
  // @brief Create an array constraint given the left and right ranges of the array, and the element constraints.
  VhdlConstraint* createArrayConstraint(SInt32 range_left, SInt32 range_right, VhdlValue* elem_left, VhdlValue* elem_right);
  
  
private: // Private Data

  // Map of function call signatures onto VhdlConstraint* pointers for use in evaluating constraints,
  // and for function uniquification.
  UtMap<UtString, VhdlConstraint*> mFunctionReturnConstraints;
  // Map of subprogram (function and procedure) call signatures onto uniquified subprogram bodies
  UtMap<UtString, VhdlSubprogramBody*> mSubprogramBodies;
  // Keeps track of the number of unique copies made for a particular subprogram.
  // Used for creating uniq names.
  UtMap<UtString, UInt32> mUniqCopyCount;
};

} // namespace verific2nucleus

#endif // __CARBONCONSTRAINTMANAGER_H_
