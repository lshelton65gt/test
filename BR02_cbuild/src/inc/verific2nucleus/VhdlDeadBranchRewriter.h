
// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __VHDLDEADBRANCHREWRITER_H_
#define __VHDLDEADBRANCHREWRITER_H_

// Project headers
#include "verific2nucleus/VhdlCSElabVisitor.h"

// Carbon Utilities
#include "util/UtList.h"
#include "util/UtSet.h"

namespace verific2nucleus {

class CarbonConstraintManager;

using namespace Verific;

//Main class for replacing dead branch statements with null and conditions constant expression with true/false constants 
class VhdlDeadBranchRewriter : public VhdlCSElabVisitor {
///@name Constructors
public:
    VhdlDeadBranchRewriter(CarbonConstraintManager* cm, CarbonStaticElaborator* se);
    ///@name utilties
public:
    bool alreadyVisited(VhdlSubprogramBody* body);
    bool nullifyDeadAltStmtsAndVisitLiveAltStmts(VhdlCaseStatement* node, VhdlCaseStatementAlternative* live_alt);
    //@brief Return true if case expression is supporte constant (non-composite constant)
    bool isSupportedConstCaseCond(VhdlValue* case_cond);
    virtual void reportFailure(VhdlTreeNode* node, const UtString& msg);
    //@ brief Call this after tree walk complete, this finishes the analysis by deleting statements.
    bool finish();
    ///@name Visitors
public:
    virtual void VHDL_VISIT(VhdlIfStatement, node);
    virtual void VHDL_VISIT(VhdlCaseStatement, node);
    // For managing data-flows (TODO: consider moving this to VhdlCSEElabVisitor at least 2 visitors (VhdlSubprogCallUniquifier and VhdlDeadBranchRewriter use same implementation))
    virtual void VHDL_VISIT(VhdlLoopStatement, node);
    // Below 3 visitors enable subprogram body walk (in opposite to base visitor approach of visiting subprograms from declaration place)
    virtual void VHDL_VISIT(VhdlSubprogramBody, node);
    virtual void VHDL_VISIT(VhdlIndexedName, node);
    virtual void VHDL_VISIT(VhdlOperator, node);

private:
    //@brief Remember child statements we need to nullify, these must be delayed until after
    // traversal or else IDs get deleted during traversal which causes segfaults.
    void rememberNullifyChildStmts(VhdlTreeNode* parent_node, Array* statements);

    //@brief Nullify child statements after traversal is complete.
    bool doNullifyChildStmts();

    //@brief Walk the subprogram body, including doing necessary setup.
    void doWalkSubprogramBody(VhdlSubprogramBody* subprog_body, Array* args);

    //@brief Parent node and child statements pair, used to nullify child statements.
    typedef UtPair<VhdlTreeNode*,Array*> ParentChildPair;

    //@brief List of child statements to nullify post-traversal.
    UtList<ParentChildPair> mNullifyList;

    // Return true if subprogram body has already been visited.
    UtSet<VhdlSubprogramBody*> mVisited;              // Set of subprogram bodies that have already been visited.
}; // class VhdlDeadBranchRewriter

} // namespace verific2nucleus

#endif // __VHDLDEADBRANCHREWRITER_H_
