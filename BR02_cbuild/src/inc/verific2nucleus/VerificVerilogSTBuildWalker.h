// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __VERIFIC_VERILOG_ST_BUILD_WALKER_H
#define __VERIFIC_VERILOG_ST_BUILD_WALKER_H

#include "VeriVisitor.h"    // Visitor base class definition

// Project headers
#include "util/SourceLocator.h"
#include "util/UtPair.h"
#include "util/UtMap.h"
#include "util/UtStack.h"
#include "nucleus/NUStmt.h"

class NUNet;
class NUModule;
class NUExpr;

namespace verific2nucleus {


class VerificNucleusBuilder;
class V2NDesignScope;
class VerificDesignManager;
class VerificVhdlDesignWalker;

using namespace Verific;

/**
 * @class VerificVerilogSTBuildWalker
 * 
 * @brief Build up symbol table with all the branch and net information as well user type information) 
 *
 * example:
 * TODO
 */
    
class VerificVerilogSTBuildWalker: public Verific::VeriVisitor
{
public:
    VerificVerilogSTBuildWalker(VerificNucleusBuilder* verificNucleusBuilder, UtMap<VeriTreeNode*,StringAtom*>* nameRegistry,
        UtMap<VeriTreeNode*,NUNet*>* declarationRegistry);
    ~VerificVerilogSTBuildWalker();
private:
    ///@brief add new symbol table scope (to both elab and unelab branches), for root module case - elabParent, unelabParent branches will be NULL, in all other cases (there is a assertion for mentioned logic).
    ///@param name - name of the new branch scope
    ///@param unelabParent - parent scope for unelaborated branch
    ///@param elabParent - parent scope for elaborated branch
    void aSTScope(const UtString& name, STBranchNode* unelabParent, STBranchNode* elabParent);
    ///@brief add new symbol table node(net) (to both elab and unelab branches), current scope branches used as a parent
    ///@param netName - name of the node(net)
    ///@param net - created nucleus net object
    ///@param netType - verific enum for net types
    void aSTNode(const UtString& netName, NUNet* net, unsigned netType);
private:
    //visit verific verilog parse tree nodes which can contain scopes or net declarations
    virtual void VERI_VISIT(VeriModule, node);
    virtual void VERI_VISIT(VeriFunctionDecl, node);
    virtual void VERI_VISIT(VeriAlwaysConstruct, node);
    virtual void VERI_VISIT(VeriTaskDecl, node);
    virtual void VERI_VISIT(VeriSeqBlock, node);
    virtual void VERI_VISIT(VeriModuleInstantiation, node);
    virtual void VERI_VISIT(VeriDataDecl, node);
    virtual void VERI_VISIT(VeriAnsiPortDecl, node);
    virtual void VERI_VISIT(VeriNetDecl, node);
    virtual void VERI_VISIT(VeriSelectedName, node);
    virtual void VERI_VISIT(VeriConditionalStatement, node);
    virtual void VERI_VISIT(VeriCaseStatement, node);
    virtual void VERI_VISIT(VeriCaseItem, node);
    virtual void VERI_VISIT(VeriFor, node);
    virtual void VERI_VISIT(VeriEventControlStatement, node);
    virtual void VERI_VISIT(VeriRepeat, node);
    virtual void VERI_VISIT(VeriWhile, node);
    virtual void VERI_VISIT(VeriInitialConstruct, node);
    virtual void VERI_VISIT(VeriDelayControlStatement, node);
    virtual void VERI_VISIT(VeriGenerateBlock, node);
    
///@name Utilities
private:
    UtString gVisibleName(VeriTreeNode* ve_node, const UtString& origName);
    VeriDataType* gMaxRangeDataType(VeriDataType* first, VeriDataType* second);
    VeriRange* gMaxRange(VeriRange* first, VeriRange* second);
    VeriIdDef* fActualRefId(VeriExpression* vpc);
    UtString createNucleusString(const UtString& data);
    NUNet* findNet(VeriTreeNode* verificNet);
    StringAtom* findName(VeriTreeNode* verificObject);
///@name Members related to scope
private:
    ///@brief Get the current scope for construction.
    V2NDesignScope* gScope();
    ///@brief Push the current scope. Set current scope to new_scope, create new branch in symbol table, and set current branch scope newly created one
    // (both for elab and unelab branches)
    void PushScope(V2NDesignScope* s, const UtString& scopeName);
    ///@brief Pop the scope to what it was
    void PopScope();
    ///@brief The current scope for construction
    V2NDesignScope* mCurrentScope;
    ///@brief The scope stack (used during construction)
    UtStack<V2NDesignScope*> mScopeStack;
    // for building symbol table we will use api provided by nucleus builder
    VerificNucleusBuilder* mNucleusBuilder;
    ///@name Object registery
private:
    UtMap<VeriTreeNode*, StringAtom*>* mNameRegistry;
    UtMap<VeriTreeNode*, NUNet*>* mDeclarationRegistry;
};

}

#endif // __VERIFIC_VERILOG_ST_BUILD_WALKER_H
