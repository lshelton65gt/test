// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef VERIFIC_VHDL_STBUILD_WALK_H_
#define VERIFIC_VHDL_STBUILD_WALK_H_

#include "verific2nucleus/VerificPrePopInfoWalker.h"
#include "verific2nucleus/VerificNucleusBuilder.h"

//RJC RC#18 The following comment 'forward declaration' (and another one a few lines below) is unnecessary, it is obvious to the reader that this is a forward declaration.
//forward declaration
//ADDRESSED

namespace verific2nucleus {

using namespace Verific;

//forward declaration
class STBuildWalkHelper;
class VerificSTBuildWalkerImpl;    

//RJC RC#18  I see that in this case the class name VerificSTBuildWalker is using a name patterned after the interra walker callbacks (that is what the CB in the name refers to)
// however this class is designed for the Verific _VISIT macro so if you wanted to name the class STBuildWalkerVisitors or something like that then it
// might make the code easier to understand by someone who has never seen the interra flow.  We expect that this code will be in use 10
// years from now so anything done today will have a 10 year shadow.  Shadows can be good or bad.  They are bad when they obscure the code
// or make it harder to navigate, as is the current state since the class refers to an artifact of a walker that is not being used here.
// They can be good if they allow the reader to relax in the cool shade -- such as when everything is just where the reader expects, and the
// terminology is consistent throughout.
// Once you decide on a name for this class you should rename this file based on that class, so if the name of this class is STBuildWalkerVisitors then the name of this file should be
// STBuildWalkerVisitors.h with the appropriate changes to the include guard .
//ADDRESSED
//! Walker to build elaborated and unelaborated design data symbol tables.
//RJC RC#18 you should explain what types of symbols are being added here, or why only 8 visitors are needed.  Do these 9 items cover all situations that might provide a level of scope, or a new name?
//ADDRESSED

/**
 * @class VerificSTBuildWalker
 * 
 * @brief Added net information to the skleton symbol table
 *        provided by PrePopInfo phase.
 *        Each declared net added to the corresponding scope
 *        branch where net is declared.
 *
 *        The VerificSTBuildWalker class travers through vhdl parse tree
 *        and visit nodes which are can contain declaration of net
 *        and add corresponding net information to symbol table
 *
 * example:
 *  entity pos_nat is
 *   port ( in1 : positive;
 *          in2 : natural;
 *          out1 : out natural
 *         );
 *  end;
 *
 * architecture pos_nat of pos_nat is
 * begin
 *  process(in1,in2)
 *  begin
 *   out1<= in1 + in2;
 *  end process;
 * end;
 *
 * Symbol table provided by PrePopInfo
 * 
 *       Name: work_pos_nat_default_pos_nat (0xb7b5938c)
 *       Parent: (nil)
 *       BOM: 0x0
 *
 *         Name: named_decl_lb (0xb7b59354)
 *         Parent: 0xb7b5938c
 *         BOM: 0x0
 *
 * Result symbol table
 *
 * Num nodes: 5
 * AtomicCache: 0xb7800074
 *
 * Name: work_pos_nat_default_pos_nat (0xb7b893fc)
 * Parent: (nil)
 * BOM: 
 *   Name: in1 (0xb7be3f60)
 *   Parent: 0xb7b893fc
 *   Master: 0xb7be3f60
 *   Store: 0xb7be3f60
 *   BOM:   Aliases:
 *   Name: in2 (0xb7be3fa0)
 *   Parent: 0xb7b893fc
 *   Master: 0xb7be3fa0
 *   Store: 0xb7be3fa0
 *   BOM:   Aliases:
 *   Name: out1 (0xb7be3fe0)
 *   Parent: 0xb7b893fc
 *   Master: 0xb7be3fe0
 *   Store: 0xb7be3fe0
 *   BOM:   Aliases:
 *   Name: named_decl_lb (0xb7b89450)
 *   Parent: 0xb7b893fc
 *   BOM: 
 */
    
class VerificSTBuildWalker: public Verific::VhdlVisitor
{
public:

    VerificSTBuildWalker(VerificPrePopInfoWalker* prePopInfo,
                  VerificNucleusBuilder* nucleusBuilder,
                  UtMap<VhdlTreeNode*, StringAtom*>* nameRegistry,
                  IODBDesignDataSymTabs* symtabs);
    ~VerificSTBuildWalker();
    
private:
    //visit verific parse tree nodes which
    //can containt net declaration
    virtual void VHDL_VISIT(VhdlEntityDecl, node);
    virtual void VHDL_VISIT(VhdlInterfaceDecl, node);
    virtual void VHDL_VISIT(VhdlFileDecl, node);
    virtual void VHDL_VISIT(VhdlSignalDecl, node);
    virtual void VHDL_VISIT(VhdlVariableDecl, node);
    virtual void VHDL_VISIT(VhdlConstantDecl, node);
    virtual void VHDL_VISIT(VhdlAliasDecl, node);
    virtual void VHDL_VISIT(VhdlArchitectureBody, node);
    virtual void VHDL_VISIT(VhdlProcessStatement, node);
    virtual void VHDL_VISIT(VhdlBlockStatement, node);
    virtual void VHDL_VISIT(VhdlComponentInstantiationStatement, node);
    
private:
    void createBranchForHierRef();
private:

    void addHierRefNet(VhdlIdDef* node);
    void addNet(VhdlIdDef* node);
    void pushElabScope(const char* scope);
    void pushElabScope(StringAtom* scopeAtom);
    void popElabScope();
    void pushElabAndUnelabScopes(const char* scope, SInt32 scopeType);
    void popElabAndUnelabScopes();
    StringAtom* findName(VhdlTreeNode* verificObject);

    VerificNucleusBuilder* mNucleusBuilder;
    UtMap<VhdlTreeNode*, StringAtom*>* mNameRegistry;
    VerificSTBuildWalkerImpl* mSTBuildImpl;
    STBranchNode* mPrePopElabScope;
    VerificPrePopInfoWalker* mPrePopInfo;
};

}

#endif
