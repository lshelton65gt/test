// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef VERIFIC_VHDL_PRE_POP_INFO_H_
#define VERIFIC_VHDL_PRE_POP_INFO_H_

#include "util/CarbonAssert.h"
#include "util/UtHashMap.h"
#include "util/UtMap.h"
#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "util/UtXmlWriter.h"
#include "util/AtomicCache.h"
#include "symtab/STFieldBOM.h"
#include "symtab/STBranchNode.h"
#include "symtab/STSymbolTable.h"
#include "util/Zstream.h"
#include "VhdlVisitor.h"
#include "verific2nucleus/VerificNucleusBuilder.h"

namespace verific2nucleus {

using namespace Verific;

//forward declaration
class VerificPrePopInfoWalkerImpl;

//RJC RC#18  what is this class used for?, I am guessing that the comment should explain that this class is a set of VHDL_VISIT methods that
//are used to fill in the symbol table?  Some information that is needed here: Why are only 4 visitor methods required?  Once this walker has
//finished, what is the result? Is a symbol table populated? Are NU objects been created?
//RJC RC#18  comment on the consistency of file names and class names -- In the carbon style classes that derive from a Verific::VhdlVisitor
//should use a name that hints at the use of the class.  See the comment in VerificSTBuildWalk.h about the potential renaming of
//STBuildWlakCB to STBuildWalkerVisitors.  Therefore I suggest that PrePopInfo might be better named something like PrePopInfoWalkerors.  In
//any case the name of this file should match the name of this class.
// Once you decide on a name for this class you should rename this file based on that class, so if the name of this class is PrePopInfoWalkerors then the name of this file should be
// PrePopInfoWalkerors.h with the appropriate changes to the include guard .
//ADDRESSED

/**
 * @class VerificPrePopInfoWalker
 *
 * @brief Build hierarchy of branch nodes for symbol table
 *        The PrePopInfo class generate symbol table skeleton
 *        for scopes, which is represented in NU as an NUScope
 *        object.
 *        This class provide only hierarchy of scopes corresponding
 *        to source file
 *
 * example:
 *  entity pos_nat is
 *   port ( in1 : positive;
 *          in2 : natural;
 *          out1 : out natural
 *         );
 *  end;
 *
 * architecture pos_nat of pos_nat is
 * begin
 *  process(in1,in2)
 *  begin
 *   out1<= in1 + in2;
 *  end process;
 * end;  
 *       Name: work_pos_nat_default_pos_nat (0xb7b5938c)
 *       Parent: (nil)
 *       BOM: 0x0
 *
 *         Name: named_decl_lb (0xb7b59354)
 *         Parent: 0xb7b5938c
 *         BOM: 0x0
 */

class VerificPrePopInfoWalker : public Verific::VhdlVisitor
{
public:

    VerificPrePopInfoWalker(VerificNucleusBuilder* nuBuilder, UtMap<VhdlTreeNode*, StringAtom*>* nameRegistry);
   ~VerificPrePopInfoWalker();

public:
    void uniquify();
//    Helper* gHelper() { return mHelper; }
    void print() const;
    STBranchNode* findElabScope(STBranchNode* parent, StringAtom* instAtom);
    StringAtom* findName(VhdlTreeNode* verificObject);
    
private:
    //visit nodes on verific parse tree which can contain scopes
    virtual void VHDL_VISIT(VhdlEntityDecl, node);
    virtual void VHDL_VISIT(VhdlBlockStatement, node);
    virtual void VHDL_VISIT(VhdlProcessStatement, node);
    virtual void VHDL_VISIT(VhdlComponentInstantiationStatement, node);

private:
    void createBranchForHierRef();
    void createBranchForRecord();
    
private:
    VerificNucleusBuilder* mNucleusBuilder;
    UtMap<VhdlTreeNode*, StringAtom*>* mNameRegistry;
    VerificPrePopInfoWalkerImpl* mVerificPrePopInfoWalkerImpl;
};

static const char* scElabBOMSig = "PrePopVerificCB_ElabBOM";

enum NucleusType {
  eNone, //!< No type assigned.
  eTaskFunction, //!< NUTask (tasks and functions)
  eModule, //!< NUModule
  eNamedDeclScope, //!< NUNamedDeclarationScope
  eContAssign //!< NUContAssign (gate primitive, e.g)
};

static const char* sNucleusTypeStr(NucleusType type)
{
  switch (type)
  {
  case eNone: return "None"; break;
  case eTaskFunction: return "NUTask"; break;
  case eModule: return "NUModule"; break;
  case eNamedDeclScope: return "NUNamedDeclarationScope"; break;
  case eContAssign: return "NUContAssign"; break;
  }
  return NULL;
}

//------------------- BranchStack -----------------------//

//! Manages stack of symbol table branch nodes.
/*!
  Has ability to backup current stack and work on a new stack. The new stack starts
  with root which is at the bottom of current stack. This can backup only
  one stack.
*/
class BranchStack
{
public:
    
    BranchStack()
        : mStack(NULL),
          mSavedStack(NULL)
        {
            mStack = new STBStack;
        }
    ~BranchStack()
        {
            delete mStack;
            delete mSavedStack;
        }
    
    //! Empty stack
    bool empty()
        {
            return mStack->empty();
        }
    //! Push to top of stack.
    void push(STBranchNode* scope)
        {
            mStack->push_back(scope);
        }
    //! Pop from top of the stack.
  void pop()
        {
            mStack->pop_back();
        }
    //! The branch node at top of the stack.
    STBranchNode* top()
        {
            return mStack->back();
        }
    
    //! Save current stack away and begin a new stack with root inserted in it.
    /*!
      Can be used to save only one stack. Call restoreStack before calling this again.
    */
    void saveStackAndBeginAtRoot()
        {
            INFO_ASSERT(mSavedStack == NULL, "Unexpected multi-level stack save.");
      mSavedStack = mStack;
      mStack = new STBStack;
      // Start at the root node in saved stack.
      mStack->push_back(mSavedStack->front());
        }
    //! Restore the backed up stack and delete the current one.
    void restoreStack()
        {
            INFO_ASSERT(mSavedStack != NULL, "No stack saved for restore.");
            delete mStack;
            mStack = mSavedStack;
            mSavedStack = NULL;
        }
    
private:
    typedef UtList<STBranchNode*> STBStack;
    STBStack* mStack;
    STBStack* mSavedStack;
};

//This class represents implementation of VerificPrePopInfoWalker
//which encapsulates 2 symbol tables
//1. elaborated
//2. unelaborated
//Above mentioned symbol tables are created in this class
class VerificPrePopInfoWalkerImpl
{
  //! Module signature class
  /*!
    A module signature is based on a the signatures of its children.
  */
  class ModuleSignature
  {
  public:
    ModuleSignature() : mRef(0) {}
    ~ModuleSignature() {}

    //! Append a child signature.
    void append(const char* partialSig)
    {
      mSignature.push_back(partialSig);
    }

    //! Compose this module's signature.
    const char* compose(UtString* buf) const
    {
      for (UtStringArray::UnsortedCLoop p = mSignature.loopCUnsorted();
           ! p.atEnd(); ++p)
      {
        if (! buf->empty())
          *buf << "_";
        *buf << *p;
      }
      return buf->c_str();
    }

    //! For hashmap/sets.
    size_t hash() const {
      UtString buf;
      compose(&buf);
      size_t ret = buf.hash();
      ret += mRef;
      return ret;
    }

    //! compare two module signatures.
    bool operator==(const ModuleSignature& other) const
    {
      if (this == &other)
        return true;
      
      const UtStringArray& otherSig = other.mSignature;
      UInt32 mySize = mSignature.size();
      if (mySize != otherSig.size())
        return false;
      for (UInt32 i = 0; i < mySize; ++i)
      {
        if (strcmp(mSignature[i], otherSig[i]) != 0)
          return false;
      }
      return true;
    }

    //! Increment the reference count
    /*!
      This happens when a module is uniquified.
    */
    UInt32 incrRefCnt() 
    {
      ++mRef;
      return mRef;
    }

  private:
    UtStringArray mSignature;
    UInt32 mRef;
  }; //ModuleSignature

  class ElabBranchData
  {
  public:
    CARBONMEM_OVERRIDES
    
    ElabBranchData() : mDeclarationScope(NULL), 
                       mNucleusType(eNone),
                       mSignature(NULL)
    {}
    ~ElabBranchData() {}

    //! Get the current signature
    ModuleSignature* getSignature() { return mSignature; }

    //! Put a signature
    /*!
      Used to replace a current signature, usually.
    */
    void putSignature(ModuleSignature* newSig)
    {
      mSignature = newSig;
    }

    //! Set the uniquename to the StringAtom
    void putUniqueBlockName(const StringAtom* nameBr) {
      mUniqueBlockName.assign(nameBr->str());
    }

    //! Set the uniquename to the constructed name
    void putUniqueBlockName(const UtString& name) {
      mUniqueBlockName.assign(name);
    }
    
    void putDeclarationScope(STBranchNode* branchNode)
    {
      mDeclarationScope = branchNode;
    }

    STBranchNode* getDeclarationScope()
    {
      return mDeclarationScope;
    }

    void putNucleusType(NucleusType type)
    {
      mNucleusType = type;
    }

    NucleusType getNucleusType() const {
      return mNucleusType;
    }
    
    bool isModule() const {
      return mNucleusType == eModule;
    }

    bool isTaskFunction() const {
      return mNucleusType == eTaskFunction;
    }

    const char* getUniqueBlockName() const
    {
      INFO_ASSERT(! mUniqueBlockName.empty(), "Block name of elaborated block was not set.");
      return mUniqueBlockName.c_str();
    }
    
    UtString& getUniqueBlockNameStr()
    {
      return mUniqueBlockName;
    }

    void print() const
    {
      UtIO::cout() << "\tUniqueBlockName: " << getUniqueBlockName() << UtIO::endl;
      UtIO::cout() << "\tDeclarationScope: ";
      if (mDeclarationScope)
      {
        UtIO::cout() << UtIO::hex << mDeclarationScope << UtIO::dec << ": ";
        UtString buf;
        mDeclarationScope->compose(&buf);
        UtIO::cout() << buf;
      }
      else 
        UtIO::cout() << "(null)";
      UtIO::cout() << UtIO::endl;
      
      UtIO::cout() << "\tNucleusType: " << sNucleusTypeStr(mNucleusType) << UtIO::endl;
      
      UtIO::cout() << "\tSignature: ";
      if (mSignature)
      {
        UtString buf;
        UtIO::cout() << mSignature->compose(&buf);
      }
      else 
        UtIO::cout() << "(null)";
      UtIO::cout() << UtIO::endl;
    }

    void writeXml(UtXmlWriter*  xmlWriter) const
    {
      xmlWriter->WriteAttribute("Type", "ElabBranchData*");

      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",  "mUniqueBlockName");
      xmlWriter->WriteAttribute("Type",  "UtString");
      xmlWriter->WriteAttribute("Value", mUniqueBlockName.c_str());
      xmlWriter->EndElement();

      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",     "mDeclarationScope");
      xmlWriter->WriteAttribute("Type",     "STBranchNode*");
      xmlWriter->WriteAttribute("Pointer",  (const void*) mDeclarationScope);
      if (mDeclarationScope != NULL ) {
        UtString buf;
        mDeclarationScope->compose(&buf);
        xmlWriter->WriteAttribute("Value",buf.c_str());
      }

      xmlWriter->EndElement();

      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",  "mNucleusType");
      xmlWriter->WriteAttribute("Type",  "NucleusType");
      xmlWriter->WriteAttribute("Value", sNucleusTypeStr(mNucleusType));
      xmlWriter->EndElement();


      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",     "mSignature");
      xmlWriter->WriteAttribute("Type",     "ModuleSignature*");
      xmlWriter->WriteAttribute("Pointer",  (const void*) mSignature);
      if (mSignature) {
        UtString buf;
        mSignature->compose(&buf);
        xmlWriter->WriteAttribute("Value",buf.c_str());
      }
      xmlWriter->EndElement();
      return;

    }

    
  private:
    UtString mUniqueBlockName;
    STBranchNode* mDeclarationScope;
    NucleusType mNucleusType;
    ModuleSignature* mSignature;
  }; //ElabBranchData
  
  friend class ElabBranchData;

  class ElabBOM : public STFieldBOM
  {
  public:
    CARBONMEM_OVERRIDES

    ElabBOM() {}
    virtual ~ElabBOM() {}

    static ElabBranchData* castBOM(STBranchNode* node) {
      ElabBranchData* ret = static_cast<ElabBranchData*>(node->getBOMData());
      return ret;
    }

    static const ElabBranchData* castBOM(const STBranchNode* node) {
      // gcc 2.95.3 is brain dead. You need to explicitly cast the
      // void* to const void* before casting to ElabBranchData*.
      const void* bomdata = node->getBOMData();
      const ElabBranchData* ret = static_cast<const ElabBranchData*>(bomdata);
      return ret;
    }
    
    virtual void writeBOMSignature(ZostreamDB& out) const
    {
      out << scElabBOMSig;
    }
    
    virtual ReadStatus readBOMSignature(ZistreamDB& in, UtString* errMsg)
    {
      UtString tmpSig;
      in >> tmpSig;
      if (in.fail())
        return eReadFileError;
      
      ReadStatus stat = eReadOK;    
      if (tmpSig.compare(scElabBOMSig) != 0)
      {
        *errMsg << "Signature mismatch - expected '" << scElabBOMSig << "', got '" << tmpSig << "'";
        stat = eReadIncompatible;
      }
      return stat;
    }
    
    virtual Data allocBranchData()
    {
      return new ElabBranchData;
    }
    
    virtual Data allocLeafData()
    {
      return NULL;
    }
    
    virtual void freeBranchData(const STBranchNode*, Data* bomdata)
    {
      if (bomdata)
      {
        ElabBranchData* brData = static_cast<ElabBranchData*>(*bomdata);
        delete brData;
      }
    }
    
    virtual void freeLeafData(const STAliasedLeafNode* leaf, Data* bomdata)
    {
      if (bomdata)
        ST_ASSERT(*bomdata == NULL, leaf);
    }
    
    virtual void preFieldWrite(ZostreamDB&)
    {}
    
    virtual void writeLeafData(const STAliasedLeafNode*, ZostreamDB&) const
    {}
    
    virtual void writeBranchData(const STBranchNode*, ZostreamDB&,
                                 AtomicCache*) const
    {}
    
    virtual ReadStatus preFieldRead(ZistreamDB&)
    {
      return eReadOK;
    }
    
    virtual ReadStatus readLeafData(STAliasedLeafNode*, ZistreamDB&, MsgContext*)
    {
      return eReadIncompatible;
    }
    
    virtual ReadStatus readBranchData(STBranchNode*, ZistreamDB&, MsgContext*)
    {
      return eReadIncompatible;
    }
    
    virtual void printBranch(const STBranchNode* branch) const
    {
      UtString buf;
      branch->compose(&buf);
      UtIO::cout() << "FullName: " << buf << UtIO::endl;
      const ElabBranchData* brData = castBOM(branch);
      brData->print();
    }
    
    virtual void printLeaf(const STAliasedLeafNode* leaf) const
    {
      UtString buf;
      leaf->compose(&buf);
      UtIO::cout() << "FullName: " << buf << UtIO::endl;
    }
    //! Return BOM class name
    virtual const char* getClassName() const
    {
      return "ElabBOM";
    }
    
    //! Write the BOMData for a branch node
    virtual void xmlWriteBranchData(const STBranchNode* branch ,UtXmlWriter* xmlWriter) const
    {
      const ElabBranchData* elabData = ElabBOM::castBOM(branch);
      if(elabData == NULL) {
        return;
      }

      elabData->writeXml(xmlWriter);

    }
    
    //! Write the BOMData for a leaf node
    virtual void xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/ ,UtXmlWriter* /*writer*/ ) const
    {
      
    }

  }; //ElabBOM

  friend class ElabBOM;


public:
  CARBONMEM_OVERRIDES

  VerificPrePopInfoWalkerImpl(AtomicCache* atomicCache, bool verbose)
    : mVerbose(verbose)
  {
    mDeclarationSymTab = new STSymbolTable(&mEmptyBOM, atomicCache);
    mElabSymTab = new STSymbolTable(&mElabBOM, atomicCache);
  }

  ~VerificPrePopInfoWalkerImpl() {
    mModuleSignatures.clearPointerValues();
    delete mDeclarationSymTab;
    delete mElabSymTab;
  }


  void print() const
  {
    UtIO::cout() << "UNELABORATED SYMBOLTABLE" << UtIO::endl;
    mDeclarationSymTab->print();
    UtIO::cout() << UtIO::endl << UtIO::endl;
    UtIO::cout() << "ELABORATED SYMBOLTABLE" << UtIO::endl;
    mElabSymTab->print();
  }

  void pushDeclarationScope(StringAtom* unelab)
  {
    STBranchNode* parent = NULL;
    if (! mDeclarationBranches.empty())
      parent = mDeclarationBranches.top();
    
    STBranchNode* curUnelabScope = mDeclarationSymTab->createBranch(unelab, parent);
    mDeclarationBranches.push(curUnelabScope);
    
    if (mElabBranches.empty())
    {
      // The unelab branch stack better have been empty too.
      ST_ASSERT(parent == NULL, parent);
      
      // We are at the top level. Push the unelaborated scope as the
      // root elaborated scope
      STBranchNode* elabScope = mElabSymTab->createBranch(unelab, NULL);
      mElabBranches.push(elabScope);
      ElabBranchData* elabData = ElabBOM::castBOM(elabScope);
      elabData->putUniqueBlockName(unelab);
      elabData->putNucleusType(eModule);
    }
  }

  void popDeclarationScope()
  {
    if (! mDeclarationBranches.empty())
    {
      mDeclarationBranches.pop();
      if (mDeclarationBranches.empty())
      {
        // pop the root elaborated branch
        INFO_ASSERT(! mElabBranches.empty(), "Elaborated branch stack inconsistent with declaration branch stack.");
        mElabBranches.pop();
        ST_ASSERT(mElabBranches.empty(), mElabBranches.top());
      }
    }
  }

  STBranchNode* pushElabScope(StringAtom* scopeName, NucleusType blockType)
  {
    INFO_ASSERT(! mElabBranches.empty(), scopeName->str());
    STBranchNode* parent = mElabBranches.top();
    STBranchNode* elabScope = mElabSymTab->createBranch(scopeName, parent);
    mElabBranches.push(elabScope);

    ElabBranchData* elabData = ElabBOM::castBOM(elabScope);
    // Default the uniquename to the name of the pushed elaborated
    // scope name. If this is a block within a module, this will not
    // change, but if this is a module/entity then this will change to
    // the unique blockname
    elabData->putUniqueBlockName(scopeName);
    // Set the nucleus type of the block that is being instanced.
    elabData->putNucleusType(blockType);
    
    STBranchNode* declScope = mDeclarationBranches.top();
    elabData->putDeclarationScope(declScope);
    return elabScope;
  }

  void popElabScope()
  {
    if (! mElabBranches.empty())
      mElabBranches.pop();
  }
  
  void saveStacksAndBeginAtRoot()
  {
    mDeclarationBranches.saveStackAndBeginAtRoot();
    mElabBranches.saveStackAndBeginAtRoot();
  }

  void restoreStacks()
  {
    mDeclarationBranches.restoreStack();
    mElabBranches.restoreStack();
  }

  //! Gets the declaration scope for the elab'd instance
  STBranchNode* getInstanceDeclarationScope(STBranchNode* instance)
  {
    ElabBranchData* elabBranchData = ElabBOM::castBOM(instance);
    return elabBranchData->getDeclarationScope();
  }

  void putUniqueBlockName(STBranchNode* scope, const UtString& uniqueName)
  {
    ElabBranchData* elabBranchData = ElabBOM::castBOM(scope);
    elabBranchData->putUniqueBlockName(uniqueName);
  }
  
  STBranchNode* getElabScope()
  {
    return mElabBranches.top();
  }

  STBranchNode* getDeclarationScope()
  {
    return mDeclarationBranches.top();
  }

  void uniquify()
  {
    for (STSymbolTable::RootIter rIter = mElabSymTab->getRootIter(); 
         ! rIter.atEnd(); ++rIter)
    {
      STSymbolTableNode* symNode = *rIter;
      STBranchNode* bnode = symNode->castBranch();
      if (bnode)
      {
        ElabBranchData* elabBranchData = ElabBOM::castBOM(bnode);
        if (elabBranchData->isModule())
          calculateUnique(bnode);
      }
    }
  }

  STBranchNode* findElabScope(STBranchNode* parent, StringAtom* instAtom)
  {
    STSymbolTableNode* symNode = mElabSymTab->find(parent, instAtom);
    ST_ASSERT(symNode, parent);
    STBranchNode* scope = symNode->castBranch();
    ST_ASSERT(scope, symNode);
    return scope;
  }

  const char* getUniqueBlockName(const STBranchNode* instance) const
  {
    const ElabBranchData* bnodeData = ElabBOM::castBOM(instance);
    return bnodeData->getUniqueBlockName();
  }

  //! Generate a unique name for the given process based either on it's label
  //! or file/line number if it has no label.
  StringAtom* uniquifyProcess(Verific::VhdlTreeNode* concProc, SourceLocator* loc)
  {
      (void)concProc;
      (void)loc;
      /*
    // Does process already have a unique atom?
    UtMap<vhNode,StringAtom*>::iterator itr = mProcs.find(concProc);
    StringAtom* procName = NULL;
    if (itr == mProcs.end()) {
      // Is process labeled?
      JaguarString blockName(vhGetLabel(concProc));
      if (blockName != NULL) {
        procName = context->vhdlIntern(blockName);
      } else {
        // Construct process name based on source file and line number since
        // it is not labeled. Also attach a unique integer so that multiple
        // processes on the same line don't have same names.
        UtString procStr("$block_"); // prefix with $ to make it illegal verilog.
        UtString path, fileStr;
        OSParseFileName(loc->getFile(), &path, &fileStr);
        size_t dotPlace = fileStr.find(".");
        if (dotPlace != UtString::npos) {
          fileStr.replace(dotPlace, 1, "_");
        }
        procStr << fileStr << "_L" << loc->getLine() << ";" << mProcs.size();
        AtomicCache* atomCache = context->getStringCache();
        procName = atomCache->intern(procStr.c_str(), procStr.size());
      }
      mProcs.insert(UtMap<vhNode,StringAtom*>::value_type(concProc, procName));
    } else {
      procName = itr->second;
    }
    return procName;*/
    return 0;
  }

  StringAtom* getUniqueVhdlProcessName(Verific::VhdlTreeNode* concProc)
  {
    UtMap<Verific::VhdlTreeNode*,StringAtom*>::iterator itr = mProcs.find(concProc);
    return itr->second;
  }
  STSymbolTable* getDeclarationsSymTab() 
  {
    return mDeclarationSymTab;
  }

  STSymbolTable* getElaborationsSymTab() 
  {
    return mElabSymTab;
  }

private:
  BranchStack mDeclarationBranches;
  BranchStack mElabBranches;

  STEmptyFieldBOM mEmptyBOM;
  ElabBOM mElabBOM;
  // The unelaborated symboltable. Module names and their parent(s)
  STSymbolTable* mDeclarationSymTab;
  // The elaborated symboltable (instance path names)
  STSymbolTable* mElabSymTab;

  typedef UtHashMap<UtString, ModuleSignature*> StrModSigMap;
  // Map of module/entity names to module signatures
  StrModSigMap mModuleSignatures;
  
  typedef UtHashMap<ModuleSignature*, StringAtom*, HashPointerValue<ModuleSignature*> > ModSigToAtomMap;
  // Map of ModuleSignatures used to create a unique module name. Only
  // signatures that triggered a uniquifying event are put into this map.
  ModSigToAtomMap mDerivedNames;

  bool mVerbose;
  // Map of VHDL processes to their name atoms.
  UtMap<Verific::VhdlTreeNode*,StringAtom*> mProcs;

  // Recursive function that traverses to the end of the branch and
  // calculates unique module names as it pops back up.
  void calculateUnique(STBranchNode* bnode)
  {
    STBranchNodeIter bIter(bnode);
    STSymbolTableNode* child;
    SInt32 index;
    
    ModuleSignature* calcSignature = new ModuleSignature;
    
    // calculate children first, so we calculate bottom-up
    while ((child = bIter.next(&index)))
    {
      STBranchNode* childBranch = child->castBranch();
      if (childBranch)
      {
        ElabBranchData* elabBranchData = ElabBOM::castBOM(childBranch);
        // Do not add the tasks/functions to the signature. There
        // could be a ton of those and they don't offer any
        // uniqueness.
        if (! elabBranchData->isTaskFunction())
        {
          // RECURSIVE CALL TO CALCULATEUNIQUE
          calculateUnique(childBranch);
          
          // After calculating the uniq name for the childBranch, the
          // signature for the containing branch can now append the child.
          const char* uniqueModuleName = elabBranchData->getUniqueBlockName();
          calcSignature->append(uniqueModuleName);
        }
      }
    }
    
    // Get the current name and see if we have a non-matching signature
    ElabBranchData* bnodeData = ElabBOM::castBOM(bnode);
    UtString& curUniqName = bnodeData->getUniqueBlockNameStr();
    bnodeData->putSignature(calcSignature);

    ModuleSignature* savedSig = mModuleSignatures.insertInit(curUniqName, calcSignature);
    
    // Check if savedSig and calcSignature are the same value.
    // If savedSig and calcSignature are the same pointer then this
    // was the first occurrence of this module name. So just move
    // on. 
    if (savedSig != calcSignature)
    {
      if (*savedSig == *calcSignature)
      {
        bnodeData->putSignature(savedSig);

        // Delete the calculated signature.
        delete calcSignature;
      }
      else
      {
        // We have a signature that is different than the first
        // occurrence of this module. We may have already uniquified
        // the module with this signature. 
        StringAtom* uniqDeclScopeAtom = NULL;

        ModSigToAtomMap::iterator p = mDerivedNames.find(calcSignature);
        if (p != mDerivedNames.end())
        {
          // We already have uniquified with this signature. So, just
          // use that uniquename.
          uniqDeclScopeAtom = p->second;
          ModuleSignature* sameSig = p->first;
          bnodeData->putSignature(sameSig);

          // delete the redundant signature
          delete calcSignature;
          calcSignature = sameSig;
        }
        else
        {
          // This means we must
          // uniquify the module now, so upscopes can properly be
          // uniquified.
          
          // Up the reference count on the saved signature so we don't
          // duplicate a generated name.
          UInt32 currentCount = savedSig->incrRefCnt();
          
          // Create a declaration scope. Note, we are not filling this
          // scope with the scopes it is instancing. We only need a name
          // here for uniquification purposes. And we want to track what
          // we've created and where.
          UtString uniqDeclScopeName(curUniqName);
          uniqDeclScopeName << "_#" << currentCount;
          AtomicCache* atomCache = mDeclarationSymTab->getAtomicCache();
          STBranchNode* declScope = bnodeData->getDeclarationScope();
          uniqDeclScopeAtom = atomCache->intern(uniqDeclScopeName.c_str(), uniqDeclScopeName.size());
          mDeclarationSymTab->createBranch(uniqDeclScopeAtom, declScope);
          
          ModuleSignature* newSavedSig = mModuleSignatures.insertInit(uniqDeclScopeName, calcSignature);
          // make sure this name is unique.
          ST_ASSERT(newSavedSig == calcSignature, bnode);
        }
        
        if (mVerbose)
        {
          UtString buf;
          bnode->compose(&buf);
          UtIO::cout() << "Uniquifying block " << buf << ": " 
                       << curUniqName << " -> " << uniqDeclScopeAtom->str()
                       << UtIO::endl;
        }
        
        // Set the elaborated path to point to the new declaration
        // scope
        bnodeData->putUniqueBlockName(uniqDeclScopeAtom);
        
        
        const StringAtom* scopeNameChk = mDerivedNames.insertInit(calcSignature, uniqDeclScopeAtom);
        // make sure that we haven't saved this signature before.
        ST_ASSERT(uniqDeclScopeAtom == scopeNameChk, bnode);
        
        // write the mapping to disk
        // TBD
      }
    }
  }
};

}

#endif
