// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __VERIFIC_DUMP_HIERARCHY
#define __VERIFIC_DUMP_HIERARCHY

// Just for types, we may do forward declaration as well
#include "verific2nucleus/VerificDesignWalkerCB.h" 
#include "localflow/DesignHierarchy.h" // for XML dumper


// Project headers
#include "util/SourceLocator.h"
#include "util/UtPair.h"
#include "util/UtMap.h"
#include "util/UtStack.h"
#include "nucleus/NUStmt.h"
#include "LineFile.h"

// Forward declarations
class IODBNucleus;

// Extending Verific base interface
namespace verific2nucleus {

class VerificNucleusBuilder;
class V2NDesignScope;
class VerificDesignManager;
class VerificVhdlDesignWalker;
class VerificTicProtectedNameManager;

//! A class to dump the design hierarrchy in XML format
/*! Walks the design and dumps the instance hierarchies including
 *  the generics/parameters and ports for each instance inside the 
 *  top level instance in XML format.
 *
 *  Note that the 'instance hierarchy' includes generate labels
 *  and process labels. These are required by the modelstudio
 *  hierarchy browser to construct the full path names of
 *  signals inside processes, and underneath generates. Without
 *  these labels, the browser cannot find these signals in
 *  the Carbon database.
 */
class VerilogDumpHierarchyCB : public Verific::VerilogDesignWalkerCB {
public:
    //! constructor
    VerilogDumpHierarchyCB(UtString fileName, IODBNucleus* iodb, VerificTicProtectedNameManager* nameManager);
    virtual ~VerilogDumpHierarchyCB();
//@name Public API
public:
    void dumpXml();
    //! Adds all the files used in the design
    void addFile(UtString fileName, UtString fileType, UtString language);
    Status hdlEntity(Verific::VerilogDesignWalkerCB::VisitPhase phase, Verific::VeriModule* node, const char* entityName);
    void checkForIncludes();
    void checkForDefines(UtString fileName, File* filePtr);
    Status addSinglePort(Verific::VeriIdDef* id, Verific::VeriModule* master, Verific::VeriExpression* pc);
    Status addPorts(Verific::VeriModule* master);
    UtString gVisibleName(Verific::VeriTreeNode* ve_node, const UtString& origName);
    void getHdlEntityName(Verific::VeriModule* module, UtString* nameBuf);
    UtString getVerificName(Verific::VeriTreeNode* node);
    //! Visitors
public:
    virtual Status VERI_WALK(VeriModule, node, phase);
    virtual Status VERI_WALK(VeriModuleInstantiation, node, phase);
    virtual Status VERI_WALK(VeriGenerateBlock, node, phase);

private:
    ///@name Prevent the compiler from implementing the following
private:
    VerilogDumpHierarchyCB(VerilogDumpHierarchyCB &node);
    VerilogDumpHierarchyCB& operator=(const VerilogDumpHierarchyCB &rhs);
private:
    //! constructor
    VerilogDumpHierarchyCB();

    //! Check for `includes in the file. Only for VLOG.
    //void checkForIncludes(veNode file);
    //! Check for `define in the file. Only for VLOG.
    //void checkForDefines(veNode file, File* filePtr);

    DesignHierarchy*         mDesignHierarchy;
    IODBNucleus*             mIODB;
    VerificTicProtectedNameManager* mTicProtectedNameMgr;
    
    //TicProtectedNameManager* mTicProtectedNameMgr;
    //bool                     mInProtectedRegion;
    //VHDLCaseT                mVhdlCase;
};


} // namespace verific2nucleus 

#endif // #ifndef __VERIFIC_DUMP_HIERARCHY

