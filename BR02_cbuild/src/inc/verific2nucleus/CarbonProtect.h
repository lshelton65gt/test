// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONPROTECT_H
#define __CARBONPROTECT_H

// Project headers
#include "verific2nucleus/VerificDesignManager.h"

// Verific headers.
#include "Protect.h"
#include "LineFile.h"

// Other project headers
#include "compiler_driver/ProtectedSource.h"
#include "util/UtStringArray.h"

namespace verific2nucleus {

// Forward declarations
class VerificDesignManager;

   /*! @class CarbonProtect
    *  @brief carbon object to handle encrypt and decrypt of protected regions
    */
  class CarbonProtect : public Verific::Protect
  {
  public:
    // this implementation is currently just a stub, any use of a protected region will result in an error message
    CarbonProtect(VerificDesignManager* mgr);
    ~CarbonProtect();
    // CarbonProtect() : Protect(64) {}

    /**
     * Encrypt an input buffer in_buf and write the encrypted output
     * to output buffer out_buf. out_buf is allocated by the caller.
     * size is length in bytes of in_buf. out_buf should be at least
     * as big as in_buf. Also size MUST be a multiple of _block_size.
     */
    // TODO need to decide whether or not we need this part
    // Carbon version doing encrypt using it's own lexer, to support +protect, the part is separate from Cheetah parser, so can be reused for Verifi
     // TODO if it has some references to cheetah context may need to move it just out of it. It seems encrypt part uses it's own lexer for it
     //virtual unsigned encrypt(const char *in_buf, char *out_buf, unsigned size) ;

    /**
     * Decrypt an input buffer in_buf and write the un encrypted output
     * to output buffer out_buf. out_buf is allocated by the caller.
     * size is length in bytes of in_buf. out_buf should be at least
     * as big as in_buf. Also size MUST be a multiple of _block_size.
     * lf - starting linefile of `protected section (needed for decrypt error/warning report)
     */
    virtual unsigned decrypt(const char *in_buf, char *out_buf, unsigned size, Verific::linefile_type lf) ;
    /**
     * VIPER 6665: Same function as above but for block-by-block decryption
     * using streams. User should create a 'verific_stream' object with
     * the given encrypted text and Verific will read the decrypted text
     * block-by-block from that stream in its lexer.
     */
    //virtual verific_stream *get_decryption_stream_for_string(const char *in_buf, unsigned size) ;

    /**
     * Decrypt the data_block as set using the `protect directive of
     * the RTL using the other directives stored in _directive_map.
     * This API returns an allocated string, which will be free'd by
     * the caller using Strings::free.
     */
    //virtual char *decrypt() ;

    /**
      * Carbon specific algorith for decryption.
      */
    const char* decryptCarbon(const char* buf, Verific::linefile_type lf);

//    void setDesignManager(VerificDesignManager* mgr) { mVerificDesignMgr = mgr; }
    //virtual unsigned decrypt(const char *in_buf, char *out_buf, unsigned size);
    //virtual char* decrypt(void);
  private:
    VerificDesignManager* mVerificDesignMgr;
    bool mHaveReportedUnsupported;
    //ProtectedSource mProtectBufs;

    UtStringArray mCryptBufs;
    UtStringArray mCEMFilenames;
    UtStringArray mProtectBufs;
    UtStringArray mFilenamePatterns;
  };
}

#endif
