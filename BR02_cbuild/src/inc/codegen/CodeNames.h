// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implement legal identifier mapping support to convert Verilog names
  into legal C++ names.
*/
#ifndef __CodeNames_h_
#define __CodeNames_h_

#include "util/UtMap.h"

class AtomicCache;
class StringAtom;
class UtString;
class PerfectHash;		// Decouple gperf implementation
class RandomValGen;

//! Tranformation class to check for legal names and convert as necessary
/*!
 * Inherits C++/cbuild reserved word name lookup capabilities (the
 * hash() and is_reserved_word() methods
 */


class CodeNames
{
public:
  typedef UtMap<StringAtom*, StringAtom*> NameMap_t;    

  CodeNames (bool obfuscate, AtomicCache* cache);
  ~CodeNames();

  //! empty out the translation table
  void clear (void){ mNameTable.clear ();}

  //! Lookup name
  /*!
   * Look for the name in the map.  If a mapping exists, return it; otherwise
   * make sure that the name is a legal C++ name (can't be reserved word and
   * it can't use characters outside the identifier set [A-Za-z0-1_]
   *
   * \arg name the Net or Module name
   * \returns A printable StringAtom that is also a legal C++ identifier
   */
  StringAtom* translate(const StringAtom *name, bool suppressObfuscate = false);

  //! Lookup a name from a string
  StringAtom* translate(const UtString* name, bool suppressObfuscate = false);

  //! Dump the name table as a debugging/documentation aid
  void print (UtOStream&);

  //! Dump the name table as a sed script to a compressed encrypted file
  bool dumpTable(const char* filename, UtString* errmsg);

private:
  void genRandomIdent (UtString* buffer, UInt32 size);

  StringAtom* validate(const StringAtom* name);

  //! Uniquify with \a prefix.  \a force is used to cause truncated names to force uniquification
  StringAtom* unique (const char *prefix, bool force);
    
  //! Random generator
  RandomValGen* mRandom;

  //! counter for uniquifying generated names
  unsigned int mUuid;

  //! Handle onto symboltable
  AtomicCache*	mCache;

  //! Perfect Hash checker
  PerfectHash *mph;

  // Privately maintained map connecting StringAtoms to legal strings
  NameMap_t mNameTable;

  bool mObfuscate;
};  
#endif

/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
