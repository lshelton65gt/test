// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __Tristatebv_h_
#define __Tristatebv_h_

#ifndef __carbon_priv_h_
#include "codegen/carbon_priv.h"
#endif

#ifndef __Tristate_h_
#include "codegen/Tristate.h"
#endif

#ifndef __BitVector_h_
#include "util/BitVector.h"
#endif

// A templatized template of Tristate<T,S> which also specializes on
// the size of the vector.  This is needed to distinguish between
// BitVector Tristates and POD Tristates.
//
// Since we can't depend on free integral coercions for arithmetic
// involving BitVectors, we have to make the base type of the tristate
// compatible with BitVectors.
//
template <UInt32 _Nb> class Tristate<BitVector<_Nb>,_Nb>
  : public BitVector<_Nb>
{
protected:  
  BitVector<_Nb> mStrength;

public:
  typedef BitVector<_Nb> masktype;
  typedef BitVector<_Nb>* ptrtype;

  typedef BitVector<_Nb> StrengthT;
  typedef BitVector<_Nb> SignalT;

  //! Trivial Constructor (Carbon toplevel class will initialize data)
  //  (set drive strength high)
  Tristate (): BitVector<_Nb>() {mStrength.set ();}

  //! Less trivial constructor, strength is strongest by default
  Tristate (const BitVector<_Nb>& value) :
    BitVector<_Nb>(value)
  {
    mStrength.set();
  }

  //! Copy Constructor
  Tristate (const Tristate& v)
    : BitVector<_Nb>(v), mStrength (v.mStrength) {}

  //! Constructor by pieces
  Tristate (const BitVector<_Nb>& data, const BitVector<_Nb>& s)
    : BitVector<_Nb>(data),
      mStrength (s) {}

  //! Trivial destructor
  ~Tristate () {}

  //! Accessor for drive strength
  const BitVector<_Nb>& getStrength () const {return mStrength;}

  //! Reset strength with external drive
  void resetExternal(const BitVector<_Nb>& s) {
    mStrength = s;
    mStrength.flip();
  }
  
  //! Used by the debug_generator to get the internals of the class
  void getInternalPtrs(UInt32** data, UInt32** drive)
  {
    *data = this->getUIntArray();
    *drive = mStrength.getUIntArray();
  }
  
  void setStrength () { mStrength.set (); }

  void setStrength (size_t pos, size_t size) {
    mStrength.lpartsel (pos, size).set ();
  }

  void setStrength (const StrengthT& s) { mStrength |= s; }

  //! Set part of a drive strength using a mask AND a pos, size
  void setStrength (const StrengthT& s, UInt32 pos, UInt32 )
  {
    mStrength |= (s << pos);
  }

  //! clear strength
  void clearStrength () {
    mStrength.reset();
  }

  //! Clear part of a drive strength
  void clearStrength (const BitVector<_Nb>& s) {
    mStrength &= ~s;
  }

  //! reset strength
  void reset (const BitVector<_Nb>& s) {mStrength = s;}

  //! Overloaded
  void reset (void) {
    clearStrength();
    BitVector<_Nb>::reset ();
  }

  void resetunmasked (void) {
    this->operator&=(mStrength);	// data &= mask;
  }

  //! Invert the strength mask to convert an xdrive value
  void flip (void) {
    mStrength.flip ();
  }

  //! Overload assignment from base type T
  Tristate& operator=(const BitVector<_Nb>& value)
  {
    this->BitVector<_Nb>::operator=(value);

    // No strength manipulation for simple assignments.
    return *this;
  }

  //! Overload
  Tristate operator=(UInt64 value)
  {
    this->BitVector<_Nb>::operator=(value);
    return *this;
  }

  //! Overload assignment from one Tristate  to another.
  /*!
   * Note that an assignment does not pass strength; the lhs is driven strongly
   * even if the rhs is driven weakly.
   * If you want to keep the strength, use the copy constructor or set().
   */
  Tristate& operator=(const Tristate& value)
  {
    set(*(value.getData()));
    return *this;
  }

  //! Just update the signal with uniform strength
  void set (const BitVector<_Nb>& data) {
    this->BitVector<_Nb>::operator=(data);
    mStrength.set ();	// Force mask to all ones
  }

  //! Set one tristate based on another.  Unlike operator=, this obeys strength.
  void set (const Tristate& value)
  {
    set(value, value.getStrength());
  }

  void set (const BitVector<_Nb>& data, const StrengthT& s) {
    // Use the base vector assignment operator
    this->BitVector<_Nb>::operator &=(~s);

    BitVector<_Nb> temp(data);
    temp &= s;
    BitVector<_Nb>::operator |= (temp);
    mStrength |= s;
  }

  // Pullup initialization (internal tristate)
  void set (void) { mStrength.reset (); BitVector<_Nb>::set (); }

  // Driving a single bit
  void set (size_t pos, SInt32 val) {
    BitVector<_Nb>::set(pos, val);
  }

  // Pullup initialization (external tristate)
  void setunmasked (void) {
    for(UInt32 i=0; i < BitVector<_Nb>::Nw; ++i)
	this->_M_w[i] |= ~mStrength._M_w[i];
    this->_M_do_sanitize ();
  }

  // We don't need conversion operators to downcast to the BitVector<>.  
  // That comes for free from inheriting directly from class BitVector.

  //! simple accessor for data
  const BitVector<_Nb> * getData() const { return this; }
  BitVector<_Nb> * getData() { return this; }

  //! Return base type (work around 2.95 bug)
  BitVector<_Nb>& getT () { return *(BitVector<_Nb>*)this; }

  //! Overload
  const BitVector<_Nb>& getT () const { return *(BitVector<_Nb>*)this; }

  //! &= for data, strength not touched
  Tristate& operator &=(const BitVector<_Nb>& value)
  {
    this->BitVector<_Nb>::operator &=(value);
    return *this;
  }

  //! |= for data, strength not touched
  Tristate& operator |=(const BitVector<_Nb>& value)
  {
    this->BitVector<_Nb>::operator |=(value);
    return *this;
  }

#if 0
  void driveOut (BitVector<_Nb> xdata[3])
  {
    for(UInt32 i=0; i < BitVector<_Nb>::Nw; ++i)
      {
	xdata[eTriData]._M_w[i] &= ~mStrength._M_w[i];
	xdata[eTriIdrive]._M_w[i] = ~mStrength._M_w[i];

	xdata[eTriData]._M_w[i] |=
	  (this->BitVector<_Nb>::_M_w[i] & mStrength._M_w[i]);
      }
  }
#endif
};


// A templatized template of Tristate<T,S> which also specializes on
// the size of the vector.  This is needed to distinguish between
// BitVector Tristates and POD Tristates.
//
// Since we can't depend on free integral coercions for arithmetic
// involving BitVectors, we have to make the base type of the tristate
// compatible with BitVectors.
//
template <UInt32 _Nb> class Tristate<SBitVector<_Nb>,_Nb>
  : public SBitVector<_Nb>
{
protected:  
  SBitVector<_Nb> mStrength;

public:
  typedef SBitVector<_Nb> masktype;
  typedef SBitVector<_Nb>* ptrtype;

  typedef SBitVector<_Nb> StrengthT;
  typedef SBitVector<_Nb> SignalT;

  //! Trivial Constructor (Carbon toplevel class will initialize data)
  //  (set drive strength high)
  Tristate (): SBitVector<_Nb>() {mStrength.set ();}

  //! Less trivial constructor, strength is strongest by default
  Tristate (const SBitVector<_Nb>& value) :
    SBitVector<_Nb>(value)
  {
    mStrength.set();
  }

  //! Copy Constructor
  Tristate (const Tristate& v)
    : SBitVector<_Nb>(v), mStrength (v.mStrength) {}

  //! Constructor by pieces
  Tristate (const SBitVector<_Nb>& data, const SBitVector<_Nb>& s)
    : SBitVector<_Nb>(data),
      mStrength (s) {}

  //! Trivial destructor
  ~Tristate () {}

  //! Accessor for drive strength
  const SBitVector<_Nb>& getStrength () const {return mStrength;}

  //! Reset strength with external drive
  void resetExternal(const SBitVector<_Nb>& s) {
    mStrength = s;
    mStrength.flip();
  }
  
  //! Used by the debug_generator to get the internals of the class
  void getInternalPtrs(UInt32** data, UInt32** drive)
  {
    *data = this->getUIntArray();
    *drive = mStrength.getUIntArray();
  }
  
  void setStrength () { mStrength.set (); }

  void setStrength (size_t pos, size_t size) {
    mStrength.lpartsel (pos, size).set ();
  }

  void setStrength (const StrengthT& s) { mStrength |= s; }

  //! Set part of a drive strength using a mask AND a pos, size
  void setStrength (const StrengthT& s, UInt32 pos, UInt32 )
  {
    mStrength |= (s << pos);
  }

  //! clear strength
  void clearStrength () {
    mStrength.reset();
  }

  //! Clear part of a drive strength
  void clearStrength (const SBitVector<_Nb>& s) {
    mStrength &= ~s;
  }

  //! reset strength
  void reset (const SBitVector<_Nb>& s) {mStrength = s;}

  //! Overloaded
  void reset (void) {
    clearStrength();
    SBitVector<_Nb>::reset ();
  }

  void resetunmasked (void) {
    this->operator&=(mStrength);	// data &= mask;
  }

  //! Invert the strength mask to convert an xdrive value
  void flip (void) {
    mStrength.flip ();
  }

  //! Overload assignment from base type T
  Tristate& operator=(const SBitVector<_Nb>& value)
  {
    this->SBitVector<_Nb>::operator=(value);

    // No strength manipulation for simple assignments.
    return *this;
  }

  //! Overload
  Tristate operator=(UInt64 value)
  {
    this->SBitVector<_Nb>::operator=(value);
    return *this;
  }

  //! Overload assignment from one Tristate  to another.
  /*!
   * Note that an assignment does not pass strength; the lhs is driven strongly
   * even if the rhs is driven weakly.
   * If you want to keep the strength, use the copy constructor or set().
   */
  Tristate& operator=(const Tristate& value)
  {
    set(*(value.getData()));
    return *this;
  }

  //! Just update the signal with uniform strength
  void set (const SBitVector<_Nb>& data) {
    this->SBitVector<_Nb>::operator=(data);
    mStrength.set ();	// Force mask to all ones
  }

  //! Set one tristate based on another.  Unlike operator=, this obeys strength.
  void set (const Tristate& value)
  {
    set(value, value.getStrength());
  }

  void set (const SBitVector<_Nb>& data, const StrengthT& s) {
    // Use the base vector assignment operator
    this->SBitVector<_Nb>::operator &=(~s);

    SBitVector<_Nb> temp(data);
    temp &= s;
    SBitVector<_Nb>::operator |= (temp);
    mStrength |= s;
  }

  // Pullup initialization (internal tristate)
  void set (void) { mStrength.reset (); SBitVector<_Nb>::set (); }

  // Driving a single bit
  void set (size_t pos, SInt32 val) {
    SBitVector<_Nb>::set(pos, val);
  }

  // Pullup initialization (external tristate)
  void setunmasked (void) {
    for(UInt32 i=0; i < SBitVector<_Nb>::Nw; ++i)
	this->_M_w[i] |= ~mStrength._M_w[i];
    this->_M_do_sanitize ();
  }

  // We don't need conversion operators to downcast to the SBitVector<>.  
  // That comes for free from inheriting directly from class SBitVector.

  //! simple accessor for data
  const SBitVector<_Nb> * getData() const { return this; }
  SBitVector<_Nb> * getData() { return this; }

  //! Return base type (work around 2.95 bug)
  SBitVector<_Nb>& getT () { return *(SBitVector<_Nb>*)this; }

  //! Overload
  const SBitVector<_Nb>& getT () const { return *(SBitVector<_Nb>*)this; }

  //! &= for data, strength not touched
  Tristate& operator &=(const SBitVector<_Nb>& value)
  {
    this->SBitVector<_Nb>::operator &=(value);
    return *this;
  }

  //! |= for data, strength not touched
  Tristate& operator |=(const SBitVector<_Nb>& value)
  {
    this->SBitVector<_Nb>::operator |=(value);
    return *this;
  }


#if 0
  void driveOut (SBitVector<_Nb> xdata[3])
  {
    for(UInt32 i=0; i < SBitVector<_Nb>::Nw; ++i)
      {
	xdata[eTriData]._M_w[i] &= ~mStrength._M_w[i];
	xdata[eTriIdrive]._M_w[i] = ~mStrength._M_w[i];

	xdata[eTriData]._M_w[i] |=
	  (this->SBitVector<_Nb>::_M_w[i] & mStrength._M_w[i]);
      }
  }
#endif
};

template <UInt32 _Nb> class Bidirect<BitVector<_Nb>, _Nb>
: public Tristate<BitVector<_Nb>, _Nb>
{
public:
  typedef BitVector<_Nb> Tdata;
  typedef BitVector<_Nb> Tmask;
protected:
  Tmask mXDrive;
  Tdata mXData;
  
public:
  typedef BitVector<_Nb> masktype;
  typedef BitVector<_Nb>* ptrtype;

  //! Trivial Constructor (Carbon toplevel class will initialize data)
  Bidirect () : Tristate<BitVector<_Nb>, _Nb>(), mXDrive (0), mXData (0) {
    mXDrive.set();
  }

  //! Copy Constructor
  Bidirect (const Bidirect& v) : Tristate<BitVector<_Nb>, _Nb>(v),
				 mXDrive (v.mXDrive), mXData (v.mXData) {}

  //! Constructor by pieces
  Bidirect (const Tdata& data, const Tmask& s, const Tmask& x=0, const Tdata& d=0)
    : Tristate<BitVector<_Nb>,_Nb>(data, s), mXDrive (x), mXData (d) {}

  //! Less trivial constructor
  Bidirect (const Tdata& value) : Tristate<BitVector<_Nb>,_Nb>(value),
				    mXDrive (0), mXData (0){
    mXDrive.set();
  }

  //! Trivial destructor
  ~Bidirect () {}

  //! Access the saved external drive mask
  Tmask& getXDrive () const { return (Tmask&)mXDrive; }

  //! Access the saved external data
  /*! Note that the shell does a read-modify write on the data so this
   *  includes the deposited and current value for the data.
   */
  Tmask& getXData () const { return (Tmask&)mXData; }

  //! Used by structapi ??
  void resetExternal(const Tmask& s) {
    mXDrive = s;
  }

  void setXDrive (const Tmask& xd) { mXDrive = xd; }
  void setXData (const Tmask& xd) { mXData = xd; }

  //! Used by the debug_generator to get the internals of the class
  void getInternalPtrs(UInt32** idata, UInt32** idrive, UInt32** xdata, UInt32** xdrive)
  {
    Tristate<BitVector<_Nb>,_Nb>::getInternalPtrs(idata, idrive);
    *xdata = mXData.getUIntArray();
    *xdrive = mXDrive.getUIntArray();
  }

  // BUG1880 - was missing Bidirect<T,S>::operator=(const T&) for T=bitvector.
  //
  Bidirect& operator=(const Tdata& value) {
    this->Tristate<BitVector<_Nb>, _Nb>::operator=(value);
    return *this;
  }

  //! Assignment from simple integer (BUG7002, missing operator= for BitVector overloads)
  Bidirect& operator=(const SInt64 value)
  {
    this->Tristate<BitVector<_Nb>, _Nb>::operator=(value);
    return *this;
  }
};

template <UInt32 _Nb> class Bidirect<SBitVector<_Nb>, _Nb>
: public Tristate<SBitVector<_Nb>, _Nb>
{
public:
  typedef SBitVector<_Nb> Tdata;
  typedef SBitVector<_Nb> Tmask;
protected:
  Tmask mXDrive;
  Tdata mXData;
  
public:
  typedef SBitVector<_Nb> masktype;
  typedef SBitVector<_Nb>* ptrtype;

  //! Trivial Constructor (Carbon toplevel class will initialize data)
  Bidirect () : Tristate<SBitVector<_Nb>, _Nb>(), mXDrive (0), mXData (0) {
    mXDrive.set();
  }

  //! Copy Constructor
  Bidirect (const Bidirect& v) : Tristate<SBitVector<_Nb>, _Nb>(v),
				 mXDrive (v.mXDrive), mXData (v.mXData) {}

  //! Constructor by pieces
  Bidirect (const Tdata& data, const Tmask& s, const Tmask& x=0, const Tdata& d=0)
    : Tristate<SBitVector<_Nb>,_Nb>(data, s), mXDrive (x), mXData (d) {}

  //! Less trivial constructor
  Bidirect (const Tdata& value) : Tristate<SBitVector<_Nb>,_Nb>(value),
				    mXDrive (0), mXData (0){
    mXDrive.set();
  }

  //! Trivial destructor
  ~Bidirect () {}

  //! Access the saved external drive mask
  Tmask& getXDrive () const { return (Tmask&)mXDrive; }

  //! Access the saved external data
  /*! Note that the shell does a read-modify write on the data so this
   *  includes the deposited and current value for the data.
   */
  Tmask& getXData () const { return (Tmask&)mXData; }

  //! Used by structapi ??
  void resetExternal(const Tmask& s) {
    mXDrive = s;
  }

  void setXDrive (const Tmask& xd) { mXDrive = xd; }
  void setXData (const Tmask& xd) { mXData = xd; }

  //! Used by the debug_generator to get the internals of the class
  void getInternalPtrs(UInt32** idata, UInt32** idrive, UInt32** xdata, UInt32** xdrive)
  {
    Tristate<SBitVector<_Nb>,_Nb>::getInternalPtrs(idata, idrive);
    *xdata = mXData.getUIntArray();
    *xdrive = mXDrive.getUIntArray();
  }

  // BUG1880 - was missing Bidirect<T,S>::operator=(const T&) for T=bitvector.
  //
  Bidirect& operator=(const Tdata& value) {
    this->Tristate<SBitVector<_Nb>, _Nb>::operator=(value);
    return *this;
  }
  //! Assignment from simple integer (BUG7002, missing operator= for BitVector overloads)
  Bidirect& operator=(const SInt64 value) {
    this->Tristate<SBitVector<_Nb>, _Nb>::operator=(value);
    return *this;
  }
};

#endif

/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
