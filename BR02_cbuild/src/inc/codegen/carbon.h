// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
/*
  Support macros/functions for C++ or C programs.
*/

#ifndef __carbon_h_
#define __carbon_h_

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif

#endif
