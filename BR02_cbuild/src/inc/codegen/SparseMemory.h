// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/* Template class to support memories */
/*!
  \file

  This class is a template class that uses a hash table to implement
  sparse memories.
*/

#ifndef __SparseMemory_h_
#define __SparseMemory_h_

#ifndef __carbon_priv_h_
#include "codegen/carbon_priv.h"
#endif

#ifndef MULTI_THREAD_SUPPORT
#ifndef __MemManager_h_
#include "util/MemManager.h"
#endif
#endif

#ifndef __BitVector_h_
#include "util/BitVector.h"
#endif

#ifndef _DYN_TEMP_MEMORY_H_
#include "codegen/DynTempMemory.h"
#endif

#ifndef _STATIC_TEMP_MEMORY_H_
#include "codegen/StaticTempMemory.h"
#endif

//#define DEBUG_SPARSE
#ifdef DEBUG_SPARSE
#include <stdio.h>
#endif

#ifdef MEMORYPERF
#ifndef __MemoryPerf_h_
#include "util/MemoryPerf.h"
#endif
#define MEMC(N,w,d)  MemPC->count##N(w,d);
#else
#define MEMC(N,w,d)
#endif

#ifndef __MemoryAccess_h_
#include "codegen/MemoryAccess.h"
#endif

#ifndef _CARBON_TYPE_CREATE_H_
#include "shell/carbon_type_create.h"
#endif

// Memory callbacks are needed for both replay and onDemand
#if defined(CARBON_REPLAY) || defined(CARBON_ONDEMAND)
#define CARBON_MEM_WRITE_CB
#endif

#ifdef CARBON_SAVE_RESTORE
template <class _MemType, int _Width, int _MSW, int _LSW> class SparseMemory;

template <class _MemType, int _Width, int _MSW, int _LSW>
CARBON_SECTION("carbon_checkpoint_code")
void writeToStream(UtOCheckpointStream* f, const SparseMemory<_MemType,_Width,_MSW,_LSW>&mem);

template <class _MemType, int _Width, int _MSW, int _LSW>
CARBON_SECTION("carbon_checkpoint_code")
void readFromStream(UtICheckpointStream* f, SparseMemory<_MemType, _Width, _MSW, _LSW>& mem);

#endif //CARBON_SAVE_RESTORE

template <int _NumPorts, class _MemType, int _Width, int _MSW, int _LSW>
class SparseStateUpdateMemory;
template <class _MemType, int _Width, int _MSW, int _LSW>
class SparseDynStateUpdateMemory;

//! SparseMemory template class
/*!
 * This template class implements a memory object. It allows reading
 * and writing the memory from the Carbon Model or from the Shell
 * API.
 */
template <class _MemType, int _Width, SInt32 _MSW, SInt32 _LSW>
class SparseMemory : MemoryAccess<_MemType, ((_Width > 64) ? eMemBitVector
  					  : (_Width > 32) ? eMemPODLong : eMemPOD) >
{
 public:
  enum {
    eBitsPerWord = (8 * sizeof(UInt32)),
    eWordWidth = (_Width + eBitsPerWord - 1) / eBitsPerWord,
    eMemUnitSize = eWordWidth * sizeof(UInt32),
    eMemUpperBound = (_MSW >= _LSW) ? _MSW : _LSW,
    eMemLowerBound = (_MSW >= _LSW) ? _LSW : _MSW
  };

 private:
  static inline UInt32 ComputeDepth (SInt32 msw, SInt32 lsw) {
    UInt32 width;
    if (msw > lsw)
      width = msw - lsw;
    else
      width = lsw - msw;
    width += 1;
    return width;
  }

  typedef UtHashMapFastIter< UInt32, _MemType > MemoryData;
  typedef typename MemoryData::MapEntry MapEntry;

#ifdef CARBON_SAVE_RESTORE

  friend void writeToStream<_MemType, _Width, _MSW, _LSW>(UtOCheckpointStream*, const SparseMemory< _MemType, _Width, _MSW, _LSW>&);
  friend void readFromStream<_MemType, _Width, _MSW, _LSW>(UtICheckpointStream*, SparseMemory< _MemType, _Width, _MSW, _LSW>&);

#endif

  _MemType& writeAccess(SInt32 i) {
    if (EXPECTED_VALUE(inRange(i), 1)) {
      _MemType &retval = mMemory.insertInit(i, mZeroBucket);
      return retval;
    }
    else {
      REPORT_MEM_OOB ("write", "", _MSW, _LSW, i);
      return *static_cast<_MemType*>( (void*)&gCarbonMemoryBitbucket);
    }
  }

#ifdef CARBON_MEM_WRITE_CB
  void reportWriteAddress(SInt32 address) {
    carbonInterfaceMemoryCBManagerReportAddress(mCallbacks, address);
  }
#else
  void reportWriteAddress(SInt32) {}
#endif

public:
  SparseMemory() : mZeroBucket(0)
    {
      MEMC(Constructed,_Width,ComputeDepth(_MSW,_LSW));
#ifdef DEBUG_SPARSE
      printf("SparseMem constructor: this = %p, &mZeroBucket = %p\n",
             this, &mZeroBucket);
#endif
#ifdef CARBON_MEM_WRITE_CB
      mCallbacks = carbonInterfaceAllocMemoryCBManager();
#else
      mCallbacks = NULL;
#endif
    }
  
  ~SparseMemory()
    {
#ifdef CARBON_MEM_WRITE_CB
      carbonInterfaceFreeMemoryCBManager(mCallbacks);
#endif
    }

  // The following get functions are used by the shell
  void* getHashTable() { return &mMemory; }
  carbonHashFn getHashFn() { return MemoryData::sHashFn; }
  carbonHashEqFn getHashEqFn() { return MemoryData::sEqFn; }
  
  ShellMemoryCBManager* getCallbackArray() {
    return mCallbacks;
  }


  typedef _MemType value_type;
  typedef typename MemoryData::UnsortedCLoop UnsortedCLoop;

  //! address accessor
  /*!
    The shell uses this directly. Hence it is public.
  */
  const _MemType& readAccess(SInt32 i) const {
    const MapEntry* me = mMemory.findEntry(i);
    if (EXPECTED_VALUE (me == NULL, 0)) // Assume we only read what we wrote
    {
      // cache miss
      if (EXPECTED_VALUE(not inRange(i), 0)) {
        (void)0;                // prevent empty block without oob reporting
        REPORT_MEM_OOB ("read", "",  _MSW, _LSW, i);
      }

      // Whether it's in-range or not, just read zeros
      return mZeroBucket;
    }
    return me->mKeyVal.second;
  }

  //! Function to get the absolute depth of the memory
  inline UInt32 getDepth() const { return ComputeDepth(_MSW,_LSW); }

  //! Function to get the msw of the memory
  inline SInt32 getMSW() const { return _MSW; }

  //! Function to get the lsw of the memory
  inline SInt32 getLSW() const { return _LSW; }

  SInt32 denormalize (SInt32 i) const {
    SInt32 index;
    if (_MSW >= _LSW)
      index = i + _LSW;
    else
      index = getDepth () + (_MSW - 1) - i;
    return index;
  }

  SInt32 normalize (SInt32 i) const {
    SInt32 index;
    if (_MSW >= _LSW)
      index = (i - _LSW);
    else
      index = getDepth () - 1 - (i - _MSW);
    return index;
  }

  //! Write access to a memory location
  _MemType& write(SInt32 i) { 
    MEMC(Writes,_Width,ComputeDepth(_MSW,_LSW));
    reportWriteAddress(i);
#ifdef DEBUG_SPARSE
    printf("write: mem[%x] has address %p\n", i, &writeAccess(i));
#endif
    return writeAccess (i); 
  }

  //! For now, we are not implementing the "no-check" versions of accessors, since sparse memories are really slow anyways.
  _MemType& writeNoCheck(SInt32 i) {
    return write(i);
  }

  //! Overload read access
  const _MemType& read(SInt32 i) const { 
    MEMC(Reads,_Width,ComputeDepth(_MSW,_LSW));
#ifdef DEBUG_SPARSE
    printf("read:  mem[%x] has address %p\n", i, &readAccess(i));
#endif
    return readAccess (i); 
  }

  //! For now, we are not implementing the "no-check" versions of accessors, since sparse memories are really slow anyways.
  const _MemType& readNoCheck(SInt32 i) const {
    return read(i);
  }

  void writeValue(SInt32 addr, const _MemType& val) {
    if (EXPECTED_VALUE(inRange(addr),1)) {
      reportWriteAddress(addr);
      mMemory.insertValue(addr, val);
    }
    else {
      REPORT_MEM_OOB ("write", "", _MSW, _LSW, addr);
    }
  }

  void writeValueNoCheck(SInt32 addr, const _MemType& val) {
    reportWriteAddress(addr);
    mMemory.insertValue(addr, val);
  }

  //! Function to explicitly write a location in memory
  void directWrite(SInt32 addr, const _MemType& val)
  {
    MEMC(Writes,_Width,ComputeDepth(_MSW,_LSW));
    writeValue(addr, val);
  }

  //! write a row of memory
  void writeLocation(SInt32 address, const UInt32* data)
  {
    MEMC(Writes,_Width,ComputeDepth(_MSW,_LSW));
#ifdef DEBUG_SPARSE
    for (UInt32 i = 0; i < eWordWidth; ++i)
      printf("writeLoc: mem[%x][%d] <= %x\n", address, i, data[i]);
#endif
    writeRow(write(address), data);
  }

  //! read a row of memory
  void readLocation(SInt32 address, UInt32* data) const
  {
    MEMC(Reads,_Width,ComputeDepth(_MSW,_LSW));
    readRow(read(address), data);
    
#ifdef DEBUG_SPARSE
    for (UInt32 i = 0; i < eWordWidth; ++i)
      printf("readLoc: mem[%x][%d] => %x\n", address, i, data[i]);
#endif
  }

  static void sMemWriteRow(SInt32 address, const UInt32* data, void* mem)
    {
      SparseMemory<_MemType, _Width, _MSW, _LSW>* me = (SparseMemory<_MemType, _Width, _MSW, _LSW>*) mem;
      // use writeAccess not writeNoCheck, so we don't call the
      // memory callback in the shell.
      _MemType& loc = me->writeAccess(address);
      me->writeRow(loc, data);
    }

  static void sMemWriteRowWord(SInt32 address, const UInt32* data, int index, void* mem)
    {
      SparseMemory<_MemType, _Width, _MSW, _LSW>* me = (SparseMemory<_MemType, _Width, _MSW, _LSW>*) mem;
      // use writeAccess not writeNoCheck, so we don't call the
      // memory callback in the shell.
      _MemType& loc = me->writeAccess(address);
      me->writeRowWord(loc, data, index);
    }

  static void sMemWriteRowRange(SInt32 address, const UInt32* data, int index, int length, void* mem)
    {
      SparseMemory<_MemType, _Width, _MSW, _LSW>* me = (SparseMemory<_MemType, _Width, _MSW, _LSW>*) mem;
      // use writeAccess not writeNoCheck, so we don't call the
      // memory callback in the shell.
      _MemType& loc = me->writeAccess(address);
      me->writeRowRange(loc, data, index, length);
    }

  static void sMemReadRow(SInt32 address, UInt32* data, void* mem)
    {
      SparseMemory<_MemType, _Width, _MSW, _LSW>* me = (SparseMemory<_MemType, _Width, _MSW, _LSW>*) mem;
      const _MemType& loc = me->readNoCheck(address);
      me->readRow(loc, data);
    }

  static void sMemReadRowWord(SInt32 address, UInt32* data, int index, void* mem)
    {
      SparseMemory<_MemType, _Width, _MSW, _LSW>* me = (SparseMemory<_MemType, _Width, _MSW, _LSW>*) mem;
      const _MemType& loc = me->readNoCheck(address);
      me->readRowWord(loc, data, index);
    }

  static void sMemReadRowRange(SInt32 address, UInt32* data, int index, int length, void* mem)
    {
      SparseMemory<_MemType, _Width, _MSW, _LSW>* me = (SparseMemory<_MemType, _Width, _MSW, _LSW>*) mem;
      const _MemType& loc = me->readNoCheck(address);
      me->readRowRange(loc, data, index, length);
    }
  
  
  CarbonMemWriteRowFn getWriteRowFn() {
    return sMemWriteRow;
  }

  CarbonMemReadRowFn getReadRowFn() {
    return sMemReadRow;
  }

  CarbonMemWriteRowWordFn getWriteRowWordFn() {
    return sMemWriteRowWord;
  }

  CarbonMemReadRowWordFn getReadRowWordFn() {
    return sMemReadRowWord;
  }

  CarbonMemWriteRowRangeFn getWriteRowRangeFn() {
    return sMemWriteRowRange;
  }

  CarbonMemReadRowRangeFn getReadRowRangeFn() {
    return sMemReadRowRange;
  }

  CarbonMemRowStoragePtrFn getRowStoragePtrFn() {
    // This is only meaningful for non-sparse memories, in which all
    // the storage is allocated at creation.
    return NULL;
  }
  
  //! Function to initialize the memory from a readmemX file.
  bool readmem(HDLReadMemX* readmemx)
  {
    if (!carbonInterfaceReadMemOpenFile(readmemx))
      return false;

    SInt64 address;
    UInt32 data[eWordWidth];
    while (carbonInterfaceReadMemGetNextWord(readmemx, &address, data))
      writeLocation((SInt32)address, data);

    carbonInterfaceReadMemCloseFile(readmemx);

    // Return value is not used by generated model
    return true;
  }

  //! Assignment operator from a temp-memory, to update master
  template<int _NumPorts>
  inline SparseMemory& operator=(const StaticTempMemory<_NumPorts, SparseMemory>& src) {
    const SparseMemory* master = src.getBaseMemory ();
    if (this == master)
      src.executeQueuedWrites(false);
    else {
      clear ();
      slowCopy (src);
    }
    return *this;
  }

  typedef DynTempMemory<SparseMemory> TempMem;

  // Dyntemp memory is easier
  inline SparseMemory& operator=(const TempMem& src) {
    const SparseMemory* master = src.getBaseMemory ();
    if (this == master) {
      src.executeQueuedWrites(false);
    }
    else {
      // Update existing entries in target from source and master
      clear ();
      slowCopy (src);
    }
    return *this;
  }

  // Handle sparse dyntemp memories
  void slowCopy (const TempMem& src) {
    for (UnsortedCLoop p(src.getBaseMemory ()->mMemory); !p.atEnd (); ++p) {
      UInt32 srcaddr = p.getKey();
      UInt32 destaddr = denormalize( src.normalize( srcaddr ));
      writeNoCheck (destaddr) = src.readNoCheck (srcaddr);
    }
  }

  // Handle multiported static sparse memories.
  template <int NumPorts>
  void slowCopy (const StaticTempMemory<NumPorts, SparseMemory>& src) {
    for (UnsortedCLoop p(src.getBaseMemory ()->mMemory); !p.atEnd (); ++p) {
      UInt32 srcaddr = p.getKey();
      UInt32 destaddr = denormalize( src.normalize( srcaddr ));
      writeNoCheck (destaddr) = src.readNoCheck (srcaddr);
    }
  }

  //! Clears the entire memory, essentially setting to 0
  void clear() {
#ifdef CARBON_MEM_WRITE_CB
    // Report the addresses that will be written
    for (UnsortedCLoop p(mMemory); !p.atEnd (); ++p) {
      reportWriteAddress(p.getKey());
    }
#endif
    mMemory.clear();
  }

  //! template to allow memory of different ranges.
  template <class _SparseMem>
  SparseMemory& operator=(const _SparseMem& src) {
    clear ();        // Wipe out existing data

    // And replicate into new memory
    for (UnsortedCLoop p(src.mMemory); !p.atEnd (); ++p) {
      SInt32 addr = p.getKey ();
      addr = src.normalize(addr);
      addr = denormalize(addr);
      writeNoCheck(addr) = p.getValue();
    }

    return *this;
  }

  SparseMemory& operator=(const SparseMemory& src) {
    clear ();        // Wipe out existing data

    // And replicate into new memory
    for (UnsortedCLoop p(src.mMemory); !p.atEnd (); ++p) {
      writeNoCheck(p.getKey()) = p.getValue();
    }

    return *this;
  }

  //! Memory equalities...
  template <class _SparseMem>
  bool operator==(const _SparseMem& src) const {

    if (getStorageSize() != src.getStorageSize())
      return false;

    if (getDepth() != src.getDepth())
      return false;

    // There was previously a very subtle bug (bug5782).  You can't
    // compare two hash-maps by using two parallel unsorted loops.  If
    // one hash-map has grown to have a different number of buckets
    // than the other, they will not iterate in the same order.  We
    // can use two sorted loops, but that's pretty slow.  Better to
    // loop over one and do lookups in the other.
    //
    // Also, be careful about addresses, which have to be normalized
    // if the memories do not have the same dimensions
    //
    // Here's my attempt to fix this harder-than-you-expect routine.
    // We need to allow explicit stores of the default zero value in the
    // hash-tables, because codegen makes it difficult for us to
    // filter them out, since we just return a writable location.
    //
    // So we can either slog through the entire memory range, or we
    // can go through both memories hash-tables as needed.  I'm insane so I'm
    // going to try the latter.
    //
    // Note that we do not have to worry about comparing a Memory to
    // a SparseMemory because we choose sparseness by size, and cbuild
    // will fold comparisons of unlike sized memories to constant 0
    // (says Tim, 3/20/06).
    UInt32 qsize = mMemory.size();
    for (UnsortedCLoop p(src.mMemory); !p.atEnd(); ++p) {
      SInt32 addr = p.getKey();
      addr = src.normalize(addr);
      addr = denormalize(addr);
      const MapEntry* me = mMemory.findEntry(addr);
      if (me != NULL) {
        if (me->mKeyVal.second != p.getValue()) {
          return false;
        }
        --qsize;
      }
      else if (p.getValue() != mZeroBucket) {
        return false;
      }
    }

    // Having gotten through all of src.mMemory, we need to make
    // sure we've hit all the entries in mMemory as well.
    for (UnsortedCLoop q(mMemory); (qsize != 0) && !q.atEnd(); ++q) {
      SInt32 addr = q.getKey();
      addr = normalize(addr);
      addr = src.denormalize(addr);
      const MapEntry* me = src.mMemory.findEntry(addr);
      if (me == NULL) {
        // We are looking for entries that are *not* in src.mMemory
        // to make sure they are 0.
        if (q.getValue() != mZeroBucket) {
          return false;
        }
        --qsize;
      }
    }
    return true;
  } // bool operator==

  //! Memory inequality
  template <class _Mem>
    bool operator!=(const _Mem& src) const {
    return !(*this == src);
  }

  // comparison between a Memory and an associated dynamic temp memory
  template<class _Mem> bool operator==(const DynTempMemory<_Mem> & src) const {
    const _Mem* other = src.getBaseMemory ();

    if (this != other) {
      // comparing a temp against some memory that's not its parent.
      if (getStorageSize () != other->getStorageSize ()
          || getDepth () != other->getDepth ())
        return false;

      // compare ALL the members
      for (UnsortedCLoop p(mMemory); !p.atEnd (); ++p)
        if (src.read (p.getKey ()) != p.getValue ())
          return false;

      return true;
    }

    // If we had an iterator over the temp memory, we could just compare
    // the elements actually in the temp

    for (UnsortedCLoop p(mMemory); !p.atEnd (); ++p)
      if (src.read (p.getKey ()) != p.getValue ())
        return false;

    return true;
  } // template<class _Mem> bool operator==

  template<class _Mem>
    bool operator!=(const DynTempMemory<_Mem>& src) const {
    return !(*this == src);
  }

  // comparison between a Memory and an associated static temp memory
  template <int NumPorts>
    bool operator==(const StaticTempMemory<NumPorts, SparseMemory>& src) const {
   
    const SparseMemory* other = src.getBaseMemory ();

    if (this != other) {
      // comparing a temp against some memory that's not its parent.
      if (getStorageSize () != other->getStorageSize ()
          || getDepth () != other->getDepth ())
        return false;

      // compare ALL the members
      for (UnsortedCLoop p(mMemory); !p.atEnd (); ++p) {
        if (src.read (p.getKey ()) != p.getValue ())
          return false;
      }

      return true;
    }

    // Since this temp is comparing against its master, we only need to look
    // at the temps.  But since we don't have an iterator over the temp memory
    // classes, we can't do that yet.
    // compare ALL the members
    for (UnsortedCLoop p(mMemory); !p.atEnd (); ++p) {
      if (src.read (p.getKey ()) != p.getValue ())
        return false;
    }

    return true;
  }

  template<int NumPorts>
    bool operator!=(const StaticTempMemory<NumPorts, SparseMemory>& src) const {
    return !(*this == src);
  }


  //! The memory data, stored in a UtHashMap
  MemoryData mMemory;

  //! Get the size of the memory storage
  UInt32 getStorageSize() const
  {
    return (UInt64) ComputeDepth(_MSW,_LSW) * sizeof(_MemType);
  }
private:
  //! Hide copy constructors
  SparseMemory(const SparseMemory&);


  // A function to test if an address is in range
  bool inRange(SInt32 address) const
  {
    if (CARBON_NO_OOB)
      return true;
    else if (_MSW > _LSW)
      return ((address >= _LSW) && (address <= _MSW));
    else
      return ((address >= _MSW) && (address <= _LSW));
  }

  ShellMemoryCBManager*  mCallbacks;

  //! Used for in range but unwritten reads and for initializing newly
  //  created writable locations
  const _MemType mZeroBucket;

}; // class SparseMemory




#ifdef CARBON_SAVE_RESTORE
//! Save the state of a SparseMemory by writing out the number of
//elements in the hash table, <n> address/data pairs, and the memory bit
//bucket value.
template <class _MemType, int _Width, int _MSW, int _LSW>
CARBON_SECTION("carbon_checkpoint_code")
void writeToStream(UtOCheckpointStream* f, const SparseMemory<_MemType,_Width,_MSW,_LSW>&mem)
{

  int memSize = mem.mMemory.size();
  carbonInterfaceOCheckpointStreamWrite(f, reinterpret_cast<const char*>(&memSize), sizeof(int));
  typedef typename SparseMemory<_MemType, _Width, _MSW, _LSW>::UnsortedCLoop MyUnsortedLoop;
  MyUnsortedLoop p;
  for ( p = (mem.mMemory.loopCUnsorted()); !p.atEnd(); ++p ) {
    // Emit address/data pairs into the output stream
    carbonInterfaceOCheckpointStreamWrite(f, reinterpret_cast<const char*>(&p.getKey()), sizeof(p.getKey()));
    carbonInterfaceOCheckpointStreamWrite(f, reinterpret_cast<const char*>(&p.getValue()), sizeof(p.getValue()));
  }

}

//! Restore the state of a SparseMemory by reading in the number of
//elements in the hash table, <n> address/data pairs, and the memory bit
//bucket value.
template <class _MemType, int _Width, int _MSW, int _LSW>
CARBON_SECTION("carbon_checkpoint_code")
void readFromStream(UtICheckpointStream* f, SparseMemory<_MemType, _Width, _MSW, _LSW>& mem)
{
  int tableSize;
  carbonInterfaceICheckpointStreamRead(f, reinterpret_cast<char*>(&tableSize), sizeof(int));

  mem.clear();

  UInt32 address;
  _MemType data;
  for ( int i = 0; i < tableSize; i++ ) {
    // Read address/data pairs from the input stream
    carbonInterfaceICheckpointStreamRead(f, reinterpret_cast<char*>(&address), sizeof(address));
    carbonInterfaceICheckpointStreamRead(f, reinterpret_cast<char*>(&data), sizeof(data));
    mem.write( address ) = data;
  }

}
#endif //CARBON_SAVE_RESTORE


#ifdef CARBON_SAVE_RESTORE
template <int _NumPorts, class _MemType, int _Width, int _MSW, int _LSW> class SparseStateUpdateMemory;

template <int _NumPorts, class _MemType, int _Width, int _MSW, int _LSW>
CARBON_SECTION("carbon_checkpoint_code")
void writeToStream(UtOCheckpointStream* f, const SparseStateUpdateMemory<_NumPorts, _MemType, _Width, _MSW, _LSW>&mem);

template <int _NumPorts, class _MemType, int _Width, int _MSW, int _LSW>
CARBON_SECTION("carbon_checkpoint_code")
void readFromStream(UtICheckpointStream* f, SparseStateUpdateMemory<_NumPorts, _MemType, _Width, _MSW, _LSW>&mem);
#endif //CARBON_SAVE_RESTORE


//! SparseStateUpdateMemory template class
/*!
 * This template class is derived from the SparseMemory template class and
 * creates a variant on the memory object. This variant fixes cycles
 * between sequential memory writes and reads by double buffering the
 * writes.
 */
template <int _NumPorts, class _MemType, int _Width, int _MSW, int _LSW>
class SparseStateUpdateMemory : public SparseMemory<_MemType, _Width, _MSW, _LSW>
{
public:
  typedef _MemType value_type;
  typedef SparseMemory<_MemType, _Width, _MSW, _LSW> _Base;
  typedef StaticTempMemory<_NumPorts, _Base> TempMemory;

#ifdef CARBON_SAVE_RESTORE
  friend void writeToStream< _NumPorts, _MemType, _Width, _MSW, _LSW >(UtOCheckpointStream*,
    const SparseStateUpdateMemory< _NumPorts, _MemType, _Width, _MSW, _LSW >&);
  friend void readFromStream<_NumPorts, _MemType, _Width, _MSW, _LSW >(UtICheckpointStream*,
    SparseStateUpdateMemory< _NumPorts, _MemType, _Width, _MSW, _LSW >&);
#endif

  //! constructor
  SparseStateUpdateMemory() : _Base() {
    mTemp.putBaseMemory (this);
    mLocalTemp.putBaseMemory (this);
  }

  //! destructor
  ~SparseStateUpdateMemory() {}

  //! Access the underlying memory without the double-buffering.  Called in initial blocks
  _Base& base () { return static_cast<_Base&>(*this); }

  struct reference
  {
    typedef typename MASKTYPE (value_type) masktype;

    reference(SparseStateUpdateMemory* mem, SInt32 addr)
    {
      mMem = mem;
      mAddr = addr;
    }

    value_type& operator=(const value_type& val)
    {
      mMem->mLocalTemp.write(mAddr) = val;
      return (value_type&)val;
    }

    value_type& operator=(const reference& ref) 
    {
      return operator=((value_type&)(ref));
    }

    operator value_type&() {
      _Base* mem = mMem;
      return mem->write(mAddr);
    }

    // access the underlying object
    value_type& ref (void) {
      return mMem->mLocalTemp.write (mAddr);
    }

    SparseStateUpdateMemory* mMem;
    SInt32 mAddr;
  };
  friend struct reference;

  //! Get a memory location for access, this is used for writing to the memory
  reference write(SInt32 addr)
  {
    return reference(this, addr);
  }

  //! For now, we are not implementing the "no-check" versions of accessors, since sparse memories are really slow anyways.
  reference writeNoCheck(SInt32 addr)
  {
    return reference (this, addr);
  }

  // Read a memory location
  const _MemType& read(SInt32 addr) const
  {
    return mLocalTemp.read(addr);
  }

  //! For now, we are not implementing the "no-check" versions of accessors, since sparse memories are really slow anyways.
  const _MemType& readNoCheck(SInt32 addr) const
  {
    return read(addr);
  }

  SparseStateUpdateMemory& operator=(const _Base & src) {
    for (typename _Base::UnsortedCLoop i(src.mMemory); !i.atEnd (); ++i) {
      this->write (i.getKey()) = src.read (i.getKey());
    }
    return *this;
  }

  SparseStateUpdateMemory& operator=(const StaticTempMemory<_NumPorts, SparseStateUpdateMemory> & src) {
    const SparseStateUpdateMemory* master = src.getBaseMemory ();
    if (this == master)
      src.executeQueuedWrites(false);
    else {
      this->clear ();
      slowCopy (src);
    }
    return *this;
  }

  SparseStateUpdateMemory& operator=(const SparseStateUpdateMemory & src) {
    if (this == &src)
      return *this;

    this->clear ();
    for (typename SparseStateUpdateMemory::UnsortedCLoop i(src.mMemory);
        !i.atEnd (); ++i)
    {
      this->write (i.getKey()) = src.read (i.getKey());
    }
    return *this;
  }

  SparseStateUpdateMemory& operator=(const SparseDynStateUpdateMemory<_MemType, _Width, _MSW, _LSW> & src) {
    for(typename SparseDynStateUpdateMemory<_MemType, _Width, _MSW, _LSW>::UnsortedCLoop i(src.mMemory);
        !i.atEnd (); ++i)
    {
      this->write (i.getKey()) = src.read (i.getKey());
    }
    return *this;
  }

  //! Sync the local temp memory to the global temp memory
  void localSync()
  {
    mTemp.copyQueuedWrites(mLocalTemp);
  }

  //! Synchronize the memory
  void sync()
  {
    mTemp.executeQueuedWrites(true);
  }

private:
  //! Hide copy and assign constructors
  SparseStateUpdateMemory(const SparseStateUpdateMemory&);

  // If you change anything in here, check codegen/emitUtil.cxx
  TempMemory mTemp;
  TempMemory mLocalTemp;
}; // class SparseStateUpdateMemory

#ifdef CARBON_SAVE_RESTORE
template <int _NumPorts, class _MemType, int _Width, int _MSW, int _LSW>
CARBON_SECTION("carbon_checkpoint_code")
void writeToStream(UtOCheckpointStream* f, const SparseStateUpdateMemory<_NumPorts, _MemType, _Width, _MSW, _LSW>&mem)
{
  writeToStream(f, *static_cast< const SparseMemory< _MemType, _Width, _MSW, _LSW >* >(&mem));
}

template <int _NumPorts, class _MemType, int _Width, int _MSW, int _LSW>
CARBON_SECTION("carbon_checkpoint_code")
void readFromStream(UtICheckpointStream* f, SparseStateUpdateMemory<_NumPorts, _MemType, _Width, _MSW, _LSW>&mem)
{
  readFromStream(f, *static_cast< SparseMemory<_MemType, _Width, _MSW, _LSW>*>(&mem));
}
#endif //CARBON_SAVE_RESTORE

//! SparseDynStateUpdateMemory template class
/*!
 * This template class is derived from the SparseMemory template class and
 * creates a variant on the memory object. This variant fixes cycles
 * between sequential memory writes and reads by double buffering the
 * writes.
 */
template <class _MemType, int _Width, int _MSW, int _LSW>
class SparseDynStateUpdateMemory : public SparseMemory<_MemType, _Width, _MSW, _LSW>
{
public:
  typedef _MemType value_type;
  typedef SparseMemory<_MemType, _Width, _MSW, _LSW> _Base;
  typedef DynTempMemory<_Base> TempMemory;

  //! constructor
  SparseDynStateUpdateMemory()
  {
    mTemp.putBaseMemory (this);
    mLocalTemp.putBaseMemory (this);
  }

  //! destructor
  ~SparseDynStateUpdateMemory() {}

  //! Access the underlying memory without the double-buffering
  _Base& base () { return static_cast<_Base&>(*this); }

  struct reference
  {
    typedef typename MASKTYPE (value_type) masktype;

    reference(SparseDynStateUpdateMemory* mem, SInt32 addr)
    {
      mMem = mem;
      mAddr = addr;
    }

    // Performance issue for bit-vectors...but I can't get
    // all our code to compile with const value_type&.
    value_type& operator=(const value_type& val)
    {
      mMem->mLocalTemp.write(mAddr) = val;
      return (value_type&)val;
    }

    value_type& operator=(const reference& ref) 
    {
      return operator=((value_type&)(ref));
    }

    operator value_type&() {
      _Base* mem = mMem;
      return mem->write(mAddr);
    }

    // access the underlying object
    value_type& ref (void) {
      return mMem->mLocalTemp.write (mAddr);
    }

    SparseDynStateUpdateMemory* mMem;
    SInt32 mAddr;
  };

#if pfGCC_2
  friend struct reference;
#endif

  //! Get a memory location for access, this is used for writing to the memory
  reference write(SInt32 addr)
  {
    return reference(this, addr);
  }

  // Read a memory location
  const _MemType& read(SInt32 addr) const
  {
    return mLocalTemp.read(addr);
  }

  //! For now, we are not implementing the "no-check" versions of accessors, since sparse memories are really slow anyways.
  const _MemType& readNoCheck(SInt32 addr) const
  {
    return read(addr);
  }

  reference writeNoCheck(SInt32 addr)
  {
    return write(addr);
  }

  SparseDynStateUpdateMemory& operator=(const _Base & src) {
    for(typename _Base::UnsortedCLoop i(src.mMemory); !i.atEnd (); ++i) {
      this->write (i.getKey()) = src.read (i.getKey());
    }
    return *this;
  }

  // Handle sparse dyntemp memories
  void slowCopy (const DynTempMemory<SparseDynStateUpdateMemory>& src) {
    for (typename _Base::UnsortedCLoop p(src.getBaseMemory ()->mMemory);
         !p.atEnd (); ++p)
    {
      writeNoCheck (p.getKey ()) = src.readNoCheck (p.getKey ());
    }
  }

  SparseDynStateUpdateMemory& operator=(const DynTempMemory<SparseDynStateUpdateMemory> & src) {
    const SparseDynStateUpdateMemory* master = src.getBaseMemory ();
    if (this == master)
      src.executeQueuedWrites(false);
    else {
      *this = *master;
      slowCopy (src);
    }
    
    return *this;
  }

  template <int _NumPorts>
  SparseDynStateUpdateMemory& operator=(const SparseStateUpdateMemory<_NumPorts, _MemType, _Width, _MSW, _LSW> & src) {
    // iterate thru members of the underlying sparse memory
    for(typename _Base::UnsortedCLoop i(static_cast<const _Base&>(src).mMemory);
        !i.atEnd (); ++i)
    {
      // but read thru the buffers of the state-update cache
      this->write (i.getKey()) = src.read (i.getKey());
    }
    return *this;
  }

  SparseDynStateUpdateMemory& operator=(const SparseDynStateUpdateMemory & src) {
    for(typename SparseDynStateUpdateMemory::UnsortedCLoop i(src.mMemory);
        !i.atEnd (); ++i)
    {
      this->write (i.getKey()) = src.read (i.getKey());
    }
    return *this;
  }

  //! Sync the local temp memory to the global temp memory
  void localSync()
  {
    mTemp.copyQueuedWrites(mLocalTemp);
  }

  //! Synchronize the memory
  void sync()
  {
    mTemp.executeQueuedWrites(true);
  }

private:
  //! Hide copy and assign constructors
  SparseDynStateUpdateMemory(const SparseDynStateUpdateMemory&);

  // If you change anything in here, check codegen/emitNet.cxx
  TempMemory mTemp;
  TempMemory mLocalTemp;
}; // class SparseDynStateUpdateMemory

#undef DEBUG_SPARSE
#endif // __CARBON_SPARSEMEMORY_H__
