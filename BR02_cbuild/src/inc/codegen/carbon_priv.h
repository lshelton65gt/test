// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
/*
  Support macros/functions for Carbon C++ runtime environment.
*/

#ifndef __carbon_priv_h_
#define __carbon_priv_h_

#ifdef __cplusplus
#ifndef __CARBON_SYS_INCLUDE_H__
#include "codegen/SysIncludes.h"
#endif
#endif

#ifndef __CarbonPlatform_h_
#include "util/CarbonPlatform.h"
#endif

#ifndef __carbon_shelltypes_h_
#include "shell/carbon_shelltypes.h"
#endif

#ifdef __cplusplus
#ifndef __MemManager_h_
#include "util/MemManager.h"
#endif
#endif

#ifndef __CarbonAssert_h_
#include "util/CarbonAssert.h"
#endif

#ifndef __CarbonRunTime_h_
#include "util/CarbonRunTime.h"
#endif

#if !defined(CARBON_NO_OOB)
#define CARBON_NO_OOB 0
#endif

#if pfGCC
/* GCC syntax to suppress unused variable warnings. */
#define UNUSED __attribute__((unused))
#if pfGCC_2
#define ALWAYS_INLINE
#define NOINLINE
#else
#define ALWAYS_INLINE __attribute__((always_inline))
#define NOINLINE __attribute__((noinline))
#endif
#else
#define UNUSED
#define ALWAYS_INLINE
#define NOINLINE
#endif

#if pfGCC_3 || pfGCC_4
/* GCC syntax to inform compiler of expected value of an expression (ie for branch prediction). */
#define EXPECTED_VALUE(expr,val) __builtin_expect((expr),(val))
#else
#define EXPECTED_VALUE(expr,val) (expr)
#endif

// Tag a piece of data as behaving like Fortran COMMON.  Used only by memory BITBUCKETs
// Require 8 byte alignment so that we can handle memories of UInt64 on Sun.
//
#if pfGCC
#define COMMON __attribute__ ((common, aligned(8)))
#else
#define COMMON
#endif

#if (pfGCC || pfICC) && (!pfWINDOWS)
/* GCC syntax to mark a method as belonging to a named section. */
#define CARBON_SECTION(section_name) __attribute__((section(section_name)))
#else
#define CARBON_SECTION(section_name)
#endif

#if CARBON_CHECK_OOB
#define REPORT_OOB(msg,o,hi,lo,i) carbon_oob_access(msg,o, ConstantRange(hi,lo), i)
#else
#define REPORT_OOB(msg,o,hi,lo,i)
#endif

#ifndef __STRING
//! Stringify expression
#define __STRING(exp) #exp
#endif

// template magic to get a usable integral type for a POD or structure
template <typename T> struct Masktype { typedef typename T::masktype masktype; };

// All reference types are handled by the "immediate" type
template<typename T> struct Masktype<T&> { typedef typename T::masktype masktype; };

// Define specialized overloads for other types.
#define MT(x,y) template<> struct Masktype<x>{typedef y masktype;}
MT (bool, UInt32);
MT (SInt8,UInt32);
MT (SInt16,UInt32);
MT (SInt32,UInt32);
MT (SInt64,UInt64);

MT (UInt8,UInt32);
MT (UInt16,UInt32);
MT (UInt32,UInt32);
MT (UInt64,UInt64);

MT (const bool, UInt32);
MT (const UInt8,UInt32);
MT (const SInt8,UInt32);
MT (const UInt16,UInt32);
MT (const SInt16,UInt32);
MT (const UInt32,UInt32);
MT (const SInt32,UInt32);
MT (const UInt64,UInt64);
MT (const SInt64,UInt64);

#undef MT

#define MASKTYPE(x) Masktype<x>::masktype

// Given a type return the same-sized signed type
template <typename T> struct SignedType { typedef typename T::signtype signtype; };
#define ST(x,y) template<> struct SignedType<x>{ typedef y signtype; }
ST (bool, SInt8);
ST (SInt8,SInt8);
ST (SInt16,SInt16);
ST (SInt32,SInt32);
ST (SInt64,SInt64);

ST (UInt8,SInt8);
ST (UInt16,SInt16);
ST (UInt32,SInt32);
ST (UInt64,SInt64);

ST (const bool, SInt8);
ST (const UInt8,SInt8);
ST (const SInt8,SInt8);
ST (const UInt16,SInt16);
ST (const SInt16,SInt16);
ST (const UInt32,SInt32);
ST (const SInt32,SInt32);
ST (const UInt64,SInt64);
ST (const SInt64,SInt64);
#undef ST


// GCC3.4.3 and GCC3.4.4 have a bug [14950] where templated functions can't be
// marked as always inlined.
#if pfGCC_3 && (pfGCC_3_4_3 || pfGCC_3_4_4 || pfGCC_3_4_5 || pfGCC_3_4_6)
#define TEMPLATE_ALWAYS_INLINE
#else
#define TEMPLATE_ALWAYS_INLINE ALWAYS_INLINE
#endif

// templated function to return the width of the primitive masktype
// associated with T

template <typename T> inline UInt32 TYPEWIDTH () TEMPLATE_ALWAYS_INLINE;
template <typename T>
inline UInt32 TYPEWIDTH () { return sizeof(typename MASKTYPE (T)) * 8; }

// Expand to either a UInt32 or UInt64 mask.
//
// There is a bug in GCC3.4.3 (scheduled to be fixed in 3.4.4 and already fixed in
// gcc 4.0) that rejects the ALWAYS_INLINE on template-d functions.
//

template <typename T> inline T carbon_mask (UInt32 w) TEMPLATE_ALWAYS_INLINE;
template <typename T> inline T carbon_mask (UInt32 w) {
  if (w >= int(sizeof(T) * 8))
    return T (~0);
  else
    return ~((~T (0)) << w);
}

#define MASK(TYP,W) carbon_mask<TYP>(W)

#define GET_SLICE(var,hi,lo) WGET_SLICE((var), (lo), (hi)+1-(lo))
#define GET_SLICE_CHECK(var,hi,lo) WGET_SLICE_CHECK((var),(lo), (hi)+1-(lo))

#define GET_BIT(var,bitnum) WGET_SLICE((var),(bitnum),1)

#define WGET_SLICE(VAR,LOW,WID) \
        (((VAR)>>(LOW)) & MASK(MASKTYPE(typeof(VAR)),WID))
#define WGET_SLICE_CHECK(var,low,wid) \
        carbon_wget_slice_check(var, low, wid)

#define GET_SLICE_DIRTY (VAR,HI,LO)  ((VAR) >> (LO))

#define WGET_SLICE_DIRTY(VAR,LO,WID) \
        ((VAR) >> (LO))

#define GET_BIT_DIRTY(var,bitnum) WGET_SLICE_DIRTY((var),(bitnum),1)

// Have to defend against SET_SLICE(v,3,1, (GET_SLICE(v,2,0)))!
// For debug check that val has no dirty bits.

#if pfGCC_4_2
template <typename DT, typename VT> inline void WUNCHECKED_SET_SLICE (DT& var, UInt32 pos, UInt32 width,
                                                                        VT val)
{
  typedef typename MASKTYPE (DT) PodType;
  // Declare a mask of the base-type of this object.
  const PodType  mask (MASK (PodType, width));

#if CARBON_CHECK_OOB
  if ((val & ~mask) != 0)
    REPORT_OOB ("dirty bits stored", __STRING (var), pos + width -1 , pos, val);
#endif
  var = (var & ~(mask << pos)) | (PodType (val) << pos);
}

#else
#define WUNCHECKED_SET_SLICE(VAR,LO,WID,VAL) \
   (VAR) = ((VAR) & (~(MASK (MASKTYPE (typeof(VAR)), WID) << (LO)))) | ((VAL) << (LO))
#endif

#define WUNCHECKED_SET_SLICE_SFT(VAR,LO,WID,SFT,VAL) \
   (VAR) = ((VAR) & (~(MASK (MASKTYPE (typeof(VAR)), WID) << (LO)))) | ((VAL) << ((LO)-(SFT)))

#define UNCHECKED_SET_SLICE(var, hi, lo, val) \
        WUNCHECKED_SET_SLICE((var), (lo), 1+(hi)-(lo), (val))

#define WSET_SLICE(v,l,w,val)  WUNCHECKED_SET_SLICE((v),(l),(w),(val))

#define WSET_SLICE_CHECK(v,vwid,l,w,val) \
        carbon_wset_slice_check<typeof(v),typeof(val),vwid>(v,l,w,val)

#define WSET_SLICE_SHIFT(v,l,w,s,val)  WUNCHECKED_SET_SLICE_SFT((v),(l),(w),(s),(val))

#define WSET_BIT(var,bit,val) WSET_SLICE((var),(bit),1,(val))
#define WSET_BIT_CHECK(var,bit,val) CSET_BIT(sizeof(typeof(var))*8,(var),(bit),(val))

#define WCSET_BIT(lim,var,bit,val) \
	do { if ((lim) > (bit)) SET_BIT((var),(bit),(val)); } while (0)

#define WSET_MASKED(var,lo,width,val)      \
        (var) = MASK(MASKTYPE(typeof(var)), (width)) & (val)

#define SET_SLICE(v,h,l,val) UNCHECKED_SET_SLICE((v),(h),(l),(val))

#if pfGCC_4_2
#define SET_BIT(var,bit,val) carbon_set_bit((var), (bit), (val))
template <typename DT, typename VT> inline void carbon_set_bit (DT &var, UInt32 bit, VT val)
{
  typedef typename MASKTYPE (DT) PodType;
  const PodType mask (MASK (PodType,1));

  var = (var & ~(mask << bit)) | (PodType(val&1) << bit);
}
#else
#define SET_BIT(var,bit,val) WSET_SLICE ((var),(bit),1,(val))
#endif


#define SET_BIT_CHECK(var,bit,val) CSET_BIT( sizeof(typeof(var))*8, var, bit, val)

#define CSET_BIT(lim,var,bit,val) \
	do { if (UInt32(lim) > UInt32(bit)) SET_BIT((var),(bit),(val)); } while (0)

#define SET_MASKED(var,hi,lo,val)      \
	(var) = MASK(MASKTYPE(typeof(var)),(hi) + 1 - (lo)) & (val)

//! Sign-extend a value of Itype (viz. UInt9) to a resulting precision Otype (viz SInt20)
/*!
 * Given a POD value of \a ITYPE and precision \a ISIZE, convert it to \a OTYPE of
 * assumed precision \a OSIZE.
 *
 * Example:  Extender<SInt12, 12, SInt4, 4>(someBits)
 *
 * Takes 4 bits, treats bit3 as the sign, extends it to 12 bits (the physical size of SInt12).
 *
 * Normally, you would call Extender<> with a physical size like SInt16, but emitConcat does
 * sign-extensions to arbitrary size when implementing sign-extending concats.
 */
template <typename Otype, typename Itype, unsigned int Osize, unsigned int Isize>
class Extender
{
private:
  Extender ();
  Extender& operator=(const Extender&);
public:
#if pfICC_9 || pfGCC_4_1_0 || pfGCC_4_2 || 1
  // icc 9.0 whines about signed 1 bit types, so use shifting.
  // gcc4.x doesn't extend signed packed types correctly.
  //
  // We now actually get better code with this (even compiling
  // with 3.4.3) than with the #else-branch code.  Ideally, I'd like
  // to use Otype for the shifting not SInt64, but that doesn't seem to
  // simulate correctly on some designs.
  enum {signSize = (sizeof(SInt64)*8) - Isize};
  Otype m_value;
  Extender (Itype value) : m_value ((SInt64 (value) << signSize) >> signSize){}
#else
  typedef typename SignedType<Itype>::signtype signtype;
  signtype m_value : Isize;
  Extender (Itype value): m_value (value){}
#endif

  Extender (const Extender& src) : m_value (src.m_value) {}

  // Give me back the object, extended to its new size
  Otype operator ()() const {
    if (Osize == (sizeof(Otype)*8))
      return static_cast<Otype>(m_value);
    else
      return static_cast<Otype>(m_value) & MASK (Otype, Osize);
  }
};

// Inline versions of the popcount and reduction xor functions
/*
 * The inlined versions works with gcc 4.1 and 4.2.
 */

//! Count the 1's in a 32 bit value
UInt32 carbon_popcount(UInt32 x) ALWAYS_INLINE;
inline
UInt32 carbon_popcount(UInt32 x)
{
  x -= ((x >> 1) & 0x55555555);
  x = (((x >> 2) & 0x33333333) + (x & 0x33333333));
  x = (((x >> 4) + x) & 0x0f0f0f0f);
  x += (x >> 8);
  x += (x >> 16);
  return (x & 0x000003F);
}

UInt32 carbon_popcount (UInt64 x) ALWAYS_INLINE;
inline
UInt32 carbon_popcount (UInt64 x)
{
  return carbon_popcount (UInt32 (x>>32))
    + carbon_popcount (UInt32 (x&0xffffffff));
}

//! Parity for a 32 bit value
UInt32 carbon_redxor(UInt32 x) ALWAYS_INLINE;
inline
UInt32 carbon_redxor(UInt32 x)
{
  /*
   * Could do sequence like
   * __asm__("shld %2,%1,16\n"   // %2 = %1 << 16
   *         "xor %R1,%R2\n"     // 16 bits
   *         "xor %q1,%Q1\n"     // 8 bits, PF set on result
   *         "setpo %q0" : "=g"(result) : "0"(x), "&r"()); return result;
   */
  x ^= (x >> 16);
  x ^= (x >> 8);
  x ^= (x >> 4);
  x &= 0xf;
  return ((0x6996 >> x) & 0x1);
}

//! Parity for a 64 bit value
UInt32 carbon_redxord(UInt64 x) ALWAYS_INLINE;
inline
UInt32  carbon_redxord(UInt64 x)
{
  UInt32 tx = ((UInt32) x) ^ ((UInt32) (x>>32));
  return carbon_redxor(tx);
}

// Template function for checkin if the data is negative
template <typename T> bool negcheck (T /* x */ ) { return false; }
template <> inline bool negcheck<SInt8>(SInt8 x) {return (x<0); }
template <> inline bool negcheck<SInt16>(SInt16 x) {return (x<0); }
template <> inline bool negcheck<SInt32>(SInt32 x) {return (x<0); }
template <> inline bool negcheck<SInt64>(SInt64 x) {return (x<0); }

template <typename T1, typename T2>
inline T1
carbon_vhmod(T1 x, T2 y, UInt32 xsiz)
{
  const UInt32 tWidth = sizeof(T1)*8;

  if ( !y )
  {
    return T1 (0);
  }
  if ((T1(1) << (xsiz -1)) & x)
  {
    x = ((xsiz == tWidth ) ? -x : -(((~T1 (0)) << xsiz ) | x ));
    if (negcheck(y))
    {
      return ( - (x%(-y)));
    }
    else if ( x%(y) )
    {
      return (y - (x%y));
    }
    else
    {
      return (x%y);
    }
  }
  else if ( negcheck(y) && (x%(-y)) )
  {
    return ((x%(-y)) - (-y));
  }
  else
  {
    return (x%y);
  }
}

template <typename T>
inline T
carbon_abs(T x, UInt32 siz)
{
  if ((T(1) << (siz-1)) & x)
    return -x;
  else
    return x;
}

template <typename T>
inline T
carbon_ror(T data, SInt32 count, SInt32 size)
{
  count %= size;
  if (count == 0)
    return data;
  if (count < 0)
    return (((data << -count) | ((data >> (size + count)) & MASK (T,(-count)))) & 
           MASK (UInt64, size) );
  else
    return ((((data >> count) & MASK (T, size-count)) | 
            (data << (size - count))) & MASK (UInt64, size) );
}

template <typename T>
inline T
carbon_rol(T data, SInt32 count, SInt32 size)
{
  count %= size;
  if (count == 0)
    return data;
  if (count < 0)
    return ((((data >> -count) & MASK (T, size+count)) | 
             (data << (size + count))) & MASK (UInt64, size) );
  else
    return (((data << count) | ((data >> (size - count)) & MASK (T, count))) & 
           MASK (UInt64, size) );
}


template <typename T>
inline T
carbon_srl(T data, SInt32 count, SInt32 size)
{
  const SInt32 tWidth = sizeof(T)*8;

  if (0 == count)
  {
    return data;
  }
  else if (count < 0) //Negative count, so do shift left logical(,SLL) operation
  {
    if (-count > size)
      count = -size;
    return ( ( (size == -count) ? T (0) : ( data << (-count) ) ) &
             ( (size == tWidth) ? ~(T (0)) : MASK (T, size) ) );
  }
  else
  {
    if (count > size)
      count = size;
    return ( ( (count == size) ? T (0) : ( ( data >> count ) & 
               MASK (T, size-count) ) ) & ( (size == tWidth) ? 
               ~(T (0)) : MASK (T, size) ) );
  }
}

template <typename T>
inline T carbon_sll(T data, SInt32 count, SInt32 size)
{
  const SInt32 tWidth = sizeof(T)*8;
  if (0 == count)
  {
    return data;
  }
  else if (count < 0) // Negative count, so do shift right logical(, SRL) 
  {
    if (-count > size)
      count = -size;
    return ( ( (-count == size) ? T (0) : ( ( data >> (-count) ) & 
               MASK (T, size+count) ) ) & ( (size == tWidth) ? 
               ~(T (0)) : MASK (T, size) ) );
  }
  else
  {
    if ( count > size )
      count = size;
    return ( ( (count == size) ? T (0) : ( data << count ) ) & 
             ( (size == tWidth) ? ~(T (0)) : MASK (T, size) ) );
  }
}

///////////////////////////////////////////////////////////////////////////////
//
// FUNCTION(s): carbon_sra():
// OPERATOR(VHDL): SRA, Shift Right Arith
//
// The functions carbon_srad() and carbon_sra() (, defined later in this file )
// have been implemented for the VHDL operator "SRA" . The function
// carbon_sra() will be called when the width of the LHS of the "SRA" operator
// is less or equal to 32 bit. Similarly carbon_srad() will be called if the
// LHS width is less or equal to 64 bit. These two functions implement the
// following behavior which is the definition of the VHDL SRA operation.
//
// Example:
// ========
// signal  data, output : bit_vector(7 downto 0);
// signal count : integer range -7 to 7;
// 
// output <= data SRA count;
//
// If count is negative then the equivalent RHS will be "data SLA abs(count)".
// Below is the equivalent VHDL of the above assignment when count is positive:
//
// output(7 downto 7 - count + 1) <= (others => data(7));
// output(7 - count downto 0) <= data(7 downto count);
//
// Example relationship between RHS vs LHS of the above assignment:
//
// data        count   output
// ========    =====   =======
// 01011111     3      00001011
// 11011111     3      11111011
// 01011111     8      00000000
// 11011011     8      11111111
///////////////////////////////////////////////////////////////////////////////


// Function to accomplish arithmetic right shift operation
template <typename T>
inline T
carbon_sra(T data, SInt32 count, SInt32 size)
{
  const SInt32 tWidth = sizeof(T)*8;
  if (0 == count)
  {
    return data;
  }
  else if (count < 0) // Negative count, so do shift left arith(, SLA) operation
  {
    if (-count > size)
      count = -size;
    return ( ( (size == -count) ? ( (T (1) & data) ? ~(T (0)) : 
             T (0) ) : ( ( (data << (-count)) | ( (T (1) & data) ? 
             MASK (T, -count) : T (0) ) ) ) ) & ( (size == tWidth) ? 
             ~(T (0)) : MASK (T, size) )) ;
  }
  else
  {
    if (count > size)
      count = size;
    return ( ( (size == count) ? 
             ( (T (1) & T( data >> (size - 1) ) ) ? 
             ~(T (0)) : T (0) ) : ( ( ( data >> count) | 
             ( ( T (1) & ( data >> (size - 1))) ? 
             ( ((~T (0)) << (size - count)) ) : 
             T (0) ) ) ) ) & ( (size == tWidth) ? ~(T (0)) : MASK (T,size) )) ;
  }
}

///////////////////////////////////////////////////////////////////////////////
//
// FUNCTION(s): carbon_sla():
// OPERATOR(VHDL): SLA, Shift Left Arith
//
// The functions carbon_slad() and carbon_sla() (, defined later in this file )
// have been implemented for the VHDL operator "SLA" . The function
// carbon_sla() will be called when the width of the LHS of the "SLA" operator
// is less or equal to 32 bit. Similarly carbon_slad() will be called if the
// LHS width is less or equal to 64 bit. These two functions implement the
// following behavior which is the definition of the VHDL SLA operation.
//
// Example:
// ========
// signal  data, output : bit_vector(7 downto 0);
// signal count : integer range -7 to 7;
// 
// output <= data SLA count;
//
// If count is negative then the equivalent RHS will be "data SRA abs(count)".
// Below is the equivalent VHDL of the above assignment when count is positive:
//
// output(7 downto count) <= data(7 - count downto 0);
// output(count - 1 downto 0) <= (others => data(0));
//
// Example relationship between RHS vs LHS of the above assignment:
//
// data        count   output
// ========    =====   =======
// 01010101     3      10101111
// 11011010     3      11010000
// 01011111     8      11111111
// 11011010     8      00000000
///////////////////////////////////////////////////////////////////////////////

// Function to accomplish arithmetic left shift operation on 32 bit data
template <typename T>
inline T
carbon_sla(T data, SInt32 count, SInt32 size)
{
  if (0 == count)
  {
    return data;
  }
  else if (count < 0) // Negative count, so do shift right arith(,SRA) operation
  {
    if (-count > size)
      count = -size;
    return ( ( (size == -count) ? ( ( 1 & ( data >> (size - 1) ) ) ? 
             ~(T (0)) : T (0) ) : ( ( ( data >> (-count) ) | 
             ( ( T (1) & ( data >> (size - 1) ) ) ? 
             ( (~(T(0)) << ( size - (-count) ) ) ) : T (0))) ) ) & 
             MASK (T, size) );
  }
  else
  {
    if (count > size)
      count = size;
    return ( ( (size == count) ? ( (1 & data) ? ~(T (0)) : T (0) ) : 
             ( ( (data << count) | ( (T (1) & data) ? MASK (T, count) : 
             T (0) ) ) ) ) & MASK (T, size) );
  }
}

// Function to extend input data by padding the extended bits signs or
// zeros This DOESN'T handle VHDL's resize() function which retains
// the source signbit even when truncating the result.
//
// Extend \a type T \a data to \a newSize bits as signed or unsigned, indicated
// by RT and T.  Returns type \a RT.
//
// var i : std_logic_vector(0 to 6);
// SXT(i,10)                            => carbon_vhext<SInt16, UInt7>(i,10)
// EXT(i,10)                            => carbon_vhext<UInt16, UInt7>(i,10)
// 
template <typename RT, typename T>
inline RT
carbon_vhext(T data, SInt32 newSize)
{
  if (0 >= newSize) {           // Worst case, return 0
    return RT (0);
  }

  RT result = data;             // Extend or truncate data according to the source type
  
  const SInt32 rtWidth = sizeof(RT)*8;

  if (newSize >= rtWidth)
    return result;                // Already as wide as we can be, so we're done

  if (RT (1) > RT (~0)) {
    // signed type - e.g. eBiVhExt
    // extend from bit newsize
    SInt32 excessBits = (rtWidth - newSize);
    result <<= excessBits;
    result >>= excessBits;      // extend from newSize-1
  } else {
    // unsigned type - e.g. eBiVhZxt, truncate to newSize bits.
    result &= MASK (RT, newSize);
  }
  return result;
}

// Function guaranteed to return nonzero result, 
template <typename T> inline const T nonzero (const T val) {
  return val ? val : T (1);
} 

// Function to do change detect and update a temp old value
template <typename T> inline bool changeDetect(T& tempNet, T expr)
{
  return (expr == tempNet) ? false : ((tempNet = expr) ,  true);
}

inline int carbon_FFO(UInt32 x)
{
  if (!x)
    return -1;

  int r;
#if pfX86

  __asm__ ("bsf %1,%0" : "=r" (r) : "rm"(x)); // finds lowest set bit 
#else
  // create mask up to least-significant 1 and count those bits
  r = carbon_popcount ((x & -((SInt32)x)) - 1);
#endif

  return r;
}

inline int carbon_FLO (UInt32 x)
{
  if (!x) return -1;

#if pfX86
  int r;
  __asm__ ("bsr %1,%0" : "=r" (r) : "rm"(x)); // finds most-significant 1

#else
  // Make dense sequence of ones below MSB
  x |= (x>>1);
  x |= (x>>2);
  x |= (x>>4);
  x |= (x>>8);
  x |= (x>>16);
  int r = carbon_popcount (x) - 1;
#endif

  return r;
}

inline int carbon_DFFO (UInt64 x)
{
  if (x==0)
    return -1;

#if pfX86_64
  SInt64 r;
  __asm__ ("bsf %1,%0" : "=r" (r) : "rm"(x));
#else
  int r = carbon_FFO (UInt32 (x));
  if (r >= 0)
    return r;

  r = carbon_FFO ((UInt32) (x >> 32)) + 32;
#endif
  return r;
}

inline int carbon_DFLO (UInt64 x)
{
  if (x == 0)
    return -1;

#if pfX86_64
  SInt64 r;
  __asm__ ("bsr %1,%0" : "=r"(r) : "rm"(x));
#else
  int r = carbon_FLO ((UInt32)(x >> 32));
  if (r >= 0)
    return r + 32;

  r = carbon_FLO (UInt32 (x));
#endif

  return r;
}


inline int carbon_FFZ (UInt32 x, UInt32 size=32)
{
  return carbon_FFO ((~x) & MASK (UInt32,size));
}

inline int carbon_DFFZ (UInt64 x, UInt32 size=64)
{
  return carbon_DFFO ((~x) & MASK (UInt64,size));
}

inline int carbon_FLZ (UInt32 x, UInt32 size=32)
{
  return carbon_FLO ((~x) & MASK (UInt32,size));
}

inline int carbon_DFLZ (UInt64 x, UInt32 size=64)
{
  return carbon_DFLO ((~x) & MASK (UInt64,size));
}

inline SInt32 min (SInt32 a, SInt32 b) { return (a<b) ? a: b;}

// generate a checked store into a bitfield, handling out-of-bounds
// DTWIDTH is the bit-width of DT and should be 1..64
template <typename DT, typename VT, SInt32 DTWIDTH> inline void 
carbon_wset_slice_check (DT &var, SInt32 pos, SInt32 width, VT value)
{
  if (EXPECTED_VALUE (pos < 0, 0)) {
    REPORT_OOB ("write", "a vector", width-1, 0, pos);
    if (EXPECTED_VALUE ((-pos) >= width, 0)) {
      return;                   // all out of bounds
    }
    width += pos;               // reduce width
    value >>= (-pos);           // reduce / reposition value
    DT mask = MASK (DT, min (width, DTWIDTH));
    var = (var & ~mask) | (value & mask);
    return;
  } else if (EXPECTED_VALUE (pos >= DTWIDTH, 0)) {
    REPORT_OOB ("write", "a vector", width-1, 0, pos);
    return;                     // all out-of-bounds
  }

  width = min (DTWIDTH-pos, width);
  DT mask = MASK (DT, width) << pos;
  var = (var & (~mask)) | ((DT (value) << pos) & mask);
}

// generate a checked load from a bitfield, handling out-of-bounds and
// shifting the value read as needed
template <typename VT> inline typename MASKTYPE (VT)
carbon_wget_slice_check (VT value, SInt32 pos, SInt32 width)
{
  if (EXPECTED_VALUE (pos < 0, 0)) {
    REPORT_OOB ("read", "a vector", width-1, 0, pos);
    if (EXPECTED_VALUE ((-pos) >= width, 0))
      return 0;                   // all out of bounds
    value <<= -pos;
    pos = 0;
    return value & MASK (VT, width);
  } else if (EXPECTED_VALUE ((pos >= SInt32(sizeof(VT) * 8)), 0)) {
    REPORT_OOB ("read", "a vector", width-1, 0, pos);
    return 0;                     // all out-of-bounds
  }

  return (value >> pos) & MASK (VT, width);
}

// Algorithm for integer exponentiation taken from
//     http://en.wikipedia.org/wiki/Modular_exponentiation
// I ignored the modulus.  Al's previous code was linear in
// the value of exp, which could be large, especially with a
// randomly generated testcase.
//
// Note that carbon_pow will return 1 for negative exp, which is
// not correct, but the Verilog LRM says real exponentiation should
// be used if either argument is signed.
template <typename T> inline T
carbon_pow (T value, int exp) {

  T prod (1);
  while (exp > 0) {
    if ((exp & 1) == 1) {
      prod *= value;
    }
    exp >>= 1;
    value *= value;
  }
  return prod;
}
  
// Specialized support for untyped stores.  This is an attempt to work around
// alias analysis limitations in C++ compilers.  It appears to not be a
// sufficiently large enough hammer to keep compilers from incorrectly
// thinking that memory contents are unchanged.
//
template <typename IT, typename OT>
union pieces {
  IT whole;
  OT parts[sizeof(IT)/sizeof(OT)];
};

template <typename _Itype, typename _Atype> inline
const _Atype& AlignedRead ( const _Itype& r, int i)
{
  const pieces<_Itype, _Atype>& ur (reinterpret_cast<const pieces<_Itype,_Atype>&>(r));
#if pfGCC_3 || pfICC || pfGCC_4
    // Mark as read of _Itype* memory
  __asm__ ("# read %0" : : "m" (r) : /*"memory"*/);
#elif pfGCC_2
  asm("": : : "memory"); 
#elif pfMSVC
  asm("");
#else
#error "need to mark this as a read of object"
#endif
    return ur.parts[i];
}

template <typename _Itype, typename _Atype> inline
_Atype& AlignedWrite (_Itype& w, int i)
{
  pieces <_Itype, _Atype>& uw (reinterpret_cast<pieces<_Itype,_Atype>&>(w));

  // Mark as potential write of _Itype* memory
#if pfGCC_3 || pfICC || pfGCC_4
  __asm__ ("# write %0" : "+m" (w) : : /*"memory"*/);
#elif pfGCC_2
  asm ("" : : : "memory");
#elif pfMSVC
  asm ("");
#else
#error "need to mark this as a write of object"
#endif //pfGCC
    return uw.parts[i];
}


#endif
