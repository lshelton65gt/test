// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 
#ifndef __codegen_h_
#define __codegen_h_

#include "nucleus/NUPortConnection.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUCModel.h"
#include "nucleus/NUModuleInstance.h"

#include "codegen/CGContext_t.h"
#include "codegen/CodeNames.h"
#include "codegen/CGOFiles.h"
#include "codegen/CodegenErrorLexer.h"

#include "schedule/Schedule.h"
#include "schedule/CommonWrapper.h"

#include "iodb/IODBTypes.h"

#include "util/UtStack.h"
#include "util/UtString.h"
#include "util/UtIOStream.h"
#include "util/CodeStream.h"
#include <climits>

// Some useful constant values

  
// Vectors larger than this size require special support.
const size_t LLONG_BIT = CHAR_BIT * (sizeof (UInt64));

#undef LONG_BIT
const size_t LONG_BIT = (CHAR_BIT * sizeof (UInt32));
const size_t SHORT_BIT= (CHAR_BIT * sizeof(short));

class StringAtom;
class STBranchNode;
class ArgProc;
class NUDesign;
class NUModuleInstance;
class NUModuleElab;
class NUNetElab;
class NUCostContext;
class CarbonContext;
struct ResetGuards;
struct ExtraConds;
class MsgContext;
class Stats;
class DynBitVector;
class GenSched;
class GenProfile;
class CGProfile;
class CGBucketID;
class ScheduleBuckets;
class NUNetRefFactory;
class Fold;
class GenSchedFactory;
class UtOStream;
class UtOStream;
class UtUniquify;
class RankNets;
class CGOFiles;
class CGPortIface;
class CGPortIfaceNet;
class IODB;
class FuncLayout;
class CGNetElabCount;
class LangCppScope;
class CGTopLevelSchedule;
class InducedFaultTable;

class CodeGen;
extern CodeGen* gCodeGen;

//! CodeGen interface
/*!
 * Housekeeping class for Code Generation.  Contains file pointers and
 * global context for translating Verilog to C++.
 */
class CodeGen
{
public:
  //! Helper for constant-pool map ordering
  struct ConstPool_Less {
    bool operator() (const DynBitVector* p, const DynBitVector* q) const;
  };

  //! Constants are pooled and associated with a unique id of the form cv$\<n\>
  typedef UtMap<const DynBitVector*, UInt32, ConstPool_Less> ConstPool_t;


  friend class GenSched;

  //! Would this net be passed by reference?
  static bool sIsReferenceParameter(const NUNet* net);

  //! Check the target args
  /*! The -target and -o args are checked earlier so that the -getTarget argument really
   *  shows the selected compiler
   */
  bool checkTargetArgs ();

  //! Check that the parsed args are valid for codegen
  /*!
    This also updates some internal data structures based on the
    parsed arguments.
  */
  bool checkArgs();

  //! If true, force codegen to not run the backend compile.
  void putNoCC(bool noCC);

  //! Generate code for -testdriver option
  void codeVerilogTestDriver();

  //! Compile the simulation to produce expected output files.
  void compileSimulation (void);

  //! Parse the backend compilation output
  void parseMakeOutput (UtString &filename, bool debugging, bool inBuildDirectory);

  //! Walk the module hierarchy to determine which run system tasks/funcs
  void determineSystemCalls(NUDesign*);

  //! Conditionally emit a section specification, based on target compiler.
  void emitSection(UtOStream &out, const UtString &section);

  //! should this net be written to the type dictionary?
  /*!
    This function used to take just an NUNet*, but we need to check if
    the actual elaborated node is in the iodb. If it is, then it
    will be written to the db, and thus must be added to the type
    dictionary.
  */
  static bool isIncludedInTypeDictionary(const STAliasedLeafNode* leaf,
                                         IODB* iodb);

  //! Name formatting classes (derived from Stroustrup 21.4.6.3)
  struct CxxBoundName;

  //! Generate C++-friendly names from Verilog names.
  class CxxName {
    //! Let stream output access class internals
    friend UtOStream& operator << (UtOStream&, const CodeGen::CxxBoundName&);
    //! Let UtString converter access internals also
    friend UtString& operator << (UtString&, const CodeGen::CxxBoundName&);

    friend struct CxxBoundName;

    //! UtString prefix (typically m_, f_ or get_)
    const char *mPrefix;
    //! UtString suffix
    const char *mSuffix;

  public:
    //! Trivial constructor
    /*! \arg pref is prefix UtString prepended to name
     *  \arg suff is suffix UtString (usually empty)
     */
    explicit CxxName(const char *pref="m_", const char *suff="")
      :mPrefix(pref), mSuffix(suff) {}

    //! Associate name with particular format
    CxxBoundName operator() (const StringAtom *name) const
    {
      return CxxBoundName (*this, name, NULL);
    }

    //! \overload
    CxxBoundName operator() (const STBranchNode* parent) const;

    //! \overload
    CxxBoundName operator() (const NUNet *name) const
    {
      return CxxBoundName (*this, name->getName(), name->getScope());
    }
    //! \overload
    CxxBoundName operator() (const NUModuleInstance *name) const
    {
      return CxxBoundName (*this, name->getName(), name->getParent());
    }
    //! \overload
    CxxBoundName operator() (const NUModule *name) const
    {
      return CxxBoundName (*this, name->getName (), NULL);
    }
    //! \overload
    CxxBoundName operator() (const NUNetElab *name) const
    {
      return CxxBoundName (*this, name->getNet ()->getName (), name->getNet()->getScope());
    }
  };

  /*! Composite class to associate properties (CxxName) and a
   *  specific symbol (StringAtom).  See Stroustrup 21.4.6.3.
   */
  struct CxxBoundName{
    //! printing format
    const CxxName& n;
    //! symbol to format
    const StringAtom *mName;
    //! Scope of the name
    const NUScope* mScope;

    // Trivial constructor
    CxxBoundName(const CxxName& nn, const StringAtom *name, const NUScope* scope)
      : n(nn), mName(name), mScope(scope) {}

    // Conversion to a StringAtom*
    operator const StringAtom *() const {
      return gCodeGen->getCodeNames ()->translate (mName);
    }

    // Convert to formatted string
    operator const UtString () const;

    // Construct a prefix for scope if it's not a module scope.
    void constructScopePrefix(UtString* buf) const;
    // Construct a prefix for the given scope.
    static void sConstructScopePrefix(const NUScope* scope, UtString* buf);
  };

  //! virtual destructor.
  /*! Responsible for cleanup of codegen resources. */
  ~CodeGen ();

  //! Returns the namespace name for genereated code
  const char* getNamespace() const;

  //! Generate code
  /*!
    Given a schedule and a tree, generate C++ code and
    convert that to an object library.

    \returns NOTHING.
  */
  void generateCode(SCHSchedule * sched, NUDesign *tree,
		    Stats* stats, bool phaseStats);


  //! mark the scheduled blocks - non-scheduled blocks are ignored
  void visitSchedule (void);

  //! Constructor -- sets up user option flags
  CodeGen(CarbonContext*,
	  IODBNucleus*,
	  IODBTypeDictionary*,
	  NUNetRefFactory*,
          SourceLocatorFactory*,
	  AtomicCache*,
	  ESPopulateExpr*,
          MsgContext*);

  //! set up the command line options (call only once)
  void setupOptions();

  //! Set the tristate mode
  void setTristateMode(TristateModeT mode) { mTriStateMode = mode; }

  //! Mark all clocks as referenced
  void markClocks();

  //! Generate unique id of design : cbuild version : time-stamp
  /*!
    If the uid has already been created, it just returns that, so this
    can be called multiple times for the same design.
  */
  void getUID (UtString*);
  
  //! Prints the targetted compiler name.
  void printBackendTarget() const;

  //! Get the target base name
  const UtString* getTargetName() const { return &mTargetName; }

  //! Get the target directory
  const UtString* getIfaceTag() const { return &mIfaceTag; }

  //!Get current class being compiled
  const StringAtom * getClass (void) const
  {
    return mClassName;
  }
  
  //!Set the current class being compiled
  void putClass (const StringAtom* s) {mClassName = s;}

  //! Get the name of the model's current bucket variable.
  UtString getBucketVarName() const {
    return UtString(gCodeGen->getTopName()->str()) + "_current_bucket";
  }

  //! Get the name of the model's current schedule bucket variable.
  UtString getScheduleBucketVarName() const {
    return UtString(gCodeGen->getTopName()->str()) + "_current_schedule_bucket";
  }

  //! Is this net treated as a tristate?
  bool isTristate (const NUNet *n) const;

  //! Is this net modelled as a primary bidirect
  /*!
   * Primary bidirects are used for the structure interface and maintain
   * xdrive and idrive values in addition to the signal value.
   */
  bool isBidirect (const NUNet *n) const;

  //! Does this net have a virtual implementation
  bool hasVtab(const NUNet *n);

  //! Should we generate code to check the sanity of the offsets stored in the symbol table?
  bool genOffsetSanity() const {return mCodegenOffsetSanity;}

  //! Should we indicate that dead code was detected?
  bool isVerboseDeadCode () const {return mVerboseDeadCode;}
  //! Account for the vtab pointer when doing address-of manipulations
  size_t vtabAdjustment(const NUNet *n);

  //! print out method declarations for all encapsulated combinational cycles
  void emitCycleDeclarations();

  //! QualifiedNet:index map type
  typedef UtMap<const NUNetElab*, int> NetToIntMap;

  //!A named iterator
  typedef NetToIntMap::iterator ClockIter;

  //!A named iterator
  typedef NetToIntMap::iterator ResetIter;


  //! m_\<name\>
  //  const char* getMemberName(const NUNet* net, UtString* buf) const;
  //! Name of net,  no prefix, suitable for code generation
  //  const char* getGenerateName(const NUNet* net, UtString* buf) const;
  
  //! Return basic type information
  const char * CBaseType (size_t prec)
  {
    if (prec <= LLONG_BIT)
      return "CarbonUInt";
    else
      return "BitVector";	// Missing <size>!
  }

  //! Track maximum required alignment for a class
  /*!
   * \param align address must be multiple of this.
   * \returns maximal alignment seen in compiling current class
   */
  size_t alignment (size_t align=1);

  //! Support computing storage offsets
  //! \return offset relative to class base of this member.
  //! Beware of compiler dependencies here!
  size_t allocation (size_t size, size_t align=1, size_t numElem=1);

  //! Support computing onDemand state offsets
  //! \return offset relative to start of entire onDemand state buffer
  size_t onDemandStateAllocation (size_t size)
  {
    size_t current = mOnDemandStateSize;
    mOnDemandStateSize += size;
    return current;
  }

  //! Begin code generation for a class
  void startClassData (void) {
    mClassAlignment = 1;
    mClassAllocation = 0; }

  //! Reset data alignment/allocation
  void stopClassData (void) {
    mClassAlignment = UtUINT32_MAX;
    mClassAllocation = 0; }

  //! Restore allocation when we conditionally allocate a struct
  void putAllocation (size_t alloc);

  //! Restore alignment
  void putAlignment (size_t alignment);

  //! Get current value of allocation.
  size_t getAllocation();

  //! Template class filename for BitVector\<\> template
  UtString cBitVector_h_path;

  //! class header file for Checkpoint I/O stream classes
  UtString cCheckpointStream_h_path;

  //! Template class filename for TRI-enable template.
  UtString cTristate_h_path;

  //! Template class filename for TRI-enable Bitvector template.
  UtString cTristateBV_h_path;

  //! Template class filename for Memory<> template
  UtString cMemory_h_path;

  //! Template class filename for SparseMemory<> template
  UtString cSparseMemory_h_path;

  //! Template class filename for Forcible<T,MT> template
  UtString cForcible_h_path;

  //! Template class filename for ForciblePOD<T> template
  UtString cForciblePOD_h_path;

  //! class header filename for C++ Carbon runtime support
  UtString cCarbonRunTime_h_path;

  //! shell type creation header file name
  UtString cShellNetCreate_h_path;

  //! MemoryCreateInfo structure header file name
  UtString cMemoryCreateInfo_h_path;

  // Template class filename for CarbonNet template
  //  const char * cCarbonNet_h_path;

  //! Change array index map function name
  const char* cChangeMapFnName;

  //! Return handle to reserved-name translation object
  CodeNames* getCodeNames (void) {return mCodeNames;}

  //! Return handle to msgContext
  MsgContext * getMsgContext (void) {return mMsgContext;}

  //! Return handle to CarbonContext
  CarbonContext *getCarbonContext (void) {return mCarbonContext;}

  NUNetRefFactory* getNetRefFactory () { return mNetRefFactory;}

  //! Return root symbol name (needed for hierarchical names)
  const StringAtom *getTopName (void) { return mTopName;}

  //! Return root module
  const NUModule* getTopModule (void) { return mTopModule;}

  //! Set the top module
  void putTopModule (const NUModule* mod) { mTopModule = mod; }

  //! Manage constants table.
  /*!
   * BitVector constants are constructed once per module and shared
   * as much as possible.  
   */
  UInt32 addPooledConstant (const DynBitVector& bv, bool sign);

  //! Emit BitVector literals for this module
  void dumpConstantPool (UtOStream& outfile, CGContext_t context);

  //! Destroy existing constant pool
  void flushConstantPool (void);

  //! Save the current constant pool for later retrieval
  void saveConstantPool (const NUModule* mod);

  //! Get the constant pool associated with a given module
  void restoreConstantPool (const NUModule* mod);
  
  //! get the atomic cache
  AtomicCache* getAtomicCache() {return mAtomicCache;}

  //! Remember schedule predicates
  typedef std::pair<const SCHScheduleMask*, const SCHInputNets*> PredKey;

  typedef UtMap<const PredKey, int> PredicateToIntMap;
  typedef UtStack<PredicateToIntMap*> PredicateToIntMapStack;

  //! Stack of maps between predicates and an associated temporary
  PredicateToIntMapStack mPredicateMapStack;

  //! Number received by previous predicate.
  int mPredicateTempCount;

  typedef UtMap<const SCHEvent*, int> EventToIntMap;
  typedef UtStack<EventToIntMap*> EventToIntMapStack;

  //! Stack of maps between events and the map to an associated temporary.
  EventToIntMapStack mEventMapStack;

  //! Number received by previous event.
  int mEventTempCount;

  //! Push onto a stack for event/predicate temporaries
  void pushTempContext();

  //! Pop off a stack for event/predicate temporaries
  void popTempContext();

  CGProfile* getCGProfile() { return mCGProfile; }
  CGBucketID* getBucketID() { return mBucketID; }
  ScheduleBuckets* getSchedBuckets() { return mSchedBuckets; }


  //! Distinguish between two kinds of clocks as carbon_<tag>_schedule()
  enum clockKind
    {
      ePrimaryClock = -1,	//!< This clock is a PI
      eDerivedClock = -2	//!< This clock is derived
    };


  //! All the clocks we schedule on, and index into parameter array
  //! Map clock:inputparm for codegeneration.
  NetToIntMap mClockTable;

  //! A map from primary clocks to an index.
  /*! This is used by the code to test for race conditions
   */
  NetToIntMap mPrimaryClocks;

  //! All the clocks we schedule on, and index into parameter array
  //! Map clock:inputparm for codegeneration.
  NetToIntMap mSyncResetTable;
  NetToIntMap mAsyncResetTable;

  //! All input parameters have a Map to the changed[] array (or -1)
  CGNetElabCount* mChangedMap;

  //! debug routine to print the changed map
  void printChangedMap();

  //! True if bounds checking code is disabled
  bool mNoOOB;

  //! True if out-of-bounds accesses should be reported.
  bool mCheckOOB;

  //! True if smart inlining enabled
  bool mDoInline;

  //! True if precompiled headers are enabled.
  bool mPrecompiledHeaders;

  //! True if save()/restore() support is desired.
  bool mDoCheckpoint;

  //! True if -shareConstants allows pooling and elimination
  bool mShareConstants;

  //! True if -codegenOffsetSanity is specified
  bool mCodegenOffsetSanity;

  //! True if -sanitizeCheck is specified
  bool mSanitizeCheck;

  //! True if -verboseDeadCode is specified
  bool mVerboseDeadCode;

  //! True if we are embedding the IODB file in the generated library
  bool mEmbedIODB;

  //! Inline any generated functions where \#calls * \#instrs is \<= this number
  SInt32 mInlineTotalLimit;

  //! Always inline any generated functions with <= this number of instructions
  SInt32 mInlineAlwaysInstrLimit;

  //! Limit on function size for inlined schedule actions.
  SInt32 mFunLimit;

  //! Cost model
  NUCostContext *mCosts;

  //! Profile code generation
  GenProfile *mGenProfile;

  //! Return the compiler target for codegen selection
  enum TargetCompiler getTarget () const {return mTargetCompiler;}

  NUScope* getScope () const;

  void pushScope (const NUScope* b);

  void popScope (void);

  Fold* getFold () const { return mFold; }
  void putFold(Fold* fold) {mFold = fold;}

  //! note that a changed-flag has been written
  int writeChanged(int changed);

  //! note that a changed-flag has been read, verify it was written first
  int readChanged(int changed);

  bool is64bit () const { return m64; }
  bool is32bit () const { return !m64; }
  static bool isNamedBlock (const STBranchNode *node);

  //! Translate and print a C++ hierarchical name
  /*!
   * If appendSep is true, appropriate separator is stuck at the end
   * of the generated path. Ex: top.blk$inst.  test.blk1$blk2$
   */
  void outputHierPath(UtOStream &out, const STBranchNode *parent,
                      bool appendSep = true, const char *sep = ".");

  //! Translate a C++ hierarchical path to a C++ compatible name
  /*!
   * \param includeTop if true, include top level of hierarchy in the generated path
   * \param makeMembers if true, each segment of path is treated as a member (i.e. 'm_' prefix).
   */  
  UtString& CxxHierPath(UtString *path, const STBranchNode *parent,
                        const char *sep = ".", const STBranchNode *limit=0, 
                        bool includeTop = true, bool makeMembers = true);

  char getOptLevel () const {return mOptLevel; }

  //! Return true if the specified bit-size should be coded as a sparse memory.
  bool isSparseMemoryBitSize(UInt32 bit_size) const { return (mSparseMemoryBitCapacity < ((UInt64)bit_size)); }

  //! Return the design we're codegenerating
  const NUDesign *getDesign() const { return mTree; };

  //! Are we passing build errors straight to standard error
  bool isPassingBuildErrors () const;

  //! Should we annotate the generated code
  bool annotateGeneratedCode () const;

  //! Should we use the (rather slow) indent engine
  bool useIndentEngine () const;

  //! Are we obfuscating output
  bool isObfuscating() {return mIsObfuscating;}

  //! Are we generating an onDemand model?
  bool isOnDemand() {return mOnDemand;}

  //! Was -profileGenerate passed?
  bool isProfileGenerate() const;

  //! Was -profileUse passed?
  bool isProfileUse() const;

  //! Go to the next c-model call name
  void incrCModelCallName(void);

  //! Get a unique string for this c-model call
  const UtString& getCModelCallName() const { return *mCModelCallName; }

  //! Get the cgaux info associated with a net
  CGAuxInfo* getCGOp(const NUNet* net);

  //! Overload
  CGAuxInfo* getCGOp (const NUNamedDeclarationScope* block);

  //! Put the cgaux info associated with a net
  void putCGOp(const NUNet* net, CGAuxInfo* cgOp);

  //! Overload
  void putCGOp (const NUNamedDeclarationScope* block, CGAuxInfo* cgOp);

  const RankNets * getRankNets() { return mRankNets; }
  CGOFiles * getCGOFiles() { return mCGOFiles;}

  UtOStream &CGOUT()
  {
    return mCGOFiles->CGOUT();
  }

  UtOStream *CGFileOpen(const char *path, const char *extra, 
    bool isObfuscating, bool needCleartext = false) 
  {
    return mCGOFiles->CGFileOpen(path, extra, isObfuscating, needCleartext);
  };

  UtOStream &switchStream (CGContext_t context) 
  {
    return mCGOFiles->switchStream(context);
  };


  void addExtraHeader (const UtString& hdr)
  {
    mCGOFiles->addExtraHeader (hdr);
  };

  void addExtraHeader (const NUModule* mod)
  {
    mCGOFiles->addExtraHeader (mod);
  };

  SCHSchedule* getSched() const { return mSched; };

  //! Remeber the given node as being emitted already.  Only used for continuous drivers.
  void addToEmittedSet(const NUUseDefNode *node);

  //! Return true if the given node has been emitted already.  Only used for continuous drivers.
  bool inEmittedSet(const NUUseDefNode *node) const;

  //! Returns order information for function-layout functions - zero implies not ordered.
  UInt32 getLayoutOrder (const NUUseDefNode* node) const;

  //! Construct a list of modules which are needed by non-local task hierrefs appearing in the given node.
  /*!
    \param hierTaskEnables Will be populated with all found hierarchical task enables
   */
  void generateTaskHierRefs(const NUUseDefNode *node, NUTaskEnableVector &hierTaskEnables);

  //! Generate the include files needed to access hierarchical tasks.
  void generateTaskHeaders (const NUTaskEnableVector &hierTaskEnables);

#ifdef INTERPRETER
  //! Establish an offset for an elaborated module instance for the benefit
  //! of the interpreter
  void putModuleOffset(STBranchNode*, UInt32);

  //! Find the offset within the data structure for a module
  UInt32 getModuleOffset(STBranchNode*) const;
#endif

  //! Set the current translation to internal mode (default)
  /*! Switches to the internal code names translation table. This
   *  means obfuscation should occur on all translations.
   */
  void putInternalMode() { mCodeNames = mInternalCodeNames; }

  //! Set the current translation to interface mode
  /*! Switches to the interface code names translation table. This
   *  means obfuscation does not occur on all translations.
   */
  void putInterfaceMode() { mCodeNames = mInterfaceCodeNames; }

  //! Accessor for changed-array indices
  SInt32 getChangedIndex(const NUNetElab* net) const;

  //! -1 => not used, 0|1 are branch direction hint default value
  SInt32 getBuiltinExpectValue () { return mBuiltinExpectValue; }

  CGPortIface* getPortIface() { return mPortIface; }

  bool precompiledHeaders () const { return mPrecompiledHeaders; }
  
  //! Generate PLI wrapper to make design callable from Verilog simulators
  void codePliInterface( NUDesign *design, CNetSet &bothEdgeClks,
                         PortNetSet &validOutPorts, PortNetSet &validInPorts,
                         PortNetSet &validTriOutPorts, PortNetSet &validBidiPorts,
                         CNetSet &clksUsedAsData, ClkToEvent &clkEdgeUses );
  void codePliVerilogWrapper( NUDesign *design ) ;

  IODBNucleus* getIODB () const { return mIODB; }

  //! Remember that a hierarchical reference has been seen as a port alias.
  /*!
   * \sa isLocallyRelativeHierRef
   */
  void rememberPortAliasedHierRef(NUNet * hier_ref_net);

  //! Determine if a given hierarchical reference can be considered locally relative.
  /*!
   * Locally relative hierarchical references used as port connections
   * cannot be coded as direct references to their resolution because
   * that resolution may be a reference. That reference may not be
   * initialized at the time we try to read from the hierarchical
   * reference.
   *
   * This method understands which hierarchical references are used as
   * port connections and disqualifies those hierarchical references
   * as locally relative.
   *
   * As a result, this method may return false when
   * NUNetHierRef::isLocallyRelative returns true.
   *
   * \sa rememberPortAliasedHierRef
   */
  bool isLocallyRelativeHierRef(const NUNet * hier_ref_net,
                                NUModuleInstanceVector * instance_path = NULL,
                                NUNet ** resolved_net = NULL) const;


  //! put current module
  void putCurrentModule (const NUModule* curr) { mCurrentModule = curr; }

  //! get current module
  const NUModule* getCurrentModule (void) const { return mCurrentModule; }

  //! Add a temp net that should be declared
  void addTempNet(const NUTempMemoryNet* tmem);

  //! Check if a temp memory net is undeclared
  /*! The routine that calls this is going to declare the temp memory
   *  net so if we find it we also remove it from the set.
   */
  bool isUndeclaredTempNet(const NUTempMemoryNet* tmem);

  //! Clear the temp memory nets when we are done with a block
  /*! Some temp memory initialization statements might get deleted by
   *  dead code analysis (if the entire memory is written) so we
   *  should just clear these.
   */
  void clearUndeclaredTempNets(void);

  //! Get the directory where the generated .cxx files will go
  const char* getBuildDirectory() {return mbuildDirectory.c_str(); }

  //! are we embedding IODB files in the generated library?
  bool embedIODB() const {return mEmbedIODB;}

  //! Maintain mMaxMemoryWidth tracking the largest memory width
  UInt32 getMaxMemoryWidth () const { return mMaxMemoryWidth; }
  void putMaxMemoryWidth (UInt32 w) {mMaxMemoryWidth = w; }

  //! Has this net been declared as a slow clock using a directive?
  bool isSlowClock(const NUNetElab*) const;

  //! Has this net been declared as a fast reset using a directive?
  bool isFastReset(const NUNetElab*) const;

  //! Generate code to start a sample scheduling bucket.
  void startSampleSchedBucket(const UtString& bucket_name, LangCppScope* scope,
                              int bucket_id = -1);
  //! Generate code to start a sample scheduling bucket.
  void startSampleSchedBucket(const UtString& bucket_name, UtOStream& stream,
                              int bucket_id = -1);
  //! Generate code to stop a sample scheduling bucket.
  void stopSampleSchedBucket(LangCppScope* scope, bool pop = true);
  //! Generate code to stop a sample scheduling bucket.
  void stopSampleSchedBucket(UtOStream& stream, bool pop = true);

  //! Assume .CXX module is empty
  void putEmptyCxx (const NUModule*);

  //! Indicate that we wrote something to the module we need to compile
  void putNonEmptyCxx (const NUModule*);

  //! Do we we need this module's .cxx file?
  bool isEmptyCxx (const NUModule*) const;

  //! encode an IODB file as a C++ file
  bool encodeIODBFile(CarbonDBType type);

  //! \return the HDL annotation factory
  CodeAnnotationManager &getHdlAnnotationManager () { return mHdlAnnotations; }

  //! \return the Implementation annotation factory
  CodeAnnotationManager &getImplAnnotationManager () { return mImplAnnotations; }

  //! Update the c-model calls for a given module
  void findCModelCalls(const NUModule* module);

  //! Release the memory for the current module's c-model calls
  void freeCModelCalls(void);

  //! Get the c-model calls for a given interface
  NUCModelCallVectorLoop loopCModelCalls(const NUCModel* cmodel);

  //! Test if there are any c-model calls
  bool hasCModelCalls(void) { return !mCModelCalls->empty(); }

  //! Test if there are any c-model calls for a given c-model
  bool hasCModelCalls(const NUCModel* cmodel);

private:
  //! Track modules that contain no interesting member functions in the .cxx file
  UtSet<const NUModule*> mEmptyModules;

  //! helper function to run make steps
  /*! 
    returns status of the compile 
  */
  int runMakeStep (const UtString& cmd, const char* logFile, const char* errMsg);

  //! Stack of block contexts
  NUScopeStack mScopeStack;

  //! Maintain size of current class for debug information
  size_t mClassAllocation;

  //! Maintain overall size of onDemand state
  size_t mOnDemandStateSize;

  //! Maximal alignment required
  size_t mClassAlignment;

  //! Number of external parameters in the interface
  int mNumExtParams;

  // Copy constructor is unsupported.
  CodeGen(const CodeGen&) :
    mCarbonContext(0) {}
  CodeGen(const CodeGen*) :
    mCarbonContext(0) {}

  //! Database that contains I/O pins
  IODBNucleus* mIODB;

  //! Handle to argument list and compiler driver.
  CarbonContext *mCarbonContext;

  //! Dictionary mapping types to a unique index
  IODBTypeDictionary* mTypeDictionary;

  //! Setting of -tristate flag
  TristateModeT mTriStateMode;

  //! NetRefFactory handle
  NUNetRefFactory *mNetRefFactory;

  //! Table of constants mapping <value:uuid>
  ConstPool_t *mConstantPool;
  
  typedef UtMap<const NUModule*, ConstPool_t*> ModuleConstantPoolMap;
  //! Table of mappings from <module:constantpool>
  ModuleConstantPoolMap* mConstantPoolMap;
  
  //! Remember working directory before codegen
  /*! We have to OSChdir() to a scratch directory, create
    the files, generate the C++, compile, link and place
    the output into the correct target directory.
  */
  UtString mcurrentDirectory;
 
  //! Directory where we build the intermediate pieces
  UtString mbuildDirectory;

  //! basename of desired target (from -o or default libdesign)
  UtString mTargetFile;

  //! target file name WITHOUT any extension.
  UtString mTargetName;

  //! Fully expanded directory where user-visible results go
  //. (dirname(mTargetFile) == mTargetDirectory)
  UtString mTargetDirectory;

  //! namespace name for generated code
  UtString mNamespace;

  //! True if testdriver is to be created
  bool mDoTestDriver;

  //! True if testdriver is to be compiled (before linking)
  bool mCompileTestDriver;

  //! True if testdriver should be linked (the default)
  bool mLinkTestDriver;

  bool mGeneratingMem;
  bool mGeneratingSparseMem;

  typedef UtSet<UInt32, std::less<UInt32> > UInt32Set;

  void setupBuildDirectory (void);
  void cleanupBuildDirectory (void);

  void chdirToOrigDir (void);
  void chdirToBuildDir (void); // valid only after setupBuildDir

  //! Generate declarations for header files, etc common to all schedule files
  void codeScheduleBoilerPlate (void);

  //! Generate code for initializing c-models
  void codeCModelInitialization (void);

  //! Generate code for creating c-models
  void codeCModelCreate (void);

  //! Generate code for registering c-models
  void codeCModelRegistration (void);

  //! Generate code for c-model replay mode changes
  void codeCModelModeChange(void);

  //! Generate C wrapper to make design callable as c-model
  void codeCModelWrapper(NUDesign *design);

  //! Generate code to create PLI task instance data
  void codePliCreate(void);

  //! Generate initial callbacks to PLI task instances
  void codePliInit();

  //! Generate code for instantiation, initialization
  void codeInitialization (void);

  //! Generated code for the mode change function
  void codeModeChange(void);

  //! Generate code for the runtime schedules.
  void codeSchedule(void);

  //! Generate the save and resore functions
  void codeSaveRestore (void);
  
  //! Generate canonical name for clock schedule functions
  void generate_schedule_name (UtString &name,
			       const NUNetElab* clock=0,
			       SCHSequential* sc=0,
			       const char* suffix=0);

  //! Code clock-sensitive schedule function
  void generate_clock_schedule (const NUNetElab*,
				SCHSequential* sc,
				ClockEdge e,
				const char *unique);

  //! Overloaded
  void generate_clock_schedule (const NUNetElab*,
				SCHCombinational* comb,
				const char *unique);

  //! Code combinational schedule
  void generate_combinational_schedule (SCHCombinational *comb,
					const char* subName);

  // BEGIN MY EDITS 
  friend class CGTopLevelSchedule;
  void getNetElabEmitString(UtString* buf, const NUNetElab* netElab);
  void codeProfPop(int bucket_id, LangCppScope* scope);
  void codeProfPush(int bucket_id, LangCppScope* scope);
  void genCombScheduleCalls(LangCppScope* cppScope,
                            CGTopLevelSchedule* topLevelSched,
                            SCHScheduleType schedType,
                            const char* schedName, const char* fnPrefix);
  void genDataScheduleCalls (LangCppScope* scope, CGTopLevelSchedule* topLevelSched);
  void codeClearNets(LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched);
  //! Generate the glitch detection logic.
  void codeClockGlitchDetection(LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched);
  void codeClockFunction(LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched);
  //! Generate the calls to sequential schedules
  void genSequentialCalls(UInt32Set & SyncSensitives, 
                          LangCppScope* cppScope,
                          CGTopLevelSchedule* topLevelSched);
  //! Generate the calls to combinational schedules (transition and sample)
  void genCombinationalCalls (SCHScheduleType type, const char * subName,
                              LangCppScope* cppScope, 
                              CGTopLevelSchedule* topLevelSched);
  //! Generate all derived clock schedules.
  void codeDerivedClockSchedules(LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched);
  //! Generates all the old clock local storage and init
  void codeOldClockInit(LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched,
                        SCHClocksLoop c);
  //! Code the initial states for the oldclock_N_ variables.
  void codeDerivedClockEdgeDetection(LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched);
  void codeDerivedScheduleCall(SCHDerivedClockBase* dclBase,
                               const SCHInputNets* inputNets,
                               UInt32Set& SyncSensitives,
                               LangCppScope* cppScope,
                               CGTopLevelSchedule* topLevelSched);
  void codeDerivedClockCycleCall(SCHDerivedClockCycle* dclCycle,
                                 UInt32Set& SyncSensitives,
                                 LangCppScope* cppScope,
                                 CGTopLevelSchedule* topLevelSched);
  int codeDerivedChanged(const NUNetElab* derivedClk,
                         const char* writeVarPrefix,
                         const char* readVarPrefix,
                         bool orChange,
                         LangCppScope* cppScope,
                         CGTopLevelSchedule* topLevelSched);
  //! Generate the derived clock logic calling sequence
  void codeDerivedClockCall(SCHDerivedClockLogic* dcl,
                            UInt32Set& SyncSensitives,
                            const char* varPrefix,
                            bool combTest,
                            LangCppScope* cppScope,
                            CGTopLevelSchedule* topLevelSched);
  //! generate sync() call for PI or Derived clocks.  
  void SyncOnClock(UInt32Set& sensed, SCHSequentials::SULoop l, 
                   LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched);
  //! Format a derived clock evaluation block
  void evalDerivedClock(const char* fn,
                        int foundClockIndex,
                        const ResetGuards& rg,
                        const NUNetElab* clk,
                        const SCHScheduleMask* extraEdges,
                        const char* varPrefix, 
                        LangCppScope* cppScope,
                        CGTopLevelSchedule* topLevelSched);
  void codeResetGuardExtraCondSeqCall(const char* fn,
                                      int foundClockIndex,
                                      const ResetGuards& rg,
                                      const NUNetElab* clk,
                                      const SCHScheduleMask* extraEdges,
                                      const char* varPrefix, 
                                      LangCppScope* cppScope,
                                      CGTopLevelSchedule* topLevelSched);
  void
  codeDCLCycleChanged(const NUNetElab* clk,
                      bool clearSettled, LangCppScope* cppScope,
                      CGTopLevelSchedule* topLevelSched);
  // END MY EDITS

  //! Possibly clears the dcl_changed flag
  /*! This emits code to clear the dcl changed flag for iterations
   *  other than the first. It should only be called for break net dcl
   *  cycle clocks. This are the ones that are used before computed in
   *  a cycle.
   */
  void maybeClearDCLChanged(const NUNetElab* clk,
                            LangCppScope* cppScope,
                            CGTopLevelSchedule* topLevelSched);

  //! Generate code to set the current model for profiling
  void genSetCurrentModel(UtOStream& out);

  //! Generate code to restore the current model for profiling
  void genRestoreCurrentModel(UtOStream& out);

  //! Generate header file for external interfaces
  void codeInterface(void);

  typedef UtVector<SCHDerivedClockBase*> DCLScheds;
  typedef std::pair<const SCHInputNets*, DCLScheds> DCLGroup;
  typedef UtVector<DCLGroup> DCLGroups;

  typedef UtVector<SCHCombinational*> CombinationalSchedules;
  typedef std::pair<const SCHInputNets*, CombinationalSchedules> CombinationalGroup;
  typedef UtVector<CombinationalGroup> CombinationalGroups;

  typedef UtVector<SCHSequential*> SequentialSchedules;
  typedef std::pair<const SCHInputNets*, SequentialSchedules> SequentialGroup;
  typedef UtVector<SequentialGroup> SequentialGroups;


  //! Factor schedules by unique input set.
  template<class ScheduleGroupClass, class ScheduleLoopClass>
  bool groupSchedulesByInputs(ScheduleGroupClass & scheduleGroups,
			      ScheduleLoopClass loop);


  void codeDerivedSchedule(SCHDerivedClockLogic* dcl);

  //! Generate a test for whether glitch detection is on
  void codeGlitchTest(UtOStream& schedSrc);


  //! Generate header for internal schedule.cxx use
  void codeScheduleHeader ();


  //! Generate test driver for this design
  /*!
    \returns false if there is nothing to compile (empty top-level
    module), true otherwise
  */
  bool codeTestDriver(UInt32 vector_count, 
                      SInt32 vector_seed, 
                      UInt32 reset_length, 
                      UInt32 clock_delay, 
                      const char *vectorfile,
                      bool   debug, 
                      bool   dump_vcd, 
                      bool   dump_fsdb, 
                      bool   useFullDB, 
                      bool   glitchDetect, 
                      bool   checkpoint,
                      bool   skipObservables);

  //! Compile the change array access function
  void genChgArrToStoreFn();

  //! Generate the C++ realization of the type dictionary
  void compileDictionary (void);

  //! Decides which shell headers to include
  void compileDictionaryIncludes (void);

  //! writes header file inclusion to disk
  void writeDictionaryInclude(const char* headerRoot);

  //! Decorate the symbol-table with C++ type information.
  void decorateSymbolTable (void);

  /*!
   * Mark nets in the symbol table as needing debug schedule if they have a def in
   * a debug schedule node which is not covered by a continous driver.
   *
   * Note that debug schedule marking also occurs in updateTypeInformationFromSymtab.
   */
  void findDebugScheduleCornerCase(STSymbolTable *symtab);

public:

  //! Save away the type information from symbol table entries.
  /*! 
    These leaf nodes are not traversed during a design walk (flattened
    aliases, for example)

    This needs to be visible so that CarbonContext::runGuiBuildSymtab can
    construct the piece of the symbol table needed for the GUI.
  */
  void updateTypeInformationFromSymtab(STSymbolTable * symtab);

private:

  // bad english, but maybe better than generatingPortInterface, since
  // we tend to use 'is' for booleans, and 'generating' could have the
  // connotation of 'in the midst of'.
  bool isGeneratePortInterface() const;
  // Fills the CGPortIface object
  void populatePortInterface();
  // Generates carbon_design_ports declaration code
  void codePortInterface(UtOStream* file);
  // Generates code based on CGPortIface - must call populatePortInterface first.
  void codePortInterfaceInitialization();
  CGPortIfaceNet* addCGPortIfaceBidiOrOut(NUNetElab* netElab, const STAliasedLeafNode* node, bool useElabName);
  void generateCarbonSimulation(UtOStream& out, const StringAtom* name);
  void writeSimplePointerDeclare(UtOStream* out, 
                                 CGPortIfaceNet* ifaceNet, 
                                 SInt32 changeArrayIndex,
                                 const UtString& name);
  void writePortInterfaceDeclare(UtOStream* out,
                                 CGPortIfaceNet* ifaceNet);

  typedef UtHashSet<CGPortIfaceNet*> PortIfaceNetSet;
  void codePortObjVarInit(CGPortIfaceNet* net, const STSymbolTableNode* node, bool isConst, PortIfaceNetSet* visited);
  

  //! Sets up member variables needed for the backend compile
  void setupBuildDirectoryVars (void);

  //! Writes out the backend makefile and xref table
  void createCompileConfigFiles (void);

  //! Complain if .gcda files (from -generateProfile) are missing.
  void verifyFeedbackExists();

  //! Make sure each Net has an entry in the Type Dictionary
  /*!
    \param net Net to add type information for. If NULL, this function
    returns immediately.
    \param storageNet Must not be NULL if net is not NULL. The NUNet
    of the storage node of the alias ring.
    \param leaf The current leaf that we are adding information for.
    \param symtab The symboltable. Will not add or remove any nodes
    from it. It is used to lookup nodes and update the boms, if
    necessary.
  */
  void AddTypeToDictionary (const NUNet* net, const NUNet* storageNet,
                            STAliasedLeafNode* leaf, 
                            STSymbolTable* symtab);

  //! Determine if we're generating a 64-bit model, and set the m64 flag
  /*! 
   *  Check CARBON_TARGET_ARCH and cbuild command line switches to
   *  determine if a 64-bit model should be built.  Will report an
   *  error if a 64-bit model is requested from a 32-bit platform.
   */
  void determineGenerating64BitModel(ArgProc* args);

  //! List of all generated classes.
  const StringAtom* mClassName;

  //! Interface Tag (normally derived from -o filename
  /*!  This is used to make interface function names unique, so that
    multiple simulation objects can coexist. */
  UtString mIfaceTag;

  
  //! UID for design we are compiling
  UtString mDesignUID;
  
  //! Top-level class instance name
  const StringAtom *mTopName;

  //! Top-level module
  const NUModule* mTopModule;
  
  //! 
  //! Internal parse tree
  NUDesign	*mTree;

  //! Computed scheduling information -- static for declareCycleMethods()
  SCHSchedule	*mSched;

  //! Message Context
  MsgContext	*mMsgContext;

  //! C++ name translation cache for the current names names
  /*! This variable either points to mInterfaceCodeNames or
   *  mInternalCodeNames depending on the current translate mode.
   *
   *  Use putInternalMode() and putInterfaceMode() for details.
   */
  CodeNames *mCodeNames;

  //! C++ name translation cache for externally visible names
  /*! This table contains non-obfuscated translations of HDL names.
   */
  CodeNames *mInterfaceCodeNames;

  //! C++ name translation cache for internal names (default)
  /*! This table contains obfuscated translations of HDL names.
   */
  CodeNames* mInternalCodeNames;

  //! (outer) factory used to initialize inner one in class CompilationErrorScanner
  SourceLocatorFactory* mSourceLocatorFactory;

  //! Name cache for generating new names
  AtomicCache* mAtomicCache;

  enum TargetCompiler mTargetCompiler;
  enum TargetCompilerVersion mTargetCompilerVersion;

  //! Cache for expr synthesis
  ESPopulateExpr *mESPopulate;

  //! Fold optimization driver
  Fold *mFold;

  //! set of changed indices that have been written
  UtHashSet<int> mChangedWritten;

  //! GenSched object generator
  GenSchedFactory* mGenSchedFactory;

  //! Remember if we're generating 64-bit code
  bool m64;

  //! Are we obfuscating?
  bool mIsObfuscating;

  //! Are we *not* using SpeedCC
  bool mIsNoSpeedCC;

  //! Are we generating an onDemand model?
  bool mOnDemand;

  //! Does the type dictionary reference bitvectors?
  bool mGeneratingBVs;

  //! Value associated with -O flag (0,1,2,3 or 's')
  char mOptLevel;
  
  //! Set when a debuggable model is desired
  bool mDebuggableModel;

  /*! 
    Memories which use more bits than this threshold will be
    implemented at runtime as a SparseMemory instead of a Memory.
    
    The value of this parameter is computed from the -memoryCapacity
    switch unless the -memoryBitCapacity switch is overridden.
  */
  UInt32 mSparseMemoryBitCapacity;

  //! Largest width memory coded with a shared OOB value.
  UInt32 mMaxMemoryWidth;

  // For testdriver
  class PortOrderSort;

  // For emitting c-model port translations
  int mCModelCallIndex;
  UtString* mCModelCallName;

  // Keep the cgaux info for a net in a map, rather than in the NUNet,
  // so it doesn't affect memory during peak usage time between elaboration
  // and REAlias.
  typedef UtHashMap<const NUNet*, CGAuxInfo*> NetToCGAuxMap;
  NetToCGAuxMap* mNetToCGAuxMap;

  // Keep the cgaux info for the block scopes too
  typedef UtHashMap<const NUNamedDeclarationScope*, CGAuxInfo*> BlockToCGAuxMap;
  BlockToCGAuxMap* mBlockToCGAuxMap;

  RankNets * mRankNets;

  CGOFiles * mCGOFiles;

  // Keep track of what schedulable functions we have emitted.
  UtSet<const NUUseDefNode*> mEmittedFuncs;

#ifdef INTERPRETER
  typedef UtHashMap<STBranchNode*, UInt32> ModuleOffsetMap;
  ModuleOffsetMap* mModuleOffsetMap;
#endif
  int mBuiltinExpectValue;


  //! Set of hierarchical references which appear as port aliases.
  /*!
   * These hierarchical references cannot be considered as locally
   * relative because they need to be initialized prior to design
   * construction.
   */
  NUNetSet * mPortAliasedHierRefs;

  CGPortIface* mPortIface;

  FuncLayout* mLayout;

  //! Track current module being compiled
  const NUModule* mCurrentModule;

  //! Abstraction to keep track of temp memory nets that have to be declared
  typedef UtSet<const NUTempMemoryNet*> UndeclaredTemps;

  //! Data for the undeclared temps during a block
  UndeclaredTemps* mUndeclaredTemps;

  //! Compile-time profiling object
  CGProfile* mCGProfile;
  CGBucketID* mBucketID;        // Object for block buckets
  ScheduleBuckets* mSchedBuckets; // Schedule buckets
  int mTopLevelSchedID;         // Bucket index of carbon_design_schedule

  //! Induced error table for regression of annotations
  InducedFaultTable *mInducedFaults;

  //! Implemenation code annotation
  CodeAnnotationManager mHdlAnnotations;
  CodeAnnotationManager mImplAnnotations;

  //! Storage to keep track of all the c-model calls for a given interface
  /*! This storage is created and deleted for each NUModule as codegen
   *  walks the design.
   */
  NUCModelCalls* mCModelCalls;

  //! \class CompilationErrorScanner
  /*! Callbacks for error messages discovered in libdesign.codegen.errors 
   */
  /* \note At each gcc invocation, when error-lines are found in libdesign.codegen.errors
   * by CxxError(...), they are appended to a list of SourceLocator objects.
   * At the end of all processing by CodeGen::runMakeStep(...)
   * a shell script is generated from that list by buildSearchAnnotationsScript(...) to
   * allow examination of the annotations associated with lines that caused gcc errors.
   */
  class CompilationErrorScanner : public CodegenErrorLexer {
  public:

    CompilationErrorScanner (SourceLocatorFactory *sloc_factory,
			     MsgContext &msg_context,
                             const char *filename,
			     const UInt32 flags = 0) : 
      CodegenErrorLexer (msg_context, filename, flags), 
      mSlocFactory(sloc_factory){}

    virtual ~CompilationErrorScanner () {}

    //! Creates a shell script file to process gcc-form errors with 'searchannotations'
    void buildSearchAnnotationsScript ( UtString scriptFilePathAndName,
					MsgContext *msg_context );

    // callback for patterns recognised by the scanner
    virtual void MakeError (const char *, const UInt32, const char *);
    virtual void SpeedCC (const char *);
    virtual void CxxError (const char *, const UInt32, const char *);

  private:
    SourceLocatorFactory* mSlocFactory;

    // List of all the <file, line number> locations found by the CxxError call-back
    UtList <SourceLocator> mSlocList;

  };

  UtString mDebugScheduleName;          //!< identifier for debug_schedule
  UtString mDepositComboScheduleName;   //!< identifier for deposit_combo_schedule
  UtString mSaveFnName;                 //!< identifier for the save C function
  UtString mRestoreFnName;              //!< identifier for the restore C function
  UtString mOnDemandSaveFnName;         //!< identifier for the on-demand save C function
  UtString mOnDemandRestoreFnName;      //!< identifier for the on-demand restore C function

public:

  const char *getDebugScheduleName () const { return mDebugScheduleName.c_str (); }
  const char *getDepositComboScheduleName () const { return mDepositComboScheduleName.c_str (); }

};

// It's not enough to declare these as friends inside class CxxName.  They
// need to be generally visible.
UtOStream& operator << (UtOStream& f, const CodeGen::CxxBoundName& n);
UtString&  operator << (UtString&  s, const CodeGen::CxxBoundName& n);

#endif	// __codegen_h_

/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
