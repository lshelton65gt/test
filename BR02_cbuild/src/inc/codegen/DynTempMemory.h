// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _DYN_TEMP_MEMORY_H_
#define _DYN_TEMP_MEMORY_H_

#ifndef __UtHashMapFastIter_h_
#include "util/UtHashMapFastIter.h"
#endif

//! DynTempMemory template class
/*!
 * This template class implements a temporary memory object for implementing
 * non-blocking memory writes.  It keeps a hash-table of address/data pairs
 * which will get written into the real memory when temp goes out of scope.
 * 
 * In this implementation, an arbitrary number of writes may occur to the
 * memory within the block, so it is suitable for using when there are
 * non-blocking assigns to a memory from a for-loop.  For other cases
 * StaticTempMemory can be used.
 *
 * DynTempMemories can involve large numbers of pending writes
 * during initialization for example (see bug4814).  To handle this
 * more efficiently a hash_map is used to store the writes instead of
 * the linked list used for StaticTempMemory.
 */
template <class _Memory>
class DynTempMemory
{
public:
  typedef typename _Memory::value_type value_type;
  typedef UtHashMapFastIter<SInt32, value_type> Mem_map;
  typedef typename Mem_map::UnsortedCLoop UnsortedCLoop;
  typedef typename Mem_map::RemoveLoop RemoveLoop;
  typedef typename Mem_map::MapEntry MapEntry;
  typedef typename Mem_map::iterator Mem_map_it;
  typedef typename Mem_map::const_iterator Mem_map_const_it;

  DynTempMemory(const _Memory& src)
    : mBaseMemory (const_cast<_Memory*>(&src))
  {
  }

  DynTempMemory()
    : mBaseMemory (0)
  {
  }

  DynTempMemory(const DynTempMemory& src) :
    mBaseMemory(src.mBaseMemory)
  {
    *this = src;
  }

  //! Destructor
  ~DynTempMemory()
  {
  }

  SInt32 denormalize (SInt32 i) const {
    return mBaseMemory->denormalize(i);
  }

  SInt32 normalize (SInt32 i) const {
    return mBaseMemory->normalize(i);
  }

  //! Copy over both the data stored in the temp src, *and* the data
  //! stored in src's base-memory.
  /*!
   *! See codegen/testsuite/testSparseMemory.cxx, testTempCopies().
   *! Also the same routine in testMemory.cxx
   */
  DynTempMemory& operator=(const DynTempMemory& src) {
    if (&src != this) {
      mWrites.clear();
      for (SInt32 i = mBaseMemory->eMemLowerBound;
           i <= mBaseMemory->eMemUpperBound;
           ++i)
      {
        mWrites.insertValue(i, src.readNoCheck(i));
      }
    }
    return *this;
  }


  //! Assignment operator from the memory to the temp
  DynTempMemory& operator=(const _Memory& src) {
    // just visit the existing elements via the iterator would
    // cause the same issues as above.  
    for (SInt32 addr = _Memory::eMemLowerBound;
         addr <= _Memory::eMemUpperBound; ++addr)
    {
      SInt32 destaddr = mBaseMemory->denormalize(src.normalize(addr));
      mWrites.insertValue(destaddr, src.readNoCheck(addr));
    }
    return *this;
  }

  //! Assign from another static temp to this; assume same depth.
  template <class _TempMemory>
  DynTempMemory& operator=(const _TempMemory & src) {
    for (SInt32 destaddr = mBaseMemory->eMemLowerBound;
         destaddr <= mBaseMemory->eMemUpperBound;
         ++destaddr)
    {
      SInt32 srcaddr = src.denormalize(normalize(destaddr));
      mWrites.insertValue(destaddr, src.readNoCheck (srcaddr));
      // mWrites[i] = src.read (i);
    }
    return *this;
  }
    
  //! Function to assign locations of the memory.
  //! We never read from temp memories.
  value_type& write(SInt32 i)
  {
    MapEntry* me;
    if (mWrites.insertUninitialized(i, &me)) {
      // If a pending write exists, use that location, or initialize
      // the location from the base memory.  This is required to
      // handle partial writes to memory words.  Multiple partial
      // writes must use the data from any previous writes since
      // ordering matters.

      me->mKeyVal.first = i;
      me->mKeyVal.second = mBaseMemory->read(i);
    }
    return me->mKeyVal.second;
  } // value_type& write

  //! Function to assign locations of the memory.
  void writeValue(SInt32 i, const value_type& val) {
    // OOB-check?
    mWrites.insertValue(i, val);
  }

  //! Function to assign locations of the memory.
  void writeValueNoCheck(SInt32 i, const value_type& val) {
    mWrites.insertValue(i, val);
  }

  // Called for -no-OOB, but there's nothing to avoid here.
  value_type& writeNoCheck (SInt32 i)
  {
    return write (i);
  }

  //! Read from the master if we don't have a local value.
  const value_type& read(SInt32 i) const {
    MapEntry* me = mWrites.findEntry(i);

    if (me == NULL) {
      return mBaseMemory->read (i);
    }
    else {
      return me->mKeyVal.second;
    }
  }

  //! For now, we are not implementing the "no-check" versions of accessors, 
  //  since sparse memories are really slow anyways.
  const value_type& readNoCheck(SInt32 addr) const
  {
    return read(addr);
  }

  void copyQueuedWrites(DynTempMemory& src) {
    // Efficiently walk through the queued writes, removing them from src.
    for (RemoveLoop i(src.mWrites); !i.atEnd(); ++i) {
      mWrites[ i.getKey() ] = i.getValue();
    }
  }

  void executeQueuedWrites(bool update) const
  {
    // Efficiently walk through the queued writes, removing them
    for (RemoveLoop i(const_cast<Mem_map&>(mWrites)); !i.atEnd(); ++i) {
      if (update) {
	mBaseMemory->directWrite(i.getKey(), i.getValue());
      } else {
	mBaseMemory->write(i.getKey()) = i.getValue();
      }
    }
  } // void executeQueuedWrites

  void putBaseMemory (_Memory* src) { mBaseMemory=src; }
  const _Memory* getBaseMemory () const { return mBaseMemory; }

  UnsortedCLoop loopCUnsorted() const {return UnsortedCLoop(mWrites);}

private:
  _Memory* mBaseMemory;
  mutable Mem_map mWrites;
}; // class DynTempMemory

#endif
