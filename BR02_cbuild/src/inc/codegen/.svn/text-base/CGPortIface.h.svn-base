// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Declaration for an encapsulated PortInterface code generator.
*/

#ifndef _CGPortIface_h_
#define _CGPortIface_h_

#include "util/Iter.h"

class NUNetElab;
class CarbonExpr;
class UtString;
class StringAtom;
class UtOStream;
class STAliasedLeafNode;
class IODBNucleus;

//! Information holder for port nets
/*!
  I would have declared this inside CGPortIface, but I want to be able
  to forward declare the class and use it in headers.
*/
class CGPortIfaceNet
{
public:

  //! Enumeration for port attributes
  enum Flag {
    eNone = 0, //!< initializer
    eTri = 1, //!< Tristate net
    eBid = 2, //!< Bidirectional port
    eIn = 4, //!< 2 state Input port
    eOut = 8, //!< 2 state Output port
    eForcible = 0x10, //!< Forcible port
    eObserve = 0x20, //!< Marked as observable
    eSigned = 0x40, //!< Signed port
    eReal = 0x80, //!< real port (not possible currently)
    eComboRunNet = 0x100, //!< Causes combo run
    eDeposit = 0x200, //!< Marked as depositable
    eClock =   0x400, //!< net is a clock
    eReset =   0x800, //!< net is a reset
    e2State = 0x1000, //!< user directives suggest a 2-state wrapper port
    e4State = 0x2000 //!< user directives suggest a 4-state wrapper port
  };

  //! constructor
  CGPortIfaceNet(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, Flag extraFlags);
  
  //! Constructor for dead nets
  CGPortIfaceNet(const STSymbolTableNode* node, const NUNet* net, bool useElabName, Flag extraFlags);
 
  //! destructor
  ~CGPortIfaceNet() {}

  //! Get the width of the net
  UInt32 getWidth() const;
    
  //! Set the change index. Defaults to -1.
  void setChangeIndex(SInt32 index) { mChangeIndex = index; }
  //! Append a flag onto port attributes
  void addFlag(Flag flag) { mFlags = Flag(mFlags | flag); }
    
  //! Get the NUNetElab
  /*!
    This will be null for dead nets
  */
  NUNetElab* getNetElab() { return mNetElab; }
  //! Get the NUNetElab (const version)
  const NUNetElab* getNetElab() const { return mNetElab; }

  //! Get the STSymbolTableNode* associated with this net
  const STSymbolTableNode* getSymNode() const { 
    return mSymNode;
  }

  //! Get the NUNet
  const NUNet* getNUNet() const;

  //! Get the StringAtom of the NUNet
  /*!
    This will work for dead nets as well as regular nets
  */
  StringAtom* getNUNetName() const;

  //! Get the change index (-1, if no change index)
  SInt32 getChangeIndex() const { return mChangeIndex; }

  //! Does this have input direction (net is read)?
  bool hasInputDir() const { return isInput() || isBid() || isDeposit(); }
  //! Does this have output direction (net is written)?
  bool hasOutputDir() const { return isOutput() || isBid() || isTriOut() || isObserve(); }

  //! Is this a 2 state input port?
  bool isInput() const { return (mFlags & eIn) != 0; }
  //! Is this a 2 state output port?
  bool isOutput() const { return !isTri() && ((mFlags & eOut) != 0); }
  //! Is this a tristate net? (tristate out, tristate dep/ob)
  bool isTri() const { return (mFlags & eTri) != 0; }
  //! Is this a tristate output?
  bool isTriOut() const { return (isTri() && (mFlags & eOut) != 0); }
  //! Is this a bidirectional port?
  bool isBid() const { return (mFlags & eBid) != 0; }
  //! Is this a forcible port?
  bool isForcible() const { return (mFlags & eForcible) != 0; }
  //! Is this a marked deposit net?
  bool isDeposit() const { return (mFlags & eDeposit) != 0; }
  //! Is this a marked observe net?
  bool isObserve() const { return (mFlags & eObserve) != 0; }
  //! Is this port mapped to an expression?
  bool isExpressioned() const { return mExpr != NULL; }
  //! Is this port signed?
  bool isSigned() const { return (mFlags & eSigned) != 0; }
  //! Is this port real?
  bool isReal() const { return (mFlags & eReal) != 0; }
  //! Is this net pulled? (tristate/bidi)
  bool isPulled() const;
  //! Runs deposit/combo schedule?
  bool isRunCombo() const { return (mFlags & eComboRunNet) != 0; }
  //! Is this port a clock?
  bool isClock() const { return (mFlags & eClock) != 0; }
  //! Is this port a reset?
  bool isReset() const { return (mFlags & eReset) != 0; }
  //! is port to be exposed as 2-state?
  bool is2State() const { return (mFlags & e2State) != 0; }
  //! is port to be exposed as 4-state?
  bool is4State() const { return (mFlags & e4State) != 0; }
  //! is port not a real port?  
  /*!
    True for depositables and observables
    that are exposed by scDepositSignal or scObserveSignal
  */
  bool isNotAPort() const { return isDeposit() || isObserve(); }

  //! Get base member name for net
  /*!
   * Do not call this directly - just helper function for CGPortIface.
   * User CGPortInterface::getExternalValue, etc. to fetch port member names.
   */
  const char *getMemberName() const { return mMemberName.c_str(); }

  //! Get base name for net - like member name but w/o m_ prefix
  /*!
   * Do not call this directly - just helper function for CGPortIface.
   * User CGPortInterface::getExternalValue, etc. to fetch port member names.
   */
  const char *getGenerateName() const { return mGenerateName.c_str(); }

  //! This returns the full hierarchical name in Verilog form.
  const char* getFullName() const { return mFullName.c_str(); }

  //! Is this a dead net?
  bool isDead() const { return mNetElab == NULL; }

  //! Put the net's expression
  /*!
    This makes isExpression() true if expr is not NULL. 
  */
  void putExpression(const CarbonExpr* expr) {
    mExpr = expr;
  }
  
  //! Get the expression for this net
  /*!
    Returns null if this net is not an expression
  */
  const CarbonExpr* getExpression() const { return mExpr; }
  
private:
  NUNetElab* mNetElab;
  const STSymbolTableNode* mSymNode;
  const NUNet* mNet;
  const CarbonExpr* mExpr;
  Flag mFlags;
  SInt32 mChangeIndex;
  UtString mMemberName;
  UtString mGenerateName;
  UtString mFullName;

  void init(const STAliasedLeafNode* node, bool useElabName);
  
  // forbid
  CGPortIfaceNet();
  CGPortIfaceNet(const CGPortIfaceNet&);
  CGPortIfaceNet& operator=(const CGPortIfaceNet&);
};

//! A class to encapsulate code-generating port interface usage
/*!
  Wrappers use the generated port interface for fast-as-possible
  deposits and examines. To make the wrapper generation using the port
  interface homogeneous all member name generation, codegen-based
  direction, and other miscellaneous functionality is within this
  class.
  
  For example, an NUNet::isBid() returns true if the user specified it
  as a bid, except when port coercion is active. Codegen has a
  isBidirect() function that will tell us what codegen ultimately
  generated for the net.

  Also, the names of members for the port interface can be confusing
  because of split ports, tristates, and bidirects. Not to mention the
  types because the ports can be marked forcible.
*/
class CGPortIface
{
public:
  //! constructor
  CGPortIface();
  
  //! destructor
  ~CGPortIface();


  //! Net Iterator
  typedef Iter<CGPortIfaceNet*> NetIter;

  //! Loop all the nets that have input direction
  NetIter loopInputDir();

  //! Loop all the nets that have output direction
  NetIter loopOutputDir();

  //! Loop all nonports
  NetIter loopNonPorts();

  
  //! Loop all the 2 state inputs
  NetIter loopIns();
  //! Loop all the 2 state outputs
  NetIter loopOuts();
  //! Loop all the tristate outputs (not bidis)
  NetIter loopTriOuts();
  //! Loop all the bidirects
  NetIter loopBidis();
  //! Loop all dead inputs
  NetIter loopDeadIns();
  //! Loop top level ports, in order
  NetIter loopPrimaryPorts(IODBNucleus *db);

  //! Get the member name for the change array
  const char* getChangeArray() const;

  //! Get the member name for the runcombo pointer.
  const char* getRunComboPtr() const;
  
  //! Get external data member name
  /*!
    This represents the external value of the net.

    This will return the member name for a normal 2 state net. It will
    return and name + _xdata for a bidirect or tristate. Otherwise,
    just name where name is a c++ class member name.
  */
  const char* getExternalValue(const CGPortIfaceNet* net, UtString* buf);

  //! Get internal data member name
  /*!
    Only applicable to bidirects. Asserts if the net is not a bidirect
    
    This represents the internal value of the net, meaning what the
    model sees the value as.

    Will return name + _idata;
  */
  const char* getInternalValue(const CGPortIfaceNet* net, UtString* buf);

  //! Get external drive member name
  /*!
    Only applicable to bidirects. Asserts if the net is not a bidirect
    
    This represents the external drive of the net, meaning whether or
    not the outside world is driving the net.

    Will return name + _xdrive;
  */
  const char* getExternalDrive(const CGPortIfaceNet* net, UtString* buf);

  //! Get internal drive member name
  /*!
    Only applicable to bidirects and tristate outputs. Asserts if the
    net is neither a bidirect nor a tristate output.
    
    This represents the internal drive of the net, meaning whether or
    not the model is driving.

    Will return name + _idrive;
  */
  const char* getInternalDrive(const CGPortIfaceNet* net, UtString* buf);

  //! Get force mask member name
  /*!
    Only applicable to forcible nets. This will assert if the net is
    not forcible.
    
    This represents the bits of the net that are forced and thus
    Only applicable to forcible nets cannot be changed.

    Will return name + _forcemask;
  */
  const char* getForceMask(const CGPortIfaceNet* net, UtString* buf);


  //! Add input
  /*!
    All add functions add the attributes to the Net that is passed
    back.
  */
  CGPortIfaceNet* addIn(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags);

  //! Add an unused input
  /*!
    This is only needed for nets that do not have an NUNetElab and are
    aliased to something already handled in the live list. For
    example, collapseClocks will remove the NUNetElabs for the slave
    clocks in the design port list and replace them with the
    master. We need the actual port name.
  */
  CGPortIfaceNet* addDeadIn(const STSymbolTableNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags);

  //! Add output
  /*!
    All add functions add the attributes to the Net that is passed
    back.
  */
  CGPortIfaceNet* addOut(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags);

  //! Add tristate output
  /*!
    All add functions add the attributes to the Net that is passed
    back.
  */
  CGPortIfaceNet* addTriOut(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags);
  //! Add bidi
  /*!
    All add functions add the attributes to the Net that is passed
    back.
  */
  CGPortIfaceNet* addBidi(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags);


  //! Get the interface C type for the given net
  const char* getCType(const CGPortIfaceNet* net, UtString* buf) const;

  //! Returns whether or not this net should be a CarbonNetID*
  /*!
    This is systemc specific. We don't support forcible, expressioned,
    or deposit nets that are only outputs in the design.
  */
  static bool sIsNetHandle(const CGPortIfaceNet* net);

private:
  //! helper class to hide all the vectors and maps
  struct Helper;
  Helper* mHelper;

  void addCommonFlags(CGPortIfaceNet* net);

  //! Accessor to get interface type name
  /*!
    This returns UInt1->UInt64 and UInt32 for > 64 bits
  */
  const char* getIfaceType(const NUNet* net,  UtString* buf) const;

};

#endif
