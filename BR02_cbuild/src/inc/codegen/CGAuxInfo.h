// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 
  
#ifndef CGAUXINFO_H_
#define CGAUXINFO_H_
#include "util/CarbonAssert.h"

//! CodeGen operand attributes
enum OpFlags
  {
    eOpAccessor	= 0x01,		/*!< Accessor function available */
    eOpMember 	= 0x02,		/*!< Member variable exists */
    eOpReferenced=0x04,	   	/*!< Generated code referencing this Net */
    eOpVCDSymbol= 0x08,		/*!< Symbol is addressable in debug symtab */
    eOpDereferenced=0x10,	/*!< Symbol is a reference variable */
    eOpPortAliased=0x20,	/*!< Symbol is aliased to lower-level member */
    eOpPortAllocated=0x40,	/*!< Allocation is present at a lower-level member */
    eOpStaticAssign=0x80,	/*!< Symbol is constant valued */
    eOpUnused=0x100,		/*!< Member variable unused (and undefined!) */
    eOpFormal=0x200,		/*!< Member initialized via constructor */
    eOpAllocated=0x400,		/*!< Member has valid offset */
    eOpConstructorParam=0x800,	/*!< Member is initialized via constructor */
    eOpVhdlRecordPort=0x1000,   /*!< Member is a VHDL record port */
    eOpUsedAccessor = 0x2000    /*!< Accessor function was used */
  };

//! Decorate Nets with CodeGen specific information
/*! Auxilliary operand information for code-generation.  Used to track
  how NUNets are translated and compute debugging information.
 */
class CGAuxInfo
{
private:
  //! Offset within module's translation of the class member
  //associated with the net
  size_t mOffset;

  //! Type Tag
  int mDictionaryType;

  //! Flags to remember how emitCode() processed this
  OpFlags mFlags;

public:
  //! Trivial constructor
  CGAuxInfo (): mOffset (0), mDictionaryType (0), mFlags (OpFlags (0)) {}

  //! Default destructor
  ~CGAuxInfo(){};

  //! Return the offset (relative to the immediately containing class)
  //of this symbol
  size_t getOffset () const {
    INFO_ASSERT (mFlags & eOpAllocated, "Offset not set"); //Only have an offset if it was set
    return mOffset;
  }

  //! Set calculated byte-offset of class member
  void setOffset (size_t o) {
    INFO_ASSERT (! (mFlags & eOpAllocated), "Offset already set");// Can't allocate a net twice.
    mOffset = o;
    mFlags = OpFlags (mFlags | eOpAllocated);
  }

  //! Access the operand flags
  OpFlags getFlags () const {return mFlags;}

  //! Does a get_<NET> function exist for this net
  bool hasAccessor () const {return mFlags & eOpAccessor;}

  //! Did we code a reference to the get_<NET> function?
  bool needsAccessor () const {return (mFlags & eOpUsedAccessor) !=0 ;}

  //! Indicate we called the accessor
  void usedAccessor () { mFlags = OpFlags (mFlags | eOpUsedAccessor); }

  //! Does a member variable exist for this Net?
  bool hasMember () const {return mFlags & eOpMember;}

  //! Does the net have a computed offset (implying it's allocated)
  bool hasOffset () const {return (mFlags & eOpAllocated) != 0; }

  //! Is this net unreferenced
  bool isUnreferenced () const {return (mFlags & eOpReferenced) == 0;}

  bool isReferenced () const {return (mFlags & eOpReferenced) != 0;}

  //! Should this net be given a Type ID and Offset in the symbol table?
  bool isVCDSymbol () const { return mFlags & eOpVCDSymbol;}

  //! Symbol is a reference variable (T& v) from an output parameter list
  bool isDereferenced () const { return mFlags & eOpDereferenced; }

  //! Symbol is aliased to a lower-level member
  bool isPortAliased () const { return mFlags & eOpPortAliased; }

  //! Allocation is present at a lower-level member
  bool isPortAllocated () const { return mFlags & eOpPortAllocated; }

  //! Symbol is a VHDL record member
  bool isVhdlRecordPort () const { return mFlags & eOpVhdlRecordPort; }

  //! Symbol is initialized via constructor and has a_FOO form
  bool hasFormal () const { return mFlags & eOpFormal; }

  //! Mark as having a formal
  void setFormal () { mFlags = OpFlags (mFlags | eOpFormal); }

  //! Symbol is hardwired to a constant value.
  bool isStaticAssign () const {return mFlags & eOpStaticAssign; }

  //! Is symbol unreferenced in code?
  bool isUnused () const { return mFlags & eOpUnused; }

  //! Is this port expected to be initialized by a constructor?
  bool isConstructed () const { return (mFlags & eOpConstructorParam) != 0;}

  //! Set as initialized via constructor
  void setConstructed () { mFlags = OpFlags (mFlags | eOpConstructorParam);}

  //! Mark as unused in code
  void setUnused () { mFlags = OpFlags (mFlags | eOpUnused); }

  //! Mark as aliased
  void setPortAliased () { mFlags = OpFlags (mFlags | eOpPortAliased);}

  //! Mark that allocation occurs underneath.
  void setPortAllocated () { mFlags = OpFlags (mFlags | eOpPortAllocated);}

  //! Mark that this symbol is a vhdl record member used as a port
  void setVhdlRecordPort () { mFlags = OpFlags (mFlags | eOpVhdlRecordPort);}

  //! Mark as dereferenced output param
  void setDereferenced () { mFlags = OpFlags (mFlags | eOpDereferenced); }

  //! Mark as suitable for debug info
  void setVCDSymbol () { mFlags = OpFlags (mFlags | eOpVCDSymbol); }

  //! We have a get_XXX() method for this NET
  void setAccessor () {mFlags = OpFlags (mFlags | eOpAccessor);}

  //! We have a member variable m_XXX for this NET.
  void setMember () {mFlags = OpFlags (mFlags | eOpMember);}

  //! This NET was used in some expression
  void setReferenced ();

  //! Mark as statically allocated.
  void setStaticAssign () {mFlags = OpFlags (mFlags | eOpStaticAssign);}

  //! Update all the code-generation flags.
  void setFlags (OpFlags f) { mFlags = f; }

  //! Set the index into the type dictionary
  void setTypeID (int i) {mDictionaryType = i;}

  //! Get the index into the type dictionary
  int getTypeID () const {return mDictionaryType; }
};

#endif
/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
