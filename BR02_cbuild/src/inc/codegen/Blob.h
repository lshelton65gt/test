// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 
/*!\file
  This file defines Blob objects, the granularity at which block
  profiling sampling buckets are defined.
  Blobs are always blocks, continuous assigns, tasks/functions, C Models,
  UDP tables, and primitive gate instances */
  
#ifndef __Blob_h_
#define __Blob_h_

#include <utility>              // For std::pair
#ifndef NUUSEDEFNODE_H_
#include "nucleus/NUUseDefNode.h" // For NUType
#endif
#ifndef SOURCELOCATOR_H_
#include "util/SourceLocator.h"
#endif
#ifndef __UtString_h_
#include "util/UtString.h"
#endif

//! Blob class
/*!
 * A blob is a starting location, construct type, and bucket ID.
 *
 * When a blob is created its constructor assigns its bucket ID.
 */
class Blob
{
public:
  //! Constructor.
  Blob(const SourceLocator& loc, const NUBase* node, const NUModule* parent);
  //! Constructor for the non-Blob Blob.
  Blob(const SourceLocator& loc, const char* type);
  //! SourceLocator accessor
  const SourceLocator& getLoc() const { return mSourceLoc; }
  //! Type accessor
  const char* getTypeStr() const { return mTypeStr.c_str(); }
  //! Bucket accessor
  int getBucket() const { return mBucket; }
  //! Bucket accessor
  void setBucket(int id) { mBucket = id; }
  //! Get the parent module for this blob (NULL for non-blobs)
  const NUModule* getParent() const { return mParent; }
private:
  //! The location of the start of the blob.
  SourceLocator mSourceLoc;
  //! The bucket allocated for this blob.  -1 means not allocated.
  int mBucket;
  //! The type of the nucleus node.
  UtString mTypeStr;
  //! The parent module for this Blob (or NULL for non-blobs)
  const NUModule* mParent;
};


class CodeGen;

//! BlobMap class
/*!
 * Mapping from a statement's source locator to its enclosing blob.
 *
 * This could be implemented as a standard map (UtHashMap) of tuples
 * from all statements to their blobs, but blobs are naturally
 * inserted in order, and all statements (keys) with a given blob
 * (value) are adjacent, so that would store lots of redundant
 * information.
 * Instead, for each file we can use a vector of (SourceLocator, Blob)
 * pairs and binary search to find the last blob starting before the
 * given statement.
 */
class BlobMap
{
public:
  //! Constructor
  BlobMap(CodeGen* cg);
  //! Destructor
  ~BlobMap();
  //! Find the blob containing the given statement.
  /*!
   * That's the one in the same file with the greatest line number
   * less than or equal to the statement's.
   */ 
  const Blob* lookup(SourceLocator stmtLoc);
  //! Add a new blob
  void insert(const NUUseDefNode* node, const NUModule* parent);

  void findBlobs(NUDesign* design);
private:
  // Would it be better to store a Blob rather than Blob* in BlobPair?
  typedef std::pair<UInt32, const Blob*> BlobPair;

  // BlobSet is used while inserting Blobs, VecMap is used when
  // generating code to look up source locators to find the owning
  // Blob.

  // BlobSet contains only the first Blob for a given SourceLocator.
  // This avoids storing uniquified modules multiple times.  If a
  // perverse coder defines multiple blobs on one long line, only the
  // first one will be defined as a blob.  BlobSet is sorted, so when
  // done finding Blobs they can be copied to VecMap in order.

  // Comparison functor for BlobSet.
  struct BlobCompare {
    bool operator()(const Blob* b1, const Blob* b2) const {
      return b1->getLoc() < b2->getLoc();
    }
  };      
  typedef UtSet<const Blob*, BlobCompare> BlobSet;
  //! Set for storing blobs while finding them.
  BlobSet* mBlobSet;

  // VecMap is a map with one entry for each source file.  Each entry
  // is a vector of BlobPair objects, which contain {line_number,
  // Blob*}.  This allows looking up the owning Blob of a
  // SourceLocator by doing a binary search of the vector.

  typedef UtVector<BlobPair> BlobVec;
  typedef UtHashMap<UtString, BlobVec> VecMap;
  //! Map for looking up blob owning a SourceLocator.
  VecMap mBlobs;

  const Blob* binarySearch(const BlobVec& vec, UInt32 line);
  //! A Blob for SourceLocators that have no Blob. 
  Blob* mNonBlob;

  CodeGen* mCodeGen;
};


#endif // __Blob_h_
