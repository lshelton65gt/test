// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/* Template class to support memories */
/*!
  \file

  This class is a template that overloads the '[]' operator to hide
  the actual implementation of a memory. For now it only does static
  size arrays.
*/
//#define DEBUG_ACCESS

#ifndef __memory_h_
#define __memory_h_

#ifndef __carbon_priv_h_
#include "codegen/carbon_priv.h"
#endif

#ifndef MULTI_THREAD_SUPPORT
#ifndef __MemManager_h_
#include "util/MemManager.h"
#endif
#endif

#ifndef __BitVector_h_
#include "util/BitVector.h"
#endif

#ifdef MEMORYPERF
#ifndef __MemoryPerf_h_
#include "util/MemoryPerf.h"
#endif
#define MEMC(N,w,d)  MemPC->count##N(w,d);
#else
#define MEMC(N,w,d)
#endif

#ifndef __MemoryAccess_h_
#include "codegen/MemoryAccess.h"
#endif

#ifndef _CARBON_TYPE_CREATE_H_
#include "shell/carbon_type_create.h"
#endif

#ifndef _DYN_TEMP_MEMORY_H_
#include "codegen/DynTempMemory.h"
#endif

#ifndef _STATIC_TEMP_MEMORY_H_
#include "codegen/StaticTempMemory.h"
#endif

// Memory callbacks are needed for both replay and onDemand
#if defined(CARBON_REPLAY) || defined(CARBON_ONDEMAND)
#define CARBON_MEM_WRITE_CB
#endif

template <int _NumPorts, class _MemType, int _Width, int _MSW, int _LSW>
class StateUpdateMemory;
template <class _MemType, int _Width, int _MSW, int _LSW>
class DynStateUpdateMemory;

//! Memory template class
/*!
 * This template class implements a memory object. It allows reading
 * and writing the memory from the Carbon Model or from the Shell
 * API.
 */
template <class _MemType, int _Width, SInt32 _MSW, SInt32 _LSW >
class Memory : MemoryAccess < _MemType, ((_Width > 64) ? eMemBitVector
                                        : (_Width > 32) ? eMemPODLong : eMemPOD) >
{
 public:
  enum {                        // computed attributes of a memory
    eBitsPerWord = (8 * sizeof(UInt32)),
    eWordWidth = (_Width + eBitsPerWord - 1) / eBitsPerWord,
    eMemUnitSize = eWordWidth * sizeof(UInt32),
    eMemUpperBound = (_MSW >= _LSW) ? _MSW : _LSW,
    eMemLowerBound = (_MSW >= _LSW) ? _LSW : _MSW
  };

 private:
  //! Hide copy constructors
  Memory(const Memory&);

  // A function to test if an address is in range
  bool inRange(SInt32 address) const
  {
    if (CARBON_NO_OOB)
      return true;
    else if (_MSW > _LSW)
      return ((address >= _LSW) && (address <= _MSW));
    else
      return ((address >= _MSW) && (address <= _LSW));
  }

  //! The memory data
  _MemType* mMemory;

  static inline UInt32 ComputeDepth (SInt32 msw, SInt32 lsw) {
    UInt32 width;
    if (msw > lsw)
      width = msw - lsw;
    else
      width = lsw - msw;
    width += 1;
    return width;
  }

  inline UInt32 numAllocatedRows() const {
    return ComputeDepth(_MSW,_LSW);
  }

  //! Return access to a specified index in memory, does bounds checking.
  _MemType& access (SInt32 i) const {
    if (EXPECTED_VALUE(inRange(i), 1))
      return accessNoCheck (i);

    // OOB case
    REPORT_MEM_OOB ("memory", "", _MSW, _LSW, i);
    return * static_cast<_MemType*>((void*)&gCarbonMemoryBitbucket);
  }

  ShellMemoryCBManager*  mCallbacks;

#ifdef CARBON_MEM_WRITE_CB
  void reportWriteAddress(SInt32 address) {
    carbonInterfaceMemoryCBManagerReportAddress(mCallbacks, address);
  }
  void reportAllAddresses() {
    SInt32 upper = _MSW;
    SInt32 lower = _LSW;
    if (upper < lower )
    {
      lower = _MSW;
      upper = _LSW;
    }
    for (SInt32 addr = lower; addr <= upper; ++addr)
      reportWriteAddress(addr);
  }
#else
  void reportWriteAddress(SInt32) {}
  void reportAllAddresses() {}
#endif
  
public:
  /*!
   * The actual memory is stored as a dynamically allocated array.
   */
  Memory()
  {
    MEMC(Constructed,_Width,ComputeDepth(_MSW,_LSW));
    UInt32 numRows = numAllocatedRows();
    mMemory = (_MemType*) CARBON_ALLOC_VEC(_MemType, numRows);
    memset(mMemory, 0, sizeof(_MemType)*numRows);
#ifdef CARBON_MEM_WRITE_CB
    mCallbacks = carbonInterfaceAllocMemoryCBManager();
#else
    mCallbacks = NULL;
#endif
  }
  ~Memory()
  {
    CARBON_FREE_VEC(mMemory, _MemType, numAllocatedRows());
#ifdef CARBON_MEM_WRITE_CB
    carbonInterfaceFreeMemoryCBManager(mCallbacks);
#endif
  }

  typedef _MemType value_type;

  ShellMemoryCBManager* getCallbackArray() {
    return mCallbacks;
  }

  SInt32 denormalize (SInt32 i) const {
    SInt32 index;
    if (_MSW >= _LSW)
      index = i + _LSW;
    else
      index = getDepth () + (_MSW - 1) - i;
    return index;
  }

  SInt32 normalize (SInt32 i) const {
    SInt32 index;
    if (_MSW >= _LSW)
      index = (i - _LSW);
    else
      index = getDepth () - 1 - (i - _MSW);
    return index;
  }

  //! Return access to a specified index in memory, assumes index is in-bounds.
  /*!
    Used directly by the shell. Hence, this is a public function.
  */
  _MemType& accessNoCheck (SInt32 i) const {
    // Convert [MSW..LSW] into zero-based access
    SInt32 index = normalize (i);
 
    // The index should not be out of range because we test for that.
#ifdef DEBUG_ACCESS
    printf("mMemory = %p, this = %p, &mMemory[i] = %p, address = %x, index = %x\n",
           mMemory, this, &mMemory[index], i, index);
#endif
    return (_MemType&)mMemory[index];
  }

  //! Function to get the absolute depth of the memory
  inline UInt32 getDepth() const { return ComputeDepth(_MSW,_LSW); }

  //! Function to get the msw of the memory
  inline SInt32 getMSW() const { return _MSW; }

  //! Function to get the lsw of the memory
  inline SInt32 getLSW() const { return _LSW; }

  //! Write access to a memory location
  _MemType& write(SInt32 i) { 
    MEMC(Writes,_Width,ComputeDepth(_MSW,_LSW));
    reportWriteAddress(i);
    return access (i); 
  }

  //! Write a value into an address
  /*!
   *! In sparse memories, this is more efficient than getting a
   *! validated, initialized memory reference, because we can
   *! construct the memory entry with the correct value in the
   *! first place.  I don't think this makes much difference for
   *! Memory, but I am trying to keep parallel implementations.
   *!
   *! At some point the Memory/SparseMemory implementations should
   *! be refactored better.
   */
  void writeValue(SInt32 addr, const value_type& val) {
    if (EXPECTED_VALUE(inRange(addr), 1)) {
      reportWriteAddress(addr);

      // Convert [MSW..LSW] into zero-based access
      SInt32 index = normalize (addr);
#ifdef DEBUG_ACCESS
      printf("mMemory = %p, &mMemory[%d] = %p, addr = %x, index = %x, val=%x\n",
             mMemory, index, &mMemory[index], addr, index, val);
#endif
      mMemory[index] = val;
    }
    else {
      REPORT_MEM_OOB ("memory", "", _MSW, _LSW, addr);
    }
  }

  //! no-oob version of writeValue
  void writeValueNoCheck(SInt32 addr, const value_type& val) {
    reportWriteAddress(addr);
    // Convert [MSW..LSW] into zero-based access
    SInt32 index = normalize (addr);
    mMemory[index] = val;
  }
  
  //! Write access to a memory location, but index is not checked for bounds
  _MemType& writeNoCheck(SInt32 i) { 
    MEMC(Writes,_Width,ComputeDepth(_MSW,_LSW));
    reportWriteAddress(i);
    return accessNoCheck (i); 
  }

  //! Overload read access
  const _MemType& read(SInt32 i) const { 
    MEMC(Reads,_Width,ComputeDepth(_MSW,_LSW));
    return access (i); 
  }

  //! Read access, but index is not checked for bounds
  const _MemType& readNoCheck(SInt32 i) const { 
    MEMC(Reads,_Width,ComputeDepth(_MSW,_LSW));
    return accessNoCheck (i); 
  }

 //! Function to explicitly write a location in memory
  void directWrite(SInt32 addr, const _MemType& val) {
    MEMC(Writes,_Width,ComputeDepth(_MSW,_LSW));
    writeValue(addr, val);
  }
 
  //! write a row of memory
  void writeLocation(SInt32 address, const UInt32* data)
  {
    MEMC(Writes,_Width,ComputeDepth(_MSW,_LSW));
#ifdef DEBUG_ACCESS
    for (UInt32 i = 0; i < eWordWidth; ++i)
      printf("write: mem[%x][%d] <= %x\n", address, i, data[i]);
#endif
    writeRow(write(address), data);
  }

  //! read a row of memory. Used by shell. 
  /*!
    The address range has already been checked.
  */
  void readLocation(SInt32 address, UInt32* data) const
  {
    MEMC(Reads,_Width,ComputeDepth(_MSW,_LSW));
    readRow(readNoCheck(address), data);
    
#ifdef DEBUG_ACCESS
    for (UInt32 i = 0; i < eWordWidth; ++i)
      printf("read: mem[%x][%d] => %p:%x\n", address, i, &(read(address)), data[i]);
#endif
  }

  // Shell hooks. The shell has pointers to these static functions and
  // calls them for writes/read on rows > 64 bits.
  static void sMemWriteRow(SInt32 address, const UInt32* data, void* mem)
    {
      Memory<_MemType, _Width, _MSW, _LSW>* me = (Memory<_MemType, _Width, _MSW, _LSW>*) mem;
      // use accessNoCheck not writeNoCheck, so we don't call the
      // memory callback in the shell.
      _MemType& loc = me->accessNoCheck(address);
      me->writeRowClean(loc, data, _Width);
    }

  static void sMemWriteRowWord(SInt32 address, const UInt32* data, int index, void* mem)
    {
      Memory<_MemType, _Width, _MSW, _LSW>* me = (Memory<_MemType, _Width, _MSW, _LSW>*) mem;
      // use accessNoCheck not writeNoCheck, so we don't call the
      // memory callback in the shell.
      _MemType& loc = me->accessNoCheck(address);
      me->writeRowWordClean(loc, data, index, _Width);
    }

  static void sMemWriteRowRange(SInt32 address, const UInt32* data, int index, int length, void* mem)
    {
      Memory<_MemType, _Width, _MSW, _LSW>* me = (Memory<_MemType, _Width, _MSW, _LSW>*) mem;
      // use accessNoCheck not writeNoCheck, so we don't call the
      // memory callback in the shell.
      _MemType& loc = me->accessNoCheck(address);
      me->writeRowRange(loc, data, index, length);
    }

  static void sMemReadRow(SInt32 address, UInt32* data, void* mem)
    {
      Memory<_MemType, _Width, _MSW, _LSW>* me = (Memory<_MemType, _Width, _MSW, _LSW>*) mem;
      const _MemType& loc = me->readNoCheck(address);
      me->readRow(loc, data);
    }

  static void sMemReadRowWord(SInt32 address, UInt32* data, int index, void* mem)
    {
      Memory<_MemType, _Width, _MSW, _LSW>* me = (Memory<_MemType, _Width, _MSW, _LSW>*) mem;
      const _MemType& loc = me->readNoCheck(address);
      me->readRowWord(loc, data, index);
    }

  static void sMemReadRowRange(SInt32 address, UInt32* data, int index, int length, void* mem)
    {
      Memory<_MemType, _Width, _MSW, _LSW>* me = (Memory<_MemType, _Width, _MSW, _LSW>*) mem;
      const _MemType& loc = me->readNoCheck(address);
      me->readRowRange(loc, data, index, length);
    }
  
  static void* sMemRowStoragePtr(SInt32 address, void* mem)
    {
      Memory<_MemType, _Width, _MSW, _LSW>* me = (Memory<_MemType, _Width, _MSW, _LSW>*) mem;
      _MemType& loc = me->accessNoCheck(address);
      void* storagePtr = me->getStoragePtr(loc);
      return storagePtr;
    }
  
  
  CarbonMemWriteRowFn getWriteRowFn() {
    return sMemWriteRow;
  }

  CarbonMemReadRowFn getReadRowFn() {
    return sMemReadRow;
  }

  CarbonMemWriteRowWordFn getWriteRowWordFn() {
    return sMemWriteRowWord;
  }

  CarbonMemReadRowWordFn getReadRowWordFn() {
    return sMemReadRowWord;
  }

  CarbonMemWriteRowRangeFn getWriteRowRangeFn() {
    return sMemWriteRowRange;
  }

  CarbonMemReadRowRangeFn getReadRowRangeFn() {
    return sMemReadRowRange;
  }
  
  CarbonMemRowStoragePtrFn getRowStoragePtrFn() {
    return sMemRowStoragePtr;
  }
  
  //! Function to initialize the memory from a readmemX file.
  bool readmem(HDLReadMemX* readmemx)
  {
    if (!carbonInterfaceReadMemOpenFile(readmemx))
      return false;

    SInt64 address;
    UInt32 data[eWordWidth];
    while (carbonInterfaceReadMemGetNextWord(readmemx, &address, data))
    {
      writeLocation((SInt32)address, data);
    }

    carbonInterfaceReadMemCloseFile(readmemx);

    // Return value is not used by generated model
    return true;
  }


  //! Assignment not allowed in verilog, but is defined for VHDL

  //! template to allow memory of different ranges.
  template <class _Mem>
  Memory& operator=(const _Mem& src) {
    // report all addresses as being written
    reportAllAddresses();
    ::memcpy (mMemory, src.getStorage (), sizeof(_MemType) *  ComputeDepth (_MSW, _LSW));
    return *this;
  }

  //! need this one to override any default operator=() method because
  /*! bitwise copying of any object with a pointer is WRONG.  Or, BitVector 
   * of the same range will copy the pointe and end up pointing to the same 
   * address.
   */
  Memory& operator=(const Memory& src) {
    // report all addresses as being written
    reportAllAddresses();
    ::memcpy (mMemory, src.getStorage (), sizeof(_MemType) *  ComputeDepth (_MSW, _LSW));
    return *this;
    }

  template <class Mem>
    void slowCopy (const Mem& src) {
    const SInt32 msw = src.getBaseMemory()->getMSW();
    const SInt32 lsw = src.getBaseMemory()->getLSW();
    SInt32 lower, upper;
    if ( msw > lsw )
    {
      lower = lsw;
      upper = msw;
    }
    else
    {
      lower = msw;
      upper = lsw;
    }
    for (SInt32 addr = lower; addr <= upper; ++addr)
    {
      SInt32 destaddr = denormalize(src.normalize(addr));
      writeValueNoCheck (destaddr, src.readNoCheck (addr));
    }
  }

  //! shortcut for a dynamic memory
  typedef DynTempMemory<Memory> TempMem;

  //! Assignment operator from a dynamic temp-memory, to update master
  Memory& operator=(const TempMem& src) {
    const Memory* master = src.getBaseMemory ();
    if (master == this)
      src.executeQueuedWrites(false);
    else
      slowCopy (src);

    return *this;
  }

  //! Memory equalities...

  //! Convert small memories into an integer.
  /*! This method assumes that the entire memory fits into _Int. It is used in
   *  codegen for binary operators when one operand is an entire memory, the
   *  other operand is an integer type and the entire memory can be packed into
   *  an _Int instance. It is prudent to precede generation of an
   *  uncheckedCoerce with an assertion that the operand size fits into the
   *  integer.
   */
  template <typename _Int, UInt32 _size>
    _Int uncheckedCoerce () const
    {
      _Int value = 0;
      for (SInt32 i = ComputeDepth(_MSW,_LSW) - 1; i >= 0; --i) {
        value <<= _Width;
        value |= mMemory [i];
      }
      return value;
    }

  template <typename _Int>
  _Int coerce() const
  {
    _Int value = 0;
    for (SInt32 i = ComputeDepth(_MSW,_LSW) - 1; i >= 0; --i) {
      value <<= _Width;
      value |= mMemory [i];
    }
    return value;
  }

  // Create a byte array of the passed-in memory.
  // It is assumed that the passed-in bit vector is initialized to 0 and is of appropriate
  // size.
  // We are creating a byte array because the memory word may be of any number of types
  // (pods, bitvectors).
  // By consolidating into a byte array, this makes the memory into a consistent form where
  // we can then compare two memories which may have different word types.
  void populateByteArray(UInt8* bytes) const
  {
    UInt64 cur_index = 0;
    for (UInt32 i = 0; i < getDepth(); ++i) {
      // Process the word bit-by-bit so that we don't have to specialize the code
      // for all the various legal word types.
      BitVector<_Width,false> m(mMemory[i]);
      for (UInt32 p = 0; p < _Width; ++p) {
        UInt64 cur_word_index = cur_index / 8;
        UInt64 cur_bit_index = cur_index % 8;
        UInt8 bit_val = m.test(p) ? 1 : 0;
        bytes[cur_word_index] = bytes[cur_word_index] | (bit_val << cur_bit_index);
        ++cur_index;
      }
    }
  }

  // Catchall
  template <class _Mem>
  bool operator==(const _Mem& src) const {

    if ((getStorageSize() != src.getStorageSize()) || (getDepth() != src.getDepth())) {
      // Check for memories that have the same size but different shape (i.e. 3X2 vs. 3X2)
      if (getActualSize() != src.getActualSize()) {
	return false;
      } else {
        // Convert the memories into byte arrays, then compare them.
        // Need to do this because the arrays may be of arbitrary word type.
        UInt64 num_bits = (_Width * ((_MSW>_LSW)?(_MSW-_LSW+1):(_LSW-_MSW+1)));
        UInt64 byte_size = (num_bits + 7) / 8;
        UInt8* bv1 = CARBON_ALLOC_VEC(UInt8, byte_size);
        UInt8* bv2 = CARBON_ALLOC_VEC(UInt8, byte_size);
        memset(bv1, 0, byte_size);
        memset(bv2, 0, byte_size);
        populateByteArray(bv1);
        src.populateByteArray(bv2);
        bool same = (memcmp(bv1, bv2, byte_size) == 0);
        CARBON_FREE_VEC(bv1, UInt8, byte_size);
        CARBON_FREE_VEC(bv2, UInt8, byte_size);
        return same;
      }
    }

    for (UInt32 i = 0; i < ComputeDepth(_MSW,_LSW); ++i) {

#ifdef DEBUG_ACCESS
      printf("cmp: mMemory[%d]=%p, src.getStorage()[%d]=%p\n",
	     i, &(mMemory[i]), i, &(src.getStorage()[i]));
      UInt32 * mem1=(UInt32 *)&(mMemory[i]);
      UInt32 * mem2=(UInt32 *)&(src.getStorage()[i]);
      if (_Width > eBitsPerWord) {
	for (UInt32 j = 0; j < eWordWidth; ++j)
	  printf("mMemory[%d][%d]=%x, src.getStorage()[%d][%d]= %x\n",
		 i, j, mem1[j], i, j, mem2[j]);
      }
      else
	  printf("mMemory[%d]=%x, src.getStorage()[%d]= %x\n",
		 i, mem1[0], i, mem2[0]);
#endif

      if ( mMemory [i] != src.getStorage()[i]){
	return false;
      }
    }
    return true;
  }

  //! Memory inequality
  template <class _Mem>
    bool operator!=(const _Mem& src) const {
    return !(*this == src);
  }

  // comparison between a Memory and an associated dynamic temp memory
  template <class Mem>
    bool operator==(const DynTempMemory<Mem>& src) const {
   
    const Memory* other = src.getBaseMemory ();

    if (this != other) {
      // comparing a temp against some memory that's not its parent.
      if (getStorageSize () != other->getStorageSize ()
          || getDepth () != other->getDepth ())
        return false;

      // compare ALL the members
      for (UInt32 i = 0; i < ComputeDepth (_MSW,_LSW); ++i)
        if (src.readNoCheck (i) != readNoCheck (i))
          return false;

      return true;
    }

    // Since this temp is comparing against its master, we only need to look
    // at the temps, by iterating over the hash-table in the DynTempMemory.
    for (typename DynTempMemory<Mem>::UnsortedCLoop p(src.loopCUnsorted());
         !p.atEnd (); ++p)
    {
      // Should use src.readNoCheck rather than src.read -- but that would
      // require re-testing.  Noted in bug5803.
      if (src.read (p.getKey ()) != p.getValue ())
        return false;
    }
    return true;
  }

  template<class Mem>
    bool operator!=(const DynTempMemory<Mem>& src) const {
    return !(*this == src);
  }

  // comparison between a Memory and an associated static temp memory
  template <int NumPorts>
    bool operator==(const StaticTempMemory<NumPorts, Memory>& src) const {
   
    const Memory* other = src.getBaseMemory ();

    if (this != other) {
      // comparing a temp against some memory that's not its parent.
      if (getStorageSize () != other->getStorageSize ()
          || getDepth () != other->getDepth ())
        return false;

      // compare ALL the members
      for (UInt32 i = 0; i < ComputeDepth (_MSW,_LSW); ++i)
        if (src.readNoCheck (i) != readNoCheck (i))
          return false;

      return true;
    }

    // Since this temp is comparing against its master, we only need to look
    // at the temps, by iterating over the array-table in the StaticTempMemory.
    for (typename StaticTempMemory<NumPorts, Memory>::UnsortedCLoop p(src);
         !p.atEnd(); ++p)
    {
      if (readNoCheck (p.getKey()) != p.getValue()) {
        return false;
      }
    }

    return true;
  }

  template<int NumPorts>
    bool operator!=(const StaticTempMemory<NumPorts, Memory>& src) const {
    return !(*this == src);
  }

  //! Templated function to assign from multiport static temp memory to memory
  template <int _NumPorts> 
  Memory& operator=(const StaticTempMemory<_NumPorts, Memory>& src) {
    const Memory* master = src.getBaseMemory ();
    if (master == this)
      src.executeQueuedWrites (false);
    else
      slowCopy (src);

    return *this;
  }

  //! Get the start of the array of data
  _MemType* getStorage() const {return mMemory;}

  //! Get the size of the memory storage
  UInt64 getStorageSize() const
  {
    int depth = ComputeDepth(_MSW,_LSW);
    int size  = sizeof(_MemType);
    return (UInt64) depth * size;
  }

  UInt64 getActualSize() const
  {
    int depth = ComputeDepth(_MSW,_LSW);
    return (UInt64) depth * _Width;
  }

}; // class Memory

#ifdef CARBON_SAVE_RESTORE
template <class _MemType, int _Width, int _MSW, int _LSW>
void writeToStream(UtOCheckpointStream* f, const Memory<_MemType,_Width,_MSW,_LSW>&m)
{
  carbonInterfaceOCheckpointStreamWrite(f, (const void *) m.getStorage (), m.getStorageSize());
}

template <class _MemType, int _Width, int _MSW, int _LSW>
void readFromStream(UtICheckpointStream* f, Memory<_MemType, _Width, _MSW, _LSW>& m)
{
  carbonInterfaceICheckpointStreamRead(f, (char *) m.getStorage (), m.getStorageSize());
}
#endif //CARBON_SAVE_RESTORE



//! StateUpdateMemory template class
/*!
 * This template class is derived from the Memory template class and
 * creates a variant on the memory object. This variant fixes cycles
 * between sequential memory writes and reads by double buffering the
 * writes.
 */
template <int _NumPorts, class _MemType, int _Width, int _MSW, int _LSW>
class StateUpdateMemory : public Memory<_MemType, _Width, _MSW, _LSW>
{
public:
  typedef _MemType value_type;
  typedef Memory<_MemType, _Width, _MSW, _LSW> _Base;
  typedef StaticTempMemory<_NumPorts, _Base> TempMemory;
  //! constructor
  StateUpdateMemory() : Memory<_MemType, _Width, _MSW, _LSW>(), mTemp (), mLocalTemp ()
  {
    mTemp.putBaseMemory (this);
    mLocalTemp.putBaseMemory (this);
  }

  //! destructor
  ~StateUpdateMemory() {}

  //! Access the underlying memory without the double-buffering.  Used by codegen
  //! to initialize members of memory in HDL initial blocks
  _Base& base () { return static_cast<_Base&>(*this); }

  //! Access the underlying memory without the double-buffering.  Used
  //by DynTempMemory::operator= to get the bounds of a memory being
  //copied.  This is the same as base(), but the name needs to be the
  //same as the routine to get the base in all the tempMemory types.
  _Base& getBaseMemory() { base(); }

  struct reference
  {
    typedef typename MASKTYPE (value_type) masktype;

    reference(StateUpdateMemory* mem, SInt32 addr)
    {
      mMem = mem;
      mAddr = addr;
    }

    value_type& operator=(const value_type& val)
    {
      mMem->mLocalTemp.writeValue(mAddr, val);
      return (value_type&)val;
    }

    value_type& operator=(const reference& ref) 
    {
      return operator=((value_type&)(ref));
    }

    operator value_type&() {
      _Base* mem = mMem;
      return mem->write(mAddr);
    }

    // access the underlying object
    value_type& ref (void) {
      return mMem->mLocalTemp.write (mAddr);
    }

    StateUpdateMemory* mMem;
    SInt32 mAddr;
  };
  friend struct reference;

  //! Get a memory location for access, this is used for writing to the memory
  reference write(SInt32 addr)
  {
    return reference(this, addr);
  }
  reference writeNoCheck(SInt32 addr)
  {
    return reference(this, addr);
  }

  // Read a memory location
  const _MemType& read(SInt32 addr) const
  {
    return mLocalTemp.read(addr);
  }
  const _MemType& readNoCheck(SInt32 addr) const
  {
    return mLocalTemp.readNoCheck(addr);
  }

  StateUpdateMemory& operator=(const StaticTempMemory<_NumPorts, StateUpdateMemory> & src) {

    const StateUpdateMemory *master = src.getBaseMemory ();
    if (master == this)
      src.executeQueuedWrites(false);
    else
      slowCopy (src);

    return *this;
  }

  StateUpdateMemory& operator=(const Memory<_MemType, _Width, _MSW, _LSW> & src) {
    for (SInt32 i = src.eMemLowerBound; i <= src.eMemUpperBound; ++i) {
      this->writeValueNoCheck(i, src.readNoCheck(i));
    }
    return *this;
  }

  StateUpdateMemory& operator=(const StateUpdateMemory & src) {
    for (SInt32 i = src.eMemLowerBound; i <= src.eMemUpperBound; ++i) {
      this->writeValueNoCheck(i, src.readNoCheck(i));
    }
    return *this;
  }

  StateUpdateMemory& operator=(const DynStateUpdateMemory<_MemType, _Width, _MSW, _LSW> & src) {
    for (SInt32 i = src.eMemLowerBound; i <= src.eMemUpperBound; ++i) {
      this->writeValueNoCheck(i, src.readNoCheck(i));
    }
    return *this;
  }

  //! Sync the local temp memory to the global temp memory
  void localSync()
  {
    mTemp.copyQueuedWrites(mLocalTemp);
  }

  //! Synchronize the memory
  void sync()
  {
    mTemp.executeQueuedWrites(true);
  }

private:
  //! Hide copy and assign constructors
  StateUpdateMemory(const StateUpdateMemory&);

  // If you chaonge anything in here, check sizeofStateUpdateMemory in
  // codegen/emitNet.cxx
  TempMemory mTemp;
  TempMemory mLocalTemp;
}; // class StateUpdateMemory

#ifdef CARBON_SAVE_RESTORE
template <int _NumPorts, class _MemType, int _Width, int _MSW, int _LSW>
void writeToStream(UtOCheckpointStream* f, const StateUpdateMemory<_NumPorts, _MemType, _Width, _MSW, _LSW>&m)
{
  carbonInterfaceOCheckpointStreamWrite(f, (const void *) m.getStorage (), m.getStorageSize());
}

template <int _NumPorts, class _MemType, int _Width, int _MSW, int _LSW>
void readFromStream(UtICheckpointStream* f, StateUpdateMemory<_NumPorts, _MemType, _Width, _MSW, _LSW>&m)
{
  carbonInterfaceICheckpointStreamRead(f, (char *) m.getStorage (), m.getStorageSize());
}
#endif //CARBON_SAVE_RESTORE

//! DynStateUpdateMemory template class
/*!
 * This template class is derived from the Memory template class and
 * creates a variant on the memory object. This variant fixes cycles
 * between sequential memory writes and reads by double buffering the
 * writes.
 */
template <class _MemType, int _Width, int _MSW, int _LSW>
class DynStateUpdateMemory : public Memory<_MemType, _Width, _MSW, _LSW>
{
public:
  typedef _MemType value_type;
  typedef Memory<_MemType, _Width, _MSW, _LSW> _Base;
  typedef DynTempMemory<_Base> TempMemory;

  //! constructor
  DynStateUpdateMemory()
  {
    mTemp.putBaseMemory (this);
    mLocalTemp.putBaseMemory (this);
  }

  //! destructor
  ~DynStateUpdateMemory() {}

  //! Access the underlying memory without the double-buffering
  _Base& base () { return static_cast<_Base&>(*this); }

  struct reference
  {
    typedef typename MASKTYPE (value_type) masktype;

    reference(DynStateUpdateMemory* mem, SInt32 addr)
    {
      mMem = mem;
      mAddr = addr;
    }

    // Performance issue for bit-vectors...but I can't get
    // all our code to compile with const value_type&.
    value_type& operator=(const value_type& val)
    {
      mMem->mLocalTemp.writeValue(mAddr, val);
      return (value_type&)val;
    }

    value_type& operator=(const reference& ref) 
    {
      return operator=((value_type&)(ref));
    }

    operator value_type&() {
      _Base* mem = mMem;
      return mem->write(mAddr);
    }

    // access the pending copy
    value_type& ref (void) {
      return mMem->mLocalTemp.write (mAddr);
    }


    DynStateUpdateMemory* mMem;
    SInt32 mAddr;
  };

#if pfGCC_2
  friend struct reference;
#endif

  //! Get a memory location for access, this is used for writing to the memory
  reference write(SInt32 addr)
  {
    return reference(this, addr);
  }

  reference writeNoCheck(SInt32 addr)
  {
    return reference(this, addr);
  }

  void writeValue(SInt32 addr, const value_type& val) {
    mLocalTemp.writeValue(addr, val);
  }

  void writeValueNoCheck(SInt32 addr, const value_type& val)
  {
    mLocalTemp.writeValueNoCheck(addr, val);
  }

  // Read a memory location
  const _MemType& read(SInt32 addr) const
  {
    return mLocalTemp.read(addr);
  }
  const _MemType& readNoCheck(SInt32 addr) const
  {
    return mLocalTemp.readNoCheck(addr);
  }

  DynStateUpdateMemory& operator=(const DynTempMemory<DynStateUpdateMemory> & src) {
    const DynStateUpdateMemory* master = src.getBaseMemory ();
    if (master == this)
      src.executeQueuedWrites(false);
    else
      slowCopy (src);

    return *this;
  }

  DynStateUpdateMemory& operator=(const Memory<_MemType, _Width, _MSW, _LSW> & src) {
    for (SInt32 i = src.eMemLowerBound; i <= src.eMemUpperBound; ++i) {
      this->writeValueNoCheck (i, src.readNoCheck (i));
    }
    return *this;
  }

  DynStateUpdateMemory& operator=(const DynStateUpdateMemory & src) {
    for (SInt32 i = src.eMemLowerBound; i <= src.eMemUpperBound; ++i) {
      this->writeValueNoCheck (i, src.readNoCheck (i));
    }
    return *this;
  }

  template <int _NumPorts> 
  DynStateUpdateMemory& operator=(const StateUpdateMemory<_NumPorts, _MemType, _Width, _MSW, _LSW> & src) {
    for (SInt32 i = src.eMemLowerBound; i <= src.eMemUpperBound; ++i) {
      this->writeValueNoCheck (i, src.readNoCheck (i));
    }
    return *this;
  }

  //! Sync the local temp memory to the global temp memory
  void localSync()
  {
    mTemp.copyQueuedWrites(mLocalTemp);
  }

  //! Synchronize the memory
  void sync()
  {
    mTemp.executeQueuedWrites(true);
  }

private:
  //! Hide copy and assign constructors
  DynStateUpdateMemory(const DynStateUpdateMemory&);

  // If you change anything in here, check sizeofDynStateUpdateMemory in
  // codegen/emitNet.cxx
  TempMemory mTemp;
  TempMemory mLocalTemp;
}; // class DynStateUpdateMemory


#endif // __memory_h_
