// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CGNetElabCount_h_
#define CGNetElabCount_h_

#include "util/UtMap.h"
#include "util/IterMap.h"

class NUNetElab;

//! Class to assign indices to unique net elabs
class CGNetElabCount
{
public:
  //! constructor
  CGNetElabCount();
  //! destructor
  ~CGNetElabCount();
  
  //! Insert the netelab and return its index
  /*!
    If the netelab already exists, it returns the index it was already
    assigned.
  */
  int insert(const NUNetElab* netElab);
  
  //! Insert a netelab but give it an invalid index
  /*!
    In some cases we want to add the netelab to the unique set, but we
    don't want to have a unique value associated with it.
  */
  void insertNoCount(const NUNetElab* netElab);

  //! Returns true if the netelab is in the set
  bool exists(const NUNetElab* netElab) const;
  
  //! Returns the index (valid or not) for the netelab
  /*!
    This NU_ASSERTs if netElab is not already in the set.
  */
  int getIndex(const NUNetElab* netElab) const;

  //! The number of entries in the set
  UInt32 size() const;

  //! Type definition to loop the map of netelabs to indices
  typedef IterMap<const NUNetElab*, int> NetElabMapIter;
  //! Loop the map of netelabs to indices
  NetElabMapIter loopNetElabs();

private:
  int mCount;

  typedef UtMap<const NUNetElab*, int> NetToIntMap;
  NetToIntMap* mMap;
};

#endif
