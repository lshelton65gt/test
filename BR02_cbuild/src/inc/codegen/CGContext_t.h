// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __cgcontext_t_h_
#define __cgcontext_t_h_
 
//!Set of attributes for code generation, passed up and down tree.

/*!These attributes provide us with the context information we need to
 * to determine what kind of C++ code we're generating.
 * Attributes can be inherited (passed down the tree) or synthesized
 * (passed up the tree), or both.
 *
 * Some attributes are also used to indicate what output file we are writing
 * the translation into; c.f. eCGSchedule, eCGIface.
 */

const UInt64 eCGVoid	=      0x0;	/*!< No special value returned [IS] */

/*!
 * When a lvalue codes a macro or function call instead of the LHS of
 * an assignment statement, it returns eCGBitfield, indicating that
 * the RHS must emit the closing parenthesis.
 */
const UInt64 eCGBitfield =      0x1;	/*!< value is a bitfield.[S] */

/*!
 * The coded operand was a bitvector (e.g. more than 64 bits)
 */
const UInt64 eCGHuge	=      0x2;	/*!< value > sizeof UInt64 [IS] */

/*!
 * The operand being coded is an lvalue.  This is typically passed
 * down FROM the Nucleus classes like NUIdentLvalue and NUVarselLvalue
 * to their ident.  It's important for coding memory accesses as it
 * directs the generation of a read or write access to the memory.
 *
 * eCGLvalue should be cleared in the context passed down to the
 * indexing expressions for a NUMemselLvalue or NUvarselLvalue; those
 * expressions are rValues.
 */
const UInt64 eCGLvalue	=      0x4;	/*!< value needed for its lvalue [I] */

/*!
 * Set to indicate that we are generating code for class headers, and
 * that you don't want to generate the body definition (unless it's
 * inlined) for functions.  This is part of a group of related context
 * flags including eCGDefine, eCGSchedule, eCGIface, eCGConstructor
 * that require different styles of code emission for net names or
 * other special-case processing.
 */
const UInt64 eCGDeclare	=      0x8;	/*!< coding class declarations.[I] */

/*!
 * Coding the class function bodies.
 */
const UInt64 eCGDefine	=     0x10;	/*!< coding class definitions. [I] */

/*!
 * Set to indicate that we're generating code for the schedules.
 */
const UInt64 eCGSchedule =     0x20;	/*!< schedule or instantiation [I] */

/*!
 * Passed down when generating code for an enabled driver.  We
 * construct a simple assignment to the tristate variable and DON'T
 * want to engage the code in NUAssign::emitCode() that would set the
 * drive-strength to a default value.
 */
const UInt64 eCGStrengthSet=   0x40;	/*!< Tristate drive strength set [I] */

/*!
 * Context flags are used in NUNet::emitCode() to decide if special
 * versions of names are required (e.g. m_foo vs. a_foo or just 'foo',
 * depending on what kind of code we are generating.  'NameOnly' says
 * no prefix/suffix decorations should be applied to the name.
 */
const UInt64 eCGNameOnly =     0x80;	/*!< just generate symbol name [I] */

/*!
 * Generating a CModel interface header file
 */
const UInt64 eCGIface	=    0x100;	/*!< generate interface header [I]*/

/*!
 * We are generating a name in a mode that requires printing an access
 * hierarchy.
 */
const UInt64 eCGHierName =    0x200;	/*!< emit hierarchy a.m_b.m_c[I] */

/*!
 * Coding the top-level module
 */
const UInt64 eCGMain     =    0x400;	/*!< writing to main program file [I]*/

/*!
 * 
 */
const UInt64 eCGNoPrefix =    0x800;	/*!< no m_, f_ or get_ prefixes [I] */

/*!
 * Generating code via one of the emitCodeList functions, where the
 * punctuation between statements or expression is supplied by
 * emitCodeList arguments, so that no semicolons should be inserted by
 * the individual emitCode() functions.
 */
const UInt64 eCGList     =   0x1000;	/*!< don't punctuate list [I] */

/*!
 * NUAssign::emitCode() generates special code for assignment to a
 * forcible variable.  The eCGForcible flag tells NUNet::emitCode() to
 * generate an access to the mask for the Forcible variable, not to
 * the data for the variable.
 */
const UInt64 eCGForcible =   0x2000;	/*!< reference to forcible mask [I] */

/*!
 * When an expression appears in a conditional-flow context, such as:
 * \code
 *      if (expr)....
 *      x = (expr ? a : b);
 *      y = ex1 || ex2;
 * \endcode
 * We don't need a computable copy of the expression, we only need to
 * know if the expression is non-zero (true) or zero (false).  So for
 * the above three cases, we will pass eCGFlow down to codegen the
 * expression, and if it can generate a more efficient expression that
 * provides that information it will.
 *
 * Examples:
 * \verbatim
 *      Verilog:  if ( vector[5] )      C++:    if (vector & 0x20)
 *             :  if ( bitvector )         :    if (bitvector.any())
 *             :  if ({x,y,z})             :    if (x || y || z)
 * \endverbatim
 */
const UInt64 eCGFlow	=   0x4000;	/*!< flow-value conditional test [I] */

/*!
 * When codegen generates code for an expression, it could produce
 * excess bits or it could produce a result that is exactly the right
 * size.  Rather than ALWAYS masking expressions to their declared bit
 * precision, we identify those expressions which compute excess bits
 * and expect the caller to mask the result before they use it.  This
 * often allows us to avoid masking, or to only mask once.
 *
 * Consider \verbatim
 *      a8 = (b10 + 1'b1);
 * \endverbatim
 *
 * Verilog says the size of the rhs is 10 bits.  But adding 1 to a 10
 * bit value could overflow into 11 bits.  But rather than mask in the
 * NUBinaryOp::emitCode(), we just don't return eCGPrecise.
 *
 * In NUAssign::emitCode(), the assignment operation notes that the
 * RHS is imprecise, but that it is larger than the 8 bit destination
 * ANYWAY, so it doesn't need to mask the assignment at all.
 *
 * Similarly, if have something like \verbatim
 *
 *      a4 = ((b8 + 1) & 4'b1010);
 *
 * \endverbatim the NUBinaryOp::emitCode will return eCGPrecise,
 * because the AND with a 4 bit mask guarantees that the imprecise
 * result returned by "b8 + 1") will be no larger than 4 bits, and
 * there is no mask needed before assigning to a4.
 *
 * It is the callers responsibility to test the precise flag and mask
 * off the imprecise bits if the operation being coded would cause the
 * excess bits to affect the computation of the precise bits returned
 * by this term.  Consider \verbatim
 *
 *      ((a8+1'b1) << 4) >> 4
 *
 * \endverbatim the plus returns imprecise, but the left-shift doesn't
 * care because it only needs 8 bits of result.  However the right
 * shift DOES care, so it must mask the implicitly 13 bit value
 * (a8+'1b1)<< 4
 */
const UInt64 eCGPrecise	=   0x8000;	/*!< no excess bits [S] */

/*!
 * CodeGen's first pass tags every visited net in the CGAuxInfo net
 * map.  Only visited nets will be assigned storage in the class
 * declaration; any unvisited net is considered as unreferenced and
 * dead.
 */
const UInt64 eCGMarkNets =  0x10000;	/*!< set eGenerateCode flag [I] */

/*!
 * Coding the class constructor (either as part of an instantiation of
 * a containing or derived class, or when defining the class's constructor
 */
const UInt64 eCGConstructor=0x20000;	/*!< coding class constructor [I] */

/*!
 * Some lvalues can't be computed directly as LVALUES and have to be
 * specailly handled.  The only two cases of this are Lvalue concats
 * and nested Lvalue varsels.  In these cases, the returned delayed
 * flag causes special processing to occur.
 */
const UInt64 eCGDelayed	=  0x40000;	/*!< lvalue NOT emitted yet [S] */

/*!
 * Because BVref's are aliases to an existing BitVector object, but a
 * copy-constructed BVref STILL points to the same BitVector object,
 * C++ has trouble generating expressions involving BVrefs.  It
 * assumes that it can create a copy and use that as a temporary; but
 * that would alter the underlying object.  So, any time we codegen
 * expressions with BVrefs that would require a temporary, we codegen
 * a BitVector<> constructor initialized from the BVref and then use
 * that temporary for the remaining expression.
 */
const UInt64 eCGNeedsATemp= 0x80000;	/*!< needs writeable temp [I] */

/*!
 * If codegen noticed the 'Needs-A-Temp" request and was able to honor
 * it by producing an expression that constructed a bitvector
 * temporary, then we return a context result indicating that we have
 * a valid temporary and that we don't need to "ask" any other operand
 * to generate a temp.  For example, \verbatim
 *
 *    BV120 = A[0 +: 120] + A[120 +: 120];   // two BVrefs referring to A
 * 
 * When coding the "+", we notice that the expressions involve BVrefs
 * and pass down a \c eCGNeedsATemp flag to
 * NUvarselRvalue::emitCode(), where it ends up generating \verbatim
 *
 *   BV120 = BitVector<120, false>(A.partsel(0,120)) + A.partsel(120, 120);
 *
 * \endverbatim.  And the operator overloading for class BitVector
 * finds \verbatim
 *
 *      BitVector<120> operator+(BitVector<120>& left, BVref& right)
 * 
 * \endverbatim
 */
const UInt64 eCGIsATemp  = 0x100000;	/*!< subtree yields a temporary [S] */

/*!
 * obsolete?
 */
//const UInt64 eCGIsSSA    = 0x200000;	/*!< SSA lhs not in rhs [I] */

/*!
 * When coding function definition signature, we don't want a ";" after
 * the close parenthesis because we will emit the function body in {}.
 *
 * Similarly, some statements don't emit their own closing ";", they let
 * the caller of their emitCode() function terminate them (perhaps with a
 * comma or a close parenthesis or bracket)
 */
const UInt64 eCGNoSemi   = 0x400000;	/*!< Suppress ';' [I] */

/*!
 * obsolete?
 */
//const UInt64 eCGConstant = 0x800000;   	/*!< evaluates to constant [S] */

/*!
 * Indicates that nets are declared in a dynamic scope (function body)
 * and an initialization expression should be generated as part of the
 * variable's definition.  Not needed for static scoped nets, as they
 * are zeroed by the design's initialization and setup code allocating
 * zero-filled memory for the top-level object.
 */
const UInt64 eCGZeroInit =0x1000000;	/*!< Initialize local vars [I] */

/*!
 * Set to encourage aggressive C++ function inlining for modules that
 * are only instantiated once.  This might be subsumed by flattening or
 * early flattening passes.
 */
const UInt64 eCGInline	=0x2000000;	/*!< Module instantiated once [I] */

/*!
 * Generating code for an initial block as opposed to an always block.
 * Initialization of a state-update-memory requires special code generation
 * and doesn't introduce localSync() calls.
 */
const UInt64 eCGInitial  =0x4000000;	/*!< verilog initial-block [I] */

/*!
 * Generating code for functions in schedule-execution order.
 */
const UInt64 eCGFunctionLayout =0x8000000;     /*!< functions in schedule order [I] */

/*!
 * Second pass of code generator. Generating .cxx file with full knowledge.
 */
const UInt64 eCGSecondPass = 0x10000000;	/*!< Second define pass[I] */ 

/*!
 * Used to construct 'thunks' for a hierarchical task call so that each
 * of the potential resolutions has a callable function.
 */
const UInt64 eCGFunctor= 0x20000000;     /*!< functor for task hier refs[I] */

/*!
 * Control printing net name as 'get_'XXX'()'
 */
const UInt64 eCGAccess = 0x40000000;     /*!< accessor from a parent[I] */

/*!
 * Only set when coding elaborated expressions for a cycle.
 */
const UInt64 eCGCycleCopy = 0x80000000; /*!< cycle-settling detection [I]*/

/*!
 * Indicate that a mixed mode expression involving BitVectors and 32 or 64 bit
 * integral values is permitted.  Typically, an operator will note that some
 * operands have self-determined or effective sizes no larger than an integral
 * type (32 or 64 bits) and that we have implemented overloaded operators
 * that avoid materializing the larger BitVector temporary for that operand.
 * For example, \verbatim
 *
 *      BV128 + 128'b1;
 *
 * \endverbatim could avoid materializing a bitvector temporary with a value
 * of 1 by calling BitVector<128,false>::operator+(BV128, UInt32(1).
 */
const UInt64 eCGBVOverloadSupported =
                            KUInt64(0x100000000); //!< 32/64 bit overloads [I]

/*!
 * Indicate the BitVector is in the context of an expression (to
 * distinguish from an assignment.)  Used to decide if a cast to
 * UInt32 is necessary for the BitVector overload supported operand -
 * the cast is required for an expression, but not if it is in the
 * context of an assignment.
 */
const UInt64 eCGBVExpr = KUInt64(0x200000000);  //!< Overloaded expression [I]

/*!
 * Code generating an expression in a context that requires that the operand
 * be properly sign-extended to a physical size
 */
const UInt64 eCGNeedsExtension =
             KUInt64 (0x400000000);  //!< operand needs to be sign-extended[IS]

/*!
 * Do not allow a primary (ident, memsel, varsel, concat) to return an
 * imprecise value
 */
const UInt64 eCGPrecisePrimary    =  KUInt64 (0x800000000); //!< [IS]

/*!
 * CodeGen is in a context where an eBiDownTo expression is permitted and
 * the expression should evaluate to its lower bound.  Encountering eBiDownTo
 * in any context other than as the index expression to a NUVarselRvalue or
 * NUVarselLvalue is an error because no other code-generating context knows
 * what to do with these expressions.
 */
const UInt64 eCGDownToValid = KUInt64 (0x1000000000); //!< eBiDownTo legal[I]

/*!
 * The operand is ok to be dirty and need not be masked - used in
 * pattern matched cases only, currently only passed down to the form
 * (x \<shiftop\b> c)
 */
const UInt64 eCGDirtyOk = KUInt64 (0x2000000000); //!<  [I]

/*!
 * We have identified that this expression is suitable for OP= code generation
 *
 * Essentially, when we see \verbatim
 *
 *      X = X op Y;
 *
 * \endverbatim and 'op' is an operator having a valid C++ op=() overload,
 * we suppress code-generation of the Lvalue and instead just codegen the
 * right-side expression with the eCGOpEqual flag.  This causes us to
 * treat the left-operand as a lvalue, (passing eCGLvalue down to the
 * tree to X's NUIdentRvalue), and replacing 'op' by 'op='.
 */
const UInt64 eCGOpEqual = KUInt64 (0x4000000000); //!< is op= candidate [I]

/*!
 * We are emitting a statement list.
 */
const UInt64 eCGStmtList = KUInt64 (0x8000000000);

/*!
 * Emit declaration as a reference e.g. BitVector <256> &m_foo.
 */
const UInt64 eCGReference = KUInt64 (0x10000000000);

/*!
 * Get CBaseTypeFmt to emit a static cast
 */
const UInt64 eCGStaticCast = KUInt64 (0x20000000000);

/*!
 * We are emitting an actual parameter
 */
const UInt64 eCGActualParameter       = KUInt64 (0x40000000000);


/*!
 * We are emitting an actual parameter. This attribute is used for the
 * temporary fix for bug7683. Bug7696 requires elimination of the temporary
 * used to pach 7683. Remove this when 7696 is closed.
 */
const UInt64 eCGSignedActual = KUInt64 (0x80000000000);

// Some utility functions for testing context flags

inline bool isDefine(CGContext_t c)  { return (c & eCGDefine)!= 0;}
inline bool isDeclare(CGContext_t c) { return (c & eCGDeclare)!= 0;}
inline bool isNameOnly(CGContext_t c) { return (c & eCGNameOnly)!= 0;}
inline bool isLvalue(CGContext_t c)  { return (c & eCGLvalue)!= 0;}
inline bool isBitfield(CGContext_t c) { return (c & eCGBitfield)!= 0;}
inline bool isHierName(CGContext_t c) { return (c & eCGHierName)!= 0;}
inline bool isNoPrefix(CGContext_t c) { return (c & eCGNoPrefix)!= 0;}
inline bool isList(CGContext_t c)    { return (c & eCGList)!= 0;}
inline bool isFlowValue (CGContext_t c) { return (c & eCGFlow)!= 0;}
inline bool isSchedule (CGContext_t c) { return (c & eCGSchedule)!= 0; }
inline bool isInterface (CGContext_t c) { return (c & eCGIface)!= 0; }
inline bool isPrecise (CGContext_t c) { return (c & eCGPrecise)!= 0;}
inline bool isForced (CGContext_t c) { return (c & eCGForcible) != 0;}
inline bool isMarkNets (CGContext_t c) { return (c & eCGMarkNets)!= 0;}
inline bool isConstructor (CGContext_t c) { return (c & eCGConstructor)!= 0;}
inline bool isHuge (CGContext_t c) { return (c & eCGHuge)!=0;}
inline bool isDelayed (CGContext_t c) {return (c & eCGDelayed) != 0; }
inline bool isATemp (CGContext_t c) {return (c & eCGIsATemp) != 0; }
inline bool NeedsATemp (CGContext_t c) {return (c & eCGNeedsATemp) != 0; }
//inline bool isSSA (CGContext_t c) {return (c & eCGIsSSA) != 0; }
inline bool noSemi (CGContext_t c) {return (c & eCGNoSemi) != 0; }
inline bool isZeroInit (CGContext_t c) {return (c & eCGZeroInit) != 0; }
//inline bool isConstant (CGContext_t c) {return (c & eCGConstant) != 0; }
inline bool isInlineCandidate (CGContext_t c) {return (c & eCGInline) != 0; }
inline bool isInitialBlock (CGContext_t c) {return (c & eCGInitial) != 0; }
inline bool isFunctor (CGContext_t c) {return (c &eCGFunctor) != 0; }
inline bool isAccessor (CGContext_t c) {return (c &eCGAccess) != 0; }
inline bool isCycleCopy (CGContext_t c) {return (c & eCGCycleCopy) != 0; }
inline bool isStrengthSet (CGContext_t c) {return (c & eCGStrengthSet) != 0; }
inline bool isBVOverloadSupported (CGContext_t c) {return (c & eCGBVOverloadSupported) != 0; }
inline bool isBVExpr (CGContext_t c) {return (c & eCGBVExpr) != 0; }
inline bool needsExtension (CGContext_t c) {return (c & eCGNeedsExtension) != 0; }
inline bool needsPrecisePrimary (CGContext_t c) {return (c  & eCGPrecisePrimary) != 0;}
inline bool isDownToValid (CGContext_t c) { return (c & eCGDownToValid) != 0; }
inline bool isDirtyOk(CGContext_t c)  { return (c & eCGDirtyOk)!= 0;}
inline bool isOpEqual (CGContext_t c) { return (c & eCGOpEqual)!=0;}
inline bool isVoid (CGContext_t c) { return c == eCGVoid; }
inline bool isSecondPass (CGContext_t c) { return (c & eCGSecondPass) != 0; }
inline bool isFunctionLayout (CGContext_t c) { return (c & eCGFunctionLayout) != 0; }
inline bool isStmtList (CGContext_t c) { return (c & eCGStmtList) != 0; }
inline bool isReference (CGContext_t c) { return (c & eCGReference) != 0; }
inline bool isStaticCast (CGContext_t c) { return (c & eCGStaticCast) != 0; }
inline bool isActualParameter (CGContext_t c) { return (c & eCGActualParameter) != 0; }
inline bool isSignedActual (CGContext_t c) { return (c & eCGSignedActual) != 0; }

/* clear the precise flag.. */
inline CGContext_t Imprecise (CGContext_t c) { return (c & ~eCGPrecise);}

inline CGContext_t Precise (CGContext_t c) { return (c | eCGPrecise);}

// If we got a temporary already, don't ask for another one
inline CGContext_t TempRequest (CGContext_t c) {
  return (isATemp (c) ? eCGVoid : eCGNeedsATemp); }
#endif
