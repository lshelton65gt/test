// -*-c++-*-
/*****************************************************************************
 Copyright (c) 2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.
******************************************************************************/

#ifndef __CARBON_COMPONENT_ACCESS_H__
#define __CARBON_COMPONENT_ACCESS_H__

#include "eslapi/CAInterface.h"
#include "carbon/carbon_shelltypes.h"

//! CA interface for accessing internals of a Carbon component
class CarbonComponentAccess : public eslapi::CAInterface
{
 public:
  static eslapi::if_name_t IFNAME() { return "carbon.ComponentAccess"; }
  static eslapi::if_rev_t IFREVISION() { return 0; }

  //! Returns the CarbonObjectID for the component
  virtual CarbonObjectID* getCarbonObject() const = 0;

  //! Callback class for various component events
  class Callback
  {
  public:
    typedef enum
    {
      Begin,    //!< Called at the beginning of the function
      End       //!< Called at the end of the function
    } Order;

    // Called during the component's update() function
    virtual void update(Order /* when */, uint64_t /* cycle */) {}
    // Called during the component's communicate() function
    virtual void communicate(Order /* when */, uint64_t /* cycle */) {}
  };

  //! Add a callback for component events
  virtual void addCallback(Callback* cb) = 0;
};

#endif
