// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/* Template class supporting forcible nets.  */
/*!\file
 */

#ifndef __ForciblePOD_h_
#define __ForciblePOD_h_

#ifndef __carbon_priv_h_
#include "codegen/carbon_priv.h"
#endif

#ifdef CARBON_SAVE_RESTORE
#ifndef __UtCheckpointStream_h_
#include "util/UtCheckpointStream.h"
#endif
#endif
/* 
 * Compiler doesn't allow "typedef typename T OtherT"
 */
#if pfGCC_2
#define TYPENAME
#else
#define TYPENAME typename
#endif

/*!
 * ForciblePOD<T> objects contain a value of POD T and a mask indicating
 * which bits of the object are protected from assignment.  Codegen detects
 * assignments to a forciblePOD object (such as)
 *
 *    fvar = input;
 *
 * and generates code that looks like
 *
 *    fvar.data = (fvar.data & fvar.mask) | (input &~mask);
 *
 * to preserve the "forced" bits
 */


template <typename T> class ForciblePOD
{
private:
  
  //! The readable value.
  T mData;

  //! Storage for the force mask
  T mMask;

  //! Make simple assignment unavailable
  ForciblePOD& operator=(const ForciblePOD& src);

  //! Copy Constructor is suppressed also - only pass-by-reference allowed
  ForciblePOD (const ForciblePOD& v);

public:
  //! Trivial Constructor
  explicit ForciblePOD (const T &data=T (0), const T &s=T (0)) : mData(data), mMask(s) {}

  //! Trivial destructor
  ~ForciblePOD () {}

  //! Simple assignment from the base is okay
  ForciblePOD& operator=(const T& src)
  {
    mData = (mData & mMask) | (src & ~mMask);
    return *this;
  }

  // All non-pods must supply this
  typedef typename MASKTYPE (T) masktype;

  //! read-only Accessor for value
  const T& getData () const { return mData; }

  //! read-write Accessor for value
  T& getData () { return mData; }

  //! Accessor for force mask
  const T& getMask () const {return mMask;}

  //! read-only accessor for a force mask of 'size' bits beginning at position 'pos'
  T getMask (UInt32 pos, UInt32 size) const {
    return (mMask >> pos) & ((static_cast<T>(1) << size)-1);
  }

  //! implicit cast so data can be used in an expression
  operator const T&() const {return mData;}

  T* getForceMask () const { return (T*) &mMask; }

  T* getStorage () const { return (T*) &mData; }

  //! clear the mask
  void clearMask () {
    mMask = T (0);
  }

  //! Clear part of a force mask
  void clearMask (const T &s) {
    mMask &= ~s;
  }

  //! Clear a field of a mask
  void clearMask (size_t pos, size_t size) {
    if (size == 64)
      mMask = 0;
    else
      mMask &= ~((~((~T(0)) << size)) << pos);
  }

  //! Set all of a mask
  void setMask () {
    mMask = ~T (0);
  }

  //! Set part of a mask
  void setMask (UInt32 pos, UInt32 size)
  {
    if (size == 64)
      mMask = ~T(0); // silence icc
    else
      mMask |= (~((~T (0)) << size)) << pos;
  }

  //! Or in a mask
  void setMask (T s) { mMask |= s; }

  const T& getT () const { return mData; }

  //! Simplest assignment
  void setData (const T data) {
    mData = (data & ~mMask) | (mData & mMask);
  }

  //  Forcible* getRef() {
  //    return this;
  //  }
};


#ifdef CARBON_SAVE_RESTORE
// i/o support for save & restore
template <typename T> 
void writeToStream(UtOCheckpointStream* f, const ForciblePOD<T>& v)
{
  writeToStream(f, v.getData());
  writeToStream(f, v.getMask());
}

template <typename T> 
void readFromStream(UtICheckpointStream* f, ForciblePOD<T>& v)
{
  T value;

  readFromStream(f, value);
  v.setData (value);

  T mask;
  readFromStream(f, mask);
  v.setMask (mask);
}

#endif //CARBON_SAVE_RESTORE
#endif
