// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 
#ifndef CGOFILES_H_
#define CGOFILES_H_

/*!
  \file
  Handle code generation file IOs.  Moved from codegen.h 1.273 (+)
*/

#include "nucleus/Nucleus.h"
#include "nucleus/NUModule.h"
#include "util/UtString.h"

class CodeGen;
class SCHSchedule;
class MsgContext;
class UtUniquify;
class InducedFaultTable;

//! Class 
/*!
 */
class CGOFiles
{
  //! A comparator for indirect strings...
  struct CmpString{
    bool operator()(const UtString*a, const UtString*b) const { return *a < *b; }
  };

  //! Collect the extra header files needed for each compilation unit.
  typedef UtSet<const UtString*, CmpString> ExtraHeadersSet;

  ExtraHeadersSet* mExtraHeaders;

public:
  //! Constructor
  CGOFiles(CodeGen &, InducedFaultTable *, MsgContext *msg);

  //! Destructor
  ~CGOFiles();

  //! Open file and insert boilerplate text.
  /*!
    \warning Note that caller must free std::ofstream at file close! 
  */
  UtOStream * CGFileOpen(const char *path, const char *extra, 
    bool isObfuscating, bool needCleartext = false);

  typedef UtMap<const NUModule*, ExtraHeadersSet*> ClassHeaderMap;
  ClassHeaderMap *mClassHeaderMap;

  //!Change  stream to current output context.
  /*! \returns reference to current output file. */
  UtOStream& switchStream (CGContext_t context);

  //!The output stream we are currently writing to
  /*!\return std::ostream & to current output file. */
  UtOStream& CGOUT()
  {
    return (UtOStream &)*mCurrent;
  }

  UtOStream& getScheduleSrc()
  {
    return (UtOStream &)*mScheduleSrc;
  }

  UtOStream& getIfaceHdr()
  {
    return (UtOStream &)*mIfaceHdr;
  }

  //! Get the pointer to the current stream
  UtOStream* getCurrentStream() { return mCurrent; }
  //! Replace the current stream with another stream
  /*!
    This is used to get past the horrific emitCode of netelabs, et.al
    so we can get the string that would be emitted to the file stream.
    So, replace mCurrent with a UtOStringStream* and then
    put the old value back after your done.
  */
  void putCurrentStream(UtOStream* newCur) 
  {
    mCurrent = newCur;
  }

  //! Open a .cxx file to which we can output functions.
  /*!
   * The opened file will become the file to which codegen emits definitions.
   */
  UtOStream *addFunctionFile(UInt32 file_num, bool precompiled);

  //! map from module names to OS-legalized class names
  typedef UtMap<const NUModule*,const char*> ModuleOSNameMap;
  ModuleOSNameMap* mModuleOSNameMap;

  //! uniquification sets for header filenames and c++ filenames
  UtUniquify* mClassFilenames;

  //!Process another module class.
  /*!
    Given a class name, create the file handles for the class.
  */
  void addClassFile (const NUModule *module, bool header, bool precompiled);

  //!Finish class .h or .cxx files
  void closeClassFile (bool header);

  //! Save set of headers needed to compile class for module mod
  void saveExtraHeaders (const NUModule* mod);

  //! Discard a set of extra headers
  void flushExtraHeaders (const NUModule* mod);
    
  //! Bring them back to active
  void restoreExtraHeaders (const NUModule* mod);

  //! Generate a legal filename for the class/headers for a module
  const char* genModuleFilename(const NUModule* mod);

  //! Delete a class file
  void deleteModuleFilename (const NUModule* mod);

  //! Add header to required set.
  void addExtraHeader (const UtString& hdr);

  //! Add header for a module
  void addExtraHeader (const NUModule*);

  //! Emit \#includes for the extra file headers to CGOUT
  void printExtraHeaders ();

  //! Emit include of the header file for the given module's class.
  void generateHeaderInclude(UtOStream &out, const NUModule *mod);
	      
  //! Emit include of the header files for all the given module's class'es.
  void generateHeaderIncludes(UtOStream &out, const NUModuleList &module_list);

  //! Do we want to *think* about spilling a schedule file?
  bool checkForSpill (int size);

  //! File to output all initialization and cycle-schedule into.
  UtOStream	*mScheduleSrc;

  //! File for schedule spill results.
  UtOStream *mScheduleSpill;

  //! Amount of data written to the spill file.
  UInt64      mScheduleSpillSize;

  //! Limit of data to write to each spill file.
  UInt64      mScheduleSpillLimit;

  //! File to output the translated class definition (implementation) into.
  UtOStream	*mCurrentClassSrc;

  //! File to output the translated class declaration into.
  UtOStream	*mCurrentClassHdr;

  //! File for test driver (a main program)
  UtOStream *mDriver;

  //! File where we are currently writing to
  UtOStream *mCurrent;

  //! File supporting C/C++ interface to outside world
  UtOStream *mIfaceHdr;

private:

  //! CodeGen context
  CodeGen &mCodegen;

  //! Induced fault table for codegen diagnostic regression
  InducedFaultTable *mInducedFaults;

  //! Message Context
  MsgContext	*mMsgContext;

};

#endif
