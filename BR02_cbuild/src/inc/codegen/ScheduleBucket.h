// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef SCHEDULEBUCKET_H_
#define SCHEDULEBUCKET_H_

#ifndef CGPROFILE_H_
#include "codegen/CGProfile.h"
#endif

class ScheduleBucket;
typedef UtArray<ScheduleBucket*> BucketListType;

// Compile time representation of buckets used for sample-based
// profiling of schedules.
class ScheduleBucket {
public:
  // The parent is the enclosing schedule.
  ScheduleBucket(const UtString& name, int id, ScheduleBucket* parent);
  ScheduleBucket* getParent() { return mParent; }
  void setParent(ScheduleBucket* parent) { mParent = parent; }
  int getID() { return mID; }
  operator UtString();
private:
  UtString mName;
  int mID;
  ScheduleBucket* mParent;
};

// Keep track of ScheduleBucket objects as they're added during code
// generation.  Maintain hierarchy necessary for reporting schedules
// that contain other schedules.  Allow writing mapping of bucket
// names to IDs and bucket hierarchy to libdesign.prof file.
class ScheduleBuckets {
public:
  ScheduleBuckets();
  ~ScheduleBuckets();
  // Create a new bucket whose parent is the current bucket.
  int push(const UtString& name);
  // Some schedule buckets consume time of their own and contain lower
  // level schedule buckets.  So popping needs to return the higher
  // level bucket ID so we can track time spent there.
  int pop();
  UInt32 numBuckets() { return mNextID; }
  // Return the current bucket object.
  ScheduleBucket* getCurrent() { return mCurrent; }
  // Write the buckets to the .prof file, to be read by the report generator.
  void record();
private:
  // Current enclosing bucket, used for parent when pushing down
  ScheduleBucket* mCurrent;
  // Monotonically increasing ID of the next created ScheduleBucket
  int mNextID;
  BucketListType mBuckets;
};

#endif // SCHEDULEBUCKET_H_
