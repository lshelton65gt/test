// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _INDUCED_FAULT_H_
#define _INDUCED_FAULT_H_

/*! \file inc/InducedFaultTable.h A mechanism for induced artificial faults into 
 * generated code to test the annotation-based fault diagnostic mechanism.
 */

#include "util/CodeStream.h"

class ArgProc;
class MsgContext;

//! \class InducedFaultTable
/*! This class maintains a mapping from filenames to
 *  CodeStream::InducedFaultMap instances.  The table is initialised by reading
 *  from a specification files. Under the hood, the read method interfaces to a
 *  flex/bison derived parser.
 */

class InducedFaultTable : public UtHashMap <UtString, CodeStream::InducedFaultMap *> {
public:

  InducedFaultTable (MsgContext *);
  virtual ~InducedFaultTable ();

  //! Add a fault to the table
  void add (UtString filename, UInt32 lineno, UtString fault);

  //! Add an appended line to a table
  void append (UtString filename, UtString fault);

  //! Add a prepended line to a table
  void prepend (UtString filename, UtString fault);

  //! Read a set of fault specifications from a file
  bool read (const char *filename);

  //! Look for the map for a given file.
  /*! \note The directory part of the path is stripped and only the filename
   *  part is used for lookup. In practice, the file will always be from
   *  .carbon.libdesign so the filename only is certainly adequate. The parser
   *  called by read reports an error if any '/' characters are found in a
   *  filename.
   */
  CodeStream::InducedFaultMap *findMap (const char *path) const;

  // Diagnostics
  void print (UtOStream &) const;
  void pr () const;

  // bury the Flex/Bison parser in a parser class
  class Parser {
  public:
    //! ctor for Parser
    /*! \param filename name of the open input file, used only for error messages
     *  \param in the already opened input file
     *  \param parent the induced fault table under construction
     *  \param msg_context message stream for whining at
     */
    Parser (const char *filename, FILE *in, InducedFaultTable &parent, MsgContext *msg_context);
    virtual ~Parser ();

    //! Call the bison parser
    SInt32 parse ();
    
    //! Error message writer called from the parser
    /*! \note This method looks at yylineo for the linenumber and writes the
     *   error message to message stream prefixed with filename: lineno:
     */
    void error (const char *fmt, va_list ap);

    //! Add a fault to the table
    void add (const char *filename, UInt32 lineno, const char *fault);

    //! Add an appended line to the table
    void append (const char *filename, const char *fault);

    //! Add a prepended line to the table
    void prepend (const char *filename, const char *fault);

    //! Singleton instance of the parser
    /*! \note This is initialised by the Parser constructor to point at
     *  itself. The destructor resets sInstance to NULL.
     *  \note The yyerror function and the parser actions call methods on
     *  sInstance.
     */
    static Parser *sInstance;

  private:
    const char *mFilename;              //!< name of the file being parsed
    InducedFaultTable &mParent;         //!< handle onto the fault table
    MsgContext *mMsgContext;            //!< whining stream
  };

private:

  MsgContext *mMsgContext;              //!< whining stream

};

#endif
