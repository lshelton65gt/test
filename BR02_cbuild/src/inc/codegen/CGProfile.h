// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CGPROFILE_H_
#define CGPROFILE_H_

#ifndef __CarbonAssert_h_
#include "util/CarbonAssert.h"
#endif
#ifndef SOURCELOCATOR_H_
#include "util/SourceLocator.h"
#endif
#ifndef __UtIOStream_h_
#include "util/UtIOStream.h"
#endif
#ifndef __codegen_h_
#include "codegen/codegen.h"
#endif

class Blob;

// Manage bucket ID allocation for code generation.
class CGBucketID
{
public:
  CGBucketID() : mBucketID(0), mNumBuckets(0) {}
  // Ensure next() wasn't called after numBuckets().
  ~CGBucketID() {
    INFO_ASSERT(mNumBuckets == 0 || mBucketID == mNumBuckets,
                "CGBucketID::next() called after CGBucketID::numBuckets()"); }
  int next() { return mBucketID++; }
  int numBuckets() { mNumBuckets = mBucketID; return mNumBuckets; }
private:
  int mBucketID;           // ID of the next bucket
  // To ensure next() isn't called after numBuckets()
  int mNumBuckets;         // Total number of buckets allocated
};

class CGProfile
{
public:
  CGProfile();
  ~CGProfile() { delete mCompileDB; mCompileDB = NULL; }
  //! Generate code to switch buckets, if force or in different blob.
  void emitStartBucket(bool force, const SourceLocator& loc);
  //! Generate code to switch to the non-bucket.
  void emitStopBucket();
  //! Forget bucket for which we last generated code.
  void resetBucket();
  //! Open the compile-time database.
  void setFile(const char* fileRoot);
  //! Add a new bucket to the compile-time database.
  void recordBucket(const Blob* blob);
  UtOFStream& getCompileDB() { return *mCompileDB; }
private:
  UtOFStream* mCompileDB;
  int mPrevBucket;
};

#endif // CGPROFILE_H_
