// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/* Template class to support memories */

//! Memory data access class
/*!
 * read/write methods converting between external array of UInt32's and internal
 * host-efficient data.  Template parameter instance selects between alternative
 * implementations for small PODs (8,16,32), 64-bit data types and BitVectors.
 */

#ifndef __MemoryAccess_h_
#define __MemoryAccess_h_

#ifndef __carbon_interface_h_
#include "shell/carbon_interface.h"
#endif

#if CARBON_CHECK_OOB
#define REPORT_MEM_OOB(msg,o,hi,lo,i) carbon_oob_memory_access(msg,o, ConstantRange(hi,lo), i)
#else
#define REPORT_MEM_OOB(msg,o,hi,lo,i)
#endif

extern "C" UInt32 gCarbonMemoryBitbucket[] COMMON;

// Utility functions to call the correct C interface function based on C++ function overloading
static inline void sCpSrcToDestRange(UInt64* dest, const UInt32 *src, size_t index, size_t length)
{
  carbonInterfaceCpSrc32ToDest64Range(dest, src, index, length);
}
static inline void sCpSrcToDestRange(UInt32* dest, const UInt32 *src, size_t index, size_t length)
{
  carbonInterfaceCpSrc32ToDest32Range(dest, src, index, length);
}
static inline void sCpSrcToDestRange(UInt16* dest, const UInt32 *src, size_t index, size_t length)
{
  carbonInterfaceCpSrc32ToDest16Range(dest, src, index, length);
}
static inline void sCpSrcToDestRange(UInt8* dest, const UInt32 *src, size_t index, size_t length)
{
  carbonInterfaceCpSrc32ToDest8Range(dest, src, index, length);
}

static inline void sCpSrcToDestWord(UInt32* dest, const UInt64* src, size_t wordIndex)
{
  carbonInterfaceCpSrc64ToDest32Word(dest, src, wordIndex);
}
static inline void sCpSrcToDestWord(UInt32* dest, const UInt32* src, size_t wordIndex)
{
  carbonInterfaceCpSrc32ToDest32Word(dest, src, wordIndex);
}
static inline void sCpSrcToDestWord(UInt32* dest, const UInt16* src, size_t wordIndex)
{
  carbonInterfaceCpSrc16ToDest32Word(dest, src, wordIndex);
}
static inline void sCpSrcToDestWord(UInt32* dest, const UInt8* src, size_t wordIndex)
{
  carbonInterfaceCpSrc8ToDest32Word(dest, src, wordIndex);
}

static inline void sCpSrcWordToDest(UInt64* dest, const UInt32 src, size_t wordIndex)
{
  carbonInterfaceCpSrc32WordToDest64(dest, src, wordIndex);
}
static inline void sCpSrcWordToDest(UInt32* dest, const UInt32 src, size_t wordIndex)
{
  carbonInterfaceCpSrc32WordToDest32(dest, src, wordIndex);
}
static inline void sCpSrcWordToDest(UInt16* dest, const UInt32 src, size_t wordIndex)
{
  carbonInterfaceCpSrc32WordToDest16(dest, src, wordIndex);
}
static inline void sCpSrcWordToDest(UInt8* dest, const UInt32 src, size_t wordIndex)
{
  carbonInterfaceCpSrc32WordToDest8(dest, src, wordIndex);
}
static inline void sCpSrcWordToDest(UInt32* dest, const UInt64 src, size_t wordIndex)
{
  carbonInterfaceCpSrc64WordToDest32(dest, src, wordIndex);
}
static inline void sCpSrcWordToDest(UInt32* dest, const UInt16 src, size_t wordIndex)
{
  carbonInterfaceCpSrc16WordToDest32(dest, src, wordIndex);
}
static inline void sCpSrcWordToDest(UInt32* dest, const UInt8 src, size_t wordIndex)
{
  carbonInterfaceCpSrc8WordToDest32(dest, src, wordIndex);
}


// Itemize the different template specializations we need to represent
enum {
  eMemPOD=0,
  eMemPODLong=1,
  eMemBitVector=2};

template <class _MemType, UInt32 instance=eMemPOD>
class MemoryAccess
{
public:
  void readRow (const _MemType& loc, UInt32* data) const {
    *data = (UInt32) loc;
  }
  void writeRow ( _MemType& loc, const UInt32* data) {
    loc = *data;
  }

  void writeRowClean ( _MemType& loc, const UInt32* data, UInt32 width) {
    loc = *data & carbonInterfaceGetWordMask(width);
  }

  // additions for shell access

  void readRowWord(const _MemType& loc, UInt32* dataWord, int) const {
    readRow(loc, dataWord);
  }

  void readRowRange(const _MemType& loc, UInt32* data, int index, int length) const {
    sCpSrcToDestRange(data, &loc, index, length);
  }

  void writeRowWord(_MemType& loc, const UInt32* dataWord, int) {
    writeRow(loc, dataWord);
  }

  void writeRowWordClean(_MemType& loc, const UInt32* dataWord, int, UInt32 width) {
    writeRowClean(loc, dataWord, width);
  }

  void writeRowRange(_MemType& loc, const UInt32* data, int index, int length) {
    sCpSrcToDestRange(&loc, data, index, length);
  }

  void* getStoragePtr(_MemType& loc) {
    // The memory type is a simple POD
    return &loc;
  }
};

// specializations for 64 bits and bitvectors
template <class _MemType>
class MemoryAccess<_MemType, eMemPODLong>
{
public:
  void readRow (const _MemType& loc, UInt32* data) const {
    data[0] = UInt32 (loc);
    data[1] = UInt32 (loc >> 32);
  }
  void writeRow ( _MemType& loc, const UInt32* data) {
    loc = data[0] | (UInt64 (data[1]) << 32);
  }

  void writeRowClean ( _MemType& loc, const UInt32* data, UInt32 width) {
    writeRow(loc, data);
    loc &= carbonInterfaceGetWordMaskLL(width);
  }

  // additions for shell access

  void readRowWord(const _MemType& loc, UInt32* dataWord, int index) const {
    sCpSrcToDestWord(dataWord, &loc, index);
  }

  void readRowRange(const _MemType& loc, UInt32* data, int index, int length) const {
    sCpSrcToDestRange(data, &loc, index, length);
  }

  void writeRowWord(_MemType& loc, const UInt32* dataWord, int index) {
    sCpSrcWordToDest(&loc, dataWord, index);
  }
  
  void writeRowWordClean ( _MemType& loc, const UInt32* data, int index, UInt32 width) {
    writeRowWord(loc, data, index);
    loc &= carbonInterfaceGetWordMaskLL(width);
  }
  

  void writeRowRange(_MemType& loc, const UInt32* data, int index, int length) {
    sCpSrcToDestRange(&loc, data, index, length);
  }

  void* getStoragePtr(_MemType& loc) {
    // The memory type is a simple POD
    return &loc;
  }
};

template <class _MemType>
class MemoryAccess<_MemType, eMemBitVector>
{
public:
  void readRow (const _MemType& loc, UInt32* data) const {
    std::memcpy ((void *) data, (const void *) &loc, loc.Nw * sizeof(UInt32));
  }
  void writeRow ( _MemType& loc, const UInt32* data) {
    std::memcpy ((void *) &loc, (const void *) data, loc.Nw * sizeof(UInt32));
  }

  void writeRowClean ( _MemType& loc, const UInt32* data, UInt32 width) {
    writeRow(loc, data);
    UInt32* row = loc.getUIntArray();
    row[loc.getUIntArraySize() - 1] &= carbonInterfaceGetWordMask(width);
  }

  // additions for shell access

  void readRowWord(const _MemType& loc, UInt32* dataWord, int index) const {
    const UInt32* row = loc.getUIntArray();
    *dataWord = row[index];
  }

  void readRowRange(const _MemType& loc, UInt32* data, size_t index, size_t length) const {
    const UInt32* row = loc.getUIntArray();
    sCpSrcToDestRange(data, row, index, length);
  }

  void writeRowWord(_MemType& loc, const UInt32* dataWord, int index) {
    UInt32* row = loc.getUIntArray();
    row[index] = *dataWord;
  }

  void writeRowWordClean(_MemType& loc, const UInt32* dataWord, int index, UInt32 width) {
    UInt32* row = loc.getUIntArray();
    row[index] = *dataWord;
    row[loc.getUIntArraySize() - 1] &= carbonInterfaceGetWordMask(width);
  }
  
  void writeRowRange(_MemType& loc, const UInt32* data, size_t index, size_t length) {
    UInt32* row = loc.getUIntArray();
    sCpSrcToDestRange(row, data, index, length);
  }

  void* getStoragePtr(_MemType& loc) {
    // The memory type is a BitVector
    return loc.getUIntArray();
  }
};
#endif // __MemoryAccess_h_

