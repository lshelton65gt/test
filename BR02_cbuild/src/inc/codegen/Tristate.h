// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/* Template class supporting tristate nets.  */
/*!\file
 */

#ifndef __Tristate_h_
#define __Tristate_h_

#ifndef __carbon_priv_h_
#include "codegen/carbon_priv.h"
#endif

// Need enum Strength
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifdef CARBON_SAVE_RESTORE
#ifndef __UtCheckpointStream_h_
#include "util/UtCheckpointStream.h"
#endif
#endif
/* 
 * Compiler doesn't allow "typedef typename T StrengthT"l

 */
#if pfGCC_2
#define TYPENAME
#else
#define TYPENAME typename
#endif

/*!
 * Tristate<T,S> objects remember the strongest assignment to their associated
 * data member until they are reset. There are problems with WAND and WOR if
 * the strength masks aren't identical.  I'm not certain what to do in that
 * case.
 */


//! External interface to testbench (3 values as contiguous array)
enum Tri
{
  eTriData=0,			//<! the signal data
  eTriXdrive=1,			//<! the testbench drive (0=>driven)
  eTriIdrive=2			//<! the internal drive (0=>driven)
};

//! Mask generation helper class

template <class T, UInt32 S> struct _TriMask {
  static T Mask()
  {
    return (T (1) << S) - T (1);
  }
};
#if !pfMSVC
// template specializations broken in Visual C++?
template <class T> struct _TriMask<T, 32> {
  static T Mask () { return ~T (0);}
};

template <class T> struct _TriMask<T, 64> {
  static T Mask () { return ~T (0); }
};
#endif



//! Encapsulates some typed object - type T, bitwidth S

template <typename T, UInt32 S> class Tristate
{
protected:
  //! The readable value.
  T mData;

  //! Storage for the strength mask
  T mStrength;

public:
  typedef typename MASKTYPE (T) masktype;
  typedef T* ptrtype;

  typedef T SignalT;
  typedef T StrengthT;

  //! Trivial Constructor
  explicit Tristate () : mData(0), mStrength(_TriMask<T,S>::Mask ()) {}

  //! Less trivial constructor, strength is strongest by default
  explicit Tristate (const T &data) : mData(data),
				      mStrength(_TriMask<T,S>::Mask ()) {}

  //! Constructor by pieces
  explicit Tristate (const T &data, const T &s) : mData(data), mStrength(s) {}

  //! Copy Constructor
  Tristate (const Tristate& v) : mData (v.mData), mStrength (v.mStrength) {}

  //! Trivial destructor
  ~Tristate () {}

  //! Accessor for drive strength
  const T& getStrength () const {return mStrength;}

  //! Reset strength with external drive
  void resetExternal(const T s) {
    mStrength = ~s & _TriMask<T,S>::Mask ();
  }

  //! clear all drive strength
  void clearStrength () {
    mStrength = T (0);
  }

  //! Clear part of a drive strength
  void clearStrength (const T &s) {
    mStrength &= (~s) & _TriMask<T,S>::Mask ();
  }

  //! Set all of a drive strength
  void setStrength () {
    mStrength = _TriMask<T,S>::Mask ();
  }

  //! Set part of a drive strength
  void setStrength (UInt32 pos, UInt32 size)
  {
    if (size == 64)
      mStrength = T(UtUINT64_MAX); // silence icc
    else
      mStrength |= ((~(UtUINT64_MAX << size)) << pos) & _TriMask<T,S>::Mask ();
  }

  //! Set part of a drive strength using a mask AND a pos, size
  void setStrength (T s, UInt32 pos, UInt32 size)
  {
    mStrength |= ((s & carbon_mask<T>(size)) << pos);
  }


  //! Or in a drive strength
  void setStrength (T s) { mStrength |= s; }

  //! reset strength.
  void reset (T s) { mStrength = s; }

  //! Overloaded
  void reset (void) { mStrength = 0; mData = 0; }

  // Clear bits that are not driven externally.
  void resetunmasked (void) {
    mData &= mStrength;
  }

  //! invert the strength mask
  void flip (void) {mStrength = _TriMask<T,S>::Mask () & ~mStrength;}

  /*!
   * Provide conversion operator.  This allows us to support basic 
   * mixed-mode arithmetic operations over T x Tristate<T,S>
   */
  //! Conversion operator to base class
  /* NB gcc 3.2.x doesn't do free conversion correctly. */

#if !pfGCC_2
  // GCC2 is anal about the presence of a const and non-const conversion operator
  // (it generates warnings) Don't give it the opportunity to tell you it likes
  // the non-const operator better....
  operator const T& () const { return mData; }
#endif

  //! Overload
  operator T& () { return mData;}

  //! Allow simple assignment when unconditionally driven (called from
  //! schedule.cxx)
  void setData (const T& value)
  {
    mData = value;
  }

  Tristate& operator =(const T& value)
  {
    mData = value;
    return *this;
  }

  //! simple accessor for data
  T getData() const { return mData; }

  

  //! Work around GCC 2.95 bugs where (T&) Tristate<T,S>&() fails.
  //T& getT () { return mData; }

  //! Overload
  const T& getT () const { return mData;}

  //! accessor for non-modifiable pointer to data
  /*!
    Needed for things that need to return a const T*. 
  */
  //const T* getDataRef() const { return &mData; }

  //! assignment Overload from one tristate to another.
  /*!
   * Note that an assignment does not pass strength; the lhs is driven strongly
   * even if the rhs is driven weakly.
   * If you want to keep the strength, use the copy constructor or set().
   */
  Tristate& operator =(const Tristate& value)
  {
    this->set (value.mData);
    return *this;
  }
  
  //! The basic resolution - simplest case.
  void set (T data)
  {
    mData = data;
    mStrength = _TriMask<T,S>::Mask ();
  }

  // Possibly driving only part of a net; resolve against existing
  // drivers.
  //
  void set (const T data, T s) {
    s &= _TriMask<T,S>::Mask ();

    mData = (data & s) | (mData & ~s);

    // Or-in these drivers to anything already being driven.
    setStrength (s);
  }

  //! Set one tristate based on another.  Unlike operator=, this obeys strength.
  void set (const Tristate& value)
  {
    set(value.mData, value.getStrength());
  }

  // Basic pullup
  void set (void) { mStrength = 0; mData = _TriMask<T,S>::Mask (); }

  void setunmasked (void) {
    mData |= (~mStrength & _TriMask<T,S>::Mask ());
  }
    
#if 0
  //! export Tristate from internal form to external
  void driveOut (T xdata[3])
  {
    xdata[eTriData] &= ~mStrength;		// Clear bits we intend to drive
    xdata[eTriData] |= (mData & mStrength); 	// Drive them
    xdata[eTriIdrive]
      = (~mStrength & _TriMask<T,S>::Mask ());  // Set mask of driving signals
  }
#endif

  Tristate* getRef() {
    return this;
  }

  //! Used by the debug_generator to get the internals of the class
  void getInternalPtrs(T** data, T** drive)
  {
    *data = &mData;
    *drive = &mStrength;
  }
  
};

// Just like a Tristate, but must save external drive for comparisons.
template <class T, UInt32 S> class Bidirect: public Tristate<T,S>
{
  //! XDrive indicates if externally driven on last simulation cycle
  T mXDrive;			// Saved xdrive value
  T mXData;			// Saved user-deposited value

public:
  typedef typename MASKTYPE (T) masktype;
  typedef T* ptrtype;

  //! Default Constructor
  explicit Bidirect () : Tristate<T,S>(), mXDrive (_TriMask<T,S>::Mask ()), mXData (0) {}

  //! Copy Constructor
  Bidirect (const Bidirect& bidi) : Tristate<T,S> (bidi),
			     mXDrive (bidi.mXDrive), mXData (bidi.mXData)
  {}

  //! Universal constructor
  explicit Bidirect (const T data, const T s=0, const T x=0, const T d=0)
    : Tristate<T,S> (data,s), mXDrive (x), mXData (d) {}


  //! Conversion operator to base class
#if !pfGCC_2
  operator const T&() const { return this->mData; }
#endif
  //! Overloaded
  operator T&() { return this->mData; }

  //! Assignment from simple integer
  Bidirect& operator =(const T& value)
  {
    this->mData = value;
    return *this;
  }

  //!  used by structapi ???
  void resetExternal(const T s) {
    mXDrive = s;
  }

  //! Used by the debug_generator to get the internals of the class
  void getInternalPtrs(T** idata, T** idrive, T** xdata, T** xdrive)
  {
    Tristate<T,S>::getInternalPtrs(idata, idrive);
    *xdata = &mXData;
    *xdrive = &mXDrive;
  }

  //! Access the saved external drive mask
  T& getXDrive () const{ return (T&)mXDrive; }

  //! Access the saved external data
  /*! Note that the shell does a read-modify write on the data so this
   *  includes the deposited and current value for the data.
   */
  T& getXData () const{ return (T&)mXData; }

  void setXDrive (const T& xd) { mXDrive = xd; }
  void setXData (const T& xd) { mXData = xd; }
};

#ifdef CARBON_SAVE_RESTORE
// i/o support for save & restore
template <typename T, UInt32 S> 
CARBON_SECTION("carbon_checkpoint_code")
void writeToStream(UtOCheckpointStream* f, const Tristate<T,S>& v)
{
  writeToStream(f, v.getT());
  writeToStream(f, v.getStrength());
}

template <typename T, UInt32 S> 
CARBON_SECTION("carbon_checkpoint_code")
void readFromStream(UtICheckpointStream* f, Tristate<T,S>& v)
{
  T value;

  readFromStream(f, value);
  v = value;

  readFromStream(f, value);
  v.setStrength (value);
}

template <typename T, UInt32 S> 
CARBON_SECTION("carbon_checkpoint_code")
void writeToStream(UtOCheckpointStream* f, const Bidirect<T,S>& v)
{
  writeToStream(f, v.getT());
  writeToStream(f, v.getStrength());
  writeToStream(f, v.getXDrive());
  writeToStream(f, v.getXData());
}

template <typename T, UInt32 S> 
CARBON_SECTION("carbon_checkpoint_code")
void readFromStream(UtICheckpointStream* f, Bidirect<T,S>& v)
{
  T value;

  readFromStream(f, value);
  v = value;

  readFromStream(f, value);
  v.setStrength (value);

  readFromStream(f, value);
  v.setXDrive (value);

  readFromStream(f, value);
  v.setXData (value);
}
#endif //CARBON_SAVE_RESTORE

#endif
