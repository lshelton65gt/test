/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef __UNROLLCONCAT_H_
#define __UNROLLCONCAT_H_

/*!
 * Handle operand expansion for both concatenation classes
 * (CRepeatRHS, CRepeatLHS).  In both situations we want to create a
 * vector of value:shift pairs (lvalues or rvalues depending on which
 * concat type we're handling) and use that table to drive code
 * generation.
 *
 * Currently shared between backend and codegen.  The intention is to move all
 * this code into backend, removing all nucleus creation from codegen.
 *
 * \arg concat the NUxxx concatenation node
 * \arg table pointer to collection of std::pair<const NUxxx*, size_t>
 * \arg shiftCount previous field size.
 * \arg context message context allowing error message output.
 * \returns filled in collection and size of field.
 */
template <typename T, typename R, typename PAIR> size_t
unrollConcat (const T& concat, UtVector<PAIR> *table, size_t shiftCount, MsgContext* context)
{
  size_t nops = concat.getNumArgs ();	// Number of operands to concat

  // Go backward thru the list getting the operands.

  R repeatCount (&concat);

  for (int j = repeatCount.getRepeatCount (); j > 0; --j)
    {
      for (int i=nops-1; i>=0; --i)
	{
	  // common base class for NUConcatOp and NUConcatLvalue
	  const NUUseNode *cat = concat.getArg (i);
	  const T* nested = dynamic_cast<const T*>(cat);

	  // If this operand is a concat TOO, then recurse
	  if (nested)
	    shiftCount = unrollConcat<T,R,PAIR> (*nested, table, shiftCount, context);
	  else
	    {
              typename PAIR::first_type arg (concat.getArg (i));

              if (arg->sizeVaries ())
                context->CGVaryingSizeExpression (&arg->getLoc ());

	      table->push_back (PAIR(arg,shiftCount));
	      shiftCount = arg->getBitSize ();
	    }
	}
    }

  // Now the table is ordered in reverse.  First entry is low bits, and
  // the pair.second is the amount to shift this value to get it out of the way

  // If we are recursing, return the size of the last field - it's the amount
  // we need to shift the "next" field in order to make room for that field when
  // we build the 
  return shiftCount;
}

//! Used privately to fool unrollConcat() into handling LHS
//! which doesn't actually have a getRepeatCount() method.
//!

class CRepeatLHS
{
public:
  CRepeatLHS (const NUConcatLvalue*){}
  size_t getRepeatCount (void) const { return 1;}
};

// Similarly to fool RHS concats.
class CRepeatRHS
{
  const NUConcatOp* mExpr;
public:
  CRepeatRHS (const NUConcatOp *r): mExpr (r) {}

  size_t getRepeatCount (void) const { return mExpr->getRepeatCount ();}
};
#endif
