/* -*- C++ -*- */
/*****************************************************************************

 Copyright (c) 2009-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  This is defines types used by Carbon Model Studio in the generated code that implements
  a CADI class that interfaces with a Carbon model.
*/
#ifndef _CarbonCADITypes_h_
#define _CarbonCADITypes_h_

#include "carbon/carbon_shelltypes.h"
#include "carbon/carbon_capi.h"
#include "eslapi/CADI.h"

#include <string>
#include <vector>

static inline void rightShift_u32(CarbonUInt32 *data, int numWords, int shift);


//! Structure used when initializing the CADI interface
struct SimpleRegInfo_t {
  char*                    name;        // name of the register to display in the GUI
  unsigned int             bitsWide;    // number of bits in register
  eslapi::CADIRegDisplay_t display;     // eslapi::CADI_REGTYPE_BOOL,HEX,INT,UINT,FLOAT,STRING
  char*                    description;
};

//! Structure used when initializing the CADI interface
struct GroupInfo_t {
  char *description;
  int   start;       // index into array of SimpleRegInfo_t of first reg in group
  int   end;         // index of last reg in group
};

//! Structure used when initializing the CADI interface
struct MemSpaceInfo_t {
  char*                       memSpaceName;
  unsigned int                bitsPerMau;
  eslapi::CADIAddrSimple_t    maxAddress;
  int                         isProgramMemory;
  unsigned int                cyclesToAccess;
  eslapi::CADIMemReadWrite_t  readWrite;
  char*                       description;
};

//! Base class for CADI debug registers
class CarbonDebugRegister
{
public:
  CarbonDebugRegister(CarbonObjectID* obj)
: mObj(obj) { };

  virtual ~CarbonDebugRegister() { };
  virtual void reset() = 0;
  virtual void read(CarbonUInt32 *buf) = 0;
  virtual void write(CarbonUInt32 *buf) = 0;
  virtual void readOffset(CarbonUInt32 *buf, CarbonUInt32 offset, CarbonUInt32 shift, CarbonUInt32 mask) = 0;
  virtual void writeOffset(CarbonUInt32 *buf, CarbonUInt32 offset, CarbonUInt32 shift, CarbonUInt32 mask) = 0;

protected:
  // Pointer to Carbon Model object
  CarbonObjectID* 	mObj;
};

//! Interface class for CADI memory accesses
class CarbonDebugMemoryBlock {
public:
  //! Constructor
  CarbonDebugMemoryBlock(const char* name, uint64_t base, uint64_t size, eslapi::CADIMemReadWrite_t access):
    mName(name),
    mBaseAddr(base),
    mSize(size),
    mAccess(access)
  {}

  virtual ~CarbonDebugMemoryBlock()
  {}

  //! Memory Access Methods
  virtual bool memWrite(uint64_t startAddress, uint32_t unitsToWrite, uint32_t unitSizeInBytes, const uint8_t *data, uint32_t *actualNumOfUnitsWritten, uint8_t doSideEffects)=0;
  virtual bool memRead(uint64_t startAddress, uint32_t unitsToRead, uint32_t unitSizeInBytes, uint8_t *data, uint32_t *actualNumOfUnitsRead, uint8_t doSideEffects)=0;

  virtual eslapi::CAInterface* getWarmCacheIF() { return NULL; }

  //! Block Address Decoder, here startAddr is an address in terms of unitSizeInBytes units
  virtual bool decode(uint64_t startAddr, uint32_t requestedUnits, uint32_t unitSizeInBytes, uint32_t *actualUnits) const
  {
    uint64_t startByteAddress = startAddr * unitSizeInBytes;
    bool  result    = false;

    // If not a hit, actualUnits has to reflect that nothing was accessed
    *actualUnits    = 0;

    // First Check address for this block to see if it's a hit
    if((startByteAddress >= mBaseAddr) && (startByteAddress < (mBaseAddr + mSize))) {
      result = true;
      // Now check if the entire access fits in this block
      int64_t bytesFromEnd = (mSize+mBaseAddr) - (startByteAddress + (requestedUnits*unitSizeInBytes));
      
      // If this number is negative we would have gone out of bound, so limit the access to fewer units
      if(bytesFromEnd < 0)
        *actualUnits = (uint32_t)(requestedUnits + (bytesFromEnd/unitSizeInBytes));
      else {
        *actualUnits = (uint32_t)requestedUnits;
      }
    }
    return result;
  }

  //! Name
  const char* name() const
  {
    return mName.c_str();
  }

  //! Base Address
  uint64_t base() const
  {
    return mBaseAddr;
  }

  //! Size
  uint64_t size() const
  {
    return mSize;
  }
  
  //! Access Type
  eslapi::CADIMemReadWrite_t access() const
  {
    return mAccess;
  }
  
private:
  std::string                mName;
  uint64_t                   mBaseAddr;  // in bytes
  uint64_t                   mSize;      // in bytes
  eslapi::CADIMemReadWrite_t mAccess;
};

//! Base Class for CADI Memories
class CarbonDebugMemory {
public:
  
  //! Desctructor
  virtual ~CarbonDebugMemory()
  {
    for (std::vector<CarbonDebugMemoryBlock*>::iterator iter = mBlocks.begin();
	 iter != mBlocks.end();
	 ++iter) {
      delete (*iter);
    }
    mBlocks.clear();
  }

  //! Memory Access Methods, must be implemented by derived class
  virtual bool memWrite(uint64_t startAddress, uint32_t unitsToWrite, uint32_t unitSizeInBytes, const uint8_t *data, uint32_t *actualNumOfUnitsWritten, uint8_t doSideEffects)=0;
  virtual bool memRead(uint64_t startAddress, uint32_t unitsToRead, uint32_t unitSizeInBytes, uint8_t *data, uint32_t *actualNumOfUnitsRead, uint8_t doSideEffects)=0;
  
  virtual eslapi::CAInterface* getWarmCacheIF() 
  { 
    // this assumes only one memory block
    if (mBlocks.size() > 0)
      return mBlocks[0]->getWarmCacheIF();
    return NULL; 
  }


  //! Number of blocks in the memory
  uint32_t numBlocks()
  {
    return mBlocks.size();
  }

  //! Name of given block
  const char* blockName(int index)
  {
    return mBlocks[index]->name();
  }

  //! Start Address of given block
  uint64_t blockStart(int index) const
  {
    return mBlocks[index]->base();
  }

  //! End Address of given block
  uint64_t blockEnd(int index) const
  {
    return mBlocks[index]->base() + mBlocks[index]->size() -1;
  }

  //! Size of given block
  uint64_t blockSize(int index) const
  {
    return mBlocks[index]->size();
  }

  eslapi::CADIMemReadWrite_t blockAccess(int index) const
  {
    return mBlocks[index]->access();
  }

  CarbonStatus Readmemh(CarbonObjectID* memoryObject, const char* rtlMemPath, const char* filename)
  {    
    CarbonMemoryID* signal = carbonFindMemory(memoryObject, rtlMemPath);
    if (signal != NULL)
      return carbonReadmemh(signal, filename);
    else
      return eCarbon_ERROR;
  }

  CarbonStatus Readmemb(CarbonObjectID* memoryObject, const char* rtlMemPath, const char* filename)
  {    
    CarbonMemoryID* signal = carbonFindMemory(memoryObject, rtlMemPath);
    if (signal != NULL)
      return carbonReadmemb(signal, filename);
    else
      return eCarbon_ERROR;
  }


protected:
  std::vector<CarbonDebugMemoryBlock*> mBlocks;
};

// was used as Wrapper around memcpy to consider endianness when copying a
// CarbonUInt32 array to a uint8_t array, but now there is only support for littleEndian
// MaxSim always treats the uint8_t array as little endian
static inline void memcpy32to8(uint8_t *dest, const CarbonUInt32 *src, CarbonUInt32 bytes)
{
  memcpy(dest, src, bytes);
}

// was used as Wrapper around memcpy to consider endianness when copying a
// uint8_t array to a CarbonUInt32 array, but now there is only support for littleEndian
// MaxSim treats the uint8_t array as little endian, regardless of platform.
static inline void memcpy8to32(CarbonUInt32 *dest, const uint8_t *src, CarbonUInt32 bytes)
{
  memcpy(dest, src, bytes);
}

//! Utility function to inline left shift a value represented by an array of UInt32's
static inline void leftShift_u32(CarbonUInt32 *data, int numWords, int shift)
{
  // If shift is negative, we're going the wrong way.
  if(shift < 0) {
    rightShift_u32(data, numWords, -shift);
    return;
  }

  // First break down the shift to full UInt32's and bits
  int wordShift = shift/32;
  int bitShift  = shift % 32;

  // Now if the word shift is larger or equal to number of words, we should shift in all 0's
  if(wordShift >= numWords)
    memset(data, 0, numWords*sizeof(CarbonUInt32));
  else {
    for(int i = numWords-1; i >= wordShift; --i) {
      data[i] = data[i-wordShift] << bitShift;
      // Shift in lower order word, if it exists
      if( (i-wordShift-1) >= 0 && bitShift > 0 && wordShift > 0) {
        data[i] |= (data[i-wordShift-1] >> (32-bitShift));
      }
    }
    // Shift in zeros at the end
    if(wordShift > 0) memset(data, 0, wordShift*sizeof(CarbonUInt32));
  }
}

//! Utility function to inline left shift a value represented by an array of UInt32's
static inline void rightShift_u32(CarbonUInt32 *data, int numWords, int shift)
{
  // If shift is negative, we're going the wrong way.
  if(shift < 0) {
    leftShift_u32(data, numWords, -shift);
    return;
  }

  // First break down the shift to full UInt32's and bits
  int wordShift = shift/32;
  int bitShift  = shift % 32;

  // Now the word shift is larger or equal to number of words, we would shift in all 0's
  if(wordShift >= numWords)
    memset(data, 0, numWords*sizeof(CarbonUInt32));
  else {
    for(int i = 0; i < numWords-wordShift; ++i) {
      data[i] = data[i+wordShift] >> bitShift;
      // Shift in higher order word, if it exists
      if( (i+wordShift+1) < numWords && bitShift > 0 && wordShift > 0) data[i] |= (data[i+wordShift+1] << (32-bitShift));
    }
    // Shift in zeros at the end
    if(wordShift > 0) memset(&data[numWords-wordShift], 0, wordShift*sizeof(CarbonUInt32));
  }
}

//! Utility Function to create a mask that can span multiple UInt32 words
static void inline createMask_u32(CarbonUInt32 *data, CarbonUInt32 numWords, CarbonUInt32 width, CarbonUInt32 shift)
{
  // Fist clear data
  memset(data, 0, numWords*sizeof(CarbonUInt32));
  
  // Break down the shift to full UInt32's and bits
  CarbonUInt32 wordWidth = width/32;
  CarbonUInt32 bitWidth  = width % 32;

  // Create 0 aligned mask by first setting full words
  for(CarbonUInt32 i = 0; i < wordWidth; ++i) {
    if(i > numWords) break; // Don't go over buffer size
    data[i] = (CarbonUInt32)-1;
  }

  // Then set mask in partial word
  if(wordWidth < numWords) {
    data[wordWidth] = (1<<bitWidth)-1;
  }

  // Last shift the mask to the right position
  leftShift_u32(data, numWords, shift);
}

//! Helper class for Internal memory map information collected from IP-XACT file
// This stores the memory map(s) for a specific slave interface (LHS)
class CarbonInternalMemMapEntry {
public:
  CarbonInternalMemMapEntry(int numRegions) : mNumRegions(numRegions)
  {
    start = new CarbonUInt64[numRegions];
    size  = new CarbonUInt64[numRegions];
    name  = new std::string[numRegions];
  }
  ~CarbonInternalMemMapEntry()
  {
    delete [] start;
    delete [] size;
    delete [] name;
  }

  // returns true if addr is within a defined region
  bool isHit(CarbonUInt64 addr) const
  {
    for (int i=0; i<mNumRegions; ++i)
    {
      if ((addr >= start[i]) && (addr < (start[i]+size[i])))
        return true;
    }
    return false;
  }

  int getNumRegions(){
    return mNumRegions;
  }
  
  CarbonUInt64* start;
  CarbonUInt64* size;
  std::string* name;
private:
  int mNumRegions;
};

#endif

