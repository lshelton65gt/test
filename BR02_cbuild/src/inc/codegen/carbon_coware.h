// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Classes to support CoWare models
*/

#ifndef _CARBON_COWARE_H_
#define _CARBON_COWARE_H_

/*
 * Base abstract class to perform reads/writes to blocks of a memory in a uniform way
 */
template<typename _DT>
class CarbonScMemoryBlockBase
{
public:
  //! constructor
  CarbonScMemoryBlockBase() {}

  //! destructor
  virtual ~CarbonScMemoryBlockBase() {}
    
  //! Function to initialize the memory handle
  virtual void initialize(CarbonObjectID* obj, const char* rtlPath) = 0;

  //! Function to do a single or burst read
  virtual void read(unsigned int index, unsigned int wordSize, unsigned int offset, unsigned int dataSize,
                    _DT* readData) = 0;

  //! Function to do a single or burst write
  virtual void write(unsigned int index, unsigned int wordSize, unsigned int offset, unsigned int dataSize,
                     const _DT* writeData) = 0;

  //! Function to test if this block is in the index/bit range for the request
  virtual bool inRange(unsigned int index, unsigned int wordSize, unsigned int offset, unsigned dataSize) = 0;
};

/*
 * A class that defines a block of memory tied to an RTL pin
 */
template<typename _response_type, typename _request_type, typename _DT,
         CarbonUInt32 _blockStartAddr, CarbonUInt32 _blockWordSize,
         CarbonSInt64 _rtlStartAddr, CarbonSInt64 _rtlIncr,
         CarbonUInt32 _blockBitOffset, CarbonUInt32 _rtlBitOffset, CarbonUInt32 _blockBitSize>
class CarbonScMemoryBlock : public CarbonScMemoryBlockBase<_DT>
{
public:
  //! Constructor
  CarbonScMemoryBlock(const char* name) : CarbonScMemoryBlockBase<_DT>()
  {
    // Initialize creates these structures
    mName = name;
    mRtlHndl = NULL;
    mCarbonStorage = NULL;
    mMemoryStorage = NULL;
  }

  //! Destructor
  virtual ~CarbonScMemoryBlock()
  {
    delete[] mCarbonStorage;
    delete[] mMemoryStorage;
  }

  //! Function to initialize the memory handle
  void initialize(CarbonObjectID* obj, const char* rtlPath)
  {
    // TDB handle, failure in a better way
    mRtlHndl = carbonFindMemory(obj, rtlPath);
    assert(mRtlHndl != NULL);
    mCarbonStorage = new CarbonUInt32[carbonMemoryRowNumUInt32s(mRtlHndl)];
    mMemoryStorage = new CarbonUInt32[(sizeof(_DT)+3)/4];
  }

  //! Function to read some bits for the memory
  /*! Read a number of bits from the Carbon RTL memory into the
   *  response. This function assume that bits are in range.
   */
  void read(unsigned int index, unsigned int wordSize, unsigned int offset, unsigned int dataSize,
            _DT* readData)
  {
    // Figure out the start index and number of words to read in terms of the full memory
    unsigned int destAddr;
    unsigned int readSize;
    unsigned int addrOffset;
    computeStartAndSize(index, wordSize, _blockStartAddr, _blockWordSize, destAddr, readSize, addrOffset);

    // Compute the RTL source index from the destination index and this blocks information
    CarbonSInt64 rtlAddr = (_rtlStartAddr + ((CarbonSInt64)addrOffset * _rtlIncr));

    // Compute the destination bit and size as well as the source bit
    unsigned int destOffset;
    unsigned int readBitSize;
    unsigned int bitOffset;
    computeStartAndSize(offset, dataSize, _blockBitOffset, _blockBitSize, destOffset, readBitSize, bitOffset);

    // Compute the RTL source offset from the destination offset. We
    // create a structure to copy the data.
    unsigned int rtlOffset = _rtlBitOffset + bitOffset;
    CarbonAbstractRegisterID copyStruct = carbonAbstractRegisterCreate(rtlOffset, readBitSize, destOffset);
#if _CARBON_DEBUG_SCML
    cout << "CARBON-DEBUG: read: " << mName << ": abstract reg dest offset: " << destOffset
         << ", bitSize: " << readBitSize
         << ", RTL offset: " << rtlOffset
         << endl;
#endif

    // Now do the read
    for (unsigned int i = 0; i < readSize; ++i) {
      // Read from the Carbon memory for this word
      carbonExamineMemory(mRtlHndl, rtlAddr, mCarbonStorage);
#if _CARBON_DEBUG_SCML
      cout << "CARBON-DEBUG: read: " << mName << ": memory block read, addr: 0x" << hex << rtlAddr << dec << endl;
      for (int j = 0; j < carbonMemoryRowNumUInt32s(mRtlHndl); ++j) {
        cout << "CARBON-DEBUG: read: carbon data[" << j << "]: 0x" << hex << mCarbonStorage[j] << endl;
      }
#endif

      // Merge the data using the mMemoryStorage. We do a ready modify
      // write. I'm not sure if we can avoid the two mempy's and a
      // assume taht the _DT type can be used like an array of
      // uint32's.
      memcpy(mMemoryStorage, &readData[destAddr], sizeof(_DT));
      CarbonAbstractRegisterXfer(copyStruct, mCarbonStorage, mMemoryStorage);
      memcpy(&readData[destAddr], mMemoryStorage, sizeof(_DT));
#if _CARBON_DEBUG_SCML
      cout << "CARBON-DEBUG: read: " << mName << ": memory merge, addr: 0x" << hex << destAddr << dec << endl;
      for (int j = 0; j < (sizeof(_DT)+3)/4; ++j) {
        cout << "CARBON-DEBUG: read: memory data[" << j << "]: 0x" << hex << mMemoryStorage[j] << endl;
      }
#endif

      // Move to the next word
      rtlAddr += _rtlIncr;
      ++destAddr;
    }

    // Release memory
    carbonAbstractRegisterDestroy(copyStruct);
  } // void read

  //! Function to write some bits for the memory
  /*! Write a number of bits from the Carbon RTL memory into the
   *  response. This function assume that bits are in range.
   */
  void write(unsigned int index, unsigned int wordSize, unsigned int offset, unsigned int dataSize,
             const _DT* writeData)
  {
    // Figure out the start index and number of words to write in terms of the write data
    unsigned int srcAddr;
    unsigned int writeSize;
    unsigned int addrOffset;
    computeStartAndSize(index, wordSize, _blockStartAddr, _blockWordSize, srcAddr, writeSize, addrOffset);

    // Compute the RTL source index from the destination index and this blocks information
    CarbonSInt64 rtlAddr = (_rtlStartAddr + ((CarbonSInt64)addrOffset * _rtlIncr));

    // Compute the destination bit and size as well as the source bit
    unsigned int srcOffset;
    unsigned int writeBitSize;
    unsigned int bitOffset;
    computeStartAndSize(offset, dataSize, _blockBitOffset, _blockBitSize, srcOffset, writeBitSize, bitOffset);

    // Compute the RTL source offset from the destination offset. We
    // create a structure to copy the data.
    unsigned int rtlOffset = _rtlBitOffset + bitOffset;
    CarbonAbstractRegisterID copyStruct = carbonAbstractRegisterCreate(srcOffset, writeBitSize, rtlOffset);
#if _CARBON_DEBUG_SCML
    cout << "CARBON-DEBUG: write: " << mName << ": abstract reg src offset: " << srcOffset
         << ", bitSize: " << writeBitSize
         << ", RTL offset: " << rtlOffset
         << endl;
#endif

    // Now do the write
    for (unsigned int i = 0; i < writeSize; ++i) {
#if _CARBON_DEBUG_SCML
      cout << "CARBON-DEBUG: write: " << mName << ": memory write addr: 0x" << hex << index
           << ", data: 0x" << writeData[srcAddr] << dec << endl;
#endif

      // Read from the Carbon memory for this word
      carbonExamineMemory(mRtlHndl, rtlAddr, mCarbonStorage);
#if _CARBON_DEBUG_SCML
      cout << "CARBON-DEBUG: write: " << mName << ": memory block read, addr: 0x" << hex << rtlAddr << dec << endl;
      for (int j = 0; j < carbonMemoryRowNumUInt32s(mRtlHndl); ++j) {
        cout << "CARBON-DEBUG: write: read data[" << j << "]: 0x" << hex << mCarbonStorage[j] << endl;
      }
#endif

      // Merge the data using the mMemoryStorage. We do a read modify write
      memcpy(mMemoryStorage, &writeData[srcAddr], sizeof(_DT));
      CarbonAbstractRegisterXfer(copyStruct, mMemoryStorage, mCarbonStorage);
#if _CARBON_DEBUG_SCML
      cout << "CARBON-DEBUG: write: " << mName << ": memory data[0x" << hex << srcAddr
           << "] = 0x" << writeData[srcAddr] << dec << endl;
#endif

      // Write it back to the Carbon memory
      carbonDepositMemory(mRtlHndl, rtlAddr, mCarbonStorage);
#if _CARBON_DEBUG_SCML
      cout << "CARBON-DEBUG: write: " << mName << ": memory block write, addr: 0x" << hex << rtlAddr << dec << endl;
      for (int j = 0; j < carbonMemoryRowNumUInt32s(mRtlHndl); ++j) {
        cout << "CARBON-DEBUG: write: write data[" << j << "]: 0x" << hex << mCarbonStorage[j] << endl;
      }
#endif

      // Move to the next word
      rtlAddr += _rtlIncr;
      ++srcAddr;
    }
  } // void write

  //! Function to test if a request is in range
  bool inRange(unsigned int index, unsigned int wordSize, unsigned int offset, unsigned dataSize)
  {
    // Check if the word is in range
    if ((index < _blockStartAddr) && ((index + wordSize) <= _blockStartAddr)) {
      // It is before the start of this range
      return false;
    }
    if (index >= (_blockStartAddr + _blockWordSize)) {
      // It is after the end of this range
      return false;
    }

    // Check if the bits are in range
    if ((offset < _blockBitOffset) && ((offset + dataSize) <= _blockBitOffset)) {
      // It is before the start of this range
      return false;
    }
    if (offset >= (_blockBitOffset + _blockBitSize)) {
      return false;
    }

    // It must be in range
    return true;
  } // bool inRange

private:
  //! Helper function to compute the start and size for either a set of words or bits
  /*! This computes the access start and size for the data from the
   *  caller. For reads this is where to store the data in the _DT
   *  word and for writes it is where to get the data from the _DT
   *  words.
   */
  void computeStartAndSize(unsigned int start, unsigned int size, 
                           unsigned int blockStart, unsigned int blockSize,
                           unsigned int& actualStart, unsigned int& actualSize, unsigned int& blockOffset)
  {
    // Compute where in the _DT data we should start the access. We
    // also compute the offset into the block where we will perform
    // the access.
    if (blockStart > start) {
      // Some part of this data was for other blocks. So we need to
      // adjust the start.
      actualStart = blockStart - start;
      blockOffset = 0;
      actualSize = (size - actualStart);
    } else {
      // The start in this block. So we start at 0
      actualStart = 0;
      blockOffset = start - blockStart;
      actualSize = size;
    }

    // Adjust the size
    if (actualSize > (blockSize - blockOffset)) {
      actualSize = blockSize - blockOffset;
    }
  } // void computeStartAndSize

  //! String name for debug
  std::string mName;

  //! The RTL handle for Carbon access
  CarbonMemoryID* mRtlHndl;

  //! Storage for doing reads and writes from/to the Carbon memory
  CarbonUInt32* mCarbonStorage;

  //! Storage for doing reads and writes from/to the SCML memory
  CarbonUInt32* mMemoryStorage;
}; // class CarbonScMemoryBlock

/*
 * Class to maintain a set of memory blocks and perform reads and writes
 */
template<typename _request_type, typename _response_type, typename _DT>
class CarbonScMemory
{
public:
  //! constructor
  CarbonScMemory(const char* name) : mName(name) {}

  //! destructor
  ~CarbonScMemory() {}

  //! Add a block to the set that needs to be searched
  void addBlock(CarbonScMemoryBlockBase<_DT>* memoryBlock)
  {
    mMemoryBlocks.push_back(memoryBlock);
  }

  //! Transport function to implement reads and writes
  _response_type transport(const _request_type& req, scml_memory<_DT>& mem)
  {
    // Get the pertinent request details
    PVType reqType = req.getType();
    unsigned int burstCount = req.getBurstCount();
    _response_type resp = req.obtainResp();
    unsigned long long address = req.getAddress();
    unsigned int index = mem.addressToIndex(address);
    unsigned int offset = req.getOffset();
    unsigned int dataSize = req.getDataSize();

    // Assume this process succeeds
    resp.setResponse(pvOk);

    // Figure out the size of the access
    unsigned int wordSize;
    if (burstCount <= 1) {
      wordSize = 1;
    } else {
      wordSize = burstCount;
    }

    // Perform the read/write operation
    if (reqType == pvRead) {
      // Read
#if _CARBON_DEBUG_SCML
      cout << "CARBON-DEBUG: transport: " << mName << ": memory read, index: 0x" << hex << index
           << ", memory index: " << index
           << ", wordSize: " << dec << wordSize
           << ", offset: " << offset
           << ", dataSize: " << dataSize
           << endl;
#endif
      _DT* readData = req.getReadDataDestination();
      for (int i = 0; i < mMemoryBlocks.size(); ++i) {
        CarbonScMemoryBlockBase<_DT>* memoryBlock = mMemoryBlocks[i];
        if (memoryBlock->inRange(index, wordSize, offset, dataSize)) {
          memoryBlock->read(index, wordSize, offset, dataSize, readData);
        }
      }

    } else {
      // Write
#if _CARBON_DEBUG_SCML
      cout << "CARBON-DEBUG: transport: " << mName << ": memory write, index: 0x" << hex << index
           << ", wordSize: " << dec << wordSize
           << ", offset: " << offset
           << ", dataSize: " << dataSize
           << endl;
#endif
      _DT* writeData = req.getWriteDataSource();
      for (int i = 0; i < mMemoryBlocks.size(); ++i) {
        CarbonScMemoryBlockBase<_DT>* memoryBlock = mMemoryBlocks[i];
        if (memoryBlock->inRange(index, wordSize, offset, dataSize)) {
          memoryBlock->write(index, wordSize, offset, dataSize, writeData);
        }
      }
    }

    return resp;
  } // _response_type transport

private:
  //! The name for debugging
  std::string mName;

  //! Storage for the sub blocks
  std::vector<CarbonScMemoryBlockBase<_DT>*> mMemoryBlocks;
}; // class CarbonScMemory

#endif // _CARBON_COWARE_H_
