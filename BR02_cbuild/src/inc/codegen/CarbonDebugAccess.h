// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _CarbonDebugAccess_h_
#define _CarbonDebugAccess_h_

enum CarbonDebugAccessStatus {
  eCarbonDebugAccessStatusOk,        // Access was successfully completed for entire transaction
  eCarbonDebugAccessStatusPartial,   // Access was Partially completed 
  eCarbonDebugAccessStatusOutRange,  // Access was out of range, no access was completed
  eCarbonDebugAccessStatusError      // Some error occured, no access was completed
};

enum CarbonDebugAccessDirection {
  eCarbonDebugAccessRead,
  eCarbonDebugAccessWrite
};

#endif
  
