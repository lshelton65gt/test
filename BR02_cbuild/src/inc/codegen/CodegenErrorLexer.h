// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*! \file
 *
 * Wrapper for the libdesign.codegen.error scanner.
 *
 * Two environment variable turn on tracing for debugging the lexer. Set
 * CARBON_ERRORS_FLEX_DEBUG=1 to set yyflex_debug for this scanner. This causes
 * flex to trace the rules that are triggered while scanning. Set
 * CARBON_ERRORS_DEBUG to the callbacks up from the lexer.
 */

#ifndef _CODEGEN_ERROR_LEXER_H_
#define _CODEGEN_ERROR_LEXER_H_

class MsgContext;

//! \class CodegenError
/*! This abstract class wraps the flex-derived libdesign.codegen.error
 *   scanner. The base class defines abstract virtual callback methods that are
 *   called by the scanner when specific constructs are recognised in the
 *   scanner file.
 */
class CodegenErrorLexer {
public: CARBONMEM_OVERRIDES

  enum Options {
    cYYDEBUG = 1<<0,                    //!< enable the flex debugging output
    cDEBUG = 1<<1                       //!< enable the CodegenErrorLexer debugging output
  };

  CodegenErrorLexer (MsgContext &msg_context, const char *filename, const UInt32 flags);
  virtual ~CodegenErrorLexer () {}

  //! scan a file using the flex scanner
  /*! Open the file and call the scanner.
   *  \param filename Name of the file to scan
   *  \return iff the file could be opened for scanning
   *  \note If the file could not be opened a suitable error message is output.
   *  \note The scanner will callback to the callback methods of this object.
   */
  bool scan ();

  // Callback methods

  //! Called when a warning is found
  void foundWarning () { mNWarnings++; }

  //! \return the number of warnings
  UInt32 numWarnings () const { return mNWarnings; }

  //! Called when a "SpeedCC: ..." message is recognised.
  virtual void SpeedCC (const char *) {}

  //! Called when "filename:lineno: severity: ..." is recognised
  virtual void CxxError (const char *, const UInt32, const char *) {}

  //! Called when "...Make...:lineno: *** ..." is recognised
  /*! \note These messages are written by the make $(error ...) construct */
  virtual void MakeError (const char *, const UInt32, const char *) {}

  //! A make error without a filename or a linenumber
  /*! \note This will include output of the form:
   *  \verbatim
   *     make: *** [f_000000.o] Error 1
   *     make: *** Waiting for unfinished jobs....
   *  \endverbatim
   *  Not much usefull can be done with these.
   */
  virtual void MakeError (const char *) {}

  //! Output a debug message if mDebugging is set
  void Debug (const char *fmt, ...);

  //! \return the message context
  MsgContext &getMsgContext () { return mMsgContext; }

protected:

  MsgContext &mMsgContext;              //!< whining stream
  const UInt32 mFlags;                  //!< options
  bool mDebugging;                      //!< set when the base class output debugging information
  const char *mFilename;                //!< name of the libdesign.codegen.errors file
  UInt32 mNWarnings;                    //!< number of warnings found
};

#endif
