// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __Forcible_h_
#define __Forcible_h_

#ifndef __carbon_priv_h_
#include "codegen/carbon_priv.h"
#endif

#ifndef __BitVector_h_
#include "util/BitVector.h"
#endif


// A templatized template of Forcible<T> which also specializes on
// non-POD types such as BitVector<N>, Tristate<T> where the mask is
// not the same type as the forced-object.  This is needed to
// distinguish between BitVector Forcibles and POD Forcibles.
//
// Since we can't depend on free integral coercions for arithmetic
// involving BitVectors or Tristates, we have to inherit behavior
// from the base class
//
template <typename T, typename MT=T> class Forcible
  : public T
{
private:
  MT mMask;

  //! Make simple assignment unavailable
  Forcible& operator=(const Forcible& src);

  //! Copy Constructor is suppressed also - only pass-by-reference allowed
  Forcible (const Forcible& v);

public:
  //! Trivial Constructor (Carbon toplevel class will initialize data)
  Forcible ():T (), mMask (MT (0u)) {}

  //! Less trivial constructor, No forced bits by default.
  // Note that we initialize from a simple type value NOT the complex type
  Forcible (const T value) : T(value), mMask (MT (0u)) {}

  //! Constructor by pieces
  Forcible (const T data, const MT s) : T (data), mMask (s) {}

  //! Trivial destructor
  ~Forcible () {}

  // All non-pods must supply this
  typedef MT masktype;

#if 0
  // Can't have this, because when T=MT, we have two identical signatures.
  // I think we can live without being able to assign a Tristate or
  // StateUpdate to a Forcible<Tristate<U>,U>
  Forcible& operator=(const T& src) { return setData (src); }
#endif

  //! Access the forcing mask
  const MT& getMask () const { return mMask ;}

  template<bool _stype> const BVref<_stype> 
  getMask (UInt32 pos, UInt32 size) const {
    return mMask.lpartsel (pos,size);
  }

  MT* getForceMask () const { return (MT*) &mMask; }

  T* getStorage () const { return (T*) this; }

  void setMask () { mMask.set (); }

  void setMask (size_t pos, size_t size) {
    mMask.lpartsel (pos, size).set ();
  }

  void setMask (const MT s) { mMask = s; }

  //! clear mask
  void clearMask () {
    mMask.reset ();
  }

  //! Clear part of a mask
  void clearMask (const MT& s) {
    mMask &= ~s;
  }

  //! Clear part of a mask using pos/size
  void clearMask (size_t pos, size_t size) {
    mMask.lpartsel (pos, size).reset ();
  }

  // Just update the signal with the underlying value
  Forcible& setData (const MT & adata) {
    // Mask off the pieces that are forced so we can't change 'em
    MT data  = adata & ~mMask;
    
    (*this) &= mMask;           // Clear unforced bits
    (*this) |= data;            // Set unforced bits.

    return *this;
  }

  const Forcible& operator=(const MT& src) {
    return setData (src);
  }

  // We don't need conversion operators to downcast to T
  // That comes for free from deriving directly from class T
  // But, if class T is itself something that depends on conversion
  // to a simpler type in order to do operations, then
  // C++ won't do an implicit conversion followed by an
  // explicit conversion. (consider, for example:
  //
  //	Forcible<Bidirect<UInt1>, UInt1> x;  x = 1;
  //
  // we have to define conversion operations to bridge that gap.

  // icc warning #597 occurs here when T == MT (can ignore)
  operator T&() {
    return (T&) *this;
  }

  // icc warning #597 occurs here when T == MT (can ignore)
  operator const T&() const {
    return (const T&) *this;
  }

  //! simple accessor for data
  const T& getData() const { return *(const T*)this; }

  //! Overloaded for non-const object
  T& getData () { return *(T*)this;}

  //! Return base type (work around 2.95 bug)
  T& getT () { return *(T*)this; }

  //! Overload
  const T& getT () const { return *(T*)this; }

};

#ifdef CARBON_SAVE_RESTORE
// i/o support for save & restore
template <typename T, typename MT> 
CARBON_SECTION("carbon_checkpoint_code")
void writeToStream(UtOCheckpointStream* f, const Forcible<T,MT>& v)
{
  writeToStream(f, v.getData());
  writeToStream(f, v.getMask());
}

template <typename T, typename MT> 
CARBON_SECTION("carbon_checkpoint_code")
void readFromStream(UtICheckpointStream* f, Forcible<T,MT>& v)
{
  T value;

  readFromStream(f, value);
  v.setData (value);

  MT mask;
  readFromStream(f, mask);
  v.setMask (mask);
}

#endif //CARBON_SAVE_RESTORE


#endif

/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
