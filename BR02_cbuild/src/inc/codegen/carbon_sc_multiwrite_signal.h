/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __carbon_sc_multiwrite_signal_h_
#define __carbon_sc_multiwrite_signal_h_

#if defined(SYSTEMC_VERSION) && SYSTEMC_VERSION < 20070314
// If using SystemC older than 2.2.0, just use sc_signal.
#define carbon_sc_multiwrite_signal sc_signal
#else

// This class is just like SystemC's sc_signal<T>, but silently allows
// multiple writers.
template <class T>
class carbon_sc_multiwrite_signal
: public sc_signal<T>
{
public:

    // typedefs

    typedef carbon_sc_multiwrite_signal<T> this_type;
    typedef sc_signal<T> base_type;

public:

    // constructors

    carbon_sc_multiwrite_signal()
	: base_type( sc_gen_unique_name( "multiwrite_signal" ) )
	{}

    explicit carbon_sc_multiwrite_signal( const char* name_ )
	: base_type( name_ )
	{}


    // interface methods

    virtual void register_port( sc_port_base&, const char* );

    // write the new value
    virtual void write( const T& );


    // other methods

    carbon_sc_multiwrite_signal<T>& operator = ( const T& a )
	{ write( a ); return *this; }

    carbon_sc_multiwrite_signal<T>& operator = ( const base_type& a )
	{ write( a.read() ); return *this; }

    carbon_sc_multiwrite_signal<T>& operator = ( const this_type& a )
	{ write( a.read() ); return *this; }

    virtual const char* kind() const
        { return "carbon_sc_multiwrite_signal"; }

protected:

    virtual void update();

private:

    // disabled
    carbon_sc_multiwrite_signal( const carbon_sc_multiwrite_signal<T>& );
};


// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII

template <class T>
inline
void
carbon_sc_multiwrite_signal<T>::register_port( sc_port_base& port_, const char* if_typename_ )
{
    // sc_signal checks for multiple writers here, and reports an
    // error if multiple writers are detected.  This class allows
    // multiple writers.
}


// write the new value

template <class T>
inline
void
carbon_sc_multiwrite_signal<T>::write( const T& value_ )
{
    // sc_signal checks for multiple writers here, and reports an
    // error if multiple writers are detected.  This class allows
    // multiple writers.

    this->m_new_val = value_;
    if( !( this->m_new_val == this->m_cur_val ) ) {
	this->request_update();
    }
}


template <class T>
inline
void
carbon_sc_multiwrite_signal<T>::update()
{
    this->m_cur_val = this->m_new_val;
    if ( sc_signal<T>::m_change_event_p )
	    sc_signal<T>::m_change_event_p->notify(SC_ZERO_TIME);
    this->m_delta = sc_delta_count();
}

// Does this also need to override write() and update() for
// carbon_sc_multiwrite_signal<bool> and
// carbon_sc_multiwrite_signal<sc_logic>?

#endif
#endif
