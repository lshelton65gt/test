// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _STATIC_TEMP_MEMORY_H_
#define _STATIC_TEMP_MEMORY_H_

//! StaticTempMemory template class
/*!
 * This template class implements a temporary memory object for implementing
 * non-blocking memory writes.  It keeps an array of address/data pairs
 * which will get written into the real memory when temp goes out of scope.
 */
template <int _NumPorts, class _Memory>
class StaticTempMemory
{
public:
  typedef typename _Memory::value_type value_type;

  //! Structure to capture a write operation
  struct WriteRecord
  {
    SInt32   mAddress;
    value_type mData;
  };

  struct UnsortedCLoop {
    UnsortedCLoop(const StaticTempMemory& tmem):
      mPtr(&tmem.mWrites[0]),
      mEnd(&tmem.mWrites[tmem.mIndex])
    {}
    UnsortedCLoop():
      mPtr(NULL),
      mEnd(0)
    {}

    bool atEnd() const {return mPtr == mEnd;}
    void operator++() {++mPtr;}
    SInt32 getKey() const {return mPtr->mAddress;}
    const value_type& getValue() const {return mPtr->mData;}

  private:
    const WriteRecord* mPtr;
    const WriteRecord* mEnd;
  };

  friend struct UnsortedCLoop;

#if 0
  struct const_iterator {
    const_iterator(WriteRecord* wr): mWriteRecord(wr) {}
    const_iterator(): mWriteRecord(0) {}
    void operator++() {++mWriteRecord;}
    bool operator==(const const_iterator& other) {
      return mWriteRecord == other.mWriteRecord;
    }
    bool operator!=(const const_iterator& other) {
      return mWriteRecord != other.mWriteRecord;
    }
  };

  typedef const WriteRecord& const_iterator;

  const_iterator begin() const {return mWrites[0];}
  const_iterator end() const {return mWrites[mIndex];}
  typedef LoopMap<const StaticTempMemory, const_iterator> UnsortedCLoop;
#endif

  StaticTempMemory(const _Memory&src): mBaseMemory (const_cast<_Memory*>(&src)), mIndex(0) {}

  StaticTempMemory(): mBaseMemory (0), mIndex(0) {}

  ~StaticTempMemory() {}

  //! Function to get the msw of the memory
  inline SInt32 getMSW() const { return mBaseMemory->getMSW(); }

  //! Function to get the lsw of the memory
  inline SInt32 getLSW() const { return mBaseMemory->getLSW(); }

  SInt32 denormalize (SInt32 i) const {
    return mBaseMemory->denormalize(i);
  }

  SInt32 normalize (SInt32 i) const {
    return mBaseMemory->normalize(i);
  }

  //! Assignment operator from the memory to the temp
  template <typename MemType>
  StaticTempMemory& operator=(const MemType& src) {
    for (SInt32 i= src.eMemLowerBound; i <= src.eMemUpperBound; ++i) {
      SInt32 destaddr = mBaseMemory->denormalize(src.normalize(i));
      this->writeValueNoCheck (destaddr, src.readNoCheck (i));
    }
    return *this;
  }

  //! Assign from another static temp to this; assume same depth.
  StaticTempMemory& operator=(const StaticTempMemory & src) {
    for (SInt32 i = mBaseMemory->eMemLowerBound;
         i <= mBaseMemory->eMemUpperBound;
         ++i)
    {
      SInt32 destaddr = mBaseMemory->denormalize(src.normalize(i));
      this->writeValueNoCheck (destaddr, src.readNoCheck (i));
    }
    return *this;
  }

  //! Assign from a dynamic temp to this; assume same depth.
  StaticTempMemory& operator=(const DynTempMemory<_Memory> & src) {
    for (SInt32 i = mBaseMemory->eMemLowerBound;
         i <= mBaseMemory->eMemUpperBound;
         ++i)
    {
      SInt32 destaddr = mBaseMemory->denormalize(src.normalize(i));
      this->writeValueNoCheck (destaddr, src.readNoCheck (i));
    }
    return *this;
  }
    
  //! See if the address is already in the temp array or allocate
  WriteRecord& find (SInt32 i)
  {
    // Search for old value to get correct read-modify-write
    // behavior on partial row writes in Verilog 2001 & VHDL.
    // Verilog 95 does not have that capability but we don't know
    // that in codegen.  Adding this capability made StaticTempMemories
    // have n^2 write-behavior.  We should always use DynTempMemory
    // or keep a bitvector in StaticTempMemory to help us determine
    // whether we need to search.
    for(SInt32 k=0; k<mIndex; ++k)
    {
      WriteRecord& w = mWrites[k];
      if (w.mAddress == i)
        return w;
    }

    // Not there yet
#ifdef CDB
    ASSERT(mIndex < _NumPorts);
#endif
    WriteRecord& w = mWrites[mIndex++];
    w.mAddress = i;
    w.mData = mBaseMemory->read (i);
    return w;
  }
    
  //! See if the address is already in the temp array or allocate
  /*!
   *! This does not appear to check address bounds.  should it?
   */
  void writeValue(SInt32 i, const value_type& val)
  {
    for(SInt32 k=0; k<mIndex; ++k)
    {
      WriteRecord& w = mWrites[k];
      if (w.mAddress == i) {
        w.mData = val;
        return;
      }
    }

    // Not in there yet
#ifdef CDB
    ASSERT(mIndex < _NumPorts);
#endif
    WriteRecord& w = mWrites[mIndex++];
    w.mAddress = i;
    w.mData = val;
  }
    
  //! version of writeValue that is not expected to check address validity
  void writeValueNoCheck(SInt32 i, const value_type& val) {
    writeValue(i, val);
  }

  //! Function to assign locations of the memory.
  //! We never read from temp memories.
  value_type& write(SInt32 i)
  {
    return find (i).mData;
  }

  value_type& writeNoCheck(SInt32 i)
  {
    return write(i);
  }

  //! Read from the master if we don't have a local value.
  const value_type& read(SInt32 idx) const
  {
    for (SInt32 i = mIndex-1; i>=0 ; --i) {
      const WriteRecord & w = mWrites[i];
      if (w.mAddress==idx) {
        return w.mData;
      }
    }
    return mBaseMemory->read(idx);
  }

  //! Read from the master if we don't have a local value.
  const value_type& readNoCheck(SInt32 idx) const
  {
    for (SInt32 i = mIndex-1; i>=0 ; --i) {
      const WriteRecord & w = mWrites[i];
      if (w.mAddress==idx) {
        return w.mData;
      }
    }
    return mBaseMemory->readNoCheck(idx);
  }

  void executeQueuedWrites(bool update) const
  {
    for (SInt32 i = 0; i < mIndex; ++i) {
      const WriteRecord & w = mWrites[i];
      if (update) {
        mBaseMemory->directWrite(w.mAddress,w.mData);
      } else {
        mBaseMemory->writeValueNoCheck(w.mAddress, w.mData);
      }
    }
    mIndex = 0;
  } // void executeQueuedWrites

  void copyQueuedWrites(StaticTempMemory& src)
  {
    for (SInt32 i = 0; i < src.mIndex; ++i) {
      WriteRecord& w = src.mWrites[i];
      writeValueNoCheck(w.mAddress, w.mData);
    }
    src.mIndex = 0;
  }

  void putBaseMemory (_Memory* src) { mBaseMemory=src; }
  const _Memory* getBaseMemory () const { return mBaseMemory; }

private:
  //! Hide copy constructor
  StaticTempMemory(const StaticTempMemory&);

  //! Associated real memory
  _Memory *mBaseMemory;

  mutable SInt32 mIndex;
  WriteRecord mWrites[_NumPorts];
}; // class StaticTempMemory

#endif // _STATIC_TEMP_MEMORY_H_
