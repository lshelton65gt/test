// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef __CARBONDBWRITE_H_
#define __CARBONDBWRITE_H_

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/SourceLocator.h"
#include "symtab/STSymbolTable.h"
#include "exprsynth/ExprFactory.h"
#include "iodb/CbuildShellDB.h"

class HdlHierPath;
class SCHScheduleFactory;
class SCHSignature;
class NUBase;
class NUElabBase;
class ZostreamDB;
class ZistreamDB;
class NUConst;
class NUNet;
class IODBIntrinsic;
class IODB;
class CbuildSymTabBOM;
class DynBitVectorFactory;
class SymTabIdentBP;
class UserType;
class UserTypeFactory;

//! \class CSymTabSelect
/*! Selects the symbol table nodes that are written to the cbuild database.
 */
class CSymTabSelect : public STNodeSelectDB
{
public:
  //! constructor
  /*!
    \param dumpTemps If true, carbon-generated temporaries will
    allowed to be dumped
    \param bomManager The bom manager. This is needed for determining
    whether or not a node has an expression.
  */
  CSymTabSelect(bool dumpTemps, CbuildSymTabBOM* bomManager, IODB* iodb);

  //! virtual destructor
  virtual ~CSymTabSelect();

protected:
  //! True if leaf is to be written
  /*!
    Reasons for not writing a leaf:
    \li Leaf is a memory (currently, not supported)
    \li Leaf is a temporary and temps are not being written
    \li Leaf was specified in a list of non-db nets
  */
  virtual bool computeSelectLeaf(const STAliasedLeafNode* leaf, const HdlHierPath*);
  
  //! Currently selects all branches
  /*!
    This will change when selective scope database is implemented.
  */
  virtual bool computeSelectBranch(const STBranchNode* /*branch*/, const HdlHierPath* /*hdlPath*/) {
    return true;
  }
    
  virtual void requestAlwaysSelect(const STSymbolTableNode* node)
  {
    // No constraints on nodes to be selected
    mAlwaysSelect.insert(node);
  }

private:
  bool mDumpTemps;
  CbuildSymTabBOM* mBOMManager;
  IODB* mIODB;
};

//! \class ElaboratedGUISymTabSelect
/*! Selects the symbol table nodes that are written to the GUI visibility
 *  database.
 */

class ElaboratedGUISymTabSelect : public STNodeSelectDB
{
public:

  //! constructor
  ElaboratedGUISymTabSelect () {}

  //! virtual destructor
  virtual ~ElaboratedGUISymTabSelect() {}

protected:

  //! True if leaf is to be written
  virtual bool computeSelectLeaf(const STAliasedLeafNode* leaf, const HdlHierPath*);
  
  //! Currently selects all branches
  virtual bool computeSelectBranch(const STBranchNode *, const HdlHierPath *) 
  { return true; }
    
  virtual void requestAlwaysSelect(const STSymbolTableNode* node)
  {
    // No constraints on nodes to be selected
    mAlwaysSelect.insert(node);
  }
};

//! \class UnelaboratedGUISymTabSelect
/*! Selects the unelaborated symbol table nodes that are written to the GUI
 *  visibility database. Currently, all symbols for which source location
 *  information is available are written.
 */

class UnelaboratedGUISymTabSelect : public STNodeSelectDB
{
public:

  //! constructor
  UnelaboratedGUISymTabSelect () {}

  //! virtual destructor
  virtual ~UnelaboratedGUISymTabSelect() {}

protected:

  //! True if leaf is to be written
  virtual bool computeSelectLeaf(const STAliasedLeafNode* leaf, const HdlHierPath*);
  
  //! Currently selects all branches
  virtual bool computeSelectBranch(const STBranchNode *, const HdlHierPath *) 
  { 
    return true;                        // every branch is required
  }

  virtual void requestAlwaysSelect(const STSymbolTableNode* node)
  {
    // No constraints on nodes to be selected
    mAlwaysSelect.insert(node);
  }
};

//! BOM Manager for cbuild's symtab db
class CbuildSymTabBOM : public STFieldBOM
{
public:
  CbuildSymTabBOM();

  //! virtual destructor
  virtual ~CbuildSymTabBOM();

  //! Set the bitvector factory
  void putBVFactory(DynBitVectorFactory* bvFactory);

  //! STFieldBOM::allocLeafData()
  virtual Data allocLeafData();

  //! STFieldBOM::allocBranchData()
  virtual Data allocBranchData();
  
  //! STFieldBOM::freeLeafData()
  virtual void freeLeafData(const STAliasedLeafNode* leaf, Data* bomdata);

  //! STFieldBOM::freeBranchData()
  virtual void freeBranchData(const STBranchNode* branch, Data* bomdata);

  //! Write the BOMData for a leaf node
  virtual void writeLeafData(const STAliasedLeafNode* leaf, ZostreamDB& out) const;

  //! Write the BOMData for a branch node
  virtual void writeBranchData(const STBranchNode* branch, ZostreamDB& out,
                               AtomicCache* atomicCache) const;
  
  //! STFieldBOM::printLeaf()
  virtual void printLeaf(const STAliasedLeafNode* leaf) const;

  //! STFieldBOM::printBranch()
  virtual void printBranch(const STBranchNode* branch) const;

  //! Currently asserts - not legal
  virtual ReadStatus readLeafData(STAliasedLeafNode* leaf, ZistreamDB& in, 
                                  MsgContext* msg);
  
  //! Currently asserts - not legal
  virtual ReadStatus readBranchData(STBranchNode* branch, ZistreamDB& in, 
                                    MsgContext* msg);

  //! This calls the symboltable's writeDB method 
  bool writeData(ZostreamDB& out, STSymbolTable* symtab, STNodeSelectDB* selector) const;
  
  //! Write the r/w interface signature
  virtual void writeBOMSignature(ZostreamDB& out) const;
  
  //! Make the atomCache that holds the userTypes available for writing
  virtual void setAtomicCache(AtomicCache* atomCache);

  //! Copy string Atoms into current Atomic cache
  virtual void copyStringsIntoSymTable();

  //! Invoked by symbol table writer prior to BOM field dump to file.
  virtual void preFieldWrite(ZostreamDB& out);

  //! Returns eReadOK;
  virtual ReadStatus preFieldRead(ZistreamDB& in);
  
  //! Read the r/w interface signature
  virtual ReadStatus readBOMSignature(ZistreamDB& in, UtString*);


  //! Return BOM class name
  virtual const char* getClassName() const;

  //! Write the BOMData for a branch node
  virtual void xmlWriteBranchData(const STBranchNode* branch,UtXmlWriter* writer) const;

  //! Write the BOMData for a leaf node
  virtual void xmlWriteLeafData(const STAliasedLeafNode* leaf,UtXmlWriter* writer) const;

  //! Place a populated schedulefactory into the fieldBOM
  /*!
    This will iterate through the signatures and create a signature to
    index mapping to be written to the db.
  */
  void putScheduleFactory(SCHScheduleFactory* sf);

  //! Place a populated UserTypeFactory into the fieldBOM
  /*!
    This will write out the factory to the db, when it is being written out.
  */
  void putUserTypeFactory(UserTypeFactory* utf);

  //! Given a set of NUBase* return all associated NUElabBase*
  typedef UtHashSet<NUBase*> BaseSet;
  typedef UtHashSet<NUElabBase*> ElabBaseSet;
  typedef UtHashSet<STAliasedLeafNode*> LeafNodeSet;
  static void getMasters(BaseSet* unelab, LeafNodeSet* masters, STSymbolTable* table);
  static void getElabs(BaseSet* unelab, ElabBaseSet* elab, STSymbolTable* table);

  //! Get the expression factory
  ESFactory* getExprFactory();
  
  //! Map an identifier to an expression
  void mapExpr(CarbonIdent* ident, CarbonExpr* expr);

  //! Remove the mapped expression associated with ident
  /*!
    This makes the net that the ident represents not expressioned
    anymore. 
  */
  void removeMappedExpr(CarbonIdent* ident);

  //! Given an existing ident, get the describing expression
  /*!
    Returns NULL if the ident is not found or if there is a NULL
    expression describing the ident
  */
  CarbonExpr* getExpr(CarbonIdent* ident);
  //! const version of getExpr
  const CarbonExpr* getExpr(const CarbonIdent* ident) const;

  //! Return the ident->expression map
  CarbonIdentExprMap* getIdentExprMap() { 
    return &mExprMap;
  }

  //! Is this an expression mapped node?
  bool isExprMappedNode(const STSymbolTableNode* node) const;

  //! Get the expression that is mapped to this node
  /*!
    Returns NULL if the node is not an expression mapped node.
  */
  CarbonExpr* getMappedExpr(STSymbolTableNode* node);

  //! Get the leaf nodes of an expression that is mapped to this node
  /*! If the passed in symbol table node has an expression, it walks
   *  the expression, finds all the symbol table nodes in the idents,
   *  and populates the return vector.
   *
   *  \param node The symbol table leaf node to test
   *
   *  \param nodes A vector to fill in
   *
   *  \returns true if there is an expression, false otherwise.
   */
  bool getMappedExprLeafNodes(STAliasedLeafNode* node,
                              STAliasedLeafNodeVector* nodes);

  //! Get the original symbol table node if this is a port split subordinate
  /*!
   *  \param node A symbol table leaf node that may be part of 
   *  an expression.
   *
   *  \param origNodes The original symboltable nodes that the
   *  expression represents. This will be unmodified if the node is
   *  not an expression or is an expression that has no nets.
   */
  static void getOriginalNodes(const STAliasedLeafNode* node, STAliasedLeafNodeVector* origNodes);
  
  //! Get the sym tab back pointer if node is an expression subordinate or NULL
  static const SymTabIdentBP* getSymTabIdentBP(const STAliasedLeafNode* node);

  //! Gets base from node
  static NUBase* getNucleusObj(const STSymbolTableNode* node);
  //! places base on node
  static void putNucleusObj(STSymbolTableNode* node, NUBase* base);
  //! Gets the user type from the node
  static const UserType* getUserType(const STSymbolTableNode* node);
  //! Places the user type on the node
  static void putUserType(STSymbolTableNode* node, const UserType* ut);
  //! Gets storage offset from storage node of leaf
  static SInt32 getStorageOffset(const STAliasedLeafNode* leaf);
  //! If leaf is a storage node, place typeIndexTag into its bom
  static void setTypeTag(STAliasedLeafNode* leaf, int typeIndexTag);
  //! If leaf is a storage node, place typeIndex into its bom
  static void setTypeIndex(STAliasedLeafNode* leaf, UInt32 typeIndex);
  //! If leaf is a storage node, place storage offset into its bom
  static void setStorageOffset(STAliasedLeafNode* nLeaf, SInt32 symbolOffset);
  //! Get the CarbonIdent for this leaf, if it exists.
  static CarbonIdent* getIdent(const STAliasedLeafNode* leaf);
  //! Place the CarbonIdent for this leaf into its bom
  static void putIdent(STAliasedLeafNode* leaf, CarbonIdent* ident);
  //! Is the type tag valid for this leaf's storage node?
  static bool isTypeTagValid(const STAliasedLeafNode* leaf);
  //! Is the type index valid for this leaf's storage node?
  static bool isTypeIndexValid(const STAliasedLeafNode* leaf);
  //! Get the type index for this leaf's storage bom
  static UInt32 getTypeIndex(const STAliasedLeafNode* leaf);
  //! Get type tag from storage node of leaf
  static int getTypeTag(const STAliasedLeafNode* leaf);
  //! Set the signature on the storage node for this leaf.
  static void setSCHSignature(STAliasedLeafNode* leaf, const SCHSignature* sig);
  //! Get the signature on the storage node for this leaf.
  static const SCHSignature* getSCHSignature(const STAliasedLeafNode* leaf);
  //! Place intrinsic on leaf's bom
  static void setIntrinsic(STAliasedLeafNode* leaf, const IODBIntrinsic* intrinsic);
  //! Get intrinsic on leaf's bom
  static const IODBIntrinsic* getIntrinsic(const STAliasedLeafNode* leaf);
  //! Get constant val from storage, if it exists (NULL, otherwise)
  static const DynBitVector* getConstantVal(const STAliasedLeafNode* leaf);
  //! Is the node a subordinate of a mapped expression ($portsplit, e.g.)
  static bool isExprSubordinate(const STSymbolTableNode* node);
  //! Set the ring as driving a combinational schedule or not
  static void markComboRun(STAliasedLeafNode* leaf, bool isComboRun);
  //! Returns whether or not the ring drives a combo schedule.
  static bool isComboRun(const STAliasedLeafNode* leaf);
  
  //! Place a constant value for the storage of this leaf
  void putConstantVal(STAliasedLeafNode* leaf, NUConst* val);
  
  //! Returns true if leaf is a storage node
  /*!
    True if leaf->getInternalStorage() != NULL && leaf ==
    leaf->getStorage()
  */
  static bool isStorageNode(const STAliasedLeafNode* leaf);

  //! Returns true if leaf has real tag type info
  /*!
    True if the type tag is valid and the tag id is not
    CbuildShellDB::eNoTypeId.
  */
  static bool hasMeaningfulTagId(const STAliasedLeafNode* leaf);

  //! True if the node is marked with eConstValWordId or eConstValId
  static bool isConstantTagType(const STAliasedLeafNode* leaf);
  
  //! Runs through the symboltable and sets storage on unaliased nodes
  /*!
    STSymbolTable::setThisStorage() is not called on all storage
    nodes, because aliasing only calls it on ring lens > 1. This
    ensures that all actual storage nodes have that method called on
    them.
    \warning This can only be called after codegen has generated code,
    bug before the type dictionary is compiled.
  */
  static void markStorageNodes(IODB* db);

  //! Mark the storage node for this net to run debug schedule
  /*!
    Alias rings that have some bits that are flops and others that are
    combo logic require that the debug_schedule() be run when dumping
    waveforms, if the storage is sample scheduled.
  */
  static void markRunDebugSchedule(STAliasedLeafNode* leaf);

  //! Mark storage node that this ring represents a force subordinate
  /*!
    A force subordinate is a pseudo net that represents either the
    value or the mask of the actual forcible net.
  */
  static void markForceSubordinate(STAliasedLeafNode* leaf);

  //! Returns true if this leaf is a value or mask of a forcible
  static bool isForceSubordinate(const STAliasedLeafNode* leaf);

  //! Mark storage node that this ring needs to be depositable
  static void markDepositableNet(STAliasedLeafNode* leaf);

  //! Mark storage node that this ring needs to be observable
  static void markObservableNet(STAliasedLeafNode* leaf);

  //! Mark storage node that this ring cannot be wave dumped
  static void markInvalidWaveNet(STAliasedLeafNode* leaf);
  //! Returns whether or not the net has an invalid waveform
  static bool isInvalidWaveNet(const STAliasedLeafNode* leaf);

  //! Mark storage node that this ring represents a clock
  static void markClockNet(STAliasedLeafNode* leaf);
  //! Returns whether or not this net is a clock
  static bool isClockNet(const STAliasedLeafNode* leaf);

  //! Mark the storage node that this ring is a state output
  /*!
    Currently gets set only during the db write phase. We may want to
    move this marking to an earlier time after elaboration.
  */
  static void markStateOutput(STAliasedLeafNode* leaf);
  //! Returns whether or not this net is a state output
  static bool isStateOutput(const STAliasedLeafNode* leaf);

  //! Get the storage node flags
  static CbuildShellDB::NodeFlags getNodeFlags(const STAliasedLeafNode* leaf);

  //! Needs to be called before writeData to setup data structures.
  /*
    \param srcSymtab The reference symboltable. This will be used with
    the selector
    \param dstSymtab The destination symboltable. This symboltable is
    filled with any new stsymboltablenode's that are referenced from
    the preparation. This is helpful if you are building a symboltable
    that contains a subset of a larger symboltable.
    \param selector Used with srcSymtab to decide whether or not a
    node will be written.
  */
  void prepareForDBWrite(STSymbolTable* srcSymtab,
                         STSymbolTable* dstSymtab,
                         STNodeSelectDB* selector,
                         STSymbolTable::LeafAssoc *leafAssoc) const;

  //! Copy the branch data bom from one branch to another
  static void copyBranchData(const STBranchNode* srcBranch,
                             STBranchNode* dstBranch,
                             AtomicCache* atomicCache);

  //! Copy the leaf data bom from one leaf to another
  static void copyLeafData(const STAliasedLeafNode* srcLeaf,
                           STAliasedLeafNode* dstLeaf);

  //! Copy only the storage leaf info from one leaf to another
  /*!
    This does not copy the contents of the bom. This only copies the
    contents of the storage information within the bom.
  */
  static void copyStorageLeafInfo(const STAliasedLeafNode* srcLeaf,
                                  STAliasedLeafNode* dstLeaf);
  
private:
  SCHScheduleFactory* mScheduleFactory;

  UserTypeFactory* mUserTypeFactory;

  CarbonIdentExprMap mExprMap;
  
  mutable ESFactory mExprFactory;

  mutable DynBitVectorFactory* mBVPool;

  mutable ExprDBContext* mExprDBContext;

  class CbuildElabInfo;
  class CbuildBranchDataBOM;
  class ExprSelectIdentWalk;

  //! This casts the opaque bomdata pointer to a CbuildDataBom*
  static CbuildElabInfo* castLeafBOM(Data bomdata);

  static CbuildElabInfo* castStoreLeafBOM(Data bomdata);

  //! This casts the opaque bomdata pointer to a CbuildDataBom*
  static CbuildBranchDataBOM* castBranchBOM(Data bomdata);

  static const CbuildElabInfo* getElabInfoConst(const STAliasedLeafNode* leaf);
  static CbuildElabInfo* getElabInfo(const STAliasedLeafNode* leaf);

  //! Deals with internal expressions of a leaf that will be written.
  void prepareLeafForDB(const STAliasedLeafNode* leaf, STNodeSelectDB* selector, 
                        bool dstSymtabIsSrc, STSymbolTable::LeafAssoc *) const;
  
};

#endif
