// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef MSG_COUNT_MGR_H_
#define MSG_COUNT_MGR_H_

#include "util/CbuildMsgContext.h"
#include "util/UtString.h"
#include "util/UtMap.h"

//! \file MsgCountMgr.h
/*! The MsgCountManager class consolidates code that is used to 
    count messages, and limit the number of times a message 
    is allowed to be printed. It works in conjunction with the
    MsgContext callback. It's called whenever a message is about
    to be printed. 

    The code is used by the Interra DesignPopulate class, and
    by the Verific Verific2Nucleus populator.
*/

class MsgCountManager
{
public:
  MsgCountManager(void) : 
    mMaxMsgRepeatCount(0), 
    mMsgContext(0)
  {};

public:
  eCarbonMsgCBStatus getMsgCBStatus(CarbonMsgSeverity severity,
                                    int msgNumber, 
                                    const char* text,
                                    unsigned int len);

  void setMaxMsgRepeatCount(SInt32 count) {mMaxMsgRepeatCount = count; };
  void setMsgContext(MsgContext* c) {mMsgContext = c; };
private:
  class MsgCount
  {
  public:
    MsgCount()
      : mCount(0)
    {
    }

    //! Add a new message if different from the current one. Increment
    //! the count if they're same.
    void addMessage(const char* msg, unsigned int len);
    //! Get current message.
    const char* getMsg() const { return mMsg.c_str(); }
    //! Get current count.
    SInt32 getCount() const { return mCount; }

  private:
    UtString mMsg;
    SInt32   mCount;
  };

  typedef UtMap<int, MsgCount> MsgCountMap;
  typedef MsgCountMap::iterator MsgCountMapIter;

  MsgCountMap mMsgCountMap;
  SInt32 mMaxMsgRepeatCount;
  MsgContext *mMsgContext;
};

#endif
