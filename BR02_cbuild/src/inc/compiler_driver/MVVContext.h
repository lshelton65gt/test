// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef MVV_CONTEXT_H
#define MVV_CONTEXT_H

#include "compiler_driver/Interra.h"
#include "compiler_driver/CarbonContext.h"

extern void mvv_spew(FILE* logFilePtr,
                     mvvMessageType severity, int msgNo,
                     const char *sourceFileName,
                     int sourceLineNo,
                     const char *format, va_list pvar);

extern void mvv_prepop_spew(FILE* /*logFilePtr*/,
                            mvvMessageType severity, int /*msgNo*/,
                            const char* /*sourceFileName*/,
                            int /*sourceLineNo*/,
                            const char* /*format*/, va_list /*pvar*/);

extern bool showMVVStatus;

class MVVContext
{
public:
  MVVContext(CarbonContext* cc);
  ~MVVContext();
  void adjustSeverities();
};

#endif // MVV_CONTEXT_H
