// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef JAGUAR_CONTEXT_H
#define JAGUAR_CONTEXT_H

#include "compiler_driver/Interra.h"
#include "localflow/DesignPopulate.h"
#include "util/AtomicCache.h"

class NUDesign;
class CarbonContext;
class LFContext;
class NUModule;

class JaguarContext
{
public:
  JaguarContext(CarbonContext* cc);
  ~JaguarContext();

  //! Run the Interra Jaguar parser on the source VHDL
  bool parseVhdl();
  //! Gets the top level design unit
  bool getTopDesignUnit(mvvNode* topMvvPrimary, 
                        mvvNode* topMvvArchitecture,
                        mvvNode* topBlkCfg);
  //! Sets the generics of the top level design unit
  bool setTopLevelGenerics(vhNode topMvvPrimary);
  //! Prepare to populate Nucleus from the completed Jaguar parse tree
  /*!
    This does all the necessary stuff to allow for the
    InterraDesignWalker to walk the vhdl top design. It returns the
    primary, architecture and the block config.
    Returns true if status is ok.
  */
  bool prepPopulateVhdl(mvvNode* topMvvPrimary, mvvNode* topMvvArchitecture, 
                        mvvNode* topBlkCfg);
  
  //! Post processing after population
  /*!
    This deciphers the error code and does some simple post processing
    of the design.
    Returns true if the populate and the post phase were successful.
  */
  bool postPopulate(mvvNode topMvvPrimary, mvvNode topMvvArchitecture,
                    Populate::ErrorCode err_code, NUDesign* design);

  //! Finds all inline directives 
  //! It calls declarePragmaDirectives function to apply them
  void processMetaDirectives( );

  //! \param dirName is the directive, like "carbon depositSignal"
  //! \param entName is the entity 
  //! \param sigName is the signal name that the directive should be applied
  //! \param node is the actual Jaguar node
  //! if the sigName == NULL, the directive is a module directive
  //! and it's applied to the entity
  void declarePragmaDirectives(const char* dirName,
                               const char* entName,
			       const char* sigName,
			       vhNode node);

    
  //! Determine and return the case sensitivity mode
  VHDLCaseT determineCaseMode();

  //! Get the case mode.
  VHDLCaseT getCase() const {return mCase;}

  //! get the instantiated design.
  NUDesign* getDesign() {return mDesign;}

  //! get the work library.
  const char* getLogicalWorkLib();

  //! Checks that all vhdl files exist and are readable.
  bool checkJaguarArgs(MsgContext* msgContext);

  //! Prepare for/Clear from Pre-population interra walk
  /*!
    \param prePopWalk If true this turns off notes and warnings from
    Jaguar. If false, resets to normal messaging.
  */
  void putInPrePopWalk(bool prePopWalk);

  //! command line argument keyword for library specification
  static const char *scVhdlLib;
  //! command line argument keyword for top VHDL design unit specification
  static const char * scVhdlTop;
  //! command line argument keyword for VHDL compile-only mode
  static const char *scVhdlCompile;
  //! Default VHDL library name
  static const char *scDefaultVhdlLib;
  //! command line argument keyword for VHDL text case handling (lower,upper,preserve)
  static const char *scVhdlCase;
  //! command line argument keyword for disabling the default -synth option.
  static const char *scNoSynth;
  //! command line argument keyword for VHDL-87 mode
  static const char *sc87;
  //! command line argument keyword for VHDL-93 mode
  static const char *sc93;
  //! command line argument keyword for message suppression (deprecated)
  static const char *scSuppress;
  //! command line argument keyword for specifying synth prefixes
  static const char *scSynthPrefix;
  //! command line argument keyword for specifying vhdl file extensions
  static const char *scVhdlExt;
  //! command line argument keyword to enable dumping of source stack on error
  static const char *scVhdlErrorStack;
  //! command line argument keyword to limit the number of source stack
  //! locations dumped on error
  static const char *scVhdlErrorStackLimit;
  //! command line argument keyword to limit the number of recursion loops
  //! a recursive function can perform.
  static const char *scVhdlRecursionLimit;
  //! command line argument to disable constant propagation during population.
  static const char *scNoPplConstProp;
  //! command line argument to disable constant propagation of function calls that return constant.
  static const char *scNoVhdlConstPropFuncCalls;
  //! command line argument to enable automatic compile order
  static const char *scAutoCompileOrder;
  //! command line argument to disable automatic compile order
  static const char *scNoAutoCompileOrder;


  static const SInt32 scMaxRecursionLimit;

private:

  //! transmit the severities from the MsgContext, plus the default ones
  //! that we want, into Jaguar
  void adjustSeverities();

  CarbonContext* mCarbonContext;
  NUDesign* mDesign;
  VHDLCaseT mCase;
};
#endif // JAGUAR_CONTEXT_H
