// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef __INTERRA_H_
#define __INTERRA_H_

/*! \file 
  This file wraps the Interra include files PI.h, JAGUAR.h, and
  mvvPI.h
*/

// Here we artificially create a class hierarchy between all the types
// of void* pointers that Interra uses.

struct veNodeStruct;
struct vhNodeStruct;

struct mvvNodeStruct {
  veNodeStruct* castVeNode() { return reinterpret_cast<veNodeStruct*>(this); }
  vhNodeStruct* castVhNode() { return reinterpret_cast<vhNodeStruct*>(this); }
};

struct veNodeStruct: public mvvNodeStruct {};
struct vhNodeStruct: public mvvNodeStruct {};
struct vhExprStruct: public vhNodeStruct {};
struct vhArrayStruct: public vhNodeStruct {};

// Create the typedefs to Interra's user-visible type names
typedef struct mvvNodeStruct* mvvNode;
typedef struct veNodeStruct*  veNode;
typedef struct vhNodeStruct*  vhNode;
typedef struct vhExprStruct*  vhExpr;
typedef struct vhArrayStruct* vhArray;

enum VHDLCaseT {eVHDLLower, eVHDLUpper, eVHDLPreserve};

class UtString;

#ifdef _MSC_VER
// Macro used by JAGUAR.h
#define _VC8
#endif

#ifdef __JAGUAR_PI_H_
#error "include JAGUAR.h only from Interra.h"
#endif

#define JAGUAR_CONST_STRING_TYPEDEFS

#include "JAGUAR.h"
#include "util/CarbonAssert.h"

// Declare the Jaguar mixed language enabling API missing from JAGUAR.h .
extern "C" WIN_DLL_EXPORT void vhEnableMixedLanguage(VhBoolean flag);

#include "PI.h"
#include "mvvPI.h"
extern void gCheetahAssert(const char* msg, const char* type, veNode obj);
extern const char* gGetObjTypeStr(veObjType objType);
// Function that returns a description string corresponding to a jaguar node.
extern const char* gGetVhNodeDescription(vhNode jag_node);
extern bool gCheetahGenTokenFile(const char* tokenFilename, veNode root,
                                 int argc, char** argv);
class CheetahStr;
extern bool gCheetahGetName(UtString* str, veNode obj);

// Helper class to manage string memory returned from Cheetah
class CheetahStr {
public:
  CheetahStr(char* str): mStr(str) {}
  CheetahStr(): mStr(NULL) {}
  ~CheetahStr() {
    if (mStr != NULL) {
      veFreeString(mStr);
    }
    mStr = NULL;
  }
  operator char*() const {return mStr;}

  char* operator=(char* str) {
    if (mStr != NULL) {
      veFreeString(mStr);
    }
    mStr = str;
    return str;
  }

private:
  CheetahStr(const CheetahStr&);
  CheetahStr& operator=(const CheetahStr&);

  char* mStr;
};

// Helper class to manage string memory returned from Mvv
class MvvStr {
public:
  MvvStr(char* str): mStr(str) {}
  MvvStr(): mStr(NULL) {}
  ~MvvStr() {
    if (mStr != NULL) {
      mvvFreeStrMem(mStr);
    }
    mStr = NULL;
  }
  operator char*() const {return mStr;}

  char* operator=(char* str) {
    if (mStr != NULL) {
      mvvFreeStrMem(mStr);
    }
    mStr = str;
    return str;
  }

private:
  MvvStr(const MvvStr&);
  MvvStr& operator=(const MvvStr&);
  
  char* mStr;
};

class CheetahList {
public:
  CheetahList(): mList(0) {}
  CheetahList(veIter iter): mList(iter) {}
  ~CheetahList() {
    if (mList != NULL) {
      veFreeListMemory(mList);
      mList = NULL;
    }
  }
  // indicator to show that the method that was used to construct this CheetahList actually returned NULL
  bool isNull() const {
    return (mList == NULL);
  }
  operator veIter() const {
    return mList;
  }

  //! assign from veIter
  veIter operator=(veIter src) {
    if (mList != NULL)
      veFreeListMemory(mList);
    mList = src;
    return src;
  }

private:
  CheetahList& operator=(const CheetahList& src);
  CheetahList(const CheetahList& src);

  veIter mList;
};

class JaguarList {
public:
  JaguarList(): mList(0) {}
  JaguarList(vhList iter): mList(iter) {}
  //! Copy constructor.  WARNING: this will rewind the source list!
  JaguarList(const JaguarList& src )
  {
    mList = NULL;
    *this = src;
  }
  ~JaguarList() {
    if (mList != NULL) {
      vhDeleteList(mList);
      mList = NULL;
    }
  }
  operator vhList() const {
    return mList;
  }

  //! assign from vhList
  vhList operator=(vhList src) {
    if (mList != NULL)
      vhDeleteList(mList);
    mList = src;
    return src;
  }

  //! Deep copy assignment.  WARNING: this will rewind the source list!
  JaguarList& operator=(const JaguarList& src) {
    if (mList != NULL)
    {
      INFO_ASSERT( mList != src.mList, "Attempted copy of parser list to itself!" );
      vhDeleteList(mList);
    }
    mList = vhwCreateNewList( VH_TRUE );
    src.rewind();
    while ( vhNode node = vhGetNextItem( src.mList ))
    {
      vhwAppendToList( mList, node );
    }
    src.rewind();
    rewind();
    return *this;
  }

  bool empty() const {
    return size() == 0;
  }
  size_t size() const {
    return (mList == NULL)? 0: vhListGetLength(mList);
  }
  //! I have no idea what the return value means.
  int rewind() const {
    return ( mList == NULL ) ? 0 : vhListRewind( mList );
  }

private:
  vhList mList;
};

// Helper class to manage read-only string memory returned from Jaguar
class JaguarString {
public:
  JaguarString(vhString str): mStr(str) {}
  JaguarString(): mStr(NULL) {}
  ~JaguarString() {
  }
  operator const char*() const {return mStr;}

  const char* operator=(vhString str) {
    mStr = str;
    return str;
  }

  //! need to get a char* to pass as args to non-const-hip Jaguar functions
  char* get_nonconst() {return const_cast<char*>(mStr);}

private:
  JaguarString(vhUserString str); // JaguarUserString should be used
  JaguarString(const JaguarString&);
  JaguarString& operator=(const JaguarString&);

  vhString mStr;
};

// Helper class to manage string memory returned from Jaguar
class JaguarUserString {
public:
  JaguarUserString(vhUserString str): mStr(str) {}
  JaguarUserString(): mStr(NULL) {}
  ~JaguarUserString() {
    if (mStr != NULL) {
      vhFreeString(mStr);
    }
    mStr = NULL;
  }
  operator const char*() const {return mStr;}

  const char* operator=(vhUserString str) {
    if (mStr != NULL) {
      vhFreeString(mStr);
    }
    mStr = str;
    return str;
  }

private:
  JaguarUserString(const JaguarString&);
  JaguarUserString& operator=(const JaguarString&);

  vhUserString mStr;
};

#endif // __INTERRA_H_
