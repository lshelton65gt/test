// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CARBON_CONTEXT_H
#define CARBON_CONTEXT_H

#include <stdarg.h>
#include "util/ArgProc.h"
#include "symtab/STSymbolTable.h"
#include "util/SourceLocator.h"
#include "util/UtLicense.h"
#include "util/UtMap.h"
#include "util/Loop.h"
#include "flow/FLFactory.h"
#include "util/CbuildMsgContext.h"
#include "util/HdlFileCollector.h"
#include "verific2nucleus/VerificDesignManager.h"

class CodeGen;
class SCHScheduleFactory;
class SCHSchedule;
class NUDesign;
class LocalFlow;
class Elaborate;
class ReachableAliases;
class Backend;
class SCHSchedule;
class CheetahContext;
class JaguarContext;
class MVVContext;
class VerificContext;
class HdlFileCollector;
class MsgContext;
class MsgStreamIO;
class CarbonLog;
class Stats;
class IODB;
class CbuildSymTabBOM;
class STSymbolTable;
class AtomicCache;
class RETransform;
class Split;
class ESFactory;
class ESPopulateExpr;
class GOGlobOpts;
class RENetWarning;
class VhdlPortTypeMap;
class BlobMap;
class LocalAnalysis;
class TicProtectedNameManager;

namespace verific2nucleus {
    class VerificTicProtectedNameManager;
}

#define CRYPT(str) CarbonContext::sDoCrypt(__FILE__, __LINE__)

#define CHEETAH_MSG_OFFSET 20000
#define JAG_MSG_OFFSET 30000
#define MVV_MSG_OFFSET 40000

//! CarbonContext
/*!
  The Carbon Builder (cbuild) contains some data that is maintained
  across multiple phases.  This class holds onto that data, and keeps
  track of which phase we are in.
*/ 
class CarbonContext
{
public:
  friend class CheetahContext;
  friend class JaguarContext;
  friend class MVVContext;

  //! constructor
  CarbonContext(int* argc, char** argv, Stats*);

  //! constructor with an existing design & a "master" CarbonContext
  CarbonContext(NUDesign*, CarbonContext*, const char* root_suffix);

  //! destructor
  ~CarbonContext();

  //! Enumeration for argument parsing phases
  enum ArgPass {
    ePassMode = 1, //!< Pass to determine cbuild mode
    ePassCarbon, //!< Pass to process carbon options
    ePassHdl //!< Pass to process hdl-specific options
  };

  //! Keep track of phase
  enum Phase {
    eParseCmdline,
    eParseAttributes,
    eParseDirectives,
    eParseLanguages,
    eDumpHierarchy,
    ePopulateNucleus,
    eDumpDesignPopulate, 
    eFindBlobs,
    ePostPopulateResynth,
    eMakeSizesExplicit,
    eEvalDesignDirectives,
    eEarlyFlatteningAnalysis,
    eGlobalOptimizations,
#ifdef CARBON_PV
    eProgrammersView,
#endif
    ePortAnalysis,
    eLocalAnalysis,
    ePostLocalAnalysis,
    eElaboratedAnalysis,
    eHierAlias,
    eReduce,
    eDebugPrint,
    eSchedule,
    eMarkConstants,
    eBackend,
    eCodeGen,
    eGuiDBFork,
    eGuiDBWait,
    eGuiBuildSymtab,
    eGuiDBWrite,
    eDBWrite,
    ePrintCbuildBanner,
    eCheckoutCbuildLicense,
    eBackendCompile,
    eProtectSource,
    eLowCG,
#ifdef INTERPRETER
    eInterpGen,
#endif
    eSVInspector,
    eLastPhase
  };

  Phase getLastPhase() const { return mLastPhase; }

  //! Run though all compiler phases
  bool run();

  //! Run the specified tool phase.
  bool runPhase(Phase phase_id);

  //! Get the command-line (having been stripped already of the
  //! cbuild-specific options
  int getArgc() {return mArgc;}
  char** getArgv() {return mArgv;}

  //! Get the user settings
  ArgProc* getArgs() {return &mArgs;}

  //! Get the Nucleus context (returns NULL if cPopulateNucleus not yet run)
  NUDesign* getDesign() {return mDesign;}

  //! Get the Stats context
  Stats* getStats() const {return mStats;}

  //! Get the SymbolTable context (returns NULL if elaboration not yet run)
  STSymbolTable* getSymbolTable() {return mSymbolTable;}
  
  CbuildSymTabBOM* getSymbolTableBOM() { return mCbuildSymTabBOM; }
  //! Get the String Cache
  AtomicCache* getAtomicCache() {return mAtomicCache;}

  //! Get the Schedule (returns NULL if cSchedule not yet run)
  SCHSchedule* getSchedule() {return mSchedule;}

  //! Return the netref factory
  NUNetRefFactory *getNetRefFactory() {return mNetRefFactory;}

  //! Return the flow factory
  FLNodeFactory *getFlowFactory() {return mFlowFactory;}

  Split* getSplit() {return mSplit;}

  LocalFlow* getLocalFlow() {return mLocalFlow;}

  Elaborate* getElaborate() {return mElaborate;}

  SourceLocatorFactory* getSourceLocatorFactory() {
    return &mSourceLocatorFactory;
  }

  MsgContext* getMsgContext() {return mMsgContext;}

  const char* getInstallDir() {return mInstallDir.c_str();}

  //! get the IO database context
  IODBNucleus* getIODB() {return mIODB;}

  //! get the file root (e.g. dir/libdesign) to form dir/libdesign.clock
  const char* getFileRoot() {return mFileRoot.c_str();}

  //! is memory debugging on?
  bool isDumpMemory() const {return mDumpMemory;}

  //! generate C-Model wrapper?
  bool doGenerateCModelWrapper() {return mGenerateCModelWrapper;}

  //! generate SystemC wrapper?
  bool doGenerateSystemCWrapper() {return mGenerateSystemCWrapper;}

  //! command line argument keyword for forcing vhdl loop unroll
  static const char *scVhdlLoopUnroll;

  //! command line argument keyword for allowing file-IO subprograms (file_open, read write...)to be included in model
  static const char *scVhdlEnableFileIO;

  static const char *scEnableCSElab;
  static const char *scDisableCSElab;

  //! Global function to decrypt hidden static strings
  static const char* sDoCrypt(const char* file, UInt32 lineNumber);

  //! enables verilog output
  static const char* scDumpVerilog;
  //! enables extra dumpVerilog for CSElab
  static const char* scDumpVerilogCSE;
  //! Global arg section name
  static const char* scGenCompileControl;
  //! Global arg section name
  static const char* scInputFileControl;
  //! Global arg section name
  static const char* scModuleControl;
  //! Global arg section name
  static const char* scNetControl;
  //! Global arg section name
  static const char* scOutputControl;
  //! Global arg section name
  static const char* scVHDL;
  //! Global arg section name
  static const char* scVerilog;
  //! Global arg section name
  static const char* scMaxMsgRepeatCount;

  //! Global unprocessed argument group name
  static const char* scUnprocVlog;
  //! Global unprocessed argument group name
  static const char* scUnprocVHDL;

  static const char* scLicenseStr;
  //! Pragmas for net directives
  static const char* scNetPragmas[];
  //! Pragmas for module directives
  static const char* scModulePragmas[];
  //! Pragmas for function directives
  static const char* scFunctionPragmas[];

  //! Global -vspCompileFileFlag option string.
  /*!
    Needed in CarbonContext.cxx and codegen.cxx
  */
  static const char* scVSPCompileFileFlag;

  //! Global -topLevelModule option string
  static const char* scTopLevelParam;

  //! Suffix added to Vhdl subprograms to make them unique
  static const char* sUniqSubprogramSuffix;

  //! switches needed in multiple files
  static const char* scEnableIgnoreZeroReplicationConcatItem;
  static const char* scDisableIgnoreZeroReplicationConcatItem;
  static const char* scEnable2005StylePowerOperator;
  static const char* scDisable2005StylePowerOperator;
  static const char* scSVerilog;

  //! Set flag indicating that VHDL files were parsed
  //void setHaveVhdlFiles() { mHaveVhdlFiles = true; }
  //! Get flag indicating that VHDL files were parsed
 // bool getHaveVhdlFiles() const { return mHaveVhdlFiles; }

  //! get the FILE* for the error log
  FILE* getErrorFile();

  //! get the FILE* for the warning log
  FILE* getWarningFile();

  //! get the FILE* for the suppress log
  FILE* getSuppressFile();

  //! true if we are generating an encrypted model
  bool genEncryptedModel() const {return mGenEncryptedModel;}

  //! return the license context
  UtLicense* getLicense() {return mLicense;}

  //! is a diagnostics license available?
  bool hasDiagnosticsLicense() const {return mHaveDiagLicense;}

  //! Was -generateCleartextSource specified?
  bool isGenerateCleartextSource() const;

  //! Make sure connection with license server is still good
  void doHeartbeat();

  //! Specify the requested top-level module name (called from cheetah parser)
  /*! work around Cheetah bug */
  void putTopModuleName(const char* modname);

  //! Find the requested top-level module name, or NULL if none specified
  /*! work around Cheetah bug */
  const char* getTopModuleName() const;

  //! Log Analyzer Messages
  void logAnalyzerMessages(FILE* logFilePtr, MsgContextBase::Severity sev,
                           int msgNo, const char *sourceFileName,
                           unsigned long sourceLineNo,
                           const char *format, va_list pvar);

  JaguarContext *getJaguarContext() const { return mJaguarContext; }
  CheetahContext *getCheetahContext() const { return mCheetahContext; }
  MVVContext *getMVVContext() const { return mMVVContext; }
  const HdlFileCollector *getFileCollector() const { return mFileCollector; }

  void createDumpMemoryTask();

  //! Calls all memory release routines for Cheetah, Jaguar, and MVV.
  bool freeInterraMemory();

  //! Get the map of port names to declared VHDL types.  Should only be used by codegen
  const VhdlPortTypeMap *getVhdlPortTypeMap() const { return mPortTypeMap; }

  //! Modifiable version of getVhdlPortTypeMap
  VhdlPortTypeMap *getVhdlPortTypeMap()  { 
    return mPortTypeMap;
  }

  //! BlobMap accessor
  BlobMap* getBlobMap() { return mBlobMap; }

  //! Should Jaguar warnings be shown to the user?  This is left 'true'
  //! most of the time, except when the localflow/VhPopulate* are 'exploring'
  //! the VHDL syntax tree in a fashion where they don't want warnings to
  //! be shown (e.g. because they might be redundant).
  bool showJaguarWarnings() const;

  //! Push suppression-state of jaguar warnings
  void pushSuppressJaguarWarnings();

  //! Pop suppression-state of jaguar warnings
  void popSuppressJaguarWarnings();

  //! If needed, checkin any licenses and destroy the license object
  void destroyLicense();

  //! If we are crashing, call this and exit.
  void emergencyCheckinLicenses();

  //! write a .cmd file that contains the cbuild options, and maybe the input files
  void writeCmdFile(const char* filename, bool includeFile,const char* filePrefix, // extra string that will be prefixed to the file
		            bool writeNCmdFile = false);

  //! check -vhdlCase option
  void checkVhdlCase();

  //! check vhdlTop
  bool checkVhdlOnTop();

  //! Create LangVer and fileName pair
  bool callVerificParserAndStaticElaboration(LangVer langVer, bool autoCompileOrder, bool compileOnly, bool hasVerilogLib,
    bool hasVhdlLib, bool hasLibmap, bool hasVhdlNoWriteLib);
  
  //! are we running in +protect mode?
  bool protectingSource() { return mProtectSource; }

  //! how many source files have been protected?
  UInt32 protectedSourceCount() { return mProtectedSourceCount; }

  //! dump a directory of verilog based on the current nucleus state
  void dumpVerilog(const char* dirName);

  //! dump a the current nucleus state
  void dumpNExpressions(const char* phaseName);

  //! Is this a VSP Compile?
  bool isVSPCompile() const;

  //! Is this a backend compile only (called by vspbuild script)
  bool isBackendCompileOnly() const;

  //! Is this just parsing the backend compilation output only
  bool isParseMakeOutputOnly (UtString *filename) const;

  //! Is this just reading an N-expression file
  bool isReadNExpressionOnly () const;

  //! Is profiling requested?
  bool isProfiling() const { return mIsProfiling; }

  //! was System Verilog Inspection requested?
  bool isSVInspector() const { return (0 != mSVInspectorCount); }

  //! was -systemverilog specified? (all verilog files handled as system verilog)
  bool isSystemVerilogMode() const { return mSVerilog; }

  //! Are we compiling with profiling enabled?
  /*!
   * The current plan is to always compile with profiling enabled.
   * That allows the user to turn on profiling without rerunning
   * cbuild.  If it's slow, we can have compileWithProfiling() return
   * isProfiling() (meaning to compile in profiling only when the user
   * specifies -profile).
   */
  bool compileWithProfiling() const { return true; }

  //! Prepare for/Clear from Pre-population interra walk
  /*!
    \param prePopWalk If true this turns off notes and warnings from
    Cheetah/Jaguar/Mvv. If false, resets to normal messaging.
  */
  void putInPrePopWalk(bool prePopWalk);

  //! returns the current setting for PrePopWalk flag
  bool getInPrePopWalk() const {return mCurrentPrePopWalkFlag; }

  bool isVerboseUniquify() const;

  TicProtectedNameManager* getTicProtectedNameManager() {return mTicProtectedNameMgr;}
  
  verific2nucleus::VerificTicProtectedNameManager* getVerificTicProtectedNameManager() {return mVerificTicProtectedNameMgr;}

  //! Use to see whether we're using the new record rewrite code
  bool isNewRecord() const;

  //! Is this a library only compile?
  bool isLibraryCompileOnly() const;

  AtomicCache* mIPFeatures;

  class CarbonCfg;

  //! get the tristate mode requested by the user
  TristateModeT getTristateMode() const {return mTristateMode;}

  bool createDirectory(const UtString* dir);

  //! returns true if population should use verilog 2005 style power operator either via language choice (-sverilog) or hidden command line switch (-enable2005StylePowerOperator or -disable2005StylePowerOperator)
  bool usingVerilog2005StylePowerOperator();

private:
  void debugXmlWriteSymbolTables(STSymbolTable* declarationsSymTab,STSymbolTable* elaborationsSymTab);
  void commonInit();
  void commonDestruct();
  void setupOptions(void);
  void setupPhaseFlow(void);
  void setupPhases(void);

  void doLicenseExit(const char* reason);

  static CarbonCfg* sCarbonCfg;

  void findInstallDir();
  // apply customer license tag to the Customer DB
  bool applyCustomerTag();

  bool parsePhase(const char* phaseName, Phase*) const;

  bool runParseCmdline();
  bool runParseLanguages();
  void handleVerificSynthPrefix();
  void handleVerificTranslatePragmas(); 
  bool callCarbonStaticElaboration();
  bool runVerificParseLanguages();
  bool runInterraParseLanguages();
  bool runDumpHierarchy();
  bool runVerificDumpHierarchy();
  bool runInterraDumpHierarchy();
  bool runPopulateNucleus();
  bool runVerificPopulateNucleus();
  bool runInterraPopulateNucleus();
  bool runDumpDesignPopulate();
  bool runFindBlobs();
  bool runPostPopulateResynth();
  bool runMakeSizesExplicit();
  bool runParseAttributes();
  bool runParseDirectives();
  bool runEarlyFlatteningAnalysis();
  bool runGlobalOptimizations();
#ifdef CARBON_PV
  bool runProgrammersView();
#endif
  bool runPortAnalysis();
  bool runLocalAnalysis();
  bool runPostLocalAnalysis();
  bool runElaboratedAnalysis();
  bool runEvalDesignDirectives();
  bool runDebugPrint();

  void dumpNetFlags();
  void dumpRegInit();
  void genMultiDUT(UInt32 numInstances);
  void formatFlow(FLNodeElab*, UtString*);
  void dumpNetRefs();
  void findNameWidths(NUModule* mod, UInt32* maxModWidth, UInt32* maxInstWidth, UInt32 depth);
  void printHierarchy(UtOBStream& file, NUModule* mod, bool *previous_was_protected, UInt32 instanceColumn, UInt32 locColumn, UInt32 depth);
  void dumpHierarchy(const char* phaseName);

  //! Process the design and remove any tasks without callers. return false if there was a problem detected.
  bool removeUncalledTasks(MsgContext * msg_ctx, bool verbose);

/*
  bool runElaborate();
  bool runGlobalAnalysis();
*/
  bool runHierAlias();
  bool runReduce();
  bool runSchedule();
  bool runBackend();
  bool runCodeGen();
  bool runLowCG ();
  bool runMarkConstants();
  bool runDBWrite();
  bool runGuiDBFork();
  bool runGuiDBWait();
  bool runGuiBuildSymtab();
  bool runGuiDBWrite();
  bool runBackendCompile();

  bool runPrintCbuildBanner();
  bool runCheckoutCbuildLicense();
  bool runProtectSource();
  bool runSVInspector();
  bool runInterpGen();

  bool runReadNExpressions ();
  void analyzeAndRegisterValueForVhdlExt();
/*
  bool runWriteDebugTable();
*/
  // forbid
  CarbonContext(const CarbonContext&);
  CarbonContext& operator=(const CarbonContext&);

  typedef UtMap<Phase, UtString> PhaseToNameTable;
  typedef UtMap<Phase, UtString> PhaseToNamePhraseTable;
  typedef Loop<PhaseToNameTable> PhaseToNameTableLoop;

  typedef UtVector<Phase> PhaseFlow;
  typedef Loop<PhaseFlow> PhaseFlowLoop;

  class LicCB;
  friend class LicCB;
  
  class PreParseData;
  friend class PreParseData;

  LicCB* createLicenseCB(CarbonContext* cc, UtLicense** lic);
  void destroyLicenseCB(LicCB* cc);

  // Statistics for various compiler passes. This should be the first
  // member so that we can initialize it first. This allows sbrk to be
  // called as early as possible.
  Stats* mStats;
  
  ArgProc& mArgs;
  UtStringArgv mArgvBuffer;
  UtStringArgv mUnexpandedArgv;
  UtStringArgv mSaveArgv;
  bool mDumpMemory;
  bool mGenerateCModelWrapper;
  bool mGenerateSystemCWrapper;
  int mArgc;
  char **mArgv;
  //! ArgProc callback functor for collecting source code files
  /*! Contains the VHDL file<->library associations needed by the
   *  JaguarContext, and the list of Verilog files to be compiled. */
  HdlFileCollector *mFileCollector;

  UtString mFileRoot;
  AtomicCache* mAtomicCache;
  CbuildSymTabBOM* mCbuildSymTabBOM;
  STSymbolTable* mSymbolTable;
  CodeGen* mCodeGen;
  NUDesign* mDesign;
  ReachableAliases* mReachableAliases;
  SCHScheduleFactory* mScheduleFactory;
  SCHSchedule* mSchedule;
  Backend* mBackend;
  SourceLocatorFactory& mSourceLocatorFactory;
  FLNodeFactory *mFlowFactory;
  FLNodeElabFactory *mFlowElabFactory;
  LocalFlow* mLocalFlow;
  Elaborate* mElaborate;
  CheetahContext* mCheetahContext;
  JaguarContext* mJaguarContext;
  MVVContext *mMVVContext;
  VerificContext *mVerificContext;
  MsgContext* mMsgContext;
  MsgStreamIO* mErrStream;
  CarbonLog* mLogStream;
  IODBNucleus* mIODB;
  NUNetRefFactory *mNetRefFactory;
  TristateModeT mTristateMode;
  RETransform* mTransform;
  Split * mSplit;
  UtString mInstallDir;
  UtString mCongruencyInfoFilename;
#ifdef CARBON_PV
  UtString mPSDFileName;
#endif
  ESFactory* mExprFactory;
  ESPopulateExpr* mPopulateExpr;
  UtString mTopModuleName;
  LicCB* mLicenseCB; 
  UtLicense* mLicense;
  UtOStream * mFlatteningTraceFile;
  PreParseData* mPreParseData;
  BlobMap* mBlobMap;

  typedef bool (CarbonContext::*PhaseExec)();
  //Phase mFirstPhase;
  Phase mLastPhase;
  Phase mCostPhase;
  PhaseExec mPhases[eLastPhase];
  PhaseToNameTable mPhaseNames;             // name of phase
  PhaseToNamePhraseTable mPhaseNamePhrases; // english phrase for phase
  PhaseFlow mPhaseFlow;


  UInt32 mJaguarSuppressDepth;
  bool mModulePhaseStats;
  bool mHaveDiagLicense;
  bool mGenEncryptedModel;
//  bool mHaveVhdlFiles;
  bool mProtectSource;  // true means +protected was given on command line
  UtString mProtectedSourceExtension;
  UInt32 mProtectedSourceCount; // number of files protected
  bool mIsProfiling;
  int mSVInspectorCount;        // negative means unlimited count; 0 means do not run svInspector; positive is the maximum number of lines printed for a particular language unit
  bool mSVerilog;

  VhdlPortTypeMap* mPortTypeMap;

  //! Provide context to populate elaborated net read/written flags
  //! before aggressive analysis, but print dead-net warnings after
  //! codegen, where we figure out which nets are wave-able.  We don't
  //! want to print warnings on dead nets that are not in the waveforms.
  RENetWarning* mPostSchedWarnings;

  TicProtectedNameManager* mTicProtectedNameMgr;
  verific2nucleus::VerificTicProtectedNameManager* mVerificTicProtectedNameMgr;

  // In order to test the GUI DB writer, a child process is forked and
  // libdesign.gui.db is written by that child. The parent process waits for
  // the child to terminate. If the child terminates normally then compilation
  // continues, otherwise a fatal error is induced.

  bool mWritingGuiDB;                   //!< set when libdesign.gui.db is written
  bool mForkingWriteGuiDB;              //!< set when a child is forked for writing libdesign.gui.db
  bool mVerboseGuiDB;                   //!< force extra checking of GUI database writing
  int mWriteGuiDBPID;                   //!< pid of the child process
  
  // Verific specific objects
  verific2nucleus::VerificDesignManager mVerificDesignManager; 


#define GUIDBFORK_USES_STRDUP (pfLINUX||pfLINUX64)

#if GUIDBFORK_USES_STRDUP
  // The output of the libdesign.gui.db write subprocess is captured in a
  // file. I don't know how to do this on Windows so, for now, this is only
  // done on a Unix box.
  UtString mWriteGUIOutputFile;         //!< filename of the output file
#endif

  //! Run the specified tool phase.
  bool runOnePhase(PhaseExec phase, Phase phase_id, const char * phase_name);

  //! Print out a banner in the standard format.
  void printBanner(const char * short_name, const char * long_name, const char * version);

  //! Prints the targetted compiler name.
  void printBackendTarget() const;

  //! Prints the cbuild release number to the given stream
  void printVersion(UtOStream& out) const;

  //! Prints the cbuild versioning info, including CVS version and Interra library versions
  void printVerboseVersion() const;

  //! Print the cbuild usage and exit
  void printUsage();

  //! Prints the possible phases (for use with -lastPhase)
  void printLastPhaseStrings() const;

  //! Sets up fredo - disables optimizations
  void initFredo();

  //! Checkout licenses based on VSP Licensing PRD
  bool doVSPLicensing();

  //! Report a VSP license checkout failure.
  void reportVSPCheckoutFailure(UtString& reason);

  //! Report a AMBA Designer checksum failure
  void reportADChecksumFailure( bool report_as_note, const UtString& reason, const char* filename );

  bool protectUnprotectFile(int argc, char* const* argv, bool*  isProtUnprot, UtString* errMsg);
  bool getEFfileContext(const char* file_name, UtString* buffer, UtString* errMsg);

  //! Check for command-line switches that have been removed
  /*!
    Occasionally, we need to abruptly remove a command-line option
    without deprecating it first.  In that case, it's nice to print a
    customized error message.

    \returns true if a removed command-line option was specified
   */
  bool checkRemovedSwitches();

  //! Perform special quoting for +define arguments
  const char* quotePlusDefine(const char* arg, UtString *buf);
  
  //! Enable a timebombed license in the model, if requested
  /*!
    This returns true if no errors were encountered.  That is, no
    timebomb was requested, or one was and it was set successfully.
   */
  bool doTimeBomb(UtCustomerDB::Signature* sig);

  CarbonContext* mMaster;

  bool mCurrentPrePopWalkFlag;
  
}; // class CarbonContext


#endif
