// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef CHEETAH_CONTEXT_H
#define CHEETAH_CONTEXT_H

#include "util/UtVector.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/Interra.h"
#include "localflow/DesignPopulate.h"

class NUDesign;

class CheetahContext
{
public:
  CheetahContext(CarbonContext* cc,
                 TicProtectedNameManager* tic_protected_nm_mgr);
  ~CheetahContext();

  //! Call Cheetah on the Verilog design files.  
  /*! This leaves Cheetah having fully parsed the Verilog, but no
   *  creation of Nucleus data structures has been performed at this
   *  time. */
  bool parseVerilog();
  
  //specific verific cmd line parser
  bool parseVerificVerilogCmdLine();
  //! Prepare to populate Nucleus with Verilog data
  /*!
    This gets the top level mvvNodes from the interra structures. It
    then runs any requested things like token-generation or
    cem-generation.
    returns true if the status is ok.
  */
  bool prepPopulateVerilog(mvvNode* topPrimary, mvvNode* topArchitecture, mvvNode* topBlkCfg);

  //! Checks status and does any post population cleanup/analysis
  /*!
    Returns true if the population and the post phase were successful.
  */
  bool postPopulate(Populate::ErrorCode err_code, NUDesign* design);

  NUDesign* getDesign() {return mDesign;}

  //! Does this option require a filename value?
  static bool isFileOption(const char* optionName);

  //! Does this option require a non-filename value?
  static bool isValueOption(const char* optionName);

  //! Is this option handled by Cheetah?
  /*!
    Returns true for illegal Cheetah options too. This is meant to be
    able to distinguish a non-existent option v. one that is handled
    in some way, so we can properly ignore verilog options when we are
    compiling vhdl only.
  */
  static bool isVerilogOption(const char* optionName);
  
  //! Note that a line of code comes from an encrypted block
  void markEncrypted(const char* filename, unsigned int line);

  //! Note that a line of code comes from an CEM block
  void markCEM(const char* filename, unsigned int line, const char* name);

  //! Is this file/line within a protected block?
  bool requiresProtection(const char* filename, unsigned int line);

  //! Is this file/line within a CEM file, and if so, what's the CEM filename?
  bool isCEMFile(const char* filename, unsigned int line, UtString* cemFile);

  //! Note that this CEM file was parsed via -v, so tokens-generation can do the same
  void putIsCemDashV(const char* cemFilename);

  //! When generating a tokens subdir, we want to copy the libdesign.dir to the
  //! subdirectory after the .dir file has been closed.
  bool copyDirectivesFileToTokenDir(const char* directivesFilename);

  //! Prepare for/Clear from Pre-population interra walk
  /*!
    \param prePopWalk If true this turns off notes and warnings from
    Cheetah. If false, resets to normal messaging.
  */
  void putInPrePopWalk(bool prePopWalk);

  //! Gets the top level module for the design
  veNode getTopModule();

  //! Sets the values of the parameters in the top level module
  bool setTopLevelParameters(veNode topLevelModule);

  Populate::ErrorCode declarePragmaDirectives(const char* modName,
                                              const char* netName,
                                              veNode node);

private:
  friend bool CarbonContext::runParseLanguages( );
  friend bool CarbonContext::runInterraParseLanguages( );
  static const char* decrypt(void* clientData, const char* buf,
                             const char* filename, unsigned int line);
  bool genCarbonEncryptedModel(const UtStringArray& cemModules,
                               veNode root);
  bool genTokenFile(const char* filename, veNode root);
  static bool writeFile(const char* filename,
			 UtString* contents);
  bool loadCarbonEncryptedModel(const char* modelFile,
                                UtStringArgv* arg,
                                size_t i);
  void cleanProtectTempFiles();
  bool decryptCemFile(const char* modelFile, unsigned long* index);

  bool processMetaDirectives(veNode root);
  void adjustSeverities();

  CarbonContext* mCarbonContext;
  NUDesign* mDesign;
  UtStringArray mCryptBufs;
  UtStringArray mCEMFilenames;
  UtStringArray mProtectBufs;
  UtStringArray mFilenamePatterns;

  UtString mCEMProtectFilePattern;
  UInt32 mObfuscationIndex;
  UtString mTokenDir;
  //! The root node returned by Cheetah
  //! Shared between parseVerilog and populateVerilog
  veNode mRoot;

  // Cheetah asks us to re-parse cem files found with uselib, and
  // to get the right answer we must re-obfuscate the same way each
  // time, so remember the buffer indices for each file
  typedef UtHashMap<UtString, UInt32, HashValue<UtString> > DecryptMap;
  DecryptMap mDecryptMap;

  typedef UtHashSet<veNode> NodeSet;
  NodeSet mCovered;

  UtVector<size_t> mProtectIndices;

  //! Keep track of source locators that are marked as encrypted
  typedef UtHashSet<SourceLocator, HashValue<SourceLocator> > SourceLocatorSet;
  SourceLocatorSet mEncryptedLines;

  typedef UtHashMap<SourceLocator, StringAtom*, HashValue<SourceLocator> >
  SourceLocatorStringMap;
  SourceLocatorStringMap mCEMLines;

  AtomSet mCemFiles;
  AtomSet mCemDashVFiles;     // cem files read in via -v

  TicProtectedNameManager* mTicProtectedNameMgr;
};

#endif // CHEETAH_CONTEXT_H
