// -*- C++ -*-
#ifndef ARMContext_h__INCLUDED_
#define ARMContext_h__INCLUDED_
/*****************************************************************************


 Copyright (c) 2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

//! class ARMContext
/*! thin wrapper arround the ARM supplied routines, 
 *  such as the checksum verifier
 */


#include "util/CarbonTypes.h"

class UtString;
class ARMContext
{
 public:
  //! constructor
  ARMContext(){}
  //! destructor
  ~ARMContext(){}


  //! perform the AMBA Designer checksum test on file: filepath, 
  //! \returns false if test fails, and errMsg will point to a string that explains the reason
  static bool csTest(const char* filepath, UtString* errMsg); 

 private:
    CARBON_FORBID_DEFAULT_CTORS(ARMContext);
};
#endif
