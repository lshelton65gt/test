// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __JAGUARBROWSER_H_
#define __JAGUARBROWSER_H_

#include "Interra.h"

//! Class to handle callbacks from JaguarBrowser.
/*!
  Derive from this class and overload vh*() functions to handle callbacks.
*/
class JaguarBrowserCallback
{
public:
  enum Status {
    eNormal,
    eStop,
    eSkip,
    eInvalid
  };

  enum Phase {
    ePre,
    ePost
  };

  JaguarBrowserCallback() {}
  virtual ~JaguarBrowserCallback() {}

  //! Literals
  virtual Status vhcharlit(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhdeclit(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhbaselit(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhidenumlit(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhstring(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhbitstring(Phase /*phase*/, vhNode /*node*/) { return eNormal; }

  //! Identifier expressions
  virtual Status vhobject(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhexternal(Phase /*phase*/, vhNode /*node*/) { return eNormal; }

  //! Identifiers
  virtual Status vhvariable(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhsignal(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhconstant(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhfiledecl(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhforindex(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhaliasdecl(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhattrbdecl(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhattrbspec(Phase /*phase*/, vhNode /*node*/) { return eNormal; }

  //! Expression types
  virtual Status vhbinary(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhunary(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhindname(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhslicename(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhslicenode(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhfuncnode(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhassociation(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhselectedname(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhfunccall(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhtypeconv(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhqexpr(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhaggregate(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhelementass(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhattrbname(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhrange(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhrangeleft(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhrangeright(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhsimplename(Phase /*phase*/, vhNode /*node*/) { return eNormal; }

  //! Statements
  virtual Status vhvarasgn(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhsigasgn(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhif(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhelsif(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhfor(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhloop(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhwhile(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhreturn(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhprocedurecall(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhwait(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhcase(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhcasealter(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhnext(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhexit(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhnull(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhassert(Phase /*phase*/, vhNode /*node*/) { return eNormal; }   // this_assert_OK
  virtual Status vhreport(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhselsigasgn(Phase /*phase*/, vhNode /*node*/) { return eNormal; }

  //! Types
  virtual Status vhconstrainedarray(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhunconstrainedarray(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhenumelement(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhenumtype(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhfiletype(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhfloattype(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhinttype(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhrecord(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhrecordelement(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhsubtypedecl(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhsubtypeindication(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhtypedecl(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhselectdecl(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhindexconstraint(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhindexunconstrained(Phase /*phase*/, vhNode /*node*/) { return eNormal; }
  virtual Status vhrangeconstraint(Phase /*phase*/, vhNode /*node*/) { return eNormal; }

};

//! Callback-based generic jaguar object browse.
/*!
  Browse through the node given to browse() method invoking
  callbacks for each jaguar object visited.
  Browsing can start at a sequential statement or an expression or 
  an identifier . Starting at entities, configurations,
  architectures etc is not supported.
  Browsing ignores initial values and type information.
*/
class JaguarBrowser
{
public:
  //! Constructor
  /*!
    Provide an object of class derived from JaguarBrowserCallback
  */
  JaguarBrowser(JaguarBrowserCallback& callback);
  ~JaguarBrowser();

  //! Start browsing given statement/expression/identifier jaguar node.
  JaguarBrowserCallback::Status browse(vhNode node);
  //! Browse the specified list of jaguar nodes in the list order.
  JaguarBrowserCallback::Status browseList(JaguarList& list);

  //! Enable/Disable browsing through type information of signal/variable/port declarations.
  /*!
    This is set to false by default.
  */
  void putTypeBrowseEnable(bool enable) {
    mTypeBrowseEnable = enable;
  }

  //! Enable/Disable browsing through the initial values of variable/signal/constant declarations.
  /*!
    This is set to false by default.
  */
  void putDeclInitialBrowseEnable(bool enable) {
    mDeclInitialBrowseEnable = enable;
  }

private:
  JaguarBrowser(const JaguarBrowser&);
  JaguarBrowser& operator=(const JaguarBrowser&);

  JaguarBrowserCallback::Status browseVarAsgn(vhNode node);
  JaguarBrowserCallback::Status browseSigAsgn(vhNode node);
  JaguarBrowserCallback::Status browseIf(vhNode node);
  JaguarBrowserCallback::Status browseElsIf(vhNode node);
  JaguarBrowserCallback::Status browseFor(vhNode node);
  JaguarBrowserCallback::Status browseWhile(vhNode node);
  JaguarBrowserCallback::Status browseLoop(vhNode node);
  JaguarBrowserCallback::Status browseReturn(vhNode node);
  JaguarBrowserCallback::Status browseProcedureCall(vhNode node);
  JaguarBrowserCallback::Status browseWait(vhNode node);
  JaguarBrowserCallback::Status browseCase(vhNode node);
  JaguarBrowserCallback::Status browseCaseAlter(vhNode node);
  JaguarBrowserCallback::Status browseNext(vhNode node);
  JaguarBrowserCallback::Status browseExit(vhNode node);
  JaguarBrowserCallback::Status browseNull(vhNode node);
  JaguarBrowserCallback::Status browseAssert(vhNode node);
  JaguarBrowserCallback::Status browseReport(vhNode node);
  JaguarBrowserCallback::Status browseCharLit(vhNode node);
  JaguarBrowserCallback::Status browseDecLit(vhNode node);
  JaguarBrowserCallback::Status browseBaseLit(vhNode node);
  JaguarBrowserCallback::Status browseIdeNumLit(vhNode node);
  JaguarBrowserCallback::Status browseString(vhNode node);
  JaguarBrowserCallback::Status browseBitString(vhNode node);
  JaguarBrowserCallback::Status browseObject(vhNode node);
  JaguarBrowserCallback::Status browseExternal(vhNode node);
  JaguarBrowserCallback::Status browseForIndex(vhNode node);
  JaguarBrowserCallback::Status browseAliasDecl(vhNode node);
  JaguarBrowserCallback::Status browseAttrbDecl(vhNode node);
  JaguarBrowserCallback::Status browseAttrbSpec(vhNode node);
  JaguarBrowserCallback::Status browseSignal(vhNode node);
  JaguarBrowserCallback::Status browseVariable(vhNode node);
  JaguarBrowserCallback::Status browseFileDecl(vhNode node);
  JaguarBrowserCallback::Status browseConstant(vhNode node);
  JaguarBrowserCallback::Status browseBinary(vhNode node);
  JaguarBrowserCallback::Status browseUnary(vhNode node);
  JaguarBrowserCallback::Status browseIndName(vhNode node);
  JaguarBrowserCallback::Status browseSliceName(vhNode node);
  JaguarBrowserCallback::Status browseSliceNode(vhNode node);
  JaguarBrowserCallback::Status browseFuncNode(vhNode node);
  JaguarBrowserCallback::Status browseAssociation(vhNode node);
  JaguarBrowserCallback::Status browseSelectedName(vhNode node);
  JaguarBrowserCallback::Status browseFuncCall(vhNode node);
  JaguarBrowserCallback::Status browseTypeConv(vhNode node);
  JaguarBrowserCallback::Status browseQExpr(vhNode node);
  JaguarBrowserCallback::Status browseAggregate(vhNode node);
  JaguarBrowserCallback::Status browseElementAss(vhNode node);
  JaguarBrowserCallback::Status browseAttrbName(vhNode node);
  JaguarBrowserCallback::Status browseRange(vhNode node);
  JaguarBrowserCallback::Status browseSimpleName(vhNode node);
  JaguarBrowserCallback::Status browseConstrainedArray(vhNode node);
  JaguarBrowserCallback::Status browseUnconstrainedArray(vhNode node);
  JaguarBrowserCallback::Status browseEnumElement(vhNode node);
  JaguarBrowserCallback::Status browseEnumType(vhNode node);
  JaguarBrowserCallback::Status browseFileType(vhNode node);
  JaguarBrowserCallback::Status browseFloatType(vhNode node);
  JaguarBrowserCallback::Status browseIntType(vhNode node);
  JaguarBrowserCallback::Status browseRecord(vhNode node);
  JaguarBrowserCallback::Status browseRecordElement(vhNode node);
  JaguarBrowserCallback::Status browseSubtypeDecl(vhNode node);
  JaguarBrowserCallback::Status browseSubtypeIndication(vhNode node);
  JaguarBrowserCallback::Status browseTypeDecl(vhNode node);
  JaguarBrowserCallback::Status browseSelectDecl(vhNode node);
  JaguarBrowserCallback::Status browseIndexConstraint(vhNode node);
  JaguarBrowserCallback::Status browseIndexUnconstrained(vhNode node);
  JaguarBrowserCallback::Status browseRangeConstraint(vhNode node);

  void invalidNode(vhNode node);

  JaguarBrowserCallback& mCallback;
  bool mTypeBrowseEnable;
  bool mDeclInitialBrowseEnable;
};

#endif
