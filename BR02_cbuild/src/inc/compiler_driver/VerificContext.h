// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2012-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef VERIFIC_CONTEXT_H
#define VERIFIC_CONTEXT_H

// the following code can be replaced in its entirety with verific specific code

#include "compiler_driver/MVVContext.h"
#include "compiler_driver/Interra.h"
#include "compiler_driver/CarbonContext.h"
#include "VhdlRuntimeFlags.h"
#include "LineFile.h"

extern bool showVerificStatus;

class VerificContext
{
public:
  VerificContext(CarbonContext* cc);
  ~VerificContext();
  void processCommandLineSwitches();
  void adjustMessageText();
  void adjustSeverities();
  void suppressAllWarnings();
  const char* verificReleaseString();
  bool parseVerilogArgs();

private:
  Verific::LineFile * mLineFile;

  UtString mVerificReleaseIdentifier;
};

#endif
