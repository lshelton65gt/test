// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef VHDLPORTTYPEMAP_H_
#define VHDLPORTTYPEMAP_H_

#include "util/UtString.h"
#include "util/UtList.h"

class ConstantRange;

//! Information holder for a vhdl port type
class VhdlPortType
{
public:
  CARBONMEM_OVERRIDES

  //! Enum for vhdl port direction type
  enum VhdlPortDir {
    eInput, //!< input
    eOutput, //!< output 
    eInout, //!< inout
    eBuffer //!< Buffer (like output)
  };

  //! Constructor
  VhdlPortType(const UtString& type);

  //! Basic constructor w/o type info
  VhdlPortType();

  //! Destructor
  ~VhdlPortType();

  //! Reassign the type string
  void putTypeStr(const UtString& type);

  //! Get the type string
  const UtString& getTypeStr() const;
  
  //! Put the port direction
  void putDirection(VhdlPortDir portDir) { mPortDir = portDir; }
  //! Get the port direction
  VhdlPortDir getDirection() const { return mPortDir; }
  
  // Currently, the following two methods are not used.

  //! If this in an integer subrange, set the left and right bounds
  void putIntegerValueRange(SInt32 left, SInt32 right);
  //! Get the integer subrange. NULL if it isn't a subrange
  const ConstantRange* getIntegerValueRange() const { return mRange; }
  
private:
  VhdlPortDir mPortDir;
  UtString mType;

  ConstantRange* mRange;
};

//! Object to store information about port types
/*!
  This is used for generating the VHDL test driver.  Maps top-level port
  names to their declared string types. It stores the IEEE
  package type, if any, used in the vhdl. It stores the top-level
  configuration name and library, if any. It also stores all the 
  package names that contain the type declarations used by the ports
  so that the packages can be properly specified in the generated
  testbench.
*/
class VhdlPortTypeMap
{
public:
  CARBONMEM_OVERRIDES

  //! constructor
  VhdlPortTypeMap();

  //! destructor
  ~VhdlPortTypeMap();

  //! Generate or get the VhdlPortType associated with the port name
  /*!
    If the port type was already generated the type information gets
    updated with the supplied type string.
  */
  VhdlPortType* genPortType(const UtString& name, const UtString& type);
  
  //! Generate a VhdlPortType that does not yet have type information
  /*!
    This is used in the populator to save off information about
    primary ports. But, at the point that we do that we may not have
    the vhNodes that are needed to get the type string.
  */
  VhdlPortType* genPortTypeBasic(const char* name);

  //! Get the VhdlPortType associated with name.
  /*!
    Returns NULL if no port with that name is found.
  */
  const VhdlPortType* getPortType(const UtString& name) const;
  
  //! Put the Vhdl top-level configuration, if any
  /*!
    Used by testdriver.
  */
  void putTopLevelConfig(const char* workLib, const char* configName);

  //! Does this design have a top level configuration?
  bool hasTopLevelConfig() const;

  //! Get the top-level configuration
  /*!
    Returns empty strings if there is no top level config
  */
  void getTopLevelConfig(UtString* workLib, UtString* configName) const;

  //! Stores the ieee package used
  void addIEEEPackage(UtString* package);

  //! Checks to see if the ieee package exists
  bool hasIEEEPackage(const UtString& package) const;

  //! Gets the ieee package list
  void getIEEEPackages(UtList<UtString>* packages) const;

  //! Stores the name of package where types of ports are defined
  /*!
    Used by testdriver
  */
  void putPackageName(UtString* packageName);

  //! Gets the list of packages used
  /*!
    Used by testdriver
  */
  void getPackageNames(UtList<UtString>* packages) const;
  
private:
  class Helper;
  Helper* mHelper;
};

#endif
