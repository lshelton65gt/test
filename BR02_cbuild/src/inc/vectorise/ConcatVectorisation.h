// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _CONCAT_VECTORISATION_H
#define _CONCAT_VECTORISATION_H

#include "vectorise/ConcatAnalysis.h"

//! \class ConcatVectorisation
/*! Constructs a vectorisation scheme from a netmap.
 *  \note The class is subclassed of NuToNuFn so that when replace leaves if
 *  called during application of the vectorisation to a module, this can be
 *  used as the functor.
 */

class ConcatVectorisation {
public:

  ConcatVectorisation (IODBNucleus *iodb, MsgContext *msg_context,
    ConcatAnalysis &context, 
    NUModule *module, UtOStream *summary, bool verbose) : 
    mIODB (iodb), mMsgContext (msg_context), 
    mConcatAnalysis (context), mModule (module), mSummary (summary), mVerbose (verbose) 
  {}

  virtual ~ConcatVectorisation ();

  //! option bits for construct
  enum ConstructOptions {
    cVECTORISE_PRIMARY = 1<<0         //! vectorise primary ports
  };

  //! Construct a vectorisation plan from a graph.
  bool construct (ConcatAnalysis::NetMap &, const UInt32 min_cluster_size, const UInt32 options);

  //! Populate a set with all the nets eliminated by this.
  void getEliminatedNets (NUNetSet *) const;

  //! Apply the plan
  bool apply ();
  bool apply (ConcatVectorisation &parent, NUModuleInstance *);

  //! \return iff this actually vectorises anything at all...
  bool isDegenerate () const { return mVectorNets.empty (); }

  //! \return iff a scalar net has been replaced in the vectorisation
  /*! \param scalar the scalar net under consideration
   *  \param vector if replaced, this returns the new vector net
   *  \param bit if replaced, this is the bit position in the vector
   */
  bool isReplaced (NUNet *scalar, NUNet **vector, SInt32 *bit) const;

  //! Write the output from -verboseXXXX
  void verbose ();

  //! Write a vectorisation report to a file.
  /*! \note This is really intended for a prelimimary release that rather
   *   than vectorising, gives explicit flattening hints.
   */

  enum Flags {
    cDEEP           = 1<<0,
    cNET            = 1<<1,           //!< output the net name (if created)
    cNET_FLAGS      = 1<<2,           //!< output the net flags
    cPRIMARIES      = 1<<3,           //!< specifically note any vectorised primary ports
    cDEFAULT        = cDEEP|cNET|cPRIMARIES
  };

  void report (UtOStream &, const UInt32 flags = cDEEP) const;

  //! Construct new vector ports and replace the module port vector
  /*! \note This does not delete the old nets */
  void replacePorts ();

  //! Lookup the replacement for a net.
  bool findReplacement (NUNet *, NUNet **, UInt32 *) const;

  //! \return the module to vectorise
  NUModule *getModule () const { return mModule; }

  //! Test whether a net is replaced
  bool lookup (NUNet *, NUNet **, UInt32 *) const;

private:

  // prevent assignment and copying
  ConcatVectorisation (const ConcatVectorisation &);
  ConcatVectorisation &operator = (const ConcatVectorisation &);

  // This instance constructs a vectorisation plan that consists of an
  // instances of ScalarNet for each scalar net that is tagged for
  // vectorisation, and an instance of VectorNet for each new vector net that
  // will be created. Instances of ScalarNet are links to the VectorNet that
  // denotes their vectorisation, while instance of VectorNet contain a
  // vector of the vectorised ScalarNet instances.

  class VectorNet;                    // forward declaration
  class ScalarNet;

  //! \class ScalarNet
  //! Representation of a scalar net that will be replaced by a vector bitsel.
  class ScalarNet {
  public:

    ScalarNet (NUNet *net, VectorNet *vector, const UInt32 bit) : 
      mNet (net), mVector (vector), mBit (bit), mReplaced (false) {}

    virtual ~ScalarNet ();

    //! \return the extant net that will be vectorised
    NUNet *getNet () const { return mNet; }

    //! \return the vector that replaces this net
    VectorNet *getVector () const { return mVector; }

    //! \return the bit in the vector that replaces this net
    UInt32 getBit () const { return mBit; }

    //! \return iff this is the msb of the vector
    UInt32 isMsb () const { return mVector->getMsb () == this; }

    //! \return iff this is a primary port
    bool isPrimaryPort () const { return mNet->isPrimaryPort (); }

    //! \return a printable representation of this
    const char *image () const { return mNet->getName ()->str (); }

    //! \return the size of the underlying net
    UInt32 getBitSize () const { return mNet->getBitSize (); }

    //! Mark the scalar net for deletion in the destructor
    void markAsReplaced () { mReplaced = true; }

    typedef UtList <ScalarNet *> List;
    typedef UtMap <NUNet *, ScalarNet *> Map;

    //! adds makeName to the standard vector
    class Vector : public UtVector <ScalarNet *> {
    public:
      Vector () {}
      virtual ~Vector () {}

      //! form a name from the vector of scalar names
      bool makeName (NUModule *, StringAtom **) const;

    };

  private:

    NUNet *mNet;                        //!< the net that will be replaced in the design
    VectorNet *mVector;                 //!< the vector that contains this
    const UInt32 mBit;                  //!< the bit in the vector that denotes this
    bool mReplaced;                     //!< the net has been replaced in the design

    // forbidden...
    ScalarNet (const ScalarNet &);
    ScalarNet &operator = (const ScalarNet &);

  };

  //! \class VectorNet
  //! Representation of the vector that replaces a list of scalars
  class VectorNet {
  public:

    VectorNet (ConcatAnalysis &, NUModule *module, bool verbose);

    virtual ~VectorNet () {}

    typedef UtList <VectorNet *> List;

    //! Construct the new net.
    //! \note No new nets may be added after calling freeze
    void freeze (NUModule *);

    //! has this vector been frozen
    bool isFrozen () const { return mFrozen; }

    //! \return the new net created for this vector
    NUNet *getNet () const;

    //! Append a scalar to the end of the list of scalars for this
    void append (ScalarNet *scalar);

    //! \return the bit size of this vector
    UInt32 getBitSize () const { return mBitSize; }

    //! \return the msb scalar of this vector
    ScalarNet *getMsb () const { return mScalars.back (); }

    //! \return iff the vector net devoid of scalars
    /*! \note this really should never happen, if there are no scalars then the
     *  vector net should not have been created in the first place.
     */
    bool isDegenerate () const { return mScalars.empty (); }

    //! Iterators across the scalars
    ScalarNet::Vector::iterator begin () { return mScalars.begin (); }
    ScalarNet::Vector::iterator end ()   { return mScalars.end (); }
    ScalarNet::Vector::const_iterator begin () const { return mScalars.begin (); }
    ScalarNet::Vector::const_iterator end () const   { return mScalars.end (); }
    ScalarNet::Vector::const_reverse_iterator rbegin () const { return mScalars.rbegin (); }
    ScalarNet::Vector::const_reverse_iterator rend () const   { return mScalars.rend (); }

    //! Populate a set with all the nets eliminated by this.
    void getEliminatedNets (NUNetSet *) const;

    /*! The port vectorisation object store its VectorNet objects in a set with
     *  an ordering function. The order is used only to get a deterministic
     *  order in the gold files.
     */

    struct Cmp {
      bool operator () (const VectorNet *a, const VectorNet *b) const
      { return NUNet::compare (a->mFirstScalar, b->mFirstScalar) < 0;    }
    };

    typedef UtSet <VectorNet *, Cmp> Set;

  private:

    enum {
      cMAX_NAME_SIZE = 128              //!< maximum length of names to form
    };

    ConcatAnalysis &mConcatAnalysis;    //!< context
    bool mFrozen;                       //!< has the vector been frozen
    UInt32 mBitSize;                    //!< bit size of the vector
    NUModule *mModule;                  //!< containing module
    bool mVerbose;                      //!< noisy or quiet
    NUNet *mNet;                        //!< the new vector net
    ScalarNet::Vector mScalars;         //!< the scalars replaced by this

    NUNet *mFirstScalar;                //!< first scalar in the set
    /*! \note this is used only for the comparison mechanism to get a
     *  deterministic ordering of the verbose output for golding.
     */

    // forbidden...
    VectorNet (const VectorNet &);
    VectorNet &operator = (const VectorNet &);

  };

  //! Construct an instance for a scalar net that will be replaced
  void makeScalar (VectorNet *vector, NUNet *net, const UInt32 bit);

  //! Construct an instance of a new vector net;
  VectorNet *makeVector (NUNetList &);

  IODBNucleus *mIODB;                   //!< plumbing...
  MsgContext *mMsgContext;              //!< more plumbing...
  ConcatAnalysis &mConcatAnalysis;      //!< parent context
  NUModule *mModule;                    //!< module containing the nets
  UtOStream *mSummary;                  //!< summary stream
  bool mVerbose;                        //!< be noisy when making nets
  ScalarNet::Map mScalarNets;           //!< all the scalar net instances
  VectorNet::List mVectorNets;          //!< all the vector net instances

};                                    // Vectorisation

#endif
