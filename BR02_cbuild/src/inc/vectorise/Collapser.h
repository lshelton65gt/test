// -*-C++-*-    $Revision: 1.5 $
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _COLLAPSER_H
#define _COLLAPSER_H

#include "nucleus/NUCost.h"
#include "vectorise/ConcatAnalysis.h"

//! Collapser class
/*!
  Implements collapsing of case statements and contiguous sequences of assign
  statements.
  
  To create a statement-sequence collapsing instance use:

  \code
    NUStmt *prototype_statement = first statement in sequence
    Collapser collapse (prototype_statement);
  \endcode

  For each successive potentially collapsible statement use:

  \code
    NUStmt *next_statement = next statement in sequence
    if (collapse.acrete (next_statement)) {
      next_statement matched the prototype_statement
    }
  \endcode
  
  If one or more statements matched the prototype statement, then create the
  replacement statement with:

  \code
    NUStmt *replacement;
    if (collapse.createCollapsedAssignment (&replacement)) {
      replacement is the collapsed equivalent of all the matched statement.
    }
  \endcode
  
  Matching case items is the same:

  \code
    NUCaseItem *prototype_item = first case item
    Collapser collapse (prototype_item);
    ...
    NUCaseItem *next_item = next case item
    if (collapse.acrete (next_item)) {
      next_item matched the prototype item
    }
  \endcode
  
  If all the case items matched the prototype, then the case statement is
  collapsed into two assignments, a selector net assignment and the collapsed
  case item statements:
  
  \code
    NUCase *case = the case statement that caused all this trouble;
    NUStmt *sel_assign;
    NUStmt *case_assign;
    if (collapse.createCollapsedCase (case, &sel_assign, &case_assign)) {
      sel_assign is the assignment of the selector expression to a created temporary
      case_assign is the single assignment that replaces the case
    }
  \endcode

  Recognition of collapsible sequences is implemented by the Matcher
  class. Collapser contains a single instance of Matcher. Matcher constructs
  a pattern matcher from a prototype object and then applies the pattern
  matcher to subsequent objects. A sequence of matcher items can then be
  collapsed into a simpler, equivalent item.  

  - Sequences of assignment statements are collapsed into a single
    assignment. 
  - Sequences of case items from a case statement are collapsed into a
    pair of assigments.

 */

class CommonConcat;
class IODBNucleus;

class Collapser {
public:

  //! constructor
  /*!
    \param param The inference parameters for this instantiaion
    \param scope The containing scope (used for synthesising nets) 
    \param head The first statement in the collapsible sequence
    \pre head != NULL
  */
  Collapser (const Inference::Params &params, NUScope *scope, IODBNucleus *, MsgContext *,
    const NUAssign *head, CommonConcat *);
  Collapser (const Inference::Params &params, NUScope *scope, IODBNucleus *, MsgContext *,
    const NUCaseItem *head, CommonConcat *);

  //! acrete
  /*! This method attempts to extend the Collapser with another item
    \param next The next statement or case item to consider
    \returns iff next is collapsible with the current context of this
    \post iff next is collapsible, then the context of this is extended
    
    \code
    iterator it;
    first_item = *(it++);
    Collapser collapse (first_item);
    ...
    next_item = *it;
    UInt32 n_matched = 0;
    while (!it.atend () && collapse.acrete (next_item)) {
    n_matched++;
    it++;
    }
    \endcode
  */

  bool acrete (const NUAssign *next)
  { return mMatcher.acrete (next); }

  bool acrete (const NUCaseItem *next)
  { return mMatcher.acrete (next); }

  //! \return iff the matcher pattern construction failed
  bool isFailed () const { return mMatcher.isFailed (); }

  //! createCollapsed
  /*!  
    This method constructs a collapsed equivalent for the list of assignments
    that were matched by this.  
    \param replacement Holds a reference to the constructed statement
    \returns iff an equivalent assignment statement was constructed
    \post if successful, then replacement denotes the equivalent statement
    \code
    if (n_matched == 0) {
    nothing matched
    } else if (collapse.createCollapsed (&replacement)) {
    remove the list of statements and splice in the replacement
    }
    \endcode
  */
  bool createCollapsed (NUStmt **replacement);

  //! createCollapsed
  /*!  
    This method constructs a pair of statements that replaces a matched case
    statement
    \param case The original case statement
    \param replacement Holds a reference to the constructed assignment
    \param selector Holds a reference to the constructed selector assignment
    statement
    \returns iff an equivalent statements were constructed
    \post if successful, then replacement denotes the equivalent assignment
    statement to the lvalue referenced by each case item
    \post if successful, then selector is a statement that assigns the correct
    value to the selector net used in replacement
    \code
    if (n_matched == 0) {
    nothing matched
    } else if (collapse.createCollapsed (case, &replacement, &sel_assign)) {
    remove the case statement and splice in the selector and replacement statements
    }
    \endcode
    \note A new temporary net is allocated. The net is assign in selector.
  */
  bool createCollapsed (const NUCase *case_stmt, NUAssign **replacement, NUAssign **selector);

  /*!
    \return the sliced lvalue
    \pre this is an assignment matcher
    \note This method must only be used for Inference diagnostic output
  */
  const NULvalue *getSliced () const { return mMatcher.getSliced (); }

  /*!
    \return iff the matched range is a constant range
    \pre this is an assignment matcher
    \note This method must only be used for Inference diagnostic output
  */
  bool isConstIndex () const { return mMatcher.getFirstMatchedIndex () == NULL; }

  /*!
    \return iff the match collapsed into a concatenation rather than a vector.
    \pre this is an assignment matcher
    \note This method must only be used for Inference diagnostic output
  */
  bool collapsedIntoConcat () const { return mMatcher.collapsedIntoConcat (); }

  /*!
    \return The range of the first matched index
    \pre isConstIndex
    \pre this is an assignment matcher
    \note This method must only be used for Inference diagnostic output
  */
  const ConstantRange getFirstMatchedRange () const { return mMatcher.getFirstMatchedRange (); }
  const ConstantRange getLastMatchedRange () const { return mMatcher.getLastMatchedRange (); }

  /*!
    \return The range of the last matched index
    \pre !isConstIndex
    \pre this is an assignment matcher
    \note This method must only be used for Inference diagnostic output
  */
  const NUExpr *getFirstMatchedIndex () const { return mMatcher.getFirstMatchedIndex (); }
  const NUExpr *getLastMatchedIndex () const { return mMatcher.getFirstMatchedIndex (); }

  //! print
  /*! This method dumps a human readable representation of the collapse pattern
    matcher. 
    \param deep Passed down to the nucleus print method for arguments that are
    nucleus objects.
    \param indent Number of characters to indent the output.
  */
  void print (bool deep = true, int indent = 0) const;

  //! Output the complexity of the replacement and the aggregate
  /*! This method writes the complexity of the aggregate cost of the matched
    statements to s. If replacement is non-NULL, then the complexity of the
    replacement cost is also written.
  */
  void dumpComplexity (UtOStream &s, const NUAssign *replacement = NULL) const
  {
    mMatcher.dumpComplexity (s, replacement);
  }

  /*! Check the complexity of replacement against the aggregate complexity.
    \param replacement The replacement statement for the matched assigns.
    \pre replacement is a correct replacement for the matched statements.
  */

  bool allowCollapse (const NUAssign *replacement) const
  {
    float aggregate_cost, replacement_cost;
    return mMatcher.allowCollapse (replacement, &aggregate_cost, &replacement_cost);
  }

  //! Concat adjacency extraction.
  /*! The extract method scans any concats in the set of matched assignment
   *  statements and tries to create edges the netmap for each pair of
   *  interesting that occur next to each other in either a left-side or a
   *  right-side concat.
   *
   *  \param context The concat analysis instance used to qualify nets.
   *  \param netmap The graph into which edges may be inserted
   *
   *  \note For each adjacent pair of nets (a,b) in each concat of the form
   *  {..., a, b, ...} extract will attempt to add an edge from a to b to
   *  netmap is both context.isInteresting (a) and context.isInteresting
   *  (b). That is, the ConcatAnalysis context decided which nets it is
   *  concerned with.
   *
   *  \note When extract finds an interesting pair of nets (a,b) it called
   *  netmap->maybeAddEdge (a,b). This will add a edge to the graph if there is
   *  no extant edge from a to be, neither a nor b have been scratched from the
   *  graph and the edge from a to b has not been scratched from the graph. If
   *  there is an extant edge, the weight of the edge gets incremented.
   */
  bool extract (ConcatAnalysis &context, ConcatAnalysis::NetMap *netmap);

private:

  MsgContext *mMsgContext;              //! for alerts

  // The matcher needs to embed secondary objects in a matching
  // plans to recognised and construct various constructs. These objects form a
  // class hierarchy with base class Collapser::Embedded.

  class Matcher;

  //! Output internal state dump when verboseAlert is specified
  void internalError (const char *file, const int line, const char *cond, 
    const NUBase *context0, const NUBase *context1 = NULL) const
  {
    mMatcher.internalError (file, line, cond, context0, context1);
  }


  //! \class Embedded
  /*! Registers the object with a matcher for deletion in the matcher
   *  destructor.
   */

  class Embedded {
  public:

    Embedded (Matcher &matcher) : mMatcher (matcher), mMsgContext (mMatcher.getMsgContext ())
    { 
      matcher.Register (this); 
    }

    virtual ~Embedded () {}

  protected:

    //! Output internal state dump when verboseAlert is specified
    void internalError (const char *file, const int line, const char *cond, 
      const NUBase *context0, const NUBase *context1 = NULL) const
    {
      mMatcher.internalError (file, line, cond, context0, context1);
    }

    Matcher &mMatcher;
    MsgContext *mMsgContext;
  };

  //! \class Constant
  /*! Internal representation of collapsible constants.
   */

  class Constant : public Embedded {
  public:

    //! Constructor
    /*! Initialises a Constant object from a nucleus constant
      \param range The initial bit range.
      \param value The nucleus constant.
      \param next Next item in deletion chain.
      \param size_to_range Trim the constant to fit in range.
    */
    Constant (Matcher &, MsgContext *, const ConstantRange &range, const NUConst *value,
      bool size_to_range);

    //! create a nucleus constant object from this
    bool create (NUConst **result) const;
    // post: result is an NUConst instance representing this constant
    // returns: iff the instance was successfully created

    //! set bits in this from a nucleus object
    bool set (const ConstantRange &range, const NUConst *);

    //! test a nucleus object against this
    /*! \return iff the nucleus object and this denote the same value 
     */
    bool test (const NUConst *) const;

    //! construct a human readable representation
    /*! \param s A string that will receive the representation
      \post The representation is appended to s
      \return s.c_str ()
      \note This is intended for debugging
    */
    const char *format (UtString *s) const;

    // These methods are intended for use from the debugger:

    //! write the human readable string
    void pr () const;

    //! form a nucleus object, display the object and then free the storage
    void pr2 () const;

    //! \return the number of matches applied to the constant
    UInt32 getNMatched () const { return mNMatched; }

    //! Rewind back to n_matched
    /*! If a constant is matched in an expression, and the expression
     *  subsequently fails later in the match, then the constant must be
     *  rewound to the previous state before create.
     *  \pre n_matched = getNMatched () || n_matched = getNMatched () + 1
     */
    void rewind (const UInt32 n_matched);

  private:

    const NUConst *mPrototype;          //!< prototype constant
    MsgContext *mMsgContext;            //!< for alerts
    DynBitVector mValue;                //!< value part of the constant
    DynBitVector mDrive;                //!< drive when mHasHZ
    ConstantRange mPrevRange;           //!< range before last match
    ConstantRange mRange;               //!< current bit range of this
    bool mHasXZ;                        //!< set when the constant contains X or Z
    UInt32 mNMatched;                   //!< incremented when another match acretes to the constant

  };

  //! \class ConstantFunction
  /*! Representation of functions in one variable derived from sequences of
   *  constants,
   */

  class ConstantFunction : public Embedded {
  public:
    
    /*! \param matcher The enclosing vector matcher
     *  \param prototype The first expression
     *  \param with Width of the expression
     *  \param index Value of the single variable for the first item
     *  \param value Value of the function for the initial variable value
     */
    ConstantFunction (Collapser::Matcher &matcher, const NUExpr *prototype, const UInt32 width,
      const UInt32 base_index, const UInt32 base_value);
    ~ConstantFunction () {}

    //! Match the next (index, value) pair
    bool matchNext (const UInt32 index, const UInt32 value);

    //! Create an expression representing the function over a net
    bool create (NUNet *, NUExpr **) const;

    //! dump a human readable representation
    void print (UtOStream &s, const bool deep, const UInt32 indent) const;

  private:

    const NUExpr *mPrototype;            //!< first expression
    const UInt32 mWidth;
    enum {
      cIS_AFFINE = 1<<0,
      cIS_POWER  = 1<<1
    };
    UInt32 mFlags;
    UInt32 mNMatched;                   //!< number of matches
    const UInt32 mBaseIndex, mBaseValue;//!< index and value of the first item

    // Matching constants against (1<<index) + offset
    struct Power {
    public:

      Power (const NUExpr *prototype, const UInt32 base_index, const UInt32 base_value) : 
        mPrototype (prototype), mOffset (base_value - (1<<base_index)) {}

      //! Match the next index, value pair against the function
      /* \return iff value = (1<<index) + offset
       */
      bool next (const UInt32 n_matched, const UInt32 index, const UInt32 value);

      //! Create the expression (1<<index) + offset
      bool create (const UInt32 width, NUNet *, NUExpr **expr) const;

      const NUExpr *mPrototype;         //!< the first expression
      SInt32 mOffset;                   //!< computed offset
    } mPower;

    // Matching constants against multiplier * index + offset
    struct Affine {
      
      Affine (const NUExpr *prototype) : mPrototype (prototype), mMultiplier (0), mOffset (0) {}

      //! Match the next index, value against the function
      /*! \return iff value = multiplier * index + offset
       */
      bool next (const UInt32 n_matched, const UInt32 base_index, const UInt32 base_value, 
        const UInt32 index, const UInt32 value);
      
      //! construct the expression multiplier * index + offset
      bool create (const UInt32 width, NUNet *, NUExpr **) const;

      const NUExpr *mPrototype;         //!< the first expression
      SInt32 mMultiplier, mOffset;      //!< function coefficients
    } mAffine;

  };

  //! \class LvalueConcat
  /*! Accumulates the components of a left-side concat when matching successive
   *  lvalues.
   */
  class LvalueConcat : public Embedded {
  public:

    /* \param matcher The enclosing vector matcher
     * \param lvalue The first left-side
     */

    LvalueConcat (Matcher &matcher, const NULvalue *lvalue) : 
      Embedded (matcher), mPrototype (lvalue), mLvalue (NULL), 
      mFixedSegments (), mFloatingSegments (), mBound () {}

    /*! Each successively matched left-side is represented by a segment
     *  instance that encapsulates the lvalue, the lsb in the constructed
     *  concat and the width
     */

    struct Segment {
      Segment () : mLsb (0), mWidth (0), mLvalue (NULL) {}
      Segment (SInt32 lsb, UInt32 width, const NULvalue *lvalue) :
        mLsb (lsb), mWidth (width), mLvalue (lvalue) {}
      SInt32 mLsb;                      //!< position in the collapsed set
      UInt32 mWidth;                    //!< width in the collapsed set
      const NULvalue *mLvalue;          //!< the component
    };


    typedef UtMap <const SInt32, struct Segment> FixedSegments;
    typedef UtMap <const NULvalue *, struct Segment> FloatingSegments;

    //! Attempt to match each segment with a bit position.
    bool resolve (bool *floating);

    //! Add a segment to the concat
    void acrete (const NULvalue *lvalue, UInt32 width);

    //! Bind an lvalue in the concat to a fixed position
    bool fixate (const NULvalue *lvalue, SInt32 lsb);

    // Queries
    const NULvalue *getPrototype () const { return mPrototype; }
    const NULvalue *getCurrent () const { return mLvalue; }

    // Assorted iterators...

    typedef FloatingSegments::iterator floating_iterator;
    typedef FixedSegments::iterator fixed_iterator;
    typedef FloatingSegments::const_iterator floating_const_iterator;
    typedef FixedSegments::const_iterator fixed_const_iterator;
    typedef FixedSegments::reverse_iterator fixed_reverse_iterator;

    fixed_iterator fixed_begin () { return mFixedSegments.begin (); }
    fixed_iterator fixed_end () { return mFixedSegments.end (); }
    fixed_reverse_iterator fixed_rbegin () { return mFixedSegments.rbegin (); }
    fixed_reverse_iterator fixed_rend () { return mFixedSegments.rend (); }

    floating_iterator floating_begin () { return mFloatingSegments.begin (); }
    floating_iterator floating_end () { return mFloatingSegments.end (); }

    fixed_const_iterator fixed_begin () const { return mFixedSegments.begin (); }
    fixed_const_iterator fixed_end () const { return mFixedSegments.end (); }
    floating_const_iterator floating_begin () const { return mFloatingSegments.begin (); }
    floating_const_iterator floating_end () const { return mFloatingSegments.end (); }

    // Diagnostics
    void pr (UtOStream &, bool deep, int indent) const;
    void pr () const;

  private:

    const NULvalue *mPrototype;         //!< the first lvalue
    const NULvalue *mLvalue;            //!< the current lvalue

    FixedSegments mFixedSegments;       //!< segments with a fixed bit position
    FloatingSegments mFloatingSegments; //!< segments without a fixed position

    UtSet <SInt32> mBound;              //!< track the occuppied bit positions

  };

  
  //! \class RvalueConcat
  /*! Accumulates the components of a right-side concat when matching successive
   *  expressions.
   */
  class RvalueConcat : public Embedded {
  public:
    RvalueConcat (Matcher &matcher) : Embedded (matcher), mMap () {}

    /*! Each successively matched left-side is represented by a segment
     *  instance that encapsulates the lvalue, the lsb in the constructed
     *  concat and the width
     */

    struct Segment {
      Segment () : mIndex (0), mWidth (0), mExpr (NULL) {}
      Segment (UInt32 index, UInt32 width, const NUExpr *expr) :
        mIndex (index), mWidth (width), mExpr (expr) {}
      UInt32 mIndex;                    //!< position in the collapsed set
      UInt32 mWidth;                    //!< width in the collapsed set
      const NUExpr *mExpr;              //!< the concat segment
    };

    typedef UtMap <const UInt32, struct Segment> ConcatMap;

    void append (const UInt32 index, const UInt32 width, const NUExpr *expr)
    {
      Segment segment (index, width, expr);
      mMap [index] = segment;
    }

    ConcatMap mMap;

    // Diagnostics
    void pr (UtOStream &, bool deep = false, int indent = 0);
    void pr ();

  };

  //! Pattern matcher symbols
  /*! The pattern matcher is explicitly represented as a sequence of symbols
    and operands. The symbols are defined in Collapser.def which gets multiply
    abused by the preprocessor. Here, the INSTRUCTION_SYMBOL and
    INSTRUCTION_ARG macros are defined to define the enum types. Unfortunately,
    if you're reading the pretty doxygenated document you just cannot see it!
  */

  enum Symbol {
#define INSTRUCTION_SYMBOL(_tag_, _nargs_) e##_tag_,
#define INSTRUCTION_ARG(_tag_, _type_) e##_tag_##Arg,
#include "vectorise/Collapser.def"
    eNSymbols                           //!< number of distinct symbol types
  };

  //! Matcher instructions
  /*! The collapse pattern matcher is constructed as a vector of
    Collapser::Instruction instances. An instance is either an matcher operator
    or a matcher argument.
  */

  struct Instruction {

    //! Default constructor - does nothing
    Instruction () {}

    /*! An instruction is instantiated with either an operator symbol or with
      an argument. Note that because doxygen does not process the include
      directive the argument constructors are not visible in the html.

    */

    Instruction (const Symbol symbol, const int lineno);
    Instruction (const Symbol symbol);  

#define INSTRUCTION_ARG(_tag_, _type_) \
    Instruction (_type_ arg);          \
    _type_ &get##_tag_ ();
#include "vectorise/Collapser.def"

    //! \returns the pattern matcher symbol
    Symbol getSymbol () const { return static_cast <Symbol> (mSymbol); }

    //! \returns one plus the number of arguments
    UInt16 getWidth () const { return mWidth; }

    //! prints out a human readable form of the instruction
    void print (bool deep, int indent);

    //! produce a human readable identifier for matcher instructions
    static const char *cvt (const Symbol);

  private:

    UInt16 mSymbol;
    //!< The pattern symbol.
    UInt16 mWidth;
    //!< One plus the number of arguments.

    // when mSymbol is eOperand, the element is an argument for the preceding
    // element... note that constants can acrete multiple operands...

    union {
      int mLineno;
#define INSTRUCTION_ARG(_tag_, _type_) _type_ m##_tag_;
#include "vectorise/Collapser.def"
    } mArg;
    //!< Look in Collapser.def for the full list of argments.

#define INSTRUCTION_ARG(_tag_, _type_) void print_##_tag_ (bool deep, int indent) const;
#include "vectorise/Collapser.def"

  };

  // The matcher builds plans as series of instructions embedding in
  // blocks. The first block is allocated within the matcher instance. Overflow
  // blocks are chained through mNext.

  enum {
    eBLOCK_SIZE = 32,
    eCONSTANT_POOL_SIZE = 32
  };

  //! Instruction blocks
  /*! Arrays of instructions are allocated in instruction blocks. The first
    block of instructions is allocated within the Matcher instance. In
    particular, if the Collapser object is allocated on the stack, then the
    first block of instructions is also allocated on the stack. Thus no
    storage allocation is needed for most Collapser instantiations. When
    pattern matcher generation overflows a block, another block of
    instructions is dynamically allocated and chained via the mNext member.
  */
  struct Block {

    Block () : mLength (0), mNext (NULL) {}

    UInt32 mLength;                     //!< number of instructions in this block
    struct Block *mNext;                //!< next instruction block in the chain
    Instruction mInstruction [eBLOCK_SIZE];
    //!< the payload...

  };

  //! Pattern Matcher for sequence
  /*! This class implements a pattern matcher for sequences of nucleus objects
    and a method for collapsing matched sequences into equivalent simpler
    constructs.
  */

  class Matcher {
  public:

    friend class Embedded;

    //! Flags that modify the behavious of the matcher
    enum Flags {
      // Thes flags tweak the global behaviors of the matcher.
      eASSIGNS         = 1<<0,          //!< matching a list of assigns
      eCASE            = 1<<1,          //!< matching in a case statement
      eVERBOSE_ALERTS  = 1<<2,          //!< dump internal state with alerts
      // These flags are passed down the build, apply, or create well to tweak
      // the behavior in local regions of the nucleus tree
      eSCALAR          = 1<<3,          //!< matching in a non-vector component
      eNO_XZ           = 1<<4,          //!< reject matches of X or Z in constants
      eVARSEL_IDENT    = 1<<5,          //!< matching in the ident part of a varsel
      eINDEX           = 1<<6,          //!< matching in the index of a varsel
      eCOND            = 1<<7,          //!< matching in the condition of eTeCond
      eSCALAR_COND     = 1<<8,          //!< do not vectorise a ternary cond
      eTEST_Z          = 1<<9,          //!< test for Z const (override NO_XZ)
      eBUILD_IDENT_VEC = 1<<10,         //!< construct a vector on NUIdentRvalues
      eINNER           = 1<<11,         //!< creating a non-root node of an expression
      eCONCAT          = 1<<12,         //!< create a concat node
      eLHSCONCAT       = 1<<13,         //!< constructing a lhs concat
      eALLOW_LHSCONCAT = 1<<14,         //!< allow construction of lhs concats
      eALLOW_RHSCONCAT = 1<<15,         //!< allow construction of rhs concats from idents
      eALLOW_CONCAT    = eALLOW_LHSCONCAT|eALLOW_RHSCONCAT,
      // These flags are passed back as synthesised attribute
      eIS_Z            = 1<<16,         //!< found a constant that was all Z
      eCONSTANT        = 1<<17,         //!< expression is constant
      eLSB_ONLY        = 1<<18,         //!< only using the LSB of an expression
      eREDUCING        = 1<<19,         //!< doing a reducing operator
      eNONE            = 0
    };

#ifdef CDB
    static const char *cvt (char *s, const UInt32 size, const UInt32 flags);
#endif

    //! Constructor
    /*!
      \param params The Inference options for this instantiation.
      \param scope The containing scope.
      \param flags eCASE or eASSIGNS
    */
    Matcher (const Inference::Params &params, NUScope *scope, IODBNucleus *, MsgContext *,
      CommonConcat *concats, const UInt32 flags);

    ~Matcher ();

    //! Register an embedded object for deletion by this Matcher destructor
    void Register (Embedded *embedded) { mEmbedded.push_back (embedded); }

    //! Build a proxy matcher for assignment sequences
    /*! Given that for most sequences of statements adjacent pairs will not
      match, the pattern matcher is not constructed immediately. Instead a
      pattern consisting of a proxy symbol and a reference to the prototype is
      built. The real matcher is constructed on the first matcher application.
      \param prototype The first assignment statement in the sequence under
      consideration.
    */
    void buildProxyMatcher (const NUAssign *prototype);

    //! Build a proxy matcher for case item sequences
    /*! \param prototype The first case item in the sequence.
     */
    void buildProxyMatcher (const NUCaseItem *prototype);

    //! consider the next assignment statement against the pattern matcher.
    /*!
      \pre This is a matcher for assignment lists.
      \param stmt The next statement to consider.
      \returns iff stmt matched and can be collapsed along with the other
      matched statements.
    */
    bool acrete (const NUAssign *stmt);

    //! Consider the next case item statement against the pattern matcher.
    /*!
      \pre This is a matcher for case item lists.
      \param item The next case item to consider.
      \returns iff item matched and can be collapsed along with the other
      matched case items.
    */
    bool acrete (const NUCaseItem *item);

    //! Synthesise a statement that is equivalent to all the matched statements
    /*! Once a sequence of assignments has been matched, the equivalent
      collapsed statement is constucted by the collapse method.
      \pre This is an assignment sequence matcher.
      \returns iff an equivalent statement was constructed
      \param stmt Handle on the equivalent statement.
      \post if successful, then stmt denotes the equivalant statement.
    */
    bool collapse (NUStmt **stmt);

    //! Extract concat adjacency information from the matcher.
    /*! \note see the description for Collapser::extract for an outline of
     *  extraction.
     *  \note This method scans the matcher operations in the matcher plan
     *  looking for matches to left-side or right-side concats.
     */
    bool extract (ConcatAnalysis &, ConcatAnalysis::NetMap *);

    //! Synthesise a pair of statements that is equivalent to the case statement
    /*! If all the case items in a case statement matched then the case
      statement may be replaced by a pair of assignments. This method
      synthesised those statements,
      \pre This is case item matcher.
      \pre All the items in case_stmt matched in this.
      \returns iff an equivalent pair of statements was constructed
      \param case_stmt The case statement
      \param stmt Handle on the returned case item assignment
      \param selector Handle on the returned case selector assignment
      \post if successful, then stmt and selector denote the synthesised statements.
    */
    bool collapse (const NUCase *case_stmt, NUAssign **stmt, NUAssign **selector);

    //! isFailed returns true if the matcher will never match a collapsible sequence
    bool isFailed () const { return mIsFailed; }

    const NULvalue *getSliced () const { return mSlicedLHS; }
    const ConstantRange getFirstMatchedRange () const { return mFirstMatchedRange.fakeRange (); }
    const ConstantRange getLastMatchedRange () const { return mLastMatchedRange.fakeRange (); }
    const NUExpr *getFirstMatchedIndex () const { return mFirstMatchedIndex; }
    const NUExpr *getLastMatchedIndex () const { return mLastMatchedIndex; }
    bool collapsedIntoConcat () const { return mCollapsedIntoConcat; }


    //! return the top nucleus object
    const NUBase *getHead () const { return mHead; }

    //! print a human readble representation
    void print (bool deep, int indent) const;
    //! shorthand print for the debugger
    void pr () const;

    //! Output the complexity of the replacement and the aggregate
    /*! This method writes the complexity of the aggregate cost of the matched
      statements to s. If replacement is non-NULL, then the complexity of the
      replacement cost is also written.
    */
    void dumpComplexity (UtOStream &s, const NUAssign *replacement) const;

    /*! Check the complexity of replacement against the aggregate complexity.
      \param replacement The replacement statement for the matched assigns.
      \pre replacement is a correct replacement for the matched statements.
    */
    bool allowCollapse (const NUAssign *replacement, 
      float *aggregate_cost, float *replacement_cost) const;

    //! classify an assignment into eAssignBlocking, eAssignContinuous, eAssignNonBlocking
    Symbol getFlavour (const NUAssign *assign);

    //! access the message handle
    MsgContext *getMsgContext () const { return mMsgContext; }

    //! Output internal state dump when verboseAlert is specified
    void internalError (const char *file, const int line, const char *cond, 
      const NUBase *, const NUBase * = NULL) const;

  private:

    CommonConcat *mConcats;
    const Inference::Params &mParams;   //!< Inference options passed to the constructor

    /*! When vectorising a sequence of assignments to a scalar into an lvalue
      concat, the span is considered "abstract". That is, just the width but
      no lsb defined.
    */

    class Span : private ConstantRange {
    public:

      enum Direction {
        eIncreasing,                    //!< varsel index is increasing
        eDecreasing,                    //!< varsel index is decreasing
        eExpanding,                     //!< varsel index may go either way
        eUnknownDirection
      };                                //!< direction of change

      enum Flavour {
        cBOGUS = 0,                     //!< uninitialised, useless!
        cCONCRETE = 1,                  //!< concrete span where lsb is fixed
        cFLOATING = 2,                  //!< abstract span with unfixed lsb
        cFIXED = 3                      //!< abstract span where lsb was fixed by rhs
      };

      Span ();
      Span (const ConstantRange &range);
      Span (const NULvalue *, const SInt32 lsb, const SInt32 msb, const Flavour flavour,
        const SInt32 index = -1);

      // coercions

      const ConstantRange bindRange (const Matcher &matcher) const
      {
        const SInt32 lsb = getLsb (matcher);
        const SInt32 msb = lsb + getLength () - 1;
        return ConstantRange (msb, lsb);
      }

      const ConstantRange fakeRange () const 
        // this must *only* be used for diagnostics
      { return ConstantRange (ConstantRange::getMsb (), ConstantRange::getLsb ()); }

      // assignment

      Span &operator = (const ConstantRange &range);
      Span &operator = (const Span &range);

      // queries

      SInt32 getLsb (const Matcher &) const;
      SInt32 getMsb (const Matcher &) const;

      inline SInt32 getIndex () const { return mIndex; }
      inline UInt32 getLength () const { return ConstantRange::getLength (); }
      inline Direction getDirection () const { return mDirection; }
      inline const NUBase *getAnchor () const { return mAnchor; }

      // predicates

      inline bool isIncreasing () const { return mDirection == eIncreasing; }
      inline bool isDecreasing () const { return mDirection == eDecreasing; }
      inline bool isExpanding () const { return mDirection == eExpanding; }

      inline bool isAnchoredAt (const NUBase *anchor) const { return mAnchor == anchor; }

      inline bool operator == (const Span &x) const
      { return mFlavour == x.mFlavour 
          && ConstantRange::getLsb () == x.ConstantRange::getLsb () 
          && ConstantRange::getMsb () == x.ConstantRange::getMsb (); }

      inline bool operator == (const ConstantRange &x) const
      { return mFlavour == cCONCRETE 
          && ConstantRange::getLsb () == x.getLsb () 
          && ConstantRange::getMsb () == x.getMsb (); }

      inline bool operator != (const ConstantRange &x) const
      { return mFlavour != cCONCRETE 
          || ConstantRange::getLsb () != x.getLsb () 
          || ConstantRange::getMsb () != x.getMsb (); }

      inline Flavour getFlavour () const { return mFlavour; }
      inline bool isFloating () const { return mFlavour == cFLOATING; }
      inline bool isConcrete () const { return mFlavour == cCONCRETE; }

      inline bool isSameLength (const Span &x) const 
      { return getLength () == x.getLength (); }

      inline bool isJustAbove (const Matcher &matcher, const Span &x) const 
      { return getLsb (matcher) == x.getMsb (matcher) + 1; }

      inline bool isJustBelow (const Matcher &matcher, const Span &x) const 
      { return getMsb (matcher) == x.getLsb (matcher) - 1; }

      inline bool isAdjacent (const Matcher &matcher, const ConstantRange &x, const SInt32 offset) const
      { 
        return getLsb (matcher) - offset == x.getMsb () + 1 
          || getMsb (matcher) - offset == x.getLsb () - 1; 
      }

      inline bool isContainedBy (const ConstantRange *range) const
      { ASSERT (mFlavour == cCONCRETE); return range->contains (*this); }


      // operations
      
      void extendDown (const UInt32);
      void extendUp   (const UInt32);
      void bind (const Direction x, const NUBase *context);
      void fixate (const SInt32 lsb, const NUBase *context);

      // diagnostics

      const char *image (char *buffer, const int size) const;
      void pr () const;

    private:

      Flavour mFlavour;
      const NULvalue *mLvalue;
      Direction mDirection;
      const NUBase *mAnchor;
      SInt32 mIndex;
    };

    class FloatingSpan : public Span {
    public:
      FloatingSpan (const NULvalue *lvalue, const UInt32 width, const SInt32 index);
    };

    //! Matcher::iterator
    /*! The matcher iterator provides sequential access to the linear
      instruction sequences of the pattern matcher.
    */

    class iterator {
    public:

      friend class Matcher;

      iterator () : 
        mMatcher (NULL), mBlock (NULL), mIndex (0) {}

      //! instantiate an iterator at the beginning of a matcher instruction series 
      iterator (const Matcher *matcher) : 
        mMatcher (const_cast <Matcher *> (matcher)), mBlock (&mMatcher->mFirst), mIndex (0) {}

      //! access an instruction via an offset
      /*! \param n Offset from the iterator location
        \return a reference to instruction n places after the iterator
        location
        \note It is permitted to modify the instruction via the returned
        reference
      */
      Instruction &operator [] (const int n) 
      { 
        //
        // NOTE - this code assumes that the maximum number of operands for any
        // pattern matcher instruction is less than eBLOCK_SIZE. I can't
        // imagine any reason to make eBLOCK_SIZE so small that this would be a
        // problem....
        if (mIndex + n < mBlock->mLength) {
          return mBlock->mInstruction [mIndex + n];
        } else {
          NU_ASSERT (mBlock->mNext != NULL, mMatcher->mHead);
          return mBlock->mNext->mInstruction [mIndex + n - mBlock->mLength];
        }
      }

      bool isInBounds (const int n) const
      {
        if (mIndex + n < mBlock->mLength) {
          return true;                  // it's in this block
        } else if (mBlock->mNext != NULL
          && mIndex + n - mBlock->mLength < mBlock->mNext->mLength) {
          return true;
        } else {
          return false;
        }
      }

      Instruction &operator * () { return mBlock->mInstruction [mIndex]; }
      Instruction *operator -> () { return &mBlock->mInstruction [mIndex]; }

      Symbol getSymbol () const { return mBlock->mInstruction [mIndex].getSymbol (); }

      //! increment operator
      /*! Increment the iterator past the current instruction
        \note The increment operator will also skip the instruction arguments
      */

      void operator ++ (int)
      {
        mIndex += mBlock->mInstruction [mIndex].getWidth ();
        if (mIndex >= mBlock->mLength) {
          mIndex -= mBlock->mLength;
          mBlock = mBlock->mNext;
        }
      }

      //! termination predicate
      bool atEnd () const 
      { 
        // When increment hits the end of a block, it follows the mNext link
        // into the next block in the chain. Usually, when we walk past the
        // last instruction, the next link is NULL. However, a truncate
        // instruction may have left an allocate block with no instructions at
        // the end of the block chain.
        return mBlock == NULL || mIndex >= mBlock->mLength;
      }

      //! bind an iterator to the current matcher instruction append point
      void bind (Matcher *matcher)
      {
        mMatcher = matcher;
        mBlock = matcher->mCurrent;
        mIndex = mBlock->mLength;
      }

    private:

      Matcher *mMatcher;                //!< the matcher containing the instructions
      Block *mBlock;                    //!< current block of the iteration point
      UInt32 mIndex;                    //!< index in current block

    };

    friend class iterator;

    void truncate (iterator &it) 
    { 
      it.mBlock->mLength = it.mIndex;
      if (it.mBlock->mNext != NULL) {
        // This instruction spans the end of the current instruction block. We
        // also need to zero out the next block and then wind back the current
        // block pointer that denotes the append point in the matcher.
        it.mMatcher->mCurrent = it.mBlock;
        it.mBlock->mNext->mLength = 0;
      }
    }

    UInt32 mFlags;                      //!< flags pass to the constructor
    /*!< These flags are passed to each build, apply, create instance */

    // Metrics
    NUCost mAggregateCost;              //! cumulative cost of matched items
    const bool mUseMetrics;             //! set if metrics should be considered
    const bool mVerboseMetrics;         //! output metric information
    const UInt32 mThreshold;            //! threshold for passing a collapse

    NUScope *mScope;                    //!< scope passed to the constructor
    /*!< Used by the case matcher to synthesise the selector net. */

    IODBNucleus *mIODB;                 //!< needed to test for protected nets
    MsgContext *mMsgContext;            //!< for writing errors and alerts

    const NUBase *mHead;                //!< Top-level prototype item.
    bool mIsFailed;                     //!< Set when a failing matcher is produced
    /*!< When matcher construction recognises an unmatchable construct this
      flag is set. Collapser::acrete will test this flag before bothing to
      apply the matcher */

    UInt32 mNMatched;                   //!< number of statements matched
    UInt32 mNextMark;                   //!< next mark for instruction stream
    /*! Number marks are used both for sanity checks in the instruction stream
      and to skip over chunks of the pattern during synthesis. */
    struct Block mFirst;                //!< statically allocated first instruction block
    struct Block *mCurrent;             //!< current instruction block for build

    UtList <Embedded *> mEmbedded;      //!< embedded objects for deletion in this destructor

    LvalueConcat *mLvalueConcat;

    // Keep a reference to the net referenced on the lhs of the assignment so
    // that assignments in which the lhs net also occurs on the rhs can be
    // checked for consistents.
    //! net on lhs of an assignment
    /*! \note this must be used only in assignment matchers
     */
    class NUNetSet : public UtSet <const NUNet *> {
    public:
      NUNetSet () : UtSet <const NUNet *> () {}
      bool isMember (const NUNet *net) const { return find (net) != end (); }
      void pr () const;
    };
    
    NUNet *mLHSNet;                     //!< net on lhs of vectorising assignment
    NUNetSet mLHSNets;                  //!< nets on lhs of concat assignment
    NUNetSet mRHSNets;                  //!< nets on rhs of concat assignment
    const NULvalue *mSlicedLHS;         //!< use for debug information
    Symbol mAssignFlavour;              //!< eAssignBlocking, NonBlocking or Continuous

    //! Validate a rhs net
    /*! When construction a lhs concat recursive references are not allowed.
      \param net The net to be checked
      \returns mLHSNets.isMember (net)
      \post if !mLHSNets.isMember (net) then mRHSNets.isMember (net)
    */
    bool addRhsNet (const NUNet *net);

    //! Validate a lhs net
    /*! When construction a lhs concat recursive references are not allowed.
      \param net The net to be checked
      \returns mRHSNets.isMember (net)
      \post if !mRHSNets.isMember (net) then mLHSNets.isMember (net)
    */
    bool addLhsNet (const NUNet *net);

    //! Mark the pattern matcher as failed
    /*! When build recognises a construct that can never collapse it calls the
      fail method. This inserts an eFail symbol into the stream and sets
      mIsFailed.
    */
    void fail (const int lineno);
    void fail ();

    //! append another instruction into the pattern matcher.
    void append (const Instruction &);

    //! create an iterator that denotes the current insertion point in the pattern.
    /*! Some constructs requre reserving a place in the pattern instruction
        stream, build more of the pattern, and then writing back into the
        reserved location. This method reserves a place and sets an iterator
        that may be used to modify the stream.
    */
    void append (iterator *it);

    //! construct a proxy matcher for expressions.
    /*! When a pattern matcher for an assignment statement is built, rather
      than building the complete matcher for the rhs, a proxy matcher is
      appended to the instruction stream. Then, if the lhs matches, application
      of the proxy matcher induces construction of the real matcher for the
      expression.

      This reduces overhead when a new matcher is applied to the second
      assignment in a sequence and the lhs is different.
    */
    void buildProxyMatcher (const NUExpr *prototype, const UInt32 flags);

    //! Expand all the proxies in the matcher plan into a real plan.
    /*! In some instances, a collapse method may be called when the matcher has
      been built but ever applied. In this instance, the plan will include
      proxies. The method expands all the proxies into real plans.
    */

    void expandProxies ();

    //! Build a pattern matcher for a construct.
    /*! Adds instructions for matching proto onto the matcher instruction
      stream.
      \param proto The prototype nucleus object.
      \param flags Matcher flags
      \note Some of the build methods return a net. For example, the build for
      a NUVarselLvalue passes back the lhs net.
      \note Some of the build methods return a range. For example the build for
      NUConcatLvalue returns the range of the complete concat lvalue.
    */

    void build (const NUAssign *proto,       const UInt32 flags);
    void build (const NUCaseItem *proto,     const UInt32 flags);
    void build (const NUConst *proto,        const UInt32 flags, UInt32 *attr);
    void build (const NUExpr *proto,         const UInt32 flags, UInt32 *attr);
    void build (const NUBinaryOp *proto,     const UInt32 flags, UInt32 *attr);
    void build (const NUUnaryOp *proto,      const UInt32 flags, UInt32 *attr);
    void build (const NUTernaryOp *proto,    const UInt32 flags, UInt32 *attr);
    void build (const NUConcatLvalue *proto, const UInt32 flags, NUNet **, Span *span);
    void build (const NUIdentLvalue *proto,  const UInt32 flags, NUNet **, Span *span);
    void build (const NUIdentRvalue *proto,  const UInt32 flags, NUNet **, UInt32 *attr);
    void build (const NULvalue *proto,       const UInt32 flags, NUNet **, Span *span);
    void build (const NUMemselLvalue *proto, const UInt32 flags, NUNet **, Span *span);
    void build (const NUMemselRvalue *proto, const UInt32 flags, NUNet **, UInt32 *attr);
    void build (const NUVarselLvalue *proto, const UInt32 flags, NUNet **, Span *span);
    void build (const NUVarselRvalue *proto, const UInt32 flags, NUNet **, UInt32 *attr);
    void build (const NUConcatOp *proto,     const UInt32 flags, UInt32 *attr);

    //! apply
    /*! This method applies a pattern matcher to a nuclues tree.
      \param n The location in the matcher we are currently matching
      \param test The nucleus tree to test against the pattern.
      \note The apply method will replace any proxies in the pattern with an
      actual matcher.
      \note Some of the apply methods return a range. For example the apply
      method for NUVarselLvalue returns the partition.
    */

    bool apply (iterator &n, const NUAssign *test,       const UInt32 flags);
    bool apply (iterator &n, const NUCaseItem *test,     const UInt32 flags);
    bool apply (iterator &n, const NUConst *test,        const UInt32 flags);
    bool apply (iterator &n, const NUConcatOp *test,     const UInt32 flags);
    bool apply (iterator &n, const NUExpr *test,         const UInt32 flags);
    bool apply (iterator &n, const NUBinaryOp *test,     const UInt32 flags);
    bool apply (iterator &n, const NUUnaryOp *test,      const UInt32 flags);
    bool apply (iterator &n, const NUTernaryOp *test,    const UInt32 flags);
    bool apply (iterator &n, const NUConcatLvalue *test, const UInt32 flags, Span *range);
    bool apply (iterator &n, const NUIdentLvalue *test,  const UInt32 flags, Span *range);
    bool apply (iterator &n, const NUIdentRvalue *test,  const UInt32 flags);
    bool apply (iterator &n, const NULvalue *test,       const UInt32 flags, Span *range);
    bool apply (iterator &n, const NUMemselLvalue *test, const UInt32 flags, Span *range);
    bool apply (iterator &n, const NUMemselRvalue *test, const UInt32 flags);
    bool apply (iterator &n, const NUVarselLvalue *test, const UInt32 flags, Span *range);
    bool apply (iterator &n, const NUVarselRvalue *test, const UInt32 flags);

    //! Extract
    /*! Extract the concat-adjacency information from the matcher plan for a
     *  given concat type.
     *  \note extract <NUConcatLvalue> is used to extract the information for a
     *  left-side concat matched with an eLvalueConcat match operator.
     *  \note extract <NUConcatOp> is used to extract the information for a
     *  right-side concat matched with an eRvalueConcat match operator.
     */

    template <class ConcatType> bool extract (iterator &n, ConcatAnalysis &, ConcatAnalysis::NetMap *);

    //! create
    /*! This method takes the matcher plan and state and synthesises the
      equivalent collapsed statement for a matched set of items.
      \pre The matcher matched at least one statemnt.
      \param n The location in the pattern for synthesis.
      \param result Handle for the result.
    */
    void create (iterator &n, NUAssign **result,       const UInt32 flags);
    void create (iterator &n, NUBinaryOp **result,     const UInt32 flags);
    void create (iterator &n, NUConcatOp **result,     const UInt32 flags);
    void create (iterator &n, NUConst **result,        const UInt32 flags);
    void create (iterator &n, NUExpr **result,         const UInt32 flags);
    void create (iterator &n, NUIdentRvalue **result,  const UInt32 flags);
    void create (iterator &n, NUConcatLvalue **result, const UInt32 flags);
    void create (iterator &n, NUIdentLvalue **result,  const UInt32 flags);
    void create (iterator &n, NULvalue **result,       const UInt32 flags);
    void create (iterator &n, NUMemselLvalue **result, const UInt32 flags);
    void create (iterator &n, NUMemselRvalue **result, const UInt32 flags);
    void create (iterator &n, NUTernaryOp **result,    const UInt32 flags);
    void create (iterator &n, NUUnaryOp **result,      const UInt32 flags);
    void create (iterator &n, NUVarselLvalue **result, const UInt32 flags);
    void create (iterator &n, NUVarselRvalue **result, const UInt32 flags);

    //! Matcher state
    Span mFirstMatchedRange;
    Span mLastMatchedRange;
    const NUExpr *mFirstMatchedIndex;
    const NUExpr *mLastMatchedIndex;
    bool mCollapsedIntoConcat;

    Span mSpan;                         //!< cumulative lhs range
    Span mSubspan;                      //!< current lhs range

    class IntegerMap : public UtMap <SInt32, SInt32> {
    public:
      IntegerMap () : UtMap <SInt32, SInt32> () {}
      bool get (const SInt32 index, SInt32 *value) const
        {
          const_iterator it = find (index);
          if (it == end ()) {
            return false;
          } else {
            *value = it->second;
            return true;
          }
        }
      void insert (SInt32 key, SInt32 value)
        { UtMap <SInt32, SInt32>::insert (std::pair <SInt32, SInt32> (key, value)); }

      void print (UtOStream &, int indent = 0, int width = 79) const;
      void pr () const;
    };

    IntegerMap mConcreteToAbstract;
    IntegerMap mSpanBindings;

    bool getFixationOffset (const SInt32 index, SInt32 *lsb) const
    { return mSpanBindings.get (index, lsb); }

    bool fixate (Span *span, const SInt32 lsb, const NULvalue *, const NUBase *);

    // Matching within case items requires that the constant condition is kept
    // in the matcher state
    SInt32 mCaseBaseIndex;              //!< index of the first case item
    SInt32 mCaseIndex;                  //!< index of the current case item
    NUNet *mCaseSelectorId;             //!< identifier of the synthesised case selector

    // helper method

    //! returns iff the given range is in bounds for the net
    static bool isInBounds (const NUNet *, const Span &);
    static bool isInBounds (const NUNet *, const ConstantRange &);

    //! extends mSpan with the given range
    void extend (const NUStmt *, const Span &);

    //! test whether a case item is collapsible
    bool isCollapsible (const NUCaseItem *item, SInt32 *index, NUStmt **stmt) const;

    // The indices for NUMemselLvalue and NUMemselRvalue are handled
    // identically. These methods abstract out the common code.
    void build_memsel_index (const NUExpr *index, const UInt32 flags);
    bool apply_memsel_index (iterator &n, const NUExpr *index, const UInt32 flags);
    void create_memsel_index (iterator &n, const NUExpr *anchor, NUExpr **index, const UInt32 flags);

    //! Index processing for NUVarselRvalue
    void build_index (const Symbol, const NUNet *, const NUVarselRvalue *, const UInt32);
    bool apply_index (iterator &, const Symbol, const NUNet *, const NUVarselRvalue *,
      const NUVarselRvalue *, const UInt32, ConstantRange *, UInt32 *local);

    /*! test whether the selector expression of a ternary condition should be
      mapped into a vectored expression */
    bool isVectoredSel (const NUExpr * expr) const;
    bool isVectoredSel (const NUOp * op) const;

    //! map a normalised offset into the actual index of a net
    UInt32 computeIndexForNet(const NUNet * net, SInt32 offset) const;

    //! print from an iterator until the end of the instructions sequence
    void print (iterator it, bool deep, int indent) const;

  };                                    // class Matcher

  friend class Matcher;

  Matcher mMatcher;
  //!< The pattern matcher

  //! Output a human readable representation of the pattern matcher
  void pr () const;

};

#endif
