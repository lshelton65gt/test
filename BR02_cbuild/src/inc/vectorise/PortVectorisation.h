// -*-C++-*-    $Revision: 1.6 $
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _PORT_VECTORISATION_H
#define _PORT_VECTORISATION_H

// options to enable "under-construction" code
#define PORTVEC_SINGLE_BIT_VECTORS 0    //!< 1 to vectorise 1-bit vector nets

#include "util/GenericDigraph.h"
#include "util/GraphDot.h"
#include "vectorise/ConcatAnalysis.h"
#include "vectorise/ConcatVectorisation.h"
#include "reduce/OccurrenceLogger.h"

class ArgProc;                          // external declaration
class AtomicCache;                      // external declaration
class IODBNucleus;                      // external declaration
class MsgContext;                       // external declaration
class NetRefFactory;                    // external declaration
class OccurrenceLogger;                 // external declaration
class Stats;                            // external declaration
class NUModuleInstance;                 // external declaration
class ProtectedAlias;                   // external declaration

class PortCollapser;                    // forward declaration
class PortVectoriser;                   // forward declaration

/*! \file
 *
 *  Port vectorisation looks for vectorisation opportunities between whole
 *  identfier occurrences of ports and scalar nets within a module.
 *
 *  The interface to PortVectorisation is through class PortCollapser. A single
 *  PortCollapser instance is created and the the NUDesign object is passed to
 *  the instance for analysis. Analysis proceeds in XXX phases:
 *
 *  \li The setup phase initialises the data structures used during port
 *  vectorisation. In particular, mapping between actual parameters and formal
 *  parameters at module instances and a PortVectoriser instance for each
 *  module.
 *
 *  \li The discovery phase in which the netmaps are initialised with edges
 *  between potentially concat adjacent nets (\see{ConcatAnalysis.h}.
 *  
 *  After discovery, any potential vectorisation opportunities between scalar
 *  nets and ports in each module are denoted by an edge between the nets in
 *  the appropriate netmap.
 *
 *  \li Propagation of vectorisation opportunities and constraints up and down
 *  the module hierarchy until either convergence or a well-defined limit on
 *  the number of iterations is reached.
 *
 *  After propagation potential vectorisation opportunities will have walked up
 *  and down the graph, and disqualifications of opportunities will also have
 *  walked up and down the graph.
 *
 *  For example, given the module
 *
 *  module M (a, b, x);
 *    input a, b;
 *    output [1:0] x;
 *    assign       x [1] = a;
 *    assign       x [0] = b;
 *  endmodule
 *
 *  Discovery will have added the edge (a,b) to the netmap for M. If M is
 *  instantiated in N as:
 *
 *  module N (i, j, ...)
 *    input i,j;
 *    M m (i, j, ...)
 *  endmodule
 *
 *  Propagation up will add the edge (i,j) to the netmap for N because of the
 *  edge (a,b) for M.
 *
 *  Conversely, if M is instead instantiation in P as
 *
 *  module P (y,z, ...);
 *    input [1:0] y,z;
 *    M m (y [0], z [0], ...);
 *  endmodule
 * 
 *  Propagation down will scratch the edge (a,b) in M because (y[0],z[0]) do
 *  not vectorise.
 * 
 *  \li winnowing of conflicting and ambiguous vectorisations.
 *
 *  Vectorisation opportunities can be disqualified either during propagation,
 *  as the above example illustrates, or because of mutually inconsistent
 *  vectorisation opportunities between scalars. These mutually inconsistent
 *  opportunities are inferred from cycles in the graph.
 *
 *  For example, if the module L is defined as:
 *
 *  module L (a, b, x);
 *    input a, b;
 *    output [1:0] x;
 *    assign       x [0] = a;
 *    assign       x [1] = b;
 *  endmodule
 *
 *  Then there is an edge (a,b) in L. Now if both L and M are instantiated in
 *  N with actual parameters that vectorise in conflicting orders:
 *
 *  module N (...);
 *    M m (i,j);
 *    L l (i,j);
 *  endmodule
 *
 *  Then propagation up from M adds the edge (i,j), but propagation up from L
 *  adds the edge (j,i). This is a cycle in the netmap, and there is no
 *  consistent vectorisation. Winnowing deletes both i and j from the netmap.
 *
 *  Furthermore, branches in the netmap indicate ambiguous, but not illegal
 *  vectorisations.
 *
 *  \note Branching can probably only happen after cycle removal when there are
 *  unbound formal parameters. However, just because I cannot think of another
 *  example that leads to branches does not mean that there is not any.
 *
 *  \li Construction of a vectorisation scheme for each module from the net map
 *  encapsulated in the appropriate PortVectoriser instance.
 *
 *  \li Application of the vectorisation scheme to the module to replace sets of
 *  scalar ports and locals with vector nets, and uses of these nets with
 *  bitsels of the new vector.
 *
 *  \note Investigate whether it makes sense to replaced the tandem maps used
 *  for the actual and formal parameters with a single graph.
 *
 */

//! \class PortVectoriser
/*! For each module, a PortVectoriser instance is created. Each instance
 *  encapsulates:
 *
 *  \li A ConcatAnalysis::NetMap instance that is initialised to contains
 *  edges between nets that denote vectorisation opportunities.
 *
 *  \li A  ConcatAnalysis::Vectorisation instance that constructs and yields
 *  the vectorisation scheme for the module after the convergent netmap has
 *  been generated.
 *  
 */

class PortVectoriser {
public:

  class Options;                        // forward declaration

  PortVectoriser (
    PortCollapser *collapser,
    AtomicCache *strCache,
    IODBNucleus *iodb, 
    Stats *stats, 
    MsgContext *msgContext,
    NUNetRefFactory *netRefFactory,
    ProtectedAlias &protect,
    NUModule *,
    const Options &);

  ~PortVectoriser () {}

  // options
  enum {
#define PORTVEC_OPTION(_tag_, _bit_) c##_tag_ = (1<<_bit_),
#define PORTVEC_OPTION_MASK(_tag_, _mask_) c##_tag_ = (_mask_),
#include "vectorise/PortVectorisation.def"
    cALL = 0xFFFFFFFF
  };

  //! Comparison operator for PortVectoriser instances
  struct Cmp {
    bool operator () (const PortVectoriser *a, const PortVectoriser *b) const
    { return NUModule::compare (a->mModule, b->mModule) < 0;    }
  };

  //! Aggregation of PortVectoriser instances
  typedef UtMap <NUModule *, PortVectoriser *, NUModuleCmp> Map;

  //! Map that owns the port vectoriser instances
  class ManagedMap : public Map {
  public:

    ManagedMap () : Map () {}

    virtual ~ManagedMap ()
    {
      for (iterator it = begin (); it != end (); it++) {
        delete it->second;
      }
    }

  private:

    // forbidden
    ManagedMap (const ManagedMap &);
    ManagedMap &operator = (const ManagedMap &);

  };

  //! Add a pr method to the standard set.
  class Set : public UtSet <PortVectoriser *, Cmp> {
  public:

    Set () : UtSet <PortVectoriser *, Cmp> () {}

    // construct a human readable representation
    void compose (UtString *) const;

    // diagnostic output
    void pr () const;

  };

  typedef UtList <PortVectoriser *> List;

  //! Possibly add an edge to the netmap.
  /*! The edge is added if no such edge exists, neither of the end points has
   *  is scratched out and no edge from a to b has been scratched.
   *  \return iff a new edge was added.
   */
  bool maybeAddEdge (NUNet *a, NUNet *b, ConcatAnalysis::NetMap::Edge **edge)
  { return mMap.maybeAddEdge (a, b, edge); }

  //! Encapsulate option processing for port vectorisation
  class Options {
  public:

    Options (ArgProc *args, MsgContext *msg_context, const char *file_root) : 
      mFileRoot (file_root), mOptions (0), mArgs (0), mMaxIterations (0), 
      mReportFile (NULL), mSummaryFile (NULL), mMinCluserSize (cMIN_CLUSTER_SIZE), mSuffix (NULL), 
      mReportFileName (NULL)
    { parse (args, msg_context); }

    ~Options ();

    //! Set the options mask from the command line
    void parse (ArgProc *, MsgContext *);

    //! Test option bits
    /*! \return iff all of the options bits in mask are set in this */
    bool operator & (const UInt32 mask) const { return (mOptions & mask) == mask; }

    /*! \return iff any of the options bits in mask are set in this */
    bool operator | (const UInt32 mask) const { return (mOptions & mask) != 0; }

    //! \return the report file stream or NULL if no report is required
    UtOStream *getReportFile () const { return mReportFile; }

    //! \return the summary file stream or NULL if no summary is required
    UtOStream *getSummaryFile () const { return mSummaryFile; }

    //! \return the minimum vectorisable cluster size
    UInt32 getMinClusterSize () const { return mMinCluserSize; }

    //! \return -portVecMaxIterations
    UInt32 maxIterations () const { return mMaxIterations; }

    //! Construct a human readable representation
    void compose (UtString *) const;

    //! Diagnostic output.
    void pr () const;

    //! Construct a filename suffix based on the option bits.
    /*! \note This is usefull for generating log and trace files in which the
     *  filename encodes some of the options.
     */
    bool makeFilename (const char *prefix, const char *suffix, char *, const size_t) const;

    //! map a string into the option mask.
    /*! \note This is used during development to implement the -portVec option.
     */
    static bool cvt (const char *, UInt32 *options);

  private:

    // forbidden
    Options (const Options &);
    Options &operator = (const Options &);

    const char *mFileRoot;              //!< file root from CarbonContext
    UInt32 mOptions;                    //!< the option bitmap
    UInt32 mArgs;                       //!< the argument bitmap
    UInt32 mMaxIterations;              //!< value of -portVecMaxIterations
    UtOStream *mReportFile;             //!< the report file stream
    UtOStream *mSummaryFile;            //!< the summary file stream
    UInt32 mMinCluserSize;              //!< minimum vectorisable cluster size
    const char *mSuffix;                //!< additional suffix for report files
    const char *mReportFileName;        //!< explicitly requested report filename

    enum {
      cDEFAULT_MIN_CLUSTER_SIZE = 8
    };

    enum {
      cREPORT_FILE = 1<<0,              //!< Set in mArgs when a report file is created
      cREPORT_STDOUT = 1<<1,            //!< Set in mArgs when reporting to stdout
      cMIN_CLUSTER_SIZE = 1<<2,         //!< Set when -portVecMinCluster is encountered
      cSUFFIX = 1<<3                    //!< addition suffix for report files
    };

  };

  //! \return the module this is vectorising
  NUModule *getModule () const { return mModule; }

  //! look for potential port vectorisations in the module
  bool discovery ();

  //! propagate opportunities and constraints up and down the module hierarchy
  bool propagate ();

  //! scratch any formal parameters that can never be vectorised
  void winnowNeverVectorisableFormals ();

  //! Derive constraints from the internal structure of the graph
  void structural_winnowing ();

  //! Propagate constraints to parents and children in the module hierarchy
  bool propagate (PortVectoriser::Set *workset);

  //! Construct the vectorisation plan
  bool construct () 
  { 
    const UInt32 options = (mOptions & cVECTORISE_PRIMARY) ? 
      ConcatVectorisation::cVECTORISE_PRIMARY : 0;
    return mVectorisation.construct (mMap, mOptions.getMinClusterSize (), options);
  }

  //! Apply the vectorisation plan to the module body
  bool apply ();

  //! Apply the vectorisation plan to module instantiations
  bool applyToInstances ();

  //! Open the port vectorisation log file building the filename from root
  static void openOccurrenceLog (MsgContext &, const char *root, const char *ident);

  //! Close the concat log file
  static void closeOccurrenceLog ();

  //! dump the verbose information
  void verbose ();

  //! write the vectorisation report to a stream
  void report (UtOStream &s, const UInt32 flags = ConcatVectorisation::cDEFAULT) const 
  { mVectorisation.report (s, flags); }

  enum OccurrenceKeys {
#define PORTVEC_OCCURRENCE(_tag_) c##_tag_,
#include "vectorise/PortVectorisation.def"
    cNKeys
  };

  //! \return the netmap for the module
  const ConcatAnalysis::NetMap &getMap () const { return mMap; }

  operator ConcatAnalysis & () { return mBitNetAnalysis; }

  const char *image () const { return mModule->getName ()->str (); }

  //! options for compose
  enum {
    cINDENT_MASK = 0xFF,                //!< bottom eight bits are indent
    cNETMAP = 1<< 8,                    //!< include the netmap
    cFORMAL_MAP = 1<<9,                 //!< include the formal parameter map
    cACTUAL_MAP = 1<<10,                 //!< include the actual parameter map
    cPARAM_MAPS = cFORMAL_MAP|cACTUAL_MAP,
    cNO_HEADING = 1<<11                 //!< do not display the heading
  };

  //! construct a human readable string
  void compose (UtString *, const UInt32 options = cNETMAP) const;

private:

  // prevent assignment and copying
  PortVectoriser (const PortVectoriser &);
  PortVectoriser &operator = (const PortVectoriser &);

  const Options &mOptions;              //!< inherited options
  PortCollapser *mCollapser;            //!< the parent collapser object
  AtomicCache *mStrCache;               //!< string cache (external)
  IODBNucleus *mIODB;                   //!< IODB (external)
  Stats *mStats;                        //!< performance statistics (external)
  MsgContext *mMsgContext;              //!< message context (external)
  NUNetRefFactory *mNetRefFactory;      //!< not quite unnecessary
  ProtectedAlias &mProtect;             //!< pessimistic approximation of aliases
  NUModule *mModule;                    //!< module under consideration
  ConcatAnalysis::NetMap mMap;          //!< port dependency map

  static OccurrenceLogger *sLog;        //!< port vectorisation occurrence logger

  //! \class BitnetConcatAnalysis
  /*! Specialise concat analysis looking for scalar ports and nets
  */

  class BitnetConcatAnalysis : public ConcatAnalysis {
  public:

    BitnetConcatAnalysis (AtomicCache *str_cache,
      IODBNucleus *iodb, 
      MsgContext *msg_context, 
      NUNetRefFactory *net_ref_factory,
      ProtectedAlias &protect,
      NUModule *module,
      const PortVectoriser::Options &options) : 
      ConcatAnalysis (str_cache, iodb, msg_context, net_ref_factory, protect, module), mOptions (options)
    {}

    virtual ~BitnetConcatAnalysis () {}

    //! Implementation of superclass interface
    /*! Consider single bit, non-protected nets as interesting */
    virtual bool isInteresting (NUNet *net);

    //! Implementation of superclass interface
    virtual UInt32 classifyNet (NUNet *net);
    
  private:

    // forbidden
    BitnetConcatAnalysis (const BitnetConcatAnalysis &);
    BitnetConcatAnalysis &operator = (const BitnetConcatAnalysis &);

    const PortVectoriser::Options &mOptions; //! inherited options

    // diagnostic callbacks from ConcatAnalysis
    virtual void note_extract (const NUAssign *, const NUAssign *, const UInt32, 
      ConcatAnalysis::NetMap &) const;
    virtual void note_cycles (GraphNodeSet &) const;
    virtual void note_forward_pruning (ConcatAnalysis::NetMap::Edge::Set &) const;
    virtual void note_backward_pruning (ConcatAnalysis::NetMap::Edge::Set &) const;
    virtual void note_vectorisation (NUStmtList::iterator, const UInt32 length) const;
    virtual void note_vector_edge (ConcatAnalysis::NetMap::Edge *) const;
    virtual void note_apply (NUModule *) const;
    virtual void note_apply (NUModuleInstance *) const;

  };

  //! Object for concat analysis of the scalar nets and ports of this module
  BitnetConcatAnalysis mBitNetAnalysis;
  
  //! The vectorisation plan constructed from the final results of the analysis
  ConcatVectorisation mVectorisation;

  // Vectorisation opportunities are discovered in the statements of a module
  // and are then propagated up and down the module hierarchy. The port
  // vectoriser constructs a representations of all the interesting parameter
  // bindings.
  //
  // Class Binding is the abstract base class for a hierarchy of classes that
  // denote binding from a formal to an actual.
  //
  // Class FormalMap implements a mapping from the formal parameters of a
  // module to the actual parameters in a parent for all the interesting formal
  // parameters of the module.
  //
  // Class ActualMap reverses the formal map. That is, it maps from all the
  // scalar actual parameters to their binding to an interesting formal
  // parameter in a child. Note that this is a multimap because the same actual
  // may appear multiple times in a single module instance.

  //! \class Binding
  /*! Base class for representing a (formal,actual) binding in a module instance */

  class Binding : public OccurrenceLogger::Visible {
  public:

    Binding (NUNet *formal) : mFormal (formal) {}
    virtual ~Binding () {}

    typedef UtMap <NUNet *, Binding *, NUNetCmp> Map;
    typedef UtMultiMap <NUNet *, Binding *, NUNetCmp> MultiMap;

    //! \return the direction of the parameter
    virtual PortDirectionT getDirection () const = 0;
    
    //! \return iff the parameter is a single bit
    virtual bool isScalar () const = 0;

    //! \return iff this parameter vectorises adjacent to another parameter
    virtual bool vectorisesWith (const PortVectoriser &, const Binding *) const = 0;

    //! \return the actual parameter
    //! \pre isScalar
    virtual NUNet *getActual () const = 0;

    //! \return the formal parameter
    NUNet *getFormal () const { return mFormal; }

    //! Construct a human readable representation
    virtual void compose (UtString *s, const UInt32 indent) const = 0;

    //! Diagnostic output
    void pr () const;

  protected:

    // forbidden
    Binding (const Binding &);
    Binding &operator = (const Binding &);

    NUNet *mFormal;                     //! formal parameter

  };

  //! \class IdentBinding
  /*! Binding of a formal parameter to a scalar actual parameter. The direction
   *  and nucleus type of the actual are template arguments.
   */

  template <PortDirectionT _dir, class _IdentType>
  class IdentBinding : public Binding {
  public:
    IdentBinding (NUNet *formal, _IdentType *actual) :
      Binding (formal), mActual (actual), mActualNet (actual->getIdent ()) {}

    //! \return the direction
    virtual PortDirectionT getDirection () const { return _dir; }
    
    //! These are the scalar actual parameters
    virtual bool isScalar () const { return true; }

    //! \return the actual parameter
    virtual NUNet *getActual () const { return mActualNet; }

    //! Test whether this identifier vectorises with another.
    /*! The test is implemented within the PortVectoriser by examining the
     *  netmap.
     */
    virtual bool vectorisesWith (const PortVectoriser &context, const Binding *other) const
    {
      const IdentBinding *ident = dynamic_cast <const IdentBinding *> (other);
      if (ident == NULL) {
        return false;
      } else {
        return context.test (mActual, ident->mActual);
      }
    }

    //! Construct a human readable output
    virtual void compose (UtString *s, const UInt32 indent) const;

  private:

    _IdentType *mActual;                //!< the scalar actual parameter
    NUNet *mActualNet;                  //!< the whole identifier net

  };

  //! \class VarselBinding
  /*! Binding of a formal parameter to a varsel actual parameter. The direction
   *  and nucleus type of the actual are template arguments.
   */
  template <PortDirectionT _dir, class _VarselType>
  class VarselBinding : public Binding {
  public:

    VarselBinding (NUNet *formal, _VarselType *actual) :
      Binding (formal), mActual (actual) {}

    //! \return the direction
    virtual PortDirectionT getDirection () const { return _dir; }

    //! These are not scalars
    virtual bool isScalar () const { return false; }

    //! Varsel bindings are not whole identifiers so this method always asserts out
    virtual NUNet *getActual () const { NU_ASSERT (isScalar (), mActual); return NULL; }

    //! Test whether this identifier vectorises with another.
    /*! The test is implemented within the PortVectoriser */
    virtual bool vectorisesWith (const PortVectoriser &context, const Binding *other) const
    {
      const VarselBinding *varsel = dynamic_cast <const VarselBinding *> (other);
      if (varsel == NULL) {
        return false;
      } else {
        return context.test (mActual, varsel->mActual);
      }
    }

    //! Construct a human readable output
    virtual void compose (UtString *s, const UInt32 indent) const;

  private:
    _VarselType *mActual;
  };

  //! \return iff a pair of bit select expressions vectorise together
  bool test (const NUVarselRvalue *a, const NUVarselRvalue *b) const;

  //! \return iff a pair of bit select lvalues vectorise together
  bool test (const NUVarselLvalue *a, const NUVarselLvalue *b) const;

  //! \return iff a pair of scalar nets vectorise in this module
  bool test (const NUIdentRvalue *a, const NUIdentRvalue *b) const;

  //! \return iff a pair of scalar nets vectorise in this module
  bool test (const NUIdentLvalue *a, const NUIdentLvalue *b) const;

public:

  //! \class FormalMap
  /*! Maps formal parameters to actual parameter bindings in a module instance */
  class FormalMap : public Binding::Map {
  public:

    FormalMap (ConcatAnalysis &, NUModuleInstance *, bool verbose,
      PortVectoriser *parent, PortVectoriser *child);

    virtual ~FormalMap ();

    typedef UtList <FormalMap *> List;

    //! Adds cleanup of the list elements to the basic list.
    class ManagedList : public List {
    public:

      ManagedList () : List () {}

      virtual ~ManagedList ()
      {
        for (iterator it = begin (); it != end (); it++) {
          delete *it;
        }
      }

    private:

      ManagedList (const ManagedList &);
      ManagedList &operator = (const ManagedList &);

    };

    //! Adds diagnostic output to the basic multimap
    class MultiMap : public UtMultiMap <PortVectoriser *, FormalMap *> {
    public:

      MultiMap () : UtMultiMap <PortVectoriser *, FormalMap *> () {}

      //! construct a human readable representation
      void compose (UtString *, const UInt32 options) const;

      //! Diagnostic output
      void pr () const;

    };

    //! \return the parent vectoriser instance
    PortVectoriser *getParent () const { return mParent; }

    //! \return the child vectoriser instance
    PortVectoriser *getChild () const { return mChild; }

    //! Add the mapping from a formal parameter to the binding
    void add (Binding *binding) { insert (value_type (binding->getFormal (), binding)); }

    //! \return the module instance that induced this mapping
    NUModuleInstance *getInstance () const { return mInstance; }

    //! Propagate from an edge in module to an edge in the module
    /*! \note The edge connects ports or local nets in module.
     */
    bool propagate (PortVectoriser *parent, PortVectoriser *module, 
      ConcatAnalysis::NetMap::Edge *edge, bool *scratch_edge,
      UInt32 *count, UInt32 *delta);

    //! Look for the binding of a formal parameter
    bool lookup (NUNet *net, Binding **);

    //! \return source location of the module instance
    const SourceLocator &getLoc () const;

    //! Construct a human readable output
    void compose (UtString *, const UInt32 options) const;

    //! Diagnostic output
    void pr () const;

  private:

    NUModuleInstance *mInstance;        //!< the inducing module instance
    const bool mVerbose;                //!< noisy or nice
    PortVectoriser *mParent;            //!< parent module vectoriser
    PortVectoriser *mChild;             //!< child module vectoriser

  };

  //! \class ActualMap
  /*! The ActualMap is used to propagate vectorisation opportunities down the
   *  module hierarchy. The netmap in a module will contain edges between
   *  scalar nets that have been identified as potentially vectorisable
   *  together. The ActualMap for a module instance maps the scalar nets of the
   *  module to their occurrences as actual parameters in the module
   *  instance. If there is an edge between two scalar nets, and both occur as
   *  actual parameters in the module instance, then an edge between the
   *  corresponding forms is pushed down the hierarchy. A net can occur
   *  multiple times in a single instantiation, so the mapping is a multi map.
   */

  class ActualMap : public Binding::MultiMap {
  public:

    ActualMap (const FormalMap &, bool verbose,
      PortVectoriser *parent, PortVectoriser *child);

    // The bindings references in the actual map are owned and managed in a
    // formal map so the ActualMap destructor does not need to free storage.
    virtual ~ActualMap () {}

    typedef UtList <ActualMap *> List;

    //! Add storage management to the basic list
    class ManagedList : public List {
    public:

      ManagedList () : List () {}

      virtual ~ManagedList ()
      {
        for (iterator it = begin (); it != end (); it++) {
          delete *it;
        }
      }

    private:

      // forbidded
      ManagedList (const ManagedList &);
      ManagedList &operator = (const ManagedList &);

    };

    //! Add diagnostic output to the basic multimap
    class MultiMap : public UtMultiMap <PortVectoriser *, ActualMap *> {
    public:

      MultiMap () : UtMultiMap <PortVectoriser *, ActualMap *> () {}

      //! construct a human readable representation
      void compose (UtString *, const UInt32 options) const;

      //! Diagnostic output
      void pr () const;

    };

    //! \return the parent vectoriser instance
    PortVectoriser *getParent () const { return mParent; }

    //! \return the child vectoriser instance
    PortVectoriser *getChild () const { return mChild; }

    //! Add the mapping from an actual to the binding to this map
    void add (Binding *binding) { insert (value_type (binding->getActual (), binding)); }

    //! \return the module instance that induced this mapping
    NUModuleInstance *getInstance () const { return mInstance; }

    //! Propagate from an edge in the parent module to an edge in module
    /*! \note The edge connects ports or local nets in parent
     */
    bool propagate (PortVectoriser *module, PortVectoriser *parent, 
      ConcatAnalysis::NetMap::Edge *edge, bool *scratch_edge,
      UInt32 *count, UInt32 *delta);

    bool lookup (NUNet *net, iterator &begin, iterator &end);

    //! \return source location of the module instance
    const SourceLocator &getLoc () const;

    //! Construct a human readable output
    void compose (UtString *, const UInt32 indent) const;

    //! Diagnostic output
    void pr () const;

  private:

    NUModuleInstance *mInstance;        //!< inducing module instance
    const bool mVerbose;                //!< noisy or nice
    PortVectoriser *mParent;            //!< parent module vectoriser
    PortVectoriser *mChild;             //!< child module vectoriser
  };

  //! \class ActualIndex
  /*! Provides an index into the actual maps so that the outer loop of
   *  propagation down can be over the netmap edges in the module
   *
   *  \note Constructing this index allows propagation to be driven by
   *  iterating over the edges in the netmap of a module and immediately
   *  identifying the ActualMaps that include the source of the edge as an
   *  actual parameter. Without this, propagation can blow up on modules that
   *  induce large netmaps.
   */

  class ActualIndex : public UtMultiMap <NUNet *, ActualMap *> {
  public:
    
    ActualIndex () {}

    typedef std::pair <iterator, iterator> Range;

    //! Add another map to the index
    void acrete (ActualMap *map)
    {
      for (ActualMap::iterator it = map->begin (); it != map->end (); it++) {
        insert (std::pair <NUNet *, ActualMap *> (it->first, map));
      }
    }

  };

  //! \class FormalIndex
  /*! Provides an index into the formal maps so that the outer loop of
   *  propagation can be over the netmap edges in the module.
   *
   *  \note This index could be dispensed with currently because we winnow out
   *  formals that are missing in at least one instance of the module. However,
   *  it is included for future work.
   *
   *  TODO: if a formal parameter is unbound in a module instance, avoid
   *  winnowing the formal from each and every FormalMap instance for that
   *  module.
   */

  class FormalIndex : public UtMultiMap <NUNet *, FormalMap *> {
  public:

    FormalIndex () {}

    typedef std::pair <iterator, iterator> Range;

    //! Add another map to the index
    void acrete (FormalMap *map)
    {
      for (FormalMap::iterator it = map->begin (); it != map->end (); it++) {
        insert (std::pair <NUNet *, FormalMap *> (it->first, map));
      }
    }

  };

  //! Add the formal paramter mapping for an instance to the parent of the instance
  void addParent (PortVectoriser *parent, FormalMap *map);

  //! Add the actual parameter mapping for an instance to the child of the instance
  void addChild (PortVectoriser *child, ActualMap *map);

private:

  ActualMap::MultiMap mChildren;        //!< instances contained in this module
  FormalMap::MultiMap mParents;         //!< instances of this module
  ActualIndex mActualIndex;             //!< index into the actual parameter bindings
  FormalIndex mFormalIndex;             //!< index into the formal parameter bindings

public:

  //! Run sanity checks on the modified module
  bool isSane () const;

};

/*! \class PortCollapser
 *  \brief Port vectorisation context for the entire design.
 */

class PortCollapser {

public:

  PortCollapser (
    AtomicCache *strCache,              // all the...
    IODBNucleus *iodb,                  // ... usual stuff
    Stats *stats,                       // ... that gets passed
    MsgContext *msgContext,             // ... types
    NUNetRefFactory *netRefFactory,     // 
    bool phaseStats,
    PortVectoriser::Options &options);  // configuration

  ~PortCollapser ();

  void design (NUDesign *);

private:

  AtomicCache *mStrCache;               // all the...
  IODBNucleus *mIODB;                   // ... usual stuff
  Stats *mStats;                        // ... passed
  MsgContext *mMsgContext;              // ... down to these
  NUNetRefFactory *mNetRefFactory;      // ... types of
  bool mPhaseStats;                     // ... objects
  PortVectoriser::Options &mOptions;    //!< options vector

  //! perform the discovery phase of port vectorisation over the entire design
  bool discovery (PortVectoriser::List &);

  //! perform the construction phase of port vectorisation
  void construct (PortVectoriser::Map &, PortVectoriser::Set *);

  // keep the parameter list to be freed by their destructors
  PortVectoriser::FormalMap::ManagedList mFormals; //!< list of formal parameter mappings
  PortVectoriser::ActualMap::ManagedList mActuals; //!< list of actual parameter mappings

  //! Build the formal-to-binding and actual-to-binding maps
  void build_parameter_maps (NUDesign *, PortVectoriser::Map &);

  //! apply the port vectorisation plan
  void apply (PortVectoriser::Map &pvmap, PortVectoriser::Set &);

  //! Run sanity checks on the modified design
  bool isSane (const PortVectoriser::List &) const;

};

#endif
