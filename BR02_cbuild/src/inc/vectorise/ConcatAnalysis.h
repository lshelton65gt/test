// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _CONCAT_ANALYSIS_H
#define _CONCAT_ANALYSIS_H

#include "util/Graph.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUModule.h"
#include "nucleus/NuToNu.h"
#include "reduce/AllocAlias.h"

/*! \class ConcatAnalysis 
 *
 *  Concat analysis looks for vectorisation opportunities gleaned from
 *  occurrences of whole identifiers in module definitions.
 *
 *  Define a pair of nets to be <b> concat adjacent </b> in a statement if
 *  there is a concat in the statement in which the nets appear as whole
 *  identfiers in consecutive positions in the concat.
 *
 *  Define a pair of nets to be <b> potentially concat adjacent </b> in a set
 *  of statements if the nets are either cocat adjacent or there is a
 *  vectorisation of the statements in which the nets are concat adjacent.
 *
 *  The concat analysis class contains methods and data structures that
 *  construct a directed graph to represent potential concat adjacency in a
 *  module.  Potential concat adjacency is represented in a graph in which the
 *  nodes of the graph denote nets and an edge from a to be indicates that a
 *  and b are potentially concat adjacent and that a is in the more significant
 *  bit position of the concat than b.
 * 
 *  When a node is created, it is assigned an integer classification by calling
 *  the classifyNet method of the ConcatAnalysis object associated with the
 *  graph. This is an abstract virtual method of ConcatAnalysis and must be
 *  implemented in a subclass. The special classification cSCRATCHED is
 *  reservered by the NetMap implementation and must not be returned by
 *  classifyNet. This classification is applied to a node when a client calls
 *  maybeScratchNet on the appropriate net.
 *
 *  For example, BitnetConcatAnalysis in PortVectorisation classifies nets with
 *  the least significant nine bits of the netflags. This precludes edges
 *  between disparate nets.
 *
 *  The ConcatAnalysis::discovery method analyses the module to create the
 *  edges between the potentially concat adjacent nodes. It uses the collapser
 *  to infer potential adjacency. For statements that are not matched by the
 *  collapser it directly scans concat expressions and lvalues in statements.
 *
 *  Discovery will only add an edge between two nets if:
 * 
 *  \li isInteresting returns two for both nets,
 *  \li classifyNet returns the same classification for each net, 
 *  \li Neither of the two nets has been scratched from the graph,
 *  \li The edge has not been sratched from the graph.
 *
 *  Subclasses must implement isInteresting and classifyNet.
 *
 *  Edges are weighted with an integer value. When a node is initially created
 *  it is assigned weight one. When maybeAddEdge is called for a pair of nets
 *  with a extant edge, the weight is increased. Currently, this weighting is
 *  used only for choosing between ambiguous vectorisations induced by
 *  branching in the netmap. Future development may use this weight to break
 *  conflicting vectorisations induced by cycles in the netmap.
 *
 *  For example, given the pair of assignments:
 *
 *  a = i & x[0];
 *  b = j & x[1];
 *
 *  The collapser recognises that this can be vectorised as {b,a} = {j,i} & x.
 *  Discovery will add edges (b,a) and (j,i) are added to the graph if all the
 *  context-dependent conditions defined by the ConcatAnalysis subclass are
 *  satisfied.
 *
 *  Client code may modify the netmaps by the following methods:
 *
 *  \li scratchEdge removes an edge from the graph and prevents maybeAddEdge
 *  from adding the same edge in the future.
 *
 *  \li maybeScratchNet removes all edges to and from a net and prevents
 *  maybeAddEdge from adding any more edges to or from that net.
 *
 *  \li scratch_cycles calls maybeScratchNet for each node that is part of a
 *  cycle. After calling scratch_cycles, the graph is acyclic.
 *
 *  \li remove_cycles removes all nodes that are part of a cycle from the
 *  graph.
 *
 *  \li remove_branches looks for branches in an acyclic graph and removes
 *  edges to eliminate all branches.
 *
 *  \li maybeAddEdge adds an edge between a pair of nodes if they have the same
 *  classification, are both deemed interesting by the ConcatAnalysis instance,
 *  neither node is scratched and the node is not scratched.
 *
 *  Once a client of ConcatAnalysis has finished processing a net map, a
 *  vectorisation scheme can be extracted into a ConcatAnalysis::Vectorisation
 *  instance. 
 *
 *  \note Currently, the vectorisation scheme can be examined with
 *  Vectorisation::verbose or Vectorisation::report. The next phase of 
 *  implementation will apply the scheme to a module.
 *
 */

/* TODO - improve cycle removal in the generated net graphs. Currently every
 * node that is part of a cycle is removed. This is much too big a hammer.
 */

class ProtectedAlias;                   // forward declaraion

class ConcatAnalysis {
public:

  // forward declarations
  class NetMap;

  ConcatAnalysis (
    AtomicCache *str_cache,
    IODBNucleus *iodb, 
    MsgContext *msg_context, 
    NUNetRefFactory *net_ref_factory,
    ProtectedAlias &protect,
    NUModule *module) : 
    mStrCache (str_cache),
    mIODB (iodb),
    mMsgContext (msg_context), 
    mNetRefFactory (net_ref_factory),
    mProtect (protect),
    mModule (module)
  {}

  virtual ~ConcatAnalysis () {}

  //! construct the graph denoting adjacency in concats
  /*! The module that was passed into the constructor is analysed looking for
   *  concat both in extant nucleus and in potential vectorisations of the
   *  nucleus. For each net a, b that are adjacent in such a concat, an edge from
   *  a to b is added to graph. The graph is then checked for cycles and every node
   *  that is part of a cycle is removed from the graph.
   *  \return !graph->empty ()
   */
  bool discovery (NetMap *graph);

  //! construct the adjacency net map for a list of statements.
  bool discovery (NUScope *scope, NUStmtList &stmts, NetMap *netmap);

  //! \enum Classification
  /*! Subclasses must define the classifyNet abstract virtual method. This
   *  method returns an integer classification of the net. NetMap::maybeAddEdge
   *  will not create edges between nets with a different classification.
   *
   *  For example, the port and local net specialisation of ConcatAnalysis
   *  realised by PortVectorisation::BitnetConcatAnalysis uses the bottom nine
   *  bits of the net flags as the classification. Thus, maybeAddEdge will only
   *  create edges between like nets.
   *
   *  The distinguished classification cSCRATCH must never be returned by the
   *  classification method in the subclass. It is used within ConcatAnalysis
   *  to denote nets that are removed from further consideration by
   *  maybeScratchNet.
   */

  enum Classification {
    cSCRATCHED = 0xFFFFFFFF
  };

  //! \return the classification of a net
  /*! The subclass must implement the classification method */
  virtual UInt32 classifyNet (NUNet *net) = 0;

  //! \return whether or not the net is considered of interest.
  /*! The analysis only considers "interesting" nets. That is, adjacent nets
   *  are added to the graph only if they are both interesting. Subclasses of
   *  ConcatAnalysis must define the class of interesting nets by defining the
   *  isInteresting predicate. For example, port vectorisation only considered
   *  single-bit ports as interesting. 
   */
  virtual bool isInteresting (NUNet *net) = 0;

  enum NetFlagging {
    //! Mask for the net type... these bits must match
    cNET_TYPE = 0x000f,
    //! Mask for attributes that are prevent vectorisation
    cNEVER_VECTORISE =
      e2DNetMask|eInClkPath|eInDataPath|eEdgeTrigger|eForcibleNet|eDepositNet|eReset|eSigned|eNonStaticNet,
    //! Mask of bits that must match
    cMUST_MATCH = eTriWritten|ePullUp|ePullDown|eRecordPort|eConstZ|eClearAtEnd,
    //! Testing for the match also uses the type and declaration bits.
    cMATCH_MASK = cNET_TYPE|eDeclareMask|cMUST_MATCH,
    //! Mask of bits that are set by or'ing the bits of the scalars
    cDISJUNCTION = eInaccurateNet|eReadNet|eWrittenNet,
    //! Mask of bits that are always set in the vector net
    cSET_IN_VECTOR = eAllocatedNet|eTempNet,
    //! Mask of bits that are always reset in the vector net
    cRESET_IN_VECTOR = eAliasedNet|eBlockLocalNet|eDeadNet|eRecordPort,
    //! Mask of bits that are asserted to be zero (debug builds only)
    cASSERT_ZERO = eInClkPath|eInDataPath,
    //! Mask of bits that are asserted to be one (debug builds only)
    cASSERT_ONE = eAllocatedNet
  };

  //! \return whether a pair of nets with the given flags are vectorisable
  /*! The subclass must decide whether a pair of nets can be vectorised. Note
   *  that the netflags of the new net are constructed by or'ing together the
   *  netflags of all the scalar nets.
   */

  static bool isVectorisable (const NetFlags a, const NetFlags b)
  { 
    return (a & cMATCH_MASK) == (b & cMATCH_MASK);
  }

  static UInt32 combineFlags (const UInt32 a, const UInt32 b)
  {
    return ((a & cMATCH_MASK) | cSET_IN_VECTOR | (a & cDISJUNCTION) | (b & cDISJUNCTION)) 
      & ~cRESET_IN_VECTOR;
  }

  //! return the module under analysis
  NUModule *getModule () const { return mModule; }

  //! return the string cache
  AtomicCache *getStrCache () const { return mStrCache; }

  //! return the message context
  MsgContext *getMsgContext () const { return mMsgContext; }

  //! return the net ref factory
  NUNetRefFactory *getNetRefFactory () const { return mNetRefFactory; }

  /*! \class NetMap 
   *  \brief A weighted, directed graph type where nodes correspond to NUNet instances.
   */

  class NetMap : public Graph {
  public:

    NetMap (ConcatAnalysis &context) : Graph (), mNodes (), mConcatAnalysis (context){}
    virtual ~NetMap ();

    class Node;                           // forward declaration
    class Edge;                           // forward declaration

    typedef UtSet <GraphEdge *> GraphEdgeSet;

    //! Possibly add an edge to this graph.
    /*! The edge is added if no such edge exists, neither of the end points has
     *  is scratched out and no edge from a to b has been scratched, and each
     *  net has the same classification as computed by mConcatAnalysis.classifyNet.
     *  \return iff a new edge was added. 
     */
    bool maybeAddEdge (NUNet *, NUNet *, Edge **);

    //! Possibly scratch a node from the net.
    /*! The node is scratched if it has not previously been scratched out.
     *  \return whether or not the net had to be scratched 
     *  \note If the net is not in the graph, then a new node is created and
     *  immediately scratched out.
     */
    bool maybeScratchNet (NUNet *);

    //! Remove an edge and mark it scratched
    void scratchEdge (Edge *);

    //! Remove the cycles from a net map.
    /*! Nodes in the cycle are deleted from the graph 
     *  \note Cycles in the netmap indicate mutually inconsistent vectorisation
     *  opportunities.
     */
    bool remove_cycles (const ConcatAnalysis &);
    
    //! Remove cycles from the graph by scratching out node.
    /*! All nodes in all cycles are scratched */
    bool scratch_cycles (const ConcatAnalysis &);

    //! remove the branches from a net map
    /*! \note Branches in the netmap indicate ambiguous vectorisation
      opportunities.
    */
    bool remove_branches ();

    //! \return iff the graph contains no nodes
    bool empty () const { return mNodes.empty (); }

    //! Locate a node in the graph.
    /*! \param net The net represented by the desired node.
     *  \return The node corresponding to a net.
     *  \note If there is no node corresponding to the net, then NULL is returned.
     */
    Node *findNode (NUNet *net) const;

    //! test for an edge between two nets
    bool hasEdge (NUNet *a, NUNet *b) const;

    /*! \class Edge
     *  \brief Weighted edges connecting NetMap nodes.
     */

    class Edge : public GraphEdge {
    public:

      friend class NetMap;
      friend class Node;

      typedef UtSet <Edge *> Set;

      //! \return the weight of this edge.
      SInt32 getWeight () const { return mWeight; }

      //! Increment the weight of this edge.
      SInt32 operator ++ () { mWeight++; return mWeight; }

      //! \return the source node of this directed edge.
      Node *getFrom () const { return mFrom; }

      //! \return the sink edge of this directed edge.
      Node *getTo () const { return mTo; }

      //! Construct a string describing this edge
      void compose (UtString *) const;

      //! Diagnostic output
      void pr () const;

      typedef UtMap <NUNet *, Edge *, NUNetCmp> Map;

    private:

      // Only let our friends create and destroy edges
      Edge (Node *from, Node *to) : 
        GraphEdge (), mFlags (0), mScratch (0), mFrom (from), mTo (to), mWeight (1) 
      {}

      virtual ~Edge ();

      UInt32 mFlags;                      //!< Flags required by the superclass
      UIntPtr mScratch;                   //!< Scratch storage required by the superclass
      Node *mFrom, *mTo;                  //!< End points of this edge
      SInt32 mWeight;                     //!< Weight of this edge

      //! Remove references to the edge in the graph
      void snip ();

    };

    /*! \class Node
     *  \brief Nodes of NetMap encapsulate NUNet instances.
     *
     *  \note Each node is given an integer classification by the classifyNet
     *  abstract virtual method of a ConcatAnalysis instance. maybeAddEdge will
     *  not create edges between nodes with a different classification.
     *
     *  \note Nodes can be scratched from further consideration by calling
     *  NetMap::scratchNodes. maybeAddEdge will not create an edge into or out
     *  of a scratched node.
     */

    class Node : public GraphNode {
    public:

      friend class NetMap;
      friend class Edge;

      //! \return the classification of a node
      UInt32 getClassification () const { return mClassification; }

      //! \return iff this node has been scratched

      bool isScratched () const { return mClassification == cSCRATCHED; }
      void setClassification (const UInt32 classification) { mClassification = classification; }

      //! \return the encapsulated net.
      NUNet *getNet () const { return mNet; }

      //! \return iff there is an edge from this to a node for net
      bool hasEdge (NUNet *net) const { return mEdges.find (net) != mEdges.end (); }

      //! Add an edge from this to another node.
      /*! \pre there is no edge from this to node
       */
      Edge *addEdge (Node *node);

      //! Scratch an edge from the map
      void scratchEdge (Edge *edge) { mScratched.insert (edge->getTo ()->getNet ()); }

      //! Iterate over the edges of from this node
      /*! \note The order is determined by NUNet::compare over the destinated
          nodes of the directed edges.
      */
      Iter <GraphEdge *> edges ()
      { return Iter <GraphEdge *>::create (Loop <GraphEdgeSet> (mEdgeSet)); }

      //! Iterate over the edge out of this node
      Edge::Map::iterator out_begin () { return mEdges.begin (); }
      Edge::Map::iterator out_end () { return mEdges.end (); }

      //! Iterate over the edge into this node
      Edge::Map::iterator in_begin () { return mBackEdges.begin (); }
      Edge::Map::iterator in_end () { return mBackEdges.end (); }

      //! \return The out degree of this node.
      UInt32 outDegree () const { return mEdges.size (); }

      //! \return The in degree of this node.
      UInt32 inDegree () const { return mBackEdges.size (); }

      //! \return Iff this node has no in-edges.
      bool isSource () const { return mBackEdges.empty (); }

      //! \return Iff this node has no out-edges.
      bool isSink () const { return mEdges.empty (); }

      //! \return The name of the net encapsulated by this node.
      const char *image () const { return mNet->getName ()->str (); }

      //! remove the node from the graph
      void snip ();

      struct Cmp {
        bool operator () (const Node *a, const Node *b) const
        { return NUNet::compare (a->mNet, b->mNet) < 0;    }
      };

      typedef UtSet <Node *, Cmp> Set;

      //! Compose a human readable representation of the node
      void compose (UtString *, const UInt32 = 0) const;

      //! Diagnostic output
      void pr () const;
      
    private:

      Node (NUNet *net, const UInt32 classification) : 
        GraphNode (), mFlags (0), mScratch (0), mClassification (classification), mNet (net), 
        mEdges (), mScratched () {}

      Node (Node &other) : 
        GraphNode (), mFlags (other.mFlags), mScratch (0), mClassification (other.mClassification), 
        mNet (other.mNet), mEdges (), mScratched () {}
                           
      virtual ~Node ();

      //! Insert an edge into this node.
      /*! \note This fixes up mEdges and mEdgeSet of this node, and mBackEdges of
       *  the destination node of the edge.
       */
      void insert (Edge *);

      UInt32 mFlags;                    //!< Flags required by the superclass
      UIntPtr mScratch;                 //!< Scratch storage required by the superclass
      UInt32 mClassification;           //!< current status of the node 
      NUNet *mNet;                      //!< Encapsulated net.
      Edge::Map mEdges;                 //!< Edges out of this node.
      Edge::Map mBackEdges;             //!< Edges into this node
      GraphEdgeSet mEdgeSet;            //!< Set of edges out of this node (used for the edge iterator)
      UtSet <NUNet *> mScratched;       //!< record end points of scratched edges
      UInt32 mLength;                   //!< path length used for branch elimination

    };

    //! Get an iterator over all nodes in the graph
    virtual Iter <GraphNode *> nodes ()
    { return Iter <GraphNode *>::create (Loop <GraphNodeSet> (mNodeSet)); }

    //! Get an iterator over all edges from a node
    virtual Iter <GraphEdge *> edges (GraphNode *node) const
    { return static_cast <Node *> (node)->edges (); }

    //! Get the terminal node of an edge
    virtual GraphNode *endPointOf (const GraphEdge *edge) const
    { return static_cast <const Edge *> (edge)->mTo; }

    //! set flag bits for a node
    virtual void setFlags (GraphNode *node, UInt32 mask) 
    { static_cast <Node *> (node)->mFlags |= mask; }

    //! clear flag bits for a node
    virtual void clearFlags (GraphNode *node, UInt32 mask) 
    { static_cast <Node *> (node)->mFlags &= ~mask; }

    //! test if any of a set of flag bits is set for a node
    virtual bool anyFlagSet (const GraphNode *node, UInt32 mask) const
    { return (static_cast <const Node *> (node)->mFlags & mask) != 0; }

    //! test if all bits from a set of flag bits are set for a node
    virtual bool allFlagsSet (const GraphNode *node, UInt32 mask) const
    { return (static_cast <const Node *> (node)->mFlags & mask) == mask; }

    //! get the full set of flag bits for a node
    virtual UInt32 getFlags (const GraphNode *node) const
    { return static_cast <const Node *> (node)->mFlags; }

    //! set the scratch data for a node
    virtual void setScratch (GraphNode *node, UIntPtr scratch)
    { static_cast <Node *> (node)->mScratch = scratch; }

    //! retrieve the scratch data for a node
    virtual UIntPtr getScratch (const GraphNode *node) const
    { return static_cast <const Node *> (node)->mScratch; }

    //! set flag bits for an edge
    virtual void setFlags (GraphEdge *edge, UInt32 mask)
    { static_cast <Edge *> (edge)->mFlags |= mask; }

    //! clear flag bits for a edge
    virtual void clearFlags (GraphEdge *edge, UInt32 mask) 
    { static_cast <Edge *> (edge)->mFlags &= ~mask; }

    //! test if any of a set of flag bits is set for a edge
    virtual bool anyFlagSet (const GraphEdge *edge, UInt32 mask) const
    { return (static_cast <const Edge *> (edge)->mFlags & mask) != 0; }

    //! test if all bits from a set of flag bits are set for a edge
    virtual bool allFlagsSet (const GraphEdge *edge, UInt32 mask) const
    { return (static_cast <const Edge *> (edge)->mFlags & mask) == mask; }

    //! get the full set of flag bits for a edge
    virtual UInt32 getFlags (const GraphEdge *edge) const
    { return static_cast <const Edge *> (edge)->mFlags; }

    //! set the scratch data for a edge
    virtual void setScratch (GraphEdge *edge, UIntPtr scratch)
    { static_cast <Edge *> (edge)->mScratch = scratch; }

    //! retrieve the scratch data for a edge
    virtual UIntPtr getScratch (const GraphEdge *edge) const
    { return static_cast <const Edge *> (edge)->mScratch; }

    //! extract a subgraph of the given graph containing only certain nodes
    virtual Graph *subgraph (GraphNodeSet& keepSet, bool copyScratch);

    //! Remove the set of nodes and all edges incident on them from the graph
    virtual void removeNodes (const GraphNodeSet &doomed);

    //! Scratch the set of nodes and remove all edges incident on them from the graph
    virtual void scratchNodes (const GraphNodeSet &doomed);

    //! Remove a set of edges
    void removeEdges (const Edge::Set &doomed);

    //! Return the number of nodes in the graph
    UInt32 size () const { return mNodes.size (); }

    //! Construct a string describing the graph
    /*! \param options Composition options, determined by \enum ComposeOptions.
     *  \note The lower eight bit of options are used for the indentation amount
     */
    void compose (UtString *s, const UInt32 options = 0) const;

    //! \enum ComposeOptions
    enum ComposeOptions {
      cINDENT_MASK = 0xFF,              //!< lower bits are the indent amount
      cWEIGHT      = 1<<9,              //!< show the edge weights
      cLOCATION    = 1<<10,             //!< show the source location
      cLINE        = 1<<11,             //!< output each edge on a different line
      cNONE        = 0
    };

    //! Diagnostic output
    void pr () const;

  private:

    //! Locate the node corresponding to a net
    /*! \note If there is no node corresponding to net, then a new node is created
     */
    Node *getNode (NUNet *net);

    //! Comparison method for sorting nodes
    /*! The sorting order is derived from the ordering of the encapsulated nets.
     */
    //! Insert a new node into the graph.
    /*! \note Updates mNodes and mNodeSet
     */
    void insert (Node *);

    typedef UtMap <NUNet *, Node *> NodeMap;

    NodeMap mNodes;                     //!< Map of the nodes in this graph indexed by the net
    GraphNodeSet mNodeSet;              //!< Set of the nodes in this graph (used for the node iterator)

    ConcatAnalysis &mConcatAnalysis;    //! node classification context

    //! helper method for remove_branches
    bool prune_forwards (NetMap::Node *node, NetMap::Edge::Set *);

    //! helper method for remove_branches
    bool prune_backwards (NetMap::Node *node, NetMap::Edge::Set *);

    //! zero the length field of every node
    void zero_length ();
      
  };                                    // class NetMap

public:

  // diagnostic methods

  //! called after each extraction of dependencies from a statement cluster
  virtual void note_extract (const NUAssign *, const NUAssign *, const UInt32, NetMap &) const {}

  //! called when cycles are found in the net map
  virtual void note_cycles (GraphNodeSet &) const {}

  //! called when forward edges are pruned from the net map
  virtual void note_forward_pruning (NetMap::Edge::Set &) const {}
  
  //! called when backward edges are pruned from the net map
  virtual void note_backward_pruning (NetMap::Edge::Set &) const {}

  //! called when the collapser has found potential vectorisations
  virtual void note_vectorisation (NUStmtList::iterator, const UInt32) const {}

  //! called when the collapser adds a new edge
  virtual void note_vector_edge (NetMap::Edge *) const {}

  //! called when a vectorisation is applied to a module definition
  virtual void note_apply (NUModule *) const {}

  //! called when a vectorisation is applied to a module instance
  virtual void note_apply (NUModuleInstance *) const {}

  //! Format net information relevant to vectorisation for diagnostics
  void compose (UtString *, const NUNet *) const;

  //! Format netflags into human readable form
  void compose (UtString *, const NetFlags) const;

protected:

  AtomicCache *mStrCache;               //!< all the
  IODBNucleus *mIODB;                   //!< ... usual
  MsgContext *mMsgContext;              //!< ... plumbing
  NUNetRefFactory *mNetRefFactory;      //!<
  ProtectedAlias &mProtect;             //!< closure of protected nets
  NUModule *mModule;                    //!< module under investigation

private:

  // prevent assignment and copying
  ConcatAnalysis (const ConcatAnalysis &);
  ConcatAnalysis &operator = (const ConcatAnalysis &);

};


#endif
