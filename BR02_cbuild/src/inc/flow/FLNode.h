// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef FLNODE_H_
#define FLNODE_H_

#include "flow/Flow.h"
#include "nucleus/NUNetRef.h"

//template<class T, size_t size> class MemoryPool;

/*!
  \file
  FLNode and FLNodeBound class declarations.
*/


// Note: this macro is written this way so that it can be invoked like a
// function, with the final semicolon supplied by the user.  A brace-delimited
// statement is no good, because it leads to syntax errors with code like:
// if (cond)
//   FLN_ASSERT(predicate,flow);
// else
//   ...

#define FLN_ASSERT(exp, flow) \
  do { \
    if (!(exp)) { \
      (flow)->printAssertInfo(__FILE__, __LINE__, __STRING(exp)); \
    } \
  } while (0) /* no trailing semicolon -- it is supplied by the caller */


//! FLNode class
/*!
 * Flow node - models the def of a net, at some construct, with connections
 * to other nodes.  This is unelaborated.
 *
 * Flow nodes may be nested, in the case of block constructs (for example,
 * an always block), or module instantiations.  In the case of nesting, the
 * parent flow node still has valid fanin information, but the fanin skips
 * over the nested internals.  For example, if the always block is:
 * \code
     always @(a)
     begin
       b = a;
       c = b;
     end
 * \endcode
 * The FLNode for the always block def of 'c' will return getFanin() = 'a'.
 * But, for the details on how 'a' propagates to 'c', you must step through the
 * nested blocks.
 */
class FLNode
{
public:
  // MemoryPool
  typedef FLNode* iterator;
  typedef FLNode& reference;
  typedef FLNodeSet Set;

  // Type information useful for template code
  typedef FLNodeFactory FactoryType;
  typedef NUNet DefNetType;

  //! True if this FLNode is any type of bounded flow node (e.g. input, bid, hier_ref, deposit/force, or undriven)
  bool isBoundNode() const { return (getType() & eFLBound); }
  
  //! Return the type of this node.
  FLTypeT getType() const {return (FLTypeT) mType;}

  StringAtom* getName() const { return getDefNet()->getName(); }

  //! Return the def net.
  NUNet *getDefNet() const;

  //! Return the def net ref.
  NUNetRefHdl getDefNetRef() const { return mNetRef; }

  //! Return the def net ref -- the factory argument matches FLNodeElab::getDefNetRef()
  NUNetRefHdl getDefNetRef(NUNetRefFactory* /* factory */) const { return mNetRef; }

  //! Return the language construct.
  virtual NUUseDefNode *getUseDefNode() const { return mUseDefNode; }

  //! Change the language construct.
  void setUseDefNode ( NUUseDefNode * use_def ) { mUseDefNode = use_def; }

  //! Return true if this node has a nested node and the nesting is because of
  //! hierarchy.
  bool hasNestedHier() const;

  //! Return true if this node has a nested node and the nesting is because of
  //! a block construct.
  bool hasNestedBlock() const;

  //! Return the only nested flow node.
  //  This will return NULL if the block has multiple nested flows!
  FLNode* getSingleNested() const;

  //! Get the number of nested flows.
  UInt32 numNestedFlows() const;

  //! Add this to node's nested hierarchy set.  Not valid to call if hasNestedBlock() is true.
  void addNestedHier(FLNode *nested);

  //! Add this to node's nested block set.  Not valid to call if hasNestedHier() is true.
  void addNestedBlock(FLNode *nested);

  //! Clear our nested block. Used when trying to in-place patch up flow.
  void clearNestedBlock();

  //! Abstraction to visit loop fanin
  typedef Iter<FLNode*> FaninLoop;

  //! Clear the level fanin set (\sa clearEdgeFanin)
  virtual void clearFanin();

  //! Clear the edge fanin set (\sa clearFanin)
  virtual void clearEdgeFanin();

  //! Add the given fanin node to the level fanin set of this node
  virtual void connectFanin(FLNode *fanin_node);

  //! Add all fanin to the level fanin set of this node.
  virtual void connectFaninFromLoop(FaninLoop & loop);

  //! Add the given edge fanin node to the edge fanin set of this node
  virtual void connectEdgeFanin(FLNode *fanin_node);

  //! Add all edge fanin to the edge fanin set of this node.
  virtual void connectEdgeFaninFromLoop(FaninLoop & loop);

  //! Add the given net ref's drivers as fanin to the level fanin set of this node
  /*!
   * Drivers will be added for which (net_ref.*fn)(driver) is true.
   */
  virtual void connectFanin(const NUNetRefHdl &net_ref, NUNetRefCompareFunction fn);

  //! Add the given net ref's driver as fanin to this node skipping bid port cycles
  /*! This routine uses loopContinuousDrivers to find all the drivers of the
   *  fanin net. For bid ports this will find the the flow node
   *  representing the module instance for the same bid port
   *  connection. If we connect that it will look like the bid
   *  connection drives itself. So this routine avoids that problem.
   */
  virtual void connectFaninSkipBidCycle(const NUNetRefHdl &net_ref,
                                        NUNetRefCompareFunction fn);

  //! Add the given edge net ref's drivers to the edge fanin set of this node
  /*!
   * Drivers will be added for which (net_ref.*fn)(driver) is true.
   */
  virtual void connectEdgeFanin(const NUNetRefHdl &net_ref, NUNetRefCompareFunction fn);

  //! Add the given net ref's driver as fanin to this node skipping bid port cycles
  /*! This routine uses loopContinuousDrivers to find all the drivers of the
   *  fanin net. For bid ports this will find the the flow node
   *  representing the module instance for the same bid port
   *  connection. If we connect that it will look like the bid
   *  connection drives itself. So this routine avoids that problem.
   */
  virtual void connectEdgeFaninSkipBidCycle(const NUNetRefHdl &net_ref,
                                            NUNetRefCompareFunction fn);

  //! Checks if this flow node and a potential fanin represent the same bid port
  bool isSameBidPortConnection(FLNode* fanin);

  //! Remove \a driver from both level and edge fanin sets.
  virtual void removeFanin(FLNode* driver);

  //! Remove \a driver from the level fanin set.
  virtual void removeLevelFanin(FLNode * driver);

  //! Remove \a driver from the edge fanin set.
  virtual void removeEdgeFanin(FLNode * driver);


  //! Replace the list of given fanins with another list of drivers; used for
  //! aliasing.
  virtual void replaceFaninsWithLoop(const FLNodeList &old_drivers,
				     NUNet::DriverLoop new_drivers);

  //! Replace any fanins which are keys of the given map with the set of nodes
  //! mapped-to.
  virtual void replaceFaninsWithMap(const FLNodeNodeSetMap &replacement_map);

  //! Replace the old driver with the new driver in the fanin
  virtual void replaceFanin(FLNode *old_driver, FLNode *new_driver);

  //! Change the def net from the old net to the new net
  virtual void replaceDefNetRef(const NUNetRefHdl &old_ref, const NUNetRefHdl &new_ref);

  //! Iterate over all nodes which fanin to this node.
  FaninLoop loopFanin();

  //! Iterate over all edge nodes which fanin to this node.
  FaninLoop loopEdgeFanin();

  //! Abstraction to iterate over level and edge fanin
  typedef LoopPair<FaninLoop, FaninLoop> AllFaninLoop;

  //! Iterate over level and edge fanin (note duplicates are not pruned)
  AllFaninLoop loopAllFanin();

  //! Iterate over all nested nodes for this node.
  Iter<FLNode*> loopNested();

  //! Is the specified node a fanin for the given sensitivity?
  virtual bool isFanin(FLNode* fanin, Sensitivity sense) const;

  //! returns true if \a flow is a nested node of this.  If \a recurse == true then recurse through nested nodes looking for \a flow.
  bool isNested(FLNode* flow, bool recurse=false) const;

  //! Determine if the provided fanin node is a legal fanin of this node.
  /*!
   * Flow referencing a NUPortConnection assume that all fanin are
   * legal. This is because their uses may be either in the parent or
   * child module; more work would be needed to perform the proper
   * check (this mirrors FLNodeElab::checkFaninOverlap).
   *
   * Virtual nets allow all fanin. The $extnet flow includes
   * connections to protected-observable nets which are not always
   * modelled in the UD. The module instance flow is also different
   * for virtual nets than for other nets.
   *
   * Edge fanin are not checked during this call. Use
   * checkEdgeFaninOverlap to determine if a FLNode is a legal edge
   * fanin.
   *
   * \sa FLNode::checkEdgeFaninOverlap 
   * \sa FLNodeElab::checkFaninOverlap
   * \sa FLNodeElab::checkEdgeFaninOverlap
   */
  virtual bool checkFaninOverlap(const FLNode * fanin, NUNetRefFactory * netRefFactory);

  //! Determine if the provided fanin node is a legal edge fanin of this node.
  /*!
   * Only FLNode with an NUAlwaysBlock as its NUUseDefNode node may
   * have edge fanin. Flow with any other type of NUUseDefNode will
   * return false.
   *
   * Unlike checkFaninOverlap, virtual nets ($extnet, $ostnet, $cfnet,
   * etc.) are not blindly allowed.
   *
   * \sa FLNode::checkFaninOverlap 
   * \sa FLNodeElab::checkFaninOverlap
   * \sa FLNodeElab::checkEdgeFaninOverlap
   */
  virtual bool checkEdgeFaninOverlap(const FLNode * fanin, NUNetRefFactory * netRefFactory);
  
  //! Return the nodes which fanin to this node and are a def of the given net.
  /*!
   * Nodes will be returned for which (node->getDefNetRef().*fn)(net_ref) is true.
   */
  FLNodeSet* getFaninForNet(const NUNetRefHdl &net_ref, NUNetRefCompareFunction fn) const;

  //! Create a fanin iterator.  Must be deleted by the caller.
  FLNodeIter* makeFaninIter(FLIterFlags::TraversalT visit,
                            FLIterFlags::TraversalT stop,
                            FLIterFlags::NestingT nesting,
                            bool visit_start=false,
                            FLIterFlags::IterationT iteration=FLIterFlags::eIterNodeOnce);

  //! Create a fanin iterator.  Must be deleted by the caller.
  FLNodeIter* makeFaninIter(int visit,
                            int stop,
                            int nesting,
                            bool visit_start=false,
                            FLIterFlags::IterationT iteration=FLIterFlags::eIterNodeOnce)
  {
    return makeFaninIter((FLIterFlags::TraversalT) visit,
                         (FLIterFlags::TraversalT) stop,
                         (FLIterFlags::NestingT) nesting,
                         visit_start,
                         iteration);
  }

  //! Dump myself to cout
  virtual void print(bool recurse=true, int indent=0) const;

  //! Dump myself to UtIO::cout() for post-processing
  virtual void ppPrint(int indent) const;

  //! is this node an encapsuled cycle?
  bool isEncapsulatedCycle() const {return false;}

  //! Set the mark
  void putMark(bool flag) { mMark = flag; }

  //! Query the mark
  bool getMark() const { return mMark; }

  //! Assertion support, print out relevant information.
  void printAssertInfo(const char* file, int line, const char* exprStr) const;

  static int compare(const FLNode* f1, const FLNode* f2);
  bool operator<(const FLNode & other) const {
    return compare(this, &other) < 0;
  }

  //! To qualify as a member of a MemoryPool, FLNode must implement putIsAllocated.
  /*! See the comments in src/inc/util/MemoryPool.h. */
  void putIsAllocated(bool flag);

  //! To qualify as a member of a MemoryPool, FLNodeElab must implement isAllocated.
  /*! See the comments in src/inc/util/MemoryPool.h. */
  bool isAllocated() const {return mIsAllocated;}

protected:
  //! Ensure that creation and deletion can only go through FLNodeFactory.
  friend class FLNodeFactory;
  friend class MemoryPool<FLNode, 4096>;

  //! constructor
  /*!
    \param net_ref points to the net that this FLNode models the defs for.
    \param node is the nucleus construct which defines the semantics of this definition.
    \param type Type of node.

   * Protected so that only FLNodeFactory or derived classes can call.
   */
  FLNode(const NUNetRefHdl &net_ref, NUUseDefNode *node, FLTypeT type);

  //! constructor used by FLNodeBound
  FLNode(const NUNetRefHdl &net_ref, FLTypeT type):
    mNetRef(net_ref),
    mUseDefNode(0),
    mType((unsigned char) type),
    mNestingHier(false),
    mMark(false)
  {}

  //! destructor
  /*!
   * Protected so that only FLNodeFactory or derived classes can call.
   */
  virtual ~FLNode() {}

  //! Net ref for which this flow node defines a value.
  NUNetRefHdl mNetRef;

  //! nucleus construct defining the semantics of this definition.
  NUUseDefNode* mUseDefNode;

  //! Set of data fanin nodes (nets used as clock or reset edges in sequential always blocks go in mEdgeFanin instead)
  FLNodeSet mFanin;

  //! Set of edge (reset, clock) fanin nodes (data fanin goes in mFanin instead), this set is empty except for sequential always blocks.
  FLNodeSet mEdgeFanin;

  //! If this construct has nesting, this is/are the nested flow node(s).
  FLNodeSet mNested;

  //! The type of node.
  unsigned char mType;

  //! Cheap way to do this for now, if mNested is non-empty, then if this is true,
  //! the nesting is hierarchical, else its block.
  bool mNestingHier;

  //! General mark flag, useful for mark/sweep.
  /*!
   * There is no guarantee of the state of this flag; passes do not need to clean up after
   * themselves.  So, if you want it cleared, you must clear it yourself.
   */
  bool mMark;

  //! helper flag for MemoryPool
  bool mIsAllocated;

private:
  //! Hide copy and assign constructors.
  FLNode(const FLNode&);
  FLNode& operator=(const FLNode&);
};


//! FLNodeBound : public FLNode class
/*!
 * Flow node which models the top-level inputs and bids of a module.
 */
class FLNodeBound : public FLNode
{
public:
  //! Will always return 0
  NUUseDefNode *getUseDefNode() const;

  //! Invalid to call
  void clearFanin();

  //! Invalid to call
  void connectFanin(FLNode *fanin_node);

  //! Invalid to call
  void connectFanin(const NUNetRefHdl &net_ref, NUNetRefCompareFunction fn);


  //! Invalid to call
  void replaceFanin(FLNode *old_driver, FLNode *new_driver);

  //! Change the def net from the old net to the new net
  void replaceDefNetRef(const NUNetRefHdl &old_ref, const NUNetRefHdl &new_ref);

  //! Dump myself to cout
  void print(bool recurse=true, int indent=0) const;

  //! Dump myself to UtIO::cout() for post-processing
  void ppPrint(int indent) const;

private:
  //! Ensure that creation and deletion can only go through FLNodeFactory.
  friend class FLNodeFactory;
  friend class MemoryPool<FLNode, 4096>;

  //! constructor
  /*!
    \param net_ref Which net this flow node defines.
    \param type and it's type

   * Private so that only FLNodeFactory can call.
   */
  FLNodeBound(const NUNetRefHdl &net_ref, FLTypeT type) : FLNode(net_ref, type)
  {}

  //! destructor
  /*!
   * Private so that only FLNodeFactory can call.
   */
  ~FLNodeBound() {}

  //! Hide copy and assign constructors.
  FLNodeBound(const FLNodeBound&);
  FLNodeBound& operator=(const FLNodeBound&);
};

//! Comparator for value-based FLNode comparisons
struct FLNodeCmp
{
  bool operator()(const FLNode* flow1, const FLNode* flow2) const
  {
    return FLNode::compare(flow1, flow2) < 0;
  }
};

#endif
