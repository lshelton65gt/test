// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef FLITER_H_
#define FLITER_H_

#include "nucleus/Nucleus.h"
#include "util/UtStack.h"
#include "flow/FLNodeSet.h"
#include "flow/FLNodeElabSet.h"
#include "flow/FLNodeElab.h"

typedef FLNodeElabSet::iterator FLNodeElabSetIter;
typedef FLNodeElabSet::const_iterator FLNodeElabSetConstIter;
typedef FLNodeSet::iterator FLNodeSetIter;
typedef Loop<const FLNodeElabSet> FLNodeElabSetConstLoop;

/*!
  \file
  Declaration of templatized iterator class used to traverse the flow
  node graph.
*/

//! FLIter class
/*!
 * Abstract base class for flow iterators.
 */
template<class FlowNode>
class FLIterBase: public Iter<FlowNode*>::RefCnt
{
#if ! pfGCC_2 && !pfMSVC
  using Iter<FlowNode*>::RefCnt::operator();
#endif

public:
  //! Force the iterator not to expand through the current node.
  virtual void doNotExpand() = 0;

  //! Start/restart the iteration.  Automatically called at construction. 
  virtual void begin() = 0;

  //! Return true if no more nodes
  virtual bool atEnd() const = 0;

  //! Advance to the next node.
  virtual void operator++() = 0;

  //! Advance to the next node.
  inline void next() {++(*this);}

  //! Advance to the next node, get it, check for end
  bool operator()(typename Iter<FlowNode*>::value_type* ptr) {
    if (atEnd())
      return false;
    *ptr = **this;
    return true;
  }

  // Must define to silence ICC, but don't really need it.
  bool operator()(const typename Iter<FlowNode*>::value_type**) {
    INFO_ASSERT (0, "unimplemented function");
    return true;
  }

  //! Returns all flags this node could possibly match, except eAll or eNone.
  virtual FLIterFlags::TraversalT getCurFlags() const = 0;

  //! destructor
  virtual ~FLIterBase() {}
};

#ifdef CDB
void gFlowIterElementStopHere();
#endif

template<class FlowNode> class FlowIterElement
{
public:
  FlowIterElement(FlowNode* node, FlowNode* fanout,
                  Sensitivity sensitivity, bool isNestedRelationship)
  {
    mNode = node;
    mFanout = fanout;
    mSensitivity = sensitivity;
    mIsNestedRelationship = isNestedRelationship;
  }

/*
  FlowIterElement(FlowNode* first, FlowNode* second)
  {
#ifdef CDB
    gFlowIterElementStopHere();
#endif
    mNode = first;
    mFanout = second;
    mSensitivity = eSensLevelOrEdge;
  }
*/

  bool operator==(const FlowIterElement& src)
    const
  {
    return ((mNode == src.mNode) &&
            (mFanout == src.mFanout) &&
            (mSensitivity == src.mSensitivity) &&
            (mIsNestedRelationship == src.mIsNestedRelationship));
  }

  size_t hash() const
  {
    return (((size_t) mNode) + 7*((size_t) mFanout));
  }

  bool operator<(const FlowIterElement& a) const
  {
    return this->compare (a) < 0;
  }

  int compare(const FlowIterElement& src)
    const
  {
    if (src == *this)
      return 0;
    if (mNode < src.mNode)
      return -1;
    if (mNode > src.mNode)
      return 1;
    if (mFanout < src.mFanout)
      return -1;
    if (mFanout > src.mFanout)
      return 1;
    if (mSensitivity < src.mSensitivity)
      return -1;
    if (mSensitivity > src.mSensitivity)
      return 1;
    if (mIsNestedRelationship && !src.mIsNestedRelationship)
      return -1;

    // otherwise src == *this.
    INFO_ASSERT((!mIsNestedRelationship && src.mIsNestedRelationship), "Cannot tell the difference between two FlowIterElements");
    return 1;
  }

  FlowNode* mNode;
  FlowNode* mFanout;
  Sensitivity mSensitivity : 16;
  bool mIsNestedRelationship;
};

template<class FlowNode> struct CmpFlowIterElement
{
  bool operator()(const FlowIterElement<FlowNode>& f1,
                  const FlowIterElement<FlowNode>& f2)
  {
    return f1.compare(f2) < 0;
  }
};


//! FLIterTemplate class
/*!
 * Iterate over a flow graph. Templatized, so can walk pre-elab and
 * post-elab the same.
 */
template<class FlowNode, class FlowNodeBound> class FLIterTemplate :
  public FLIterBase<FlowNode>
{
public:
  //! constructor
  /*!
    \param node Flow node to begin traversal.
    \param visit Which nodes to visit, during the traversal.  The iterator will
      return these nodes.  If eAll, all nodes are visited.
    \param stop Which nodes to stop the traversal.  Note the iterator will not
      necessarily return these nodes, unless they also match the visit flags.
    \param nesting What to do when find nesting; step into it or over it.
    \param visit_start If true, the passed-in node will be used to start traversal.
      If false, the fanin of the passed-in node will be used.
    \param iteration How to control iteration through the fanin.

    Some examples of usage:

    To iterate over just the immediate fanin to a node, use:
    \code
    FLIterTemplate(node, eAll, eAll, false, false, eIterNodeOnce);
    \endcode

    To iterate over all fanin to a node, back to "natural" stoppers of inputs,
    undriven nets, and loops, use:
    \code
    FLIterTemplate(node, eAll, eNone, false, false, eIterNodeOnce);
    \endcode

    To iterate over all the state points between this node and local inputs, use:
    \code
    FLIterTemplate(node, eState, eLocalInput, false, false, eIterNodeOnce);
    \endcode
   */
  FLIterTemplate(FlowNode *node,
		 FLIterFlags::TraversalT visit,
		 FLIterFlags::TraversalT stop,
		 FLIterFlags::NestingT nesting,
		 bool visit_start,
		 FLIterFlags::IterationT iteration);

  //! destructor
  virtual ~FLIterTemplate();

  //! Force the iterator not to expand through the current node.
  void doNotExpand();

  //! Start/restart the iteration.  Automatically called at construction. 
  virtual void begin();

  //! Return true if no more nodes
  bool atEnd() const;

  //! Advance to the next node.
  void operator++();

  //! Return the current node.
  FlowNode* operator*() const;

  //! Return the current node.
  FlowNode* getCur() const;

  //! Return the node which preceeded this on the traversal;
  //! getCur() fans out to getCurParent()
  FlowNode* getCurParent() const;

  //! Returns true if we got here due to a nesting relationship with parent
  bool isNestedRelationship() const;

  //! Indicate whether we traversed to this node via a level or edge
  Sensitivity getSensitivity() const;

  //! Returns all flags this node could possibly match, except eAll or eNone.
  FLIterFlags::TraversalT getCurFlags() const;

  //! Get the initial sensitivity based on visit-flags
  Sensitivity getInitialSensitivity() const;

protected:
  //! constructor for use by derived classes only.  Does not call begin.
  FLIterTemplate(FLIterFlags::TraversalT visit,
		 FLIterFlags::TraversalT stop,
		 FLIterFlags::NestingT nesting,
		 bool visit_start,
		 FLIterFlags::IterationT iteration);

  //! Find the next node to visit.
  void advanceToVisit();

  UtStack<FlowIterElement<FlowNode> > mStack;

  // Keep both mHaveSeenPairSet and mHaveSeenSet in FLIterTemplate,
  // by value, plus an additional flag, mIteration, to indicate which
  // is appropriate to use.  This is, I think, a superiour alternative
  // for this class to keeping pointers to those two sets and using
  // their null-nes to indicate which of them to use.  The reason is
  // that the FLIterTemplates get allocated on the stack, so whether
  // they cost 8+8+4=20 bytes or 4+4=8 bytes doesn't make much difference.
  //
  // But FLIterTemplates get constructed and destructed a very large
  // number of times, requiring a lot of calls to new/delete if those
  // are kept in the class as pointers.  Alternatively, constructing
  // an empty UtHashSet on the stack is very cheap.  The constructor
  // does no initial allocation.  So let's instantiate sets by value
  // and choose which to use based on the flag mIteration.

  typedef typename FlowNode::Set FlowNodeSet;
  FlowNodeSet mHaveSeenSet;
  typedef UtHashSet< FlowIterElement<FlowNode>,
                     HashValue< FlowIterElement<FlowNode> >,
                     HashMgr< FlowIterElement<FlowNode> > >
  FlowIterElementSet;
  FlowIterElementSet mHaveSeenPairSet;
  FLIterFlags::IterationT mIteration;

private:
  //! Hide copy and assign constructors.
  FLIterTemplate(const FLIterTemplate&);
  FLIterTemplate& operator=(const FLIterTemplate&);

protected:
  //! Push the given node's fan onto the traversal stack.
  /*!
   * Doesn't check if the current top node is a stop node, must do that
   * before you call this.
   */
  void pushNextFan(FlowNode *node, Sensitivity sense);

  //! Add this record to the to-visit stack, if neccessary
  void scheduleVisit(FlowNode* node, FlowNode* fanout, Sensitivity sense,
                     bool isNestedRelationship);
private:
  //! Return true if the given node is a stop node.
  bool isStopNode(FlowNode *node);

  //! Return true if the given node is a visit node.
  bool isVisitNode(FlowNode *node);

  //! Return true if the given node is a nested node we want to step into.
  bool isStepIntoNode(FlowNode *node);

  //! Return true if the given node is a nested node we want to branch at.
  bool isBranchAtNode(FlowNode *node);

  //! Remember that we've iterated over the given node, fanout pair.
  void rememberIterate(const FlowIterElement<FlowNode>& e);

  //! Return true if want to iterate over the given node, fanout pair.
  bool doIterate(FlowNode *node, FlowNode *fanout, Sensitivity sense,
                 bool isNestedRelationship);

  FlowNode* mStartNode;
  FLIterFlags::TraversalT mVisitFlags;
  FLIterFlags::TraversalT mStopFlags;
  FLIterFlags::NestingT mNestingFlags;
  
protected:
  bool mVisitStart;
private:
  bool mForceStop;
};


//! FLSetIterTemplate class
/*!
 * Iterate over a flow graph using some set of starting points.
 * Templatized, so can walk pre-elab and post-elab the same.
 */
template<class FlowNode, class FlowNodeBound> class FLSetIterTemplate :
  public FLIterTemplate<FlowNode, FlowNodeBound>
{
public:
  typedef typename FlowNode::Set FlowNodeSet;

  //! constructor
  /*!
    \param flow_set List of starting flow.  This set is be copied.
    \param visit Which nodes to visit, during the traversal.  The iterator will
      return these nodes.  If eAll, all nodes are visited.
    \param stop Which nodes to stop the traversal.  Note the iterator will not
      necessarily return these nodes, unless they also match the visit flags.
    \param nesting What to do when find nesting; step into it or over it.
    \param visit_start If true, the passed-in nodes will be used to start traversal.
      If false, the fanin of the passed-in node will be used.
    \param iteration How to control iteration through the fanin.
  */
  FLSetIterTemplate(FlowNodeSet*,
		    FLIterFlags::TraversalT visit,
		    FLIterFlags::TraversalT stop,
		    FLIterFlags::NestingT nesting,
		    bool visit_start,
		    FLIterFlags::IterationT iteration);

  //! Start/restart the iteration.  Automatically called at construction. 
  void begin();

  //! destructor
  virtual ~FLSetIterTemplate();

protected:
  //! constructor for use by derived classes only.  Does not call begin.
  FLSetIterTemplate(FLIterFlags::TraversalT visit,
		    FLIterFlags::TraversalT stop,
		    FLIterFlags::NestingT nesting,
		    bool visit_start,
		    FLIterFlags::IterationT iteration);

  //! Set list of nets to traverse fanin.  List will be copied.
  void setFlowSet(FlowNodeSet *flow_set);
  FlowNodeSet mFlowSet;

private:
  //! Hide copy and assign constructors.
  FLSetIterTemplate(const FLSetIterTemplate&);
  FLSetIterTemplate& operator=(const FLSetIterTemplate&);
};


//! FLNetIterTemplate class
/*!
 * Iterate over the fanin to a list of nets.  Templatized, so can walk pre-elab and
 * post-elab the same.
 */
template<class Net, class FlowNode, class FlowNodeBound> class FLNetIterTemplate :
  public FLIterTemplate<FlowNode, FlowNodeBound>
{
public:
  //! constructor
  /*!
    \param net_list List of nets to traverse fanin.  This list will be copied.
    \param visit Which nodes to visit, during the traversal.  The iterator will
      return these nodes.  If eAll, all nodes are visited.
    \param stop Which nodes to stop the traversal.  Note the iterator will not
      necessarily return these nodes, unless they also match the visit flags.
    \param nesting What to do when find nesting; step into it or over it.
    \param iteration How to control iteration through the fanin.
  */
  FLNetIterTemplate(UtList<Net*> *net_list,
		    FLIterFlags::TraversalT visit,
		    FLIterFlags::TraversalT stop,
		    FLIterFlags::NestingT nesting,
		    FLIterFlags::IterationT iteration);

  //! constructor with a single net instead of a list of nets
  FLNetIterTemplate(Net* net,
		    FLIterFlags::TraversalT visit,
		    FLIterFlags::TraversalT stop,
		    FLIterFlags::NestingT nesting,
		    FLIterFlags::IterationT iteration);

  //! Start/restart the iteration.  Automatically called at construction. 
  void begin();

  //! destructor
  virtual ~FLNetIterTemplate();

protected:
  //! constructor for use by derived classes only.  Does not call begin.
  FLNetIterTemplate(FLIterFlags::TraversalT visit,
		    FLIterFlags::TraversalT stop,
		    FLIterFlags::NestingT nesting,
		    FLIterFlags::IterationT iteration);

  //! Set list of nets to traverse fanin.  List will be copied.
  void setNetList(UtList<Net*> *net_list);
  UtList<Net*> mNetList;

private:
  //! Hide copy and assign constructors.
  FLNetIterTemplate(const FLNetIterTemplate&);
  FLNetIterTemplate& operator=(const FLNetIterTemplate&);
};


// Iterate over all of a design's elaborated flow.
class FLDesignElabIter :
  public FLNetIterTemplate<NUNetElab,FLNodeElab,FLNodeBoundElab>
{
public:
  //! constructor
  /*!
    \param design The design.
    \param symtab The symbol table used to look up elaboration.
    \param start Masks eModuleLocals and eClockMasters saying whether to start with those
    \param visit Which nodes to visit, during the traversal.  The iterator will
      return these nodes.  If eAll, all nodes are visited.
    \param stop Which nodes to stop the traversal.  Note the iterator will not
      necessarily return these nodes, unless they also match the visit flags.
    \param nesting What to do when find nesting; step into it or over it.
    \param iteration How to control iteration through the fanin.
   */
  FLDesignElabIter(NUDesign *design,
		   STSymbolTable *symtab,
		   FLIterFlags::StartT start,
		   FLIterFlags::TraversalT visit,
		   FLIterFlags::TraversalT stop,
		   FLIterFlags::NestingT nesting,
		   FLIterFlags::IterationT iteration);

  //! destructor
  ~FLDesignElabIter();

private:
  //! Hide copy and assign constructors.
  FLDesignElabIter(const FLDesignElabIter&);
  FLDesignElabIter& operator=(const FLDesignElabIter&);
};

#endif
