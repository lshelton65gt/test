// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implements a cache for elaborated UD
*/

#ifndef _FLELABUSECACHE_H_
#define _FLELABUSECACHE_H_

class NUNetRefSet;
class NUNetRefFactory;
class FLNodeElab;
class FLNode;
class NUNetElab;
class NUNetRefHdl;

//! Class FLElabUseCache
/*! During the sanity elab passes, overlap checks are done. To avoid
 *  N*N operations this cache, keeps track of the UD information.
 */
class FLElabUseCache
{
public:
  //! Constructor
  FLElabUseCache(NUNetRefFactory* netRefFactory, bool edge);

  //! Destructor
  ~FLElabUseCache();

  //! Get the matching uses for two elaborated flows
  const NUNetRefSet* getMatchingUses(FLNodeElab* fanoutElab, 
                                     const FLNodeElab* faninElab);

private:
  //! Container for the cached data
  /*! The cache data is as follows:
   *
   * 1. A NUNetRefSet for each unelaborated flow (so that it is
   *    retrieved once per unelaborated flow).
   *
   * 2. A lookup table per hierarchical instance of the flow node.
   *    This quickly gets the uses per fanin net elab. These are created per fanout flow elab
   *    hierarchy.
   */
  class CacheData
  {
  public:
    //! Constructor (creates the unelaborated UD data)
    CacheData(NUNetRefFactory* netRefFactory, FLNode* flow, bool edge);

    //! Destructor
    ~CacheData();

    //! Get the uses for a given elaborated fanin of this node
    const NUNetRefSet* getUses(STBranchNode* hier, NUNetElab* netElab,
                               NUNetRefFactory* netRefFactory);

  private:
    //! Abstraction for the use def set per elaborated fanin
    typedef UtHashMap<NUNetElab*, NUNetRefSet*> ElabUses;

    //! Abstraction to keep track of elaborated data per hierarchical instance
    typedef UtHashMap<STBranchNode*, ElabUses*> HierElabUses;

    //! Helper function to add a given elaborated net ref
    void addElabNetRef(ElabUses*, NUNetElab*, const NUNetRefHdl&, NUNetRefFactory*);

    //! The unelaborated use information for this node
    NUNetRefSet* mUses;

    //! The elaborated uses data per hierarchical instance of the flow node.
    HierElabUses* mHierElabUses;
  }; // class CacheData

  //! NetRef factory for set creation
  NUNetRefFactory* mNetRefFactory;

  //! Flag to indicate whether we are processing level or edge uses
  bool mEdge;

  //! Abstraction for the cache of UD data
  typedef UtHashMap<FLNode*, CacheData*> UDCache;

  //! The UD Cache
  UDCache* mUDCache;

  //! And empty net ref set for when we find no uses
  NUNetRefSet* mEmpty;

}; // class FLElabUseCache

#endif // _FLELABUSECACHE_H_
