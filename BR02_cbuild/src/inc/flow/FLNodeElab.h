// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef FLNODEELAB_H_
#define FLNODEELAB_H_

#include "util/CarbonAssert.h"
#include "util/LoopPair.h"
#include "nucleus/NUNet.h"
#include <cstddef>


//template<class T, size_t size> class MemoryPool;

class NUUseDefNode;

class SCHSignature;
class HierName;
class STBranchNode;
class FLNodeElab;
class NUCycle;
class FLNodeElabFactory;
class UtIOFileStream;
class FLElabUseCache;

// Note: this macro is written this way so that it can be invoked like a
// function, with the final semicolon supplied by the user.  A brace-delimited
// statement is no good, because it leads to syntax errors with code like:
// if (cond)
//   FLN_ELAB_ASSERT(predicate,flow);
// else
//   ...

#define FLN_ELAB_ASSERT(exp, flowElab) \
  do { \
    if (!(exp)) { \
      (flowElab)->printAssertInfo(__FILE__, __LINE__, __STRING(exp)); \
    } \
  } while (0) /* no trailing semicolon -- it is supplied by the caller */

/*!
  \file
  FLNodeElab and FLNodeBoundElab class declarations.
*/

//! FLNodeElab class
/*!
 * A flow node in some elaborated hierarchy.
 *
 * See the FLNode class for a general description of flow nodes.  Elaborated
 * flow behaves the same as unelaborated flow, except that elaborated flow exists
 * for some specific piece of elaborated design.
 *
 * TBD: if not much reason to keep FLNode around after elaboration, may change
 * the constructor to take a NUUseDefNode instead of a FLNode.
 */
class FLNodeElab
{
public:
  // for MemoryPool
  typedef FLNodeElab* iterator;
  typedef FLNodeElab& reference;
  typedef FLNodeElabSet Set;

  // Type information useful for template code
  typedef FLNodeElabFactory FactoryType;
  typedef NUNetElab DefNetType;

  //! Return the unelaborated flow node.
  FLNode *getFLNode() const { return mFLNode; }

  //! Return the elaborated def net.
  NUNetElab *getDefNet() const { return mNetElab; }

  //! Return the def net ref, of the local net getDefNet()->getNet()
  NUNetRefHdl getDefNetRef(NUNetRefFactory*) const;

  //! Return the language construct.
  virtual NUUseDefNode *getUseDefNode() const;

  //! Return the elaborated hierarchy.
  STBranchNode *getHier() const { return mHier; }

  //! True if this node is a bounded flow node (an input, for example)
  virtual bool isBoundNode() const;

  //! Return the type of this node.
  FLTypeT getType() const;
  
  //! Return true if this node has a nested node and the nesting is because of
  //! hierarchy.
  bool hasNestedHier() const;

  //! Return true if this node has a nested node and the nesting is because of
  //! a block construct.
  bool hasNestedBlock() const;

  //! Return the single nested flow for this node.
  // This will return NULL is there are multiple nested flows!
  FLNodeElab* getSingleNested() const;

  //! Get the number of nested flow nodes.
  UInt32 numNestedFlows() const;

  //! Add this to node's nested hierarchy set.  Not valid to call if hasNestedBlock() is true.
  void addNestedHier(FLNodeElab *nested);

  //! Add this to node's nested block set.  Not valid to call if hasNestedHier() is true.
  void addNestedBlock(FLNodeElab *nested);

  //! Clear our nested block. Used when trying to in-place patch up flow.
  void clearNestedBlock();

  //! Clear all fanin
  virtual void clearFanin();

  //! Is the specified node a fanin for the given sensitivity?
  virtual bool isFanin(FLNodeElab* node, Sensitivity sense) const;

  //! Is the specified node nested under this one?
  bool isNested(FLNodeElab* flow, bool recurse=false) const;

  //! Determine if the provided fanin node is a legal fanin of this node.
  /*!
   * Elaborated flow referencing a NUPortConnection assume that all
   * fanin are legal. This is because their uses may be either in the
   * parent or child module; more work would be needed to perform the
   * proper check (this mirrors FLNode::checkFaninOverlap).
   *
   * Virtual nets allow all fanin. The $extnet flow includes
   * connections to protected-observable nets which are not always
   * modelled in the UD. The module instance flow is also different
   * for virtual nets than for other nets.
   *
   * Edge fanin are not checked during this call. Use
   * checkEdgeFaninOverlap to determine if a FLNodeElab is a legal
   * edge fanin.
   *
   * Note that the elab uses cache should be unique for
   * checkFaninOverlap and checkEdgeFaninOverlap.
   *
   * \sa FLNode::checkFaninOverlap
   * \sa FLNode::checkEdgeFaninOverlap
   * \sa FLNodeElab::checkEdgeFaninOverlap 
   */
  virtual bool checkFaninOverlap(const FLNodeElab * fanin, 
                                 NUNetRefFactory * netRefFactory,
                                 FLElabUseCache* cache);

  //! Determine if the provided fanin node is a legal edge fanin of this node.
  //! Determine if the provided fanin node is a legal edge fanin of this node.
  /*!
   * Only FLNodeElab with an NUAlwaysBlock as its NUUseDefNode node
   * may have edge fanin. Flow with any other type of NUUseDefNode
   * will return false.
   *
   * Unlike checkFaninOverlap, virtual nets ($extnet, $ostnet, $cfnet,
   * etc.) are not blindly allowed.
   *
   * Note that the elab uses cache should be unique for
   * checkFaninOverlap and checkEdgeFaninOverlap.
   *
   * \sa FLNode::checkFaninOverlap
   * \sa FLNode::checkEdgeFaninOverlap
   * \sa FLNodeElab::checkFaninOverlap 
   */
  virtual bool checkEdgeFaninOverlap(const FLNodeElab * fanin,
                                     NUNetRefFactory * netRefFactory,
                                     FLElabUseCache* cache);

  //! Clear all edge fanin
  virtual void clearEdgeFanin();

  //! Add the given fanin node to this node
  virtual void connectFanin(FLNodeElab *fanin_node);

  //! Add all fanin to this node.
  virtual void connectFaninFromLoop(FLNodeElabLoop & loop);

  //! Add the given edge fanin node to this node
  virtual void connectEdgeFanin(FLNodeElab *fanin_node);

  //! Add all edge fanin to this node.
  virtual void connectEdgeFaninFromLoop(FLNodeElabLoop & loop);

  //! Replace the list of given fanins with another list of drivers; used for
  //! aliasing.
  virtual void replaceFaninsWithList(const FLNodeElabList& old_drivers,
                                     const FLNodeElabList &new_drivers);

  //! Replace a fanin node with that node's fanin; used by merging
  virtual void replaceFaninWithFanin(FLNodeElab* fanin);

  //! Change the def net from the old net to the new net.
  virtual void replaceDefNet(NUNetElab *old_net, NUNetElab *new_net);

  //! Change the underlying FLNode.
  virtual void replaceFLNode(FLNode * old_flow, FLNode * new_flow);

  //! Remove driver from both level and edge fanin sets.
  /*!
   * Assert if driver was not found in either the level or edge fanin set.
   */
  virtual void removeFanin(FLNodeElab* driver);

  //! Remove driver from level fanin set.
  /*!
   * Assert if driver was not found in level fanin set.
   */
  virtual void removeLevelFanin(FLNodeElab * driver);

  //! Remove driver from edge fanin set.
  /*!
   * Assert if driver was not found in edge fanin set.
   */
  virtual void removeEdgeFanin(FLNodeElab * driver);

  //! Returns whether this flow node has level fanin
  /*! This is used to see if an async reset has level fanin
   */
  virtual bool hasLevelFanin() const;

  //! Returns whether this flow node has edge-sensitive fanin
  virtual bool hasEdgeFanin() const;

  //! Returns whether this flow node has any fanin
  bool hasFanin() {return hasLevelFanin() || hasEdgeFanin();}

  //! Return all level nodes which fanin to this node.
  virtual FLNodeElabLoop loopFanin();

  //! Return all edge nodes which fanin to this node.
  virtual FLNodeElabLoop loopEdgeFanin();

  //! Abstraction to iterate over the level and edge fanin
  typedef LoopPair<FLNodeElabLoop, FLNodeElabLoop> AllFaninLoop;

  //! Returns an iterator over all fanin for this elaborated flow
  /*! This routine uses the loopFanin() and loopEdgeFanin() routines
   *  to do its work.
   *
   *  Note that duplicates between the level and edge fanin are not
   *  pruned.
   */
  virtual AllFaninLoop loopAllFanin();

  //! Return all of this nodes nested nodes.
  virtual FLNodeElabLoop loopNested();

  //! Return the nodes which fanin to this node and are a def of the given net.
  virtual FLNodeElabSet* getFaninForNet(NUNetElab *net) const;

  //! Create a fanin iterator.  Must be deleted by the caller.
  virtual FLNodeElabIter* makeFaninIter(FLIterFlags::TraversalT visit,
                                        FLIterFlags::TraversalT stop,
                                        FLIterFlags::NestingT nesting,
                                        bool visit_start=false,
                                        FLIterFlags::IterationT iteration=FLIterFlags::eIterNodeOnce);

  //! Create a fanin iterator.  Must be deleted by the caller.
  FLNodeElabIter* makeFaninIter(int visit,
                                int stop,
                                int nesting,
                                bool visit_start=false,
                                FLIterFlags::IterationT iteration=FLIterFlags::eIterNodeOnce)
  {
    return makeFaninIter((FLIterFlags::TraversalT) visit,
                         (FLIterFlags::TraversalT) stop,
                         (FLIterFlags::NestingT) nesting,
                         visit_start,
                         iteration);
  }

  //! Assign the signature for this block. This information includes
  //  the set of clock edges on which this node must be scheduled and
  //  the set of clock edges on which it is sampled.
  void putSignature(const SCHSignature* s) {mSignature = s;}

  //! Return the SCHSignature for this node
  const SCHSignature* getSignature() const {return mSignature;}

  //! Put the # of logic levels between this node and state
  void putDepth(int depth);

  //! Get the # of logic levels between this node and state
  int getDepth() const {return mScheduleDepth;}

  //! Set that this node has been scheduled
  void putSchedulePass(UInt32 pass);

  //! Check if this node has been scheduled
  UInt32 getSchedulePass() const {return mSchedulePass;}

  //! Set a set of scratch flags
  void setScratchFlags(UInt8 flags)
  {
    mScheduleFlags.bits.scratchFlags |= flags;
  }

  //! Clear a set of scratch flags
  void clearScratchFlags(UInt8 flags)
  {
    ASSERT(sizeof(flags) == 1);
    mScheduleFlags.bits.scratchFlags &= (~flags & 0xFF);
  }

  //! Check if a set of scratch flags is set
  bool allScratchFlagsSet(UInt8 flags)
  {
    return (mScheduleFlags.bits.scratchFlags & flags) == flags;
  }

  //! Check if any of a set of scratch flags is set
  bool anyScratchFlagsSet(UInt8 flags)
  {
    return (mScheduleFlags.bits.scratchFlags & flags) != 0;
  }

  //! Set that this node represents a clock.
  void putIsClock(bool val) {mScheduleFlags.bits.isClock = (unsigned int)val;}

  //! Check if this node represents a clock.
  bool isClock() const {return (bool)(mScheduleFlags.bits.isClock);}

  //! Set that this node represents a clock.
  void putIsClockPin(bool val)
  {
    mScheduleFlags.bits.isClockPin = (unsigned int)val;
  }

  //! Check if this node represents a clock.
  bool isClockPin() const {return (bool)(mScheduleFlags.bits.isClockPin);}

  //! Set that this node is in the data path
  void putIsData(bool val) {mScheduleFlags.bits.isData = (unsigned int) val;}

  //! Check if this node is in the data path
  bool isData() {return (bool) mScheduleFlags.bits.isData;}

  //! Set that this node represents a derived clock.
  void putIsDerivedClock(bool val)
  {
    ASSERT(!val || isClock());
    mScheduleFlags.bits.isDerivedClock = (unsigned int)val;
  }

  //! Check if this node represents a derived clock.
  bool isDerivedClock() const
  {
    return (isClock() && (bool)(mScheduleFlags.bits.isDerivedClock));
  }

  //! Set that this node represents a derived clock.
  void putIsPrimaryClock(bool val)
  {
    ASSERT(!val || isClock());
    mScheduleFlags.bits.isDerivedClock = (unsigned int)!val;
  }

  //! Check if this node represents a primary clock.
  bool isPrimaryClock() const
  {
    return (isClock() && !(bool)(mScheduleFlags.bits.isDerivedClock));
  }

  //! Set that this node represents a derived clock logic.
  void putIsDerivedClockLogic(bool val)
  {
    ASSERT(!val || !isClock());
    mScheduleFlags.bits.isDerivedClockLogic = (unsigned int)val;
  }

  //! Check if this node represents a derived clock.
  bool isDerivedClockLogic() const
  {
    return ((bool)(mScheduleFlags.bits.isDerivedClockLogic));
  }

  //! Check if this node has been seen already during clock expression synthesis
  bool isSCHClkExprCycle() const
  {
    return ((bool)(mScheduleFlags.bits.isSCHClkExprCycle));
  }

  //! Set the flag bit to track cycles in clock expression synthesis
  void putIsSCHClkExprCycle(bool isSCHClkExprCycle)
  {
    mScheduleFlags.bits.isSCHClkExprCycle = isSCHClkExprCycle;
  }

  //! Check if this node is ready to schedule
  bool isScheduled() const
  {
    return ((bool)(mScheduleFlags.bits.isScheduled));
  }

  //! Set this node is ready to schedule
  void putIsScheduled(bool isScheduled)
  {
    mScheduleFlags.bits.isScheduled = isScheduled;
  }

  //! Check if this node is in a cycle
  bool isInCyclePath() const
  {
    return ((bool)(mScheduleFlags.bits.isInCyclePath));
  }

  //! Set this node is in a cycle
  void putIsInCyclePath(bool isInCyclePath)
  {
    mScheduleFlags.bits.isInCyclePath = isInCyclePath;
  }

  //! Set whether this node is live logic
  void putIsLive(bool isLive)
  {
    mScheduleFlags.bits.isLive = isLive;
  }

  //! Check if this node is live logic
  bool isLive() const
  {
    return (bool) mScheduleFlags.bits.isLive;
  }

  //! Set whether this node is live logic
  void putIsSampled(bool isSampled)
  {
    FLN_ELAB_ASSERT((!isSampled || !mScheduleFlags.bits.mustBeTransition), this);
    mScheduleFlags.bits.isSampled = isSampled;
  }

  //! Check if this node is live logic
  bool isSampled()
  {
    return (bool) mScheduleFlags.bits.isSampled;
  }

  //! Set whether this node is live logic
  void putMustBeTransition(bool mustBeTransition)
  {
    ASSERT((!mustBeTransition || !mScheduleFlags.bits.isSampled));
    mScheduleFlags.bits.mustBeTransition = mustBeTransition;
  }

  //! Check if this node is live logic
  bool mustBeTransition()
  {
    return (bool) mScheduleFlags.bits.mustBeTransition;
  }

  //! Set whether this node is reachable logic
  void putIsReachable(bool isReachable)
  {
    mScheduleFlags.bits.isReachable = isReachable;
  }

  //! Check if this node is reachable logic
  bool isReachable() const
  {
    return (bool) mScheduleFlags.bits.isReachable;
  }

  //! Set whether this node is inactive logic
  void putIsInactive(bool isInactive)
  {
    mScheduleFlags.bits.isInactive = isInactive;
  }

  //! Check if this node is inactive logic
  bool isInactive()
  {
    return (bool) mScheduleFlags.bits.isInactive;
  }

  //! Dump myself to cout
  virtual void print(bool recurse=true, int indent=0) const;

  //! Dump my name, def-net, and location to cout
  virtual void pname(int indent, bool newline = true) const;

  //! generate a verilog string for the object and place in \a buf
  virtual void compose(UtString* buf, const STBranchNode* scope, int indent, bool recurse) const;

  //! Dump myself to UtIO::cout() for post-processing
  virtual void ppPrint(int indent=0) const;

  //! debug help: return the type of what I'm looking at
  virtual const char* typeStr() const;

  //! Dump the whole fanin tree for human viewing
  void dump(FLNodeElabFactory*, bool stopAtState = false,
            FILE* cout = NULL) const;

  typedef UtSet<const FLNodeElab*> NodeElabSet;
  // helper function for dump() -- if you want to exclude nodes, put them in 'covered'
  void dumpHelper(NodeElabSet* covered, int depth,
                  Sensitivity sense,
                  FLNodeElabFactory*,
                  bool stopAtState,
                  UtOStream& cout) const;

  //! is this node an encapsulated cycle?
  virtual bool isEncapsulatedCycle() const;

  //! Get the cycle usedef node if this is a cycle, NULL otherwise
  virtual const NUCycle* getCycle() const;

  //! Get the cycle usedef node if this is a cycle, NULL otherwise
  virtual NUCycle* getCycle();

  //! get some appropriate canonical name for this node
  const HierName* getName() const;

  static int compare(const FLNodeElab* f1, const FLNodeElab* f2);
  bool operator<(const FLNodeElab& other) const {
    return compare(this, &other) < 0;
  }

  //! Print assertion information for this elaborated flow
  void printAssertInfo(const char* file, int line, const char* exprStr) const;

  //! Find the bits of an elaborated net that are used by a flow's usedef node
  NUNetRefHdl findUsedBits(NUNetRefFactory* netRefFactory, NUNetElab* net);

  //! Determine whether specified flow-node is a true fanin of this
  bool hasBitFanin(FLNodeElab* fanin, FLElabUseCache* cache);

  //! To qualify as a member of a MemoryPool, FLNodeElab must implement putIsAllocated.
  /*! See the comments in src/inc/util/MemoryPool.h. */
  void putIsAllocated(bool flag);

  //! To qualify as a member of a MemoryPool, FLNodeElab must implement isAllocated.
  /*! See the comments in src/inc/util/MemoryPool.h. */
  bool isAllocated() const {return mScheduleFlags.bits.mIsAllocated; }

  //! helper function for findUsedBits and hasBitFanin
  void getUsesHelper(NUNetRefSet* uses);


protected:
  //! Ensure that creation and deletion can only go through FLNodeElabFactory.
  friend class FLNodeElabFactory;
  friend class MemoryPool<FLNodeElab, 4096>;

  //! constructor
  /*!
    \param flnode The corresponding unelaborated flow node.
    \param net The elaborated net this flow defines.
    \param hier The elaborated hierarchy in which this flow node exists.

   * Protected so that only FLNodeElabFactory or derived classes can call.
   */
  FLNodeElab(FLNode *flnode,
             NUNetElab *net,
             STBranchNode *hier);
  
  //! destructor
  /*!
   * Protected so that only FLNodeElabFactory or derived classes can call.
   */
  virtual ~FLNodeElab() {}

  //! helper function for findUsedBits and hasBitFanin
  STBranchNode * getHierarchyHelper(NUNetElab* net);

  //! Corresponding unelaborated flow node.
  FLNode *mFLNode;

  //! Net for which this flow node defines a value.
  NUNetElab *mNetElab;

  //! Hierarchy in which this flow node belongs.
  STBranchNode *mHier;

  //! Set of data fanin nodes
  FLNodeElabSet mFanin;

  //! Set of edge (reset, clock) fanin nodes
  FLNodeElabSet mEdgeFanin;

  //! If this construct has nesting, this is the nested flow node.
  FLNodeElabSet mNested;

  //! A timing signature for this logic component - see the SCHSchedule class.
  const SCHSignature* mSignature;

  //! The flow depth for this logic component -- see the SCHSchedule class.
  short mScheduleDepth;

  UInt16 mSchedulePass;	// A pass counter used to make sure we
                            // don't traverse flow nodes more than
                            // once during various scheduling
                            // passes.

  //! A bitfield so that the scheduler can do its job
  union {
    struct {
      UInt8 scratchFlags;	// A set of scratch flags for each
				// scheduler pass to
				// set/clear/test. It is the
				// responsibility of each pass to
				// clear these flags after they are
				// done.
      UInt8 isClockPin : 1;	// If set, this means that this
				// node feeds a clock pin of a
				// sequential node. This is
				// different than a clock because
				// we alias some of them leaving
				// with fewer clocks than clock
				// pins.

      UInt8 isClock : 1;	// If set, this flow node
				// represents a clock. This can
				// either be a derived clock or
				// primary clock.

      UInt8 isDerivedClock : 1; // If it is a clock then this flag
				// determines if the clock is derived or
				// primary. Otherwise, it has no
				// meaning.

      UInt8 isData : 1;		// If set, this flow node fans into the
				// D input of some flop somewhere or is a PO

      UInt8 isDerivedClockLogic : 1;	// This logic is used to determine
					// the value for a derived clock.

      UInt8 isSCHClkExprCycle : 1;	// if set then this flow has
                                        // already been seen during
                                        // this clock expression
                                        // expansion (loop found).

      UInt8 isScheduled : 1;	// This bit is used to mark nodes
				// that have been assigned to a
				// schedule. This is so that we
				// don't process the nodes more
				// than once but it is also useful
				// for finding nodes that have not
				// been scheduled.
      UInt8 isInCyclePath : 1;	// Used to detect combinational
				// cycles. This is set to indicate the
				// flow node is in the recursive
				// history. If we hit something that is
				// in our history, then we found a
				// cycle.
      UInt8 isLive : 1;    	// Used in the scheduling
                                // mark/sweep algorithm to see
                                // which nodes are reachable
                                // from primary
                                // outputs/bidirects
      UInt8 mustBeTransition: 1;	// If set, this node must be
					// scheduled with its
					// transition mask. This is
					// done for logic driven by
					// blocks, or logic that feeds
					// a visibility point.
      UInt8 isSampled : 1;	// If set, we are scheduling
				// this node based on its
				// sample mask. By default all
				// nodes are transition mask
				// scheduled since not all
				// blocks can even be schedule
				// with their sample mask.
      UInt8 isReachable : 1;	// Used by the factory
                                // mark/sweep to determine
                                // which nodes (including
                                // nesting) are reachable
                                // through primary
                                // outputs/bidirects.
      UInt8 isInactive : 1;	// If set, this is an inactive flop

      //! Cheap way to do this for now, if mNested is non-empty, then if this is true,
      //! the nesting is hierarchical, else its block.
      UInt8 mNestingHier : 1;

      //! used by MemoryPool to detect liveness of a FLNodeElab
      UInt8 mIsAllocated : 1;
    } bits;
    UInt32 flags;               // For fast clearing
  } mScheduleFlags;

private:
  //! Hide copy and assign constructors.
  FLNodeElab(const FLNodeElab&);
  FLNodeElab& operator=(const FLNodeElab&);
};


//! FLNodeBoundElab class
/*!
 * A flow boundary node in some elaborated hierarchy.
 *
 * See the FLNodeBound class for a general description of flow boundary nodes.
 */
class FLNodeBoundElab : public FLNodeElab
{
public:

  //! Will always return 0; use the block node
  NUUseDefNode *getUseDefNode() const;

  //! Invalid to call
  virtual void clearFanin();

  //! Invalid to call
  void connectFanin(FLNodeElab *fanin_node);

  //! Invalid to call
  void connectEdgeFanin(FLNodeElab *fanin_node);

  //! Invalid to call.
  void replaceFaninsWithLoop(const FLNodeElabList& old_drivers,
			     NUNetElab::DriverLoop new_drivers);

  //! Invalid to call
  void replaceFaninWithFanin(FLNodeElab* fanin);

// JDM 3/20/03 -- the replaceDefNet override below had the wrong
// signature (took NUNet*) and was not doing any overriding.  When I
// corrected the signature, langcov/undriven.v started asserting.
// 
//  //! Invalid to call
//  void replaceDefNet(NUNetElab *old_net, NUNetElab *new_net);

  //! Dump myself to cout
  void print(bool recurse=true, int indent=0) const;

  //! Dump myself to UtIO::cout() for post-processing
  void ppPrint(int indent=0) const;

  //! debug help: return the type of what I'm looking at
  virtual const char* typeStr() const;

private:
  //! Ensure that creation and deletion can only go through FLNodeElabFactory.
  friend class FLNodeElabFactory;
  friend class MemoryPool<FLNodeElab, 4096>;

  //! constructor
  /*!
    \param flnode The corresponding unelaborated flow node.
    \param net The elaborated net this flow defines.
    \param hier The elaborated hierarchy in which this flow node exists.

   * Private so that only FLNodeElabFactory can call.
   */
  FLNodeBoundElab(FLNode *flnode,
		  NUNetElab* net,
		  STBranchNode *hier);

  //! destructor
  /*!
   * Private so that only FLNodeElabFactory can call.
   */
  ~FLNodeBoundElab() {}

  //! Hide copy and assign constructors.
  FLNodeBoundElab(const FLNodeBoundElab&);
  FLNodeBoundElab& operator=(const FLNodeBoundElab&);
};


//! class FLFlowSensePair
class FLFlowSensePair
{
public:
  //! constructor
  FLFlowSensePair(FLNodeElab* flow, Sensitivity sense)
  {
    mFlow = flow;
    mSense = sense;
  }

  //! destructor
  ~FLFlowSensePair() {}

  //! Comparison operator to keep sorted list
  bool operator<(const FLFlowSensePair& fsp) const
  {
    return FLNodeElab::compare(mFlow, fsp.mFlow) < 0;
  }

  //! Get the flow
  FLNodeElab* getFlow() const { return mFlow; }

  //! Get the sensitivity
  Sensitivity getSensitivity() const { return mSense; }
private:
  FLNodeElab* mFlow;
  Sensitivity mSense;
};

typedef UtVector<FLFlowSensePair> FLFaninNodes;

enum FlowCycleMode {eFlowIntoCycles, eFlowOverCycles};
  

//! class FaninInternal
/*! This class creates a sorted traversal over the fanin to a flow node.
 */
class FaninInternal : public FLNodeElabLoop::RefCnt
{
#if !pfGCC_2 && !pfMSVC
  using FLNodeElabLoop::RefCnt::operator();
#endif

public:
  typedef FLNodeElab* value_type;

  //! constructor
  FaninInternal(FLNodeElab* flow, unsigned int additionalFlags,
		unsigned int nestingFlags, bool sorted,
                FLNodeElabFactory* factory);

  //! destructor
  ~FaninInternal() {}

/*
  FaninInternal(const FaninInternal& src)
    : mFaninNodes(src.mFaninNodes),
      mIter(src.mIter),
      mFactory(src.Factory)
  {
  }
*/

  bool operator()(value_type* flow)
  {
    if (mIter == mFaninNodes.end())
      return false;
    *flow = **this;
    ++mIter;
    return true;
  }

  bool operator()(const value_type**) { ASSERT (0); return true;}

  //! at end of iteration?
  bool atEnd() const {
#if pfGCC_2
    FaninInternal* fi = const_cast<FaninInternal*>(this);
    return fi->mIter == fi->mFaninNodes.end();
#else
    return mIter == mFaninNodes.end();
#endif
  }

  //! advance through iteration
  void operator++() {++mIter;}

  //! get iterator's current value
  FLNodeElab* operator*() const;

  //! get the sensitivity that caused us to traverse to the current node
  Sensitivity getSensitivity() const {return mIter->getSensitivity();}

private:
  FLFaninNodes mFaninNodes;
  FLFaninNodes::iterator mIter;
  FLNodeElabFactory* mFactory;
};

//! class Fanin
/*!
 */
class Fanin : public FLNodeElabLoop
{
public:
  //! leave factory NULL if you want to descend into cycles
  Fanin(FLNodeElabFactory* factory,
        FLNodeElab* flow,
        FlowCycleMode cycleMode,
        unsigned int additionalFlags = 0,
        unsigned int nestingFlags = 0, bool sorted = true) :
    FLNodeElabLoop(new FaninInternal(flow, additionalFlags, nestingFlags,
                                     sorted,
                                     (cycleMode == eFlowOverCycles)
                                     ? factory : 0))
  {}

  ~Fanin()
  {}


  //! get the sensitivity that caused us to traverse to the current node
  Sensitivity getSensitivity() const
  {
    FaninInternal* fi = static_cast<FaninInternal*>(getBase());
    return fi->getSensitivity();
  }
};

//! class CFanin -- sugar for makeFaninIter so I don't leak -- use Fanin for a non-const version
class CFanin : public FLNodeElabLoop
{
public:
  //! leave factory NULL if you want to descend into cycles
  CFanin(FLNodeElabFactory* factory, 
         const FLNodeElab* flow,
         FlowCycleMode cycleMode,
         unsigned int additionalFlags = 0,
         unsigned int nestingFlags = 0, bool sorted = true) :
    FLNodeElabLoop(new FaninInternal((FLNodeElab*)flow, additionalFlags,
                                     nestingFlags, sorted,
                                     (cycleMode == eFlowOverCycles)
                                     ? factory : 0))
  {}
  
  ~CFanin()
  {}

  //! get the sensitivity that caused us to traverse to the current node
  Sensitivity getSensitivity() const
  {
    FaninInternal* fi = static_cast<FaninInternal*>(getBase());
    return fi->getSensitivity();
  }
};

//! Structure to compare to elaborated flow pointers
struct FLNodeElabCmp
{
  //! constructor
  FLNodeElabCmp() {}

  // compare operator
  bool operator()(const FLNodeElab* flowElab1, const FLNodeElab* flowElab2) const
  {
    return (*flowElab1) < (*flowElab2);
  }
};

#endif
