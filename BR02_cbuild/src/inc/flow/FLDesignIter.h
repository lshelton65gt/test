// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _FL_DESIGN_ITER_H_
#define _FL_DESIGN_ITER_H_

class STSymbolTable;
#include "flow/FLNodeElab.h"

//! FLDesignElabIter class
/*!
 * Iterate over all of a design's elaborated flow.
 */
class FLDesignElabIter :
  public FLNetIterTemplate<NUNetElab,FLNodeElab,FLNodeBoundElab>
{
public:
  //! constructor
  /*!
    \param design The design.
    \param symtab The symbol table used to look up elaboration.
    \param start Masks eModuleLocals and eClockMasters saying whether to start with those
    \param visit Which nodes to visit, during the traversal.  The iterator will
      return these nodes.  If eAll, all nodes are visited.
    \param stop Which nodes to stop the traversal.  Note the iterator will not
      necessarily return these nodes, unless they also match the visit flags.
    \param nesting What to do when find nesting; step into it or over it.
    \param iteration How to control iteration through the fanin.
   */
  FLDesignElabIter(NUDesign *design,
		   STSymbolTable *symtab,
		   FLIterFlags::StartT start,
		   FLIterFlags::TraversalT visit,
		   FLIterFlags::TraversalT stop,
		   FLIterFlags::NestingT nesting,
		   FLIterFlags::IterationT iteration);

  //! destructor
  ~FLDesignElabIter();

private:
  //! Hide copy and assign constructors.
  FLDesignElabIter(const FLDesignElabIter&);
  FLDesignElabIter& operator=(const FLDesignElabIter&);
};


#endif // _FL_DESIGN_ITER_H_
