// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef FLOW_H_
#define FLOW_H_

/*!
  \file
  Declaration file for the flow package.
*/

#include "nucleus/Nucleus.h"
#include "util/UtSet.h"
#include "util/UtStack.h"
#include "util/UtMap.h"
#include "util/UtMultiMap.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/Iter.h"
#include "util/UtArray.h"

// please do not use "using namespace std"

class FLFan;
class FLNode;
class FLNodeBound;
class FLNodeElab;
class FLNodeBoundElab;
class FLDesignElabIter;
class FLNodeFactory;
class FLNodeElabFactory;
class FLNodeElabCycle;

typedef UtHashSet<FLNode*,
                  HashHelper<FLNode*>,
                  HashMgr<FLNode*> >
FLNodeSet;
typedef UtHashSet<FLNodeElab*,
                  HashHelper<FLNodeElab*>,
                  HashMgr<FLNodeElab*> >
FLNodeElabSet;

//! FLIterFlags class -- just a namespace for flags
class FLIterFlags
{
public:
  //! Traversal flags used to indicate visit and stop nodes
  /*!
   * If you add a new flag here then you need to modify the template: helperNodeGetFlags
   *
   */
  enum TraversalT
  {
    eState = 0x1,         //!< Node is a state element
    eCombDriver = 0x2,    //!< Node is driven by combinational logic

    ePrimaryInput = 0x4,  //!< Node is a primary input
    ePrimaryOutput = 0x8, //!< Node is a primary output
    ePrimaryBid = 0x10,   //!< Node is a primary bid
    ePrimaryPorts = ePrimaryInput | ePrimaryOutput | ePrimaryBid,  //!< Node is a primary port
    eLocalInput = 0x20,   //!< Node is a local input
    eLocalOutput = 0x40,  //!< Node is a local output
    eLocalBid = 0x80,     //!< Node is a local bid
    eLocalPorts = eLocalInput | eLocalOutput | eLocalBid,  //!< Node is a local port
    eBound = 0x100,       //!< Node is a Bound node
    eControlFlow =    0x200,    //!< Node is a control flow node


    eHasNestedHier =  0x400,    //!< Node has a nested hierarchy node
    eHasNestedBlock = 0x800,    //!< Node has a nested block node
    eHasNestedAny = eHasNestedHier | eHasNestedBlock,  //!< Node has any type of nesting

    eEdgesOnly =     0x1000,    //!< Only travserse through edge-sensitive nets
    eLevelsOnly =    0x2000,    //!< Only travserse through level-sensitive nets

    eLeafNode    =   0x4000,    //!< Node has no nesting -- useful as a stop flag

    eAll =           0x8000,    //!< Matches all nodes
    eNone =          0x10000    //!< Matches no nodes

  };

  //! Flags to indicate what to do when find a nested node.
  /*!
   * These flags allow stepping into and/or over all nesting constructs.
   * If Over is chosen, then the flow over the nesting will be traversed.
   * If Into is chosen, then the flow inside the nesting will be traversed.
   * If BranchAt is chose, then both the flow inside the nesting and the flow
   * over the nesting will be traversed.
   */
  enum NestingT
  {
    eOverAll = 0,               //!< Step over all nesting
    eIntoNestedHier = 1,        //!< Step into nested hierarchy
    eIntoNestedBlock = 2,       //!< Step into nested blocks
    eBranchAtNestedHier = 4,    //!< Branch at nested hierarchy
    eBranchAtNestedBlock = 8,   //!< Branch at nested blocks
    eIntoAll = eIntoNestedHier | eIntoNestedBlock,             //!< Step into all nesting
    eBranchAtAll = eBranchAtNestedHier | eBranchAtNestedBlock  //!< Branch at all nesting
  };

  //! Flags to indicate how to control the iteration and determine how often we
  //! traverse nodes.
  enum IterationT
  {
    eIterNoControl = 0,      //!< No iteration control, nodes will be visited multiple times, user must control
    eIterNodeOnce = 1,       //!< Guarantee single traversal of all nodes
    eIterFaninPairOnce = 2   //!< Nodes will be visited multiple times, but the std::pair<node,fanout> will be singly-traversed
  };

  enum StartT
  {
    // outputs, bidis, and observables are implicit
    eIOOnly        = 0x0,
    eModuleLocals  = 0x1,
    eClockMasters  = 0x2,
    eAllElaboratedNets = 0x4
  };
}; // class FLIterFlags
  

template<class FlowNode, class FlowNodeBound> class FLIterTemplate;
template<class FlowNode, class FlowNodeBound> class FLSetIterTemplate;
template<class Net, class FlowNode, class FlowNodeBound> class FLNetIterTemplate;

typedef FLIterTemplate<FLNode, FLNodeBound> FLNodeIter;
typedef FLIterTemplate<FLNodeElab, FLNodeBoundElab> FLNodeElabIter;

typedef FLSetIterTemplate<FLNode, FLNodeBound> FLSetIter;
typedef FLSetIterTemplate<FLNodeElab, FLNodeBoundElab> FLSetElabIter;

typedef FLNetIterTemplate<NUNet, FLNode, FLNodeBound> FLNetIter;
typedef FLNetIterTemplate<NUNetElab, FLNodeElab, FLNodeBoundElab> FLNetElabIter;

typedef UtList<FLNode*> FLNodeList;
typedef UtList<FLNode*>::iterator FLNodeListIter;
typedef UtList<FLNode*>::const_iterator FLNodeListConstIter;
typedef UtList<FLNodeElab*> FLNodeElabList;
typedef UtList<FLNodeElab*>::iterator FLNodeElabListIter;
typedef UtList<FLNodeElab*>::const_iterator FLNodeElabListConstIter;
typedef Loop<FLNodeElabSet> FLNodeElabSetLoop;
typedef CLoop<FLNodeElabSet> FLNodeElabSetCLoop;
typedef UtHashSet<FLNodeElab*> FLNodeElabHashSet;
typedef UtHashSet<FLNodeElab*>::iterator FLNodeElabHashSetIter;
typedef UtMap<FLNode*,FLNode*> FLNodeToFLNodeMap;
typedef UtMap<FLNodeElab*,FLNodeElab*> FLNodeElabToFLNodeElabMap;
typedef UtMap<NUNet*,FLNode*> FLNodeMap;
typedef FLNodeMap::iterator FLNodeMapIter;
typedef UtMap<FLNode*,FLNodeElab*> FLNodeElabMap;
typedef FLNodeElabMap::iterator FLNodeElabMapIter;
typedef UtMap<const FLNode*, FLNodeElabSet> FLNodeConstElabMap;
typedef UtMultiMap<NUNet*,FLNode*> FLNodeMultiMap;
typedef FLNodeMultiMap::iterator FLNodeMultiMapIter;
typedef std::pair<FLNodeMultiMapIter,FLNodeMultiMapIter> FLNodeMultiMapRange;
typedef UtMap<FLNodeElab*,NUNetElabSet> FLNodeElabMultiMap;
typedef FLNodeElabMultiMap::iterator FLNodeElabMultiMapIter;
typedef UtHashMap<FLNodeElab*, FLNodeElabCycle*> FLCycleMap;
typedef UtHashMap<FLNodeElabCycle*, FLNodeElab*> FLReverseCycleMap;
typedef Iter<FLNodeElab*> FLNodeElabLoop;
typedef Iter<FLNode*> FLNodeLoop;
typedef UtMap<FLNode*,FLNodeSet > FLNodeNodeSetMap;
typedef FLNodeNodeSetMap::iterator FLNodeNodeSetMapIter;
typedef FLNodeNodeSetMap::const_iterator FLNodeNodeSetMapConstIter;

//! Abstraction for a vector of flow nodes
typedef UtArray<FLNodeElab*> FLNodeElabVector;

//! Iterator over a vector of flow nodes
typedef FLNodeElabVector::const_iterator FLNodeElabVectorIter;

//! Reverse iterator over a vector of flow nodes
typedef FLNodeElabVector::reverse_iterator FLNodeElabVectorRevIter;

//! Abstraction to get a vector of vector of flow nodes
typedef UtVector<FLNodeElabVector> FLNodeElabVectors;

//! Abstraction to iterator over a vector of vector of flow nodes
typedef FLNodeElabVectors::iterator FLNodeElabVectorsIter;

//! Abstraction for a loop over the vector of flows
typedef Loop<FLNodeElabVector> FLNodeElabVectorLoop;

//! Abstraction for a loop over the vector of flows
typedef CLoop<FLNodeElabVector> FLNodeElabVectorCLoop;

//! Abstraction for a vector of flow nodes
typedef UtArray<FLNode*> FLNodeVector;

//! Iterator over a vector of flow nodes
typedef FLNodeVector::const_iterator FLNodeVectorIter;

//! Abstraction to get a vector of vector of flow nodes
typedef UtVector<FLNodeVector> FLNodeVectors;

//! Abstraction to iterator over a vector of vector of flow nodes
typedef FLNodeVectors::iterator FLNodeVectorsIter;

//! Abstraction for a loop over the vector of flows
typedef Loop<FLNodeVector> FLNodeVectorLoop;

//! Abstraction for a loop over the vector of flows
typedef CLoop<FLNodeVector> FLNodeVectorCLoop;

//! Types of flow nodes
enum FLTypeT
{
  eFLNormal       = 0x1,  //!< Normal (non-bound) flow node.
  eFLBoundHierRef = 0x2,  //!< Bound created due to hierarchical reference
  eFLBoundPort    = 0x4,  //!< Bound created due to input or bid port
  eFLBoundProtect = 0x8,  //!< Bound created due to protected net (ie deposit or force)
  eFLBoundUndriven = 0x10, //!< Bound created due to driver not found
  eFLCycle         = 0x20, //!< Encapsulated cycle node (elab only)
  eFLNonHierRef    = ( eFLNormal | eFLCycle | eFLBoundPort | eFLBoundProtect | eFLBoundUndriven ),
  eFLBound      = ( eFLBoundHierRef | eFLBoundPort | eFLBoundProtect | eFLBoundUndriven )
};

#endif
