// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef FLNODEELABCYCLE_H_
#define FLNODEELABCYCLE_H_

#include "flow/FLNodeElab.h"

//! FLNodeElabCycle class
/*!
 * A flow node representing an encapsulated combinational cycle
 *
 * This class adds a pointer to the NUCycle structure that
 * represents the cycle.  It is illegal to manipulate fanin
 * through this class, so many of the parent class's methods are
 * overridden here to disable them.  All fanin information
 * is maintained by the NUCycle and shared across all FLNodeElabCycles
 * in the cycle, and all fanin manipulation should be automatically
 * handled by the NUCycle structure.
 */
class FLNodeElabCycle: public FLNodeElab
{
public:
  virtual ~FLNodeElabCycle();

  //! is this node a encapsulated cycle?
  virtual bool isEncapsulatedCycle() const;

  //! Return the type of this node.
  FLTypeT getType() const;

  //! what type is this object?
  virtual const char* typeStr() const;

  //! return the cycle usedef node
  virtual NUCycle* getCycle();

  //! return the cycle usedef node
  virtual const NUCycle* getCycle() const;

  //! override this to avoid crashing
  virtual NUUseDefNode* getUseDefNode() const;

  //! Always returns false; a cycle is not a bound node
  virtual bool isBoundNode() const;

  //! Clear all fanin - disabled for FLNodeElabCycle
  virtual void clearFanin();

  //! Is the specified node a fanin for the given sensitivity? - disabled for FLNodeElabCycle
  virtual bool isFanin(FLNodeElab* node, Sensitivity sense) const;

  //! Clear all edge fanin - disabled for FLNodeElabCycle
  virtual void clearEdgeFanin();

  //! Add the given level fanin node to this node - disabled for FLNodeElabCycle
  virtual void connectFanin(FLNodeElab *fanin_node);

  //! Add the given edge fanin node to this node - disabled for FLNodeElabCycle
  virtual void connectEdgeFanin(FLNodeElab *fanin_node);

  //! Replace the list of given fanins with another list of drivers; used for
  //! aliasing. - disabled for FLNodeElabCycle
  virtual void replaceFaninsWithLoop(const FLNodeElabList& old_drivers,
                                     NUNetElab::DriverLoop new_drivers);

  //! Replace a fanin node with that node's fanin; used by merging - disabled for FLNodeElabCycle
  virtual void replaceFaninWithFanin(FLNodeElab* fanin);

  //! Remove driver from both level and edge fanin sets. - disabled for FLNodeElabCycle
  /*!
   * Assert if driver was not found in either the level or edge fanin set.
   */
  virtual void removeFanin(FLNodeElab* driver);

  //! Remove driver from level fanin set. - disabled for FLNodeElabCycle
  /*!
   * Assert if driver was not found in level fanin set.
   */
  virtual void removeLevelFanin(FLNodeElab * driver);

  //! Remove driver from edge fanin set. - disabled for FLNodeElabCycle
  /*!
   * Assert if driver was not found in edge fanin set.
   */
  virtual void removeEdgeFanin(FLNodeElab * driver);

  //! Returns whether this flow node has level fanin
  virtual bool hasLevelFanin() const;

  //! Returns whether this flow node has edge-sensitive fanin - always false for FLNodeElabCycle
  virtual bool hasEdgeFanin() const;

  //! Return all level nodes which fanin to this node.
  virtual FLNodeElabLoop loopFanin();

  //! Return all edge nodes which fanin to this node. - always empty for FLNodeElabCycle
  virtual FLNodeElabLoop loopEdgeFanin();

  //! Return the nodes which fanin to this node and are a def of the given net.
  virtual FLNodeElabSet* getFaninForNet(NUNetElab *net) const;

  void print(bool recurse_arg, int indent_arg) const;

  //! get unique cycle index
  UInt32 getCycleIndex() const {return mCycleIndex;}

private:
  //! Ensure that creation and deletion can only go through FLNodeElabFactory.
  friend class FLNodeElabFactory;
  friend class MemoryPool<FLNodeElab, 4096>;

  //! constructor -- used only through factory
  FLNodeElabCycle(NUCycle* node, NUNetElab* net, UInt32 index)
    : FLNodeElab(NULL, net, NULL),
      mCycleIndex(index)
  {
    mNode = node;
  }

  //! The NUCycle that this FLNodeElabCycle is associated with.
  //! All fanin information is taken from the NUCycle.
  NUCycle* mNode;

  //! This index is used to get deterministic sorting of similar cycle nodes
  UInt32 mCycleIndex;
};

#endif // FLNODEELABCYCLE_H_
