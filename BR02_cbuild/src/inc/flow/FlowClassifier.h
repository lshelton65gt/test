// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef _FLOW_CLASSIFIER_H_
#define _FLOW_CLASSIFIER_H_

/*! \file
 * The FLFlowClassifier is used to match flow nodes from the
 * elaborated or unelaborated flow structures with their corresponding
 * use-def nodes in Nucleus.  This can be used, for example, to group
 * the flow nodes from a case statement according to what select
 * condition causes the driver to be used.
 *
 * This file also defines some specialized classes that simplify common
 * classifications for If, Case, etc.
 */

#include "nucleus/NUNetRef.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "flow/Flow.h"
#include "flow/FLNode.h"
#include "util/UtVector.h"
#include "util/UtMap.h"
#include "util/Loop.h"

class NUUseDefNode;

/*! The FLFlowClassifier class encapsulates the logic to classify flow
 *  nodes according to associated use def nodes.
 *
 *  The use def nodes are associated with a key type 'K', which can be
 *  true/false for an If statement, or an NUCaseItem* for a case
 *  statement, or a BDD representing a control expression, or just an
 *  NUUseDefNode*, or any other way that you want to group the flow
 *  nodes.
 *
 *  The 'FT' type should be FLNode for unelaborated flow or FLNodeElab
 *  for elaborated flow.
 *
 *  This classifier supports classifying flows on useDef nodes for
 *  multiple starting flows. This was added to avoid an N*M
 *  performance degradation found with case statements. It allows the
 *  same classifier structure to be used on the same case statement
 *  for multiple defs of the case statement. The visible difference
 *  due to this change is that adding a flow to be classified requires
 *  both the starting flow (eg for the case statement) and the flow to
 *  be classified (eg for the case item).
 *
 *  For examples of how to use this class, (or instead of using this class),
 *  see the IfClassifier, CaseClassifier, etc. also in this file.
 */
template<typename K, typename FT>
class FLFlowClassifier
{
public:
  typedef UtVector<FT*> FlowVector;
  typedef Loop<FlowVector> FlowLoop;

  //! Constructs an empty classifier
  FLFlowClassifier(NUUseDefNode* useDef)
    : mUseDef(useDef), mUseDefMap(), mFlowMap(), mUnclassified(),
      mIsStale(false)
  {
  }

  //! Destructor
  ~FLFlowClassifier() {}

  //! Add a usedef node associated with the specified key
  void addUseDef(K key, NUUseDefNode* node)
  {
    mUseDefMap[node] = key;
    mIsStale = !mUnclassified.empty();
  }

  //! Add a flow node to classify
  /*! Adds a flow pair of parent flow, nested flow to be classified.
   *
   *  \param flowKey the flow for the outer construct.
   *  \param flow the flow to be classified.
   *
   *  Note that it is assumed that the flowKey is the same for all its
   *  potential nested flows to be classified. It is also asssumed
   *  that the flowKey's useDef node is the same for all flowKeys. But
   *  only the latter is checked
   */
  void addFlow(FT* flowKey, FT* flow)
  {
    NU_ASSERT(flowKey->getUseDefNode() == mUseDef, mUseDef);
    mUnclassified.push_back(FlowPair(flowKey,flow));
    mIsStale = true;
  }

  //! Loop over all flow nodes that matched usedef nodes for the given keys
  /*! This iterates over all the nested flows for the key added in
   *  addUseDef and the flowKey added with addFlow.
   */
  FlowLoop loopFlows(K key, FT* flowKey)
  {
    updateFlowMap();
    FlowMapKey fKey(key, flowKey);
    typename FlowMap::iterator p = mFlowMap.find(fKey);
    if (p == mFlowMap.end())
      return FlowLoop();
    else
      return FlowLoop(p->second);
  }

private:
  //! Update the maps so that all flow nodes that can be classified have been processed
  void updateFlowMap()
  {
    if (!mIsStale || mUnclassified.empty())
      return;
    
    // iterate over all unclassified flow nodes, adding
    // to the FlowMap if there is a UseDef pointer match.
    Unclassified stillUnclassified;
    for (UnclassifiedLoop l(mUnclassified); !l.atEnd(); ++l) {
      const FlowPair& pair = *l;
      FT* fanin = pair.second;
      FT* flow = pair.first;
      NUUseDefNode* faninUseDef = fanin->getUseDefNode();
      typename UDMap::iterator p = mUseDefMap.find(faninUseDef);
      if (p == mUseDefMap.end())
        stillUnclassified.push_back(FlowPair(flow, fanin));
      else
      {
        K key = p->second;
        FlowMapKey fKey(key, flow);
        mFlowMap[fKey].push_back(fanin);
      }
    }
    mUnclassified.swap(stillUnclassified);
    
    mIsStale = false;
  }
private:
  typedef UtMap<NUUseDefNode*,K> UDMap;
  typedef std::pair<K, FT*> FlowMapKey;
  typedef UtMap<FlowMapKey,FlowVector> FlowMap; 
  NUUseDefNode* mUseDef;    //!< The usedef being classified
  UDMap mUseDefMap;         //!< The map of use def nodes to their associated keys
  FlowMap mFlowMap;         //!< The map of keys to the flow nodes that match usedefs with that key

  typedef std::pair<FT*, FT*> FlowPair;
  typedef UtVector<FlowPair> Unclassified;
  typedef Loop<Unclassified> UnclassifiedLoop;
  Unclassified mUnclassified; //!< All of the flow nodes that don't match or haven't been tested

  /*! mIsStale is true when there are flow nodes in mUnclassified that
   *  haven't been tested against all known usedef nodes.  It gets set
   *  when flow nodes are added, or when a new usedef is added and
   *  there are unclassified flow nodes.  It gets cleared by updateFlowMap().
   */
  bool mIsStale;            
};

//! A specialized classifier to group flow nodes for if statements according then or else
template<typename FT>
class FLIfClassifier
{
public:
  typedef typename FLFlowClassifier<bool,FT>::FlowLoop FlowLoop;
  FLIfClassifier(FT* flow);
  FlowLoop loopThen(FT* flowKey)
  {
    return mClassifier.loopFlows(true, flowKey);
  }
  FlowLoop loopElse(FT* flowKey)
  {
    return mClassifier.loopFlows(false, flowKey);
  }
private:
  FLFlowClassifier<bool,FT> mClassifier;
};

template<typename FT>
FLIfClassifier<FT>::FLIfClassifier(FT* flow)
  : mClassifier(flow->getUseDefNode())
{
  NUIf* ifStmt = dynamic_cast<NUIf*>(flow->getUseDefNode());
  FLN_ASSERT((ifStmt != NULL), flow);

  // Add the useDef nodes for the then clauses
  for (NUStmtLoop s = ifStmt->loopThen(); !s.atEnd(); ++s)
  {
    NUStmt* stmt = *s;
    mClassifier.addUseDef(true,stmt); // label 'then' use defs
  }

  // Add the useDef nodes for the else clauses
  for (NUStmtLoop s = ifStmt->loopElse(); !s.atEnd(); ++s)
  {
    NUStmt* stmt = *s;
    mClassifier.addUseDef(false,stmt); // label 'else' use defs
  }
  
  // Enter all of the flow nodes to be classified
  for (Iter<FT*> l = flow->loopNested(); !l.atEnd(); ++l)
  {
    FT* nested = *l;
    mClassifier.addFlow(flow, nested);
  }
}

//! A specialized classifier to group flow nodes for case statements according case item
template<typename FT>
class FLCaseClassifier
{
public:
  //! Abstraction for the case classifier
  typedef FLFlowClassifier<NUCaseItem*, FT> Classifier;

  //! Iterator over the classified items
  typedef typename Classifier::FlowLoop FlowLoop;

  //! constructor with an initial flow
  FLCaseClassifier(NUUseDefNode* useDef);

  //! destructor
  ~FLCaseClassifier();

  //! Returns an iterator over classified items
  FlowLoop loopCaseItem(NUCaseItem* caseItem, FT* flowKey)
  {
    return mClassifier.loopFlows(caseItem, flowKey);
  }

  //! Adds another flow for the same case statement
  void classifyFlow(FT* flow);

private:
  Classifier mClassifier;
  typedef UtSet<FT*> Covered;
  Covered* mCovered;
};

template<typename FT>
FLCaseClassifier<FT>::FLCaseClassifier(NUUseDefNode* useDef)
  : mClassifier(useDef)
{
  NUCase* caseStmt = dynamic_cast<NUCase*>(useDef);
  NU_ASSERT( (caseStmt != NULL), useDef);

  // Keep a covered set for flows we have already visited
  mCovered = new Covered;

  // Add the useDef nodes for all the case items
  for (NUCase::ItemLoop l = caseStmt->loopItems(); !l.atEnd(); ++l)
  {
    NUCaseItem* caseItem = *l;
    for (NUStmtLoop s = caseItem->loopStmts(); !s.atEnd(); ++s)
    {
      NUStmt* stmt = *s;
      mClassifier.addUseDef(caseItem,stmt); // match flow to stmts, grouped by case item
    }
  }
} // FLCaseClassifier<FT>::FLCaseClassifier

template<typename FT> FLCaseClassifier<FT>::~FLCaseClassifier()
{
  delete mCovered;
}

template<typename FT> void FLCaseClassifier<FT>::classifyFlow(FT* flow)
{
  // Make sure we haven't done this flow yet
  if (mCovered->find(flow) == mCovered->end()) {
    mCovered->insert(flow);
  } else {
    return;
  }

  // Enter all of the flow nodes to be classified
  for (Iter<FT*> l = flow->loopNested(); !l.atEnd(); ++l)
  {
    FT* nested = *l;
    mClassifier.addFlow(flow, nested);
  }
}


//! A classifier that selects all of the fanin of a flow node that is used in an expression
template<typename FT>
class FLExprFaninClassifier
{
public:
  typedef UtVector<FT*> FlowVector;
  typedef Loop<FlowVector> FlowLoop;
  FLExprFaninClassifier(FT* flow, NUExpr* expr);
  FlowLoop loopExprFanin() { return FlowLoop(mExprFanin); }
private:
  FlowVector mExprFanin; //!< A vector of fanin flow nodes for the expression
};

template<typename FT>
FLExprFaninClassifier<FT>::FLExprFaninClassifier(FT* flow, NUExpr* expr)
{
  // get the netRefs for nets used by the expressions
  NUNetRefFactory tempNetRefFactory;
  NUNetRefSet netRefs(&tempNetRefFactory);
  expr->getUses(&netRefs);
  
  // For each fanin flow node, see if it overlaps a net ref in the NetRefSet.
  // Because the fanin defnet ref can be for a different net than the 
  // netref in the use set (because of elaborated aliasing), we elaborate
  // the net from the use set in the flow's hierarchy to test for
  // compatible nets, then use the overlapsBits test to check for overlap.
  STBranchNode* hier = flow->getHier();
  UtSet<FT*> covered;
  for (Iter<FT*> l = flow->loopFanin(); !l.atEnd(); ++l)
  {
    FT* fanin = *l;
    if (covered.find(fanin) == covered.end()) // discard duplicate fanin
    {
      covered.insert(fanin);
      NUNetElab* faninNetElab = fanin->getDefNet();
      NUNetRefHdl defNetRef = fanin->getDefNetRef(&tempNetRefFactory);

      for (NUNetRefSet::iterator loop = netRefs.begin(); loop != netRefs.end(); ++loop)
      {
        NUNetRefHdl useNetRef = *loop;
        NUNetElab* netElab = useNetRef->getNet()->lookupElab(hier);

        if ((netElab == faninNetElab) && useNetRef->overlapsBits(*defNetRef))
        {
          mExprFanin.push_back(fanin);
          break;  // once we have found an overlap, move on to the next fanin
        }
      }
    }
  }
}

//! A specialized classifier to group flow nodes for enabled drivers according to driver/enable
template<typename FT>
class FLEnabledDriverClassifier
{
public:
  typedef typename FLExprFaninClassifier<FT>::FlowLoop FlowLoop;
  FLEnabledDriverClassifier(FT* flow);
  ~FLEnabledDriverClassifier() { delete mDrivers; delete mEnables; }
  FlowLoop loopContinuousDrivers() { return mDrivers->loopExprFanin(); }
  FlowLoop loopEnables() { return mEnables->loopExprFanin(); }
private:
  FLExprFaninClassifier<FT>* mDrivers; //!< A classifier for driver fanin
  FLExprFaninClassifier<FT>* mEnables; //!< A classifier for enable fanin
};

template<typename FT>
FLEnabledDriverClassifier<FT>::FLEnabledDriverClassifier(FT* flow)
{
  NUEnabledDriver* edStmt = dynamic_cast<NUEnabledDriver*>(flow->getUseDefNode());
  FLN_ASSERT((edStmt != NULL),flow);

  mDrivers = new FLExprFaninClassifier<FT>(flow, edStmt->getDriver());
  mEnables = new FLExprFaninClassifier<FT>(flow, edStmt->getEnable());
}

#endif // _FLOW_CLASSIFIER_H_
