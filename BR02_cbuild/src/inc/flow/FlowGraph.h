// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  Declaration of the FlowGraph class derived from the Graph
  base class.  The FlowGraph is a light-weight adapter class that
  allows generic graph algorithms to be applied to the carbon
  flow graph for a design.
*/

#ifndef _FLOWGRAPH_H_
#define _FLOWGRAPH_H_

template<typename UserEdgeData, typename UserNodeData>
class GenericDigraph;

#if !pfGCC_4_2
#include "util/GenericDigraph.h"
#endif

#include "util/GraphDot.h"
#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"
#include "nucleus/NUModule.h"

// forward class declarations
class NUDesign;
class NUModule;
class STSymbolTable;

template<typename Flow>
class FlowGraph
{
public:
  typedef GenericDigraph<void*,Flow*> GraphType;
  FlowGraph<Flow>(NUDesign* design, STSymbolTable* symtab);
  FlowGraph<Flow>(NUModule* module);
  void addNet(typename Flow::DefNetType* net);
  void addFlow(Flow* flow, bool addNested=true);
  void addFanin(Flow* flow, bool addNested=true);
  void addFanout(Flow* /*flow*/);
  void clear();
  void write(const char* filename);
  static FlowGraph<Flow>* createElab(NUDesign* design, STSymbolTable* symtab);
  static FlowGraph<Flow>* createUnelab(NUModule* module);
  static void placeHolder();
  ~FlowGraph<Flow>();
private:
  typedef UtMap<Flow*, typename GraphType::Node*> NodeMap;
  void update();
  void addEdges(typename GraphType::Node* node, Flow* flow, NodeMap& nodeMap);
private:  
  NUDesign*      mDesign;
  STSymbolTable* mSymtab;
  NUModule*      mModule;
  UtSet<Flow*>   mShow;
  GraphType*     mGraph;
  bool           mStale;
};

typedef enum { eLevel=1, eEdge=2, eNested=4 } EdgeTypeFlag;

//! A derived GraphDotWriter for writing a flow graph in dot format
template<typename Flow>
class FlowGraphDotWriter : public GraphDotWriter
{
 public:
  FlowGraphDotWriter(UtOStream& os) : GraphDotWriter(os) {};
  FlowGraphDotWriter(const UtString& filename) : GraphDotWriter(filename) {};
 private:
  void writeLabel(Graph* g, GraphNode* node);
  void writeLabel(Graph* g, GraphEdge* edge);
};

// provide convenience types for both types of flow
typedef FlowGraph<FLNode>     UnelabFlowGraph;
typedef FlowGraph<FLNodeElab> ElabFlowGraph;

typedef FlowGraphDotWriter<FLNode>     UnelabFlowGRaphDotWriter;
typedef FlowGraphDotWriter<FLNodeElab> ElabFlowGRaphDotWriter;

#endif // _FLOWGRAPH_H_
