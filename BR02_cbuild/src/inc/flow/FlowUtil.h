// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef FLOW_UTIL_H
#define FLOW_UTIL_H

#include "nucleus/NUNetRef.h"

class FLNode;
class STBranchNode;
class NUUseDefNode;

//! Utility class for caching the input used to create a specific flow node.
class NUNetNode 
{
public:
  NUNetNode(const NUNetRefHdl& net_ref, NUUseDefNode * node, bool marker=false);

  bool operator<(const NUNetNode &other) const;

private:
  const NUNetRefHdl mNetRef;
  NUUseDefNode * mNode;
  bool mMarker;
};

//! Utility class for caching the input used to create a specific flow elab node.
class FlowNetHier {
public:
  FlowNetHier(FLNode * flow, NUNetElab * net, STBranchNode * hier);

  bool operator<(const FlowNetHier &other) const;
  
private:
  FLNode * mFlow;
  NUNetElab * mNet;
  STBranchNode * mHier;
};

/*!
  Utility map -- given input to FLNodeFactory->create(), determine if a flow node already exists.
 */
typedef UtMap<const NUNetNode,FLNode*> NUNetNodeToFlowMap;

/*!
  Utility map -- given input to FLNodeElabFactory->create(), determine if a flow node already exists.
*/
typedef UtMap<FlowNetHier,FLNodeElab*> FlowNetHierToFlowElabMap;

#endif
