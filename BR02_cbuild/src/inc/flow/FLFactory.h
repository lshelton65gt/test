// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef FLFACTORY_H_
#define FLFACTORY_H_

#include "flow/Flow.h"
#include "util/MemoryPool.h"
#include "util/LoopPair.h"
#include "util/Loop.h"
#include "flow/FLNodeElabCycle.h"
#include "flow/FLNode.h"

class STBranchNode;
class NUCycle;

/*!
  \file
  Flow factory class declarations.
*/


//! FLFactoryTemplate class
/*!
 * Templatized class to manage unelaborated and elaborated flow node objects.
 *
 * Manage flow node objects; responsible for creation and deletion of flow
 * nodes.
 *
 * Also, presents a simple way to iterate over all flow nodes, even those
 * nodes which may not be connected in the graph (or in a particular walk of
 * the graph).  This allows easy determination of dead nodes, for instance.
 *
 * This is an instantiatable class (instead of static), because it is expected
 * that unconnected graphs of flow nodes will be necessary.
 */
template<class FlowNode, class FlowNodeBound>
class FLFactoryTemplate
{
public:
  //! constructor
  FLFactoryTemplate();

  //! destructor
  virtual ~FLFactoryTemplate();

  //! Destroy a FlowNode.
  virtual void destroy(FlowNode *flnode);

  //! Populate a set of the freed pointers in this factory.
  virtual void getFreeSet(MemoryPoolPtrSet* free_ptrs);

  //! Purge all the flow nodes
  /*! This destroys any allocated flow nodes and resets the memory
   *  pool to the starting size (one chunk). This should be done
   *  between uses of this factory where we exepct the item size to
   *  potentially shrink considerably.
   */
  void purge(void) { mPool.purge(); }

  //! Returns the number of allocated flow nodes in the factory.
  /*! This routine uses the memory pool to indicate how many allocated
   *  flow nodes there are. It is expected to be constant time to get
   *  the information.
   */
  int numAllocated() const { return mPool.numAllocated(); }

  //! print size information about this factory
  void printStats(int indent_arg=0) {
    int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
    for (int i = 0; i < numspaces; i++) {UtIO::cout() << " ";}
    UtIO::cout() << "Number of pool elements: " << mPool.numAllocated() << UtIO::endl;
  }


protected:
  friend class FLNodeElabFactory;

  //! Allocate space for a new flow node.  Does not call constructor.
  void *malloc();

  //! Pool of flow nodes.
  MemoryPool<FlowNode> mPool;

private:
  //! Hide copy and assign constructors.
  FLFactoryTemplate(const FLFactoryTemplate&);
  FLFactoryTemplate& operator=(const FLFactoryTemplate&);
};


//! FLNodeFactory class
/*!
 * Manage unelaborated flow node objects.
 *
 * NOTE: Be sure to keep this interface consistent with SplitFlowFactory.
 */
class FLNodeFactory : public FLFactoryTemplate<FLNode, FLNodeBound>
{
public:
  //! constructor
  FLNodeFactory();

  //! destructor
  ~FLNodeFactory();

  //! Create a FLNode.
  virtual FLNode *create(const NUNetRefHdl &net_ref, NUUseDefNode *node);

  //! Create a FLNodeBound.
  virtual FLNodeBound *createBound(const NUNetRefHdl &net_ref, FLTypeT type);

  //! Use the pool's iterator.
  typedef MemoryPool<FLNode>::iterator iterator;

  //! Return the first node in the pool; STL begin() semantics.
  iterator begin();

  //! Return the end of the pool; STL end() semantics.
  iterator end();

  //! An alternative iteration type supported by both FLNode and FLNodeElab factories
  typedef Loop<MemoryPool<FLNode> > FlowLoop;

  //! Loop over all the FLNode* in the factory
  virtual FlowLoop loopFlows();

  //! dump all flows in factory to to stdout
  void print();

private:
  //! Hide copy and assign constructors.
  FLNodeFactory(const FLNodeFactory&);
  FLNodeFactory& operator=(const FLNodeFactory&);
};


//! FLNodeElabFactory class
/*!
 * Manage elaborated flow node objects.
 *
 * NOTE: Be sure to keep this interface consistent with SplitFlowElabFactory.
 */
class FLNodeElabFactory : public FLFactoryTemplate<FLNodeElab, FLNodeBoundElab>
{
public:
  //! constructor
  FLNodeElabFactory();

  //! destructor
  ~FLNodeElabFactory();

  //! Create a FLNode.
  virtual FLNodeElab *create(FLNode *flnode,
			     NUNetElab *net,
			     STBranchNode *hier);

  //! Create a FLNodeElabCycle based on a flow
  virtual FLNodeElabCycle* createCycle(NUCycle* node, FLNodeElab*, UInt32 index);

  //! Create a FLNodeBound.
  virtual FLNodeBoundElab* createBound(FLNode* flnode,
                                       NUNetElab* net,
                                       STBranchNode* hier);

  typedef Loop<MemoryPool<FLNodeElab> > NodeLoop;

  typedef Loop<MemoryPool<FLNodeElabCycle> > CycleLoop;
  
  typedef LoopPair<NodeLoop, CycleLoop> FlowLoop;

  //! Destroy a FLNodeElab or FLNodeElabCycle (overrides FLFactoryTemplate)
  virtual void destroy(FLNodeElab* flnode);

  //! Loop over all the FLNodeElab* and FLNodeElabCycle* in the factory
  virtual FlowLoop loopFlows();

  //! Loop over all the FLNodeElabCycle* in the factory
  virtual CycleLoop loopCycleFlows();

  //! Is the given flow-node encapsulated in an NUCycle?
  virtual bool isInCycle(FLNodeElab* flow) const;

  //! Get a node whose fanin can be traversed without encountering a cycle
  virtual FLNodeElab* getAcyclicNode(FLNodeElab* flow) const;

  //! Get a node whose fanin may include cyclic paths
  virtual FLNodeElab* getCyclicNode(FLNodeElab* flow) const;

  //! dump all flows in factory to to stdout
  void print();

private:
  //! Hide copy and assign constructors.
  FLNodeElabFactory(const FLNodeElabFactory&);
  FLNodeElabFactory& operator=(const FLNodeElabFactory&);
  FLFactoryTemplate<FLNodeElabCycle, FLNodeElabCycle> mCycleFactory;
  FLCycleMap        mCycleMap;        //!< a map from cyclic nodes to their acylic partners
  FLReverseCycleMap mReverseCycleMap; //!< a map from acyclic nodes to their cyclic partners
};

#endif
