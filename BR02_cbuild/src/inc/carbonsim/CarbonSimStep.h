// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMSTEP_H_
#define __CARBONSIMSTEP_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimStep class.
*/
#endif        //CARBON_EXTERNAL_DOC

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef __CARBONSIMFUNCTIONGEN_H_
#include "carbonsim/CarbonSimFunctionGen.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif


//! CarbonSimStep class
/*!
  This class manages attributes associated with a system step. 
  A step is a transition in the system that is event driven.
*/
class CarbonSimStep : public CarbonSimFunctionGen
{
public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
  /*!
      \param offset The offset value for the step. This parameter
      is used to delay the step until the specified number of ticks
      has elapsed.
      \param initial_value The initial value of the step. The default
      is 0.
  */

  CarbonSimStep(CarbonTime offset, UInt32 initial_value = 0);

  //! Destructor.
  virtual ~CarbonSimStep(void);

  //! Gets the step's current value.
  inline UInt32 getValue(void) const { return CarbonSimFunctionGen::getValue(); };

  //! Gets the step's value at the specified time.
  /*!
      \param tick The time at which to get the step's value.
  */
  UInt32 getValue(CarbonTime tick);

  //! Checks if the step transitions again.
  /*!
      \retval true If the step does transition again.
      \retval false If the step does not transition again.
  */
  virtual bool hasNextTransitionTick(void);

  //! Gets the step's next transition time.
  CarbonTime getNextTransitionTick(void);

 private:
  CarbonSimStep(void);
  CarbonSimStep(const CarbonSimStep&);  
  CarbonSimStep& operator=(const CarbonSimStep&);
};


#endif
