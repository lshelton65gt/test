// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __CARBONSIMTRACE_H_
#define __CARBONSIMTRACE_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimTrace class.
*/

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef __UtObserver_h_
#include "util/UtObserver.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonSimMaster;
class CarbonSimNet;


//! CarbonSimTrace class
/*!
  This class encapsulates information assicated with a trace.
*/
class CarbonSimTrace : public UtObserver<CarbonSimNet>
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  CarbonSimTrace(CarbonSimMaster * master, CarbonSimNet * signal, UInt32 id);

  //! Destructor.
  virtual ~CarbonSimTrace(void);

  //! Trace the value.
  void trace(void);

  //! Get the trace ID.
  inline UInt32 getID(void) const { return mID; };

  //! Print this to std::cout.
  void print(bool verbose = true, UInt32 indent = 0);

  //! Tell the tracepoint that the net changed value. 
  /*!
    This message is sent to the tracepoint when the value
    on the net has changed.  This is part of the UtObserver
    interface.
   */
  void update(CarbonSimNet * net);

 private:
  CarbonSimMaster * mMaster;
  CarbonSimNet * mSignal;
  UInt32 mHitCount;
  const UInt32 mID;

 private:
  CarbonSimTrace(void);
  CarbonSimTrace(const CarbonSimTrace&);  
  CarbonSimTrace& operator=(const CarbonSimTrace&);
};


#endif	//CARBON_EXTERNAL_DOC
#endif
