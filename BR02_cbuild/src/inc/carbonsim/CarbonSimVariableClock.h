// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMVARIABLEBCLOCK_H_
#define __CARBONSIMVARIABLEBCLOCK_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimVariableClock class.
*/
#endif

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#include "carbonsim/CarbonSimClock.h"
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif


//! CarbonSimVariableClock class
/*!
    Each system clock is controlled using a unique CarbonSimVariableClock. 
    The clock must be added to the CarbonSimMaster, which manages 
    the clock and insures that the correct actions take place when 
    the clock transitions.  The CarbonSimVariableClock enhanses the
    CarbonSimClock object by allowing the period and duty cycle to be
    modified during the simulation, as could happen with a clock generator
    inside a device.
*/
class CarbonSimVariableClock : public CarbonSimClock
{
 public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
  /*! A clocking object similar to CarbonSimClock with added 
      functions that allows the clock period and duty cycle to
      be selectively changed during the simulation.

      \param period The period for the clock, in ticks.
      \param duty_cycle The duty cycle for the clock, in ticks.
      \param initial_value The initial value of the clock. The default is 0.
      \param offset The offset value of the clock. The default is 0.
      \sa CarbonSimClock
  */
  CarbonSimVariableClock(CarbonTime period, CarbonTime duty_cycle, UInt32 initial_value = 0, CarbonTime offset = 0);


  //! Updates the clock's period and duty_cycle.
  void setSpeed(CarbonTime period, CarbonTime duty_cycle);

#ifndef CARBON_EXTERNAL_DOC
  //! Destructor.
  virtual ~CarbonSimVariableClock(void);

#endif	//CARBON_EXTERNAL_DOC

 private:
  CarbonSimVariableClock(void);
  CarbonSimVariableClock(const CarbonSimVariableClock&);  
  CarbonSimVariableClock& operator=(const CarbonSimVariableClock&);
};


#endif
