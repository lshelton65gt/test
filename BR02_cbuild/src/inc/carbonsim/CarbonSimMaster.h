// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMMASTER_H_
#define __CARBONSIMMASTER_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimMaster class.
*/
#endif	//CARBON_EXTERNAL_DOC



#include "carbon/carbon_shelltypes.h"
#include "carbon/UtObserver.h"
#include "carbonsim/CarbonSimObjectType.h"
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonSimClock;
class CarbonSimFunctionGen;
class CarbonSimMasterI;
class CarbonSimMemory;
class CarbonSimNet;
class CarbonSimObjectInstance;
class CarbonSimStep;
class CarbonSimTransactor;
class CarbonSimWire;
class UtString;


//! CarbonSimMaster class
/*!
  The CarbonSimMaster class controls the flow of the validation. All 
  validation objects, including Carbon Models, transactors, and other 
  CarbonSim classes can be registered with the CarbonSimMaster. 
  CarbonSimMaster manages the relationships between these models including 
  the execution, dependencies, and flow of data. CarbonSimMaster provides 
  an interface that allows users to start, stop, and interactively step 
  through a validation run. In general, this makes it much easier to 
  control the execution of models in the Carbon validation environment. 
  CarbonSimMaster controls time, and with each step in the validation will 
  automatically increment time to the correct value.
*/

class CarbonSimMaster
{
 public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Use this to create an instance of a CarbonSimMaster class.
  static CarbonSimMaster * create(void);

  //! Destructor.
  virtual ~CarbonSimMaster(void);

  //! Adds a model to the master.  
  /*! This method exists for legacy purposes only and may be removed 
    from a future release.
  */
  void addModel(CarbonObjectID *object, const char * definition_name, const char * instance_name);


  //! Add an object type to the master.  
  /*! Tell the master how to create a new CarbonSim object.  A type
      needs to be added before trying to create an object.
   */
  CarbonSimObjectType * addObjectType(const char * type_name, CarbonSimObjectType::InstanceCreationFunction creation_function);


  //! Adds an instance to the master.
  /*! Add an instance of a Carbon Object to the master.

    \param object_type A valid object type.
    \param instance_name The instance name of the object; the full HDL
    pathname.
    \param db_type Are we loading the full db or just the io?
    \param init_flags Initialization flags.
   */
  CarbonSimObjectInstance * addInstance(CarbonSimObjectType * object_type, const char * instance_name, CarbonDBType db_type = eCarbonFullDB, CarbonInitFlags init_flags = eCarbon_NoFlags);


  //! Adds an alias to the master.
  /*! Use this function to create an alias for a net. An alias can
      be used by other routines, instead of the full HDL path name, 
      to access a net. For example, the program counter can be accessed 
      using the defined alias PC instead of top.U0.U43.regs.PC_reg.

    \param alias The alias for the signal.
    \param signal The full HDL pathname of the signal.
  */
  void addAlias(const char * alias, CarbonSimNet * signal);


  //! Adds the specified clock to the master.
  /*! All clock signals must be added to the CarbonSimMaster to make 
      sure that they toggle correctly.

      \param clk The clock to be added.
  */
  void addClock(CarbonSimClock *clk);

  //! Adds the specified step to the master.
  /*! All step function signals must be added to CarbonSimMaster to
      insure that the step occurs at the correct time.

      \param stp The step to be added.
  */
  void addStep(CarbonSimStep *stp);

  //! Adds the specified wire to the master.
  /*! Use this function to add \e all wires to CarbonSimMaster. This
      will insure that interconnect traffic flows at the correct time.

      \param wire The wire to be added.
  */
  void addWire(CarbonSimWire *wire);

  //! Adds the specified transactor to the master.
  /*! Use this function to add \e all transactors in the system.

     \param transactor The transactor to be added.
  */
  void addTransactor(CarbonSimTransactor *transactor);

  //! Gets the current tick.
  inline CarbonTime getTick(void) const { return mCurrentTick; };

  //! Runs the validation until interrupted.
  /*! If you use the interrupt() function to stop the validation run,
      you must recall this function to resume the run.

      \sa interrupt()
  */
  void run(void);

  //! Runs the validation until all transactors have interrupted
  /*!
    All transactors must use tallyInterrupt(). 
    
    \param cumulative If true, once all the transactors have fired
    this returns. If false, this will only return if all the
    transactors fire on the same time step.

    \sa tallyInterrupt()
  */
  void runTransactors(bool cumulative);

  //! Runs the validation for the specified number of ticks, until interrupted.
  /*! If you use the interrupt() function to stop the validation run, 
      you must recall a run() function to resume it. If you reissue
      run() without specifying delta_ticks, the validation will run
      to the end.

      \param delta_ticks The number of ticks to run before stopping 
      the validation.
      \param ignoreInterrupts If true, this will definitely run for
      delta_ticks or until the simulation finishes, whichever happens
      first. Interrupts by transactors will be ignored. By default,
      interrupts stop the run at the point of interrupt.
  */
  void run(CarbonTime delta_ticks, bool ignoreInterrupts = false);

  //! Interrupts a validation run.
  /*! If you use this function to interrupt a validation run, you 
      must call the run() function to resume it.

      \sa run()
  */
  void interrupt(void);

  //! Tallies an interrupt from the transactor
  /*!
    Once all transactors have interrupted a runTransactors() call,
    runTransactors() will return. This also has the same functionality
    as interrupt(). If run() was called then this will interrupt run()
    immediately.

    \sa runTransactors()
  */
  void tallyInterrupt(CarbonSimTransactor* transactor);

  //! Finishes a validation run.
  /*! Once finish() is called, the validation run cannot be restarted.    

      \sa interrupt()
   */
  void finish(void);

  //! Checks if the validation finished.
  /*!
      \retval true If the validation run is finished.
      \retval false If the validation run is not finished.
  */
  bool isFinished(void);

  //! Steps the validation to the next clock edge.
  void step(void);

  //! Steps the validation the specified number of ticks, or to the next clock edge.
  /*! 
      \param delta_ticks The number of ticks to step through.
   */
  void step(CarbonTime delta_ticks);

  //! Finds the specified net.
  /*! 
      \param path_name The full HDL pathname for the net. The name may be:
      \li full vector - bit references and ranges are ignored. Note that
      if a vector bit reference of sub range is specified, the entire vector
      range will get passed back.
      \li scalar
   */
  CarbonSimNet * findNet(const char * path_name);


  //! Creates the specified net.
  /*! 
      This is used to create CarbonSimNet objects that will be examined
      that are not contained within a Carbon Object.

      \param path_name The full HDL pathname for the net.
      \param msb The msb of this net.  Omit for a scalar.
      \param lsb The lsb of this net.  Omit for a scalar.
   */
  CarbonSimNet * allocInputNet(const char * path_name, SInt32 msb = 0, SInt32 lsb = 0);


  //! Creates the specified net.
  /*! 
      This is used to create CarbonSimNet objects that will be deposited
      to that are not contained within a Carbon Object.

      \param path_name The full HDL pathname for the net.
      \param msb The msb of this net.  Omit for a scalar.
      \param lsb The lsb of this net.  Omit for a scalar.
   */
  CarbonSimNet * allocOutputNet(const char * path_name, SInt32 msb = 0, SInt32 lsb = 0);


  //! Finds a net by its alias.
  /*! 
      \param alias The alias for the net.
   */
  CarbonSimNet * findNetByAlias(const char * alias) const;


  //! Finds the specified memory.
  /*! 
      \param path_name The full HDL path name for the memory.
   */
  CarbonSimMemory * findMemory(const char * path_name);


  //! Stores the specified wire.
  /*! 
    \param wire The wire to store in this object
    \param wire_signal_name The full HDL pathname for a net that
    is connected to the wire.
  */
  void storeWire(CarbonSimWire * wire, const char * wire_signal_name);


  //! Finds the specified wire.
  /*! 
      \param wire_signal_name The full HDL pathname for a net that
      is connected to the wire.
   */
  CarbonSimWire * findWire(const char * wire_signal_name) const;


#ifndef CARBON_EXTERNAL_DOC
  //! Prints this to UtIO::cout().
  /*!
      \param verbose
      \param indent
  */
  void print(bool verbose = true, UInt32 indent = 0);
#endif

  //! Notifies the specified observer when the validation run is finished.
  /*!
      \param observer The observer to notify.
      \sa removeNotifyOnFinish()
   */
  void addNotifyOnFinish(UtObserver<CarbonSimMaster> * observer);

  //! Discontinues notifying the specified observer when the validation run is finished.
  /*!
     \param observer The observer to notify.
     \sa addNotifyOnFinish()
   */
  void removeNotifyOnFinish(UtObserver<CarbonSimMaster> * observer);

  //! Notifies \e all observers when the validation run is finished.
  /*!
      \sa addNotifyOnFinish()
  */
  void notifyFinish(void);

  //! Finds the specified object instance.
  /*! This function returns a pointer to the object for the specified
      instance name.

      \param instance_name The instance name for the object.
  */
  CarbonSimObjectInstance * findInstance(const char * instance_name) const;


 private:
  //! Runs internal validation step.
  void schedule(CarbonTime tick_time);

  //! Finds the next function edge.
  /*! Finds the next edge in the set of function generators. If none
  **  
  **  \retval false If none of the function generators has an edge.
  */
  bool findNextFunctionEdgeTick(CarbonTime * next_tick);

  //! Allocates a new CarbonSimNet.
  CarbonSimNet * createNet(const char * path_name);

  //! Looks up a cached net.
  CarbonSimNet * findCachedNet(const char * path_name) const;

  //! Caches a net.
  void storeCachedNet(CarbonSimNet * signal);

  //! Allocates a new CarbonSimMemory.
  CarbonSimMemory * createMemory(const char * path_name);

  //! Looks up a cached memory.
  CarbonSimMemory * findCachedMemory(const char * path_name) const;

  //! Caches a memory.
  void storeCachedMemory(CarbonSimMemory * signal);

  //! Convert CarbonSim type names to shell type names.
  bool convertCarbonSimNameToShellName(const char * path_name, CarbonSimObjectInstance ** object, UtString * shell_name);

 private:
  CarbonTime mCurrentTick;
  const CarbonTime mMaxTick;
  CarbonSimMasterI * mPrivate;
  bool mInterrupted;
  bool mFinished;
  bool mIsInitialStep;

 private:
  CarbonSimMaster(void);
  CarbonSimMaster(const CarbonSimMaster&);  
  CarbonSimMaster& operator=(const CarbonSimMaster&);
};


#endif
