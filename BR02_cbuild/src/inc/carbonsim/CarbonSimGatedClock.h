// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMGATEDBCLOCK_H_
#define __CARBONSIMGATEDBCLOCK_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimGatedClock class.
*/
#endif

#include "carbon/carbon_shelltypes.h"
#include "carbonsim/CarbonSimClock.h"
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif


//! CarbonSimGatedClock class
/*!
    Each system clock is controlled using a unique CarbonSimGatedClock. 
    The clock must be added to the CarbonSimMaster, which manages 
    the clock and insures that the correct actions take place when 
    the clock transitions.
*/
class CarbonSimGatedClock : public CarbonSimClock
{
 public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
  /*! A clocking object similar to CarbonSimClock with an added 
      gating function that allows the clock to be selectively 
      enabled and disabled.

      \param period The period for the clock, in ticks.
      \param duty_cycle The duty cycle for the clock, in ticks.
      \param initial_value The initial value of the clock. The default is 0.
      \param offset The offset value of the clock. The default is 0.
      \sa CarbonSimClock
  */
  CarbonSimGatedClock(CarbonTime period, CarbonTime duty_cycle, UInt32 initial_value = 0, CarbonTime offset = 0);


  //! Enables the clock to toggle.
  inline void enable(void) { mEnabled = true; };

  //! Disables the clock from toggling.
  inline void disable(void) { mEnabled = false; };

  //! Gets the status of the clock enable.
  inline bool isEnabled(void) { return mEnabled; };

#ifndef CARBON_EXTERNAL_DOC
  //! Destructor.
  virtual ~CarbonSimGatedClock(void);

  //! Gets the clock's period.
  inline CarbonTime getPeriod(void) const { return mPeriod; }

  //! Gets the clock's duty cycle.
  inline CarbonTime getDutyCycle(void) const { return mDutyCycle; }

  //! Checks if the clock transitions again.
  inline bool hasNextTransitionTick(void) { return mEnabled; };

  //! Gets the clock's next transition time.
  virtual CarbonTime getNextTransitionTick(void);
#endif	//CARBON_EXTERNAL_DOC

 private:
  bool mEnabled;

 private:
  CarbonSimGatedClock(void);
  CarbonSimGatedClock(const CarbonSimGatedClock&);  
  CarbonSimGatedClock& operator=(const CarbonSimGatedClock&);
};


#endif
