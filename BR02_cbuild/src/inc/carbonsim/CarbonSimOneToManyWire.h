// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMONETOMANYWIRE_H_
#define __CARBONSIMONETOMANYWIRE_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimOneToManyWire class.
*/
#endif

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef __CARBONSIMWIRE_H_
#include "carbonsim/CarbonSimWire.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonSimNet;
class CarbonSimOneToManyWireI;


//! CarbonSimOneToManyWire class
/*!
  CarbonSimOneToManyWire provides an interface for connecting multiple
  Carbon Models to each other. This class is bound to the Carbon Models 
  to be connected, and must be registered with the CarbonSimMaster. If 
  the Carbon Models are distributed across multiple processes or machines, 
  CarbonSimOneToManyWire will manage the connection without any change 
  in the interface that is called by the user. The abstraction that 
  CarbonSimOneToManyWire provides creates a highly flexible and portable 
  environment for linking to Carbon Models.
*/
class CarbonSimOneToManyWire : public CarbonSimWire
{
 public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
  /*! Declares a wire between a net and multiple destinations.
      This function should be used in conjunction with addDestination()
      to create a one-to-many relationship between nets.

      \param src The source net of the wire.
      \param pull The pull of the source net. A net can be pulled up, 
      pulled down, or not pulled at all (the default). If the net has 
      no driver, this parameter indicates the pull mode of that net.
  */
  CarbonSimOneToManyWire(CarbonSimNet * src, CarbonSimWire::PullType pull = eCarbonSimWirePullNone);

  //! Destructor.
  virtual ~CarbonSimOneToManyWire(void);

  //! Adds a destination for this wire.
  /*! This function may be called multiple times to declare each 
      destination of the wire.

      \param dst The destination net of the wire. This may be any 
      legal net defined in the system that can be driven.
  */
  void addDestination(CarbonSimNet * dst);


#ifndef CARBON_EXTERNAL_DOC
  // Causes the data to move.  
  virtual void flow(void);

  //! Prints this to UtIO::cout().
  virtual void print(bool verbose = true, UInt32 indent = 0);
#endif


 private:
  CarbonSimNet * mSrc;
  CarbonSimOneToManyWireI * mPrivate;

 private:
  CarbonSimOneToManyWire(void);
  CarbonSimOneToManyWire(const CarbonSimOneToManyWire&);  
  CarbonSimOneToManyWire& operator=(const CarbonSimOneToManyWire&);
};


#endif
