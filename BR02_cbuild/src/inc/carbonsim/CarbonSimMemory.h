// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMMEMORY_H_
#define __CARBONSIMMEMORY_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimMemory class.
*/
#endif	//CARBON_EXTERNAL_DOC


#include "carbon/carbon_shelltypes.h"
#include "carbonsim/CarbonSimObjectInstance.h"
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif


class UtString;

//! CarbonSimMemory class
/*!
  CarbonSimMemory provides a uniform method for accessing CarbonMemory 
  objects in the Validation Manger API. All routines in this class 
  correspond directly to DesignPlayer API functions.
*/
class CarbonSimMemory
{
 public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
  /*!
      \param object A valid object ID.
      \param memory A valid memory ID.
      \param name Full HDL pathname of the memory.
  */

  CarbonSimMemory(CarbonSimObjectInstance * object, CarbonMemory * memory, const char * name);

  //! Destructor.
  virtual ~CarbonSimMemory(void);

  //! Gets Carbon Model.
  inline CarbonObjectID * getCarbonObject(void) const { return mObject->getCarbonObject(); };
  
  //! Gets Carbon memory.
  inline CarbonMemory * getCarbonMemory(void) const { return mMemory; };

  //! Gets Carbon memory name.
  const char * getName(void) const;

  //! Returns the low address of this memory.
  virtual SInt64 getLowAddress(void) const;

  //! Returns the high address of this memory.
  virtual SInt64 getHighAddress(void) const;

  //! Returns the width of the memory.
  virtual UInt32 getRowBitWidth(void) const;

  //! Returns the total number of UInt32s needed to represent a row of data.
  virtual UInt32 getRowNumUInt32s(void) const;

  //! Returns the Least Significant Bit of row.
  virtual SInt32 getRowLSB(void) const;

  //! Returns the Most Significant Bit of row.
  virtual SInt32 getRowMSB(void) const;

  //! Writes memory row from array of UInt32s.
  /*!
      \param address A row index into the memory.
      \param buf The value to deposit.
  */
  virtual CarbonStatus deposit(SInt64 address, const UInt32 *buf);

  //! Examines row at memory address.
  /*!
      \param address A row index into the memory.
      \param buf The return value.
  */
  virtual CarbonStatus examine(SInt64 address, UInt32 * buf) const;

  //! Formats a memory.
  /*!
      \param value The buffer in which to put the string.
      \param len The length of the buffer.
      \param radix The radix for formatting (binary, octal, hexadecimal, decimal).
      \param address The address of the memory to format.
  */
  virtual CarbonStatus format(char * value, int len,
                              CarbonRadix radix, SInt64 address) const;
 
  //! Reads a file of hex data into a memory.
  /*!
      \param filename File from which to load data.
  */
  virtual CarbonStatus readmemh(const char *filename);
 
  //! Reads a file of binary data into a memory.
  /*!
      \param filename File from which to load data.
  */
  virtual CarbonStatus readmemb(const char *filename);


 private:
  CarbonSimObjectInstance * mObject;
  CarbonMemory * mMemory;
  UtString * mName;

 private:
  CarbonSimMemory(void);
  CarbonSimMemory(const CarbonSimMemory&);  
  CarbonSimMemory& operator=(const CarbonSimMemory&);
};


#endif
