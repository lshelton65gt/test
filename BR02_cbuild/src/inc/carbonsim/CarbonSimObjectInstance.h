// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMOBJECTINSTANCE_H_
#define __CARBONSIMOBJECTINSTANCE_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimObjectInstance class.
*/
#endif


#include "carbon/carbon_shelltypes.h"
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif


class CarbonSimObjectInstanceI;
class CarbonSimObjectType;


//! CarbonSimObjectInstance class
/*!
  This class provides a C++ frontend to the C API functions for manipulating
  CarbonObjectIDs.  All routines here correspond directly to C API 
  routines.
*/
class CarbonSimObjectInstance
{
public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
  CarbonSimObjectInstance(const char * instance_name, CarbonSimObjectType * object_type, CarbonDBType db_flags, CarbonInitFlags init_flags);

  //! Destructor.
  virtual ~CarbonSimObjectInstance(void);


  //! Get the instance name of this object.
  const char * getName(void) const;

  //! Get the type of this object.
  inline CarbonSimObjectType * getType(void) const
  {
    return mType;
  }

  //! Get the encapsulated Carbon Model.
  inline CarbonObjectID * getCarbonObject(void) const
  {
    return mCarbonObject;
  }

  //! Cause scheduling to occur.
  CarbonStatus schedule(CarbonTime tick_time);

private:
  CarbonSimObjectType * mType;
  
public:
  // Needed for carbon_replay_system integration.
  CarbonObjectID * mCarbonObject;

private:
  CarbonSimObjectInstanceI * mPrivate;
  
private:
  CarbonSimObjectInstance(void);
  CarbonSimObjectInstance(const CarbonSimObjectInstance&);  
  CarbonSimObjectInstance& operator=(const CarbonSimObjectInstance&);
};


#endif
