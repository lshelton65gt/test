// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#ifndef __CARBONSIMBREAKPOINT_H_
#define __CARBONSIMBREAKPOINT_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimBreakpoint class.
*/

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef __UtObserver_h_
#include "carbon/UtObserver.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonSimMaster;
class CarbonSimNet;


//! CarbonSimBreakpoint class
/*!
  This class encapsulates information assicated with a breakpoint.
*/
class CarbonSimBreakpoint : public UtObserver<CarbonSimNet>
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  CarbonSimBreakpoint(CarbonSimMaster * master, CarbonSimNet * signal, UInt32 * value, UInt32 id);

  //! Destructor.
  virtual ~CarbonSimBreakpoint(void);

  //! Check break condition.
  void check(void);

  //! Get the breakpoint ID.
  inline UInt32 getID(void) const { return mID; };

  //! Print this to UtIO::cout().
  void print(bool verbose = true, UInt32 indent = 0);

  //! Tell the breakpoint that the net changed value. 
  /*!
    This message is sent to the breakpoint when the value
    on the net has changed.  This is part of the UtObserver
    interface.
   */
  void update(CarbonSimNet * net);

 private:
  CarbonSimMaster * mMaster;
  CarbonSimNet * mSignal;
  SInt32 mNumWords;
  UInt32 * mBreakValue;
  UInt32 * mCurrentValue;
  UInt32 mHitCount;
  const UInt32 mID;

 private:
  CarbonSimBreakpoint(void);
  CarbonSimBreakpoint(const CarbonSimBreakpoint&);  
  CarbonSimBreakpoint& operator=(const CarbonSimBreakpoint&);
};


#endif	//CARBON_EXTERNAL_DOC
#endif	//__CARBONSIMBREAKPOINT_H
