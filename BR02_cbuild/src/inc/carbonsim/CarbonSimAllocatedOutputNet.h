// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMALLOCATEDOUTPUTNET_H_
#define __CARBONSIMALLOCATEDOUTPUTNET_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimAllocatedOutputNet class.
*/
#endif


#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef __CARBONSIMALLOCATEDNET_H_
#include "carbonsim/CarbonSimAllocatedNet.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif




//! CarbonSimAllocatedOutputNet class
/*!
*/
class CarbonSimAllocatedOutputNet : public CarbonSimAllocatedNet
{
 public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
  CarbonSimAllocatedOutputNet(const char * name, SInt32 msb = 0, SInt32 lsb = 0);


  //! Destructor.
  virtual ~CarbonSimAllocatedOutputNet(void);


  //! Checks if this net is an input.
  /*!
      \retval true If the net is a scalar.
      \retval false If the net is not a scalar.
  */
  virtual bool isInput(void) const;

  //! Checks if this net is an output.
  /*!
      \retval true If the net is an output.
      \retval false If the net is not an output.
  */
  virtual bool isOutput(void) const;

  //! Checks if this net is a bidirect.
  /*!
      \retval true If the net is a bidirect.
      \retval false If the net is not a bidirect.
  */
  virtual bool isBidirect(void) const;

  //! Checks if this net is a tristate.
  /*!
      \retval true If the net is a tristate.
      \retval false If the net is not a tristate.
  */
  virtual bool isTristate(void) const;


  //! Checks if this net is driving logic.
  /*!
      \retval true If the net is driving logic.
      \retval false If the net is not driving logic.
  */
  virtual bool isDriving(void) const;

  //! Checks if this net is depositable
  /*!
    \retval true If the net is depositable.
    \retval false If the net is not depositable.
  */
  virtual bool isDepositable(void) const;


 private:
  CarbonSimAllocatedOutputNet(void);  
  CarbonSimAllocatedOutputNet(const CarbonSimAllocatedOutputNet&);  
  CarbonSimAllocatedOutputNet& operator=(const CarbonSimAllocatedOutputNet&);
};


#endif
