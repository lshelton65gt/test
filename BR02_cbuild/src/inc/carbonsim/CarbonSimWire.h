// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMWIRE_H_
#define __CARBONSIMWIRE_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimWire class.
*/
#endif

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonSimNet;


//! CarbonSimWire class
/*!
  CarbonSimWire provides an interface for connecting multiple Carbon 
  Models to each other. This class is bound to the ports on the models
  to be connected, and must be registered with the CarbonSimMaster. If 
  the Carbon Models are distributed across multiple processes or machines, 
  CarbonSimWire will manage the connection without any change in the 
  interface that is called by the user. The abstraction that CarbonSimWire 
  provides creates a highly flexible and portable environment for linking 
  to Carbon Models.
*/
class CarbonSimWire
{
public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Pull specification enumeration
  enum PullType {
    eCarbonSimWirePullNone, //!< No pull
    eCarbonSimWirePullUp, //!< Pull up
    eCarbonSimWirePullDown, //!< Pull down
    eCarbonSimWirePullINVALID //!< Invalid
  };

 public: 
  //! Constructor.
  CarbonSimWire(CarbonSimWire::PullType pull);

  //! Destructor.
  virtual ~CarbonSimWire(void);


#ifndef CARBON_EXTERNAL_DOC
  // Causes the data to move.
  virtual void flow(void) = 0;

  //! Prints this to UtIO::cout().
  virtual void print(bool verbose = true, UInt32 indent = 0);
#endif


protected:

  //! Allocate internal copy buffers.
  /*
    Allocate and initialize that will be used to move data between the nets connected
    to the wire as well as any buffers that will be uaed to propogate pull information.
   */
  void allocBuffers(CarbonSimNet * net);
  void deleteBuffers(void);
  static UInt32 * allocCopyBuffer(CarbonSimNet * net);
  static UInt32 * allocPullDataBuffer(CarbonSimNet * net, CarbonSimWire::PullType pull);
  static UInt32 * allocPullDriveBuffer(CarbonSimNet * net, CarbonSimWire::PullType pull);
  static UInt32 * allocUndriveDriveBuffer(CarbonSimNet * net);
  static void clearBits(UInt32 * b, UInt32 num_words);
  static void setBits(UInt32 * b, UInt32 num_words, UInt32 num_set_bits);

protected:
  CarbonSimWire::PullType mPull;
  UInt32 * mDriveData;
  UInt32 * mDriveDrive;
  UInt32 * mPullData;
  UInt32 * mPullDrive;
  UInt32 * mUndriveDrive;


 private:
  CarbonSimWire(void);
  CarbonSimWire(const CarbonSimWire&);  
  CarbonSimWire& operator=(const CarbonSimWire&);
};


#endif
