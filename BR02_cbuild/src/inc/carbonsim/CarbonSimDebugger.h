// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMDEBUGGER_H_
#define __CARBONSIMDEBUGGER_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimDebugger class.
*/


#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonSimBreakpoint;
class CarbonSimMaster;
class CarbonSimNet;
class CarbonSimTrace;
class CarbonSimTransactorAdaptor;
class CarbonSimDebuggerI;


//! CarbonSimDebugger class
/*!
  This is the top level debugger object.
*/
class CarbonSimDebugger
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  CarbonSimDebugger(CarbonSimMaster * master);

  //! Destructor.
  virtual ~CarbonSimDebugger(void);

  //! Start the debugger.
  inline CarbonSimMaster * getSimMaster(void) const { return mMaster; };

  //! Add a transactor adaptor to the debugger.
  void addTransactorAdaptor(const char * adaptor_name, CarbonSimTransactorAdaptor * adaptor);

  //! Find the named transactor adaptor.
  CarbonSimTransactorAdaptor * findTransactorAdaptor(const char * adaptor_name) const;

  //! Start the debugger.
  inline void start (void) { mRunning = true; };

  //! Start the debugger.
  inline void stop (void) { mRunning = false; };

  //! Is the debugger running?
  inline bool running (void) const { return mRunning; };
  
  //! Execute quit.
  virtual void doQuit(void);
  
  //! Execute a single step.
  virtual void doStep(void);

  //! Execute indefinitely.
  virtual void doRun(void);

  //! Execute up to the given tick.
  virtual void doRun(CarbonTime end_tick);

  //! Continue execution.
  virtual void doContinue(void);

  //! Peek at the value of the specified signal.
  virtual void doPeek(CarbonSimNet * signal);

  //! Poke a new value on the specified signal.
  virtual void doPoke(CarbonSimNet * signal, UInt32 * new_value);

  //! Add a breakpoint to the specified signal.
  virtual void doAddBreakpoint(CarbonSimNet * signal, UInt32 * break_value);

  //! Add a trace to the specified signal.
  virtual void doAddTrace(CarbonSimNet * signal);

  //! Add an alias for the specified signal.
  void doAddAlias(const char * alias, CarbonSimNet * signal);

  //! Display the current tick.
  virtual void doShowTick(void);

  //! Print this to UtIO::cout().
  void print(bool verbose = true, UInt32 indent = 0);

 private:
  CarbonSimDebuggerI* mPrivate;
  CarbonSimMaster * mMaster;
  UInt32 mBreakpointIDCount;
  UInt32 mTraceIDCount;
  bool mRunning;

 private:
  CarbonSimDebugger(void);
  CarbonSimDebugger(const CarbonSimDebugger&);  
  CarbonSimDebugger& operator=(const CarbonSimDebugger&);
};


#endif	//CARBON_EXTERNAL_DOC
#endif
