// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMTRANSACTION_H_
#define __CARBONSIMTRANSACTION_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimTransaction class.
*/
#endif


#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif


class CarbonSimTransactor;


//! CarbonSimTransaction class
/*! Every transactor will consist of a number of transactions. This 
    class provides a uniform method of adding and calling transactions. 
    A transaction is a single operation. For example, the bits of
    a PCI read operation are combined to make \e one transaction.
    This class provides a level of abstraction that allows the
    validation to run faster.
*/
class CarbonSimTransaction
{
 public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
 CarbonSimTransaction(void) {}

  //! Destructor.
  virtual ~CarbonSimTransaction(void) {}

  //! Steps the transaction the specified number of ticks.
  /*!
      \param transactor The transactor to which this transaction 
      belongs.
      \param tick The number of ticks to step through.
  */
  virtual void step(CarbonSimTransactor * transactor, CarbonTime tick) = 0;

  //! Checks if the transaction is complete.
  /*!
      \retval true If the transaction is complete.
      \retval false If the transaction is not complete.
  */
  virtual bool isDone(void) = 0;

#ifndef CARBON_EXTERNAL_DOC
  //! Prints this to UtIO::cout().
  void print(bool verbose = true, UInt32 indent = 0) {}
#endif

 private:
  CarbonSimTransaction(const CarbonSimTransaction&);  
  CarbonSimTransaction& operator=(const CarbonSimTransaction&);
};


#endif
