// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMWRAPPEDNET_H_
#define __CARBONSIMWRAPPEDNET_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimWrappedNet class.
*/
#endif


#ifndef __CARBONSIMNET_H_
#include "carbonsim/CarbonSimNet.h"
#endif
#ifndef __CARBONSIMOBJECTINSTANCE_H_
#include "carbonsim/CarbonSimObjectInstance.h"
#endif
#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef __UtObserver_h_
#include "carbon/UtObserver.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif


class CarbonSimWrappedNetI;


//! CarbonSimWrappedNet class
/*!
  CarbonSimWrappedNet provides access to signals that exist
  within a Carbon Model.
*/
class CarbonSimWrappedNet : public CarbonSimNet
{
 public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
  /*!
      \param object A valid Carbon Model object ID.
      \param net A valid net ID.
      \param name Full HDL pathname of the net.
  */
  CarbonSimWrappedNet(CarbonSimObjectInstance * object, CarbonNetID * net, const char * name);

  //! Destructor.
  virtual ~CarbonSimWrappedNet(void);

  //! Returns this
  virtual const CarbonSimWrappedNet* castWrappedNet() const;

  //! Gets a Carbon Model.
  inline CarbonObjectID * getCarbonObject(void) const { return mObject->getCarbonObject(); };
  
  //! Gets Carbon net.
  inline CarbonNetID * getCarbonNet(void) const { return mNet; };

  //! Gets Carbon net name.
  const char * getName(void) const;

  //! Returns the Least Significant Bit of a vector.
  virtual SInt32 getLSB(void) const;

  //! Returns the Most Significant Bit of a vector.
  virtual SInt32 getMSB(void) const;

  //! Checks if this net is a scalar.
  /*!
      \retval true If the net is a scalar.
      \retval false If the net is not a scalar.
  */
  virtual bool isScalar(void) const;

  //! Checks if this net is a vector.
  /*!
      \retval true If the net is a vector.
      \retval false If the net is not a vector.
  */
  virtual bool isVector(void) const;

  //! Checks if this net is an input.
  /*!
      \retval true If the net is a scalar.
      \retval false If the net is not a scalar.
  */
  bool isInput(void) const;

  //! Checks if this net is an output.
  /*!
      \retval true If the net is an output.
      \retval false If the net is not an output.
  */
  bool isOutput(void) const;

  //! Checks if this net is a bidirect.
  /*!
      \retval true If the net is a bidirect.
      \retval false If the net is not a bidirect.
  */
  bool isBidirect(void) const;

  //! Checks if this net is a tristate.
  /*!
      \retval true If the net is a tristate.
      \retval false If the net is not a tristate.
  */
  virtual bool isTristate(void) const;

  //! Checks if this net is driving logic.
  /*!
      \retval true If the net is driving logic.
      \retval false If the net is not driving logic.
  */
  virtual bool isDriving(void) const;

  //! Checks if this net is depositable
  /*!
    \retval true If the net is depositable.
    \retval false If the net is not depositable.
  */
  virtual bool isDepositable(void) const;

  //! Sets the value of this net.
  /*!
      \param buf The deposit value.
      \param drive The drive (Z or non-Z) of the value. Default value is 0 (NULL).
  */
  CarbonStatus deposit(const UInt32* buf, const UInt32* drive = 0);
  
  //! Gets the value of this net.
  /*!
      \param value The return value of the net; may be NULL. 
      Must be large enough to contain the entire net's value.
      \param drive The drive value of the net. The default is 
      0 (NULL). If not NULL and the net is a tristate or a bidirect, 
      it will have 1s set in the bit positions where the net is not 
      being driven. If the net is a non-tristate, the drive value 
      is set to 0. The drive must be of the same width as the value.
  */
  CarbonStatus examine(UInt32* value, UInt32* drive = 0) const;

  //! Forces the value of this net.
  /*! This function will force the net to a given value, overriding 
      the value from whatever logic is driving it. This function works 
      only if the net was marked as forcible during Carbon Model compilation. 
      Once you force a net to a specific value, it will remain at that 
      value until you explicitly release it using the release() function. 
      This is a useful function when you are trying to isolate a problem 
      in the design, or testing a potential solution to a problem.
      
      \param value The value to force on the net.
      \sa release()
  */
  CarbonStatus force(const UInt32* value);

  //! Releases this net from a forced value.
  /*! This function will release the entire net from the current value.

      \sa force()
  */
  CarbonStatus release(void);

  //! Formats this net for display.
  /*! Note that this function does not automatically add the terminating
      null ('\\0') to the generated string. The string may be larger 
      than the value, and may be used for other operations in addition
      to retrieving a value.

      \param value Buffer in which to put the string.
      \param len The length of the buffer.
      \param radix The radix for formatting the string (binary, ocatal, 
      hexadecimal, decimal).
  */
  CarbonStatus format(char* value, int len, CarbonRadix radix) const;
  
  //! Notifies the specified observer when the signal's value changes.
  void addNotifyOnValueChange(UtObserver<CarbonSimNet> * observer);

  //! Discontinues notifying the specified observer when the signal's value changes.
  void removeNotifyOnValueChange(UtObserver<CarbonSimNet> * observer);

  //! Notifies \e all observers when the signal's value changes.
  void notifyValueChange(void);

  //! Returns the number of UInt32s needed to store the value of this net.
  SInt32 getNumUInt32s(void) const;

  //! Returns the number of bits in this net.
  SInt32 getBitWidth(void) const;


 private:
  static void valueChange(CarbonObjectID * /* model */, CarbonNetID * /* net*/, CarbonClientData signal, UInt32* /* value */, UInt32* /* drive */);

 private:
  CarbonSimObjectInstance * mObject;
  CarbonNetID * mNet;
  CarbonNetValueCBDataID * mCallback;
  CarbonSimWrappedNetI* mPrivate;
  UInt32 * mDrive;

 private:
  CarbonSimWrappedNet(void);
  CarbonSimWrappedNet(const CarbonSimWrappedNet&);  
  CarbonSimWrappedNet& operator=(const CarbonSimWrappedNet&);
};


#endif
