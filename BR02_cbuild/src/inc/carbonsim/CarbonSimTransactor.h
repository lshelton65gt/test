// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMTRANSACTOR_H_
#define __CARBONSIMTRANSACTOR_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimTransactor class.
*/
#endif


#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif


class CarbonSimNet;
class CarbonSimTransaction;
class CarbonSimTransactorI;

// Worker function definitions for the CarbonSimTransactor's base class.  These
// are defined as functions rather than as class members to avoid putting 
// definitions for the transactor's data members in a public header file, which
// complicates linking outside of Carbon.  Note that CarbonSimTransactor is
// completely inline, which means that it gets compiled by the client.  This
// is needed to ensure the -frtti vs -fno-rtti can be made by the client.  You
// can't mix those gcc compile-flags between parent/child in class inheritance.
extern CarbonSimTransactorI* CarbonSimTransactorCreate();
extern void CarbonSimTransactorDestroy(CarbonSimTransactorI*);
extern void CarbonSimTransactorSubmit(CarbonSimTransactorI*, CarbonSimTransaction*);
extern SInt32 CarbonSimTransactorNumTransactions(CarbonSimTransactorI*);
extern CarbonSimTransaction* CarbonSimTransactorGetFirstTransaction(CarbonSimTransactorI*);
extern void CarbonSimTransactorPopFirstTransaction(CarbonSimTransactorI*);

//! CarbonSimTransactor class
/*! All transactions are added to a CarbonSimTransactor, which
    is then bound to a clock. The clock advises the transactor
    when it must perform some action, based on an event that
    has occurred.
*/
class CarbonSimTransactor
{
 public:
  //! Destructor.
  virtual ~CarbonSimTransactor() {
    CarbonSimTransactorDestroy(mPrivate);
  }

#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
  CarbonSimTransactor() {
    mPrivate = CarbonSimTransactorCreate();
    mId = -1;
  }

  //! Assign an id to this transactor (called by CarbonSimMaster)
  void putId(SInt32 id);

  //! Get the id for this transactor
  SInt32 getId() const { return mId; }

  //! Tells the transactor its clock has incremented.
  /*! This method advises the transactor that an event has 
      occurred that requires it to take some action. This will 
      generally be the clock generator to which the transactor 
      has been bound.

      \param tick The number of ticks the clock incremented.
   */
  virtual void step(CarbonTime tick) = 0;

  //! Adds a transaction to the the transactor.
  /*!
      \param transaction The transaction to add.
  */
  virtual void submit(CarbonSimTransaction * transaction) {
    CarbonSimTransactorSubmit(mPrivate, transaction);
  }

  //! Adds an idle event to the transactor.
  /*! This function adds an event of idle cycles. This might 
      be useful, for example, to space consecutive write and 
      read routines.

      \param cycles The number of cycles to be idle.
  */
  virtual void idle(UInt64 cycles) = 0;

  //! Notifies the transactor that a group of transactions is about to be submitted.
  /*! This method advises the transactor that several transactions 
      are about to arrive. This information can be used by the transactor 
      implementation to optimize the manner in which the transactions 
      are executed, or it can be completely ignored.
  */
  virtual void beginTransactionGroup(void) {
  }

  //! Notifies the transactor that a group of transactions are finished being submitted.
  /*! This method advises the transactor that a group  of transactions 
      is complete. This information can used by the transactor implementation 
      to optimize the manner in which the transactions are executed, or it 
      can be completely ignored.
  */
  virtual void endTransactionGroup(void) {
  }

#ifndef CARBON_EXTERNAL_DOC
  //! Prints this to UtIO::cout().
  /*!
      \param verbose
      \param indent
  */
  virtual void print(bool /*verbose*/ = true, UInt32 /*indent*/ = 0) {
  }    
#endif

 protected:
#if 0
  //! Looks up and assigns a signal to an internal signal.
  virtual bool bindSignal(CarbonObjectID * model, const char *signal_name, CarbonSimNet ** signal);
#endif

 protected:
  //! Gets number of transactions pending.
  SInt32 numTransactions() const {
    return CarbonSimTransactorNumTransactions(mPrivate);
  }

  //! Gets pending transaction.
  CarbonSimTransaction* getFirstTransaction() const {
    return CarbonSimTransactorGetFirstTransaction(mPrivate);
  }

  //! Pops pending transaction off stack.
  void popFirstTransaction() {
    CarbonSimTransactorPopFirstTransaction(mPrivate);
  }

  CarbonSimTransactorI* mPrivate;

 private:
  SInt32 mId;

  CarbonSimTransactor(const CarbonSimTransactor&);  
  CarbonSimTransactor& operator=(const CarbonSimTransactor&);
};


#endif
