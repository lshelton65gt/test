// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMBIDIWIRE_H_
#define __CARBONSIMBIDIWIRE_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimBidiWire class.
*/
#endif

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef __CARBONSIMWIRE_H_
#include "carbonsim/CarbonSimWire.h"
#endif
#ifndef __UtObserver_h_
#include "carbon/UtObserver.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif


class CarbonSimNet;
class CarbonSimBidiWireI;


//! CarbonSimBidiWire class
/*!
  CarbonSimBidiWire provides an interface for connecting multiple 
  Carbon Models to each other. CarbonSimBidiWire is bound to the 
  ports on the models to be connected, and must be registered 
  with the CarbonSimMaster. If the Carbon Models are distributed 
  across multiple processes or machines, CarbonSimBidiWire will 
  manage the connection without any change in the interface that 
  is called by the user. The abstraction that CarbonSimBidiWire 
  provides creates a highly flexible and portable environment for 
  linking to Carbon Models.

  Note that this class looks for multiple sources of data and
  multiple destinations. This same functionality may be accomplished
  using CarbonSimOneToOne() for each connection, but it would not be
  as efficient.

*/
class CarbonSimBidiWire : public CarbonSimWire
{
 public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
  CarbonSimBidiWire(CarbonSimWire::PullType pull);

  //! Constructor.
  CarbonSimBidiWire(void);

  //! Destructor.
  virtual ~CarbonSimBidiWire(void);

  //! Adds a driver to the wire.
  /*! 
      \param dst The net destination of the driver. This may be 
      any legal net defined in the system that drives logic, or 
      is driven.
  */
  void addDriver(CarbonSimNet * dst);

  //! Notifies the specified observer when there is a drive conflict.
  /*!
      \param observer The observer to notify.
      \sa removeNotifyOnDriveConflict
  */
  void addNotifyOnDriveConflict(UtObserver<CarbonSimBidiWire> * observer);

  //! Discontinues notifying the specified observer when there is a drive conflict.
  /*!
    \param observer The observer to stop notifying.
    \sa addNotifyOnDriveConflict()
  */
  void removeNotifyOnDriveConflict(UtObserver<CarbonSimBidiWire> * observer);

#ifndef CARBON_EXTERNAL_DOC
  // Cause the data to move.  
  virtual void flow(void);

  //! Prints this to UtIO::cout().
  virtual void print(bool verbose = true, UInt32 indent = 0);
#endif

 private:
  void init(void);

  //! Notifies \e all observers when there is a drive conflict.
  void notifyDriveConflict(void);

  bool isObserved(void);


 private:
  CarbonSimNet * mSrc;
  CarbonSimBidiWireI * mPrivate;

 private:
  CarbonSimBidiWire(const CarbonSimBidiWire&);  
  CarbonSimBidiWire& operator=(const CarbonSimBidiWire&);
};


#endif
