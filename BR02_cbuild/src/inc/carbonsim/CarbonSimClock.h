// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMCLOCK_H_
#define __CARBONSIMCLOCK_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimClock class.
*/
#endif

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef __CARBONSIMFUNCTIONGEN_H_
#include "carbonsim/CarbonSimFunctionGen.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif


//! CarbonSimClock class
/*!
    Each system clock is controlled using a unique CarbonSimClock. 
    The clock must be added to the CarbonSimMaster, which manages 
    the clock and insures that the correct actions take place when 
    the clock transitions.
*/
class CarbonSimClock : public CarbonSimFunctionGen
{
 public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
  /*! When declaring a CarbonSimClock, you \e must provide a value 
      for period and duty cycle (which are specified in ticks, not 
      speed). The parameters initial_value and offset are optional.

      Once declared, the user should never need to call any other 
      functions for the CarbonSimClock, since all toggling and 
      schedule calls are handled by CarbonSimMaster.

      \param period The period of the clock, in ticks. A clock with 
      a period of 100 will run toggle twice as fast as a clock with 
      a period of 200.
      \param duty_cycle The duty cycle of the clock, in ticks. That is, 
      the top edge of the clock.
      \param initial_value The initial value of the clock. The default is 0.
      \param offset The offset value for the clock. The default is 0. 
      This parameter is used to delay toggling of the clock until the 
      specified number of ticks has elapsed.
  */
  CarbonSimClock(CarbonTime period, CarbonTime duty_cycle, UInt32 initial_value = 0, CarbonTime offset = 0);

#ifndef CARBON_EXTERNAL_DOC
  //! Destructor.
  virtual ~CarbonSimClock(void);

  //! Gets the clock's period.
  inline CarbonTime getPeriod(void) const { return mPeriod; }

  //! Gets the clock's duty cycle.
  inline CarbonTime getDutyCycle(void) const { return mDutyCycle; }

  //! Gets the clock's current value.
  inline UInt32 getValue(void) const { return CarbonSimFunctionGen::getValue(); };

  //! Gets the clock's value at the specified time.
  virtual UInt32 getValue(CarbonTime tick);

  //! Checks if the clock transitions again.
  inline bool hasNextTransitionTick(void) { return true; };

  //! Gets the clock's next transition time.
  virtual CarbonTime getNextTransitionTick(void);
#endif	//CARBON_EXTERNAL_DOC

 protected:
  CarbonTime mPeriod;
  CarbonTime mDutyCycle;
  CarbonTime mNonDutyCycle;

 private:
  CarbonSimClock(void);
  CarbonSimClock(const CarbonSimClock&);  
  CarbonSimClock& operator=(const CarbonSimClock&);
};


#endif
