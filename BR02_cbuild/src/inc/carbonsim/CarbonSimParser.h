// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMPARSER_H_
#define __CARBONSIMPARSER_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimParser class.
*/


#include <stdio.h>
#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonSimDebugger;
class CarbonSimParserI;

class CarbonSimParser
{
public: CARBONMEM_OVERRIDES
  CarbonSimParser(FILE* in, FILE* out, CarbonSimDebugger * debugger);
  ~CarbonSimParser(void);
  SInt32 parse(void);
  SInt32 scan(void *lval);

  CarbonSimDebugger* getDebugger() const;

 private:
  CarbonSimParser(void);
  CarbonSimParser(const CarbonSimParser&);
  CarbonSimParser& operator=(const CarbonSimParser&);

  CarbonSimDebugger* mDebugger;
};


#endif	//CARBON_EXTERNAL_DOC
#endif
