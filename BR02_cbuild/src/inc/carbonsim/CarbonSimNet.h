// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMNET_H_
#define __CARBONSIMNET_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimNet class.
*/
#endif


#include "carbon/carbon_shelltypes.h"
#include "carbon/UtObserver.h"
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonSimWrappedNet;
class CarbonSimAllocatedNet;

//! CarbonSimNet class
/*!
  CarbonSimNet provides a uniform method for access CarbonNetID
  objects in the Validation Manager API. All routines in this 
  class correspond directly to DesignPlayer API functions.
*/
class CarbonSimNet
{
 public: 
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Constructor.
  CarbonSimNet(void);

  //! Destructor.
  virtual ~CarbonSimNet(void);

  //! Return this as a CarbonSimWrappedNet, if applicable
  /*!
    \retval non-NULL If this wraps a CarbonNetID*.
    \retval NULL If this does not wrap a CarbonNetID*.
  */
  virtual const CarbonSimWrappedNet* castWrappedNet() const;

  //! Modifiable version of castWrappedNet()
  CarbonSimWrappedNet* castWrappedNet()
  {
    const CarbonSimNet* me = const_cast<const CarbonSimNet*>(this);
    return const_cast<CarbonSimWrappedNet*>(me->castWrappedNet());
  }

  //! Return this as a CarbonSimAllocatedNet, if applicable
  /*!
    \retval non-NULL If this represents a net in the system that is
    not part of the carbon-compiled design.
    \retval NULL If this net represents a net in the carbon-compiled
    design.
  */
  virtual const CarbonSimAllocatedNet* castAllocatedNet() const;

  //! Modifiable version of castAllocatedNet()
  CarbonSimAllocatedNet* castAllocatedNet()
  {
    const CarbonSimNet* me = const_cast<const CarbonSimNet*>(this);
    return const_cast<CarbonSimAllocatedNet*>(me->castAllocatedNet());
  }

  //! Gets net name.
  virtual const char * getName(void) const = 0;

  //! Returns the Least Significant Bit of a vector.
  /*! Returns the least signifigant bit of a vector.  This values
      will be 0 if this is a scalar.
  */
  virtual SInt32 getLSB(void) const = 0;

  //! Returns the Most Significant Bit of a vector.
  /*! Returns the most signifigant bit of a vector.  This values
      will be 0 if this is a scalar.
  */
  virtual SInt32 getMSB(void) const = 0;

  //! Checks if this net is a scalar.
  /*!
      \retval true If the net is a scalar.
      \retval false If the net is not a scalar.
  */
  virtual bool isScalar(void) const = 0;

  //! Checks if this net is a vector.
  /*!
      \retval true If the net is a vector.
      \retval false If the net is not a vector.
  */
  virtual bool isVector(void) const = 0;

  //! Checks if this net is an input.
  /*!
      \retval true If the net is a scalar.
      \retval false If the net is not a scalar.
  */
  virtual bool isInput(void) const = 0;

  //! Checks if this net is an output.
  /*!
      \retval true If the net is an output.
      \retval false If the net is not an output.
  */
  virtual bool isOutput(void) const = 0;

  //! Checks if this net is a bidirect.
  /*!
      \retval true If the net is a bidirect.
      \retval false If the net is not a bidirect.
  */
  virtual bool isBidirect(void) const = 0;

  //! Checks if this net is a tristate.
  /*!
      \retval true If the net is a tristate.
      \retval false If the net is not a tristate.
  */
  virtual bool isTristate(void) const = 0;

  //! Checks if this net is driving logic.
  /*!
      \retval true If the net is driving logic.
      \retval false If the net is not driving logic.
  */
  virtual bool isDriving(void) const = 0;

  //! Checks if this net is depositable
  /*!
    \retval true If the net is depositable.
    \retval false If the net is not depositable.
  */
  virtual bool isDepositable(void) const = 0;

  //! Sets the value of this net.
  /*!
      \param buf The deposit value.
      \param drive The drive (Z or non-Z) of the value. Default value is 0 (NULL).
  */
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive = 0) = 0;
  
  //! Gets the value of this net.
  /*!
      \param value The return value of the net; may be NULL. 
      Must be large enough to contain the entire net's value.
      \param drive The drive value of the net. The default is 
      0 (NULL). If not NULL and the net is a tristate or a bidirect, 
      it will have 1s set in the bit positions where the net is not 
      being driven. If the net is a non-tristate, the drive value 
      is set to 0. The drive must be of the same width as the value.
  */
  virtual CarbonStatus examine(UInt32* value, UInt32* drive = 0) const = 0;

  //! Forces the value of this net.
  /*! This function will force the net to a given value, overriding 
      the value from whatever logic is driving it. This function works 
      only if the net was marked as forcible during Carbon Model compilation. 
      Once you force a net to a specific value, it will remain at that 
      value until you explicitly release it using the release() function. 
      This is a useful function when you are trying to isolate a problem 
      in the design, or testing a potential solution to a problem.
      
      \param value The value to force on the net.
      \sa release()
  */
  virtual CarbonStatus force(const UInt32* value) = 0;

  //! Releases this net from a forced value.
  /*! This function will release the entire net from the current value.

      \sa force()
  */
  virtual CarbonStatus release(void) = 0;

  //! Formats this net for display.
  /*! Note that this function does not automatically add the terminating
      null ('\\0') to the generated string. The string may be larger 
      than the value, and may be used for other operations in addition
      to retrieving a value.

      \param value Buffer in which to put the string.
      \param len The length of the buffer.
      \param radix The radix for formatting the string (binary, ocatal, 
      hexadecimal, decimal).
  */
  virtual CarbonStatus format(char* value, int len, CarbonRadix radix) const = 0;
  
  //! Notifies the specified observer when the signal's value changes.
  virtual void addNotifyOnValueChange(UtObserver<CarbonSimNet> * observer) = 0;

  //! Discontinues notifying the specified observer when the signal's value changes.
  virtual void removeNotifyOnValueChange(UtObserver<CarbonSimNet> * observer) = 0;

  //! Notifies \e all observers when the signal's value changes.
  virtual void notifyValueChange(void) = 0;

  //! Returns the number of UInt32s needed to store the value of this net.
  virtual SInt32 getNumUInt32s(void) const = 0;

  //! Returns the number of bits in this net.
  virtual SInt32 getBitWidth(void) const = 0;


 private:
  CarbonSimNet(const CarbonSimNet&);  
  CarbonSimNet& operator=(const CarbonSimNet&);
};


#endif
