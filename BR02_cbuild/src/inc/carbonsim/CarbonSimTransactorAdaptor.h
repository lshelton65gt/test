// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONSIMTRANSACTORADAPTOR_H_
#define __CARBONSIMTRANSACTORADAPTOR_H_


#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimTransactorAdaptor class.
*/



#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef __Util_h_
#include "util/Util.h"
#endif
#ifndef _LOOP_H_
#include "util/Loop.h"
#endif
#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif


class DynBitVector;
class CarbonSimTransactor;
class UtString;


//! CarbonSimTransactorAdaptor class
/*!
  This is used to create a connection between the debugging
  infrastructure and an actual transactor.
*/
class CarbonSimTransactorAdaptor
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  CarbonSimTransactorAdaptor(CarbonSimTransactor * transactor);

  //! Destructor.
  virtual ~CarbonSimTransactorAdaptor(void);

  typedef UtArray<DynBitVector *> BitVectorArgList;
  typedef Loop<BitVectorArgList> BitVectorArgListLoop;


  //! Pass a message onto a transactor.
  virtual bool dispatch(const UtString * command, CarbonSimTransactorAdaptor::BitVectorArgList * bits_list);

 protected:
  CarbonSimTransactor * mTransactor;

 private:
  CarbonSimTransactorAdaptor(void);
  CarbonSimTransactorAdaptor(const CarbonSimTransactorAdaptor&);  
  CarbonSimTransactorAdaptor& operator=(const CarbonSimTransactorAdaptor&);
};


#endif	//CARBON_EXTERNAL_DOC
#endif
