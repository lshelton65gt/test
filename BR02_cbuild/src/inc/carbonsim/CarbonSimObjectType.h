// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMOBJECTTYPE_H_
#define __CARBONSIMOBJECTTYPE_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimObjectType class.
*/
#endif


#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif


class CarbonSimObjectInstance;
class CarbonSimObjectTypeI;


//! CarbonSimObjectType class
/*!
  This class provides a C++ frontend to the C API functions for manipulating
  CarbonObjectTypes.  All routines here correspond directly to C API 
  routines.
*/
class CarbonSimObjectType
{
public:  
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

  //! Type for creation functions.
  typedef CarbonObjectID * (* InstanceCreationFunction)(CarbonDBType, CarbonInitFlags);

public: 
  //! Constructor.
  CarbonSimObjectType(const char * type_name, InstanceCreationFunction creation_function, CarbonDBType default_db_type = eCarbonFullDB, CarbonInitFlags default_init_flags = eCarbon_NoFlags);

  //! Destructor.
  virtual ~CarbonSimObjectType(void);

  //! Get the definition name of this object type.
  const char * getName(void) const;

  //! Get the creation function.
  InstanceCreationFunction getCreationFunction(void) const;

  //! Get the default database type.
  CarbonDBType getDefaultDBType(void) const;

  //! Get the default init flags.
  CarbonInitFlags getDefaultInitFlags(void) const;


 private:
  CarbonSimObjectTypeI * mPrivate;


 private:
  CarbonSimObjectType(void);
  CarbonSimObjectType(const CarbonSimObjectType&);  
  CarbonSimObjectType& operator=(const CarbonSimObjectType&);
};


#endif
