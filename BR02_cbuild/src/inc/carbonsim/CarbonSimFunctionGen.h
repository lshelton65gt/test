// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMFUNCTIONGEN_H_
#define __CARBONSIMFUNCTIONGEN_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimFunctionGen class.
*/



#include "carbon/carbon_shelltypes.h"
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonSimNet;
class CarbonSimTransactor;
class CarbonSimFunctionGenI;
class CarbonSimMaster;

//! CarbonSimFunctionGen class
/*!
  This class keeps track of the various attributes associated with
  a system functiongen.
*/
class CarbonSimFunctionGen
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  CarbonSimFunctionGen(CarbonTime offset, UInt32 initial_value = 0);

  //! Destructor.
  virtual ~CarbonSimFunctionGen(void);

  //! Get the step's offset.
  inline CarbonTime getOffset(void) const { return mOffset; };

  //! Get the functiongen's current value.
  UInt32 getValue(void) const { return mCurrentValue; };

  //! Get the functiongen's value at the given time.
  virtual UInt32 getValue(CarbonTime tick) = 0;

  //! Set the functiongen's current tick value.
  virtual void setTick(CarbonTime tick);

  //! Get the functiongen's current tick value.
  inline CarbonTime getTick(void) const { return mCurrentTick; };

  //! Does the functiongen transition again?
  virtual bool hasNextTransitionTick(void) = 0;

  //! Get the functiongen's next transition time.
  virtual CarbonTime getNextTransitionTick(void) = 0;

  //! Bind a signal to stimulate when the functiongen transitions.
  bool bindSignal(CarbonSimNet * net);

  //! Bind a signal to stimulate when the functiongen transitions.
#if 0
  bool bindSignal(CarbonObjectID * model, const char *signal);
#endif

  //! Bind a transactor to stimulate when the functiongen transitions.
  bool bindTransactor(CarbonSimTransactor * transactor);

  //! Print this to std::cout.
  virtual void print(bool verbose = true, UInt32 indent = 0);


  //! Set the master
  /*!
    This also adds any existing bound transactors to the sim master's
    list. This can only be called once for this generator.
  */
  void putMaster(CarbonSimMaster* simMaster);

 protected:
  const CarbonTime mOffset;
  const UInt32 mInitialValue;
  UInt32 mCurrentValue;
  CarbonTime mCurrentTick;

 private:
  CarbonSimFunctionGenI* mPrivate;
  CarbonSimMaster* mMaster;

 private:
  CarbonSimFunctionGen(void);
  CarbonSimFunctionGen(const CarbonSimFunctionGen&);  
  CarbonSimFunctionGen& operator=(const CarbonSimFunctionGen&);
};


#endif	//CARBON_EXTERNAL_DOC
#endif
