// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONSIMONETOONEWIRE_H_
#define __CARBONSIMONETOONEWIRE_H_

#ifndef CARBON_EXTERNAL_DOC
/*!
  \file
  The CarbonSimOneToOneWire class.
*/
#endif

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif
#ifndef __CARBONSIMWIRE_H_
#include "carbonsim/CarbonSimWire.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonSimNet;

/*!
  \defgroup CarbonSimExternC Extern C CarbonSim functions
*/


//! CarbonSimOneToOneWire class
/*!
  CarbonSimOneToOneWire provides an interface for connecting multiple 
  Carbon Models to each other. This class is bound to the ports on the 
  Carbon Models to be connected, and must be registered with the 
  CarbonSimMaster. If the Carbon Models are distributed across multiple 
  processes or machines, CarbonSimOneToOneWire will manage the connection 
  without any change in the interface that is called by the user. The 
  abstraction that CarbonSimOneToOneWire provides creates a highly 
  flexible and portable environment for linking to Carbon Models.

  See \ref CarbonSimExternC for connecting CarbonSimOneToOneWire objects
  over socket connections.
*/
class CarbonSimOneToOneWire : public CarbonSimWire
{
 public:   
#ifndef CARBON_EXTERNAL_DOC
  CARBONMEM_OVERRIDES
#endif

//! Constructor.
/*! Declares a wire between two nets--a one-to-one relationship.

    \param src The source net of the wire.
    \param dst The destination net of the wire. This may be any legal 
    net defined in the system that can be driven.
    \param pull The pull mode of the source net. A net can be pulled 
    up, pulled down, or not pulled at all (the default). If the net 
    has no driver, this parameter indicates the pull mode for that net.
*/

  CarbonSimOneToOneWire(CarbonSimNet * src, CarbonSimNet * dst, CarbonSimWire::PullType pull = eCarbonSimWirePullNone);

  //! Destructor.
  virtual ~CarbonSimOneToOneWire(void);


#ifndef CARBON_EXTERNAL_DOC
  // Causes the data to move.  
  virtual void flow(void);

  //! Prints this to UtIO::cout().
  virtual void print(bool verbose = true, UInt32 indent = 0);
#endif


 private:
  CarbonSimNet * mSrc;
  CarbonSimNet * mDst;

 private:
  CarbonSimOneToOneWire(void);
  CarbonSimOneToOneWire(const CarbonSimOneToOneWire&);  
  CarbonSimOneToOneWire& operator=(const CarbonSimOneToOneWire&);
};

extern "C" {

  /*!
    \addtogroup CarbonSimExternC Extern C CarbonSim functions
    The following are extern C functions to create CarbonSimOneToOneWire
    objects which communicate over sockets.

    @{
  */
  
  //! Constructor for outgoing socket
  /*! Declares a wire between a CarbonSimNet and a socket connection.  The wire
    is unidirectional, with values flowing from the CarbonSimNet to the socket.
    The socket connection will be to the local machine at the port number passed
    to this function.  The socket specified here must also be connected to a
    CarbonSimOneToOne wire created with the carbonSimOneToInWireInCreate
    function in order to complete the connection from DesignPlayer to DesignPlayer.
    
    \param src The source net of the wire.
    \param port The socket port number to be written.
    \param buffer_size The number of entries to buffer before writing to the socket.
    
    \note The use of carbonSimOneToOneSocketWireOutCreate and
    carbonSimOneToOneSocketWireInCreate requires that
    \$(CARBON_SOCKET_LIB) be added to the Makefile link command of the
    application. This needs to appear \e before \$(CARBON_LIB_LIST).
    
    \sa carbonSimOneToOneSocketWireInCreate()
  */
  CarbonSimOneToOneWire* carbonSimOneToOneSocketWireOutCreate(CarbonSimNet* src, UInt32 port, UInt32 buffer_size=1);

  //! Constructor for incoming socket
  /*! 
    Declares a wire between a CarbonSimNet and a socket connection.  The wire
    is unidirectional, with values flowing from the socket to the CarbonSimNet.
    The socket connection will be to the IP address and socket number passed
    to this function.  An IP address of 127.0.0.1 will connect to the local
    machine, while any other IP address will connect to a socket on a remote
    machine.  The socket specified here must also be connected to a
    CarbonSimOneToOne wire created with the carbonSimOneToOneWireOutCreate
    function in order to complete the connection from DesignPlayer to
    DesignPlayer.
    
    \param ipaddr The IP address of the socket to be read.
    \param port The socket port number to be read.
    \param dst The destination net of the wire. This may be any legal 
    net defined in the system that can be driven.
    \param buffer_size The number of entries that have been buffered to be
    read from the socket.
    
    \note
    The use of carbonSimOneToOneSocketWireOutCreate and
    carbonSimOneToOneSocketWireInCreate requires that
    \$(CARBON_SOCKET_LIB) be added to the Makefile link command of the
    application. This needs to appear \e before \$(CARBON_LIB_LIST).
    
    \sa carbonSimOneToOneSocketWireOutCreate()
  */
  CarbonSimOneToOneWire* carbonSimOneToOneSocketWireInCreate(const char * ipaddr, UInt32 port, CarbonSimNet * dst, UInt32 buffer_size=1);

  /*! @} */
}

#endif
