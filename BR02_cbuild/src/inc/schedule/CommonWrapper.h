/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/UtHashSet.h"
#include "util/UtHashMap.h"

class CGPortIfaceNet;
class SCHEvent;
class NUNet;

typedef UtHashSet<const NUNet*> CNetSet;
typedef UtHashMap<const NUNet*, const SCHEvent*> ClkToEvent;
typedef UtHashSet<const CGPortIfaceNet*> PortNetSet;
typedef UtArray<const CGPortIfaceNet*> PortNetVec;

