// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  Contains some base scheduler types. This file is useful for defining
  types that are needed to know the basic scheduler types without
  needing the contents. By including this file instead of Schedule.h
  we get fewer recompiles on Schedule.h changes.
*/

#ifndef _SCHEDULETYPES_H_
#define _SCHEDULETYPES_H_

// Forward definitions
class SCHScheduleMask;
class SCHSchedule;
class SCHSequential;
class SCHCombinational;
class SCHSequentials;

//! Types for all schedules
/*! Note that the combinational schedules are ordered to make printing
 *  of multi-schedule blocks easier to understand.
 */
enum SCHScheduleType
{
  eSequentialEdge,      //!< An edge sub schedule of a sequential
  eSequential,		//!< A standard Sequential schedule
  eSequentialDCL,	//!< A Derived Clock Logic Sequential schedule
  eCombInput,		//!< The input schedule
  eCombAsync,		//!< The asychronous schedule
  eCombDCL,		//!< A Derived Clock Logic combinational sched.
  eCombSample,		//!< A sample mask combinational schedule
  eCombTransition,	//!< A transition mask combinational schedule
  eCombInitial,		//!< The initial schedule for initial blocks
  eCombInitSettle,      //!< Combo blocks that must be run at init
  eCombDebug,		//!< The debug schedule
  eCombForceDeposit,	//!< Schedule to run after a force/deposit
  eDCL,                 //!< A Derived clock logic schedule
};

//! Creates a schedule type mask from a schedule type
#define SCHCreateScheduleTypeMask(type) ((SCHScheduleTypeMask)(1 << (type)))

//! Masks for all schedule types
/*! These are created from SCHScheduleType. The first group are single
 *  bit set masks that represent one schedule type. The eMaskAll
 *  entries are combinations of schedules.
 */
enum SCHScheduleTypeMask
{
  eMaskSequentialEdge      = (1 << eSequentialEdge),
  eMaskSequential          = (1 << eSequential),
  eMaskSequentialDCL       = (1 << eSequentialDCL),
  eMaskCombInput           = (1 << eCombInput),
  eMaskCombAsync           = (1 << eCombAsync),
  eMaskCombDCL             = (1 << eCombDCL),
  eMaskCombSample          = (1 << eCombSample),
  eMaskCombTransition      = (1 << eCombTransition),
  eMaskCombInitial         = (1 << eCombInitial),
  eMaskCombInitSettle      = (1 << eCombInitSettle),
  eMaskCombDebug           = (1 << eCombDebug),
  eMaskCombForceDeposit    = (1 << eCombForceDeposit),
  eMaskDCL                 = (1 << eDCL),

  eMaskAllSequentials      = (eMaskSequential | eMaskSequentialDCL),
  eMaskAllSimulationCombs  = (eMaskCombInput | eMaskCombAsync | eMaskCombDCL |
                              eMaskCombSample | eMaskCombTransition),
  eMaskAllCombinationals   = (eMaskAllSimulationCombs | eMaskCombInitial |
                              eMaskCombInitSettle | eMaskCombDebug |
                              eMaskCombForceDeposit),
  eMaskAllSchedules        = (eMaskAllSequentials | eMaskAllCombinationals),
  eMaskAllSimulationScheds = (eMaskAllSequentials | eMaskAllSimulationCombs)
};

//! General iterator abstraction for all vectors of blocks (seq, combo, etc.)
typedef Iter<FLNodeElab*> SCHIterator;

//! Abstraction for the set of input nets a schedule is sensitive to
typedef UtSet<const NUNetElab*, NUNetElabCmp> SCHInputNets;

//! Abstraction for an iterator for asynchronous/input/DCL schedule inputs
typedef Loop<SCHInputNets> SCHInputNetsLoop;
typedef CLoop<const SCHInputNets> SCHInputNetsCLoop;

//! Abstraction for a vector of combinational schedules
typedef UtVector<SCHCombinational*> SCHCombinationals;

//! Iterator over combinationals
typedef Loop<SCHCombinationals> SCHCombinationalsLoop;

//! Abstraction for a vector of vector of sequential schedules
typedef UtVector<SCHSequentials*> SCHSequentialsVector;

//! Iterator over combinationals
typedef Loop<SCHSequentialsVector> SCHSequentialsVectorLoop;

//! Abstraction for a set of elaborated clocks
typedef UtSet<const NUNetElab*, NUNetElabCmp> SCHClocks;

//! Abstraction for an iterator over the elaborated clocks
typedef CLoop<const SCHClocks> SCHClocksLoop;

//! Abstraction to iterate over both clock masters and inverts.
typedef LoopPair<SCHClocksLoop, SCHClocksLoop> SCHClockMastersAndInvertsLoop;

#endif // _SCHEDULETYPES_H_
