// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _DERIVEDCLOCKLOGIC_H_
#define _DERIVEDCLOCKLOGIC_H_

class SCHBlockFlowNodes;

//! SCHDerivedClockBase class
/*! This is a base class to represent derivied clock schedules. These
 *  schedules produce one more more clocks to be used as triggers for
 *  other schedules.
 *
 *  This is not a complete class and must be derived to provide the
 *  full functionality.
 */
class SCHDerivedClockBase : public SCHScheduleBase
{
public:
  //! constructor
  SCHDerivedClockBase(int id);

  //! destructor
  ~SCHDerivedClockBase();

  //! set the schedule mask
  virtual void setMask(const SCHScheduleMask* mask) = 0;

  //! get the scheduleMask
  virtual const SCHScheduleMask* getMask() const = 0;

  //! Iterate over the Clocks for this derived clock
  SCHClocksLoop loopClocks() const;

  //! Get a clock to represent the derived clock logic schedule
  const NUNetElab* getRepresentativeClock() const;

  //! Test if this schedule generates the given clock
  bool generatesClock(const NUNetElab* clkElab) const;

  //! get an iterator over all the state update nets in this schedule
  virtual SCHSequentials::SULoop loopSUNets() const = 0;

  //! Find all the state update nets in this schedule
  virtual void findSUNets(const SCHBlockFlowNodes& blockFlowNodes) = 0;

  //! Get the set of input nets this DCL's combo schedule is sensitive to
  virtual SCHInputNetsCLoop loopDCLNets() const = 0;

  //! Get the set of input nets this DCL's combo schedule is sensitive to
  virtual const SCHInputNets* getDCLNets() const = 0;

  //! Get the depth with other derived clock logic schedules (for ordering)
  int getDepth() const { return mDepth; }

  //! Put the depth with other derived clock logic schedules (for ordering)
  void putDepth(int depth) { mDepth = depth; }

  //! Get the id for this derived clock logic schedule
  int getID() const { return mID; }

  //! Cast to a simple DCL schedule or return NULL
  virtual SCHDerivedClockLogic* castDCL() = 0;

  //! Cast to a DCL Cycle schedule or return NULL
  virtual SCHDerivedClockCycle* castDCLCycle() = 0;

  //! Cast to a simple DCL schedule or return NULL
  virtual const SCHDerivedClockLogic* castDCL() const = 0;

  //! Cast to a DCL Cycle schedule or return NULL
  virtual const SCHDerivedClockCycle* castDCLCycle() const = 0;

  //! Iterator over all the sub sequential schedules
  typedef LoopMulti<SCHSequentials::SequentialLoop> SequentialLoop;

  //! Iterate over all the sub sequential schedules
  virtual SequentialLoop loopMultiSequential() const = 0;

  //! Get the combinational schedule(s) (used internal only)
  virtual void getCombinationalSchedules(SCHCombinationals* combinationals)
    const = 0;

  //! Get the sequential schedules(s) for each DCL sub schedule
  virtual void getSequentialSchedules(SCHSequentialsVector* sequentials)
    const = 0;

  //! Compare two DCL schedules
  int compareSchedules(const SCHDerivedClockBase* dcl) const;

  //! Function to compare two schedules in a predictable way
  virtual int compareSchedules(const SCHScheduleBase* schedBase) const;

  //! Merge another DCL schedule into this one
  void mergeSchedule(SCHDerivedClockBase* otherSched);

  //! Returns the number of clocks for this schedule
  int numClocks() const { return mClocks->size(); }

protected:
  // Shared clocks set so that we can share operations. We store this
  // for cycles which is a compound schedule because we iterate over
  // clocks very frequently.
  SCHClocks* mClocks;

private:
  int mID;    // Used so we can figure out which schedule a node is in
  int mDepth; // Used to sort derived clock logic schedules
}; // class SCHDerivedClockBase : public SCHScheduleBase

  
//! SCHDerivedClockLogic class
/*!
 * This class contains the logic schedule used to derive another
 * clock.  Each object represents the schedule to produce the value
 * for that clock. As such it will have only one schedule mask that
 * is the combination of all the schedule masks.
 *
 * This schedule can contain both sequential blocks and
 * combinational blocks.
 */
class SCHDerivedClockLogic : public SCHDerivedClockBase
{
public:
  //! constructor
  SCHDerivedClockLogic(NUCNetElabSet& clocks, const SCHScheduleMask* mask,
		       SCHUtil* util, int id);

  //! destructor
  ~SCHDerivedClockLogic();

  //! set the schedule mask
  void setMask(const SCHScheduleMask* mask);

  //! get the scheduleMask
  const SCHScheduleMask* getMask() const;

  //! Function to print information about this schedule
  virtual void print(bool printNets, SCHMarkDesign* mark = NULL) const;

  //! Return an iterator for all the Sequential schedules - see documentation for class SCHSchedule
  SCHSequentials::SequentialLoop loopSequential() const;

  //! get the combinational schedule -- see documentation for class SCHSchedule
  SCHIterator getSchedule();

  //! get an iterator over all the state update nets in this schedule
  SCHSequentials::SULoop loopSUNets() const;

  //! Find all the state update nets in this schedule
  void findSUNets(const SCHBlockFlowNodes& blockFlowNodes);

  //! Add a combinational node to a given derived clock logic schedule.
  /*! This routine adds a flow node to this derived clock logic
   *  combinational schedule. This schedule represents one of the
   *  valid trigger condition combinations for the derived clock
   *  logic section of the design. Once a node gets assigned here it
   *  cannot be removed. But a combinational node may be in more
   *  than one combinational schedules.
   *
   *  This routine should only be used by the SCHSchedule class.
   *
   *  \param node The flow node to add to this schedule.
   */
  void addCombinationalNode(FLNodeElab* node);

  //! Get the sequential schedules (used internal only)
  SCHSequentials* getSequentialSchedules() const { return mSequentials; }

  //! Get the combinational schedule (used internal only)
  SCHCombinational* getCombinationalSchedule() const { return mCombinational; }

  //! Get the combinational schedule(s) (used internal only)
  void getCombinationalSchedules(SCHCombinationals* combinationals) const;

  //! Get the set of input nets this DCL's combo schedule is sensitive to
  virtual SCHInputNetsCLoop loopDCLNets() const;

  //! Get the set of input nets this DCL's combo schedule is sensitive to
  virtual const SCHInputNets* getDCLNets() const;

  //! Returns if this DCL is being traversed
  bool isBusy() const { return mBusy; }

  //! Sets if this DCL is being traversed
  void putBusy(bool busy) { mBusy = busy; }

  //! Cast to a simple DCL schedule or return NULL
  SCHDerivedClockLogic* castDCL();

  //! Cast to a DCL Cycle schedule or return NULL
  SCHDerivedClockCycle* castDCLCycle();

  //! Cast to a simple DCL schedule or return NULL
  const SCHDerivedClockLogic* castDCL() const;

  //! Cast to a DCL Cycle schedule or return NULL
  const SCHDerivedClockCycle* castDCLCycle() const;

  //! Iterate over all the sub sequential schedules
  SequentialLoop loopMultiSequential() const;

  //! Get the sequential schedules(s) for each DCL sub schedule
  void getSequentialSchedules(SCHSequentialsVector* sequentials) const;

  //! Function to print information for -dumpSchedule
  void dump(int indent, SCHMarkDesign* mark) const;

  //! Set whether this DCL schedule is nested in a cycle or not
  void putCycleNode(bool cycleNode) { mCycleNode = cycleNode; }

  //! Test whether this DCL schedule is part of a cycle or not
  bool isCycleNode(void) const { return mCycleNode; }

  //! Override cast to a derived clock logic schedule
  SCHDerivedClockLogic* castDerivedClockLogic() { return this; }

  //! Override cast to a derived clock logic schedule
  const SCHDerivedClockLogic* castDerivedClockLogic() const { return this; }

private:
  SCHCombinational* mCombinational;
  SCHSequentials* mSequentials;
  bool mBusy;	// Used to find cycles through derived clock logic schedules
  bool mCycleNode; // If set, this DCL schedule is part of a cycle
}; // class SCHDerivedClockLogic

//! SCHDerivedClockCycle class
/*! This class contains a set of simple derived clock logic schedules
 *  that have cyclic dependencies. It is presented as an ordered
 *  vector of SCHDerivedClockLogic schedules.
 */
class SCHDerivedClockCycle : public SCHDerivedClockBase
{
private:
  typedef UtSet<const NUNetElab*> BreakNets;

public:
  //! constructor
  SCHDerivedClockCycle(int id);

  //! destructor
  ~SCHDerivedClockCycle();

  //! set the schedule mask
  void setMask(const SCHScheduleMask* mask);

  //! get the scheduleMask
  const SCHScheduleMask* getMask() const;

  //! Add a DCL schedule
  void addSubSchedule(SCHDerivedClockLogic* dcl);

  // An array of DCL schedules in this DCL cycle
  typedef UtVector<SCHDerivedClockLogic*> SubSchedules;

  //! Iterator over the DCL sub-schedules
  typedef Loop<SubSchedules> SubSchedulesLoop;

  //! Iterate over the DCL schedules
  SubSchedulesLoop loopSubSchedules() const;

  //! Iterate over the DCL schedules
  SubSchedulesLoop loopSortedSubSchedules() const;

  //! get an iterator over all the state update nets in this schedule
  SCHSequentials::SULoop loopSUNets() const;

  //! Find all the state update nets in this schedule
  void findSUNets(const SCHBlockFlowNodes& blockFlowNodes);

  //! Get the set of input nets this DCL's combo schedule is sensitive to
  virtual SCHInputNetsCLoop loopDCLNets() const;

  //! Get the set of input nets this DCL's combo schedule is sensitive to
  virtual const SCHInputNets* getDCLNets() const;

  //! Cast to a simple DCL schedule or return NULL
  SCHDerivedClockLogic* castDCL();

  //! Cast to a DCL Cycle schedule or return NULL
  SCHDerivedClockCycle* castDCLCycle();

  //! Cast to a simple DCL schedule or return NULL
  const SCHDerivedClockLogic* castDCL() const;

  //! Cast to a DCL Cycle schedule or return NULL
  const SCHDerivedClockCycle* castDCLCycle() const;

  //! Iterate over all the sub sequential schedules
  SequentialLoop loopMultiSequential() const;

  //! Get the combinational schedule(s) (used internal only)
  void getCombinationalSchedules(SCHCombinationals* combinationals) const;

  //! Get the sequential schedules(s) for each DCL sub schedule
  void getSequentialSchedules(SCHSequentialsVector* sequentials) const;

  //! Function to print information about this schedule
  virtual void print(bool printNets, SCHMarkDesign* mark = NULL) const;

  //! Function to print information for -dumpSchedule
  void dump(int indent, SCHMarkDesign* mark) const;

  //! Add the summary DCL input nets computed from the sub schedules
  void putDCLInputNets(const SCHInputNets* inputNets);

  //! Returns true if this clock is computed after it is used
  /*! Break clocks in the cycle (simple DCL schedules) are scheduled
   *  after its fanout. This means that they do not reach their fanout
   *  until the next iteration in the while loop.
   *
   *  This is important because it means their change flag should
   *  survive to the next iteration and so it should not be computed
   *  till the end of the while loop. That way we guarantee that edges
   *  are only seen once by schedules.
   *
   *  It is also important to know what clocks are not break
   *  points. These are clocks that can compute their changed flags
   *  immediately, allowing any follow up schedules that use the value
   *  to get correctly computed in the current iteration. This helps
   *  the cycle settle more quickly.
   */
  bool isBreakClock(const NUNetElab* clkElab) const;

  //! Iterator over break nets computed by this cycle
  typedef CLoop<BreakNets> BreakNetsLoop;

  //! Returns an iterator over the break nets for this entire cycle
  /*! Break nets are the same as break clocks except that codegen
   *  needs a different interface for accessing them. Instead of
   *  testing whether the clock is a break clock, it needs an iterator
   *  over them.
   *
   *  See isBreakClock() for why we need to know about these
   *  nets. Essentially these are nets that are computed after they
   *  are used so we need to use them to decide to iterate.
   */
  BreakNetsLoop loopBreakNets() const;

  //! Add a break clock (clock computed after it is used)
  void addBreakClock(const NUNetElab* clkElab);

  //! Add a break net (clock logic computed after it is used)
  void addBreakNet(const NUNetElab* netElab);

  //! Return the number of sub schedules in this cycle
  int size(void) const { return mSubSchedules->size(); }

  //! Override cast to a derived clock cycle schedule 
  SCHDerivedClockCycle* castDerivedClockCycle() { return this; }

  //! Override cast to a derived clock cycle schedule 
  const SCHDerivedClockCycle* castDerivedClockCycle() const { return this; }

  //! Function to remove dead break nets (most likely due to block merging)
  void removeDeadBreakNets(SCHUtil* util);

private:
  // Data for the DCL sub schedules
  SubSchedules* mSubSchedules;

  // Types and data to keep track of the clocks and nets that are
  // computed after they are used. Therefore, the changed flag for
  // clocks must survive till the next iteration. Also, changes on any
  // of these nets requires further iteration.
  //
  // The break clocks and break nets are kept separate. This is
  // because of how codegen needs the data. It does a loop global
  // change detection for clocks and a loop local change detection for
  // derived clock logic break nets.
  SCHClocks* mBreakClocks;
  BreakNets* mBreakNets;

  // Storage to make iteration over State update and DCL input nets
  // simpler and sorted. They are mutable because this memory gets
  // updated every time a caller wants to iterate over the data.
  //
  // This is done with temporary storage because we expect it takes
  // space and we expect this to be called late and infrequently in
  // the compile.
  mutable SCHSequentials::SUNets* mSUNets;
  const SCHInputNets* mDCLInputNets;

  // Storage for the combined schedule mask.
  const SCHScheduleMask* mMask;
}; // class SCHDerivedClockCycle : public SCHDerivedClockBase

    
#endif // _DERIVEDCLOCKLOGIC_H_
