// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _COMBINATIONAL_H_
#define _COMBINATIONAL_H_

//! SCHCombinational class
/*!
 * This class contains the combinational logic schedule for a set of
 * clock patterns.  Any combinational block will be in exactly one
 * schedule, even if it is reached by more than one clock edge.  At
 * runtime, when the shell detects that a set of clocks changed, it
 * may run several combinational schedules.  For example, there may
 * be a combinational schedule for (posedge clk1), and another one
 * for (posedge clk1 or posedge clk2).  When clk1 has a rising edge,
 * both schedules must be executed.
 */
class SCHCombinational : public SCHScheduleBase
{
public:
  //! constructor
  SCHCombinational(SCHScheduleType type, const SCHScheduleMask* mask,
		   SCHDerivedClockLogic* parent, int inputAsyncIndex = 0);

  //! destructor
  ~SCHCombinational();

  //! get the scheduleMask
  const SCHScheduleMask* getMask() const {return mMask;}

  //! get the schedule -- see documentation for class SCHSchedule
  SCHIterator getSchedule() const;

  //! Get the set of input nets this schedule is sensitive to
  SCHInputNetsCLoop loopInputNets() const;

  //! Get the input/async schedule index
  int getInputAsyncIndex() const
  {
    SCHScheduleType type = getScheduleType();
    SCHED_ASSERT((type == eCombInput) || (type == eCombAsync), this);
    return mInputAsyncIndex;
  }

  //! Get the transition/sample index
  int getIndex() const
  {
    SCHScheduleType type = getScheduleType();
    SCHED_ASSERT((type == eCombSample) || (type == eCombTransition), this);
    return mIndex;
  }

  //! Put the transition/sample index
  void putIndex(int index) 
  {
    SCHScheduleType type = getScheduleType();
    SCHED_ASSERT((type == eCombSample) || (type == eCombTransition), this);
    mIndex = index;
  }

  //! Test if this schedule is empty
  bool empty() const;

  //! Get the size for this schedule in nodes
  UInt32 size() const;

  //! update the schedule mask (internal use only)
  void putMask(const SCHScheduleMask* mask) { mMask = mask; }

  //! Add a combinational node to a given combinational schedule.
  /*! This routine adds a flow node to this combinational
   *  schedule. This combinational schedule represents one of the
   *  valid trigger condition combinations for the design. Once a
   *  node gets assigned here it cannot be removed. But a
   *  combinational node may be in more than one combinational
   *  schedules.
   *
   *  This routine should only be used by the SCHSchedule class.
   *
   *  \param node The flow node to add to this schedule.
   */
  void addCombinationalNode(FLNodeElab* node);
  
  //! Remove any flow that has been deleted from the schedule.
  void removeFlows(const SCHDeleteTest& deleteTest);

  //! Add a combinational block to a given combinational schedule.
  /*! A combinational block is represented as a set of nodes for an
   *  always block that belong in this schedule.
   *
   *  This routine should only be used by the scheduler.
   */
  void addCombinationalBlock(FLNodeElabVector& nodes);

  //! Abstraction for the block schedule
  typedef UtVector<FLNodeElabVector*> BlockSchedule;

  //! Abstraction for an iterator over the block schedule
  typedef Loop<BlockSchedule> BlockScheduleLoop;

  //! Get an iterator over the block schedule.
  BlockScheduleLoop getBlockSchedule();

  //! Clear the combinational block schedule
  void clearBlockSchedule();

  //! clear the combinational schedule (needed for re sorting the schedule)
  void clearSchedule();

  //! Get the parent derived clock logic schedule
  SCHDerivedClockLogic* getParentSchedule() const
  {
    SCHED_ASSERT(getScheduleType() == eCombDCL, this);
    return mParent;
  }

  //! Get the set of input nets in canonical form
  const SCHInputNets* getAsyncInputNets() const { return mAsyncInputNets; }

  //! Set the input nets for this schedule
  void putAsyncInputNets(const SCHInputNets* inputNets)
  {
    mAsyncInputNets = inputNets;
  }

  //! Compare two combinational schedules
  virtual int compareSchedules(const SCHScheduleBase* schedBase) const;

  //! Compare two combinational schedules
  static int compare(const SCHCombinational* c1, const SCHCombinational* c2)
  {
    return c1->compareSchedules(c2);
  }

  //! Function to print information about this schedule
  virtual void print(bool printNets, SCHMarkDesign* mark = NULL) const;

  //! Function to print information for -dumpSchedule
  void dump(int indent, SCHMarkDesign* mark) const;

  //! Override cast to a combinational schedule
  SCHCombinational* castCombinational() { return this; }

  //! Override cast to a combinational schedule
  const SCHCombinational* castCombinational() const { return this; }

private:
  // Hide copy and assign constructors.
  SCHCombinational(const SCHCombinational&);
  SCHCombinational& operator=(const SCHCombinational&);

  // Helper function to deleted flows from a vector
  void removeFlows(const SCHDeleteTest& deleteTest, FLNodeElabVector* flows);

  // Data for a temporary view into the schedule
  BlockSchedule* mBlockSchedule;

  // The final mask and schedule
  const SCHScheduleMask* mMask;
  FLNodeElabVector* mSchedule;

  // The input nets that affect this async/input schedule 
  const SCHInputNets* mAsyncInputNets;
  int mInputAsyncIndex;

  // Used to sort the schedules
  int mIndex;

  // We somethings need to know if this is a sub schedule of a parent
  // schedule (dervied clock schedule) and if so which one. So we
  // store a pointer to our parent schedule.
  SCHDerivedClockLogic* mParent;
}; // class SCHCombinational : public SCHScheduleBase



#endif // _COMBINATIONAL_H_
