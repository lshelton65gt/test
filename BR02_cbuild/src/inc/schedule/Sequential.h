// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Class definition for all sequential schedules
*/

#ifndef _SEQUENTIAL_H_
#define _SEQUENTIAL_H_

#include "util/LoopMulti.h"
#include "util/LoopThunk.h"
#include "util/LoopFilter.h"

class SCHMarkDesign;
class SCHSequentialEdge;
class SCHBlockFlowNodes;

//! SCHSequential class
/*!
 * This class contains the schedule for a given unique clock and
 * asynchronous set/reset schedule mask. The asynchronous set/reset
 * mask is a set of prioritized conditions under which an
 * asychronous set/reset occurs. The priority is indicated in the
 * SCHEvent conditions in the mask.
 */
class SCHSequential : public SCHScheduleBase
{
public:
  //! constructor
  SCHSequential(SCHScheduleType type, const NUNetElab* netElab,
		const SCHScheduleMask* asyncMask,
		const SCHScheduleMask* extraMask,
                SCHDerivedClockLogic* parent);

  //! destructor
  ~SCHSequential();

  //! get the set/reset mask
  const SCHScheduleMask* getAsyncMask() const { return mAsyncMask; }

  //! get the clock or async set/reset net
  const NUNetElab* getEdgeNet() const {return mEdgeNet;}

  //! Get the extra edges mask
  const SCHScheduleMask* getExtraEdgesMask() const { return mExtraEdgesMask; }

  //! Add a sequential node to a given sequential schedule.
  /*! This routine adds a flow node to this sequential
   *  schedule. This sequential schedule represents one of the edges
   *  for a clock in the design. Once a node gets assigned here it
   *  cannot be removed. In addition a sequential node may be in
   *  only one sequential schedule.
   *
   *  This routine should only be used by the SCHSchedule class.
   *
   *  \param node The flow node to add to this schedule.
   *  \param edge Which edge of the clock to assign this flow node
   */
  void addSequentialNode(FLNodeElab* node, ClockEdge edge);

  //! Get the schedule for a clock edge -- see documentation for class SCHSchedule
  SCHIterator getSchedule(ClockEdge edge) const;

  //! Get the list of sequential nodes in this schedule
  /*! This routine gets the list of nodes before they are ready for
   *  code generation. It should only be used by the scheduler.
   */
  SCHNodeSetLoop loopNodes(ClockEdge edge);

  //! Test if the schedule for a given clock edge is empty
  bool empty(ClockEdge edge) const;

  //! Get the size for a given clock edge in blocks
  UInt32 size(ClockEdge edge) const;

  //! Remove the duplicate flow nodes in a sequential schedule
  void removeDuplicateNodes();

  //! Remove any flow nodes according to a delete test
  void removeFlows(const SCHDeleteTest& deleteTest);

  //! Returns if this schedule is ordered yet
  bool isOrdered() const { return (mOrder != 0); }

  //! sets this schedule as ordered
  void putOrder(UInt32 order)
  {
    SCHED_ASSERT(mOrder == 0, this);
    mOrder = order;
  }

  //! gets this schedule's order
  UInt32 getOrder() const { return mOrder; }

  //! sort the sequential nodes in this schedule
  void sortNodes();
  
  //! Get the parent derived clock logic schedule
  SCHDerivedClockLogic* getParentSchedule() const
  {
    SCHED_ASSERT(getScheduleType() == eSequentialDCL, this);
    return mParent;
  }

  //! Compare two sequential schedules
  /*! This function compares any two sequential schedules including
   *  sequential sschedules that are subsets of derived clock logic
   *  schedules. If a comparison is needed for two schedules within
   *  the same main schedule (normal sequential schedules or
   *  sequential schedules within a single derived clock logic
   *  schedule) then use the CmpSequential structure above.
   *
   *  Note that although the argument is a SCHScheduleBase it cannot
   *  be anything but a SCHSequential.
   */
  virtual int compareSchedules(const SCHScheduleBase* schedBase) const;

  //! Function to print details on this schedule
  virtual void print(bool printNets, SCHMarkDesign* mark = NULL) const;

  //! Function to print information for -dumpSchedule
  void dump(int indent, SCHMarkDesign* mark) const;

  //! Override cast to a sequential schedule
  SCHSequential* castSequential() { return this; }

  //! Override cast to a sequential schedule
  const SCHSequential* castSequential() const { return this; }

  //! Get a sequential edge schedule
  SCHSequentialEdge* getSequentialEdge(ClockEdge edge)
  {
    return mSubSchedules[edge];
  }

private:
  //! Function to print all the flow nodes in this schedule
  void dumpSchedule(int indent, ClockEdge edge, UtString& asyncBuf,
                    UtString& extraBuf, UtString& branchNetsBuf,
                    SCHMarkDesign* mark) const;

  // SequentialSchedules needs access to private types
  friend class SCHSequentialSchedules;

  // The following fields define this schedule. They are used to sort
  // the schedules as well as to decide what sequential nodes can be
  // scheduled together. Only nodes with exact same fields can be
  // scheduled together.
  //
  // The edge net is the net which triggers running this schedule.
  //
  // The async mask are the conditions under which a higher priority
  // schedule should win (async set/reset schedules). This schedule
  // should not run for any of those reasons.
  //
  // The extra mask are extra edge conditions under which we should
  // run this schedule. This only occurs if this schedule represents
  // and async reset with a non-constant on the RHS. In this case the
  // schedule should run on lower priorty clock and async conditions
  // where this async condition is active. For 99% of the logic we
  // expect this to be NULL. We do this to match simulation for what
  // we consider non-synthesizable logic.
  const NUNetElab* mEdgeNet;
  const SCHScheduleMask* mAsyncMask;
  const SCHScheduleMask* mExtraEdgesMask;
  
  // Helper routine to print nets in a schedule
  void printSchedNets(ClockEdge edge, SCHMarkDesign* mark) const;
  
  //! The two edge schedules
  SCHSequentialEdge* mSubSchedules[2];

  // Once we have ordered the schedules this order variable will be
  // set. We can then sort the set of sequential schedules this
  // schedule is part of.
  UInt32 mOrder;

  // We somethings need to know if this is a sub schedule of a parent
  // schedule (dervied clock schedule) and if so which one. So we
  // store a pointer to our parent schedule.
  SCHDerivedClockLogic* mParent;
}; // class SCHSequential : public SCHScheduleBase

//! SCHSequentialEdge class
/*! This class represents a single edge of sequential schedule. This
 *  is broken up so that combinational and sequential base schedules
 *  are more homogeneous.
 */
class SCHSequentialEdge : public SCHScheduleBase
{
public:
  //! constructor
  SCHSequentialEdge(SCHSequential* parent, ClockEdge edge);

  //! destructor
  virtual ~SCHSequentialEdge();

  //! Add a sequential node to a given sequential schedule.
  /*! This routine adds a flow node to this sequential
   *  schedule. This sequential schedule represents one of the edges
   *  for a clock in the design. Once a node gets assigned here it
   *  cannot be removed. In addition a sequential node may be in
   *  only one sequential schedule.
   *
   *  This routine should only be used by the SCHSchedule class.
   *
   *  \param node The flow node to add to this schedule.
   */
  void addSequentialNode(FLNodeElab* node);

  //! Get the schedule for this clock edge
  SCHIterator getSchedule() const;

  //! Get the list of sequential nodes in this schedule
  /*! This routine gets the list of nodes before they are ready for
   *  code generation. It should only be used by the scheduler.
   */
  SCHNodeSetLoop loopNodes();

  //! Test if the schedule for this clock edge is empty
  bool empty() const;

  //! Get the size for this clock edge in blocks
  UInt32 size() const;

  //! Remove the duplicate flow nodes in a sequential schedule
  void removeDuplicateNodes();

  //! Remove any flow nodes according to a delete test
  void removeFlows(const SCHDeleteTest& deleteTest);

  //! Get the parent sequential schedule
  SCHSequential* getParentSchedule() const { return mParent; }

  //! Compare two sequential schedules
  /*! This function compares any two sequential schedules including
   *  sequential sschedules that are subsets of derived clock logic
   *  schedules. If a comparison is needed for two schedules within
   *  the same main schedule (normal sequential schedules or
   *  sequential schedules within a single derived clock logic
   *  schedule) then use the CmpSequential structure above.
   *
   *  Note that although the argument is a SCHScheduleBase it cannot
   *  be anything but a SCHSequential.
   */
  virtual int compareSchedules(const SCHScheduleBase* schedBase) const;

  //! Override cast to a sequential edge schedule
  SCHSequentialEdge* castSequentialEdge() { return this; }

  //! Override cast to a sequential edge schedule
  const SCHSequentialEdge* castSequentialEdge() const { return this; }

  //! Helper function to print the nets of a schedule
  void printSchedNets(SCHMarkDesign* mark) const;

  //! Sort the flow nodes in this sub schedule
  void sortNodes();

  //! Function to print details on this schedule
  /*! (Not supported, call on parent)
   */
  void print(bool printNets, SCHMarkDesign* mark = NULL) const;

  //! Function to print information for -dumpSchedule
  /*! (Not supported, call on parent)
   */
  void dump(int indent, SCHMarkDesign* mark) const;


private:
  // The following are used to export the schedule to the code generator
  FLNodeElabVector* mSchedule;
  bool mScheduleCreated;

  // The folowing are used to make sure we have unique nodes
  SCHNodeSet* mScheduleSet;

  //! The parent sequential schedule
  SCHSequential* mParent;

  //! The edge this schedule represents (one of posedge or negedge)
  /*! Used mainly for comparisons
   */
  ClockEdge mEdge;
}; // class SCHSequentialEdge

//! SCHSequentials class
/*!
 */
class SCHSequentials
{
private:
  typedef UtVector<SCHSequential*> SequentialVector;

public:
  //! Constructor
  SCHSequentials(SCHScheduleType type, SCHUtil* util);

  //! Destructor
  ~SCHSequentials();

  //! Find the sequential map but don't create it if it doesn't exist
  SCHSequential* findSequential(const NUNetElab* edgeNet,
                                const SCHScheduleMask* asyncMask,
                                const SCHScheduleMask* extraMask);

  //! Add a sequential schedule or return an existing one if it already exists
  SCHSequential* addSequential(const NUNetElab* clk,
			       const SCHScheduleMask* asyncMask,
			       const SCHScheduleMask* extraMask,
			       SCHDerivedClockLogic* parent);

  //! Add a node to a sequential schedule in this schedule set
  void scheduleSequentialNode(FLNodeElab* flow, NUUseDefNode* driver,
			      SCHDerivedClockLogic* parent,
                              const SCHScheduleMask* extraMask,
                              SCHMarkDesign* markDesign);

  //! Abstraction for an iterator for sequential-node schedules
  typedef Loop<SequentialVector> SequentialLoop;

  //! Return an iterator for the Sequential nodes -- see the SCHSchedule class documentation
  SequentialLoop loopSequential();

  //! sort the sequential nodes in this schedule
  void sortNodes();

  //! Abstraction for the set of state update nets
  typedef UtVector<NUNetElab*> SUNets;

  //! Abstraction to iterate over all state update nets for this schedule set
  typedef Loop<SUNets> SULoop;

  //! Get an iterator over all the SU nets in all the sequential schedules
  SULoop loopSUNets();

  //! Find all the SU nets in all the sequential schedules
  void findSUNets(const SCHBlockFlowNodes& blockFlowNodes);

  //! Order the sequentials to reduce the state updates
  void sort();

  //! Remove any sequential schedules which have an inactive clock
  void removeInactiveSchedules(void);

private:
  // Private class to keep a map of sequential schedules.
  class SequentialSortFields
  {
  public:
    //! constructor
    SequentialSortFields(const NUNetElab* netElab,
                         const SCHScheduleMask* asyncMask,
                         const SCHScheduleMask* extraMask) :
      mNetElab(netElab), mAsyncMask(asyncMask), mExtraMask(extraMask) {}

    //! destructor
    ~SequentialSortFields() {}

    //! Use for sorting in maps. First by net elab, then by schedule mask
    bool operator<(const SequentialSortFields& nmp1) const;

  private:
    const NUNetElab* mNetElab;
    const SCHScheduleMask* mAsyncMask;
    const SCHScheduleMask* mExtraMask;
  }; // class SequentialSortFields

  typedef UtMap<const SequentialSortFields, SCHSequential*> SequentialMap;

  SequentialMap* mMap;
  SequentialVector* mVector;
  SCHScheduleType mScheduleType;

  // Place to store state update nets
  SUNets* mSUNets;
  void findSUNets(SCHIterator i, NUNetElabSet* coveredNets,
                  NUNetElabSet* coveredSUNets,
                  const SCHBlockFlowNodes& blockFlowNodes);

  // helper class
  SCHUtil* mUtil;
}; // class SCHSequentials

#endif // _SEQUENTIAL_H_
