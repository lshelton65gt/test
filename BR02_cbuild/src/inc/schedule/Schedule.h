// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  A class to create design schedules.
*/

#ifndef _SCHEDULE_H_
#define _SCHEDULE_H_

#include "util/IterMap.h"
#include "util/UtOrder.h"
#include "util/Loop.h"

#include "nucleus/NUNet.h"      // For NUNetElabCmp

#include "flow/Flow.h"

#include "schedule/ScheduleTypes.h"
#include "schedule/CommonWrapper.h"

class ArgProc;
class CGPortIface;
class Directives;
class ESPopulateExpr;
class FLNodeElab;
class FLNodeElabCycle;
class IODBNucleus;
class MsgContext;
class NUDesign;
class NUNetElab;
class NUUseDefNode;
class RETransform;
class SCHBlockMerge;
class SCHBlockSplit;
class SCHClkAnal;
class SCHCombinational;
class SCHCombinationalSchedules;
class SCHConditionalExecution;
class SCHCreateSchedules;
class SCHCycleDetection;
class SCHDerivedClockBase;
class SCHDerivedClockCycle;
class SCHDerivedClockLogic;
class SCHDerivedClockSchedules;
class SCHDump;
class SCHEvent;
class SCHInputNetsFactory;
class SCHMarkDesign;
class SCHReady;
class SCHScheduleData;
class SCHScheduleFactory;
class SCHSequential;
class SCHSequentialSchedules;
class SCHSignature;
class SCHSortCombBlocks;
class SCHTimingAnalysis;
class SCHUseDefHierPair;
class SCHUtil;
class STSymbolTable;
class Split;
class Stats;
class ZostreamDB;

//! Structure to compare two elaborated flow node pointers by their contents
struct SCHCmpFlow
{
  bool operator()(const FLNodeElab* f1, const FLNodeElab* f2) const;
};

//! Syntactic sugar for a set of nodes
typedef UtSet<FLNodeElab*, SCHCmpFlow> SCHNodeSet;

//! Abstraction for a loop over a set of nodes
typedef Loop<SCHNodeSet> SCHNodeSetLoop;

//! Iterator for a set of flow nodes
typedef SCHNodeSet::iterator SCHNodeSetIter;

//! Compares two Elaborated Flow Nodes
/*!
  When inserting nodes in a sequential schedule, we should
  only insert one FLNodeElab* per block, even though there will
  be an FLNodeElab* for every block output.
*/
struct SCHCmpNodes
{
  //! Override the default comparison function
  bool equal(const FLNodeElab* n1, const FLNodeElab* n2) const;
  bool lessThan1 (const FLNodeElab* a, const FLNodeElab* b) const;
  size_t hash(const FLNodeElab* n1) const;
};

//! Abstraction to retain only one FLNodeElab from any given NUUseDefNode
typedef UtHashSet<FLNodeElab*, SCHCmpNodes> SCHUniqueBlockNodeSet;

//! Abstraction to decide whether a flow node should be deleted
class SCHDeleteTest
{
public:
  SCHDeleteTest() {}
  virtual ~SCHDeleteTest() {}
  virtual bool operator()(FLNodeElab*) const = 0;
};

// The folowing includes defined the major scheduler classes. They are
// in separate files just to keep file sizes managable.
#include "schedule/ScheduleBase.h"
#include "schedule/Sequential.h"
#include "schedule/Combinational.h"
#include "schedule/DerivedClockLogic.h"
#include "schedule/SchedulesLoop.h"

//! SCHSchedule class
/*!
 * This class implements the global scheduler.  It uses the FLNodeElab class
 * to iterate over the design connectivity and determine a schedule by 
 * examining clocking.
 */
class SCHSchedule
{
public:
  //! SCHSchedule constructor
  SCHSchedule(NUNetRefFactory*,
              MsgContext*,
              STSymbolTable*,
              RETransform*,
              Split * split,
              FLNodeFactory*,
              FLNodeElabFactory*,
              ArgProc* args,
              IODBNucleus* directives,
              ESPopulateExpr* populateExpr,
              SourceLocatorFactory* sourceLocatorFactory);

  //! SCHSchedule destructor
  ~SCHSchedule();

  //! Set up the required command-line options (call only once no
  //! matter how many times SCHSchedule is constructed)
  void setupOptions();

  //! Build the schedule for the entire design
  /*! This routine creates a number of schedules for the entire design
   *  such that under a set of input conditions, we execute the
   *  minimum amount of logic. The possible input conditions are that
   *  a clock or set of clocks has changed or design primary inputs
   *  have changed.
   *
   *  To this end the routine puts logic in one of the following
   *  classes of schedules. They are:
   *
   *  - Sequential Schedule - All edge triggered devices. There are
   *    two schedules for every clock in the design.
   *
   *  - Asynchronous Schedule - All combinational devices that are
   *    sensitive to design primary inputs and influence design
   *    primary outputs.
   *
   *  - Combinational Schedule - All combinational devices that are
   *    sensitive to at least one edge triggered device. There can be
   *    as many schedules as there are valid trigger condition
   *    combinations in the design.
   *
   *  - Input Schedule - All combinational devices that are sensitive
   *    to any design primary inputs.
   *
   *  - Initial Schedule - All combinational devices that
   *	initialize data to a constant. This can include initial
   *	statements in Verilog and constants that cannot be
   *	propagated further.
   *
   *  - Derived Clock Logic Schedule - All devices that need to
   *	execute to produce the value for a derived clock. This
   *	should be run first so that we can then make the
   *	appropriate calls to all the sequential and
   *	combinational schedules.
   *
   *  Once this routine completes it produces a schedule that can be
   *  traversed by the code generator to call the design blocks in the
   *  correct order.
   *
   *  \todo Need to create a debug schedule.
   *
   *  \param design Pointer to the design to be scheduled.
   *
   */
  bool buildSchedule(NUDesign* design, Stats* stats, 
                     bool phaseStats, bool dumpSchedule, 
                     bool doCModelWrapper, 
		     TristateModeT tristateMode,
                     const char* destDir);

  //! Mark a net as being observable, forcing it to be scheduled even if dead
  void observeNet(NUNetElab* net);

  //! Walk the design and update the symbol table with constant and live/dead information
  void markWaveableNets();

  //! Abstraction for a set of derived clock logic schedules
  typedef UtVector<SCHDerivedClockBase*> DerivedClockLogicVector;

  //! Abstraction for an iterator for derived clock logic schedules
  typedef Loop<DerivedClockLogicVector> DerivedClockLogicLoop;

  //! Abstraction for a set of combinational schedules
  typedef UtVector<SCHCombinational*> Combinationals;

  //! Abstraction for an iterator for combinational-block schedules
  typedef Loop<Combinationals> CombinationalLoop;

  //! Abstraction for a map of async resets to edge counts
  typedef UtMap<const NUNetElab*, UInt32*, NUNetElabCmp> AsyncResetMap;

  //! Abstraction for an iterator for async resets
  typedef LoopMap<AsyncResetMap> AsyncResetLoop;

  //! Return an iterator for all the Sequential objects
  /*! Sequential-block schedules are kept in SCHSequential
   *  instances.  Each instance has a posedge schedule and a negedge
   *  schedule. The details are in the example below.
   *
   *  For each sequential schedule you can get the edge pin
   *  (Sequential::getEdgeNet()) and a schedule mask for the
   *  asynchronous set/reset logic (SCHSequential::getAsyncMask()).
   *
   *  The edge pin is the net under which this schedule should
   *  execute. This can either be a clock for a normal schedule or an
   *  asynchronous set/reset pin if this is an asynchronous reset
   *  schedule that has been morphed into a clock schedule.
   *
   *  The asynchronous set/reset mask indicates when asynchronous
   *  set/resets will occur and the priority. Use
   *  SCHScheduleMask::loopEvents() to get the edges for the various
   *  asynchronous set/reset. So if you have clk as the edge pin and
   *  posedge reset1 and posedge reset2 as the async mask, then the
   *  generated code to call the schedule should look like:

   \verbatim
        Generated Code:
	   if (~reset1 && ~reset2 && changed[clk])
	   {
	      if (clk)
	        run_clk_posedge();
	      else
	        run_clk_negedge();
	   }
   \endverbatim

   *
   *  In the same case as above, there will be a reset1 and reset2
   *  sequential schedule. The reset1 schedule will have posedge
   *  reset2 in its asynchronous mask. The reset2 schedule will have a
   *  NULL asynchronous mask. The code for calling those schedules
   *  should look like:

   \verbatim
        Generated Code:
	   if (~reset2 && changed[reset1])
	   {
	     if (reset1)
	       run_reset1_posedge();
	     else
	       run_reset1_negedge();
	   }
	   if (changed[reset2])
	   {
	     if (reset2)
	       run_reset2_posedge();
	     else
	       run_reset2_negedge();
	   }
   \endverbatim

   *
   *  The order of the sequential class is not important but the order
   *  of the sequential blocks in each edge schedule is in the order
   *  they should execute.
   *
   *  The sequential blocks in this schedule should be in one and only
   *  one schedule.

   \verbatim
   Example:
     SCHSequentials::SequentialLoop p;
     for (p = sched->loopSequential(); !p.atEnd(); ++p)
     {
       SCHSequential* seq = *p;

       // Use the following two bits of information to determine when to run
       // the logic
       const NUNetElab* clk = sc->getEdgeNet();
       const SCHScheduleMask* asyncMask = sc->getAsyncMask();

       // Use this to generate code for the logic
       SCHIterator iter;
       if (!seq->empty(eClockPosedge))
         iter = seq->getSchedule(eClockPosedge);
       ...
       if (!seq->empty(eClockNegedge)
         iter = seq->getSchedule(eClockNegedge);
       ...
     }
   \endverbatim
  */
  SCHSequentials::SequentialLoop loopSequential();

  //! get an iterator over all the state update nets in this schedule
  SCHSequentials::SULoop loopSUNets();

  //! SCHIterator for groups of SCHCombinational objects
  /*! Combinational-block schedules are kept in SCHCombinational
   *  instances.  Logic blocks that are sensitive to more than one
   *  clock edge can only appear in one schedule.  When a set of
   *  clocks transitions simultaneously, several schedules may get run
   *  in response.  For example, if we have signal "a" sensitive to
   *  "posedge clk1", and "b" sensitive to "posedge clk2", and "c"
   *  which is "a|b", will be sensitive to either "clk1" or "clk2".
   *  In this case, there will be three separate schedules created:
   *  "posedge clk1", "posedge clk2", and "posedge clk1 or posedge
   *  clk2".  If "posedge clk1" is seen, then both the first and third
   *  schedules are run.
   *
   *  There are four types of combinational schedules:
   *
   *  1. Runs whenever the values could transition and are triggered by
   *     some clock conditions.
   *
   *  2. Runs whenever the values it produces are needed and are
   *     triggered by some clock conditions.
   *
   *  3. Runs whenever a primary input/bid that feeds it changes and
   *     the schedule affects the value of a primary output/bid.
   *
   *  4. Runs whenever a primary input/bid that feeds it changes and
   *     the schedule does not affect the value of a primary output/bid.
   * 
   *  The order of the schedules in the loop is the order in which
   *  they should be executed.
   *
   *  The combinational blocks in this schedule may be in other
   *  schedules as well.
   *
   *  \param type This is either eCombTransition, eCombSample,
   *  eCombAsync, or eCombInput.
   *
   \verbatim
   Example:
     SCHSchedule::CombinationalLoop p;
     for (p = sched->loopCombinational(eCombTransition); !p.atEnd(); ++p)
     {
       SCHCombinational* comb = *p;
       const SCHScheduleMask* mask = comb->getMask();
       const SCHInputNets* inputNets = comb->getInputNets();
       SCHIterator iter = comb->getSchedule();
       ...
     }
     for (p = sched->loopCombinational(eCombSample); !p.atEnd(); ++p)
     {
       SCHCombinational* comb = *p;
       const SCHScheduleMask* mask = comb->getMask();
       const SCHInputNets* inputNets = comb->getInputNets();
       SCHIterator iter = comb->getSchedule();
       ...
     }
     for (p = sched->loopCombinational(eCombAsync); !p.atEnd(); ++p)
     {
       SCHCombinational* comb = *p;
       const SCHScheduleMask* mask = comb->getMask();
       const SCHInputNets* inputNets = comb->getInputNets();
       SCHIterator iter = comb->getSchedule();
       ...
     }
     for (p = sched->loopCombinational(eCombInput); !p.atEnd(); ++p)
     {
       SCHCombinational* comb = *p;
       const SCHScheduleMask* mask = comb->getMask();
       const SCHInputNets* inputNets = comb->getInputNets();
       SCHIterator iter = comb->getSchedule();
       ...
     }
   \endverbatim
  */
  CombinationalLoop loopCombinational(SCHScheduleType type);

  //! Return an iterator for all the SCHDerivedClockLogic objects
  /*! DerivedClockLogic-block schedules are kept in
      SCHDerivedClockLogic instances. This iterator gets
      each derived clock logic object which can then provide the
      sequential and combinational sub-schedules.

      The sequential sub-schedule can be obtained by calling
      SCHDerivedClockLogic::loopSequential(). The
      combinational logic can be obtained by calling
      SCHDerivedClockLogic::getSchedule.

      The order to execute the schedules is to iterator over the
      derived clock logic schedules. For each DCL schedule, first
      schedule all the sequential sub schedules and then schedule the
      combinational schedule.

      The sequential blocks in the sequential sub-schedules should be
      in one and only one schedule. The combinational blocks can be in
      another schedule.

      \verbatim
      Example:
        // Schedule the sequential sub-classes
        SCHSchedule::DerivedClockLogicLoop p;
	for (p = sched->loopDerivedClockLogic(); !p.atEnd(); ++p)
	{
	  SCHDerivedClockLogic* dcl = *p;
	  const NUNetElab* clk = dcl->getClock();
	  for (SCHSequentials::SequentialLoop s = dcl->loopSequential();
	       !s.atEnd(); ++s)
	  {
	     SCHSequential* seq = *p;

	     // Use the following two bits of information to determine
	     // when to run the logic
	     const NUNetElab* clk = s->getEdgeNet();
	     const SCHScheduleMask* asyncMask = s->getAsyncMask();

	     // Use this to generate code for the sequential schedules
	     // or to generate calls to the schedule
	     SCHIterator iter;
	     if (!seq->empty(eClockPosedge))
	     {
	       iter = seq->getSchedule(eClockPosedge);
	       ...
	     }
	     if (!seq->empty(eClockNegedge))
	     {
	       iter = seq->getSchedule(eClockNegedge);
	       ...
	     }
	  }

	  // Get the set of nets this DCL's combinational schedule is
	  // sensitive to.
	  SCHInputNetsLoop p;
	  for (p = dcl->loopDCLNets(); !p.atEnd(); ++p)
	    // Use to generate if statement in call to schedule

	  // Generate logic for the combinational schedule or call schedule
	  SCHIterator iter = dcl->getSchedule();
	  ...
	}
      \endverbatim
   */
  DerivedClockLogicLoop loopDerivedClockLogic();

  typedef Loop<UtSet<NUCycle*,DerefOrder<NUCycle> > > CycleLoop;

  //! Get all the combinational cycle nodes -- these are not in schedule order
  /*! This routine is used to find all the combinational cycle nodes, for
   *  the purposes of declaring the combinational cycle methods in the
   *  class for the top level module.  These nodes don't come out in any
   *  particular order.
   */
  CycleLoop loopCycles();

  //! Get the list of all clocks
  /*!
    \verbatim
    Example:
    for (SCHClocksLoop p = sched->loopClocks(); !p.atEnd(); ++p)
    {
      NUNetElab* netElab = *p;
      ...
    }
    \endverbatim
  */
  SCHClocksLoop loopClocks();

  //! Is the net a clock that is a primary input?
  bool isNUNetPrimaryClockInput(const NUNet& net) const;

  //! Test whether an elaborated net is a frequently depositable.
  /*! Nets marked depositable normally create a deposit schedule that
   *  is run after the deposit occurs. This technique assumes that
   *  deposits are infrequent. But in some cases the deposits are
   *  frequent and it is more efficient to treat them like primary
   *  inputs and associate them with a change flag.
   *
   *  This routine returns true if this is a depositable net that we
   *  expect will be deposited frequently.
   */
  bool isFrequentDepositable(const NUNetElab* netElab) const;

  //! Test whether an elaborated net is writable (depositable/forcible)
  /*! The scheduler keeps track of all netElabs that are
   *  depositable/forcible. The IODB only keeps track of the original
   *  node that is depositable. So use this if you need to test a
   *  netElab.
   */
  bool isWritable(const NUNetElab* netElab) const;

  //! Get the list of all asynchronous sets/resets (also in clock list)
  /*! This allows test generation routines to determine if a clock is
   *  really a reset and produce a better testing program.

    \verbatim
    Example:
    for (AsyncResetLoop p = sched->loopAsyncResets(); !p.atEnd(); ++p)
    {
      NUNetElab* netElab = p.getKey();
      UInt32* edgeCounts = p.getValue();
      UInt32 posedgeCounts = edgeCounts[eClockPosedge];
      UInt32 negedgeCounts = edgeCounts[eClockNegedge];
      ...
    }
    \endverbatim
  */
  AsyncResetLoop loopAsyncResets();

  //! Get the schedule for nodes that must be run at initialization time
  /*! This routine is used to create a schedule that should be run at
   *  time 0 of the simulation. It contains all initial blocks in the
   *  desing. It may also need to be run if we support a reset
   *  command.
   *
   *  The combinational blocks are in the order they should be
   *  executed.
   *
   *  The following example shows how to get and iterator for the this
   *  schedule

    \verbatim
    Example:
      SCHCombinational* initial = sched->getInitialSchedule();
      if (!initial->empty())
      {
        for (SCHIterator p = initial->getSchedule(); !p.atEnd(); ++p)
	{
          FLNodeElab* node = *p;
	  ...
	}
      }
    \endverbatim
   */
  SCHCombinational* getInitialSchedule();

  //! Get the schedule for nodes that must be run at initialization time
  /*! This routine is used to create a schedule that should be run at
   *  time 0 of the simulation. It contains all the combinational
   *  blocks (no initial blocks) that must be run to have their values
   *  reach a settled state at time 0. This schedule must run after
   *  the initial schedule.  It may also need to be run if we support
   *  a reset command.
   *
   *  The combinational blocks are in the order they should be
   *  executed.
   *
   *  The following example shows how to get and iterator for the this
   *  schedule

    \verbatim
    Example:
      SCHCombinational* initSettle = sched->getInitSettleSchedule();
      if (!initSettle->empty())
      {
        for (SCHIterator p = initSettle->getSchedule(); !p.atEnd(); ++p)
	{
          FLNodeElab* node = *p;
	  ...
	}
      }
    \endverbatim
   */
  SCHCombinational* getInitSettleSchedule();

  //! Get the schedule for nodes that must be run at debug time
  /*! This routine is used to create a schedule that should be run at
   *  whenever the model must be accurate. This can be because the
   *  user is doing vcd dumping or called an API function to access data.
   *
   *  The combinational blocks are in the order they should be
   *  executed.
   *
   *  The following example shows how to get and iterator for the this
   *  schedule

    \verbatim
    Example:
      SCHCombinational* debug = sched->getDebugSchedule();
      if (!debug->empty())
      {
        for (SCHIterator p = debug->getSchedule(); !p.atEnd(); ++p)
	{
          FLNodeElab* node = *p;
	  ...
	}
      }
    \endverbatim
   */
  SCHCombinational* getDebugSchedule();

  //! Get the schedule for nodes that must be run after a force/deposit
  /*! This routine is used to create a schedule that should be run
   *  whenever a force, release, or deposit has occured. This allows
   *  that value to propagatte.
   *
   *  The combinational blocks are in the order they should be
   *  executed.
   *
   *  The following example shows how to get and iterator for the this
   *  schedule

    \verbatim
    Example:
      SCHCombinational* forceDeposit = sched->getForceDepositSchedule();
      if (!debug->empty())
      {
        for (SCHIterator p = forceDeposit->getSchedule(); !p.atEnd(); ++p)
	{
          FLNodeElab* node = *p;
	  ...
	}
      }
    \endverbatim
   */
  SCHCombinational* getForceDepositSchedule();

  //! Return the schedule factory
  SCHScheduleFactory* getFactory() { return mScheduleFactory; }

  //! SCHSchedule print statistics function
  /*! Prints statistics on how well scheduling is doing. This allows
   *  the field to give us feedback on what we can do better.
   *
   *  The data is always printed to a file \<fileRoot\>.scheduleStats, but
   *  if output is true, it is also printed to the screen
   */
  void printScheduleStats(const char* fileRoot, bool output) const;

  //! SCHSchedule print function
  /*! Prints the entire schedule for the design. See the buildSchedule
   *  function for the details on the types of schedules and the data
   *  contained within.
   */
  void print() const;

  //! Raw events may have a downstream clock reference.  This finds the canonical reference
  const NUNetElab* getClockMaster(const NUNetElab*, bool* invert = NULL);
  const NUNetElab* getClockMaster(const STSymbolTableNode*, bool* invert = NULL);

  //! Get the NUNetElab for a clock from its STSymbolTableNode
  static const NUNetElab* clockFromName(const STSymbolTableNode* clkName);

  //! Iterator for all sequential schedules
  SCHSchedulesLoop loopAllSequentials();

  //! Iterator for all combinational schedules
  SCHSchedulesLoop loopAllCombinationals();

  //! Iterator for all simulation combinational schedule (no init or debug)
  SCHSchedulesLoop loopSimulationCombinationals();

  //! Iterator for all simulation schedules (no init or debug)
  SCHSchedulesLoop loopAllSimulationSchedules();

  //! Figure out if a schedule is sequential or combinational
  bool isSequentialSchedule(SCHScheduleType type) const
  {
    SCHScheduleTypeMask typeMask = SCHCreateScheduleTypeMask(type);
    return ((typeMask & eMaskAllSequentials) == typeMask);
  }

  //! Function to create a name for a schedule type
  void scheduleName(SCHScheduleType type, UtString* buf);

  //! Function to determine if a mask is the input mask
  bool isInputMask(const SCHScheduleMask* mask) const;

  //! Generate a SystemC wrapper for the design
  /*!
    \param fileRoot Name of design which can be used as a file root
    \param schedAllClkEdges By default, the systemc wrapper generates
    code optimized for speed, not calling schedule on irrelevant clock
    edges. This changes the default behavior to always call schedule
    on every clock edge.
    \param synthTristates SystemC Wrapper generation normally uses
    only 2 states for best performance. But, if the notion of whether
    or not a net is being driven is needed then the wrapper needs to
    synthesize code to properly handle tristate ports. If true, this will
    synthesize tristate ports. This doesn't change the design in any
    way, only the presentation of the design to the instantiator in
    SystemC.
  */
  void generateSystemCWrapper(const char* fileRoot, bool schedAllClkEdges,
                              bool synthTristates,
    CNetSet &bothEdgeClks,
    PortNetSet &validOutPorts,
    PortNetSet &validInPorts,
    PortNetSet &validTriOutPorts,
    PortNetSet &validBidiPorts,
    CNetSet &clksUsedAsData,
    ClkToEvent &clkEdgeUses );

  //! Compute clock edges and live i/os
  /*!
    If generatingWrapper is true, a sanity check is done on all the
    ports looking for any complex ports we cannot support in the
    wrapper generator. This should be removed once the python-based
    generator is fully functional.
  */
  bool wrapperSensitivity(
    CGPortIface *portIface,
    CNetSet &bothEdgeClks,
    PortNetSet &validOutPorts,
    PortNetSet &validInPorts,
    PortNetSet &validTriOutPorts,
    PortNetSet &validBidiPorts,
    CNetSet &clksUsedAsData,
    ClkToEvent &clkEdgeUses,
    bool generatingWrapper);

  //! Abstraction for clear at end of schedule elaborated nets
  typedef UtMap<const SCHScheduleMask*, NUNetElabVector*> ClearAtEndNets;

  //! Abstraction to iterate over clear at end of schedule elaborated nets
  typedef LoopMap<ClearAtEndNets> ClearAtEndNetsLoop;

  //! Iterate over the clear at end of schedule elaborated nets
  ClearAtEndNetsLoop loopClearAtEndNets();

  //! Check if the schedule has any DCL cycles
  bool hasDCLCycles(void);

  //! Test if a clock is computed in an async schedule
  bool isAsyncClock(const NUNetElab* clkElab) const;

  //! Loop over all the clocks in any async schedule
  SCHClocksLoop loopAsyncClocks() const;

private:
  // Hide copy and assign constructors.
  SCHSchedule(const SCHSchedule&);
  SCHSchedule& operator=(const SCHSchedule&);

  //! Helper function to populate async flow for IODB
  void addAsyncFanin(FLNodeElab* flowElab, FLNodeElab* output, FLNodeElabSet* covered);

  // Helper function to populate the IO data base
  void populateIODB();

  // Debug routine
  void generateCModelDirectives(NUDesign* design, const char* fileRoot);
  void dumpExprSynthesis();


  // Class for dumping schedule information
  SCHDump* mDump;

  // Scheduling Utility Functions
  SCHUtil* mUtil;

  // Classes for creating various schedules (represent processes)
  SCHCycleDetection* mCycleDetection;
  SCHClkAnal* mClkAnal;
  SCHMarkDesign* mMarkDesign;
  SCHScheduleData* mScheduleData;
  SCHTimingAnalysis* mTimingAnalysis;
  SCHCreateSchedules* mCreateSchedules;
  SCHScheduleFactory* mScheduleFactory;
  ArgProc* mArgs;

  class SynthCB; // elab expression callback
  class EnableExprCalculator;
  friend class EnableExprCalculator;
  class ExprImplicitDriveWalker;
}; // class SCHSchedule

#endif // _SCHEDULE_H_
