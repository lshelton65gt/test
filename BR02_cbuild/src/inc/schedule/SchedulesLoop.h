// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _SCHEDULESLOOP_H_
#define _SCHEDULESLOOP_H_

#include "util/LoopMulti.h"

//! Class to allow iteration over all schedules
/*!
 * The order that schedules are iterated is an attempt at a frequency of
 * execution ordering.  Note that no analysis is performed; the ordering
 * is static.  The ordering is:
 *  1. DCL
 *  2. Sequential
 *  3. Transition
 *  4. Sample Combinational
 *  5. Input
 *  6. Async Combinational
 *  7. Initial
 *  8. Debug
 *  9. Force/Deposit
 */
class SCHSchedulesLoop
{
public:
  //! empty constructor
  SCHSchedulesLoop() { mIndex = 0; }

  //! constructor
  SCHSchedulesLoop(SCHScheduleData*, SCHScheduleTypeMask);

  //! destructor
  ~SCHSchedulesLoop() {}

  //! move to the next schedule
  void operator++() { ++mIndex; }

  //! Are we at the end
  bool atEnd() const { return mIndex >= mSchedules.size(); }

  //! Get the general schedule
  SCHScheduleBase* getScheduleBase() const { return mSchedules[mIndex]; }

  //! Get the combinational schedule (or NULL)
  SCHCombinational* getCombinationalSchedule() const;

  //! Get the sequential schedule (or NULL)
  SCHSequential* getSequentialSchedule() const;

  //! type to make an iterator over multiple schedules for sequentials
  typedef LoopMulti<SCHIterator> Iterator;

  //! Get an iterator for the schedule
  /*! This routine returns a Iterator for the schedule. For
   *  combinational schedules there is only one schedule but for
   *  sequential schedules there are up to two schedules. This routine
   *  combines the schedules into one for sequential schedules. If the
   *  edge of the schedule is important, then don't use this routine.
   */
  Iterator getSchedule() const;

private:
  // Vector of schedules and their types
  UtVector<SCHScheduleBase*> mSchedules;
  unsigned int mIndex;
}; // class SCHSchedulesLoop

#endif // _SCHEDULESLOOP_H_
