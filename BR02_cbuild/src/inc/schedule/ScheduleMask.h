// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef _SCHEDULE_MASK_H_
#define _SCHEDULE_MASK_H_


#include <cassert>
#include "util/CarbonTypes.h"
#include "util/CarbonAssert.h"
#include "util/UtArray.h"
#include "util/Util.h"

class ZostreamDB;
class ZistreamDB;

/*!
  \file
  Class objects to manage SCHScheduleMasks
*/

class SCHSchedule;
class SCHEvent;
class STSymbolTableNode;
class STBranchNode;

//! SCHScheduleMask Class
/*! A SCHScheduleMask indicates the events on which a node may
 *  transition or get sampled.
 *
 *  SCHScheduleMasks, once assigned, are never modified, so they can
 *  be shared.  We keep track of the number of users of a reference
 *  count so we know which schedules to synthesize.
 *
 *  Today the memory for a SCHScheduleMask is not freed when the
 *  reference count goes to zero. This is because it is very likely
 *  that the same SCHScheduleMask will be re-created again in the
 *  future.
 */
class SCHScheduleMask
{
private:
  struct EventPtrCmp
  {
    bool operator()(const SCHEvent* const& ev1, const SCHEvent* const& ev2)
      const;
  };

public: CARBONMEM_OVERRIDES
  //! constructor
  SCHScheduleMask();

  typedef UtHashSet<const SCHEvent*> EventPtrSet;
  typedef UtArray<const SCHEvent*> EventVector;

  //! construct from a set of events (used by factory)
  SCHScheduleMask(const EventPtrSet& events, UInt32 numClkNets);

  //! Special constructor to incrementally build a new schedule mask
  //! from two existing ones.  This has got its own constructor because
  //! we can take advantage of the fact that the masks are pre-sorted,
  //! so we can do a merge sort.
  SCHScheduleMask(const SCHScheduleMask* m1,
                  const SCHScheduleMask* m2);
  
  //! destructor
  ~SCHScheduleMask();
      
  //! copy ctor
  SCHScheduleMask(const SCHScheduleMask& src);

  //! get number of Signatures that have this SCHScheduleMask
  int getRefCnt() const { return mRefCnt; }
  
  //! assign this SCHScheduleMask to a Signature
  const SCHScheduleMask* assign() const { ++mRefCnt; return this; }

  //! de-assign this SCHScheduleMask from a Signature
  /*!
   * This happens when all Signatures that use this mask get deleted.
   * Note that SCHScheduleMasks, once assigned cannot be mutated.
   */
  void release() const
  {
    // For now, never free these
    --mRefCnt;
    INFO_ASSERT(mRefCnt >= 0, "Schedule mask release exceeds allocation");
  }

  //! Copy a schedule mask.  This cannot be run after the Mask is assigned.
  SCHScheduleMask& operator=(const SCHScheduleMask& src);

  //! Typedef to loop over the set of events in a schedule mask
//#if MASK_USE_HASH_SET
//  typedef EventPtrSet::SortedLoop SortedEvents;
//  typedef EventPtrSet::UnsortedCLoop UnsortedEvents;
//#else
//  typedef CLoop<EventPtrSet> SortedEvents;
//  typedef SortedEvents UnsortedEvents;
//#endif
  typedef CLoop<EventVector> SortedEvents;
  typedef SortedEvents UnsortedEvents;

  //! Are two masks equivalent?  This is used in hashing.
  bool operator==(const SCHScheduleMask& mask) const;

  //! Routine used to create a ScheduleMask hash set MaskSet
  size_t hash() const;

  //! Checks if a clock is present in the mask
  bool isClockPresent(const STSymbolTableNode* clk) const;

  //! Create a UtString that represents this SCHScheduleMask
  /*! The caller provides the storage for a UtString where this routine
   *  will put a UtString representation of this schedule mask. The
   *  schedule mask tells us to the events a piece of logic
   *  is sensitive to. See the SCHEvent documentation for more
   *  details on legal events.
   *
   *  \param buf A UtString buffer in which this routine appends the  event
   *  \param scope The scope for this SCHScheduleMask
   *  \param includeRoot if true and hierNameis true then include the
   *  root as part of the name
   *  \param hierName if true then generate a hierarchical name
   *  \param separator string to use between hierarchical name segments
   *
   */
  void compose(UtString* buf, const STBranchNode* scope, bool includeRoot = true,
               bool hierName = true, const char* separator = ".") const;

  //! Iterator to traverse the list of events associated with a schedule mask
  /*! Use this iterator to get access to a SCHEventPtr which is a
      pointer to a SCHEvent. You can use this to access the conditions
      under which a schedule should execute. See the SCHEvent
      documentation to find out how to interpret the events.
   
      The following example shows how to loop through the events for a
      schedule mask.
   
      \verbatim
      for (UnsortedEvents l = loopEvents(); !l.atEnd(); ++l)
      {
        const SCHEvent* event = *l;
        if (event->isPrimaryInput())
   	  ...
        else if (event->isPrimaryOutput())
          ...
        else
   	{
          // must be a clock or asynchronous events (asynchronous
	  // events are overloaded into clocks)
   	  INFO_ASSERT(event->isClockEvent(), "Clock event must be a clock");
          ...

	  // Check the priority of this event, non-zero means it
	  // is an asynchronous set or reset.
	  UInt32 priority = event->getPriority();
   	}
      }
      \endverbatim
   */
#if MASK_USE_HASH_SET
  UnsortedEvents loopEvents() const;
  SortedEvents loopEventsSorted() const;
#else
  UnsortedEvents loopEvents() const;
  SortedEvents loopEventsSorted() const;
#endif

  //! debug routine
  void print() const;

  //! return the number of edges to which this mask is sensitive
  size_t numEdges() const;

  //! return the number of unique event nets
  size_t numEventNets() const;

  //! Compare two SCHScheduleMasks.
  /*! This compare function does not really tell if a schedule mask is
   *  a subset of another one. It is simply here to create a
   *  consistent order between schedule masks so that the maps work
   *  consistently.
   *
   *  To tell if a SCHScheduleMask m1 is a subset of SCHScheduleMask
   *  m2, then combine the two schedule masks and if the new mask is
   *  identical to m2, then m1 is a subset of m2.
   */
  static int compare(const SCHScheduleMask* m1, const SCHScheduleMask* m2);

  bool operator<(const SCHScheduleMask& other) const
  {
    return compare(this, &other) < 0;
  }

  //! Put the index for this schedule mask
  /*! The index is used to determine relative frequency of execution
   *  for schedule masks. See the ScheduleFactory for details. This
   *  should not be used except by the ScheduleFactory code.
   */
  void putIndex(UInt32 index) const { mIndex = index; }

  //! Get the index for this schedule mask
  /*! The index is used to determine relative frequency of execution
   *  for schedule masks. See the ScheduleFactory for details. This
   *  should not be used except by the ScheduleFactory code.
   */
  UInt32 getIndex(void) const { return mIndex; }

  bool dbWrite(ZostreamDB& db) const;
  bool dbRead(ZistreamDB& db);

  //! Does the mask have the specified event?
  bool hasEvent(const SCHEvent*) const;

  //! Does this mask include another mask in its entirety?
  bool includes(const SCHScheduleMask* smallMask, bool ignoreConstants) const;

  //! clear out all the events in the mask
  void clear();

  bool hasInput() const;
  bool hasOutput() const;
  bool isConstant() const;

private:
  void computeEventNets() const;

  mutable int mRefCnt;
  EventVector mEvents;
  mutable UInt32 mNumClkNets;
  mutable UInt32 mIndex;
}; // class SCHScheduleMask


#endif // _SCHEDULE_MASK_H_
