// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef _SIGNATURE_H_
#define _SIGNATURE_H_


class ZistreamDB;
class ZostreamDB;

/*!
  \file
  Class object to manage Signatures.
*/

class SCHScheduleMask;

//! A Signature Class to store timing in flow nodes.
/*! A Signature is a schedule mask pair to indicate when a node
 *  transitions and when it is sampled.
 *
 *  We store the signature in flow nodes. Signatures were created to
 *  take advantage of the fact that there will probably be less unique
 *  transition/sample pairs than there are flow nodes in a
 *  design. This way we save on a pointer.
 *
 *  Today the memory for a Signature is not freed when the reference
 *  count goes to zero. This is because it is very likely that the
 *  same Signature will be re-created again in the future.
*/
class SCHSignature {
public: CARBONMEM_OVERRIDES
  //! constructor with only a transition mask
  /*! Creates a signature with a valid transition mask and empty
   *  sample mask. The reference count for this object starts at zero
   *  so that it can be modified. Only when it get assigned does the
   *  object become read only.
   *
   *  \param transitionMask A pointer to a  transition mask.
   */
  SCHSignature(const SCHScheduleMask* transitionMask)
  {
    INFO_ASSERT(transitionMask != NULL,
                "Creating schedule signature with a null transition mask");

    mRefCnt = 0;
    mTransition = transitionMask;
    mSample = NULL;
  }

  //! constructor with both a transition and sample mask
  /*! Creates a signature with a valid transition mask and sample
   *  mask. See the transition mask only constructor for information
   *  on how the reference count is handled.
   *
   *  \param transitionMask A pointer to a transition mask.
   *  \param sampleMask A pointer to a sample mask.
   */
  SCHSignature(const SCHScheduleMask* transitionMask, const SCHScheduleMask* sampleMask)
  {
    mRefCnt = 0;
    mTransition = transitionMask;
    mSample = sampleMask;
  }

  //! empty contructor for DB read
  SCHSignature()
  {
    mRefCnt = 0;
    mTransition = NULL;
    mSample = NULL;
  }

  //! destructor
  ~SCHSignature()
  {
  }

  //! assign this SCHSignature to a flow node
  /*! A signature is assigned to a flow node so that the scheduler can
   *  determine when that node transitions and samples.
   *
   *  A signature should be assigned when it is placed on a flow node
   *  so that the memory does not get deleted and so that we guarantee
   *  that the signature isn't changed by some process operating on a
   *  different flow node with the same signature.
   */
  const SCHSignature* assign() const
  {
    // Assign the schedule masks as well
    if (mTransition != NULL)
      mTransition->assign();
    if (mSample != NULL)
      mSample->assign();

    ++mRefCnt;
    return this;
  }

  //! de-assign this SCHSignature from a flow node
  /*!
   * This happens when a node has been proved sensitive to another
   * clock edge or when the sampling information changes.  Note that
   * SCHSignatures, once assigned cannot be mutated.
   *
   * By deassigning this signature we could potentially free unused
   * signatures.
   *
   * \todo Look into freeing the memory when a signature is no longer
   * needed.
   */
  void release() const
  {
    // Release the schedule masks
    if (mTransition != NULL)
      mTransition->release();
    if (mSample != NULL)
      mSample->release();

    // For now, never free these
    --mRefCnt;
    INFO_ASSERT(mRefCnt >= 0, "Schedule signature release exceeds allocation");
  }

  //! Get the transition mask for this signature
  const SCHScheduleMask* getTransitionMask() const { return mTransition; }

  //! Get the sample mask for this signature
  const SCHScheduleMask* getSampleMask() const { return mSample; }

  //! Copy a signature.  This cannot be run after the SCHSignature is assigned.
  SCHSignature& operator=(const SCHSignature& src)
  {
    if (&src != this)
    {
      INFO_ASSERT(mRefCnt == 0, "Overwriting an assigned schedule signature");
      mTransition = src.mTransition;
      mSample = src.mSample;
    }
    return *this;
  }

  //! Are two signatures equivalent?  This is used in the set.
  bool operator==(const SCHSignature& signature) const
  {
    return ((mTransition == signature.mTransition) &&
	    (mSample == signature.mSample));
  }

  //! Returns a hash value to support the SCHSignatureSet type.
  size_t hash() const
  {
    size_t hashval = 0;
    hashval += ((size_t) mTransition) + ((size_t) mSample);
    return hashval;
  }

  //! A debug routine to print a signature.
  void print() const;

  bool dbWrite(ZostreamDB& db) const;
  bool dbRead(ZistreamDB& db);

  //! compare two signatures (schedule and mask)
  static int compare(const SCHSignature* s1, const SCHSignature* s2)
  {
    int cmp = 0;
    if (s1->mTransition && !s2->mTransition)
      cmp = -1;
    else if (!s1->mTransition && s2->mTransition)
      cmp = 1;
    else if (s1->mTransition)
      cmp = SCHScheduleMask::compare(s1->mTransition, s2->mTransition);

    if (cmp == 0)
    {
      if (s1->mSample && !s2->mSample)
        cmp = -1;
      else if (!s1->mSample && s2->mSample)
        cmp = 1;
      else if (s1->mSample)
        cmp = SCHScheduleMask::compare(s1->mSample, s2->mSample);
    }
    return cmp;
  } // static int compare

  bool operator<(const SCHSignature& other) const {
    return compare(this, &other) < 0;
  }

private:
  //! get the number of nodes that have this SCHSignature
  int getRefCnt()
  {
    return mRefCnt;
  }

  mutable int		  mRefCnt;	// Allows us to delete unused copies
  const SCHScheduleMask*  mTransition;	// When this block transitions
  const SCHScheduleMask*  mSample;	// When this block is sampled
};

#endif // _SIGNATURE_H_
