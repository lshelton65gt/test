// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  
  Class definition for the base of all schedules
*/

#ifndef _SCHEDULEBASE_H_
#define _SCHEDULEBASE_H_


class SCHUtil;
class SCHSequential;
class SCHSequentialEdge;
class SCHCombinational;
class SCHDerivedClockLogic;
class SCHDerivedClockCycle;
class SCHMarkDesign;

//! SCHScheduleBase class
/*! This class is the base from which all sub-schedules are
 *  created. There are currently two sub-schedule types sequential
 *  and combinational.
 */
class SCHScheduleBase
{
public:
  //! constructor
  SCHScheduleBase(SCHScheduleType type) :
    mScheduleType(type), mBranchNets(NULL)
  {}

  //! destructor
  virtual ~SCHScheduleBase() {}

  //! get the schedule type
  SCHScheduleType getScheduleType() const { return mScheduleType; }

  //! Function to compare two schedules in a predictable way
  virtual int compareSchedules(const SCHScheduleBase* schedBase) const = 0;

  //! Function to compare schedule
  bool operator<(const SCHScheduleBase& other) const
  {
    return compareSchedules(&other) < 0;
  }

  //! Function to compare schedules
  static int compare(const SCHScheduleBase* s1, const SCHScheduleBase* s2)
  {
    int cmp = s1->getScheduleType() - s2->getScheduleType();
    if (cmp == 0) {
      cmp = s1->compareSchedules(s2);
    }
    return cmp;
  }

  //! Function to print details on this schedule
  virtual void print(bool printNets, SCHMarkDesign* mark = NULL) const = 0;

  //! Function to print information for -dumpSchedule
  virtual void dump(int indent, SCHMarkDesign* mark) const = 0;

  //! Create a dump string for this schedules branch nets
  void composeBranchNets(UtString* buf, SCHMarkDesign* mark) const;

  //! Set the branch nets for this schedule
  /*! The branch nets are the set of nets that must change for this
   *  schedule to do any work. This is an alternative scheduling
   *  technique on top of the schedule mask. It allows us to branch
   *  around a larger number of schedules when they have the same
   *  branch nets but different schedule masks.
   */
  void putBranchNets(const SCHInputNets* branchNets)
  {
    mBranchNets = branchNets;
  }

  //! Get the branch nets for this schedule
  const SCHInputNets* getBranchNets(void) const { return mBranchNets; }

  //! Loop the branch nets for this schedule. mBranchNets must NOT be null
  SCHInputNetsCLoop loopBranchNets(void) const;

  //! Print assertion header information for this schedule
  /*! This should be used in conjuction with printAssertBody() and
   *  printAssertTrailer().
   */
  static void printAssertHeader(const char* file, int line, const char* exprStr,
                                const char* assertName);

  //! Print the body of an assert for this schedule
  /*! This should be used in conjuction with printAssertHeader() and
   *  printAssertTrailer().
   */
  void printAssertBody(void) const;

  //! Print assertion trailer information for this schedule
  /*! This should be used in conjuction with printAssertHeader() and
   *  printAssertBody().
   */
  static void printAssertTrailer(void);

  //! Cast to a sequential schedule (if it is one) or NULL
  virtual SCHSequential* castSequential() { return NULL; }

  //! Cast to a sequential edge schedule (if it is one) or NULL
  virtual SCHSequentialEdge* castSequentialEdge() { return NULL; }

  //! Cast to a combinational schedule (if it is one) or NULL
  virtual SCHCombinational* castCombinational() { return NULL; }

  //! Cast to a derived clock logic schedule (if it is one) or NULL
  virtual SCHDerivedClockLogic* castDerivedClockLogic() { return NULL; }

  //! Cast to a derived clock cycle schedule (if it is one) or NULL
  virtual SCHDerivedClockCycle* castDerivedClockCycle() { return NULL; }

  //! Cast to a sequential schedule (if it is one) or NULL
  virtual const SCHSequential* castSequential() const { return NULL; }

  //! Cast to a sequential edge schedule (if it is one) or NULL
  virtual const SCHSequentialEdge* castSequentialEdge() const { return NULL; }

  //! Cast to a combinational schedule (if it is one) or NULL
  virtual const SCHCombinational* castCombinational() const { return NULL; }

  //! Cast to a derived clock logic schedule (if it is one) or NULL
  virtual const SCHDerivedClockLogic* castDerivedClockLogic() const {return NULL; }

  //! Cast to a derived clock cycle schedule (if it is one) or NULL
  virtual const SCHDerivedClockCycle* castDerivedClockCycle() const {return NULL; }


protected:
  //! Helper function for dumping the indentation
  static void dumpIndent(int indent);

  //! Helper function for dumping a flow node
  static void dumpFlowNode(FLNodeElab* flow, SCHScheduleType type, int index,
                           int indent, SCHMarkDesign* mark);

private:
  //! Hide copy and assign constructors.
  SCHScheduleBase(const SCHScheduleBase&);
  SCHScheduleBase& operator=(const SCHScheduleBase&);

  SCHScheduleType mScheduleType;
  const SCHInputNets* mBranchNets;
};

// Note: this macro is written this way so that it can be invoked like a
// function, with the final semicolon supplied by the user.  A brace-delimited
// statement is no good, because it leads to syntax errors with code like:
// if (cond)
//   SCHED_ASSERT(predicate, sched);
// else
//   ...
#define SCHED_ASSERT(exp, sched) \
  do { \
    if (!(exp)) { \
      SCHScheduleBase::printAssertHeader(__FILE__, __LINE__, __STRING(exp), \
                                         "SCHED_ASSERT"); \
      sched->printAssertBody(); \
      SCHScheduleBase::printAssertTrailer(); \
    } \
  } while (0) /* no trailing semicolon -- it is supplied by the caller */

#define SCHED_ASSERT2(exp, sched1, sched2) \
  do { \
    if (!(exp)) { \
      SCHScheduleBase::printAssertHeader(__FILE__, __LINE__, __STRING(exp), \
                                         "SCHED_ASSERT2"); \
      sched1->printAssertBody(); \
      sched2->printAssertBody(); \
      SCHScheduleBase::printAssertTrailer(); \
    } \
  } while (0) /* no trailing semicolon -- it is supplied by the caller */

#endif // _SCHEDULEBASE_H_
