// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _EVENT_H_
#define _EVENT_H_


#include "util/CarbonAssert.h"
#include "util/CarbonTypes.h"
#include "symtab/STSymbolTableNode.h" 
#include "util/UtString.h"

class ZostreamDB;
class ZistreamDB;

//! A class to represent an execution event.
/*! A SCHEvent represents a condition under which logic may execute or
 *  be sampled. See SCHScheduleMask for details on how events are
 *  used. The possible events include:
 *
 *    - The logic is sensitive to a design primary input.
 *
 *    - The logic is sampled by a design primary output.
 *
 *    - The logic is sensitive to or sampled by logic that executes on
 *      a clock transition. The valid clock transitions are posedge and
 *     	negedge.
 * 
 *  There are a set of functions to determine the type of event and
 *  some of the associated information. To tell what type of event it
 *  is use isPrimaryInput(), isPrimaryOutput(), or isClockEvent(). To
 *  get the clock information use getClock() and getClockEdge().
 *
 *  Some sequential blocks may have asynchronous set/reset. These
 *  blocks have been split apart and now the individual parts are
 *  sensitive to more than one clock in a priority order. So some
 *  clock events have a priority other than 0 (the default priority
 *  for an ordinary clock). The higher the priority schedules override
 *  the lower priority schedules.
 *
 *  The ScheduleMaskFactory only allows one of these to exist, for any
 *  event, via a hash table.  Thereafter, SCHEvent* can be safely
 *  compared with ==.
 */
class SCHEvent
{
public: CARBONMEM_OVERRIDES
  //! constructor for a synchronous clock edge or asynchronous reset/set
  /*! Asynchronous set/reset and clock edges are treated as the same
   *  type of event. The only difference is that asynchronous
   *  set/reset events have a higher priority than 0. All clock edge
   *  events have a priority of 0.
   *
   *  \param clock The clock/set/reset signal
   *
   *  \param edge Either eClockPosedge or eClockNegedge
   *
   *  \param priority 0 for clock events and 1 or greater for
   *  asynchronous set/reset events. The priority dictates the
   *  precedence of the operation. The higher the priority, the higher
   *  the precedence.
   */
  SCHEvent(const STSymbolTableNode* clock, ClockEdge edge, UInt32 priority)
  {
    mClock = clock;
    mEdge = edge;
    mEventType = eClockEdge;
    mPriority = priority;
  }

  //! construction for an I/O or constant
  SCHEvent(EventType eventType)
  {
    mClock = NULL;
    mEventType = eventType;
    INFO_ASSERT((eventType == eIn) || (eventType == eOut) ||
                (eventType == eConstant),
                "Invalid schedule event type");
    mPriority = 0;
    mEdge = eClockPosedge; // init all fields
  }

  //! no-arg constructor for DB read
  SCHEvent()
  {
    mClock = NULL;
    mEventType = eConstant;
    mPriority = 0;
    mEdge = eClockPosedge; // init all fields
  }

  //! equality operator
  bool operator==(const SCHEvent& ev) const
  {
    return ((mEventType == ev.mEventType) &&
	    (mClock == ev.mClock) && (mEdge == ev.mEdge) &&
	    (mPriority == ev.mPriority));
  }

  //! hash function.  guess where this is used.
  size_t hash() const
  {
    return ((size_t) mEdge) | ((size_t) mClock) | ((size_t) mPriority);
  }

  //! debug routine to print this SCHEvent
  void print() const;

  //! debug routine to convert the event to a string.
  void compose(UtString* buf, const STBranchNode* scope, bool includeRoot = true,
               bool hierName = true, const char* separator = ".") const;

  //! is this a event a primary input?
  bool isPrimaryInput() const {return mEventType == eIn;}

  //! is this event a primary output?
  bool isPrimaryOutput() const {return mEventType == eOut;}

  //! is this event a constant event?
  bool isConstant() const { return (mEventType == eConstant); }

  //! is this event a clock transition?
  bool isClockEvent() const
  {
    return (mEventType == eClockEdge) && (mClock != NULL);
  }

  //! get the clock for an event.
  const STSymbolTableNode* getClock() const {
    INFO_ASSERT(isClockEvent(), 
                "getClock() only legal on schedule clock event type");
    return mClock;
  }

  //! Get the modifiable clock node
  STSymbolTableNode* getClockModify() const {
    return const_cast<STSymbolTableNode*>(mClock);
  }
  
//rjc  void putClock(const STSymbolTableNode* clock) {
//rjc    ST_ASSERT(isClockEvent(), clock);
//rjc    mClock = clock;
//rjc  }

  //! get the edge for a clock event
  ClockEdge getClockEdge() const
  {
    INFO_ASSERT(isClockEvent(),
                "getClockEdge() only legal on schedule clock event type");
    return mEdge;
  }

  //! get the priority for an async event
  UInt32 getPriority() const
  {
    INFO_ASSERT(isClockEvent(),
                "getPriority() only legal on schedule clock event type");
    return mPriority;
  }

  //! compares two events
  static int compare(const SCHEvent* ev1, const SCHEvent* ev2)
  {
    // Check if they are the same type of event
    int cmp = ((int) ev1->mEventType) - ((int) ev2->mEventType);
    if (cmp != 0)
      return cmp;

    // Same type of event, for constant or PI, return they are the same
    if (ev1->mEventType != eClockEdge)
      return 0;

    // For async and clock, sort further (priority, name, edge)
    cmp = ((int) ev1->mPriority) - ((int) ev2->mPriority);
    if( cmp != 0 )
      return cmp;
      
    cmp = STSymbolTableNode::compare(ev1->mClock, ev2->mClock);
    if( cmp != 0 )
      return cmp;
    
    cmp = ev1->mEdge - ev2->mEdge;

    return cmp;
  } // static int compare

  int operator<(const SCHEvent& ev1) const
  {
    return compare(this, &ev1) < 0;
  }

  bool dbWrite(ZostreamDB& db) const;
  bool dbRead(ZistreamDB& db);

private:
  const STSymbolTableNode* mClock;
  ClockEdge mEdge;
  EventType mEventType;
  UInt32 mPriority;
}; // class SCHEvent

#endif // _EVENT_H_
