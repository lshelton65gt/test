// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __BDD_SOURCE_H__
#define __BDD_SOURCE_H__

#include "bdd/BDD.h"

class FLNodeElab;

//! Abstract base class for controlling BDD construction
class BDDSource
{
public:
  //! Destructor
  virtual ~BDDSource() {};

  //! Return a BDD representing true
  virtual BDD trueBDD() const = 0;

  //! Return a BDD representing false
  virtual BDD falseBDD() const = 0;

  //! Return a representation of an invalid BDD
  virtual BDD invalid() const = 0;

  //! Return the inversion of a BDD
  virtual BDD opNot(const BDD& bdd) const = 0;

  //! Return the AND of two BDDs
  virtual BDD opAnd(const BDD& bdd1, const BDD& bdd2) const = 0;

  //! Return the OR of two BDDs
  virtual BDD opOr(const BDD& bdd1, const BDD& bdd2) const = 0;

  //! Return true if one BDD implies another
  virtual bool implies(const BDD& assumption, const BDD& conclusion) const = 0;
  
  //! Perform any necessary pre-analysis before computing a BDD
  virtual void seed(const FLNodeElab* flow) = 0;

  //! Construct a BDD for the condition of an elaborated flow for an If statement
  virtual BDD bdd(const FLNodeElab* ifFlow) = 0;
};


//! A simple expression-to-BDD source
class SimpleBDDSource : public BDDSource
{
public:
  //! Constructor with optional BDDContext
  SimpleBDDSource(BDDContext* context = NULL);

  //! Destructor
  virtual ~SimpleBDDSource();

  //! Return a BDD representing true
  virtual BDD trueBDD() const;

  //! Return a BDD representing false
  virtual BDD falseBDD() const;

  //! Return a representation of an invalid BDD
  virtual BDD invalid() const;

  //! Return the inversion of a BDD
  virtual BDD opNot(const BDD& bdd) const;

  //! Return the AND of two BDDs
  virtual BDD opAnd(const BDD& bdd1, const BDD& bdd2) const;

  //! Return the OR of two BDDs
  virtual BDD opOr(const BDD& bdd1, const BDD& bdd2) const;

  //! Return true if one BDD implies another
  virtual bool implies(const BDD& assumption, const BDD& conclusion) const;

  //! Perform any necessary pre-analysis before computing a BDD
  virtual void seed(const FLNodeElab* flow);

  //! Construct a BDD for an if condition or always block edge condition
  virtual BDD bdd(const FLNodeElab* flow);

private:
  bool        mWeOwnContext;

protected:
  BDDContext* mBDDContext;
};

#endif // __BDD_SOURCE_H__

