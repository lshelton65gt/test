// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 
  
#ifndef __LangCppType_h_
#define __LangCppType_h_

#include "util/UtString.h"

class LangCppStaticCast;
class LangCppVariable;

class LangCppClassType;
class LangCppPodType;
class LangCppPointerType;
class LangCppReferenceType;
class LangCppVoidPointer;

//! C++ type
class LangCppType
{
public:
  CARBONMEM_OVERRIDES

  //! Architecture that a type is representing
  /*!
    Note that the self-determined architecture uses
    sizeof(CarbonSIntPtr) to determine pointer size.
  */
  enum Architecture {
    eArch32, //!< 32-bit architecture
    eArch64, //!< 64-bit architecture
    eArchSelf //!< Self-determined
  };

  //! Set the architecture, which is defaulted to eArchSelf.
  static void sPutArchitecture(Architecture arch);
  
  //! Get the size of a pointer in bytes for this arch
  static SInt32 sGetPointerSize();

  //! Sign type enumeration
  enum SignT {
    eUnsigned,
    eSigned
  };


  LangCppType(const char* typeName, SignT sign);
  LangCppType(LangCppType* ref);
  virtual ~LangCppType();

  //! Get the size in bytes of an identifier of this type
  /*!
    This returns a signed integer in case we want to declare a
    specialized type that has an invalid width.
  */
  virtual SInt32 getByteWidth() const = 0;

  //! Type reference
  LangCppReferenceType* reference();
  //! Pointer to this type
  LangCppPointerType* pointer();

  SignT getSign() const { return mSign; }
  
  const char* getSignStr() const;

  const UtString& getTypeNameStr() const { return mTypeName; }
  
  virtual const LangCppClassType* castClass() const;
  virtual const LangCppPodType* castPod() const;
  //! Cast to void*
  virtual const LangCppVoidPointer* castVoidP() const;
  virtual const LangCppPointerType* castPointerType() const;
  virtual const LangCppReferenceType* castReference() const;

  LangCppClassType* castClass()
  {
    const LangCppType* me = const_cast<const LangCppType*>(this);
    return const_cast<LangCppClassType*>(me->castClass());
  }

  LangCppPodType* castPod()
  {
    const LangCppType* me = const_cast<const LangCppType*>(this);
    return const_cast<LangCppPodType*>(me->castPod());
  }

  LangCppVoidPointer* castVoidP()
  {
    const LangCppType* me = const_cast<const LangCppType*>(this);
    return const_cast<LangCppVoidPointer*>(me->castVoidP());
  }


  LangCppPointerType* castPointerType()
  {
    const LangCppType* me = const_cast<LangCppType*>(this);
    return const_cast<LangCppPointerType*>(me->castPointerType());
  }
  

  LangCppReferenceType* castReference()
  {
    const LangCppType* me = const_cast<const LangCppType*>(this);
    return const_cast<LangCppReferenceType*>(me->castReference());
  }

  //! Append type information to msg string
  virtual void compose(UtString* msg) const = 0;

  //! Emit type into the code stream
  /*!
    This emits a type use into the code stream. Normally, you do
    not emit a type use, but if you are doing a static_cast, then the
    the type within the <> is a use of the type. In the case we are
    static-casting a pointer type to a non-pointer type, and the
    non-pointer type is a reference, we do not want to emit an &.

    E.g., if we called staticCast(myClassType->reference(),
    myPointer), the code that emits the static cast knows we need to
    have a pointer type in between the <>, but we don't want to emit
    the & of the reference in this case.
  */
  virtual void emitUse(UtOStream& out) const;

  //! Emit a variable declaration into the code stream
  virtual void emitDeclare(UtOStream& out, const LangCppVariable* var) const;
  virtual void emitDeclare(UtOStream& out) const;

  //! Emit reference operator. (class only)
  /*!
    If this is not a class type or a pointer to a class type, this
    asserts.
  */
  virtual void emitRefOperator(UtOStream& out) const;


  //! Object comparison for sets and general compare
  ptrdiff_t compare(const LangCppType* otherType) const;

  //! Create a static_cast expression for a variable
  LangCppStaticCast* staticCast(LangCppVariable* hdl);

protected:
  UtString mTypeName;
  SignT mSign;
  LangCppReferenceType* mReference;
  LangCppPointerType* mPointer;

  static Architecture smArchitecture;

  virtual ptrdiff_t compareHelper(const LangCppType* otherType) const = 0;

private:
  void init(const char* typeName, SignT sign);
};

//! The base class of all pointer types
class LangCppVoidPointer : public LangCppType
{
public:
  CARBONMEM_OVERRIDES

  LangCppVoidPointer();

  virtual ~LangCppVoidPointer();
  virtual void compose(UtString* msg) const;
  virtual void emitUse(UtOStream& out) const;
  virtual void emitDeclare(UtOStream& out, const LangCppVariable* var) const;
  virtual void emitDeclare(UtOStream& out) const;

  virtual const LangCppVoidPointer* castVoidP() const;

  virtual SInt32 getByteWidth() const;
  
protected:
  virtual ptrdiff_t compareHelper(const LangCppType* otherType) const;
  //! emit the typename of this pointer
  /*!
    Used by emitDeclare to distinguish void v. the type we are pointing
    to. 
  */
  virtual void emitTypeName(UtOStream& out) const;
};

//! A pointer to a concrete type
class LangCppPointerType : public LangCppVoidPointer
{
public:
  CARBONMEM_OVERRIDES

  LangCppPointerType(LangCppType* pointed);
  virtual ~LangCppPointerType();
  virtual void compose(UtString* msg) const;
  virtual void emitUse(UtOStream& out) const;

  virtual void emitRefOperator(UtOStream& out) const;

  virtual const LangCppPointerType* castPointerType() const;

  const LangCppType* getPointed() const { return mPointed; }
  
protected:
  virtual ptrdiff_t compareHelper(const LangCppType* otherType) const;
  virtual void emitTypeName(UtOStream& out) const;

private:
  LangCppType* mPointed;
};

//! A reference to a type
class LangCppReferenceType : public LangCppType
{
public:
  CARBONMEM_OVERRIDES

  LangCppReferenceType(LangCppType* referenced);
  virtual ~LangCppReferenceType();
  
  virtual const LangCppClassType* castClass() const;
  virtual const LangCppPodType* castPod() const;

  virtual void compose(UtString* msg) const;
  virtual void emitUse(UtOStream& out) const;
  virtual void emitDeclare(UtOStream& out) const;
  virtual void emitDeclare(UtOStream& out, const LangCppVariable* var) const;

  virtual void emitRefOperator(UtOStream& out) const;

  //! Returns the byte width of what this is referencing.
  virtual SInt32 getByteWidth() const;
  
protected:
  virtual ptrdiff_t compareHelper(const LangCppType* otherType) const;

private:
  LangCppType* mReferencedType;
};

//! A pod (non-class) type
class LangCppPodType : public LangCppType
{
public:
  CARBONMEM_OVERRIDES

  LangCppPodType(const char* typeName, UInt32 byteWidth, SignT sign);
  virtual ~LangCppPodType();

  virtual const LangCppPodType* castPod() const;


  UInt32 getNumBytes() const { return mNumBytes; }

  virtual void compose(UtString* msg) const;

  virtual SInt32 getByteWidth() const;

protected:
  virtual ptrdiff_t compareHelper(const LangCppType* otherType) const;

private:
  UInt32 mNumBytes;
};

//! Class type
/*!
  Class types are pretty simple currently. They are mainly used to
  help emit method call and member variables of a class variable.
*/
class LangCppClassType : public LangCppType
{
public:
  CARBONMEM_OVERRIDES

  LangCppClassType(const char* typeName);
  virtual ~LangCppClassType();

  virtual const LangCppClassType* castClass() const;

  virtual void compose(UtString* msg) const;

  virtual void emitRefOperator(UtOStream& out) const;

  //! Returns the size of this class
  /*!
    We have to return a valid number so that we can use the type to
    create identifiers in the expression factory.

    Class members are not yet catalogued when creating a class or
    using a class. If we decide to do that, we can change this to
    actually return the number of bytes of the members including
    alignment. So, currently, upon creation of a type, we set it to
    4. But, it is user-settable with putByteWidth().
  */
  virtual SInt32 getByteWidth() const;
  
  void putByteWidth(SInt32 numBytes);

protected:
  virtual ptrdiff_t compareHelper(const LangCppType* otherType) const;

private:
  SInt32 mNumBytes;
};

#endif
