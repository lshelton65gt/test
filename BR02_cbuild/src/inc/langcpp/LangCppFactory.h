// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  A factory that helps create and manage LangCpp
  structures. Currently, only expressions are handled, but it might be
  useful to have all LangCppStmtTree structures created in a factory
  in order to do high-level optimizations. 
*/

#ifndef __LangCppFactory_H_
#define __LangCppFactory_H_

#include "symtab/STFieldBOM.h"
#include "exprsynth/ExprFactory.h"
#include "hdl/HdlVerilogPath.h"
#include "langcpp/LangCppStmtTree.h"

class AtomicCache;
class STSymbolTable;

//! Class to help create elements of a LangCppStmtTree
/*!
  The statement tree uses CarbonExprs as the basis of all arithmetic
  and logical expressions. This class helps manage that. In addition,
  some functions are here that just make things easier to cut down on
  repeated code.

  One thing that I might consider doing is making all tree elements
  created through the factory. That is a rather big change, and I'm
  not sure if it is worth it.
*/
class LangCppFactory
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  LangCppFactory();
  //! Destructor
  ~LangCppFactory();

  /*
    string-unique identifiers. Used to make variables
  */
  
  //! Create an ident of a given name with given byte size
  CarbonIdent* createIdent(const char* identName, UInt32 byteSize);
  //! Create an ident that is an array
  /*!
    \param identName Identifier name
    \param numElems Number of elements in the array
    \param elemByteSize the size in bytes of each element of the
    array.
  */
  CarbonIdent* createArrayIdent(const char* identName, UInt32 numElems, UInt32 elemByteSize);

  //! Create an identifier bitselect
  /*!
    \param arrayIdent An identifier that is an array. No check is done
    to see if it is an array.
    \param chgArrayIndex Index into the array
    \param byteSize the size in bytes of the resulting expression
  */
  CarbonExpr* createIdentBitsel(CarbonIdent* arrayIdent, SInt32 chgArrayIndex, UInt32 byteSize);
  
  //! Create a logical not (expr == 0) of a CarbonExpr
  CarbonExpr* createLogicalNot(CarbonExpr* expr);
 
  //! Create expr != 0 in scope.
  LangCppCarbonExpr* createNeqZero(LangCppCarbonExpr* expr, 
                                   LangCppScope* scope);


  /*
    LangCppExpr* methods
  */

  
  //! Create a LangCpp expression in a scope based on a CarbonExpr 
  /*!
    The CarbonExpr should be consistent with the LangCppStmtTree. The
    expression passed in must contain idents that are variables in the
    tree. The expression is walked to find idents in the current scope.
  */
  LangCppCarbonExpr* createLangCppExpr(CarbonExpr* expr, LangCppScope* curScope);
  
  //! Create a binary op on two expressions
  /*!
    Perform the binary operation on the two expressions in the given
    scope.
  */
  LangCppCarbonExpr* createBinaryOp(CarbonExpr::ExprT binOp,
                                    LangCppExpr* expr1, LangCppExpr* expr2,
                                    LangCppScope* scope);


  //! Create a binary op on two expressions, one of which is a CarbonExpr
  /*!
    Useful if you are creating identifiers and need to iteratively do
    binary operations on them.
    Perform the binary operation on the two expressions in the given
    scope.
  */
  LangCppCarbonExpr* createBinaryOp(CarbonExpr::ExprT binOp,
                                    LangCppExpr* expr1, CarbonExpr* expr2,
                                    LangCppScope* scope);
  
  //! Create a logical not of a cpp variable in a scope.
  LangCppCarbonExpr* createLogicalNot(LangCppVariable* var, LangCppScope* scope);
  
  //! Create a simple less-than cpp expression in a scope
  LangCppCarbonExpr* createLessThanInt(LangCppVariable* var, 
                                       SInt32 number,
                                       LangCppScope* scope);
  
  //! Create a 0 expression in a scope
  LangCppCarbonExpr* createZeroExpr(UInt32 numBytes, LangCppScope* scope);
  //! Create a 1 expression in a scope
  LangCppCarbonExpr* createOneExpr(UInt32 numBytes, LangCppScope* scope);

  //! Create a cpp expression of an ident bitselect
  LangCppCarbonExpr* createIdentBitselExpr(CarbonIdent* ident, SInt32 index, UInt32 byteSize, LangCppScope* scope);
  
  /*
    statement functions. 
  */

  //! Assign a variable a CarbonExpr expression in a scope
  /*!
    This basically does:
    \code
    var = varAlias
    \endcode

    In addition, varAlias is stored in the scope as an alias to var,
    allowing for unique lookups based on CarbonExpr.
  */
  void addAssignAlias(LangCppVariable* var, CarbonExpr* varAlias, LangCppScope* scope);

  //! Assign a variable a LangCppCarbonExpr expression in a scope
  /*!
    This basically does:
    \code
    var = varAlias
    \endcode

    In addition, varAlias is stored in the scope as an alias to var,
    allowing for unique lookups based on CarbonExprs inside
    LangCppCarbonExprs.
  */
  void addAssignAlias(LangCppVariable* var, LangCppCarbonExpr* varAlias, LangCppScope* scope);


  //! Declare-Assign a variable to a CarbonExpr expression in a scope
  /*!
    This basically does:
    \code
    Type var = varAlias
    \endcode

    In addition, varAlias is stored in the scope as an alias to var,
    allowing for unique lookups based on CarbonExpr.
  */
  void addDeclAssignAlias(LangCppVariable* var, CarbonExpr* varAlias, LangCppScope* scope);

  //! Declare-Assign a variable to a LangCppCarbonExpr expression in a scope
  /*!
    This basically does:
    \code
    Type var = varAlias
    \endcode

    In addition, varAlias is stored in the scope as an alias to var,
    allowing for unique lookups based on CarbonExprs inside
    LangCppCarbonExprs.
  */
  void addDeclAssignAlias(LangCppVariable* var, LangCppCarbonExpr* varAlias, LangCppScope* scope);

  //! Create a LangCppExpr* given an SInt32 
  LangCppExpr* createSIntExpr(SInt32 param, LangCppScope* scope);

  /* 
     scope functions
  */

  //! Declare a variable with the given string name
  /*!
    This will create an identifier and declare the variable inside the
    given scope.
  */
  LangCppVariable* declareVariable(LangCppType* type, const char* varName, LangCppScope* scope);

  //! Add an integer constructor parameter to a variable declaration
  /*!
    The constructor is emitted when the declaration of the variable is
    emitted. If there are no constructor parameters, no explicit
    constructor is called (the default constructor is
    used). Parameters are emitted in the order they are added.
    
    This adds a simple UInt32 to the constructor parameters.
  */
  void addConstructUIntParam(LangCppVariable* var, UInt32 size, LangCppScope* scope);
  
  /*
    misc functions
  */
  //! Get the expression factory
  ESFactory* getExprFactory() { return &mExprFactory; }


private:
  ESFactory mExprFactory;
  STEmptyFieldBOM mFieldBOM;
  HdlVerilogPath mHdlPath;

  AtomicCache* mAtomicCache;
  STSymbolTable* mSymTab;
  ConstantRangeFactory* mConstantRangeFactory;

  //! Walk the CarbonExpr and find any embedded C++ variables.
  void gatherCppVars(CarbonExpr* singleExpr, LangCppScope* scope, LangCppVariableArray* cppVars);

  class IdentFinder;
};

#endif
