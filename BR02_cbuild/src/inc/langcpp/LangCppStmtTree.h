// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  A series of classes that help implement representation of the C++
  language. It does not cover all aspects of the language, but is
  helpful in optimizing and emitting code.

  The basic idea is to have a nested list of statements (hence, a
  tree) that can be emitted directly to a file. The top-most statement
  is a file scope, which has as a parent a global scope or context.
*/
  
#ifndef __LangCppStmtTree_h_
#define __LangCppStmtTree_h_

#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtArray.h"

#include "langcpp/LangCppType.h"


class LangCppScope;
class LangCppStatement;
class LangCppVariable;
class LangCppExpr;
class LangCppFuncCall;
class LangCppStaticCast;
class LangCppDereference;
class LangCppFunction;
class LangCppIf;
class LangCppFileScope;
class LangCppUserExpr;
class LangCppCarbonExpr;
class CarbonIdent;
class CarbonExpr;
class LangCppFor;
class LangCppElse;
class LangCppElseIf;
class LangCppReturn;
class LangCppIfDef;
class LangCppEndIf;

typedef UtHashMap<UtString, LangCppType*> StrLangCppTypeMap;
typedef UtArray<LangCppStatement*> LangCppStmtList;
typedef UtHashMap<CarbonIdent*, LangCppVariable*> IdentToLangCppVarMap;
typedef UtArray<LangCppVariable*> LangCppVariableArray;
typedef UtArray<LangCppExpr*> LangCppExprArray;
typedef UtArray<LangCppScope*> LangCppScopeArray;

#if pfHAS_VARIADIC_MACROS
//! Variadic macro to NULL terminate a defined array declaration.
#define NTARRAY(...) { __VA_ARGS__, NULL }
#endif

class LangCppWalker;

//! The most basic c++ action statement
class LangCppStatement
{
public:
  CARBONMEM_OVERRIDES

  LangCppStatement();
  virtual ~LangCppStatement();

  //! Emit c++ syntax to the stream.
  /*!
    \param out The output stream
    \param indent The starting indent spacing for this statement.
  */
  virtual void emit(UtOStream& out, UInt32 indent) const = 0;

  //! Visit routine used by LangCppWalker to traverse statements
  virtual void visit(LangCppWalker* walker) = 0;

  //! Cast to a scope. NULL if not a scope
  virtual const LangCppScope* castScope() const;
  //! non-const version of the cast
  LangCppScope* castScope()
  {
    const LangCppStatement* me = const_cast<const LangCppStatement*>(this);
    return const_cast<LangCppScope*>(me->castScope());
  }
};

//! Assignment statement
class LangCppAssign : public LangCppStatement
{
public:
  CARBONMEM_OVERRIDES

  LangCppAssign(LangCppExpr* lhs, LangCppExpr* rhs);
  virtual ~LangCppAssign();
  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);

protected:
  LangCppExpr* mLhs;
  LangCppExpr* mRhs;
};

//! lhs |= rhs
class LangCppOrAssign : public LangCppAssign
{
public:
  CARBONMEM_OVERRIDES

  LangCppOrAssign(LangCppExpr* lhs, LangCppExpr* rhs);
  virtual ~LangCppOrAssign();
  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);
};

//! lhs &= rhs
class LangCppAndAssign : public LangCppAssign
{
public:
  CARBONMEM_OVERRIDES

  LangCppAndAssign(LangCppExpr* lhs, LangCppExpr* rhs);
  virtual ~LangCppAndAssign();
  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);
};

//! ++var
class LangCppPrefixIncrement : public LangCppAssign
{
public:
  CARBONMEM_OVERRIDES
  LangCppPrefixIncrement(LangCppVariable* var);
  virtual ~LangCppPrefixIncrement();
  
  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);
  
private:
  LangCppVariable* mVariable;
};

//! \<type\> var = rhs
class LangCppDeclAssign : public LangCppAssign
{
public:
  CARBONMEM_OVERRIDES
  LangCppDeclAssign(LangCppVariable* lhs, LangCppExpr* rhs);
  virtual ~LangCppDeclAssign();
  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);
private:
  LangCppVariable* mVariable;
};

//! func();
class LangCppFuncCallStmt : public LangCppStatement
{
public:
  CARBONMEM_OVERRIDES
  LangCppFuncCallStmt(LangCppFuncCall* call);
  virtual ~LangCppFuncCallStmt();
  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);

  LangCppFuncCall* getCallExpr() { return mCall; }

private:
  LangCppFuncCall* mCall;
};


//! Line comment (double forward slash)
class LangCppLineComment : public LangCppStatement
{
public:
  CARBONMEM_OVERRIDES
  LangCppLineComment(const char* comment);
  virtual ~LangCppLineComment();
  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);

private:
  UtString mComment;
};

//! Memcpy call.
class LangCppMemCpy : public LangCppStatement
{
public:
  CARBONMEM_OVERRIDES
  LangCppMemCpy(LangCppVariable* dest, LangCppVariable* src, UInt32 numBytes);
  virtual ~LangCppMemCpy();
  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);

private:
  LangCppVariable* mDest;
  LangCppVariable* mSrc;
  UInt32 mNumBytes;
};

//! Memset function stmt.
class LangCppMemSet : public LangCppStatement
{
public:
  CARBONMEM_OVERRIDES
  LangCppMemSet(LangCppVariable* dest, LangCppVariable* val, UInt32 numBytes);
  virtual ~LangCppMemSet();
  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);

private:
  LangCppVariable* mDest;
  LangCppVariable* mVal;
  UInt32 mNumBytes;
};

//! Return statement
class LangCppReturn : public LangCppStatement
{
public:
  CARBONMEM_OVERRIDES
  LangCppReturn(LangCppExpr* returnAction);
  virtual ~LangCppReturn();
  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);

private:
  LangCppExpr* mReturnAction;
};

//! A #ifdef 
class LangCppIfDef : public LangCppStatement
{
public:
  CARBONMEM_OVERRIDES
  LangCppIfDef(const char* ifCond);
  virtual ~LangCppIfDef();
  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);
  
private:
  UtString mIfCond;
};

//! A #endif
class LangCppEndIf : public LangCppStatement
{
public:
  CARBONMEM_OVERRIDES
  LangCppEndIf();
  virtual ~LangCppEndIf();
  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);

};

//! Scope base class
class LangCppScope : public LangCppStatement
{
public:
  CARBONMEM_OVERRIDES
  LangCppScope(LangCppScope* parent);

  virtual ~LangCppScope();

  virtual const LangCppScope* castScope() const;

  //! add an if statement to this scope
  LangCppIf* addIf(LangCppExpr* condition);
  //! Add a function call statment to this scope
  LangCppFuncCallStmt* addCall(LangCppFuncCall* call);

  //! Declare a variable in this scope
  /*!
    Nothing happens if the variable has already been declared in an
    ancestor scope, except the type is checked.
  */
  LangCppVariable* declareVariable(LangCppType* varType, CarbonIdent* ident);

  //! Add an assign statement
  LangCppAssign* addAssign(LangCppExpr* lhs, LangCppExpr* rhs);
  //! Special alias assign statement
  /*!
    This is used to alias the rhs to the lhs. The rhs is a carbon expr
    from an ESFactory, thus aliasing can be done based on CarbonExpr
    pointers.
  */
  LangCppAssign* addAssignAlias(LangCppVariable* lhs, CarbonExpr* rhs, 
                                const LangCppVariableArray& referencedVars);

  //! Special alias assign statement w/ LangCppCarbonExpr rhs
  LangCppAssign* addAssignAlias(LangCppVariable* lhs, 
                                LangCppCarbonExpr* rhs);

  //! Declarative assign. e.g., UInt32 me = someExpr;
  LangCppDeclAssign* addDeclAssign(LangCppVariable* lhs, LangCppExpr* rhs);

  //! Special alias declarative assign statment w/ CarbonExpr rhs
  LangCppDeclAssign* addDeclAssignAlias(LangCppVariable* lhs, CarbonExpr* rhs,
                                        const LangCppVariableArray& 
                                        referencedVars);

  //! Special alias declarative assign statment w/ LangCppCarbonExpr rhs
  LangCppDeclAssign* addDeclAssignAlias(LangCppVariable* lhs, 
                                        LangCppCarbonExpr* rhs);

  //! Add a ++var assignment.
  LangCppAssign* addPrefixIncrement(LangCppVariable* var);

  //! Add a |= assign
  LangCppAssign* addOrAssign(LangCppExpr* lhs, LangCppExpr* rhs);
  //! Add a &= assign
  LangCppAssign* addAndAssign(LangCppExpr* lhs, LangCppExpr* rhs);

  //! Creates single line comment
  void addLineComment(const char* comment);

  //! Add a user-defined statement
  /*!
    Variable and expression management is left to the user.
  */
  void addUserStatement(const UtString& stmt);
  
  //! Within this scope declare a class type
  LangCppClassType* declareClassType(const char* className);
  //! Get a previously declared class type
  LangCppClassType* getClassType(const char* className);
  //! Declare a pod type
  LangCppPodType* declarePodType(const char* typeName, LangCppType* typeDef);
  //! Get a previously declared pod type
  LangCppPodType* getPodType(const char* typeName);
  //! Get any previously declared type.
  LangCppType* getType(const char* typeName);

  //! Emit this scope. 
  /*!
    Emitting a scope begins with declarin variables first. Then, the
    statements are written to the stream.
  */
  virtual void emit(UtOStream& out, UInt32 indent) const;
  virtual void visit(LangCppWalker* walker);

  //! Find a variable in an ancestor or this scope
  LangCppVariable* findVariable(CarbonIdent* ident);

  //! Return the variable the expression was assigned to
  /*!
    Returns NULL if the expression wasn't assigned to a variable in
    this scope or ancestors' scopes.
  */
  LangCppVariable* getAlias(CarbonExpr* expr);

  //! Return the variable the LangCppCarbonExpr was assigned to
  LangCppVariable* getAlias(LangCppCarbonExpr* expr);

  //! Create a sub scope in this scope
  LangCppScope* addScope();

  //! Add a for loop scope.
  /*!
    For loops are a little messy. A for loop is a scope, and you need
    to give it initial, condition, and post clauses. But you have to
    have the scope before you can do that. And you need to declare
    variables as in a normal scope, but you better make it a decl
    assign in the initial clause! 
    So, create each clause separately within the scope and then set
    them with putInitialClause(), putConditional(), and
    putPostClause(). Nothing except a ';', if applicable, is emitted
    for any clause that is not set. 

    The interface for this can probably be better, but I currently
    can't think of one.
  */
  LangCppFor* addFor();
  
  //! Add a memcpy of two variables
  void addMemCpy(LangCppVariable* dest,
                 LangCppVariable* src,
                 UInt32 numBytes);

  //! Add a memset of a variable,
  void addMemSet(LangCppVariable* dest,
                 LangCppVariable* val,
                 UInt32 numBytes);

  //! Add a return statment. expr can be NULL.
  void addReturn(LangCppExpr* expr);
  
  //! Add an ifdef statement
  void addIfDef(const char* preProcessStr);
  //! Add an endif statement
  void addEndIf();

  //! Create a static_cast expression for a variable
  LangCppStaticCast* staticCast(LangCppType* type, LangCppVariable* hdl);
  //! Create a dereference of and expression
  LangCppDereference* dereference(LangCppExpr* expr);

  //! Call a method on a variable that is expected to be a class
  /*!
    \param instance Instance of the class on which the method will be
    called.
    \param methodName The name of the method to call
    \param paramList A NULL terminated list of parameters. 

    The NTARRAY macro was created for this type of interface. For
    example, if you need to call the 'munge' method on myClass and
    pass parameters 'a', 'b', and 'c', which are integers in the
    scope, you would do: 
    \code
    LangCppExpr* paramList[] = NTARRAY{a, b, c};
    scope->callMethod(myClass, "munge", paramList);
    \endcode
  */
  LangCppFuncCall*  callMethod(LangCppVariable* instance,
                               const char* methodName,
                               LangCppExpr* paramList[] = NULL);

  //! Get a member of a variable that is expected to be a class
  LangCppVariable*  getMember(LangCppVariable* instance,
                              CarbonIdent* memberName,
                              LangCppType* memberType);

  //! Call a function by name (not a method call)
  /*!
    \param functionName The name of the function to call
    \param paramList A NULL terminated list of parameters. 
    
    The NTARRAY macro was created for this type of interface. For
    example, if you need to call the function 'squeeze' and
    pass parameters 'a', 'b', and 'c', which are integers in the
    scope, you would do: 
    \code
    LangCppExpr* paramList[] = NTARRAY{a, b, c};
    scope->callFunction("squeeze", paramList);
    \endcode
  */
  LangCppFuncCall*  callFunction(const char* functionName,
                                 LangCppExpr* paramList[] = NULL);
  
  //! Only call this if you new an expr and need a place to manage it.
  /*!
    This is normally called by other methods in this class to store
    expressions that will need to be cleaned up.
  */
  void addExpr(LangCppExpr* expr);  

  //! Create a user expression
  LangCppUserExpr* createUserExpr(const char* exprStr);
  //! Creata a wrapped CarbonExpr
  /*!
    The refVars array is a list of C++ variables that the expression
    references. This is needed to manage read/write counts.
  */
  LangCppCarbonExpr* createCarbonExpr(CarbonExpr* expr, const LangCppVariableArray& refVars);
  
protected:
  //! Add stmt to this scope's list of statements
  void addStmt(LangCppStatement* stmt);
  //! Add a variable to the variable map
  void addVariable(LangCppVariable* variable);
  //! Add a new scope statement to this scope
  /*!
    This appends the child scope array and then calls addStmt.
  */
  void addChildScope(LangCppScope* scope);
  //! Find a variable in this scope.
  LangCppVariable* findVariableInScope(CarbonIdent* ident);
  


private:
  LangCppScope* mParent;
  StrLangCppTypeMap mScopeTypes;
  LangCppStmtList mStatements;
  IdentToLangCppVarMap mVariables;
  LangCppScopeArray mChildScopes;

  typedef UtHashMap<CarbonExpr*, LangCppVariable*> ExprVarMap;
  ExprVarMap mCarbonExprVarAliases;

  // place holder for expressions that we have to cleanup.
  LangCppExprArray mSavedExprs;

  //! Find a type of a given name. Returns NULL if not found
  LangCppType* findType(const char* typeName);
  //! Add a newly created type to the type map.
  void addType(LangCppType* type);

  void emitVariableDecls(UtOStream& out, UInt32 indent) const;

  struct VariableSort;
};

//! Global scope. 
/*!
  This is used mainly to declare pod types and any other global
  structures needed in the statement tree.
*/
class LangCppGlobal : public LangCppScope
{
public:
  CARBONMEM_OVERRIDES
  LangCppGlobal();
  virtual ~LangCppGlobal();
  
  //! void type
  LangCppPodType* getVoidType() { return mVoid; }
  //! bool type
  LangCppPodType* getBoolType() { return mBool; }
  //! unsigned char
  LangCppPodType* getUCharType() { return mUChar; }
  //! (signed) char
  LangCppPodType* getSCharType() { return mSChar; }

  //! Asserts. You cannot emit the global scope
  virtual void emit(UtOStream&, UInt32) const;
  //! Asserts. You currently cannot visit the global scope
  virtual void visit(LangCppWalker* walker);

protected:
  LangCppPodType* mVoid;
  LangCppPodType* mBool;
  LangCppPodType* mUChar;
  LangCppPodType* mSChar;
};

//! For loop scope
class LangCppFor : public LangCppScope
{
public:
  CARBONMEM_OVERRIDES
  LangCppFor(LangCppScope* parent);
  virtual ~LangCppFor();

  //! Get the scope the for is looping over
  LangCppScope* getClauseScope() { return mSubScope; }
  //! Initial clause of the for
  void putInitClause(LangCppStatement* assign);
  //! Conditional part of the for
  void putLoopConditional(LangCppExpr* cond);
  //! Post clause of the for
  void putPostClause(LangCppStatement* stmt);

  virtual void emit(UtOStream& out, UInt32) const;
  virtual void visit(LangCppWalker* walker);

private:
  LangCppScope* mSubScope;
  LangCppStatement* mInitClause;
  LangCppExpr* mCond;
  LangCppStatement* mPostClause;
};

//! Function scope
class LangCppFunction : public LangCppScope
{
public:
  CARBONMEM_OVERRIDES
  //! Enumeration to describe function linkage
  enum Linkage {
    eStatic, //!< Static linkage
    eGlobal //!< Global linkage
  };

  //! Function attributes
  enum Attrib {
    eAttribNone, //!< No extra attributes
    eAttribInline //!< Inline the function
  };

  LangCppFunction(const UtString& name, Linkage linkage, LangCppType* retType,
                  LangCppScope* parent);
  virtual ~LangCppFunction();

  //! Declare a function parameter.
  LangCppVariable* declareParameter(LangCppType* paramType, CarbonIdent* paramName);

  void addAttribute(Attrib attrib);

  virtual void emit(UtOStream& out, UInt32 indent) const;
  virtual void visit(LangCppWalker* walker);

private:
  UtString mFuncName;
  Linkage mLinkage;
  Attrib mAttribs;
  LangCppType* mReturnType;
  LangCppVariableArray mParameters;

  const char* getLinkageStr() const;
};

//! File scope
/*!
  File scope mainly holds function scopes, but could hold variable
  declarations and other things that are at the file level.
*/
class LangCppFileScope : public LangCppScope
{
public:
  CARBONMEM_OVERRIDES
  LangCppFileScope(LangCppGlobal* globalScope);
  virtual ~LangCppFileScope();

  //! Add a function to the file scope
  LangCppFunction* addFunction(const char* funcName, LangCppFunction::Linkage linkage, 
                               LangCppType* returnType);

  virtual void visit(LangCppWalker* walker);

private:
  LangCppGlobal* mGlobalScope;
  UtStringSet mScopeNames;
};

//! If scope
class LangCppIf : public LangCppScope
{
public:
  CARBONMEM_OVERRIDES
  LangCppIf(LangCppExpr* condition, LangCppScope* parent);
  virtual ~LangCppIf();

  //! Get the scope that the if is guarding
  LangCppScope* getClauseScope() { return mSubScope; }

  //! Attach an else-if to this if statement
  virtual LangCppElseIf* addElseIf(LangCppExpr* condition);
  //! Attach the one and only else statement to this if scope
  virtual LangCppElse* addElse();
  
  virtual void emit(UtOStream& out, UInt32 indent) const;
  virtual void visit(LangCppWalker* walker);

private:
  // if condition
  LangCppExpr* mCondition;
  // If the condition is true, run this scope
  LangCppScope* mSubScope;

  LangCppElse* mElseScope;
  LangCppScopeArray mElseIfs;
};

//! Else-if scope
class LangCppElseIf : public LangCppIf
{
public:
  CARBONMEM_OVERRIDES
  LangCppElseIf(LangCppExpr* condition, LangCppScope* parent);
  virtual ~LangCppElseIf();

  //! asserts
  virtual LangCppElseIf* addElseIf(LangCppExpr* condition);

  //! asserts
  virtual LangCppElse* addElse();

  //! Emit the else if construct
  virtual void emit(UtOStream& out, UInt32 indent) const;
  virtual void visit(LangCppWalker* walker);
};

//! Add else scope
class LangCppElse : public LangCppScope
{
public:
  CARBONMEM_OVERRIDES
  LangCppElse(LangCppScope* parent);
  virtual ~LangCppElse();
  virtual void emit(UtOStream& out, UInt32 indent) const;
  virtual void visit(LangCppWalker* walker);
};

//! User-defined statement stored in string.
/*!
  If any LangCVariables are used, the caller has to manually up their
  reference counts. But, why would you do that? Use one of the actual
  statements if you are using a variable that is created within
  this system. 
  This is mainly for calls outside the statement tree
  (mGenProfile, e.g.).
*/
class LangCppUserStatement : public LangCppStatement
{
public:
  CARBONMEM_OVERRIDES
  LangCppUserStatement(const UtString& statementStr);
  virtual ~LangCppUserStatement();
  virtual void emit(UtOStream& out, UInt32 indent) const;
  virtual void visit(LangCppWalker* walker);
private:
  UtString mStatement;
};

//! Basic C++ expression
/*!
  Currently, the read and write counts of an expression are limited
  only to variables. These are not yet used, but will be to help
  eliminate dead variables.
*/
class LangCppExpr
{
public:
  CARBONMEM_OVERRIDES
  LangCppExpr();
  virtual ~LangCppExpr();

  //! Increment the read count on the expression
  virtual void incrRead();
  //! Increment the write count on the expression
  virtual void incrWrite();
  //! Decrement the read count on the expression
  virtual void decrRead();
  //! Decrement the write count on the expression
  virtual void decrWrite();

  //! Number of reads on this expression
  virtual UInt32 numReads() const;
  //! Number of writes on this expression
  virtual UInt32 numWrites() const;
  //! References on the expression (reads + writes)
  virtual UInt32 refCnt() const;

  //! Get the sign of the expression
  LangCppType::SignT getSign() const { return mSign; }
  //! is this a signed expression?
  bool isSigned() const { return mSign == LangCppType::eSigned; }
  
  //! Emit the expression to a stream
  virtual void emit(UtOStream& out) const = 0;

  //! Visit routine used by LangCppWalker to traverse expressions
  virtual void visit(LangCppWalker* walker) = 0;

  //! Returns NULL by default.
  /*!
    This is needed to treat LangCppCarbonExprs and LangCppVariables
    similarly. We can argue that a LangCppVariable is a
    LangCppCarbonExpr, but they really are different. Variables are
    monitored for liveness, non-variable expressions are not (for now, 
    anyway). I might change this so that variables and carbonexprs are
    more related.
  */
  virtual CarbonExpr* getExpr() const;

protected:
  LangCppType::SignT mSign;
};

//! CarbonExpr statement
class LangCppCarbonExpr : public LangCppExpr
{
public:
  CARBONMEM_OVERRIDES
  LangCppCarbonExpr(CarbonExpr* expr, const LangCppVariableArray& referencedVars);
  virtual ~LangCppCarbonExpr();
  
  virtual void incrRead();
  virtual void incrWrite();
  virtual void decrRead();
  virtual void decrWrite();

  virtual void emit(UtOStream& out) const;
  virtual void visit(LangCppWalker* walker);

  //! Get the carbon expression
  virtual CarbonExpr* getExpr() const;

  //! Same as getExpr(), but not virtual
  CarbonExpr* getCarbonExpr() { return mExpr; }

  //! Get the list of referenced variables
  const LangCppVariableArray& getReferencedVariables() const { 
    return mVariables;
  }

protected:
  //! Adds variable to the expression. 
  /*!
    Called from LangCppVariable constructor
  */
  void addVariable(LangCppVariable* var);

  //! Constructor for LangCppVariable
  LangCppCarbonExpr(CarbonExpr* expr);

private:
  CarbonExpr* mExpr;
  LangCppVariableArray mVariables;
};

//! Expression describing a variable
/*!
  The name of the variable is a CarbonExpr. This makes using a factory
  to maintain variables easy. This is a LangCppCarbonExpr in order to
  make operations on variables easy to do and maintain.
*/
class LangCppVariable : public LangCppCarbonExpr
{
public:
  CARBONMEM_OVERRIDES
  LangCppVariable(LangCppType* type, CarbonIdent* identifier);
  virtual ~LangCppVariable();

  //! Attributes of a variable
  enum Attrib {
    eAttribNone, //!< No extra attributes
    eAttribUnused = 0x1, //!< Always emit with UNUSED macro
    eAttribNoDeclare = 0x2, //!< Variable is previously declared
    eAttribRegister = 0x4 //!< Declare with 'register' keyword
  };
  
  virtual void incrRead();

  virtual void incrWrite();

  virtual void decrRead();

  virtual void decrWrite();

  virtual UInt32 numReads() const;
  virtual UInt32 numWrites() const;

  virtual UInt32 refCnt() const;

  //! Add an attribute to this variable.
  void addAttribute(Attrib flag);

  //! Get the type of this variable
  LangCppType* getType() { return mType; }

  //! Returns the class type of this variable
  /*!
    If this variable is not a class type, this asserts.
  */
  LangCppClassType* getClassType();

  //! Compose this variable into a message string
  void compose(UtString* msg) const;

  //! Get the CarbonIdent
  CarbonIdent* getIdent() const { return mIdentifier; }

  //! Emit this variable as a declaration
  void emitDeclare(UtOStream& out) const;
  //! Emit this variable as a use.
  virtual void emit(UtOStream& out) const;
  virtual void visit(LangCppWalker* walker);

  //! Returns true if we are not to declare this variable separately.
  /*!
    A decl assign or a variable outside the stmt tree would be a
    no-declare. The decl assign declares the variable in an
    assignment statement instead of at the beginning of the scope.
  */
  bool isNoDeclare() const { return (mAttributes & eAttribNoDeclare) != 0; }
  //! Emit declaration with the UNUSED macro
  /*!
    The UNUSED macro is a carbon macro that sets the gcc unused
    attribute. This quiets gcc when a variable is declared but never
    used.
  */
  bool isDeclaredUnused() const { 
    return (mAttributes & eAttribUnused) != 0; 
  }
  
  bool isDeclaredRegister() const {
    return (mAttributes & eAttribRegister) != 0; 
  }

  //! When constructing the variable pass a param to the constructor
  void addConstructParam(LangCppExpr* param);

private:
  CarbonIdent* mIdentifier;
  LangCppType* mType;
  UInt32 mReadCnt;
  UInt32 mWriteCnt;
  Attrib mAttributes;

  LangCppExprArray mConstructParams;
};

//! A class member variable
class LangCppMemberVariable : public LangCppVariable
{
public:
  CARBONMEM_OVERRIDES
  LangCppMemberVariable(LangCppType* type, CarbonIdent* ident, LangCppVariable* classInstance);
  virtual ~LangCppMemberVariable();

  virtual void emit(UtOStream& out) const;
  virtual void visit(LangCppWalker* walker);
  
private:
  LangCppVariable* mClassInstance;
};

//! Static cast expression
class LangCppStaticCast : public LangCppExpr
{
public:
  CARBONMEM_OVERRIDES
  LangCppStaticCast(LangCppType* castTo, LangCppType* castFrom, LangCppExpr* expr);
  virtual ~LangCppStaticCast();

  virtual void incrRead();

  virtual void incrWrite();

  virtual void decrRead();

  virtual void decrWrite();

  virtual void emit(UtOStream& out) const;
  virtual void visit(LangCppWalker* walker);

private:
  LangCppType* mCastTo;
  LangCppType* mCastFrom;
  LangCppExpr* mExpr;
};

//! Function call expression
/*!
  Note that a function call is the basis of a function call
  statement. This is because you could have a function call as a rhs
  part of an expression or a conditional. And you can also have a
  function call as a statement by itself.
*/
class LangCppFuncCall : public LangCppExpr
{
public:
  CARBONMEM_OVERRIDES
  LangCppFuncCall(const char* funcName, LangCppExpr* paramList[]);
  virtual ~LangCppFuncCall();
  virtual void emit(UtOStream& out) const;

  virtual void visit(LangCppWalker* walker);

private:
  UtString mCall;
  LangCppExprArray mParameters;

  //! Pass an expression as a parameter to the function.
  void passParam(LangCppExpr* param);
};

//! Class method call
class LangCppMethodCall : public LangCppFuncCall
{
public:
  CARBONMEM_OVERRIDES
  LangCppMethodCall(const char* funcName, 
                    LangCppVariable* instance,
                    LangCppExpr* paramList[]);
  virtual ~LangCppMethodCall();

  virtual void emit(UtOStream& out) const;
  virtual void visit(LangCppWalker* walker);

private:
  LangCppVariable* mInstance;
};

//! Dereference expression
class LangCppDereference : public LangCppExpr
{
public:
  CARBONMEM_OVERRIDES
  LangCppDereference(LangCppExpr* expr);
  virtual ~LangCppDereference();

  virtual void incrRead();

  virtual void incrWrite();

  virtual void decrRead();

  virtual void decrWrite();

  virtual void emit(UtOStream& out) const;
  virtual void visit(LangCppWalker* walker);

private:
  LangCppExpr* mExpr;
};

//! User-defined c++ statement
/*!
  This is needed for emitting nucleus into the stmt tree. It is not
  known what we are adding, so a generic user statement is used.
  
  Note: The user statement must emit itself completely. It is up to
  the statement to have the ; or the } or whatever it needs to end the
  statement.
*/
class LangCppUserExpr : public LangCppExpr
{
public:
  CARBONMEM_OVERRIDES
  LangCppUserExpr(const char* str);
  virtual ~LangCppUserExpr();

  virtual void emit(UtOStream& out) const;
  virtual void visit(LangCppWalker* walker);

private:
  UtString mStr;
};
#endif
