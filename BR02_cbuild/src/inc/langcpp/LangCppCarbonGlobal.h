// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __LangCppCarbonGlobalDecl_h_
#define __LangCppCarbonGlobalDecl_h_

#include "langcpp/LangCppStmtTree.h"
class LangCppCarbonGlobal : public LangCppGlobal
{
public:
  CARBONMEM_OVERRIDES

  LangCppCarbonGlobal();
  virtual ~LangCppCarbonGlobal();

  LangCppPodType* getUInt8Type() { return &mUInt8; }
  LangCppPodType* getSInt8Type() { return &mSInt8; }
  LangCppPodType* getSInt32Type() { return &mSInt32; }
  LangCppPodType* getUInt32Type() { return &mUInt32; }
  LangCppPodType* getUInt64Type() { return &mUInt64; }

private:
  LangCppPodType mUInt32;
  LangCppPodType mSInt32;
  LangCppPodType mUInt64;
  LangCppPodType mSInt8;
  LangCppPodType mUInt8;

};

#endif
