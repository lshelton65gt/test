// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __LangCppWalker_h_
#define __LangCppWalker_h_

/*!
  \file
  Declaration of a walker class for all the LangCpp constructs.
*/

#include "util/c_memmanager.h"
  
class LangCppScope;
class LangCppStatement;
class LangCppVariable;
class LangCppExpr;
class LangCppFuncCall;
class LangCppStaticCast;
class LangCppFunction;
class LangCppIf;
class LangCppFileScope;
class LangCppUserExpr;
class LangCppCarbonExpr;
class LangCppFor;
class LangCppElse;
class LangCppElseIf;
class LangCppReturn;
class LangCppIfDef;
class LangCppEndIf;
class LangCppLineComment;
class LangCppFuncCallStmt;
class LangCppMemCpy;
class LangCppMemSet;
class LangCppUserStatement;
class LangCppAssign;
class LangCppDeclAssign;
class LangCppOrAssign;
class LangCppAndAssign;
class LangCppPrefixIncrement;
class LangCppMemberVariable;
class LangCppMethodCall;

//! A walker class for all the LangCpp constructs.
/*!
  To use this either call visitStmt or visitExpr. Those will call the
  appropriate function for traversal. Note that those are not virtual.
  
  Derive from this class and overload the visit and/or previsit
  routines for the constructs that you are interested in. The default
  for all the virtual void functions is to simply return. The default
  for all the bool pre-visit functions is to return true.

  As each construct is visited, the construct's internals will also be
  traversed. Most, but not all, constructs have previsit routines to
  stop traversal into the particular construct. The very basic
  constructs do not have pre-visits as they do not traverse into
  anything. However, derivations of basic constructs will have a
  previsit routine, in case you are looking for that particular
  construct. 
  The exception to that rule is LangCppVariable. That is so
  basic, yet it is derived from LangCppCarbonExpr. It doesn't make
  sense to have a previsit routine for that because the cost of not
  visiting it is actually greater than visiting it. So, visiting a
  LangCppVariable causes two visits, one to the LangCppVariable and
  one to the LangCppCarbonExpr.
  
  \note This class must remained inlined so that derivation can take
  place in both libcarbon and in cbuild. Dynamic cast is turned off
  for these files and this causes a type info issue in cbuild unless
  the virtual functions are inlined.
*/
class LangCppWalker
{
public:
  CARBONMEM_OVERRIDES

  virtual ~LangCppWalker() {}

  // basic methods

  //! Visit a statement.
  void visitStmt(LangCppStatement* stmt) { stmt->visit(this); }
  //! Visit an expressions
  void visitExpr(LangCppExpr* expr) { expr->visit(this); }

  // Basic statement visits

  //! Visit a line comment
  virtual void visitLineComment(LangCppLineComment*) {}
  //! Visit a scope
  virtual void visitScope(LangCppScope*) {}
  //! Visit a function-call statement
  /*!
    This is called for all Function call statements including method
    calls.
  */
  virtual void visitFuncCallStmt(LangCppFuncCallStmt*) {}

  //! Visit a memcpy statement
  virtual void visitMemCpy(LangCppMemCpy*) {}
  //! Visit a memset statement
  virtual void visitMemSet(LangCppMemSet*) {}
  //! Visit a return statement
  virtual void visitReturn(LangCppReturn*) {}

  //! Visit an ifdef
  virtual void visitIfDef(LangCppIfDef*) {}
  //! Visit an endif
  virtual void visitEndIf(LangCppEndIf*) {}
  //! Visit a user-created statement
  virtual void visitUserStatement(LangCppUserStatement*) {}
  //! Visit an assign statment
  virtual void visitAssign(LangCppAssign*) {}

  // Pre statement visits. Note not all statements have pre-visit
  // routines. Only the interesting ones and ones that are derived.

  //! Previsit a scope. Do not traverse if false is returned
  virtual bool preVisitScope(LangCppScope*) { return true; }
  //! Previsit a for. Do not traverse if false is returned
  virtual bool preVisitFor(LangCppFor*) { return true; }
  //! Previsit a function. Do not traverse if false is returned
  virtual bool preVisitFunction(LangCppFunction*) { return true; }
  //! Previsit an if. Do not traverse if false is returned
  /*!
    Note that even though an 'if' is a scope, the visitScope routine
    is NOT called for the if because the order of else ifs/else
    statements are crucial.
  */
  virtual bool preVisitIf(LangCppIf*) { return true; }
  //! Previsit an else if. Do not traverse if false is returned
  /*!
    Note that even though an 'else if' is a scope, the visitScope
    routine is NOT called for the if because the order of else
    ifs statements are crucial.
  */
  virtual bool preVisitElseIf(LangCppElseIf*) { return true; }
  //! Previsit an else scope. Do not traverse if false is returned
  /*!
    Visiting an else scope does cause the visitScope routine to be
    called since the else, if it exists, has to be the last scope in
    an if statement
  */
  virtual bool preVisitElse(LangCppElse*) { return true; }
  //! Previsit a function call statement.
  virtual bool preVisitFuncCallStmt(LangCppFuncCallStmt*) { return true; }
  //! Previsit a memcpy statement
  virtual bool preVisitMemCpy(LangCppMemCpy*) { return true; }
  //! Previsit a memset statement
  virtual bool preVisitMemSet(LangCppMemSet*) { return true; }
  //! Previsit a return statement.
  virtual bool preVisitReturn(LangCppReturn*) { return true; }
  //! Previsit an assign statement. 
  /*!
    Note that this gets called for all assign statement derivations.
  */
  virtual bool preVisitAssign(LangCppAssign*) { return true; }
  //! Previsit a declaration-assign statement
  virtual bool preVisitDeclAssign(LangCppDeclAssign*) { return true; }
  //! Previsit an OR assign statement
  virtual bool preVisitOrAssign(LangCppOrAssign*) { return true; }
  //! Previsit an AND assign statement
  virtual bool preVisitAndAssign(LangCppAndAssign*) { return true; }
  //! Previsit a prefix increment assign statement
  virtual bool preVisitPrefixIncrement(LangCppPrefixIncrement*) { return true; }
  //! Previsit a file scope
  virtual bool preVisitFileScope(LangCppFileScope*) { return true; }

  // Expression visits

  //! visit a variable.
  /*!
    Note that since a variable is also a LangCppCarbonExpr, the visit
    function for that will also be called.
  */
  virtual void visitVariable(LangCppVariable*) {}
  //! Visit a static cast expression
  virtual void visitStaticCast(LangCppStaticCast*) {}
  //! Visit a dereference expression
  virtual void visitDereference(LangCppDereference*) {}
  //! Visit a function call expression
  virtual void visitFuncCall(LangCppFuncCall*) {}
  //! Visit a user-created expression
  virtual void visitUserExpr(LangCppUserExpr*) {}
  //! Visit a carbon expression
  virtual void visitCarbonExpr(LangCppCarbonExpr*) {}


  // Expression previsits

  //! Previsit a member variable. Do not traverse if false is returned
  virtual bool preVisitMemberVariable(LangCppMemberVariable*) { return true; }
  //! Previsit a static cast. Do not traverse if false is returned
  virtual bool preVisitStaticCast(LangCppStaticCast*) { return true; }
  //! Previsit a dereference. Do not traverse if false is returned
  virtual bool preVisitDereference(LangCppDereference*) { return true; }
  //! Previsit a function call. Do not traverse if false is returned
  /*!
    All function call derivations call this as well.
  */
  virtual bool preVisitFuncCall(LangCppFuncCall*) { return true; }
  //! Previsit a method call. Do not traverse if false is returned
  /*!
    Note that preVisitFuncCall() will also be called for this.
  */
  virtual bool preVisitMethodCall(LangCppMethodCall*) { return true; }
};

#endif

