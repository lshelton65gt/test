/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
/*!
  \file
  Implementation of User-Defined Task/Functions to access Carbon models from
  Verilog simulators.
*/
#ifndef _CARBON_PLI_H_
#define _CARBON_PLI_H_

#ifndef CARBON_EXTERNAL_DOC

extern int carbon_deposit_calltf();
extern int carbon_deposit_checktf();

extern int carbon_deposit_memory_calltf();
extern int carbon_deposit_memory_checktf();

extern int carbon_dumpfile_calltf();
extern int carbon_dumpfile_checktf();

extern int carbon_dumpfile_fsdb_auto_switch_calltf();
extern int carbon_dumpfile_fsdb_auto_switch_checktf();

extern int carbon_dump_control_calltf();
extern int carbon_dump_control_checktf();

extern int carbon_dumpvars_calltf();
extern int carbon_dumpvars_checktf();

extern int carbon_examine_calltf();
extern int carbon_examine_checktf();
extern int carbon_examine_sizetf();

extern int carbon_auto_examine_calltf();
extern int carbon_auto_examine_checktf();

extern int carbon_examine_memory_calltf();
extern int carbon_examine_memory_checktf();
extern int carbon_examine_memory_sizetf();

extern int carbon_force_calltf();
extern int carbon_force_checktf();

extern int carbon_force_range_calltf();
extern int carbon_force_range_checktf();

extern int carbon_readmem_calltf();
extern int carbon_readmem_checktf();

extern int carbon_release_calltf();
extern int carbon_release_checktf();

extern int carbon_release_range_calltf();
extern int carbon_release_range_checktf();

#else

/*!

  \mainpage Carbon Model PLI Task Reference

  <center>
  \image html cds-logo.gif
  \htmlonly
  <small>The Answer to Validation.<sup>TM</sup></small>
  \endhtmlonly
  </center> 
 
  \section intro Using a Carbon Model with a Verilog Simulator

  The Carbon compiler includes an option that enables you to generate a PLI
  wrapper around the object being compiled. This wrapper will enable
  the Carbon Model to be integrated into a test environment running
  under a traditional event-based simulator. A Carbon Model may be 
  accessed from a Verilog simulator using the standard Verilog PLI 
  mechanism. 

  Since a Carbon Model replaces the Verilog HDL code for a module, the Verilog 
  simulator no longer has access to the names and values of signals 
  internal to that module. The system tasks and functions described 
  here are provided to enable you to modify, examine, force, and release 
  signals, as well as dump waveforms.

  Refer to the <em>Carbon Compiler User Manual</em> for details on using the
  -pliWrapper option.

  Also see the <em>Carbon Model Validation User Manual</em> for information
  about using the Carbon Model Validation tool in your VHDL or Verilog
  simulation environment.

  \section names Signal Names

  Because the signals accessed by these tasks are internal to the 
  Carbon Model, the Verilog simulator does not recognize them by name.
  Therefore, when you pass the name of a signal to a task you must
  use a quoted string; otherwise the simulator will indicate that no such
  signal exists.
*/

/*! \defgroup pliTasks PLI Tasks and Functions
  @{
*/

/*!
  \brief Deposits the given value on a net.

  \param signal_name A quoted string giving a full or relative path to the net.
  \param value A Verilog HDL expression that resolves to the new value for the net.

  \code
    // 'cpu' is a Carbon Model with internal register 'program_counter'.  
    c cpu(clk, data, etc);
    
    always @ (posedge clk)
      $display($time, $carbon_examine("c.program_counter"));

    initial
      // zero the counter
      #100 $carbon_deposit("c.program_counter", 32'h00000000);  
  \endcode
*/
$carbon_deposit(signal_name, value);

/*!
  \brief Retrieves the current value of the given net.

  The return value is the low 32 bits of the value.

  The optional 'result' argument, if given, will receive the entire
  signal value.  In this way values larger than 32 bits may be examined.

  \param signal_name A quoted string giving a full or relative path to the net.

  \param result (optional) A writable HDL signal that is large enough
  to hold the value being examined.

  \code
    // 'cpu' is a Carbon Model with internal register 'program_counter'.  
    c cpu(clk, data, etc);

    reg [63:0] big_value;
        
    always @ (posedge clk) begin
      $carbon_examine("c.big_number", big_value);
      $display($time, big_value, $carbon_examine("c.program_counter"));
    end

    initial
      // zero the counter
      #100 $carbon_deposit("c.program_counter", 32'h00000000);  
  \endcode
*/
$carbon_examine(signal_name, result);

/*!
  \brief Automatically keeps a Verilog signal up to date with the
  latest Carbon value

  This task associates a signal with the Carbon model with an HDL
  signal.  Whenever the Carbon model changes the value of the given signal, 
  the HDL signal will be automatically updated with the new value.

  \note The given signal should be marked with the observeSignal
  compiler directive to ensure that it will be available for
  monitoring at runtime.

  \param signal_name A quoted string giving a full or relative path to the Carbon net.
  \param hdl_signal A writable HDL signal.

  \code
    // 'cpu' is a Carbon Model with internal register 'program_counter'.  
    c cpu(clk, data, etc);

    reg [31:0] pc;

    // 'cpu' is a Carbon Model with internal register 'program_counter'.  
    initial begin
      $carbon_auto_examine("c.program_counter", pc);
      $monitor(pc);
    end
  \endcode
*/
$carbon_auto_examine(signal_name, hdl_signal);

/*!
  \brief Deposits the given value to the specified address of the memory.

  \param signal_name A quoted string giving a full or relative path to the net.
  \param addr The index of the memory into which to deposit the value.
  \param value A Verilog HDL expression that resolves to the new value for the memory.
*/
$carbon_deposit_memory(signal_name, addr, value);

/*!
  \brief Retrieves the value at the given memory address.

  The return value is the low 32 bits of the value.

  The optional 'result' argument, if given, will receive the entire
  signal value.  In this way values larger than 32 bits may be examined.

  \param signal_name A quoted string giving a full or relative path to the memory.
  \param addr The memory address to examine.
  \param result (optional) A writable HDL signal that is large enough
  to hold the value being examined.
*/
$carbon_examine_memory(signal_name, addr, result);

/*!
  \brief Forces a net to a specified value.

  This function will force the net to a given value, overriding the
  value from whatever logic is driving it. This function works only if
  the net was marked as forcible during Carbon Model compilation. Once you
  force a net to a specific value, it will remain at that value until
  you explicity release it using the $carbon_release task. This is
  a useful function when you are trying to isolate a problem in the
  design, or testing a potential solution to a problem.

  \param signal_name A quoted string giving a full or relative path to the net.
  \param value A Verilog HDL expression that resolves to the new value for the net.

  \code
    // 'cpu' is a Carbon model instance with internal signal 'q'.

    // force cpu.q = 1'b0;
    $carbon_force("cpu.q", 1'b0);

    // release cpu.q;
    $carbon_release("cpu.q");
  \endcode
*/
$carbon_force(signal_name, value);

/*!
  \brief Forces a bit range to a specified value.

  This function will force a value onto the specified range of a
  net. This function works only if the net was marked as forcible
  during Carbon Model compilation. Once you force a range to a specific
  value, it will remain at that value until you explicitly release it
  using the carbon_release_range task.

  \param signal_name A quoted string giving a full or relative path to the net.
  \param value A Verilog HDL expression that resolves to the new value for the net.
  \param range_msb An integer giving the most significant bit of the range.
  \param range_lsb An integer giving the least significant bit of the range.

  \code
    // 'cpu' is a Carbon model instance with internal signal 'q'.

    // force bits 3, 4, 5 of cpu.q to 0
    $carbon_force_range("cpu.q", 3'b0, 5, 3);

    // release cpu.q;
    $carbon_release_range("cpu.q", 5, 3);
  \endcode
*/
$carbon_force_range(signal_name, value, range_msb, range_lsb);

/*!
  \brief Releases a net from a forced value.

  This function will release the entire net from the current value,
  whether it was fully forced or not. If the specified net was not
  forced to a value, this function has no real effect.

  \param signal_name A quoted string giving a full or relative path to the net.

  \code
    // 'cpu' is a Carbon model instance with internal signal 'q'.

    // force cpu.q = 1'b0;
    $carbon_force("cpu.q", 1'b0);

    // release cpu.q;
    $carbon_release("cpu.q");
  \endcode
*/
$carbon_release(signal_name);

/*!
  \brief Releases a bit range from a forced value.

  This function will release the entire specified range, whether the
  range is fully forced or not. If the specified range of the net was
  not forced, this function has no real effect.

  \param signal_name A quoted string giving a full or relative path to the net.
  \param range_msb An integer giving the most significant bit of the range.
  \param range_lsb An integer giving the least significant bit of the range.

  \code
    // 'cpu' is a Carbon model instance with internal signal 'q'.

    // force bits 3, 4, 5 of cpu.q to 0
    $carbon_force_range("cpu.q", 3'b0, 5, 3);

    // release cpu.q;
    $carbon_release_range("cpu.q", 5, 3);
  \endcode
*/
$carbon_release_range(signal_name, range_msb, range_lsb);

/*!
  \brief Reads a file of hex data into a memory..

  The file is read from lowest value address to the highest value.
  For example, if the memory address range is specifed as [127:0] or
  as [0:127], the beginning of the file is read into address 0 and
  the end of the file is read into address 127. Explicit addressing
  (@addr) simply makes the reader jump to that address.

  \param memory_name A quoted string giving a full or relative path to the memory.
  \param file_name A quoted string giving giving the name of the files to load.
*/
$carbon_readmemh(file_name, memory_name);

/*!
  \brief Reads a file of binary data into a memory.

  The file is read from lowest value address to the highest value.
  For example, if the memory address range is specifed as [127:0] or
  as [0:127], the beginning of the file is read into address 0 and
  the end of the file is read into address 127. Explicit addressing
  (@addr) simply makes the reader jump to that address.

  \param memory_name A quoted string giving a full or relative path to the memory.
  \param file_name A quoted string giving giving the name of the files to load.
*/
$carbon_readmemb(file_name, memory_name);

/*!
  \brief Specifies the output file for waveforms.

  A separate dump file is generated for each model instance. The
  given file_name is used to automatically generate file names for
  each model instance output file. Output files are named by appending 
  the instance name of each Carbon model to the given file_name.

  The file format is determined by the specified extension.  ".vcd"
  generates a VCD format file, ".fsdb" generates a Debussy file. If you
  specify any other extension, or none at all, a VCD file is generated
  by default. If no file_name is given, "dump.vcd" is used by default.

  \param file_name The name of the waveform output file.

  \code
    // Design with Carbon model instances "top.a" and "top.b".

    $carbon_dumpfile("dump.vcd");
    $carbon_dumpvars;

    // Will create files:
    //  dump_top.a.vcd
    //  dump_top.b.vcd

    // with all visible signals.
  \endcode
*/
$carbon_dumpfile(file_name);

/*!
  \brief Specifies the output file for waveforms.


  The file format is determined by the specified extension.  ".vcd"
  generates a VCD format file, ".fsdb" generates a Debussy file. If you
  specify any other extension, or none at all, a VCD file is generated
  by default. If no file_name is given, "dump.vcd" is used by default.

  \param file_name The name of the waveform output file.

*/

/*! 
  \brief Initializes the wave dumper to dump in Debussy's FSDB
  format, and to automatically open a new FSDB dump file when a
  specified size limit has been reached.

  A separate set of dump files is generated for each model
  instance. The given fname_prefix is used to automatically
  generate file names for each model instance output file. Output
  files are named by appending the model instance name and the file
  index number to the fname_prefix.

  When this function initiates, it opens an FSDB file
  \<fname_prefix\>_\<model_instance\>_0.fsdb.  When the
  specified file limit is reached, this file will be closed and
  \<fname_prefix\>_\<model_instance\>_1.fsdb will be
  opened. This process will continue until the specified maximum
  number of files is reached, at which point the file index is
  returned to 0 and \<fname_prefix\>_\<model_instance\>_0.fsdb
  is overwritten.

  This task may be called only once--multiple sets of waveform files
  for a single run is not currently supported.

  By default, the fsdb writer uses file synchronization. If you
  encounter a runtime error or warning with fsdb writing indicating a
  problem with file syncing, set the Novas environment variable,
  FSDB_ENV_SYNC_CONTROL to 'off'. For example, in csh: setenv
  FSDB_ENV_SYNC_CONTROL off

  This function cannot be used in conjunction with $carbon_dumpfile.

  \param fname_prefix A quoted string giving the prefix for the
   generated waveform files.  The waveform files will be named
   beginning with this string followed by
   _\<model_instance\>_\<num\>.fsdb, where num is the current file
   index. Num begins at 0 and increases by one when an FSDB file
   reaches its megabyte limit. If the maximum number of files has been
   reached, the file index is reset to 0, and the file is overwritten.

  \param limit_megs The size limit in megabytes for an FSDB file.  Once
  the limit is reached, the file index is increased, and a new file is
  opened. Must be at least 2. A specification of 0 or 1 will
  automatically be upgraded to 2 and a warning will be issued.

  \param max_files The maximum number of FSDB files to create FOR EACH
  MODEL INSTANCE during the simulation. If 0 then always increase the
  file index through the entire simulation, never overwriting a
  file. (Specify 0 if you do not want to overwrite any FSDB file
  created during the simulation.)  If 2 or greater the maximum number
  of files is allowed to be written.  In this case, the file index is
  reset to 0 once the maximum number of files has been reached causing
  files to be overwritten. Setting this to 1 is not permitted.

  \code
    // Design with Carbon model instances "top.a" and "top.b".

    $carbon_dumpfile_fsdb_auto_switch("dump", 5, 2);
    $carbon_dumpvars;

    // Will inisitally create the files:
    //  dump_top.a_0.fsdb
    //  dump_top.b_0.fsdb

    // with all visible signals.
  \endcode

*/
$carbon_dumpfile_fsdb_auto_switch(fname_prefix, limit_megs, max_files);

/*!
  \brief Enables waveform dumping for a given scope.

  Multiple instances of $carbon_dumpvars have a cumulative effect
  (i.e., the set of signals dumped is the union of all signals given 
  by all instances).

  \param level Number of levels of hierarchy to dump (same as Verilog $dumpvars).
  If 0, all underlying scopes are traversed. If not specified, 0 is default.
  \param scope A quoted string giving the HDL path to a signal internal to the
  Carbon model. The path may be relative to the current module or a full
  hierarchical path.
*/
$carbon_dumpvars(level, scope, ...);

/*!
  \brief Begins dumping waveforms.

  \param instance A quoted string giving a full or relative path to a net or 
  module instance. If not specified, all instances are affected.
*/
$carbon_dumpon(instance, ...);

/*!
  \brief Stops dumping waveforms.
 
  \param instance A quoted string giving a full or relative path to a net or 
  module instance. If not specified, all instances are affected.
*/
$carbon_dumpoff(instance, ...);

/*!
  @}
*/

#endif
#endif
