// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef CARBON_PLI_WRAPPER_H
#define CARBON_PLI_WRAPPER_H

#include "codegen/SysIncludes.h"
#include "util/CarbonPlatform.h"
#include "shell/carbon_shelltypes.h"
#include "util/CarbonPlatform.h"
#include "util/MemManager.h"
#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "shell/carbon_capi.h"

/*!
  \file
  Classes used by PLI-based interface to Verilog event simulators.

  Included by PLI wrapper runtime and the generated wrapper.
*/

//! CarbonPliBuffer class
/*!
 * Provides generic interface to moving data values between
 * Carbon nets and PLI task parameters.
 */
class CarbonPliBuffer
{
 public: CARBONMEM_OVERRIDES
  /*!
    \param pliParm The number of the PLI parameter in the task argument list.
    \param numBits Size of net in bits.
    \param numUInt32s Size of net in UInt32, to allocate buffer space.
    \param useDrive false if Carbon net cannot accept drive.
   */
  CarbonPliBuffer(int pliParm, 
                  unsigned int numBits, 
                  unsigned int numUInt32s,
                  bool useDrive);
  ~CarbonPliBuffer();

  //! get a pointer to the current value
  UInt32* getValue() { return mValue; }

  //! get a pointer to the current drive (may be NULL)
  UInt32* getDrive() { return mDrive; }

  //! read value from PLI task parameter into buffer, return true if value changed
  bool pliRead();

  //! write value from buffer to PLI task parameter
  void pliWrite(UInt32 *value, UInt32 *drive);
  void pliWrite() { pliWrite(mValue, mDrive); }

 private:
  int     mPliParm;
  UInt32* mValue;
  UInt32* mDrive;
  p_tfexprinfo mPliInfo;
  PLI_BYTE8 *mInstance;
};

//! CarbonPliPort class
/*!
 *  Models the connection between the Verilog module port and the Carbon
 *  DesignPlayer port.
 */
enum carbonPliEdge {
  ePliNoEdges,
  ePliPosedge,
  ePliNegedge,
  ePliBothEdges
};

///////////////////////////////////////////////////////////////////////////////
class CarbonPliPort
{
 public: CARBONMEM_OVERRIDES
  /*!
    \param obj The CarbonObjectID
    \param net The CarbonNetID of the DesignPlayer port 
    \param pliParm The number of the PLI parameter in the task argument list.
    \param isDepositable false if we cannot write to the Carbon port.
   */
  CarbonPliPort(CarbonObjectID* obj, CarbonNetID* net, int pliParm, bool isDepositable); 
  ~CarbonPliPort();

  //! check the given PLI parameter to make sure it is readable and/or writable
  static bool checkPliType(int pliParm, bool read, bool write);

  //! get the CarbonID for the associated net
  CarbonNetID* getNet() { return mNet; }

  //! get a pointer to the current value
  UInt32* getValue() { return mBuffer->getValue(); }

  //! read value from PLI parameter and write it to Carbon net, return true if value changed
  bool pliToCarbon(CarbonObjectID *model)
  {
    bool changed = false;
    if (mIsDepositable)
    {
      changed = mBuffer->pliRead();
      if (changed)
        carbonDeposit(model, mNet, mBuffer->getValue(), mBuffer->getDrive());
    }
    return changed;
  }

  //! read value from Carbon handle and write it to PLI parameter
  void carbonToPli(CarbonObjectID *model)
  {
    carbonExamine(model, mNet, mBuffer->getValue(), mBuffer->getDrive());
    mBuffer->pliWrite();
  }
  
  carbonPliEdge schedTrigger() { return mSchedTrigger; }
  void setSchedTrigger(carbonPliEdge val) { mSchedTrigger = val; }

 protected:
  bool             mIsDepositable;
  carbonPliEdge    mSchedTrigger;  // Changes to these ports trigger carbonSchedule calls.
  CarbonNetID*     mNet;
  CarbonPliBuffer* mBuffer;
};


class CarbonPliWrapperContext;

//! CarbonPliModel class
/*!
 *  Represents a Carbon model instance in a way that facilitates
 *  implemtation of UDTFs to manipulate the ports, dump waves, etc.
 */
class CarbonPliModel
{
public: CARBONMEM_OVERRIDES
  /*!
   * \param hdlInstance The Verilog HDL name for the module instance.
   * \param carbonName The Carbon name for the model
   * \param model The Carbon handle to the model instance
   * \param numPorts ... number of ports ...
   * \param timeScale local time units for model instance
   */
  CarbonPliModel(const char *hdlInstance, 
                 const char *carbonName, 
                 CarbonObjectID *model, 
                 int numPorts,
                 CarbonTimescale timeScale);
  ~CarbonPliModel();

  void              setContext(CarbonPliWrapperContext *context) { mContext = context; };
  CarbonPliWrapperContext* getContext() { return mContext; };
  CarbonObjectID*   getModel() { return mModel; };
  CarbonPliPort*    getPort(int portNumber) { return mPorts[portNumber]; };
  void              setPort(int portNumber, CarbonPliPort* net) { mPorts[portNumber] = net; };
  unsigned int      getDepth() { return mDepth; };
  const char*       getCarbonName() { return mCarbonName; };
  const char*       getHdlName() { return mHdlName; };
  void              dumpVars(const char *path, unsigned int level);
  void              dumpControl(bool on);
  void             *getUserData() { return mUserData; };
  void              setUserData(void *userData) { mUserData = userData; };

private:
  void initWaveFile();

  CarbonPliWrapperContext* mContext;
  char*             mHdlName;    // HDL name of model instance
  char*             mCarbonName; // Carbon name for top level scope
  CarbonObjectID*   mModel;      // handle to Carbon model instance
  CarbonPliPort**   mPorts;      // handles to nets for each port
  size_t            mNumPorts;	 // Number of pointers in mPorts
  bool*             mSchedulePort;// flags to indicate ports which need scheduling
  unsigned int      mDepth;      // how deep in the design hierarchy
  CarbonWaveID*     mWave;       // Carbon wave context
  CarbonTimescale   mTimeScale;  // the local time units for model instance
  void*             mUserData;   // data allocated by the user for use in callbacks
};

//! CarbonPliWrapperContext class
/*!
 * There is only one CarbonPliWrapperContext object, and it manages
 * the entire set of Carbon model instances.
 *
 * A map from Verilog HDL instance name to CarbonPliModel instance
 * is maintained and used by the UDTF tasks that enable waves, 
 * deposit, examine, etc. from HDL.
 */
class CarbonPliWrapperContext
{
private:

  //! Helper class for comparing char* keys for a UtHashMap
  /*!
    By default, maps with pointer keys compare them by looking at the
    pointer values, assuming they're unique.  That's probably not true
    in the way strings are stored in the CarbonModelInstanceMap.
    Instead, we want to compare the actual string values.
   */
  class InstanceMapHelper
  {
  public:
    CARBONMEM_OVERRIDES

    size_t hash(const char* val) const
    {
      // This is an inline function in UtString.h, so it shouldn't
      // cause any external references.
      return UtString::hash(val);
    }

    bool lessThan(const char* v1, const char* v2) const
    {
      return (strcmp(v1, v2) < 0);
    }

    bool lessThan1(const char* v1, const char* v2) const
    {
      return (strcmp(v1, v2) < 0);
    }

    bool equal(const char* v1, const char* v2) const
    {
      return (strcmp(v1, v2) == 0);
    }
  };

  typedef UtHashMap<const char*, CarbonPliModel *, InstanceMapHelper> CarbonModelInstanceMap;

public: CARBONMEM_OVERRIDES
  CarbonPliWrapperContext();
  ~CarbonPliWrapperContext();

  //! Typedef for an iterator of the map of models
  typedef CarbonModelInstanceMap::UnsortedLoop ModelLoop;

  //! add a new model instance to the map
  void addModel(CarbonPliModel *data);

  //! Find a model instance by HDL path.
  /*!
   * \param scope HDL path, absolute base path for relative lookup.
   * \param path HDL path, may be relative.
   * \param localPath if given, will be set to the Carbon name for the Carbon instance.
   * \warning The caller is responsible for freeing storage allocated for localPath.
   */
  CarbonPliModel* findModel(const char *scope, const char *path, char **localPath = NULL);

  //! Loop function to traverse the list of clocks in a design
  ModelLoop loopModels();

  void setDumpAutoSwitch(const char *name, CarbonUInt32 limitMegs, CarbonUInt32 maxFiles);
  bool getDumpAutoSwitch();
  CarbonUInt32 getDumpLimitMegs();
  CarbonUInt32 getDumpMaxFiles();
  void setDumpFile(const char *name);
  /*!
   * \warning The caller is responsible for freeing storage allocated for name.
   */
  void getDumpFileName(const char *scope, char** name, CarbonWaveFileType &fileType);

  //! Allocate and copy a string
  static void sAllocateAndCopy(char** dest, const char* src);

  //! Allocate and copy a string with explicit size
  static void sAllocateAndCopy(char** dest, const char* src, size_t size);

  //! Reallocate and append to a string
  /*!
   * \warning The destination buffer must be valid
   */
  static void sReallocateAndAppend(char** dest, const char* src);

private:
  // map from Carbon model instances names to CarbonObjectID*
  CarbonModelInstanceMap mModelInstances;

  char*              mDumpName;       // dump file base name
  CarbonWaveFileType mDumpType;       // dump file format
  CarbonUInt32       mDumpLimitMegs;  // auto-switch wave file size limit
  CarbonUInt32       mDumpMaxFiles;   // auto-switch number of wave files limit
  bool               mDumpAutoSwitch; // true if using auto-switch dump mode
};

//! CarbonPliWrapperContextManager class
/*!
 * Provides structured access to the PLI context object.
 */
class CarbonPliWrapperContextManager
{
public:
  // get the PLI context 
  static CarbonPliWrapperContext* getPliContext();

private:
  static CarbonPliWrapperContext* mContext;
};

#endif 
