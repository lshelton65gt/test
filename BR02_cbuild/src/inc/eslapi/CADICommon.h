/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef CADI2Common_H
#define CADI2Common_H

#include "eslapi/CAInterface.h"

# ifdef WIN32
#  ifdef EXPORT_CADI
#   define CADI_WEXP    __declspec(dllexport)
#  elif !defined(NO_IMPORT_CADI)
#   define CADI_WEXP    __declspec(dllimport)
#  else
#   define CADI_WEXP
#  endif
# else
#  define CADI_WEXP
# endif

#define CADI_SPEC_MAJOR_VERSION 2
#define CADI_SPEC_MINOR_VERSION 0

#endif
