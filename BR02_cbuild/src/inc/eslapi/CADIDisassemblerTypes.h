// ===========================================================================
// Description : CADIDisassembler types declaration.
//
// Author      : Carbon
// Release     : 2.0.0
//
// Copyright (c)  2008 Carbon Design Systems Inc., 2006-2008 ARM. All rights reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE.
//

#ifndef CADIDisassemblerTypes_h
#define CADIDisassemblerTypes_h

// Types have moved to CADIDisassembler.h, this header is only for compatibility
#include "eslapi/CADIDisassembler.h"

#endif

// end of file CADIDisassemblerTypes.h
