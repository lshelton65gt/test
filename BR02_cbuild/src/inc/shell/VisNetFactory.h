// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef __VIS_NET_FACTORY_H__
#define __VIS_NET_FACTORY_H__

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#include "carbon/carbon_shelltypes.h"
#include "util/UtHashMap.h"

class ShellNet;
class ShellVisNet;
class STSymbolTableNode;
class STAliasedLeafNode;
class CarbonHookup;
class IODBRuntime;
class IODBTypeDictionary;
class IODBIntrinsic;
class UserType;

//! Class for creating/managing nets created through the visibility API
/*!
  This class creates/manages expression nets needed for array/struct
  visibility.  This includes both the CarbonExprNet objects and
  associated CarbonExprs.  While all the nets are owned by the
  factory, the auxiliary objects are not.  The CarbonModel's existing
  factories for CarbonExprs, DynBitVectors, etc., are used for these.
 */
class VisNetFactory
{
public:
  CARBONMEM_OVERRIDES

  VisNetFactory(CarbonHookup* hookup, IODBRuntime* db);
  ~VisNetFactory();

  //! Create a net, applying any required expressions, for a visibility node
  ShellNet* createNet(const CarbonDatabaseNode* node);

  //! Class to manage memory allocation of temporary expression nets
  /*!
  When examining/depositing an entire array, ShellNets are found for
  each element.  This allows the complex expression creation to be
  leveraged.  However, since the user didn't explicitly look up all
  those nets, we don't want to keep them around - there might be a
  lot.

  When these temporary ShellNets are created, they are recorded here,
  along with any other allocated objects.  The destructor takes care
  of freeing everything.
  */
  class TempNetManager
  {
  public:
    TempNetManager();
    ~TempNetManager();

    ShellNet *mNet;
    const IODBIntrinsic *mIntrinsic;
  };

  typedef UtArray<const ConstantRange*> RangeArray;
  //! Create a temporary net, without using any factories
  ShellNet* createTempNet(CarbonDatabaseNode* node, const UserType *userType,
                          TempNetManager* netMgr, const SInt32* indices,
                          UInt32 numDims, const RangeArray& ranges);

protected:
  //! Class to represent the attributes that identify an expression net
  /*!
    An expression net is created from a CarbonExpr and IODBIntrinsic.
    Each of these subelements is created by a factory object that
    ensures no duplicate objects are created.  That is, there cannot
    be two unique CarbonExpr (or IODBIntrinsic) objects that are
    equivalent.

    We want to do the same thing with expression nets.  If we already
    have an expression net whose CarbonExpr and IODBIntrinsic match
    those of a new request, we just return the existing net.  This
    class facilitates that lookup by encapsulating the attributes.

    Since duplicate instances of the attribute objects are never
    created, we know that two equivalent instances of this class will
    have identical attribute pointers.  As a result, we can do a
    simple pointer comparison instead of doing deep compare of each
    attribute.
   */
  class NetAttr
  {
  public:
    CARBONMEM_OVERRIDES

    NetAttr(const CarbonNet* wrappedNet, const IODBIntrinsic* intrinsic,
            bool needsBitsel, bool needsMemsel, UInt32 bitsel, SInt32 memsel);
    ~NetAttr();

    size_t hash() const;
    bool equal(const NetAttr* other) const;
    NetAttr* allocateACopy() const;

  protected:
    const CarbonNet* mWrappedNet;
    const IODBIntrinsic* mIntrinsic;
    const bool mNeedsBitsel;
    const bool mNeedsMemsel;
    const UInt32 mBitsel;
    const SInt32 mMemsel;
  };

  //! Helper class to compare/hash attributes of CarbonExprNets stored in a map
  /*!
    For hashing/comparison, we need to look at the attributes'
    members, not just their pointers.
   */
  class MapHelper
  {
  public:
    CARBONMEM_OVERRIDES

    size_t hash(const NetAttr *var) const;
    bool equal(const NetAttr *v1, const NetAttr *v2) const;
  };
  
  //! Class to manage partselect and memory index expressions
  class ExprData
  {
  public:
    ExprData();
    ~ExprData();

    void setBitsel(UInt32 bitsel);
    void setMemIndex(UInt32 index);
    void putShellNet(ShellNet* net) { mShellNet = net; }
    void putDesignLeaf(STAliasedLeafNode* node) { mDesignLeaf = node; }
    void putArrayRange(const ConstantRange* range, bool exprDataOwnsRange) { mExprDataOwnsArrayRange= exprDataOwnsRange; mArrayRange = range; }
    void putNumBits(UInt32 numBits) { mNumBits = numBits; }

    bool getBitsel(UInt32* bitsel) const;
    bool getMemIndex(UInt32 *index) const;
    ShellNet* getShellNet() const { return mShellNet; }
    STAliasedLeafNode* getDesignLeaf() const { return mDesignLeaf; }
    const ConstantRange* getArrayRange() const { return mArrayRange; }
    UInt32 getNumBits() const { return mNumBits; }

  protected:
    bool mNeedsBitsel;          // true if a bit or partsel is needed, and in that case mBitsel will be valid
    bool mNeedsMemIndex;        // true if a memory word is indexed, and in that case mMemIndex will be valid
    UInt32 mBitsel;             // the lsb for the bitsel, this is normalized.
    UInt32 mMemIndex;           // the word index of memory (if memory was resynthesized then it is index into
                                // the resynthesized memory (which is always normalized), and if there were
                                // multiple unpacked dimensions then mMemIndex is the index in the resynthesized memory )
    UInt32 mNumBits;            // the number of bits required.   (It is not clear that this can be combined with mBitsel
                                // to define a range of bits selected is [mNumBits+mBitsel-1:mBitsel])

    ShellNet *mShellNet;
    STAliasedLeafNode *mDesignLeaf;
    bool mExprDataOwnsArrayRange; // if true then ExprData::~ExprData must delete mArrayRange
    const ConstantRange *mArrayRange; // the user declared range for a word of memory. that is the [10:8] of
                                      // reg [10:8] mem [2:3][4:7];  currently mArrayRange is only set if there is a single packed dimension
  };

  //! Looks in the cache for an existing net with the given attributes
  ShellVisNet* lookupNet(const NetAttr& attr);
  //! Makes a copy of the attribute and associates it with the net
  void cacheNet(const NetAttr& attr, ShellVisNet* net);

  typedef UtArray<UInt32> UIntArray;
  /*!
    Walks a visibility symbol table node and its parents, building an
    list of indices and bounds for any that refer to an index of an
    array
  */
  void buildIndices(const CarbonDatabaseNode* node, UIntArray* indices, UIntArray* sizes);

  //! Determine the required expression (bitsel/memsel) to map a visibility node to a ShellNet
  void calcExpr(UIntArray* containingArrayIndices,  UIntArray* containingArraySizes,
                ExprData* exprData, bool isMemory, UInt32 width);
  //! Determine all expression data for a node
  void getExpressionData(const CarbonDatabaseNode* node, const UserType* userType,
                         UIntArray* containingArrayIndices, UIntArray* containingArraySizes,
                         ExprData* exprData);
  //! Possibly create an expression net from a node and expression data
  ShellNet* maybeCreateExpressionNet(const ExprData& exprData, const CarbonDatabaseNode* node,
                                     TempNetManager* netMgr);

  CarbonHookup *mHookup;                //!< The CarbonModel's CarbonHookup
  IODBRuntime *mDB;                     //!< The CarbonModel's IODBRuntime
  IODBTypeDictionary *mTypeDictionary;  //!< The CarbonModel's IODBTypeDictionary
  
  typedef UtHashMap<const NetAttr*, ShellVisNet*, MapHelper> AttrNetMap;
  AttrNetMap mAllocatedNets;    //!< Map of all allocated expression nets, keyed by object encapsulating their attributes
};

#endif
