// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONSCALAR_H_
#define __CARBONSCALAR_H_


#ifndef __CARBONSCALARBASE_H_
#include "shell/CarbonScalarBase.h"
#endif

//! Derived instance of scalar (one-bit) nets.
class CarbonScalar: public virtual CarbonScalarBase
{
protected:
  //! The raw scalar storage
  UInt8* mScalar;
  
  //! returns raw storage
  UInt32 getExamineStore() const;

  //! Assign the scalar the value of buf
  inline bool assignValue(const UInt32* buf);
  
public: CARBONMEM_OVERRIDES
  //! constructor
  CarbonScalar (UInt8* addr);

  //! constructor with signed char
  /*!
    Needed because this shares the same generation function as a
    CarbonVector1 which can be signed.
  */
  CarbonScalar (SInt8* addr);

  //! virtual destructor
  virtual ~CarbonScalar();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;

  //! ShellNet::isDataNonZero()
  virtual bool isDataNonZero() const;

  //! Deposit the value of buf into the net
  /*!
    \param buf A 1 element array of UInt32s
  */
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  
  //! examine the value. buf is a 1 element array of UInt32s
  /*! 
    \param buf address of buffer to return value (zero-extended
    boolean). 
    \param drive Address of buffer to return model drive
  */
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode, CarbonModel*) const;

  //! free the scalar shadow
  virtual void freeShadow(Storage* shadow);

  //! alloc a scalar shadow
  virtual Storage allocShadow() const;

  //! Compare the shadow and the storage values
  virtual ValueState compare(const Storage shadow) const;
  
  //! Write value to valueStr if shadow and storage differ
  virtual ValueState writeIfNotEq(char* valueStr, size_t len, Storage* shadow,
                                  NetFlags);
  
  //! Write current value into valueStr (always binary)
  virtual CarbonStatus format(char* valueStr, size_t len, CarbonRadix,
                               NetFlags, CarbonModel*) const;

  //! Update the shadow value based on the current value
  virtual void update(Storage* shadow) const;

  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int) const;

  //! Bybass extra deposit hooks, and update storage directly
  /*!
    This was added to allow OnDemand to update the value of its idle
    status net (which is a CarbonScalar) without affecting the
    mSeenDeposit flag in CarbonHookup.
   */
  virtual void bypassDeposit(const UInt32 *buf, const UInt32*);

protected:
  ValueState internalCompare(const Storage shadow, UInt32 scalar) const;
  
  //! Examines model drive value
  /*!
    If there is no drive value and driveBuf is not NULL, driveBuf is
    set to all 0's when the mode is eCalcDrive or eXDrive, else 1's if
    the net is an input.
  */
  virtual void examineModelDrive(UInt32* driveBuf, ExamineMode mode) const;

  //! ShellNet::formatForce
  virtual CarbonStatus formatForce(char* valueStr, size_t len, 
                                   CarbonRadix strFormat, NetFlags flags, ShellNet*, CarbonModel*) const;

  //! ShellNet::writeIfNotEqForce
  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, 
                                       ShellNet::Storage* shadow,
                                       NetFlags flags, ShellNet*);

  inline void doDeposit(const UInt32* buf, CarbonModel* model);
  inline void doFastDeposit(const UInt32* buf, CarbonModel* model);
};	

#endif
