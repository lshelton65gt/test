// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _CARBONEXPRNET_H_
#define _CARBONEXPRNET_H_


#include "shell/ShellNetWrapper.h"

class CarbonExpr;
class IODBIntrinsic;
class DynBitVector;

class CarbonExprNet : public ShellNetWrapper1ToN
{
public: 
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonExprNet(CarbonExpr* expr, const IODBIntrinsic* range);

  //! Virtual destructor
  virtual ~CarbonExprNet();

  /* 1 to N interface */
  
  virtual void gatherWrappedNets(NetVec* netVec) const;
  virtual void replaceWrappedNets(const NetVec& netVec);

  /* ShellNet interface */

  //! Set tristateness
  void putIsTristate(bool isTristate);
  //! Set forcibility
  void putIsForcible(bool isForcible);
  //! Set inputness
  void putIsInput(bool isInput);

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  
  
  //! Reimplements ShellNet::getIntrinsic()
  virtual const IODBIntrinsic* getIntrinsic() const;
  
  //! ShellNet::isInput()
  virtual bool isInput() const;
  
  //! ShellNet::isForcible()
  virtual bool isForcible() const;
  
  //! CarbonNet::isVector()
  virtual bool isVector () const;

  //! CarbonNet::isScalar()
  virtual bool isScalar() const;
  //! CarbonNet::isTristate()
  virtual bool isTristate() const;

  //! ShellNet::isDataNonZero()
  virtual bool isDataNonZero() const;

  //! ShellNet::setToDriven()
  virtual bool setToDriven(CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);

  //! ShellNet::setWordToUndriven()
  virtual bool setWordToUndriven(int index, CarbonModel* model);

  //! ShellNet::setRangeToUndriven()
  virtual bool setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model);

  //! CarbonNet::examine()
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  //! CarbonNet::examineWord()
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  //! CarbonNet::examineRange()
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;

  //! CarbonNet::deposit()
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! CarbonNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! CarbonNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::allocShadow()
  virtual Storage allocShadow() const;

  //! ShellNet::freeShadow()
  virtual void freeShadow(Storage* shadow);

  //! ShellNet::writeIfNotEq()
  virtual ValueState writeIfNotEq(char* valueStr, size_t len, Storage* shadow,
                                  NetFlags flags);

  //! ShellNet::force()
  virtual CarbonStatus force(const UInt32* buf, CarbonModel* model);
  //! ShellNet::forceWord()
  virtual CarbonStatus forceWord(UInt32 buf, int index, CarbonModel* model);
  //! ShellNet::forceRange()
  virtual CarbonStatus forceRange(const UInt32* buf, 
                                  int range_msb, int range_lsb, 
                                  CarbonModel* model);

  //! ShellNet::release()
  virtual CarbonStatus release(CarbonModel* model);
  //! ShellNet::releaseWord()
  virtual CarbonStatus releaseWord(int index, CarbonModel* model);
  //! ShellNet::releaseRange()
  virtual CarbonStatus releaseRange(int range_msb, int range_lsb, CarbonModel* model);
  

  //! Update the shadow value based on the current value
  /*!
    \param shadow Storage to write to. Must be the same size as the
    internal data
    \warning Unimplemented for memories
  */
  virtual void update(Storage* shadow) const;

  //! Compare the shadow and the storage values
  /*!
    \returns whether or not if the storage has changed from the
    shadow copy
    \warning unimplemented for memories
  */
  virtual ValueState compare(const Storage shadow) const;

  //! Write current value into valueStr according to strFormat
  /*!
    \warning Not implemented for memories
    This may change the size of the valueStr parameter if the valueStr
    size is not large or small enough to fit the entire value.
    \returns eCarbon_OK if the valueStr is big enough.
  */
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel*) const;


  //! Returns 1 if the net has a drive conflict.
  virtual int hasDriveConflict() const;


  //! Returns this
  virtual const CarbonExprNet* castExprNet() const;

  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);

  //! ShellNet::getExternalDrive()
  void getExternalDrive(UInt32* xdrive) const;

  //! Add the value control mask (constant bits) to the exprnet
  void putControlMask(const UInt32* controlMask);

  //! Get the control mask if it exists.
  virtual const UInt32* getControlMask() const;

  //! ShellNet::runvalueChangeCB()
  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const;

  //! This should pass to the sub net to execute correctly
  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive);

  //! This should pass to the sub net to execute correctly
  virtual void updateUnresolved(Storage* shadow) const;

  //! Returns false
  virtual bool isReal() const;
  //! Returns error
  virtual CarbonStatus examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const;
  //! Returns NULL 
  virtual const CarbonMemory* castMemory() const;
  //! Returns NULL 
  virtual const CarbonModelMemory* castModelMemory() const;
  //! Returns NULL 
  virtual const CarbonVectorBase* castVector() const;
  //! Returns NULL 
  virtual const CarbonScalarBase* castScalar() const;
  //! Returns NULL 
  virtual const ShellNetConstant* castConstant() const;
  //! Returns NULL 
  virtual const CarbonForceNet* castForceNet() const;

  //! asserts
  virtual void putToZero(CarbonModel*);
  //! asserts
  virtual void putToOnes(CarbonModel*);
  //! asserts
  virtual CarbonStatus setRange(int, int, CarbonModel*);
  //! asserts
  virtual CarbonStatus clearRange(int, int, CarbonModel*);
  //! asserts
  virtual int hasDriveConflictRange(SInt32, SInt32) const;
  //! asserts
  virtual CarbonStatus examineValXDriveWord(UInt32*, UInt32*, int) const;
  //! asserts
  virtual void setRawToUndriven(CarbonModel* model);
  //! asserts
  virtual ValueState writeIfNotEqForce(char*, size_t, Storage*, NetFlags, ShellNet*);
  //! asserts
  virtual CarbonStatus formatForce(char*, size_t, CarbonRadix, NetFlags, ShellNet*, CarbonModel*) const;
  //! asserts
  virtual void putChangeArrayRef(CarbonChangeType*);
  //! asserts
  virtual CarbonChangeType* getChangeArrayRef();

private:
  //! Attributes for expression nets
  enum NetAttributes {
    eNone = 0x0, //!< No special attributes
    eInput = 0x1, //!< net is an input
    eTristate = 0x2, //!< Net is a tristate
    eForcible = 0x4 //!< Net is forcible
  };

  CarbonExpr* mExpr;
  const IODBIntrinsic* mIntrinsic;
  const UInt32* mControlMask;
  NetAttributes mAttribFlags;

  class ConflictDetectWalk;
  class ResolveXDrive;
  class SubNetGatherWalk;
  class SubNetReplaceWalk;

  void prepareDepositRange(const UInt32* buf, const UInt32* drive,
                           DynBitVector* valBuf, DynBitVector* drvBuf,
                           size_t index, size_t length);
  
  void prepareDepositWord(UInt32 buf, UInt32 drive,
                          DynBitVector* valBuf, DynBitVector* drvBuf,
                          int index);
  
  void prepareDeposit(const UInt32* buf, const UInt32* drive,
                      DynBitVector* valBuf, DynBitVector* drvBuf,
                      int numWords, int bitWidth);
  
  void setupWordRange(size_t* bitIndex, size_t* length, int index);
};

#endif
