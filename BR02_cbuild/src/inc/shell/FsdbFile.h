// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef __FSDBFILE_H_
#define __FSDBFILE_H_


#include "util/CarbonPlatform.h"
#include "util/UtMap.h"
#include "util/UtHashMap.h"

#include "shell/WaveDump.h"
#include "ffwAPI.h"

//! Maps a UserType* to an integer
typedef UtHashMap<const UserType*, int>   UserTypeToIntMap;

class MsgContext;

class FsdbFile : public WaveDump
{
public:
  CARBONMEM_OVERRIDES

  //! destructor
  virtual ~FsdbFile();
  
  //! Static method to open FSDB file
  /*!
    \param fileName Name of file to open
    \param timescale Timescale of the simulation
    \param errMsg Pointer to UtString for error message
    \param msgContext to print messages while dumping
    \returns New FsdbFile object or NULL if the file could not be
    opened
  */
  static FsdbFile* open(const char* fileName, CarbonTimescale timescale,
                        UtString* errMsg, MsgContext* msgContext);

  //! Static method to open an auto-switch FSDB file
  /*!
    \param fileNamePrefix Prefix of name of file to open
    \param timescale Timescale of the simulation
    \param limitMegs Size limit in megabytes of each fsdb file.
    \param maxFiles Maximum number of files to switch to. 0 means
    unlimited.
    \param msgContext Message context used to notify user of switching event.
    \param errMsg Pointer to UtString for error message
    \returns New FsdbFile object or NULL if the file could not be
    opened
  */
  static FsdbFile* openAutoSwitch(const char* fileNamePrefix, 
                                  CarbonTimescale timescale,
                                  unsigned int limitMegs,
                                  unsigned int maxFiles,
                                  MsgContext* msgContext,
                                  UtString* errMsg);

  //! WaveDump::closeHierarchy()
  virtual FileStatus closeHierarchy(UtString* errMsg);

  //! WaveDump::flush()
  virtual FileStatus flush(UtString* errMsg);
  
  //! WaveDump::close()
  virtual FileStatus close(UtString* errMsg);

  //! WaveDump::supportsArrayScopes()
  virtual bool supportsArrayScopes() const;

  //! WaveDump::isFsdb()
  virtual bool isFsdb() const;

  //! WaveDump::addUserVar()
  virtual CarbonWaveUserData* addUserVar(CarbonVarType netType, 
                                         CarbonVarDirection direction,
                                         CarbonDataType dataType,
                                         int lbitnum, int rbitnum,
                                         CarbonClientData data, 
                                         const char* varName, 
                                         CarbonBytesPerBit bpb,
                                         const char* fullPathScopeName,
                                         const char* scopeSeparator,
                                         UtString* errMsg);

  //! WaveDump::addChangedWithDbl()
  virtual void addChangedWithDbl(WaveHandle* sig, double value);

  //! WaveDump::addChangedWithInt32()
  virtual void addChangedWithInt32(WaveHandle* sig, SInt32 value);
  
  //! WaveDump::addChangedWithInt64()
  virtual void addChangedWithInt64(WaveHandle* sig, UInt64 value);

  //! WaveDump::setValDbl()
  void setValDbl(WaveHandle* sig, double value);

  //! WaveDump::setValInt32()
  void setValInt32(WaveHandle* sig, SInt32 value);
  
  //! WaveDump::setValInt64()
  void setValInt64(WaveHandle* sig, UInt64 value);

  void getCurrentFsdbTime(UInt32 curTime[2]) const;

  //! Switch the current FSDB file (manual switch)
  /*! This function closes the currently open FSDB file and opens a
   *  new FSDB file using the provided name and the same variables
   *  provided for the old FSDB file.
   *
   *  \returns true if succesful
   */
  virtual bool switchFSDBFile(const char* newFile);

  //! Get the msgcontext
  MsgContext* getMsgContext() { return mMsgContext; }

  //! Helper function to switch the current file
  /*! Needs to be public so that it is callable from a static callback
   *  function provided to the Debussy API.
   */
  void switchFileHelper(const char* fileName, ffwObject* oldObj,
                        UInt32 high, UInt32 low);

protected:
  //! Constructor, not for public consumption
  /*!
    This is called by the static Open() method.
    \param fileName Name of file to open
    \param timescale Timescale of the simulation
    \param isSuccess True if file open was success
    \param errMsg Pointer to UtString for error message
    \param msgContext to print messages while dumping
  */
  FsdbFile(const char* fileName, CarbonTimescale timescale,
           bool* isSuccess, UtString* errMsg, MsgContext* msgContext);

  //! Called by derivatives.
  FsdbFile(CarbonTimescale timescale, MsgContext* msgContext);
  void writeFsdbHeader();

  //! This is used to create enum types
  //! This should be UIntPtr to be able to compile both on 32 and 64 bits machines.
  //! It is casted to void* later. 
  UIntPtr   mFsdbType;   

  //! This is a  map between userType and enum type
  UserTypeToIntMap   mUserTypeToEnumTypeMap;

  //! Create fsdb type for vhdl enumeration
  void* createEnumFSDBtype(const UserType* ut, WaveHandle* handle, UtString* errMsg);

  //! Create fsdb enum type for vhdl character type.
  //! The FSDB_DT_VHDL_CHARACTER is not used, because nCompare considers 
  //! it different from the enum type used by other simulators
  void* createEnumCharFSDBtype(const UserType* ut, WaveHandle* handle, UtString* errMsg);

  //! Put the new ffwObject (from switching)
  void putFfwObj(ffwObject* obj);

  //! WaveDump::dumpDatapAll()
  virtual void dumpDataAll();

  //! WaveDump::dumpDataOff()
  virtual void dumpDataOff();
  
  //! WaveDump::dumpDataOn()
  virtual void dumpDataOn();

  ffwObject* mFfwObj;

private:
  virtual void writeValueChanges();
  virtual void writeAllUserData();

  void updateUserData();

  virtual void writeHandleValue(WaveHandle* handle);

  // This is only in FsdbFile.cxx and it really needs to be inlined.
  // I can inline it here because it is private and it is defined and
  // used in only 1 file.
  inline void writeFsdbHandleValue(WaveHandle* handle);

  // Based on the properties in the sig, determine if the value is
  // actually a negative number
  inline SInt32 actualValue(WaveHandle* sig, SInt32 value);

  void closeFfwObj();
  void writeTime();
  void updateFsdbValue(WaveHandle* handle);
  void updateVhdlFsdbValue(WaveHandle* handle);
  void updateVhdlBoolean(WaveHandle* handle);
  void updateVhdlBit(WaveHandle* handle);
  void updateVhdlStdLogic(WaveHandle* handle);
  void updateVhdlStdULogic(WaveHandle* handle);
  void updateVhdlCharacter(WaveHandle* handle);
  void updateVhdlEnum(WaveHandle* handle);
  void writeHierBranch(WaveScope* scope, UtString* errMsg);
  FileStatus writeHierarchy(UtString* errMsg);
  const char* stringifyTimescale() const;
  void setTimescale();
  void setVersion();
  void setDate();

  void init();

  class AllVals;
  class UserDataMap;

  AllVals* mAllVals;
  UserDataMap* mUserDataMap;

  //! is FSDB_ENV_SYNC_CONTROL env "off" (false)
  bool mFsdbEnvSyncControl;

  bool mIsOpen;

#if pfICC
  char mPad[3];
#endif

  //! Message context for failures during dumping/switching
  MsgContext* mMsgContext;

  // forbid
  FsdbFile();
  FsdbFile(const FsdbFile&);
  FsdbFile& operator=(const FsdbFile&);
};

//! Fsdb auto switch file object
class FsdbAutoSwitchFile : public FsdbFile
{
public:
  //! constructor
  FsdbAutoSwitchFile(const char* fileNamePrefix, 
                     CarbonTimescale timescale, 
                     UInt32 limitMegs, 
                     UInt32 maxFiles,
                     MsgContext* msgContext,
                     bool* isSuccess,
                     UtString* errMsg);
  
  //! virtual destructor
  virtual ~FsdbAutoSwitchFile();

  //! Get the index used for name_<index>.fsdb
  UInt32 getFileIndex() const { return mFileIndex; }
  //! Increment or rollover 
  void incrFileIndex();
  
  //! Figure out the current filename
  void calcFilename();
  //! Get the current filename
  const char* getFilename() const;
  
  //! Get the filename prefix
  const char* getFilePrefix() const;

  //! Switch the current FSDB file (manual switch)
  /*! This function closes the currently open FSDB file and opens a
   *  new FSDB file using the provided name and the same variables
   *  provided for the old FSDB file.
   *
   *  Note that this function will update the file prefix using the
   *  new file name and reset the file index to 0. So the file name
   *  will be <newFile>0.fsdb.
   *
   *  \returns true if succesful
   */
  virtual bool switchFSDBFile(const char* newFile);

private:
  UInt32 mMaxFiles;
  UInt32 mFileIndex;
  UtString* mFilePrefix;
  UtString* mFilename;
};

#endif
