// -*-C++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _SHELLVISNET_H_
#define _SHELLVISNET_H_

#include "shell/ShellNetWrapper.h"

class CarbonDatabaseNode;

//! Base class for visibility nets, which map to parts of a wrapped net
class ShellVisNet : public ShellNetWrapper1To1
{
public:
  CARBONMEM_OVERRIDES

  ShellVisNet(ShellNet* wrappedNet, const CarbonDatabaseNode* dbNode, const IODBIntrinsic* intrinsic);
  virtual ~ShellVisNet();

  virtual const ShellVisNet *castShellVisNet() const;

  //! Return the DB node represented by this net
  virtual const CarbonDatabaseNode* getDBNode() const;

  //! Return the normalized bitselect, if valid
  virtual bool getBitsel(UInt32* bitsel) const = 0;

  // In many ways, a visibility net is similar to a CarbonExprNet.  In
  // fact, visibility nets were once implemented that way.  As such,
  // almost all the virtual functions that are reimplemented by
  // CarbonExprNet are reimplemented here as well, either in the base
  // class or in the derived versions.

  virtual void getTraits(Traits* traits) const;
  virtual const IODBIntrinsic* getIntrinsic() const;
  virtual bool isVector() const;
  virtual bool isScalar() const;
  virtual Storage allocShadow() const;
  virtual void freeShadow(Storage* shadow);
  virtual ValueState writeIfNotEq(char* valueStr, size_t len, Storage* shadow,
                                  NetFlags flags);
  virtual CarbonStatus format(char* valueStr, size_t len,
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel*) const;
  virtual void update(Storage* shadow) const;
  virtual void updateUnresolved(Storage* shadow) const;
  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const;

  // A visibility net is never a memory, even if the net it wraps is a
  // memory, so these need to be reimplemented.
  virtual const CarbonMemory* castMemory() const;
  virtual const CarbonModelMemory* castModelMemory() const;

  // Derivations must implement these
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const = 0;
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const = 0;
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const = 0;
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model) = 0;
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model) = 0;
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model) = 0;
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model) = 0;
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model) = 0;
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model) = 0;

  // These are not implemented for expression nets (they assert), so
  // there's no reason visibility nets should need them.

  //! asserts
  virtual void putToZero(CarbonModel*);
  //! asserts
  virtual void putToOnes(CarbonModel*);
  //! asserts
  virtual CarbonStatus setRange(int, int, CarbonModel*);
  //! asserts
  virtual CarbonStatus clearRange(int, int, CarbonModel*);
  //! asserts
  virtual int hasDriveConflictRange(SInt32, SInt32) const;
  //! asserts
  virtual CarbonStatus examineValXDriveWord(UInt32*, UInt32*, int) const;
  //! asserts
  virtual void setRawToUndriven(CarbonModel* model);
  //! asserts
  virtual ValueState writeIfNotEqForce(char*, size_t, Storage*, NetFlags, ShellNet*);
  //! asserts
  virtual CarbonStatus formatForce(char*, size_t, CarbonRadix, NetFlags, ShellNet*, CarbonModel*) const;
  //! asserts
  virtual void putChangeArrayRef(CarbonChangeType*);
  //! asserts
  virtual CarbonChangeType* getChangeArrayRef();

  // Can't have reals as part of visibility nets - returns error
  virtual CarbonStatus examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const;

protected:
  const CarbonDatabaseNode* mDBNode;            //!< The corresponding DB node for this net
  const IODBIntrinsic* mIntrinsic;              //!< Intrinsic representing the visibility net, not the primitive
  UInt32 mNumWords;                             //!< Number of UInt32s to represent the width of the wrapped net
  UInt32* mValBuf;                              //!< Scratch value buffer for examining/depositing the wrapped net
  UInt32* mDriveBuf;                            //!< Scratch drive buffer for examining/depositing the wrapped net
  UInt32 mBitWidth;                             //!< Bit width of the visibility net, not the primitive

  //! Class to handle storage operations
  /*!
    All nets need to allocate storage to store shadow copies of their
    values, compare the shadow to the current value, update the
    shadow, etc.  For visibility nets, the data type of the storage is
    not always the same, even for a particular derived type.  For that
    reason, a custom storage operator object is created for the
    particular data type, and all operations involving that type are
    delegated to the object.

    For example, a generic CarbonTriValShadow could be used for all
    visibility nets to represent their values.  However, for good
    performace while dumping waveforms, it makes sense to use a simple
    POD pointer when possible.  This allows the wave dumper (and the
    value change callback engine) to bypass the shell and access the
    model's storage directly.

    For visibility nets that have to select a bit from a vector, or a
    bit from a word of a memory, this can't be done.  However, for a
    whole word of a memory, we can get a POD pointer if the memory is
    non-sparse, and the word width is 64 bits or less.  In that case,
    the storage for shadows, compares, etc. can be a simple POD.
   */
  class StorageOp;
  class StorageOpNonPOD;
  template <typename POD> class StorageOpPOD;
  class StorageOpPODArray;
  template <typename POD> class StorageOpPODBitsel;
  class StorageOpPODArrayBitsel;

  //! The custom storage operation handler for the net
  /*!
    This needs to be mutable, because we need to create it before it's
    used, and that may occur in some reimplemented const functions.
    We can't allocate this in the constructor, because
    ShellNetMemBitsel is derived from ShellNetMemsel and would create
    two objects.
   */
  mutable StorageOp* mStorageOp;

  //! Allocate the correct kind of storage operator for this net
  virtual StorageOp* createStorageOp() const = 0;

};

//! Implements a bitselect of a vector net
class ShellVisNetBitsel : public ShellVisNet
{
public:
  CARBONMEM_OVERRIDES

  ShellVisNetBitsel(ShellNet* wrappedNet, const CarbonDatabaseNode* dbNode,
                    const IODBIntrinsic* intrinsic, UInt32 bit, SInt32 hdlBit);
  virtual ~ShellVisNetBitsel();

  virtual bool getBitsel(UInt32* bitsel) const;

  // Reimplementations of ShellNetWrapper1To1 functions
  virtual bool isDataNonZero() const;
  virtual bool setToDriven(CarbonModel* model);
  virtual bool setToUndriven(CarbonModel* model);
  virtual bool setWordToUndriven(int index, CarbonModel* model);
  virtual bool setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model);
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  virtual CarbonStatus force(const UInt32* buf, CarbonModel* model);
  virtual CarbonStatus forceWord(UInt32 buf, int index, CarbonModel* model);
  virtual CarbonStatus forceRange(const UInt32* buf,
                                  int range_msb, int range_lsb,
                                  CarbonModel* model);
  virtual CarbonStatus release(CarbonModel* model);
  virtual CarbonStatus releaseWord(int index, CarbonModel* model);
  virtual CarbonStatus releaseRange(int range_msb, int range_lsb, CarbonModel* model);
  virtual ValueState compare(const Storage shadow) const;
  virtual int hasDriveConflict() const;
  virtual void getExternalDrive(UInt32* xdrive) const;
  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive);

protected:
  const UInt32 mBit;            //!< Overall bit offset into the underlying net
  const UInt32 mWord;           //!< Word offset into the underlying net
  const UInt32 mWordBit;        //!< Bit offset into the word
  const UInt32 mBitMask;        //!< Bit mask for the word
  const SInt32 mHdlBit;         //!< The bit index of the HDL signal (not normalized)

  virtual StorageOp* createStorageOp() const;
};

//! Implements a wordselect of a memory net
class ShellVisNetMemsel : public ShellVisNet
{
public:
  CARBONMEM_OVERRIDES

  ShellVisNetMemsel(ShellNet* wrappedNet, const CarbonDatabaseNode* dbNode,
                    const IODBIntrinsic* intrinsic, SInt32 address);
  virtual ~ShellVisNetMemsel();

  virtual bool getBitsel(UInt32* bitsel) const;

  virtual void replaceNet(ShellNet* net);

  // Reimplementations of ShellNetWrapper1To1 functions
  // Memories aren't forcible, so those functions aren't needed.
  virtual bool isDataNonZero() const;
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  virtual ValueState compare(const Storage shadow) const;
  virtual void getExternalDrive(UInt32* xdrive) const;

protected:
  const UInt32 mAddress;        //!< Address into the memory for this word
  CarbonMemory* mMem;           //!< Cast of the wrapped net to a memory

  //!< Update the memory pointer based on the current net pointer
  void updateMem();

  //!< Fix MSB/LSB specified in terms of the visibility net's range to that of the underlying memory.
  void fixRange(SInt32* msb, SInt32* lsb) const;

  virtual StorageOp* createStorageOp() const;
};

//! Implements a bitselect of a wordselect of a memory net
class ShellVisNetMemBitsel : public ShellVisNetMemsel
{
public:
  CARBONMEM_OVERRIDES

  ShellVisNetMemBitsel(ShellNet* wrappedNet, const CarbonDatabaseNode* dbNode,
                       const IODBIntrinsic* intrinsic, SInt32 address, UInt32 bit, SInt32 hdlBit);
  virtual ~ShellVisNetMemBitsel();

  virtual bool getBitsel(UInt32* bitsel) const;

  // Reimplementations of ShellNetWrapper1To1 functions
  virtual bool isDataNonZero() const;
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  virtual void getExternalDrive(UInt32* xdrive) const;

protected:
  const UInt32 mBit;            //!< Overall bit offset into the underlying net
  const UInt32 mWord;           //!< Word offset into the underlying net
  const UInt32 mWordBit;        //!< Bit offset into the word
  const UInt32 mBitMask;        //!< Bit mask for the word
  const SInt32 mHdlBit;         //!< The bit index of the HDL signal (not normalized)

  virtual StorageOp* createStorageOp() const;
};

#endif
