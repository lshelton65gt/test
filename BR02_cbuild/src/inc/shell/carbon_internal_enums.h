/* -*- C++ -*- */
/*****************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __carbon_internal_enums_h_
#define __carbon_internal_enums_h_

  //! any UserType language  has to be either verilog or vhdl.
typedef  enum  {
    eLanguageTypeVerilog,
    eLanguageTypeVhdl,
    eLanguageTypeUnknown
} CarbonLanguageType;


  //! Signal type enumeration
  /*!
    Order is important. WaveHandle::isHardwareNet() checks if the type is > eVerilogTypeTime.
  */

typedef enum  { 
    eVerilogTypeUnknown,  //!< It's not verilog net
    eVerilogTypeUnsetNet,    //!< User type is not known
    eVerilogTypeInteger,     //!< integer type
    eVerilogTypeParameter,   //!< parameter type
    eVerilogTypeReal,        //!< real number type
    eVerilogTypeRealTime,    //!< Realtime type
    eVerilogTypeTime,        //!< Time type
    eVerilogTypeReg,         //!< register type
    eVerilogTypeSupply0,     //!< Power supply LOW
    eVerilogTypeSupply1,     //!< Power supply HIGH
    eVerilogTypeTri,         //!< Tri-state signal
    eVerilogTypeTriAnd,      //!< Tri-state And signal
    eVerilogTypeTriOr,       //!< Tri-state Or signal
    eVerilogTypeTriReg,      //!< Tri-state register
    eVerilogTypeTri0,        //!< Tri-state LOW signal
    eVerilogTypeTri1,        //!< Tri-state HIGH signal
    eVerilogTypeWAnd,        //!< Wired And signal
    eVerilogTypeWire,        //!< Wire type
    eVerilogTypeWOr,         //!< Wired Or type
    eVerilogTypeUWire        //!< UWire type
} CarbonVerilogType;

typedef enum {
    eVhdlTypeBit,      //!< VHDL bit type
    eVhdlTypeStdLogic, //!< VHDL std logic type
    eVhdlTypeStdULogic,//!< VHDL std ulogic type
    eVhdlTypeBoolean,  //!< VHDL boolean type
    eVhdlTypeIntType,  //!< VHDL inttype (not derived from eVhdlTypeInteger)
    eVhdlTypeInteger,  //!< VHDL integer type
    eVhdlTypeNatural,  //!< VHDL natural type
    eVhdlTypePositive, //!< VHDL positive type
    eVhdlTypeReal,     //!< VHDL real type
    eVhdlTypeChar,     //!< VHDL character type
    eVhdlTypeUnknown   //!< Error case
} CarbonVhdlType;



#endif
