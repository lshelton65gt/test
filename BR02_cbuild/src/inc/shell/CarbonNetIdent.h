// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef __CarbonNetIdent_h_
#define __CarbonNetIdent_h_


#ifndef __CARBON_SYS_INCLUDE_H__
#include "codegen/SysIncludes.h"
#endif
#ifndef __Expr_h_
#include "exprsynth/Expr.h"
#endif

class ShellNet;
class CarbonModel;

class CarbonNetIdent : public CarbonIdent
{
public: CARBONMEM_OVERRIDES
  // helper classes

  //! CarbonNet Expression Assign Context
  class AssignContext : public ExprAssignContext
  {
  public: CARBONMEM_OVERRIDES
    //! Enumeration for assignment mode
    enum ModeT {
      eDeposit, //!< Normal deposit
      eForce, //!<  Force
      eRelease, //!< Release
      eForceRange, //!< Force a range
      eReleaseRange //!< Release a range
    };
    
    //! Constructor - incomplete for ranges
    AssignContext(ModeT mode);
    
    //! virtual destructor
    virtual ~AssignContext();
    
    //! Get the assignment mode
    ModeT getMode() const;

    //! Put the index and length for a range
    void putIndexLength(size_t index, size_t length);

    //! Get the index and length for a range
    void getIndexLength(size_t* index, size_t* length);

    //! Is the assign still searching for a range?
    bool isActiveRange() const;

    //! Advance the range index and lower the length
    /*!
      Once the range index reaches 0 it stays at zero. Therefore,
      isActiveRange() would return false once that happens.
    */
    void chopIndexLength(size_t numBits);
    
    //! Copies internals 
    virtual ExprAssignContext* copy() const;

    //! Updates the status with the current status
    /*!
      If the status is already error, it cannot be changed to ok. So,
      this will do nothing in that case.
    */
    void updateStatus(CarbonStatus status);

    //! Get the current status
    CarbonStatus getStatus() const;

  private:
    ModeT mMode;
    size_t mBitIndex;
    size_t mRangeLength;
    CarbonStatus mStatus;

    AssignContext();
    AssignContext(const AssignContext&);
    AssignContext& operator=(const AssignContext&);
  };
  
  class EvalContext : public ExprEvalContext
  {
  public: CARBONMEM_OVERRIDES
    //! Enum for evaluation mode
    enum ModeT {
      eExamine, //! Normal examine mode
      eDriveCalc, //! Calculate drive mode
      eXDrive, //! Get xdata and xdrive
      eFormat //! Reinterpret value for format mode
    };
    
    //! Constructor with mode
    EvalContext(ModeT mode);

    //! virtual destructor
    virtual ~EvalContext();

    //! Set the mode
    void setMode(ModeT mode);
    
    //! Get the mode
    ModeT getMode() const;

    //! Resize the context
    void setBitWidth(int bitWidth);

    virtual ExprEvalContext* copy() const;
  private:
    ModeT mMode;

    EvalContext();
    EvalContext(const EvalContext&);
    EvalContext& operator=(const EvalContext&);
  };

  CarbonNetIdent(ShellNet* net, CarbonModel* model, const DynBitVector& useMask,
                 bool isSigned, UInt32 size);
  virtual ~CarbonNetIdent();
  
  virtual ptrdiff_t compare(const CarbonExpr* other) const;

  virtual const STAliasedLeafNode* getNode(DynBitVector* usageMask) const;

  virtual AssignStat assign(ExprAssignContext* context);

  virtual AssignStat assignRange(ExprAssignContext* context, 
                                 const ConstantRange& range);

  virtual void print(bool, int indent) const;

  virtual SignT evaluate(ExprEvalContext* evalContext) const;
  
  virtual const char* typeStr() const;

  virtual void composeIdent(ComposeContext* context) const;

  const CarbonNetIdent* castCarbonNetIdent() const;

  ShellNet* getShellNet() {
    const CarbonNetIdent* me = const_cast<const CarbonNetIdent*>(this);
    return const_cast<ShellNet*>(me->getShellNet());
  }

  const ShellNet* getShellNet() const;

  //! Replace the shellnet
  /*!
    This is needed for replay net instrumentation.
  */
  void putShellNet(ShellNet* replacement);
  
  //! Returns true if this ident represents the entire net
  virtual bool isWholeIdentifier() const;

private:
  ShellNet* mShellNet;
  CarbonModel* mCarbonModel;

  void prepareFormat(DynBitVector* drive) const;

  void doDeposit(AssignContext* context);
  void doDepositRange(AssignContext* context, 
                      const ConstantRange& range);
  
  void doForce(AssignContext* context);
  void doRelease(AssignContext* context);
  void doForceRange(AssignContext* context, 
                    const ConstantRange& range);
  void doReleaseRange(AssignContext* context,
                      const ConstantRange& range);
  
  void doCalcRange(AssignContext* context, 
                   size_t* limitLength,
                   int lsb,
                   int msb);

  bool allowDeposit(ExprAssignContext* context);

  void applyRange(AssignContext* context, int msb, int lsb, 
                  size_t chopAmount);
};
#endif
