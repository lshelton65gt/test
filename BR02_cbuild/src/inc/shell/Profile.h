// -*- C++ -*-
/*******************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef __PROFILE_H_
#define __PROFILE_H_


#ifndef __carbon_shelltypes_h_
#include "shell/carbon_shelltypes.h"
#endif
#ifndef __SHELLGLOBAL_H_
#include "shell/ShellGlobal.h"
#endif
#ifndef ShellMsgContext_H
#include "util/ShellMsgContext.h"
#endif
#ifndef __UtHashMap_h_
#include "util/UtHashMap.h"
#endif
#ifndef __UtIOStream_h_
#include "util/UtIOStream.h"
#endif
#if !pfWINDOWS
#include <sys/time.h>
#include <signal.h>             // For sighandler_t
#endif

class Profile
{
 public:
  CARBONMEM_OVERRIDES

  //! Return the single instance of the class
  static Profile* instance();
  //! Delete the instance.
  /*!
    We expose this rather than the destructor to avoid requiring this
    code: 
      delete Profile::instance();
    which instantiates the object to just immediately delete it.
   */
  static void destroy();

  void addModel(const CarbonModel* model, const char* modelName);
  // We don't have a removeModel() because we want profiling
  // information to survive after model goes away.

  void startSampling();
  void stopSampling();

  // Class instantiated by REGISTER_API macro in carbon_capi.cxx to
  // maintain the currently executing API's index, for profiling.  The
  // current API is stored in the model so we can keep track of models
  // in multiple threads.
  class CurrentAPI
  {
  public:
    CurrentAPI(const CarbonModel* model, unsigned apiIndex) : mPrevModel (0) {
      // TODO: If an API with no model, should probably store in a
      // global and live with multi-threading inaccuracies for those
      // APIs.

      // TODO: bail if not doing -profile (but only -simpleProfile).
      if (model) {
        const_cast<CarbonModel*>(model)->putCurrentAPI(apiIndex);
        // Save current model (before calling this API) for restoring in
        // destructor.  Otherwise, we charge non-Carbon code to Carbon.
        mPrevModel = ShellGlobal::gGetCurrentModel();
        ShellGlobal::gSetCurrentModel(const_cast <CarbonModel *> (model)->getObjectID ());
        mRestoreModel = true;
      }
      else
        mRestoreModel = false;
    }
    ~CurrentAPI() {
      if (mRestoreModel)
        ShellGlobal::gSetCurrentModel(mPrevModel);
    }
  private:
    bool mRestoreModel;
    CarbonObjectID *mPrevModel;
  };

 private:
  Profile();
  ~Profile();
  // Since model objects get deleted by carbonDestroy(), we need
  // to maintain our own information about the models we're profiling.
  class ModelInfo
  {
  public:
    CARBONMEM_OVERRIDES
    ModelInfo(const CarbonModel* model, const char* modelName);
    ModelInfo(const ModelInfo& copy);

    // Array of hitcounts for API function profiling.
    UtArray<unsigned> mModelAPIs;
    ShellGlobal::ModelBuckets mModelBuckets;
    ShellGlobal::ModelBuckets mModelScheduleBuckets;

    UInt32 mUnprofiledHits;     // Number of hits if compiled w/o -profile

    const UtString& getUID() { return mUID; }
    const UtString& getName() { return mName; }
  private:
    UtString mUID;
    UtString mName;             // Name of design
  };

  //! Write profile information for the given model to the given data file.
  void save(ModelInfo* modelInfo, UtOFStream& stream);

  // Static, because it's passed to signal().
  static void SIGPROFsignalHandler(int sig);

#if pfWINDOWS
  static void threadFunc(void*);
  static bool smSampling;
#else
  typedef void (*sighandler_t)(int);
  struct sigaction mPrevAction;
  itimerval mPrevItimerval;
#endif // pfWINDOWS

  volatile UInt32 mSampleCount;

  //! The collection of active models.
  typedef UtHashMap<const CarbonModel*, ModelInfo*> ModelsInfo;
  ModelsInfo mModelsInfo;

  static Profile* smTheInstance;
};
#endif
