// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __ON_DEMAND_STATE_H__
#define __ON_DEMAND_STATE_H__

#include "util/UtHashSet.h"
#include "util/UtArray.h"

#define CARBON_BUG7175

class OnDemandStateFactory;

//! Class to manage all state for onDemand comparisons
/*!  
  All the members of a Carbon Model that are needed to represent and restore
  its state are stored in this class.  This includes (almost) all
  state points in the design, plus things like inputs and
  transition-scheduled nets (including outputs).

  Inputs are needed because they're sticky, and don't need to be
  applied before every schedule call.  Transition-scheduled nets are
  essentially state points, since their values are calculated after
  their fanin state points are calculated, and there's (currently) no
  way to split that up and manually run a transition schedule to
  update the signals' values.  Sample-scheduled nets can be excluded
  because the Carbon Model already recalculates their values when they may not
  be correct.

  Also, large non-sparse and all sparse memories (which might be
  stateful) are excluded from the Carbon Model state.  The benefit of this is
  that the state data for a Carbon Model is both relatively small and of a
  fixed size, allowing fast memcmp()-based comparisons.  Correctness
  is achieved by aborting any state-tracking when one of these
  non-saved memories changes.

  The state is saved/restored in a way similar to that of a Carbon Model's
  normal save/restore, although custom functions (saving the reduced
  state set) are used.
 */
class OnDemandState
{
public:
  CARBONMEM_OVERRIDES

  friend class OnDemandStateFactory;

  //! Add some data to the current state
  void add(CarbonUInt32 num_bytes, const CarbonUInt8 *data);
  //! Read some data from the current state
  void read(CarbonUInt32 num_bytes, CarbonUInt8 *data);
  //! Closes and restructures a state whose size isn't known
  void close();
  //! Flushes any pending data that has not actually been saved yet
  void flush();
  //! Resets the internal state pointer to allow future restores
  void reset();
  //! Get the value at an offset
  /*!
    \warning No bounds/pointer checking is done here!
   */
  CarbonUInt8 getValue(UInt32 offset) { return mData[offset]; }
  //! Get the buffer pointer to an offset
  /*!
    \warning No bounds/pointer checking is done here!
   */
  UInt8 *getOffset(UInt32 offset) { return &mData[offset]; }
  //! Recalculate hash value, based on current data
  void recalculateHashValue();


  //! overloaded comparison operator
  bool operator==(const OnDemandState &other) const;

  CarbonUInt32 mSize;           //!< Number of bytes in this state
  CarbonUInt32 mNumUInt32;      //!< Number of UInt32s in this state
  CarbonUInt8 *mData;           //!< Pointer to the state data
  size_t mHashVal;              //!< Running checksum hash value

protected:
#ifdef CARBON_BUG7175
  UtArray<CarbonUInt32> mTempArray;     //!< Temporary state array until the required size is known
#else
  UtArray<CarbonUInt8> mTempArray;      //!< Temporary state array until the required size is known
#endif
  CarbonUInt32 mCurrIndex;              //!< Current index into the state buffer for reads/writes

  // Members to keep track of data we've been asked
  // to write, but haven't yet.
  const CarbonUInt8 *mBufferedDataStart;   //!< Pointer to the start of a data region we need to save
  const CarbonUInt8 *mBufferedDataNext;    //!< Pointer to the location that will continue the region
  CarbonUInt32 mBufferedDataSize;          //!< Number of bytes in the region

  //! Copies data in our pending buffer to the actual state storage
  void writeBufferedData();

  //! Allocates the state storage buffer
  void allocateBuffer();

private:
  //! Construct an onDemand state of known size
  OnDemandState(CarbonUInt32 size);
  //! Construct an onDemand state of unknown size
  OnDemandState();
  ~OnDemandState();

  //! Hide
  OnDemandState(const OnDemandState&);
  OnDemandState &operator=(const OnDemandState&);
};

//! Factory class for allocating objects and managing storage
class OnDemandStateFactory
{
public:
  CARBONMEM_OVERRIDES

  OnDemandStateFactory();
  ~OnDemandStateFactory();

  //! Allocates a new object of unknown size, owning storage
  OnDemandState *create();

  //! Allocates a new object of known size, owning storage
  OnDemandState *create(CarbonUInt32 size);

  //! Frees the last allocated object
  void freeLast();

  //! Frees all allocated objects
  void clear();

protected:
  typedef UtArray<OnDemandState *> ObjectArray;
  ObjectArray mAllocated;               //!< All objects allocated by the factory
};

//! Helper class for OnDemandStateSet hashing and comparisons
class OnDemandStateSetHelper
{
public:
  CARBONMEM_OVERRIDES

  size_t hash(const OnDemandState *var) const;
  bool equal(const OnDemandState *v1, const OnDemandState *v2) const;
};

//! HashSet to store onDemand state
typedef UtHashSet<OnDemandState*, OnDemandStateSetHelper> OnDemandStateSet;

#endif
