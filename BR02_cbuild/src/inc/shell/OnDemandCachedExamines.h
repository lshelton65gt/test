// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __ON_DEMAND_CACHED_EXAMINES_H__
#define __ON_DEMAND_CACHED_EXAMINES_H__

#include "util/UtHashMap.h"
#include "shell/ShellNet.h"

class ShellNetOnDemand;

//! Stores values of carbonExamine()s during idle states
class OnDemandCachedExamines
{
public:
  CARBONMEM_OVERRIDES

  OnDemandCachedExamines();
  ~OnDemandCachedExamines();

  //! Represents a single examine call
  class Examine
  {
  public:
    CARBONMEM_OVERRIDES

    // Creates an examine object with value/drive storage
    Examine(UInt32 num_uint32);
    ~Examine();

    //! Copies saved results of the examine, if available, returning success
    bool tryCopy(CarbonStatus *status, UInt32 *val, UInt32 *drive, ShellNet::ExamineMode mode);

    //! Saves the results of an examine
    void save(CarbonStatus status, UInt32 *val, UInt32 *drive, ShellNet::ExamineMode mode);

  protected:
    UInt32 mSize;           // Size of the value/drive buffers, in bytes
    UInt32 *mValue;         // Stored value buffer
    UInt32 *mDrive;         // Stored drive buffer
    bool mValueValid;       // Do we have a valid value?
    bool mDriveValid;       // Do we have a valid drive?
    ShellNet::ExamineMode mMode;      // The mode of the saved examine
    CarbonStatus mStatus;   // The return value of the saved examine

  private:
    //! Hide
    Examine(const Examine&);
    Examine &operator=(const Examine&);
  };

  //! Attempts to find and execute a saved examine, returning success
  /*!
    If successful, sets the status, value, and drive in the repective arguments.

    Regardless of success, the examine_ptr argument is set to the
    Examine object associated with this net.  It can be used in the
    future when saving the result of the real examine.
   */
  bool lookup(const ShellNetOnDemand *net, CarbonStatus *status, UInt32 *val, UInt32 *drive,
              ShellNet::ExamineMode mode, Examine **examine_ptr);

protected:
  typedef UtHashMap<const ShellNetOnDemand*, Examine*> ExamineMap;
  ExamineMap mExamineMap;   // Map of examines we've saved

private:
  //! Hide
  OnDemandCachedExamines(const OnDemandCachedExamines&);
  OnDemandCachedExamines &operator=(const OnDemandCachedExamines&);
};

#endif
