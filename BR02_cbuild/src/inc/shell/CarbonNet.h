// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONNET_H_
#define __CARBONNET_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#ifndef __carbon_shelltypes_h_
#include "shell/carbon_shelltypes.h"
#endif
#include <cstdlib>
#ifndef __MemManager_h_
#include "util/MemManager.h"
#endif

#if pfSPARCWORKS
using std::size_t;
#endif

typedef SInt32 CarbonMemAddrT;

class CarbonModel;

/*!
  Namespace to hold generic utility functions
*/
namespace CarbonUtil
{
  //! \returns Bitwidth of range
  size_t getRangeBitWidth(int range_msb, int range_lsb);
  
  //! \returns Number of words needed to represent range
  size_t getRangeNumUInt32s(int range_msb, int range_lsb);

  //! Returns lsb closer to msb by amount
  int closeDistance(int msb, int lsb, int amount, bool* add);

  //! Calculates the bit index and length of a subrange
  CarbonStatus calcIndexLength(int msb, int lsb, int range_msb, int range_lsb, size_t* index, size_t* length, CarbonModel* model);

}

// forward declarations
class ShellNet;

//! Virtual base class for runtime value referencing
/*!
  This class is the virtual method interface for accessing intrinsic
  net information. 
*/
class CarbonNet
{
public: 
  CARBONMEM_OVERRIDES

  //! Need virtual destructor so derived objects cleanup correctly.
  virtual ~CarbonNet() = 0;

  //! Is this object a VectorNet? MT-Safe
  virtual bool isVector () const=0;

  //! Returns true if the net is a scalar, false otherwise. MT-Safe
  virtual bool isScalar() const = 0;

  //! Returns true if the net is a real number, false otherwise. MT-Safe
  virtual bool isReal() const = 0;

  //! Returns true if this net is a tristate, false otherwise. MT-Safe
  virtual bool isTristate() const = 0;

  //! Bit width (MSB-LSB+1). MT-Safe
  virtual int getBitWidth () const = 0;

  //! in/out vector length for objects. MT-Safe
  virtual int getNumUInt32s () const = 0;

  //! \return LSB of vector.
  /*!
    This is MT-Safe.
    \warning Will return 0 for scalars
   */
  virtual int getLSB () const = 0;

  //! \return MSB of vector
  /*!
    This is MT-Safe.
    \warning Will return 0 for scalars.
  */
  virtual int getMSB () const = 0;

  //! Casts to a ShellNet
  ShellNet* castShellNet()
  {
    const CarbonNet* me = const_cast<const CarbonNet*>(this);
    return const_cast<ShellNet*>(me->castShellNet());
  }
  
  //! Const version of ShellNet cast
  virtual const ShellNet* castShellNet() const = 0;

  virtual void fastDeposit (const UInt32* buf, const UInt32* drive, CarbonModel* model) = 0;

  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model) = 0;

  //! ShellNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model) = 0;

  //! ShellNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model) = 0;

};

class UtOBStream;
class ShellNetRecordMem;

//! Structure for Memories
class CarbonMemory
{
public: 
  CARBONMEM_OVERRIDES
  
  //! constructor, called by descendants
  CarbonMemory() {}

  //! virtual destructor
  virtual ~CarbonMemory () {}

  //! Get the right address of this memory. MT-Safe
  virtual CarbonMemAddrT getRightAddr() const = 0;

  //! Get the left address of this memory. MT-Safe
  virtual CarbonMemAddrT getLeftAddr() const = 0;

  //! Returns the width of the memory. MT-Safe
  virtual int getRowBitWidth () const = 0;
  
  //! Returns the total number of UInt32s to represent a row of data. MT-Safe
  virtual int getRowNumUInt32s () const = 0;

  //! return Least Significant Bit of row. MT-Safe
  virtual int getRowLSB () const = 0;
  
  //! return Most Significant Bit of row. MT-Safe
  virtual int getRowMSB () const  = 0;

  //! Examine row at memory address
  /*!
    This is MT-Safe.
    \param address Address of memory to examine
    \param buf Pointer to an array of size getNumUInt32s()
  */
  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const = 0;

  //! Examine a word of a row at memory address
  /*!
    This is MT-Safe
    \param address Address of memory to examine
    \param index Index of word into row of memory
    \returns Word at index of row
  */
  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const = 0;

  //! Examine a range of a row at memory address
  /*!
    This is MT-Safe
    \param address Address of memory to examine
    \param buf Pointer to an array of UInt32s large enough to hold  the specified
    range
    \param range_msb Most significant bit of the range to examine
    \param range_lsb Least significant bit of the range to examine
  */
  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *buf, int range_msb, int range_lsb) const = 0;

  //! Write memory row from array of UInt32s.
  /*!
    This is MT-Safe

    \param address Address of memory to which to write
    \param buf Pointer to little-endian-arranged array of UInt32s that
    is size getNumUInt32s()
  */
  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf) = 0;


  //! Write 1 word of a memory row from a UInt32
  /*!
    This is MT-Safe
    \param address Address of memory to which to write
    \param buf Data word to write
    \param index Index of word to write into row of memory
  */
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index) = 0;

  //! Write specified range of memory row from array of UInt32s.
  /*!
    This is MT-Safe.
    \param address Address of memory to which to write
    \param buf Pointer to array of UInt32s containing the value for
    the specified range. The first bit of buf will be placed at the
    range_lsb of the row at the specified address.
    \param range_msb Most significant bit of range being written
    \param range_lsb Least significant bit of range being written
  */
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32* buf, int range_msb, int range_lsb) = 0;

  //! Format a memory address
  /*!
    This is MT-Safe.
    \param valueStr Buffer to write value to. 
    \param len The total length in bytes of valueStr.
    \param strFormat Format of value
    \param address Address of memory row
    \returns eCarbon_OK if the valueStr is big enough
  */
  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const = 0;


  //! Dump a range of memory addresses
  /*!
    \param startAddress Beginning address at which to dump
    \param endAddress Ending address at which to dump
    \param outFile File to which to output the range
    \param format Format to dump
   */
  CarbonStatus dumpAddressRange(CarbonMemAddrT startAddress, CarbonMemAddrT endAddress, const char* outFile, CarbonRadix format) const;

  //! Read a file of hex data into a memory
  /*!
    This is \e not MT-Safe.
    \param filename File to read
    \returns true if successful
  */
  virtual CarbonStatus readmemh(const char* filename) = 0;

  //! Read a file of binary data into a memory
  /*!
    This is \e not MT-Safe
    \param filename File to read
    \returns true if successful
  */
  virtual CarbonStatus readmemb(const char* filename) = 0;

  //! Read a file of hex data into a memory range
  /*!
    This is \e not MT-Safe.
    \param filename File to read
    \param startAddr Starting address
    \param endAddr Starting address
    \returns true if successful
  */
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr) = 0;

  //! Read a file of binary data into a memory range
  /*!
    This is \e not MT-Safe
    \param filename File to read
    \param startAddr Starting address
    \param endAddr Starting address
    \returns true if successful
  */
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr) = 0;

  ShellNet* castMemToShellNet()
  {
    const CarbonMemory* me = const_cast<const CarbonMemory*>(this);
    return const_cast<ShellNet*>(me->castMemToShellNet());
  }
  
  //! Casts to  ShellNet Object
  virtual const ShellNet* castMemToShellNet() const = 0;
  
  //! Get the CarbonModel pointer.
  /*! 
    This returns NULL if there is no CarbonModel associated with
    it. This is needed by carbon_capi.cxx for api profiling.
  */
  virtual CarbonModel* getCarbonModel() const = 0;

  //! Is this memory marked as depositable?
  virtual bool isDepositable() const = 0;

  //! Dump the value at an address along with a newline
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const = 0;

  //! Returns this as a ShellNetRecordMem, NULL if it isn't
  virtual const ShellNetRecordMem* castShellNetRecordMem() const = 0;

  //! modifiable cast to a ShellNetRecordMem
  ShellNetRecordMem* castShellNetRecordMem()
  {
    const CarbonMemory* me = const_cast<const CarbonMemory*>(this);
    return const_cast<ShellNetRecordMem*>(me->castShellNetRecordMem());
  }
  
private:
  CarbonMemory(const CarbonMemory&);
  CarbonMemory& operator=(const CarbonMemory&);
};

class ShellMemoryCBManager;
class ShellNetPlaybackMem;

class CarbonModelMemory : public CarbonMemory
{
public:
  CARBONMEM_OVERRIDES

  CarbonModelMemory() : mModel(NULL), mChangeArrayRef(NULL)
  {}

  virtual ~CarbonModelMemory() {}


  //! Set the module/instance to which this memory is related
  void setCarbonModel(CarbonModel* instance) {
    mModel = instance;
  }

  //! Is this memory marked as depositable?
  virtual bool isDepositable() const;

  //! Returns NULL
  virtual const ShellNetRecordMem* castShellNetRecordMem() const;

  //! Returns the model pointer
  virtual CarbonModel* getCarbonModel() const;

  //! Returns the ShellMemoryCBManager for this net
  /*!
    The ShellMemoryCBManager is the list of callbacks that the Carbon Model
    calls whenever the memory is written.

    Returns NULL if there is no callback manager, which probably means
    that the Carbon Model is older than the release that has memory write
    callbacks.
  */
  virtual ShellMemoryCBManager* getShellMemoryCBManager() = 0;

  //! This adds any non-zero values in this memory to the playback mem
  virtual void syncPlaybackMem(ShellNetPlaybackMem* playMem) const = 0;

  //! Is this a sparse memory?
  virtual bool isSparse() const = 0;

  //! Returns a pointer to the storage for a particular address.
  virtual void* getStoragePtr(CarbonMemAddrT addr) const = 0;
  
protected:
  CarbonModel* mModel;
  CarbonChangeType* mChangeArrayRef;

  //! Perform cleanup/marking tasks after a memory write
  /*!
    This is needed for depositable memories, where a memory is marked
    as depositable in the directives file and needs to run logic after
    every api deposit to it.

    This also sets the change array reference
  */
  void postMemoryWrite();

  //! Sets change array reference
  void putChangeArrayRef(CarbonChangeType*);
  //! Returns change array reference
  CarbonChangeType* getChangeArrayRef();

private:
  CarbonModelMemory(const CarbonModelMemory&);
  CarbonModelMemory& operator=(const CarbonModelMemory&);
  
};

#endif
