// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __SHELLNETRECORD_H_
#define __SHELLNETRECORD_H_

#include "shell/ShellNetWrapper.h"
#include "util/UtHashSet.h"

//! Class for recording stimuli to a net
/*!
  This wraps a net that needs to be recorded.

  Note that expression nets and forcible nets do not get recorded, but
  their subnets do. As a result, force is not recorded as a force, but
  rather a deposit to the storage value and the forcemask.
 */
class ShellNetRecord : public ShellNetReplay
{
public:
  CARBONMEM_OVERRIDES

  ShellNetRecord(ShellNet* subNet, Touched* touchBuffer);

  virtual ~ShellNetRecord();

  //! Returns the same number as getNumUInt32s.
  /*!
    This is inlined and non-virtual unlike getNumUInt32s
  */
  UInt32 getRecordNumWords() const { return mNumWords; }

  //! Returns this
  virtual const ShellNetRecord* castShellNetRecord() const;

  //! Get the value buffer
  virtual UInt32* getValueBuffer() = 0;
  //! Get the drive buffer (NULL if not a tristate)
  virtual UInt32* getDriveBuffer() = 0;

  //! Get the external reference index
  /*!
    This purpose of this is to have a number-based referencing system
    such that the owner of these nets can properly categorize
    them. This is for external organization (i.e., api-caused
    manipulations). 
    
    If the index is not set, this defaults to returning -1. 
  */
  SInt32 getExternalIndex() const { return mExternalIndex; }

  
  //! Get the internal reference index
  /*!
    This purpose of this is to have a number-based referencing system
    such that the owner of these nets can properly categorize
    them. This is for internal organization (i.e., Carbon Model-caused
    manipulations). 

    If the index is not set, this defaults to returning -1. 
  */
  SInt32 getInternalIndex() const { return mInternalIndex; }
  
  //! Set the internal index
  void putInternalIndex(SInt32 index) { mInternalIndex = index; }
  //! Set the external index
  void putExternalIndex(SInt32 index) { mExternalIndex = index; }

  //! Create a record net for primNet based on this net's storage
  /*!
    This is called when instrumenting a CarbonNet.
    
    All record storage nets get created during record setup. Aliases
    of the storage net get created here.
  */
  virtual ShellNetRecord* cloneStorage(ShellNet* primNet) = 0;

  //! Was this net written to?
  /*!
    The mTouched buffer should get set to zero by the container class
    holding this net. For replay, the EventRecorder owns the master
    touch buffer and sets it to zero after every recordSchedule call.
  */
  bool isTouched() const { return *mTouched != 0; }

  //! Sets touched flag to true, also clearing the masked flag
  inline void setTouched() {
    mTouched->set();
  }

  //! Sets touched flag to true, without affecting the masked flag
  inline void setTouchedOnly() {
    mTouched->setTouchedOnly();
  }

  //! Sets Touched to be true if val is non-zero
  inline void addTouched(UInt32 val)
  {
    *mTouched |= val;
  }

  //! Sets the masked flag in the net's touched buffer
  inline void setMasked() {
    mTouched->setMasked();
  }

  //! Returns whether this net's API access was masked
  /*!
    A masked access is one that doesn't change the entire net,
    e.g. carbonDepositRange().
   */
  bool isMasked() const { return mTouched->isSetMasked(); }

  //! Asserts. Record nets are not released/forced
  virtual CarbonStatus force(const UInt32* buf, CarbonModel* model);
  //! Asserts. Record nets are not released/forced
  virtual CarbonStatus forceWord(UInt32 buf, int index, CarbonModel* model);
  //! Asserts. Record nets are not released/forced
  virtual CarbonStatus forceRange(const UInt32* buf, 
                                  int range_msb, int range_lsb, 
                                  CarbonModel* model);
  

  //! Asserts. Record nets are not released/forced
  virtual CarbonStatus release(CarbonModel* model);
  //! Asserts. Record nets are not released/forced
  virtual CarbonStatus releaseWord(int index, CarbonModel* model);
  //! Asserts. Record nets are not released/forced
  virtual CarbonStatus releaseRange(int range_msb, int range_lsb, CarbonModel* model);

  //! asserts
  virtual void putToZero(CarbonModel* model);
  //! asserts
  virtual void putToOnes(CarbonModel* model);
  //! asserts
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! asserts
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);

  //! asserts
  virtual void setRawToUndriven(CarbonModel*);

  //! asserts
  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const;

  //! returns false
  virtual bool setToUndriven(CarbonModel* model);

protected:
  //! size of net in words
  UInt32 mNumWords;
  //! Mask to sanitize value
  UInt32 mTailMask;
  //! Scratch buffer for value. Has appropriate number of words
  UInt32* mValueScratch;
  //! Touch buffer
  /*!
    This gets set to 1 anytime the net is written. This does not get
    set for memories, and in fact, should be null for memories.

    In the case of response nets, this is not exactly the
    case. isTouched() will be true for those only if the value has
    changed.
  */
  Touched* mTouched;
  
  // Reference into an array where the replay manager can find this
  // net for external writes
  SInt32 mExternalIndex;
  // Reference into an array where the replay manager can find this
  // net for internal writes
  SInt32 mInternalIndex;
};

class ShellNetRecordTwoStateWord : public ShellNetRecord
{
public:
  CARBONMEM_OVERRIDES

  ShellNetRecordTwoStateWord(ShellNet* subNet, UInt32* valBuf, UInt32* maskBuf, Touched* touchBuffer);

  virtual ~ShellNetRecordTwoStateWord();
  
  //! Returns the value buffer
  virtual UInt32* getValueBuffer();
  //! Returns NULL
  virtual UInt32* getDriveBuffer();

  //! Clone the storage
  virtual ShellNetRecord* cloneStorage(ShellNet* primNet);

  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Set all bits in the value to 0
  virtual void putToZero(CarbonModel* model);
  //! Set all bits in the value to 1
  virtual void putToOnes(CarbonModel* model);
  //! Set all bits in the range of the value to 1
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! Set all bits in the range of the value to 0
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);


protected:
  //! Pointer to the buffer where the value of the net is stored
  UInt32* mValBuf;
  //! Pointer to the buffer where the mask of the net's access is stored
  UInt32* mMaskBuf;
  //! True if the net owns the storage for the mask buffer
  bool mOwnMaskBuf;

private:
  inline void recordValueCheck(const UInt32* buf);
  inline void recordValue(const UInt32* buf);
  inline void doFastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  inline void doFastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
};

//! Two state clock
/*!
  Recording a clock requires that we force a change during
  processChanges if we see a double edge prior to the
  carbonSchedule. So, if the value is currently 1, and then we deposit
  a 0 and then a 1, we need to make the player require a posedge.
  
  This is done by double-buffering the clock value. The shadow is the
  shadow that is compared against in processChanges.
*/
class ShellNetRecordTwoStateClk : public ShellNetRecordTwoStateWord
{
public:
  CARBONMEM_OVERRIDES

  ShellNetRecordTwoStateClk(ShellNet* subNet, UInt32* valBuf, UInt32* maskBuf, UInt32* valShadow, Touched* touchBuffer);

  virtual ~ShellNetRecordTwoStateClk();
  
  //! Clone the storage
  virtual ShellNetRecord* cloneStorage(ShellNet* primNet);

  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Set all bits in the value to 0
  virtual void putToZero(CarbonModel* model);
  //! Set all bits in the value to 1
  virtual void putToOnes(CarbonModel* model);
  //! Set all bits in the range of the value to 1
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! Set all bits in the range of the value to 0
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);

private:
  UInt32* mValShadow;

  inline void saveCurrentValue();
};

//! Two state clock that is also a state output
/*!
  Clocks that are also state outputs (i.e. depositible statepoints in
  the design) are more difficult than ordinary clocks.  Like ordinary
  clocks, we need to be able to support fake edges.  However, we can't
  just shadow the last deposited value, because the net's value may
  have changed as a result of the model running.

  For example, if we deposit a 1, then the model runs and changes the
  net's value to 0, and then we deposit a 1 again, an edge should be
  detected.  If instead the second deposit was a 0, an edge should not
  be detected.

  The chosen solution is to throw out the idea of shadow value
  comparisons and just record every time this net is touched along
  with the value.  We save the last two deposits so fake edges can be
  detected.

  This is done by encoding multiple deposits in the 32-bit value
  buffer for the net.  Since clocks must be scalars, this isn't a
  problem.  The encoding is described in an enumerated type in the
  ShellNetReplay class.
*/
class ShellNetRecordTwoStateClkStateOutput : public ShellNetRecordTwoStateWord
{
public:
  CARBONMEM_OVERRIDES

  ShellNetRecordTwoStateClkStateOutput(ShellNet* subNet, UInt32* valBuf, UInt32* maskBuf, Touched* touchBuffer);

  virtual ~ShellNetRecordTwoStateClkStateOutput();
  
  //! Clone the storage
  virtual ShellNetRecord* cloneStorage(ShellNet* primNet);

  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Set all bits in the value to 0
  virtual void putToZero(CarbonModel* model);
  //! Set all bits in the value to 1
  virtual void putToOnes(CarbonModel* model);
  //! Set all bits in the range of the value to 1
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! Set all bits in the range of the value to 0
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);

private:
  //! Record the current stimulus, specifying whether the net has already been touched
  void saveStimulus(bool touched);

  //! Scratch buffer for parent class to use
  /*!
    In order to use our value encoding, we need a scratch variable for
    the parent class to update when it does deposits, etc.  We can
    then update the real buffer.
   */
  UInt32 mRawValBuf;

  //! The real record buffer pointer
  /*!
    The encoded deposit values will be stored in this buffer, which is
    the one that replay uses for recording stimulus.
   */
  UInt32 *mEncodedValBuf;
};

class ShellNetRecordTwoStateA : public ShellNetRecord
{
public:
  CARBONMEM_OVERRIDES

  ShellNetRecordTwoStateA(ShellNet* subNet, UInt32* valBuf, UInt32* maskBuf, Touched* touchBuffer);

  virtual ~ShellNetRecordTwoStateA();

  //! Clone the storage
  virtual ShellNetRecord* cloneStorage(ShellNet* primNet);

  //! Returns the value buffer
  virtual UInt32* getValueBuffer();
  //! Returns NULL
  virtual UInt32* getDriveBuffer();

  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Set all bits in the value to 0
  virtual void putToZero(CarbonModel* model);
  //! Set all bits in the value to 1
  virtual void putToOnes(CarbonModel* model);
  //! Set all bits in the range of the value to 1
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! Set all bits in the range of the value to 0
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);

private:
  //! Pointer to the buffer where the value of the net is stored
  UInt32* mValBuf;
  //! Pointer to the buffer where the mask of the net's access is stored
  UInt32* mMaskBuf;
  //! True if the net owns the storage for the mask buffer
  bool mOwnMaskBuf;
};

class ShellNetRecordTristate : public ShellNetRecord
{
public:
  CARBONMEM_OVERRIDES

  ShellNetRecordTristate(ShellNet* subNet, UInt32* valBuf, UInt32* drvBuf, Touched* touchBuffer);

  virtual ~ShellNetRecordTristate();

  //! Clone the storage
  virtual ShellNetRecord* cloneStorage(ShellNet* primNet);

  //! Returns the value buffer
  virtual UInt32* getValueBuffer();
  //! Returns the drive buffer
  virtual UInt32* getDriveBuffer();

  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual void setRawToUndriven(CarbonModel* model);

  virtual bool setToUndriven(CarbonModel* model);

  virtual bool resolveXdrive(CarbonModel* model);

protected:
  UInt32* mValBuf;
  UInt32* mDrvBuf;

  //! Scratch buffer for drive. Has appropriate number of words
  UInt32* mDriveScratch;
};

//! Object for recording Bidirect clocks 
/*!
  All we do here is simply save off the current value of the value and
  drive into shadows before depositing. By doing this, the
  processChanges function will detect an actual change on a double
  edge and record it.
*/
class ShellNetRecordBidirectClk : public ShellNetRecordTristate
{
public:
  CARBONMEM_OVERRIDES

  ShellNetRecordBidirectClk(ShellNet* subNet, UInt32* valBuf, UInt32* drvBuf, UInt32* valShadow, UInt32* drvShadow, Touched* touchBuffer);

  virtual ~ShellNetRecordBidirectClk();

  //! Clone the storage
  virtual ShellNetRecord* cloneStorage(ShellNet* primNet);

  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual void setRawToUndriven(CarbonModel* model);

  virtual bool setToUndriven(CarbonModel* model);

  virtual bool resolveXdrive(CarbonModel* model);

private:
  UInt32* mValShadow;
  UInt32* mDrvShadow;

  inline void saveCurrentValue();
};

//! Object for recordable memories
class ShellNetRecordMem : public ShellNetRecord
{
public:
  CARBONMEM_OVERRIDES

  //! constructor
  /*!
    \param mem The primitive memory to wrap in a record net
    \param externalIndex Outside reference for external memory writes
    \param addrChanges The set of addr changes this net writes to to
    notify when a memory write occurs
  */
  ShellNetRecordMem(ShellNet* mem, UInt32 externalIndex, SInt32HashSet* addrChanges);

  virtual ~ShellNetRecordMem();

  //! Returns this
  virtual const ShellNetRecordMem* castShellNetRecordMem() const;

  //! Clone the storage
  virtual ShellNetRecord* cloneStorage(ShellNet* primNet);

  //! returns null. Mems do not have value buffers
  virtual UInt32* getValueBuffer();
  //! returns null. Mems do not have drive buffers
  virtual UInt32* getDriveBuffer();

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);


  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);

  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32* buf, int range_msb, int range_lsb);
  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);

  void clearChanges() { mChangedAddrs->clear(); }

  const UInt32* fetchValue(SInt32 addr);
  const SInt32HashSet* getAddressChanges() const { return mChangedAddrs; }

  // not allowed on memory nets

  //! asserts
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! asserts
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! asserts
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  //! asserts
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! asserts
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! asserts
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

private:
  // list of externally-changed addresses. Cleared with clearChanged();
  SInt32HashSet* mChangedAddrs;

  inline void recordMemWrite(CarbonMemAddrT addr);
  inline CarbonStatus recordReadMem(const char* fileName, CarbonRadix radix);
  inline CarbonStatus recordReadMem(const char* filename, SInt64 startAddr, SInt64 endAddr, CarbonRadix radix);
};

//! Object for net responses during record.
/*!
  This wraps outputs and observables, so they can update the response
  buffer while recording.
*/
class ShellNetRecordResponse : public ShellNetRecord
{
public:
  CARBONMEM_OVERRIDES

  ShellNetRecordResponse(ShellNet* subNet, UInt32* valBuf, Touched* touchBuffer);
  
  virtual ~ShellNetRecordResponse();

  //! Returns the value buffer
  virtual UInt32* getValueBuffer();
  //! Returns NULL, unless this is a tristate
  virtual UInt32* getDriveBuffer();
  
  //! Update the recording buffer with the current value
  virtual void updateBuffer(CarbonModel*  model);

  // not allowed on response nets
  //! asserts
  virtual ShellNetRecord* cloneStorage(ShellNet* primNet);
  //! asserts
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! asserts
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! asserts
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  //! asserts
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! asserts
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! asserts
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

protected:
  UInt32* mValBuf;
};

class ShellNetRecordResponseTristate : public ShellNetRecordResponse
{
public:
  CARBONMEM_OVERRIDES

  ShellNetRecordResponseTristate(ShellNet* subNet, 
                                 UInt32* valBuf, UInt32* drvBuf, 
                                 Touched* touchBuffer);
  
  virtual ~ShellNetRecordResponseTristate();

  //! Returns the drive buffer
  virtual UInt32* getDriveBuffer();
  
  //! Update the recording buffer with the current value
  virtual void updateBuffer(CarbonModel*  model);

private:
  UInt32* mDrvBuf;
};


//! Object for handling deposits to non-depositable nets
/*!
  The nature of Carbon models is such that state points that are not
  marked as depositable by the user may nonetheless have deposits to
  them "stick".  However, only explicitly depositable nets are
  recorded by replay.  To do otherwise would require extensive
  rearchitecture.

  The best we can do is to print an error when a deposit occurs to a
  non-depositable net while in record mode.
*/
class ShellNetRecordNotDepositable : public ShellNetRecord
{
public:
  CARBONMEM_OVERRIDES

  ShellNetRecordNotDepositable(ShellNet* subNet);

  virtual ~ShellNetRecordNotDepositable();
  
  //! Returns the value buffer
  virtual UInt32* getValueBuffer();
  //! Returns NULL
  virtual UInt32* getDriveBuffer();

  //! Clone the storage
  virtual ShellNetRecord* cloneStorage(ShellNet* primNet);

  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Set all bits in the value to 0
  virtual void putToZero(CarbonModel* model);
  //! Set all bits in the value to 1
  virtual void putToOnes(CarbonModel* model);
  //! Set all bits in the range of the value to 1
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! Set all bits in the range of the value to 0
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);

  // Depositable memories use a completely different mechanism to
  // record their accesses/changes.  Non-depositable memories need to
  // implement these so that errors can be issued.
  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32* buf, int range_msb, int range_lsb);
  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);
};


//! class ReplayRecordBuffer
/*! This class manages the record buffers for all nets in one
 *  place. It manages storage for recording all nets and determining
 *  what nets/words changed.
 */
class ReplayRecordBuffer
{
public: CARBONMEM_OVERRIDES
  //! constructor
  ReplayRecordBuffer(UInt32 dataSize);

  //! destructor
  ~ReplayRecordBuffer();

  //! Allocate a set of words in the record buffer for a net
  /*!
   A primary buffer and optionally a secondary buffer can be
   allocated.  If the secondaryBuffer parameter is NULL, no space will
   be allocated for the mask.

   Returns the buffer pointers for data/mask, the touched pointer, and
   the offset into the buffer. (The latter is used for writing to the
   symbol table used by playback).

   The primary buffer is typically used for the net's value, while the
   secondary buffer is used for its drive or mask.
   */
  void allocateValues(UInt32 netWordSize,
                      UInt32* bufferIndex,
                      UInt32** primaryBuffer,
                      UInt32** secondaryBuffer,
                      UInt32** primaryShadow,
                      UInt32** secondaryShadow,
                      ShellNetReplay::Touched* touched,
                      ShellNetRecord* stimNet);

  //! Associates a stimulus net with a range of offsets in the record buffer
  /*!
    This is an alternative to passing the stimulus net as a parameter
    to allocateValues() when reserving storage for a record net.  It
    should only be used if the stimulus net does not yet exist at
    allocation time.
   */
  void mapOffsetsToStimNet(UInt32 offset, UInt32 words, ShellNetRecord *stimNet);

  //! Callback class for changed values
  class ChangeCallback
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    ChangeCallback() {}

    //! virtual destructor
    virtual ~ChangeCallback() {}

    //! Callback function - must be overriden
    /*! Get notified about a changed value. The changed values are
     *  made up of a offset from the beginning of the buffer, the size
     *  of the change and a pointer to the start of the changed value.
     *
     *  The size can be 1, 4, 8, 16, and so on.
     */
    virtual void changedData(UInt32 startByte,UInt32 size,const UInt8* value) = 0;
  };

  //! Look for and handle changes in the buffer
  /*! This process does three things for all changed indexes:
   *
   *  - calls the callback function provided
   *
   *  - Sets the corrected touched array entry
   *
   *  - Updates the shadow value
   */
  void processChanges(bool recordAll, ChangeCallback* callback);

  //! Get the number of words for this data buffer
  UInt32 getNumDataWords(void) const { return mNumWords; }

  //! Get the Value (data) Buffer as a DynBitVector*
  DynBitVector* getValueBufferPointer() { return &mValueBV; }
  
  //! Get the data buffer - const
  const UInt32* getDataBuffer() const { return mValues; }

  //! Get the value buffer - non-const
  /*!
    Not overloading getDataBuffer() because I don't want to step on
    const optimization by gcc, in case that is happening. Overloading
    it to non-const would most likely make all calls non-const.
  */
  UInt32* getValueBuffer() { return mValues; }

  //! Get the ShadowBuffer, modifiable
  UInt32* getShadowBuffer() { return mShadowValues; }

  //! Update the shadow from the value
  void updateShadow(void);

  //! Update the value from the shadow
  void updateValue(void);

  //! Print the actual values in hex
  void printValues() const;

  //! Print the shadow values in hex
  void printShadows() const;

private:
  //! helper function to call the callback function on data changes
  inline void notifyChange(ChangeCallback* callback, UInt32 notifySize,
                           UInt32 notifyOffset);

  //! The storage for the value 
  /*!
    This needs to be DynBitVectors for easy saving to disk.
  */
  DynBitVector mValueBV;
  //! The storage for the shadow
  /*!
    This doesn't need to be a DynBitVector, but it should have the
    same form as the mValueBV, which it is shadowing.
  */
  DynBitVector mShadowBV;

  //! Pointer to Storage for the value array
  UInt32* mValues;

  //! Pointer Storage for the shadow array
  UInt32* mShadowValues;

  //! The map from the value array offset to the correct touched pointer
  ShellNetReplay::Touched** mTouchedReference;

  //! A map from offset to the stim net matching the response net
  /*! We need this to record response nets so that they can tell if a
   *  deposit occurred. That is because a Carbon Model value should clobber the
   *  deposited value and so we have to treat deposited nets as
   *  changed.
   */
  ShellNetRecord** mStimNets;

  //! The current value allocated index
  UInt32 mAllocIndex;

  //! The number of data words
  UInt32 mNumWords;
}; // class ReplayRecordBuffer

#endif
