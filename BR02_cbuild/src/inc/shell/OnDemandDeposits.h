// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __ON_DEMAND_DEPOSITS_H__
#define __ON_DEMAND_DEPOSITS_H__

#include "util/UtHashSet.h"
#include "util/UtArray.h"
#include "util/UtHashMap.h"

class ShellNetOnDemand;
class OnDemandDepositFactory;
class CarbonModel;

//! Class to manage all deposits that will be used in onDemand comparisons
/*!
  This class potentially stores all the deposits for a given schedule
  call while searching for an onDemand pattern.  It persists for the
  life of the pattern as the expected stimulus for a particular state
  in the pattern.

  The class keeps an array of ShellNetOnDemand pointers for all
  eligible nets that have been looked up with carbonFindNet, and
  buffer space to store all the nets' deposited values.  OnDemandMgr
  maintains one master copy of this class, and makes copies as needed
  to store in the repeating sequence.  As a result, a particular net
  will always have the same pointer/value offsets in any two instances
  of this class.  This makes comparisons easy - just a memcmp, after
  verifying that the number of nets and values is the same.

  Also, the pointer and value arrays are logically separate, but
  physically contiguous in memory.  This allows both arrays to be
  compared with a single memcmp.
 */
class OnDemandDeposits
{
public:
  CARBONMEM_OVERRIDES

  friend class OnDemandDepositFactory;

  //! Info about a net's pointer and value location in the buffers
  /*!

    The subordinate entry is only used for clocks.  Normally, only the
    primary entry is used.  However, it's legal to create a clock edge
    by calling carbonDeposit() twice with different values, followed
    by a single schedule call.  In this case, the first deposit will
    be stored with the primary entry, and the second will be stored
    with the subordinate entry.

    This keeps equality testing simple.  In situations in which a fake
    edge is not created, the subordinate entry's location in the
    buffer will be empty.  Note that an occurrance of a fake edge
    where a real edge is expected will cause a divergence, and vice
    versa.

    When we apply saved deposits after a divergence, the order is
    important.  For this reason, only the primary entry is saved in
    the net map.  When we encounter a primary that has a subordinate,
    we apply both deposits in the correct order.

    Nothing special needs to be done in debug mode.  Both the primary
    and subordinate entries have storage in the pointer/value arrays,
    so we'll be able to map any differences back to the original net.
   */
  struct NetMapEntry
  {
    CARBONMEM_OVERRIDES

    ~NetMapEntry()
    {
      // The subordinate isn't included in the net map, so it needs to
      // be deleted here.
      delete subord;
    }

    CarbonUInt32 net_index;         //!< Index into the mNets array
    CarbonUInt32 val_index;         //!< Index into the mVals array
    CarbonUInt32 num_uint32;        //!< Size of the value
    CarbonUInt32 last_mask;         //!< Mask of valid bits in the last word
    NetMapEntry *subord;            //!< Subordinate entry, only for clocks
  };

  //! Adds a net to our known list, possibly as a subordinate to an existing master
  OnDemandDeposits::NetMapEntry *addNet(ShellNetOnDemand *net, OnDemandDeposits::NetMapEntry *master);
  //! Adds a deposit value for a net
  /*!
    This can only be called for a net that has been added with addNet().
   */
  void add(ShellNetOnDemand *net, OnDemandDeposits::NetMapEntry *entry, const CarbonUInt32 *val);

  //! Clears the current set of deposits
  void clear()
  {
    // Zero out our net/data pointers
    carbon_duffset(static_cast<UInt32*>(mBuf), &static_cast<UInt32*>(mBuf)[mBufSize/sizeof(UInt32)], static_cast<UInt32>(0));
  }

  //! Comparison function
  bool operator==(const OnDemandDeposits &other) const;

  //! Detailed comparison for debug mode
  ShellNetOnDemand *getDivergentNet(const OnDemandDeposits &other);

  //! Apply the saved set of deposits to a model
  void apply(CarbonModel *obj);

  CarbonUInt32 getNumNets() const { return mNumNets; }
  CarbonUInt32 getNumVals() const { return mNumNets; }

  // For ease of use, the net pointers and values are conceptually
  // stored in separate arrays.  However, for fast comparisons, we
  // store the two arrays in contiguous memory.  The storage for the
  // values of all the nets comes first, followed by space for the
  // pointers of all the nets.
  //
  // If a net has been deposited since the last schedule call, its
  // entry in the net array is set to its ShellNetOnDemand pointer,
  // and the deposited value is copied into the value array entry.  If
  // the net has not been deposited, both its net and value entries
  // are zero.
  //
  // The implementations of the following functions are ugly, but they
  // allow us to isolate the "two arrays as one" from the rest of the
  // class.

  //! Returns a pointer to the element at an index in the value array
  CarbonUInt32 *getValPtr(UInt32 index) const { return &(reinterpret_cast<CarbonUInt32*>(mBuf))[index]; }
  //! Returns a pointer to the element at an index in the net array
  ShellNetOnDemand **getNetPtr(UInt32 index) const { return &(reinterpret_cast<ShellNetOnDemand**>(getValPtr(mNumVals)))[index]; }
  //! Returns a reference to the element at an index in the value array
  CarbonUInt32 &getVal(UInt32 index) const { return *getValPtr(index); }
  //! Returns a reference to the element at an index in the net array
  ShellNetOnDemand *&getNet(UInt32 index) const { return *getNetPtr(index); }

protected:
  typedef UtHashMap<ShellNetOnDemand *, NetMapEntry *> NetMap;
  //! Map of net to storage locations
  NetMap mNetMap;

  CarbonUInt32 mNumNets;        //!< Number of nets we know about
  CarbonUInt32 mNumVals;        //!< Total number of UInt32s representing the values

  void *mBuf;                   //!< Pointer to the combined net/value buffer
  CarbonUInt32 mBufSize;        //!< Overall size (in bytes) of the combined net/value buffer

private:
  OnDemandDeposits();
  OnDemandDeposits(const OnDemandDeposits &other);
  ~OnDemandDeposits();

  //! Hide
  OnDemandDeposits &operator=(const OnDemandDeposits&);
};

//! Factory class for allocating objects and managing storage
class OnDemandDepositFactory
{
public:
  CARBONMEM_OVERRIDES

  OnDemandDepositFactory();
  ~OnDemandDepositFactory();

  //! Allocates a new object, owning storage
  OnDemandDeposits *create();

  //! Allocates a new copy of an object, owning storage
  OnDemandDeposits *copy(const OnDemandDeposits *other);

  //! Frees the last allocated object
  void freeLast();

  //! Frees all allocated objects
  void clear();

  //! Frees all allocated objects, including the protected one
  void clearAll();

protected:
  typedef UtArray<OnDemandDeposits *> ObjectArray;
  ObjectArray mAllocated;
  OnDemandDeposits *mProtected;
};

//! Helper class for our hash set, to do smart hashing and comparison
class OnDemandDepositSetHelper
{
public:
  CARBONMEM_OVERRIDES

  size_t hash(const OnDemandDeposits *var) const;
  bool equal(const OnDemandDeposits *v1, const OnDemandDeposits *v2) const;
};

//! Hash set to quickly find deposits
typedef UtHashSet<OnDemandDeposits*, OnDemandDepositSetHelper> OnDemandDepositSet;

#endif
