// -*-c++-*-
/******************************************************************************
 Copyright (c) 2008-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __carbon_interface_h_
#define __carbon_interface_h_

/*!
  \file 
  C interface between model and shell
*/

#ifndef __CARBON_SYS_INCLUDE_H__
#include "codegen/SysIncludes.h"
#endif
#include "carbon/carbon_shelltypes.h"
#include "util/CarbonTypes.h"

class CarbonModel;
class DynBitVector;
class UtIStream;
class UtOStream;
class UtIOStringStream;
class HdlIStream;
class HdlOStream;
class VerilogOutFileSystem;
class UtICheckpointStream;
class UtOCheckpointStream;
class ShellMemoryCBManager;
class CarbonReplayCModelData;
class HDLReadMemX;
class ShellNet;

//! Duplicate of typedef in CarbonModel
typedef void (*CarbonCModelPlaybackFn)(void*, UInt32*, UInt32*);
//! Duplicate of typedef in CarbonModel
typedef ShellNet* (*CarbonNetGenerator)(void*, bool);

// This is only compiled internally with a C++ compiler, so no ifdefs are required
extern "C" {

  //! Wrapper around ShellGlobal::newModel()
  CarbonModel* carbonInterfaceNewModel(const char* model_name, const char* dbFileRoot, CarbonDBType whichdb,
                                       const char* designVersion, carbon_model_descr* descr);
  
  // DynBitVector interface

  //! Allocate a DynBitVector
  DynBitVector* carbonInterfaceAllocDynBitVector(UInt32 size);
  //! Free a DynBitVector
  void carbonInterfaceFreeDynBitVector(DynBitVector* dynBV);
  //! Set a bit in a DynBitVector
  void carbonInterfaceSetDynBitVector(DynBitVector* dynBV, UInt32 bit);

  // CarbonValRW interface

  //! Wrapper around CarbonValRW::getWordMask()
  UInt32 carbonInterfaceGetWordMask(UInt32 numBits);
  //! Wrapper around CarbonValRW::getWordMaskLL()
  UInt64 carbonInterfaceGetWordMaskLL(UInt64 numBits);
  //! Wrapper around CarbonValRW::cpSrcToDestRange() (UInt32->UInt64)
  void carbonInterfaceCpSrc32ToDest64Range(UInt64* dest, const UInt32 *src, size_t index, size_t length);
  //! Wrapper around CarbonValRW::cpSrcToDestRange() (UInt32->UInt32)
  void carbonInterfaceCpSrc32ToDest32Range(UInt32* dest, const UInt32 *src, size_t index, size_t length);
  //! Wrapper around CarbonValRW::cpSrcToDestRange() (UInt32->UInt16)
  void carbonInterfaceCpSrc32ToDest16Range(UInt16* dest, const UInt32 *src, size_t index, size_t length);
  //! Wrapper around CarbonValRW::cpSrcToDestRange() (UInt32->UInt8)
  void carbonInterfaceCpSrc32ToDest8Range(UInt8* dest, const UInt32 *src, size_t index, size_t length);

  //! Wrapper around CarbonValRW::cpSrcToDestWord() (UInt64->UInt32)
  void carbonInterfaceCpSrc64ToDest32Word(UInt32* dest, const UInt64* src, size_t wordIndex);
  //! Wrapper around CarbonValRW::cpSrcToDestWord() (UInt32->UInt32)
  void carbonInterfaceCpSrc32ToDest32Word(UInt32* dest, const UInt32* src, size_t wordIndex);
  //! Wrapper around CarbonValRW::cpSrcToDestWord() (UInt16->UInt32)
  void carbonInterfaceCpSrc16ToDest32Word(UInt32* dest, const UInt16* src, size_t wordIndex);
  //! Wrapper around CarbonValRW::cpSrcToDestWord() (UInt8->UInt32)
  void carbonInterfaceCpSrc8ToDest32Word(UInt32* dest, const UInt8* src, size_t wordIndex);

  //! Wrapper around CarbonValRW::cpSrcWordToDest() (UInt32->UInt64)
  void carbonInterfaceCpSrc32WordToDest64(UInt64* dest, const UInt32 src, size_t wordIndex);
  //! Wrapper around CarbonValRW::cpSrcWordToDest() (UInt32->UInt32)
  void carbonInterfaceCpSrc32WordToDest32(UInt32* dest, const UInt32 src, size_t wordIndex);
  //! Wrapper around CarbonValRW::cpSrcWordToDest() (UInt32->UInt16)
  void carbonInterfaceCpSrc32WordToDest16(UInt16* dest, const UInt32 src, size_t wordIndex);
  //! Wrapper around CarbonValRW::cpSrcWordToDest() (UInt32->UInt8)
  void carbonInterfaceCpSrc32WordToDest8(UInt8* dest, const UInt32 src, size_t wordIndex);
  //! Wrapper around CarbonValRW::cpSrcWordToDest() (UInt64->UInt32)
  void carbonInterfaceCpSrc64WordToDest32(UInt32* dest, const UInt64 src, size_t wordIndex);
  //! Wrapper around CarbonValRW::cpSrcWordToDest() (UInt16->UInt32)
  void carbonInterfaceCpSrc16WordToDest32(UInt32* dest, const UInt16 src, size_t wordIndex);
  //! Wrapper around CarbonValRW::cpSrcWordToDest() (UInt8->UInt32)
  void carbonInterfaceCpSrc8WordToDest32(UInt32* dest, const UInt8 src, size_t wordIndex);

  //! Wrapper around CarbonValRW::cpSrcRangeToDestRange()
  void carbonInterfaceCpSrcRangeToDestRange(UInt32* dest, size_t dstIndex,
                                            const UInt32* src, size_t srcindex,
                                            size_t length);


  //! Wrapper around HdlVerilogString::convertToStrRep() (UInt32*)
  char* carbonInterfaceConvertPtrToStrRep(char* buffer, UInt32 bufferSize, UInt32 numCharsToConvert, const UInt32* source);
  //! Wrapper around HdlVerilogString::convertToStrRep() (UInt8)
  char* carbonInterfaceConvert8ToStrRep(char* buffer, UInt32 bufferSize, UInt8 source);
  //! Wrapper around HdlVerilogString::convertToStrRep() (UInt16)
  char* carbonInterfaceConvert16ToStrRep(char* buffer, UInt32 bufferSize, UInt16 source);
  //! Wrapper around HdlVerilogString::convertToStrRep() (UInt32)
  char* carbonInterfaceConvert32ToStrRep(char* buffer, UInt32 bufferSize, UInt32 source);
  //! Wrapper around HdlVerilogString::convertToStrRep() (UInt64)
  char* carbonInterfaceConvert64ToStrRep(char* buffer, UInt32 bufferSize, UInt64 source);

  //! Converts values into the 32 bit format used by the %u format specifier
  void carbonInterfaceConvertValueToRawURep(char* buffer, UInt32 bufferSize, const UInt32* source);

  //! Converts values into the 32 bit format used by the %z format specifier
  void carbonInterfaceConvertValueToRawZRep(char* buffer, UInt32 bufferSize, const UInt32* source, const UInt32* drive);

  // Control interfaces

  //! Encapsulation of ShellGlobal::gCarbonGetControlHelper() and ControlHelper::runControlSysTask()
  void carbonInterfaceRunControlSysTask(CarbonOpaque instance, UInt32 verbosity, CarbonControlType callbackType,
                                        const char* verilogFilename, int verilogLineNumber);


  // Random interfaces

  //! Wrapper around ShellGlobal::gCarbonGetRandomSeed()
  SInt32* carbonInterfaceGetRandomSeed(CarbonOpaque instance);
  //! Wrapper around HdlVerilogDist::ChiSquare()
  SInt32 carbonInterfaceVerilogDistChiSquare(SInt32 *seed, SInt32 df);
  //! Wrapper around HdlVerilogDist::Erlang()
  SInt32 carbonInterfaceVerilogDistErlang(SInt32 *seed, SInt32 k, SInt32 mean);
  //! Wrapper around HdlVerilogDist::Exponential()
  SInt32 carbonInterfaceVerilogDistExponential(SInt32 *seed, SInt32 mean);
  //! Wrapper around HdlVerilogDist::Normal()
  SInt32 carbonInterfaceVerilogDistNormal(SInt32 *seed, SInt32 mean, SInt32 sd);
  //! Wrapper around HdlVerilogDist::Poisson()
  SInt32 carbonInterfaceVerilogDistPoisson(SInt32 *seed, SInt32 mean);
  //! Wrapper around HdlVerilogDist::T()
  SInt32 carbonInterfaceVerilogDistT(SInt32 *seed, SInt32 df);
  //! Wrapper around HdlVerilogDist::Uniform()
  SInt32 carbonInterfaceVerilogDistUniform(SInt32 *seed, SInt32 start, SInt32 end);
  //! Wrapper around HdlVerilogDist::Random()
  SInt32 carbonInterfaceVerilogDistRandom(SInt32 *seed);

  // Memory callback interfaces

  //! Allocate a ShellMemoryCBManager
  ShellMemoryCBManager* carbonInterfaceAllocMemoryCBManager();
  //! Free a ShellMemoryCBManager
  void carbonInterfaceFreeMemoryCBManager(ShellMemoryCBManager* cb);
  //! Wrapper around ShellMemoryCBManager::reportAddress()
  void carbonInterfaceMemoryCBManagerReportAddress(ShellMemoryCBManager* cb, SInt32 address);
  
  // Cmodel data interfaces

  //! Allocate a CarbonReplayCModelData
  CarbonReplayCModelData* carbonInterfaceAllocReplayCModelData(carbon_model_descr* model, void* hndl, void* cmodel,
                                                                 int maxInputWords, int maxOutputWords);
  //! Free a CarbonReplayCModelData
  void carbonInterfaceFreeReplayCModelData(CarbonReplayCModelData* data);
  //! Wrapper around CarbonReplayCModelData::getUserData()
  void* carbonInterfaceGetReplayCModelUserData(CarbonReplayCModelData* data);
  //! Encapsulation of all CarbonReplayCModelData accessors
  void carbonInterfaceReplayCModelDataGetMembers(CarbonReplayCModelData* data, CarbonModel** modelPtr, void** userDataPtr,
                                                 void** cModelPtr, UInt32** inputsPtr, UInt32** outputsPtr);
  //! Wrapper around CarbonReplayCModelData::putModel()
  void carbonInterfacePutReplayCModelDataModel(CarbonReplayCModelData* data, carbon_model_descr* model);

  //! Wrapper around CarbonModel::preRecordCModel()
  void carbonInterfacePreRecordCModel(CarbonModel* model, void* cmodelData, UInt32 context, UInt32* outputs);
  //! Wrapper around CarbonModel::recordCModel()
  void carbonInterfaceRecordCModel(CarbonModel* model, void* cmodelData, UInt32 context, UInt32* inputs, UInt32* outputs);
  //! Wrapper around CarbonModel::getCModelValues()
  /*!
    Note that while CarbonModel::getCModelValues() returns an enum,
    this function returns a bool.  The return value is true if the
    cmodel output values should be read from the DB, and false if the
    cmodel should be run.
   */
  bool carbonInterfaceGetCModelValues(CarbonModel* model, void* cmodelData, UInt32 context, UInt32** outputs);

  //! Wrapper around CarbonReplayCModelData::getModel() and CarbonModel::onDemandDivergentCModel()
  bool carbonInterfaceOnDemandDivergentCModel(CarbonReplayCModelData* data);

  // HdlReadMemX interfaces

  //! Allocate a HdlReadMemX
  HDLReadMemX* carbonInterfaceAllocReadMem(const char* fileName, bool hexFormat, UInt32 bitWidth,
                                               SInt64 startAddress, SInt64 endAddress, bool checkComplete,
                                               CarbonOpaque instance);
  //! Free a HdlReadMemX
  void carbonInterfaceFreeReadMem(HDLReadMemX* readmem);
  //! Wrapper around HdlReadMemX::openFile()
  bool carbonInterfaceReadMemOpenFile(HDLReadMemX* readmem);
  //! Wrapper around HdlReadMemX::closeFile()
  void carbonInterfaceReadMemCloseFile(HDLReadMemX* readmem);
  //! Wrapper around HdlReadMemX::getNextWord()
  /*!
    \returns true when internal result is HDLReadMemX::eRMValidData
   */
  bool carbonInterfaceReadMemGetNextWord(HDLReadMemX* readmem, SInt64* address, UInt32* data);


  // Stream interfaces

  //! Wrapper around UtOStream::formatBignum()
  bool carbonInterfaceUtOStreamFormatBignum(UtOStream* str, const UInt32* data, UInt32 numBits);
  //! Wrapper around UtOStream::operator<<(const BVRef<>) (which calls formatBignum())
  bool carbonInterfaceUtOStreamFormatBignumRange(UtOStream* str, const UInt32* data, UInt32 size, UInt32 offset);

  //! Encapsulation of ShellGlobal::gCarbonGetVhdlInFileSystem() and HdlIStream::HdlIFileOpen()
  UInt32 carbonInterfaceOpenVHDLInputFileSystem(CarbonOpaque instance, const char* filename, const char* mode);
  //! Encapsulation of ShellGlobal::gCarbonGetVhdlOutFileSystem() and HdlOStream::HdlOFileOpen()
  UInt32 carbonInterfaceOpenVHDLOutputFileSystem(CarbonOpaque instance, const char* filename, const char* mode);
  //! Encapsulation of ShellGlobal::gCarbonGetVerilogFileSystem() and VerilogOutFileSystem::VerilogOFileOpen()
  UInt32 carbonInterfaceOpenVerilogOutputFileSystem(CarbonOpaque instance, const char* filename, const char* mode);
  
  //! Encapsulation of ShellGlobal::gCarbonGetVhdlInFileSystem() and HdlIStream::HdlIFileOpen() (with status)
  UInt32 carbonInterfaceOpenVHDLInputFileSystemStatus(CarbonOpaque instance, UInt2* status, const char* filename, const char* mode);
  //! Encapsulation of ShellGlobal::gCarbonGetVhdlOutFileSystem() and HdlOStream::HdlOFileOpen() (with status)
  UInt32 carbonInterfaceOpenVHDLOutputFileSystemStatus(CarbonOpaque instance, UInt2* status, const char* filename, const char* mode);
  
  //! Encapsulation of ShellGlobal::gCarbonGetVhdlInFileSystem() and HdlIStream::close()
  bool carbonInterfaceCloseVHDLInputFileSystem(CarbonOpaque instance, UInt32 fd);
  //! Encapsulation of ShellGlobal::gCarbonGetVhdlOutFileSystem() and HdlOStream::close()
  bool carbonInterfaceCloseVHDLOutputFileSystem(CarbonOpaque instance, UInt32 fd);
  //! Encapsulation of ShellGlobal::gCarbonGetVerilogFileSystem() and VerilogOutFileSystem::close()
  bool carbonInterfaceCloseVerilogOutputFileSystem(CarbonOpaque instance, UInt32 fd);
  
  //! Encapsulation of ShellGlobal::gCarbonGetVhdlOutFileSystem() and HdlOStream::putTargetFileDescriptor()
  /*!
    \returns The intermediate VHDL output file system
   */
  HdlOStream* carbonInterfacePutVHDLOutputFileDescriptor(CarbonOpaque instance, UInt32 fd);

  //! Encapsulation of ShellGlobal::gCarbonGetVhdlInFileSystem() and HdlIStream::putTargetFileDescriptor()
  /*!
    \returns The intermediate VHDL input file system
   */
  HdlIStream* carbonInterfacePutVHDLInputFileDescriptor(CarbonOpaque instance, UInt32 fd);

  //! Encapsulation of ShellGlobal::gCarbonGetVhdlInFileSystem() and HdlIStream::putTargetFileDescriptor()
  /*!
    \returns The new input stream
   */
  UtIStream* carbonInterfacePutNewVHDLInputFileDescriptor(CarbonOpaque instance, UInt32 fd);

  //! Encapsulation of ShellGlobal::gCarbonGetVerilogFileSystem() and VerilogOutFileSystem::putTargetFileDescriptor()
  /*!
    \returns The intermediate Verilog output file system
   */
  VerilogOutFileSystem* carbonInterfacePutVerilogOutputFileDescriptor(CarbonOpaque instance, UInt32 fd);

  //! Encapsulation of ShellGlobal::gCarbonGetVhdlInFileSystem() and HdlIStream::endOfFile()
  bool carbonInterfaceVHDLInputFileSystemEOF(CarbonOpaque instance, UInt32 fd);

  //! Encapsulation of ShellGlobal::gCarbonGetVerilogFileSystem() and VerilogOutFileSystem::flush()
  bool carbonInterfaceFlushVerilogOutputFileSystem(CarbonOpaque instance);

  //! Encapsulation of ShellGlobal::gCarbonGetVerilogFileSystem() and VerilogOutFileSystem::flush() (with file descriptor)
  bool carbonInterfaceFlushVerilogOutputFileSystemFD(CarbonOpaque instance, UInt32 fd);

  //! Allocates a UtIOStringStream
  UtIOStringStream* carbonInterfaceAllocStringStream();

  //! Wrapper around HdlIStream::readLine()
  void carbonInterfaceHdlIStreamReadLine(HdlIStream* hstr, UtIOStringStream* sstr);

  //! Wrapper around HdlOStream::writeLine()
  void carbonInterfaceHdlOStreamWriteLine(HdlOStream* hstr, UtIOStringStream* sstr);

  // Since the generated model doesn't include the class headers, it
  // doesn't know that some classes inherit from others.  These
  // functions do an explicit upcast to get around that.

  //! Cast UtIOStringStream to UtIStream
  UtIStream* carbonInterfaceCastIOStringStreamToIStream(UtIOStringStream* sstr);
  //! Cast UtIOStringStream to UtOStream
  UtOStream* carbonInterfaceCastIOStringStreamToOStream(UtIOStringStream* sstr);
  //! Cast VerilogOutFileSystem to UtOStream
  UtOStream* carbonInterfaceCastVerilogOutFileSystemToOStream(VerilogOutFileSystem* fs);
  //! Cast HdlOStream to UtOStream
  UtOStream* carbonInterfaceCastHdlOStreamToOStream(HdlOStream* str);


  //! Wrapper around UtOStream::operator<<(const char*)
  void carbonInterfaceWriteStringToOStream(UtOStream* ostr, const char* s);
  //! Wrapper around UtOStream::operator<<(UInt64)
  void carbonInterfaceWriteUInt64ToOStream(UtOStream* ostr, UInt64 num);
  //! Wrapper around UtOStream::operator<<(SInt64)
  void carbonInterfaceWriteSInt64ToOStream(UtOStream* ostr, SInt64 num);
  //! Wrapper around UtOStream::operator<<(UInt32)
  void carbonInterfaceWriteUInt32ToOStream(UtOStream* ostr, UInt32 num);
  //! Wrapper around UtOStream::operator<<(SInt32)
  void carbonInterfaceWriteSInt32ToOStream(UtOStream* ostr, SInt32 num);
  //! Wrapper around UtOStream::operator<<(UInt16)
  void carbonInterfaceWriteUInt16ToOStream(UtOStream* ostr, UInt16 num);
  //! Wrapper around UtOStream::operator<<(SInt16)
  void carbonInterfaceWriteSInt16ToOStream(UtOStream* ostr, SInt16 num);
  //! Wrapper around UtOStream::operator<<(double)
  void carbonInterfaceWriteDoubleToOStream(UtOStream* ostr, double num);
  //! Wrapper around UtOStream::operator<<(const char)
  void carbonInterfaceWriteCharToOStream(UtOStream* ostr, const char ch);

  //! Wrapper around UtOStream::write()
  void carbonInterfaceWriteBufToOStream(UtOStream* ostr, const char* buf, UInt32 size);

  //! Wrapper around UtIStream:operator>>(UInt64)
  void carbonInterfaceReadUInt64FromIStream(UtIStream* istr, UInt64& num);
  //! Wrapper around UtIStream:operator>>(SInt64)
  void carbonInterfaceReadSInt64FromIStream(UtIStream* istr, SInt64& num);
  //! Wrapper around UtIStream:operator>>(UInt32)
  void carbonInterfaceReadUInt32FromIStream(UtIStream* istr, UInt32& num);
  //! Wrapper around UtIStream:operator>>(SInt32)
  void carbonInterfaceReadSInt32FromIStream(UtIStream* istr, SInt32& num);
  //! Wrapper around UtIStream:operator>>(UInt16)
  void carbonInterfaceReadUInt16FromIStream(UtIStream* istr, UInt16& num);
  //! Wrapper around UtIStream:operator>>(SInt16)
  void carbonInterfaceReadSInt16FromIStream(UtIStream* istr, SInt16& num);
  //! Wrapper around UtIStream:operator>>(UInt8)
  void carbonInterfaceReadUInt8FromIStream(UtIStream* istr, UInt8& num);
  //! Wrapper around UtIStream:operator>>(double)
  void carbonInterfaceReadDoubleFromIStream(UtIStream* istr, double& num);
  //! Wrapper around UtIStream:operator>>(char)
  void carbonInterfaceReadCharFromIStream(UtIStream* istr, char& ch);
  //! Wrapper around UtIStream:operator>>(BVref<false>&)
  void carbonInterfaceReadBVRefFromIStream(UtIStream* istr, UInt32* data, UInt32 size, UInt32 offset);

  //! Set UtOStream radix (dec)
  void carbonInterfaceSetOStreamRadixDec(UtOStream* ostr);
  //! Set UtOStream radix (sdec)
  void carbonInterfaceSetOStreamRadixSDec(UtOStream* ostr);
  //! Set UtOStream radix (hex)
  void carbonInterfaceSetOStreamRadixHex(UtOStream* ostr);
  //! Set UtOStream radix (HEX)
  void carbonInterfaceSetOStreamRadixHEX(UtOStream* ostr);
  //! Set UtOStream radix (oct)
  void carbonInterfaceSetOStreamRadixOct(UtOStream* ostr);
  //! Set UtOStream radix (bin)
  void carbonInterfaceSetOStreamRadixBin(UtOStream* ostr);
  //! Set UtIStream radix (dec)
  void carbonInterfaceSetIStreamRadixDec(UtIStream* istr);
  //! Set UtIStream radix (sdec)
  void carbonInterfaceSetIStreamRadixSDec(UtIStream* istr);
  //! Set UtIStream radix (hex)
  void carbonInterfaceSetIStreamRadixHex(UtIStream* istr);
  //! Set UtIStream radix (HEX)
  void carbonInterfaceSetIStreamRadixHEX(UtIStream* istr);
  //! Set UtIStream radix (oct)
  void carbonInterfaceSetIStreamRadixOct(UtIStream* istr);
  //! Set UtIStream radix (bin)
  void carbonInterfaceSetIStreamRadixBin(UtIStream* istr);

  //! Set UtOStream max width
  void carbonInterfaceSetOStreamMaxWidth(UtOStream* ostr, UInt32 width);
  //! Set UtIStream max width
  void carbonInterfaceSetIStreamMaxWidth(UtIStream* istr, UInt32 width);

  //! Set UtOStream fill
  void carbonInterfaceSetOStreamFill(UtOStream* ostr, char fill);
  //! Set UtIStream fill
  void carbonInterfaceSetIStreamFill(UtIStream* istr, char fill);

  //! Set UtOStream width
  void carbonInterfaceSetOStreamWidth(UtOStream* ostr, UInt32 width);
  //! Set UtIStream width
  void carbonInterfaceSetIStreamWidth(UtIStream* istr, UInt32 width);

  //! Set UtOStream precision
  void carbonInterfaceSetOStreamPrecision(UtOStream* ostr, UInt32 precision);
  //! Set UtIStream precision
  void carbonInterfaceSetIStreamPrecision(UtIStream* istr, UInt32 precision);

  //! Set UtOStream float mode (exponent)
  void carbonInterfaceSetOStreamFloatModeExponent(UtOStream* ostr);
  //! Set UtOStream float mode (fixed)
  void carbonInterfaceSetOStreamFloatModeFixed(UtOStream* ostr);
  //! Set UtOStream float mode (general)
  void carbonInterfaceSetOStreamFloatModeGeneral(UtOStream* ostr);
  //! Set UtIStream float mode (exponent)
  void carbonInterfaceSetIStreamFloatModeExponent(UtIStream* istr);
  //! Set UtIStream float mode (fixed)
  void carbonInterfaceSetIStreamFloatModeFixed(UtIStream* istr);
  //! Set UtIStream float mode (general)
  void carbonInterfaceSetIStreamFloatModeGeneral(UtIStream* istr);

  //! Set UtOStream justification (right)
  void carbonInterfaceSetOStreamJustificationRight(UtOStream* ostr);
  //! Set UtOStream justification (left
  void carbonInterfaceSetOStreamJustificationLeft(UtOStream* ostr);
  //! Set UtOStream justification (internal)
  void carbonInterfaceSetOStreamJustificationInternal(UtOStream* ostr);
  //! Set UtIStream justification (right)
  void carbonInterfaceSetIStreamJustificationRight(UtIStream* istr);
  //! Set UtIStream justification (left)
  void carbonInterfaceSetIStreamJustificationLeft(UtIStream* istr);
  //! Set UtIStream justification (internal)
  void carbonInterfaceSetIStreamJustificationInternal(UtIStream* istr);

  //! Set UtOStream positive display (show)
  void carbonInterfaceSetOStreamPosDisplayShow(UtOStream* ostr);
  //! Set UtOStream positive display (no show)
  void carbonInterfaceSetOStreamPosDisplayNoShow(UtOStream* ostr);

  //! Send endline to UtOStream
  void carbonInterfaceOStreamEndl(UtOStream* ostr);

  //! Wrapper around UtIStream::fail()
  bool carbonInterfaceIStreamFail(UtIStream* istr);

  //! Get UtIO::cout()
  UtOStream* carbonInterfaceGetCout();

  //! Get UtIO::cerr()
  UtOStream* carbonInterfaceGetCerr();


  // Checkpoint stream interfaces

  //! Wrapper around UtICheckpointStream::read()
  UInt32 carbonInterfaceICheckpointStreamRead(UtICheckpointStream* str, void* buffer, UInt32 numBytes);
  //! Wrapper around UtOCheckpointStream::write()
  UInt32 carbonInterfaceOCheckpointStreamWrite(UtOCheckpointStream* str, const void* buffer, UInt32 numBytes);

  //! Wrapper around UtICheckpointStream::failed
  UInt32 carbonInterfaceICheckPointStreamFailed (UtICheckpointStream *);

  //! Wrapper around UtOCheckpointStream::failed
  UInt32 carbonInterfaceOCheckPointStreamFailed (UtOCheckpointStream *);

  // These are only used for ordinary save/restore, not OnDemand, so they're in carbon_interface.cxx

  //! Wrapper around UtIOStringStream::save()
  void carbonInterfaceSaveIOStringStream(UtOCheckpointStream* str, UtIOStringStream** pp);
  //! Wrapper around UtIOStringStream::restore()
  void carbonInterfaceRestoreIOStringStream(UtICheckpointStream* str, UtIOStringStream** pp);

  // Block profiling interface

  //! Wrapper around ShellGlobal::gGetCurrentModel()
  struct carbon_model_descr* carbonInterfaceGetCurrentModel();

  // Misc. ShellGlobal interfaces

  //! Wrapper around ShellGlobal::gCarbonAddMessageContext()
  void carbonInterfaceAddMessageContext(carbon_model_descr* descr);
  //! Wrapper around ShellGlobal::reportSHLProfileGenerate()
  void carbonInterfaceReportProfileGenerate(bool val);
  //! Wrapper around ShellGlobal::setCurrentModel
  void carbonInterfaceSetCurrentModel(carbon_model_descr* descr);
}

#endif
