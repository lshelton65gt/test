/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _CARBON_TYPE_CREATE_H_
#define _CARBON_TYPE_CREATE_H_

#include "util/CarbonTypes.h"

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
#define STRUCT struct
#endif

  /* Memory read function for memory rows */
  typedef void (*CarbonMemReadRowFn)(SInt32 address, UInt32* data, void* mem);
  /* Memory write function for memory rows */
  typedef void (*CarbonMemWriteRowFn)(SInt32 address, const UInt32* data, void* mem);

  /* Memory read function for memory row word */
  typedef void (*CarbonMemReadRowWordFn)(SInt32 address, UInt32* data, int index, void* mem);

  /* Memory write function for memory row word */
  typedef void (*CarbonMemWriteRowWordFn)(SInt32 address, const UInt32* data, int index, void* mem);

  /* Memory read function for memory row range */
  typedef void (*CarbonMemReadRowRangeFn)(SInt32 address, UInt32* data, int index, int length, void* mem);
  /* Memory write function for memory row range */
  typedef void (*CarbonMemWriteRowRangeFn)(SInt32 address, const UInt32* data, int index, int length, void* mem);
  
  /* Memory raw row storage pointer function */
  typedef void* (*CarbonMemRowStoragePtrFn)(SInt32 address, void* mem);
  
  /* Memory create closure (really is a struct, so no need for the
     macro) */
  typedef struct ShellMemoryCreateInfo ShellMemoryCreateInfoID;

  /* Non-input, non-tristate types */
  STRUCT ShellNet* CarbonScalarCreate(UInt8* addr);
  STRUCT ShellNet* CarbonVector1Create(UInt8* addr);
  STRUCT ShellNet* CarbonVector2Create(UInt16* addr);
  STRUCT ShellNet* CarbonVector4Create(UInt32* addr);
  STRUCT ShellNet* CarbonVector8Create(UInt64* addr);
  STRUCT ShellNet* CarbonVectorACreate(UInt32* addr);
  STRUCT ShellNet* CarbonVectorASCreate(UInt32* addr);

  /* input, non-tristate types */
  STRUCT ShellNet* CarbonScalarInputCreate(UInt8* addr);
  STRUCT ShellNet* CarbonVector1InputCreate(UInt8* addr);
  STRUCT ShellNet* CarbonVector2InputCreate(UInt16* addr);
  STRUCT ShellNet* CarbonVector4InputCreate(UInt32* addr);
  STRUCT ShellNet* CarbonVector8InputCreate(UInt64* addr);
  STRUCT ShellNet* CarbonVectorAInputCreate(UInt32* addr);
  STRUCT ShellNet* CarbonVectorASInputCreate(UInt32* addr);

  /* non-input, tristate types */
  STRUCT ShellNet* CarbonTristateScalarCreate(UInt8* idata, UInt8* idrive);
  STRUCT ShellNet* CarbonTristateVector1Create(UInt8* idata, UInt8* idrive);
  STRUCT ShellNet* CarbonTristateVector2Create(UInt16* idata, UInt16* idrive);
  STRUCT ShellNet* CarbonTristateVector4Create(UInt32* idata, UInt32* idrive);
  STRUCT ShellNet* CarbonTristateVector8Create(UInt64* idata, UInt64* idrive);
  STRUCT ShellNet* CarbonTristateVectorACreate(UInt32* idata, UInt32* idrive);

  /* non-input, tristate types - signed */
  STRUCT ShellNet* CarbonTristateScalarSCreate(SInt8* idata, SInt8* idrive);
  STRUCT ShellNet* CarbonTristateVector1SCreate(SInt8* idata, SInt8* idrive);
  STRUCT ShellNet* CarbonTristateVector2SCreate(SInt16* idata, SInt16* idrive);
  STRUCT ShellNet* CarbonTristateVector4SCreate(SInt32* idata, SInt32* idrive);
  STRUCT ShellNet* CarbonTristateVector8SCreate(SInt64* idata, SInt64* idrive);
  // Long signed vectors have an unsigned underlying container for the data
  STRUCT ShellNet* CarbonTristateVectorASCreate(UInt32* idata, UInt32* idrive);

  /* non-input, structure api, bidi types */
  STRUCT ShellNet* CarbonTristateScalarBidiCreate(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive);
  STRUCT ShellNet* CarbonTristateVector1BidiCreate(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive);
  STRUCT ShellNet* CarbonTristateVector2BidiCreate(UInt16* idata, UInt16* idrive, UInt16* xdata, UInt16* xdrive);
  STRUCT ShellNet* CarbonTristateVector4BidiCreate(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive);
  STRUCT ShellNet* CarbonTristateVector8BidiCreate(UInt64* idata, UInt64* idrive, UInt64* xdata, UInt64* xdrive);
  STRUCT ShellNet* CarbonTristateVectorABidiCreate(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive);

  /* non-input, structure api, bidi types - signed */
  STRUCT ShellNet* CarbonTristateScalarBidiSCreate(SInt8* idata, SInt8* idrive, SInt8* xdata, SInt8* xdrive);
  STRUCT ShellNet* CarbonTristateVector1BidiSCreate(SInt8* idata, SInt8* idrive, SInt8* xdata, SInt8* xdrive);
  STRUCT ShellNet* CarbonTristateVector2BidiSCreate(SInt16* idata, SInt16* idrive, SInt16* xdata, SInt16* xdrive);
  STRUCT ShellNet* CarbonTristateVector4BidiSCreate(SInt32* idata, SInt32* idrive, SInt32* xdata, SInt32* xdrive);
  STRUCT ShellNet* CarbonTristateVector8BidiSCreate(SInt64* idata, SInt64* idrive, SInt64* xdata, SInt64* xdrive);
  // Long signed vectors are created differently in the type
  // dictionary, hence the name difference
  STRUCT ShellNet* CarbonTristateVectorASBidiCreate(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive);

  /* depositable non-bidi tristates  */
  STRUCT ShellNet* CarbonTristateScalarDepInputCreate(UInt8* idata, UInt8* idrive);
  STRUCT ShellNet* CarbonTristateVector1DepInputCreate(UInt8* idata, UInt8* idrive);
  STRUCT ShellNet* CarbonTristateVector2DepInputCreate(UInt16* idata, UInt16* idrive);
  STRUCT ShellNet* CarbonTristateVector4DepInputCreate(UInt32* idata, UInt32* idrive);
  STRUCT ShellNet* CarbonTristateVector8DepInputCreate(UInt64* idata, UInt64* idrive);
  STRUCT ShellNet* CarbonTristateVectorADepInputCreate(UInt32* idata, UInt32* idrive);

  /* depositable non-bidi tristates - signed*/
  STRUCT ShellNet* CarbonTristateScalarDepInputSCreate(SInt8* idata, SInt8* idrive);
  STRUCT ShellNet* CarbonTristateVector1DepInputSCreate(SInt8* idata, SInt8* idrive);
  STRUCT ShellNet* CarbonTristateVector2DepInputSCreate(SInt16* idata, SInt16* idrive);
  STRUCT ShellNet* CarbonTristateVector4DepInputSCreate(SInt32* idata, SInt32* idrive);
  STRUCT ShellNet* CarbonTristateVector8DepInputSCreate(SInt64* idata, SInt64* idrive);
  // Long signed vectors are created differently in the type
  // dictionary, hence the name difference
  STRUCT ShellNet* CarbonTristateVectorASDepInputCreate(UInt32* idata, UInt32* idrive);

  /* input, bidi types */
  STRUCT ShellNet* CarbonTristateScalarInputCreate(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive);
  STRUCT ShellNet* CarbonTristateVector1InputCreate(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive);
  STRUCT ShellNet* CarbonTristateVector2InputCreate(UInt16* idata, UInt16* idrive, UInt16* xdata, UInt16* xdrive);
  STRUCT ShellNet* CarbonTristateVector4InputCreate(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive);
  STRUCT ShellNet* CarbonTristateVector8InputCreate(UInt64* idata, UInt64* idrive, UInt64* xdata, UInt64* xdrive);
  STRUCT ShellNet* CarbonTristateVectorAInputCreate(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive);

  /* input, bidi types - signed */
  STRUCT ShellNet* CarbonTristateScalarInputSCreate(SInt8* idata, SInt8* idrive, SInt8* xdata, SInt8* xdrive);
  STRUCT ShellNet* CarbonTristateVector1InputSCreate(SInt8* idata, SInt8* idrive, SInt8* xdata, SInt8* xdrive);
  STRUCT ShellNet* CarbonTristateVector2InputSCreate(SInt16* idata, SInt16* idrive, SInt16* xdata, SInt16* xdrive);
  STRUCT ShellNet* CarbonTristateVector4InputSCreate(SInt32* idata, SInt32* idrive, SInt32* xdata, SInt32* xdrive);
  STRUCT ShellNet* CarbonTristateVector8InputSCreate(SInt64* idata, SInt64* idrive, SInt64* xdata, SInt64* xdrive);
  // Long signed vectors are created differently in the type
  // dictionary, hence the name difference
  STRUCT ShellNet* CarbonTristateVectorASInputCreate(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive);

  /* force net type */
  STRUCT ShellNet* CarbonForceNetCreate(STRUCT ShellNet* storage, STRUCT ShellNet* forceMask);

  /* Non-sparse Memory types */
  STRUCT ShellNet* CarbonShellMemory64x8Create2(ShellMemoryCreateInfoID* memClosure, UInt8* mem);
  STRUCT ShellNet* CarbonShellMemory64x16Create2(ShellMemoryCreateInfoID* memClosure, UInt16* mem);
  STRUCT ShellNet* CarbonShellMemory64x32Create2(ShellMemoryCreateInfoID* memClosure, UInt32* mem);
  STRUCT ShellNet* CarbonShellMemory64x64Create2(ShellMemoryCreateInfoID* memClosure, UInt64* mem);
  STRUCT ShellNet* CarbonShellMemory64xACreate2(ShellMemoryCreateInfoID* memClosure, void* mem);

  /* Sparse Memory types */
  STRUCT ShellNet* CarbonShellSparseMemory32x8Create2(ShellMemoryCreateInfoID* memClosure, void* mem);  
  STRUCT ShellNet* CarbonShellSparseMemory32x16Create2(ShellMemoryCreateInfoID* memClosure, void* mem);
  STRUCT ShellNet* CarbonShellSparseMemory32x32Create2(ShellMemoryCreateInfoID* memClosure, void* mem);
  STRUCT ShellNet* CarbonShellSparseMemory32x64Create2(ShellMemoryCreateInfoID* memClosure, void* mem);


  /* Old memory functions for backward compatibility */
  STRUCT ShellNet* CarbonShellMemory64x8Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, UInt8* mem);
  STRUCT ShellNet* CarbonShellMemory64x16Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, UInt16* mem);
  STRUCT ShellNet* CarbonShellMemory64x32Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, UInt32* mem);
  STRUCT ShellNet* CarbonShellMemory64x64Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, UInt64* mem);
  STRUCT ShellNet* CarbonShellMemory64xACreate(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, 
                                               CarbonMemReadRowFn readRow, CarbonMemWriteRowFn writeRow,
                                               CarbonMemReadRowWordFn readRowWord, CarbonMemWriteRowWordFn writeRowWord,
                                               CarbonMemReadRowRangeFn readRowRange, CarbonMemWriteRowRangeFn writeRowRange,
                                               void* mem);

  /* Sparse Memory types */
  STRUCT ShellNet* CarbonShellSparseMemory32x8Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, void* mem);  
  STRUCT ShellNet* CarbonShellSparseMemory32x16Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, void* mem);
  STRUCT ShellNet* CarbonShellSparseMemory32x32Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, void* mem);
  STRUCT ShellNet* CarbonShellSparseMemory32x64Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, void* mem);
  
#ifdef __cplusplus
} /* extern "C" */
#endif /* !cplusplus */

#undef STRUCT
  
#endif
