// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONTRISTATESCALARINPUT_H_
#define __CARBONTRISTATESCALARINPUT_H_


#ifndef __CARBONTRISTATESCALAR_H_
#include "shell/CarbonTristateScalar.h"
#endif

//! normal tristate scalar input
class CarbonTristateScalarInput : public CarbonTristateScalar
{
public: CARBONMEM_OVERRIDES
  //! constructor
  CarbonTristateScalarInput(UInt8* data, UInt8* drive);
  CarbonTristateScalarInput(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive);

  //! virtual destructor
  virtual ~CarbonTristateScalarInput();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! ShellNet::isInput()
  virtual bool isInput() const;

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);
 
  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::deposit()
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);
  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);

  virtual void recomputeChangeMask();  
private:
  inline void doDepositInput(const UInt32* buf, const UInt32* drive, 
                             CarbonModel* model);

  CarbonChangeType* mChangeArrayRef;
 
  inline void calcChangeMask();

};

#endif
