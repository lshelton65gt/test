// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __SHELLMEMORYCREATEINFO_H_
#define __SHELLMEMORYCREATEINFO_H_

#ifndef __SHELLGLOBAL_H_
#include "shell/ShellGlobal.h"
#endif
#ifndef _CARBON_TYPE_CREATE_H_
#include "shell/carbon_type_create.h"
#endif

//! Type for array of memory callbacks.
/*!
  Used by Memory.h and SparseMemory.h as well as the shell.
*/
typedef UtArray<ShellGlobal::MemoryCallback*> MemoryCallbackVec;

//! Enum for versioning the MemoryCreateInfo structure
enum ShellMemoryCreateInfoVersion {
  eShellMemoryCreateInfoVersion1 = 1,
  eShellMemoryCreateInfoVersion2 = 2,
  eShellMemoryCreateInfoLatest = eShellMemoryCreateInfoVersion2
};

class ShellMemoryCBManager;

//! Structure used by the gen_ functions
/*!
  This passes information about the memory as well as needed
  pointers utilized by the CarbonMemory subclasses.
*/
struct ShellMemoryCreateInfo
{
  CARBONMEM_OVERRIDES
  
  ShellMemoryCreateInfo() :
    mVersion(eShellMemoryCreateInfoLatest),
    mMemReadRowFn(NULL),
    mMemWriteRowFn(NULL),
    mMemReadRowWordFn(NULL),
    mMemWriteRowWordFn(NULL),
    mMemReadRowRangeFn(NULL),
    mMemWriteRowRangeFn(NULL),
    mMemCBs(NULL),
    mMsb(-1),
    mLsb(-1),
    mLeftAddr(-1),
    mRightAddr(-1),
    mMemRowStoragePtrFn(NULL)
  {}
  
  void putReadRowFn(CarbonMemReadRowFn fn)
  {
    mMemReadRowFn = fn;
  }
  
  void putWriteRowFn(CarbonMemWriteRowFn fn)
  {
    mMemWriteRowFn = fn;
  }
  
  void putReadRowWordFn(CarbonMemReadRowWordFn fn)
  {
    mMemReadRowWordFn = fn;
  }
  
  void putWriteRowWordFn(CarbonMemWriteRowWordFn fn)
  {
    mMemWriteRowWordFn = fn;
  }
  
  void putReadRowRangeFn(CarbonMemReadRowRangeFn fn)
  {
    mMemReadRowRangeFn = fn;
  }
  
  void putWriteRowRangeFn(CarbonMemWriteRowRangeFn fn)
  {
    mMemWriteRowRangeFn = fn;
  }
  
  void putRowStoragePtrFn(CarbonMemRowStoragePtrFn fn)
  {
    mMemRowStoragePtrFn = fn;
  }
  
  void putMemCBArray(ShellMemoryCBManager* arr)
  {
    mMemCBs = arr;
  }
  
  void putMsb(SInt32 msb)
  {
    mMsb = msb;
  }
  
  void putLsb(SInt32 lsb)
  {
    mLsb = lsb;
  }
  
  void putLeftAddr(SInt64 addr)
  {
    mLeftAddr = addr;
  }
  
  void putRightAddr(SInt64 addr)
  {
    mRightAddr = addr;
  }

  SInt32 getMsb() const { return mMsb; }
  SInt32 getLsb() const { return mLsb; }
  SInt64 getLeftAddr() const { return mLeftAddr; }
  SInt64 getRightAddr() const { return mRightAddr; }
  CarbonMemReadRowFn getReadRowFn() const { return mMemReadRowFn; }
  CarbonMemWriteRowFn getWriteRowFn() const { return mMemWriteRowFn; }
  CarbonMemReadRowWordFn getReadRowWordFn() const { return mMemReadRowWordFn; }
  CarbonMemWriteRowWordFn getWriteRowWordFn() const { return mMemWriteRowWordFn; }
  CarbonMemReadRowRangeFn getReadRowRangeFn() const { return mMemReadRowRangeFn; }
  CarbonMemWriteRowRangeFn getWriteRowRangeFn() const { return mMemWriteRowRangeFn; }
  CarbonMemRowStoragePtrFn getRowStoragePtrFn() const { return mMemRowStoragePtrFn; }

  ShellMemoryCBManager* getCallbackArray() { return mMemCBs; }

  ShellMemoryCreateInfoVersion getVersion() const { return mVersion; }

  //! The version of this structure
  ShellMemoryCreateInfoVersion  mVersion;

  //! Read row function for large rows
  CarbonMemReadRowFn mMemReadRowFn;
  //! Write row function for large rows
  CarbonMemWriteRowFn mMemWriteRowFn;
  //! Read row word function for large rows
  CarbonMemReadRowWordFn mMemReadRowWordFn;
  //! Write row word function for large rows
  CarbonMemWriteRowWordFn mMemWriteRowWordFn;
  //! Read row range function for large rowsw
  CarbonMemReadRowRangeFn mMemReadRowRangeFn;
  //! Write row range function for large rows
  CarbonMemWriteRowRangeFn mMemWriteRowRangeFn;
  
  //! The memory callback array
  ShellMemoryCBManager* mMemCBs;
  
  //! The row MSB
  SInt32 mMsb;
  //! The row LSB
  SInt32 mLsb;
  //! The left part of the address range [l:r]
  SInt64 mLeftAddr;
  //! The right part of the address range [l:r]
  SInt64 mRightAddr;

  // New members added in version 2.  These must come at the end of
  // the class, because older models have already been built with the
  // older version of this header, and so when it calls the inline put
  // methods above, the original member offsets will be used.
  //
  // Ugly, yes, but unfortunately, it's too late to make them
  // non-inlined, so this is the best we can do to allow additions to
  // this class.

  //! Row storage access function for large rows
  CarbonMemRowStoragePtrFn mMemRowStoragePtrFn;

};

//! Class that manages internal memory callbacks
class ShellMemoryCBManager
{
  // default function when no callbacks are added
  static void sDoNothing(MemoryCallbackVec&, SInt32)
  {}

  // Called when there is at least one callback
  static void sExecuteCBs(MemoryCallbackVec& vec, SInt32 address)
  {
    for (MemoryCallbackVec::iterator p = vec.begin(),
           e = vec.end(); p != e; ++p)
    {
      ShellGlobal::MemoryCallback* memCB = *p;
      memCB->reportAddress(address);
    }
  }

public:
  CARBONMEM_OVERRIDES

  //! constructor
  ShellMemoryCBManager() : mReportFn(sDoNothing), mNeedsOnDemandCB(false) {}

  //! Push the callback into the vector and setup the report fn
  ShellGlobal::MemoryCallback* allocCB()
  {
    ShellGlobal::MemoryCallback* cb = new ShellGlobal::MemoryCallback(mCallbacks.size());
    mCallbacks.push_back(cb);
    mReportFn = sExecuteCBs;
    return cb;
  }

  void remove(ShellGlobal::MemoryCallback* doomed)
  {
    UInt32 numElems = mCallbacks.size();
    UInt32 index = doomed->mWriteCBIndex;
    if (index < numElems - 1)
    {
      INFO_ASSERT(mCallbacks[index] == doomed, "Memory callback object consistency check failed.");
      // replace the element at the index with the last thing in the
      // array.
      ShellGlobal::MemoryCallback* replacer = mCallbacks.back();
      replacer->mWriteCBIndex = index;
      mCallbacks[index] = replacer;
    }
    else
    {
      // Index better be the last element
      INFO_ASSERT(index == numElems - 1, "Index out-of-range");
      INFO_ASSERT(mCallbacks[index] == doomed, "Memory callback object consistency check failed.");
    }

    delete doomed;
    // Remove the last element.
    mCallbacks.pop_back();
  }

  //! Call the callbacks or do nothing
  void reportAddress(SInt32 address)
  {
    (*mReportFn)(mCallbacks, address);
  }

  void putNeedsOnDemandCB() { mNeedsOnDemandCB = true;}

  bool needsOnDemandCB() { return mNeedsOnDemandCB; }

private:
  MemoryCallbackVec mCallbacks;  

  typedef void (*ReportFn)(MemoryCallbackVec&,SInt32);
  ReportFn mReportFn;
  bool mNeedsOnDemandCB;        // Does the managed memory need a write callback for onDemand?
};

#endif
