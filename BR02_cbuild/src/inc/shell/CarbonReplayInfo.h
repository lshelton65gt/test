// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONREPLAYINFO_H_
#define __CARBONREPLAYINFO_H_

#include "util/UtString.h"
#include "util/UtArray.h"

class CarbonModel;

//! Class to manage Replay user callbacks
/*!
  This base class provides the user interface to Replay callbacks,
  allowing them to be enabled/disabled.  The real
  content/implementation of the specific callback type must be
  implemented in a derived version.  The base class exists so that the
  user doesn't have to deal with a different handle type for each
  unique callback type.
 */
class CarbonReplayCBData
{
public:
  CARBONMEM_OVERRIDES

  CarbonReplayCBData() { enable(); }

  virtual ~CarbonReplayCBData() {}

  //! Enable the callback
  void enable() { mEnabled = true; }

  //! Disable the callback
  void disable() { mEnabled = false; }

  //! Is the callback enabled?
  bool isEnabled() { return mEnabled; }

private:
  bool mEnabled;
};

//! Class to represent a mode change callback function
class ReplayModeChangeCB : public CarbonReplayCBData
{
public:
  CARBONMEM_OVERRIDES

  ReplayModeChangeCB(CarbonModeChangeCBFunc fn, void *userData);
  virtual ~ReplayModeChangeCB();

  //! Runs the callback function, if enabled, with the given mode change data
  void run(CarbonObjectID* context,
           CarbonVHMMode from,
           CarbonVHMMode to);

protected:
  CarbonModeChangeCBFunc mCBFunc;               //!< Mode change callback function pointer
  void *mCBData;                                //!< Mode change callback data
};

//! Class to represent a checkpoint callback function
class ReplayCheckpointCB : public CarbonReplayCBData
{
public:
  CARBONMEM_OVERRIDES

  ReplayCheckpointCB(CarbonCheckpointCBFunc fn, void *userData);
  virtual ~ReplayCheckpointCB();

  //! Runs the callback function, if enabled, with the given checkpoint data
  void run(CarbonObjectID* context,
           CarbonUInt64 totalScheduleCalls,
           CarbonUInt32 checkpointNumber);

protected:
  CarbonCheckpointCBFunc mCBFunc;               //!< Checkpoint callback function pointer
  void *mCBData;                                //!< Checkpoint callback data
};

//! Object for replay information needed by replay apis
class CarbonReplayInfo
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonReplayInfo(CarbonModel* model);

  //! Destructor
  ~CarbonReplayInfo();

  //! Return the carbon model associated with this ReplayInfo 
  CarbonModel* getCarbonModel() {
    return mCarbonModel;
  }

  //! Put the database name
  /*!
    This verifies that the systemName and instanceName are not NULL.
  */
  CarbonStatus putDB(const char* systemName, const char* instanceName);

  //! Put the directory action
  /*!
    Returns an error if the action is invalid.
  */
  CarbonStatus putDirAction(CarbonDirAction action);

  //! Set the save frequency
  CarbonStatus putSaveFrequency(UInt32 recoverPercent, CarbonUInt64 minNumCalls);

  //! Returns true if the save frequency was set
  bool isSaveFrequencyInit() const { return mIsSaveFreqInit; }

  //! Returns the minimum save frequency
  CarbonUInt64 getMinSaveFrequency() const { return mMinSaveFrequency; }

  //! Returns the recover percentage
  UInt32 getRecoverPercentage() const { return mRecoverPercent; }
  
  //! Add a mode change function and user data
  /*!
    \param fn Function to call upon a mode change
    \param userData Data to be passed to the mode change function
    \retval Handle for the callback object
  */
  CarbonReplayCBDataID* addModeChangeCB(CarbonModeChangeCBFunc fn, void* userData);
  
  //! Add a checkpoint callback
  /*!
    This gets called whenever a checkpoint is taken during record or
    is encountered in the database during playback.

    \param fn Function to call at checkpoint time
    \param userData Data to be passed to the checkpoint function
    \retval Handle for the callback object
  */
  CarbonReplayCBDataID* addCheckpointCB(CarbonCheckpointCBFunc fn, void* userData);

  //! Possibly disable all checkpoint callbacks
  /*!
    This is required during divergence recovery.
   */
  void setAllCheckpointCBsDisabled(bool disabled) { mDisableAllCheckpointCBs = disabled; }

  //! Set the work area
  /*!
    If the directory is NULL an error is returned.
  */
  CarbonStatus putWorkArea(const char* workArea);

  //! Was the directory action set?
  bool isDirActionInit() const { return mIsDirActionInit; }

  //! Was the work area set?
  bool isWorkAreaInit() const { return ! mWorkArea.empty(); }
  
  //! Was the system directory set?
  bool isSystemDirInit() const { return ! mSystemDir.empty(); }

  //! Was the instance directory set?
  bool isInstanceDirInit() const { return ! mInstanceDir.empty(); }

  //! Assemble the system and instance names into a directory name
  bool assembleDBName(UtString* buf);

  //! Get the work area
  const char* getWorkArea() const { return mWorkArea.c_str(); }

  //! Get the system directory
  const char* getSystemDir() const { return mSystemDir.c_str(); }

  //! Get the directory action
  CarbonDirAction getDirAction() const { return mDirAction; }

  //! call the mode change functions.
  void callModeChangeCBs(CarbonVHMMode prev, CarbonVHMMode next);

  //! Call the checkpoint cb functions
  void callCheckpointCBs(CarbonUInt64 numSchedCalls, CarbonUInt32 checkPointNum);

private:
  CarbonModel* mCarbonModel;
  typedef UtArray<ReplayModeChangeCB*> ModeChangeCBArray;
  ModeChangeCBArray mModeChangeCBs;
  typedef UtArray<ReplayCheckpointCB*> CheckpointCBArray;
  CheckpointCBArray mCheckpointCBs;
  bool mDisableAllCheckpointCBs;
  CarbonDirAction mDirAction;
  CarbonUInt64 mMinSaveFrequency;
  UInt32 mRecoverPercent;
  UtString mWorkArea;
  UtString mSystemDir;
  UtString mInstanceDir;
  bool mIsDirActionInit;
  bool mIsSaveFreqInit;
  CarbonStatus checkDir(const char* dirName);
};

#endif
