// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CARBONREPLAYDECL_H_
#define CARBONREPLAYDECL_H_

/*!
  \file
  This is used to forward declare replay objects needed in header
  files.
*/

namespace CarbonReplay
{
  struct ReplayCheckpointClosure;

  class ReplayCModel;
  typedef UtHashMap<void*, ReplayCModel*> ReplayCModelMap;
}

#endif
