// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef __CARBON_MEM_IDENT_H__
#define __CARBON_MEM_IDENT_H__

#include "exprsynth/Expr.h"

class ShellNet;
class CarbonModel;
class CarbonMemory;

class CarbonMemIdent : public CarbonIdent
{
public:
  CARBONMEM_OVERRIDES

  CarbonMemIdent(ShellNet* net, CarbonModel* model, bool isSigned, UInt32 size);
  virtual ~CarbonMemIdent();

  virtual ptrdiff_t compare(const CarbonExpr* other) const;

  virtual const STAliasedLeafNode* getNode(DynBitVector* usageMask) const;

  virtual AssignStat assign(ExprAssignContext* context);

  virtual AssignStat assignRange(ExprAssignContext* context, 
                                 const ConstantRange& range);

  virtual void print(bool = false, int indent = 0) const;

  virtual SignT evaluate(ExprEvalContext* evalContext) const;
  
  virtual const char* typeStr() const;

  virtual void composeIdent(ComposeContext* context) const;

  const CarbonMemIdent* castCarbonMemIdent() const;

  //! Returns true if this ident represents the entire net
  virtual bool isWholeIdentifier() const;

  virtual SignT evaluateRange(ExprEvalContext* context, 
                              const ConstantRange& range);

  ShellNet* getShellNet() {
    const CarbonMemIdent* me = const_cast<const CarbonMemIdent*>(this);
    return const_cast<ShellNet*>(me->getShellNet());
  }

  const ShellNet* getShellNet() const;

  //! Replace the shellnet
  /*!
    This is needed for replay net instrumentation.
  */
  void putShellNet(ShellNet* replacement);

protected:
  ShellNet *mShellNet;
  CarbonModel *mCarbonModel;
  CarbonMemory *mMemoryNet;
};

#endif
