// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONVECTOR_H_
#define __CARBONVECTOR_H_


#ifndef __CARBONVECTORBASE_H_
#include "shell/CarbonVectorBase.h"
#endif

class CarbonVector : public CarbonVectorBase
{
public: CARBONMEM_OVERRIDES
  CarbonVector();
  virtual ~CarbonVector();

protected:
  virtual void setConstantBits() = 0;

private:
  CarbonVector(const CarbonVector&);
  CarbonVector& operator=(const CarbonVector&);
};


class CarbonVector1 : public CarbonVector
{
public: CARBONMEM_OVERRIDES
  CarbonVector1(UInt8* vec);
  CarbonVector1(SInt8* vec);

  virtual ~CarbonVector1();

  virtual void getTraits(Traits* traits) const;

  ShellNet::Storage allocShadow() const;
  
  void freeShadow(ShellNet::Storage* shadow);

  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);

  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);
  
  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  
  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;

  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  virtual void update(ShellNet::Storage* shadow) const;
  
  virtual ShellNet::ValueState compare(const ShellNet::Storage shadow) const;

  virtual ShellNet::ValueState writeIfNotEq(char* valueStr, size_t len, ShellNet::Storage* shadow,
                                            NetFlags);
  
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat,
                              NetFlags, CarbonModel*) const;

  virtual bool isDataNonZero() const;

  virtual const UInt8* getExamineStore() const;

  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask);
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel*) const;

protected:
  virtual void setConstantBits();
  virtual void examineModelDrive(UInt32* drive, ExamineMode) const;
  virtual void examineModelDriveRange(UInt32* drive, size_t index, size_t length) const;
  
  inline void doDeposit(const UInt32* buf, CarbonModel* model);
  inline void doFastDeposit(const UInt32* buf, CarbonModel* model);
  
  CarbonStatus doDepositWord(UInt32 buf, int index, CarbonModel* model);
  CarbonStatus doDepositRange(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model);
  
  UInt8* mVector;

  bool assignValueRange(const UInt32* buf, size_t index, size_t length); 

private:
  bool assignValue(const UInt32* buf); 


private:

  CarbonVector1();
  CarbonVector1(const CarbonVector1&);
  CarbonVector1& operator=(const CarbonVector1&);
};

class CarbonVector2 : public CarbonVector
{
public: CARBONMEM_OVERRIDES
  CarbonVector2(UInt16* vec);
  CarbonVector2(SInt16* vec);
  virtual ~CarbonVector2();

  virtual void getTraits(Traits* traits) const;

  ShellNet::Storage allocShadow() const;
  
  void freeShadow(ShellNet::Storage* shadow);

  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);

  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  
  virtual CarbonStatus examineWord(UInt32* buf, int index, 
                                   UInt32* drive, ExamineMode mode, CarbonModel*) const;

  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;

  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  virtual void update(ShellNet::Storage* shadow) const;
  
  virtual ShellNet::ValueState compare(const ShellNet::Storage shadow) const;

  virtual ShellNet::ValueState writeIfNotEq(char* valueStr, size_t len, ShellNet::Storage* shadow,
                                            NetFlags);
  
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat,
                              NetFlags, CarbonModel*) const;

  virtual bool isDataNonZero() const;

  virtual const UInt16* getExamineStore() const;

  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask);
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel*) const;

protected:
  virtual void setConstantBits();
  virtual void examineModelDrive(UInt32* drive, ExamineMode) const;
  virtual void examineModelDriveRange(UInt32* drive, size_t index, size_t length) const;

  inline void doDeposit(const UInt32* buf, CarbonModel* model);
  inline void doFastDeposit(const UInt32* buf, CarbonModel* model);
  CarbonStatus doDepositWord(UInt32 buf, int index, CarbonModel* model);
  CarbonStatus doDepositRange(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model);

  bool assignValueRange(const UInt32* buf, size_t index, size_t length); 
  
private:
  bool assignValue(const UInt32* buf); 


protected:
  UInt16* mVector;

private:

  CarbonVector2();
  CarbonVector2(const CarbonVector2&);
  CarbonVector2& operator=(const CarbonVector2&);
};


class CarbonVector4 : public CarbonVector
{
public: CARBONMEM_OVERRIDES
  CarbonVector4(UInt32* vec);
  CarbonVector4(SInt32* vec);

  ShellNet::Storage allocShadow() const;
  
  void freeShadow(ShellNet::Storage* shadow);

  virtual ~CarbonVector4();

  virtual void getTraits(Traits* traits) const;

  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);

  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;

  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  virtual void update(ShellNet::Storage* shadow) const;
  
  virtual ShellNet::ValueState compare(const ShellNet::Storage shadow) const;

  virtual ShellNet::ValueState writeIfNotEq(char* valueStr, size_t len, ShellNet::Storage* shadow,
                                            NetFlags);
  
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat,
                              NetFlags, CarbonModel*) const;

  virtual bool isDataNonZero() const;

  virtual const UInt32* getExamineStore() const;

  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask);
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel*) const;

protected:
  virtual void setConstantBits();
  virtual void examineModelDrive(UInt32* drive, ExamineMode) const;
  virtual void examineModelDriveRange(UInt32* drive, size_t index, size_t length) const;

  inline void doDeposit(const UInt32* buf, CarbonModel* model);
  inline void doFastDeposit(const UInt32* buf, CarbonModel* model);
  CarbonStatus doDepositWord(UInt32 buf, int index, CarbonModel* model);
  CarbonStatus doDepositRange(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model);

  bool assignValueRange(const UInt32* buf, size_t index, size_t length); 
  
private:
  bool assignValue(const UInt32* buf); 


protected:
  UInt32* mVector;

private:

  CarbonVector4();
  CarbonVector4(const CarbonVector4&);
  CarbonVector4& operator=(const CarbonVector4&);
};


class CarbonVector8 : public CarbonVector
{
public: CARBONMEM_OVERRIDES
  CarbonVector8(UInt64* vec);
  CarbonVector8(SInt64* vec);
  CarbonVector8(CarbonReal *vec);

  virtual ~CarbonVector8();

  virtual void getTraits(Traits* traits) const;

  ShellNet::Storage allocShadow() const;
  
  void freeShadow(ShellNet::Storage* shadow);

  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);

  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  //! Retrieve the value of the vector when it represents a real value
  virtual CarbonStatus examine(CarbonReal* buf, UInt32*, ExamineMode, CarbonModel*) const;
  
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;

  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  virtual void update(ShellNet::Storage* shadow) const;
  
  virtual ShellNet::ValueState compare(const ShellNet::Storage shadow) const;

  virtual ShellNet::ValueState writeIfNotEq(char* valueStr, size_t len, ShellNet::Storage* shadow,
                                            NetFlags);
  
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat,
                              NetFlags, CarbonModel*) const;

  virtual bool isDataNonZero() const;

  virtual const UInt64* getExamineStore() const;

  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask);
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel*) const;

protected:
  virtual void setConstantBits();
  virtual void examineModelDrive(UInt32* drive, ExamineMode) const;
  virtual void examineModelDriveWord(UInt32* drive, int index) const;
  virtual void examineModelDriveRange(UInt32* drive, size_t index, size_t length) const;

  inline void doDeposit(const UInt32* buf, CarbonModel* model);
  CarbonStatus doDepositWord(UInt32 buf, int index, CarbonModel* model);
  CarbonStatus doDepositRange(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model);

  bool assignValueRange(const UInt32* buf, size_t index, size_t length); 
  
private:
  bool assignValue(const UInt32* buf); 
  bool assignValueWord(UInt32 buf, int index);


protected:
  UInt64* mVector;

private:

  CarbonVector8();
  CarbonVector8(const CarbonVector8&);
  CarbonVector8& operator=(const CarbonVector8&);
};

class CarbonVectorA : public CarbonVector
{
public: CARBONMEM_OVERRIDES
  CarbonVectorA(UInt32* vec);
  virtual ~CarbonVectorA();

  virtual void getTraits(Traits* traits) const;

  ShellNet::Storage allocShadow() const;
  
  void freeShadow(ShellNet::Storage* shadow);

  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);

  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;

  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  virtual void update(ShellNet::Storage* shadow) const;
  
  virtual ShellNet::ValueState compare(const ShellNet::Storage shadow) const;

  virtual ShellNet::ValueState writeIfNotEq(char* valueStr, size_t len, ShellNet::Storage* shadow,
                                            NetFlags);
  
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat,
                              NetFlags, CarbonModel*) const;

  virtual bool isDataNonZero() const;

  virtual const UInt32* getExamineStore() const;

  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask);
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel*) const;

protected:
  virtual void setConstantBits();
  virtual void examineModelDrive(UInt32* drive, ExamineMode) const;
  virtual void examineModelDriveWord(UInt32* drive, int index) const;
  virtual void examineModelDriveRange(UInt32* drive, size_t index, size_t length) const;

  inline void doDeposit(const UInt32* buf, CarbonModel* model);
  CarbonStatus doDepositWord(UInt32 buf, int index, CarbonModel* model);
  CarbonStatus doDepositRange(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model);

  bool assignValueRange(const UInt32* buf, size_t index, size_t length); 

private:
  bool assignValue(const UInt32* buf); 
  bool assignValueWord(UInt32 buf, int index);


protected:
  // This is an actual array of UInt32s
  UInt32* mVector;

  //! Sign extend function 
  /*!
    used only by CarbonVectorAS and CarbonVectorASInput

  */
  void signExtend();

  //! Zero extraneous bits in buf
  void sanitize(UInt32* buf) const;
  
  //! Zero extraneous bits in word, if index is last word
  void sanitizeWord(UInt32* buf, UInt32 index) const;
  
private:

  CarbonVectorA();
  CarbonVectorA(const CarbonVectorA&);
  CarbonVectorA& operator=(const CarbonVectorA&);
};

//! Signed vector class
/*!
  We have to do our own sign extension when depositing to the
  internals of SBitVector
*/
class CarbonVectorAS : public CarbonVectorA
{
 public:
  CARBONMEM_OVERRIDES

  CarbonVectorAS(UInt32* vec);
  virtual ~CarbonVectorAS();

  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);
  
  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);
  
  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

 private:
  CarbonVectorAS();
  CarbonVectorAS(const CarbonVectorAS&);
  CarbonVectorAS& operator=(const CarbonVectorAS&);
};

#endif

