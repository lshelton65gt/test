// -*-c++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __carbon_interface_xtor_h_
#define __carbon_interface_xtor_h_

/*!
  \file 
  C interface between transactors and shell
*/

#ifndef __CARBON_SYS_INCLUDE_H__
#include "codegen/SysIncludes.h"
#endif
#include "carbon/carbon_shelltypes.h"
#include "util/CarbonTypes.h"

class MsgStreamIO;
class MsgContext;

// This is only compiled internally with a C++ compiler, so no ifdefs are required
extern "C" {

  //! Allocates a MsgStreamIO
  MsgStreamIO* carbonInterfaceXtorAllocMsgStream(FILE* stream, bool includeStatusMsgs);
  //! Frees a MsgStreamIO
  void carbonInterfaceXtorFreeMsgStream(MsgStreamIO* str);
  //! Allocates a MsgContext and associates a stream with it
  MsgContext* carbonInterfaceXtorAllocMsgContext(MsgStreamIO* msg);
  //! Frees a MsgContext
  void carbonInterfaceXtorFreeMsgContext(MsgContext* msg);

  // Wrappers around message context functions
  void carbonInterfaceXtorPCIEInvLinkWidth(MsgContext* msgContext, const char* name, const UInt32 linkwidth);

  void carbonInterfaceXtorPCIENotConfiged(MsgContext* msgContext, const char* name);

  void carbonInterfaceXtorPCIEInvTransaction(MsgContext* msgContext, const char* name);

  void carbonInterfaceXtorPCIEInvTransactionInternal(MsgContext* msgContext, const char* name, const char* cmdstr);

  void carbonInterfaceXtorPCIEUnknownFlowCtrlType(MsgContext* msgContext, const char* name, const char* transaction);

  void carbonInterfaceXtorPCIESplitFailed(MsgContext* msgContext, const char* name, const char* transaction);

  void carbonInterfaceXtorPCIECompletionWithNoMatch(MsgContext* msgContext, const char* name, const UInt32 trans_id);

  void carbonInterfaceXtorPCIEInvalidAckSequenceNum(MsgContext* msgContext, const char* name, const UInt32 seq_num, const UInt32 last_seq_num);

  void carbonInterfaceXtorPCIEAckWithNoMatch(MsgContext* msgContext, const char* name, const UInt32 seq_num, const UInt32 last_seq_num);

  void carbonInterfaceXtorPCIEInvalidNakSequenceNum(MsgContext* msgContext, const char* name, const UInt32 seq_num, const UInt32 last_seq_num);

  void carbonInterfaceXtorPCIENakForAckTransaction(MsgContext* msgContext, const char* name, const UInt32 last_seq_num, const UInt32 seq_num);

  void carbonInterfaceXtorPCIECorruptDLLP(MsgContext* msgContext, const char* name);

  void carbonInterfaceXtorPCIEBadCRCOnTLPDoingRetry(MsgContext* msgContext, const char* name, const UInt32 seq_num);

  void carbonInterfaceXtorPCIEGetReturnDataCmdIsNull(MsgContext* msgContext, const char* name);

  void carbonInterfaceXtorPCIEGetReceivedTransCmdIsNull(MsgContext* msgContext, const char* name);

  void carbonInterfaceXtorPCIEInvalidMaxPayloadSize(MsgContext* msgContext, const char* name, const UInt32 payload_size);

  void carbonInterfaceXtorPCIEInvalidRCB(MsgContext* msgContext, const char* name, const UInt32 rcb);

  void carbonInterfaceXtorPCIEUnsupportedSpeed(MsgContext* msgContext, const char* name);

  void carbonInterfaceXtorPCIEUnknownTransactionCategory(MsgContext* msgContext, const char* name, const char* type_string, const UInt32 type);

  void carbonInterfaceXtorPCIEIllegalInterfaceType(MsgContext* msgContext, const char* name);

  void carbonInterfaceXtorPCIEInvalidLinkIndex(MsgContext* msgContext, const char* name, const UInt32 index, const UInt32 link_width);

  void carbonInterfaceXtorPCIEInvalidClockNet(MsgContext* msgContext, const char* name, const char* clock_net);

  void carbonInterfaceXtorPCIEInvalid10BitSignal(MsgContext* msgContext, const char* name, const char* signal_name);

  void carbonInterfaceXtorPCIEInvalid10BitSymbol(MsgContext* msgContext, const char* name, const UInt32 symbol, const UInt32 disparity, const UInt32 lane, const char* time, const UInt32 idle);

  void carbonInterfaceXtorPCIEInvalidIndex(MsgContext* msgContext, const char* name, const char* signal, const UInt32 index);

  void carbonInterfaceXtorPCIEInvalidPipeSignal(MsgContext* msgContext, const char* name, const char* signal);

  void carbonInterfaceXtorPCIEInvalidSignalLinkIndex(MsgContext* msgContext, const char* name, const char* signal, const UInt32 index, const UInt32 link_width);

  void carbonInterfaceXtorPCIESecondDLLPInCycle(MsgContext* msgContext, const char* name, const char* time);

  void carbonInterfaceXtorPCIEInvalidTSNumber(MsgContext* msgContext, const char* name);

  void carbonInterfaceXtorPCIEUnableToExpandReceivedPackets(MsgContext* msgContext, const char* name, const UInt32 total_pkts, const UInt32 length, const UInt32 start);

  void carbonInterfaceXtorPCIEInvalidTLPType(MsgContext* msgContext, const UInt32 type);


}

#endif
