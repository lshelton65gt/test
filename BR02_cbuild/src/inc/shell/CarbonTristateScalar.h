// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONTRISTATESCALAR_H_
#define __CARBONTRISTATESCALAR_H_


#ifndef __CARBONSCALARBASE_H_
#include "shell/CarbonScalarBase.h"
#endif

class ShellNetTristate1;

//! Tristated Scalars
class CarbonTristateScalar: public virtual CarbonScalarBase
{
protected:
  //! Tristated storage
  ShellNetTristate1* mTri;

  //! Examines word of value and external drive
  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int) const;
  //! Sets the value of the tristate. Returns true if changed
  bool assignValue(const UInt32* value, const UInt32* drive);
  void copyValAndXDrive(UInt32* val, UInt32* drv) const;
  void examineValue(UInt32* val, UInt32* drv) const;
  void calcValue(UInt32* val, UInt32* drv) const;
  void examineXVals(UInt32* val, UInt32* drv) const;

  ValueState compareValue(UInt8* val, UInt8* drv) const;

  inline void doDeposit(const UInt32* buf, const UInt32* drive,
                        CarbonModel* model);

  virtual ValueState writeIfNotEqForce(char*, size_t, Storage*, NetFlags, ShellNet*);


  virtual bool isDataNonZero() const;
  
public: CARBONMEM_OVERRIDES
  //! constructor
  CarbonTristateScalar (UInt8* data, UInt8* drive);
  CarbonTristateScalar (UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive);

  //! virtual destructor
  virtual ~CarbonTristateScalar();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! ShellNet::setToDriven()
  virtual bool setToDriven(CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);

  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);

  //! ShellNet::setWordToUndriven()
  virtual bool setWordToUndriven(int index, CarbonModel* model);

  //! ShellNet::setRangeToUndriven()
  virtual bool setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model);

  virtual void setRawToUndriven(CarbonModel* model);

  virtual Storage allocShadow() const;

  virtual void freeShadow(Storage* shadow);

  virtual void update(Storage* shadow) const;

  virtual void updateUnresolved(Storage* shadow) const;

  virtual ValueState compare(const Storage shadow) const;

  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive);

  virtual ValueState writeIfNotEq(char* valueStr, size_t len, Storage* shadow,
                                  NetFlags flags);

  virtual CarbonStatus format(char* valueStr, size_t len, 
                               CarbonRadix strFormat,
                               NetFlags flags, CarbonModel*) const;

  virtual CarbonStatus formatForce(char* valueStr, size_t len, 
                                   CarbonRadix strFormat,
                                   NetFlags flags, ShellNet*,
                                   CarbonModel*) const;

  //! This is a tristate
  virtual bool isTristate() const;

  //! Deposits value and drive
  virtual CarbonStatus deposit(const UInt32 *buf, const UInt32* drive, CarbonModel* model);
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);

  virtual CarbonStatus examine(UInt32 *buf, UInt32* drive, ExamineMode, CarbonModel*) const;

  //! ShellNet::hasDriveConflict()
  virtual int hasDriveConflict() const;
};

#endif

