// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef _SHELL_SYM_NODE_IDENT_H_
#define _SHELL_SYM_NODE_IDENT_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#ifndef __SymTabExpr_h_
#include "exprsynth/SymTabExpr.h"
#endif
#ifndef _EXPRFACTORY_H_
#include "exprsynth/ExprFactory.h"
#endif

class IODBRuntime;

//! Shell-side identifier expression 
class ShellSymNodeIdent : public SymTabIdent
{
public: 
  CARBONMEM_OVERRIDES

  //! Constructor
  ShellSymNodeIdent(STAliasedLeafNode* node,
		    UInt32 bitSize,
                    const DynBitVector* usageMask,
                    const IODBRuntime* db);
  
  //! virtual destructor
  virtual ~ShellSymNodeIdent();

  //! CarbonExpr::compare()
  virtual ptrdiff_t compare(const CarbonExpr* other) const;

  //! CarbonExpr::typeStr()
  virtual const char* typeStr() const;
  
  //! Not allowed currently. Will assert.
  virtual UInt32 determineBitSize() const;

  //! CarbonIdent::getNode()
  /*!
    This returns the Expression SymbolTable node, \e NOT the design
    table node.
  */
  virtual const STAliasedLeafNode* getNode(DynBitVector* usageMask) const;

  
  //! Returns shell symnode ident
  const ShellSymNodeIdent* castShellSymNodeIdent() const;
  
  //! Hash function so that we can do caching
  virtual size_t hash() const;

private:
  const IODBRuntime* mDB;
  DynBitVector mUseMask;
};

//! Symboltable node identifier with backpointer
/*!
  Split nodes ($portsplit...) need to refer to their original
  nets. The backpointer supplied to this class is an expression of the
  original net. So, if the split node represents bits 3 to 0 of the
  original node the CarbonExpr is a partselect ranging from 3 to 0 of
  the original net.
*/
class ShellSymNodeIdentBP : public ShellSymNodeIdent
{
public:
  CARBONMEM_OVERRIDES

  ShellSymNodeIdentBP(STAliasedLeafNode* node,
                      UInt32 bitSize,
                      const DynBitVector* usageMask,
                      const IODBRuntime* db, 
                      CarbonExpr* backPointer);
  

  virtual ~ShellSymNodeIdentBP();
  
  //! CarbonExpr::compare()
  virtual ptrdiff_t compare(const CarbonExpr* other) const;

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! CarbonExpr::print()
  virtual void print(bool, int indent) const;
  
  //! CarbonExpr::typeStr()
  virtual const char* typeStr() const;

  //! Returns this
  virtual const ShellSymNodeIdentBP* castShellSymNodeIdentBP() const;

  //! Get the back pointer
  CarbonExpr* getBackPointer() { 
    const ShellSymNodeIdentBP* me = const_cast<const ShellSymNodeIdentBP*>(this);
    return const_cast<CarbonExpr*>(me->getBackPointer());
  }
  
  //! Get the back pointer - const version
  const CarbonExpr* getBackPointer() const { return mBackPointer; }

  //! compose based on given mode
  virtual void composeIdent(ComposeContext* context) const;

private:
  CarbonExpr* mBackPointer;
};

class SymNodeDBExprContext : public ExprDBContext
{
public: CARBONMEM_OVERRIDES
  SymNodeDBExprContext(const IODBRuntime* caller, STSymbolTable* symTab, 
                       DynBitVectorFactory* factory);
  virtual ~SymNodeDBExprContext();
  
  virtual CarbonIdent* createIdent(STAliasedLeafNode* node, 
				   UInt32 bitSize,
                                   const DynBitVector* usageMask,
                                   CbuildShellDB::CarbonIdentType type,
                                   const CarbonExprVector* nestedExprs);

  virtual void destroyIdent(CarbonIdent* doomedIdent);

private:
  const IODBRuntime* mDB;
};

#endif
