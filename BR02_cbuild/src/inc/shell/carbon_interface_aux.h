// -*-c++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __carbon_interface_aux_h_
#define __carbon_interface_aux_h_

/*!
  \file 
  inline C++ auxilliary functions for C interface between model and shell
*/

#include "shell/carbon_interface.h"
#include "util/BitVector.h"
#include "codegen/carbon_priv.h"

// Overloaded C++ functions for streams to call the correct C equivalent functions
static inline void sWriteToOStream(UtOStream* ostr, const char* s) ALWAYS_INLINE;
static inline void sWriteToOStream(UtOStream* ostr, const char* s)
{
  carbonInterfaceWriteStringToOStream(ostr, s);
}
static inline void sWriteToOStream(UtOStream* ostr, UInt64 num) ALWAYS_INLINE;
static inline void sWriteToOStream(UtOStream* ostr, UInt64 num)
{
  carbonInterfaceWriteUInt64ToOStream(ostr, num);
}
static inline void sWriteToOStream(UtOStream* ostr, SInt64 num) ALWAYS_INLINE;
static inline void sWriteToOStream(UtOStream* ostr, SInt64 num)
{
  carbonInterfaceWriteSInt64ToOStream(ostr, num);
}
static inline void sWriteToOStream(UtOStream* ostr, UInt32 num) ALWAYS_INLINE;
static inline void sWriteToOStream(UtOStream* ostr, UInt32 num)
{
  carbonInterfaceWriteUInt32ToOStream(ostr, num);
}
static inline void sWriteToOStream(UtOStream* ostr, SInt32 num) ALWAYS_INLINE;
static inline void sWriteToOStream(UtOStream* ostr, SInt32 num)
{
  carbonInterfaceWriteSInt32ToOStream(ostr, num);
}
static inline void sWriteToOStream(UtOStream* ostr, UInt16 num) ALWAYS_INLINE;
static inline void sWriteToOStream(UtOStream* ostr, UInt16 num)
{
  carbonInterfaceWriteUInt16ToOStream(ostr, num);
}
static inline void sWriteToOStream(UtOStream* ostr, SInt16 num) ALWAYS_INLINE;
static inline void sWriteToOStream(UtOStream* ostr, SInt16 num)
{
  carbonInterfaceWriteSInt16ToOStream(ostr, num);
}
static inline void sWriteToOStream(UtOStream* ostr, double num) ALWAYS_INLINE;
static inline void sWriteToOStream(UtOStream* ostr, double num)
{
  carbonInterfaceWriteDoubleToOStream(ostr, num);
}
static inline void sWriteToOStream(UtOStream* ostr, const char ch) ALWAYS_INLINE;
static inline void sWriteToOStream(UtOStream* ostr, const char ch)
{
  carbonInterfaceWriteCharToOStream(ostr, ch);
}
template<UInt32 Width, bool _stype>
static inline void sWriteToOStream(UtOStream* ostr, const BitVector<Width, _stype>& bv) ALWAYS_INLINE;
template<UInt32 Width, bool _stype>
static inline void sWriteToOStream(UtOStream* ostr, const BitVector<Width, _stype>& bv)
{
  carbonInterfaceUtOStreamFormatBignum(ostr, bv.getUIntArray(), bv.size());
}
template<bool _stype>
static inline void sWriteToOStream(UtOStream* ostr, const BVref<_stype>& bv) ALWAYS_INLINE;
template<bool _stype>
static inline void sWriteToOStream(UtOStream* ostr, const BVref<_stype>& bv)
{
  // Extract the BitVector pointer, offset, and size
  carbonInterfaceUtOStreamFormatBignumRange(ostr, bv._M_wp, bv._M_bsiz, bv._M_bpos);
}

static inline void sReadFromIStream(UtIStream* istr, UInt64& num) ALWAYS_INLINE;
static inline void sReadFromIStream(UtIStream* istr, UInt64& num)
{
  carbonInterfaceReadUInt64FromIStream(istr, num);
}
static inline void sReadFromIStream(UtIStream* istr, SInt64& num) ALWAYS_INLINE;
static inline void sReadFromIStream(UtIStream* istr, SInt64& num)
{
  carbonInterfaceReadSInt64FromIStream(istr, num);
}
static inline void sReadFromIStream(UtIStream* istr, UInt32& num) ALWAYS_INLINE;
static inline void sReadFromIStream(UtIStream* istr, UInt32& num)
{
  carbonInterfaceReadUInt32FromIStream(istr, num);
}
static inline void sReadFromIStream(UtIStream* istr, SInt32& num) ALWAYS_INLINE;
static inline void sReadFromIStream(UtIStream* istr, SInt32& num)
{
  carbonInterfaceReadSInt32FromIStream(istr, num);
}
static inline void sReadFromIStream(UtIStream* istr, UInt16& num) ALWAYS_INLINE;
static inline void sReadFromIStream(UtIStream* istr, UInt16& num)
{
  carbonInterfaceReadUInt16FromIStream(istr, num);
}
static inline void sReadFromIStream(UtIStream* istr, SInt16& num) ALWAYS_INLINE;
static inline void sReadFromIStream(UtIStream* istr, SInt16& num)
{
  carbonInterfaceReadSInt16FromIStream(istr, num);
}
static inline void sReadFromIStream(UtIStream* istr, UInt8& num) ALWAYS_INLINE;
static inline void sReadFromIStream(UtIStream* istr, UInt8& num)
{
  carbonInterfaceReadUInt8FromIStream(istr, num);
}
static inline void sReadFromIStream(UtIStream* istr, double& num) ALWAYS_INLINE;
static inline void sReadFromIStream(UtIStream* istr, double& num)
{
  carbonInterfaceReadDoubleFromIStream(istr, num);
}
static inline void sReadFromIStream(UtIStream* istr, char& ch) ALWAYS_INLINE;
static inline void sReadFromIStream(UtIStream* istr, char& ch)
{
  carbonInterfaceReadCharFromIStream(istr, ch);
}
static inline void sReadFromIStream(UtIStream* istr, const BVref<false>& bv) ALWAYS_INLINE;
static inline void sReadFromIStream(UtIStream* istr, const BVref<false>& bv)
{
  // The fact that the BVref reference is marked const is bad, bad,
  // bad, because it's actually modified by manipulating the
  // underlying BitVector array.  However, it has to be marked const
  // because this function is called with a temporarily-scoped BVref,
  // and the compiler won't let you pass a non-const one of those by
  // reference.  e.g.
  //
  // BitVector bv;
  // sReadFromIStream(istr, BVref<false>(bv));
  //
  // This is being used in place of UtIStream::operator>>(const
  // BVref<false>&), which also had the incorrect const qualifier, so
  // it will stay incorrect for now.

  // Extract the BitVector pointer, offset, and size
  carbonInterfaceReadBVRefFromIStream(istr, bv._M_wp, bv._M_bsiz, bv._M_bpos);
}


// All PODs have the same implementation for these
static inline void readFromStream(UtICheckpointStream* str, UInt8& val) ALWAYS_INLINE;
static inline void readFromStream(UtICheckpointStream* str, UInt8& val)
{
  carbonInterfaceICheckpointStreamRead(str, &val, sizeof(val));
}
static inline void readFromStream(UtICheckpointStream* str, SInt8& val) ALWAYS_INLINE;
static inline void readFromStream(UtICheckpointStream* str, SInt8& val)
{
  carbonInterfaceICheckpointStreamRead(str, &val, sizeof(val));
}
static inline void readFromStream(UtICheckpointStream* str, UInt16& val) ALWAYS_INLINE;
static inline void readFromStream(UtICheckpointStream* str, UInt16& val)
{
  carbonInterfaceICheckpointStreamRead(str, &val, sizeof(val));
}
static inline void readFromStream(UtICheckpointStream* str, SInt16& val) ALWAYS_INLINE;
static inline void readFromStream(UtICheckpointStream* str, SInt16& val)
{
  carbonInterfaceICheckpointStreamRead(str, &val, sizeof(val));
}
static inline void readFromStream(UtICheckpointStream* str, UInt32& val) ALWAYS_INLINE;
static inline void readFromStream(UtICheckpointStream* str, UInt32& val)
{
  carbonInterfaceICheckpointStreamRead(str, &val, sizeof(val));
}
static inline void readFromStream(UtICheckpointStream* str, SInt32& val) ALWAYS_INLINE;
static inline void readFromStream(UtICheckpointStream* str, SInt32& val)
{
  carbonInterfaceICheckpointStreamRead(str, &val, sizeof(val));
}
static inline void readFromStream(UtICheckpointStream* str, UInt64& val) ALWAYS_INLINE;
static inline void readFromStream(UtICheckpointStream* str, UInt64& val)
{
  carbonInterfaceICheckpointStreamRead(str, &val, sizeof(val));
}
static inline void readFromStream(UtICheckpointStream* str, SInt64& val) ALWAYS_INLINE;
static inline void readFromStream(UtICheckpointStream* str, SInt64& val)
{
  carbonInterfaceICheckpointStreamRead(str, &val, sizeof(val));
}
static inline void readFromStream(UtICheckpointStream* str, double& val) ALWAYS_INLINE;
static inline void readFromStream(UtICheckpointStream* str, double& val)
{
  carbonInterfaceICheckpointStreamRead(str, &val, sizeof(val));
}

static inline void writeToStream(UtOCheckpointStream* str, const UInt8& val) ALWAYS_INLINE;
static inline void writeToStream(UtOCheckpointStream* str, const UInt8& val)
{
  carbonInterfaceOCheckpointStreamWrite(str, &val, sizeof(val));
}
static inline void writeToStream(UtOCheckpointStream* str, const SInt8& val) ALWAYS_INLINE;
static inline void writeToStream(UtOCheckpointStream* str, const SInt8& val)
{
  carbonInterfaceOCheckpointStreamWrite(str, &val, sizeof(val));
}
static inline void writeToStream(UtOCheckpointStream* str, const UInt16& val) ALWAYS_INLINE;
static inline void writeToStream(UtOCheckpointStream* str, const UInt16& val)
{
  carbonInterfaceOCheckpointStreamWrite(str, &val, sizeof(val));
}
static inline void writeToStream(UtOCheckpointStream* str, const SInt16& val) ALWAYS_INLINE;
static inline void writeToStream(UtOCheckpointStream* str, const SInt16& val)
{
  carbonInterfaceOCheckpointStreamWrite(str, &val, sizeof(val));
}
static inline void writeToStream(UtOCheckpointStream* str, const UInt32& val) ALWAYS_INLINE;
static inline void writeToStream(UtOCheckpointStream* str, const UInt32& val)
{
  carbonInterfaceOCheckpointStreamWrite(str, &val, sizeof(val));
}
static inline void writeToStream(UtOCheckpointStream* str, const SInt32& val) ALWAYS_INLINE;
static inline void writeToStream(UtOCheckpointStream* str, const SInt32& val)
{
  carbonInterfaceOCheckpointStreamWrite(str, &val, sizeof(val));
}
static inline void writeToStream(UtOCheckpointStream* str, const UInt64& val) ALWAYS_INLINE;
static inline void writeToStream(UtOCheckpointStream* str, const UInt64& val)
{
  carbonInterfaceOCheckpointStreamWrite(str, &val, sizeof(val));
}
static inline void writeToStream(UtOCheckpointStream* str, const SInt64& val) ALWAYS_INLINE;
static inline void writeToStream(UtOCheckpointStream* str, const SInt64& val)
{
  carbonInterfaceOCheckpointStreamWrite(str, &val, sizeof(val));
}
static inline void writeToStream(UtOCheckpointStream* str, const double& val) ALWAYS_INLINE;
static inline void writeToStream(UtOCheckpointStream* str, const double& val)
{
  carbonInterfaceOCheckpointStreamWrite(str, &val, sizeof(val));
}

// IOStringStream is a special case
static inline void readFromStream(UtICheckpointStream* str, UtIOStringStream*& sstr) ALWAYS_INLINE;
static inline void readFromStream(UtICheckpointStream* str, UtIOStringStream*& sstr)
{
  carbonInterfaceRestoreIOStringStream(str, &sstr);
}
static inline void writeToStream(UtOCheckpointStream* str, UtIOStringStream*& sstr) ALWAYS_INLINE;
static inline void writeToStream(UtOCheckpointStream* str, UtIOStringStream*& sstr)
{
  carbonInterfaceSaveIOStringStream(str, &sstr);
}

#endif
