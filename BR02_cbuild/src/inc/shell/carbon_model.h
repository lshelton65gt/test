// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __carbon_model_h_
#define __carbon_model_h_

#define CARBON_MODEL_VERSION 1

#ifndef __carbon_shelltypes_h_
#include "shell/carbon_shelltypes.h"
#endif

#ifdef __cplusplus
class CarbonModel;                      // external declaration
class CarbonNet;
class CarbonMemory;
class CarbonWave;
class ShellNet;
class DynBitVector;
class ControlHelper;
class UtOCheckpointStream;
class UtICheckpointStream;
class VerilogOutFileSystem;
class HdlIStream;
class HdlOStream;
#else
struct CarbonModel;
struct ShellNet;
struct ControlHelp;
struct UtOCheckpointStream;
struct UtICheckpointStream;
struct HdlIStream;
struct HdlOStream;
#endif

#ifdef __cplusplus
extern "C" {
#endif

struct carbon_model_descr;              // forward
struct carbon_model_private;            // external
struct carbon_change_array;             // external

// These function prototype typedefs replace the typedefs of the same names
// that used to be in ShellGlobals.h. This is a pure C interface.

//! Design save callback
typedef int (*CarbonSaveFn) (UtOCheckpointStream *, struct carbon_model_descr *);

//! Design restore callback
typedef int (*CarbonRestoreFn) (UtICheckpointStream *, struct carbon_model_descr *);

//! Design core destruction function type definition
typedef void (*CarbonCoreDestroyFn) (struct carbon_model_descr *);

//! Design core schedule function type definition
typedef CarbonStatus (*CarbonCoreScheduleFn)(struct carbon_model_descr *, CarbonUInt64);

//! Debug schedule function type definition
typedef void (*CarbonSimpleScheduleFn)(struct carbon_model_descr *);

//! Design core initialize function type definition
typedef void (*CarbonCoreInitializeFn)(struct carbon_model_descr *, void*, void*, void*);

//! Design core change array to storage offset map fn type
//typedef void (*CarbonChgIndToStorageFn)(CarbonHookup*);
typedef void (*CarbonChgIndToStorageFn)(struct carbon_model_descr *);

//! Design core simulation over fn type
typedef void (*CarbonSimulationOverFn)(struct carbon_model_descr *);

//! ShellNet generation
typedef ShellNet * (*CarbonDebugGeneratorFn) (UInt32 index, void *storage, int isScalar);

//! Write the IO database into a file
typedef void (*CarbonGetIODB)(const char** buffer, int* buflen);

//! Write the Full database into a file
typedef void (*CarbonGetFullDB)(const char** buffer, int* buflen);

//! Structure passed by model to setupProfiling().
// This is a structure rather than separate parameters so it can be
// extended without modifying the method's signature.
struct CarbonProfileInfo {
  const char* modelName;
  CarbonUInt32 numBlockBuckets;
  volatile int* currentBlockBucket;
  CarbonUInt32 numScheduleBuckets;
  volatile int* currentScheduleBucket;
};

#ifdef PROFILING

// mem barrier is specified different ways for different compilers
#if pfMSVC
#define MEM_BARRIER _ReadWriteBarrier()
#elif pfGCC
#define MEM_BARRIER __asm__ __volatile__("":::"memory")
#else
#define MEM_BARRIER 
#endif


#define SETPROFILEBUCKET(name,number) \
   MEM_BARRIER;\
   name = number;\
   MEM_BARRIER;
#else
// if profiling is not enabled then the SETPROFILEBUCKET macro does nothing, use the (void) to avoid warnings about unused macro arguments
#define SETPROFILEBUCKET(name,number) \
  (void)number
#endif


#ifdef __cplusplus

// These functions are used to implement the convenience methods, such as
// getModel in carbon_model_descr. There are no guarentees that the C++
// compiler used to compile the test bench is the same as the compiler used to
// compile libcarbon. To work around this, the following idiom is used:
//
// Suppose that we desire a the method carbon_model_descr::foo (UInt32 x). The
// define the function CarbonFoo (struct carbon_model_descr *, UInt32 x). The
// define inline carbon_model_descr::foo that calls CarbonFoo (this, x). The
// testbench compiler will generate a call to the C function
// CarbonFoo. CarbonFoo is compiled as a C function in libcarbon. To summarise:
//
// extern "C" {
//   ReturnType CarbonFoo (struct carbon_model_descr *, ArgType1, ArgType2);
// }
//
// struct carbon_model_descr {
//   ...
//   inline ReturnType foo (ArgType1 arg1, ArgType2 arg2)
//   { return CarbonFoo (arg1, arg2); }
//   ...
// };

CarbonModel *carbonPrivateGetModel (struct carbon_model_descr *);

CarbonUInt32 *carbonPrivateGetRunDepositComboSchedPointer (struct carbon_model_descr *);

//! Returns the current run mode
CarbonVHMMode carbonPrivateGetRunMode (struct carbon_model_descr *);

//! Run the initialization function
CarbonStatus carbonPrivateRunInitialize (struct carbon_model_descr *,
  void* cmodelData, void* reserved1, void* reserved2);

//! Find a memory based on its name
CarbonMemory *carbonPrivateFindMemory (struct carbon_model_descr *, const char *name);

//! Find a net based on its name
CarbonNet* carbonPrivateFindNet (struct carbon_model_descr *, const char* name);

//! Reads the net's value and puts it into buf
CarbonStatus carbonPrivateExamine (struct carbon_model_descr *, const CarbonNet* net, 
  CarbonUInt32* value, CarbonUInt32* drive);

//! Examine a word of the net's value
CarbonStatus carbonPrivateExamineWord(struct carbon_model_descr *, const CarbonNet* net, 
  CarbonUInt32* value, int index, CarbonUInt32* drive);

//! Return the already created CarbonWave
CarbonWave* carbonPrivateGetCarbonWave (struct carbon_model_descr *);

//! Called by generated model to initialize profiling.
void carbonPrivateSetupProfiling (struct carbon_model_descr *, CarbonProfileInfo* prof_info);

//! Run the schedule and update time. MT-Safe
CarbonStatus carbonPrivateSchedule (struct carbon_model_descr *, CarbonTime currentSimulationTime);

//! Run the clock schedule and update time. MT-Safe
CarbonStatus carbonPrivateClkSchedule (struct carbon_model_descr *, CarbonTime currentSimulationTime);
 
//! Run the data schedule and update time. MT-Safe
CarbonStatus carbonPrivateDataSchedule (struct carbon_model_descr *, CarbonTime currentSimulationTime);

//! Run the async schedule and update time. MT-Safe
CarbonStatus carbonPrivateAsyncSchedule(struct carbon_model_descr *, CarbonTime currentSimulationTime);

//! Run the dep-combo schedule, if needed.
void carbonPrivateDepComboSchedule (struct carbon_model_descr *);
 
//! Get the current simulation time. MT-Safe
CarbonTime carbonPrivateGetSimulationTime (const struct carbon_model_descr *);

//! Put the current simulation time. MT-Safe
void carbonPrivatePutSimulationTime (struct carbon_model_descr *, CarbonTime simTime);

//! Use the Stats obj to print the interval stats
void carbonPrivatePrintIntervalStatistics (struct carbon_model_descr *descr, const char *label);

//! If statistics are to be printed, print them out
void carbonPrivateMaybePrintStatsStdout (struct carbon_model_descr *, const char* name, size_t objSize);

//! Increment the schedule call count
void carbonPrivateIncrScheduleCallCount (struct carbon_model_descr *);

//! Reset the schedule call count
void carbonPrivateResetScheduleCallCount (struct carbon_model_descr *);

//! Allocate space for primary clock changed array and clock names
// This routine only be called if glitch detection is turned on
void carbonPrivateAllocatePrimaryClockChangeData (struct carbon_model_descr *descr, size_t numClocks);

//! Set the name for a given primary clock
// This routine only be called if glitch detection is turned on
void carbonPrivateSetClockName (struct carbon_model_descr *, size_t index, const char* name);

//! Clear the primary clock changed array
// This routine only be called if glitch detection is turned on
void carbonPrivateClearPrimaryClockChanges (struct carbon_model_descr *);

//! Test and potential print a race
// This routine only be called if glitch detection is turned on
void carbonPrivateCheckPrimaryClockRace (struct carbon_model_descr *, const DynBitVector& curChanges);

//! No longer used, but must remain for backwards compatibilty
void carbonPrivateSHLClockGlitch (struct carbon_model_descr *, const char *name, 
  unsigned int oldVal, unsigned int newVal);

//! Reports a clock glitch in the model
/*!
  The index is used to look up the clock's name in an array stored in the IODB.
*/
void carbonPrivateSHLClockGlitchByIndex (struct carbon_model_descr *, UInt32 index, 
  unsigned int oldVal, unsigned int newVal);

//! \return the control helper object for the model
ControlHelper *carbonPrivateGetControlHelper (struct carbon_model_descr *);

//! \return the control helper object for the model
ControlHelper *carbonPrivateGetControlHelper (struct carbon_model_descr *);

//! returns the current carbon status for the current run of the schedule
CarbonStatus carbonPrivateGetCarbonStatus  (struct carbon_model_descr *);

//! reset the carbonStatus for the current run of the schedule
void carbonPrivateResetCarbonStatus (struct carbon_model_descr *);

//! use this to update the carbonStatus when an error has been processed in the schedule
void carbonPrivateUpdateCarbonStatusDueToError (struct carbon_model_descr *);

//! use this to update the carbonStatus when a stop has been processed in the schedule
void carbonPrivateUpdateCarbonStatusDueToStop (struct carbon_model_descr *);

//! use this to update the carbonStatus when a finish has been processed in the schedule
void carbonPrivateUpdateCarbonStatusDueToFinish (struct carbon_model_descr *);

typedef struct carbon_change_array CarbonChangeArray;

//! Create a change array and initialise the elements to CARBON_CHANGE_FALL_MASK
CarbonChangeArray *carbonPrivateCreateChangeArray (CarbonUInt32 n_elements);

//! Destroy a change array
void carbonPrivateDestroyChangeArray (CarbonChangeArray *);

//! Set all the elements of a change array to a value
void carbonPrivateSetChangeArray (CarbonChangeArray *, CarbonChangeType value);

//! Initialise the elements of the change array
void carbonPrivateInitChangeArray (CarbonChangeArray *, CarbonChangeType);

//! Reset the elements of the change array to the initial value
/*! The sets the elements to the value passed in the last call to
 *  carbonPrivateInitChangeArray
 */
void carbonPrivateReInitChangeArray (CarbonChangeArray *);

struct carbon_model_private *carbonPrivateCreatePrivateModelData (struct carbon_model_descr *, CarbonOpaque handle);
void carbonPrivateFreePrivateModelData (struct carbon_model_descr *);

void carbonPrivateCallDebugCallback (struct carbon_model_descr *);
void carbonPrivateCallDestroyCallback (struct carbon_model_descr *);
int carbonPrivateCheckAndClearRunDepositComboSchedule (struct carbon_model_descr *);

void carbonPrivateResetScheduleCallCount (struct carbon_model_descr *);

//! returns the VerilogOutFileSystem for this model.
VerilogOutFileSystem* carbonPrivateGetVerilogOutFileSystem (struct carbon_model_descr *);

//! returns the VhdlOutFileSystem for this model.
HdlOStream* carbonPrivateGetVhdlOutFileSystem (struct carbon_model_descr *);

//! returns the VhdlInFileSystem for this model.
HdlIStream* carbonPrivateGetVhdlInFileSystem (struct carbon_model_descr *);

//! Remove a model from the shell model finder
void carbonPrivateRemoveModel (struct carbon_model_descr *);

#endif

//! \struct carbon_top
/*! Gather field required in the top class into a single struct. There must be
 *  a single instance of carbon_top declared as the first member of the
 *  top-module class. sizeof (carbon_top) is used as the fixed offset to the
 *  data members of the class.
 */
struct carbon_top {

#if __cplusplus

  // The generated constructor for the top class binds unused0 with the return
  // value of a call to fixup_hierref_pointers. Storing an otherwise useless
  // return value ensures that the call never gets optimised away.

  carbon_top (CarbonUInt32 unused0) : mUnused0 (unused0), mDescr (NULL)
  {}
#endif
  
  CarbonUInt32 mUnused0;
  carbon_model_descr *mDescr;

};

//! \struct carbon_model_descr
/*! The carbon_model_descr encapsulates references to data structures and
 *  functions in the generated Carbon model. It is created in the
 *  carbon_design_create method emitted by the compiler.
 *  
 *  The model-compatibility strategy is this:
 *
 *  If new members get added, this structure can be extended, as long as the
 *  old members are left in place.  When a model is initialized, the generated
 *  code will indicate which fields it has provided via a version number.
 * 
 *  If a new libcarbon is initialized by a ShellGlobal::newModel call from an
 *  old model, it will know based on the version number which fields were
 *  provided.  It should be possible for a user to link together models
 *  compiled with different versions of cbuild as long as the libcarbon is at
 *  least as new as the newest model.
 * 
 *  If an old libcarbon is initialized by a createModel call from a new model,
 *  it can exit indicating that the libcarbon is is upward compatible but not
 *  downward compatible.
 *
 */

struct carbon_model_descr {

  /**********************************************************
   * DO NOT CHANGE THE ORDER OF ANY OF THESE MEMBERS OR ADD *
   * ADD NEW MEMBERS ANYWHERE BUT AT THE END OF THE STRUCT. *
   * REALLY, JUST DON'T DO IT!                              *
   **********************************************************/

  CarbonUInt32 mVersion;                      //!< version number of the C model interface
  struct carbon_model_private *mPrivate;

  CarbonInitFlags mInitFlags;

  /*! For mVersion == 0, mHdl is a carbon_id. For mVersion > 0, it is the top
      module class that is not subclassed from carbon_id */

  // The class instance that contains all the model data.
  void *mHdl;                           //!< handle to the hdl
  CarbonUInt32 mHdlSize;                //!< size of the mHdl structure

  //! Unique identifier for the model
  const char *mUID;

  //! Pointer to the model simulation time--points back to the location in ShellGlobal
  CarbonTime *mpTime;

  CarbonOpaque mCModelData;             //!< Data passed to all c-models

  // Pointers to generated functions.
  CarbonCoreDestroyFn destroyFn;
  CarbonCoreScheduleFn schedFn;
  CarbonCoreInitializeFn initFn;
  CarbonChgIndToStorageFn chgIndFn;
  CarbonSimulationOverFn simOverFn;
  CarbonGetIODB getIODBFn;
  CarbonGetFullDB getFullDBFn;
  CarbonCoreScheduleFn clkSchedFn;
  CarbonCoreScheduleFn dataSchedFn;
  CarbonModeChangeCBFunc modeChangeFn;
  CarbonCoreScheduleFn asyncSchedFn;
  CarbonSimpleScheduleFn debugSchedFn;
  CarbonSimpleScheduleFn depositComboSchedFn;
  CarbonDebugGeneratorFn debugGeneratorFn;
  CarbonSaveFn saveFn;
  CarbonRestoreFn restoreFn;
  CarbonSaveFn onDemandSaveFn;
  CarbonRestoreFn onDemandRestoreFn;

  // The change array.
  carbon_change_array *mChangeArray;

  // If the testbench is linked with one C RTL (e.g., MS), and libcarbon.dll is
  // linked with another (e.g., MinGW's glibc), they each have their own
  // buffers for stdio streams, so output appears in the wrong order.  So the
  // Carbon Model passes the testbench's stdout and stderr and functions that
  // operate on them so UtIO and MsgContexts use the same streams as the
  // testbench.
  FILE* mStdout;
  FILE* mStderr;
  size_t (*pfwrite) (const void *, size_t, size_t, FILE *);
  int (*pfflush) (FILE *);

  //
  // Address of the model 
  //
  void *mModel;                         //!< this is really a CarbonModel *

#ifdef __cplusplus

  // Convenience methods - without these a myriad of testbenches and probably
  // much customer code would need to be changed.

  //! Returns a pointer to the CarbonModel
  inline CarbonModel *getModel ()
  { return carbonPrivateGetModel (this); }

  //! Returns a const pointer to the carbon model
  inline const CarbonModel *getModel () const
  { return carbonPrivateGetModel (const_cast <carbon_model_descr *> (this)); }

  //! Returns the current run mode
  inline CarbonVHMMode getRunMode ()
  { return carbonPrivateGetRunMode (this); }

  inline CarbonStatus initialize (void* cmodelData, void* reserved1, void* reserved2)
  { return carbonPrivateRunInitialize (this, cmodelData, reserved1, reserved2); }

  //! Find a memory based on its name
  inline CarbonMemory* findMemory (const char* name)
  { return carbonPrivateFindMemory (this, name); }

  //! Find a net based on its name
  inline CarbonNet* findNet (const char* name)
  { return carbonPrivateFindNet (this, name); }
  
  //! Reads the net's value and puts it into buf
  inline CarbonStatus examine(const CarbonNet* net, CarbonUInt32* value, 
    CarbonUInt32* drive = (CarbonUInt32 *) 0)
  { return carbonPrivateExamine (this, net, value, drive); }

  //! Examine a word of the net's value
  inline CarbonStatus examineWord(const CarbonNet* net, CarbonUInt32* value, int index, 
    CarbonUInt32* drive = (CarbonUInt32 *) 0)
  { return carbonPrivateExamineWord (this, net, value, index, drive); }

  //! Return the already created CarbonWave
  inline CarbonWave* getCarbonWave ()
  { return carbonPrivateGetCarbonWave (this); }

  //! Called by generated model to initialize profiling.
  inline void setupProfiling(CarbonProfileInfo* prof_info)
  { carbonPrivateSetupProfiling (this, prof_info); }

  //! Run the schedule and update time. MT-Safe
  inline CarbonStatus schedule(CarbonTime currentSimulationTime)
  { return carbonPrivateSchedule (this, currentSimulationTime); }

  //! Run the clock schedule and update time. MT-Safe
  inline CarbonStatus clkSchedule(CarbonTime currentSimulationTime)
  { return carbonPrivateClkSchedule (this, currentSimulationTime); }
 
  //! Run the data schedule and update time. MT-Safe
  inline CarbonStatus dataSchedule(CarbonTime currentSimulationTime)
  { return carbonPrivateDataSchedule (this, currentSimulationTime); }

  //! Run the async schedule and update time. MT-Safe
  inline CarbonStatus asyncSchedule(CarbonTime currentSimulationTime)
  { return carbonPrivateAsyncSchedule (this, currentSimulationTime); }

  //! Run the dep-combo schedule, if needed.
  inline void depComboSchedule ()
  { carbonPrivateDepComboSchedule (this); }
 
  //! Get the current simulation time. MT-Safe
  inline CarbonTime getSimulationTime () const
  { return carbonPrivateGetSimulationTime (this); }

  //! Put the current simulation time. MT-Safe
  inline void putSimulationTime(CarbonTime simTime)
  { carbonPrivatePutSimulationTime (this, simTime); }

  //! \return the change array
  inline carbon_change_array *getChangeArray ()
  { return mChangeArray; }
  
  //! \return iff replay is enabled
  inline int isReplayEnabled () const
  { return mInitFlags & eCarbon_Replay; }

  //! \return iff on demand is enabled
  inline int isOnDemandEnabled () const
  { return mInitFlags & eCarbon_OnDemand; }

  //! \return iff statistics are enabled
  inline int printStatistics () const
  { return mInitFlags & eCarbon_EnableStats; }

  //! \return iff the init routine should be run are part of create
  inline int runInit () const
  { return (mInitFlags & eCarbon_NoInit) == 0; }

  //! \return iff glitch detection should be enabled
  inline int checkClockGlitches () const
  { return mInitFlags & eCarbon_ClockGlitchDetect; }

  //! If statistics are to be printed, print them out
  inline void maybePrintStatsStdout (const char* name, size_t objSize)
  {
    carbonPrivateMaybePrintStatsStdout (this, name, objSize);
  }

  //! Print interval statistics
  inline void printIntervalStatistics (const char *label)
  {
    carbonPrivatePrintIntervalStatistics (this, label);
  }

  //! Reset the schedule call count
  inline void resetScheduleCallCount ()
  {
    carbonPrivateResetScheduleCallCount (this);
  }

  //! Increment the schedule call count
  inline void incrScheduleCallCount ()
  {
    carbonPrivateIncrScheduleCallCount (this);
  }

  //! Set the simulation time
  inline bool setTime (UInt64 time)
  {
    if (*mpTime == time) {
      return false;                     // no change
    } else {
      *mpTime = time;
      return true;
    }
  }

  //! \return the simulation time
  inline UInt64 getTime (void) const
  { return *mpTime; }

  //! Set the name for a given primary clock
  // This routine only be called if glitch detection is turned on
  inline void setClockName (size_t index, const char* name) 
  { carbonPrivateSetClockName (this, index, name); }

  //! Allocate space for primary clock changed array and clock names
  // This routine only be called if glitch detection is turned on
  inline void allocatePrimaryClockChangeData (size_t numClocks)
  { carbonPrivateAllocatePrimaryClockChangeData (this, numClocks); }

  //! Clear the primary clock changed array
  // This routine only be called if glitch detection is turned on
  inline void clearPrimaryClockChanges ()
  { carbonPrivateClearPrimaryClockChanges (this); }

  //! Test and potential print a race
  // This routine only be called if glitch detection is turned on
  inline void checkPrimaryClockRace (const DynBitVector& curChanges)
  { carbonPrivateCheckPrimaryClockRace (this, curChanges); }

  //! Reports a clock glitch in the model
  inline void SHLClockGlitch (UInt32 index, 
    unsigned int oldVal, unsigned int newVal)
  { carbonPrivateSHLClockGlitchByIndex (this, index, oldVal, newVal); }

  //! \return the control helper object for the model
  ControlHelper *getControlHelper ()
  { return carbonPrivateGetControlHelper (this); }

  //! returns the current carbon status for the current run of the schedule
  inline CarbonStatus getCarbonStatus()
  { return carbonPrivateGetCarbonStatus (this); }

  //! reset the carbonStatus for the current run of the schedule
  inline void resetCarbonStatus()
  { carbonPrivateResetCarbonStatus (this); }

  //! use this to update the carbonStatus when an error has been processed in the schedule
  inline void updateCarbonStatusDueToError ()
  { carbonPrivateUpdateCarbonStatusDueToError (this); }

  //! use this to update the carbonStatus when a stop has been processed in the schedule
  inline void updateCarbonStatusDueToStop()
  { carbonPrivateUpdateCarbonStatusDueToStop (this); }

  //! use this to update the carbonStatus when a finish has been processed in the schedule
  inline void updateCarbonStatusDueToFinish()
  { carbonPrivateUpdateCarbonStatusDueToFinish (this); }

  //! Set the data passed to all c-models
  inline void putCModelData (CarbonOpaque model_data)
  { mCModelData = model_data; }

  //! Get the data passed to all c-models
  inline CarbonOpaque getCModelData () const
  { return mCModelData; }

  //! returns the VerilogOutFileSystem for this model.
  inline VerilogOutFileSystem* getVerilogOutFileSystem ()
  { return carbonPrivateGetVerilogOutFileSystem (this); }

  //! returns the VhdlOutFileSystem for this model.
  inline HdlOStream* getVhdlOutFileSystem ()
  { return carbonPrivateGetVhdlOutFileSystem (this); }

  //! returns the VhdlInFileSystem for this model.
  inline HdlIStream* getVhdlInFileSystem ()
  { return carbonPrivateGetVhdlInFileSystem (this); };

private:

  carbon_model_descr ();
  carbon_model_descr (const carbon_model_descr &);
  carbon_model_descr& operator = (const carbon_model_descr&);

  //! Hide the delete operator
  /*! This model descriptor is created in the generated carbon_design_create
   *  method. It must be freed with carbon_design_destroy and never with
   *  delete.
   */
  void operator delete (void *p);

#endif
};

void carbonPrivateBeginChangeIndToStorageMap (struct carbon_model_descr *);
void carbonPrivateMapChangeIndToStorage (struct carbon_model_descr *, CarbonSInt32, CarbonSInt32);
void carbonPrivateEndChangeIndToStorageMap (struct carbon_model_descr *);

  //! Abstraction for a c-model playback function
  /*! If a c-model needs to be run during playback (has external side
   *  effects), then we need a consistent way to call all of them from
   *  the playback engine. This is done by calling them using a
   *  common * prototype across all c-models.
   *
   *  The arguments are:
   *
   *  cmodelData- A pointer to the c-model class data within the
   *              design hierarchy. Each c-model has a different
   *              type so it is stored as a void*. We might consider
   *              using a base class for all c-models instead.
   *
   *  inputs    - An array of CarbonUInt32 inputs for the c-model. Each
   *              c-model knows how to copy the inputs to
   *              the explicit run function arguments.
   *              
   *  outputs   - An array of CarbonUInt32 inputs for the c-model. Each
   *              c-model knows how to copy the c-model explicit
   *              outputs back to the CarbonUInt32 array storage.
   */
typedef void (*CModelPlaybackFn)(void* cmodelData, 
  CarbonUInt32* inputs, CarbonUInt32* outputs);

#ifdef __cplusplus
class ShellNet;
//! CarbonNet constructor function type definition
typedef ShellNet* (*CarbonNetGenerator)(void*, bool);
#else
//! CarbonNet constructor function type definition
typedef void* (*CarbonNetGenerator)(void*, int);
#endif


void carbonPrivateRegisterCModel (struct carbon_model_descr *, void* cmodelData, void *userData, 
  CarbonUInt32 context, const char* name, CarbonUInt32 numInputWords, 
  CarbonUInt32 numOutputWords, CModelPlaybackFn fn);

int carbonPrivateIsCheckpointEvent (struct carbon_model_descr *);

void carbonPrivateFinalizeScheduleCall (struct carbon_model_descr *);

CarbonChangeType *carbonPrivateGetChanged (struct carbon_model_descr *);
SInt8 carbonPrivateGetChangeInit (struct carbon_model_descr *);

CarbonUInt64 *carbonPrivateGetTimeVarAddr (CarbonOpaque);

#ifdef __cplusplus
}
#endif

#endif

