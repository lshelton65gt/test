// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _OnDemandTraceFileWriter_h_
#define _OnDemandTraceFileWriter_h_

#include "util/UtString.h"

class ModelData;
class UtOBStream;

class OnDemandTraceFileWriter
{
public:
  CARBONMEM_OVERRIDES

  OnDemandTraceFileWriter(const char *fname);
  ~OnDemandTraceFileWriter();

  void addModel(const char *name, CarbonObjectID *model);
  bool enable();
  void disable();
  void flush();
  void append(const char*);

  // Event type encodings
  static const char scDebugEvent = 'd';        // Debug (a.k.a trace) event
  static const char scLooking = 'l';           // Start looking
  static const char scIdle = 'i';              // Enter idle state
  static const char scNotLooking = 'n';        // Not looking (backoff/stopped)
  static const char scNoEvent = 'x'   ;        // No event
  static const char scTimestamp = 't';         // simulation start timestamp
  static const char scEndOfData = 'z';         // Simulation finished 

  void putSimStartTimestamp(UInt64 ts);

private:
  UtString        mFileName;
  UtString        mBuffer;
  bool            mEnabled;
  ModelData      *mModelData;
  UInt64          mSimStartTimestamp;

  void writeEndOfData();
};

#endif // _OnDemandTraceFileWriter_h_
