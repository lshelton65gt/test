// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __SHELLNETPLAYBACK_H_
#define __SHELLNETPLAYBACK_H_

#include "shell/ShellNetWrapper.h"
#include "util/DynBitVector.h"

//! Typedef for wide memories
typedef UtHashMap<SInt32, DynBitVector> SInt32DynBVMap;

//! Object for holding memory changes for memory writes
/*!
  Appending an address creates a hash entry or retrieves the current
  entry and passes that back for modification.
*/
struct ReplayMemChangeMap
{
  CARBONMEM_OVERRIDES
  
  ReplayMemChangeMap(UInt32 numRowWords) :
    mNumRowWords(numRowWords)
  {}
  
  ~ReplayMemChangeMap() {
    clear();
  }
  
  UInt32* appendAddr(SInt32 address)
  {
    UInt32* data = NULL;
    SInt32DynBVMap::MapEntry* entry = mMem.findEntry(address);
    if (entry)
    {
      DynBitVector& bv = entry->mKeyVal.second;
      data = bv.getUIntArray();
    }
    else
    {
      DynBitVector& bv = mMem[address];
      bv.resize(mNumRowWords * 32);
      data = bv.getUIntArray();
    }
    return data;
  }
  

  void clear()
  {
    mMem.clear();
  }
  
  SInt32DynBVMap mMem;
  UInt32 mNumRowWords;
};

//! Class for playing stimuli to a net
/*!
  This wraps a net that needs to be played back.

  Note that expression nets and forcible nets do not get recorded, but
  their subnets do. As a result, force is not recorded as a force, but
  rather a deposit to the storage value and the forcemask.
 */
class ShellNetPlayback : public ShellNetReplay
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlayback(ShellNet* subNet, const Touched& touchBuffer);

  //! Called by mems an non-replayable nets
  ShellNetPlayback(ShellNet* subNet);
  
  virtual ~ShellNetPlayback();
  
  //! Returns this
  virtual const ShellNetPlayback* castShellNetPlayback() const;

  //! Asserts. Playback nets are not released/forced
  virtual CarbonStatus force(const UInt32* buf, CarbonModel* model);
  //! Asserts. Playback nets are not released/forced
  virtual CarbonStatus forceWord(UInt32 buf, int index, CarbonModel* model);
  //! Asserts. Playback nets are not released/forced
  virtual CarbonStatus forceRange(const UInt32* buf, 
                                  int range_msb, int range_lsb, 
                                  CarbonModel* model);
  

  //! Asserts. Playback nets are not released/forced
  virtual CarbonStatus release(CarbonModel* model);
  //! Asserts. Playback nets are not released/forced
  virtual CarbonStatus releaseWord(int index, CarbonModel* model);
  //! Asserts. Playback nets are not released/forced
  virtual CarbonStatus releaseRange(int range_msb, int range_lsb, CarbonModel* model);

  //! Create a playback net for primNet based on this net's storage
  /*!
    This is called when instrumenting a CarbonNet.
    
    All the storage aliases of replayable nets get stored in the
    replay db. Those storage replay nets get created when reading the
    db. Aliases of the storage net get created here.
  */
  virtual ShellNetPlayback* cloneStorage(ShellNet* primNet) = 0;

  //! Assign the model value and drive buffers to the playback net
  /*!
    It is required to call this after construction and before the
    first use of a playback net. The model value buffers are NULL in
    all the playback nets until this is called, and there are no
    checks for nullness during the playback.

    Value and drive must not be NULL for tristates.
    Drive must be NULL for non-tristates.
    Drive can be NULL for response nets. 
  */
  virtual void putModelValueBuffers(UInt32* value, UInt32* drive) = 0;
  virtual void getModelValueBuffers(UInt32** value, UInt32** drive) const = 0;

  //! Get the deposit buffers
  /*!
    For twostates, *drive will be NULL.
    For tristates, both value and drive will be NON-NULL.
    For response nets, both value and drive will be NULL.
  */
  virtual void getDepositBuffers(UInt32** value, UInt32** drive) const = 0;

  //! Get the encoded deposit buffers
  /*!
    Most playback nets don't encode their deposit values, so this
    defaults to calling getDepositBuffers().  Reimplement this if the
    derived class uses encoded deposits.
   */
  virtual void getEncodedDepositBuffers(UInt32** value, UInt32** drive) const;

  //! Get the mask buffer, if it exists
  /*!
    Some nets (specifically those that can represent depositable state
    outputs) have a mask buffer indicating which bits have been
    modified by the API.  If the buffer exists, the derived class must
    reimplement this function.  The default implementation returns
    NULL.
  */
  virtual UInt32* getMaskBuffer() const;

  //! Returns true if the net value has changed from a db read.
  bool isTouched() const { return mTouched != 0; }
  
  //! Set the touched flag to true, also clearing the masked flag
  inline void setTouched(CarbonModel* model) {
    mTouched.set();
    model->getHookup()->setSeenDeposit();
  }

  //! Set isTouched() to be true if changed is 1
  inline void addTouched(UInt32 changed, CarbonModel* model)
  {
    mTouched |= changed;
    model->getHookup()->setSeenDeposit();
  }
  
  //! Sets the masked flag in the touched buffer
  inline void setMasked() {
    mTouched.setMasked();
  }

  //! Set the touched flag to true, without affecting the masked flag
  inline void setTouchedOnly(CarbonModel* model) {
    mTouched.setTouchedOnly();
    model->getHookup()->setSeenDeposit();
  }

  //! Returns true if the net value has been masked
  /*!
    A masked access is one that doesn't change the entire net,
    e.g. carbonDepositRange().
   */
  bool isMasked() const { return mTouched.isSetMasked(); }

  //! Returns the touched indicator
  Touched* getTouchedIndicator() { return &mTouched; }
  
  //! By default, asserts but is overridden by appropriate sub-classes
  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const;
  

  // non-virtual function to get the bitwidth
  UInt32 getWidth() const { return mBitWidth; }

#include "shell/ShellNetMacroDefine.h"
  inline UInt32 getNumWords() const
  {
    return CNUMWORDS((int)mBitWidth - 1, 0);
  }

  //! asserts
  virtual void putToZero(CarbonModel* model);
  //! asserts
  virtual void putToOnes(CarbonModel* model);
  //! asserts
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! asserts
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);
  //! asserts
  virtual bool isDataNonZero() const;
  //! asserts
  virtual void setRawToUndriven(CarbonModel*);

  //! returns false
  virtual bool setToUndriven(CarbonModel* model);  
  //! returns false
  virtual bool resolveXdrive(CarbonModel* model);

  //! Returns true if this is a simple playback clock
  /*!
    Response nets and non-clocks will always return false.
    
    So, a bidirect, which is also a response will return true for the
    deposit side (ShellNetPlaybackBidirectClk), but false for the
    observe side (ShellNetPlaybackResponse).

    "Simple" means that the net being touched implies a clock edge.
    In order to support fake edges, the model's change mask for a
    clock is set whenever a new value is read from the playback
    buffer.  However, this is not true for clocks that are also state
    outputs, because a new playback value doesn't necessarily imply
    an edge.
  */
  virtual bool isSimpleClock() const;

  //! Returns true if the encoded value indicates a change
  /*!
    Some derivations use encoded values to represent, for instance,
    multiple deposits that cause a fake clock edge.  This function
    returns whether the encoded value represents a value change.  The
    default implemenation returns false.
   */
  virtual bool isEncodedValueChanged() const;

protected:
  UInt32 mBitWidth;
  UInt32 mTailMask;
  Touched mTouched;


  //! Given a wrapper ShellNet, get the model value buffers
  /*!
    This is for formatForce.
  */
  static void genericGetModelValueBuffers(ShellNet* wrapperNet, UInt32** value, UInt32** drive);
  //! Does the heavy lifting for formatForce. Only for two state nets
  CarbonStatus twoStateFormatForceHelper(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, const UInt32* modelValue, 
                                         ShellNet* forceMask, UInt32 bitWidth, CarbonModel* model) const;

protected:
  //! for use by non-depositable nets ONLY
  void doReportNotDepositable(CarbonModel* model);

private:
  void init(ShellNet* subNet);
};

//! Dummy class for nets that were not recorded
/*!
  Errors on deposit and examine
*/
class ShellNetPlaybackNotReplayable : public ShellNetPlayback
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackNotReplayable(ShellNet* primNet, CarbonModel* model);
  virtual ~ShellNetPlaybackNotReplayable();

  //! errors
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! errors
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! errors
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const;
  
  //! errors
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel* model) const;
  
  //! errors
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! errors
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! errors
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! complains
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! complains
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! complains
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! asserts
  virtual ShellNetPlayback* cloneStorage(ShellNet* primNet);
  //! asserts
  virtual void putModelValueBuffers(UInt32* value, UInt32* drive);
  //! asserts
  virtual void getModelValueBuffers(UInt32** value, UInt32** drive) const;
  //! asserts
  virtual void getDepositBuffers(UInt32** value, UInt32** drive) const;

  /* memories */
  //! errors
  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  //! errors
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  //! errors
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb);
  
  //! errors
  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;
  //! complains
  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  //! errors
  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;
  
  //! errors
  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  //! errors
  virtual CarbonStatus readmemh(const char* filename);
  //! errors
  virtual CarbonStatus readmemb(const char* filename);
  //! errors
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  //! errors
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);
  
  //! complains
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;

private:
  CarbonModel* mCarbonModel;
};

class ShellNetPlaybackTwoStateWord : public ShellNetPlayback
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackTwoStateWord(ShellNet* subNet, UInt32* depositBuf,
                               UInt32* maskBuf,
                               UInt32 idriveVal, 
                               UInt32 xdriveVal,
                               const Touched& touchBuffer);
  
  virtual ~ShellNetPlaybackTwoStateWord();
  
  //! Reads the nets values and puts into buf
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! Reads the nets values and puts into a real-number buf
  virtual CarbonStatus examine (CarbonReal* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! Examine a word of the value
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! Examine an arbitrary range of the value
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const;

  //! Examine a word of the  xdrive and value
  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  //! Get the external drive.
  virtual void getExternalDrive(UInt32* xdrive) const;
  
  //! Format value
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel* model) const;
  
  //! Format called by force formatting
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel* model) const;
  
  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);


  virtual ShellNetPlayback* cloneStorage(ShellNet* primNet);
  virtual void putModelValueBuffers(UInt32* value, UInt32* drive);
  virtual void getModelValueBuffers(UInt32** value, UInt32** drive) const;
  virtual void getDepositBuffers(UInt32** value, UInt32** drive) const;
  virtual UInt32* getMaskBuffer() const;

  //! Set all bits in the value to 0
  virtual void putToZero(CarbonModel* model);
  //! Set all bits in the value to 1
  virtual void putToOnes(CarbonModel* model);
  //! Set all bits in the range of the value to 1
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! Set all bits in the range of the value to 0
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);

  virtual bool isDataNonZero() const;

  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const;

protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTwoStateWord* instanceMe(ShellNet* subNet);

  //! Pointer to the buffer where the deposited value of the net is stored
  UInt32* mDepositBuf;
  //! Pointer to the buffer where the actual value of the net is stored
  UInt32* mModelValue;
  //! Pointer to the buffer where the mask of the net's access is stored
  UInt32* mMaskBuf;
  //! True if the net owns the storage for the mask buffer
  bool mOwnMaskBuf;
  // The idrive value for this playback net
  const UInt32 mIDriveValue;
  // The xdrive value for this playback net
  const UInt32 mXDriveValue;

  inline void doDepositNoTouch(const UInt32* buf)
  {
    *mDepositBuf = *buf & mTailMask;
    *mModelValue = *mDepositBuf;
  }

private:
  inline void doExamine(UInt32* buf, UInt32* drive,
                        ExamineMode mode) const;
  
  // Does doDepositNoTouch but also sets touched.
  inline void doDeposit(const UInt32* buf, CarbonModel* model);
  inline CarbonStatus doDepositRange(const UInt32* buf, 
                                     int range_msb, int range_lsb,
                                     CarbonModel* model);
  
};

//! Two-state clock
/*!
  Clocks are double buffered so that the shadow value is the previous
  value of the clock so changes can be detected. The touched reference
  only gets set if a clock deposit changes from the last deposit.
*/
class ShellNetPlaybackTwoStateClk : public ShellNetPlaybackTwoStateWord
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackTwoStateClk(ShellNet* subNet, UInt32* depositBuf,
                              UInt32* maskBuf,
                              UInt32 idriveVal, 
                              UInt32 xdriveVal,
                              UInt32* valShadow,
                              const Touched& touchBuffer);
  
  virtual ~ShellNetPlaybackTwoStateClk();
  
  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);


  //! Set all bits in the value to 0
  virtual void putToZero(CarbonModel* model);
  //! Set all bits in the value to 1
  virtual void putToOnes(CarbonModel* model);
  //! Set all bits in the range of the value to 1
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! Set all bits in the range of the value to 0
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);

  //! Returns true
  virtual bool isSimpleClock() const;

protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTwoStateWord* instanceMe(ShellNet* subNet);

  UInt32* mValShadow;

private:
  inline void calcTouched(UInt32 buf, CarbonModel* model);

  inline CarbonStatus doDepositRange(const UInt32* buf, 
                                     int range_msb, int range_lsb,
                                     CarbonModel* model);

  inline void doClkDeposit(const UInt32* buf, CarbonModel* model);
};


//! Class for clocks that errors on examine
class ShellNetPlaybackTwoStateClkWriteOnly : public ShellNetPlaybackTwoStateClk
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackTwoStateClkWriteOnly(ShellNet* subNet, UInt32* depositBuf,
                                       UInt32* maskBuf,
                                       UInt32 idriveVal, 
                                       UInt32 xdriveVal,
                                       UInt32* valShadow,
                                       const Touched& touchBuffer);
  
  virtual ~ShellNetPlaybackTwoStateClkWriteOnly();

  //! errors
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  
  //! errors
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! errors
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const;

  //! errors
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel* model) const;

protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTwoStateWord* instanceMe(ShellNet* subNet);

};

//! Two state clock that is also a state output
/*!
  Clocks that are also state outputs (i.e. depositible statepoints in
  the design) are more difficult than ordinary clocks.  Like ordinary
  clocks, we need to be able to support fake edges.  However, we can't
  just shadow the last deposited value, because the net's value may
  have changed as a result of the model running.

  For example, if we deposit a 1, then the model runs and changes the
  net's value to 0, and then we deposit a 1 again, an edge should be
  detected.  If instead the second deposit was a 0, an edge should not
  be detected.

  The chosen solution is to throw out the idea of shadow value
  comparisons and just record every time this net is touched along
  with the value.  We save the last two deposits so fake edges can be
  detected.

  This is done by encoding multiple deposits in the 32-bit value
  buffer for the net.  Since clocks must be scalars, this isn't a
  problem.  The encoding is described in an enumerated type in the
  ShellNetReplay class.
*/
class ShellNetPlaybackTwoStateClkStateOutput : public ShellNetPlaybackTwoStateWord
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackTwoStateClkStateOutput(ShellNet* subNet, UInt32* depositBuf,
                                         UInt32* maskBuf,
                                         UInt32 idriveVal, 
                                         UInt32 xdriveVal,
                                         const Touched& touchBuffer);
  
  virtual ~ShellNetPlaybackTwoStateClkStateOutput();
  
  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! This class uses encoded deposit buffers, so reimplement this
  virtual void getEncodedDepositBuffers(UInt32** value, UInt32** drive) const;

  //! Set all bits in the value to 0
  virtual void putToZero(CarbonModel* model);
  //! Set all bits in the value to 1
  virtual void putToOnes(CarbonModel* model);
  //! Set all bits in the range of the value to 1
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! Set all bits in the range of the value to 0
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);

  //! Returns false, since state output clocks are not simple
  virtual bool isSimpleClock() const;
  //! Reimplemented to interpret the encoded value buffer
  virtual bool isEncodedValueChanged() const;

protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTwoStateWord* instanceMe(ShellNet* subNet);

private:
  inline void calcTouched(UInt32 buf, CarbonModel* model);

  inline CarbonStatus doDepositRange(const UInt32* buf, 
                                     int range_msb, int range_lsb,
                                     CarbonModel* model);

  inline void doClkDeposit(const UInt32* buf, CarbonModel* model);

  //! Record the current stimulus into the encoded value buffer
  void saveStimulus();

  //! Scratch buffer for parent class to use
  /*!
    In order to use our value encoding, we need a scratch variable for
    the parent class to update when it does deposits, etc.  We can
    then update the real buffer.
   */
  UInt32 mRawValBuf;

  //! The real record buffer pointer
  /*!
    The encoded deposit values will be stored in this buffer, which is
    the one that replay uses for divergence detection and recovery.
   */
  UInt32 *mEncodedValBuf;
};


//! Class that errors on examine
class ShellNetPlaybackTwoStateWordWriteOnly : public ShellNetPlaybackTwoStateWord
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackTwoStateWordWriteOnly(ShellNet* subNet, UInt32* depositBuf,
                                        UInt32* maskBuf,
                                        UInt32 idriveVal, 
                                        UInt32 xdriveVal,
                                        const Touched& touchBuffer);
  
  virtual ~ShellNetPlaybackTwoStateWordWriteOnly();

  //! errors
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  
  //! errors
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! errors
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const;

  //! errors
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel* model) const;

protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTwoStateWord* instanceMe(ShellNet* subNet);
};


//! Class that errors on deposit
class ShellNetPlaybackTwoStateWordReadOnly : public ShellNetPlaybackTwoStateWord
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackTwoStateWordReadOnly(ShellNet* subNet, UInt32* depositBuf,
                                       UInt32* maskBuf,
                                       UInt32 idriveVal, 
                                       UInt32 xdriveVal,
                                       const Touched& touchBuffer);
  
  virtual ~ShellNetPlaybackTwoStateWordReadOnly();

  //! errors
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! errors
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! errors
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! complains
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! complains
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! complains
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTwoStateWord* instanceMe(ShellNet* subNet);
};

class ShellNetPlaybackTwoStateA : public ShellNetPlayback
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackTwoStateA(ShellNet* subNet, UInt32* depositBuf, UInt32* maskBuf, const UInt32* idriveVal, const UInt32* xdriveVal, const Touched& touchBuffer);

  virtual ~ShellNetPlaybackTwoStateA();

  
  //! Reads the nets values and puts into buf
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! Reads the nets values and puts into a real-number buf
  virtual CarbonStatus examine (CarbonReal* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! Examine a word of the value
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! Examine an arbitrary range of the value
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const;

  //! Examine a word of the  xdrive and value
  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  //! Get the external drive.
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! Format value
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel* model) const;
  
  //! Format called by force formatting
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel* model) const;

  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);


  virtual ShellNetPlayback* cloneStorage(ShellNet* primNet);
  virtual void putModelValueBuffers(UInt32* value, UInt32* drive);
  virtual void getModelValueBuffers(UInt32** value, UInt32** drive) const;
  virtual void getDepositBuffers(UInt32** value, UInt32** drive) const;
  virtual UInt32* getMaskBuffer() const;

  // These are needed by force masks

  //! Set all bits in the value to 0
  virtual void putToZero(CarbonModel* model);
  //! Set all bits in the value to 1
  virtual void putToOnes(CarbonModel* model);
  //! Set all bits in the range of the value to 1
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! Set all bits in the range of the value to 0
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);

  virtual bool isDataNonZero() const;

  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const;
protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTwoStateA* instanceMe(ShellNet* subNet);

  //! Pointer to the buffer where the deposited value of the net is stored
  UInt32* mDepositBuf;
  //! Pointer to the buffer where the actual value of the net is stored
  UInt32* mModelValue;
  //! Pointer to the buffer where the mask of the net's access is stored
  UInt32* mMaskBuf;
  //! True if the net owns the storage for the mask buffer
  bool mOwnMaskBuf;
  // The idrive value for this playback net
  const UInt32* mIDriveValue;
  // The xdrive value for this playback net
  const UInt32* mXDriveValue;

private:
  inline void doExamine(UInt32* buf, UInt32* drive,
                        ExamineMode mode) const;
  
  inline void doDeposit(const UInt32* buf, CarbonModel* model);
  inline void doDepositWord(UInt32 buf, int index, CarbonModel* model);
  inline CarbonStatus doDepositRange(const UInt32* buf, 
                                     int range_msb, int range_lsb,
                                     CarbonModel* model);
  
  inline void wordSanitize(UInt32 numWords);
  inline void widthSanitize();
};

//! Class that errors on examine
class ShellNetPlaybackTwoStateAWriteOnly : public ShellNetPlaybackTwoStateA
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackTwoStateAWriteOnly(ShellNet* subNet, UInt32* depositBuf, UInt32* maskBuf, const UInt32* idriveVal, const UInt32* xdriveVal, const Touched& touchBuffer);
  
  virtual ~ShellNetPlaybackTwoStateAWriteOnly();

  //! errors
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  
  //! errors
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! errors
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const;

  //! errors
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel* model) const;

protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTwoStateA* instanceMe(ShellNet* subNet);
};

//! Class that errors on deposit
class ShellNetPlaybackTwoStateAReadOnly : public ShellNetPlaybackTwoStateA
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackTwoStateAReadOnly(ShellNet* subNet, UInt32* depositBuf, UInt32* maskBuf, const UInt32* idriveVal, const UInt32* xdriveVal, const Touched& touchBuffer);
  
  virtual ~ShellNetPlaybackTwoStateAReadOnly();

  //! errors
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);
  
  //! errors
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! errors
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! complains
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! complains
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! complains
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTwoStateA* instanceMe(ShellNet* subNet);

};

class ShellNetTristateA;

//! Tristate playback net
/*! 
  This is the base class of tristate playback nets. Bidirects are
  derived from this class.
*/
class ShellNetPlaybackTristate : public ShellNetPlayback
{
public:
  CARBONMEM_OVERRIDES

  //! Called by the bidirect constructor
  ShellNetPlaybackTristate(ShellNet* subNet, const Touched& touched);

  ShellNetPlaybackTristate(ShellNet* subNet, 
                           UInt32* depositValBuf, 
                           UInt32* depositDrvBuf, 
                           const Touched& touchBuffer);
  
  virtual ~ShellNetPlaybackTristate();
  
  //! Reads the nets values and puts into buf
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  
  //! Reads the nets values and puts into a real-number buf
  virtual CarbonStatus examine (CarbonReal* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! Examine a word of the value
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  
  //! Examine an arbitrary range of the value
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const;

  //! Examine a word of the  xdrive and value
  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  //! Get the external drive.
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! Format value
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel* model) const;
  
  //! Format called by force formatting
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel* model) const;

  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  
  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  
  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  

  virtual ShellNetPlayback* cloneStorage(ShellNet* primNet);
  virtual void putModelValueBuffers(UInt32* value, UInt32* drive);
  virtual void getModelValueBuffers(UInt32** value, UInt32** drive) const;
  virtual void getDepositBuffers(UInt32** value, UInt32** drive) const;

  virtual void setRawToUndriven(CarbonModel* model);
  virtual bool setToUndriven(CarbonModel* model);
  virtual bool setToDriven(CarbonModel* model);
  virtual bool resolveXdrive(CarbonModel* model);

  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const;

protected:
  ShellNetTristateA* mTri;

protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTristate* instanceMe(ShellNet* subNet, UInt32* depositValBuf, UInt32* depositDrvBuf);
  
private:
  inline void doExamine(UInt32* buf, UInt32* drive, ExamineMode mode) const;

  inline void doFastDeposit(const UInt32* buf, 
                            const UInt32* drv,
                            CarbonModel* model);
  
  inline void doFastDepositWord(UInt32 buf, int index,
                                UInt32 drv, CarbonModel* model);
  
  inline CarbonStatus doFastDepositRange(const UInt32* val, 
                                         int range_msb, 
                                         int range_lsb, 
                                         const UInt32* drv,
                                         CarbonModel* model);
};

//! Class that errors on examine
class ShellNetPlaybackTristateWriteOnly : public ShellNetPlaybackTristate
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackTristateWriteOnly(ShellNet* subNet, 
                                    UInt32* depositValBuf, 
                                    UInt32* depositDrvBuf, 
                                    const Touched& touchBuffer);
  
  virtual ~ShellNetPlaybackTristateWriteOnly();

  //! errors
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  
  //! errors
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  
  //! errors
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const;
  
  //! errors
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel* model) const;
protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTristate* instanceMe(ShellNet* subNet, UInt32* depositValBuf, UInt32* depositDrvBuf);
};

//! Class that errors on deposit
class ShellNetPlaybackTristateReadOnly : public ShellNetPlaybackTristate
{
public:
  CARBONMEM_OVERRIDES
  
  ShellNetPlaybackTristateReadOnly(ShellNet* subNet, 
                                   UInt32* depositValBuf, 
                                   UInt32* depositDrvBuf, 
                                   const Touched& touchBuffer);
  
  virtual ~ShellNetPlaybackTristateReadOnly();

  //! errors
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);
  
  //! errors
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! errors
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! complains
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! complains
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! complains
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTristate* instanceMe(ShellNet* subNet, UInt32* depositValBuf, UInt32* depositDrvBuf);

};

//! Bidirect playback net
/*!
  This is different from a tristate in that there is xdata and
  xdrive. Non-bidirects do not have external value components. A
  deposit to a non-bidirect, illegal but allowed, just changes the
  idrive and idata.
*/
class ShellNetPlaybackBidirect : public ShellNetPlaybackTristate
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackBidirect(ShellNet* subNet, 
                           UInt32* depositValBuf, 
                           UInt32* depositDrvBuf,
                           const Touched& touchBuffer);

  virtual ~ShellNetPlaybackBidirect();

  void getExternalDrive(UInt32* xdrive) const;
  virtual ShellNetPlayback* cloneStorage(ShellNet* primNet);
  virtual void putModelValueBuffers(UInt32* value, UInt32* drive);
  virtual void getDepositBuffers(UInt32** value, UInt32** drive) const;
  virtual int hasDriveConflict() const;
  virtual int hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const;

protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTristate* instanceMe(ShellNet* subNet, UInt32* depositValBuf, UInt32* depositDrvBuf);;
};

//! Bidirect clock
/*!
  A bidirect clock change is detected by comparing the last calculated
  deposit to the current calculated value. So, we save the current
  value and drive to their shadows, then deposit, and finally compare
  the resultant value and drive with the shadows. If a change is
  detected, the touch buffer is set.
*/
class ShellNetPlaybackBidirectClk : public ShellNetPlaybackBidirect
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackBidirectClk(ShellNet* subNet, 
                              UInt32* depositValBuf, 
                              UInt32* depositDrvBuf,
                              UInt32* valShadow,
                              UInt32* drvShadow,
                              const Touched& touchBuffer);

  virtual ~ShellNetPlaybackBidirectClk();
  
  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  
  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  
  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  

  virtual bool setToUndriven(CarbonModel* model);
  virtual bool setToDriven(CarbonModel* model);
  virtual bool resolveXdrive(CarbonModel* model);

  //! Returns true
  virtual bool isSimpleClock() const;

protected:
  //! Allocate myself as a new object (used by cloneStorage)
  virtual ShellNetPlaybackTristate* instanceMe(ShellNet* subNet, UInt32* depositValBuf, UInt32* depositDrvBuf);

private:
  //! Pointer to value in mTri
  UInt32* mValBuf;
  //! Pointer to drive in mTri
  UInt32* mDrvBuf;
  
  // shadows of value and drive
  UInt32* mValShadow;
  UInt32* mDrvShadow;

  inline void doClkDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);

  inline CarbonStatus doDepositRange(const UInt32* buf, 
                                     int range_msb, int range_lsb,
                                     const UInt32* drive,
                                     CarbonModel* model);

  inline void saveCurrentValue();
  inline void calcTouched(CarbonModel* model);
};

//! Memory playback net
class ShellNetPlaybackMem : public ShellNetPlayback
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackMem(ShellNet* mem, UInt32 externalIndex, ReplayMemChangeMap* addrChanges, SInt32HashSet* memChanges);
  virtual ~ShellNetPlaybackMem();

  //! asserts
  virtual void putModelValueBuffers(UInt32* value, UInt32* drive);
  //! asserts
  virtual void getModelValueBuffers(UInt32** value, UInt32** drive) const;
  //! asserts
  virtual void getDepositBuffers(UInt32** value, UInt32** drive) const;
  //! asserts
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! asserts
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! asserts
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  //! asserts
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! asserts
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! asserts
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! External api writes
  ReplayMemChangeMap* getWrittenAddrs() const { return mAddrChanges; }

  //! Writes to the playback memory without detection.
  virtual void backDoorWrite(SInt32 address, const DynBitVector* value) = 0;

protected:
  inline void notifyWriteAddress(CarbonMemAddrT address, const UInt32* value);
  inline int formatRow(char* valueStr, size_t len, CarbonRadix strFormat, const UInt32* data) const;
  
  ReplayMemChangeMap* mAddrChanges;
  SInt32HashSet* mMemChanges;
  SInt32 mExternalIndex;
  CarbonModel* mCarbonModel;
};

//! Memory playback with 1 word rows
class ShellNetPlaybackMem1 : public ShellNetPlaybackMem
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackMem1(ShellNet* memNet, UInt32 externalIndex, ReplayMemChangeMap* addrChanges, SInt32HashSet* memChanges, SInt32UInt32Map* memStorage);
  virtual ~ShellNetPlaybackMem1();

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb);
  
  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;
  
  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);
  
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;

  virtual ShellNetPlayback* cloneStorage(ShellNet* primNet);

  virtual void backDoorWrite(SInt32 address, const DynBitVector* value);

protected:
  SInt32UInt32Map* mMem;

private:
  inline UInt32 getRowValue(CarbonMemAddrT address) const;
  inline CarbonStatus notifyReadMem(const char* filename, CarbonRadix radix);
  inline CarbonStatus notifyReadMem(const char* filename, SInt64 startAddr, SInt64 endAddr, CarbonRadix radix, bool checkRange);
};

//! Memory that errors on examines
class ShellNetPlaybackMem1WriteOnly : public ShellNetPlaybackMem1
{
public:
  CARBONMEM_OVERRIDES
  
  ShellNetPlaybackMem1WriteOnly(ShellNet* memNet, UInt32 externalIndex, ReplayMemChangeMap* addrChanges, SInt32HashSet* memChanges, SInt32UInt32Map* memStorage);
  virtual ~ShellNetPlaybackMem1WriteOnly();
  
  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;
  
  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;
  
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;

  virtual ShellNetPlayback* cloneStorage(ShellNet* primNet);
};

//! Memory playback with wide rows
class ShellNetPlaybackMemA : public ShellNetPlaybackMem
{
public:
  CARBONMEM_OVERRIDES

  ShellNetPlaybackMemA(ShellNet* memNet, UInt32 externalIndex, ReplayMemChangeMap* addrChanges, SInt32HashSet* memChanges, SInt32DynBVMap* memStorage);
  virtual ~ShellNetPlaybackMemA();

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb);
  
  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;
  
  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);
  
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;

  virtual ShellNetPlayback* cloneStorage(ShellNet* primNet);

  virtual void backDoorWrite(SInt32 address, const DynBitVector* value);

protected:
  SInt32DynBVMap* mMem;
  
private:
  DynBitVector mZeroValue;

  inline DynBitVector* getRowOrCreate(CarbonMemAddrT address);
  inline const DynBitVector* getRowValue(CarbonMemAddrT address) const;
  inline CarbonStatus notifyReadMem(const char* filename, CarbonRadix radix);
  inline CarbonStatus notifyReadMem(const char* filename, SInt64 startAddr, SInt64 endAddr, CarbonRadix radix, bool checkRange);
};

//! Memory that errors on examines
class ShellNetPlaybackMemAWriteOnly : public ShellNetPlaybackMemA
{
public:
  CARBONMEM_OVERRIDES
  
  ShellNetPlaybackMemAWriteOnly(ShellNet* memNet, UInt32 externalIndex, ReplayMemChangeMap* addrChanges, SInt32HashSet* memChanges, SInt32DynBVMap* memStorage);
  virtual ~ShellNetPlaybackMemAWriteOnly();
  
  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;
  
  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;
  
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;

  virtual ShellNetPlayback* cloneStorage(ShellNet* primNet);
};

//! Object for net responses during playback.
class ShellNetPlaybackResponse : public ShellNetPlayback
{
public:
  CARBONMEM_OVERRIDES

  //! Create a response net without the value and drive buffers
  /*!
    The value and drive buffers are NULL as a result, but they can be
    set later with putModelValueBuffers().
  */
  ShellNetPlaybackResponse(ShellNet* subNet, const Touched& touch);
  
  virtual ~ShellNetPlaybackResponse();
  

  virtual void putModelValueBuffers(UInt32* value, UInt32* drive);
  virtual void getModelValueBuffers(UInt32** value, UInt32** drive) const;
  virtual void getDepositBuffers(UInt32** value, UInt32** drive) const;

  // not allowed on response nets
  //! asserts
  virtual ShellNetPlayback* cloneStorage(ShellNet* primNet);

  //! asserts
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! asserts
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! asserts
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  //! asserts
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! asserts
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! asserts
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

private:
  UInt32* mValBuf;
  UInt32* mDrvBuf;
};

#endif
