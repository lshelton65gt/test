// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONTRISTATEVECTORINPUT_H_
#define __CARBONTRISTATEVECTORINPUT_H_


#ifndef __CARBONTRISTATEVECTOR_H_
#include "shell/CarbonTristateVector.h"
#endif

class CarbonTristateVector1Input : public CarbonTristateVector1
{
public: CARBONMEM_OVERRIDES
  
  CarbonTristateVector1Input(UInt8* idata, UInt8* idrive, 
                             UInt8* xdata, UInt8* xdrive);
  
  CarbonTristateVector1Input(UInt8* idata, UInt8* idrive);
  
  virtual ~CarbonTristateVector1Input();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! ShellNet::isInput()
  virtual bool isInput() const;

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);

  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::deposit()
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
 
  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);
  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);

  virtual void recomputeChangeMask();
  
private:
  inline void doFastDepositInput(const UInt32* buf, const UInt32* drive,
                                 CarbonModel* model);
  inline void doFastDepositWordInput(UInt32 buf, int index,UInt32 drive,
                                     CarbonModel* model);
  inline CarbonStatus doDepositWordInput(UInt32 buf, int index, UInt32 drive,
                                         CarbonModel* model);
  inline CarbonStatus doFastDepositRangeInput(const UInt32* buf, int range_msb,
                                              int range_lsb,  const UInt32* drive,
                                              CarbonModel* model);

  CarbonChangeType* mChangeArrayRef;

  inline void calcChangeMask();
};

class CarbonTristateVector2Input : public CarbonTristateVector2
{
public: CARBONMEM_OVERRIDES
  
  CarbonTristateVector2Input(UInt16* idata, UInt16* idrive, 
                             UInt16* xdata, UInt16* xdrive);
  
  CarbonTristateVector2Input(UInt16* idata, UInt16* idrive);
  
  virtual ~CarbonTristateVector2Input();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! ShellNet::isInput()
  virtual bool isInput() const;

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);

  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::deposit()
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
 
  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);
  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);
  
private:
  inline void doFastDepositInput(const UInt32* buf, const UInt32* drive,
                                 CarbonModel* model);
  inline void doFastDepositWordInput(UInt32 buf, int index,UInt32 drive,
                                     CarbonModel* model);
  inline CarbonStatus doDepositWordInput(UInt32 buf, int index, UInt32 drive,
                                         CarbonModel* model);
  inline CarbonStatus doFastDepositRangeInput(const UInt32* buf, int range_msb,
                                              int range_lsb,  const UInt32* drive,
                                              CarbonModel* model);
  CarbonChangeType* mChangeArrayRef;
};

class CarbonTristateVector4Input : public CarbonTristateVector4
{
public: CARBONMEM_OVERRIDES
  
  CarbonTristateVector4Input(UInt32* idata, UInt32* idrive, 
                             UInt32* xdata, UInt32* xdrive);
  
  CarbonTristateVector4Input(UInt32* idata, UInt32* idrive);
  
  virtual ~CarbonTristateVector4Input();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! ShellNet::isInput()
  virtual bool isInput() const;
  
  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);

  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::deposit()
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
 
  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);
  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);
  
private:
  inline void doFastDepositInput(const UInt32* buf, const UInt32* drive,
                                 CarbonModel* model);
  inline void doFastDepositWordInput(UInt32 buf, int index,UInt32 drive,
                                     CarbonModel* model);
  inline CarbonStatus doDepositWordInput(UInt32 buf, int index, UInt32 drive,
                                         CarbonModel* model);
  inline CarbonStatus doFastDepositRangeInput(const UInt32* buf, int range_msb,
                                              int range_lsb,  const UInt32* drive,
                                              CarbonModel* model);
  CarbonChangeType* mChangeArrayRef;
};

class CarbonTristateVector8Input : public CarbonTristateVector8
{
public: CARBONMEM_OVERRIDES
  
  CarbonTristateVector8Input(UInt64* idata, UInt64* idrive, 
                             UInt64* xdata, UInt64* xdrive);
  
  CarbonTristateVector8Input(UInt64* idata, UInt64* idrive);
  
  virtual ~CarbonTristateVector8Input();
  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! ShellNet::isInput()
  virtual bool isInput() const;

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);

  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::deposit()
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
 
  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);
  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);
  
private:
  inline void doFastDepositInput(const UInt32* buf, const UInt32* drive,
                                 CarbonModel* model);
  inline void doFastDepositWordInput(UInt32 buf, int index,UInt32 drive,
                                     CarbonModel* model);
  inline CarbonStatus doDepositWordInput(UInt32 buf, int index, UInt32 drive,
                                         CarbonModel* model);
  inline CarbonStatus doFastDepositRangeInput(const UInt32* buf, int range_msb,
                                              int range_lsb,  const UInt32* drive,
                                              CarbonModel* model);
  CarbonChangeType* mChangeArrayRef;

};

class CarbonTristateVectorAInput : public CarbonTristateVectorA
{
public: CARBONMEM_OVERRIDES
  
  CarbonTristateVectorAInput(UInt32* idata, UInt32* idrive, 
                             UInt32* xdata, UInt32* xdrive);
  
  CarbonTristateVectorAInput(UInt32* idata, UInt32* idrive);
  
  virtual ~CarbonTristateVectorAInput();
  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! ShellNet::isInput()
  virtual bool isInput() const;

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);

  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::deposit()
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
 
  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);
  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);
  
private:
  inline void doFastDepositInput(const UInt32* buf, const UInt32* drive,
                                 CarbonModel* model);
  inline void doFastDepositWordInput(UInt32 buf, int index,UInt32 drive,
                                     CarbonModel* model);
  inline CarbonStatus doDepositWordInput(UInt32 buf, int index, UInt32 drive,
                                         CarbonModel* model);
  inline CarbonStatus doFastDepositRangeInput(const UInt32* buf, int range_msb,
                                              int range_lsb,  const UInt32* drive,
                                              CarbonModel* model);
  CarbonChangeType* mChangeArrayRef;
};

//! Signed input tristate vector class
/*!
  We have to do our own sign extension when depositing to the
  internals of SBitVector
*/
class CarbonTristateVectorASInput : public CarbonTristateVectorAInput
{
 public:
  CARBONMEM_OVERRIDES

  CarbonTristateVectorASInput(UInt32* idata, UInt32* idrive, 
                             UInt32* xdata, UInt32* xdrive);
  
  CarbonTristateVectorASInput(UInt32* idata, UInt32* idrive);

  virtual ~CarbonTristateVectorASInput();
  
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);
  
  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);
  
  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

 private:
  CarbonTristateVectorASInput();
  CarbonTristateVectorASInput(const CarbonTristateVectorASInput&);
  CarbonTristateVectorASInput& operator=(const CarbonTristateVectorASInput&);
};

#endif
