// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef __CARBONWAVEIMP_H_
#define __CARBONWAVEIMP_H_

// FYI, this file does not need all these guards. It is only included
// by 1 source file, AFAIK - MS

#ifndef __CARBON_SYS_INCLUDE_H__
#include "codegen/SysIncludes.h"
#endif
#ifndef __CARBONWAVE_H_
#include "shell/CarbonWave.h"
#endif
#ifndef __UtString_h_
#include "util/UtString.h"
#endif
#ifndef __CARBONWAVENETASSOC_H_
#include "shell/CarbonWaveNetAssoc.h"
#endif

#include "util/UtStringArray.h"

class CarbonHookup;
class WaveDump;
class CarbonWaveVC;
class CarbonWaveRegistrar;

//! CarbonWave implementation object
/*!
  This object implements the CarbonWave interface.
*/
class CarbonWaveImp : public CarbonWave
{
public: CARBONMEM_OVERRIDES
  //! constructor
  /*!
    \param model An instance of a design. The CarbonModel must be
    valid.
  */
  CarbonWaveImp(CarbonHookup* model);

  //! destructor
  virtual ~CarbonWaveImp();

  //! Initializes the dumper for FSDB format
  /*!
    Sets the data dumper to the dataFile. Can only be called once.
  */
  void putInit(WaveDump* dataFile);


  //! CarbonWave::getTimeScale()
  virtual CarbonTimescale getTimeScale() const;

  //! CarbonWave::getTime()
  virtual CarbonTime getTime() const;

  //! CarbonWave::dumpOn()
  virtual void dumpOn();

  //! CarbonWave::dumpOff()
  virtual void dumpOff();

  //! CarbonWave::dumpAll()
  virtual void dumpAll();

  //! CarbonWave::dumpFlush()
  virtual CarbonStatus dumpFlush();

  //! CarbonWave::dumpVar()
  virtual CarbonStatus dumpVar(CarbonNet* net);

  //! CarbonWave::dumpVars()
  virtual CarbonStatus dumpVars(unsigned int levels, const char* specifier);

  //! CarbonWave::dumpStateIO()
  virtual CarbonStatus dumpStateIO(unsigned int levels, 
                                    const char* specifier);

  //! CarbonWave::dumpSizeLimit()
  /*! 
      Speeds up the process of waveform dumping by limiting the
      process only to the signals for which waveform dumping has
      been enabled and their bit size does not exceed the
      passed limit size. 
  */
  virtual CarbonStatus dumpSizeLimit(UInt32 size);

  //! CarbonWave::closeHierarchy()
  virtual void closeHierarchy();

  //! put user prefix to hierarchy
  virtual CarbonStatus putPrefixHierarchy(const char* prefix, bool replaceDesignRoot);

  //! CarbonWave::addUserData()
  virtual CarbonWaveUserData* addUserData(CarbonVarType netType, 
                                          CarbonVarDirection direction,
                                          CarbonDataType dataType,
                                          int lbitnum, int rbitnum,
                                          CarbonClientData data, 
                                          const char* varName, 
                                          CarbonBytesPerBit bpb,
                                          const char* fullPathScopeName,
                                          const char* scopeSeparator);

  //! CarbonWave::manualUpdateWaveform()
  virtual void manualUpdateWaveform();

  //! Is the hierarchy open?
  /*!
    Open is defined as not having been closed yet; if there is
    no wave file, this will return true.
   */
  bool isHierarchyOpen();

  //! Flush any pending data. Use before destruction
  void flushPendingData();

  //! Update the net values at the current time
  /*!
    This is used by both the scheduled waveform update and the public
    api function that allows users to update the waveform manually
    without running carbonSchedule() again. This is usually a result
    of depositing to signals within a callback, but they want to see
    the new value at the current time.
  */
  void runWaveSchedule();

  //! CarbonWave::getCarbonModel()
  virtual CarbonModel* getCarbonModel();

  //! Are we running the debug schedule when update the waveform?
  bool debugScheduleActive() const;

  //! Manual FSDB switch
  virtual CarbonStatus switchFSDBFile(const char* fileName);

private:
  enum WaveState {
    eDataNone,
    eDataUpdate,
    eDataDumpAll,
    eDataOff,
    eDataOn
  };
 
private:
  CarbonHookup* mHookup;
  WaveDump* mDataFile;
  WaveState mWaveState;
  CarbonWaveVC* mVCEngine;
  CarbonWaveRegistrar* mNetRegistrar;

  UtStringArray mHierPrefix;

  bool mReplaceDesignRoot;


  void waveOff();
  void waveOn();
  void waveAll();
  void waveUpdate();



  CarbonStatus maybeCreateRegistrar();

  void setChangedNets(CarbonWaveNetAssoc::NetWriteFlags flags);

  CarbonStatus extractPaths(const char* specifier,
                            UtStringArray* argNames);

  bool doForceWrite();

  //forbid
  CarbonWaveImp();
  CarbonWaveImp(const CarbonWaveImp&);
  CarbonWaveImp& operator=(const CarbonWaveImp&);
};

#endif
