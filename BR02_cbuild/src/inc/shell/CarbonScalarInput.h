// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONSCALARINPUT_H_
#define __CARBONSCALARINPUT_H_


#ifndef __CARBONSCALAR_H_
#include "shell/CarbonScalar.h"
#endif

//! normal scalar input
class CarbonScalarInput : public CarbonScalar
{
public: CARBONMEM_OVERRIDES
  //! constructor
  CarbonScalarInput(UInt8* addr);
  //! virtual destructor
  virtual ~CarbonScalarInput();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! ShellNet::isInput()
  virtual bool isInput() const;

  //! ShellNet::examineValXDriveWord()
  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  //! ShellNet::examineModelDrive()
  virtual void examineModelDrive(UInt32* driveBuf, ExamineMode mode) const;

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);
 
  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::examine()
  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  //! ShellNet::deposit()
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
 
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
 
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual void recomputeChangeMask(); 
private:
  CarbonChangeType* mChangeArrayRef;
 
  inline void calcChangeMask();
  inline void assignValueInput(const UInt32* buf, CarbonModel* model);
};

#endif
