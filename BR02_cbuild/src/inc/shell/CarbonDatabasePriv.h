// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CARBONDATABASEPRIV_H_
#define CARBONDATABASEPRIV_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

class CarbonDatabase;
class CarbonDatabaseNode;
class IODBRuntime;
class STSymbolTableNode;

//! Class providing privileged access to CarbonDatabase and CarbonDatabaseNode objects
/*!
  Carbon Model Studio (and other internal applications) use the DB API
  to query the design.  This is the same API exposed to customers, so
  that's a good think, because it increases test coverage.  However,
  CMS needs some additional functionality that we don't want to expose
  to the user.

  We could just give those applications access to CarbonDatabase.h and
  CarbonDatabaseNode.h, but that may encourage future development to
  bypass the external DB API.  Instead, this class provides access to
  the specific additional functionality that is needed.

  This is implemented as a class and not C functions (like the rest of
  the DB API) solely to discourage customers from trying to call this
  code.
 */
class CarbonDatabasePriv
{
public:
  CARBONMEM_OVERRIDES

  CarbonDatabasePriv() {}
  ~CarbonDatabasePriv() {}

  static const IODBRuntime* getIODB(const CarbonDatabase* db);
  static const STSymbolTableNode* getSymTabNode(const CarbonDatabaseNode* dbNode);
  static int getNodeIndex(const CarbonDatabaseNode* dbNode);
};


#endif
