// -*- C++ -*-

/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Declaration of global functions for shell and carbon model use.
*/

#ifndef __SHELLGLOBAL_H_
#define __SHELLGLOBAL_H_


#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif
#ifndef _CPP_CSTDDEF
#include <cstddef>
#endif
#ifndef _CPP_CSTDLIB
#include <cstdlib>
#endif

#if pfSPARCWORKS
using std::size_t;
#endif

#ifndef MsgContext_H
#include "util/MsgContext.h"
#endif
#ifndef __OSWrapper_h_
#include "util/OSWrapper.h"     // For OSStdio
#endif

#ifndef __carbon_model_h_
#include "shell/carbon_model.h"
#endif

class MsgContext;
class IODB;
class IODBRuntime;
class CarbonModel;
class CarbonHookup;
class VerilogOutFileSystem;
class HdlOStream;
class HdlIStream;
class ControlHelper;
class UtString;
class UtOStream;
class HDLFileSystem;
class STSymbolTableNode;
class HierName;

namespace ShellGlobal {

  //! Enum for range testing
  enum RangeTestStatus {
    eRangeOK, //!< Successful range test
    eRangeIllegalOrder, //!< msb and lsb are reversed
    eRangeIndexOutOfBounds //!< Index is out of bounds
  };

  //! Maximum directory list length
  const UInt32 cDirListLength = 2047;

  //! Global Function to get a Shell message context
  /*!
    The carbon object can be the design instance or any storage inside
    the instance.
  */
  MsgContext* gCarbonGetMessageContext(CarbonOpaque instance);

  //! Add a message callback 
  /*!
    Register a callback for messages from the shell and the given 
    model instance.
  */
  CarbonMsgCBDataID* gCarbonAddMsgCB(CarbonModel *context,
                                     CarbonMsgCB cbFun, 
                                     CarbonClientData userData);

  //! Remove a message callback 
  /*!
    Remove a callback for messages from the shell and the given 
    model instance.
  */
  void gCarbonRemoveMsgCB(CarbonModel *context, CarbonMsgCBDataID **ppCbData);

  //! Global Function to get a random seed
  /*!
    The carbon object can be the design instance or any storage inside
    the instance.
  */
  SInt32* gCarbonGetRandomSeed(CarbonOpaque instance);

  //! Global Function to get the VerilogFileSystem for the \a instance
  /*!
    The carbon object (argument) can be the design instance or any
    storage inside the instance.
  */
  VerilogOutFileSystem* gCarbonGetVerilogFileSystem(CarbonOpaque instance);

  //! Global Function to get the VhdlOutFileSystem for the \a instance
  /*!
    The carbon object (argument) can be the design instance or any
    storage inside the instance.
  */
  HdlOStream* gCarbonGetVhdlOutFileSystem(CarbonOpaque instance);

  //! Global Function to get the VhdlInFileSystem for the \a instance
  /*!
    The carbon object (argument) can be the design instance or any
    storage inside the instance.
  */
  HdlIStream* gCarbonGetVhdlInFileSystem(CarbonOpaque instance);

  //! Global Function to get the ControlHelper for the \a instance
  /*!
    The carbon object (argument) can be the design instance or any
    storage inside the instance.
  */
  ControlHelper* gcarbonPrivateGetControlHelper(CarbonOpaque instance);

  //! Global Function to add a instance specific context info such as messageContext or VerilogOFileSystem
  /*!
    The message context is in the model.
    The VerilogOFileSystem is associated with the model.
  */
  void gCarbonAddMessageContext(CarbonObjectID*);

  //! Remove the model from the findModelInfo() data structure.
  /*!
    Remove reference to Programmer Error Message Context
  */
  void gCarbonRemoveModel(CarbonObjectID *model);

  //! Return the Programmer Error Message Context
  /*!
    This will create a message context, if needed. It shouldn't because
    the init functions should create the message context for us. But, we
    cannot guarantee that the user will call the init functions.

    If the message context is created in this function
    the ProgErrStream is set to fail after the next report.
  */
  MsgContext* getProgErrMsgr();

  //! Set the shell file path
  /*!
    This applies on a global scale. All shell files will be searched
    in the list of directories.
    \returns eCarbon_ERROR if dirList is too long (> 2047, currently).
  */
  CarbonStatus setFilePath(const char* dirList);

  //! Search for a file in the current file path
  /*!
    This will look for the file given by fileName through each
    directory in the directory search list 
    in order, or just the current directory if setFilePath was not
    called.
    
    If the file is found in the directory list, fileName will be
    changed to reflect the new path.
    
    \returns true if the file was found, false if it wasn't.
  */
  bool searchFilePath(UtString* fileName);

  //! This creates and opens the db for the design specified by model_name
  /*!
    \param model_name Name of the design. It is the same as what
    cbuild produces for \<design\> in with lib\<design\>.a
    \param descr Handle to the generated model
    \param dbFilenameOrBuffer if bufferLen is non-zero, this is the entire
           contents of the desired IODB file.  Otherwise it's the name
           of the IODB file, including the extension
    \param bufferLen the length of the IODB buffer.  If 0, dbFilenameOrBuffer
           is interpreted as the filename
    \param whichDB enum to tell whether to open the full db or the io
    db.
    \returns The database, or NULL if an error occurred.
  */
  
  IODBRuntime* gCarbonCreateDB(CarbonObjectID* descr,
                               const char* dbFilenameOrtBuffer, int bufferLen,
                               CarbonDBType whichDB, UInt32* id);
  
  //! This removes the reference to the database for the instance
  /*!
    No error checking is done.

    The reference to the DB is always removed.  The reference to the
    change array index object is only removed if requested.
  */
  void gCarbonReleaseDB(IODBRuntime** iodb, bool removeChangeIndexRef);

  CarbonModel* newModel(const char* model_name,
    const char* dbFileRoot,
    CarbonDBType whichdb,
    const char* designVersion,
    CarbonObjectID *);

  struct MemoryCallback
  {
    CARBONMEM_OVERRIDES

    MemoryCallback(UInt32 writeIndex)
      : mMemId(NULL), 
        mMemWriteCB(NULL),
        mUserData(NULL), 
        mWriteCBIndex(writeIndex)
    {}
    
    CarbonMemoryID* mMemId;
    
    typedef void (*CarbonMemWriteCB)(CarbonMemoryID* mem,
                                     CarbonClientData userData,
                                     CarbonSInt64 addr);
    
    CarbonMemWriteCB mMemWriteCB;
    CarbonClientData mUserData;
    UInt32 mWriteCBIndex;

    void reportAddress(CarbonSInt64 addr)
    {
      (*mMemWriteCB)(mMemId,
                     mUserData,
                     addr);
    }
  };

  enum MemoryCallbackVersion {
    eMemCBVersion1,
    eMemCBLatest = eMemCBVersion1
  };

  //! Utility function to test a range within given limits
  /*!
    This will use the programmer's error message context.
    \param msb The actual msb
    \param lsb The actual lsb
    \param range_msb The requested msb
    \param range_lsb The requested lsb
    \param model May be NULL. Used for message context.
    \returns whether or not the range is within the actual msb and
    lsb.
  */
  CarbonStatus carbonTestRange(int msb, int lsb,
                               int range_msb, int range_lsb, 
                               CarbonModel* model);

  //! Utility function to test a range within given limits
  /*!
    This uses the supplied msg context
    \param msb The actual msb
    \param lsb The actual lsb
    \param range_msb The requested msb
    \param range_lsb The requested lsb
    \returns whether or not the range is within the actual msb and
    lsb.
  */
  CarbonStatus carbonTestRangeContext(int msb, int lsb,
                                      int range_msb, int range_lsb,
                                      MsgContext* context);

  //! Utility function to test an address range within given limits
  /*!
    This will use the programmer's error message context.
    \param msb The actual most significant address
    \param lsb The actual least significant address
    \param range_msb The requested msa
    \param range_lsb The requested lsa
    \param model May be NULL. Used for msg context
    \param memName Name of memory or readmem file.
    \returns whether or not the range is within the actual msa and
    lsa.
  */
  CarbonStatus carbonTestAddressRange(SInt64 msa, SInt64 lsa,
                                      SInt64 range_msa, SInt64 range_lsa, 
                                      CarbonModel* model,
                                      const HierName* memName);

  //! Reports an open file error using the programmer error context
  /*!
    \warning out \e better be in a bad state; otherwise, a senseless
    error message will be output.
  */
  void reportOutFileOpenError(UtOStream* out);

  //! Reports a close file error using the programmer error context
  /*!
    \warning out \e better be in a bad state; otherwise, a senseless
    error message will be output.
  */
  void reportOutFileCloseError(UtOStream* out);

  //! Report that the drive is attempting to be set on a non-tristate
  /*!
    Returns the status based on the severity of the message.
  */
  CarbonStatus reportSetDriveOnNonTristate(const STSymbolTableNode* name, CarbonModel* model);

  //! Report a deposit to constant attempt
  void reportDepositToConstant(const STSymbolTableNode* name, CarbonModel* model);

  //! Report that a net is not forcible
  void reportNotForcible(const STSymbolTableNode* name, CarbonModel* model);

  //! Report that a net is not depositable
  /*!
    Returns whether or not a net is depositable based on message
    severity.
  */
  bool reportNotDepositable(const STSymbolTableNode* name, CarbonModel* model);
  
  //! Report that a replay net is not observable
  /*!
    Normally, all api-accessible, valid wave nets are observable, even
    if they are sample-scheduled. However, during replay, we can only
    observe nets that were explicitly marked observable (or primary
    i/os).
  */
  void reportNotReplayObservable(const STSymbolTableNode* name, CarbonModel* model);

  //! Report that a replay net is not depositable
  /*!
    While some state points may be depositable in normal mode even if
    they weren't explicitly marked depositable, deposits to them won't
    be recorded by replay.
  */
  void reportNotReplayDepositable(const STSymbolTableNode* name, CarbonModel* model);

  //! Tell user this model was compiled with -profileGenerate, callable from generated code.
  void reportSHLProfileGenerate(bool doit);

  //! Test whether or not the address falls with the given limits
  /*!
    This will use the programmer's error message context.

    \param address Requested address
    \param high_addr Highest specified address
    \param low_addr Lowest specified address
    \param model May be NULL. Used for msg context.
    \param memName Name of memory or readmem file.
    \returns eCarbon_OK if the address is between the high and low,
    inclusive.
  */
  CarbonStatus carbonTestAddress(SInt64 address, SInt64 high_addr, SInt64 low_addr, CarbonModel* model, const HierName* memName);

  //! Test if index falls within low and high
  /*!
    This uses the programmer msg context. 

    \param index Requested index
    \param lowIndex Lowest specified index
    \param highIndex Highest specified index
    \param model May be NULL. used for msg context.
    \returns eCarbon_OK if the index falls within the low and high
    indices.

  */
  CarbonStatus carbonTestIndex(int index, int lowIndex, int highIndex, CarbonModel* model);

  //! Report that the buffer length is insufficient
  /*!
    This uses the programmer msg context. This is needed by
    files that cannot include a MsgContext header.

    \param len Length of insufficient buffer
    \param model May be NULL. Used for msgcontext
  */
  void reportInsufficientBufferLength(size_t len, CarbonModel* model);

  //! Do license heartbeat
  void doHeartbeat();

  //! Interface for change array to storage mapping
  class ChangeIndexStorageMapI
  {
  public: CARBONMEM_OVERRIDES
    //! virtual destructor
    virtual ~ChangeIndexStorageMapI() {}
    //! MapIndex to storage offset
    virtual void mapIndex(SInt32 chIndex, SInt32 storageOffset) = 0;
    //! Get index from storage offset
    virtual SInt32 getIndex(SInt32 storageOffset) const = 0;
  };

  //! Create a map for change array index to storage offset
  ChangeIndexStorageMapI* createChangeIndStorageMap(IODB* db);

  //! Global Function to set the current simulation time.
  /*! This can be slow due to a linear search through all the models to find
  the correct instance, so use with caution. See gCarbonGetTimevarAddr
  */
  void gCarbonSetSimTime( CarbonOpaque instance, const CarbonTime newTime );

  //! Retrieve a pointer to this module's time variable.
  /*! This pointer is referenced primarily in the main model schedule loop for
  efficient updating of the simulation time every time the schedule is
  called.  The solution (hack) is that during carbon_id construction
  this function is called to get the address of the time variable.
  The returned address is stored in the carbon_id object and is used
  to update the time from the schedule in an O(1) fashion.
  The pointer is also used in the generated code with its value
  dereferenced to get the time value for $time and $stime
  */
  CarbonTime* gCarbonGetTimevarAddr( CarbonOpaque instance );

  //! Set the licensing mechanism to block mode if block is true
  /*
    If block is false, this will put the license mechanism back into
    the default mode.
    Calling this also initializes the licensing object. So, you can
    call this before checking out licenses from other apis.
  */
  void setLicenseQueuing(bool block);

  //! This simply checks out the necessary vsp licenses
  /*!
    This will replace the underlying doVSPLicensing when DesignPlayers
    are no longer supported. Currently, this is used by the database
    api.
  */
  bool checkoutVSPLicenses(const IODBRuntime* iodb, const char* dbFileName);

  //! Checkout customer runtime
  bool checkoutRuntimeCustomer(const char* custStr, UtString* licReason);
  bool checkoutSimulationRuntime(const char* custStr, UtString* licReason);

  //! This simply checks for an ARM SoC Designer integration license.
  bool initSocVsp();
  //! This simply checks a platarch license
  bool initPlatArch();
  //! This checks a systemc license
  bool initSystemC();
  //! This checks a modelvalidation license
  bool initModelValidation();
  //! This checks an IP creator license
  bool initPackage();
  //! This checks out a carbon internal license
  bool initModelKits();
  //! This checks a base compiler license
  bool initCompiler();
  //! This checks an AMBA Designer-specific compiler license
  bool initADCompiler();
  //! This checks for (but does not check out) a SoCD SystemC export license
  bool initSoCDSystemCExport();

  //! Return a corresponding CarbonStatus for a msg severity
  CarbonStatus severityToStatus(MsgContextBase::Severity sev);
    
  //! Returns true if the severity corresponds to an error.
  bool severityIsError(MsgContextBase::Severity sev);

  //! Set the currently executing model instance.
  void gSetCurrentModel(CarbonObjectID *);

  //! Get the currently executing model instance.
  CarbonObjectID *gGetCurrentModel();

  typedef UtArray<UInt32> ModelBuckets;

  // Mutex functions to protect common data structures
  //
  // Model creation and destruction (along with some other operations)
  // modify data that is shared across multiple model instances.  For
  // thread safety, a sort of blanket mutex is locked for these
  // operations.
  void lockMutex();
  void unlockMutex();
  void lockDatabaseMutex();
  void unlockDatabaseMutex();

  bool isLocked();
}
#endif
