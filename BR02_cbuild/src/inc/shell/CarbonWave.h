// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONWAVE_H_
#define __CARBONWAVE_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif

class CarbonNet;
class CarbonWaveUserData;
class CarbonModel;

//! Carbon wave dump interface
/*!
  This object is used to dump value changes of a given model to a wave
  file, the format of which is decided by the user.
*/
class CarbonWave
{
public: CARBONMEM_OVERRIDES
#if ! defined(CARBON_EXTERNAL_DOC)
  //! destructor
  virtual ~CarbonWave() = 0;
#endif

  //! Get the timescale value
  virtual CarbonTimescale getTimeScale() const = 0;

  //! Get the last waveform dump time
  /*!
    This can be different from the simulation time in that the
    waveform dumping may be turned off and the simulation is still
    running. This returns that last dump time. If no dump has yet been
    performed it returns 0.
  */
  virtual CarbonTime getTime() const = 0;

  //! Put a user prefix hierarchy to all the names added
  /*!
    \warning This cannot be called once data is being recorded.
    To simplify comparisons of wave files a user may need to add a
    level of hierarchy that does not exist in the generated code.
    \warning Do \e NOT add the path delimiter to the end of the
    prefix. For example, in Verilog, do not pass a path like "a.b.";
    instead, pass just "a.b".

    \returns false if the prefix hierarchy is invalid. True otherwise.
  */
  virtual CarbonStatus putPrefixHierarchy(const char* prefix, bool replaceDesignRoot) = 0;

  //! Set all the values to their current values and resume dumping
  /*!
    dumpVar() or dumpVars() must have been called prior to
    calling this function as well as dumpOff(). All the scopes and
    nets that were registered with dumpvars will be dumped at the
    current time with their current updated values, and each
    successive call of the schedule function will dump any changed
    values.
    
    \warning This takes effect on the next schedule() call.
    \sa dumpVars(), dumpVar(), dumpOff()
  */
  virtual void dumpOn() = 0;

  //! Set all the values to x and stop dumping
  /*!
    dumpVars() or dumpVar() must have been called prior
    to calling function. All the scopes and nets that were registered
    with dumpvars will be dumped at the current time with their
    values as 'x'. There will be no more value dumping on each call of
    schedule() until dumpOn is called. This includes dumpAll().
    
    \warning This takes effect on the next schedule() call.
    \sa dumpVars(), dumpVar(), dumpOn(), dumpAll()
  */
  virtual void dumpOff() = 0;

  //! Dump the current values of all the registered waveform variables
  /*!
    dumpVars() or dumpVar() must have been called prior
    to calling this function. All the scopes and nets that were
    registered with dumpvars will be dumped at the current time with
    their current values, instead of only the nets that have
    changed. If dumpOff() was called prior then this call is ignored.
    
    \warning This takes effect on the next schedule() call.
    \sa dumpVars(), dumpVar(), dumpOn(), dumpOff()
  */
  virtual void dumpAll() = 0;

  //! Flush the waveform file's buffer
  /*!
    \returns eCarbon_OK if no problems were encountered, eCarbon_ERROR
    if there was a problem writing to the file. 
  */
  virtual CarbonStatus dumpFlush() = 0;

  //! Register net for wave dump
  /*!
    This will add the given net to the list of registered nets to dump
    to the waveform file.

    \param net The net to register
    \returns eCarbon_OK if the net is found and no errors were
    encountered, eCarbon_ERROR if there were any problems.
  */
  virtual CarbonStatus dumpVar(CarbonNet* net) = 0;

  //! register scopes/nets traversing specified levels for wave dump
  /*!
    This dumps the values of all the nets listed and/or all the nets
    within the scopes listed that have changed. This happens on each
    call of schedule(). If a scope is specified and 'levels' is not
    zero, all thes nets encountered depth number scopes downward are
    also dumped. If depth is zero all nets for all
    scopes downward are dumped. This function essentially registers
    the list of nets and scopes to be monitored and dumped into a wave
    file.
    \warning At least one scope or signal must be specified.
    \warning This \e cannot be called if data has already been
    recorded.

    \param levels The number of levels to traverse downward beginning
    from each specified scope. If 0, all underlying scopes are
    traversed
    \param specifier A comma separated and/or space separated
    list of scopes and/or nets to dump into a waveform file.
    \returns eCarbon_OK if all specified scopes/nets are found and no
    errors were encountered, eCarbon_ERROR if there were any problems.
  */
  virtual CarbonStatus dumpVars(unsigned int levels, const char* specifier) 
    = 0;

  //! register state and primary i/o scopes/nets traversing specified levels for wave dump
  /*!
    This dumps the values of all the nets listed and/or all the nets
    within the scopes listed that are state, primary i/o, or
    clocks. This happens on each call of schedule(). If a scope is
    specified and 'levels' is not zero, all thes nets encountered
    depth number scopes downward are also dumped. If depth is zero all
    nets for all scopes downward are dumped. This function essentially
    registers the list of nets and scopes to be monitored and dumped
    into a wavefile.
    
    \warning At least one scope or signal must be specified.
    \warning This \e cannot be called if data has already been
    recorded.
    \warning If non-state, non i/o, or non clocks are specified in the
    list a warning message is printed and the relevant signals are
    ignored.
    
    \param levels The number of levels to traverse downward beginning
    from each specified scope. If 0, all underlying scopes are
    traversed
    \param specifier A comma separated and/or space separated
    list of scopes and/or nets to dump into a waveform file.
    \returns eCarbon_OK if all specified scopes/nets are found and no
    errors were encountered, eCarbon_ERROR if there were any problems.
  */
  virtual CarbonStatus dumpStateIO(unsigned int levels, 
                                   const char* specifier) = 0;
    
  //! Limits Waveform Dumping by Signal Size
  /*! 
      Speeds up the process of waveform dumping by limiting the
      process only to the signals for which waveform dumping has
      been enabled and their bit size does not exceed the
      passed limit size. 
  */
  virtual CarbonStatus dumpSizeLimit(UInt32 size) = 0;

  //! Close the wave hierarchy. No more signals can be added.
  virtual void closeHierarchy() = 0;

  //! Add user-defined data to the waveform
  virtual CarbonWaveUserData* addUserData(CarbonVarType netType, 
                                          CarbonVarDirection direction,
                                          CarbonDataType dataType,
                                          int lbitnum, int rbitnum,
                                          CarbonClientData data, 
                                          const char* varName, 
                                          CarbonBytesPerBit bpb,
                                          const char* fullPathScopeName,
                                          const char* scopeSeparator) = 0;

  //! Manually update the waveform 
  /*!
    This will run the deposit/force schedule, if needed, and the debug
    schedule, if needed before updating the waveform.
  */
  virtual void manualUpdateWaveform() = 0;

  //! Get the associated CarbonModel.
  virtual CarbonModel* getCarbonModel() = 0;

  //! Manual FSDB switch
  /*! If this is an FSDB file, it closes the current file and opens a
   *  new file using the provided file name. If the original file was
   *  opened as a normal file, it is closed and the new file is opened
   *  as specified. If the original file was an auto split file, then
   *  this file name is the new prefix and the index is reset to 0.
   *
   */
  virtual CarbonStatus switchFSDBFile(const char* fileName) = 0;
};

#endif
