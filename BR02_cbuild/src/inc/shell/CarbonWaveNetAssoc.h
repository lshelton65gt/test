// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// author: Mark Seneski

#ifndef __CARBONWAVENETASSOC_H_
#define __CARBONWAVENETASSOC_H_


#ifndef __SHELLNET_H_
#include "shell/ShellNet.h"
#endif
#ifndef __CARBONDBREAD_H_
#include "shell/CarbonDBRead.h"
#endif
#ifndef __STSYMBOLTABLENODE_H_
#include "symtab/STSymbolTableNode.h"
#endif

class WaveHandle;
class WaveDump;

class ShellData;
class CarbonDatabase;
class CarbonValueChangeBase;
class CarbonValueChangedBitsIter;

//! Helper class for vcd to CarbonNet association
class CarbonWaveNetAssoc
{
public: CARBONMEM_OVERRIDES
  //! constructor
  CarbonWaveNetAssoc();
  
  //! destructor
  ~CarbonWaveNetAssoc();
  
  enum NetWriteFlags {
    eValNoHandle = 0x1,
    eValWriteVal = 0x2,
    eValUpdate = 0x4
  };

  static bool sDoWriteVal(NetWriteFlags flags) 
  {
    return ((flags & eValWriteVal) != 0);
  }
  
  //! Non pod shadow and wavedump update 
  bool writeIfNotEq(ShellNet::Storage shadowValue);
  
  void maybeUpdateValue(NetWriteFlags flags, WaveDump* waveFile, bool isChanged, bool doFormatIfChanged);

  void setHandle(WaveHandle* handle);
  WaveHandle* getHandle() const { return mHandle; }
  
  void setNet(ShellNet* net);
  ShellNet* getNet() const { return mCarbonNet; }
  
  //! True if the shellnet knows about its aliases
  bool mIsAliased;

  static ShellData* sGetOrCreateShellData(STSymbolTableNode* node);
  static ShellData* sGetOrCreateShellData(CarbonDatabaseNode* node);

  static inline ShellData* sGetShellData(STSymbolTableNode* node)
  {
    ShellData* ret = NULL;
    ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);
    if (bomdata)
      ret = bomdata->getShellData();
    return ret;
  }

protected:
  // needed to create vcd hierarchy
  //! Vcd Handle for this net
  /*!
    For branches, this will be NULL.
  */
  WaveHandle* mHandle;

  //! The CarbonNet
  /*!
    For branches, this will be NULL.
  */
  ShellNet* mCarbonNet;
  
  //! Enumeration for the type of net we have
  enum HandleType {
    eNormal,
    eInteger,
    eReal,
    eTime,
    eInvalid
  };
  HandleType mHandleType;

  //! Net flags.  Only valid for normal nets
  NetFlags mNetFlags;
};

typedef UtArray<CarbonWaveNetAssoc*> NetAssocVec;
typedef Loop<NetAssocVec> NetAssocVecLoop;
typedef CLoop<NetAssocVec> NetAssocVecCLoop;

//! Class to associate a single primitive ShellNet with a list of associations for nets that are bitselects of it
class CarbonWaveNetAssocGroup
{
public:
  CARBONMEM_OVERRIDES

  CarbonWaveNetAssocGroup(const void* storage, UInt32 numAssocs);
  ~CarbonWaveNetAssocGroup();

  void addAssoc(CarbonWaveNetAssoc* assoc, UInt32 index);

  const void* getStorage() const { return mStorage; }
  UInt32 getNumAssocs() const { return mNetAssocs.size(); }

  void maybeUpdateValue(CarbonWaveNetAssoc::NetWriteFlags flags, WaveDump* waveFile, bool isChanged, bool doFormatIfChanged);

  void putVCAndIndex(CarbonValueChangeBase* vc, UInt32 index);

protected:
  const void* mStorage;
  NetAssocVec mNetAssocs;
  CarbonValueChangedBitsIter* mChangedBitsIter;
};

typedef UtArray<CarbonWaveNetAssocGroup*> NetAssocGroupVec;
typedef Loop<NetAssocGroupVec> NetAssocGroupVecLoop;
typedef CLoop<NetAssocGroupVec> NetAssocGroupVecCLoop;

/*
  During performance tuning, I tried sorting the CarbonNets by offset
  as well as the allocation of the shadows. Whether I just sorted the
  CarbonNet handles in each schedule or if I just sorted the shadows
  or both, performance was negatively affected. I left the code in for
  further investigation at some point. To re-enable define
  SORT_BY_OFFSET to be 1 - MS
*/
#define SORT_BY_OFFSET 0

#endif
