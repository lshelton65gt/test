// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONWAVEUSERDATA_H_
#define __CARBONWAVEUSERDATA_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#ifndef __carbon_shelltypes_h_
#include "shell/carbon_shelltypes.h"
#endif

class WaveDump;
class UtString;

//! object that is used as a buffer for user-defined waveform data
class CarbonWaveUserData
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonWaveUserData(WaveDump* fsdbFile, 
                     int lbitnum, int rbitnum,
                     CarbonClientData data, 
                     const char* varName, 
                     CarbonBytesPerBit bpb,
                     const char* fullPathScopeName);

  //! Destructor
  ~CarbonWaveUserData();

  //! Register a change
  void registerChange();

  //! Remove this object from the FsdbFile data structures
  void removeReferences();

  //! For debugging. Get the var name.
  const char* getVarName() const;
  //! For debugging. Get the scope
  const char* getScopeName() const;

  //! Clear out the change field
  void clearChange() { mHasChanged = false; }

  //! Was registerChange() called during this timeslice?
  bool hasChanged() const { return mHasChanged; }

  //! Was registerChange() ever called?
  bool isInit() const { return mIsInit; }

  //! Is the user data active - i.e. has it not been freed by the user?
  bool isActive() const { return mIsActive; }

  //! Get the data buffer
  unsigned char* getBuffer() { return mBuffer; }
  
  //! Get the user value
  CarbonClientData getUserData() { return mUserData; }

  
private:
  WaveDump* mFsdbFile;
  CarbonClientData mUserData;
  CarbonLogicByteType* mBuffer; // same as fsdb's byte_T*
  UInt32 mBufferLen; // length of buffer in bytes
  UtString* mScopeName;
  UtString* mVarName;
  bool mHasChanged;
  bool mIsInit;
  bool mIsActive;
};

#endif
