// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Declaration of ShellMemory class
*/

#ifndef __SHELLMEMORY_H_
#define __SHELLMEMORY_H_


#include "shell/ShellNet.h"
#include "shell/ShellMemoryCreateInfo.h"
#include "shell/carbon_type_create.h" // for CarbonMemallocValFn
#include "util/UtHashMapFastIter.h"

class HDLReadMemX;
class CarbonModel;
class UtOBStream;

//! Shell memory base class
class ShellMemory : public CarbonModelMemory,
                    public ShellNetPrimitive
{
public: 
  CARBONMEM_OVERRIDES
  ShellMemory(SInt32 msb, SInt32 lsb, CarbonMemAddrT hiAddr, CarbonMemAddrT loAddr);
  virtual ~ShellMemory();

  void putCallbackArray(ShellMemoryCBManager* vec) { mCallbacks = vec; }

  virtual ShellMemoryCBManager* getShellMemoryCBManager();

  virtual int getRowBitWidth () const;

  virtual int getRowNumUInt32s () const;

  virtual int getRowLSB () const;
  
  virtual int getRowMSB () const;
  
  virtual CarbonMemAddrT getRightAddr() const;

  virtual CarbonMemAddrT getLeftAddr() const;

  virtual const CarbonMemory* castMemory() const;
  virtual const CarbonModelMemory* castModelMemory() const;

  virtual const ShellNet* castMemToShellNet() const;

  /* shellnet interface -- not meant for memories */
  //! Returns the same as getRowBitWidth
  virtual int getBitWidth () const;

  //! Returns the same as getRowNumUInt32s.
  virtual int getNumUInt32s () const;


  //! returns false
  virtual bool isVector() const;
  //! returns false
  virtual bool isScalar() const;
  //! returns false
  virtual bool isReal() const;
  //! returns false
  virtual bool isTristate() const;
  //! does nothing
  virtual void fastDeposit(const UInt32*, const UInt32*, CarbonModel*);
  //! returns error
  virtual CarbonStatus deposit(const UInt32*, const UInt32*, CarbonModel*);
  //! returns error
  virtual CarbonStatus depositWord(UInt32, int, UInt32, CarbonModel*);
  //! returns error
  virtual CarbonStatus depositRange(const UInt32*, int, int, const UInt32*, CarbonModel*);
  //! returns false
  virtual bool isDataNonZero() const;
  //! returns false
  virtual bool setToDriven(CarbonModel*);
  //! returns false
  virtual bool setToUndriven(CarbonModel*);
  //! returns false
  virtual bool setWordToUndriven(int, CarbonModel*);
  //! returns false
  virtual bool resolveXdrive(CarbonModel*);
  //! does nothing
  virtual void getExternalDrive(UInt32*) const;
  //! returns false;
  virtual bool setRangeToUndriven(int, int, CarbonModel*);
  //! returns error
  virtual CarbonStatus examine(UInt32*, UInt32*, ExamineMode, CarbonModel*) const;
  //! returns error
  virtual CarbonStatus examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const;
  //! returns error
  virtual CarbonStatus examineWord(UInt32*, int, UInt32*, ExamineMode mode, CarbonModel*) const;
  //! returns error
  virtual CarbonStatus examineRange(UInt32*, int, int, UInt32*, CarbonModel*) const;
  //! does nothing
  virtual void fastDepositWord(UInt32, int, UInt32, CarbonModel*);
  //! does nothing
  virtual void fastDepositRange(const UInt32*, int, int, const UInt32*, CarbonModel*);
  //! returns error
  virtual CarbonStatus force(const UInt32*, CarbonModel*);
  //! returns error
  virtual CarbonStatus forceWord(UInt32, int, CarbonModel*);
  //! returns error
  virtual CarbonStatus forceRange(const UInt32*, int, int, CarbonModel*);
  //! returns error
  virtual CarbonStatus release(CarbonModel*);
  //! returns error
  virtual CarbonStatus releaseWord(int, CarbonModel*);
  //! returns error
  virtual CarbonStatus releaseRange(int, int, CarbonModel*);
  //! returns NULL
  virtual Storage allocShadow() const;
  //! does nothing
  virtual void freeShadow(Storage*);
  //! does nothing
  virtual void update(Storage*) const;
  //! returns unchanged
  virtual ValueState compare(const Storage) const;
  //! returns unchanged
  virtual ValueState writeIfNotEq(char*, size_t, Storage*, NetFlags);
  //! returns error
  virtual CarbonStatus format(char*, size_t, CarbonRadix, NetFlags, CarbonModel*) const;
  //! returns false
  virtual bool isForcible() const;
  //! returns false
  virtual bool isInput() const;
  //! returns NULL
  virtual const CarbonVectorBase* castVector() const;
  //! Returns NULL
  virtual const CarbonScalarBase* castScalar() const;
  //! returns NULL
  virtual const ShellNetConstant* castConstant() const;

  //! does nothing
  virtual void putToZero(CarbonModel*);
  //! does nothing
  virtual void putToOnes(CarbonModel*);
  //! returns error
  virtual CarbonStatus setRange(int, int, CarbonModel*);
  //! returns error
  virtual CarbonStatus clearRange(int, int, CarbonModel*);
  //! returns 0
  virtual int hasDriveConflict() const;
  //! returns 0
  virtual int hasDriveConflictRange(SInt32, SInt32) const;
  //! returns error
  virtual CarbonStatus examineValXDriveWord(UInt32*, UInt32*, int) const;
  //! does nothing
  virtual void setRawToUndriven(CarbonModel*);
  //! does nothing
  virtual void getTraits(Traits*) const;
  //! returns unchanged
  virtual ValueState writeIfNotEqForce(char*, size_t, Storage*, NetFlags, ShellNet*);
  //! returns error
  virtual CarbonStatus formatForce(char*, size_t, CarbonRadix, NetFlags, ShellNet*, CarbonModel*) const;
  //! Sets change array reference
  virtual void putChangeArrayRef(CarbonChangeType*);
  //! Returns change array reference
  virtual CarbonChangeType* getChangeArrayRef();
  //! returns NULL
  virtual const UInt32* getControlMask() const;

  //! Calculates the addresses to give to HDLReadMemX
  static void sGetLowHiAddrs(CarbonMemAddrT left, 
                             CarbonMemAddrT right,
                             CarbonMemAddrT* low,
                             CarbonMemAddrT* hi);

protected:
  int formatString(char* valueStr, size_t len, const UInt32* data, CarbonRadix strFormat) const;
  int formatString64(char* valueStr, size_t len, UInt64 data, CarbonRadix strFormat) const;
    
  CarbonMemAddrT mLeftAddr;
  CarbonMemAddrT mRightAddr;
  SInt32 mMsb;
  SInt32 mLsb;

  // For older libdesigns this is NULL.
  ShellMemoryCBManager* mCallbacks;
};

//! Address is an CarbonMemAddrT, row is a UInt8
class ShellMemory64x8 : public ShellMemory
{
public: CARBONMEM_OVERRIDES
  ShellMemory64x8(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, UInt8* mem);
  virtual ~ShellMemory64x8();

  virtual bool isSparse() const;
  virtual void* getStoragePtr(CarbonMemAddrT addr) const;

  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb);

  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);

  virtual void syncPlaybackMem(ShellNetPlaybackMem* playMem) const;

  UInt8 getVal(CarbonMemAddrT address) const;

protected:
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;
  
private:
  UInt8* mMemory;

  void setVal(CarbonMemAddrT address, UInt8 val);

  CarbonStatus readmemfile(HDLReadMemX* readmemx);
};

//! Address is an CarbonMemAddrT, row is a UInt16
class ShellMemory64x16 : public ShellMemory
{
public: CARBONMEM_OVERRIDES
  ShellMemory64x16(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, UInt16* mem);
  virtual ~ShellMemory64x16();

  virtual bool isSparse() const;
  virtual void* getStoragePtr(CarbonMemAddrT addr) const;

  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb);

  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);

  virtual void syncPlaybackMem(ShellNetPlaybackMem* playMem) const;

  UInt16 getVal(CarbonMemAddrT address) const;

protected:
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;
  
private:
  UInt16* mMemory;

  void setVal(CarbonMemAddrT address, UInt16 val);

  CarbonStatus readmemfile(HDLReadMemX* readmemx);
};

//! Address is an CarbonMemAddrT, row is a UInt32
class ShellMemory64x32 : public ShellMemory
{
public: CARBONMEM_OVERRIDES
  ShellMemory64x32(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, UInt32* mem);
  virtual ~ShellMemory64x32();

  virtual bool isSparse() const;
  virtual void* getStoragePtr(CarbonMemAddrT addr) const;

  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb);

  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);

  virtual void syncPlaybackMem(ShellNetPlaybackMem* playMem) const;

  UInt32 getVal(CarbonMemAddrT address) const;

protected:
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;
  
private:
  UInt32* mMemory;

  void setVal(CarbonMemAddrT address, UInt32 val);

  CarbonStatus readmemfile(HDLReadMemX* readmemx);
};

//! Address is an CarbonMemAddrT, row is a UInt64
class ShellMemory64x64 : public ShellMemory
{
public: CARBONMEM_OVERRIDES
  ShellMemory64x64(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, UInt64* mem);
  virtual ~ShellMemory64x64();

  virtual bool isSparse() const;
  virtual void* getStoragePtr(CarbonMemAddrT addr) const;

  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb);

  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);

  virtual void syncPlaybackMem(ShellNetPlaybackMem* playMem) const;

  UInt64 getVal(CarbonMemAddrT address) const;
  
protected:
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;
  
private:
  UInt64* mMemory;

  void setVal(CarbonMemAddrT address, const UInt32* val);
  void setValPrim(CarbonMemAddrT address, UInt64 val);

  CarbonStatus readmemfile(HDLReadMemX* readmemx);
};

//! Address is an CarbonMemAddrT, row is a UInt32 array
/*!
  This can be either sparse or non-sparse.  The read/write function
  pointers handle that abstraction.
 */
class ShellMemory64xA : public ShellMemory
{
public: CARBONMEM_OVERRIDES
  ShellMemory64xA(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, 
                  CarbonMemReadRowFn readRow, CarbonMemWriteRowFn writeRow,
                  CarbonMemReadRowWordFn readRowWord, CarbonMemWriteRowWordFn writeRowWord,
                  CarbonMemReadRowRangeFn readRowRange, CarbonMemWriteRowRangeFn writeRowRange,
                  CarbonMemRowStoragePtrFn rowStoragePtrFn,
                  void* mem);
  virtual ~ShellMemory64xA();

  virtual bool isSparse() const;
  virtual void* getStoragePtr(CarbonMemAddrT addr) const;

  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb);

  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);

  virtual void syncPlaybackMem(ShellNetPlaybackMem* playMem) const;

protected:
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;
  
private:
  CarbonMemReadRowFn mReadRowFn;
  CarbonMemWriteRowFn mWriteRowFn;

  CarbonMemReadRowWordFn mReadRowWordFn;
  CarbonMemWriteRowWordFn mWriteRowWordFn;

  CarbonMemReadRowRangeFn mReadRowRangeFn;
  CarbonMemWriteRowRangeFn mWriteRowRangeFn;

  CarbonMemRowStoragePtrFn mRowStoragePtrFn;
  void* mMemory;

  CarbonStatus readmemfile(HDLReadMemX* readmemx);
};

//! Sparse Memory with 32 bit keys - base class
class ShellSparseMemory32Key : public ShellMemory
{
public: CARBONMEM_OVERRIDES
  ShellSparseMemory32Key(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, void* mem);
  virtual ~ShellSparseMemory32Key();
  
  virtual bool isSparse() const;
  virtual void* getStoragePtr(CarbonMemAddrT addr) const;

protected:
  template<typename DataType>
  const DataType* getRow(CarbonMemAddrT address) const;

  template<typename DataType>
  DataType* getRowOrCreate(CarbonMemAddrT address);

  template<typename DataType>
  void doSyncPlaybackMem(ShellNetPlaybackMem* playMem) const;

private:
  void* mTable;
};

//! Address is an SInt32, row is a UInt8
class ShellSparseMemory32x8 : public ShellSparseMemory32Key
{
public: CARBONMEM_OVERRIDES

  ShellSparseMemory32x8(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, void* mem);
  virtual ~ShellSparseMemory32x8();


  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb);

  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);

  virtual void syncPlaybackMem(ShellNetPlaybackMem* playMem) const;

protected:
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;

private:
  CarbonStatus readmemfile(HDLReadMemX* readmemx);
  void setVal(CarbonMemAddrT address, UInt8 val);
};

//! Address is an SInt32, row is a UInt16
class ShellSparseMemory32x16 : public ShellSparseMemory32Key
{
public: CARBONMEM_OVERRIDES

  ShellSparseMemory32x16(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, void* mem);
  virtual ~ShellSparseMemory32x16();


  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb);

  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);

  virtual void syncPlaybackMem(ShellNetPlaybackMem* playMem) const;

protected:
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;

private:
  CarbonStatus readmemfile(HDLReadMemX* readmemx);
  void setVal(CarbonMemAddrT address, UInt16 val);
};

//! Address is an SInt32, row is a UInt32
class ShellSparseMemory32x32 : public ShellSparseMemory32Key
{
public: CARBONMEM_OVERRIDES

  ShellSparseMemory32x32(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, void* mem);
  virtual ~ShellSparseMemory32x32();


  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb);

  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);

  virtual void syncPlaybackMem(ShellNetPlaybackMem* playMem) const;

protected:
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;

private:
  CarbonStatus readmemfile(HDLReadMemX* readmemx);
  void setVal(CarbonMemAddrT address, UInt32 val);
};

//! Address is an SInt32, row is a UInt64
class ShellSparseMemory32x64 : public ShellSparseMemory32Key
{
public: CARBONMEM_OVERRIDES

  ShellSparseMemory32x64(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, void* mem);
  virtual ~ShellSparseMemory32x64();


  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, int range_msb, int range_lsb) const;

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb);

  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);

  virtual void syncPlaybackMem(ShellNetPlaybackMem* playMem) const;
  
protected:
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;

private:
  CarbonStatus readmemfile(HDLReadMemX* readmemx);

};

#endif
