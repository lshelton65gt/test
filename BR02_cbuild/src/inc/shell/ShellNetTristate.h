// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __SHELLNETTRISTATE_H_
#define __SHELLNETTRISTATE_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif

class ShellNetTristate
{
public: CARBONMEM_OVERRIDES
  virtual ~ShellNetTristate();

  virtual void getExamineValue(UInt32* val, UInt32* drv, size_t width) const = 0;
  virtual bool assign(const UInt32* data, const UInt32* drive, size_t) = 0;

  //! Needed for deassertExternalDriveWord
  virtual bool deassertExternalDrive(size_t) = 0;

  //! Returns false. A non-bidi cannot have a conflict
  virtual bool hasDriveConflict(size_t width) const;
  //! Returns false
  virtual bool hasDriveConflictRange(size_t, size_t, size_t) const;

  //! Implements assign for 1 word prims. Not UInt64 or array
  virtual bool assignWord(UInt32 dataWord, UInt32 driveWord, size_t index, size_t width);

  //! Implements 1 word assignRange. Doesn't work with UInt64, array, bidis
  virtual bool assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t width);

  //! Implements 1 word examine. Doesn't work with UInt64 or array
  virtual void getExamineValueWord(UInt32* valWord, UInt32* drvWord, size_t index, size_t width) const;

  //! Returns true if this tristate is a bidirect
  virtual bool isBidirect() const;

  //! Returns getExamineValue. Bidirects must override this function.
  /*!
    This will work for all tristates including UInt64's and arrays.
  */
  virtual void getCalculatedValue(UInt32* val, UInt32* drv, size_t width) const;
  static size_t sCalcNumWords(size_t width);

  //! Only works for primitives that are UInt32 or less. 
  virtual bool deassertExternalDriveWord(int, size_t);

  // Normal tristates do not have xdrives
  virtual bool resolveExternalDrive(size_t);
};

class ShellNetTristate1 : public ShellNetTristate
{
public: CARBONMEM_OVERRIDES
  ShellNetTristate1(UInt8* data, UInt8* drive);
  virtual ~ShellNetTristate1();

  virtual void setConstantBits(const UInt32* overrideMask, size_t width);

  virtual void getCalculatedValuePrim(UInt8* val, UInt8* drv, size_t width) const;

  virtual bool compareCalcValue(const UInt8* val, const UInt8* drv, size_t width) const;


  UInt8 getIData() const;
  UInt8 getIDrive() const;
  void getExternalValues(UInt32* data, UInt32* drive, size_t) const;

  virtual UInt8 getXDrive(size_t) const;

  virtual void getXValues(UInt8* data, UInt8* drive, size_t) const;

  virtual void undrivenInit(size_t);

  virtual bool assign(const UInt32* data, const UInt32* drive, size_t);


  virtual void getExamineValue(UInt32* val, UInt32* drv, size_t width) const;
  virtual void getExamineValueRange(UInt32* valRange, UInt32* drvRange, size_t index, size_t length, size_t width) const;

  
  virtual bool assertExternalDrive(size_t);
  virtual bool deassertExternalDrive(size_t);
  virtual bool deassertExternalDriveRange(size_t bitIndex, 
                                          size_t rangeLength,
                                          size_t);

  virtual void copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t width) const;

  virtual void getValuePtrs(const UInt8** val, const UInt8** xdrv, const UInt8** idrv) const;

protected:
  UInt8* mIData;
  UInt8* mIDrive;

private:
  ShellNetTristate1();
  ShellNetTristate1(const ShellNetTristate1&);
  ShellNetTristate1& operator=(const ShellNetTristate1&);
};

class ShellNetBidirect1 : public ShellNetTristate1
{
public: CARBONMEM_OVERRIDES
  ShellNetBidirect1(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive);
  virtual ~ShellNetBidirect1();
  virtual void setConstantBits(const UInt32* overrideMask, size_t width);

  virtual void getCalculatedValuePrim(UInt8* val, UInt8* drv, size_t width) const;

  virtual bool assign(const UInt32* data, const UInt32* drive, size_t);
  virtual bool assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t width);

  void copy(const ShellNetTristate1& src, size_t width);

  virtual void getCalculatedValue(UInt32* val, UInt32* drv, size_t width) const;

  virtual bool isBidirect() const;

  virtual bool assertExternalDrive(size_t);
  virtual bool deassertExternalDrive(size_t);
  virtual bool deassertExternalDriveRange(size_t bitIndex, 
                                          size_t rangeLength,
                                          size_t);
  virtual bool resolveExternalDrive(size_t);


  virtual UInt8 getXDrive(size_t) const;
  virtual void getXValues(UInt8* data, UInt8* drive, size_t) const;

  virtual void copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t width) const;

  virtual void getValuePtrs(const UInt8** val, const UInt8** xdrv, const UInt8** idrv) const;

  virtual bool hasDriveConflict(size_t) const;
  virtual bool hasDriveConflictRange(size_t index, size_t length, size_t bitwidth) const;

private:
  UInt8* mXData;
  UInt8* mXDrive;

  ShellNetBidirect1();
  ShellNetBidirect1(const ShellNetBidirect1&);
  ShellNetBidirect1& operator=(const ShellNetBidirect1&);

  // sync internal value to external value and drive
  void syncIX();
  // sync external value to internal value
  void syncXI();
};



class ShellNetTristate2 : public ShellNetTristate
{
public: CARBONMEM_OVERRIDES
  ShellNetTristate2(UInt16* data, UInt16* drive);
  virtual ~ShellNetTristate2();
  virtual void setConstantBits(const UInt32* overrideMask, size_t width);

  virtual void getCalculatedValuePrim(UInt16* val, UInt16* drv, size_t width) const;
  virtual bool compareCalcValue(const UInt16* val, const UInt16* drv, size_t width) const;

  UInt16 getIData() const;
  UInt16 getIDrive() const;
  void getExternalValues(UInt32* data, UInt32* drive, size_t) const;

  virtual UInt16 getXDrive(size_t) const;
  virtual void getXValues(UInt16* data, UInt16* drive, size_t) const;

  virtual void undrivenInit(size_t);
  virtual bool assign(const UInt32* data, const UInt32* drive, size_t);

  virtual void getExamineValue(UInt32* val, UInt32* drv, size_t width) const;
  virtual void getExamineValueRange(UInt32* valRange, UInt32* drvRange, size_t index, size_t length, size_t width) const;
  


  virtual bool assertExternalDrive(size_t);
  virtual bool deassertExternalDrive(size_t);
  virtual bool deassertExternalDriveRange(size_t bitIndex, 
                                          size_t rangeLength,
                                          size_t);

  virtual void copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t width) const;

  virtual void getValuePtrs(const UInt16** val, const UInt16** xdrv, const UInt16** idrv) const;

protected:
  UInt16* mIData;
  UInt16* mIDrive;

private:
  ShellNetTristate2();
  ShellNetTristate2(const ShellNetTristate2&);
  ShellNetTristate2& operator=(const ShellNetTristate2&);
};

class ShellNetBidirect2 : public ShellNetTristate2
{
public: CARBONMEM_OVERRIDES
  ShellNetBidirect2(UInt16* idata, UInt16* idrive, UInt16* xdata, UInt16* xdrive);
  virtual ~ShellNetBidirect2();
  virtual void setConstantBits(const UInt32* overrideMask, size_t width);

  virtual void getCalculatedValuePrim(UInt16* val, UInt16* drv, size_t width) const;


  virtual bool assign(const UInt32* data, const UInt32* drive, size_t);
  virtual bool assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t width);
  void copy(const ShellNetTristate2& src, size_t width);
  virtual void getCalculatedValue(UInt32* val, UInt32* drv, size_t width) const;
  virtual bool isBidirect() const;

  virtual bool assertExternalDrive(size_t);
  virtual bool deassertExternalDrive(size_t);
  virtual bool deassertExternalDriveRange(size_t bitIndex, 
                                          size_t rangeLength,
                                          size_t);
  virtual bool resolveExternalDrive(size_t);


  virtual UInt16 getXDrive(size_t) const;
  virtual void getXValues(UInt16* data, UInt16* drive, size_t) const;
  virtual void copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t width) const;
  virtual void getValuePtrs(const UInt16** val, const UInt16** xdrv, const UInt16** idrv) const;

  virtual bool hasDriveConflict(size_t) const;
  virtual bool hasDriveConflictRange(size_t index, size_t length, size_t bitwidth) const;

private:
  UInt16* mXData;
  UInt16* mXDrive;

  void syncIX();
  void syncXI();

  ShellNetBidirect2();
  ShellNetBidirect2(const ShellNetBidirect2&);
  ShellNetBidirect2& operator=(const ShellNetBidirect2&);
};

class ShellNetTristate4 : public ShellNetTristate
{
public: CARBONMEM_OVERRIDES
  ShellNetTristate4(UInt32* data, UInt32* drive);
  virtual ~ShellNetTristate4();
  virtual void setConstantBits(const UInt32* overrideMask, size_t width);
  virtual void getCalculatedValuePrim(UInt32* val, UInt32* drv, size_t width) const;

  virtual bool compareCalcValue(const UInt32* val, const UInt32* drv, size_t width) const;

  UInt32 getIData() const;
  UInt32 getIDrive() const;
  void getExternalValues(UInt32* data, UInt32* drive, size_t) const;

  virtual UInt32 getXDrive(size_t) const;
  virtual void getXValues(UInt32* data, UInt32* drive, size_t) const;

  virtual void undrivenInit(size_t);

  virtual bool assign(const UInt32* data, const UInt32* drive, size_t);

  virtual void getExamineValue(UInt32* val, UInt32* drv, size_t width) const;
  virtual void getExamineValueRange(UInt32* valRange, UInt32* drvRange, size_t index, size_t length, size_t width) const;
  


  virtual bool assertExternalDrive(size_t);
  virtual bool deassertExternalDrive(size_t);
  virtual bool deassertExternalDriveRange(size_t bitIndex, 
                                          size_t rangeLength,
                                          size_t);

  virtual void copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t width) const;
  virtual void getValuePtrs(const UInt32** val, const UInt32** xdrv, const UInt32** idrv) const;

protected:
  UInt32* mIData;
  UInt32* mIDrive;

private:
  ShellNetTristate4();
  ShellNetTristate4(const ShellNetTristate4&);
  ShellNetTristate4& operator=(const ShellNetTristate4&);
};

class ShellNetBidirect4 : public ShellNetTristate4
{
public: CARBONMEM_OVERRIDES
  ShellNetBidirect4(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive);
  virtual ~ShellNetBidirect4();
  virtual void setConstantBits(const UInt32* overrideMask, size_t width);

  virtual void getCalculatedValuePrim(UInt32* val, UInt32* drv, size_t width) const;


  virtual bool assign(const UInt32* data, const UInt32* drive, size_t);
  virtual bool assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t width);
  void copy(const ShellNetTristate4& src, size_t width);
  virtual void getCalculatedValue(UInt32* val, UInt32* drv, size_t width) const;
  virtual bool isBidirect() const;

  virtual bool assertExternalDrive(size_t);
  virtual bool deassertExternalDrive(size_t);
  virtual bool deassertExternalDriveRange(size_t bitIndex, 
                                          size_t rangeLength,
                                          size_t);
  virtual bool resolveExternalDrive(size_t);


  virtual UInt32 getXDrive(size_t) const;
  virtual void getXValues(UInt32* data, UInt32* drive, size_t) const;
  virtual void copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t width) const;
  virtual void getValuePtrs(const UInt32** val, const UInt32** xdrv, const UInt32** idrv) const;

  virtual bool hasDriveConflict(size_t) const;
  virtual bool hasDriveConflictRange(size_t index, size_t length, size_t bitwidth) const;

private:
  UInt32* mXData;
  UInt32* mXDrive;

  void syncIX();
  void syncXI();

  ShellNetBidirect4();
  ShellNetBidirect4(const ShellNetBidirect4&);
  ShellNetBidirect4& operator=(const ShellNetBidirect4&);
};

class ShellNetTristate8 : public ShellNetTristate
{
public: CARBONMEM_OVERRIDES
  ShellNetTristate8(UInt64* data, UInt64* drive);
  virtual ~ShellNetTristate8();
  virtual void setConstantBits(const UInt32* overrideMask, size_t width);

  virtual void getCalculatedValuePrim(UInt64* val, UInt64* drv, size_t width) const;

  virtual bool compareCalcValue(const UInt64* val, const UInt64* drv, size_t width) const;

  UInt64 getIData() const;
  UInt64 getIDrive() const;
  void getExternalValues(UInt32* data, UInt32* drive, size_t) const;

  virtual UInt64 getXDrive(size_t) const;
  virtual void getXValues(UInt64* data, UInt64* drive, size_t) const;

  virtual void undrivenInit(size_t);
  virtual bool assign(const UInt32* data, const UInt32* drive, size_t);
  virtual bool assignWord(UInt32 dataWord, UInt32 driveWord, size_t index, size_t width);
  virtual bool assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t);

  virtual void getExamineValue(UInt32* val, UInt32* drv, size_t width) const;
  virtual void getExamineValueWord(UInt32* valWord, UInt32* drvWord, size_t index, size_t width) const;

  virtual void getExamineValueRange(UInt32* valRange, UInt32* drvRange, size_t index, size_t length, size_t width) const;
  
  

  virtual bool assertExternalDrive(size_t);
  virtual bool deassertExternalDrive(size_t);
  virtual bool deassertExternalDriveWord(int index, size_t);
  virtual bool deassertExternalDriveRange(size_t bitIndex, 
                                          size_t rangeLength,
                                          size_t);

  virtual void copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t width) const;
  virtual void getValuePtrs(const UInt64** val, const UInt64** xdrv, const UInt64** idrv) const;

protected:
  UInt64* mIData;
  UInt64* mIDrive;

private:
  ShellNetTristate8();
  ShellNetTristate8(const ShellNetTristate8&);
  ShellNetTristate8& operator=(const ShellNetTristate8&);
};

class ShellNetBidirect8 : public ShellNetTristate8
{
public: CARBONMEM_OVERRIDES
  ShellNetBidirect8(UInt64* idata, UInt64* idrive, UInt64* xdata, UInt64* xdrive);
  virtual ~ShellNetBidirect8();
  virtual void setConstantBits(const UInt32* overrideMask, size_t width);

  virtual void getCalculatedValuePrim(UInt64* val, UInt64* drv, size_t width) const;


  virtual bool assign(const UInt32* data, const UInt32* drive, size_t);

  void copy(const ShellNetTristate8& src, size_t width);
  virtual void getCalculatedValue(UInt32* val, UInt32* drv, size_t width) const;

  virtual bool isBidirect() const;

  virtual bool assertExternalDrive(size_t);
  virtual bool deassertExternalDrive(size_t);
  virtual bool deassertExternalDriveWord(int index, size_t);
  virtual bool deassertExternalDriveRange(size_t bitIndex, 
                                          size_t rangeLength,
                                          size_t);
  virtual bool resolveExternalDrive(size_t);


  virtual UInt64 getXDrive(size_t) const;
  virtual void getXValues(UInt64* data, UInt64* drive, size_t) const;
  virtual void copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t width) const;
  virtual void getValuePtrs(const UInt64** val, const UInt64** xdrv, const UInt64** idrv) const;

  virtual bool hasDriveConflict(size_t bitwidth) const;
  virtual bool hasDriveConflictRange(size_t index, size_t length, size_t bitwidth) const;

private:
  UInt64* mXData;
  UInt64* mXDrive;

  void syncIX();
  void syncXI();

  ShellNetBidirect8();
  ShellNetBidirect8(const ShellNetBidirect8&);
  ShellNetBidirect8& operator=(const ShellNetBidirect8&);
};

class ShellNetTristateA : public ShellNetTristate
{
public: CARBONMEM_OVERRIDES
  ShellNetTristateA(UInt32* idata, UInt32* idrive);
  virtual ~ShellNetTristateA();
  virtual void setConstantBits(const UInt32* overrideMask, size_t width);

  //  virtual void getXValues(UInt64* data, UInt64* drive) const;

  virtual void undrivenInit(size_t width);
  virtual bool assign(const UInt32* data, const UInt32* drive, size_t width);
  virtual bool assignWord(UInt32 dataWord, UInt32 driveWord, size_t index, size_t width);
  virtual bool assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t width);

  virtual void getExamineValue(UInt32* val, UInt32* drv, size_t width) const;
  virtual void getExamineValueWord(UInt32* valWord, UInt32* drvWord, size_t index, size_t width) const;

  virtual void getExamineValueRange(UInt32* valRange, UInt32* drvRange, size_t index, size_t length, size_t width) const;
  
  virtual bool compareCalcValue(const UInt32* val, const UInt32* drv, size_t width) const;


  virtual bool assertExternalDrive(size_t width);
  virtual bool deassertExternalDrive(size_t width);
  virtual bool deassertExternalDriveWord(int index, size_t);
  virtual bool deassertExternalDriveRange(size_t bitIndex, 
                                          size_t rangeLength,
                                          size_t);

  virtual void copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t width) const;
  virtual void getValuePtrs(const UInt32** val, const UInt32** xdrv, const UInt32** idrv) const;

  const UInt32* getIData() const;
  const UInt32* getIDrive() const;
  void getExternalValues(UInt32* data, UInt32* drive, size_t) const;

  virtual UInt32 getXDriveWord(size_t index, size_t width) const;

  void getIValues(UInt32* idata, UInt32* idrive, size_t width) const;
  virtual void getXValues(UInt32* xdata, UInt32* xdrive, size_t width) const;

  virtual UInt32 getCalcDriveWord(UInt32 index, size_t width) const;

  virtual void signExtend(UInt32 numWords, UInt32 bitWidth);

  void getIPtrs(UInt32** ival, UInt32** idrv);
  virtual void getAllPtrs(UInt32** xval, UInt32** xdrv, UInt32** ival, UInt32** idrv);

protected:
  // arrays
  UInt32* mIData;
  mutable UInt32* mIDrive;

private:

  //! Inverts the idrive and cleans the dirty bits
  /*!
    This has to be const to allow for examinations.
  */
  void flipIDrive(size_t width) const;


  ShellNetTristateA();
  ShellNetTristateA(const ShellNetTristateA&);
  ShellNetTristateA& operator=(const ShellNetTristateA&);
};

class ShellNetBidirectA : public ShellNetTristateA
{
public: CARBONMEM_OVERRIDES
  ShellNetBidirectA(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive);
  virtual ~ShellNetBidirectA();
  virtual void setConstantBits(const UInt32* overrideMask, size_t width);

  virtual bool assign(const UInt32* data, const UInt32* drive, size_t width);
  virtual bool assignWord(UInt32 dataWord, UInt32 driveWord, size_t index, size_t width);
  virtual bool assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t width);

  void copy(const ShellNetTristateA& src, size_t width);
  virtual void getCalculatedValue(UInt32* val, UInt32* drv, size_t width) const;

  virtual bool isBidirect() const;

  virtual bool assertExternalDrive(size_t);
  virtual bool deassertExternalDrive(size_t);
  virtual bool deassertExternalDriveWord(int index, size_t);
  virtual bool deassertExternalDriveRange(size_t bitIndex, 
                                          size_t rangeLength,
                                          size_t);
  virtual bool resolveExternalDrive(size_t);


  virtual void getXValues(UInt32* data, UInt32* drive, size_t width) const;
  virtual void copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t width) const;
  virtual void getValuePtrs(const UInt32** val, const UInt32** xdrv, const UInt32** idrv) const;

  virtual bool hasDriveConflict(size_t bitwidth) const;
  virtual bool hasDriveConflictRange(size_t index, size_t length, size_t bitwidth) const;

  virtual UInt32 getXDriveWord(size_t index, size_t width) const;

  virtual UInt32 getCalcDriveWord(UInt32 index, size_t width) const;

  virtual void signExtend(UInt32 numWords, UInt32 bitWidth);

  virtual void getAllPtrs(UInt32** xval, UInt32** xdrv, UInt32** ival, UInt32** idrv);

  virtual bool compareCalcValue(const UInt32* val, const UInt32* drv, size_t width) const;

private:
  UInt32* mXData;
  UInt32* mXDrive;

  void syncXI(size_t numWords);
  void syncIX(size_t numWords);
  void cleanXData(size_t numWords, UInt32 mask);

  ShellNetBidirectA();
  ShellNetBidirectA(const ShellNetBidirectA&);
  ShellNetBidirectA& operator=(const ShellNetBidirectA&);
};

#endif
