// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __ON_DEMAND_MGR_H__
#define __ON_DEMAND_MGR_H__

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#include "carbon/carbon_shelltypes.h"
#include "shell/ShellGlobal.h"
#include "shell/OnDemandDeposits.h"
#include "shell/OnDemandState.h"
#include "shell/OnDemandCallback.h"
#include "shell/OnDemandCModel.h"
#include "shell/OnDemandCachedExamines.h"
#include "shell/CarbonModel.h"
#include "shell/carbon_model.h"
#include "shell/ShellMemoryCreateInfo.h"
#include "shell/ShellNet.h"

class ShellNetOnDemand;
class ShellNet;
class OnDemandDebug;
class OnDemandDebugInfo;
class OnDemandModeChangeCB;
class CarbonScalarBase;


//! Management class for onDemand Carbon Model execution
/*!

  This class manages nearly everything related to onDemand execution
  of a Carbon Model, specifically:

  * Configuration of parameters and settings
  * Tracking stimulus and resulting state, and detecting repeating
    patterns
  * Detecting divergence and maintaining design consistency while in a
    repeating pattern
  * Notifying the user of state changes and other events

  Some of this work is delegated to other classes, such as the low
  level deposit/state saving/comparisons, and interfacing to net
  callbacks and cmodels.

  The onDemand manager has four states:

  * Looking - actively searching for a repeating stimulus/state pattern
  * Backoff - backoff period after a failed attempt to find a repeating pattern
  * Idle - in a detected repeating pattern
  * Disabled - temporarily disabled

  When in looking mode, stimulus is saved, the Carbon Model is run, and the
  resulting state is saved.  The stimulus/state pair is compared
  against the previous saved pairs in the sequence.  If a match is
  found, the start/end points are recorded, and idle mode is entered.
  If the maximum number of stimulus/state pairs is reached, or an
  unsupported stimulus is seen, the search is aborted and the backoff
  state is entered.

  When in idle mode, the Carbon Model is not actually run.  The stimulus
  continues to be recorded and is compared against the saved stimulus
  in the pattern.  If there is a stimulus divergence, the Carbon Model is
  restored to the last good state in the sequence, the divergent
  stimulus is applied, and backoff mode is entered.

  Net change callbacks are recorded when in looking mode and played
  back at the appropriate time when idle.  Cmodels have their
  inputs/outputs recorded when looking, and are called when idle,
  handling divergent outputs when necessary.  Examines while idle
  force a state restore (although not a break from idle), but are
  cached to avoid additional restores from that state in the sequence.
 */
class OnDemandMgr
{
public:
  CARBONMEM_OVERRIDES

  //! Construct an onDemand manager for a Carbon Model
  OnDemandMgr(CarbonModel *obj, CarbonUInt32 max_states, CarbonUInt32 backoff_states);
  ~OnDemandMgr();

  //! Finalize initialization of the OnDemand manager
  void finalizeInit();

  //! Set the maximum number of states to track
  void setMaxStates(CarbonUInt32 max_states);

  //! Set backoff strategy
  void setBackoffStrategy(CarbonOnDemandBackoffStrategy strategy);

  //! Set backoff count
  void setBackoffCount(CarbonUInt32 backoff_count);

  //! Set backoff decay percentage
  void setBackoffDecayPercentage(CarbonUInt32 decay_percentage);

  //! Set backoff max decay
  void setBackoffMaxDecay(CarbonUInt32 max_decay);

  //! Register a mode change callback
  CarbonOnDemandCBDataID *addModeChangeCB(CarbonOnDemandModeChangeCBFunc fn, void *userData);

  //! Wrap a normal net to allow onDemand functionality
  ShellNetOnDemand *wrapNet(ShellNet *net);

  //! Return our model pointer
  CarbonModel *getModel() { return mObj; }
  
  //! Return the hookup pointer
  CarbonHookup *getHookup () { return mObj->getHookup (); }

  //! Are we in a repeating state?
  bool isIdle() { return mMode == eCarbonOnDemandIdle; }

  //! What mode are we in?
  CarbonOnDemandMode getMode() { return mMode; }

  //! Record that a non-idle access was seen, invalidating the idle state search
  void setNonIdleAccess(OnDemandDebugInfo *debug_info, CarbonOnDemandDebugType type);

  //! Restore the last known state
  void restoreState(CarbonOnDemandDebugType reason, OnDemandDebugInfo *debug_info);

  //! Add a deposit to an idle deposit net
  void addDeposit(ShellNetOnDemand *net, OnDemandDeposits::NetMapEntry *entry, const CarbonUInt32 *val)
  {
    mCurrDeposits->add(net, entry, val);
  }

  //! Add a net value change callback
  void addNetValueChangeCB(CarbonNetValueCBData *cb);

  //! Register a cmodel instance and context
  void registerCModel(void *cmodel, void *userdata,
                      CarbonUInt32 context_id, const char *name,
                      CarbonUInt32 input_words, CarbonUInt32 output_words,
                      CModelPlaybackFn fn);

  //! Record a cmodel call's previous outputs
  void preRecordCModel(void *cmodel, CarbonUInt32 context_id, CarbonUInt32 *outputs);

  //! Record a cmodel call with inputs and outputs
  void recordCModel(void *cmodel, CarbonUInt32 context_id, CarbonUInt32 *inputs, CarbonUInt32 *outputs);

  //! Get the actual outputs of a cmodel call, provided there was a cmodel divergence
  bool maybeGetCModelCallOutput(CarbonModel::CModelRecoveryStatus *status, void* cmodelData, UInt32 context_id, UInt32** outputs);

  //! Returns whether any cmodel divergences need to be handled
  bool anyCModelDivergence() { return mRestoreCModel != 0; }

  //! Adds a memory callback manager for a design memory
  void addMemoryCB(ShellMemoryCBManager *cb_mgr, const ShellNet *net);

  //! Temporarily disable onDemand
  void disable(bool internal);

  //! Reenable onDemand
  void reEnable(bool internal);

  //! Try to find a cached examine during an idle state
  bool getCachedExamine(const ShellNetOnDemand *net, CarbonStatus *status, UInt32 *val, UInt32 *drive,
                        ShellNet::ExamineMode mode, OnDemandDebugInfo *debug_info, OnDemandCachedExamines::Examine **examine_ptr);

  //! Enable statistics reporting
  void enableStats() { mStatsEnabled = true; }

  //! Enable debug mode and set the reporting level
  void setDebugLevel(CarbonOnDemandDebugLevel level);

  //! Is debug mode enabled?
  bool debugModeEnabled() { return (mDebug != NULL); }

  //! Register a debug event callback
  CarbonOnDemandCBDataID *addDebugCB(CarbonOnDemandDebugCBFunc fn, void *userData);

  //! Attempt to make this net an idle deposit, returning success
  CarbonStatus makeIdleDeposit(CarbonNetID *net);

  //! Attempt exclude this net from state comparisons, returning success
  CarbonStatus exclude(CarbonNetID *net);

  //! Returns current simulation time
  CarbonTime getTime() { return mTime; }

  //! Enable/disable a net change callback
  void setNetCBEnable(CarbonNetValueCBData *cb, bool enabled);

  //! Enable/disable a user callback
  CarbonStatus setUserCBEnable(CarbonOnDemandCBData* cb, bool enable);

  void installEnabledSchedulePointers()
  {
    mScheduleFnEnabled.resetHookupSchedules(getHookup ());
  }

  void installDisabledSchedulePointers()
  {
    mScheduleFnDisabled.resetHookupSchedules(getHookup ());
  }

  void installIdleSchedulePointers()
  {
    mScheduleFnIdle.resetHookupSchedules(getHookup ());
  }

  //! Return the name of the OnDemand idle signal in the top scope of the design
  static const char *sIdleSignalName() { return "$OnDemandIdle"; }

  typedef UtArray<OnDemandDeposits *> DepositArray;
  typedef UtArray<OnDemandState *> StateArray;
  typedef UtArray<ScheduleFnContainer::ScheduleType> SchedTypeArray;

protected:
  CarbonModel *mObj;                                //!< Our Carbon model pointer
  CarbonUInt32 mMaxStates;                          //!< Maximum number of states to track before giving up
  CarbonUInt32 mBackoffStates;                      //!< Number of states to wait after a failure before trying again
  CarbonTime mTime;                                 //!< Last carbonSchedule() time

  OnDemandDepositSet mDepositSet;                   //!< HashSet to store input patterns we're tracking
  OnDemandStateSet mStateSet;                       //!< HashSet to store design state we're tracking
  DepositArray mDepositArray;                       //!< Array of input patterns in our sequence
  StateArray mStateArray;                           //!< Array of states in our sequence
  typedef UtArray<OnDemandCallbackCollection *> CBArray;
  CBArray mCBArray;                                 //!< Array of callbacks triggered by states in our sequence
  typedef UtArray<OnDemandCModelCallCollection *> CModelArray;
  CModelArray mCModelArray;                         //!< Array of cmodel calls run at states in our sequence
  typedef UtArray<OnDemandCachedExamines *> ExamineArray;
  ExamineArray mExamineArray;                       //!< Array of cached examines during idle states
  typedef UtHashMap<ShellNet*, ShellNetOnDemand*> WrapNetMap;
  WrapNetMap mWrappedNets;                          //!< Map of all ShellNets we've already wrapped for onDemand
  SchedTypeArray mSchedArray;                       //!< Array of schedule types called at states in our sequence

  CarbonOnDemandMode mMode;                         //!< Current onDemand mode
  CarbonUInt32 mNumStates;                          //!< Number of potential repeating states being tracked
  CarbonUInt32 mStartState;                         //!< Index of first state in repeating pattern
  CarbonUInt32 mLastState;                          //!< Index of last state in repeating pattern
  CarbonUInt32 mCurrState;                          //!< Current repeating state
  CarbonUInt64 mRepeatCount;                        //!< Number of times we've gone through the repeating pattern
  CarbonUInt32 mNumStateBytes;                      //!< Number of bytes required to store the model's state data
  OnDemandDeposits *mCurrDeposits;                  //!< All the idle deposits applied this cycle
  ScheduleFnContainer::ScheduleType mCurrSched;     //!< Current schedule type requested
  OnDemandCallbackCollection *mCurrCB;              //!< Collection of callbacks triggered this cycle
  OnDemandCModelCallCollection *mCurrCModel;        //!< Collection of cmodel calls occurring this cycle
  OnDemandCModelCallCollection *mRestoreCModel;     //!< Collection of cmodel calls that need to be restored on a divergence
  MemoryCallbackVec mMemCBs;                        //!< Array of memory change callbacks we need to check
  CarbonUInt32 mBackoffTimer;                       //!< Number of schedules before we try looking for a pattern again
  CarbonOnDemandBackoffStrategy mBackoffStrategy;   //!< Currently selected backoff strategy
  UInt32 mBackoffDecayPct;                          //!< Percentage to increase the backoff timer in decaying mode
  UInt32 mBackoffDecayMax;                          //!< Maximum backoff value when in decaying mode
  UInt32 mBackoffDecayMaxMult;                      //!< Maximum backoff multiple when in decaying mode
  UInt32 mBackoffStatesOrig;                        //!< Original backoff value when in decaying mode (same as mBackoffStates in constant mode)
  UInt32 mBackoffDecayNext;                         //!< Next backoff value when in decaying mode
  bool mInitialized;                                //!< Has initialization been completed?
  bool mBackoffRestart;                             //!< Should we restart the backoff timer on every non-idle access?
  bool mNonIdleAccess;                              //!< Has a non-idle access occurred?
  bool mRestored;                                   //!< Has the model already been restored since the last schedule?
  bool mMemCBTriggered;                             //!< Was a memory write callback triggered on the previous schedule call?
  bool mInternallyDisabled;                         //!< Have we been disabled through some internal (e.g. replay) mechanism?
  bool mReEnableAfterSchedule;                      //!< Should we re-enable onDemand after the next schedule call?
  bool mBackoffEntered;                             //!< Did we just enter a backoff state?
  bool mAnyCModels;                                 //!< Are there any cmodels registered?
  bool mNextSetIdlePointers;                        //!< Should we install the idle schedule pointers after the next schedule call?

  OnDemandDepositFactory mDepositFactory;           //!< Factory for OnDemandDeposit objects
  OnDemandStateFactory mStateFactory;               //!< Factory for OnDemandState objects
  OnDemandCallbackFactory mCallbackFactory;         //!< Factory for OnDemandCallback and OnDemandDepositCollection objects
  OnDemandCModelFactory mCModelFactory;             //!< Factory for OnDemandCModelCall and OnDemandCModelCallCollection objects

  bool mStatsEnabled;                               //!< Are stats enabled?
  CarbonUInt64 mSchedulesRequested;                 //!< How many times was a schedule call requested?
  CarbonUInt64 mSchedulesRun;                       //!< How many times did we actually run the schedule?
  CarbonUInt32 mLongestPattern;                     //!< How many states were in the longest pattern we saw?

  OnDemandDebug *mDebug;                            //!< Object to manage debug mode reporting
  OnDemandDebugInfo *mAPIDebugInfo;                 //!< Debug info object for reporting general API events

  typedef UtHashSet<CarbonOnDemandCBData*> UserCBSet;
  UserCBSet mUserCBs;                               //!< All registered user callbacks
  typedef UtArray<OnDemandModeChangeCB*> ModeChangeCBArray;
  ModeChangeCBArray mModeChangeCBs;                 //!< All registered mode change callbacks

  CarbonScalarBase *mIdleNet;                       //!< Net holding the current idle status

  //! Model state save function
  static CarbonUInt32 streamWriteFunc(void *stream, const void *data, const CarbonUInt32 numBytes);
  //! Model state restore function
  static CarbonUInt32 streamReadFunc(void *stream, void *data, const CarbonUInt32 numBytes);

  //! Try to find a repeating state
  void checkpointLooking();
  //! Try to continue in the current repeating pattern
  void checkpointIdle();
  //! Try to match the current set of deposits and state to a previously seen one
  void match();
  //! Clear our state and deposit sets, plus the callback and cmodel arrays
  void clear();
  //! Reset all idle state tracking, possibly entering backoff mode
  void reset(bool enter_backoff);
  //! Try to extend our current repeating state into a longer one
  bool extendRepeatingState();
  //! Auxiliary function for copying saved states
  void duplicateState(UInt32 length);
  //! Set our current mode, possibly triggering a mode change callback
  void setMode(CarbonOnDemandMode newmode);
  //! Process callbacks from the last schedule call
  void processCBs();
  //! Run the saved cmodel calls for a state and return status
  bool runCModels(CarbonUInt32 state);
  //! Check for memory writes during the previous schedule call
  void checkMemCBs();
  //! Enter the configured backoff mode
  void enterBackoff();
  //! Setup debug mode
  void initDebug();
  //! Register a user callback
  void registerUserCB(CarbonOnDemandCBDataID *cb);
  //! Recalculate backoff variables based on new user configuration
  void updateBackoffVars();

  //! onDemand main schedule function for looking/backoff modes
  static CarbonStatus sScheduleEnabled(CarbonObjectID *, CarbonTime t);
  //! onDemand clock schedule function for looking/backoff modes
  static CarbonStatus sClockScheduleEnabled(CarbonObjectID *, CarbonTime t);
  //! onDemand data schedule function for looking/backoff modes
  static CarbonStatus sDataScheduleEnabled(CarbonObjectID *, CarbonTime t);
  //! onDemand async schedule function for looking/backoff modes
  static CarbonStatus sAsyncScheduleEnabled(CarbonObjectID *, CarbonTime t);

  //! onDemand main schedule function for stopped mode
  static CarbonStatus sScheduleDisabled(CarbonObjectID *, CarbonTime t);
  //! onDemand clock schedule function for stopped mode
  static CarbonStatus sClockScheduleDisabled(CarbonObjectID *, CarbonTime t);
  //! onDemand data schedule function for stopped mode
  static CarbonStatus sDataScheduleDisabled(CarbonObjectID *, CarbonTime t);
  //! onDemand async schedule function for stopped mode
  static CarbonStatus sAsyncScheduleDisabled(CarbonObjectID *, CarbonTime t);

  //! onDemand main schedule function for idle mode
  static CarbonStatus sScheduleIdle(CarbonObjectID *, CarbonTime t);
  //! onDemand clock schedule function for idle mode
  static CarbonStatus sClockScheduleIdle(CarbonObjectID *, CarbonTime t);
  //! onDemand data schedule function for idle mode
  static CarbonStatus sDataScheduleIdle(CarbonObjectID *, CarbonTime t);
  //! onDemand async schedule function for idle mode
  static CarbonStatus sAsyncScheduleIdle(CarbonObjectID *, CarbonTime t);

  //! The original schedule function pointers
  ScheduleFnContainer mScheduleFnOrig;
  //! The onDemand schedule function pointers
  ScheduleFnContainer mScheduleFnEnabled;
  ScheduleFnContainer mScheduleFnDisabled;
  ScheduleFnContainer mScheduleFnIdle;

  //! Run a real Carbon Model schedule and checkpoint
  CarbonStatus runScheduleAndCheckpoint(ScheduleFnContainer::ScheduleType type, CarbonModel *model, CarbonTime t);
  //! Do only the idle mode checkpoint
  CarbonStatus runIdleCheckpoint(ScheduleFnContainer::ScheduleType type, CarbonTime t);
  //! Run only the original Carbon Model schedule, bypassing onDemand
  CarbonStatus runOriginalVHMSchedule(ScheduleFnContainer::ScheduleType type, CarbonModel *model, CarbonTime t);

private:
  //! Hide
  OnDemandMgr(const OnDemandMgr&);
  OnDemandMgr &operator=(const OnDemandMgr&);

};

//! Class for user data for memory write callbacks
class OnDemandMemWriteCB
{
public:
  CARBONMEM_OVERRIDES

  OnDemandMemWriteCB(bool *trigger_ptr,
                     bool *initPtr,
                     OnDemandDebug **debug_ptr,
                     OnDemandDebugInfo *my_debug_info,
                     const ShellNet *net)
    : mTriggerPtr(trigger_ptr),
      mInitPtr(initPtr),
      mDebugPtr(debug_ptr),
      mMyDebugInfo(my_debug_info),
      mNet(net)
  {}

  ~OnDemandMemWriteCB();

  //! Update the OnDemandMgr triggered flag, and possibly the debug info
  void setTriggered();

  //! Check if the OnDemandMgr has been initialized
  bool isInitialized() const { return *mInitPtr; }

  bool *mTriggerPtr;                     //!< Pointer to the OnDemandMgr memory write trigger
  bool *mInitPtr;                        //!< Pointer to the OnDemandMgr's initialized flag
  OnDemandDebug **mDebugPtr;             //!< Pointer to the OnDemandDebug object pointer
  OnDemandDebugInfo *mMyDebugInfo;       //!< Debug info for this memory   
  const ShellNet *mNet;                  //!< The ShellNet for the memory
};

//! Class to manage OnDemand user callbacks
/*!
  This base class provides the user interface to OnDemand callbacks,
  allowing them to be enabled/disabled.  The real
  content/implementation of the specific callback type must be
  implemented in a derived version.  The base class exists so that the
  user doesn't have to deal with a different handle type for each
  unique callback type.
 */
class CarbonOnDemandCBData
{
public:
  CARBONMEM_OVERRIDES

  CarbonOnDemandCBData() { enable(); }

  virtual ~CarbonOnDemandCBData() {}

  //! Enable the callback
  void enable() { mEnabled = true; }

  //! Disable the callback
  void disable() { mEnabled = false; }

  //! Is the callback enabled?
  bool isEnabled() { return mEnabled; }

private:
  bool mEnabled;
};

//! Class to represent a mode change callback function
class OnDemandModeChangeCB : public CarbonOnDemandCBData
{
public:
  CARBONMEM_OVERRIDES

  OnDemandModeChangeCB(CarbonOnDemandModeChangeCBFunc fn, void *userData);
  virtual ~OnDemandModeChangeCB();

  //! Runs the callback function, if enabled, with the given mode change data
  void run(CarbonObjectID* context,
           CarbonOnDemandMode from,
           CarbonOnDemandMode to,
           CarbonTime simTime);

protected:
  CarbonOnDemandModeChangeCBFunc mCBFunc;           //!< Mode change callback function pointer
  void *mCBData;                                    //!< Mode change callback data
};


#endif
