// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONHOOKUP_H_
#define __CARBONHOOKUP_H_

#ifndef __CARBON_SYS_INCLUDE_H__
#include "codegen/SysIncludes.h"
#endif
#ifndef __SHELLGLOBAL_H_
#include "shell/ShellGlobal.h"
#endif


#ifndef __carbon_model_h_
#include "shell/carbon_model.h"
#endif

#ifndef __carbon_model_private_h_
#include "shell/carbon_model_private.h"
#endif

class CarbonWaveImp;
class CarbonWave;
class IODBRuntime;
class STAliasedLeafNode;
class STSymbolTableNode;
class ShellNet;
class CarbonModel;
class UtString;
class ESFactory;
class CarbonExamineScheduler;
class ShellData;
class MsgContext;
class ControlHelper;
class UtOCheckpointStream;
class UtICheckpointStream;
class ScheduleFnContainer;

#ifdef INTERPRETER
class Interpreter;
#endif

//! Carbon shell to design hookup object
/*!
  This handles the internal sinews of logically linking the debug
  shell to the generated design.
*/
class CarbonHookup
{
public: CARBONMEM_OVERRIDES

  //! constructor
  CarbonHookup (UInt32 id, CarbonObjectID *, CarbonModel *);

  //! destructor
  virtual ~CarbonHookup();
  
  //! Place the design's destroy function
  void putDestroyFn(::CarbonCoreDestroyFn fn);

  //! Get the design's destroy function
  ::CarbonCoreDestroyFn getDestroyFn() { return mDestroyFn; }

  //! Place the design's schedule function
  void putScheduleFn(::CarbonCoreScheduleFn fn);

  //! Place the design's clk schedule function (noInputFlow)
  void putClkScheduleFn(::CarbonCoreScheduleFn fn);

  //! Place the design's data schedule function (noInputFlow)
  void putDataScheduleFn(::CarbonCoreScheduleFn fn);

  //! Place the design's initialize function
  void putInitializeFn(::CarbonCoreInitializeFn fn);

  //! Place the design's mode change function
  void putModeChangeFn(CarbonModeChangeCBFunc modeChangeFn);
  
  //! Place the design's async schedule function
  void putAsyncScheduleFn(::CarbonCoreScheduleFn fn);

  //! Place the design's debug schedule function
  void putDebugScheduleFn(::CarbonSimpleScheduleFn fn);

  //! Place the design's deposit combo function
  void putDepositComboScheduleFn(::CarbonSimpleScheduleFn fn);

  //! Place the design's simulation over function
  void putSimulationOverFn(::CarbonSimulationOverFn fn);

  //! Creates the CarbonWave, if needed
  CarbonWaveImp* createWave();
  
  //! Gets the current CarbonWaveImp pointer
  CarbonWaveImp* getWave();

  //! Closes the current CarbonWave, if it exists
  void closeWave();
  
  //! Place the db in this object
  void putDB(IODBRuntime* db);

  //! Stop calling the waveform schedule
  void deactivateWave()
  { set_debug_callback (NULL); }

  //! Start calling the waveform schedule again
  void activateWave();

  //! Return the change array size
  inline SInt32 getChangeArraySize () const { return mChangeArraySize; }

  //! Return the change array
  inline CarbonChangeType *getChangeArray () { return mChangeArray; }

  //! Get the IODB
  const IODBRuntime* getDB() const;
  //! Get a modifiable IODB
  IODBRuntime* getDB();

  //! Get the IODB base class
  /*!
    Needed for a wavetestbench test,and I don't want to include
    IODBRuntime in the bom
  */
  const IODB* getBaseDB() const;
  //! Modifiable
  IODB* getBaseDB();

  //! Return a CarbonNet for the given leaf
  ShellNet* getCarbonNet(STAliasedLeafNode* leaf);

  // change array mapping functions
  
  //! Place an already processed map into the hookup
  void putChangeArrayToStorageMap(ShellGlobal::ChangeIndexStorageMapI* indMap);
  
  //! Function to begin mapping change array to storage
  /*!
    This will do any needed munging prior to opening the change index
    to storage map for insertions.
   */
  void beginChangeIndToStorageMap();

  //! Insert a mapping of a change array index to a storage offset
  void mapChangeIndToStorage(SInt32 chIndex, SInt32 storageOffset);

  //! Function to end mapping change array to storage
  /*!
    This will close the map and add it to the global db manager so
    other instances can use it.
  */
  void endChangeIndToStorageMap();
  
  //! Called by the design to run the wave schedule
  static void sRunWaveSchedule(CarbonObjectID *descr);

  //! Get the CarbonModel's id
  UInt32 getId() const;

  //! Run any hookup modifications needed post schedule call
  /*!
    This will clear the change registry and fix up bidirects.
  */
  //  void postScheduleCleanup();

  //! Get the associated CarbonModel
  CarbonModel* getCarbonModel();

  //! Frees the net (decrs refcount, cleans up).
  void freeNet(ShellNet** net);

  //! Return the change array index associated with node
  /*!
    If no change array index is associated with the node then -1 is
    returned.
  */
  SInt32 getChangeArrayIndex(const STAliasedLeafNode* node) const;

  //! Return a pointer to the entry in the change array at index
  /*!
    \warning validChgArrIndex must pertain to an actual entry in the
    change array.
  */
  CarbonChangeType* getChangeArrayEntry(SInt32 validChgArrIndex);

  //! Returns the boolean value in the change array
  /*!
    \warning validChgArrIndex must pertain to an actual entry in the
    change array.
  */
  bool getIsChanged(SInt32 validChgArrIndex) const;

  //! True if the node is a leaf node and is constant
  bool isConstant(const STSymbolTableNode* node) const;

  //! True if the node will create a valid CarbonNet
  bool hasValidCarbonNet(const STAliasedLeafNode* node) const;

  //! Get this instances expression factory
  ESFactory* getExprFactory();

  //! Get the value change examination scheduler
  CarbonExamineScheduler* getCarbonExamineScheduler();

  //! Get the ShellData associated with the node
  /*!
    Returns NULL if a shelldata structure was not created yet.
  */
  ShellData* getShellData(STAliasedLeafNode* node);

  //! Composes the net/module name for symNode
  void getNodeName(UtString* name, const STSymbolTableNode* symNode) const;

  //! Update the waveform at the current time.
  void updateWaveform();

  //! Run the deposit/force schedule, if needed
  /*!
    This is currently only used to update the waveform at the current
    time without calling carbonSchedule(). This is to propagate a
    deposit through combination logic.
  */
  void maybeRunDepositSchedule();

  //! Returns true if waveform dumping is active
  bool isWaveActive() const
  { return get_debug_callback () != NULL; }

  //! Run the design's schedule function
  //! Run the design's schedule function
  virtual CarbonStatus runSchedule(CarbonTime simTime)
  {
    return (*mScheduleFn)(mDescr, simTime);
  }

  //! Run the design's clk schedule function
  virtual CarbonStatus runClkSchedule(CarbonTime simTime)
  {
    return (*mClkScheduleFn)(mDescr, simTime);
  }

  //! Run the design's data schedule function
  virtual CarbonStatus runDataSchedule(CarbonTime simTime)
  {
    return (*mDataScheduleFn)(mDescr, simTime);
  }

  //! Run the design's async schedule function
  virtual CarbonStatus runAsyncSchedule(CarbonTime simTime)
  {
    return (*mAsyncScheduleFn)(mDescr, simTime);
  }

  //! Run the design's mode change function
  void runModeChangeFn(CarbonVHMMode prev, CarbonVHMMode next)
  {
    (*mModeChangeFn)(mDescr, NULL, prev, next);
  }

  //! Get the schedule function pointer
  ::CarbonCoreScheduleFn getScheduleFn() const { 
    return mScheduleFn;
  }

  //! Get the schedule function pointer
  CarbonCoreScheduleFn getClkScheduleFn() const { 
    return mClkScheduleFn;
  }

  //! Get the schedule function pointer
  CarbonCoreScheduleFn getDataScheduleFn() const { 
    return mDataScheduleFn;
  }

  //! Get the schedule function pointer
  ::CarbonCoreScheduleFn getAsyncScheduleFn() const { 
    return mAsyncScheduleFn;
  }

  //! Returns and resets the boolean of whether or not nets were deposited
  /*!
    The flag is reset to the initial value which is determined by the
    isPortInterfaceGenerated boolean in the IODB. If the port
    interface is generated the initial value is true. Otherwise, it is
    false. This is due to the port interface NOT using the api, and
    thus the 'depositSeen' flag would never get set. The initial value is
    also affected by the CARBON_NO_SCHEDULE_COMPRESS environmen
    variable which overrides the initial value to be true.
  */
  inline bool resetSeenDeposit() { 
    bool seenDeposit = mSeenDeposit;
    mSeenDeposit = mSeenDepositInitValue;
    return seenDeposit;
  }

  //! Sets the boolean to true. We have seen a deposit.
  inline void setSeenDeposit() {
    mSeenDeposit = true;
  }

  void addRunDepositComboSched (bool runit);

  //! Locate storage within the model
  void *findNetStorage (int offset);

  //! Construct a ShellNet
  ShellNet *makeShellNet (UInt32, void *, bool);

  //! Set the debug callback
  void set_debug_callback (CarbonShellCallBackFn f);

  //! Get the debug callback
  CarbonShellCallBackFn get_debug_callback () const;

  //! \return the message context
  MsgContext *getMsgContext ();

  //! \return a handle used for the model finder
  CarbonOpaque getHandle () const { return mDescr->mHdl; }

  //! \return the init flags for the model
  CarbonInitFlags getInitFlags () const;

  //! Set the init flags for the model
  void putInitFlags (int flags);

  //! \return the unique identifier for the design
  const char *getUID () const;

  //! Call the simOverFn
  void callSimOver ();

  //! Call the destroyFn
  void callDestroy ();

  //! Call the initialisation function
  void callInit (void *, void *, void *);

  //! Call the debug_schedule method
  void call_debug_schedule ();

  //! \return the ControlHelper instance for the design
  ControlHelper *getControlHelper ();

  bool checkAndClearRunDepositComboSchedule ();
  void call_deposit_combo_schedule ();

  //! Get the simulation time
  UInt64 getTime (void) const;

  //! Set simulation time
  bool setTime (UInt64);

  //! save simulation time, schedule count, model state, etc. 
  bool save(UtOCheckpointStream&, CarbonModel *);

  //! restore simulation time, schedule count, model state, etc. 
  bool restore(UtICheckpointStream&, CarbonModel *);

  //! \class ChangeArrayDescr
  /*! This class is used to save and restore the value of the change array.
   */
  class ChangeArrayDescr {
  public: CARBONMEM_OVERRIDES
    friend class CarbonHookup;
    ChangeArrayDescr ();
    virtual ~ChangeArrayDescr ();
  private:
    CarbonChangeType *mSaved;
    CarbonChangeType mSavedInit;
  };

  void saveChangeArray (ChangeArrayDescr &);
  void restoreChangeArray (ChangeArrayDescr &);
  void setChangeArray (CarbonChangeType value);
  CarbonChangeType getInit () const;
  void setInit (CarbonChangeType value) const;


  //! onDemand version of simSave, called from OnDemandMgr
  bool simSaveOnDemand (UtOCheckpointStream&, CarbonModel *);

  //! onDemand version of simRestore, called from OnDemandMgr
  bool simRestoreOnDemand (UtICheckpointStream&, CarbonModel *);
  
  //! \return a handle on the simulation
  CarbonObjectID *getObjectID ()
  { return mDescr; }

  //! Construct a suitable ScheduleFnContainer
  ScheduleFnContainer *createScheduleFnContainer () const;
  
  //! Initialise the ScheduleFnContainer from this hookup
  void init (ScheduleFnContainer *) const;

private:
#ifdef INTERPRETER
  // Hook to use the interpreter rather than code-generated schedule
  static CarbonStatus interpreterSchedule(CarbonModel* model, CarbonTime);
#endif

  CarbonObjectID *mDescr;
  carbon_model_private *mPrivate;
  CarbonWaveImp* mWave;
  IODBRuntime* mDB;
  CarbonModel* mModel;
  ShellGlobal::ChangeIndexStorageMapI* mIndStoreMap;
  CarbonModeChangeCBFunc mModeChangeFn;
  CarbonCoreDestroyFn mDestroyFn;
  CarbonCoreScheduleFn mScheduleFn;
  CarbonCoreInitializeFn mInitializeFn;
  CarbonSimulationOverFn mSimulationOverFn;
  CarbonCoreScheduleFn mClkScheduleFn;
  CarbonCoreScheduleFn mDataScheduleFn;
  CarbonCoreScheduleFn mAsyncScheduleFn;
  CarbonSimpleScheduleFn mDebugScheduleFn;
  CarbonSimpleScheduleFn mDepositComboScheduleFn;
  UInt32 mId;

#ifdef INTERPRETER
  Interpreter* mInterpreter;
#endif

  ESFactory* mExprFactory;
  CarbonExamineScheduler* mExamineSched;
  
  //! Dummy change array reference
  /*!
    All ShellNets with input characteristics need a change array
    reference that gets updated when the net is deposited.  Normally,
    the net has an offset reserved for it in the generated model's
    change array.  However, not all ShellNets have an offset.  Those
    that don't still need a valid pointer to write.  Since the value
    is never used anywhere, one dummy entry can be shared among all
    such nets.
   */
  CarbonChangeType mDummyChangeArrayRef;
  
  // This gets set whenever a net is deposited to. This allows us to
  // skip schedule calls that have no deposits associated with them.
  bool mSeenDeposit;
  // This gets set if either -systemCWrapper or -createPortInterface
  // is used on the cbuild command line. It may also be set due to an
  // old carbon compile.
  bool mSeenDepositInitValue;

  // Class that deals with creating the subnets of an expression
  class ExprNetCreate;

  void clearDB();
  void deleteNet(STSymbolTableNode* node);

  CarbonChangeType *mChangeArray;
  SInt32 mChangeArraySize;

protected:

  //! Delete all the shell nets in mDB->getDesignSymbolTable ()
  void trashDesignSymbolTable ();

};

#endif
