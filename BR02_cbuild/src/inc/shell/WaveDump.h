// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __WAVEDUMP_H_
#define __WAVEDUMP_H_


#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef _ITER_H_
#include "util/Iter.h"
#endif
#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif
#ifndef __Util_h_
#include "util/Util.h"
#endif
#ifndef __HDLID_H_
#include "hdl/HdlId.h"
#endif
#include "shell/carbon_internal_enums.h"

class UtString;
class StringAtom;

class WaveChild;
class WaveHandle;
class WaveScope;
class CarbonWaveUserData;
class FsdbFile;
class UserType;

//! Child iterator
typedef Iter<WaveChild*> WaveChildIter;
//! Handle iterator
typedef Iter<WaveHandle*> WaveHandleIter;
//! Scope iterator
typedef Iter<WaveScope*> WaveScopeIter;  

//! Wave child class
/*!
  This class doesn't actually implement much.  It exists to provide a
  base class for both WaveHandle and WaveScope so they can looped
  together.
 */
class WaveChild
{
public:
  CARBONMEM_OVERRIDES

  WaveChild() {}
  virtual ~WaveChild() {}

  // Move a common function here so it can be pure virtual and prevent
  // this class from being instanced on its own
  virtual const StringAtom* getNameAtom() const = 0;

  virtual const WaveHandle* castWaveHandle() const;
  virtual const WaveScope* castWaveScope() const;

  virtual WaveHandle* castWaveHandle()
  {
    const WaveChild* p = this;
    return const_cast<WaveHandle*>(p->castWaveHandle());
  }

  virtual WaveScope* castWaveScope()
  {
    const WaveChild* p = this;
    return const_cast<WaveScope*>(p->castWaveScope());
  }
};

//! Wave Net Handle class
/*!
  This object represents a net and its value. The value change of
  the net \e only gets written every time the time is advanced.
*/
class WaveHandle : public WaveChild
{
public: CARBONMEM_OVERRIDES

  //! Value (both scalar and vector) type definition
  typedef char* Value;

  //! Wave implementation specific data type
  typedef void* Obj;
  typedef const void* CObj;

  //! Scopes create handles
  WaveHandle(const StringAtom* sigName, CarbonVerilogType type, CarbonVhdlType vtype,
             CarbonVarDirection direction,
             const HdlId& nameInfo, const UserType* ut);
  
  //! destructor
  virtual ~WaveHandle();

  //! Get the name/identifier
  /*!
    If the net is a vector this does \e NOT return the range. 
    \sa getNameWithRange()
  */
  const char* getName() const;
  //! Get StringAtom for handle.
  virtual const StringAtom* getNameAtom() const {
    return mName;
  }

  //! Get the signal direction
  CarbonVarDirection getDirection() const;

  //! Get the identifier with range information
  /*!
    \param buf Pointer to UtString object to store result
    \param addVecSpace Defaults to true. If addVecSpace is true this
    will add a space between the identifier and the range. This is
    needed to compensate for a comparescan bug with vectors in vcd
    format files.
    \param prefix Prepends the prefix, if provided, to the name.
    \returns UtString stored in UtString object, ie., the identifier and
    the range if this is a vector. If this is not a vector then no
    range information is given.
    
    \warning Due to bug in CompareScan we are forced to put a space
    in-between the identifier and the range. So, this returns the
    id, and if a vec, a space, and then the range.
    
    \sa getName()
  */
  const char* getNameWithRange(UtString* buf, bool addVecSpace = true,
                               const char* prefix = NULL) const;
  
  //! Get the signal type of this signal
  CarbonVerilogType getType() const;
  
  //! Get the bit size of this signal
  UInt32 getSize() const;
  
  //! Get the Most Significant Bit of this vector
  /*!
    If this signal is not a vector then this returns -1.
  */
  SInt32 getMSB() const;
  
  //! Get the Least Significant Bit of this vector
  /*!
    If this signal is not a vector then this returns -1.
  */
  SInt32 getLSB() const;

  //! Get the user type for the net associated with this wave handle.
  const UserType* getUserType() const;
  
  //! Set the value of this vector at current time
  /*!
    This sets the value of the handle and schedules it for dumping
    to the wave file. The user must determine if the value has
    changed or not.
    \param value Value that is the size of this signal and contains
    the value of this signal 
    \warning Value must be of the same size as this signal unless it
    is real number
  */
  void updateValue(const Value value);

  //! Set the value of this vector at current time, but don't register
  /*!
    This sets the value of the handle, but does not put it in the
    change table.
    
    \param value Value that is the size of this signal and contains
    the value of this signal 
    \warning Value must be of the same size as this signal unless it
    is a real number
  */
  void setValue(const Value value);

  //! Just set the value of this real type
  /*!
    This does \e not schedule the handle to be updated in the wave
    file.
  */
  void setValueDbl(double dblValue);

  //! Just set the value of this integer type
  /*!
    This does \e not schedule the handle to be updated in the wave
    file. This is called by the default action of
    addChangedInt32. This simply turns the integer into a binary
    string.
  */
  void setValueInt32(UInt32 intValue);

  //! Just set the value of this time type
  /*!
    This does \e not schedule the handle to be updated in the wave
    file.
  */
  void setValueInt64(UInt64 intValue);
  
  //! Set the value if this handle is a real type
  /*!
    This only does something if the type is either real or a
    parameter. It will assert in debug mode if the type is not
    either of those. This schedules the handle for update to the wave
    file.
    
    \param dblValue Real value to which the output UtString should be
    set.
  */
  void updateValueDbl(double dblValue);
  
  //! Set the value of this handle if it is an integer type
  /*!
    Updates the integer value and schedules this handle for update to
    the file.
  */
  void updateValueInt32(SInt32 intValue);

  //! Set the value of this handle if it is a time type
  /*!
    Updates the integer value and schedules this handle for update to
    the file. Currently, only time types can be 64 bits.
  */
  void updateValueInt64(UInt64 intValue);
  
  //! Returns the current value of this signal
  inline Value getCurrentValue() const {
    return mValue;
  }
  
  //! Returns the current value of this signal for modification
  /*!
    For implementations that don't keep their own vectors of values
    for signals, they can access the vectors directly using this
    method instead of setValueChange(). The current value of the
    signal gets written each time the time is advanced.
    
    \warning <b> DO NOT CHANGE THE SIZE OF THE VALUE!! </b>
  */
  inline Value getValue() {
    return mValue;
  }
  
  //! Schedules this handle for update
  /*!
    This is \e not to be used when using setValueChange(const Value&).
    This is needed when using getValue()
  */
  void setIsChanged();
  
  //! Add aliased signal to this handle
  /*!
    Aliased signals share the same identifier code.
    The best way to use this is to add the sigHandle alias and 
    throw away sigHandle. Then, only update the value of this
    handle. The code is written so that you don't have to do that,
    but it is a little more efficient to only look at a value once.
    
    \warning Aliases must be added to the lexigraphically smallest, ie
    alphabetically first, handle in the list in order to ensure
    portability across waveform dumpers.

    \param sigHandle This should be a previously unaliased Handle,
    but that is not required. 
  */
  void addAlias(WaveHandle* sigHandle);

  //! Returns true if this is not a master handle
  bool isSlaveAlias() const;

  //! Returns true if a value change is registered
  inline bool isRegistered() const {
    return mIsRegistered;
  }


  //! Returns the absolute master handle of this Handle
  inline WaveHandle* getTopHandle(const WaveHandle* sig) 
  {
    const WaveHandle* me = const_cast<const WaveHandle*>(this);
    return const_cast<WaveHandle*>(me->getTopHandle(sig));
  }
  

  //! const version of getTopHandle
  inline const WaveHandle* getTopHandle(const WaveHandle* sig) const
  {
    const WaveHandle* link = sig;
    while (link->mMasterHandle)
      link = link->mMasterHandle;
    
    return link;
  }
  
  //! Invalidate the value change
  inline void unregister()
  {
    mIsRegistered = false;
  }

  //! Returns true if this handle contains a real (8 byte) value
  bool isReal() const;

  //! Returns true if this handle contains an integer (4 byte) value
  bool isInteger() const;

  //! Returns true if this handle is a time handle
  bool isTime() const;

  //! Returns true if this handle is a vhdl character handle
  bool isCharacter() const;

  //! Returns true if this handle is a vhdl enumeration handle
  bool isEnumeration() const;

  //! Returns true if this handle is a vhdl signal handle
  bool isVhdlSig() const;

  CarbonVhdlType getVhdlType() const;

  //! Returns true if this handle contains a hardware design net
  /*!
    For example, if the net is a wire or a reg, but not real or an
    integer.
  */
  bool isHardwareNet() const;

  //! Set wave implementation-specific data
  void setObj(Obj data);
  
  //! Get wave implementation-specific data
  Obj getObj();

  //! Get wave implementation-specific data, const
  CObj getObj() const;

  //! x out the value of the signal.
  /*!
    This does not update the change registry.
  */
  void setValueX();

  virtual const WaveHandle* castWaveHandle() const;

private:
  const StringAtom* mName;
  CarbonVerilogType mType;
  CarbonVhdlType mVhdlType;
  CarbonVarDirection mDirection;
  const UserType* mUserType; // User type info for this wave handle.
  HdlId mNameInfo;
  Value mValue;
  WaveHandle* mMasterHandle;
  Obj mRep; // Wave implementation specific data
  bool mIsRegistered;

  void deleteValue();

  // forbid
  WaveHandle();
  WaveHandle(const WaveHandle&);
  WaveHandle& operator=(const WaveHandle&);
};

//! Scope object
/*!
  A scope can be of several types (module, task, or function).
  Nets are added to a scope and handles are passed back for
  signal value manipulation.
*/
class WaveScope : public WaveChild
{
public: CARBONMEM_OVERRIDES

  //! constant handle iterator

  //! Scope type enumeration
  enum ScopeType {
    eModule,    //!< Module scope
    eTask,      //!< Task scope
    eFunction,  //!< Function scope
    eBegin,     //!< Begin scope
    eFork,      //!< Fork scope
    eStruct,    //!< Structure scope
    eArray      //!< Array scope
  };


  //! destructor
  virtual ~WaveScope();

  //! Add signal to watch list
  /*!
    Adds signal for update to watch list.
    \param sigName Name of signal
    \param type Signal type
    \param direction Direction of signal
    \param nameInfo Information about net (scalar, vector, range)
    \returns Handle of signal for update
    \warning Cannot be called after CloseHierarchy(). Do not delete
    the Handle. This is done by the owning class on
    destruction.
  */
  WaveHandle* addSignal(const StringAtom* sigName, 
                        CarbonVerilogType type,
                        CarbonVarDirection direction,
                        const HdlId& nameInfo, const UserType* ut = NULL,
                        CarbonVhdlType vtype = eVhdlTypeUnknown);
    
  //! Return the scope type
  ScopeType getType() const;

  //! Put the name of this scope into supplied UtString and return a
  //! pointer to the string in that UtString.
  const char* getName(UtString* nameStr) const;

  //! Put the name of this scope with range if appropriate, into supplied
  //! UtString and return a pointer to the string in that UtString.
  /*!
    For scopes of type eArray, the range is appended so that names look
    like "top.arr[3:0]" instead of just "top.arr" in the waveforms.
  */
  const char* getNameWithRange(UtString* nameStr) const;

  //! Return the name of this scope as a string atom
  virtual const StringAtom* getNameAtom() const;

  //! Return the parent scope of this scope.
  const WaveScope* getParentScope() const;

  //! Loop over the WaveHandles and WaveScopes in this scope
  WaveChildIter loopChildren();

  //! Loop over the WaveHandles in this scope
  WaveHandleIter loopHandles();

  //! Loop over the WaveScopes in this scope, non-const
  WaveScopeIter loopScopes();

  //! Scopes are created by WaveFiles
  WaveScope(const StringAtom* name, WaveScope* parentScope, ScopeType type,
            const UserType* ut, CarbonLanguageType language);

  //! Add a child scope this this scope
  void addChild(WaveScope* scope);

  //! Sort all children
  void sortChildren();

  //! Returns true if this scope is empty
  bool isEmpty() const;

  //! Returns true if this scope has any signals
  bool hasSignals() const;

  //! Returns true if this scope has child scopes
  bool hasChildScopes() const;

  //! Returns the total of signals and child scopes.
  UInt32 numChildren() const;

  //! Gets the language of the scope
  CarbonLanguageType getScopeLanguage() const {
    return mScopeLanguage;
  };

  virtual const WaveScope* castWaveScope() const;

private:
  const StringAtom* mScopeName;
  WaveScope* mParentScope;
  ScopeType mType;
  const UserType*    mUserType;      // User type handle for WaveScope

  CarbonLanguageType           mScopeLanguage; // The module language
  
  class Children;
  Children* mChildren;  
  
  // forbid
  WaveScope(const WaveScope&);
  WaveScope& operator=(const WaveScope&);
};

//! Interface for wave dumping
/*!
  This object is an abstraction for several waveform file formats. All
  file i/o methods are available only through this object.
*/
class WaveDump
{
public: CARBONMEM_OVERRIDES
  //! constructor
  WaveDump(CarbonTimescale timescale);

  //! virtual destructor
  virtual ~WaveDump();

  //! Simulation time type definition
  typedef UInt64 SimTime;
  
  //! Wave File mode enumeration
  enum FileMode { 
    eCreate, //!< Create or truncate file for write
    eAppend  //!< Append to existing file or create new one
  };

  //! File status enumeration
  enum FileStatus {
    eOK,    //!< File written to/read from with success
    eError  //!< Error while writing to/reading from file
  };


  //! Set the time scale units
  void setTimeScale(CarbonTimescale units);

  //! Get the time scale
  CarbonTimescale getTimeScale();
  
  //! Set initial time
  /*!
    This is the beginning of time. It defaults to 0.
    \param absTime The absolute value of the beginning of time.
    \warning this cannot be called after advanceTime().
  */
  void setInitialTime(const SimTime absTime);

  //! Get the number of times that time has advanced
  SInt32 numAdvanceTimes() const;

  //! Return true if the hierarchy is still open
  /*!
    If the hierarchy is still open then it is still legal to add more
    nets
  */
  inline bool isHierarchyOpen() const
  {
    return mHierOpen;
  }

  //! Returns true if dumping is turned on
  bool isDumpOn() const;

  //! Get the current time
  SimTime getCurrentTime() const;
  
  //! Add scope
  /*!
    \param scopeName Name of scope to add
    \param parentScope Scope to which the added scope is attached. If
    NULL, then the scope being added is a root.
    \param type Type of scope being attached
    \returns New Scope object attached to parentScope
    \warning Do not attach a scope to the same parentScope more than
    once.
  */
  WaveScope* attachScope(const StringAtom* scopeName, 
                         WaveScope* parentScope, 
                         WaveScope::ScopeType type,
                         const UserType* ut,
                         CarbonLanguageType language);

  //! Close hierarchy formation
  /*!
    When adding signals, hierarchy is created internally. Once all the
    signals are added this is called to write out the signal hierarchy
    and begin signal value update.
  */
  virtual FileStatus closeHierarchy(UtString* errMsg) = 0;

  //! Add handle with modified value to change table
  void addChanged(WaveHandle* modifiedHandle);
  
  //! Change the value of the signal and add to change table
  void addChangedWithVal(WaveHandle* sig, const WaveHandle::Value value);
  
  //! Change the value of a real signal and add to change table
  /*!
    Different waveform dumpers have different ways of dealing with
    real numbers. So, pass this off to the implementation.
  */
  virtual void addChangedWithDbl(WaveHandle* sig, double value) = 0;

  //! Change the value of a real signal and add to change table
  /*!
    Different waveform dumpers have different ways of dealing with
    32 bit integers. So, pass this off to the implementation.
  */
  virtual void addChangedWithInt32(WaveHandle* sig, SInt32 value) = 0;
  
  //! Change the value of a real signal and add to change table
  /*!
    Different waveform dumpers have different ways of dealing with
    64 bit integers. So, pass this off to the implementation.
  */
  virtual void addChangedWithInt64(WaveHandle* sig, UInt64 value) = 0;

  //! Write value changes and advance time to curTime
  /*!
    \param curTime Monotonically increasing number that represents
    time. This is \e not the delta of the current sim time and current
    waveform time. If curTime is less than or equal to the current
    waveformTime it is ignored. The only exception to this is the first
    timeslot to be written out. If curTime has not changed (or even if
    it has gone backwards in time) the first timeslot will be written
    out with the initial time which is set with setInitialTime (0 by
    default).
  */
  void advanceTime(const SimTime curTime);
  
  //! Dump all values of all variables
  /*!
    Dumping must be turned on when calling this; otherwise, it has no
    effect. Advances time to curTime. If curTime is less than waveform
    time this will do a dump on the current waveform time.
    \sa dumpOff, dumpOn
  */
  virtual void dumpAll(const SimTime curTime);

  //! Turn dumping off
  /*!
    This assumes that dumping is turned on (default).
    Advances time to curTime. Nothing is done if curTime is less than
    or equal to the current waveformTime or if dumping is already
    turned off.
    \sa dumpOn, dumpAll
  */
  virtual void dumpOff(const SimTime curTime);
  
  //! Turn dumping back on
  /*!
    This assumes that dumping was turned off.
    Advances time to curTime. Nothing is done if curTime is less than
    or equal to the current waveformTime or if dumping is already
    turned on.
    \sa dumpOff, dumpAll
  */
  virtual void dumpOn(const SimTime curTime);

  //! Explicitly flush the buffer
  /*!
    This is done automatically when the buffer reaches or exceeds the
    barrier size, but this method is provided for special cases where
    the buffer should be flushed manually.

    \param errMsg Pointer to UtString to hold error message 
    \returns cOK if no error while writing file
  */
  virtual FileStatus flush(UtString* errMsg) = 0;
  
  //! Close the file
  /*!
    This is done automatically upon deletion of this object, but is
    provided for manual control.

    \param errMsg Pointer to UtString to hold error message     
    \returns cOK if no error while closing
  */
  virtual FileStatus close(UtString* errMsg) = 0;

  //! Returns true if this file format supports array scopes
  virtual bool supportsArrayScopes() const;

  //! Returns true if this is an fsdb file
  virtual bool isFsdb() const;

  //! Add data to a waveform file - fsdb only
  /*!
    This asserts if the file is not an fsdb file.
  */
  virtual CarbonWaveUserData* addUserVar(CarbonVarType netType, 
                                         CarbonVarDirection direction,
                                         CarbonDataType dataType,
                                         int lbitnum, int rbitnum,
                                         CarbonClientData data, 
                                         const char* varName, 
                                         CarbonBytesPerBit bpb,
                                         const char* fullPathScopeName,
                                         const char* scopeSeparator,
                                         UtString* errMsg);

  //! Remove user data references from data file structure lists
  /*!
    This is simply a cleanup of the user data from any objects inside
    this class or a derived class. It does not remove it from the
    waveform. This is called just before the impending doom of
    userData.

    This only has effect with fsdb files, but it has to be a virtual
    function so the fsdb library doesn't get sucked in for non-fsdb
    runs.
  */
  virtual void eraseUserData(CarbonWaveUserData* userData);

  //! Loop over all the WaveScopes 
  WaveScopeIter loopScopes();

  //! Loop over all the root scopes
  WaveScopeIter loopRoots();

  //! Sort the root nodes
  void sortRoots();

  //! Write all the values to file
  void writeAllValues();

  //! Flush pending value changes
  /*!  
    This is needed to flush pending value changes when the simulation
    ends, the user has decided to turn dumping off, or a checkpoint is
    restored.
  */
  void flushPendingData();

  //! True if the initial time has been set
  /*!
    This returns whether or not the initial time was set in the
    waveform. 
  */
  inline bool isInitTimeSet() const { return mInitTimeSet; }

  //! switches fsdb files
  /*! This asserts if the file is not an fsdb file.
   */
  virtual bool switchFSDBFile(const char* fileName);

protected:
  static const char* cShellVersion;

  typedef UtArray<WaveHandle*> HandleList;
  typedef Loop<HandleList> HandleListLoop;
  typedef CLoop<HandleList> HandleListCLoop;

  SInt32 mTimeCnt;
  CarbonTimescale mTimeScale;
  SimTime mCurTime;
  HandleList mChanges; // Value change list
  bool mHierOpen;
  bool mDumpOn;
  bool mInitTimeSet;

  void xAllHandles();

  void clearAuxHierStructs();

  bool isEmpty(WaveScope* scope);
  
  virtual void writeValueChanges() = 0;

  virtual void writeHandleValue(WaveHandle* handle) = 0;

  //! For fsdb files - write any user-defined data.
  virtual void writeAllUserData();

  //! Dump all data
  virtual void dumpDataAll() = 0;

  //! Dump all data with dumpoff command
  virtual void dumpDataOff() = 0;
  
  //! Dump all data with dumpon command
  virtual void dumpDataOn() = 0;

private:
  class ScopeNodes;
  ScopeNodes* mScopeNodes;

  // forbid
  WaveDump();
  WaveDump(const WaveDump&);
  WaveDump& operator=(const WaveDump&);
};

#endif
