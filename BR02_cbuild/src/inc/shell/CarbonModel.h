// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONMODEL_H_
#define __CARBONMODEL_H_

#ifndef __CARBON_SYS_INCLUDE_H__
#include "codegen/SysIncludes.h"
#endif
#ifndef __SHELLGLOBAL_H_
#include "shell/ShellGlobal.h"
#endif
#ifndef __CARBONWAVE_H_
#include "shell/CarbonWave.h"
#endif
#ifndef __CARBONHOOKUP_H_
#include "shell/CarbonHookup.h"
#endif
#ifndef __DynBitVector_h_
#include "util/DynBitVector.h"
#endif
#ifndef __carbon_model_h_
#include "shell/carbon_model.h"
#endif

#ifndef CARBONREPLAYDECL_H_
#include "shell/CarbonReplayDecl.h"
#endif

// forward declaration
class WaveDump;
class UtICheckpointStream;
class UtOCheckpointStream;
class OnDemandMgr;
class IODBRuntime;
class CarbonDatabaseRuntime;

using CarbonReplay::ReplayCheckpointClosure;

//! Structure for holding a shadow and its corresponding net.
class CarbonShadowStruct {
public: CARBONMEM_OVERRIDES

  //! Abstraction for a shadow value
  typedef void* Shadow;

#ifndef CARBON_EXTERNAL_DOC
  //! constructor
  /*!
    A user would never construct one of these. This is used by the
    Carbon internals to create the structure
  */
  CarbonShadowStruct(Shadow val, CarbonNet* net) {
    mShadow = val;
    mNet = net;
  }
  
  //! destructor
  ~CarbonShadowStruct() {}
#endif
  
  // Explicit methods for getting the shadow and net
  //! Returns The Shadow Value
  inline Shadow getShadow() { return mShadow; }
  //! Returns the corresponding net
  inline CarbonNet* getNet() { return mNet; }

  //! Returns the corresponding net
  inline const CarbonNet* getNet() const { return mNet; }

  // or reference them directly...

  //! The actual shadow value
  Shadow mShadow;
  //! The actual CarbonNet
  CarbonNet* mNet;
};


//! Object for holding two equal-sized BVs
/*!
  This is used by CarbonModel to examine nets for net value change
  callbacks
*/
struct DynBitVectorPair
{
  CARBONMEM_OVERRIDES
  
  //! Width of one DynBitVector
  size_t size() const 
  {
    return mVal.size();
  }
  
  //! Resizes both the val and the drv
  void resize(size_t nSize)
  {
    mVal.resize(nSize);
    mDrv.resize(nSize);
  }
  
  //! Sets the value and drive to 0
  void clear()
  {
    mVal = 0;
    mDrv = 0;
  }
  
  //! Value buffer
  DynBitVector mVal;
  //! Drive buffer
  DynBitVector mDrv;
};

//! A class to store information for a c-model during a replay record session
/*!
 */
class CarbonReplayCModelData
{
public:
  CARBONMEM_OVERRIDES

  //! constructor
  CarbonReplayCModelData(CarbonObjectID* closure, void* hndl, void* cmodel,
                         int maxInputWords, int maxOutputWords);

  //! destructor
  ~CarbonReplayCModelData();

  void putModel(CarbonObjectID* closure) { mModel = closure->getModel (); }

  CarbonModel* getModel() const { return mModel; }

  void* getUserData() const { return mHndl; }

  void* getCModel() const { return mCmodel; }

  UInt32* getInputs() const { return mInputs; }

  UInt32* getOutputs() const { return mOutputs; }

private:
  CarbonModel*  mModel;         // Pointer to call CarbonModel::recordCModel
  void*         mHndl;          // The user's handle
  void*         mCmodel;        // Pointer to the c-model instance data
  UInt32        mMaxInputWords; // The maximum number of inputs for all variants
  UInt32        mMaxOutputWords;// The maximum number of outputs for all variants
  UInt32*       mInputs;        // Array to hold inputs during record
  UInt32*       mOutputs;       // Array to hold outputs during record
};

//! The design context and interface for extrinsic properties of nets
/*!
  This is the design context for the user. This gets created using the
  carbon_{design}_create() function call.

  This class gives access to extrinsic behaviour of nets such as
  whether or not a particular net is an output or if it is a reset or
  a clock, etc. 
  This also gives access to the CarbonWave object for wave dumping as
  well as a call for cycle scheduling.
*/
#ifdef CARBON_EXTERNAL_DOC
class CarbonObject
#else
class CarbonModel
#endif
{
public: CARBONMEM_OVERRIDES
#ifndef CARBON_EXTERNAL_DOC
  //! destructor
  ~CarbonModel();
#endif

  //! \return the C descriptor for this Carbon model
  CarbonObjectID *getObjectID () { return mHookup->getObjectID (); }

  //! Is the net an input? Not MT-safe
  bool isInput(const CarbonNet* net) const; 
  //! Is the net an output? Not MT-safe
  bool isOutput(const CarbonNet* net) const;
  //! Is the net a bidirect? Not MT-safe
  bool isBidirect(const CarbonNet* net) const;
  //! Is the net a clock? Not MT-safe
  bool isClock(const CarbonNet* net) const;
  //! Does the net drive a schedule on its posedge? Not MT-safe
  bool isPosedgeClock(const CarbonNet* net) const;
  //! Does the net drive a schedule on its negedge? Not MT-safe
  bool isNegedgeClock(const CarbonNet* net) const;
  //! Is the net a reset? Not MT-safe
  bool isReset(const CarbonNet* net) const;
  //! Is the net a posedge reset? Not MT-safe
  bool isPosedgeReset(const CarbonNet* net) const;
  //! Is the net a negedge reset? Not MT-safe
  bool isNegedgeReset(const CarbonNet* net) const;
  //! Is the net a primary i/o? Not MT-safe
  bool isPrimaryPort(const CarbonNet* net) const;
  //! Is the net internal to the design, ie, not a primary i/o? Not MT-safe
  bool isInternal(const CarbonNet* net) const;
  //! Is the net asynchronous? Not MT-safe
  bool isAsync(const CarbonNet* net) const;
  //! Is the net synchronous? Not MT-safe
  bool isSync(const CarbonNet* net) const;
  //! Is this net depositable? MT-safe
  bool isDepositable(const CarbonNet* net) const;
  //! Is this memory depositable?
  bool isMemDepositable(const CarbonMemory* mem) const;
  //! Is this net forcible? MT-safe
  bool isForcible(const CarbonNet* net) const;
  
  //! Has the model been initialized?
  bool isInitialized() const { return mIsInitialized; }

  //! Save the state of the simulation
  /*!
    \warning Not MT-Safe
  */
  CarbonStatus simSave(const char* filename);

  //! Restore the state of the simulation
  /*!
    \warning Not MT-Safe
  */
  CarbonStatus simRestore(const char* filename);

  //! Save the state of the simulation to a user-provided stream
  /*!
    \warning Not MT-Safe
  */
  CarbonStatus simStreamSave(CarbonStreamWriteFunc, void *userData, const char *streamName);

  //! Restore the state of the simulation from a user-provided stream
  /*!
    \warning Not MT-Safe
  */
  CarbonStatus simStreamRestore(CarbonStreamReadFunc, void *userData, const char *streamName);
  
  //! Write arbitrary data to the checkpoint file being used by an active
  //! save operation.
  /*!
    \warning Not MT-Safe
  */
  CarbonStatus checkpointWrite(const void *data, const UInt32 numBytes);
  
  //! Read data from the checkpoint file being used by an active
  //! restore operation.
  /*!
    \warning Not MT-Safe
  */
  CarbonStatus checkpointRead(void *data, const UInt32 numBytes);
  
  //! Find a net based on its name
  /*!
    \param name Full path name of the requested net
    \returns NULL if the net is not found
    \warning Not MT-Safe
  */
  CarbonNet* findNet(const char* name);

  //! Find a net and allocate a shadow for it
  /*!
    This does a findNet(), allocShadow() sequence. 

    \warning Allocating a great number of shadows will significantly
    increase memory use. It is advisable to allocate shadows
    sparingly.
    \returns NULL if findNet() failed.
  */
  CarbonShadowStruct* findNetAllocShadow(const char* name);

  //! Find a memory based on its name
  /*!
    \param name Full path name to the requested memory
    \returns The CarbonMemory object if the name referes to a memory,
    NULL otherwise
    \warning Not MT-Safe
  */
  CarbonMemory* findMemory(const char* name);

  //! Free a reference structure from the memory heap
  /*!
    This is not needed, generally; however, this may be useful if a
    user needs to conserve space for performance (swapping) issues,
    and a net reference is not needed anymore.
    \param netPtr A pointer to the reference to be freed. The
    reference will be set to NULL after being freed. This is to avoid
    use of a freed pointer. By setting it to NULL, if a user uses a
    freed reference, the application will segv, which is highly
    desirable compared to 'undefined' when using a uninitialized,
    freed pointer. If the netPtr is NULL or if *netPtr is NULL then
    this function has no effect.

    \warning This is NOT MT-Safe.
  */
  void freeNet(CarbonNet** netPtr);

  //! Same as freeNet(), but for memories
  void freeMemory(CarbonMemory** memPtr);

  //! Force a net with specified value
  /*!
    This works just like deposit(), except that the value is forced. The
    net (or an alias) must have been specified in the directives file.
    
    This is MT-Safe.
    \param net Net to force
    \param buf Value to force
  */
  CarbonStatus force(CarbonNet* net, const UInt32* buf);

  //! Force a word of the net
  /*!
    Works just like depositWord() except that the value is forced.
    \sa force(), releaseWord()
    
    MT-Safe
  */
  CarbonStatus forceWord(CarbonNet* net, UInt32 buf, int index);

  //! Force a range of the net
  /*!
    Works just like depositRange() except that the value is forced.
    \sa force(), releaseRange()

    MT-Safe
  */
  CarbonStatus forceRange(CarbonNet* net, const UInt32* buf, 
                          int range_msb, int range_lsb);

  //! Release a forced net
  /*!
    This has no effect if the CarbonNet is not forced.
    
    This is MT-Safe.
    \param net Net to release
  */
  CarbonStatus release(CarbonNet* net);

  //! Release a word of a forced net
  /*!
    MT-Safe
    \sa release()
  */
  CarbonStatus releaseWord(CarbonNet* net, int index);

  //! Release a range of a forced net
  /*!
    MT-Safe
    \sa release()
  */
  CarbonStatus releaseRange(CarbonNet* net, 
                            int range_msb, int range_lsb);
  

  //! Run the initialization function
  /*! This should be called before any other register initialization
   *  or the schedule function is called. This routine allows the
   *  model to create its c-models and run the HDL initialization
   *  logic.
   *
   *  This routine is only necessary/legal if the eCarbon_NoInit flag
   *  is passed to the model create function and if it hasn't been
   *  called before.
   *
   *  The purpose of this alternate methodology is to allow work to be
   *  done between the create and initialize passes and to pass data
   *  to the c-model's create functions.
   */
  CarbonStatus initialize(void* cmodelData, void* reserved1, void* reserved2);

  //! Initialize all registers to be 1
  /*!
    This is MT-Safe.
    \warning Not yet implemented
  */
  void setInitRegisters() {};

  //! Initialize all registers to be 0
  /*!
    This is MT-Safe.
    \warning Not yet implemented
  */
  void clearInitRegisters() {};

  //! Initialize all registers randomly
  /*!
    This will will initialize all the registers with a random
    algorithm based on the seed passed in.

    This is MT-Safe.
    \warning Not yet implemented
  */
  void randInitRegisters(SInt32 /*seed*/) {}

  //! Run the schedule and update time. MT-Safe
  CarbonStatus schedule(CarbonTime currentSimulationTime);

  //! Run the clock schedule and update time. MT-Safe
  CarbonStatus clkSchedule(CarbonTime currentSimulationTime);
 
  //! Run the data schedule and update time. MT-Safe
  CarbonStatus dataSchedule(CarbonTime currentSimulationTime);

  //! Run the async schedule and update time. MT-Safe
  CarbonStatus asyncSchedule(CarbonTime currentSimulationTime);

  //! Run the dep-combo schedule, if needed.
  void depComboSchedule();
 
  //! Get the current simulation time. MT-Safe
  CarbonTime getSimulationTime() const;

  //! Put the current simulation time. MT-Safe
  void putSimulationTime(CarbonTime simTime);


  //! This enables all bits externally of a tristate net
  /*!
    Sets all the drive bits to 0 (meaning driven).
    This is ineffective for non-tristates.
    This is MT-Safe.
  */
  //void setToDriven(CarbonNet* net);

  //! This disables all xdrive bits of a tristate net
  /*!
    Sets all the drive bits to 1 (meaning undriven).
    This is ineffective for non-tristates,
    This is MT-Safe.
  */
  void setToUndriven(CarbonNet* net);

  //! Disables all internally driven bits of xdrive of a tristate net 
  /*!
    Sets all xdrive bits that are internally driven to 1 (meaning undriven).
    This is ineffective for non-tristates,
    This is MT-Safe.
  */
  void resolveXdrive(CarbonNet* net);

  //! Gets the external drive for a net
  /*!
    This is only useful for bidirects. Inputs return all 0's. Outputs
    and non-primaries return all 1's.
  */
  void getExternalDrive(CarbonNet* net, UInt32* xdrv);
  

  //! Returns true if the net is a primary input
  bool isPrimaryInput(const CarbonNet* net) const;
  
  //! Allocate a shadow value of the given net.
  /*!
    The CarbonShadowStruct that is returned will have a shadow value in it
    with the current value of the CarbonNet. The CarbonShadowStruct can be
    used to keep track of changes of a net. The CarbonShadowStruct also has
    a reference to the net that is passed in. It is highly recommended
    that callers only keep one copy of the net around. So, a good way
    to create shadows is:
    \code
    CarbonModel::CarbonShadowStruct* shadow = NULL;
    {
      CarbonNet* net = model->findNet("a.b");
      shadow = model->allocShadow(net);
    }
    model->deposit(shadow->mNet, someValue);
    \endcode

    The user may deallocate the shadow by calling freeShadow(). Any
    unfreed shadows will be deallocated when the CarbonModel is
    destroyed.

    \warning Allocating a great number of shadows will significantly
    increase memory use. It is advisable to allocate shadows sparingly.
  */
  CarbonShadowStruct* allocShadow(CarbonNet* net);
  
  //! Free a shadow value
  /*!
    This deallocates the shadow value. The shadowPtr de-reference will
    be NULL after returning. 
    For example,
    \code
    model->freeShadow(&shadow);
    \endcode
  */
  void freeShadow(CarbonShadowStruct** shadowPtr);
  
  //! Returns true if the value changed, and updates shadow
  /*!
    If the net in the CarbonShadowStruct has changed (thus, making the
    shadow and the net differ in value) 'true' is returned, and the
    shadow value will be updated to the current value.
  */
  bool compareUpdate(CarbonShadowStruct* shadow);

  //! Reads the net's value and puts it into buf
  /*!
    \param net The CarbonNet
    \param value is an array of UInt32s which has at least the size of 
    getNumUInt32s() 
    \param drive is an array of UInt32s which has at least the size of 
    getNumUInt32s(). If NULL, the drive is not examined. Defaults to
    NULL. This parameter has no effect on non-tristates.

    This is MT-Safe.
  */
  CarbonStatus examine(const CarbonNet* net, UInt32* value, UInt32* drive = NULL);
  
  //! Examine a word of the net's value
  /*!
    \param net The CarbonNet
    \param value A pointer to a UInt32
    \param index Index into the array of UInt32s which represents the
    value
    \param drive is a pointer to a word. If NULL, the drive is not
    examined. Defaults to NULL. This parameter has no effect on
    non-tristates.

    This is MT-Safe.
  */
  CarbonStatus examineWord(const CarbonNet* net, UInt32* value, 
                            int index, UInt32* drive = NULL);
  
  //! Examine an arbitrary range of a net's value
  /*!
    \param net The CarbonNet
    \param buf A pointer to an array of UInt32s large enough to hold
    the value of the specified range.
    \param range_msb Most signifigant bit of range to examine.
    \param range_lsb Least signifigant bit of range to examine.
    \param drive is an array of UInt32s large enough to hold the drive
    value in the specified range. If NULL, the drive is not
    examined. Defaults to NULL. This parameter has no effect on
    non-tristates.

    This is MT-Safe.
  */
  CarbonStatus examineRange(const CarbonNet* net, UInt32 *buf, int range_msb, int range_lsb, UInt32* drive = NULL);

  //! Write net's current value into valueStr according to strFormat
  /*!
    This is MT-Safe.
    \param net Net of which to print the value
    \param valueStr Character buffer into which the value is placed.
    \param len The length of the character buffer, including the byte
    for the terminating character.
    \param strFormat Radix to which to format the value
    \returns eCarbon_OK if the valueStr is big enough.
  */
  CarbonStatus format(const CarbonNet* net, char* valueStr, size_t len, CarbonRadix strFormat);


  //! Get the name of the net
  /*!
    \warning This is NOT MT-Safe.
    Places the name of the net into the buffer. This is a method on
    CarbonModel because CarbonNets don't know from which hdl language
    they are derived.
    
    \param net Net from which to get the name
    \param buffer Properly-sized character buffer
    \param len Length of character buffer
    \param includeVector If false, vector ranges will not be included
    in the name
    \returns eCarbon_OK if the buffer is big enough, eCarbon_ERROR
    otherwise. Also places the name of the net into the buffer if the
    buffer is big enough.
  */
  CarbonStatus getName(const CarbonNet* net, char* buffer, size_t len, bool includeVector = true) const;
  
  //! Compose the name of a net into a UtString
  void composeNetName(const CarbonNet* net, UtString* nameBuf, bool includeVector) const;

  //! Create a CarbonWave object
  /*!
    This is \e not MT-Safe.

    This should only be called once. This allocates a CarbonWave
    object.

    \param dumpFile File writer. This needs to be created separately
    in order to keep 3rd party vendor libraries from being sucked in
    when not requested.
    \param errMsg If there was an error when creating the dumpfile,
    this string must have the error message. This centralizes the
    error code for different formats
    \returns The newly created CarbonWave, or NULL if any problems
    were encountered.
  */
  CarbonWave* waveCreate(WaveDump* dumpFile,
                         const UtString* errMsg);
  
  //! Return the already created CarbonWave
  /*!
    This is MT-Safe.
    This is a convenience method so the caller does not have to keep a
    local variable for CarbonWave. This simply returns the CarbonWave
    object. It will be NULL if waveCreate() has not been called.
    \sa waveCreate()
  */
  CarbonWave* getCarbonWave();

  //! Close the waveform associated with this model
  void waveClose();

  //! Add a callback to be called on a CarbonNet value change
  /*!
    On any value change of the specified CarbonNet after a schedule()
    call, the supplied function will be called.

    \param fn Function to call on a net change
    \param userData Data to pass to the function
    \param net Net to monitor
    \returns A structure with all the supplied parameters. This is
    needed to remove a callback. It can also be used to make an inline
    change to the callback. The user can change the function to be
    called or the userData supplied, but not the net that is
    monitored.

    \warning The user is responsible for uniqueness. Therefore, if a
    function is added twice with the same net it will be called twice.
   */
  CarbonNetValueCBData* addNetChangeCB(CarbonNetValueCB fn,
                                       CarbonClientData userData,
                                       CarbonNet* net);
  
  //! Add a callback to be called on a CarbonNet value or drive change
  /*!

    On any value or drive change of the specified CarbonNet after a
    schedule() call, the supplied function will be called.

    \param fn Function to call on a net change
    \param userData Data to pass to the function
    \param net Net to monitor
    \returns A structure with all the supplied parameters. This is
    needed to remove a callback. It can also be used to make an inline
    change to the callback. The user can change the function to be
    called or the userData supplied, but not the net that is
    monitored.

    \warning The user is responsible for uniqueness. Therefore, if a
    function is added twice with the same net it will be called twice.
   */
  CarbonNetValueCBData* addNetValueDriveCB(CarbonNetValueCB fn,
                                           CarbonClientData userData,
                                           CarbonNet* net);
  
  // deprecated slower callback mechanism
  CarbonNetCBData* addNetChangeCB_old(CarbonNetCB fn,
                                      CarbonClientData userData,
                                      CarbonNet* net);


  //! Are these two nets aliased together?
  /*!
    \param n1 Must be non-null
    \param n2 Must be non-null
    \returns True if the two nets are aliased together, false
    otherwise.
    \warning if either n1 or n2 is null, false is returned
    This is MT-Safe
  */
  bool areAliased(const CarbonNet* n1, const CarbonNet* n2) const;

  //! Get the pull mode on a net
  CarbonPullMode getPullMode(const CarbonNet* net) const;
  
  //! Check for conflicts on the internal and external drives
  /*!
    This is only meaningful for bidirects. All other nets will return
    0. For bidirects, this will return 1 if any bit of the internal
    drive of the net is being driven simultaneously with the external
    drive.
  */
  int hasDriveConflict(CarbonNet* net) const;

  //! Check if the deposit schedule should be run after a mem deposit
  void maybeMemDepositRunCombo(const CarbonMemory* mem);

#ifndef CARBON_EXTERNAL_DOC
  //! Get the CarbonHookup object
  /*!
    \internal
  */
  CarbonHookup* getHookup();

  //! Get the CarbonHookup object
  /*!
    \internal
  */
  const CarbonHookup* getHookup() const;


  //! \return a handle used for the model finder
  CarbonOpaque getHandle () const { return getHookup ()->getHandle (); }

  //! constructor only called by ShellGlobal.
  /*!
    \internal
  */
  CarbonModel(CarbonObjectID *,
              const char* dbFilenameOrBuffer,
              int bufferSize,
              CarbonDBType, bool*);

#endif
  
  // should we issue deposit-to-combinational logic warnings?
  void putIssueDepositComboWarning(bool);

  // should we issue deposit-to-combinational constant warnings?
  void putIssueDepositConstWarning(bool);

  // should we issue non-observable net warnings?
  void putIssueNotObservableWarning(bool);

  // should we issue inaccurate net warnings?
  void putIssueStaleNetWarning(bool);

  //! Run the sample (aka debug) schedule, if it hasn't already
  /*!
    Using 'const' here because examines are supposed to be
    non-modifying.
   */
  void runSampleSchedule() const;

  //! Enable a disabled callback
  void enableNetValueCB(CarbonNetValueCBData* cbData);
  //! Disable an enabled callback.
  void disableNetValueCB(CarbonNetValueCBData* cbData);

  inline bool issueDepositComboWarning() const { return mIssueDepositComboWarning; }
  inline bool issueDepositConstWarning() const { return mIssueDepositConstWarning; }

  //! Called by generated model to initialize profiling.
  void setupProfiling(CarbonProfileInfo* prof_info);

  //! Return true if this model was compiled with -profile.
  bool compiledWithProfile() const { return getBlockBucket() != scNoBucket; }

  //! Return reference to model's current block profiling bucket.
  const volatile int& getBlockBucket() const { return *mCurrentBlockBucket; }
  //! Return reference to model's current schedule profiling bucket.
  const volatile int& getScheduleBucket() const { return *mCurrentScheduleBucket; }

  //! Return number of profiling block buckets needed for this design.
  UInt32 getNumBlockBuckets() const { return mNumBlockBuckets; }
  //! Return number of profiling schedule buckets needed for this design.
  UInt32 getNumScheduleBuckets() const { return mNumScheduleBuckets; }

  //! Return the index of the API currently executing.
  unsigned getCurrentAPI() const { return mCurrentAPI; }
  //! Set the index of the API the model's currently executing.
  void putCurrentAPI(unsigned id) { mCurrentAPI = id; }

  //! Get the message context for this model
  MsgContext* getMsgContext() const;

  //! Get the replay info structure. NULL if not replayable.
  CarbonReplayInfo* getReplayInfo();

  //! Start recording
  CarbonStatus replayRecordStart();

  //! Schedule a dump of the design into the replay database
  CarbonStatus replayRecordScheduleCheckpoint();
  
  //! Stop recording
  CarbonStatus replayRecordStop();

  //! Start playback
  CarbonStatus replayPlaybackStart();

  //! Stop playback
  CarbonStatus replayPlaybackStop();

  //! Control verbose divergence reporting
  CarbonStatus replaySetVerboseDivergence(bool verbose);

  //! Is verbose divergence reporting enabled?
  bool replayIsVerboseDivergence();

  //! Initialize for replay. 
  /*!
    This has to be called before any nets are allocated.
  */
  void enableReplay();

  //! Returns the current run mode
  CarbonVHMMode getRunMode() const { return mRunMode; }

  //! Creates the onDemand manager, enabling onDemand support
  /*!
    This is called by ShellGlobal::createModel() if the onDemand init flag is specified.
   */
  void enableOnDemand();

  //! Returns whether the Carbon Model has onDemand enabled
  int onDemandIsEnabled();

  //! Sets the maximum number of states for onDemand to track
  CarbonStatus onDemandSetMaxStates(CarbonUInt32 max_states);

  //! Configures OnDemand backoff strategy
  CarbonStatus onDemandSetBackoffStrategy(CarbonOnDemandBackoffStrategy strategy);

  //! Sets OnDemand backoff count
  CarbonStatus onDemandSetBackoffCount(CarbonUInt32 count);

  //! Sets OnDemand backoff decay percentage
  CarbonStatus onDemandSetBackoffDecayPercentage(CarbonUInt32 percentage);

  //! Sets OnDemand backoff maximum decay 
  CarbonStatus onDemandSetBackoffMaxDecay(CarbonUInt32 max);

  //! Registers an onDemand mode change callback function
  CarbonOnDemandCBDataID* onDemandAddModeChangeCB(CarbonOnDemandModeChangeCBFunc fn, void *userData);

  //! Enables onDemand statistics
  /*!
    This enables very basic statistic gathering, such as number of
    requested schedule calls vs. actual schedule calls executed, and
    the length of the longest repeating pattern.  Statistics will be
    printed at model destruction.

    More detailed statistical information can be obtained by
    registering a mode change callback or by enabling debug mode.
   */
  CarbonStatus onDemandEnableStats();

  //! Temporarily stops onDemand
  /*!
    When onDemand is stopped, no pattern searching is done, any
    active idle state is exited, and the Carbon Model is run normally.
   */
  CarbonStatus onDemandStop();

  //! Starts a new idle pattern search
  /*!
    This function forces onDemand to begin a new pattern search,
    overriding a previous stop command or any backoff timer that may
    be active.  This function has no effect if a pattern search is
    already in progress, or if the Carbon Model is already in an idle state.
   */
  CarbonStatus onDemandStart();

  //! Enables OnDemand debug mode and configures its event reporting level
  /*!
    Debug mode reports details about certain events that affect
    onDemand execution.  These events include unsupported API calls,
    state restores, stimulus divergences, and failed pattern searches.

    Debug mode has overhead associated with recording and
    reporting these events, and so it should only be used when
    initially tuning an onDemand environment.
   */
  CarbonStatus onDemandSetDebugLevel(CarbonOnDemandDebugLevel level);

  //! Registers an OnDemand debug callback function
  CarbonOnDemandCBDataID* onDemandAddDebugCB(CarbonOnDemandDebugCBFunc fn, void* userData);

  //! Enables/disables an OnDemand callback function
  CarbonStatus onDemandSetCBEnable(CarbonOnDemandCBDataID* cb, bool enable);

  CarbonStatus onDemandMakeIdleDeposit(CarbonNetID *net);

  CarbonStatus onDemandExclude(CarbonNetID *net);

  //! Force an OnDemand non-idle access
  /*!
    This will cause any idle search to be aborted.  The specified node
    name will be used for the corresponding OnDemand debug event.
  */
  void onDemandForceNonIdleAccess(const UtString& nodeName);

  //! Force an OnDemand state restore access
  /*!
    This will cause OnDemand to restore the current model state if
    idle (but will not cause an exit from the idle state).  The
    specified node name will be used for the corresponding OnDemand
    debug event.
  */
  void onDemandForceStateRestore(const UtString& nodeName);

  //! Returns the DB API object for this model
  CarbonDatabaseRuntime* getDBAPI() { return mDBAPI; }

  //! Returns a symbol table node for a CarbonNet
  const CarbonDatabaseNode* getDBNodeFromNet(CarbonNetID* net);

  //! Examines an array node to a file
  CarbonStatus examineArrayToFile(const CarbonDatabaseNode* node, CarbonRadix format, const char* filename);

  //! Deposits an array node from a file
  CarbonStatus depositArrayFromFile(const CarbonDatabaseNode* node, CarbonRadix format, const char* filename);

  //! Returns the total number of schedule calls
  UInt64 getTotNumSchedCalls() const { return mTotNumSchedCalls; }
  
  //! Decrement one from the total number of schedule calls
  /*!
    Called by the replay recover mechanism.
    This asserts if the total number of sched calls is 0 before
    subtraction. (this should never happen).
  */
  void decrTotNumSchedCalls() { 
    INFO_ASSERT(mTotNumSchedCalls > 0, "Cannot decrement schedule call.");
    --mTotNumSchedCalls; 
  }


  //! Function to register a c-model that will need to execute during playback
  /*! Register a c-model instance with the replay system. The c-model's
   *  key is the combination of the cmodelData and context.
   *
   *  \param cmodelData - A pointer to the c-model class data within
   *                      the design hierarchy.
   *
   *  \param userData   - A pointer to the c-model's user data allocated
   *                      on creation.
   *
   *  \param context    - The context for each c-model is needed to
   *                      differentiate all the different c-model
   *                      calls. NUModule c-models can call the same
   *                      user supplied run function with different
   *                      arguments.
   *
   *  \param name       - The full hierarchal path to the c-model
   *                      instance.
   *
   *  \param numInputWords - The number of UInt32 words passed as input
   *                         for this NUCModelCall instance.
   *
   *  \param numOutputWords - The number of UInt32 words written by this
   *                          NUCModelCall instance.
   */
  void registerCModel(void* cmodelData, void *userData, UInt32 context, const char* name,
                      UInt32 numInputWords, UInt32 numOutputWords,
                      CModelPlaybackFn fn);

  //! Function to prepare to record a call of a c-model during a replay record session
  /*! If a c-model needs to be played during Replay, then it must be
   *  recorded. This function is called for every such NUCModelCall
   *  instance.  All the instances are differentiated by using the
   *  cmodelData and context. This is needed to record the outputs of
   *  the cmodel prior to calling the run function. Upon playback we
   *  have to give the last value of the outputs when running the
   *  function.
   *
   *  \param cmodelData - A pointer to the c-model class data within
   *                      the design hierarchy.
   *
   *  \param context    - The context for each c-model is needed to
   *                      differentiate all the different c-model
   *                      calls. NUModule c-models can call the same
   *                      user supplied run function with different
   *                      arguments.
   *              
   *  \param outputs    - An array of UInt32 inputs for the c-model.
   *                      Each c-model knows how to copy the c-model
   *                      explicit outputs back to the UInt32 array
   *                      storage.
   */
  void preRecordCModel(void* cmodelData, UInt32 context, UInt32* outputs);

  //! Function to record a call of a c-model during a replay record session
  /*! If a c-model needs to be played during Replay, then it must be
   *  recorded. This function is called for every such NUCModelCall
   *  instance.  All the instances are differentiated by using the
   *  cmodelData and context.
   *
   *  \param cmodelData - A pointer to the c-model class data within
   *                      the design hierarchy.
   *
   *  \param context    - The context for each c-model is needed to
   *                      differentiate all the different c-model
   *                      calls. NUModule c-models can call the same
   *                      user supplied run function with different
   *                      arguments.
   *
   *  \param inputs     - An array of UInt32 inputs for the c-model.
   *                      Each c-model knows how to copy the inputs
   *                      to the explicit run function arguments.
   *              
   *  \param outputs    - An array of UInt32 inputs for the c-model.
   *                      Each c-model knows how to copy the c-model
   *                      explicit outputs back to the UInt32 array
   *                      storage.
   */
  void recordCModel(void* cmodelData, UInt32 context, UInt32* inputs,
                    UInt32* outputs);

  //! Abstraction for the current recovery status for a c-model
  /*! If a divergence occurs because of a c-model, then within the
   *  schedule call for recovery, the c-model might take the values
   *  either from the DB (before divergence point) or from the c-model
   *  (after the divergence point).
   */
  enum CModelRecoveryStatus {
    eCModelUseDB,       //!< Before divergence, use the recorded outputs
    eCModelRunFn        //!< After the divergence, call the c-model
  };

  //! Function to retrieve the recorded output values for a c-model
  /*! During recovery, the c-models are excised from the Carbon Model
   *  schedule. This is because the c-model is already at the
   *  divergence point and we only need its outputs to get the Carbon Model to
   *  that point.
   *
   *  Note that we don't need the inputs. However, we may want to pass
   *  the inputs from the Carbon Model to the shell for error detection.
   *
   *  \param cmodelData - A pointer to the c-model class data within
   *                      the design hierarchy.
   *
   *  \param context    - The context for each c-model is needed to
   *                      differentiate all the different c-model
   *                      calls. NUModule c-models can call the same
   *                      user supplied run function with different
   *                      arguments.
   *  \param outputs    - A place to put the c-models array.
   *
   *  \returns eCModelUseDB if the outputs are provided by replay
   *  system and eCModelRunFn if the c-model should be called (past
   *  the divergence point within the recovery schedule call because
   *  the divergence occurred in the middle of the schedule).
   */
  CModelRecoveryStatus getCModelValues(void* cmodelData, UInt32 context,
                                       UInt32** outputs);

  //! Returns true if we are currently within a checkpoint call
  /*!
    This only happens during record or playback in replay mode when
    taking a checkpoint or restoring a checkpoint for recovery or
    during playback initialization when taking a checkpoint for
    initial state comparison.
  */
  bool isCheckpointEvent() const { return mIsCheckpointEvent; }

  //! Take a checkpoint for replay record. 
  /*!
    This puts the model in a checkpoint state, so that
    isCheckpointEvent() returns true. After the checkpoint is
    finished, the model is out of the checkpoint state and
    isCheckpointEvent() is false.
    
    This checkpoint state information is used by cmodels that have to
    be played back so they do not store or restore.

    Returns the same status as simSave()
  */
  CarbonStatus saveReplayCheckpoint(const char* fileName, const ReplayCheckpointClosure& closure);


  //! Restore a checkpoint for replay recover. 
  /*!
    This puts the model in a checkpoint state, so that
    isCheckpointEvent() returns true. After the checkpoint is
    finished, the model is out of the checkpoint state and
    isCheckpointEvent() is false.
    
    This checkpoint state information is used by cmodels that have to
    be played back so they do not store or restore.

    Returns the same status as simRestore()
  */
  CarbonStatus restoreReplayCheckpoint(const char* fileName, ReplayCheckpointClosure* closure);
  
  //! Returns the map of cmodel calls used during replay
  CarbonReplay::ReplayCModelMap *getReplayCModelMap();

  //! Returns the value buffer pointer for net value change callbacks
  /*!
    The value buffer is used to examine the value when a net changes
    to pass it to the callback function. The value buffer is big
    enough to hold the widest callback net.
  */
  DynBitVectorPair* getNetValueChangeCBBuffer();

  //! Returns whether onDemand encountered any divergent cmodels
  bool onDemandDivergentCModel();

  //! Install OnDemand schedule pointers in the model
  void onDemandInstallSchedulePointers();

  //! get the IODB
  IODBRuntime* getIODB() const;

  //! Applies wrappers necessary for Replay and OnDemand
  ShellNet* applyNetWrappers(ShellNet* origNet, bool doOnDemand);

protected:
  /* deprecated functions needed for back-compatibility */

  //! Deposit an entire value to the net's value
  /*!
    For most nets this is the most efficient way to set a value. For
    very wide nets (say, over 128 bits long), where only a range or an
    aligned word of the value is being changed, it may be faster to
    use depositRange() or depositWord(). For tristate nets, however,
    due to the drive complexity there is no speed benefit from
    depositWord() or depositRange().

    \param net The CarbonNet
    \param buf Array of at least size getNumUInt32s(). The value will
    be copied into the nets internally represented value. If this
    parameter is NULL, it means to set the drive bits to
    undriven. This particular feature would only have any effect on
    tristates.
    \param drive is an array of UInt32s which has at least the size of 
    getNumUInt32s(). If NULL, the drive defaults to driven. Defaults to
    NULL. This parameter has no effect on non-tristates.
    \returns eCarbon_OK if the deposit succeeded.
    \sa getNumUInt32s()

    This is MT-Safe.
  */
  CarbonStatus deposit(CarbonNet* net, const UInt32* buf, const UInt32* drive = NULL);

  //! Deposit 1 word of the net's value at index (of value array)
  /*!
    This is MT-Safe.

    This is a very efficient way to set an aligned word of a wide
    net. Best efficiency is gained when depositing all the changing
    words to a net in sequence. Efficiency is lost if the code does
    something like this:

    \code
    // changing net1
    model->depositWord(net1, val1, 0);

    // changing net2
    model->depositWord(net2, val2, 0);

    // changing net1
    model->depositWord(net1, val3, 3);
    \endcode

    It is more efficient to do the following instead:
    \code
    // Deposit all changing values to net1 in sequence
    model->depositWord(net1, val1, 0);
    model->depositWord(net1, val3, 3);

    // Then, change other nets
    model->depositWord(net2, val2, 0);
    \endcode

    \param net The CarbonNet
    \param buf A UInt32 to deposit
    \param index Index into the array of UInt32s
    \param drive is the word value for the drive. Defaults to 0. 
    This has no effect on non-tristates.
    \returns eCarbon_OK if the deposit succeeded.
    \sa deposit() for notes on efficiency.
  */
  CarbonStatus depositWord(CarbonNet* net, UInt32 buf, int index, UInt32 drive = 0);

  //! Deposit value ino the specified range of the net's value
  /*!
    This is MT-Safe.

    Efficiency is best if multiple ranges of a single net are done in
    sequence without intervening deposits to other nets. See
    depositWord().
    
    \param net The CarbonNet
    \param buf An array of UInt32 which holds the range value to
    deposit. The least significant bit of buf will be placed at the
    range_lsb of value.
    \param range_msb high bit of the range.
    \param range_lsb low bit if the range.
    \param drive is an array of UInt32s - same size and value rules as
    buf. If NULL, the drive is not set. Defaults to NULL. This
    parameter has no effect on non-tristates.
    \returns eCarbon_OK if the deposit succeeded.
    \sa deposit() and depositWord() for notes on efficiency

    \warning The drive parameter is different than deposit() here. In
    deposit(), if the drive parameter is NULL then it is assumed to be
    externally driven. This is \e not the case here. If it is NULL, the
    drive value is untouched.
  */
  CarbonStatus depositRange(CarbonNet* net, const UInt32* buf, int range_msb, 
                             int range_lsb, 
                             const UInt32* drive = NULL);

  //! Set the run mode
  void putRunMode(CarbonVHMMode runMode) { mRunMode = runMode; }

  //! Sync the net value change callback shadows
  /*!
    This forces the callback shadows to be the same value as the
    current value. This is needed for restore and replay.

    If requested, callbacks will be collated first.  This is typically
    done at the beginning of the schedule call following callback
    registration.  However, in order for the shadow values to be
    updated for callbacks that have been registered since the last
    schedule call, collation is required first.

    \param collate If true, callbacks will be collated first.
  */
  void updateNetValueChangeCBShadows(bool collate);

  //! Temporarily disable onDemand, if applicable
  void maybeDisableOnDemand();

  //! Re-enable a temporarily-disabled onDemand, if applicable
  void maybeReEnableOnDemand();

  //! Prepare OnDemand for model save
  void onDemandPreSave();

  //! Prepare OnDemand for model restore
  void onDemandPreRestore();

private:
  //! Helper function for the various CB add functions.
  CarbonNetValueCBData* changeCallbackFinish(CarbonNetValueCBData* cbData,
                                             ShellNet* shlNet);

  const SInt32 cSchedCallLimit;
  // Number of schedule calls until next heartbeart.
  SInt32 mNumSchedCallsUntilHeartbeat;
  SInt32 mPostSchedInaccurateNets;
  CarbonHookup* mHookup;
  // Total number of schedule calls since the beginning of time.
  UInt64 mTotNumSchedCalls;

  ShellNet* findShellNet(const CarbonDatabaseNode* node, bool forCarbonMemory);

  class ShadowList;
  ShadowList* mShadowList;
  
  
  class DataHelper;
  friend class DataHelper;
  DataHelper* mDataHelper;

  // stream pointers are valid only during save/restore operations.
  UtOCheckpointStream *mCheckpointWriteStream;
  UtICheckpointStream *mCheckpointReadStream;

  bool mIsCheckpointEvent;
  bool mIsInitialized;
  mutable bool mSampleSchedHasRun;
  mutable bool mIssueDepositComboWarning;
  mutable bool mIssueDepositConstWarning;
  mutable bool mIssueNotObservableWarning;
  mutable bool mIssueStaleNetWarning;

  // Bucket value indicating there is no bucket.  This is used to
  // return a bucket number from a model compiled without -profile,
  // and used as the initialization value before the bucket number has
  // been set.  The compiled model (with -profile) uses -1 to indicate
  // not in any bucket.  We use -2 to indicate the model was compiled
  // w/o -profile.
  static const volatile int scNoBucket;

  //! Profiling block bucket ID of the currently executing generated code
  const volatile int* mCurrentBlockBucket;
  // Profiling schedule bucket ID of the currently executing generated code
  const volatile int* mCurrentScheduleBucket;
  // The API function currently executing
  unsigned mCurrentAPI;
  //! Number of profiling block buckets needed for this design
  UInt32 mNumBlockBuckets;
  // Number of profiling schedule buckets needed for this design
  UInt32 mNumScheduleBuckets;

  //! The current run mode
  CarbonVHMMode mRunMode;

  // replay data
  class ReplayData;
  // Replay bom manager
  class ReplayBOM;
  friend class ReplayBOM;
  ReplayBOM* mReplayBOM;
  ReplayBOM* getReplayBOM() { return mReplayBOM; }

  // onDemand manager
  friend class OnDemandMgr;
  OnDemandMgr *mOnDemandMgr;
  //! Returns the onDemand manager object
  OnDemandMgr *getOnDemandMgr() { return mOnDemandMgr; }

  //! DB API object
  CarbonDatabaseRuntime *mDBAPI;

  // Used for reading symboltable leaves in a replay symtab
  struct ReplayReadLeafClosure;
  
  void putInitialized() { mIsInitialized = true; }

  void putIsCheckpointEvent(bool isCheckpoint)
  {
    mIsCheckpointEvent = isCheckpoint;
  }

  inline void postSchedule();

  bool checkObservable(const ShellNet* shlNet) const;
  inline bool checkForcible(ShellNet* forceNet, bool* runCombo) const;

  //! Returns the vhm type
  /*!
    The iodb may say it is replayable, but if replay was not enabled
    at construction, this returns eNormal.
  */
  CarbonVHMTypeFlags determineVHMTypeFlags() const;

  //! This does the license heartbeat
  inline void licCheck()
  {
    --mNumSchedCallsUntilHeartbeat;
    if (mNumSchedCallsUntilHeartbeat == 0)
    {
      ShellGlobal::doHeartbeat();
      mNumSchedCallsUntilHeartbeat = cSchedCallLimit;
    }
  }
  
  // common code for checkpoint save/restore
  CarbonStatus save(UtOCheckpointStream &out);
  CarbonStatus restore(UtICheckpointStream &in);
  CarbonStatus simCommonRestore(ZistreamDB& zin);
  CarbonStatus simCommonSave(ZostreamDB& zout);
  
  //forbid
  CarbonModel();
  CarbonModel(const CarbonModel&);
  CarbonModel& operator=(const CarbonModel&);
};

//! Class to manage schedule function pointers
struct ScheduleFnContainer
{
  CARBONMEM_OVERRIDES
    
  struct InitSchedData
  {
    CARBONMEM_OVERRIDES

    void* mCModelData;
    void* mReserved1;
    void* mReserved2;
  };
    
  enum ScheduleType {
    eDesign,
    eClk,
    eData,
    eAsync,
  };

  ScheduleFnContainer() : 
    mDesignScheduleFn(NULL),
    mClkScheduleFn(NULL),
    mDataScheduleFn(NULL),
    mAsyncScheduleFn(NULL),
    mInitSchedData(NULL)
  {}

  ~ScheduleFnContainer()
  {
    delete mInitSchedData;
  }

  void init(const CarbonHookup* hookup)
  {
    init(hookup->getScheduleFn(),
         hookup->getClkScheduleFn(),
         hookup->getDataScheduleFn(),
         hookup->getAsyncScheduleFn());
  }

  void init(::CarbonCoreScheduleFn designScheduleFn,
            ::CarbonCoreScheduleFn clkScheduleFn,
            ::CarbonCoreScheduleFn dataScheduleFn,
            ::CarbonCoreScheduleFn asyncScheduleFn)
  {
    mDesignScheduleFn = designScheduleFn;
    mClkScheduleFn = clkScheduleFn;
    mDataScheduleFn = dataScheduleFn;
    mAsyncScheduleFn = asyncScheduleFn;
  }

  bool isInit() const
  {
    return ((mDesignScheduleFn != NULL) &&
            (mClkScheduleFn != NULL) &&
            (mDataScheduleFn != NULL) &&
            (mAsyncScheduleFn != NULL));
  }

  void saveInitSchedData(void* cmodeldata, void* reserved1, void* reserved2)
  {
    if (! mInitSchedData)
      mInitSchedData = new InitSchedData;

    mInitSchedData->mCModelData = cmodeldata;
    mInitSchedData->mReserved1 = reserved1;
    mInitSchedData->mReserved2 = reserved2;
  }
  
  void getInitSchedData(void** cmodelData, void** reserved1, 
                        void** reserved2)
  {
    if (mInitSchedData)
    {
      *cmodelData = mInitSchedData->mCModelData;
      *reserved1 = mInitSchedData->mReserved1;
      *reserved2 = mInitSchedData->mReserved2;
    }
    else
      *cmodelData = *reserved1 = *reserved2 = NULL;

  }

  CarbonStatus callSchedule(CarbonModel* model, CarbonTime simTime)
  {
    return (*mDesignScheduleFn)(model->getObjectID (), simTime);
  }

  CarbonStatus callClkSchedule(CarbonModel* model, CarbonTime simTime)
  {
    return (*mClkScheduleFn)(model->getObjectID (), simTime);
  }

  CarbonStatus callDataSchedule(CarbonModel* model, CarbonTime simTime)
  {
    return (*mDataScheduleFn)(model->getObjectID (), simTime);
  }

  CarbonStatus callAsyncSchedule(CarbonModel* model, CarbonTime simTime)
  {
    return (*mAsyncScheduleFn)(model->getObjectID (), simTime);
  }

  CarbonStatus callScheduleType(CarbonModel* model, CarbonTime simTime, ScheduleType type)
  {
    switch (type) {
    case eDesign:
      return callSchedule(model, simTime);
    case eClk:
      return callClkSchedule(model, simTime);
    case eData:
      return callDataSchedule(model, simTime);
    case eAsync:
      return callAsyncSchedule(model, simTime);
    default:
      return eCarbon_ERROR;
    }
  }

  void resetHookupSchedules(CarbonHookup* hookup)
  {
    hookup->putScheduleFn(mDesignScheduleFn);
    hookup->putClkScheduleFn(mClkScheduleFn);
    hookup->putDataScheduleFn(mDataScheduleFn);
    hookup->putAsyncScheduleFn(mAsyncScheduleFn);
  }

private:
  ::CarbonCoreScheduleFn mDesignScheduleFn;
  ::CarbonCoreScheduleFn mClkScheduleFn;
  ::CarbonCoreScheduleFn mDataScheduleFn;
  ::CarbonCoreScheduleFn mAsyncScheduleFn;
  // Data needed to call carbonInitialized. This is NULL until
  // carbonInitialize is called.
  InitSchedData* mInitSchedData;
};

#ifndef CARBON_EXTERNAL_DOC
//! The external world deals with CarbonObjects
typedef struct carbon_model_descr CarbonObject;
#endif

#endif
