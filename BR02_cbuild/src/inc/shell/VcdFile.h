// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __VCDFILE_H_
#define __VCDFILE_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef __UtString_h_
#include "util/UtString.h"
#endif
#ifndef __UtIOStream_h_
#include "util/UtIOStream.h"
#endif
#ifndef __WAVEDUMP_H_
#include "shell/WaveDump.h"
#endif
#ifndef _UtStringArray_h_
#include "util/UtStringArray.h"
#endif

class StringAtom;

/*!
  \file
  Declaration of VcdFile reader/writer class object
*/

//! VCD reader/writer
/*!
  VCD (Value Change Dump) reader/writer class. This is invoked by the
  shell to capture the user-specified signals. It is also used in
  auxiliary programs to analyze data and to map the data to other
  formats.
*/
class VcdFile : public WaveDump
{
public: 
  CARBONMEM_OVERRIDES

  //! Constructor, not for public consumption
  /*!
    This is called by the static Open() method.
    \param fileMode Mode of the file to Open
    \param fileName Name of file to open
    \param timescale Timescale of the simulation
    \param isSuccess True if file open was success
    \param errMsg Pointer to UtString for error message
  */
  VcdFile(FileMode fileMode, const char* fileName,
          CarbonTimescale timescale,
          bool* isSuccess, UtString* errMsg);
  
  //! destructor
  virtual ~VcdFile();
  
  //! Static method to open VcdFile
  /*!
    \param fileMode R/W Mode of file to open
    \param fileName Name of file to open
    \param timescale Timescale of the simulation
    \param errMsg Pointer to UtString for error message
    \returns New VcdFile object or NULL if the file could not be
    opened
  */
  static VcdFile* open(FileMode fileMode, 
                       const char* fileName,
                       CarbonTimescale timescale,
                       UtString* errMsg);
    
  //! Set Auto Flush buffer size
  /*!
    Auto flushing dumps the internal buffer when the barrierSize is
    reached or exceeded. It defaults to 64 kbytes.
    
    \param bufferSize Size of buffer in kilobytes
  */
  void setAutoFlush(UInt32 bufferSize);

  //! Close hierarchy formation
  /*!
    During addSignal, hierarchy is created internally. Once all the
    signals are added this is called to write out the signal hierarchy
    and begin signal value update.
  */
  FileStatus closeHierarchy(UtString* errMsg);
  
  //! Explicitly flush the buffer
  /*!
    This is done automatically when the buffer reaches or exceeds the
    barrier size, but this method is provided for special cases where
    the buffer should be flushed manually.

    \param errMsg Pointer to UtString to hold error message 
    \returns cOK if no error while writing file
  */
  virtual FileStatus flush(UtString* errMsg);
  
  //! Close the file
  /*!
    This is done automatically upon deletion of this object, but is
    provided for manual control.

    \param errMsg Pointer to UtString to hold error message     
    \returns cOK if no error while closing
  */
  virtual FileStatus close(UtString* errMsg);

  //! Change the value of a real signal and add to change table
  virtual void addChangedWithDbl(WaveHandle* sig, double value);

  //! WaveDump::addChangedWithInt32()
  virtual void addChangedWithInt32(WaveHandle* sig, SInt32 value);
  
  //! WaveDump::addChangedWithInt64()
  virtual void addChangedWithInt64(WaveHandle* sig, UInt64 value);

protected:
  //! Dump all values of all variables
  /*!
    Dumping must be turned on when calling this; otherwise, it has no
    effect. Advances time to curTime. If curTime is less than waveform
    time this will do a dump on the current waveform time.
    \sa dumpOff, dumpOn
  */
  virtual void dumpDataAll();

  //! Turn dumping off
  /*!
    This assumes that dumping is turned on (default).
    Advances time to curTime. Nothing is done if curTime is less than
    or equal to the current waveformTime or if dumping is already
    turned off.
    \sa dumpOn, dumpAll
  */
  virtual void dumpDataOff();
  
  //! Turn dumping back on
  /*!
    This assumes that dumping was turned off.
    Advances time to curTime. Nothing is done if curTime is less than
    or equal to the current waveformTime or if dumping is already
    turned on.
    \sa dumpOff, dumpAll
  */
  virtual void dumpDataOn();

private:
  UtStringArray mGlyphStrs;
  VcdFile();
  VcdFile(const VcdFile&);
  VcdFile& operator=(const VcdFile&);

  // helper functions

  // Write value changes to file
  virtual void writeValueChanges();
  virtual void writeHandleValue(WaveHandle* handle);

  // non-virtual writeHandleValue
  void writeVcdHandleValue(WaveHandle* handle);

  void writeTime();
  // Removes leading backslashes for escaped names
  void VcdifyName(UtString* name);

  // check file state
  FileStatus checkFileState(UtString* errMsg);

  //! Hieroglyphics class
  /*!
    This is used to generate character identifier codes for variables
    in the vcd file.
  */
  class Glyph
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    Glyph();
    //! destructor
    ~Glyph();

    // The VCD standard supports ascii characters between and including
    // ! and ~
    const char cLexLow;
    const char cLexHigh;

    //! increment operator, calls Increment()
    Glyph& operator++();
    
    //! Increment glyph, expand as needed
    void Increment();

    //! Return the current glyph
    const char* getStr() const;

  private:
    UtString mIdCode;
    
    Glyph(const Glyph&);
    Glyph& operator=(const Glyph&);
  };

  UInt32 mBufferKb;

  UtOBStream mVcdOut;
  UtString mVcdName;
  const char* cIndent;



  // Write helpers
  bool writeDate(UtString* errMsg);
  bool writeVersion(UtString* errMsg);
  bool writeTimeScale(UtString* errMsg);
  bool writeHierarchy(UtString* errMsg);  

  const char* stringifyTimescale() const;
  const char* stringifyScopeType(WaveScope::ScopeType type) const;
  const char* stringifySigType(CarbonVerilogType type) const;
  void writeHierBranch(WaveScope* scope, Glyph& glyph);

  bool setGlyph(WaveHandle* handle, Glyph& glyph);
  inline const char* getGlyph(const WaveHandle* handle) const;
};

#endif
