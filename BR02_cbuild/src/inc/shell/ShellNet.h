// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __SHELLNET_H_
#define __SHELLNET_H_


#ifndef __CARBON_SYS_INCLUDE_H__
#include "codegen/SysIncludes.h"
#endif
#ifndef __CARBONNET_H_
#include "shell/CarbonNet.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif

#ifndef __CARBONDBREAD_H_
#include "shell/CarbonDBRead.h"
#endif

#ifndef __CARBONMODEL_H_
#include "shell/CarbonModel.h"
#endif

class HierName;
class ShellNetConstant;
class CarbonForceNet;
class CarbonExprNet;
class STAliasedLeafNode;
class CarbonVectorBase;
class CarbonScalarBase;
class ConstantRange;
class ShellNetTristate1;
class ShellNetTristate2;
class ShellNetTristate4;
class ShellNetTristate8;
class ShellNetTristateA;
class CarbonModel;
class ShellNetWrapper;

//! storage used for shadowing tristates and general nets
/*!
  This is mainly used for tristates, but can be used to shadow the
  entire value and drive of any net. The replay system uses this.
*/
class CarbonTriValShadow {
public:  CARBONMEM_OVERRIDES

  //! constructor
  CarbonTriValShadow(int numWords) {
    mValue = (UInt32*) CarbonMem::malloc(sizeof(UInt32) * numWords);
    mDrive = (UInt32*) CarbonMem::malloc(sizeof(UInt32) * numWords);
  }
  
  //! destructor
  ~CarbonTriValShadow() {
    CarbonMem::free(mValue);
    CarbonMem::free(mDrive);
  }
  
  //! The value
  UInt32* mValue;
  //! The drive
  UInt32* mDrive;
};


//! Default implementation for CarbonNet
/*!
  Used as a base class for all types
*/
class ShellNet : public virtual CarbonNet
{
public: 
  CARBONMEM_OVERRIDES

  //! Placeholder for traits of a net
  /*!
    This is mainly needed for type tests before depositing to nets.
  */
  struct Traits 
  {
    CARBONMEM_OVERRIDES

    //! Simple initializing constructor
    /*!
      Initializes all elements to be false or NULL.
    */
    Traits();

    //! Simple destructor
    ~Traits();
  
    //! Return the constant net pointer
    const ShellNetConstant* getConstant() const { return mConstantNet; }
    //! Return whether this has complex storage
    bool hasComplexStorage() const { return mHasComplexStorage; }
    //! Is the net a tristate?
    bool isTristate() const { return mIsTristate; }
    //! Is the net a real (double) net?
    bool isReal() const { return mIsReal; }
    //! Is the net an input (input or bidi)?
    bool hasInputSemantics() const { return mHasInputSemantics; }

    //! Return whether the net's value can't be accessed as a POD
    bool isNonPod() const { return (hasComplexStorage() || isTristate() || isReal()); }

    SInt32 getPodBitsel() const { return mPodBitsel; }

    //! Return the width
    /*!
      This is 0 for constants and expressions.
    */
    UInt32 getWidth() const { return mWidth; }

    //! Return the byte storage
    /*!
      It is up to the caller to decide if this is the correct method
      or not by using getWidth.
    */
    const UInt8* getByte() const { return mStorage.mByte; }
    //! Return the short storage
    /*!
      It is up to the caller to decide if this is the correct method
      or not by using getWidth.
    */
    const UInt16* getShort() const { return mStorage.mShort; }
    //! Return the word array storage
    /*!
      It is up to the caller to decide if this is the correct method
      or not by using getWidth. 
      This method should be used for values that are 4 words or
      greater than 8 words.
    */
    const UInt32* getLongPtr() const { return mStorage.mLongPtr; }

    //! Return the long long storage
    /*!
      It is up to the caller to decide if this is the correct method
      or not by using getWidth. 
    */
    const UInt64* getLLong() const { return mStorage.mLLong; }

    //! Return the real number storage
    /*!
      It is up to the caller to decide if this is the correct method
      or not by using isReal().
    */
    const CarbonReal* getReal() const { return mStorage.mReal; }
    
    //! Return the tristate byte storage
    /*!
      It is up to the caller to decide if this is the correct method
      or not by using getWidth and isTristate().
    */
    const ShellNetTristate1* getTriByte() const { return mStorage.mTriByte; }

    //! Return the tristate short storage
    /*!
      It is up to the caller to decide if this is the correct method
      or not by using getWidth and isTristate().
    */
    const ShellNetTristate2* getTriShort() const { return mStorage.mTriShort; }

    //! Return the tristate long word storage
    /*!
      It is up to the caller to decide if this is the correct method
      or not by using getWidth and isTristate().
    */
    const ShellNetTristate4* getTriLong() const { return mStorage.mTriLong; }

    //! Return the tristate long long storage
    /*!
      It is up to the caller to decide if this is the correct method
      or not by using getWidth and isTristate().
    */
    const ShellNetTristate8* getTriLLong() const { return mStorage.mTriLLong; }

    //! Return the tristate array storage
    /*!
      It is up to the caller to decide if this is the correct method
      or not by using getWidth and isTristate().
    */
    const ShellNetTristateA* getTriArray() const { return mStorage.mTriArray; }
    
    //! Set the byte storage
    void putByte(const UInt8* storage) { mStorage.mByte = storage; }
    //! Set the short storage
    void putShort(const UInt16* storage) { mStorage.mShort = storage; }
    //! Set the long word or array storage
    void putLongPtr(const UInt32* storage) { mStorage.mLongPtr = storage; }
    //! Set the long long storage
    void putLLong(const UInt64* storage) { mStorage.mLLong = storage; }
    //! Set the real number storage
    void putReal(const CarbonReal* storage) { mStorage.mReal = storage; }

    //! Set the tri byte storage
    void putTriByte(const ShellNetTristate1* storage) { mStorage.mTriByte = storage; }
    //! Set the tri short storage
    void putTriShort(const ShellNetTristate2* storage) { mStorage.mTriShort = storage; }
    //! Set the tri long word
    void putTriLong(const ShellNetTristate4* storage) { mStorage.mTriLong = storage; }
    //! Set the tri long long storage
    void putTriLLong(const ShellNetTristate8* storage) { mStorage.mTriLLong = storage; }
    //! Set the tri array storage
    void putTriArray(const ShellNetTristateA* storage) { mStorage.mTriArray = storage; }

    //! Primitive Constant net;
    const ShellNetConstant* mConstantNet;
    //! Does this net have complex storage (e.g. CarbonExprNet, ShellVisNet)?
    bool mHasComplexStorage;

    //! Width of value. 
    /*!
      Will be 0 for constant and expression nets 
    */
    UInt32 mWidth;
    
    //! Union for representing storage pointer
    /*! Null for constants and expressions */
    union StorageUnion {
      //! 1 bytes
      const UInt8* mByte;
      //! 2 bytes
      const UInt16* mShort;
      //! 4 bytes or > 8 bytes
      const UInt32* mLongPtr;
      //! 8 bytes
      const UInt64* mLLong;
      //! Real number value
      const CarbonReal* mReal;
      //! Tristate 1 byte
      const ShellNetTristate1* mTriByte;
      //! Tristate 2 bytes
      const ShellNetTristate2* mTriShort;
      //! Tristate 4 bytes
      const ShellNetTristate4* mTriLong;
      //! Tristate 8 bytes
      const ShellNetTristate8* mTriLLong;
      //! Tristate Word Array
      const ShellNetTristateA* mTriArray;
    };

    //! Storage Pointer or Tristate Pointer.
    /*!
      This always gives back the storage in the model, even in when
      -noInputFlow is used on the SPEEDCompiler command line. This
      storage pointer should only be used for reading a value AFTER
      the schedule has run. 
    */
    StorageUnion mStorage;

    //! Does the net have an input within it?
    /*!
      Expression nets can contain input nets, but expression nets are
      not CarbonInputs; therefore, expression nets may have input
      semantics but they cannot be cast to CarbonInputs.
    */
    bool mHasInputSemantics;
    //! Is the net a tristate?
    bool mIsTristate;
    //! Is the net a real number?
    bool mIsReal;
    /*!
      If this net represents a bitselect of a POD, this is the
      normalized bit index into it.  Otherwise, this is -1.
    */
    SInt32 mPodBitsel;
  };

  //! enum for value state
  enum ValueState { 
    eUnchanged, //!< Value is unchanged
    eChanged  //!< Value has changed
  };

  //! Mode for examination
  /*!
    For non-tristates, eIDrive will set the bits to correspond to the
    nets direction (1 value for an input and a 0 value for an
    output. eXDrive will do the same but from the external point of view
    (a 0 value for an input and a 1 value for an output). 
    eCalcDrive will be the same as eXDrive.
    
    For tristates, eCalcDrive is a combination of eIDrive and
    eXDrive. If either one is driving, the result is driven. 
  */ 
  enum ExamineMode {
    eIDrive, //! Examine the internal drive
    eCalcDrive, //! Calculate the net drive
    eXDrive //! Examine the external drive
  };
  
  //! Storage type definition
  typedef void* Storage;
  
  //! constructor
  ShellNet();
  
  //! virtual destructor
  virtual ~ShellNet();

  /* ------------------- */
  /* defaulted functions */

  //! Returns IODBIntrinsic object
  virtual const IODBIntrinsic* getIntrinsic() const;

  //! Bit width (MSB-LSB+1)
  virtual int getBitWidth () const;

  //! in/out vector length for objects.
  virtual int getNumUInt32s () const;

  //! \return LSB of vector
  /*!
    \warning undefined for scalars. 
   */
  virtual int getLSB () const;

  //! \return MSB of vector
  /*!
    \warning undefined for scalars. 
  */
  virtual int getMSB () const;

  //! Get CarbonDatabaseNode, if it exists
  /*!
    Only visibility nets or nets that wrap them should reimplement this
   */
  virtual const CarbonDatabaseNode* getDBNode() const;

  /* ------------------------------------------------- */
  /* functions that must be defined by derived classes */

  //! Is this object a VectorNet?
  virtual bool isVector () const = 0;

  //! Returns true if the net is a scalar, false otherwise  
  virtual bool isScalar() const = 0;

  //! Returns true if this net is a tristate, false otherwise
  virtual bool isTristate() const = 0;

  //! Returns true if this net is a real number, false otherwise
  virtual bool isReal() const = 0;

  //! Is the value non-zero?
  /*!
    Only valid for non-tristates.
  */
  virtual bool isDataNonZero() const = 0;

  //! Sets all the xdrive bits to driven, Tristates only
  /*!
    \returns true if the xdrive bits change
  */
  virtual bool setToDriven(CarbonModel* model) = 0;

  //! Sets all the xdrive bits to undriven, Tristates only
  /*!
    \returns true if the xdrive bits change
  */
  virtual bool setToUndriven(CarbonModel* model) = 0;

  //! Sets all xdrive bits of word
  //! Sets word of xdrive to undriven. Tristates only
  /*!
    \returns true if the xdrive bits change
  */
  virtual bool setWordToUndriven(int index, CarbonModel* model) = 0;

  //! Sets all xdrive bits not internally driven to undriven
  /*!
    \returns true if the xdrive bits change
  */
  virtual bool resolveXdrive(CarbonModel* model) = 0;

  //! Get the external contribution to the value of the net
  /*!
    This gets what the xdrive is currently set to. You should run
    carbonResolveBidi() if you want to disable bits that are
    internally driven.
    This function only makes sense for bidirects. 0's are returned for
    inputs and 1's for outputs.
  */
  virtual void getExternalDrive(UInt32* drive) const = 0;
  
  //! Sets range of xdrive to undriven. Tristates only
  /*!
    \returns true if the xdrive bits change
  */
  virtual bool setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model) = 0;
  
  //! Reads the nets values and puts into buf
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const = 0;

  //! Reads the nets values and puts into a real-number buf
  virtual CarbonStatus examine (CarbonReal* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const = 0;

  //! Examine a word of the value
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const = 0;

  //! Examine an arbitrary range of the value
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const = 0;

  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model) = 0;

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model) = 0;

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model) = 0;

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model) = 0;
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model) = 0;
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model) = 0;

  //! Force a value
  virtual CarbonStatus force(const UInt32* buf, CarbonModel* model) = 0;
  //! Force a word of the value
  virtual CarbonStatus forceWord(UInt32 buf, int index, CarbonModel* model) = 0;
  //! Force a range of the value
  virtual CarbonStatus forceRange(const UInt32* buf, 
                                  int range_msb, int range_lsb, 
                                  CarbonModel* model) = 0;
  
  //! Release the forced value
  virtual CarbonStatus release(CarbonModel* model) = 0;
  //! Release a word of the forced value
  virtual CarbonStatus releaseWord(int index, CarbonModel* model) = 0;
  //! Release a range of the forced value
  virtual CarbonStatus releaseRange(int range_msb, int range_lsb, CarbonModel* model) = 0;


  //! Allocate space for storing a shadow copy of this net's value
  /*!
    \warning Unimplemented for memories
  */
  virtual Storage allocShadow() const = 0;

  //! Free space allocated with allocShadow()
  /*!
    \param shadow Storage to delete
    \warning Unimplemented for memories
  */
  virtual void freeShadow(Storage* shadow) = 0;

  //! Update the shadow value based on the current value
  /*!
    \param shadow Storage to write to. Must be the same size as the
    internal data
    \warning Unimplemented for memories
  */
  virtual void update(Storage* shadow) const = 0;

  //! Update the shadow value based on the current value without resolution
  /*!

    Note: that this function's base implementation is to use
    update. That is because for all types but tri-states, we don't do
    any resolution.

    \param shadow Storage to write to. Must be the same size as the
    internal data
    \warning Unimplemented for memories
  */
  virtual void updateUnresolved(Storage* shadow) const;

  //! Compare the shadow and the storage values
  /*!
    \returns whether or not if the storage has changed from the
    shadow copy
    \warning unimplemented for memories
  */
  virtual ValueState compare(const Storage shadow) const = 0;

  //! Write value to string if shadow and storage differ
  /*!
    If the current value differs from the shadow, write as binary to
    valueStr, update current value, and return eChanged

    \param valueStr Buffer to write value to. If the value does not
    change, this buffer is not guaranteed to \e not change. This will
    hold the current value if the value did change. This buffer 
    \b MUST be initialized. It is checked to have an x in the first
    byte, in which case the state will be eChanged and the value will
    be updated.
    \param len Total length of the valueStr
    \param shadow Value to compare against. Must be the same size as
    internal storage
    \param flags Net-specific flags needed for bidi resolution.
    \returns Whether or not the value did change.
    \warning Not defined for memories
  */
  virtual ValueState writeIfNotEq(char* valueStr, size_t len, Storage* shadow,
                                  NetFlags flags) = 0;

  //! Write current value into valueStr according to strFormat
  /*!
    \warning Not implemented for memories
    This may change the size of the valueStr parameter if the valueStr
    size is not large or small enough to fit the entire value.
    
    \param model May be NULL. Needed to report error messages.
    \returns eCarbon_OK if the valueStr is big enough.
  */
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel* model) const = 0;

  //! Is this net forcible?
  /*!
    This is not the same as castForceNet(). All CarbonForceNets are
    forcible, but that is not the entire forcible set of all
    ShellNets. CarbonExprNets can also be forcible, but they are not
    force nets. They may contain CarbonForceNets however.
   */
  virtual bool isForcible() const = 0;

  //! Is this expression net an input?
  virtual bool isInput() const = 0;

  //! const version of memory cast
  virtual const CarbonMemory* castMemory() const = 0;

  //! const version of model memory cast
  virtual const CarbonModelMemory* castModelMemory() const = 0;

  //! const version of vector cast
  virtual const CarbonVectorBase* castVector() const = 0;

  //! const version of scalar cast
  virtual const CarbonScalarBase* castScalar() const = 0;

  //! cast to a ShellNetConstant
  virtual const ShellNetConstant* castConstant() const = 0;

  //! Cast to a CarbonForceNet
  virtual const CarbonForceNet* castForceNet() const = 0;

  //! Const cast to a cCarbonExprNet
  virtual const CarbonExprNet* castExprNet() const = 0;

  //! Return this as a const wrapped shell net
  /*!
    Returns NULL if this is not a wrapped shellnet.
  */
  virtual const ShellNetWrapper* castShellNetWrapper() const = 0;
  

  //! Set all bits in the value to 0
  virtual void putToZero(CarbonModel* model) = 0;
  //! Set all bits in the value to 1
  virtual void putToOnes(CarbonModel* model) = 0;
  //! Set all bits in the range of the value to 1
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model) = 0;
  //! Set all bits in the range of the value to 0
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model) = 0;

  //! Returns 1 if the net has a drive conflict.
  virtual int hasDriveConflict() const = 0;
  
  //! Does the net range have a drive conflict?
  /*!
    \retval 1 if the net's range has a drive conflict
    \retval 0 if there is no conflict
    \retval -1 if the net range is invalid
  */
  virtual int hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const = 0;
  
  //! Returns a word of external drive and value
  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const = 0;

  //! Sets the actual storage to undriven
  /*!
    This sets the storage of the net to undriven,
    unconditionally. This works just like setToUndriven(), however
    with no flows, setToUndriven() doesn't take effect until the
    shadow updates the storage. This method updates the storage
    immediately.
    
    \warning This method is intended for initialization and should not
    be used for any purpose. 
  */
  virtual void setRawToUndriven(CarbonModel* model) = 0;

  //! Get the traits of the net
  virtual void getTraits(Traits* traits) const = 0;

  //! Called by CarbonForceNet for conditional formatting
  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask) = 0;
  //! Called by CarbonForceNet for formatting
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel* model) const = 0;

  //! set the pointer into the change array , if applicable
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex) = 0;

  //! Get the pointer to the change array. Returns NULL if none
  virtual CarbonChangeType* getChangeArrayRef() = 0;

  //! Recomputes the change mask based on the current value.
  /*!
    This effectively makes a deposit of the same value seen as an
    edge.
    This function defaults to doing nothing. Only scalar and 1 bit
    vector input derivations should override this.
  */
  virtual void recomputeChangeMask();

  //! Get the control mask for this net. 
  /*!
    Note: scalars will always return NULL.
  */
  virtual const UInt32* getControlMask() const = 0;

  /* --------------------- */
  /* Non-virtual Functions */

  //! Set the name of this net
  void setName(HierName* name);

  //! True if the net is connected to a pullup/pulldown
  static bool isPulled(NetFlags flags);

  //! Compares and updates the shadow value to the current value
  ValueState compareAndUpdate(Storage* shadow);

  //! Compares and updates the shadow value to the current value
  /*!
    Updates shadow, value, and drive if the value changed.
  */
  ValueState compareUpdateExamine(Storage* shadow, UInt32* value, UInt32* drive);
  
  
  //! Compares and updates the shadow value to the current value/drive unresolved
  /*!

    Updates shadow, value, and drive if the value or drive changes. It
    does not resolve the value or drive with the external value before
    determining if it changed. This means it only looks at the Carbon
    value.

    Note: that this function's base implementation is to use
    update. That is because for all types but tri-states, we don't do
    any resolution.

  */
  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive);
  
  
  //! Get the name of this net
  inline HierName* getName() {
    const ShellNet* me = const_cast<const ShellNet*>(this);
    return const_cast<HierName*>(me->getName());
  }

  //! Get the name of this net - read only
  inline const HierName* getName() const {
    return mName;
  }

  //! Get the name as a symbol leaf
  inline STAliasedLeafNode* getNameAsLeaf()
  {
    const ShellNet* me = const_cast<const ShellNet*>(this);
    return const_cast<STAliasedLeafNode*>(me->getNameAsLeaf());
  }

  //! Get symbol leaf - read only
  inline const STAliasedLeafNode* getNameAsLeaf() const
  {
    return (const STAliasedLeafNode*) (getName());
  }
  
  inline const ShellDataBOM* getBOMData() const
  {
    return ShellSymTabBOM::getStorageDataBOM(getNameAsLeaf());
  }
  
  //! Get the number of references to this net.
  UInt16 getRefCnt() const { return mRefCount; }

  //! Set the reference count. Used by wrapper classes.
  void putRefCnt(UInt16 cnt) { mRefCount = cnt; }
  
  //! Increment the reference count
  void incrCount();

  //! Decrement the reference count
  /*!
    Returns true if the reference count reaches zero. The reference
    count will NOT go below zero.
  */
  bool decrCount();

  //! cast this net to a ShellNetWrapper
  ShellNetWrapper* castShellNetWrapper()
  {
    const ShellNet* me = this;
    return const_cast<ShellNetWrapper*>(me->castShellNetWrapper());
  }

  //! cast this net to a CarbonMemory
  CarbonMemory* castMemory()
  {
    const ShellNet* me = this;
    return const_cast<CarbonMemory*>(me->castMemory());
  }

  //! cast this net to a CarbonModelMemory
  CarbonModelMemory* castModelMemory()
  {
    const ShellNet* me = this;
    return const_cast<CarbonModelMemory*>(me->castModelMemory());
  }

  //! Dynamic cast this net to a CarbonVectorBase
  CarbonVectorBase* castVector()
  {
    const ShellNet* me = this;
    return const_cast<CarbonVectorBase*>(me->castVector());
  }
  
  //! non-const cast to CarbonScalarBase
  CarbonScalarBase* castScalar()
  {
    const ShellNet* me = this;
    return const_cast<CarbonScalarBase*>(me->castScalar());
  }

  //! non-const cast to CarbonForceNet
  CarbonForceNet* castForceNet()
  {
    const ShellNet* me = this;
    return const_cast<CarbonForceNet*>(me->castForceNet());
  }

  //! non-const cast to a CarbonExprNet
  CarbonExprNet* castExprNet()
  {
    const ShellNet* me = const_cast<const ShellNet*>(this);
    return const_cast<CarbonExprNet*>(me->castExprNet());
  }

  //! run a net value change callback based on a full shadow
  /*!
    This examines the current value, compares the value against the
    shadow, updates the shadow, and if the value did change calls the
    user's callback.
    
    Note: if this is a two state primitive net, the drive portion of
    the fullShadow is ignored. It is not compared against and it is
    not updated.
  */
  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const = 0;
protected:

  //! Returns a const version of this
  virtual const ShellNet* castShellNet() const;

  bool isDepositAllowed(CarbonModel* model, const ShellDataBOM** bomPtr)
  {
    const ShellDataBOM* bom = getBOMData();
    *bomPtr = bom;
    bool isDepositable = bom->isDepositable();
    if (! isDepositable)
    {
      if (model->issueDepositComboWarning())
        isDepositable = ShellGlobal::reportNotDepositable(getNameAsLeaf(), model);
      else
        // allow deposit if msg sev allows
        isDepositable = true;
    }
    return isDepositable;
  }
  
  void doUpdateVHM(bool changed, CarbonModel* model)
  {
    const ShellDataBOM* bom = getBOMData();
    bool runCombo = bom->isComboRun() & changed;
    model->getHookup ()->addRunDepositComboSched(runCombo);
    // For now, include all deposits including ones that don't change
    // as a 'seen' deposit
    model->getHookup()->setSeenDeposit();
  }

  //! Format a net's value
  static CarbonStatus sDoFormat(char* valueStr, size_t len, 
                                CarbonRadix strFormat, 
                                const UInt32* val, 
                                const UInt32* idrive,
                                const UInt32* overrideMask,
                                int bitWidth, CarbonModel* model);

  
private:
  //forbid
  ShellNet(const ShellNet&);
  ShellNet& operator=(const ShellNet&);

  HierName* mName;
  UInt16 mRefCount;
};

//! Class for primitive nets
/*!
  This is an object to classify nets that are directly connected to
  the storage in the generated code. In other words, these are the
  nets that actually query and manipulate the Carbon Model.

  Constants are the exception. They do not reference storage in the
  vhm, but they are still considered primitive.
*/
class ShellNetPrimitive : public ShellNet
{
public:
  CARBONMEM_OVERRIDES

  //! virtual destructor
  virtual ~ShellNetPrimitive();

  //! returns NULL. Forcibles (wrappers) are not primitive
  virtual const CarbonForceNet* castForceNet() const;

  //! returns NULL. Expressions (wrappers) are not primitive
  virtual const CarbonExprNet* castExprNet() const;

  //! returns NULL. Wrappers are not primitive
  virtual const ShellNetWrapper* castShellNetWrapper() const;

  //! Asserts. For now, we use the compare/update methodology.
  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const;

protected:
  bool* mSeenDeposit;
};

#endif
