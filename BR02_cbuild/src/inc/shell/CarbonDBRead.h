// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONDBREAD_H_
#define __CARBONDBREAD_H_


#ifndef __STFIELDBOM_H_
#include "symtab/STFieldBOM.h"
#endif
#ifndef __CBUILDSHELLDB_H_
#include "iodb/CbuildShellDB.h"
#endif
#ifndef _IODB_TYPES_H_
#include "iodb/IODBTypes.h"
#endif

#ifndef __CBUILDSHELLDB_H_
#include "iodb/CbuildShellDB.h"
#endif

#ifndef __STALIASEDLEAFNODE_H_
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STBranchNode.h"
#endif

class HdlHierPath;
class ScheduleFactory;
class SCHSignature;
class SCHScheduleFactory;
class ZistreamDB;
class ZostreamDB;
class ShellData;
class SymNodeDBExprContext;
class IODBRuntime;
class ESFactory;
class CarbonExpr;
class DynBitVectorFactory;
class STSymbolTableNode;
class ShellSymTabBOM;
class UserTypeFactory;

//! Shell-side symbol table leaf data
class ShellDataBOM
{
public: 
  CARBONMEM_OVERRIDES

  //! constructor
  ShellDataBOM();

  //! destructor
  ~ShellDataBOM();

  //! print contents to stdout
  void print() const;

  //! prints contents to stdout for carbondb
  void carbondbPrint() const;

  //! Get the net flags
  inline NetFlags getNetFlags() const
  {
    return mNetFlags;
  }
  
  //! Is this node's value described by an expression
  bool isExpression() const;

  //! Get the type dictionary index
  UInt32 getTypeIndex() const;

  //! Get the type tag
  /*!
    This is meant to be compared against the
    CbuildShellDB::TypeIndexTag enum
  */
  int getTypeTag() const;

  //! Get the node flags
  CbuildShellDB::NodeFlags getNodeFlags() const;

  //! Get the intrinsic type information
  const IODBIntrinsic *getIntrinsic() const;

  //! Get the storage offset from base
  SInt32 getStorageOffset() const;
  
  //! Get the value of the constant <= 32 bits
  UInt32 getStorageValueWord() const;

  //! Get the value of the constant > 32 bits
  const DynBitVector* getStorageValue() const;

  //! Get the modifiable schedule signature
  SCHSignature* getSCHSignature();
  
  //! Get the const schedule signature
  const SCHSignature* getSCHSignature() const;

  //! Does this node represent a dead net?
  bool isDeadNet() const;

  //! Get the modifiable ShellData object
  ShellData* getShellData();

  //! Get const ShellData
  const ShellData* getShellData() const;
  
  //! Set the net flags
  void setNetFlags(NetFlags netFlags);
  
  //! Set the tag type index word.
  void setTagTypeIndex(UInt32 tagTypeIndex);

  //! Set the node flag enum
  void setNodeFlags(CbuildShellDB::NodeFlags nodeFlags);
  
  //! Set the intrinsic
  void setIntrinsic( const IODBIntrinsic *intrinsic );
  
  //! Set the storage offset
  void setStorageValue(const CbuildShellDB::StorageValue& storageInfo);
  
  //! Set the signature 
  /*!
    \warning This memory is shared with the signature index.
  */
  void setSCHSignature(SCHSignature* signature);

  //! Set the ShellData
  void setShellData(ShellData* shlData);

  //! Set the expression
  void setExpr(CarbonExpr* expr);

  //! Get the expression
  /*!
    If NULL, the node's expression is itself.
  */
  const CarbonExpr* getExpr() const;

  //! non-const version of getExpr
  CarbonExpr* getExpr();

  //! Place user type information.
  void setUserType(const UserType* ut);

  //! Get user type or NULL if this leaf has no user type information.
  const UserType* getUserType() const;

  void putIsDepositable()
  {
    mNodeFlags = CbuildShellDB::NodeFlags(mNodeFlags | CbuildShellDB::eDepositable);
  }

  void putIsObservable()
  {
    mNodeFlags = CbuildShellDB::NodeFlags(mNodeFlags | CbuildShellDB::eObservable);
  }

  //! True if this is a value or mask net of a forcible
  bool isForceSubordinate() const {
    return ((mNodeFlags & CbuildShellDB::eForceSubord) != 0);
  }

  //! Is this ring depositable?
  bool isDepositable() const {
    return ((mNodeFlags & CbuildShellDB::eDepositable) != 0);
  }

  //! Is this ring observable
  /*!
    Rings are marked observable only if they are outputs or explicitly
    marked observable or forcible.
  */
  bool isObservable() const {
    return ((mNodeFlags & CbuildShellDB::eObservable) != 0);
  }

  //! Is this ring a state output?
  bool isStateOutput() const {
    return ((mNodeFlags & CbuildShellDB::eStateOutput) != 0);
  }
  
  bool needsDebugSched() const {
    return ((mNodeFlags & CbuildShellDB::eRunDebugSched) != 0);
  }

  bool isInvalidWaveNet() const {
    return ((mNodeFlags & CbuildShellDB::eInvalidWaveNet) != 0);
  }

  bool isComboRun() const {
    return ((mNodeFlags & CbuildShellDB::eComboRun) != 0);
  }

  bool isClock() const {
    return ((mNodeFlags & CbuildShellDB::eClock) != 0);
  }
  
private:
  NetFlags mNetFlags;
  UInt32 mTagTypeIndex;
  const IODBIntrinsic *mIntrinsic;
  CbuildShellDB::NodeFlags mNodeFlags;
  SCHSignature* mSignature;
  CbuildShellDB::StorageValue mStorageValue;
  CarbonExpr* mExpr;
  const UserType* mUserType;

  // other shell needs
  ShellData* mShellData;

  void init();
  void printNodeFlags() const;
  void printStorage(UInt32 indent) const;

  class DeadCheckWalk;
};

//! Shell-side symbol table branch data
class ShellBranchDataBOM
{
public: 
  CARBONMEM_OVERRIDES

  //! constructor
  ShellBranchDataBOM();

  //! destructor
  ~ShellBranchDataBOM();

  //! print contents to stdout
  void print() const;

  //! Get the hier flags
  inline HierFlags getHierFlags() const { return mHierFlags; }

  //! Get the language
  HierFlags getSourceLanguage() const
  {
    return HierFlags(mHierFlags & eLanguageMask);
  }

  //! Get the hierarchy type
  HierFlags getHierType() const { return HierFlags(mHierFlags & eHierTypeMask); }

  //! Get the module name or NULL if it is not a module
  StringAtom* getModuleName() const { return mModuleName; }

  //! Get the user type or NULL if there's no user type information, for this branch.
  const UserType* getUserType() const { return mUserType; }

private:
  //! Set the hier flags (Can only be set by ShellSymTabBOM class)
  void setHierFlags(HierFlags hierFlags) { mHierFlags = hierFlags; }

  //! Set the module name (Can only be set by ShellSymTabBOM class)
  void setModuleName(StringAtom* moduleName) { mModuleName = moduleName; }

  //! Set the branch node user type information.
  void setUserType(const UserType* ut) { mUserType = ut; }

  //! Allow ShellSymTabBOM to set fields
  friend class ShellSymTabBOM;

private:
  //! The hierarchy flags which is the hier type and source language
  HierFlags mHierFlags;

  //! The module name if this is a module or NULL
  /*! TBD: Do we need to tf or decl scope name?
   */
  StringAtom* mModuleName;

  //! User type information for branch node.
  const UserType* mUserType;
}; // class ShellBranchDataBOM


//! Shell-side SymbolTable Bill Of Materials Manager
class ShellSymTabBOM : public STFieldBOM
{
public: 
  CARBONMEM_OVERRIDES

  //! constructor
  ShellSymTabBOM();

  //! virtual destructor
  virtual ~ShellSymTabBOM();
  
  //! STFieldBOM::allocLeafData()
  virtual Data allocLeafData();

  //! STFieldBOM::allocBranchData()
  virtual Data allocBranchData();
  
  //! STFieldBOM::freeLeafData()
  virtual void freeLeafData(const STAliasedLeafNode*, Data* bomdata);

  //! STFieldBOM::freeBranchData()
  virtual void freeBranchData(const STBranchNode*, Data* bomdata);

  //! Does nothing
  virtual void preFieldWrite(ZostreamDB& out);

  //! Write the BOMData for a leaf node
  virtual void writeLeafData(const STAliasedLeafNode* leaf, ZostreamDB& out) const;

  //! Currently asserts - not legal
  virtual void writeBranchData(const STBranchNode* branch, ZostreamDB& out,
                               AtomicCache*) const;
  //! Currently asserts - not legal
  virtual void writeBOMSignature(ZostreamDB& out) const;

  //! Read the r/w interface signature
  virtual ReadStatus readBOMSignature(ZistreamDB& in, UtString* errMsg);

  //! STFieldBOM::printLeaf()
  virtual void printLeaf(const STAliasedLeafNode* leaf) const;

  //! STFieldBOM::printBranch()
  virtual void printBranch(const STBranchNode* branch) const;

  //! STFieldBOM::preFieldRead()
  virtual ReadStatus preFieldRead(ZistreamDB& in);

  //! Read BOMData for a leafnode
  virtual ReadStatus readLeafData(STAliasedLeafNode* leaf, ZistreamDB& out, 
                                  MsgContext* msg);
  
  //! Read BOMData for a branchnode - currently reads nothing
  virtual ReadStatus readBranchData(STBranchNode* branch, ZistreamDB& out, 
                                    MsgContext* msg);

 
  //! Return BOM class name
  virtual const char* getClassName() const
  {
    return "ShellSymTabBOM";
  }

  //! Write the BOMData for a branch node
  virtual void xmlWriteBranchData(const STBranchNode* /*branch*/,UtXmlWriter* /*writer*/) const
  {
  }

  //! Write the BOMData for a leaf node
  virtual void xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/,UtXmlWriter* /*writer*/) const
  {
  }


  
  //! This gets and casts the opaque bomdata pointer to a ShellDataBom*
  static inline ShellDataBOM* getLeafBOM(const STAliasedLeafNode* leaf)
  {
    return static_cast<ShellDataBOM*>(leaf->getBOMData());
  }

  //! This gets and casts the opaque bomdata pointer to a ShellDataBom*
  static inline ShellDataBOM* getLeafBOM(const STSymbolTableNode* node)
  {
    const STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf != NULL) {
      return ShellSymTabBOM::getLeafBOM(leaf);
    } else {
      return NULL;
    }
  }

  //! This gets and casts the opaque bomdata pointer to a ShellBranchDataBom*
  static inline ShellBranchDataBOM* getBranchBOM(const STBranchNode* branch)
  {
    return static_cast<ShellBranchDataBOM*>(branch->getBOMData());
  }

  //! This gets and casts the opaque bomdata pointer to a ShellBranchDataBom*
  static inline ShellBranchDataBOM* getBranchBOM(const STSymbolTableNode* node)
  {
    const STBranchNode* branch = node->castBranch();
    if (branch != NULL) {
      return ShellSymTabBOM::getBranchBOM(branch);
    } else {
      return NULL;
    }
  }

  //! Composes the proper name for the given node
  /*!
    Fixes up expression node name to give a name in terms of the
    original node.
    \param node Node from which to get name
    \param buf Where the name goes
    \param noBackPointer If the node is a split net and this is true do
    not print the backpointer expression as the name but the actual
    name of the split node ($portsplit...). False if the backpointer
    name is desired.
    \param includeVectorRange For non-expression nodes, if the vector
    range is not desired set this to false. Has no effect on an
    expression node.
  */
  static void composeName(const STSymbolTableNode* node, UtString* buf, bool noBackPointer, 
                          bool includeVectorRange);

  //! Composes the proper leaf name for the given node
  /*!
    Fixes up the expression node name to give a leaf name in terms of
    the original node.
    \param node Node from which to get leaf name
    \param buf Where the leaf name goes
    \param includeVectorRange For non-expression nodes, if the vector
    range is not desired set this to false. Has no effect on an
    expression node
  */
  static void composeLeafName(const STSymbolTableNode* node, UtString* buf, 
                              bool includeVectorRange);

  //! Returns a backpointer of an expression net, if it exists
  /*!
    Port split nodes point back to the original node through a back
    pointer identifier bit or partselect. This returns that
    expression. NULL if there is no back pointer.
  */
  static const CarbonExpr* getBackPointer(const STSymbolTableNode* node);
  
  //! Returns the storage node's ShellDataBOM
  static inline const ShellDataBOM* getStorageDataBOM(const STAliasedLeafNode* leaf)
  {
    const STAliasedLeafNode* storage = leaf->getStorage();
    return ShellSymTabBOM::getLeafBOM(storage);
  }

  //! Place a populated schedulefactory into the fieldBOM
  /*!
    This will iterate through the signatures and create a signature to
    index mapping to be written to the db.
  */
  void putScheduleFactory(SCHScheduleFactory* sf);

  //! Place a UserTypeFactory into the fieldBOM.
  void putUserTypeFactory(UserTypeFactory* utf);

  //! Read the symbol table data using db file and message context
  bool readData(ZistreamDB& in, IODBRuntime* iodb, MsgContext* mc);

  //! Enumeration for various BOM capabilities
  enum Capabilities
  {
    eSTCBranchBOMV1 = 0x00000001,  //!< Can read branch bom data (none before)
    eSTCUserTypeV1  = 0x00000002   //!< Can read user type for leaf/branch bom data
  };

  //! put the shell symbol table BOM capabilities
  void addCapability(Capabilities capability)
  {
    mCapabilities = Capabilities(mCapabilities|capability);
  }

  //! Does the BranchBOMV1 capability exist?
  bool hasBranchBOMV1Capability() const
  {
    return (mCapabilities & eSTCBranchBOMV1) != 0;
  }

  bool hasUserTypeV1Capability() const
  {
    return (mCapabilities & eSTCUserTypeV1) != 0;
  }

private:
  SCHScheduleFactory* mScheduleFactory;
  UserTypeFactory* mUserTypeFactory;
  Capabilities mCapabilities;
  
  SymNodeDBExprContext* mExprDBContext;
  ESFactory* mExprFactory;

}; // class ShellSymTabBOM : public STFieldBOM


#endif
