// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __SHELL_NET_ON_DEMAND_H__
#define __SHELL_NET_ON_DEMAND_H__

#include "shell/ShellNetWrapper.h"
#include "shell/OnDemandDeposits.h"

class OnDemandMgr;
class OnDemandDebugInfo;

//! onDemand shell net wrapper
/*!
  This class manages examines, deposits, etc. to nets while in
  onDemand mode.  If onDemand has detected a repeating idle state,
  certain net API functions are not actually applied to the model -
  they're just communicated to the onDemand manager.  Other API
  functions are actually executed, but require the onDemand manager
  to update the model's state first.

  Only deposit() and fastDeposit() with null drive are recorded by
  OnDemandMgr.  All other deposit-like API calls are considered
  non idle accesses and force onDemand into normal mode.

  Additionally, only some nets themselves have their deposits tracked
  by the OnDemand engine.  These nets are labeled as idle deposits.
  Since stimulus during idle conditions is likely to be simply clock
  transitions, only clock nets (plus user-specified nets) are idle
  deposits.

  Since deposits to non idle deposit nets hinder onDemand from finding and
  remaining in an idle state, local change detection is done on
  deposit() and depositFast() calls to non idle deposit nets.  Also, since
  examine() would normally force a state restore when onDemand is
  idle, its results are cached in OnDemandMgr and queried before
  actually restoring the Carbon Model state.
 */
class ShellNetOnDemand : public ShellNetWrapper1To1
{
public:
  CARBONMEM_OVERRIDES

  //! Constructs an onDemand wrapper for a net
  ShellNetOnDemand(ShellNet *net, OnDemandMgr *mgr);
  virtual ~ShellNetOnDemand();

  virtual const ShellNetOnDemand *castShellNetOnDemand() const;

  // The following functions need to be reimplemented
  // based on the onDemand state
  virtual bool isDataNonZero() const;
  virtual bool setToDriven(CarbonModel* model);
  virtual bool setToUndriven(CarbonModel* model);
  virtual bool setWordToUndriven(int index, CarbonModel* model);
  virtual bool resolveXdrive(CarbonModel* model);
  // The base class's version of this should be OK
//   virtual void getExternalDrive(UInt32* drive) const;
  virtual bool setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model);
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  virtual CarbonStatus examine (CarbonReal* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const;
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  virtual CarbonStatus force(const UInt32* buf, CarbonModel* model);
  virtual CarbonStatus forceWord(UInt32 buf, int index, CarbonModel* model);
  virtual CarbonStatus forceRange(const UInt32* buf, 
                                  int range_msb, int range_lsb, 
                                  CarbonModel* model);
  virtual CarbonStatus release(CarbonModel* model);
  virtual CarbonStatus releaseWord(int index, CarbonModel* model);
  virtual CarbonStatus releaseRange(int range_msb, int range_lsb, CarbonModel* model);
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel* model) const;
  virtual void putToZero(CarbonModel* model);
  virtual void putToOnes(CarbonModel* model);
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);
  virtual int hasDriveConflict() const;
  virtual int hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const;
  // The base class's version of this should be OK
//   virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  // Now, the memory functions...
  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;
  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;
  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *buf, int range_msb, int range_lsb) const;
  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);
  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);
  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32* buf, int range_msb, int range_lsb);
  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;
  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;
  virtual CarbonStatus readmemh(const char* filename);
  virtual CarbonStatus readmemb(const char* filename);
  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);
  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);


  //! Returns whether this net is an idle deposit net
  /*!
    For performance reasons, onDemand won't attempt to find a repeating state if
    the model is unlikely to be idle.  Heuristics of the net, plus user directives determine which nets,
    when written, indicate that an idle state is unlikely.  The remaining nets don't rule out an idle state,
    and therefore are elibigle for stimulus tracking.
   */
  bool isIdleDeposit() const { return mIdleDeposit; }

  //! Checks if the deposit request matches the most recent one, and updates the shadow copy
  bool shadowChanged(const UInt32 *val, const UInt32 *drive);

  OnDemandDebugInfo *getDebugInfo() const { return mDebug; }

  //! Instruments the net for onDemand debug
  void initDebug();

  //!< Stores the deposit buffer entry
  void setDepositEntry(OnDemandDeposits::NetMapEntry *entry)
  {
    mDepEntry = entry;
  }

  //! Returns whether this net is a clock
  /*! 
    Clocks need special attention, because we need to record
    multiple deposits per schedule call, to allow fake edges.
   */
  bool isClock() const { return mIsClock; }

  void makeIdleDeposit()
  {
    mIdleDeposit = true;
    // Don't shadow elibigle nets
    mDoShadow = false;
  }

protected:
  OnDemandMgr *mMgr;            //!< The onDemand manager
  bool mIdleDeposit;            //!< Does this net have its deposits tracked when looking for an idle state?
  bool mDoShadow;               //!< Should we do deposit shadow value comparisons?
  bool mShadowValid;            //!< Is the shadow deposit value we have stored accurate?
  UInt32 *mShadowValue;         //!< Shadow value of last deposit
  UInt32 *mShadowDrive;         //!< Shadow drive of last deposit
  bool mShadowDriveNull;        //!< Did the last deposit use a null drive?
  UInt32 mShadowSize;           //!< Size of shadow buffers
  OnDemandDebugInfo *mDebug;    //!< Representation of this net for onDemand debug
  OnDemandDeposits::NetMapEntry *mDepEntry;     //!< Information about this net's location in the OnDemandDeposits buffers
  bool mIsClock;                //!< Is this net a clock?

private:
  //! Hide
  ShellNetOnDemand(const ShellNetOnDemand&);
  ShellNetOnDemand &operator=(const ShellNetOnDemand&);
};

#endif
