// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __ON_DEMAND_DEBUG_H__
#define __ON_DEMAND_DEBUG_H__

#include "util/UtArray.h"
#include "shell/OnDemandMgr.h"

class STAliasedLeafNode;

//! Class to represent reasons for mode changes
class OnDemandDebugInfo
{
public:
  CARBONMEM_OVERRIDES

  OnDemandDebugInfo(CarbonOnDemandDebugAction action, const UtString &name)
    : mAction(action),
      mName(name)
  {}

  ~OnDemandDebugInfo() {}

  const CarbonOnDemandDebugAction mAction;           //!< Event action
  const UtString mName;         //!< Entity name
};

//! Class to represent a debug event callback function
class OnDemandDebugCB : public CarbonOnDemandCBData
{
public:
  CARBONMEM_OVERRIDES

  OnDemandDebugCB(CarbonOnDemandDebugCBFunc fn, void *userData);
  virtual ~OnDemandDebugCB();

  //! Runs the callback function, if enabled, with the given debug event data
  void run(CarbonObjectID *obj,
           CarbonTime simTime,
           CarbonOnDemandDebugType type,
           CarbonOnDemandDebugAction action,
           const char *path,
           CarbonUInt32 length);

protected:
  CarbonOnDemandDebugCBFunc mCBFunc;                //!< Debug callback function pointer
  void *mCBData;                                    //!< Debug callback data
};

//! Class for storing and reporting onDemand debug events
/*!
  This class is responsible for implementing the user interface and
  some core functionality of OnDemand debug mode.  The class manages
  reporting debug events via a callback function, along with filtering
  events based on verbosity level.  It also maps offsets in the
  OnDemandState object to the design node whose value is stored there.
 */
class OnDemandDebug
{
public:
  CARBONMEM_OVERRIDES
  OnDemandDebug(OnDemandMgr *mgr);
  ~OnDemandDebug();

  //! Validate the size of the debug offset map against the OnDemandState size
  void checkSize(UInt32 size);
  //! Determine why a repeating pattern was not found
  void analyzeFailedPattern(const OnDemandMgr::DepositArray &dep_array,
                            const OnDemandMgr::StateArray &state_array,
                            const OnDemandMgr::SchedTypeArray &sched_array,
                            UInt32 num_states);
  //! Report a debug event
  /*!
    The length parameter is only meaningful if the reason is eCarbonOnDemandDebugFail.
   */
  void report(CarbonOnDemandDebugType reason, OnDemandDebugInfo *debug_info, UInt32 length);

  //! Record a debug info object for later reporting
  void record(OnDemandDebugInfo *debug_info)
  {
    mInfo = debug_info;
  }

  //! Clear the recorded debug info object;
  void clear()
  {
    mInfo = NULL;
  }
  
  //! Register a callback function
  CarbonOnDemandCBDataID *addCB(CarbonOnDemandDebugCBFunc fn, void *userData);

  //! Set the event reporting level
  void setLevel(CarbonOnDemandDebugLevel level)
  {
    mLevel = level;
  }

  //! Are we reporting all events?
  bool reportAllEvents() const
  {
    return (mLevel == eCarbonOnDemandDebugAll);
  }

  //! Attempt to exclude a net from state comparisons
  CarbonStatus exclude(CarbonNetID *net);

  //! Attempt to exclude a leaf from state comparisons
  CarbonStatus exclude(STAliasedLeafNode *leaf);

  //! Mask out excluded offsets from the state buffer
  void maskExcluded(OnDemandState *stateBuffer);

  //! Pick a user-friendly name for this leaf
  static void makeFriendlyName(const STAliasedLeafNode *leaf, UtString *name);

protected:
  //! Map offsets in the state array to design nets
  void mapDebugOffsets();

  typedef UtArray<UInt32> UInt32Array;
  //! Find deposits and internal nets that didn't repeat in a pattern, returning success
  bool findNonRepeatingNets(const OnDemandMgr::DepositArray &dep_array,
                            const OnDemandMgr::StateArray &state_array,
                            const OnDemandMgr::SchedTypeArray &sched_array,
                            UInt32 num_states,
                            UInt32Array *pattern_lengths);

  //! Determines the least common multiple of an array of lengths
  static UInt32 sFindLCM(const UInt32Array &patternLengths);

  //! Remove entries with larger multiples from the pattern length array
  static void sFilterMultiples(UInt32Array *patternLengths, UInt32 maxLength);

  OnDemandMgr *mMgr;                    //!< The onDemand manager
  OnDemandDebugInfo *mInfo;             //!< The saved debug info object
  typedef UtArray<OnDemandDebugCB*> DebugCBArray;
  DebugCBArray mDebugCBs;               //!< All registered debug event callbacks
  CarbonOnDemandDebugLevel mLevel;      //!< The current reporting level

  // For fast lookup, we store a pointer to a debug info object at
  // each offset in the debug map.  However, if a net spans multiple
  // offsets, only one debug info object is allocated, so we track the
  // allocated objects separately.
  typedef UtArray<OnDemandDebugInfo*> DebugArray;
  DebugArray mDebugMap;                 //!< Map of state offsets to debug info
  DebugArray mAllocatedNodes;           //!< The allocated info objects

  OnDemandDebugInfo mDepInfo;           //!< Common debug info object for reporting non-repeating deposits
  OnDemandDebugInfo mSchedInfo;         //!< Common debug info object for reporting non-repeating schedules

  // To allow runtime exclusion of state points, we maintain a map of
  // symtab leaves to their offset/size in the state buffer.
  typedef std::pair<UInt32, UInt32> OffsetSizePair;
  typedef UtHashMap<STAliasedLeafNode*, OffsetSizePair> LeafOffsetMap;
  LeafOffsetMap mLeafOffsets;

  // A list of the offset/sizes to be excluded
  typedef UtArray<OffsetSizePair*> ExclusionList;
  ExclusionList mExclusions;

  //! Special offsets within the array of pattern lengths created on a failed pattern
  enum {
    eOffsetDeposits       = 0,          //!< Non-repeating deposits
    eOffsetSchedule       = 1,          //!< Non-repeating schedule types
    eOffsetNum            = 2           //!< Number of offsets
  };

private:
  //! Hide
  OnDemandDebug(const OnDemandDebug&);
  OnDemandDebug &operator=(const OnDemandDebug&);
};

#endif
