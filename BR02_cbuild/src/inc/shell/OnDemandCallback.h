// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __ON_DEMAND_CALLBACK_H__
#define __ON_DEMAND_CALLBACK_H__

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#include "carbon/carbon_shelltypes.h"
#include "util/UtArray.h"
#include "util/UtHashMap.h"

class CarbonNetValueCBData;
class ShellNet;
class OnDemandCallbackFactory;

//! onDemand control for a single callback
/*!
  Net change callbacks are handled in onDemand by replacing the
  user's callback function and data with an intercept function and an
  instance of this class.  When a callback triggers, this class saves
  the fact that it was called, along with the net's value/drive, and
  then calls the user's function with the original user data.

  As OnDemandMgr saves states in a repeating pattern, it includes all
  the callbacks that ran on that schedule call.  Then, when in idle
  mode, it can just use the saved value/drive to call the user's
  function.
 */
class OnDemandCallback
{
public:
  CARBONMEM_OVERRIDES

  friend class OnDemandCallbackFactory;

  //! Wrapper function to intercept the normal mode callback
  static void sInterceptCB(CarbonObject *, CarbonNet *, CarbonClientData userdata, CarbonUInt32 *val, CarbonUInt32 *drive);

  //! Resets the callback, clearing whether it was called
  void reset();

  //! Runs the original callback
  void run();

  //! Was this callback called this schedule?
  bool wasCalled() { return mCalled; }

protected:
  CarbonNetValueCBData *mCB;            //!< The original complete callback data structure
  CarbonNetValueCB mCBFunc;             //!< The original callback function
  CarbonClientData mCBData;             //!< The original callback data

  CarbonObject *mObj;                   //!< The Carbon model for the callback
  ShellNet *mNet;                       //!< The net for the callback

  bool mCalled;                         //!< Was this callback called this schedule call?
  const bool *mEnabled;                 //!< Pointer to whether this callback is currently enabled/disabled
  CarbonUInt32 mNumUInt32;              //!< Number of UInt32s needed to store the value/drive
  CarbonUInt32 mSize;                   //!< Number of bytes needed to store the value/drive
  CarbonUInt32 *mVal;                   //!< Buffer containing the net's new value
  CarbonUInt32 *mDrive;                 //!< Buffer containing the net's new drive

  //! Records that this callback was called with the specified value/drive
  void setCalled(const CarbonUInt32 *val, const CarbonUInt32 *drive);

private:
  //! Constructs an onDemand callback to wrap a raw callback
  OnDemandCallback(CarbonObject *obj, CarbonNetValueCBData *cb, const bool *enabled);
  //! Copy constructor
  OnDemandCallback(const OnDemandCallback &other);
  ~OnDemandCallback();

  //! Hide
  OnDemandCallback &operator=(const OnDemandCallback&);
};

//! A collection of all known registered callbacks
/*!
  OnDemandMgr has a master instance of this class, which contains all

  the currently-registered callbacks in the Carbon Model.  Copies of the
  collection are made as necessary to store along with the states in
  the repeating sequence.
 */
class OnDemandCallbackCollection
{
public:
  CARBONMEM_OVERRIDES

  friend class OnDemandCallbackFactory;

  //! Adds a callback to the collection
  void add(OnDemandCallback *cb);

  //! Resets all the callbacks in the collection
  void reset();

  //! Runs all the callbacks in the collection
  void run();

  //! Returns whether any of the saved callbacks were run this schedule call
  bool anyCalled();

protected:
  typedef UtArray<OnDemandCallback*> CallbackArray;
  CallbackArray mCallbacks;                     //!< All the callbacks we know about
  CarbonUInt32 mNumCallbacks;                   //!< Number of callbacks in the collection

private:
  OnDemandCallbackCollection();
  ~OnDemandCallbackCollection();

  //! Hide
  OnDemandCallbackCollection(const OnDemandCallbackCollection&);
  OnDemandCallbackCollection &operator=(const OnDemandCallbackCollection&);
};


//! Factory class for allocating objects and managing storage
/*!

  The factory distinguishes between protected and non-protected
  objects.  Protected objects exist for the entire lifetime of the
  Carbon Model, whereas non-protected ones exist only for the lifetime of a
  repeating idle sequence.

  There is only one protected callback collection, which serves as a
  master copy for OnDemandMgr.  When a callback is registered, a
  protected OnDemandCallback is added to the master copy.  This master
  copy always reflects the current state of the callbacks in the Carbon Model,
  i.e. whether or not they were called on the most recent schedule,
  and their nets' value/drive.

  When a callback collection needs to be saved as part of a sequence,
  a non-protected copy of the master collection is made.  All these
  copies are deallocated when an idle sequence is aborted.

  The master copy needs to persist because it contains information
  that is only available during callback registration, such as the
  user's registered callback function and data.

  The clear() method deallocates non-protected objects, and clearAll()
  deallocates everything.
 */
class OnDemandCallbackFactory
{
public:
  CARBONMEM_OVERRIDES

  OnDemandCallbackFactory();
  ~OnDemandCallbackFactory();

  //! Allocates a new callback, owning storage
  OnDemandCallback *createCallback(CarbonObject *obj, CarbonNetValueCBData *cb);

  //! Allocates a new callback collection, owning storage
  OnDemandCallbackCollection *createCollection();

  //! Allocates a new copy of a callback collection, owning all storage
  OnDemandCallbackCollection *copyCollection(const OnDemandCallbackCollection *other);

  //! Frees all allocated objects
  void clear();

  //! Frees all allocated objects, including protected ones
  void clearAll();

  //! sets/clears the enabled flag
  void setEnable(CarbonNetValueCBData *cb, bool enabled);

protected:
  typedef UtArray<OnDemandCallback *> CallbackArray;
  typedef UtArray<OnDemandCallbackCollection *> CollectionArray;
  CallbackArray mCallbacks;                             //!< Allocated callbacks that will be freed on a clear
  CollectionArray mCollections;                         //!< Allocated collections that will be freed on a clear
  OnDemandCallbackCollection *mProtectedCollection;     //!< Single protected collection that will not be freed on a clear
  CallbackArray mProtectedCallbacks;                    //!< Protected callbacks that will not be freed on a clear
  typedef UtHashMap<CarbonNetValueCBData*, bool*> CBEnabledMap;
  CBEnabledMap mEnabledMap;                             //!< Map of callback data to its current enabled/disabled status
  typedef UtArray<bool*> EnablePoolArray;
  EnablePoolArray mEnablePool;                          //!< Pool holding arrays of bools to manage callback enable/disable status
  UInt32 mEnablesUsed;                                  //!< Number of enables used in the most recently allocated pool
  static const UInt32 scEnablePoolSize = 100;           //!< Number of enables available in each pool.

  //! Allocates a new copy of a callback, owning storage
  OnDemandCallback *copyCallback(const OnDemandCallback *other);
  //! Gets a new enable pointer for a callback, allocating a new pool if necessary
  bool *getNewEnablePointer();
};


#endif
