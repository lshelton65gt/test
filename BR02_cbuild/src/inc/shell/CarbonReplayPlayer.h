// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CARBONREPLAYPLAYER_H_
#define CARBONREPLAYPLAYER_H_

#include "shell/CarbonReplayRecorder.h"
#include "util/UtIStream.h"
#include "shell/ShellNetPlayback.h"
#include "util/UtStringArray.h"

//! An array of SInt32s
typedef UtArray<SInt32> SInt32Vec;

namespace CarbonReplay 
{
  static const char* sTagStr(Tag tag)
  {
    switch(tag)
    {
    case eCheckpoint:
      return "Checkpoint";
      break;
    case eStimuli:
      return "Stimuli";
      break;
    case eResponse:
      return "Response";
      break;
    case eSchedule:
      return "Schedule";
      break;
    case eMemWrite:
      return "MemWrite";
      break;
    case eEmptyTouchedResponse:
      return "EmptyTouchedResponse";
      break;
    case eEmptyTouchedStimuli:
      return "EmptyTouchedStimuli";
      break;
    case eEndOfFile:
      return "EndOfFile";
      break;
    case eCModelCall:
      return "CModelCall";
      break;
    case eCModelInput:
      return "CModelInput";
      break;
    case eCModelOutput:
      return "CModelOutput";
      break;
    case eCModelPreOutput:
      return "CModelPreOutput";
      break;
    case eNoEvent:
      return "NoEvent";
      break;
    }
    return "(unknown)";
  }

  typedef UtArray<UInt32*> UInt32PtrVec;

  //! Closure for DB Data
  /*!
    This holds the last read event and has assorted data by
    event. The events are not overwritten until the database
    overwrites them, ie., they are not cleared on every db event
    read.
  */
  struct EventClosure
  {
    CARBONMEM_OVERRIDES
    
    //! constructor
    /*!
      mEvent and mScheduleEvent will be set to non-sensical values,
      so any switch statement will fail. The time is set to 0.
    */
    EventClosure() : mTime(0), 
                     mWheelInterrupt(0),
                     mCheckpointNum(0),
                     mTotNumSchedCalls(0),
                     mScheduleStatus(eCarbon_OK),
                     mPrintTruncatedFileWarning(true),
                     mHasSchedEvent(false)
    {
      init();
    }
    
    void init()
    {
      // set the initial event tags to an invalid tag
      // to ensure initialization happens when reading the db
      mEvent = Tag(-1);
      mScheduleType = '\0';

      mNumStims = 0;
      mEndOfFile = false;
      mHasSchedEvent = false;
      mWheelInterrupt = 0;
      clearExternalMemChanges();
      clearInternalMemChanges();
      clearTouchedStim();
    }
      
    void clearExternalMemChanges()
    {
      // resize doesn't delete storage like clear does
      mExternalMemChangeIndices.resize(0);
    }

    void clearInternalMemChanges()
    {
      // resize doesn't delete storage like clear does
      mInternalMemChangeIndices.resize(0);
    }

    //! Easy access to the response buffer
    UInt32* getResponseBuffer()
    {
      return mResponse.getUIntArray();
    }

    /*! 
      Returns non-zero if the event is what we want or if the
      wheel was interrupted.
    */
    inline unsigned stopWheel(Tag expected) const
    {
      return (mEvent == expected) | mWheelInterrupt;
    }

    void clearTouchedStim()
    {
      mStimuliTouched.clear();
    }

    Tag mEvent;
    CarbonTime mTime;
    // Non-zero if something interrupts the wheel so that it should
    // not go any further.
    unsigned mWheelInterrupt;
    // The number of stimuli events we've seen since the last
    // checkpoint
    UInt32 mNumStims;
    // Number of checkpoints we come across while reading.
    // Warning: this does not get reset at recovery.
    UInt32 mCheckpointNum;    
    // total number of schedule calls since playback started
    // Warning: this does not get reset at recovery.
    UInt64 mTotNumSchedCalls;

  
    // Checkpoint file name
    UtString mCheckpoint;
    // Expected stimuli
    DynBitVector mExpectedStimuli;
    // Schedule Response
    DynBitVector mResponse;
    // Touched indicators for responses
    TouchedContainer mTouchedResponses;
    // Touched indicators for stimuli
    StimuliTouched mStimuliTouched;
    
    SInt32Vec mExternalMemChangeIndices;
    SInt32Vec mInternalMemChangeIndices;

    // carbonSchedule() status and type
    CarbonStatus mScheduleStatus;
    char mScheduleType;

    //! True if we saw the end of the file tag
    bool mEndOfFile;

    //! True if we should print missing end of file warnings.
    /*! A truncated file can get read twice. Once during playback
     *  and once during recovery. We disable printing the message
     *  during recovery so that we don't have a double message.
     */
    bool mPrintTruncatedFileWarning;

    // True if a schedule call or cmodel call has been encountered in
    // the current event file
    bool mHasSchedEvent;

  };


  // forward decl
  class ReplayPlayer;

  //! Object to handle playing cmodels for playback/recover modes
  class CModelPlayer
  {
  public:
    CARBONMEM_OVERRIDES
    
    CModelPlayer(ReplayPlayer* player)
      : mPlayer(player)
    {}
    
    //! virtual destructor
    virtual ~CModelPlayer();

    //! Gets called when a cmodel call is to be played back.
    virtual bool playCModelCall(ReplayCModelCall*) = 0;
    
    ReplayPlayer* getReplayPlayer() { return mPlayer; }
  protected:
    ReplayPlayer* mPlayer;
  };

  struct RecoverContext;

  //! Object to manage indexing a DynBitVector as partitions
  class DynBVPartitioner
  {
  public:
    CARBONMEM_OVERRIDES

    DynBVPartitioner() : mWordIndex(0) {}

    void putNumWords(UInt32 numWords)
    {
      mBV.resize(numWords * 32);
    }

    UInt32* allocWordPartition(UInt32 numWords)
    {
      INFO_ASSERT(mWordIndex + numWords <= mBV.getUIntArraySize(), "Allocation beyond bitvector buffer");
      UInt32* arr = mBV.getUIntArray();
      UInt32* ret = &arr[mWordIndex];
      mWordIndex += numWords;
      return ret;
    }

  private:
    DynBitVector mBV;
    UInt32 mWordIndex;
  };


  //! Object to handle bookkeeping of cmodel calls from the db.
  class ReplayCModelManager
  {
  public:
    CARBONMEM_OVERRIDES
    
    ReplayCModelManager(ReplayPlayer* player);
    
    ~ReplayCModelManager();

    //! Add a cmodel to the playback mechanism
    void addCModel(ReplayCModel* replayCModel);

    //! Clear the current call
    void clearCall()
    {
      mCurrentCModelCall = NULL;
    }
    
    //! Returns the current call. NULL, if there is no call.
    ReplayCModelCall* getCurrentCall() {
      return mCurrentCModelCall;
    }

    //! Number of formal cmodels
    UInt32  numCModels() const { return mCModels.size(); }
    
    //! Get the cmodel at the given id
    ReplayCModel* getCModel(UInt32 id)
    {
      INFO_ASSERT(id < numCModels(), "Invalid cmodel id.");
      ReplayCModel* cmodel = mCModels[id];
      INFO_ASSERT(cmodel, "Null Cmodel.");
      return cmodel;
    }
    
    //! Sets the current cmodel call.
    void putCurrentCall(ReplayCModelCall* requestedCall)
    {
      mCurrentCModelCall = requestedCall;
    }
    

    //! Calls the pending cmodel call, if there is one.
    /*!
      During recovery this does not call the cmodel.
    */
    bool playPendingCModelCall()
    {
      bool ret = false;
      // avoid the virtual call, if there is no cmodel call
      if (mCurrentCModelCall)
      {
        ret = mCModelPlayer->playCModelCall(mCurrentCModelCall);
        if (! ret)
          // Didn't diverge, clear out the call
          clearCall();
      }
      return ret;
    }

    //! Initializes structures for recover mode
    void setRecoverMode(RecoverContext& recoverContext);

  private:
    
    typedef UtArray<ReplayCModel*> ReplayCModelVec;
    // Convenience vector to quickly lookup replay cmodels that are
    // stored in the ReplayBOM.
    ReplayCModelVec mCModels;
    // When reading the database, we need a place holder for the
    // current ReplayCModelCall since we have to break up the call
    // into id, input, output in the db
    ReplayCModelCall* mCurrentCModelCall;

    // Object that handles playing a cmodel based on run mode.
    CModelPlayer* mCModelPlayer;

    // CModel Player type for playback mode
    class CModelPlayerPlayback;
    // CModel Player type for recover mode
    class CModelPlayerRecover;
  };

  //! Object that copies recovery info from a CModelCall
  struct CModelRecover
  {
    CARBONMEM_OVERRIDES

    //! Constructor
    /*!
      \param cmodelCall The cmodel call to grab the information from
      to reconstruct its outputs.
      \param notShadow If true use the value buffer. If false use the
      shadow buffer. The value buffer contains the value at the time
      of divergence. The shadow buffer has the value from the
    database.
    */
    CModelRecover(ReplayCModelCall* cmodelCall, bool notShadow) :
      mOutputValue(cmodelCall->numOutputWords() * 32, 
                   notShadow ? cmodelCall->getOutputValueBuffer() : cmodelCall->getOutputShadowBuffer(),
                   cmodelCall->numOutputWords())
    {
      mFormal = cmodelCall->getFormal();
      mContextId = cmodelCall->getContextId();
    }

    const ReplayCModel* mFormal;
    UInt32 mContextId;
    DynBitVector mOutputValue;
  };

  typedef UtArray<DynBitVector*> DynBVVec;
  
  //! Object for reading memory changes from the database
  /*!
    The values line up with the address. So, mValues[0] is the value
    at the address of mAddrs[0].
  */
  struct DBMem
  {
    CARBONMEM_OVERRIDES
      
    DBMem(UInt32 numWords) : mNumRowWords(numWords)
    {}

    ~DBMem() {
      clear();
    }

    //! Add an address to the DBMem, getting back a value buffer.
    /*!
      Meant for address writes. Appending the same address more than
      once allocates extra memory for the value. Since only the last
      value is meaningful, you should not call this on the same
      address more than once per schedule call.
    */
    DynBitVector* appendAddr(SInt32 addr)
    {
      mAddrs.push_back(addr);
      DynBitVector* value = new DynBitVector(mNumRowWords * 32);
      mValues.push_back(value);
      return value;
    }
      

    //! Clear all the values and addresses
    void clear()
    {
      mAddrs.clear();
      for (DynBVVec::iterator p = mValues.begin(), 
             e = mValues.end(); p != e; ++p)
        delete *p;
      mValues.clear();
    }

    //! copy information from this to another dbmem
    /*! 
      The destination dbmem must have the same number of
      mNumRowWords.
    */
    void copyTo(DBMem* mem)
    {
      INFO_ASSERT(mem->mNumRowWords == mNumRowWords, "Incompatible DBMems");
        
      UInt32 numAddrs = mAddrs.size();
      for (UInt32 i = 0; i < numAddrs; ++i)
      {
        SInt32 addr = mAddrs[i];
        DynBitVector* value = mem->appendAddr(addr);
        *value = *(mValues[i]);
      }
    }

    //! List of addresses
    SInt32Vec mAddrs;
    //! List of values (same number as number of addresses)
    DynBVVec mValues;
    //! The size in words of each row.
    UInt32 mNumRowWords;
  };


  //! An array of DBMems
  typedef UtArray<DBMem*> DBMemVec;

  //! A very simple queue for cmodel calls for recovery
  /*!
    The queue continues to grow until clear() is called.
  */
  class CModelRecoverQueue
  {
  public:
    CARBONMEM_OVERRIDES

    CModelRecoverQueue();

    ~CModelRecoverQueue();
      
    //! Clear the queue and set the current index to 0
    void clear();

    bool empty() const { return mIndex == mCModelCalls.size(); }

    //! Takes a cmodel call and puts a recover call on the queue
    /*!
      See CModelRecover for information on notShadow.
    */
    void pushCall(ReplayCModelCall* cmodelCall, bool notShadow);
      
    CModelRecover* popCall();

  private:
    typedef UtArray<CModelRecover*> CModelRecoverVec;
    CModelRecoverVec mCModelCalls;
    UInt32 mIndex;
  };

  //! Object holding state needed for recovery
  /*!
    The caller of recover() and recoverNoSchedule() must allocate this
    (on the stack is fine).
  */
  struct RecoverContext
  {
    CARBONMEM_OVERRIDES

    DBMemVec mDBExternalMems;
    EventClosure mCpClosure;
    //! A cmodel call which caused divergence (may stay NULL)
    CModelRecover* mDivergingCModel;
    CarbonReplayInfo* mReplayInfo;
    CarbonHookup* mHookup;
    //! Queue of calls encountered in the db during recovery.
    CModelRecoverQueue mCModelRecoverQueue;
    
    //! Array of original net values. Used by recovery response check
    DynBVVec mIndexToOrigValue;
    //! Array of original net drives. Used by recovery response check
    DynBVVec mIndexToOrigDrive;

    //! Array of player net values. Used by recovery response check
    UInt32PtrVec mIndexToPlayValue;
    //! Array of player net drives. Used by recovery response check
    UInt32PtrVec mIndexToPlayDrive;

    //! True when we have reached the divergence time of a cmodel
    /*!
      The schedule call that caused the cmodel to diverge is actually
      recorded after all the cmodels have run, so 'time' here really
      means the last cmodel event that occurred which caused the
      divergence
    */
    bool mReachedCModelDivergenceTime;
    

    //! Removes contents from a DBMemVec
    static void clearMemVec(DBMemVec* vec)
    {
      for (DBMemVec::iterator p = vec->begin(),
             e = vec->end(); p != e; ++p)
        delete *p;
      vec->clear();
    }

    //! Clears the contents of DBMemVec
    /*!
      Keeps the contents of DBMemVec, but calls the clear method on
      the DBMems contained by it.
    */
    static void cleanMemVec(DBMemVec* vec)
    {
      for (DBMemVec::iterator p = vec->begin(),
             e = vec->end(); p != e; ++p)
      {
        DBMem* mem = *p;
        mem->clear();
      }
    }
      
    static void copyMemVec(DBMemVec* dest, DBMemVec* src)
    {
      for (DBMemVec::iterator p = src->begin(),
             e = src->end(); p != e; ++p)
      {
        DBMem* srcMem = *p;
        DBMem* dbmem = new DBMem(srcMem->mNumRowWords);
        srcMem->copyTo(dbmem);
        dest->push_back(dbmem);
      }
    }

    RecoverContext(CarbonReplayInfo* replayInfo,
                   CarbonHookup* hookup);

    void init();
    
    ~RecoverContext()
    {
      clearMemVec(&mDBExternalMems);
      delete mDivergingCModel;
      CarbonModel* model = mHookup->getCarbonModel();
      // Possible only if somehow we are recovering and we are being
      // destroyed prior to finishing recover
      if (model->getRunMode() == eCarbonRunRecover)
        mHookup->runModeChangeFn(eCarbonRunRecover, eCarbonRunNormal);        
    }
    
    //! Adds a cmodel call to the recover events
    /*! 
      This only gets called during recovery by the CModelManager's
      cmodel recover player. This adds the call onto the queue of
      current cmodel calls. The queue should be empty once the
      schedule event has occurred. It gets cleared once the divergence
      point has been reached.
      
      \param cmodelCall Cmodel call to queue
      \param notShadow if true use the value buffer. If false, use the
      shadow buffer. The value buffer has the simulation value at the
      divergence point. The shadow buffer is updated by the reader.
    */
    void pushRecoverCModelCall(ReplayCModelCall* cmodelCall, bool notShadow)
    {
      mCModelRecoverQueue.pushCall(cmodelCall, notShadow);
    }

    void putReachedCModelDivergenceTime(bool reached)
    {
      mReachedCModelDivergenceTime = reached;
    }

    CarbonModel::CModelRecoveryStatus
    getCModelCallOutput(ReplayCModel* registeredCModel, UInt32 context, UInt32** outputs)
    {
      if (mReachedCModelDivergenceTime && mCModelRecoverQueue.empty())
        return CarbonModel::eCModelRunFn;
      
      CarbonModel::CModelRecoveryStatus stat = CarbonModel::eCModelUseDB;
      // will assert if the queue is empty
      CModelRecover* call = mCModelRecoverQueue.popCall();
      INFO_ASSERT(registeredCModel == call->mFormal, "Unexpected CModel call.");
      INFO_ASSERT(call->mContextId == context, "Out-of-order cmodel call");

      // If we are at the divergence time and the queue is now empty,
      // then this cmodel call caused the divergence
      if (mReachedCModelDivergenceTime && mCModelRecoverQueue.empty())
      {
        // We are at the divergence point. We need the output of the
        // cmodel that caused the divergence
        INFO_ASSERT(mDivergingCModel, "Recovery out of sync with cmodel divergence.");
        INFO_ASSERT(mDivergingCModel->mFormal == registeredCModel, "CModel call recovery is out-of-sync.");
        *outputs = mDivergingCModel->mOutputValue.getUIntArray();
        // We are in the middle of the last schedule call before we
        // diverged. The recovery mechanism didn't change the mode
        // yet. This has to.
        mReplayInfo->callModeChangeCBs(eCarbonRunRecover, eCarbonRunNormal);
      }
      else
        *outputs = call->mOutputValue.getUIntArray();
      return stat;
    }

    //! Gets called by the ReplayBOM's recover routine.
    void postSchedule()
    {
      // clear the cmodel call queue, so we don't have too much space
      // allocated.
      mCModelRecoverQueue.clear();
    }
    

    //! Clears src external mem, and saves off the state
    /*!
      the src external mem will still have the same number of DBMems
      as it had before, but they will be clear. Meanwhile, this
      context's mDBExternalMems will have the original state.
    */
    void saveExternalMems(DBMemVec* src)
    {
      clearMemVec(&mDBExternalMems);
      copyMemVec(&mDBExternalMems, src);
      cleanMemVec(src);
    }
      
    void restoreExternalMems(DBMemVec* dest)
    {
      cleanMemVec(dest);
      copyMemVec(dest, &mDBExternalMems);
    }

      
    //! If the cmodel call is not null, it's output values are saved
    void saveCModelCall(ReplayCModelCall* cmodelCall)
    {
      if (cmodelCall)
        mDivergingCModel = new CModelRecover(cmodelCall, true);
    }
      
  };

  //! File callback handler for ReplayPlayer
  class ReplayIStreamCB : public UtBinIStreamCB
  {
  public:
    CARBONMEM_OVERRIDES

    ReplayIStreamCB(ReplayPlayer* player)
      : mPlayer(player)
    {}

    virtual ~ReplayIStreamCB() {}

    /*!
      This takes the current file and places into a doomed stated. The
      file actually gets closed when the next file is opened or if the
      stream is destroyed.
    */
    virtual void reportEOF();
    virtual void reportError(const char* errMsg);
      
  private:
    ReplayPlayer* mPlayer;
  };


  //! Object for a ShellNetPlayback paired with a Touched buffer
  /*!
    The pairing is used to keep the ShellNetPlayback's Touched buffers
    in sync with the EventClosure's touched buffers.  One of these
    objects is created for each offset in the the playback value
    buffer.
  */
  struct NetTouchedPair
  {
    CARBONMEM_OVERRIDES

    NetTouchedPair(ShellNetPlayback* net, ShellNetPlayback::Touched* touchBuf, bool isMask)
      : mNet(net), mTouched(touchBuf), mIsMask(isMask)
    {}

    //! The net corresponding to this offset in the buffer
    ShellNetPlayback* mNet;
    //! The Touched object corresponding to this offset in the buffer
    ShellNetPlayback::Touched* mTouched;
    //! True if this offset represents a net's deposit mask (vs. its value)
    bool mIsMask;
  };

  //! Array of NetTouchedPairs
  typedef UtArray<NetTouchedPair*> NetTouchedPairVec;

  //! High-level Event Database Reader
  /*!
    This handles reading the events and putting them in a closure to
    allow easy access from callers.
  */
  class ReplayPlayer
  {
    //! Helper object for reading the event file index
    struct EventIndexClosure
    {
      CARBONMEM_OVERRIDES

      //! Comment in the event index
      UtString mComment;
      //! Event file name
      UtString mEventFile;
    };


  public:
    CARBONMEM_OVERRIDES

    //! Class for reading the event db
    /*!
      This wraps whatever file i/o methodology we wish to use for the
      db. Encapsulated within the ReplayPlayer.
    */
    class EventDBReader
    {
    public:
      CARBONMEM_OVERRIDES

      EventDBReader() : mFd(NULL), mDoomedFd(NULL)
      {}
    
      ~EventDBReader()
      {
        cleanGarbage();
        delete mFd;
      }

      void cleanGarbage()
      {
        delete mDoomedFd;
        mDoomedFd = NULL;
      }

      void maybeStop(bool stop)
      {
        mFd->maybeStop(stop);
      }

      void setStop()
      {
        mFd->setStop();
      }

      void open(const UtString& filename, UtBinIStreamCB* streamCB)
      {
        cleanGarbage();
        mDoomedFd = mFd;
        mFd = new UtBinIStream(filename.c_str(), streamCB);
      }

      void reopen(const UtString& fileToOpen, UtBinIStreamCB* streamCB)
      {
        UtString filename(fileToOpen);
        open(filename, streamCB);
        cleanGarbage();
      }

      const char* getFilename() const { return mFd->getFilename(); }

      bool close() { return mFd->close(); }
      inline bool bad() const { return mFd->fail(); }
      inline bool eof() const { return mFd->eof(); }

      inline unsigned isStopped() const { return mFd->isStopped(); }

      const char* getErrmsg() { return mFd->getErrmsg(); }

      inline void read(char* buf, UInt32 len)
      {
        mFd->read(buf, len);
      }

      inline void read(unsigned char* buf, UInt32 len)
      {
        char* charBuf = reinterpret_cast<char*>(buf);
        read(charBuf, len);
      }

      inline EventDBReader& operator>>(char& c) {
        mFd->readChar(&c);
        return *this;
      }

      inline EventDBReader& operator>>(CarbonStatus& stat)
      {
        char val = '\0';
        mFd->readChar(&val);
        stat = static_cast<CarbonStatus>(val);
        return *this;
      }

      inline EventDBReader& operator>>(UtString& str)
      {
        UInt32 len;
        mFd->readUInt32(&len);
        str.resize(len);
        read(str.getBuffer(), len);
        return *this;
      }

      inline EventDBReader& operator>>(UInt64& num)
      {
        mFd->readUInt64(&num);
        return *this;
      }

      inline EventDBReader& operator>>(UInt32& num)
      {
        mFd->readUInt32(&num);
        return *this;
      }

      inline EventDBReader& operator>>(SInt32& num)
      {
        mFd->readUInt32((UInt32*) &num);
        return *this;
      }

      inline EventDBReader& operator>>(Tag& c)
      {
        char tmp = static_cast<char>(eNoEvent);
        mFd->readChar(&tmp);
        c = static_cast<Tag>(tmp);
        return *this;
      }

      inline char peekChar() { return mFd->peekChar(); }

    private:
      UtBinIStream* mFd;
      UtBinIStream* mDoomedFd;
    };

    //! A vector of play response nets
    typedef UtArray<ShellNetPlaybackResponse*> ResponseNetVec;
    //! Looper for play response net vector
    typedef Loop<ResponseNetVec> ResponseNetVecLoop;

    //! constructor - opens the event database.
    ReplayPlayer(const UtString& dbName, const char* indexName, 
                 CarbonModel* model,
                 CBDataValuePairVec* netChangeCBs,
                 CarbonReplayInfo* replayInfo);

    ~ReplayPlayer();

    //! Reads the size of the stim buffer and allocs it.
    /*!
      Can only be called once.
    */
    void readStimulusTouchedSize(ZistreamDB& in);

    //! Number of clock and state output nets (in the touched buffer)
    UInt32 getNumClkAndStateOutputNets() const {
      return mStimuliTouched.getNumClkAndStateOutputNets();
    }

    //! Map a playback net to a word or words of the stim buffer
    /*!
      This is used to only deposit to nets that have changed during
      recovery. This is for correctness as well as speed. Since the
      stim buffer is not always the actual value of the net, we cannot
      rely on the value to be correct unless we know it is
      touched. 
      The mapping here is used to set the touched buffer on a net if a
      byte within its value has changed due to a deposit.
    */
    void mapNetToInputBuffer(ShellNetPlayback* net, UInt32 bufferIndex, UInt32 numWords, UInt32 isTristate,
                             bool isClock, bool isStateOutput, UInt32 touchIndex);

    //! Map a playback net to a word or words of the response buffer
    /*!
      This mapping is not really utilized yet, but can be to speed up
      net value change callbacks. 
    */
    void mapNetToResponseBuffer(ShellNetPlayback* net, UInt32 bufferIndex, UInt32 numWords, UInt32 isTristate);

    //! Add a cmodel to the player
    void addCModel(ReplayCModel* replayCModel);

    //! Call after a schedule call during recovery
    void recoverPostSchedule(RecoverContext& recoverContext)
    {
      checkActualResponses(recoverContext);
      recoverContext.postSchedule();
      mClosure.clearTouchedStim();
    }

    /*! 
      True if a cmodel caused the divergence and the last read cmodel
      from the database is the cmodel that caused the divergence. This
      is only valid in recover mode.
    */
    bool isLastCModelEventDivergence() const;

    void putCheckpointFile(const char* fileName)
    {
      mClosure.mCheckpoint.assign(fileName);
    }

    const UtString& getCheckpointFile() const { return mClosure.mCheckpoint; }

    //! Call this to set the recover checkpoint file to the current closure checkpoint.
    void saveRecoverEvents()
    {
      syncRecoverInfo();
    }

    // Add an api-writable, but not a Carbon Model writable memory to the
    // playback
    ShellNetPlaybackMem* addExternalPlaybackMem(ShellNet* primNet, UInt32 numWords, UInt32 externalIndex)
    {
      // Currently, all api-accessible memories are depositable. The
      // ones that are not observable need to override the examine routines.
      const ShellDataBOM* vhmNodeData = ShellSymTabBOM::getStorageDataBOM(primNet->getNameAsLeaf());
      bool observable = vhmNodeData->isObservable();
      
      ShellNetPlaybackMem* memPlay = NULL;
      if (numWords == 1)
      {
        if (observable)
          memPlay = new ShellNetPlaybackMem1(primNet, externalIndex, allocAddrChangeSet(numWords), getExternalChangedMemSet(), allocWordRowMem());
        else
          memPlay = new ShellNetPlaybackMem1WriteOnly(primNet, externalIndex, allocAddrChangeSet(numWords), getExternalChangedMemSet(), allocWordRowMem());
      }
      else
      {
        if (observable)
          memPlay = new ShellNetPlaybackMemA(primNet, externalIndex, allocAddrChangeSet(numWords), getExternalChangedMemSet(), allocArrayRowMem());
        else
          memPlay = new ShellNetPlaybackMemAWriteOnly(primNet, externalIndex, allocAddrChangeSet(numWords), getExternalChangedMemSet(), allocArrayRowMem());
      }

      CarbonModelMemory* primMem = primNet->castModelMemory();
      ST_ASSERT(primMem, primNet->getName());
      primMem->syncPlaybackMem(memPlay);
      
      if (mExternalPlaybackMems.size() < externalIndex + 1)
        mExternalPlaybackMems.resize(externalIndex + 1);
      
      mExternalPlaybackMems[externalIndex] = memPlay;
      addExternalDBMem(externalIndex, numWords);
      return memPlay;
    }
    
    // Add a Carbon Model-writable and api observable memory to the playback.
    void addInternalPlaybackMem(ShellNetPlaybackMem* memPlay, UInt32 numWords, UInt32 internalIndex)
    {
      if (mInternalPlaybackMems.size() < (unsigned)internalIndex + 1)
        mInternalPlaybackMems.resize(internalIndex + 1);
      
      ST_ASSERT(mInternalPlaybackMems[internalIndex] == NULL, memPlay->getName());
      mInternalPlaybackMems[internalIndex] = memPlay;

      addInternalDBMem(internalIndex, numWords);
    }

    //! Returns file status. Prints error if it is in a bad state
    /*!
      By calling this, you don't have to print an error message if the
      file is in a bad state. This function does that for you. So, you
      can simplify the calling/reporting code.
    */
    CarbonStatus getStatus()
    {
      // we read the index file but it was empty
      // If there are no event files we cannot check the
      // playbackstream. We'll crash (mFd == NULL)
      if (mEventFileList.empty())
      {
        mCarbonModel->getMsgContext()->SHLReplayNoEventFiles(mDBName->c_str());
        mStatus = eCarbon_ERROR;
      }
      
      return mStatus;
    }

    void reportFileError(const char* errMsg)
    {
      mCarbonModel->getMsgContext()->SHLFileProblem(errMsg);
      mStatus = eCarbon_ERROR;
    }

    //! Sync the stimcheck var to num events.
    /*!
      This is needed for the recovery wheel to spin to the current
      time.
      \note This is a const method because mStimCheck is mutable and
      this is called from the const method hasDiverged()
    */
    inline void syncStimCheck() const
    {
      mStimCheck = mNumEvents;
    }

    //! Sync the stimcheck var to the last cmodel event
    /*!
      This is just like syncStimCheck(), but the number of read events
      has passed the current cmodel being played. Therefore, we need
      to set the stim checker to the last cmodel event if a cmodel
      diverges.
    */
    inline void syncStimCheckToLastCModelEvent()
    {
      mStimCheck = mLastCModelEvent;
    }
    
    //! Returns true if the deposits and expected do not match
    inline bool hasDiverged() 
    {
      syncStimCheck();
      // Get the expected and current value arrays, along with the number of words in them
      const UInt32 numWords = mClosure.mExpectedStimuli.getUIntArraySize();
      const UInt32* expected = mClosure.mExpectedStimuli.getUIntArray();
      const UInt32* current = mPlaybackInputBuffer.getUIntArray();
      
      // Get the touched buffers for clocks and state outputs, and the number of words in them.
      // The touched buffers contain both the touched and masked bits.
      const UInt32* expectedClkAndStateOutputChgs = mClosure.mStimuliTouched.getClkAndStateOutputBufferArray();
      const UInt32* currentClkAndStateOutputChgs = mStimuliTouched.getClkAndStateOutputBufferArray();
      const UInt32 numTouchWords = mStimuliTouched.getNumClkAndStateOutputWords();

      // memcmp the current/expected values, as well as the number of memory changes
      const UInt32 numValBytes = numWords * sizeof(UInt32);
      const UInt32 numTouchBytes = numTouchWords * sizeof(UInt32);
      bool diverged = 
        (memcmp(expected, current, numValBytes) != 0) ||
        (memcmp(expectedClkAndStateOutputChgs, currentClkAndStateOutputChgs, numTouchBytes) != 0) ||
        (mExternalMemChanges.size() != mClosure.mExternalMemChangeIndices.size());
      
      // If we diverged, avoid the address sorting.
      if (!diverged && !mExternalMemChanges.empty())
      {
        // No divergence yet. Must check external memory writes.
        // Sort the external writes by address. The read side was
        // already sorted when it was recorded.
        UInt32 i = 0;
        for (SInt32HashSet::SortedLoop p = mExternalMemChanges.loopSorted(); 
             ! diverged && ! p.atEnd(); ++p)
        {
          SInt32 index = *p;
          diverged = mClosure.mExternalMemChangeIndices[i] != index;
          ++i;
          
          // Grab the playback memory in question
          const ShellNetPlaybackMem* shellMem = mExternalPlaybackMems[index];
          
          // Get its addres changes
          ReplayMemChangeMap* writtenAddrs = shellMem->getWrittenAddrs();
          SInt32DynBVMap& addrs = writtenAddrs->mMem;

          // Grab the db read memory
          DBMem* dbMem = mDBExternalMems[index]; 
          
          // check the sizes before branching, even if the index
          // doesn't match -- one less branch.
          diverged = diverged || (addrs.size() != dbMem->mAddrs.size());
          if (!diverged)
          {
            // Must sort the written loop. The read-side was already
            // sorted during record
            UInt32 j = 0;
            for (SInt32DynBVMap::SortedLoop q = addrs.loopSorted(); ! diverged && ! q.atEnd(); ++q, ++j)
            {
              // First check that the expected address was written.
              SInt32 writtenAddr = q.getKey();
              SInt32 expectedAddr = dbMem->mAddrs[j];

              diverged = writtenAddr != expectedAddr;
              if (!diverged)
              {
                // Must check that the expected value was written to
                // the address
                DynBitVector& writtenVal = q.getValue();
                DynBitVector* expectedVal = dbMem->mValues[j];
                const UInt32* writtenArr = writtenVal.getUIntArray();
                const UInt32* expectedArr = expectedVal->getUIntArray();
                diverged = memcmp(writtenArr, expectedArr, writtenVal.numWords() * sizeof(UInt32)) != 0;
              } // if not diverged
            } // for addrs
          } // if not diverged

          if (diverged) {
            const STAliasedLeafNode *primLeaf = shellMem->getNameAsLeaf();
            UtString name;
            ShellSymTabBOM::composeName(primLeaf, &name, false, false);
            UtString reason;
            reason << "Stimulus difference on memory " << name;
            reportDivergenceReason(reason.c_str());
          }

          // The data from the database has been checked so we no
          // longer need it. Even if we diverge, the
          // expected memory changes at the point of divergence are
          // not interesting, only the actual applied memory changes
          // are.
          dbMem->clear();
        } // for external mem changes
      } // if not diverged
      
      // Only clear the changes if we have not diverged. If we
      // diverged we need those changes to stimulate the recovery
      if (!diverged && !mExternalMemChanges.empty())
      {
        mExternalMemChanges.clear();
        
        // Run through the external mems and clear them all. Assuming
        // that there are not very many memories in the typical design
        // it is faster to run through all the memories and clear them
        // than just the ones that changed (we'd have to store that
        // information somewhere causing more alloc/dealloc).
        for (ReplayMemChangeMapVec::iterator p = mAddrChangeSets.begin(),
               e = mAddrChangeSets.end(); p != e; ++p)
        {
          ReplayMemChangeMap* mem = *p;
          mem->clear();
        }
      }

      if (diverged)
      {
        // If we have diverged make sure we don't close this file and
        // make sure that the recovery information is up to date.
        mNoNextEventFile = true;
        saveRecoverEvents();

        // Print the reason for divergence
        Tag peekEvent = static_cast<Tag>(mPlaybackStream.peekChar());
        // Stimulus comparisons are done before EOF is detected.  If
        // this really is the end of the recorded data, don't bother
        // printing the bogus stimulus divergence.
        if (peekEvent != eNoEvent) {
          UInt32HashSet divergentOffsets;
          // Record which words in the value buffer diverged.
          for (UInt32 i = 0; i < numWords; ++i) {
            if (expected[i] != current[i]) {
              divergentOffsets.insert(i);
            }
          }
          // Same for the touched/masked words.  This requires a
          // little more work because each net has only a pair of bits
          // in the touched/masked buffer.  The divergence reporting
          // deals with offsets into the value buffer, so we need to
          // do that mapping.
          for (UInt32 i = 0; i < numTouchWords; ++i) {
            // Create a single UInt32 representing which bits
            // differed.
            UInt32 diff = expectedClkAndStateOutputChgs[i] ^ currentClkAndStateOutputChgs[i];
            if (diff != 0) {
              // Find each bit that differed
              for (UInt32 j = 0; j < 32; ++j) {
                UInt32 mask = 1 << j;
                if ((diff & mask) != 0) {
                  // There are a pair of bits (touched bit and masked
                  // bit) in the buffer for each net.  Use the current
                  // word and bit index to calculate the overall
                  // offset for this net.  Scale to account for the
                  // pair of bits per entry.
                  UInt32 touchedOffset = ((i * 32) + j) / ShellNetReplay::Touched::eBitsPerEntry;
                  // For each of these pairs, we've recorded the
                  // offset into the value buffer for its
                  // corresponding net.  Look it up and add that
                  // offset to the divergent set.
                  UInt32 wordOffset = mTouchedOffsetToWordOffset[touchedOffset];
                  divergentOffsets.insert(wordOffset);
                }
              }
            }
          }

          identifyDivergentNets(divergentOffsets);
          // Memory divergences were reported above, but only if we
          // had to check individual memories, addresses, etc.  If the
          // number of memory changes differed, we didn't bother.  In
          // that case, report the extra/missing memory accesses here.
          identifyDivergentMemories();
        }
      }
      return diverged;
    }

    inline void setInputToGiven(const DynBitVector& given, const StimuliTouched& givenTouched)
    {
      // This is truly paranoid, but I need to make sure that the
      // underlying word arrays are not freed and realloced. I can't
      // assume that the DynBitVector::operator= will keep it.
      DynBitVector::sEqualSizeDynBVCopy(&mPlaybackInputBuffer, given);

      mStimuliTouched.copy(givenTouched);
    }
    
    inline void setInputToExpected()
    {
      setInputToGiven(mClosure.mExpectedStimuli, 
                      mClosure.mStimuliTouched);
    }

    //! Save the current stimuli into the context
    /*!
      This also sets up the recoverContext for response checks. It can
      be argued that this should be separate, but this really does
      coincide precisely with saving/restoring the current
      stimuli. Once we restore the stimuli, we no longer need to use
      the response checking buffers. So, for now, I'm keeping them
      tied together.
    */
    void saveCurrentStimuli(RecoverContext& recoverContext);

    //! Restore the current stimuli given by the context
    void restoreCurrentStimuli(RecoverContext& recoverContext);

    //! Check if the last event read is what we expected
    /*!
      This prints an error message if the expected does not match in
      addition to returning false.
    */
    bool hasExpected(Tag expected)
    {
      if (mClosure.mEvent != expected)
      {
        mCarbonModel->getMsgContext()->SHLReplayUnexpectedEvent(mDBName->c_str(), 
                                                                sTagStr(expected), sTagStr(mClosure.mEvent));
        return false;
      }
      return true;
    }

    //! Read the next event in the event db.
    void readNextEvent()
    {
      // If the wheel was interrupted. Do not read anymore.
      
      // We have to execute a read in order to find out if we are
      // actually at the end of the file.
      mClosure.mEvent = eNoEvent;
      mPlaybackStream >> mClosure.mEvent; 

      // If we are at the end of the file, open the next event file
      // and then check the eof again. If there is an eof, return,
      // diverge. Should never have an empty events file.
      mNumEvents += ! mPlaybackStream.isStopped();
      switch(mClosure.mEvent)
      {
      case eCheckpoint:
        processCheckpoint();
        break;
      case eStimuli:
        // we need to know when the first stimuli event happens. If we
        // diverge before stimuli then we do not want to apply the
        // closure's stimuli to the actual design.
        ++mClosure.mNumStims;
        readStimuliValueBuffers(mClosure.mExpectedStimuli, mInputWordToNet);
        break;
      case eResponse:
        readValueBuffers(mClosure.mResponse, mResponseWordToNet);
        break;
      case eSchedule:
        readScheduleCall();
        break;
      case eMemWrite:
        readMemWriteEvent();
        break;
      case eEmptyTouchedResponse:
        // Change the tag back to response and don't read
        // any data
        mClosure.mEvent = eResponse;
        break;
      case eEmptyTouchedStimuli:
        // Change the tag back to stimuli and don't read
        // any data
        mClosure.mEvent = eStimuli;
        break;
      case eEndOfFile:
        mClosure.mEndOfFile = true;
        break; 
      case eCModelCall:
        readCModelCallEvent();
        break;
      case eCModelInput:
        readCModelInput();
        break;
      case eCModelOutput:
        readCModelOutput();
        break;
      case eCModelPreOutput:
        readCModelPreRunOutput();
        break;
      case eNoEvent:
        if (processNoEvent())
          return;
        break;
      default:
        INFO_ASSERT(0, "Unrecognized event.");
        break;
      }
    }
    
    //! Get the number of events seen so far
    UInt64 numEvents() const { return mNumEvents; }

    void maybeStopStream(bool maybeStop)
    {
      mPlaybackStream.maybeStop(maybeStop);
    }

    void stopStream()
    {
      mPlaybackStream.setStop();
    }

    //! Keep reading the file until we find the event we want.
    /*! 
      Returns true if we find the event. False, if we come to the eof.
    */
    bool runEventWheel(Tag untilEvent)
    {
      // Have to use an integer; otherwise, valgrind reports an
      // uninitialized read with the bit-wise operations.
      unsigned done = 0;
      while (done == 0)
      {
        readNextEvent();
        /* Windows compile complains about unsafe mix of unsigned int
           and bool unless I explicitly cast the bools.
        */
        done = mClosure.stopWheel(untilEvent) | mPlaybackStream.isStopped();
      }
      return untilEvent == mClosure.mEvent;
    }

    void runToResponse()
    {
      while (1) 
      {
        
        // We have to execute a read in order to find out if we are
        // actually at the end of the file.
        mClosure.mEvent = eNoEvent;
        mPlaybackStream >> mClosure.mEvent; 
        
        // If hit the end of file or the stream has errored, eNoEvent
        // will be the event.  processNoEvent() will determine if we
        // continue in this case.
        /* Note the symmetric case statement. All case labels simply
           call a function, except for eNoEvent which needs to
           return if processNoEvent returns true. The symmetry allows
           for gcc to highly optimize the case statement with gotos
           and precomputed offsets. If you explicitly inline any
           of the functions into the label, this optimization goes
           away.
        */
        mNumEvents += ! mPlaybackStream.isStopped();
        switch(mClosure.mEvent)
        {
        case eEmptyTouchedResponse:
          readEmptyTouchedResponse();
          return;
          break;
        case eResponse:
          readValueBuffers(mClosure.mResponse, mResponseWordToNet);
          return;
          break;
        case eSchedule:
          readScheduleCall();
          break;
        case eCModelCall:
          readCModelCallEvent();
          break;
        case eCModelInput:
          readCModelInput();
          break;
        case eCModelOutput:
          readCModelOutput();
          break;
        case eCModelPreOutput:
          readCModelPreRunOutput();
          break;
        case eNoEvent:
          if (processNoEvent())
            return;
          break;
        case eMemWrite:
          readMemWriteEvent();
          break;
        default:
          INFO_ASSERT(0, "Unrecognized event.");
          break;
        }
      }
    }

    void readScheduleCall()
    {
      // We may have a pending cmodel call to play.
      if (! mCModelManager->playPendingCModelCall())
      {
        ++mClosure.mTotNumSchedCalls;
        mClosure.mHasSchedEvent = true;
        mPlaybackStream >> mClosure.mScheduleType >>
          mClosure.mScheduleStatus;
      }
    }
    
    void runToStimuli()
    {
      // If the wheel was interrupted. Do not read anymore.
      while (1) {
        
        // We have to execute a read in order to find out if we are
        // actually at the end of the file.
        mClosure.mEvent = eNoEvent;
        mPlaybackStream >> mClosure.mEvent; 
        
        // If we are at the end of the file, open the next event file
        // and then check the eof again. If there is an eof, return,
        // diverge. Should never have an empty events file.
        mNumEvents += ! mPlaybackStream.isStopped();
        switch(mClosure.mEvent)
        {
        case eStimuli:
          // we need to know when the first stimuli event happens. If we
          // diverge before stimuli then we do not want to apply the
          // closure's stimuli to the actual design.
          ++mClosure.mNumStims;
          readStimuliValueBuffers(mClosure.mExpectedStimuli, mInputWordToNet);
          return;
          break;
        case eEmptyTouchedStimuli:
          // Change the tag back to stimuli and don't read
          // any data
          mClosure.mEvent = eStimuli;
          return;
          break;
        case eCheckpoint:
          processCheckpoint();
          break;
        case eEndOfFile:
          mClosure.mEndOfFile = true;
          break; 
        case eMemWrite:
          readMemWriteEvent();
          break;
        case eNoEvent:
          if (processNoEvent())
            return;
          break;
        default:
          INFO_ASSERT(0, "Unrecognized event.");
          break;
        }
      }
    }

    void clearTouchedStimuli()
    {
      // clear the current stim changes in the playback nets
      mStimuliTouched.clear();
    }

    void runNetChangeCBs()
    {
      // call net value change cbs if we are not in a clock-only
      // schedule.
      if (mClosure.mScheduleType != scClkSchedule)
      {

        // Net change callbacks are held in a CBDataValuePair, so we
        // can keep the shadow value separate.
        UInt32* val = mNetChangeValueBuffer->mVal.getUIntArray();
        UInt32* drv = mNetChangeValueBuffer->mDrv.getUIntArray();
        for (CBDataValuePairVec::iterator q = mNetChangeCBs->begin(),
               r = mNetChangeCBs->end(); q != r; ++q)
        {
          CBDataValuePair* cbDataValuePair = *q;
          CarbonNetValueCBData* cbData = cbDataValuePair->getCBData();
          if (cbData->isEnabled())
          {
            CarbonTriValShadow* shadow = cbDataValuePair->getValDrvShadow();
            ShellNet* userNet = cbData->getShellNet();
            userNet->runValueChangeCB(cbData, val, drv, shadow, mCarbonModel);
          }
        }
      }
    }

    void syncNetChangeCBShadows()
    {
      // Net change callbacks are held in a CBDataValuePair, so we
      // can keep the shadow value separate.
      for (CBDataValuePairVec::iterator q = mNetChangeCBs->begin(),
             r = mNetChangeCBs->end(); q != r; ++q)
      {
        CBDataValuePair* cbDataValuePair = *q;
        cbDataValuePair->updateShadow();
      }
    }

    void updateStimuli()
    {
      // Clear out the mem changes in the closure
      mClosure.clearExternalMemChanges();
      mClosure.clearTouchedStim();
      runToStimuli();
    }
    
    // Update the response of the playback mems. These get reflected
    // in carbonExamines and carbonExamineMemories.
    bool updateResponse()
    {
      mCModelManager->clearCall();
      
      mClosure.clearInternalMemChanges();
      // eof means either end of file or end of reading (the wheel may
      // be interrupted.)
      runToResponse();
      const bool eof = mPlaybackStream.isStopped();
      if (!eof & !mClosure.mInternalMemChangeIndices.empty())
      {
        // we may have internal memory writes that need to be dealt with.
        for (SInt32Vec::iterator p = mClosure.mInternalMemChangeIndices.begin(),
               e = mClosure.mInternalMemChangeIndices.end(); p != e; ++p)
        {
          SInt32 index = *p;
          // Grab the ShellNetPlaybackMem and the DBMem, both of which
          // are in the same index of their respective arrays.
          ShellNetPlaybackMem* mem = mInternalPlaybackMems[index];
          DBMem* dbMem = mDBInternalMems[index];
          UInt32 numChanges = dbMem->mAddrs.size();
          for (UInt32 i = 0; i < numChanges; ++i)
          {
            SInt32 addr = dbMem->mAddrs[i];
            DynBitVector* value = dbMem->mValues[i];
            // Write the value to the memory directly, avoiding write
            // detection.
            mem->backDoorWrite(addr, value);
          }
          
          dbMem->clear();
        }
      }
      return eof;
    }

    //! Set the input and expected buffer sizes to the given bitwidth
    /*!
      This also resizes the clk and non-clk touched stimuli buffers
    */
    void reserveInputBuffers(UInt32 bitWidth);
    
    //! Set the response buffer size to the given bitwidth
    void resizeResponseBufferWidth(UInt32 bitWidth);

    //! Reserve space in the response net vector
    /*!
      This is an optional function that allows you to set up the
      response net vector if you know how big it is going to be.
    */
    void reserveResponseNets(UInt32 numNets);

    ShellNetReplay::Touched* getStimuliTouchedAt(UInt32 index, bool isClock);
    StimuliTouched* getStimuliTouched() { return &mStimuliTouched; }

    //! Returns the touched buffer at the given index for responses.
    ShellNetReplay::Touched* getTouchedResponse(UInt32 index);

    //! Returns the touched buffer for responses
    /*!
      This gets update during the db read and automatically writes it
      into the response nets. These then can be used to call net value
      change callbacks.
    */
    DynBitVector* getTouchedResponseBuffer()
    {
      return mClosure.mTouchedResponses.getBuffer();
    }


    //! Add a net to the response net vector
    void addResponseNet(ShellNetPlaybackResponse* responseNet)
    {
      mPlayResponseNets.push_back(responseNet);
    }

    //! Print the actual Carbon Model net response values.
    /*!
      Only useful during recovery since the Carbon Model nets are not run
      during playback.
    */
    void printOrigResponseVals() const;
    //! Print the current playback response nets' values
    void printPlayResponseVals() const;
    //! Check the Carbon Model values vs. the expected values
    /*!
      Gets called during postSchedule during recovery.
    */
    void checkActualResponses(RecoverContext& recoverContext);

    //! Loop the response net vector
    ResponseNetVecLoop loopResponseNets()
    {
      return ResponseNetVecLoop(mPlayResponseNets);
    }
    
    //! Get the input (deposit) buffer.
    UInt32* getInputBuffer()
    {
      return mPlaybackInputBuffer.getUIntArray();
    }

    //! Get the input buffer as a DynBitVector*
    DynBitVector* getInputBufferBV()
    {
      return &mPlaybackInputBuffer;
    }

    //! Get the response buffer as a DynBitVector*
    DynBitVector* getResponseBufferBV()
    {
      return &mClosure.mResponse;
    }

    //! Close the database
    bool close(UtString* errMsg)
    {
      bool ret = mPlaybackStream.close();
      if (! ret)
        *errMsg << mPlaybackStream.getErrmsg();
      return ret;
    }

    //! Closes and reopens the current event file
    /*!
      This also sets up the player to be in recover mode
    */
    CarbonStatus setRecoverMode(RecoverContext& recoverContext);

    ShellNetPlaybackMem* getPlaybackMem(UInt32 index)
    {
      return mExternalPlaybackMems[index];
    }

    // This takes stimuli read from the db for a memory and applies it
    // to the original memory (passed in)
    void transferExpectedStimToMem(CarbonMemory* origMem, UInt32 memIndex)
    {
      DBMem* memInfo = mDBExternalMems[memIndex];
      UInt32 numAddrChanges = memInfo->mAddrs.size();
      for (UInt32 i = 0; i < numAddrChanges; ++i)
      {
        SInt32 addr = memInfo->mAddrs[i];
        DynBitVector* value = memInfo->mValues[i];
        origMem->depositMemory(addr, value->getUIntArray());
      }
      // clear the addr changes
      memInfo->clear();
    }

    SInt32HashSet* getExternalChangedMemSet()
    {
      return &mExternalMemChanges;
    }

    //! This is the value of StimCheck when the replay info was
    //synced.
    UInt64 getDivergencePoint() const
    {
      return mRecoverDivergencePoint;
    }

    //! mark a node (from the replay table) as a clock
    void addRecordedClock(STAliasedLeafNode* replayNode)
    {
      mRecordedClocks.insert(replayNode);
    }

    //! mark a node (from the replay table) as a state output
    void addRecordedStateOutput(STAliasedLeafNode* replayNode)
    {
      mRecordedStateOutputs.insert(replayNode);
    }

    void reserveClockShadow(UInt32 totalNumWords)
    {
      mClockShadowBuffer.putNumWords(totalNumWords);
    }

    UInt32* allocClkShadow(UInt32 numWords)
    {
      return mClockShadowBuffer.allocWordPartition(numWords);
    }

    //! Returns true if the replay table node is a clock
    bool isRecordedClock(STAliasedLeafNode* leaf)
    {
      return mRecordedClocks.count(leaf) > 0;
    }

    //! Returns true if the replay table node is a state output
    bool isRecordedStateOutput(STAliasedLeafNode* leaf)
    {
      return mRecordedStateOutputs.count(leaf) > 0;
    }

  public:
    //! Public access to event closure
    EventClosure mClosure;

    // Checks the eof validity of the current file and opens the next
    // event file
    bool transitionNextEventFile()
    {
      // Check if we didn't hit the end of file tag.
      if (!mClosure.mEndOfFile) {
        // Note that we set end of file to true because it takes a
        // bit for the calling code to unwind to the point where it
        // detects this issue. It can call this routine multiple
        // times before that happens.
        //
        // Note that we don't want to print a duplicate warning
        // during recovery so we disable the printing of this warning.
        if (mClosure.mPrintTruncatedFileWarning) {
          mCarbonModel->getMsgContext()->SHLEventFileTruncated();
        }
        mClosure.mEndOfFile = true;
      }

      // If this file had at least one schedule call, the checkpoint
      // at the beginning of the file is the recovery checkpoint.
      if (! mNoNextEventFile)
      {
        // Sync the stim check to the current number of events so we
        // play all schedule calls.
        syncStimCheck();
        // Now save the recover info.
        saveRecoverEvents();
      }
      else {
        // If we have diverged do not open the next event file. We
        // know we diverged within the current event file.  Note that
        // if we successfully open and event file it will clear the
        // end of file flag.
        return false;
      }

      return openNextEventFile();
    }

    //! Pre-recovery steps
    void preRecover(const char scheduleType);
    //! Report a divergence reason
    void reportDivergenceReason(const char *reason);


  private:
    EventDBReader mPlaybackStream;
    DynBitVector mPlaybackInputBuffer;
    CarbonStatus mStatus;
    CarbonModel* mCarbonModel;
    const UtString* mDBName;
    // Use this when restoring the model in recovery.
    UtString mRecoverEventFile;
    UInt64 mRecoverDivergencePoint;

    typedef UtHashSet<STAliasedLeafNode*> LeafSet;
    // NOTE - state outputs that are clocks are in both sets!
    // Set of replay table nodes that are clocks and were recorded.
    LeafSet mRecordedClocks;
    // Set of replay table nodes that are stateoutputs and were recorded.
    LeafSet mRecordedStateOutputs;
    DynBVPartitioner mClockShadowBuffer;

    UtBinIStreamCB* mStreamCB;

    // The number of events seen in the event file
    UInt64 mNumEvents;
    // The point at which we have done the previous stimuli divergence
    // check
    mutable UInt64 mStimCheck;
    mutable UInt64 mLastCModelEvent;
    
    ResponseNetVec mPlayResponseNets;

    // List of storage mem.
    mutable SInt32HashSet mExternalMemChanges;

    typedef UtArray<ReplayMemChangeMap*> ReplayMemChangeMapVec;
    // List of allocated address change sets given to storage memories
    /*
      These sets are used by the memory to notify us which addresses
      have changed.
    */
    mutable ReplayMemChangeMapVec mAddrChangeSets;
    
    // The master list of external db mems. This is here instead of
    // inside the closure so we can easily copy the closure without
    // having to reallocated the DBMems.
    DBMemVec mDBExternalMems;
    // The master list of internal db mems.
    DBMemVec mDBInternalMems;
    
    typedef UtArray<ShellNetPlaybackMem*> PlaybackMemVec;    
    PlaybackMemVec mExternalPlaybackMems;
    PlaybackMemVec mInternalPlaybackMems;
    
    typedef UtArray<SInt32UInt32Map*> SInt32UInt32MapVec;
    SInt32UInt32MapVec mWordRowMems;

    typedef UtArray<SInt32DynBVMap*> SInt32DynBVMapVec;
    SInt32DynBVMapVec mArrayRowMems;

    // List of ordered event files that will be used as the playback
    // data.
    UtStringArray mEventFileList;
    // Current index into EventFileList
    UInt32 mEventFileListIndex;

    CarbonReplayInfo* mReplayInfo;

    CBDataValuePairVec* mNetChangeCBs;
    DynBitVectorPair* mNetChangeValueBuffer;

    // Touch buffer for stimuli
    StimuliTouched mStimuliTouched;
    
    // Map of input buffer word to net touch pairs
    NetTouchedPair** mInputWordToNet;
    // Map of reponse buffer word to net touch pairs
    NetTouchedPair** mResponseWordToNet;
    // Map of touched offset to corresponding word offset
    UInt32* mTouchedOffsetToWordOffset;

    NetTouchedPairVec mAllNetTouchedPairs;

    ReplayCModelManager* mCModelManager;

    /*!
      This tells the event wheel to not open the next event file
      when run out of events in the current event file. This is used
      when there is a divergence detected.
    */
    bool mNoNextEventFile;
    bool mUpdateTouchedStatus;

    //! Have we reported a divergence reason for this schedule call?
    bool mDivergenceReported;

    void readEmptyTouchedResponse()
    {
      // Change the tag back to response and don't read
      // any data
      mClosure.mEvent = eResponse;
    }

    void processCheckpoint()
    {
      ++mClosure.mCheckpointNum;
      mReplayInfo->callCheckpointCBs(mClosure.mTotNumSchedCalls, mClosure.mCheckpointNum);
      mClosure.mCheckpoint.clear();
      mPlaybackStream >> mClosure.mCheckpoint;
      if (! mPlaybackStream.bad())
      {
        // reset the number of stimuli we've seen
        mClosure.mNumStims = 0;
      }
    }
    
    bool processNoEvent()
    {
      mNumEvents -= !mPlaybackStream.isStopped();
      mPlaybackStream.cleanGarbage();
      return mPlaybackStream.isStopped();
    }

    // Save off needed information for recovery.
    void syncRecoverInfo()
    {
      mRecoverEventFile = mPlaybackStream.getFilename();
      mRecoverDivergencePoint = mStimCheck;
    }
    
    void readCModelInput()
    {
      mLastCModelEvent = mNumEvents;
      ReplayCModelCall* currentCall = mCModelManager->getCurrentCall();
      INFO_ASSERT(currentCall, "Current Cmodel is NULL.");
      readRecordBuffer(currentCall->getInputValueBuffer());
    }
    
    void readCModelOutput()
    {
      mLastCModelEvent = mNumEvents;
      ReplayCModelCall* currentCall = mCModelManager->getCurrentCall();
      INFO_ASSERT(currentCall, "Current Cmodel is NULL.");
      readRecordBuffer(currentCall->getOutputShadowBuffer());
    }

    void readCModelPreRunOutput()
    {
      mLastCModelEvent = mNumEvents;
      ReplayCModelCall* currentCall = mCModelManager->getCurrentCall();
      INFO_ASSERT(currentCall, "Current Cmodel is NULL.");
      readRecordBuffer(currentCall->getPreRunOutputValueBuffer());
    }

    void readCModelCallEvent()
    {
      // A cmodel call implies a schedule event.
      mClosure.mHasSchedEvent = true;

      UInt32 numCModels = mCModelManager->numCModels();
      UInt32 id = numCModels;
      mPlaybackStream >> id;
      ReplayCModel* cmodel = mCModelManager->getCModel(id);

      UInt32 numCModelContexts = cmodel->numContexts();
      UInt32 contextId = numCModelContexts;

      mPlaybackStream >> contextId;
      INFO_ASSERT(contextId < numCModelContexts, "Invalid cmodel context id.");
      ReplayCModelCall* requestedCall = cmodel->getContext(contextId);
      INFO_ASSERT(requestedCall, "Null cmodel call context.");

      // if we have a pending call, we must play it.
      // If a cmodel caused a divergence, the closure's
      // mWheelInterrupt will have fired. 
      if (! mCModelManager->playPendingCModelCall())
        // did not diverge
        mCModelManager->putCurrentCall(requestedCall);

      // Set the last cmodel event AFTER the call is read because we
      // may have had a pending cmodel
      mLastCModelEvent = mNumEvents;
    }
    
    void readMemWriteEvent()
    {
      char writeType = '\0';
      mPlaybackStream >> writeType;

      UInt32 index = 0;
      UInt32 numChanges = 0;
      mPlaybackStream >> index;
      mPlaybackStream >> numChanges;

      DBMem* mem = NULL;

      switch(writeType)
      {
      case scExternalMemWrite:
        mClosure.mExternalMemChangeIndices.push_back(index);
        if (index >= mDBExternalMems.size())
        {
          UtString buf;
          buf << "Invalid DBExternalMem index: " << index;
          mCarbonModel->getMsgContext()->SHLReplayCorruptDB(mDBName->c_str(), buf.c_str());
          mStatus = eCarbon_ERROR;
        }
        else
          mem = mDBExternalMems[index];
        break;
      case scInternalMemWrite:
        mClosure.mInternalMemChangeIndices.push_back(index);
        if (index >= mDBInternalMems.size())
        {
          UtString buf;
          buf << "Invalid DBInternalMem index: " << index;
          mCarbonModel->getMsgContext()->SHLReplayCorruptDB(mDBName->c_str(), buf.c_str());
          mStatus = eCarbon_ERROR;
        }
        else
          mem = mDBInternalMems[index];
        break;
      default:
        {
          UtString buf("Unrecognized MemWrite token: ");
          buf << writeType;
          INFO_ASSERT(0, buf.c_str());
        }
        break;
      }
      
      // We have either an internal or external DBMem. In either
      // case, we do the same thing: append the address and value
      // from the db to it.
      if (mem)
      {
        for (UInt32 i = 0; i < numChanges; ++i)
        {
          SInt32 addr = 0;
          mPlaybackStream >> addr;
          DynBitVector* memVal = mem->appendAddr(addr);
          readDynBV(*memVal);
        }
      }
    }
    
    // Places an internally written DBMem into the given index of an
    // array
    void addInternalDBMem(SInt32 internalIndex, UInt32 numWords)
    {
      if (mDBInternalMems.size() < (unsigned)internalIndex + 1)
        mDBInternalMems.resize(internalIndex + 1);


      DBMem* dbMem = mDBInternalMems[internalIndex];
      if (dbMem == NULL)
      {
        dbMem = new DBMem(numWords);
        mDBInternalMems[internalIndex] = dbMem;
      }
      else
        INFO_ASSERT(dbMem->mNumRowWords == numWords, "Consistency check failed.");
    }

    // Places an externally written DBMem into the given index of an
    // array
    void addExternalDBMem(SInt32 externalIndex, UInt32 numWords)
    {
      if (mDBExternalMems.size() < (unsigned)externalIndex + 1)
        mDBExternalMems.resize(externalIndex + 1);


      DBMem* dbMem = mDBExternalMems[externalIndex];
      if (dbMem == NULL)
      {
        dbMem = new DBMem(numWords);
        mDBExternalMems[externalIndex] = dbMem;
      }
      else
        INFO_ASSERT(dbMem->mNumRowWords == numWords, "Consistency check failed.");
    }
    
    // Adds a change set to the addr change set vector and returns it.
    ReplayMemChangeMap* allocAddrChangeSet(UInt32 numRowWords)
    {
      ReplayMemChangeMap* addrChangeSet = new ReplayMemChangeMap(numRowWords);
      
      mAddrChangeSets.push_back(addrChangeSet);
      return addrChangeSet;
    }

    // Allocs a memory buffer with rows <= 32 bits
    SInt32UInt32Map* allocWordRowMem()
    {
      SInt32UInt32Map* wordMem = new SInt32UInt32Map;
      mWordRowMems.push_back(wordMem);
      return wordMem;
    }

    // Allocs a memory buffer with rows > 32 bits
    SInt32DynBVMap* allocArrayRowMem()
    {
      SInt32DynBVMap* arrayMem = new SInt32DynBVMap;
      mArrayRowMems.push_back(arrayMem);
      return arrayMem;
    }

    // This handles re-initializing myself whenever a new index file
    // is opened or if I am reopening an event file.
    void initEventStatus()
    {
      mNumEvents = 0;
      mStimCheck = 0;
      mLastCModelEvent = 0;
      mClosure.init();
    }

    // Go to the next event file event in the index
    bool runIndexToFile(UtIBStream& indexStream, EventIndexClosure& closure)
    {
      bool done = false;
      char curTag = '\0'; // initialize to something
      while (! done)
      {
        indexStream >> curTag;
        if (isspace(curTag) && ! indexStream.eof() && !indexStream.bad())
          continue;
        
        switch (curTag)
        {
        case scIndexComment:
          indexStream.getline(&closure.mComment);
          break;
        case scIndexEventFile:
          indexStream >> closure.mEventFile;
          done = true;
          break;
        case '\0':
          // badness. Something is wrong with the index stream.
          done = true;
          break;
        }
        done = (curTag == scIndexEventFile) | (indexStream.eof()) | (indexStream.bad());
      }
      return curTag == scIndexEventFile;
    }
    
    void readEventIndex(const char* indexName)
    {
      UtIBStream indexStream(indexName);
      EventIndexClosure indexClosure;
      while (runIndexToFile(indexStream, indexClosure))
        mEventFileList.push_back(indexClosure.mEventFile);
      
      if (indexStream.bad())
      {
        mCarbonModel->getMsgContext()->SHLFileProblem(indexStream.getErrmsg());
        mStatus = eCarbon_ERROR;        
      }
    }

    
    // Progresses the index file to the next event file and opens it.
    bool openNextEventFile()
    {
      bool isIndexGood = mEventFileListIndex < mEventFileList.size();
      if (isIndexGood)
      {
        const char* eventFileName = mEventFileList[mEventFileListIndex];
        ++mEventFileListIndex;
        
        UtString fullEventPath;
        OSConstructFilePath(&fullEventPath, mDBName->c_str(), eventFileName);
        mPlaybackStream.open(fullEventPath, mStreamCB);
        initEventStatus();
      }
      return isIndexGood;
    }
    
    // Call this to initialize a buffer recorded as a
    // ReplayRecordBuffer. See readValueBuffers and readRecordBuffer
    char* initializeReadRecordBuffer(UInt32* arr, ReplayChangeCallback::OffsetSizePair* pair)
    {
      char* buf = reinterpret_cast<char*>(arr);
      pair->whole = 0;
      mPlaybackStream >> pair->whole;
      return buf;
    }
    
    // Call this to test if we should read a change from a ReplayRecordBuffer
    inline bool testRecordBufferRead(const ReplayChangeCallback::OffsetSizePair& pair)
    {
      // Read the offset/size/values until we get a size of 0
      return  !mPlaybackStream.isStopped() && (pair.whole != 0);
    }
    
    // Call this to update the value from a ReplayRecordBuffer
    inline void readRecordBufferChange(char* buf, ReplayChangeCallback::OffsetSizePair* pair)
    {
      mPlaybackStream.read(&buf[pair->parts.offset], pair->parts.size);
      pair->whole = 0;
      mPlaybackStream >> pair->whole;
    }

    // Simply read the value without any touched information
    inline void readRecordBuffer(UInt32* arr)
    {
      ReplayChangeCallback::OffsetSizePair pair;
      char* buf = initializeReadRecordBuffer(arr, &pair);
      while (testRecordBufferRead(pair))
        readRecordBufferChange(buf, &pair);
    }

    // Only stimulus and response buffer reads would call this. See readValueBuffers
    void readRecordBufferWithTouched(UInt32* arr, NetTouchedPair** wordMap)
    {
      ReplayChangeCallback::OffsetSizePair pair;
      char* buf = initializeReadRecordBuffer(arr, &pair);
      while (testRecordBufferRead(pair)) {
        // The writer only writes changes, so for every change set
        // the expected touched buffer for the corresponding net 
        const UInt32 beginWord = pair.parts.offset/sizeof(UInt32);
        const UInt32 endWord = (pair.parts.offset + pair.parts.size - 1)/sizeof(UInt32);
        // This needs to be done before the Touched objects are
        // updated, because we may need to check the new value.
        readRecordBufferChange(buf, &pair);
        for (UInt32 i = beginWord; (i <= endWord); ++i)
        {
          // For each word that changed, look up the corresponding
          // net/touched pair and set it as touched.
          NetTouchedPair* netPair = wordMap[i];
          INFO_ASSERT(netPair, "NULL net mapping");
          netPair->mTouched->set();
          // Additionally, if this word represents the net's mask
          // (instead of its value), and the mask is meaningful
          // (i.e. non-zero), set the masked flag as well.
          if (netPair->mIsMask && (arr[i] != 0)) {
            netPair->mTouched->setMasked();
          }
        }
      }
    }
    
    // Call this to read a response buffer 
    inline void readValueBuffers(DynBitVector& dynBV, NetTouchedPair** wordMap)
    {
      UInt32* arr = dynBV.getUIntArray();
      if (! mUpdateTouchedStatus)
        readRecordBuffer(arr);
      else
        readRecordBufferWithTouched(arr, wordMap);
    }

    // Must call this to read the stimuli buffer so we can get the
    // proper touched updates. We need to check the clock changes
    // v. what clocks we expect to change.
    inline void readStimuliValueBuffers(DynBitVector& dynBV, NetTouchedPair** wordMap)
    {
      UInt32* arr = dynBV.getUIntArray();
      readRecordBufferWithTouched(arr, wordMap);
    }
    
    void readDynBV(DynBitVector& dynBV)
    {
      UInt32 numWords = dynBV.getUIntArraySize();
      UInt32* arr = dynBV.getUIntArray();
      readWordArray(arr, numWords);
    }

    void readWordArray(UInt32* arr, UInt32 numWords)
    {
      for (UInt32 i = 0; i < numWords; ++i)
        mPlaybackStream >> arr[i];
    }

    //! Gets the UInt32 recorded model pointers from the response nets.
    void fillArrWithResponseUInts(UInt32PtrVec* indexToPlayValue,
                                  UInt32PtrVec* indexToPlayDrive) const;

    //! Allocs DynBVs for Response net values
    /*!
      Must call cleanArrOfResponseBuffers() to dealloc the BVs.
    */
    void fillArrWithResponseBuffers(DynBVVec* valBufs, 
                                    DynBVVec* drvBufs) const;
    //! Used to dealloc BVs from fillArrWithResponseBuffers
    void cleanArrOfResponseBuffers(DynBVVec* valBufs, 
                                   DynBVVec* drvBufs) const;
    void getNetValDrv(ShellNet* net, 
                      DynBVVec* indexToValue,
                      DynBVVec* indexToDrive,
                      UInt32 i,
                      DynBitVector** value,
                      DynBitVector** drive) const;

    void getPrintNetValDrv(ShellNet* net, 
                        DynBVVec* indexToValue,
                        DynBVVec* indexToDrive,
                        UInt32 i) const;
    
    void printNetValDrv(ShellNet* net, 
                        DynBitVector* value,
                        DynBitVector* drive) const;
    void printNetValDrvArr(ShellNet* net, 
                           const UInt32* value,
                           const UInt32* drive,
                           UInt32 numWords) const;
    
    //! Identify and report nets that diverged, based on buffer offsets
    void identifyDivergentNets(const UInt32HashSet &divergentOffsets);
    //! Identify and report memory changes that caused divergence
    void identifyDivergentMemories();

  };
}
#endif
