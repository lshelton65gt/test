/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _carbon_version_h_
#define _carbon_version_h_

/*!
  \file 
  C function, available from Python, to get the Carbon release ID.
  For example: "C2006.04 SP2(4675)"
*/

#ifdef __cplusplus
extern "C" {
#endif

/*! carbon release id (e.g. "C2006.04 SP2(4675)") */
const char * carbonGetReleaseId();

#ifdef __cplusplus
}
#endif

#endif //  _carbon_version_h_
