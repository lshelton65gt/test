// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __REPLAYSYSTEM_H_
#define __REPLAYSYSTEM_H_

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtIOEnum.h"
#include "util/UtUInt64Factory.h"
#include "util/UtArray.h"
#include "util/UtStringUtil.h"
#include "shell/carbon_system.h"

class OnDemandTraceFileWriter;

#ifndef NO_EXTERN_REPLAY_STRINGS
extern const char* gCarbonReplayStateStrings[];
extern const char* gCarbonOnDemandStateStrings[];
extern const char* gCarbonOnDemandBackoffStrategyStrings[];
extern const char* gCarbonReplaySimProcessStatusStrings[];
extern const char* gCarbonReplayEventTypeStrings[];
#endif

#define CARBON_GUI_FLAG "-carbonGUI"
#define CARBON_MODELSTUDIO_PID "CARBON_MODELSTUDIO_PID"
#define CARBON_MODELSTUDIO_SIM "CARBON_MODELSTUDIO_SIM"

typedef UtIOEnum<CarbonVHMMode,
                 gCarbonReplayStateStrings> CarbonReplayStateIO;

typedef UtIOEnum<CarbonOnDemandMode,
                 gCarbonOnDemandStateStrings> CarbonOnDemandStateIO;

/*
enum CarbonReplaySimProcessStatus {
  eReplaySimRun,                //! simulator is compute-bound
  eReplaySimPause,              //! simulator is paused waiting for user input
  eReplaySimExit                //! simulator has exited
};
typedef UtIOEnum<CarbonReplaySimProcessStatus,
                 gCarbonReplaySimProcessStatusStrings>
CarbonReplaySimProcessStatusIO;
*/

enum CarbonReplayEventType {
  eReplayEventExit,                    //! simulator exits
  eReplayEventNormal,                  //! component goes to normal mode
  eReplayEventCheckpoint,              //! replay system state checkpoint
  eReplayEventUpdate,                  //! simulator-generated status update
  eReplayEventPause,                   //! simulator was paused
  eReplayEventResume,                  //! simulator resumes
  eReplayEventRecord,                  //! component goes to record mode
  eReplayEventPlayback,                //! component goes to playback mode
  eReplayEventRecover,                 //! component goes to recovery
  eOnDemandEventNotIdle,               //! component goes to onDemand looking mode
  eOnDemandEventIdle,                  //! component goes to onDemand idle mode
  eOnDemandEventDisabled               //! component goes to onDemand disabled mode
};
typedef UtIOEnum<CarbonReplayEventType,
                 gCarbonReplayEventTypeStrings>
CarbonReplaySimEventTypeIO;


typedef UtIOEnum<CarbonOnDemandBackoffStrategy,
                 gCarbonOnDemandBackoffStrategyStrings>
CarbonOnDemandBackoffStrategyIO;

class CarbonSystemComponent;

//! replay system context
/*!
 *! This class is used to represent a set of Carbon Models participating
 *! in a system simulation, and control their participation in Replay.
 *!
 *! The class is instantiated once in the system simulation process,
 *! using singleton().  It allows a single API interface to control
 *! and monitor the replay status of every model participating in
 *! the simulation.
 *!
 *! The class is also instantiated in a GUI process.  The GUI is kept
 *! in sync with the simulation process via one of several methods:
 *!   - polling and checking file-modification times (all platforms)
 *!   - signals on unix (e.g. SIGUSR1)
 *!   - something else on Windows (TBD)
 *!
 *! It stores this state in two ascii files, and keeps those files
 *! up-to-date at all times.  One file is written by the simulation
 *! process and read by the GUI (SYSTEM_NAME.css), and the other is
 *! read by the simulation process and written by the GUI (SYSTEM_NAME.csu).
 *!   .css = Carbon Studio Simulation
 *!   .csu = Carbon Studio User
 *!
 *! Both these files contain exactly the same fields, and in the
 *! "steady state" they are identical.  The files diverge when the
 *! user makes a change in the GUI, and reconverge when the simulation
 *! updates the state of the system to reflect the GUI request.  The
 *! files also diverge when the state changes from the simulation, such
 *! when any of the components change replay-mode from Playback to Recovery
 *! to Normal.  When the GUI process polls the .css file and reflects that
 *! change in the GUI, it will update the .csu file and the files will
 *! reconverge.
 *!
 *! The files contain the names of the components in the system,
 *! and the current replay state of that system.  Carbon Explorer is
 *! intended to be a general purpose mechanism to allow the user to
 *! control a running Carbon simulation system from a GUI or batch
 *! program.  At the moment, the only control offered is for Replay.
 *! But in the future, it can be extended to offer:
 *!     - waveform control
 *!     - hierarchy-browser value back annotation (hardware view)
 *!     - abstract register viewing (software view)
 *!     - profiling control (hardware & software views)
 *!
 *! Batch programs can also be created using this class that can, as
 *! an alternative to a GUI, write the .csu file.  E.g.
 *!   carbon replay -record -interval 500 -database boot_ce -system my_soc
 *!   carbon replay -playback -database boot_ce -system my_soc
 *! in these examples, the "replay" program reads my_soc.css, to determine
 *! which components are in it, and then sets them all to the requested
 *! setting, writing my_soc.csu.  Following that script, the next time
 *! the simulation starts, it reads my_soc.csu to determine what the
 *! user wants.
 *!
 *! While the simulation is running, it can annotate the current state
 *! (e.g. playback -> recovery -> normal) and if a GUI is running it
 *! can poll the file and reflect the status in the GUI.
 *!
 *! The file-writing is all done by writing to a .tmp file and then
 *! renaming it, so there is never a half-written file on the disk.
 *! In addition, the file reading must be robust so that a failed
 *! call to read() does not change the current state at all.
 *!
 *!
 *! This class (CarbonReplaySystem) contains the data model and the
 *! file read/write infrastructure.  CarbonSystemSim contains
 *! the methods used from the simulation side, including the direct
 *! control of models via the Carbon C API.  CarbonReplaySystemUser
 *! contains the methods used from the user side (GUI or batch program).
 *!
 *! It is also used in a GUI process to give the end-user explicit control
 *! over the replay state in the system.  When used in this context, it
 *! cannot call the carbonReplayInfo functions directly, but can write
 *! the new state in the system information file and signal the simulation
 *! process to read that file and make the appropriate calls.
 *!
 *! When a GUI is active, it has sole write access to the system database
 *! file.  The simulation process can only read the file.  
 *!
 *! The design is that the simulation process invokes the GUI as a
 *! subprocess can communicates with it by sending it commands via the
 *! GUI's stdin.
 *!
 *! How will the simulation process know to re-read the file from the GUI?
 *! In Maxsim, it can re-read the file if it has changed, whenever the
 *! simulation pauses or restarts.  Maxsim provides a callback hook to help us
 *! implement this solution rather easily.  This approach has the property
 *! that any changes made to the Replay state in the GUI will only be
 *! reflected in the simulation when the simulation stops.
 *!
 *!    void simulationStartStopCallback() {
 *!      gCarbonReplaySystem->readSystem();
 *!    }
 *!
 *! In CoWare, I don't know if this approach is possible.  One approach
 *! that is possible is to add an additional if-statement to our generated
 *! System-C code:
 *!    gCarbonReplaySystem->pollSystem();
 *!    carbonSchedule(...);
 *! pollSystem() 
 */
struct CarbonReplaySystem {
  CARBONMEM_OVERRIDES
  //CarbonReplaySimProcessStatus getStatus() const {return mSimStatus;}

protected:
  typedef UtHashMap<UtString,CarbonSystemComponent*> NameComponentMap;
public:

  //! Event class
  /*!
   *! Events gets logged during simulation so that the GUI can show the user
   *! what is happening.  They include:
   *!
   *!   - Replay state checkpoints
   *!   - Playback state checkpoints (after Mark implements them)
   *!   - Programmatic updates from the simulation process, to help the GUI
   *!     show progress when checkpoints are not occurring or are infrequent
   *!
   */
  class Event {
  public:
    CARBONMEM_OVERRIDES

    Event(CarbonReplayEventType type, CarbonSystemComponent* comp,
          UInt64 cycles, UInt64 totalSchedCalls, UInt64 compSchedCalls,
          double real, double user, double sys, double sim);

    //! Construct a checkpoint from an input file
    Event(CarbonReplaySystem*, UtIStream& in);

    //! write a checkpoint to an output file
    void write(UtOStream& out);

    CarbonReplaySimEventTypeIO mType;
    CarbonSystemComponent* mComponent;      // may be NULL for pause/resume/exit
    UInt64 mCycles;
    UInt64 mTotalSchedCalls;
    UInt64 mComponentSchedCalls;
    double mReal;
    double mUser;
    double mSys;
    double mSimTime;
  };

  //! ctor
  CarbonReplaySystem(const char* systemName = "carbon");

  //! dtor
  virtual ~CarbonReplaySystem();

  //! put the system name
  void putSystemName(const char* name) {mSystemName = name;}
  const char* getSystemName() const {return mSystemName.c_str();}

  //! read the system information from the .css and .csu files
  /*!
   *! Reads first the .css file from the simulator to get the components
   *!
   *! *pchanged is set to true if any of the system state changed as a result
   *! of the read.  The intent is that when the GUI, reads the state from the
   *! .css file, it will redraw the widgets if *pchanged is true.  Conversely,
   *! when the simulation process reads the GUI's state from the .csu file,
   *! and finds that the user has requested a state-change of some sort, 
   *! it can call the Carbon C API functions to put that state-change into
   *! effect.
   */
  bool readSystem(bool onlyIfChanged = false);

  //! poll the system information with minimal overhead
  /*!
   *! This call may use a concurrent thread to monitor the system information
   *! file, or use something like SIGUSR1 on Linux, to detect
   *! if a readSystem() call is needed.  This solution may be needed
   *! for CoWare, but it's not clear how to make it work on Windows.
   *!
   *! Specifically, this call will not make any system calls (in mainline)
   *! unless the file has actually been changed.
   */
  void pollSystem();

  //! write the system information to the .css file
  bool writeSystem();

  //! get any error message associated with last I/O operation
  const char* getErrmsg();

  //! Set Replay verbosity
  void setVerboseReplay(int verbose);

  //! add new component
  /*!
   *! When called from inside the simulation process, the model should
   *! be non-null.  When called inside the GUI, the model can be NULL,
   */
  void addComponent(const char* component_name);

#define REMOVE_COMPONENT_SUPPORT 0
#if REMOVE_COMPONENT_SUPPORT
  //! remove a ReplayInfo* from the existing system
  virtual void removeComponent(const char* component_name);
#endif

  //! start a GUI in a subprocess to allow the user to manage the replay
  //! state
  /*!
   *! When a GUI is active, the GUI becomes the sole owner of the replay
   *! database file.  This method is expected to be run from a process
   *! that owns Carbon models.  When a GUI is started, any already-known
   *! components are transmitted to the GUI via the name of the database
   *! file.  If new componentns are added to the system after the GUI has
   *! been started, then the GUI is notified of the new components by sending
   *! it a message via the GUI's stdin.
   *!
   *! When the user changes replay parameters, the GUI state i
   */
  bool startGUI();

  //! Kill the GUI if it's running
  void killGUI();

  //! is there a GUI alive?
  bool isGUIAlive();

  //! Establish the name of the database
  /*!
   *! This database is a directory name, in which we will
   *! store the databases of each system model, according to
   *! their instance name.
   */
  virtual void putDatabaseDirectory(const char* dir);

  //! Get the current database directory
  const char* getDatabaseDirectory();

  //! Set the replay state of an individual model
  /*!
   *! By default, every component is in Normal mode -- it will participate
   *! automatically when the user calls record() or playback().  But to
   *! Set the state of an individual model, this method can be used.
   */
  void putReplayState(const char* component_name, CarbonVHMMode state);

  //! get the current replay state of a component
  CarbonVHMMode getReplayState(const char* component_name);

  //! is the specified component enabled?
  bool isEnabled(const char* name);

  //! Did the user request recording for the specified component?
  bool isRecordRequested(const char* name);

  //! Did the user request playback for the specified component?
  bool isPlaybackRequested(const char* name);

  //! put whether the specified component is enabled
  void putIsEnabled(const char* name, bool ena);

  //! Re-initialize the replay system
  void clear(bool retain_events_and_components = false);

  //! put the checkpoint interval
  virtual void putCheckpointInterval(UInt32 recoverPercentage, 
                                     UInt32 num_min_calls);

  //! get the checkpoint interval
  UInt32 getCheckpointInterval() const {return mMinInterval;}

  UInt32 getRecoverPercentage() const {return mRecoverPercentage;}

  //! Set the onDemand state of an individual model
  void putOnDemandState(const char* component_name, CarbonOnDemandMode state);

  //! Get the current onDemand state of a component
  CarbonOnDemandMode getOnDemandState(const char* component_name);

  //! Set the maximum OnDemand states for a component
  void putOnDemandMaxStates(const char* component_name, UInt32 onDemandMaxStates);
  //! Get the maximum OnDemand states for a component
  UInt32 getOnDemandMaxStates(const char* component_name) const;

  //! Set the number of OnDemand backoff states for a component
  void putOnDemandBackoffStates(const char* component_name, UInt32 onDemandBackoffStates);
  //! Get the number of OnDemand backoff states for a component
  UInt32 getOnDemandBackoffStates(const char* component_name) const;

  //! Set the OnDemand backoff decay percentage for a component
  void putOnDemandBackoffDecayPercent(const char* component_name, UInt32 onDemandBackoffDecayPercent);
  //! Get the OnDemand backoff decay percentage for a component
  UInt32 getOnDemandBackoffDecayPercent(const char* component_name) const;

  //! Set the OnDemand backoff maximum decay factor for a component
  void putOnDemandBackoffMaxDecay(const char* component_name, UInt32 onDemandBackoffMaxDecay);
  //! Get the OnDemand backoff maximum decay factor for a component
  UInt32 getOnDemandBackoffMaxDecay(const char* component_name) const;

  //! Set the OnDemand backoff strategy for a component
  void putOnDemandBackoffStrategy(const char* component_name, CarbonOnDemandBackoffStrategy onDemandBackoffStrategy);
  //! Get the OnDemand backoff strategy for a component
  CarbonOnDemandBackoffStrategy getOnDemandBackoffStrategy(const char* component_name) const;
  //! Get the backoff strategy name for a component
  const char * getOnDemandBackoffStrategyName(const char* component_name) const;

  //! Set the OnDemand trace status for a component
  void putOnDemandTrace(const char* component_name, bool enable);
  //! Get the OnDemand trace status for a component
  bool getOnDemandTraceRequested(const char* component_name) const;

  //! Set the OnDemand trace file for a component
  void putOnDemandTraceFile(const char* component_name, const char* fname);
  //! Get the OnDemand trace file for a component
  const char *getOnDemandTraceFile(const char* component_name) const;

  //! Adds a signal to the OnDemand idle deposit set for a component
  void addOnDemandIdleDeposit(const char* componentName, const char* signalName);
  //! Clears the OnDemand idle deposit set for a component
  void clearOnDemandIdleDeposit(const char* componentName);

  //! Excludes a signal from the OnDemand state comparisons for a component
  void addOnDemandExcluded(const char* componentName, const char* signalName);
  //! Clears the OnDemand excluded state point set for a component
  void clearOnDemandExcluded(const char* componentName);

  typedef NameComponentMap::SortedLoop ComponentLoop;

  //! loop over all components.
  ComponentLoop loopComponents() {return mNameComponentMap.loopSorted();}

  //! Process command-line args from argc/argv pair
  /*!
   *! returns false if failure (error in getErrmsg())
   */
  bool processCmdline(int* argc, char** argv, bool reInitOptions,
                      bool fromMain);

  //! Process command-line args from string
  /*!
   *! returns false if failure (error in getErrmsg())
   */
  bool processCmdline(const char* cmd, bool fromMain);

  //! Reads the command line from the system UI file and processes it
  CarbonSystemReadCmdlineStatus readCmdline(bool onlyIfChanged);

  //! Put an argument processing context (for -usage help sharing)
  void putArgProc(ArgProc*);

  bool isRecordAll() const {return mRecordAll;}
  bool isPlaybackAll() const {return mPlaybackAll;}
  bool writeCmdline();
  virtual void resetSimulation();
  void clearRequests();
  void recordComponent(const char* name);
  void playbackComponent(const char* name);
  void replayStopComponent(const char* name);
  void recordAll();
  void playbackAll();

  void onDemandStartComponent(const char *name);
  void onDemandStopComponent(const char *name);

  bool isOnDemandStartRequested(const char* name);
  bool isOnDemandStopRequested(const char* name);

  //! Returns a pointer to a component, if it exists
  CarbonSystemComponent* findComponent(const char* name);

  //! Returns a const pointer to a component, if it exists
  const CarbonSystemComponent* findComponent(const char* name) const;

  UInt32 numEvents() const {return mEvents.size();}
  Event* getEvent(UInt32 idx) const {return mEvents[idx];}

  //void putSimStatus(CarbonReplaySimProcessStatus status) {mSimStatus = status;}
  CarbonReplayEventType getSimStatus() const {return mSimStatus;}

  UInt32 getSimPID() const {return mSimPID;}

  //! Get the timestamp of the last time the simulation .css file was read
  UInt64 getSimReadTime() const {return mSimReadTime;}

  const char* getSimHost() const {return mSimHost.c_str();}

  //! compute the total number of times carbonSchedule has been called on any model
  UInt64 totalScheduleCalls() const {return mTotalScheduleCalls;}

  //! Lookup a component given its index, return NULL if out-of-bounds
  CarbonSystemComponent* getComponent(UInt32 index) const;

  //! Number of components
  UInt32 numComponents() const;

  //! record a component checkpoint
  void addEvent(CarbonReplayEventType type, CarbonSystemComponent* comp,
                UInt64 cycles, double real, double user, double sys,
                double simtime);

  //! record a mode-change
  void addModeChange(CarbonSystemComponent* comp, CarbonReplayEventType type,
                     UInt64 cycles, double real, double user, double sys,
                     double simtime);

  //! set verbose logging mode so mode-changes and checkpoints are loggged to stdout
  void putVerbose(bool verbose) {mVerbose = verbose;}

  //! Is the system in verbose mode?
  bool isVerbose() const {return mVerbose;}

  //! Has the simulation provided accurate cycle-count information?
  bool isCycleCountSpecified() const {return mCycleCountSpecified;}

  //! Has the simulation provided accurate simulation-time information?
  bool isSimTimeSpecified() const {return mSimTimeSpecified;}

  //! Specify simulation time units (e.g. "ns", "ps", etc.)
  void putSimTimeUnits(const char* units) {mSimTimeUnits = units;}

  //! get the simulation time units
  const char* getSimTimeUnits() const {return mSimTimeUnits.c_str();}

  //! Determine whether ccontrol is alive for this system.
  bool checkActiveCControl(bool noisy);

  //! The GUI calls this routine to indicate that it's active
  void writeCControlActiveFile();

  //! The GUI calls this routine to indicate it's no longer active
  void clearCControlActiveFile();

  //! Start ccontrol, want wait for it to acknowledge that it has started
  bool startCControl(UInt32 timeout_seconds);

  //! Request ccontrol to authorize simulation start, and wait for grant
  bool waitForCControlSimStart();

  //! check to see if a sim request has been issued (called from GUI)
  bool checkSimRequest();

  //! grant the request (called from GUI)
  void writeSimGrant();

  //! get the simulation timestamp
  UInt64 getSimStartTimestamp() const {return mSimStartTimestamp;}

  //! update the simulation timestamp
  void putSimStartTimestamp(UInt64 timestamp);

protected:
  void declareCmdlineOptions(bool reInitOptions);
  void generateCmdline(UtString* cmd);
  virtual void resetOnDemandFirstEventFlags();
  void detectSimRestart();

  NameComponentMap mNameComponentMap;
  
  UtString mDatabaseDirectory;
  UtString mSystemName;
  UtString mErrmsg;
  UInt32 mVersion;
  UInt32 mMinInterval;
  UInt32 mRecoverPercentage;

  CarbonVHMMode mUserMode;      //! did the user request "record" or "playback"?
  ArgProc* mArgs;
  bool mOwnArgs;                // should we delete mArgs on exit?
  bool mArgsDeclared;

  bool mRecordAll;
  bool mPlaybackAll;
  bool mVerbose;
  bool mSyncGUI;                // does the user want the GUI synced?
  bool mGUIWasSynched;          // prevent the GUI from being synced 2 times
  UtStringSet mPlaybackComponents;
  UtStringSet mRecordComponents;
  UtStringSet mNormalComponents;
  UtUInt64Factory mUInt64Factory;
  UInt64 mSimReadTime;          // cached stat modtime for .css file
  SInt64 mSimReadSize;          // cached stat size for .css file
  UInt64 mEventsReadTime;       // cached stat modtime for .cse file
  SInt64 mEventsReadSize;       // cached stat size for .cse file
  UInt64 mUserReadTime;
  UInt64 mTotalScheduleCalls;

  UtStringSet mOnDemandStartComponents;
  UtStringSet mOnDemandStopComponents;

  typedef UtArray<Event*> EventArray;
  EventArray mEvents;
  //CarbonReplaySimProcessStatusIO mSimStatus;
  CarbonReplaySimEventTypeIO mSimStatus;
  UInt32 mSimPID;
  UtString mSimHost;

  bool mCycleCountSpecified;
  bool mSimTimeSpecified;

  UtString mSimTimeUnits;

  //! For old .csu files/command line - set OnDemand max states for all components
  void putOnDemandMaxStates(UInt32 onDemandMaxStates);
  //! For old .csu files/command line - set OnDemand backoff states for all components
  void putOnDemandBackoffStates(UInt32 onDemandBackoffStates);
  //! For old .csu files/command line - set OnDemand backoff decay percentage states for all components
  void putOnDemandBackoffDecayPercent(UInt32 onDemandBackoffDecayPercent);
  //! For old .csu files/command line - set OnDemand backoff max decay for all components
  void putOnDemandBackoffMaxDecay(UInt32 onDemandBackoffMaxDecay);
  //! For old .csu files/command line - set OnDemand backoff strategy for all components
  void putOnDemandBackoffStrategy(CarbonOnDemandBackoffStrategy onDemandBackoffStrategy);
  //! For old .csu files/command line - set OnDemand trace for all components
  void putOnDemandTrace(bool enable);

private:
  // read CSS file from scratch, overwriting existing settings
  bool readSystemCSS(bool onlyIfChanged);

  // read Events file incrementally, appending to existing events
  bool readSystemEvents(bool onlyIfChanged);

  void readEvent(UtIStringStream& line);
  void clearEvents();

  //! as we declare arguments, keep a set of them so we can determine
  //! whether any of them have been parsed.
  const char* declareCmd(const char*);
  UtStringSet mCmdArgs;
                         
  //! components in order of when they were inserted
  UtArray<CarbonSystemComponent*> mComponents;
#define REPLAY_USE_ENV_VAR 0
#if REPLAY_USE_ENV_VAR
  UtString mReplayCmdEnv;
#endif

  // This has to be persistent because the argc and argv can be passed
  // to processCmdLine from a higher-level system, so we need to
  // remove the command line options we care about and pass back the
  // resultant argc/argv.
  UtStringArgv mArgvBuffer;

  // Position to try seek to in the .cse (events) file when reloading a
  // .css file
  SInt64 mEventFilePosition;
  UInt32 mPrevEventSize;        // for incremental append to event file

  // Timestamp for the beginning of a simulation.  This is used to help,
  // when reading .cse files, whether they pertain to the current simulation.
  UInt64 mSimStartTimestamp;
}; // struct CarbonReplaySystem

//! Component class
class CarbonSystemComponent
{
public:
  CARBONMEM_OVERRIDES

  //! empty ctor for maps -- must call putName after creating
  CarbonSystemComponent(CarbonReplaySystem*);

  //! ctor
  CarbonSystemComponent(const char* name, CarbonReplaySystem*);

  //! dtor
  ~CarbonSystemComponent();

  //! Common initialization code
  void init();

  //! read from file (not including name)
  bool read(UtIStream&, bool hasOnDemandStatus, bool hasOnDemandSettings);

  //! write to file (not including keyword or name)
  void write(UtOStream&);

  //! get the name
  const char* getName() const {return mName.c_str();}

  //! get the model ID
  /*!
    Returns NULL if the model was destroyed.
  */
  CarbonObjectID* getModel() const;

  //! Get the ID string
  const char* getID() const { return mIdString.c_str(); }

  //! Get the user data provided by the integration
  void* getUserData() const { return mUserData; }

  //! get the replay context
  CarbonReplayInfoID* getReplayInfo() const {return mReplayInfo;}

  //! get the current state
  const CarbonReplayStateIO& getReplayState() const {return mReplayState;}

  //! put the model
  void putModel(CarbonObjectID** modelPtr) {mModel = modelPtr;}

  //! Put the ID string
  void putID(const char* id) { mIdString = id; }

  //! Put the user data associated with the component
  void putUserData(void* userData) { mUserData = userData; }

  //! put the replay context
  void putReplayInfo(CarbonReplayInfoID* replayInfo) {mReplayInfo = replayInfo;}

  //! put the state
  void putReplayState(CarbonReplayStateIO state) {mReplayState = state;}

  //! put the onDemand state
  void putOnDemandState(CarbonOnDemandStateIO state) {mOnDemandState = state;}

  //! get the current onDemand state
  const CarbonOnDemandStateIO& getOnDemandState() const {return mOnDemandState;}

  //! put the name
  void putName(const char* name) { mName = name; }
               
  //! is this component enabled for replay?
  bool isEnabled() const {return mEnabled;}

  //! change whether this component is enabled for replay
  void putIsEnabled(bool ena) {mEnabled = ena;}

  //! assignment operator
  CarbonSystemComponent& operator=(const CarbonSystemComponent& src);

  //! put the component index
  void putIndex(UInt32 idx) {mIndex = idx;}

  //! get the component index
  /*!
   *! note that this can change when a new component is added, because
   *! it will be re-sorted
   */
  UInt32 getIndex() const {return mIndex;}

  //! get the number of schedule calls so far on this component
  UInt64 getNumScheduleCalls() const {return mSchedCalls;}

  //! put the number of schedule calls so far on this component
  void putScheduleCalls(UInt64 calls) {mSchedCalls = calls;}

  //! get the number of schedule calls so far on this component
  UInt32 getNumCheckpoints() const {return mNumCheckpoints;}

  //! put the number of schedule calls so far on this component
  void putNumCheckpoints(UInt32 n) {mNumCheckpoints = n;}

  //! Set the maximum OnDemand states
  void putOnDemandMaxStates(UInt32 onDemandMaxStates);
  //! Get the maximum OnDemand states
  UInt32 getOnDemandMaxStates() const;

  //! Set the number of OnDemand backoff states
  void putOnDemandBackoffStates(UInt32 onDemandBackoffStates);
  //! Get the number of OnDemand backoff states
  UInt32 getOnDemandBackoffStates() const;

  //! Set the OnDemand backoff decay percentage
  void putOnDemandBackoffDecayPercent(UInt32 onDemandBackoffDecayPercent);
  //! Get the OnDemand backoff decay percentage
  UInt32 getOnDemandBackoffDecayPercent() const;

  //! Set the OnDemand backoff maximum decay factor
  void putOnDemandBackoffMaxDecay(UInt32 onDemandBackoffMaxDecay);
  //! Get the OnDemand backoff maximum decay factor
  UInt32 getOnDemandBackoffMaxDecay() const;

  //! Set the OnDemand backoff strategy
  void putOnDemandBackoffStrategy(CarbonOnDemandBackoffStrategy onDemandBackoffStrategy);
  //! Get the OnDemand backoff strategy
  const CarbonOnDemandBackoffStrategyIO& getOnDemandBackoffStrategy() const;

  //! put the name of the file for OnDemand trace data
  void putOnDemandTraceFile(const char* fileName) { mTraceFile = fileName; }

  //! get the name of the file for OnDemand trace data
  const char* getOnDemandTraceFile() const { return mTraceFile.c_str(); }

  //! enable/disable collecting OnDemand trace data
  void putOnDemandTrace(bool enable);

  //! Get OnDemand trace status
  bool getOnDemandTrace() const;

  //! Adds a signal to the OnDemand idle deposit set
  void addOnDemandIdleDeposit(const char* signalName);
  //! Clears the OnDemand idle deposit set
  void clearOnDemandIdleDeposit();

  //! Excludes a signal from the OnDemand state comparisons
  void addOnDemandExcluded(const char* signalName);
  //! Clears the OnDemand excluded state point set
  void clearOnDemandExcluded();

  typedef UtStringSet::SortedLoop SignalLoop;

  //! Loop over OnDemand idle deposit signals
  SignalLoop loopOnDemandIdleDeposit() const { return mOnDemandIdleDepositSignals.loopSorted(); }

  //! Loop over OnDemand excluded signals
  SignalLoop loopOnDemandExcluded() const { return mOnDemandExcludedSignals.loopSorted(); }

  //! keep track of the last event recorded on this component
  void putLastEvent(CarbonReplaySystem::Event* event) {mLastEvent = event;}

  //! get the last event
  CarbonReplaySystem::Event* getLastEvent() const {return mLastEvent;}

  void updateOnDemandTrace();

  //! update the simulation timestamp
  void putSimStartTimestamp(UInt64 timestamp);

  bool isFirstEvent() const {return mFirstEvent;}
  void putIsFirstEvent(bool val) {mFirstEvent = val;}

private:
  UtString mName;
  CarbonObjectID** mModel;
  UtString mIdString;
  void* mUserData;
  CarbonReplayInfoID* mReplayInfo;
  CarbonReplayStateIO mReplayState;
  CarbonOnDemandStateIO mOnDemandState;
  bool mEnabled;
  bool mFirstEvent;
  UInt32 mIndex;
  UInt64 mSchedCalls;
  UInt32 mNumCheckpoints;
  UInt32 mOnDemandMaxStates;
  UInt32 mOnDemandBackoffStates;
  UInt32 mOnDemandBackoffDecayPercent;
  UInt32 mOnDemandBackoffMaxDecay;
  CarbonOnDemandBackoffStrategyIO mOnDemandBackoffStrategy;  
  bool mOnDemandTraceEnabled;
  UtString mTraceFile;
  OnDemandTraceFileWriter *mOnDemandTraceFileWriter;
  UtStringSet mOnDemandIdleDepositSignals;
  UtStringSet mOnDemandExcludedSignals;
  CarbonReplaySystem::Event* mLastEvent;
  CarbonReplaySystem* mSystem;
}; // class CarbonSystemComponent

//! Class to handle component-specific .csu/command line arguements
/*!

  This class handles component-specific arguments passed to the system
  via the command line or the .csu file.  Other arguments are handled
  using the ArgProc class, but that doesn't work here.  ArgProc allows
  switches whose values are boolean, integers, reals, or strings.  For
  several component-specific settings, we need two arguments -- a
  string component name, plus a boolean, integer, real, or string
  value.

  Extending the ArgProc class seems like overkill.  This simple class
  doesn't have features like help text, default values, etc., but
  those aren't really necessary.  While passing these
  component-specific options on the command line will work, they are
  mainly for use in the .csu file, which is auto-generated.
 */

class CarbonSystemComponentSettings
{
public:
  CARBONMEM_OVERRIDES

  CarbonSystemComponentSettings();
  ~CarbonSystemComponentSettings();

  //! Add a switch which takes an integer parameter
  /*!
    Each component can have one instance of each integer switch.  If
    additional instances of the switch are encountered when parsing,
    they will overwrite any previous values.
  */
  void addIntSwitch(const char* switchName);

  //! Add a switch which takes a string parameter
  /*!
    Each component can have multiple instances of each string switch.
    Each instance of a string switch is appended to the existing list
    of values for that switch/component pair.
  */
  void addStringSwitch(const char* switchName);

  //! Parse arguments, returning success
  /*!
    The known switches are parsed and removed, updating argc/argv to
    reflect this.  If any known switches are badly formed, the parse
    aborts, but any well-formed switches already seen are extracted.
  */
  bool parseArgs(int* argc, char** argv);

protected:
  typedef UtHashMap<UtString, UInt32> IntSwitchMap;

  typedef UtStringArray* StringArrayPtr;
  typedef UtHashMap<UtString, StringArrayPtr> StringSwitchMap;

  //! Class to contain the sets of integer and string switches for a component
  class Switches
  {
  public:
    CARBONMEM_OVERRIDES

    Switches()
    {
      mIntSwitches = new IntSwitchMap;
      mStringSwitches = new StringSwitchMap;
    }
    ~Switches()
    {
      delete mIntSwitches;
      delete mStringSwitches;
    }

    IntSwitchMap* getIntSwitches() { return mIntSwitches; }
    StringSwitchMap* getStringSwitches() { return mStringSwitches; }

    const IntSwitchMap* getIntSwitches() const { return mIntSwitches; }
    const StringSwitchMap* getStringSwitches() const { return mStringSwitches; }

  protected:
    IntSwitchMap *mIntSwitches;                 //! Map of integer switches to their values
    StringSwitchMap *mStringSwitches;           //! Map of string switches to their arrays of values
  };

  typedef Switches* SwitchesPtr;
  typedef UtHashMap<UtString, SwitchesPtr> CompSwitchMap;
  CompSwitchMap mCompSwitches;                  //! Map of component names to their switches

  UtStringSet mIntSwitches;                     //! Set of known integer switches
  UtStringSet mStringSwitches;                  //! Set of known string switches

public:
  typedef CompSwitchMap::SortedLoop CompSwitchLoop;
  //! Loops over all componenents that have switches set
  CompSwitchLoop loopCompSwitches() { return mCompSwitches.loopSorted(); }

  typedef IntSwitchMap::SortedLoop IntSwitchLoop;
  //! Loops over all the integer switch names/values for a component
  static IntSwitchLoop sLoopIntSwitches(CompSwitchLoop* compLoop);
  //! Look up a particular integer switch value, returning success
  static bool sGetIntSwitchVal(const CompSwitchLoop& compLoop, const char *switchName, UInt32* val);

  typedef StringSwitchMap::SortedLoop StringSwitchLoop;
  //! Loops over all the string switch names/value arrays for a component
  static StringSwitchLoop sLoopStringSwitches(CompSwitchLoop* compLoop);
  //! Look up a particular string switch value, returning success
  static bool sGetStringSwitchValArray(const CompSwitchLoop& compLoop, const char *switchName, const UtStringArray** arr);

};

#define CARBON_STUDIO_USER_EXT ".csu"
#define CARBON_STUDIO_SIMULATION_EXT ".css"

// As of 10/19/07, we write the events to a separate file that
// can be incrementally appended to.  It will have a .cse extension.
// The system parameters are stil written to the .css file, and must
// be re-read every time, but they are of fixed size.
#define CARBON_STUDIO_EVENTS_EXT ".cse"

#define CARBON_STUDIO_ONDEMAND_TRACE_EXT ".cot"

#endif // __REPLAYSYSTEM_H_
