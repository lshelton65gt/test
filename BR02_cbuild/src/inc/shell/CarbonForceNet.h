// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONFORCENET_H_
#define __CARBONFORCENET_H_

#include "shell/ShellNetWrapper.h"

class CarbonForceNet : public ShellNetWrapper1ToN
{
public: CARBONMEM_OVERRIDES
  //! Constructor
  CarbonForceNet(ShellNet* storage, ShellNet* forceMask);
  
  //! virtual destructor
  virtual ~CarbonForceNet();

  /* 1 to N interface */

  virtual void gatherWrappedNets(NetVec* netVec) const;
  virtual void replaceWrappedNets(const NetVec& netVec);

  /* ShellNet interface */

  //! ShellNet::isVector()
  virtual bool isVector () const;

  //! ShellNet::isScalar()
  virtual bool isScalar() const;

  //! ShellNet::isTristate()
  virtual bool isTristate() const;

  //! ShellNet::isReal()
  virtual bool isReal() const;

  //! ShellNet::isDataNonZero()
  virtual bool isDataNonZero() const;

  //! ShellNet::setToDriven()
  virtual bool setToDriven(CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);

  //! ShellNet::setWordToUndriven()
  virtual bool setWordToUndriven(int index, CarbonModel* model);

  //! ShellNet::setRangeToUndriven()
  virtual bool setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model);

  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* drive) const;

  //! ShellNet::examine()
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! ShellNet::examineWord()
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! ShellNet::examineRange()
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const;

  //! ShellNet::deposit()
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! ShellNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! ShellNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::allocShadow()
  virtual Storage allocShadow() const;

  //! ShellNet::freeShadow()
  virtual void freeShadow(Storage* shadow);

  //! ShellNet::update()
  virtual void update(Storage* shadow) const;

  //! ShellNet::compare()
  virtual ValueState compare(const Storage shadow) const;

  //! ShellNet::writeIfNotEq()
  virtual ValueState writeIfNotEq(char* valueStr, size_t len, Storage* shadow,
                                  NetFlags flags);

  //! ShellNet::format()
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel* model) const;
  

  //! ShellNet::isInput()
  virtual bool isInput() const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);

  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::hasDriveConflict() 
  virtual int hasDriveConflict() const;
  
  //! ShellNet::isForcible()
  virtual bool isForcible() const;

  //! ShellNet::castForceNet() 
  virtual const CarbonForceNet* castForceNet() const;

  //! If a vector that has constant bits, put the control mask
  /*!
    The force net must be a vector
  */
  void putControlMask(const UInt32* controlMask);

  //! Force a value
  virtual CarbonStatus force(const UInt32* buf, CarbonModel* model);
  //! Force a word of the value
  virtual CarbonStatus forceWord(UInt32 buf, int index, CarbonModel* model);
  //! Force a range of the value
  virtual CarbonStatus forceRange(const UInt32* buf, 
                                  int range_msb, int range_lsb, 
                                  CarbonModel* model);
  
  //! Release the forced value
  virtual CarbonStatus release(CarbonModel* model);
  //! Release a word of the forced value
  virtual CarbonStatus releaseWord(int index, CarbonModel* model);
  //! Release a range of the forced value
  virtual CarbonStatus releaseRange(int range_msb, int range_lsb, CarbonModel* model);

  //! Checks for conflict on storage in given range
  virtual int hasDriveConflictRange(SInt32, SInt32) const;
  //! Examines xdrive word of storage
  virtual CarbonStatus examineValXDriveWord(UInt32*, UInt32*, int) const;

  //! Calls setRawToUndriven on storage. Does not check forcemask
  /*!
    \warning This does not check the forcemask. This method is only
    intended for initialization and should not be used for any other
    purpose.
  */
  virtual void setRawToUndriven(CarbonModel* model);

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;

  //! Put a constant range. (vectors only)
  /*!
    Will assert if this forcible net does not represent a vector
  */
  void putRange(const ConstantRange* range);

  //! Calls storage's examine real.
  virtual CarbonStatus examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const;
  //! Returns NULL
  virtual const CarbonMemory* castMemory() const;
  //! Returns NULL
  virtual const CarbonModelMemory* castModelMemory() const;
  //! Returns NULL
  virtual const CarbonVectorBase* castVector() const;
  //! Returns NULL 
  virtual const CarbonScalarBase* castScalar() const;
  //! Returns NULL
  virtual const ShellNetConstant* castConstant() const;
  //! Returns NULL
  virtual const CarbonExprNet* castExprNet() const;

  //! Get the control mask, if it exists.
  virtual const UInt32* getControlMask() const;

  /* the following were created specifically for force net
     functionality. They have no meaning on a force net, only on their
     storage or mask
  */
     
  //! Not valid for a force net. asserts
  virtual void putToZero(CarbonModel*);
  //! Not valid for a force net. asserts
  virtual void putToOnes(CarbonModel*);
  //! Not valid for a force net. asserts
  virtual CarbonStatus setRange(int, int, CarbonModel*);
  //! Not valid for a force net. asserts
  virtual CarbonStatus clearRange(int, int, CarbonModel*);
  //! Not valid for a force net, only its storage. asserts
  virtual ValueState writeIfNotEqForce(char*, size_t, Storage*, NetFlags, ShellNet*);

  //! Not valid for a force net, only its storage. asserts
  virtual CarbonStatus formatForce(char*, size_t, CarbonRadix, NetFlags, ShellNet*, CarbonModel*) const;

  //! Passes this down to the mStorage net
  /*!
    Note that the force mask plays no part in the value of the net. So
    if you force a net to the current value, the callback will not be
    called.
  */
  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const;
  
  //! This should pass to the sub net to execute correctly
  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive);

  //! This should pass to the sub net to execute correctly
  virtual void updateUnresolved(Storage* shadow) const;

  //! Get the value net
  ShellNet* getValueNet() { return mStorage; }
  //! Get the mask net
  ShellNet* getMaskNet() { return mForceMask; }

private:
  ShellNet* mStorage;
  ShellNet* mForceMask;

  // forbid
  CarbonForceNet(const CarbonForceNet&);
  CarbonForceNet& operator=(const CarbonForceNet&);
};

#endif
