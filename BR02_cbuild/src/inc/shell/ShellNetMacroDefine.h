// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef SHELLNETMACRODEFINE_H_
#define SHELLNETMACRODEFINE_H_

/*
  Any defines here must be undefined in ShellNetMacroUndef.h if
  included in a header file.
*/

//! Calculate the abs value. Not expr side-effect safe
#define CABS(e) ((e)>=0 ? (e) : -(e))

//! calculate number of words
#define CNUMWORDS(msb, lsb) (((8 + CABS((msb)-(lsb)))/8 + sizeof(UInt32) - 1)/sizeof(UInt32))
//! calculate bit width
#define CBITWIDTH(msb, lsb) (1 + CABS((msb) - (lsb)))

#endif
