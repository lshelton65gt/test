// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONVECTORINPUT_H_
#define __CARBONVECTORINPUT_H_


#ifndef __CARBONVECTOR_H_
#include "shell/CarbonVector.h"
#endif

//! normal vector input
class CarbonVector1Input : public CarbonVector1
{
public: CARBONMEM_OVERRIDES
  //! unsigned constructor
  CarbonVector1Input(UInt8* vec);
  //! signed constructor
  CarbonVector1Input(SInt8* vec);

  //! virtual destructor
  virtual ~CarbonVector1Input();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! ShellNet::isInput()
  virtual bool isInput() const;

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);

  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::examine()
  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  //! ShellNet::deposit()
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
 
  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual void recomputeChangeMask();

protected:
  virtual void examineModelDrive(UInt32* drive, ExamineMode) const;
  virtual void examineModelDriveRange(UInt32* drive, size_t index, size_t length) const;

private:
  CarbonChangeType* mChangeArrayRef;

  inline void calcChangeMask();
  inline void doDepositInput(const UInt32* buf, CarbonModel* model);
  inline CarbonStatus doDepositWordInput(UInt32 buf, int index, CarbonModel* model);
  inline CarbonStatus doDepositRangeInput(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model);
};

class CarbonVector2Input : public CarbonVector2
{
public: CARBONMEM_OVERRIDES
  //! unsigned constructor
  CarbonVector2Input(UInt16* vec);
  //! signed constructor
  CarbonVector2Input(SInt16* vec);

  //! virtual destructor
  virtual ~CarbonVector2Input();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! ShellNet::isInput()
  virtual bool isInput() const;

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);

  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::examine()
  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  //! ShellNet::deposit()
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

protected:
  virtual void examineModelDrive(UInt32* drive, ExamineMode) const;
  virtual void examineModelDriveRange(UInt32* drive, size_t index, size_t length) const;

private:
  CarbonChangeType* mChangeArrayRef;

  inline void doDepositInput(const UInt32* buf, CarbonModel* model);
  inline CarbonStatus doDepositWordInput(UInt32 buf, int index, CarbonModel* model);
  CarbonStatus doDepositRangeInput(const UInt32* buf, int range_msb, int range_lsb, CarbonModel* model);
  
};

class CarbonVector4Input : public CarbonVector4
{
public: CARBONMEM_OVERRIDES
  //! unsigned constructor
  CarbonVector4Input(UInt32* vec);
  //! signed constructor
  CarbonVector4Input(SInt32* vec);

  //! virtual destructor
  virtual ~CarbonVector4Input();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! ShellNet::isInput()
  virtual bool isInput() const;

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);

  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::examine()
  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  //! ShellNet::deposit()
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

protected:
  virtual void examineModelDrive(UInt32* drive, ExamineMode) const;
  virtual void examineModelDriveRange(UInt32* drive, size_t index, size_t length) const;

private:
  CarbonChangeType* mChangeArrayRef;

  inline void doDepositInput(const UInt32* buf, CarbonModel* model);
  inline CarbonStatus doDepositWordInput(UInt32 buf, int index, CarbonModel* model);
  inline CarbonStatus doDepositRangeInput(const UInt32* buf, int range_msb, int range_lsb, CarbonModel* model);
};

class CarbonVector8Input : public CarbonVector8
{
public: CARBONMEM_OVERRIDES
  //! unsigned constructor
  CarbonVector8Input(UInt64* vec);
  //! signed constructor
  CarbonVector8Input(SInt64* vec);
  //! real number constructor
  CarbonVector8Input(CarbonReal* vec);

  //! virtual destructor
  virtual ~CarbonVector8Input();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const; 

  //! ShellNet::isInput()
  virtual bool isInput() const;

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);

  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::examine()
  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  //! ShellNet::examine()
  /*!
    The 1,2, and 4 input versions do not implement this because the
    base class just calls examine. But, because this is > 1 word, it
    has to be implemented here.
  */
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! ShellNet::deposit()
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);
  
  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

protected:
  virtual void examineModelDrive(UInt32* drive, ExamineMode) const;
  virtual void examineModelDriveWord(UInt32* drive, int index) const;
  virtual void examineModelDriveRange(UInt32* drive, size_t index, size_t length) const;

private:
  CarbonChangeType* mChangeArrayRef;

  inline void doDepositInput(const UInt32* buf, CarbonModel* model);
  inline CarbonStatus doDepositWordInput(UInt32 buf, int index, CarbonModel* model);
  inline CarbonStatus doDepositRangeInput(const UInt32* buf, int range_msb, int range_lsb, CarbonModel* model);
};

class CarbonVectorAInput : public CarbonVectorA
{
public: CARBONMEM_OVERRIDES
  //! constructor
  CarbonVectorAInput(UInt32* vec);

  //! virtual destructor
  virtual ~CarbonVectorAInput();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! ShellNet::isInput()
  virtual bool isInput() const;

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  //! ShellNet::putChangeArrayRef()
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);

  //! ShellNet::getChangeArrayRef()
  virtual CarbonChangeType* getChangeArrayRef();

  //! ShellNet::examine()
  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  //! ShellNet::examine()
  /*!
    The 1,2, and 4 input versions do not implement this because the
    base class just calls examine. But, because this is > 1 word, it
    has to be implemented here.
  */
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! ShellNet::deposit()
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::depositWord()
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::depositRange()
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

protected:
  virtual void examineModelDrive(UInt32* drive, ExamineMode) const;
  virtual void examineModelDriveWord(UInt32* drive, int index) const;
  virtual void examineModelDriveRange(UInt32* drive, size_t index, size_t length) const;

private:
  CarbonChangeType* mChangeArrayRef;

  inline void doDepositInput(const UInt32* buf, CarbonModel* model);
  inline CarbonStatus doDepositWordInput(UInt32 buf, int index, CarbonModel* model);
  inline CarbonStatus doDepositRangeInput(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model);
};

//! Signed input vector class
/*!
  We have to do our own sign extension when depositing to the
  internals of SBitVector
*/
class CarbonVectorASInput : public CarbonVectorAInput
{
 public:
  CARBONMEM_OVERRIDES

  CarbonVectorASInput(UInt32* vec);
  virtual ~CarbonVectorASInput();

  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  
  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);
  
  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);
  
  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

 private:
  CarbonVectorASInput();
  CarbonVectorASInput(const CarbonVectorASInput&);
  CarbonVectorASInput& operator=(const CarbonVectorASInput&);
};

#endif
