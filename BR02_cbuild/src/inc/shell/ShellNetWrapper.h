// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __SHELLNETWRAPPER_H_
#define __SHELLNETWRAPPER_H_

#ifndef __SHELLNET_H_
#include "shell/ShellNet.h"
#endif

#include "util/Util.h"

class ShellNetWrapper1To1;
class ShellNetWrapper1ToN;

//! Net wrapper abstraction
/*!
  Derived by classes that wrap one or more primitive nets.
*/
class ShellNetWrapper : public ShellNet
{
public:
  CARBONMEM_OVERRIDES

  //! virtual destructor
  virtual ~ShellNetWrapper();

  ShellNetWrapper1To1* castShellNetWrapper1To1()
  {
    const ShellNetWrapper* me = const_cast<const ShellNetWrapper*>(this);
    return const_cast<ShellNetWrapper1To1*>(me->castShellNetWrapper1To1());
  }

  ShellNetWrapper1ToN* castShellNetWrapper1ToN()
  {
    const ShellNetWrapper* me = const_cast<const ShellNetWrapper*>(this);
    return const_cast<ShellNetWrapper1ToN*>(me->castShellNetWrapper1ToN());
  }

  //! casts this to ShellNetWrapper1To1. NULL, if it isn't
  virtual const ShellNetWrapper1To1* castShellNetWrapper1To1() const;
  //! casts this to ShellNetWrapper1ToN. NULL, if it isn't
  virtual const ShellNetWrapper1ToN* castShellNetWrapper1ToN() const;


  //! type for list of ShellNets
  typedef UtArray<ShellNet*> NetVec;
  
  //! Gathers the nets that are wrapped by the net wrapper
  /*!
    Adds all the nets wrapped by this net to netVec.

    All implementations of this must modify the size of the vector to
    equal the number of nets this wrapper wraps.
    
    The nets are always added in the same order to the vector on
    subsequent calls. Therefore, the ordering can be used to replace
    the underlying nets with replacedWrappedNets()
  */
  virtual void gatherWrappedNets(NetVec* netVec) const = 0;

  //! Replace the wrapped nets with the given nets
  /*!
    The nets are replaced in the same order that gatherWrappedNets()
    gathers the nets.
    This asserts that the size of the vector matches the number of
    wrapped nets.  
  */
  virtual void replaceWrappedNets(const NetVec& netVec) = 0;

  /* ShellNet interface */

  //! Returns this
  virtual const ShellNetWrapper* castShellNetWrapper() const;
};

class ShellNetReplay;
class ShellNetOnDemand;
class ShellVisNet;

//! Wrapper for a single primitive net
/*!
  Since this wraps exactly 1 complete net, this implements all the
  ShellNet interface functions.
*/
class ShellNetWrapper1To1 : public ShellNetWrapper,
                            public CarbonMemory
{
public:
  CARBONMEM_OVERRIDES

  //! Wrap the given net
  ShellNetWrapper1To1(ShellNet* net);
  
  //! virtual destructor
  virtual ~ShellNetWrapper1To1();

  //! Returns this
  virtual const ShellNetWrapper1To1* castShellNetWrapper1To1() const;

  //! Returns NULL if this is not a replay net
  /*!
    This is needed for force formatting and net instrumentation
    Defaults to return NULL.
  */
  virtual const ShellNetReplay* castShellNetReplay() const;

  //! Returns this if this is a record memory. NULL, otherwise
  virtual const ShellNetRecordMem* castShellNetRecordMem() const;
  
  //! Non-const version of cast
  ShellNetReplay* castShellNetReplay()
  {
    const ShellNetWrapper1To1* me = const_cast<const ShellNetWrapper1To1*>(this);
    return const_cast<ShellNetReplay*>(me->castShellNetReplay());
  }

  //! Returns this if this is an ondemand net. NULL, otherwise
  virtual const ShellNetOnDemand* castShellNetOnDemand() const;
  
  //! Non-const version of cast
  ShellNetOnDemand* castShellNetOnDemand()
  {
    const ShellNetWrapper1To1* me = const_cast<const ShellNetWrapper1To1*>(this);
    return const_cast<ShellNetOnDemand*>(me->castShellNetOnDemand());
  }

  //! Returns this if this is a visibility net. NULL, otherwise
  virtual const ShellVisNet* castShellVisNet() const;
  
  //! Non-const version of cast
  ShellVisNet* castShellVisNet()
  {
    const ShellNetWrapper1To1* me = const_cast<const ShellNetWrapper1To1*>(this);
    return const_cast<ShellVisNet*>(me->castShellVisNet());
  }

  //! Get CarbonDatabaseNode, if it exists
  virtual const CarbonDatabaseNode* getDBNode() const;

  //! Replace the net that this wraps.
  virtual void replaceNet(ShellNet* net);

  //! Return the net that this wraps
  ShellNet* getNet() { return mNet; }

  //! Puts mNet in the netVec
  /*!
    Same as getNet
  */
  virtual void gatherWrappedNets(NetVec* netVec) const;

  //! Replaces mNet with the net in netVec 
  /*!
    same as replaceNet()
  */
  virtual void replaceWrappedNets(const NetVec& netVec);
  
  //! Returns true if the net is a vector, false otherwise
  virtual bool isVector () const;
  
  //! Returns true if the net is a scalar, false otherwise  
  virtual bool isScalar() const;

  //! Returns true if this net is a tristate, false otherwise
  virtual bool isTristate() const;

  //! Returns true if this net is a real number, false otherwise
  virtual bool isReal() const;

  //! Is the value non-zero?
  /*!
    Only valid for non-tristates.
  */
  virtual bool isDataNonZero() const;

  //! Sets all the xdrive bits to driven, Tristates only
  /*!
    \returns true if the xdrive bits change
  */
  virtual bool setToDriven(CarbonModel* model);

  //! Sets all the xdrive bits to undriven, Tristates only
  /*!
    \returns true if the xdrive bits change
  */
  virtual bool setToUndriven(CarbonModel* model);

  //! Sets all xdrive bits of word
  //! Sets word of xdrive to undriven. Tristates only
  /*!
    \returns true if the xdrive bits change
  */
  virtual bool setWordToUndriven(int index, CarbonModel* model);

  //! Sets all xdrive bits not internally driven to undriven
  /*!
    \returns true if the xdrive bits change
  */
  virtual bool resolveXdrive(CarbonModel* model);

  //! Get the external contribution to the value of the net
  /*!
    This gets what the xdrive is currently set to. You should run
    carbonResolveBidi() if you want to disable bits that are
    internally driven.
    This function only makes sense for bidirects. 0's are returned for
    inputs and 1's for outputs.
  */
  virtual void getExternalDrive(UInt32* drive) const;
  
  //! Sets range of xdrive to undriven. Tristates only
  /*!
    \returns true if the xdrive bits change
  */
  virtual bool setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model);
  
  //! Reads the nets values and puts into buf
  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! Reads the nets values and puts into a real-number buf
  virtual CarbonStatus examine (CarbonReal* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! Examine a word of the value
  virtual CarbonStatus examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;

  //! Examine an arbitrary range of the value
  virtual CarbonStatus examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const;

  //! Deposit an entire value to the net's value
  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model);

  //! Deposit 1 word of the value at index (of value array)
  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);

  //! Deposit the values in a buffer into the specified range.
  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Deposit without checks.
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! depositWord without checks.
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! depositRange without checks.
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! Force a value
  virtual CarbonStatus force(const UInt32* buf, CarbonModel* model);
  //! Force a word of the value
  virtual CarbonStatus forceWord(UInt32 buf, int index, CarbonModel* model);
  //! Force a range of the value
  virtual CarbonStatus forceRange(const UInt32* buf, 
                                  int range_msb, int range_lsb, 
                                  CarbonModel* model);
  
  //! Release the forced value
  virtual CarbonStatus release(CarbonModel* model);
  //! Release a word of the forced value
  virtual CarbonStatus releaseWord(int index, CarbonModel* model);
  //! Release a range of the forced value
  virtual CarbonStatus releaseRange(int range_msb, int range_lsb, CarbonModel* model);


  //! Allocate space for storing a shadow copy of this net's value
  /*!
    \warning Unimplemented for memories
  */
  virtual Storage allocShadow() const;

  //! Free space allocated with allocShadow()
  /*!
    \param shadow Storage to delete
    \warning Unimplemented for memories
  */
  virtual void freeShadow(Storage* shadow);

  //! Update the shadow value based on the current value
  /*!
    \param shadow Storage to write to. Must be the same size as the
    internal data
    \warning Unimplemented for memories
  */
  virtual void update(Storage* shadow) const;

  //! Compare the shadow and the storage values
  /*!
    \returns whether or not if the storage has changed from the
    shadow copy
    \warning unimplemented for memories
  */
  virtual ValueState compare(const Storage shadow) const;

  //! Write value to string if shadow and storage differ
  /*!
    If the current value differs from the shadow, write as binary to
    valueStr, update current value, and return eChanged

    \param valueStr Buffer to write value to. If the value does not
    change, this buffer is not guaranteed to \e not change. This will
    hold the current value if the value did change. This buffer 
    \b MUST be initialized. It is checked to have an x in the first
    byte, in which case the state will be eChanged and the value will
    be updated.
    \param len Total length of the valueStr
    \param shadow Value to compare against. Must be the same size as
    internal storage
    \param flags Net-specific flags needed for bidi resolution.
    \returns Whether or not the value did change.
    \warning Not defined for memories
  */
  virtual ValueState writeIfNotEq(char* valueStr, size_t len, Storage* shadow,
                                  NetFlags flags);

  //! Write current value into valueStr according to strFormat
  /*!
    \warning Not implemented for memories
    This may change the size of the valueStr parameter if the valueStr
    size is not large or small enough to fit the entire value.
    
    \param model May be NULL. Needed to report error messages.
    \returns eCarbon_OK if the valueStr is big enough.
  */
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags flags,
                              CarbonModel* model) const;

  //! Is this net forcible?
  /*!
    This is not the same as castForceNet(). All CarbonForceNets are
    forcible, but that is not the entire forcible set of all
    ShellNets. CarbonExprNets can also be forcible, but they are not
    force nets. They may contain CarbonForceNets however.
   */
  virtual bool isForcible() const;

  //! Is this expression net an input?
  virtual bool isInput() const;

  //! const version of memory cast
  virtual const CarbonMemory* castMemory() const;

  //! const version of model memory cast
  virtual const CarbonModelMemory* castModelMemory() const;

  //! const version of vector cast
  virtual const CarbonVectorBase* castVector() const;

  //! const version of scalar cast
  virtual const CarbonScalarBase* castScalar() const;

  //! Dynamic cast to a ShellNetConstant
  virtual const ShellNetConstant* castConstant() const;

  //! Cast to a CarbonForceNet
  virtual const CarbonForceNet* castForceNet() const;

  //! Const cast to a cCarbonExprNet
  virtual const CarbonExprNet* castExprNet() const;

  //! Set all bits in the value to 0
  virtual void putToZero(CarbonModel* model);
  //! Set all bits in the value to 1
  virtual void putToOnes(CarbonModel* model);
  //! Set all bits in the range of the value to 1
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! Set all bits in the range of the value to 0
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);

  //! Returns 1 if the net has a drive conflict.
  virtual int hasDriveConflict() const;
  
  //! Does the net range have a drive conflict?
  /*!
    \retval 1 if the net's range has a drive conflict
    \retval 0 if there is no conflict
    \retval -1 if the net range is invalid
  */
  virtual int hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const;
  
  //! Returns a word of external drive and value
  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  //! Sets the actual storage to undriven
  /*!
    This sets the storage of the net to undriven,
    unconditionally. This works just like setToUndriven(), however
    with no flows, setToUndriven() doesn't take effect until the
    shadow updates the storage. This method updates the storage
    immediately.
    
    \warning This method is intended for initialization and should not
    be used for any purpose. 
  */
  virtual void setRawToUndriven(CarbonModel* model);

  //! Get the traits of the net
  virtual void getTraits(Traits* traits) const;

  //! Called by CarbonForceNet for conditional formatting
  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask);
  //! Called by CarbonForceNet for formatting
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel* model) const;

  //! set the pointer into the change array , if applicable
  virtual void putChangeArrayRef(CarbonChangeType* changeArrayIndex);

  //! Get the pointer to the change array. Returns NULL if none
  virtual CarbonChangeType* getChangeArrayRef();

  //! Returns the control mask, if it exists
  virtual const UInt32* getControlMask() const;

  virtual const ShellNet* castMemToShellNet() const;  

  virtual CarbonMemAddrT getRightAddr() const;

  virtual CarbonMemAddrT getLeftAddr() const;

  virtual int getRowBitWidth () const;
  
  virtual int getRowNumUInt32s () const;

  virtual int getRowLSB () const;
  
  virtual int getRowMSB () const ;

  virtual CarbonStatus examineMemory(CarbonMemAddrT address, UInt32* buf) const;

  virtual UInt32 examineMemoryWord(CarbonMemAddrT address, int index) const;

  virtual CarbonStatus examineMemoryRange(CarbonMemAddrT address, UInt32 *buf, int range_msb, int range_lsb) const;

  virtual CarbonStatus depositMemory(CarbonMemAddrT address, const UInt32* buf);


  virtual CarbonStatus depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index);

  virtual CarbonStatus depositMemoryRange(CarbonMemAddrT address, const UInt32* buf, int range_msb, int range_lsb);

  virtual CarbonStatus formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const;

  virtual void dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const;

  virtual CarbonStatus readmemh(const char* filename);

  virtual CarbonStatus readmemb(const char* filename);

  virtual CarbonStatus readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr);

  virtual CarbonStatus readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr);

  virtual CarbonModel* getCarbonModel() const;
  virtual bool isDepositable() const;

  //! By default, passes this through to the subnet
  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const;

  //! This should pass to the sub net to execute correctly
  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive);

  //! This should pass to the sub net to execute correctly
  virtual void updateUnresolved(Storage* shadow) const;

protected:
  //! The wrapped net
  ShellNet* mNet;
  //! MemNet, if applicable
  /*!
    This is NULL for non-memories.
  */
  CarbonMemory* mMemNet;

private:
  void removeSubNet();
};

//! Wrapper for N nets, where N >= 1
/*!
  Mainly for expression and forcible nets. Note that expression nets
  may have only 1 net within them, but because it is an expression it
  is not considered a 1 to 1.
*/
class ShellNetWrapper1ToN : public ShellNetWrapper
{
public:
  CARBONMEM_OVERRIDES
  
  //! virtual destructor
  virtual ~ShellNetWrapper1ToN();

  //! Returns this
  virtual const ShellNetWrapper1ToN* castShellNetWrapper1ToN() const;  

  //! By default, asserts. 
  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const;

  //! By default, asserts. 
  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive);

  //! By default, asserts. 
  virtual void updateUnresolved(Storage* shadow) const;

};

class ShellNetPlayback;
class ShellNetRecord;

//! Wrapper for a replay net
class ShellNetReplay : public ShellNetWrapper1To1
{
public:
  CARBONMEM_OVERRIDES


  /*!
    Helper object for buffer representing whether or not a net or nets
    have been touched (written to), and whether that access was masked
    (i.e. affected only part of the net).

    The mask itself is not recorded here - that's done in a separate
    buffer that the net writes to, similar to value and drive buffers.
    The purpose of the masked flag here is to provide a fast way of
    checking whether a net's access is masked, without an expensive
    compare of the mask buffer.

    A DynBitVector and an index is passed into the constructor. That
    is the full buffer and the index into the full buffer.
    For performance reasons, the test/set methods on DynBitVector
    cannot be used. So, we take the word and the index into that word
    and implement the test and set methods inline.
  */
  struct Touched
  {
    CARBONMEM_OVERRIDES

    Touched() : mTouchedAndMaskedBuffer(NULL), mBitIndex(0), mWordIndex(0)
    {}

    Touched(UInt32* buffer, UInt32 index)
    {
      init(buffer, index);
    }

    Touched(DynBitVector* buffer, UInt32 index) 
    {
      UInt32* arr = buffer->getUIntArray();
      init(arr, index);
    }

    //! Encoding of bits in the buffer
    /*!
      Multiple flags need to be encoded in the buffer.  In order for
      multiple Touched objects to be packed into a UInt32, the number
      of flags needs to be a power of 2.
     */
    enum {
      eTouchedBitOffset,        //!< Whether the net was touched (changed)
      eMaskedBitOffset,         //!< Whether the net was masked (partially changed)
      eBitsPerEntry             //!< Number of bits needed
    };

    //! Mask for encoded bits
    enum {
      eTouchedBitMask = 1 << eTouchedBitOffset,
      eMaskedBitMask = 1 << eMaskedBitOffset
    };

    /*!
      The value can only be 0 or 1. For performance reasons that is
      not checked. 
      Doing a (val & 1) instead of a (val != 0) is 2 less
      instructions.
    */
    inline Touched& operator=(UInt32 val)
    {
      register UInt32 indexMask = eTouchedBitMask << mBitIndex;
      register const UInt32 valMask = (val & eTouchedBitMask) << mBitIndex;
      *mTouchedAndMaskedBuffer = (*mTouchedAndMaskedBuffer & ~indexMask) | valMask;
      // Unconditionally clear the masked bit.  It will need to be set
      // explicitly if this is a masked access.
      indexMask = eMaskedBitMask << mBitIndex;
      *mTouchedAndMaskedBuffer &= ~indexMask;
      return *this;
    }

    inline void set()
    {
      UInt32 indexMask = eTouchedBitMask << mBitIndex;
      *mTouchedAndMaskedBuffer |= indexMask;
      // Unconditionally clear the masked bit.  It will need to be set
      // explicitly if this is a masked access.
      indexMask = eMaskedBitMask << mBitIndex;
      *mTouchedAndMaskedBuffer &= ~indexMask;
    }

    inline void setTouchedOnly()
    {
      UInt32 indexMask = eTouchedBitMask << mBitIndex;
      *mTouchedAndMaskedBuffer |= indexMask;
    }

    inline bool isSet() const { 
      return (*mTouchedAndMaskedBuffer & (eTouchedBitMask << mBitIndex)) != 0;
    }
    
    /*!
      Record that there has been a masked access.  This needs to be
      called in addition to set(), as it doesn't affect the touched
      status.
    */
    inline void setMasked()
    {
      UInt32 indexMask = eMaskedBitMask << mBitIndex;
      *mTouchedAndMaskedBuffer |= indexMask;
    }

    /*!
      Returns whether there has been a masked access.  This does not
      look at the touched status.
    */
    inline bool isSetMasked() const { 
      return (*mTouchedAndMaskedBuffer & (eMaskedBitMask << mBitIndex)) != 0;
    }
    
    //! Only compares touched, not masked!
    bool operator==(UInt32 val) const
    {
      const UInt32 indexMask = eTouchedBitMask << mBitIndex;
      const bool valBool = val != 0;
      const bool touchBool = (*mTouchedAndMaskedBuffer & indexMask) != 0;
      return valBool == touchBool;
    }

    //! Only compares touched, not masked!
    bool operator!=(UInt32 val) const
    {
      return ! this->operator==(val);
    }
    
    
    //! Only sets the touched flag, not masked!
    Touched& operator|=(UInt32 val)
    {
      const UInt32 simpleVal = val != 0;
      const UInt32 indexedValMask = simpleVal << mBitIndex;
      *mTouchedAndMaskedBuffer |= indexedValMask;
      // Unconditionally clear the masked bit.  It will need to be set
      // explicitly if this is a masked access.
      UInt32 indexMask = eMaskedBitMask << mBitIndex;
      *mTouchedAndMaskedBuffer &= ~indexMask;
      return *this;
    }

    //! The bit index into the word of the full touched buffer
    UInt32 getBitIndex() const { return mBitIndex; }

    //! The word index into the full touched buffer
    UInt32 getWordIndex() const { return mWordIndex; }
    
    //! Return the bit index into the full buffer
    /*!
      This calculates the absolute bit index into the full buffer,
      elaborating the word index and adding the bit index.
    */
    UInt32 getBufferIndex() const {
      return (mWordIndex * 32) + mBitIndex;
    }
    
  private:
    //! The word into the full touched/masked buffer
    UInt32* mTouchedAndMaskedBuffer;
    //! The bit index into the word of the full touched/masked buffers
    UInt32 mBitIndex;
    //! The word index into the full touched/masked buffers
    UInt32 mWordIndex;

    void init(UInt32* arr, UInt32 index)
    {
      // The index must be a multiple of the entry size
      INFO_ASSERT(index % eBitsPerEntry == 0, "Unaligned index for Touched object");
      mWordIndex = index/32;
      mTouchedAndMaskedBuffer = &arr[mWordIndex];
      mBitIndex = index % 32;
    }
  };

  //! Touched buffer type definition
  /*!
    Type definition for buffer representing whether or not a net or
    nets have been touched (written to).
  */
  //typedef unsigned char Touched;

  ShellNetReplay(ShellNet* srcNet);

  //! virtual destructor
  virtual ~ShellNetReplay();
  
  //! Returns this
  virtual const ShellNetReplay* castShellNetReplay() const;
  
  //! Returns NULL if this is not a playback net
  /*!
    This is needed for force formatting.
    Defaults to return NULL.
  */
  virtual const ShellNetPlayback* castShellNetPlayback() const;

  //! Returns NULL if this is not a record net
  /*!
    This is needed for force formatting.
    Defaults to return NULL.
  */
  virtual const ShellNetRecord* castShellNetRecord() const;

  
  //! Non-const version of cast
  ShellNetPlayback* castShellNetPlayback()
  {
    const ShellNetReplay* me = const_cast<const ShellNetReplay*>(this);
    return const_cast<ShellNetPlayback*>(me->castShellNetPlayback());
  }

  //! Non-const version of cast
  ShellNetRecord* castShellNetRecord()
  {
    const ShellNetReplay* me = const_cast<const ShellNetReplay*>(this);
    return const_cast<ShellNetRecord*>(me->castShellNetRecord());
  }
  
  // Functions that must be dealt with by derivations

  virtual CarbonStatus deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model) = 0;

  virtual CarbonStatus depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model) = 0;

  virtual CarbonStatus depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model) = 0;

  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model) = 0;
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model) = 0;
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model) = 0;

  virtual void putToZero(CarbonModel* model) = 0;
  virtual void putToOnes(CarbonModel* model) = 0;
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model) = 0;
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model) = 0;

  virtual void setRawToUndriven(CarbonModel* model) = 0;
  virtual bool setToUndriven(CarbonModel* model) = 0;

  //! Encoding of bits in the value buffer for state output clocks
  /*!  
    Clocks that are also state outputs use a custom encoding of the
    record/playback value buffer to deal with fake edges.  See the
    ShellNetRecordTwoStateClkStateOutput and
    ShellNetPlaybackTwoStateClkStateOutput classes for details. Since
    primitive clocks are always scalars, we have plenty of space in
    the 32-bit word.

    This enum is located here so both classes can use it.
   */
  enum {
    eLastValue         = 0x00000001,    //!< Last value deposited to the clock
    ePrevValue         = 0x00000002,    //!< Previous value deposited to the clock
    eLastValueValid    = 0x00000004,    //!< Is the last value valid?
    ePrevValueValid    = 0x00000008     //!< Is the previous valud valid?
  };


};

#endif
