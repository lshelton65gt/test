// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _OnDemandTraceFileReader_h_
#define _OnDemandTraceFileReader_h_

#include "util/UtString.h"

class ModelData;
class UtIBStream;

typedef void (*ReadTraceDebugCallbackFunc)(void *userData,
                                           CarbonUInt64 schedCalls,
                                           CarbonTime simTime,
                                           CarbonOnDemandDebugType type,
                                           CarbonOnDemandDebugAction action,
                                           UInt32 length,
                                           const char *path);

typedef void (*ReadTraceModeChangeCallbackFunc)(void *userData,
                                                CarbonUInt64 schedCalls,
                                                CarbonTime simTime,
                                                bool enterIdle);
typedef void (*ReadTraceStaleDataCallbackFunc)(void *userData);

class OnDemandTraceFileReader
{
public:
  CARBONMEM_OVERRIDES

  OnDemandTraceFileReader(const char *fname,
                          ReadTraceDebugCallbackFunc debug_func,
                          ReadTraceModeChangeCallbackFunc mode_change_func,
                          ReadTraceStaleDataCallbackFunc,
                          void *data);
  ~OnDemandTraceFileReader();

  bool read();

  const char* getErrmsg() const {return mErrmsg.c_str();}

  bool isEndOfData() const {return mEndOfData;}

  void clearFilePos() {mReadPosition = 0;}

  bool checkFileForUpdate();

private:
  void staleDataReset();

  UtString               mFileName;
  ReadTraceDebugCallbackFunc       mDebugFunc;
  ReadTraceModeChangeCallbackFunc  mModeChangeFunc;
  ReadTraceStaleDataCallbackFunc   mStaleDataFunc;
  void                  *mClientData;
  UtString               mBuffer;
  UtString               mErrmsg;
  bool                   mEndOfData;
  SInt64                 mReadPosition;
  UInt64                 mSimStartTimestamp;
  UInt64                 mReadTime;
  SInt64                 mReadSize;

};

#endif // _OnDemandTraceFileReader_h_
