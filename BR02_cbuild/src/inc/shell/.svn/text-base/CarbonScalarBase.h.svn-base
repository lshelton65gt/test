// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONSCALARBASE_H_
#define __CARBONSCALARBASE_H_


#ifndef __SHELLNET_H_
#include "shell/ShellNet.h"
#endif

//! Base class for CarbonScalars
class CarbonScalarBase: public ShellNetPrimitive
{
public: CARBONMEM_OVERRIDES
  //! constructor
  CarbonScalarBase();

  //! virtual destructor
  virtual ~CarbonScalarBase();

  //! Net is always a scalar
  virtual bool isScalar() const;

  //! ShellNet::putToZero()
  virtual void putToZero(CarbonModel* model);
  //! ShellNet::putToOnes()
  virtual void putToOnes(CarbonModel* model);
  //! ShellNet::setRange()
  CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  //! ShellNet::clearRange()
  CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);

  virtual CarbonStatus examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const = 0;
  //! same functionality as examine
  virtual CarbonStatus examineWord (UInt32* buf, int, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  //! same functionality as examine
  virtual CarbonStatus examineRange (UInt32* buf, int , int, UInt32* drive, CarbonModel* model) const;

  //! Same functionality as deposit
  virtual CarbonStatus depositWord(UInt32 buf, int, UInt32 drive, CarbonModel* model);
  //! Same functionality as deposit
  virtual CarbonStatus depositRange(const UInt32 *buf, int, int, const UInt32* drive, CarbonModel* model);

  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  /* functions to finish ShellNet interface */
  
  //! returns false
  virtual bool isVector() const;

  //! returns false
  virtual bool isReal() const;

  //! returns false
  virtual bool isTristate() const;
  
  //! returns false
  virtual bool setToDriven(CarbonModel*);
  //! returns false
  virtual bool setToUndriven(CarbonModel*);
  //! returns false
  virtual bool setWordToUndriven(int, CarbonModel*);
  //! returns false
  virtual bool resolveXdrive(CarbonModel*);
  //! returns false
  virtual bool setRangeToUndriven(int, int, CarbonModel*);
  //! returns error
  virtual CarbonStatus examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const;
  //! returns error
  virtual CarbonStatus force(const UInt32*, CarbonModel*);
  //! returns error
  virtual CarbonStatus forceWord(UInt32, int, CarbonModel*);
  //! returns error
  virtual CarbonStatus forceRange(const UInt32*, int, int, CarbonModel*);
  //! returns error
  virtual CarbonStatus release(CarbonModel*);
  //! returns error
  virtual CarbonStatus releaseWord(int, CarbonModel*);
  //! returns error
  virtual CarbonStatus releaseRange(int, int, CarbonModel*);
  //! returns false
  virtual bool isForcible() const;
  //! returns false
  virtual bool isInput() const;
  //! returns NULL
  virtual const CarbonMemory* castMemory() const;
  //! returns NULL
  virtual const CarbonModelMemory* castModelMemory() const;
  //! returns NULL
  virtual const CarbonVectorBase* castVector() const;
  //! Returns this
  virtual const CarbonScalarBase* castScalar() const;
  //! returns NULL
  virtual const ShellNetConstant* castConstant() const;
  //! returns 0
  virtual int hasDriveConflict() const;
  //! returns 0
  virtual int hasDriveConflictRange(SInt32, SInt32) const;
  //! Does nothing
  virtual void setRawToUndriven(CarbonModel* model);
  //! Does nothing
  virtual void putChangeArrayRef(CarbonChangeType*);
  //! returns NULL
  virtual CarbonChangeType* getChangeArrayRef();
  //! returns NULL
  virtual const UInt32* getControlMask() const;


  //! Bypass extra deposit hooks, and update storage directly
  /*!
    Base version of this function will assert.  You need to implement
    it in any derived classes in which you want to use it.

    This isn't a very glamorous function because of the default assert
    and the lack of similar functions in other ShellNet derivatives.
    However, it's not worth changing the whole infrastructure at this
    time.
   */
  virtual void bypassDeposit(const UInt32 *buf, const UInt32 *drive);
};	


#endif
