/* -*- C++ -*- */
/*****************************************************************************

 Copyright (c) 2005-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __carbon_system_h_
#define __carbon_system_h_

/*!
  \file 
  File containing the API for the Carbon System which includes functions to manage multiple Carbon Models in a system simulation and communicate with the Carbon Model Studio GUI.
*/

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif

/*!
  \defgroup CarbonSystem Carbon System Functions
*/

/*!
  \addtogroup CarbonSystem
  \brief The following are the Carbon System functions.
  @{
*/


#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
/*!
  \brief Macro to allow both C and C++ compilers to use this include file in
  a type-safe manner.
  \hideinitializer
*/
#define STRUCT struct
#endif

  /*!
    \brief Carbon System reference structure

    The CarbonSys provides the context for managing a number of Carbon
    components in a system. The components can be replayable.
  */
  typedef STRUCT CarbonSystemSim CarbonSys;

  /*!
    \brief Carbon Component handle

    The CarbonComp provides the context for managing a Carbon
    component in a Carbon system.
  */
  typedef STRUCT CarbonSystemComponent CarbonSC;

  /*!
    \brief Carbon Component iterator
   */
  typedef STRUCT CarbonSystemComponentIter CarbonSCIter;

  /*!
    \brief Cycle-count callback handle

    In order to annotate cycles-per-second estimates in Carbon
    Model Studio, the integrator of the CarbonSys must supply a
    callback to get the current cycle-count.  If this is not supplied,
    then speed must be estimated on the basis of schedule-calls.
  */
  typedef CarbonUInt64 (*CarbonSystemCycleCountFn)(void *clientData);

  /*!
    \brief Sim-time callback handle

    In order to annotate simulation-time progress in Carbon Model
    Studio, the integrator of the CarbonSys must supply a callback
    to get the current simulation time.  If this is not supplied,
    then speed must be estimated on the basis of schedule-calls.
  */
  typedef double (*CarbonSystemSimTimeFn)(void *clientData);

  /*!
    \brief Gets the current system

    For components that are somewhat indepdendent of each other, this
    function gets a shared Carbon system that can be used to manage
    the system simulation.

    \returns The Carbon system handle.

  */
  CarbonSys* carbonGetSystem(int* firstCall);

  /*!
    \brief Sets the system name

    The system name should be the same for all participating
    components.

    \param sys The Carbon system handle
    \param name The name to give to the system
   */
  void carbonSystemPutName(CarbonSys* sys, const char* name);

  /*!
    \brief Adds a new component to the Carbon system

    \param sys The Carbon system handle
    \param componentName A unique name for the new system component
    \param modelPtr A pointer to the Carbon Model for the new
    component. The variable representing *modelPtr must be the same as
    the one that will be passed to carbonDestroy(). This extra
    pointer reference is needed to detect if carbonDestroy() was called
    on the CarbonObjectID.
    \returns A handle to the Carbon System component

  */
  CarbonSC* carbonSystemAddComponent(CarbonSys* sys, const char* componentName,
                                     CarbonObjectID** modelPtr);

  /*!
    \brief Looks up a component by its component name

    \param sys The Carbon system handle

    \param componentName The unique component name specified in
    carbonSystemAddComponent().

    \returns A handle to the Carbon System component or NULL
   */
  CarbonSC* carbonSystemFindComponent(CarbonSys* sys, const char* componentName);

  /*!
    \brief Returns an iterator to visit all the components in the system.

    Use the function carbonSystemComponentNext() to retrieve the
    components in a loop. Once complete, free the iterator memory with
    carbonSystemFreeComponentIter().

    \param sys The Carbon system handle

    \returns A handle to a component iterator.
   */
  CarbonSCIter* carbonSystemLoopComponents(CarbonSys* sys);

  /*!
    \brief Returns the current component in a loop and moves to the next one

    \param iter The iterator returned by carbonSystemLoopComponents().

    \returns The CarbonSC* or NULL if we are at the end of the components.
   */
  CarbonSC* carbonSystemComponentNext(CarbonSCIter* iter);

  /*!
    \brief Frees the component iterator created by carbonSystemLoopComponents().

    \param iter The iterator returned by carbonSystemLoopComponents().
   */
  void carbonSystemFreeComponentIter(CarbonSCIter* iter);

  /*!
    \brief Updates the runtime GUI data file associated with this system.

    The Carbon system file is created by the simulation running
    system.  When running multiple Carbon Models with Carbon Model
    Studio, the GUI polls the Carbon system file to display the correct
    information.  This function should be called periodically by the
    simulation to update the file.

    Calling this function too often can slow the simulation. It is
    best to call it infrequently either by number of carbonSchedule()
    calls or when the simulation status changes (running vs debugging,
    for example).

    \param sys The Carbon system handle
   */
  void carbonSystemUpdateGUI(CarbonSys* sys);

  /*!
    \brief Gets the number of components (both replayable on non-replayable)

    This is useful in determining if a new component is the first
    component.

    \param sys The Carbon system handle
    \returns The current number of components in the system.
   */
  int carbonSystemNumComponents(CarbonSys* sys);

  /*!
    \brief Gets the number of replayable components.

    This is useful in determining if a new component is the first
    replayable component.

    \param sys The Carbon system handle
    \returns The current number of replayable components in the system.
   */
  int carbonSystemNumReplayableComponents(CarbonSys* sys);

  /*!
    \brief Notifies the Carbon system that it should re-read data from the 
    Carbon Model Studio GUI
    
    A system simulation should call this routine periodically to cause
    the system to update data from Carbon Model Studio. The best time
    to do this is when the simulation status changes.

    This function is faster if the onlyIfChanged parameter is set to
    1. In that case the system only reads the command line if the file
    has changed since the last read.

    If the eCarbonCmdError is returned, use carbonSystemGetErrmsg() to
    get an error message string.
    
    \param sys The Carbon system handle
    \param onlyIfChanged Set if the file status should be checked
    \returns one of eCarbonChanged, eCarbonUnchanged, or
    eCarbonCmdError.
   */
  CarbonSystemReadCmdlineStatus carbonSystemReadFromGUI(CarbonSys* sys,
                                                        int onlyIfChanged);

  /*!
    \brief Gets an error message string associated with the last failed API call.

    \param sys The Carbon system handle
    \returns An error message string.
   */
  const char* carbonSystemGetErrmsg(CarbonSys* sys);

  /*!
    \brief Set verbosity level for Replay events

    If verbosity is set to 1, informational messages will be displayed
    when Replay mode changes and checkpoint occur.  If verbosity is
    set to 0, no messages will occur.

    \param sys The Carbon system handle
    \param verbose Verbosity value
  */
  void carbonSystemSetVerboseReplay(CarbonSys* sys, int verbose);
  
  /*!
    \brief Tests whether this component is replayable or not
    
    Not all Carbon components can participate in Replay. This API
    determines whether this Carbon component can participate.

    \param comp The Carbon design component.
    \retval 1 if the component is replayable
    \retval 0 if the component is not replayable
   */
  int carbonSystemComponentReplayable(CarbonSC* comp);

  /*!
    \brief Returns the Carbon Model associated with a given component

    \param comp The Carbon design component returned by either
    carbonSystemAddComponent(), carbonSystemFindComponent(), or
    carbonSystemComponentNext().

    \returns A pointer to the Carbon Model, or NULL if it was already destroyed.

   */
  CarbonObjectID* carbonSystemComponentGetModel(CarbonSC* comp);

  /*!
    \brief Returns the registered name for this component

    \param comp The Carbon design component returned by either
    carbonSystemAddComponent(), carbonSystemFindComponent(), or
    carbonSystemComponentNext().

    \returns A string name for this component

   */
  const char* carbonSystemComponentGetName(CarbonSC* comp);

  /*!
    \brief Returns the user data provided when adding the component

    \param comp The Carbon design component returned by either
    carbonSystemAddComponent(), carbonSystemFindComponent(), or
    carbonSystemComponentNext().

    \returns The user data
   */
  void* carbonSystemComponentGetUserData(CarbonSC* comp);

  /*!
    \brief Stores the user data provided in the component structure

    The field is initialized with NULL at construction. This function
    overwrites the existing value whether it is NULL (uninitialized)
    or contains a value from a previous call to this function.

    Use the carbonSystemComponentGetUserData() to retreive the data.

    \param comp The Carbon design component returned by either
    carbonSystemAddComponent(), carbonSystemFindComponent(), or
    carbonSystemComponentNext().

    \param userData The user data to store
   */
  void carbonSystemComponentPutUserData(CarbonSC* comp, void* userData);

  /*!
    \brief Tells Carbon Model Studio that the system simulation has shut down

    Calling this function when the simulation has shut down updates the
    status line of Carbon Model Studio.

    \param sys The Carbon system handle
  */
  void carbonSystemShutdown(CarbonSys* sys);

  /*!
    \brief Establish a callback to tell Carbon Model Studio how many cycles
    have been run so far.

    \param sys The Carbon system handle
    \param fn Function to call to get the current number of cycles
    \param clientData Context to pass to fn
  */
  void carbonSystemPutCycleCountCB(CarbonSys* sys,
                                   CarbonSystemCycleCountFn fn,
                                   void *clientData);

  /*!
    \brief Remove the cycle-count callback

    When a simulation is being terminated, no more cycles will
    occur, so there is no need to call the callback anymore.  Note
    that we will still consider this a "cycle-counted" simulation,
    and not a "schedule-call-counted" simulation.
   
    It is necessary to do this to avoid calling the callback using data
    that is no longer valid memory.

    \param sys The Carbon system handle
  */
  void carbonSystemClearCycleCountCB(CarbonSys* sys);

  /*!
    \brief Establish a callback to tell Carbon Model Studio how much
    simulation time has been run so far.

    \param sys The Carbon system handle
    \param fn Function to call to get the current simulation time
    \param clientData Context to pass to fn
  */
  void carbonSystemPutSimTimeCB(CarbonSys* sys,
                                CarbonSystemSimTimeFn fn,
                                void *clientData);

  /*!
    \brief Remove the sim-time callback
     
      When a simulation is being terminated, no more simulation time will
      occur, so there is no need to call the callback anymore.  

      It is necessary to do this to avoid calling the callback using data
      that is no longer valid memory.

    \param sys The Carbon system handle
  */
  void carbonSystemClearSimTimeCB(CarbonSys* sys);

  /*!
    \brief Establish a callback to tell Carbon Model Studio the simulation
    time units.
    
    \param rss The Carbon system handle
    \param units The simulation time units to display (e.g. "ps")

    \sa carbonSystemPutSimTimeCB 
  */
  void carbonSystemPutSimTimeUnits(CarbonSys* rss, const char* units);

  /*!
    \brief Checks whether the system has been shut down
    
    The system can be shut down expliticly with the
    carbonSystemShutdown function.  This function returns the current
    status of the system.

    If the system has been created, but no updates to it have occurred,
    it is considered to be shut down.

    \param comp The Carbon system handle
    \retval 1 if the system is shut down
    \retval 0 if the system is not shut down
   */
  int carbonSystemIsShutdown(CarbonSys* sys);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif

