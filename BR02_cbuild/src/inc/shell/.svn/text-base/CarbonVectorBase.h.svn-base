// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONVECTORBASE_H_
#define __CARBONVECTORBASE_H_


#ifndef __SHELLNET_H_
#include "shell/ShellNet.h"
#endif

#ifndef __UtConv_h_
#include "util/UtConv.h"
#endif

#ifndef __SHELLGLOBAL_H_
#include "shell/ShellGlobal.h"
#endif

//! Format a non-tristate vector value into a string
/*!
  \param valueStr The output buffer
  \param len The length in bytes of the output buffer
  \param strFormat The radix to which to convert the value.
  \param vec The storage value - what is actually in the model
  \param forceMask If the vector is forcible, the forcemask is passed
  in (NULL otherwise). This trumps any override value that may appear
  in the overrideMask
  \param overrideMask An array that is 3x times the number of words
  needed to represent the value, if the value has constant x/z/0/1
  bits. The first set of words is the control mask, the second set of
  words is the value mask, and the third set of words is the xz mask.
  See IODB.h, getConstNetBitMask, for more info.
  \param numBits The number of bits in the vector
*/
template <typename T> 
CarbonStatus valueFormatString(char* valueStr, size_t len, 
                               CarbonRadix strFormat, const T* vec,
                               const T* forceMask,
                               const UInt32* overrideMask,
                               size_t numBits, CarbonModel* model)
{
  int numCharsConverted = -1;
  CarbonStatus stat = eCarbon_OK;
  if (overrideMask == NULL)
  {
    switch(strFormat)
    {
    case eCarbonBin:
      numCharsConverted = CarbonValRW::writeBinValToStr(valueStr, len, vec, numBits);
      break;
    case eCarbonHex:
      numCharsConverted = CarbonValRW::writeHexValToStr(valueStr, len, vec, numBits);
      break;
    case eCarbonOct:
      numCharsConverted = CarbonValRW::writeOctValToStr(valueStr, len, vec, numBits);
      break;
    case eCarbonDec:
      numCharsConverted = CarbonValRW::writeDecValToStr(valueStr, len, vec, true, numBits);
      break;
    case eCarbonUDec:
      numCharsConverted = CarbonValRW::writeDecValToStr(valueStr, len, vec, false, numBits);
      break;
    }
  }
  else
  {
    switch(strFormat)
    {
    case eCarbonBin:
      numCharsConverted = CarbonValRW::writeBinXZValToStr(valueStr, len, vec, (T*) NULL, (T*) NULL, forceMask, overrideMask, false, numBits);
      break;
    case eCarbonHex:
      numCharsConverted = CarbonValRW::writeHexXZValToStr(valueStr, len, vec, (T*) NULL, (T*) NULL, forceMask, overrideMask, false, numBits);
      break;
    case eCarbonOct:
      numCharsConverted = CarbonValRW::writeOctXZValToStr(valueStr, len, vec, (T*) NULL, (T*) NULL, forceMask, overrideMask, false, numBits);
      break;
    case eCarbonDec:
      numCharsConverted = CarbonValRW::writeDecXZValToStr(valueStr, len, vec, (T*) NULL, (T*) NULL, forceMask, overrideMask, false, true, numBits);
      break;
    case eCarbonUDec:
      numCharsConverted = CarbonValRW::writeDecXZValToStr(valueStr, len, vec, (T*) NULL, (T*) NULL, forceMask, overrideMask, false, false, numBits);
      break;
    }
  }
  
  if (numCharsConverted == -1)
  {
    ShellGlobal::reportInsufficientBufferLength(len, model);
    stat = eCarbon_ERROR;
  }
  return stat;
}

class ConstantRange;

class CarbonVectorBase : public ShellNetPrimitive
{
public:  CARBONMEM_OVERRIDES
  CarbonVectorBase();
  virtual ~CarbonVectorBase();

  virtual int getBitWidth() const;
  virtual int getNumUInt32s() const;
  virtual bool isVector() const;
  virtual int getLSB() const;
  virtual int getMSB() const;

  virtual const CarbonVectorBase* castVector() const;

  //! Set up shadow for NoInputFlow types
  /*!
    This defaults to doing nothing. CarbonHookup should call this on
    all input vectors when noInputFlow was specified on the cbuild
    command line.
  */
  virtual void maybeSetupShadow();

  void putRange(const ConstantRange* range);

  //! Adds the override/control mask to the vector
  /*!
    If the vector has constant x/z/0/1 bits in it, mControlMask will be
    non-null. This should only be called if controlMask exists.
    
    \param controlMask Must not be NULL. 3x the number of words needed
    to represent the vector. First set of words is the control
    mask. The second set of words is the value mask, and the third set
    of words is the xz mask. See IODB.h, getConstNetBitMask, for more
    info.
  */
  void putControlMask(const UInt32* controlMask);


  //! ShellNet::putToZero()
  virtual void putToZero(CarbonModel* model);

  //! ShellNet::putToOnes()
  virtual void putToOnes(CarbonModel* model);
  
  //! ShellNet::setRange()
  virtual CarbonStatus setRange(int range_msb, int range_lsb, CarbonModel* model);
  
  //! ShellNet::clearRange()
  virtual CarbonStatus clearRange(int range_msb, int range_lsb, CarbonModel* model);

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrive) const;

  /* functions to finish ShellNet interface */
  
  //! returns false
  virtual bool isScalar() const;

  //! returns false
  virtual bool isReal() const;

  //! returns false
  virtual bool isTristate() const;

  //! Returns control mask, if it exists
  virtual const UInt32* getControlMask() const;
  
  //! returns false
  virtual bool setToDriven(CarbonModel*);
  //! returns false
  virtual bool setToUndriven(CarbonModel*);
  //! returns false
  virtual bool setWordToUndriven(int, CarbonModel*);
  //! returns false
  virtual bool resolveXdrive(CarbonModel* model);
  //! returns false
  virtual bool setRangeToUndriven(int, int, CarbonModel*);
  //! returns error
  virtual CarbonStatus examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const;
  //! returns error
  virtual CarbonStatus force(const UInt32*, CarbonModel*);
  //! returns error
  virtual CarbonStatus forceWord(UInt32, int, CarbonModel*);
  //! returns error
  virtual CarbonStatus forceRange(const UInt32*, int, int, CarbonModel*);
  //! returns error
  virtual CarbonStatus release(CarbonModel*);
  //! returns error
  virtual CarbonStatus releaseWord(int, CarbonModel*);
  //! returns error
  virtual CarbonStatus releaseRange(int, int, CarbonModel*);
  //! returns false
  virtual bool isForcible() const;
  //! returns false
  virtual bool isInput() const;
  //! returns NULL
  virtual const CarbonMemory* castMemory() const;
  //! returns NULL
  virtual const CarbonModelMemory* castModelMemory() const;
  //! Returns NULL
  virtual const CarbonScalarBase* castScalar() const;
  //! returns NULL
  virtual const ShellNetConstant* castConstant() const;
  //! returns 0
  virtual int hasDriveConflict() const;
  //! returns 0
  virtual int hasDriveConflictRange(SInt32, SInt32) const;
  //! Does nothing
  virtual void setRawToUndriven(CarbonModel* model);
  //! Does nothing
  virtual void putChangeArrayRef(CarbonChangeType*);
  //! returns NULL
  virtual CarbonChangeType* getChangeArrayRef();

protected:
  const ConstantRange* mRange;
  const UInt32* mControlMask;
  
  //! Set all the constant bits in the vector
  /*!
    This modifies the actual storage in the model by setting the bits
    that are constant 0 or 1, based on the control mask.
  */
  virtual void setConstantBits() = 0;

  //! Complains if drive is set
  /*!
    This is only meant to be used by non-tristate input nets. This
    is used to look for an attempt at de-asserting the drive on a non
    tristate net. 'drive' must not be NULL.
  */
  CarbonStatus checkIfDriveSet(const UInt32* drive, CarbonModel* model);
  
  //! Complains if drive is set in range
  /*!
    Used by depositRange on non-tristate input nets. 'drive' must not
    be NULL.
    \sa checkIfDriveSet
  */
  CarbonStatus checkIfDriveSetRange(const UInt32* drive, int range_msb, int range_lsb, CarbonModel* model);
  
private:
  CarbonVectorBase(const CarbonVectorBase&);
  CarbonVectorBase& operator=(const CarbonVectorBase&);

  CarbonStatus noAllocDepositValRange(const UInt32* val, const UInt32* drv, 
                                      int range_msb, int range_lsb, 
                                      CarbonModel* model);
};

#endif
