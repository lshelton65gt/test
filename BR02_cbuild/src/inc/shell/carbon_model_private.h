// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __carbon_model_private_h_
#define __carbon_model_private_h_

#ifndef __carbon_model_h_
#include "shell/carbon_model.h"
#endif

#ifndef __OSWrapper_h_
#include "util/OSWrapper.h"
#endif

#ifndef _STATS_H_
#include "util/Stats.h"
#endif

class ControlHelper;
class DynBitVector;
class HDLFileSystem;
class HdlIStream;
class HdlOStream;
class MsgContext;
class UtICheckpointStream;
class UtOCheckpointStream;
class VerilogOutFileSystem;

struct CarbonShellCBStruct;             // forward

//! private model data
/*! The carbon_model_private structure contains model data structures that are
 *  allocated and manipulated only by the shell. This header file should not be
 *  included into any of the generated files. The pointer to the private data
 *  should be considered an opaque pointer in the generated
 *  model. Consequently, this object is not versioned. It is instantiated by
 *  the shell, so the shell always works with the "latest" version.
 *
 *  All this could probably be moved into the CarbonHookup object
 *  (src/inc/shell/CarbonHookup.h). It ended here 
 *
 * 
 */
class carbon_model_private {
public:
  CARBONMEM_OVERRIDES

  carbon_model_private (struct carbon_model_descr *, void *);
  ~carbon_model_private ();

  MsgContext *getMsgContext () const { return mMsgContext; }

  ControlHelper *getControlHelper () const { return mControlHelper; }

  VerilogOutFileSystem *getVerilogOutFileSystem () const { return mVerilogOutFileSystem; }
  HdlOStream *getVhdlOutFileSystem () const { return mVhdlOutFileSystem; }
  HdlIStream *getVhdlInFileSystem () const { return mVhdlInFileSystem; }
  HDLFileSystem *getHDLFileSystem () const { return mHDLFileSystem; }

  CarbonShellCBStruct *getShellCallBack () const { return mShellCallBack; }

  CarbonUInt32 checkAndClearRunDepositComboSchedule ();

  void addRunDepositComboSchedule (bool runit) 
  { 
    if (runit) {
      mRunDepositComboSched = 1;
    }
  }

  CarbonUInt32 *getRunDepositComboSchedPointer () { return &mRunDepositComboSched; }

  void incrScheduleCallCount ()
  { mScheduleCallCount++; }

  void resetScheduleCallCount ()
  { mScheduleCallCount = 0; }

  void printIntervalStatistics (const char *label) const
  { mStats->printIntervalStatistics (label); }

  UInt64 getScheduleCallCount () const { return mScheduleCallCount; }

  void allocatePrimaryClockChangeData (size_t numClocks);

  void setClockName (size_t index, const char* name);

  void clearPrimaryClockChanges ();

  void checkPrimaryClockRace (const DynBitVector& curChanges);

  ControlHelper *getControlHelper () { return mControlHelper; }

  void allocateFileSystems ();
  void deleteFileSystems ();

private:

  CarbonObjectID *mDescr;
  MsgContext *mMsgContext;
  MsgStreamIO *mErrStream;
  CarbonUInt32 mRunDepositComboSched;
  CarbonShellCBStruct *mShellCallBack;
  UInt64 mScheduleCallCount;
  Stats *mStats;
  ControlHelper *mControlHelper;

  // Dynamic bitvector to keep track of changed primary clocks (size is model
  // dependent). Note we could use a BitVector and save one some allocation and
  // code but it requires creating this data in the top module class and making
  // templated functions to manipulate them. This seems difficult for unknown
  // gain at this point. But it should be revisited if DynBitVector's cause
  // problems in performance.
  DynBitVector* mChangedPrimaryClocks;

  // Array of strings to print messages about primary clocks (size is
  // model dependent)
  const char** mPrimaryClocks;

  HDLFileSystem *mHDLFileSystem;
  VerilogOutFileSystem *mVerilogOutFileSystem;
  HdlOStream *mVhdlOutFileSystem;
  HdlIStream *mVhdlInFileSystem;

};

typedef void (*CarbonShellCallBackFn) (struct carbon_model_descr *);

//! Callback structure that holds the function and the data
struct CarbonShellCBStruct {
  CARBONMEM_OVERRIDES

  UInt32 mVersion;                      //!< should be the same as for carbon_model

  //! constructor
  CarbonShellCBStruct () : mDebugFunc (0), mDestroyFunc (0),  mClientData (0)
  {}

  //! destructor
  ~CarbonShellCBStruct() {}

  //! Shell callback function
  CarbonShellCallBackFn mDebugFunc;

  //! Shell destroy function
  CarbonShellCallBackFn mDestroyFunc;

  //! Callback's client data
  CarbonOpaque mClientData;

private:
  
  CarbonShellCBStruct (const CarbonShellCBStruct &);
  CarbonShellCBStruct& operator =(const CarbonShellCBStruct &);

};

//! The change array
struct carbon_change_array {

  CarbonChangeType* mChanged;           //!< vector of change values
  CarbonUInt32 mNumElems;               //!< number of elements in mArray
  CarbonChangeType mInit;               //!< value installed by carbonPrivateReInitChangeArray

  //! Save the change array to a checkpoint stream
  bool save (UtOCheckpointStream &out);

  //! Restore the change array from a checkpoint stream
  bool  restore (UtICheckpointStream &in);

  //! Reset the elements of the change array to the initial value
  inline void reInit ()
  { carbonPrivateReInitChangeArray (this); }

private:
  /*! carbon_change_array instances must only be created with
   *  carbonPrivateCreateChangeArray, so declare all the constructors private.
   */
  carbon_change_array ();
  carbon_change_array (const carbon_change_array&);
  carbon_change_array& operator=(const carbon_change_array&);

  //! Hide the delete operator
  void operator delete (void *p);

};

#endif

