// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __SHELLNETCONSTANT_H_
#define __SHELLNETCONSTANT_H_


#ifndef __SHELLNET_H_
#include "shell/ShellNet.h"
#endif

class IODBIntrinsic;

class ShellNetConstant : public ShellNetPrimitive
{
public: CARBONMEM_OVERRIDES
  ShellNetConstant();
  virtual ~ShellNetConstant();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  //! illegal
  virtual CarbonStatus deposit (const UInt32*, const UInt32*, CarbonModel* model);
  //! illegal
  virtual CarbonStatus depositWord (UInt32, int, UInt32, CarbonModel* model);
  //! illegal
  virtual CarbonStatus depositRange (const UInt32*, int, int, 
                                      const UInt32*, CarbonModel* model);

  //! illegal
  virtual void fastDeposit (const UInt32*, const UInt32*, CarbonModel* model);
  //! illegal
  virtual void fastDepositWord (UInt32, int, UInt32, CarbonModel* model);
  //! illegal
  virtual void fastDepositRange (const UInt32*, int, int, 
                                 const UInt32*, CarbonModel* model);
  
  //! ShellNet::format()
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat, NetFlags,
                              CarbonModel* model) const;

  //! ShellNet::castConstant()
  virtual const ShellNetConstant* castConstant() const;

  //! ShellNet::writeIfNotEq()
  ShellNet::ValueState writeIfNotEq(char* valueStr, size_t len, Storage*, NetFlags);

  //! ShellNet::getExternalDrive()
  virtual void getExternalDrive(UInt32* xdrv) const;

  //! Returns false
  virtual bool isReal() const;
  //! Returns false
  virtual bool isTristate() const;
  //! Returns false
  virtual bool setToDriven(CarbonModel*);
  //! Returns false
  virtual bool setToUndriven(CarbonModel*);
  //! Returns false
  virtual bool setWordToUndriven(int, CarbonModel*);
  //! Returns false
  virtual bool resolveXdrive(CarbonModel*);
  //! Returns false
  virtual bool setRangeToUndriven(int, int, CarbonModel*);
  //! Returns error
  virtual CarbonStatus force(const UInt32*, CarbonModel*);
  //! Returns error
  virtual CarbonStatus forceWord(UInt32, int, CarbonModel*);
  //! Returns error
  virtual CarbonStatus forceRange(const UInt32*, int, int, CarbonModel*);
  //! Returns error
  virtual CarbonStatus release(CarbonModel*);
  //! Returns error
  virtual CarbonStatus releaseWord(int, CarbonModel*);
  //! Returns error
  virtual CarbonStatus releaseRange(int, int, CarbonModel*);
  //! returns NULLw
  virtual Storage allocShadow() const;
  //! Does nothing
  virtual void freeShadow(Storage*);
  //! Does nothing
  virtual void update(Storage*) const;
  //! Does unchanged
  virtual ValueState compare(const Storage) const;
  //! Does nothing
  virtual bool isForcible() const;
  //! Does nothing
  virtual bool isInput() const;
  //! returns NULL
  virtual const CarbonMemory* castMemory() const;
  //! returns NULL
  virtual const CarbonModelMemory* castModelMemory() const;
  //! returns NULL
  virtual const CarbonVectorBase* castVector() const;
  //! Returns NULL
  virtual const CarbonScalarBase* castScalar() const;

  //! asserts
  virtual void putToZero(CarbonModel*);
  //! asserts
  virtual void putToOnes(CarbonModel*);
  //! asserts
  virtual CarbonStatus setRange(int, int, CarbonModel*);
  //! asserts
  virtual CarbonStatus clearRange(int, int, CarbonModel*);
  //! returns 0
  virtual int hasDriveConflict() const;
  //! returns 0
  virtual int hasDriveConflictRange(SInt32, SInt32) const;
  //! Returns error
  virtual CarbonStatus examineValXDriveWord(UInt32*, UInt32*, int) const;
  //! Does nothing
  virtual void setRawToUndriven(CarbonModel*);
  //! asserts
  virtual ValueState writeIfNotEqForce(char*, size_t, Storage*, NetFlags, ShellNet*);
  //! asserts
  virtual CarbonStatus formatForce(char*, size_t, CarbonRadix, NetFlags, ShellNet*, CarbonModel*) const;
  //! Does nothing
  virtual void putChangeArrayRef(CarbonChangeType*);
  //! returns NULL
  virtual CarbonChangeType* getChangeArrayRef();
  
  //! Does nothing
  /*!
    This function is used by Replay during playback mode to run change
    callbacks on a net.  This normally asserts on primitives, since
    they should be replaced by a ShellNetPlayback.  However, constants
    are never instrumented during Replay.

    Since callbacks are never triggered on constant nets during normal
    mode, this is a no-op.
   */
  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, 
                                UInt32* newVal, 
                                UInt32* newDrv,
                                CarbonTriValShadow* fullShadow,
                                CarbonModel* model) const;
protected:
  //! Get the primitive (optimized) value.
  /*!
    \param numStoredWords This will have the actual number of words
    stored to represent the constant. This may be less than 
    (bitwidth +31)/32
   */
  virtual const UInt32* getInternalValue(UInt32* numStoredWords) const = 0;

  //! Issue a deposit to constant warning
  void issueDepositWarning(CarbonModel* model);
  
  //! Returns NULL. ShellNetConstOverride implements this.
  virtual const UInt32* getControlMask() const;
};

class ShellNetConstOverride : public ShellNetConstant
{
public: CARBONMEM_OVERRIDES
  ShellNetConstOverride(const UInt32* value, const UInt32* overrideMask, const IODBIntrinsic* intrinsic);
  virtual ~ShellNetConstOverride();
  
  virtual bool isScalar() const;
  virtual bool isVector() const;
  virtual int getLSB() const;
  virtual int getMSB() const;
  virtual int getBitWidth() const;
  virtual int getNumUInt32s() const;
  virtual bool isDataNonZero() const;
  virtual CarbonStatus examine(UInt32* buf, UInt32*, ExamineMode, CarbonModel*) const;
  virtual CarbonStatus examine(CarbonReal* buf, UInt32*, ExamineMode, CarbonModel*) const;
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32*, ExamineMode mode, CarbonModel*) const;
  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, 
                                    int range_lsb, UInt32*, CarbonModel*) const;

  virtual CarbonStatus format(char* valueStr, size_t len, 
                               CarbonRadix strFormat, NetFlags, CarbonModel*) const;

  virtual const UInt32* getControlMask() const;

protected:
  virtual const UInt32* getInternalValue(UInt32* numStoredWords) const;

private:
  const UInt32* mValue;
  const UInt32* mOverrideMask;
  const IODBIntrinsic* mIntrinsic;
};

class ShellNetConstScalar : public ShellNetConstant
{
public: CARBONMEM_OVERRIDES
  ShellNetConstScalar(UInt32 value);
  virtual ~ShellNetConstScalar();
  
  virtual bool isScalar() const;
  virtual bool isVector() const;
  virtual int getBitWidth() const;
  virtual int getNumUInt32s() const;
  virtual bool isDataNonZero() const;
  virtual CarbonStatus examine(UInt32* buf, UInt32*, ExamineMode, CarbonModel*) const;
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32*, ExamineMode mode, CarbonModel*) const;
  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, 
                                     int range_lsb, UInt32*, CarbonModel*) const;

  //! Returns error
  virtual CarbonStatus examine(CarbonReal* buf, UInt32*, ExamineMode, CarbonModel*) const;

protected:
  virtual const UInt32* getInternalValue(UInt32* numStoredWords) const;

private:
  UInt32 mValue;
};

class ShellNetConstVector : public ShellNetConstant
{
public: CARBONMEM_OVERRIDES
  ShellNetConstVector(const UInt32* value, UInt32 numWords, const IODBIntrinsic* intrinsic);
  virtual ~ShellNetConstVector();

  virtual bool isVector() const;
  virtual bool isScalar() const;
  virtual int getBitWidth() const;
  virtual int getNumUInt32s() const;
  virtual bool isDataNonZero() const;
  virtual int getLSB() const;
  virtual int getMSB() const;
  virtual CarbonStatus examine(UInt32* buf, UInt32*, ExamineMode, CarbonModel*) const;
  virtual CarbonStatus examine(CarbonReal* buf, UInt32*, ExamineMode, CarbonModel*) const;
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32*, ExamineMode mode, CarbonModel*) const;
  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, 
                                     int range_lsb, UInt32*, CarbonModel*) const;

protected:
  virtual const UInt32* getInternalValue(UInt32* numStoredWords) const;  

private:
  const UInt32* mValue;
  const IODBIntrinsic* mIntrinsic;
  UInt32 mNumStoredWords; // Number of words in mValue
};

class ShellNetConstReal : public ShellNetConstVector
{
public:
  ShellNetConstReal(const UInt32* value, const IODBIntrinsic* intrinsic);
  
  virtual ~ShellNetConstReal();
  virtual CarbonStatus examine(CarbonReal* buf, UInt32*, ExamineMode, CarbonModel*) const;
  virtual bool isReal() const;
};

class ShellNetConstVectorWord : public ShellNetConstant
{
public: CARBONMEM_OVERRIDES
  ShellNetConstVectorWord(UInt32 value, const IODBIntrinsic* intrinsic);
  virtual ~ShellNetConstVectorWord();

  virtual bool isVector() const;
  virtual bool isScalar() const;
  virtual int getBitWidth() const;
  virtual int getNumUInt32s() const;
  virtual bool isDataNonZero() const;
  virtual int getLSB() const;
  virtual int getMSB() const;
  virtual CarbonStatus examine(UInt32* buf, UInt32*, ExamineMode, CarbonModel*) const;
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32*, ExamineMode mode, CarbonModel*) const;
  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, 
                                    int range_lsb, UInt32*, CarbonModel*) const;

  //! returns error.
  virtual CarbonStatus examine(CarbonReal* buf, UInt32*, ExamineMode, CarbonModel*) const;

protected:
  virtual const UInt32* getInternalValue(UInt32* numStoredWords) const;  

private:
  const IODBIntrinsic* mIntrinsic;
  UInt32 mValue;
};

#endif
