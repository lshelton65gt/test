// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CARBONREPLAYRECORDER_H_
#define CARBONREPLAYRECORDER_H_

#include "shell/ShellNetRecord.h"
#include "shell/CarbonNetValueCBData.h"
#include "shell/ShellMemoryCreateInfo.h"
#include "shell/CarbonReplayInfo.h"
#include "shell/carbon_model.h"
#include "symtab/STSymbolTable.h"
#include "util/ShellMsgContext.h"
#include "util/UtBinIStream.h"
#include "util/UtIOStream.h"

//! This turns on debugging output for cmodels in both record and playback
#define REPLAY_CMODEL_VERBOSE 0

namespace CarbonReplay
{
  //! Enumeration for event tags
  /*!
    Tags are used to mark data in the database as a particular type
  */
  enum Tag {
    eNoEvent = '\0', //!< No event
    eCheckpoint = '@', //!< Checkpoint tag
    eStimuli = 's', //!< Stimuli tag
    eResponse = 'r', //!< Response tag
    eSchedule = 'u', //!< a schedule api func called
    eMemWrite = 'm', //!< memory address write
    eEmptyTouchedStimuli = 'c', //!< Empty touched stim buffer
    eEmptyTouchedResponse = 'd', //!< Empty touched response buffer
    eEndOfFile = 'e', //!< End of file tag (to validate file)
    eCModelCall = 'f', //!< CModel call
    eCModelInput = 'g', //!< CModel input value
    eCModelOutput = 'h', //!< CModel output value
    eCModelPreOutput = 'i' //!< CModel output value prior to call
  };
  
  // List of schedule types in char form
  static const char scNormalSchedule = 'b';
  static const char scClkSchedule = 'c';
  static const char scDataSchedule = 'd';
  static const char scInitialSchedule = 'e';
  static const char scAsyncSchedule = 'a';

  // List of memory write types
  static const char scExternalMemWrite = 'e';
  static const char scInternalMemWrite = 'i';

  // List of char tags for index files
  // Index files are textual, so using an enum here is cumbersome
  
  //! '#' comment tag
  static const char scIndexComment = '#';
  //! 'f' event file tag
  static const char scIndexEventFile = 'f';

  static const char scChkPtPrefix[] = "checkpoint_";
  
  /*! 
    Helper function that simply takes an array of anything and
    resizes it if the requested index is not yet available in the
    array.
  */
  template<typename T> void sFitIndexIntoArray(T& arr, UInt32 index)
  {
    if (arr.size() <= index)
      arr.resize(index + 1);
  }
  
  //! Object for managing a touched buffer
  /*!
    This provides a central control point for the individual
    ShellNetReplay::Touched objects for each recorded net.  It serves
    as a factory for them.

    DynBitVectors are reserved for the touched and masked bits of the
    Touched objects, and their storage is referenced when the Touched
    object is created.  This centralized storage allows all
    touched/masked bits to be set/reset easily.
  */
  class TouchedContainer
  {
  public:
    CARBONMEM_OVERRIDES

    TouchedContainer() : mNumNets(0),
                         mIndex(0)
    {
    }

    ~TouchedContainer()
    {
      cleanTouched();
    }

    //! Only call this once
    void putSize(UInt32 numNets)
    {
      INFO_ASSERT(mNumNets == 0, "putSize called multiple times");
      mNumNets = numNets;
      // Need space for both touched and masked bits
      mTouchedAndMaskedBuffer.resize(numNets * ShellNetReplay::Touched::eBitsPerEntry);

      for (UInt32 i = 0; i < mNumNets; ++i) {
        // Scale to account for the pair of bits.
        ShellNetReplay::Touched* touched = new ShellNetReplay::Touched(&mTouchedAndMaskedBuffer, i * ShellNetReplay::Touched::eBitsPerEntry);
        mTouchedNets.push_back(touched);
      }
    }

    //! Assign all the touched buffers to 1, and clear all masked buffers
    void setAll()
    {
      // Each net has a pair of bits in the buffer.  The even bit is
      // the touched flag and the odd bit is the masked flag.  This is
      // not terribly efficient, but this is called seldomly during
      // simulation.  It's more important to make clear() fast, since
      // that's called after every schedule call.
      for (UInt32 i = 0; i < mNumNets; ++i) {
        UInt32 bit = i * ShellNetReplay::Touched::eBitsPerEntry + ShellNetReplay::Touched::eTouchedBitOffset;
        mTouchedAndMaskedBuffer.set(bit);
      }
    }

    //! Get the size of the buffer (number of touched buffers)
    UInt32 getSize() const { return mNumNets; }

    //! Clear all touched/masked buffers
    void clear()
    {
      mTouchedAndMaskedBuffer.reset();
    }

    //! Retrieve the touched buffer at the next index
    ShellNetReplay::Touched* allocNextTouched(UInt32* index)
    {
      INFO_ASSERT(mIndex < mNumNets, "Allocation beyond the end of a replay change array buffer");
      *index = mIndex;
      return mTouchedNets[mIndex++];
    }

    //! Retrieve the touched buffer at the given index.
    ShellNetReplay::Touched* allocAt(UInt32 index)
    {
      INFO_ASSERT(index < mNumNets, "Allocation beyond the end of a replay change array buffer");
      mIndex = index;
      return mTouchedNets[mIndex];
    }
    
    //! Get the underlying touched/masked buffer DynBV
    DynBitVector* getBuffer() { return &mTouchedAndMaskedBuffer; }

    //! Get the underlying touched/masked buffer DynBV
    const DynBitVector* getBuffer() const { return &mTouchedAndMaskedBuffer; }

    //! Get the array within the touched/masked DynBV
    const UInt32* getBufferArray() const { return mTouchedAndMaskedBuffer.getUIntArray(); }

    //! Number of words needed to represent the buffer
    UInt32 numWords() const { return mTouchedAndMaskedBuffer.getUIntArraySize(); }

    //! This does not copy the pointers from src, it creates its own.
    TouchedContainer& operator=(const TouchedContainer& src)
    {
      if (&src != this)
      {
        // Remove any Touched buffers already allocated.
        cleanTouched();
        // Allocate my own touched buffers based on the src's size
        putSize(src.mNumNets);
        // Now copy the dynbvs. Note that the resize in putSize
        // makes the two bvs equal in size, so I don't have to worry
        // about a resize happening in the operater=.
        mTouchedAndMaskedBuffer = src.mTouchedAndMaskedBuffer;
        // Now copy the index
        mIndex = src.mIndex;
      }
      return *this;
    }

    //! Copy the values from src
    /*!
      This assumes that src and this are the same size. The underlying
      Touched buffer pointers are not destroyed. This simply copies the
      src's mTouchBuffer, mMaskedBuffer, and mIndex.
    */
    void copy(const TouchedContainer& src)
    {
      if (&src != this)
      {
        // This assert is expensive during recovery
        //INFO_ASSERT(src.mNumNets == mNumNets, "Invalid src container size passed to copyEq");
        // copy the touched buffer value
        DynBitVector::sEqualSizeDynBVCopy(&mTouchedAndMaskedBuffer, src.mTouchedAndMaskedBuffer);
        // and the index
        mIndex = src.mIndex;
      }
    }

  private:
    //! The touched/masked buffer
    /*!
      Entries in this buffer are allocated in pairs.  The low (even)
      bit represents the touched flag, and the high (odd) bit
      represents the masked flag.
     */
    DynBitVector mTouchedAndMaskedBuffer;
    
    typedef UtArray<ShellNetReplay::Touched*> TouchedVec;
    //! The Touched array
    TouchedVec mTouchedNets;
    //! The number of nets
    UInt32 mNumNets;
    //! The current net index
    UInt32 mIndex;

    void cleanTouched()
    {
      for (UInt32 i = 0; i < mNumNets; ++i)
        delete mTouchedNets[i];
      mTouchedNets.resize(0);
    }

    // forbid
    TouchedContainer(const TouchedContainer&);
  };
  
//! Object to manage TouchedContainers
/*!
  All replay nets (record and playback) have a Touched object.  During
  record mode, all nets update their corresponding Touched object when
  they're deposited.  During playback mode, all nets have their
  Touched object updated when a new value is read from the playback
  stream.

  Touched objets are allocated from two different container objects -
  one for nets representing clocks and state outputs, and one
  representing other nets.  This is because clocks and state outputs
  need special consideration during playback and recovery.

  For other nets, the current value can simply be compared with the
  expected value to determine if a divergence has occurred.  This is
  because the net's value can only change through the API.  However,
  clocks and state outputs are different.  For clocks, a fake edge can
  be created, in which case the final value of the net will not have
  changed.  For state outputs, a redeposit of the last deposited value
  may be significant, because the internal value of the net may have
  changed since the last deposit.

  In both cases, this new deposit is indistinguishable from the
  absence of a deposit if we only compare the value buffer.  For
  clocks and state outputs, we need to compare the Touched objects as
  well.  This is easier if they're all allocated from a single
  container, because we can just compare the container's contiguous
  storage.

  During record mode, no special treatment is needed for the
  clock/state output container vs. the other one.  However, this
  object is saved to the replay database during record and restored
  during playback, so it needs to have the same contents during both
  modes.
*/
  class StimuliTouched
  {
  public:
    CARBONMEM_OVERRIDES

    StimuliTouched();
    
    //! Alloc a touched buffer, return the index.
    /*!
      Indices are not unique. isClockOrStateOutput determines which touched array
      gets used.
    */
    ShellNetReplay::Touched* allocTouched(UInt32* touchedIndex, bool isClockOrStateOutput)
    {
      if (isClockOrStateOutput)
        return mTouchedClksAndStateOutputs.allocNextTouched(touchedIndex);
      else
        return mTouchedOther.allocNextTouched(touchedIndex);
      return NULL;
    }

    //! Alloc a touched buffer at the index
    /*!
      Indices are not unique. isClockOrStateOutput determines which touched array
      gets used.
    */
    ShellNetReplay::Touched* allocTouchedAt(UInt32 touchedIndex, bool isClockOrStateOutput)
    {
      if (isClockOrStateOutput)
        return mTouchedClksAndStateOutputs.allocAt(touchedIndex);
      else
        return mTouchedOther.allocAt(touchedIndex);
      return NULL;
    }

    //! Clear the value of the touched buffer
    void clear()
    {
      mTouchedClksAndStateOutputs.clear();
      mTouchedOther.clear();
    }

    //! Copy assuming same size src, no realloc
    void copy(const StimuliTouched& src)
    {
      mTouchedClksAndStateOutputs.copy(src.mTouchedClksAndStateOutputs);
      mTouchedOther.copy(src.mTouchedOther);
    }
    
    void putNumClksAndStateOutputs(UInt32 num)
    {
      mTouchedClksAndStateOutputs.putSize(num);
    }
   
    void putNumOther(UInt32 num)
    {
      mTouchedOther.putSize(num);
    }

    //! Calculate the size based on number of clks/state outputs and total inputs
    void calcSize(UInt32 numClocksAndStateOutputs, UInt32 totalNumInputs)
    {
      INFO_ASSERT(totalNumInputs >= numClocksAndStateOutputs, "Inconsistent number of inputs.");
      
      mTouchedClksAndStateOutputs.putSize(numClocksAndStateOutputs);
      mTouchedOther.putSize(totalNumInputs - numClocksAndStateOutputs);
    }

    //! Set all the clock and state output touches to 1
    void setAllClksAndStateOutputs()
    {
      mTouchedClksAndStateOutputs.setAll();
    }

    //! Writes this object to the db
    bool dbWrite(ZostreamDB& out) const;
    //! Reads this object to the db
    bool dbRead(ZistreamDB& in);
    //! Writes only the number of clks/non-clks
    bool writeSize(ZostreamDB& out) const;
    //! Reads only the number of clks/non-clks
    /*!
      Can only be called once.
    */
    bool readSize(ZistreamDB& in);


    //! Retrieve the underlying UInt32 array in the clock and state output touch buffer
    const UInt32* getClkAndStateOutputBufferArray() const {
      return mTouchedClksAndStateOutputs.getBufferArray();
    }

    //! Number of entries in the clock and state output touch buffer
    UInt32 getNumClkAndStateOutputNets() const { return mTouchedClksAndStateOutputs.getSize(); }

    //! Number of words needed to represent the clock and state output touch buffer
    UInt32 getNumClkAndStateOutputWords() const {
      return mTouchedClksAndStateOutputs.numWords();
    }
    
  private:
    //! Container/factory for Touched objects for clocks and state outputs
    TouchedContainer mTouchedClksAndStateOutputs;
    //! Container/factory for Touched objects for other nets
    TouchedContainer mTouchedOther;
  };

  //! File closure for replay checkpoints
  /*!
    This gets used to add/read extra info to/from the checkpoints
    during replay.
  */
  struct ReplayCheckpointClosure
  {
    CARBONMEM_OVERRIDES
    
    ReplayCheckpointClosure(DynBitVector* stim,
                            StimuliTouched* stimTouched,
                            DynBitVector* response,
                            DynBitVector* responseTouched,
                            ReplayCModelMap *cModelMap);

    //! writes this object to the stream
    bool dbWrite(ZostreamDB& out) const;
    //! reads this object from the stream
    bool dbRead(ZistreamDB& in);
    //! writes CModel outputs to the stream
    void writeCModelOutputs(ZostreamDB &out) const;
    //! reads CModel inputs from the stream
    void readCModelOutputs(ZistreamDB &in);
    
  private:
    static const UInt32 cVersion;

    DynBitVector* mReplayStimuli;
    DynBitVector* mReplayResponse;
    DynBitVector* mReplayTouchedResponse;
    StimuliTouched* mReplayStimTouched;
    typedef UtHashMap<UInt32, ReplayCModel*> IdCModelMap;
    IdCModelMap mIdCModelMap;
  };  


  //! Object that holds Net callback data and shadow
  struct CBDataValuePair
  {
    CARBONMEM_OVERRIDES

    CBDataValuePair(CarbonNetValueCBData* cbData)
      : mCBData(cbData),
        mValDrvShadow(cbData->getShellNet()->getNumUInt32s())
    {
      // initialize the shadow
      updateShadow();
    }
    
    CarbonNetValueCBData* getCBData() { return mCBData; }
    CarbonTriValShadow* getValDrvShadow() { return &mValDrvShadow; }
    
    void updateShadow()
    {
      mCBData->getShellNet()->examine(mValDrvShadow.mValue,
                                      mValDrvShadow.mDrive,
                                      ShellNet::eCalcDrive, NULL);
    }

  private:
    CarbonNetValueCBData* mCBData;
    CarbonTriValShadow mValDrvShadow;
  };

  typedef UtArray<CBDataValuePair*> CBDataValuePairVec;

  //! Class for writing the event db
  /*!
    This wraps whatever file i/o methodology we wish to use for the
    db.
  */
  class EventDBWriter
  {
  public:
    CARBONMEM_OVERRIDES

    EventDBWriter() : mFd(NULL)
    {}
    
    ~EventDBWriter()
    {
      delete mFd;
    }

    bool isOpen(void) { return (mFd != NULL) && mFd->is_open(); }

    void open(const UtString& filename)
    {
      delete mFd;
      mFd = new UtOBStream(filename.c_str());
    }

    bool close() { return mFd->close(); }
    bool bad() const { return mFd->bad(); }
    const char* getErrmsg() { return mFd->getErrmsg(); }
    
    void write(const UInt8* data, UInt32 len)
    {
      const char* buf = reinterpret_cast<const char*>(data);
      mFd->write(buf, len);
    }

    EventDBWriter& operator<<(CarbonStatus stat)
    {
      char val = stat;
      mFd->write(&val, 1);
      return *this;
    }
      
    EventDBWriter& operator<<(const char c)
    {
      mFd->write(&c, 1);
      return *this;
    }

    EventDBWriter& operator<<(const char* str)
    {
      UInt32 len = strlen(str);
      mFd->write((const char*)(&len), sizeof(UInt32));
      mFd->write(str, len);
      return *this;
    }


    EventDBWriter& operator<<(const UtString& str)
    {
      UtString copy(str);
      UInt32 len = copy.size();
      mFd->write((const char*)(&len), sizeof(UInt32));
      mFd->write(copy.getBuffer(), len);
      return *this;
    }

    EventDBWriter& operator<<(UInt64 num)
    {
      mFd->write((const char*)(&num), sizeof(UInt64));
      return *this;
    }

    EventDBWriter& operator<<(UInt32 num)
    {
      mFd->write((const char*)(&num), sizeof(UInt32));
      return *this;
    }

    EventDBWriter& operator<<(SInt32 num)
    {
      mFd->write((const char*)(&num), sizeof(SInt32));
      return *this;
    }

    EventDBWriter& operator<<(UtIO::Endl)
    {
      char tmp = '\n';
      mFd->write(&tmp, 1);
      // mFd->flush();
      return *this;
    }

    EventDBWriter& operator<<(Tag c)
    {
      char tmp = c;
      mFd->write(&tmp, 1);
      return *this;
    }

  private:
    UtOBStream* mFd;
  };

  //! Class to handle stim and response net changes
  class ReplayChangeCallback: public ReplayRecordBuffer::ChangeCallback
  {
  public: 
    CARBONMEM_OVERRIDES
  
    //! Structure to store compressed offset/size for a changed value event
    /*! We want to minimize the reading so we break up a 32-bit words
     *  into 24 bits of offset and 8 bits of size. If an event buffer
     *  ever gets to greater than 16 meg, this won't work.
     */
    union OffsetSizePair {
      struct {
        UInt32      offset: 24;
        UInt8       size;
      } parts;
      UInt32        whole;
    };
  
    //! Constructor
    ReplayChangeCallback(EventDBWriter& stream, Tag tag) : 
      mStream(stream), mTag(tag), mTagWritten(false)
    {}
  
    //! Write this value change
    virtual void changedData(UInt32 startByte, UInt32 size, const UInt8* value)
    {
      // If we haven't written the tag yet, do so now
      if (!mTagWritten) {
        mStream << mTag;
        mTagWritten = true;
      }
    
      // The maximum size is 255 so write up to that at a time
      UInt32 bytesLeft = size;
      UInt32 curOffset = 0;
      while (bytesLeft > 0) {
        // Write the offset/size pair
        OffsetSizePair pair;
        pair.parts.offset = startByte + curOffset;
        pair.parts.size = std::min(bytesLeft, (UInt32)255);
        mStream << pair.whole;
      
        // Write the data
        mStream.write(&value[curOffset], pair.parts.size);

        // Move on to the next set of data
        bytesLeft -= pair.parts.size;
        curOffset += pair.parts.size;
      }
    }

    void writeTerminator()
    {
      // Only need to terminate if we wrote something
      if (mTagWritten) {
        // Since we are terminating the data, clear the tag. The
        // terminator has a size/offset of 0 since a size of 0 is
        // invalid.
        mTagWritten = false;
        mStream << (UInt32)0;
      }
    }

    void writeTerminator(Tag terminateTag)
    {
      bool isTagWritten = mTagWritten;
      writeTerminator();
      if (! isTagWritten)
        mStream << terminateTag;
    }

  private:
    //! The stream to write changes to
    EventDBWriter& mStream;

    //! The tag for this buffer - it is not written if nothing changes
    Tag mTag;

    //! If set we have already written the tag
    bool mTagWritten;
  };

  class ReplayRecorder
  {
  public:
    CARBONMEM_OVERRIDES


    //! constructor - opens the event db
    ReplayRecorder(const UtString& dbName, const char* fileName, CarbonModel* model, CarbonReplayInfo* replayInfo);
    
    //! destructor
    ~ReplayRecorder();
    
    //! A vector of record response nets
    typedef UtArray<ShellNetRecordResponse*> RecResponseVec;
    //! Looper for record response net vector
    typedef Loop<RecResponseVec> ResponseIter;

    //! Loop the response nets
    ResponseIter loopResponseNets() 
    {
      return ResponseIter(mRecordResponseNets);
    }

    //! A vector of STALNs
    typedef UtArray<ShellNetRecord*> RecordNetVec;
    //! Looper for STALN vector
    typedef Loop<RecordNetVec> RecordNetVecIter;

    //! Loop the clocks (non state output) that are being recorded
    /*!
      State output clocks are excluded because they're in the state output set.
     */
    RecordNetVecIter loopRecordClocks() {
      return RecordNetVecIter(mRecordClocks);
    }

    //! Loop the state outputs that are being recorded
    RecordNetVecIter loopRecordStateOutputs() {
      return RecordNetVecIter(mRecordStateOutputs);
    }

    //! Add a clock (non state output) that will be recorded
    /*!
      State output clocks are excluded because they're in the state output set.
     */
    void addRecordClock(ShellNetRecord* clk)
    {
      mRecordClocks.push_back(clk);
    }

    //! Add a state output that will be recorded
    void addRecordStateOutput(ShellNetRecord* net)
    {
      mRecordStateOutputs.push_back(net);
    }

    UInt32 numRecordClocks() const { return mRecordClocks.size(); }

    UInt32 numRecordStateOutputs() const { return mRecordStateOutputs.size(); }

    //! Add a response net
    void addResponseNet(ShellNetRecordResponse* responseNet)
    {
      mRecordResponseNets.push_back(responseNet);
    }

    void recordInitialSchedule()
    {
      recordScheduleCall(scInitialSchedule, 0, eCarbon_OK);
    }
    
    //! Record the current deposit buffer to db
    void recordStimuli()
    {
      // memory changes
      recordExternalMemWrites();

      // Write the net values that changed first - this sets the
      // touched values.
      recordValueBuffers(mRecordInputBuffer, mInputValueChangeCallback, eEmptyTouchedStimuli);
    }
    
    //! Record the current response buffer to db.
    void recordResponse()
    {
      // Memory changes
      recordInternalMemWrites();

      // Compare the response values. This sets the touched bits and
      // writes the changes values to the record stream.
      recordValueBuffers(mRecordResponseBuffer, mResponseValueChangeCallback, eEmptyTouchedResponse);
    }
    
    //! Record a checkpoint file
    void recordCheckpointDBEvent(const UtString& fileName)
    {
      mRecordStream << eCheckpoint << fileName;
    }
    
    //! Get the current status. Error if in a bad state
    /*!
      Returns the current status of the db file. If it is in a bad
      state, it prints an error message.
    */
    CarbonStatus getStatus()
    {
      if (mStatus == eCarbon_OK)
      {
        if (mIndexStream.bad())
        {
          mCarbonModel->getMsgContext()->SHLFileProblem(mIndexStream.getErrmsg());
          mStatus = eCarbon_ERROR;
        }
        else if (mRecordStream.bad())
        {
          mCarbonModel->getMsgContext()->SHLFileProblem(mRecordStream.getErrmsg());
          mStatus = eCarbon_ERROR;
        }
      }
      return mStatus;
    }

    // Do a simsave here and record the event.
    CarbonStatus recordCheckpoint()
    {
      UtString chkPointFileName(scChkPtPrefix);
      chkPointFileName << mCurCheckpointNum;
      ++mCurCheckpointNum;
      
      UtString chkPointFilePath;
      OSConstructFilePath(&chkPointFilePath, mDBName->c_str(), chkPointFileName.c_str());
      
      ReplayCheckpointClosure 
        chkPointClosure(mRecordInputBuffer->getValueBufferPointer(),
                        &mStimuliTouched,
                        mRecordResponseBuffer->getValueBufferPointer(),
                        mResponseTouchedContainer.getBuffer(),
                        mCarbonModel->getReplayCModelMap());
      
      CarbonStatus stat = mCarbonModel->saveReplayCheckpoint(chkPointFilePath.c_str(), chkPointClosure);
      if (stat == eCarbon_OK)
      {
        // record the event to the event file. Note we only save the
        // filename, not the path, in case we move the instance
        // directory to another system.
        recordCheckpointDBEvent(chkPointFileName);
        
        // Now call the checkpoint callbacks
        mReplayInfo->callCheckpointCBs(mTotNumSchedCalls, mCurCheckpointNum);
      }

      return stat;
    }
    
    //! Recompute the checkpoint frequency
    /*!
      Recompute the checkpoint freq based on recovery percentage
      and total number of schedule calls so far.
    */
    void recomputeCheckpointFreq()
    {
      UInt32 recoverPercentage = mReplayInfo->getRecoverPercentage();
      if ((recoverPercentage > 0) & (recoverPercentage < 100))
      {
        double actualPercent = recoverPercentage/100.0;
        // This rounds to the nearest integer
        double preciseCalc = mTotNumSchedCalls * actualPercent;
        UInt64 percentBasedCalc = static_cast<UInt64>(preciseCalc + 0.5);
        mCheckpointFreq = std::max(mReplayInfo->getMinSaveFrequency(),
                                   percentBasedCalc);
      }
      else if (recoverPercentage == 0)
        // fixed interval
        mCheckpointFreq = mReplayInfo->getMinSaveFrequency();
      else if (recoverPercentage == 100)
        // never take a snapshot, set mCheckpointFreq to 0.
        mCheckpointFreq = 0;
    }

    //! This makes the recorder take a checkpoint on the next schedule call
    void scheduleCheckpoint()
    {
      // doing an explicit cast here to make sure gcc treats the rhs
      // as an SInt64, so I can get a -1.
      mNumSchedCallsSinceChkPt = static_cast<SInt64>(mCheckpointFreq - 1);
    }
    
    //! Increment the schedule call total and take a checkpoint, if
    //! applicable
    /*!
      Returns true if everything is ok. Returns false if there were
      any problems, including file errors.
    */
    bool updateCheckpointStatus()
    {
      bool isGood = true;

      ++mTotNumSchedCalls;

      // If we have reached the checkpoint limit then end the current
      // event file, open a new one and record the checkpoint.
      ++mNumSchedCallsSinceChkPt;
      // We do an == here because a save freq of 0 never checkpoints
      if ((unsigned)mNumSchedCallsSinceChkPt == mCheckpointFreq)
      {
        recomputeCheckpointFreq();
        mNumSchedCallsSinceChkPt = 0;
 
        // Close the curent file and open a new one
        openNextEventFile();
        
        // Check the status first. If that is bad, don't
        // bother recording a checkpoint.
        if ((getStatus() != eCarbon_OK) ||
            recordCheckpoint() != eCarbon_OK)
          isGood = false;
      }
      return isGood;
    }

    //! Record a schedule event to the db
    /*!
      This records the current time and the current schedule
      call type.
      It then updates the response nets, and records the response
      buffer.
    */
    void recordScheduleCall(const char scheduleType, UInt64 /*simTime*/,
                            CarbonStatus schedStat)
    {
      // Record the schedule call
      mRecordStream << eSchedule << scheduleType << schedStat;

      // Update the response of the Carbon Model. Note that this has to be called
      // after the Carbon Model schedule function has been run.
      updateResponse();
      // Now write the response to disk
      recordResponse();

      // Now clear the input touched buffers. We can't clear it until
      // here because the recording of response buffers uses that
      // data.
      mStimuliTouched.clear();

      mRecordedScheduleCall = true;
    }

    void allocateInputBuffer(UInt32 numWords)
    {
      mRecordInputBuffer = new ReplayRecordBuffer(numWords);
    }

    ReplayRecordBuffer* getInputBuffer(void) { return mRecordInputBuffer; }

    //! Get the input size in bits
    UInt32 getInputBufferWidth() const
    {
      return mRecordInputBuffer->getNumDataWords() * sizeof(UInt32) * 8;
    }

    //! Get the input size in words
    UInt32 getInputBufferWords() const
    {
      return mRecordInputBuffer->getNumDataWords();
    }

    //! Writes the size of the stimulus touched buffer
    void writeStimulusTouchedSize(ZostreamDB& out) const;
    
    void allocateResponseBuffer(UInt32 numWords, UInt32 numNets)
    {
      mRecordResponseBuffer = new ReplayRecordBuffer(numWords);
      mResponseTouchedContainer.putSize(numNets);
      // Set this to all true for now, so change detection can
      // happen for the response buffer. Real change detection for
      // response nets is not yet implemented.
      mResponseTouchedContainer.setAll();
    }

    ReplayRecordBuffer* getResponseBuffer(void) { return mRecordResponseBuffer; }

    //! Get the response size in bits
    UInt32 getResponseBufferWidth() const
    {
      return mRecordResponseBuffer->getNumDataWords() * sizeof(UInt32) * 8;
    }
    
    //! Get the response size in words
    UInt32 getResponseBufferWords() const
    {
      return mRecordResponseBuffer->getNumDataWords();
    }
    
    //! Get the number of response nets
    UInt32 getNumResponseNets() const 
    {
      return mResponseTouchedContainer.getSize();
    }

    bool close(UtString* errMsg)
    {
      mRecordStream << eEndOfFile;
      bool ret = mRecordStream.close();
      if (! ret)
        *errMsg << mRecordStream.getErrmsg();

      return ret;
    }

    void closeRecordFile()
    {
      if (mRecordStream.isOpen()) {
        UtString errMsg;
        if (!close(&errMsg)) {
          mCarbonModel->getMsgContext()->SHLFileProblem(errMsg.c_str());
        }
      }
    }


    void openNextEventFile()
    {
      // Terminate the previous file if we have one open)
      closeRecordFile();

      UtString eventFileName("event.");
      eventFileName << mNumEventFiles;
      UtString fullEventPath;
      OSConstructFilePath(&fullEventPath, mDBName->c_str(), eventFileName.c_str());
      
      ++mNumEventFiles;
      mRecordStream.open(fullEventPath);

      UtString timeStr;
      OSGetTimeStr("%b %d, %Y  %H:%M:%S", &timeStr);
      mIndexStream << scIndexComment << " " << timeStr << UtIO::endl;
      // Save the time and the total number of schedule calls since
      // simulation began (may be different than since record began)
      mIndexStream << scIndexComment 
                   << " Simulation time: " << mCarbonModel->getSimulationTime()
                   << ", " 
                   << mCarbonModel->getTotNumSchedCalls() << " schedule calls"
                   << UtIO::endl;
      // Save the event filename, not the absolute path
      mIndexStream << scIndexEventFile << " " << eventFileName << UtIO::endl;
      // Flush the file now, so we can monitor manually what event
      // file is being/has been recorded.
      mIndexStream.flush();
    }

    //! Add a memory that can be written to by the api
    ShellNetRecordMem* addExternalRecordMem(ShellNet* primMem)
    {
      UInt32 index = mExternalRecordMems.size();
      ShellNetRecordMem* recordMem = new ShellNetRecordMem(primMem, index, allocAddrChangeSet());
      mExternalRecordMems.push_back(recordMem);
      return recordMem;
    }

    //! Add a memory that can be written to by the vhm.
    /*!
      \note Since all the api-accessible memories are add as external
      record mems, this is a subset of those.
    */
    void addInternalRecordMem(ShellNetRecordMem* recordMem, ShellMemoryCBManager* cbManager)
    {
      // Create a callback for internal memory writes
      ShellGlobal::MemoryCallback* cb = cbManager->allocCB();
      
      // Create an info structure so we can record the response
      UInt32 index = mInternalMemWriteInfos.size();
      InternalMemWriteInfo* writeInfo = new InternalMemWriteInfo(recordMem, cb);
      mInternalMemWriteInfos.push_back(writeInfo);
      recordMem->putInternalIndex(index);
      
      // setup the callback.
      // For our purposes, we need the primitive net as the callback
      // memid. It will be used to remove the callback.
      cb->mMemId = recordMem->getNet()->castMemory();
      cb->mUserData = writeInfo;
      cb->mMemWriteCB = sInternalMemWrite;
    }

    //! Create a set of addr changes (passed to a ShellNetRecord)
    SInt32HashSet* allocAddrChangeSet() 
    {
      SInt32HashSet* hashSet = new SInt32HashSet;
      mAddrChangeSets.push_back(hashSet);
      return hashSet;
    }
    

    //! Public access to the record stream. Used for cmodel record
    EventDBWriter& getRecordStream() { return mRecordStream; }

    void reserveTouched(UInt32 numClocksAndStateOutputs, UInt32 totalNumInputs)
    {
      mStimuliTouched.calcSize(numClocksAndStateOutputs, totalNumInputs);

      // Set all the clock/state output touches to 1 since the first schedule call
      // writes out all the touched buffers. This also keeps the
      // player and the recorder consistent in checkpointing.
      mStimuliTouched.setAllClksAndStateOutputs();
    }

    ShellNetReplay::Touched* allocStimuliTouched(UInt32* index, bool isClock)
    {
      return mStimuliTouched.allocTouched(index, isClock);
    }

    ShellNetReplay::Touched* allocResponseTouched(UInt32* index)
    {
      return mResponseTouchedContainer.allocNextTouched(index);
    }

  private:
    
    //! Object for handling vhm-caused memory writes
    /*!
      This is given to the vhm memories as user data to be passed in
      the memory write callbacks.
    */
    struct InternalMemWriteInfo
    {
      CARBONMEM_OVERRIDES
      
      InternalMemWriteInfo(ShellNetRecordMem* mem, ShellGlobal::MemoryCallback* cb)
        : mMem(mem), mCallback(cb)
      {}
      
      void clearChanges()
      {
        mAddrChanges.clear();
      }
      
      ShellNetRecordMem* mMem;
      ShellGlobal::MemoryCallback* mCallback;
      SInt32HashSet mAddrChanges;
    };
    
    //! The callback for internal memory writes.
    static void sInternalMemWrite(CarbonMemoryID*,
                                  CarbonClientData userData,
                                  CarbonSInt64 addr)
    {
      /* Note: We cannot get the value of the memory at the given
         address because it may not yet be there. Most of the time,
         the vhm writes a memory like:
         mem.write(addr) = value;
         
         This gets called during the write() call. The value is not
         yet there. So, we have to wait until the schedule call is
         over (otherwise, instrument the operator= method on each
         memory type).
      */
      InternalMemWriteInfo* writeInfo = static_cast<InternalMemWriteInfo*>(userData);
      writeInfo->mAddrChanges.insert(addr);
    }

  private:
    UtOBStream mIndexStream;
    EventDBWriter mRecordStream;
    CarbonModel* mCarbonModel;
    CarbonStatus mStatus;
    CarbonReplayInfo* mReplayInfo;
    UInt32 mNumEventFiles;
    UInt32 mCurCheckpointNum;
    SInt64 mNumSchedCallsSinceChkPt;
    // Total number of schedule calls since record started
    UInt64 mTotNumSchedCalls;
    UInt64 mCheckpointFreq;
    
    // Array of record nets that are also clocks (non state output).
    RecordNetVec mRecordClocks;
    // Array of record nets that are also state outputs.
    RecordNetVec mRecordStateOutputs;

    StimuliTouched mStimuliTouched;
    TouchedContainer mResponseTouchedContainer;
    
    ReplayRecordBuffer* mRecordInputBuffer;
    ReplayRecordBuffer* mRecordResponseBuffer;

    ReplayChangeCallback* mInputValueChangeCallback;
    ReplayChangeCallback* mResponseValueChangeCallback;

    const UtString* mDBName;
    RecResponseVec mRecordResponseNets;

    typedef UtArray<ShellNetRecordMem*> RecordMemVec;
    // Array of all (storage) externally-accessible memory nets
    RecordMemVec mExternalRecordMems;

    typedef UtArray<InternalMemWriteInfo*> MemWriteInfoVec;

    // Currently a subset of mExternalRecordMems, these are the
    // observable memories that need to have their internal memory
    // writes recorded along with other needed info to perform the
    // record.
    MemWriteInfoVec mInternalMemWriteInfos;

    // A vector of sets of SInt32s
    typedef UtArray<SInt32HashSet*> SInt32HashSetVec;
    /* The sets of address changes. No order is kept during
       allocation. These are created when creating the
       ShellNetRecordMems, and those keep the pointers to the hash
       sets. This vector is mainly for memory management and
       unordered, unassociated memory iterations.
    */
    SInt32HashSetVec mAddrChangeSets;

    // boolean that tells us if recordScheduleCall has not been called
    /*
      We cannot use mTotNumScheduleCalls because that doesn't get
      incremented when carbonInitialize is called. This is used to
      write out the record buffers at the beginning of the record
      session.
    */
    bool mRecordedScheduleCall;

    void removeMemoryCallbacks()
    {
      for (MemWriteInfoVec::iterator p = mInternalMemWriteInfos.begin(),
             e = mInternalMemWriteInfos.end(); p != e; ++p)
      {
        InternalMemWriteInfo* memInfo = *p;
        ShellNet* memNet = memInfo->mCallback->mMemId->castMemToShellNet();
        CarbonModelMemory* modelMem = memNet->castModelMemory();
        ST_ASSERT(modelMem, memNet->getName());
        ShellMemoryCBManager* cbmanager = modelMem->getShellMemoryCBManager();
        // being extra careful here. The cbmanager should never be
        // null in replay, but we are removing the callbacks, and
        // there is no reason to crash here.
        if (cbmanager)
          cbmanager->remove(memInfo->mCallback);
      }
    }

    // Records internal memory writes
    void recordInternalMemWrites()
    {
      // Run through the internal memories that were written
      UInt32 numElems = mInternalMemWriteInfos.size();
      for (UInt32 i = 0; i < numElems; ++i)
      {
        InternalMemWriteInfo* memWriteInfo = mInternalMemWriteInfos[i];
        recordMemStim(scInternalMemWrite, i, memWriteInfo->mMem, &memWriteInfo->mAddrChanges);
        memWriteInfo->clearChanges();
      }
    }

    // Records external memory writes
    void recordExternalMemWrites()
    {
      // Run through the external memories that were written
      UInt32 numElems = mExternalRecordMems.size();
      for (UInt32 i = 0; i < numElems; ++i)
      {
        ShellNetRecordMem* recordMem = mExternalRecordMems[i];
        const SInt32HashSet* addrChanges = recordMem->getAddressChanges();
        recordMemStim(scExternalMemWrite, i, recordMem, addrChanges);
        // Clear the address changes
        recordMem->clearChanges();
      }
    }

    void recordMemStim(const char memWriteType, 
                       UInt32 writeIndex, 
                       ShellNetRecordMem* recordMem,
                       const SInt32HashSet* addrChanges)
    {
      if (! addrChanges->empty())
      {
        // Format: MemWriteToken array_index num_changes addr val addr...
        UInt32 numAddrChanges = addrChanges->size();
        mRecordStream << eMemWrite << memWriteType <<
          writeIndex << numAddrChanges;
        UInt32 numWords = recordMem->getRecordNumWords();
        
        // Loop through the written addresses and record the values
        for (SInt32HashSet::SortedLoop q = addrChanges->loopSorted();
             ! q.atEnd(); ++q)
        {
          SInt32 addr = *q;
          mRecordStream << addr;
          const UInt32* value = recordMem->fetchValue(addr);
          recordWordArray(value, numWords);
        }
      }
    }

    // Update the response nets
    void updateResponse()
    {
      // Update the values of each response net from the Carbon model
      for (ResponseIter p = loopResponseNets();
           ! p.atEnd(); ++p)
      {
        ShellNetRecordResponse* net = *p;
        net->updateBuffer(mCarbonModel);
      }
    }


    /*
      Record the value buffers (input/response). This records a tag, followed by
      offset/value pairs.
      
      Note that it only writes the tag if there is at least once
      change in the buffer. The callback handles all of that.
    */
    void recordValueBuffers(ReplayRecordBuffer* replayBuffer,
                            ReplayChangeCallback* callback,
                            Tag noChangeTerminator)
    {
      replayBuffer->processChanges(! mRecordedScheduleCall, callback);
      callback->writeTerminator(noChangeTerminator);
    }

    /*
      Low-level record of a uint32 array. Note that the size of the
      array must be implicitly known by the reader.
    */
    void recordWordArray(const UInt32* arr, UInt32 numWords)
    {
      for (UInt32 i = 0; i < numWords; ++i)
        mRecordStream << arr[i];
    }
  };

  // forward decl
  class ReplayCModelCall;

  //! Type for an array of ReplayCModelCalls
  typedef UtArray<ReplayCModelCall*> ReplayCModelCallVec;
  
  //! Object representing a user's cmodel 
  /*!
    These get recorded in the replay database and will be played back.
  */
  class ReplayCModel
  {
  public:
    CARBONMEM_OVERRIDES

    ReplayCModel(void* cmodelData, void* userData, const char* name, UInt32 cmodelId);

    ~ReplayCModel();
    
    //! Add a cmodel call context to the this cmodel
    void addContext(UInt32 contextId, UInt32 numInputs,
                    UInt32 numOutputs, CModelPlaybackFn fn);

    //! Record the outputs of the cmodel before calling the run function
    void preRecordCall(EventDBWriter& dbWriter, UInt32 contextId, UInt32* outputs);

    //! Record a call to this cmodel. Records the context as well
    void recordCall(EventDBWriter& dbWriter, UInt32 contextId, UInt32* inputs, UInt32* outputs);

    //! Returns this cmodels id
    UInt32 getId() const { return mCModelId; }

    //! Returns the full name of this cmodel
    const char* getName() const { return mName.c_str(); }

    //! Returns the cmodel storage
    void* getStorage() { return mCModelStorage; }

    //! Returns the cmodel user data
    void* getUserData() { return mCModelUserData; }

    //! Number of contexts
    UInt32 numContexts() const { return mContexts.size(); }

    //! Get the context at index
    ReplayCModelCall* getContext(UInt32 index) { return mContexts[index]; }

    //! Clears output shadow buffers for all calls
    void clearOutputShadowBuffers();

  private:
    // Actual storage of cmodel
    void* mCModelStorage;
    // Actual user data for cmodel
    void* mCModelUserData;
    // Tag used for indexing into all known cmodels
    UInt32 mCModelId;
    // Name of cmodel
    UtString mName;
    // One cmodel can have multiple contexts.
    ReplayCModelCallVec mContexts;

    void maybeResizeContextArray(UInt32 contextId);
  };

  //! Object representing a cmodel call
  /*!
    One cmodel can have many interfaces that need to be interpreted
    differently, although they have the same
    signature. (Differently-sized in/out buffers).
  */
  class ReplayCModelCall
  {
  public:
    CARBONMEM_OVERRIDES
  
    //! This also contructs the ReplayRecordBuffers
    ReplayCModelCall(UInt32 id, UInt32 numInputWords, 
                     UInt32 numOutputWords, 
                     CModelPlaybackFn fn,
                     ReplayCModel* formalModel)
      : mContextId(id), mNumInputWords(numInputWords), 
        mNumOutputWords(numOutputWords),
        mNumCalls(0),
        mFn(fn), mFormalCModel(formalModel),
        mInputBuffer(numInputWords),
        mOutputBuffer(numOutputWords),
        mPreRunOutputBuffer(numOutputWords),
        mDummyWord(1), // set to one to allow processChanges to work
        mDummyTouched(&mDummyWord, 0)
    {
      UInt32* dummyValShadow;

      // NULL is passed for the mask pointer for the buffer
      // allocations below.  Cmodel inputs/outputs are never masked so
      // it's unnecessary.
      UInt32 valueIndex = 0;
      mInputBuffer.allocateValues(numInputWords, &valueIndex, &mInputValue,
                                  NULL, &dummyValShadow, NULL, &mDummyTouched, NULL);
      INFO_ASSERT(valueIndex == 0, "Cmodel Input RecordBuffer inconsistency");

      mOutputBuffer.allocateValues(numOutputWords, &valueIndex, &mOutputValue,
                                   NULL, &dummyValShadow, NULL, &mDummyTouched, NULL);
      INFO_ASSERT(valueIndex == 0, "Cmodel Output RecordBuffer inconsistency");

      mPreRunOutputBuffer.allocateValues(numOutputWords, &valueIndex, &mPreRunOutputValue,
                                         NULL, &dummyValShadow, NULL, &mDummyTouched, NULL);
      INFO_ASSERT(valueIndex == 0, "Cmodel Output RecordBuffer inconsistency");
    }
    
    //! Returns the CModel 
    const ReplayCModel* getFormal() const { return mFormalCModel; }
    
    //! Plays the cmodel call with the current values of the i/o 
    /*!
      Returns false if the output array mismatches the stored value
      in the mOutputBuffer shadow.
    */
    bool play()
    {
      // Initialize the output value to the pre-call value.
      ::memcpy(mOutputValue, mPreRunOutputValue, mNumOutputWords * sizeof(UInt32));
      (*mFn)(mFormalCModel->getUserData(), mInputValue, mOutputValue);
      UInt32* savedOuts = mOutputBuffer.getShadowBuffer();
      ++mNumCalls;
#if REPLAY_CMODEL_VERBOSE
      UtIO::cout() << "Id: " << mFormalCModel->getId() << UtIO::endl;
      UtIO::cout() << "\tContext: " << mContextId << UtIO::endl;
      UtIO::cout() << "\tCall: " << mNumCalls << UtIO::endl;
      UtIO::cout() << "\tIn:\t ";
      mInputBuffer.printValues();
      UtIO::cout() << "\tOut:\t ";
      mOutputBuffer.printValues();
      UtIO::cout() << "\tSOut:\t";
      mOutputBuffer.printShadows();
      UtIO::cout().flush();
#endif
      return memcmp(mOutputValue, savedOuts, mNumOutputWords * sizeof (UInt32)) != 0;
    }
    

    //! Records the pre-run outputs of a context call directly to the database.
    /*!
      This gets called before calling the cmodel run function. We need
      to record the initial values of the cmodel prior to
      modification. 
      This also starts the cmodel call recording process, so this must
      be called.
    */
    void preRecordCall(EventDBWriter& dbWriter, UInt32* outputs)
    { 
      // I'd like to remove the memcpy here. I could make another
      // processChanges like call which takes a buffer instead of
      // using the buffer that is allocated by the
      // ReplayRecordBuffer. 
      ::memcpy(mPreRunOutputValue, outputs, mNumOutputWords * sizeof(UInt32));
      
      dbWriter << mContextId;
      ReplayChangeCallback outChangeCB(dbWriter, eCModelPreOutput);
      mPreRunOutputBuffer.processChanges(mNumCalls == 0, &outChangeCB);
      outChangeCB.writeTerminator();
    }
    
    //! Records a context call directly into the database.
    void recordCall(EventDBWriter& dbWriter, UInt32* inputs, UInt32* outputs)
    {
      // I'd like to remove the memcpy here. I could make another
      // processChanges like call which takes a buffer instead of
      // using the buffer that is allocated by the
      // ReplayRecordBuffer. Maybe an option at construction to not
      // construct the mValues and mTouched storage as well.
      ::memcpy(mInputValue, inputs, mNumInputWords * sizeof(UInt32));
      ::memcpy(mOutputValue, outputs, mNumOutputWords * sizeof(UInt32));
      
      ReplayChangeCallback inChangeCB(dbWriter, eCModelInput);
      mInputBuffer.processChanges(mNumCalls == 0, &inChangeCB);
      inChangeCB.writeTerminator();

      ReplayChangeCallback outChangeCB(dbWriter, eCModelOutput);
      mOutputBuffer.processChanges(mNumCalls == 0, &outChangeCB);
      outChangeCB.writeTerminator();
      ++mNumCalls;
#if REPLAY_CMODEL_VERBOSE
      UtIO::cout() << "\tContext: " << mContextId << UtIO::endl;
      UtIO::cout() << "\tCall: " << mNumCalls << UtIO::endl;
      // inputs
      UtIO::cout() << "\tIn:\t ";
      mInputBuffer.printValues();
      // pre-out
      UtIO::cout() << "\tPOut:\t";
      mPreRunOutputBuffer.printValues();
      // post-out
      UtIO::cout() << "\tOut:\t";
      mOutputBuffer.printValues();
      
      UtIO::cout().flush();
#endif
    }

    //! Clears the output shadow buffer
    void clearOutputShadowBuffer()
    {
      memset(getOutputShadowBuffer(), 0, numOutputWords() * sizeof(UInt32));
    }

    //! The cmodel stimulus
    UInt32* getInputValueBuffer() { return mInputValue; }

    //! The result of the cmodel call
    UInt32* getOutputValueBuffer() { return mOutputValue; }

    //! Return the number of output words
    UInt32 numOutputWords() const { return mNumOutputWords; }

    /*!
      During record, the last cmodel output values.
      During playback, the expected cmodel output values.
    */
    UInt32* getOutputShadowBuffer() 
    {
      return mOutputBuffer.getShadowBuffer();
    }

    //! The value of the cmodel outputs prior to calling run
    UInt32* getPreRunOutputValueBuffer() 
    {
      return mPreRunOutputBuffer.getValueBuffer();
    }

    //! Returns the context id of the cmodel call
    UInt32 getContextId() const { return mContextId; }

  private:
    // Cmodel context id
    UInt32 mContextId;
    // Number of input words for this context
    UInt32 mNumInputWords;
    // Number of output words for this context
    UInt32 mNumOutputWords;
    // Number of calls so far in this session
    UInt32 mNumCalls;
    // Playback function
    CModelPlaybackFn mFn;
    // The cmodel on which this call is being executed
    ReplayCModel* mFormalCModel;
    
    // The input buffer (response for replay)
    ReplayRecordBuffer mInputBuffer;
    // The output buffer (stimulus for replay)
    ReplayRecordBuffer mOutputBuffer;
    // A buffer for the pre-run output values.
    ReplayRecordBuffer mPreRunOutputBuffer;

    // The pointer to the input buffer storage
    UInt32* mInputValue;
    // The pointer to the output buffer storage
    UInt32* mOutputValue;
    // The pointer to the pre-run output buffer storage
    UInt32* mPreRunOutputValue;

    // dummy variables to complete the record buffer (touched
    // references)
    UInt32 mDummyWord;
    ShellNetReplay::Touched mDummyTouched;
  };
}

#endif
