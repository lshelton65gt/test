// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONMEMFILE_H_
#define __CARBONMEMFILE_H_


#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif

class CarbonModel;

//! Class to wrap the parsing and reading of a generic memory dat file
class CarbonMemFile
{
public: CARBONMEM_OVERRIDES
  //! Constructor
  CarbonMemFile(CarbonModel* context, const char* fileName, CarbonRadix format, UInt32 rowWidth, int decreasingAddresses);
  //! Destructor
  ~CarbonMemFile();

  //! Parse the data. Returns error if any error happened.
  CarbonStatus load();

  //! Get the first and last addresses.
  /*!
    Returns eCarbon_ERROR if the memfile hasn't been loaded.
  */
  CarbonStatus getFirstAndLastAddrs(SInt64* firstAddress, SInt64* lastAddress) const;

  //! Return the row at address. NULL if memfile isn't loaded.
  const UInt32* getRow(SInt64 address) const;

  //! Populate an array with row data. 
  /*!
    See carbon_capi.h:carbonMemFilePopulateArray() for details.
  */
  CarbonStatus populateArray(UInt32* theArray, UInt32 numArrayWords, 
                             UInt32 rowBitIndex, UInt32 numBits);

private:
  class Helper;
  Helper* mHelper;
};

#endif
