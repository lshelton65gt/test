// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __carbon_abstract_register_h_
#define __carbon_abstract_register_h_

/*!
  \file 
  File containing the API for the Carbon AbstractRegister manipulation api.
*/


#ifdef __cplusplus
extern "C" {
#endif

#ifndef __carbon_shelltypes_h_
#include "carbon/carbon_shelltypes.h"
#endif

  /*!
    \brief Carbon Abstract Register structure
    
    The CarbonAbstractRegisterID provides the context for accessing abstract
    registers.
  */
typedef struct CarbonAbstractRegister *CarbonAbstractRegisterID;

  /*!
    \brief Create Carbon AbstractRegister Context
    
    \param srcStart The starting bit position from the src array
    to bein extraction from.

    \param numBits The number of bits to extract beginning at srcStart

    \param dstStart The starting bit position in the dst array to begin
    insertion of numBits starting from srcStart
  */
CarbonAbstractRegisterID carbonAbstractRegisterCreate(CarbonUInt32 srcStart, CarbonUInt32 numBits, CarbonUInt32 dstStart);

  /*!
    \brief Frees the memory allocated via carbonAbstractRegisterCreate
  */
void carbonAbstractRegisterDestroy(CarbonAbstractRegisterID ctx);

  /*!
    \brief Extract and transfer bits from src to dst
    
    \param src The array of CarbonUInt32s used to source the bits

    \param dst The destination array of CarbonUInt32s to receive
    the extracted bits.
  */
void CarbonAbstractRegisterXfer(CarbonAbstractRegisterID ctx, const CarbonUInt32* src, CarbonUInt32* dst);

#ifdef __cplusplus
}
#endif

#endif


