// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonNetValueCBData_H_
#define __CarbonNetValueCBData_H_

#include "shell/ShellNet.h"

//! Callback data class for callbacks related to nets
class CarbonNetValueCBData
{
public: 
  CARBONMEM_OVERRIDES

  //! Constructor - internally created
  CarbonNetValueCBData(CarbonNetValueCB fn, CarbonClientData data,
                       ShellNet* net, ShellNet::ExamineMode examineMode);
  //! destructor
  ~CarbonNetValueCBData();
  
  //! User function
  CarbonNetValueCB mUserFn;

  //! User data
  CarbonClientData mUserData;
  
  const ShellNet* getShellNet() const { return mNet; }
  
  ShellNet* getShellNet() { 
    const CarbonNetValueCBData* me = const_cast<CarbonNetValueCBData*>(this);
    return const_cast<ShellNet*>(me->getShellNet());
  }

  void putIsSampleScheduled(bool isSampleSched) { 
    mIsSampleSched = isSampleSched;
  }

  inline bool isSampleScheduled() const { return mIsSampleSched; }

  inline bool isEnabled() const { return mEnabled; }
  
  inline void disable() { mEnabled = false; }
  inline void enable() { mEnabled = true; }

  inline ShellNet::ExamineMode getExamineMode() { return mExamineMode; }

  inline void executeCB(CarbonObjectID* closure, UInt32* val, UInt32* drv)
  {
#ifdef PROFILE_USER_CALLBACKS
    SPRecorder r(model);
#endif
    (*mUserFn)(closure, mNet, mUserData, val, drv);
  }

private:
  ShellNet* mNet;
  ShellNet::ExamineMode mExamineMode;
  bool mEnabled;
  bool mIsSampleSched;
};

//! deprecated cb data structure
class CarbonNetCBData
{
public: CARBONMEM_OVERRIDES
#ifndef CARBON_EXTERNAL_DOC
  //! Constructor - internally created
  CarbonNetCBData(CarbonNetCB fn, CarbonClientData data);
  //! destructor
  ~CarbonNetCBData();
#endif
  
  //! User function
  CarbonNetCB mUserFn;
  
  CarbonNetValueCBData* mNewLayer;
  
  CarbonClientData mUserData;
  
  inline void executeCB(CarbonModel* model)
  {
#ifdef PROFILE_USER_CALLBACKS
    SPRecorder r(model);
#endif
    (*mUserFn)(model->getObjectID (), mNewLayer->getShellNet(), mUserData);
  }
};
#endif
