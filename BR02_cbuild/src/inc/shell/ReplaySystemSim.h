// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __REPLAYSYSTEMSIM_H_
#define __REPLAYSYSTEMSIM_H_

#include "shell/ReplaySystem.h"
#include "util/Stats.h"

class CarbonSystemComponentIter
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonSystemComponentIter(CarbonReplaySystem::ComponentLoop loop) :
    mLoop(loop)
  {}

  //! Get the next node or NULL if we are done
  CarbonSystemComponent* getNext();

private:
  //! The iterator created by CarbonReplaySystem::loopComponents()
  CarbonReplaySystem::ComponentLoop mLoop;
};

class CarbonSystemSim : public CarbonReplaySystem {
public:
  CARBONMEM_OVERRIDES

  //! ctor
  CarbonSystemSim();

  //! dtor
  virtual ~CarbonSystemSim();

  CarbonSystemComponent* addComponent(const char* component_name,
                                      CarbonObjectID** model,
                                      void* userData = NULL);

  CarbonSystemComponent* findComponent(const char* component_name);
  
  static void component_changed(CarbonObjectID* model,
                                void* userContext,
                                CarbonVHMMode,
                                CarbonVHMMode changingTo);


  static void checkpoint_cb(CarbonObjectID* model,
                            void* userContext,
                            CarbonUInt64 totalScheduleCalls,
                            CarbonUInt32 checkpointNumber);

  static void ondemand_mode_changed(CarbonObjectID* context,
                                    void* userContext,
                                    CarbonOnDemandMode from,
                                    CarbonOnDemandMode to,
                                    CarbonTime simTime);

#if REMOVE_COMPONENT_SUPPORT
  //! remove a ReplayInfo* from the existing system
  virtual void removeComponent(const char* component_name);
#endif

  //! Look up component name from object, return NULL if none found
  const char* findComponentName(CarbonObjectID*);

  //! Look up Carbon Model from component name, return NULL if none found
  CarbonObjectID* findModel(const char* component);

  //! Get a singleton instance of a CarbonReplaySystem
  /*!
   *! I normally don't like these sorts of APIs, but this is the only
   *! way I can think of to associate all components of a system
   *! simulation together.  E.g. maxsim components do not know anything
   *! about each other.
   *!
   *! Note that while this particular call requires a static variable,
   *! and is therefore non-retrant, the rest of the system is designed
   *! to be re-entrant.
   */
  static CarbonSystemSim* singleton(bool* firstCall);

  //! Establish the name of the database
  /*!
   *! This database is a directory name, in which we will
   *! store the databases of each system model, according to
   *! their instance name.
   */
  virtual void putDatabaseDirectory(const char* dir);

  //! put the checkpoint interval
  virtual void putCheckpointInterval(UInt32 recoverPercentage, 
                                     UInt32 num_min_calls);

  virtual void resetSimulation();

  //! Returns the number of currently replayable components
  int numReplayableComponents(void) const { return mNumReplayableComponents; }

  //! record a system cycle (does not need to be done every cycle)
  //void addCycle(UInt64 cycleNumber);

  //! record cpu statistics for the entire system
  void updateSystem(CarbonReplayEventType type);

  //! record cpu statistics for a single component
  void updateComponent(CarbonSystemComponent*, CarbonReplayEventType type);

  //! put the cycle-count callback fn
  void putCycleCountCB(CarbonSystemCycleCountFn fn,
                       void *clientData);

  //! put the cycle-count callback fn
  void putSimTimeCB(CarbonSystemSimTimeFn fn,
                    void *clientData);

  //! Remove the cycle-count callback
  /*!
   *! When a simulation is being terminated, no more cycles will
   *! occur, so there is no need to call the callback anymore.  Note
   *! that we will still consider this a "cycle-counted" simulation,
   *! and not a "schedule-call-counted" simulation.
   *!
   *! It's necesary to do this to avoid calling the callback using data
   *! that is no longer valid memory
   */
  void clearCycleCountCB() {putCycleCountCB(0, 0);}

  //! Remove the sim-time callback
  /*!
   *! When a simulation is being terminated, no more simulatin time will
   *! occur, so there is no need to call the callback anymore.  
   *!
   *! It's necesary to do this to avoid calling the callback using data
   *! that is no longer valid memory
   */
  void clearSimTimeCB() {putSimTimeCB(0, 0);}

protected:
  virtual void resetOnDemandFirstEventFlags();

private:
  void determineCompScheduleCalls(CarbonSystemComponent* comp);
  void updateTotalSchedCalls();
  UInt64 getCycles();
  double getSimTime();
  void printCycle(UInt64 schedCalls);
  void composeCycle(UInt64 schedCalls, UtString* cycleMsg);
  void updateStats(CarbonSystemComponent *comp, CarbonReplayEventType type, bool calc_schedule_calls);
  void initOnDemandTrace();

  typedef UtHashMap<CarbonObjectID*,CarbonSystemComponent*> ModelComponentMap;
  ModelComponentMap mModelComponentMap;

  Stats mStats;
  int mNumReplayableComponents;
  UInt64 mCycles;
  double mSimTime;
  CarbonSystemCycleCountFn mCycleCountFn;
  void* mCycleCountClientData;
  CarbonSystemSimTimeFn mSimTimeFn;
  void* mSimTimeClientData;
}; // struct CarbonSystemSim : public CarbonReplaySystem

#endif // __REPLAYSYSTEMSIM_H_
