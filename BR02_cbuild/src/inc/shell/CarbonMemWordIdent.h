// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef __CARBON_MEM_WORD_IDENT_H__
#define __CARBON_MEM_WORD_IDENT_H__

#include "exprsynth/Expr.h"
#include "shell/CarbonMemIdent.h"

//! CarbonIdent representing a particular word of a memory
/*!
  This class basically wraps a CarbonBinaryOp for a memory select of a
  CarbonMemIdent, and leverages that expression's ability to
  examine/deposit a single word of the memory.

  While the subexpression could have been avoided by interacting with
  the memory net directly, the implementation of assignRange() would
  have been more complicated.  It's much easier to use the
  AssignContext/EvalContext infrastructure directly rather than
  mucking with UInt32 buffers.
*/
class CarbonMemWordIdent : public CarbonIdent
{
public:
  CARBONMEM_OVERRIDES

  CarbonMemWordIdent(CarbonMemIdent *expr1, CarbonConst *expr2, UInt32 bitSize, bool isSigned, bool computeSigned);
  virtual ~CarbonMemWordIdent();

  virtual ptrdiff_t compare(const CarbonExpr* other) const;

  virtual const STAliasedLeafNode* getNode(DynBitVector* usageMask) const;

  virtual AssignStat assign(ExprAssignContext* context);

  virtual AssignStat assignRange(ExprAssignContext* context, 
                                 const ConstantRange& range);

  virtual void print(bool = false, int indent = 0) const;

  virtual SignT evaluate(ExprEvalContext* evalContext) const;
  
  virtual const char* typeStr() const;

  virtual void composeIdent(ComposeContext* context) const;

  const CarbonMemWordIdent* castCarbonMemWordIdent() const;

  //! Returns true if this ident represents the entire net
  virtual bool isWholeIdentifier() const;

  //! Gets the CarbonMemIdent this wraps
  CarbonMemIdent* getMemIdent() const;

protected:
  //! The binary op to implement the memory word select
  CarbonBinaryOp mBinOp;

  //! Gets the CarbonConst for the index of the mem select
  CarbonConst* getIndexExpr() const;
};

#endif
