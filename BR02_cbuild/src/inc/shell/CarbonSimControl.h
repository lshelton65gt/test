// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CarbonSimControl_h_
#define __CarbonSimControl_h_


#ifndef __carbon_shelltypes_h_
#include "shell/carbon_shelltypes.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h" // needed for CarbonOpaque
#endif
#ifndef __carbon_model_h_
#include "shell/carbon_model.h"
#endif
#ifndef __Util_h_
#include "util/Util.h"
#endif

//! A class used to define/maintain/call the callback functions associated with $stop and $finish
class ControlHelper
{
 public: CARBONMEM_OVERRIDES

  //! constructor
  ControlHelper(CarbonOpaque model_handle);

  //! destructor
  ~ControlHelper();

  //! Has a $finish been executed in this model?
  bool isFinished() const { return mIsFinished; }


  //! returns the current carbon status for the current run of the schedule
  CarbonStatus getCarbonStatus() const { return mCarbonStatus; }

  //! reset the carbonStatus for the current run of the schedule
  void resetCarbonStatus() { mCarbonStatus = eCarbon_OK; }

  //! call this when a stop is executed in the current model
  void updateCarbonStatusDueToStop();
  //! call this when a finish is executed in the current model
  void updateCarbonStatusDueToFinish();
  //! call this when an error condition has been detected in the current model
  void updateCarbonStatusDueToError();

  //! do the runtime operations necessary for the \a callbackType controlSysTask
  // this is what gets called by the emited code for $stop and $finish
  void runControlSysTask( CarbonOpaque const subModule,
                          UInt32 verbosity,
                          CarbonControlType callbackType,
                          const char* verilogFilename,
                          int verilogLineNumber);

  //! here is a default callback function for control tasks 
  /*! this is not intended for users, it will automatically be
   * installed if no user defined callback has been defined 
   */ 
  static void controlDefaultCBFunction (CarbonObjectID * /* carbonObject --unused*/,
                                        CarbonControlType /* callbackType * --unused*/,
                                        CarbonClientData /* data --unused */,
                                        const char* verilogFilename,
                                        int verilogLineNumber);




  /*! \brief Add a callback function for the {\a carbonModel, \a controlType} pair
   *
   * \param fn Function that will be added to the call list
   * \param carbonModel the Model in which the function was registered
   * \param userData Data to pass to the function
   * \param controlType  type of control function this callback is applied to, ($stop or $finish).
   *
   * \returns A pointer to a class with all the supplied parameters. It is needed to remove a callback. 
   */
  static RegisteredControlCBData* adminAddControlCB(CarbonControlCBFunction fn,
                                                    CarbonModel* carbonModel,
                                                    CarbonClientData userData,
                                                    CarbonControlType controlType);


  /*! \brief Remove a previously registered control callback function
   *
   * This removes the registered callback from the system control that
   * it was previously registered with. It also invalidates the passed
   * in data structure.
   *
   * \param pcallbackHandle Pointer to a valid
   * RegisteredControlCBData object. The de-referenced pointer
   * (*pcallbackHandle) will be NULL when this method returns.
   * If this parameter or if the de-referenced object is NULL,
   * no action is taken.
   */
  static void adminRemoveControlCB(RegisteredControlCBData** pcallbackHandle);

 private:
  ControlHelper(const ControlHelper&);
  ControlHelper& operator=(const ControlHelper&);

  //! list of control callbacks, in the order they will be called,
  /*!
    $stop and $finish are combined in this list
    
    Removing a callback is an O(n) operation. But, we don't expect too
    many of these.
  */
  typedef UtArray<RegisteredControlCBData*> ControlCBList;
  typedef Loop<ControlCBList> ControlCBListLoop;
  ControlCBList * mControlCBs;

  //! call this when a $finish is executed
  void putIsFinished() { mIsFinished = true; }

  //! tracks whether this model has executed a $finish
  bool mIsFinished;

  //! keeps track of the status of the execution of the current schedule
  /*! there is a priority for values (high to low):
   *    eCarbon_ERROR eCarbon_FINISH eCarbon_STOP eCarbon_OK
   * the only way to get to eCarbon_OK is with a call to
   *  resetCarbonStatus
   *
   * \sa updateCarbonStatusDueToError, updateCarbonStatusDueToFinish,
   *  updateCarbonStatusDueToStop, resetCarbonStatus
   */
  CarbonStatus mCarbonStatus;

  /*! \brief this holds the handle for the default stop callback
   * function.
   *
   * if null then the default has been removed
  */
  RegisteredControlCBData* mDefaultStop;
  /*! \brief this holds the handle for the default finish callback
   * function.
   *
   * if null then the default has been removed
  */
  RegisteredControlCBData* mDefaultFinish;
};

//! data class for callbacks related to Verilog control task, for internal use
class RegisteredControlCBData
{
public: CARBONMEM_OVERRIDES
  //! Constructor - internally created
  RegisteredControlCBData(CarbonControlCBFunction fn,
                          CarbonModel* carbonObject,
                          CarbonClientData data,
                          CarbonControlType type)
    : mUserFn(fn),  mUserData(data), mCarbonModel(carbonObject), mCallbackType(type)
  {}

  //! destructor
  ~RegisteredControlCBData()
  {}

  //! User function
  CarbonControlCBFunction mUserFn;

  //! User data
  CarbonClientData mUserData;


  //! get the type of control function this was defined for
  CarbonControlType getControlCallbackType() const {return mCallbackType;}

  //! get the Carbon Model that was associated with the callback function when it was registered
  CarbonModel* getCarbonModel() const {return mCarbonModel;}

private:
  RegisteredControlCBData(const RegisteredControlCBData&);
  RegisteredControlCBData& operator=(const RegisteredControlCBData&);

  CarbonModel* mCarbonModel;
  CarbonControlType mCallbackType;
};


#endif  // __CarbonSimControl_h_
