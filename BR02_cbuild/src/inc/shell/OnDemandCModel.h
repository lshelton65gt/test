// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __ON_DEMAND_CMODEL_H__
#define __ON_DEMAND_CMODEL_H__

#include "util/UtArray.h"
#include "util/UtHashMap.h"
#include "shell/CarbonModel.h"
#include "shell/carbon_model.h"

// ***NOTE***
// There is some overlap in functionality between these classes and
// those used for replay.  This should be addressed in the future.

class OnDemandDebugInfo;

//! Class for a cmodel call context
/*!
  This class represents a particular context of a cmodel.  A context
  is the enumerated value passed to the cmodel's run() function that
  indicates the reason it's being called.  A pointer to a generated
  function, along with the cmodel's user data is stored.  This
  generated function takes a packed array of input and output values
  and calls the true cmodel run() function appropriately.

  Given a pointer to an array of input and output values, this class
  can run the actual cmodel and report whether there was an output
  divergence.
 */
class OnDemandCModelContext
{
public:
  CARBONMEM_OVERRIDES

  friend class OnDemandCModelFactory;

  //! Runs this context with the specified input/output data, returning success/failure
  bool run(CarbonUInt32 *inputs, CarbonUInt32 *outputs, CarbonUInt32 *prev_outputs);

  CarbonUInt32 getInputWords() { return mInputWords; }
  CarbonUInt32 getOutputWords() { return mOutputWords; }
  CarbonUInt32 getInputOffset() { return mInputOffset; }
  CarbonUInt32 getOutputOffset() { return mOutputOffset; }

  //! Initializes debug mode info
  void initDebug();

  OnDemandDebugInfo *getDebugInfo() { return mDebugInfo; }

protected:
  void *mUserData;                              //!< Pointer to the cmodel's user data
  CarbonUInt32 mContext;                        //!< Enumerated context value
  UtString mName;                               //!< Name associated with this cmodel instance
  CarbonUInt32 mInputWords;                     //!< Number of input words needed for this context
  CarbonUInt32 mOutputWords;                    //!< Number of output words needed for this context
  CarbonUInt32 mInputOffset;                    //!< Offset into value array for this context's input values
  CarbonUInt32 mOutputOffset;                   //!< Offset into value array for this context's output values
  CModelPlaybackFn mPlayFn;             //!< The function that will run the cmodel with saved inputs/outputs
  CarbonUInt32 *mOutputBuffer;                  //!< Temporary output buffer for cmodel to write
  OnDemandDebugInfo *mDebugInfo;                //!< Optional debug mode information
  
private:
  OnDemandCModelContext(void *userdata, CarbonUInt32 context, const char *name,
                        CarbonUInt32 input_words, CarbonUInt32 output_words,
                        CarbonUInt32 input_offset, CarbonUInt32 output_offset,
                        CModelPlaybackFn fn);
  ~OnDemandCModelContext();

  //! Hide
  OnDemandCModelContext(const OnDemandCModelContext&);
  OnDemandCModelContext &operator=(const OnDemandCModelContext&);
};

//! Class to track an instance of a cmodel call, including input/output values
/*!
  This class represents a particular instance of a cmodel context
  being called.  It's actually a pretty thin class.  It only has a
  pointer to the OnDemandCModelContext object, as well as pointers to
  the inputs, previous outputs, and expected outputs for the call.

  Note that the input/output storage is not allocated here.  The
  storage for all registered cmodels is allocated when the
  OnDemandCModelCallCollection is created.
 */
class OnDemandCModelCall
{
public:
  CARBONMEM_OVERRIDES

  friend class OnDemandCModelFactory;

  //! Runs this call with the saved input/output data, returning success/failure
  bool run();

  OnDemandCModelContext *getContext() { return mContext; }
  CarbonUInt32 *getOutputBuffer() { return mOutputs; }

protected:
  OnDemandCModelContext *mContext;              //!< Context information for this call
  CarbonUInt32 *mInputs;                        //!< Pointer into to the common cmodel input buffer for this call
  CarbonUInt32 *mOutputs;                       //!< Pointer into to the common cmodel output buffer for this call
  CarbonUInt32 *mPrevOutputs;                   //!< Pointer into to the common cmodel previous output buffer for this call

private:
  OnDemandCModelCall(OnDemandCModelContext *context, CarbonUInt32 *inputs, CarbonUInt32 *outputs, CarbonUInt32 *prev_outputs);
  ~OnDemandCModelCall();

  //! Hide
  OnDemandCModelCall(const OnDemandCModelCall&);
  OnDemandCModelCall &operator=(const OnDemandCModelCall&);
};


//! Class to track all cmodel calls for a single schedule call
/*!
  This class contains all the cmodel calls made during a particular
  call to carbonSchedule().

  Upon creation, a storage buffer for the maximum number of input,
  output, and previous output words is created.  This means that each
  cmodel context can have a fixed offset into each of these buffers,
  regardless of how many or which cmodels were actually called during
  a schedule call.  When a call is added to the collection, the
  context is queried for its offsets, and the appropriate buffer
  pointers are passed to the call.
 */
class OnDemandCModelCallCollection
{
public:
  CARBONMEM_OVERRIDES

  friend class OnDemandCModelFactory;

  //! Adds a call to this collection
  void addCall(OnDemandCModelCall *call);

  //! Is this collection empty?
  bool isEmpty() { return (mNumCalls == 0); }

  //! Runs all the calls in this collection, returning status
  bool run();

  //! Returns the status of a cmodel call in this collection
  CarbonModel::CModelRecoveryStatus getCallStatus(OnDemandCModelContext *context, CarbonUInt32 **outputs);

  //! Returns a pointer to the input buffer location at an index
  CarbonUInt32 *getInputBufPtr(CarbonUInt32 index) { return &mInputs[index]; }
  //! Returns a pointer to the output buffer location at an index
  CarbonUInt32 *getOutputBufPtr(CarbonUInt32 index) { return &mOutputs[index]; }
  //! Returns a pointer to the previous output buffer location at an index
  CarbonUInt32 *getPrevOutputBufPtr(CarbonUInt32 index) { return &mPrevOutputs[index]; }

  //! Returns the last-run context, i.e. the one that caused a divergence
  OnDemandCModelContext *getLastRunContext();

protected:
  typedef UtArray<OnDemandCModelCall *> CallArray;
  CallArray mCalls;                             //!< Array of all calls made
  CarbonUInt32 mNumCalls;                       //!< Number of calls
  CarbonUInt32 *mInputs;                        //!< Common input value buffer for all calls
  CarbonUInt32 *mOutputs;                       //!< Common output value buffer for all calls
  CarbonUInt32 *mPrevOutputs;                   //!< Common previous output value buffer for all calls
  CarbonUInt32 mInputWords;                     //!< Number of valid input words in the buffer
  CarbonUInt32 mOutputWords;                    //!< Number of valid input words in the buffer
  const CarbonUInt32 mMaxInputWords;            //!< Maximum size of the input buffer
  const CarbonUInt32 mMaxOutputWords;           //!< Maximum size of the output buffer
  CarbonUInt32 mLastCModelRun;                  //!< Index of the last cmodel run on a schedule call

private:
  OnDemandCModelCallCollection(CarbonUInt32 max_input_words, CarbonUInt32 max_output_words);
  ~OnDemandCModelCallCollection();

  //! Hide
  OnDemandCModelCallCollection(const OnDemandCModelCallCollection&);
  OnDemandCModelCallCollection &operator=(const OnDemandCModelCallCollection&);
};


// Class to allocate/free onDemand cmodel objects and manage common data
/*!
  All the onDemand cmodel classes are allocated through the factory.
  Since OnDemandCModelCallCollection contains several allocated
  OnDemandCModelCalls, the factory has a copy method to do a deep copy
  of a collection while maintaining ownership of the storage.

  The factory also maintains a map of all the known cmodels in the
  design.  The key is the cmodel object's address, and the value is an
  array of contexts for that cmodel.  The array is indexed by the
  generated enum associated for that context.  Cmodels are added to
  the map when they are registered as part of Carbon Model initialization.

  The factory's clear() method deallocates all objects that are needed
  only for a particular idle sequence.  This includes all cmodel
  collections and the calls they contain.  The contexts are not
  deallocated, since they are created at model initialization time and
  need to persist for the life of the Carbon Model.  The clearAll() method
  deallocates all objects, including registered contexts and the
  context map.
 */
class OnDemandCModelFactory
{
public:
  CARBONMEM_OVERRIDES

  OnDemandCModelFactory();
  ~OnDemandCModelFactory();

  //! Adds a context
  void addContext(void *cmodel, void *userdata,
                  CarbonUInt32 context_id, const char *name,
                  CarbonUInt32 input_words, CarbonUInt32 output_words,
                  CModelPlaybackFn fn);

  //! Creates a call instance for a context, also recording the previous output values
  void addCallToCollection(void *cmodel, CarbonUInt32 context_id, 
                           CarbonUInt32 *outputs,
                           OnDemandCModelCallCollection *collection);

  //! Records the inputs and resulting output values from a call
  void saveCallResultToCollection(void *cmodel, CarbonUInt32 context_id, 
                                  CarbonUInt32 *inputs, CarbonUInt32 *outputs,
                                  OnDemandCModelCallCollection *collection);

  //! Instruments all cmodel contexts for debug mode
  void initDebug();

  //! Creates a collection of cmodel calls
  OnDemandCModelCallCollection *createCollection();

  //! Finds a cmodel context in the map
  OnDemandCModelContext *findContext(void *cmodel, CarbonUInt32 context_id);

  //! Frees all allocated objects
  void clear();

  //! Frees all allocated objects, including protected ones
  void clearAll();

protected:
  typedef UtArray<OnDemandCModelContext *> ContextArray;
  typedef UtHashMap<void *, ContextArray *> ContextMap;
  ContextMap mContextMap;                            //!< Map of all cmodel contexts
  OnDemandCModelCallCollection::CallArray mCalls;    //!< All allocated cmodel calls
  typedef UtArray<OnDemandCModelCallCollection *> CollectionArray;
  CollectionArray mCollections;                      //!< All allocated cmodel call collections
  CarbonUInt32 mMaxInputWords;                       //!< Maximum input words for all cmodels
  CarbonUInt32 mMaxOutputWords;                      //!< Maximum output words for all cmodels
};


#endif
