// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONTRISTATEVECTOR_H_
#define __CARBONTRISTATEVECTOR_H_


#ifndef __CARBONVECTORBASE_H_
#include "shell/CarbonVectorBase.h"
#endif

template <typename T> 
CarbonStatus doTristateFormatPrim(char* valueStr, size_t len, 
                                  CarbonRadix strFormat, 
                                  const T* val, 
                                  const T* xdrive, 
                                  const T* idrive,
                                  const T* forceMask,
                                  const UInt32* overrideMask,
                                  int bitWidth, bool pulled,
                                  CarbonModel* model)
{
  int numCharsConverted = -1;
  CarbonStatus stat = eCarbon_OK;
  switch(strFormat)
  {
  case eCarbonBin:
    numCharsConverted = CarbonValRW::writeBinXZValToStr(valueStr, len, val, xdrive, idrive, forceMask, overrideMask, pulled, bitWidth);
    break;
  case eCarbonHex:
    numCharsConverted = CarbonValRW::writeHexXZValToStr(valueStr, len, val, xdrive, idrive, forceMask, overrideMask, pulled, bitWidth);
    break;
  case eCarbonOct:
    numCharsConverted = CarbonValRW::writeOctXZValToStr(valueStr, len, val, xdrive, idrive, forceMask, overrideMask, pulled, bitWidth);
    break;
  case eCarbonDec:
    numCharsConverted = CarbonValRW::writeDecXZValToStr(valueStr, len, val, xdrive, idrive, forceMask, overrideMask, pulled, true, bitWidth);
    break;
  case eCarbonUDec:
    numCharsConverted = CarbonValRW::writeDecXZValToStr(valueStr, len, val, xdrive, idrive, forceMask, overrideMask, pulled, false, bitWidth);
    break;
  }
  
  if (numCharsConverted == -1)
  {
    ShellGlobal::reportInsufficientBufferLength(len, model);
    stat = eCarbon_ERROR;
  }
  return stat;
}


class ShellNetTristate1;
class ShellNetTristate2;
class ShellNetTristate4;
class ShellNetTristate8;
class ShellNetTristateA;

class CarbonTristateVector: public CarbonVectorBase
{
public: CARBONMEM_OVERRIDES
  CarbonTristateVector();
  virtual ~CarbonTristateVector();

  // This is a tristate
  virtual bool isTristate() const;

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

private:
  CarbonTristateVector(const CarbonTristateVector&);
  CarbonTristateVector& operator=(const CarbonTristateVector&);
};

class CarbonTristateVector1 : public CarbonTristateVector
{
public: CARBONMEM_OVERRIDES
  //! constructor for bidi vectors <= 8 bits
  CarbonTristateVector1(UInt8* idata, UInt8* idrive, 
                       UInt8* xdata, UInt8* xdrive);
  
  //! constructor for tristate vectors <= 8 bits
  CarbonTristateVector1(UInt8* idata, UInt8* idrive);
  
  virtual ~CarbonTristateVector1();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  virtual ShellNet::Storage allocShadow() const;
  
  virtual void freeShadow(ShellNet::Storage* shadow);

  //! ShellNet::setToDriven()
  virtual bool setToDriven(CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);

  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);

  //! ShellNet::setWordToUndriven()
  virtual bool setWordToUndriven(int index, CarbonModel* model);

  //! ShellNet::setRangeToUndriven()
  virtual bool setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model);

  //! ShellNet::setRawToUndriven()
  virtual void setRawToUndriven(CarbonModel* model);

  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);

  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;

  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  virtual void update(ShellNet::Storage* shadow) const;

  virtual void updateUnresolved(ShellNet::Storage* shadow) const;
  
  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive);

  virtual ShellNet::ValueState compare(const ShellNet::Storage shadow) const;

  virtual ShellNet::ValueState writeIfNotEq(char* valueStr, size_t len, ShellNet::Storage* shadow,
                                            NetFlags);
  
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat,
                              NetFlags, CarbonModel*) const;

  virtual bool isDataNonZero() const;

  virtual int hasDriveConflict() const;
  virtual int hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const;

  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask);
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel*) const;

protected:
  virtual void setConstantBits();

  bool assignValue(const UInt32* buf, const UInt32* drive); 
  bool assignValueWord(UInt32 val, UInt32 drv, int index);
  bool assignValueRange(const UInt32* buf, const UInt32* range, size_t index, size_t length); 

  inline void doFastDeposit(const UInt32* buf, const UInt32* drive,
                            CarbonModel* model);
  inline void doFastDepositWord(UInt32 buf, int index, UInt32 drive,
                                CarbonModel* model);
  inline CarbonStatus doDepositWord(UInt32 buf, int index, UInt32 drive,
                                    CarbonModel* model);
  inline CarbonStatus doFastDepositRange(const UInt32* buf,int range_msb,
                                         int range_lsb,const UInt32* drive,
                                         CarbonModel* model);

private:
  
  void examineValue(UInt32* val, UInt32* drv) const;
  void examineValueWord(UInt32* val, UInt32* drv, 
                                int index) const;
  void examineValueRange(UInt32* val, UInt32* drv, 
                                 size_t index, 
                                 size_t length) const;
  
  void calcValue(UInt32* val, UInt32* drv) const;
  void calcValuePrim(UInt8* val, UInt8* drv) const;
  void calcValueWord(UInt32* val, UInt32* drv, int index) const;

  void examineXVals(UInt32* val, UInt32* drv) const;

  ValueState compareValue(const UInt8* val, const UInt8* drv) const;
  void copyValAndXDriveWord(UInt32* val, UInt32* drv, 
                                    int index) const;

  void getValuePtrs(const UInt8** val, const UInt8** xdrv, const UInt8** idrv) const;

protected:
  ShellNetTristate1* mTri;
  
private:

  CarbonTristateVector1();
  CarbonTristateVector1(const CarbonTristateVector1&);
  CarbonTristateVector1& operator=(const CarbonTristateVector1&);
};

class CarbonTristateVector2 : public CarbonTristateVector
{
public: CARBONMEM_OVERRIDES
  //! constructor for bidi vectors <= 8 bits
  CarbonTristateVector2(UInt16* idata, UInt16* idrive, 
                       UInt16* xdata, UInt16* xdrive);
  
  //! constructor for tristate vectors <= 8 bits
  CarbonTristateVector2(UInt16* idata, UInt16* idrive);
  
  virtual ~CarbonTristateVector2();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  virtual ShellNet::Storage allocShadow() const;
  
  virtual void freeShadow(ShellNet::Storage* shadow);

  //! ShellNet::setToDriven()
  virtual bool setToDriven(CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);

  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);

  //! ShellNet::setWordToUndriven()
  virtual bool setWordToUndriven(int index, CarbonModel* model);

  //! ShellNet::setRangeToUndriven()
  virtual bool setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model);

  //! ShellNet::setRawToUndriven()
  virtual void setRawToUndriven(CarbonModel* model);

  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);

  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;

  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  virtual void update(ShellNet::Storage* shadow) const;
  
  virtual void updateUnresolved(ShellNet::Storage* shadow) const;
  
  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive);

  virtual ShellNet::ValueState compare(const ShellNet::Storage shadow) const;

  virtual ShellNet::ValueState writeIfNotEq(char* valueStr, size_t len, ShellNet::Storage* shadow,
                                            NetFlags);
  
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat,
                              NetFlags, CarbonModel*) const;

  virtual bool isDataNonZero() const;

  virtual int hasDriveConflict() const;
  virtual int hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const;

  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask);
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel*) const;

protected:
  virtual void setConstantBits();


  inline void doFastDeposit(const UInt32* buf, const UInt32* drive,
                            CarbonModel* model);
  inline void doFastDepositWord(UInt32 buf, int index, UInt32 drive,
                                CarbonModel* model);
  inline CarbonStatus doDepositWord(UInt32 buf, int index, UInt32 drive,
                                    CarbonModel* model);
  inline CarbonStatus doFastDepositRange(const UInt32* buf,int range_msb,
                                         int range_lsb,const UInt32* drive,
                                         CarbonModel* model);

  bool assignValue(const UInt32* buf, const UInt32* drive); 
  bool assignValueWord(UInt32 val, UInt32 drv, int index);
  bool assignValueRange(const UInt32* buf, const UInt32* range, size_t index, size_t length); 

private:
  
  void examineValue(UInt32* val, UInt32* drv) const;
  void examineValueWord(UInt32* val, UInt32* drv, 
                                int index) const;
  void examineValueRange(UInt32* val, UInt32* drv, 
                                 size_t index, 
                                 size_t length) const;
  
  void calcValue(UInt32* val, UInt32* drv) const;
  void calcValuePrim(UInt16* val, UInt16* drv) const;
  void calcValueWord(UInt32* val, UInt32* drv, int index) const;

  void examineXVals(UInt32* val, UInt32* drv) const;

  ValueState compareValue(const UInt16* val, const UInt16* drv) const;
  void copyValAndXDriveWord(UInt32* val, UInt32* drv, 
                                    int index) const;
  void getValuePtrs(const UInt16** val, const UInt16** xdrv, const UInt16** idrv) const;

protected:
  ShellNetTristate2* mTri;
  
private:

  CarbonTristateVector2();
  CarbonTristateVector2(const CarbonTristateVector2&);
  CarbonTristateVector2& operator=(const CarbonTristateVector2&);
};

class CarbonTristateVector4 : public CarbonTristateVector
{
public: CARBONMEM_OVERRIDES
  //! constructor for bidi vectors <= 8 bits
  CarbonTristateVector4(UInt32* idata, UInt32* idrive, 
                       UInt32* xdata, UInt32* xdrive);
  
  //! constructor for tristate vectors <= 8 bits
  CarbonTristateVector4(UInt32* idata, UInt32* idrive);
  
  virtual ~CarbonTristateVector4();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  virtual ShellNet::Storage allocShadow() const;
  
  virtual void freeShadow(ShellNet::Storage* shadow);

  //! ShellNet::setToDriven()
  virtual bool setToDriven(CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);

  //! ShellNet::setWordToUndriven()
  virtual bool setWordToUndriven(int index, CarbonModel* model);

  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);

  //! ShellNet::setRangeToUndriven()
  virtual bool setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model);

  //! ShellNet::setRawToUndriven()
  virtual void setRawToUndriven(CarbonModel* model);

  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);

  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;

  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  virtual void update(ShellNet::Storage* shadow) const;
  
  virtual void updateUnresolved(ShellNet::Storage* shadow) const;
  
  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive);

  virtual ShellNet::ValueState compare(const ShellNet::Storage shadow) const;

  virtual ShellNet::ValueState writeIfNotEq(char* valueStr, size_t len, ShellNet::Storage* shadow,
                                            NetFlags);
  
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat,
                              NetFlags, CarbonModel*) const;

  virtual bool isDataNonZero() const;

  virtual int hasDriveConflict() const;
  virtual int hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const;

  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask);
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel*) const;

protected:
  virtual void setConstantBits();

  inline void doFastDeposit(const UInt32* buf, const UInt32* drive,
                            CarbonModel* model);
  inline void doFastDepositWord(UInt32 buf, int index, UInt32 drive,
                                CarbonModel* model);
  inline CarbonStatus doDepositWord(UInt32 buf, int index, UInt32 drive,
                                    CarbonModel* model);
  inline CarbonStatus doFastDepositRange(const UInt32* buf,int range_msb,
                                         int range_lsb,const UInt32* drive,
                                         CarbonModel* model);

  bool assignValue(const UInt32* buf, const UInt32* drive); 
  bool assignValueWord(UInt32 val, UInt32 drv, int index);
  bool assignValueRange(const UInt32* buf, const UInt32* range, size_t index, size_t length); 

private:
  
  void examineValue(UInt32* val, UInt32* drv) const;
  void examineValueWord(UInt32* val, UInt32* drv, 
                                int index) const;
  void examineValueRange(UInt32* val, UInt32* drv, 
                                 size_t index, 
                                 size_t length) const;
  
  void calcValue(UInt32* val, UInt32* drv) const;
  void calcValuePrim(UInt32* val, UInt32* drv) const;
  void calcValueWord(UInt32* val, UInt32* drv, int index) const;

  void examineXVals(UInt32* val, UInt32* drv) const;

  ValueState compareValue(const UInt32* val, const UInt32* drv) const;
  void copyValAndXDriveWord(UInt32* val, UInt32* drv, 
                                    int index) const;
  void getValuePtrs(const UInt32** val, const UInt32** xdrv, const UInt32** idrv) const;

protected:
  ShellNetTristate4* mTri;
  
private:

  CarbonTristateVector4();
  CarbonTristateVector4(const CarbonTristateVector4&);
  CarbonTristateVector4& operator=(const CarbonTristateVector4&);
};

class CarbonTristateVector8 : public CarbonTristateVector
{
public: CARBONMEM_OVERRIDES
  //! constructor for bidi vectors <= 8 bits
  CarbonTristateVector8(UInt64* idata, UInt64* idrive, 
                       UInt64* xdata, UInt64* xdrive);
  
  //! constructor for tristate vectors <= 8 bits
  CarbonTristateVector8(UInt64* idata, UInt64* idrive);
  
  virtual ~CarbonTristateVector8();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  virtual ShellNet::Storage allocShadow() const;
  
  virtual void freeShadow(ShellNet::Storage* shadow);

  //! ShellNet::setToDriven()
  virtual bool setToDriven(CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);

  //! ShellNet::setWordToUndriven()
  virtual bool setWordToUndriven(int index, CarbonModel* model);

  //! ShellNet::setRangeToUndriven()
  virtual bool setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model);

  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);

  //! ShellNet::setRawToUndriven()
  virtual void setRawToUndriven(CarbonModel* model);

  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);

  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;

  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  virtual void update(ShellNet::Storage* shadow) const;
  
  virtual void updateUnresolved(ShellNet::Storage* shadow) const;
  
  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive);

  virtual ShellNet::ValueState compare(const ShellNet::Storage shadow) const;

  virtual ShellNet::ValueState writeIfNotEq(char* valueStr, size_t len, ShellNet::Storage* shadow,
                                            NetFlags);
  
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat,
                              NetFlags, CarbonModel*) const;

  virtual bool isDataNonZero() const;

  virtual int hasDriveConflict() const;
  virtual int hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const;

  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask);
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel*) const;

protected:
  virtual void setConstantBits();

  inline void doFastDeposit(const UInt32* buf, const UInt32* drive,
                            CarbonModel* model);
  inline void doFastDepositWord(UInt32 buf, int index, UInt32 drive,
                                CarbonModel* model);
  inline CarbonStatus doDepositWord(UInt32 buf, int index, UInt32 drive,
                                    CarbonModel* model);
  inline CarbonStatus doFastDepositRange(const UInt32* buf,int range_msb,
                                         int range_lsb,const UInt32* drive,
                                         CarbonModel* model);

  bool assignValue(const UInt32* buf, const UInt32* drive); 
  bool assignValueWord(UInt32 val, UInt32 drv, int index);
  bool assignValueRange(const UInt32* buf, const UInt32* range, size_t index, size_t length); 

private:
  
  void examineValue(UInt32* val, UInt32* drv) const;
  void examineValueWord(UInt32* val, UInt32* drv, 
                                int index) const;
  void examineValueRange(UInt32* val, UInt32* drv, 
                                 size_t index, 
                                 size_t length) const;
  
  void calcValue(UInt32* val, UInt32* drv) const;
  void calcValuePrim(UInt64* val, UInt64* drv) const;
  void calcValueWord(UInt32* val, UInt32* drv, int index) const;
  void examineXVals(UInt32* val, UInt32* drv) const;

  ValueState compareValue(const UInt64* val, const UInt64* drv) const;
  void copyValAndXDriveWord(UInt32* val, UInt32* drv, 
                                    int index) const;
  void getValuePtrs(const UInt64** val, const UInt64** xdrv, const UInt64** idrv) const;

protected:
  ShellNetTristate8* mTri;
  
private:

  CarbonTristateVector8();
  CarbonTristateVector8(const CarbonTristateVector8&);
  CarbonTristateVector8& operator=(const CarbonTristateVector8&);
};

class CarbonTristateVectorA : public CarbonTristateVector
{
public: CARBONMEM_OVERRIDES
  //! constructor for bidi vectors
  CarbonTristateVectorA(UInt32* idata, UInt32* idrive, 
                       UInt32* xdata, UInt32* xdrive);
  
  //! constructor for tristate vectors
  CarbonTristateVectorA(UInt32* idata, UInt32* idrive);
  
  virtual ~CarbonTristateVectorA();

  //! ShellNet::getTraits()
  virtual void getTraits(Traits* traits) const;  

  virtual ShellNet::Storage allocShadow() const;
  
  virtual void freeShadow(ShellNet::Storage* shadow);

  //! ShellNet::setToDriven()
  virtual bool setToDriven(CarbonModel* model);

  //! ShellNet::setToUndriven()
  virtual bool setToUndriven(CarbonModel* model);

  //! ShellNet::setWordToUndriven()
  virtual bool setWordToUndriven(int index, CarbonModel* model);

  //! ShellNet::setRangeToUndriven()
  virtual bool setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model);

  //! ShellNet::resolveXdrive()
  virtual bool resolveXdrive(CarbonModel* model);

  //! ShellNet::setRawToUndriven()
  virtual void setRawToUndriven(CarbonModel* model);

  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);

  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);

  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const;
  
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const;

  virtual CarbonStatus examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const;

  virtual CarbonStatus examineValXDriveWord(UInt32* val, UInt32* drv, int index) const;

  virtual void update(ShellNet::Storage* shadow) const;
  
  virtual void updateUnresolved(ShellNet::Storage* shadow) const;
  
  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive);

  virtual ShellNet::ValueState compare(const ShellNet::Storage shadow) const;

  virtual ShellNet::ValueState writeIfNotEq(char* valueStr, size_t len, ShellNet::Storage* shadow,
                                            NetFlags);
  
  virtual CarbonStatus format(char* valueStr, size_t len, 
                              CarbonRadix strFormat,
                              NetFlags, CarbonModel*) const;

  virtual bool isDataNonZero() const;

  virtual int hasDriveConflict() const;
  virtual int hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const;

  virtual ValueState writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask);
  virtual CarbonStatus formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel*) const;

protected:
  virtual void setConstantBits();

  inline void doFastDeposit(const UInt32* buf, const UInt32* drive,
                            CarbonModel* model);
  inline void doFastDepositWord(UInt32 buf, int index, UInt32 drive,
                                CarbonModel* model);
  inline CarbonStatus doDepositWord(UInt32 buf, int index, UInt32 drive,
                                    CarbonModel* model);
  inline CarbonStatus doFastDepositRange(const UInt32* buf,int range_msb,
                                         int range_lsb,const UInt32* drive,
                                         CarbonModel* model);

  bool assignValue(const UInt32* buf, const UInt32* drive); 
  bool assignValueWord(UInt32 val, UInt32 drv, int index);
  bool assignValueRange(const UInt32* buf, const UInt32* range, size_t index, size_t length); 

  //! sign extend the value and the drive
  /*!
    used only by CarbonTristateVectorAS and
    CarbonTristateVectorASInput
    
  */
  void signExtend();
  
  //! Zero extraneous bits in buf and drive
  void sanitize(UInt32* buf, UInt32* drive) const;
  
  //! Zero extraneous bits in buf/drive words, if index is last word
  void sanitizeWord(UInt32* buf, UInt32* drive, UInt32 index) const;

private:
  
  void examineValue(UInt32* val, UInt32* drv) const;
  void examineValueWord(UInt32* val, UInt32* drv, 
                                int index) const;
  void examineValueRange(UInt32* val, UInt32* drv, 
                                 size_t index, 
                                 size_t length) const;
  
  void calcValue(UInt32* val, UInt32* drv) const;
  void calcValueWord(UInt32* val, UInt32* drv, int index) const;
  void examineXVals(UInt32* val, UInt32* drv) const;

  ValueState compareValue(const UInt32* val, const UInt32* drv) const;
  void copyValAndXDriveWord(UInt32* val, UInt32* drv, 
                                    int index) const;

  UInt32 getCalcDriveWord(UInt32 index) const;
  const UInt32* getExamineStore() const;
  void getValuePtrs(const UInt32** val, const UInt32** xdrv, const UInt32** idrv) const;

protected:
  ShellNetTristateA* mTri;
  
private:

  CarbonTristateVectorA();
  CarbonTristateVectorA(const CarbonTristateVectorA&);
  CarbonTristateVectorA& operator=(const CarbonTristateVectorA&);
};

//! Signed tristate vector class
/*!
  We have to do our own sign extension when depositing to the
  internals of SBitVector
*/
class CarbonTristateVectorAS : public CarbonTristateVectorA
{
 public:
  CARBONMEM_OVERRIDES

  //! constructor for bidi vectors
  CarbonTristateVectorAS(UInt32* idata, UInt32* idrive, 
                       UInt32* xdata, UInt32* xdrive);
  
  //! constructor for tristate vectors
  CarbonTristateVectorAS(UInt32* idata, UInt32* idrive);

  virtual ~CarbonTristateVectorAS();
  
  virtual CarbonStatus examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const;
  
  virtual CarbonStatus examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const;


  virtual CarbonStatus deposit(const UInt32* buf, const UInt32*, CarbonModel* model);
  
  virtual CarbonStatus depositWord(UInt32 buf, int index, UInt32, CarbonModel* model);
  
  virtual CarbonStatus depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model);
  
  //! ShellNet::fastDeposit()
  virtual void fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model);
  //! ShellNet::fastDepositWord()
  virtual void fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model);
  //! ShellNet::fastDepositRange()
  virtual void fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model);

 private:
  CarbonTristateVectorAS();
  CarbonTristateVectorAS(const CarbonTristateVectorAS&);
  CarbonTristateVectorAS& operator=(const CarbonTristateVectorAS&);
};

#endif
