// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __SHELLDATA_H_
#define __SHELLDATA_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif

class ShellNet;
class CarbonWaveNetAssoc;

//! Keyed placeholder for instance-specific data
/*!
  The key here is an id that is assigned to the CarbonModel upon
  construction. Each instance of the same design will have a different
  id.

  Since this object is used by all instances of the same design,
  access to it needs to be protected with a mutex.
*/
class ShellData
{
public: CARBONMEM_OVERRIDES
  //! constructor
  ShellData();
  //! destructor
  ~ShellData();

  //! Get the modifiable CarbonNet
  ShellNet* getCarbonNet(UInt32 id)
  {
    const ShellData* me = const_cast<const ShellData*>(this);
    return const_cast<ShellNet*>(me->getCarbonNet(id));
  }

  //! Get const CarbonNet
  const ShellNet* getCarbonNet(UInt32 id) const;

  //! Get the modifiable NetAssoc
  CarbonWaveNetAssoc* getNetAssoc(UInt32 id);
  
  //! Get const NetAssoc
  const CarbonWaveNetAssoc* getNetAssoc(UInt32 id) const;

  //! Put the CarbonNet
  void putCarbonNet(ShellNet* net, UInt32 id);
  
  //! Put the NetAssoc
  void putNetAssoc(CarbonWaveNetAssoc* netAssoc, UInt32 id);
  
private:
  class Helper;
  Helper* mHelper;
};

#endif
