// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file 
  Classe(s) specific to string handling in Verilog
*/

#ifndef __HdlVerilogString_h_
#define __HdlVerilogString_h_


#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

class UtString;

//! a class to contain a collection of methods related to string processing specific to Verilog
class HdlVerilogString
{
public: CARBONMEM_OVERRIDES

  //! Constructor, 
  HdlVerilogString();
  //! destructor  (why virtual?  to keep someone from making an instance of this class?)
  virtual ~HdlVerilogString();

  /*! \brief convert a bitvector into char * representation, with trailing NULL 

     \param buffer Destination buffer
     \param bufferSize size of \a buffer, including trailing null.
     \param source pointer to Source 
     \param numCharsToConvert max number of characters to take from \a source
  */
  static char *convertToStrRep(char * buffer, UInt32 bufferSize, UInt32 numCharsToConvert, const UInt32 * source);


  /*! \brief convert a UInt8 into char * representation, with trailing NULL 

     \param buffer Destination buffer
     \param bufferSize size of \a buffer, including trailing null.
     \param source pointer to Source 
     \note the max number of characters to take from \a source is 1
  */
  static char *convertToStrRep(char * buffer, UInt32 bufferSize, UInt8  source);


  /*! \brief convert a UInt16 into char * representation, with trailing NULL 

     \param buffer Destination buffer
     \param bufferSize size of \a buffer, including trailing null.
     \param source pointer to Source 
     \note the max number of characters to take from \a source is 2
  */
  static char *convertToStrRep(char * buffer, UInt32 bufferSize, UInt16 source);

  /*! \brief convert a UInt32 into char * representation, with trailing NULL 

     \param buffer Destination buffer
     \param bufferSize size of \a buffer, including trailing null.
     \param source pointer to Source 
     \note the max number of characters to take from \a source is 4
  */
  static char *convertToStrRep(char * buffer, UInt32 bufferSize, UInt32 source);

  /*!
     \brief convert a UInt64 into char * representation, with trailing NULL 

     \param buffer Destination buffer
     \param bufferSize size of \a buffer, including trailing null.
     \param source pointer to Source 
     \note the max number of characters to take from \a source is 8
  */
  static char *convertToStrRep(char * buffer, UInt32 bufferSize, UInt64 source);


  //! reverse an array of characters, size chars are reversed, caller should avoid including the trailing zero within size
  static void reverse(char * str, UInt32 size);

  /*! \brief Convert a verilog string into a string of 1's and 0's that represents that string
   *
   * During this conversion some escaped characters are first
   * converted into a single charcter equivalent before conversion to
   * the binary form.  The folloing table lists the supported Verilog
   * escaped characters and the single character they are mapped to.
   \verbatim
    This function converts
     \n into a newline
     \t to a tab
     \\ into a slash
     \" into a double quote
     \d into the char represented by the octal number d (d may be up to 3 digits long)
   \endverbatim
   * 
   *
   * \param orig_str null terminated c-string to be converted
   * \param binary_str where the converted string is placed
   *
   */
  static void convertVerilogStringToBinaryString(const UtString* orig_str, UtString* binary_str);
private:
  // Hide copy and assign constructors.
  HdlVerilogString(const HdlVerilogString&);
  HdlVerilogString& operator=(const HdlVerilogString&);

  /*!  converts any non-trailing null characters into spaces
   *
   * NOTE this routine expectes the characters in buffer to be stored
   * in reverse order.
   *
   * The Verilog LRM states that leading zeros are not printed when a
   * string stored in a register is $displayed with a %s format.
   * We interpret leading zeros to mean leading NULL characters.
   *
   * The LRM is silent about how to handle non-leading NULL
   * characters.
   *
   * Aldec and nc print these non-leading NULL characters as spaces.
   * This routine does the necessary conversion for a buffer
   * where the characters are stored in reverse order.
   */
  static void convertNonTrailingNullToSpace(char* buffer, SInt32 size);
  
};
#endif  // __HdlVerilogString_h_
