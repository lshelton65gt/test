// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file 
  Classes releated to file operations used within verilog simulations.
  Such as for $fopen and $fdisplay.  Includes support for multi channel descriptors.
*/

#ifndef __HdlVerilogOStream_h_
#define __HdlVerilogOStream_h_


#ifndef __BitVector_h_
#include "util/BitVector.h"
#endif
#ifndef __UtCachedFileSystem_h_
#include "util/UtCachedFileSystem.h"
#endif
#ifndef __HdlFileSystem_h_
#include "hdl/HdlFileSystem.h"
#endif
#ifndef __UtIOStream_h_
#include "util/UtIOStream.h"
#endif
#ifndef __HdlOStream_h_
#include "hdl/HdlOStream.h"
#endif

class MsgContext;

//! a collection of information about files used for output in verilog
/*
  derived from HdlOStream which contains common VHDL/verilog file operations.

  In particular, this adds support for Multi Channel Descriptors
 */
class HdlVerilogOStream: public HdlOStream
{
#if ! pfGCC_2
  using HdlOStream::flush;
  using HdlOStream::close;
#endif

public: CARBONMEM_OVERRIDES
  //! Constructor, 
  HdlVerilogOStream(HDLFileSystem * hdl_file_system, MsgContext* msgContext):
   HdlOStream(hdl_file_system, msgContext) 
  {}
 
  //! destructor
  ~HdlVerilogOStream() {}

  //! flush all verilog files specified by \a fileDescriptor
  /*! this will flush stdout and stderr if included in the \a
   * fileDescriptor
   */
  bool flush(UInt32 fileDescriptor);

  //! close all the verilog files specified by \a fileDescriptor
  /*! this will close stdout and stderr if included in the \a
   * fileDescriptor
   */
  bool close(UInt32 fileDescriptor);

  //! \brief set the files defined by \a descriptor to be the current output files,
  // the next output operation to VerilogOutFileSystem will send data
  // to these/this file.
  void putTargetFileDescriptor( UInt32 descriptor );

  //! Open a file for output, returns the file descriptor.
  UInt32 VerilogOFileOpen(const char* filename, const char* mode);

  //! Open a file for output, returns the MCD (multi channel descriptor)
  UInt32 VerilogOFileOpen(const char* filename);

};
#endif // __HdlVerilogOStream_h_
