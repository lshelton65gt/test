// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file 
  Classes related to file input operation used within VHDL/verilog simulations.
  Such as for $fgets ,$fread, $fgetc ,READ etc. 
*/

#ifndef __HdlIStream_h_
#define __HdlIStream_h_


#ifndef __BitVector_h_
#include "util/BitVector.h"
#endif
#ifndef __UtCachedFileSystem_h_
#include "util/UtCachedFileSystem.h"
#endif
#ifndef __HdlFileSystem_h_
#include "hdl/HdlFileSystem.h"
#endif
#ifndef __UtIStream_h_
#include "util/UtIStream.h"
#endif
#ifndef __UtHashMap_h_
#include "util/UtHashMap.h"
#endif
#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif

class UtIOStringStream;
class MsgContext;

//! a collection of common information about files used for input in VHDL/verilog
/*
  derived from UtIStream so that we can benefit from the operator>>
  definitions.

  there are multiple sets of files that this keeps track of
  \li files that are standard stdin
  \li Files that have been opened for input with $fopen
 
 */
class HdlIStream: public UtIStream
{
public: CARBONMEM_OVERRIDES
  //! Constructor, 
  HdlIStream(HDLFileSystem * hdl_file_system, MsgContext* msgContext);
  //! destructor
  virtual ~HdlIStream();



  //! if any of the files are open
  bool is_open() const;

  //! close all non-reserved files managed by HdlFileSystem
  bool close() {  return mHDLFileSystem->close(); }

  bool close(UInt32 fileDescriptor);
    
  //! read specified number of character into buf
  virtual bool read(char* buf, UInt32 len);

  //! read bytes from a file
  virtual UInt32 readFromFile(char* buf, UInt32 len);

  //! \brief set the file defined by \a descriptor to be the current input file
  // the next output operation to HdlIStream will take data from this file.
  UtIStream* putSourceFileDescriptor( UInt32 descriptor );

  //! Read into the StringStream  from file associated with mSourceDescriptor
  void readLine( UtIOStringStream* str_stream );
  
  //  Open a file for input, returns the file descriptor and status
  UInt32 HdlIFileOpen( UInt2* status, const char* filename, const char* mode);

  //! Open a file for input, returns the file descriptor.
  UInt32 HdlIFileOpen(const char* filename, const char* mode);

  //! Open a file for input.
  UInt32 HdlIFileOpen(const char* filename);

  //! return if end of file reached.
  bool endOfFile(UInt32 fileDescriptor);

  //! Return handle to msgContext
  MsgContext * getMsgContext (void) {return mMsgContext;}


  //! typedef for file descriptors, we use UtIStream since they may be UtIO:cin or UtICstream
  typedef UtArray<UtIStream *> SourceFileDescriptors;
  //! typedef for map from file descriptors to streams, we use UtIStream since they may be either UtIO:cin or UtICstream
  typedef UtHashMap<UInt32, UtIStream *> FDtoStreamMap;

  virtual SInt64 seek(SInt64 pos, UtIO::SeekMode);

protected:
  //! the file system used for this design
  HDLFileSystem*  mHDLFileSystem;


  //! the descriptor that will be used for the next read operation
  /* this descriptor will be a FD.
  */
  UInt32 mSourceFileDescriptor;

  //! zero or more UtIStreams, it is always in sync with mSourceFileDescriptor
  SourceFileDescriptors mSourceFileDescriptors;

  //! the MSB of a 32 bit file descriptor distinguishes between MCD and standard FD (if MSB is 0 then you have a MCD)
  static const UInt32 sNonMCDFlagBit = (1U<<31);

  enum reservedFD { eMCDstdin=0,        //!< tradition (not LRM) says this is MCD stdin 
                    eMCDstdout=1,	//!< tradition (not LRM) says this is MCD stdout 
                    eFDstdin=(sNonMCDFlagBit|0),  //!< LRM says x80000000 is reserved for STDIN
                    eFDstdout=(sNonMCDFlagBit|1), //!< LRM says x80000001 is reserved for STDOUT
                    eFDstderr=(sNonMCDFlagBit|2)  //!< LRM says x80000002 is reserved for STDERR
  };

  //! Message Context
  MsgContext* mMsgContext;

};
#endif // __HdlIStream_h_
