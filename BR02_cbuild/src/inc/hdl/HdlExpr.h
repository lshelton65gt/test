// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION

  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING AND/OR
  DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN
  CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _HDLEXPR_H_
#define _HDLEXPR_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

//! Object that parses and composes HDL expressions
/*! This is an abstract interface that is used by all the different
 *  HDL's. It parses and composes HDL expressions.
 *
 *  This class currently only implements constants by converting them
 *  to dynamic bit vectors. In the future we would like to create more
 *  complex expressions. A decision has to be made on whether we
 *  create Nucleus or Carbon expressions or a third alternative
 *  (although that seems wrong at this point).
 */
class HdlExpr
{
public: CARBONMEM_OVERRIDES
  //! virtual destructor
  virtual ~HdlExpr() {}

  //! Enumeration for the possible return values when parsing an expression
  enum Status
  {
    eSuccess,   //!< Parsing was successful
    eWarning,   //!< A possible issue was found; details in string message
    eError      //!< An error was encountered; details in string message
  };

  //! Parses a string for an HDL constant and fills in a DynBitVector
  /*! This routine parses strings of the form "32'h1234abcd" that
   *  match Verilog constant syntax. It fills in a dynamic bit vector
   *  with the value.
   *
   *  \param constStr - a string representing a constant in the HDL syntax.
   *
   *  \param value - a dynamic bitvector that is filled with the
   *  value. Note that this routine will resize the dynamic bit vector
   *  so that it has the space.
   *
   *  \param errMsg - a string to be filled in with an appropriate
   *  message if there is a problem parsing the constant. This is only
   *  filled in if the return value is eWarning or eError.
   *
   *  \returns one of eSuccess, eWarning, or eError. 
   */
  virtual Status parseConst(const char* constStr,
                            DynBitVector& value,
                            UtString* errMsg) = 0;
}; // class HdlExpr

#endif // _HDLEXPR_H_
