// -*- C++ -*-                
/*****************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __HDLHIERPATH_H_
#define __HDLHIERPATH_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#include "util/Util.h"

class StringAtom;
class AtomicCache;
class HierName;
class HdlId;
class UtString;
class UtStringArray;

//! Object that parses and composes HDL hierarchical paths
/*!
  This is an abstract interface that is used by all the different
  HDL's. It parses and composes hierarchical paths. A path can contain
  one or more identifiers.
*/
class HdlHierPath
{
public:  
  CARBONMEM_OVERRIDES

  //! Type definition for a vector of StringAtoms
  typedef UtArray<StringAtom*> StrAtomVec;
  
  //! virtual destructor
  virtual ~HdlHierPath() {}

  //! Parsing Status enumeration
  enum Status {
    eLegal, //!< Legal HDL token/name
    eIllegal, //!< Illegal HDL token/name
    eEndPath, //!< Reached the end of the UtString, token parsing only
    eNoExist //!< The StringAtom for token does not exist in the cache
  };

  //! Decompose hierarchical path into list of strings
  /*!
    \param path Hierarchical path name to parse
    \param ids Pointer to list storage. The results of the parsing
    will be put in here in order
    \param info Upon decomposition this will hold the final identifier
    information. So, if the path represents a vector, the vector
    information will be contained in this object.
    \returns Whether or not the path is valid
  */
  virtual Status decompPath(const char* path, UtStringArray* ids, HdlId* info) const = 0;

  //! Decompose hierarchical path into a list of StringAtoms
  /*!
    This method is meant to save lookups for StringAtoms when querying
    the symboltable. If the hierarchical parts of the name are not in
    the atomic cache then this method will return false. In other
    words, the StringAtoms must already exist in the cache.

    \param path Hierarchical path name
    \param ids Pointer to a list of StringAtoms. This is where the
    result will reside.
    \param strCache The UtString cache of pre-existing strings.
    \param info Upon decomposition this will hold the final identifier
    information. So, if the path represents a vector, the vector
    information will be contained in this object.
    \returns eLegal if the path was parsed successfully. eIllegal if the
    path is an illegal HDL path or eNoExist if the scopes of the path do not
    already reside in the strCache.
  */
  virtual Status decompPathAtom(const char* path, StrAtomVec* ids, 
                                const AtomicCache* strCache,
                                HdlId* info) const = 0;

  //! Parses path to the end of the next token (scope in path)
  /*!
    This modifies the path UtString to reflect what is left in the path
    after parsing the next token. See each implementation's notes for
    more detail.
    \param path Modifiable pointer to path name. This can be passed in
    again to get the next token until the entire path is parsed.
    \param buffer Storage area for the next token. Buffer is cleared
    prior to parsing each token.
    \param info Each scope's info as it is parsed is placed in this
    object
    \param keepVectorInfo If vector information is found and this is
    true the vector information will be added (or kept) in the name;
    otherwise, it could be removed depending on the semantics of the
    HDL language.
    \returns If the end of the UtString is encountered or an illegal
    token then this returns eEndPath; otherwise, the status will tell
    you if the identifier is legal or not. An identifier is legal if
    parsing takes place up until an illegal identifier. Buffer is modified to
    contain the identifier
  */
  virtual Status parseToken(const char** path, UtString* buffer, HdlId* info, bool keepVectorInfo) const = 0;

  //! Parses path to the end of the next token (scope in path)
  /*!
    This is the same as parseToken with a modifiable char*, but this
    uses a modifiable string.
    
    \sa parseToken(const char**, UtString*, HdlId*, bool)
  */
  virtual Status parseToken(UtString* pathStr, UtString* buffer, HdlId* info, bool keepVectorInfo) const = 0;

  //! Parses non-null terminated string to the end of the next token
  /*!
    \param pathBegin The beginning of the string to parse
    \param pathEnd The end of the string to parse (must be in the same
    buffer - undefined results otherwise).
    \param buffer Buffer into which the parsed token will reside
    \param info Optionally NULL parameter to give information on the
    token that was parsed.
    \param keepBrackets If true and vector is parsed the brackets will
    remain in the identifier.
    
    \sa parseToken(const char**, UtString*, HdlId*, bool)
  */
  virtual Status parseToken(const char** pathBegin, const char* pathEnd, UtString* buffer, HdlId* info, bool keepBrackets) const = 0;

  //! Parse an entire name in a list
  /*!
    This basically does parseTokens until a name is completed (ends
    with an unexpected space).
    \param path Modifiable const char* of the path to parse. Points at
    the end of the last token parsed.
    \param ids Storage area for the identifiers
    \param info Information about the name
    \returns Parsing status as in parseToken
  */
  virtual Status parseName(const char** path, UtStringArray* ids, HdlId* info) const = 0;
  
  //! Compose path from list of strings
  /*!
    \param ids List of identifiers to compose into a full path. It is
    assumed that these identifiers are valid in order to save
    performance and complexity.
    \param buffer Storage area for the full path name
    \param info If not NULL, this method uses this to compose the
    final identifier in the path name; otherwise, it is assumed that
    the path name is a scalar.
    \returns The contents of buffer.
  */
  virtual const char* compPath(const UtStringArray& ids, UtString* buffer,
                               const HdlId* info = NULL) const = 0;

  //! Compose path from list of StringAtoms
  /*!
    \param ids List of identifiers in the form of StringAtoms to
    compose into a full path. It is assumed that these identifiers are
    valid in order to save performance and complexity.
    \param buffer Storage area for the full path name
    \param info If not NULL, this method uses this to compose the
    final identifier in the path name; otherwise, it is assumed that
    the path name is a scalar.
    \returns The contents of buffer.
  */
  virtual const char* compPathAtom(const StrAtomVec& ids, UtString* buffer,
                                   const HdlId* info = NULL) const = 0;
  
  //! Compose path from HierName
  /*!
    \param name Hierarchical name used to compose the path
    \param buffer Storage area for the full path name
    \param info If not NULL, this method uses this to compose the
    final identifier in the path name; otherwise, it is assumed that
    the path name is a scalar.
    \returns The contents of buffer.
  */    
  virtual const char* compPathHier(const HierName* name, UtString* buffer,
                                   const HdlId* info = NULL) const = 0;

  //! Compose a path from name and append tail to the name
  /*!
    \param name Hierarchical Name, most likely a scope name
    \param tail Final identifier to add to the composed path, most
    likely a signal name within the scope. This can be NULL.
    \param buffer Storage for composed string
    \param info If not NULL, this method uses this to compose the
    final identifier in the path name; otherwise, it is assumed that
    the path name is a scalar
    \returns The contents of the buffer
  */
  virtual const char* compPathHierAppend(const HierName* name, StringAtom* tail, UtString* buffer, 
                                         const HdlId* info = NULL) const = 0;

  //! Compose a path from an assumed path and an additional identifier
  /*!
    It may be necessary to incrementally compose a path from a
    buffer. In this case, the buffer would be repeatedly passed in and
    the next identifier would be appended to it. 
    
    \param buffer Buffer to which to append. It may already contain a path.
    \param tail A UtString to append to the buffer in the semantics of
    the HDL language. This can be NULL.
    \param info If not NULL, this will be used to compose the final
    identifier in the path name; otherwise, it is assumed that the
    path name is a scalar.
    \returns The contents of the buffer

    \warning It is assumed that the contents of the buffer represents
    a \e valid HDL path. The contents are \b not checked for validity.
  */
  virtual const char* compPathAppend(UtString* buffer, const char* tail,
                                     const HdlId* info = NULL) const = 0;
  

  //! Allow wildcards or not
  /*!
    \param allow If true, wildcard characters will be allowed in HDL
    names as part of the identifier.
  */
  virtual void putAllowWildcard(bool allow) = 0;


  //! Take an identifier and try to make it legal if not already
  /*!
    This is useful for identifiers that have the escape stripped
    off. This makes any illegal identifier that does not have a space
    in the middle legal. If there is a space of any kind in the middle
    of the identifier or if the string is empty this will return eIllegal.
    This will strip off ranges and bit selects upon legalization and
    put the info into the HdlId structure that is passed in.
    
    \param id This is the identifier that will be made legal
    in-line. I.e., The result will also be put in this string.
    \param info This must be non-null. Info about which type the now
    legal id is considered to be.
    \param keepVectorInfo If vector information is found and this is
    true the vector information will be added (or kept) in the name;
    otherwise, it could be removed depending on the semantics of the
    HDL language.
    \warning This ONLY works for identifiers, not hierarchical
    paths. So, don't pass in, e.g. \verbatim in verilog, a.b.&*(, and
    expect a.b.\&*( . This will return \a.b.&*( .\endverbatim
  */
  virtual Status makeLegalId(UtString* id, HdlId* info, bool keepVectorInfo) const = 0;


  //! Take a non-null terminated identifier and try to make it legal if not already
  /*!

    \sa makeLegalId(UtString*, HdlId*, keepVectorInfo)
    
    \param pathBegin Pointer to the beginning of string to
    consider. This gets modified to end where what the legal aspect of
    the token is considered to be after slashifying if applicable. So,
    for example, if the id was a "*" and "*" is not legal HDL. This will
    legalize it and increment *pathBegin.
    \param pathEnd End of string to consider. This must be in the same
    buffer space as pathBegin; otherwise, undefined results will
    occur.
    \param buffer This is where the legal id will be placed.
    \param keepVectorInfo If vector information is found and this is
    true the vector information will be added (or kept) in the name;
    otherwise, it could be removed depending on the semantics of the
    HDL language.
    \warning This ONLY works for identifiers, not hierarchical
    paths. So, don't pass in, e.g. \verbatim in verilog, a.b.&*(, and
    expect a.b.\&*( . This will return \a.b.&*( .\endverbatim
  */
  virtual Status makeLegalId(const char** pathBegin, const char* pathEnd, UtString* buffer, HdlId* info, bool keepVectorInfo) const = 0;

  //! Parse a non-null terminated string that has only vector info
  /*!
     This is mainly just for parsers. It might be that an identifier
     is being parsed in tokens and the vector information is in a
     separate token.
     
     \param s Pointer to string to consider for vector information. It
     should begin only with whitespace and/or vector characters.
     \param endPtr The end of the string
     \param info Gets filled with the vector information
     \retval eLegal if the vector information is valid or if there is
     no vector information.
     \retval eIllegal If a vector was found, but it is invalid
  */
  virtual Status parseVectorInfo(const char** s, const char* endPtr, HdlId* info) const = 0;

  //! Append vector info to buffer
  /*!
    Blindly appends any vector or index information to an identifier.

    \param buf Buffer to append to
    \param info Identifier information. Asserts if it is type
    eInvalid.
  */
  virtual void appendVectorInfo(UtString* buf, const HdlId* info) const = 0;
};

#endif
