// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2013 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION

  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING AND/OR
  DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN
  CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Methods related to support for: timescale, timeunit, timeprecision ...
*/
#ifndef _HDLTIME_H_
#define _HDLTIME_H_

#include "util/c_memmanager.h"
#include "shell/carbon_shelltypes.h"

class MsgContext;
class SourceLocator;

class HdlTime
{
public:
  CARBONMEM_OVERRIDES
  HdlTime() {}
  ~HdlTime() {}

  // We are unable to pass SourceLocator and MsgContext (it seems there is compilation order problem)
  // I don't wanted big changes in the flows order, so added message encodings
  // 0 - NOMESSAGE = no report
  // 1 - WARNING = only warning will be reported
  // 2 - ERROR =  only error will be reported
  // 3 - WARNING_AND_ERROR = both warning and error will be reported (constructing by sum = ERROR + WARNING)
  // This isn't quite nice we should be careful in future support and messaging
  typedef enum {NOMESSAGE=0, WARNING, ERROR, WARNING_AND_ERROR} MsgStatus;

  //! Array of string defining the valid Verilog time units
  static const char *VerilogTimeUnits[18]; /* the values of this array are set in HdlTime.cxx */

  /*! Parses a string that is a Verilog time specification (e.g. 1ns or 100ms )
   *  \returns a number that is the power of 10 that represents the time unit., ie the string 100ms returns -3
   */
  static SInt8 parseTimeString( const char *timeString, unsigned* status);
}; // class HdlTime

#endif // _HDLTIME_H_
