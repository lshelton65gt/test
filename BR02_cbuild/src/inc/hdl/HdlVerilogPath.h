// -*- C++ -*-                
/*****************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __HDLVERILOGPATH_H_
#define __HDLVERILOGPATH_H_


#ifndef __HDLHIERPATH_H_
#include "hdl/HdlHierPath.h"
#endif

#ifndef _UtStringArray_h_
#include "util/UtStringArray.h"  // needed for UnsortedCLoop
#endif

/*!
  \file
  HdlVerilogPath implements a Verilog version of HdlHierPath
*/

class HdlInfo;

//! Object that parses and composes Verilog hierarchical paths
/*!
  This handles Verilog path/identifier parsing with and without
  wildcards and Verilog path composition.
*/
class HdlVerilogPath : public HdlHierPath
{
public: CARBONMEM_OVERRIDES

  //! Default constructor
  HdlVerilogPath(); 

  //! virtual destructor
  virtual ~HdlVerilogPath();

  //! Decompose hierarchical path into list of strings
  virtual Status decompPath(const char* path, UtStringArray* ids, HdlId* info) const;


  //! Decompose hierarchical path into a list of StringAtoms
  /*!
    The verilog identifiers must already exist in the strCache. This
    will fail if they do not or if the path does not represent a valid
    hierarchical verilog path
  */
  virtual Status decompPathAtom(const char* path, StrAtomVec* ids, 
                                const AtomicCache* strCache,
                                HdlId* info) const;
  
  //! Parses path to the end of the next token (scope in path)
  /*!
    This modifies the path UtString to reflect what is left in the path
    after parsing the next token. For example, if the hierarchical
    path name is "top.middle.end", the first call would return "top"
    and the path would be "middle.end". The second call would return
    "middle" and the path would be "end". 
    This also handles whitespace between identifiers. Therefore if
    the path is actually a list of names, like "reg1 reg2 u2.a" then
    the first call would return reg1, the second would return reg2,
    the third would return u2, etc.
    The keepBrackets parameter allows for keeping the names of
    vectorized modules. For example, if a module foo is vectorized
    [0:2], and one instance of it is foo[0], then parseToken will stop
    after parsing foo[0], but only return foo in the buffer if
    keepBrackets is false. If keepBrackets is true, then foo[0] would
    be returned. Obviously, this would have the same effect on a
    vector net.
  */
  virtual Status parseToken(const char** path, UtString* buffer, HdlId* info, bool keepBrackets) const;

  //! Parses path to the end of the next token (scope in path)
  /*!
    \sa parseToken(const char**, UtString*, HdlId*, bool)
  */
  virtual Status parseToken(UtString* pathStr, UtString* buffer, HdlId* info, bool keepBrackets) const;

  //! Parses non-null terminated string to the end of the next token
  /*!
    \sa parseToken(const char**, UtString*, HdlId*, bool)
  */
  virtual Status parseToken(const char** pathBegin, const char* pathEnd, UtString* buffer, HdlId* info, bool keepBrackets) const;
  

  //! Parse an entire name in a list of Verilog paths
  virtual Status parseName(const char** path, UtStringArray* ids, HdlId* info) const;

  //! Compose path from list of strings
  virtual const char* compPath(const UtStringArray& ids, UtString* buffer,
                               const HdlId* info = NULL) const;  


  //! Compose path from list of StringAtoms
  /*!
    This does exactly the same thing as compPath() but with
    StringAtoms
  */
  virtual const char* compPathAtom(const StrAtomVec& ids, UtString* buffer,
                                   const HdlId* info = NULL) const;

  //! Compose path from HierName
  /*!
    This creates a verilog hierarchical path from a hierarchical name
    structure
  */
  virtual const char* compPathHier(const HierName* name, UtString* buffer,
                                   const HdlId* info = NULL) const;
  
  //! Compose a path from name and append tail to the name
  /*!
    This creates a Verilog hierarchical path from a hierarchical name
    and a tail that is appended as a the final identifier.
  */
  virtual const char* compPathHierAppend(const HierName* name, StringAtom* tail, UtString* buffer, 
                                         const HdlId* info = NULL) const;

  //! Compose a verilog path from an assumed path and an additional identifier
  virtual const char* compPathAppend(UtString* buffer, const char* tail,
                                     const HdlId* info = NULL) const;

  //! Allow wildcards or not
  virtual void putAllowWildcard(bool allow);

  //! HdlHierPath::makeLegalId()
  virtual Status makeLegalId(UtString* id, HdlId* info, bool keepBrackets) const;

  //! HdlHierPath::makeLegalId()
  virtual Status makeLegalId(const char** pathBegin, const char* pathEnd, UtString* buffer, HdlId* info, bool keepVectorInfo) const;

  //! HdlHierPath::parseVectorInfo()
  virtual Status parseVectorInfo(const char** s, const char* endPtr, HdlId* info) const;

  //! HdlHierPath::appendVectorInfo()
  virtual void appendVectorInfo(UtString* buf, const HdlId* info) const;

private:
  bool mAllowWildcard;

  //! helper class
  class ListType
  {
  public: CARBONMEM_OVERRIDES
    //! virtual destructor
    virtual ~ListType() = 0;
    //! add id to list, return false if it fails
    virtual bool append(const char* id) = 0;

    //! Begin iteration
    /*!
      Do not call append() after beginning iteration
      \returns The first UtString in the iteration
    */
    virtual const char* beginIter() = 0;
    
    //! Get the next list item
    /*!
      Meant only for lists that are already constructed.
      \warning Behavior undefined if append is done after this is
      called. beginIter() must be called before using this function.
      \returns NULL when at the end of the list
    */
    virtual const char* next() = 0;
  };

  //! helper class, UtString context
  class ListStrType : public ListType
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    ListStrType(UtStringArray* hierList);
    //! for iteration only
    ListStrType(const UtStringArray& hierList);

    //! virtual destructor
    virtual ~ListStrType();
    //! add id to list, always returns true
    bool append(const char* id);
    
    //! begin UtString iteration
    const char* beginIter();
    
    //! Get the next list item
    const char* next();

  private:
    UtStringArray* mList;
    UtStringArray::UnsortedCLoop mIter;

    // helper
    const char* getIterStr() const;

    // forbid
    ListStrType();
    ListStrType(const ListStrType&);
    ListStrType& operator=(const ListStrType&);
  };

  //! helper class, StringAtom* context
  class ListAtomType : public ListType
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    ListAtomType(HdlHierPath::StrAtomVec* hierList, const AtomicCache* cache);
    //! for iteration only
    ListAtomType(const HdlHierPath::StrAtomVec& hierList);
    //! virtual destructor
    virtual ~ListAtomType();
    //! add id to list, return false if id is not in the stringcache
    bool append(const char* id);

    //! begin UtString iteration
    const char* beginIter();
    
    //! Get the next list item
    const char* next();

  private:
    HdlHierPath::StrAtomVec* mList;
    const AtomicCache* mStrCache;
    HdlHierPath::StrAtomVec::const_iterator mIter;

    // helper
    const char* getIterStr() const;

    ListAtomType();
    ListAtomType(const ListAtomType&);
    ListAtomType& operator=(const ListAtomType&);
  };


  // helper functions
  Status decompPathToList(ListType& theList, const char* path, HdlId* info) const;
  const char* compPathFromList(ListType& theList, UtString* buffer, 
                               const HdlId* info) const;

  const char* compPathFromHierName(const HierName* name, UtString* buffer) const;
  void addInfo(const HdlId* info, UtString* buffer) const;

  bool substrToken(const char** pathStr,
                   const char* endPtr,
                   HdlId* info,
                   Status* stat,
                   UtString* token) const;
    
  void composeScalar(UtString* out, const char* in) const;

  //! extracts a single bitselect/partselect from a string and updates the HdlId
  Status parseBrackets(const char** s, const char* endPtr, HdlId* info) const;

};  // class HdlVerilogPath : public HdlHierPath
#endif
