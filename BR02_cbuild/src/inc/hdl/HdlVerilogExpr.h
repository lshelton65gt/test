// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION

  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING AND/OR
  DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN
  CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _HDLVERILOGEXPR_H_
#define _HDLVERILOGEXPR_H_


#include "hdl/HdlExpr.h"

//! Object that parses Verilog expressions
/*!
 * This currently only handles various Verilog constant descriptions
 * and converts them to a dynamic bitvector.
 */
class HdlVerilogExpr : public HdlExpr
{
public: CARBONMEM_OVERRIDES
  //! Constructor
  HdlVerilogExpr();

  //! Destructor
  virtual ~HdlVerilogExpr();

  //! Parses a string for a Verilog constant and fills in a DynBitVector
  /*! This routine parses strings of the form "32'h1234abcd" that
   *  match Verilog constant syntax. It fills in a dynamic bit vector
   *  with the value.
   *
   *  *NOTE*: This routine only supports hex and binary right now
   *
   *  \param constStr - a string in Verilog syntax
   *
   *  \param value - a dynamic bitvector that is filled with the
   *  value. Note that this routine will resize the dynamic bit vector
   *  so that it has the space.
   *
   *  \param errMsg - a string to be filled in with an appropriate
   *  message if there is a problem parsing the constant. This is only
   *  filled in if the return value is eWarning or eError.
   *
   *  \returns one of eSuccess, eWarning, or eError. 
   */
  Status parseConst(const char* constStr, DynBitVector& value,
                    UtString* errMsg);

}; // class HdlVerilogExpr

#endif // _HDLVERILOGEXPR_H_
