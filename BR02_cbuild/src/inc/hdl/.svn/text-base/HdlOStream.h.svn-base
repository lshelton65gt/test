// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file 
  Classes releated to file operations used within VHDL/verilog simulations.
  Such as for $fopen ,$fdisplay, FILE_OPEN and WRITE.  Includes support for 
  multi channel descriptors.
*/

#ifndef __HdlOStream_h_
#define __HdlOStream_h_


#ifndef __BitVector_h_
#include "util/BitVector.h"
#endif
#ifndef __UtCachedFileSystem_h_
#include "util/UtCachedFileSystem.h"
#endif
#ifndef __HdlFileSystem_h_
#include "hdl/HdlFileSystem.h"
#endif
#ifndef __UtIOStream_h_
#include "util/UtIOStream.h"
#endif
#ifndef __UtHashMap_h_
#include "util/UtHashMap.h"
#endif
#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif

class UtIOStringStream;
class MsgContext;

//! a collection of common information about files used for output in VHDL/verilog
/*
  derived from UtOStream so that we can benefit from the operator<<
  definitions.

  there are multiple sets of files that this keeps track of
  \li files that are standard (stdout, stderr)
  \li Files that have been opened for output with $fopen
  \li Files that have been setup for output during next operation (by call to enableTargets)
 */
class HdlOStream: public UtOStream
{
public: CARBONMEM_OVERRIDES
  //! Constructor, 
  HdlOStream(HDLFileSystem * hdl_file_system, MsgContext* msgContext);
  //! destructor
  virtual ~HdlOStream();



  //! returns true if any file managed by HdlOStream is open
  bool is_open() const;

  //! flush all files managed by HdlFileSystem (and stdout, stderr)
  bool flush() {  return mHDLFileSystem->flush(); }

  //! flush all files specified by \a fileDescriptor
  /*! this will flush stdout and stderr if included in the \a
   * fileDescriptor
   */
  bool flush(UInt32 fileDescriptor);

  //! close all non-reserved files managed by HdlFileSystem
  bool close() {  return mHDLFileSystem->close(); }

  //! close all the files specified by \a fileDescriptor
  /*! this will close stdout and stderr if included in the \a
   * fileDescriptor
   */
  bool close(UInt32 fileDescriptor);
    
  //! send buf to the outputStream(s) that have been setup with putTargetFileDescriptor
  virtual bool write(const char* buf, UInt32 len);

  //! \brief set the files defined by \a descriptor to be the current output files,
  // the next output operation to HdlOStream will send data
  // to these/this file.
  void putTargetFileDescriptor( UInt32 descriptor );

  //! write the StringStream asociated with descriptor to the File.
  void writeLine( UtIOStringStream* str_stream);

  //! Open a file for output, returns status with file_descriptor
  UInt32 HdlOFileOpen( UInt2* status, const char* filename, const char* mode);

  //! Open a file for output, returns the file descriptor.
  UInt32 HdlOFileOpen(const char* filename, const char* mode);

  //! Open a file for output, returns the MCD (multi channel descriptor)
  UInt32 HdlOFileOpen(const char* filename);

  //! Return handle to msgContext
  MsgContext * getMsgContext (void) {return mMsgContext;}

  //! Return handle to hdlFileSystem
  HDLFileSystem* getHDLFileSystem (void) {return  mHDLFileSystem;}

  //! typedef for file descriptors, we use UtOStream since they may be UtIO:cout or UtOCstream
  typedef UtArray<UtOStream *> TargetFileDescriptors;
  //! typedef for map from file descriptors to streams, we use UtOStream since they may be either UtIO:{cout,cerr} or UtOCstream
  typedef UtHashMap<UInt32, UtOStream *> FDtoStreamMap;



protected:
  //! the file system used for this design
  HDLFileSystem*  mHDLFileSystem;


  //! the descriptor that will be used for the next write operation
  /* this descriptor may be either a MCD or a FD, if a MCD and
     multiple bits were specified then the output will go to multiple
     streams.
  */
  UInt32 mTargetFileDescriptor;

  //! zero or more UtOStreams, it is always in sync with mTargetFileDescriptor
  /*
    This set of streams are the set that will be written to during the
    next output operation.  Always in sync with mTargetFileDescriptor
   */
  TargetFileDescriptors mTargetFileDescriptors;

  //! the MSB of a 32 bit file descriptor distinguishes between MCD and standard FD (if MSB is 0 then you have a MCD)
  static const UInt32 sNonMCDFlagBit = (1U<<31);

  enum reservedFD { eMCDstdin=0,        //!< tradition (not LRM) says this is MCD stdin 
                    eMCDstdout=1,	//!< tradition (not LRM) says this is MCD stdout 
                    eFDstdin=(sNonMCDFlagBit|0),  //!< LRM says x80000000 is reserved for STDIN
                    eFDstdout=(sNonMCDFlagBit|1), //!< LRM says x80000001 is reserved for STDOUT
                    eFDstderr=(sNonMCDFlagBit|2)  //!< LRM says x80000002 is reserved for STDERR
  };

  //! Message Context
  MsgContext* mMsgContext;

};
#endif // __HdlOStream_h_
