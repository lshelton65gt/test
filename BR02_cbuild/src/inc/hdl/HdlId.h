// -*- C++ -*-                
/*****************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __HDLID_H_
#define __HDLID_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#include <cstring>

class ConstantRange;

//! HDL Identifier object
/*!
  This helps describe an identifier within a hierarchical path.
*/
class HdlId 
{
public: 
  CARBONMEM_OVERRIDES
  
  //! Default constructor.
  /*!
    This defaults to a scalar
  */
  HdlId()
  {
    mType = eScalar;
    mVect.uRange.stMSB = 0;
    mVect.uRange.stLSB = 0;    
    mAllowIndices = false;
    mIndices = NULL;
    mIndicesAllocated = 0;
  }
  
  //! Constructor for vector bit reference
  HdlId(SInt32 bitRef) 
  {
    mType = eVectBit;
    
    // for consistent hashing make sure the entire union is
    // initialized
    mVect.uRange.stMSB = 0;
    mVect.uRange.stLSB = 0;    
    
    // now set the bitref
    mVect.uIndex = bitRef;

    mAllowIndices = false;
    mIndices = NULL;
    mIndicesAllocated = 0;
  }
  
  //! Constructor for vector range
  HdlId(SInt32 msb, SInt32 lsb)
  {
    mType = eVectBitRange;
    mVect.uRange.stMSB = msb;
    mVect.uRange.stLSB = lsb;

    mAllowIndices = false;
    mIndices = NULL;
    mIndicesAllocated = 0;
  }

  //! Copy constructor
  HdlId(const HdlId& hdlId)
  {
    *this = hdlId;
  }

  //! Assignment operator
  HdlId& operator=(const HdlId& hdlId)
  {
    if (this == &hdlId)
      return *this;

    mType = hdlId.getType();

    mIndices = NULL;
    mIndicesAllocated = hdlId.mIndicesAllocated;
    if (mIndicesAllocated != 0) {
      // Need to allocate and copy the other index array
      mIndices = CARBON_ALLOC_VEC(SInt32, mIndicesAllocated);
      memcpy(mIndices, hdlId.mIndices, mIndicesAllocated * sizeof(SInt32));
    }

    mAllowIndices = hdlId.getAllowIndices();

    switch(mType)
    {
    case eScalar:
      mVect.uRange.stMSB = 0;
      mVect.uRange.stLSB = 0;
      break;
    case eVectBit:
      mVect.uRange.stMSB = 0;
      mVect.uRange.stLSB = 0; 
      mVect.uIndex = hdlId.getVectIndex();
      break;
    case eVectBitRange:
      mVect.uRange.stMSB = hdlId.getVectMSB();
      mVect.uRange.stLSB = hdlId.getVectLSB();
      break;
    case eArrayIndex:
      mVect.uRange.stMSB = 0;
      mVect.uRange.stLSB = 0; 
      mVect.uNumDims = hdlId.getNumDims();
      break;
    case eInvalid:
      break;
    }

    return *this;
  }
  
  //! Destructor
  ~HdlId()
  {
    // Need to free array if allocated.  We can't just check whether
    // this is an array index type, because the same HdlId may be
    // reused, which may change its type without actually freeing the
    // array.
    if (mIndices != NULL) {
      CARBON_FREE_VEC(mIndices, SInt32, mIndicesAllocated);
    }
  }
  
  //! Identifier type enumeration
  enum Type {
    eVectBitRange, //!< Vector bit range
    eVectBit, //!< Vector bit reference
    eScalar, //!< Scalar identifier
    eArrayIndex, //!< Array index/indices
    eInvalid //!< Invalid identifier
  };
  
  //! Return the identifier type 
  inline Type getType() const { return mType; }
  //! If this is a vector bit reference, return the index
  inline SInt32 getVectIndex() const { return mVect.uIndex; }
  //! Return the vector LSB for bit range
  inline SInt32 getVectLSB() const { return mVect.uRange.stLSB; }
  //! Return the vector MSB for bit range
  inline SInt32 getVectMSB() const { return mVect.uRange.stMSB; }
  //! Return whether indices are allowed in the last token
  inline bool getAllowIndices() const { return mAllowIndices; }
  
  //! Set the type
  inline void setType(Type type) { mType = type; }
  //! Set the vector index
  inline void setVectIndex(SInt32 index) { mVect.uIndex = index; }
  //! Set the vector LSB
  inline void setVectLSB(SInt32 lsb) { mVect.uRange.stLSB = lsb; }
  //! Set the vector MSB
  inline void setVectMSB(SInt32 msb) { mVect.uRange.stMSB = msb; }
  //! Set whether indices are allowed in the last token
  inline void setAllowIndices(bool allow) { mAllowIndices = allow; }
  
  //! Set vector components given a ConstantRange
  void setVectorParams(const ConstantRange& range);
  
  //! Add an array dimension to a VectBit or ArrayIndex type
  void addDimension(SInt32 index);
  //! For array indices, return the number of dimensions
  inline UInt32 getNumDims() const { return mVect.uNumDims; }
  //! For array indices, return the index for a dimension
  inline SInt32 getDim(UInt32 index) const { return mIndices[index]; }


  //! Get the width
  inline UInt32 getWidth() const {
    UInt32 width = 0;
    if ((mType == eScalar) || (mType == eVectBit) || (mType == eArrayIndex))
      width = 1;
    else
    {
      SInt32 diff = mVect.uRange.stMSB - mVect.uRange.stLSB;
      if (diff < 0)
        diff = -diff;
      ++diff;
      width = UInt32(diff);
    }
    return width;
  }
  
  //! Does the id refer to a vector bit or partsel?
  inline bool hasVectorSemantics() const
  {
    return (mType == eVectBit) || (mType == eVectBitRange)|| (mType == eArrayIndex);
  }
  
  //! Hash 
  size_t hash() const {
    return ((size_t) mType) + (mVect.uRange.stMSB ^ mVect.uRange.stLSB);
  }
  
  //! equality operator
  bool operator==(const HdlId& id) const
  {
    int cmp = mType - id.mType;
    if (cmp == 0)
    {
      if (mType == eVectBit)
        cmp = getVectIndex() - id.getVectIndex();
      else if (mType == eVectBitRange)
      {
        cmp = getVectLSB() - id.getVectLSB();
        if (cmp == 0)
          cmp = getVectMSB() - id.getVectMSB();
      }
      else if (mType == eArrayIndex)
      {
        cmp = getNumDims() - id.getNumDims();
        for (UInt32 i = 0; (cmp == 0) && (i < getNumDims()); ++i) {
          cmp = getDim(i) - id.getDim(i);
        }
      }
      else
        cmp = 0; // scalar has no vector info.
    }
    return (cmp == 0);
  }

  //! Returns -1 if <, 0 if ==, 1 if >
  static int compare(const HdlId* a, const HdlId* b);

#if !pfSPARCWORKS			// workaround Forte' 7 bug
private:
#endif
  //! A structure to indicate vector range
  struct Range {
    //! Least significant bit
    SInt32 stLSB;
    //! Most significant bit
    SInt32 stMSB;
  };
  
  //! A vector specification
  union VectSpec {
    //! Range for eVectBitRange
    Range uRange;
    //! Bit reference for eVectBit
    SInt32 uIndex;
    //! Number of dimensions for eArrayIndex
    UInt32 uNumDims;
  };
  
private:
  Type mType;
  VectSpec mVect;

/*!
  This member controls whether terminating brackets should be included
  when parsing a hierarchical path.  In some cases (e.g. looking up a
  signal in a waveform file), they should be excluded -
  "top.mysig[31:0]" should be parsed into "top.mysig" so that each
  token matches the StringAtom stored in the symbol table.  This is
  the default behavior.

  However, when looking up nodes/nets through the DB API and C API,
  the ending brackets need to be included.  "top.my2darray",
  "top.my2darray[1]", and "top.my2darray[1][2]" are very different
  entities!

  Note that non-terminal braces
  (e.g. "top.arrayedModuleInstance[1].mysig") are always preserved.
*/
  bool mAllowIndices;


  //! Pointer to array indices represented by this object
  /*!
    This is only allocated if the type is eArrayIndex
   */
  SInt32 *mIndices;
  //! Allocated size of array indices
  UInt32 mIndicesAllocated;
};

//! HdlId factory class
class HdlIdFactory
{
public:
  CARBONMEM_OVERRIDES

  //! constructor
  HdlIdFactory();
  
  //! destructor
  ~HdlIdFactory();
  
  const HdlId* getHdlId(const HdlId& id);
private:
  class Helper;
  Helper* mHelper;
};

#endif
