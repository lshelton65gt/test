// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  A class to open and parse a readmemX file (binary or hexadecimal).
*/

#ifndef _READMEMX_H_
#define _READMEMX_H_


#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif

//! Return value abstractor for HDLReadMemX::getNextWord
enum HDLReadMemXResult
{
  eRMValidData,		//!< Valid data found and returned
  eRMEnd,		//!< End of file encountered
  eRMSyntaxError,	//!< A syntax error occured while parsing the file
  eRMInvalidData,	//!< Invalid data in a number
  eRMAddressOutOfRange	//!< Address out of range for data
};


//! HDLReadMemX class used to open, parse, and close $readmemX files.
class HDLReadMemX
{
public: CARBONMEM_OVERRIDES
  //! constructor for memory of known dimensions
  /*! Creates an object to parse readmemX files and return the data
   *  one line at a time.
   *
   *  \param fileName The name of the file to read.
   *
   *  \param hexFormat If set it is readmemh, otherwise it is
   *  readmemb.
   *
   *  \param bitWidth The expected bit width of the memory.
   *
   *  \param startAddress The start address range for the memory to be
   *  loaded. This should either come from the memory or from the
   *  $readmemx task (task takes priorty)
   *
   *  \param endAddress The end address range for the memory to be
   *  loaded. The same rules apply as for startAddress.
   *
   *  \param checkComplete If set, we should check if the complete
   *  range specified is loaded or not. According to the Verilog spec
   *  this should only be checked if the range was specified in the
   *  task. If the range is not specified and the memory range is
   *  used, then checking should not occur. Checking also does not
   *  occur if there is an address specified in the file.
   *
   *  \param instance Pointer to the HDL instance that owns this
   *  object. This is used to get the MsgContext in error messaging.
   */
  HDLReadMemX(const char* fileName, bool hexFormat, UInt32 bitWidth,
	      SInt64 startAddress, SInt64 endAddress, bool checkComplete,
              CarbonOpaque instance);

  //! constructor for memory with unknown depth
  /*! Creates an object to parse readmemX files and return the data
   *  one line at a time.
   *
   *  \param fileName The name of the file to read.
   *
   *  \param hexFormat If set it is readmemh, otherwise it is
   *  readmemb.
   *
   *  \param bitWidth The expected bit width of the memory.
   *
   *  \param isDecreasing If the addresses decrease instead of
   *  increase (the default) then this should be true.
   *
   *  \param instance Pointer to an HDL instance. This is used to get
   *  the MsgContext in error messaging.
   */
  HDLReadMemX(const char* fileName, bool hexFormat, UInt32 bitWidth,
	      bool isDecreasing, CarbonOpaque instance);

  //! destructor
  ~HDLReadMemX();

  //! open the file to prepare for parsing
  bool openFile();

  //! close the file, we are done
  void closeFile();

  //! Get the start address (for the range)
  SInt64 getStartAddress() const { return mStartAddress; }

  //! Get the end address (for the range)
  SInt64 getEndAddress() const { return mEndAddress; }

  //! Get the width of a row
  UInt32 getRowWidth() const { return mBitWidth; }

  //! Get the filename
  const char* getFileName() const;

  //! Get the next memory address and row
  /*! Gets the next row of memory and its address if successful. If it
   *  fails then it prints a message.
   *
   *  \warning Do not call this if this class was instantiated with
   *  the unknown depth constructor
   *
   *  \param address A pointer to a place to store the address
   *
   *  \param data A pointer to an array in which to store the
   *  data. The ascii numbers are stored from left to right in
   *  indices 0, 1, ... N of the array. The caller must allocate
   *  sufficient number of Uint32 words to store the expected number
   *  of bits.
   *
   *  \returns One of eRMValidData, eRMError, or eRMEnd. For
   *  eRMValidData the address and data parameters contain valid
   *  memory data. For eRMError the lineNum parameter contains the
   *  line number of the failure. For eRMEnd the end of file was
   *  encountered.
   */
  HDLReadMemXResult getNextWord(SInt64* address, UInt32* data);

  //! Get the next memory address and row for unknown depth memory
  /*!
   *  \warning This should only be used if this class was instantiated
   *  with the unknown depth constructor.
   *
   *  \param address A pointer to a place to store the address
   *
   *  \param data A pointer to an array in which to store the
   *  data. The ascii numbers are stored from left to right in
   *  indices 0, 1, ... N of the array. The caller must allocate
   *  sufficient number of Uint32 words to store the expected number
   *  of bits.
   *
   *  \returns One of eRMValidData, eRMError, or eRMEnd. For
   *  eRMValidData the address and data parameters contain valid
   *  memory data. For eRMError the lineNum parameter contains the
   *  line number of the failure. For eRMEnd the end of file was
   *  encountered.
   *
  */
  HDLReadMemXResult getNextRowNoDepth(SInt64* address, UInt32* data);
private:
  // class to hide some data types
  class AuxLex;
  friend class AuxLex;

  AuxLex* mAux;

  // Place to store data for return to the caller.
  UInt32* mData;
  SInt64 mAddress;
  SInt64 mStartAddress;
  SInt64 mEndAddress;
  SInt32 mDirection;

  // Information about the memory and file
  UInt32 mBitWidth;
  bool mHexFormat;

  // Used to keep track of the addresses we have parsed for
  UInt32 mDepth;
  UInt32 mRemainingCount;
  CarbonOpaque mInstance;
  bool mCheckComplete;
  bool mNextReadOverflows;

  void init(const char* fileName, 
            bool hexFormat, UInt32 bitWidth,
            SInt64 startAddress, SInt64 endAddress, 
            bool checkComplete,
            CarbonOpaque instance);

  HDLReadMemXResult readNextRow(UInt32* data);
};

#endif // _READMEMX_H_
