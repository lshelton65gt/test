// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file 
  Classes releated to the basic file operations used within HDL
  simulations of any language, includes the tracking of file descriptors.
*/

#ifndef __HdlFileSystem_h_
#define __HdlFileSystem_h_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#ifndef __BitVector_h_
#include "util/BitVector.h"
#endif
#ifndef __UtCachedFileSystem_h_
#include "util/UtCachedFileSystem.h"
#endif
#ifndef __UtIStream_h_
#include "util/UtIStream.h"
#endif
#ifndef __UtIOStream_h_
#include "util/UtIOStream.h"
#endif
#ifndef __UtIOStringStream_h_
#include "util/UtIOStringStream.h"
#endif
#ifndef __UtHashMap_h_
#include "util/UtHashMap.h"
#endif
#ifndef __UtHashSet_h_
#include "util/UtHashSet.h"
#endif

class MsgContext;
class UtOCheckpointStream;
class UtICheckpointStream;

//! all Verilog file descriptors fit into a UInt32, what about VHDL?
typedef UInt32 HDLFD;

//! a collection of information about files that are used in hdl model simulation
/*
  This class knows about some reserved sets of files for verilog and
  vhdl, and keeps track of all files that have been opened.

  there are multiple sets of files that this keeps track of
  \li files that are standard (stdout, stderr)
  \li Files that have been opened for output with $fopen
 */
class HDLFileSystem
{
public: CARBONMEM_OVERRIDES
  //! Constructor, 
  HDLFileSystem(MsgContext* msgContext);
  //! destructor
  virtual ~HDLFileSystem();


  //! Open a file and return the HDL file Descriptor
  /*!
    \param status returns the status for vhdl FILE_OPEN
    \param filename name of file
    \param mode: r rw ...
    \param isMCD true if the descriptor must follow the verilog MCD style
    
    \return a HDL file descriptor, used by other HDLFileSystem
     methods, or zero if unable to open file
   */
  HDLFD HdlFileOpen(UInt2* status, const char* filename, const char* mode,
                    bool isWritable, bool isMCD);

  HDLFD HdlFileOpen(const char* filename, const char* mode, 
                    bool isWritable, bool isMCD);
  
  //! close all files managed by HdlFileSystem (except stdout stderr)
  bool close();

  //! close the file associated with \a fileDescriptor (a no-op if fileDescriptor is for stdin, stdout or stderr)
  bool close(HDLFD fileDescriptor);

  //! returns true if any file managed by HdlFileSystem is open
  bool is_open() const;

  //! flush all files managed by HDLFileSystem (and stdout, stderr)
  bool flush();

  //! flush the file associated with \a fileDescriptor
  bool flush(HDLFD fileDescriptor);


  //! locate the UtOStream associdated with \a fd, if found it is put into \a oStream,
  /*! \retval true when a stream was found, a pointer to the stream is
                   placed in \a oStream.
      \retval false when no stream is associated with \a fd.  \a
                   oStream is unchanged.
   */
  bool getHdlFileStream(UtOStream** oStream, HDLFD fd);

  //! locate the UtIStream associdated with \a fd, if found it is put into \a iStream,
  /*! \retval true when a stream was found, a pointer to the stream is
                   placed in \a iStream.
      \retval false when no stream is associated with \a fd.  \a
                   iStream is unchanged.
   */
  bool getHdlFileStream(UtIStream** iStream, HDLFD fd);

  //! set of streams associated with a file descriptor, need both an
  /*! input and output since the user might have opened a single fd for
   * both input and output.
   */

  //! Return handle to msgContext
  MsgContext * getMsgContext (void) {return mMsgContext;}

  class HDLStreamInfo
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    HDLStreamInfo(UtOStream* out_stream, UtIStream* in_stream) :
      mOutStream(out_stream), mInStream(in_stream) {}
    //! destructor
    ~HDLStreamInfo() {}

    // we use UtOStream since they may be either UtIO:{cout,cerr} or UtOCstream
    UtOStream* mOutStream;
    // we use UtIStream since they may be either UtIO:{cin} or UtICstream
    UtIStream* mInStream;
  };
  //! typedef for map from file descriptors to stream info,
  typedef UtHashMap<HDLFD, HDLStreamInfo> FDtoStreamMap;

  //! returns true if \a filedescriptor is one of the pre-reserved values
  bool isReservedFD(HDLFD fileDescriptor)
  {
    return (mFDReservedSet.find(fileDescriptor) != mFDReservedSet.end());
  }
  
  //! reserve a file descriptor, and associate it with stream(s)
  bool reserveFileDescriptor(HDLFD descriptor, UtIStream * in_stream, UtOStream * out_stream);

  //! undo the reserveation of a file descriptor, and un associate it with its stream(s)
  bool unReserveFileDescriptor(HDLFD descriptor);

  //! save state to checkpoint
  bool save(UtOCheckpointStream &out);

  //! restore state from checkpoint
  bool restore(UtICheckpointStream &in);

private:
  //! map of HDLFD (file descriptors) to stream info struct,
  FDtoStreamMap mFDtoStreamMap;

  typedef UtHashSet<HDLFD> HDLFDSet;
  HDLFDSet mFDReservedSet;

  
  // HDLFileSystem owns a cached file system that it uses to distribute streams upon file opens
  UtCachedFileSystem*  mCachedFileSystem;


  //! support for HDLFileOpen, 
  /* this is where the file is opened and insterted in mFDtoStreamMap
   */
  bool HDLFileOpenHelper (const char* filename, HDLFD descriptor,
                          bool isWritable, const char* mode);

  //! internal method to close a stream(no checking for validity of stream), returns true if ok
  bool closeAStream(UtOStream* stream);
  bool closeAStream(UtIStream* stream);

  //! internal method to flush a stream(no checking for validity of stream), returns true if ok
  bool flushAStream(UtOStream* stream);

  //! Message Context
  MsgContext* mMsgContext;

};
#endif // __HdlFileSystem_h_
