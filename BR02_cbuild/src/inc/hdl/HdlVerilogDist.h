// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __HdlVerilogDist_h_
#define __HdlVerilogDist_h_


#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif


/*!
  \file
  This file contains the standard Verilog probalistic distribution
  functions as specified by IEEE Standard 1364-2001.
*/


//! HdlVerilogDist namespace
/*!
  Contains the standard Verilog routines for generating probablistic values.

  The implementations of these functions (except where noted) were taken
  directly from the IEEE Standard 1364-2001 and carbonized.  DO NOT CHANGE
  THESE IMPLEMENTATIONS.

  Documentation for these functions is parroted from the spec in a hope
  that they are generally useful.  Please excuse the several gaps.
*/
namespace HdlVerilogDist
{
  //! Get a value using the Chi Square distrubution.
  /*!
    This function returns a new 32-bit random number each time it
    is called using the Chi Square distribution.  The random number
    is signed; it can be positive or negative.

    \param seed This controls the numbers that the method returns such that
    different seeds generate different streams.  This function will always
    return the same value given the same seed.  The function will update this
    parameter with a new seed.

    \param df This controls the degree of freedom.  The larger the values
    will spread the numbers returned over a larger range.  This must be
    greater than 0.

    \returns A signed 32 bit integer value.

    \note  The code provided in the standard looks borken.  Specifically,
    it looks like there was a cut and paste error between the Exponential
    function and this one.  An alternative was located on the internet and
    substituted.
   */
  SInt32 ChiSquare(SInt32 *seed, SInt32 df);


  //! Get a value using the Erlangian distrubution.
  /*!
    This function returns a new 32-bit random number each time it
    is called using the Erlangian distribution.  The random number
    is signed; it can be positive or negative.

    \param seed This controls the numbers that the method returns such that
    different seeds generate different streams.  This function will always
    return the same value given the same seed.  The function will update this
    parameter with a new seed.

    \param k This must be greater than 0.

    \param mean This causes the value returned by the function to approach
    the value specified.

    \returns A signed 32 bit integer value.
   */
  SInt32 Erlang(SInt32 *seed, SInt32 k, SInt32 mean);


  //! Get a value using the Exponential distrubution.
  /*!
    This function returns a new 32-bit random number each time it
    is called using the Exponential distribution.  The random number
    is signed; it can be positive or negative.

    \param seed This controls the numbers that the method returns such that
    different seeds generate different streams.  This function will always
    return the same value given the same seed.  The function will update this
    parameter with a new seed.

    \param mean This causes the value returned by the function to approach
    the value specified.

    \returns A signed 32 bit integer value.
   */
  SInt32 Exponential(SInt32 *seed, SInt32 mean);


  //! Get a value using the Normal distrubution.
  /*!
    This function returns a new 32-bit random number each time it
    is called using the Normal distribution.  The random number
    is signed; it can be positive or negative.

    \param seed This controls the numbers that the method returns such that
    different seeds generate different streams.  This function will always
    return the same value given the same seed.  The function will update this
    parameter with a new seed.

    \param mean This causes the value returned by the function to approach
    the value specified.

    \param sd Standard deviation.  The larger the value, the more widely
    distributed the values.

    \returns A signed 32 bit integer value.
   */
  SInt32 Normal(SInt32 *seed, SInt32 mean, SInt32 sd);


  //! Get a value using the Poisson distrubution.
  /*!
    This function returns a new 32-bit random number each time it
    is called using the Poisson distribution.  The random number
    is signed; it can be positive or negative.

    \param seed This controls the numbers that the method returns such that
    different seeds generate different streams.  This function will always
    return the same value given the same seed.  The function will update this
    parameter with a new seed.

    \param mean This causes the value returned by the function to approach
    the value specified.

    \returns A signed 32 bit integer value.
   */
  SInt32 Poisson(SInt32 *seed, SInt32 mean);


  //! Get a value using the T distrubution.
  /*!
    This function returns a new 32-bit random number each time it
    is called using the T distribution.  The random number
    is signed; it can be positive or negative.

    \param seed This controls the numbers that the method returns such that
    different seeds generate different streams.  This function will always
    return the same value given the same seed.  The function will update this
    parameter with a new seed.

    \param df This controls the degree of freedom.  The larger the values
    will spread the numbers returned over a larger range.  This must be
    greater than 0.

    \returns A signed 32 bit integer value.
   */
  SInt32 T(SInt32 *seed, SInt32 df);


  //! Get a value using the Uniform distrubution.
  /*!
    This function returns a new 32-bit random number each time it
    is called using the Uniform distribution.  The random number
    is signed; it can be positive or negative.

    \param seed This controls the numbers that the method returns such that
    different seeds generate different streams.  This function will always
    return the same value given the same seed.  The function will update this
    parameter with a new seed.

    \param start Upper bound of the values returned.

    \param end Lower bound of the values returned.

    \returns A signed 32 bit integer value.
   */
  SInt32 Uniform(SInt32 *seed, SInt32 start, SInt32 end);


  //! Get a value using the Uniform distrubution.
  /*!
    This function returns a new 32-bit random number each time it
    is called using the Uniform distribution.  The random number
    is signed; it can be positive or negative.

    This function is equvalent to:
    \code
        Uniform(seed, UtSINT32_MIN, UtSINT32_MAX);
    \endcode

    \returns A signed 32 bit integer value.
   */
  SInt32 Random(SInt32 *seed);

  // private:
  double InternalUniform(SInt32 *seed, SInt32 start, SInt32 end);
  double InternalNormal(SInt32 *seed, SInt32 mean, SInt32 deviation);
  double InternalExponential(SInt32 *seed, SInt32 mean);
  SInt32 InternalPoisson(SInt32 *seed, SInt32 mean);
  double InternalChiSquare(SInt32 *seed, SInt32 deg_of_free);
  double InternalT(SInt32 *seed, SInt32 deg_of_free);
  double InternalErlangian(SInt32 *seed, SInt32 k, SInt32 mean);
}


#endif
