// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef __BDD_H_
#define __BDD_H_

#include <cstddef>
#ifndef NUCLEUS_H_
#include "nucleus/Nucleus.h"
#endif
#ifndef __STBRANCHNODE_H_
#include "symtab/STBranchNode.h"
#endif
#ifndef NUEXPR_H_
#include "nucleus/NUExpr.h"     // needed for NUOp::OpT
#endif
#ifndef _UT_ORDER_H_
#include "util/UtOrder.h"       // For carbonPtrCompare()
#endif

/*!
  \file
  BDD Package.

  The purpose of this file is to abstract a specific BDD package; we will most
  likely always use someone else's BDD implementation, but which implementation
  we use may change over time.

  The user of this package should use the BDD and BDDContext classes only.
*/

// if you want to switch to a different bdd package then do so by
// setting ONE of these macros to 1, and the rest to 0.  You must also
// edit the definition of the make variable: BDD_PACKAGE 
// (defined in Makefile.extra)
#define BDD_USE_ATT 1
#define BDD_USE_CUDD 0

#if BDD_USE_ATT
#define BDD_ATT(x) x
#define BDD_CUDD(x)
#endif
#if BDD_USE_CUDD
#define BDD_ATT(x)
#define BDD_CUDD(x) x
#endif

class BDDContext;
class NUExprFactory;
class NUCostContext;

//! A binary decision diagram.
class BDD
{
public:
  //! empty constructor
  BDD() : mRep(0), mBDDContext(0) {}

  //! copy constructor
  BDD(const BDD& bdd);

  //! assign operator
  BDD& operator=(const BDD& bdd);

  //! destructor
  ~BDD();

  //! equality operator
  bool operator==(const BDD& bdd) const
  {
    return mRep == bdd.mRep;
  }

  //! inequality operator
  bool operator!=(const BDD& bdd) const
  {
    return mRep != bdd.mRep;
  }

  //! less-than operator
  bool operator<(const BDD& bdd) const
  {
    return mRep < bdd.mRep;
  }

  //! hash value
  size_t hash() const
  {
    return (size_t)mRep;
  }

  //! a printed representation of BDD
  void print(FILE*);
  
  //! Is this a valid BDD
  bool isValid() const { return (mRep != 0); }

  //! Compose a textual representation of the BDD
  void compose(UtString* str, bool escapeNewlines = false);

private:
  //! Make BDDContext a friend so it can get at our base representation.
  friend class BDDContext;

  //! From Cudd BDD library - do not use this, it will change if we use a different package.
#if BDD_USE_CUDD
  typedef struct DdNode *__bdd;
#endif
#if BDD_USE_ATT
  typedef struct bdd_ *__bdd;
#endif

  //! constructor
  BDD(__bdd rep, BDDContext*);

  //! Return our representation.
  __bdd rep() const { return mRep; }

  //! Actual BDD Representation
  __bdd mRep;

  BDDContext* mBDDContext;
};


//! A base class to unify the various constructs that can become BDDs.
/*! They are currently strings, NUNets, and NUNetElabs.
 */
class BDDIdent
{
public:
  //! constructor
  BDDIdent() {}

  //! destructor
  virtual ~BDDIdent() {}

  //! An abstraction for the various BDD identifier types
  enum Type
  {
    eBDDString,
    eBDDNet,
    eBDDNetBit,
    eBDDNetElab,
    eBDDNetElabBit,
    eBDDExpr
  };

  //! A function to figure out the type of this BDD identifier
  virtual Type getType() const = 0;

  //! Make a string that represents this identifier
  virtual const UtString* makeString() const = 0;

  //! A function to get the string pointer if this is a string
  virtual const UtString* getString() const;

  //! A function to get the NUNet* if this is a net
  virtual NUNet* getNet() const;

  //! A function to get the NUNet* and bit if this is a net bit
  virtual NUNet* getNetBit(SInt32*) const;

  //! A function to get the NUNetElab* if this is a net elab
  virtual NUNetElab* getNetElab() const;

  //! A function to get the NUNetElab* and bit if this is a net elab bit
  virtual NUNetElab* getNetElabBit(SInt32*) const;

  //! A function to get the NUExpr* and bit if this is a net elab bit
  virtual const NUExpr* getExpr() const;
};

//! A string BDD identifier
class BDDStringIdent : public BDDIdent
{
public:
  //! constructor
  BDDStringIdent(const UtString& str) : BDDIdent(), mString(str) {}

  //! destructor
  ~BDDStringIdent() {}

  //! A function to figure out the type of this BDD identifier
  virtual BDDIdent::Type getType() const;

  //! Make a string that represents this identifier
  virtual const UtString* makeString() const;

  //! A function to get the string pointer
  virtual const UtString* getString() const;

private:
  UtString mString;
};

//! A net BDD identifier
class BDDNetIdent : public BDDIdent
{
public:
  //! constructor
  BDDNetIdent(NUNet* net) : BDDIdent(), mNet(net) {}

  //! destructor
  ~BDDNetIdent() {}

  //! A function to figure out the type of this BDD identifier
  virtual BDDIdent::Type getType() const;

  //! Make a string that represents this identifier
  virtual const UtString* makeString() const;

  //! A function to get the net
  virtual NUNet* getNet() const;

private:
  NUNet* mNet;
};

//! A net BDD identifier
class BDDNetBitIdent : public BDDIdent
{
public:
  //! constructor
  BDDNetBitIdent(NUNet* net, SInt32 bit) :
    BDDIdent(), mNet(net), mBit(bit)
  {}

  //! destructor
  ~BDDNetBitIdent() {}

  //! A function to figure out the type of this BDD identifier
  virtual BDDIdent::Type getType() const;

  //! Make a string that represents this identifier
  virtual const UtString* makeString() const;

  //! A function to get the net elab bit
  virtual NUNet* getNetBit(SInt32* bit) const;

private:
  NUNet* mNet;
  SInt32 mBit;
};

//! A netElab BDD identifier
class BDDNetElabIdent : public BDDIdent
{
public:
  //! constructor
  BDDNetElabIdent(NUNetElab* netElab) : BDDIdent(), mNetElab(netElab) {}

  //! destructor
  ~BDDNetElabIdent() {}

  //! A function to figure out the type of this BDD identifier
  virtual BDDIdent::Type getType() const;

  //! Make a string that represents this identifier
  virtual const UtString* makeString() const;

  //! A function to get the net elab
  virtual NUNetElab* getNetElab() const;

private:
  NUNetElab* mNetElab;
};

//! An expression BDD identifier
class BDDExprIdent : public BDDIdent
{
public:
  //! constructor
  BDDExprIdent(const NUExpr* expr) :
    BDDIdent(), mExpr(expr)
  {}

  //! destructor
  ~BDDExprIdent() {
  }

  //! A function to figure out the type of this BDD identifier
  virtual BDDIdent::Type getType() const;

  //! Make a string that represents this identifier
  virtual const UtString* makeString() const;

  //! A function to get the expression
  virtual const NUExpr* getExpr() const;

private:
  const NUExpr* mExpr;
};

//! A netElab BDD identifier
class BDDNetElabBitIdent : public BDDIdent
{
public:
  //! constructor
  BDDNetElabBitIdent(NUNetElab* netElab, SInt32 bit) :
    BDDIdent(), mNetElab(netElab), mBit(bit)
  {}

  //! destructor
  ~BDDNetElabBitIdent() {}

  //! A function to figure out the type of this BDD identifier
  virtual BDDIdent::Type getType() const;

  //! Make a string that represents this identifier
  virtual const UtString* makeString() const;

  //! A function to get the net elab bit
  virtual NUNetElab* getNetElabBit(SInt32* bit) const;

private:
  NUNetElab* mNetElab;
  SInt32 mBit;
};

//! Context to use BDDs
/*!
 * Maintain a mapping of BDDs to Nucleus objects, and manage memory and
 * creation of bdd's.
 *
 * The invalid() BDD represents a BDD which could not be constructed for some
 * reason (blow up, unsupported, etc).
 *
 * If an invalid() BDD is passed to any of the op functions, an invalid() BDD
 * will result.
 */
class BDDContext
{
public:
  //! constructor
  BDDContext(UInt32 nodeLimit = 5*1000*1000);

  //! Return the BDD for the net.
  /*!
   * If the given net has a BDD assigned-to it, that BDD will be returned.
   * Otherwise a new free variable will be returned.
   */
  BDD bdd(NUNet *net);

  //! Return the BDD for the net bit
  /*!
   * If the given net bit has a BDD assigned-to it, that BDD will be
   * returned. Otherwise a new free variable will be returned.
   */
  BDD bdd(NUNet* net, SInt32 bit);

  //! Return the BDD for the elaborated net.
  /*!
   * If the given elaborated net has a BDD assigned-to it, that BDD will be returned.
   * Otherwise a new free variable will be returned.
   */
  BDD bdd(NUNetElab *net_elab);

  //! Return the BDD for the elaborated net elab bit
  /*!
   * If the given elaborated net bit has a BDD assigned-to it, that
   * BDD will be returned.  Otherwise a new free variable will be
   * returned.
   */
  BDD bdd(NUNetElab* netElab, SInt32 bit);

  //! Return the BDD for the expression.
  /*!
   * If a scope is given, the expression is elaborated to that scope,
   * otherwise the unelaborated expression is used.
   *
   * If the given expression has a BDD assigned-to it, that BDD will be returned.
   * Otherwise a new BDD will be constructed for it.
   */
  BDD bdd(const NUExpr* expr, STBranchNode *scope=0);

  //! Return the BDD for a unique string.
  /*!
   * This function is used to create BDDs from simple string
   * identifiers when we are using the BDD package outside of the
   * nuclues. There is no correlation between these identifiers and
   * the ones created with nucleus objects.
   */
  BDD bdd(const UtString& str);

  //! Function to create a sub bdd for an expression
  /*! Use this function when creating a BDD for a sub expression of an
   *  expression. This routine is more light weight that the bdd(const
   *  NUExpr*) function.
   */
  BDD bddRecurse(const NUExpr* expr, STBranchNode* scope = 0);

  //! Generate a new free variable
  /*! Note that this free variable does not have a name so only use it
   *  if printing is not important.
   */
  BDD var();

  //! Assign the bdd to the net.
  void assign(const BDD& bdd, NUNet *net);

  //! Assign the bdd to the elaborated net.
  void assign(const BDD& bdd, NUNetElab *net_elab);

  //! Assign the bdd to the elaborated net bit
  void assign(const BDD& bdd, NUNetElab* netElab, SInt32 bit);

  //! Assign the bdd to the expression (only when caching)
  void assign(const BDD& bdd, const NUExpr* expr);

  //! Deassign the bdd for the net.
  void deassign(NUNet *net);

  //! Deassign the bdd for the elaborated net.
  void deassign(NUNetElab *net_elab);

  //! Deassign the bdd for the elaborated net bit
  void deassign(NUNetElab* netElab, SInt32 bit);

  //! Return true if the net has a current BDD assignment.
  bool isAssigned(NUNet *net) const;

  //! Return true if the elaborated net has a current BDD assignment.
  bool isAssigned(NUNetElab *net_elab) const;

  //! Return true if the elaborated net bit has a current BDD assignment.
  bool isAssigned(NUNetElab* netElab, SInt32 bit) const;

  //! Unary Not
  BDD opNot(const BDD& expr);

  //! Binary Equality
  BDD opEqual(const BDD& expr1, const BDD& expr2);

  //! Binary And
  BDD opAnd(const BDD& expr1, const BDD& expr2);

  //! Binary Nand
  BDD opNand(const BDD& expr1, const BDD& expr2);

  //! Binary Or
  BDD opOr(const BDD& expr1, const BDD& expr2);

  //! Binary Nor
  BDD opNor(const BDD& expr1,const  BDD& expr2);

  //! Binary Xor
  BDD opXor(const BDD& expr1, const BDD& expr2);

  //! Binary Xnor
  BDD opXnor(const BDD& expr1, const BDD& expr2);

  //! Ternary Conditional
  BDD opCond(const BDD& cond, const BDD& expr1, const BDD& expr2);

  //! Value 0
  BDD val0() const { return mZero; }

  //! Value 1
  BDD val1() const { return mOne; }

  //! Invalid BDD
  BDD invalid() const { return mInvalid; }

  //! Returns the variable at the top of a BDD
  BDD topVariable(const BDD& bdd);

  //! Returns the then variable at the top of a BDD
  BDD thenVariable(const BDD& bdd);

  //! Returns the else variable at the top of a BDD
  BDD elseVariable(const BDD& bdd);

  //! Determine if this BDD is the negation of a variable
  bool isInversion(const BDD& bdd);

  //! Returns true if the condition depends on the value of var
  bool dependsOn(const BDD& condition, const BDD& var);

  //! Returns true if the conclusion is true whenever the assumption is true
  bool implies(const BDD& assumption, const BDD& conclusion);

  //! Fill in pointers to the original unlaborated net for a given BDD variable
  void decodeVariable(const BDD& bdd, NUNet** netPtr, SInt32* bitPtr);

  //! Fill in pointers to the original elaborated net for a given BDD variable
  void decodeVariable(const BDD& bdd, NUNetElab** netPtr, SInt32* bitPtr);

  //! Returns a new BDD with value substituted for var
  BDD substitute(const BDD& bdd, const BDD& var, const BDD& value);

  //! Returns the size of a BDD
  UInt32 size(const BDD& bdd) const;

  //! Returns the number of BDD entries stored in the context
  UInt32 numEntries() const;

  //! Empty all the maps maintained inside the BDD library, releasing BDDs
  void clear();

#if defined(CDB) || defined(BDD_PRINT_DETAIL)
  //! Get the name for a bdd (debug function)
  const UtString* str(const BDD& bdd);
#endif

  //! Prints the bdd (debug function)
  void print(BDD* bdd, FILE*);

  //! Dump the entire BDD context to a file
  void dump(const char* filename);

  //! Compose a textual representation of the BDD
  void compose(UtString* str, BDD* bdd, bool escapeNewlines = true);

  //! Control optional cache for saving BDDs associated with each expression.
  /*! This can improve performance dramatically if you are computing
   *! lots of BDDs through an expression tree.  E.g.
   *!      a & (b & (c & d))
   *!      b & (c & d)
   *!      c & d
   *! With caching off, we must recompute c&d three times.  With
   *! caching on, we would save the result and do a hash lookup
   *! to find the BDD.
   *!
   *! The drawback to this approach is that if you wish to recompute
   *! a BDD on an expression, changing the BDD values for the leaves,
   *! caching will get in the way.  So we have explicit calls to
   *! turn caching on & off.  By default it is off.
   *!
   *! The caller can supply an expression factory, or one will be created
   *! and owned by the BDD context if none is specified.
   *!
   *! Expression caching can be turned on & off & on without losing the cache.
   */
  void putExpressionCaching(bool onOff);

  //! Allow sub-expressions that cannot be BDD'd to be treated as terminals
  /*!
   *! See test/fold/bdd_mixed.v for an example of an expression that benefits
   *! from BDD-folding, even though it contains a sub-piece that cannot be
   *! represented as a BDD.
   *!
   *! Note that BDDs created with this switch are "pessimistic".  For example:
   *!
   *!     wire [7:0] vec;
   *!     (vec == {8{1'b0}}) && (vec == {8{1'b1}})
   *!
   *!      Assume that we cannot represent the vectorized == operator. If I 
   *!     understand your change correctly, this results in a BDD:
   *!
   *!      VAR1 = "vec == {8{1'b0}}"
   *!     VAR2 = "vec == {8{1'b1}}"
   *!     BDD = VAR1 ? (VAR2 ? 1'b1 : 1'b0) : 1'b0
   *!
   *!      The truth table for this looks like:
   *!
   *!              VAR2
   *!              0  1
   *!     VAR1 0   0  0
   *!          1   0  1
   *!
   *!      If the user of this BDD asks if the expression can take value 1, it 
   *!     appears that the answer is "yes". This is incorrect, however, because 
   *!     both VAR1 and VAR2 are not independent variables.
   *!
   *!      You cannot answer satisfiability questions about these expressions 
   *!     without understanding if the selected input path is legal with respect 
   *!     to the chosen variables.
   *!
   *! However, when using BDDs for optimization, this results only in pessimism,
   *! or rather, less optimism than is theoretically possible, but
   *! more optimism than you get when you can't construct a BDD for
   *! the equation at all.
   */
  void putMakeIdentsForInvalidBDDs(bool onOff);

  //! Use BDDs conservatively -- avoid blow-ups
  /*!
   *! If the BDD package is being used for simplifying or finding
   *! constants (e.g. Fold), then we want to avoid trying to compute
   *! BDDs on expressions that are unlikely to simplify.  BDDPrescan
   *! can be used outside this package to limit the use of the BDD
   *! package to situations where it is likely to benefit us.  But
   *! this variable adds the additional protection of making
   *! managerNearingInternalLimits() look at numEntries() and avoid
   *! exceeding the constant 40k (arrived at experimentally).
   */
  void putIsConservative(bool onOff);

  //! Supply an expression factory to use in caching.
  /*!
   *! If none is supplied, BDDContext will make a private one.
   *! This routine must be called before turning on caching.
   */
  void putExpressionFactory(NUExprFactory*);

  //! returns true if the bdd manager is close to exceeding an internal limit
  /*! if this returned true then the caller should reset the manager before trying to use it
   */
  bool managerNearingInternalLimits();

  //! destructor
  ~BDDContext();

  //! Generate an expression from a BDD.  Expression caching must be on.
  /*! \sa putExpressionCaching
   */
  const NUExpr* bddToExpr(const BDD& bdd, const SourceLocator& loc);

  //! Let bddToExpr create muxes for if-then-else BDDs, rather than ?:,
  //! if costs indicate that is the superior choice
  void putCreateMuxes(bool on_off);

  //! Special method for reducing the effectiveness of comparing
  //! expression BDDs, used for -fredo.
  static void cripple();

  //! Open up the floodgates for this context instance
  void openWide();

  //! Return true if expressions are reduced during BDD->Expr conversion
  bool isReduceExpressions() const { return mReduceExpressions; }

  //! Should we reduce expressions during BDD->Expr conversion?
  void putReduceExpressions(bool reduce) { mReduceExpressions = reduce; }

private:
  //! Hide copy and assign constructors.
  BDDContext(const BDDContext&);
  BDDContext& operator=(const BDDContext&);

  //! create a BDD from its package rep
  BDD mkbdd(BDD::__bdd rep) {return BDD(rep, this);}

  //! Either creates or returns a unique identifier for a string
  BDDIdent* getIdent(const UtString& str) const;

  //! Either creates or returns a unique identifier for a net
  BDDIdent* getIdent(NUNet* net) const ;

  //! Either creates or returns a unique identifier for a net bit
  BDDIdent* getIdent(NUNet* net, SInt32 bit) const ;

  //! Either creates or returns a unique identifier for a net elab
  BDDIdent* getIdent(NUNetElab* netElab) const;

  //! Either creates or returns a unique identifier for a net elab bit
  BDDIdent* getIdent(NUNetElab* netElab, SInt32 bit) const;

  //! Either creates or returns a unique identifier for a net elab bit
  BDDIdent* getIdent(const NUExpr* expr) const;

  //! Generic assign for a nucleus object.
  void genericAssign(const BDD& bdd, BDDIdent* ident);

  //! Generic deassign for a nucleus object.
  void genericDeassign(BDDIdent* ident);

  //! Generic isAssigned for a nucleus object.
  bool genericIsAssigned(BDDIdent* ident) const;

  // Generic create BDD routine
  BDD genericCreateBDD(BDDIdent* ident);

  //! Convert an inversion BDD to an unmanaged, unsized NUExpr
  NUExpr* bddInversionToExpr(const BDD& bdd, const SourceLocator& loc);
  
  //! Convert a composite (?:) BDD to an unmanaged, unsized NUExpr
  NUExpr* bddCondOpToExpr(const BDD& bdd, const SourceLocator& loc);

  //! Convert a terminal (var/bitsel) BDD to an unmanaged, unsized NUExpr
  const NUExpr* bddVarToExpr(const BDD& bdd, const SourceLocator& loc);

  //! Synthesize a binary operator based on BDDs, --> unmanaged, unsized
  NUExpr* binaryExpr(NUOp::OpT op,
                     const BDD& bdd1, 
                     const BDD& bdd2,
                     const SourceLocator& loc);

  //! helper for construction -- shared by clear()
  void init();

  //! helper for destruction -- shared by clear();
  void destroy();
  

  //! Function to check the expression cache for a BDD (for performance)
  /*! If caching is enabled then check if we have seen this expression
   *  before. If so, return the previously computed bdd.
   */
  bool isCached(const NUExpr* expr, BDD* retBDD);

  //! Function to save a bdd in the cache
  /*! If caching is enabled, then store this bdd for the given
   *  expression. Note that with caching invalid bdd's are uniquified
   *  so isomorphic expressions compare. So use the return value of
   *  the function for the actual bdd.
   */
  BDD storeCache(const NUExpr* expr, BDD bdd);

#if defined(CDB) || defined(BDD_PRINT_DETAIL)
  //! Used for debugging BDDs
  const UtString* identName(BDDIdent* ident);
#endif

  // A structure to compare idents
  struct CompareIdent
  {
    bool operator()(const BDDIdent* id1, const BDDIdent* id2) const
    {
      BDDIdent::Type t1 = id1->getType();
      BDDIdent::Type t2 = id2->getType();
      ptrdiff_t cmp = (SIntPtr)t1 - (SIntPtr)t2;
      if (cmp == 0)
      {
	// Same type, compare the entries
	switch (t1)
	{
	case BDDIdent::eBDDString:
	  // Compare the strings
	  cmp = id1->getString()->compare(*id2->getString());
	  break;

	case BDDIdent::eBDDNet:
	  // Compare the nets (pointer order is acceptable for now)
          // Difference is in units of sizeof(int)
	  cmp = carbonPtrCompare(id1->getNet(), id2->getNet());
	  break;

	case BDDIdent::eBDDNetBit: 
	  {
	    // Compare the nets (pointer order is acceptable for now)
	    SInt32 bit1, bit2;
	    cmp = carbonPtrCompare(id1->getNetBit(&bit1), id2->getNetBit(&bit2));
	    if (cmp==0)
	      // Same net; check the bits
	      cmp = bit1 - bit2;
	  }
	  break;

	case BDDIdent::eBDDNetElab:
	  // Compare the netelabs (pointer order is acceptable for now)
	  cmp = carbonPtrCompare(id1->getNetElab(), id2->getNetElab());
	  break;

	case BDDIdent::eBDDNetElabBit:
	  {
	    // Compare the net (pointer order is acceptable for now)
	    SInt32 bit1, bit2;
	    cmp = carbonPtrCompare(id1->getNetElabBit(&bit1),
                                   id2->getNetElabBit(&bit2));
	    if (cmp == 0)
	      // Same net elabs, check the bits
	      cmp = bit1 - bit2;
	  }
	  break;

	case BDDIdent::eBDDExpr:
          cmp = id1->getExpr()->compare(*id2->getExpr(), false, false, true);
	  break;

	default:
          INFO_ASSERT(0,"Unknown BDD identifier type");
	  cmp = 0;
	} // switch
      } // if

      return cmp < 0;
    } // bool operator
  }; // struct CompareIdent

  //! From Cudd BDD library - do not use this, it will change if we use a different package.
#if BDD_USE_CUDD
  typedef struct DdManager *__bdd_manager;
#endif
#if BDD_USE_ATT
  typedef struct bdd_manager_ *__bdd_manager;
#endif

  static char* namingFn(BDDContext::__bdd_manager /* manager */, BDD::__bdd bdd, void* env);

  // Type and data to hold all the possible BDD identifiers
  typedef UtSet<BDDIdent*, CompareIdent> BDDIdents;
  mutable BDDIdents* mBDDIdents;

  //! Type to hold Nucleus->BDD mappings
  typedef UtMap<BDDIdent*,BDD> BDDMap;

  //! Maintain mapping Nucleus->BDD
  BDDMap mBDDMap;

#if defined(CDB) || defined(BDD_PRINT_DETAIL)
  // String map for debugging. This gives a name to each BDD Identifier
  typedef UtMap<BDDIdent*, const UtString*> DbgStringMap;
  DbgStringMap* mDbgStringMap;
#endif

  // Reverse map for BDD identifiers for which we create BDDs
  typedef UtMap<BDD, BDDIdent*> ReverseMap;
  ReverseMap* mReverseMap;

  friend class BDD;
  __bdd_manager getManager() {return mManager;} // so bdd's can destruct

  //! BDD manager.
  __bdd_manager mManager;

  // Optional cache for saving BDDs associated with each expression.
  NUExprFactory* mExprFactory;
  NUCostContext* mCostContext;
  bool mOwnFactory;
  bool mExprCaching;
  bool mCreateMuxes;
  bool mIsConservative;   //! avoid running BDDs that are unlikely to simplify

  //! Empirically, test/cust/star/ar/atop finds 28 master clocks and
  // clock analysis runs in 5 seconds.  Increasing this cost limit
  // makes clock analysis take a lot longer but it still winds up
  // with 28 clocks.
  SInt32 mMaxCost;// = 100;

  //! An alternative metric is for expressions with high gate count but
  // low operation count, like test/fold/consteval.v which has
  //      input [149:0] c;
  //	o[46] = ((c === 0) == (~|c));
  // which BDDs to 'true' really quickly.  Probably the uniformity of
  // the expression makes it an easy BDD, despite the fact that, after
  // bit-blasting, there are 150 variables and 300 gates or so.  So even
  // if our max gate-count is exceeded we will try the BDD if there are
  // 8 operations or less
  SInt32 mMaxOpCost;// = 8;

  //! How many extra BDD nodes are we willing to grow with each expression?
  SInt32 mBDDMaxDelta;// = 20000;

  //! The maximum size a bdd can get for any given NUExpr.
  UInt32 mMaxBDDSize;

  //! Should we apply reductions during BDD->Expr transformations?
  bool mReduceExpressions;

  // Should we make arbitrary BDD 'identifiers' for expressions that cannot
  // be broken down into a BDD?  That can be helpful to use BDD algorithms
  // to reduce
  //     d & (a == b) | ~d;
  // to 
  //     (a == b) | ~d;
  // even if a==b is not BDD-able.
  bool mMakeIdentsForInvalidBDDs;

  struct CmpExpr: public HashPointer<const NUExpr*> {
    // override lessThan for sorting
    bool lessThan(const NUExpr* e1, const NUExpr* e2) const {
      return e1->compare(*e2, true, true, true) < 0;
    }
  };

  typedef UtHashMap<const NUExpr*, BDD, CmpExpr> ExprBDDMap;
  ExprBDDMap* mExprBDDMap;
  typedef UtHashMap<BDD, const NUExpr*, HashValue<BDD> > BDDExprMap;
  BDDExprMap* mBDDExprMap;

  // local copy of simple values
  BDD mZero;
  BDD mOne;
  BDD mInvalid;
};

#endif
