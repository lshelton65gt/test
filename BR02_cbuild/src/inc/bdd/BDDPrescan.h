// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#ifndef _BDD_PRESCAN_H_
#define _BDD_PRESCAN_H_

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetRefHashSet.h"
#include "util/UtHashMap.h"
#include "util/UtArray.h"

//! class to analyze an expression and decide if BDDs are likely
//! to find any simplifications.
/*!
 *! My assumption is that BDDs are a win only when one of the
 *! following is true:
 *!   - there is a constant in the expression (other than a varsel index)
 *!   - a bit in a variable is referenced more than once
 *!   - there is more than one inverter (! or ~)
 *!   - there are few enough variables that the BDDs will not blow up
 *! If none of those are true then there is no point creating a bdd.  And
 *! creating a BDD is shockingly expensive for, say, |(a32 & b32).
 *!
 *! The challenge here is that walking an expression is order N, and Fold
 *! wants to compute BDDs on expressions at every level.  So we
 *! must collect the information for sub-expressions and store it in a
 *! map, and use that rather than recursing.
 */
class BDDPrescan : public NUDesignCallback {
  
  //! helper class to keep information about an expression to help
  //! decide if it is worth running BDD analysis on it.  There is
  //! one of these for every NUExpr* in the expression tree.
  class ExprInfo {
  public:
    //! ctor
    ExprInfo(NUNetRefFactory* nrf);

    //! increment the inverter-count
    void addInversion();

    //! indicate a reference to an entire net
    void addNet(NUNet* net);

    //! indicate a reference to a range of bits in a netn
    void addNetRange(NUNet* net, const ConstantRange& range);

    //! aggregate info from a sub-expr
    void addSubExpr(const ExprInfo* subInfo);

    //! guess if a BDD for this sub-expression looks promising
    bool isBDDPromising() const;

    void putHasConstants(bool x) {mHasConstants = x;}

    //! print some trace information about this expression
    void print();

  private:
    SInt32 mInversions;
    bool mBitsRefedMoreThanOnce;
    bool mHasConstants;
    bool mTooManyNets;
    NUNetRefHashSet mRefs;
  }; // class ExprInfo

  //! map from NUExpr to ExprInfo 
  /*!
   *! TBD Review comment: Key should be const NUExpr*.
   */
  typedef UtHashMap<NUExpr*,ExprInfo*> ExprInfoMap;

public:

  //! ctor
  BDDPrescan(NUNetRefFactory*);

  //! dtor
  ~BDDPrescan();

  //! standard override
  Status operator()(Phase, NUBase*);

  //! handle any expr
  Status operator()(Phase phase, NUExpr* expr);

  //! special case handling of NUBinaryOp looking for a negation
  Status operator()(Phase phase, NUBinaryOp* expr);

  //! special case handling of NUUnnaryOp looking for a negation
  Status operator()(Phase phase, NUUnaryOp* expr);

  //! special case handling of an ident looking for a net reference
  Status operator()(Phase phase, NUIdentRvalue* expr);

  //! special case handling of an ident looking for a net bit reference
  Status operator()(Phase phase, NUVarselRvalue* expr);

  Status operator()(Phase phase, NUConst* expr);

  //! public accessor function to allow callers to determine an
  //! expressions viability for BDD analysis
  bool isBDDPromising(const NUExpr* expr);

  //! print some trace information about expr
  void print(const NUExpr* expr, bool show_expression);

  //! Clear the cached prescan information
  void clear();

private:
  //! manage the NUExpr*->ExprInfo map as we descend into the expr tree
  Status pushPop(Phase phase, NUExpr* expr);

  NUNetRefFactory* mNetRefFactory;

  //! This map caches the expression info at every sub-expr in the
  //! expression tree, and is persistent through the lifetime of
  //! this map
  ExprInfoMap mInfoMap;

  //! Array used only during the expression-tree-walk to keep track
  //! of the current stack of ExprInfo structures.
  /*!
   *! This is needed so
   *! that as we pop out of the tree we can aggregate the sub-expr info
   *! into that of the owner expression
   */
  UtArray<ExprInfo*> mInfoArray;

  UInt32 mDepth;

  ExprInfo* mCurrentBDDPrescan;
}; // class BDDPrescan : public NUDesignCallback

#endif
