// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _CEXPR_VALIATOR_H_
#define _CEXPR_VALIATOR_H_

#include "gui/CQt.h"

//! widget to support C expression validatation
/*!
 *! This class allows the user or the programmer to edit C expressions
 *! with a line editor.  At every change the C expressions are analyzed
 *! for syntactic correctness, and any identifiers found are compared
 *! against a list of established valid identifiers.
 *!
 *! Any syntax errors result in the typein field and error text going red.
 *! Any identifiers found that are not in the established valid identifier
 *! set are listed in the error label, and are colored purple.
 */
class CExprValidator : public QObject {
  Q_OBJECT
public:
  //! ctor
  CExprValidator(CQtContext*, const char* name, bool allow_empty);

  //! dtor
  ~CExprValidator();

  //! put the current expression
  void putExpr(const char*);

  //! clear the set of valid identifiers
  void clearIdents();
  
  //! add a valid identifier
  void addIdent(const char*);

  //! Get the top level widget for layout
  QWidget* getWidget() {return mSplitter;}

  //! Get the current error text
  const char* getErrorText() const {return mErrorText.c_str();}

  //! Get the current color
  QColor getColor() const {return mColor;}

  //! line-edit-like setEnabled
  void setEnabled(bool);

  //! line-edit-like setText
  void setText(const char*);

  //! Perform validation on the stored expression and identifier set
  bool validate();

  //! Get the current expression value
  const char* getValue() const {return mValue.c_str();}

public slots:
  void updateText(const QString& str);

signals:
  //! indicate to anyone interested that our value changed
  void textChanged(const QString&);

private:
  QLineEdit* mLineEdit;
  QLineEdit* mErrorWidget;
  QWidget* mSplitter;
  UtString mValue;
  UtString mErrorText;
  UtStringSet* mValidIdentifiers;
  QColor mColor;
  CQtContext* mCQt;
  bool mValidationDirty;
  bool mAllowEmpty;
};

#endif //  _CEXPR_VALIATOR_H_
