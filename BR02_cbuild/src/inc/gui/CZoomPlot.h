// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "util/CarbonTypes.h"
#include "util/c_memmanager.h"
#include <qwt_plot.h>
#include <qwt_symbol.h>

//! class to create a zoomable plot area using Qwt and Qt
/*!
 *! This is based on the 'realtime' example which is in 
 *! /tools/linux/qwt-5.0.0rc1/examples/realtime_plot
 */
class CZoomPlot : public QwtPlot {
  Q_OBJECT
public:
  CARBONMEM_OVERRIDES

  //! ctor
  CZoomPlot(QWidget* parent = NULL);

  //! dtor
  ~CZoomPlot();

  //! user must override this method
  virtual void getExtents(double* xsize, double* ysize) = 0;

  virtual void update() = 0;
};

