// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CQT_PUSHBUTTON_CONTROL_H_
#define _CQT_PUSHBUTTON_CONTROL_H_

#include "gui/CQtControl.h"

class QPushButton;
class CQtContext;

//! Simple pushbuttons do not seem to respond to simulated mouse events!
class CQtPushButtonControl : public CQtControl {
  Q_OBJECT
public:
  CQtPushButtonControl(QPushButton* pb, CQtContext*);
  virtual void dumpState(UtOStream& stream);
  virtual void getValue(UtString* str) const;
  virtual bool putValue(const char*, UtString* /* errmsg */);
  virtual QWidget* getWidget() const;

public slots:
  void clicked();

private:
  QPushButton* mPushButton;
  CQtContext* mCQt;
};

#endif
