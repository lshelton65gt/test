// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CQT_CONTROL_H_
#define _CQT_CONTROL_H_

#include "util/CarbonPlatform.h"
#include <QWidget>
#include "shell/carbon_shelltypes.h"

class UtString;
class QComboBox;
class QLineEdit;
class CTextEdit;
class CQtContext;
class QSpinBox;
class CHexSpinBox;
class UtOStream;
class QCheckBox;
class QRadioButton;

//! class to control a Qt Widget or Action
/*!
 * Carbon library for using Qt.  This layer has three benefits over using
 * raw Qt:
 *    - uniform string-based set/get methodology for all widgets.  This
 *      facilitates both "Undo" and regression testing the contents of
 *      the GUI
 *    - event capture for recording tests.
 *    - master name registry for all widgets, which is also needed for
 *      regression testing
 *    - sync methodology for subprocesses and timers, so that a regression 
 *      replay doesn't get ahead of the outside world
 *
 * The general strategy is:
 *    - define a class CQtContext which knows about all the widgets
 *    - for every active Qt widget class, define a CQt version of that
 *      class that provides the uniform get/set interface, and overrides
 *      the event handlers to allow us to capture events.  That helps
 *      us create new tests by just using the GUI.
 *    - Rely on the existing Qt ability to send events to widgets
 *
 * I'm afraid that multiple inheritance is required to make this really
 * convenient.  We have to inherit from the Qt widget class (e.g. QtSpinBox),
 * and we also need to inherit from CQtControl, which provides the
 * get/set interface.  There is also some boilerplate override of the
 * Qt virtual methods for handling certain types events that must be
 * specified in each subclass
 */
class CQtControl : public QObject {
  Q_OBJECT
public:
  //! ctor
  CQtControl();

  //! dtor
  virtual ~CQtControl();

  //! get the current value of a CQt widget as a string
  /*!
   *! The manner in which the widget data is encoded in the string is
   *! left up to the individual derivation.  The only requirement is
   *! that the result of getValue must be legal to pass into putValue.
   */
  virtual void getValue(UtString* str) const = 0;

  //! set the value of a CQt widget as a string
  /*!
   *! The manner in which the widget data is encoded in the string is
   *! left up to the individual derivation.  The only requirement is
   *! that the result of getValue must be legal to pass into putValue.
   *! If putValue is passed an invalid value for the widget, there
   *! is no effect.  An explanation of the problem should be placed
   *! in errmsg, and false is returned.  true is returned on success.
   */
  virtual bool putValue(const char* str, UtString* errmsg) = 0;

  //! Put the value as an integer index -- this only makes sense for ComboBoxes
  virtual bool putIndex(UInt32 index, UtString* errmsg);

  //! Given X/Y coordinates within a widget, get device-independent location
  /*!
   *! When recording events for regression-test playback, we want to avoid
   *! dependence on fonts, screen resolution, and visual tweaks.  For many
   *! widgets (e.g. QPushButton), exact x/y position does not matter.
   *!
   *! \returns false on failure (x/y out-of-bounds)
   *! 
   *! \sa LocationToXY
   */
  virtual bool XYToLocation(int x, int y, UtString* location,
                            UtString* errmsg);

  //! Given an location within the widget, get x/y coordinates
  /*!
   *! This is the inverse of XYToLocation, and is used during regression
   *! test playback to generate the proper x/y coordinates for an location
   *! within a widget, given the current font and device resolution.  Some
   *! widgets, such as QPushButton, don't care about the location, and can
   *! simply return x,y in the center of the widget.
   *! 
   *! \sa XYToLocation
   */
  virtual bool locationToXY(const char* location, int* x, int* y,
                            UtString* errmsg);

  virtual QWidget* getWidget() const = 0;

  virtual void dumpState(UtOStream& file) = 0;

  virtual void dumpWidgetProperties(UtOStream& file);
}; // class CQtControl


// Combo-boxes pull up another widget when you click on the
// arrow.  Let's not record that:
//  registerWidget(new CQtGenericControl, widget, widgetName);
// instead, let's record just the final result
class CQtComboBoxControl : public CQtControl {
public:
  CQtComboBoxControl(QComboBox* cb);
  virtual void getValue(UtString* str) const;
  virtual bool putValue(const char* str, UtString* /* errmsg */);
  virtual bool putIndex(UInt32 index, UtString* errmsg);
  virtual QWidget* getWidget() const;
  virtual void dumpState(UtOStream& stream);

private:
  QComboBox* mComboBox;
};


//! Simple pushbuttons do not seem to respond to simulated mouse events!
class CQtLineEditControl : public CQtControl {
  Q_OBJECT
public:
  CQtLineEditControl(QLineEdit* le, CQtContext*);
  virtual void getValue(UtString* str) const;
  virtual bool putValue(const char*, UtString* /* errmsg */);
  virtual QWidget* getWidget() const;
  virtual void dumpState(UtOStream& stream);

public slots:
  void textChanged(const QString&);

private:
  QLineEdit* mLineEdit;
  CQtContext* mCQt;
};

//! Simple pushbuttons do not seem to respond to simulated mouse events!
class CQtTextEditControl : public CQtControl {
  Q_OBJECT
public:
  CQtTextEditControl(CTextEdit* le, CQtContext*);
  virtual void getValue(UtString* str) const;
  virtual bool putValue(const char*, UtString* /* errmsg */);
  virtual QWidget* getWidget() const;
  virtual void dumpState(UtOStream& stream);

public slots:
  void textChanged();

private:
  CTextEdit* mTextEdit;
  CQtContext* mCQt;
};

//! Simple pushbuttons do not seem to respond to simulated mouse events!
class CQtSpinBoxControl : public CQtControl {
  Q_OBJECT
public:
  CQtSpinBoxControl(QSpinBox* sb, CQtContext*);
  virtual void getValue(UtString* str) const;
  virtual bool putValue(const char*, UtString* /* errmsg */);
  virtual QWidget* getWidget() const;
  virtual void dumpState(UtOStream& stream);

public slots:
  void valueChanged(int);

private:
  QSpinBox* mSpinBox;
  CQtContext* mCQt;
};

//! Simple pushbuttons do not seem to respond to simulated mouse events!
class CQtHexSpinBoxControl : public CQtControl {
  Q_OBJECT
public:
  CQtHexSpinBoxControl(CHexSpinBox* sb, CQtContext*);
  virtual void getValue(UtString* str) const;
  virtual bool putValue(const char*, UtString* /* errmsg */);
  virtual QWidget* getWidget() const;
  virtual void dumpState(UtOStream& stream);

public slots:
  void valueChanged(const QString&);

private:
  CHexSpinBox* mSpinBox;
  CQtContext* mCQt;
};

//! Checkbox control with partial state
class CQtCheckBoxControl : public CQtControl {
  Q_OBJECT
public:
  CQtCheckBoxControl(QCheckBox* sb, CQtContext*);
  virtual void getValue(UtString* str) const;
  virtual bool putValue(const char*, UtString* /* errmsg */);
  virtual QWidget* getWidget() const;
  virtual void dumpState(UtOStream& stream);

public slots:
  void stateChanged();

private:
  QCheckBox* mCheckBox;
  CQtContext* mCQt;
};

#endif
