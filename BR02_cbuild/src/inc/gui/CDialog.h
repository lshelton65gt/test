// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _C_DIALOG_H_
#define _C_DIALOG_H_

#include "util/c_memmanager.h"
#include "util/CarbonTypes.h"
#include "util/UtString.h"
#include <QDialog>

class CDialog : public QDialog
{
  Q_OBJECT
public:
  CARBONMEM_OVERRIDES

  CDialog(QWidget *parent);

  // Interface function for setting data during playback
  void putRecordData(const UtString &data);
  const UtString &getData() {return mStringValue;}
  void updateData();

protected:
  // Common function to set dialog's data, to be used for both user
  // input and event playback.  The derived class's implementation
  // should call the base class function upon completion to emit a
  // signal used for event recording, and to update the local string
  // value.
  virtual void setData(const UtString &data);

private:
  //! string representation of the dialog's value
  UtString mStringValue;

signals:
  void dataEntered(const UtString &data);

};


#endif
