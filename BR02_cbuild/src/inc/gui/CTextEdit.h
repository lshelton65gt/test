// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _C_TEXT_EDIT_H_
#define _C_TEXT_EDIT_H_

#include "util/c_memmanager.h"
#include "util/CarbonTypes.h"
#include <QTextEdit>

class CTextEdit : public QTextEdit {
  Q_OBJECT
public:
  CARBONMEM_OVERRIDES

  CTextEdit();

  void putRowSizeConstraint(UInt32 minRows, UInt32 preferredRows);
  virtual QSize sizeHint() const; // override
private:
  UInt32 mPreferredHeight;
};

#endif
