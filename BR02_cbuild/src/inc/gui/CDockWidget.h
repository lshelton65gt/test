/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "gui/CQt.h"
#include <QDockWidget>

//! dock widget that has a close signal
class CDockWidget : public QDockWidget
{
  Q_OBJECT

public:
  //! ctor
  CDockWidget(const char*, QWidget *parent = 0);

  //! dtor
  void closeEvent(QCloseEvent *e);

signals:
  void closed(QDockWidget *);
};
