// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CHEX_SPIN_BOX_H_
#define _CHEX_SPIN_BOX_H_

#include "util/CarbonPlatform.h"
#include <QSpinBox>
#include "util/DynBitVector.h"
class UtString;

class CHexSpinBox : public QAbstractSpinBox {
  Q_OBJECT
public:
  CHexSpinBox();
  CHexSpinBox(QWidget* parent);
  virtual ~CHexSpinBox();

/*
  virtual QString textFromValue(int v) const;
  virtual int valueFromText(const QString&v) const;
*/

  virtual StepEnabled stepEnabled() const;
  virtual void stepBy(int steps);
  virtual QValidator::State validate(QString& input, int& pos) const;

  bool getValue(DynBitVector* bv) const;
  void setValue(const DynBitVector& bv);
  void setValue(int);

  bool setValue(const char* str, UtString* errmsg);

  void setMinimum(const DynBitVector&);
  void setMaximum(const DynBitVector&);

  void setSingleStep(int);
  void setMinimum(int);
  void setMaximum(int);

  bool inRange(const DynBitVector& bv) const;

public slots:
  void lineEditValueChanged(const QString&);

signals:
  void valueChanged(const QString&);
  void valueChanged(const DynBitVector&);
  void valueChanged(int);

private:
  class CHexValidator : public QValidator {
  public:
    CHexValidator(CHexSpinBox* spinBox);
    State validate(QString& input, int& pos) const;

  private:
    CHexSpinBox* mSpinBox;
  };

  int mSingleStep;
  DynBitVector mMinimum;
  DynBitVector mMaximum;
  CHexValidator mValidator;
};
  
#endif
