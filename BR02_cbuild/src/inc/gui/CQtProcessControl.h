// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CQT_PROCESS_CONTROL_H_
#define _CQT_PROCESS_CONTROL_H_

#include "gui/CQtControl.h"

class QProcess;
class CQtContext;

//! Help synchronize QProcess exiting with event-file playback
class CQtProcessControl : public CQtControl {
  Q_OBJECT
public:
  CQtProcessControl(QProcess* pb, CQtContext*);
  virtual void dumpState(UtOStream& stream);
  virtual void getValue(UtString* str) const;
  virtual bool putValue(const char*, UtString* /* errmsg */);
  virtual QWidget* getWidget() const;

public slots:
  void finished(int exit_status);
  void started();

private:
  QProcess* mProcess;
  CQtContext* mCQt;
  bool mActive;
  int mLastExitStatus;
  UInt32 mNumExits;
  QWidget* mWidget;
};

#endif
