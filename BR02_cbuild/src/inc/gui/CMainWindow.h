// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*/

#ifndef _CMAIN_WINDOW_H_
#define _CMAIN_WINDOW_H_

#include "gui/CQt.h"
#include <QMainWindow>

class CMainWindow : public QMainWindow {
public:
  CMainWindow(QWidget *parent = 0, Qt::WindowFlags flags = 0);
  virtual ~CMainWindow();

  //! overridable method that can be overridden by any widget
  /*!
   *! This provides the mechanism for Carbon Explorer to automatically
   *! save the file on every edit, rather than waiting for the user to
   *! explicitly hit "save"
   */ 
  virtual void notifyModified();
};

#endif // _CMAIN_WINDOW_H_
