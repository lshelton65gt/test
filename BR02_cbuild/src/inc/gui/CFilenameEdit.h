// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CFILENAME_EDIT_H_
#define _CFILENAME_EDIT_H_

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include <QPushButton>
#include <QLineEdit>

class CQtContext;

//! Edit a filename, witha 'browse' button to pop up a file browser
/*!
 *! This is just an hbox with a QLineEdit and a QPushButton which
 *! will pull up a QFileBrowser.  The reason we need this class is
 *! to help manage record/playback, because QFileBrowser is, on
 *! Windows (and Mac) a native wieget
 */
class CFilenameEdit : public QWidget {
  Q_OBJECT
public:
  CFilenameEdit(const char* widgetName,
                 bool readFile, const char* prompt,
                 const char* filter, CQtContext* cqt);
  ~CFilenameEdit();

  const char* getFilename() const;
  void putFilename(const char* fname) {mLineEdit->setText(fname);}

  QWidget* getPushButton() const {return mPushButton;}

signals:
  void filenameChanged(const char*);
  
public slots:
  void lineEditChanged(const QString&);
  void openFileDialog();

private:

  bool mReadFile;
  UtString mPrompt;
  UtString mFilter;
  mutable UtString mFileBuffer;
  QLineEdit* mLineEdit;
  QPushButton* mPushButton;
  CQtContext* mCQt;
};
  
#endif
