// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CDRAG_DROP_TREE_WIDGET_H_
#define _CDRAG_DROP_TREE_WIDGET_H_

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include <QTreeWidget>
#include "gui/CQtControl.h"
#include "util/UtHashSet.h"

class CQtContext;

//! A tree widget that initiates drag & drop of text
class CDragDropTreeWidget : public QTreeWidget {
  Q_OBJECT
public:
  CARBONMEM_OVERRIDES

  CDragDropTreeWidget(bool enaDrop, QWidget *parent = 0);

  //! override item selection method so that we don't record programmatic
  //! selections in event logs
  virtual void setItemSelected(QTreeWidgetItem* item, bool sel);
  virtual void expandItem(QTreeWidgetItem* item);

  //! override to avoid recording programmatic selection
  void takeTopLevelItem(int idx);

  //! find an item, take it out of the tree and delete it, assert if it fails
  void deleteItem(QTreeWidgetItem* item);

  //! Override mouse events in order to initiate drags
  virtual void mousePressEvent(QMouseEvent* e);
  virtual void mouseReleaseEvent(QMouseEvent* e);
  virtual void mouseMoveEvent(QMouseEvent* e);

  //! Override events in order to handle drops
  virtual void dragEnterEvent(QDragEnterEvent*);
  virtual void dragMoveEvent(QDragMoveEvent*);
  virtual void dropEvent(QDropEvent*);
  virtual void dragLeaveEvent(QDragLeaveEvent* event);
 
  //! drag-under animation helper functions
  void animateDropSite(const QPoint& pos);
  void clearAnimation();
  void setDragImmediate(bool value) { mDragImmediate = value; }

  void setEnableDrop(bool value) { mEnableDrop=value; }

  //! Set the current data for initiating a drag.
  /*! If the data is empty, drags will not occur
   */
  void putDragData(const char*);

  void deselectAll();

  bool recordingEnabled() const {return mEnableRecord;}
  void replayDrop(const char* str, QTreeWidgetItem* item);
  void emitSelectionChanged();

  // Trolltech bug # 135151 is a broken interaction between sorted
  // columns with programmatic calls to expandItem().  A workaround
  // was suggested by Anders Bakken - Support Engineer at Trolltech,
  // in response to issue # N137100.  We intercept the call to
  // setSortingEnabled, always telling QTreeWidget that the list is
  // not sortable.  But we can make the header display the sort-arrow
  // button anyway, and we can then connect that signal to our
  // onHeaderClicked method, which triggers the sort for for the
  // user.

  void setSortingEnabled(bool ena);  // intercept this call to QTreeWidget, which is broken

public slots:
  void qtSelectionChangedHandler();
  void onHeaderClicked(int section);

signals:
  void dropReceived(const char* dropText, QTreeWidgetItem* item);
  //void selectionChanged(const ItemArray&);
  void selectionChanged();
  void mapDropItem(QTreeWidgetItem**);

protected:
  void startDrag();

  enum DragState {
    eDragIdle, eDragStarted, eLookForMove
  };
  DragState mDragState;

  enum DropState {
    eDropIdle, eDropStarted
  };
  DropState mDropState;

  // If the user presses the left mouse button over a selected item,
  // we have to determine if the user wants to drag&drop or just
  // change the selection.  If it turns out that he releases the button
  // without moving away from the item, then we have to execute the
  // button press.
  QMouseEvent* mSaveMousePress;
  QTreeWidgetItem* mSaveItem;

  QDrag* mDrag;
  bool mEnableDrop;
  bool mEnableRecord;
  bool mEnableSelectSignal;
  UtString mDragData;
  QTreeWidgetItem* mDropSiteItem;
# define DD_MAX_COLUMNS 10 // UtArray is not defined on objects like QColor
  QBrush mDropSaveBrush[DD_MAX_COLUMNS];
  UInt32 mDropColumnCount;
  bool mDropItemSelected;
  bool mDropEnableSelectSignal;
  bool mDragImmediate;
  bool mSortingEnabled;
}; // class CDragDropTreeWidget : public QTreeWidget
  
class CQtDragDropWidgetControl : public CQtControl {
  Q_OBJECT
public:
  CARBONMEM_OVERRIDES

  CQtDragDropWidgetControl(CDragDropTreeWidget* widget, CQtContext* cqt);

  virtual QWidget* getWidget() const;

  virtual void dumpState(UtOStream& stream);

  virtual void getValue(UtString* str) const;

  typedef UtHashSet<QTreeWidgetItem*> ItemSet;

  //! Parse an item-list expressed as a sequence like "1 4 5.3.2"
  /*!
   *! This indicates:
   *!    the 1st root item (counting from 0)
   *!    the fourth root item
   *!    the second child of the third child of the 5th root item
   *! This is how we encode widget items in an event log
   */
  bool parseItemList(const char* str, ItemSet* items);

  void encodeItemList(UtString* buf, const ItemSet& items);

  void encodeNestedItems(UtString* buf, const UtString& path,
                         QTreeWidgetItem* item, const ItemSet& items);
  
  // Walk thru every friggin item and determine if it's selection
  // status is right or wrong, and if so, change it.  Return true
  // if any selection or deselection occurred
  bool selectItems(const ItemSet& items);

  bool selectItems(const ItemSet& items, QTreeWidgetItem* parent);

  void expandItems(const ItemSet& items);

  void collapseItems(const ItemSet& items);

  // putValue is used here as a hook to control the tree widget
  // by changing the
  virtual bool putValue(const char* str, UtString* errmsg);
  
public slots:
  void itemSelectionChanged();
  void itemExpanded(QTreeWidgetItem*);
  void itemCollapsed(QTreeWidgetItem*);
  void dropReceived(const char* str, QTreeWidgetItem* item);
  void itemChanged(QTreeWidgetItem*,int);

private:
  void dumpItem(UInt32 depth, UtOStream& stream, QTreeWidgetItem* item);

private:  
  CDragDropTreeWidget* mWidget;
  CQtContext* mCQt;
}; // class CQtDragDropWidgetControl : public CQtControl


#endif
