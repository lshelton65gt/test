// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _CQT_H_
#define _CQT_H_

#include "util/CarbonPlatform.h"
#include "util/Util.h"
#include "util/StringAtom.h"
#include "util/CarbonAbort.h"
#include "util/CarbonAssert.h"
#include "util/UtString.h"
#include "util/UtHashSet.h"
#include <QApplication>
#include "gui/CTextEdit.h"
#include "gui/CTabWidget.h"
#include <QLineEdit>
#include <QSpinBox>
#include <QTreeWidget>
#include <QComboBox>
#include <QPushButton>
#include <QRadioButton>
#include <QCheckBox>
#include <QVBoxLayout>
#include <QGroupBox>

class CQtContext;
class CHexSpinBox;
class CDragDropTreeWidget;
class CQtControl;
class CFilenameEdit;
class CHelpWindow;
class CTextEdit;
class Assistant;
class QMainWindow;
class CMainWindow;
class QProcess;

QLayout& operator<<(QLayout& mgr, QLayout* sub);
QLayout& operator<<(QLayout& mgr, QWidget* widget);

UtOStream& operator<<(UtOStream&, const QString&);
UtString& operator<<(UtString&, const QString&);

class CQt {
public:
  static QWidget* hSplit(QWidget* w1, QWidget* w2, QWidget* w3 = NULL);
  static QWidget* vSplit(QWidget* w1, QWidget* w2, QWidget* w3 = NULL);
  static QGroupBox* makeGroup(const char* name, QLayout* layout,
                            bool fixedVertical = false);
  static QGroupBox* makeGroup(const char* name, QWidget* widget,
                            bool fixedVertical = false);
  static QVBoxLayout* vbox(QWidget* w1 = NULL, QWidget* w2 = NULL,
                           QWidget* w3 = NULL, QWidget* w4 = NULL,
                           QWidget* w5 = NULL);
  static QLayout* hbox(QWidget* w1 = NULL, QWidget* w2 = NULL,
                       QWidget* w3 = NULL, QWidget* w4 = NULL,
                       QWidget* w5 = NULL, QWidget* w6 = NULL,
                       QWidget* w7 = NULL, QWidget* w8 = NULL,
                       QWidget* w9 = NULL, QWidget* w10 = NULL,
                       QWidget* w11 = NULL);
  static QWidget* makeWidget(QLayout* layout);
  static void addGridRow(QGridLayout* grid, const char* prompt,
                         QWidget* widget);
  static void addGridRow(QGridLayout* grid, const char* prompt,
                         QLayout* layout);


  //! Determine the default color for text 
  /*!
    This shouldn't be called often as it constructs a column and
    retrieves the color from that.
  */
  static QColor getDefaultTextColor();
};

// runs the normal 'connect' method but asserts that it was successful
#define CQT_CONNECT(sender, signal, receiver, slot) do { \
  bool signal_slot_connected = \
    connect(sender, SIGNAL(signal), receiver, SLOT(slot)); \
  INFO_ASSERT(signal_slot_connected, "signal/slot connection failure"); \
} while (0)


// Override class for having assertions show up as pop-ups instead
// of instant exits with no messages on Windows
class CQtAbortOverride : public CarbonAbortOverride {
public:
  //! ctor
  CQtAbortOverride(const char* appName, QWidget* widget, CQtContext* cqt);

  //! dtor
  virtual ~CQtAbortOverride();

  //! User must override this to change the way printing occurs
  virtual void printHandler(const char*);

  //! User must override this to change the way the application exits
  virtual void abortHandler();

private:
  QString mAppName;
  QWidget* mWidget;
  CQtContext* mCQt;
  bool mDoFlush;
};

typedef QList<QTreeWidgetItem*> QTreeItemList;

class CQtContext : public QApplication {
  Q_OBJECT
public:
  CQtContext(int& argc, char** argv);
  ~CQtContext();

  //! Initialize argument processing, etc.
  void init(int& argc, char** argv);

  //! construct a recordable, replayable QTextEdit
  CTextEdit* textEdit(const char* widgetName, CTextEdit* widget=NULL);

  //! construct a recordable, replayable QLineEdit
  QLineEdit* lineEdit(const char* widgetName, QLineEdit* widget=NULL);

  //! construct a recordable, replayable QSpinBox
  QSpinBox* spinBox(const char* widgetName, QSpinBox* widget=NULL);

  //! construct a recordable, replayable QSpinBox
  CHexSpinBox* hexSpinBox(const char* widgetName, CHexSpinBox* widget=NULL);

  //! construct a recordable, replayable QTreeWidget
  QTreeWidget* treeWidget(const char* widgetName, QTreeWidget* widget=NULL);

  //! construct a recordable, replayable QTreeWidget with Drag & Drop support
  CDragDropTreeWidget* dragDropTreeWidget(const char* widgetName, bool enaDrop, CDragDropTreeWidget* widget=NULL);

  //! construct a recordable, replayable QComboBox
  QComboBox* comboBox(const char* widgetName, QComboBox* widget=NULL);

  //! construct a recordable, replayable combo box from a UtIOEnum
  template<class Enum>
  QComboBox* enumBox(const char* widgetName) {
    QComboBox* eb = comboBox(widgetName);
    for (const char** p = Enum::getStrings(); *p != NULL; ++p) {
      eb->addItem(*p);
    }
    return eb;
  }

  //! construct a recordable, replayable QTabWidget
  CTabWidget* tabWidget(const char* widgetName, CTabWidget* widget=NULL, QWidget* parent=NULL);

  //! construct a recordable, replayable QPushButton
  QPushButton* pushButton(const char* widgetName, const char* label, QPushButton* widget=NULL);

  //! construct a recordable, replayable QProcess
  QProcess* process(const char* widgetName, QProcess* proc=NULL);

  //! construct a recordable, replayable QRadioButton
  QRadioButton* radioButton(const char* widgetName, const char* label, QRadioButton* widget=NULL);

  //! construct a recordable, replayable QCheckBox
  QCheckBox* checkBox(const char* widgetName, const char* label, QCheckBox* widget=NULL);

  //! construct a field for entering a filename
  CFilenameEdit* filenameEdit(const char* widgetName,
                              bool readFile, const char* prompt,
                              const char* filter);

  void recordPutValue(QWidget* widget, const char* text);
  void registerAction(QAction* action, const char* name);

  bool isPlaybackActive() const {return mPlaybackFile != NULL;}
  bool isRecordActive() const {return mRecordFile != NULL;}

  void recordFilename(const char* filename);
  bool getPlaybackFile(UtString* filename);
  bool popupFileDialog(QWidget* widget,
                       bool readFile,
                       const char* prompt,
                       const char* filter,
                       UtString* result);

  void dumpStateHierarchy();
  void enableRecord();
  void disableRecord();

  void flush();

  void setIgnoreRegistration(bool value) { mIgnoreRegistration=value; }

  void registerWidget(CQtControl* control,
                      const char* name,
                      bool recordEvents);
  void unregisterWidget(QWidget* widget);
  void unregisterAction(QAction* action);
  void launchHelp(const char* helpTag);
  void launchHelpUrl(const char* url);

  //! Show a blocking modal dialog for a warning message, and spew to stderr
  /*! Any event playback will be terminated. */
  void warning(QWidget* widget, const QString& message);

  //! If a popup warning dialog is created, it should stop any playback
  //! that might occur, at least until the dialog box is closed
  void stopPlayback();

  //! Specify the main window
  void putMainWindow(QWidget* mainWindow) {mMainWindow = mainWindow;}

  //! fetch the xtor definition file (if any from the command line
  const char *getXtorDefFile() 
  {
    if (mXtorDefFile.length() == 0)
      return NULL;
    else
    {
      static UtString tmp;
      tmp.clear();
      tmp << mXtorDefFile;
      return tmp.c_str();
    }
  }
 

  void setXtorDefFile(const char* newVal) 
  {
    mXtorDefFile = newVal;
  }

  //! Get the default foreground color (probably black)
  QColor getForegroundColor();

  //! Map help page tags to userdoc URLs
  void initializeHelpPages();

  QGroupBox* makeGroup(const char* name, const char* tag,
                     QLayout* layout,
                     bool fixedVertical = false);

  QGroupBox* makeGroup(const char* name, const char* tag,
                     QWidget* widget,
                     bool fixedVertical = false);

  //! record an 'exit' command, used for window-manager-triggered close events
  void recordExit();

  //! tell the event playback system to wait for this process to exit
  void waitForProcess(QProcess* process);

  //! tell the event playback system that a process has now exited
  void processFinished(QProcess*);

public slots:
  void setModified();
  void readNextEvent();
  void recordTrigger(StringAtom*,bool);
  void recordText(StringAtom*,const QString&);
  void recordIndex(StringAtom* name, int index);

protected:
  // intercept events
  virtual bool eventFilter(QObject* obj, QEvent* event);

  // allow derived classes to add additional args
  virtual void addCustomArgs();
  ArgProc* mArgs;

private:
  bool isRecording() const {
    return (mRecordFile != NULL) && (mDisableRecordStack == 0);
  }
  void registerChildren(QWidget* widget, const char* widgetName);
  void defkey(const char* keyname, Qt::Key key);
  void setupKeymap();
  void recordKey(const char* name, const char* eventName,
                 QKeyEvent* keyEvent);
  void recordMouse(CQtControl*, const char* name, const char* eventName,
                   QMouseEvent* mouseEvent);
  QEvent* parseKeyEvent(QEvent::Type type, UtIStringStream& tok);
  QEvent* parseMouseEvent(CQtControl*, QEvent::Type type,
                          UtIStringStream& tok);
  void playEvent(const char* buf);

  AtomicCache* mAtomicCache;

  typedef UtHashMap<UtString,CQtControl*> NameControlMap;
  NameControlMap* mNameControlMap;

  typedef UtHashMap<QWidget*,StringAtom*> WidgetNameMap;
  WidgetNameMap* mWidgetNameMap;

  typedef UtHashMap<QAction*,StringAtom*> ActionNameMap;
  ActionNameMap* mActionNameMap;

  typedef UtHashMap<StringAtom*,int> NameToKeyMap;
  typedef UtHashMap<int,StringAtom*> KeyToNameMap;
  NameToKeyMap* mNameToKeyMap;
  KeyToNameMap* mKeyToNameMap;

  UtOStream* mRecordFile;
  UtIStream* mPlaybackFile;
  QString mXtorDefFile;

  QTimer* mNextEventTimer;

  UtString mPlaybackFilenameBuf;

  UInt32 mDisableRecordStack;   // only record if non-zero
  bool mPlaybackBusy;
  bool mIgnoreRegistration;

  // this bit of magic stolen from AtomicCache::StringCache
  struct CharStarHash {
    size_t hash(const char* str) const {return UtString::hash(str);}
    bool equal(const char* s1, const char* s2) const {return strcmp(s1, s2) == 0;}
    bool lessThan1(const char* s1, const char* s2) const {return s1 < s2;}
    bool lessThan(const char* s1, const char* s2) const {return strcmp(s1, s2) < 0;}
  };
  typedef UtHashMap<const char *, const char *, CharStarHash> TagToUrlMap;
  TagToUrlMap *mHelpPageMap;

  //CHelpWindow* mHelpWindow;
  Assistant* mHelpWindow;
  QWidget* mMainWindow;
  bool mExitLogged;
  UtHashSet<QProcess*> mProcessWait;
}; // class CQtContext : public QApplication

// To block the event-recording of programmatic changes to widgets,
// we instantiate this class as an automatic in all slots.  Note that
// enableRecord and disableRecord have stack behavior
struct CQtRecordBlocker {
  CQtRecordBlocker(CQtContext* cqt)
    : mCQt(cqt)
  {
    mCQt->disableRecord();        // don't record macros while updating widgets
  }
  ~CQtRecordBlocker() {
    mCQt->enableRecord();

  }
  CQtContext* mCQt;
};

#endif // _CQT_H_
