// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CQT_DIALOG_CONTROL_H_
#define _CQT_DIALOG_CONTROL_H_

#include "gui/CQtControl.h"
#include "util/c_memmanager.h"

class CDialog;
class UtString;
class CQtContext;

class CQtDialogControl : public CQtControl {
  Q_OBJECT
public:
  CARBONMEM_OVERRIDES

  CQtDialogControl(CDialog *dialog, CQtContext *cqt);
  virtual void dumpState(UtOStream& stream);
  virtual void getValue(UtString* str) const;
  virtual bool putValue(const char*, UtString* /* errmsg */);
  virtual QWidget* getWidget() const;

public slots:
  void dataEntered(const UtString &data);

private:
  CDialog* mDialog;
  CQtContext* mCQt;
};

#endif
