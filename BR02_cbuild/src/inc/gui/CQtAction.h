// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CQT_ACTION_H_
#define _CQT_ACTION_H_

#include "gui/CQtControl.h"
#include <QObject>

class StringAtom;
class QAction;
class UtOStream;

//! class to wrap an action
/*!
 *! This helps us create a generic recording mechanism for actions,
 *! by providing a signal that is raised when the action is triggered,
 *! and that provides the name of the action.
 *!
 *! The only reason this is necessary is because signals in Qt do
 *! not provide any sort of context to the receiver, except for
 *! the receiver object itself.  This helps carry a name through
 *! to the slot.
 */
class CQtAction : public CQtControl {
  Q_OBJECT
public:
  CQtAction(StringAtom* name, QAction* action);

  virtual QWidget* getWidget() const;

  virtual void getValue(UtString*) const;
  virtual bool putValue(const char*, UtString*);
  virtual void dumpState(UtOStream& stream);

public slots:
  void triggered(bool);
  void textChanged(const QString&);
  void indexChanged(int index);
signals:
  void notify(StringAtom*,bool);
  void notifyText(StringAtom*,const QString&);
  void notifyIndex(StringAtom*,int index);
private:
  StringAtom* mName;
  QAction* mAction;
};

#endif
