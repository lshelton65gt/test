// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _C_TABLEWIDGET_H_
#define _C_TABLEWIDGET_H_

#include "util/c_memmanager.h"
#include "util/CarbonTypes.h"
#include <QTableWidget>

class UtString;

class CTableWidget : public QTableWidget
{
  Q_OBJECT
public:
  CARBONMEM_OVERRIDES

  CTableWidget(QWidget *parent);

  // Interface function for setting data during playback
  void putRecordData(const UtString &data);

protected:
  // Allow setModelData to be called with a string
  // instead of an editor, so event playback can
  // use it.
  // If the table has a delegate, the delegate's
  // setModelData() should call this.
  virtual void setModelData(const UtString &data, QAbstractItemModel *model,
                            const QModelIndex &index);

signals:
  void cellEdited(QTableWidgetItem*);

};


#endif
