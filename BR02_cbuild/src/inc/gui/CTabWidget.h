// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _C_TAB_WIDGET_H_
#define _C_TAB_WIDGET_H_

#include <QTabWidget>

//! derive a class from QTabWidget just to get access to the QTabBar
class CTabWidget : public QTabWidget {
public:
  CTabWidget() : QTabWidget(0) {}
  CTabWidget(QWidget* parent) : QTabWidget(parent) {}
  QTabBar* getTabBar() {return tabBar();}
};

#endif
