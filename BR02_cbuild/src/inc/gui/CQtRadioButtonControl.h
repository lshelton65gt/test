// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CQT_RADIOBUTTON_CONTROL_H_
#define _CQT_RADIOBUTTON_CONTROL_H_

#include "gui/CQtControl.h"

class QRadioButton;
class CQtContext;

//! Simple pushbuttons do not seem to respond to simulated mouse events!
class CQtRadioButtonControl : public CQtControl {
  Q_OBJECT
public:
  CQtRadioButtonControl(QRadioButton* pb, CQtContext*);
  virtual void dumpState(UtOStream& stream);
  virtual void getValue(UtString* str) const;
  virtual bool putValue(const char*, UtString* /* errmsg */);
  virtual QWidget* getWidget() const;

public slots:
  void toggled(bool);

private:
  QRadioButton* mRadioButton;
  CQtContext* mCQt;
};

#endif
