// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!  \file
  This file implements three classes for handling types:
  IODBIntrinsic, IODBGenTypeEntry, and IODBTypeDictionary.  The
  TypeDictionary manages the set of Intrinsics and GenTypeEntries within
  a given symbol table.
*/
#ifndef _IODB_TYPES_H_
#define _IODB_TYPES_H_


#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/UtArray.h"
#include "util/Loop.h"

class ZostreamDB;
class ZistreamDB;
class ConstantRange;
class ConstantRangeFactory;


//! IODBIntrinsic class -- uniquely identifies the type of an object.
/*!  
This class contains information as to the type of object
(scalar/real/vector/memory) as well as the associated ranges for vector width
and memory length.  This class is basically pointless for a scalar or real,
excessively sized for a vector, and just right for a memory.
Subclassing this (or something) for each of the three types might be a
good idea.
*/
class IODBIntrinsic
{
public: CARBONMEM_OVERRIDES
  //! Enumeration to define the type of a net.
  enum Type
  {
    eScalar, //!< A scalar intrinsic
    eVector, //!< A vector intrinsic
    eMemory  //!< A memory intrinsic
  };

  //! Default constructor; used for scalars.
  IODBIntrinsic() {
    mVecRange = NULL;
    mMemAddrRange = NULL;
    mType = eScalar;
  }

  //! Constructor for reals
  /*! \param type to initialize the intrinsic to
   */
  IODBIntrinsic( Type type ) {
    mVecRange = NULL;
    mMemAddrRange = NULL;
    mType = type;
  }

  //! Constructor for vectors.
  IODBIntrinsic( const ConstantRange *vecRange ) {
    mVecRange = vecRange;
    mMemAddrRange = NULL;
    mType = eVector;
  }

  //! Constructor for memories.
  IODBIntrinsic( const ConstantRange *vecRange, const ConstantRange *memAddrRange ) {
    mVecRange = vecRange;
    mMemAddrRange = memAddrRange;
    mType = eMemory;
  }

  Type getType() const {
    return mType;
  }

  //! Hash function for the dictionary
  size_t hash() const;

  bool operator==( const IODBIntrinsic& other ) const;

  //! get the msb.
  /*!
   * For vector and memory types, this returns the left bound of the
   * word range.
   * \sa getHighAddr, getLowAddr
   */
  SInt32 getMsb() const;

  //! get the lsb
  /*!
   * For vector and memory types, this returns the right bound of the
   * word range.
   * \sa getHighAddr, getLowAddr
   */
  SInt32 getLsb() const;

  //! Returns the range for this vector
  /*!
    Returns NULL if this is not a vector or a memory.

    If this is a memory, this returns the constant range for a memory
    row.
    \sa getMemAddrRange
  */
  const ConstantRange* getVecRange() const {
    return mVecRange;
  }

  //! Returns the memory address range for this memory
  /*!
    Returns NULL if this is not a memory.
    \sa getVecRange
  */
  const ConstantRange* getMemAddrRange() const {
    return mMemAddrRange;
  }

  //! save a ZostreamDB into the DB
  bool dbWrite(ZostreamDB&) const;

  //! restore a ZostreamDB from the DB
  bool dbRead(ZistreamDB&);

  //! get the width
  size_t getWidth() const;

  //! get the high addr
  /*!
   * For memory types, return the left bound of the address range.
   * \sa getMsb, getLsb
   */
  SInt32 getHighAddr() const;

  //! get the low addr
  /*!
   * For memory types, return the right bound of the address range.
   * \sa getMsb, getLsb
   */
  SInt32 getLowAddr() const;

  //! print the intrinsic type information to cout
  void print() const;

private:
  const ConstantRange *mVecRange;
  const ConstantRange *mMemAddrRange;
  Type mType;
};


//! Generated type entry class
/*!
  This class is used by codegen to generate the needed debug_generator
  functions for the shell. This is usually a very small subset of the
  IODBIntrinsics.
*/
class IODBGenTypeEntry
{
public: CARBONMEM_OVERRIDES
  //! Attribute flags enumeration
  enum AttribFlags {
    eNone = 0x0, //!< No attributes assigned
    ePrimaryInput = 0x1, //!< Primary input
    eMinAttribute = ePrimaryInput,
    eClock = 0x2, //!< Clock - UNUSED
    eNoFlow = 0x4, //!< Input does not flow before clock - UNUSED
    eForcible = 0x8, //!< Net can be forced
    eBidirect = 0x10, //!< Tristate net is a bidirect
    eSigned = 0x20, //!< Net has signed context
    eTristate = 0x40, //!< Net is a tristate
    eReal = 0x80,     //!< Net is a real number
    eMaxAttribute = eReal
  };
  

  //! Default constructor, used by DB.
  IODBGenTypeEntry();

  //! GenTypeEntry copy constructor
  IODBGenTypeEntry( const IODBGenTypeEntry& entry );

  //! construct a GTE with the supplied data
  IODBGenTypeEntry( const IODBIntrinsic *intrinsic,
                    AttribFlags attribs = eNone );

  //! destructor
  ~IODBGenTypeEntry();

  ptrdiff_t compare(const IODBGenTypeEntry* b) const;
  
  bool operator<(const IODBGenTypeEntry& b) const {
    return compare(&b) < 0;
  }
  size_t hash() const;
  bool operator==(const IODBGenTypeEntry& b) const {
    return compare(&b) == 0;
  }
  
  //! Get the underlying IODBIntrinsic
  const IODBIntrinsic* getIntrinsic() const;
  
  //! Set the underlying IODBIntrinsic
  void setIntrinsic(const IODBIntrinsic *i) {
    mIntrinsic = i;
  }

  //! save a ZostreamDB into the DB
  bool dbWrite(ZostreamDB&) const;

  //! restore a ZostreamDB from the DB
  bool dbRead(ZistreamDB&);

  //! is this type a tristate?
  bool isTristate() const;

  //! Is this a vector?
  bool isVector() const;

  //! Is this type for a primary input?
  bool isPrimaryInput() const;

  //! Is this type for a primary clock input?
  bool isPrimaryClockInput() const;

  //! Is this type a bidirect?
  bool isBidirect() const;

  //! Is this a signed type?
  bool isSigned() const;

  //! Is this type forcible?
  bool isForcible() const;

  //! Is this type a real number?
  bool isReal() const;

  //! Function to simplify masking out specific AttribFlags
  /*!
    \param srcFlags The original set of flags
    \param excludeFlags One or more AttribFlags to set to 0 in
    srcFlags
    \returns New set of srcFlags that have any of the flags specified
    in excludeFlags set to 0.
  */
  static AttribFlags calcAttribFlagsExclude(AttribFlags srcFlags,
                                            AttribFlags excludeFlags);

  //! Returns the current AttribFlags excluding specified flags
  /*!
    Works just like calcAttribFlagsExclude except that the src flags
    are this entries source flags
  */
  AttribFlags getAttribFlagsExclude(AttribFlags excludeFlags) const;

  //! Returns the value of the two merged flags
  static AttribFlags mergeFlags(AttribFlags flagsOne, AttribFlags flagsTwo);

  //! print the type information to cout
  void print() const;

private:
  //! Function used by both this operator== and IODBGenTypeEntry's lessThan.
  static UInt32 sMaskForCompare(UInt32 flags);

  //! helper function for hash/compare
  UInt32 numBytesNeeded() const;

  const IODBIntrinsic* mIntrinsic;
  // word of flags for other attributes--from enum AttribFlags
  UInt32 mAttrib;
};


//! TypeDictionary class -- identifies a unique set of TypeEntries
class IODBTypeDictionary
{
public: CARBONMEM_OVERRIDES
  IODBTypeDictionary();
  ~IODBTypeDictionary();

  //! Add a scalar intrinsic to the dictionary
  //! \returns the intrinsic
  const IODBIntrinsic* addScalarIntrinsic();

  //! Add a vector intrinsic to the dictionary
  //! \param msb Integer most significant bit
  //! \param lsb Integer least significant bit
  //! \returns the intrinsic
  const IODBIntrinsic* addVectorIntrinsic( int msb, int lsb );

  //! Add a memory intrinsic to the dictionary, returning the intrinsic
  //! \param hi Integer left address bound
  //! \param lo Integer right address bound
  //! \param msb Integer most significant word bit
  //! \param lsb Integer least significant word bit
  //! \returns the intrinsic
  const IODBIntrinsic* addMemoryIntrinsic( int hi, int lo, int msb, int lsb );

  //! Return the index of the specified GenTypeEntry, creating a new one if needed
  /*!
    The intrinsic must have been added to the type dictionary before
    this call.  If this genTypeEntry has not been encountered before a
    new one is created and entered into the type dictionary.
    \param intrinsic The intrinsic for this type
    \param flags The GenTypeEntry flags for this type
    \returns The index of the GenTypeEntry in the dictionary
    \sa addScalarIntrinsic, addVectorIntrinsic, addMemoryIntrinsic
   */
  SInt32 addTypeEntry( const IODBIntrinsic *intrinsic,
                      IODBGenTypeEntry::AttribFlags flags );

  //! Get intrinsic associated with the supplied intrinsic index
  const IODBIntrinsic* getIntrinsic(UInt32 index) const;

  //! Get the underlying IODBIntrinsic (non-const)
  IODBIntrinsic* getIntrinsicNonConst(UInt32 index);

  //! Get the type entry associated with a generated type index
  const IODBGenTypeEntry* getGenTypeEntry(UInt32 genIndex) const;

  //! Get the type entry associated with a generated type index
  IODBGenTypeEntry* getGenTypeEntryNonConst(UInt32 genIndex);

  //! return the number of unique dictionary items (intrinsics)
  SInt32 size() const {return mDictVector.size();}
  
  //! Returns the number of unique generated types
  SInt32 numGenTypes() const;

  //! save a ZostreamDB into the DB
  bool save(ZostreamDB&) const;

  //! restore a ZostreamDB from the DB
  bool restore(ZistreamDB&);

  typedef UtArray<IODBIntrinsic*> TypeVector;

  //! typedef for looping through unique data types
  typedef Loop<TypeVector> TypeLoop;

  //! loop over all the defined types, in numeric order
  TypeLoop loopTypes() {return TypeLoop(mDictVector);}

  //! Returns the generated type index for a scalar with these attributes
  /*!
    \returns -1 if no matching type is found.
  */
  SInt32 findGenTypeIndexScalar(IODBGenTypeEntry:: AttribFlags attribFlags) const;

  //! Returns the generated type index for a vector with these attributes
  /*!
    The parameter, entry, is used for the vector parameters of the
    vector type to find. The entry must be created from this
    dictionary.
    
    \returns -1 if no matching type is found.
  */
  SInt32 findGenTypeIndexVector(const IODBIntrinsic* entry, 
                                IODBGenTypeEntry::AttribFlags flags) const;

  //! Returns the generated type index given the type entry index
  /*!
    \param typeIndex The IODBIntrinsic index
    \returns The generated type index for the IODBIntrinsic. -1 if
    there is no generated type index for this entry
  */
  SInt32 findGenTypeIndex(SInt32 typeIndex) const;
  
private:
  //! Returns the generated type index
  SInt32 findGenEntry(const IODBGenTypeEntry& genTE ) const;

  //! Returns the intrinsic type index
  SInt32 findIntrinsic(const IODBIntrinsic& i) const;

  //! Finds or creates the corresponding IODBIntrinsic
  IODBIntrinsic* getCreateIntrinsic(const IODBIntrinsic& key);

  //! Create and/or return a cached constant range
  /*!
   * If no range exists with the supplied bounds a new ConstantRange
   * will be created.  Otherwise, the pre-existing ConstantRange is
   * returned.
   * \param msb, lsb The bounds of the desired ConstantRange
   * \returns The ConstantRange
   */
  const ConstantRange* getConstantRange(SInt32 msb, SInt32 lsb);

  typedef UtHashMap<const IODBIntrinsic*, SInt32,
                    HashPointerValue<const IODBIntrinsic*> > DictMap;
  typedef DictMap::const_iterator CDictIter;

  typedef UtArray<IODBIntrinsic*> EntryVec;
  typedef Loop<EntryVec> EntryVecLoop;

  typedef UtHashMap<IODBGenTypeEntry*, SInt32,
                    HashPointerValue<IODBGenTypeEntry*> > GenTypeMap;
  typedef GenTypeMap::const_iterator CGenTypeIter;
  
  typedef UtArray<IODBGenTypeEntry*> GenTypeVec;
  typedef Loop<GenTypeVec> GenTypeVecLoop;

  // forbid
  IODBTypeDictionary(const IODBTypeDictionary&);
  IODBTypeDictionary& operator=(const IODBTypeDictionary&);

  //! mDictMap (and mDictVector) store the type intrinsics
  DictMap mDictMap;
  EntryVec mDictVector;

  //! mGenTypeMap (and mGenTypeVec) store the genTypeEntry
  GenTypeMap mGenTypeMap;
  GenTypeVec mGenTypeVec;

  //! mParams stores the set of ConstantRanges used in the dictionary
  ConstantRangeFactory* mParams;
};

#endif // _IODB_TYPES_H_
