// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _IODBDESIGNDATA_H_
#define _IODBDESIGNDATA_H_

#include "symtab/STBranchNode.h"
#include "symtab/STFieldBOM.h"
#include "util/UtList.h"
#include "nucleus/Nucleus.h"

class UserType;
class UserTypeFactory;

//! Holds design data associated with a branch/leaf node
//! in the design data symbol tables.
/*! The data for now is protected mutable flag. More data related
  to VHDL visibility is expected to be added here.
*/
class IODBDesignNodeData
{
public:

  IODBDesignNodeData()
    : mUnelabScope(NULL),
      mIsProtectedMutable(false),
      mType(NULL)
  {}

  ~IODBDesignNodeData() {}

  void putUnelabScope(STBranchNode* unelabScope) {
    mUnelabScope = unelabScope;
  }

  void setProtectedMutable() {
    mIsProtectedMutable = true;
  }

  void putUserType(const UserType* typ) {
    mType = typ;
  }

  const STBranchNode* getUnelabScope() const {
    return mUnelabScope;
  }

  bool isProtectedMutable() const {
    return mIsProtectedMutable;
  }

  const UserType* getUserType() const {
    return mType;
  }
  
  void writeXml(UtXmlWriter* xmlWriter) const;

private:
  STBranchNode* mUnelabScope; // Unelab scope for this elab scope.
  bool mIsProtectedMutable; // Is node protected mutable?
  const UserType* mType;
};

//! BOM for design data symbol tables.
class IODBDesignDataBOM : public STFieldBOM
{
public:
  IODBDesignDataBOM() {}
  virtual ~IODBDesignDataBOM() {}

  static IODBDesignNodeData* castBOM(STSymbolTableNode* node);
  static const IODBDesignNodeData* castBOM(const STSymbolTableNode* node);

  virtual void writeBOMSignature(ZostreamDB& out) const;
  virtual ReadStatus readBOMSignature(ZistreamDB& in, UtString* errMsg);
  virtual Data allocBranchData();
  virtual Data allocLeafData();
  virtual void freeBranchData(const STBranchNode*, Data* bomdata);
  virtual void freeLeafData(const STAliasedLeafNode*, Data* bomdata);

  virtual void preFieldWrite(ZostreamDB&) {}
  virtual void writeLeafData(const STAliasedLeafNode*, ZostreamDB&) const {}
  virtual void writeBranchData(const STBranchNode*, ZostreamDB&,
                               AtomicCache*) const {}
  virtual ReadStatus preFieldRead(ZistreamDB&);
  virtual ReadStatus readLeafData(STAliasedLeafNode*, ZistreamDB&, MsgContext*);
  virtual ReadStatus readBranchData(STBranchNode*, ZistreamDB&, MsgContext*);
  virtual void printBranch(const STBranchNode* /*branch*/) const;
  virtual void printLeaf(const STAliasedLeafNode* /*leaf*/) const;
  //! Return BOM class name
  virtual const char* getClassName() const;

  //! Write the BOMData for a branch node
  virtual void xmlWriteBranchData(const STBranchNode* branch,UtXmlWriter* xmlWriter) const;

  //! Write the BOMData for a leaf node
  virtual void xmlWriteLeafData(const STAliasedLeafNode* leaf,UtXmlWriter* xmlWriter) const;

};

//! Manages elaborated and unelaborated symbol tables.
/*!
  The symbol tables are populated during a pre-population walk. The walker walks
  into all declaration scopes except sub-programs i.e tasks/procedures/functions.
  It adds all scopes and the declared nets in those scopes to both the tables.
  The elaborated symbol table BOM contains pointers from elaborated scopes to
  unelaborated scopes. The leafs are marked as protected mutable if they're
  depositable or forcible. This information is needed during population to
  avoid constant propagation protected mutable nets.
*/
class IODBDesignDataSymTabs
{
public:

  // Symbol table types.
  enum SymTabT {
    eUnelab, eElab
  };

  // Symbol table node types.
  enum SymTabNodeT {
    eLeaf, eBranch
  };

  IODBDesignDataSymTabs(AtomicCache* cache);
  ~IODBDesignDataSymTabs();

  //! Add node of branch/leaf type to elab/unelab symbol table.
  STSymbolTableNode* createNode(STBranchNode* parent, StringAtom* child,
                                SymTabT stTyp, SymTabNodeT nodeTyp);
  //! Find node in elab/unelab symbol table.
  STSymbolTableNode* findNode(STBranchNode* parent, StringAtom* child,
                              SymTabT stTyp);
  //! Mark the node with given path as protected mutable. Return false
  //! if path does not exist.
  bool markProtMutable(const UtStringArray& elemList);
  //! Is the net marked protected mutable.
  bool isUnelabProtMutable(NUNet* net) const;

  //! Get elaborated symbol table.
  STSymbolTable* getElabSymTab() {
    return mElabST;
  }

  //! Get user type creation factory.
  UserTypeFactory* getUserTypeFactory() {
    return mTypeFactory;
  }

  //! \return the unelaborated symbol table
  const STSymbolTable *getUnelabSymbolTable () const { return mUnelabST; }

  //! Get the atomic cache
  AtomicCache* getAtomicCache() const {return  mAtomicCache; }

private:

  bool markProtMutable(const UtStringArray& elemList, SInt32 elemListIndex,
                       STBranchNode* parent);

  AtomicCache* mAtomicCache;

  STSymbolTable* mUnelabST;
  IODBDesignDataBOM mUnelabBOM;
  
  STSymbolTable* mElabST;
  IODBDesignDataBOM mElabBOM;

  UserTypeFactory* mTypeFactory;
};

#endif
