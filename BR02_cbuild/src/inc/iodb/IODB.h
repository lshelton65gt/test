// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef IODB_H
#define IODB_H


#include "util/CarbonPlatform.h"
#include "symtab/STSymbolTable.h"
#include "util/SourceLocator.h"
#include "util/MsgContext.h" // for Severity
#include "util/Loop.h"
#include "util/LoopFilter.h"
#include "util/Iter.h"

class HdlHierPath;
class SCHScheduleFactory;
class STNodeSelectDB;
class StrToken;
class SCHSignature;
class IODBTypeDictionary;
class AtomicCache;
class CarbonExpr;
class ESFactory;
class DynBitVector;
class DynBitVectorFactory;
class UserTypeFactory;

//! IODB class -- parses wildcard (eventually) net specifications
/*!

From UserRequirements.doc:

\verbatim

collapseClock <clock name list> This will cause all the listed clock
signals to be treated as one clock (for scheduling purposes).

divideClock <base clock name, divisor, edge,  phase, clock name list>
This will cause all the listed clocks to be treated as <divisor>
frequency divided from the base clock.  Edge (pos, neg) determines what
edge of the base clock the clock is derived from.  Phase (high, low)
determines if the clock starts low or high.

invertClock <base clock, clock name list> : This will cause all the
listed clocks to be treated as inversions of the base clock.

fastClock <clock names> Identify the fastest running clocks which should
be executed unconditionally to increase performance.

slowClock <clock names> Identify clocks that are routed slowly in the
chip, so that they are slower than async resets.  If a slow clock
rises at the same time as a reset is deasserted, then the clock block
runs.  By default, clocks are assumed to be faster than resets, and
so resets will be assumed to still be deasserted in a race.  See also
fastReset.  A slowClock will be slower than any reset, but those resets
will not necessarily be faster than other clocks.

fastReset <reset names> Identify resets that are routed fast in the
chip, so that they are faster than clocks.  If a clock
rises at the same time as a reset is deasserted, then the clock block
runs.  By default, clocks are assumed to be faster than resets, and
so resets will be assumed to still be deasserted in a race.  See also
slowClock.  A fastReset will be faster than any clock, but those clocks
will not necessarily be slower than other resets.

tieNet <value, signal name> This causes the compiler to tie the
specified signal to the specified value (0 or 1). As a byproduct, it
could case the logic associated with the signal to be removed by the
compiler.  This will allow us to get rid of scan and other useless
signals that may pervade the design.

forceSignal <signal names> Allow the named signals to be driven from the
outside world via force and release verilog and API commands.  The
compiler can automatically detect the signals that are forced in the via
the verilog force keyword.

observeSignal <signal names> Allow direct read access to signal name
from the external environment.

depositSignal <signal names> Allow direct write access to signal names
from the external environment.

ignoreOutput <signal name list> : This tells the compiler that the
specified output signals are not needed and that their associated logic
can be optimized away.

cModuleBegin <module name> Mark a Verilog module as a c-model. The
module declaration will be used to get the port names and
directions. Any statements in the Verilog body will be ignored. This
directive must be followed by a number of other cXXX directives and
finally terminate with a cModuleEnd directive.

cPortBegin <port name> Provide information about an output port for a
c-model. There must be a preceding cModuleBegin directive. This
directive can be followed by cTiming and cFanin directives. It must be
terminated with a cPortEnd directive.

cTiming <timing-type> [<clock-name>] Provides timing information for
the preceding cPortBegin output port. The timing type can be done of
rise, fall, or async. Rise and fall indicate that the pin is fed by
one or more synchronous elements with the given clock edge. Async
indicates that there is at least one asynchronous path from this
outputs to some/all of the c-models inputs.

cFanin <net1> [ <net2> ... <netN> ] Provides flow information from
outputs to inputs. This flow information is associated with the
previous cTiming directive or is applied to the async timing if there
is no previous cTiming directive.

cPortEnd Terminates the information for a given c-model output port.

cModuleEnd Terminates the information for a given c-model.

substituteModule <old_mod> portsBegin o1 o2 o3 portsEnd \
                 <new_mod> portsBegin n1 n2 n3 portsEnd
  Instances of <old_mod> will be replaced with instances of <new_mod>.
  (o1, o2, o3...) are formal ports of the old module. 

blastNet <net name list> Instructs cbuild to bit-blast the named nets.

errorMsg <mnemonics> : Change mnemonics compiler message to error
messages.

warningMsg <mnemonics> : Change mnemonics compiler messages to warning
messages.

infoMsg <mnemonics> : Change mnemonics compiler messages to
informational messages.

silentMsg <mnemonics> : Suppress printing of mnemonics compiler
messages.

delayedClock <net name list> Instructs cbuild that the given net(s)
are delayed versions of a clock and should be treated as such. An
example in which to use this is as follows:

    assign clk_buf = #1 clk;
    always @ (posedge clk)
      q <= clk_buf;

Marking clk_buf as a delayed clock will prevent optimizations from
using the new value of clk when computing q. It really should be based
on the delayed (old) value of clk.

 \endverbatim
*/
class IODB
{
public: CARBONMEM_OVERRIDES
  //! base class constructor
  IODB(AtomicCache*, STSymbolTable*, MsgContext*,
       SCHScheduleFactory*);

  //! destructor
  ~IODB();

  //! Retrieve the msgcontext passed into the contructor
  MsgContext* getMsgContext() const;

  //! reset the schedule factory
  void putScheduleFactory(SCHScheduleFactory* sf) {mScheduleFactory = sf;}

  //! containers used for holding the various sets of design elements
  typedef UtHashSet<STSymbolTableNode*> NameSet;

  //! containers used for holding the various sets of sets of design elements
  typedef UtHashMap<STSymbolTableNode*, NameSet*> NameSetMap;

  //! map names to integers
  typedef UtHashMap<STSymbolTableNode*, UInt32> NameIntMap;

  //! containers used for holding constant information
  typedef UtHashMap<const STSymbolTableNode*, const DynBitVector*> NameValueMap;

  //! containers used for holding mappings of nodes to expressions
  typedef UtHashMap<STAliasedLeafNode*, CarbonExpr*> NameExprMap;

  //! containers used for holding arrays of nodes
  typedef UtArray<STSymbolTableNode*> NameVec;

  //! iterator for contained design elements
  typedef Iter<STSymbolTableNode*> NameSetLoop;

  //! iterator for contained design elements
  typedef NameSetMap::SortedLoop NameSetMapLoop;

  //! iterator for map of names to integers
  typedef NameIntMap::SortedLoop NameIntMapLoop;

  //! iterator for contained design elements and const values
  typedef NameValueMap::SortedLoop NameValueLoop;

  //! iterator for expression/leaf node pairs
  typedef NameExprMap::SortedLoop NameExprMapLoop;

  //! Type for looping over a name array
  typedef Loop<NameVec> NameVecLoop;

  //! Iterate over all enable expressions
  NameExprMapLoop loopEnableExprs() { return mEnableExprs.loopSorted();}
  
  //! Iterate over all exposed nets
  NameSetLoop loopExposed()
  {
    return NameSetLoop::create(mExposedNets.loopSorted());
  }

  //! Iterate over all fast clock nets
  NameSetLoop loopFastClocks()
  {
    return NameSetLoop::create(mFastClocks.loopSorted());
  }

  //! Iterate over all traced nets
  NameSetLoop loopTraced()
  {
    return NameSetLoop::create(mTracedNets.loopSorted());
  }

  //! Type for the NameIntMap value
  typedef std::pair<STSymbolTableNode*, UInt32> NameIntValue;

  //! Abstraction for the types of deposits
  enum DepositTypes {
    eDTSimple,          //!< depositSignal directive
    eDTSystemC,         //!< scDeposit directive
    eDTFrequent,        //!< depositSignalFrequent directive
    eDTInfrequent,      //!< depositSignalInfrequent directive
    eDTModelValidation, //!< mvDepositSignal directive
    eDTODIdleDeposit,   //!< implicitly depositable due to onDemandIdleDeposit directive
    eDTNumTypes         //!< Number of directive types (used for all loop)
  };

  //! Abstraction for a mask of the various deposit types
  enum DepositMask {
    eDTSimpleMask          = (1 << eDTSimple),
    eDTSystemCMask         = (1 << eDTSystemC),
    eDTFrequentMask        = (1 << eDTFrequent),
    eDTInfrequentMask      = (1 << eDTInfrequent),
    eDTModelValidationMask = (1 << eDTModelValidation),
    eDTAllMask             = ((1 << eDTNumTypes) - 1)
  };

  //! Abstraction for the types of observes
  enum ObserveTypes {
    eOTSimple,          //!< observeSignal directive
    eOTSystemC,         //!< scObserveSignal directive
    eOTModelValidation, //!< mvObserveSignal directive
    eOTNumTypes         //!< Number of directive types (used for all loop)
  };

  //! Abstraction for a mask of the various observe types
  enum ObserveMask {
    eOTSimpleMask          = (1 << eOTSimple),
    eOTSystemCMask         = (1 << eOTSystemC),
    eOTModelValidationMask = (1 << eOTModelValidation),
    eOTAllMask             = ((1 << eOTNumTypes) - 1)
  };

  //! Filter to only get a certain type of deposits
  class DepositFilter
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    DepositFilter(DepositMask depositMask) : mDepositMask(depositMask)
    {
      INFO_ASSERT((mDepositMask & ~eDTAllMask) == 0,
                  "Deposit filter created with invalid deposit mask");
    }

    //! Filter function
    bool operator()(NameIntValue value) const
    {
      return (value.second & mDepositMask) != 0;
    }

  private:
    DepositMask mDepositMask;
  }; // class DepositFilter

  //! Filter to only get a certain type of observes
  class ObserveFilter
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    ObserveFilter(ObserveMask observeMask) : mObserveMask(observeMask)
    {
      INFO_ASSERT((mObserveMask & ~eOTAllMask) == 0,
                  "Observe filter created with invalid observe mask");
    }

    //! Filter function
    bool operator()(NameIntValue value) const
    {
      return (value.second & mObserveMask) != 0;
    }

  private:
    ObserveMask mObserveMask;
  }; // class ObserveFilter

  //! Abstraction for a filtered iteration
  typedef LoopFilter<NameIntMapLoop, DepositFilter> DepositFilterLoop;

  //! Abstraction for a filtered iteration
  typedef LoopFilter<NameIntMapLoop, ObserveFilter> ObserveFilterLoop;

  //! Class to make a map loop look like a set loop
  class NameMapToSetFunctor
  {
  public: CARBONMEM_OVERRIDES
    typedef STSymbolTableNode* value_type;
    typedef STSymbolTableNode* reference;
    STSymbolTableNode* operator()(NameIntValue value);
    STSymbolTableNode* operator()(NameIntValue value) const;
  };

  //! Loop functor to make a map loop look like a set loop for deposits
  typedef LoopFunctor<DepositFilterLoop, NameMapToSetFunctor> DepositLoop;

  //! Loop functor to make a map loop look like a set loop for observes
  typedef LoopFunctor<ObserveFilterLoop, NameMapToSetFunctor> ObserveLoop;

  //! Iterate over all depositable nets
  NameSetLoop loopDeposit()
  {
    DepositFilter filter(eDTAllMask);
    DepositFilterLoop filterLoop(mDepositNets.loopSorted(), filter);
    DepositLoop depositLoop(filterLoop);
    return NameSetLoop::create(depositLoop);
  }

  //! Iterate over the deposit map (including the deposit flags)
  NameIntMapLoop loopDepositMap()
  {
    return mDepositNets.loopSorted();
  }

  //! Iterate over all frequently depositable nets
  NameSetLoop loopFrequentDeposit()
  {
    DepositFilter filter(eDTFrequentMask);
    DepositFilterLoop filterLoop(mDepositNets.loopSorted(), filter);
    DepositLoop depositLoop(filterLoop);
    return NameSetLoop::create(depositLoop);
  }

  //! Number of depositable nets
  UInt32 numDeposits() const { return mDepositNets.size(); }

  //! Remove given name from set of depositable nets
  void removeDeposit(STSymbolTableNode *name) {mDepositNets.erase(name);}

  //! Iterate over all observed nets
  NameSetLoop loopObserved()
  {
    ObserveFilter filter(eOTAllMask);
    ObserveFilterLoop filterLoop(mObservedNets.loopSorted(), filter);
    ObserveLoop observeLoop(filterLoop);
    return NameSetLoop::create(observeLoop);
  }

  //! Number of observable nets
  UInt32 numObserved() const { return mObservedNets.size(); }

  //! Iterate over the observed map (including the observed flags)
  NameIntMapLoop loopObservedMap()
  {
    return mObservedNets.loopSorted();
  }

  //! Iterate over all forcible nets
  NameSetLoop loopForced()
  {
    return NameSetLoop::create(mForcedNets.loopSorted());
  }

  //! Iterate over all nets marked as delayed clocks
  NameSetLoop loopDelayedClocks()
  {
    return NameSetLoop::create(mDelayedClocks.loopSorted());
  }

  //! Iterate over all ignored outputs
  NameSetLoop loopIgnoreOutputs()
  {
    return NameSetLoop::create(mIgnoreOutputNets.loopSorted());
  }

  //! Iterate over all collapsed clock sets
  NameSetMapLoop loopCollapseClocks() {return mCollapsedClocks.loopSorted();}

  //! Iterate over all assigned clock frequencies
  NameIntMapLoop loopClockSpeeds() {return mClockSpeeds.loopSorted();}

  //! Iterate over all onDemand state offsets
  NameIntMapLoop loopOnDemandStateOffsets() {return mOnDemandStateOffsets.loopSorted();}

  //! Iterate over all tie nets and their values
  NameValueLoop loopTieNets() {return mTieNets->loopSorted();}

  //! Iterate over all constant nets and their values
  NameValueLoop loopConstNets() {return mConstNets->loopSorted();}

  //! Iterate over all inputs
  NameSetLoop loopInputs()
  {
    return NameSetLoop::create(mInputs.loopSorted());
  }

  //! Iterate over all outputs
  NameSetLoop loopOutputs()
  {
    return NameSetLoop::create(mOutputs.loopSorted());
  }

  //! Iterate over all bidirectional nets
  NameSetLoop loopBidis()
  {
    return NameSetLoop::create(mBidis.loopSorted());
  }

  //! Iterate over all clocks nets
  NameSetLoop loopClocks()
  {
    return NameSetLoop::create(mClocks.loopSorted());
  }

  //! Iterate over all nets that sensitize c-models
  NameSetLoop loopCModelSenseNets()
  {
    return NameSetLoop::create(mCModelSenseNets.loopSorted());
  }

  //! Iterate over all clock tree nets
  /*!
    These are primary inputs or depositables that indirectly drive
    clock inputs on flops.
   */
  NameSetLoop loopClockTree()
  {
    return NameSetLoop::create(mClockTree.loopSorted());
  }

  //! Iterate over all primary clock inputs
  /*!
    These are primary inputs that directly drive clock inputs on flops.
  */
  NameSetLoop loopPrimaryClocks()
  {
    return NameSetLoop::create(mPrimaryClocks.loopSorted());
  }

  //! Get the number of Primary clocks
  UInt32 numPrimaryClocks() const {return mPrimaryClocks.size(); }

  //! Iterater over all async nets
  NameSetLoop loopAsyncs()
  {
    return NameSetLoop::create(mAsyncs.loopSorted());
  }
  
  //! Iterater over all async output nets
  NameSetMapLoop loopAsyncOutputs() { return mAsyncOutputs.loopSorted(); }
  
  //! Iterater over all async fanin
  NameSetLoop loopAsyncFanin(const STSymbolTableNode* node);
  
  //! Iterater over all IgnoreSynthCheck nets
  NameSetLoop loopIgnoreSynthCheck()
  {
    return NameSetLoop::create(mIgnoreSynthCheck.loopSorted());
  }

  //! Iterater over all async output nets
  NameSetLoop loopAsyncDeposits()
  {
    return NameSetLoop::create(mAsyncDeposits.loopSorted());
  }
  
  //! Iterater over all posedge async reset nets
  NameSetLoop loopAsyncPosResets()
  {
    return NameSetLoop::create(mAsyncPosResets.loopSorted());
  }
  
  //! Iterater over all negedge async reset nets
  NameSetLoop loopAsyncNegResets()
  {
    return NameSetLoop::create(mAsyncNegResets.loopSorted());
  }

  //! Iterate over all hidden top-most modules
  NameSetLoop loopHiddenModules()
  {
    return NameSetLoop::create(mHiddenModuleTops.loopSorted());
  }
  
  //! Iterate over all observed nets
  NameSetLoop loopScObserved()
  {
    ObserveFilter filter(eOTSystemCMask);
    ObserveFilterLoop filterLoop(mObservedNets.loopSorted(), filter);
    ObserveLoop observeLoop(filterLoop);
    return NameSetLoop::create(observeLoop);
  }

  //! Iterate over all depositable nets
  NameSetLoop loopScDeposit()
  {
    DepositFilter filter(eDTSystemCMask);
    DepositFilterLoop filterLoop(mDepositNets.loopSorted(), filter);
    DepositLoop depositLoop(filterLoop);
    return NameSetLoop::create(depositLoop);
  }

  //! Iterate over all observed nets
  NameSetLoop loopMvObserved()
  {
    ObserveFilter filter(eOTModelValidationMask);
    ObserveFilterLoop filterLoop(mObservedNets.loopSorted(), filter);
    ObserveLoop observeLoop(filterLoop);
    return NameSetLoop::create(observeLoop);
  }

  //! Iterate over all depositable nets
  NameSetLoop loopMvDeposit()
  {
    DepositFilter filter(eDTModelValidationMask);
    DepositFilterLoop filterLoop(mDepositNets.loopSorted(), filter);
    DepositLoop depositLoop(filterLoop);
    return NameSetLoop::create(depositLoop);
  }

  //! Iterate over the top level ports in order
  NameVecLoop loopPrimaryPorts() { return NameVecLoop(mPrimaryPorts); }

  //! Iterate over posedge schedule triggers
  NameSetLoop loopPosedgeScheduleTriggers() 
  {
    return NameSetLoop::create(mPosedgeScheduleTriggers.loopSorted());
  }

  //! Iterate over negedge schedule triggers
  NameSetLoop loopNegedgeScheduleTriggers() 
  {
    return NameSetLoop::create(mNegedgeScheduleTriggers.loopSorted());
  }

  //! Iterate over schedule triggers which also feed a data path
  NameSetLoop loopScheduleTriggersUsedAsData()
  {
    return NameSetLoop::create(mScheduleTriggersUsedAsData.loopSorted());
  }

  //! Loop over OnDemand idle deposit nets
  NameSetLoop loopOnDemandIdleDeposit()
  {
    return NameSetLoop::create(mOnDemandIdleDepositNets.loopSorted());
  }

  //! Iterate over OnDemand excluded nets
  NameSetLoop loopOnDemandExcluded()
  {
    return NameSetLoop::create(mOnDemandExcludedNets.loopSorted());
  }

  //! Iterate over OnDemand runtime excluded nets
  NameSetLoop loopOnDemandRuntimeExcluded()
  {
    return NameSetLoop::create(mOnDemandRuntimeExcludedNets.loopSorted());
  }

  //! Return the iodb symbol table
  STSymbolTable* getIOSymbolTable() {return mDesignTable /*&mIOTable*/;}

  //! Return the design symbol table
  const STSymbolTable* getDesignSymbolTable() const { return mDesignTable; }

  //! Return modifiable design symbol table
  STSymbolTable* getDesignSymbolTable() {
    const IODB* me = const_cast<const IODB*>(this);
    return const_cast<STSymbolTable*>(me->getDesignSymbolTable());
  }

  const HdlHierPath* getHdlHier() const {
    return mHdl;
  } 


  //! Get a pointer to the entire type dictionary
  IODBTypeDictionary* typeDictionary() {return mTypeDictionary;}

  //! Get a pointer to the user design type factory
  UserTypeFactory* userTypeFactory() { return mUserTypeFactory; }

  //! True if node is an i/o
  bool isPrimary(const STSymbolTableNode* node) const;
  bool isPrimaryInput(const STSymbolTableNode* node) const;
  bool isPrimaryOutput(const STSymbolTableNode* node) const;
  bool isPrimaryBidirect(const STSymbolTableNode* node) const;
  bool isClock(const STSymbolTableNode* node) const;
  bool isClockTree(const STSymbolTableNode* node) const;
  bool isPrimaryClock(const STSymbolTableNode* node) const;
  bool isAsyncNode(const STSymbolTableNode* node) const;
  bool isAsyncOutputNode(const STSymbolTableNode* node) const;
  bool isAsyncDepositNode(const STSymbolTableNode* node) const;
  bool hasOutputEvent(const SCHSignature* signature) const;
  bool hasPosedgeEvent(const SCHSignature* node) const;
  bool hasNegedgeEvent(const SCHSignature* node) const;
  bool isAsync(const SCHSignature* node) const;
  bool isSync(const SCHSignature* node) const;
  bool isPosedgeReset(const STSymbolTableNode* node) const;
  bool isNegedgeReset(const STSymbolTableNode* node) const;
  bool isForcible(const STSymbolTableNode* node) const;
  bool isDepositable(const STSymbolTableNode*) const;
  bool isScDepositable(const STSymbolTableNode*) const;
  bool isMvDepositable(const STSymbolTableNode*) const;
  bool isFrequentDepositable(const STSymbolTableNode*) const;
  bool isInfrequentDepositable(const STSymbolTableNode*) const;
  bool isObservable(const STSymbolTableNode*) const;
  bool isScObservable(const STSymbolTableNode*) const;
  bool isMvObservable(const STSymbolTableNode*) const;
  bool isFastClock(const STSymbolTableNode*) const;
  bool isSlowClock(const STSymbolTableNode*) const;
  bool isFastReset(const STSymbolTableNode*) const;
  //! Was node specified in a exposeSignal directive?
  bool isExposed(const STSymbolTableNode* node) const;
  //! Assigned an override mask based on net analysis
  bool hasConstantOverride(const STSymbolTableNode*) const;
  bool isWrapperPort2State(const STSymbolTableNode* node) const;
  bool isWrapperPort4State(const STSymbolTableNode* node) const;
  bool isOnDemandIdleDeposit(const STSymbolTableNode* node) const;
  bool isOnDemandExcluded(const STSymbolTableNode* node) const;
  bool isTied(const STSymbolTableNode* node) const;
  const DynBitVector* getTieValue(const STSymbolTableNode* node) const;
  UInt32 getClockSpeed(const STSymbolTableNode* node) const;
  UInt32 getDefaultClockSpeed() const {return mDefaultSpeed;}
  UInt32 getOnDemandStateSize() const {return mOnDemandStateSize;}
  
  //! Return true if the given node has a specified clock speed, false otherwise.
  /*!
   * This is needed because getClockSpeed() returns 0 when there is no clock
   * speed, but 0 is a valid value for a clock speed, so we need this to
   * differentiate.
   */
  bool hasClockSpeed(const STSymbolTableNode* node) const;

  //! Return the speed of the highest clock known.
  //  If no clock speeds are known, this will return same as getDefaultClockSpeed().
  UInt32 getHighestClockSpeed() const;
  
  //! Returns true if the module is hidden
  /*!
    If this is a leaf the parent module is checked
    \sa isNetHidden
  */
  bool isModuleHidden(const STSymbolTableNode* moduleOrLeaf) const;

  //! Returns true if the net is hidden
  /*!
    Returns true if this net is within a hidden module and has not
    been exposed. 
    A net can be exposed with any of the exposeSignal, observeSignal,
    depositSignal, or forceSignal directives
  */
  bool isNetHidden(const STAliasedLeafNode* leaf) const;

  //! Is this net exposed via directives?
  /*!
    This is not the same as isExposed. This returns true if the net
    was exposed with the exposeSignal, depositSignal, forceSignal, or
    observeSignal directives.
  */
  bool isNetExposed(const STAliasedLeafNode* leaf) const;

  //! Extension for full db filename
  static const char* scFullDBExt;
  //! Extension for io db filename
  static const char* scIODBExt;
  //! Extension for GUI db filename
  static const char* scGuiDBExt;
  //! Extension for auto db filename
  /*!
    This isn't a real extension, but rather a user-friendly string
    that can be used for error reporting.
   */
  static const char* scAutoDBExt;

  //! Set the target compiler
  void putTargetCompiler(TargetCompiler tcomp);

  //! Get the target compiler
  /*!
    Only valid after setting it with putTargetCompiler().
  */
  TargetCompiler getTargetCompiler() const;

  //! Set the target compiler version
  void putTargetCompilerVersion(TargetCompilerVersion version);

  //! Get the target compiler version
  TargetCompilerVersion getTargetCompilerVersion() const;

  //! Set the target compiler's -Wc flags (if any)
  void putTargetFlags (const UtString& tflags);

  //! Get the flags
  const char* getTargetFlags () const;

  //! get the atomic cache
  AtomicCache* getAtomicCache() const {return mAtomicCache;}

  //! Puts whether or not this is a noInputFlow compile
  void putNoInputFlow(bool noInputFlowSpecified);

  //! Returns true if this is noInputFlow compile
  bool isNoInputFlow() const;

  //! Number of bidirect types in the entire design
  /*!
    This number will be the same whether or not this is an io or a
    full database. It is used mainly as a maximum number.
  */
  UInt32 numTotalBidis() const;

  //! Number of primary inputs
  UInt32 numPrimaryInputs() const { return mInputs.size(); }
  //! Number of primary bidirects
  UInt32 numPrimaryBidis() const { return mBidis.size(); }
  
  //! Put the ID string for the design
  /*!
    The string is a unique identifier for the design. So, it includes
    the name of the design, the revision of the compiler and a
    timestamp.
  */
  void putDesignId(const UtString& designId);

  //! Get the design id
  //! Get the ID string for the design
  /*!
    \sa putDesignID
  */
  const char* getDesignId() const;

  
  //! Set the interface name (from -o lib<ifacetag>.a
  /*!
    The string is the name of the design that is being compiled. It is
    used to generate the carbon_<ifacetag>_create function.
  */
  void putIfaceTag(const UtString& ifaceTag);

  //! Get the interface name
  const char* getIfaceTag() const;

  //! Set the top-level module name
  /*!
    This is the name of the top-level module
   */
  void putTopLevelModuleName(const char* topModName);

  //! Get the top-level module name
  const char* getTopLevelModuleName() const;

  //! Set the SystemC wrapper module name
  /*!
    This is the name of the top-level SC_MODULE name for the wrapper.
    This is specified using the -systemCModuleName switch. This should
    be set to the top-level module name if the switch was not
    specified.
  */
  void putSystemCModuleName(const char* sysCName);
  
  //! Get the SystemC Wrapper module name
  const char* getSystemCModuleName() const;

  //! Set a Carbon Model type flag
  void setVHMTypeFlags(CarbonVHMTypeFlags vhmType);

  //! Clear a Carbon Model type flag
  void clearVHMTypeFlags(CarbonVHMTypeFlags vhmType);

  //! Get the Carbon Model type flags
  CarbonVHMTypeFlags getVHMTypeFlags() const;
  
  //! True if this database allows force subordinate leaves
  /*! 
    Force subordinates are the value and mask of a forcible net. Prior
    to Oct. 2006, those subordinates did not have entries in the
    db. They used the forcible net's alias leaf. 
  */
  bool allowForceSubordinates() const;

  //! Get the schedule factory
  SCHScheduleFactory* getSchedFactory() const { return mScheduleFactory; }

  //! Get the value override mask for the given node
  /*!
    \returns NULL if the node does not have an override mask
    \param node Node to check. The storage of this node is the one
    that would be saved in the mConstNets map.
    
    The DynBitVector* that is returned has the following format:
    - It has 3x the number of words needed to represent the value.
    - The first set of words is the control mask. If any of these bits
    are 1, then the next two sets of words are interesting. For each
    bit of the net, there is a control bit.
    - The next set of words is the value mask.
    - The final set of words is the xz mask. If any of these bits are
    set then the corresponding bit is either x or z, depending on the
    value mask.

    Value table

    \verbatim
              value
              -----
              0  1
    xzmask  0 0  1
            1 z  x
    \endverbatim

    If the xzmask is 0, but the control mask is 1, then the value bit
    means that the bit is constant.
  */
  const DynBitVector* getConstNetBitMask(const STAliasedLeafNode* node) const;

  //! Compose the hierarchical pathname for symNode
  void composeName(UtString* name, const STSymbolTableNode* symNode) const;

  //! Returns the name of the value field of the forcible net.
  /*!
    This doesn't check if net is forcible. It assumes it is and
    creates the value name from it.
    The name will be something like:
    \<netname\>_$shellnet_value, where netname is the name of the
    original net.

    This does not return a full path to the name.

    \returns interned name in strCache
  */
  static StringAtom* sGetForceValueName(const STAliasedLeafNode* net, AtomicCache* strCache);

  //! Returns the name of the mask field of the forcible net.
  /*!
    This doesn't check if net is forcible. It assumes it is and
    creates the mask name from it.
    The name will be something like:
    \<netname\>_$shellnet_mask, where netname is the name of the
    original net.

    This does not return a full path to the name.
    \returns interned name in strCache
  */
  static StringAtom* sGetForceMaskName(const STAliasedLeafNode* net, AtomicCache* strCache);
  
  
  //! Given a constnet bitmask, is the given width overridden?
  /*!
    This only returns true if the override mask is all ones for the
    given width. No checks are done to validate the width. The width
    must correspond to the overrideMask which corresponds to the
    bitwidth of the net that it describes.
  */
  static bool sIsOverrideMaskAllOnes(const DynBitVector* overrideMaskBV, 
                                     UInt32 width);

  //! Get the bitvector factory
  DynBitVectorFactory* getBVFactory() const { return mBVPool; }

  //! Enum for database entries
  /*!
    This starts at a non-zero number just for uniqueness.
  */
  enum DBEntry {
    eEntrySymTab=755, //!< STSymbolTable entry
    eEntryTypeDB, //!< TypeDictionary entry
    eEntryDesignInfo, //!< DesignInfo entry
    eEntryCustDB, //!< UtCustomerDB enry
    eEntryVersion, //!< IODB sig and version
    eEntryCapabilities, //!< set of fields (strings) known to this database
    eEntrySourceLocator //!< dumped SourceLocatorFactory
  };

  //! Modifies buf with the iodb entry name into the zip archive
  const char* getEntryName(UtString* buf, DBEntry entryType) const;

  //! Get the database type
  CarbonDBType getDBType() const { return mDesignInfo->getDBType(); }

  //! Get the version of the IODB that was read.
  UInt32 getIODBVersion() const;

  //! Is the given node used as a live input?
  /*!
    A dead input may have a data path that goes nowhere. A live input
    has a datapath and/or a clk path to logic that eventually
    affects outputs or observables.

    \note Only explicit primary ports and scDepositables can return
    true. Their aliases cannot.
  */
  bool isUsedAsLiveInput(const STSymbolTableNode* node) const;

  //! Is the given node used as a live output?
  /*!
    A dead output is not driven by anything.

    \note Only explicit primary ports and scObservables can return
    true. Their aliases cannot.
  */
  bool isUsedAsLiveOutput(const STSymbolTableNode* node) const;

  //! Is the given node a positive edge schedule trigger?
  /*!
    If a net drives a schedule on its positive edge this will return
    true.
    \note Only explicit primary ports and scDepositables can return
    true. Their aliases cannot.
  */
  bool isPosedgeScheduleTrigger(const STSymbolTableNode* node) const;


  //! Is the given node a negative edge schedule trigger?
  /*!
    If a net drives a schedule on its negative edge this will return
    true.
    \note Only explicit primary ports and scDepositables can return
    true. Their aliases cannot.
  */
  bool isNegedgeScheduleTrigger(const STSymbolTableNode* node) const;

  //! Compute the set of schedule triggers that occur on both edges
  /*!
    \param bothEdges Will be appended to with nodes that trigger
    schedules on both edges. This set will not be cleared. Must not be
    NULL.
  */
  void computeBothEdgeScheduleTriggers(NameSet* bothEdges) const;

  //! Is this a schedule trigger that is also used in the data path?
  /*!
    Clocks as data always have to be updated when deposited, but
    may only need to run the schedule on a given edge, if they only
    drive a single edge.

    This returns true only if node is a schedule trigger and it is
    used in a data path.
  */
  bool isScheduleTriggerUsedAsData(const STSymbolTableNode* node) const;
  
  //! Put whether or not the port interface is codegened
  /*!
    Defaults to true in case an old iodb with a systemCWrapper
    generation is read with a new libcarbon, so the shell schedule
    flag will not break those compilations.
  */
  void putIsPortInterfaceGenerated(bool isGenerated);
  
  //! Returns true if the port interface code was codegened
  /*!
    Also returns true if this is an older database that didn't have
    this information.
  */
  bool isPortInterfaceGenerated() const;

  //! Puts the software version string into the database.
  void putSoftwareVersion(const char* versionStr);


  //! Return the software version string
  const char* getSoftwareVersion() const;

  //! Put whether or not clock glitch detection is codegened
  void putIsClockGlitchGenerated(bool isGenerated);
  
  //! Returns true if clock glitch detection was codegened
  bool isClockGlitchGenerated() const;


  //! Get the Signature and version of this libcarbon's iodb
  /*!
    Returns the version and modifies the buffer with the signature
    Call this to compare the version of the input db versus the
    version of the software.
  */
  static UInt32 sGetSigAndVersion(UtString* signature);

  //! Get the version of the DB that was read
  UInt32 getDBVersion() {return mInputDBVersion;}
  
private:

  void init(AtomicCache* cache, STSymbolTable* st,
            MsgContext* msgs,
            SCHScheduleFactory*);

  static const SInt32 cEndOfRecord;

  

protected:
  //! Add a capability
  void addCapability(const char*);

  //! Does the database we are reading have the specified capability?
  bool testCapability(const char*) const;

  NameSet mExposedNets;
  NameSet mForcedNets;
  NameSet mAsyncResetPaths;
  NameSet mFastClocks;
  NameSet mSlowClocks;
  NameSet mFastResets;
  NameSet mIgnoreSynthCheck;
  NameIntMap mObservedNets;

  //! A set of nets to debug possible scheduler issues
  /*! It is not clear if this is useful or not or whether other
   *  compiler passes will use this data. The idea is that the AE can
   *  supply a set of problematic nets and each pass can use them to
   *  trace back the logic and print relevant information to help
   *  debug a problem.
   */
  NameSet mTracedNets;

  NameSet mInputNets;
  NameIntMap mDepositNets;
  NameSet mIgnoreOutputNets;
  NameSet mInputs;
  NameSet mOutputs;
  NameSet mBidis;
  NameSet mClocks;
  NameSet mCModelSenseNets;
  NameSet mClockTree;
  NameSet mPrimaryClocks;
  NameSet mAsyncs;
  NameSetMap mAsyncOutputs;
  NameSet mAsyncDeposits;
  NameSet mAsyncPosResets;
  NameSet mAsyncNegResets;
  NameSet mLevelHighResets;
  NameSet mLevelLowResets;
  NameSet mWrapPort2StateNets;
  NameSet mWrapPort4StateNets;
  NameVec mPrimaryPorts;
  NameSet mDelayedClocks;
  NameSet mLiveInputDirNets;
  NameSet mLiveOutputDirNets;
  NameSet mPosedgeScheduleTriggers;
  NameSet mNegedgeScheduleTriggers;
  NameSet mScheduleTriggersUsedAsData;
  NameSet mOnDemandIdleDepositNets;
  NameSet mOnDemandExcludedNets;
  NameSet mEmptySet;

  /*
    List of modules that are hidden. This does NOT include child
    branches.
  */
  NameSet mHiddenModuleTops;
  
  //NameSet mInputScheduleNets;
  //  NameSet mAsyncScheduleNets;
  NameExprMap mEnableExprs;

  // Keep track of which bits of which vector nets are proved constant,
  // including Z and X.  Each bit can have 5 distinct values, 01XZV,
  // where V means the bit is variable, and must be determined from the
  // model.
  NameValueMap* mConstNets;

  NameSetMap mCollapsedClocks;
  NameIntMap mClockSpeeds;
  NameValueMap* mTieNets;
  AtomicCache* mAtomicCache;
  MsgContext* mMsgContext;
  HdlHierPath* mHdl;

  //! Map onDemand state nodes to their offsets within the onDemand state buffer
  NameIntMap mOnDemandStateOffsets;
  //! Total size of onDemand state
  UInt32 mOnDemandStateSize;
  //! Nodes that need to be excluded from OnDemand state at runtime
  NameSet mOnDemandRuntimeExcludedNets;

  //! Map of string attributes/values
  typedef UtHashMap<UtString, UtString> StringAttrMap;
  StringAttrMap mStringAttrMap;
  //! Map of int attributes/values
  typedef UtHashMap<UtString, UInt32> IntAttrMap;
  IntAttrMap mIntAttrMap;

  //! Array of clock names for glitch detection
  /*!
    We can't store an array of actual UtStrings like in the attribute
    maps above because UtArray can only hold PODs.  We can't easily
    use StringAtom pointers, because the entire AtomicCache isn't
    written to the DB - just the entries required for the symtab
    that's being written.

    Instead, use UtString pointers, allocate and deallocate manually,
    and read/write the raw string data to/from the DB.
   */
  typedef UtArray<UtString*> StringArray;
  StringArray mClockGlitchNames;

  STSymbolTable* mDesignTable;  // contains entire design namespace

  SCHScheduleFactory* mScheduleFactory;
  IODBTypeDictionary* mTypeDictionary;

  UserTypeFactory* mUserTypeFactory;

  UInt32 mDefaultSpeed;

  //! The version of the database that was read.
  UInt32 mInputDBVersion;

  //! The software version string (e.g., C2007.04 (5481))
  UtString mSoftwareVersion;

  //! Container class for basic info about design
  class DesignInfo
  {
  public:
    CARBONMEM_OVERRIDES
    
    //! Constructor
    DesignInfo();
    //! Destructor
    ~DesignInfo();

    //! Set the design identifier
    void putDesignId(const UtString& designId);

    //! Get the design identifier
    const char* getDesignId() const;

    //! Set the design interface name
    void putIfaceTag(const UtString& ifaceTag);

    //! Get the design interface name
    const char* getIfaceTag() const;

    //! Set whether or not this is a NIF-compiled design
    void putNoInputFlow(bool noInputFlow);
    
    //! Get NIF boolean
    bool isNoInputFlow() const;

    //! Set the number of bidirects
    void putNumBidis(UInt32 numBidis);
    
    //! Get the number of bidirects
    UInt32 numBidirects() const;
    
    //! Set the target compiler
    void putTargetCompiler(TargetCompiler targetCompiler);

    //! Get the target compiler
    TargetCompiler getTargetCompiler() const;

    //! Set the target compiler version
    void putTargetCompilerVersion(TargetCompilerVersion targetCompilerVersion);

    //! Get the target compiler version
    TargetCompilerVersion getTargetCompilerVersion() const;

    //! Set the target compiler flags
    void putTargetFlags(const UtString& flags);

    //! Get the target compiler flags
    const char* getTargetFlags() const;

    //! Set the db type (only at write time)
    void putDBType(CarbonDBType dbType);

    //! Get the db type
    CarbonDBType getDBType() const;

    //! Put the top-level module name
    void putTopLevelModuleName(const char* topModName);
    
    //! Get the top-level module name
    const char* getTopLevelModuleName() const;

    //! Put the systemC wrapper module name
    void putSystemCModuleName(const char* sysCName);
    
    //! Get the systemC wrapper module name
    const char* getSystemCModuleName() const;
    
    //! Set a Carbon Model type flag
    void setVHMTypeFlags(CarbonVHMTypeFlags vhmType);

    //! Clear a Carbon Model type flag
    void clearVHMTypeFlags(CarbonVHMTypeFlags vhmType);

    //! Get the Carbon Model type flags
    CarbonVHMTypeFlags getVHMTypeFlags() const;
    
    //! Write this to the database
    bool dbWrite(ZostreamDB& db) const;

    //! Read from the db
    bool dbRead(ZistreamDB& db);

    //! Get the version of the design info
    UInt32 getVersion() const;

    //! Put whether or not the port interface is codegened
    /*!
      Defaults to true in case an old iodb with a systemCWrapper
      generation is read with a new libcarbon, so the shell schedule
      flag will not break those compilations.
    */
    void putIsPortInterfaceGenerated(bool isGenerated);

    //! Returns true if the port interface code was codegened
    /*!
      Also returns true if this is an older database that didn't have
      this information.
    */
    bool isPortInterfaceGenerated() const;

    //! Put whether or not clock glitch detection is codegened
    void putIsClockGlitchGenerated(bool isGenerated);
  
    //! Returns true if clock glitch detection was codegened
    bool isClockGlitchGenerated() const;

  private:
    UInt32 mNumBidirects;
    UInt32 mVersion;
    TargetCompiler mTargetCompiler;
    TargetCompilerVersion mTargetCompilerVersion;
    CarbonDBType mDBType;
    UtString mDesignId;
    UtString mIfaceTag;
    UtString mTopLevelModuleName;
    UtString mSystemCModuleName;
    UtString mTargetFlags;
    CarbonVHMTypeFlags mVHMTypeFlags;
    bool mNoInputFlow;
    bool mPortIfaceGenerated;
    bool mClockGlitchGenerated;
  };

  //! CarbonModel Design information
  DesignInfo* mDesignInfo;

  //! Generic DynBitVector factory
  mutable DynBitVectorFactory* mBVPool;

  //! do we own the bitvector pool, or are we sharing it with another?
  bool mOwnBVPool;

  //! Capabilities capture the set of fields that are written to the database.
  /*! If you add a new field to be written, when you modify the reader
   *  to look for the new field, you should first test whether the
   *  database you are reading has that field in it.  See
   *  addCapability() and testCapability() for details
   *
   *  Added new capabilities during IODB construction.
   */
  UtStringSet* mCapabilities;
};

#endif
