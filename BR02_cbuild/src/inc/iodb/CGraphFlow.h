// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef CGRAPHFLOW_H
#define CGRAPHFLOW_H

#include "iodb/CGraph.h"
#include "flow/FLNodeElab.h"
#include "nucleus/NUNetRef.h"

class REAlias;

//! construct elaborated flow-graph with nesting removed
class CGraphFlow : public CGraph {
public:
  CARBONMEM_OVERRIDES

  //! ctor
  CGraphFlow(NUNetRefFactory*, REAlias*, SourceLocatorFactory* slc,
             AtomicCache* shared_cache);

  //! dtor
  ~CGraphFlow();

  //! create a graph from a design
  void design(NUDesign* design);

  //! typedef to represent our cache of dump-verilog output
  typedef UtHashMap<NUUseDefNode*,StringAtom*> DumpVerilogMap;

private:
  Node* processFlow(FLNodeElab* flow);
  Node* unbound(FLNodeElab* flow);
  Node* makeNode(Type type, FLNodeElab*);
  void composeDescription(FLNodeElab* flow, CGraph::Node* node);
  void queueFanin(Node* node,
                  StringAtom* subnode_symbol,
                  FLNodeElab* fanin);

  void collectExprFanin(FLNodeElab* flow, NUExpr* expr, Node* node);

  NUNetRefFactory* mNetRefFactory;
  REAlias* mAlias;

  // Hash function for FLNodeElab that looks at the usedef/hier pair
  // and ignores the net being def'd.  This means we will make one
  // cgraph node for every FLNodeElab, even if multiple nets are def'd.
  struct HashNode
  {
    CARBONMEM_OVERRIDES

    //! hash operator -- requires class method hash()
    size_t hash(FLNodeElab* flow) const {
      return ((((size_t) flow->getUseDefNode()) >> 2) ^
              ((size_t) flow->getHier()));
    }

    //! equal function
    bool equal(FLNodeElab* f1, FLNodeElab* f2) const {
      return ((f1->getUseDefNode() == f2->getUseDefNode()) &&
              (f1->getHier() == f2->getHier()));
    }

    //! lessThan operator -- for sorted iterations
    bool lessThan(FLNodeElab* f1, FLNodeElab* f2) const {
      return *f1 < *f2;
    }
  };

  typedef UtHashMap<FLNodeElab*,Node*,HashNode> FlowNodeMap;
  FlowNodeMap mFlowNodeMap;

  struct NodeConnection {
    NodeConnection(Node* node, FLNodeElab* fanin, StringAtom* sym) :
      mNode(node), mFanin(fanin), mSym(sym)
    {
    }
    Node* mNode;
    FLNodeElab* mFanin;
    StringAtom* mSym;
  };
  typedef UtList<NodeConnection> NodeConnectionList;
  NodeConnectionList mFaninQueue;

  DumpVerilogMap mDumpVerilogMap;
  StringAtom* mEmptyDesc;
};

#endif // CGRAPH_H
