// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _IODBRUNTIME_H_
#define _IODBRUNTIME_H_


#ifndef IODB_H
#include "iodb/IODB.h"
#endif

#ifndef __UtCustomerDB_h_
#include "util/UtCustomerDB.h"
#endif
#ifndef __STSYMBOLTABLE_H_
#include "symtab/STSymbolTable.h"
#endif

#ifndef __Expr_h_
#include "exprsynth/Expr.h"
#endif

#ifndef __CBUILDSHELLDB_H_
#include "iodb/CbuildShellDB.h"
#endif

#ifndef SOURCELOCATOR_H_
#include "util/SourceLocatorFactory.h"
#endif

#ifndef __UtHashMap_h_
#include "util/UtHashMap.h"
#endif

#ifndef _IODBRUNTIMEALIASBOM_H
#include "iodb/IODBRuntimeAliasBOM.h"
#endif

#ifndef _IODBDIRECTIVE_H_
#include "iodb/IODBDirective.h"
#endif

class DynBitVector;
class CarbonIdent;
class IODBGenTypeEntry;
class IODBIntrinsic;
class ShellBranchDataBOM;

class IODBRuntime : public IODB
{
public: CARBONMEM_OVERRIDES
  //! constructor
  IODBRuntime(AtomicCache*, STSymbolTable*, MsgContext*, SCHScheduleFactory*,
    ESFactory* exprFactory, SourceLocatorFactory *sourceLocatorFactory);

  //! virtual destructor
  virtual ~IODBRuntime();

  //! restore the iodb and the private symbol table from a database file
  bool readDB(const char* filename, CarbonDBType whichDB);

  //! restore the iodb and the private symbol table from a buffer
  bool readDBFromBuffer(const char* file, size_t sz, CarbonDBType whichDB);

  //! Get the type information for a signal 
  const IODBGenTypeEntry* getType(const STSymbolTableNode* node) const;

  //! Get the intrinsic type info for a signal
  const IODBIntrinsic* getLeafIntrinsic( const STAliasedLeafNode* node ) const;

  //! Get the type information for a signal 
  const IODBGenTypeEntry* getLeafType(const STAliasedLeafNode* node) const;

  //! Get the type information for a signal (returns NULL for modules)
  const SCHSignature* getSignature(const STSymbolTableNode* node) const;

  //! Get the user design type information for the given branch/leaf node if it exists.
  const UserType* getUserType(const STSymbolTableNode* node) const;

  //! Get the customer db which is embedded in the design db
  const UtCustomerDB* getCustomerDB() const;
  
  //! Modifiable form of the customer database. Used by lmtool/sp
  UtCustomerDB* getCustomerDB()
  {
    const IODBRuntime* me = const_cast<const IODBRuntime*>(this);
    return const_cast<UtCustomerDB*>(me->getCustomerDB());
  }

  //! Get the Expression Symbol Table
  const STSymbolTable* getExprSymTab() const;

  //! non-const 
  STSymbolTable* getExprSymTab() {
    const IODBRuntime* me = const_cast<const IODBRuntime*>(this);
    return const_cast<STSymbolTable*>(me->getExprSymTab());
  }
  
  //! Get the expression factory
  ESFactory* getExprFactory();

  //! Create a ShellSymNodeIdent
  CarbonIdent* createExprIdent(const STAliasedLeafNode* net, UInt32 bitSize, const DynBitVector* usageVec, CbuildShellDB::CarbonIdentType type, const CarbonExprVector* exprVec);

  //! Run through the design and look for bidis not in loopBidis()
  /*!
    As this runs through the design the buffer is updated with names
    of signals that have a bidi type but are not in the mBidis
    list. Each bidi will be on its own line and will have a storage
    description as well as the words "unlisted bidi" at the end of the
    line. If none are found, buffer is untouched.
  */
  void doBidiCheck(UtString* buffer);

  //! Returns the generated type index given a leaf node
  /*!
    \sa IODBTypeDictionary::findGenTypeIndex
  */
  SInt32 getLeafGenTypeIndex(const STAliasedLeafNode* node) const;

  //! Does this net have an invalid value when being dumped?
  bool isInvalidWaveNet(const STSymbolTableNode* node) const;

  //! Returns true if overrideMask's control bits are all 1's
  static bool sIsCompletelyOverridden(const DynBitVector* overrideMask, const IODBIntrinsic* intrinsic);
  //! Sets drive bits to 1 for z's and 0 for anything else.
  static void sSetOverride(const UInt32* overrideMask, UInt32* val, UInt32* drive, UInt32 numWords);
  //! sSetOverrideUndriven on index into overrideMask (1 word)
  static void sSetOverrideWord(const UInt32* overrideMask, UInt32* val, UInt32* drive, UInt32 numWords, int index);

  //! Given a value, xz, and control mask, create the override mask
  static void sComposeOverrideMask(DynBitVector* override, const UInt32* value, 
                                   const UInt32* xzMask, const UInt32* control, 
                                   UInt32 numWords);
  
  //! Given an override, extract the value, xz, and control masks
  static void sExtractOverride(const UInt32* overrideMask, const UInt32** value, 
                               const UInt32** xzMask, const UInt32** control, 
                               UInt32 numWords);
  
  //! Is this node completely constant?
  bool isFullyConstant(const STAliasedLeafNode*) const;

  //! Get the enable expr, if any,  for this node
  /*!
    Returns NULL if there is no enable expr. 
  */
  const CarbonExpr* getEnableExpr(const STSymbolTableNode* node) const;

  //! Is this node a memory?
  bool is2DArray(const STSymbolTableNode* node) const;
  bool isVector(const STSymbolTableNode* node) const;
  bool isScalar(const STSymbolTableNode* node) const;
  bool isTristate(const STSymbolTableNode* node) const;
  bool isConstant(const STSymbolTableNode* node) const;
  //! Is the alias ring marked as a clock?
  bool isMarkedClock(const STSymbolTableNode* node) const;
  SInt32 getWidth(const STSymbolTableNode* node) const;
  SInt32 getLsb(const STSymbolTableNode* node) const;
  SInt32 getMsb(const STSymbolTableNode* node) const;
  SInt32 get2DArrayLeftAddr(const STSymbolTableNode* node) const;
  SInt32 get2DArrayRightAddr(const STSymbolTableNode* node) const;

  // Unfortunately the BOM data attached to symbol table nodes depends on
  // whether the symbol is elaborated or unelaborated. The 
  enum ElaborationState {
    eElaborated,
    eUnelaborated
  };

  //! Is this net an input port of a module?
  bool isInput(const STSymbolTableNode* node, const ElaborationState = eElaborated) const;
  //! Is this net an output port of a module?
  bool isOutput(const STSymbolTableNode* node, const ElaborationState = eElaborated) const;
  //! Is this net a bidirect port of a module?
  bool isBidirect(const STSymbolTableNode* node, const ElaborationState = eElaborated) const;

  //! Is this net or expression an async?
  bool isRuntimeAsync(const STSymbolTableNode* node) const;
  
  //! Is this net or expression an async output?
  bool isRuntimeAsyncOutput(const STSymbolTableNode* node) const;
  
  //! Is this net or expression an async deposit?
  bool isRuntimeAsyncDeposit(const STSymbolTableNode* node) const;
  
  //! Is this net a temp (port split temp for example)
  bool isTemp(const STSymbolTableNode* node) const;

  //! Get the pull mode for the node
  CarbonPullMode getPullMode(const STSymbolTableNode* node) const;

  //! Returns true if this node is sample-scheduled
  bool isSampleScheduled(const STSymbolTableNode* node) const;

  //! Returns the source language for this branch node
  HierFlags getSourceLanguage(const STBranchNode* branch) const;

  //! Returns the source language for this leaf node
  HierFlags getSourceLanguage(const STAliasedLeafNode* node) const;

  //! Returns the declaration type for this net
  /*! This should match the HDL declaration type either for Verilog or
   *  VHDL.
   *
   *  If this node is not for a net it returns NULL
   */
  const char* declarationType(const STSymbolTableNode* node) const;

  //! Returns the module/architecture name for branch nodes or NULL for leafs
  const char* componentName(const STSymbolTableNode* node) const;

  //! Returns the source HDL for a given symbol table node
  const char* sourceLanguage(const STSymbolTableNode* node) const;

  //! \return the source location associated with a symbol
  bool findLoc (const STSymbolTableNode *, SourceLocator *) const;

  //! \return the unelaborated symbol corresponding to an elaborated symbol
  bool getUnelaborated (const STSymbolTableNode *, STSymbolTableNode **) const;

  //! \return the unelaborated symbol table
  const STSymbolTable *getUnelaboratedSymtab () const { return &mUnelaboratedSymtab; }

  //! \return list of design directives
  IODBDesignDirective::List &getDesignDirectives () { return mDesignDirectives; }

  //! \return list of module directives
  IODBModuleDirective::List &getModuleDirectives () { return mModuleDirectives; }

  //! Look up a string attribute
  bool getStringAttribute(const char* name, UtString* value);

  //! Look up an integer attribute
  bool getIntAttribute(const char* name, UInt32* value);

  //! Get a clock name for glitch detection based on its index
  UtString* getClockGlitchName(UInt32 index);

private:
  //! Gets the branch data associated with a branch node
  const ShellBranchDataBOM* getBranchData(const STBranchNode* branch) const;

  //! shared helper function for parsing the database
  bool readDBHelper(const char* filename,
                    ZistreamZip& symDBZip,
                    CarbonDBType whichDB);

  //! Function to read the deposit info from a db
  /*! This function can read deposits from old db's and convert itthem
   *  to the new format
   */
  bool readDeposits(ZistreamDB& symDB);

  //! Function to read an old deposit set and convert it to the new format
  bool readOldDepositSet(ZistreamDB& symDB, IODB::DepositMask mask);

  //! Function to read the observe info from a db
  /*! This function can read observes from old db's and convert them
   *  to the new format
   */
  bool readObserves(ZistreamDB& symDB);

  //! Function to read an old observe set and convert it to the new format
  bool readOldObserveSet(ZistreamDB& symDB, IODB::ObserveMask mask);

  ESFactory* mExprFactory;
  UtCustomerDB mCustomerRuntimeDB;
  STEmptyFieldBOM mEmptyBOM;
  IODBRuntimeAliasBOM mUnelaboratedBOM;
  STSymbolTable mExprSymTab;
  SourceLocatorFactory *mSourceLocatorFactory;
  STSymbolTable mUnelaboratedSymtab;

  typedef UtHashMap <const STSymbolTableNode *, SourceLocator> LocationMap;
  LocationMap mLocations;

  typedef UtHashMap <const STSymbolTableNode *, STSymbolTableNode *> ElaborationMap;
  ElaborationMap mElaborationMap;

  IODBDesignDirective::List mDesignDirectives;
  IODBModuleDirective::List mModuleDirectives;

  class ExprFlagChecker;
};

#endif

