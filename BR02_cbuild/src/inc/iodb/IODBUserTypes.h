
// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!  \file
  This file is a header for classes that represent user defined types in the
  input design. They fall into 3 categories, scalars, arrays and records.
*/
#ifndef _IODB_USER_TYPES_H_
#define _IODB_USER_TYPES_H_

#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtArray.h"
#include "util/ConstantRange.h"
#include "shell/carbon_internal_enums.h"
#include "iodb/IODB.h"


class ZostreamDB;
class ZistreamDB;
class ConstantRange;
class UserType;
class UserScalar;
class UserArray;
class UserStruct;
class UserEnum;
class StringAtom;
class UserTypeFactory;
class UtXmlWriter;

typedef UtArray<const UserType*> UserTypeVector;
typedef UtArray<UserTypeVector*> VectorUserTypeVector;
typedef UserTypeVector::iterator UserTypeVectorIter;
typedef UserTypeVector::const_iterator UserTypeVectorCIter;
typedef UserTypeVector::reverse_iterator UserTypeVectorRIter;
typedef UserTypeVector::const_reverse_iterator UserTypeVectorCRIter;

typedef UtArray<StringAtom*> AtomVector;
typedef AtomVector::iterator AtomVectorIter;
typedef AtomVector::const_iterator AtomVectorCIter;

typedef HashPointerValue<const UserType*> UTCompare;
typedef UtHashSet<const UserType*, UTCompare> TypeCSet;
typedef TypeCSet::iterator TypeCSetIter;
typedef TypeCSet::const_iterator TypeCSetCIter;

//! Abstract base class for a user defined type.
class UserType
{
public: CARBONMEM_OVERRIDES

  enum Type {
    eScalar,     //!< VHDL Example: std_logic, bit etc
    eArray,      //!< Single/multi-dim arrays VHDL Example: std_logic_vector
    eEnum,       //!< Enumatation type
    eStruct,     //!< VHDL Example: record
    eStructEnd   //!< This type is only used to mark the population of a record
  };

  virtual ~UserType();

  //! Get the language of the node
  CarbonLanguageType getLanguageType() const {return mLanguageType;}

  //! Get the Verilog node type
  CarbonVerilogType getVerilogType() const {return mVerilogType;}

  //! Get the Vhdl node type
  CarbonVhdlType getVhdlType() const {return mVhdlType;}

  //! Find out if the user type is a scalar/array/structure.
  virtual Type getType() const = 0;

  //! Find out if the user type is a scalar/array/structure.
  virtual const char* getClassName() const = 0;

 //! If initial declaration of this array requires it, specify a range or not
  bool isRangeRequiredInDeclaration() const { return mIsRangeRequiredInDeclaration; }

  //! User defined or basic data type name string.
  virtual StringAtom* getTypeName() const;

  //! Get the instrinsic type name
  virtual StringAtom* getIntrinsicTypeName() const;

  //! Library name where the User defined type is compiled
  virtual StringAtom* getLibraryName() const;

  //! Package name where the User defined type is compiled
  virtual StringAtom* getPackageName() const;

  //! Print the type information
  virtual void print(UtOStream* out = NULL) const = 0;

  //! xml Write the type information
  virtual void writeXml(UtXmlWriter* xmlWriter) const;



  //! Write to the IODB file.
  bool dbWrite(ZostreamDB& f) const;
  //! Static function to read from database and create appropriate
  //! derivation of UserType. Return NULL if it fails to read from file.
  static UserType* dbRead(ZistreamDB& f);

  //! Compare function for sets and maps.
  int compare(const UserType* ut) const;

  //! Hash function for hash map.
  virtual size_t hash() const;

  //! Compare function.
  bool operator==(const UserType& other) const;

  //! Finds out the size of the user type
  virtual UInt32 getSize() const = 0;

  //! Cast to scalar
  virtual const UserScalar* castScalar() const;

  //! Cast to array
  virtual const UserArray* castArray() const;

  //! Cast to struct
  virtual const UserStruct* castStruct() const;

  //! Cast to enum
  virtual const UserEnum* castEnum() const;

protected:
  //! Constructor
  UserType(CarbonLanguageType lang, 
	   CarbonVerilogType sigType, 
	   CarbonVhdlType vhdlType, 
	   UtString* compositeTypeName,
	   bool isRangeRequiredInDeclaration,
           UtString* mLibraryName,
           UtString* mPackageName);

  //! Type of Verilog net
  CarbonVerilogType    mVerilogType;
  //! Type of Vhdl signal/variable
  CarbonVhdlType   mVhdlType;
  //! Language of the net/signal/variable
  CarbonLanguageType  mLanguageType;

  //! Derived specialization of db read/write.
  virtual bool writeToDB(ZostreamDB& f) const = 0;

  void setSortIndex(UInt32 index) { mSortIndex = index; }
  UInt32 getSortIndex() const { return mSortIndex; }

  //! Convert the specified StringAtom* to the one that can be saved to file.
  StringAtom* translateForSave(StringAtom* atom) const;

  //! Save element strings into Atomic Cache of symbol table,
  //! that get's written in the symtab.db file
  virtual void translateStringsForSave() {};

  //! Sete atomic cache for user types
  static void sSetAtomicCacheForSave(AtomicCache* cache) {
    sAtomicCacheForSave = cache;
  }

  static void sSetIODB(IODB* iodb) {
    sIODB = iodb;
  }

  static IODB* sGetIODB() {
    return sIODB;
  }
  

  //! Compare function for sets and maps.
  virtual int compareHelper(const UserType* ut) const = 0;

  //! Util functions for ConstantRange
  bool writeRange(const ConstantRange* range, ZostreamDB& f) const;
  static bool readRange(ConstantRange* range, ZistreamDB& f);
  void printRange(const ConstantRange* range, UtOStream& out) const;

  static const char*  sGetCarbonLanguageTypeString(CarbonLanguageType langType);
  static const char*  sGetCarbonVerilogTypeString(CarbonVerilogType signType);
  static const char*  sGetCarbonVhdlTypeString(CarbonVhdlType vhdlType);
  

  // Types should be sorted in the order in which they were created. This way
  // the elements of the type are written out prior to the types themselves.
  UInt32 mSortIndex;

  //! Indicates the atomic cache to be used by UserTypes to save themselves to file
  //! when dbWrite is called.
  static AtomicCache* sAtomicCacheForSave;

  static IODB*  sIODB;  

  friend class UserTypeFactory;

  //! Stores the actual type name
  StringAtom* mTypeName;

  //! Does the subtype declaration include a constraint or not
  bool mIsRangeRequiredInDeclaration;

  //! Stores the library and the package names for vhdl types
  StringAtom* mLibraryName;
  StringAtom* mPackageName;
};

//! Represents a pre-defined or user defined scalar type.
class UserScalar : public UserType
{
public: CARBONMEM_OVERRIDES

  virtual ~UserScalar();

  //! Return type as scalar.
  Type getType() const {
    return eScalar;
  }

  //! Return type as scalar.
  const char* getClassName() const {
    return "UserScalar";
  }

  const ConstantRange* getRangeConstraint() const {
    return mRangeConstraint;
  }

  //! Print the type information
  void print(UtOStream* out = NULL) const;

  //! xml Write the type information
  virtual void writeXml(UtXmlWriter* xmlWriter) const;

  static UserType* readFromDB(CarbonLanguageType lang, CarbonVerilogType verType, 
			      CarbonVhdlType vhdlType, UtString& typeName, 
                              UtString& libName, UtString& packName,
			      ZistreamDB& f);

  //! Hash function for hash map.
  virtual size_t hash() const;

  //! Finds out the size of the user type
  virtual UInt32 getSize() const;

  //! Cast to scalar
  virtual const UserScalar* castScalar() const;

protected:

  //! Constructor
  UserScalar(CarbonLanguageType lang, 
	     CarbonVerilogType sigType, 
	     CarbonVhdlType vhdlType, 
	     ConstantRange* rangeConstraint, 
	     UtString* compositeTypeName,
	     bool isRangeRequiredInDeclaration,
             UtString* libName,
             UtString* packName);

  //! Derived specialization of db read/write.
  virtual bool writeToDB(ZostreamDB& f) const;

  //! Compare function for sets and maps.
  virtual int compareHelper(const UserType* ut) const;

  //! Returns the size of a scalar in the absence of constraints
  UInt32 getUnconstrainedSize() const;

  ConstantRange* mRangeConstraint;

  friend class UserTypeFactory;
};

//! Represents a pre-defined or user defined single/multi dimensional array.
class UserArray : public UserType
{
public: CARBONMEM_OVERRIDES

  virtual ~UserArray();

  //! Get the instrinsic type name
  virtual StringAtom* getIntrinsicTypeName() const;

  //! Return type as array.
  virtual Type getType() const {
    return eArray;
  }

  //! Return type as array
  const char* getClassName() const {
    return "UserArray";
  }


  /*!
    Example:
      type rec_array is array(3 downto 0) of rec;
      type rec_2d_array is array(1 downto 0) of rec_array;
      type rec_3d_array is array(7 downto 0) of rec_2d_array;

    Index   Type Name     Range
      0     rec_2d_array  [7:0]
      1     rec_array     [1:0]
      2     rec           [3:0]

    For multi-dimensional arrays, the element type name for each
    dimension will be same as the final dimension element type name.

    Example:

      type rec_3d_array is array(7 downto 0, 1 downto 0, 3 downto 0) of rec;

    The element type name will be "rec" for all dimensions.
  */

  //! Return element type of this array.
  /*!
    The element type of array rec_3d_array in example above is 
    UserType* rec_2d_array which represents 

    array(1 downto 0) of rec_array;

    array type.
   */
  const UserType* getElementType() const {
    return mElementType;
  }

  //! The methods below allow looking at various dimensions of the array.
  //! They iterate through elements of elements and so on upto the non-array
  //! element dimension to get to the requested dimension.

  //! Return number of array dimensions.
  /*!
    Returns 3 for above example.
  */
  UInt32 getNumDims() const;
  
  //! Returns the type handle for the given dimension
  /*!
    For above example:

    dimIdx   UserType* type
      0,1      eArray
      2        eStruct
  */
  const UserType* getDimElementType(UInt32 dimIdx) const;

  //! Returns true if the array is a packed array. Note
  //  that there may be any number of packed dimensions,
  //  so this doesn't necessarily mean a 1-d packed array.
  bool isPackedArray(void) const {
    return mIsPackedArray;
  }
  
  //! Get the range for array.
  const ConstantRange* getRange() const {
    return mRange;
  }

  //! Print the type information
  void print(UtOStream* out = NULL) const;

  //! xml Write the type information
  virtual void writeXml(UtXmlWriter* xmlWriter) const;


  static UserType* readFromDB(CarbonLanguageType lang, CarbonVerilogType verType, 
			      CarbonVhdlType vhdlType, UtString& typeName,
                              UtString& libName, UtString& packName,
			      ZistreamDB& f);

  //! Hash function for hash map.
  virtual size_t hash() const;

  //! Finds out the size of the user type
  virtual UInt32 getSize() const;

  //! Cast to array
  virtual const UserArray* castArray() const;

  //! Does this represent a vector, i.e. a 1-D array of scalars?
  bool isVector() const;

  //! Does this represent a memory, i.e. a 2-D array of scalars?
  bool isMemory() const;

  //! If a vector or memory, returns the range representing its width
  const ConstantRange* getVectorRange() const;

  //! If a memory, returns the range representing its depth
  const ConstantRange* getMemoryRange() const;

protected:

  //! Constructor
  UserArray(CarbonLanguageType lang, 
	    CarbonVerilogType sigType, 
	    CarbonVhdlType vhdlType, 
	    ConstantRange *range, 
	    const UserType* elemType, 
            bool isPackedArray,
	    UtString* compositeTypeName,
	    bool isRangeRequiredInDeclaration,
            UtString* libName,
            UtString* packName);

  //! Derived specialization of db read/write.
  virtual bool writeToDB(ZostreamDB& f) const;

  //! Compare function for sets and maps.
  virtual int compareHelper(const UserType* ut) const;

private:
  ConstantRange *mRange;
  const UserType* mElementType;
  bool mIsPackedArray;
  
  friend class UserTypeFactory;
};

//! Represents user defined structure type.
class UserStruct : public UserType
{
public: CARBONMEM_OVERRIDES
  
  virtual ~UserStruct();

  //! Return type as structure.
  virtual Type getType() const {
    return eStruct;
  }

  //! Return type as struct
  const char* getClassName() const {
    return "UserStruct";
  }


  /*!
    Example:

    type rec is record
      f1 : integer 0 to 3;
      f2 : bit_vector(3 downto 0);
    end record

    The fields are ordered in the order of user declaration.

    FieldIndex    FieldName

      0             f1
      1             f2
  */

  //! Returns the number of fields in the structure.
  UInt32 getNumFields() const {
    return mFieldTypes.size();
  }

  //! Return the name of field at given index.
  StringAtom* getFieldName(UInt32 fieldIdx) const {
    return mFieldNames[fieldIdx];
  }

  //! Returns the type handle for structure field at given field index.
  const UserType* getFieldType(UInt32 fieldIdx) const {
    return mFieldTypes[fieldIdx];
  }

  //! Print the type information
  void print(UtOStream* out = NULL) const;

  //! xml Write the type information
  virtual void writeXml(UtXmlWriter* xmlWriter) const;


  static UserType* readFromDB(CarbonLanguageType lang, CarbonVerilogType verType, 
			      CarbonVhdlType vhdlType, UtString& typeName,
                              UtString& libName, UtString& packName,
			      ZistreamDB& f);

  //! Hash function for hash map.
  virtual size_t hash() const;

  //! Finds out the size of the user type
  virtual UInt32 getSize() const;

  //! Save element strings into Atomic Cache of symbol table,
  //! that gets written in the symtab.db file
  virtual void translateStringsForSave();

  //! Cast to struct
  virtual const UserStruct* castStruct() const;

protected:

  //! Constructor
  UserStruct(CarbonLanguageType lang, 
	     CarbonVerilogType sigType, 
	     CarbonVhdlType vhdlType, 
             UserTypeVector& fields, 
	     AtomVector& fieldNames, 
	     UtString* compositeTypeName,
             UtString* libName,
             UtString* packName);

  //! Derived specialization of db read/write.
  virtual bool writeToDB(ZostreamDB& f) const;

  //! Compare function for sets and maps.
  virtual int compareHelper(const UserType* ut) const;

private:
  UserTypeVector mFieldTypes;
  AtomVector     mFieldNames;

  friend class UserTypeFactory;
};


//! Represents a user defined enumeration
class UserEnum : public UserType
{
public: CARBONMEM_OVERRIDES

  virtual ~UserEnum();

  //! Return type as array.
  virtual Type getType() const {
    return eEnum;
  }

  //! Return type as struct
  const char* getClassName() const {
    return "UserEnum";
  }


  //! Get the range for enumaration
  const ConstantRange* getRange() const {
    return mRange;
  }

  //! Print the type information
  void print(UtOStream* out = NULL) const;

  //! xml Write the type information
  virtual void writeXml(UtXmlWriter* xmlWriter) const;


  static UserType* readFromDB(CarbonLanguageType lang, CarbonVerilogType verType, 
			      CarbonVhdlType vhdlType, UtString& typeName,
                              UtString& libName, UtString& packName,
			      ZistreamDB& f);

  //! Hash function for hash map.
  virtual size_t hash() const;

  //! Finds out the size of the user type
  virtual UInt32 getSize() const;

  //! Cast to enum
  virtual const UserEnum* castEnum() const;

  //! Get number of elements
  UInt32 getNumberElems() const {
    return mElementNames.size();
  }

  //! Get number of encoding elements
  UInt32 getNumberEncodingElems() const {
    return mElementEncodings.size();
  }

  //! Get i'th element
  StringAtom* getElem(UInt32 index) const {
    return mElementNames[index];
  }

  //! Get i'th element of the ENUM_ENCODING
  StringAtom* getEncodingElem(UInt32 index) const {
    return mElementEncodings[index];
  }

  //! Determine if an ENUM_ENCODING attribute has been associated with this enum
  bool hasEnumEncodingAttribute() { return (mElementEncodings.size() == 0) ? false : true; }

  //! Save element strings into Atomic Cache of symbol table,
  //! that get's written in the symtab.db file
  virtual void translateStringsForSave();

protected:

  //! Constructor
  UserEnum(CarbonLanguageType lang, 
	   CarbonVerilogType sigType, 
	   CarbonVhdlType vhdlType, 
	   ConstantRange *range, 
	   AtomVector& enumElems, 
	   AtomVector& enumEncodingElems,
	   UtString* compositeTypeName,
	   bool isRangeRequiredInDeclaration,
           UtString* libName,
           UtString* packName);

  //! Derived specialization of db read/write.
  virtual bool writeToDB(ZostreamDB& f) const;

  //! Compare function for sets and maps.
  virtual int compareHelper(const UserType* ut) const;

private:
  ConstantRange *mRange;
  AtomVector    mElementNames;
  AtomVector    mElementEncodings;

  friend class UserTypeFactory;
};


//! Helps build type information unique for each type name.
class UserTypeFactory
{
public: CARBONMEM_OVERRIDES

  UserTypeFactory(AtomicCache* atomicCache, IODB* iodb);
  ~UserTypeFactory();

  /*! All the add methods below will return the existing type handle if
      it exists, otherwise a new type is created, added to the factory and
      a handle to that is returned.
      If the StringAtom for typeName is specified, the type with that typeName
      is created. If not, the user type shall remain unnamed. Example:

      type rec_arr is array(3 downto 0) of rec;
      signal sig1 : rec_arr;

      Here rec_arr is the typeName.
  */

  //! Return a scalar type handle.
  const UserType* maybeAddScalarType(CarbonLanguageType lang, 
				     CarbonVerilogType sigType,
                                     CarbonVhdlType vhdlType, 
				     ConstantRange* rangeConstraint = NULL,
				     UtString* compositeTypeName = NULL,
				     bool isRangeRequiredInDeclaration = false,
                                     UtString* libName = NULL,
                                     UtString* packName = NULL);

  //! Return an array type handle.
  const UserType* maybeAddArrayType(CarbonLanguageType lang, 
				    CarbonVerilogType sigType, 
				    CarbonVhdlType vhdlType, 
				    ConstantRange *range, 
				    const UserType* elemType,
                                    bool isPackedArray,
				    UtString* compositeTypeName = NULL,
				    bool isRangeRequiredInDeclaration = false,
                                    UtString* libName = NULL,
                                    UtString* packName = NULL);

  //! Return a struct type handle.
  const UserType* maybeAddStructType(CarbonLanguageType lang, 
				     CarbonVerilogType sigType, 
				     CarbonVhdlType vhdlType, 
				     UserTypeVector& fields,  
				     AtomVector& fieldNames,
				     UtString* compositeTypeName,
                                     UtString* libName = NULL,
                                     UtString* packName = NULL);

  //! Return a enum type handle.
  const UserType* maybeAddEnumType(CarbonLanguageType lang, 
				   CarbonVerilogType sigType, 
				   CarbonVhdlType vhdlType, 
				   ConstantRange *range, 
				   AtomVector& enumElems,
				   AtomVector& enumEncodingElems,
				   UtString* compositeTypeName,
				   bool isRangeRequiredInDeclaration = false,
                                   UtString* libName = NULL,
                                   UtString* packName = NULL);

  //! Specify the atomic cache used for IODB write.
  /*!
    This atomic cache is usually different from the one used by factory to create 
    UserType names. This call interns all the UserType names into this cache, and
    saves a pointer to it for use during save operation.
  */
  void setUserTypesAtomicCache(AtomicCache* atomicCache);

  //! Save all the strings in the user types into current AtomicCache
  void copyStringsIntoSymTable();

  //! Save to a ZostreamDB for IODB.
  bool save(ZostreamDB& out) const;
  //! Restore from a ZistreamDB for IODB.
  bool restore(ZistreamDB&);

  AtomicCache* getAtomicCache() const {
    return mAtomicCache;
  }

  bool isEmpty() const {
    return mTypeSet.empty();
  }

  //! Print each unique type in the factory once.
  void print(UtOStream* out = NULL) const;

private:

  const UserType* maybeAddType(UserType* ut);
  void getSortedTypes(UserTypeVector& sortedTypes) const;

  typedef UtHashSet<UserType*,UTCompare> TypeSet;
  typedef TypeSet::iterator TypeSetIter;
  typedef TypeSet::const_iterator TypeSetCIter;

  TypeSet mTypeSet;
  AtomicCache* mAtomicCache;
  UInt32 mNumUniqTypes;
  IODB*  mIODB;  
};

#endif
