// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
// \file Implement the IODBNucleus class
#ifndef _IODBNUCLEUS_H_
#define _IODBNUCLEUS_H_


#include "iodb/IODBDirective.h"
#include "iodb/IODB.h"
#include "util/UtCustomerDB.h"
#include "util/CarbonTypes.h"
#include "util/UtStringArray.h"
#include "nucleus/Nucleus.h"
#include "schedule/CommonWrapper.h"

class NUModule;
class NUDesign;
class NUNet;
class NUBase;
class NUNetElab;
class ArgProc;
class CbuildSymTabBOM;
class UtOBStream;
class ReachableAliases;
class UtWildcard;
class IODBDesignDataSymTabs;
class CopyContext;
class UserType;

typedef UtVector<UtString> NUPortList;
typedef UtMap<UtString,UtString> NUPortMap;

//! IODBNucleus class
class IODBNucleus : public IODB
{
public:
    typedef enum {Lang_MIN=0,VERILOG=0,VHDL,Lang_MAX} Lang;
private:
  // Compare two strings
  struct CmpStrs
  {
    bool operator()(const UtString* s1, const UtString* s2) const
    {
      return s1->compare(*s2) < 0;
    }
  };
  
public: CARBONMEM_OVERRIDES
  //! constructor
  IODBNucleus(AtomicCache*, 
              STSymbolTable*, 
              MsgContext*, 
              ArgProc*,
              SourceLocatorFactory*,
              SCHScheduleFactory*);
  
  //! destructor
  virtual ~IODBNucleus();

  //! parse a directives file 
  /*! 
   *  Evaluates message suppressions but delaying
   *  the evaluation of design directives until evaluateDesignDirectives()
   *  is called.
   *  The issue here is that we want to parse directives very early in 
   *  in the compiler to catch syntax errors in the file, and to have
   *  message suppressions affect the design-parsing process.
   */
  bool parseFile(const char* filename, const char* unexpandedFilename);
  
  //! Parse option for making all CModels callable in replay
  /*!
    This must be set prior to parsing directives to have any effect.
  */
  void putPlaybackAllCModels(bool playbackAll) { 
    mPlaybackAllCModels = playbackAll;
  }

  //! Set the elaborated and unelaborated symbol tables used to hold
  //! design data like directives, user type information etc.
  /*!
    The specified object will be owned and deleted by this class.
  */
  void setDesignDataSymbolTables(IODBDesignDataSymTabs* symtabs);
  IODBDesignDataSymTabs* getDesignDataSymbolTables();
  
  //! Populate elaborated and unelaborated symbol tables with design directives.
  bool populateDesignDirectives();

  //! evaluate the design directives in the directives files that have been
  //! read by parseFile
  bool evaluateDesignDirectives(NUDesign* design);

  //! resolve the directives onto the post-elaborated design table
  bool resolveDirectives();
  bool checkDirectives(STSymbolTable *);

  //! Populate design outputs  in the NUDesign class
  /*! A number of the directives require populating observability
   *  points in the NUDesign class. This needs to be done after every
   *  elaboration pass so that iteration from design outputs works
   *  correctly.
   *
   *  \param design The design in which to populate the outputs
   *
   *  \param symTabBOM The BOM for the symbol table to do lookups
   *
   *  \param symtab Optional argument that indicates the symbol table
   *  to use. If it is non-NULL, all IODB symbol table nodes are
   *  translated to the given symbol table before the NUDesign is
   *  populated.
   */
  void populateDesignOutputs(NUDesign* design, CbuildSymTabBOM* symTabBOM,
                             STSymbolTable* symtab = NULL);

  //! Copy the user type information to design symbol table.
  /*!
    After population, the user type information is populated in
    IODBDesignData's elaborated symbol table. On elaboration of 
    design later, this needs to be copied to the design symbol table.
  */
  void copyUserTypeInfo();

  //! resolve the force directives, which require hierarchical aliasing
  void resolveForce();

  //! resolve up any constant values that were declared on multiple
  //! aliases of the same net.  Whenever we find the same bit declared
  //! in conflicting ways, resolve the conflict so that anything overrides
  //! a Z, and all other conflicts result in X.  Put that resolved value
  //! on the storage alias only.
  void resolveConstants();

  //! Resolve depositable directives
  /*!
   * This should be called immediately after hierarchical aliasing.
   * The nets will be marked depositable, and also the given ReachableAliases
   * class will be called to remember the depositable elaborated nets.
   */
  void resolveDepositable(CbuildSymTabBOM* symTabBOM,
                          ReachableAliases *reachables,
                          STSymbolTable* symtab = NULL,
                          bool markDepositAliases = true);

  //! Get the aliases marked depositable for a netElab
  void getDepositableAliases(const NUNetElab* netElab,
                             STAliasedLeafNodeVector* nodes);

  //! Find all nets which are marked as inaccurate.
  void resolveInaccurate();

  //! Mark all aliases of OnDemand excluded nets
  void resolveOnDemandExcluded();

  //! Find all the elaborated nets that have been marked with collapse clock
  /*! This is useful because these nets can be thought of as driven
   *  even if they have no driver or a deposit/force marker.
   */
  void findCollapseNets(STSymbolTable* symtab);

  //! is this netlab a subordinate of a collapseClock directive?
  bool isCollapseSubordinate(NUNetElab*) const;

  //! invalidate the collapseNets cache created via findCollapseNets
  void clearCollapseNets();

  //! Get the filename for a full database or io database
  void getDBFileName(UtString* fname, CarbonDBType type, const char* fileroot);

  //! write the database files (symtab and io)
  bool writeFiles(bool doSymTabDB,
                  bool includeTmps, 
                  const char* fileRoot,
                  CbuildSymTabBOM* bomManager,
                  ESFactory* exprFactory,
                  bool verboseErrors,
                  bool strictIODB);

  //! write the database file for the GUI
  bool writeGuiDB (const char *fileRoot, CbuildSymTabBOM *bomManager, ESFactory *, bool verboseErrors);

  //! Map from a node stored in a directive into a node*, or return NULL if failure
  STSymbolTableNode* getNode(const STSymbolTableNode* node, STSymbolTable* symtab);

  //! map from a node stored in a directive into a NUNetElab*, or return NULL if failure
  NUNetElab* getNet(const STSymbolTableNode* node,
                    STSymbolTable* symtab = NULL);


  //! Declare an internal signal for inclusion in the IO database
  void declareIOSignal(const STSymbolTableNode* node,
                       NetFlags portDirection);

  //! Declare a clock
  void declareClock(const STSymbolTableNode* node);
  
  //! Declare a clock tree net
  void declareClockTree(const STSymbolTableNode* node);
  
  //! Declare a primary clock
  void declarePrimaryClock(const STSymbolTableNode* node);

  //! Declare an asynchronous input signal
  void declareAsync(const STSymbolTableNode* node);

  //! Declare an asynchronous output signal
  void declareAsyncOutput(const STSymbolTableNode* node, const STSymbolTableNode* faninNode);

  //! Declare an asynchronous deposit signal
  void declareAsyncDeposit(const STSymbolTableNode* node);

  //! Declare a reset signal
  void declareReset(const STSymbolTableNode* node, ClockEdge edge);
  
  //! Declare a net that sensitizes CModels.
  void declareCModelSense(const STSymbolTableNode* node);

  //! Mark a net name as having an invalid waveform.
  /*! Nets have invalid waveforms when the compiler decides that their
   *  values are not always useful, and contrives to compute them only
   *  when they are needed.  Compiling with -g should turn off this
   *  optimization.
   */
  void declareInvalidWaveNet(STSymbolTableNode* node);

  //! Returns true if the node is an invalid wave net
  bool isInvalidWaveNet(const STSymbolTableNode* node);
  
  //! Mark the specified bits of a net as having constant value
  void declareConstBits(const STAliasedLeafNode* node,
                        const DynBitVector& mask,
                        Carbon4StateVal);

  //! Function to remember const bits before the elaborated symtab is created
  /*! This can be called instead of declareConstBits before the
   *  elaborated symbol table is populated. Then after elaboration,
   *  call translateConstBits to apply the constants.
   */
  void rememberConstBits(const STAliasedLeafNode* node, 
                         const DynBitVector& mask,
                         Carbon4StateVal val);

  //! Determine if this net is a constant, and if so, return it in val.
  /*!
   *! This routine will only return true if the constant has 0s and 1s.
   *! Any X/Z bits and false is returned.
   */
  bool isConstant01(const STAliasedLeafNode* node, DynBitVector* val);

  //! Function to apply the remembered const bits
  /*! This routine walks the remembered const bits (set with
   *  rememberConstBits) and calls declareConstBits on each entry.
   */
  void translateConstBits();

  //! Mark the specified bits of a constant as having x or z
  void assignOverrideMask(NUConst* expr, const STAliasedLeafNode* node);

  //! create an override mask {control,value,drive} and value, that can be used in write{Bin,Oct,Dec,Hex}XZValToStr()
  static void sGetTempOverrideMask(const NUConst* constVar, DynBitVector* overrideMask, DynBitVector* value);

  //! Declare an enable bound to a node
  /*!
    Mainly used for port bidis, this will hold a CarbonExpr for an
    enabled net.
    \param enabledNet The net leaf node that is enabled by the
    expression.
    \param expr The enable expression
  */
  void declareEnableExpr(const STAliasedLeafNode* enabledNet,
                         CarbonExpr* expr);

  //! Is any instance of an NUNet* depositable?
  /*! This routine is used to determine if the depositSignal directive
   *  was used on this net or any of its aliases. It is used by code
   *  generation so that it can treat the net as a primary input.
   */
  bool isDepositableNet(const NUNet* net) const;

  //! Is any instance of an NUNet* forcible?
  /*! This routine is used to determine if the forceSignal directive
   *  was used on this net or any of its aliases. It is used by code
   *  generation so that it can treat the net as a primary input.
   */
  bool isForcibleNet(const NUNet* net) const;

  //! Has this net been requested act as an async reset via directive?
  /*! This routine is used to determine if the asyncReset directive
   *  was used on this net or any of its aliases.  It is used by Reset
   *  analysis to add edge sensitivies to always-blocks prior to splitting
   *  them out into separate blocks
   */
  bool isAsyncResetNet(const NUNet* net) const;

  //! Is this symbol tied to a constant (instance based search)
  const DynBitVector* isTieNet(const STSymbolTableNode* node) const;
  
  //! Are there any blastNet directives?
  bool hasBlastedNets() const;

  //! Is this module and net bit blasted?
  /*! This routine is used to determine if the blastNet directive
   *  was used on this net. It is used by vector separation
   *  prior to elaboration, so that the net can be bit-blasted.
   */
  bool isBlastedNet(const NUNet* net) const;

  //! Annotate a node, usually a clock, with a speed setting
  void putClockSpeed(const STSymbolTableNode* node, UInt32 speed);

  //! Indicate that this clock is routed slower than async resets
  void putIsSlowClock(const STSymbolTableNode* node);

  //! Indicate that this reset is routed faster than clocks
  void putIsFastReset(const STSymbolTableNode* node);

  //! Hide visibility into every instance of a module
  void hideModule(NUModule* mod);

  //! Request that this module be optimized with a ternary gate optimization technique.
  void ternaryGateOptimization(NUModule* mod);

  //! Transform async resets into synchronous resets for this module
  void noAsyncResets(NUModule* mod);

  //! Disable OutputSysTasks in this module (if they were enabled with -enableOutputSysTasks)
  void disableOutputSysTasks(NUModule* mod);

  //! Enable OutputSysTasks in this module (even if -enableOutputSysTasks was not specified on cmd line)
  void enableOutputSysTasks(NUModule* mod);

  //! Utility method for resolving module directives (flatten & hideMod)
  typedef void (IODBNucleus::*ModuleFunction)(NUModule*);

  //! Mark that all instances of a module and all of its contents are to be flattened.
  void flattenModule(NUModule*);

  //! Mark that all contents of a module are to be flattened.
  void flattenModuleContents(NUModule*);

  //! Mark that flattening is allowed under/within the named module.
  void allowFlattening(NUModule*);

  //! Mark that all sub-hierarchy of a module is to be flattened.
  void disallowFlattening(NUModule*);

  //! wildcard match on modules
  void findMatchingModules(NUModuleList* modules, const char* modName);

  //! Wildcard match on tasks within a given module.
  void findMatchingTasks(NUModule * module,
                         const char * taskNameToMatch,
                         NUTaskList * tasks);

  //! Static function to find all the children with matching names for the wildcard.
  static void sGetWildcardMatchingChildren(STBranchNode* parent,
                                           STSymbolTable* symtab, const char* name,
                                           UtList<STSymbolTableNode*>* matchingChildren);

  //! This is intended to be called when decoding a .cem file
  void addHideModuleName(const char* modName);

  //! Remove a net from all IODB sets
  /*! This call merely schedules a net for removal. Call the
   *  flushRemovedNets to actually do the removal.
   *
   *  The nets are accumulated like this to avoid an M*N algorithm.
   */
  void removeNet(STAliasedLeafNode* node);

  //! Erase removed nets from a net map
  void flushRemovedNets(NameSetMap* netMap);

  //! Remove all the nets that were scheduled with removeNet
  void flushRemovedNets();

  //! A unique set of port names
  typedef UtSet<UtString*, CmpStrs> CPortNames;

  //! An iterator to visit a unique set of port names
  typedef Loop<CPortNames> CPortNamesLoop;

  //! The types of c-model timing
  enum CPortTimingType {
    eCTInvalid,	//!< Unknown output port timing
    eCTRise,	//!< The output port changes on the rising edge of a clock
    eCTFall,	//!< The output port changes on the falling edge of a clock
    eCTAsync	//!< The output port has asynchronous paths from its inputs.
  };

  //! Check if a module is marked as a cmodel
  /*! Checks the database populated by the directives to see if this
   *  module has been declare as a CModule. It it has, it marks the
   *  database entry as touched and returns true. During the directive
   *  evaluation pass, if any cmodules have not been touched, a
   *  warning is printed.
   */
  bool isCModel(const char* modName);

  //! Saves the onDemand state offset for a node
  void putOnDemandStateOffset(STSymbolTableNode *leaf, UInt32 offset);

  //! Saves the total onDemand state size
  void putOnDemandStateSize(UInt32 size);

  //! Record a runtime OnDemand state exclusion net
  void putOnDemandRuntimeExcluded(STSymbolTableNode *leaf);

  //! Add a string attribute
  void addStringAttribute(const UtString& name, const UtString& value);
  //! Add an integer attribute
  void addIntAttribute(const UtString& name, UInt32 value);

  //! Remove a node from the set of OnDemand idle deposit nets
  void removeOnDemandIdleDeposit(STSymbolTableNode *node);

  //! Add a clock name for glitch detection, returning its index
  UInt32 addClockGlitchName(const UtString& name);

  //! Forward class definition
  class CPortEntry;

  //! Abstraction for a a c-model output port entry
  class CPortEntry
  {
  public: CARBONMEM_OVERRIDES
    //! Abstraction for the type of fanin for this entry
    enum FaninType
    {
      eUnspecified,     //!< There was a missing fanin directive (not legal)
      eFullFanin,       //!< All input ports should be listed as fanin (except clock edges)
      eEmptyFanin,      //!< There should be no level fanin
      eListedFanin      //!< Use the loopFanin iterator to get the fanin
    };

    //! constructor
    CPortEntry(CPortTimingType type, UtString* clkName,
               const SourceLocator loc) :
      mTimingType(type), mFaninType(eUnspecified), mClkName(clkName), mLoc(loc)
    {
      mCPortNames = new CPortNames;
    }

    //! destructor
    ~CPortEntry()
    {
      delete mCPortNames;
    }

    //! Get the timing information for this port entry
    CPortTimingType getPortTiming(const UtString** clkName) const
    {
      if (clkName != NULL)
        *clkName = mClkName;
      return mTimingType;
    }

    //! Put the fanin type
    void putFaninType(FaninType type) { mFaninType = type; }

    //! Add a set of known fanins for this port (fanin type must be listed)
    void addFanin(UtString* inPort);

    //! Get the fanin type
    FaninType getFaninType() const { return mFaninType; }

    //! Iterate over the port fanin for this entry (specific to this timing)
    CPortNamesLoop loopFanin() const;

    //! Comparison routine so that we can create unique sets of port entries
    /*! Note that we don't compare by location so we can test for
     *  duplicate directives.
     */
    int compare(const CPortEntry* other) const
    {
      int cmp = mTimingType - other->mTimingType;
      if (cmp == 0)
      {
	if ((mClkName == NULL) && (other->mClkName == NULL))
	  cmp = 0;
	else if (mClkName == NULL)
	  cmp = 1;
	else if (other->mClkName == NULL)
	  cmp = -1;
	else
	{
	  cmp = mClkName->compare(*other->mClkName);
	  if (cmp == 0)
            LOC_ASSERT(mClkName == other->mClkName, mLoc);
	}
      }
      return cmp;
    }

    //! Get the location of the directive
    const SourceLocator getLoc() const { return mLoc; }

  private:
    //! Hide copy and assign constructors.
    CPortEntry(const CPortEntry&);
    CPortEntry& operator=(const CPortEntry&);

    // The data for this port entry
    CPortTimingType mTimingType;
    FaninType mFaninType;
    UtString* mClkName;
    CPortNames* mCPortNames;
    const SourceLocator mLoc;
  }; // class CPortEntry

  //! Comparison for port entries so that we have unique ones
  struct ComparePortEntries
  {
    bool operator()(const CPortEntry* pe1, const CPortEntry* pe2) const
    {
      return pe1->compare(pe2) < 0;
    }
  };

  //! Abstraction for the complete timing/fanin for a c-model output port
  typedef UtSet<CPortEntry*, ComparePortEntries> CPort;

  //! An iterator over the c-model output port timing/fanin entries
  typedef CLoop<CPort> CPortLoop;

  //! Abstraction for the output port timing/fanin information
  typedef UtMap<UtString*, CPort*, CmpStrs> CPorts;

  //! An iterator over the c-model output ports
  typedef LoopMap<CPorts> CPortsLoop;

  //! Returns an iterator over the output port timing/flow information
  CPortsLoop loopOutputPorts(const char* modName) const;

  //! Returns true if the c-model has a null port
  bool hasNullPort(const char* modName) const;

  //! Returns true if the c-model should be run during playback
  bool isCallOnPlayback(const char* modName, bool isModule) const;

  //! Returns an iterator over the output port entries
  CPortLoop loopPortEntries(const CPort*) const;

  //! Returns an iterator over the input ports
  CPortNamesLoop loopInputPorts(const char* modName) const;

  //! Information about a c-model task/function argument
  class CTFArg
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    CTFArg(PortDirectionT dir, int bitSize, UtString* name) :
      mDirection(dir), mBitSize(bitSize), mName(name)
    {}

    //! Get the direction for this port
    PortDirectionT getDirection() const { return mDirection; }

    //! Get the bit size for this port
    int getBitSize() const { return mBitSize; }

    //! Get the name for this port
    UtString* getName() const { return mName; }

    //! Returns true if this is a NULL port
    bool isNullPort() const;

    PortDirectionT	mDirection;
    int			mBitSize;
    UtString*		mName;
  };

  //! Abstraction for a UDTF's arguments
  typedef UtVector<CTFArg*> CTFArgs;

  //! Abstraction to iterate over a UDTF's arguments
  typedef Loop<CTFArgs> CTFArgsLoop;

  //! Iterate over a UDTF's arguments
  CTFArgsLoop loopTFArgs(const char* name) const;

  //! Is this net, in the form "module.net", mentioned in an input directive?
  bool isInputNet(StringAtom* mod, StringAtom* net) const;

  //! IOTable map type
  typedef UtHashMap<const STSymbolTableNode*, SourceLocator> IOTable;
  
  //! Is this name mentioned in any directive at all?
  bool isDirectiveName(const char* name) const;

  //! Returns the customer db
  UtCustomerDB* getCustDB();

  //! Do some sanity checks and any clean up before writing the db
  void preWriteCheck();

  //! Interface to the various isProtected methods.
  /*!
   * This class is here to only allow NUNet to have access
   * to those methods.  The rest of the compiler should call
   * the corresponding NUNet methods.  This is because the IODB methods
   * does not look at aliases, but the NUNet methods do look at aliases.
   */
  class ProtectedInterface
  {
  private:
    friend class NUNet;
    static bool isProtected(const IODBNucleus *iodb, const NUNet *net)
    {
      return iodb->isProtected(net);
    }
    static bool isProtectedObservable(const IODBNucleus *iodb, const NUNet *net)
    {
      return iodb->isProtectedObservable(net);
    }
    static bool isProtectedMutable(const IODBNucleus *iodb, const NUNet *net)
    {
      return iodb->isProtectedMutable(net);
    }
    static void putIsProtectedObservable(IODBNucleus *iodb, const NUNet *net)
    {
      iodb->markProtectedObservable(net);
    }
    static void putIsProtectedMutable(IODBNucleus *iodb, const NUNet *net)
    {
      iodb->markProtectedMutable(net);
    }
    static void putIsDepositable(IODBNucleus *iodb, NUNet *net)
    {
      iodb->markDepositable(net);
    }
    static void removeIsProtectedMutable(IODBNucleus *iodb, const NUNet *net)
    {
      iodb->removeProtectedMutable(net);
    }
    static void removeIsProtectedObservable(IODBNucleus *iodb, const NUNet *net)
    {
      iodb->removeProtectedObservable(net);
    }
  };
  
  //! Add this net's intrinsic information to the type dictionary
  void addTypeIntrinsic( const NUNet *net );

  //! Add the top level ports to the database.
  /*!
    The ports come from an unelaborated design. After elaboration,
    translatePrimaryPorts is called to translate the StringAtoms
    saved here to the elaborated symbol table nodes.
  */
  void putPrimaryPorts (const NUNetList& ports);

  //! Translate unelaborated top-level ports to elaborated nodes
  void translatePrimaryPorts();
  
  //! Run through all nodes in the iodb and initialize their tag type
  /*!
    This must be called after allocating the storage boms. Currently,
    this is done after the scheduler runs (actually, as the last
    step).
    This will properly initialize the type tags for nodes we know that
    we have to write out.
  */
  void initTagTypes();
  
  //! parse a single directive line from a file.
  /*!
   *! This is called from the directives-file parsing code and also
   *! the meta-comment verilog parser.
   */
  bool parseDirective(const char* keyword, const char* rest,
                      StrToken& tok, const SourceLocator& loc);

  //! Append all protected and primary i/o netelabs to the given set
  void appendProtectedNetElabs(NUNetElabSet* elabs, NUDesign* design); 

  // Stores the string information for the substituteModule directives.
  class NUSubstituteModuleData
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    NUSubstituteModuleData(UtString name) :
     mFound(false), mPortMap(NULL), mName(name)
    {}
    ~NUSubstituteModuleData(){ if(mPortMap) delete mPortMap; }

    bool      	mFound;
    NUPortMap*  mPortMap;
    UtString	mName;
  };

  class CaseInsensitiveCompare :
    public std::binary_function<UtString&, UtString&, bool>
  {
  public:
    //! Performs a case insensitive comparison of string keys.
    bool operator()(const UtString& p1, const UtString& p2) const {
      return (strcasecmp(p1.c_str(), p2.c_str()) < 0);
    }
  };

  typedef UtMap<UtString, NUSubstituteModuleData* > NUSubstituteModuleMap;

  typedef UtMap<UtString, NUSubstituteModuleData*, CaseInsensitiveCompare > NUCaseInsensitiveSubstituteModuleMap;
 
  //! Create data which represents the module/instance substitution.
  bool addSubstituteModule(StrToken& tok, const SourceLocator& loc, Lang lang);
  bool readPortList(StrToken& tok, const SourceLocator& loc, NUPortList*);
  bool findSubstituteModule(const UtString& orig_mod, bool case_insensitive_search,
                            UtString *new_mod, NUPortMap **portMap, Lang& lang);
  //! Looping context all the substitute module directives
  typedef LoopMap<NUSubstituteModuleMap> SubstituteLoop;

  //! Loop over all the substitute module directives
  SubstituteLoop loopSubstituteModules(Lang lang) {
    return SubstituteLoop(mSubstituteDUMap[lang]);
  }

  //! Free the memory for storing the sustituteModule directives.
  //! Check for unused directives.
  void freeSubstituteModules();
  
  //! Share a bitvector factory allocated & managed elsewhere
  /*!
   *! The user must ensure that the specified bvpool outlasts this IODB
   */
  void shareBVFactory(DynBitVectorFactory* bvpool);

  //! A simple clock event
  typedef std::pair<ClockEdge, STSymbolTableNode*> ClockEvent;

  //! A list of clock events
  typedef UtVector<ClockEvent> ClockEvents;

  //! Iterator over a list of clock events
  typedef Loop<ClockEvents> ClockEventsLoop;

  //! Returns an iterator over the outputs timing events
  ClockEventsLoop loopOutputsTimingEvents();

  //! Returns an iterator over the inputs timing events
  ClockEventsLoop loopInputsTimingEvents();

  //! An array of wildcards
  typedef UtVector<UtWildcard*> Wildcards;

  //! Iterator over a list of wildcards
  typedef Loop<Wildcards> WildcardsLoop;

  //! Returns an iterator over the vector match strings
  WildcardsLoop loopVectorMatches();

  //! Returns true if there are vector matches
  bool hasVectorMatches() const;

  //! Open up libdesign.dir so we can log all the directives we've parsed
  bool openLogFile(const char* filename);

  //! Close libdesign.dir 
  void closeLogFile();

  //! Return the source locator for the node in the iotable
  /*!
    If the node is in the mIOTable map then it will return the locator
    assigned to that node; otherwise, it returns NULL.
  */
  const SourceLocator* getLocFromIOTable(const STSymbolTableNode* node) const;

  //! Is this node in the iotable?
  bool isInIOTable(const STSymbolTableNode* node) const;

  //! Walk the symboltable and mark reconstruction nets
  void markReconSubordinates();

  //! Set value for dead bits that do not affect outputs
  void putDeadBitWave(Carbon4StateVal val) { mDeadBitWave = val; }
  //! Set value for dead bits that do affect outputs
  void putUndrivenBitWave(Carbon4StateVal val) { mUndrivenBitWave = val; }
  //! Get value for dead bits that do not affect outputs
  Carbon4StateVal getDeadBitWave() const { return mDeadBitWave; }
  //! Get value for dead bits that do affect outputs
  Carbon4StateVal getUndrivenBitWave() const { return mUndrivenBitWave; }

  //! Get the bommanager
  CbuildSymTabBOM* getBOMManager();

  //! return true if the user specified this module in a disableOutputSysTask directive.
  bool isDisableOutputSysTasks(StringAtom* module_name) const;
  //! return true if the user specified this module in a enableOutputSysTask directive.
  bool isEnableOutputSysTasks(StringAtom* module_name) const;


  //! Is this module specified in any kind of net directive?
  /*!
   *! Keeps track of all modules listed in net-directives, such
   *! as observeSignal and tieNet.  Those need to be disqualified from
   *! congruence analysis.  Note that module-directives are all recorded
   *! in flags in NUModule, and congruency knows how to compare those
   *! so that only similarly-marked modules are considered for congruence
   *! with one another.
   */
  bool isModuleSpecifiedInNetDirective(NUModule* mod) const;

  //! Perform the following net replacements for all design directives
  void replaceNets(const NUNetReplacementMapNonConst& replacements);

  //! Find a named symbol-table node in the parse table
  STBranchNode* findBranch(const STBranchNode* node, StringAtom* sym);

  //! Get the nucleus object associated with a parse-tree node
  static NUBase* getNucleusObject(const STSymbolTableNode* node);

  //! Get the parse table
  STSymbolTable* getParseTable();

  //! Fully elaborate the scopes in the parse-table
  void fullElabParseTable();

  //! Get the NUDesign object
  NUDesign* getDesign() { return mDesign; }

  //! Map a force subordinate to the actual forcible
  /*!
    A forcible has a value and mask within it. The shell uses those
    nets explicitly. To keep forcibles and their subordinates easy to
    maintain in the shell, we need to write out the subordinates
    whenever we write out the actual forcible type in the db.

    This maps the subordinate to the actual forcible. So, during leaf
    selection, a leaf is checked to be a subordinate. If it is, we get
    the actual, check if that is selected for write. If so, that
    subordinate will also get written.
  */
  void mapForceSubordinate(STAliasedLeafNode* subord, STAliasedLeafNode* forcible);
  
  //! Returns the forcible node, given a force-subordinate
  /*!
    To check if a net is a subordinate, use the more efficient
    CbuildSymTabBOM::isForceSubordinate(leaf);
    
    This returns NULL if the leaf is not a subordinate.
  */
  const STAliasedLeafNode* getSubordinateMasterForcible(const STAliasedLeafNode* forceSubordinate) const;

  //! Saves the schedule-generated sensitivities for wrappers.
  void saveWrapperSensitivity(const CNetSet &bothEdgeClks,
                              const PortNetSet &validOutPorts,
                              const PortNetSet &validInPorts,
                              const PortNetSet &validTriOutPorts,
                              const PortNetSet &validBidiPorts,
                              const CNetSet &clksUsedAsData,
                              const ClkToEvent &clkEdgeUses);

  //! Add to the list of nets that are optimized away during population.
  /*!
    A sanity check performed after evaluateDesignDirective(), to ensure that
    nets optimzied away during population ..for example by constant propagating
    them, are not protected mutable.
  */
  void addPopulationOptimizedNets(NUNetSet& optimizedNets);

  //! Copy the protected net maps from a src IODB to this.
  /*!
   *! NOTE:  THIS IS EXPERIMENTAL CODE AND NOT ALL THE MEMBER VARIABLES
   *! OF THE IODB ARE TRANSFERRED YET.
   */
  void copyProtectedNets(CopyContext& cc, const IODBNucleus& src);

private:
  friend class ProtectedInterface;

  //! Compute which schedule trigger sets the pnet belongs in
  /*!
    Called by saveWrapperSensitivity.
  */
  void computeScheduleTriggerSetMembership(const CGPortIfaceNet* pnet,
                                           const CNetSet &bothEdgeClks,
                                           const CNetSet &clksUsedAsData,
                                           const ClkToEvent &clkEdgeUses);
    
  /*!
   * Warn for primary inputs or inouts being depositable and remove
   * from the set.  Removed from the set to make sure that there is no
   * performance impact (for example, scheduling doesn't create
   * pessimistic schedule for them).
   */
  void removeInvalidDeposits(STSymbolTable* symtab, CbuildSymTabBOM* symTabBOM);

  /*!
   * Apply the deposit markings to the split nets for any port split
   * deposit nodes.
   */
  void applySplitNetDeposits(ReachableAliases* reachables,
                             STSymbolTable* symtab,
                             CbuildSymTabBOM* symTabBOM);

  /*!
   * The nets will be marked depositable, and also the given ReachableAliases
   * class will be called to remember the depositable elaborated nets.
   */
  void depositNetMarker(ReachableAliases *reachables,
                        STSymbolTable* symtab,
                        bool markDepositAliases);

  //! Mark all of the reachable aliases of the given elaborated net as depositable.
  void markReachableAliasesDepositable(NUNetElab *net, ReachableAliases *reachables);

  //! Is any instance of an NUNet* protected?
  /*!
   * The ::isProtected*() methods are intended to be called by
   * unelaborated analysis, which must make pessimistic decisions on
   * which nets may be optimized.
   *
   * If *any* instance of a module declares a net protected, all
   * instances of that net will be marked as protected (since this is
   * an unelaborated mark).
   *
   * When performing elaborated analyses, use the observable nodes
   * set/other interfaces for less pessimism.
   *
   * Nets mentioned in user directives are considered protected.
   *
   * \param net The net that may be marked as protected.
   * \returns true If the net is marked for either observability or mutability protection.
   *
   * \sa isProtectedObservable, isProtectedMutable
   */
  bool isProtected(const NUNet* net) const {
    return (isProtectedObservable(net) or isProtectedMutable(net));
  }


  //! Is any instance of a NUNet* written outside its declaring module?
  /*!
   * A net is marked for mutability protection if some instance of
   * that net is marked for user writing through the forceSignal
   * or depositSignal directives
   *
   * If *any* instance of a module declares a net protected, all
   * instances of that net will be marked as protected (since this is
   * an unelaborated mark).
   *
   * \param net The net that may be marked for mutability protection.
   * \returns true If the net is marked for mutability protection.
   *
   * \sa isProtected, isProtectedObservable
   */
  bool isProtectedMutable(const NUNet* net) const;

  //! Is any instance of a NUNet* read outside its declaring module?
  /*!
   * A net is marked for observability protection if some instance of
   * that net is marked for user reading through the observeSignal
   * directive, or marked for clock equivalence through the
   * collapseClock directive.
   *
   * If *any* instance of a module declares a net protected, all
   * instances of that net will be marked as protected (since this is
   * an unelaborated mark).
   *
   * \param net The net that may be marked for observability protection.
   * \returns true If the net is marked for observability protection.
   * 
   * \sa isProtected, isProtectedMutable
   */
  bool isProtectedObservable(const NUNet* net) const;

  bool parsePaths(const char* p, NameSet* names, const SourceLocator& loc);
  bool parseAssocPaths(const char* p, NameSetMap* names, const SourceLocator& loc);
  bool adjustSeverity(MsgContextBase::Severity sev, StrToken* token,
                      const SourceLocator& loc);
  bool adjustDefault(UInt32* number, StrToken* token, const SourceLocator& loc);

  void checkTypeID(const STSymbolTableNode* protectedNode, const char* marking, CbuildSymTabBOM* symTabBOM) const;
  
  void findBidis(STAliasedLeafNode* leaf, NameSet* bidis);
  // Runs through alias loop looking for eFlop or eLatch on the
  // netflags. Then, marks the alias ring if either is present.
  void maybeMarkStateOutput(STAliasedLeafNode* leaf);
  void cleanConstant(STSymbolTableNode*);

  struct SymTabVal
  {
    SymTabVal() {
      mSrcLocID = SourceLocator::NULL_ID;
      mNucleusObj = NULL;
    }
    
    SourceLocator::ID mSrcLocID;
    NUBase* mNucleusObj;
  };

  class SymTabBOM;
  friend class SymTabBOM;

  class SymTabBOM : public virtual STFieldBOM
  {
  public: CARBONMEM_OVERRIDES
    SymTabBOM();
    virtual ~SymTabBOM();
    
    virtual Data allocBranchData();
    virtual Data allocLeafData();
    
    virtual void freeLeafData(const STAliasedLeafNode*, Data* bomdata);
    virtual void freeBranchData(const STBranchNode*, Data* bomdata);

    void freeData(Data* bomdata);

    virtual void preFieldWrite(ZostreamDB&);
    virtual ReadStatus preFieldRead(ZistreamDB&);

    virtual void writeLeafData(const STAliasedLeafNode* leaf, ZostreamDB& out) const;
    
    virtual void writeBranchData(const STBranchNode* branch, ZostreamDB& out,
                                 AtomicCache*) const;
    
    virtual ReadStatus readLeafData(STAliasedLeafNode* leaf, ZistreamDB& out, 
                                    MsgContext* msg);
    
    virtual ReadStatus readBranchData(STBranchNode* branch, ZistreamDB& out, 
                                      MsgContext* msg);
    
    virtual void printBranch(const STBranchNode* branch) const;
    virtual void printLeaf(const STAliasedLeafNode* leaf) const;
    
    void printData(const Data bomdata) const;

    //static const SymTabVal* castBOM(const Data bomdata);
    static SymTabVal* castBOM(Data bomdata);

    virtual void writeBOMSignature(ZostreamDB&) const {
    }
    
    virtual ReadStatus readBOMSignature(ZistreamDB&, UtString*) {
      return eReadIncompatible;
    }

    //! Return BOM class name
    virtual const char* getClassName() const
    {
      return "SymTabBOM";
    }
    
    //! Write the BOMData for a branch node
    virtual void xmlWriteBranchData(const STBranchNode* /*branch*/,UtXmlWriter* /*writer*/) const
    {
    }
    
    //! Write the BOMData for a leaf node
    virtual void xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/,UtXmlWriter* /*writer*/) const
    {
    }
    


  };

  //! Check that all module-based directives reference valid module names.
  void checkModuleDirectives();

  //! Check that all task/function directives reference valid module.task/function names.
  void checkTFDirectives();

  //! Add any implicit directives
  /*!
    Some directives need to be in place without being specified by the
    user.  For example, OnDemand adds an "idle" status net to the top
    module, and it needs to be marked observable.
   */
  void addImplicitDirectives();

  // Note that StringAtom* is immutable, so making it const is superfluous
  typedef UtHashSet<StringAtom*> AtomSet;

  //! resolve a module directive, now that we have parsed the modules
  void resolveModuleDirective(const AtomSet&, ModuleFunction, IODBDirective::ModuleDirective);

  //! Mark a name for observability protection. (return true if successful on marking)
  bool markProtectedObservable(STSymbolTableNode * node);  

  //! Mark a net for observability protection.
  void markProtectedObservable(const NUNet* net);  

  //! Remove net from list of protected observable nets
  void removeProtectedObservable(const NUNet* net); 

  //! Mark a name for mutability protection. (return true if successful on marking)
  bool markProtectedMutable(STSymbolTableNode * node);  

  //! Mark a net for mutability protection.
  void markProtectedMutable(const NUNet* net);   

  //! Remove net from list of protected mutable nets
  void removeProtectedMutable(const NUNet* net);  

  void markDepositable(NUNet* net);
  void removeDepositableNUNet(NUNet* net);

  void markForcible(NUNet* net);
  void markAsyncResets();

  enum ProtectedMode {
    eProtectedUnknown    = 0x0,
    eProtectedObservable = 0x1,
    eProtectedMutable    = 0x2
  };


  // Marks the net with the directive
  bool markDirNet(STSymbolTableNode *node,  NameSet* nodes, const UtStringArray& elemList, 
		  IODBDirective::DesignDirective dir, ProtectedMode protectedMode);
  // Marks the subnets with the directive
  bool markDirSubNets(STBranchNode *node,  NameSet* nodes, const UtStringArray& elemList, 
		      IODBDirective::DesignDirective dir, ProtectedMode protectedMode);

  //! recursive method used to build elaborated names and apply \a protectedMode to items that match by name
  /*!
   * \returns true if any nets were added to the nodes set indicating that they had \a protectedMode applied to them.
   */
  bool buildNodes(IODBDirective::DesignDirective dir,
                  STBranchNode* parent, 
                  size_t i,
                  const UtStringArray& elemList,
                  const SourceLocator& loc,
                  NameSet* nodes,
                  bool findModules,
                  bool withinATask,
		  ProtectedMode protectedMode);
  void fullElabModule(NUModule* mod, STBranchNode* parent);

  STBranchNode* elabDeclScope(STBranchNode* parent,
                     NUNamedDeclarationScope* declScope);
  void elabInstance(NUModuleInstance* inst, STBranchNode* parent);
  void declareNet(STBranchNode* parent, NUNet* net,
                  StringAtom* searchSym,
                  STSymbolTableNode** foundNode);
  /*!
    Elaborates nets within a branch, attempting to match a requested node.    

    \param branch The branch to elaborate
    \param matchStr String to match (can be NULL)
    \param matchNode Pointer to the node to store a match (cannot be NULL)
    \param isLeaf Is the requested node a leaf?
    \param findModules Should module instances be elaborated?
   */
  void elaborateBranch(STBranchNode *branch,
                       StringAtom *matchStr,
                       STSymbolTableNode **matchNode,
                       bool isLeaf,
                       bool findModules);
                       

  STBranchNode* buildBranch(const STBranchNode* node);
  bool writeSet(ZostreamDB& symDB, NameSet& ns, STSymbolTable* symtab);
  bool writeMap(ZostreamDB& symDB, NameSetMap& nsMap, STSymbolTable* symtab);
  STSymbolTableNode* declareSignal(const STSymbolTableNode* node);
  STSymbolTableNode* elabNode(StringAtom* name, NUBase* obj,
                              STBranchNode* parent, bool isLeaf);
  bool buildRoots(IODBDirective::DesignDirective dir, const UtStringArray& elemList,
                  const SourceLocator& loc,
                  NameSet* nodes,
                  bool findModules,
		  ProtectedMode protectedMode,
                  IODBDirective::DirectiveMode dirMode);

  // When populating the sets in IODB from the directives files, we are
  // using an elaborated symbol table created by the IODB parsing code:
  // mParseIOTable.
  // After the "real" elaboration takes place in the Reduce phase, we want
  // to get rid of mParseIOTable and change all the sets in the IODB to
  // reference the "real" symbol table.
  // Those ::fix* methods do that.
  bool fixSet(NameSet* ns);
  bool fixIntMap(NameIntMap* ns);
  bool fixSetMap(NameSetMap* ns);
  bool fixValueMap(NameValueMap* nvm, bool errOnMissing);
  bool fixClockEvents(ClockEvents* clockEvents);

  // Copy user type information for fields to the branch leafs.
  void copyFieldUserTypeInfo(STBranchNode* branch, const UserType* ut);
    
  // Inconveniently, we must support the Cheetah population process
  // testing for port direction overrides *before* we have any
  // nucleus objects or a symbol table.  This kludge helps us do that.
  UtStringSet mInputNetStrings; 

  // These are the vector match strings
  Wildcards mVectorMatches;

  // This table is used for encrypted model construction, to avoid
  // obfuscating names that are mentioned in directives
  UtStringSet mDirectiveNames;

  // The following types and data are used for storing module-based
  // blast net directives.
  UtStringSet mBlastedNetNames;
  
  typedef UtArray<StringAtom*> AtomVec;
  AtomVec mDesignPortNames;

  IOTable mIOTable;

  SymTabBOM mSymTabBOM;
  ArgProc * mArgs;
  IOTable* mParseIOTable;
  STSymbolTable* mParseTable;   // parsed before design is loaded
  bool mIsElaborated;
  typedef UtVector<STBranchNode*> STNodeVector;
  typedef UtHashMap<NUModule*,STNodeVector> InstanceMap;
  InstanceMap* mInstanceMap;

  // Types and data for constants that are created before the final
  // elaborated symbol table is created. We store these in terms of
  // the IODB parse table and then translate them to the real symbol
  // table when it is available.
  typedef std::pair<DynBitVector, Carbon4StateVal> ConstVal;
  typedef UtMap<STAliasedLeafNode*, ConstVal> ConstTable;
  typedef LoopMap<ConstTable> ConstTableLoop;
  ConstTable mConstTable;

  bool addDesignDirective(IODBDirective::DesignDirective dirType, const char* p,
                          const SourceLocator& loc,
                          const char* val = NULL);
  bool addValueDirective(IODBDirective::DesignDirective dirType, StrToken& tok,
                         const SourceLocator& loc);
  bool addEventDirective(IODBDirective::DesignDirective dirType, StrToken& tok,
                         const SourceLocator& loc);

  //! Remember that we've seen a module-based directive.
  bool addModuleDirective(const char* p, const SourceLocator& loc,
    IODBDirective::ModuleDirective eType);

  //! Process the module directives
  bool evaluateModuleDirective (IODBModuleDirective *);

  //! Remember that we've seen an inline \<task\> directive.
  bool addInlineTFTok(const char* p, const SourceLocator & loc);

  /*! \brief go through all tokens for a directive, remember the names seen, also set flags on the nodes for those names.
   *
   * \param dir directive being processed (includes all tokens)
   * \param names set to keep track of items that have been seen
   * \param protectedMode  what mode that is to be set protected/observable/mutable
   * \param skip  if true, then skip first token
   * \param findModules
   * \param dirMode  (controls if directive applied to modules/instances/or both)
   */
  bool recordPaths(IODBDesignDirective* dir, NameSet* names, 
		   ProtectedMode protectedMode,
		   bool skip = false,
                   bool findModules = false,
                   IODBDirective::DirectiveMode dirMode = IODBDirective::eDirBoth);

  //! Class to record a name association
  /*! Derived this class to perform the various associations necessary
   */
  class AssocPathCallback
  {
  public:
    //! constructor
    AssocPathCallback(MsgContext* msgContext) : mMsgContext(msgContext) {}

    //! virtual destructor
    virtual ~AssocPathCallback() {}

    //! Callback on a single name association (return true for success)
    virtual bool recordPaths(IODBDesignDirective* dir, const NameSet& nodes,
                             ProtectedMode protectedMode) = 0;

    //! Get the msg context
    MsgContext* getMsgContext(void) const { return mMsgContext; }
    
  private:
    MsgContext* mMsgContext;
  };

  //! Class to record associations with other names
  class NameSetMapCallback : public AssocPathCallback
  {
  public:
    //! constructor
    NameSetMapCallback(IODBNucleus* iodb, MsgContext* msgContext, 
                       NameSetMap* nameMap) :
      AssocPathCallback(msgContext), mIODB(iodb), mNameMap(nameMap) {}

    //! Function to handle a name association
    bool recordPaths(IODBDesignDirective* dir, const NameSet& nodes,
                     ProtectedMode protectedMode);

  private:
    IODBNucleus* mIODB;
    NameSetMap* mNameMap;
  }; // class NameSetMapCallback : public AssocPathCallback

  //! Class to record associations with an integer
  class NameIntMapCallback : public AssocPathCallback
  {
  public:
    //! constructor
    NameIntMapCallback(NameIntMap* nameIntMap, MsgContext* msgContext) : 
      AssocPathCallback(msgContext), mNameIntMap(nameIntMap),
      mNumber(0), mNumberComputed(false)
    {}

    //! Function to handle a name association
    bool recordPaths(IODBDesignDirective* dir, const NameSet& nodes, ProtectedMode);

  private:
    NameIntMap* mNameIntMap;
    UInt32 mNumber;
    bool mNumberComputed;
  }; // class NameIntMapCallback : public AssocPathCallback

  //! Class to record the deposit associations
  class DepositMapCallback : public AssocPathCallback
  {
  public:
    DepositMapCallback(NameIntMap* nameIntMap, DepositTypes depositType,
                       MsgContext* msgContext) :
      AssocPathCallback(msgContext), mNameIntMap(nameIntMap),
      mDepositType(depositType)
    {}

    //! Function to handle a name association
    bool recordPaths(IODBDesignDirective* dir, const NameSet& nodes, ProtectedMode);

  private:
    NameIntMap* mNameIntMap;
    DepositTypes mDepositType;
  };

  //! Class to record the observe associations
  class ObserveMapCallback : public AssocPathCallback
  {
  public:
    ObserveMapCallback(NameIntMap* nameIntMap, ObserveTypes observeType,
                       MsgContext* msgContext) :
      AssocPathCallback(msgContext), mNameIntMap(nameIntMap),
      mObserveType(observeType)
    {}

    //! Function to handle a name association
    bool recordPaths(IODBDesignDirective* dir, const NameSet& nodes, ProtectedMode);

  private:
    NameIntMap* mNameIntMap;
    ObserveTypes mObserveType;
  };

  // Friend classes to process name associations
  friend class NameSetMapCallback;
  friend class NameIntMapCallback;
  friend class DepositMapCallback;
  friend class ObserveMapCallback;

  /*! \brief go through all tokens for a directive that associates two
   * (or more) items together, remember the items seen, also set flags
   * on the nodes for those names. 
   *
   * \param dir directive being processed (includes all tokens)
   * \param protectedMode  what mode that is to be set protected/observable/mutable
   * \param callback contains function to call for each association
   * \param singleNode If set only translate the first token to a node
   * \param findModules
   * \param dirMode  (controls if directive applied to modules/instances/or both)
   */
  bool recordAssocPaths(IODBDesignDirective* dir,
			ProtectedMode protectedMode,
                        AssocPathCallback* callback,
                        bool singleNode = false,
                        bool findModules = false,
                        IODBDirective::DirectiveMode dirMode = IODBDirective::eDirBoth);
  
  bool recordPathValue(IODBDesignDirective *dir, NameValueMap* nameValMap,
                       ProtectedMode protectedMode, bool findModules = false,
                       IODBDirective::DirectiveMode dirMode = IODBDirective::eDirBoth);
  bool recordEventPaths(IODBDesignDirective *dir, ClockEvents* clockEvents,
                        ProtectedMode protectedMode);
  bool recordWildcard(IODBDesignDirective *dir, Wildcards* wildcards);

  bool convertValue(const char* val, const SourceLocator&, DynBitVector&);

  IODBDesignDirective::List mDesignDirectives; //!< design directives to be evaluated
  IODBModuleDirective::List mModuleDirectives; //!< store for module directives

  int mLineNumber;
  UtString mFilename;

  NUDesign* mDesign;

  // Information about module c-models in the design
  struct CModuleData;
  friend struct CModuleData;
  struct CModuleData
  {
    CModuleData(const SourceLocator& loc) : 
      mLoc(loc), mTouched(false), mCallOnPlayback(false)
    {}
    SourceLocator	mLoc;		// Location of cModuleBegin directive
    CPortNames		mInputPorts;
    CPorts		mOutputPorts;
    bool		mTouched;
    bool                mCallOnPlayback;
  };

  // The seen arguments so far
  typedef UtSet<UtString*, CmpStrs> SeenArgs;

  // Information about task/function c-models in the design
  struct CTFData
  {
    CTFData(const SourceLocator& loc) :
      mLoc(loc), mTouched(false), mCallOnPlayback(false), mFunctionResultSize(0)
    {}

    SourceLocator	mLoc;
    CTFArgs		mArgs;
    SeenArgs		mSeenArgs;
    bool		mTouched;
    bool                mCallOnPlayback;
    int			mFunctionResultSize;
  };

#if pfSPARCWORKS
  struct CModelHash
  {
      size_t hash(const UtString* var) const {
        return var->hash();
      }
    
    //! lessThan operator -- for sorted iterations -- relies on class operator<
    bool lessThan(const UtString* v1, const UtString* v2) const {
      return *v1 < *v2;
    }
    
    //! shallow less-than function -- for Microsoft hash tables
    bool lessThan1(const UtString* v1, const UtString* v2) const {
      return v1 < v2;
    }
    
    //! equal operator -- for hashing on pointer value
    bool equal(const UtString* v1, const UtString* v2) const {
      return *v1 == *v2;
    }
  };
#else
  typedef HashPointerValue<UtString*> CModelHash;
#endif

  typedef UtHashMap<UtString*, CModuleData*, CModelHash> CModules;
  CModules* mCModules;

  typedef UtHashMap<UtString*, CTFData*, CModelHash> CTFs;
  CTFs* mCTFs;

  enum CModelType
  {
    eInvalid,
    eCModule,
    eCFunction,
    eCTask
  };

  // Context for inserting cmodel directives
  struct CModelContext;
  friend struct CModelContext;
  struct CModelContext
  {
    CModelContext() :
      modelName(NULL), modelType(eInvalid), 
      moduleData(NULL), portName(NULL), port(NULL), portEntry(NULL),
      tfData(NULL), ignoreDirectives(false), mCallOnPlayback(false)
    {}
    const UtString*	modelName;
    CModelType		modelType;
    CModuleData*	moduleData;
    const UtString*	portName;
    CPort*		port;
    CPortEntry*		portEntry;
    CTFData*		tfData;
    bool		ignoreDirectives;
    bool                mCallOnPlayback;
  };

  CModelContext* mCModelContext;


  // Functions to populate the c-model directives
  bool getCModelName(const char*,StrToken&,const SourceLocator&,const char**);
  CModuleData* findCModuleData(const char* modName) const;
  bool addCModelDirective(IODBDirective::DesignDirective dir, StrToken& tok,
			  const SourceLocator& loc);
  bool addCModule(StrToken&, const SourceLocator&, CModelContext&);
  bool getCPortName(StrToken& tok, const SourceLocator& loc,
		    const char* portType, const char* directive,
		    const char** portName);
  CModuleData* getModule(const SourceLocator& loc, CModelContext& cntxt,
			 const char* directive, const char** modName = NULL);
  bool addCPort(StrToken& tok, const SourceLocator& loc, CModelContext& cntxt);
  bool addCNullPort(const SourceLocator& loc, CModelContext& cntxt);
  void createCPort(CModuleData*, const char*, CModelContext&);
  CPort*getPort(const SourceLocator&, CModelContext&,
		const char* directive, const char** portName = NULL);
  UtString* addInputPort(CModuleData* data, const char* clkName);
  bool addCTiming(StrToken&, const SourceLocator&, CModelContext&);
  bool addCFanin(StrToken&, const SourceLocator&, CModelContext&);
  bool terminateCPort(StrToken&, const SourceLocator&, CModelContext&,
                      const char*);
  bool terminateCModule(StrToken&, const SourceLocator&, CModelContext&);
  bool addCFunction(StrToken& tok, const SourceLocator& loc, CModelContext&);
  bool terminateCFunction(StrToken&, const SourceLocator& loc, CModelContext&);
  bool addCTask(StrToken& tok, const SourceLocator& loc, CModelContext& cntxt);
  bool terminateCTask(StrToken&, const SourceLocator& loc, CModelContext&);
  bool createCArg(CTFData* data, const char* argName, int bitSize,
                  PortDirectionT argDirection, const SourceLocator& loc);
  bool addCArg(StrToken& tok, const SourceLocator& loc, CModelContext& cntxt);
  bool addCNullTaskOutput(StrToken& tok, const SourceLocator& loc,
                          CModelContext& cntxt);
  CTFData* getTF(const SourceLocator&, CModelContext&, CModelType,
		 const char*, const char** name = NULL);
  CTFData* findCTFData(const char* name) const;
  bool addCallOnPlayback(StrToken&, const SourceLocator&,
                         CModelContext& cntxt);

  void checkMarkHidden(const STBranchNode* bnode);

  void mapSymNodeToLoc(const STSymbolTableNode* node, 
                       const SourceLocator& loc);
  bool writeSetSelective(ZostreamDB& symDB, NameSet& ns, STSymbolTable* symtab);
  bool isHierRef(const STSymbolTableNode* node) const;

  // Data for input/output timing information
  ClockEvents* mOutputsTimingEvents;
  ClockEvents* mInputsTimingEvents;

  // This gets the NUNetElab from the node and marks it and its
  // aliases as forcible. This is called in resolveForce and should
  // only be called within that context.
  void gatherMarkForcible(STSymbolTableNode* node, bool forceMarking);

  // This just gets the current factoried dynbv from the mConstNets
  // set for the given node and copies it to bv. If node doesn't have
  // an entry, bv is only resized to the correct width.
  void getCurrentConstValue(const STAliasedLeafNode* node, UInt32 bvBitSize, DynBitVector* bv) const;

  //! Helper function to write a map of symbols to an int
  bool writeNameIntMap(NameIntMapLoop p, ZostreamDB& symDB, STSymbolTable* symtab);

  //! Helper class for database writing 
  class DBClosure;
  friend class DBClosure;
  //! save the iodb and the private symbol table to a database
  /*!
    If the selector is NULL, just the i/os and directives are written
    to the db. If it isn't NULL, the entire db is written out as per
    the selector. This can be called many times. However,
    preWriteCheck must be called once before any calls to writeDB.
  */
  bool writeDB(const char* filename, 
    DBClosure* enableClosure);

  //! Helper function so that writeDB can write to .tmp and rename
  bool writeDBHelper(const UtString& fileAndExt, DBClosure* dbClosure);

  //! Perform a sanity check that none of the nets optimized away during population
  //! are protected mutable.
  bool sanityCheckPplOptNets();

  //! Delete the design data symbol tables.
  void deleteDesignDataSymTabs();

  //! Check for a deprecated directive that was removed, returning true (and printing a message) if it is
  bool checkRemovedDirective(IODBDirective::DesignDirective dir, const char* path, const SourceLocator& loc, const char* value);

  SourceLocatorFactory* mSourceLocatorFactory;
  NUCNetSet mProtectedMutableNets;
  NUCNetSet mProtectedObservableNets;

  NUCNetSet mDepositableNUNets;
  NUCNetSet mForcibleNets;
  NUCNetSet mAsyncResetNets;

  //! Keep track of all modules listed in net-directives, such
  //! as observeSignal and tieNet.  Those need to be disqualified from
  //! congruence analysis.  Note that module-directives are all recorded
  //! in flags in NUModule, and congruency knows how to compare those
  //! so that only similarly-marked modules are considered for congruence
  //! with one another.
  UtHashSet<const NUModule*> mModulesSpecifiedInNetDirectives;

  AtomSet mHideModuleNames;
  AtomSet mTernaryGateOptimization;
  AtomSet mNoAsyncResetNames;
  AtomSet mDisableOutputSysTasksNames; // for modules that appeared in disableOutputSysTask directive
  AtomSet mEnableOutputSysTasksNames; // for modules that appeared in enableOutputSysTask directive

  AtomSet mFlattenModuleNames;
  AtomSet mFlattenModuleContentsNames;
  AtomSet mAllowFlatteningModuleNames;
  AtomSet mDisallowFlatteningModuleNames;

  //! Map from module name to task/function names
  typedef UtHashMap<StringAtom*, AtomSet> AtomToAtomSet;

  //! Tracks which tasks/functions are marked for forced inlining.
  AtomToAtomSet mInlineTFNames;

  //! Database of IP tag to be stored in the model
  UtCustomerDB mCustomerDB;

  struct BidiEnable;

  class ExprIdentWalk;
  friend class ExprIdentWalk;
  class ForcibleExprBPWalk;
  friend class ForcibleExprBPWalk;

  class IdentCycleDetect;
  class ExprMarkDeposit;
  class DeadExprDetect;
  class DeadNetWarn;
  class ExprAccuracyWalker;

  // Used to store nets to be removed from the I/O database and
  // directives.
  NameSet* mRemoveNets;

  class MarkForceWalk;
  friend class MarkForceWalk;

  UtString mSubstituteDirective[Lang_MAX];
  
  // The original substitute module map with case sensitive module name keys.
  NUSubstituteModuleMap mSubstituteDUMap[Lang_MAX];

  // Copy of the substitute module map entries with lower case module name keys.
  NUCaseInsensitiveSubstituteModuleMap mCaseInsensitiveSubstituteDUMap[Lang_MAX];

  //! File that logs all the directives we have parsed
  UtOBStream* mDirLogFile;

  //! Keep track of whether we are currently parsing a directives file
  bool mParsingDirectivesFile;
  //! If true, all cmodels will be callable by the replay system.
  bool mPlaybackAllCModels;
  
  //! Keep track of the last filename for which we logged an embedded directive
  UtString mDirLogPrevFile;

  //! Keep a map of collapse nets, populated via findCollapseNets(), used
  //! by constant propagation and RENetWarning
  NUNetElabSet* mCollapseNets;

  Carbon4StateVal mDeadBitWave;
  Carbon4StateVal mUndrivenBitWave;

  typedef UtHashMap<STAliasedLeafNode*, STAliasedLeafNode*> LeafToLeafMap;
  LeafToLeafMap mForceSubordinates;

  //! Keep a set of STSymbolTableNode instances identified as valid paths in checkDirectives.
  UtSet <const STSymbolTableNode *> mCheckedDirectives;

  //! Elaborated and Unelaborated symbol tables used to hold design data.
  IODBDesignDataSymTabs* mDDSymTabs;

  //! Set of nets that have been optimized at population time.
  NUNetSet mPplOptNets;

#if 0
  //! We must begin writing the full DB early, to catch the drivers,
  //! so keep a pointer in the IODB
  ZostreamZip* mFullDB;
#endif


}; //class IODBNucleus : public IODB

#endif
