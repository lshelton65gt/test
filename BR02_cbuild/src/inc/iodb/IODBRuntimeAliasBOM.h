// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/* The unelaborated symbol table entries contain an NUAliasBOM payload. This is
 * a little problematic to use in the GUI at runtime as this BOM contains
 * nucleus objects that cannot and should not be created at runtime. To solve
 * this, the data payload is read into an IODBRuntimeAliasDataBOM.
 */

#ifndef _IODBRUNTIMEALIASBOM_H
#define _IODBRUNTIMEALIASBOM_H

#include "symtab/STFieldBOM.h"
#include "exprsynth/ExprFactory.h"
#include "util/UtHashMap.h"

// forward declarations.
class STSymbolTable;
class STSymbolTableNode;
class ZostreamDB;
class ZistreamDB;

//! Bill of Materials for a module's alias db
class IODBRuntimeAliasDataBOM
{
public: CARBONMEM_OVERRIDES
  //! constructor
  IODBRuntimeAliasDataBOM();

  //! destructor
  ~IODBRuntimeAliasDataBOM();

  //! print contents to stdout
  void print() const;

  enum Flags {
    eIsInput = 1<<0,
    eIsOutput = 1<<1,
    eIsBidi = 1<<2
  };

  void setInput () { mFlags |= eIsInput; }
  void setOutput () { mFlags |= eIsOutput; }
  void setBidi () { mFlags |= eIsBidi; }

  bool isInput () const { return (mFlags & eIsInput) != 0; }
  bool isOutput () const { return (mFlags & eIsOutput) != 0; }
  bool isBidi () const { return (mFlags & eIsBidi) != 0; }

  HierFlags getHierFlags () const { return mHierFlags; }

  friend class IODBRuntimeAliasBOM;

private:
  
  union {
    UInt32 mFlags;                      //!< for leafs
    HierFlags mHierFlags;               //!< for branches
  };

};

//! BOM Manager for a module's alias db.
class IODBRuntimeAliasBOM : public virtual STFieldBOM
{
public: CARBONMEM_OVERRIDES
  IODBRuntimeAliasBOM();

  //! virtual destructor
  virtual ~IODBRuntimeAliasBOM();

  //! Allocate a Data object to be placed in the symbol table.
  virtual Data allocBranchData();

  //! Allocate a Data object to be placed in the symbol table.
  virtual Data allocLeafData();

  //! Delete the Data object
  virtual void freeBranchData(const STBranchNode*, Data * bomdata);

  //! Delete the Data object
  virtual void freeLeafData(const STAliasedLeafNode*, Data * bomdata);

  //! does nothing
  virtual void preFieldWrite(ZostreamDB&);

  //! Returns eReadOK
  virtual ReadStatus preFieldRead(ZistreamDB&);

  //! Write the BOMData for a leaf node
  virtual void writeLeafData(const STAliasedLeafNode* leaf, ZostreamDB& out) const;

  //! Write the BOMData for a branch node
  virtual void writeBranchData(const STBranchNode* branch, ZostreamDB& out,
                               AtomicCache*) const;

  //! print contents to stdout
  virtual void printLeaf(const STAliasedLeafNode* leaf) const;

  //! print contents to stdout
  virtual void printBranch(const STBranchNode* branch) const;

  virtual ReadStatus readLeafData(STAliasedLeafNode* leaf, ZistreamDB& in, 
                                  MsgContext* msg);
  
  //! Currently asserts - not legal
  virtual ReadStatus readBranchData(STBranchNode* branch, ZistreamDB& in, 
                                    MsgContext* msg);

  //! Write the r/w interface signature
  virtual void writeBOMSignature(ZostreamDB& out) const;
  
  //! Read the r/w interface signature
  virtual ReadStatus readBOMSignature(ZistreamDB& in, UtString*);

  //! Return BOM class name
  virtual const char* getClassName() const;

  //! Write the BOMData for a branch node
  virtual void xmlWriteBranchData(const STBranchNode* branch,UtXmlWriter* writer) const;

  //! Write the BOMData for a leaf node
  virtual void xmlWriteLeafData(const STAliasedLeafNode* leaf,UtXmlWriter* writer) const;


  //! This casts the opaque bomdata pointer to a IODBRuntimeAliasDataBOM *
  static IODBRuntimeAliasDataBOM * castBOM(Data bomdata);

  //! This casts a symbol table node to an IODBRuntimeAliasDataBOM
  static IODBRuntimeAliasDataBOM *castBOM (const STSymbolTableNode *);

  static const char *getSignature () { return sSignature; }
  static UInt32 getVersion () { return sVersion; }

private:

  static const char *sSignature;
  static const UInt32 sVersion;

  void freeData(Data * bomdata);
  Data allocData();
  void printData(const Data bomdata) const;  
};

#endif // NUALIASDB_H_
