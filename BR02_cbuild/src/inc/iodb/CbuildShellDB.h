// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef __CBUILDSHELLDB_H_
#define __CBUILDSHELLDB_H_


#include "util/CarbonTypes.h"
#include "util/Util.h"
#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

class ZostreamDB;
class ZistreamDB;
class DynBitVector;
class IODBIntrinsic;
class UtString;
class UserType;

//! Low-level reader/writer for Symboltable database
class CbuildShellDB
{
public: 
  CARBONMEM_OVERRIDES

  //! Type ID tag values for the TypeIndex field 
  /*!
   * Positive values are indices into the debug_generator tables
   * \warning Do \e NOT add more enum values between eNoTypeId and
   * eOffsetId. That allows us to tell if something is constant by
   * seeing if the type index tag value is between those two values.
   */
  enum TypeIndexTag {
    eNoTypeId = 0, //!< No debug type information provided
    eConstValWordId, //!< Offset holds value of <= 32 bit constant
    eConstValId, //!< Offset holds value of > 32 bit constant
    eOffsetId, //!< Offset holds offset
    eExprId //!< The value is represented by an expression
  };
  

  //! Union of all the different storage types for a net
  union StorageValue
  {
    SInt32 mOffsetToStorage;
    UInt32 mImmediateStorage;
    const DynBitVector* mLargeStorage;
  };

  //! General purpose boolean flags for storage nodes
  enum NodeFlags {
    eNoNodeFlags = 0, //!< No flags set
    eRunDebugSched = 0x1,  //!< Run debug schedule
    eDepositable = 0x2, //!< Depositable ring
    eInvalidWaveNet = 0x4, //!< Incorrect value if dumped
    eComboRun = 0x8, //!< Deposit/Force to net causes combo schedule
    eForceSubord = 0x10, //!< Force value or mask within a forcible net
    eClock = 0x20, //!< Clock ring
    eObservable = 0x40, //!< Observable ring
    eStateOutput = 0x80 //!< Flop or Latch output
  };

  //! Supported CarbonIdent derivations for db write/read
  enum CarbonIdentType {
    eSymTabIdent, //!< Typical CarbonIdent
    eSymTabIdentBP //!< SymTabIdent with 1 contained expression
  };

  //! Static fucntion to print the tag and type index
  static void printTagTypeIndex(UInt32 indent, UInt32 tagTypeIndex);

  //! Static function to extract type tag from a tagTypeIndex
  static int getTypeTag(UInt32 tagTypeIndex);

  //! Static function to enter a type tag into a tagTypeIndex
  static void enterTypeTag(UInt32* tagTypeIndex, int typeTag);

  //! Static function to extract type index from a tagTypeIndex
  static UInt32 getTypeIndex(UInt32 tagTypeIndex);

  //! Static function to enter a type index into a tagTypeIndex
  static void enterTypeIndex(UInt32* tagTypeIndex, UInt32 typeIndex);

  //! write the signature of this r/w interface to out
  static void writeSignature(ZostreamDB& out);

  //! read the signature of the r/w interface to in
  /*!
    Returns false if the signature is not compatible
  */
  static bool readSignature(ZistreamDB& in, UtString* errMsg);

  //! write data to stream
  static void write(ZostreamDB& out, UInt32 netFlags, UInt32 signatureIndex, 
                    UInt32 tagtypeIndex, const IODBIntrinsic *intrinsic,
                    const StorageValue& storageInfo, UInt32 exprExist, 
                    UInt32 exprFactoryIndex, NodeFlags nodeFlags, const UserType* ut);
  
  //! read data from stream
  /*!
    This doesn't check the status of the stream. The caller should as
    soon as this returns, assuming it does return.
  */
  static void read(ZistreamDB& in, UInt32* netFlags, UInt32* signatureIndex, 
                   UInt32* tagtypeIndex, IODBIntrinsic **intrinsic,
                   StorageValue* storageInfo,
                   UInt32* exprExist, UInt32* exprFactoryIndex, 
                   NodeFlags* nodeFlags, UserType** ut);

  //! Write the branch data to the I/O stream
  static void writeBranch(ZostreamDB& out, UInt32 hierFlags, StringAtom* atom,
                          const UserType* ut);

  //! Read the branch data from the I/O stream
  static void readBranch(ZistreamDB& in, UInt32* hierFlags, StringAtom** atom,
                         UserType** ut);

  //! Returns true if the typeCode represents a constant type
  /*!
    \param typeCode A TypeIndexTag. This is usually called after
    getTypeTag() which returns an int because of the way it is stored.
  */
  static bool isConstantType(int typeCode);
};
#endif
