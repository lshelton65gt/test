// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*! \file IODBDirective.h This header declares the IODBDirective, IODBDesignDirective and
 *  IODBModuleDirective classes.
 */

#ifndef _IODBDIRECTIVE_H_
#define _IODBDIRECTIVE_H_

#include "util/UtArray.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/SourceLocator.h"

class UtOstream;
class ZostreamDB;
class ZistreamDB;

class IODBDirective {
public: CARBONMEM_OVERRIDES

  IODBDirective () : mLoc (), mValue () {}

  IODBDirective (const SourceLocator& loc, const char* val) : 
    mLoc(loc), mValue (val) {}

  virtual ~IODBDirective () {}

  //! Design directives
  /*! \note Do not change the order of these declarations. Changing the order
   *  will break backwards compatibility when reading the GUI database.
   *
   *  \note If the directives are modified or deleted then tweak the
   *  dirToString function
   */

  enum DesignDirective {
    eFastClock, eObserve, eForce, eDeposit, eIgnoreOutput, eCollapseClocks,
    eInputDir, eCModuleBegin, eCPortBegin, eCTiming, eCFanin, eCPortEnd,
    eCModuleEnd, eClockSpeed, eExpose, eCFunctionBegin, eCFunctionEnd,
    eCTaskBegin, eCTaskEnd, eCArg, eTieNet, eCNullPortBegin, eCNullPortEnd,
    eCHasSideEffect, eCCallOnPlayback, eSubstituteModule, eSubstituteEntity,
    eOutputsTiming, eInputsTiming, eBlast, eVectorMatch, 
    eWrapperPort2State, eWrapperPort4State,
    eScDeposit, eScObserve, eDelayedClock, eDepositFrequent, 
    eDepositInfrequent, eSlowClock, eFastReset, eAsyncReset, eTraceSignal,
    eOnDemandIdleDeposit, eOnDemandExcluded, eMvDeposit, eMvObserve,
    eIgnoreSynthCheck
  };

  //! Module-specific directives
  /*! \note Do not change the order of these declarations. Changing the order
   *  will break backwards compatibility when reading the GUI database.
   *
   *  \note If the directives are modified or deleted then tweak the
   *  dirToString function
   */

  enum ModuleDirective {
    eHideModule,
    eTernaryGateOptimization,
    eFlattenModule,
    eFlattenModuleContents,
    eAllowFlattening,
    eDisallowFlattening,
    eNoAsyncReset,
    eDisableOutputSysTasks,
    eEnableOutputSysTasks
  };

  //! Enum for controlling how directives work.
  /*! The directives can be module based only (module.net), instance
   *  based only (top.A.B.net), or both. This enum defines what mode a
   *  directive uses.
   */
  enum DirectiveMode {
    eDirModule,         //!< process directive nets as module.net
    eDirInstance,       //!< process directive nets as instance names
    eDirBoth            //!< process directive nets as both module/instance
  };

  //! \return the name of a design directive
  static const char* dirToString(DesignDirective dir);

  //! \return the name of a module directive
  static const char* dirToString(ModuleDirective eType);

  //! \return the source location that induced the directive
  SourceLocator getLocation () const { return mLoc; }

  //! \return the optional value arguments
  const UtString &getValue () const { return mValue; }

  //! \class TokenVector
  /*! This is an irritating little class. What is really needed is a subset of
   *  the functionality of UtVector <UtStringArray>. However UtVector build on
   *  top of std::vector. However, STL cannot be used in the runtime! This
   *  builds on the STL-free UtArray.
   */
  class TokenVector: public UtArray <UtStringArray *> {
  public: CARBONMEM_OVERRIDES
    TokenVector () {}
    virtual ~TokenVector ();
    void push_back (const UtStringArray &);
  };

  //typedef UtArray <UtStringArray> TokenVector;

  typedef Loop <TokenVector> TokenVectorLoop;

  UInt32 numTokens () const { return mTokens.size (); }
  TokenVectorLoop loopTokens () { return TokenVectorLoop (mTokens); }
  UtStringArray firstToken () const { return *mTokens [0]; }

  void addToken (const UtStringArray &tok) 
  { mTokens.push_back (tok); }

  //! Write a directive to a database file
  virtual bool writeDB (SourceLocatorFactory &, ZostreamDB &) = 0;

  //! Read a directive from a database file
  virtual bool readDB (SourceLocatorFactory *tmpFactory, SourceLocatorFactory *factory, ZistreamDB &) = 0;

  //! Visibility for debugging
  virtual void print (UtOStream &) const;
  void pr () const;

protected:

  //! Write the source locator, token vector and value
  bool writeArgs (SourceLocatorFactory &, ZostreamDB &);

  //! Read the source locator, token vector and value
  bool readArgs (SourceLocatorFactory *, SourceLocatorFactory *, ZistreamDB &);

private:

  IODBDirective (const IODBDirective &); // forbid
  IODBDirective &operator = (const IODBDirective &); // forbid

  SourceLocator mLoc;                   //!< location of the directive
  TokenVector mTokens;                  //!< arguments to the directive
  UtString mValue;                      /*!< used to hold auxiliary parse
                                          tokens such as the frequency from
                                          clockSpeed */
};

class IODBDesignDirective : public IODBDirective {
public: CARBONMEM_OVERRIDES

  class List : public UtArray <IODBDesignDirective *> {
  public: CARBONMEM_OVERRIDES
    List () : UtArray <IODBDesignDirective *> () {}
    virtual ~List ();
  };

  IODBDesignDirective () : IODBDirective () {}

  IODBDesignDirective (DesignDirective dir, const SourceLocator& loc, const char* val) :
    IODBDirective (loc, val), mDirective (dir)
  {}

  virtual void print (UtOStream &) const;

  DesignDirective getDirective () const { return mDirective; }

  //! Write a directive to a database file
  virtual bool writeDB (SourceLocatorFactory &, ZostreamDB &);

  //! Read a directive from a database file
  virtual bool readDB (SourceLocatorFactory *tmpFactory, SourceLocatorFactory *factory, ZistreamDB &);

private:

  DesignDirective mDirective;

};

class IODBModuleDirective : public IODBDirective {
public: CARBONMEM_OVERRIDES

  class List : public UtArray <IODBModuleDirective *> {
  public: CARBONMEM_OVERRIDES
    List () : UtArray <IODBModuleDirective *> () {}
    virtual ~List ();
  };

  IODBModuleDirective () : IODBDirective () {}

  IODBModuleDirective (ModuleDirective dir, const SourceLocator& loc) :
    IODBDirective (loc, ""), mDirective (dir)
  {}

  virtual void print (UtOStream &) const;

  ModuleDirective getDirective () const { return mDirective; }

  //! Write a directive to a database file
  virtual bool writeDB (SourceLocatorFactory &, ZostreamDB &);

  //! Read a directive from a database file
  virtual bool readDB (SourceLocatorFactory *tmpFactory, SourceLocatorFactory *factory, ZistreamDB &);

private:

  ModuleDirective mDirective;

};


#endif
