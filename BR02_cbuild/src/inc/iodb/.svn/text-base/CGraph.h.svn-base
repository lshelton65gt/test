// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef CGRAPH_H
#define CGRAPH_H

#include "shell/carbon_dbapi.h"
#include "symtab/STSymbolTableNode.h"
#include "util/SourceLocator.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"

#define USE_CARBON_DB 0

class STSymbolTable;
class CGraphDummyFieldBom;

//! Elaborated flow-graph with classified nesting
/*!
 *! The FLNodeElab graph's 'nesting' implementation does not easily
 *! lend itself to drawing simple causality graphs.   It's hard to
 *! distinguish different sorts of fanin and nesting from one another.
 *!
 *! The CGraph class makes those distinctions more explicit, with
 *! a symbol-based subnode data model.  Also, some of the nesting
 *! levels are removed to make simpler graphs.  This is done primarily
 *! for the benefit of a GUI showing the graph.
 */
class CGraph {
public:
  CARBONMEM_OVERRIDES

  //! Source text is represented by an ordered sequence of
  //! pair<filename,vector[lines]>.  The vector<lines> always
  //! has an even number of line-numbers, as it has start1,end1,start2,end2...
  //! This is more convenient then using a vector<pair<int,int>> because 
  //! UtVector causes linking problems cause it pulls in exception handling,
  //! and UtArray can't (as of this writing) handle object semantics.
  typedef UtArray<UInt32> IntVec;
  typedef std::pair<const char*,IntVec> FileAndLines;
  typedef UtArray<FileAndLines*> FileAndLinesVec;

  class Node;
  class Cluster;
  typedef UtHashSet<Node*> NodeSet;
  typedef NodeSet::SortedLoop NodeSetLoop;
  typedef UtHashMap<StringAtom*,Node*> SymNodeMap;
  typedef UtHashMap<const STSymbolTableNode*,NodeSet> PathNodeSetMap;
  typedef UtArray<Node*> NodeVec;
  typedef std::pair<StringAtom*,NodeVec> SymNodesPair;
  typedef UtArray<SymNodesPair*> SymNodesVector;
  typedef UtHashMap<StringAtom*,UInt32> SymIndexMap;
  typedef UtHashSet<Cluster*> ClusterSet;
  typedef ClusterSet::SortedLoop ClusterSetLoop;
  typedef UtArray<Cluster*> ClusterVec;
  typedef UtHashSet<Cluster*,HashPointerValue<Cluster*> > ClusterFactory;

  //! Which type of Node is this?
  enum Type {eSequential, eIf, eCase, eComplexCase, eBlackBox, eUnbound,
             eInput, eOutput, eBidirect, eForce, eDeposit, eObserve};

  // The StringAtom* is a compact printable representation of the bits, e.g.
  //"[7:4,2]"
  typedef std::pair<const STSymbolTableNode*,StringAtom*> NetAndBits;

  //! hash the NetAndBits pair so that we can make sets of them and not
  //! enter in the same net/bits pair twice.  Note that this mechanism
  //! does not do anything intelligent overlapping sets of bits, but it
  //! doesn't need to because the population is driven by the def-net-ref
  //! on a flow-node, where all the net-bits written by a single UseDefNode
  //! are already aggregated together.
  struct HashNetAndBits {
    CARBONMEM_OVERRIDES

    //! hash operator -- requires class method hash()
    size_t hash(const NetAndBits& nb) const {
      return ((((size_t) nb.first) >> 2) ^
              ((size_t) nb.second));
    }

    //! equal function
    bool equal(const NetAndBits& nb1, const NetAndBits& nb2) const {
      return ((nb1.first == nb2.first) &&
              (nb1.second == nb2.second));
    }

    //! lessThan operator -- for sorted iterations
    bool lessThan(const NetAndBits& nb1, const NetAndBits& nb2) const {
      return HierName::compare(nb1.first, nb2.first) < 0;
    }
  };

  typedef UtHashSet<NetAndBits,HashNetAndBits> NetAndBitsSet;
  typedef NetAndBitsSet::SortedLoop NetAndBitsLoop;


  //! graph node
  class Node {
    //! ctor is private, use CGraph::addNode
    Node(Type type, 
         const STSymbolTableNode* outnet,
         StringAtom* netBits,
         const STSymbolTableNode* scope,
         StringAtom* desc); // UD-node scope may not match net
    Node() : mContinuousDriver(true), mIndex(-1) {}       // must call read afterwards

    //! dtor
    ~Node();

    //! write node to DB, excluding references
    void write(ZostreamDB&);

    //! write references to other nodes to DB
    void writeRefs(ZostreamDB&);

    //! read DB
    bool read(ZistreamDB&, SourceLocatorFactory* slc);

    //! read references to other nodes
    bool readRefs(ZistreamDB& db);

  public:
    CARBONMEM_OVERRIDES

    friend class CGraph;

    //! Add a fanin node.
    /*!
     *! This fanin is only for the expressions that determine whether
     *! or not the sub-nodes are live.
     */
    void addFanin(Node*);

    //! add a nested node, which is associated with a symbol
    /*!
     *! e.g. for if-statements, the "then" node is associated with "1",
     *! the "else" node with "0".
     *!
     *! for eCase, the associated node is the numeric value
     *! to compare against the select-statement.  This does not allow
     *! for casex pattern matching, or for non-constant labels.  eComplexCase
     *! allows that, in which case the string is the expression which must
     *! be evaluated, in sequence.
     *!
     *! For sequential nodes, there is only one subnode, and it's
     *! activated based on the edge-expression, which is something like
     *! "posedge clk and not posedge rst".  These may not be handled yet.
     */
    void addNode(StringAtom*, Node*);

    //! add a sequential fanin
    void addSeqFanin(Cluster*);

    //! add a sequential fanout
    void addSeqFanout(Cluster*);


    //! get a string describing the bits of the nets the node is driving
    const char* getNetBits() const;

    //! write a hierarchical name to the DB
    void writeHierarchy(ZostreamDB& db, const STSymbolTableNode* node);

    //! write/memo-ize an interned string
    void writeString(ZostreamDB& db, const char* str);

    //! loop over all the fanin nodes
    NodeSetLoop loopFanin() {return mFanin.loopSorted();}

    //! how many fanin nodes?
    UInt32 numFanin() const {return mFanin.size();}

    //! loop over all fanout nodes?
    NodeSetLoop loopFanout() {return mFanout.loopSorted();}

    //! how many fanout nodes?
    UInt32 numFanout() const {return mFanout.size();}

    //! how many sub-nodes
    UInt32 numSubNodes() const {return mNodes.size();}

    //! get the vector of Nodes associated with a particular subnode,
    //! identified by index
    const NodeVec& getSubNodes(UInt32 i) const {return mNodes[i]->second;}

    // non-constant version of above 
    NodeVec& getSubNodes_nc(UInt32 i) {return mNodes[i]->second;}


    //! loop over all the sequential fanin nodes
    ClusterSetLoop loopSeqFanin() {return mSeqFanin.loopSorted();}


    //! how many Seq fanin nodes?
    UInt32 numSeqFanin() const {return mSeqFanin.size();}
    
    
    //! loop over all Seq fanout nodes?
    ClusterSetLoop loopSeqFanout() {return mSeqFanout.loopSorted();}
    
    //! how many parents?
    UInt32 numParents() const {return mParents.size();}
    
    //! loop over all parent nodes?
    ClusterSetLoop loopParents() {return mParents.loopSorted();}
    
    
    //! how many Seq fanout nodes?
    UInt32 numSeqFanout() const {return mSeqFanout.size();}
    

    //! get the symbol associated with a particular subnode
    StringAtom* getSym(UInt32 i) const {return mNodes[i]->first;}
    
    //! does this net continuously drive the net?
    /*!
     *! This node is not a continuous driver if it's sub-node of
     *! a condition.  This differs a bit with the NUNet continuous
     *! driver set, and that's on purpose.  The reason is that we
     *! don't want to exposure the user to the three-levels of nesting
     *! required for an unconditional blocking assign.  On the other
     *! hand, the nested flow for the branches of an if-statement each
     *! are contributing to the drive of the same net, but neither of
     *! those accurately represents the net out of context.
     */
    bool isContinuousDriver() const {return mContinuousDriver;}

    //! get the node type as a string
    const char* getTypeStr() const;

    //! get the node type as an enum
    Type getType() const {return mType;}

    //! get the scope node
    const STSymbolTableNode* getScope() const {return mScope;}

    //! debug print routine
    void print(CGraph* cgraph, UInt32 indent, bool recurse);

    //! add this cluster to the list of clusters that "own" this node
    void addParent(Cluster*);

    //! compare this node with another one for sorting
    bool operator<(const Node& other) const {return compare(this, &other) < 0;}

    //! lexically compare this node with another one
    static int compare(const Node* c1, const Node* c2);

    //! give this node an index, primarily for debug printing
    void putIndex(SInt32 idx) { mIndex = idx; }
    SInt32 getIndex() const {return mIndex;}

    //! add a source-locator
    void addLocator(const SourceLocator& loc);

    //! add an output net
    void addOutNet(const STSymbolTableNode*, StringAtom* bits);

    NetAndBitsLoop loopNetAndBits() const {return mOutNets.loopSorted();}

  private:
    Type mType;
    FileAndLinesVec mSource;
    NetAndBitsSet mOutNets;
    const STSymbolTableNode* mScope;
    StringAtom* mDescription;
    NodeSet mFanin;
    NodeSet mFanout;
    ClusterSet mSeqFanin;
    ClusterSet mSeqFanout;
    ClusterSet mParents;

    SymNodesVector mNodes;
    SymIndexMap mSymIndexMap;
    bool mContinuousDriver;
    SInt32 mIndex;
  };

  //! cluster class
  class Cluster {
    //! ctor is private, use CGraph::addCluster
    Cluster(Node* fanin, Node* fanout);

    //! dtor
    ~Cluster() {}

  public:
    CARBONMEM_OVERRIDES

    friend class CGraph;

    typedef std::pair<UInt32,UInt32> IntPair;
    typedef UtHashMap<Node*,IntPair> DepthMap;

    //! Add a node to the cluster
    /*!
     *! Note that a Node may be a child of multiple clusters
     */
    void addChild(Node*);

    //! debug print routine
    void print(CGraph* cgraph, UInt32 indent, bool recurse);

    //! compare this cluster with another one for sorting
    bool operator<(const Cluster& other) const {return compare(this, &other) < 0;}

    static int compare(const Cluster* c1, const Cluster* c2);

    //! give this node an index, primarily for debug printing
    void putIndex(SInt32 idx) { mIndex = idx; }

    size_t hash() const {
      return (size_t) mFanin + (size_t) mFanout;
    }

    bool operator==(const Cluster& other) const {
      return (mFanin == other.mFanin) && (mFanout == other.mFanout);
    }

  private:
    void computeDepthFromFanin(Node* node, UInt32 depth);
    void computeDepthFromFanout(Node* node, UInt32 depth);
    void computeDepths();

    Node* mFanin;
    Node* mFanout;
    NodeSet mChildren;
    DepthMap mDepthMap;
    SInt32 mIndex;
  };

  //! ctor
  CGraph(SourceLocatorFactory* slc, AtomicCache* shared_cache);

  //! dtor
  ~CGraph();

  //! read the CGraph from a file, return false if failure, indicating error
  bool read(const char* filename, UtString* errmsg, CarbonDB* db);

  //! write the CGraph to a file, return false if failure, indicating error
  bool write(const char* filename, UtString* errmsg);

  //! look up a graph-node from a net name
  NodeSetLoop findDrivers(const STSymbolTableNode*);

  StringAtom* intern(const char*);
  StringAtom* intern(const UtString& s) {return intern(s.c_str());}

  Node* addNode(Type type, 
                const STSymbolTableNode* outnet,
                StringAtom* netBits,
                const STSymbolTableNode* scope, // UD-node scope may != net
                StringAtom* desc);

  void buildNetNodeMap();

  void buildSequentialFanin();  // build sequential fanin. call once cgraph is built.

  void computeClusterDepths();


  //! how many nodes in the graph?
  UInt32 numNodes() const {return mAllNodes.size();}

  //! get a node in the graph
  Node* getNode(UInt32 i) const {return mAllNodes[i];}

  //! dump ascii for the cgraph to stdout
  void dump();

  //! sort all the nodes & clusters so they are in a consistent order, and assign indexes
  void sort();

  //! get source code for node
  bool getNodeDescription(Node* node, UtString* buf) const;

private:
  SourceLocatorFactory* mSourceLocatorFactory;
  PathNodeSetMap mPathNodeSetMap;
  AtomicCache* mPrivateAtomicCache;
  AtomicCache* mSharedAtomicCache;
  NodeVec mAllNodes;
  ClusterVec mAllClusters;
  ClusterFactory mClusterFactory;

  //! private symbol table is used because some nodes are excluded
  //! from the symtab.db, and that makes it difficult to read the
  //! cgraph file without error
  STSymbolTable* mSymbolTable;
  CGraphDummyFieldBom* mCGraphDummyFieldBom;

  // helper functions for building sequential fanin
  void buildSequentialFaninStart(Node* currentNode, Node* currentSNode);
  void buildSequentialFaninRecurse(Node* currentNode, Node* seqDestNode,
                                   NodeSet *covered, NodeVec* path) ;
}; // class CGraph

#endif // CGRAPH_H
