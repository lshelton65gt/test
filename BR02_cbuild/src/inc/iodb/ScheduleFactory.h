// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _SCHEDULE_FACTORY_H_
#define _SCHEDULE_FACTORY_H_


#include "schedule/Event.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Signature.h"

#include "util/UtHashSet.h"

class ZostreamDB;
class ZistreamDB;
class STSymbolTable;
class UtOStream;
class IODB;

//! Set of schedule masks
typedef UtHashSet<const SCHScheduleMask*> SCHScheduleMaskSet;

//! Loop over schedule mask set
typedef CLoop<const SCHScheduleMaskSet> SCHScheduleMaskSetLoop;

//! SCHScheduleFactory class
/*!
 * Contains the universe of distinct ScheduleMask and ClockEdge structures,
 * which are shared, and which we only make one copy of for each unique
 * instance, allowing pointer comparison with == and reducing memory
 * consumption
 */
class SCHScheduleFactory
{
public: CARBONMEM_OVERRIDES
  //! Type definition for the set of unique ScheduleMask entries.
  typedef UtHashSet<SCHScheduleMask*,
                  HashPointerValue<const SCHScheduleMask*> > SCHMaskSet;
  typedef UtHashSet<SCHEvent*,
                  HashPointerValue<const SCHEvent*> > SCHEventSet;
  typedef UtHashSet<SCHSignature*,
                  HashPointerValue<const SCHSignature*> > SCHSignatureSet;

  //! code common to all constructors
  void commonConstructor();

  //! constructor
  SCHScheduleFactory();

  //! constructor  Use this if the related IODB is known at the time of construction.
  //! Compiletime schedule factory should have am initialized iodb to look at clock speeds
  //! when sortng masks.
  SCHScheduleFactory(IODB* iodb);

  //! destructor
  ~SCHScheduleFactory();

  //! Used for compile time node sorting where we need to know the symboltable
  //! to find the clock speeds.  -Dylan
  void putIODB(IODB* iodb) { mIODB = iodb; }

  //! Lookup or build a unique SCHEvent structure for a clock event
  /*! For a given clock and edge, there will only be one SCHEvent
   *  constructed per factory
   */
  const SCHEvent* buildClockEdge(const STSymbolTableNode* clock, ClockEdge edge,
                                 UInt32 priority = 0);

  //! build or lookup a shared ScheduleMask from a SCHEvent
  /*! This routine builds a ScheduleMaks from an event. The event can
   *  either be a clock transition or the special *input* and *output*
   *  events. This routine is usually called when creating the
   *  transition mask for a sequential block for the first time.
   *
   *  \param ev The event for which to create the ScheduleMask.
   *
   *  \returns a unique ScheduleMask representing the event parameter.
   */
  const SCHScheduleMask* buildMask(const SCHEvent* ev);

  //! Returns a unique SCHScheduleMask representing and design input
  /*! When the input SCHScheduleMask is placed on a node (as a
   *  transitionMask in a SCHSignature) we know that the node is
   *  sensetive to one of the design inputs.
   */
  const SCHScheduleMask* getInputMask() const {
    return mInputMask;
  }

  //! Returns a unique SCHScheduleMask representing a constant value.
  const SCHScheduleMask* getConstantMask() const {
    return mConstantMask;
  }

  //! Returns a unique SCHScheduleMask representing and design output
  /*! When the output SCHScheduleMask is placed on a node (as a
   *  sampleMask in a SCHSignature) we know that the node's value affects
   *  one of the design inputs.
   */
  const SCHScheduleMask* getOutputMask() const {
    return mOutputMask;
  }

  //! Given two masks, append them and find/build a shared
  //! SCHScheduleMask for them.
  /*! This routine is used as the design is traversed determining
   *  either the transition or sampling SCHScheduleMasks for a
   *  node. During the traversal, existing SCHScheduleMasks are combined
   *  to create new masks. The new mask is the OR of the two masks.
   *
   *  The SCHScheduleMasks passed in are not modified.
   *
   *  \param mask1 the first mask to combine. It must not be NULL.
   *
   *  \param mask2 the second mask to combine. It must not be NULL.
   *
   *  \returns a unique SCHScheduleMask representing the OR of the two
   *  input SCHScheduleMasks
   */
  const SCHScheduleMask* appendMasks(const SCHScheduleMask* mask1,
				     const SCHScheduleMask* mask2);

  //! Remove the input event from a schedule mask and return a new mask.
  /*! This routine is used to remove the input event for combinational
   *  schedules. The input schedule takes care of making blocks
   *  execute based on input changes.
   *
   *  \param mask The transition schedule mask which may have *input*
   *  in it.
   *
   *  \return A new mask without *input* in it or the same mask.
   */
  const SCHScheduleMask* removeInputMask(const SCHScheduleMask* mask);

  //! Remove the output event from a schedule mask and return a new mask.
  /*! This routine is used to remove the output event for combinational
   *  schedules.
   *
   *  \param mask The sample schedule mask which may have *output* in
   *  it.
   *
   *  \return A new mask without *output* in it or the same mask.
   */
  const SCHScheduleMask* removeOutputMask(const SCHScheduleMask* mask);

  //! Give a set of schedule masks, create a single mask from them
  /*! It is faster to build a schedule mask from a set than doing n-1
   *  appends.
   */
  const SCHScheduleMask* mergeScheduleMasks(const SCHScheduleMaskSet& masks);

  //! Build a signature from a transition schedule mask
  /*! Builds a unique signature from a transition mask. The sample
   *  mask is assumed to be empty.
   *
   *  \param transitionMask a unique SCHScheduleMask that describes when
   *  a node transitions. It must not be NULL.
   *
   *  \returns a unique SCHSignature made up of the transitionMask and a
   *  NULL sample mask.
   */
  const SCHSignature* buildSignature(const SCHScheduleMask* transitionMask);

  //! Build a signature from both a transition and sample schedule mask
  /*! Builds a unique signature from a transition mask and a sample mask.
   *
   *  \param transitionMask a unique SCHScheduleMask that describes when
   *  a node transitions.
   *
   *  \param sampleMask a unique SCHScheduleMask that describes when a
   *  node is sampled (read) or NULL.
   *
   *  \returns a unique SCHSignature made up of the transitionMask and the
   *  sample mask.
   */
  const SCHSignature* buildSignature(const SCHScheduleMask* transitionMask,
                                     const SCHScheduleMask* sampleMask);

  //! Given two signatures, merge them and find/build a shared
  //! Signature for them.
  /*! This routine is used for writing out the signature of
   *  a resettable-flop's Q output.
   */
  const SCHSignature* mergeSignatures(const SCHSignature*,const SCHSignature*);

  //! Set of signatures
  typedef UtHashSet<const SCHSignature*> SignatureSet;

  //! Build a mask by combining a set of signatures
  const SCHSignature* mergeSignatures(const SignatureSet& sigSet);

  //! Checks if a signature is sensitive to primary inputs
  /*! Checks if the transition mask has *input* in it. It does this by
   *  adding *input* to the mask and if the mask doesn't change it
   *  means it already exists.
   *
   *  \param signature the signature to test.
   *
   *  \returns true if the transition mask has *input*
   */
  bool hasInputMask(const SCHSignature* signature);

  //! Checks if a signature is sampled by primary outputs
  /*! Checks if the sample mask has *output* in it. It does this by
   *  adding *output* to the mask and if the mask doesn't change it
   *  means it already exists.
   *
   *  \param signature the signature to test.
   *
   *  \returns true if the sample mask has *output*
   */
  bool hasOutputMask(const SCHSignature* signature);

  //! Compares the transition and sample masks for a signature
  /*! This routine is used to determine if we should run with our
   *  transition or sample mask. The algorithm to determine this is
   *  complex but at the base is this function that determines which
   *  mask would cause us to execute less often.
   *
   *  \param signature The signature to test.
   *
   *  \returns -1 if transition mask is less frequent, 0 if they are
   *  equivalent or we can't tell, and 1 if the sample mask is less
   *  frequent.
   */
  int compareSignatureMasks(const SCHSignature* signature);

  struct CmpPrintOrder
  {
    size_t hash(const SCHScheduleMask* var) const {
      return ((size_t) var) >> 2;
    }
    
    //! lessThan operator -- for sorted iterations -- relies on class operator<
    bool lessThan(const SCHScheduleMask* v1, const SCHScheduleMask* v2) const;
    
    //! shallow less-than function -- for Microsoft hash tables
    bool lessThan1(const SCHScheduleMask* v1, const SCHScheduleMask* v2) const 
    {
      return lessThan(v1, v2);
    }
    
    //! equal operator -- for hashing on pointer value
    bool equal(const SCHScheduleMask* v1, const SCHScheduleMask* v2) const {
      return v1 == v2;
    }
    
  };
  
  typedef UtHashSet<const SCHScheduleMask*, CmpPrintOrder> PrintMaskSet;

  //! Print the schedule masks in their frequency order
  void printScheduleMaskOrder(UtOStream& out);

  //! write the schedule information to the database
  bool writeDatabase(ZostreamDB&);

  //! read the schedule information from the database
  bool readDatabase(ZistreamDB&);

  //! declare a clock
  //void addClock(STSymbolTableNode* clk) {mClocks.insert(clk);}

  //! loop structure for iterating over clocks, order not guaranteed
  //typedef UtHashSet<STSymbolTableNode*>::UnsortedLoop UnsortedClockLoop;

  //! iterate over clocks, order not guaranteed
  //UnsortedClockLoop loopClocks() {return mClocks->loopUnsorted();}

  //! iterator for all signatures
  typedef SCHSignatureSet::SortedLoop SignatureLoop;

  //! iterate over all signatures
  SignatureLoop loopSignatures();
  
  //! return the value of the highest clockSpeed in this mask.
  UInt32 fastestClock(const SCHScheduleMask* m) const;

  //! Return the number of signatures
  UInt32 numSignatures() const;

  //! Start building a mask
  void clearMaskBuilder();

  //! Add an event to a mask under construction
  /*!
   *! You must first use clearMaskBuilder
   */
  void addEvent(const SCHEvent* ev);

  //! construct a unique SCHScheduleMask from a SCHScheduleMask
  /*! This is intended for use while finding the shared SCHScheduleMask
   *  structure from mMaskEvents
   */
  const SCHScheduleMask* buildMask();

  //! Create a signature from another factory's signature 
  /*!
    \param srcSignature Signature from a different factory
    \param symTab Symboltable to lookup clock nodes. Safe lookups are
    done, so it is not assumed that the same atomic cache was used
    between symboltables between factories.
  */
  const SCHSignature* translateSignature(const SCHSignature* srcSignature, const STSymbolTable* symTab);

private:
//  //! Check if a schedule mask has another mask in it.
//  /*!
//   *  We don't want the act of checking the mask to create a new mask.
//   */
//   bool hasSubMask(const SCHScheduleMask* mask, const SCHScheduleMask* subMask);

  //! construct a unique SCHSignature from a SCHSignature
  /*! This is intended for use while finding the shared SCHSignature
   *  structure from a temporary one that is constructed incrementally
   *  while examining a node's fanin.
   */
  const SCHSignature* getSignature(const SCHSignature& signature);

  struct CmpMasksFreq
  {
    //! hash operator -- requires class method hash()
    size_t hash(const SCHScheduleMask* mask) const {
      return (size_t) mask;
    }

    //! equal function
    bool equal(const SCHScheduleMask* m1, const SCHScheduleMask* m2) const {
      return m1 == m2;
    }

    //! shallow less-than function -- for Microsoft hash tables
    bool lessThan1(const SCHScheduleMask* m1, const SCHScheduleMask* m2) const
    {
      return lessThan(m1, m2);
    }

    //! lessThan operator -- for sorted iterations
    bool lessThan(const SCHScheduleMask* m1, const SCHScheduleMask* m2) const;
  }; // struct CmpMasksFreq

  friend struct CmpMasksFreq;

  typedef UtHashSet<const SCHScheduleMask*, CmpMasksFreq> OrderedMasks;

  // Sort the masks by what we think is a frequency of execution
  void sortMasks();

  // used for db read verification
  bool expect(ZistreamDB& db, const char* token);

  // Used by translateSignature
  void addEventsFromMask(const SCHScheduleMask* srcMask, const STSymbolTable* symTab);
  
  // Objects that are maintained by this factory
  SCHMaskSet* mMasks;
  SCHEventSet* mEvents;
  SCHSignatureSet* mSignatures;

  // The following are essentially static copies for the input,
  // constant, and output events and masks for use in the scheduler.
  SCHEvent* mInputEvent;
  const SCHScheduleMask* mInputMask;
  SCHEvent* mOutputEvent;
  const SCHScheduleMask* mOutputMask;
  SCHEvent* mConstantEvent;
  const SCHScheduleMask* mConstantMask;

  // Scratch mask used for constructing temporary ones for lookup in
  // the mask factory
  SCHScheduleMask::EventPtrSet mMaskEvents;
  UInt32 mMaskNumClkNets;

  // The following are used to compare masks. We try to compare them
  // efficiently by doing a comparison of all types of masks up
  // front. That way the comparisons are cheap. But if a new mask gets
  // added to the factory, then we have to re-do the comparisons.
  bool mMaskComparisonsAccurate;

  // The IODB which contains the mask symbol nodes.  Needed by sortMasks
  // to lookup the clock frequencies.
  IODB* mIODB;
  

}; // class SCHScheduleFactory

#endif // _SCHEDULE_FACTORY_H_
