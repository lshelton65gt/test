// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _ASSISTANT_H_
#define _ASSISTANT_H_

class AssistantMainWindow;

//! Class-level access to Qt's 'assistant' program, which is linked
//! into the Carbon wizard application.
class Assistant {
public:
  //! ctor
  Assistant(const char* helpPath);

  //! dtor
  ~Assistant();

  //! raise/de-iconify the help window and bring it up with an HTML file
  void show(const char* htmlFilename);

private:
  AssistantMainWindow* mMainWindow;
};

#endif
