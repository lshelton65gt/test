// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Converts expressions involving vectors into word-at-a-time loops.
*/
#ifndef __STRIPPER_H__
#define __STRIPPER_H__

#include "nucleus/Nucleus.h"
#include "nucleus/NUExpr.h"	// For NUOp::OpT
#include "util/DynBitVector.h"

class CopyContext;
class ReductionFramework;

//! Annotations on special requirements for stripping
enum Notes {
  eNeedsTemporary =  0x001,     //!< A temp required to compute stripmined
  eUnalignedOperands=0x002,	//!< Bitvectors are not aligned
  eHasPartsel =      0x004,	//!< Not restricted to complete vectors 
  eNeedsForLoop =    0x008,     //!< Stripped statement uses a for-loop
  eChanged = 	       0x010,   //!< Something Changed
  eReduction =       0x020	//!< Unary reduction of a vector expression
};


/*!
 * Strip mine expressions to unroll bitvector operations or otherwise
 * simplify the code generation.
 */

class Stripper
{
public:
  // Construct the Stripper.
  Stripper (ArgProc* arg,
            AtomicCache* atomic,
            NUNetRefFactory* factory,            
            MsgContext* msgCtxt,
            NUScope* scope,
            NUStmt* stmt
            );

  ~Stripper ();

  //! Is this a strip-mineable statement?
  bool isStrippable ();

  //! Generate the statement(s) to unroll this.
  NUBlock* generate ();

  //! Must construct a temporary to avoid clobbering LHS too early.
  bool needsTemporary () const {
    return (mFlags & eNeedsTemporary) != 0;
  }

  //! A vector with the word size is okay if there are no NUPartsels.
  bool isWholeVectorExpr () const {
    return (mFlags & eHasPartsel) == 0;
  }

  //! The vector strips aren't consistently aligned
  bool isUnaligned () const {
    return (mFlags & eUnalignedOperands) != 0;
  }

  //! Does this strip need a prefix assignment before the loop
  bool needsPrefix () const {
    return getOffset () != 0;
  }

  //! Did strip-mining this change the statement in any way?
  bool isTransformed () const {
    return (mFlags & eChanged) != 0;
  }

  //! Does this strip need a suffix assignment?
  bool needsSuffix () const;

  //! Does stripping this require a for-loop?
  bool needsForLoop () const { return (mFlags & eNeedsForLoop) != 0; }

  //! How wide a vector are we stripping? 
  UInt32 getWidth (void) const;

  bool stripSize (UInt32 s) const;

  //! What's the common bit offset of the part-selects?
  SInt32 getOffset () const { return mOffset; }

  //! Get block
  NUBlock *getBlock () const { return mBlock; }

  //! Construct a temporary assignment and return the temp
  NUExpr* genTempAssign (NUExpr* e);
  //! Annotate this stripmine with information learned on the tree walk.
  void annotate (enum Notes aNote) const { mFlags |= aNote; }

  void countOperation () const {++mOpCount;}

  void countOperation (UInt32 delta) const { mOpCount += delta; }

  UInt32 OperationCount () const { return mOpCount; }

  //! Remember that we transformed an expression
  void setChanged () {mFlags |= eChanged;}

  //!Can only strip vector segments that are the same size
  bool checkWidth (UInt32 width) const;

  //! Does this represent a unary reduction of a stripminable expression?
  bool isReduction () const { return (mFlags & eReduction); }

  //! What is the reduction expression?
  const NUExpr *getReductionExpr () const;

  //! What is the operator for the reduction?
  NUOp::OpT getReductionOp () const;

  //! Is this assignment statement stripmineable?
  bool isStrippable (NUAssign*);

  //! Overloaded
  bool isStrippable (NUContAssign*);

  //! Record size of prefix strip
  void putPrefixLength (UInt32 len) const { mPrefixLength = len; }

  //! Size of prefix strip
  UInt32 getPrefixLength() const {return mPrefixLength; }

  //! Record size of suffix strip
  void putSuffixLength (UInt32 len) const { mSuffixLength = len; }

  //! Size of suffix strip
  UInt32 getSuffixLength () const { return mSuffixLength; }
  void addStripMineStopper (NUExpr* cond);
  bool isStripMineStopper (NUExpr* cond);

#if 0
  //! Helper class to manage any expression that might need to be
  //! created by factorIndexExpr
  class ExprManager {
  public:
    ExprManager(): mExpr(NULL) {}
    ~ExprManager() {
      if (mExpr != NULL) {
        delete mExpr;
      }
    }
    void putExpr(NUExpr* expr) {mExpr = expr;}
  private:
    NUExpr* mExpr;
  };

  /*!
   *! Note that the returned expression might need to have been created
   *! by factorIndexExpr, so the caller must pass in an ExprManager,
   *! which will free that expression when it goes out of scope
   */
#endif

  //! Does index expression reference a word aligned bit?  If so return it
  /*!
   *! caller must free returned expression if non-null
   */
  static NUExpr* factorIndexExpr (const NUExpr* indexExpr, UInt32* w);

  //! predicate version of factorIndexExpr
  static bool isFactorable(const NUExpr* indexExpr);

  //! Check that this new offset is compatible with existing offsets
  bool checkOffset (SInt32 offset) const;

  //! Do we have ANY offset set yet?
  bool anyOffset () const;


private:
  //! Dump the stripmine state for verbose printout
  void dump (UtOStream& out) const;

  //! reset internal state of Stripper after looking at a statement
  void reset ();

  //! Construct a for-loop for the stripped vector operation
  /*!
   * Strip-mine the vector assignment statement \a stmt, and insert the
   * equivalent statements into the block.
   */
  void generateForLoop (NUAssign *stmt, CopyContext& cc);

  //! Examine an expression tree to see if it's a strippable reduction
  bool checkReduction (const NUExpr*e);

  //! Does destination overlap with source expression values
  bool destOverlapsSrc () const;

  Stripper ();
  Stripper (Stripper&);
  Stripper & operator =(Stripper&);

  //! The original statement before stripmining
  NUStmt *mStmt;

  //! The containing scope for any new declarations
  NUScope* mScope;

  //! Argument processor context
  ArgProc* mArg;

  //! Atomic cache for generating names
  AtomicCache* mAtomic;

  //! Netref factory
  NUNetRefFactory* mFactory;

  //! Message context
  MsgContext* msgContext;

  //! keep track of common prefix for generated variables
  UtString mGenvarPrefix;

  //! Flags recording information discovered about this expression
  mutable int mFlags;
  
  //! Alignment of bitvectors
  mutable SInt32 mOffset;

  //! Width of bitvectors
  mutable UInt32 mWidth;

  //! Width of prefix strip
  mutable UInt32 mPrefixLength;

  //! Width of suffix strip
  mutable UInt32 mSuffixLength;

  //! the block that scopes the strip-mined statement
  NUBlock *mBlock;

  //! Count of number of operations (&,|,^, etc) in expression
  mutable UInt32 mOpCount;


  //! Allowable strip width.  We will stripmine vectors of this size or larger
  UInt32 mViableStripWidth;

  bool mDisable;
  bool mVerbose;

  // Reduction Framework.
  mutable ReductionFramework *mRed;

  //! Set of expressions that strip-mining should not recurse into
  /*!
   * These are typically the selector in a ?: expression
   */
  NUExprSet mStripMineStopperSet;
};

#endif
