// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef BACKEND_H_
#define BACKEND_H_

#ifndef NUCLEUS_H_
#include "nucleus/Nucleus.h"
#endif

class ArgProc;
class Stats;
class AtomicCache;
class ElabAllocAlias;
class MsgContext;
class Backend;
class IODBNucleus;

//! Post-scheduling, pre-emit analysis
class Backend
{
public:
  Backend(NUNetRefFactory *netref_factory,
	  MsgContext *msg_context,
	  STSymbolTable *symtab,
	  AtomicCache *str_cache,
	  IODBNucleus *iodb,
	  ArgProc *arg);

  ~Backend();

  //! set up the command-line options.  Call this only once even if
  //! there are multiple Backend objects created
  void setupOptions();

  //! Perform backend analysis on the design
  void analyzeDesign(NUDesign *design, bool phase_stats, Stats *stats);

private:
  //! Hide copy and assign constructors.
  Backend(const Backend&);
  Backend& operator=(const Backend&);

  NUNetRefFactory *mNetRefFactory;
  MsgContext *mMsgContext;
  STSymbolTable *mSymtab;
  AtomicCache *mStringCache;
  IODBNucleus *mIODB;
  ArgProc *mArg;
  ElabAllocAlias *mAllocAlias;
};


#endif
