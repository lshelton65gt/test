// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WFABSFILEREADER_H_
#define __WFABSFILEREADER_H_



#include "util/UtString.h"
#include "util/UtVector.h"
#include "waveform/WfVCD.h"
#include "waveform/WfAbsSignal.h"
#include "util/CarbonTypes.h"

class HdlId;
class STSymbolTable;
class STAliasedLeafNode;

typedef UtHashSet<WfAbsSignal*> WfAbsSignalSet;

/*!
  \file
  Declaration of the WfAbsFileReader class.
*/

//! Interface for managing waveform control and reporting
/*!
  This abstract interface is to be derived from and passed into the
  file reader's constructor. Messages by the file reader will be sent
  through this interface. This interface also gives access to the
  calling structure whenever the file reader wishes to
  give control back to the caller during parsing.
*/
class WfAbsControl
{
public:
  virtual ~WfAbsControl() {}

  virtual void initialValueWarning(const char* msg) const = 0;
  
  
  virtual bool timeSlice(WfAbsSignalSet* changedSigs, const UInt64* currentTime, const UInt64* nextTime) = 0;
};

//! WfAbsFileReader class
/*!
  Abstract base class.  This class specifies an interface for reading a file
  that contains waveform (or value change) data, without being tied to a
  specific file format.

  Below it is an example program using this interface.  It outlines the calling
  order of the object methods.

 \code
  WfAbsFileReader wf(argv[1]);

  if (!wf.openFile())
  {
    return -1;
  }

  wf.loadSignals();

  wf.getSimulatorVersion(v);
  wf.getSimulationDate(d);

  if (wf.findSignal(UtString("top.m1.net1")))
  {
    UtIO::cout() << "Found \top.m1.net\"." << UtIO::endl;
  }

  WfAbsSignal * s1 = wf.watchSignal(UtString("top.m1.net1"));
  WfAbsSignal * s2 = wf.watchSignal(UtString("top.t1.index"));

  wf.unloadSignals();
  wf.loadValues();

  WfAbsSignalVCIter *v = s1->createSignalVCIter();
  for (; !v->atMaxTime(); v->gotoNextTime())
  {
    UtString value;
    v->getValue(value);
    UtIO::cout() << v->getTime() << " " << foo << UtIO::endl;
  }
  v->gotoPrevTime();

  delete s1;
  delete s2;

  wf.unloadValues();
 \endcode

 \sa
 WfAbsSignal
 \sa
 WfAbsSignalVCIter
 */
class WfAbsFileReader
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  /*!
    \param filename The name of the file that contains the waveform data to be read.
    The file is not opened at this time.
   */
  WfAbsFileReader(const char* filename) : mFilename(filename) 
  {}
  
  //! Destructor.
  virtual ~WfAbsFileReader() 
  {}

  //! If true, Treat x's as a separate value
  virtual void putDo4State(bool do4State) = 0;

  //! Enum for describing function status
  enum Status
    {
      eOK, //!< Operation completed successfully
      eError, //!< Operation failed
      eWarning //!< Operation completed, but warnings were encountered
    };
  
  //! A helper struct for maintaining hierarchical relationships
  struct SigHier
  {
    SigHier() : mSignal(NULL), mLeaf(NULL) {}
    
    WfAbsSignal* mSignal;
    STAliasedLeafNode* mLeaf;
  };


  //! The name of the file being read.
  /*!
    \return The name of the file being read.
   */
  const UtString & getFileName(void) const { return mFilename; };

  //! The simulator that created the waveform data.
  /*!
    This returns a UtString containing the the simulator (and it's version) used
    to create this  waveform file.  The data contained within the UtString follows
    no set format.
    \todo Remove the parameter and make it return a UtString reference.
   */
  virtual void getSimulatorVersion(UtString &v) const = 0;

  //! The creation date of the waveform data.
  /*!
    This returns a UtString containing the creation date of this waveform file. The 
    data contained within the UtString follows no set format.
    \todo Remove the parameter and make it return a UtString reference.
   */
  virtual void getSimulationDate(UtString &d) const = 0;

  //! The time timescale unit of the waveform data.
  /*!
    Returns the WfVCD::TimescaleUnit of the simulation.  See the documentation
    of that type for legal units.  This routine should not be called before
    loadSignals is called.
    \sa wfVCD::TimescaleUnit, loadSignals
   */
  virtual WfVCD::TimescaleUnit getTimescaleUnit(void) const = 0;

  //! The time timescale value of the waveform data.
  /*!
    Returns the WfVCD::TimescaleValue of the simulation.  See the documentation
    of that type for legal values.  This routine should not be called before
    loadSignals is called.
    \sa wfVCD::TimescaleValue, loadSignals
   */
  virtual WfVCD::TimescaleValue getTimescaleValue(void) const = 0;
  /*
  virtual void getScopeSeparator(UtString &s) = 0;
  */

  //! Load non-value change information about all signals in the waveform file.
  /*!
    \deprecated This method does nothing.

    Read all non-value change information about all of the signals in the
    waveform file. The file is opened at this point. Any error
    messages associated with opening the file or parsing the signals
    is in the errMsg string. Returns eOK if successful.

    \note The amount of time taken to execute this method and the amount of
    memory it consumes will be proportional to the number of signals contained
    in the waveform file.
    \sa unloadSignals
   */
  virtual Status loadSignals(UtString* errMsg) = 0;

  //! Unload non-value change information about signals.
  /*!
    This method will purge most of the signal information.  watchSignal or
    findSignal can no longer be called after this method completes.
    \sa loadSignals
    \sa watchSignal
    \sa findSignal
   */
  virtual void unloadSignals(void) = 0;

  //! Determine if the named signal exists in the eaveform file.
  /*!
    Find the named signal and allocate a new signal.  This method must
    be called after loadSignals and before unloadSignals.  This method
    will not find any signal that has not already been watched using
    watchSignal.
    \note The user is responsible for calling delete when they are done with
    the WfAbsSignal that is returned.
    \param signal The name of the signal to be found.
    \param msg If not NULL and problems were discovered with the
    signal that was requested, the reason why will be put here. This
    is an additive operation. The msg that is passed in is not
    cleared. If it is not empty a newline will be added before the new
    message.
    \retval 0 The signal could not be found.
    \retval !0 A pointer to an allocated signal. 
    \sa loadSignals
    \sa unloadSignals
    \sa watchSignal
   */
  virtual WfAbsSignal * findSignal(const UtString &signal, UtString* msg) const = 0;

  //! Watch value changes of the named signal
  /*!
    This indicates that the user is interested in observing the value changes
    for this signal.  If the user does not watch a signal, its value change
    data will not be loaded.  This method must be called after loadSignals
    and can not be called after unloadSignals.
    \note The user is responsible for calling delete when they are done with
    the WfAbsSignal that is returned.
    \param signal The name of the signal to be watched.
    \retval 0 The signal could not be found.
    \retval !0 A pointer to an allocated signal. 
    \sa loadValues
    \sa loadSignals
    \sa unloadSignals
    \sa findSignal
   */
  virtual WfAbsSignal * watchSignal(const UtString &signal) = 0;

  //! Returns true if the signal exists in the waveform
  virtual bool signalExists(const UtString& signal) const = 0;


  //! Add a signal to a watch list if not already present
  /*!
    In some cases, a WfAbsSignal* is not needed and we simply need to
    add a signal to the watch list. 
    \returns false if the signal is not in the source waveform, true
    otherwise.
  */
  virtual bool maybeAddWatchSignal(const UtString &signal_name) = 0;

  //! Load value change information for watched signals.
  /*!
    Read value change information for signals registered with
    watchSignal.  This method can only be called once.
    \param msg Must be non-null. If an error or warning occurs the
    reason is put here. The string is cleared first.
    \note The amout of time taken to execute this method is proportional to
    the amount of value change data present in the waveform file.  The amount
    of memory consumed is proportional to the number of signals being watched.
    \sa watchSignal
   */
  virtual Status loadValues(UtString* msg) = 0;

  //! Unload value change information for watched signals.
  /*!
    Unloads value change information for watched signals. Values
    cannot be loaded again.
    \sa loadValues
   */
  virtual void unloadValues(void) = 0;

  //! Retrieve the internal symbol table
  /*!
    The internal symbol table has all the nets of the design found in
    the waveform file.
  */
  virtual STSymbolTable* getSymbolTable() const = 0;

  //! Get the signal info from a leaf node on the symboltable
  virtual void fillHdlId(HdlId* info, const STAliasedLeafNode* net) const = 0;

  //! Watch all signals and put all signals in a vector
  virtual void listAndWatchAll(UtVector<SigHier>* sigVec) = 0;  

  //! Allocate a value change iterator for the signal.
  /*!
    This method allocates and returns to the user a value change
    iterator that is used to traverse and inspect the value change data
    associated with thw signal.  The user can create as many of these
    per signal as desired.  The user is responsible for deleting the
    returned object.
    \retval 0 Something went wrong.
    \retval !0 A newly allocated value change iterator.
   */
  virtual WfAbsSignalVCIter* createSignalVCIter(WfAbsSignal* sig, UtString* msg) = 0;  

  //! Get the maximum simulation time in the wave file
  virtual UInt64 getMaxSimulationTime() const = 0;

  //! Get the total number of signals
  virtual UInt32 getNumSignals() const = 0;

  //! Flush past values
  virtual void flushValues() = 0;

 protected:
  UtString mFilename;

 private:
  /* Forbidden */
  WfAbsFileReader(void);
  WfAbsFileReader(const WfAbsFileReader&);
  WfAbsFileReader& operator=(const WfAbsFileReader&);
};


#endif
