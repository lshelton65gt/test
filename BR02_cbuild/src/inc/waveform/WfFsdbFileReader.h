// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*
  Authors:  Mark Seneski
*/

#ifndef _WF_FSDB_FILE_READER_H_
#define _WF_FSDB_FILE_READER_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#ifndef __UtMap_h_
#include "util/UtMap.h"
#endif

#include "waveform/WfVCDBase.h"

class WfFsdbFileReader : public WfVCDBase
{
 public: 
  CARBONMEM_OVERRIDES

  WfFsdbFileReader(const char* filename, AtomicCache* strCache, DynBitVectorFactory* dynFactory, UtUInt64Factory* timeFactory, WfAbsControl* control);

  virtual ~WfFsdbFileReader();

  virtual Status loadSignals(UtString* errMsg);

  virtual void unloadSignals(void);

  virtual Status loadValues(UtString* msg);

  virtual void unloadValues(void);

  virtual UInt32 getNumSignals() const;

protected:  
  //bool isWatchedSignal(Signal* sig) const;
  virtual void insertWatchedId(IWaveIdCode* absId);
  virtual void gatherWatchedIds(WaveIdSet* allWatched);

private:
  class TrvsHdlId;
  class Buffer;
  class ReadObject;
  friend class ReadObject;
  ReadObject* mFsdbObj;
  mutable UtString* mErrMsg;
  mutable Status mStat;
  bool openFile(UtString* reason);
  void closeFile(void);
  void doErrMsg(const char* msg, Status stat, const char* postMsg) const;

  void doVariable(WfVCD::Identifier& varId, WfVCD::SignalType sigType, UInt32 size, int idcode, bool isVerilog);

  class FsdbIdHash;
  friend class FsdbIdHash;
  
  FsdbIdHash* mKnownIds;
  FsdbIdHash* mWatchedSignals;

  typedef UtMap<UInt64, TrvsHdlId*> TimeTrvsHdlMap;
  inline void insertTime(TrvsHdlId* hdlId, TimeTrvsHdlMap* timeTrvsHdls);
  inline void addValueChanges(TrvsHdlId* lowestId);
  void processWatchedSignals(TimeTrvsHdlMap& timeTrvsHdls);

  typedef UtArray<TrvsHdlId*> TrvsHdlVec;
  inline void loadWatchedSignals(TimeTrvsHdlMap* timeTrvsHdls, TrvsHdlVec* allTrvsHdls);
  inline void unloadWatchedSignals(TrvsHdlVec* allTrvsHdls);

  /* Forbidden */
  WfFsdbFileReader(void);
  WfFsdbFileReader(const WfFsdbFileReader&);
  WfFsdbFileReader& operator=(const WfFsdbFileReader&);
};

//! Create a WfFsdbFileReader. 
/*!
  This is defined separately in order to separate VCD from FSDB. This
  allows cwavetestbenchgen to only include the library if fsdb reading
  is allowed.
*/
WfFsdbFileReader* WfFsdbFileReaderCreate(const char* fileName,
                                         AtomicCache* strCache,
                                         DynBitVectorFactory* factory,
                                         UtUInt64Factory* timeFactory,
                                         WfAbsControl* control = NULL);

#endif
