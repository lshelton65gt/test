// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __WFVCDBASE_H_
#define __WFVCDBASE_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#include "waveform/WfAbsFileReader.h"
#include "symtab/STFieldBOM.h"

#include "hdl/HdlVerilogPath.h"
#include "waveform/WfVCD.h"

#include "util/UtArray.h"
#include "util/UtString.h"
#include "waveform/WfVCDTimeValue.h"

class AtomicCache;
class HdlId;
class STSymbolTableNode;
class STSymbolTable;
class DynBitVectorFactory;
class UtUInt64Factory;

class WfVCDBase : public WfAbsFileReader, public virtual STFieldBOM
{
  friend class WfVCDSignal;
  friend class WfVCDSignalVCIter;
  
  class WfLeafBOM;
  friend class WfLeafBOM;

  class WfBranchBOM;
  friend class WfBranchBOM;

#define CARBON_SORT_BLASTED_NETS 0
#if CARBON_SORT_BLASTED_NETS
  class BlastSort;
#endif

  struct CreateValue;
  //  struct IterGotoTime;
  //  struct CreateValIter;
  //friend struct CreateValIter;

  class Scope;

public: 
  CARBONMEM_OVERRIDES

  WfVCDBase(const char* filename, AtomicCache* strCache, DynBitVectorFactory* factory, UtUInt64Factory* uint64Factory, WfAbsControl* control);
  
  //! virtual destructor 
  virtual ~WfVCDBase();

  //! If true, x's are treated as 0's
  void putDo4State(bool do4State);

  class ISignal;
  class Signal;
  friend class Signal;
  class BitBlastSignal;
  class WaveGlyph;
  class FsdbId;
  
  class IWaveIdCode
  {
    typedef UtArray<WfVCDSignal*> SigVec;

  public: 
    CARBONMEM_OVERRIDES

    IWaveIdCode(UInt32 sigWidth) : mSigWidth(sigWidth) {}

    virtual ~IWaveIdCode();

    virtual const WaveGlyph* castGlyph() const;
    
    WaveGlyph* castGlyph();

    virtual const FsdbId* castFsdbId() const;
    FsdbId* castFsdbId();

    const WfVCDTimeValueList* getValueList() const;
    WfVCDTimeValueList* getValueList();
    
    UInt32 getSignalWidth() const { return mSigWidth; }

    void clearAffected();
    void addAffected(WfVCDSignal* watchedSig);

    void insertAffected(WfAbsSignalSet* sigSet) const;
  private:
    UInt32 mSigWidth;

  protected:
    WfVCDTimeValueList mValueList;
    SigVec mAffectedSigs;
  };
  
  class WaveGlyph : public IWaveIdCode
  {
  public: CARBONMEM_OVERRIDES
    WaveGlyph(const char* glyph, UInt32 sigWidth);

    virtual ~WaveGlyph();
    
    virtual const WaveGlyph* castGlyph() const;
    
    const char* symbol() const;

  private:
    UtString mGlyph;

    WaveGlyph();
  };

  class FsdbId : public IWaveIdCode
  {
  public: CARBONMEM_OVERRIDES
    FsdbId(int idCode, UInt32 sigWidth);

    virtual ~FsdbId();
    
    virtual const FsdbId* castFsdbId() const;
  
    //! This returns true if this is not an FsdbVhdlId
    virtual bool isVerilog() const;

    int idCode() const;

  private:
    int mIdCode;

    FsdbId();
  };

  
  class FsdbVhdlId : public FsdbId
  {
  public: CARBONMEM_OVERRIDES
    FsdbVhdlId(int idCode, UInt32 sigWidth);

    virtual ~FsdbVhdlId();

    //! This returns false
    virtual bool isVerilog() const;

  private:
    FsdbVhdlId();
  };
  
  class ISignal
  {
    friend class WfVCDBase;
  public: 
    CARBONMEM_OVERRIDES

    virtual ~ISignal();

    virtual UInt32 getWidth(void) const = 0;
    virtual SInt32 getLsb(void) const = 0;
    virtual SInt32 getMsb(void) const = 0;
    virtual const STSymbolTableNode * getName(void) const = 0;
    virtual WfVCD::SignalType getSignalType(void) const = 0;
    virtual HdlId::Type getDimensionType(void) const = 0;
    virtual void calcValue(const UInt64* curTime, DynBitVectorFactory* dynFactory) = 0;

    virtual const Signal* castSignal() const;
    virtual const BitBlastSignal* castBitBlast() const;

    Signal* castSignal();

    BitBlastSignal* castBitBlast();

  };

  class Signal : public ISignal
  {
    friend class WfVCDBase;
    friend class BitBlastSignal;
  public: 
    CARBONMEM_OVERRIDES

    Signal(const STSymbolTableNode *name, IWaveIdCode* idcode, WfVCD::SignalType signal_type, UInt32 width, const HdlId* info);
    
    virtual ~Signal();
    
    UInt32 getWidth(void) const;
    SInt32 getLsb(void) const;
    SInt32 getMsb(void) const;
    const STSymbolTableNode * getName(void) const;
    const IWaveIdCode* getIdCode() const;
    IWaveIdCode* getIdCode();
    WfVCD::SignalType getSignalType(void) const;
    HdlId::Type getDimensionType(void) const;
    virtual void calcValue(const UInt64* curTime, DynBitVectorFactory* dynFactory);

    virtual const Signal* castSignal() const;

    const HdlId* getHdlId() const { return mHdlInfo; }
        
    size_t hash() const;
    bool operator==(const Signal& other) const;
    bool operator<(const Signal& other) const;

  private:
    const HdlId* mHdlInfo;
    UInt32 mWidth;
    const STSymbolTableNode *mName;
    IWaveIdCode* mIdCode;
    WfVCD::SignalType mSignalType;

  private:
    Signal(void);
    Signal(const Signal&);
    Signal& operator=(const Signal&);
  };

  class BitBlastSignal : public ISignal
  {
    friend class WfVCDBase;
  public: 
    CARBONMEM_OVERRIDES

    BitBlastSignal();
    virtual ~BitBlastSignal();
    
    void addSignal(Signal* sigBit); 
    
    virtual UInt32 getWidth(void) const;
    virtual SInt32 getLsb(void) const;
    virtual SInt32 getMsb(void) const;
    virtual const STSymbolTableNode * getName(void) const;
    virtual WfVCD::SignalType getSignalType(void) const;
    virtual HdlId::Type getDimensionType(void) const;
    virtual void calcValue(const UInt64* curTime, DynBitVectorFactory* dynFactory);
    
    virtual const BitBlastSignal* castBitBlast() const;
    
    bool isWatched() const;
    void putIsWatched(bool isWatched);
    
    typedef UtArray<Signal*> SigList;
    SigList mBlastList;
    
    const WfVCDTimeValueList* getValList() const;
    WfVCDTimeValueList* getValList();
    
  private:
    UInt32 mWidth;
    WfVCDTimeValueList mValueList;
    bool mIsWatched;
  };


  //! Allocate an opaque WfBOM
  virtual Data allocBranchData();
  //! Allocate an opaque WfBOM
  virtual Data allocLeafData();

  //! Free the opaque WfBOM
  virtual void freeBranchData(const STBranchNode*, Data* bomdata);
  //! Free the opaque WfBOM
  virtual void freeLeafData(const STAliasedLeafNode*, Data* bomdata);

  //! Print the contents of the bomdata
  virtual void printLeaf(const STAliasedLeafNode* leaf) const;

  //! Print the contents of the bomdata
  virtual void printBranch(const STBranchNode* branch) const;

  //! does nothing
  virtual void preFieldWrite(ZostreamDB&);
  //! Returns eReadOK
  virtual ReadStatus preFieldRead(ZistreamDB&);

  //! Currently not allowed for this class
  virtual void writeLeafData(const STAliasedLeafNode*, ZostreamDB&) const;
  //! Currently not allowed for this class
  virtual void writeBranchData(const STBranchNode*, ZostreamDB&,
                               AtomicCache*) const;
  //! Currently not allowed for this class
  virtual ReadStatus readLeafData(STAliasedLeafNode*, ZistreamDB&, 
                                  MsgContext*);
  //! Currently not allowed for this class
  virtual ReadStatus readBranchData(STBranchNode*, ZistreamDB&, 
                                    MsgContext*);

  virtual void listAndWatchAll(UtVector<SigHier>* sigVec);

  //! Set signal bits with no value to given value
  /*!
    All bits of unlisted values will be set to the given value
    (0|1|x|z). Defaults to x. If an invalid value is passed in 'x' is
    assumed.

    This can only be called before loadValues().
  */
  void putUnlistedValueSetting(char value);

  //! Currently not allowed for this class
  virtual void writeBOMSignature(ZostreamDB&) const {
  }


  //! Return BOM class name
  virtual const char* getClassName() const
  {
    return "UnknownBOM";
  }

  //! Write the BOMData for a branch node
  virtual void xmlWriteBranchData(const STBranchNode* /*branch*/,UtXmlWriter* /*writer*/) const
  {
  }

  //! Write the BOMData for a leaf node
  virtual void xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/,UtXmlWriter* /*writer*/) const
  {
  }


  
  virtual ReadStatus readBOMSignature(ZistreamDB&, UtString*) {
    return eReadIncompatible;
  }

  void getSimulatorVersion(UtString &v) const;
  void getSimulationDate(UtString &d) const;
  WfVCD::TimescaleUnit getTimescaleUnit(void) const;
  WfVCD::TimescaleValue getTimescaleValue(void) const;

  WfAbsSignal * findSignal(const UtString &signal, UtString* msg) const;
  WfAbsSignal * watchSignal(const UtString &signal);

  bool signalExists(const UtString& signal) const;

  //! WfAbsFileReader::getMaxSimulationTime
  virtual UInt64 getMaxSimulationTime() const;

  //! WfAbsFileReader::flushValues
  virtual void flushValues();

  virtual WfAbsSignalVCIter* createSignalVCIter(WfAbsSignal* sig, UtString* msg);

  STSymbolTable* getSymbolTable() const {
    return mSymbolTable;
  }


  void fillHdlId(HdlId* info, const STAliasedLeafNode* net) const;
  bool maybeAddWatchSignal(const UtString &signal_name);

  bool advanceSimulationTime(UInt64 newTime);
  void doDate(const char *d);
  void doVersion(const char *v);
  void doComment(const char *c);
  void doPushScope(const WfVCD::Identifier &scope_ident, WfVCD::ScopeType scope_type);
  bool doPopScope(void);
  void clearScopes();
  void endLoadSignals();
  void endLoadValues();

  static WfLeafBOM* castLeafBOM(const Data bomdata);
  static WfBranchBOM* castBranchBOM(const Data bomdata);

  //! Evaluates the timeValue and sets the object's time value
  WfVCD::TimescaleValue evaluateTimescaleValue(SInt32 timeValue);
  //! Evaluates the unit and sets the object's timescale. 
  WfVCD::TimescaleUnit evaluateTimescaleUnit(char* unit);

private:
  UtString mDate;
  UtString mVersion;
  WfVCD::TimescaleUnit mTimescaleUnit;
  WfVCD::TimescaleValue mTimescaleValue;
  AtomicCache* mStringCache;
  HdlVerilogPath mVerilogPath;
  STSymbolTable *mSymbolTable;
  UtArray<STSymbolTableNode *> mScopeStack;

  typedef UtArray<BitBlastSignal*> BlastedSigVec;
  BlastedSigVec mBlastedSigs;

  UtUInt64Factory* mTimePtrs;
  DynBitVectorFactory* mDynFactory;

  typedef UtArray<IWaveIdCode*> WaveIdVec;
  WaveIdVec mChangedWaveIds;

  class DefaultWfControl;
  DefaultWfControl* mDefaultControl;
  WfAbsControl* mUserControl;

  char mUnlistedVal;
  bool mDo4State;
  bool mFirstValueChanges;

protected:
  bool mValsRead;

  //! The current time. May be reset during wave processing
  UInt64 mSimulationTime;
  //! The maximum time found in the vcd file.
  UInt64 mMaxSimTime;

  typedef UtHashSet<IWaveIdCode*> WaveIdSet;

  //virtual bool isWatchedSignal(Signal* sig) const = 0;
  virtual void insertWatchedId(IWaveIdCode* absId) = 0;
  virtual void gatherWatchedIds(WaveIdSet* allWatched) = 0;

  //  void collectBlastedValues(UtString* msg);
  Scope* createScope(const WfVCD::Identifier &ident, WfVCD::ScopeType scope_type);
  STSymbolTableNode * createSymbolTableNode(const WfVCD::Identifier &ident, bool is_leaf);

  void addValueChange(IWaveIdCode* id, const char* value);
  void addValueChangeReal(IWaveIdCode* id, CarbonReal realVal);
  void addValueChangeInt(IWaveIdCode* id, UInt32 intVal);

  void setSignalOnNode(STSymbolTableNode *node, IWaveIdCode* glyphObj, const WfVCD::Identifier& ident, WfVCD::SignalType signal_type, UInt32 size);

private:

  void createSymbolTable(void);
  void destroySymbolTable(void);
  WfVCDTimeValueList* getValueListById(Signal* sig) const;

  STSymbolTableNode * lookupSignal(const UtString &signal) const;
  const WfVCDBase::WfLeafBOM* getLeafBOM(const STSymbolTableNode *node) const;
  WfLeafBOM* getLeafBOM(STSymbolTableNode *node)
  {
    const WfVCDBase* me = const_cast<const WfVCDBase*>(this);
    return const_cast<WfLeafBOM*>(me->getLeafBOM(node));
  }
  

  inline void setScopeOnNode(STSymbolTableNode *node, Scope *scope);
  inline ISignal * getSignalFromNode(const STSymbolTableNode *node) const;

  //void setValList(const char *glyph, WfVCDTimeValueList* l);

  WfVCDTimeValueList* getValueList(ISignal* signal);
  //ISignal* getISignal(const UtString& signal_name) const;

  WfVCDTimeValue* getCurrentTimeValuePair(WfVCDTimeValueList* valList);
  void generateSigValue(const char* value, 
                        UInt32 sigWidth,
                        const DynBitVector** val);

  void insertWatchedSignal(Signal* sig, WfVCDSignal* affectedSignal);

  void manualInitializeWaveId(IWaveIdCode* idCode, const UInt64* timePtr, WfAbsSignalSet* affectedSigs);
  bool reportValueChanges(const UInt64* nextTime);

  void flushSignal(Signal* sig);

  class SignalFactory;
  friend class SignalFactory;
  SignalFactory* mSignalFactory;


  bool mRepeatedHierarchy;

  // forbid
  WfVCDBase();
  WfVCDBase(const WfVCDBase&);
  WfVCDBase& operator=(const WfVCDBase&);
};  

#endif
