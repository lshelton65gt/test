// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WFVCDFILEREADER_H_
#define __WFVCDFILEREADER_H_



#include "waveform/WfVCDBase.h"

class AtomicCache;
class WfVCDFileParse;

class WfVCDFileReader : public WfVCDBase
{
public: CARBONMEM_OVERRIDES
  WfVCDFileReader(const char* filename, AtomicCache* strCache, DynBitVectorFactory* factory, UtUInt64Factory* timeFactory, WfAbsControl* control = NULL);
  
  //! virtual destructor 
  virtual ~WfVCDFileReader();

  /*
  ** WfAbsFileReaderInterface
  */

  //! Load the signals
  /*!
    The file is opened at this point. If the file fails to open this
    returns false and the errMsg is set with the reason.
    Otherwise, any parsing errors during hierarchy processing are fatal.
  */
  virtual Status loadSignals(UtString* errMsg);
  virtual void unloadSignals(void);

  //! Load the values
  /*!
    Value parsing is non-fatal if an error occurs. However, parsing
    stops at the first error.
  */
  virtual Status loadValues(UtString* msg);
  virtual void unloadValues(void);
  
  virtual UInt32 getNumSignals() const;

  /*
  ** WfAbsVCDParserActions Interface
  */
  void doVariable(WfVCD::Identifier &var_ident, WfVCD::SignalType var_type, UInt32 size, const char *glyph);
  bool doTime(UInt64 sim_time);
  void doValue(const char *glyph, const char *value);
  void doValueReal(const char *glyph, CarbonReal value);
  void doEvent(const char* glyph);
  
  void incrLineNo();

  //! Report error and die
  void fatalParseError(const char* msg);

  int getLineNo() const;

  //! Skip value setting
  /*!
    This is only used to skip dumpall commands in the vcd file
  */
  void putSkipValues(bool skip);

protected:
  //virtual bool isWatchedSignal(Signal* sig) const;
  virtual void insertWatchedId(IWaveIdCode* absId);
  virtual void gatherWatchedIds(WaveIdSet* allWatched);

private:
  WfVCDFileParse* mParser;

  class GlyphNodeTable;
  friend class GlyphNodeTable;
  GlyphNodeTable* mWatchedSignals; 
  GlyphNodeTable* mKnownSignals; 
  bool mSkipValues;

 private:  
  void createParser();
  void destroyParser();
  WfVCDTimeValueList* getValList(const char *glyph) const;
  
  /* Forbidden */
  WfVCDFileReader();
  WfVCDFileReader(const WfVCDFileReader&);
  WfVCDFileReader& operator=(const WfVCDFileReader&);
};


#endif
