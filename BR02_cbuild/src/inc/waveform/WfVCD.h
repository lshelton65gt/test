// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#ifndef __WFVCD_H_
#define __WFVCD_H_




#include "util/UtString.h"
#include "util/CarbonTypes.h"
#include "hdl/HdlId.h"

namespace WfVCD
{
  enum ScopeType
  {
    eWfVCDScopeTypeModule,    //!< Module scope
    eWfVCDScopeTypeTask,      //!< Task scope
    eWfVCDScopeTypeFunction,  //!< Function scope
    eWfVCDScopeTypeBegin,     //!< Begin scope
    eWfVCDScopeTypeFork,      //!< Fork scope
    eWfVhdlArchitecture,      //!< VHDL Architecture
    eWfVhdlProcedure,         //!< VHDL Procedure
    eWfVhdlFunction,          //!< VHDL Function
    eWfVhdlRecord,            //!< VHDL Record
    eWfVhdlProcess,           //!< VHDL Process
    eWfVhdlBlock,             //!< VHDL Block
    eWfVhdlForGenerate,       //!< VHDL For Generate
    eWfVhdlIfGenerate,        //!< VHDL If Generate
    eWfVCDScopeTypeINVALID
  };

  enum SignalType
  { 
    eWfVCDSignalTypeEvent,       //!< event type
    eWfVCDSignalTypeInteger,     //!< integer type
    eWfVCDSignalTypeParameter,   //!< parameter type
    eWfVCDSignalTypeReal,        //!< real number type
    eWfVCDSignalTypeReg,         //!< register type
    eWfVCDSignalTypeSupply0,     //!< Power supply LOW
    eWfVCDSignalTypeSupply1,     //!< Power supply HIGH
    eWfVCDSignalTypeTime,        //!< Time type
    eWfVCDSignalTypeTri,         //!< Tri-state signal
    eWfVCDSignalTypeTriAnd,      //!< Tri-state And signal
    eWfVCDSignalTypeTriOr,       //!< Tri-state Or signal
    eWfVCDSignalTypeTriReg,      //!< Tri-state register
    eWfVCDSignalTypeTri0,        //!< Tri-state LOW signal
    eWfVCDSignalTypeTri1,        //!< Tri-state HIGH signal
    eWfVCDSignalTypeWAnd,        //!< Wired And signal
    eWfVCDSignalTypeWire,        //!< Wire type
    eWfVCDSignalTypeWOr,         //!< Wired Or type
    eWfVhdlSignal,               //!< VHDL Signal
    eWfVhdlVariable,               //!< VHDL Variable
    eWfVhdlConstant,             //!< VHDL Constant
    eWfVhdlFile,                 //!< VHDL File
    eWfVCDSignalTypeINVALID
  };

  enum TimescaleUnit
  { 
    eWfVCDTimescaleUnitFS, //!< Femtoseconds
    eWfVCDTimescaleUnitPS, //!< Picoseconds
    eWfVCDTimescaleUnitNS, //!< Nanoseconds
    eWfVCDTimescaleUnitUS, //!< Microseconds
    eWfVCDTimescaleUnitMS, //!< Milliseconds
    eWfVCDTimescaleUnitS, //!< Seconds
    eWfVCDTimescaleUnitINVALID
  };

  enum TimescaleValue
  {
    eWfVCDTimescaleValue1,
    eWfVCDTimescaleValue10,
    eWfVCDTimescaleValue100,
    eWfVCDTimescaleValueINVALID
  };

  class Identifier
  {
  public: 
    CARBONMEM_OVERRIDES
    
    Identifier(const UtString& name, const HdlId& info) : 
      mInfo(info), mName(name) 
    {}
    
    Identifier(const UtString& name) : mName(name)
    {}
    
    ~Identifier() 
    {}
		
    
    const HdlId& getHdlId() const { 
      return mInfo; 
    }

    const char * getName(void) const { 
      return mName.c_str(); 
    }
    
    HdlId::Type getDimensionType(void) const { 
      return mInfo.getType(); 
    }
    
    UtString* getNameString() { 
      return &mName; 
    }
    
  private:
    HdlId mInfo;
    UtString mName;
    
    Identifier();
  };
};

#endif
