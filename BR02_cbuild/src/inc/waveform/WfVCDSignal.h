// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WFVCDSIGNAL_H_
#define __WFVCDSIGNAL_H_



#include "util/UtString.h"
#include "waveform/WfAbsSignal.h"
#include "waveform/WfVCDFileReader.h"
#include "waveform/WfVCDSignalVCIter.h"
#include "util/CarbonTypes.h"


class WfVCDSignal : public WfAbsSignal
{
 public: CARBONMEM_OVERRIDES
  WfVCDSignal(WfVCDFileReader::ISignal* signal, const WfVCDTimeValueList* values);
  ~WfVCDSignal(void); 

  virtual const STSymbolTableNode* getSymNode() const;

  void getName(UtString* name) const;
  UInt32 getWidth(void) const;
  SInt32 getLsb(void) const;
  SInt32 getMsb(void) const;

  bool isReal() const;

  virtual void print() const;

  WfAbsSignalVCIter * createSignalVCIter(void);
  const WfVCDBase::ISignal* getInternalSignal() const;

  WfVCDBase::ISignal* getInternalSignal() {
    const WfVCDSignal* me = const_cast<const WfVCDSignal*>(this);
    return const_cast<WfVCDBase::ISignal*>(me->getInternalSignal());
  }

  virtual WfVCDSignal* castVCDSignal();

  virtual void getLastValue(DynBitVector* val, DynBitVector* drv) const;

 private:
  WfVCDFileReader::ISignal* mSignal;
  const WfVCDTimeValueList* mValues;
 
 private:
  /* Forbidden */
  WfVCDSignal(void);
  WfVCDSignal(const WfVCDSignal&);
  WfVCDSignal& operator=(const WfVCDSignal&);
};


#endif
