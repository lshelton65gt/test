// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WFVCDSIGNALVCITER_H_
#define __WFVCDSIGNALVCITER_H_



#include "util/CarbonTypes.h"
#include "waveform/WfAbsSignalVCIter.h"
#include "waveform/WfVCDTimeValue.h"

class WfVCDSignal;
class WfVCDFileReader;

/*!
  \file
  A concrete class that implements the interface specified
  by WfAbsSignalVCIter.
*/

//! WfVCDSignalVCIter class
/*!
  Concrete implementation of the WfAbsSignalVCIter interface for
  VCD data.  This class provides access to the value change data
  for a particular signal.

  More detailed documentation can be found with the WfAbsSignalVCIter
  class.
*/
class WfVCDSignalVCIter : public WfAbsSignalVCIter
{
 public: CARBONMEM_OVERRIDES
  WfVCDSignalVCIter(const WfVCDTimeValueList* values, WfVCDSignal* s);
  virtual ~WfVCDSignalVCIter(void);

  WfAbsSignal* getSignal();

  void getName(UtString* name) const;
  UInt64 getTime(void) const;
  const UInt64* getTimePtr(void) const;
  CarbonReal getValueReal() const;
  void getCompleteValue(const DynBitVector** value) const;
  void getValueDrive(const UInt32** value, const UInt32** drive) const;
  void getDynValue(DynBitVector* value, DynBitVector* drive) const;
  void getValueStr(UtString* value) const;

  UInt32 getWidth(void) const;

  UInt64 getMinTime(void);
  UInt64 getMaxTime(void);
  UInt64 getNextTransitionTime(void);
  UInt64 getPrevTransitionTime(void);

  bool atMinTime(void);
  bool atMaxTime(void);
  bool atStart(void);
  bool atEnd(void);
  bool atLastTransition(void);

  void gotoTime(UInt64 t);
  void gotoNextTime(void);
  void gotoPrevTime(void);
  void gotoNextChangeTime(void);
  void gotoPrevChangeTime(void);
  void gotoMinTime(void);
  void gotoMaxTime(void);

  static void calcValueDrive(DynBitVector* val, DynBitVector* drv, const DynBitVector* completeVal, UInt32 width);

 private:
  WfVCDTimeValueList::const_iterator findNextTransition(void);

 private:
  WfVCDSignal* mSignal;
  const WfVCDTimeValueList* mValues;
  WfVCDTimeValueList::const_iterator mCurrentValue;
  UInt64 mLastRequestedTime;

 private:
  // Forbidden.
  WfVCDSignalVCIter(void);
  WfVCDSignalVCIter(const WfVCDSignalVCIter&);
  WfVCDSignalVCIter& operator=(const WfVCDSignalVCIter&);
};


#endif
