// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WFABSSIGNALVCITER_H_
#define __WFABSSIGNALVCITER_H_



#include "util/UtString.h"
#include "util/CarbonTypes.h"

class DynBitVector;
/*!
  \file
  Abstract interface for accessing a signal's value change data
  from a waveform file.
*/

class WfAbsSignal;

//! WfAbsSignalVCIter class
/*!
  Abstract base class.  This class specifies an interface for accessing the
  value changes associated with a particular signal.
*/
class WfAbsSignalVCIter
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  /*!
    Construct a new signal value iterator.
   */
  WfAbsSignalVCIter(void) { };

  //! Structure for dealing with multiple iterators through time
  /*!
    Assuming this multiple iterators are passed through this structure
    this will maintain the lowest change time
  */
  struct IterNextTime
  {
    //! constructor
    IterNextTime(UInt64 lastTime) : mLowTime(UtUINT64_MAX), 
                                    mLastTime(lastTime), mChanged(false)  
    {}
    
    static inline UInt64 min(UInt64 a, UInt64 b) {
      if (a <= b)
        return a;
      return b;
    }

    //! Iterator manipulation
    void operator()(WfAbsSignalVCIter* iter) {
      UInt64 curTime = iter->getTime();
      iter->gotoNextChangeTime();
      UInt64 nextTime;
      if (iter->atEnd())
        nextTime = curTime;
      else
        nextTime = iter->getTime();
      if (nextTime != curTime)
      {
        mLowTime = min(mLowTime, nextTime);
        mChanged = true;
      }
    }
    
    //! Return the low change time
    UInt64 getLowTime() const { 
      UInt64 ret = mLowTime;
      if (! mChanged)
        ret = mLastTime;
      return ret; 
    }
    
    //! The low change time
    UInt64 mLowTime;
    UInt64 mLastTime;
    bool mChanged;
  };


  //! Destructor.
  virtual ~WfAbsSignalVCIter(void) { };

  //! Get the associated signal
  virtual WfAbsSignal* getSignal() = 0;

  //! The name of the signal.
  /*!
    Return the short name of the signal.
    \param name A UtString used to return the signal name.
    \return The name of the signal.
   */
  virtual void getName(UtString* name) const = 0;

  //! Get current time.
  /*!
    Return the current time location of this iterator.
   */
  virtual UInt64 getTime(void) const = 0;

  //! Get pointer to current time.
  /*!
    Return the current time location of this iterator.
   */
  virtual const UInt64* getTimePtr(void) const = 0;


  //! Get time of the next transition.
  /*!
    Return the time of the next value transition in the
    waveform.
   */
  virtual UInt64 getNextTransitionTime(void) = 0;


  //! Get time of the previous transition.
  /*!
    Return the time of the previous value transition in the
    waveform.
   */
  virtual UInt64 getPrevTransitionTime(void) = 0;

  //! Get current value for a real
  /*!
    Return the current value of this iterator of a real signal.
   */
  virtual CarbonReal getValueReal() const = 0;

  //! Get the current full value
  /*!
    If there is a drive value. The drive is prepended to the
    DynBitVector. Therefore the first set of words of the DynBitVector
    are for the value and the second set of words are for the drive.
    
    If there is no drive value, the DynBitVector will have the exact
    width as getWidth() returns.
  */
  virtual void getCompleteValue(const DynBitVector** value) const = 0;

  //! Get the current value in val/drive form
  /*!
    Drive may come back as NULL, meaning there is no x/z information.
  */
  virtual void getValueDrive(const UInt32** value, const UInt32** drive) const = 0;

  //! Copy the the complete value into the DynBitVectors.
  /*!
    If there is no drive, the drive returned will be all zeros
  */
  virtual void getDynValue(DynBitVector* value, DynBitVector* drive) const = 0;
  
  //! Get the value in string form. This is slow.
  virtual void getValueStr(UtString* value) const = 0;
  
  //! Get the bit width of the values.
  /*!
    Return the bitwidth of the values returned by this object.
   */
  virtual UInt32 getWidth(void) const = 0;


  //! Get the minimum time value.
  /*!
    Return the lower bound of the time span for which there is value
    change data.
    \return A time value.
   */
  virtual UInt64 getMinTime(void) = 0;

  //! Get the maximum time value.
  /*!
    Return the upper bound of the time span for which there is value
    change data.
    \return A time value.
   */
  virtual UInt64 getMaxTime(void) = 0;

  //! Is the iterator at the minimum time?
  /*!
    This method indicates if the iterator is pointing at the
    first value of the value stream.

    \retval true The iterator is at the minimum time.
    \retval false The iterator is not at the minimum time.
   */
  virtual bool atMinTime(void) = 0;

  //! Is the iterator at the maximum time?
  /*!
    This method indicates if the iterator is pointing at the
    last value of the value stream.

    \retval true The iterator is at the maximum time.
    \retval false The iterator is not at the maximum time.
   */
  virtual bool atMaxTime(void) = 0;

  //! Is the iterator at the start of the values?
  /*!
    This method indicates if the iterator is pointing before the
    first value of the value stream.

    \retval true The iterator is at the start of the values.
    \retval false The iterator is not at the start of the values.
   */
  virtual bool atStart(void) = 0;

  //! Is the iterator at the end of the values?
  /*!
    This method indicates if the iterator is pointing after the
    last value of the value stream.

    \retval true The iterator is at the end of the values.
    \retval false The iterator is not at the end of the values.
   */
  virtual bool atEnd(void) = 0;

  //! Is the iterator at the final transition?
  /*!
    \retval true The iterator is at the final transition.
    \retval false The iterator is not at the final transition.
   */
  virtual bool atLastTransition(void) = 0;

  //! Go to the specified time.
  /*!
    Move the iterator to the specified point in time.  If this time
    is outside of the iterator's range, the iterator will be positioned
    at the minimum or maximum time.
   */
  virtual void gotoTime(UInt64 t) = 0;

  //! Go to the next value change.
  /*!
    Move the iterator forward in time to the next value change
    event for this signal.  The value may or may not actually
    change.  If the iterator is already at the maximum time, this
    method will have no effect.
   */
  virtual void gotoNextTime(void) = 0;

  //! Go to the previous value change.
  /*!
    Move the iterator backward in time to the previous value change
    event for this signal.  The value may or may not actually
    change.  If the iterator is already at the minimum time, this
    method will have no effect.
   */
  virtual void gotoPrevTime(void) = 0;

  //! Go to the next value change.
  /*!
    Move the iterator forward in time to the next value change
    event for this signal. If the iterator is already at the maximum time, this
    method will have no effect.
   */
  virtual void gotoNextChangeTime(void) = 0;

  //! Go to the previous value change.
  /*!
    Move the iterator backward in time to the previous value change
    event for this signal.  If the iterator is already at the minimum time, this
    method will have no effect.
   */
  virtual void gotoPrevChangeTime(void) = 0;

  //! Go to the first value change.
  /*!
    Move the iterator to the lower bound in its time range.  This
    method produces the same result as:
    \code
    gotoTime(getMinTime())
    \endcode
   */
  virtual void gotoMinTime(void) = 0;

  //! Go to the last value change.
  /*!
    Move the iterator to the upper bound in its time range.  This
    method produces the same result as:
    \code
    gotoTime(getMaxTime())
    \endcode
   */
  virtual void gotoMaxTime(void) = 0;

  //getBitSize(UInt32);

 private:
  // Forbidden
  WfAbsSignalVCIter(const WfAbsSignalVCIter&);
  WfAbsSignalVCIter& operator=(const WfAbsSignalVCIter&);
};


#endif
