// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WFABSSIGNAL_H_
#define __WFABSSIGNAL_H_



#include "util/UtString.h"
#include "util/CarbonTypes.h"
#include "waveform/WfAbsSignalVCIter.h"


/*!
  \file
  Declaration of the WfAbsFileSignal class.
*/

class WfVCDSignal;
class STSymbolTableNode;

//! WfAbsSignal class
/*!
  Abstract base clase.  This class specifies an interface for signals that
  have been read from a waveform (or value change) data file, without being
  tied to a specific implementation or format.

  In general, a user will not directly create a signal object, but will
  receive via the WfAbsFileReader::watchSignal() method.

  \sa WfAbsFileReader
*/
class WfAbsSignal
{
protected:
  //! Constructor.
  /*!
    This constructs a signal object.  In general, the user will not directly
    create a signal object.
  */
  WfAbsSignal(void) : mUserData(NULL) 
  {}

public: 
  CARBONMEM_OVERRIDES

  //! Destructor.
  virtual ~WfAbsSignal(void) 
  {}

  //! The name of the signal.
  /*!
    Return the short name of the signal.
    \param name A UtString used to return the signal name.
    \return The name of the signal.
   */
  virtual void getName(UtString* name) const = 0;

  //! Get the waveform symtab node for this signal
  virtual const STSymbolTableNode* getSymNode() const = 0;

  virtual UInt32 getWidth(void) const = 0;
  //virtual UInt32 getUInt32Words(void) const  = 0;
  virtual SInt32 getLsb(void) const = 0;
  virtual SInt32 getMsb(void) const = 0;
  virtual bool isReal() const = 0;
  virtual void print() const = 0;

  virtual WfVCDSignal* castVCDSignal();

  virtual void getLastValue(DynBitVector* val, DynBitVector* drv) const = 0;

  typedef void* GenericData;
  void putUserData(GenericData data) {
    mUserData = data;
  }

  GenericData getUserData() const {
    return mUserData;
  }

  private:
  GenericData mUserData;
  
  /* Forbidden */
  WfAbsSignal(const WfAbsSignal&);
  WfAbsSignal& operator=(const WfAbsSignal&);
};


#endif
