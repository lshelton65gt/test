// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WFVCDTIMEVALUE_H_
#define __WFVCDTIMEVALUE_H_



#include "util/UtString.h"
#include "util/Util.h"

//class WfVCDTimeValueLess;


/*!
  \file
  Classes used to store waveform times and values.
*/


//! WfVCDTimeValue class
/*!
*/
class WfVCDTimeValue
{
  union Val
  {
    CARBONMEM_OVERRIDES

    const DynBitVector* mFullValue;
    const CarbonReal* mReal;
  };
  
public: 
  CARBONMEM_OVERRIDES
  
  WfVCDTimeValue(const UInt64* t): mTime(t)
  { 
    mVal.mReal = NULL;
  }

  ~WfVCDTimeValue(void)
  {
  }

  //! get the current time
  inline UInt64 getTime(void) const
  {
    return *mTime;
  }

  //! get pointer to the current time
  inline const UInt64* getTimePtr(void) const
  {
    return mTime;
  }

  inline void getValue(const DynBitVector** fullval) const
  {
    *fullval = mVal.mFullValue;
  }

  inline CarbonReal getValueReal() const
  {
    return *mVal.mReal;
  }

  void putValue(const DynBitVector* val)
  {
    mVal.mFullValue = val;
  }
  
  void putValueReal(const CarbonReal* realVal)
  {
    mVal.mReal = realVal;
  }
  
protected:
  const UInt64* mTime;
  Val mVal;
  
private:
  // Forbidden.
  WfVCDTimeValue(void);
  
  WfVCDTimeValue& operator=(const WfVCDTimeValue&);
};


//! WfVCDTimeValueList type
/*!
*/

class WfVCDTimeValueLess
{
 public: CARBONMEM_OVERRIDES
  inline bool operator()(const WfVCDTimeValue &x, const WfVCDTimeValue *y) const
  {
    return x.getTime() < y->getTime();
  };

  inline bool operator()(const WfVCDTimeValue *x, const WfVCDTimeValue &y) const
  {
    return x->getTime() < y.getTime();
  };
};


//! WfVCDTimeValueList type
/*!
*/
typedef UtArray<WfVCDTimeValue *> WfVCDTimeValueList;


#endif
