/* -*- C++ -*- */
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*
  Authors:  Mark Seneski
*/

#ifndef __WfFsdbReaderConduit_h_
#define __WfFsdbReaderConduit_h_

/*!
  \file

  The purpose of this file is to allow for C++ linking of the fsdb
  reader on Windows when we have a mingw-compiled libcarbon. Since
  the fsdbreader supplied by Debussy is C++, we create a C conduit to
  the classes. Even though this is a C conduit, it must be compiled by
  a C++ compiler because we use the Debussy-supplied fsdbKit.h file
  which defines all the types needed by the reader. That file and the files
  it includes assume a C++ compiler.
  
  The names of the functions are the same names found in ffrAPI.h with
  fsdbReader prefixed and the first letter of the ffrAPI function
  capitalized.
*/
#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
#error We are using the supplied fsdb reader types, which are defined in C++ syntax. This can only be compiled by C++ compilers currently.
#endif

  /* Define the Debussy types */
#include "ffrKit.h"
  
  typedef STRUCT ffrObject FsdbReaderFfrObjectID;
  typedef STRUCT ffrVCIterOne FsdbReaderFfrVCTrvsHdl;

  /* utility functions */
  fsdbRC fsdbReaderFfrCheckFSDB(const char* fileName);

  /* digit and unit get modified based on buf 
     'buf' here is something like "1 ns"
  */
  fsdbRC fsdbReaderFfrExtractScaleUnit(str_T buf, uint_T* digit, char** unit);


  /* fsdb reader ffrObject functions) */

  /* ffrOpen2
     Set ffrOpenOption to FFR_OPEN_OPT_DFT for the default.
  */
  FsdbReaderFfrObjectID* fsdbReaderFfrOpen2(str_T fname, fsdbTreeCBFunc tree_cb_func,
                                            void* tree_client_data, ffrOpenOption opt);
  
  
  fsdbFileType fsdbReaderFfrGetFileType(FsdbReaderFfrObjectID* id);

  void fsdbReaderFfrClose(FsdbReaderFfrObjectID* id);

  str_T fsdbReaderFfrGetSimVersion(FsdbReaderFfrObjectID* id);
  str_T fsdbReaderFfrGetSimDate(FsdbReaderFfrObjectID* id);
  str_T fsdbReaderFfrGetScaleUnit(FsdbReaderFfrObjectID* id);

  fsdbRC fsdbReaderFfrReadScopeVarTree(FsdbReaderFfrObjectID* id);
  void fsdbReaderFfrAddToSignalList(FsdbReaderFfrObjectID* id, int idCode);
  void fsdbReaderFfrLoadSignals(FsdbReaderFfrObjectID* id);
  void fsdbReaderFfrUnloadSignals(FsdbReaderFfrObjectID* id);
  FsdbReaderFfrVCTrvsHdl* fsdbReaderFfrCreateVCTraverseHandle(FsdbReaderFfrObjectID* id, int idCode);

  /* traverse handle functions */
  bool_T fsdbReaderTrvsHdlFfrHasIncoreVC(FsdbReaderFfrVCTrvsHdl* trvsID);
  void fsdbReaderTrvsHdlFfrGetMaxXTag(FsdbReaderFfrVCTrvsHdl* trvsID, fsdbTag64* timeVal);
  void fsdbReaderTrvsHdlFfrGetXTag(FsdbReaderFfrVCTrvsHdl* trvsID, fsdbTag64* timeVal);
  void fsdbReaderTrvsHdlFfrGetVC(FsdbReaderFfrVCTrvsHdl* trvsID, byte_T** vc);
  fsdbBytesPerBit fsdbReaderTrvsHdlFfrGetBytesPerBit(FsdbReaderFfrVCTrvsHdl* trvsID);
  uint_T fsdbReaderTrvsHdlFfrGetBitSize(FsdbReaderFfrVCTrvsHdl* trvsID);
  fsdbRC fsdbReaderTrvsHdlFfrGotoNextVC(FsdbReaderFfrVCTrvsHdl* trvsID);
  void fsdbReaderTrvsHdlFfrFree(FsdbReaderFfrVCTrvsHdl* trvsID);

#ifdef __cplusplus
}
#endif

#undef STRUCT

#endif
