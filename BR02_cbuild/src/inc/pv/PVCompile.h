/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "pv/PV.h"
#include "util/UtHashMap.h"

class PVCompileContext : public PVContext {
public:
  PVCompileContext(CarbonContext* cc);
  ~PVCompileContext();

  //! find the size of this net (works in runtime or compile-time)
  virtual UInt32 findNetSize(const char* net_name) const;
 
  virtual CarbonContext* getMasterCarbonContext() const;

  virtual NUDesign* getMasterDesign() const;

  virtual bool tieInputPort(const char* net_name,
                            const char* val,
                            PVPhase* phase);

  void optimize();

  void collectSequences(TVec* tvec, PVTransaction::SVec *sv);

  bool signalDirective(PVPhase*,
                       const char* directive_name,
                       const char* net_name,
                       const char* val);

  bool optimizePhase(PVPhase* phase, NUStmtList* stmts);
  bool optimizeConstants(NUDesign* phase_design);
  bool schedulePhase(PVPhase*, NUStmtList* stmts);

  //void schedule(NUStmtList* stmts);
  static void scheduleComb(NUStmtList* stmts, SCHCombinational* comb);
  static void scheduleEdge(NUStmtList* stmts,
                           SCHSchedule* sched,
                           NUNetElab* clk_net,
                           ClockEdge edge);

  void copyStmtsToRoot(NUModule* root, NUStmtList* stmts,
                       PVSequence* sequence);

  void createMuxedIONets(PVPhase* phase, PVDataPortLoop ports);
  void findMuxedIOReferences(PVPhase* phase,
                             PVDataPortLoop ports,
                             NUNetReplacementMap* rep_map);
  bool applyTieDirectives(PVPhase* phase);

private:
  typedef UtHashMap<PVPhase*,NUDesign* >PhaseDesignMap;
  PhaseDesignMap mPhaseDesignMap;

  CarbonContext* mMasterCarbonContext;
};
