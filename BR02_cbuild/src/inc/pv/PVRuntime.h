/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "pv/PV.h"
#include "shell/carbon_capi.h"

//! class to walk over PV address block and identify the affected transactions
/*!
 *! This iterator requires that tvec is sorted by start-address.  It uses
 *! binary search to find the first transaction in the address range,
 *! and then walks through the transaction vector to find subsequent ones.
 *!
 *! This loop supports buffered transactions.  While a user is making 
 *! consecutive transfers with increasing address and adjacent blocks,
 *! we can defer the transfer until we reach the end of each transaction.
 *!
 *! The iterator will yield transactions that are complete and ready
 *! to execute.
 *!
 *! For this reason, this Loop class is not reconstructed each time 
 *! read/write is called.  We keep a persistent instance of this loop
 *! in the PVContext so it can retain the previous state.
 */
class PVAddressToTransactionContext {
public:
  CARBONMEM_OVERRIDES

  //! ctor
  PVAddressToTransactionContext(const PVContext::TVec& tvec);

  //! To flush any cached read data, all we need to do is forget we had it
  void clear();

  void reset(char* target_addr, const char* base_addr, UInt32 byte_count);

  //! Returns true if this transfer is consecutive with previous one, and in the
  //! same PVtransaction
  bool transfer(char* target_addr, const char* base_addr, UInt32 byte_count);

  //! Have we reached the last full transaction?
  bool atEnd() const {return (mTransaction == NULL) || !mIsTransactionFull;}

  PVTransaction* getTransaction() {return mTransaction;}
  UInt32 getAddress() {return mAddr;}
  UInt32 getTransactionByteCount() {return mTransactionByteCount;}
  bool reachesEndOfTransaction() const {return mIsTransactionFull;}
  bool errorOccurred() const {return mErrorOccurred;}

  void operator++();

private:

  void findFirstTransaction();
  void computeTransactionBounds();


  const PVContext::TVec& mTVec;
  const char* mData;
  UInt32 mAddr;
  UInt32 mByteCount;
  bool mErrorOccurred;
  bool mIsTransactionFull;
  PVTransaction* mTransaction;
  UInt32 mTransactionByteCount;
  UInt32 mIndex;
  UtString mDataBuffer;
}; // class PVAddressToTransactionLoop

class PVRuntimeContext : public PVContext {
public:
  CARBONMEM_OVERRIDES

  PVRuntimeContext(CarbonObjectID* model, const char* base_address,
                   CarbonUInt32 time_delta = 10);

  //! returns true if successful, false if not all the data was in a transaction
  bool write(char* target_addr, const char* data, UInt32 byte_count);

  //! returns true if successful, false if not all the data was in a transaction
  bool read(char* target_addr, char* data, UInt32 byte_count);

  virtual void runSchedule();

  virtual CarbonObjectID* getModel() const;
  virtual UInt32 findNetSize(const char* net_name) const;

  bool initialize();

private:
  bool initTransactions(TVec& tvec);

  PVAddressToTransactionContext mReader;
  PVAddressToTransactionContext mWriter;
  const char* mBaseAddress;

  CarbonObjectID* mModel;
  CarbonTime mTime;
  CarbonUInt32 mTimeDelta;

  //! Buffer for caching incomplete read & write data
  UtString mBuffer;
  char* mBufferPtr;
};

