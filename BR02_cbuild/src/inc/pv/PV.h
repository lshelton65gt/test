// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef PV_H_
#define PV_H_

/*!
  \file 
  Declaration of programmer's view context
*/

#include "nucleus/Nucleus.h"
#include "pv/PV.h"
#include "util/Loop.h"
#include "util/UtArray.h"
#include "util/UtString.h"
#include "util/SourceLocator.h"
#include "cfg/CarbonCfg.h"

// cbuild class predefines
class CarbonContext;
class PVAddressToTransactionContext;
class PVNet;
class PVPhase;
class PVSequence;
class PVTransaction;
class SCHCombinational;
class SCHSchedule;
class Stats;

typedef UtHashMap<UtString,PVNet*> PVNetMap;
typedef UtHashSet<PVNet*> PVNetSet;
enum PVTransactionType {ePVRead, ePVWrite};


class PVContext {
public:
  CARBONMEM_OVERRIDES

  PVContext();
  virtual ~PVContext();

  PVTransaction* addTransaction(const char* tname, const char* clk,
                                PVTransactionType type,
                                UInt32 addr_start, UInt32 addr_end);

  typedef UtArray<PVTransaction*> TVec;
  typedef Loop<TVec> TransactionLoop;
  TransactionLoop loopReadTransactions() {
    return TransactionLoop(mReadTransactions);
  }
  TransactionLoop loopWriteTransactions() {
    return TransactionLoop(mWriteTransactions);
  }

  virtual void runSchedule();
  virtual CarbonObjectID* getModel() const;

  //! create or find an existing PV Net of given name
  PVNet* getPVNet(const char* name);

  //! find the size of this net (works in runtime or compile-time)
  virtual UInt32 findNetSize(const char* net_name) const = 0;

  virtual bool tieInputPort(const char* net_name, const char* val, PVPhase*);

  const char* getErrmsg() const {return mErrmsg.c_str();}

protected:

  TVec mWriteTransactions;
  TVec mReadTransactions;
  //typedef UtHashMap<UtString,PVTransactionClock> ClockMap;
  //ClockMap mClockMap;
  PVNetMap mNetMap;

  UtString mErrmsg;
}; // class PVContext

class PVNet {
public:
  CARBONMEM_OVERRIDES

  PVNet(PVContext*, const char* net_name, UInt32 bit_size);
  ~PVNet();

  bool initialize(CarbonObjectID* model);
  void deposit(const CarbonUInt32* val, const CarbonUInt32* drive = NULL);
  void examine();
  UInt32 numWords() const {return (mBitSize + 31) / 32;}
  UInt32 numBytes() const {return (mBitSize + 7) / 8;}
  UInt32 numBits() const {return mBitSize;}

  CarbonUInt32* getBuffer() const {return mBuffer;}

  void putIsReadable(bool val) {mIsReadable |= val;}
  void putIsWritable(bool val) {mIsWritable |= val;}

  const char* getName() const {return mNetName.c_str();}

private:
  static void valueChangedCB(CarbonObjectID*, CarbonNetID*,
                             CarbonClientData client_data,
                             CarbonUInt32* val, CarbonUInt32*);

  UtString mNetName;
  UInt32 mBitSize;
  CarbonNetID* mShellNet;
  PVContext* mPVContext;
  CarbonUInt32* mBuffer;
  bool mIsReadable;
  bool mIsWritable;
  CarbonNetValueCBDataID* mCallback;
};

//! class to specify a mapping between data & port
/*!
 *! This class maps between any bits in the PV data, which is a char*
 *! array, to any bits in the hardware model, which is a UInt32 array.
 *! 
 *! The mapping can be of arbitrary complexity.  The caller is responsible
 *! for establishing any byte-swapping requirements if the endian-ness
 *! of the simulation host differs from the endian-ness of the DUT.
 *!
 *! The caller handles this because this specifies a 
 */
class PVDataPort {
public:
  CARBONMEM_OVERRIDES

  PVDataPort(PVNet*, UInt32 byte_offset, bool is_readable, bool is_writable);
  ~PVDataPort();

  void read(char* data);
  void write(const char* data);

  PVNet* getNet() const {return mNet;}
  void putMultiplexedName(const char* name) {mMuxedName = name;}
  const char* getMultiplexedName() const {return mMuxedName.c_str();}

  //! replace the net used for this port
  /*!
   *! This is intended for use in the runtime to use a phase-multiplexed
   *! port.
   */
  void putNet(PVNet*);

private:
  PVNet* mNet;
  UtString mMuxedName;
  UInt32 mByteOffset;           // into the data array
};
  
//! class to specify a mapping between address & port
class PVAddressPort {
public:
  CARBONMEM_OVERRIDES

  PVAddressPort(PVNet*, SInt32 shift, SInt32 offset);
  ~PVAddressPort();

  void deposit(CarbonUInt32 addr);

private:
  SInt32 mShift;
  SInt32 mOffset;
  PVNet* mNet;
};
  
//! class to specify a tied input port
class PVTiePort {
public:
  CARBONMEM_OVERRIDES

  PVTiePort(PVNet*, const char* val);
  ~PVTiePort();

  void deposit();
  const char* getValueString() const {return mValueString.c_str();}
  PVNet* getNet() const {return mNet;}

private:
  PVNet* mNet;
  CarbonUInt32* mValue;
  UtString mValueString;
};
  

typedef UtArray<PVAddressPort*> PVAddressPortVec;
typedef Loop<PVAddressPortVec> PVAddressPortLoop;
typedef UtArray<PVDataPort*> PVDataPortVec;
typedef Loop<PVDataPortVec> PVDataPortLoop;
typedef UtArray<PVTiePort*> PVTiePortVec;
typedef Loop<PVTiePortVec> PVTiePortLoop;

class PVTransaction {
public:
  CARBONMEM_OVERRIDES

  PVTransaction(PVContext*, const char* tname, const char* clk,
                PVTransactionType type, UInt32 addr_start,
                UInt32 addr_end);
  ~PVTransaction();

  PVSequence* addSequence(const char* name = NULL);

  typedef UtArray<PVSequence*> SVec;
  typedef Loop<SVec> SequenceLoop;
  SequenceLoop loopSequences() {return SequenceLoop(mSequences);}

  PVContext* getPVContext() const {return mPVContext;}
  const char* getTransactionName() const {return mTransactionName.c_str();}
  const char* getClkName() const {return mTransactionClockName.c_str();}

  void putPartner(PVTransaction* partner);

  bool initialize(CarbonObjectID* model);
  void read(char* data);
  void write(const char* data);
  CarbonObjectID* getModel() const {return mPVContext->getModel();}

  PVNet* getTransactionClock() const { return mTransactionClockNet; }

  UInt32 getAddressStart() const {return mAddressStart;}
  UInt32 getAddressEnd() const {return mAddressEnd;}
  UInt32 getSize() const { return mAddressEnd - mAddressStart + 1;}

  void toggleClock();

  PVTransactionType getType() const {return mType;}

  //! bind input port net to offset within 'data'
  /*!
   *! TBD - handling little endian vs big endian
   *!
   *! This indicates to the API that a call to carbonDeposit
   *! should be generated.
   */
  bool bindInputData(PVNet* pv_net, UInt32 byte_offset);

  //! bind output port net to offset within 'data'
  /*!
   *! TBD - handling little endian vs big endian
   *!
   *! This indicates to the API that a call to carbonExamine
   *! on this port should be generated.
   */
  bool bindOutputData(PVNet* pv_net, UInt32 byte_offset);

private:
  PVContext* mPVContext;
  UtString mTransactionName;
  UtString mTransactionClockName;

  //! For CA models, we will need to toggle the normal transaction clock
  //! (e.g. HCLK) on every phase.  This is it.
  PVNet* mTransactionClockNet;

  UInt32 mAddressStart;
  UInt32 mAddressEnd;
  SVec mSequences;

  // read or write
  PVTransactionType mType;

  // If this transaction is a read, then its partner is a write
  PVTransaction* mPartner;

  PVDataPortVec mReadPorts;
  PVDataPortVec mWritePorts;
};

//! class to specify a transaction sequence
class PVSequence {
public:
  CARBONMEM_OVERRIDES

  PVSequence(PVTransaction* transaction, UInt32 index, const char* name = NULL);
  ~PVSequence();

  PVPhase* addPhase(const char* name = NULL);

  typedef UtArray<PVPhase*> PVec;
  typedef Loop<PVec> PhaseLoop;
  PhaseLoop loopPhases() {return PhaseLoop(mPhases);}

  PVTransaction* getTransaction() const {return mTransaction;}
  const char* getClkName() const {return mTransaction->getClkName();}
  UInt32 getIndex() const {return mIndex;}
  PVContext* getPVContext() const {return mTransaction->getPVContext();}
  const char* getName() const {return mName.c_str();}

  void schedule();

  bool initialize(CarbonObjectID* model);
  void read(UInt32 address, char* data);
  void write(UInt32 address, const char* data);
  CarbonObjectID* getModel() const {return mTransaction->getModel();}

  NUStmtList mStmts;

  const char* getClockName() const {return mSequenceClockName.c_str();}

  bool findMultiplexedIOs(bool replace_nets);

private:
  void toggleClock();

  UtString mName;
  PVTransaction* mTransaction;
  UInt32 mIndex;
  PVec mPhases;

  // For pv-optimized models, we generate a clock that executes an entire
  // sequence in one call to carbonSchedule.  This is its name so we can
  // find it at runtime in the shell.  For CA models with a PV interface,
  // this string will be empty
  UtString mSequenceClockName;
  PVNet* mSequenceClockNet;

  //! These ports are the result of aggregating all the phase ports,
  //! replicating signficant ports as needed
  PVAddressPortVec mAddressPorts;
  PVDataPortVec mDataPorts;
};

//! class to specify a transaction Phase
class PVPhase {
public:
  CARBONMEM_OVERRIDES

  PVPhase(PVSequence*, UInt32 index, const char* name = NULL);
  ~PVPhase();

  //! bind input port net to offset within 'data'
  /*!
   *! TBD - handling little endian vs big endian
   *!
   *! This indicates to the API that a call to carbonDeposit
   *! should be generated.
   */
  bool bindInputData(PVNet* pv_net, UInt32 byte_offset);

  //! bind output port net to offset within 'data'
  /*!
   *! TBD - handling little endian vs big endian
   *!
   *! This indicates to the API that a call to carbonExamine
   *! on this port should be generated.
   */
  bool bindOutputData(PVNet* pv_net, UInt32 byte_offset);

  //! bind input port net to (address >> shift) + offset
  /*!
   *! This indicates to the API that a call to carbonDeposit should be
   *! generated.  Note that the address will be shifted left if shift<0.
   */
  bool bindAddress(const char* input_port_name,
                   SInt32 shift, SInt32 offset);

  //void optimizeConstants();
  //bool optimize();

  //! optimize & schedule the design for this transaction phase
  //void design(NUStmtList*);

#if 0
  void copyModuleContents(NUModule* master_mod,
                          NUModule* transaction_mod);
  void copyModuleContents();
  void putGuardNet(NUNet*);
  void findUseDefNets(NUModule* transaction_mod,
                      NUModule* master_mod,
                      NUNetSet* master_uses,
                      NUNetSet* master_defs);
#endif


  const char* getClkName() const {return mSequence->getClkName();}
  PVSequence* getSequence() const {return mSequence;}
  PVTransaction* getTransaction() const {return mSequence->getTransaction();}
  PVContext* getPVContext() const {return mSequence->getPVContext();}

  const char* getName() const {return mName.c_str();}

  bool initialize(CarbonObjectID* model);
  void setupTies();
  void setupAddress(UInt32 address);
  void readData(char* data);
  void write(UInt32 address, const char* data);
  CarbonObjectID* getModel() const {return mSequence->getModel();}

  void addTie(PVNet* pv_net, const char* val);

  PVTiePortLoop loopTies() {return PVTiePortLoop(mTiePorts);}
  PVDataPortLoop loopReadPorts() {return PVDataPortLoop(mReadPorts);}
  PVDataPortLoop loopWritePorts() {return PVDataPortLoop(mWritePorts);}

  bool multiplexPort(PVDataPort* port, bool replace_net);

  void getPhaseNetName(const char* net_name, UtString* phase_name);

private:
  PVSequence* mSequence;
  UInt32 mIndex;
  UtString mName;

  //! These ports are defined by the user (XML file), and are based on the
  //! original design names.  They are replicated, uniquified, and aggregated
  //! in the owner PVSequence
  PVAddressPortVec mAddressPorts;
  PVDataPortVec mReadPorts;
  PVDataPortVec mWritePorts;
  PVTiePortVec mTiePorts;
}; // class PVPhase

#endif // PV_H_
