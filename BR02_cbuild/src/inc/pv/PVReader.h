// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef PVREADER_H_
#define PVREADER_H_

#include "util/c_memmanager.h"
#include "util/UtString.h"
#include "libxml/tree.h"

class PVXmlParser {
public:
  CARBONMEM_OVERRIDES

  //! constructor
  PVXmlParser();

  //! destructor
  ~PVXmlParser();

  //! fetch the error message string
  /*!
   *  Fetch any error text that was accumulated by the parser
   */
  const char *errorText();

protected:
  bool isElement(xmlNode *node, const char *name);
  bool hasAttr(xmlNode *node, const char *name);
  void getContent(xmlNode *node, UtString *content);
  void reportError(const char *message);
  bool getError() const {return mErrorReported;}

  //! pre-parsing XML package setup
  void parseSetup();
  //! post-parsing XML package cleanup
  void parseCleanup();

 private:
  // error handling
  static void sXmlStructuredErrorHandler(void *arg, xmlErrorPtr error);
  bool mErrorReported;
  UtString mErrorText;
};

/*!
 * Simple class to get attributes from libxml2 nodes.
 * Hides the casting and takes care of calling xmlFree().
 */
class PVXmlAttr
{
public:
  CARBONMEM_OVERRIDES

  PVXmlAttr(xmlNode *node, const char *name) {
    mValue = xmlGetProp(node, BAD_CAST name);
  }

  ~PVXmlAttr() {
    if (mValue != NULL) {
      xmlFree(mValue);
    }
  }

  const char *value() {
    return (const char *) mValue;
  }
private:
  xmlChar *mValue;
};


#endif


