// -*-C++-*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*
 * callback class (for use with VerilogDesignWalker), used to inspect and gather statistics about sv designs
 * @Brief callback class for use with VerilogDesignWalker
 *
*/

#ifndef _SVINSPECTORCB_H_
#define _SVINSPECTORCB_H_

#include "util/UtStackPOD.h"
#include "util/UtArray.h"
#include "util/UtMap.h"
#include "LineFile.h"


#include "verific2nucleus/VerificDesignWalkerCB.h"

//! CrbnLanguageUnit class
/*!
 * container for keeping track of the places a language unit was seen 
 */
class CrbnLanguageUnit
{
  typedef UtArray<int> ExampleTest;

  typedef UtArray<Verific::linefile_type> SourceLocatorList;

 public:
  CrbnLanguageUnit() {}
  
  ~CrbnLanguageUnit() {}
  

  void addItem(Verific::linefile_type lf) {
    mSourceLocators.push_back(lf);
  }
  
  void printItems(int currentLimit);
  unsigned isSameLineFile(const Verific::linefile_type left, const Verific::linefile_type right);
  unsigned numItems() {return mSourceLocators.size();}
  

 private:
  SourceLocatorList mSourceLocators;
};



//! SVInspector class 
/*!
 * data manager used by SVInspectorCB methods, for statistics about language units encountered in a design
 */
class SVInspector{
public:
  typedef UtStackPOD<const Verific::VeriModuleItem*>  VerificHierarchyStack;

  typedef UtMap<UtString,CrbnLanguageUnit*> LanguageUnitMap;
  typedef LanguageUnitMap::iterator LanguageUnitMapIter;

  void addModuleItem(const Verific::VeriModuleItem* ve_mod){
    mHierarchy.push(ve_mod);
  }
  
  void addModuleInstance(const Verific::VeriModuleItem* ve_mod){
    mHierarchy.push(ve_mod);
  }
  void popModuleInstance(){
    if (!mHierarchy.empty()) {
      mHierarchy.pop();
    }
  }
  void popModuleItem(){
    if (!mHierarchy.empty()) {
      mHierarchy.pop();
    }
  }
  bool isTopModule(){
    return ( mHierarchy.empty());
  }

  static const char* getPrintToken(unsigned veri_token);

  // add (tally) the item with 'name' that was seen at location 'lf'
  void sawLU(const UtString & name, Verific::linefile_type lf);
  void sawLU(const char * name, Verific::linefile_type lf);
  void logMsg(Verific::VerilogDesignWalkerCB::VisitPhase phase, const UtString & msg, Verific::linefile_type lf);
  // print a report of all language units
  void printAllLU(int limit);
  bool isFromPackage(Verific::VeriIdDef* start_id){
    return isFromWhat(start_id, true, false);
  }
  bool isFromInterface(Verific::VeriIdDef* start_id){
    return isFromWhat(start_id, false, true);
  }
  bool isBoundedQueue(Verific::VeriIdDef* start_id){
    return isQueue(start_id, false, true);
  }
  bool isUnBoundedQueue(Verific::VeriIdDef* start_id){
    return isQueue(start_id, true, false);
  }
  void getFlagsForDataType(Verific::VeriDataType* dataType, bool *isVoidType, bool *isIntType);

  Verific::VeriIdDef* getNearestPackage(Verific::VeriIdDef* start_id);

private:
  bool isQueue(Verific::VeriIdDef* start_id, bool lookForUnbounded, bool lookForBounded);
  bool isFromWhat(Verific::VeriIdDef* start_id, bool lookForPackage, bool lookForInterface);


  VerificHierarchyStack mHierarchy;

  LanguageUnitMap  mLUMap;                // map, indexed by language unit to file:line
};

//! SVInspectorCB class 
/*!
 * for use with Verific::VerilogDesignWalkerCB, one method for each type of VeriTreeNode
 */
class SvinspectorCB : public Verific::VerilogDesignWalkerCB {
public:

  ///@brief 
  SvinspectorCB(int limit):
    mProcessModuleContents(true),
    mSVInspectorCount(limit)
  {}
  virtual ~SvinspectorCB() {}

  void fillVeriVariableModifier(const Verific::VeriVariable& node, UtString* modifier);
  ///@name Walker methods tobe redefined (per need) in derived classses
  /// Please note that each of these functions called twice (PRE phase, POST phase, so phases needs to be handled properly
  /// So almost always we should have someething like a switch in their implementation 
  /// if (phase == VISITPRE) {
  ///     // PRE WORK
  /// } else if (phase == VISITPOST) {
  ///     // POST WORK
  /// }
public:
///    // The following class definitions can be found in VeriTreenode, phase.h
  virtual Status VERI_WALK(VeriTreeNode, node, phase) ;
///    virtual Status VERI_WALK(VeriCommentnode, node, phase) ;
///
///    // The following class definitions can be found in VeriModule.h
  virtual Status VERI_WALK(VeriModule, node, phase) ;
///    virtual Status VERI_WALK(VeriPrimitive, node, phase) ;
///    virtual Status VERI_WALK(VeriConfiguration, node, phase) ;
///
///    // The following class definitions can be found in VeriExpression.h
    virtual Status VERI_WALK(VeriExpression, node, phase) ;
///    virtual Status VERI_WALK(VeriName, node, phase) ;
    virtual Status VERI_WALK(VeriIdRef, node, phase) ;
    virtual Status VERI_WALK(VeriIndexedId, node, phase) ;
///    virtual Status VERI_WALK(VeriSelectedName, node, phase) ;
    virtual Status VERI_WALK(VeriIndexedMemoryId, node, phase) ;
///    virtual Status VERI_WALK(VeriConcat, node, phase) ;
///    virtual Status VERI_WALK(VeriMultiConcat, node, phase) ;
    virtual Status VERI_WALK(VeriFunctionCall, node, phase) ;
    virtual Status VERI_WALK(VeriSystemFunctionCall, node, phase) ;
///    virtual Status VERI_WALK(VeriMinTypMaxExpr, node, phase) ;
    virtual Status VERI_WALK(VeriUnaryOperator, node, phase) ;
    virtual Status VERI_WALK(VeriBinaryOperator, node, phase) ;
///    virtual Status VERI_WALK(VeriQuestionColon, node, phase) ;
///    virtual Status VERI_WALK(VeriEventExpression, node, phase) ;
///    virtual Status VERI_WALK(VeriPortConnect, node, phase) ;
///    virtual Status VERI_WALK(VeriPortOpen, node, phase) ;
    virtual Status VERI_WALK(VeriAnsiPortDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriTimingCheckEvent, node, phase) ;
///    virtual Status VERI_WALK(VeriDataType, node, phase) ;
///    virtual Status VERI_WALK(VeriNetDataType, node, phase) ; // VIPER #4896
///    virtual Status VERI_WALK(VeriPathPulseVal, node, phase) ;
///
///    // The following class definitions can be found in VeriId.h
   virtual Status VERI_WALK(VeriIdDef, node, phase) ;
   virtual Status VERI_WALK(VeriVariable, node, phase) ;
///    //virtual Status VERI_WALK(VeriMemoryId, node, phase) ;
   virtual Status VERI_WALK(VeriInstId, node, phase) ;
   virtual Status VERI_WALK(VeriModuleId, node, phase) ;
///    virtual Status VERI_WALK(VeriUdpId, node, phase) ;
///    virtual Status VERI_WALK(VeriConfigurationId, node, phase) ;
///    virtual Status VERI_WALK(VeriTaskId, node, phase) ;
///    virtual Status VERI_WALK(VeriFunctionId, node, phase) ;
///    virtual Status VERI_WALK(VeriGenVarId, node, phase) ;
   virtual Status VERI_WALK(VeriParamId, node, phase) ;
///    virtual Status VERI_WALK(VeriBlockId, node, phase) ;
///
///    // The following class definitions can be found in VeriMisc.h
    virtual Status VERI_WALK(VeriRange, node, phase) ;
///    virtual Status VERI_WALK(VeriStrength, node, phase) ;
///    virtual Status VERI_WALK(VeriNetRegAssign, node, phase) ;
///    virtual Status VERI_WALK(VeriDefParamAssign, node, phase) ;
///    virtual Status VERI_WALK(VeriCaseItem, node, phase) ;
///    virtual Status VERI_WALK(VeriGenerateCaseItem, node, phase) ;
///    virtual Status VERI_WALK(VeriPath, node, phase) ;
///    virtual Status VERI_WALK(VeriDelayOrEventControl, node, phase) ;
///    virtual Status VERI_WALK(VeriConfigRule, node, phase) ;
///    virtual Status VERI_WALK(VeriInstanceConfig, node, phase) ;
///    virtual Status VERI_WALK(VeriCellConfig, node, phase) ;
///    virtual Status VERI_WALK(VeriDefaultConfig, node, phase) ;
///    virtual Status VERI_WALK(VeriUseClause, node, phase) ;
///#ifdef VERILOG_PATHPULSE_PORTS
///    virtual Status VERI_WALK(VeriPathPulseValPorts, node, phase) ;
///#endif
///
///    // The following class definitions can be found in VeriModuleItem.h
///    virtual Status VERI_WALK(VeriModuleItem, node, phase) ;
    virtual Status VERI_WALK(VeriDataDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriNetDecl, node, phase) ;
    virtual Status VERI_WALK(VeriFunctionDecl, node, phase) ;
    virtual Status VERI_WALK(VeriTaskDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriDefParam, node, phase) ;
///    virtual Status VERI_WALK(VeriContinuousAssign, node, phase) ;
///    virtual Status VERI_WALK(VeriGateInstantiation, node, phase) ;
   virtual Status VERI_WALK(VeriModuleInstantiation, node, phase) ;
///    virtual Status VERI_WALK(VeriSpecifyBlock, node, phase) ;
///    virtual Status VERI_WALK(VeriPathDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriSystemTimingCheck, node, phase) ;
///    virtual Status VERI_WALK(VeriInitialConstruct, node, phase) ;
   virtual Status VERI_WALK(VeriAlwaysConstruct, node, phase) ;
///    virtual Status VERI_WALK(VeriGenerateConstruct, node, phase) ;
///    virtual Status VERI_WALK(VeriGenerateConditional, node, phase) ;
///    virtual Status VERI_WALK(VeriGenerateCase, node, phase) ;
///    virtual Status VERI_WALK(VeriGenerateFor, node, phase) ;
///    virtual Status VERI_WALK(VeriGenerateBlock, node, phase) ;
///    virtual Status VERI_WALK(VeriTable, node, phase) ;
///    virtual Status VERI_WALK(VeriPulseControl, node, phase) ;
///
///    // The following class definitions can be found in VeriStatement.h
///    virtual Status VERI_WALK(VeriStatement, node, phase) ;
    virtual Status VERI_WALK(VeriBlockingAssign, node, phase) ;
    virtual Status VERI_WALK(VeriNonBlockingAssign, node, phase) ;
    virtual Status VERI_WALK(VeriGenVarAssign, node, phase) ;
    virtual Status VERI_WALK(VeriAssign, node, phase) ;
    virtual Status VERI_WALK(VeriDeAssign, node, phase) ;
///    virtual Status VERI_WALK(VeriForce, node, phase) ;
///    virtual Status VERI_WALK(VeriRelease, node, phase) ;
    virtual Status VERI_WALK(VeriTaskEnable, node, phase) ;
    virtual Status VERI_WALK(VeriSystemTaskEnable, node, phase) ;
///    virtual Status VERI_WALK(VeriDelayControlStatement, node, phase) ;
///    virtual Status VERI_WALK(VeriEventControlStatement, node, phase) ;
    virtual Status VERI_WALK(VeriConditionalStatement, node, phase) ;
    virtual Status VERI_WALK(VeriCaseStatement, node, phase) ;
///    virtual Status VERI_WALK(VeriLoop, node, phase) ;
///    virtual Status VERI_WALK(VeriForever, node, phase) ;
///    virtual Status VERI_WALK(VeriRepeat, node, phase) ;
///    virtual Status VERI_WALK(VeriWhile, node, phase) ;
///    virtual Status VERI_WALK(VeriFor, node, phase) ;
///    virtual Status VERI_WALK(VeriWait, node, phase) ;
///    virtual Status VERI_WALK(VeriDisable, node, phase) ;
///    virtual Status VERI_WALK(VeriEventTrigger, node, phase) ;
///    virtual Status VERI_WALK(VeriSeqBlock, node, phase) ;
///    virtual Status VERI_WALK(VeriParBlock, node, phase) ;
///
///    // The following class definitions can be found in VeriConstVal.h
///    virtual Status VERI_WALK(VeriConst, node, phase) ;
///    virtual Status VERI_WALK(VeriConstVal, node, phase) ;
   virtual Status VERI_WALK(VeriIntVal, node, phase) ;
   virtual Status VERI_WALK(VeriRealVal, node, phase) ;
///
///    // The following class definitions can be found in VeriLibrary.h
///    virtual Status VERI_WALK(VeriLibraryDecl, node, phase) ;
///
///    // The System Verilog Class
    virtual Status VERI_WALK(VeriInterface, node, phase) ;
///    virtual Status VERI_WALK(VeriProgram, node, phase) ;
    virtual Status VERI_WALK(VeriClass, node, phase) ;
///    virtual Status VERI_WALK(VeriPropertyDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriSequenceDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriModport, node, phase) ;
///    virtual Status VERI_WALK(VeriModportDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriClockingDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriConstraintDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriBindDirective, node, phase) ;
    virtual Status VERI_WALK(VeriPackage, node, phase) ; // Std SV 1800 addition
///    virtual Status VERI_WALK(VeriChecker, node, phase) ; // Std SV 1800 addition
///    virtual Status VERI_WALK(VeriSequentialInstantiation, node, phase) ; // Std SV 1800 addition
    virtual Status VERI_WALK(VeriTypeRef, node, phase) ;
///    virtual Status VERI_WALK(VeriKeyword, node, phase) ;
    virtual Status VERI_WALK(VeriStructUnion, node, phase) ;
    virtual Status VERI_WALK(VeriEnum, node, phase) ;
///    virtual Status VERI_WALK(VeriDotStar, node, phase) ;
///    virtual Status VERI_WALK(VeriIfOperator, node, phase) ;
///    virtual Status VERI_WALK(VeriSequenceConcat, node, phase) ;
///    virtual Status VERI_WALK(VeriClockedSequence, node, phase) ;
///    virtual Status VERI_WALK(VeriAssignInSequence, node, phase) ;
///    virtual Status VERI_WALK(VeriDistOperator, node, phase) ;
///    virtual Status VERI_WALK(VeriSolveBefore, node, phase) ;
    virtual Status VERI_WALK(VeriCast, node, phase) ;
///    virtual Status VERI_WALK(VeriNew, node, phase) ;
///    virtual Status VERI_WALK(VeriConcatItem, node, phase) ;
///    virtual Status VERI_WALK(VeriAssertion, node, phase) ;
    virtual Status VERI_WALK(VeriJumpStatement, node, phase) ;
///    virtual Status VERI_WALK(VeriDoWhile, node, phase) ;
///    virtual Status VERI_WALK(VeriInterfaceId, node, phase) ;
///    virtual Status VERI_WALK(VeriProgramId, node, phase) ;
///    virtual Status VERI_WALK(VeriCheckerId, node, phase) ;
    virtual Status VERI_WALK(VeriTypeId, node, phase) ;
///    virtual Status VERI_WALK(VeriModportId, node, phase) ;
///    virtual Status VERI_WALK(VeriNull, node, phase) ;
///    virtual Status VERI_WALK(VeriDollar, node, phase) ;
///    virtual Status VERI_WALK(VeriAssignmentPattern, node, phase) ;
///    virtual Status VERI_WALK(VeriMultiAssignmentPattern, node, phase) ;
///    virtual Status VERI_WALK(VeriTimeLiteral, node, phase) ;
///    virtual Status VERI_WALK(VeriOperatorId, node, phase) ;
///    virtual Status VERI_WALK(VeriOperatorBinding, node, phase) ;
///    virtual Status VERI_WALK(VeriForeachOperator, node, phase) ;
    virtual Status VERI_WALK(VeriStreamingConcat, node, phase) ;
///    virtual Status VERI_WALK(VeriNetAlias, node, phase) ;
///    virtual Status VERI_WALK(VeriTimeUnit, node, phase) ;
    virtual Status VERI_WALK(VeriForeach, node, phase) ;
///    virtual Status VERI_WALK(VeriWaitOrder, node, phase) ;
    virtual Status VERI_WALK(VeriCondPredicate, node, phase) ;
///    virtual Status VERI_WALK(VeriDotName, node, phase) ;
    virtual Status VERI_WALK(VeriTaggedUnion, node, phase) ;
///    virtual Status VERI_WALK(VeriClockingId, node, phase) ;
///    virtual Status VERI_WALK(VeriClockingDirection, node, phase) ;
///    virtual Status VERI_WALK(VeriClockingSigDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriRandsequence, node, phase) ;
///    virtual Status VERI_WALK(VeriCodeBlock, node, phase) ;
///    virtual Status VERI_WALK(VeriProduction, node, phase) ;
///    virtual Status VERI_WALK(VeriProductionItem, node, phase) ;
///    virtual Status VERI_WALK(VeriProductionId, node, phase) ;
///    virtual Status VERI_WALK(VeriWith, node, phase) ;
///    virtual Status VERI_WALK(VeriWithExpr, node, phase) ;
///    virtual Status VERI_WALK(VeriInlineConstraint, node, phase) ;
///    virtual Status VERI_WALK(VeriWithStmt, node, phase) ;
    virtual Status VERI_WALK(VeriArrayMethodCall, node, phase) ;
///    virtual Status VERI_WALK(VeriInlineConstraintStmt, node, phase) ;
///    virtual Status VERI_WALK(VeriCovergroup, node, phase) ;
///    virtual Status VERI_WALK(VeriCoverageOption, node, phase) ;
///    virtual Status VERI_WALK(VeriCoverageSpec, node, phase) ;
///    virtual Status VERI_WALK(VeriBinDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriBinValue, node, phase) ;
///    virtual Status VERI_WALK(VeriOpenRangeBinValue, node, phase) ;
///    virtual Status VERI_WALK(VeriTransBinValue, node, phase) ;
///    virtual Status VERI_WALK(VeriDefaultBinValue, node, phase) ;
///    virtual Status VERI_WALK(VeriSelectBinValue, node, phase) ;
///    virtual Status VERI_WALK(VeriTransSet, node, phase) ;
///    virtual Status VERI_WALK(VeriTransRangeList, node, phase) ;
///    virtual Status VERI_WALK(VeriSelectCondition, node, phase) ;
///    virtual Status VERI_WALK(VeriTypeOperator, node, phase) ;
///    virtual Status VERI_WALK(VeriImportDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriLetDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriDefaultDisableIff, node, phase) ;
///    virtual Status VERI_WALK(VeriExportDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriScopeName, node, phase) ;
///    virtual Status VERI_WALK(VeriNamedPort, node, phase) ;
    virtual Status VERI_WALK(VeriPatternMatch, node, phase) ;
///    virtual Status VERI_WALK(VeriConstraintSet, node, phase) ;
///    virtual Status VERI_WALK(VeriCovgOptionId, node, phase) ;
///    virtual Status VERI_WALK(VeriSeqPropertyFormal, node, phase) ;
///    virtual Status VERI_WALK(VeriBinsId, node, phase) ;
///    virtual Status VERI_WALK(VeriLetId, node, phase) ;
///    virtual Status VERI_WALK(VeriCaseOperator, node, phase) ;
///    virtual Status VERI_WALK(VeriCaseOperatorItem, node, phase) ;
///    virtual Status VERI_WALK(VeriIndexedExpr, node, phase) ;
///    virtual Status VERI_WALK(VeriDPIFunctionDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriDPITaskDecl, node, phase) ;
///    virtual Status VERI_WALK(VeriExternForkjoinTaskId, node, phase) ;
///    virtual Status VERI_WALK(VeriPrototypeId, node, phase) ;
///

  bool doProcessModuleContents(bool newVal){
    bool oldVal = mProcessModuleContents;
    mProcessModuleContents = newVal;
    return oldVal;
  }

///@name Prevent the compiler from implementing the following
private:
  SvinspectorCB(SvinspectorCB &node);
  SvinspectorCB& operator=(const SvinspectorCB &rhs);

  SVInspector mSVInspector;

  bool mProcessModuleContents;  // if true then process the contents of the next VeriModule that is encountered

  int mSVInspectorCount;
};



#endif
