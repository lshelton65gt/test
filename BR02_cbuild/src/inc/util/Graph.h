// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  Declaration of the Graph abstract base class.  Specific graph instances can be
  defined by creating derived classes of Graph.
*/

#ifndef _GRAPH_H_
#define _GRAPH_H_


#include "util/CarbonTypes.h"
#include "util/Iter.h"
#include "util/UtArray.h"
#include "util/UtHashSet.h"
#include "util/UtSet.h"

//! Place-holder class representing a graph node
class GraphNode
{
public: CARBONMEM_OVERRIDES
  //! constructor
  GraphNode() {}

  //! virtual destructor
  virtual ~GraphNode() {}
};

typedef UtSet<GraphNode*> GraphNodeSet;

//! Place-holder class repesenting a graph edge
class GraphEdge
{
public: CARBONMEM_OVERRIDES
  //! constructor
  GraphEdge() {}

  //! virtual destructor
  virtual ~GraphEdge() {}
};

//! Abstract base class representing a generic graph data structure
class Graph
{
 public: CARBONMEM_OVERRIDES
  Graph() {};

  //! Get an iterator over all nodes in the graph
  virtual Iter<GraphNode*> nodes() = 0;

  //! Get an iterator over all edges from a node
  virtual Iter<GraphEdge*> edges(GraphNode* node) const = 0;

  //! Get the terminal node of an edge
  virtual GraphNode* endPointOf(const GraphEdge* edge) const = 0;

  // nodes and edges can have flags and scratch data, which is useful when implementing graph algorithms

  //! set flag bits for a node (OR in flags from mask)
  virtual void setFlags(GraphNode* node, UInt32 mask) = 0;

  //! clear flag bits for a node (clear flags that are set in mask)
  virtual void clearFlags(GraphNode* node, UInt32 mask) = 0;

  //! test if any of a set of flag bits is set for a node
  virtual bool anyFlagSet(const GraphNode* node, UInt32 mask) const = 0;

  //! test if all bits from a set of flag bits are set for a node
  virtual bool allFlagsSet(const GraphNode* node, UInt32 mask) const = 0;

  //! get the full set of flag bits for a node
  virtual UInt32 getFlags(const GraphNode* node) const = 0;

  //! set the scratch data for a node
  virtual void setScratch(GraphNode* node, UIntPtr scratch) = 0;

  //! retrieve the scratch data for a node
  virtual UIntPtr getScratch(const GraphNode* node) const = 0;

  //! set flag bits for an edge (OR in flags from mask)
  virtual void setFlags(GraphEdge* edge, UInt32 mask) = 0;

  //! clear flag bits for an edge (clear flags that are set in mask)
  virtual void clearFlags(GraphEdge* edge, UInt32 mask) = 0;

  //! test if any of a set of flag bits is set for an edge
  virtual bool anyFlagSet(const GraphEdge* edge, UInt32 mask) const = 0;

  //! test if all bits from a set of flag bits are set for an edge
  virtual bool allFlagsSet(const GraphEdge* edge, UInt32 mask) const = 0;

  //! get the full set of flag bits for an edge
  virtual UInt32 getFlags(const GraphEdge* edge) const = 0;

  //! set the scratch data for an edge
  virtual void setScratch(GraphEdge* edge, UIntPtr scratch) = 0;

  //! retrieve the scratch data for an edge
  virtual UIntPtr getScratch(const GraphEdge* edge) const = 0;

  //! extract a subgraph of the given graph containing only certain nodes
  virtual Graph* subgraph(GraphNodeSet& keepSet, bool copyScratch) = 0;

  //! Remove the set of nodes and all edges incident on them from the graph
  virtual void removeNodes(const GraphNodeSet& removeSet) = 0;

  virtual ~Graph() {};
}; // class Graph

//! BiGraph class
/*! This class provides an interface for a bidirectional graph. It has
 *  all the features of a Graph as well as additional functions only
 *  available for bidrected graphs.
 *
 *  The benefit of bidirected graphs is that it is quick to remove and
 *  add nodes in the middle of the graph.
 */
class BiGraph : public Graph
{
public:
  CARBONMEM_OVERRIDES
  BiGraph() {}

  //! Get an iterator over all start edges from a node
  virtual Iter<GraphEdge*> reverseEdges(GraphNode* node) const = 0;

  //! Get the starting point for an edge
  virtual GraphNode* startPointOf(const GraphEdge* edge) const = 0;

  //! Remove an edge from a graph
  virtual void removeEdge(GraphEdge* edge) = 0;

  //! Add an edge to a graph
  /*! Returns NULL if the edge was added, returns the existing edge if
   *  an equivalent edge was already there (and therefore the new edge
   *  was not added).
   */
  virtual GraphEdge* addEdge(GraphNode* start, GraphEdge* edge) = 0;

  //! Move an edge within this graph -- it modifies the end point
  /*! This function removes the edge from the graph, modifies the
   *  edge, and then reinserts it into the graph. If an equivalent
   *  edge already exists, it is not added. No memory allocation or
   *  deallocation occurs.
   *
   *  Returns an existing edge if an equivalent edge already exists
   *  and NULL if it doesn't.
   */
  virtual GraphEdge* modifyOutEdge(GraphEdge* edge, GraphNode* to) = 0;

  //! Move an in edge within this graph -- it modifies the start point
  /*! This function removes the edge from the graph, modifies the
   *  edge, and then reinserts it into the graph. If an equivalent
   *  edge already exists, it is not added. No memory allocation or
   *  deallocation occurs.
   *
   *  Returns an existing edge if an equivalent edge already exists
   *  and NULL if it doesn't.
   */
  virtual GraphEdge* modifyInEdge(GraphEdge* edge, GraphNode* start) = 0;
}; // class BiGraph : public Graph

//! A base class for iterating over the nodes and edges of a graph.
/*! Specific graph algorithms can be implemented by specializing this
 *  class.
 */
class GraphWalker
{
 public: CARBONMEM_OVERRIDES
  //! a GraphWalker::Command returned from the visitor functions controls the walker's path
  typedef UInt32 Command;
  typedef UtArray<GraphNode*> Nodes;
  typedef UtArray<GraphEdge*> Edges;
  static const Command GW_CONTINUE = 0;  //!< continue with DFS
  static const Command GW_BACKTRACK = 1; //!< backtrack from a node, ignoring all remaining children
  static const Command GW_SKIP = 2;      //!< do not recurse through this edge
  static const Command GW_FAIL = 3;      //!< immediately return failure
  static const Command GW_SUCCEED = 4;   //!< immediately return success

  GraphWalker() {};

  //! traverse the graph calling the visitor functions.
  virtual bool walk(Graph* g);

  //! traverse the graph from a starting node, calling the visitor functions.
  virtual bool walkFrom(Graph* g, GraphNode* node);

  //! reset the graph before traversing it using walkFrom()
  virtual void reset(Graph* g);

  //! function called to fill a vector with the graph's nodes
  virtual void nodes(Nodes* nodeVector, Graph* g);

  //! function called to fill a vector with a node's edges
  virtual Iter<GraphEdge*> edges(GraphNode* node);

  //! function called before visiting any children of a node
  virtual Command visitNodeBefore(Graph*, GraphNode*) { return GW_CONTINUE; }

  //! function called between visiting children of a node
  virtual Command visitNodeBetween(Graph*, GraphNode*) { return GW_CONTINUE; }

  //! function called after visiting all children of a node
  virtual Command visitNodeAfter(Graph*, GraphNode*) { return GW_CONTINUE; }

  //! function called when traversing a tree edge
  virtual Command visitTreeEdge(Graph*, GraphNode*, GraphEdge*) { return GW_CONTINUE; }

  //! function called when traversing a cross edge
  virtual Command visitCrossEdge(Graph*, GraphNode*, GraphEdge*) { return GW_CONTINUE; }

  //! function called when traversing a back edge
  virtual Command visitBackEdge(Graph*, GraphNode*, GraphEdge*) { return GW_CONTINUE; }

  //! function called when backtracking to determine what to do at this edge
  virtual bool continueBacktrack(Graph*, GraphNode*) { return false; }

  //! returns true if node could be used as a starting node during a walk default always return true
  virtual bool isValidStartNode(Graph*, GraphNode*) { return true; }

  virtual ~GraphWalker() {};

 private:
  //! A set of nodes in the pending state
  UtHashSet<GraphNode*> mPending;

  //! A set of nodes in the finished state
  UtHashSet<GraphNode*> mFinished;

  //! internal traversal function used when walking a graph node
  Command visit(GraphNode* node);

  //! internal traversal function used when walking a graph edge
  Command visitEdge(GraphNode* node, GraphEdge* edge);

  //! internal function to decide whether we should return after node visit
  /*! This function tests the input cmd. It can update the cmd and
   *  return true or false. A return of true indicates the recursion
   *  should pop back up a level.
   */
  bool returnVisitNode(Command* cmd);

  //! internal function to decide whether we should return after edge visit
  /*! This function tests the input cmd. It can update the cmd and
   *  return true or false. A return of true indicates the recursion
   *  should pop back up a level.
   */
  bool returnVisitEdge(Command* cmd);

private:
  void edges(Edges* edgeVector, Graph* g, GraphNode* node);// old version

  Graph* mGraph;
};

//! A base class for sorted iteration over the nodes and edges of a graph.
/*! This class is derived from the GraphWalker class and overrides the
 *  nodes and edges virtual functions to populate sorted vectors of
 *  the nodes and edges. The sorting is done according the the
 *  provided compare structures.
 */
class GraphSortedWalker : public GraphWalker
{
#if ! pfGCC_2
  using GraphWalker::walk;
  using GraphWalker::walkFrom;
#endif

public:
  CARBONMEM_OVERRIDES

  //! constructor
  GraphSortedWalker();

  //! virtual destructor
  virtual ~GraphSortedWalker();

  //! A class to compare two graph nodes
  class NodeCmp
  {
  public:
    CARBONMEM_OVERRIDES

    //! constructor
    NodeCmp() {}

    //! virtual destructor
    virtual ~NodeCmp() {}

    //! virtual function to compare two nodes
    virtual bool operator()(const GraphNode*, const GraphNode*) const = 0;
  };

  //! A class to compare two graph edges
  class EdgeCmp
  {
  public:
    CARBONMEM_OVERRIDES

    //! constructor
    EdgeCmp() {}

    //! virtual destructor
    virtual ~EdgeCmp() {}

    //! virtual function to compare two edges
    virtual bool operator()(const GraphEdge*, const GraphEdge*) const = 0;
  };

  //! Function to walk the graph
  virtual bool walk(Graph* g, NodeCmp* nodeCmp, EdgeCmp* edgeCmp);

  //! traverse the graph from a starting node, calling the visitor functions.
  virtual bool walkFrom(Graph* g, GraphNode* node, NodeCmp*, EdgeCmp*);

  //! Override the nodes function to sort the vector
  virtual void nodes(Nodes* nodeVector, Graph* graph);

  //! Override the edge function to sort the edges
  virtual Iter<GraphEdge*> edges(GraphNode* node);

private:
  //! EdgeCmpWrapper class
  /*! This class is used to make copy construction of the edge compare
   *  work with STL containers. Copy construction of the base clas
   *  looses the virtual pointer of the derived class. This wrapper
   *  class stores the compare class by pointer so that we retain the
   *  right vptr. The operator() for this wrapper class calls the one
   *  in the member variable edge compare class.
   */
  class EdgeCmpWrapper
  {
  public: CARBONMEM_OVERRIDES
    EdgeCmpWrapper(EdgeCmp* edgeCmp) : mEdgeCmp(edgeCmp) {}
    EdgeCmpWrapper(const EdgeCmpWrapper& w) { mEdgeCmp = w.mEdgeCmp; }
    ~EdgeCmpWrapper() {}
    bool operator()(const GraphEdge* e1, const GraphEdge* e2) const
    {
      return (*mEdgeCmp)(e1, e2);
    }
  private:
    EdgeCmp* mEdgeCmp;
  };

  //! NodeCmpWrapper class
  /*! This class is used to make copy construction of the node compare
   *  work with STL containers. Copy construction of the base clas
   *  looses the virtual pointer of the derived class. This wrapper
   *  class stores the compre class by pointer so that we retain the
   *  right vptr. The operator() for this wrapper class calls the one
   *  in the member variable node compare class.
   */
  class NodeCmpWrapper
  {
  public: CARBONMEM_OVERRIDES
    NodeCmpWrapper(NodeCmp* nodeCmp) : mNodeCmp(nodeCmp) {}
    NodeCmpWrapper(const NodeCmpWrapper& w) { mNodeCmp = w.mNodeCmp; }
    ~NodeCmpWrapper() {}
    bool operator()(const GraphNode* e1, const GraphNode* e2) const
    {
      return (*mNodeCmp)(e1, e2);
    }
  private:
    NodeCmp* mNodeCmp;
  };

  // Store the edge and node compare classes
  NodeCmpWrapper mNodeCmpWrapper;
  EdgeCmpWrapper mEdgeCmpWrapper;
}; // class GraphSortedWalker : public GraphWalker


//! Find our roots.
class GraphRootFinder
{
public:
  //! Constructor.
  GraphRootFinder() {};
  //! Destructor.
  virtual ~GraphRootFinder() {};

  //! Find all nodes in the graph without fanout. Cyclic graphs may not have roots.
  void findRoots(Graph * graph, GraphNodeSet* rootSet);

  //! Override if you want to exclude certain edges from root discovery.
  virtual bool processEdge(Graph * /*graph*/, GraphEdge * /*edge*/) { return true; }
private:
};

#endif // _GRAPH_H_
