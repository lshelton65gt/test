// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonAssert_h_
#define __CarbonAssert_h_

//#include "config.h"
#ifndef	_ASSERT_H
#ifdef __cplusplus
#include <cassert>
#else
#include <assert.h>
#endif
#endif

/*!
  \file
  Definitions for asserting on "must" conditionals. Contains versions
  for both debug and product, where the assert will be turned off.
*/

#ifndef __STRING
//! Stringify expression
#define __STRING(exp) #exp
#endif

//! Assert with a informational message
#define INFO_ASSERT(exp, helpMsg) _do_HELPFUL_ASSERT((exp), __STRING(exp), __FILE__, __LINE__, (helpMsg))

//! Assert with an informational message but do not print expression.
#define INFO_ASSERT_NO_EXPRESSION(exp, helpMsg) _do_HELPFUL_ASSERT((exp), NULL, __FILE__, __LINE__, (helpMsg))

//! Call obj->printAssertInfo(), then the file/line and abort
/*!
 * \exprStr may be NULL in which case just the msg and the file/line information is printed.
 */
extern void CarbonHelpfulAssert(const char* file, long line, const char* expStr, const char* msg);

inline void _do_HELPFUL_ASSERT(bool exp, const char* expStr, const char* file,
                               long lineNumber, const char* msg)
{
  if (!exp)
    CarbonHelpfulAssert(file, lineNumber, expStr, msg);
  // not reached
}

extern void CarbonAbort(const char* file, long line, const char* expStr, const char* caller);

extern void CarbonPrintAssertBanner();

//! if ! expr call funcCall function then abort with file/line information
#define FUNC_ASSERT(exp, funcCall) \
  do { \
    if (!(exp)) { \
      CarbonPrintAssertBanner(); \
      funcCall; \
      CarbonAbort( __FILE__, __LINE__, __STRING(exp), "PRINT_ASSERT"); \
    } \
  } while (0) /* no trailing semicolon -- it is supplied by the caller */


#ifdef CDB

#define ASSERT(exp) assert(exp)          /* this_assert_OK */

#define VERIFY(exp) _do_VERIFY((exp), __STRING(exp), __FILE__, __LINE__)


inline bool _do_VERIFY(bool exp, const char* expStr, const char* file,
		       long lineNumber)
{
  if (!exp)
    CarbonAbort(file, lineNumber, expStr, "VERIFY");
  return exp;
}

#else

//! Assert on false expression
#define ASSERT(exp) assert(exp)     /* this_assert_OK */

//! Conditional assert
/*!
  Useful in if/while blocks

  \code
  if (VERIFY(a))
    // do something
  \endcode
*/
#define VERIFY(exp) (exp)

#endif

#endif
