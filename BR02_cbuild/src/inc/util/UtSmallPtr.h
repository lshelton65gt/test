/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _UT_SMALLPTR_H_
#define _UT_SMALLPTR_H_

#define JUST_DONT_USE_SMALL_POINTERS 1

#include "util/CarbonPlatform.h"

/* These macros allow storing pointers on an LP64 architecture in
   four rather than eight bytes.  We know we allocated the memory from
   within a certain range, and the LSBs are zero, so we can right shift
   the pointer without losing information.
*/
#if pfLP64

#if JUST_DONT_USE_SMALL_POINTERS

/* Small pointers disabled */
#define SMALLPTR(type) type
#define SMALLPTR_EXPAND(type, sp) ((type) (sp))
#define SMALLPTR_SHRINK(ptr) ((void*) ptr)

#else

/* Small pointers enabled */
#define SMALLPTR(type) UInt32
#define SMALLPTR_EXPAND(type, sp) ((type) SmallPtrExpand(sp))
#define SMALLPTR_SHRINK(ptr) SmallPtrShrink((void*) ptr)

#endif

#ifdef __cplusplus
extern "C" {
#endif

extern void* SmallPtrExpand(UInt32);
extern UInt32 SmallPtrShrink(const void*);

#ifdef __cplusplus
}
#endif


#else

#define SMALLPTR(type) type
#define SMALLPTR_EXPAND(type, sp) ((type) (sp))
#define SMALLPTR_SHRINK(ptr) ((void*) ptr)

#endif

#endif /*  _UT_SMALLPTR_H_ */
