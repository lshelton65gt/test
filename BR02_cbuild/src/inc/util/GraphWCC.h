// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003, 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  Contains an algorithm for computing weakly-connected components of a graph.
  The algorithm uses a Union-Find data structure to partition the graph nodes
  into equivalence classes, where two nodes are in the same equivalence class
  whenever there is an edge between them in the graph.
*/

#ifndef _GRAPHWCC_H_
#define _GRAPHWCC_H_


#include "util/Graph.h"
#include "util/GenericDigraph.h"
#include "util/UtList.h"
#include "util/UnionFind.h"
#include "util/Loop.h"

/*!
  A derived class of GraphWalker specialized to compute the weakly connected components
  of a directed graph.
 */
class GraphWCC : public GraphWalker
{
public:  CARBONMEM_OVERRIDES
  // The class that represents a single connected component
  class Component
  {
  public:  CARBONMEM_OVERRIDES
    Component() : mNodes() {};
    typedef CLoop<UtSet<GraphNode*> > NodeLoop;
    void addNode(GraphNode* node) { mNodes.insert(node); }
    NodeLoop loopNodes() const { return NodeLoop(mNodes); }
    size_t size() const { return mNodes.size(); }
    friend class GraphWCC;
  private:
    UtSet<GraphNode*> mNodes;
  };

  typedef CLoop<UtList<Component*> > ComponentLoop; //!< Type for looping over components
  GraphWCC();

  //! Analyze the graph
  void compute(Graph*g);

  //! Returns the number of weakly connected components in the graph
  int numComponents() const { return mComponents.size(); }

  //! Iterates over all components
  ComponentLoop loopComponents() const;

  //! Extract the subgraph of nodes in the component
  Graph* extractComponent(Component* component, bool copyScratch = false) const;

  ~GraphWCC();

private:
  // these are the visiting functions in which the WCC algorithm is implemented
  GraphWalker::Command visitNodeBefore(Graph*, GraphNode* node);
  GraphWalker::Command visitTreeEdge(Graph* g, GraphNode* from, GraphEdge* edge);
  GraphWalker::Command visitBackEdge(Graph* g, GraphNode* from, GraphEdge* edge);
  GraphWalker::Command visitCrossEdge(Graph* g, GraphNode* from, GraphEdge* edge);

private:
  // Types for templatized digraph classes
  typedef GenericDigraph<void*, Component*> Digraph;
  typedef Digraph::Edge CEdge;
  typedef Digraph::Node CNode;

  // Delete all the components in this graph
  void cleanup();

  UnionFind<GraphNode*> mUnionFind;     //!< Union-Find structure for computing equivalence classes

  Graph* mOriginalGraph;                //!< the original graph components were computed from
  UtList<Component*> mComponents;       //!< the list of components built during the walk
};

#endif // _GRAPHWCC_H_
