// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef CONSTANTRANGE_H_
#define CONSTANTRANGE_H_

#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#include <functional>

class UtString;
class ZostreamDB;
class ZistreamDB;

//! ConstantRange class
/*!
 * [msb:lsb] constant-valued range.
 */
class ConstantRange
{
public: CARBONMEM_OVERRIDES
  //! default constructor, needed for db reading
  ConstantRange() : mMsb(-1), mLsb(-1)
  {}

  ConstantRange(SInt32 msb, SInt32 lsb) :
    mMsb(msb), mLsb(lsb)
  {}

  ConstantRange(const ConstantRange& copy) :
    mMsb(copy.mMsb), mLsb(copy.mLsb)
  {}

  // assignment operator
  ConstantRange& operator=(const ConstantRange& other)
  {
    if(this != &other)
    {
      mMsb = other.getMsb();
      mLsb = other.getLsb();
    }

    return *this;
  }


  SInt32 getLsb() const { return mLsb; }
  SInt32 getMsb() const { return mMsb; }
  UInt32 getLength() const;

  //! Returns the number of words needed to represent the range
  UInt32 numWordsNeeded() const;

  //! Returns the number of bytes needed to represent the range
  UInt32 numBytesNeeded() const;

  //! Return true if an index of the given number of bits is guaranteed to be within this range.
  bool safeIndex(UInt32 num_bits) const;

  void setLsb(SInt32 lsb) { mLsb = lsb; }
  void setMsb(SInt32 msb) { mMsb = msb; }

  //! The following operation truncates this constant range
  /*! Often we need just a sub set of the bits of a net. This is done
   *  with sizing as opposed to explicit part selects. So I am
   *  creating an operation to resize a constant range. Note this is
   *  destructive.
   */
  void truncate(UInt32 newLength);

  inline bool operator ==(const ConstantRange& other) const
  {
    return (getLsb () == other.getLsb ()) && (getMsb () == other.getMsb ());
  }

  inline bool operator !=(const ConstantRange& other) const
  {
    return ! (*this == other);
  }

  //! Return true if the given index is within this range
  inline bool contains(SInt32 idx) const
  {
    if (mMsb > mLsb) {
      return ((idx >= mLsb) and (idx <= mMsb));
    } else {
      return ((idx <= mLsb) and (idx >= mMsb));
    }
  }

  //! Return true if the given range is completely within this range
  inline bool contains(const ConstantRange& r) const
  {
    return (contains(r.getLsb()) and contains(r.getMsb()));
  }

  //! Return true if the given range is adjacent with this range
  /*! (e.g. {3:2}->adjacent{5:4} is true
   *        {6:6}->adjacent{5:4} is true
   *        {2:1}->adjacent{5:4} is false
   *        {2:3}->adjacent{5:4} is false (different directions)
   */
  bool adjacent(const ConstantRange& r) const;

  //! Return the combination of range for this and the given range
  /*!
   * Do not confuse this with a concat since the ranges must also be
   * adjacent. If the ranges could be combined either way, then the
   * returned range will have the range for this on the 
   * left end.  ie {7:7}->combine{8:8}  --> {7:8}
   */
  ConstantRange combine(const ConstantRange& r) const;
  
  //! Return true if the given range overlaps this range
  bool overlaps(const ConstantRange& r) const;

  //! Return the overlap of this with the given range
  /*!
   * They must overlap.
   */
  ConstantRange overlap(const ConstantRange& r) const;

  //! Shift a range by an offset value
  void adjust (SInt32 offset) { mMsb += offset; mLsb += offset; }

  //! Return the rightmost range value:  min(msb,lsb)
  SInt32 rightmost() const;

  //! Return the leftmost range value:  max(msb,lsb)
  SInt32 leftmost() const;

  //! Return the idx offset from the lsb
  /*! This routine asserts if the index is out of range.  \sa offsetUnbounded
   */
  UInt32 offsetBounded(SInt32 idx) const;

  //! Retrun the idx offset from the lsb
  /*! This routine alows idx to be outside the range msb:lsb, and can
   *! return a negative number.  \sa offsetBounded
   */
  SInt32 offsetUnbounded(SInt32 idx) const;

  //! Returns the bit index given an offset from the lsb
  SInt32 index(UInt32 offset, bool checkBounds = true) const;

  //! Normalize this range with respect to the given range
  //! The given range is usually the declared range of a net.
  //! Normalize shifts a range from a declared range to a normalized range.
  //! Denormalizre converts it back.
  //! e.g.  [3:4]->normalize([1:5])  ----> [2:1]
  void normalize( const ConstantRange *r, bool checkBounds = true);
  
  //! Denormalize this range with respect to the given range
  void denormalize( const ConstantRange *r, bool checkBounds = true);
  
  static ConstantRange* getMax(ConstantRange* first, ConstantRange* second);
  
  // Debug print to stdout
  void print() const;

  // format into [msb:lsb]
  const char* format(UtString* str) const;

  //! write this object to a database
  bool dbWrite(ZostreamDB&) const;

  //! read this object from a database
  bool dbRead(ZistreamDB&);

  size_t hash() const;

  //! general compare function
  ptrdiff_t compare(const ConstantRange& b) const;

  //! less than for UtMap
  bool lessThan(const ConstantRange& b) const {return compare(b) < 0;}

  //! Is b within this range but with flipped indices?
  /*!
    If b is contained with this range, then this returns true if b is
    flipped within the range. 
    
    For example, if this range is [8:1], and b is [7:2] this will
    return false, because b is contained but is not flipped. However,
    if b is [2:7] then this returns true.
  */
  bool isContainedFlip(const ConstantRange& b) const;

  //! Is b the same direction as this range?
  /*!
    This is just like isContainedFlip but containedness is not
    checked. So, if the b's msb and this msb are both logically
    related in the same way to their respective lsb's then this
    returns false. However, if b's msb is say, greater than it's lsb
    and this range's msb is less than it's lsb then this returns true.
  */
  bool isFlipped(const ConstantRange& b) const;

  //! Switch the msb and the lsb
  void switchSBs();

  //! Is this a big-endian (MSB >= LSB) or little-endian (MSB < LSB)
  bool bigEndian () const { return mMsb >= mLsb; }

private:
  SInt32 mMsb;
  SInt32 mLsb;
};

template<class _Obj> class UtArray;
template<class _Obj> class UtVector;
typedef UtArray<ConstantRange*> ConstantRangeArray;
typedef UtVector<ConstantRange> ConstantRangeVector;

//! class for ConstantRange vector comparisons.
/*!
  Provides the ConstantRangeVector less-than/compare function for a UtMap with
  ConstantRangeVector as key. 
*/
class ConstantRangeVectorCompare :
  public std::binary_function<ConstantRangeVector&, ConstantRangeVector&, bool>
{
public:
  //! Less than operation for UtMap. Returns true only if vec1 < vec2.
  bool operator() (const ConstantRangeVector& vec1,
                   const ConstantRangeVector& vec2) const;

  //! Return integer =0 for equal <0 for lesser and >0 for greater
  /*!
    Returns true if vec1 has fewer elements than vec2. If both have
    same number of elements, returns true if vec1 has an element that
    is less than vec2 when searching in order. ConstantRange::compare()
    method is used for comparison.
  */
  SInt32 compare(const ConstantRangeVector& vec1,
                 const ConstantRangeVector& vec2) const;
};

//! Class to manage ConstantRange pointers
class ConstantRangeFactory
{
public:
  CARBONMEM_OVERRIDES
  
  //! constructor
  ConstantRangeFactory();

  //! destructor
  ~ConstantRangeFactory();

  //! Get or create a ConstantRange
  /*!
    The ConstantRange returned cannot be modified. It is in a set by
    value.
  */
  const ConstantRange* find(SInt32 msb, SInt32 lsb);

  //! write to the database
  bool dbWrite(ZostreamDB& out) const;
  
  //! read from the database
  bool dbRead(ZistreamDB& in);

  //! Add entries from a different ConstantRangeFactory
  void add(const ConstantRangeFactory& otherFactory);

private:
  //! helper class
  class Aux;
  Aux* mAux;
};

#endif
