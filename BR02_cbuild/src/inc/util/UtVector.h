// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtVector_h_
#define __UtVector_h_
#ifndef __MemManager_h_

#include "util/MemManager.h"
#endif

#include "util/CarbonTypes.h"
#include <vector>

//! alternate STL vector definition that uses the Carbon memory allocator
template<class _Obj>
class UtVector: public std::vector<_Obj, CarbonSTLAlloc<_Obj> >
{
public: CARBONMEM_OVERRIDES
  typedef std::vector<_Obj, CarbonSTLAlloc<_Obj> > _Base;
#if pfICC_8 | pfICC_7 // | pfSPARCWORKS
  typedef typename _Base::value_type value_type;
  typedef typename std::vector<_Obj, typename CarbonSTLAlloc<_Obj> >::iterator iterator;
  typedef typename _Base::reference reference;
  typedef typename _Base::const_iterator const_iterator;
#endif
  UtVector(size_t sz): _Base(sz) {}
  UtVector(size_t sz, CarbonSTLAlloc<_Obj> __a): _Base(sz, __a) {}
  UtVector(size_t sz, const _Obj& init): _Base(sz, init) {}
  UtVector(size_t sz, const _Obj& init, CarbonSTLAlloc<_Obj> __a):
    _Base(sz, init, __a) {}
  UtVector(CarbonSTLAlloc<_Obj> __a): _Base(__a)  {}
  template <class _In>
    UtVector (_In first, _In last): _Base (first, last) {}
  UtVector() {}

#if pfGCC_2
  class const_iterator: public random_access_iterator<const _Obj, size_t>
  {
  public: CARBONMEM_OVERRIDES
    typedef const _Obj value_type;
    typedef const _Obj& reference;
    typedef const _Obj* pointer;
    typedef ptrdiff_t difference_type;

    const_iterator(const _Obj* ptr): mPtr(ptr) {}
    const_iterator(): mPtr(0) {}
    ~const_iterator() {}

    const_iterator(const const_iterator& src): mPtr(src.mPtr) {}
    const_iterator& operator=(const const_iterator& src)
    {
      mPtr = src.mPtr;
      return *this;
    }

    bool operator==(const const_iterator& i) const {return i.mPtr == mPtr;}
    bool operator!=(const const_iterator& i) const {return i.mPtr != mPtr;}
    int operator-(const const_iterator& i) const {return mPtr - i.mPtr;}
    const_iterator operator-(int k) const {return const_iterator(mPtr - k);}
    const_iterator operator+(int k) const {return const_iterator(mPtr + k);}
    const_iterator operator++() {++mPtr; return *this;}
    const_iterator operator--() {--mPtr; return *this;}
    const_iterator operator++(int) {const_iterator prev(*this); ++mPtr; return prev;}
    const_iterator operator--(int) {const_iterator prev(*this); --mPtr; return prev;}
    reference operator*() const {return *mPtr;}
    bool operator<(const const_iterator& i) const {return mPtr < i.mPtr;}
    pointer operator->() {return &(operator*());}
    const pointer operator->() const {return &(operator*());}
    const_iterator operator+=(int k) {mPtr += k; return *this;}
    const_iterator operator-=(int k) {mPtr -= k; return *this;}

    const _Obj* mPtr;
  };

  class iterator: public random_access_iterator<_Obj, size_t>
  {
  public: CARBONMEM_OVERRIDES
    typedef _Obj value_type;
    typedef _Obj& reference;
    typedef ptrdiff_t difference_type;
    typedef const _Obj* pointer;

    iterator(_Obj* ptr): mPtr(ptr) {}
    iterator(): mPtr(0) {}
    ~iterator() {}

    iterator(const iterator& src): mPtr(src.mPtr) {}
    iterator& operator=(const iterator& src)
    {
      mPtr = src.mPtr;
      return *this;
    }

    bool operator==(const iterator& i) const {return i.mPtr == mPtr;}
    bool operator!=(const iterator& i) const {return i.mPtr != mPtr;}
    int operator-(const iterator& i) const {return mPtr - i.mPtr;}
    iterator operator-(int k) const {return iterator(mPtr - k);}
    iterator operator+(int k) const {return iterator(mPtr + k);}
    iterator operator++() {++mPtr; return *this;}
    iterator operator--() {--mPtr; return *this;}
    iterator operator++(int) {iterator prev(*this); ++mPtr; return prev;}
    iterator operator--(int) {iterator prev(*this); --mPtr; return prev;}
    reference operator*() const {return *mPtr;}
    bool operator<(const iterator& i) const {return mPtr < i.mPtr;}
    pointer operator->() {return &(operator*());}
    const pointer operator->() const {return &(operator*());}
    iterator operator+=(int k) {mPtr += k; return *this;}
    iterator operator-=(int k) {mPtr -= k; return *this;}

    operator const_iterator() const {return const_iterator(mPtr);}
//    operator const UtVector<_Obj>::const_iterator() const {
//      return UtVector<_Obj>::const_iterator(mPtr);
//    }

    _Obj* mPtr;
  };

  iterator begin() {return iterator(_Base::begin());}
  iterator end() {return iterator(_Base::end());}
  const_iterator begin() const {return const_iterator(_Base::begin());}
  const_iterator end() const {return const_iterator(_Base::end());}

  void erase(const iterator& p, const iterator& q)
  {
    _Base::erase(p.mPtr, q.mPtr);
  }
  void erase(const iterator& p)
  {
    _Base::erase(p.mPtr);
  }
  template<class _Iter>
  void insert(iterator pos, _Iter i1, _Iter i2) {
    _Base::insert(pos.mPtr, i1, i2);
  }
  void insert(iterator pos, _Base::value_type &v) {
    _Base::insert(pos.mPtr, v);
  }
#endif
};

#endif
