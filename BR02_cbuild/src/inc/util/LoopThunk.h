// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _LOOPTHUNK_H_
#define _LOOPTHUNK_H_


#include "util/StlIterator.h"

//! LoopThunk template class
/*!
  This class provides a mechanism for a class to expose iteration over
  the elements yielded by multiple homogeneous sub-loops.  If you need
  heterogeneous loops then you must wrap them all with Iter<value_type>.

  This class is templatized with an _OuterLoop class that iterates
  over a collection of intermediate objects (_ObjectType), plus an
  _InnerLoop class, that iterates over a collection of the desired
  objects (value_type).  The third template argument is a functor
  class, _LoopGen, that generates an _InnerLoop given from an
  _ObjectType.

  An example is the best way to clarify this.  Let's say you have
  a UtList<module*>, and each module has a UtList<net*>.  If you want
  to iterate over all the nets of all the modules, then you would
  supply Loop<UtList<module*> > for the _OuterLoop, and _Loop<UtList<net*> >
  for the inner loop.  The _LoopGen class would have a single
  operator() method which returns an _InnerLoop given an _ObjectType.

  \verbatim
  Example:

  struct NetLoopExtractor {NUModule::NetLoop operator()(NUModule* mod) {
    return mod->loopLocals();
  }};

  struct NetLoopGen
  {
    Loop<UtList<net*> > operator()(module* mod) {return mod->loopNets();}
  };

  typedef LoopThunk<Loop<UtList<module*> >,
                    Loop<UtList<net*> >,
                    NetLoopGen> NetLoop;
  \endverbatim

  LoopThunks are constructed with an _OuterLoop, and optionally a
  _LoopGen.  The _LoopGen class must have an default constructor
  in order to support a default constructed LoopThunk.  But if
  a working _LoopGen requires a constructor with an argument, then
  you must can pass in a pre-constructed _LoopGen as to the constructor
  of LoopThunk.

  _LoopGen must support copy-construction and assignment.
*/

template <class _OuterLoop, class _InnerLoop, class _LoopGen>
class LoopThunk
{
public: CARBONMEM_OVERRIDES
  typedef typename _OuterLoop::value_type _ObjectType;
  typedef typename _InnerLoop::value_type value_type;
  typedef typename _InnerLoop::reference reference;
  typedef StlIterator<LoopThunk> iterator;

  //! Default constructor
  LoopThunk()
  {
  }

  //! Constructor with explicit extractor instance
  LoopThunk(_OuterLoop loops, _LoopGen extractor)
    : mOuterLoop(loops), mLoopGen(extractor)
  {
    nextObj();
  }

  //! Constructor for use with implicitly created extractor
  LoopThunk(_OuterLoop loops)
    : mOuterLoop(loops)
  {
    nextObj();
  }

  //! Copy constructor to help the user manage & pass around Loops.
  LoopThunk(const LoopThunk& src):
    mOuterLoop(src.mOuterLoop),
    mCurrent(src.mCurrent),
    mLoopGen(src.mLoopGen),
    mEmpty(src.mEmpty)
  {
  }

  //! Assignment operator to help the user manage & pass around Loops.
  LoopThunk& operator=(const LoopThunk& src)
  {
    if (&src != this)
    {
      mOuterLoop = src.mOuterLoop;
      mCurrent = src.mCurrent;
      mLoopGen = src.mLoopGen;
      mEmpty = src.mEmpty;
    }
    return *this;
  }

  void operator++()
  {
    INFO_ASSERT(!mCurrent.atEnd(), "Cannot advance past the end of an iterator.");
    ++mCurrent;
    while (mCurrent.atEnd() && !mOuterLoop.atEnd())
      nextObj();
  }

  reference operator*()
  {
    return *mCurrent;
  }

  bool atEnd() const
  {
    return mEmpty || mCurrent.atEnd();
  }

  //! combine *, ++, and atEnd()
  bool operator()(value_type* ptr)
  {
    if (atEnd())
      return false;
    *ptr = *mCurrent;
    ++(*this);
    return true;
  }

  //! The start of the loop.
  iterator begin()
  {
    return iterator(this);
  }

  //! The tail of the loop
  iterator end()
  {
    return iterator(NULL);
  }

  //! Return the number of elements remaining -- note:  order N operation
  size_t size() const
  {
    size_t s = 0;
    for (LoopThunk tmp = *this; !tmp.atEnd(); ++tmp)
      ++s;
    return s;
  }

private:
  //! Find the next object to iterate over.
  void nextObj()
  {
    mEmpty = false;
    bool done = false;
    while (not done) {
      if (mOuterLoop.atEnd()) {
        mEmpty = true;
        done = true;
      } else {
        // Use the operator() method to advance, so that we do not have
        // to copy _ObjectType (which may have something complex like a map).
        const _ObjectType *subobj_iter;
        if (mOuterLoop(&subobj_iter)) {
          mCurrent = mLoopGen(*subobj_iter);
          // If this current is empty, then try the next outer.
          // The mOuterLoop operator() call has already advanced mOuterLoop.
          done = not mCurrent.atEnd();
        } else {
          // Should never happen; atEnd() was tested previously.
          INFO_ASSERT(0, "mOuterLoop's operator() returned false but atEnd() returned false");
        }
      }
    }
  }

  _OuterLoop mOuterLoop;
  _InnerLoop mCurrent;
  _LoopGen mLoopGen;
  bool mEmpty;
};

#endif //  _LOOPTHUNK_H_
