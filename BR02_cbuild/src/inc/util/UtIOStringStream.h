// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtIOStringStream_h_
#define __UtIOStringStream_h_


/*!
  \file
  Carbon representation for VHDLs LINE variable. It behaves like a FIFO buffer.
*/

#include "util/CarbonTypes.h"
#include "util/UtString.h"
#ifndef __UtIO_h_
#include "util/UtIO.h"
#endif
class UtOStringStream;
class UtIStringStream;
class UtICheckpointStream;
class UtOCheckpointStream;

class UtIOStringStream: public UtIStringStream
{

public: CARBONMEM_OVERRIDES
  UtIOStringStream(): mOutput(&mOutBuf) {}

  // constructor for checkpoint restore
  UtIOStringStream(UtICheckpointStream& in);

  virtual ~UtIOStringStream() {}

  //! override readFromFile, which is mostly empty in UtIStringStream
  virtual UInt32 readFromFile(char*, UInt32);

  //! Grab all the operator<< functionality from the output side
  template<class T> UtOStream& operator<<(const T& var) {
    mEOF = false;
    return mOutput << var;
  }

  void reset();
  void insert(const char*, UInt32);

  // save state to checkpoint file
  virtual bool save(UtOCheckpointStream& out);

  static void save(UtOCheckpointStream &out, UtIOStringStream **ppObj) {
    (*ppObj)->save(out);
  }

  static void restore(UtICheckpointStream &in, UtIOStringStream **ppObj) {
    *ppObj = new UtIOStringStream(in);
  }

private:
  UtString mOutBuf;
  UtOStringStream mOutput;
};



#endif // __UtIOStringStream_h_
