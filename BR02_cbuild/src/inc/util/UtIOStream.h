// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtIOStream_h_
#define __UtIOStream_h_


/*!
  \file
  Carbon replacement for C++ IOStreams, which is a very thin wrapper
  over C stdio.  The rationale here is that IOStream implementations
  are specific to the C++ compiler, and our users may wish to use a
  different C++ compiler from the one we are using.  If we rely on
  our compilers runtime IOStream implementation there will be clashes
  if the user tries to use a different C++ compiler.

  This doesn't replace any sophisticated formatting done in the iostream
  style.  That will have to be hacked.  This is meant to simplify the
  random uses of UtIO::cout() << "foo" << x << y ...;
*/

#include "util/CarbonTypes.h"
#include "util/UtString.h"
#ifndef __UtIO_h_
#include "util/UtIO.h"
#endif

class UtOStream;
class UtFileBuf;
class CodeAnnotation;

//! Output stream class
/*!
  This is to be used in place of std::ostream.
  This should only be used as a formatter for the derived classes.
*/
class UtOStream: public UtIOStreamBase
{
public: CARBONMEM_OVERRIDES
  //! constructor
  UtOStream();
  //! virtual destructor
  virtual ~UtOStream();
  //! write a UtString
  UtOStream& operator<<(const UtString& s);
  //! Write a string
  UtOStream& operator<<(const char* s);
  //! Formt bit-vector based on a UInt32* and num bits and send to stream
  bool formatBignum(const UInt32* data, UInt32 numBits);
  //! Write a BitVector or DynBitVector to stream
  template<class BV> bool formatBitvec(const BV& bv) {
    return formatBignum(bv.getUIntArray(), bv.size());
  }
  //! Write a UInt64
  UtOStream& operator<<(UInt64 num);
  //! write an SInt64
  UtOStream& operator<<(SInt64 num);
  //! Write a UInt32
  UtOStream& operator<<(UInt32 num);
  //! Write an SInt32
  UtOStream& operator<<(SInt32 num);
  //! Write a UInt16
  UtOStream& operator<<(UInt16 num);
  //! Write an SInt16
  UtOStream& operator<<(SInt16 num);
  //  UtOStream& operator<<(int num);
  //  UtOStream& operator<<(unsigned int num);
  //! Write a double
  UtOStream& operator<<(double num);
  //! Write a char
  UtOStream& operator<<(const char ch);
  //! Write a void* as a hex number
  UtOStream& operator<<(void* ptr);
  //! Write a const void* as a hex number
  UtOStream& operator<<(const void* ptr);
  //! Set the radix
  UtOStream& operator<<(UtIO::Radix radix);
  //! Set the floatmode
  UtOStream& operator<<(UtIO::FloatMode floatMode);
  //! Set the width
  UtOStream& operator<<(UtIO::Width w);
  //! Set the max width
  UtOStream& operator<<(UtIO::MaxWidth w);
  //! Set the precision
  UtOStream& operator<<(UtIO::Precision p);
  //! set so the positive sign is shown
  UtOStream& operator<<(UtIO::ShowPositve);
  //! Set the fill char
  UtOStream& operator<<(UtIO::Fill f);
  //! Flush the buffer
  UtOStream& operator<<(UtIO::Flush f);
  //! Insert newline (and don't flush the buffer)
  UtOStream& operator<<(UtIO::Endl);
  //! determine if we left/right/internal justify
  UtOStream& operator<<(UtIO::FieldMode fieldMode);
  //! control slashification of output strings
  UtOStream& operator<<(UtIO::Slashification);
  //! control numeric formats for C/C++ parsing
  UtOStream& operator<<(UtIO::ParsibleFormat);
  //! write an annotation
  UtOStream& operator<<(const CodeAnnotation &);
  

  //! Get the error messages for a failed operator<< 
  /*! The << operators can fail in scenarios including:
   *!   - out of disk space
   *!   - other network error
   *!   - when the stream was not successfully opened
   *! But operator<< has no mechanism to report error messages.
   *! All the error messages between calls to getErrmsg are
   *! accumulated in this string, separated by newlines.  After
   *! this routine is called, the message buffer resets.  The string
   *! returned by this call is valid until the next call to
   *! getErrmsg.
   */
  virtual const char* getErrmsg();

  //! Write a buffer to the stream, returning false if an error is discovered
  /*! Note that an error may not be discovered until a later call to write
   *! or an explicit call to flush(), due to buffering.  The message text
   *! is accessed by getErrmsg()
   */
  virtual bool write(const char* buf, UInt32 len) = 0;

  //! Write an annotation to the output stream
  /*! The base class UtOStream class does not track file line and column so this is a nop */
  virtual void write (const CodeAnnotation &) {}

  //! Default implementation of bad
  virtual bool bad() const;

  //! get the filename associated with this stream, or NULL if none defined yet
  /*! This is really for derived classes that do define a filename
   * since UtOStream does not have a filename
   */
  virtual const char * getFilename() const;

  //! get the current lineno for this stream
  /*! This is only implemented in the CodeStream subclass
   */
  virtual UInt32 getLineno () const { return 0; }

  //! Explicit call to flush the buffer
  virtual bool flush() = 0;

  //! record an error, accessible by getErrmsg
  void reportError(const char* errmsg);

  //! \return iff we are at the start of a line
  /*! \note The base method always returns false. isStartOfLine is implemented
       only in the CodeStream subclass */
  virtual bool isStartOfLine () const { return false; }

  //! Output statistics on the stream
  virtual void print (UtOStream *) const;
  virtual void pr () const;

protected:
  bool writeHelper(const char* buf, UInt32 len, OSStdio::fwrite_typedef pwritefunc, FILE* file);
  bool flushHelper(OSStdio::fflush_typedef pflushfunc, FILE* file);

private:
  UtOStream (const UtOStream&);
  UtOStream& operator=(const UtOStream&);

  /*! \brief determines the prefix/suffix, how long a prefix/suffix, and how much of \a buf should
   * be displayed based on current settings of mWidth, mField and mMaxWidth
   * \a val_len must be the length of the data in buf upon entry (buf
   * is not necessarily a null terminated c string).
   * the values returned by this method (in prefix_ptr, val_ptr, suffix_ptr) are only valid until the next
   * call to this method, or buf is changed
   */
  void calculatePaddingAndDataSizes(const char * buf, const char ** prefix_ptr, size_t * prefix_len, const char ** val_ptr, size_t * val_len, const char ** suffix_ptr, size_t* suffix_len);

  /*! \brief internal method, does the guts of the inserter '<<',
   *  \a num_chars must specify the number of chars in buf (this allows
   *  for the printing of nulls within char*).
   */
  void inserterHelper(const char * buf, size_t num_chars);

  //! error text to be returned by the next call to getErrmsg
  UtString* mErrmsg;
  //! error text previously returned by getErrmsg()
  UtString* mPrevErrmsg;

  //! a pattern of characters used for filling
  UtString* mfillPatternOther;
};

//! Class to output to FILE*
/*!
  This class does not own the FILE* and will not close it upon
  deletion, although it will close it on an explicit call to close.
*/
class UtOFileStream: public UtOStream
{
  UtOFileStream (const UtOFileStream&);
  UtOFileStream& operator=(const UtOFileStream&);

public: CARBONMEM_OVERRIDES
  //! Constructor for general FILE*
  UtOFileStream(FILE* f);

  //! Get the FILE*
  FILE* getFile();
  
  //! UtOStream::is_open()
  virtual bool is_open() const;
  //! UtOStream::close()
  virtual bool close();
  //! UtOStream::flush()
  virtual bool flush();

  //! Called by UtOStream
  virtual bool write(const char* s, UInt32 size);

protected:
  //! Default constructor for derived classes
  UtOFileStream();
  //! The FILE*
  FILE* mFile;
};

//! Class that owns the FILE*
class UtOFStream: public UtOFileStream
{
public:
  CARBONMEM_OVERRIDES

  //! Opens the file
  UtOFStream(const char* fileName);
  //! will close the file.
  ~UtOFStream();
private:
  UtOFStream(const UtOFStream&); // forbid
  UtOFStream& operator=(const UtOFStream&); // forbid
};

//! StringStream to replace standard string stream
class UtOStringStream: public UtOStream
{
  UtOStringStream (const UtOStringStream&);
  UtOStringStream& operator=(const UtOStringStream&);

public: CARBONMEM_OVERRIDES
  //! Constructor for use with a UtString
  UtOStringStream(UtString* f);

  virtual bool flush();
  //! True if the file/buffer is open
  virtual bool is_open() const;
  //! Close the file/buffer
  virtual bool close();

  //! Called by UtOStream
  virtual bool write(const char* s, UInt32 size);

private:
  UtString* mStr;
};

//! Buffered output stream to replace FILE* -- this is interrupt-safe
class UtOBStream : public UtOStream
{
  UtOBStream (const UtOBStream &);
  UtOBStream& operator=(const UtOBStream&);

public: CARBONMEM_OVERRIDES
  enum {cDefaultBufferSize = 64 * 1024};
  enum OpenMode {
    eOBStreamWriteMode,
    eOBStreamAppendMode
  };
  //! Constructor - opens the file.  Default buffer size is really 64k
  UtOBStream(const char* fileName, const char* mode = "w", UInt32 bufSize = cDefaultBufferSize);

  //! Constructor - restores file from checkpoint.
  UtOBStream(UtICheckpointStream &in);

  //! Destructor - closes the file
  ~UtOBStream();

  //! UtOStream::is_open()
  virtual bool is_open() const;
  //! UtOStream::close() returns false if error
  virtual bool close();
  //! UtOStream::flush() returns false if error
  virtual bool flush();

  //! How many times have we had to write to the physical file?
  UInt32 getNumWrites() const {return mNumWrites;}

  //! get the filename associated with this stream, or empty string if none defined yet
  virtual const char * getFilename() const;

  bool writeStr(const char* s, UInt32 size);
  
  //! save state of stream to checkpoint file.
  virtual bool save(UtOCheckpointStream &);

  //! Called by UtOStream
  virtual bool write(const char* s, UInt32 size);

protected:
  //! open the file descriptor if it is currently closed
  bool open(bool firstTime);

  //! Overridden by UtOCStream
  virtual bool getFD();

  //! restore state of stream from checkpoint file.
  virtual bool restore(UtICheckpointStream &);

  // level 1 file descriptor
  int mFD;

private:
  UtFileBuf* mBuffer;
  UInt32 mNumWrites;            // just for diagnostics
  UtString mFilename;
  OpenMode mOpenMode;
};

class UtCachedFileSystem;

//! UtOCStream class -- buffered output-stream that shares file descriptors
class UtOCStream: public UtOBStream
{
  UtOCStream (const UtOCStream&);
  UtOCStream& operator=(const UtOCStream&);

public: CARBONMEM_OVERRIDES
  //! ctor -- opens the file
  UtOCStream(const char* filename, UtCachedFileSystem*, const char* mode,
             UInt32 bufSize = UtOBStream::cDefaultBufferSize);

  //! Constructor - restores file from checkpoint.
  UtOCStream(UtICheckpointStream &in, UtCachedFileSystem* sys);

  //! destructor -- closes the file
  ~UtOCStream();

  //! override explicit close to keep track of how we will report is_open
  virtual bool close();

  //! Override is_open because we don't need to know if we are physically
  //! open -- we just need to know if we have *ever* been opened
  virtual bool is_open() const;

protected:
  //! Overrides UtOBStream::getFD to refresh our file descriptor
  virtual bool getFD();

private:
  friend class UtCachedFileSystem;    // allow this class to call invalidFD()

  //! release the physical file for use by another UtOCStream
  /*! called only by UtOCStreamSystem */
  void releaseFD();

  //! Does this cached file have a currently-active file descriptor?
  bool hasActiveFD() const;

  //! What was the file-cache timestamp at the time of the last physical Write?
  virtual UInt32 getTimestamp() const;

  //! code common to all constructors
  void initialize(UtCachedFileSystem* sys);

  UtCachedFileSystem* mFileCache;
  bool mIsValid;                // Cannot rely on mFD
  UInt32 mTimestamp;
}; // class UtOCStream: public UtOBStream

#endif // __UtIOStream_h_
