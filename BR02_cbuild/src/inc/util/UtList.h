// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtList_h_
#define __UtList_h_
#ifndef __MemManager_h_

#include "util/MemManager.h"
#endif

#include <list>

//! alternate STL list definition that uses the Carbon memory allocator
template<class _Obj>
class UtList: public std::list<_Obj, CarbonSTLAlloc<_Obj> >
{
  typedef std::list<_Obj, CarbonSTLAlloc<_Obj> > _Base;
public:  CARBONMEM_OVERRIDES

  template<class _Iter> UtList(_Iter _begin, _Iter _end):
    _Base(_begin, _end)
    {}

  UtList() {}

#if pfGCC_2
//  friend bool operator== __STL_NULL_TMPL_ARGS (
//    const UtList& __x, const UtList& __y);
#endif
};

#endif
