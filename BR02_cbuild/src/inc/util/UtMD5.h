// -*- C++ -*-
/*
* This code implements the MD5 message-digest algorithm.
* The algorithm is due to Ron Rivest.  This code was
* written by Colin Plumb in 1993, no copyright is claimed.
* This code is in the public domain; do with it what you wish.
*
* Equivalent code is available from RSA Data Security, Inc.
* This code has been tested against that, and is equivalent,
* except that you don't need to include two pages of legalese
* with every copy.
*
* To compute the message digest of a chunk of bytes, declare an
* MD5Context structure, pass it to MD5Init, call MD5Update as
* needed on buffers full of bytes, and then call MD5Final, which
* will fill a supplied 16-byte array with the digest.
*/
#ifndef __UtMD5_h_
#define __UtMD5_h_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#include "util/CarbonTypes.h"

class UtString;
class ZostreamDB;
class ZistreamDB;

class UtMD5
{
public: CARBONMEM_OVERRIDES
  //! Constructor
  /*!
    This initializes the md5 hash
  */
  UtMD5();

  //! Destructor
  ~UtMD5();
  
  //! Append buffer of given size to hash
  /*!
    This simply takes the buffer and its length and updates the hash.
  */
  void update(const char* buf, UInt32 len);
  
  //! Append string buffer to hash
  /*!
    Appends the string to the hash
  */
  void update(const UtString& buf);

  //! Finishes the hash and returns the digest
  /*!
    Pass in the allocated digest buffer and it will be filled with the
    computed hash of the given buffer(s).
  */
  void finalize(unsigned char digest[16]);

  //! Return the digest for a file
  /*!
    This opens the given filename, and fills the digest variable with
    the md5sum. if an error occurred, the reason is put into the error
    string.
    \returns true if successful, false if unsuccessful (file i/o problem)
  */
  bool sumFile(const char* filename, unsigned char digest[16], UtString* errMsg);

  //! Writes the srcFile digest to a crbn file
  /*!
    This is used by cmd5sum and cbuild to identify carbon-owned files
  */
  bool dbWriteCrbn(ZostreamDB& out, const char* srcFile);

  //! Read the digest from a crbn file
  /*!
    This is used by cmd5sum and cbuild to identify carbon-owned files
  */
  bool dbReadCrbn(ZistreamDB& in, unsigned char digest[16]); 

  //! Compare two digests. Returns true if they are equal
  static bool compareDigests(const unsigned char digest1[16],
                             const unsigned char digest2[16]);
private:
  
  struct MD5Context {
    UInt32 buf[4];
    UInt32 bits[2];
    unsigned char in[64];
  };

  MD5Context mContext;
  
  void MD5Init();
  void MD5Update(unsigned char const *buf,
                 UInt32 len);
  void MD5Final(unsigned char digest[16]);
  void MD5Transform(UInt32 buf[4], UInt32 const in[16]);
};

#endif /* !__UtMD5_h_ */
