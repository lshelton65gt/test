// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file

  This file contains a utility that can transform from one
  GenericDigraph to another GenericDigraph. It does not require a
  GenericDigraph but is does require the types SrcGraph::NodeDataType,
  SrcGraph::EdgeDataType, DstGraph::NodeDataType, and
  DstGraph::EdgeDataType. These are provided by GenericDigraph.

  This class uses the GraphBuilder class to do its work
*/

#ifndef _GRAPHTRANSFORM_H_
#define _GRAPHTRANSFORM_H_

#include "util/GraphBuilder.h"

//! Implements a class to transform one graph to another
template<typename SrcGraph, typename DstGraph>
class UtGraphTransform
{
private:
  typedef typename SrcGraph::Node SrcGraphNode;
  typedef typename SrcGraph::Edge SrcGraphEdge;
  typedef typename SrcGraph::NodeDataType SrcNodeDataType;
  typedef typename SrcGraph::EdgeDataType SrcEdgeDataType;
  typedef typename DstGraph::NodeDataType DstNodeDataType;
  typedef typename DstGraph::EdgeDataType DstEdgeDataType;

public:
  //! Constructor
  UtGraphTransform(SrcGraph* srcGraph) : mSrcGraph(srcGraph)
  {
    mBuilder = new GraphBuilder<DstGraph>;
  }

  //! Destructor
  virtual ~UtGraphTransform() { delete mBuilder; }

  //! Override this function to map from src node data to dst node data
  /*! Converts the src node data and flags to data and flags for the
   *  dst graph node
   *
   *  \param srcNodeData The data from the src graph
   *
   *  \param dstNodeData A place to store the data for the dst graph
   *  node
   *
   *  \param flags A pointer to flags for the graph node. It is
   *  initialized with the data from the src graph node. It will be
   *  used to write the dst graph so update it if it should be
   *  different.
   */
  virtual void mapNodeData(SrcNodeDataType srcNodeData, 
                           DstNodeDataType* dstNodeData,
                           UInt32*) = 0;

  //! Override this function to map from src edge data to dst edge data
  /*! Converts the src edge data and flags to data and flags for the dst
   *  graph edge
   *
   *  \param srcEdgeData The edge data from the src graph
   *
   *  \param dstEdgeData A place to store the data for the dst graph
   *  edge
   *
   *  \param flags A pointer to flags for the graph node. It is
   *  initialized with the data from the src graph node. It will be
   *  used to write the dst graph so update it if it should be
   *  different.
   *
   *  \returns Whether this edge should be added uniquely or not. If
   *  true, only unique edges are added, otherwise all edges are
   *  added.
   */
  virtual bool mapEdgeData(SrcEdgeDataType srcEdgeData,
                           DstEdgeDataType* dstEdgeData,
                           UInt32*) = 0;

  //! Function to transform the graph
  DstGraph* transform()
  {
    // Create the nodes and edges. The nodes are created on
    // demand. The graph builder guarantees we don't create
    // duplicates. See translateNode.
    for (Iter<GraphNode*> n = mSrcGraph->nodes(); !n.atEnd(); ++n) {
      // translate from the src graph to the dst graph
      GraphNode* graphNode = *n;
      DstNodeDataType nodeData = translateNode(graphNode);
   
      // Create the edges
      for (Iter<GraphEdge*> e = mSrcGraph->edges(graphNode); !e.atEnd(); ++e) {
        // Translate the edge data
        GraphEdge* edge = *e;
        DstEdgeDataType dstEdgeData;
        SrcGraphEdge* srcEdge = mSrcGraph->castEdge(edge);
        SrcEdgeDataType srcEdgeData = srcEdge->getData();
        UInt32 flags = mSrcGraph->getFlags(edge);
        bool unique = mapEdgeData(srcEdgeData, &dstEdgeData, &flags);
        
        // Translate the to node for this edge and add it
        GraphNode* graphToNode = mSrcGraph->endPointOf(edge);
        DstNodeDataType toNodeData = translateNode(graphToNode);
        if (unique) {
          mBuilder->addEdgeIfUnique(nodeData, toNodeData, dstEdgeData, flags);
        } else {
          mBuilder->addEdge(nodeData, toNodeData, dstEdgeData, flags);
        }
      }
    } // for

    return mBuilder->getGraph();
  } // void transform

private:
  //! Helper function to create/find a node in the dst graph
  /*! The node can be translated on demand in two spots in the
   *  transform routine. It is translated as part of the outer for
   *  loop and also for the to node of an edge. This routine factors
   *  out the common code.
   */
  DstNodeDataType translateNode(GraphNode* node)
  {
    // Get the data for this node in the src graph
    SrcGraphNode* srcNode = mSrcGraph->castNode(node);
    SrcNodeDataType srcData = srcNode->getData();

    // Translate it and build a new node
    UInt32 flags = mSrcGraph->getFlags(srcNode);
    DstNodeDataType dstNodeData;
    mapNodeData(srcData, &dstNodeData, &flags);
    mBuilder->addNode(dstNodeData, flags);

    // Return the node data
    return dstNodeData;
  }
    
  //! The src graph that is translated
  SrcGraph* mSrcGraph;

  //! The builder for the destination graph
  GraphBuilder<DstGraph>* mBuilder;
}; // class UtGraphTransform

#endif // _GRAPHTRANSFORM_H_
