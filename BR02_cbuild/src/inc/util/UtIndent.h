// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _UT_INDENT_H_
#define _UT_INDENT_H_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

class UtString;

//! UtIndent class -- helps line up columns in formatted text sent to a string
class UtIndent
{
public: CARBONMEM_OVERRIDES
  //! constructor
  UtIndent(UtString* buf);

  //! emit a newline into the string and reset column
  void newline();

  //! indent to the specified column -- return false if no spaces added
  bool tab(const SInt32 targetColumn);

  //! indent to the standard column for Loc(ation) comments
  bool tabToLocColumn(const SInt32 targetColumn = 50)
  {
    return tab(targetColumn);
  }

  //! clear the start-of-last line position only
  /*!
    Useful if you are passing in a buffer that has stuff already in
    it, but you don't want that to be considered the beginning of the
    indent. LangCppStmtTree uses this.
  */
  void clearLastLinePos() { mPosAtStartOfLastLine = 0; }
  
  //! Clear the string
  void clear();

  //! Get the current length since the last line.
  size_t getCurLineLength() const;

  //! Get the buffer
  UtString* getBuffer() { return mBuf; }

private:
  UtString* mBuf;
  size_t mPosAtStartOfLastLine;
};

#endif
