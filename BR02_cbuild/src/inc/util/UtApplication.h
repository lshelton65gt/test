// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#ifndef __UtApplication_h_
#define __UtApplication_h_



#ifndef __ArgProc_h_
#include "util/ArgProc.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef __UtString_h_
#include "util/UtString.h"
#endif

class MsgContext;
class MsgStreamIO;


class UtApplication
{
public: CARBONMEM_OVERRIDES
  //! Constructor.
  /*!
    Construct a top level application object.
    \param argc Number of command line arguments.
    \param argv Array of command line arguments.
   */
  UtApplication (int* argc, char **argv);

  //! Destructor.
  virtual ~UtApplication(void);

  //! Run the application.
  /*!
    Run the application.
    \return The exit status of program.
  */
  virtual SInt32 run(void) = 0;

  //! The name of the installation directory.
  const UtString & getInstallDir(void) const;

  //! Get the argument processor
  ArgProc* getArgProc();

 protected:
  //! Determine where the installation directory is.
  void setInstallDir(void);

  ArgProc mArgs;
  int* mArgc;
  char **mArgv;
  UtString mInstallDir;
  MsgStreamIO *mErrorStream;
  MsgContext *mMsgContext;

 private:
  UtApplication(void);
  UtApplication(const UtApplication&);
  UtApplication& operator=(const UtApplication&);
};
#endif
