// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  Contains an algorithm for computing strongly-connected components of a graph
  and related information, like component graphs.
*/

#ifndef _GRAPHSCC_H_
#define _GRAPHSCC_H_



#include "util/Graph.h"
#include "util/GenericDigraph.h"
#include "util/UtStack.h"
#include "util/UtList.h"
#include "util/Loop.h"

/* The strongly-connected components algorithm implemented here is an adaptation of
 * Trajan's classic algorithm.
 */

/*!
  A derived class of GraphWalker specialized to compute the strongly connected components
  of a directed graph.
 */
class GraphSCC : public GraphWalker
{
public:  CARBONMEM_OVERRIDES
  // The class that represents a single connected component
  class Component
  {
  public:  CARBONMEM_OVERRIDES
    Component(bool isCyclic) : mNodes(), mIsCyclic(isCyclic) {};
    typedef CLoop<UtSet<GraphNode*> > NodeLoop;
    void addNode(GraphNode* node) { mNodes.insert(node); }
    NodeLoop loopNodes() const { return NodeLoop(mNodes); }
    bool isCyclic() const { return mIsCyclic; }
    size_t size() const { return mNodes.size(); }
    friend class GraphSCC;
  private:
    UtSet<GraphNode*> mNodes;
    bool mIsCyclic;
  };

  typedef CLoop<UtList<Component*> > ComponentLoop; //!< Type for looping over components

  //! Constructor
  GraphSCC();

  //! Analyze the graph
  void compute(Graph*g);

  //! Returns true if and only if the analyzed graph contained a cycle
  bool hasCycle() const { return (!mCyclicComponents.empty()); }

  //! Returns the number of strongly connected components in the graph
  int numComponents() const { return mComponents.size(); }

  //! Returns the number of cyclic components in the graph
  int numCycles() const { return mCyclicComponents.size(); }

  //! Iterates over all components
  /*! Note: you cannot assume that a component with only one node is acylic.
      It may have contained a self-edge!  If you only want cyclic components,
      use loopCyclicComponents().
   */
  ComponentLoop loopComponents() const;

  //! Iterates over only cyclic components
  ComponentLoop loopCyclicComponents() const;

  //! Extract the subgraph of nodes in the component
  Graph* extractComponent(Component* component, bool copyScratch = false) const;
  //! Abstraction for the component digraph
  typedef GenericDigraph<void*, Component*> CompGraph;

  /*! Returns a new GenericDigraph in which there is one node for each
    component in the original graph, each node's user data points to the
    component it represents, and the graph contains an edge between nodes
    if and only if there was at least one edge between nodes in the corresponding
    components in the original graph.
   */
  CompGraph* buildComponentGraph(Graph* g);

  //! Get the component from a graph node
  Component* getComponent(const GraphNode* node) const;

  ~GraphSCC();

private:
  // these are the visiting functions in which the SCC algorithm is implemented
  GraphWalker::Command visitNodeBefore(Graph* g, GraphNode* node);
  GraphWalker::Command visitNodeAfter(Graph* g, GraphNode* node);
  GraphWalker::Command visitTreeEdge(Graph* g, GraphNode* from, GraphEdge* edge);
  GraphWalker::Command visitBackEdge(Graph* g, GraphNode* from, GraphEdge* edge);
  GraphWalker::Command visitCrossEdge(Graph* g, GraphNode* from, GraphEdge* edge);

  //! An internal class used to hold algorithm state while enumerating SCCs
  class SCCRecord
  {
  public:  CARBONMEM_OVERRIDES
    SCCRecord(GraphNode* node, UInt32 discovery, GraphNode* parent=NULL)
      : mNode(node), mParent(parent), mDiscovery(discovery), mMinLink(discovery), mIsInCycle(false) {}
    void updateMinLink(UInt32 val) { if (val < mMinLink) mMinLink = val; }
    bool isRoot() const { return mDiscovery == mMinLink; }
    bool isInCycle() const { return mIsInCycle; } // only valid for roots
    void setInCycle() { mIsInCycle = true; }
    UInt32 minLink() const { return mMinLink; }
    GraphNode* node() const { return mNode; }
    GraphNode* parent() const { return mParent; }
  private:
    GraphNode* mNode;
    GraphNode* mParent;
    UInt32 mDiscovery;
    UInt32 mMinLink;
    bool mIsInCycle;
  };
private:
  // Types for templatized digraph classes
  typedef CompGraph::Edge CEdge;
  typedef CompGraph::Node CNode;

  // Delete all the components in this graph
  void cleanup();

  UInt32 mCount;                        //!< node discovery counter
  UtArray<SCCRecord*> mStack;
                                        //!< a stack of records repesenting nodes under consideration

  Graph* mOriginalGraph;                //!< the original graph components were computed from
  UtList<Component*> mComponents;       //!< the list of components built during the walk
  UtList<Component*> mCyclicComponents; //!< the list of cyclic components built during the walk

  CompGraph* mCG;                         //!< Component graph

  bool mIsCyclic;                       //!< set to true when a back edge is encountered
};

#endif // _GRAPHSCC_H_
