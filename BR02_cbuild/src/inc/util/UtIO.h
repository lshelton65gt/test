// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtIO_h_ 
#define __UtIO_h_ 

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#include "util/CarbonTypes.h"

class UtIStream;
class UtOStream;
class UtICheckpointStream;
class UtOCheckpointStream;


#ifdef cout
#undef cout
#endif
#ifdef cin
#undef cin
#endif
#ifdef cerr
#undef cerr
#endif

//! Utility class for UtOStream and UtIStream
/*!
  This is more of a namespace to mimick std::ios
*/
class UtIO
{
public: CARBONMEM_OVERRIDES

  //! Radix enumeration
  enum Radix {
    dec = 'u', //!< Unsigned decimal
    sdec = 'd', //!< Signed decimal
    hex = 'x', //!< hexadecimal
    HEX = 'X', //!< HEXADECIMAL
    oct = 'o', //!< Octal
    bin = 'b'  //!< Binary
  };
    
  //! FloatMode for double and float types
  enum FloatMode
  {
    exponent = 'e', //!< use exponent format  0.00e+00
    fixed = 'f',    //!< Fixed format         0.00
    general = 'g'   //!< Fixed or exponent format, whichever smaller
  };

  //! Standard endl manipulator
  enum Endl {
    endl = '\n' //!< newline
  };

  //! the type of adjustment within an output field
  enum FieldMode
  {
    right,                      // right justified (default)     '   123'
    left,                       // left justified                '123   '
    internal                    // filled between sign and value '   123'
  };

  //! Standard flush manipulator
  enum Flush
  {
    flush = 'f' //!< flush
  };
  
  enum ShowPositve
  {
    noshowpositive = false, //!< do not show plus sign
    showpositive   = true   //!< show plus sign
  };

  //! Slashification
  enum Slashification {
    raw,      //!< strings are emitted as is
    slashify, //!< strings are conditionally slashified, csh-style, so that any string that's printed can be parsed
    quotify   //!< strings are unconditionally quoted, csh-style, so that any string that's printed can be parsed
  };

  //! C/C++ Parse form
  enum ParsibleFormat {
    undecorated,       //!< numbers are emitted in human-friendly form
    iso_c              //!< numbers are tagged with 'u' as needed
  };

  //! Seek modes
  enum SeekMode {
    seek_cur,          //!< seek to the offset relative to the current position
    seek_set,          //!< seek to the specified absolute offset
    seek_end           //!< seek to the offset relative to the end of the file
  };

  //! Width manipulation
  struct Width
  {
    //! Width set
    Width(UInt32 w): mWidth(w) {}
    //! Width copy
    Width(const Width& src): mWidth(src.mWidth) {}

    //! Width member (minimum width)
    UInt32 mWidth;
  };

  //! MaxWidth manipulation
  struct MaxWidth
  {
    //! MaxWidth set
    MaxWidth(UInt32 w): mMaxWidth(w) {}
    //! MaxWidth copy
    MaxWidth(const MaxWidth& src): mMaxWidth(src.mMaxWidth) {}

    //! Max Width member (maximum width, 0 means unlimited)
    UInt32 mMaxWidth;
  };

  //! Fill manipulation
  struct Fill
  {
    //! Fill set
    Fill(char c): mFillChar(c) {}
    //! Fill copy
    Fill(const Fill& src): mFillChar(src.mFillChar) {}
    //! Fill member
    char mFillChar;
  };

  //! Precision manipulation
  struct Precision
  {
    //! precision set
    Precision(UInt32 p): mPrecision(p) {}
    //! precision copy
    Precision(const Precision& src): mPrecision(src.mPrecision) {}
    //! precision member
    UInt32 mPrecision;
  };

  //! access to standard error stream
  static UtOStream& cerr();
  //! access to standard out stream
  static UtOStream& cout();
  //! access to standard input stream
  static UtIStream& cin();

  //! Initialize stdout file pointer
  static void setStdout(FILE* file);
  //! Initialize stderr file pointer
  static void setStderr(FILE* file);

  //! Manipulate fill char
  static Fill setfill(char c) {return Fill(c);}
  //! Manipulate width
  static Width setw(UInt32 w) {return Width(w);}
  //! Manipulate max width (like width this only applies to next output operation)
  static MaxWidth setMaxw(UInt32 w) {return MaxWidth(w);}
  //! Manipulate precision
  static Precision setprecision(UInt32 p) {return Precision(p);}

  static void memCleanup();

  //! translate the seek-mode enum to the C stdlib SEEK_CUR/SEEK_SET/SEEK_END
  static int seekModeToInt(SeekMode);

private:
  UtIO();       // no public constructors -- this is just a namespace

  static void memCleanupIStream();
};

//! Base class for output streams
class UtIOStreamBase
{
public: CARBONMEM_OVERRIDES
  //! constructor
  UtIOStreamBase();

  //! destructor
  virtual ~UtIOStreamBase();

  //! Set the stream width
  void setwidth(int);

  //! True if the file/buffer is open, if false, sets errmsg
  virtual bool is_open() const = 0;
  //! Close the file/buffer
  virtual bool close() = 0;
  //! True if the file/buffer is in a bad state
  virtual bool bad() const = 0;
  //! override ! -- same as bad()
  inline bool operator!() const {return bad();}

  //! implement this for any stream that must be saved and restored.
  virtual bool save(UtOCheckpointStream &);

  //! this should only be called by UtCachedFileSystem
  virtual bool hasActiveFD() const;
  virtual UInt32 getTimestamp() const;
  virtual void releaseFD();

  //! return \a source if its value is simply true or false, if it is neither then return \a limited_value
  /*! this method is used for protection of arguments from calls from
   * within gdb  where the default args should have been used. Using
   * this method allows us to not modify the args themselves because
   * that can corrupt the stack.
   */
  static bool limitBoolArg(const bool& source, bool limited_value);
  //! return \a source if its value is between min/max values, else return \a limited_value
  /*! this method is used for protection of arguments from calls from
   * within gdb  where the default args should have been used. Using
   * this method allows us to not modify the args themselves because
   * that can corrupt the stack.
   */
  static int limitIntArg(const int& source, const int min_in_range_value, const int max_in_range_value, const int limited_value);

protected:
  //! String format
  char* sfmt(const char* type);
  //! unsigned decimal format
  char* ufmt(const char* type);
  //! double format
  char* dfmt();
  //! alphanumeric format
  char* afmt();
  //! char format
  char* cfmt();
  //! Initialize
  void init();
  //! Reset manipulators
  inline void reset();
  //! Reset fill manipulator
  inline void resetFill();
  //! Reset width manipulator
  inline void resetWidth();
  //! Reset width manipulator
  inline void resetMaxWidth();

  //! Signed radix tag
  UtIO::Radix mSignedRadix;
  //! Radix tag
  UtIO::Radix mRadix;
  //! Floatmode tag
  UtIO::FloatMode mFloatMode;
  //! Format string
  char mFormat[15];
  //! Width
  UInt32 mWidth;
  //! Max Width
  UInt32 mMaxWidth;
  //! Fill character
  char mFillChar;
  //! precision
  UInt32 mPrecision;
  //! if true show the + sign symbol
  bool mShowPositive;
  //! if true, slashify strings
  bool mSlashify;
  //! if true, quote all strings
  bool mQuotify;
  //! field
  UtIO::FieldMode mField;
  //! C-parsible numeric forms
  UtIO::ParsibleFormat mParseFormat;
} ;


#endif // __UtIO_h_ 
