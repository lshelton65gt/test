// -*- C++ -*-                
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __AtomicCache_h_
#define __AtomicCache_h_


#include <cstring>
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef _ITERMAP_H_
#include "util/IterMap.h"
#endif


/*!
  \file
  Declaration of a string table to be used as a string cache.
*/

class StringAtom;
class UtString;

//! Generic String Cache
/*!
  This object is an associative table of char* to StringAtom.
*/
class AtomicCache
{
private:
  //! less than operator object for char*
  class CString_less 
  {
  public: CARBONMEM_OVERRIDES

    //! called by associative table for compare
    bool operator() (const char* p, const char* q) const 
    {
      return std::strcmp(p, q) < 0;
    }
  };
  
public: CARBONMEM_OVERRIDES

  //! constructor
  AtomicCache();
  //! destructor
  ~AtomicCache();

  typedef IterMap<const char*, StringAtom*> CacheIter;
  
  //! Number of cache entries
  UInt32 size() const;

  //! Get iterator to iterate through StringAtoms
  /*!
    \warning The table cannot be modified while iterating. If it is,
    the iterator will become invalid and will produce undefined
    results.
  */
  CacheIter getCacheIter();

  //! Create StringAtom of a string of unknown length
  /*!
    \param str A string
    \returns StringAtom created for str
  */
  StringAtom* intern(const char* str);
  //! Create StringAtom of string with given length
  /*!
    \param str A string
    \param len Length of string (or less)
    \returns StringAtom created for str of length len
  */
  StringAtom* intern(const char* str, size_t len);

  //! Create StringAtom of UtString
  /*!
    \param str A string
  */
  StringAtom* intern(const UtString&);

  //! If the StringAtom already exists return it
  /*!
    \returns NULL if the string was not already interned; otherwise,
    the associated StringAtom.
  */
  StringAtom* getIntern(const char* str) const;

private:
  class StringCache;
  StringCache* mStringCache;
} ;

#endif
