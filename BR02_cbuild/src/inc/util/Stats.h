// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  A class to keep track of resource usage statistics
*/

#ifndef _STATS_H_
#define _STATS_H_


#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif

#ifndef __Util_h_
#include "util/Util.h"
#endif
#ifndef __UtString_h_
#include "util/UtString.h"
#endif
#ifndef _CARBON_HASH_VALUE_
#include "util/HashValue.h"
#endif

#include <stdio.h>
#include <cstddef>
#include <ctime>
#include <cstdlib>
#include <utility>              // for std::pair
#include <algorithm>            // for std::max

#if pfMSVC
#include "util/VCWindows.h"
#else
#if pfWINDOWS
#define NOGDI                   // To avoid macro conflicts
#include <windows.h>
typedef LARGE_INTEGER RealtimeType;
typedef struct { FILETIME user; FILETIME system; } CpuStruct;
#else
#include <sys/time.h>
#include <sys/times.h>

typedef timeval RealtimeType;
typedef struct tms CpuStruct;
#endif
#endif

// In windows, there appear to be #defines for min & max, and so we
// get syntax errors if we try to use std::min or std::max
#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif


double
computeRealTime(RealtimeType endTime, RealtimeType startTime);
double
computeUserTime(CpuStruct endTime, CpuStruct startTime);
double
computeSystemTime(CpuStruct endTime, CpuStruct startTime);

class AccumStats;
class UtOBStream;

struct StatsProgressRecord {
  const char* phaseName;
  double percent;
};

//! A resource statistics class
class Stats
{
public: CARBONMEM_OVERRIDES
  //! constructor
  Stats(FILE*);

  //! default constructor
  /*!
   * This one is safe to call from carbon_id where we might be using a different compiler
   * for the libcarbon runtime than we are using for the carbon simulation.
   */
  Stats ();
  //! destructor
  virtual ~Stats();

  //! Consolidated data we track
  class StatsData
  {
    friend class Stats;

  public: CARBONMEM_OVERRIDES
    //! Default constructor
    StatsData() :
      mReal(0),
      mUser(0),
      mSys(0),
      mSize(0),
      mAllocSize(0),
      mHiWater(0)
    {}

    //! Copy constructor
    StatsData(const StatsData& other)
    {
      if (this != &other) {
	mReal = other.mReal;
	mUser = other.mUser;
	mSys = other.mSys;
	mSize = other.mSize;
	mAllocSize = other.mAllocSize;
	mHiWater = other.mHiWater;
      }
    }

    //! Assign
    StatsData& operator=(const StatsData& other)
    {
      if (this != &other) {
	mReal = other.mReal;
	mUser = other.mUser;
	mSys = other.mSys;
	mSize = other.mSize;
	mAllocSize = other.mAllocSize;
	mHiWater = other.mHiWater;
      }
      return *this;
    }

    //! Plus operator sums the times, and for memory it takes the max
    StatsData& operator+=(const StatsData& other)
    {
      mReal += other.mReal;
      mUser += other.mUser;
      mSys += other.mSys;
      mSize = std::max(mSize, other.mSize);
      mAllocSize = std::max(mAllocSize, other.mAllocSize);
      mHiWater = std::max(mHiWater, other.mHiWater);
      return *this;
    }

    //! Get the real time
    double getRealTime() const { return mReal; }
    //! Get the user time
    double getUserTime() const { return mUser; }
    //! Get the system time
    double getSysTime() const { return mSys; }

  private:
    //! Real, user, system time
    double mReal;
    double mUser;
    double mSys;

    //! System-reported virtual memory size
    double mSize;

    //! Bytes currently allocated in the memory system
    unsigned long mAllocSize;

    //! High water memory mark
    unsigned long mHiWater;
  };

  //! Print the statistics since the beginning
  void printTotalStatistics();

  //! Push a new interval timer on the stack.
  /*! This should be used when a top level interval needs to be
   *  sub-timed. A push should not be done for the top level intervals
   *  because one is already done automatically by the constructor.
   */
  void pushIntervalTimer();

  //! Collect the statistics since the last call into data
  void getIntervalStatistics(StatsData* data);

  //! Collect the statistics since the last call into data
  /*!
   *! Do not update the 'current' interval with the collected
   *! data.  Call getIntervalStatistics or printIntervalStatistics
   *! to do that.
   */
  void peekIntervalStatistics(StatsData* data);

  //! Print the statistics for the current interval
  virtual void printIntervalStatistics(const char* intervalName, StatsData* intervalStats = NULL);

  //! Pop an interval timer off the stack. It is no longer needed.
  void popIntervalTimer();

  //! get the memory allocated by this process from the OS perspective, in Meg
  double getMemAlloced();

  //! Determine whether detailed phase statistics will be emitted
  void putEchoStats(bool echoStats) {mEchoStatistics = echoStats;}

  //! Determine whether progress reports will be emitted
  void putEchoProgress(bool echoProgress) {mEchoProgress = echoProgress;}

  //! Gather statistics into the named file
  bool gatherStatistics(const char* filename, UtString* errmsg);

  //! Load collated progress statistics from the structure
  bool loadStatistics(StatsProgressRecord* spr);

protected:
  //! Estimate percent complete and time remaining
  /*!
   *! These valuies are written into mEstimatedTimeRemaining and mPercentComplete.
   *! We use these member variables rather than return values from the method because
   *! if we are currently doing a phase that is not in the estimator databse, then
   *! we will just use the previous numbers.
   */
  bool findEstimatedTimeRemaining(const char* phaseName);

  // Let AccumStats get at mIntervalIndex for other stats structures.
  friend class AccumStats;

  // A structure and stack to keep the set of data used in statistics
  class Resources
  {
  public: CARBONMEM_OVERRIDES
    //! constructor with data
    Resources(RealtimeType startRealTime, CpuStruct startTime)
    {
      mStartRealTime = startRealTime;
      mStartTime = startTime;
    }

    //! Simple constructor
    Resources() {}

    RealtimeType mStartRealTime;
    CpuStruct mStartTime;
  };
  typedef UtStack<Resources, UtArray<Resources*> > ResourceStack;

  //! common construction code
  void constructor (void);

  //! Tally up current interval into the given data structure.
  void computeIntervalStatistics(StatsData* data,
				 const RealtimeType &start_real_time,
				 const CpuStruct &start_time,
                                 bool update = true);

  //! Prints a status line for a period of computation
  void printStats(UtOStream& out,
                  const char* interval,
                  int intervalIndex,
                  const StatsData &data);

  // start times for the total calculations
  RealtimeType mStartRealTime;
  CpuStruct mStartTime;

  // start time for the interval calculations
  ResourceStack* mIntervalStack;
  int mIntervalIndex;

  // Place to write output
  UtOStream& mOut;

  // Do we own mOut (and therefore need to delete it on destruction?)
  bool mOwnOut;

  // Used to compute virtual memory consumed
  void* mStartSbrk;

  //! The client *always* calls printIntervalStatistics.  This variable
  //! controls whether that results in I/O or not.  By default, cbuild
  //! will *not* echo the detailed phase-stats info, but *will* print
  //! progress messages.  "cbuild -q" turns that off.
  bool mEchoStatistics;
  bool mEchoProgress;

  //! A framework for estimated percent-complete status messages requires
  //! that we keep track of typical statistics.  So whenever there are
  //! major performance changes or changes in compilation order, we
  //! need to gather statistics with "runlist -custall", with cbuild
  //! switch -gatherStats \<directory\> identifying a directory into which to
  //! write statistics files.  All we are interested in here is elapsed
  //! time.
  UtOBStream* mGatherStatsFile;

  typedef std::pair<double,UInt32> DurationIndex;
  typedef UtHashMap<UtString,DurationIndex> PhaseMap;
  PhaseMap* mPhaseMap;

  StatsData mAbsData;
  double mEstimatedTotalTime;
  UInt32 mPercentComplete;
  UInt32 mPhaseIndex;
  UInt32 mPrevPhaseIndex;

  CARBON_FORBID_DEFAULT_CTORS(Stats);
}; // class Stats


//! Class to accumulate statistics
/*!
 * Use this class instead of Stats if you want to profile a piece of
 * code which is invoked many times, but you just want a total over
 * all the invocations, instead of printing out stats for each
 * invocation.
 */
class AccumStats: public Stats
{
public: CARBONMEM_OVERRIDES
  //! Constructor
  /*!
   * For these constructors, if you have a current Stats you are
   * using, pass that in; then the "SL#" will be consistent between
   * what this class prints and what your passed-in Stats prints.
   */
  AccumStats(Stats *s=0);
  AccumStats(FILE *f, Stats *s=0);

  //! Destructor
  ~AccumStats();

  //! Accumulate the statistics for the current interval
  void printIntervalStatistics(const char* intervalName, StatsData* intervalStats = NULL);

  //! Dump out the statistics for all accumulated intervals
  void printAllAccumulated();

private:
  //! Class to hold information for one bucket
  class BucketData
  {
  public: CARBONMEM_OVERRIDES
    BucketData() :
      mLevel(0)
    {}

    BucketData(UtString &name, UInt32 level) :
      mName(name),
      mLevel(level)
    {}

    BucketData(const BucketData& other)
    {
      if (this != &other) {
	mName = other.mName;
	mLevel = other.mLevel;
	mData = other.mData;
      }
    }

    ~BucketData() {}

    UtString mName;
    UInt32 mLevel;
    StatsData mData;
  };

  //! Currently-available bucket ident
  UInt32 mNextIdent;

  //! Mapping of bucket names to idents
  typedef UtHashMap<UtString,UInt32> StringIntMap;
  StringIntMap* mIntervalIdentMap;

  //! Vector of accumulated stats
  typedef UtArray<BucketData*> BucketVector;
  BucketVector* mAccumStats;
};

//! StatsInterval class
/*! This class encapsulates all the work necessary to manage a new
 *  level for the interval timer. On construction it pushes the
 *  interval timer and on return it pops the interval timer. It also
 *  tests for the phaseStats being false and disabling printing.
 */
class UtStatsInterval
{
public:
  //! constructor
  UtStatsInterval(bool phaseStats, Stats* stats)
    : mPhaseStats(phaseStats), mStats(stats)
  {
    if (mPhaseStats) {
      mStats->pushIntervalTimer();
    }
  }

  //! destructor
  ~UtStatsInterval()
  {
    if (mPhaseStats) {
      mStats->popIntervalTimer();
    }
  }

  //! Print interval stats
  void printIntervalStatistics(const char* intervalName)
  {
    if (mPhaseStats) {
      mStats->printIntervalStatistics(intervalName);
    }
  }

private:
  bool mPhaseStats;
  Stats* mStats;
}; // class UtStatsInterval

#endif // _STATS_H_
