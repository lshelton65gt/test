// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __BIT_VEC_DESC_H__
#define __BIT_VEC_DESC_H__

#include "util/DynBitVector.h"
#include "util/Util.h"
#include "util/HashValue.h"

// Implement a descriptor for arbitrary sized BitVectors, where we
// expect most of the bits are contiguous.  The basic idea is to
// use 30 bits to represetnts 15-bits of start-position and 15-bits
// of length.  The bottom two bits are 1.  If the start+length does
// not fit in 15+15 bits, then the bottom two bits are 0, and the
// upper 30 bits give us a pointer to a DynBitVector* or maybe another
// structure that represents the bits we want.

// We will still use temp DynBitVectors to provide all the functionality.

class DynBitVecDesc {
public: CARBONMEM_OVERRIDES
# define DynBitVecDesc_SMALL_CONTIGUOUS 0x3
# define DynBitVecDesc_SINGLE_BIT 0x1
# define DynBitVecDesc_ALL_ONES  0x2
# define DynBitVecDesc_MODE_MASK 0x3
# define DynBitVecDesc_FIELD_MASK 0x7FFF

  DynBitVecDesc(const DynBitVector& desc, DynBitVectorFactory*);
  DynBitVecDesc() {
    clear();
  }

  DynBitVecDesc(SInt32 bit, DynBitVectorFactory* factory);
  DynBitVecDesc(SInt32 start, SInt32 size, DynBitVectorFactory* factory);
  void populateRange(SInt32 start, SInt32 size, DynBitVectorFactory* factory);

  ~DynBitVecDesc() {
    clear();
  }

  void clear();

  bool isBitVec() const {
    return (mRep.startAndSize & DynBitVecDesc_MODE_MASK) == 0;
  }

  bool isSingleBit() const {
    return (mRep.startAndSize & DynBitVecDesc_MODE_MASK) ==
      DynBitVecDesc_SINGLE_BIT;
  }

  bool isAllOnes() const {
    return (mRep.startAndSize & DynBitVecDesc_MODE_MASK) ==
      DynBitVecDesc_ALL_ONES;
  }

  bool isSmallRange() const {
    return (mRep.startAndSize & DynBitVecDesc_MODE_MASK) ==
      DynBitVecDesc_SMALL_CONTIGUOUS;
  }

  //! Fill a DynBitVector matching our representation.
  void getBitVec(DynBitVector* tempbitvec, UInt32 numBits) const;
  
  //! Hashing is easy because our bit-vectors are unique
  size_t hash() const {return mRep.startAndSize;}

  //! Comparison is easy because our bit-vectors are unique
  bool operator==(const DynBitVecDesc& other) const {
    return mRep.startAndSize == other.mRep.startAndSize;
  }

  //! Is this bitvector empty?
  bool empty() const {
    return mRep.startAndSize == DynBitVecDesc_SMALL_CONTIGUOUS;
  }

  //! Does this bit vector have all 1s in its first 'size' elements?
  bool all(UInt32 size) const;

  //! How many bits are set?
  UInt32 count() const;

  //! Get Deterministic order for two bitvectors
  static int compare(const DynBitVecDesc& dbv1, UInt32 numBits1,
                     const DynBitVecDesc& dbv2, UInt32 numBits2);

  //! If this bitvector represents a contiguous range, set *startBit
  //! and *size, return true
  bool getContiguousRange(UInt32* startBit, UInt32* sz) const;

  // Do these vectors have any common bits set, in their first 'size' bits?
  bool anyCommonBitsSet(const DynBitVecDesc& other, UInt32 size) const;

  //! Does this vector cover the other?
  bool covers(const DynBitVecDesc & other, UInt32 size) const;

  //! Does this vector cover the bit?
  bool test(UInt32 offset) const;

  //! Prototype for combination methods.
  typedef void (*CombineFn)(const DynBitVecDesc& dbv1,
                            const DynBitVecDesc& dbv2,
                            DynBitVector * result,
                            UInt32 size);

  //! union two vectors 
  static void merge(const DynBitVecDesc & one, 
                    const DynBitVecDesc & two,
                    DynBitVector * result,
                    UInt32 size);

  //! intersect two vectors
  static void intersect(const DynBitVecDesc & one, 
                        const DynBitVecDesc & two,
                        DynBitVector * result,
                        UInt32 size);

  //! set-subtraction of two vectors.
  static void subtract(const DynBitVecDesc & one, 
                       const DynBitVecDesc & two,
                       DynBitVector * result,
                       UInt32 size);
private:
  UInt32 getRangeSize() const {return mRep.startAndSize >> 17;}
  UInt32 getRangeStart() const {return (mRep.startAndSize >> 2)
                                  & DynBitVecDesc_FIELD_MASK;}
  UInt32 getBit() const {return mRep.startAndSize >> 2;}
  UInt32 getAllOnesSize() const {return mRep.startAndSize >> 2;}

  union {
    const DynBitVector* bitvec;
    UInt32 startAndSize;
  } mRep;
} ;

#endif // __BIT_VEC_DESC_H__
