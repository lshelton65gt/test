// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef __CarbonRunTime_h_
#define __CarbonRunTime_h_

#ifndef CONSTANTRANGE_H_
#include "util/ConstantRange.h"
#endif

extern "C" {
//! NxM multiply yielding K word results
void carbon_multiply (const unsigned int *a, const unsigned int *b, unsigned int *c, int m, int n, int k);
void carbon_signed_multiply (const unsigned int *a, const unsigned int *b, unsigned int *c, int m, int n, int k);

//! Round-away from zero as required by Verilog LRM
SInt64 carbon_round_real (CarbonReal r);

//! converter for Verilog $realtobits
UInt64 carbon_real_to_bits(CarbonReal r);
//! converter for Verilog $bitstoreal, returns 0.0 if bits do not represent a real number
CarbonReal carbon_bits_to_real(const UInt64 i64);

//! Runtime OOB reporting
void carbon_oob_access (const char* contextMsg, const char* object, const ConstantRange& bounds,
                        SInt32 index);
void carbon_oob_memory_access (const char* contextMsg, const char* object, const ConstantRange& bounds,
                        SInt32 index);

}


#endif
