// -*- C++ -*-                
/*****************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UTLIBXMLWRITER_H_
#define __UTLIBXMLWRITER_H_
#include "util/c_memmanager.h"
#include "util/CarbonTypes.h"
#include "util/CarbonAssert.h"
#include "util/UtString.h"
#include "util/UtXmlWriter.h"
#include "libxml/encoding.h"
#include "libxml/xmlwriter.h"



class UtLibXmlWriter: public UtXmlWriter 
{

public: 
  CARBONMEM_OVERRIDES

  UtLibXmlWriter();
  UtLibXmlWriter(const char* xmlEncoding);

  //! virtual destructor
  virtual ~UtLibXmlWriter();
public:

  virtual void StartWriter(UtString&    targetFileName);
  virtual void StartWriter(const char*  targetFileName);
  virtual void StartElement(UtString&   name);
  virtual void StartElement(const char* name);
  virtual void EndElement();
  virtual void WriteAttribute(UtString&    name, UtString&  value);
  virtual void WriteAttribute(UtString&    name, const char*  value);
  virtual void WriteAttribute(const char*  name, UtString&  value);
  virtual void WriteAttribute(const char* name, const char* value);

  virtual void WriteAttribute(const char* name, const void* value);
  virtual void WriteAttribute(const char* name, bool value);
  virtual void WriteAttribute(const char* name, SInt32 value);

  virtual void WriteElement(UtString& name, UtString& value);
  virtual void WriteElement(const char* name, UtString& value);
  virtual void WriteElement(UtString& name, const char* value);
  virtual void WriteElement(const char* name, const char* value);
  virtual void EndWriter() ;


  void setWritePointers(bool state);
  bool getWritePointers();

private:
  UtString                 mXmlEncoding;
  xmlTextWriterPtr         mXmlWriter;
  bool                     mWritePointers;

};

#endif
