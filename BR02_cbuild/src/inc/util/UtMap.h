// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtMap_h_
#define __UtMap_h_

#ifndef __MemManager_h_
#include "util/MemManager.h"
#endif

#include "util/Util.h"
#include <map>

//! alternate STL map definition that uses the Carbon memory allocator
template<class _key, class _tp, class _compare >
class UtMap: public std::map<_key, _tp, _compare,
                             CarbonSTLAlloc<std::pair<const _key, _tp> > >
{
  typedef std::map<_key, _tp, _compare,
                   CarbonSTLAlloc<std::pair<const _key, _tp> > > _base;
public: CARBONMEM_OVERRIDES

  template<class _iter> UtMap(_iter _begin, _iter _end):
    _base(_begin, _end)
    {}

  UtMap(_compare& __cmp) : _base(__cmp) {}
  UtMap() {}
};

#endif
