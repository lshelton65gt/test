// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003, 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#ifndef __UtObserver_h_
#define __UtObserver_h_

#include "carbon/c_memmanager.h"

template <class T>
class UtObserver
{
 public:
  CARBONMEM_OVERRIDES

  UtObserver(void) {}

  virtual ~UtObserver(void) {}

  virtual void update(T *subject) = 0;


 private:
  UtObserver(const UtObserver&);
  UtObserver& operator=(const UtObserver&);
};


#endif
