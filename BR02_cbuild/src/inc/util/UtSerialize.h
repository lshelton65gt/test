// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtSerialize_h_
#define __UtSerialize_h_

#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"

//! abstract base class for a serializable object
class UtSerialObject {
public:
  CARBONMEM_OVERRIDES

  //! ctor
  UtSerialObject() {}

  //! dtor
  virtual ~UtSerialObject() {}

  //! read this object, return true for success
  virtual bool read(UtIStream*) = 0;

  //! write this object, indenting if this is an aggregate (struct or container)
  virtual void write(UtOStream*, UInt32 indent) = 0;
};

//! serializable atomic object (string, int, double)
/*!
 *! the only requirement on the atomic object is that UtOStream and UtIStream
 *! know how to serialize it
 */
template<class Object>
class UtSerialAtomic : public UtSerialObject {
public:
  CARBONMEM_OVERRIDES

  //! ctor
  UtSerialAtomic(Object* obj) : mObj(obj) {}

  //! dtor
  virtual ~UtSerialAtomic() {}

  //! write this object
  virtual void write(UtOStream* os, UInt32 /*indent*/) {
    *os << *mObj;
  }

  //! read this object, return true for success
  virtual bool read(UtIStream* is) {
    return (*is >> *mObj);
  }

private:
  Object* mObj;
};

//! serializer container (list or vector)
/*!
 *! the list or vector must be STL-style, because this class will
 *! call push_back(), back(), and will iterate over the container.
 */
template<class Container, class SerialObject>
class UtSerialContainer : public UtSerialObject {
public:
  CARBONMEM_OVERRIDES

  //! the object out of which the vector is made
  typedef typename Container::value_type value_type;

  //! ctor
  UtSerialContainer(Container* container) : mContainer(container) {}

  //! dtor
  virtual ~UtSerialContainer() {}

  //! read this container
  /*!
   *! this must be inlined because the .cxx file does not know
   *! the exact container-type being instantiated
   */
  virtual bool read(UtIStream* is) {
    *is >> UtIO::quotify;
    UtString tok;
    if (! (*is >> tok)) {
      return false;
    }
    if (tok != "[") {
      is->reportError("Expected [ to open array");
    }
    mContainer->clear();
    char c;
    while (*is >> c) {
      if (! isspace(c)) {

        if (c == ']') {
          return true;            // end of array without errors
        }

        is->unget(&c, 1);

        mContainer->push_back(value_type());
        value_type& obj = mContainer->back();
        SerialObject sobj(&obj);
        if (!sobj.read(is)) {
          return false;         // failed to read object
        }
      }
    }
    is->reportError("Expected ] to close array");
    return false; 
  } // virtual bool read
        
  //! write container
  virtual void write(UtOStream* os, UInt32 indent) {
    *os << UtIO::quotify;
    *os << '[' << '\n';
    indent += 2;
    for (typename Container::iterator p = mContainer->begin(),
           e = mContainer->end(); p != e; ++p)
    {
      for (UInt32 i = 0; i < indent; ++i) {
        *os << ' ';
      }
      value_type& obj = *p;
      SerialObject sobj(&obj);
      sobj.write(os, indent);
      *os << '\n';
    }
    indent -= 2;
    for (UInt32 i = 0; i < indent; ++i) {
      *os << ' ';
    }
    *os << ']';
  } // virtual void write

private:
  Container* mContainer;
}; // class UtSerialContainer : public UtSerialObject

//! serializable structure with named fields
class UtSerialStruct : public UtSerialObject {
public:
  CARBONMEM_OVERRIDES

  //! map of field-names to objects
  typedef UtHashMap<UtString,UtSerialObject*> NameMap;

  //! ctor
  UtSerialStruct();

  //! dtor
  virtual ~UtSerialStruct();

  //! read this structure
  virtual bool read(UtIStream* is);
        
  //! write this structure
  virtual void write(UtOStream* os, UInt32 indent);

  //! add an atomic object
  template<class Object>
  void addAtomic(const char* name, Object* obj) {
    INFO_ASSERT(mNameMap.find(name) == mNameMap.end(), "duplicate entry");
    mNameMap[name] = new UtSerialAtomic<Object>(obj);
  }

  //! add an arbitrary structure
  template<class T>
  void addStruct(const char* name, T* obj) {
    INFO_ASSERT(mNameMap.find(name) == mNameMap.end(), "duplicate entry");
    UtSerialStruct* ss = new UtSerialStruct;
    mNameMap[name] = ss;
    obj->serialize(ss);
  }

  //! add an arbitrary container
  template<class Container, class SerialObject>
  void addContainer(const char* name, Container* container) {
    INFO_ASSERT(mNameMap.find(name) == mNameMap.end(), "duplicate entry");
    UtSerialContainer<Container, SerialObject>* sc =
      new UtSerialContainer<Container, SerialObject>(container);
    mNameMap[name] = sc;
  }

  //! add a container for atomic types
  template<class Container>
  void addAtomicContainer(const char* name, Container* container) {
    addContainer<Container, UtSerialAtomic<typename Container::value_type> >
      (name, container);
  }

private:
  NameMap mNameMap;
};

#endif
