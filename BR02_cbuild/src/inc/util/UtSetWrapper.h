// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __UtSetWrapper_h_
#define __UtSetWrapper_h_


#include "util/Iter.h"
#include "util/Util.h"
#include "util/CarbonTypes.h"   // for FORWARD_ITERATOR

// This thin wrapper over AdaptiveSet helps avoid debug size explosion
// by keeping all the set iteration, insertion, and lokoup code in one
// .o file, rather than having it replicated everywhere it gets
// included.

class _ObjBase;
class UtSetWrapper_IterP;

class UtSetWrapper
{
public: CARBONMEM_OVERRIDES
  typedef _Obj value_type;
  typedef _Obj reference;
  typedef const _Obj const_reference;
  typedef Iter<_Obj> SortedLoop;
  typedef Iter<_Obj> UnsortedLoop;
  typedef Iter<_Obj> UnsortedCLoop;

  class iterator: public FORWARD_ITERATOR(_Obj)
  {
  public:  CARBONMEM_OVERRIDES
    iterator() {mIter = NULL;}
    iterator(UtSetWrapper_IterP* iter) {mIter = iter;}
    ~iterator();
    iterator(const iterator& src);
    iterator& operator=(const iterator& src);
    iterator& operator++();
    bool operator==(const iterator& other) const;
    bool operator!=(const iterator& other) const;
    _Obj operator*() const;
    size_t operator-(const iterator& other) const;

    UtSetWrapper_IterP* mIter;
  }; // class iterator

  typedef iterator const_iterator;

  UtSetWrapper();
  ~UtSetWrapper();
  UtSetWrapper(const UtSetWrapper&);
  UtSetWrapper& operator=(const UtSetWrapper&);

  //! create a loop through a sorted image of the objects contained in the set
  /*! To use this method, you must have an operator< defined in the _KeyType.
   */
  SortedLoop loopSorted() const;

  //! Loop through the objects contained in the set, in an order that
  //! can change from run to run
  UnsortedLoop loopUnsorted();

  //! Loop through the objects contained in the set, in an order that
  //! can change from run to run
  UnsortedCLoop loopUnsorted() const;

  //! find an object, returning end() if that object is not in the set.
  const_iterator find(const _Obj k) const;
  iterator find(const _Obj k);
  void /*std::pair<iterator,bool> */insert(const _Obj k);
  //template<typename T> void insert(T b, T e);

  template<class _Iterator> void insert(_Iterator b, _Iterator e)
  {
    for (; b != e; ++b)
      insert(*b);
  }


  iterator insert(iterator /*__position*/, const value_type& __x);
  iterator begin();
  iterator end();
  const_iterator begin() const;
  const_iterator end() const;
  size_t size() const;
  void clear();
  void erase(iterator i);
  size_t erase(reference);
  bool empty() const;
  bool operator==(const UtSetWrapper& other) const;

private:
  _Container* mSet;
};

#endif // __UtSetWrapper_h_

