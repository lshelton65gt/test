// -*- C++ -*-                
/*****************************************************************************
 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UTXMLWRITER_H_
#define __UTXMLWRITER_H_
#include "util/c_memmanager.h"
#include "util/CarbonTypes.h"
#include "util/UtString.h"



class UtXmlWriter 
{

public:
  UtXmlWriter() {}
  virtual ~UtXmlWriter() {}

  virtual void StartWriter(UtString&    targetFileName)= 0;
  virtual void StartWriter(const char*  targetFileName)= 0;
  virtual void StartElement(UtString&   name)= 0;
  virtual void StartElement(const char* name)= 0;
  virtual void EndElement()= 0;
  virtual void WriteAttribute(UtString&    name, UtString&  value)= 0;
  virtual void WriteAttribute(UtString&    name, const char*  value)= 0;
  virtual void WriteAttribute(const char*  name, UtString&  value)= 0;
  virtual void WriteAttribute(const char* name, const char* value)= 0;

  virtual void WriteAttribute(const char* name, const void* value)= 0;
  virtual void WriteAttribute(const char* name, bool value)= 0;
  virtual void WriteAttribute(const char* name, SInt32 value)= 0;

  virtual void WriteElement(UtString& name, UtString& value)= 0;
  virtual void WriteElement(const char* name, UtString& value)= 0;
  virtual void WriteElement(UtString& name, const char* value)= 0;
  virtual void WriteElement(const char* name, const char* value)= 0;
  virtual void EndWriter() = 0;


};

#endif
