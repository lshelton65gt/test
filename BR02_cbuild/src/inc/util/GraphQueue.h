// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file

  Implements ordering nodes in a directed graph. If there are cycles a
  virtual function is called to find spots to break them.
*/

#ifndef _GRAPHQUEUE_H_
#define _GRAPHQUEUE_H_

#include "util/Graph.h"
#include "util/Loop.h"
#include "util/UtOrder.h"

//! class UtGraphQueue
/*!
 */
class UtGraphQueue
{
public: CARBONMEM_OVERRIDES
  //! class NodeCmp
  /*! This class is used to compare nodes that are ready to
   *  schedule. A derived version of this must be passed to
   *  UtGraphQueue for it to pay attention to an order when choosing
   *  from multiple ready nodes.
   *
   *  By default this class sorts by pointer.
   */
  class NodeCmp
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    NodeCmp() {}

    //! destructor
    virtual ~NodeCmp() {}

    //! Compare two graph nodes
    virtual bool operator()(const GraphNode*, const GraphNode*) const;
  };

  //! class EdgeCmp
  /*! This class is used to compare two edges. It is useful in sorting
   *  edges by their relative breaking potential.
   *
   *  By default this class sorts by pointer.
   */
  class EdgeCmp
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    EdgeCmp() {}

    //! destructor
    virtual ~EdgeCmp() {}

    //! compare two graph edges
    virtual bool operator()(const GraphEdge*, const GraphEdge*) const;
  };

  //! constructor
  UtGraphQueue();

  //! destructor
  virtual ~UtGraphQueue();

  //! Initialize the queue
  /*! This routine analyzing the graph and puts nodes into a ready
   *  queue if they are ready to schedule. Call the schedule routine
   *  to begin scheduling them.
   */
  void init(Graph* graph, NodeCmp* graphNodeCmp, EdgeCmp* graphEdgeCmp);

  //! Schedule nodes
  /*! Begins scheduling nodes in the graph from fanins to fanouts.
   *
   *  It calls the scheduleNode virtual function to actually schedule the
   *  node.
   *
   *  It calls breakCycle virtual function if there are no ready nodes
   *  but scheduling is not complete. This can only occur if there is
   *  a cycle in the graph.
   */
  void scheduleWalk();

  //! Get the remaining fanin count for a given node
  /*! The fanin count is populated for each node during the init()
   *  function. As nodes get scheduled, edges get removed from the
   *  graph. This decrements the corresponding fanin count for each
   *  node.
   *
   *  Perhaps fanin count should be renamed to incident count?
   */
  SIntPtr getFaninCount(GraphNode* node);

protected:
  //! Schedule a node
  /*! This function must be overriden to process the scheduled nodes
   *  in the given order.
   */
  virtual void scheduleNode(GraphNode* node, SInt32 depth) = 0;

  //! Break a cycle
  /*! This function must be overriden to break any cycles in the
   *  graph. This function is called when there are no * nodes in the
   *  ready queue but there are nodes remaining to be * scheduled.
   */
  virtual void breakCycle() = 0;

  //! Defer the scheduling for some subset of ready nodes.
  /*!
    At least one ready node must be left un-deferred.
   */
  virtual void deferReadyNodes(const UtArray<GraphNode*> & ready, 
                               UtSet<GraphNode*> * deferred);

  //! Store data in the node
  /*! This class needs to store data in each node for processing. But
   *  the derived class needs to manage the scratch memory on each
   *  graph node. This virtual function needs to be overloaded to
   *  store data in the derived classes node data on behalf of this
   *  base class.
   *
   *  The default behavior is to write the graph nodes's scratch area.
   */
  virtual void putNodeData(GraphNode* node, SIntPtr data);

  //! Get data from a node
  /*! See the documentation for putNodeData() to see why this is
   *  needed. This routine gets the data stored on a node.
   *
   *  The default behavior is to read the graph nodes's scratch area.
   */
  virtual SIntPtr getNodeData(GraphNode* node) const;

  //! Initialize a graph node
  /*! This function does nothing by default. Override it to initialize
   *  any graph node data of interest. This function is called as the
   *  first thing in the init() routine. This allows the derived class
   *  to allocate space for each node. It is expected that
   *  putNodeData() can be called after this function is called.
   */
  virtual void initGraphNode(GraphNode* node);

  //! Initialize a graph edge
  /*! This function does nothing by default. Override it to initialize
   *  any graph edge data of interest. This function is called after
   *  initGraphNode() is called on all graph nodes.
   *
   *  The function should return whether the edge should be used or
   *  ignored in scheduling. This feature was put in there to allow
   *  the code to ignore self edges but may be used for other reasons
   *  as well.
   */
  virtual bool initGraphEdge(GraphNode* from, GraphEdge* edge, GraphNode* to);

  //! Break an edge
  /*! This routine should be called when an edge is broken. This
   *  occurs when nodes are removed from the ready queue and
   *  scheduled. But it can also be called from breakCycle() to pick a
   *  point to break a cycle.
   *
   *  If any nodes get a zero fanin count as a result of this edge
   *  break, they get added to the ready queue.
   *
   *  Either this routine or breakNode should be called from
   *  breakCycle to make progress.
   *
   *  \param edge The edge to use to break the cycle. If this is NULL,
   *  the graph queue uses the sorted list of live edges and picks the
   *  first one.
   */
  void breakEdge(GraphEdge* edge);

  //! Break a node
  /*! This routine should be called when a node is picked for breaking
   *  a cycle. All edges from this node (that are not already broken)
   *  are broken with breakEdge()
   *
   *  Either this routine or breakEdge should be called from
   *  breakCycle to make progress.
   */
  void breakNode(GraphNode* node);

private:
  // Functions to manipulate the fanin count in the graph nodes
  void incrFaninCount(GraphNode* node);
  SIntPtr decrFaninCount(GraphNode* node);
  void putFaninCount(GraphNode* node, SIntPtr faninCount);

  // Functions to manipulate the ready queue
  void updateReadyQueue();

  //! Remember that deferred nodes are still ready and schedulable.
  void rememberDeferredReady(GraphNode* node);

  //! EdgeCmpWrapper class
  /*! This class is used to make copy construction of the edge compare
   *  work with STL containers. Copy construction of the base clas
   *  looses the virtual pointer of the derived class. This wrapper
   *  class stores the compare class by pointer so that we retain the
   *  right vptr. The operator() for this wrapper class calls the one
   *  in the member variable edge compare class.
   */
  class EdgeCmpWrapper
  {
  public: CARBONMEM_OVERRIDES
    EdgeCmpWrapper(EdgeCmp* edgeCmp) : mEdgeCmp(edgeCmp) {}
    EdgeCmpWrapper(const EdgeCmpWrapper& w) { mEdgeCmp = w.mEdgeCmp; }
    ~EdgeCmpWrapper() {}
    bool operator()(const GraphEdge* e1, const GraphEdge* e2) const
    {
      return (*mEdgeCmp)(e1, e2);
    }
  private:
    EdgeCmp* mEdgeCmp;
  };

  //! NodeCmpWrapper class
  /*! This class is used to make copy construction of the node compare
   *  work with STL containers. Copy construction of the base clas
   *  looses the virtual pointer of the derived class. This wrapper
   *  class stores the compre class by pointer so that we retain the
   *  right vptr. The operator() for this wrapper class calls the one
   *  in the member variable node compare class.
   */
  class NodeCmpWrapper
  {
  public: CARBONMEM_OVERRIDES
    NodeCmpWrapper(NodeCmp* nodeCmp) : mNodeCmp(nodeCmp) {}
    NodeCmpWrapper(const NodeCmpWrapper& w) { mNodeCmp = w.mNodeCmp; }
    ~NodeCmpWrapper() {}
    bool operator()(const GraphNode* e1, const GraphNode* e2) const
    {
      return (*mNodeCmp)(e1, e2);
    }
  private:
    NodeCmp* mNodeCmp;
  };

  // The graph to schedule
  Graph* mGraph;

  // The current set of edges/nodes that have not been broken/scheduled.
  typedef UtSet<GraphEdge*, EdgeCmpWrapper> LiveEdges;
  LiveEdges* mLiveEdges;
  typedef UtSet<GraphNode*, NodeCmpWrapper> NodeSet;
  typedef Loop<NodeSet> NodeSetLoop;
  NodeSet* mLiveNodes;

  // Information on the scheduling of nodes
  SInt32 mNumRemaining;
  NodeSet* mReady;
  NodeSet* mNewReady;
  NodeSet* mDeferredReady;
  SInt32 mDepth;
}; // class UtGraphQueue

#endif // _GRAPHQUEUE_H_
