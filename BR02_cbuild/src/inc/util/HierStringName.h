// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003, 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __HierStringName_h_
#define __HierStringName_h_


#ifndef __HierName_h_
#include "util/HierName.h"
#endif
#ifndef __UtString_h_
#include "util/UtString.h" // should use "class string;" but that does not compile.
#endif



/*!
  \file
  A simple hierarchical UtString name implementation of HierName. This
  is needed for hierarchical path creation for look ups as well as
  other string-only hdl needs.
*/


class StringAtom;

//! HierStringName implementation
/*!
  This is a low overhead implementation of HierName.
*/
class HierStringName : public virtual HierName
{
public: CARBONMEM_OVERRIDES
  //! Constructor
  /*!
    \param id Name(identifier) of this hierarchical segment
  */
  HierStringName(StringAtom* id);

  //! Copy constructor
  HierStringName(const HierStringName& src);

  //! virtual destructor
  virtual ~HierStringName();

  /* specialization */

  //! Allows placement of the parent name
  void putParent(HierName* parent) {mParent = parent;}

  
  /* implementation */

  //! Return this name string
  const char* str() const;

  //! Return the name object
  StringAtom* strObject() const;

  //! Return the Parent read-only
  /*!
    \returns NULL if there is no parent
  */
  const HierName* getParentName() const;

  //! Compose a hierarchical name (currently unimplemented)
  virtual void composeHelper(UtString*, bool includeRoot, bool hierName,
                             const char* separator, bool escapeIfMultiLevel) const;

  //! Print this path to stdout. Used only for debugging and assertions
  /*!
    While debugging, it becomes very difficult to know which
    name/str/scope you are looking at, so this function is needed
    to ease pain.
    
    \warning ONLY USE FOR DEBUGGING
  */
  void print() const;

protected:
   virtual void printInfo() const;

private:
  StringAtom* mAtom;
  HierName* mParent;
};

#endif
