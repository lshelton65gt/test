// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtSet_h_
#define __UtSet_h_
#ifndef __MemManager_h_

#include "util/MemManager.h"
#endif

#include "util/Util.h"
#include <set>

//! alternate STL set definition that uses the Carbon memory allocator
#if pfTEMPLATE_NO_FORWARD_ARGS
template<class _key, class _Compare = std::less<_key> >
#else
template<class _key, class _Compare>
#endif
class UtSet: public std::set<_key, _Compare, CarbonSTLAlloc<_key> >
{
  typedef std::set<_key, _Compare, CarbonSTLAlloc<_key> > _Base;
public: CARBONMEM_OVERRIDES
  UtSet(CarbonSTLAlloc<_key> __a): _Base(__a)  {}
  UtSet(_Compare& __cmp) : _Base(__cmp) {}
  template <class _In> UtSet (_In first, _In last) : _Base (first, last) {}
  UtSet() {}
};

template<class _key, class _Compare>
class UtMultiSet: public std::multiset<_key, _Compare, CarbonSTLAlloc<_key> >
{
  typedef std::multiset<_key, _Compare, CarbonSTLAlloc<_key> > _Base;
public: CARBONMEM_OVERRIDES
  UtMultiSet(CarbonSTLAlloc<_key> __a): _Base(__a)  {}
  UtMultiSet() {}
};

#endif
