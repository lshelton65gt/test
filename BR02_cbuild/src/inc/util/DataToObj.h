// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#ifndef _DATA_TO_OBJ_H_
#define _DATA_TO_OBJ_H_

#include "util/Util.h"

bool UtConvertFileToCFunction(const char* inputDataFile,
                              const char* cFileName,
                              const char* cFunctionName,
                              UtString* errmsg);

#endif // _DATA_TO_OBJ_H_
