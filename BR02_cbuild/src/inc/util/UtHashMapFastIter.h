// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtHashMapFastIter_h_
#define __UtHashMapFastIter_h_


#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif
#ifndef _CARBON_HASH_VALUE_
#include "util/HashValue.h"
#endif
#ifndef _LOOPMAP_H_
#include "util/LoopMap.h"
#endif
#ifndef __MemManager_h_
#include "util/MemManager.h"
#endif
#include <algorithm> // for std::sort in visual c++
#ifndef __Util_h_
#include "util/Util.h"
#endif

#include "util/carbon_hashtable.h"
#include "util/carbon_hashtable_itr.h"
#include <utility> // for std::pair

template<typename _KeyType, typename _ValueType,
         typename _HashType = HashHelper<_KeyType>,
         typename _HashKeyMgr = HashMgr<_KeyType>,
         typename _HashValMgr = HashMgr<_ValueType> >
class UtHashMapFastIter;

//! UtHashMapFastIter class -- wrapper over a C hash map implementation
/*!
 * 
 * See UtHashMap.  This version is identical, but supports fast
 * iteration, even if the number of buckets grows large.  In exchange,
 * slightly more memory is consumed:
 *   - an extra pointer per entry (a next-pointer in a linked list of entries)
 *   - an extra pointer per hash table (the head of the linked list of all entries)
 * and this implementation lacks an erase method.  An erase method could
 * be provided at the expense of an additional pointer per entry -- perhaps
 * that could be added as UtHashMap3.
 *
 * Though individual erasure is not provided, clear() works fine.
 */
template<typename _KeyType, typename _ValueType, typename _HashType,
         typename _HashKeyMgr, typename _HashValMgr>
class UtHashMapFastIter
{
public: CARBONMEM_OVERRIDES
  typedef _KeyType key_type;
  typedef _ValueType mapped_type;
  typedef std::pair<_KeyType, _ValueType> NameValue;
  typedef NameValue value_type;

  struct MapEntry : public carbon_hashEntryStruct {
    value_type mKeyVal;
    MapEntry* mNext;            // used for link-listing every hashtable element
    static MapEntry* cast(carbon_hashEntryStruct* ptr) {
      return (MapEntry*) ptr;
    }
    static const MapEntry* cast(const carbon_hashEntryStruct* ptr) {
      return (const MapEntry*) ptr;
    }
  };

  //! Normal iterator implementation
  /*!
    Do not instantiate.
  */
  struct iterator {
    CARBONMEM_OVERRIDES

    typedef NameValue value_type;
    typedef value_type& reference;
    typedef value_type* pointer;

    iterator() : mMapEntry(NULL) {
    }

    iterator(MapEntry* me) :
      mMapEntry(me)
    {
    }

    value_type* getPtr() const {
      return &(mMapEntry->mKeyVal);
    }

    pointer operator->() const { 
      return &(mMapEntry->mKeyVal);
    }
    reference operator*() const {
      return *getPtr();
    }


    bool operator==(const iterator& __it) const
    { return mMapEntry == __it.mMapEntry; }
    bool operator!=(const iterator& __it) const
    { return mMapEntry != __it.mMapEntry; }

    iterator& operator++() {
      mMapEntry = mMapEntry->mNext;
      return *this;
    }

    // iterator operator++(int); Don't allow this

    MapEntry* mMapEntry;
  }; // struct iterator

  //! Normal const_iterator implementation
  /*!
    Do not instantiate.
  */
  struct const_iterator {
    CARBONMEM_OVERRIDES

    typedef NameValue value_type;
    typedef value_type& reference;
    typedef value_type* pointer;

    const_iterator() : mMapEntry(NULL) {
    }

    const_iterator(MapEntry* me) :
      mMapEntry(me)
    {
    }

    const_iterator(carbon_hashEntry* e) {
      mMapEntry = MapEntry::cast(e);
    }

    const_iterator(const iterator& iter): 
      mMapEntry(iter.mMapEntry)
    {
    }

    value_type* getPtr() const {
      return &(mMapEntry->mKeyVal);
    }

    pointer operator->() const { 
      return &(mMapEntry->mKeyVal);
    }
    reference operator*() const {
      return *getPtr();
    }


    bool operator==(const const_iterator& __it) const
    { return mMapEntry == __it.mMapEntry; }
    bool operator!=(const const_iterator& __it) const
    { return mMapEntry != __it.mMapEntry; }

    const_iterator& operator++() {
      mMapEntry = mMapEntry->mNext;
      return *this;
    }

    // const_iterator operator++(int); Don't allow this

    MapEntry* mMapEntry;
  }; // struct const_iterator


  //! The non-static wrapper (in a template!) for key hashing
  UInt32 hashFn(const carbon_hashEntry* e) {
    const MapEntry* me = MapEntry::cast(e);
    _HashType hashType;
    return hashType.hash(me->mKeyVal.first);
  }

  //! The non-static wrapper for key comparison
  int eqFn(const void* key1, const carbon_hashEntry* e) {
    const _KeyType* key1V = (const _KeyType*) key1;
    const MapEntry* me = MapEntry::cast(e);
    _HashType hashType;
    return hashType.equal(*key1V, me->mKeyVal.first);
  }
  
  //! The static wrapper for passing as procedure ptr
  static UInt32 sHashFn(void* hs, const carbon_hashEntry* e)
  {
    return ((UtHashMapFastIter*)hs)->hashFn(e);
  }

  //! The static wrapper for key comparison
  static int sEqFn(void* hs, const void* key1, const carbon_hashEntry* e) {
    return ((UtHashMapFastIter*)hs)->eqFn(key1, e);
  }
    
  //! Constructor
  UtHashMapFastIter()
  {
    createHash();
  }
  
  //! Destructor
  ~UtHashMapFastIter()
  {
    freeEntries();
    carbon_hashtable_clean(&mHash, sizeof(MapEntry));
  }

  typedef UtHashMapFastIter<_KeyType, _ValueType, _HashType, _HashKeyMgr, _HashValMgr>
  HashMapType;

  typedef LoopMap<HashMapType> UnsortedLoop;

  //! hand-coded UnsortedCLoop for speed (avoid end() iterator overhead)
  struct UnsortedCLoop {
    UnsortedCLoop(const UtHashMapFastIter& hmap) : mMapEntry(hmap.mChain) {
    }
    UnsortedCLoop(const UnsortedCLoop& ucl):
      mMapEntry(ucl.mMapEntry)
    {
    }
    UnsortedCLoop(): mMapEntry(NULL) {
    }      
    UnsortedCLoop& operator=(const UnsortedCLoop& ucl) {
      if (&ucl != this) {
        mMapEntry = ucl.mMapEntry;
      }
      return *this;
    }

    bool atEnd() const {return mMapEntry == NULL;}
    void operator++() {mMapEntry = mMapEntry->mNext;}
    value_type* getPtr() const {
      return &(mMapEntry->mKeyVal);
    }
    const _KeyType& getKey() const {
      return mMapEntry->mKeyVal.first;
    }
    const _ValueType& getValue() const {
      return mMapEntry->mKeyVal.second;
    }

  private:
    MapEntry* mMapEntry;
  };

  //! Iterate over every entry, removing them as we go.
  /*! The user must run this loop to completion -- it is not legal to
   *! destruct the loop partway through, or to use the map while
   *! the loop is alive.  These constraints are not checked.
   */
  struct RemoveLoop {
    RemoveLoop(UtHashMapFastIter& hmap) :
      mMap(&hmap),
      mMapEntry(hmap.mChain)
    {
    }
    RemoveLoop(const RemoveLoop& rl):
      mMap(rl.mMap),
      mMapEntry(rl.mMapEntry)
    {
    }
    RemoveLoop(): mMap(NULL), mMapEntry(NULL) {
    }
    RemoveLoop& operator=(const RemoveLoop& rl) {
      if (&rl != this) {
        mMap = rl.mMap;
        mMapEntry = rl.mMapEntry;
      }
      return *this;
    }

    ~RemoveLoop() {
      if (mMap->mChain != NULL) {
        mMap->mChain = NULL;
        carbon_hashtable_clear_internal(&(mMap->mHash));
      }
    }

    bool atEnd() const {return mMapEntry == NULL;}
    void operator++() {
      MapEntry* doomed = mMapEntry;;
      mMapEntry = doomed->mNext;
      carbonmem_dealloc(doomed, sizeof(MapEntry));
    }
    value_type* getPtr() const {
      return &(mMapEntry->mKeyVal);
    }
    const _KeyType& getKey() const {
      return mMapEntry->mKeyVal.first;
    }
    const _ValueType& getValue() const {
      return mMapEntry->mKeyVal.second;
    }

  private:
    UtHashMapFastIter* mMap;
    MapEntry* mMapEntry;
  };

  class LoopI;
  friend class LoopI;
  friend struct iterator;
  friend struct RemoveLoop;

  class LoopI
  {
  public: 
    CARBONMEM_OVERRIDES

    typedef NameValue value_type;
    typedef const value_type& reference;

    struct CmpPtr
    {
      CARBONMEM_OVERRIDES

      CmpPtr(const carbon_hashtable* /*table*/) /*: mHashTable(table)*/ {}
      bool operator()(const MapEntry* p,
		      const MapEntry* q) const {
        _HashType hashType;
        return hashType.lessThan(p->mKeyVal.first, q->mKeyVal.first);
      }
      //const carbon_hashtable* mHashTable;
    };

    LoopI(const HashMapType& sh)
    {
      mHashTable = &sh.mHash;
      mSorted.reserve(sh.size());
      for (MapEntry* me = sh.mChain; me != NULL; me = me->mNext) {
        mSorted.push_back(me);
      }
      std::sort(mSorted.begin(), mSorted.end(), CmpPtr(mHashTable));
      mPtr = mSorted.begin();
      mEnd = mSorted.end();
    }

    // Have to put copy ctor & assign operator for now because
    // we don't have IterMap to wrap this yet.
    LoopI(const LoopI& src)
    {
      copy(src);
    }

    LoopI& operator=(const LoopI& src)
    {
      if (this != &src) {
        copy(src);
      }
      return *this;
    }

    bool atEnd() const {return mPtr == mEnd;}
    void operator++() {++mPtr;}

    //! Go to the last element of the sorted array
    void toLast() { mPtr = mEnd; --mPtr; }

    //! Go back to the beginning of the sorted array
    void rewind() { mPtr = mSorted.begin(); }

    const typename _HashKeyMgr::RefType& getKey() {
      MapEntry* me = *mPtr;
      return me->mKeyVal.first;
    }
    typename _HashValMgr::RefType& getValue() {
      MapEntry* me = *mPtr;
      return me->mKeyVal.second;
    }

    reference operator*() const {
      MapEntry* me = *mPtr;
      return me->mKeyVal;
    }

  private:
    const carbon_hashtable* mHashTable;
    typedef UtArray<MapEntry*> NameValueVec;
    NameValueVec mSorted;
    //_HashValMgr mHashValMgr;
    //_HashKeyMgr mHashKeyMgr;
    typename NameValueVec::iterator mPtr;
    typename NameValueVec::iterator mEnd;

    void copy(const LoopI& src)
    {
      mHashTable = src.mHashTable;
      mSorted = src.mSorted;
      // mSorted may have been reallocated, so fix up iterators.
      mPtr = mSorted.begin() +
        (src.mPtr - const_cast<LoopI&>(src).mSorted.begin());
      mEnd = mSorted.end();
    }
  };

  typedef LoopI SortedLoop;     // would like to wrap with Iter...

  //! Get the internal hashtable. Valid until destruction
  carbon_hashtable* getCHash() { return &mHash; }

  //! create a loop through a sorted image of the objects contained in the hash-map
  /*! To use this method, you must have an operator< defined in the _KeyType. */
  SortedLoop loopSorted() const {return SortedLoop(*this);}

  //! Loop through the objects contained in the map, in an order that can change from run to run
  UnsortedLoop loopUnsorted() {return UnsortedLoop(*this);}

  //! Loop through the objects contained in the map, in an order that can change from run to run
  UnsortedCLoop loopCUnsorted() const {return UnsortedCLoop(*this);}

  const_iterator find(const _KeyType& k) const {
    return const_iterator(findEntry(k));
  }
  
  size_t count(const _KeyType& k) const {
    return findEntry(k) != NULL;
  }
  
  // stl-compliant find -- requires construction of an end() iterator on failure
  iterator find(const _KeyType& k) {
    return iterator(findEntry(k));
  }
  
  const MapEntry* findEntry(const _KeyType& k) const {
    return const_cast<UtHashMapFastIter*>(this)->findEntry(k);
  }

  // find that returns the MapEntry directly, or NULL
  MapEntry* findEntry(const _KeyType& k) {
    _HashType hashType;
    return MapEntry::cast(carbon_hashtable_findEntry(&mHash, &k,
                                                     sHashFn, sEqFn,
                                                     (void*) this, 
                                                     hashType.hash(k)));
  }

  //! Type for the return value of STL-compatible insert()
  typedef std::pair<iterator,bool> IterBoolPair;

  //! slow STL-compatible insert
  IterBoolPair insert(const value_type& v) { 
    carbon_hashEntry* e;
    _HashType hashType;
    bool ret = carbon_hashtable_maybe_insert(&mHash, (void*) &v.first,
                                             sHashFn, sEqFn, (void*) this,
                                             hashType.hash(v.first),
                                             sizeof(MapEntry), &e);
    MapEntry* me = MapEntry::cast(e);
    if (ret) {
      _HashKeyMgr::copy(v.first, &(me->mKeyVal.first));
      me->mNext = mChain;
      mChain = me;
    }
    _HashValMgr::copy(v.second, &(me->mKeyVal.second));
    return IterBoolPair(iterator(me), ret);
  }

  //! insert a (key,val) pair, don't care about whether a new elt was created
  void insertNoIter(const value_type& v) { 
    carbon_hashEntry* e;
    _HashType hashType;
    bool ret = carbon_hashtable_maybe_insert(&mHash, (void*) &v.first,
                                             sHashFn, sEqFn, (void*) this,
                                             hashType.hash(v.first),
                                             sizeof(MapEntry), &e);
    MapEntry* me = MapEntry::cast(e);
    if (ret) {
      _HashKeyMgr::copy(v.first, &(me->mKeyVal.first));
      me->mNext = mChain;
      mChain = me;
    }
    _HashValMgr::copy(v.second, &(me->mKeyVal.second));
  }

  //! Fast, highly dangerous insertion of an uninitialized element
  /*!
   *! The caller is repsonsible for initializing both mapEntry->mKeyVal.first
   *! and mapEntry->mKeyVal.second, if the return value is true.
   *!
   *! The memory will be completely raw random bits, so be very careful
   *! if the objects being stored would try to cleanup the old value
   *! on assignment.
   *!
   *! This method is intended for SparseMemory and related functions,
   *! which generally deal with integers and bit-vectors, which will
   *! not try to cleanup old values on assignment.
   */
  bool insertUninitialized(const _KeyType& key, MapEntry** me) {
    _HashType hashType;
    if (carbon_hashtable_maybe_insert(&mHash, (void*) &key,
                                      sHashFn, sEqFn, (void*) this,
                                      hashType.hash(key),
                                      sizeof(MapEntry),
                                      (carbon_hashEntry**) me))
    {
      (*me)->mNext = mChain;
      mChain = *me;
      return true;
    }
    return false;
  }

  //! Insert key,val as separate args, don't care whether new insert occurred
  void insertValue(const _KeyType& key, const _ValueType& val) {
    carbon_hashEntry* e;
    _HashType hashType;
    bool ret = carbon_hashtable_maybe_insert(&mHash, (void*) &key,
                                             sHashFn, sEqFn, (void*) this,
                                             hashType.hash(key),
                                             sizeof(MapEntry), &e);
    MapEntry* me = MapEntry::cast(e);
    if (ret) {
      _HashKeyMgr::copy(key, &(me->mKeyVal.first));
      me->mNext = mChain;
      mChain = me;
    }
    _HashValMgr::copy(val, &(me->mKeyVal.second));
  }

  iterator begin() {
    return iterator(mChain);
  }
  iterator end() {
    return iterator(NULL);
  }

  const_iterator begin() const {
    return const_iterator(mChain);
  }

  const_iterator end() const {
    return const_iterator((MapEntry*)NULL);
  }


  //! Non-STL function, insert and initialize a value in one step
  /*!
    If there is already an entry in the table,  it just returns that;
    otherwise, it creates the entry and initializes it to initVal and
    returns the initialized value.
  */
  typename _HashValMgr::RefType& insertInit(const _KeyType& key, const _ValueType& initVal)
  {
    carbon_hashEntry* e;
    _HashType hashType;
    bool inserted =
      carbon_hashtable_maybe_insert(&mHash, (void*) &key,
                                    sHashFn, sEqFn, (void*) this,
                                    hashType.hash(key),
                                    sizeof(MapEntry), &e);
    MapEntry* mapEntry = MapEntry::cast(e);
    typename _HashValMgr::RefType& val = mapEntry->mKeyVal.second;

    if (inserted) {
      _HashValMgr::copy(initVal, &val);
      _HashKeyMgr::copy(key, &(mapEntry->mKeyVal.first));
      mapEntry->mNext = mChain;
      mChain = mapEntry;
    }
    return val;
  }
  
  typename _HashValMgr::RefType& operator[](const _KeyType& key) {
    carbon_hashEntry* e;
    _HashType hashType;
    int inserted =
      carbon_hashtable_maybe_insert(&mHash, (void*) &key,
                                    sHashFn, sEqFn, (void*) this,
                                    hashType.hash(key),
                                    sizeof(MapEntry), &e);
    MapEntry* mapEntry = MapEntry::cast(e);
    typename _HashValMgr::RefType& val = mapEntry->mKeyVal.second;

    if (inserted) {
      _HashKeyMgr::copy(key, &(mapEntry->mKeyVal.first));
      _HashValMgr::initValue(&val); // zeros opaques, empty ctor for classes
      mapEntry->mNext = mChain;
      mChain = mapEntry;
    }

    return val;
  }
  
  size_t size() const {
    return mHash.sizes.entrycount;
  }
  
  //! This does not reduce the capacity of the hashtable
  void clear() {
    if (mChain != NULL) {
      freeEntries();
      carbon_hashtable_clear(&mHash, sizeof(MapEntry));
      mChain = NULL;
    }
  }

  //! clear the hashtable and remove all the memory
  void clean() {
    if (mChain != NULL) {
      freeEntries();
      carbon_hashtable_clean(&mHash, sizeof(MapEntry));
    }
  }
    
  //! This does not reduces the capacity of the hashtable to store
  //! the existing entries.
  void clear_and_resize() {
    if (mChain != NULL) {
      freeEntries();
      carbon_hashtable_clear_and_resize(&mHash, sizeof(MapEntry));
      mChain = NULL;
    }
  }
 
  bool empty() const {
    return size() == 0;
  }
  
/*
 * UtHashMapFastIter does not support erasing individual elements
 *  void erase(iterator& i) {
 *    value_type& keyval = *i;
 *    _HashKeyMgr::destroy(&keyval.first);
 *    _HashValMgr::destroy(&keyval.second);
 *    carbon_hashtable_iterator_remove_current(&i.mIter, sizeof(MapEntry));
 *  }
 *  
 *  size_t erase(const _KeyType& key) {
 *    carbon_hashtable_itr itr;
 *    if (!doSearch(key, &itr)) {
 *      return 0;
 *    }
 *    MapEntry* me = MapEntry::cast(itr.e);
 *    _HashKeyMgr::destroy(&me->mKeyVal.first);
 *    _HashValMgr::destroy(&me->mKeyVal.second);
 *    carbon_hashtable_iterator_remove_current(&itr, sizeof(MapEntry));
 *    return 1;
 *  }
 */
  
  void swap(UtHashMapFastIter& hs)
  {
    // in general this is C++-unsafe, but this will work
    // because mHash is a C structure.
    struct carbon_hashtable tmp = mHash;
    MapEntry* tmpChain = mChain;
    mHash = hs.mHash;
    mChain = hs.mChain;
    hs.mHash = tmp;
    hs.mChain = tmpChain;
  }

  void freeMemory() { 
    freeEntries();
    carbon_hashtable_clean(&mHash, sizeof(MapEntry));
    createHash();
  }
  

  //! assignment operator
  UtHashMapFastIter& operator=(const UtHashMapFastIter& hs) {
    if (&hs != this) {
      freeMemory();
      copy(hs);
    }
    return *this;
  }
  
  //! copy ctor
  UtHashMapFastIter(const UtHashMapFastIter& hs) {
    createHash();
    copy(hs);
  }

  //! clear, deleting the contained pointer values
  void clearPointerValues() {
    // In this implementation, we are continuing to iterate through
    // a hash table that has deleted stuff in it, and will likely
    // crash if any of the equality/hash functions are called on
    // any of those elements, so this is a little risky, but most
    // obvious hash-table implementions should not be problematic,
    // and this saves the big vector copy.
    for (iterator p = begin(), e = end(); p != e; ) {
      _ValueType v = p->second;
      ++p;
      delete v;
    }
    clear();
  }

  //! clear, deleting the contained pointer keys
  void clearPointerKeys() {
    // In this implementation, we are continuing to iterate through
    // a hash table that has deleted stuff in it, and will likely
    // crash if any of the equality/hash functions are called on
    // any of those elements, so this is a little risky, but most
    // obvious hash-table implementions should not be problematic,
    // and this saves the big vector copy.
    for (iterator p = begin(), e = end(); p != e; ) {
      _KeyType v = p->first;
      ++p;
      delete v;
    }
    clear();
  }

private:
  // Disable comparison until we implement it properly
  bool operator==(const UtHashMapFastIter&) const;

  void copy(const UtHashMapFastIter& hs) {
    for (MapEntry* me = hs.mChain; me != NULL; me = me->mNext) {
      value_type& keyValue = me->mKeyVal;
      insertNoIter(keyValue);
    }
  }

  struct carbon_hashtable mHash;

  void freeEntries() {
#ifdef CDB
    SInt32 sz = size();
#endif
    for (MapEntry* me = mChain; me != NULL; me = me->mNext) {
#ifdef CDB      
      INFO_ASSERT(sz > 0, "chain corruption");
      --sz;
#endif
      value_type& keyval = me->mKeyVal;
      _HashKeyMgr::destroy(&keyval.first);
      _HashValMgr::destroy(&keyval.second);
    }
#ifdef CDB      
    INFO_ASSERT(sz == 0, "items remaining");
#endif
  }

  void createHash() {
    CARBON_HASHTABLE_INIT(&mHash);
    mChain = NULL;
  }

  MapEntry* mChain;
};

#endif

