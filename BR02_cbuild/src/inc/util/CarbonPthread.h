// -*-c++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBONPTHREAD_H__
#define __CARBONPTHREAD_H__

/*!
  \file Wrapper around pthread header file
*/

// Suppress weak pthread symbols.  We want linking to fail if the user
// forgets to use -lpthread.  Otherwise strange crashes occur.
#define _GLIBCXX_GTHREAD_USE_WEAK 0
#include <pthread.h>

#endif
