// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _LOOP_H_
#define _LOOP_H_

#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

//! Loop iterator base class
/*!
  This class does all the work for the various Loop classes. Due to
  compiler restrictions with Sun's SparcWorks compilers and constant
  restrictions with gcc 2.95 iterator types must be handled
  carefully. This class allows us to just handle them explicitly.
*/
template <class _ContainerType, class _Iterator, class _ValueType, class _RefType> class BaseLoop
{
public: CARBONMEM_OVERRIDES
  typedef _ValueType value_type;
  typedef _RefType reference;
  typedef _Iterator iterator;

  BaseLoop()
  {
    /* This ensures that a loop created with this constructor will be
     * atEnd, i.e., empty even if we don't know what values mPtr and
     * mEnd have upon construction.  This way 'Loop<_ContainerType>()'
     * will yield an empty iterator, which is useful for returning
     * empty loops from functions.
     */
#if _HAS_ITERATOR_DEBUGGING
    // Visual C++ verifies at runtime that iterators are valid in
    // debug builds, so when that's the case use a static container so
    // we can have real iterators rather than the "singular" value.
    mEmptyContainer = new _ContainerType;
    mPtr = mEmptyContainer->begin();
    mEnd = mEmptyContainer->end();
#else
    mPtr = mEnd;
#endif
  }

  //! Construct a Loop using the container's begin() and end() methods
  BaseLoop(_ContainerType& container):
    mPtr(container.begin()),
    mEnd(container.end())
#if _HAS_ITERATOR_DEBUGGING
    ,mEmptyContainer(NULL)
#endif
  {
  }

  //! Construct a Loop using the container's begin() and end() methods
  /*
  BaseLoop(const_reference container):
    mPtr(container.begin()),
    mEnd(container.end())
#if _HAS_ITERATOR_DEBUGGING
    ,mEmptyContainer(NULL)
#endif
  {
  }
  */

  //! Construct a Loop using explicitly specified begin and end points
  BaseLoop(const iterator& begin, const iterator& end):
    mPtr(begin),
    mEnd(end)
#if _HAS_ITERATOR_DEBUGGING
    ,mEmptyContainer(NULL)
#endif
  {
  }

  //! Copy constructor to help the user manage & pass around Loops.
  BaseLoop(const BaseLoop& src):
    mPtr(src.mPtr),
    mEnd(src.mEnd)
#if _HAS_ITERATOR_DEBUGGING
    ,mEmptyContainer(NULL)
#endif
  {
  }

#if _HAS_ITERATOR_DEBUGGING
  ~BaseLoop()
  {
    delete mEmptyContainer;
  }
#endif

  //! Assignment operator to help the user manage & pass around Loops.
  BaseLoop& operator=(const BaseLoop& src)
  {
    if (&src != this)
    {
      mPtr = src.mPtr;
      mEnd = src.mEnd;
    }
    return *this;
  }

  //! Gets the value.  Works just like a native STL iterator
  reference operator*() const
  {
    return (reference)*mPtr;
  }
  
  //! Move forward through the iteration.  Just like STL
  iterator& operator++()
  {
    ++mPtr;
    return mPtr;
  }

  //! Detect if we are at the end.  Just like (p == container.end()) in STL.
  bool atEnd() const
  {
    return mPtr == mEnd;
  }

  bool operator()(value_type* ptr)
  {
    if (atEnd())
      return false;
    *ptr = *mPtr;
    ++mPtr;
    return true;
  }

  bool operator()(const value_type** ptr)
  {
    if (atEnd())
      return false;
    const value_type& ref = *mPtr;
    *ptr = &ref;
    ++mPtr;
    return true;
  }

  //! The current location of the loop (at the start if you never did ++Loop
  iterator begin() {return mPtr;}
  
  //! The end of the loop
  iterator end() {return mEnd;}

protected:
  iterator mPtr;
  iterator mEnd;
#if _HAS_ITERATOR_DEBUGGING
  _ContainerType* mEmptyContainer;
#endif
};

//! Loop template class
/*!
  This class provides a mechanism for a class to expose iteration over
  its STL containers, without exposing the functionality of the
  container. The idea is to give the user a terse, fast, forward
  iteration through a structure.  If the user wants anything more
  sophisticated than that, he can use the iterator to populate his own
  private, self-managed container.
 
  We don't want to expose anything more sophisticated than forward iteration
  because we want to allow a class implementor to choose between container
  classes without invaliding exposed iterators.
 
  If you want to iterate through a map, it's somewhat more elegant to use
  LoopMap, which has explicit getValue and getKey methods.
 
  \verbatim
  Example:
 
  class NUModule: public NUUseDefNode
  {
    ...
    typedef Loop<NUNetList> NetListLoop;
    NetListLoop loopInputPorts() {return NetListLoop(mInputNetList);}
    ...
  }
 
  ...
  for (NUModule::NetListLoop l = mod->loopInputPorts(); !l.atEnd(); ++l)
  {
    NUNet* net = *l;
    ...
  }
  \endverbatim

  If you want your loop to use reverse iteration, you can specify the
  reverse_iterator when you declare your loop template, and use the
  second constructor form which takes explicit begin/end arguments:

  \verbatim
  typedef UtVector<FLNodeElab*> NodeVector;
  typedef Loop<NodeVector, NodeVector::reverse_iterator> Iterator;
  Iterator loopAsyncs() {
  return Iterator(mAsyncSchedule.rbegin(), mAsyncSchedule.rend());
  }
  \endverbatim

  Note that while this class does not expose iteration capabilities
  of the container class, it does in effect expose the container class,
  and the container's iterator class, to the caller.  This makes it
  difficult for the caller to pass iterators to generic functions.

  In the scheduling case, there are asynchronous, combinational, and
  sequential schedules, each of which iterate over FLNodeElab*.  But
  the scheduler may want to use a reverse-vector for asynchronous,
  a forward vector for combinational, and a deque for sequential.
  It would be impossible to express these all as an Iterator as
  described above.  The appropriate thing then is to switch to 
  class Iter, which with a small runtime penalty, completely abstracts
  the container & iterator type from the caller.  Please see the
  documentation for class Iter for details.
*/
template <typename _ContainerType> 
class Loop : public BaseLoop<_ContainerType,
                             typename _ContainerType::iterator,
                             typename _ContainerType::value_type,
                             typename _ContainerType::reference
                             >
{
  typedef BaseLoop<_ContainerType,
                   typename _ContainerType::iterator,
                   typename _ContainerType::value_type,
                   typename _ContainerType::reference
                   > _BaseLoop;

public: CARBONMEM_OVERRIDES
  typedef typename _ContainerType::iterator iterator;
  typedef typename _ContainerType::reference reference;

  //! Default constructor
  /*!
    Needed for iterator returns such as
    \verbatim
    Loop<myObj> iter;
    if (getIter(&iter))
    ...
    \endverbatim
    \warning Behavior is undefined if methods are used after using
    this constructor and the iterator is not properly initialized.
  */
  Loop()
  {
  }

  Loop(_ContainerType& container) : _BaseLoop(container)
  {
  }
  
  //! Construct a Loop using explicitly specified begin and end points
  Loop(const iterator& begin, const iterator& end) 
    : _BaseLoop(begin, end)
  {
  }
  
  //! Copy constructor to help the user manage & pass around Loops.
  Loop(const Loop& src) : _BaseLoop(src)
  {
  }
};

// Partial specialization for const container.
template <typename _ContainerType> 
class Loop<const _ContainerType> : public BaseLoop<const _ContainerType,
                             typename _ContainerType::const_iterator,
                             typename _ContainerType::value_type,
                             typename _ContainerType::const_reference
                             >
{
  typedef BaseLoop<const _ContainerType,
                   typename _ContainerType::const_iterator,
                   typename _ContainerType::value_type,
                   typename _ContainerType::const_reference
                   > _BaseLoop;

public: CARBONMEM_OVERRIDES
  typedef typename _ContainerType::const_iterator const_iterator;
  typedef typename _ContainerType::const_reference const_reference;

  //! Default constructor
  /*!
    Needed for iterator returns such as
    \verbatim
    Loop<myObj> iter;
    if (getIter(&iter))
    ...
    \endverbatim
    \warning Behavior is undefined if methods are used after using
    this constructor and the iterator is not properly initialized.
  */
  Loop()
  {
  }

  Loop(const _ContainerType& container) : _BaseLoop(container)
  {
  }
  
  //! Construct a Loop using explicitly specified begin and end points
  Loop(const const_iterator& begin, const const_iterator& end) 
    : _BaseLoop(begin, end)
  {
  }
  
  //! Copy constructor to help the user manage & pass around Loops.
  Loop(const Loop& src) : _BaseLoop(src)
  {
  }
};

// This class is no longer necessary now that Loop is partially specialized
// for a const container.
template <typename _ContainerType>
class CLoop : public BaseLoop<const _ContainerType,
                              typename _ContainerType::const_iterator,
                              typename _ContainerType::value_type,
                              typename _ContainerType::const_reference
                              >
{
  typedef BaseLoop<const _ContainerType,
                   typename _ContainerType::const_iterator,
                   typename _ContainerType::value_type,
                   typename _ContainerType::const_reference
                   > _BaseLoop;
public: CARBONMEM_OVERRIDES

  //! BaseLoop::BaseLoop(_ContainerType&)
  CLoop(const _ContainerType& container) : _BaseLoop(container)
  {}

  //! Construct a Loop using explicitly specified begin and end points
  CLoop(const typename _ContainerType::const_iterator& begin,
        const typename _ContainerType::const_iterator& end): _BaseLoop(begin, end) {}

  //  CLoop(const CLoop& src): _BaseLoop(static_cast<const _BaseLoop>(src)) {}
  CLoop() {}
};


//! Reverse-iterator loop
template <typename _ContainerType>
class RLoop: public BaseLoop<_ContainerType,
                             typename _ContainerType::reverse_iterator,
                             typename _ContainerType::value_type,
                             typename _ContainerType::reference
                             >
{
  typedef BaseLoop<_ContainerType,
                   typename _ContainerType::reverse_iterator,
                   typename _ContainerType::value_type,
                   typename _ContainerType::reference
                   > _BaseLoop;
  
public: CARBONMEM_OVERRIDES

  //! Construct a Loop using the container's begin() and end() methods
  RLoop(_ContainerType& container) :
    _BaseLoop(container.rbegin(),container.rend()) 
  {}

  //! Construct a Loop using explicitly specified begin and end points
  RLoop(const typename _ContainerType::reverse_iterator& begin,
        const typename _ContainerType::reverse_iterator& end): _BaseLoop(begin, end) {}

  RLoop(const RLoop& src): _BaseLoop(src) {}
  RLoop() {}
};


//! Reverse-iterator const loop
template <typename _ContainerType>
class CRLoop: public BaseLoop<const _ContainerType,
                              typename _ContainerType::const_reverse_iterator,
                              typename _ContainerType::value_type,
                              typename _ContainerType::const_reference
                              >
{
  typedef BaseLoop<const _ContainerType,
                   typename _ContainerType::const_reverse_iterator,
                   typename _ContainerType::value_type,
                   typename _ContainerType::const_reference
                   > _BaseLoop;
  
public: CARBONMEM_OVERRIDES

  //! Construct a Loop using the container's rbegin() and rend() methods
  CRLoop(_ContainerType& container) :
    _BaseLoop(container.rbegin(),container.rend())
  {}

  CRLoop(const _ContainerType& container) :
    _BaseLoop(container.rbegin(),container.rend())
  {}

  //! Construct a Loop using explicitly specified begin and end points
  CRLoop(const typename _ContainerType::const_reverse_iterator& begin,
         const typename _ContainerType::const_reverse_iterator& end): _BaseLoop(begin, end) {}

  CRLoop(const CRLoop& src): _BaseLoop(src) {}
  CRLoop() {}
};

#endif // _LOOP_H_
