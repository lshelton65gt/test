// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtIStream_h_
#define __UtIStream_h_


/*!
  \file
  Carbon replacement for C++ IOStreams, which is a very thin wrapper
  over C stdio.  The rationale here is that IOStream implementations
  are specific to the C++ compiler, and our users may wish to use a
  different C++ compiler from the one we are using.  If we rely on
  our compilers runtime IOStream implementation there will be clashes
  if the user tries to use a different C++ compiler.

  This doesn't replace any sophisticated formatting done in the iostream
  style.  That will have to be hacked.  This is meant to simplify the
  random uses of UtIO::cout() << "foo" << x << y ...;
*/

#include "util/CarbonTypes.h"
#include "util/UtString.h"
#ifndef __UtIO_h_
#include "util/UtIO.h"
#endif

class UtIStream;
class UtFileBuf;
template<bool _stype> class BVref;
class DynBitVector;
class UtICheckpointStream;
class UtOCheckpointStream;

//! Input stream class
/*!
  This is to be used in place of std::istream.
  This should only be used as a formatter for the derived classes.
*/
class UtIStream: public UtIOStreamBase
{
public: CARBONMEM_OVERRIDES
  enum {cDefaultBufferSize = 64 * 1024};

  //! constructor
  UtIStream(UInt32 bufsiz = cDefaultBufferSize);
  //! virtual destructor
  virtual ~UtIStream();
  //! read a UtString
  UtIStream& operator>>(UtString& s);
  //! Read a bit-vector based on a UInt32* and num bits from the stream
  //! in the current format.
  /*! Note that the caller must know the number.  \sa readExpandBignum
   *! of bits ahead of time
   */
  bool readFixedBignum(UInt32* data, UInt32 numBits);
  //! Read a BitVector or DynBitVector to stream
  template<class BV> bool readBitvec(BV* bv) {
    return readFixedBignum(bv.getUIntArray(), bv.size());
  }
  //! Read an pre-sized bitvector
  UtIStream& operator>>(DynBitVector& bv);

  //! Read an arbitrary sized bitvector
  /*! If sizeToToken is true, then size bv based the number of characters
   *! in the token
   */
  bool readDynBitVec(DynBitVector* bv, bool sizeToToken);

  template<class BV> bool readBitVec(BV* bv) {
    return readBitVec(bv->getUIntArray(), bv->size());
  }

  bool readBitVec(UInt32* words, UInt32 numBits);

  //! read a BVref
  UtIStream& operator>>(const BVref<false>& v);
  //! Overload
  UtIStream& operator>>(const BVref<true>& v);
  //! Read a UInt64
  UtIStream& operator>>(UInt64& num);
  //! read an SInt64
  UtIStream& operator>>(SInt64& num);
  //! Read a UInt32
  UtIStream& operator>>(UInt32& num);
  //! Read an SInt32
  UtIStream& operator>>(SInt32& num);
  //! Read a UInt16
  UtIStream& operator>>(UInt16& num);
  //! Read an SInt16
  UtIStream& operator>>(SInt16& num);
  //  UtIStream& operator>>(int& num);
  //  UtIStream& operator>>(unsigned int& num);
  // Read a UInt8
  UtIStream& operator>>(UInt8& num);
  //! Read a double
  UtIStream& operator>>(double& num);
  //! Read a char
  UtIStream& operator>>(char& ch);
  //! Read a void* as a hex number
  UtIStream& operator>>(void*& ptr);
  //! Set the radix
  UtIStream& operator>>(UtIO::Radix radix);
  //! Set the floatmode
  UtIStream& operator>>(UtIO::FloatMode floatMode);
  //! Set the width
  UtIStream& operator>>(UtIO::Width w);
  //! Set the max width
  UtIStream& operator>>(UtIO::MaxWidth w);
  //! Set the precision
  UtIStream& operator>>(UtIO::Precision p);
  //! Set the fill char
  UtIStream& operator>>(UtIO::Fill f);
  //! Flush the buffer
  UtIStream& operator>>(UtIO::Flush f);
  //! set the left/right/internal justify mode
  UtIStream& operator>>(UtIO::FieldMode fieldMode);
  //! control slashification of output strings
  UtIStream& operator>>(UtIO::Slashification);
  //! read an arbitrary type that has an appropriate 'read' method
  template<typename _Type>
  UtIStream& operator>>(_Type& val) {
    (void) val.read(*this);
    return *this;
  }

  //! Read past an expected token.  If the next token is not as expected,
  //! fail() is asserted, with getErrmsg() giving details
  UtIStream& operator>>(const char* keyword);

  //! Get the error messages for a failed operator>> 
  /*! The >> operators can fail in scenarios including:
   *!   - out of disk space
   *!   - other network error
   *!   - when the stream was not successfully opened
   *! But operator>> has no mechanism to report error messages.
   *! All the error messages between calls to getErrmsg are
   *! accumulated in this string, separated by newlines.  After
   *! this routine is called, the message buffer resets.  The string
   *! returned by this call is valid until the next call to
   *! getErrmsg.
   *!
   *! Note that the flush, is_open, and close methods all take
   *! error message buffers directly, and so they do not record
   *! their error messages here.
   */
  virtual const char* getErrmsg();

  //! has end of file been reached?
  bool eof() const;

  //! Default implementation of bad
  virtual bool bad() const;

  //! fail() means that the file is healthy but the contents didn't parse
  /*! fail implies bad, but not vice versa */
  virtual bool fail() const;

  //! returns NULL if bad(), non-null otherwise
  operator void*() const;

  //! get the filename associated with this stream, or NULL if none defined yet
  /*! This is really for derived classes that do define a filename
   * since UtIStream does not have a filename
   */
  virtual const char * getFilename() const;

  //! Read the specified number of characters into the buffer.  No NUL termination.
  /*! \returns the number of characters actually read
   *! That will be smaller than 'len', possibly 0, if EOF is reached.
   *! Other file errors will result in a return value of -1.  Details
   *! of the error can be obtained from getErrmsg();
   */
  UInt32 read(char* buf, UInt32 len);

  //! special case for reading a single character
  UInt32 readChar(char* buf);

  //! special case for reading a UInt32
  UInt32 readUInt32(UInt32* buf);

  //! get a line of text into UtString* buffer.  The delimiter *is* included
  bool getline(UtString* buf, char delim = '\n');

  //! return current file position
  SInt64 tell() const {return mPos;}

  //! insert characters into the input buffer and clear error status
  /*! If you unget more characters than have ever been read from the file,
   *! then subsequent tell() operations may yield negative numbers.
   */
  void unget(const char*, UInt32 len);

  //! reset the input buffer
  void reset();

  //! strip CR characters inserted by DOS from getline()
  void putStripCR(bool stripCR) {mStripCR = stripCR;}

  //! record an error, accessible by getErrmsg
  void reportError(const char* errmsg);

  //! Seek to a position.  Flag is UtIO::seek_cur UtIO::seek_set UtIO::seek_end
  virtual SInt64 seek(SInt64 pos, UtIO::SeekMode mode) = 0;

  //! Retry reading a file that may have been appended to since last read
  /*!
   *! When reading a file that is simultaneously being written,
   *! UtIStream can mistakenly cache mEOF because the previous read
   *! ran out of bytes.  If the caller has evidence this file may
   *! have been updated (e.g. from OSStatFile) then it can call this
   *! method to clear the mEOF flag
   */
  void clearEOF() {mEOF = false;}

protected:
  //! For this base class to implement efficient parsing, the derived
  //! classes must support a mechanism to access a read-buffer that's
  //! got N characters (if available).
  /*! This is only called by the
   *! higher level routines like operator>> and getline.  If the user
   *! only calls read(), then this routine will not be called.  So if
   *! the user wants unbuffered access, a derived class can support
   *! that as long as the user calls no routines that require buffering.
   */
  bool peakBuffer(UInt32 size, char** buf, UInt32* actualSize,
                  bool skipWhitespace = true);

  //! consume some or all of the bytes that you have peaked at
  void consumeBuffer(UInt32 numBytes);

  //! Get bytes from the physical file -- do not call directly
  //! except from readBytes or you will miss the unget buffers
  virtual UInt32 readFromFile(char* buf, UInt32 len) = 0;

  //! Get more bytes from unget buffers or physical file
  UInt32 readBytes(char* buf, UInt32 len);

  //! Read a delimited buffer, limited to specified field width
  bool readDelimBuffer(UtString* str, const bool* delimSet);

  //! error occurred parsing the file
  bool mFailure;
  //! end of file reached
  bool mEOF;
  //! should we strip \r characters in getline?
  bool mStripCR;

  UtFileBuf* mFileBuf;
  SInt64 mPos;
  UInt32 mNumReads;            // just for diagnostics
  UtString mOSErrmsg;

private:
  //! Save an extra buffer to help with unget, when UtFileBuf fills up
  void ungetSaveBuffer(const char* str, UInt32 len);

  //! error text to be returned by the next call to getErrmsg
  UtString* mErrmsg;
  //! error text previously returned by getErrmsg()
  UtString* mPrevErrmsg;
  //! extra buffer(s) for "unget", needed only if there is no room in mFileBuf
  struct StringCell {
    CARBONMEM_OVERRIDES

    StringCell(const char*, int len, StringCell*);
    UtString str;
    UInt32 index;
    StringCell* next;
  };
  StringCell* mUngetBuffers;
};

//! Class to output to FILE*
/*!
  This class does not own the FILE* and will not close it upon
  deletion, although it will close it on an explicit call to close.
*/
class UtIFileStream: public UtIStream
{
public: CARBONMEM_OVERRIDES
  //! Constructor for general FILE*
  UtIFileStream(FILE* f);

  //~ dtor
  ~UtIFileStream();

  //! Get the FILE*
  FILE* getFile();
  
  //! UtIStream::is_open()
  virtual bool is_open() const;
  //! UtIStream::close()
  virtual bool close();

  bool getline(UtString* buf, char delim = '\n');
  //virtual UInt32 read(char* s, UInt32 size);

  virtual SInt64 seek(SInt64 pos, UtIO::SeekMode flag);

protected:
  virtual UInt32 readFromFile(char* buf, UInt32 len);

  //! Default constructor for derived classes
  UtIFileStream();
  //! The FILE*
  FILE* mFile;
};

//! Class that owns the FILE*
class UtIFStream: public UtIFileStream
{
public: CARBONMEM_OVERRIDES
  //! Opens the file
  UtIFStream(const char* fileName);
  //! will close the file.
  ~UtIFStream();
};

//! StringStream to replace standard string stream
class UtIStringStream: public UtIStream
{
public: CARBONMEM_OVERRIDES
  //! Constructor for use with a const char*
  UtIStringStream(const char* str);
  //! Constructor for use with a const char* and length
  UtIStringStream(const char* str, size_t len);
  //! Constructor for use with a UtString
  UtIStringStream(const UtString& str);
  //! empty constructor (attach to string with str())
  UtIStringStream();

  //! dtor
  ~UtIStringStream();

  //! Attach the stream to a string
  void str(const char* str);
  //! Is the file open?
  virtual bool is_open() const;
  //! Close the file/buffer
  virtual bool close();
  //! Called by UtIStream
  //virtual UInt32 read(char* buf, UInt32 size);

  virtual SInt64 seek(SInt64 pos, UtIO::SeekMode flag);

protected:
  virtual UInt32 readFromFile(char* buf, UInt32 len);

private:
  void init(const char*, size_t);
  const char* mInputBuffer;
  size_t mInputBufferSize;
  size_t mStringPos;
}; // class UtIStringStream: public UtIStream

//! Buffered output stream to replace FILE* -- this is interrupt-safe
class UtIBStream : public UtIStream
{
public: CARBONMEM_OVERRIDES
  //! Constructor - opens the file.  Default buffer size is really 64k
  UtIBStream(const char* fileName,
             UInt32 bufSize = UtIStream::cDefaultBufferSize);

  //! Constructor - restore from checkpoint file.
  UtIBStream(UtICheckpointStream &in);

  //! Destructor - closes the file
  ~UtIBStream();

  //! UtIStream::is_open()
  virtual bool is_open() const;
  //! UtIStream::close() returns false if error
  virtual bool close();

  //! How many times have we had to read to the physical file?
  UInt32 getNumReads() const {return mNumReads;}

  //! get the filename associated with this stream, or empty string if none defined yet
  virtual const char * getFilename() const;

  //! Called by UtIStream
  //virtual UInt32 read(char* s, UInt32 size);

  //! save state of stream to checkpoint file.
  virtual bool save(UtOCheckpointStream &);

  virtual SInt64 seek(SInt64 pos, UtIO::SeekMode flag);

protected:
  virtual UInt32 readFromFile(char* buf, UInt32 len);

  //! open the file descriptor if it is currently closed
  bool open(bool firstTime);

  //! Overridden by UtICStream
  virtual bool getFD();

  //! restore state of stream from checkpoint file.
  virtual bool restore(UtICheckpointStream &);

  // level 1 file descriptor
  int mFD;

  SInt64 mPhysicalPos;

private:
  UtString mFilename;
}; // class UtIBStream : public UtIStream

class UtCachedFileSystem;

//! UtICStream class -- buffered input-stream that shares file descriptors
class UtICStream: public UtIBStream
{
public: CARBONMEM_OVERRIDES
  //! ctor -- opens the file
  UtICStream(const char* filename, UtCachedFileSystem*,
             UInt32 bufSize = UtIBStream::cDefaultBufferSize);

  //! Constructor - restore from checkpoint file
  UtICStream(UtICheckpointStream &in, UtCachedFileSystem* sys);

  //! destructor -- closes the file
  ~UtICStream();

  //! override explicit close to keep track of how we will report is_open
  virtual bool close();

  //! Override is_open because we don't need to know if we are physically
  //! open -- we just need to know if we have *ever* been opened
  virtual bool is_open() const;

protected:

  //! Overrides UtIBStream::getFD to refresh our file descriptor
  virtual bool getFD();

private:
  friend class UtCachedFileSystem;    // allow this class to call invalidFD()

  //! release the physical file for use by another UtICStream
  /*! called only by UtICStreamSystem */
  void releaseFD();

  //! Does this cached file have a currently-active file descriptor?
  bool hasActiveFD() const;

  //! What was the file-cache timestamp at the time of the last physical Read?
  UInt32 getTimestamp() const {return mTimestamp;}

  //! common code used by constructors for initialization
  void initialize(UtCachedFileSystem* sys);

  UtCachedFileSystem* mFileCache;
  bool mIsValid;                // Cannot rely on mFD
  UInt32 mTimestamp;
}; // class UtICStream: public UtIBStream

#endif // __UtIStream_h_
