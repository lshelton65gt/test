/* -*- C++ -*- */
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _C_UTIL_H_
#define _C_UTIL_H_

/*!
  \file
  Utilities needed by C source files. 
*/

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

  /*!
    \brief The same as strerror_r on Linux
  */
  char* carbon_lasterror(char* buf, size_t len);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
