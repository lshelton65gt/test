// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __UtFileBuf_h_
#define __UtFileBuf_h_

#include "util/CarbonTypes.h"
#include "util/UtString.h"

#include <algorithm>            // for std::min

//! File buffer class
/*!
  This handles the buffering mechanism for files.  It was originally written
  by Mark Seneski for Zstream but was pulled out for use in UtCachedFile by
  Joshua Marantz.
*/
class UtFileBuf
{
public: CARBONMEM_OVERRIDES
  //! constructor with buffers size
  UtFileBuf(UInt32 size);

  //! destructor
  ~UtFileBuf();

  //! Set the buffer size
  /*!
    This is not normally needed unless buffering beyond 64kb is
    desired. 
    \warning Any data that is already in the buffer is lost!
    \param size Size of storage in bytes
  */
  void resize(UInt32 size);

  //! Writes s into buffer
  /*!
    Returns the number of bytes written to buffer.
    If the buffer is full, returns 0.
  */
  UInt32 write(const char* s, UInt32 size);

  //! Get the entire buffer and its current size
  /*!
    \param size Pointer to the write index into the buffer
    \returns The beginning of the buffer and modified size
  */
  char* getBufferAll(UInt32* size);

  //! Get the remaining contents of the buffer (not yet consumed
  /*!
    \param size Pointer to the write index into the buffer
    \returns The beginning of the buffer and modified size
  */
  char* getBufferRemaining(UInt32* size);

  //! Returns the buffer write index
  UInt32 getWriteIndex() const;

  //! Returns the buffer read index
  inline UInt32 getReadIndex() const {  return mBufIndexR; }
  
  //! Set the buffer write index
  /*!
    This is meant only for reading files to allow raw access to the
    storage char array. If the controlling class writes to the array
    it has to update the write index.
  */
  void putWriteIndex(UInt32 numBytes);

  //! Set the buffer read index
  /*!
    This is meant for only reading files. Once a file is open, the
    first few bytes are checked to determine if the file is
    compressed or not. If it isn't the read index needs to be reset
    to 0, so the full uncompressed file can be accessed.
  */
  void resetReadIndex();

  //! Read from the buffer len bytes into p.
  /*!
    Returns the number of bytes actually read.
  */
  UInt32 readBuffer(char* p, UInt32 len) {
    UInt32 numRead = std::min(mBufIndexW - mBufIndexR, len);
    MEMCPY(p, &mBuf[mBufIndexR], numRead);
    mBufIndexR += numRead;
    return numRead;
  }

  //! Return the next char to be read.
  /*!
    \warning If there is nothing in the buffer then by specification
    what is returned is undefined, although it is guaranteed to be
    some initialized memory.

    \sa isEndBufRead()
  */
  char readPeek() const;

  //! Returns true if there is nothing left in the buffer
  inline bool isEndBufRead() const { return ((mBufIndexW - mBufIndexR) == 0); }
  
  //! Reset the buffer index
  void reset();

  //! Return the size of buffer in bytes
  UInt32 capacity() const;

  //! Return the allowable number of bytes to use
  UInt32 size() const;

  //! Manually increment the read index
  /*!
    Returns the incremented read index
  */
  UInt32 incrReadIndex();

  //! Manually set the read index
  void putReadIndex(UInt32 index);

  //! Returns the buffer starting at mBufIndexR
  /*!
    asserts if the write index is less then the read index
  */
  char* getCurrentPos();

  inline void consume(UInt32 numBytes) {
    mBufIndexR += numBytes;
    if (mBufIndexR == mSize) {
      reset();
    }
  }

  inline bool canRead(UInt32 numBytes) const {
    return ((mBufIndexR + numBytes) <= mBufIndexW);
  }

  void moveRemainingBytesToStart();
  char* getWriteBuf(UInt32* numBytes);

private:
  UInt32 mSize;
  UInt32 mCapacity;
  char* mBuf;
  UInt32 mBufIndexW;
  UInt32 mBufIndexR;

  // forbid
  UtFileBuf();
  UtFileBuf(const UtFileBuf&);
  UtFileBuf& operator=(const UtFileBuf&);
};

#endif // __UtFileBuf_h_
