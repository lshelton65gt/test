// -*- C++ -*-
#ifndef __UtLicenseMsg_h_
#define __UtLicenseMsg_h_
/*****************************************************************************


 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

//! Messaging interface for licensing situations
/*!
  In some rare, but important, situations the licensing code will
  go into a state that should be noted by the application. For
  example, if we are actually queuing licenses, it would be nice to
  tell the application that we are waiting for a license, so a nice
  message can be displayed.
*/
class UtLicense::MsgCB 
{
public:
  CARBONMEM_OVERRIDES

  //! Virtual destructor
  virtual ~MsgCB() = 0;

  //! The license daemon has queued our request
  /*!
    \param feature Name of the FEATURE or INCREMENT that is being
    checked out.
  */
  virtual void waitingForLicense(const char* feature) = 0;

  //! Report that a queued license has been obtained.
  virtual void queuedLicenseObtained(const char* feature) = 0;

  //! Requeuing a license checkout
  /*!
    If the license object suspects that it is in a license checkout
    deadlock with another process, it will relinquish all its
    checked out licenses and queue them up again. This gets called
    when they get queued up again.

    \param featureName Name of the FEATURE or INCREMENT that is being
    checked out.
  */
  virtual void requeueLicense(const char* featureName) = 0;

  //! Relinquishing license
  /*!
    If the license object suspects that it is in a license checkout
    deadlock with another process, it will relinquish all its
    checked out licenses and queue them up again. This gets called
    when they get relinquished.

    \param featureName Name of the FEATURE or INCREMENT that is being
    checked out.
  */
  virtual void relinquishLicense(const char* featureName) = 0;

  //! Exit CB. This must not return!
  /*!
    If we lose the connection to the license server (over a long
    period of time) we must exit. This has a catch all mechanism
    underneath it. If this does not return, the error is printed to
    stderr and then a forced exit happens. 

    \warning All applications that use licensing must define this
    and exit appropriately.

    \param reason The reason why we are exiting. Print this to the
    message context of the application.
  */
  virtual void exitNow(const char* reason) = 0;

};



#endif // __UtLicenseMsg_h_
