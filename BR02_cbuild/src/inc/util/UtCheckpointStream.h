// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtCheckpointStream_h_
#define __UtCheckpointStream_h_


/*!
  \file

  I/O streams used by the model to save and restore checkpoint files.

  The implementation is just a thin wrapper that reduces all << and >>
  operators to a read and write of bytes, which is delegated to the
  functions given at construction time.

  This allows the user to use our default (Zstream) file save
  format, or to supply read/write functions that interact with the
  host simulator, storing our state in the host simulator's checkpoint
  file.
*/

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#ifndef  __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef __carbon_priv_h_
#include "codegen/carbon_priv.h"
#endif

class MsgContext;
class UtString;

//! Base class for checkpoint streams.
/*!
  Provides basic handling of stream name, error messages.
*/
class UtCheckpointStreamBase
{
public: CARBONMEM_OVERRIDES

  // function called when there is an error trying to read or write.
  typedef void (*ErrorFunction)(void *context, UtString &message);

  UtCheckpointStreamBase(void *userContext, const char *name, ErrorFunction errorFunc)
    : mUserContext(userContext), mName(name), mErrorFunc(errorFunc), mFailed(false)
   {};

  const char *getName() {
    return mName;
  }

  bool fail() {
    return mFailed;
  }

protected:
  void          *mUserContext; // user data to be passed to read/write funcs
  const char    *mName;        // name of stream for error messages
  ErrorFunction  mErrorFunc;   // function to call on read/write failure
  bool           mFailed;      // true if an error has been detected
};

//! Read from a checkpoint stream.
/*!
  Maps << and >> operators to a single read function provided by
  the user.
*/
class UtICheckpointStream : public UtCheckpointStreamBase
{
public: CARBONMEM_OVERRIDES
           
  //! typedef for function to read from the checkpoint stream          
  typedef UInt32 (*ReadFunction)(void *context, void *buffer, const UInt32 numBytes);
 
  //! constructor
  UtICheckpointStream(ReadFunction readFunc, void *userContext, const char *name, ErrorFunction errorFunc)
    : UtCheckpointStreamBase(userContext, name, errorFunc), mReadFunc(readFunc)
    {}

  //! string read
  CARBON_SECTION("carbon_checkpoint_code")
  UtICheckpointStream& operator >> (UtString &str);

  //! bool read
  CARBON_SECTION("carbon_checkpoint_code")
  UtICheckpointStream& operator >> (bool& b) {
    read(&b, sizeof(b));
    return *this;
  }

  //! char read
  CARBON_SECTION("carbon_checkpoint_code")
  UtICheckpointStream& operator >> (char& c) {
    read(&c, sizeof(c));
    return *this;
  }

  //! unsigned char read
  CARBON_SECTION("carbon_checkpoint_code")
  UtICheckpointStream& operator >> (unsigned char& c) {
    read(&c, sizeof(c));
    return *this;
  }

  //! signed char read
  CARBON_SECTION("carbon_checkpoint_code")
  UtICheckpointStream& operator >> (signed char& c) {
    read(&c, sizeof(c));
    return *this;
  }

  //! double read
  CARBON_SECTION("carbon_checkpoint_code")
  UtICheckpointStream& operator >> (double& num) {
    read(&num, sizeof(num));
    return *this;
  }
  
  //! SInt32 read
  CARBON_SECTION("carbon_checkpoint_code")
  UtICheckpointStream& operator >> (SInt32& num) {
    read(&num, sizeof(num));
    return *this;
  }

  //! UInt32 read
  CARBON_SECTION("carbon_checkpoint_code")
  UtICheckpointStream& operator >> (UInt32& num) {
    read(&num, sizeof(num));
    return *this;
  }

  //! SInt16 read
  CARBON_SECTION("carbon_checkpoint_code")
  UtICheckpointStream& operator >> (SInt16& num) {
    read(&num, sizeof(num));
    return *this;
  }

  //! UInt16 read
  CARBON_SECTION("carbon_checkpoint_code")
  UtICheckpointStream& operator >> (UInt16& num) {
    read(&num, sizeof(num));
    return *this;
  }

  //! UInt64 read
  CARBON_SECTION("carbon_checkpoint_code")
  UtICheckpointStream& operator >> (UInt64& num) {
    read(&num, sizeof(num));
    return *this;
  }

  //! SInt64 read
  CARBON_SECTION("carbon_checkpoint_code")
  UtICheckpointStream& operator >> (SInt64& num)  {
    read(&num, sizeof(num));
    return *this;
  }

  //! Buffer read
  UInt32 read(void *buffer, UInt32 numBytes);

  //! check that the given token is next in the input stream
  bool checkToken(const char *tag);

 private:
  ReadFunction mReadFunc;
};

//! Write data to a checkpoint stream
/*!
  Maps << and >> operators to a single write function, which is
  provided by the user.
*/
class UtOCheckpointStream : public UtCheckpointStreamBase
{
public: CARBONMEM_OVERRIDES
           
  //! typedef for function to write to the checkpoint stream          
  typedef UInt32 (*WriteFunction)(void *context, const void *buffer, const UInt32 numBytes);
 
  //! constructor
  UtOCheckpointStream(WriteFunction writeFunc, void *userContext, const char *name, ErrorFunction errorFunc)
    : UtCheckpointStreamBase(userContext, name, errorFunc), mWriteFunc(writeFunc)
    {};

  //! string write
  CARBON_SECTION("carbon_checkpoint_code")
  UtOCheckpointStream& operator << (const char *str);

  //! bool write
  CARBON_SECTION("carbon_checkpoint_code")
  UtOCheckpointStream& operator << (const bool &b) {
    write(&b, sizeof(b));
    return *this;
  }

  //! char write
  CARBON_SECTION("carbon_checkpoint_code")
  UtOCheckpointStream& operator << (const char &c) {
    write(&c, sizeof(c));
    return *this;
  }

  //! unsigned char write
  CARBON_SECTION("carbon_checkpoint_code")
  UtOCheckpointStream& operator << (const unsigned char &c) {
    write(&c, sizeof(c));
    return *this;
  }

  //! signed char write
  CARBON_SECTION("carbon_checkpoint_code")
  UtOCheckpointStream& operator << (const signed char &c) {
    write(&c, sizeof(c));
    return *this;
  }
  
  //! double write
  CARBON_SECTION("carbon_checkpoint_code")
  UtOCheckpointStream& operator << (const double &num) {
    write(&num, sizeof(num));
    return *this;
  }
  
  //! SInt32 write
  CARBON_SECTION("carbon_checkpoint_code")
  UtOCheckpointStream& operator << (const SInt32 &num) {
    write(&num, sizeof(num));
    return *this;
  }

  //! UInt32 write
  CARBON_SECTION("carbon_checkpoint_code")
  UtOCheckpointStream& operator << (const UInt32 &num) {
    write(&num, sizeof(num));
    return *this;
  }

  //! SInt16 write
  CARBON_SECTION("carbon_checkpoint_code")
  UtOCheckpointStream& operator << (const SInt16 &num) {
    write(&num, sizeof(num));
    return *this;
  }

  //! UInt16 write
  CARBON_SECTION("carbon_checkpoint_code")
  UtOCheckpointStream& operator << (const UInt16 &num) {
    write(&num, sizeof(num));
    return *this;
  }

  //! UInt64 write
  CARBON_SECTION("carbon_checkpoint_code")
  UtOCheckpointStream& operator << (const UInt64 &num) {
    write(&num, sizeof(num));
    return *this;
  }

  //! SInt64 write
  CARBON_SECTION("carbon_checkpoint_code")
  UtOCheckpointStream& operator << (const SInt64 &num)  {
    write(&num, sizeof(num));
    return *this;
  }

  //! Buffer write
  UInt32 write(const void *buffer, UInt32 numBytes);

  //! Write the given tag to the output stream.  This will be verified by
  //! checkToken() on restore.
  void writeToken(const char *tag);

private:
  WriteFunction mWriteFunc;
};

#endif  // __UtCheckpointStream_h_
