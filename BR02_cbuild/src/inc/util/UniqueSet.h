// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  This file contains template classes to manage unique sets of
  items. This allows the caller to do simple pointer comparisons
  between two sets to see if they are the same.
*/

#ifndef _UNIQUESET_H_
#define _UNIQUESET_H_

#include "util/UtSet.h"

template <typename Item>
class UtUniqueSetFactory
{
private:
  // Abstraction to compare two items
  struct CmpItems
  {
    bool operator()(const Item* i1, const Item* i2) const
    {
      return Item::compare(i1, i2) < 0;
    }
  };

public: CARBONMEM_OVERRIDES
  // Abstraction for a set of items
  typedef UtSet<Item*, CmpItems> ItemSet;

  // Abstraction to compare to item sets
  struct CmpItemSets
  {
    static int compare(const ItemSet* s1, const ItemSet* s2)
    {
      // Check for easy case
      if (s1 == s2)
	return 0;

      // If the sizes are different then they are different
      int size1 = s1->size();
      int size2 = s2->size();
      if (size1 != size2)
	return size1 - size2;

      // Check the items one by one
      typename ItemSet::const_iterator p1 = s1->begin();
      typename ItemSet::const_iterator p2 = s2->begin();
      int cmp = 0;
      for (; p1 != s1->end() && (cmp == 0); ++p1, ++p2)
      {
	const Item* i1 = *p1;
	const Item* i2 = *p2;
	cmp = Item::compare(i1, i2);
      }
      return cmp;
    }
    bool operator()(const ItemSet* s1, const ItemSet* s2) const
    {
      return compare(s1,s2) < 0;
    }
  }; // struct CmpItemSets

  // Abstractions for the factory
  typedef UtSet<ItemSet*, CmpItemSets> UniqueSets;
  typedef typename UtSet<ItemSet*, CmpItemSets>::iterator UniqueSetsIter;

  //! constructor
  UtUniqueSetFactory()
  {
    mUniqueSets = new UniqueSets;
  }

  //! destructor
  ~UtUniqueSetFactory()
  {
    clear();
    delete mUniqueSets;
  }

  //! Function to create a new unique set from an existing set and a new item
  /*! If the existing unique set is NULL a new unique set is created
   *  with just the new item. Otherwise the new item is added to the
   *  passed in set, creating a new unique set.
   */
  const ItemSet* createUniqueSet(const ItemSet* itemSet, Item* item)
  {
    // Create a new set on the stack. If it doesn't match any in our
    // stored sets then we create a new one.
    ItemSet srchSet;

    // Merge the new set, but be careful because the item set can be NULL
    if (itemSet != NULL)
      srchSet = *itemSet;
    srchSet.insert(item);

    // Return the new item set
    return findOrCreateItemSet(&srchSet);
  }

  //! Function to merge two unique sets from two existing sets
  /*! If either set is NULL, the other is returned
   */
  const ItemSet* mergeUniqueSets(const ItemSet* is1, const ItemSet* is2)
  {
    // Check for simple cases first
    if (is1 == NULL) {
      return is2;
    } else if (is2 == NULL) {
      return is1;
    } else if (is1 == is2) {
      return is1;
    }

    // Create a new set and insert both values in
    ItemSet srchSet = *is1;
    srchSet.insert(is2->begin(), is2->end());

    // Return the new set
    return findOrCreateItemSet(&srchSet);
  }

  //! An abstration for a vector of items
  typedef UtVector<Item*> ItemVector;

  //! An iterator for the vector of items
  typedef typename ItemVector::iterator ItemVectorIter;

  //! Function to create a new unique set from a vector of items.
  const ItemSet* createUniqueSet(ItemVector& items)
  {
    // Create a new set on the stack and fill it with the items. If it
    // doesn't exist in our factory we will allocate it belo.w
    ItemSet srchSet;
    for (ItemVectorIter i = items.begin(); i != items.end(); ++i)
    {
      Item* item = *i;
      srchSet.insert(item);
    }

    // Return the new item set
    return findOrCreateItemSet(&srchSet);
  }

  //! Function to empty the factory
  void clear()
  {
    for (UniqueSetsIter p = mUniqueSets->begin();
	 p != mUniqueSets->end();
	 ++p)
    {
      ItemSet* itemSet = *p;
      delete itemSet;
    }
    mUniqueSets->clear();
  }

private:
  // Routine to create a new item set or find an existing one
  const ItemSet* findOrCreateItemSet(ItemSet* itemSet)
  {
    // Check if this already exists
    ItemSet* newSet;
    UniqueSetsIter pos = mUniqueSets->find(itemSet);
    if (pos == mUniqueSets->end())
    {
      // Create and insert the new set
      newSet = new ItemSet;
      *newSet = *itemSet;
      mUniqueSets->insert(newSet);
    }
    else
      // It already exists
      newSet = *pos;
    return newSet;
  }

  // Data for the factory
  UniqueSets* mUniqueSets;
}; // class UtUniqueSetFactory
  

#endif // _UNIQUESET_H_
