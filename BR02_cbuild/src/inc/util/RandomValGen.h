// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef RANDOMVALGEN_H
#define RANDOMVALGEN_H

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#include "util/CarbonTypes.h"
#include <math.h>


/*!
  \file
  This file implents a random number generatator and associated
  adaptor classes for use with STL containers.
*/


//! Random value generating class.
/*! 
  This class will generate a random value in a deterministic way for
  various data types based on an initial seed value.  The generator
  implements<sup>1</sup> the "Mersenne Twister"<sup>2</sup> 
  algorithm.

  \see RandomValue

  \note
  <ol>
  <li>This is heavily modified and Carbonized version of code found at
      http://www.agner.org/random/.
  <li> M. Matsumoto and T. Nishimura. Mersenne twister: a 623-dimensionally 
       equidistributed uniform pseudo-random number generator. <i>
       ACM Transactions on Modeling and Computer Simulation</i> 
       (January 1998) vol. 8, no. 1, pg 3-30.
  <li>The authors have released this algorithm for commercial reuse.
 </ol>
*/
class RandomValGen 
{
  enum RandValGenConstants 
  {
#if 1
    // define constants for MT11213A
    eN = 351,
    eM = 175, 
    eR = 19, 
    eMATRIX_A = 0xE4BD75F5,
    eTEMU = 11, 
    eTEMS = 7, 
    eTEMT = 15, 
    eTEML = 17, 
    eTEMB = 0x655E5280, 
    eTEMC = 0xFFD58000
#else
    // or constants for MT19937
    eN = 624, 
    eM = 397, 
    eR = 31, 
    eMATRIX_A = 0x9908B0DF,
    eTEMU = 11, 
    eTEMS = 7, 
    eTEMT = 15, 
    eTEML = 18, 
    eTEMB = 0x9D2C5680, 
    eTEMC = 0xEFC60000
#endif
  };

 public: CARBONMEM_OVERRIDES
  RandomValGen();
  RandomValGen(SInt32 seed);
  void Reseed(SInt32 seed);

  //! Return a random number in the interval 0.0 <= value < 1.0.
  double DRandom();
  //! Return a random number in the interval min <= value < max.
  double DRRandom(double min, double max);
  //! Return a random number in the interval 0 <= value < 2^32.
  UInt32 URandom();
  //! Return a random number in the interval min <= value <= max.
  UInt32 URRandom(UInt32 min, UInt32 max);
  //! Return a random number in the interval -(2^31) <= value < 2^31.
  SInt32 SRandom();
  //! Return a random number in the interval min <= value <= max.
  SInt32 SRRandom(SInt32 min, SInt32 max);

 private:
  UInt32 mState[eN];
  SInt32 mStateIndex;
};

//! functor class for generating random UInt32s
struct RandomGenUInt32 {
  CARBONMEM_OVERRIDES

  //! default ctor
  RandomGenUInt32(SInt32 seed = 0): mRand(seed), mRange(false) {}
  //! ctor with range
  RandomGenUInt32(SInt32 seed, UInt32 min, SInt32 max)
    : mRand(seed), mMin(min), mMax(max), mRange(true) {}
  //! generate random number
  UInt32 operator()() {
    return mRange ? mRand.URRandom(mMin, mMax)
      : mRand.URandom();
  }
private:
  RandomValGen mRand;
  UInt32 mMin;
  UInt32 mMax;
  bool mRange;
};

//! functor class for generating random UInt32s
struct RandomGenSInt32 {
  CARBONMEM_OVERRIDES

  //! default ctor
  RandomGenSInt32(SInt32 seed = 0): mRand(seed), mRange(false) {}
  //! ctor with range
  RandomGenSInt32(SInt32 seed, SInt32 min, SInt32 max)
    : mRand(seed), mMin(min), mMax(max), mRange(true) {}
  //! generate random number
  SInt32 operator()() {
    return mRange ? mRand.SRRandom(mMin, mMax)
      : mRand.SRandom();
  }
private:
  RandomValGen mRand;
  SInt32 mMin;
  SInt32 mMax;
  bool mRange;
};

//! functor class for generating random doubles
struct RandomGenDouble {
  CARBONMEM_OVERRIDES

  //! default ctor
  RandomGenDouble(SInt32 seed): mRand(seed), mRange(false) {}
  //! ctor with range
  RandomGenDouble(SInt32 seed, double min, double max)
    : mRand(seed), mMin(min), mMax(max), mRange(true) {}
  //! generate random number
  double operator()() {
    return mRange ? mRand.DRRandom(mMin, mMax)
      : mRand.DRandom();
  }
private:
  RandomValGen mRand;
  double mMin;
  double mMax;
  bool mRange;
};

//! Simple algorithm that takes a string and generates a seed from it
/*!
  This macro is numbered (1) in case we want to add different
  generation schemes. 

  
  Key algorithm, e.g, VSP:
  \code
  const char* product = "VSP";
  SInt32 seed = 0;
  seed += product[0];
  seed += product[1];
  seed += product[2];
  seed *= 32;
  
  RandomValGen r(seed);
  key = r.SRandom();
  \endcode
*/
#define RANDOM_SEEDGEN_1(key, str, strSize) \
  do { \
    SInt32 seed = 0; \
    for (UInt32 i = 0; i < (strSize); ++i) { \
      seed += (str)[i]; \
    } \
    seed *= 32; \
    RandomValGen r(seed); \
    key = r.SRandom(); \
  } while (0) /* caller adds semicolon */

#endif  

