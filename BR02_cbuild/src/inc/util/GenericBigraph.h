// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!  \file 
  Declaration of the GenericBigraph class derived from Graph.
  This is a useful general-purpose graph class for representing
  bidirectional directed graphs.  It is possible that specialized
  derivatives of Graph may be lighter-weight for some applications,
  but this implementation is available as a default choice in cases
  where optimized derivatives are not needed.
  
  This implementation is simpler to the GenericDigraph templated
  class. Use GenericBigraph if you need to be able to modify graphs in
  place and back pointers make it more efficient to fix up the
  graph. Otherwise use the GenericDigraph implementation which uses
  less memory.
*/

#ifndef _GENERICBIGRAPH_H_
#define _GENERICBIGRAPH_H_


#include "util/Graph.h"

#include "util/CarbonTypes.h"
#include "util/UtList.h"
#include "util/UtHashSet.h"
#include "util/UtMap.h"
#include "util/Iter.h"
#include "util/Loop.h"
#include "util/LoopFunctor.h"

#include <algorithm>

//! Implements the Graph interface for a directed graph
template<typename UserEdgeData, typename UserNodeData>
class GenericBigraph : public BiGraph
{
public: CARBONMEM_OVERRIDES
  typedef UserEdgeData EdgeDataType;
  typedef UserNodeData NodeDataType;

public:
  //! Constructor an empty graph
  GenericBigraph() : mNodes() {}

  //! Forward class declaration
  class Node;

  //! Represents a directed edge with flags, scratch and user data
  class Edge : public GraphEdge
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    Edge(Node* start, Node* to, UserEdgeData userData) :
      mScratch(0), mStart(start), mTo(to), mUserData(userData), mFlags(0)
    {}

    //! destructor
    ~Edge() {};

    //! retrieve the user data value associated with this edge
    UserEdgeData getData() const { return mUserData; }

    //! Get the to field for this edge
    Node* getEndPoint() const { return mTo; }

    //! Get the start field for this edge
    Node* getStartPoint() const { return mStart; }

  private:
    // The Bigraph class must be a friend to modify the edge data in place
    friend class GenericBigraph;

    //! set the user data value associated with this edge
    void setData(UserEdgeData userData) { mUserData = userData; }

    //! Modify the end point of this edge
    void setEndPoint(Node* to) { mTo = to; }

    //! Modify the start point of this edge
    void setStartPoint(Node* start) { mStart = start; }

    UIntPtr mScratch;        //!< a scratch value/pointer available to algorithms
    Node* mStart;             //!< the node this is an edge from
    Node* mTo;               //!< the node this is an edge to
    UserEdgeData mUserData;  //!< the user data associated with this edge
    UInt32 mFlags;           //!< flag bits available to graph algorithms
  };

  //! Function to get the generic bigraph edge from a graph edge
  Edge* castEdge(GraphEdge* graphEdge) const
  {
    return dynamic_cast<Edge*>(graphEdge);
  }

  //! Function to get the generic bigraph edge from a graph edge
  const Edge* castEdge(const GraphEdge* graphEdge) const
  {
    return dynamic_cast<const Edge*>(graphEdge);
  }

private:
  //! In edge hash function to store edges by their start pointer
  /*! Note that the lessThan operators are not yet implemented. If we
   *  need them to do sorted iteration they can be added.
   */
  class HashInEdge
  {
  public:
    //! constructor
    HashInEdge() {}

    //! hash operator - required for UtHashSet
    size_t hash(const Edge* edge) const { return (size_t)edge->getStartPoint(); }

    //! Equal operator - required for UtHashSet
    bool equal(const Edge* e1, const Edge* e2) const
    {
      return e1->getStartPoint() == e2->getStartPoint();
    }
  };

  //! Out edge hash function to store edges by their to pointer
  /*! Note that the lessThan operators are not yet implemented. If we
   *  need them to do sorted iteration they can be added.
   */
  class HashOutEdge
  {
  public:
    //! constructor
    HashOutEdge() {}

    //! hash operator - required for UtHashSet
    size_t hash(const Edge* edge) const { return (size_t)edge->getEndPoint(); }

    //! Equal operator - required for UtHashSet
    bool equal(const Edge* e1, const Edge* e2) const
    {
      return e1->getEndPoint() == e2->getEndPoint();
    }
  };

  //! Abstraction for a hash manager
  typedef HashMgr<Edge*> HashEdgeMgr;

  //! Abstraction for a set of start edges
  /*! We want a small footprint so we need to pass the types for the
   *  HashType and HashKeyMgr.
   */
  typedef UtHashSet<Edge*, HashOutEdge, HashEdgeMgr> OutEdges;

  //! Abstraction for a set of to edges
  /*! We want a small footprint so we need to pass the types for the
   *  HashType and HashKeyMgr.
   */
  typedef UtHashSet<Edge*, HashInEdge, HashEdgeMgr> InEdges;

public:
  //! Represents a bigraph node with flags, scratch and user data
  class Node : public GraphNode
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    Node(UserNodeData userData = NULL) : 
      mScratch(0), mOutEdges(), mInEdges(), mUserData(userData), mFlags(0)
    {}

    //! destructor
    ~Node()
    {
      // Shouldn't delete a node until it has been removed from the
      // graph
      INFO_ASSERT(mOutEdges.empty(),
                  "Inconsitency in bigraph - delete node that is still attached.");
      INFO_ASSERT(mInEdges.empty(),
                  "Inconsitency in bigraph - delete node that is still attached.");
    }

    //! retrieve the user data value associated with this node
    UserNodeData getData() const { return mUserData; }

  private:
    // The bigraph must be a friend to call the private functions
    friend class GenericBigraph;

    //! Add an edge from this node
    Edge* addOutEdge(Edge* edge)
    {
      typedef typename OutEdges::iterator OutEdgesIter;
      OutEdgesIter pos = mOutEdges.find(edge);
      if (pos == mOutEdges.end()) {
        // Doesn't exist, insert it
        mOutEdges.insert(edge);
        return NULL;
      } else {
        Edge* existingEdge = *pos;
        return existingEdge;
      }
    }

    //! Add an edge to this node
    bool addInEdge(Edge* edge)
    {
      typedef typename InEdges::iterator InEdgesIter;
      std::pair<InEdgesIter, bool> result = mInEdges.insert(edge);
      return result.second;
    }

    //! Find if start edge exists
    bool outEdgeExists(Edge* edge) const
    {
      return mOutEdges.find(edge) != mOutEdges.end();
    }

    //! Find if a to edge exists
    bool inEdgeExists(Edge* edge) const
    {
      return mInEdges.find(edge) != mInEdges.end();
    }

    //! Remove an edge from this node
    void removeOutEdge(Edge* edge)
    {
      mOutEdges.erase(edge);
    }

    //! Remove an edge to this node
    void removeInEdge(Edge* edge)
    {
      mInEdges.erase(edge);
    }

    //! Remove all edges for this node
    /*! The caller is responsible for memory deletion
     */
    void removeEdges()
    {
      mOutEdges.clear();
      mInEdges.clear();
    }

    //! set the user data value associated with this node
    void setData(UserNodeData userData) { mUserData = userData; }

    UIntPtr mScratch;       //!< a scratch value/pointer available to algorithms
    OutEdges mOutEdges;     //!< the list of edges from this node
    InEdges mInEdges;       //!< the list of edges to this node
    UserNodeData mUserData; //!< the user data associated with this node
    UInt32 mFlags;          //!< flag bits available to graph algorithms
  };

  //! Function to get the generic bigraph node from a graph node
  Node* castNode(GraphNode* graphNode) const
  {
    return dynamic_cast<Node*>(graphNode);
  }

  //! Function to get the generic bigraph node from a graph node
  const Node* castNode(const GraphNode* graphNode) const
  {
    return dynamic_cast<const Node*>(graphNode);
  }

  //! Add a node to the graph
  void addNode(GraphNode* node) { mNodes.insert(node); }

  //! And an edge to the graph - returns the duplicate if it was not added
  GraphEdge* addEdge(GraphNode* startGraphNode, GraphEdge* graphEdge)
  {
    // Add it to the start nodes to edge set. If it already exists then
    // we are done.
    Edge* edge = castEdge(graphEdge);
    Node* start = edge->getStartPoint();
    INFO_ASSERT(castNode(startGraphNode) == start,
                "Inconsitency in bigraph -- adding an edge with invalid start");
    Node* to = edge->getEndPoint();
    Edge* existingEdge = start->addOutEdge(edge);
    if (existingEdge == NULL) {
      // We added it to the start node, so it must be added to the to
      // node in the reveverse direction.
      bool added = to->addInEdge(edge);
      INFO_ASSERT(added, "Inconsitency in bigraph -- reverse edge exists");
      return NULL;

    } else {
      // It must exist in the to edge
      INFO_ASSERT(to->inEdgeExists(edge),
                  "Inconsitency bigraph -- reverse edge missing");
    }
    return existingEdge;
  }

  //! Remove and edge from the graph
  void removeEdge(GraphEdge* graphEdge)
  {
    // Make sure the edge exists in both the start and to nodes
    Edge* edge = castEdge(graphEdge);
    Node* start = edge->getStartPoint();
    Node* to = edge->getEndPoint();
    INFO_ASSERT(start->outEdgeExists(edge),
                "Attempt to remove a non-existant edge");
    INFO_ASSERT(to->inEdgeExists(edge),
                "Inconsitency found in a bigraph -- reverse edge missing");

    // Remove it from both edge lists from both points of the edge
    start->removeOutEdge(edge);
    to->removeInEdge(edge);
  }

  //! Move an edge within this graph -- it modifies the end point
  /*! This function removes the edge from the graph, modifies the
   *  edge, and then reinserts it into the graph. No memory allocation
   *  or deallocation occurs.
   *
   *  Returns NULL if the edge was added, it returns the existing edge
   *  if an equivalent edge already existed in the graph.
   */
  GraphEdge* modifyOutEdge(GraphEdge* graphEdge, GraphNode* graphTo)
  {
    // Convert to genericbigraph types
    Edge* edge = castEdge(graphEdge);
    Node* to = castNode(graphTo);

    // First remove the edge in both directions
    removeEdge(edge);

    // Now we can modify the endPoint in place
    edge->setEndPoint(to);

    // Now can can add this edge back into the graph
    return addEdge(edge->getStartPoint(), edge);
  }

  //! Move an in edge within this graph -- it modifies the start point
  /*! This function removes the edge from the graph, modifies the
   *  edge, and then reinserts it into the graph. No memory allocation
   *  or deallocation occurs.
   *
   *  Returns NULL if the edge was added, it returns the existing edge
   *  if an equivalent edge already existed in the graph.
   */
  GraphEdge* modifyInEdge(GraphEdge* graphEdge, GraphNode* graphStart)
  {
    // Convert to genericbigraph types
    Edge* edge = castEdge(graphEdge);
    Node* start = castNode(graphStart);

    // First remove the edge in both directions
    removeEdge(edge);

    // Now we can modify the start point in place
    edge->setStartPoint(start);

    // Now can can add this edge back into the graph
    return addEdge(start, edge);
  }

  //! Transpose an edge (note that it is not legal to transpose an attached edge)
  void transposeEdge(GraphEdge* graphEdge, GraphNode* newToGraphNode)
  {
    // Convert to genericbigraph types
    Edge* edge = castEdge(graphEdge);
    Node* newToNode = castNode(newToGraphNode);

    // Make sure this is a valid transpose
    INFO_ASSERT(edge->getStartPoint() == newToNode,
                "Inconsistency found in graph: invalid transpose");
    INFO_ASSERT(!edge->getStartPoint()->outEdgeExists(edge),
                "Inconsistency found in graph: may not transpose attached edge");

    // Swap the start and to nodes
    edge->setStartPoint(edge->getEndPoint());
    edge->setEndPoint(newToNode);
  }

  //! Get the size of the graph (number of nodes)
  size_t size() const { return mNodes.size(); }

  //! Iterator over all nodes in the graph
  Iter<GraphNode*> nodes()
  {
    return Iter<GraphNode*>::create(Loop<GraphNodeSet>(mNodes));
  }

  //! Iterate over all edges from the given node
  Iter<GraphEdge*> edges(GraphNode* node) const
  {
    Node* n = static_cast<Node*>(node);
    Loop<OutEdges> loop = Loop<OutEdges>(n->mOutEdges);
    typedef CastingFunctor<Edge*,GraphEdge*> EdgeFunctor;
    EdgeFunctor ef;
    typedef LoopFunctor<Loop<OutEdges>, EdgeFunctor> FromFunctor;
    return Iter<GraphEdge*>::create(FromFunctor(loop, ef));
  }

  //! Iterate over all reverse edges from the given node
  Iter<GraphEdge*> reverseEdges(GraphNode* node) const
  {
    Node* n = static_cast<Node*>(node);
    Loop<InEdges> loop = Loop<InEdges>(n->mInEdges);
    typedef CastingFunctor<Edge*,GraphEdge*> EdgeFunctor;
    EdgeFunctor ef;
    typedef LoopFunctor<Loop<InEdges>, EdgeFunctor> ToFunctor;
    return Iter<GraphEdge*>::create(ToFunctor(loop, ef));
  }

  //! Get the endPoint of the edge
  GraphNode* endPointOf(const GraphEdge* edge) const
  {
    const Edge* e = static_cast<const Edge*>(edge);
    return e->getEndPoint();
  }

  //! Get the startpoint of the edge
  GraphNode* startPointOf(const GraphEdge* edge) const
  {
    const Edge* e = static_cast<const Edge*>(edge);
    return e->getStartPoint();
  }

  //! Set the data for a given node
  void setData(Node* node, UserNodeData userData)
  {
    node->setData(userData);
  }

  //! walk over the graph using a supplied GraphWalker
  void walk(GraphWalker* gw);

  //! Set flag bits on a node (OR in flags from mask)
  void setFlags(GraphNode* node, UInt32 mask)
  {
    Node* n = static_cast<Node*>(node);
    n->mFlags |= mask;
  }

  //! Clear flag bits on a node (clear flags that are set in mask)
  void clearFlags(GraphNode* node, UInt32 mask)
  {
    Node* n = static_cast<Node*>(node);
    n->mFlags &= ~mask;
  }

  //! Test if any matching flag bit is set on a node
  bool anyFlagSet(const GraphNode* node, UInt32 mask) const
  {
    const Node* n = static_cast<const Node*>(node);
    return (n->mFlags & mask) != 0;
  }

  //! Test is all matching flag bits are set on a node
  bool allFlagsSet(const GraphNode* node, UInt32 mask) const
  {
    const Node* n = static_cast<const Node*>(node);
    return (n->mFlags & mask) == mask;
  }

  //! Get the full set of flag bits for a node
  UInt32 getFlags(const GraphNode* node) const
  {
    const Node* n = static_cast<const Node*>(node);
    return n->mFlags;
  }

  //! Set the scratch field on a node
  void setScratch(GraphNode* node, UIntPtr scratch)
  {
    Node* n = static_cast<Node*>(node);
    n->mScratch = scratch;
  }

  //! Retrieve the scratch field on a node
  UIntPtr getScratch(const GraphNode* node) const
  {
    const Node* n = static_cast<const Node*>(node);
    return n->mScratch;
  }

  //! Set flag bits on an edge (OR in flags from mask)
  void setFlags(GraphEdge* edge, UInt32 mask)
  {
    Edge* e = static_cast<Edge*>(edge);
    e->mFlags |= mask;
  }

  //! Clear flag bits on an edge (clear flags that are set in mask)
  void clearFlags(GraphEdge* edge, UInt32 mask)
  {
    Edge* e = static_cast<Edge*>(edge);
    e->mFlags &= ~mask;
  }

  //! Test if any matching flag bit is set on an edge
  bool anyFlagSet(const GraphEdge* edge, UInt32 mask) const
  {
    const Edge* e = static_cast<const Edge*>(edge);
    return (e->mFlags & mask) != 0;
  }

  //! Test is all matching flag bits are set on an edge 
  bool allFlagsSet(const GraphEdge* edge, UInt32 mask) const
  {
    const Edge* e = static_cast<const Edge*>(edge);
    return (e->mFlags & mask) == mask;
  }

  //! Get the full set of flag bits for an edge
  UInt32 getFlags(const GraphEdge* edge) const
  {
    const Edge* e = static_cast<const Edge*>(edge);
    return e->mFlags;
  }

  //! Set the scratch field on an edge
  void setScratch(GraphEdge* edge, UIntPtr scratch)
  {
    Edge* e = static_cast<Edge*>(edge);
    e->mScratch = scratch;
  }

  //! Retrieve the scratch field on an edge
  UIntPtr getScratch(const GraphEdge* edge) const
  {
    const Edge* e = static_cast<const Edge*>(edge);
    return e->mScratch;
  }

  //! Remove the set of nodes and all edges incident on them from the graph
  void removeNodes(const GraphNodeSet& removeSet);

  //! Remove all the edges for a node
  /*! The caller is responsible for memory deletion
   */
  void removeEdges(GraphNode* graphNode)
  {
    Node* node = castNode(graphNode);
    node->removeEdges();
  }

  //! Return a copy of the subgraph of the graph made from the given nodes
  /*! Creates a new graph that contain all the nodes in the keepSet as
   *  well as any edges between the nodes in the keepSet.
   *
   *  \param keepSet     the set of nodes to keep in the subgraph
   *
   *  \param copyScratch true if the scratch fields should be copied into the
   *                     subgraph nodes. Normally scratch values are not copied,
   *                     but this flag allows this policy to be reversed,
   *                     in case the scratch fields contain data which is also
   *                     relevant to the copied subgraph nodes.
   */
  virtual Graph* subgraph(GraphNodeSet& keepSet, bool copyScratch = false);

  ~GenericBigraph()
  {  
    // Gather all the nodes in a set
    GraphNodeSet removeSet;
    for (Iter<GraphNode*> n = nodes(); !n.atEnd(); ++n) {
      GraphNode* node = *n;
      removeSet.insert(node);
    }

    // Use the remove nodes function
    removeNodes(removeSet);
  }

private:
  GraphNodeSet mNodes; //!< a set of all nodes in the graph
}; // class GenericBigraph : public Graph


/*!
 * Prune nodes in the remove set and edges incident on them from the graph.
 * Any pointers to the pruned nodes or edges are invalid after the call.
 */
template<typename UserEdgeData, typename UserNodeData>
void 
GenericBigraph<UserEdgeData,UserNodeData>::removeNodes(const GraphNodeSet& removeSet)
{
  // Remove any edges that are no longer needed. We can't delete the
  // nodes in the removeSet till after this loop because it destroys
  // the backpointer set we are cleaning up.
  typedef GraphNodeSet::const_iterator NodeSetIter;
  for (NodeSetIter n = removeSet.begin(); n != removeSet.end(); ++n) {
    // Gather the edges from removed nodes out to other nodes
    GraphNode* node = *n;
    UtSet<GraphEdge*> deleteEdges;
    for (Iter<GraphEdge*> iter = edges(node); !iter.atEnd(); ++iter) {
      GraphEdge* edge = *iter;
      deleteEdges.insert(edge);
    }

    // Gather edges from other nodes in to deleted nodes
    for (Iter<GraphEdge*> iter = reverseEdges(node); !iter.atEnd(); ++iter) {
        GraphEdge* edge = *iter;
        deleteEdges.insert(edge);
    }
      
    // Now delete the edges outside of iteration
    for (UtSet<GraphEdge*>::iterator e = deleteEdges.begin();
         e != deleteEdges.end(); ++e) {
      GraphEdge* graphEdge = *e;
      Edge* edge = castEdge(graphEdge);
      removeEdge(edge);
      delete edge;  
    }
  } // for

  // Delete the nodes and update the nodes in the graph
  for (NodeSetIter n = removeSet.begin(); n != removeSet.end(); ++n) {
    GraphNode* node = *n;
    mNodes.erase(node);
    delete node;
  }
} // GenericBigraph<UserEdgeData,UserNodeData>::removeNodes

/*!
 * Retrieve a copy of the subgraph of this graph made from only nodes in the 
 * supplied keep set, and the edges between those nodes.  The graph returned
 * is an independent copy of a portion of the original graph -- the caller
 * can free the original graph and still use the subgraph.  The caller must
 * delete the subgraph when they are through with it.
 */
template<typename UserEdgeData, typename UserNodeData>
Graph*
GenericBigraph<UserEdgeData,UserNodeData>::subgraph(GraphNodeSet& keepSet,
                                                    bool copyScratch)
{
  // Create a new graph
  GenericBigraph<UserEdgeData,UserNodeData>* sg;
  sg = new GenericBigraph<UserEdgeData,UserNodeData>;

  // copy nodes in subgraph - remember the mapping of the nodes from
  // the original graph to the subgraph
  UtMap<Node*,Node*> nodeMap;
  for (GraphNodeSet::const_iterator n = keepSet.begin(); n != keepSet.end(); ++n)
  {
    Node* gdn = castNode(*n);
    Node* gdn_copy = new Node(gdn->getData());
    sg->addNode(gdn_copy);
    sg->clearFlags(gdn_copy, ~0u);
    sg->setFlags(gdn_copy, getFlags(gdn));
    if (copyScratch)
      sg->setScratch(gdn_copy, getScratch(gdn));
    nodeMap[gdn] = gdn_copy;
  }

  // copy edges in subgraph
  for (GraphNodeSet::const_iterator n = keepSet.begin(); n != keepSet.end(); ++n)
  {
    GraphNode* graphNode = *n;
    Node* start = castNode(graphNode);
    for (Iter<GraphEdge*> iter = edges(start); !iter.atEnd(); ++iter)
    {
      // Get the edge in terms of the bigraph types
      GraphEdge* graphEdge = *iter;
      Edge* edge = castEdge(graphEdge);
      Node* to = edge->getEndPoint();

      // Make sure this edge is to a node in the subgraph
      typename UtMap<Node*,Node*>::iterator pos = nodeMap.find(to);
      if (pos != nodeMap.end()) {
        // It is, create the edge
        Node* newTo = pos->second;
        Node* newStart = nodeMap[start];
        Edge* newEdge = new Edge(newStart, newTo, edge->getData());

        // Add it and clear and set flags as requested
        sg->addEdge(newStart, newEdge);
        sg->clearFlags(newEdge, ~0u);
        sg->setFlags(newEdge, getFlags(edge));
        if (copyScratch)
          sg->setScratch(newEdge, getScratch(edge));
      }
    }
  }
  return sg;
}

#endif // _GENERICBIGRAPH_H_
