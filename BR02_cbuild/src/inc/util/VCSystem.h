// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// This file must be included before any other system files, to avoid
// pulling in exception handling in Visual C++.

#ifndef _VCSYSTEM_H_
#define _VCSYSTEM_H_

#if pfMSVC

/*
 * It appears that including any STL headers, such as <deque>, <list>,
 * or <vector>, brings in <stdexcept>, which fails to compile when
 * exception handling has not been explicitly enabled.  This occurs
 * even if we use /U_HAS_EXCEPTIONS on the command line.  If you don't
 * believe me try compiling UtConv.obj without this.
 *
 * So the way I found to defeat this is to explicitly undefine
 * _HAS_EXCEPTIONS right here and then immediately include
 * <stdexcept>, so that it won't get re-included by some other path.
 * I think there may be something related to pre-compiled headers that
 * is confusing me.  But this seems to work.
 *
 * Another option would be to enable exception handling via /EHsc,
 * /EHs or /EHa (not sure which).
 */
#ifdef _HAS_EXCEPTIONS
#undef _HAS_EXCEPTIONS
#endif
#define _HAS_EXCEPTIONS 0

#include <exception>
// Must define a dummy definition of 'exception' otherwise typeinfo.h fails,
// because it derives from exception.  We will not get a clash because we
// have already included exception and forced it to not do anything
class exception {
public:
};

#include <stdexcept>

#include <cstdlib>

#define _INC_MINMAX             /* minmax.h is useless and harmful */


#endif // pfMSVC
#endif // _VC_SYSTEM_H_
