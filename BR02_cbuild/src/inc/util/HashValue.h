// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003, 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  Classes to declare common hash/compare/eq methods
*/

#ifndef _CARBON_HASH_VALUE_
#define _CARBON_HASH_VALUE_


#ifndef __MemManager_h_
#include "util/MemManager.h"
#endif

#include "util/Util.h"
#include "util/carbon_hashtable.h"

// If this platform's pointers are 64 bits, compress them down to 32 bits.
#if pfLP64
#define SHRUNKENPTR(type) ShrunkenPtr<type>
#else
#define SHRUNKENPTR(type) typename type::ObjectType
#endif

//! HashValue class -- helps define hash functors for hash_set and hash_map,
//! for hashing on objects
template<typename _KeyType>
struct HashValue
{
  CARBONMEM_OVERRIDES

  //! hash operator -- requires class method hash()
  size_t hash(const _KeyType& var) const {
    return var.hash();
  }

  //! equal function
  bool equal(const _KeyType& v1, const _KeyType& v2) const {
    return v1 == v2;
  }

  //! shallow less-than function -- for Microsoft hash tables
  bool lessThan1(const _KeyType& v1, const _KeyType& v2) const {
    return v1 < v2;
  }

  //! lessThan operator -- for sorted iterations
  bool lessThan(const _KeyType& v1, const _KeyType& v2) const {
    return v1 < v2;
  }

};

//! HashPointer class -- helps define hash functors for hash_set and hash_map,
//! for hashing on pointers
template<typename _KeyType, int shift>
struct HashPointer
{
  CARBONMEM_OVERRIDES

  //! hash operator -- relies on pointer value with optional shift
  size_t hash(const _KeyType& var) const {
    return ((size_t) var) >> shift;
  }

  //! lessThan operator -- for sorted iterations -- relies on class operator<
  bool lessThan(const _KeyType& v1, const _KeyType& v2) const {
    return *v1 < *v2;
  }

  //! shallow less-than function -- for Microsoft hash tables
  bool lessThan1(const _KeyType& v1, const _KeyType& v2) const {
    return v1 < v2;
  }

  //! equal operator -- for hashing on pointer value
  bool equal(const _KeyType& v1, const _KeyType& v2) const {
    return v1 == v2;
  }

};

//! HashOpaque32 class -- helps define hash functors for hash_set and hash_map, 
//! for hashing on opaque 32-bit objects (not pointers or C++ objects).
template<typename _KeyType>
struct HashOpaque32
{
  CARBONMEM_OVERRIDES

  //! hash operator -- just returns the value though that isn't much of a
  //! hash function
  size_t hash(const _KeyType& var) const {
    return ((size_t) var);
  }

  //! lessThan operator -- for sorted iterations -- relies on class operator<
  bool lessThan(const _KeyType& v1, const _KeyType& v2) const {
    return v1 < v2;
  }

  //! shallow less-than function -- for Microsoft hash tables
  bool lessThan1(const _KeyType& v1, const _KeyType& v2) const {
    return v1 < v2;
  }

  //! equal operator -- for hashing on pointer value
  bool equal(const _KeyType& v1, const _KeyType& v2) const {
    return v1 == v2;
  }

};

//! HashPointer class -- helps define hash functors for hash_set and hash_map, 
//! for hashing on objects, on a set holding pointers
template<typename _KeyType>
struct HashPointerValue
{
  CARBONMEM_OVERRIDES

  //! hash operator -- relies on pointer value with optional shift
  size_t hash(const _KeyType& var) const {
    return var->hash();
  }

  //! lessThan operator -- for sorted iterations -- relies on class operator<
  bool lessThan(const _KeyType& v1, const _KeyType& v2) const {
    return *v1 < *v2;
  }

  //! shallow less-than function -- for Microsoft hash tables
  bool lessThan1(const _KeyType& v1, const _KeyType& v2) const {
    return v1 < v2;
  }

  //! equal operator -- for hashing on pointer value
  bool equal(const _KeyType& v1, const _KeyType& v2) const {
    return (v1 == v2) || (*v1 == *v2);
  }
};

//
// ShrunkenPtr : Smart pointer class for storing 64-bit pointers in 32-bits
//
// This class is for referencing keys and values that use HashPointerMgr
// in UtHashMap and UtHashSet.  Keys and values are actually stored as
// 32-bit unsigned integers in C (SmallPtr).  Since this class needs to
// allow modifying those small pointers, it's mapped directly on top of
// them by casting them to this class.  For example, if p->v is a SmallPtr
// managed by _HashValMgr you can turn it into a ShrunkenPtr like this:
//    *reinterpret_cast<ShrunkenPtr<_HashValMgr>*>(&p->v)
// This is done for you when necessary by the manager class's getRefType()
// method.

template <class T>		// T is a manager class
class ShrunkenPtr
{
  typedef typename T::ObjectType ObjectType;
 public: CARBONMEM_OVERRIDES
  // We need to provide operator=, so statements like:
  // 	map[key] = val;
  // work.  (The map class's operator[] returns a reference to this class.)
  // By modifying the reference, we modify the 32-bit representation of the
  // pointer stored in the hash map.
  ShrunkenPtr& operator=(const ObjectType& val) {
    mShrunkenVal = T::shrink(val);
    return *this;
  }

  // This class can't provide an ObjectType& conversion, because if
  // the caller modified the temporary expanded value we passed out,
  // the real shrunken value stored in the map would remain unchanged,
  // so the reference semantics would be broken.  (But we could provide
  // a const ObjectType& conversion.)

  operator ObjectType() const { // Conversion to type ObjectType
    // Object and Pointer managers will expand the pointer, while the Opaque32
    // manager will just cast the ShrunkenPtr to the correct type.
    return T::expand(mShrunkenVal);
  }

  // Allow ->
  ObjectType operator->() const {
    return T::expand(mShrunkenVal);
  }

 private:
  ShrunkenPtr();		// Disallow construction
  UInt32 mShrunkenVal;
};


//
// Value Managers
//

//! Value manager for objects with value-semantics for UtHashMap/Set
template<typename _Type>
struct HashObjectMgr
{
  CARBONMEM_OVERRIDES

  typedef _Type ObjectType;
  // Type that's returned by operator[] and in pair returned by iterator.
  // For an object, it's the object.
  typedef _Type RefType;
  typedef const _Type ConstRefType;
  typedef _Type Representation;

  //! determine a byte-size required for storing an object
  static size_t size() { return sizeof(_Type); }

  //! Construct a copy of the object in the specified storage
  static void copy(const _Type& src, void* storage) {
    new (storage) _Type(src);
  }

  //! Destroy an object in place 
  static void destroy(void* storage) {
    _Type* ptr = (_Type*) storage;
    ptr->~_Type();
  }

  static _Type& castToValue(void* storage) {
    return * ((_Type*) storage);
  }

  static void initValue(_Type* ptr) {
    (void) new (ptr) _Type;
  }
};


//! Value manager for pointer types for UtHashMap
template<typename _Type>
struct HashOpaqueMgr
{
  CARBONMEM_OVERRIDES

  typedef _Type ObjectType;
  // Type that's returned by operator[] and in pair returned by iterator
  typedef _Type RefType;
  typedef const _Type ConstRefType;
  typedef _Type Representation;

  //! determine a byte-size required for storing an object
  static size_t size() { return sizeof(_Type); }

  //! Construct a copy of the object in the specified storage
  static void copy(const _Type& src, void* storage) {
    castToValue(storage) = src;
  }

  //! Destroy an object in place 
  static void destroy(void*) {
  }

  //! Cast the number or object pointer to its type
  static _Type& castToValue(/*const*/ void* v)
  {
    return *((_Type*)v);
  }

  static void initValue(_Type* storage) {
    *storage = 0;
  }
};

#define HashPointerMgr HashOpaqueMgr
#define HashObject64Mgr HashOpaqueMgr
#define HashOpaque32Mgr HashOpaqueMgr

// We want the third through fifth template parameters of UtHashMap to
// default intelligently in Util.h.  They need to use different
// classes if the corresponding parameters are pointers than if they
// aren't.  So we define a minimal template class, and specialize it
// (using partial specialization) for the case where the type is a
// pointer and (using complete specialization) where it's an opaque
// type.  Using the fourth and fifth parameters as an example, in the
// normal case we simply derive from HashObjectMgr<T>, in the pointer
// case we derive from HashPointerMgr<T*>, and in the opaque case we
// derive from HashOpaque32Mgr<>.

template<class T> class HashHelper     : public HashValue<T> {};
template<class T> class HashHelper<T*> : public HashPointer<T*> {};
template<> class HashHelper<UInt8>     : public HashOpaque32<UInt8> {};
template<> class HashHelper<SInt8>     : public HashOpaque32<SInt8> {};
template<> class HashHelper<UInt16>    : public HashOpaque32<UInt16> {};
template<> class HashHelper<SInt16>    : public HashOpaque32<SInt16> {};
template<> class HashHelper<UInt32>    : public HashOpaque32<UInt32> {};
template<> class HashHelper<SInt32>    : public HashOpaque32<SInt32> {};
template<> class HashHelper<UInt64>    : public HashOpaque32<UInt64> {};
template<> class HashHelper<SInt64>    : public HashOpaque32<SInt64> {};

template<class T> class HashMgr     : public HashObjectMgr<T> {};
template<class T> class HashMgr<T*> : public HashPointerMgr<T*> {};
template<> class HashMgr<UInt8>     : public HashOpaque32Mgr<UInt8> {};
template<> class HashMgr<SInt8>     : public HashOpaque32Mgr<SInt8> {};
template<> class HashMgr<UInt16>    : public HashOpaque32Mgr<UInt16> {};
template<> class HashMgr<SInt16>    : public HashOpaque32Mgr<SInt16> {};
template<> class HashMgr<UInt32>    : public HashOpaque32Mgr<UInt32> {};
template<> class HashMgr<SInt32>    : public HashOpaque32Mgr<SInt32> {};
template<> class HashMgr<UInt64>    : public HashObject64Mgr<UInt64> {};
template<> class HashMgr<SInt64>    : public HashObject64Mgr<SInt64> {};


// To minimize the footprint of most UtHashSets, we want to avoid
// including member variables for the HashType, HashKeyMgr, and HashValueMgr.
// Those each have sizeof==1, and so we wind up increasing the footprint
// from 8 bytes to 12 bytes, which is bad if you have one of these for
// every NUNet* and FLNode*.  Most of the time we don't need those helper
// classes to be actual member variables, because they have no data.
// But it is possible that a HashValue class may need some sort of local
// state, such as a boolean flag.  DynBitVectorFactory's custom HashValue
// class needs that.  In that case, we must retain the HashType as a member
// variable, which makes it hard to have one body of code to solve this
// problem.

// The answer lies in Yet Another Template Class, which is used
// to wrap the carbon_hashtable*.  We can have two versions of this,
// one that has just the carbon_hashtable* and another one that also
// has the HashValue.

//! UtHashSmallWrapper class for HashValue implementations that do not need state
template<typename _KeyType, typename _HashType>
struct UtHashSmallWrapper {
  CARBONMEM_OVERRIDES

  size_t hash(const _KeyType& var) const {
    _HashType ht;
    return ht.hash(var);
  }

  //! equal function
  bool equal(const _KeyType& v1, const _KeyType& v2) const {
    _HashType ht;
    return ht.equal(v1, v2);
  }

  //! shallow less-than function -- for Microsoft hash tables
  bool lessThan1(const _KeyType& v1, const _KeyType& v2) const {
    _HashType ht;
    return ht.lessThan1(v1, v2);
  }

  //! lessThan operator -- for sorted iterations
  bool lessThan(const _KeyType& v1, const _KeyType& v2) const {
    _HashType ht;
    return ht.lessThan(v1, v2);
  }

  struct carbon_hashtable mHash;
};

//! UtHashBigWrapper class for HashValue implementations that needs state
template<typename _KeyType, typename _HashType>
struct UtHashBigWrapper {
  CARBONMEM_OVERRIDES

  UtHashBigWrapper(_HashType hashType):
    mHashType(hashType)
  {
  }

  size_t hash(const _KeyType& var) const {
    return mHashType.hash(var);
  }

  //! equal function
  bool equal(const _KeyType& v1, const _KeyType& v2) const {
    return mHashType.equal(v1, v2);
  }

  //! shallow less-than function -- for Microsoft hash tables
  bool lessThan1(const _KeyType& v1, const _KeyType& v2) const {
    return mHashType.lessThan1(v1, v2);
  }

  //! lessThan operator -- for sorted iterations
  bool lessThan(const _KeyType& v1, const _KeyType& v2) const {
    return mHashType.lessThan(v1, v2);
  }

  struct carbon_hashtable mHash;
  _HashType mHashType;
};


#endif // _CARBON_HASH_VALUE_
