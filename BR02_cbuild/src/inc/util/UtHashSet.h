// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtHashSet_h_
#define __UtHashSet_h_


#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif
#ifndef _CARBON_HASH_VALUE_
#include "util/HashValue.h"
#endif
#ifndef _LOOP_H_
#include "util/Loop.h"
#endif
#ifndef __MemManager_h_
#include "util/MemManager.h"
#endif
#include <algorithm> // for std::sort in visual c++
#ifndef __Util_h_
#include "util/Util.h"
#endif

#include "util/carbon_hashtable.h"
#include "util/carbon_hashtable_itr.h"
#include <utility> // for std::pair

//! UtHashSet class -- wrapper over a C hash set implementation
/*!
 * 
 * STL's hash implementation is slow and bloated. This class allows
 * for (mostly) the same interface as the HashSet class but with a
 * tremendous speed up and much much less code. The one limitation of
 * this is that you cannot store references, only pointers. You can
 * store objects by allocating them on the heap "under-the-covers"
 * by using HashObjectMgr.
 *
 * Note that the keys are 'managed' by a HashPointerMgr or a HashObjectMgr.
 * See comments in UtHashMap.h for additional information.
 *
 * So, if I wanted to have a set of strings:
 *
 * UtHashSet<UtString>
 *
 * The default template parameters cause this to be equivalent to:
 *
 * UtHashSet<UtString, HashValue<UtString>, HashObjectMgr<UtString> >
 *
 * The second and third template parameters to UtHashSet default as
 * follows via the magic of template specialization:
 *
 *  o The second parameter defaults to HashPointer<> if the first
 *    parameter is a pointer, to HashOpaque32<> if it's an integral type,
 *    and to HashValue<> otherwise.
 *
 *  o The third parameter defaults to HashPointerMgr<> if the first
 *    parameter is a pointer, to HashOpaque32Mgr<> if it's an integral
 *    type, and to HashObjectMgr<> otherwise.
 *
 * This all means that you can usually rely on the defaults, though to
 * provide your own hash() function you'd use HashPointerValue<> rather
 * than HashPointer<> for the second parameter.
 *
 * See HashSet for HashValue and other related documentation.
 */
template<typename _KeyType, typename _HashType, typename _HashKeyMgr,
         typename _HashWrapper>
class UtHashSet
{
public: CARBONMEM_OVERRIDES
  struct SetEntry : public carbon_hashEntryStruct {
    typename _HashKeyMgr::Representation mKey;
    static SetEntry* cast(carbon_hashEntryStruct* ptr) {
      return (SetEntry*) ptr;
    }
    static const SetEntry* cast(const carbon_hashEntryStruct* ptr) {
      return (const SetEntry*) ptr;
    }
  };

  //! Normal iterator implementation
  /*!
    Do not instantiate.
  */
  struct iterator : public std::iterator<std::forward_iterator_tag, _KeyType> {
    CARBONMEM_OVERRIDES

    iterator() {
      CARBON_HASHTABLE_ITERATOR_NULLINIT(&mIter);
    }

    iterator(UtHashSet* hs, carbon_hashEntry* e) {
      mIter.h = &(hs->mHashWrapper.mHash);
      mIter.e = e;
#ifdef CDB
      mIter.mNoAdvance = 1;
      mIter.index = 0xdeadbeef;
      mIter.prev = (carbon_hashEntry*) 0xdeadbeef;
#endif
    }

    iterator(const struct carbon_hashtable_itr& iter) :
      mIter(iter)
    {
    }

    typename _HashKeyMgr::RefType* getPtr() const {
      SetEntry* se = SetEntry::cast(mIter.e);
      return &(se->mKey);
    }

    typename _HashKeyMgr::RefType* operator->() const {
      SetEntry* se = SetEntry::cast(mIter.e);
      return &(se->mKey);
    }
    typename _HashKeyMgr::RefType& operator*() const {
      return *operator->();
    }

    bool operator==(const iterator& __it) const
    { return mIter.e == __it.mIter.e; }
    bool operator!=(const iterator& __it) const
    { return mIter.e != __it.mIter.e; }

    iterator& operator++() {
      (void) carbon_hashtable_iterator_advance(&mIter);
      return *this;
    }

    // iterator operator++(int); Don't allow this

    carbon_hashtable_itr mIter;
    _HashKeyMgr mKeyManager;
  };

  //! Normal const_iterator implementation
  /*!
    Do not instantiate.
  */
  struct const_iterator //: public std::iterator<std::forward_iterator_tag, const _KeyType>
  {
    CARBONMEM_OVERRIDES

    typedef const _KeyType& reference;
    typedef const _KeyType* pointer;
    typedef _KeyType value_type;
    typedef std::forward_iterator_tag iterator_category;
    typedef ptrdiff_t difference_type;

    const_iterator()
    {
      CARBON_HASHTABLE_ITERATOR_NULLINIT(&mIter);
    }

    const_iterator(const struct carbon_hashtable_itr& iter) :
      mIter(iter)
    {
    }

    const_iterator(const iterator& __it) :
      mIter(__it.mIter)
    {
    }

    const typename _HashKeyMgr::RefType* getPtr() const {
      SetEntry* se = SetEntry::cast(mIter.e);
      return &(se->mKey);
    }
    const typename _HashKeyMgr::RefType* operator->() const {
      SetEntry* se = SetEntry::cast(mIter.e);
      return &(se->mKey);
    }
    const typename _HashKeyMgr::RefType& operator*() const {
      return *operator->();
    }

    bool operator==(const const_iterator& __it) const
    { return mIter.e == __it.mIter.e; }
    bool operator!=(const const_iterator& __it) const
    { return mIter.e != __it.mIter.e; }

    const_iterator& operator++() {
      carbon_hashtable_iterator_advance(&mIter);
      return *this;
    }

    // iterator operator++(int); Don't allow this

    carbon_hashtable_itr mIter;
    _HashKeyMgr mKeyManager;
  }; // struct const_iterator

  //! void* abstraction for client data used in hash/eq functions
  typedef void* ClientData;

  //! The non-static wrapper (in a template!) for key hashing
  UInt32 hashFn(const carbon_hashEntry* key) {
    const SetEntry* se = SetEntry::cast(key);
    return mHashWrapper.hash(se->mKey);
  }

  //! The non-static wrapper for key comparison
  int eqFn(const void* key1, const carbon_hashEntry* e) {
    const _KeyType* key1V = (const _KeyType*) key1;
    const SetEntry* se = SetEntry::cast(e);
    return mHashWrapper.equal(*key1V, se->mKey);
  }
  
  //! The static wrapper for passing as procedure ptr
  static UInt32 sHashFn(ClientData hs, const carbon_hashEntry* key) {
    return ((UtHashSet*)hs)->hashFn(key);
  }

  //! The static wrapper for key comparison
  static int sEqFn(ClientData hs, const void* key1,
                   const carbon_hashEntry* e)
  {
    return ((UtHashSet*)hs)->eqFn(key1, e);
  }
  
  //! Constructor
  UtHashSet()
  {
    createHash();
  }
  
  //! Constructor with hashtype
  UtHashSet(_HashType ht):
    mHashWrapper(ht)
  {
    createHash();
  }
  
  //! Destructor
  ~UtHashSet()
  {
    freeEntries();
    carbon_hashtable_clean(&mHashWrapper.mHash, sizeof(SetEntry));
  }

  typedef _KeyType key_type;
  typedef _KeyType value_type;
  typedef typename _HashKeyMgr::RefType& reference;  
  typedef typename _HashKeyMgr::ConstRefType& const_reference;

  typedef Loop<UtHashSet> UnsortedLoop;
  typedef CLoop<const UtHashSet> UnsortedCLoop;
  
  class LoopI;
  friend class LoopI;
  friend struct iterator;
  
  class LoopI
  {
  public: 
    CARBONMEM_OVERRIDES
    typedef const typename _HashKeyMgr::RefType& reference;
    typedef _KeyType value_type;

    struct CmpPtr
    {
      CARBONMEM_OVERRIDES

      CmpPtr(const _HashWrapper* wrapper) : mHashWrapper(wrapper) {}
      bool operator()(const SetEntry* p,
		      const SetEntry* q) const
      {
        return mHashWrapper->lessThan(p->mKey, q->mKey);
      }
      const _HashWrapper* mHashWrapper;
    };

    LoopI() : mHashTable(0), mSorted(0) {}
    LoopI(const UtHashSet& sh)
    {
      mHashTable = &sh.mHashWrapper.mHash;
      mSorted.reserve(sh.size());
      for (const_iterator p = sh.begin(), e = sh.end(); p != e; ++p) {
        SetEntry* nv = SetEntry::cast(p.mIter.e);
        mSorted.push_back(nv);
      }
      std::sort(mSorted.begin(), mSorted.end(), CmpPtr(&sh.mHashWrapper));
      mPtr = mSorted.begin();
      mEnd = mSorted.end();
    }

    // Have to put copy ctor & assign operator for now because
    // we don't have IterSet to wrap this yet.
    LoopI(const LoopI& src)
    {
      copy(src);
    }

    LoopI& operator=(const LoopI& src)
    {
      if (this != &src)
        copy(src);
      return *this;
    }

    bool atEnd() const {return mPtr == mEnd;}
    void operator++() {++mPtr;}
    const typename _HashKeyMgr::RefType& operator*() const {
      SetEntry* se = *mPtr;
      return se->mKey;
    }
    
    bool operator()(_KeyType* ptr) {
      if (atEnd())
        return false;
      SetEntry* se = *mPtr;
      *ptr = se->mKey;
      ++mPtr;
      return true;
    }

    bool operator()(const _KeyType** ptr) {
      if (atEnd())
        return false;
      SetEntry* se = *mPtr;
      *ptr = &se->mKey;
      ++mPtr;
      return true;
    }

  private:
    const carbon_hashtable* mHashTable;
    typedef UtArray<SetEntry*> NameValueVec;
    NameValueVec mSorted;
    typename NameValueVec::iterator mPtr;
    typename NameValueVec::iterator mEnd;

    void copy(const LoopI& src)
    {
      mHashTable = src.mHashTable;
      mSorted = src.mSorted;
      // mSorted may have been reallocated, so fix up iterators.
      mPtr = mSorted.begin() +
        (src.mPtr - const_cast<LoopI&>(src).mSorted.begin());
      mEnd = mSorted.end();
    }
  };

  typedef LoopI SortedLoop;     // would like to wrap with Iter...

  //! create a loop through a sorted image of the objects contained in the hash-map
  /*! To use this method, you must have an operator< defined in the _KeyType. */
  SortedLoop loopSorted() const {return SortedLoop(*this);}

  //! Loop through the objects contained in the map, in an order that can change from run to run
  UnsortedLoop loopUnsorted() {return UnsortedLoop(*this);}

  //! Loop through the objects contained in the map, in an order that can change from run to run
  UnsortedCLoop loopCUnsorted() const {return UnsortedCLoop(*this);}

  const_iterator find(const _KeyType& k) const {
    carbon_hashtable_itr hIter;
    if (doSearch(k, &hIter))
    {
      return const_iterator(hIter);      
    }

    // default is end()
    const_iterator a;
    return a;
  }
  
  iterator find(const _KeyType& k) {
    carbon_hashtable_itr hIter;
    if (doSearch(k, &hIter))
    {
      return iterator(hIter);      
    }

    // default is end()
    iterator a;
    return a;
  }
  
  size_t count(const _KeyType& k) const {
    carbon_hashtable_itr hIter;
    if (doSearch(k, &hIter))
      return 1;
    else
      return 0;
  }

  //! insert an element if it's not already there.  Returns true if an
  //! insert occurred, false if the element was already there.  This
  //! is faster than the STL-compatible insert().
  bool insertWithCheck(const key_type& k) {
    carbon_hashEntry* e;
    if (carbon_hashtable_maybe_insert(&mHashWrapper.mHash, (void*) &k,
                                      sHashFn, sEqFn, (void*) this,
                                      mHashWrapper.hash(k),
                                      sizeof(SetEntry), &e))
    {
      SetEntry* se = static_cast<SetEntry*>(e);
      _HashKeyMgr::copy(k, &(se->mKey));
      return true;
    }
    return false;
  }

  //! Type for the return value of STL-compatible insert()
  typedef std::pair<iterator,bool> IterBoolPair;

  //! STL-compatible form of insert returning an iterator/bool pair
  /*! Note: the return-value of this is expensive to construct and is
   *! rarely used.  Consider using insertWithCheck, which is faster, and
   *! still returns the bool indicating whether the insertion occurred.
   */
  IterBoolPair insert(const key_type& k) { 
    carbon_hashEntry* e;
    bool ret = carbon_hashtable_maybe_insert(&mHashWrapper.mHash, (void*) &k,
                                             sHashFn, sEqFn, (void*) this,
                                             mHashWrapper.hash(k),
                                             sizeof(SetEntry), &e) == 1;
    if (ret) {
      SetEntry* se = static_cast<SetEntry*>(e);
      _HashKeyMgr::copy(k, &(se->mKey));
    }

    return IterBoolPair(iterator(this, e), ret);
  }

  //! Insert from an iterator
  template<typename T> void insert(T b, T e) {
    for (; b != e; ++b) {
      insertWithCheck(*b);
    }
  }

  //! Insert from an loop
  template<typename T> void insertLoop(T loop) {
    for (; !loop.atEnd(); ++loop) {
      insertWithCheck(*loop);
    }
  }

  iterator begin() {
    iterator a;
    if (mHashWrapper.mHash.sizes.entrycount > 0)
    {
      carbon_hashtable_iterator_init(&a.mIter, &mHashWrapper.mHash);
    }
    return a;
  }
  iterator end() {iterator a; return a;} // default constructs to end

  const_iterator begin() const {
    const_iterator a;
    if (mHashWrapper.mHash.sizes.entrycount > 0)
    {
      carbon_hashtable_iterator_init(&a.mIter, 
                 const_cast<struct carbon_hashtable*>(&mHashWrapper.mHash));
    }
    return a;
  }

  const_iterator end() const {const_iterator a; return a;}

  size_t size() const {
    return mHashWrapper.mHash.sizes.entrycount;
  }
  
  void clear() {
    freeEntries();

    // Zeroing the capacity of hash tables when the programmer reduces
    // the chance that we'll get into an n^2 situation iterating over
    // hash-tables if they grow large, get cleared, and then stay small.
    //
    // This did not have much affect on compile time in our existing perf
    // tests but I suspect it will help Nortel.  There is some evidence that
    // this helps runtime and compile memory but DoPerfCheck isn't too
    // reliable.
    carbon_hashtable_clean(&mHashWrapper.mHash, sizeof(SetEntry));
  }
  
  bool empty() const {
    return size() == 0;
  }
  
  void erase(iterator& i) {
    _KeyType& key = *i;
    _HashKeyMgr::destroy(&key);
    carbon_hashtable_iterator_remove_current(&i.mIter, sizeof(SetEntry));
  }
  
  size_t erase(const _KeyType& key) { 
    size_t result = 0;		// Not found

    // First free key and value.
    carbon_hashtable_itr itr;
    if (doSearch(key, &itr)) {
      result = 1;		// Found
      SetEntry* se = SetEntry::cast(itr.e);
      _HashKeyMgr::destroy(&se->mKey);
      carbon_hashtable_iterator_remove_current(&itr, sizeof(SetEntry));
    }
    return result;
  }
  
  void swap(UtHashSet& hs)
  {
    struct carbon_hashtable tmp = mHashWrapper.mHash;
    mHashWrapper.mHash = hs.mHashWrapper.mHash;
    hs.mHashWrapper.mHash = tmp;
  }

  void freeMemory() { 
    freeEntries();
    carbon_hashtable_clean(&mHashWrapper.mHash, sizeof(SetEntry));
    createHash();
  }
  
  //! assignment operator
  UtHashSet& operator=(const UtHashSet& hs)
  {
    if (this != &hs) {
      freeMemory();
      copy(hs);
    }
    return *this;
  }
  
  //! copy ctor
  UtHashSet(const UtHashSet& hs)  {
    createHash();
    copy(hs);
  }
  
  //! Compare two sets by value, using operator== from the KeyType on each elt
  bool operator==(const UtHashSet& hs) const
  {
    if (size() != hs.size()) {
      return false;
    }
    const_iterator not_found = hs.end();
    for (const_iterator p = begin(), e = end(); p != e; ++p) {
      const _KeyType& k1 = *p;
      if (hs.find(k1) == not_found) {
        return false;
      }
    }
    return true;
  }
  
  //! Compare two sets by value, using operator== from the KeyType on each elt
  bool operator!=(const UtHashSet& hs) const {
    return !(*this == hs);
  }

  void clearPointers()
  {
    for (iterator p = begin(); p != end(); ++p)
    {
      _KeyType d = *p;
      delete d;
    }
    clear();
  }

  //! insert the contents of src into this
  void insertSet(const UtHashSet& src) {copy(src);}

  //! remove members of src from this, ignoring any members of src not in this
  void eraseSet(const UtHashSet& src) {
    if (!src.empty()) {
      for (const_iterator p = src.begin(), e = src.end(); p != e; ++p) {
        const _KeyType& key = *p;
        erase(key);
      }
    }
  }    

private:
  _HashWrapper mHashWrapper;

  void copy(const UtHashSet& hs) {
    if (!hs.empty()) {
      for (const_iterator p = hs.begin(), e = hs.end(); p != e; ++p) {
        const _KeyType& key = *p;
        insertWithCheck(key);
      }
    }
  }

  void freeEntries() {
    if (!empty()) {
      for (iterator p = begin(), e = end(); p != e; ++p) {
        _KeyType& key = *p;
        _HashKeyMgr::destroy(&key);
      }
    }
  }

  void createHash()
  {
    CARBON_HASHTABLE_INIT(&mHashWrapper.mHash);
  }
  
  void doInsert(const _KeyType& k, SetEntry** entry)
  {
    carbon_hashEntry* hashEntry;
    carbon_hashtable_insert(&mHashWrapper.mHash, sHashFn,
                            this, &hashEntry, sizeof(SetEntry),
                            mHashWrapper.hash(k));
    *entry = static_cast<SetEntry*>(hashEntry);
    _HashKeyMgr::copy(k, &((*entry)->mKey));
  }

  bool doSearch(const _KeyType& k, struct carbon_hashtable_itr* hIter) const
  {
    CARBON_HASHTABLE_ITERATOR_NULLINIT(hIter);
    return carbon_hashtable_search(&mHashWrapper.mHash, (void*) &k,
                                   sHashFn, sEqFn,
                                   (void*) this, hIter,
                                   mHashWrapper.hash(k)) != 0;
  }
} ; //UtHashSet

#endif
