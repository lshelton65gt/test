/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

//! File LicenseBypass.h
/*!
 *! This file solves the scenario where Carbon wants to issue one license
 *! that serves both a GUI and an engine.  When the engine is run from
 *! the command-line, it must check out a license.  But when the engine
 *! runs underneath the GUI, the GUI has already checked out a license
 *! and so the engine must not check out a second one.
 *!
 *! This was reported in bug7109, for the maxsim wizard and carmgr.
 *!
 *! A general solution to this problem is required because we also want
 *! to have this apply to the compiler GUI and the compiler.  We want
 *! the compiler GUI to check out a compiler license because we want to
 *! increase compiler usage.  The compiler GUI should not be a separate
 *! license because we won't be able to charge as much for it.
 *!
 *! The general idea here is for the GUI to create an ascii token which,
 *! when provided as an environment variable CLB (Carbon License Bypass)
 *! to the subprocess, bypasses the license checking.  Here's strategy:
 *!
 *! 1. Find the absolute system time, in seconds
 *! 2. Scramble the time value (to make it look less like simple time).
 *! 3. Take the MD5, and form a string   "time md5-value"
 *! 4. Encrypt the string using the carbon encrypter, which makes it binary
 *! 5. Encode the string into ascii, so it's suitable for passing through
 *!    the OS to as an environment variable.
 *!
 *! In the engine, we look for that environment variable (CLB) and validate
 *! it by:
 *!
 *! 1. Decoding
 *! 2. Decrypting
 *! 3. Extracting the stored MD5 value and the scrambled time.
 *! 4. Take the MD5 of the scrambled time again and validate that
 *!    it matches the MD5 from the environment variables.
 *! 5. Unscramble the time.
 *! 6. Get the current system time and make sure that not too much time
 *!    has expired.  128 seconds should be plenty of time between subprocess
 *!    launch and validating the license bypass env var.
 *!
 *! So we create an envirnoment variable that, if the user manages to
 *! capture the value, will contain a free license good for two minutes.
 *! I don't believe it will be worth anyone's time to reverse-engineer
 *! the above process, including proprietary encoding, encrypting, MD5,
 *! and time scrambling, algorithms.  That's what it would take to try
 *! and "renew" that huge ascii token.  This is an example of the environment
 *! variable that was produced by this process:
 *!   CLB=pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQvZCQkI2QkJC16AyjpqGloyekIqQnwKDAAKwK6i7z5RzA
 *!   ZzKD7upSkU+R7LCamw>:
 *!
 *! This is all implemented as macros to make it harder to reverse-engineer
 *! by disassembly.
 */

#include "util/OSWrapper.h"
#include "util/UtIStream.h"
#include "util/UtMD5.h"
#include "util/UtSimpleCrypt.h"
#include "util/UtStringUtil.h"
#include "util/Zstream.h"

static int sClbCrypt[3];
static char sClbDecrypt[4];
#define DECRYPT_CLB \
  SCRAMBLECODE3("CLB", sClbCrypt);           \
  sClbDecrypt[3] = '\0';                     \
  SCRAMBLECODE3(sClbCrypt, sClbDecrypt)

// We capture the current time as a number of seconds since 1970 (or
// whatever).  I want to avoid having an environment variable look like
// a time, so I shuffle the bits around a bit in a fashion where they can
// be reversed pretty easily, by applying the same transform again. 
// 
// The purpose of this is to make it look a lot less like a time, in that if
// you tried to do it twice in a row, the delta between the two numbers would
// be harder to predict and spot a pattern.
//
// Beyond that we will ascii-encode it so it won't even be numeric.
// 
#define BITMASK(var, src_bit, dst_bit, size) \
  (((var >> src_bit) & ((1 << size) - 1)) << dst_bit)
#define BITSWAP(var, src_bit, dst_bit, size) \
  BITMASK(var, src_bit, dst_bit, size) | \
  BITMASK(var, dst_bit, src_bit, size)
  
#define SCRAMBLE_TIME(time) \
  BITSWAP(time, 0,  29, 3) | \
  BITSWAP(time, 3,  25, 4) | \
  BITSWAP(time, 7,  23, 2) | \
  BITSWAP(time, 9,  18, 5) | \
  BITSWAP(time, 14, 16, 2)

static int sLicDebugCrypt[16];
static char sLicDebugDecrypt[17];

#define LIC_DEBUG_SETUP                                \
  UtOBStream* debug_log = NULL;                        \
  static UInt32 debug_counter = 0;                     \
  SCRAMBLECODE16("CARBON_LIB_DEBUG", sLicDebugCrypt);  \
  sLicDebugDecrypt[16] = '\0';                         \
  SCRAMBLECODE16(sLicDebugCrypt, sLicDebugDecrypt);    \
  if (getenv(sLicDebugDecrypt) != NULL) {              \
    char a[2];                                         \
    a[0] = 'a'; a[1] = '\0';                           \
    debug_log = new UtOBStream(sLicDebugDecrypt, a);   \
  }

#define LIC_DEBUG_CLEANUP \
  if (debug_log != NULL) {delete debug_log;}
    
#define LIC_DEBUG2(a, b) \
  if (debug_log) {*debug_log << debug_counter++ << ' ' << a << b << '\n';}
#define LIC_DEBUG3(a, b, c) \
  if (debug_log) {*debug_log << debug_counter++ << ' ' << a << b << c << '\n';}
#define LIC_DEBUG4(a, b, c, d) \
  if (debug_log) {*debug_log << debug_counter++ << ' ' << a << b << c << d << '\n';}

// That should be very confusing as I'm not even swapping at byte boundaries

//! add a license-bypass environment variable to a QProcess
#define LICENSE_ENV_SETUP(process)                                        \
  DECRYPT_CLB;                                                            \
  LIC_DEBUG_SETUP;                                                        \
  UtString time_md5, encrypted, encoded, env_var;                         \
  UInt64 current_time = OSGetSecondsSince1970();    /* get time */        \
  LIC_DEBUG2('c', current_time);                                          \
  UInt32 time_low = (UInt32) current_time;                                \
  UInt32 time_high = (UInt32) (current_time >> 32);                       \
  LIC_DEBUG4('t', time_low, '_', time_high);                              \
  time_low = SCRAMBLE_TIME(time_low);               /* scramble */        \
  time_high = SCRAMBLE_TIME(time_high);                                   \
  LIC_DEBUG4('s', time_low, '_', time_high);                              \
  time_md5 << time_low << ' ' << time_high;                               \
  UtMD5 md5;                                                              \
  md5.update(time_md5);                                                   \
  unsigned char md5buf[16];                                               \
  md5.finalize(md5buf);                             /* md5 */             \
  time_md5 << ' ';                                                        \
  time_md5.append((char*) md5buf, 16);                                    \
  ZENCRYPT_STRING(time_md5, &encrypted);                                  \
  StringUtil::asciiEncode(encrypted.c_str(),       /* ascii encode */     \
                          encrypted.size(), &encoded);                    \
  env_var << sClbDecrypt << '=' << encoded;                               \
  LIC_DEBUG2('e', env_var);                                               \
  QStringList env = QProcess::systemEnvironment();                        \
  env << env_var.c_str();                                                 \
  process->setEnvironment(env);                                           \
  LIC_DEBUG_CLEANUP


#define LICENSE_BYPASS_TIMEOUT 128

//! validate the license-bypass environment variable, setting
//! bool variable "bypass" to true if the license check can be bypassed
#define LICENSE_ENV_CHECK(bypass)                                         \
  bypass = false;                                                         \
  DECRYPT_CLB;                                                            \
  LIC_DEBUG_SETUP;                                                        \
  const char* encoded = getenv(sClbDecrypt);                              \
  if (encoded != NULL) {                                                  \
    LIC_DEBUG2('E', encoded);                                             \
    UtString decoded, decrypted, time_md5;                                \
    StringUtil::asciiDecode(encoded,               /* ascii decode */     \
                            strlen(encoded), &decoded);                   \
    ZDECRYPT_STRING(decoded, &decrypted);                                 \
    UtIStringStream is(decrypted);                                        \
    UInt32 time_low, time_high;                                           \
    char space;                                                           \
    unsigned char md5buf[16], env_md5[16];                                \
    if ((is >> time_low) && (is >> time_high) && (is >> space) &&         \
        (space == ' ') && (is.read((char*) env_md5, 16) == 16))           \
    {                                                                     \
      time_md5 << time_low << ' ' << time_high;                           \
      UtMD5 md5;                                                          \
      md5.update(time_md5);                                               \
      md5.finalize(md5buf);                     /* md5 */                 \
      LIC_DEBUG4('S', time_low, '_', time_high);                          \
      time_low = SCRAMBLE_TIME(time_low);       /* descramble */          \
      time_high = SCRAMBLE_TIME(time_high);                               \
      LIC_DEBUG4('T', time_low, '_', time_high);                          \
      UInt64 time_from_env_var = time_low |                               \
        (((UInt64) time_high) << 32);                                     \
      if (memcmp(md5buf, env_md5, 16) == 0) {                             \
        UInt64 current_time = OSGetSecondsSince1970();                    \
        LIC_DEBUG2('C', current_time);                                    \
        SInt64 diff = current_time - time_from_env_var;                   \
        LIC_DEBUG2('D', diff);                                            \
        bypass = (current_time >= time_from_env_var) &&                   \
          (diff < LICENSE_BYPASS_TIMEOUT);                                \
        LIC_DEBUG2('b', bypass);                                          \
      }                                                                   \
    }                                                                     \
  }                                                                       \
  LIC_DEBUG_CLEANUP
