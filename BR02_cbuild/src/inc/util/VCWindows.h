// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003, 2004, 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _VC_WINDOWS_H_
#define _VC_WINDOWS_H_

#define NOGDI                   // To avoid macro conflicts
#include <windows.h>
#undef min
#undef max
typedef LARGE_INTEGER RealtimeType;
typedef struct { FILETIME user; FILETIME system; } CpuStruct;
#else
#include <sys/time.h>
#include <sys/times.h>
typedef timeval RealtimeType;
typedef struct tms CpuStruct;

#endif //  _VC_WINDOWS_H_
