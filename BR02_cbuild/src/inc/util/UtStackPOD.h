// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtStackPOD_h_
#define __UtStackPOD_h_

#include "util/UtStack.h"
#include "util/UtArray.h"

// A stack using a UtArray as its container.  UtArray accepts only POD types
// the size of a pointer or smaller, so this template has those restrictions.
template<class T>
class UtStackPOD : public UtStack<T, UtArray<T> > {
public:
  CARBONMEM_OVERRIDES

  UtStackPOD() {}
};

#endif
