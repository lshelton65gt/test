// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _UT_UINT64_FACTORY_H_
#define _UT_UINT64_FACTORY_H_

#include "util/UtHashSet.h"

class ZostreamDB;
class ZistreamDB;

//! Object which maintains UInt64 pointers
class UtUInt64Factory
{
  //! Pointer value hash for UInt64s
  /*!
    Helper class to define a hash-table of const UInt64*, where
    we don't allow duplicate values (even if the pointers are
    different)
  */
  struct HashUInt64Ptr: public HashPointerValue<const UInt64*> {
    size_t hash(const UInt64* n) const {
      return (((size_t) *n) & 0xffffffff) ^ ((size_t) (*n >> 32));
    }
  };
  
  typedef UtHashSet<UInt64*, HashUInt64Ptr> NumPtrSet;
  
public:
  CARBONMEM_OVERRIDES

  //! Unsorted Loop
  typedef NumPtrSet::UnsortedLoop UnsortedLoop;
  //! Unsorted Constant Loop
  typedef NumPtrSet::UnsortedCLoop UnsortedCLoop;
  //! Sorted Loop
  typedef NumPtrSet::SortedLoop SortedLoop;

  //! constructor
  UtUInt64Factory();

  //! destructor
  ~UtUInt64Factory();

  //! Get or create the corresponding UInt64 pointer
  const UInt64* getNumPtr(UInt64 val);

  //! Get UInt64 equivalent of double
  /*!
    Doubles and UInt64s are the same size on all platforms.

    This converts the double into a UInt64, and then factories the
    UInt64. The UInt64 pointer is then casted back to a double
    pointer.
  */
  const CarbonReal* getRealPtr(CarbonReal val);
  
  //! Simply insert the value into the set
  /*!
    getNumPtr does the same thing except it returns the inserted
    value.
  */
  void insert(UInt64 val);

  //! Number of UInt64s in the set
  UInt32 size() const { return mNums.size(); }
  
  //! Loop unsorted
  UnsortedLoop loopUnsorted() { return mNums.loopUnsorted(); }
  //! Constant Loop unsorted
  UnsortedCLoop loopCUnsorted() const { return mNums.loopCUnsorted(); }
  //! Loop sorted
  SortedLoop loopSorted() const { return mNums.loopSorted(); }

  //! write to the database
  bool dbWrite(ZostreamDB& out) const;

  //! read from the database
  bool dbRead(ZistreamDB& in);
  
private:
  NumPtrSet mNums;

  const UInt64* find(UInt64* val);
};

#endif // _UT_UINT64_FACTORY_H_
