// -*- C++ -*-
#ifndef __UtLicense_h_
#define __UtLicense_h_
/*****************************************************************************


 Copyright (c) 2003-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/CarbonTypes.h"
#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

// forward declaration
class UtString;
class RandomValGen;
class UtStringArray;

//! Object to manage licenses
/*!
  There are 2 modes for checking out licenses: Reference counted, and
  multiple. Reference counting is the default, and it simply checks
  out 1 license of each particular feature or feature name and
  increases the reference count on each subsequent checkout, and
  decreases the reference count on each release. Upon reaching 0, the
  actual checked out license is released.

  Multiple licenses simply checks out another license for each call to
  checkout, and releases exactly 1 license on each call to release.
*/
class UtLicense
{
public: CARBONMEM_OVERRIDES
  //! Enumeration of all license types
  /*! 
   *  All new license enumerations must go at the end of the list (but
   *  before eInvalid).
   *
   *  A reading from the Book of Licensing: In the beginning was
   *  Verilog, and Verilog was with Carbon, and Verilog was Carbon.
   *  From the beginning, Verilog was with Carbon.  All things were made
   *  with Verilog, and without Verilog nothing was made that has been
   *  made.  In Verilog were written designs, and the designs were the
   *  light of our Customers.  And Carbon shone in the EDA market, but
   *  the EDA market said, "Let there be VHDL!"  Then
   *  there came a language, sent by IEEE, whose name was VHDL.  And
   *  VHDL greatly discombobulated the licensing process.
   * 
   *  The eSpeedCompiler enum here was the original license when the
   *  cbuild only handled Verilog.  Once VHDL needed to be licensed,
   *  eSpeedCompiler was deprecated, with eSpeedCompilerVlog replacing
   *  it.  This is why we seemingly have two different licenses for the
   *  same thing. eSpeecCompilerVhdl is for standalone VHDL, and
   *  eSpeedCompilerMixed is used to cosimulate both VHDL and Verilog.
   */
  enum Type
    {
      eSpeedCompiler,      //!< Old SPEEDCompiler (Verilog-only) license DEPRECATED
      eDesignPlayer,       //!< DesignPlayer license
      eMemory,             //!< Memory model license
      eTransactor,         //!< Transactor license
      eDIAGNOSTICS,        //!< Diagnostics licenses for debugging cbuild output
      eSpeedCompilerVlog,  //!< SPEEDCompiler Verilog license
      eSpeedCompilerVhdl,  //!< SPEEDCompiler VHDL license
      eSpeedCompilerMixed, //!< SPEEDCompiler Mixed VHDL & Verilog license
      eCwaveTestbench,     //!< Cwavetestbench license
      eLMToolSP,           //!< LMTool/SP license
      eVSPCompiler,        //!< VSP Compiler license
      eVSPRuntime,         //!< VSP Seat license
      eVSPCompOpt,         //!< VSP Compiler Optimization license DEPRECATED
      eVSPCompVerilog,     //!< VSP Verilog Compiler license DEPRECATED
      eVSPCompVhdl,        //!< VSP Vhdl Compiler license DEPRECATED
      eVSPExecVHM,         //!< VSP VHM Execution license DEPRECATED
      eUnknown,            //!< User-specified license
      eIntfRealViewSOC,    //!< Maxsim Realview SOC interface
      eLMToolSPTimed,      //!< LMTool/SP time-bomb creation license
      eCleartext,          //!< License to allow generating clear-text C++
      eVSPReplay,          //!< Replayable Carbon Model
      eVSPExecPlatArch,    //!< Coware Abstract Registers
      eDumpVerilog,        //!< dumpVerilog license
      eOnDemand,           //!< Runtime OnDemand license
      eModelValidation,    //!< Model Validation
      eSystemCComp,        //!< SystemC Components
      eIPCreator,          //!< IP Creator
      eTimebombBase,       //!< Base feature name for creation of timebombed models
      eSoCDSystemCExport,  //!< Ability to generate an exported SystemC model from a SoCD component
      eVSPCompilerSV,      //!< VSP Compiler license for SystemVerilog (temporary license used during development of support for SystemVerilog)
      eInvalid             //!< Invalid license. This \e must be last
    };
  
  //! Licensing mode enumeration
  enum Mode
    {
      eRefCount, //!< Only checkout single licenses and reference count subsequent checkouts
      eMultiple //!< Checkout an additional license for each checkout
    };

  class MsgCB;

  //! constructor
  /*!
    \param msgCB \e REQUIRED call back implementation
    \param useHeartbeats By default we do not require pthreads and we
    do server/client heartbeats automatically. If this is false
    pthreads is required and a dedicated thread will be used to do the
    heartbeats automatically. 

    Note that we default the client
    non-response timeout to flexlm's default of 7200 seconds. Use
    putServerTimeout to change that.
  */
  UtLicense(MsgCB* msgCB, bool useHeartbeats = true);
  
  //! destructor
  ~UtLicense();

  //! Maximum number of seconds you can set the server timeout to
  static const int cServerTimeoutMax = 15300;

  //! Set the client max non-response time
  /*!
    Flexlm defaults to allowing 7200 seconds without a ping/heartbeat
    response from the client. You can set it up to 15300 seconds. 
    After this time has elapsed without a heartbeat from the client
    the server times out and checks the licenses back in.

    This must be called before any calls to checkout. Behavior is
    undefined if called after licenses are already checked out.

    If you want to set it to the maximum, use the cServerTimeoutMax
    constant.
  */
  void putServerTimeout(int numSeconds);

  //! Set the licensing mode
  /*!
    Sets the licensing mode which includes reference counting and
    multiple license checkout.
    
    The default is to do reference counting. 
  */
  void setMode(Mode mode);

  //! Send a heartbeat to the license server
  /*!
    This is \b REQUIRED. Automatic heartbeats have been disabled. This
    is the only way to guarantee that we have a license checked
    out. This method will send a heartbeat to the server if
    appropriate and will \e not wait for a response unless the server
    is down, in which case, it tries to reconnect.
    If an error occurs during heartbeat resolution the application
    will exit. See setExitCB() to define your own exit strategy.
  */
  void heartbeat();


  //! Check to see if any flavor of the core compiler is licensed
  /*!  This will allow you to check to see if any version of cbuild has
    a feature existing for it without consuming a license.  This feature
    allows the utilities (like SpeedCC) that only requires a cbuild
    license to exist to work, irrespective of the particular flavor of
    cbuild the customer has.  Currently, Verilog, VHDL, and mixed
    Verilog/VHDL are the permitted variations.  NOTE: this routine must
    be updated if additional cbuild licensing versions are created.

    \param reason This is the license server message.  If no valid
    cbuild license exists, or of there was a problem checking for it
    then this will have the license server error in it.
    \returns true if the feature exists, false otherwise.
    \sa doesFeatureExist()
  */
  bool isCompilerLicensed( UtString *reason );

  //! Check if the license feature exists
  /*!
    This will allow you to check if a feature exists in the user's
    license file without consuming a license. Note this is non-const
    because the license object has to be manipulated in order to do
    the check.

    \param feature Check to see if this feature exists
    \param reason This is the license server error message. If the
    feature doesn't exist or if there was a problem checking it then
    this will have the license server error in it.
    \returns true if the feature exists, false otherwise.
  */
  bool doesFeatureExist(Type feature, UtString* reason);

  //! Does user-specifed feature name exist?
  /*!
    \param featureName Name to check for existence in license file
    \param reason Error messaging
  */
  bool doesFeatureNameExist(const char* featureName,
                            UtString* reason);

  //! Get features matching a prefix
  /*!
    Returns an array of all non-expired features beginning with a prefix string.
   */
  void getFeaturesMatchingPrefix(const char* prefix, UtStringArray* matching);

  //! Checkout a license for a feature
  /*!
    \param feature The feature against which a license is checked out.
    \param reason If the license checkout fails the reason why it
    failed is put into this buffer. Must \e not be NULL.
    \returns true if the license was checked out, false otherwise.
  */
  bool checkout(Type feature, UtString* reason);

  //! Checkout a license for a feature by name
  /*!
    This is meant only for licenses that do not have a specific
    license type. The type that the feature name is assigned is
    eUnknown.

    \param featureName Feature name to checkout. 
    \param reason Reason why the checkout fails
    \sa doesFeatureNameExist()
  */
  bool checkoutFeatureName(const char* featureName, 
                           UtString* reason);

  //! Checkout a license for a feature from a list of feature names
  /*!
    This function attempts to check out each feature name in the list,
    in order, until a checkout succeeds.

    This is meant only for licenses that do not have a specific
    license type. The type that the feature name is assigned is
    eUnknown.

    \param featureList List of feature name to attempt to checkout. 
    \param reason Reason why the checkout fails
    \sa doesFeatureNameExist()
  */
  bool checkoutFeatureNameFromList(const UtStringArray& featureList,
                                   UtString* reason);

  //! Is this feature name checked out?
  bool isFeatureNameCheckedOut(const char* featureName) const;

  //! Is this feature checked out?
  /*!
    \returns True if the feature has a checked out license associated
    with it.
  */
  bool isCheckedOut(Type feature) const;

  //! Is any feature name from this list checked out?
  /*!
    \returns True if the any of the features in the list has a checked
    out license associated with it.
  */
  bool isFeatureNameFromListCheckedOut(const UtStringArray& featureList) const;

  //! Check the license back in.
  /*!
    Release a feature
    \param feature Feature being checked back in.
  */
  void release(Type feature);

  //! Release a license by featureName
  /*!
    \sa checkoutFeatureName()

    \param featureName Feature to release. Only 1 reference is
    released.
    If non-null, the first feature with the name will be released.
  */
  void releaseFeatureName(const char* featureName);
  
  //! Release all licenses that are checked out by this process
  /*!
    Use this in lieu of release() to check
    licenses back in when your application is exiting and you just
    want to check in all the licenses that you checked out.
  */
  void releaseAll();
  
  //! Set the installation directory
  /*!
    The 'license' directory is expected to be under this directory and
    the license directory is used as the \e default license
    location. The directory does not have to exist.
    \warning If not set, the installation directory defaults to '/',
    which results in the default license directory location to be
    '/license'.
  */
  void putInstallDir(const UtString* installDir);

  //! get a string describing a license file feature 
  void getFeatureName(UtString*, Type);

  //! Wait for a license, rather than erroring immediately if none is available
  /*!
   *! By default, the value is false, meaning that if there is no license
   *! available, a failure will occur.  If the user prefers to have the
   *! program wait until a license becomes available, putBlocking(true)
   *! should be called.
   */
  void putBlocking(bool block);

  //! Return the license messenging callback object
  MsgCB* getMsgCB();

  //! We are crashing. Check the licenses back in.
  /*! 
    Only call this if we are crashing (and caught in a signal
    handler). This simply checks in all the licenses. Any UtLicense
    calls after calling this has undefined behavior. It is recommended
    to exit after calling this. Do not try to delete the UtLicense (or
    any other memory for that matter).
  */
  void emergencyCheckin();

private:
  //! Enum for checkout status
  enum CheckoutStatus
    {
      eSuccess, //!< License successfully checked out
      eFail, //!< License Not checked out
      eRestart //!< Restart license checkout (all licenses)
    };
  
  static bool isDeprecatedFeature(Type feature);
  // A silly class that counts the number of total licenses currently
  // checked out. It is needed to solve a chicken/egg
  // problem. JobManager uses this and passes it to a Job which needs
  // it in order to decide whether or not to queue. I can't pass a
  // JobManager to a Job without first declaring the interface here, which I
  // don't want to do. So, I'm passing the counter instead.
  class Counter;

  struct LicenseData;
  friend struct LicenseData;

  struct Job;
  friend struct Job;

  /*!
    The job manager manages all the flexlm job ids. There are more
    than one jobid whenever multiple vendor strings are used to
    checkout licenses. We expect VERY FEW vendor strings in a typical
    application, usually 1 or 2. 
    So, the manager keeps an array of Jobs, each having a unique
    vendor string assigned to them. Heartbeats are done on all jobs if
    manual heartbeats are used. Checkins use the appropriate job to
    reference the license feature.

    This is needed to solve bug 5644.
  */
  class JobManager;
  friend class JobManager;
  JobManager* mJobManager;
  Mode mMode;
  MsgCB* mMsgCB;
  RandomValGen* mRandom;

  class FeatureHelper;
  friend class FeatureHelper;
  FeatureHelper* mFeatureHelper;

  void deleteJob(Job* job);

  LicenseData* getLicenseData(Type feature, UtString* featureStr);
  int getNumLicensesToCheckout(LicenseData* lcData);

  CheckoutStatus doCheckout(const char* transFeature, 
                            LicenseData* lcData, 
                            UtString* reason);

  bool relinquishAndRestart(LicenseData* lcDataCause, UtString* reason);
  bool relinquish(const LicenseData* lcDataCause, const FeatureHelper* filteredFeatures);

  bool prepDataAndCheckout(const char* featureName, 
                           LicenseData* lcData, 
                           Job* job, 
                           UtString* reason);
  
  bool doExistCheck(const char* featureName, 
                    UtString* reason);

  //! Common checkout code once a LicenseData object has been created
  bool checkout(const char* featureName, LicenseData* lcData, UtString* reason);


  //forbid
  UtLicense();
  CARBON_FORBID_DEFAULT_CTORS(UtLicense);
};
#endif
