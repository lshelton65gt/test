// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __HierName_h_
#define __HierName_h_


/*!
  \file
  The highest name abstraction possible. A pure interface for a
  hierarchical name.
*/

#include <stdio.h> // for FILE

#include "util/CarbonTypes.h"
#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

class StringAtom;
class UtString;
class STBranchNode;

//! HierName interface
/*!
  An interface to describe a hierarchical name.
*/
class HierName
{
public: CARBONMEM_OVERRIDES
  //! virtual destructor
  virtual ~HierName() {}
    
  //! Return this name string
  virtual const char* str() const = 0;
    
  //! Return the name object
  virtual StringAtom* strObject() const = 0;
    
  //! Return the Parent read-only
  /*!
    \returns NULL if there is no parent
  */
  virtual const HierName* getParentName() const = 0;
    
  //! Compose a name, optionally generate a full hierarchical name with option to include the top of hierarchy
  /*!
    \param buf The buffer used to compose the name.
    \param includeRoot if true then top of hierarchy is included in a hierarchical name
    \param hierName if true then a hierarchical name is generated
    \param separator is used between levels of hierarchical names
    \param escapeIfMultiLevel if true then resulting name will be escaped
           if a hierarchical name was generated
  */
  void compose(UtString* buf, bool includeRoot = true, bool hierName = true,
               const char *separator = ".", bool escapeIfMultiLevel = false ) const
  {
    // call the class specific helper method, we define a single compose method on 
    // the root class so that every reference to this method will see the default arguments.
    composeHelper( buf, includeRoot, hierName, separator, escapeIfMultiLevel );
  }

  //! a class specific method for composing a name, see compose method for definition of the args
  virtual void composeHelper(UtString* buf, bool includeRoot, bool hierName,
                             const char *separator, bool escapeIfMultiLevel) const = 0;

  //! Print this path to a FILE* (by default stdout). Used for debugging and assertions
  /*!
    While debugging, it becomes very difficult to know which
    name/str/scope you are looking at, so this function is needed
    to ease pain.
      
    \warning ONLY USE FOR DEBUGGING
  */
  virtual void print() const = 0;
    
  //! Alphabetic comparison of name hierarchies.  -1 is <; 0 is ==; 1 is >
  static int compare(const HierName* n1, const HierName* n2);

  //! less-than operator for ordering
  bool operator<(const HierName& other) const;

  //! hash helper function
  UInt32 hash() const;

  //! find the depth of the name (e.g. "a.b.c" depth is 3)
  SInt32 findDepth() const;

  //! hash comparison function
  inline bool operator==(const HierName& hn) const {
    return compare(this, &hn) == 0;
  }

  //! Print assertion information for this symbol table object
  void printAssertInfo(const char* file, int line, const char* exprStr) const;

protected:
  //! Print utility
  virtual void printInfo() const = 0;
};

//! output stream operator to print hierarchical name
UtString& operator<<(UtString& str, const HierName& name);

//! Functor for container comparisons.
struct HierNameCmp
{
  bool operator()(const HierName* hn1, const HierName* hn2) const
  {
    return HierName::compare(hn1,hn2) < 0;
  }
};

#endif
