// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef DISJOINTRANGE_H_
#define DISJOINTRANGE_H_

#include "util/ConstantRange.h"
#include "util/UtMap.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtList.h"
#include "util/Loop.h"
#include "util/LoopFunctor.h"

class ConstantRange;

//! DisjointRange class
/*!
  This class is best described by the problem it's trying to solve.
  
     reg [63:0] out;
     always @(*)
       case (sel)
       0: out = 64'b0;
       1: out = {32'bz, 32'b0};
       2: out = {32'b0, 32'bz};
       3: out = 64'bz;
       endcase

  We want to resynthesize this to use explicit continuous enabled drivers,
  and we need two of them -- one for the high 32 bits, one for the low
  32-bits.  This class is what figures out that the number of enables needed
  is 2, and how those map to ranges in the original net

     reg [63:0] out, out_drive;
     reg [1:0] out_ena;
     always @(*)
       case (sel)
       0: out_drive        = 64'b0;      out_ena = 2'b11;
       1: out_drive[31:0]  = 32'b0;      out_ena = 2'b01;
       2: out_drive[63:32] = 32'b0;      out_ena = 2'b10;
       3:                                out_ena = 2'b00;
       endcase
    assign  out[63:32] = out_ena[1] ? out_drive[63:32] : 32'bz;
    assign  out[31:0]  = out_ena[0] ? out_drive[31:0]  : 32'bz;


 This class facilitates this transformation by transforming the
 ConstantRange set
    ((63,0),(63,32),(31,0)) 
 into the two disjoint ranges
    ((63,32),(31,0)) 

 This mapping is created by calling insert(ConstantRange) for all the
 input ranges.

 The resultant set of disjoint ranges can be obtained via loopRanges().
 Each range is associated with an index, which in the tristate application,
 corresponds to a bit in the enable vector.

 The construct() method is used to determine the range of enable-bits touched
 by any of the original ConstantRanges.

 This class represents the computed disjoint ranges with a class called
 DisjointRange::Slot.  It represents the original ConstantRange structures
 that were directly inserted by the user as a DisjointRange::Request.

 We keep a map of the DisjointRange::Slot structures back to the
 DisjointRange::Request structures.  The if a Request overlaps a Slot, then
 it must wholly contain the Slot.  That is, ConstantRange::contains is true.
 Specifically, request->getRange()->contains(slot->getRange()).

 Slots map to any number of Requests, and each Request may may to any number
 of slots.
*/
class DisjointRange {
public:
  CARBONMEM_OVERRIDES

  class Slot;
  typedef UtHashSet<Slot*> SlotPtrSet;
  typedef Loop<SlotPtrSet> SlotLoop;

  //! sorted loop over all the slots (high to low)
  typedef SlotPtrSet::SortedLoop SortedSlotLoop;


  class Request;
  typedef UtHashSet<Request*> RequestPtrSet;
  typedef Loop<RequestPtrSet> RequestLoop;
  typedef UtHashMap<ConstantRange,Request*> RequestMap;

  class Request;
  typedef UtList<Request*> RequestPtrList;

  //! class representing an original ConstantRange inserted by the user
  /*!
   *! the numeric space covered by ConstantRanges is broken into disjoint
   *! slots.  Each original Request falls into one or more slots.
   */
  class Request {
  public:
    CARBONMEM_OVERRIDES

    //! ctor
    Request(const ConstantRange& range)
      : mRange(range) {}

    //! dtor -- removes itself from slots
    ~Request() {
      for (Loop<SlotPtrSet> p(mSlots); !p.atEnd(); ++p) {
        Slot* slot = *p;
        slot->eraseRequest(this);
      }
    }

    //! insert this range into a slot
    void insertSlot(Slot* slot) {
      mSlots.insert(slot);
    }

    //! remove this range from a slot
    void eraseSlot(Slot* slot) {
      mSlots.erase(slot);
    }

    //! consistency check
    void check();

    //! get the original ConstantRange
    const ConstantRange& getRange() const {return mRange;}

    //! Is this range in a slot?
    bool hasSlot(Slot* slot) const {return mSlots.find(slot) != mSlots.end();}

    //! loop over all slots
    SlotLoop loopSlots() {return SlotLoop(mSlots);}

    //! how many slots?
    UInt32 numSlots() const {return mSlots.size();}

    //! sorted loop over slots
    SortedSlotLoop loopSortedSlots() {
      return SortedSlotLoop(mSlots);
    }


  private:
    ConstantRange mRange;
    SlotPtrSet mSlots;
  };

  //! class to represent a disjoint range derived from a set of overlapping ranges
  class Slot {
  public:
    CARBONMEM_OVERRIDES

    //! construct a slot
    Slot(const ConstantRange& range, DisjointRange* disjoint_range) :
      mRange(range),
      mDisjointRange(disjoint_range)
    {}

    //! dtor
    ~Slot() {
      for (Loop<RequestPtrSet> p(mRequests); !p.atEnd(); ++p) {
        Request* request = *p;
        request->eraseSlot(this);
      }
    }

    //! insert a range if it overlaps the slot
    void maybeInsertRequest(Request* request);

    //! erase a request
    void eraseRequest(Request* request);

    //! does this slot have a range?
    bool hasRequest(Request* request) const {
      return mRequests.find(request) != mRequests.end();
    }

    //! copy any overlapping ranges from another slot
    void copyRequests(const Slot* old_slot);

    //! consistency check
    void check();

    //! get the ConstantRange covered by this slot
    const ConstantRange& getRange() const {return mRange;}

    //! associate this slot with a numeric index
    void putIndex(SInt32 index) const {mIndex = index;}

    //! get the index
    SInt32 getIndex() const {return mIndex;}

    //! loop over all requests covered by this slot
    RequestLoop loopRequests() {return RequestLoop(mRequests);}

    //! sorting comparison with another slot.
    /*!
     *! We want high numbers first for usage in PortSplitting, where we
     *! want to replace out_drive[63:0] with
     *! {out_drive[63:32],out_drive[31:0]}, and not the reverse.
     */
    bool operator<(const Slot& other) const {
      return mRange.getLsb() > other.mRange.getLsb();
    }

  private:
    RequestPtrSet mRequests;
    ConstantRange mRange;
    DisjointRange* mDisjointRange;
    mutable SInt32 mIndex;
  };

  //! LSB-Ordered map of disjoint slots
  typedef UtMap<SInt32,Slot*> SlotMap;

  //! ctor
  DisjointRange();

  //! dtor
  ~DisjointRange();

  //! Loop that yields all the original Requests that overlap the specified range
  /*!
   *! For simplicity of implementation, this loop collects all the overlapping
   *! ranges in the constructor and the iteration just loops over a stored
   *! list.  This could also be written to dynamically traverse the
   *! DisjointRange maps as the iteration progresses.  That would add some
   *! code complexity but reduce memory overhead while the iterator is active.
   */
  class OverlapLoop {
  public:
    //! ctor
    OverlapLoop(DisjointRange* dr, const ConstantRange& overlap_range);

    //! next
    void operator++() {++mRequestIter;}

    //! get the current originally requested range
    const ConstantRange& operator*() const {return (*mRequestIter)->getRange();}

    //! have we reached the last one?
    bool atEnd() {return mRequestIter == mPending.end();}

  private:
    OverlapLoop();
    OverlapLoop(const OverlapLoop&);
    OverlapLoop& operator=(const OverlapLoop&);

    RequestPtrList::iterator mRequestIter;
    RequestPtrList mPending;
  };

  //! Insert a new, potentially overlapping ranges, maintaining the disjoint sets
  //! incrementally.  The range is assumed to have msb>=lsb
  void insert(const ConstantRange& range) {
    insert(range.getLsb(), range.getMsb());
  }

  //! insert method based on min/max.
  void insert(SInt32 rmin, SInt32 rmax);

  //! erase a range
  void erase(const ConstantRange& range);

  //! has this precise specified range been requested?
  bool hasRequest(const ConstantRange& crange) const;

  //! get the number of disjoint ranges
  UInt32 numSlots() const {return mSlotMap.size();}

  //! loop over the LSB->Slot map
  typedef LoopMap<SlotMap> SlotMapLoop;

  //! loop over all the disjoint ranges
  /*!
   *! Note that this iteration interface exposes a bit too much of the
   *! implementation of DisjointRange and may impact our ability to
   *! change the implementation, depending on how many callers there
   *! are.
   */
  SlotMapLoop loopSlots() {return SlotMapLoop(mSlotMap);}

  //! constant loop over LSB->Slot map
  typedef CLoopMap<SlotMap> SlotCLoop;

  //! loop over all the disjoint ranges (const version)
  SlotCLoop loopSlots() const {return SlotCLoop(mSlotMap);}

  //! construct an overlapping range from a disjoint range.
  /*! false is returned if provided range is not one of the original
   *! ones inserted.
   *!
   *! In the enable-bits example above, You would get this mapping:
   *!     range==63:0  --> *indexRange==1:0
   *!     range==63:32 --> *indexRange==1:1
   *!     range==31:0  --> *indexRange==0:0
   */
  bool construct(const ConstantRange& range, ConstantRange* indexRange) const;

  friend class OverlapLoop;
  friend class Slot;

  void print() const;
  void check();

  //! Find a request given the ConstantRange that was used to create it
  Request* findRequest(const ConstantRange&) const;

private:
  //! remove this slot from the DisjointRange structure
  void removeSlot(Slot* slot);

  void insertHelper(SInt32 rmin, SInt32 rmax, Request* request, Slot* orig_slot);
  void findSlotIndices() const;

  RequestMap mRequestMap;
  SlotMap mSlotMap;
  mutable bool mSlotsChanged;
}; // class DisjointRange

#endif
