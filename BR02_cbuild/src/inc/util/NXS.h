// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _NXS_H_
#define _NXS_H_

#ifdef __GCCXML__

#define NXSBUILD(_string_) __attribute((gccxml("build(" #_string_ ")")))
#define NXSDISTILL __attribute((gccxml("distill")))
#define NXSNESTED __attribute((gccxml("nested")))
#define NXSBASETYPE __attribute((gccxml("basetype")))
#define NXSOMITTED __attribute((gccxml("omitted")))
#define NXSMASK __attribute((gccxml("mask")))
#define NXSCHOICE __attribute((gccxml("choice")))
#define NXSSELECT __attribute((gccxml("select")))

#else

#define NXSBUILD(_string_)
#define NXSDISTILL
#define NXSNESTED
#define NXSBASETYPE
#define NXSOMITTED
#define NXSMASK
#define NXSCHOICE
#define NXSSELECT

#endif

#endif
