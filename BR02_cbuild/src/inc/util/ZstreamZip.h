// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __ZstreamZip_h_
#define __ZstreamZip_h_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#ifndef __Zstream_h_
#include "util/Zstream.h"
#endif

#ifndef __UtHashSet_h_
#include "util/UtHashSet.h"
#endif

/*!
  \file
  This is a class replacement for the zip/unzip program. The classes
  defined here implement a file and buffer database that will allow
  for rapid zipping and unzipping of particular files and buffers.
*/

// forward decls
class ZistreamEntry;

//! Base class for ZostreamZip and ZistreamZip
class ZstreamZip
{
  friend class ZistreamEntry;

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  ZstreamZip();
  //! destructor
  virtual ~ZstreamZip();

  //! void* operator for conditional checks
  /*!
    This allows a user to do 
    \verbatim
    f.open(....)
    if (f)
    ...
    \endverbatim
  */
  operator void*() const;

  //! Logical Not operator for conditional checks
  /*!
    This allows a user to do
    \verbatim
    f.open(...)
    if (!f)
    ...
    \endverbatim
  */
  int operator!() const;

  //! Returns true if a failure occurred during writing
  bool fail() const;

  //! Get file error
  /*!
    Use this when calling methods that do not take error messages, or
    if the error message was missed/ignored. The error message could
    be empty, so you should check for failure first.
  */
  const char* getFileError();

  //! check for unique entry
  bool checkUnique(const char* id);

  //! Enumeration for zstream type
  enum FileType {
    eFileDB, //!< Zstream Database 
    eFileZstream, //!< Zstream non-database
    eFileTxt //!< Textual or freeform
  };

  //! Get the filename of the zipstream archive
  void getFilename(UtString* filename) const;

protected:
  //! Base stream
  Zstream* mStream;

  //! The zip entry
  struct ZipEntry
  {
    CARBONMEM_OVERRIDES

    //! constructor
    ZipEntry(SInt64 filePos, const char* id, FileType fileType) : 
      mFilePos(filePos), mType(fileType), mNext(NULL), mName(id)
    {}
    
    //! Get the entry's file position
    SInt64 getFilePos() const { return mFilePos; }

    //! Set the next entry
    void putNext(ZipEntry* entry)
    {
      mNext = entry;
    }

    //! Return the entry name
    const UtString& getName() const {
      return mName;
    }

    //! Get the next entry
    /*!
      \returns the unmodifiable next entry in the list. NULL if this
      is the last entry
    */
    const ZipEntry* getNext() const {
      return mNext;
    }

    //! Get the file type
    FileType getType() const {
      return mType;
    }

    //! File position
    SInt64 mFilePos;

    //! File type (db or not)
    FileType mType;

    //! Next Entry
    ZipEntry* mNext;

    //! Entry name
    UtString mName;
  };

  //! The first entry in the file list
  ZipEntry* mFirstEntry;
  //! The last entry in the file list
  ZipEntry* mLastEntry;

  //! The zip archive-specific error message
  UtString mErrMsg;
  
  //! Typedef for String -> ZipEntry
  typedef UtHashMap<UtString, ZipEntry*> StringEntryMap;

  //! Entry Map
  StringEntryMap mEntryIds;

  //! Sets the Zstream in use
  inline void putZstream(Zstream* zstrm)
  {
    mStream = zstrm;
  }

  //! Puts entry into map. Asserts if not unique
  void addUnique(const char* id, ZipEntry* entry);

  //! Checks the active entry for error.
  virtual void checkActiveEntryError();

  // forbid
  ZstreamZip(const ZstreamZip&);
  ZstreamZip& operator=(const ZstreamZip&);
};

//! Class to write out a carbon zip archive
/*!
  This class can take filenames and entire buffers (with ids) and zip
  them into a compressed/encrypted archive.
*/
class ZostreamZip : public ZstreamZip
{
public: 
  CARBONMEM_OVERRIDES

  //! constructor with open
  /*!
    This opens the file. Check fail() to see if the open worked.
    getFileError() will return any error that has occurred.
  */
  ZostreamZip(const char* outputFile, void* key);
  
  //! destructor
  /*!
    The destructor will finalize and close the file if it is not
    already closed. However, file errors are not caught. It is highly
    recommended that the user call finalize before destruction.
  */
  ~ZostreamZip();

#define ZOSTREAMZIP_INIT_FUNC arGauss
  inline void ZOSTREAMZIP_INIT_FUNC()
  {
    // fix up mOutFile
    ZOSTREAM_PTR_INIT(mOutFile);
    putZstream(mOutFile);
    open();
  }

  //! Add a database entry and return the descriptor
  /*!
    The underlying form of the archive is a ZostreamDB. So, if you
    wish to add data to the archive as a ZostreamDB and you don't want
    to write out a separate file before adding it to the archive then
    this is the way to go.

    When you are finished adding the data with the ZostreamDB provided
    call endDatabaseEntry(). This will update the archive and prepare it
    for any more additions.

    \warning DO NOT CLOSE THE FILE when you are finished writing the
    data. This will invalidate the archive. Furthermore, when you are
    finished using the descriptor, do not use it again. There is no
    way currently for the archive to know if you have used it after
    calling endDatabaseEntry(), so you could very easily corrupt
    the archive in horribly mysterious ways.

    \warning This will assert if this has already been called without
    calling endDatabaseEntry().

    \sa endDatabaseEntry()

    \param id A unique buffer id for the buffer in the archive. This
    is the name that the entry will get.
    \returns Essentially the archive file descriptor that has been
    prepared for data entry for the id. Returns NULL if something has
    gone wrong.
  */
  ZostreamDB* addDatabaseEntry(const char* id);

  //! Add an entry from a different archive
  /*!
    Returns true if the write was successful.
  */
  bool addEntry(ZistreamEntry* readEntry, FileType filetype);

  //! Add a file to the archive
  /*!
    \param file File to add to the archive
    \param fileType Type of file that is being added. Note that this
    defaults to eFileTxt - a non-zstream file. If the file is a
    zstream file you must specify why kind of zstream file it is,
    eFileZip or eFileDB.
    \returns True if the file was successfully added, false
    otherwise. Asserts if the file was already added or it matches
    another buffer with the same id

    \warning Will assert if this is called between addDatabaseEntry()
    and endDatabaseEntry()
  */
  bool addFile(const char* file, FileType fileType = eFileTxt);

  //! Add a buffer to the archive
  /*!
    This adds a buffer entry to the zip archive using the eFileTxt
    type.

    \param id A unique buffer id for the buffer in the archive. This
    is the name that the entry will get.
    \param buffer The buffer to add to the archive
    \returns True if the buffer was successfully added, false
    otherwise. Asserts if the id is not unique

    \warning Will assert if this is called between addDatabaseEntry()
    and endDatabaseEntry()    
  */
  bool addBuffer(const char* id, const UtString& buffer);
  
  //! Finalize the archive and close the file.
  /*!
    This gets called upon destruction, but the caller should do this
    explicitly to catch any file errors. Once this function is
    called no other calls can be made to the instance of this class.
    
    \returns true if the file was successfully finalized, else false.
  */
  bool finalize();

  //! Close out a database file entry
  /*!
    This should \e only be called if addDatabaseEntry() has been
    called and you are ready to 'close' the database entry.
    
    \warning Asserts if addDatabaseEntry() hasn't been called.

    \returns true if there were no file problems. 
  */
  bool endDatabaseEntry();
  
private:
  ZostreamDB* mOutFile;

  SInt64 mBeginPos;

  // used for addDatabaseEntry
  class DBEntry;
  DBEntry* mCurDBEntry;

  void addEntryDescriptor(const char* name);
  void createZipEntry(const char* id, FileType fileType);
  void beginFileEntry();
  void endFileEntry();

  //! Open the output file archive
  /*!
    \returns true on success, false otherwise
  */
  bool open();


  // forbid
  ZostreamZip(const ZostreamZip&);
  ZostreamZip& operator=(const ZostreamZip&);  
};

class ZistreamZip : public ZstreamZip
{
public: 
  CARBONMEM_OVERRIDES

  //! Enumeration for rewrite actions on entries
  enum EntryAction {
    eEntryUnmodified, //!< No modifications made
    eEntryModified, //!< Entry is modified
    eEntryDelete //!< Delete the entry
  };

  //! Interface for an archive rewriter
  /*!
    The user needs to implement this interface in order to call
    rewrite().
  */
  class EntryRewriter
  {
  public:
    CARBONMEM_OVERRIDES

    //! virtual destructor
    virtual ~EntryRewriter();

    //! Return what we are going to do with the given entry name
    /*!
      \warning Returning eEntryModified is only supported for
      entries that were added with a ZostreamDB (ie.,
      addDatabaseEntry());
    */
    virtual EntryAction whichAction(const UtString& entryName) const = 0;

    //! Modify a db entry
    /*!
      This gets called as a result of returning eEntryModified in
      whichAction() for the given entryName. The user just needs to
      write the object to the database and return the status of the
      stream. Do not call endDatabaseEntry().

      This function is non-const in case the user wants to update his
      EntryRewriter object with regards to the entry (e.g., update a
      map that this particular entry has been dealt with).
      
      for example:
      \code
      if (strcmp(entryName, "myobj")==0) {
        mMyObj->dbWrite(*outDB);
        return ! outDB.fail();
      }
      \endcode
    */
    virtual bool modifyDBEntry(ZistreamEntry* readEntry, 
                               const UtString& entryName, 
                               ZostreamDB* outDB) = 0;

    //! Message that the orig file is being backed up
    virtual void msgBackingupFile(const char* origFilename, 
                                  const char* backupFilename) const = 0;

    //! Message opening a new temporary file for writing.
    virtual void msgWritingTmpFile(const char* tmpFile) const = 0;

    //! Message moving tmp file to orig file
    virtual void msgMovingTmpToOrig(const char* tmpFile, 
                                    const char* origFile) const = 0;
    
  };

  //! constructor with open
  /*!
    This opens the file. Check fail() to see if the open worked.
    getFileError() will return any error that has occurred.
  */
  ZistreamZip(const char* inFile, void* key);

  //! constructor from string-buffer
  /*!
    This gets the text of the file from a buffer, rather than opening
    up a file
  */
  ZistreamZip(size_t buflen, const char* buf, void* key);

  //! Destructor
  /*!
    This closes the file.
  */
  ~ZistreamZip();
  
  //! Get a Zistream pointer to the given entry name
  /*!
    This also does a switchEntry on the returned entry. The active
    entry is not touched if there was an error finding the entry.
    
    \warning Do NOT delete the returned entry. The carbon zip archive
    will do that for you. If you wish to conserve memory, call
    finishEntry().
    \param entryName The name of the file or buffer to which you are
    trying to get a handle.
    \returns A Zistream to the entry. NULL if the entry does not exist
    or if there was an error. If NULL is return getFileError() will
    have the reason.
  */
  ZistreamEntry* getEntry(const char* entryName);
  
  //! Delete the ZistreamEntry pointer
  /*!
    This gets done automatically upon destruction of ZistreamZip, but
    if you need to conserve memory usage, you may want to close some
    of them yourself. This is also useful if you need to reopen the
    entry in the archive.
    The entry pointer gets NULLed on this call. If a null is passed in
    it is ignored.
  */
  void finishEntry(ZistreamEntry** entry);

  //! Switch ZistreamEntry activity to the one given
  /*!
    In the presence of multiple entries being open at the same time,
    only 1 can have access to the file descriptor. So, this function
    allows you to switch between different entries.
  */
  void switchEntry(ZistreamEntry* entry);

  //! Get all the entry names in the archive
  /*!
    This will get all the entries in the archive in the order they
    were added.

    \returns true if successful, false otherwise
    \param ids Array to append ids to
  */
  bool getAllEntryIds(UtStringArray* ids);

  //! Rewrite the database, allowing modifications
  /*!
    Rewrite the archive calling the user at each entry point to allow
    for modifications. The user is responsible for writing the
    modifications and returning the appropriate EntryAction.
    
    The basic algorithm here is to create a file of the same name plus
    the pid(). Then move the old file to a backup file, name.bkup, and
    finally move the new file (with the pid extension) to the
    original filename.
    
    Note that modifications can only be done to database type entries
    (ie., ZostreamDB-based). There is no technical reason why this is
    the case except that more work would have to be done in order to
    support it. Basically, marking an entry as modified, will create a
    database entry in the new file by using addDataBaseEntry();
    
    Once this is called, this ZistreamZip object can no longer be
    used. You'll have to destroy this object and create a new one in
    order to read the rewritten file. In order to enforce this, the
    underlying zistream will be put into an error state.
    
    \returns false if there was a problem rewriting the file. True
    otherwise.
  */
  bool rewrite(EntryRewriter* userRewriter);

#define ZISTREAMZIP_INIT_FUNC arDist
  inline void ZISTREAMZIP_INIT_FUNC()
  {
    // Fix up mInFile
    ZISTREAM_PTR_INIT(mInFile);
    putZstream(mInFile);
    open();
  }

protected:
  virtual void checkActiveEntryError();

private:
  ZistreamEntry* mActiveEntry;
  SInt64 mHeaderPos;
  ZistreamDB* mInFile;
  typedef UtHashSet<ZistreamEntry*> ZistreamSet;
  ZistreamSet mZistreams;
  typedef UtHashMap<UtString, ZistreamEntry*> NameZEntryMap;
  // This is only used for consistency in getEntry.
  NameZEntryMap mEntryZistreams;

  UInt32 mBufferedFileSize;

  bool readEntry(UtString* name, FileType* fileType);

  bool checkEntries();

  void deactivateCurEntry();

  //! Open the zip file archive
  /*!
    \returns true on success, false otherwise
  */
  bool open();
  
  // forbid
  ZistreamZip();
  ZistreamZip(const ZistreamZip&);
  ZistreamZip& operator=(const ZistreamZip&);

  class ZistreamEntryN;
  class ZistreamDBEntryN;
};


//! Class to allow access to Zistream methods from a zip entry
/*!
  When reading a carbon zip archive, the caller may want to get an
  entry and then read it like it was its own file. This class allows
  for that, provided the caller has called the
  ZistreamZip::switchStream() method prior to using the class. If
  multiple entries are open then the switchEntry method needs to be
  called with each non-consecutive usage.
*/
class ZistreamEntry
{
  friend class ZistreamZip;

public: CARBONMEM_OVERRIDES
  //! constructor
  /*!
    \param beginOffset The absolute offset into the file where this
    entry begins
  */
  ZistreamEntry(SInt64 beginOffset);
  
  //! virtual destructor
  virtual ~ZistreamEntry();
  
  //! True if this ZistreamEntry has control of the file descriptor
  bool isActive() const;

  //! Cast this to a Zistream, will always be non-null
  virtual Zistream* castZistream() = 0;

  //! Cast this to a ZistreamDB. This could be NULL
  /*!
    This could return null if the entry is not a database entry.
  */
  virtual ZistreamDB* castZistreamDB() = 0;

protected:
  //! Set whether or not this entry has control of the file descriptor
  void putIsActive(bool isActive);

  //! Mark the current file descriptor position for this entry
  void markPosition();
  
  //! Get the current expected file descriptor position.
  SInt64 getCurrentPosition() const;

private:
  SInt64 mCurPosition;
  bool mIsActive;
  
  
  // forbid
  ZistreamEntry();
  ZistreamEntry(const ZistreamEntry&);
  ZistreamEntry& operator=(const ZistreamEntry&);
};

#define ZISTREAMZIP_INIT(var) \
   var.ZISTREAMZIP_INIT_FUNC()

#define ZISTREAMZIP_PTR_INIT(var) \
   var->ZISTREAMZIP_INIT_FUNC()

//! ZistreamZip var (filename)
#define ZISTREAMZIP(var, filename) \
   ZistreamZip var (filename, ZSTREAM_KEY); \
   ZISTREAMZIP_INIT(var)

//! ZistreamZip var (buf, len)
#define ZISTREAMZIP_STRING(var, buf, len) \
   ZistreamZip var (len, buf, ZSTREAM_KEY); \
   ZISTREAMZIP_INIT(var)

//! var  = new ZistreamZip(name)
#define ZISTREAMZIP_ALLOC(var, name) \
   var  = new ZistreamZip(name, ZSTREAM_KEY); \
   ZISTREAMZIP_PTR_INIT(var)

#define ZOSTREAMZIP_INIT(var) \
   var.ZOSTREAMZIP_INIT_FUNC()

#define ZOSTREAMZIP_PTR_INIT(var) \
   var->ZOSTREAMZIP_INIT_FUNC()

//! ZostreamZip var(name)
#define ZOSTREAMZIP(var, name) \
   ZostreamZip var (name, ZSTREAM_KEY); \
   ZOSTREAMZIP_INIT(var)

//! var  = new ZostreamZip(name)
#define ZOSTREAMZIP_ALLOC(var, name) \
   var  = new ZostreamZip(name, ZSTREAM_KEY); \
   ZOSTREAMZIP_PTR_INIT(var)

#endif
