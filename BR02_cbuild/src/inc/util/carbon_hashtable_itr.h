/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBON_HASHTABLE_ITR__
#define __CARBON_HASHTABLE_ITR__
#include "util/carbon_hashtable.h"

#ifdef __cplusplus
extern "C" {
#endif

  /*!
    \brief hashtable iterator structure.
  */
  struct carbon_hashtable_itr
  {
    struct carbon_hashtable *h;
    carbon_hashEntry *e;
    carbon_hashEntry *prev;
    UInt32 index;
    UInt8 mNoAdvance;
  };


  /*!
    \brief Allocate an iterator on the heap. This fully initializes the
    iterator and it will be pointing to the first element in the
    hashtable.
  */
  struct carbon_hashtable_itr *
  carbon_hashtable_iterator(struct carbon_hashtable *h);

  /*!
    If an iterator is created on the stack, it is uninitialized. This
    just initializes it. It is not a valid iterator, though. It does not
    point to anything in the hashtable.
  */
  void carbon_hashtable_iterator_nullinit(struct carbon_hashtable_itr* i);

  /*!
    fast version of carbon_hashtable_iterator_nullinit
  */
# define CARBON_HASHTABLE_ITERATOR_NULLINIT(iter) \
     memset(iter, 0, sizeof(struct carbon_hashtable_itr))

  /*!
    If an iterator is created on the stack, it is uninitialized. This
    initializes it, so that it is pointing to the first element in the
    hashtable.
  */
  void carbon_hashtable_iterator_init(struct carbon_hashtable_itr* itr, 
                                      struct carbon_hashtable* h);

  /*!
    This removes the current entry from the hashtable pointed to by the
    iterator, leaving the iterator in a state where it may not be
    pointing to a valid entry, but advancing it will go to the next
    entry
  */
  void carbon_hashtable_iterator_remove_current(struct carbon_hashtable_itr *i,
                                                unsigned int entrySize);
  /*!
    Advances to the next element in the hashtable. Unless the iterator's
    mNoAdvance member is nonzero, in which case this sets that to zero
    and returns.
  */
  int carbon_hashtable_iterator_advance(struct carbon_hashtable_itr *itr);

  /*!
    Reduced complexity 'advance' to allow UtHashMap::UnsortedCLoop to run
    slightly faster.  This version cannot handle deletions.
  */
  void
  carbon_hashtable_iterator_simple_advance(struct carbon_hashtable_itr *itr);

  /*!
    \brief Destroys (frees) the iterator.
  */
  void carbon_hashtable_iterator_destroy(struct carbon_hashtable_itr *i);

#ifdef __cplusplus
}
#endif

#endif /* __CARBON_HASHTABLE_ITR_CWC22__*/

/*
 * Copyright (C) 2002 Christopher Clark <firstname.lastname@cl.cam.ac.uk>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * */
