/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _CARBON_ABORT_H_
#define _CARBON_ABORT_H_

#include "util/c_memmanager.h"

//! Class to allow applications to override default assert behavior
/*!
 *! The application program can derive a class from this one.  Constructing
 *! the class will establish the override, and destructing it will restore
 *! the previous behavior.
 */
class CarbonAbortOverride {
public:
  CARBONMEM_OVERRIDES

  CarbonAbortOverride();
  virtual ~CarbonAbortOverride() { sOverride = mSaveOverride; }


  //! User must override this to change the way printing occurs
  virtual void printHandler(const char*) {}

  //! User may override this to change the way the application exits
  virtual void abortHandler() {
#ifndef __COVERITY__     // coverity views abort as too natural a death
    abort();
#endif
  }

  void install();

  static void print(const char*);
  static void abort();

private:
  CarbonAbortOverride* mSaveOverride;
  static CarbonAbortOverride* sOverride;
};

#endif
