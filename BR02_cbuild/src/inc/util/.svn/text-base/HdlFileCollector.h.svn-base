// -*- C++ -*-
#ifndef __HdlFileCollector_h_
#define __HdlFileCollector_h_
/******************************************************************************

 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*! \file 
 * Verilog file and VHDL File->library association class
 */
#include "util/FileCollector.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtArray.h"
#include "util/UtList.h"

bool sIsOption(const char* arg);

class LangVer {
public:
  enum internal_type {
    LangVer_MIN = 0,
    VHDL87=0,
    VHDL93,

    Verilog,                    // this is Verilog95
    Verilog2001,
    SystemVerilog2005,
    SystemVerilog2009,
    SystemVerilog2012,
    LangVerVerilog_MIN = Verilog,
    LangVerVerilog_MAX = SystemVerilog2012,
    LangVer_MAX
  };
private:
  internal_type m_value;
public:
  const bool hasVerilogFiles() const;
  const bool hasVHDLFiles() const;
  inline operator internal_type() const { return m_value; }
  inline bool operator==(internal_type value) const { return m_value == value; }
  inline bool operator!=(internal_type value) const { return m_value != value; }
  inline void operator++(int) { m_value = static_cast<internal_type>(m_value+1); }
  inline LangVer eEnumMax(LangVer) { return LangVer_MAX; }
  inline LangVer eEnumMin(LangVer) { return LangVer_MIN; }
  inline LangVer() { }
  inline LangVer(internal_type value) : m_value(value) { }
  inline LangVer(UInt32 value) : m_value(static_cast<internal_type>(value)) { }
};


/*! \class HdlLib
 *
 *  Private class to store the library<->files mapping
 *
 *  There will be one of these for each HDL library in the design.
 *  The logical and physical library information are stored here.
 */
class HdlLib
{
public: CARBONMEM_OVERRIDES

  typedef UtStringArray                 hdlLibFiles;
  typedef hdlLibFiles::iterator         iterator;
  typedef iterator                      hdlFileIterator;
  typedef hdlLibFiles::const_iterator   const_iterator;
  typedef const_iterator                const_hdlFileIterator;
  

   HdlLib( const UtString& library, const UtString& path );

   ~HdlLib();

   void addFile( const UtString& hdlFile, LangVer ver );

   //! Return a string containing the logical HDL library name
   const UtString& getLogicalLibrary() const;
   
   //! Return a string containing the physical HDL library path
   const UtString& getLibPath() const;

   //! Set the logical HDL library name
   void setLogicalLibrary( const UtString& library );
   
   //! Set the physical HDL library path
   void setLibPath( const UtString& path );

   void setFileRoot( const char* fileRoot );

   //! Provide iteration over mFilesAndArgs[ver]
   const_iterator begin( LangVer ver ) const;
   
   //! Provide iteration over mFilesAndArgs[ver]
   const_iterator end( LangVer ver ) const;

   //! Find the file
   iterator find( const char* file, LangVer ver );

   //! Get the number of files for this LangVer
   UInt32 getNumberOfFiles( LangVer ver );

   //! Remove the file pointed to by the iterator and put it at the end of the list
   const_iterator placeFileAtBottom(LangVer, const char* file );

   // Following member is depracated. It is introduced to be compatible with old cbuild language parsers behavior.
   LangVer libraryLanguage(void) { return mLibLang; }
   void setLibraryLanguage(LangVer langVer) { mLibLang = langVer;}

private:
   HdlLib();

   UtString mLogicalLibrary;
 
   UtString mPhysicalLibPath;
 
   hdlLibFiles mFilesAndArgs[LangVer::LangVer_MAX];

   // Following member is depracated. It is introduced to be compatible with old cbuild language parsers behavior.
   LangVer mLibLang;
};


/*!
 * \class HdlFileCollector
 * \brief Instantiated callback for Cbuild's ArgProc
 *
 * This class collects HDL libraries and HDL files.
 *  It associates the HDL files with their respective library.
 */
class HdlFileCollector : public FileCollector
{
public: CARBONMEM_OVERRIDES
  //! Iterator over the HDL libraries
  typedef UtArray<HdlLib*>           hdlLibList;
  typedef hdlLibList::iterator       iterator;
  typedef iterator                   hdlLibListIterator;
  typedef hdlLibList::const_iterator const_iterator;
  typedef const_iterator             const_hdlLibListIterator;

  //! constructor
  HdlFileCollector();

  //! virtual destructor
  virtual ~HdlFileCollector();

//! Provide a callback from arg parsing
/*! This method currently does the parsing of the -lib, (legacy) -vhdlLib,
 * (legacy) -vlogLig and all hdl files.
 *  
 *  \returns true if the callback has consumed the arg and param
 */
  virtual bool scanArgument( const char* arg, const char* param );

  // FIXME remove this call
  //! Add a Verilog argument to the internal Verilog arg list
  virtual void addArgument( const char *arg );

  // Scan the collected file lists and return true if any version of a verilog or SystemVerilog file has been seen
  const virtual bool hasVerilogFiles() const ;

  // Scan the collected file lists and return true if any version of a VHDL file has been seen
  const virtual bool hasVHDLFiles() const ;

  //! Set the name of the default work library
  // This this both VHDL and verilog library which comes as last on the command line.
  void setDefaultLib( const char* defaultLib, const char* defaultPath, LangVer langVer );

  //! The name of the last library specified either in the command line or in the
  //  library map file. VHDL or Verilog
  const char* getDefaultLibName( LangVer langVer ) const;

  //! The path of the last library specified either in the command line or in
  // the library map file
  const char* getDefaultLibPath( LangVer langVer ) const;

  //! Set the default work library paths to file root.
   // Set the filesystem ROOT for the libraries.
  void setFileRoot( const char* fileRoot );

  void preParseCmdlineSetFileRoot( const char* fileRoot ) { mFileRoot = fileRoot; }

  //! Provide iteration over the list of HDL libraries
  const_hdlLibListIterator begin() const;
   
  //! Provide iteration over the list of HDL libraries
  const_hdlLibListIterator end() const;

  //! Add a new library to the list of libraries
  void addLibrary( const UtString &library, const UtString &path, const LangVer );

  void addFile( const UtString &vhdlFile, LangVer ver );

  bool isHdlFile(const char* arg, LangVer& LangVer) const;

   //! Get the number of files for this LangVer
   UInt32 getNumberOfLibs() const;

  bool hasHdlFiles(LangVer) const;

  bool hasHdlOptions(LangVer) const;

  //! Prints the ordered list of files
  void printOrderedFileList() const;

  //! Get the library mapping file
  bool parseLibMapFile(const char* libMapFile);

  //! Save the library mapping file
  bool saveLibMapFile(const char* libMapFile);

  void addHdlExt(LangVer, const char*);

  bool librarySpecifiedOnCmdLine(void) const { return mLibSpecifiedOnCmdLine; }

  bool hasLangLib(LangVer ver) const { return mHasLangLib[ver]; }

  void setVHDLVersion(LangVer ver) {mVhdlVer = ver; }
  void setVerilogVersion(LangVer ver) {mVerilogVersion = ver;}

private:

  //! Parse library name and path from specified argument.
  void scanHdlLib(const char* param, UtString& libName, UtString& libPath);

  bool mHasLangLib[LangVer::LangVer_MAX];

  //! The current VHDL language version for files
  LangVer mVhdlVer;

  //! The current Verilog/SystemVerilog language version
  LangVer mVerilogVersion;

   //! Vector type to store extensions
  typedef UtStringArray ExtensionVector;
   
   //! Vector of file extensions (see note in HdlFileCollector::HdlFileCollector about the use of this vector)
  ExtensionVector mHdlExtensionVector[LangVer::LangVer_MAX];

  //! The user specified file root
  UtString mFileRoot;

  //! The last library specified
  UtString mDefaultLibName[LangVer::LangVer_MAX];
  UtString mDefaultLibPath[LangVer::LangVer_MAX];

  hdlLibList mLibs;

  //! The current library
  HdlLib* mCurrentLib;

  static const char* scHdlLibOption;

  bool mLibSpecifiedOnCmdLine;

public:
  static const char* scLegacyHdlLibOption[2];

};

#endif

// END FILE

