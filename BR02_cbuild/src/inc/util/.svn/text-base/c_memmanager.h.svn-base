/* -*- C++ -*- */
/*****************************************************************************

 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _C_MEMMANAGER_H_
#define _C_MEMMANAGER_H_

#ifndef __CarbonPlatform_h_
#ifdef CARBON_SPEEDCC
#include "util/CarbonPlatform.h"
#else
#include "carbon/CarbonPlatform.h"
#endif
#endif

#include <sys/types.h>

#if pfWINDOWS
#include <stddef.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

  /*!
    \brief Allocate memory, to be freed with carbonmem_free.
    Use this method and not carbonmem_alloc when you cannot
    keep track of the memory size to pass to carbonmem_dealloc.
  */
  void* carbonmem_malloc(size_t size);

  /*!
    \brief Free memory allocated with carbonmem_malloc
  */
  void carbonmem_free(void* ptr);

  /*!
    \brief Allocate memory, to be freed with carbonmem_dealloc
    Use this method and not carbonmem_malloc when you can keep
    track of the size to pass to carbonmem_dealloc.
  */
  void* carbonmem_alloc(size_t size);

  /*!
    \brief Extern C version of memsystem deallocation
  */
  void carbonmem_dealloc(void* ptr, size_t sz);

  /*!
    \brief Extern C version of memsystem reallocation

    Note that this is not identical to standard realloc(), because
    the old_size must be provided.  Memory allocated with carbon_reallocate
    can be freed with carbonmem_dealloc, not carbonmem_free.  The memory
    can originate from carbonmem_alloc, not carbonmem_malloc.
  */
  void* carbonmem_reallocate(void* p, size_t old_size, size_t new_size);


  /*!
    \brief Extern C version of memsystem reallocation

    This corresponds to standard realloc().  The old_size is not required.
    Memory allocated with carbon_realloc can be freed with carbonmem_free,
    not carbonmem_dealloc.  The memory can originate from carbonmem_malloc,
    not carbonmem_alloc.
  */
  void* carbonmem_realloc(void* p, size_t new_size);

#ifdef __cplusplus
}

/*! Macros to manage space for a vector of objects.  Note that the
 *! object's constructor will not be called, so this should not be used
 *! for arrays of objects with constructors.  You want to use these rather
 *! than "arr = new int[numElts]" and "delete [] arr" because these will
 *! call the Carbon memory manager directly to get space, rather than
 *! referencing global operator new[] and delete[].  This avoids link
 *! dependencies and reliance on our customers to use Carbon's global
 *! new/delete override to get decent performance.
 *! Example usage.  Instead of:
 *!  buf = new int[numInts];
 *!  delete [] buff;
 *! use:
 *!  buf = CARBON_ALLOC_VEC(int, numInts);
 *!  CARBON_FREE_VEC(buf, int, numInts);
 *! Note that StringUtil::alloc() and free() can be used for strings.
 */
#define CARBON_ALLOC_VEC(type, numElts) \
  ((type*)carbonmem_alloc((numElts) * (sizeof(type))))
#define CARBON_FREE_VEC(ptr, type, numElts) \
  carbonmem_dealloc((void*) (ptr), (numElts) * (sizeof(type)))

/*! Macros to use when heap-managing a C++ object using Carbon's memory
 *! manager.  These should not be needed, as long as every class or
 *! struct we define uses the "CARBONMEM_OVERRIDES" macro.  But if
 *! a manager class, such as those declared in UtHashMap.h and HashValue.h
 *! needs to 'new' an object that in some cases might be a POD (e.g
 *! a UInt64) and other cases may be a C++ object, such as UtString,
 *! then it needs to make sure that the Carbon memory manager is
 *! called for both of those by using placed new.  This macro may
 *! also be used for allocating memory for a class or struct that
 *! is defined external to Carbon, such as std::pair.
 */
#define CARBON_NEW(type) new (carbonmem_alloc(sizeof(type))) type
#define CARBON_DELETE(obj, type) do { \
  (obj)->~type(); \
  carbonmem_dealloc((void*) (obj), sizeof(type)); \
} while (0)


/*
 * To allow the Carbon runtime to work efficiently in a customer
 * environment where we are not using Carbon's global operator
 * new/delete, we must explicitly override class-specific operator
 * new/delete.  This macro helps do that.
 * Almost all of our classes should start something like this:
 *    class Foo
 *    {
 *    public: CARBONMEM_OVERRIDES
 * After that, "o = new Foo" and "delete o" will use our allocator.
 */

#ifdef __GCCXML__
#define CARBONMEM_OVERRIDES
#else
#define CARBONMEM_OVERRIDES \
  void* operator new(size_t size) {return carbonmem_alloc(size);} \
  void* operator new[](size_t size) {return carbonmem_alloc(size);} \
  void* operator new(size_t, void* ptr) {return ptr;} /* placed new */ \
  void operator delete(void* ptr, size_t sz) {carbonmem_dealloc(ptr, sz);} \
  void operator delete[](void* ptr, size_t sz) {carbonmem_dealloc(ptr, sz);} \
  /* icc warns if we define placement new without corresponding delete */ \
  void operator delete(void*, void*) {}
#endif
#endif /* cplusplus */

#endif
