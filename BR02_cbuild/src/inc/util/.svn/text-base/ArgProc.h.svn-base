// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __ArgProc_h_
#define __ArgProc_h_


#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef _LOOP_H_
#include "util/Loop.h"
#endif
#ifndef __UtString_h_
#include "util/UtString.h"
#endif
#ifndef __UtStringUtil_h_
#include "util/UtStringUtil.h"
#endif
#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif
#ifndef __UtHashMap_h_
#include "util/UtHashMap.h"
#endif
#ifndef __FileCollector_h_
#include "util/FileCollector.h"
#endif
#ifndef _UtStringArray_h_
#include "util/UtStringArray.h"
#endif
#ifndef _LOOPFUNCTOR_H_
#include "util/LoopFunctor.h"
#endif



/*!
  \file
  A class object that does command line argument processing.
*/


//! Argument Processing Class
/*! 
  This class enables linkage between options and documentation for the
  options. Help documentation is required as an argument of all option
  additions, so as the options change the documentation should change
  with them.

  Documentation of options supports the \<verbatim\> \</verbatim\>
  keywords, so already formatted text can be used and will not be
  touched by the internal formatter. 

  \todo
  add plus define support, e.g. +define+MINE

  \warning Do not insert newlines, indentations, or tabs in
  strings. Formatting is done automatically.
*/
class ArgProc
{
private:
  struct DerefDouble {
    typedef double value_type;
    typedef double reference;
    double operator()(double* ptr) const {return *ptr;}
  };
  typedef CLoop<UtArray<double*> > DoubleStarLoop;

public: CARBONMEM_OVERRIDES

  static SInt32 cAllPasses;

  //! Type definition for integer iterator
  typedef CLoop<UtArray<SInt32> > IntIter;
  //! Type definition for double iterator
  typedef LoopFunctor<DoubleStarLoop, DerefDouble> DblIter;
  //! Type definition for UtString iterator
  typedef UtStringArray::UnsortedCLoop StrIter;
  //! constructor
  ArgProc();
  //! destructor
  ~ArgProc();

  //! Enumeration of option types
  enum OptionTypeT 
    { 
      eInt, /*!< Integer option */
      eString, /*!< String option */
      eBool, /*!< Boolean option */
      eBoolOverride, /*!< Override for boolean */
      eDouble, /*!< Double (real number) option */
      eInFile, //!< Input file option 
      eOutFile //!< Output file option
    };
  
  //! Enumeration of possible command line parsing results
  //! The order is important, from least to greatest.
  enum ParseStatusT
    {
      eParsed,       /*!< Parse successful */
      eParseNote,    /*!< Informational message about command line */
      eParseWarning, /*!< Non-fatal problem found on command line */
      eParseError,   /*!< Parsing error for option(s) on command line */
      eNotParsed     /*!< Requested option or no options found */
    };
  
  //! Enumeration of possible option states
  enum OptionStateT
    {
      eKnown, /*!< The option exists in the table*/
      eUnknown, /*!< The option does not exist in the table*/
      eTypeMismatch /*!< Option exists, but the type is incorrect */
    };

  //! Enumeration for file type
  enum FileTypeT 
    {
      eRegFile, //!< File must be a regular file
      eDirExist //!< File must be an existing directory
    };
  
  //! Describe overall functionality
  /*!
    Using this method describes the overall functionality of the
    application. This description will be seen with the
    getUsageVerbose() method. The title will be centered. The name can
    be the name of the application or instance name. 

    \param title Title of overall functionality
    \param name Name of application
    \param description Unformatted documentation of application

    \verbatim
    As an example a title could be:
    FUZZY WIDGET DOCUMENTATION

    And a name could be:
    guifuz - GUI For Fuzzy Widgets
    \endverbatim
  */
  void setDescription(const char* title, const char* name, 
                      const char* description);

  
  //! Add a synopsis line to the usage output
  /*!
    This is meant to be like a man page synopsis. You can add several
    synopsis lines. 

    \verbatim
    A typical synopsis line:
    program [-option arg]
    \endverbatim
  */
  void addSynopsis(const UtString&  synopsis);

  //! Add an option that takes integer argument
  /*!
    \param optionName Option name
    \param doc Documentation of option
    \param defaultVal Default value if option is not on the command
    line
    \param noDefault If true defaultVal is ignored
    \param allowMultiple If false only the first encounter of the
    option is parsed and the rest are ignored.

    The doc parameter may contain the \<verbatim\> \</verbatim\>
    keywords. Everything in-between those keywords will be written as
    is when output, except that the text will start and end on
    newlines.

    Integer options support single character with arg and no
    whitespace constructs as well as the normal option, space, arg.
    \verbatim
    -R1
    and
    -R 1
    are equivalent.
    \endverbatim
  */
  void addInt(const char* optionName, const char* doc, 
              SInt32 defaultVal, bool noDefault,
              bool allowMultiple, SInt32 pass);

  
  //! Add an option that takes a UtString argument
  /*!
    \param optionName Option name
    \param doc Documentation of option. See addInt()
    \param defaultVal Default value if option is not on the command
    line
    \param noDefault If true defaultVal is ignored
    \param allowMultiple If false only the first encounter of the
    option is parsed and the rest are ignored.

    The doc parameter may contain the \<verbatim\> \</verbatim\>
    keywords. Everything in-between those keywords will be written as
    is when output, except that the text will start and end on
    newlines.


  */
  void addString(const char* optionName, const char* doc, 
                 const char* defaultVal, bool noDefault,
                 bool allowMultiple, SInt32 pass);
  
  //! Add a switch option 
  /*!
    Switches (booleans) are defaulted to defaultVal. So, a switch on
    the command line makes inverts the default.
    This can be overridden with addBoolOverride().

    \param optionName Option name
    \param doc Documentation of option. See addInt()
    \param defaultVal Default value if option is not on the command
    line

    The doc parameter may contain the \<verbatim\> \</verbatim\>
    keywords. Everything in-between those keywords will be written as
    is when output, except that the text will start and end on
    newlines.

  */
  void addBool(const char* optionName, const char* doc, 
               bool defaultVal,
               SInt32 pass);

  //! Override a previously added booleans value
  /*!
    This overrides a boolean option with the opposite polarity.
    This is meant for turning off things that were once options but
    are now on (true) by default. 
    
    \param overrideOptName The name of this option
    \param optionNameToOverride The name of the option this option is
    overriding.
    \param doc Documentation of this option. If NULL (the default)
    then it simply prints "Overrides optionNameToOverride" where, of
    course, the option name that is being overridden is inserted.

    \warning This will crash if optionNameToOverride is not already
    added!
  */
  void addBoolOverride(const char* overrideOptName, 
                       const char* optionNameToOverride,
                       const char* doc = NULL);

  //! Add an option that takes a number of type double as an argument
  /*!
    \param optionName Option name
    \param doc Documentation of option. See addInt()
    \param defaultVal Default value if option is not on the command
    line
    \param noDefault If true defaultVal is ignored
    \param allowMultiple If false only the first encounter of the
    option is parsed and the rest are ignored.

    The doc parameter may contain the \<verbatim\> \</verbatim\>
    keywords. Everything in-between those keywords will be written as
    is when output, except that the text will start and end on
    newlines.

    
    Double options support single character with arg and no
    whitespace constructs as well as the normal option, space, arg.
    \verbatim
    -T2.2
    and
    -T 2.2
    are equivalent.
    \endverbatim
  */
  void addDouble(const char* optionName, const char* doc,
                 double defaultVal, bool noDefault,
                 bool allowMultiple, SInt32 pass);

  //! Add an option that takes a file to be read
  /*!
    When being processed, this option enforces that the given file
    exist and be readable.

    \param optionName Option name
    \param doc Documentation of option. See addInt()
    \param defaultVal Default value if option is not on the command
    line
    \param noDefault If true defaultVal is ignored
    \param allowMultiple If false only the first encounter of the
    option is parsed and the rest are ignored.

    Filenames are strings; therefore, use the getStrIter() method to
    iterate.

    The doc parameter may contain the \<verbatim\> \</verbatim\>
    keywords. Everything in-between those keywords will be written as
    is when output, except that the text will start and end on
    newlines.
  */
  void addInputFile(const char* optionName, const char* doc,
                    const char* defaultVal, bool noDefault,
                    bool allowMultiple, SInt32 pass);

  //! File option for file that has more command line arguments
  /*!
    Adding an option here makes it only effective in
    preParseCommandLine, which will look for these options on the
    command line, remove them, take their args and read the files,
    eventually creating a complete flattened command line.

    These options always allow multiple instances on the command
    line. And there is no default value.

    \param optionName Name of Option
    \param doc Documentation for the option
  */
  void addInputArgFile(const char* optionName, const char* doc);
    
  //! Add an option that takes a file to be written
  /*!
    When being processed, this option enforces that the given path be
    writable.

    \param optionName Option name
    \param doc Documentation of option. See addInt()
    \param defaultVal Default value if option is not on the command
    line
    \param fileType This is defaulted to eRegFile. This is used
    during the parseCommandLine method to decide if an output file can
    be a directory or a file.
    \param noDefault If true defaultVal is ignored
    \param allowMultiple If false only the first encounter of the
    option is parsed and the rest are ignored.

    Filenames are strings; therefore, use the getStrIter() method to
    iterate.

    The doc parameter may contain the \<verbatim\> \</verbatim\>
    keywords. Everything in-between those keywords will be written as
    is when output, except that the text will start and end on
    newlines.
  */
  void addOutputFile(const char* optionName, const char* doc,
                     const char* defaultVal, 
                     bool noDefault,
                     FileTypeT fileType,
                     bool allowMultiple, SInt32 pass);

  //! Add an unprocessed option that takes an arg
  /*!
    Unprocessed options are not extracted from the command line and
    their value is not used to control the actions of the current
    process. 

    This is useful to wrap another processes arguments into this
    processor. When parseCommandLine is called, it will ignore these
    options. But, when getUsage or getUsageVerbose is called the
    documentation will be available as long as it is placed within a
    section.

    \sa parseCommandLine, getUsage, getUsageVerbose, addToSection, addSynonym
   */
  void addUnprocessedString(const char *group, const char* optionName,
                            const char* doc, const char *defaultVal,
                            bool noDefault,
                            bool allowMultiple, SInt32 pass);


  //! Add an alias for an argument/option
  /*!
    \param masterName Name of the canonical option
    \param subordinateName Name of the synonym
    \param subordIsDeprecated If true, the subordinateName is
    deprecated. It will not appear in documentation and its use will
    generate a warning.

    If a synonym is defined for an unprocessed option and the synonym
    is found on the command line then the master name will be used in
    its place. 
  */
  void addSynonym(const char * masterName,
                  const char * subordinateName,
                  bool subordIsDeprecated = true);


  //! Add a side effect switch
  /*!
    \param masterName  name of the canonical option
    \param sideEffectString
    if \a masterName appears in the command line, then make it appear as if the user specified the combination:
      masterName sideEffectString
    note that current support does not support chains of side effet switches, or the sideEffectString containing more than one switch
  */
  void addSideEffect(const char* masterName, const char* sideEffectString);
  
  //! Deprecate a valid option
  /*!
    Use this to deprecate an option that has no valid synonym.

    \param option Option to deprecate or not
    \param deprecated If true the option will be deprecated.
  */
  void putIsDeprecated(const char* option, bool deprecated);

  //! mark an option so that it is only allowed in an .ef file
  /*!
    Calling this will also mark 'option' so that it is allowed even
    if it was not included in a section. (You should probably not 
    include it in a section since it is an .ef only option.)

    \param option Command line option that will be marked
  */
  void setIsAllowedOnlyInEFFile(const char* option);

  //! Add an unprocessed option that does not take an arg.
  /*!
    This is useful to wrap another processes arguments into this
    processor. When parseCommandLine is called, it will ignore these
    options. But, when getUsage or getUsageVerbose is called the
    documentation will be available as long as it is placed within a
    section.

    Unprocessed booleans can appear multiple times. We don't process
    them as carbon options so we don't care how many times they
    appear. Each occurrence just gets added to a list in the group.
    
    \sa parseCommandLine, getUsage, getUsageVerbose, addToSection
   */
  void addUnprocessedBool(const char *group, const char* optionName,
                          const char* doc, SInt32 pass);

  //! Add an option that is parsed as a wildcard
  /*!
    Wildcard options are tested in the order they appear, but after
    hard coded options have been checked. So be very
    careful. If you have an option that is simply '*', it will get
    everything that isn't already an option.
    
    \param group Group to which to add the unprocessed option.
    \param optionName The wildcarded optionName, e.g., +protect*
    \param visibleOptionName The option name that should appear in the
    documentation. E.g., +protect[.ext]
    \param doc Documentation for the option
    \param pass Pass in which this should be parsed.
  */
  void addUnprocessedBoolWildcard(const char* group, const char* optionName, const char* visibleOptionName, 
                                  const char* doc,
                                  SInt32 pass);

  //! Get the matched argument for an unprocessed bool wildcard
  /*!
    Asserts if optionName is not a bool wildcard. Only valid after
    parseCommandLine has been called.

    \verbatim
    optionName is +protect*
    command line has +protect.vt
    returns a one element loop which contains '+protect.vt'
    \endverbatim
  */
  StrIter getMatchedArgs(const char* optionName);
  
  //! Returns whether or not the option name was parsed
  /*!
    \param optionName Requested option name
    \returns eParsed if the option was found, eNotParsed if it wasn't,
    or eParseError if the option could not be parsed correctly
  */
  ParseStatusT isParsed(const char* optionName) const;


  //! Returns documentation of option
  /*!
    This returns the documentation of a particular option. Useful if
    the user only wants the information for one option.

    \param optName Name of requestion option
    \param usage Pointer to UtString that will be filled with usage
    information
    \param margin Establishes the maximum screen width that the usage
    UtString will use before line wrapping.
    \returns eKnown if the optionName is known, eUnknown otherwise. If
    the option is unknown, the usage UtString is not modified. If the
    option is known the usage UtString will be modified with the usage
    information in a printable UtString as is.
  */
  OptionStateT getUsage(const char* optName, UtString* usage, 
                        SInt32 margin=80) const;

  
  //! Returns general usage of application and options
  /*!
    This fills the usage UtString with a manpage-like description of the
    application and all the options.
    
    \param usage Pointer to UtString that will be filled with usage
    information
    \param noInternalOptions If true and accessHiddenOptions() has
    been called this will \e NOT suppress printing the internal
    options.
    \param margin Establishes the maximum screen width that the usage
    UtString will use before line wrapping.
  */
  void getUsageVerbose(UtString* usage, bool noInternalOptions=false, 
                       SInt32 margin=80) const;

  //! Returns the synopsis information of the application
  /*!
    This places the synopsis information into the usage string.

    \param usage Pointer to UtString that will be filled with usage
    information
    \param margin Establishes the maximum screen width that the usage
    UtString will use before line wrapping.
  */
  void getSynopsis(UtString* usage, SInt32 margin=80) const;


  //! Pre scan the command line and put arguments into resolvedCmdLine
  /*!
    This doesn't do any real processing of the command line arguments
    except for the options added via addInputArgFile().
    preParseCommandLine will have all the command line arguments with
    the addInputArgFile option and arg replaced with the contents of
    the file (recursively, if the file contains more input arg files).

    The preParsed CommandLine then can be passed into parseCommandLine.

    \warning resolvedCmdLine is appended to. So, it should be empty
    before passing into this function.
    \warning The original argv and argc are not modified.
  */
  ParseStatusT preParseCommandLine(int argc, 
				   char* const* argv,
                                   UtStringArgv* resolvedCmdLine, 
                                   UtString* errMsg,
				   bool expandEnvVars = true);

  //! Pre scan the command line and process -ef switch
  /*!
    This processes -ef switch. If there is -ef switch, then no -f 
    switches allowed on the command line. 
    For -ef switch this function saves the index of -ef switch
    in the index argument.
    \arg num_allowed_ef is the number of allowed -ef options
  */
  ParseStatusT preParseCommandLineEF(int argc, char* const* argv,
                                     UInt32 num_allowed_ef,
                                     unsigned int* index, UtString* errMsg);

  //! This function does follow things:
  /*! 1. Tokenizes the ef_context buffer. */
  /*! 2. Populates the resolvedCmdLine with tokens from ef_context and argv. */
  ParseStatusT replaceArgvForEF(int argc, char* const* argv, unsigned int index_ef,
                                UtString* ef_context, UtStringArgv* resolvedCmdLine,
                                UtString* errMsg);

  //! Tokenize and parse a command line from a string.
  /*! See documentation for preParseCommandLine */
  /*! \arg expandDashFArgs if false then a -f filename will NOT be
           expanded to the contents of filename, thus you only get
           part of the commands from str */
  ParseStatusT tokenizeArgString(const char* str, UtStringArgv* resolveCmdLine, 
                                 UtString* errMsg, bool expandDashFArgs = true);

  //! returns the number of times str appears in the arguments argv
  UInt32 countStringMatches(int argc, char** const argv, const char* str, bool expandDashFArgs, ParseStatusT *status );
  //! returns the number of times str appears as a token in buffer
  UInt32 countStringMatches(const UtString& buffer, const char* str, bool expandDashFArgs, ParseStatusT *status );
  
  //! Parse the command line 
  /*!
    This appropriately sets all the different option keys previously
    specified with addBool, addInt, addDouble, etc.

    \param argc Pointer to integer representing the number of
    arguments
    \param argv Array of command line arguments
    \param numOptions Number of options found on the command line, 
    \e NOT the number of arguments. This param gets modified
    regardless of outcome.
    \param errMsg Pointer to UtString to store any error messages
    \param fileCollector Callback functor to collect and organize what appear
           to be files to ArgProc
    \returns Status of command line parsing. eParsed if everything is
    successful, eNotParsed if no options are found, eParseError if an
    error occurred during parsing, or eParseWarning if a non-fatal
    error in the command line was found, such as multiples of a
    non-multiple argument. errMsg will contain the reason in
    printable form for eParseWarning and eParseError.
  */
  ParseStatusT parseCommandLine(int* argc, char** argv, int* numOptions,
                                UtString* errMsg, FileCollector* fileCollector= NULL,
				int* unexpandedArgc = NULL, char** unexpandedArgv = NULL);

  //! Access to STL iterator for integer arguments
  /*!
    \param optionName The name of the option 
    \param iter Pointer to iterator
    \returns Whether or not the option exists. Modifies iter parameter
    if the option exists
  */
  OptionStateT getIntIter(const char* optionName, 
                          IntIter* iter) const;

  //! Access the first value of the named option.
  /*!
    \param optionName The name of the option 
    \param value The value of the option
    \returns Whether or not the option exists. Modifies the value
    if the option exists
  */
  OptionStateT getIntFirst(const char* optionName, SInt32 *value) const;

  //! Access the last value of the named option.
  /*!
    \param optionName The name of the option 
    \param value The value of the option
    \returns Whether or not the option exists. Modifies the value
    if the option exists
  */
  OptionStateT getIntLast(const char* optionName, SInt32 *value) const;
  
  //! Access to STL iterator for double arguments
  /*!
    \param optionName The name of the option 
    \param iter Pointer to iterator
    \returns Whether or not the option exists. Modifies iter parameter
    if the option exists
  */
  OptionStateT getDoubleIter(const char* optionName,
                             DblIter* iter) const;

  //! Get the first value for a double-precision arg
  OptionStateT getDoubleFirst(const char* optionName, double *value) const;

  //! Access to STL iterator for string arguments
  /*!
    \param optionName The name of the option 
    \param iter Pointer to iterator
    \returns Whether or not the option exists. Modifies iter parameter
    if the option exists
  */
  OptionStateT getStrIter(const char* optionName,
                          StrIter* iter)  const;

  //! Access to STL iterator for unexpanded string arguments
  /*!
    \param optionName The name of the option 
    \param iter Pointer to iterator
    \returns Whether or not the option exists. Modifies iter parameter
    if the option exists
  */
  OptionStateT getUnexpandedStrIter(const char* optionName,
				    StrIter* iter)  const;

  //! Get the number of times an option/value pair is parsed on the command line
  /*!
    \warning asserts if the option is not recognized.
    This always returns 0 for boolean options.
  */
  UInt32 getNumArgs(const char* optionName) const;

  //! Get boolean value for boolean option
  /*!
    \param optionName The name of the option 
    \param value Pointer to storage for boolean value
    \returns Whether or not the option exists and is the right
    type. Modifies value parameter if the option is valid.
  */
  OptionStateT getBoolValue(const char* optionName,
                            bool* value)  const;

  //! Set boolean value for a boolean option
  /*!
    \param optionName The name of the option 
    \param value Boolean value
    \returns Whether or not the option exists and is the right
    type.
  */
  OptionStateT setBoolValue(const char* optionName, bool value);

  //! Set value for an integer option
  /*!
    \param optionName The name of the option 
    \param value Integer value
    \returns Whether or not the option exists and is the right
    type.
  */
  OptionStateT setIntValue(const char* optionName, SInt32 value);

  //! Get boolean value for boolean operation or die trying
  /*!
    \param optionName The name of the option.  Asserts if illegal
  */
  inline bool getBoolValue(const char* optionName) const
  {
    bool val;
    OptionStateT state = getBoolValue(optionName, &val);
    INFO_ASSERT(state == eKnown, optionName);
    return val;
  }
      
  
  //! Get a single string value for an option, or return failure status
  //! if there is more than one string
  OptionStateT getStrValue(const char* optionName, const char** val) const;

  //! Get a single string value for an option, or return NULL
  //! if there is more than one string
  const char* getStrValue(const char* optionName) const;

  //! Get the last value for a string option.
  const char* getStrLast(const char* optionName) const;
  
  //! Is this option valid, and does it require a value?
  bool isValid(const char* optionName, bool* requiresValue,
               bool* isInputFile, bool* isOutputFile) const;

  //! Create a section
  /*!
    This creates a section that options can be added to. Sections must
    be created before calling addToSection. Ordering is important
    here. The order in which the sections are created is the order in
    which the sections will be displayed in the usage.

    \sa addToSection
  */
  void createSection(const char* sectionName);

  //! Add the option to the section.
  /*!
    \warning An option must be added to a section in order for it to
    be printed in the usage, unless showHiddenOptions is called.

    \warning The section must have been created by createSection. An
    assertion will occur if it has not.

    \sa createSection, showHiddenOptions
  */
  void addToSection(const char* sectionName, const char* optionName);

  //! Show the hidden options in a separate section
  /*!
    If this is called all the options that are not associated with a
    section will be displayed in an "INTERNAL" section in the usage.
  */
  void accessHiddenOptions();

  //! Allow usage of ALL unsectioned options
  /*!
    This will allow the usage of internal switches, but will not print
    help for those switches unless accessHiddenOptions() was called.
  */
  void allowUsageAll();

  //! Allow the use of an unsectioned option
  /*!
    \warning This has no effect if the option is assigned to a
    section. In such a case, it is already allowed to be used.
    \param unsectionedOption Option to which to grant usage
    \returns whether or not the option exists. Will not return
    eTypeMismatch
  */
  OptionStateT allowUsage(const char* unsectionedOption);

  //! Disallow the use of an unsectioned option
  /*!
    \warning This has no effect if the option is assigned to a
    section. In such a case, it is allowed to be used no matter what.
    \param unsectionedOption Option to which to deny usage
    \returns whether or not the option exists. Will not return
    eTypeMismatch

    This is normally called after allowUsageAll(), which allows usage
    of all unsectioned options. This function then can turn off risky
    options.
  */
  OptionStateT disallowUsage(const char* unsectionedOption);
  

  //! Indicate whether the system should warn if it encounters duplicate args
  /*! default: true
  */
  void putWarnOnDuplicates(bool warnOnDuplicates);

  //! Create a unprocessed argument group
  /*!
    This creates a group that unprocessed options can be added to. Groups must
    be created before calling addToUnprocessedGroup.

    \sa addToUnprocessedGroup
  */
  void createUnprocessedGroup(const char* groupName);

  //! Returns the list of unprocessed options
  UtStringArgv &lookupUnprocessedGroupOccurrences( const char* groupName );

  //! Reset the option to its pre-parsed state
  /*!
    This is useful if you have an argument processor that needs to be
    called more than once and you don't want to maintain the state.
  */
  void reInit(const char* optionName);
  

  //! Reset all the options in a section to their pre-parsed state
  void reInitSection(const char* sectionName);

  //! Check for invalid use of options that have been restricted so that they are only allowed in .ef files.
  /*!  \returns true if an .ef-only option was found outside of an .ef file (and appends reason to errmsg)
   */
  bool testForMisuseOfEFOptions(const UtString& ef_buffer, int argc, char** argv, UtString*errmsg );
  
  // forward declarations of private classes -- these need to be in the
  // public section or Microsoft Visual C++ complains
  class CmdLineArg;
  friend class CmdLineArg;
  class IntCmdLineArg;
  friend class IntCmdLineArg;
  class StrCmdLineArg;
  friend class StrCmdLineArg;
  class BoolCmdLineArg;
  friend class BoolCmdLineArg;
  class DoubleCmdLineArg;
  friend class DoubleCmdLineArg;
  class InFileCmdLineArg;
  friend class InFileCmdLineArg;
  class OutFileCmdLineArg;
  friend class OutFileCmdLineArg;
  class BoolOverrideArg;
  friend class BoolOverrideArg;
  class BoolWildcardArg;
  friend class BoolWildcardArg;
  class LineWrapBuf;
  friend class LineWrapBuf;
  class OptionCmp;
  friend class OptionCmp;

private:
  UtString mTitle;
  UtString mName;
  UtString mDescription;
  mutable UtString mSubstrBuf;
  UtStringArray mSynopses;
  UtStringArray mOptionNames;

  
  typedef UtHashMap<UtString, CmdLineArg*, HashValue<UtString> > OptionTable;
  OptionTable mOptionTable;

  OptionTable mUnprocessed;     // map of option string to CmdLineArg ptrs (includes synonyms)

  typedef UtArray<CmdLineArg*> OptionVector;
  typedef CLoop<OptionVector> OptionVectorCLoop;
  OptionVector mOptions;            // vector of every processed option seen
  OptionVector mUnprocessedOptions; // vector of every unprocessed option seen
  OptionVector mUnprocessedWildcardOptions;

  class Section;
  friend class Section;
  class UnprocessedGroup;
  friend class UnprocessedGroup;

  typedef UtArray<Section*> SectionVector;
  SectionVector mSections;

  typedef UtHashMap<UtString, Section*, HashValue<UtString> > SectionTable;
  SectionTable mSectionTable;

  typedef UtArray<UnprocessedGroup*> UnprocessedGroupVector;
  UnprocessedGroupVector mUnprocessedGroups;

  typedef UtHashMap<UtString, UnprocessedGroup*, HashValue<UtString> > UnprocessedGroupTable;
  UnprocessedGroupTable mUnprocessedGroupTable;

  SInt32 mPass;

  bool mAccessHiddenOptions;
  bool mWarnOnDuplicates;

  CmdLineArg* maybeAddOption(const char* optionName, const char* doc, 
                             bool allowMultiple, OptionTypeT type, SInt32 pass);

  bool passMatches(CmdLineArg* option) const;

  const CmdLineArg* lookupOption(const UtString& optionName, const char** scrunchedOption) const;
  CmdLineArg* lookupOption(const UtString& optionName, const char** scrunchedOption);

  Section* lookupSection(const UtString& sectionName);
  //! Add the option to the unprocessed argument group.
  /*!  
    \warning An option must be added to a group for it to be passed
    to either Cheetah or Jaguar.

    \warning The group must have been created by
    createUnprocessedGroup. An assertion will occur if it has not.

    \sa createUnprocessedGroup
  */
  void addToUnprocessedGroup(const char* groupName, const char* optionName);

  //! Locate the group representing the named unprocessed group
  UnprocessedGroup* lookupUnprocessedGroup(const UtString& groupName);
  
  const CmdLineArg* lookupUnprocessed(const UtString& optionName) const;
  CmdLineArg* lookupUnprocessed(const UtString& optionName);

  void addSynopsisToBuf(LineWrapBuf* lwBuf) const;

  void writeUsageToBuf(const CmdLineArg* option, 
                       LineWrapBuf* lwBuf) const;

  void setParseErrMsg(UtString* str, CmdLineArg* option, 
                      const UtString& optionName);

  bool checkInputPath(const char* inPath, UtString* str);
  bool checkOutputPath(const char* outPath, 
                       OutFileCmdLineArg* arg,
                       UtString* str);
  
  ParseStatusT 
  includeArgFile(const char* file, 
		 UtStringArgv* resolveCmdLine, 
                 UtString* errMsg, 
		 bool* nextArgIsFile,
		 bool expandEnvVars = true,
                 bool expandDashFArgs = true);

  //! parse a single command line
  /*!  puts results in resolveCmdLine
      \arg nextArgIsFile  will be set to true if the last argument processed was a -f without an associated filename
      \arg commentDepth if the file ends before the all comments are closed then this will be the number left open
      \arg expandEnvVars if true then environment variables are expanded
      \arg expandDashFArgs if true then -f <filename> are expanded and the contents are processed
   */
  ParseStatusT
  parseString(const char* str, UtStringArgv* resolveCmdLine,
              UtString* errMsg, bool* nextArgIsFile, int* commentDepth,
              bool expandDashFArgs = true,
	      bool expandEnvVars = true);
  
  bool isInputArgFile(const char* arg) const;
  const char* getSideEffect(const char* arg) const;
  
  // forbid
  ArgProc(const ArgProc&);
  ArgProc& operator=(const ArgProc);

};

#endif
