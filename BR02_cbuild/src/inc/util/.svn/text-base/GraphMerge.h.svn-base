// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file

  Implements merging nodes in a graph while trying to maintain the
  acyclic nature of the graph.
*/

#ifndef _GRAPHMERGE_H_
#define _GRAPHMERGE_H_

#include "util/GraphSCC.h"

//! class UtGraphMerge
/*! A class to merge nodes in a graph. There are a number of steps
 *  (some optional) in merging nodes in a graph. They are:
 *  
 * 1. Provide a populated graph. This must be a GenericBigraph because
 *    this algorithm needs the ability to cut nodes out of the middle
 *    and reinsert new nodes and edges.
 *
 * 2. Run a pass to make sure that the graph is acyclic.
 *
 * 3. Run a pass to merge nodes by depth. This involves doing a DFS
 *    over the graph computing the depth of every node. Nodes with the
 *    same depth can be merged without introducing cycles because
 *    there can't be a path between two nodes with the same depth.
 *
 * 4. Run a pass to merge nodes with their fanin. This can be done as
 *    long as there is no path from the fanout node to the fanin node
 *    through an unmergable node. The nodes that are mergable are then
 *    merged in the order of their depth.
 *
 * To use this class, the code must provide implementations for the
 * following abstract functions:
 *
 *
 * 1. A function that tests whether a node is a valid start node for
 *    merging with fanin.
 *
 * 2. A function that tests whether a graph node is mergable.
 *
 * 3. Given two mergable nodes, test if the pair is mergable.
 *
 * 4. A function to merge a vector of nodes in the given order.
 *
 * 5. A function to handle a node removed from the graph (merged node)
 */
class UtGraphMerge
{
public:
  //! constructor
  UtGraphMerge() : mGraph(NULL), mInitSuccess(false) {}

  //! destructor
  virtual ~UtGraphMerge() {}

  //! Initialization routine
  /*! This function initializes the graph for merging. The only step
   *  currently is to check that the graph is acyclic.
   *
   *  \param The graph to be merged
   *
   *  Returns true if initialized succeeded (no cycles found)
   */
  bool init(BiGraph* graph);

  //! Abstraction to sort graph nodes
  /*! This is needed by mergeByDepth to sort nodes into buckets. But
   *  it is also needed by mergeFanin to do sorted traversals. The
   *  latter is so that we have deterministic behavior.
   */
  typedef GraphSortedWalker::NodeCmp NodeCmp;

  //! Abstraction to sort graph edges
  /*! This is needed by mergeFanin to do sorted traversals over the
   *  graph so that we get deterministic behavior.
   */
  typedef GraphSortedWalker::EdgeCmp EdgeCmp;

  //! Perform a merge pass by depth
  /*! This function computes a depth for every node in the graph and
   *  then merges the nodes that are mergable and have the same depth.
   *
   *  \param nodeBuckets - a compare class that can sort all the graph
   *  nodes into buckets that can be merged.
   *
   *  \param nodeOrder - Once a group of nodes has been chosen for
   *  merging, it uses nodeOrder to pick an order between them. This
   *  makes the merging stable across compilers.
   */
  void mergeByDepth(NodeCmp* nodeBuckets, NodeCmp* nodeOrder);

  //! Perform a merge pass by walking the fanin
  /*! This function walks the fanin looking for cones of mergable node
   *  without a path from a mergable node through an unmergable node
   *  to a mergable node.
   *
   *  \param nodeOrder - Used to pick an ordering between nodes when
   *  there isn't one dictated by the merge. This makes the algorithm
   *  deterministic.
   *
   *  \param edgeOrder - Used to pick an ordering between edges from a
   *  node. This makes the algorithm deterministic.
   */
  void mergeFanin(NodeCmp* nodeOrder, EdgeCmp* edgeOrder);

  //! Perform a merge pass in depth order
  /*! This function computes a depth for every node in the graph and
   *  then merges the nodes that are mergable in depth order. This may
   *  introduce cycles in the graph. It is up to the caller to deal
   *  with this. No other type of merging can occur after this is
   *  done.
   *
   *  \param nodeBuckets - a compare class that can sort all the graph
   *  nodes into buckets that can be merged.
   *
   *  \param nodeOrder - Once a group of nodes has been chosen for
   *  merging, it uses nodeOrder to pick an order between them. This
   *  makes the merging stable across compilers.
   */
  void mergeByDepthOrder(NodeCmp* nodeBuckets, NodeCmp* nodeOrder);

protected:
  //! A vector of nodes (used to pass mergable nodes)
  typedef UtVector<GraphNode*> NodeVector;

  //! An iterator over the vector of nodes
  typedef CLoop<NodeVector> NodeVectorLoop;

  //! Abstract virtual function to test if this is a valid start node
  virtual bool validStartNode(const GraphNode* node) const = 0;

  //! Abstract virtual function to test if a node is mergable
  virtual bool nodeMergable(const GraphNode* node) const = 0;

  //! Abstract virtual function to test if an edge is mergable
  virtual bool edgeMergable(const GraphEdge* edge) const = 0;

  //! Abstract virtual function to merge a group of nodes
  /*! The nodes provided are in the order they should be merged. The
   *  LAST node in the vector is the one in which all the data should
   *  be placed. It will represent the list of nodes in the new graph.
   */
  virtual void mergeNodes(const NodeVector& nodes) = 0;

  //! Abstract virtual function to handle removing a node from the graph
  virtual void removeMergedNode(GraphNode* node) = 0;

  //! Abstract virtual function to handle encapsulating a cycle in the graph
  virtual void encapsulateCycle(const NodeVector& nodes) = 0;

  //! Abstract virtual function to merge a pair of edges
  /*! When merging nodes, edges that are incident to the merged nodes
   *  need to get moved to the collection node. However, when that
   *  happens it may move it to a spot where the edge already
   *  exists. Since duplicate edges are not allowed, the existing edge
   *  is kept and the edge to the merged node is deleted.
   *
   *  This virtual function gives the ability to merge any edge data.
   *
   *  The first argument is the kept edge while the second one is the
   *  deleted edge.
   */
  virtual void mergeEdges(GraphEdge* dstEdge, const GraphEdge* srcEdge) const = 0;

  //! Get the depth for a given node
  UIntPtr getDepth(const GraphNode* node) const
  {
    return mGraph->getScratch(node);
  }

  //! Override debug routine for printing
  virtual void print(const GraphNode*) const;

private:
  //! Compute the depth for nodes in a graph.
  void computeDepth();

  //! Once the depth has been assigned this function merges nodes
  void mergeNodesByDepth(NodeCmp* nodeBuckets, NodeCmp* nodeOrder);

  //! Once the depth has been assigned this function merges nodes
  void mergeNodesByDepthOrder(NodeCmp* nodeBuckets, NodeCmp* nodeOrder);

  //! Gather a sorted vector of nodes to start merging by fanin
  void gatherStartNodes(NodeVector* nodes, NodeCmp* nodeOrder);

  //! Given a starting set of nodes, merge in fanin nodes
  void mergeNodesWithFanin(const NodeVector& nodes, NodeCmp*, EdgeCmp*);

  //! This class calls nodeMergable and edgeMergable
  friend class UnmergableNodesWalker;

  //! This class calls nodeMergable
  friend class GMDepthWalker;

  //! Given a starting node, merge fanin nodes
  void mergeNodeWithFanin(GraphNode* node, NodeCmp*, EdgeCmp*);

  //! Update the graph and ask the derived class to merge some nodes
  void mergeGraphNodes(const NodeVector& nodes);

  //! Remove all the merged nodes after merging is complete
  void cleanupMergedNodes();

  //! Encapsulate a cyclic component into one node
  void encapsulateComponent(GraphSCC::Component* comp);

  //! Graph to be merged
  BiGraph* mGraph;

  //! If set, init succeeded
  bool mInitSuccess;
}; // class UtGraphMerge

#endif // _GRAPHMERGE_H_
