// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*! \file
 *  Contains an implementation of a union-find data structure.
 *  This class can be used to efficiently partition a set into equivalence
 *  classes defined by some transitive equivalence relation.
 *
 *  An example use is to partition the nodes of a directed graph into
 *  weakly-connected components by including (u,v) in the relation
 *  whenever the graph contains an edge u->v.
 *
 *  The Union-Find structure is generally useful whenever you want to
 *  build and merge sets of objects and then efficiently test whether
 *  two objects ended up in the same set, or if you want to get the
 *  full partition of the objects into fully-merged sets.
 */

#ifndef _UNION_FIND_H_
#define _UNION_FIND_H_

#include "util/UtSet.h"
#include "util/UtMap.h"
#include "util/Util.h"

//! A Union-Find class used to partition sets into equivalence classes.
template <typename T,        // the type of items
          class EqSet,       // the type of an equivalence set of items (default is UtSet<T>)
          class EqPartition> // the type of a partition of items (default is UtMap<T,EqSet>)
class UnionFind
{
private:
  //! Internal class used to hold a value and its set association
  class Entry
  {
  public: CARBONMEM_OVERRIDES
    Entry(T value) : mRepresentative(NULL), mDepth(1), mValue(value) {}
    Entry(T value, Entry* rep) : mRepresentative(rep), mDepth(1), mValue(value)
    {
    }

    Entry* mRepresentative; //!< parent pointer
    UInt32 mDepth;          //!< depth used for tree balancing
    T      mValue;          //!< the value being stored
  };
public: CARBONMEM_OVERRIDES
  typedef EqSet       EquivalenceSet;
  typedef EqPartition EquivalencePartition;

  //! Constructor - creates an empty partition
  UnionFind() : mRoots(), mEntryMap() {}
  //! Add a new value in its own partition
  void add(T value)
  {
    typename UtMap<T,Entry*>::iterator pos = mEntryMap.find(value);
    if (pos == mEntryMap.end())
    {
      Entry* entry = new Entry(value);
      mEntryMap[value] = entry;
      mRoots.insert(entry);
    }
  }
  //! Add a relation between two values -- merges their partitions
  void equate(T value1, T value2)
  {
    // if the entries are the same, just to add its own partition.
    if (value1==value2) {
      add(value1);
      return;
    }

    // find entries for the values, if they exist
    typename UtMap<T,Entry*>::iterator pos = mEntryMap.find(value1);
    Entry* entry1 = NULL;
    if (pos != mEntryMap.end()) {
      entry1 = pos->second;
    }
    Entry* entry2 = NULL;
    pos = mEntryMap.find(value2);
    if (pos != mEntryMap.end()) {
      entry2 = pos->second;
    }

    // Figure out which should be the representative and make entry2
    // point to it and make entry1 point to the equivalence.
    //
    // The only exception is if both exist and are known. In that case
    // the equivalence has already been made and we return.
    if (entry1 && entry2) {
      // Both entries are known. Return if the are already equivalenced
      entry1 = findRep(entry1);
      entry2 = findRep(entry2);
      if (entry1 == entry2) {
        return;
      }

      // Pick the entry with the larger depth as the
      // representative. The other is no longer a root so adjust it
      if (entry1->mDepth > entry2->mDepth) {
        Entry* tmp = entry2;
        entry2 = entry1;
        entry1 = tmp;
      }
      entry1->mRepresentative = entry2;
      mRoots.erase(entry1);

    } else if (entry1) {
      // entry2 is a new value, but entry1 must be deeper so make it
      // the representative
      entry2 = entry1;
      entry1 = new Entry(value2, findRep(entry2));
      mEntryMap[value2] = entry1;

    } else if (entry2) {
       // entry1 is a new value, entry2 is the representative.
      entry1 = new Entry(value1, findRep(entry2));
      mEntryMap[value1] = entry1;

    } else {
      // both values are new and entry2 is the root
      entry2 = new Entry(value2);
      entry1 = new Entry(value1, entry2);
      mEntryMap[value1] = entry1;
      mEntryMap[value2] = entry2;
      mRoots.insert(entry2);
    }

    // And now possibly adjust the depth of the representative. The
    // only reason the representative depth would get larger is if
    // they were the same depth because entry could not have a greater
    // depth.
    //
    // Note that depth is not maintained very accurately. It is
    // maintained accurately enough so that representatives have a
    // depth of 2 or more. But everything else is inconsistent.
    //
    // The depth is made invalid at the find operation where
    // representative pointers are updated.
    if (entry2->mDepth == entry1->mDepth) {
      entry2->mDepth += 1;
    }
  } // void equate

  //! Add a relation between N values.
  void equate(const EquivalenceSet & entry_set)
  {
    typename EquivalenceSet::const_iterator iter = entry_set.begin();
    if (iter != entry_set.end()) {
      T first_value = *iter;
      // Add independently in case of singletons.
      add(first_value); 
      for ( ++iter; iter != entry_set.end(); ++iter) {
        equate(first_value, *iter);
      }
    }
  }

  //! Test if a value is equivalenced.
  bool hasEquivalence(T value)
  {
    // Test if it is in our set
    typename UtMap<T,Entry*>::iterator pos = mEntryMap.find(value);
    if (pos == mEntryMap.end()) {
      // no, it can't be equivalenced
      return false;
    }

    // Get the representative node. If it is different or if this
    // entry has a depth greater than one, then it has equivalences
    Entry* entry = pos->second;
    Entry* repEntry = findRep(entry);
    return ((repEntry != entry) || (entry->mDepth > 1));
  }

  //! Find the equivalence class representative for a value
  T find(T value)
  {
    typename UtMap<T,Entry*>::iterator pos = mEntryMap.find(value);
    if (pos == mEntryMap.end())
      return value;
    Entry* entry = pos->second;
    UtList<Entry*> path;
    while (entry->mRepresentative)
    {
      path.push_front(entry);
      entry = entry->mRepresentative;
    }
    for (typename UtList<Entry*>::iterator p = path.begin(); p != path.end(); ++p)
    {
      // Note that if we want to keep the depth accurate we should
      // update the depth of the original representative pointer. But
      // doing so would mean walking towards the leaves which is not
      // possible without doing an O(N) traversal or keeping pointers
      // in both directions.
      Entry* move = *p;
      move->mRepresentative = entry;
    }
    return entry->mValue;
  }

  //! Get a map from class representative to sets containing all values in the class
  EquivalencePartition* getPartition()
  {
    EquivalencePartition* partition = new EquivalencePartition;
    for (typename UtMap<T,Entry*>::iterator p = mEntryMap.begin(); p != mEntryMap.end(); ++p)
    {
      Entry* entry = p->second;
      Entry* rep = findRep(entry);

      (*partition)[rep->mValue].insert(entry->mValue);
    }
    return partition;
  }
  //! Erase all of the data mapping values to equivalence classes
  void clear()
  {
    for (typename UtMap<T,Entry*>::iterator p = mEntryMap.begin(); p != mEntryMap.end(); ++p)
      delete p->second;
    mEntryMap.clear();
    mRoots.clear();
  }
  //! Destructor
  ~UnionFind()
  {
    clear();
  }
private:
  //! private helper function to walk the representative tree back to the root
  Entry* findRep(Entry* entry) const
  {
    while (entry->mRepresentative)
      entry = entry->mRepresentative;
    return entry;
  }
private:
  UtSet<Entry*>   mRoots;    //!< The set of class representatives
  UtMap<T,Entry*> mEntryMap; //!< A map from values to their entries
};

#endif // _UNION_FIND_H_
