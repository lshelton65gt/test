// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
// 

#ifndef __UtDebugRecursion_h_
#define __UtDebugRecursion_h_

//! DebugRecursion class
/*! This file implements a debugging aid for recursive procedures.
 * It efficiently keeps track of the stack, using design nodes as
 * labels.  This duplicates the backtracek command in gdb, but it
 * does so where the programmer controls the labels and the stack
 * frames that are shown.  Consider a recursive algorithm where there
 * are three mutually recursive routines.   Gdb might show you this:
 *
 *   (gdb) where
 *   traverse1(node=0xabcdabcd)
 *   traverse2(node=0xabcdabcd)
 *   traverse3(node=0xabcdabcd)
 *   traverse1(node=0x01010101)
 *   traverse2(node=0x01010101)
 *   traverse3(node=0x01010101)
 *   traverse1(node=0x9e9e9e9e9)
 *   traverse2(node=0x9e9e9e9e9)
 *   traverse3(node=0x9e9e9e9e9)
 * 
 * It is now up to the programmer to wade through the noise in this
 * backtrace, and decode the pointers into meaningful names.
 *
 * This class gives the programmer an easy way to instrument his
 * recursive algorithm with minimal overhead during debug, and zero
 * overhead for product builds.
 *
 * The easiest way to use this class is via the macro UtDEBUG, which
 * reduces to nothing when compiled for product.  Example:
 *
 *  static UtDebugRecursion* sDebugStack = NULL;
 *  void recursive_proc(Node* node)
 *  {
 *    UtDEBUG(&sDebugStack, "recursive_proc", node->getHierName(),
 *            &node->getLoc())
 *    ...
 *  }
 *
 */ 

#ifdef CDB
//#if 1
class UtDebugRecursion;

struct UtDebugRecursion
{
  //! constructor -- usually this is instantiated on the stack in a recursion
  /*! If the user defines the environment variable DEBUG_SPEW, then the
   *  stack track will be spewed to the screen automatically, with indentation
   *  according the recursive depth.  With or without that environment
   *  variable, the user can see the current stack trace from inside a
   *  recursive routine with one of these objects by typing
   *    (gdb) p debug.print(1)
   */
  UtDebugRecursion(const char* proc, HierName* name, const SourceLocator* loc,
                   UtDebugRecursion** stack,
                   const char* spewEnvVar)
  {
    mStack = stack;
    mProc = proc;
    mParent = *stack;
    if (mParent != NULL)
    {
      mDepth = mParent->mDepth + 1;
      mSpew = mParent->mSpew;
    }
    else
    {
      mSpew = (getenv(spewEnvVar) != NULL);
      mDepth = 0;
    }
    *stack = this;
    mName = name;
    mLoc = loc;
    if (mSpew)
      print(false);
  }

  ~UtDebugRecursion() {
    *mStack = mParent;
  }

  //! print out the current stack frame.  If recurse is true, then print
  //! out the entire stack trace
  void print(bool recurse)
  {
    UtOStream& cout = UtIO::cout();
    if (recurse)
    {
      if (mParent != NULL)
        mParent->print(true);
    }
    else
      NUBase::indent(2*mDepth);
    cout << mProc;
    UtString buf;
    if (mName != NULL)
    {
      mName->compose(&buf);  // do not include root in name 
      cout << " " << buf;
    }
    if (mLoc != NULL)
      cout << " " << mLoc->getFile() << ":" << mLoc->getLine();
    cout << UtIO::endl;
  }

  UtDebugRecursion** mStack;
  UtDebugRecursion* mParent;
  SInt32 mDepth;
  const char* mProc;
  HierName* mName;
  const SourceLocator* mLoc;
  bool mSpew;
};
#define UtDEBUG_STACK(varname) static UtDebugRecursion* varname = NULL;
#define UtDEBUG(stackp, name, hier, loc, env) UtDebugRecursion debug(name, hier, loc, stackp, env);
#define UtDEBUG_TEMP(x) x
#else
#define UtDEBUG_STACK(varname)
#define UtDEBUG(stackp, name, hier, loc, env)
#define UtDEBUG_TEMP(x)
#endif

#endif // __UtDebugRecursion_h_
