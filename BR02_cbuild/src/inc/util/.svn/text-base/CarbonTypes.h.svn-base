// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __CarbonTypes_h_
#define __CarbonTypes_h_

#ifndef __CarbonPlatform_h_
#include "util/CarbonPlatform.h"
#endif

#if pfMSVC
#define _CRT_SECURE_NO_DEPRECATE 1 // allow sprintf despite deprecation
#endif

#if (!pfWINDOWS | pfGCC | pfSPARCWORKS)
#ifndef _CPP_CSTDDEF
#include <cstddef>
#endif
#endif

#ifndef __carbon_shelltypes_h_
#include "shell/carbon_shelltypes.h"
#endif

/*!
  \file
  The definitive Carbon primitive types. This file also holds the
  description for the carbon namespace.
*/

  //! 1 byte signed maximum value
#define UtSINT8_MAX    128

  //! 1 byte signed minimum value
#define UtSINT8_MIN    (-UtSINT8_MAX - 1)

  //! 1 byte unsigned maximum value
#define UtUINT8_MAX    0xff

  //! 1 byte unsigned minimum value
#define UtUINT8_MIN    0x00

  //! 2 bytes signed maximum value
#define UtSINT16_MAX   32767

  //! 2 bytes signed minimum value
#define UtSINT16_MIN   (-UtSINT16_MAX - 1)

  //! 2 bytes unsigned maximum value
#define UtUINT16_MAX   0xffff

  //! 2 bytes unsigned minimum value
#define UtUINT16_MIN   0x0000

  //! 4 bytes signed maximum value
#define UtSINT32_MAX   2147483647

  //! 4 bytes signed minimum value
#define UtSINT32_MIN   (-UtSINT32_MAX - 1)

  //! 4 bytes unsigned maximum value
#define UtUINT32_MAX   0xffffffff

  //! 4 bytes unsigned minimum value
#define UtUINT32_MIN   0x00000000

#if pfMSVC
// Microsoft accepts 64-bit constants undecorated, and does not like the LL
// or ULL suffixes
# define KSInt64(num) num
# define KUInt64(num) num##U
#elif (pfLP64)
# define KSInt64(num) SInt64(num##L)
# define KUInt64(num) num##UL
#elif (pfGCC_2 || pfGCC_3 || pfGCC_4 || pfICC)
// gcc 2.95.3 requires an LL or ULL suffix on 64-bit constants
# define KSInt64(num) SInt64(num##LL)
# define KUInt64(num) UInt64(num##ULL)
#else
# define KSInt64(num) static_cast<SInt64>(num)
# define KUInt64(num) static_cast<UInt64>(num)
#endif


  //! 8 bytes signed maximum value
#define UtSINT64_MAX   KSInt64(9223372036854775807)

  //! 8 bytes signed minimum value
#define UtSINT64_MIN   (-UtSINT64_MAX - KSInt64(1))

  //! 8 bytes unsigned maximum value
#define UtUINT64_MAX   KUInt64(0xffffffffffffffff)

  //! 8 bytes unsigned minimum value
#define UtUINT64_MIN   KUInt64(0x0000000000000000)

//! type definition for code generated object pointers
typedef void* CarbonOpaque;

//! Clock edge.  
//! Level high/low used to represent synchronous reset.
//! Active/Inactive used to represent a constant level.
enum ClockEdge { 
  eClockEdgeStart = 0,
  eClockPosedge = eClockEdgeStart, 
  eClockNegedge, 
  eLevelHigh, 
  eLevelLow, 
  eClockEdgeStop  = eLevelLow 
};

//! Event types (today it is input, output, const, clock edge (bidir?)
enum EventType {eConstant, eIn, eOut, eClockEdge};
//! Level or edge sensitivity
enum Sensitivity {eSensLevel, eSensEdge, eSensLevelOrEdge};

//! Net flags,
/*! Some of these enums are flags that can be OR'ed together,
 * while others are used as encoded fields (see the ePortMask and eDeclareMask)
 */
enum NetFlags {
  eNoneNet       = 0x00000000, //!< Net has no flags

  ePortMask      = 0x00000003, //!< Net is a port
  eInputNet      = 0x00000001, //!< Net is an input
  eOutputNet     = 0x00000002, //!< Net is an output
  eBidNet        = 0x00000003, //!< Net is a bid

  eFlopNet       = 0x00000004, //!< Net is a flop (sequential block) output
  eLatch         = 0x00000008, //!< Net possibly behaves like a latch

  eDeclareMask   = 0x000001F0, //!< Mask to get declaration field value
  eDMTimeNet     = 0x00000010, //!< Net is declared as a time variable
  eDMWireNet     = 0x00000020, //!< Net is declared as wire
  eDMTriNet      = 0x00000030, //!< Net is declared as tri
  eDMTri1Net     = 0x00000040, //!< Net is declared as tri1
  eDMSupply0Net  = 0x00000050, //!< Net is declared as supply0
  eDMWandNet     = 0x00000060, //!< Net is declared as wand
  eDMTriandNet   = 0x00000070, //!< Net is declared as triand
  eDMTri0Net     = 0x00000080, //!< Net is declared as tri0
  eDMSupply1Net  = 0x00000090, //!< Net is declared as supply1
  eDMWorNet      = 0x000000a0, //!< Net is declared as wor
  eDMTriorNet    = 0x000000b0, //!< Net is declared as trior
  eDMTriregNet   = 0x000000c0, //!< Net is declared as trireg
  eDMRegNet      = 0x000000d0, //!< Net is declared as reg
  eDMRealNet     = 0x000000e0, //!< Net is declared as a real or realtime variable
  eDMIntegerNet  = 0x000000f0, //!< Net is declared as an integer variable

  /*
   * Next group must correspond to scalar DM nets
   */
  e2DNetMask     = 0x00000100, //!< Net is some kind of 2D object
  eDMTime2DNet   = 0x00000110, //!< Net declared as 2-dimension time variable
  eDMWire2DNet   = 0x00000120, //!< Net is declared as 2-dimension wire
  eDMReg2DNet    = 0x000001d0, //!< Net is declared as 2-dimension reg
  eDMRTime2DNet  = 0x000001e0, //!< Net declared as 2-dimension realtime
  eDMInteger2DNet= 0x000001f0, //!< Net is declared as 2-dimension integer

  // note that the following enum values are used as flags and can be OR'ed
  // with any of the above enum values on a net.
  eAliasedNet    = 0x00000200, //!< Net is aliased to another net
  eAllocatedNet  = 0x00000400, //!< Net needs storage allocated for it
  eInaccurateNet = 0x00000800, //!< Net and alias ring cannot and will not be waveformed.
  eTriWritten    = 0x00001000, //!< Net is can be driven to Z by the model
  eBlockLocalNet = 0x00002000, //!< Net is declared local to a named block
  eNonStaticNet  = 0x00004000, //!< Net does not have static semantics (ie, some temporaries)
  ePullUp        = 0x00008000, //!< Net is pulled-up
  ePullDown      = 0x00010000, //!< Net is pulled-down
  ePrimaryZ      = 0x00020000, //!< Net is tristatable or undriven, and is either a primary bid/output or aliased to one
  eInClkPath     = 0x00040000, //!< Net is used to compute a derived clock
  eInDataPath    = 0x00080000, //!< Net is used to compute data values.
  eEdgeTrigger   = 0x00100000, //!< Net is used as an edge trigger.
  eTempNet       = 0x00200000, //!< Net is a temporary created by the compiler
  eForcibleNet   = 0x00400000, //!< Net is forcible
  eDepositNet    = 0x00800000, //!< Net is Depositable
  eRecordPort    = 0x01000000, //!< Net is a record member used as a port
  eReadNet       = 0x02000000, //!< Net is read from (used as an r-value)
  eWrittenNet    = 0x04000000, //!< Net is written to (used as an l-value)
  eConstZ        = 0x08000000, //!< Net is assigned to a constant Z
  eDeadNet       = 0x10000000, //!< Net is elaboratedly dead
  eReset         = 0x20000000, //!< Net is used as a reset
  eSigned        = 0x40000000, //!< Net represents a signed value
  eClearAtEnd    = 0x80000000  //!< Net should be cleared at the end of the schedule
}; // If you add/change any flags, you must add to/fix the following
   // defines. You also need to fix carbonNetPrintFlags in CarbonTypeUtil.cxx


#define NetIsPortType(flags, flag) (((flags) & ePortMask) == (flag))
#define NetIsDeclaredAs(flags, flag) (((flags) & eDeclareMask) == (flag))
#define NetRedeclare(flags, flag) NetFlags(((flags) & ~eDeclareMask) | (flag))
#define NetClkTempPrefix "$tempclk_"
#define NetClkTempPrefixLen (sizeof(NetClkTempPrefix) - 1)
#define NetIsFlop(flags)  (((flags) & eFlopNet) != 0)
#define NetIsLatch(flags) (((flags) & eLatch) != 0)
#define NetIsAliased(flags) (((flags) & eAliasedNet) != 0)
#define NetIsAllocated(flags) (((flags) & eAllocatedNet) != 0)
#define NetIsInaccurate(flags) (((flags) & eInaccurateNet) != 0)
#define NetIsTriWritten(flags) (((flags) & eTriWritten) != 0)
#define NetIsBlockLocal(flags) (((flags) & eBlockLocalNet) != 0)
#define NetIsNonStatic(flags) (((flags) & eNonStaticNet) != 0)
#define NetIsPullUp(flags) (((flags) & ePullUp) != 0)
#define NetIsPullDown(flags) (((flags) & ePullDown) != 0)
#define NetIsPrimaryZ(flags) (((flags) & ePrimaryZ) != 0)
#define NetIsInClkPath(flags) (((flags) & eInClkPath) != 0)
#define NetIsInDataPath(flags) (((flags) & eInDataPath) != 0)
#define NetIsEdgeTrigger(flags) (((flags) & eEdgeTrigger) != 0)
#define NetIsTemp(flags) (((flags) & eTempNet) != 0)
#define NetIsForcible(flags) (((flags) & eForcibleNet) != 0)
#define NetIsDeposit(flags) (((flags) & eDepositNet) != 0)
#define NetIsRecordPort(flags) (((flags) & eRecordPort) != 0)
#define NetIsRead(flags) (((flags) & eReadNet) != 0)
#define NetIsWritten(flags) (((flags) & eWrittenNet) != 0)
#define NetIsConstZ(flags) (((flags) & eConstZ) != 0)
#define NetIsDead(flags) (((flags) & eDeadNet) != 0)
#define NetIsReset(flags) (((flags) & eReset) != 0)
#define NetIsSigned(flags) (((flags) & eSigned) != 0)
#define NetIsClearAtEnd(flags) (((flags) & eClearAtEnd) != 0)

//! NUVectorNet flags
enum VectorNetFlags {
  eNoneVectorNet   = 0x00000000,
  eSignedSubrange  = 0x00000001, //!< Net is declared as a signed subrange of integer
  eUnsignedSubrange= 0x00000002, //!< Net is declared as a positive subrange of integer
  eVhdlLineNet     = 0x00000004  //!< Net is VHDL line buffer
};


//! Net strengths
/*!
 * These are ordered weak to strong strength.
 */
enum Strength {
  eStrPull,                   //!< Pull up/down strength
  eStrDrive,                  //!< Default net drive strength
  eStrTie,                    //!< Tie strength that overrides other drivers
  eStrWeakest = eStrPull,     //!< Weakest possible strength
  eStrStrongest = eStrTie   //!< Strongest possible strength
};

//! Flags to indicate how tristates and net initialization works.
/*!
 * Note that wand, wor, triand, trior, tri0, tri1, initialization is not affected
 * by these flags.
 */
enum TristateModeT
{
  eTristateModeZ = 0,    //!< X=1, Z=0, trireg has initialization, conflict-detecting
  eTristateModeX = 1,    //!< X=don't care, Z=don't care
  eTristateMode0 = 2,    //!< X=don't care, Z=0
  eTristateMode1 = 3     //!< X=don't care, Z=1
};

//! Port direction
enum PortDirectionT { eInput, eOutput, eBid };

//! Enumeration for all supported target compilers
/*!
  The order of these is somewhat important because variables of this
  type can be written to the database. eGCC3 must be first and eNoCC
  must be last.
*/
enum TargetCompiler {
  eGCC3,                                //<! GCC 3.X.X
  eICC,                                 //<! Intel CC 7.0 and up
  eSPW,                                 //<! SparcWorks CC
  eWINX,                                //<! GCC Windows cross target
  eGCC4,                                //<! GCC 4.0 and up
  eWIN,                                 //<! GCC Windows native target
  eNoCC                                 //<! Unknown Compiler Target
};

//! Enumeration for different versions of compilers
/*! If you add new compiler version then, at least, the src/codegen/Make-foo.inc
 *  will need to be changed and a version string will be needed in
 * CodeGen::createCompileConfigFiles.
 */
enum TargetCompilerVersion {
  // For compilers that are not further versions use vanilla flavour
  eVanilla_Compiler,                    //!< default version of selected compiler
  // GCC3 version
  eGCC_343,                             //!< gcc 3.4.3
  eGCC_345,                             //!< gcc 3.4.5
  eGCC_346,                             //!< gcc 3.4.6
  // GCC4 version
  eGCC_424,                             //!< gcc 4.2.4
  eGCC_432,                             //!< gcc 4.3.2
  eGCC_422                              //!< gcc 4.2.2
};

enum Carbon4StateVal {
  eValue0,
  eValue1,
  eValueX,
  eValueZ
};

//! Enumeration to record hierarhical component specific information
/*!
 *  These flags are used to specify hierarchical specific attributes. The
 *  current list is:
 *
 *  - The type of hierarchy. One of: module, tf, or declaration.
 *
 *  - The source language for the component. Each hierarchical
 *    component for the design comes from one specific language
 *    component. This enum is used to record the langauge.
 */
enum HierFlags {
  eNoFlag         = 0x00000000,
  eHierTypeMask   = 0x0000000F,    //!< Mask for the hierarchy type
  eHTModule       = 0x00000001,    //!< Hierarchy is a module
  eHTTaskFunc     = 0x00000002,    //!< Hierarchy is a task or function
  eHTDecl         = 0x00000003,    //!< Hierarchy is a declaration scope
  eHTBlock        = 0x00000004,    //!< Unamed block scope
  eHTRecord       = 0x00000005,    //!< Hierarchy is a vhdl record
  eHTNamedBlock   = 0x00000006,    //!< Hierarchy is a named block
  eHTGenerBlock   = 0x00000007,    //!< Hierarchy is a generate block

  eLanguageMask   = 0x00000070,    //!< Mask to get the language
  eSLUnknown      = 0x00000000,    //!< Unknown source language
  eSLVerilog      = 0x00000010,    //!< Source language is Verilog
  eSLVHDL         = 0x00000020,    //!< Source language is VHDL
};

//! Enumeration for the Carbon Model (VHM) type
/*!
  Used for generation of specialized vhms.
*/
enum CarbonVHMTypeFlags {
  eVHMNormal          = 0x0,       //!< Normal
  eVHMReplay          = 0x1,       //!< Replayable
  eVHMOnDemand        = 0x2,       //!< onDemand
  eVHMAllTypes        = 0x3        //!< Mask of all type flags     
};

#if pfGCC_2
#define and     &&         
#define and_eq  &=         
#define bitand  &          
#define bitor   |          
#define compl   ~          
#define not     !          
#define not_eq  !=         
#define or      ||         
#define or_eq   |=         
#define xor     ^          
#define xor_eq  ^=         

#define FORWARD_ITERATOR(T) forward_iterator<forward_iterator_tag, T>
#define RANDOM_ACCESS_ITERATOR(T) random_access_iterator<random_access_iterator_tag, T>

#else

#define FORWARD_ITERATOR(T) std::iterator<std::forward_iterator_tag, T>
#define RANDOM_ACCESS_ITERATOR(T) std::iterator<std::random_access_iterator_tag, T>

#endif

// Everyone needs to see the macro that defines the operator new/delete overrides
// and that lives here:
//#include "util/c_memmanager.h"


#define CARBON_FORBID_DEFAULT_CTORS(className) \
  private: \
    className(const className&); \
    className& operator=(const className&)


#define DLLIMPORT
#define DLLEXPORT

#if pfGCC
#  if pfWINDOWS
#    undef DLLIMPORT
#    undef DLLEXPORT
#    define DLLIMPORT __attribute__((dllimport))
#    define DLLEXPORT __attribute__((dllexport))
#  endif
#endif

#if pfMSVC
#  undef DLLIMPORT
#  undef DLLEXPORT
#  define DLLIMPORT __declspec( dllimport )
#  define DLLEXPORT __declspec( dllexport )
#endif

/*! \brief Unsigned integer same size as pointer */
typedef CarbonUIntPtr UIntPtr;
/*! \brief Signed integer same size as pointer */
typedef CarbonSIntPtr SIntPtr;

// In windows, there appear to be #defines for min & max, and so we
// get syntax errors if we try to use std::min or std::max
#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif


#endif // __CarbonTypes_h_
