// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __UtWildcard_h_
#define __UtWildcard_h_


#include "util/UtStringArray.h"

//! UtWildcard class -- simple * and ? matching.  Not full regexps.
class UtWildcard
{
public:
  CARBONMEM_OVERRIDES
  //! empty constructor

  UtWildcard();

  //! construct a wildcard expresssion
  /*!
   *! *    matches 0 or more characters
   *! ?    matches exactly one character
   *!
   *! If honour_escaped_identifiers and wildcardExpr is a Verilog escaped
   *! identifier then no wildcarding is done.
   *!
   *! If wildcardMatchesDot is true, '.' is not treated as a special
   *! character.  If it is false, wildcard characters won't match '.'.
   *! In other words, when wildcardMatchesDot is true (the default), the
   *! wildcard characters '*' and '?' match any character.  When it is
   *! false, they match any character except '.'.
   *!
   *! This is useful for handling wildcards in the same way as cbuild
   *! directives.  If wildcardMatchesDot is true, "top.*" will match both
   *! "top.x" and "top.sub.x".  That's not how wildcards work in directives.
   *! However, if wildcardMatchesDot is false, "top.*" matches "top.x" but
   *! not "top.sub.x", which is the same as is done with directives.
   */
  UtWildcard(const char* wildcardExpr, const bool honour_escaped_identifiers = false,
             bool wildcardMatchesDot = true);

  //! destructor
  ~UtWildcard();

  //! change the wildcard expression to a new string
  void putExpr(const char* wildcardExpr, const bool honour_escaped_identifiers = false,
               bool wildcardMatchesDot = true);

  //! see if this string matches the wildcard expression
  bool isMatch(const char* str) const;

  //! bool does this wildcard have a pattern or not?
  bool empty() const {return mTokens.empty();}

  //! assign a wildcard
  UtWildcard& operator=(const UtWildcard& src);
    
  //! copy-construct a wildcard
  UtWildcard(const UtWildcard& src);

private:
  bool matchHelper(size_t i, const char* str) const;

  UtStringArray mTokens;
  bool mWildcardMatchesDot;
};

#endif // __UtWildcard_h_
