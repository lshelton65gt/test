/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef __XMLPARSING_H__
#define __XMLPARSING_H__

#include "util/CarbonPlatform.h"

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

#include "util/UtString.h"

// Helper class for XML Parsing


class UtXmlErrorHandler
{
public:
  UtXmlErrorHandler()
  {
    mErrorText = "";
    mErrorReported = false;
    mWarningReported = false;
  }

  void reportError(const char *message)
  {
    mErrorText << message << "\n";
    mErrorReported = true;
  }
  bool getError() const {return mErrorReported;}
  const char *errorText()
  {
    return mErrorText.c_str();
  }

  void reportWarning(const char *message)
  {
    mWarningText << message << "\n";
    mWarningReported = true;
  }
  bool getWarning() const {return mWarningReported;}
  const char *warningText()
  {
    return mWarningText.c_str();
  }

private:
  bool mWarningReported;
  UtString mWarningText;

  bool mErrorReported;
  UtString mErrorText;
};



class XmlParsing
{
public:

  static bool hasProp(xmlNode* node, const char* propName)
  {
    if (node != NULL)
    {
      xmlAttrPtr att = node->properties;
      while (att)
      {
        if (0 == strcmp(reinterpret_cast<const char *>(att->name), propName))
          return true;
        att = att->next;
      }
    }
    return false;
  }

  static bool getProp(xmlNode* node, const char* propName, UtString* content)
  {
    *content = "";
    xmlChar* text = xmlGetProp(node, BAD_CAST propName);
    if (text != 0) {
      *content = reinterpret_cast<const char *>(text);
      xmlFree(text);
      return true;
    }
    return false;
  }

  static void getContent(xmlNode *node, UtString *content)
  {
    *content = "";
    xmlChar *text = xmlNodeGetContent(node);
    if (text != 0) {
      *content = reinterpret_cast<const char *>(text);
      xmlFree(text);
    }
  }

  static bool isElement(xmlNode *node, const char *name)
  {
    if (node == NULL || node->type != XML_ELEMENT_NODE) {
      return false;
    }

    return strcmp((const char *) node->name, name) == 0;
  }

  static bool isTextNode(xmlNode *node)
  {
    if (node == NULL || node->type != XML_TEXT_NODE) {
      return false;
    }
    return true;
  }
 
  static bool isCommentNode(xmlNode *node)
  {
    if (node == NULL || node->type != XML_COMMENT_NODE) {
      return false;
    }
    return true;
  }

  static const char* elementName(xmlNode* node)
  {
    if (node == NULL || node->type != XML_ELEMENT_NODE)
    {
      return NULL;
    }
    return (const char*)node->name;
  }
};


#endif
