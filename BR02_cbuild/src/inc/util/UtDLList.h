// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef __UtDLList_h_
#define __UtDLList_h_

#include <cstddef>
#include "util/c_memmanager.h"
#include "util/StlIterator.h"

class UtDLListNodeBase {
 public: CARBONMEM_OVERRIDES
  UtDLListNodeBase();
  static void swap(UtDLListNodeBase *x, UtDLListNodeBase *y);
  void transfer(UtDLListNodeBase *first, UtDLListNodeBase *last);
  void hook(UtDLListNodeBase * const pos);
  void unhook();

  UtDLListNodeBase *mPrev;
  UtDLListNodeBase *mNext;
};

template<typename T>
class UtDLListNode : public UtDLListNodeBase {
public: CARBONMEM_OVERRIDES
  T mData;
};

template<typename T>
class UtDLListIterator {
public: CARBONMEM_OVERRIDES
  
  typedef UtDLListNode<T>            NodeType;
  typedef UtDLListIterator<T>        SelfType;
  
  typedef ptrdiff_t                  difference_type;
  //typedef bidirectional_iterator_tag iterator_category;   
  typedef T                          value_type;
  typedef T &                        reference;
  typedef T *                        pointer;

  // Constructors
  UtDLListIterator() {
  }

  UtDLListIterator(UtDLListNodeBase *node) : mNode(node) {
  }

  reference operator *() const 
  { return static_cast<NodeType *>(mNode)->mData; }

  pointer operator ->() const 
  { return &static_cast<NodeType *>(mNode)->mData; }
  
  SelfType & operator++() {
    mNode = mNode->mNext;
    return *this;
  }

  SelfType operator++(int) {
    SelfType tmp = *this;
    mNode = mNode->mNext;
    return tmp;
  }

  SelfType & operator--() {
    mNode = mNode->mPrev;
    return *this;
  }

  SelfType operator--(int) {
    SelfType tmp = *this;
    mNode = mNode->mPrev;
    return tmp;
  }
  
  bool operator==(const SelfType &other) const {
    return mNode == other.mNode;
  }
  bool operator!=(const SelfType &other) const {
    return mNode != other.mNode;
  }

  UtDLListNodeBase *mNode;
};

template<typename T>
class UtDLListConstIterator {
public: CARBONMEM_OVERRIDES

  typedef const UtDLListNode<T>          NodeType;
  typedef UtDLListConstIterator<T>       SelfType;
  typedef UtDLListIterator<T>            iterator;

  typedef ptrdiff_t                      difference_type;
  //typedef bidirectional_iterator_tag     iterator_category;   
  typedef T                              value_type;
  typedef const T &                      reference;
  typedef const T *                      pointer;

  // Constructors
  UtDLListConstIterator() {
  }

  UtDLListConstIterator(const UtDLListNodeBase *node) : mNode(node) {
  }

  UtDLListConstIterator(const iterator &other) : mNode(other.mNode) {
  }

  reference operator *() const 
  { return static_cast<NodeType *>(mNode)->mData; }

  pointer operator ->() const 
  { return &static_cast<NodeType *>(mNode)->mData; }
  
  SelfType & operator++() {
    mNode = mNode->mNext;
    return *this;
  }

  SelfType operator++(int) {
    SelfType tmp = *this;
    mNode = mNode->mNext;
    return tmp;
  }

  SelfType & operator--() {
    mNode = mNode->mPrev;
    return *this;
  }

  SelfType operator--(int) {
    SelfType tmp = *this;
    mNode = mNode->mPrev;
    return tmp;
  }
  
  bool operator==(const SelfType &other) {
    return mNode == other.mNode;
  }
  bool operator!=(const SelfType &other) {
    return mNode != other.mNode;
  }
  
  const UtDLListNodeBase *mNode;

};

template<typename _Val>
inline bool operator==(const UtDLListIterator<_Val> &x, const UtDLListConstIterator<_Val> &y){
  return x.mNode == y.mNode;
}

template<typename _Val>
inline bool operator!=(const UtDLListIterator<_Val> &x, const UtDLListConstIterator<_Val> &y){
  return x.mNode != y.mNode;
}

template<typename T>
class UtDLList {
public: CARBONMEM_OVERRIDES
  
  typedef UtDLListIterator<T>      iterator;
  typedef UtDLListConstIterator<T> const_iterator;
  
  // Default Constructor
  UtDLList() {
    _init();
  }
  
  // Destructor
  ~UtDLList() {
    clear();
  }

  //! Fills List with \a n copies of value \a val
  UtDLList(size_t n, const T & val) {
    _init();
    insert(begin(), n, val);
  }
  
  //! Copy Constructor
  //! Copies All element of the original list to this list
  UtDLList(const UtDLList &orig) {
    _init();
    insert(begin(), orig.begin(), orig.end());
  }
  
  //! Constructs a UtDLList from a range
  template<typename IterType>
  UtDLList(IterType from, IterType to) {
    insert(begin(), from, to);
  }

  //! Assignment Operator
  UtDLList &operator=(const UtDLList &other) {
    clear();
    insert(begin(), other.begin(), other.end());
    return *this;
  }

  iterator begin() {
    return iterator(mNode.mNext);
  }

  const_iterator begin() const {
    return const_iterator(mNode.mNext);
  }

  iterator end() {
    return iterator(&mNode);
  }

  const_iterator end() const {
    return const_iterator(&mNode);
  }

  bool empty() const {
    return &mNode == mNode.mNext;
  }

  size_t size() const {
    return _distance(begin(), end());
  }

  T & front() {
    return *begin();
  }

  const T & front() const {
    return *begin();
  }

  T & back() {
    return *(--end());
  }

  const T & back() const {
    return *(--end());
  }

  void push_front(const T & val) {
    insert(begin(), val);
  }

  void pop_front() {
    erase(begin());
  }

  void push_back(const T & val) {
    insert(end(), val);
  }

  void pop_back() {
    erase(mNode.mPrev);
  }

  iterator insert(iterator pos, const T & val) {
    UtDLListNode<T> * node = new UtDLListNode<T>;
    node->mData = val;
    node->hook(pos.mNode);

    return iterator(node);
  }

  template<typename _InputIter>
  void insert(iterator pos, _InputIter from, _InputIter to) {
    for(; from != to; ++from)
      insert(pos, *from);
  }

  iterator erase(iterator pos) {
    iterator new_pos(pos.mNode->mNext);
    pos.mNode->unhook();
    delete pos.mNode;
    return new_pos;
  }

  iterator erase(iterator from, iterator to) {
    while(from != to) from = erase(from);
    return to;
  }
  
  void clear() {
    erase(begin(), end());
  }

  void splice(iterator pos, UtDLList &orig) {
    if(!orig.empty()) {
      pos.mNode->transfer(orig.begin().mNode, orig.end().mNode->mPrev);
    }
  }

  void splice(iterator pos, UtDLList &orig, iterator orig_pos) {
    iterator iter = orig_pos;
    ++orig_pos;
    if(pos != orig_pos || pos == iter)
      return;
    pos.mNode->transfer(orig_pos, iter);
  }

  void splice(iterator pos, UtDLList &, iterator first, iterator last) {
    if(first != last) {
      pos.mNode->transfer(first.mNode, last.mNode->mPrev);
    }
  }

protected:
  UtDLListNode<T> mNode;
  
  void _init() {
    mNode.mPrev  = &mNode;
    mNode.mNext  = &mNode;
  }

  ptrdiff_t _distance(iterator first, iterator last) const {
    ptrdiff_t d = 0;
    while(first++ != last) d++;
    return d;
  }

  ptrdiff_t _distance(const_iterator first, const_iterator last) const {
    ptrdiff_t d = 0;
    while(first++ != last) d++;
    return d;
  }

};

#endif
