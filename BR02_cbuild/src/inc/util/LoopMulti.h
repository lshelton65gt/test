// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _LOOPMULTI_H_
#define _LOOPMULTI_H_


#include "util/StlIterator.h"

//! LoopMulti template class
/*!
  This class provides a mechanism for a class to expose iteration over
  the elements yielded by multiple homogeneous sub-loops.  If you need
  heterogeneous loops then you must wrap them all with Iter<value_type>.
  Or if there are a small std::fixednumber of them, then you can use LoopPair,
  which templatizes the two looping classes.

  With this looping construct, you create an empty LoopMulti<Looper>
  instance, and then call pushLoop(Looper) for every sub-loop that you need.

  This class keeps all the pushed loops in a container.  Another option
  is to use a LoopThunk, which has the same functionality as a LoopMulti,
  but does not maintain a container, instead getting the next sub-loop
  on demand.
*/

template <class _LoopType>
class LoopMulti
{
public: CARBONMEM_OVERRIDES
  typedef typename _LoopType::value_type value_type;
  typedef typename _LoopType::reference reference;

  LoopMulti()
  {
    mIndex = 0;
  }

  //! Copy constructor to help the user manage & pass around Loops.
  LoopMulti(const LoopMulti& src): mLoops(src.mLoops), mIndex(src.mIndex)
  {
  }

  //! Assignment operator to help the user manage & pass around Loops.
  LoopMulti& operator=(const LoopMulti& src)
  {
    if (&src != this)
    {
      mLoops = src.mLoops;
      mIndex = src.mIndex;
    }
    return *this;
  }

  //! Append operator to help user merge Loops.
  void pushLoopMulti(const LoopMulti& src)
  {
    if (&src != this) {
      size_t vecSize = src.mLoops.size();
      for (unsigned int i = 0; i < vecSize; ++i) { 
        pushLoop(src.mLoops[i]);
      }
    }
  }

  //! Add another sub-loop to the LoopMulti
  void pushLoop(_LoopType loop)
  {
    bool isFirst = mLoops.empty();
    mLoops.push_back(loop);
    if (isFirst)
      mIndex = 0;
    nextNode();
  }
  
  void nextNode()
  {
    while ((mIndex != mLoops.size()) && mLoops[mIndex].atEnd())
      ++mIndex;
  }

  void operator++()
  {
    INFO_ASSERT(mIndex != mLoops.size(), "Array index out-of-bounds.");
    _LoopType& loop = mLoops[mIndex];
    ++loop;
    nextNode();
  }

  reference operator*() const
  {
    INFO_ASSERT(mIndex != mLoops.size(), "Array index out-of-bounds..");
    return *mLoops[mIndex];
  }

  bool atEnd() const
  {
    return mIndex == mLoops.size();
  }

  bool operator()(value_type* ptr)
  {
    if (atEnd())
      return false;
    *ptr = *mLoops[mIndex];
    ++(*this);
    return true;
  }

  typedef StlIterator<LoopMulti<_LoopType> > iterator;

  //! The start of the loop.
  iterator begin()
  {
    return iterator(this);
  }

  //! The tail of the loop
  iterator end()
  {
    return iterator(NULL);
  }

  //! Return the number of elements remaining -- note:  order N operation
  size_t size() const
  {
    size_t s = 0;
    for (LoopMulti<_LoopType> tmp = *this; !tmp.atEnd(); ++tmp)
      ++s;
    return s;
  }

private:
  UtVector<_LoopType> mLoops;
  unsigned int mIndex;
};

#endif //  _LOOPMULTI_H_
