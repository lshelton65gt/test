// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  Generalized arithmetic class for arbitrary-length bit values.

  DynBitVector is based on BitVector.h, but modified to support dynamic resizing
  of the vector.
*/

/*
 * Copyright (c) 1998
 * Silicon Graphics Computer Systems, Inc.
 *
 * Copyright (c) 1999 
 * Boris Fomitchev
 *
 * This material is provided "as is", with absolutely no warranty expressed
 * or implied. Any use is at your own risk.
 *
 * Permission to use or copy this software for any purpose is hereby granted 
 * without fee, provided the above notices are retained on all copies.
 * Permission to modify the code and to distribute modified code is granted,
 * provided the above notices are retained, and a notice that the code was
 * modified is included with the above copyright notice.
 *
 */

#ifndef __DynBitVector_h_
#define __DynBitVector_h_


/*
 * A BitVector of size N, using words of type _WordT, will have 
 * N % (sizeof(_WordT) * CHAR_BIT) unused bits.  (They are the high-
 * order bits in the highest word.)  It is a class invariant
 * of class BitVector<> that those unused bits are always zero.
 *
 * Most of the actual code isn't contained in BitVector<> itself, but in the 
 * base class _Base_BitVector.  The base class works with whole words, not with
 * individual bits.  This allows us to specialize _Base_BitVector for the
 * important special case where the BitVector is only a single word.
 *
 * The C++ standard does not define the precise semantics of operator[].
 * In this implementation the const version of operator[] is equivalent
 * to test(), except that it does no range checking.  The non-const version
 * returns a reference to a bit, again without doing any range checking.
*/
#ifndef __UtString_h_
#include "util/UtString.h"
#endif
#ifndef CONSTANTRANGE_H_
#include "util/ConstantRange.h"
#endif
#ifndef _CPP_CSTDDEF
#include <cstddef>
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef __CarbonAssert_h_
#include "util/CarbonAssert.h"
#endif

class ZostreamDB;
class ZistreamDB;
class UtOStream;

#define JDM_NOTYET 0
#define JDM_NOTCOVERED 0

//! Compute bit width of an unsigned long.
#define __BITS_PER_WORDT	 (CHAR_BIT*sizeof(UInt32))

//! Convert bit width to equivalent number of unsigned longs
#define __BITVECTOR_WORDS(__n) \
 ((__n) < 1 ? 1 : ((__n) + __BITS_PER_WORDT - 1)/__BITS_PER_WORDT)

// We need to be able to compute numWords by doing (numBits+31)/32 so
// to avoid wrapping, use this constraint
#define DYNBITVECTOR_MAX_NUM_BITS (0xffffffff - 31)


class DynBitVector;

//! Generic DynBitVector factory, used for db writing
class DynBitVectorFactory
{
public: CARBONMEM_OVERRIDES

  //! Constructor
  /*! required are sizeUniquifies specifies wither 001 == 01.
   *! if sizeUniquifies==true, then it does *not*.  Currently,
   *! netref's share bitvectors that have different amounts of
   *! padding
   */
  DynBitVectorFactory(bool sizeUniquifies);

  //! Destructor
  ~DynBitVectorFactory();
  //! Find an already existing value in the factory
  /*!
    Returns NULL if a corresponding value is not found
  */
  const DynBitVector* find(const DynBitVector& value);
  //! Inserts a value into the factory
  void insert(const DynBitVector& value);
  //! Inserts a value or retrieves a value from the factory
  const DynBitVector* alloc(const DynBitVector& value);
  
  //! write the factory to the db
  bool dbWrite(ZostreamDB& out) const;

  //! Read the factory from the db
  bool dbRead(ZistreamDB& out);

  //! Debug function. Dumps size of factory
  void printStats(int indent_arg=0);

  //! are there any entries in the factory?
  bool empty() const;

private:
  class HashBV;
  typedef UtHashSet<DynBitVector*, HashBV, HashMgr<DynBitVector*>,
                    UtHashBigWrapper<DynBitVector*,HashBV> > BVHash;
  BVHash* mBVs;

  // forbid
  DynBitVectorFactory(const DynBitVectorFactory&);
  DynBitVectorFactory& operator=(const DynBitVectorFactory&);
};

// ------------------------------------------------------------
//! Class DynBitVector - implement extended precision support.
//!  _Nb may be any nonzero number of bits.
//
class DynBitVector {
public:
  CARBONMEM_OVERRIDES

  //! Proxy representation for bits-within-BitVector object
  struct reference
  {
    //! Address of word within a BitVector
    mutable UInt32 *mWords;
    
    //! Starting bit offset
    mutable size_t _M_bpos;
    mutable size_t mNumBits;
    //! Total size of bitfield selected.
    mutable size_t _M_bsiz;

    // should be left undefined
    //reference() {}

    // Constructor from a bitvector
    reference( const DynBitVector& __B, size_t __pos, size_t __siz=1 );

    //! Constructor from simple array of UInt32
    /*
     * In this case we assume that the pos/size are within the actual
     * memory, as we have no other guarantees.
     */
    reference (UInt32 *wp, size_t pos, size_t nb, size_t siz=1);

    //! Constructor with different size
    reference (const reference& r, size_t size);
      
    // Copy Constructor
    reference (const reference& r);

    // Default destructor
    ~reference();

    //! Deposit masked
    void deposit (UInt32 value, size_t pos, size_t siz);

    //! Any bits in the range non-zero?
    bool any (void) const;

    //! All bits in the range ones?
    bool all() const;

    //! returns the number of bits set to 1 in the current range
    size_t count (void) const;

    size_t redxor(void) const;

    //! Move bitfields.  Must handle unaligned and overlapping
    // fields.
    void anytoany(const DynBitVector::reference *src);

    //! for:	 b[i] = __x;
    //!  or:	 b[i][siz] = __x;
    /*!
     * \em Warning: assignment doesn't change the reference members
     */
    reference& operator=(UInt32 __x);
    
    //!  b[i][size] = b[__j][size];
    reference& operator=(const reference& __j);

    //! b[i][j] = bitUtVector<K>;
    //
    // Note that K should be equal to the field size
    // 
    reference& operator=(const DynBitVector& __j);

    //! b[pos][size]
    reference& operator[](size_t __siz);

    //! b[pos][size]
    const reference& operator[](size_t __siz) const;

    // Need a whole bunch of other operators to do anything
    // complex with references beyond just copying them around.
    //
    // This is going to be a LOT of work to support, as we need
    //   reference <OP> (reference|BitVector|int)
    //
    // Probably the right thing is to generate BitVectors  for the inputs
    // and do a BitVector<K>::operator+= with a scratch temporary.
    // This is far from optimal for something like:
    //		reg[31:16] += 1;
    // where we could translate to
    //		BitVector<reg.size()> temp(1<<16);
    //		reg += temp;


    reference& operator+=(UInt32 rval);
    reference& operator-=(UInt32 rval);
    reference& operator|=(UInt32 rval);
    reference& operator^=(UInt32 rval);
    reference& operator&=(UInt32 rval);
    bool operator!() const;
    bool operator~() const;
    operator UInt64 () const;
    UInt32 value () const;
    UInt64 llvalue () const;
    reference& flip();
    bool operator==(const reference& r) const;
    reference& lpartsel (size_t size);
    const reference& partsel (size_t size);
  }; // struct reference

  static inline void sEqualSizeDynBVCopy(DynBitVector* dest, const DynBitVector& src)
  {
    UInt32 numWords = src.getUIntArraySize();
    const UInt32* srcArr = src.getUIntArray();
    UInt32* destArr = dest->getUIntArray();
    
    memcpy(destArr, srcArr, numWords * sizeof(UInt32));
  }
  
  //! Number of 32-bit words needed to represent the data
  UInt32 numWords() const {return mNumBits? (mNumBits + 31)/ 32 : 1;}

  //! Write a BV to a database
  bool dbWrite(ZostreamDB&) const;

  //! Read a BV from a database
  bool dbRead(ZistreamDB&);

  //! format the bit-vector as an ascii string
  void format(UtString* buf, CarbonRadix radix) const;

  //! format the bit-vector as an ascii string
  /*! This form is usable even if you don't have a DynBitVector
    ! already constructed.  This can be called for a BitVector<>,
    ! without reallocating the data storage
  */
  static void format(UtString* buf, CarbonRadix radix,
                     const UInt32* words, UInt32 numBits);

  //! Count the ones in the a 32-bit word passed as argument \a x.
  static size_t popcount(unsigned int x);
  
  //! Parity of a 32-bit word.
  static size_t redxor_word(unsigned int x);

  friend struct reference;
  friend class BVPairIter;
  friend class ConstBVPairIter;

  // Special checked reference construction
  reference lpartsel(size_t pos, size_t size);
  const reference partsel(size_t pos, size_t size) const;

  //! default constructor
  DynBitVector();

  //! construct to a size
  explicit DynBitVector(size_t size);

  //! Constructor for 64-bit initializer
  explicit DynBitVector (size_t size, UInt64 __val);

  //! Constructor given a different sized BitVector&
  DynBitVector(size_t size, const DynBitVector& v);

  // Copy constructor
  DynBitVector(const DynBitVector& v);

  //! Constructor with an array of longs as initializers.
  DynBitVector (size_t numBits, const UInt32 array[], size_t numWords);

#if JDM_NOTYET
  //! Constructor with a reference to a bitvector as initializer
  //
  DynBitVector (const reference& v);
#endif

  //! Destructor
  ~DynBitVector();

  UInt64 llvalue() const;
  UInt32 value() const;
  CarbonReal realvalue () const;

  bool operator!(void) { return not this->any ();}

  //! set the value to the specified constant, retaining existing size
  DynBitVector& operator=(UInt64 rhs);

  //! set the value to a slice of another DBV, retaining existing size
  DynBitVector& operator=(const reference& rhs);

  //! copy the value and size from another DBV
  /*! It is important that operator= and the copy constructor work
   *! consistently, and the copy-constructor must work like this since
   *! it has no notion of an existing size.  But if you want to retain
   *! the existing size and copy the data, truncating if necessary,
   *! use the copy method.
   */
  DynBitVector& operator=(const DynBitVector& rhs);

  //! copy the value from another DBV, retaining existing size, truncating if necessary
  void copy(const DynBitVector& rhs);

  //! copy the value from a UInt64, retaining existing size, truncating if necessary
  void copy(UInt64 rhs);

  //! copy the value from CarbonReal, retaining existing size, truncating if necessary
  void copyreal(const CarbonReal rhs);

  // 23.3.5.2 BitVector operations:

  DynBitVector& operator+=(const DynBitVector& __rhs);
  DynBitVector& operator+=(unsigned int rvalue);
  DynBitVector& operator-=(const DynBitVector& __rhs);
  DynBitVector& operator-=(unsigned int rvalue);
  DynBitVector& operator/=(size_t __rhs);
  DynBitVector& operator*=(size_t __rhs);
  DynBitVector& operator&=(const DynBitVector& __rhs);
  DynBitVector& operator|=(const DynBitVector& __rhs);
  DynBitVector& operator^=(const DynBitVector& __rhs);

#if JDM_NOTYET
  DynBitVector& operator<<=(const DynBitVector&__rhs);
  DynBitVector& operator>>=(const DynBitVector&__rhs);
#endif

  // With a UInt32
  DynBitVector& operator&=(UInt32 __rhs);
  DynBitVector& operator|=(UInt32 __rhs);
  DynBitVector& operator^=(UInt32 __rhs);
  DynBitVector& operator<<=(size_t __pos);
  DynBitVector& operator>>=(size_t __pos);  

  // With a UInt64
  DynBitVector& operator&=(UInt64 __rhs);
  DynBitVector& operator|=(UInt64 __rhs);
  DynBitVector& operator^=(UInt64 __rhs);

  //
  // Extension:
  // Versions of single-bit set, reset, flip, test
  //
  DynBitVector& set(size_t __pos);
  DynBitVector& set(size_t __pos, int __val);
  DynBitVector& reset(size_t __pos);
  DynBitVector& reset();
  DynBitVector& flip(size_t __pos);
  bool test(size_t __pos) const;
  void negate();

  SInt32 getSignExtendedHighWord() const;

  //! Integer division: *remainder = *this % divider, *this /= divider;
  void integerDiv(const DynBitVector& divider, DynBitVector* remainder);

  //! Integer multiplication: *this *= multiplier
  void integerMult(const DynBitVector& muliplier);
  
  //! Fast multiply by 10.  *this *= 10
  /*!
   *! This is used by the decimal printing code so it needs to be fast
   */
  void multBy10();

  //! perform signed multiply on two operands, result in this
  void signedMultiply(const DynBitVector& lhs, const DynBitVector& rhs);

  //! perform unsigned multiply on two operands, result in this
  void multiply(const DynBitVector& lhs, const DynBitVector& rhs);

  // Set, reset, and flip.

  DynBitVector& set();
  DynBitVector& flip();
  DynBitVector operator~() const;
  DynBitVector operator-() const;

  //! Sets a range starting a pos with fillVal (0 or 1).
  void setRange(size_t pos, size_t num, int fillVal);

  //! element access:
  //for b[i];
  reference operator[](size_t __pos);
  
  //! element access:
  //for b[i];
  const reference operator[](size_t __pos) const;
  
//  bool test(size_t __pos) const;

  UInt32 to_ulong() const;

  //! returns the number of bits set to 1 in the current vector
  size_t count (void) const;
  size_t redxor(void) const;

  size_t size() const;

  void resize (size_t size);

  //! compare the values (zero-extending smaller DBV)
  bool operator==(const DynBitVector& __rhs) const;

  //! real implementation of less-than
  bool operator<(const DynBitVector& __rhs) const;

  bool operator<(UInt64 rhs) const {
    DynBitVector bv(64, rhs);
    return *this < bv;
  }
  bool operator>(UInt64 rhs) const {
    DynBitVector bv(64, rhs);
    return *this > bv;
  }
  bool operator==(UInt64 rhs) const {
    DynBitVector bv(64, rhs);
    return *this == bv;
  }
  bool operator<(SInt32 rhs) const {
    DynBitVector bv(64, rhs);
    return *this < bv;
  }
  bool operator>(SInt32 rhs) const {
    DynBitVector bv(64, rhs);
    return *this > bv;
  }
  bool operator==(SInt32 rhs) const {
    DynBitVector bv(64, rhs);
    return *this == bv;
  }

  inline bool operator<=(const DynBitVector& x) const { return !(x < *this); }
  inline bool operator>=(const DynBitVector& x) const { return !(*this < x); }
  inline bool operator>(const DynBitVector& x)  const { return x < *this; }
  inline bool operator!=(const DynBitVector& x) const { return !(*this == x); }

  template<typename T>
  inline bool operator<=(T x) const { return !(*this > x); }
  template<typename T>
  inline bool operator>=(T x) const { return !(*this < x); }
  template<typename T>
  inline bool operator!=(T x) const { return !(*this == x); }

  template<typename T>
  friend bool operator<(T x, const DynBitVector& y) { return y > x; }
  template<typename T>
  friend bool operator>(T x, const DynBitVector& y) { return y < x; }
  template<typename T>
  friend bool operator!=(T x, const DynBitVector& y) { return !(y == x); }
  template<typename T>
  friend bool operator==(T x, const DynBitVector& y) { return y == x; }
  template<typename T>
  friend bool operator<=(T x, const DynBitVector& y) { return !(y < x); }
  template<typename T>
  friend bool operator>=(T x, const DynBitVector& y) { return !(y > x); }

# define ARITH_OP(op, eq_op, T) \
  DynBitVector operator op(T rop) const { \
    DynBitVector tmp(*this); \
    tmp eq_op rop; \
    return tmp; \
  }

  ARITH_OP(<<, <<=, size_t)
  ARITH_OP(>>, >>=, size_t)

  ARITH_OP(+, +=, size_t)
  ARITH_OP(-, -=, size_t)
  ARITH_OP(*, *=, size_t)
  ARITH_OP(/, /=, size_t)

  ARITH_OP(+, +=, const DynBitVector&)
  ARITH_OP(-, -=, const DynBitVector&)
  //ARITH_OP(*, *=, const DynBitVector&)
  //ARITH_OP(/, /=, const DynBitVector&)

  ARITH_OP(|, |=, const DynBitVector&)
  ARITH_OP(&, &=, const DynBitVector&)
  ARITH_OP(^, ^=, const DynBitVector&)
# undef ARITH_OP

  // relational operators between BitVector and UInt32
  bool operator==(UInt32 rhs) const;
  bool operator<(UInt32 rhs) const;
  bool operator>(UInt32 rhs) const;
  bool any() const;
  bool none() const;
  bool all() const;

  // Check for all ones in a range
  bool isRangeOnes (UInt32 pos, UInt32 size) const;
  bool isRangeZero (UInt32 pos, UInt32 size) const;

  //! return true if this and \a v have any bit set in the same bit-position
  bool anyCommonBitsSet(const DynBitVector &v) const;

  //! print value in hex to stdout
  void printHex() const;

  //! print value in binary to stdout
  void printBin() const;

  //! compose the indices of the bits that are set, separated by commas
  void composeBits(UtString*) const;

  //! Return the number of UInt32s used by the value array
  UInt32 getUIntArraySize() const;

  //! return read-only array of UInt32s
  /*!
   * For small vectors (32 bits or fewer), we store them directly in the pointer.
   * Anything larger, we use the mWordUnion to hold a pointer to an allocated array.
   */
  const UInt32* getUIntArray() const
  {
    return (mNumBits > 32) ? mWordUnion.mWords : &mWordUnion.mOneWord;
  }

  //! modifiable array of UInt32s
  UInt32* getUIntArray()
  {
    return (mNumBits > 32) ? mWordUnion.mWords : &mWordUnion.mOneWord;
  }

  int compare(const DynBitVector& __x) const;
  bool isLessThan(const DynBitVector& __x) const;
  bool isGreaterThan(const DynBitVector& __x) const;

  int signedCompare(const DynBitVector& __x) const;

  //! Return log2(x) for x an exact power of two
  static size_t sExactLog2 (size_t x);

  //! Return smallest k, such that 2**k >= x
  static size_t sLog2 (size_t x);
  size_t hash() const;

  //! Return the set bits as a contiguous range.  If the set bits
  //! are discontiguous, return false
  bool getContiguousRange(UInt32* startBit, UInt32* size) const;

  //! Return the index of the first one; 0 if none found.
  /*!
   * WARNING: If there is no one set, 0 is returned. This is ambiguous
   * with the situation where the 0th bit is set.
   */
  UInt32 findFirstOne() const;

  //! Return the index of the last one; maxbit if none found.
  /*!
   * WARNING: If there is no one set, maxbit is returned. This is
   * ambiguous with the situation where the last bit is set.
   */
  UInt32 findLastOne() const;

  class RangeLoop;

  //! transfer bits between two bitvectors arbitrarily
  static void anytoany(const UInt32* src, UInt32* dst,
                       UInt32 srcStart, UInt32 numBits, UInt32 dstStart);

  //! Sign-extend from bit N to bit size()-1
  void signExtend(UInt32 sign_bit_index);

private:
  inline bool _M_is_greater(const DynBitVector& __x) const;
  inline bool _M_is_less(const DynBitVector& __x) const;
  inline bool _M_is_equal(const DynBitVector& __x) const;
  inline bool _M_is_any() const;
  inline size_t _M_do_count() const;
  inline UInt32 _M_do_to_ulong() const;
  inline void _M_do_add(const DynBitVector& __x);
  inline void _M_do_add (UInt32 rval);
  inline void _M_do_sub (const DynBitVector& __x);
  inline void _M_do_sub (UInt32 rval);
  inline void _M_do_neg (void);
  inline void _M_do_mul (const size_t n);
  inline void _M_do_div (const size_t n);
  inline void _M_do_and(const DynBitVector& __x);
  inline void _M_do_or(const DynBitVector& __x);
  inline void _M_do_xor(const DynBitVector& __x);
  inline void _M_do_left_shift(size_t __shift);
  inline void _M_do_right_shift(size_t __shift);
  inline void _M_do_flip();
  inline void _M_do_set();
  inline void _M_do_reset();
  void initcheck (UInt32 size);

  size_t mNumBits;
  union {
    UInt32 mOneWord;            // mNumBits <= 32
    UInt32* mWords;             // mNumBits > 32, mWords[0] is LSBs
  } mWordUnion;


  //! Clear extra bits in BitVector
  void _M_do_sanitize();

  inline UInt32& _M_getword(size_t __pos);
  UInt32& _M_hiword();
  UInt32  _M_hiword() const;
  UInt32  _M_getword(size_t __pos) const;
#if JDM_NOTCOVERED
  UInt32  _M_hiword() const;
#endif

}; // class DynBitVector 

//! DynBitVector::RangeLoop iterator class
/*! Loops through the contiguous ranges in a DynBitVector.
 *! If onesAndZeros is false, then the iterator only yields
 *! ranges of ones, e.g.
 *!     0001111110000000
 *! would only [12:7], with getCurrentState() = true.
 *! Whereas if we initialize with onesAndZeros as true, then
 *! that bit pattern with yield three values:
 *!     [6:0] false
 *!     [12:7] true
 *!     [15:13] false
 */
class DynBitVector::RangeLoop {
public:
  //! ctor
  RangeLoop(const DynBitVector&, bool onesAndZeros);

  //! dtor
  ~RangeLoop();

  //! copy ctor
  RangeLoop(const RangeLoop&);

  //! assign op
  RangeLoop& operator=(const RangeLoop&);

  //! increment
  void operator++() {next();}

  //! gets current range
  const ConstantRange& operator*() {return mCurrentRange;}

  // are we at the end of the iteration?
  bool atEnd() const {
    return mCurrentRange.getLsb() > mCurrentRange.getMsb();
  }

  //! gets current state (does the range represents ones or zeros?)
  /*! If onesAndZeros was specified as false during construction, then
   *! this will always return true if we are not atEnd()
   */
  bool getCurrentState() {return mCurrentState;}

private:
  //! advance to the next range
  void next();

  DynBitVector mBitVector;
  ConstantRange mCurrentRange;
  UInt32 mNextBit;
  UInt32 mNextSize;
  bool mCurrentState;
  bool mOnesAndZeros;
}; // class RangeLoop


// ------------------------------------------------------------

//
// 23.3.5.3 BitVector operations:
//

//! Overload binary *
inline DynBitVector operator*(const DynBitVector& __x,
				size_t __y) {
  DynBitVector __result(__x);
  __result *= __y;
  return __result;
}

//! Likewise
inline DynBitVector operator*(const DynBitVector& __x, const
DynBitVector& __y) {
  if (__y <= UtUINT32_MAX)
    {
      DynBitVector __result = __x;
      return (__result *= __y.value ());
    }
  else
    {
      DynBitVector __result = __y;
      return (__result *= __x.value ());
    }
}

//! write to a stream
UtOStream& operator<<(UtOStream&, const DynBitVector&);

//! Compare function used for sets and maps
struct DynBitVectorCmp
{
  CARBONMEM_OVERRIDES

  bool operator()(const DynBitVector* b1, const DynBitVector* b2) const
  {
    return (*b1) < (*b2);
  }
};

#ifndef DynBitVector_KEEP_DEFS
#  undef __BITS_PER_WORDT
#  undef __BITVECTOR_WORDS
#  undef BitVector
#endif

#endif /* __DynBitVector_h_ */
