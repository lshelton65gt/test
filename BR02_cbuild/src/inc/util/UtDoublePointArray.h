// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "util/c_memmanager.h"
#include "util/CarbonTypes.h"

class UtDoublePointArray {
public:
  CARBONMEM_OVERRIDES

  //! ctor
  UtDoublePointArray();

  //! dtor
  ~UtDoublePointArray();

  //! add a new point to the array
  void push_back(double x, double y);

  //! clear the array
  void clear();

  //! remove a point from the array
  void pop_back();

  //! is this array empty
  bool empty() const {return mNumPoints == 0;}

  //! get the last X
  double lastX() {return mXData[mNumPoints - 1];}

  //! get the last Y
  double lastY() {return mYData[mNumPoints - 1];}

  //! get the X data as a C array
  double* getXData() {return mXData;}

  //! get the Y data as a C array
  double* getYData() {return mYData;}

  //! get the number of points
  UInt32 size() const { return mNumPoints; }

  //! get the X value at idx
  double getX(UInt32 idx) const {return mXData[idx];}

  //! get the Y value at idx
  double getY(UInt32 idx) const {return mYData[idx];}

  //! unit-test the array.
  /*!
   *! This is a method on the class because the implementation knows about
   *! internal initial size defaults that affect the corner case attack
   */   
  void unitTest();

private:
  UInt32 mCapacity;
  UInt32 mNumPoints;
  double* mXData;
  double* mYData;
};

