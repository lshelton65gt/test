// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _INDENTED_STREAM_H_
#define _INDENTED_STREAM_H_

#include "util/c_memmanager.h"
#include "util/CarbonAssert.h"
#include "util/CarbonTypes.h"

template <typename _SinkType>
class IndentedStream {
public: CARBONMEM_OVERRIDES

  enum {
    cINITIAL_INDENT_MASK = 0xFF,
    cMAX_WIDTH_MASK = 0xFF00,
    cNOAUTO = 1<<31
  };

  enum {
    cWIDTH = 80                         //!< default maximum line width
  };

  IndentedStream (_SinkType &sink, const UInt32 options = 0);
  virtual ~IndentedStream ();
  void output (const char *s);
  void operator () (const char *s) { output (s); }
  void nl (bool force = false);
  void operator ++ (int) { mDepth += 2; }
  void operator -- (int) { mDepth -= 2; }

private:

  void doGlue (const char next_char = ' ');

  _SinkType &mSink;
  const UInt32 mOptions;
  bool mNeedsSpace;
  bool mNeedsNewline;
  UInt32 mMaxWidth;
  UInt32 mWidth;
  UInt32 mDepth;
  bool mNoAuto;

};

template <typename _Sink>
IndentedStream<_Sink> &operator << (IndentedStream <_Sink> &sink, const char *s);

template <typename _Sink>
IndentedStream<_Sink> &operator << (IndentedStream <_Sink> &sink, const UInt32 n);

#endif

