// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Class to do a sorted depth first discovery order dump of a graph
*/

#ifndef _GRAPHDUMPER_H_
#define _GRAPHDUMPER_H_

#include "util/Graph.h"
#include "util/UtString.h"

//! UtGraphDumper class
/*! Class to dump a graph to an output stream. Each line represents a
 *  node and indicates a node number (for reference), the edge data
 *  that got there, and the node data. If the node was hit as either a
 *  cross or back edge the output also indicates that by reusing the
 *  node number.
 *
 *  The format of each line without a back/cross edge is:
 *
 *  "[%d]%s%s%d:%s", nodeNumber, indentString, edgeString, parentNumber, nodeString
 *
 *  The format for a back edge node is:
 *
 *  "[*]%s%s%d:[%d]:%s", indentString, edgeString, parentNumber, nodeNumber, nodeString
 *
 *  The format for a cross edge node is:
 *
 *  "   %s%s%d:[%d]:%s", indentString, edgeString, parentNumber, nodeNumber, nodeString
 *
 *  Note that the indentation shows the edges in the graph
 */
class UtGraphDumper : public GraphSortedWalker
{
public:
  CARBONMEM_OVERRIDES

  //! constructor
  UtGraphDumper(UtOStream& out);

  //! destructor
  virtual ~UtGraphDumper() {}

  //! Function to walk and dump the given graph
  /*! walk and dump the graph.
   *
   *  \param graph - The graph to walk and dump
   *
   *  \param nodeCmp - A class used to sort graph nodes (See
   *  GraphSortedWalker)
   *
   *  \param edgeCmp - A class used to sort graph edges (See
   *  GraphSortedWalker)
   *
   *  \param edgeString - The edge string for starting nodes in the
   *  walk. They are not hit from any edges
   */
  void dump(Graph* graph, NodeCmp* nodeCmp, EdgeCmp* edgeCmp,
            const UtString& edgeStr);

  //! Print nodes in depth-first discovery time order
  virtual GraphWalker::Command visitNodeBefore(Graph*, GraphNode*);

  //! Backup one indent level when finished with a node
  virtual GraphWalker::Command visitNodeAfter(Graph*, GraphNode*);

  //! Show a cross connection edge
  virtual GraphWalker::Command visitCrossEdge(Graph*, GraphNode*, GraphEdge*);

  //! Show a back edge
  virtual GraphWalker::Command visitBackEdge(Graph*, GraphNode*, GraphEdge*);

  //! record a tree-edge connection
  virtual GraphWalker::Command visitTreeEdge(Graph*, GraphNode*, GraphEdge*);

protected:
  //! Function to create a string from the edge data
  /*! Override this function to print edge data.
   */
  virtual void edgeString(Graph* graph, GraphNode* node, GraphEdge* edge,
                          UtString* str) = 0;

  //! Function create a string from the node data
  /*! Override this function to print node data.
   */
  virtual void nodeString(Graph* graph, GraphNode* node, UtString* str) = 0;

private:
  // Helper functions
  void printNode(Graph*, GraphNode* node);
  void printCrossOrBackEdgeNode(Graph*, GraphNode*, GraphEdge*,
                                bool isBackEdge);
  void indent(UInt32 spaces);
  void recordParent(Graph* graph, GraphNode* node);

  //! The output stream to dump to
  UtOStream& mOut;

  // Used to print the info
  UInt32 mIndent;
  UInt32 mNodeCount;
  UtString mEdgeStr;
  UInt32 mParentNode;
}; // class UtGraphDumper

#endif // _GRAPHDUMPER_H_
