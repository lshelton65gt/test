// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  This class does a DFS walk and computes the depth for nodes
*/

#ifndef _GRAPHDEPTHWALKER_H_
#define _GRAPHDEPTHWALKER_H_

#include "util/Graph.h"

class UtGraphDepthWalker : public GraphWalker
{
public: CARBONMEM_OVERRIDES
  //! constructor
  UtGraphDepthWalker() : mDepth(0) {}

  //! virtual destructor
  virtual ~UtGraphDepthWalker() {}

  //! Override the visitNodeBefore to initialize the depth to 0
  Command visitNodeBefore(Graph* graph, GraphNode* node);

  //! Override the visitNodeAfter to get fanin depth and copy to walker
  Command visitNodeAfter(Graph* graph, GraphNode* node);

  //! Override the visitNodeBetween to get fanin depth and reset for child
  Command visitNodeBetween(Graph* graph, GraphNode* node);

  //! Override the visitCrossEdge to get fanin depth
  Command visitCrossEdge(Graph* graph, GraphNode*, GraphEdge* edge);

  //! Override the visitBackEdge to make sure there are no cycles
  /*! Default implementation is not too informative
   */
  Command visitBackEdge(Graph*, GraphNode*, GraphEdge*);

protected:
  //! Virtual function to init the depth of a node
  /*! Default implementation uses node scratch field
   */ 
  virtual void putDepth(Graph* graph, GraphNode* node, SIntPtr depth);

  //! Virtual function to get the depth of a node
  /*! Default implementation uses node scratch field
   */
  virtual SIntPtr getDepth(Graph* graph, GraphNode* node) const;

  //! Virtual function to update the depth of a node (max of fanin and current)
  virtual SIntPtr updateDepth(Graph* graph, GraphNode* node, SIntPtr faninDepth);

private:
  //! Function to update the walkers depth
  void updateWalker(SIntPtr depth) { mDepth = depth; }

  //! The last depth computed by a node in the graph
  SIntPtr mDepth;
}; // class UtGraphDepthWalker : public GraphWalker

#endif // _GRAPHDEPTHWALKER_H_
