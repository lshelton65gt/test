// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003, 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __MemoryPerf_h_
#define __MemoryPerf_h_
#include "util/CarbonTypes.h"

#include "util/UtHashMap.h"

class MemoryPerfData {
public: CARBONMEM_OVERRIDES
  MemoryPerfData();

  void reset();

  //! Number of memory reads.
  UInt32 mReads;

  //! Number of memory writes.
  UInt32 mWrites;

  //! Number of objects constructed.
  UInt32 mConstructed;
};

class MemoryPerf {
public: CARBONMEM_OVERRIDES
  MemoryPerf();
  ~MemoryPerf();

  //! Increment construction count for <width,depth>
  void countConstructed(UInt32 width, UInt32 depth);

  //! Increment read count for <width,depth>
  void countReads(UInt32 width, UInt32 depth);

  //! Increment write count for <width,depth>
  void countWrites(UInt32 width, UInt32 depth);

  //! Reset all counters.
  void reset();

  //! Print all counters.
  void print() const;
private:
  typedef std::pair<UInt32,UInt32> MemoryWidthDepth;
  struct HashMemoryWidthDepth : public HashValue<MemoryWidthDepth>
  {
    size_t hash(const MemoryWidthDepth& var) const {
      return (var.first << 5) | var.second; }
  };
  typedef UtHashMap<MemoryWidthDepth, MemoryPerfData*, HashMemoryWidthDepth> MemoryStatsMap;

  //! Disallow copy constructor.
  MemoryPerf (const MemoryPerf&);

  //! Check for existence of <width,depth> map entry.
  void check(UInt32 width, UInt32 depth);

  //! Track all Memory<> accesses by <width,depth>.
  MemoryStatsMap mPerfData;
};

//! Single GLOBAL instance of this class.
extern MemoryPerf * MemPC;

#endif
