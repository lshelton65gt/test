// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#ifndef _UTIOENUM_H_
#define _UTIOENUM_H_

#include "util/UtIOStream.h"
#include "util/UtIStream.h"

// Helper classes for managing C/C++ Enums, including I/O related
// to them.
//
// Someone that wants to define a new enumeration must create:
//    - the standard C/C++ enum declaration
//    - a global array of strings, null-terminated, for the enum
//    - a C++ template giving both the enum and the array of strings
//    - a typedef for the C++ template


//! Class to help define enumerated values that can be read & written
//! to ascii files in a robust and user-friendly way. 
template<typename _EnumType,       // C/C++ enumerated type
         const char** _Strings,
         _EnumType _DefaultVal = (_EnumType) 0>
class UtIOEnum {
public:
  UtIOEnum() : mValue(_DefaultVal) {
  }
  UtIOEnum(_EnumType val) : mValue(val) {
  }

  //! Implicitly get the enumerated value from this
  operator _EnumType() const {return mValue;}

  //! Get the string rep from this
  operator const char*() const {return _Strings[(int) mValue];}

  //! Assign the enumerated value
  UtIOEnum& operator=(_EnumType e) {
    mValue = e;
    return *this;
  }

  //! Explicitly get the enumerated value from this
  _EnumType getEnum() const {return mValue;}

  //! Explicitly get the string value from this
  const char* getString() const {return _Strings[(int) mValue];}

  //! Explicitly get the string array for the enumerated values
  static const char** getStrings() {return _Strings;}

  //! Convert given string to enumerated value
  bool parseString(const char *str) {
    const char** strs = _Strings;
    for (UInt32 i = 0; strs[i] != NULL; ++i) {
      if (strcmp(strs[i], str) == 0) {
        mValue = (_EnumType) i;
        return true;
      }
    }
    return false;
  }

  bool read(UtIStream& is) {
    // On reading the file, assume nothing.  Check everything
    UtString buf;
    if (is >> buf) {
      if (parseString(buf.c_str())) {
        return true;
      }
      UtString msg;
      msg << "Invalid enumerated value " << buf;
      is.reportError(msg.c_str());
    }
    return false;
  }

  void write(UtOStream& os) const {
    // On writing the file, assume val is legit (otherwise we crash)
    os << _Strings[(int) mValue];
  }

private:
  _EnumType mValue;
}; // class UtIOEnum

#if 0
//! This extra overload cannot be a class member
template<typename _EnumType, const char** _Strings, _EnumType _DefaultVal>
static UtOStream& operator<<(UtOStream& os,
                             const UtIOEnum<_EnumType, _Strings, _DefaultVal>&
                             val)
{
  // On writing the file, assume val is legit (otherwise we crash)
  const char** strs = val.getStrings();
  os << strs[(int) val.getEnum()];
  return os;
}
#endif

#endif
