// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef SOURCELOCATOR_H_
#define SOURCELOCATOR_H_

#include <cstring>
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef __Util_h_
#include "util/Util.h"
#endif
#ifndef _CARBON_HASH_VALUE_
#include "util/HashValue.h"
#endif

#ifndef __STRING
//! Stringify expression
#define __STRING(exp) #exp
#endif

// provides the source file/line which causes assertion
#define LOC_ASSERT(exp, _loc) \
  if (!(exp)) { \
     fprintf(stderr, "****INTERNAL ERROR*****\n");\
     (_loc).print(); \
     fprintf(stderr, "%s:%d \nLOC_ASSERT(%s) failed\n", \
            __FILE__, __LINE__, __STRING(exp)); \
    abort(); \
  }

class UtString;
class SourceLocator;
class AtomicCache;
class ZostreamDB;
class ZistreamDB;

//! SourceLocatorFactory class
/*!
 * Manage SourceLocator objects; responsible for creation and deletion of
 * source locators.
 */
class SourceLocatorFactory
{
public: CARBONMEM_OVERRIDES
  //! constructor
  SourceLocatorFactory();

  //! Return a locator for the given file and line number.
  SourceLocator create(const char* file, UInt32 line);

  //! destructor
  ~SourceLocatorFactory();

  friend class SourceLocator;

  // The most efficient way to write SourceLocator instances to a DB file is to
  // just spit out the 32-bit descriptors embedded in the
  // SourceLocator. However, this also requires that the contents of the
  // associated SourceLocatorFactory instance are also saved. This is fairly
  // straightforward when writing. However, the reading process will already
  // have a different SourceLocatorFactory. Therefore to read a SourceLocator,
  // first the factory used to make that source locator is needed, then the
  // 32-bit descriptor is read and translated from its original factory into the
  // destination factory.

  //! Write the source locator factory to a DB file
  bool writeDB (ZostreamDB &);

  //! Write a location to a DB file
  bool writeDB (ZostreamDB &, const SourceLocator) const;

  //! Read the source locator factory from a DB file
  bool readDB (ZistreamDB &);

  //! Skip over the factory
  static bool ignoreFactoryInDB (ZistreamDB &);

  //! Read a location from a file
  /*! To read the source location, first read the associated factory and then
   *  read each individual source location into a desination factory:
   *
   *  SourceLocatorFactory tmpFactory (...);
   *  ...
   *  tmpFactory.readDB (in);
   *  ...
   *  while () {
   *    SourceLocation loc;
   *    tmpFactory.readDB (in, &loc, mSourceLocationFactory);
   *    ...
   *  }
   *
   * (this is used in IODBRuntime::readDB).
   */
  bool readDB (ZistreamDB &, SourceLocator *loc, SourceLocatorFactory *destination);
  static bool ignoreEntryInDB (ZistreamDB &);

private:
  //! returns true if \a bits defines a tic protected source file
  bool isTicProtected(const UInt32 bits);

  //! the raw decode steps (only decode should call this)
  void decodeHelper(UInt32 bits, bool* isTicProtected, const char** filename, UInt32* lineNumber);
  
  //! Decode a descriptor
  void decode(UInt32 bits, const char** filename, UInt32* lineNumber);

  //! Hide copy and assign constructors.
  SourceLocatorFactory(const SourceLocatorFactory&);
  SourceLocatorFactory& operator=(const SourceLocatorFactory&);

  // Keep track of the raw filenames, allowing name-to-index and
  // index-to-name lookups.  We will only allow 64k distinct filenames
  // so that we can play bitfield tricks.
  typedef UtHashMap<StringAtom*,UInt32> FilenameToIndexMap;
  typedef UtArray<StringAtom*> FilenameVector;
  AtomicCache* mFilenames;
  FilenameToIndexMap* mFilenameToIndexMap;
  FilenameVector* mFilenameVector;

  // We want to allow files with very large numbers of lines, but it's
  // hard to predict what that limit should be if we want to share
  // filename index and line-number in one UInt32.  So we make another
  // layer of indirection using the filename index, and define a
  // FileBlockDesc as a UInt32 where the upper 16 bits represents
  // the upper 16-bits of line-numbers, and the lower 16 bits
  // represents the file index.  We only allow 64k of these (which
  // further restricts the 64k distinct filename limitation imposed
  // above).  But we can represent line numbers up to 2^32-2 (if
  // they're all in one file).
  typedef UInt32 FileBlockDesc;
  typedef UtHashMap<FileBlockDesc,UInt32> FileBlockDescToIndexMap;
  typedef UtArray<FileBlockDesc> FileBlockDescVector;
  FileBlockDescToIndexMap* mFileBlockDescToIndexMap;
  FileBlockDescVector* mFileBlockDescVector;
} ;

/*!
  \file
  Declaration of the classes to manage source locators.
*/

//! SourceLocator class
/*!
 * These are managed by the SourceLocatorFactory.
 *
 * Source Location - simple for now, but possibly will be expanded to support
 * multiple lines, and/or line ranges.
 *
 * In the future, may want to pack a SourceLocator into a single longword (into
 * the mPtr).
 */
class SourceLocator
{
public: CARBONMEM_OVERRIDES
  //! Constructor
  SourceLocator() : mBits(0) {}

  //! Assign operator.
  SourceLocator& operator=(const SourceLocator& loc)
  {
    if (&loc != this) {
      mBits = loc.mBits;
    }
    return *this;
  }

  //! Comparison operator for sorting
  bool operator<(const SourceLocator& b) const
  {
    return ( compare (*this, b) < 0 );
  }

  //! Copy constructor.
  SourceLocator(const SourceLocator& loc) :
    mBits(loc.mBits)
  {}

  //! Return the file name.
  const char* getFile() const;

  //! Return the line number.
  UInt32 getLine() const;

  //! Debug print to cout (do not use in product code)
  void print() const;

  //! Compose file:line to a string
  void compose(UtString* str) const;

  //! returns true if the current location is from a tic-protected source line (`protected)
  bool isTicProtected() const;

  //! destructor
  /*!
   * Memory is managed by the factory, this does nothing.
   */
  ~SourceLocator() {}

  //! Give the factory access to the StoredSourceLocator; ensure that only it
  //! can create and delete those objects.
  friend class SourceLocatorFactory;

  static int compare(const SourceLocator& s1, const SourceLocator& s2)
  {
    const char* file1, *file2;
    UInt32 line1, line2;
    SourceLocator::smTheFactory->decode(s1.mBits, &file1, &line1);
    SourceLocator::smTheFactory->decode(s2.mBits, &file2, &line2);
    
    int cmp = std::strcmp(file1, file2);
    if (cmp == 0)
      cmp = ((int) line1) - ((int) line2);
    return cmp;
  }

  //! void* object identifying a SourceLocator
  typedef UIntPtr ID;

  static ID NULL_ID;

  //! get a unique void* sized object from a SourceLocator -- needed for SymbolTable
  ID getID() {return mBits;}

  //! construct a SourceLocator from a void* sized ID -- needed for SymbolTable
  static SourceLocator fromID(ID id) {
    return SourceLocator(id);
  }

  //! Hash Function
  size_t hash() const { return (size_t) mBits; }

  //! Operator== for hashing
  bool operator==(const SourceLocator& other) const 
  {
    return (mBits == other.mBits);
  }

private:
  //! constructor
  /*!
    This is private because only SourceLocatorFactory should call.

    \param loc The factory-managed locator.
   */
  SourceLocator(UInt32 bits): mBits(bits) {}

  // Encode filename and line # each in short.  This is probably way
  // too many filenames (64k) and not nearly enough line-numbers, but
  // we will allow the line #s to overflow by encoding file-chunks
  // in the file index.  We will not be able to represent more than
  // 64k filenames :(.
  UInt32 mBits;

  static SourceLocatorFactory* smTheFactory;

} ; // class SourceLocator

#endif
