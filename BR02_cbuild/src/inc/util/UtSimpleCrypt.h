// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*
  Crypt macros for applications that cannot read in the database
  version of crypting. It is not nearly as powerful, but it will hide
  strings in applications.

  In order to use, you must know the size of the string (without the
  '\0') that you want to crypt. Use the corresponding macro for that
  string. Eg., for a string "cds", that is 3 characters long, so you
  would use SCRAMBLECODE3 in this fashion.
  int transCds[3]; // notice no room for \0
  SCRAMBLECODE3("cds", transCds);
  
  To decrypt:
  char decryptCds[4];
  decryptCds[3] = '\0';
  SCRAMBLECODE3(transCds, decryptCds);

  It is a good idea to use an integer array when encrypting and a char
  array when decrypting, mainly for readability.
*/

#ifndef _UT_SIMPLE_CRYPT_H_
#define _UT_SIMPLE_CRYPT_H_

#define cNameKey 0x82

#define SCRAMBLECODE31(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16); \
     arr[17] = name[17] ^ (cNameKey + 17); \
     arr[18] = name[18] ^ (cNameKey + 18); \
     arr[19] = name[19] ^ (cNameKey + 19); \
     arr[20] = name[20] ^ (cNameKey + 20); \
     arr[21] = name[21] ^ (cNameKey + 21); \
     arr[22] = name[22] ^ (cNameKey + 22); \
     arr[23] = name[23] ^ (cNameKey + 23); \
     arr[24] = name[24] ^ (cNameKey + 24); \
     arr[25] = name[25] ^ (cNameKey + 25); \
     arr[26] = name[26] ^ (cNameKey + 26); \
     arr[27] = name[27] ^ (cNameKey + 27); \
     arr[28] = name[28] ^ (cNameKey + 28); \
     arr[29] = name[29] ^ (cNameKey + 29); \
     arr[30] = name[30] ^ (cNameKey + 30);

#define SCRAMBLECODE30(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16); \
     arr[17] = name[17] ^ (cNameKey + 17); \
     arr[18] = name[18] ^ (cNameKey + 18); \
     arr[19] = name[19] ^ (cNameKey + 19); \
     arr[20] = name[20] ^ (cNameKey + 20); \
     arr[21] = name[21] ^ (cNameKey + 21); \
     arr[22] = name[22] ^ (cNameKey + 22); \
     arr[23] = name[23] ^ (cNameKey + 23); \
     arr[24] = name[24] ^ (cNameKey + 24); \
     arr[25] = name[25] ^ (cNameKey + 25); \
     arr[26] = name[26] ^ (cNameKey + 26); \
     arr[27] = name[27] ^ (cNameKey + 27); \
     arr[28] = name[28] ^ (cNameKey + 28); \
     arr[29] = name[29] ^ (cNameKey + 29);

#define SCRAMBLECODE28(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16); \
     arr[17] = name[17] ^ (cNameKey + 17); \
     arr[18] = name[18] ^ (cNameKey + 18); \
     arr[19] = name[19] ^ (cNameKey + 19); \
     arr[20] = name[20] ^ (cNameKey + 20); \
     arr[21] = name[21] ^ (cNameKey + 21); \
     arr[22] = name[22] ^ (cNameKey + 22); \
     arr[23] = name[23] ^ (cNameKey + 23); \
     arr[24] = name[24] ^ (cNameKey + 24); \
     arr[25] = name[25] ^ (cNameKey + 25); \
     arr[26] = name[26] ^ (cNameKey + 26); \
     arr[27] = name[27] ^ (cNameKey + 27)


#define SCRAMBLECODE27(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16); \
     arr[17] = name[17] ^ (cNameKey + 17); \
     arr[18] = name[18] ^ (cNameKey + 18); \
     arr[19] = name[19] ^ (cNameKey + 19); \
     arr[20] = name[20] ^ (cNameKey + 20); \
     arr[21] = name[21] ^ (cNameKey + 21); \
     arr[22] = name[22] ^ (cNameKey + 22); \
     arr[23] = name[23] ^ (cNameKey + 23); \
     arr[24] = name[24] ^ (cNameKey + 24); \
     arr[25] = name[25] ^ (cNameKey + 25); \
     arr[26] = name[26] ^ (cNameKey + 26)

#define SCRAMBLECODE26(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16); \
     arr[17] = name[17] ^ (cNameKey + 17); \
     arr[18] = name[18] ^ (cNameKey + 18); \
     arr[19] = name[19] ^ (cNameKey + 19); \
     arr[20] = name[20] ^ (cNameKey + 20); \
     arr[21] = name[21] ^ (cNameKey + 21); \
     arr[22] = name[22] ^ (cNameKey + 22); \
     arr[23] = name[23] ^ (cNameKey + 23); \
     arr[24] = name[24] ^ (cNameKey + 24); \
     arr[25] = name[25] ^ (cNameKey + 25);

#define SCRAMBLECODE25(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16); \
     arr[17] = name[17] ^ (cNameKey + 17); \
     arr[18] = name[18] ^ (cNameKey + 18); \
     arr[19] = name[19] ^ (cNameKey + 19); \
     arr[20] = name[20] ^ (cNameKey + 20); \
     arr[21] = name[21] ^ (cNameKey + 21); \
     arr[22] = name[22] ^ (cNameKey + 22); \
     arr[23] = name[23] ^ (cNameKey + 23); \
     arr[24] = name[24] ^ (cNameKey + 24);

#define SCRAMBLECODE23(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16); \
     arr[17] = name[17] ^ (cNameKey + 17); \
     arr[18] = name[18] ^ (cNameKey + 18); \
     arr[19] = name[19] ^ (cNameKey + 19); \
     arr[20] = name[20] ^ (cNameKey + 20); \
     arr[21] = name[21] ^ (cNameKey + 21); \
     arr[22] = name[22] ^ (cNameKey + 22); 

#define SCRAMBLECODE22(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16); \
     arr[17] = name[17] ^ (cNameKey + 17); \
     arr[18] = name[18] ^ (cNameKey + 18); \
     arr[19] = name[19] ^ (cNameKey + 19); \
     arr[20] = name[20] ^ (cNameKey + 20); \
     arr[21] = name[21] ^ (cNameKey + 21);


#define SCRAMBLECODE21(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16); \
     arr[17] = name[17] ^ (cNameKey + 17); \
     arr[18] = name[18] ^ (cNameKey + 18); \
     arr[19] = name[19] ^ (cNameKey + 19); \
     arr[20] = name[20] ^ (cNameKey + 20);


#define SCRAMBLECODE20(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16); \
     arr[17] = name[17] ^ (cNameKey + 17); \
     arr[18] = name[18] ^ (cNameKey + 18); \
     arr[19] = name[19] ^ (cNameKey + 19);

#define SCRAMBLECODE19(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16); \
     arr[17] = name[17] ^ (cNameKey + 17); \
     arr[18] = name[18] ^ (cNameKey + 18); 

#define SCRAMBLECODE18(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16); \
     arr[17] = name[17] ^ (cNameKey + 17);

#define SCRAMBLECODE17(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15); \
     arr[16] = name[16] ^ (cNameKey + 16);

#define SCRAMBLECODE16(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); \
     arr[15] = name[15] ^ (cNameKey + 15);

#define SCRAMBLECODE15(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13); \
     arr[14] = name[14] ^ (cNameKey + 14); 

#define SCRAMBLECODE14(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); \
     arr[13] = name[13] ^ (cNameKey + 13);

#define SCRAMBLECODE13(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); \
     arr[12] = name[12] ^ (cNameKey + 12); 

#define SCRAMBLECODE12(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); \
     arr[11] = name[11] ^ (cNameKey + 11); 

#define SCRAMBLECODE11(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8); \
     arr[9] = name[9] ^ (cNameKey + 9); \
     arr[10] = name[10] ^ (cNameKey + 10); 


#define SCRAMBLECODE9(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7); \
     arr[8] = name[8] ^ (cNameKey + 8);

#define SCRAMBLECODE8(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6); \
     arr[7] = name[7] ^ (cNameKey + 7);

#define SCRAMBLECODE7(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); \
     arr[6] = name[6] ^ (cNameKey + 6);

#define SCRAMBLECODE6(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); \
     arr[5] = name[5] ^ (cNameKey + 5); 

#define SCRAMBLECODE5(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); \
     arr[4] = name[4] ^ (cNameKey + 4); 

#define SCRAMBLECODE4(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); \
     arr[3] = name[3] ^ (cNameKey + 3); 

#define SCRAMBLECODE3(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1); \
     arr[2] = name[2] ^ (cNameKey + 2); 

#define SCRAMBLECODE2(name, arr) \
     arr[0] = name[0] ^ cNameKey; \
     arr[1] = name[1] ^ (cNameKey + 1);

#endif // _UT_SIMPLE_CRYPT_H_
