// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonAssert.h"

//! test a comparison function used for sorting or rb-trees to make sure it's
//! consistent over a container.  Note that this is not the less-than function,
//! this is the compare function that returns -1,0,1
template<class iterator, class Fn>
void UtTestCompareFunction(const iterator& begin, const iterator& end, Fn compare_fn) {
  // First, test all pairs
  for (iterator i = begin; i != end; ++i) {
    const typename iterator::value_type& ival = *i;
    for (iterator j = begin; j != end; ++j) {
      const typename iterator::value_type& jval = *j;
      int cmp_ij = compare_fn(ival, jval);
      int cmp_ji = compare_fn(jval, ival);

      if (cmp_ij == 0) {
        INFO_ASSERT(cmp_ji == 0, "compare reflection insanity 1");
      }
      else if (cmp_ij < 0) {
        INFO_ASSERT(cmp_ji > 0, "compare reflection insanity 2");
      }
      else {
        INFO_ASSERT(cmp_ji < 0, "compare reflection insanity 3");
      }
    }
  }

  // Now, test all triples for transitity
  for (iterator i = begin; i != end; ++i) {
    const typename iterator::value_type& ival = *i;
    for (iterator j = i; j != end; ++j) {
      const typename iterator::value_type& jval = *j;
      int cmp_ij = compare_fn(ival, jval);

      for (iterator k = j; k != end; ++k) {
        const typename iterator::value_type& kval = *k;
        int cmp_ik = compare_fn(ival, kval);
        int cmp_jk = compare_fn(jval, kval);

        if ((cmp_ij == 0) && (cmp_jk == 0)) {
          INFO_ASSERT(cmp_ik == 0, "compare transitivity insanity 1");
        }
        else if ((cmp_ij < 0) && (cmp_jk < 0)) {
          INFO_ASSERT(cmp_ik < 0, "compare transitivity insanity 2");
        }
        else if ((cmp_ij > 0) && (cmp_jk > 0)) {
          INFO_ASSERT(cmp_ik > 0, "compare transitivity insanity 3");
        }
      }
    }
  } // for
} // void UtTestCompareFunction
