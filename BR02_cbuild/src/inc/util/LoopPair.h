// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _LOOPPAIR_H_
#define _LOOPPAIR_H_


#include "util/StlIterator.h"
#include "util/Util.h"

//! LoopPair template class
/*!
  This class provides a mechanism for a class to expose iteration over
  the elements yielded by two heterogeneous sub-loops.  The sub-loops
  must of course have the same value_type.

  This class can be composed recursively to form loops of more than 2 loops,
  but this should be done only a limited amount, because every operation will
  take time proportional to the number of loops.  If there are going to be
  more than 4 subloops, you should use LoopMulti, which will run in constant
  time.  However, LoopMulti requires homogeneous loop types.
*/

template <class _LoopType1, 
          class _LoopType2, 
          class _ValueType,
          class _ReferenceType>
class LoopPair
{
public: CARBONMEM_OVERRIDES
  typedef _ValueType value_type;
  typedef _ReferenceType reference;

  LoopPair()
  {
  }

/*
  LoopPair(_LoopType1& loop1, _LoopType2& loop2)
    : mLoop1(loop1),
      mLoop2(loop2)
  {
  }
*/

  LoopPair(_LoopType1 loop1, _LoopType2 loop2)
    : mLoop1(loop1),
      mLoop2(loop2)
  {
  }
  //! Copy constructor to help the user manage & pass around Loops.
  LoopPair(const LoopPair& src): mLoop1(src.mLoop1), mLoop2(src.mLoop2)
  {
  }

  //! Assignment operator to help the user manage & pass around Loops.
  LoopPair& operator=(const LoopPair& src)
  {
    if (&src != this)
    {
      mLoop1 = src.mLoop1;
      mLoop2 = src.mLoop2;
    }
    return *this;
  }

  void operator++()
  {
    if (mLoop1.atEnd())
      ++mLoop2;
    else
      ++mLoop1;
  }

  reference operator*() const
  {
    if (mLoop1.atEnd())
      return *mLoop2;
    return *mLoop1;
  }

  bool atEnd() const
  {
    return mLoop1.atEnd() && mLoop2.atEnd();
  }

  bool operator()(value_type* ptr)
  {
    if (!mLoop1.atEnd() && mLoop1(ptr))
      return true;
    return mLoop2(ptr);
  }

  typedef StlIterator<LoopPair<_LoopType1, _LoopType2, _ValueType, _ReferenceType> > iterator;

  //! The start of the loop.
  iterator begin()
  {
    return iterator(this);
  }

  //! The tail of the loop
  iterator end()
  {
    return iterator(NULL);
  }

  //! Return the number of elements remaining -- note:  order N operation
  size_t size() const
  {
    size_t s = 0;
    for (LoopPair<_LoopType1, _LoopType2> tmp = *this; !tmp.atEnd(); ++tmp)
      ++s;
    return s;
  }

private:
  _LoopType1 mLoop1;
  _LoopType2 mLoop2;
};


#endif //  _LOOPPAIR_H_
