// -*- C++ -*-
#ifndef __FileCollector_h_
#define __FileCollector_h_
/******************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*! \file
 * Abstract ArgProc callback class
 */

#include "util/c_memmanager.h"

/*!
 * \class FileCollector
 * Abstract base class for the ArgProc callback
 */
class FileCollector
{
public:
  CARBONMEM_OVERRIDES

  //! dtor
  virtual ~FileCollector() {}

  //! Process a command line argument
  /*! \param arg The option to be processed
   *  \param param An optional parameter value associated with the option.
       Pass in NULL to indicate that there is no associated parameter.
   *  \returns true if the FileCollector has consumed the argument and parameter.
   */
  virtual bool scanArgument( const char *arg, const char *param ) = 0;
  //! Add an argument to an internal data structure
  /*! 
    The data structure the arg is stored in must be defined by the
    derived class.
   */
  virtual void addArgument( const char *fileName ) = 0;
};

#endif
