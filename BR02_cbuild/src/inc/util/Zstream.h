// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __Zstream_h_
#define __Zstream_h_


#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef __CarbonPlatform_h_
#include "util/CarbonPlatform.h"
#endif
#ifndef __UtString_h_
#include "util/UtString.h"
#endif
#ifndef _UtStringArray_h_
#include "util/UtStringArray.h"
#endif
#ifndef __UtHashMap_h_
#include "util/UtHashMap.h"
#endif
#ifndef _LOOP_H_
#include "util/Loop.h"
#endif

#define ZDB_DEBUG 0
#if ZDB_DEBUG
#ifndef __UtIOStream_h_
#include "util/UtIOStream.h"
#endif
#endif

#ifndef __CarbonAssert_h_
#include "util/CarbonAssert.h"
#endif
#ifndef __UtFileBuf_h_
#include "util/UtFileBuf.h"
#endif

typedef struct z_stream_s z_stream;

/*!
  \file
  Streaming class to write and read compressed and encrypted files.
  There are 2 formatting types: binary (ZostreamDB and ZistreamDB) and
  textual (Zostream and Zistream). Both compress and encrypt. The
  binary format is faster for reading numbers (raw casts to numbers),
  but creates larger files. The textual format will create smaller
  files, but is slower to read if the data consists mostly of numbers.
*/

//! The UtIO::endline for a Zstream.
extern const char Zendl;

//! Gzip-style compressing filestream class object base
/*!
  This is only a base class for the output and input compress streams.
  It encapsulates shared functionality such as error messaging, file
  closing, and file buffering.
  \sa Zostream, Zistream
*/
class Zstream
{
public: 
  CARBONMEM_OVERRIDES

  //! constructor
  Zstream(const char*, void* key);

  //! virtual destructor
  virtual ~Zstream();

  //! void* operator for conditional checks
  /*!
    This allows a user to do 
    \verbatim
    f.open(....)
    if (f)
    ...
    \endverbatim
  */
  operator void*() const;

  //! Logical Not operator for conditional checks
  /*!
    This allows a user to do
    \verbatim
    f.open(...)
    if (!f)
    ...
    \endverbatim
  */
  int operator!() const;

  //! Returns true if a failure occurred during writing/reading
  bool fail() const;

  //! Return error message
  /*!
    If there was a failure this returns the last error message.
  */
  const char* getError() const;

  /*! identical to getError()
   * \sa getError()
   */
  const char* getErrmsg() const {return getError();}
  
  //! User controlled set error 
  /*!
    This allows a user to set the state of the file to be bad and
    discontinue reading or writing the file. Any status returning
    calls after this will return fail() to be true. 

    \param msg User message string. This will be stored and can be
    retrieved with getError()
    \param includeFilename If true, include '\<file\>: '
    \warning This will erase any previously set error message.
  */
  void setError(const char* msg, bool includeFilename = false);

  //! Set the file buffer storage size
  /*!
    This resizes the buffer that gets written to the file. The
    larger the buffer, the less write calls. Defaults to 65536
    bytes.
    \warning This throws away any data currently in the buffer. So,
    be sure to flush the buffer if any data has been read from or
    written to the file before calling this.
    \param size Size of storage in bytes. 65536 bytes is the lower
    bound. Anything lower than the lower bound gets set to the lower
    bound.
  */
  void resizeFileBuffer(UInt32 size);
    
  //! set the data buffer storage size
  /*!
    This resizes the data buffer that gets compressed. The larger
    the data buffer, the more the data gets compressed. Defaults to
    65536 bytes.
    \warning This throws away any data currently in the buffer. So,
    be sure to flush the buffer if any data has been read from or
    written to the file before calling this.
    \param size Size of storage in bytes. 65536 bytes is the lower
    bound. Anything lower than the lower bound gets set to the lower
      bound.
  */
  void resizeDataBuffer(UInt32 size);
  
  //! Closes the file
  /*!
    This is done automatically upon destruction.
    \returns true if the file closed successfully
  */
  virtual bool close() = 0;

  //! 8 Byte swap to make Big endian
  /*!
    This does the big-endian transformation in order to allow numbers
    to be arch independent. Does byte swap on linux.
  */
  void byteSwapOnLittleEndian(SInt64* num);
  
  //! Put the file descriptor in the specified position
  /*!
    \warning For \e ADVANCED users only. Using this can do some really
    nasty things to your file. When writing, before seeking always
    flush!
    \sa seekEnd, tellfd
  */
  void seek(SInt64 pos);

  //! Place the file descriptor at the end of the file
  /*!
    \warning For \e ADVANCED users only.
    \sa seek, tellfd
  */
  virtual void seekEnd() = 0;

  //! Get the filename
  void getFilename(UtString* filename) const;

  //! mark record end
  /*!
    When reading/writing, the encryption cipher needs to be kept in
    line with seeks. Whenever a file is written into an ZostreamZip
    archive, for example, it must begin with the cipher
    initialized. Each record, after a seek, requires an
    initialization, because the cipher depends on what was last
    written.
  */
  void markRecordEnd();

protected:
  //! helper function to seek in a Zistream or Zostream
  /*!
   *! This method will call setError if an error occurs
   */
  virtual void seekHelper(SInt64 pos) = 0;

  virtual SInt64 tellfd() = 0;

  //! Data buffer
  /*!
    When this buffer becomes full, the data gets compressed.
  */
  UtFileBuf mDataBuf;

  //! File buffer
  /*!
    When this buffer becomes full, the data gets written to disk
  */
  UtFileBuf mFileBuf;

  //! File state
  bool mFileGood;
  //! File name
  UtString mFilename;

  static const UInt32 cMagicFlagSize = 3;
  static const unsigned char cMagicName[7];

  static const UInt32 cMagicNameSize = sizeof(cMagicName);
  static const UInt32 cMagicBufSize = cMagicFlagSize * 2 + cMagicNameSize;
  static const unsigned char cCarbonMagic[cMagicFlagSize];
  static const unsigned char cCarbonTail[cMagicFlagSize];
  static const UInt32 cFileVersion = 2;
  static const UInt32 cFileVersionReserved = 0;
  
  //! Buffer used by zlib for compression
  /*!
    This buffer is currently twice the size of the mDataBuf
    capacity. It doesn't need to be. It just is for convenience of
    coding. The zlib spec says that the buffer must be 0.1% + 12 bytes
    larger than the input buffer. If this becomes an issue, we will do
    the calculation. But, doing it this way assures that deflate()
    will work in one call.
  */
  char* mCompressBuf;
  
  //! The size of the compression buffer. Currently 2x mDataBuf.
  UInt32 mCompressBufSize;

  //! To confuse potential hackers, CIPHER is compiled as MemPool
#define CIPHER MemPool

  //! Auxiliary class used for buffer encryption/decryption
  class CIPHER;

  //! Member variable used for encryption/decryption
  CIPHER* mCipher;

  //! encryption/decryption for filebuffer writing
  /*!
    This is used when writing/reading the number of bytes to
    write/read. Can't use mCipher because the size used in the
    ciphering when writing out the number of bytes to write is always
    4. By having a second cipher. It makes it virtually impossible to
    see any kind of relationship.
  */
  CIPHER* mFileCipher;
  
  
  //! Set the error message
  /*!
    This sets mFileGood to be false and sets the error message
  */
  void setErrorInternal(const char* msg, z_stream* zlibStruct = NULL);  

#define ZSTREAM_INIT purify
#define ZSTREAM_CIPHER_INIT reuseBuffer
  void ZSTREAM_INIT() // formerly init()
  {
    mFileGood = true;
    mCompressBuf = NULL;
    mCompressBufSize = 0;
    ZSTREAM_CIPHER_INIT();
  }
  
private:
  UtString mErrMsg;  

  void ZSTREAM_CIPHER_INIT();

  void resizeCompressBuf();
  bool checkBufResize(UInt32* size);

  //forbid
  Zstream();
  Zstream(const Zstream&);
  Zstream& operator=(const Zstream&);
}; // class Zstream


//! Output compressed filestream class object
/*!
  This class defines an interface to output data into a compressed
  file using the same algorithm as gzip. In fact, file output by this
  class can be read using gunzip or gzcat.
  
  \sa Zistream, Zstream
*/
class Zostream : public Zstream
{
public: 
  CARBONMEM_OVERRIDES

  //! opening constructor
  /*!
    \param name name of file to open
  */
  Zostream(const char* name, void* key);
  
  //! virtual destructor
  virtual ~Zostream();

  //! returns true if the file is open
  /*!
    This goes against the style conventions due to the STL-like
    interface. is_open() returns false for Zi/oStringStreams.
  */
  bool is_open() const;

  virtual void seekEnd();

#define ZOSTREAM_INIT_AND_OPEN advancePage
#define ZOSTREAM_SETUP_VARS switchBuffer
  inline void ZOSTREAM_INIT_AND_OPEN()
  {
    ZSTREAM_INIT();
    ZOSTREAM_SETUP_VARS();
    openForWrite();
  }

  //! Get the low-level file descriptor
  /*!
    \warning You better know what you are doing! This is meant to be a
    hook for higher level classes needing to manipulate Zstreams,
    such as ZstreamZip
  */
  int getfd() {return mFile;}

  virtual void seekHelper(SInt64 pos);

  //! Flush the buffer into the file
  /*!
    Returns true if the file was written successfully
  */
  bool flush();

  //! Closes the file
  /*!
    This flushes prior to closing.
  */
  bool close();

  //! In what position is the file descriptor?
  virtual SInt64 tellfd();

  //! UtString write operator
  virtual Zostream& operator<< (const UtString& str);  

  //! system-dependent (32-bit or 64-bit) pointer
  virtual Zostream& operator<< (const void* p);

  //! char* write operator
  virtual Zostream& operator<< (const char* str);
  //! char write operator
  virtual Zostream& operator<< (const char c);  

  //! unsigned char operator
  virtual Zostream& operator<< (const unsigned char c);  
  //! signed char operator
  virtual Zostream& operator<< (const signed char c);  

  //! double write operator
  virtual Zostream& operator<< (double num);

  //! Signed long write operator
  virtual Zostream& operator<< (SInt32 num);  
  //! Unsigned long write operator
  virtual Zostream& operator<< (UInt32 num);    
  //! Signed short write operator
  virtual Zostream& operator<< (SInt16 num);    
  //! Unsigned short write operator
  virtual Zostream& operator<< (UInt16 num);    
  //! unsigned 64 bit operator
  virtual Zostream& operator<< (UInt64 num);    
  //! signed 64 bit operator
  virtual Zostream& operator<< (SInt64 num);    

  //! UtStringArray
  virtual Zostream& operator<< (const UtStringArray &strings);

  //! explicit write method with size for const char*
  /*!
    \param s Array of bytes to write
    \param n Number of bytes to write
    \warning If a zero terminated UtString is passed in and strlen is used
    the zero will not be written. 
  */
  bool write(const char* s, UInt32 n);

  //! explicit write method with size for const void*
  bool write(const void* s, UInt32 n);

  //! where am I about to write, in terms of uncompressed bytes?
  SInt64 tell() const {return mWritePos;}

#define ZOSTREAM_WRITE_MAGIC_HEADER flushPage
  //! Write out the magic header at the current position
  /*!
    \warning For advanced users only. This is meant to be a hook for
    ZstreamZip.
  */
  void ZOSTREAM_WRITE_MAGIC_HEADER();

  //! DO NOT USE
  /*!
    \internal
    Temporarily made public for ZostreamZip.
  */
  virtual void safeWrite(char* s, int numBytesW);  

protected:
  int mFile;                    // low-level file descriptor for write

  //! Special constructor for ZostreamZip entries
  Zostream(const char* name, Zostream* srcStream);

  //! Opens the file for writing. Nothing for ZoStringStream
  virtual void openForWrite();

private:

  void flushDataBuffer();
  void rawWriteToFileBuf(const char* s, UInt32 n);
  void writeCompressBufferToFile(UInt32 nu, UInt32 n);

  void ZOSTREAM_SETUP_VARS();

  //! Write a binary UInt32.
  /*!
    Normally, a Zostream only writes out text, but in the case where
    the text is surrounded by flags, codes, etc. It is useful to write
    the UInt32 out in 4 bytes.
  */
  void writeRawUInt32(UInt32 n);


  //forbid
  Zostream();
  Zostream(const Zostream&);
  Zostream& operator=(const Zostream&);

  SInt64 mWritePos;
};

class ZoStringStream : public Zostream
{
public:
  CARBONMEM_OVERRIDES

  //! opening constructor
  /*!
    \param f String buffer. This gets written to.
  */
  ZoStringStream(UtString* f, void* key);
  
  //! virtual destructor
  virtual ~ZoStringStream();

protected:
  //! Does nothing.
  virtual void openForWrite();

  //! Write to the buffer
  virtual void safeWrite(char* s, int numBytesW);  
private:
  ZoStringStream(const ZoStringStream&); // forbid
  ZoStringStream& operator=(const ZoStringStream&); // forbid

  UtString* mStringDescriptor;
};

//! Input compressed or uncompressed filestream object
/*!
  This class object can read in gzipped files or files output by the
  Zostream class.
  It is automatically detected whether or not a file is compressed and
  this class will adapt to that.
  \sa Zostream, Zstream
*/
class Zistream : public Zstream
{
public: 
  CARBONMEM_OVERRIDES

  //! opening constructor 
  Zistream(const char*, void* key);

  //! constructor that takes an already existing stream
  Zistream(UtIStream* instream, void* key);

  //! virtual destructor
  virtual ~Zistream();

  //! returns true if the file is open
  /*!
    This goes against the style conventions due to the STL-like
    interface. is_open() returns false for Zi/oStringStreams.
  */
  bool is_open() const;

  UtIStream* getfd() {return mFile;}

  //! In what position is the file descriptor?
  virtual SInt64 tellfd();

  //! close the file
  virtual bool close();

  // Does not get undef'd 
  //! Hide initialization function name
#define ZISTREAM_INIT_AND_OPEN advancePage
#define ZISTREAM_INIT_VARS columnShift
  inline void ZISTREAM_INIT_AND_OPEN()
  {
    ZSTREAM_INIT();
    ZISTREAM_INIT_VARS();
    openForRead();
  }

  // Does not get undef'd, used in Zstream.cxx
  //! Hide the name of this initialization function
#define ZISTREAM_READ_MAGIC_HEADER rewindPage
  void ZISTREAM_READ_MAGIC_HEADER();

  //! Peek at the next byte in the buffer
  /*!
    Undefined if eof is encountered or if the stream is in a bad
    state. It will return *something* in that case, but not guaranteed
    to be any particular char.
  */
  char peek();
  
  //! Returns true if EOF encountered
  bool eof() const;

  //! Returns true if the file descriptor is at the end of file
  /*!
    eof() returns true if the data being read is the end of the
    file. This returns true if the last read from the actual file
    descriptor encountered a low level end of file.
  */
  bool fileEof() const;

  //! Read n bytes into buf
  /*!
    \param buf pointer to target storage
    \param n Number of bytes to read and place into buf
    \returns Number of bytes actually read. If an error occurs 0 is
    returned and fail() will be true. If EOF is encountered, eof()
    will be true
  */
  UInt32 read(char* buf, UInt32 n);

  //! Skip numChars in the stream
  /*!
    \param numChars number of characters to skip
    \returns Actual number of characters skipped
  */
  UInt32 skip(UInt32 numChars);
  
  //! Read until carriage return
  /*!
    \param strBuf pointer to an STL string. The UtString will be
    cleared and filled with the line of information
    \returns Number of characters read
  */
  UInt32 readline(UtString* strBuf);

  //! Set the file byte hard stop
  /*!
    This is really a hook for ZstreamZip. This allows you to tell the
    underlying low-level read to consider that the given position is
    the end of the file.
  */
  void putHardStop(SInt64 fdPos);
  
  //! Set the uncompressed file buffer stop
  /*!
    This is really a hook for ZistreamEntry. This allows you to put
    an end-of-file at the uncompressed position. This is not like
    putHardStop() which stops the file descriptor from going any
    further. Here, the file descriptor can be past the given position,
    but a read will stop after reading this many bytes.
  */
  void putBufferStop(SInt64 totalBytes);

  //! Returns true if the buffer is compressed
  bool isCompressed() const;

  //! read UtString operator
  /*!
    This reads up until the next whitespace
  */
  virtual Zistream& operator>> (UtString& str);  

  //! char read
  virtual Zistream& operator>> (char& c);  
  //! unsigned char read
  Zistream& operator>> (unsigned char& c);  
  //! signed char read
  Zistream& operator>> (signed char& c);  
  
  //! int read
  /*!
    \warning This is not architecture safe.
  */
  //! double read
  virtual Zistream& operator>> (double& num);
  
  //! SInt32 read
  virtual Zistream& operator>> (SInt32& num);  
  //! UInt32 read
  virtual Zistream& operator>> (UInt32& num);    
  //! SInt16 read
  virtual Zistream& operator>> (SInt16& num);    
  //! UInt16 read
  virtual Zistream& operator>> (UInt16& num);    
  //! UInt64 read
  virtual Zistream& operator>> (UInt64& num);    
  //! SInt64 read
  virtual Zistream& operator>> (SInt64& num);    

  //! UtStringArray
  virtual Zistream& operator>> (UtStringArray &);

  //! where am I about to read, in terms of uncompressed bytes?
  SInt64 tell() {return mReadPos;}

  //! Unsets eof before calling Zstream::seek()
  virtual void seekHelper(SInt64 pos);

  //! set eof after calling Zstream::seekEnd()
  virtual void seekEnd();
  
  //! DO NOT USE
  /*!
    \internal
    Temporarily made public for ZistreamZip.
  */
  virtual int safeRead(char* fileBuf, UInt32 size, int* numRead);

  //! Flush and fill the file buffer
  /*!
    \warning Only use if you know what you are doing. 
    \warning This flushes any data that is in the file buffer, so be
    sure to process or save any data that is currently in the file
    buffer before calling this. 
    \warning If this file is a compressed file calling this will
    corrupt the data. For compressed files the internal mechanics must
    deal with filling the file buffer.

    This is needed for applications that are reading uncompressed data
    directly from the UtFileBuf.
  */
  void fillFileBuf();

  //! Get the file buffer
  /*!
    \warning The file must \e NOT be encrypted/compressed!    
    
    If the file is encrypted, this will assert. Otherwise, this
    returns the file buffer. This is meant for fast access to
    unencrypted data.

    \sa isCompressed
  */
  UtFileBuf* getFileBuf();

  //! Get the data buffer
  /*!
    \warning Only use if you know what you are doing. Incorrect usage
    with encrypted data files will cause data corruption.
    
    This gives access to the data buffer directly.

    One scenario in which to use this is if you are using Zistream as
    just a file descriptor holder for an unencrypted file. You can use
    the file buffer directly using getFileBuf() and you can deal with
    refilling the file buffer by putting left over data from the file
    buf into the databuf before refilling the file buffer.
  */
  UtFileBuf* getDataBuf();

protected:
  //! The file descriptor
  UtIStream* mFile;  

  //! boolean for the absolute end of the file and the buffer
  bool mEof;
  //! boolean for just the end of the file
  bool mFileEof;

  //! boolean for determining compressed v. uncompressed
  bool mCompressed;

  //! Do we own the UtIStream* buffer?
  bool mOwnFile;
  
  //! Destructive skip spaces 
  /*!
    The next non space byte will be put into byte
  */
  void skipSpaces(char* byte);

  //! Non-destructive skip spaces
  /*!
    This skips spaces, but does not read in the next non space byte
  */
  void skipSpacesPeek();

  //! Special constructor for unowned file descriptors
  /*!
    \param name Name of the input stream
    \param hardStop Should be set to the number of actual bytes this file is
    sized (same number that a file stat would return).
    \param srcStream The source stream from which to get the file descriptor.
  */
  Zistream(const char* name, SInt64 hardStop, Zistream* srcStream);

  //! Opens file for read. Does nothing for ZiStringStream
  virtual void openForRead();

private:
  void ZISTREAM_INIT_VARS();

  void fillReadBuf();
  UInt32 rawReadFileBuf(char* buf, UInt32 n);
  
  
  //! Non-sensical name to activate/deactive ciphers
#define ZISTREAM_CIPHER_ACTIVATE bufferDivide
  void ZISTREAM_CIPHER_ACTIVATE(void* cipher, UInt32 activate);

  //! Non-sensical name to enable back compatibility 
#define ZISTREAM_CIPHER_VERSION_SETUP bufferSubtract
  void ZISTREAM_CIPHER_VERSION_SETUP(void* cipherOpaque, UInt32 version);
  
  //! Read the next 4 bytes into a UInt32
  /*!
    This is the partner for Zostream::writeRawUInt32
  */
  void readRawUInt32(UInt32* n);


  void fillForNumConversion(UtString* buffer, bool nonInteger=false);

  // forbid
  Zistream();
  Zistream(const Zistream&);
  Zistream& operator=(const Zistream&);

  SInt64 mReadPos;
  SInt64 mHardStop;
  SInt64 mBufferStop;
};

//! Compressed output stream suitable for DB writing
/*!
  This will write the binary forms of numbers out to disk instead of
  the ASCII form. All fixed sized integers get written out in
  big-endian order.

  \warning  ints and long doubles are not allowed. This class will
  assert on ints and no operator << is defined for long doubles.
*/
class ZostreamDB : public Zostream
{
public: 
  CARBONMEM_OVERRIDES
  //! constructor
#if 0
  ZostreamDB() :
    Zostream()
  {
    initDB();
  }
#endif

  //! opening constructor
  /*!
    \param name name of file to open
  */
  ZostreamDB(const char* name, void* key);


  //! virtual destructor
  virtual ~ZostreamDB();

  //! const char write operator
  /*!
    int and char are synonymous unless explicitly handled
  */
  Zostream& operator<< (const char c);

  //! double write operator
  virtual Zostream& operator<< (double num);

  //! Signed int write operator
  virtual Zostream& operator<< (SInt32 num);  
  //! Unsigned int write operator
  virtual Zostream& operator<< (UInt32 num);    
  //! Signed short write operator
  virtual Zostream& operator<< (SInt16 num);    
  //! Unsigned short write operator
  virtual Zostream& operator<< (UInt16 num);    
  //! unsigned long long operator
  virtual Zostream& operator<< (UInt64 num);    
  //! signed long long operator
  virtual Zostream& operator<< (SInt64 num);    
  //! const char* -- must be read as a string
  virtual Zostream& operator<< (const char* val);    
  //! string
  virtual Zostream& operator<< (const UtString& val);
  //! UtStringArray
  virtual Zostream& operator<< (const UtStringArray &strings);

  //! object -- must come before a pointer reference.  Calls T::dbWrite(Zostream&)
  template<typename T> bool writeObject(const T& obj)
  {
    const void* ptr = &obj;
    INFO_ASSERT(! isMapped(ptr), "Invalid write of an object.");

    if (!obj.dbWrite(*this))
      return false;
    
    mapPtr(ptr);
    return true;
  }

  //! write a null pointer -- used for terminating containers of pointers
  bool writeNull() {
    bool ret = (*this) << ((UInt32) 0);
    return ret;
  }

  //! Returns true if the pointer has been stored as an object
  bool isMapped(const void* ptr) const;

  //! pointer -- must already have stored the object.
  template<typename T> bool writePointer(const T* ptr)
  {
    PtrToNum::iterator ptrIndex = mPtrToIndex.find(ptr);
    if (ptrIndex == mPtrToIndex.end())
    {
      fprintf(stderr, "DB Write of a pointer, %lx, that has not yet been stored as an object\n",
              (unsigned long) ptr);
      abort();
    }
    UInt32 index = ptrIndex->second;
    bool ret = (*this) << index; // store an index when an object is referenced
    return ret;
  }

  //! Test whether a pointer was mapped
  template <typename T> bool isMapped (const T *ptr) const
  {
    return mPtrToIndex.find (ptr) != mPtrToIndex.end ();
  }

  //! map a pointer that was stored in the database without operator<<
  void mapPtr(const void* ptr);

  //! Debug function to get the current size of the ptr map
  UInt32 getMapSize() const;

/*
  nasty stuff -- hope you never have to use these:

  template<class __Container, class __Loop>
  bool writeObjectContainer(const __Container& container, __Loop loop)
  {
    UInt32 num = container.size();
    if (!(*this << num))
      return false;
    const typename __Container::value_type* ptr;
    while (loop(&ptr))
      if (!writeObject(*ptr))
        return false;
    return true;
  }

  template<class __Container>
  bool writeObjectContainer(const __Container& container)
  {
    return writeObjectContainer(container, Loop<const __Container>(container));
  }
*/

  //! Template function for writing a container of pointers
  template<typename __Container>
  bool writePointerContainer(const __Container& container)
  {
    UInt32 num = container.size();
    if (!(*this << num))
      return false;
    for (CLoop<__Container> p = CLoop<__Container>(container);
         !p.atEnd(); ++p)
      if (!writePointer(*p))
        return false;
    return true;
  }

  //! Template function for writing a Loopable container of objects
  template<typename __Container, typename __Loop>
  bool writePointerValueContainer(const __Container& container,
                                  __Loop p)
  {
    UInt32 num = container.size();
    if (!(*this << num))
      return false;
    for (UInt32 i = 0; !p.atEnd(); ++p, ++i)
    {
      //! Define to debug general Zstream issues
#if ZDB_DEBUG
      UtIO::cout() << i << ": " << *p << " ";
      (*p)->print();
      UtIO::cout() << UtIO::endl;
#endif
      if (!writeObject(**p))
        return false;
    }
    return true;
  }

  //! Template for writing a standard container of objects
  template<typename __Container>
  bool writePointerValueContainer(const __Container& container)
  {
    return writePointerValueContainer(container,
                      CLoop<__Container>(container));
  }

protected:
  //! Special constructor for ZostreamZip database entries
  ZostreamDB(const char* name, Zostream* strStream);

private:
  void initDB();
  typedef UtHashMap<const void*, UInt32> PtrToNum;
  PtrToNum mPtrToIndex;

  // forbid
  Zostream& operator<< (const unsigned char c);  
  Zostream& operator<< (const signed char c);  

  ZostreamDB();
  ZostreamDB(const ZostreamDB&);
  ZostreamDB& operator=(const ZostreamDB&);
};

//! Compressed input stream suitable for DB reading
/*!
  This will enable reading of files that were written with ZostreamDB.
  All std::fixedsized integers to be read are expected to be in big-endian
  order.
  \warning  ints and long doubles are not allowed. This class will
  assert on ints and no operator << is defined for long doubles.
*/
class ZistreamDB : public Zistream
{
public: 
  CARBONMEM_OVERRIDES
  
  //! file-opening constructor 
  ZistreamDB(const char* name, void* key);

  //! string-buffer-based constructor 
  ZistreamDB(size_t sz, const char* buf, void* key);

  //! virtual destructor
  virtual ~ZistreamDB();

  //! const char read operator
  /*!
    int and char are synonymous unless explicitly handled
  */
  Zistream& operator>> (char& c);

  //! signed char
  Zistream& operator>> (signed char& c);

  //! unsigned char
  Zistream& operator>> (unsigned char& c);

  //! int
  //  virtual Zistream& operator>> (int& num);
  //! double
  virtual Zistream& operator>> (double& num);
  //! SInt32
  virtual Zistream& operator>> (SInt32& num);  
  //! UInt32
  virtual Zistream& operator>> (UInt32& num);    
  //! SInt16
  virtual Zistream& operator>> (SInt16& num);    
  //! UInt16
  virtual Zistream& operator>> (UInt16& num);    
  //! UInt64
  virtual Zistream& operator>> (UInt64& num);    
  //! SInt64
  virtual Zistream& operator>> (SInt64& num);    
  //! string
  virtual Zistream& operator>> (UtString& val);

  //! UtStringArray
  virtual Zistream& operator>> (UtStringArray &);

  //! Calls static function T::dbRead(Zistream&).
  /*!
    This method is used by classes that implement static dbRead() methods. They
    do so to allow creation of appropriate derivation of the class depending on
    the type read from the file. The dbRead() method returns class object if
    successful or NULL.

    NOTE: THIS METHOD DOES NOT MAP POINTER TO INDEX. CALL mapPtr() METHOD EXPLICITLY.
  */
  template<typename T> bool readObject(T** obj)
  {
    *obj = T::dbRead(*this);
    if (!*obj) {
      return false;
    }
    return true;
  }

  //! object -- must come before a pointer reference.  Calls T::dbRead(Zistream&)
  template<typename T> bool readObject(T* obj)
  {
    if (!obj->dbRead(*this))
      return false;
    mapPtr(obj);
    return true;
  }

  //! pointer
  template<typename T> bool readPointer(T** ptr)
  {
    void* rawPtr;
    if (readRawPointer(&rawPtr))
    {
      //      *ptr = (T*) rawPtr;
      *ptr = reinterpret_cast<T*>(rawPtr);
      return true;
    }
    return false;
  }
  
  //! Read a raw pointer (underlying implementation)
  bool readRawPointer(void** rawptr);

  //! map a pointer that was stored in the database without operator<<
  /*!
    The ptr argument is const in order to fool the compiler when
    reading in sets of constant objects. The underlying implementation
    const casts it to void*
  */
  void mapPtr(const void* ptr);

  //! Debug function to get the current size of the ptr map
  UInt32 getMapSize() const;

/*
  template<class __Container>
  bool readObjectContainer(__Container* container,
                           typename __Container::iterator inserter)
  {
    UInt32 num;
    if (!(*this >> num))
      return false;
    for (UInt32 i = 0; i < num; ++i)
    {
      typename __Container::value_type obj;
      if (!readObject(&obj))
        return false;
      container->insert(inserter, obj);
    }
    return true;
  }

  template<class __Set>
  bool readObjectContainer(__Set* container)
  {
    UInt32 num;
    if (!(*this >> num))
      return false;
    for (UInt32 i = 0; i < num; ++i)
    {
      typename __Set::value_type obj;
      if (!obj.dbRead(*this))
        return false;
      std::pair<typename __Set::iterator,bool> p = container->insert(obj);
      const typename __Set::value_type& ptr = *p.first;
      mapPtr(const_cast<typename __Set::value_type*>(&ptr));
    }
    return true;
  }
*/

  //! Read a container of pointers
  template<typename __Set>
  bool readPointerContainer(__Set* container)
  {
    UInt32 num;
    if (!(*this >> num))
      return false;
    for (UInt32 i = 0; i < num; ++i)
    {
      typename __Set::value_type ptr = NULL;
      if (!readPointer(&ptr))
        return false;
      container->insert(ptr);
    }
    return true;
  }

  //! Read a pointers into a vector
  template<typename __Vector>
  bool readPointerContainerVector(__Vector* container)
  {
    UInt32 num;
    if (!(*this >> num))
      return false;
    container->reserve(num);
    for (UInt32 i = 0; i < num; ++i)
    {
      typename __Vector::value_type ptr = NULL;
      if (!readPointer(&ptr))
        return false;
      container->push_back(ptr);
    }
    return true;
  }

  //! Read a container of objects into a list
  template<typename __List, typename __Object>
  bool readPointerValueContainerList(__List* container, __Object&)
  {
    UInt32 num;
    if (!(*this >> num))
      return false;
    for (UInt32 i = 0; i < num; ++i)
    {
      typename __List::value_type ptr = new __Object;
      if (! readObject(ptr))
        return false;
      container->push_back(ptr);
#if ZDB_DEBUG
      UtIO::cout() << i << ": " << *p << " ";
      ptr->print();
      UtIO::cout() << UtIO::endl;
#endif
    }
    return true;
  }
  
  //! Read a container of objects
  /*!
    This method uses static dbRead() methods to create objects
    instead of new'ing them.
  */
  template<typename __Set>
  bool readPointerValueContainer(__Set* container, bool uniqInserts = true)
  {
    UInt32 num;
    if (!(*this >> num))
      return false;
    for (UInt32 i = 0; i < num; ++i)
    {
      typename __Set::value_type ptr = NULL;
      if (! readObject(&ptr)) {
        return false;
      }

#if ZDB_DEBUG
      UtIO::cout() << i << ": " << ptr << " ";
      ptr->print();
      UtIO::cout() << UtIO::endl;
#endif

      std::pair<typename __Set::iterator, bool> insertStat = container->insert(ptr);
      if (! insertStat.second)
      {
        if (uniqInserts)
          INFO_ASSERT(insertStat.second, "Object read is not unique.");
        else
        {
          delete ptr;
          ptr = *(insertStat.first);
        }
      }
     
      // When reading always map the pointer even if we may have
      // inserted it already. This is for appendable objects (objects
      // that can dbRead more than once.
      mapPtr(ptr);
    }
    
    if (uniqInserts)
      INFO_ASSERT(container->size() == num, "Inconsistent object container."); // verifies insertions were unique
    return true;
  }

  //! Read a container of objects
  template<typename __Set, typename __Object>
  bool readPointerValueContainer(__Set* container, __Object&, bool uniqInserts = true)
  {
    UInt32 num;
    if (!(*this >> num))
      return false;
    for (UInt32 i = 0; i < num; ++i)
    {
      typename __Set::value_type ptr = new __Object;
      if (! ptr->dbRead(*this))
        return false;

#if ZDB_DEBUG
      UtIO::cout() << i << ": " << ptr << " ";
      ptr->print();
      UtIO::cout() << UtIO::endl;
#endif

      std::pair<typename __Set::iterator, bool> insertStat = container->insert(ptr);
      if (! insertStat.second)
      {
        if (uniqInserts)
          INFO_ASSERT(insertStat.second, "Object read is not unique.");
        else
        {
          delete ptr;
          ptr = *(insertStat.first);
        }
      }
     
      // When reading always map the pointer even if we may have
      // inserted it already. This is for appendable objects (objects
      // that can dbRead more than once.
      mapPtr(ptr);
    }
    
    if (uniqInserts)
      INFO_ASSERT(container->size() == num, "Inconsistent object container."); // verifies insertions were unique
    return true;
  }


  //! read a UtString from the database, expecting it to exactly match a token, returning false if it does not.  Does not crash if the next object is not a string
  bool expect(const char* token);

protected:
  //! For use by ZistreamZip's db entry access mechanism
  ZistreamDB(const char* name, SInt64 hardStop, Zistream* srcStream);

private:

#define ZISTREAMDB_INIT columnShift
  void ZISTREAMDB_INIT();
  typedef UtArray<void*> VoidVec;
  VoidVec* mIndexToPtr;

  ZistreamDB();
  ZistreamDB(const ZistreamDB&);
  ZistreamDB& operator=(const ZistreamDB&);

};

#define ZSTREAM_KEY (void*) 31

#define ZISTREAM_INIT(var) \
  var.ZISTREAM_INIT_AND_OPEN(); \
  var.ZISTREAM_READ_MAGIC_HEADER()

#define ZISTREAM_PTR_INIT(var) \
  var->ZISTREAM_INIT_AND_OPEN(); \
  var->ZISTREAM_READ_MAGIC_HEADER()

//! Zistream var(name)
#define ZISTREAM(var, name) \
  Zistream var (name, ZSTREAM_KEY); \
  ZISTREAM_INIT(var)

//! var = new Zistream(name)
#define ZISTREAM_ALLOC(var, name) \
  var = new Zistream(name, ZSTREAM_KEY); \
  ZISTREAM_PTR_INIT(var)

//! ZistreamDB var(name)
#define ZISTREAMDB(var, name) \
  ZistreamDB var (name, ZSTREAM_KEY); \
  ZISTREAM_INIT(var)

//! var = new ZistreamDB(name)
#define ZISTREAMDB_ALLOC(var, name) \
  var = new ZistreamDB(name, ZSTREAM_KEY); \
  ZISTREAM_PTR_INIT(var)


#define ZOSTREAM_INIT(var) \
  var.ZOSTREAM_INIT_AND_OPEN(); \
  var.ZOSTREAM_WRITE_MAGIC_HEADER()

#define ZOSTREAM_PTR_INIT(var) \
  var->ZOSTREAM_INIT_AND_OPEN(); \
  var->ZOSTREAM_WRITE_MAGIC_HEADER()

//! Zostream var(name)
#define ZOSTREAM(var, name) \
  Zostream var (name, ZSTREAM_KEY); \
  ZOSTREAM_INIT(var)

//! var = new Zostream(name)
#define ZOSTREAM_ALLOC(var, name) \
  var = new Zostream(name, ZSTREAM_KEY); \
  ZOSTREAM_PTR_INIT(var)

//! ZoStringStream var(name)
#define ZOSTRINGSTREAM(var, buffer) \
  ZoStringStream var (buffer, ZSTREAM_KEY); \
  ZOSTREAM_INIT(var)

//! var = new ZoStringStream(name)
#define ZOSTRINGSTREAM_ALLOC(var, buffer) \
  var = new ZoStringStream(buffer, ZSTREAM_KEY); \
  ZOSTREAM_PTR_INIT(var)

//! ZostreamDB var(name)
#define ZOSTREAMDB(var, name) \
  ZostreamDB var (name, ZSTREAM_KEY); \
  ZOSTREAM_INIT(var)

//! var = new ZostreamDB(name)
#define ZOSTREAMDB_ALLOC(var, name) \
  var = new ZostreamDB(name, ZSTREAM_KEY); \
  ZOSTREAM_PTR_INIT(var)

//! encrypt a string
/*!
 *! This is done as a macro to make it harder to disassemble and
 *! find the entry points in our libraries that allow decryption.
 *!
 *!   \param in const char* or const UtString&
 *!   \param out UtString*
 */
#define ZENCRYPT_STRING(in, out) {   \
  ZOSTRINGSTREAM(encrypter, (out));  \
  encrypter << (in);                 \
  encrypter.flush();                 \
}
  
//! decrypt a string
/*!
 *! This is done as a macro to make it harder to disassemble and
 *! find the entry points in our libraries that allow decryption.
 *!
 *!   \param in const char* or const UtString&
 *!   \param out UtString*
 */
#define ZDECRYPT_STRING(in, out) {               \
  UtIStringStream is(in);                        \
  ZISTREAM(decrypter, &is);                      \
  char c[100];                                   \
  UInt32 sz;                                     \
  while ((sz = decrypter.read(c, 100)) > 0) {    \
    (out)->append(c, sz);                        \
  }                                              \
}

#endif
