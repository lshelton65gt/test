// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _UT_ORDER_H_
#define _UT_ORDER_H_

#include <functional>

//! template function used for safely comparing pointer values
template <typename T>
int carbonPtrCompare(const T* p1, const T* p2)
{
  if (p1 < p2)
    return -1;
  else if (p1 > p2)
    return +1;
  else
    return 0;
}

//! template class for making comparison classes that dereference the keys before comparison
template<class _DerefKey, class _Cmp=std::less<_DerefKey> >
struct DerefOrder
{
  bool operator()(const _DerefKey* const & k1, const _DerefKey* const & k2) const
  {
    if ((k1 == NULL) && (k2 != NULL))
      return true;
    if (k2 == NULL)
      return false;
    _Cmp cmp;
    return cmp(*k1,*k2);
  }
};

//! template class for making comparison classes for ordering sets
template<class _Set>
struct SetOrder
{
  bool operator()(const _Set& s1, const _Set& s2) const
  {
    typename _Set::const_iterator p1 = s1.begin();
    typename _Set::const_iterator p2 = s2.begin();
    while (true)
    {
      // check for termination
      if (p1 == s1.end() && p2 == s2.end())
        return false;
      if (p1 == s1.end())
        return true;
      if (p2 == s1.end())
        return false;
      
      // compare current values
      const typename _Set::value_type& v1 = *p1;
      const typename _Set::value_type& v2 = *p2;
      if (v1 < v2)
        return true;
      if (v2 < v1)
        return false;
      
      // advance
      ++p1;
      ++p2;
    }
    return false; // unreachable
  }
};

#endif // _UT_ORDER_H_
