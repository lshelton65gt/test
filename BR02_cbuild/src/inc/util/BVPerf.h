// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __BVPerf_h_
#define __BVPerf_h_
#ifndef __CarbonTypes_h_

#include "util/CarbonTypes.h"
#endif
#ifndef __UtHashMap_h_
#include "util/UtHashMap.h"
#endif

// information about interesting call types.  There is a single instance
// of a BVPerfData object per BitVector<T> for a given size T

class BVPerfData {
public: CARBONMEM_OVERRIDES
  BVPerfData (): mCopy (0), mDefault (0),
		 mArray (0), mBVRefs (0), mAnyTo (0), mAnyFrom (0), mInt (0),
                 mRefEqI64(0), 
                 mRefEqI32(0),	
                 mRefEqBV(0),	
                 mRefPlusEq32(0),	
                 mRefMinusEq32(0),	
                 mRefOrEq32(0),	
                 mRefXOrEq32(0),	
                 mRefAndEq32(0),	
                 mRefL(0),	        
                 mRefLL(0),	        
                 mRef2EqRefUA(0),	
                 mRefPlusEqRef(0),	
                 mRefMinusEqRef(0),	
                 mRefOrEqRef(0),	
                 mRefXOrEqRef(0),	
                 mRefAndEqRef(0),
                 mBVEqRef(0),
                 mBVEqBVK(0)
  {}

  UInt32	mCopy; // count BitVector<T> x(y&);
  UInt32	mDefault; // count BitVector<T> x;
  UInt32	mArray; // count BitVector<T> x(array,len);
  UInt32	mBVRefs;		// count bitref from bitvector
  // Following two should have same values unless we do different src/dest
  // sized copies.
  UInt32	mAnyTo;		// count bvref copys to field of size T
  UInt32	mAnyFrom;	// count bvref copys from field of size T
  UInt32	mInt;		// BitVector<T> x(u32);
  UInt32	mRefEqI64;	// BVRef op=u64;
  UInt32	mRefEqI32;	// BVRef op=u32;
  UInt32	mRefEqBV;	// BVRef op=BV, copy from BV;
  UInt32	mRefPlusEq32;	// BVRef op+=u32
  UInt32	mRefMinusEq32;	// BVRef op-=u32
  UInt32	mRefOrEq32;	// BVRef op|=u32
  UInt32	mRefXOrEq32;	// BVRef op^=u32
  UInt32	mRefAndEq32;	// BVRef op&=u32
  UInt32	mRefL;	        // BVRef value()
  UInt32	mRefLL;	        // BVRef llvalue()
  UInt32	mRef2EqRefUA;	// BVRef ==BVRef unaligned
  UInt32	mRefPlusEqRef;	// BVRef op+=BVRef
  UInt32	mRefMinusEqRef;	// BVRef op-=BVRef
  UInt32	mRefOrEqRef;	// BVRef op|=BVRef
  UInt32	mRefXOrEqRef;	// BVRef op^=BVRef
  UInt32	mRefAndEqRef;	// BVRef op&=BVRef
  UInt32	mBVEqRef; // BV op=BVRef
  UInt32	mBVEqBVK; // BV op=BitVector<K> where K != _Nb
};

typedef UtHashMap<UInt32, BVPerfData*> BVStatsMapT;

// Only create ONE instance of this object and have a global pointer to it
class BVPerf {
  BVStatsMapT	mPerfData;

  // Check for existance of map entry for size
  BVPerfData* check (UInt32 size);

public: CARBONMEM_OVERRIDES
  BVPerf (){}
  ~BVPerf ();		// Must clean up per size counters
  void countCopy (UInt32 size);
  void countDefault (UInt32 size);
  void countArray (UInt32 size);
  void countBVRefs (UInt32 size);
  void countAnyTo (UInt32 size);
  void countAnyFrom (UInt32 size);
  void countInt (UInt32 size);
  void countRefEqI64 (UInt32 size);
  void countRefEqI32 (UInt32 size);
  void countRefEq (UInt32 size, bool is64) {
    if (is64) countRefEqI64 (size); else countRefEqI32 (size); }
  void countRefEqBV (UInt32 size);
  void countRefPlusEq32 (UInt32 size);
  void countRefMinusEq32 (UInt32 size);
  void countRefOrEq32 (UInt32 size);
  void countRefXOrEq32 (UInt32 size);
  void countRefAndEq32 (UInt32 size);
  void countRefL (UInt32 size);
  void countRefLL (UInt32 size);
  void countRef2EqRefUA (UInt32 size);
  void countRefPlusEqRef (UInt32 size);
  void countRefMinusEqRef (UInt32 size);
  void countRefOrEqRef (UInt32 size);
  void countRefXOrEqRef (UInt32 size);
  void countRefAndEqRef (UInt32 size);
  void countBVEqRef (UInt32 size);
  void countBVEqBVK (UInt32 size);

  //! Reset all counters to zero (ideally after constructing simulation object)
  void resetAll ();

  //! Print all counters
  void print (void) const; 
private:
  BVPerf (const BVPerf&) {};	// Don't allow copies to be made
};

// There's a single GLOBAL instance of this class pointed to by BVPC
extern BVPerf* BVPC;

#endif

