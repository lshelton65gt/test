// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtStack_h_
#define __UtStack_h_
#ifndef __MemManager_h_

#include "util/MemManager.h"
#endif

#ifndef __UtDeque_h_
#include "util/UtDeque.h"
#endif
#include <stack>

//! alternate STL stack definition that uses the Carbon memory allocator
template<class _Tp, class _Sequence>
class UtStack: public std::stack<_Tp, _Sequence>
{
  typedef std::stack<_Tp, _Sequence > _Base;
public: CARBONMEM_OVERRIDES
  UtStack() {}
};

#endif
