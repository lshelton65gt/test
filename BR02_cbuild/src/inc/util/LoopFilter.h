// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _LOOPFILTER_H_
#define _LOOPFILTER_H_


#include "util/StlIterator.h"

//! LoopFilter template class
/*!
  This class provides a mechanism for a class to expose iteration over
  its STL containers, where only a subset of the items are part of the
  iteration.
 
  A functor class is used as a predicate to determine membership in the
  iteration.  

  The Predicate class must define a method

  \verbatim
     bool operator()(value_type);
  \endverbatim

  which will return true if the specified value should be included in the
  iteration.
*/

template <class _LoopType, class _Predicate>
class LoopFilter
{
public: CARBONMEM_OVERRIDES
  typedef typename _LoopType::value_type value_type;
  typedef typename _LoopType::reference reference;

  LoopFilter()
  {
    mEmpty = true;
  }

  //! Constructor for use when the predicate must be explicitly constructed
  LoopFilter(_LoopType loop, _Predicate predicate):
    mLoop(loop),
    mPredicate(predicate)
  {
    mEmpty = false;
    nextNode();
  }

  //! Constructor for use when the predicate's explicit constructor will suffice
  LoopFilter(_LoopType loop):
    mLoop(loop)
  {
    mEmpty = false;
    nextNode();
  }

  //! Copy constructor to help the user manage & pass around Loops.
  LoopFilter(const LoopFilter& src): mLoop(src.mLoop), mPredicate(src.mPredicate)
  {
    mEmpty = src.mEmpty;
  }

  //! Assignment operator to help the user manage & pass around Loops.
  LoopFilter& operator=(const LoopFilter& src)
  {
    if (&src != this)
    {
      mLoop = src.mLoop;
      mEmpty = src.mEmpty;
      mPredicate = src.mPredicate;
    }
    return *this;
  }

  void operator++()
  {
    INFO_ASSERT(!mEmpty, "Loop not initialized.");
    ++mLoop;
    nextNode();
  }

  reference operator*() const
  {
    INFO_ASSERT(!mEmpty, "Loop not initialized.");
    return *mLoop;
  }

  bool atEnd() const
  {
    return mEmpty;
  }

  bool operator()(value_type* ptr)
  {
    if (atEnd())
      return false;
    *ptr = *mLoop;
    ++(*this);
    return true;
  }


  typedef StlIterator<LoopFilter<_LoopType, _Predicate> > iterator;

  //! The start of the loop.
  iterator begin()
  {
    return iterator(this);
  }

  //! The tail of the loop
  iterator end()
  {
    return iterator(NULL);
  }

  //! Return the number of elements remaining -- note:  order N operation
  size_t size() const
  {
    size_t s = 0;
    for (LoopFilter p(*this); !p.atEnd(); ++p)
      ++s;
    return s;
  }

private:
  void nextNode()
  {
    while (!mLoop.atEnd())
    {
      if (mPredicate(*mLoop))
        return;
      ++mLoop;
    }
    mEmpty = true;
  }

  bool mEmpty;
  _LoopType mLoop;
  _Predicate mPredicate;
}; // class LoopFilter

#endif //  _LOOPFILTER_H_
