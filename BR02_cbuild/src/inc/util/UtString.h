// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtString_h_
#define __UtString_h_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#ifndef  __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif

#ifndef __Util_h_
#include "util/Util.h"
#endif
#ifndef __OSWrapper_h_
#include "util/OSWrapper.h"
#endif

class UtStringI;
class UtString;

#include <cstring>		// Get strchr()
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#include <cstdio>
#include <cstdlib>
#include <cctype>               // for tolower

class UtStringArray;
class ZostreamDB;
class ZistreamDB;

//! alternate STL string definition that does not depend on SGI implementation
/*!
 *! This implementation does not pull in exception handling, which makes it
 *! easier to link across C++ compiler versions.  It is call-compatible with
 *! STL string, and regresses the same as SGI's implementation, except
 *! that it asserts where the standard says to raise an exception, and it
 *! lacks reverse iterators.
 */
class UtString
{
public: CARBONMEM_OVERRIDES
  typedef char value_type;
  typedef char* pointer;
  typedef char& reference;
  typedef const char& const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;
  typedef char* iterator;
  typedef const char* const_iterator;
  static const size_t npos = static_cast<size_t>(-1);

  iterator begin()                 {return mBuffer;}
  iterator end()                   {return mBuffer + mSize;}
  const_iterator begin() const     {return mBuffer;}
  const_iterator end() const       {return mBuffer + mSize;}

  //! This isn't really an iterator, but if you want the last character
  //! in the string it will do.
  char* rbegin() {char* p = end(); return p - 1;}
  const char* rbegin() const {const char* p = end(); return p - 1;}

  //! get number of characters in string
  size_t size() const               {return mSize;}
  
  //! get number of characters in string
  size_t length() const             {return mSize;}

  // get maximum possible number of characters in string
  // JDM -- not sure what this means, so I'm not implementing it
  // size_t max_size() const           {return mCapacity;}

  //! Get number of characters currently allocated for string \sa size
  size_t capacity() const           {return mCapacity;}

  //! does the string have 0 characters in it?
  bool empty() const                {return mSize == 0;}

  //! Get reference to nth character
  reference operator[](size_t n) {
    //INFO_ASSERT((n < mSize), "out of range");
    return mBuffer[n];
  }

  //! Get const reference to nth character
  const_reference operator[](size_t n) const {
    //INFO_ASSERT((n < mSize), "out of range");
    return mBuffer[n];
  }

  //! Get the internal buffer.
  /*!
    \warning The buffer is only valid for the current size of the
    string.
  */
  char* getBuffer() { return mBuffer; }

  //! Get nul-terminated C string representation
  const char* c_str() const         {return mBuffer;}

  //! Get C string representation, not necessarily nul-terminated
  /*! In the Carbon implementation this is nul-terminated, but you cannot
   *! depend on this
   */
  const char* data() const          {return mBuffer;}

  //! Remove all the characters from the string
  void clear();

  //! insert a character at given position
  void insert(iterator pos, char x) {insert(pos - mBuffer, 1, x);}

  // pfCYGWIN probably means MSVC-8.  strlen is declared in MS headers
  // outside the 'std' namespace so std::strlen causes errors.
#if pfCYGWIN
#define STRLEN strlen
#else
#define STRLEN std::strlen
#endif

  //! insert a nul-terminated C-string at given position
  UtString& insert(size_type pos, const char* s) {return insert(pos, s, STRLEN(s));}

  //! insert a string at given position
  UtString& insert(size_type pos, const UtString& s) {return insert(pos, s.mBuffer, s.mSize);}

  //! insert the first n characters of C-string 's' into position pos.
  UtString& insert(size_type pos, const char* s, size_type n);

  //! insert n copies of character c at positition pos.
  UtString& insert(size_type pos, size_type n, char c);

  //! Insert the given string range [first:last) at insertionPoint
  UtString& insert(iterator insertionPoint,
                   const_iterator first, const_iterator last);

  //! construct an empty string
  UtString();

  //! construct a string from a nul-terminated C string
  UtString(const char* str);

  //! construct a string from a substring(pos:pos+n-1) of str
  UtString(const UtString& str, size_t pos = 0, size_t n = npos);

  //! construct a string from a C string with explicit size
  UtString(const char* str, size_t size);
  
  //! construct a string from a repeated number of characters
  UtString(size_t n, char ch);

  //! copy ctor
  UtString& operator=(const UtString& src);

  //! assignment from nul-terminated C string
  UtString& operator=(const char* src) {assign(src); return *this;}

  //! dtor
  ~UtString();

  //! append a string
  UtString& operator+=(const UtString& s) {return append(s);}

  //! append a nul-terminated C-string
  UtString& operator+=(const char* s) {return append(s);}

  //! append a character
  UtString& operator+=(char c) {return append(1, c);}

  //! erase one character at an iterator
  void erase(iterator p) {erase(p - mBuffer, 1);}

  //! erase range [first,last)
  void erase(iterator first, iterator last);

  //! erase n characters starting at pos
  void erase(size_t pos = 0, size_t n = npos);

  //! append a string
  UtString& append(const UtString& s) {
    appendHelper(s.mBuffer, s.mSize, mSize);
    return *this;
  }

  //! append n characters of s starting at pos
  UtString& append(const UtString& s, size_t pos, size_t n);

  //! append nul-terminated C string
  UtString& append(const char* s);

  //! append n characters of C string
  UtString& append(const char* s, size_t n) {
    appendHelper(s, n, mSize);
    return *this;
  }

  //! append n copies of character c
  UtString& append(size_t n, char c);

  //! return the location (starting at \a pos) of the first occurrence of any one of the set of \a n characters in \a s
  size_t find_first_of(const char* s, size_t pos, size_t n) const;
  size_t find_first_not_of(const char* s, size_t pos, size_t n) const;
  size_t find_last_of(const char* s, size_t pos, size_t n) const;
  size_t find_last_not_of(const char* s, size_t pos, size_t n) const;

  size_t find_first_of(const char* s, size_t pos = 0) const {
    return find_first_of(s, pos, STRLEN(s));
  }
  size_t find_first_not_of(const char* s, size_t pos = 0) const {
    return find_first_not_of(s, pos, STRLEN(s));
  }    
  size_t find_first_of(const UtString& s, size_t pos = 0) const {
    return find_first_of(s.mBuffer, pos, s.mSize);
  }
  size_t find_first_not_of(const UtString& s, size_t pos = 0) const {
    return find_first_not_of(s.mBuffer, pos, s.mSize);
  }
  size_t find_first_of(char c, size_t pos = 0) const {
    return find_first_of(&c, pos, 1);
  }
  size_t find_first_not_of(char c, size_t pos = 0) const {
    return find_first_not_of(&c, pos, 1);
  }    

  size_t find_last_of(const char* s, size_t pos = npos) const {
    return find_last_of(s, pos, STRLEN(s));
  }
  size_t find_last_not_of(const char* s, size_t pos = npos) const {
    return find_last_not_of(s, pos, STRLEN(s));
  }    
  size_t find_last_of(const UtString& s, size_t pos = npos) const {
    return find_last_of(s.mBuffer, pos, s.mSize);
  }
  size_t find_last_not_of(const UtString& s, size_t pos = npos) const {
    return find_last_not_of(s.mBuffer, pos, s.mSize);
  }
  size_t find_last_of(char c, size_t pos = npos) const {
    return find_last_of(&c, pos, 1);
  }
  size_t find_last_not_of(char c, size_t pos = npos) const {
    return find_last_not_of(&c, pos, 1);
  }    


  int compare(const UtString& s) const;
/*
  int compare(size_t pos, size_t n, const UtString& s) const;
  int compare(size_t pos, size_t n, const UtString& s,
              size_t pos1, size_t n1) const;
  int compare(const char* s) const;
  int compare(size_t pos, size_t n, const char* s, size_t len = npos) const;
*/

  //! Get  enough space for 'size' bytes
  void reserve(size_t size);

  //! set the string length (may leave data uninitialized)
  void resize(size_t);

  //! resize the string, but initialized any new characters with c
  void resize(size_t n, char c);

  size_t rfind(char, size_t pos = npos) const;
  size_t find(char, size_t = 0) const;
  size_t find(const char*, size_t pos, size_t n) const;
  size_t find(const char* s, size_t pos = 0) const {
    return find(s, pos, STRLEN(s));
  }
  size_t rfind(const char* s, size_t = npos) const {
    return rfind(s, npos, STRLEN(s));
  }
  size_t rfind(const char* s, size_t pos, size_t n) const;
  size_t find(UtString& s, size_t pos = 0) const {
    return find(s.mBuffer, pos, s.mSize);
  }
  size_t rfind(UtString& s, size_t pos = npos) const {
    return rfind(s.mBuffer, pos, s.mSize);
  }

  void assign(const UtString& s, size_t pos, size_t n) {assign(s.mBuffer + pos, n);}
  void assign(const char* s, size_t n) {appendHelper(s, n, 0);}
  void assign(const char* s) {assign(s, STRLEN(s));}
  void assign(const UtString& s) {assign(s.mBuffer, s.mSize);}
  void assign(iterator b, iterator e);
  void assign(int repeat, char c);

  //! I suggest not using substr.  It's a badly defined function.
  //! Use append(src, pos, n).
  UtString substr(size_t pos = 0, size_t n = npos) const;

  bool operator<(const UtString& s) const {return compare(s) < 0;}
  bool operator==(const UtString& s) const {return compare(s) == 0;}
  bool operator!=(const UtString& s) const {return compare(s) != 0;}
  UtString operator+(const UtString&) const;
  UtString operator+(const char*) const;
  UtString operator+(char) const;

  void replace(size_t pos, size_t sz, const char* str);
  void replace(size_t pos, size_t sz, const UtString& str);
  void replace(iterator, iterator, char oldCh, char newCh);

  UtString& operator<<(const char* str) {return append(str);}
  UtString& operator<<(char c) {return append(1, c);}
  UtString& operator<<(const UtString& str) {return append(str);}
  UtString& operator<<(const void * ptr);
  UtString& operator<<(UInt64 num);
  UtString& operator<<(UInt32 num);
  UtString& operator<<(UInt16 num);
  UtString& operator<<(UInt8 num); // BEWARE: prints as unsigned 8-bit number
  UtString& operator<<(SInt64 num);
  UtString& operator<<(SInt32 num);
  UtString& operator<<(SInt16 num);
  // SInt8 missing because 'char' writes as character data
  UtString& operator<<(double num);
  
  //! Write this string to a db. Useful for the container writes
  bool dbWrite(ZostreamDB& outDB) const;
  //! Read this string from a db. Useful for the container reads
  bool dbRead(ZistreamDB& inDB);

  //! Hash a char*
  static size_t hash(const char* str)
  {
    const unsigned char *s = reinterpret_cast<const unsigned char*>(str);
    size_t h = 0; 
    for ( ; *s; ++s)
      h = 10*h + *s;
    return h;
  }
  
  size_t hash() const {return hash(c_str());}

  struct CmpLessRef
  {
    bool operator()(const UtString& a, const UtString& b) const
    {
      return (a.compare(b) < 0);
    }
  };

  //! Convert all alpha characters in the string to lower case
  void lowercase();
  //! Convert all alpha characters in the string to upper case
  void uppercase();

  //! Count the number of occurrences of a character
  size_t count(char c) const {return count(c_str(), c);}

  //! Count the number of occurrences of a character
  static size_t count(const char* str, char c);

private:
  void appendHelper(const char* str, size_t sz, size_t oldSz);
  void deallocate_helper();
  void init_helper(const char*, size_t);
  char* insert_helper(size_t pos, size_t n);

  //! Get a temporary copy of a str, if it overlaps mBuffer, otherwise return str
  /*! The returned string must be freed by StringUtil::free(tempbuf,sz),
   *! but only if *need is set to true
   */
  char* getTempOverlapBuf(const char* str, size_t sz, bool* needToFree);

  char* mBuffer;        // always sized at mCapacity+1 to accomodate nul term
  size_t mSize;         // does not include nul term
  size_t mCapacity;
};

// convert (like a stream) a UtString into a value of the type defined by second argument
// NOTE these functions modify (shorten) the value in the UTString argument by removing characters as they are converted
// NOTE the versions that generate UInt* or SInt* values allow for strings with hex (0x) and octal (0) leading characters
bool operator>>(UtString& buf, char& c);
bool operator>>(UtString& str, int& num);
bool operator>>(UtString& str, UInt64& num);
bool operator>>(UtString& str, UInt32& num);
bool operator>>(UtString& str, size_t& num);
bool operator>>(UtString& str, UInt16& num);
bool operator>>(UtString& str, UInt8& num);
bool operator>>(UtString& str, SInt64& num);
bool operator>>(UtString& str, SInt32& num);
bool operator>>(UtString& str, SInt16& num);
bool operator>>(UtString& str, double& num);

//! Get a complete line of text from a FILE.
bool fgetline(FILE* file, UtString* str, char delim = '\n');

//! Set of strings
typedef UtHashSet<UtString> UtStringSet;

#endif // UT_STRING_H
