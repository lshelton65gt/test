// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _UT_PROCESS_H_
#define _UT_PROCESS_H_

#include "util/MemManager.h"
#include "util/UtString.h"

class QProcess;
class QCoreApplication;

//! class to manage a context for creating processes
class UtProcessContext {
public:
  //! ctor
  UtProcessContext(int* argcp, char** argv);

  //! dtor
  ~UtProcessContext();

private:
  QCoreApplication* mApplication;
};

//! class to help manage subprocesses with I/O redirection
/*!
 *! This class implementation is slightly more abstract than is strictly
 *! necessary to isolate non-GUI packages from needing access to Qt includes,
 *! since the underlying process management infrastructure is supplied by Qt.
 */
class UtProcess {
public:
  CARBONMEM_OVERRIDES

  //! Construct a process and start it
  UtProcess(UtProcessContext*, char** args);

  //! dtor
  ~UtProcess();

  //! block for process termination, and return exit status code
  int wait();

  //! get the output text spewed by the subprocess
  const char* getOutputBuf() const {return mOutputBuf.c_str();}

private:
  QProcess* mProcess;
  UtString mOutputBuf;
  int mErrCode;
};

#endif
