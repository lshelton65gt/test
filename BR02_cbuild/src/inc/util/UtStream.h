// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtStream_h_
#define __UtStream_h_

#include <iostream>
#ifndef __UtString_h_
#include "util/UtString.h"
#endif
#include <string>

// This is a some messy code that needs to be maintained while we
// are still using std iostreams.  When we switch to UtOStream, and
// create UtIStream and switch to that, then we can un-expose
// raw STL strings.

static inline std::istream& operator>>(std::istream& f, UtString& buf) {
  std::string tmp;
  f >> tmp;
  buf.clear();
  buf << tmp.c_str();
  return f;
}

static inline std::ostream& operator<<(std::ostream& f, const UtString& buf)
{
  f << buf.c_str();
  return f;
}

#endif
