// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Sparcworks doesn't like UtMap and UtMultiMap in the same header
// file.

#ifndef __UtMultiMap_h_
#define __UtMultiMap_h_
#ifndef __MemManager_h_

#include "util/MemManager.h"
#endif

#include <map>


#if !pfTEMPLATE_FORWARD_ARGS
template<class _MKey, class _MTp, class _MCompare = std::less<_MKey> >
#else
template<class _MKey, class _MTp, class _MCompare>
#endif
class UtMultiMap: public
  std::multimap<_MKey, _MTp, _MCompare,
                CarbonSTLAlloc<std::pair<const _MKey, _MTp> > >
{
  typedef std::multimap<_MKey, _MTp, _MCompare,
                        CarbonSTLAlloc<std::pair<const _MKey, _MTp> > > _Base;
public: CARBONMEM_OVERRIDES

  template<class _Iter> UtMultiMap(_Iter _begin, _Iter _end):
    _Base(_begin, _end)
    {}

  UtMultiMap() {}
};

#endif
