// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtQueue_h_
#define __UtQueue_h_

#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif

#ifndef __MemManager_h_
#include "util/MemManager.h"
#endif

//! alternate STL deque definition that uses the Carbon memory allocator
/*! This is also a new implementation, re-inventing stl's implementation,
 *! just to avoid painful linking issues.
 */
template<class _Obj>
class UtQueue
{
  typedef UtArray<_Obj> Array;
public:
  CARBONMEM_OVERRIDES

  typedef size_t size_type;
  typedef _Obj value_type;
  typedef _Obj& reference;
  typedef const _Obj& const_reference;

  //! ctor from iterator
  template<class _Iter> UtQueue(_Iter _begin, _Iter _end):
    mArray(_begin, _end),
    mReadIndex(0)
  {
    mWriteIndex = mArray.size();
  }

  //! ctor
  UtQueue():
    mReadIndex(0),
    mWriteIndex(0)
  {}

  //! dtor
  ~UtQueue() {}

  void push_back(const _Obj& t) {
    // If we are writing to the end of the array, append or wrap around
    if (mWriteIndex == mArray.size()) {
      if ((mReadIndex > 0) && (mWriteIndex == (UInt32) mArray.capacity())) {
        // There is space in the beginning, and no physical space at the
        // end: wrap around the 'write'
        mArray[0] = t;
        mWriteIndex = 1;
      }

      else {
        mArray.push_back(t);
        ++mWriteIndex;
      }
    }

    // writing to the middle
    else {
      mArray[mWriteIndex] = t;
      ++mWriteIndex;
    }

    // If we have run out of space in the middle then we better re-arrange
    // the array so the queue is contiguous, rather than wrapped, to allow
    // for easier expansion.  Our policy is that there has to be one
    // empty space in the array after the write is complete, so that we
    // know that WriteIndex==ReadIndex implies "empty".
    if (mWriteIndex == mReadIndex) {
      Array newArray(0, 2*mArray.size(), false);
      typename Array::iterator pivot = mArray.begin() + mReadIndex;
      newArray.insert(newArray.end(), pivot, mArray.end());
      newArray.insert(newArray.end(), mArray.begin(), pivot);
      mReadIndex = 0;
      mWriteIndex = newArray.size();
      newArray.swap(mArray);
    }
  }

  void pop_back() {
    mWriteIndex = backIndex();
  }

  size_t size() const {
    if (mReadIndex <= mWriteIndex) {
      return mWriteIndex - mReadIndex;
    }

    UInt32 gap = mReadIndex - mWriteIndex;
    return mArray.size() - gap;
  }

  bool empty() const {return mReadIndex == mWriteIndex;}

  const _Obj& front() const {
    INFO_ASSERT(mReadIndex != mWriteIndex, "front() called on empty deque");
    return mArray[mReadIndex];
  }

  _Obj& front() {
    INFO_ASSERT(mReadIndex != mWriteIndex, "front() called on empty deque");
    return mArray[mReadIndex];
  }

  void pop_front() {
    INFO_ASSERT(mReadIndex != mWriteIndex, "pop_front() called on empty deque");
    mReadIndex = (mReadIndex + 1) % mArray.size();
    if ((mReadIndex == 0) && (mWriteIndex == mArray.size())) {
      mWriteIndex = 0;
    }
  }

  _Obj back() const {
    return mArray[backIndex()];
  }

  _Obj& back() {
    return mArray[backIndex()];
  }

  size_t capacity() const {
    return mArray.size();
  }

  void clear() {
    mArray.clear();
    mReadIndex = 0;
    mWriteIndex = 0;
  }

private:
  // NYI
  void push_front(const _Obj&);

  UInt32 backIndex() const {
    INFO_ASSERT(mReadIndex != mWriteIndex,
                "backIndex() called on empty deque");
    if (mWriteIndex == 0) {
      return mArray.size() - 1;
    }
    return mWriteIndex - 1;
  }

  Array mArray;
  UInt32 mReadIndex;
  UInt32 mWriteIndex;
};

#endif
