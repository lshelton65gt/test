// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __ScheduleHistogram_h_
#define __ScheduleHistogram_h_

#include "util/UtIOStream.h"

/*!
  \file
  Declaration of SPScheduleHistogram class.
*/

//! SPScheduleHistogram class
/*!
  Class for creating a histogram of carbonSchedule() calls, for
  Simple Profiling.
*/
class SPScheduleHistogram
{
public:
  CARBONMEM_OVERRIDES
  //! constructor
  SPScheduleHistogram();
  //! Store another time in the histogram.
  void add(double time);
  //! Print the complete histogram.
  void print(UtOStream& out);
  //! Return the largest time that's been added.
  double getMax() const { return mMaxTime; }
  //! Return true if there's no schedule information.
  bool isEmpty() const { return mTimeArraySize == 0; }
private:
  //! Partition the samples into bins.
  /*!
     Partition the samples which are currently all stored in an array
     into bins that count the number of entries.
  */
  void partition();
  //! Number of bins we use for partitioning
  static const unsigned cmNumBins = 5;
  //! Number of times we store in array before partitioning
  static const unsigned cmMaxTimes = 8192; 
  double mTimeArray[cmMaxTimes]; //!< Times used by first n calls
  unsigned mTimeArraySize;      //!< Next available entry
  double mMaxTime;              //!< The longest time for any call
  unsigned mBins[cmNumBins];    //!< Number of entries in each bin
  double mBinSpan;              //!< Difference between beginning and end of each bin
  bool mPartitioned;            //!< True once we've partitioned
};

#endif
