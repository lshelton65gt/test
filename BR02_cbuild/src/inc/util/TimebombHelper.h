// -*-C++-*-
/******************************************************************************
 Copyright (c) 2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __TIMEBOMB_HELPER_H__
#define __TIMEBOMB_HELPER_H__

class UtLicense;
class UtString;

#include "util/CarbonTypes.h"
#include "util/c_memmanager.h"

//! Class for managing timebomb license features
class TimebombHelper
{
public:
  CARBONMEM_OVERRIDES

  //! Creates a TimebombHelper without an existing license object
  /*!
    This will be used by model studio to query the license file, since
    it doesn't have a UtLicense object.
   */
  TimebombHelper();

  //! Creates a TimebombHelper around an existing license object
  TimebombHelper(UtLicense* licenseObj);

  ~TimebombHelper();

  /*!
    Returns a string (in YYYY-MM-DD form) holding the latest-expiring
    timebomb-enabling feature in the license file.  Note that
    "latest-expiring" refers to the date encoded in the license
    feature name (which controls the latest possible date the
    timebombed model can be set to expire), not the expiration date of
    the license itself (which determines that latest possible date on
    which a timebombed model can be created).

    The full license feature name corresponding to the date is also
    returned.

    The actual return value of the function is the 64-bit time
    corresponding to the latest timebomb date.  A return value of zero
    means no timebomb features were found in the license file.
   */
  SInt64 getLatestTimebomb(UtString* timebombString, UtString* featureName);

  /*!
    Attempts to convert a timebomb string (which should be in
    YYYY-MM-DD form) to a 64-bit time, returning success.
   */
  static bool sCreate64BitTimebomb(const char* timeStr, SInt64* timebomb);

  /*!
    Converts an encoded date from the license file (in YYYYMMDD form)
    to YYYY-MM-DD and 64-bit time forms.
   */
  static void sConvertTimeBombLicenseDate(const char* encodedDate, UtString* timebombStr, SInt64* timebomb);


private:
  //! The created or passed-in license
  UtLicense* mLicense;

  class LicenseCB;
  //! The license callback object we need to create if using our own license object
  LicenseCB* mCB;

  //! Do we own this license?
  bool mOwnLicense;
};

#endif
