// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _StlIterator_H_
#define _StlIterator_H_

// In gcc 3.4.* and beyond, <iterator> brings in streams, which bring
// in locales, which bring in link pain.  But <iterator> is just a
// collection of a few other include files, most of which are not
// painful.
#include "util/CarbonPlatform.h"
#if pfGCC_3_4 || pfGCC_4
#include <bits/c++config.h>
#include <cstddef>
#include <bits/stl_iterator_base_types.h>
#include <bits/stl_iterator_base_funcs.h>
#include <bits/stl_iterator.h>

// The rest of these are also in gcc 3.4 <iterator>, but are painful:
//#include <ostream>
//#include <istream>
//#include <bits/stream_iterator.h>
//#include <bits/streambuf_iterator.h>
#else
#include <iterator>
#endif

#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

//! STL Iterator template class
/*!
  This class provides a mechanism to use STL Algorithms from a Carbon Loop/Iter
  style iteration class.  These classes generally provider ++, *, and atEnd()
  methods.  This class wraps those up to provide enough STL Iterator functionality
  to run basic algorithms.  The begin() method will typically construct an
  StlIterator using a pointer to the Loop object, and the end() method will
  be constructed with NULL.  operator== is defined to identify to iterators
  as equal if and only if they are both NULL or atEnd().
*/
template<class _LoopType>
class StlIterator
#if pfGCC_2
  : public forward_iterator<forward_iterator_tag, typename _LoopType::value_type>
#else
  : public std::iterator<std::forward_iterator_tag,
                         typename _LoopType::value_type>
#endif
{
public: CARBONMEM_OVERRIDES
  StlIterator(_LoopType* iter)
  {
    if (iter != NULL)
      mIter = new _LoopType(*iter);
    else
      mIter = NULL;
  }

  StlIterator(const StlIterator<_LoopType>& i)
  {
    if (i.mIter != NULL)
      mIter = new _LoopType(*i.mIter);
    else
      mIter = NULL;
  }

  StlIterator<_LoopType>& operator=(const StlIterator<_LoopType>& i)
  {
    if (&i != this)
    {
      if (mIter != NULL)
        delete mIter;
      if (i.mIter != NULL)
        mIter = new _LoopType(*i.mIter);
      else
        mIter = NULL;
    }
    return *this;
  }

  ~StlIterator()
  {
    if (mIter != NULL)
      delete mIter;
  }

  bool atEnd() const {return (mIter == NULL) || mIter->atEnd();}

  // Two Iter::iterators are considered equal only if they are both at the end
  bool operator==(const StlIterator<_LoopType>& i)
    const
  {
    return (&i == this) || (atEnd() && i.atEnd());
  }

  bool operator!=(const StlIterator<_LoopType>& i)
    const
  {
    return !atEnd() || !i.atEnd();
  }

  void operator++()
  {
    INFO_ASSERT(mIter, "Iterator not initialized.");            // Cannot call end()++
    ++(*mIter);
  }

  typedef typename _LoopType::value_type value_type;
  typedef typename _LoopType::reference reference;

  reference operator*() {
    INFO_ASSERT(mIter, "Iterator not initialized.");            // Cannot call *end()
    return **mIter;
  }

  _LoopType* mIter;
};

#endif // _StlIterator_H_
