// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _UtFileEntries_h_
#define _UtFileEntries_h_

#include "util/OSWrapper.h"
#include "util/UtString.h"
#include "util/UtHashSet.h"

//! Class to maintain a unique set of file entries
class UtFileEntries
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  UtFileEntries();

  //! Destructor
  ~UtFileEntries();

  //! Enumeration for file status
  enum Status {
    eOK, //!< File added successfully
    eDup, //!< File is already in set.
    eStatProb //!< There was a proble statting file
  };

  //! A file entry based on device and inode
  /*!
    This has the name of the file that was first added with the
    unique device and inode characteristics. This also has the stat
    entry.

    The entry is hashed on device and inode numbers. It is sorted on
    filename.
  */
  class Entry
  {
  public:
    CARBONMEM_OVERRIDES

    Entry(const char* filePath);

    //! Return the stat entry
    const OSStatEntry* getStatEntry() const { return &mStatEntry; }
    //! Return the filename for this stat entry
    const UtString* getFilename() const { return &mFilename; }
    
    // hash and sorting methods.
    //! Hash on device and inode
    size_t hash() const;
    
    //! == operator
    /*!
      Equal if device and inode are equal on Unix.
      Equal if both filenames are the same on Windows
    */
    bool operator==(const Entry& other) const;
    
    //! Sort based on filename
    bool operator<(const Entry& other) const;
    
  private:
    friend class UtFileEntries;
    OSStatEntry mStatEntry;
    UtString mFilename;

    // forbid
    Entry();
  };

  //! Dev/Inode hash set type
  /*!
    On Windows, the operator== for this only depends on the
    filepath. There are no inodes on Windows filesystems.
  */
  typedef UtHashSet<Entry*, HashPointerValue<Entry*> > DevInodeSet;

  //! Add a file to the list of unique entries
  /*!
    \param filePath Path of file to add to unique entry set
    \param errMsg If Status is not eOK, the reason why it wasn't
    added is put here.
  */
  Status addFile(const char* filePath, UtString* errMsg);

  //! Remove file from the list of unique entries
  /*!
    \param filePath File to remove from entry list. It can be an alias
    (link) to the entry you wish to remove.

    \returns False if the file entry for the given filePath is not found.
  */
  bool removeFile(const char* filePath, Status* stat);
  
  //! Return the file entry for the given path
  /*!
    This returns the file entry for the first encounter of the
    dev/inode pair associated with filePath. So, if the file "first"
    was added and "second" is linked to "first", and "second" is
    passed into this function, The file entry for "first" will be
    passed back. They are the same physical address, so the stat entry
    would be the same anyway. The file entry must already have been
    added using addFile().
    
    \param filePath Path of file to get entry
    \param stat Will always be eOK unless there is a problem stating
    filePath, in which case it will be eStatProb
    \returns NULL if the entry does not exist or if there was a
    problem statting the file path. 
  */
  const Entry* getFileEntry(const char* filePath, Status* stat) const;

  
private:
  DevInodeSet mDevInodeSet;

  // forbid
  UtFileEntries(const UtFileEntries&);
  UtFileEntries& operator=(const UtFileEntries&);

  const Entry* internalGetEntry(Entry*) const;
  Entry* internalGetEntry(Entry*);
};

#endif
