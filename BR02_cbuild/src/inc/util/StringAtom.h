// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __StringAtom_h_
#define __StringAtom_h_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#include <cstring> // for strcmp

/*!
  \file 
  Optimized object for memory of char*. Each interned UtString becomes a
  StringAtom.
*/

//! StringAtom class to describe interned char*
/*!
  The StringAtom class is made only to be alloced by the AtomicCache
  class and is done in such a way to optimize for space. 
*/
class StringAtom 
{
public: CARBONMEM_OVERRIDES
  //! Return the char* of this atom
  inline const char* str() const 
  {
    return mPackedName;
  }
    
  //! Allow assignment to char* 
  inline operator const char*() const 
  {
    return mPackedName;
  }

  //! comparison function to make it easy to sort AtomSet (UtHashSet<StringAtom*>)
  bool operator<(const StringAtom& other) const {
    return std::strcmp(str(), other.str()) < 0;
  }

  bool operator>(const StringAtom& other) const {
    return std::strcmp(str(), other.str()) > 0;
  }

private:
  //! Unused constructor
  /*!
    The AtomicCache constructs these and does not use the class
    constructor
  */
  StringAtom();
    
  char mPackedName[1];
    
  //forbid
  StringAtom(const StringAtom&);
  StringAtom& operator=(const StringAtom&);
} ;


//! StringAtomCmp class
/*!
 * This class can be used as a compare class when doing ordered sets
 * of StringAtom's.
 */
class StringAtomCmp
{
public: CARBONMEM_OVERRIDES
  bool operator() (StringAtom* a, StringAtom *b) const
  {
    return (std::strcmp(a->str(), b->str()) < 0);
  }
};


#endif
