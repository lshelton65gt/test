// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  Generalized arithmetic class for arbitrary-length bit values.

    Note that \b ONLY compile-time sized bit lengths are supported.
    In addition to the operators that the std::bitset<> template provided,
    BitVector<> also supports integer arithmetic, assignment, and operations
    between BitVector<>'s of different widths.

    cbuild only uses BitVector<> instances for Verilog objects with sizes
    larger than "UInt64" (64 bits on most machines).

    This code is derived from the STL bitvector template class with a
    lot of Boost-isms mixed in.
*/

/*
 * Copyright (c) 1998
 * Silicon Graphics Computer Systems, Inc.
 *
 * Copyright (c) 1999 
 * Boris Fomitchev
 *
 * This material is provided "as is", with absolutely no warranty expressed
 * or implied. Any use is at your own risk.
 *
 * Permission to use or copy this software for any purpose is hereby granted 
 * without fee, provided the above notices are retained on all copies.
 * Permission to modify the code and to distribute modified code is granted,
 * provided the above notices are retained, and a notice that the code was
 * modified is included with the above copyright notice.
 *
 */

#ifndef __BitVector_h_
#define __BitVector_h_


/*
 * A BitVector of size N, using words of type UInt32, will have 
 * N % (sizeof(UInt32) * CHAR_BIT) unused bits.  (They are the high-
 * order bits in the highest word.)  It is a class invariant
 * of class BitVector<> that those unused bits are always zero.
 *
 * Most of the actual code isn't contained in BitVector<> itself, but in the 
 * base class _Base_BitVector.  The base class works with whole words, not with
 * individual bits.  This allows us to specialize _Base_BitVector for the
 * important special case where the BitVector is only a single word.
 *
 * The C++ standard does not define the precise semantics of operator[].
 * In this implementation the const version of operator[] is equivalent
 * to test(), except that it does no range checking.  The non-const version
 * returns a reference to a bit, again without doing any range checking.
*/

#ifndef __carbon_priv_h_
#include "codegen/carbon_priv.h"
#endif

// like std::min/max, but doesn't require including algorithm
template <typename T> inline T BITVECTOR_MIN (T x, T y) { return (x<y)? x : y; }
template <typename T> inline T BITVECTOR_MAX (T x, T y) { return (x>y)? x : y; }

// Performance hacking/tracking
//#define BVPERFHACK  // if defined, calls stopHere() to allow me to
// track interesting events.

#ifndef __CarbonPlatform_h_
#include "util/CarbonPlatform.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef __CarbonAssert_h_
#include "util/CarbonAssert.h"
#endif

/* Do NOT change this define name or use (CARBON_BV_SMALL_ANYTOANY). 
   It is defined in SPEEDCompiler based on a switch.
*/

#ifndef CARBON_BV_SMALL_ANYTOANY
#include "shell/carbon_interface.h"
#endif

#ifdef CARBON_SAVE_RESTORE
#ifndef __carbon_interface_h_
#include "shell/carbon_interface.h"
#endif
#endif

#if defined(BVPERFHACK)
#ifndef __MemManager_h_
#include "util/MemManager.h"
#endif
#endif

#undef LONG_BIT
#undef LLONG_BIT
#undef CHAR_BIT
#undef SHORT_BIT

#define CHAR_BIT   8u
#define SHORT_BIT 16u
#define LONG_BIT  32u
#define LLONG_BIT 64u

#ifdef BVPERF
#include "util/BVPerf.h"
#define BVC(x,s) BVPC->count##x(s)
#else
#define BVC(x,s)
#endif

// Reserve ONE word for OOB references
extern "C" UInt32 gCarbonMemoryBitbucket[] COMMON;


//! NxM multiply yielding K word unsigned result
void carbon_multiply (const unsigned int *a, const unsigned int *b, unsigned int *c,
                      int m, int n, int k);

//! NxM signed multiply yielding K word signed result
void carbon_signed_multiply (const UInt32 *a, const UInt32 *b, UInt32 *c,
                             int m, int n, int k);

/*
 * This file is used in both compile-time and run-time.
 * At run-time, we have an EXPECTED_VALUE macro to help
 * the backend compilers perform branch optimization.
 *
 * At compile-time, this macro is not defined, so
 * give it a no-op definition.
 */
#ifndef EXPECTED_VALUE
#define LOCALLY_DEFINED_EXPECTED_VALUE 1
#define EXPECTED_VALUE(expr,val) expr
#endif

//! Convert bit width to equivalent number of UInt32's
#define __BITVECTOR_WORDS(__n) \
 ((__n) < 1 ? 1 : ((__n) + LONG_BIT - 1)/LONG_BIT)

// Type large enought to hold any legal bit size or position
typedef SInt32 bp_t;
typedef UInt32 bs_t;

// The following is true for signed types only. It gets folded as a constant
// expression for either signed or unsigned types
#define __SIGNED_TYPE (static_cast<T64>(T32(~0))>>32)

  //! utility functions for positional access.
static inline UInt32 _S_whichword( bp_t __pos ) ALWAYS_INLINE;
static inline UInt32 _S_whichword( bp_t __pos )
{
  return __pos / LONG_BIT;
}

static inline UInt32 _S_whichbit( bp_t __pos ) ALWAYS_INLINE;
static inline UInt32 _S_whichbit( bp_t __pos ) {
  return __pos % LONG_BIT;
}

static inline UInt32 _S_maskbit( bp_t __pos ) ALWAYS_INLINE;
static inline UInt32 _S_maskbit( bp_t __pos ) {
  return 1U << _S_whichbit(__pos);
}

static inline SInt32 _S_firstfrag (bp_t p, bs_t s) ALWAYS_INLINE;
static inline SInt32 _S_firstfrag (bp_t p, bs_t s)
{
  if ((SInt32 (p) + (SInt32) s) > (SInt32)LONG_BIT)
    return LONG_BIT - p;
  else
    return s;
}

// SIZ bits, starting at bit POS
static inline UInt32 _S_maskbit (bp_t __pos, bs_t __siz) ALWAYS_INLINE;
static inline UInt32 _S_maskbit (bp_t __pos, bs_t __siz)
{
  return UInt32(((KUInt64(1) << __siz)-1) << __pos);
}

//! Compute log2(x)
inline UInt32 exact_log2 (UInt32 x){
  if (x == 0)
    return 0;
  else
    return carbon_FLO (x);
}

inline UInt32 exact_log2 (UInt64 x){
  if (x== 0)
    return 0;
  return carbon_DFLO (x);
}

static inline void _S_adjustPS (bp_t &pos, bs_t &size) ALWAYS_INLINE;
static inline void _S_adjustPS (bp_t &pos, bs_t &size)
{
  if (EXPECTED_VALUE ((pos < 0), 0))
  {
    REPORT_OOB ("", "vector", size-1, 0, pos);
    if ((bp_t)(pos + size) < 0) {
      size = 0;
    } else {
      size = (bp_t)(pos + size);
    }
    pos = 0;
  }
}

//! _S_adjustPSC returns adjusted pos, size, and the count to shift expr left.
#define _S_adjustPSC(NBITS, POS, SIZE, SHIFTL) \
do {                                                                            \
  if (EXPECTED_VALUE ((POS < 0),0))                                             \
  {                                                                             \
    REPORT_OOB ("", "vector", SIZE-1, 0, POS);                                      \
    bp_t newSize = SIZE + POS;                                                  \
                                                                                \
    if (EXPECTED_VALUE ((newSize < 0), 0)) {                                    \
      SIZE = 0;                                                                 \
      break;                                                                    \
    } else {                                                                    \
      SIZE = newSize;                                                           \
    }                                                                           \
                                                                                \
    SHIFTL = (-POS);                                                            \
    POS = 0;                                                                    \
  } else if (EXPECTED_VALUE ((POS >= (bp_t) NBITS), 0)) {                       \
    SIZE = 0;                                                                   \
    break;                                                                      \
  }                                                                             \
                                                                                \
  if (EXPECTED_VALUE (((bs_t)(POS + SIZE) > NBITS), 0))                         \
    SIZE = NBITS - POS;                                                         \
} while (0)

static inline void _S_adjustPtrPosSize (UInt32* &ptr, bp_t &pos, bs_t &size, bs_t max_size) ALWAYS_INLINE;
static inline void _S_adjustPtrPosSize (UInt32* &ptr, bp_t &pos, bs_t &size, bs_t max_size)
{
  if (EXPECTED_VALUE ((pos < 0), 0))
  {
    REPORT_OOB ("", "vector", max_size-1, 0, pos);
    if ((bp_t)(pos + size) < 0) {
      // piece is entirely OOB, just collapse to null reference
      size = 0;
      pos = 0;
    } else {
      // Not entirely OOB
      ptr += SInt32(pos - (LONG_BIT-1)) / SInt32 (LONG_BIT);
      pos = pos % LONG_BIT;
    }

  }
  else if (EXPECTED_VALUE ((pos >= bp_t (max_size)), 0)) {
    REPORT_OOB ("", "vector", max_size-1, 0, pos);
    pos = size = 0;             // bring back in bounds as empty field
  }
  else {
    bp_t extra = pos + size - max_size;
    if (EXPECTED_VALUE ((extra > 0), 0)) {
      REPORT_OOB ("", "vector", max_size-1, 0, pos);
      size -= extra;
    }
  }
}

#if 0
template<typename VT> static inline void
  _S_adjustPosSizeValue (bp_t &pos, bs_t &size, bs_t max_size, VT &value) ALWAYS_INLINE;
template<typename VT> static inline void 
  _S_adjustPosSizeValue (bp_t &pos, bs_t &size, bs_t max_size, VT &value)
{
  if (EXPECTED_VALUE ((pos < 0), 0))
  {
    REPORT_OOB ("", "vector", max_size-1, 0, pos);
    if ((bp_t)(pos + size) < 0)
      size = 0;
    else
      size = (bp_t)(pos + size);
    value >>= (-pos);
    pos = 0;
  }
  else if (EXPECTED_VALUE ((pos >= bp_t (max_size)), 0)) {
    REPORT_OOB ("", "vector", max_size-1, 0, pos);
    pos = size = 0;             // bring back inbound as empty field
  }
  else {
    bp_t extra = pos + size - max_size;
    if (EXPECTED_VALUE ((extra > 0), 0)) {
      REPORT_OOB ("", "vector", max_size-1, 0, pos);
      size -= extra;
    }
  }
}
#else
// better inlining as macro: POS,SIZE,VALUE must be LVALUES
#define _S_adjustPosSizeValue(pos, size, max_size, value)               \
  do {                                                                  \
    if (EXPECTED_VALUE ((pos < 0), 0)) {                                \
      REPORT_OOB ("", "vector", max_size-1, 0, pos);                    \
      if ((bp_t)(pos + size) < 0)                                       \
        size = 0;                                                       \
      else                                                              \
        size = (bp_t)(pos + size);                                      \
      value >>= (-pos);                                                 \
      pos = 0;                                                          \
    } else if (EXPECTED_VALUE ((pos >= bp_t (max_size)), 0)) {          \
      REPORT_OOB ("", "vector", max_size-1, 0, pos);                    \
      pos = size = 0;                                                   \
    } else {                                                            \
      bp_t extra = pos + size - max_size;                               \
      if (EXPECTED_VALUE ((extra > 0), 0)) {                            \
        REPORT_OOB ("", "vector", max_size-1, 0, pos);                  \
        size -= extra;                                                  \
      }                                                                 \
    }                                                                   \
  } while(0)

#endif

template <bool _stype> class BVref;

// Magic templates to categorize type T as either integral(0), BVref (1) or
// BitVector(2)
template <typename T> struct TypeDiscriminant
{
  // Assume BitVector.  Use SFINAE to reject any template match that doesn't conform
  // (BVref's don't have BVref::basetype, while BitVector::basetype is well defined.
  typedef typename T::basetype basetype;
  enum {eType = 2};
};

// Define specializations for the fundamental integral types that codegen
// can use for the RHS of a BVref assignment
#define MK_TD(T) template <> struct TypeDiscriminant<T>{enum {eType = 0}; }
MK_TD (UInt8);
MK_TD (UInt16);
MK_TD (UInt32);
MK_TD (UInt64);
MK_TD (SInt8);
MK_TD (SInt16);
MK_TD (SInt32);
MK_TD (SInt64);
MK_TD (bool);
#undef MK_TD

// Add specialization discriminants for BVref (both signed and unsigned flavors)
template <> struct TypeDiscriminant<BVref<true> > {enum {eType=1};};
template <> struct TypeDiscriminant<BVref<false> >{enum {eType=1};};

// Template function for returning sign extension
template <typename T> UInt32 sign_extension (T x);
template <> inline UInt32 sign_extension<SInt64>(SInt64 x) {return (x<0)? ~0: 0; }
template <> inline UInt32 sign_extension<UInt64>(UInt64 /* x */) {return 0;}

// Trait class for holding signed and unsigned types
template <bool b>
struct SignExtendType {};

// specialization for signed types
template <>
struct SignExtendType<true> 
{
  typedef SInt32 T32;
  typedef SInt64 T64;
};

// specialization for unsigned types
template <>
struct SignExtendType<false> 
{
  typedef UInt32 T32;
  typedef UInt64 T64;
};

template<UInt32 _Nw, bool _stype = false>
struct _Base_BitVector {
  CARBONMEM_OVERRIDES

  UInt32 _M_w[_Nw];

  typedef typename SignExtendType<_stype>::T32 T32;
  typedef typename SignExtendType<_stype>::T64 T64;

  enum {Nw = _Nw,
        Nbits = _Nw*LONG_BIT};

  //! Simplest uninitialized constructor.  DOES NOT CLEAR STORAGE!!!
  _Base_BitVector( void ) {/* _M_do_reset(); */}

  //! Copy Constructor
  _Base_BitVector (const _Base_BitVector& val) {
    ::memcpy (&this->_M_w[0], &val._M_w[0], _Nw*sizeof(UInt32));
  }

  // One word.
  explicit _Base_BitVector(UInt32 __val)
  {
    this->_M_w[0] = __val;
    this->zextend (1);
  }

  //! Constructor initialized from a single integer value
  explicit _Base_BitVector(UInt64 __val)
  {
#if defined(BVPERFHACK)
    CarbonMem::stopHere ();
#endif

    this->_M_w[0] = __val;
    if (_Nw > 1)
      this->_M_w[1] = __val >> LONG_BIT;

    if (_Nw > 2)
      ::memset (&this->_M_w[2], 0, sizeof(UInt32) * (_Nw-2));
  }

  //! Constructor from a vector of words
  _Base_BitVector (const UInt32 *v, UInt32 len)
  {
    if (_Nw <= len)
      ::memcpy(&this->_M_w[0], v, _Nw*sizeof(UInt32));
    else
      {
	::memcpy(&this->_M_w[0], v, len*sizeof(UInt32));
        this->zextend (len);
      }
  }
  
  // Helper to sign extend uninitialized words of BitVector or SBitVector
  // start is the index into _M_w where the sign is to be extended from.
  inline void sextend (UInt32 start) {
    if (EXPECTED_VALUE (((SInt32)(_Nw - (start + 1)) > 0), 1)) {
      ::memset (&this->_M_w[start+1],
                ((SInt32)(this->_M_w[start]) < 0) ? -1 : 0,
                sizeof(UInt32) * (SInt32)(_Nw - (start + 1)));
    }
  }

  // zero extend uninitialized words. 
  // start is the first location to zero (this is different from sextend!)
  inline void zextend (UInt32 start) {
    if (EXPECTED_VALUE (((SInt32)(_Nw - start) > 0), 1))
      ::memset (&this->_M_w[start],
                0,
                sizeof(UInt32) * (SInt32)(_Nw - start));
  }
    
  UInt32& _M_hiword()       { return this->_M_w[_Nw - 1]; }
  UInt32  _M_hiword() const { return this->_M_w[_Nw - 1]; }

  //! things that operate on words only...
  //! 64-bit-or-less-wide part select
  inline T64 llvalue(bp_t pos, bs_t size) const
  {
#ifdef CDB
    ASSERT(size <= LLONG_BIT);
#endif
    BVC (RefL, size);
    UInt32 shiftL = 0;
 
    if (!CARBON_NO_OOB) {
      // Handle negative positions, happily read garbage if size too big
      _S_adjustPSC (Nbits, pos, size, shiftL);
      if (EXPECTED_VALUE ((size == 0), 0))
        return 0;
    }

    const UInt32* base = this->_M_w + _S_whichword(pos);

    // for llvalue(4,64), source looks like this:
    // 95............67...64 63...............32 31............4...0
    //               *******************************************
    //       base[2] segment     base[1] segment     base[0] segment

    // First we do the base[0] segment -- bits 4:31.
    pos = pos % LONG_BIT;                         // 4
    UInt64 ret = base[0] >> pos;

    // base[1] segment -- bits 32:63, which map to destination 28:59
    UInt32 remainder = LONG_BIT - pos;            // 28
    if (EXPECTED_VALUE((size > remainder),1))
    {
      ret |= ((UInt64) base[1]) << remainder;

      // base[2] segment -- bits 64:67, which map to destination 60:63
      remainder += LONG_BIT;                      // 60
      if (size > remainder)
        ret |= ((UInt64) base[2]) << remainder;
    }
    
    if (EXPECTED_VALUE((size != 64),1))
      ret &= (((UInt64) 1) << (UInt64)size) - 1;

    return (ret << shiftL);
  } // UInt64 llvalue

  //! 32-bit-or-less-wide part select
  inline T32 value (bp_t pos, bs_t size) const
  {
#ifdef CDB
    ASSERT(size <= LONG_BIT);
#endif
    BVC (RefL, size);
    UInt32 shiftL = 0;

    // Handle negative positions, happily read garbage if size too big
    if (!CARBON_NO_OOB) {
      _S_adjustPSC (Nbits, pos, size, shiftL);
      if (EXPECTED_VALUE ((size == 0), 0))
        return 0;
    }

    UInt32 wordIndex = _S_whichword(pos);

    // for value(4,32), source looks like this:
    // 64............35...32 31..........4...0
    //               *********************
    //       base[2] segment     base[1] segment     base[0] segment

    // First we do the base[0] segment -- bits 4:31.
    UInt32 ret = this->_M_w[wordIndex] >> (pos % LONG_BIT);

    // base[1] segment -- bits 32:63, which map to destination 28:59
    UInt32 remainder = LONG_BIT - (pos % LONG_BIT);            // 28
    if (EXPECTED_VALUE((size > remainder), 0))
      ret |= this->_M_w[wordIndex+1] << remainder;

    if (EXPECTED_VALUE((size != LONG_BIT), 1))
      ret &= (1U << size) - 1;

    return (ret << shiftL);
  }

  //! guaranteed unsigned accessors
  inline UInt32 uvalue (bp_t pos, bs_t size) const { return value (pos, size); }
  inline UInt64 ullvalue (bp_t pos, bs_t size) const { return llvalue (pos, size); }

  //! word-indexed reference type (writable non-const version)
  UInt32& get_word (UInt32 index) {
    if (EXPECTED_VALUE ((!CARBON_NO_OOB && index >= _Nw ), 0)) {
      // bounds checking and OOB
      REPORT_OOB ("write", "a vector", _Nw*32 - 1, 0, index*32);
      return gCarbonMemoryBitbucket[0];
    }
    
    return this->_M_w[index];
  }

  //! word-indexed value type (const RHS version)
  UInt32 get_word (UInt32 index) const {
    if (EXPECTED_VALUE ((!CARBON_NO_OOB && index >= _Nw), 0)) {
      // bounds checking and OOB
      REPORT_OOB ("read", "a vector", _Nw*32 - 1, 0, index*32);
      return 0;
    }
    
    return this->_M_w[index];
  }

  // returns this as a real value
  CarbonReal rvalue() const
  {
    const SInt32 pos_of_first_one = 64;
    const SInt32 shift = pos_of_first_one-64; 
    T64 data64 = (this->llvalue(shift,64));	/* T64 is unsigned or signed as needed */
    CarbonReal r_temp = data64;
    if ( shift > 1 ) 
      r_temp = std::ldexp(r_temp, shift); 
    return r_temp;
  }

  //! Store upto 64 bits at the specified position.
  inline void lldeposit (bp_t pos, bs_t size, UInt64 value)
    {
      _S_adjustPosSizeValue (pos, size, Nbits, value);

      if (size == 0)
        return;
      else if (size <= LONG_BIT)
        return ldeposit (pos, size, T32 (value));
        
      if (EXPECTED_VALUE((_S_whichbit (pos)== 0), 0))	// Check for aligned store
	{
	  this->_M_w[_S_whichword (pos)] = value; // Store aligned piece

	  // and store remaining piece.
	  ldeposit (pos + LONG_BIT, size - LONG_BIT, value >> LONG_BIT);
	}
      else
	// Unaligned case.
	{
	  // store enough to reach an aligned case.
	  UInt32 partlen = LONG_BIT - _S_whichbit (pos);

	  ldeposit (pos, partlen, value);

	  size -= partlen;
	  if (EXPECTED_VALUE((size > LONG_BIT), 0))	// Still more than 32 bits?
	    {
	      pos += partlen;
	      value >>= partlen;

	      this->_M_w[_S_whichword (pos)] = value;	// Store the aligned chunck

	      ldeposit (pos+LONG_BIT, size - LONG_BIT, value >> LONG_BIT);
	    }
	  else
	    // less than 32 aligned bits remain.
	    ldeposit (pos + partlen, size, value >> partlen);
	}
    }

  //! Store upto 32 bits at the specified bit-position
  //! value is assumed to be masked to the appropriate size.
  inline void ldeposit (bp_t pos, bs_t size, UInt32 value)
    {
      _S_adjustPosSizeValue (pos, size, Nbits, value);

      if (size == 0)
        return;

      const UInt32 off = _S_whichword (pos);
      const UInt32 bit = _S_whichbit (pos);

      if (EXPECTED_VALUE(((bit + size) <= LONG_BIT), 1))
	// Fits within a single word
	this->_M_w[off] = ((this->_M_w[off]
		      & ~_S_maskbit (bit, size))
		     | (value << bit));
      else
	{
	  // piece that fits in this word
	  const UInt32 partlen = LONG_BIT - bit;

	  this->_M_w[off] = ((this->_M_w[off]
			& ~_S_maskbit (bit, partlen))
		       | (value << bit));

	  this->_M_w[off+1] = ((this->_M_w[off+1]
			  & ~_S_maskbit (0, size-partlen))
			 | (value >> partlen));
	}
    }

  UInt32 uvalue (void) const { return value (); }
  UInt64 ullvalue (void) const { return llvalue (); }

  T64 llvalue (void) const {
    BVC (RefLL, LLONG_BIT);
    union U64 {
      UInt32 v[2];
      T64 llv;
    };

    // It turns out that it doesn't pay to be too clever here. The simple code
    // populating a union and pulling out the 64 bit value gets optimized
    // by GCC for Linux
    if (pfLINUX64)
    {
      // This is marginally better than a[1]<<64 | a[0]
      const U64* p ((const U64*)(const void*)&this->_M_w[0]);
      return p->llv;
    } else {
      U64 value;
      value.v[0] = this->_M_w[0];
      value.v[1] = this->_M_w[1];
      return value.llv;
    }
  }

  T32 value (void) const {
    BVC (RefL, LONG_BIT);
    return this->_M_w[0];
  }

// Iterate thru the words of src and destination from [i..Nw-1]
#define FromWordS(i) \
   for (UInt32*dp=&this->_M_w[i], *sp = const_cast<UInt32*>(&__x._M_w[0]); \
        dp != &this->_M_w[Nw];)

// Iterate thru words of destination[i..Nw-1]
#define FromWord(i)  for(UInt32*dp=&this->_M_w[i]; dp != &this->_M_w[Nw];)

// Likewise with a constant pointer
#define FromWordC(i)  for(const UInt32*dp=&this->_M_w[i]; dp != &this->_M_w[Nw];)

#define OPERATION(opname,op) 				\
  void opname (const _Base_BitVector& __x) {            \
    FromWordS(0) *dp++ op##= *sp++;                     \
  }

  OPERATION (_M_do_and,&)
  OPERATION (_M_do_or, |)
  OPERATION (_M_do_xor, ^)

#undef OPERATION

  //! Generalized extended precision addition.
  void _M_do_add(const _Base_BitVector& __x) {
    UInt64 sum = 0;
    FromWordS (0)
      {
	sum += (UInt64) *dp + (UInt64) *sp++;
	*dp++ = sum;
	sum >>= LONG_BIT;
      }
  }

  //! \overload
  void _M_do_add (UInt32 rval) {
    UInt64 sum = rval;
    FromWord (0)
      {
	sum += (UInt64) *dp;
	*dp++ = sum;
	sum >>= LONG_BIT;
        if (!sum)               // Carry has stopped propagating
          break;
      }
  }
  //! \overload for hi-word of SInt64 addition.
  void _M_do_add (SInt32 rval) {
    SInt64 sum = rval;
    FromWord (0)
      {
	sum += (UInt64) *dp;
	*dp++ = sum;
        sum >>= LONG_BIT;
        if (!sum)
          break;
      }
  }

  //! Generalized extended precision subtraction
  void _M_do_sub (const _Base_BitVector& __x) {
    SInt64 diff = 0;
    FromWordS (0)
      {
	diff += (UInt64) *dp - (UInt64) *sp++;
	*dp++ = diff;
        diff >>= LONG_BIT;
      }
  }

  //! \overload
  void _M_do_sub (UInt32 rval) {
    SInt64 diff = rval;

    diff = -diff;
    FromWord (0)
      {
	diff += (UInt64) *dp;
	*dp++ = diff;
        diff >>= LONG_BIT;
        if (!diff)
          break;
      }
  }

  //! \overload for hi-word of SInt64 subtraction
  void _M_do_sub (SInt32 rval) {
    SInt64 diff = rval;

    diff = -diff;
    FromWord (0)
      {
	diff += (UInt64) *dp;
	*dp++ = diff;
        diff >>= LONG_BIT;
        if (!diff)
          break;
      }
  }
  
  //! Unary negation
  void _M_do_neg (void) {
    SInt64 temp = 0;
    FromWord (0)
      {
	temp -= (UInt64) *dp;
	*dp++ = temp;
        temp >>= LONG_BIT;
      }
  }
    

  //! divide operator
  void _M_do_div (UInt32 n) {
    UInt32 shifts = exact_log2 (n);
    return _M_do_right_shift (shifts);
  }

  void _M_do_div (UInt64 n) {
    UInt32 shifts = exact_log2 (n);
    return _M_do_right_shift (shifts);
  }

  //! Logical Shifts.
  void _M_do_left_shift(UInt32 __shift) 
  { 
    // Guard against out-of range shifts...
    if (__shift == 0)
      return;
    else if (__shift >= UInt32 (Nbits)) {
      ::memset (&this->_M_w[0], 0, Nw*sizeof(UInt32));
      return;
    } else if (__shift % CHAR_BIT)  {
      /*
       * Unaligned shifts are always done the hard way.  On a big-endian,
       * only WORD aligned shifts can be memmoved. 
       */
      const UInt32 __wshift = __shift / LONG_BIT;
      const UInt32 __offset = __shift % LONG_BIT;
      const UInt32 __sub_offset = LONG_BIT - __offset;

      UInt32* dp = &this->_M_w[Nw-1];
      const UInt32* sp = &this->_M_w[(Nw-1) - __wshift];

      for (; dp != &this->_M_w[__wshift]; ) {
        UInt32 srcval = *sp-- << __offset;
        srcval |= *sp >> __sub_offset;
        *dp-- = srcval;
      }

      *dp-- = *sp << __offset;

      for (; dp != &this->_M_w[-1];)
        *dp-- = 0;
    } else {
      // Little-endian machine can just move the bytes.  Big endian only
      // works for multiples of 32 so that word order stays the same
      const UInt32 byteoff = __shift / CHAR_BIT;
      char *cp = reinterpret_cast<char *>(this->_M_w);
      ::memmove (&cp[byteoff], &cp[0], _Nw*sizeof(UInt32) - byteoff);
      ::memset (&cp[0], 0, byteoff);
    }
  }

#ifdef BITVECTOR_RVO
  // Experimented with RVO shift optimizations.
  //
  void _M_value_left_shift(_Base_BitVector& dest, UInt32 __shift)
  { 
    // Guard against out-of range shifts...
    if (__shift == 0) {
      ::memcpy (&dest._M_w[0], &this->_M_w[0], Nw * sizeof(UInt32));
      return;
    } else if (__shift >= UInt32 (Nbits)) {
      ::memset (&dest._M_w[0], 0, Nw * sizeof(UInt32));
      return;
    } else if (__shift % CHAR_BIT)  {
      /*
       * Unaligned shifts are always done the hard way.  On a big-endian,
       * only WORD aligned shifts can be memmoved. 
       */
      const UInt32 __wshift = __shift / LONG_BIT;
      const UInt32 __offset = __shift % LONG_BIT;
      const UInt32 __sub_offset = LONG_BIT - __offset;
      UInt32 __n = _Nw - 1;
      UInt32* dp = &dest._M_w[__n];
      const UInt32* sp=&this->_M_w[__n - __wshift];
      while ( dp != &dest._M_w[__wshift] )
      {
        UInt32 srcval = *sp-- << __offset;
        srcval |= *sp >> __sub_offset;
        *dp-- = srcval;
      }

      *dp = *sp << __offset;

      for (; dp != &dest._M_w[-1];)
        *dp-- = 0;

    } else {
      // Little-endian machine can just move the bytes.  Big endian only
      // works for multiples of 32 so that word order stays the same
      const UInt32 byteoff = __shift / CHAR_BIT;
      char *srcp = reinterpret_cast<char *>(this->_M_w);
      char *dstp = reinterpret_cast<char *>(dest._M_w);
      ::memmove (&dstp[byteoff] , &srcp[0], _Nw*sizeof(UInt32) - byteoff);
      ::memset (&dstp[0], 0, byteoff);
    }
  }

  void _M_value_right_shift(_Base_BitVector& dest, UInt32 __shift)
  { 
    // Guard against out-of range shifts...
    if (__shift == 0) {
      ::memcpy (&dest._M_w[0], &this->_M_w[0], Nw * sizeof(UInt32));
      return;
    } else if (__shift >= UInt32 (Nbits)) {
      ::memset (&dest._M_w[0], 0, Nw * sizeof(UInt32));
      return;
    } else if (__shift % CHAR_BIT)  {
      /*
       * Unaligned shifts are always done the hard way. 
       */
      const UInt32 __wshift = __shift / LONG_BIT;
      const UInt32 __offset = __shift % LONG_BIT;
      const UInt32 __sub_offset = LONG_BIT - __offset;
      UInt32 __limit = _Nw - __wshift - 1;

      UInt32* dp = &dest._M_w[0];
      const UInt32* sp=&this->_M_w[__wshift];
      while ( dp != &dest._M_w[__limit] )
      {
        UInt32 srcval = *sp++ >> __offset;
        srcval |= *sp << __sub_offset;
        *dp++ = srcval;
      }

      *dp = *sp >>  __offset;

      for (; dp != &dest._M_w[Nw];)
        *dp++ = 0;

    } else {
      // Little-endian machine can just move the bytes.  Big endian only
      // works for multiples of 32 so that word order stays the same
      const UInt32 byteoff = __shift / CHAR_BIT;
      char *srcp = reinterpret_cast<char *>(this->_M_w);
      char *dstp = reinterpret_cast<char *>(dest._M_w);
      ::memmove (&dstp[byteoff] , &srcp[0], _Nw*sizeof(UInt32) - byteoff);
      ::memset (&dstp[0], 0, byteoff);
    }
  }
#endif

  // THIS IS A LOGICAL RIGHT SHIFT!!!
  void _M_do_right_shift(UInt32 __shift) 
  {
    // Guard against out-of range shifts...
    if (__shift >= UInt32 (Nbits)) {
      // shifted away all significance
      ::memset (&this->_M_w[0], 0, sizeof(UInt32) * Nw);
    } else if (__shift == 0) {
      // no motion needed
      return;
    } else if (__shift % CHAR_BIT) {

      const UInt32 __wshift = __shift / LONG_BIT;
      const UInt32 __offset = __shift % LONG_BIT;
      const UInt32 __sub_offset = LONG_BIT - __offset;

      UInt32* dp = &this->_M_w[0];
      const UInt32* sp = &this->_M_w[__wshift];
      for ( ; sp != &this->_M_w[Nw-1]; )
      {
        UInt32 srcval = *sp++ >> __offset;
        srcval |= *sp << __sub_offset;
        *dp++ = srcval;
      }
        
      *dp++ = *sp >> __offset;

      for (; dp != &this->_M_w[Nw]; )
        *dp++ = 0;
    } else {
      const UInt32 byteoff = __shift / CHAR_BIT;
      const UInt32 numBytes = _Nw*sizeof(UInt32) - byteoff;
      char *cp = reinterpret_cast<char *>(this->_M_w);
      ::memmove(&cp[0], &cp[byteoff], numBytes);
      ::memset (&cp[numBytes], 0, byteoff);
    }
  }

  //! default value extractor for BitVector<>
  UInt32 _M_do_to_ulong() const
  {
    return this->_M_w[0];
  } // End _M_do_to_ulong

  // Optimize special case X = ~X
  void _M_do_flip() {
    FromWord (0) {
      *dp = ~*dp;
      ++dp;
    }
  }

  // Fast set/reset operations
  void _M_do_set() {
    ::memset (&this->_M_w[0], -1, sizeof (UInt32)*_Nw);
  }

  void _M_do_reset() {
    zextend (0);
  }

  //!  compare \returns -1,0,1 (LT,EQ,GT)
  SInt32 _M_do_compare(const _Base_BitVector& __x) const {
    const UInt32* lp = &(this->_M_w[_Nw-1]),
      *rp = &(__x._M_w[_Nw-1]);

    if (_stype) {
      if (static_cast<SInt32>(*lp) < static_cast<SInt32>(*rp))
        return -1;
      else if (static_cast<SInt32>(*lp) > static_cast<SInt32>(*rp))
        return 1;

      --lp; --rp;
    }

    for (; lp != &(this->_M_w[-1]); --lp, --rp) {
      if (*lp > *rp)
        return 1;
      else if (*lp < *rp)
        return -1;
    }
    return 0;		// must have been ==
  }

  SInt32 compare(const _Base_BitVector& __x) const
  {
    return _M_do_compare(__x);
  }

  bool isLessThan(const _Base_BitVector& __x) const {
    return this->_M_do_compare (__x) < 0;
  }
  bool isGreaterThan(const _Base_BitVector& __x) const {
    return this->_M_do_compare (__x) > 0;
  }

  bool _M_is_less(const _Base_BitVector& __x) const { 
    return _M_do_compare (__x) < 0;
  } 

  bool _M_is_equal(const _Base_BitVector& __x) const {
    return _M_do_compare (__x) == 0;
  }

  bool _M_is_any() const {
    FromWordC (0)
      if (*dp++)
        return true;

    return false;
  }

  // Base class ! returns TRUE if all zero, false otherwise
  bool operator!() const {
    return ! this->_M_is_any ();
  }

  // convert representation to double
  // basic algorithm is to get 64 bits of precision, make that a positive double
  // and apply scaling and sign alteration to get a double value.
  //
  CarbonReal _M_double (bool isSigned) const {
    bp_t msb = FLO ();
    if (msb == -1)
      return 0.0;
    if (isSigned && (msb == (_Nw*LONG_BIT)-1)) {
      // negative number...
      _Base_BitVector v (*this);
      v._M_do_neg ();
      return - v._M_double (false);
    } else {
      // a positive number.
      if (msb > 64) {           // scale 
        _Base_BitVector v (*this);
        v._M_do_right_shift (msb - 64);
        UInt64 value = v._M_w[0] + UInt64(v._M_w[1]) << LONG_BIT;
        return std::ldexp ((CarbonReal)value, msb-64);
      } else {
        return (CarbonReal) (this->_M_w[0] + UInt64 (this->_M_w[1]) << LONG_BIT);
      }
    }
  }

  // boolean coercion for someone writing if (bitvec) ...
  operator void* () const {
    UIntPtr ptrInt = ((this->_M_is_any ()) ? 1 : 0);
    return (void*)ptrInt;
  }

  UInt32 _M_do_count() const {
    UInt32 __result = 0;
    FromWordC (0)
      __result += carbon_popcount (*dp++);
    return __result;
  }

  //! Return the value array as an array of UInt32s
  UInt32* getUIntArray() {
    return &this->_M_w[0];
  }

  //! Return the number of UInt32s used by the value array
  UInt32 getUIntArraySize() {
    return _Nw;
  }

  //! return read-only array of UInt32s
  const UInt32* getUIntArray() const {
    return &this->_M_w[0];
  }

  // Bit seeking functions FFO, FFZ, FLO, FLZ

  bp_t FFO () const {
    for (UInt32 i=0; i< _Nw; ++i)
    {
      bp_t pos = carbon_FFO (this->_M_w[i]);
      if (pos >= 0)
      {
        pos += i*LONG_BIT;
        return pos;
      }
    }
    return -1;
  }

  bp_t FLO () const {
    for (UInt32 i=_Nw; i > 0; --i)
    {
      bp_t pos = carbon_FLO (this->_M_w[i-1]);
      if (pos >=0)
      {
        pos += (i-1)*LONG_BIT;
        return pos;
      }
    }
    return -1;
  }

  // These ONLY work for full word bitvectors
  bp_t FLZ () const {
    for (UInt32 i=_Nw; i>0; --i)
    {
      bp_t pos = carbon_FLO (~this->_M_w[i-1]);
      if (pos >= 0)
      {
        pos += (i-1)*LONG_BIT;
        return pos;
      }
    }
    return -1;
  }
    
  bp_t FFZ () const {
    for(UInt32 i=0; i<_Nw; ++i)
    {
      bp_t pos = carbon_FFO (~this->_M_w[i]);
      if (pos >= 0)
      {
        pos += i*LONG_BIT;
        return pos;
      }
    }
    return -1;
  }

};				// _Base_BitVector


#if pfMSVC
// template specializations broken in Visual C++?
template <UInt32 _Extrabits, bool _stype> struct _BVSanitize {
  
  // workaround for partial specialization
  template<bool _type> struct sanitize_choice {
    static void __do_sanitize(UInt32& __val)
      {
         if (_Extrabits)
           __val &= ~((~0U) << _Extrabits);
      }
  };
  // specialize for signed extn
  template<> struct sanitize_choice<true> {
    static void __do_sanitize(UInt32& __val)
      {
        if (_Extrabits)
          __val = ((SInt64)(SInt32(__val << (32 - _Extrabits)))) 
                                         >> (32 - _Extrabits);
      }
  };

  static void _M_do_sanitize(UInt32& __val)
  {
    sanitize_choice<_stype>::__do_sanitize(__val);
  }
};

#else
// ------------------------------------------------------------
//! Helper class to zero out the unused high-order bits in the highest word.
template <UInt32 _Extrabits, bool _st> struct _BVSanitize {
  static void _M_do_sanitize(UInt32& __val)
    { __val &= ~((~static_cast<UInt32>(0)) << _Extrabits); }
};

//! Specialization for BitVector<K * LONG_BIT>
template <> struct _BVSanitize<0, false> {
  static void _M_do_sanitize(UInt32) {}
};

template <> struct _BVSanitize<0, true> {
  static void _M_do_sanitize(UInt32) {}
};

//! Specialization for signed BitVector
template <UInt32 _Extrabits> 
struct _BVSanitize<_Extrabits, true> {
  static void _M_do_sanitize(UInt32& __val)
    { 
      __val = ((SInt64)(SInt32(__val << (32 - _Extrabits)))) >> (32 - _Extrabits);
    }
};

#endif

template <UInt32 _Nb, bool _stype=false > class BitVector;
template <UInt32 _Nb> class SBitVector;

//! Proxy representation for bits-within-BitVector object
/*!
 * It's critical to be aware that a BVref is a proxy object, and that a
 * temporary BVref still refers to a common (possibly readonly!) BitVector.
 *
 * Also observe that a BitVector must represent upto 65k bits, while the internal
 * storage for the bit-position in a BVref is a signed 16 bit value.  This is okay
 * because BVref's are always normalized to 0..31.  We pass in 32 bit positions so
 * that we can access any bit in a 65k vector.
 */
template<bool _stype=false>
class BVref {
  typedef typename SignExtendType<_stype>::T32 T32;
  typedef typename SignExtendType<_stype>::T64 T64;

public: CARBONMEM_OVERRIDES
  //! Type characteristics
  enum {eSigned = _stype};

  //! Address of word within a BitVector
  UInt32 *_M_wp;

  //! Starting bit offset - NOTE bit-packed to 16 bits!
  bp_t _M_bpos:16;
  //! Total size of bitfield selected.
  bs_t _M_bsiz:16;

  // should be left undefined
  BVref() {BVC (BVRefs,0);}

  // Constructor from a complete bitvector (no free coercions)
  //
  template<UInt32 K , bool _st> explicit BVref (const BitVector<K, _st>& B){
    BitVector<K, _st>& __b = const_cast<BitVector<K, _st>&>(B);
    _M_wp = &__b._M_w[0];
    _M_bpos = 0;
    _M_bsiz = K;
    BVC (BVRefs,K);
  }

  template<UInt32 K, bool _st> explicit BVref (BitVector<K, _st>& B):
    _M_wp (&B._M_w[0]), _M_bpos (0), _M_bsiz (K){BVC (BVRefs, K);}

  // Constructor from a bitvector with pos/size
  template<UInt32 K, bool _st> 
  BVref( const BitVector<K, _st>& __B, bp_t __pos, bs_t __siz ) {
    BVC (BVRefs, __siz);

    //NORMALIZE ME
    _M_wp = const_cast<UInt32*>(&__B._M_w[0]);
    _S_adjustPtrPosSize (_M_wp, __pos, __siz, K);

    _M_wp += _S_whichword (__pos);
    _M_bpos = _S_whichbit(__pos);
    _M_bsiz = __siz;
  }

  // Constructor from an opposite-sign extension BVref.
  //
  BVref ( const BVref<not _stype>& v)
    : _M_wp (v._M_wp), _M_bpos (v._M_bpos), _M_bsiz (v._M_bsiz) {
    BVC (BVRefs, this->_M_bsiz);
  }

  //! Constructor from simple array of UInt32
  /*
   * In this case we assume that the pos/size are within the actual
   * memory, as we have no other guarantees.
   */
  BVref (UInt32 *wp, bp_t pos, bs_t siz):
    _M_wp(wp), _M_bpos(pos%LONG_BIT), _M_bsiz(siz) {
    BVC (BVRefs, siz);
    if (pos < 0)
      _M_bpos = pos;
    else
      _M_wp += (pos/LONG_BIT);
  }


  // Copy Constructor
  BVref (const BVref& r):
    _M_wp (r._M_wp), _M_bpos (r._M_bpos), _M_bsiz (r._M_bsiz) {
    BVC (BVRefs, r._M_bsiz);
  }

  // Constructor with an altered size
  BVref (const BVref& r, bs_t size):
    _M_wp (r._M_wp), _M_bpos (r._M_bpos), _M_bsiz (size) {
    BVC (BVRefs, size);
  }
    
  // Default destructor
  ~BVref() {}

  // Advance the descriptor by 'width' bits, subtracting from the size
  void skipBits (UInt32 width)
  {
    if (width >= _M_bsiz) {
      _M_bsiz = 0; 
    } else {
      _M_wp += (_M_bpos + width) / LONG_BIT;
      _M_bpos = (_M_bpos + width) % LONG_BIT;
      _M_bsiz -= width;
    }
  }
    
  //! Any bits in the range non-zero?
  bool any (void) const
  {
    bp_t fraglen = _S_firstfrag (_M_bpos, _M_bsiz);

    if (*_M_wp & _S_maskbit (_M_bpos, fraglen))
      // Bits in the prefix fragment were set
      return true;

    fraglen=_M_bsiz - fraglen;

    // check aligned words
    SInt32 i = 1;
    for(; fraglen >= bp_t(LONG_BIT); ++i,fraglen -= LONG_BIT)
      if (_M_wp[i])
	return true;

    // check last fragment
    if (fraglen > 0)
      if (_M_wp[i] & _S_maskbit (0, fraglen))
	return true;

    return false;
  }

  // Bit seeking functions FFO, FFZ, FLO, FLZ

  bp_t FFO () const {
    bp_t fraglen = _S_firstfrag (_M_bpos, _M_bsiz);

    bp_t pos = carbon_FFO(*_M_wp & _S_maskbit (_M_bpos, fraglen));
    if (pos >= 0)
      return pos - _M_bpos;

    fraglen=_M_bsiz - fraglen;

    // check aligned words
    SInt32 i = 1;
    for(; fraglen >= (bp_t)LONG_BIT; ++i,fraglen -= LONG_BIT)
    {
      bp_t r = carbon_FFO (_M_wp[i]);
      if (r < 0)
        continue;

      return r + (i*LONG_BIT) - _M_bpos;
    }
      
    // check last fragment
    if (fraglen > 0)
    {
      bp_t r = carbon_FFO (_M_wp[i] & _S_maskbit (0, fraglen));
      if (r < 0)
        return r;
      return r + (i*LONG_BIT) - _M_bpos;
    }
    return -1;
  }

  bp_t FFZ () const {
    bp_t fraglen = _S_firstfrag (_M_bpos, _M_bsiz);

    bp_t pos = carbon_FFO ((~*_M_wp) & _S_maskbit (_M_bpos, fraglen));
    if (pos >= 0)
      return pos - _M_bpos;

    fraglen=_M_bsiz - fraglen;

    // check aligned words
    SInt32 i = 1;
    for(; fraglen >= (bp_t)LONG_BIT; ++i,fraglen -= LONG_BIT)
    {
      pos = carbon_FFO (~_M_wp[i]);
      if (pos < 0)
        continue;

      return pos + (i*LONG_BIT) - _M_bpos;
    }
      
    // check last fragment
    if (fraglen > 0)
    {
      pos = carbon_FFO ((~_M_wp[i]) & _S_maskbit (0, fraglen));
      if (pos < 0)
        return pos;
      return pos + (i*LONG_BIT) - _M_bpos;
    }
    return -1;
  }

  bp_t FLO () const {
    bp_t firstFrag = _S_firstfrag (_M_bpos, _M_bsiz); // offset to alignment
    bp_t lastFrag = (_M_bsiz - firstFrag) % LONG_BIT;

    // Calc last word of array involved in operation
    SInt32 i = _S_whichword (_M_bsiz + _M_bpos - 1);

    if (lastFrag)
    {
      bp_t pos = carbon_FLO (_M_wp[i] & _S_maskbit (0, lastFrag));
      if (pos>=0)
        return pos + (i*LONG_BIT) - _M_bpos;
      --i;
    }

    for(; i > 0; --i)
    {
      bp_t pos = carbon_FLO (_M_wp[i]);
      if (pos < 0)
        continue;
      return pos + (i*LONG_BIT) - _M_bpos;
    }

    bp_t pos = carbon_FLO (_M_wp[0] & _S_maskbit (_M_bpos, firstFrag));
    if (pos >= 0)
      pos -= _M_bpos;

    return pos;
  }

  bp_t FLZ () const {
    bp_t firstFrag = _S_firstfrag (_M_bpos, _M_bsiz); // offset to alignment
    bp_t lastFrag = (_M_bsiz - firstFrag) % LONG_BIT;

    // Calc last word of array involved in operation
    SInt32 i = _S_whichword (_M_bsiz + _M_bpos - 1);

    if (lastFrag)
    {
      bp_t pos = carbon_FLO ((~_M_wp[i]) & _S_maskbit (0, lastFrag));
      if (pos>=0)
        return pos + (i*LONG_BIT) - _M_bpos;
      --i;
    }

    for(; i > 0; --i)
    {
      bp_t pos = carbon_FLO (~_M_wp[i]);
      if (pos < 0)
        continue;
      return pos + (i*LONG_BIT) - _M_bpos;
    }


    bp_t pos = carbon_FLO ((~_M_wp[0]) & _S_maskbit (_M_bpos, firstFrag));
    if (pos >= 0)
      pos -= _M_bpos;

    return pos;
  }

  //! count bits in the range
  UInt32 count (void) const
  {
    bp_t fraglen = _S_firstfrag (_M_bpos, _M_bsiz);
    UInt32 cnt =  carbon_popcount(*_M_wp & _S_maskbit (_M_bpos, fraglen));

    fraglen=_M_bsiz - fraglen;

    SInt32 i = 1;
    for(; fraglen >= (bp_t)LONG_BIT; ++i,fraglen -= LONG_BIT)
      cnt += carbon_popcount (_M_wp[i]);

    if (fraglen > 0)
      cnt += carbon_popcount (_M_wp[i] & _S_maskbit (0, fraglen));

    return cnt;
  }

  UInt32 redxor (void) const
  {
    bp_t fraglen = _S_firstfrag (_M_bpos, _M_bsiz);
    UInt32 result =  _M_wp[0] & _S_maskbit (_M_bpos, fraglen);

    fraglen=_M_bsiz - fraglen;

    SInt32 i = 1;
    for(; fraglen >= (bp_t)LONG_BIT; ++i,fraglen -= LONG_BIT)
      result ^= _M_wp[i];

    if (fraglen > 0)
      result ^= (_M_wp[i] & _S_maskbit (0, fraglen));

    return carbon_redxor (result);
  }

  //! Set bits in the range
  void set (void)
  {
    UInt32 *p = _M_wp;
    bp_t fraglen = _S_firstfrag (_M_bpos, _M_bsiz);

    // Initial fragment
    if (fraglen)
      *p++ |= _S_maskbit (_M_bpos, fraglen);

    fraglen=_M_bsiz - fraglen;

    // Aligned words
    for(; fraglen >= (bp_t)LONG_BIT; fraglen -= LONG_BIT)
      *p++ = ~0u;

    // Trailing fragment
    if (fraglen > 0)
      *p |= _S_maskbit (0, fraglen);
  }

  //! Clear bits in the range
  void reset (void)
  {
    UInt32* p = _M_wp;
    bp_t fraglen = _S_firstfrag (_M_bpos, _M_bsiz);

    // Initial fragment
    if (fraglen)
      *p++ &= ~_S_maskbit (_M_bpos, fraglen);

    fraglen=_M_bsiz - fraglen;

    // Aligned words
    for(; fraglen >= (bp_t)LONG_BIT; fraglen -= LONG_BIT)
      *p++ = 0;

    // Trailing fragment
    if (fraglen > 0)
      *p &= ~_S_maskbit (0, fraglen);
  }

  //! Toggle all the bits in the range.
  void flip() {
    UInt32 *p = _M_wp;
    bp_t fraglen = _S_firstfrag (_M_bpos, _M_bsiz);

    // Initial fragment
    if (fraglen)
      *p++ ^= _S_maskbit (_M_bpos, fraglen);

    fraglen=_M_bsiz - fraglen;

    // Aligned words
    for(; fraglen >= (bp_t)LONG_BIT; fraglen -= LONG_BIT)
      *p++ ^= ~0u;

    // Trailing fragment
    if (fraglen > 0)
      *p ^= _S_maskbit (0, fraglen);
  }


  //! Move bitfields.  Must handle unaligned and overlapping fields.
  void anytoany (BVref src); // NB. call by value

  // Templatize BVref assignments

  template <int I, typename T> struct BVAssign {
    // Default template for any non-specialized source.  This can't be a good
    // thing, so make the default fail gloriously.
    static void assign (BVref&, const T& )
    {
      INFO_ASSERT (0, "Failed to find specialized BVref::operator=(const T&)");
    }
  };

  // Partial specialization for BitVector types
  template <typename T> struct BVAssign<2, T> {

    // A bitvector has a size (in bits) and a signed-type.  Construct
    // a typedef that fully describes the bitvector
    typedef BitVector<T::Nb, T::SType> Vec;

    // Define the templated function for BitVector<N,S>
    static void assign (BVref& dest, const Vec& src) {
      BVC (RefEqBV, dest._M_bsiz);

      // Wrap the BitVector in an appropriately extending BVref constructor
      const BVref<Vec::SType> srcRef (src);

      // Just copy the source bits to the destination, extending as
      // required by the source if the destination operand is larger.
      dest.anytoany (srcRef);
    }
  };

  // Partial specialization for integral types.
  template <typename T> struct BVAssign<0, T> {
    static void assign (BVref& dest, T src) {
#ifdef BVPERF
      // BVPerf runtime determination whether we're doing 32 or 64 bit store
      BVPC->countRefEq (dest._M_bsiz, sizeof(T) == 8);
#endif

      enum {cIsSignedType = (T (~0) < T (1)),
            cIsUInt32 = (!cIsSignedType && (sizeof(T) == sizeof(UInt32)))
      };

      if (dest._M_bsiz > 1) { 
        typename SignExtendType<cIsSignedType>::T64 value;

        // Extend the source expression according to its type
        // (e.g. if SInt32, produce a SInt64, if T is a 64 bit type already,
        // the 'value' variable can be optimized away.
        value = src;

        UInt32 *wp = dest._M_wp;

        // Compute # of bits to store in the first word of the destination
	bs_t fraglen = _S_firstfrag (dest._M_bpos, dest._M_bsiz); 
	if (UInt32 (fraglen) < LONG_BIT) { 
          // A partial store into the MSBs of the low word of the dest
          UInt32 mask = _S_maskbit (dest._M_bpos, fraglen); 
          *wp &= ~mask; 
          *(wp++) |=  (value << dest._M_bpos) & mask; 
          value >>= fraglen; 
        } else { 
          // full word move 
          //
          *(wp++) = (UInt32) value; 
          if (cIsUInt32)
            value = 0;
          else
            value >>= LONG_BIT; 
        }
 
        // Now, the destination is word-aligned, if there are any bits left
        // to be stored into.  Compute how many more bits we have.
	fraglen = dest._M_bsiz - fraglen; 

        // Store words from the source into the destination
	while (fraglen >= LONG_BIT) { 
          *(wp++) = value;
          if (cIsUInt32)
            value = 0;
          else
            value >>= LONG_BIT;
          fraglen -= LONG_BIT;
        }

        // Store fractional bits into LSB's of most significant word of the
        // destination
        if (fraglen) {
          UInt32 mask = _S_maskbit (0, fraglen); 
          *wp &= ~mask; 
          *wp |= (value) & mask; 
        }
      } else if (dest._M_bsiz == 1) {
        // special case for single bit 
        if ( src & 1 ) 
          *dest._M_wp |= _S_maskbit(dest._M_bpos); 
        else 
          *dest._M_wp &= ~_S_maskbit(dest._M_bpos); 
      }
    }
  };

  // Partial specialization for BVref sources
  template <typename T> struct BVAssign<1, T > {
    typedef BVref<T::eSigned> BV;
    static void assign (BVref& dest, const BV& src) {
      BVC (RefEqBV, dest._M_bsiz);
      if (dest._M_bsiz == 1)
        if (src._M_wp[0] & _S_maskbit (src._M_bpos) )
          dest._M_wp[0] |= _S_maskbit (dest._M_bpos);
        else
          dest._M_wp[0] &= ~_S_maskbit (dest._M_bpos);
      else if (dest._M_bsiz > 1) {
        // Consider the cases
        //   DEST = UNSIGNED_SRC;  -- zero extend src if insufficient bits
        //   DEST = SIGNED_SRC;  -- sign extend src if insufficient bits
        if (dest._M_bsiz <= src._M_bsiz) {
          BVref srcTemp (src, dest._M_bsiz); // Just copy enough
          dest.anytoany (srcTemp); // no extensions required
        } else {
          BVref destfrag (dest, src._M_bsiz);
          // copy the real bits
          destfrag.anytoany (src);

          // if the dest was partially OOB, account for the negative
          // offset now
          SInt32 extendSize = (dest._M_bsiz - src._M_bsiz);
          if (dest._M_bpos < 0)
            extendSize += dest._M_bpos;
          if (extendSize < 0)
            extendSize = 0;

          // advance the destination pointer to the extend area and
          // set the size to the number of excess bits in the destination
          destfrag.lpartsel_advance (extendSize);

          if (not BV::eSigned || src.test (src._M_bsiz-1) == 0)
            // unsigned src or else positive signed source
            destfrag.reset ();  // clear the extra bits
          else
            destfrag.set ();    // propagate negative sign
        }
      }
    }
  };


  /*!
   * \em Warning: assignment doesn't change the BVref members
   */
  template <typename T>
  BVref& operator=(const T& src) {

    // Dispatch to type-specific implementation of assignment
    BVAssign<TypeDiscriminant<T>::eType, T>::assign (*this, src);

    return *this;
  }

  BVref& operator=(const BVref<true>& src)
  {
    BVAssign<1,BVref<true> >::assign (*this, src);
    return *this;
  }

  BVref& operator=(const BVref<false>& src)
  {
    BVAssign<1,BVref<false> >::assign (*this, src);
    return *this;
  }


  BVref& operator+=(T32 rval){
    BVC (RefPlusEq32, _M_bsiz);
    UInt32 shiftl = 0;
    bp_t pos = _M_bpos;
    bs_t size = _M_bsiz;
    _S_adjustPSC (65536, pos, size, shiftl);

    T64 sum = (T64)rval << pos;
    sum >>= shiftl;             // adjust for LSB OOB

    // Since we update the low bits below our position, include them in
    // the total fragment length.
    UInt32 fraglen = (pos + size);

    // Loop thru 32 bits at a time, quitting if the carry disappears or we run
    // out of bits.
    //
    UInt32* wp = _M_wp;
    for(bp_t i = fraglen; sum != 0 && i>0; i-=LONG_BIT)
      {
	sum += (UInt64)*wp;
	if ((UInt32)i < LONG_BIT)	// Only a small piece remains to be written
	  {
	    UInt32 mask = _S_maskbit (0, i);
	    *wp &= ~mask;
	    *wp++ |= (sum & mask);
	  }
	else
	    *wp++ = (UInt32)sum;

	sum >>= LONG_BIT;
      }

    return *this;
  }

  BVref& operator-=(T32 rval){
    BVC (RefMinusEq32, _M_bsiz);
    UInt32 shiftl = 0;
    bp_t pos = _M_bpos;
    bs_t size = _M_bsiz;
    _S_adjustPSC (65536, pos, size, shiftl);

    SInt64 diff = (T64)rval << pos;
    diff >>= shiftl;

    // Since we update the low bits below our position, include them in
    // the total fragment length.
    UInt32 fraglen = pos + size;

    // Loop thru 32 bits at a time, quitting if the carry disappears or we run
    // out of bits.
    //
    UInt32* wp = _M_wp;
    for(bp_t i = fraglen; diff != 0 && i>0; i-=LONG_BIT)
      {
	diff = (UInt64)*wp - diff;
	if ((UInt32)i < LONG_BIT)	// Only a small piece remains to be written
	  {
	    UInt32 mask = _S_maskbit (0, i);
	    *wp &= ~mask;
	    *wp++ |= (diff & mask);
	  }
	else
	    *wp++ = (UInt32)diff;

        if (diff < 0)
          diff = (diff >> LONG_BIT) | KUInt64(0xFFFFFFFF00000000) ;// sign ext
        else
          diff >>= LONG_BIT;
          
	diff = -diff;
      }

    return *this;
  }

  // bitwise operators are easier - their results don't propagate.
  BVref& operator|=(T32 rval){
    BVC (RefOrEq32, _M_bsiz);

    UInt32 shiftl = 0;
    bp_t pos = _M_bpos;
    bp_t size = _M_bsiz;

    _S_adjustPSC (65536, pos, size, shiftl);
    
    UInt32 masklen = BITVECTOR_MIN<bs_t> (size, bs_t (LONG_BIT));

    rval >>= shiftl;            // elide the OOB LSBs (if any)
    rval &= _S_maskbit (0, masklen);

    _M_wp[0] |= (rval << pos);

    // avoid shifting when pos==0 because LONG_BIT-0=32, and shifting by 32
    // yields undefined results in C. See bug7013
    if ((pos != 0) & ((UInt32)(pos + size) > LONG_BIT))
      _M_wp[1] |= (rval >> (LONG_BIT-pos));

    return *this;
  }

  BVref& operator^=(T32 rval){
    BVC (RefXOrEq32, _M_bsiz);
    UInt32 shiftl = 0;
    bp_t pos = _M_bpos;
    bs_t size = _M_bsiz;

    _S_adjustPSC (65536, pos, size, shiftl);

    UInt32 masklen = BITVECTOR_MIN (size, bs_t (LONG_BIT));

    rval >>= shiftl;
    rval &= _S_maskbit (0, masklen);

    _M_wp[0] ^= (rval << pos);

    // avoid shifting when pos==0 because LONG_BIT-0=32, and shifting by 32
    // yields undefined results in C.  See bug7013
    if ((pos != 0) & ((UInt32)(pos + size) > LONG_BIT))
      _M_wp[1] ^= (rval >> (LONG_BIT - pos));

    return *this;
  }

  BVref& operator&=(T32 rval){
    BVC (RefAndEq32, _M_bsiz);
    UInt32 shiftl = 0;
    bp_t pos = _M_bpos;
    bs_t size = _M_bsiz;

    _S_adjustPSC (65536, pos, size, shiftl);

    UInt32 masklen = BITVECTOR_MIN (size, bs_t (LONG_BIT));

    // Create mask with hole where value goes
    UInt64 mask = ~(((KUInt64(1) << masklen) - 1) << pos);

    rval >>= shiftl;
    
    // or in bits from rvalue to make composite mask
    mask |= ((UInt64)(rval) << pos);

    _M_wp[0] &= mask;

    if((UInt32)(_M_bpos + _M_bsiz) > LONG_BIT)
      _M_wp[1] &= (mask >> LONG_BIT);

    return *this;
  }

  BVref& operator<<=(UInt32 rval){
    // equivalent to a anytoany move with zeros to the low part
    if (rval >= _M_bsiz)
      *this = 0;                // shifted away all significance
    else
    {
      BVref dest(_M_wp, _M_bpos + rval, _M_bsiz - rval);
      dest = *this;
      BVref zeroPiece (_M_wp, _M_bpos, rval);
      zeroPiece = 0;
    }
    return *this;
  }

  BVref& operator>>=(UInt32 rval){
    // equivalent to anytoany move with zeros to the high part.
    if (rval >= _M_bsiz)
      *this = 0;                // shifted away all significance
    else
    {
      BVref src(_M_wp, _M_bpos + rval, _M_bsiz - rval); // select the bits kept
      *this = src;              // and just drop them in with zero extensions
    }
    return *this;
  }

  // signed right shift
  BVref& shiftRightArith (UInt32 count) {
    bool sign = this->test (_M_bsiz-1); // Was this signed?
    if (count >= _M_bsiz)       // shift it all away?
      if (sign)
        this->set ();
      else
        this->reset ();
    else {
      (*this) >>= (count);          // logical shift it 
      if (sign)
        // sign-extend the newly vacated MSBs
        BVref (_M_wp, _M_bpos + _M_bsiz - count, count).set ();
    }
        
    return *this;
  }


#define OP64(op) \
  BVref& operator op##=(T64 rval){		\
    this->operator op##=((T32) rval); 	\
						\
    if (_M_bsiz > LONG_BIT)						\
      {									\
	BVref newref (_M_wp, _M_bpos + LONG_BIT, _M_bsiz - LONG_BIT);	\
	newref op##= (T32)(rval >> LONG_BIT);			\
      }									\
    return *this;							\
  }

   OP64 (+)
   OP64 (-)
   OP64 (^)
   OP64 (|)
   OP64 (&)
#undef OP64

  // Because BVrefs are descriptors, and we don't want to allow randomly
  // writing into a source-expression, we don't implement the unary prefix
  // operators -,~,+
  // We do implement !, because it returns a bool
  //

  bool operator!() const {
    if (_M_bsiz == 1)
      return (_M_wp[0] & _S_maskbit(_M_bpos)) == 0;
    else
      // if any bits in the selection are 1's, then !X == 0
      return (!this->any ());
  }

  // WARNING - modifies in place!!
  void negate () {
    this->flip ();              // -x == (~x + 1)
    this->operator+=(1u);
  }

  //! boolean result for bit reference
  bool bvalue () const
  {
    if (_M_bsiz == 1)
      return (_M_wp[0] & _S_maskbit(_M_bpos)) != 0;
    else
      return this->any ();
  }

  //! functional converter from BVref to representable int
  T32 value () const
  {
    BVC (RefL, _M_bsiz);
    if (_M_bsiz == 1)
      return (_M_wp[0] & _S_maskbit(_M_bpos)) != 0;

    bp_t fraglen = _S_firstfrag (_M_bpos, _M_bsiz);

    UInt32 value = (_M_wp[0] >> _M_bpos) & _S_maskbit (0, fraglen);

    fraglen = _M_bsiz - fraglen;

    // avoid shifting when _M_bpos==0 because LONG_BIT-0=32, and shifting by 32
    // yields undefined results in C. See bug7013
    if (_M_bpos != 0) {
      value |= (_M_wp[1] & _S_maskbit (0, fraglen)) << (LONG_BIT - _M_bpos);
    }

    return value;
  }

  T32 value (bp_t pos, bs_t size) const
  {

    if (pos >= bp_t (_M_bsiz))
      return 0;

    BVC (RefL, size);
    size = BITVECTOR_MIN (size, bs_t (_M_bsiz - pos));
    BVref piece (_M_wp, pos + _M_bpos, size);

    return piece.value ();
  }

  UInt32 uvalue () const { return value (); }
  UInt64 ullvalue () const { return llvalue (); }

  T64 llvalue () const
  {
    BVC (RefLL, _M_bsiz);
    if (_M_bsiz == 1)
      return (_M_wp[0] & _S_maskbit(_M_bpos)) != 0;

    UInt64 temp = _M_wp[0];

    if (((UInt32)_M_bpos + (UInt32)_M_bsiz) > LONG_BIT)
      temp |= static_cast<UInt64>(_M_wp[1]) << LONG_BIT;

    temp >>= _M_bpos;

    if (((UInt32)_M_bpos + (UInt32)_M_bsiz) > (LLONG_BIT))
      temp |= static_cast<UInt64>(_M_wp[2]) << (LLONG_BIT - _M_bpos);

    if (_M_bsiz < LLONG_BIT)
      temp &= (KUInt64(1) << _M_bsiz)-1;

    return temp;
  }

  UInt32 uvalue (bp_t pos, bs_t size) const { return value (pos, size); }

  UInt64 ullvalue (bp_t pos, bs_t size) const { return llvalue (pos, size); }

  T64 llvalue (bp_t pos, bs_t size) const
  {
    if (!CARBON_NO_OOB && pos >= bp_t (_M_bsiz))
      return 0;

    BVC (RefLL, size);
    size = BITVECTOR_MIN (size, bs_t (_M_bsiz - pos));
    BVref piece (_M_wp, pos + _M_bpos, size);

    return piece.llvalue ();
  }

  // returns this as a real value
  CarbonReal rvalue() const
  {
    // get the most significant bits, up to 64 of them.
    // bp_t msb = _M_bpos + _M_bsiz - 1;
    // bp_t lsb = BITVECTOR_MAX ( (msb-63), _M_bpos );
    bs_t size = BITVECTOR_MIN<bs_t> ( _M_bsiz , bs_t (64U) );
    bp_t pos2 = BITVECTOR_MAX<bp_t> ( bp_t (_M_bsiz-64) , bp_t (0) );
    T64 data64 = (this->llvalue(pos2, size));	/* T64 is unsigned or signed as needed */ 
    CarbonReal r_temp = (CarbonReal) data64;
    const SInt32 unconverted_bits = _M_bsiz - size;
    if ( unconverted_bits > 0 )
      r_temp = std::ldexp(r_temp, unconverted_bits);
    return r_temp;
  }

  //! BVref inequalities
  bool operator<=(const BVref& r) const
  {
    return cmp (r) <= 0;
  }

  bool operator>=(const BVref& r) const
  {
    return cmp (r) >= 0;
  }

  bool operator>(const BVref& r) const
  {
    return cmp (r) > 0;
  }

  bool operator<(const BVref& r) const
  {
    return cmp (r) < 0;
  }

  //! BVref relationals with U32 and U64
#define BVrelop(op, TYPE)               \
  bool operator op(TYPE r) const        \
  {                                     \
    BitVector<64> rbv (r);              \
    return cmp (BVref (rbv, 0, sizeof(TYPE)*8)) op 0; \
  }
  
  BVrelop (<, UInt32)
  BVrelop (>, UInt32)
  BVrelop (>=, UInt32)
  BVrelop (<=, UInt32)
  BVrelop (!=, UInt32)
  BVrelop (!=, SInt32)

  BVrelop (<, UInt64)
  BVrelop (>, UInt64)
  BVrelop (>=, UInt64)
  BVrelop (<=, UInt64)
  BVrelop (!=, UInt64)
  BVrelop (!=, SInt64)
#undef BVrelop

  //! BVref equalities...
  bool operator==(const BVref& r) const {
    return cmp (r) == 0;
  }

  // Returns less-than-zero, zero or greater-than-zero result to represent
  // unsigned relationals
  int cmp (const BVref& r) const
  {
    bs_t cmpSize;

    if (_M_bsiz > r._M_bsiz)
      // check that bigger has high zeros
      {
	// descriptor for excess bits
	if (BVref(_M_wp, _M_bpos + r._M_bsiz, _M_bsiz - r._M_bsiz).any ())
	  return 1;
	cmpSize = r._M_bsiz;
      }
    else if (_M_bsiz < r._M_bsiz)
      {
	if (BVref (r._M_wp, r._M_bpos + _M_bsiz, r._M_bsiz - _M_bsiz).any ())
	  return -1;
	cmpSize = _M_bsiz;
      }
    else
      cmpSize = _M_bsiz;

    // Check for easy case - aligned operands.
    if (_M_bpos == r._M_bpos) {
      bp_t firstfrag = _S_firstfrag (_M_bpos, cmpSize); // low-order bits fragment...
      bp_t lastfrag = (cmpSize - firstfrag) % LONG_BIT; // high-order bits fragment

      // Calc last word of array involved in operation
      SInt32 msbWord = _S_whichword (cmpSize + _M_bpos - 1);

      if (lastfrag) {
        // check the MSB fragment (if any)
        UInt32 mask = _S_maskbit (0, lastfrag);
        SInt64 delta = (UInt64(_M_wp[msbWord] & mask) - UInt64(r._M_wp[msbWord] & mask));
        if (delta < 0)
          return -1;
        else if (delta > 0)
          return 1;

        --msbWord;              // work our way down...
      }

      for(; msbWord > 0; --msbWord) {
        SInt64 delta = UInt64(_M_wp[msbWord]) - UInt64(r._M_wp[msbWord]);
        if (delta < 0)
          return -1;
        else if (delta > 0)
          return 1;
      }

      // Check LSB fragment (if any)
      if (firstfrag) {
        UInt32 mask = _S_maskbit (0, firstfrag);
        SInt64 delta = UInt64(_M_wp[0] & mask) - UInt64(r._M_wp[0] & mask);

        if (delta < 0)
          return -1;
        else if (delta > 0)
          return 1;
      }

      // If we fall thru, then they must be equal...
      return 0;
    }


    BVC (Ref2EqRefUA, cmpSize);

    // Worst case, unaligned but equal in size.

    // Grab 32 bits-at-a-time if possible
    while (cmpSize >= LONG_BIT) {
      cmpSize -= LONG_BIT;

      SInt64 delta =  this->llvalue (cmpSize, LONG_BIT) - r.llvalue (cmpSize, LONG_BIT);
      if (delta < 0)
        return -1;
      else if (delta > 0)
        return 1;
    }

    // Might still be a tiny piece left in lsbs
    if (cmpSize)
    {
      SInt32 delta = this->llvalue(0,cmpSize) - r.llvalue (0,cmpSize);
      if (delta < 0)
        return -1;
      else if (delta > 0)
        return 1;
    }

    // Didn't find any differences
    return 0;
  }

  //! Useful companion
  template <UInt32 K, bool _st> 
  bool operator==(const BitVector<K, _st>& rhs) const
  {
    BVref r (rhs);
    return *this == r;
  }

  bool operator==(T64 rhs) const;
  bool operator==(T32 rhs) const {
    return operator==(T64 (rhs));
  }
    
#define REFOPDECLS \
	bs_t cpySize = BITVECTOR_MIN(this->_M_bsiz,rhs._M_bsiz);	    \
	BVref dstTemp (*this, BITVECTOR_MIN (cpySize, bs_t(LONG_BIT)));	    \
	BVref srcTemp (rhs, BITVECTOR_MIN (cpySize, bs_t(LONG_BIT)))


#define REFOPLOOP \
    for(UInt32 pieces = cpySize; (SInt32)pieces>0;		\
 	pieces -= LONG_BIT,				\
	dstTemp.lpartsel_advance (BITVECTOR_MIN (pieces, UInt32(LONG_BIT))), \
	srcTemp.partsel_advance (BITVECTOR_MIN (pieces, UInt32(LONG_BIT))))
  
  BVref& operator+=(const BVref& rhs) {
    BVC (RefPlusEqRef, _M_bsiz);
    REFOPDECLS;
    SInt64 carry = 0;

    REFOPLOOP
      {
	carry += (UInt64)dstTemp.value () + srcTemp.value ();
	dstTemp = carry;
	carry >>= LONG_BIT;
      }

    return *this;
  }

  BVref& operator-=(const BVref& rhs) {
    BVC (RefMinusEqRef, _M_bsiz);
    REFOPDECLS;
    SInt64 carry = 0;

    REFOPLOOP
      {
	carry += (UInt64)dstTemp.value () - srcTemp.value ();
	dstTemp = carry;
	carry >>= LONG_BIT;
      }

    return *this;
  }

  BVref& operator|=(const BVref& rhs) {
    BVC (RefOrEqRef, _M_bsiz);
    REFOPDECLS;

    REFOPLOOP
      {
	dstTemp |= srcTemp.value ();
      }

    return *this;
  }

  BVref& operator^=(const BVref& rhs) {
    BVC (RefXOrEqRef, _M_bsiz);
    REFOPDECLS;

    REFOPLOOP
      {
	dstTemp ^= srcTemp.value ();
      }

    return *this;
  }

  BVref& operator&=(const BVref& rhs) {
    BVC (RefAndEqRef, _M_bsiz);
    REFOPDECLS;

    REFOPLOOP
      {
	dstTemp &= srcTemp.value ();
      }

    // If the destination is LARGER than RHS, we must zero propagate
    if (this->_M_bsiz > cpySize)
      {
	BVref filler (&_M_wp[_S_whichword (_M_bpos + cpySize)],
		      _S_whichbit (_M_bpos + cpySize),
		      _M_bsiz - cpySize);
	filler = 0;		// Zero fill to end
      }
      
    return *this;
  }
#undef REFOPLOOP
#undef REFOPDECLS

  BVref& operator%=(const BVref& divisor) {
    // Only necessary for modulus
    SInt32 width = divisor.FFO ();  // X MOD 2**N => X & MASK(N)
    if (width >= _M_bsiz)
      return *this;             // modulus larger than quotient
    ldeposit (width, _M_bsiz - width, 0);
    return *this;
  }

  BVref& operator/=(const BVref& divisor) {
    SInt32 width = divisor.FFO ();
    if (__SIGNED_TYPE && this->test (_M_bsiz-1)) {
      // signed divide R / 2**K  => (R+(2**K)-1) >> K
      (*this)-=1;
      (*this)+= divisor;
      (*this).shiftRightArith (width);
    } else
      (*this)>>= width;

    return *this;
  }

  // BVRef /= integral type
  BVref& operator/= (const UInt32 divisor)
  {
    SInt32 width = carbon_FFO (divisor);
    if (__SIGNED_TYPE && this->test (_M_bsiz-1)) {
      (*this) -= T32 (1);
      (*this) += T32 (divisor);
      (*this).shiftRightArith (width);
    } else {
      (*this) >>= width;
    }
    return *this;
  }

  BVref& operator/= (const UInt64 divisor)
  {
    SInt32 width = carbon_DFFO (divisor);
    if (__SIGNED_TYPE && this->test (_M_bsiz-1)) {
      (*this) -= T32 (1);
      (*this) += T64 (divisor);
      (*this).shiftRightArith (width);
    } else {
      (*this) >>= width;
    }
    return *this;
  }

  BVref& operator*=(const BVref&) {
    INFO_ASSERT (0, "partselect multiplies not implemented");
    return *this;
  }

  BVref& operator*=(T32 mpy) {
    return this->operator*=(T64 (mpy));
  }

  BVref& operator*=(T64 mpy)
  {
#if pfGCC
    UInt32 size = __BITVECTOR_WORDS (_M_bsiz);
    UInt32 multiplicand [size];
    BVref temp(multiplicand, 0, size*LONG_BIT);
    temp = (*this); // copy to temp
    UInt32 multiplier[2];
    multiplier[0] = mpy;
    multiplier[1] = mpy>>LONG_BIT;
    if (_stype)
      carbon_signed_multiply (multiplicand, multiplier, multiplicand,
                              size, 2, size);
    else
      carbon_multiply (multiplicand, multiplier, multiplicand, size, 2, size);
    *this = temp;
#else
    INFO_ASSERT (0, "partselect multiplies not implemented");
#endif    
    return *this;
  }


  //! relative advance of BVref to new [p][s] access; used only by concatenation
  // since we are advancing in contigous pieces, we should always
  // see the new pos as equal to the old (size)
  const BVref& partsel_advance (bs_t size) {
    // Advance pointer to beyond last size accessed
    _M_wp = &_M_wp[_S_whichword((UInt32)_M_bpos + (UInt32)_M_bsiz)];
    _M_bpos = _S_whichbit((UInt32)_M_bpos + (UInt32)_M_bsiz);
    _M_bsiz = size;

    return *this;
  }

  // a field reference to the destination (writeable)
  BVref& lpartsel_advance (bs_t size) {
    // Advance pointer to beyond last size accessed
    _M_wp = &_M_wp[_S_whichword((UInt32)_M_bpos + (UInt32)_M_bsiz)];
    _M_bpos = _S_whichbit((UInt32)_M_bpos + (UInt32)_M_bsiz);
    _M_bsiz = size;

    return *this;
  }

  //! Address a subfield of a field - used for nested NUVarselLvalue's
  /*!
   *! Should this skip any of the size checks?
   */
  BVref& lpartsel_unchecked (bp_t pos, bs_t size) {
    return lpartsel(pos, size);
  }

  // Address a subfield of a field - used for nested NUVarselLvalue's
  BVref& lpartsel (bp_t pos, bs_t size) {
    if (pos >= bp_t (_M_bsiz) || pos < 0)
      _M_bsiz = 0;
    else if (bp_t(pos + size) > bp_t (_M_bsiz))
      _M_bsiz = (_M_bsiz - pos);
    else
      _M_bsiz = size;

    _M_wp = &_M_wp[_S_whichword (_M_bpos + pos)];
    _M_bpos = _S_whichbit (_M_bpos + pos);
    return *this;
  }

  //! Convert a BVref back to the address of the BitVector.
  /*!
   * Used by cbuild by adding a reinterpret_cast<BitVector<K>*>()
   * wrapper.
   */
  template <typename T> T* base (bp_t pos) const{
    return reinterpret_cast<T*>(_M_wp - (pos/LONG_BIT));
  }
    
  // BVref OP= BITVECTOR....
#define TEMPLATE_REF_OP_BV(OP) \
template <UInt32 K, bool _st>                                           \
BVref & operator OP(const BitVector<K, _st>& rhs) {                     \
    const BVref handle(rhs);						\
    return this->operator OP(handle);					\
  }

  TEMPLATE_REF_OP_BV (+=)
  TEMPLATE_REF_OP_BV (-=)
  TEMPLATE_REF_OP_BV (&=)
  TEMPLATE_REF_OP_BV (|=)
  TEMPLATE_REF_OP_BV (^=)

#undef TEMPLATE_REF_OP_BV

  // Conversion operators
  template <UInt32 K> operator const BitVector<K>() const {BitVector<K> tmp (*this); return tmp;}

  inline bool test (bp_t __pos) const
  {
    BVref aBit (_M_wp, _M_bpos + __pos, 1);
    return ((*aBit._M_wp) & _S_maskbit (aBit._M_bpos)) != 0;
  }

  inline bool checked_test (bp_t __pos) const {
    if (CARBON_NO_OOB)
      return test (__pos);
    else
      return (__pos >= bp_t (_M_bsiz) || __pos < 0) ? false : test (__pos);
  }

  void lldeposit (bp_t pos, bs_t size, UInt64 value)
  {
    _S_adjustPosSizeValue (pos, size, _M_bsiz, value);
    if (size == 0)
      return;

    BVC (RefLL, size);
    BVref piece (_M_wp, pos + _M_bpos, size);
    piece = value;
  }

  void ldeposit (bp_t pos, bs_t size, UInt32 value)
  {
    _S_adjustPosSizeValue (pos, size, _M_bsiz, value);
    if (size == 0)
      return;

    size = BITVECTOR_MIN (size, bs_t (_M_bsiz - pos));
    BVC (RefLL, size);
    BVref piece (_M_wp, pos + _M_bpos, size);
    piece = value;
  }

};				// struct BVref

// Helper functions supporting T32/T64 relop BVref (we DON'T want to use
// operators.h for these because they implement <= in terms of separate < and == tests.)
template <typename T, bool S>
inline bool operator <(const T& lop, const BVref<S>& rop) { return rop > lop; }

template <typename T, bool S>
inline bool operator <=(const T& lop, const BVref<S>& rop) { return rop >= lop; }

template <typename T, bool S>
inline bool operator ==(const T& lop, const BVref<S>& rop) { return rop == lop; }

template <typename T, bool S>
inline bool operator >(const T& lop, const BVref<S>& rop) { return rop < lop; }

template <typename T, bool S>
inline bool operator >=(const T& lop, const BVref<S>& rop) { return rop <= lop; }

template <typename T, bool S>
inline bool operator !=(const T& lop, const BVref<S>& rop) { return !(rop == lop); }


// ------------------------------------------------------------
//! Class BitVector - implement extended precision support.
//! Arguments \a _Nb may be any nonzero number of bits.
//!           \a _stype is false for unsigned vectors and true for signed vectors
//! Carbon only uses BitVector for sizes larger than 64 bits.
//
template <UInt32 _Nb, bool _stype>
class BitVector :
  public
  _Base_BitVector<__BITVECTOR_WORDS(_Nb), _stype>
{
public: CARBONMEM_OVERRIDES
  enum {Nb = _Nb, SType=_stype, Nw = __BITVECTOR_WORDS (_Nb)};
  typedef BitVector basetype;
  typedef BitVector* ptrtype;
  typedef typename SignExtendType<_stype>::T32 T32;
  typedef typename SignExtendType<_stype>::T64 T64;

  typedef _Base_BitVector<__BITVECTOR_WORDS(_Nb), _stype> _Base;
  typedef BitVector masktype;   // trait for MASK operations (cf. carbon_priv.h)
 
  //! Clear extra bits in BitVector
  void _M_do_sanitize() {
    _BVSanitize<_Nb%LONG_BIT, _stype>::_M_do_sanitize(this->_M_hiword());
  }


//  BitVector* operator&() { return this; }

//  const BitVector * operator&() const { return this; }

  //! BitVectors are implemented as arrays of UInt32
  friend class BVref<_stype>;


  // BVref construction
  BVref<_stype> lpartsel(bp_t pos, bs_t size) {

    SInt32 shiftl = 0;

    // Bounds-check this partsel against the bitvector we're selecting from
    // and set the position and size to inbound values.  
    _S_adjustPSC (_Nb, pos, size, shiftl);

    BVref<_stype> result(&this->_M_w[0], pos, size);

    // shiftl counts the number of low-order 'x' values that were OOB
    // Put this back into the partselect, so that when we eventually assign
    // to it we can correctly discard the OOB bits.  We have to do this
    // adjustment NOW, because this is our only chance to know the net
    // boundaries.
    if (!CARBON_NO_OOB && size != 0) {
      result._M_bpos -= shiftl;
      result._M_bsiz += shiftl;
    }
    return result;
  }

  // Special checked BVref construction for const BitVector
  const BVref<_stype> partsel(bp_t pos, bs_t size) const {
    UInt32* ptr = const_cast<UInt32*>(&this->_M_w[0]);
    _S_adjustPtrPosSize (ptr, pos, size, _Nb);
    return BVref<_stype>(ptr, pos, size);
  }

  // Special BVref construction for use as first word in concat
  BVref<_stype> lpartsel_unchecked(bp_t pos, bs_t size) {
    return BVref<_stype>(&this->_M_w[0], pos, size);
  }

  // Special unchecked BVref construction for const BitVector
  const BVref<_stype> partsel_unchecked(bp_t pos, bs_t size) const {
    return BVref<_stype>(const_cast<UInt32*>(&this->_M_w[0]), pos, size);
  }

  // Constructors

  //! Default (note no storage initialized except for excess bits
  BitVector(): _Base () {BVC (Default, _Nb); _M_do_sanitize (); }

  //! Copy constructor from same sign-type
  BitVector (const BitVector<_Nb, _stype>& val) 
    : _Base(val) {BVC (Copy, _Nb);} 


  //! Constructors for POD initializers
  explicit BitVector (UInt64 __val): _Base (__val) {BVC (Int, _Nb);}
  explicit BitVector (UInt32 __val): _Base (__val) {BVC (Int, _Nb);}
  explicit BitVector (UInt16 __val): _Base (UInt32 (__val)) {BVC (Int, _Nb);}
  explicit BitVector (UInt8 __val) : _Base (UInt32 (__val)) {BVC (Int, _Nb);}

  explicit BitVector (SInt64 __val): _Base (UInt64 (__val)) {BVC (Int, _Nb);}
  explicit BitVector (SInt32 __val): _Base (UInt32 (__val)) {BVC (Int, _Nb);}
  explicit BitVector (SInt16 __val): _Base (UInt32 (__val)) {BVC (Int, _Nb);}
  explicit BitVector (SInt8 __val) : _Base (UInt32 (__val)) {BVC (Int, _Nb);}

  explicit BitVector (bool val): _Base (UInt32(val)) { BVC (Int, _Nb);}

  //! Constructor given a different sized or signed BitVector&
  template <UInt32 K, bool stype2>
    explicit BitVector (const BitVector<K,stype2>& v):
    _Base((UInt32*)&v._M_w,
	  BITVECTOR_MIN (__BITVECTOR_WORDS (_Nb),
		    __BITVECTOR_WORDS (K)))
    {
      if (_Nb < K || (_Nb % LONG_BIT))
        // sanitize any extra bits copied in high word or any bits that come from
        // mismatched sign types.
        _M_do_sanitize ();
      BVC (Copy, K);
    }

  //! Constructor with an array of UInt32s as initializers.
  // We supply an unused second parameter to avoid confusions
  // between pointers and a coercible UInt32 or UInt64 literal.
  //
  BitVector (const UInt32* array, UInt32 len): _Base(array, len)
    {
      BVC (Array, _Nb);

      // Array initializer could easily have bits set that aren't in
      // bitvector (c.f. shell code that initializes using array of -1's
      _M_do_sanitize ();
    }

  //! Constructor with a same-signed BVref to a bitvector as initializer
  //
  explicit BitVector (const BVref<_stype>& v):
    _Base ()			// no init, will overwrite excess
    {
      BVC (Array, _Nb);
      if (T32 (~0) < T32 (1))
        // SBitVector
        this->operator=(BVref<_stype>(v._M_wp, v._M_bpos, v._M_bsiz));
      else
        this->operator=(v);

      _M_do_sanitize ();    // Must clear extra bits on construction
    }

  //! Constructor with alternative signed BVref to a bitvector as initializer
  explicit BitVector (const BVref<not _stype>& v):
    _Base ()			// no init, will overwrite excess
    {
      BVC (Array, _Nb);
      if (T32 (~0) < T32 (1))   // SBitVector
        this->operator=(v);
      else
        this->operator=(BVref<_stype>(v._M_wp, v._M_bpos, v._M_bsiz));

      _M_do_sanitize ();    // Must clear extra bits on construction
    }

  // Simple assignment
  BitVector& operator=(const BitVector& rhs) {
    if (this != &rhs)
      ::memcpy (&this->_M_w[0], &rhs._M_w[0],
                  sizeof(UInt32)*__BITVECTOR_WORDS (_Nb));
    return *this;
  }

  // Assignment from alternate sign object
  BitVector& operator=(const BitVector<_Nb, not _stype>& src)
    {
      ::memcpy (&this->_M_w[0], &src._M_w[0],
                sizeof(UInt32)*__BITVECTOR_WORDS (_Nb));
      _M_do_sanitize ();
      return *this;
    }

  BitVector& operator=(UInt64 rhs) {
    this->_M_w[0] = (UInt32)rhs;

    if (__BITVECTOR_WORDS (_Nb) > 1)
      this->_M_w[1] = (rhs >> LONG_BIT);

    if (__BITVECTOR_WORDS (_Nb) > 2)
      this->zextend (2);
    else
      _M_do_sanitize ();

    return *this;
  }

  BitVector& operator=(SInt64 rhs) {
    this->_M_w[0] = (UInt32)rhs;
    
    if (__BITVECTOR_WORDS (_Nb) > 1)
      this->_M_w[1] = (rhs >> LONG_BIT);

    if (__BITVECTOR_WORDS (_Nb) > 2)
      _Base::sextend (1);

    this->_M_do_sanitize ();

    return *this;
  }

  BitVector& operator=(SInt32 rhs) {
    this->_M_w[0] = (UInt32)rhs;
    if (this->Nw > 1)
      _Base::sextend (0);
    
    this->_M_do_sanitize ();
    return *this;
  }

  BitVector& operator=(UInt32 rhs) {
    this->_M_w[0] = rhs;
    if (this->Nw > 1)
      this->zextend (1);
    else
      _M_do_sanitize ();

    return *this;
  }

  // assignment from unsigned-partselect
  BitVector& operator=(const BVref<false>& rhs) {
    BVC (BVEqRef, _Nb);
    BVref<_stype> srctmp (rhs._M_wp, rhs._M_bpos, BITVECTOR_MIN<bs_t> (rhs._M_bsiz, bs_t (LONG_BIT)));
    UInt32 *wp = &this->_M_w[0];

    bp_t size = BITVECTOR_MIN<bs_t> (rhs._M_bsiz, _Nb);
    while (size > 0)
      {
	*wp++ = srctmp.value ();
	size -= LONG_BIT;

        // Note the unsigned BITVECTOR_MIN here doesn't cause trouble
        // because if size goes negative we will discard the partsel
        // adjustment we just computed.
	srctmp.partsel_advance (BITVECTOR_MIN<bs_t> (size, bs_t (LONG_BIT)));
      }

    // We might not have initialized the whole array - zero any leftovers.
    ::memset (wp, 0, (&this->_M_w[this->Nw] - wp) * sizeof(UInt32));
    this->_M_do_sanitize ();
    return *this;
  }

  // assignment from signed partselect
  BitVector& operator=(const BVref<true>& rhs) {
    BVC (BVEqRef, _Nb);
    BVref<_stype> srctmp (rhs._M_wp, rhs._M_bpos, BITVECTOR_MIN <bs_t>(rhs._M_bsiz, bs_t (LONG_BIT)));
    UInt32 *wp = &this->_M_w[0];

    bp_t size = BITVECTOR_MIN<bs_t> (rhs._M_bsiz, _Nb);
    while (size > 0)
      {
	*wp++ = srctmp.value ();
	size -= LONG_BIT;

        // Note the unsigned BITVECTOR_MIN here doesn't cause trouble
        // because if size goes negative we will discard the partsel
        // adjustment we just computed.
	srctmp.partsel_advance (BITVECTOR_MIN<bs_t> (size, bs_t (LONG_BIT)));
      }

    // We might not have initialized the whole array - sign-extend

    ::memset (wp, (rhs.test (rhs._M_bsiz-1)) ? -1:0,
              (&this->_M_w[this->Nw] - wp) * sizeof(UInt32));
    this->_M_do_sanitize ();
    return *this;
  }

  template <UInt32 K, bool _st> BitVector& operator=(const BitVector<K, _st>& rhs)
    {
      // Assignment of different-sized bitvector
      // if src is smaller than dest, must pad destination, else
      // just block-move it
      if (K==_Nb)
	{
	  if (this->getUIntArray () == rhs.getUIntArray ())
	    return *this;

	  ::memcpy (this->getUIntArray(), rhs.getUIntArray(),
                    sizeof(UInt32)*__BITVECTOR_WORDS (_Nb));
          this->_M_do_sanitize ();
	}
      else
	{
          BVC (BVEqBVK, _Nb);
	  // Assignment of different-sized bitvector
	  // if src is smaller than dest, must pad destination, else
	  //
	  if (__BITVECTOR_WORDS (_Nb) > __BITVECTOR_WORDS (K))
	    {
	      ::memcpy (&this->_M_w[0], &rhs._M_w[0], sizeof(UInt32)* __BITVECTOR_WORDS (K));
              // icc will complain (incorrectly) about OOB index
              this->zextend (__BITVECTOR_WORDS (K));
	    }
	  else
	    {
	      ::memcpy (&this->_M_w[0], &rhs._M_w[0],
                        sizeof(UInt32) * __BITVECTOR_WORDS (_Nb));
	      this->_M_do_sanitize ();
	    }
	}

      return *this;
    }

  // 23.3.5.2 BitVector operations:

  // Treat BV <op> 64bit as BV <op> LOW32, BVOFFSET <op> HIGH32
  // where BVOFFSET is a BitVector that's offset by 32 bits
#define OP64(op,opname,san)						   \
  BitVector& operator op##=(const T64 rvalue) {		        	   \
    this->_M_do_##opname ((UInt32)rvalue);				   \
    typedef _Base_BitVector<BitVector::Nw-1, _stype> HiType;               \
    HiType *p=reinterpret_cast<HiType*>((void*)&this->_M_w[1]);            \
    p->_M_do_##opname ((T32)(rvalue >> LONG_BIT));      		   \
    if (san) this->_M_do_sanitize ();					   \
    return *this;							   \
  }

  BitVector& operator+=(const BitVector& __rhs) {
    this->_M_do_add(__rhs);
    this->_M_do_sanitize ();
    return *this;
  }

  BitVector& operator+=(T32 rvalue) {
    this->_M_do_add (rvalue);
    this->_M_do_sanitize ();
    return *this;
  }

  OP64 (+, add, true)

  BitVector& operator-=(const BitVector& __rhs) {
    this->_M_do_sub(__rhs);
    this->_M_do_sanitize ();
    return *this;
  }

  OP64 (-, sub, true)

  BitVector& operator-=(T32 rvalue) {
    this->_M_do_sub (rvalue);
    this->_M_do_sanitize ();
    return *this;
  }

  BitVector& operator/=(const T32 divisor) {
    if (__SIGNED_TYPE && test (_Nb-1)) {
      (*this)+= divisor - 1;
      SInt32 count = carbon_FFO (divisor);
      BVref<_stype> self(*this);
      self.shiftRightArith (count); // shift ourself inplace
    } else {
      this->_M_do_div (UInt32 (divisor));
    }
    return *this;
  }

  BitVector& operator/=(const T64 divisor) {
    if (__SIGNED_TYPE && test (_Nb-1)) {
      (*this) += divisor - 1;
      SInt32 count = carbon_DFFO (divisor);
      BVref<_stype> self(*this);
      self.shiftRightArith (count); // shift ourself inplace
    } else {
      this->_M_do_div (UInt64 (divisor));
    }
    return *this;
  }

  BitVector& pow (const T32 rhs) {
    *this = carbon_pow(*this, rhs);
    return *this;
  }

  BitVector& operator*=(T32 __rhs) {
    if (__SIGNED_TYPE) {
      carbon_signed_multiply (&(this->_M_w[0]), (UInt32*)&__rhs, &(this->_M_w[0]),
                              this->Nw, 1, this->Nw);
    }
    else {
      carbon_multiply (&(this->_M_w[0]), (UInt32*)&__rhs, &(this->_M_w[0]),
                       this->Nw, 1, this->Nw);
    }
    this->_M_do_sanitize ();
    return *this;
  }

  BitVector& operator*=(T64 __rhs) {
    if (__SIGNED_TYPE) {
      carbon_signed_multiply (&(this->_M_w[0]), (UInt32*)&__rhs, &(this->_M_w[0]), this->Nw, 2, this->Nw);
    }
    else {
      carbon_multiply (&(this->_M_w[0]), (UInt32*)&__rhs, &(this->_M_w[0]), this->Nw, 2, this->Nw);
    }
    this->_M_do_sanitize ();
    return *this;
  }
    

  // Does NxN -> N word multiply
  BitVector& operator*=(const BitVector& __rhs) {
    if (__SIGNED_TYPE) {
      carbon_signed_multiply (&(this->_M_w[0]), &__rhs._M_w[0], &(this->_M_w[0]),
                              this->Nw, this->Nw, this->Nw);
    }
    else {
      carbon_multiply (&(this->_M_w[0]), &__rhs._M_w[0], &(this->_M_w[0]),
                       this->Nw, this->Nw, this->Nw);
    }
    this->_M_do_sanitize ();
    return *this;
  }

  // Mixed A[N] x B[K] => A[N]
  template <UInt32 K> BitVector& operator*= (const BitVector<K>& rhs) {
    if (__SIGNED_TYPE) {
      carbon_signed_multiply(&(this->_M_w[0]), &rhs._M_w[0], &(this->_M_w[0]),
                             this->Nw, rhs.Nw, this->Nw);
    }
    else {
      carbon_multiply (&(this->_M_w[0]), &rhs._M_w[0], &(this->_M_w[0]),
                       this->Nw, rhs.Nw, this->Nw);
    }
    this->_M_do_sanitize ();
    return *this;
  }

  template <UInt32 K> BitVector& operator|=(const BitVector<K>& rhs) {
    for (bp_t i=0; i<(bp_t)K; i++) {
      bool lhsBit = this->test(i);
      bool rhsBit = rhs.test(i);
      if ( lhsBit || rhsBit ) {
	this->set(i,1);
      }
    }
    return *this;
  }

  // fast multiply-by-10.  We should make integerMult in general
  // be about this fast.
  void multBy10() {
    *this *= T32 (10);                // Think this is now pretty fast....
  }

  // Clear bits of the vector starting at POS
  void modBy2N (bp_t pos) {
    if (pos > 0) {
      this->lpartsel (pos, bs_t ((this->Nw*LONG_BIT) - pos)) = 0;
    }
  }


  // Only handle powers of two
  BitVector& operator%=(const BitVector& __rhs) {
    modBy2N (__rhs.FLO ());
    return *this;
  }

  BitVector& operator%=(T64 rhs) {
    // Only valid for powers of two...
    modBy2N (carbon_DFLO (rhs));
    return *this;
  }

  BitVector& operator%=(T32 rhs) {
    modBy2N (carbon_FLO (rhs));
    return *this;
  }

  // Only need support divide by 2**N
  BitVector& operator/=(const BitVector& __rhs) {
    UInt32 count = __rhs.FFO ();
    if (__SIGNED_TYPE && test (_Nb-1)) {
      (*this)+= __rhs;
      (*this)-= T32 (1);
      (*this).rightShiftArith (count);
    } else {
      this->_M_do_right_shift (count);
    }
    return *this;
  }

  BitVector& operator&=(const BitVector& __rhs) {
    this->_M_do_and(__rhs);
    return *this;
  }
  
  BitVector& operator|=(const BitVector& __rhs) {
    this->_M_do_or(__rhs);
    this->_M_do_sanitize ();	// src might be partsel of larger thing
    return *this;
  }

  BitVector& operator^=(const BitVector& __rhs) {
    this->_M_do_xor(__rhs);
    this->_M_do_sanitize ();
    return *this;
  }

  // With a BVref
  BitVector& operator&=(const BVref<_stype>& rhs) {
    BVref<_stype> (*this).operator&=(rhs);

    return *this;
  }
   
  BitVector& operator|=(const BVref<_stype>& rhs) {
    BVref<_stype> (*this).operator|=(rhs);

    return *this;
  }

  BitVector& operator^=(const BVref<_stype>& rhs) {
    BVref<_stype> (*this).operator^=(rhs);

    return *this;
  }

  BitVector& operator+=(const BVref<_stype>& rhs) {
    BVref<_stype> (*this).operator+=(rhs);

    return *this;
  }

  BitVector& operator-=(const BVref<_stype>& rhs) {
    BVref<_stype> (*this).operator-=(rhs);
    
    return *this;
  }

#undef OP64

#define OP64(op,sanflag)			       	\
  BitVector& operator op##=(const T64 rvalue) {	        \
    this->_M_w[0] op##= rvalue;				\
    if (_Nb > LONG_BIT)                                 \
    this->_M_w[1] op##= (rvalue >> LONG_BIT);      	\
    if (((1 op 0) == 0) || __SIGNED_TYPE) {             \
      T32 hi_word = (rvalue >> LONG_BIT);               \
      FromWord(2)                                       \
        *dp++ op##= ((T64)hi_word >> LONG_BIT);         \
    }                                                   \
    if(sanflag) this->_M_do_sanitize();			\
    return *this;					\
  }

  // With a T32
  BitVector& operator&=(T32 __rhs) {
    this->_M_w[0] &= __rhs;
    FromWord (1)
      *dp++ &= ((T64)__rhs >> LONG_BIT);
    return *this;
  }

  OP64 (&,false);
  
  BitVector& operator|=(T32 __rhs) {
    this->_M_w[0] |= __rhs;
    if (__SIGNED_TYPE)
    {
      FromWord (1)
        *dp++ |= ((T64)__rhs >> LONG_BIT);
    }
    this->_M_do_sanitize ();
    return *this;
  }

  OP64 (|, true)

  BitVector& operator^=(T32 __rhs) {
    this->_M_w[0] ^= __rhs;
    if (__SIGNED_TYPE) 
    {
      FromWord (1)
        *dp++ ^= ((T64)__rhs >> LONG_BIT);
    }
    return *this;
  }

  OP64 (^, false)
#undef OP64

  BitVector& operator<<=(UInt32 __pos) {
    this->_M_do_left_shift(__pos);
    this->_M_do_sanitize();
    return *this;
  }

  // Sanitize BEFORE shifting, since vector might have had sign bits set
  // if it's really a SBitVector.
  BitVector& operator>>=(UInt32 __pos) {
    if (__SIGNED_TYPE && __pos != 0) {
      UInt32 frac = _Nb % 32;     // excess bits in high word?
      if (frac)
        this->_M_w[this->Nw-1] &= _S_maskbit (0, frac); // clear any signs
    }

    this->_M_do_right_shift(__pos);
    return *this;
  }

  //! Logical negation - true iff input all zeros
  bool operator !(void) const
    {
      return not this->any ();
    }

  //
  // Extension:
  // Versions of single-bit set, reset, flip, test
  //
  BitVector& set(UInt32 __pos) {
    this->get_word(_S_whichword (__pos)) |= _S_maskbit(__pos);
    return *this;
  }

  BitVector& checked_set (UInt32 __pos) {
    if (!CARBON_NO_OOB && __pos >= _Nb)
      return *this;
    else
      return set (__pos);
  }

  void negate() {
    this->_M_do_neg ();
    this->_M_do_sanitize ();
  }

  BitVector& set(bp_t __pos, SInt32 __val) {
    // Use word manipulations instead of an if statement to get less
    // branching in the generated code
    UInt32& theWord = this->get_word (_S_whichword (__pos));
    theWord = (theWord & ~ _S_maskbit(__pos)) | ((__val & 0x1) << _S_whichbit(__pos));
    return *this;
  }

  BitVector& checked_set (bp_t __pos, SInt32 __val) {
    if (!CARBON_NO_OOB && UInt32 (__pos) >= _Nb)
      // Single test ignores negative or too-large positive bitoffset
      return *this;
    else
      return set (__pos, __val);
  }

  BitVector& set (bp_t pos, BVref<_stype>& val) {
    return set (pos, val.partsel (0,1));
  }

  BitVector& checked_set (bp_t pos, BVref<_stype>& val) {
    return checked_set (pos, val.partsel (0,1));
  }

  BitVector& reset(bp_t __pos) {
    this->get_word(_S_whichword (__pos)) &= ~ _S_maskbit(__pos);
    return *this;
  }

  BitVector& flip(bp_t __pos) {
    this->get_word(_S_whichword (__pos)) ^= _S_maskbit(__pos);
    return *this;
  }

  // Unguarded bit-test
  inline bool test(bp_t __pos) const
  {
    return ((this->get_word(_S_whichword (__pos)) & _S_maskbit (__pos)) != 0);
  }

  // Guarded version..
  inline bool checked_test (bp_t __pos) const {
    if (CARBON_NO_OOB)
      return test (__pos);
    else
      return (__pos >= bp_t (_Nb) || (__pos < 0)) ? false : test (__pos);
  }

  // Set, reset, and flip.

  BitVector& set() {
    this->_M_do_set();
    this->_M_do_sanitize();
    return *this;
  }

  BitVector& reset() {
    this->_M_do_reset();
    return *this;
  }

  // Modifies in place
  BitVector& flip() {
    this->_M_do_flip();
    this->_M_do_sanitize();
    return *this;
  }

  // Returns a COPY of the bitvector...
  BitVector operator~() const { 
    return BitVector(*this).flip();
  }

  // Returns a copy of the bitvector...
  BitVector operator-() const {
    BitVector result(*this);

    result._M_do_neg ();
    result._M_do_sanitize ();
    return result;
  }

  // conversion to double (handled signed vs. unsigned cases.
  CarbonReal realValue () const {
    return this->_M_double (__SIGNED_TYPE);
  }

  //! element access:
  //for b[i];
  BVref<_stype> operator[](bp_t pos) {return BVref<_stype> (*this, pos, 1);}
  //  BVref operator[](int pos) {return BVref (*this, pos);}
  
  bool operator[](bp_t __pos) const { return test(__pos); }

  UInt32 to_ulong() const { return this->_M_do_to_ulong(); }

  UInt32 count() const { return this->_M_do_count(); }

  UInt32 size() const { return _Nb; }

  bool operator==(const BitVector& __rhs) const {
    return _Base::_M_is_equal(__rhs);
  }
  bool operator<(const BitVector& __rhs) const {
    return _Base::_M_is_less(__rhs);
  }

  // Support ==, <, > with BVref's - operators.h will give us
  // the composite and inverse relations.
  bool operator==(const BVref<_stype>& rhs) const {
    return rhs.cmp (BVref<_stype>(*this)) == 0;
  }

  bool operator<(const BVref<_stype>& rhs) const {
    return BVref<_stype>(*this).cmp (rhs) < 0;
  }

  bool operator>(const BVref<_stype>& rhs) const {
    return BVref<_stype>(*this).cmp (rhs) > 0;
  }

  bool operator==(T64 rhs) const {
    if (this->_M_w[0] != UInt32(rhs & 0xffffffff) ||
        ((__BITVECTOR_WORDS (_Nb) > 1) && (this->_M_w[1] != UInt32((UInt64)rhs >> LONG_BIT))))
      return false;

    UInt32 sext = sign_extension<T64>(rhs);
    FromWordC (2)
      if (*dp++ != sext)
        return false;
    return true;
  }

  // relational operators between BitVector and UInt32
  bool operator==(T32 rhs) const {
    const UInt32* wp = &this->_M_w[0];
    if (*wp++ != (UInt32)rhs)
      return false;

    if (_stype && (rhs & 0x80000000)) // signed type and signed operand
      for (; wp != &this->_M_w[this->Nw]; ++wp) {
        if (*wp != UInt32(-1))
          return false;
      }
    else // unsigned type or signed type and unsigned operand
      for (; wp != &this->_M_w[this->Nw]; ++wp) {
        if (*wp)
          return false;
      }
    return true;              // all equal
  }

  bool operator<(T32 rhs) const {
    const UInt32* wp = &(this->_M_w[this->Nw-1]);

    T32 sign_ext = T32 (0);

    if (_stype) {
      if (rhs & 0x80000000)
        sign_ext = ~T32 (0);
      if (static_cast<T32>(*wp) < sign_ext)
        return true;
      else if (static_cast<T32>(*wp) > sign_ext)
        return false;
      --wp;
    }

    // Check that intermediate words all propagate sign
    for (; wp != &this->_M_w[0]; --wp)
    {
      if (_stype) {
        if ((*wp) < (UInt32)sign_ext)
          return true;
        else if ((*wp) > (UInt32)sign_ext)
          return false;
      } else {
        if (*wp)
          return false;
      }
    }

    return *wp < (UInt32)rhs;
  }

  bool operator>(T32 rhs) const {
    const UInt32* wp = &(this->_M_w[this->Nw-1]);

    T32 sign_ext = T32 (0);

    if (_stype) {
      if (rhs & 0x80000000)
        sign_ext = ~T32 (0);
      if (static_cast<T32>(*wp) < sign_ext)
        return false;
      else if (static_cast<T32>(*wp) > sign_ext)
        return true;
      --wp;
    }

    // Check that intermediate words all propagate sign
    for (; wp != &this->_M_w[0]; --wp)
    {
      if (_stype) {
        if ((*wp) < (UInt32)sign_ext)
          return false;
        else if ((*wp) > (UInt32)sign_ext)
          return true;
      } else {
        if (*wp)
          return true;
      }
    }

    return *wp > (UInt32)rhs;
  }

  bool any() const { return this->_M_is_any(); }
  bool none() const { return !this->_M_is_any(); }

  UInt32 redxor () const
    {
      UInt32 result = 0;

      FromWordC (0)
	result ^= *dp++;

      return carbon_redxor (result);
    }

  BitVector absolute()
    {
      BitVector result(*this);
      if (result.test(_Nb - 1))
        result._M_do_neg ();

      result._M_do_sanitize ();
      return result;
    }

  BitVector rotateRight(SInt32 rotates) 
    {
      BitVector result;
      rotates %= (SInt32)_Nb;
      if (rotates < 0)
      {
        // Equivalent to rotate Left
        result.lpartsel(0, -rotates) = this->partsel(_Nb + rotates, -rotates);
        result.lpartsel(-rotates,_Nb + rotates) = 
                                              this->partsel(0, _Nb + rotates);
      }
      else
      {
        result.lpartsel(0,_Nb - rotates) = 
                                      this->partsel(rotates, _Nb - rotates);
        result.lpartsel(_Nb - rotates, rotates) = this->partsel(0, rotates);
      } 
      return result;
    }

  BitVector rotateLeft(SInt32 rotates)
    {
      BitVector result;
      rotates %= (SInt32)_Nb;
      if (rotates < 0)
      {
        // Equivalent to rotate right
        result.lpartsel(0,_Nb + rotates) = 
                                       this->partsel(-rotates, _Nb + rotates);
        result.lpartsel(_Nb + rotates, -rotates) = this->partsel(0, -rotates);
      }
      else
      {
        result.lpartsel(0, rotates) = this->partsel(_Nb - rotates, rotates);
        result.lpartsel(rotates,_Nb - rotates) = 
                                            this->partsel(0, _Nb - rotates);
      }
      return result;
    }

  // For VERILOG >>>
  BitVector shiftRightArith (UInt32 count) const
  {
    BitVector result;
    if (count >= (SInt32)_Nb)
      count = _Nb;

    result.lpartsel(0, _Nb - count) = this->partsel(count, _Nb - count);
    if (test(_Nb - 1))
    {
      result.lpartsel(_Nb - count, count).set();
    }
    else
    {
      result.lpartsel(_Nb - count, count).reset();
    }
    return result;
  }

  // VHDL sra
  BitVector shiftRightArith(SInt32 count) const
  {
    BitVector result;
    if (count < 0)
    {
      // Equivalent to shift left arith
      if (-count > (SInt32)_Nb)
        count = -_Nb;
      result.lpartsel(-count, _Nb + count) = this->partsel(0, _Nb + count);
      if (test(0))
      {
        result.lpartsel(0, -count).set();
      }
      else
      {
        result.lpartsel(0, -count).reset();
      }
    }
    else
    {
      if (count > (SInt32)_Nb)
        count = _Nb;
      result.lpartsel(0, _Nb - count) = this->partsel(count, _Nb - count);
      if (test(_Nb - 1))
      {
        result.lpartsel(_Nb - count, count).set();
      }
      else
      {
        result.lpartsel(_Nb - count, count).reset();
      }
    }
    return result;
  } // shiftRightArith()

  BitVector shiftLeftArith(SInt32 count)
  {
    BitVector result;
    if (count < 0)
    {
      // Equivalent to shift right arith
      if (-count > (SInt32)_Nb)
        count = -_Nb;
      result.lpartsel(0, _Nb + count) = this->partsel(-count, _Nb + count);
      if (test(_Nb - 1))
      {
        result.lpartsel(_Nb + count, -count).set();
      }
      else
      {
        result.lpartsel(_Nb + count, -count).reset();
      }
    }
    else
    {
      if (count > (SInt32)_Nb)
        count = _Nb;
      result.lpartsel(count, _Nb - count) = this->partsel(0, _Nb - count);
      if (test(0))
      {
        result.lpartsel(0, count).set();
      }
      else
      {
        result.lpartsel(0, count).reset();
      }
    }
    return result;
  } // shiftLeftArith()

  BitVector vhdlShiftRightLogical(SInt32 count) const
  {
    BitVector result;
    if (count < 0)
    {
      // Equivalent to shift left logical
      if (-count > (SInt32)_Nb)
        count = -_Nb;
      result.lpartsel(-count, _Nb + count) = this->partsel(0, _Nb + count);
      result.lpartsel(0, -count).reset();
    }
    else
    {
      if (count > (SInt32)_Nb)
        count = _Nb;
      result.lpartsel(0, _Nb - count) = this->partsel(count, _Nb - count);
      result.lpartsel(_Nb - count, count).reset();
    }
    return result;
  } // vhdlShiftRightLogical()

  BitVector vhdlShiftLeftLogical(SInt32 count) const
  {
    BitVector result;
    if (count < 0)
    {
      // Equivalent to shift right logical
      if (-count > (SInt32)_Nb)
        count = -_Nb;
      result.lpartsel(0, _Nb + count) = this->partsel(-count, _Nb + count);
      result.lpartsel(_Nb + count, -count).reset();
    }
    else
    {
      if (count > (SInt32)_Nb)
        count = _Nb;
      result.lpartsel(count, _Nb - count) = this->partsel(0, _Nb - count);
      result.lpartsel(0, count).reset();
    }
    return result;
  } // vhdlShiftLeftLogical()

  // Function to resize the bitvector object by padding the extra
  // bits with the SIGN bit of the original data.
  BitVector vhdlExtension(UInt32 orgSize, UInt32 newSize, bool sign) const
  {
    BitVector result (*this);

    newSize = BITVECTOR_MIN (_Nb, newSize);
    orgSize = BITVECTOR_MIN (_Nb, orgSize);

    if (newSize >= orgSize) // we need to extend the result
    {
      if (sign && result.test(orgSize - 1))
      {
        result.lpartsel(orgSize, _Nb - orgSize).set();
      }
      else
      {
        result.lpartsel(orgSize, _Nb - orgSize).reset();
      }
    }
    else // We need to truncate the result, sign-extend from the new sign position
    {
      if (sign && result.test (newSize - 1))
        result.lpartsel (newSize, _Nb - newSize).set ();
      else
        result.lpartsel (newSize, _Nb - newSize).reset ();
    }
    result._M_do_sanitize ();   // Make sure excess bits are right...
    return result;
  } // vhdlExtension()

  // Note that these can't be in Base_BitVector because we need to know
  // where the uppermost bit is...
  bp_t FLZ () const {
    // Check first word avoiding any excess bits
    if (_Nb % LONG_BIT)
    {
      bp_t pos = carbon_FLO ((~this->_M_w[this->Nw-1]) & _S_maskbit (0, _Nb % LONG_BIT));
      if (pos >= 0)
        return pos + (LONG_BIT * (this->Nw-1));
    }

    // Careful, might be pathological 1 word bitvector?
    if (_Nb < LONG_BIT)
      return -1;

    // Do full word Base_BitVector operations of possibly smaller # of words.
    typedef const _Base_BitVector<_Nb/LONG_BIT, _stype> WordVec;
    WordVec* that = reinterpret_cast<WordVec*>(this);
    return that->FLZ ();
  }


  bp_t FFZ () const {
    if (_Nb >= LONG_BIT)
    {
      typedef const _Base_BitVector<_Nb/LONG_BIT, _stype> WordVec;
      WordVec* that = reinterpret_cast<WordVec*>(this);

      bp_t pos = that->FFZ ();
      if (pos >= 0)
        return pos;
    }

    // Now if there are excess bits
    if (_Nb % LONG_BIT)
    {
      bp_t pos = carbon_FFO ((~this->_M_w[this->Nw-1]) & _S_maskbit (0,_Nb % LONG_BIT));
      if (pos >= 0)
        return pos + (LONG_BIT * (this->Nw - 1));
    }

    return -1;
  }

  bool operator<(T64 rhs) const {
    BitVector bv(rhs);
    return *this < bv;
  }
  bool operator>(T64 rhs) const {
    BitVector bv(rhs);
    return *this > bv;
  }

  // define all relational operators in terms of 12 base operators
  // defined above: <,>,== T32, <,>,== T64, <,>,== BV, <,>,== BVref
  inline bool operator<=(const BitVector& x) const { return !(x < *this); }
  inline bool operator>=(const BitVector& x) const { return !(*this < x); }
  inline bool operator>(const BitVector& x)  const { return x < *this; }
  inline bool operator!=(const BitVector& x) const { return !(*this == x); }

  // macro to define all forms of relational operator overloads for a type
# define RELATIONAL_OPS(T) \
  inline bool operator!=(T x) const { return !(*this == x); } \
  inline bool operator<=(T x) const { return !(*this > x); } \
  inline bool operator>=(T x) const { return !(*this < x); } \
  friend bool operator<(T x, const BitVector& y) { return y > x; } \
  friend bool operator>(T x, const BitVector& y) { return y < x; } \
  friend bool operator!=(T x, const BitVector& y) { return !(y == x); } \
  friend bool operator==(T x, const BitVector& y) { return y == x; } \
  friend bool operator<=(T x, const BitVector& y) { return !(y < x); } \
  friend bool operator>=(T x, const BitVector& y) { return !(y > x); }

  RELATIONAL_OPS(T32)
  RELATIONAL_OPS(T64)
  RELATIONAL_OPS(const BVref<_stype>&)

  // Define an operator for BV op T, based on BV op= T
# define BV_OP_T(op, eq_op, T) \
  friend BitVector operator op(const BitVector& lop, T rop) { \
    BitVector tmp(lop); \
    tmp eq_op rop; \
    return tmp; \
  }

  // Define an operator for T op BV where op is not commutative
# define NON_COMMUTE_T_OP_BV(op, eq_op, T) \
  friend BitVector operator op(T lop, const BitVector& rop) { \
    BitVector tmp(lop); \
    tmp eq_op rop; \
    return tmp; \
  }

  // With commutative operations, we can potentially save some word
  // accesses by considering only the bits in T without having to
  // extend them.
# define COMMUTATIVE_T_OP_BV(op, eq_op, T) \
  friend BitVector operator op(T lop, const BitVector& rop) { \
    BitVector tmp(rop); \
    tmp eq_op lop; \
    return tmp; \
  }

# undef RELATIONAL_OPS

  BV_OP_T(<<, <<=, UInt32)
  BV_OP_T(>>, >>=, UInt32)

  // define all the operators for a type
# define ALL_OPS_T(T) \
  COMMUTATIVE_T_OP_BV(+, +=, T) \
  NON_COMMUTE_T_OP_BV(-, -=, T) \
  COMMUTATIVE_T_OP_BV(*, *=, T) \
  NON_COMMUTE_T_OP_BV(%, %=, T) \
  NON_COMMUTE_T_OP_BV(/, /=, T) \
  COMMUTATIVE_T_OP_BV(|, |=, T) \
  COMMUTATIVE_T_OP_BV(&, &=, T) \
  COMMUTATIVE_T_OP_BV(^, ^=, T) \
  BV_OP_T(+, +=, T) \
  BV_OP_T(-, -=, T) \
  BV_OP_T(*, *=, T) \
  BV_OP_T(%, %=, T) \
  BV_OP_T(/, /=, T) \
  BV_OP_T(|, |=, T) \
  BV_OP_T(&, &=, T) \
  BV_OP_T(^, ^=, T)

  ALL_OPS_T(T32)
  ALL_OPS_T(T64)
  ALL_OPS_T(const BVref<_stype>&)

  // We only need to define half of these for BitVectors
  BV_OP_T(+, +=, const BitVector&)
  BV_OP_T(-, -=, const BitVector&)
  BV_OP_T(*, *=, const BitVector&)
  BV_OP_T(%, %=, const BitVector&)
  BV_OP_T(/, /=, const BitVector&)
  BV_OP_T(|, |=, const BitVector&)
  BV_OP_T(&, &=, const BitVector&)
  BV_OP_T(^, ^=, const BitVector&)

# undef ALL_OPS_T
# undef BV_OP_T
# undef COMMUTATIVE_T_OP_BV
# undef NON_COMMUTE_T_OP_BV
};

#ifdef BITVECTOR_RVO
// Use RVO for unsigned shifts
template <UInt32 _Nb>
BitVector<_Nb, false> operator << (BitVector<_Nb, false>& value, UInt32 shift)
{
  typedef BitVector<_Nb, false> BV;

  BV dest;
  value._M_value_left_shift (dest, shift);
  dest._M_do_sanitize ();
  return dest;
}

template <UInt32 _Nb>
BitVector<_Nb, false> operator >> (BitVector<_Nb, false>& value, UInt32 shift)
{
  typedef BitVector<_Nb, false> BV;

  BV dest;
  value._M_value_right_shift (dest, shift);
  dest._M_do_sanitize ();
  return dest;
}
#endif


template<UInt32 _Nb>
class SBitVector : public  BitVector<_Nb, true>
{
    
public: CARBONMEM_OVERRIDES
  typedef BitVector<_Nb, true> _SBase;
  typedef SInt32 T32;
  typedef SInt64 T64;

  // Constructors
  
  //! Default (note no storage initialized except for excess bits
  SBitVector(): _SBase () { this->_M_do_sanitize (); } 

  //! Copy constructor (excess bits initialized from copy)
  SBitVector (const SBitVector& val)
    : _SBase(val) {}

  //! Constructor for LL initializer
  explicit SBitVector (SInt64 __val) : _SBase (__val) {_SBase::sextend (1);}  

  //! Constructor with int
  explicit SBitVector (SInt32 __val) : _SBase (__val) {_SBase::sextend (0);}
  explicit SBitVector (SInt8  __val) : _SBase (SInt32 (__val)) {_SBase::sextend (0);}
  explicit SBitVector (SInt16 __val) : _SBase (SInt32 (__val)) {_SBase::sextend (0);}

  //! Constructor with bool
  explicit SBitVector (bool __val) : _SBase (__val ? SInt32 (~0): SInt32(0)){
    _SBase::sextend (0);
  }

  explicit SBitVector (UInt64 __val) : _SBase (__val) { /*sextend (1);*/}
  explicit SBitVector (UInt32 __val) : _SBase (__val) { /*sextend (0);*/}
  explicit SBitVector (UInt16 __val) : _SBase (UInt32 (__val)) { /*sextend (0);*/}
  explicit SBitVector (UInt8  __val) : _SBase (UInt32 (__val)) { /*sextend (0);*/}


  //! Constructor given a different sized BitVector& 
  template <UInt32 K, bool _st>  
  explicit SBitVector (const BitVector<K, _st>& v): 
    _SBase((UInt32*)&v._M_w, 
          BITVECTOR_MIN (__BITVECTOR_WORDS (_Nb), 
                    __BITVECTOR_WORDS (K))) 
    { 
      if (_Nb > K) 
      { 
        // The new type is LARGER, so:
        // For SBV(BV<k>&)   , no sign extension needed, can zero fill
        // For SBV(SBV<k>&)  , do sign extension if msb of source was set
        // 
        SInt32 sign_ext = (v.test(K - 1))?  
           (static_cast<typename SignExtendType<_st>::T64>(typename SignExtendType<_st>::T32(~0)) >> LONG_BIT) : 0; 

        if (sign_ext) {
          // We already have zero's in the object if it's not sign-extending
          // otherwise just fill the piece that's negative.
          BVref<true> sext (*this,K, _Nb-K);
          sext.set ();
        }
      } 

      // Finally adjust any hidden bits....
      this->_M_do_sanitize ();  
    }  
 
  //! Constructor with an array of UInt32s as initializers.
  // We supply an unused second parameter to avoid confusions
  // between pointers and a coercible UInt32 or UInt64 literal.
  //
  SBitVector (const UInt32* array, UInt32 len): _SBase(array, len)
  {
    // Array initializer could easily have bits set that aren't in
    // bitvector (c.f. shell code that initializes using array of -1's
    this->_M_do_sanitize ();
  }

  //! Constructor with a BVref to a bitvector as initializer
  //
  explicit SBitVector (const BVref<true>& v):
    _SBase ()                    // no init, will overwrite excess
    {
      this->operator=(v);
      this->_M_do_sanitize ();         // sign extend extra bits
    }

  explicit SBitVector (const BVref<false>& v):
    _SBase ()                    // no init, will overwrite excess
    {
      this->operator=(v);
      this->_M_do_sanitize ();         // sign extend extra bits
    }

  
  SBitVector& operator=(SInt64 rhs) {
    this->_M_w[0] = (UInt32)rhs;
    
    if (__BITVECTOR_WORDS (_Nb) > 1)
      this->_M_w[1] = (rhs >> LONG_BIT);

    if (__BITVECTOR_WORDS (_Nb) > 2)
      _SBase::sextend (1);
    else
      this->_M_do_sanitize ();

    return *this;
  }

  SBitVector& operator=(UInt64 rhs) {
    this->_M_w[0] = (UInt32)rhs;
    
    if (__BITVECTOR_WORDS (_Nb) > 1)
      this->_M_w[1] = (rhs >> LONG_BIT);

    if (__BITVECTOR_WORDS (_Nb) > 2)
      _SBase::zextend (2);
    else
      this->_M_do_sanitize ();

    return *this;
  }

  SBitVector& operator=(SInt32 rhs) {
    this->_M_w[0] = (UInt32)rhs;
    if (this->Nw > 1)
      _SBase::sextend (0);
    else
      this->_M_do_sanitize ();
    return *this;
  }

  SBitVector& operator=(UInt32 rhs) {
    this->_M_w[0] = (UInt32)rhs;
    
    if (__BITVECTOR_WORDS (_Nb) > 1)
      _SBase::zextend (1);
    else
      this->_M_do_sanitize ();

    return *this;
  }

  SBitVector& operator=(const BVref<true>& rhs) {
    BVC (BVEqRef, _Nb);
    BVref<true> srctmp (rhs._M_wp, rhs._M_bpos,
                        BITVECTOR_MIN ((UInt32)rhs._M_bsiz, bs_t (LONG_BIT)));
    UInt32 *wp = &this->_M_w[0];

    bp_t size = rhs._M_bsiz;
    while (size > 0)
      {
        *wp++ = srctmp.value ();
        size -= LONG_BIT;
        srctmp.partsel_advance (BITVECTOR_MIN (size, (bp_t)LONG_BIT));
      }

    // We might not have initialized the whole array - sign-ext any leftovers.
    _SBase::sextend (wp - &this->_M_w[0]);

    return *this;
  }

  SBitVector& operator=(const BVref<false>& rhs) {
    BVC (BVEqRef, _Nb);
    BVref<false> srctmp (rhs._M_wp, rhs._M_bpos,
                         BITVECTOR_MIN ((UInt32)rhs._M_bsiz, bs_t (LONG_BIT)));
    UInt32 *wp = &this->_M_w[0];

    bp_t size = rhs._M_bsiz;
    while (size > 0)
      {
        *wp++ = srctmp.value ();
        size -= LONG_BIT;
        srctmp.partsel_advance (BITVECTOR_MIN (size, (bp_t)LONG_BIT));
      }

    // We might not have initialized the whole array - zero-extend any leftovers.
    _SBase::zextend (wp - &this->_M_w[0]);

    return *this;
  }

  template <UInt32 K, bool _st> 
  SBitVector& operator=(const BitVector<K, _st>& rhs)
    {
      // Assignment of different-sized bitvector
      // if src is smaller than dest, must pad destination, else
      // just block-move it
      // If SBV = BV<k>   , zero pad.
      // If SBV = SBV<k>  , sign pad
      if (K==_Nb)
        {
          if (this->getUIntArray () == rhs.getUIntArray ())
            return *this;

          ::memcpy (this->getUIntArray(), rhs.getUIntArray(),
                    sizeof(UInt32)*__BITVECTOR_WORDS (_Nb));
          this->_M_do_sanitize ();
        }
      else
        {
          BVC (BVEqBVK, _Nb);
          // Assignment of different-sized bitvector
          // if src is smaller than dest, must pad destination, else
          //
          if (__BITVECTOR_WORDS (_Nb) > __BITVECTOR_WORDS (K))
            {
              SInt32 sign_ext = (rhs.test(K - 1))? 
                ((static_cast<typename SignExtendType<_st>::T64>(typename SignExtendType<_st>::T32(~0))) >> LONG_BIT)
                : 0;
              ::memcpy (&this->_M_w[0], &rhs._M_w[0],
                        sizeof(UInt32)* __BITVECTOR_WORDS (K));
              // Sanitize the last word copied                
              _BVSanitize<K%LONG_BIT, _st > ::_M_do_sanitize(
                                          this->_M_w[__BITVECTOR_WORDS (K) - 1]);
              ::memset (&this->_M_w[__BITVECTOR_WORDS (K)], 
                        sign_ext,
                        sizeof(UInt32) * 
                              (__BITVECTOR_WORDS (_Nb) - __BITVECTOR_WORDS (K)));
            }
          else
            {
              ::memcpy (&this->_M_w[0], &rhs._M_w[0],
                        sizeof(UInt32) * __BITVECTOR_WORDS (_Nb));
              this->_M_do_sanitize (); 
            }
        }

      return *this;
    }

  // Overload ~ and - operator as they return by value in BitVector
  SBitVector& flip() {
    this->_M_do_flip();
    this->_M_do_sanitize();
    return *this;
  }

  SBitVector operator~() const { 
    return SBitVector(*this).flip();
  }

  SBitVector operator-() const {
    SBitVector result(*this);

    result._M_do_neg ();
    result._M_do_sanitize ();
    return result;
  }

  // overload count() to not count Extrbits in hi-word
  UInt32 count (void) const
  {
    UInt32 __result = 0;
    SInt32 fraglen = _Nb;
    SInt32 i = 0;
    for (; (bp_t)LONG_BIT <= fraglen; i++,fraglen -= LONG_BIT)
      __result += carbon_popcount (this->_M_w[i]);

    if (fraglen > 0)
      __result += carbon_popcount (this->_M_w[i] & _S_maskbit (0, fraglen));

    return __result;
  }
};

// ------------------------------------------------------------

//
// 23.3.5.3 BitVector operations:
//


//! Overload binary *
template <UInt32 _Nb, bool _st>
BitVector<_Nb, _st> operator*(const BitVector<_Nb, _st>& __x,
				UInt32 __y) {
  BitVector<_Nb, _st> __result(__x);
  __result *= __y;
  return __result;
}

template <UInt32 _Nb, bool _st>
BitVector<_Nb, _st> operator*(const BitVector<_Nb, _st>& __x,
				const BitVector<_Nb, _st>& __y) {
  if (__y <= UtUINT32_MAX)
    {
      BitVector<_Nb, _st> __result (__x);
      return (__result *= __y.value ());
    }
  else
    {
      BitVector<_Nb, _st> __result(__y);
      return (__result *= __x.value ());
    }
}



//! Overload binary %
template <UInt32 _Nb, bool _st>
BitVector<_Nb, _st> operator%(const BitVector<_Nb, _st>& __x,
				const UInt32 __y) {
  UInt32 shifts = exact_log2 (__y);
  return BitVector<_Nb, _st>(__x[shifts][_Nb-shifts]);
}



// Need this to be AFTER the BitVector class declaration

template <>
inline bool BVref<false>::operator==(UInt64 rhs) const
{
   if (_M_bsiz > 64)
   {
     // check that excess bits are all zero
     if (BVref(_M_wp, _M_bpos + 64, _M_bsiz-64).any())
       return false;          // nope
   }
   return this->llvalue() == rhs;
}


template <>
inline bool BVref<true>::operator==(SInt64 rhs) const
{
   if (_M_bsiz > 64)
   {
     if (rhs < 0)
     {
       // check that excess bits are all sign bits.
       if (BVref(_M_wp, _M_bpos + 64,_M_bsiz-64).count() != UInt32(_M_bsiz - 64))
         return false;          // nope
     }
     else
     {
       // check that excess bits are all zeros
       if (BVref(_M_wp, _M_bpos + 64, _M_bsiz-64).any())
         return false;          // nope
     }
   }
   return this->llvalue() == rhs;
}

// Copy any [size or alignment] to any destination [size or alignment].
// Note that we modify the source descriptor as we go to track the pieces
// we haven't copied.
//
template<bool _stype>
inline void BVref<_stype>::anytoany (BVref<_stype> src)
  {
    // If we're performance monitoring, track BVref move src & dest
    BVC (AnyFrom, src._M_bsiz);
    BVC (AnyTo, _M_bsiz);

    if (not CARBON_NO_OOB) {
      // We could have a negative bit position (indicating an
      // partially OOB destination) and need to compensate for that.
      if (_M_bpos < 0) {
        SInt32 srcBitsLeft = src._M_bsiz + _M_bpos;
        if (srcBitsLeft > 0) {
          src.skipBits (-_M_bpos); 
          _M_bpos = 0; 
        } else if ((bs_t) (-_M_bpos) > _M_bsiz) {
          // All the source bits are ignored, but the the entire destination is
          // out of range. If we just let this fall through the the else
          // branch, then _M_bsiz + _M_bpos is negative but gets bound to the
          // unsigned size constructor parameter of the BVRef. This is not a
          // good thing!
          return;
        } else {
          // all the source bits are ignored 
          BVref dstTemp (_M_wp, 0, _M_bsiz + _M_bpos); 
          dstTemp.reset ();             // clear the remaining bits. 
          return; 
        } 
      }
    }

    // check for differing sizes!
    UInt32 cpySize = BITVECTOR_MIN (src._M_bsiz, _M_bsiz);

    if (cpySize == 0)
      // No source bits need moving - fall thru to end of function
      // where we check for excess destination bits needing to be zeroed
      ;
    else if (_M_bpos == src._M_bpos)
      // Totally aligned
      { 
        if (_M_wp == src._M_wp)
          // Copy to self - but still might be A[127:0] = A[63:0];
          goto zerofill;

        // We could have a partially OOB field (negative position)
        if (_M_bpos < 0) {
          // turn into simpler problem all inbound
          BVref dstTemp (_M_wp, 0, cpySize + _M_bpos);
          dstTemp = BVref (src._M_wp, 0, cpySize + _M_bpos);
          return;
        }

        // Move initial fragment, then move aligned words, and finally
        // move any trailing fragment.
        //
        UInt32 fraglen = _S_firstfrag (_M_bpos, cpySize);
        
        UInt32 idx=0;		// Track how much we've advanced thru
        
        if (fraglen < LONG_BIT)
          {
            UInt32 mask = _S_maskbit (_M_bpos, fraglen);
            _M_wp[0] &= ~mask;
            _M_wp[0] |= (src._M_wp[0] & mask);
            idx++;
          }
        else
          fraglen = 0;	// No fractional part moved yet.
        
        // Now move remaining words.
        for(fraglen = cpySize - fraglen;
            fraglen >= LONG_BIT; fraglen -= LONG_BIT)
          {
            _M_wp[idx] = src._M_wp[idx];
            idx++;
          }
        
        // Lastly move fragment
        if (fraglen > 0)
          {
            UInt32 mask = _S_maskbit (0, fraglen);
            _M_wp[idx] &= ~mask;
            _M_wp[idx] |= (src._M_wp[idx] & mask);
          }
      }
    else 
      // worst case access.
      // copy 32 bits at a time...
      {
        
        // Check for negative bitposition on dest and skip some or all of
        // the source.
        SInt32 tpos = _M_bpos;
        SInt32 spos = src._M_bpos;

        if (tpos < 0) {
          if (SInt32 (src._M_bsiz) < -tpos)
            cpySize = 0;        // skip over all the source
          else {
            cpySize += tpos;    // still some source bits to read
            spos -= tpos;       // skip over unused source
          }
          tpos = 0;             // where to resume writing
        }

        // Check for negative bitposition on src and skip some of dest
        if (spos < 0) {
          if ((SInt32)cpySize < -spos)  // all src bits to copy are OOB
            cpySize = 0;
          else {
            cpySize += spos;    // don't copy OOB bits
            tpos -= spos;       // write starting with inbound bits
          }
          spos = 0;
        }

#ifndef CARBON_BV_SMALL_ANYTOANY
        carbonInterfaceCpSrcRangeToDestRange(_M_wp, tpos,
                                             src._M_wp, spos,
                                             cpySize);
#else
        // Move a little piece to the destination to align it.
        UInt32 len = BITVECTOR_MIN (cpySize, UInt32 (LONG_BIT - tpos));
        BVref dstTemp (_M_wp, tpos, len); // Just enough to finish this word
        BVref srcTemp (src._M_wp, spos, len);
        
        UInt32 pieces = cpySize;
        if (tpos)		// Dest not aligned
          {
            dstTemp = srcTemp.value ();
            pieces -= len;
            
            len = BITVECTOR_MIN (pieces, UInt32 (LONG_BIT));
            dstTemp.lpartsel_advance (len);
            srcTemp.partsel_advance (len);
          }
        
        // Destination is now ALIGNED.  So move words to destination.
        
        while (pieces >= LONG_BIT) {
          *dstTemp._M_wp++ = srcTemp.value ();
          pieces -= LONG_BIT;
          srcTemp.partsel_advance (LONG_BIT);
        }
        
        // There's possibly one fractional piece left
        if (pieces)
          {
            dstTemp._M_bsiz = pieces; // amount to deposit
            srcTemp._M_bsiz = pieces;
            dstTemp = srcTemp.value ();
          }
#endif
      }

    // Now we've moved all the src bits that would fit.  If the dest
    // size is still larger, we should zero-fill those bits.
  zerofill:
    if (_M_bsiz > src._M_bsiz)
      {
	BVref filler (&_M_wp[_S_whichword (_M_bpos + src._M_bsiz)],
		      _S_whichbit (_M_bpos + src._M_bsiz),
		      _M_bsiz - src._M_bsiz);
        if (__SIGNED_TYPE)
        {
          // sign fill to end
	  filler = (src._M_wp[_S_whichword (src._M_bpos + src._M_bsiz - 1)] & 
                  _S_maskbit(_S_whichbit (src._M_bpos + src._M_bsiz -1))) ? -1:0; 
        }
        else
        {
          filler = 0;
        }

      }
  }


#ifdef CARBON_SAVE_RESTORE
// i/o support for save/restore
template <UInt32 _Nw, bool _stype> 
void writeToStream(UtOCheckpointStream* f, const _Base_BitVector<_Nw, _stype>&v) 
{
  for (UInt32 i = 0; i < _Nw; ++i)
    carbonInterfaceOCheckpointStreamWrite(f, &v._M_w[i], sizeof(UInt32));
}

template <UInt32 _Nw, bool _stype> 
void readFromStream(UtICheckpointStream* f, _Base_BitVector<_Nw, _stype>&v)
{
  for (UInt32 i = 0; i < _Nw; ++i)
    carbonInterfaceICheckpointStreamRead(f, &v._M_w[i], sizeof(UInt32));
}
#endif // CARBON_SAVE_RESTORE

//! write a BVref
class UtOStream;
UtOStream& operator<<(UtOStream&, const BVref<false>& v);
UtOStream& operator<<(UtOStream&, const BVref<true>& v);

#  undef __BITVECTOR_WORDS
#  undef BitVector
#  undef FromWordS
#  undef FromWordC
#  undef FromWord

/*
 * Undo the compile-time definition of EXPECTED_VALUE.
 */
#ifdef LOCALLY_DEFINED_EXPECTED_VALUE
#undef LOCALLY_DEFINED_EXPECTED_VALUE
#undef EXPECTED_VALUE
#endif

#endif /* __BitVector_h_ */
