// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __MemManager_h_
#define __MemManager_h_

#ifndef _CPP_CSTDDEF
#include <cstddef>             // for size_t
#endif

class UtOStream;
struct MemPool;

#ifndef __CarbonPlatform_h_
#include "util/CarbonPlatform.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif

/*!
  \file
  Memory management routines go here.  There are debugging routines to
  help trace memory allocations and frees.  

  One of the major benefits of owning our own memory management is
  the ability to histogram our memory usage with acceptable overhead.

  This is enabled by either passing the dumpfile name as the third parameter
  of the constructor, or by setting the environment variable CARBON_MEM_DUMP
  to the dumpfile name before starting up the process.  For example:

  \verbatim
    (gdb) set env CARBON_MEM_DUMP path_to_dumpfile
    (gdb) break ...
    (gdb) run ...
    (gdb) p CarbonMem::printAllocs(0)
  \endverbatim

  See the documentation for CarbonMem::printAllocs for details
*/

//! CarbonMem class -- this is a class because gdb is slow
//! calling global functions
class CarbonMem
{
public:  // do not use CARBONMEM_OVERRIDES
  //! Optional memory system initialization -- should be first call in main()
  /*! The memory system will self-initialize even if you do not call this, but
   *! this gives an opportunity for command-line based memory debugging
   *! startup, which is usually more convenient than using an environment
   *! variable.  If dumpFileName isn't NULL, memory dumping is enabled
   *! and dumped to the specified file.
   */
  CarbonMem(int* argc, char** argv, const char* dumpFileName);
  ~CarbonMem();

  //! Watch an address for when it gets allocated or freed, via breakpoint in CarbonMem::stopHere
  static void watch(void* ptr);

  //! Stop watching an address
  static void unwatch(void* ptr);

  //! check a pointer to see if it's on the watch list
  static void check(void* ptr);

  //! check a pointer to see if it's on the watch list
  static void scribble(void* ptr, size_t sz);

  //! Break on CarbonMem::stopHere if you want to know when a watched pointer is allocated or deleted
  static void stopHere();

  //! Function to return the number of bytes allocated
  static size_t getBytesAllocated();

  //! Function to get the number of bytes trapped in freelists
  static size_t getBytesTrapped();

  //! Function to get the maximum number of bytes allocated
  static size_t getHighWater();

  //! print out memory statistics
  static void printStats();

  //! Print memory-system information about a pointer.  Don't crash.
  static void printPtr(void* ptr);

  //! get information about a pointer.  don't crash.
  /*! Returns true if we know anything abou that pointer */
  static bool getPtrInfo(void* ptr, bool* isAllocated, size_t* size);

  //! Print detail about all the freed memory held in the system
  static void printDetail();

  //! record a checkpoint instruction in the memory dump database to create
  //! a snapshot of the memory during replay
  static void checkpoint(const char* filename);

  //! print out all allocated pointers of a given size.  size 0 means print all allocations
  static void printAllocations(size_t size);
    
  //! method to support STL allocator override -- free with deallocate(ptr,size)
  static void* allocate(size_t size);

  //! method to support STL allocator override -- the size is known by the caller
  static void deallocate(void* ptr, size_t);

  //! allocate memory that is freed with free(ptr)
  static void* malloc(size_t size);

  //! reallocate memory that was allocated with alloc(size)
  /*!
   *! Note that this is not identical to standard realloc(), because
   *! the old_size must be provided.  Memory allocated with
   *! CarbonMem::reallocate can be freed with carbonmem::deallocate, not
   *! CarbonMem::free.  The memory can originate from CarbonMem::allocate, not
   *! CarbonMem::malloc.
   */
  static void* reallocate(void* ptr, size_t old_size, size_t size);

  //! free without knowing the size
  static void free(void* ptr);

  //! release unused blocks back to the operating system
  static void releaseBlocks();

  //! override global operators new and delete (any ref to this is intended to force the linking libcarbonmem)
  static void overrideNewDelete();

  //! replay memory information from a dump file
  static bool replay(const char* memDumpFile, const char* executable,
                     int detail, bool listPointers);

  //! flush any open memory histogram dump database
  static void flush();

  //! cleanup the memory in preparation for exit or manual memory debugging
  /*! This is mainly intended to close a memory debugging database dump, in
   *! case one is open.
   */
  static void cleanup();

  //! access to copyright date
  static const char* CarbonGetCopyrightDateLine() {return sBuiltInCopyrightDate;};

  //! access to copyright notice
  static const char* CarbonGetCopyrightNotice() {return sBuiltInCopyrightNotice;};

  //! access to All rights reserved line
  static const char* CarbonGetRightsReservedLine() {return sBuiltInRightsReservedNotice;};

  //! access to Tech support information line
  static const char* CarbonGetTechSupportLine() {return sBuiltInTechSupportNotice;};

  //! has memory debugging been turned on?
  static bool isMemDebugOn();

  //! are we checking for memory leaks?
  bool isCheckingLeaks() const;

  //! turn off leak checking
  void disableLeakDetection();

  //! Are we running with purify?
  static bool isPurifyRunning();

  //! Are we running with valgrind?
  static bool isValgrindRunning();

  //! Round up an allocation size to align it
  static size_t roundup(size_t);

  //! Create and install a custom MemPool for this thread
  /*!
    This function is used to temporarily replace the MemPool object
    for a thread.  The ID is a pointer used to uniquely identify the
    caller so previously-created pools can be reused.  The caller must
    call restoreMemPool() when finished.

    This function is thread-safe in that it may be called
    simultaneously by multiple threads using different IDs.  It is not
    thread-safe if multiple threads use the same ID.
   */
  static MemPool* createMemPool(void* id);
  //! Restore a MemPool, replacing a previously-created one
  static void restoreMemPool(MemPool* pool);

private:
// do not remove or crypt the sBuiltIn* items, they are included so that
// executing the unix strings command on an executable or library will
// display a valid copyright notice.  They are in the memManager
// because that file will be included in all carbon products.
  static const char* sBuiltInCopyrightDate;
  static const char* sBuiltInCopyrightNotice;
  static const char* sBuiltInTechSupportNotice;
  static const char* sBuiltInRightsReservedNotice;

  bool mMemLeakCheck;
  size_t mAllocCount;
};

inline bool operator==(const CarbonMem&, const CarbonMem&) {return true;}

template<class _Tp>
struct CarbonSTLAlloc_base {
  typedef _Tp value_type;
};

#if (!pfMSVC && !pfCOWARE)
template<class _Tp>
struct CarbonSTLAlloc_base<const _Tp> {
  typedef _Tp value_type;
};
#endif
  
//! class to use for instantiating STL containers using carbon's allocator
template<class _Tp>
struct CarbonSTLAlloc : public CarbonSTLAlloc_base<_Tp>
{
  typedef CarbonSTLAlloc_base<_Tp> _base;
  typedef typename _base::value_type value_type;
  typedef size_t    size_type;
  typedef ptrdiff_t difference_type;
  typedef value_type*       pointer;
  typedef const value_type* const_pointer;
  typedef value_type&       reference;
  typedef const value_type& const_reference;

  template <class _Tp1> struct rebind {
    typedef CarbonSTLAlloc<_Tp1> other;
  };

  CarbonSTLAlloc() throw() {}
  CarbonSTLAlloc(const CarbonSTLAlloc&) throw() {}
// Don't include this for Visual C++ 6.0
#if !pfMSVC || _MSC_VER >= 1300
  template <class _Tp1> 
  CarbonSTLAlloc(const CarbonSTLAlloc<_Tp1>&) throw() {}
#endif
  ~CarbonSTLAlloc() throw() {}

  static pointer address(reference __x) { return &__x; }
#if !pfMSVC
  static const_pointer address(const_reference __x) { return &__x; }
#endif

  // __n is permitted to be 0.
  static _Tp* allocate(size_type __n, const void* = 0) {
    return __n != 0 
      ? reinterpret_cast<_Tp*>(CarbonMem::allocate(__n * sizeof(_Tp)))
      : 0;
  }

  bool operator==(const CarbonSTLAlloc&) const {return true;}
  bool operator!=(const CarbonSTLAlloc&) const { return false; }

  // __p is not permitted to be a null pointer.
  static void deallocate(pointer __p, size_type __n)
  { CarbonMem::deallocate(__p, __n * sizeof(_Tp)); }

#if pfGCC_2
  static void deallocate(void* __p, unsigned int __n)
  { CarbonMem::deallocate((pointer) __p, (size_type) __n * sizeof(_Tp)); }
#endif

  static size_type max_size() throw() 
  { return size_t(-1) / sizeof(_Tp); }

  static void construct(pointer __p, const _Tp& __val) { new(__p) _Tp(__val); }
  static void destroy(pointer __p) { __p->~_Tp(); }
};

// Intel 7.0 uses Plauger's C++ libraries, which seem to use <void>
// specialized allocators.  Add a template specialization to catch those.	
#if !pfMSVC
template<>
struct CarbonSTLAlloc<void>
{
  typedef void        value_type;
  typedef size_t    size_type;
  typedef ptrdiff_t difference_type;
  typedef void*       pointer;
  typedef const void* const_pointer;

  template <class _Tp1> struct rebind {
    typedef CarbonSTLAlloc<_Tp1> other;
  };

  CarbonSTLAlloc() throw() {}
  CarbonSTLAlloc(const CarbonSTLAlloc&) throw() {}
  template <class _Tp1> 
  CarbonSTLAlloc(const CarbonSTLAlloc<_Tp1>&) throw() {}
  ~CarbonSTLAlloc() throw() {}

};
#endif

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#endif // __MemManager_h_
