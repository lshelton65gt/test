// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _LOOP_SORTED_
#define _LOOP_SORTED_


#include "util/Iter.h"
#include "util/HashValue.h"
#include <algorithm>
#include "util/UtArray.h"

template<class _KeyType, class _HashType = HashPointer<_KeyType, 2> >
class LoopSorted: public Iter<_KeyType>::RefCnt
{
  struct CmpPtr
  {
    bool operator()(const _KeyType* p, const _KeyType* q) const {
      return mHashType.lessThan(*p, *q);
    }
    _HashType mHashType;;
  };

public: CARBONMEM_OVERRIDES
  typedef _KeyType value_type;
  typedef _KeyType reference;
  typedef _KeyType const_reference;

  template<class _Loop>
  LoopSorted(_Loop loop, size_t size = 0)
  {
    if (size != 0)
      mSorted.reserve(size);
        
    for (const _KeyType* p; loop(&p);)
      mSorted.push_back(const_cast<_KeyType*>(p));

    std::sort(mSorted.begin(), mSorted.end(), CmpPtr ());
    mPtr = mSorted.begin();
    mEnd = mSorted.end();
  }

  LoopSorted()
  {
  }

  value_type operator*() const {return **mPtr;}
  const reference getRef() const {return **mPtr;}
  bool atEnd() const {return mPtr == mEnd;}
  void operator++() {++mPtr;}
  bool operator()(_KeyType* ptr) {
    if (atEnd())
      return false;
    *ptr = **mPtr;
    ++mPtr;
    return true;
  }

  bool operator()(const _KeyType** ptr) {
    if (atEnd())
      return false;
    *ptr = *mPtr;
    ++mPtr;
    return true;
  }

  typedef typename UtArray<_KeyType*>::iterator iterator;
  //iterator begin() {return mSorted.begin();}
  //iterator end() {return mEnd;}

private:
  LoopSorted(const LoopSorted& src);      // forbid
  LoopSorted& operator=(const LoopSorted& src); // forbid

  UtArray<_KeyType*> mSorted;
  iterator mPtr;
  iterator mEnd;
}; // class LoopSorted: public Iter<_KeyType>::RefCnt

#endif
