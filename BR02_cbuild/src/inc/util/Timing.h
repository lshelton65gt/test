// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _TIMING_H_
#define _TIMING_H_


#include "util/CarbonPlatform.h"
#include "util/UtIOStream.h"

#if pfUNIX
//#include "util/UtIOStream.h"
#include <sys/times.h>
#include <unistd.h>

//! Timer class
class Timing
{
public: CARBONMEM_OVERRIDES
  //! constructor
  Timing()
  {
  }

  //! Begin time measurement
  void start()
  {
    mStartTicks = times(&mStartTimes);
  }

  //! End time measurement
  void end()
  {
    mEndTicks = times(&mEndTimes);
  }

  //! Print the end()-start() value
  void printDeltas()
  {
    UtOStream& screen = UtIO::cout();
    screen << "User=" << getUserDelta()
           << ", System=" << getSysDelta()
           << UtIO::endl;
  }

  void printAndReset(const char* label) {
    end();
    UtIO::cout() << label << ": ";
    printDeltas();
    start();
  }

  //! Just return the user time used
  int getUserDelta() {return mEndTimes.tms_utime - mStartTimes.tms_utime;}
  //! Just return the system time used
  int getSysDelta() {return mEndTimes.tms_stime - mStartTimes.tms_stime;}
  
private:
  Timing(Timing& src);
  Timing& operator=(Timing& src);

  time_t mStartTicks;
  time_t mEndTicks;
  struct tms mStartTimes;
  struct tms mEndTimes;
};

#else

class Timing
{
public: CARBONMEM_OVERRIDES
  //! constructor
  Timing()
  {
  }

  //! Begin time measurement
  void start()
  {
  }

  //! End time measurement
  void end()
  {
  }

  //! Print the end()-start() value
  void printDeltas()
  {
  }

  void printAndReset(const char*) {
  }

  //! Just return the user time used
  int getUserDelta() {return 0;}
  //! Just return the system time used
  int getSysDelta() {return 0;}
  
private:
  Timing(Timing& src);
  Timing& operator=(Timing& src);
};

#endif
#endif // _TIMING_H_

