// -*- C++ -*-
#ifndef __UtCLicense_h_
#define __UtCLicense_h_
/*****************************************************************************


 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/UtSimpleCrypt.h"

/*! Define a variable and initialize it with the encoded feature name
 *   VARIABLE_NAME - the name of the local variable to declare.
 *   FEATURE_NAME - the name of the feature to encode into the variable
 *   FEATURE_NAME_LENGTH - the length in bytes of the feature name (not counting '\0')
 */
#define CARBON_LICENSE_FEATURE(VARIABLE_NAME, FEATURE_NAME, FEATURE_NAME_LENGTH) \
  char VARIABLE_NAME[FEATURE_NAME_LENGTH + 1]; \
  { \
    int scramble_buf[FEATURE_NAME_LENGTH]; \
    VARIABLE_NAME[FEATURE_NAME_LENGTH] = '\0'; \
    SCRAMBLECODE##FEATURE_NAME_LENGTH(FEATURE_NAME, scramble_buf); \
    SCRAMBLECODE##FEATURE_NAME_LENGTH(scramble_buf, VARIABLE_NAME); \
  }

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
/*!
  \brief Macro to allow both C and C++ compilers to use this include file in
  a type-safe manner.
  \hideinitializer
*/
#define STRUCT struct
#endif


STRUCT _CarbonLicenseData_;
typedef STRUCT _CarbonLicenseData_ CarbonLicenseData;

//! C Interface to checking out a license.
CarbonLicenseData* UtLicenseWrapperCheckout(const char *featureName, const char *featureDescription);

//! C Interface to telling the server a license is still in use
void UtLicenseWrapperHeartbeat(CarbonLicenseData *pLicense);

//! C Interface to releasing a license
void UtLicenseWrapperRelease(CarbonLicenseData **ppLicense);

#ifdef __cplusplus
}
#endif

#endif // __UtCLicense_h_
