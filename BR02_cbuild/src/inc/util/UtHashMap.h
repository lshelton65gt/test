// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtHashMap_h_
#define __UtHashMap_h_


#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif
#ifndef _CARBON_HASH_VALUE_
#include "util/HashValue.h"
#endif
#ifndef _LOOPMAP_H_
#include "util/LoopMap.h"
#endif
#ifndef __MemManager_h_
#include "util/MemManager.h"
#endif
#include <algorithm> // for std::sort in visual c++
#ifndef __Util_h_
#include "util/Util.h"
#endif

#include "util/carbon_hashtable.h"
#include "util/carbon_hashtable_itr.h"
#include <utility> // for std::pair

//! UtHashMap class -- wrapper over a C hash map implementation
/*!
 * 
 * STL's hash implementation is slow and bloated. This class allows
 * for (mostly) the same interface as the HashMap class but with a
 * tremendous speed up and much much less code. The one limitation of
 * this is that objects are not stored, only pointers for both
 * the keys and values. You can cause objects to be allocated on
 * the heap "under-the-covers" by using HashObjectMgr.
 *
 * Note that both the keys and the values are 'managed' by a
 * HashPointerMgr, a HashObjectMgr, or a HashOpaque32Mgr.
 *
 * Differences between the interface provided here and STL maps include:
 *
 *  o In STL, operator[] returns a reference to the value.  In this
 *    class it returns a smart pointer proxy object when using
 *    HashPointerMgr.  The proxy object can be treated as a const
 *    reference to the value.  Since it's a const reference, you can't
 *    modify the pointer through it (and it can't be passed as a
 *    parameter to functions taking a non-const reference).  This
 *    restriction exists because the pointer itself isn't stored in
 *    the hash table -- the shrunken version of it is stored.  Thus,
 *    the non-existent pointer can't be directly modified.  You can,
 *    however, assign to the smart pointer via it's assignment
 *    operator and change the object that it points to.
 *
 *  o Similarly, iterators returned by this class reference pairs
 *    containing references to smart pointers for HashPointerMgr
 *    rather than references to the objects, as STL does.
 *
 *  o When a pointer is stored in an STL map you can 'delete map[key]'
 *    and 'delete iter->second'.  With the HashObjectMgr, those are
 *    references to a class, not a pointer.  There's a conversion
 *    operator to convert to the user's type (which is a pointer), so
 *    this should work, but due to apparent bugs in gcc before 3.4
 *    sometimes the compiler will complain that delete was passed a
 *    non-pointer.  You can work around this by casting to the correct
 *    type or assigning to a variable of the correct type and passing
 *    that.  You can use the UtHashMap<>::mapped_type typedef (as in
 *    STL) to provide the correct type.
 *
 * So, if I wanted to have a map from strings to UInt32s:
 *
 * UtHashMap<UtString, UInt32>
 *
 * Default template parameters cause this to be equivalent to:

 * UtHashMap<UtString, UInt32, HashValue<UtString>,
 * 	     HashObjectMgr<UtString>, HashPointerMgr<UInt32> >
 *
 * The third through fifth template parameters to UtHashMap default as
 * follows via the magic of template specialization:
 *
 *  o The third parameter defaults to HashPointer<> if the second
 *    parameter is a pointer, to HashOpaque32<> if it's an integral type,
 *    and to HashValue<> otherwise.
 *
 *  o The fourth and fifth parameters default based on the type of the
 *    first and second parameters, respectively.  If the first or second
 *    parameter is a pointer, HashPointerMgr<> is the default.  If it's
 *    an integral type, HashOpaque32Mgr<> is the default.  Otherwise
 *    HashObjectMgr<> is the default.
 *
 * This all means that you can usually rely on the defaults, though to
 * provide your own hash() function you'd use HashPointerValue<> rather
 * than HashPointer<> for the third parameter.
 *
 * Currently, pairs returned by iterators become invalid as of the next
 * iterator operation.
 *
 * See HashMap for HashValue and other related documentation.
 */
template<typename _KeyType, typename _ValueType, typename _HashType,
         typename _HashKeyMgr, typename _HashValMgr>
class UtHashMap
{
public: CARBONMEM_OVERRIDES
  typedef _KeyType key_type;
  typedef _ValueType mapped_type;
  typedef std::pair<_KeyType, _ValueType> NameValue;
  typedef NameValue value_type;

  struct MapEntry : public carbon_hashEntryStruct {
    value_type mKeyVal;
    static MapEntry* cast(carbon_hashEntryStruct* ptr) {
      return (MapEntry*) ptr;
    }
    static const MapEntry* cast(const carbon_hashEntryStruct* ptr) {
      return (const MapEntry*) ptr;
    }
  };

  //! Normal iterator implementation
  /*!
    Do not instantiate.
  */
  struct iterator {
    CARBONMEM_OVERRIDES

    typedef NameValue value_type;
    typedef value_type& reference;
    typedef value_type* pointer;

    iterator() {
      CARBON_HASHTABLE_ITERATOR_NULLINIT(&mIter);
    }

    iterator(bool /* no init */) {
    }

    iterator(const struct carbon_hashtable_itr& iter) :
      mIter(iter)
    {
    }

    iterator(UtHashMap* hm, carbon_hashEntry* e) {
      mIter.h = &hm->mHash;
      mIter.e = e;
#ifdef CDB
      mIter.mNoAdvance = 1;
      mIter.index = 0xdeadbeef;
      mIter.prev = (carbon_hashEntry*) 0xdeadbeef;
#endif
    }

    value_type* getPtr() const {
      MapEntry* se = MapEntry::cast(mIter.e);
      return &(se->mKeyVal);
    }

    pointer operator->() const { 
      MapEntry* se = MapEntry::cast(mIter.e);
      return &(se->mKeyVal);
    }
    reference operator*() const {
      return *getPtr();
    }


    bool operator==(const iterator& __it) const
    { return mIter.e == __it.mIter.e; }
    bool operator!=(const iterator& __it) const
    { return mIter.e != __it.mIter.e; }

    iterator& operator++() {
      (void) carbon_hashtable_iterator_advance(&mIter);
      return *this;
    }

    // iterator operator++(int); Don't allow this

    carbon_hashtable_itr mIter;
  };

  //! Normal const_iterator implementation
  /*!
    Do not instantiate.
  */
  struct const_iterator {
    CARBONMEM_OVERRIDES

    typedef NameValue value_type;
    typedef const value_type& reference;
    typedef const value_type* pointer;

    const_iterator() {
      CARBON_HASHTABLE_ITERATOR_NULLINIT(&mIter);
    }

    const_iterator(bool /* no init */) {
    }

    const_iterator(const struct carbon_hashtable_itr& iter) :
      mIter(iter)
    {
    }

    const_iterator(const iterator& __it) :
      mIter(__it.mIter)
    {
    }

    value_type* getPtr() const {
      MapEntry* se = MapEntry::cast(mIter.e);
      return &(se->mKeyVal);
    }

    pointer operator->() const { 
      MapEntry* se = MapEntry::cast(mIter.e);
      return &(se->mKeyVal);
    }
    reference operator*() const {
      return *getPtr();
    }

    bool operator==(const const_iterator& __it) const
    { return mIter.e == __it.mIter.e; }
    bool operator!=(const const_iterator& __it) const
    { return mIter.e != __it.mIter.e; }

    const_iterator& operator++() {
      (void) carbon_hashtable_iterator_advance(&mIter);
      return *this;
    }

    // iterator operator++(int); Don't allow this

    carbon_hashtable_itr mIter;
  }; // struct _UtHashMap_const_iterator


  //! The non-static wrapper (in a template!) for key hashing
  UInt32 hashFn(const carbon_hashEntry* e) {
    const MapEntry* me = MapEntry::cast(e);
    _HashType hashType;
    return hashType.hash(me->mKeyVal.first);
  }

  //! The non-static wrapper for key comparison
  int eqFn(const void* key1, const carbon_hashEntry* e) {
    const _KeyType* key1V = (const _KeyType*) key1;
    const MapEntry* me = MapEntry::cast(e);
    _HashType hashType;
    return hashType.equal(*key1V, me->mKeyVal.first);
  }
  
  //! The static wrapper for passing as procedure ptr
  static UInt32 sHashFn(void* hs, const carbon_hashEntry* e)
  {
    return ((UtHashMap*)hs)->hashFn(e);
  }

  //! The static wrapper for key comparison
  static int sEqFn(void* hs, const void* key1, const carbon_hashEntry* e) {
    return ((UtHashMap*)hs)->eqFn(key1, e);
  }
    
  //! Constructor
  UtHashMap()
  {
    createHash();
  }
  
  //! Destructor
  ~UtHashMap()
  {
    freeEntries();
    carbon_hashtable_clean(&mHash, sizeof(MapEntry));
  }

  typedef UtHashMap<_KeyType, _ValueType, _HashType, _HashKeyMgr, _HashValMgr>
  HashMapType;

  typedef LoopMap<HashMapType> UnsortedLoop;

  //! hand-coded UnsortedCLoop for speed (avoid end() iterator overhead)
  struct UnsortedCLoop {
    UnsortedCLoop(const UtHashMap& hmap) {
      carbon_hashtable_iterator_init(&mIter,
                                     const_cast<carbon_hashtable*>(&hmap.mHash));
    }
    UnsortedCLoop(const UnsortedCLoop& ucl):
      mIter(ucl.mIter)
    {
    }
    UnsortedCLoop() {
      CARBON_HASHTABLE_ITERATOR_NULLINIT(&mIter);
    }      
    UnsortedCLoop& operator=(const UnsortedCLoop& ucl) {
      if (&ucl != this) {
        mIter = ucl.mIter;
      }
      return *this;
    }

    bool atEnd() const {return mIter.e == NULL;}
    void operator++() {carbon_hashtable_iterator_simple_advance(&mIter);}
    value_type* getPtr() const {
      MapEntry* se = MapEntry::cast(mIter.e);
      return &(se->mKeyVal);
    }
    const _KeyType& getKey() const {
      MapEntry* se = MapEntry::cast(mIter.e);
      return se->mKeyVal.first;
    }
    const _ValueType& getValue() const {
      MapEntry* se = MapEntry::cast(mIter.e);
      return se->mKeyVal.second;
    }

  private:
    struct carbon_hashtable_itr mIter;
  };

  class LoopI;
  friend class LoopI;
  friend struct iterator;
  
  class LoopI
  {
  public: 
    CARBONMEM_OVERRIDES

    typedef NameValue value_type;
    typedef const value_type& reference;

    struct CmpPtr
    {
      CARBONMEM_OVERRIDES

      CmpPtr(const carbon_hashtable* /*table*/) /*: mHashTable(table)*/ {}
      bool operator()(const MapEntry* p,
		      const MapEntry* q) const {
        _HashType hashType;
        return hashType.lessThan(p->mKeyVal.first, q->mKeyVal.first);
      }
      //const carbon_hashtable* mHashTable;
    };

    LoopI(const HashMapType& sh)
    {
      mHashTable = &sh.mHash;
      mSorted.reserve(sh.size());
      for (const_iterator p = sh.begin(), e = sh.end(); p != e; ++p) {
        MapEntry* nv = MapEntry::cast(p.mIter.e);
        mSorted.push_back(nv);
      }
      std::sort(mSorted.begin(), mSorted.end(), CmpPtr(mHashTable));
      mPtr = mSorted.begin();
      mEnd = mSorted.end();
    }

    // Have to put copy ctor & assign operator for now because
    // we don't have IterMap to wrap this yet.
    LoopI(const LoopI& src)
    {
      copy(src);
    }

    LoopI& operator=(const LoopI& src)
    {
      if (this != &src) {
        copy(src);
      }
      return *this;
    }

    bool atEnd() const {return mPtr == mEnd;}
    void operator++() {++mPtr;}

    //! Go to the last element of the sorted array
    void toLast() { mPtr = mEnd; --mPtr; }

    //! Go back to the beginning of the sorted array
    void rewind() { mPtr = mSorted.begin(); }

    const typename _HashKeyMgr::RefType& getKey() {
      MapEntry* me = *mPtr;
      return me->mKeyVal.first;
    }
    typename _HashValMgr::RefType& getValue() {
      MapEntry* me = *mPtr;
      return me->mKeyVal.second;
    }

    reference operator*() const {
      MapEntry* me = *mPtr;
      return me->mKeyVal;
    }

  private:
    const carbon_hashtable* mHashTable;
    typedef UtArray<MapEntry*> NameValueVec;
    NameValueVec mSorted;
    //_HashValMgr mHashValMgr;
    //_HashKeyMgr mHashKeyMgr;
    typename NameValueVec::iterator mPtr;
    typename NameValueVec::iterator mEnd;

    void copy(const LoopI& src)
    {
      mHashTable = src.mHashTable;
      mSorted = src.mSorted;
      // mSorted may have been reallocated, so fix up iterators.
      mPtr = mSorted.begin() +
        (src.mPtr - const_cast<LoopI&>(src).mSorted.begin());
      mEnd = mSorted.end();
    }
  };

  typedef LoopI SortedLoop;     // would like to wrap with Iter...

  //! Get the internal hashtable. Valid until destruction
  carbon_hashtable* getCHash() { return &mHash; }

  //! create a loop through a sorted image of the objects contained in the hash-map
  /*! To use this method, you must have an operator< defined in the _KeyType. */
  SortedLoop loopSorted() const {return SortedLoop(*this);}

  //! Loop through the objects contained in the map, in an order that can change from run to run
  UnsortedLoop loopUnsorted() {return UnsortedLoop(*this);}

  //! Loop through the objects contained in the map, in an order that can change from run to run
  UnsortedCLoop loopCUnsorted() const {return UnsortedCLoop(*this);}

  const_iterator find(const _KeyType& k) const {
    carbon_hashtable_itr hIter;
    if (doSearch(k, &hIter))
    {
      return const_iterator(hIter);      
    }

    // default is end()
    const_iterator a;
    return a;
  }
  
  size_t count(const _KeyType& k) const {
    carbon_hashtable_itr hIter;
    if (doSearch(k, &hIter))
      return 1;
    return 0;
  }
  
  // stl-compliant find -- requires construction of an end() iterator on failure
  iterator find(const _KeyType& k) {
    carbon_hashtable_itr hIter;
    if (doSearch(k, &hIter))
    {
      return iterator(hIter);      
    }

    // default is end()
    iterator a;
    return a;
  }
  
  // find that returns the MapEntry directly, or NULL
  MapEntry* findEntry(const _KeyType& k) const {
    _HashType hashType;
    return MapEntry::cast(carbon_hashtable_findEntry(&mHash, &k,
                                                     sHashFn, sEqFn,
                                                     (void*) this, 
                                                     hashType.hash(k)));
  }

  //! Type for the return value of STL-compatible insert()
  typedef std::pair<iterator,bool> IterBoolPair;

  IterBoolPair insert(const value_type& v) { 
    carbon_hashEntry* e;
    _HashType hashType;
    bool ret = carbon_hashtable_maybe_insert(&mHash, (void*) &v.first,
                                             sHashFn, sEqFn, (void*) this,
                                             hashType.hash(v.first),
                                             sizeof(MapEntry), &e);
    MapEntry* me = MapEntry::cast(e);
    if (ret) {
      _HashKeyMgr::copy(v.first, &(me->mKeyVal.first));
    }
    _HashValMgr::copy(v.second, &(me->mKeyVal.second));
    return IterBoolPair(iterator(this, e), ret);
  }

  iterator begin() {
    iterator a(false);        // no init
    carbon_hashtable_iterator_init(&a.mIter, &mHash);
    return a;
  }
  iterator end() {iterator a; return a;} // default constructs to end

  const_iterator begin() const {
    const_iterator a(false);  // no init
    carbon_hashtable_iterator_init(&a.mIter,
                                   const_cast<struct carbon_hashtable*>(&mHash));
    return a;
  }

  const_iterator end() const {const_iterator a; return a;}


  //! Non-STL function, insert and initialize a value in one step
  /*!
    If there is already an entry in the table,  it just returns that;
    otherwise, it creates the entry and initializes it to initVal and
    returns the initialized value.
  */
  typename _HashValMgr::RefType& insertInit(const _KeyType& key, const _ValueType& initVal)
  {
    carbon_hashEntry* e;
    _HashType hashType;
    bool inserted =
      carbon_hashtable_maybe_insert(&mHash, (void*) &key,
                                    sHashFn, sEqFn, (void*) this,
                                    hashType.hash(key),
                                    sizeof(MapEntry), &e);
    MapEntry* mapEntry = MapEntry::cast(e);
    typename _HashValMgr::RefType& val = mapEntry->mKeyVal.second;

    if (inserted) {
      _HashValMgr::copy(initVal, &val);
      _HashKeyMgr::copy(key, &(mapEntry->mKeyVal.first));
    }
    return val;
  }
  
  typename _HashValMgr::RefType& operator[](const _KeyType& key) {
    carbon_hashEntry* e;
    _HashType hashType;
    int inserted =
      carbon_hashtable_maybe_insert(&mHash, (void*) &key,
                                    sHashFn, sEqFn, (void*) this,
                                    hashType.hash(key),
                                    sizeof(MapEntry), &e);
    MapEntry* mapEntry = MapEntry::cast(e);
    typename _HashValMgr::RefType& val = mapEntry->mKeyVal.second;

    if (inserted) {
      _HashKeyMgr::copy(key, &(mapEntry->mKeyVal.first));
      _HashValMgr::initValue(&val); // zeros opaques, empty ctor for classes
    }

    return val;
  }
  
  size_t size() const {
    return mHash.sizes.entrycount;
  }
  
  //! Eliminates all the entries in the hash table.
  void clear() {
    freeEntries();

    // Zeroing the capacity of hash tables when the programmer reduces
    // the chance that we'll get into an n^2 situation iterating over
    // hash-tables if they grow large, get cleared, and then stay small.
    //
    // This did not have much affect on compile time in our existing perf
    // tests but I suspect it will help Nortel.  There is some evidence that
    // this helps runtime and compile memory but DoPerfCheck isn't too
    // reliable.
    carbon_hashtable_clean(&mHash, sizeof(MapEntry));
  }

  //! clear the hashtable and remove all the memory
  void clean() {
    freeEntries();
    carbon_hashtable_clean(&mHash, sizeof(MapEntry));
  }
    
  //! This does not reduces the capacity of the hashtable to store
  //! the existing entries.
  void clear_and_resize() {
    freeEntries();
    carbon_hashtable_clear_and_resize(&mHash, sizeof(MapEntry));
  }
 
  bool empty() const {
    return size() == 0;
  }
  
  void erase(iterator& i) {
    value_type& keyval = *i;
    _HashKeyMgr::destroy(&keyval.first);
    _HashValMgr::destroy(&keyval.second);
    carbon_hashtable_iterator_remove_current(&i.mIter, sizeof(MapEntry));
  }
  
  size_t erase(const _KeyType& key) {
    carbon_hashtable_itr itr;
    if (!doSearch(key, &itr)) {
      return 0;
    }
    MapEntry* me = MapEntry::cast(itr.e);
    _HashKeyMgr::destroy(&me->mKeyVal.first);
    _HashValMgr::destroy(&me->mKeyVal.second);
    carbon_hashtable_iterator_remove_current(&itr, sizeof(MapEntry));
    return 1;
  }
  
  void swap(UtHashMap& hs)
  {
    // in general this is C++-unsafe, but this will work
    // because mHash is a C structure.
    struct carbon_hashtable tmp = mHash;
    mHash = hs.mHash;
    hs.mHash = tmp;
  }

  void freeMemory() { 
    freeEntries();
    carbon_hashtable_clean(&mHash, sizeof(MapEntry));
    createHash();
  }
  

  //! assignment operator
  UtHashMap& operator=(const UtHashMap& hs) {
    if (&hs != this) {
      freeMemory();
      copy(hs);
    }
    return *this;
  }
  
  //! copy ctor
  UtHashMap(const UtHashMap& hs) {
    createHash();
    copy(hs);
  }

  //! clear, deleting the contained pointer values
  void clearPointerValues() {
    // In this implementation, we are continuing to iterate through
    // a hash table that has deleted stuff in it, and will likely
    // crash if any of the equality/hash functions are called on
    // any of those elements, so this is a little risky, but most
    // obvious hash-table implementions should not be problematic,
    // and this saves the big vector copy.
    for (iterator p = begin(), e = end(); p != e; ) {
      _ValueType v = p->second;
      ++p;
      delete v;
    }
    clear();
  }

  //! clear, deleting the contained pointer keys
  void clearPointerKeys() {
    // In this implementation, we are continuing to iterate through
    // a hash table that has deleted stuff in it, and will likely
    // crash if any of the equality/hash functions are called on
    // any of those elements, so this is a little risky, but most
    // obvious hash-table implementions should not be problematic,
    // and this saves the big vector copy.
    for (iterator p = begin(), e = end(); p != e; ) {
      _KeyType v = p->first;
      ++p;
      delete v;
    }
    clear();
  }

  //! clear, deleting both the contained key and value pointers
  void clearPointerValuesAndKeys () {
    // In this implementation, we are continuing to iterate through
    // a hash table that has deleted stuff in it, and will likely
    // crash if any of the equality/hash functions are called on
    // any of those elements, so this is a little risky, but most
    // obvious hash-table implementions should not be problematic,
    // and this saves the big vector copy.
    for (iterator p = begin(), e = end(); p != e; ) {
      delete p->second;
      _KeyType v = p->first;
      ++p;
      delete v;
    }
    clear();
  }

private:
  // Disable comparison until we implement it properly
  bool operator==(const UtHashMap&) const;

  void copy(const UtHashMap& hs) {
    if (!hs.empty()) {
      for (const_iterator p = hs.begin(), e = hs.end(); p != e; ++p) {
        const value_type& keyValue = *p;
        insert(keyValue);
      }
    }
  }

  struct carbon_hashtable mHash;

  void freeEntries()
  {
    if (!empty()) {
      for (iterator p = begin(), e = end(); p != e; ++p) {
        value_type& keyval = *p;
        _HashKeyMgr::destroy(&keyval.first);
        _HashValMgr::destroy(&keyval.second);
      }
    }
  }

  void createHash() {
    CARBON_HASHTABLE_INIT(&mHash);
  }
  
  bool doSearch(const _KeyType& k, struct carbon_hashtable_itr* hIter) const
  {
    _HashType hashType;
    CARBON_HASHTABLE_ITERATOR_NULLINIT(hIter);
    return carbon_hashtable_search(&mHash, &k, sHashFn, sEqFn,
                                   (void*) this, hIter,
                                   hashType.hash(k)) != 0;
  }
} ; //class UtHashMap

#endif

