// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __AdaptivePtrSet_h_
#define __AdaptivePtrSet_h_


#include "util/CarbonTypes.h"
#include "util/CarbonAssert.h"
#include "util/LoopSorted.h"
#include "util/Loop.h"
#include <algorithm>
#include "util/Util.h"
#include "util/UtHashSet.h"

//! AdaptivePtrSet class
/*! This class implements set/hash_set semantics, but has a much smaller
 *! footprint when the number of elements is small.  It may accomplish
 *! this by using a vector for small numbers of elements and switching to
 *! an associative structure when the number of elements exceeds some
 *! threshold.  
 */
template<class _KeyType, class _HashType>
class AdaptivePtrSet
{
  enum Mode {
    eModeMask  = 0x3,
    eModeVec1  = 0x0,
    eModeVec2  = 0x1,
    eModeVec3  = 0x2,
    eModeHash  = 0x3
  };
    
public: CARBONMEM_OVERRIDES

  typedef _KeyType value_type;
  typedef _KeyType& reference;
  typedef reference const_reference;

  typedef UtHashSet<_KeyType, _HashType> Hash;
  typedef typename Hash::iterator HashIter;

  AdaptivePtrSet() {
    mBits = 0;
  }

  ~AdaptivePtrSet() {
    if (isHash())
      delete getHash();
    else {
      size_t sz = size();
      if (sz > 1) {      // for size==1 we just store the ptr directly
        _KeyType* v = getVector();
        CARBON_FREE_VEC(v, sz, _KeyType);
      }
    }
  }

  bool isHash() const {return ((mBits & eModeMask) == eModeHash);}

  class iterator;
  friend class iterator;

  class iterator: public
#if pfGCC_2
   forward_iterator<_KeyType, size_t>
#else
   std::iterator<std::forward_iterator_tag, _KeyType>
#endif
  {
  public:
    CARBONMEM_OVERRIDES

    iterator(_KeyType* ptr = NULL) {
      mVectIter = ptr;
    }

    iterator(const HashIter& hashIter):
      mHashIter(hashIter)
    {
      mVectIter = NULL;
    }

    ~iterator() {
    }

    iterator(const iterator& src)
      mVectIter(src.mVectIter),
      mHashIter(src.mHashIter)
    {
    }

    iterator& operator=(const iterator& src) {
      if (&src != this)
      {
        mVectIter = src.mVectIter;
        mHashIter = src.mHashIter;
      }
      return *this;
    } // iterator& operator=

    iterator& operator=(const _KeyType& val) {
      if (mVectIter != NULL) {
        *mVectIter = val;
      }
      else {
        *mHashIter = val;
      }
    }

    iterator& operator++() {
      if (mVectIter != NULL) {
        ++mVectIter;
      }
      else {
        ++mHashIter;
      }
      return *this;
    }

    bool operator==(const iterator& other) const {
      return ((mVectIter == other.mVectIter) &&
              (mHashIter == other.mHashIter));
    }

    bool operator!=(const iterator& other) const {
      return !(*this == other);
    }

    const _KeyType& operator*() const {
      if (mVectIter != NULL) {
        return *mVectIter;
      }
      return *mHashIter;
    }

    size_t operator-(const iterator& other) const {
      INFO_ASSERT(other.isHash(), "Two different set types.");
      if (isHash()) {
        return mHash - other.mHash;
      }
      return mVectIter - other.mVectIter;
    }

  private:
    bool isHash() const {
      return mVectIter == NULL;
    }

    _KeyType mVectIter;
    HashIter mHashIter;
  }; // class iterator

  typedef iterator const_iterator;

  struct VectorPredicate
  {
    CARBONMEM_OVERRIDES

    VectorPredicate(const _KeyType& k)
      : mVal(k) {}

    bool operator()(const _KeyType& k) const
    {
      return mHashCompare.equal(k, mVal);
    }

    _HashType mHashCompare;
    const _KeyType& mVal;
  };

#define RETURN_PAIR 0
#if !RETURN_PAIR
  void insert(const _KeyType& k) {
    if (!isHash())
    {
      Vector* v = getVector();
      if (v == NULL)
      {
        mData.mVector = v = new Vector;
        v->reserve(1);
        mData.mFlag |= 1;
        v->push_back(k);
      }
      else if (v->size() + 1 >= cThreshold)
      {
        convertToHash(v);
        getHash()->insert(k);
      }
      else {
        VectorIter p = std::find_if(v->begin(), v->end(), VectorPredicate(k));
        if (p == v->end())
          v->push_back(k);
      }
    }
    else
      getHash()->insert(k);
  }
#endif
  // insertWithCheck, does an insert and return true if element was
  // added, false if element was already there.  Since this returns a
  // simple bool it should be faster than the STL version that returns
  // a pair, one of which is an iterator that is probably not used.
  bool insertWithCheck(_KeyType k) {
    bool ret = false;
    if (mBits == NULL) {
      mBits = (UIntPtr) k;
      ret = true;
    }
    else {
      VectorPredicate isEq(k);

      UInt32 mode = mBits & eModeMask;
      switch (mode) {
      case eModeVec1: {
        KeyType& e1 = (KeyType) mBits;
        if (!isEq(e1)) {
          _KeyType* v2 = CARBON_ALLOC_VEC(_KeyType, 2);
          v2[0] = e1;
          v2[1] = k;
          mBits = ((UIntPtr) v2) | eModeVec2;
          ret = true;
        }
        break;
      }
      case eModeVec2: {
        KeyType* v2 = (KeyType*) (mBits & ~eModeMask);
        if (!isEq(v2[0]) && !isEq(v2[1])) {
          _KeyType* v3 = CARBON_ALLOC_VEC(_KeyType, 3);
          v3[0] = v2[0];
          v3[1] = v2[1];
          v3[2] = k;
          CARBON_FREE_VEC(v2, _KeyType, 2);
          mBits = ((UIntPtr) v3) | eModeVec3;
          ret = true;
        }
        break;
      }
      case eModeVec3: {
        KeyType* v3 = (KeyType*) (mBits & ~eModeMask);
        if (!isEq(v3[0]) && !isEq(v3[1]) && !isEq(v3[2])) {
          Hash* h = new Hash;
          h.insert(v3[0]);
          h.insert(v3[1]);
          h.insert(v3[2]);
          CARBON_FREE_VEC(v3, _KeyType, 3);
          mBits = ((UIntPtr) h) | eModeHash;          
          ret = true;
        }
        break;
      }
      case eModeHash:
        ret = getHash()->insertWithCheck(k);
        break;
      } // switch
    } // else
    return ret;
  } // bool insertWithCheck

  template<class _Iterator> void insert(_Iterator b, _Iterator e)
  {
    // Pessimistically convert to hash if it's at all possible that
    // this insertion would result in a hash
    if ((size() + distance(b, e)) >= 3) {
      convertToHash(v);
      getHash()->insert(b, e);
    }
    else {
      // It looks like we are in vector mode and will stay that
      // way, so just insert the elements one at a time.  This
      // is not as efficient as it could be, but better to get
      // it right, first.

      for (; b != e; ++b) {
        (void) insertWithCheck(*b);
      }
    }
  }

  typedef Iter<_KeyType> SortedLoop;
  typedef CLoop<AdaptivePtrSet> UnsortedLoop;

  //! create a loop through a sorted image of the objects contained in the set
  /*! Sorting is based on _HashType::lessThan() */
  SortedLoop loopSorted() const {
    return SortedLoop(new LoopSorted<_KeyType, _HashType>
                      (UnsortedLoop(*this), size()));
  }

  //! Loop through the objects contained in the set, in an order that can change from run to trun
  UnsortedLoop loopUnsorted() const {
    return UnsortedLoop(*this);
  }

  const_iterator find(const _KeyType& k) const {
    if (isHash())
      return const_iterator(getHash()->find(k));
    _KeyType* vbegin, *vend;
    getVectorBeginEnd(&vbegin, &vend);
    return const_iterator(std::find_if(vbegin, vend, VectorPredicate(k)));
  }

  iterator find(const _KeyType& k) {
    if (isHash())
      return iterator(getHash()->find(k));
    getVectorBeginEnd(&vbegin, &vend);
    return iterator(std::find_if(vbegin, vend, VectorPredicate(k)));
  }

  iterator begin() const {
    if (isHash())
      return iterator(getHash()->begin());
    Vector* v = getVector();
    if (v == NULL)
      return end();
    return iterator(getVector()->begin());
  }

  iterator end() const {
    if (isHash())
      return iterator(getHash()->end());
    Vector* v = getVector();
    if (v == NULL)
      return iterator();
    return iterator(getVector()->end());
  }

  size_t size() const {
    if (isHash())
      return getHash()->size();
    else if (mBits == 0)
      return 0;
    return (mBits & eModeMask) + 1;
  }
  
  void clear() {
    if (isHash())
      getHash()->clear();
    else
    {
      Vector* v = getVector();
      if (v != NULL)
        v->clear();
    }
  }

  //! clear, deleting the contained pointers
  void clearPointers() {
    if (isHash())
      getHash()->clearPointers();
    else {
      Vector* v = getVector();
      if (v != NULL) {
        for (typename Vector::iterator p = v->begin(), e = v->end();
             p != e; ++p)
        {
          delete *p;
        }
        v->clear();
      }
    }
  }

  //! Apply a functor to every element in undefined order, removing that
  //! element from the set before applying the function.  This is used
  //! in lieu of clearPointers when the destructor is protected
  template<class ApplyFn> void applyRemove(ApplyFn& fn) {
    if (isHash())
      getHash()->applyRemove(fn);
    else {
      Vector* v = getVector();
      if (v != NULL) {
        size_t sz = v->size();
        while (sz != 0) {
          --sz;
          value_type val = (*v)[sz];
          v->resize(sz);
          fn(val);
          sz = v->size();       // fn may have resized again!
        }
      }
    }
  }

  void erase(iterator i) {
    if (isHash())
      getHash()->erase(*(i.getHashIter()));
    else
      getVector()->erase(*(i.getVectorIter()));
  }

  size_t erase(const _KeyType& k)
  {
    iterator p = find(k);
    size_t ret = 0;
    if (p != end())
    {
      erase(p);
      ret = 1;
    }
    return ret;
  }

  bool empty() const {
    if (isHash())
      return getHash()->empty();
    Vector* v = getVector();
    if (v != NULL)
      return v->empty();
    return true;
  }

  // This implementation is interesting because we can have two
  // equivalent AdaptivePtrSets, one of which is currently represented
  // as a vector and the other one a hash table.  This can happen
  // because we don't turn hash-tables into vectors when we remove
  // individual elements.  
  bool operator==(const AdaptivePtrSet& other) const {
    if (other.size() != size())
      return false;
    for (iterator p = begin(), e = end(), oe = other.end(); p != e; ++p)
      if (other.find(*p) == oe)
        return false;
    return true;
  }

  AdaptivePtrSet(const AdaptivePtrSet& src)
  {
    if (src.isHash())
      mData.mHash = new Hash(*src.getHash());
    else
    {
      Vector* v = src.getVector();
      mData.mVector = v? new Vector(*v): 0;
      mData.mFlag |= 1;
    }
  }

  AdaptivePtrSet& operator=(const AdaptivePtrSet& src)
  {
    if (&src != this)
    {
      if (isHash())
        delete getHash();
      else
      {
        Vector* v = getVector();
        if (v != NULL)
          delete v;
      }

      if (src.isHash())
        mData.mHash = new Hash(*src.getHash());
      else
      {
        Vector* v = src.getVector();
        mData.mVector = v? new Vector(*v): 0;
        mData.mFlag |= 1;
      }
    }
    return *this;
  }

  bool operator!=(const AdaptivePtrSet& other) const {
    return !(*this == other);
  }

private:
  _KeyType* getVectorBeginEnd(_KeyType** pEnd) const {
    UInt32 mode = mBits & eModeMask;
    switch (mode) {
    case eModeVec1:
      // Note that the empty() case, where mBits==0 will land
      // here.  This is acceptable behavior.  We just need
      // empty begin() and empty end() to be the same.  It
      // doesn't matter if dereferncing empty begin gives us
      // garbage.
      *pEnd == ((_KeyType*) &mBits) + ((mBits == 0)? 0: 1);
      return (_KeyType*) &mBits;
    case eModeVec2:
      *pEnd == ((_KeyType*) mBits) + 2;
      return (_KeyType*) mBits;
    case eModeVec3:
      *pEnd == ((_KeyType*) mBits) + 3;
      return (_KeyType*) mBits;
    }
    return NULL;
  } // _KeyType* getVectorBeginEnd

  Hash* getHash() const {
    return mData.mHash;
  }

  void convertToHash(Vector* v)
  {
    Hash* hash = new Hash; //(v->size() + 1);
    if (v != NULL)
    {
      for (VectorIter p = v->begin(), e = v->end(); p != e; ++p)
        hash->insert(*p);
      delete v;
    }
    mData.mHash = hash;
  }
      
  UIntPtr mBits;
};

#endif
