// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _ITER_H_
#define _ITER_H_


#include "util/CarbonAssert.h"
#include "util/StlIterator.h"

//! Iter template class
/*!
  This class provides a mechanism for a class to expose iteration over its
  STL containers, while completely hiding the container.  The idea is to
  take the abstraction provided by Loop one step further, and and expose
  only the type of object being iterated over.
 
  A user uses an Iter exactly like a Loop, except that the container
  class is not evident in the public interface in any way.

  \verbatim
  Example:
 
  typedef Iter<Net*> NetIter;

  class Module
  {
  public: CARBONMEM_OVERRIDES
    ...
    NetIter loopInputNets();
    NetIter loopUndrivenNets();
    ...
  private:
    UtVector<Net*> mInputNets;
    UtSet<Net*> mUndrivenNets;
  }
 
  ...
  for (NetIter p = mod->loopInputNets(); !p.atEnd(); ++p)
  {
    Net* net = *p;
    ...
  }
  for (NetIter p = mod->loopUndrivenNets(); !p.atEnd(); ++p)
  {
    Net* net = *p;
    ...
  }
  \endverbatim

  On the class-implementor side, to provide an Iter, you have several choices.
  If you can generate a Loop, LoopFilter, LoopPair, LoopMulti, or LoopThunk,
  then you can create your iteration by passing that to the static method
  Iter<ObjectType>::create.  To implement the methods outlined above, we
  write:

  \verbatim
  NetIter loopInputNets() {return NetIter::create(Loop<UtVector<NUNet*> > mInputNets);}
  NetIter loopUndrivenNets() {return NetIter::create(Loop<UtSet<NUNet*> > mUndrivenNets);}
  \endverbatim  

  Iter abstracts the looping over the different containers so they look the
  same to the caller.  So there is no exposure to the caller to the
  container-type being used to hold the nets.
  
  If the nets are not held in an STL container, but are in a more
  complex graph of some sort, then you might not be able to use the
  existing Loop* template classes to implement the iteration.  You
  must instead write your own class that, at a minimum, provides
  atEnd, operator++, operator*, and operator().

  If your class holds a lot of data, then you may want to avoid writing
  a copy constructor, because in the STL methodology, iterators can copied
  around quite a bit.  You can avoid the innefficiencies associated with
  this by using reference counting.  Iter provides a built-in mechanism
  to do this:  derive your new class from the class Iter::RefCnt.

  \verbatim
    class MyNewLoopClass: public Iter<MyObj>::RefCnt
    {
    public: CARBONMEM_OVERRIDES
      MyNewLoopClass(...);
      virtual ~MyNewLoopClass() {}
      virtual bool atEnd() const {....}
      virtual void operator++() {...}
      virtual reference operator*() const {....}
      virtual bool operator()(value_type* ptr) {....}
    private:
      MyNewLoopClass(const MyNewLoopClass&); // forbid
      MyNewLoopClass& operator=(const MyNewLoopClass&); // forbid
      ...
    };
  \endverbatim  

  Then you can construct an Iter from a new'd instance of your new class.

  \verbatim
    Iter<MyObj> makeIter(...) {
      return Iter<MyObj>(new MyNewLoopClass);
    }
  \endverbatim  

  The disadvantage of this approach is that all copies of the iterator
  will share state, and so you won't be able to run multiple algorithms
  on the iterator using the STL begin() and end() methods, unless you
  re-create it.

  If your class holds relatively little data, and you implement a
  copy constructor, then you have effectively created the same
  interface as the Loop* templates, and you can use Iter::create to
  build your Iter.

  The performance implications of using an Iter<Net*> rather than
  a Loop<UtVector<Net*>> are small but non-zero.  Every method
  call to Iter will result in a virtual function call.  So if you
  write your loop with ++, *, and atEnd, then you will make three
  virtual function calls per iteration.  That is why the function
  call operator is provided -- it combines these three virtual function
  calls into one.  The looping syntax is a little different from
  a standard STL loop but it's more efficient.

  \verbatim
    Net* net;
    for (NetIter nets = module->loopInputNets(); nets(&net);)
    {
      ...
    }
  \endverbatim
 */
template <class _ValueType>
class Iter
{
public: CARBONMEM_OVERRIDES
  typedef _ValueType value_type;
  typedef _ValueType reference;

  //! Base interface for abstract iteration
  class Base
  {
  public: CARBONMEM_OVERRIDES
    Base() {
    }
    virtual ~Base() {
    }
    virtual Base* copy() const = 0;
    virtual void free() = 0;
    virtual bool atEnd() const = 0;
    virtual void operator++() = 0;
    virtual reference operator*() const = 0;
    virtual bool operator()(value_type* ptr) = 0;
    virtual bool operator()(const value_type** /*ptr*/) {
      INFO_ASSERT(0, "Not implemented.");
      return false;
    }
  };

  //! Class Iter::RefCnt implements the reference-counting to aid implementors
  class RefCnt: public Base
  {
  public: CARBONMEM_OVERRIDES
    RefCnt() {
      mRefCnt = 0;
    }
    virtual ~RefCnt() {
      INFO_ASSERT(mRefCnt == 0, "References to iterator still exist.");
    }
    virtual Base* copy() const {
      ++mRefCnt;
      return const_cast<RefCnt*>(this);
    }
    void free() {
      --mRefCnt;
      INFO_ASSERT(mRefCnt >= 0, "Reference count underflow.");
      if (mRefCnt == 0)
      {
        delete this;
      }
    }

  private:
    mutable int mRefCnt;
  };

  //! Class Iter::Factory manages the container's iterators.
  /*!
    This class is what the implementor of an Iter interface must
    instantiate with full knowledge of the container type and iterator,
    as shown in the example for class Iter.
  */
  template <class _Looper>
  class Factory: public Base
  {
#if ! pfGCC_2
    using Base::operator();
#endif

  public: CARBONMEM_OVERRIDES
    Factory(_Looper looper)
      : mLoop(looper)
    {
    }

    bool atEnd() const {return mLoop.atEnd();}
    void operator++() {++mLoop;}
    reference operator*() const {return *mLoop;}
    bool operator()(value_type* ptr) {return mLoop(ptr);}
#if pfICC
    bool operator()(const value_type* ptr) {return mLoop(ptr);}
#endif

    Base* copy() const {
      return new Factory(mLoop);
    }
    void free() {
      delete this;
    }
  private:
    Factory(const Factory& src); // forbid
    Factory& operator=(const Factory& src); // forbid

    _Looper mLoop;
  };

  //! Default constructor
  /*!
    Needed for iterator returns such as
    \verbatim
    Iter<myObj> iter;
    if (getIter(&iter))
     ...
    \endverbatim
    \warning Behavior is undefined if methods are used after using
    this constructor and the iterator is not properly initialized.
  */
  Iter()
  {
    mRoot = NULL;
  }

  template <typename _Looper> static Iter create(_Looper looper)
  {
    Iter i;
    i.mRoot = new Factory<_Looper>(looper);
    return i;
  }

  //! Construct a Iter given an Iter::Base
  Iter(Base* root)
  {
    if (root == NULL)
      mRoot = NULL;
    else
      mRoot = root->copy();
  }

  //! Copy constructor to help the user manage & pass around Iters.
  Iter(const Iter& src)
  {
    if (src.mRoot == NULL)
      mRoot = NULL;
    else
      mRoot = src.mRoot->copy();
  }

  //! Assignment operator to help the user manage & pass around Iters.
  Iter& operator=(const Iter& src)
  {
    if (&src != this)
    {
      if (mRoot != src.mRoot)
      {
        if (mRoot != NULL)
          mRoot->free();
        if (src.mRoot == NULL)
          mRoot = NULL;
        else
          mRoot = src.mRoot->copy();
      }
    }
    return *this;
  }

  //! Assignment operator to help the user manage & pass around Iters.
  Iter& operator=(Base* root)
  {
    if (mRoot != root)
    {
      if (mRoot != NULL)
        mRoot->free();
      if (root == NULL)
        mRoot = NULL;
      else
        mRoot = root->copy();
    }
    return *this;
  }

  ~Iter()
  {
    if (mRoot != NULL)
      mRoot->free();
  }

  //! Gets the value.  Works just like a native STL iterator
  reference operator*()
    const
  {
    return **mRoot;
  }

  //! Move forward through the iteration.  Just like STL
  void operator++()
  {
    ++(*mRoot);
  }

  //! Detect if we are at the end.  Just like (p == container.end()) in STL.
  bool atEnd() const
  {
    return mRoot->atEnd();
  }

  //! combines ++, !atEnd, and * in one operator:  NUNet* net; while (iter(&net)) {...}
  bool operator()(value_type* ptr)
  {
    return (*mRoot)(ptr);
  }

  bool operator()(const value_type** ptr)
  {
    return (*mRoot)(ptr);
  }

  typedef StlIterator<Iter<value_type> > iterator;
  typedef StlIterator<Iter<value_type> > const_iterator;

  //! get the base
  Base* getBase() const { return mRoot; }

  //! Return an STL iterator for use in simple algorithms (e.g. find_if, not sort)
  iterator begin() {return iterator(this);}

  //! Return an STL iterator for use in algorithms (e.g. find_if, not sort)
  iterator end() {return iterator(NULL);}

private:
  Base* mRoot;
};

#endif // _ITER_H_
