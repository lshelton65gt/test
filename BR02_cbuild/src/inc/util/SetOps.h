// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _SET_OPS_H_
#define _SET_OPS_H_

template<class _Set1, class _Set2, class _Set3>
inline void set_difference(const _Set1& s1, const _Set2& s2,
                           _Set3* s3)
{
#if 0
  set_difference(s1.begin(), s1.end(), s2.begin(), s2.end(),
                 inserter(s3, s3.end()));
#else
  typename _Set2::const_iterator set2End = s2.end();
  for (typename _Set1::const_iterator p = s1.begin(), e = s1.end(); p != e; ++p)
  {
    const typename _Set1::value_type& v = *p;
    if (s2.find(v) == set2End)
      s3->insert(v);
  }
#endif
}

template<class _Set1, class _Set2, class _Set3>
inline void set_intersection(const _Set1& s1, const _Set2& s2,
                                    _Set3* s3)
{
#if 0
  set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(),
                   inserter(s3, s3.end()));
#else
  typename _Set2::const_iterator set2End = s2.end();
  for (typename _Set1::const_iterator p = s1.begin(), e = s1.end(); p != e; ++p)
  {
    const typename _Set1::value_type& v = *p;
    if (s2.find(v) != set2End)
      s3->insert(v);
  }
#endif
}


//! Determine if two sets intersect without building a third set.
template<class _Set1, class _Set2>
inline bool set_has_intersection(const _Set1& s1, const _Set2& s2)
{
  typename _Set2::const_iterator set2End = s2.end();
  for (typename _Set1::const_iterator p = s1.begin(), e = s1.end(); p != e; ++p)
  {
    const typename _Set1::value_type& v = *p;
    if (s2.find(v) != set2End)
      return true;
  }
  return false;
}


//! Subtract one set from another without using a temporary set.
template<class _Set1, class _Set2>
inline void set_subtract(const _Set1 & s1, _Set2 * s2)
{
  // If the resultant set is empty there is nothing to do. It is
  // cheaper to test for that than iterate over a non empty subtract
  // set. Testing the subtract set is empty the iteration below will
  // deal with it efficiently.
  if (s2->empty()) {
    return;
  }

  // This used to use set_difference but that causes us to use a temp
  // NUNetRefSet that gets allocated and deallocated. This was part of
  // a performance problem. So now we iterate over the subtract set
  // and remove the the output set.
  for (typename _Set1::const_iterator p = s1.begin(), e = s1.end(); p != e; ++p)
  {
    const typename _Set1::value_type& v = *p;
    typename _Set2::iterator set2Pos = s2->find(v);
    if (set2Pos != s2->end()) {
      s2->erase(set2Pos);
    }
  }
}

//! Determine if s1 has s2 as a subset.
/*!
  If s2 is empty, we return true; the empty set is a subset of all sets.
 */
template<class _Set1, class _Set2>
inline bool set_has_subset(const _Set1 & s1, const _Set2 & s2)
{
  typename _Set1::const_iterator set1End = s1.end();
  for (typename _Set1::const_iterator p = s2.begin(), e = s2.end(); p != e; ++p)
  {
    const typename _Set1::value_type& v = *p;
    if (s1.find(v) == set1End)
      return false;
  }
  return true;
}

#endif
