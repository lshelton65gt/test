// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _LOOPFUNCTOR_H_
#define _LOOPFUNCTOR_H_


#include "util/StlIterator.h"

//! LoopFunctor template class
/*!
  This class provides a mechanism for a class to expose iteration over
  the elements yielded by an existing loop, but yield the result of
  some functor applied to each of those elements.
*/

template <class _Loop, class _Functor>
class LoopFunctor
{
public: CARBONMEM_OVERRIDES
  typedef typename _Loop::value_type _ObjectType;
  typedef typename _Functor::value_type value_type;
  typedef typename _Functor::reference reference;
  typedef StlIterator<LoopFunctor> iterator;

  //! Default constructor
  LoopFunctor()
  {
  }

  //! Constructor with explicit extractor instance
  LoopFunctor(_Loop loops, _Functor extractor)
    : mLoop(loops), mFunctor(extractor)
  {
  }

  //! Constructor for use with implicitly created extractor
  LoopFunctor(_Loop loop)
    : mLoop(loop)
  {
  }

  //! Copy constructor to help the user manage & pass around Loops.
  LoopFunctor(const LoopFunctor& src):
    mLoop(src.mLoop),
    mFunctor(src.mFunctor)
  {
  }

  //! Assignment operator to help the user manage & pass around Loops.
  LoopFunctor& operator=(const LoopFunctor& src)
  {
    if (&src != this)
    {
      mLoop = src.mLoop;
      mFunctor = src.mFunctor;
    }
    return *this;
  }

  void operator++()
  {
    ++mLoop;
  }

  value_type operator*()
    const
  {
    // Explicit assignment needed for Linux64 UtHashMap/Set iterators
    _ObjectType tmp = *mLoop;
    return mFunctor(tmp);
  }

  bool atEnd() const
  {
    return mLoop.atEnd();
  }

  //! combine *, ++, and atEnd()
  bool operator()(value_type* ptr)
  {
    if (mLoop.atEnd())
      return false;
    *ptr = mFunctor(*mLoop);
    ++mLoop;
    return true;
  }

  //! The start of the loop.
  iterator begin()
  {
    return iterator(this);
  }

  //! The tail of the loop
  iterator end()
  {
    return iterator(NULL);
  }

  //! Return the number of elements remaining -- note:  order N operation
  size_t size() const
  {
    return mLoop.size();
  }

private:
  _Loop mLoop;
  _Functor mFunctor;
};

//! A useful functor that simply casts a pointer to a new type
template<typename _InType, typename _OutType>
struct CastingFunctor
{
  typedef _OutType value_type;
  typedef _InType reference;
  value_type operator()(reference r) const { return static_cast<value_type>(r); }
};

#endif //  _LOOPFUNCTOR_H_
