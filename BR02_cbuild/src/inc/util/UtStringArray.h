// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// author: Mark Seneski

#ifndef _UtStringArray_h_
#define _UtStringArray_h_


#include "util/UtArray.h"
#include "util/Loop.h"

class UtString;

//! Class to manage an array of strings
/*!
  There is no use of UtStrings here. char* are used underneath. Having
  an array of UtStrings would require us to use UtArray (because
  UtVector is stl and no stl is allowed in the runtime
  library). UtArrays require pointer types, so we would have to use a
  UtString*, which can become burdensome. 

  This class manages the storage of the char*. So, a user should never
  do the following, unless he \e really knows what he's doing:
  
  \verbatim
  strarray[i] = NULL
  \endverbatim

  You would have to delete the string manually before doing that. The
  operator[] should only be used to access the char* underneath, not
  set the array slot with a char*. If you want to do that use a
  UtArray<char*> and manage the char*'s yourself.
*/
class UtStringArray
{
public: CARBONMEM_OVERRIDES
  //! Default constructor
  UtStringArray();
  //! Constructor with a reserved size
  UtStringArray(UInt32 reserveSize);

  //! copy constructor
  UtStringArray(const UtStringArray& src);

  //! operator =
  UtStringArray& operator=(const UtStringArray& src);

  //! destructor
  ~UtStringArray();

  //! The basic array type
  typedef UtArray<char*> CharArray;

  //! iterator type
  typedef CharArray::iterator iterator;
  //! Const iterator type
  typedef CharArray::const_iterator const_iterator;

  //! reverse iterator type
  typedef CharArray::reverse_iterator reverse_iterator;
  //! Const reverse iterator type
  typedef CharArray::const_reverse_iterator const_reverse_iterator;
  
  //! Add an entry by char*
  void push_back(const char* entry);

  //! Add an entry  by UtString
  void push_back(const UtString& str);

  //! Modifiable - Get the last element
  char* back()
  {
    const UtStringArray* me = const_cast<const UtStringArray*>(this);
    return const_cast<char*>(me->back());
  }

  //! Not Modifiable - Get the last element
  const char* back() const;
  
  //! Number of elements
  UInt32 size() const;

  //! Empty?
  bool empty() const;

  //! Clear out the array
  void clear();

  //! Truncates the last element
  void pop_back();

  //! const string array access
  const char* operator[](UInt32 index) const;
  
  //! string array access
  char* operator[] (UInt32 index);

  //! Resize
  void resize(UInt32 capacity);

  //! Type for looping
  typedef Loop<CharArray> UnsortedLoop;
  //! Type for constant looping
  typedef CLoop<CharArray> UnsortedCLoop;

  //! Non-const loop in order of element entry
  UnsortedLoop loopUnsorted();

  //! Const loop in order of element entry
  UnsortedCLoop loopCUnsorted() const;

  //! Begin iterator
  iterator begin();
  
  //! Begin const iterator
  const_iterator begin() const;
  
  //! end iterator
  iterator end();

  //! end const iterator
  const_iterator end() const;

  //! reverse begin iterator
  reverse_iterator rbegin();

  //! const reverse begin iterator
  const_reverse_iterator rbegin() const;

  //! reverse end iterator
  reverse_iterator rend();
  //! const reverse end iterator
  const_reverse_iterator rend() const;
 
  //! Erase the string at the given iterator
  void erase(iterator iter);

  //! Order N remove element from the array, maintaining array order
  /*! \returns the number of elements removed
   */
  size_t remove(const char* elt);

  //! Order N find element in array
  /*!
    Finds first element in array that matches elt
  */
  const_iterator find(const char* elt) const;

private:
  CharArray mStrs;

  void add(const char* str, UInt32 len);
  
  void copy(const UtStringArray& src);

};

#endif
