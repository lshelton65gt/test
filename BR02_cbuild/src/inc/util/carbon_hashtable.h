/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBON_HASHTABLE_H__
#define __CARBON_HASHTABLE_H__

/* Example of use:
 *
 * This came with the original code. I'm keeping it here for general
 * reference - MS
 *
 *      struct carbon_hashtable  *h;
 *      struct some_key   *k;
 *      struct some_value *v;
 *
 *      static UInt32         hash_from_key_fn( void *k );
 *      static int                  keys_equal_fn ( void *key1, void *key2 );
 *
 *      h = create_carbon_hashtable(16, 0.75, hash_from_key_fn, keys_equal_fn);
 *      k = (struct some_key *)     malloc(sizeof(struct some_key));
 *      v = (struct some_value *)   malloc(sizeof(struct some_value));
 *
 *      (initialise k and v to suitable values)
 * 
 *      if (! carbon_hashtable_insert(h,k,v) )
 *      {     exit(-1);               }
 *
 *      if (NULL == (found = carbon_hashtable_search(h,k) ))
 *      {    printf("not found!");                  }
 *
 *      if (!carbon_hashtable_remove(h,k))
 *      {    printf("Not found\n");                 }
 *
 */

#include "shell/carbon_shelltypes.h"
#include "util/UtSmallPtr.h"

#ifdef __cplusplus
extern "C" {
#endif

  struct carbon_hashtable_itr;
  struct carbonHashTableSizes  /* carbonPrimes is an array of these */
  {
    unsigned int numBuckets;
    unsigned int loadLimit;
  };

  //! Type for a shrunken hashEntry pointer
  typedef SMALLPTR(struct carbon_hashEntryStruct *) carbon_hashEntryPtr;

  /*!
    \brief Cell for storing chain of entries in a set hash bucket
  */
  typedef struct carbon_hashEntryStruct
  {
    carbon_hashEntryPtr next;
  } carbon_hashEntry;
  
  /* Typedef for function used to determine equality of two keys */
  typedef int (*carbonHashEqFn) (void*, const void* k1,
                                 const carbon_hashEntry* e);
  /* Typedef for function used to hash a key */
  typedef UInt32 (*carbonHashFn) (void*, const carbon_hashEntry*);

  /*!
    \brief  Hashtable structure

    This hashtable \e can handle multiple entries on a key, but don't
    use that yet. There is no handy iterator to get to the multiple
    values on the key.
  */
  struct carbon_hashtable {
    /* Probably there is not much benefit in using SmallPtrT for
     * this particular pointer, because on 64-bit machines we
     * will need to have a full 32-bits for the entry count,
     * and some at least 5 bits for the primeIndex.  So with
     * SmallPtrT we would have a 12 byte hash table structure,
     * but we want our memory to by 8-byte aligned, so we'd spend
     * 16 bytes anyway.
     */
    union {
      carbon_hashEntryPtr *table;
      carbon_hashEntryPtr singlebucket;
     } data;
    struct {
#if pfLP64
      UInt32 entrycount;
      UInt32 primeIndex;     /* Index into carbonPrimes array */
#else
      UInt32 entrycount: 27; /* I think 128M entries on a 32-bit machine
                              * is not going to be a limit we would hit */
      UInt32 primeIndex: 5;  /* we have only 28 primes so this is enough */
#endif
    } sizes;
  };
  
  /*!
   * \brief Initialize carbon hashtable structure
   
   * \param   size_index      int (0 means start small)
   * \returns                 newly created carbon_hashtable or NULL on failure
   */
  struct carbon_hashtable *
  create_carbon_hashtable(int size_index);
  void carbon_hashtable_init(struct carbon_hashtable *, int size_index);
  
  /*!
   * \brief fast version of carbon_hashtable_init with size 0
   */
# define CARBON_HASHTABLE_INIT(h) memset(h, 0, sizeof(struct carbon_hashtable))

  /*!
   * \brief Insert a set entry into the hashtable
   
   * \param   h         the carbon_hashtable to insert into
   * \param   k         the key - ownership is responsibility of caller
   * \param   hashFn    function for hashing keys
   * \param   userData  Client data
   * \param   theCarbon_SetEntry  Pointer the inserted element
   * \param   entrySize   The size in bytes of the entry
   * \param   index     Precalced hashvalue
   * \returns           non-zero for successful insertion
   *
   * This function will cause the table to expand if the insertion would take
   * the ratio of entries to table size over the maximum load factor.
   *
   * This function does not check for repeated insertions with a duplicate key.
   * when
   * the carbon_hashtable changes size, the order of retrieval of duplicate key
   * entries is reversed.
   * If in doubt, remove before insert.
   */
  
  void
  carbon_hashtable_insert(struct carbon_hashtable *h, 
                          carbonHashFn hashFn,
                          void* userData,
                          carbon_hashEntry** theCarbon_SetEntry,
                          unsigned int entrySize,
                          UInt32 index);
  
  /*! Insert an entry into the hash-table, but only if necessary.
   *! If an insertion occurred, 1 is returned and the caller
   *! must copy the key.  If an insertion was not necessary, then 
   *! 0 is returned.  In either case, the element is returned
   *! in pEntry.
   */
  int
  carbon_hashtable_maybe_insert(struct carbon_hashtable *h,
                                const void* k,
                                carbonHashFn hashFn,
                                carbonHashEqFn keyEqFn,
                                void* userData,
                                UInt32 hashVal,
                                UInt32 entrySize,
                                carbon_hashEntry** pEntry);

  /*!
   * \param   h   the carbon_hashtable to search
   * \param   k   the key to search for  - does not claim ownership
   * \param   hashFn    function for hashing keys
   * \param   keyEqFn   function for determining key equality
   * \param   userData  Client data
   * \param   hIter     Iterator that will be a valid iterator upon a
   *                    successful search. The iterator will point to the found element.
   * \param   index   Precalced hashvalue
   * \returns      1 if the key is found, 0 otherwise
   */

  int
  carbon_hashtable_search(const struct carbon_hashtable *h,
                          const void* k,
			  carbonHashFn hashFn,
			  carbonHashEqFn keyEqFn,
                          void* userData,
			  struct carbon_hashtable_itr* hIter,
                          UInt32 hashVal);
  
  /*!
   *! Alternate interface to carbon_hashtable_search, which does not
   *! use expensive carbon_hashtable_itr, but provides direct access
   *! to carbon_hashEntry*.  Note that this means you cannot use the
   *! results of this to execute a constant-time erase, or advance
   *! to the next entry.  This is for fast lookup only.
   */
  carbon_hashEntry*
  carbon_hashtable_findEntry(const struct carbon_hashtable *h,
                             const void* k,
                             carbonHashFn hashFn,
                             carbonHashEqFn keyEqFn,
                             void* userData,
                             UInt32 hashVal);
  
/*!
 * carbon_hashtable_remove
   
 * @param   h   the carbon_hashtable to remove the item from
 * @param   k   the key to search for
 * @param   hashFn    function for hashing keys
 * @param   keyEqFn   function for determining key equality
 * @param   userData  Client data
 * @param   entrySize   The size in bytes of the entry
 * \returns      1 if key was found, 0 if not found
 */

int
carbon_hashtable_remove(struct carbon_hashtable *h, const void* k,
			carbonHashFn hashFn, carbonHashEqFn keyEqFn,
                        void* userData,
                        unsigned int entrySize);

/*!
  This simply resets the hashtable size to 0 and clears all the
  entries.
  \param h           The carbon_hashtable to clear
  \param entrySize   The size in bytes of the entry
 */
void
carbon_hashtable_clear(struct carbon_hashtable* h, unsigned int entrySize);

/*!
 This is a highly specialized routine used by UtHashMap2 to clear all
 the bucket pointers following UtHashMap2::RemoveLoop.  Do not use this
 for any other purpose.  Note: this is a procedure rather than a macro
 because it needs access to the primes table which is private to
 carbon_hashtable.c.
*/
void
carbon_hashtable_clear_internal(struct carbon_hashtable* h);

/*!
  Resets the hashtable size to 0 and clears all the entries. 
  Also resizes the hashtable to efficiently store the the same
  number of entries that existed before the clear.
  \param h           The carbon_hashtable to clear
  \param entrySize   The size in bytes of the entry
 */
void
carbon_hashtable_clear_and_resize(struct carbon_hashtable* h, unsigned int entrySize);

/*!
  \brief Return the number of elements in the hashtable
 * @param   h   the carbon_hashtable
 * @return      the number of items stored in the carbon_hashtable
 */
UInt32
carbon_hashtable_count(const struct carbon_hashtable *h);

#if 0
/*!
 *
 * function to change the value associated with a key, where there already
 * exists a value bound to the key in the hashtable.
 * Source due to Holger Schemel.
 *
 * \param   h   the hashtable
 * \param   k   key
 * \param   v   value
 * \param   hashFn    function for hashing keys
 * \param   keyEqFn   function for determining key equality
 * \param   keyKind   whether the key is a pointer or another type of value
 * \param   valKind   whether the value is a pointer or another type of value
 * \returns non-zero upon a successful search; 0 if the key is not found.
 */
int
carbon_hashtable_change(struct carbon_hashtable *h,
			const void* k, void* v,
			carbonHashFn hashFn, carbonHashEqFn keyEqFn,
                        void* userData,
                        
                        enum KeyValKind keyKind,
                        enum KeyValKind valKind);
#endif


/*!
  \brief Destroys (frees) the hashtable
 * @param   h   the carbon_hashtable to destroy
 * @param entrySize   The size in bytes of the entry
 */

void
carbon_hashtable_destroy(struct carbon_hashtable *h, unsigned int entrySize);
void
carbon_hashtable_clean(struct carbon_hashtable *h, unsigned int entrySize);

#ifdef __cplusplus
}
#endif

#endif /* __CARBON_HASHTABLE_CWC22_H__ */

/*
 * Copyright (C) 2002 Christopher Clark <firstname.lastname@cl.cam.ac.uk>
 *
 * Modified by Mark Seneski
 *           - Added iterator population
 *           - Added client data member
 *           - Implemented prime-based hashing
 *           - Changed function names
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * */
