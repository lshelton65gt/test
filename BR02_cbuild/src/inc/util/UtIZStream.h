// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtIZStream_h_
#define __UtIZStream_h_

#include "util/UtIStream.h"
#include "util/Zstream.h"

//! An output stream to a Zistream
/*!
  Zistream has no real formatting capabilities, so this class does the
  formatting and then passes it to the underlying Zistream.
*/
class UtIZStream : public UtIStream
{
public: 
  CARBONMEM_OVERRIDES
  
  //! Opens the file as a Zistream
  UtIZStream(const char* fileName, void* key);

  //! Destructor - will close the file
  ~UtIZStream();


  //! UtIStream::is_open()
  virtual bool is_open() const;
  //! UtIStream::close()
  virtual bool close();
  //! UtIStream::bad()
  virtual bool bad() const;

#define UTIZSTREAM_INIT_FUNC stdEmul
  inline void UTIZSTREAM_INIT_FUNC()
  {
    ZISTREAM_INIT(mZFile);
  }

  virtual SInt64 seek(SInt64 pos, UtIO::SeekMode flag);

protected:
  virtual UInt32 readFromFile(char* buf, UInt32 len);

private:
  Zistream mZFile;

  //forbid
  UtIZStream();
  UtIZStream(const UtIZStream&);
  UtIZStream& operator=(const UtIZStream&);
};

#define UTIZSTREAM_INIT(var) \
   var.UTIZSTREAM_INIT_FUNC()

#define UTIZSTREAM_PTR_INIT(var) \
   var->UTIZSTREAM_INIT_FUNC() \

//! UtIZStream var(name)
#define UTIZSTREAM(var, name) \
   UtIZStream var (name, ZSTREAM_KEY); \
   UTIZSTREAM_INIT(var)

//! var  = new UtIZStream(name)
#define UTIZSTREAM_ALLOC(var, name) \
   var  = new UtIZStream(name, ZSTREAM_KEY); \
   UTIZSTREAM_PTR_INIT(var)

#endif
