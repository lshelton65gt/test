// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "util/UtIStream.h"

// Class to unify parsing simple tokenized parameter files
class UtParamFile : public UtIBStream {
public:
  //! ctor
  UtParamFile(const char* filename);

  //! dtor
  ~UtParamFile();

  bool getline(UtIStringStream* line);
  UInt32 getLineNumber() const {return mLineNumber;}

private:  
  UInt32 mLineNumber;
  UtString mBuf;
};

