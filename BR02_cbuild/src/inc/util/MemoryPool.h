// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef MEMORYPOOL_H_
#define MEMORYPOOL_H_


#ifndef __CarbonPlatform_h_
#include "util/CarbonPlatform.h"
#endif
#ifndef __UtHashSet_h_
#include "util/UtHashSet.h"
#endif
#ifndef __UtList_h_
#include "util/UtList.h"
#endif
#ifndef __CarbonAssert_h_
#include "util/CarbonAssert.h"
#endif

/*!
  \file
  MemoryPool class declaration.
*/

//! MemoryPool template class
/*!
 * Templatized memory pool class which also allows iteration over the
 * allocated items.
 *
 * Requires that the templated class provide methods
 *    bool isAllocated const();
 * and
 *    void putIsAllocated(bool);
 *
 * These must be implemented as non-virtual methods, accessing data that
 * is not in the first word of the object.  This is hairy and fragile, but
 * the alternatives are wasteful of either time or space.  The methods
 * will be called on objects that are currently not "live" but any
 * C++ conventions.
 *
 * Alternative 1 is for the pool to maintain a container of currently 
 * allocated objects.  That consumes a lot of extra memory, probably
 * averaging 8 extra bytes per object.  Keep in mind that to avoid
 * killing performance, the removal of the object from the container
 * would need to be in constant or at worst log N time.
 *
 * Another alternative is, whenever iteration over live objects is
 * desired, do a walk over the free-list first and build a temporary
 * set.  This set will take significant room, but it may not need to
 * be retained long.  In any case it requires a potentially long walk
 * to do this.
 *
 * So while the maintenance of the  allocated bit is burdensome on
 * the implementor of the class being templated, and complex to
 * describe, I think it's so superior in performance that it ought
 * to be done.
 */
template<class T, int _chunkSize>
class MemoryPool
{
public: CARBONMEM_OVERRIDES
  //! Overhead for an item in the memory pool.
  struct MemoryPoolItem
  {
    MemoryPoolItem *mNext;
  };

  typedef UtList<T*> ChunkList;
  typedef typename ChunkList::iterator ChunkIter;
  typedef typename T::iterator value_type;
  typedef typename T::reference reference;

  //! AllocLoop class
  class iterator : public FORWARD_ITERATOR(T)
  {
  public: CARBONMEM_OVERRIDES
    /*
    typedef T* value_type;
    typedef T*& reference;
    */
    //    typedef typename T::iterator value_type;
    //typedef typename T::reference reference;

    //! constructor for 'begin'
    iterator(ChunkList& chunkList)
    {
      mItem = NULL;
      mChunk = chunkList.begin();
      mEnd = chunkList.end();
      if (!chunkList.empty())
      {
        mItem = *mChunk;
        nextItem();
      }
    }

    //! constructor for 'end'
    iterator(ChunkIter last):
      mItem(NULL),
      mChunk(last),
      mEnd(last)
    {
    }

    //! copy ctor
    iterator(const iterator& src):
      mItem(src.mItem),
      mChunk(src.mChunk),
      mEnd(src.mEnd)
    {
    }

    //! empty contructor
    iterator():
      mItem(NULL)
    {
    }

    //! assign op
    iterator& operator=(const iterator& src)
    {
      if (&src != this)
      {
        mItem = src.mItem;
        mChunk = src.mChunk;
        mEnd = src.mEnd;
      }
      return *this;
    }

    //! destructor
    ~iterator() {
    }
      
    //! Check if we are at the end
    bool atEnd() const
    {
      return mChunk == mEnd;
    }

    //! dereference
    T *operator *() const {
      return mItem;
    }

    //! dereference
    T *operator ->() const {
      return operator *();
    }

    iterator& operator ++() {
      ++mItem;
      nextItem();
      return *this;
    }

    bool operator ==(const iterator& x) const {
      return (mItem == x.mItem) && (mChunk == x.mChunk);
    }
    bool operator !=(const iterator& x) const {
      return (mItem != x.mItem) || (mChunk != x.mChunk);
    }

    size_t size() const
    {
      INFO_ASSERT(0, "Not implemented.");
      return 0;
    }

  private:
    bool isFree(T* item) {
      return !item->isAllocated();
    }

    void nextItem()
    {
      T* lastInChunk = (*mChunk) + _chunkSize/(sizeof(T));
      while ((mItem == lastInChunk) || isFree(mItem))
      {
        if (mItem == lastInChunk)
        {
          ++mChunk;
          if (mChunk == mEnd)
          {
            mItem = NULL;
            return;
          }
          mItem = *mChunk;
          lastInChunk = (*mChunk) + _chunkSize/(sizeof(T));
        }
        else
          ++mItem;
      }
    }

    T* mItem;
    ChunkIter mChunk;
    ChunkIter mEnd;
  }; // class iterator : public FORWARD_ITERATOR

  //! constructor
  MemoryPool() : mFree(0), mAllocCnt(0)
  {
    expandTheFreeList();
  }

  //! Populate a set of the freed pointers in this pool.
  void getFreeSet(MemoryPoolPtrSet *free_ptrs)
  {
    for (MemoryPoolItem* p = mFree; p != NULL; p = p->mNext)
      free_ptrs->insertWithCheck(p);
  }

  //! Allocate a T element from the free list.
  /*!
   * Note, the user must call the constructor for the class; this will
   * not construct the object.
   */
  void* malloc()
  {
    if (mFree == 0) {
      expandTheFreeList();
    }

    // Remove from front of free list.
    MemoryPoolItem *head = mFree;
    mFree = head->mNext;
//#ifdef CDB
    CarbonMem::check(head);
    CarbonMem::scribble(head, sizeof(T));
//#endif
    ((T*) head)->putIsAllocated(true);
    ++mAllocCnt;
    return head;
  }

  //! Destroy a T element and return to the free list.
  void free(T *element)
  {
//#ifdef CDB
    INFO_ASSERT(element->isAllocated(), "MemoryPool isAllocated conflict");
    CarbonMem::check(element);
//#endif
    element->~T();
//#ifdef CDB
    CarbonMem::scribble(element, sizeof(T));
//#endif

    // Tack onto the front of the free list.
    element->putIsAllocated(false);
    MemoryPoolItem *item = reinterpret_cast<MemoryPoolItem*>(element);
    item->mNext = mFree;
    INFO_ASSERT(!element->isAllocated(), "MemoryPool !isAllocated conflict");
    mFree = item;
    --mAllocCnt;
  }

  //! Return the first allocated item; STL begin semantics.
  iterator begin()
  {
    return iterator(mChunkList);
  }

  //! Return the end of the allocated list; STL end semantics.
  iterator end()
  {
    return iterator(mChunkList.end());
  }

  //! Purge the pool
  /*! This routine clears all the allocated chunks except for one. The
   *  code does not assume the pool is freed, so if the alloc cnt is
   *  non-zero, it iterates over the pool and frees the allocated
   *  items. It must do this since those items have to be destructed
   *  as well as freed.
   */
  void purge(void)
  {
    // Free any allocated items
    if (mAllocCnt > 0) {
      freeItems();
      mAllocCnt = 0;
    }

    // Free all but one chunk.
    T* oneChunk = NULL;
    bool chunkFreed = false;
    for (ChunkIter i = mChunkList.begin(); i != mChunkList.end(); ++i) {
      T* chunk = *i;
      if (oneChunk == NULL) {
        // Save this one
        oneChunk = chunk;
      } else {
        freeChunk(chunk);
        chunkFreed = true;
      }
    }

    // If there is only one chunk, there is nothing to do. But if we
    // freed any chunks then we have to redo the free list. That is
    // because the free list has pointers to data in freed chunks.
    if (chunkFreed) {
      mChunkList.clear();
      mChunkList.push_back(oneChunk);
      initFreeList(oneChunk);
    }
  }

  //! Returns the number of allocated items in the pool.
  /*! The return executes in constant time.
   */
  int numAllocated() const { return mAllocCnt; }

  //! destructor
  ~MemoryPool()
  {
    // Run through allocated list, destroying everything.
    freeItems();

    // Run through chunks and deallocate them.
    freeChunks();
  }

private:
  //! Overhead pool book-keeping adds.
  static size_t overhead() { return sizeof(MemoryPoolItem); }

  //! Alignment of allocated chunks.
  static size_t alignment() { return sizeof(MemoryPoolItem*); }

  //! Add free elements to the free list.
  void expandTheFreeList()
  {
    size_t size = sizeof(T);

    // Bump size up to ensure ptr alignment.
    if ((size & ~(alignment()-1)) != size)
      size = (size + alignment()) & ~(alignment()-1);

    INFO_ASSERT(size >= sizeof(MemoryPoolItem), "Consistency check failed.");

    // Allocate a chunk
    T *chunk = reinterpret_cast<T*>(new char[_chunkSize]);
    mChunkList.push_back(chunk);
    initFreeList(chunk);
  }

  //! Recreate the free list from a chunk
  void initFreeList(T* chunk)
  {
    // Set up free pointers throughout chunk, back to front, so
    // we allocate low addrs first.
    mFree = NULL;
    for (int i = (_chunkSize / sizeof(T)) - 1; i >= 0; --i) {
      MemoryPoolItem* item = reinterpret_cast<MemoryPoolItem*>(chunk + i);
      ((T*) item)->putIsAllocated(false);
      item->mNext = mFree;
      mFree = item;
    }
  }

  //! Free all the items in the pool
  void freeItems()
  {
    for (iterator p = begin(), e = end(); p != e; ++p)
      free(*p);
  }

  //! Free all the chunks
  void freeChunks()
  {
    for (ChunkIter i = mChunkList.begin(); i != mChunkList.end(); ++i) {
      T* chunk = *i;
      freeChunk(chunk);
    }
  }

  void freeChunk(T* chunk)
  {
    delete [] ((char*) chunk);
  }

  //! List of chunks.
  ChunkList mChunkList;

  //! Next element on the free list.
  MemoryPoolItem *mFree;

  //! Allocated count so that we know how many we have
  int mAllocCnt;
}; // class MemoryPool


#endif
