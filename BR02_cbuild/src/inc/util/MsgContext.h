// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef MsgContext_H
#define MsgContext_H


class MsgStream;
class MsgCallback;
class UtString;
class MsgContextPrivate;

#include <stdio.h>
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#include "util/Util.h"

//! Message Handling class
/*! 
  This is a message handling class to localize the error strings & their
  formatting.  To use it, you must 
*/
class MsgContextBase 
{
public: CARBONMEM_OVERRIDES
  MsgContextBase();
  ~MsgContextBase();

  //! Control message notification.  
  /*!
    By default, a newly constructed MsgContext
    class will not save or print any errors.  You need to attach a MsgStream.
    The MsgStream class controls where error messages get printed,
    and whether status messages (e.g. # lines processed sorts of messages)
    will go there.  Usually you would send status messages only to the
    terminal (via cerr) but would never log them to a file.
  */
  void removeReportStream(MsgStream*);
  void addReportStream(MsgStream*);

  //! Message callbacks
  /*!
    The C API functions carbonAddMsgCB and carbonRemoveMessageCB allow the user
    to register handlers that get called every time a message is reported.

    Those functions allocate and delete MsgCallback objects which represent the
    user function and its context.
  */
  void addMessageCallback(MsgCallback *);
  void removeMessageCallback(MsgCallback *);

  //! Message severity.
  enum Severity {
    eSuppress,  //!< Do not print this message
    eStatus,    //!< Usually used for parsing progress.  Not saved to log files
    eNote,      //!< A message that is not an error, but is worth noting
    eWarning,   //!< A potential problem that may cause incorrect results
    eAlert,     //!< Like cError, but can be demoted to eWarning by the user
    eContinue,  //!< Continue the previous message
    eError,     //!< A problem that will prevent the completion of processing
    eFatal      //!< A problem that requires immediate program termination
  };

  //! MsgObject class
  class MsgObject
  {
  public: CARBONMEM_OVERRIDES
    MsgObject(int code,         // don't know code enum yet -- that's
                                // in the derived class
              const char* mneumonic,
              Severity severity,
              const char* location,
              const char* formattedText);
    ~MsgObject();
    MsgObject(const MsgObject&);
    MsgObject& operator=(const MsgObject&);

    int getMsgNumber() const {return mMsgNumber;}
    Severity getSeverity() const {return mSeverity;}
    const char* getLocation();
    const char* getText();
    const char* getHelp() const {return "";}
    void format(UtString*, bool includeSuppress) const;
    void report(FILE*, bool includeSuppress) const;
    void reportMessageBox(const char* message) const;

  private:
    int mMsgNumber;
    const char* mMneumonic;
    Severity mSeverity;
    mutable Severity mPreviousSeverity;
    UtString* mLocation;
    UtString* mText;
  };

  struct Severities {
    int code;
    const char* name;
    Severity severity;
    int msgCount;
  };

  //! Adjust the severity of the message by message number.
  bool putMsgNumSeverity(SInt32 msgNo, Severity sev);

  //! Adjust the severity of the message, identified by name. msgIdValid is false if there is no such message, msgChangeValid is false if change not allowed
  void putMsgSeverity(const char* msg, Severity sev, bool *msgIdValid, bool *msgChangeValid);

  //! Get the severity of a message, identified by name.
  bool getMsgSeverity(const char* msg, Severity* sev) const;

  //! Get the severity of a message, identified by index
  bool getMsgSeverity(SInt32 index, Severity* sev) const;

  //! Get the number of times this message has been issued, return false if there is no such message
  bool getMsgCount(const char* msgName, int* count) const;

  //! Get the number of messages that have been reported at a severity
  int getSeverityCount(Severity sev) const {return mSeverityCount[(int) sev];}

  //! Manually increase the severity count. E.g, for cheetah messages
  void incrSeverityCount(Severity severity);

  //! Invoke all the registered callbacks prior to issuing message. Returns the
  //! status returned by callbacks.
  /*! 
    The callbacks return either eCarbonMsgContinue ..which means continue reporting
    the error message, or eCarbonMsgStop which means stop invoking any more
    callbacks for this message. 
  */
  eCarbonMsgCBStatus processCallbacks(MsgObject* mo);

  //! Suppress all warnings
  void suppressAllNotes(bool suppress);
  
  //! Are all warnings suppressed?
  bool allNotesSuppressed() {return mAllNotesSuppressed;}
    
  //! Suppress all warnings
  void suppressAllWarnings(bool suppress);
  
  //! Are all warnings suppressed?
  bool allWarningsSuppressed() {return mAllWarningsSuppressed;}

  //! Convert severity to a string
  static const char* severityToString(Severity sev);
  
  //! Get an appropriate exit status for main(), based on error reported
  int getExitStatus();
  
  typedef UtArray<Severity> SeverityArray;
  typedef UtArray<SInt32> IntArray;
  
  //! Collect all the 'private' severity overrides within a numeric range
  /*! All Cheetah messages are between 20000 and 29999.  Jaguar gets
   *! 30000-39999.  Users can override these which changes how they get
   *! printed by Carbon, but we also need to tell Cheetah and Jaguar so
   *! that errors can be demoted.  We don't actually know *which* errors
   *! in Cheetah and Jaguar are really demotable, because they don't have
   *! the 'alert' concept.  But some of them are.  In time perhaps we will
   *! learn which are demotable.
   *!
   *! This routine provides a mechanism for us to find the Jaguar and
   *! Cheetah messages that the user wants to suppress, and pass that
   *! info along to Jaguar and Cheetah.
   */
  void getUnknownSeverityRange(SInt32 minSeverity, SInt32 maxSeverity,
                               SeverityArray*, IntArray*);

  /*! \brief check to see if a message severity is considered to be an error
   */
  static bool isErrorSeverity( const Severity sev )
  { return sev == eAlert || sev == eError || sev == eFatal; };

protected:
  Severity addMessage(int msgNumber, const char* mneumonic,
                      const char* location, const char* formattedText);

  // ld --enable-auto-import can't handle gDefaultSeverities[i].member expression
  // so give it something more palatable.
  //

  // An initialized array of severities.
  MsgContextBase::Severities* mDefaultSeverities;

  // privatized structure used here to avoid having to include
  // UtHashMap and UtVector from this common header file, which
  // turns out to be problematic when compiling cbuild output
  // using a different C++ compiler than what's used to create
  // libcarbon.a.
  MsgContextPrivate* mPrivate;
  int mSeverityCount[(int) eFatal + 1];
  bool mAllNotesSuppressed;
  bool mAllWarningsSuppressed;
  bool mSuppressed;             // keep track of this for 'continue' messages
};

//! MsgCallback
/*!
 * This class holds the context for a callback function provided by the user.
 * Callbacks are registered by the C API functions carbonAddMsgCB.
 */
class MsgCallback
{
public: CARBONMEM_OVERRIDES
  MsgCallback(CarbonMsgCB userFunc, CarbonClientData userData);
  ~MsgCallback();

  eCarbonMsgCBStatus callHandler(MsgContextBase::MsgObject * mo);

private:
  CarbonMsgCB       mUserFunc;
  CarbonClientData  mUserData;
};

//! MsgStream class
/*!
 * This abstract class defines a mechanism to handle reporting errors.  The 
 * derived class may choose to format the error message (e.g. with
 * MsgContext::mgObject::format) and write an ascii file, or it may buffer
 * the error messages in-memory for programmatic examination by a calling
 * function.  The idea here is that a library function may report detailed
 * errors using a MsgContext, and the caller may decide to report those
 * errors to the user, or may decide to handle the situation locally without
 * notifying the user.
 */
class MsgStream
{
public: CARBONMEM_OVERRIDES
  MsgStream();
  virtual ~MsgStream();

  virtual void report(MsgContextBase::MsgObject*) = 0;
};

//! MsgStreamIO class
/*!
 * This class implements a MsgStream that is connected to a FILE*,
 * typically stderr or stdout.  It could also be connected to a file.
 */
class MsgStreamIO: public MsgStream
{
public: CARBONMEM_OVERRIDES
  MsgStreamIO(FILE*, bool includeStatusMsgs);
  virtual void report(MsgContextBase::MsgObject* mo);
protected:
  FILE* mStream;
  bool mIncludeStatusMsgs;
};

#endif // MsgContext_H
