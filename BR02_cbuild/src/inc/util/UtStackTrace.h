// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtStackTrace_H_
#define __UtStackTrace_H_


// This class provides infrastructure for grabbing and interpreting runtime
// stack traces.  It is used for both memory histogramming and for profiling.

#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif
#ifndef __UtPair_h_
#include "util/UtPair.h"
#endif
#ifndef __UtString_h_
#include "util/UtString.h"
#endif
#ifndef __UtStringUtil_h_
#include "util/UtStringUtil.h"
#endif
#ifndef __UtIOStream_h_
#include "util/UtIOStream.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef __UtHashSet_h_
#include "util/UtHashSet.h"
#endif

// The number of stack-traces that we will actually capture
// must be a few higher than the number that we actually want
// to report to the user.  This is because there is a varying
// amount of slop that we don't want to bother showing the user,
// such as memory allocation infrastructure helper functions.
//
// The USER_STACK_DEPTH is likely to artificially limit the
// detail that the user sees to a subset of what we've actually
// stored.  But it helps us produce exactly the same reports on
// all platforms, which makes regression-testing easier.
#define USER_STACK_DEPTH 10
#define STACK_DEPTH_SLOP 5
#define MAX_STACK_DEPTH (USER_STACK_DEPTH + STACK_DEPTH_SLOP)

//! UtStackTrace class -- represents a stack trace
struct UtStackTrace {
  CARBONMEM_OVERRIDES

  void* mFrames[MAX_STACK_DEPTH];
  SInt32 mNum;
  static SInt32 smMaxStackDepth; // The largest stacktrace we've seen
  static void* mMainFrame;	// main() function's frame address

  UtStackTrace()
  {
    memset(mFrames, 0, sizeof(mFrames));
    mNum = 0;
  }

  UtStackTrace(const UtStackTrace& src)
  {
    memcpy(this, &src, sizeof(*this));
  }

  //! Are two stack traces identical?
  bool operator==(const UtStackTrace& st) const
  {
    return (memcmp(this, &st, sizeof(*this)) == 0);
  }
    
  bool operator!=(const UtStackTrace& st) const
  {
    return !(*this == st);
  }

//  UtStackTrace& operator=(const UtStackTrace& src)
//  {
//    if (&src != this)
//      memcpy(this, &src, sizeof(*this));
//    return *this;
//  }

  //! support for hashing on stack traces
  size_t hash() const
  {
    size_t h = 0;
    for (int i = 0; i < MAX_STACK_DEPTH; ++i)
      h = 10*h + (size_t) mFrames[i];
    return h;
  }

  //! compare two stack traces
  static int compare(const UtStackTrace& st1, const UtStackTrace& st2)
  {
    return memcmp(&st1, &st2, sizeof(st1));
  }

  //! grab the current stack frame
  void grab(SInt32 ignoreFrames = 0);

  //! STL iterator
  typedef void** iterator;

  //! get the first recorded frame
  iterator begin() {return &mFrames[0];}

  //! get the last recorded frame
  iterator end() {return &mFrames[mNum];}

  //! allow main() to tell us it's frame address
  static void putMainFrame(void* frame);
};

//! UtExeSymbolTable -- represents the symbol table for the executable
class UtExeSymbolTable
{
public: CARBONMEM_OVERRIDES
  UtExeSymbolTable();
  ~UtExeSymbolTable();

  typedef UtPair<UInt32, UtString> Entry;
  typedef UtArray<Entry*> Entries;
  typedef Entries::iterator iterator;

  // Remove all the entries
  void clear();

  //! Load the symbol table from the executable, if it's not already loaded
  bool load(const char* exeFilename);

  //! save the symbol table to a database.
  /*! errors are accessed by db->getError() */
  bool save(ZostreamDB* db);

  //! restore the symbol table from a database.
  /*! errors are accessed by db->getError() */
  bool restore(ZistreamDB* db);

  //! subjectively, is the procedure name in this string "interesting"?
  static bool isInteresting(const char* proc);

  //! simplify a line from the stack trace by removing template noise
  static const char* simplify(const char* proc, UtString* buf);

  //! Symbol-table searching functor class
  struct Cmp {
    bool operator()(const Entry* se1, const Entry* se2) const {
      return se1->first < se2->first;
    }
  };

  //! STL begin iterator
  iterator begin() {return mEntries.begin();}
  
  //! STL end iterator
  iterator end() {return mEntries.end();}
  
  template<class StackTrace>
  struct CorrelatedStackTrace {
    CARBONMEM_OVERRIDES

    UtString mTraceString;
    StackTrace mStackTrace;

    CorrelatedStackTrace(const UtString& traceString, const StackTrace& st):
      mTraceString(traceString),
      mStackTrace(st)
    {
    }

    // Are two stack traces identical?  Ignore # bytes allocated
    bool operator==(const CorrelatedStackTrace& st) const
    {
      return (mTraceString == st.mTraceString);
    }

    size_t hash() const {return StringUtil::hash(mTraceString.c_str());}

    static int compare(const CorrelatedStackTrace& st1,
                       const CorrelatedStackTrace& st2)
    {
      int cmp = StackTrace::compare(st1.mStackTrace, st2.mStackTrace);
      if (cmp == 0)
        cmp = strcmp(st1.mTraceString.c_str(), st2.mTraceString.c_str());
      return cmp;
    }

    // Sorting routine to sort by # by of bytes allocated.
    bool operator<(const CorrelatedStackTrace& st) const
    {
      return compare(*this, st) < 0;
    }

    const char* getStr() const {return mTraceString.c_str();}

    void operator+=(const StackTrace& st)
    {
      mStackTrace += st;
    }

    void print(UtOStream& file)
    {
      file << mTraceString << "\n";
    }
  }; // struct CorrelatedStackTrace

  //! print the information in the symbol table
  template<class StackTrace, class TraceLoop, class Context>
  void print(const char* exe, int detail, TraceLoop& loop, Context context,
             UtOStream& file)
  {
    if (load(exe))
    {
      StackTrace::printHeader(file);

      // Sort all the stack-traces in order of memory usage
      UtExeSymbolTable::Cmp cmp;
      UtString buf, traceBuf, lineBuf;

      // For detail=0 and detail=1, we need to re-correlate as several
      // distinct traces may appear the same after simplification.
      typedef CorrelatedStackTrace<StackTrace> CST;
      typedef UtHashSet<CST*, HashPointerValue<CST*> > CorrelationSet;
      CorrelationSet correlationSet;

      for (StackTrace* st; loop(&st);)
      {
        traceBuf.clear();
        lineBuf.clear();
        if (st->isInteresting())
        {
          if (detail >= 3)
            st->print(context, file);
          const char* prefix = "";
          UInt32 num_items = 0;
          for (UtStackTrace::iterator i = st->begin();
               (i != st->end()) && (num_items < USER_STACK_DEPTH); ++i)
          {
            UtExeSymbolTable::Entry addr(reinterpret_cast<UIntPtr>(*i), "");
            UtExeSymbolTable::iterator p = lower_bound(begin(), end(),
                                                       &addr, cmp);
            if (p != begin())
              --p;
            Entry* found = *p;
            const char* proc = found->second.c_str();

            switch (detail)
            {
            case 0:
              if (UtExeSymbolTable::isInteresting(proc))
              {
                traceBuf << UtExeSymbolTable::simplify(proc, &buf);
                i = st->end() - 1;
                ++num_items;
              }
              break;
            case 1:
              if (UtExeSymbolTable::isInteresting(proc))
              {
                traceBuf << prefix << UtExeSymbolTable::simplify(proc, &buf);
                prefix = "\n                     ";
                ++num_items;
              }
              break;
            case 2:
              // at detail level 2 and 3, do not limit the number of items
              // displayed.
              traceBuf << prefix << UtExeSymbolTable::simplify(proc, &buf);
              prefix = "\n                     ";
              break;
            case 3:
              file << prefix <<  proc;
              prefix = "\n                     ";
              break;
            };
          }
          if (detail == 3)
            file << "\n";
          else
          {
            CST trace(traceBuf, *st);
            CST* tracePtr = NULL;
            typename CorrelationSet::iterator p = correlationSet.find(&trace);
            if (p == correlationSet.end())
            {
              tracePtr = new CST(traceBuf, *st);
              correlationSet.insert(tracePtr);
            }
            else
            {
              tracePtr = *p;
              *tracePtr += *st;
            }
          }
          if (detail >= 3)
            st->printMore(file);
        } // if
      } // for

      // For the low levels of detail, we now need to spew the correlated
      // data
      if (detail < 3)
      {
        for (typename CorrelationSet::SortedLoop p = correlationSet.loopSorted();
             !p.atEnd(); ++p)
        {
          CST* cst = *p;
          cst->mStackTrace.print(context, file);
          cst->print(file);
          cst->mStackTrace.printMore(file);
        }
        while (!correlationSet.empty())
        {
          typename CorrelationSet::iterator p = correlationSet.begin();
          CST* cst = *p;
          correlationSet.erase(p);
          delete cst;
        }
      }
      file.flush();
    } // if
  } // void print

private:
  Entries mEntries;
};
#endif
