// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef __SetClosure_h_
#define __SetClosure_h_


#include "util/Loop.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"

template <typename Object, class Container, class ObjectHashType>
class SetClosure
{
public: CARBONMEM_OVERRIDES
  //! Constructor: Initialize marker to zero.
  SetClosure() :
    mCurrentMarker(0)
  {}

  //! Destructor: Delete all generated containers.
  ~SetClosure() {
    cleanupSets();
  }

  //! Reset all internal state.
  void clear() {
    cleanupSets();

    mObjectMap.clear();
    mSubsumedSets.clear();
    mCurrentMarker = 0;
  }

  //! Typedefs to assist with closure
  typedef UtHashMap<UInt32,UInt32> IntIntMap;
  typedef UtHashMap<Object,UInt32,ObjectHashType> ObjectIntMap;
  typedef UtHashMap<UInt32, Container*> IntSetMap;

  //! Add one set and determine its closure.
  void addSet(Container * objectSet) {
    for (typename Container::iterator iter = objectSet->begin();
	 iter != objectSet->end();
	 ++iter) {
      Object obj = (*iter);
      typename ObjectIntMap::iterator map_iter = mObjectMap.find(obj);

      // If this object is currently in a set, need to move it to the
      // new set. This looks more complicated than it is; trying to
      // minimize the number of set operations.
      if (map_iter != mObjectMap.end()) {
	if (map_iter->second != mCurrentMarker) {
	  UInt32 old_map_num = helperFindSetNum(map_iter->second);
	  if (old_map_num != mCurrentMarker) {
	    mSubsumedSets[old_map_num] = mCurrentMarker;
	  }
	  mObjectMap[obj] = mCurrentMarker;
	}
      } else {
	mObjectMap[obj] = mCurrentMarker;
      }
    }

    ++mCurrentMarker;
  }

  //! Convenience typedef of set of object-sets
  typedef UtHashSet<Container*> SetOfContainers;

  //! Iterator over a number of object-sets.
  typedef Loop<SetOfContainers> SetLoop;

  //! Perform closure operation and re-add the joined sets for further set addition.
  /*!
   * For performance reasons, the caller may want to perform an
   * in-place closure operation. This will reduce the size of
   * mSubsumedSets; mObjectMap will end up with the same size.
   */
  void optimize() {
    for (SetLoop loop = loopSets();
         not loop.atEnd();
         ++loop) {
      Container * container = (*loop);
      addSet(container);
    }
    cleanupSets();
  }

  //! Create a map from object to id; all objects in each closed set obtain the same id.
  void createMap(UtMap<Object,UInt32> * object_map, UInt32 & id) {
    for (SetLoop loop = loopSets();
         not loop.atEnd();
         ++loop) {
      ++id;
      Container * container = (*loop);
      for (typename Container::iterator iter = container->begin();
           iter != container->end();
           ++iter) {
        Object obj = (*iter);
        (*object_map)[obj] = id;
      }
    }
  }

  //! Determine the number of closed sets.
  /*!
   * Like loopSets, this method resets internal state. No further
   * calls to addSet() are permitted after querying the size of the
   * closure.
   */
  UInt32 size() {
    populateSets();
    return mSets.size();
  }

  //! Perform the closure operation and provide an iterator over the joined sets.
  /*!
   * This method resets internal state. No further calls to addSet()
   * are permitted after looping is initiatiated.
   */
  SetLoop loopSets() {
    populateSets();
    return SetLoop(mSets);
  }

private:
  /*!
   * Given a set number, return the set number to use.  The number
   * returned will either be the given number or whatever number
   * it is mapped to in subsumed_sets.  Note that the mapping in
   * subsumed_sets is recursive, so that lookup keeps happening
   * until a fixed point is reached.
   */
  UInt32 helperFindSetNum(UInt32 set_num) {
    UInt32 new_set_num = set_num;
    bool more_than_one_iteration = false;
    IntIntMap::iterator num_map_iter = mSubsumedSets.end();

    while ((num_map_iter = mSubsumedSets.find(new_set_num)) != mSubsumedSets.end()) {
      if (new_set_num != set_num) {
	more_than_one_iteration = true;
      }

      // Make sure we haven't set up a cycle in the map.
      INFO_ASSERT(new_set_num != num_map_iter->second, "Cycle created in map.");
      new_set_num = num_map_iter->second;
    }

    // Lazy fixup of mapping to speedup subsequent lookups.
    if (more_than_one_iteration) {
      mSubsumedSets[set_num] = new_set_num;
    }

    return new_set_num;
  }


  //! Perform the closure operation.
  void populateSets() {
    if (mCurrentMarker != 0) {
	// Now go through and populate the obj sets.
      IntSetMap groups;
      for (typename ObjectIntMap::iterator obj_map_iter = mObjectMap.begin();
	   obj_map_iter != mObjectMap.end();
	   ++obj_map_iter) {
	Object obj = obj_map_iter->first;
	UInt32 set_num = helperFindSetNum(obj_map_iter->second);

	typename IntSetMap::iterator set_map_iter = groups.find(set_num);
	if (set_map_iter == groups.end()) {
	  Container * new_group = new Container();
	  new_group->insert(obj);
	  groups[set_num] = new_group;
	  mSets.insert(new_group);
	} else {
	  Container * group = set_map_iter->second;
	  group->insert(obj);
	}
      }
    }
    mObjectMap.clear();
    mSubsumedSets.clear();
    mCurrentMarker = 0;
  }



  //! Delete all the generated containers.
  void cleanupSets() {
    for (typename SetOfContainers::iterator iter = mSets.begin();
	 iter != mSets.end();
	 ++iter) {
      Container * container = (*iter);
      delete container;
    }
    mSets.clear();
  }


  //! Set of distinct sets generated by the closure operator.
  SetOfContainers mSets;

  UInt32 mCurrentMarker;
  IntIntMap mSubsumedSets;
  ObjectIntMap mObjectMap;
};

#endif
