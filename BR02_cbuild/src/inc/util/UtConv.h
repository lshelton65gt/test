// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#ifndef __UtConv_h_
#define __UtConv_h_



/*!
  \file
  This file contains a central location for routines that perform
  common data conversions.
*/


#include "util/CarbonTypes.h"
#ifndef _CPP_CSTDDEF
#include <cstddef>             // for size_t
#endif

//! Utility class with conversion routines
/*!
  This class really acts as a namespace to collect together various conversion
  methods.
*/
class DynBitVector;
class UtString;

namespace UtConv
{
  //! Convert a string to long decimal, hex or octal. Binary not
  /*!
    This work just like StrToSignedDec except that the pointer is
    modified and the base is selectable. Also, this will return false
    if strPtr is empty.
    
    \param strPtr is a pointer to the string. It will be modified to
    point to the next invalid character. 
    \param value Pointer to where the resulting value will be stored.
    \param radix Base of conversion. Binary is not allowed.
    \param errMsg If not NULL and the conversion failed, the reason is
    put here
    \returns true if strPtr was successfully converted, false
    otherwise. If false is returne *value will equal 0.
  */
  bool strToLongModify(const char** strPtr, SInt32* value, CarbonRadix radix, UtString* errMsg);

  //! Convert a binary string to a carbon UInt32 array.
  /*!
    This will take a string that represents a vector of binary values of
    the specified bit width and converts it into a carbon style UInt32
    array.

    X and Z values are allowed.  They will be turned into 0s. If the
    drive argument is non-NULL, it will have a 0 for any bits that are
    x or z.
    
    \param s The source string.
    \param data The destination array.  The caller is responsible for making
    sure this array is large enough.
    \param drive This array will have a bit set if the original value was a
    0 or 1.  The caller is responsible for making sure this array is large
    enough.  A null value can be passed.
    \param bit_width The number of bits represented in the string.
    \param do4value If true, x's will cause the corresponding data bit
    to become 1.
    \retval true The conversion worked.
    \retval false The conversion did not work.
   */
  bool BinaryStringToUInt32(const char* s, UInt32 *data, UInt32 *drive,
                            UInt32 bit_width, bool do4value = false);

  //! Convert a hex string to a UInt32 array with 0/xz extension.
  /*!
    If the bit_width is specified to be bigger than the string size
    divided by 4 then the value is 0 extended. If the msb of the
    string is an x or a z then the string is extended with x or z,
    respectively.
    If the bit_width is less than the size of the string divided by 4
    then the string is 
    truncated. HexStringToUInt32() is then performed.

    Before deciding whether or not to truncate or extend, s will be
    stripped of any leading and trailing spaces.

    \param s String to convert
    \param data Data to put number into
    \param mask x/z mask for number. 1 for bits that are numbers
    \param bit_width Expected (binary) bit_width of result
    \param truncExtStat Status of truncation or extension. If s had to
    be extended this will == 1. If s had to be truncated, this will ==
    -1. If no resizing on s had to take place this will be 0.
  */
  bool HexStrToUInt32Fit(const char* s, UInt32 *data, UInt32 *mask, UInt32 bit_width, int* truncExtStat);
  
  //! Convert a binary string to a UInt32 array with 0/xz extension.
  /*!
    If the bit_width is specified to be bigger than the string then
    the value is 0 extended. If the msb of the string is an x or a z
    then the string is extended with x or z, respectively.
    If the bit_width is less than the size then the string is
    truncated. BinaryStringToUInt32() is then performed.
    
    Before deciding whether or not to truncate or extend, s will be
    stripped of any leading and trailing spaces.

    \param s String to convert
    \param data Data to put number into
    \param mask x/z mask for number. 1 for bits that are numbers.
    \param bit_width Expected (binary) bit_width of result
    \param truncExtStat Status of truncation or extension. If s had to
    be extended this will == 1. If s had to be truncated, this will ==
    -1. If no resizing on s had to take place this will be 0.
  */
  bool BinStrToUInt32Fit(const char* s, UInt32 *data, UInt32 *mask, UInt32 bit_width, int* truncExtStat);

  //! Same thing as BinStrToUInt32Fit, but creates a drive, not a mask
  /*!
    This creates a value that can be used to set a CarbonNet. The
    drive pointer can be NULL.
  */
  bool BinStrToValDrvFit(const char* s, DynBitVector *value, DynBitVector *drive, UInt32 bit_width, int* truncStat, bool do4State);
  
  //! Convert a hex string to a carbon UInt32 array.
  /*!
    This will take a string that represents a vector of hexidecimal values of
    the specified bit width and converts it into a carbon style UInt32
    array.

    X and Z values are allowed.  They will be turned into 0s. If the
    drive argument is non-NULL, it will have a 0 for any bits that are
    x or z.
    
    \param s The source string.
    \param data The destination array.  The caller is responsible
    for making sure this array is large enough.
    \param drive This array will have a bit set if the original value was a
    0 or 1.  The caller is responsible for making sure this array is large
    enough.  A null value can be passed.
    \param bit_width The number of bits represented in the string.
    \retval true The conversion worked.
    \retval false The conversion did not work.
   */
  bool HexStringToUInt32(const char* s, UInt32 *data, UInt32* drive,
                         UInt32 bit_width);


  //! Change a binary string to a hex string
  /*!
    \param value Initially contains the binary value. Will have the
    result of the conversion
    \returns true if value was converted. False if x or z was
    encountered. This asserts if a non binary character is encountered
    (such as 2 or 3, for example). If false is returned, the value
    string is untouched.
  */
  bool BinaryStrToHex(UtString* value);
  
  //! Change binary inputData of any length to unsigned decimal
  /*!
    This will take the inputData and transform it into an array of
    numbers representing each digit of the decimal number. This is
    done by converting the binary into BCD and iterating through the
    BCD digits populating the array. The performance of this routine
    is roughly n*log(n), where n = width. 
    
    \param inputData An array of UInt32s that represent a value. The
    array will be iterated bit by bit, extracting the BCD
    representation.
    \param width Number of significant bits in inputData
    \param result Output stream to which the calculated unsigned
    decimal number will be appended. 
    \param len Size in bytes of the result buffer. It must conform to
    octal size. So, (n+2)/3 + 1.
    \returns -1 if result buffer is not big enough; otherwise, the
    actual number of chars needed to represent the value
  */
  int BinaryToUnsignedDec(const UInt32* inputData, size_t width, 
                                  char* result, size_t len);

  //! Change binary inputData of any length to signed decimal
  /*!
    This will take the inputData and transform it into an array of
    numbers representing each digit of the decimal number. This is
    done by converting the binary into BCD and iterating through the
    BCD digits populating the array. First, it is determined if the
    inputData is negative or not (the highest bit is 1). If it is
    negative, the inputData is transformed into its 2's complement,
    which gives the absolute value, and then the BCD transformation is
    done. If it isn't negative, BinaryToUnsignedDec() is called.
    
    \param inputData An array of UInt32s that represent a value. The
    array will be iterated bit by bit, extracting the BCD
    representation.
    \param width Number of significant bits in inputData
    \param showPositive If the result is a positive number and this is
    true then prefix the number with a '+'. Negative numbers always
    have a '-' prefixed.
    \param result Output stream to which the calculated signed
    decimal number will be appended. 
    \param len Size in bytes of the result buffer. Must conform to
    octal rules: (n+2)/3 +1, plus 1 more if the value is negative.
    \returns -1 if result is big enough; otherwise, the actual number
    of chars needed to represent the data.
    
    \sa BinaryToUnsignedDec()
  */
  int BinaryToSignedDec(const UInt32* inputData, size_t width, 
                        bool showPositive,
                        char* result, size_t len);

  //! Convert a string to a signed long
  /*!
    Returns false if valStr does not begin with a number after any
    whitespace is skipped.
    
    \param valStr The string containing the value
    \param rem The remaining string after extracting the value
    \param value The number extracted from valStr. 0 if returning
    false.
    \param errMsg If not NULL and the conversion failed, the reason is
    put here.
  */
  bool StrToSignedDec(const char* valStr, char** rem, SInt32* value, UtString* errMsg);


  //! Zero extend a string representing a value.
  /*!
    Allocate a string that represents the specified number of bits.
    If the string is not wide enough, left extent with zeros.  The
    user must delete the returned string.
    
    \param s The inital value.
    \param bit_width The target number of bits to represent.
    \param radix The type of value represented by the string.
  */
  UtString * ZeroExtendValue(const char* s, UInt32 bit_width, CarbonRadix radix);
}


namespace CarbonValRW
{
  //! very efficient, platform-independent sign-extension.
  inline void signExtendVector(UInt32* vec, UInt32 numWords, UInt32 bitWidth)
  {
    // Figure out if the vector size is aligned to 32 bits
    const int lastBit = bitWidth % (sizeof(UInt32) * 8);
    const int isNotAligned = lastBit != 0;
    // If it is aligned shift is 0, otherwise shift is the amount of
    // shifts of 0x1 needed to grab the last bit from the last word of
    // the value.
    const int shift = isNotAligned * (lastBit - 1);
    const UInt32 signGrabMask = 1 << shift;
    
    // Grab the last bit. This is the sign to extend.
    const UInt32 signToExtend = (vec[numWords - 1] & signGrabMask) >> shift;
    
    // signToExtend is either 1 or 0. Use that to initialize the
    // extendMask which will eventually be or'd to the lastWord
    // Before we can or the extend mask we have to zero out the bits
    // used in the actual vector. Shift it by lastBit.
    const UInt32 extendMask = (UtUINT32_MAX * signToExtend) << lastBit;
    
    // At this point extendMask is either 0 or it has all the upper bits
    // beyond the vector set to 1. Now | (or) it to lastWord and reassign it
    // back to the vector
    vec[numWords - 1] |= extendMask;
  }

  void cpSrcToDest(UInt32* dest, const UInt8* src, size_t);

  void cpSrcToDest(UInt32* dest, const UInt16* src, size_t);

  void cpSrcToDest(UInt32* dest, const UInt32* src, size_t numWords);

  inline void cpSrcToDest(UInt32* dest, const UInt64* src, size_t)
  {
    dest[0] = UInt32(*src & 0x00000000ffffffff);
    dest[1] = UInt32((*src >> 32) & 0x00000000ffffffff);
  }
  
  void cpSrcToDest(UInt8* dest, const UInt32* src, size_t);
  
  void cpSrcToDest(UInt16* dest, const UInt32* src, size_t);
  
  void cpSrcToDest(UInt64* dest, const UInt32* src, size_t);

  void cpSrcToDest(UInt64* dest, const UInt64* src, size_t);
  void cpSrcToDest(UInt16* dest, const UInt16* src, size_t);
  void cpSrcToDest(UInt8* dest, const UInt8* src, size_t);

  void cpSrcToDest(CarbonReal* dest, const UInt64* src, size_t);
  void cpSrcToDest(UInt64* dest, const CarbonReal* src, size_t);

  bool typedIsEqual(const UInt8* val1, const UInt8* val2, size_t);
  bool typedIsEqual(const UInt16* val1, const UInt16* val2, size_t);
  bool typedIsEqual(const UInt32* val1, const UInt32* val2, size_t words);
  bool typedIsEqual(const UInt64* val1, const UInt64* val2, size_t);

  int memCompare(const void* shadow, const UInt8* val, size_t);
  
  int memCompare(const void* shadow, const UInt16* val, size_t);
  
  int memCompare(const void* shadow, const UInt32* val, size_t numWords);

  int memCompare(const void* shadow, const UInt64* val, size_t);

  int memCompareRange(const UInt32* src, const UInt8* val, 
                      size_t bitindex, size_t bitlength);
  
  int memCompareRange(const UInt32* src, const UInt16* val, 
                      size_t bitindex, size_t bitlength);
  
  int memCompareRange(const UInt32* buf, const UInt32* val, 
                      size_t bitindex, size_t bitlength);
  
  int memCompareRange(const UInt32* buf, const UInt64* val, 
                      size_t bitindex, size_t bitlength);
  
  void cpSrcWordToDest(UInt64* dest, const UInt32 src, size_t wordIndex);

  void cpSrcWordToDest(UInt32* dest, const UInt32 src, size_t wordIndex);
  
  void cpSrcWordToDest(UInt16* dest, const UInt32 src, size_t wordIndex);

  void cpSrcWordToDest(UInt8* dest, const UInt32 src, size_t wordIndex);

  void cpSrcWordToDest(UInt32* dest, const UInt64 src, size_t wordIndex);

  void cpSrcWordToDest(UInt32* dest, const UInt16 src, size_t wordIndex);

  void cpSrcWordToDest(UInt32* dest, const UInt8 src, size_t wordIndex);

  void cpSrcToDestWord(UInt32* dest, const UInt64* src, size_t wordIndex);
  void cpSrcToDestWord(UInt32* dest, const UInt32* src, size_t wordIndex);
  void cpSrcToDestWord(UInt32* dest, const UInt16* src, size_t wordIndex);
  void cpSrcToDestWord(UInt32* dest, const UInt8* src, size_t wordIndex);

  void cpSrcToDestRange(UInt64* dest, const UInt32 *src, size_t index, size_t length);
  
  void cpSrcToDestRange(UInt16* dest, const UInt32 *src, size_t index, size_t length);

  void cpSrcToDestRange(UInt8* dest, const UInt32 *src, size_t index, size_t length);

  void cpSrcToDestRange(UInt32* dest, const UInt32 *src, size_t index, size_t length);
  
  void cpSrcRangeToDest(UInt32* dest, const UInt32 *src, size_t index, size_t length);

  void cpSrcRangeToDest(UInt32* dest, const UInt64* src, 
                        size_t index, size_t length);

  void cpSrcRangeToDest(UInt32* dest, const UInt16* src, 
                               size_t index, size_t length);

  void cpSrcRangeToDest(UInt32* dest, const UInt8* src, 
                               size_t index, size_t length);
  
  void cpSrcRangeToDest(UInt32* dest, DynBitVector* src_bv,
                        size_t srcindex,  size_t length);

  void cpSrcRangeToDestRange(UInt32* dest, size_t dstIndex, 
                             const UInt32* src, size_t srcindex,
                             size_t length);
  
  //! Number to Binary String.
  /*!
    \returns number of chars needed to represent the value without the
    terminating null. -1 otherwise.
  */
  int writeBinValToStr(char* valueStr, size_t len, const UInt64* src, size_t bitwidth, bool useMinWidth = false);
  int writeBinValToStr(char* valueStr, size_t len, const UInt32* src, size_t bitwidth, bool useMinWidth = false);
  int writeBinValToStr(char* valueStr, size_t len, const UInt16* src, size_t bitwidth, bool useMinWidth = false);
  int writeBinValToStr(char* valueStr, size_t len, const UInt8*  src, size_t bitwidth, bool useMinWidth = false);
  int writeBinValToStr(char* valueStr, size_t len, const char*  src, size_t bitwidth, bool useMinWidth = false);

  //! Convert a number to a Hexadecimal String.
  /*!
    encodes \a bitwidth bits in Hexadecimal format, by default all bits are encoded
    so result may include leading zeros.

    \returns number of chars used to encode the value (not including the
    terminating null) or -1 if there was some failure to encode.

    \param valueStr pointer to buffer where result is placed
    \param len is the capacity of the \a valueStr buffer
          (be sure to leave space for the trailing null)
    \param src pointer to the value to be encoded
    \param bitwidth number of bits in \a src to be encoded,
    \param upperCase if true then the characters 'ABCDEF' are used
           instead of 'abcdef' when encoding hex
    \param useMinWidth if true then only enough characters are placed
           in \a valueStr to represent the value of \a src (no leading
           zeros are encoded).
  */  
  int writeHexValToStr(char* valueStr, size_t len, const UInt64* src, size_t bitwidth, bool upperCase = false, bool useMinWidth = false);
  int writeHexValToStr(char* valueStr, size_t len, const UInt32* src, size_t bitwidth, bool upperCase = false, bool useMinWidth = false);
  int writeHexValToStr(char* valueStr, size_t len, const UInt16* src, size_t bitwidth, bool upperCase = false, bool useMinWidth = false);
  int writeHexValToStr(char* valueStr, size_t len, const UInt8*  src, size_t bitwidth, bool upperCase = false, bool useMinWidth = false);
  
  //! Convert a number to a decimal String.
  /*!
    encodes \a bitwidth bits in Decimal format, (NOTE leading zeros
    are not encoded.)

    \returns number of chars used to encode the value (not including the
    terminating null) or -1 if there was some failure to encode.

    \param valueStr pointer to buffer where result is placed
    \param len is the capacity of the \a valueStr buffer
          (be sure to leave space for the trailing null)
    \param src pointer to the value to be encoded
    \param bitwidth number of bits in \a src to be encoded,
    \param doSigned if true then \a src is interpreted as a signed
           value and if negative then \a valueStr will include a
           minus sign '-' as the first character.
           If false then src is interpreted as an unsigned value.
  */  
  int writeDecValToStr(char* valueStr, size_t len, const UInt64* src, bool doSigned, size_t bitwidth);
  int writeDecValToStr(char* valueStr, size_t len, const UInt32* src, bool doSigned, size_t bitwidth);
  int writeDecValToStr(char* valueStr, size_t len, const UInt16* src, bool doSigned, size_t bitwidth);
  int writeDecValToStr(char* valueStr, size_t len, const UInt8*  src, bool doSigned, size_t bitwidth);

  //! Convert a number to a Octal String.
  /*!
    encodes \a bitwidth bits in Octal format, by default all bits are encoded
    so result may include leading zeros.

    \returns number of chars used to encode the value (not including the
    terminating null) or -1 if there was some failure to encode.

    \param valueStr pointer to buffer where result is placed
    \param len is the capacity of the \a valueStr buffer
          (be sure to leave space for the trailing null)
    \param src pointer to the value to be encoded
    \param bitwidth number of bits in \a src to be encoded,
    \param useMinWidth if true then only enough characters are placed
           in \a valueStr to represent the value of \a src (no leading
           zeros are encoded).
  */  
  int writeOctValToStr(char* valueStr, size_t len, const UInt64* src, size_t bitwidth, bool useMinWidth = false);
  int writeOctValToStr(char* valueStr, size_t len, const UInt32* src, size_t bitwidth, bool useMinWidth = false);
  int writeOctValToStr(char* valueStr, size_t len, const UInt16* src, size_t bitwidth, bool useMinWidth = false);
  int writeOctValToStr(char* valueStr, size_t len, const UInt8*  src, size_t bitwidth, bool useMinWidth = false);

  //! Binary stringify a value and mask (4 state logic)
  /*!
    This will take a value and a mask and replace the passed in str
    buffer with the resultant 4 state value representation (0,1,x,z)
  */
  void writeBin4ToStr(UtString* valBuf, const DynBitVector* value, 
                      const DynBitVector* mask);


  //! Same as write*ValToStr, except it corrects x/z values.
  int writeBinXZValToStr(char* valueStr, size_t len, 
                         const UInt8* src, 
                         const UInt8* xdrive, 
                         const UInt8* idrive, 
                         const UInt8* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         size_t bitwidth);
  int writeBinXZValToStr(char* valueStr, size_t len, 
                         const UInt16* src, 
                         const UInt16* xdrive, 
                         const UInt16* idrive, 
                         const UInt16* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         size_t bitwidth);
  int writeBinXZValToStr(char* valueStr, size_t len, 
                         const UInt32* src, 
                         const UInt32* xdrive, 
                         const UInt32* idrive, 
                         const UInt32* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         size_t bitwidth);
  int writeBinXZValToStr(char* valueStr, size_t len, 
                         const UInt64* src, 
                         const UInt64* xdrive,
                         const UInt64* idrive,
                         const UInt64* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         size_t bitwidth);
  int writeHexXZValToStr(char* valueStr, size_t len, 
                         const UInt8* src, 
                         const UInt8* xdrive, 
                         const UInt8* idrive, 
                         const UInt8* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         size_t bitwidth);
  int writeHexXZValToStr(char* valueStr, size_t len, 
                         const UInt16* src, 
                         const UInt16* xdrive, 
                         const UInt16* idrive, 
                         const UInt16* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         size_t bitwidth);
  int writeHexXZValToStr(char* valueStr, size_t len, 
                         const UInt32* src, 
                         const UInt32* xdrive, 
                         const UInt32* idrive, 
                         const UInt32* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         size_t bitwidth);
  int writeHexXZValToStr(char* valueStr, size_t len, 
                         const UInt64* src, 
                         const UInt64* xdrive,
                         const UInt64* idrive,
                         const UInt64* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         size_t bitwidth);
  int writeOctXZValToStr(char* valueStr, size_t len, 
                         const UInt8* src, 
                         const UInt8* xdrive, 
                         const UInt8* idrive, 
                         const UInt8* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         size_t bitwidth);
  int writeOctXZValToStr(char* valueStr, size_t len, 
                         const UInt16* src, 
                         const UInt16* xdrive, 
                         const UInt16* idrive, 
                         const UInt16* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         size_t bitwidth);
  int writeOctXZValToStr(char* valueStr, size_t len, 
                         const UInt32* src, 
                         const UInt32* xdrive, 
                         const UInt32* idrive, 
                         const UInt32* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         size_t bitwidth);
  int writeOctXZValToStr(char* valueStr, size_t len, 
                         const UInt64* src, 
                         const UInt64* xdrive,
                         const UInt64* idrive,
                         const UInt64* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         size_t bitwidth);
  int writeDecXZValToStr(char* valueStr, size_t len, 
                         const UInt8* src, 
                         const UInt8* xdrive, 
                         const UInt8* idrive, 
                         const UInt8* forceMask, 
                         const UInt32* overrideMask, 
                         bool isPulled,
                         bool doSigned,
                         size_t bitwidth);
  int writeDecXZValToStr(char* valueStr, size_t len, 
                         const UInt16* src, 
                         const UInt16* xdrive, 
                         const UInt16* idrive, 
                         const UInt16* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         bool doSigned,
                         size_t bitwidth);
  int writeDecXZValToStr(char* valueStr, size_t len, 
                         const UInt32* src, 
                         const UInt32* xdrive, 
                         const UInt32* idrive, 
                         const UInt32* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         bool doSigned,
                         size_t bitwidth);
  int writeDecXZValToStr(char* valueStr, size_t len, 
                         const UInt64* src, 
                         const UInt64* xdrive, 
                         const UInt64* idrive, 
                         const UInt64* forceMask, 
                         const UInt32* overrideMask, bool isPulled,
                         bool doSigned,
                         size_t bitwidth);
  //! Sets constant bits in val 
  /*!
    The overrideMask has a control mask in it. When it is non-zero and
    the xz mask (also in the overridemask) is zero, a constant is
    found. The idrive is set to always drive a constant bit.
  */
  void setConstantBits(UInt8* val, UInt8* idrive, const UInt32* overrideMask, size_t bitwidth);
  void setConstantBits(UInt16* val, UInt16* idrive, const UInt32* overrideMask, size_t bitwidth);
  void setConstantBits(UInt32* val, UInt32* idrive, const UInt32* overrideMask, size_t bitwidth);
  void setConstantBits(UInt64* val, UInt64* idrive, const UInt32* overrideMask, size_t bitwidth); 

  //! Mark bits that have a control bit set in the overrideMask
  /*!
    This is just like setConstantBits, except that only the control
    bits are looked at. If any of the control bits are 1, the
    corresponding bit in the valBuf will be set to 1. This is useful
    for masking off bits that cannot be changed.
  */
  void setControlBits(UInt32* valBuf, const UInt32* overrideMask, size_t bitwidth);

  size_t calcRangeIndex(int range_msb, int range_lsb, int msb, int lsb);

  void setToZero(UInt32* val, size_t numWords);
  void setToOnes(UInt32* val, size_t numWords);

  //! Sets range to zero, returns true if val changed
  bool setRangeToZero(UInt32* val, size_t bitIndex, size_t rangeLength);
  //! Sets range to ones, returns true if val changed
  bool setRangeToOnes(UInt32* val, size_t bitIndex, size_t rangeLength);
  
  UInt32 getWordMask(UInt32 numBits);
  UInt64 getWordMaskLL(UInt64 numBits);
  bool isZero(const UInt32* val, size_t numWords);
  bool isAllOnes(const UInt32* val, UInt32 width);

  UInt32 getRangeBitWidth(SInt32 range_msb, SInt32 range_lsb);
  UInt32 getRangeNumUInt32s(SInt32 range_msb, SInt32 range_lsb);

  void copyRange(UInt32* dest, const UInt32 *src, 
                 size_t dstindex, size_t length);

}

#endif
