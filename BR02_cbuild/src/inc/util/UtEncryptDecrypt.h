/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef __ENCRYPTDECRYT__
#define __ENCRYPTDECRYT__

#include "util/CarbonVersion.h"
#include "util/Zstream.h"
#include <time.h>
#include "util/RandomValGen.h"
#include "util/CarbonAssert.h"
#include "util/UtStringUtil.h"
#include "util/UtIStream.h"
#include "util/OSWrapper.h"

class UtEncryptDecrypt
{
public:
  UtEncryptDecrypt() {}

  // Take an input buffer string and encrypt it.
  UtString encryptBuffer(const char* inputBuffer);

  // Take an input buffer string and decrypt it, status indicates success
  UtString decryptBuffer(const char* inputBuffer, bool* status);
  
  // Decrypt the file and return the entire contents as a UtString
  UtString decryptFile(const char* filePath, UtString* errMsg, bool* status);

  // Encrypt the file and return the entire contents as a UtString
  UtString encryptFile(const char* fileName, UtString* errMsg, bool* status);

  // Encrypt the input file and write it out as filename.ef
  bool protectEFFile(const char* fileName, UtString* errMsg);

  // Decrypt the input file and write it out as filename.ori
  bool unprotectEFFile(const char* fileName, UtString* errMsg);
};
#endif

