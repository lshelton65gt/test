// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#ifndef _UT_BI_MAP_H_
#define _UT_BI_MAP_H_

#include "util/UtHashMap.h"

//! 2-way associative map
template<typename _Class1, typename _Class2> class UtBiMap {
  typedef UtHashMap<_Class1, _Class2> C1C2Map;
  typedef UtHashMap<_Class2, _Class1> C2C1Map;
  C1C2Map mC1C2Map;
  C2C1Map mC2C1Map;

public:
  UtBiMap() {}
  ~UtBiMap() {}

  void map(const _Class1& c1, const _Class2& c2) {
    mC1C2Map[c1] = c2;
    mC2C1Map[c2] = c1;
    INFO_ASSERT(mC1C2Map.size() == mC2C1Map.size(), "bimaps not same size");
  }

  void unmap(const _Class1& c1) {
    typename C1C2Map::iterator p = mC1C2Map.find(c1);
    INFO_ASSERT(p != mC1C2Map.end(), "bimap unamp failed");
    const _Class2& c2 = p->second;
    mC2C1Map.erase(c2);
    mC1C2Map.erase(p);
    INFO_ASSERT(mC1C2Map.size() == mC2C1Map.size(), "bimaps not same size");
  }

  void unmap(const _Class2& c2) {
    typename C2C1Map::iterator p = mC2C1Map.find(c2);
    INFO_ASSERT(p != mC2C1Map.end(), "bimap unamp failed");
    const _Class1& c1 = p->second;
    mC1C2Map.erase(c1);
    mC2C1Map.erase(p);
    INFO_ASSERT(mC1C2Map.size() == mC2C1Map.size(), "bimaps not same size");
  }

  _Class1 find(const _Class2& c2) {
    typename C2C1Map::iterator p = mC2C1Map.find(c2);
    INFO_ASSERT(p != mC2C1Map.end(), "bimap find failed");
    return p->second;
  }

  _Class2 find(const _Class1& c1) {
    typename C1C2Map::iterator p = mC1C2Map.find(c1);
    INFO_ASSERT(p != mC1C2Map.end(), "bimap find failed");
    return p->second;
  }

  bool test(const _Class2& c2) {
    typename C2C1Map::iterator p = mC2C1Map.find(c2);
    return p != mC2C1Map.end();
  }

  bool test(const _Class1& c1) {
    typename C1C2Map::iterator p = mC1C2Map.find(c1);
    return p != mC1C2Map.end();
  }

  bool empty() const {
    return mC1C2Map.empty();
  }

  void clear() {
    mC1C2Map.clear();
    mC2C1Map.clear();
  }
};

#endif
