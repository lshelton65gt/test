// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  Declaration of the GenericDigraph class derived from Graph.
  This is a useful general-purpose graph class for representing
  directed graphs.  It is possible that specialized derivatives of
  Graph may be lighter-weight for some applications, but this
  implementation is available as a default choice in cases where
  optimized derivatives are not needed.
*/

#ifndef _GENERICDIGRAPH_H_
#define _GENERICDIGRAPH_H_


#include "util/Graph.h"

#include "util/CarbonTypes.h"
#include "util/UtList.h"
#include "util/UtSet.h"
#include "util/UtMap.h"
#include "util/Iter.h"
#include "util/Loop.h"
#include "util/LoopFunctor.h"

#include <algorithm>

//! Implements the Graph interface for a directed graph
template<typename UserEdgeData, typename UserNodeData>
class GenericDigraph : public Graph
{
public: CARBONMEM_OVERRIDES
  typedef UserEdgeData EdgeDataType;
  typedef UserNodeData NodeDataType;

public:
  //! Construct an empty graph
  GenericDigraph() : mNodes() {}

  //! Forward class declaration
  class Node;

  //! Represents a directed edge with flags, scratch and user data
  class Edge : public GraphEdge
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    Edge(Node* to, UserEdgeData userData) :
      mScratch(0), mTo(to), mUserData(userData), mFlags(0)
    {}

    //! alternate constructor for GraphBuilder (ignore the start node)
    Edge(Node* /* start */, Node* to, UserEdgeData userData) :
      mScratch(0), mTo(to), mUserData(userData), mFlags(0)
    {}

    //! set the user data value associated with this edge
    void setData(UserEdgeData userData) { mUserData = userData; }

    //! retrieve the user data value associated with this edge
    UserEdgeData getData() const { return mUserData; }

    ~Edge() {};
    friend class GenericDigraph;
  private:
    //! Replace the to node for this edge
    void replaceToNode(Node* to) { mTo = to; }

    UIntPtr mScratch;        //!< a scratch value/pointer available to graph algorithms
    Node* mTo;               //!< the node this is an edge to
    UserEdgeData mUserData;  //!< the user data associated with this edge
    UInt32 mFlags;           //!< flag bits available to graph algorithms
  };

  //! Function to get the generic digraph edge from a graph edge
  Edge* castEdge(GraphEdge* graphEdge) const
  {
    return dynamic_cast<Edge*>(graphEdge);
  }

  //! Function to get the generic digraph edge from a graph edge, preserving const-ness
  const Edge* castEdge(const GraphEdge* graphEdge) const
  {
    return dynamic_cast<const Edge*>(graphEdge);
  }

  typedef UtList<Edge*> EdgeList;
  typedef Loop<EdgeList> EdgeLoop;

  //! Represents a digraph node with flags, scratch and user data
  class Node : public GraphNode
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    Node(UserNodeData userData = NULL) : 
      mScratch(0), mEdges(), mUserData(userData), mFlags(0)
    {}

    //! Add an edge from this node
    void addEdge(Edge* edge)
    {
      mEdges.push_front(edge);
    }

    typedef typename EdgeList::iterator EdgeListIter;

    //! Remove the edge from this node
    void removeEdge(Edge* edge)
    {
      EdgeListIter pos = std::find(mEdges.begin(), mEdges.end(), edge);
      if (pos != mEdges.end())
        mEdges.erase(pos);
    }

    //! set the user data value associated with this node
    void setData(UserNodeData userData) { mUserData = userData; }

    //! retrieve the user data value associated with this node
    UserNodeData getData() const { return mUserData; }

    friend class GenericDigraph;

    ~Node()
    {
      while (!mEdges.empty())
      {
        Edge* edge = mEdges.front();
        mEdges.pop_front();
        delete edge;
      }
    }
  private:
    //! Remove all edges for this node
    /*! The caller is responsible for memory deletion
     */
    void removeEdges()
    {
      mEdges.clear();
    }

    UIntPtr mScratch;       //!< a scratch value/pointer available to graph algorithms
    EdgeList mEdges;   //!< the list of edges from this node
    UserNodeData mUserData; //!< the user data associated with this node
    UInt32 mFlags;          //!< flag bits available to graph algorithms
  };

  //! Function to get the generic digraph node from a graph node
  Node* castNode(GraphNode* graphNode) const
  {
    return dynamic_cast<Node*>(graphNode);
  }

  //! Function to get the generic digraph node from a graph node, preserving const-ness
  const Node* castNode(const GraphNode* graphNode) const
  {
    return dynamic_cast<const Node*>(graphNode);
  }

  //! Add a node to the graph
  void addNode(GraphNode* node) { mNodes.push_front(node); }

  //! Add an edge to the graph
  void addEdge(GraphNode* graphNode, GraphEdge* graphEdge)
  {
    Node* start = castNode(graphNode);
    Edge* edge = castEdge(graphEdge);
    start->addEdge(edge);
  }

  //! Transpose an edge
  void transposeEdge(GraphEdge* graphEdge, GraphNode* newToGraphNode)
  {
    Edge* edge = castEdge(graphEdge);
    Node* newToNode = castNode(newToGraphNode);
    edge->replaceToNode(newToNode);
  }

  //! Get the size of the graph (number of nodes)
  size_t size() const { return mNodes.size(); }

  //! Iterator over all nodes in the graph
  Iter<GraphNode*> nodes()
  {
    return Iter<GraphNode*>::create(Loop<UtList<GraphNode*> >(mNodes));
  }

  //! Iterate over all edges from the given node
  Iter<GraphEdge*> edges(GraphNode* node) const
  {
    Node* n = static_cast<Node*>(node);
    EdgeLoop edge_loop(n->mEdges);
    typedef CastingFunctor<Edge*,GraphEdge*> EdgeFunctor;
    EdgeFunctor ef;
    return Iter<GraphEdge*>::create(LoopFunctor<EdgeLoop, EdgeFunctor>(edge_loop, ef));
  }

  //! Get the endpoint of the edge
  GraphNode* endPointOf(const GraphEdge* edge) const 
  {
    const Edge* e = static_cast<const Edge*>(edge);
    return e->mTo;
  }

  //! Replace the edge end point with the given node
  virtual void mutateEndPoint(GraphEdge* graphEdge, GraphNode* graphNode)
  {
    Edge* edge = castEdge(graphEdge);
    Node* node = castNode(graphNode);
    edge->replaceToNode(node);
  }

  //! walk over the graph using a supplied GraphWalker
  void walk(GraphWalker* gw);

  //! Set flag bits on a node (OR in flags from mask)
  void setFlags(GraphNode* node, UInt32 mask) { Node* n = static_cast<Node*>(node); n->mFlags |= mask; }
  //! Clear flag bits on a node (clear flags that are set in mask)
  void clearFlags(GraphNode* node, UInt32 mask) { Node* n = static_cast<Node*>(node); n->mFlags &= ~mask; }
  //! Test if any matching flag bit is set on a node
  bool anyFlagSet(const GraphNode* node, UInt32 mask) const { const Node* n = static_cast<const Node*>(node); return (n->mFlags & mask) != 0; }
  //! Test is all matching flag bits are set on a node
  bool allFlagsSet(const GraphNode* node, UInt32 mask) const { const Node* n = static_cast<const Node*>(node); return (n->mFlags & mask) == mask; }
  //! Get the full set of flag bits for a node
  UInt32 getFlags(const GraphNode* node) const { const Node* n = static_cast<const Node*>(node); return n->mFlags; }
  //! Set the scratch field on a node
  void setScratch(GraphNode* node, UIntPtr scratch) { Node* n = static_cast<Node*>(node); n->mScratch = scratch; }
  //! Retrieve the scratch field on a node
  UIntPtr getScratch(const GraphNode* node) const { const Node* n = static_cast<const Node*>(node); return n->mScratch; }

  //! Set flag bits on an edge (OR in flags from mask)
  void setFlags(GraphEdge* edge, UInt32 mask) { Edge* e = static_cast<Edge*>(edge); e->mFlags |= mask; }
  //! Clear flag bits on an edge (clear flags that are set in mask)
  void clearFlags(GraphEdge* edge, UInt32 mask) { Edge* e = static_cast<Edge*>(edge); e->mFlags &= ~mask; }
  //! Test if any matching flag bit is set on an edge
  bool anyFlagSet(const GraphEdge* edge, UInt32 mask) const { const Edge* e = static_cast<const Edge*>(edge); return (e->mFlags & mask) != 0; }
  //! Test is all matching flag bits are set on an edge 
  bool allFlagsSet(const GraphEdge* edge, UInt32 mask) const { const Edge* e = static_cast<const Edge*>(edge); return (e->mFlags & mask) == mask; }
  //! Get the full set of flag bits for an edge
  UInt32 getFlags(const GraphEdge* edge) const { const Edge* e = static_cast<const Edge*>(edge); return e->mFlags; }
  //! Set the scratch field on an edge
  void setScratch(GraphEdge* edge, UIntPtr scratch) { Edge* e = static_cast<Edge*>(edge); e->mScratch = scratch; }
  //! Retrieve the scratch field on an edge
  UIntPtr getScratch(const GraphEdge* edge) const { const Edge* e = static_cast<const Edge*>(edge); return e->mScratch; }

  //! Remove the set of nodes and all edges incident on them from the graph
  void removeNodes(const GraphNodeSet& removeSet);

  //! Remove all the edges for a node
  /*! The caller is responsible for memory deletion
   */
  void removeEdges(GraphNode* graphNode)
  {
    Node* node = castNode(graphNode);
    node->removeEdges();
  }

  //! Return a copy of the subgraph of the graph made from the given nodes and the edges between them
  /*! \param keepSet     the set of nodes to keep in the subgraph
   *  \param copyScratch true if the scratch fields should be copied into the subgraph nodes.
   *                     Normally scratch values are not copied, but this flag allows this
   *                     policy to be reversed, in case the scratch fields contain data
   *                     which is also relevant to the copied subgraph nodes.
   */
  virtual Graph* subgraph(GraphNodeSet& keepSet, bool copyScratch = false);

  ~GenericDigraph()
  {  
    // delete all nodes (which will delete their edges, in turn)
    while (!mNodes.empty())
    {
      Node* node = static_cast<Node*>(mNodes.front());
      mNodes.pop_front();
      delete node;
    }
  }

private:
  typedef UtList<GraphNode*> GraphNodeList;
  GraphNodeList mNodes; //!< a list of all nodes in the graph
}; // class GenericDigraph : public Graph


/*!
 * Prune nodes in the remove set and edges incident on them from the graph.
 * Any pointers to the pruned nodes or edges are invalid after the call.
 */
template<typename UserEdgeData, typename UserNodeData>
void GenericDigraph<UserEdgeData,UserNodeData>::removeNodes(const GraphNodeSet& removeSet)
{
  UtList<GraphNode*> keepNodes;
  for (GraphNodeList::iterator n = mNodes.begin(); n != mNodes.end(); ++n)
  {
    GraphNode* node = *n;
    if (removeSet.find(node) != removeSet.end())
    {
      delete node;
    }
    else
    {
      keepNodes.push_front(node);
      Node* dgn = castNode(node);
      UtList<GraphEdge*> deleteEdges;
      for (Iter<GraphEdge*> iter = edges(node); !iter.atEnd(); ++iter)
      {
        GraphEdge* edge = *iter;
        if (removeSet.find(endPointOf(edge)) != removeSet.end())
        {
          deleteEdges.push_front(edge);
        }
      }
      for (UtList<GraphEdge*>::iterator e = deleteEdges.begin(); e != deleteEdges.end(); ++e)
      {
        GraphEdge* edge = *e;
        Edge* dge = castEdge(edge);
        dgn->removeEdge(dge);
        delete edge;  
      }
    }
  }
  mNodes.swap(keepNodes);
}

/*!
 * Retrieve a copy of the subgraph of this graph made from only nodes in the 
 * supplied keep set, and the edges between those nodes.  The graph returned
 * is an independent copy of a portion of the original graph -- the caller
 * can free the original graph and still use the subgraph.  The caller must
 * delete the subgraph when they are through with it.
 */
template<typename UserEdgeData, typename UserNodeData>
Graph*
GenericDigraph<UserEdgeData,UserNodeData>::subgraph(GraphNodeSet& keepSet, bool copyScratch)
{
  GenericDigraph<UserEdgeData,UserNodeData>* sg = new GenericDigraph<UserEdgeData,UserNodeData>;
  UtMap<Node*,Node*> nodeMap;
  // copy nodes in subgraph
  for (GraphNodeSet::const_iterator n = keepSet.begin(); n != keepSet.end(); ++n)
  {
    Node* gdn = castNode(*n);
    Node* gdn_copy = new Node(gdn->getData());
    sg->addNode(gdn_copy);
    sg->clearFlags(gdn_copy, ~0u);
    sg->setFlags(gdn_copy, getFlags(gdn));
    if (copyScratch)
      sg->setScratch(gdn_copy, getScratch(gdn));
    nodeMap[gdn] = gdn_copy;
  }

  // copy edges in subgraph
  for (GraphNodeSet::const_iterator n = keepSet.begin(); n != keepSet.end(); ++n)
  {
    GraphNode* node = *n;
    for (Iter<GraphEdge*> iter = edges(node); !iter.atEnd(); ++iter)
    {
      GraphEdge* edge = *iter;
      GraphNode* to = endPointOf(edge);
      Node* gdto = castNode(to);
      typename UtMap<Node*,Node*>::iterator pos = nodeMap.find(gdto);
      if (pos != nodeMap.end())
      {
        Node* new_gdto = pos->second;
        Edge* gde = castEdge(edge);
        Edge* new_gde = new Edge(new_gdto, gde->getData());
        Node* new_gdn = nodeMap[castNode(node)];
        new_gdn->addEdge(new_gde);
        sg->clearFlags(new_gde, ~0u);
        sg->setFlags(new_gde, getFlags(gde));
        if (copyScratch)
          sg->setScratch(new_gde, getScratch(gde));
      }
    }
  }
  return sg;
}

#endif // _GENERICDIGRAPH_H_
