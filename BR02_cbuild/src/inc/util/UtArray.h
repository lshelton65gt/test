// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#ifndef __UtArray_h_
#define __UtArray_h_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#include "util/CarbonTypes.h"
#include "util/StlIterator.h"
#include "util/CarbonAssert.h"
#include "util/ConstantRange.h"
#include <cstring>              // memmove
#include <functional>

//! Space-efficient dynamically-sized pointer array class
/*! For vectors of size 32k or smaller, use only one
 *! word for holding size and capacity.  If the high-order
 *! bit is set, then we use the mData[-1] to hold the
 *! capacity.
 *!
 *! This class has a max capacity of 2^31-1.  All the indexing
 *! uses sized integers, which is in general much safer.
 *! 
 */
class UtPtrArray
{
public: CARBONMEM_OVERRIDES
  //! default ctor
  UtPtrArray();

  //! construct an array with specified size and capacity
  UtPtrArray(SIntPtr sz, SIntPtr capacity = 0, bool clearData = true);

  typedef const void* const* ConstPtrArray;

  //! construct an array with specified size and capacity, and data
  UtPtrArray(ConstPtrArray data, SIntPtr Ptr, SIntPtr capacity);

  //! copy ctor
  UtPtrArray(const UtPtrArray& src);

  //! assign op
  UtPtrArray& operator=(const UtPtrArray& src);

  //! dtor
  ~UtPtrArray();

  //! array-index operator -- does not resize, bounds are not asserted
  void*& operator[](SInt32 index) {
    return mData[index];
  }

  //! const array-index operator -- does not resize, bounds are not asserted
  const void* operator[](SInt32 index) const {
    return mData[index];
  }

  //! append one element, resizing if necessary
  void push_back(void* data);

  //! remove last element
  void pop_back() {resize(size() - 1);}

  //! Order N remove element from the array, retaining array order
  /*! \returns the number of elements removed
   */
  size_t remove(const void* elt);

  struct Predicate : std::unary_function<const void *, bool> {
    CARBONMEM_OVERRIDES
    virtual ~Predicate();
    virtual result_type operator()(argument_type) const = 0;
  };

  //! Order N remove elements from the list for which the predicate functor
  //! returns true, potentially changing array order
  /*! \returns the number of elements removed */
  size_t removeMatching(const Predicate&);

  //! retrieve last element
  void*& back() {return (*this)[size() - 1];}

  //! retrieve last element
  const void* back() const {return (*this)[size() - 1];}

  //! Make the array be the specified size
  void resize(SIntPtr sz, bool clearNewElements = true);

  //! Reserve space for the given number of array elements, without resizing
  void reserve(SIntPtr cap);

  //! Clear contents, freeing all the memory
  void clear();

  //! swap contents of two vectors
  void swap(UtPtrArray& other);

  //! const_iterator class
  class const_iterator: public RANDOM_ACCESS_ITERATOR(UtPtrArray) {
  public: CARBONMEM_OVERRIDES
    typedef const void*& reference;
    typedef ptrdiff_t difference_type;

    const_iterator(): mPtr(NULL) {}
    const_iterator(ConstPtrArray ptr): mPtr(ptr) {}
    ~const_iterator() {}
    bool operator==(const const_iterator& iter) const {return iter.mPtr == mPtr;}
    bool operator!=(const const_iterator& iter) const {return iter.mPtr != mPtr;}
    bool operator<(const const_iterator& iter) const {return mPtr < iter.mPtr;}
    bool operator<=(const const_iterator& iter) const {return mPtr <= iter.mPtr;}
    bool operator>(const const_iterator& iter) const {return mPtr > iter.mPtr;}
    bool operator>=(const const_iterator& iter) const {return mPtr >= iter.mPtr;}
    const_iterator& operator++() {++mPtr; return *this;}
    const_iterator& operator--() {--mPtr; return *this;}
    const_iterator operator++(int) {
      const_iterator prev(*this);
      ++mPtr;
      return prev;
    }
    const_iterator operator--(int) {
      const_iterator prev(*this);
      --mPtr;
      return prev;
    }
    SIntPtr operator-(const const_iterator& i) const {return mPtr - i.mPtr;}
    const_iterator operator+(int k) const {return const_iterator(mPtr + k);}
    const_iterator operator-(int k) const {return const_iterator(mPtr - k);}
    reference operator*() const {return (reference) *mPtr;}

    const void* operator->() const {return &(operator*());}
    const_iterator operator+=(int k) {mPtr += k; return *this;}
    const_iterator operator-=(int k) {mPtr -= k; return *this;}
  private:
    ConstPtrArray mPtr;
  };

  //! iterator class
  class iterator: public RANDOM_ACCESS_ITERATOR(UtPtrArray) {
  public: CARBONMEM_OVERRIDES
    typedef void*& reference;
    typedef ptrdiff_t difference_type;

    iterator(): mPtr(NULL) {}
    iterator(void** ptr): mPtr(ptr) {}
    ~iterator() {}
    bool operator==(const iterator& iter) const {return iter.mPtr == mPtr;}
    bool operator!=(const iterator& iter) const {return iter.mPtr != mPtr;}
    bool operator<(const iterator& iter) const {return mPtr < iter.mPtr;}
    bool operator<=(const iterator& iter) const {return mPtr <= iter.mPtr;}
    bool operator>(const iterator& iter) const {return mPtr > iter.mPtr;}
    bool operator>=(const iterator& iter) const {return mPtr >= iter.mPtr;}
    iterator& operator++() {++mPtr; return *this;}
    iterator& operator--() {--mPtr; return *this;}
    iterator operator++(int) {
      iterator prev(*this);
      ++mPtr;
      return prev;
    }
    iterator operator--(int) {
      iterator prev(*this);
      --mPtr;
      return prev;
    }
    reference operator*() const {return *mPtr;}
    operator const_iterator() const {return const_iterator(mPtr);}
    SIntPtr operator-(const iterator& i) const {return mPtr - i.mPtr;}
    iterator operator+(int k) const {return iterator(mPtr + k);}
    iterator operator-(int k) const {return iterator(mPtr - k);}

    void* operator->() const {return &(operator*());}
    iterator operator+=(int k) {mPtr += k; return *this;}
    iterator operator-=(int k) {mPtr -= k; return *this;}
  private:
    void** mPtr;
  };

  //! get first element in iteration
  iterator begin() {return iterator(mData);}

  //! get last element in iteration
  iterator end() {return iterator(mData + size());}

  //! get first element in iteration
  const_iterator begin() const {return const_iterator(mData);}

  //! get last element in iteration
  const_iterator end() const {return const_iterator(mData + size());}

  //! reverse_iterator class
  class reverse_iterator: public RANDOM_ACCESS_ITERATOR(UtPtrArray) {
  public: CARBONMEM_OVERRIDES
    typedef ptrdiff_t difference_type;

    reverse_iterator(): mPtr(NULL) {}
    reverse_iterator(void** ptr): mPtr(ptr) {}
    ~reverse_iterator() {}
    bool operator==(const reverse_iterator& iter) const {return iter.mPtr == mPtr;}
    bool operator!=(const reverse_iterator& iter) const {return iter.mPtr != mPtr;}
    bool operator<(const reverse_iterator& iter) const {return mPtr > iter.mPtr;}
    bool operator<=(const reverse_iterator& iter) const {return mPtr >= iter.mPtr;}
    bool operator>(const reverse_iterator& iter) const {return mPtr < iter.mPtr;}
    bool operator>=(const reverse_iterator& iter) const {return mPtr <= iter.mPtr;}
    reverse_iterator& operator++() {--mPtr; return *this;}
    reverse_iterator& operator--() {++mPtr; return *this;}
    reverse_iterator operator++(int) {
      reverse_iterator prev(*this);
      --mPtr;
      return prev;
    }
    reverse_iterator operator--(int) {
      reverse_iterator prev(*this);
      ++mPtr;
      return prev;
    }
    SIntPtr operator-(const reverse_iterator& i) const {return i.mPtr - mPtr;}
    reverse_iterator operator+(int k) const {return reverse_iterator(mPtr - k);}
    reverse_iterator operator-(int k) const {return reverse_iterator(mPtr + k);}

    const void* operator->() const {return &(operator*());}
    reverse_iterator operator+=(int k) {mPtr -= k; return *this;}
    reverse_iterator operator-=(int k) {mPtr += k; return *this;}

    void*& operator*() const {return *mPtr;}
  private:
    void** mPtr;
  };

  //! get first element in iteration
  reverse_iterator rbegin() {return reverse_iterator(mData + size() - 1);}

  //! get last element in iteration
  reverse_iterator rend() {return reverse_iterator(mData - 1);}

  //! const_reverse_iterator class
  class const_reverse_iterator: public RANDOM_ACCESS_ITERATOR(UtPtrArray) {
  public: CARBONMEM_OVERRIDES
    typedef const void*& reference;
    typedef ptrdiff_t difference_type;

    const_reverse_iterator(): mPtr(NULL) {}
    const_reverse_iterator(ConstPtrArray ptr): mPtr(ptr) {}
    ~const_reverse_iterator() {}
    bool operator==(const const_reverse_iterator& iter) const {return iter.mPtr == mPtr;}
    bool operator!=(const const_reverse_iterator& iter) const {return iter.mPtr != mPtr;}
    bool operator<(const const_reverse_iterator& iter) const {return mPtr > iter.mPtr;}
    bool operator<=(const const_reverse_iterator& iter) const {return mPtr >= iter.mPtr;}
    bool operator>(const const_reverse_iterator& iter) const {return mPtr < iter.mPtr;}
    bool operator>=(const const_reverse_iterator& iter) const {return mPtr <= iter.mPtr;}
    const_reverse_iterator& operator++() {--mPtr; return *this;}
    const_reverse_iterator& operator--() {++mPtr; return *this;}
    const_reverse_iterator operator++(int) {
      const_reverse_iterator prev(*this);
      --mPtr;
      return prev;
    }
    const_reverse_iterator operator--(int) {
      const_reverse_iterator prev(*this);
      ++mPtr;
      return prev;
    }

    SIntPtr operator-(const const_reverse_iterator& i) const {return i.mPtr - mPtr;}
    const_reverse_iterator operator+(int k) const {
      return const_reverse_iterator(mPtr - k);
    }
    const_reverse_iterator operator-(int k) const {
      return const_reverse_iterator(mPtr + k);
    }

    const void* operator->() const {return &(operator*());}
    const_reverse_iterator operator+=(int k) {mPtr -= k; return *this;}
    const_reverse_iterator operator-=(int k) {mPtr += k; return *this;}

    reference operator*() const {return (reference) *mPtr;}
  private:
    ConstPtrArray mPtr;
  };

  //! get first element in iteration
  const_reverse_iterator rbegin() const {
    return const_reverse_iterator(mData + size() - 1);
  }

  //! get last element in iteration
  const_reverse_iterator rend() const {
    return const_reverse_iterator(mData - 1);
  }

  //! Find the smallest power-of-two that's >= x.
  /*! returns 0 if x > 2^31 */
  static UInt32 nextPowerOfTwo(UInt32 x);

  //! Are the two ptr arrays equal?
  bool operator==(const UtPtrArray& other) const;

  //! Are the two ptr arrays unequal?
  bool operator!=(const UtPtrArray& other) const {return !(*this == other);}

  //! Return the number of elements in the array
  UInt32 size() const;

  //! Is the vector empty()?
  bool empty() const {return size() == 0;}

  //! Erase the specified element (order N)
  void erase(iterator&);

  //! Erase the range specified by the iterators (order N)
  void erase(iterator&, iterator&);

  //! Debug method to print the pointers as hex numbers
  void printPtrs();

  //! Debug method to print the pointers as char*
  void printStrings();

  //! Amount of total space allocated for this array.
  SIntPtr capacity() const;

  //! insert from an iterator, into a position identified by an index
  template<class _Iter> void insert(
    size_t pos,
    _Iter srcBegin,
    const _Iter& srcEnd)
  {
    size_t numToInsert = srcEnd - srcBegin;
    size_t old_size = size();
    resize(old_size + numToInsert);
    void** first = mData;
    void** insertion_point = first + pos;
    void** empty_space = first + old_size;
    
    // If we are not inserting at the end then we have to move some memory
    if (insertion_point != empty_space) {
      memmove(insertion_point + numToInsert, insertion_point,
              (old_size - pos) * sizeof(void*));
    }
    memcpy(insertion_point, &(*srcBegin), numToInsert * sizeof(void*));
  }

private:
  //! initialize all member vars, copy data
  void init(ConstPtrArray data, SIntPtr sz, SIntPtr cap);

  //! is this array in "big" mode (capacity >= 32k?)
  bool isBig() const;

  //! Set up data struct to have size & capacity as specified, unitialized
  void alloc(SIntPtr sz, SIntPtr cap);

  //! Clear the sz words starting at word start
  void clearRange(SIntPtr start, SIntPtr sz);

  void **mData;
  union {
    struct {
      UInt16 mSize;
      UInt16 mCapacity;
    } mSmall;
    UInt32 mSize;                // used only in "big" mode
  } mControl;
};

//! template class for small-footprint, efficient arrays of
//! simple objects like pointers and integers.  Note: copy
//! constructors and destructs will not be called for these!
//! Note: This currently doesn't work for floats or doubles,
//! as the parameter to push_back is cast to a UIntPtr, which
//! changes the bits.
template<class _Obj> class UtArray
{
public: CARBONMEM_OVERRIDES
  typedef _Obj value_type;
  typedef UInt32 size_type;
  typedef _Obj& reference;
  typedef const _Obj& const_reference;

  //! ctor
  UtArray() {
    INFO_ASSERT((sizeof (_Obj)) <= sizeof(void*), "Consistency check failed.");
  }

  //! ctor with size
  UtArray(SIntPtr sz, SIntPtr capacity = 0, bool clearData = true):
    mArray(sz, capacity, clearData) {
    INFO_ASSERT((sizeof (_Obj)) <= sizeof(void*), "Consistency check failed.");
  }

  //! copy ctor 
  UtArray(const UtArray& src): mArray(src.mArray) {
    INFO_ASSERT((sizeof (_Obj)) <= sizeof(void*), "Consistency check failed.");
  }

  //! dtor
  ~UtArray() {}

  //! Assign op
  UtArray& operator=(const UtArray& src) {
    if (&src != this)
      mArray = src.mArray;
    return *this;
  }

  //! append one element, resizing if necessary
  void push_back(_Obj data) {
    mArray.push_back((void*)(UIntPtr) data);
  }

  //! remove last element
  void pop_back() {mArray.pop_back();}

  //! retrieve last element
  reference back() {return (*this)[size() - 1];}

  //! retrieve last element
  const value_type back() const {return (*this)[size() - 1];}

  //! Do both vectors contain the same elements?
  bool operator==(const UtArray<_Obj>& other) const
  {
    return mArray == other.mArray;
  }

  //! Are the two ptr arrays unequal?
  bool operator!=(const UtArray<_Obj>& other) const {return !(*this == other);}

  //! Order N remove element from the array, maintaining array order
  /*! \returns the number of elements removed
   */
  size_t remove(const_reference elt) {return mArray.remove((const void*) elt);}

  //! Order N remove element from the list, maintaining array order
  /*! \returns the number of elements removed
   */
  template<class _Predicate>
  size_t removeMatching(const _Predicate& userPred) {
    struct Pred: public UtPtrArray::Predicate {
      Pred(const _Predicate& pred): mPred(pred) {}
      bool operator()(const void* ptr) const {
#if pfGCC_2
        return mPred((const_reference)ptr);
#elif pfGCC_4
        return mPred (reinterpret_cast<typename _Predicate::argument_type>(ptr));
#else
        return mPred(reinterpret_cast<const_reference>(ptr));
#endif
      }
      const _Predicate& mPred;
    };
    Pred pred(userPred);
    return mArray.removeMatching(pred);
  }

  //! Make the array be the specified size
  void resize(SIntPtr sz, bool clearExtra = true) {
    mArray.resize(sz, clearExtra);
  }

  //! Capacity of array
  SIntPtr capacity() const {
    return mArray.capacity();
  }

  //! Reserve space for the given number of array elements, without resizing
  void reserve(SIntPtr cap) {mArray.reserve(cap);}

  //! Clear contents, freeing memory
  void clear() {
    mArray.clear();
  }

  //! Swap contents of two arrays
  void swap(UtArray& other) {
    mArray.swap(other.mArray);
  }

  //! return the array size
  UInt32 size() const {return mArray.size();}

  //! Is the vector empty()?
  bool empty() const {return mArray.empty();}

  //! index into the array
  reference operator[](SInt32 index) {return (reference) mArray[index];}

  //! index into the array
  const value_type operator[](SInt32 index) const {
    // Cast through UIntPtr to avoid "cast from pointer to integer of
    // different size" warning on 32- or 64-bit machines.
    return (value_type)(UIntPtr) mArray[index];
  }

  class const_iterator: public RANDOM_ACCESS_ITERATOR(const _Obj)
  {
  public: CARBONMEM_OVERRIDES
    typedef const _Obj value_type;
    typedef const _Obj& reference;
    typedef const _Obj* pointer;
    typedef ptrdiff_t difference_type;

    const_iterator(const _Obj* ptr): mPtr(ptr) {}
    const_iterator(): mPtr(0) {}
    ~const_iterator() {}

    const_iterator(const UtPtrArray::const_iterator& ptr): mPtr(ptr) {}
    const_iterator(const const_iterator& src): mPtr(src.mPtr) {}

    const_iterator& operator=(const const_iterator& src)
    {
      mPtr = src.mPtr;
      return *this;
    }

    bool operator==(const const_iterator& i) const {return i.mPtr == mPtr;}
    bool operator!=(const const_iterator& i) const {return i.mPtr != mPtr;}
    bool operator<(const const_iterator& iter) const {return mPtr < iter.mPtr;}
    bool operator<=(const const_iterator& iter) const {return mPtr <= iter.mPtr;}
    bool operator>(const const_iterator& iter) const {return mPtr > iter.mPtr;}
    bool operator>=(const const_iterator& iter) const {return mPtr >= iter.mPtr;}
    SIntPtr operator-(const const_iterator& i) const {return mPtr - i.mPtr;}
    const_iterator operator-(int k) const {
      return const_iterator(mPtr - k);
    }
    const_iterator operator+(int k) const {
      return const_iterator(mPtr + k);
    }
    const_iterator operator++() {++mPtr; return *this;}
    const_iterator operator--() {--mPtr; return *this;}
    const_iterator operator++(int) {const_iterator prev(*this); ++mPtr; return prev;}
    const_iterator operator--(int) {const_iterator prev(*this); --mPtr; return prev;}
    reference operator*() const {return (reference) *mPtr;}
    pointer operator->() {return &(operator*());}
    pointer operator->() const {return &(operator*());}
    const_iterator operator+=(int k) {mPtr += k; return *this;}
    const_iterator operator-=(int k) {mPtr -= k; return *this;}

    UtPtrArray::const_iterator mPtr;
  };

  class iterator: public RANDOM_ACCESS_ITERATOR(_Obj)
  {
  public: CARBONMEM_OVERRIDES
    typedef _Obj value_type;
    typedef _Obj& reference;
    typedef ptrdiff_t difference_type;
    typedef const _Obj* pointer;

    iterator(_Obj* ptr): mPtr(ptr) {}
    iterator(): mPtr(0) {}
    iterator(const UtPtrArray::iterator& ptr): mPtr(ptr) {}
    iterator(const iterator& src): mPtr(src.mPtr) {}
    iterator& operator=(const iterator& src)
    {
      mPtr = src.mPtr;
      return *this;
    }
    ~iterator() {}


    bool operator==(const iterator& i) const {return i.mPtr == mPtr;}
    bool operator!=(const iterator& i) const {return i.mPtr != mPtr;}
    bool operator<(const iterator& iter) const {return mPtr < iter.mPtr;}
    bool operator<=(const iterator& iter) const {return mPtr <= iter.mPtr;}
    bool operator>(const iterator& iter) const {return mPtr > iter.mPtr;}
    bool operator>=(const iterator& iter) const {return mPtr >= iter.mPtr;}
    SIntPtr operator-(const iterator& i) const {return mPtr - i.mPtr;}
    iterator operator-(int k) const {
      return iterator(mPtr - k);}
    iterator operator+(int k) const {
      return iterator(mPtr + k);}
    iterator operator++() {++mPtr; return *this;}
    iterator operator--() {--mPtr; return *this;}
    iterator operator++(int) {iterator prev(mPtr); ++mPtr; return prev;}
    iterator operator--(int) {iterator prev(mPtr); --mPtr; return prev;}
    reference operator*() const {return (reference) *mPtr;}
    pointer operator->() {return &(operator*());}
    pointer operator->() const {return &(operator*());}
    iterator operator+=(int k) {mPtr += k; return *this;}
    iterator operator-=(int k) {mPtr -= k; return *this;}

    operator const_iterator() const {return const_iterator(mPtr);}
//    operator const UtVector<_Obj>::const_iterator() const {
//      return UtVector<_Obj>::const_iterator(mPtr);
//    }

    UtPtrArray::iterator mPtr;
  };

  //! get first element in iteration
  iterator begin() {return iterator(mArray.begin());}

  //! get last element in iteration
  iterator end() {return iterator(mArray.end());}

  //! get first element in iteration
  const_iterator begin() const {return const_iterator(mArray.begin());}

  //! get last element in iteration
  const_iterator end() const {return const_iterator(mArray.end());}

  //! reverse_iterator class
  class reverse_iterator: public RANDOM_ACCESS_ITERATOR(UtArray) {
  public: CARBONMEM_OVERRIDES
    typedef _Obj value_type;
    typedef _Obj& reference;
    typedef ptrdiff_t difference_type;
    typedef _Obj* pointer;

    reverse_iterator() {}
    reverse_iterator(const UtPtrArray::reverse_iterator& ptr): mPtr(ptr) {}
    ~reverse_iterator() {}
    bool operator==(const reverse_iterator& iter) const {return iter.mPtr == mPtr;}
    bool operator!=(const reverse_iterator& iter) const {return iter.mPtr != mPtr;}
    bool operator<(const reverse_iterator& iter) const {return mPtr < iter.mPtr;}
    bool operator<=(const reverse_iterator& iter) const {return mPtr <= iter.mPtr;}
    bool operator>(const reverse_iterator& iter) const {return mPtr > iter.mPtr;}
    bool operator>=(const reverse_iterator& iter) const {return mPtr >= iter.mPtr;}
    reverse_iterator& operator++() {++mPtr; return *this;}
    reverse_iterator& operator--() {--mPtr; return *this;}
    reverse_iterator operator++(int) {
      reverse_iterator prev(*this);
      ++mPtr;
      return prev;
    }
    reverse_iterator operator--(int) {
      reverse_iterator prev(*this);
      --mPtr;
      return prev;
    }
    SIntPtr operator-(const reverse_iterator& i) const {return mPtr - i.mPtr;}
    reverse_iterator operator-(int k) const {
      return reverse_iterator(mPtr - k);
    }
    reverse_iterator operator+(int k) const {
      return reverse_iterator(mPtr + k);
    }

    reference operator*() const {return (reference) *mPtr;}
    pointer operator->() {return &(operator*());}
    pointer operator->() const {return &(operator*());}
    reverse_iterator operator+=(int k) {mPtr += k; return *this;}
    reverse_iterator operator-=(int k) {mPtr -= k; return *this;}

    UtPtrArray::reverse_iterator mPtr;
  };

  //! get first element in iteration
  reverse_iterator rbegin() {return reverse_iterator(mArray.rbegin());}

  //! get last element in iteration
  reverse_iterator rend() {return reverse_iterator(mArray.rend());}

  //! const_reverse_iterator class
  class const_reverse_iterator: public RANDOM_ACCESS_ITERATOR(const UtArray) {
  public: CARBONMEM_OVERRIDES
    typedef const _Obj value_type;
    typedef const _Obj& reference;
    typedef ptrdiff_t difference_type;
    typedef const _Obj* pointer;

    const_reverse_iterator() {}
    const_reverse_iterator(const UtPtrArray::const_reverse_iterator& ptr): mPtr(ptr) {}
    ~const_reverse_iterator() {}
    bool operator==(const const_reverse_iterator& iter) const {
      return iter.mPtr == mPtr;
    }
    bool operator!=(const const_reverse_iterator& iter) const {
      return iter.mPtr != mPtr;
    }
    bool operator<(const const_reverse_iterator& iter) const {return mPtr < iter.mPtr;}
    bool operator<=(const const_reverse_iterator& iter) const {return mPtr <= iter.mPtr;}
    bool operator>(const const_reverse_iterator& iter) const {return mPtr > iter.mPtr;}
    bool operator>=(const const_reverse_iterator& iter) const {return mPtr >= iter.mPtr;}
    const_reverse_iterator& operator++() {++mPtr; return *this;}
    const_reverse_iterator& operator--() {--mPtr; return *this;}
    const_reverse_iterator operator++(int) {
      const_reverse_iterator prev(*this);
      ++mPtr;
      return prev;
    }
    const_reverse_iterator operator--(int) {
      const_reverse_iterator prev(*this);
      --mPtr;
      return prev;
    }
    SIntPtr operator-(const const_reverse_iterator& i) const {return mPtr - i.mPtr;}
    const_reverse_iterator operator-(int k) const {
      return const_reverse_iterator(mPtr - k);
    }
    const_reverse_iterator operator+(int k) const {
      return const_reverse_iterator(mPtr + k);
    }
    reference operator*() const {return (reference) *mPtr;}
    pointer operator->() {return &(operator*());}
    pointer operator->() const {return &(operator*());}
    const_reverse_iterator operator+=(int k) {mPtr += k; return *this;}
    const_reverse_iterator operator-=(int k) {mPtr -= k; return *this;}

  private:
    UtPtrArray::const_reverse_iterator mPtr;
  };

  //! get first element in iteration
  const_reverse_iterator rbegin() const {
    return const_reverse_iterator(mArray.rbegin());
  }

  //! get last element in iteration
  const_reverse_iterator rend() const {
    return const_reverse_iterator(mArray.rend());
  }

  //! Erase the specified element (order N)
  /*!
    \param iter Thing to erase. Must not be a reference in order for
    iterator expressions to work (e.g., p + 1)
  */
  void erase(iterator iter) {mArray.erase(iter.mPtr);}
  
  //! Erase the range of elements specified by the iterators (order N)
  /*!
    \param beg First element to erase. Must not be a reference in
    order for iterator expressions to work (e.g, p + 1)
    \param end Point at which to stop. This must not be a reference in
    order for iterator expressions to work (e.g., p + 3).
  */
  void erase(iterator beg, iterator end) {mArray.erase(beg.mPtr, end.mPtr);}

  //! Insert from an iterator, into a position identified by an interator
  template<class _Iter> void insert(
    iterator insertion_iter,
    _Iter srcBegin,
    const _Iter& srcEnd)
  {
    insert(insertion_iter - begin(), srcBegin, srcEnd);
  }

  //! Insert from an iterator, into a position identified by an index
  template<class _Iter> void insert(
    size_t pos,
    _Iter srcBegin,
    const _Iter& srcEnd)
  {
    mArray.insert(pos, srcBegin, srcEnd);
  }

  //! Clear contents, deleting each as an object, then clear array
  void clearPointers() {
    for (UInt32 i = 0, n = size(); i < n; ++i) {
      _Obj obj = (*this)[i];
      delete obj;
    }
    clear();
  }

private:
  UtPtrArray mArray;
};

/*! \class UtNormalizedArray
 * 
 * Provide normalized access, using the normalization available via
 * ConstantRange.  The ConstantRange must be initialized exactly once
 * before use.
 */
template <typename _Obj> class UtNormalizedArray
 {
 public:
   CARBONMEM_OVERRIDES;
   UtNormalizedArray() : mRange( NULL ) {}
   UtNormalizedArray( const ConstantRange *range ) : mRange( range ) {
     init();
   }
   ~UtNormalizedArray() {
     delete [] mArrayPtr;
   }
   void putRange( const ConstantRange *range )
   {
     INFO_ASSERT( mRange == NULL, "Cannot re-set the size of a UtNormalizedArray" );
     mRange = range;
     init();
   }
   const ConstantRange *getRange() const { return mRange; }
   //! index into the array, normalizing the supplied index
   _Obj& operator[](SInt32 index) {
     INFO_ASSERT( mRange != NULL, "Range not set in UtNormalizedArray" );
     ConstantRange idxRange( index, index );
     idxRange.normalize( mRange, true ); // bounds checking done here
     SInt32 normIdx = idxRange.getMsb();
     return mArrayPtr[normIdx];
   }
 private:
   UtNormalizedArray( const UtNormalizedArray<_Obj> &src );
   UtNormalizedArray<_Obj>& operator=( const UtNormalizedArray<_Obj> &src );
   //! A common initialization routine
   void init() {
     mArrayPtr = new _Obj[mRange->getLength()];
     memset( mArrayPtr, 0, mRange->getLength() * sizeof( _Obj ));
   }
   const ConstantRange *mRange;
   _Obj *mArrayPtr;
};

#endif // __UtArray_h_
