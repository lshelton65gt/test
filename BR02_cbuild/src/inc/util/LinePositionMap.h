// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.
*/

#ifndef _LINE_POSITION_MAP_H_
#define _LINE_POSITION_MAP_H_

#include "util/c_memmanager.h"
#include "util/CarbonTypes.h"
#include "util/Util.h"

//! map line numbers to seek indexes in a file
class LinePositionMap {
  typedef UtHashMap<UInt32,UInt64> PosMap;
  typedef UtHashMap<UtString,PosMap> FilePosMap;

public:
  CARBONMEM_OVERRIDES

  //! ctor
  LinePositionMap();

  //! dtor
  ~LinePositionMap();

  //! find a position for seeking, given a filename and line number
  /*!
   *! You may optionally pass in an input stream if the file is
   *! already open, thought the lookup will first be done on the
   *! filename anyway.  Returns true if successful, putting the
   *! value in *position.  If failed, puts error message in *errmsg.
   */
  bool getPosition(const char* filename, UInt32 line_number,
                   UInt64* position, UtString* errmsg,
                   UtIStream* file = NULL);

private:
  CARBON_FORBID_DEFAULT_CTORS(LinePositionMap);

  FilePosMap* mFilePosMap;
};

#endif
