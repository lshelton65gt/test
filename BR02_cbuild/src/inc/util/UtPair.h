// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtPair_h_
#define __UtPair_h_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#include <utility>

//! alternate STL pair definition that uses the Carbon memory allocator
template<class _First, class _Second>
class UtPair: public std::pair<_First, _Second>
{
public: CARBONMEM_OVERRIDES
  typedef std::pair<_First, _Second> _Base;
  UtPair() {}
  UtPair(_First first, _Second second): _Base(first, second) {}
};

#endif
