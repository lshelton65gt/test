// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __Util_h_
#define __Util_h_

#ifndef __CarbonPlatform_h_
#include "util/CarbonPlatform.h"
#endif

#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif

#include <functional>

// Forward declarations for util classes.

class UtOStreamFile;
class UtOStreamStr;
template<class T> class UtOStreamT;

//namespace std {template<class _K> class less;};

class UtOStream;
class UtOStringStream;
class UtIStream;
class UtIStringStream;

#if pfSPARCWORKS
#define CL class
#else
#define CL
#endif

// STL wrapper classes
template<class Obj> class UtVector;
template<class Obj> class UtArray;
template<class Obj> class UtList;
template<class Obj> class UtDeque;
template<class _Tp, class Sequence = UtDeque<_Tp> > class UtStack;
#if pfTEMPLATE_FORWARD_ARGS
template<class _K, class _Cmp = std::less<CL _K> > class UtSet;
template<class _K, class _V, class _Cmp = std::less<CL _K> > class UtMultiMap;
#endif

template<class _K, class _Cmp = std::less<CL _K> > class UtMultiSet;
template<class _key, class _V, class _Cmp = std::less<CL _key> > class UtMap;


// Helper classes for UtHashSets & UtHashMaps
template<class T, int shift = 0> struct HashPointer;
template<class T> struct HashPointerValue;
template<class T> struct HashValue;
template<class T> struct HashOpaque32;

template<typename _KeyType> struct HashOpaque32;

template<typename _Type> struct HashObjectMgr;
template<typename _Type> struct HashPointerMgr;
template<typename _Type> struct HashOpaque32Mgr;

// Use template specialization to get useful defaults for UtHashMap<> and
// UtHashSet<> parameters.

template<class T> class HashHelper;
template<class T> class HashMgr;
template<typename _KeyType, typename _ValueType,
         typename _HashType = HashHelper<_KeyType>,
         typename _HashKeyMgr = HashMgr<_KeyType>,
         typename _HashValMgr = HashMgr<_ValueType> >
class UtHashMap;

template<typename _KeyType, typename _HashType> struct UtHashSmallWrapper;
template<typename _KeyType, typename _HashType> struct UtHashBigWrapper;

template<typename _KeyType,
         typename _HashType = HashHelper<_KeyType>,
         typename _HashKeyMgr = HashMgr<_KeyType>,
         typename _HashWrapper = UtHashSmallWrapper<_KeyType,_HashType> >
class UtHashSet;

template<class _MapType, class _Iterator = typename _MapType::iterator> class LoopMap;
template<class _MapType, class _Iterator = typename _MapType::const_iterator> class CLoopMap;
template <class _ValueType> class Iter;

// Memory pool
template<class T, int _chunkSize = 4096> class MemoryPool;
typedef UtHashSet<void*> MemoryPoolPtrSet;

// Loop
template <typename _ContainerType> class Loop;
// CLoop
template <typename _ContainerType> class CLoop;
// RLoop
template <typename _ContainerType> class RLoop;
// CRLoop
template <typename _ContainerType> class CRLoop;

// Loop pair
template <class _LoopType1, 
          class _LoopType2, 
          class _ValueType = typename _LoopType1::value_type, 
          class _ReferenceType = typename _LoopType1::reference> class LoopPair;

// Set closure
template<typename Object,
         class Container,
         class ObjectHashType = HashPointer<Object,2> > class SetClosure;

// Union find
template<typename T,
         class EqSet = UtSet<T>,
         class EqPartition = UtMap<T, EqSet> > class UnionFind;

/*!
  This is a Duff's Device memory copy. You can use this to replace
  memcpy.

  The standard one has the same parameters as memcpy, but I found
  that if I pass begin and end instead of a length Duff's device
  performs 2-3% better. Therefore, if you need to replace memcpy with
  this, create a another function which simply uses this
  filler. Replay needs this to be as fast as possible.

  Another standard aspect of the device is that len should never be
  0. I tried that and made the only 2 places in the callers of this
  code only call this if the length to copy is > 0. The code was
  beautiful but slower by 1 to 2%. I.e,
  switch (count % 8)
  case 0: do { *to++ = *from++
  ...
  case 1: *to++ = *from++
  } while ((count -= 8) > 0);

  Another variation Stroustrup gives creates a counter and
  subtracts it as it runs through the do-while loop. Again, it was
  still slower than this implementation (1-2%).

  Just to be clear, the difference is not additive. So, we wouldn't
  see a 4% drop if I did both Stroustrup's version and memcpy
  parameters. It still would be around 2%.

  Also, this is compiler-dependent. Gcc eats Duff's device and just
  screams past memcpy. This may not be true with MS Visual C++ or
  any other compiler for that matter.

  I tried returning the modified obj buffer to eliminate the
  need to move the mBuffer forward after doing the copy. Slower.

  I tried the same by macro-izing this. Slower.

  - Mark Seneski
*/
template <typename T> inline void carbon_duffcpy(T* begin, const T* end, const T* obj)
{
  switch ((end - begin) & 7)
  {
  case 0: 
    while (begin != end)
    {
      *begin++ = *obj++;
    case 7: *begin++ = *obj++;
    case 6: *begin++ = *obj++;
    case 5: *begin++ = *obj++;
    case 4: *begin++ = *obj++;
    case 3: *begin++ = *obj++;
    case 2: *begin++ = *obj++;
    case 1: *begin++ = *obj++;
    }
  }   
}

/*!
  \brief memory comparison using Duff's device

  This is not functionally equivalent to memcmp, and not just because
  of the start/end arguments described above in carbon_duffcpy().
  This will not return whether a memory region is greater/less than
  another, only whether they're different.

  The tradeoff is that we may do a few extra comparisons after we
  discover that the regions don't match.  On comparisons in which we
  expect a match, this won't matter.

  \return 0 if the regions match
  \return non-zero if the regions don't match
 */

template <typename T> inline UInt32 carbon_duffcmp(T* begin, const T* end, const T* obj)
{
  UInt32 diffs = 0;
  switch ((end - begin) & 7)
  {
  case 0: 
    while ((diffs == 0) & (begin != end))
    {
      diffs += (*begin++ != *obj++);
    case 7: diffs += (*begin++ != *obj++);
    case 6: diffs += (*begin++ != *obj++);
    case 5: diffs += (*begin++ != *obj++);
    case 4: diffs += (*begin++ != *obj++);
    case 3: diffs += (*begin++ != *obj++);
    case 2: diffs += (*begin++ != *obj++);
    case 1: diffs += (*begin++ != *obj++);
    }
  }   
  return diffs;
}

//! memset using Duff's device
/*!
  \sa carbon_duffcpy
 */
template <typename T> inline void carbon_duffset(T* begin, const T* end, const T& obj)
{
  switch ((end - begin) & 7)
  {
  case 0: 
    while (begin != end)
    {
      *begin++ = obj;
    case 7: *begin++ = obj;
    case 6: *begin++ = obj;
    case 5: *begin++ = obj;
    case 4: *begin++ = obj;
    case 3: *begin++ = obj;
    case 2: *begin++ = obj;
    case 1: *begin++ = obj;
    }
  }   
}

class StringAtom;
class UtString;
class StrToken;
class AtomicCache;
class ArgProc;
class ZistreamDB;
class ZostreamDB;
class ZistreamZip;

//! AtomArray is used for representing paths in hierarchical references
typedef UtArray<StringAtom*> AtomArray;

//! Set of StringAtoms
typedef UtHashSet<StringAtom*> AtomSet;

//! HashSet of SInt32s
typedef UtHashSet<SInt32> SInt32HashSet;

//! HashSet of UInt32s
typedef UtHashSet<UInt32> UInt32HashSet;

//! HashMap of SInt32s to UInt32s
typedef UtHashMap<SInt32, UInt32> SInt32UInt32Map;

#undef CL

#endif
