// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Smart wrapper around __gnu_cxx::hash_map that improves iteration access time
// NOTE : not all funcions of __gnu_cxx::hash_map are implemented yet
// Basic idea:
// Store the (key, value) pairs in a vector, instead of a hash.  The
// hash will store (key, index) pairs, where index is the index into the
// vector where the real data is.
// find/insert operate on the hash, while begin() and end() operate on the vector

#ifndef _CARBON_HASH_MAP_H_
#define _CARBON_HASH_MAP_H_

#include <ext/hash_map>
#include <vector>
#include <algorithm> // for std::pair

template <class _Key, class _Value,
          class _Hash = __gnu_cxx::hash<_Key>,
          class _Eq = std::equal_to<_Key> >
class CarbonHashMap
{
public:
  CarbonHashMap() { }
  ~CarbonHashMap() { }
  
  typedef std::pair<_Key, _Value> value_type;
  typedef typename std::vector<value_type>::iterator iterator;
  typedef typename std::vector<value_type>::const_iterator const_iterator;
  
  iterator       begin()       {return mVector.begin();}
  iterator       end()         {return mVector.end();  }
  const_iterator begin() const {return mVector.begin();}
  const_iterator end()   const {return mVector.end();  }
  
  iterator find(const _Key k)
  {
    hash_iter_type iter = mHash.find(k);
    if (iter == mHash.end())
      return mVector.end();
    return (mVector.begin() + iter->second);
  }
  
  const_iterator find(const _Key k) const
  {
    hash_iter_type iter = mHash.find(k);
    if (iter == mHash.end())
      return mVector.end();
    return (mVector.begin() + iter->second);
  }
  
  std::pair<iterator, bool> insert(value_type val)
  {
    size_t index = mVector.size();
    hash_pair_type h_p(val.first, index);
    mHash.insert(h_p);
    
    mVector.push_back(val);
    
    std::pair<iterator, bool> ret_iter(mVector.begin() + index, true);
    return ret_iter;
  }
  
  void clear()
  {
    mHash.clear();
    mVector.clear();
  }
  
  
  _Value& operator[](const _Key& key) {
    iterator p = find(key);
    if (p != end()) {
      return p->second;
    }
    
    // We are extending the hash table with a new key.  Since we don't
    // know what value we want, we have to use the empty-arg constructor
    // to get a "blank" one.  So this method is innefficient to use
    // to insert something where the _Value is non-trival.
    value_type val(key, _Value());
    std::pair<iterator, bool> ib = insert(val);
    iterator& rp = ib.first;
    return rp->second;
  }
  int size() const {return mVector.size();}
  bool empty() const {return mVector.empty();}
  
  void erase(iterator vect_iter) {
    // ensure legitimate iterator into the vector
    iterator first = begin();
    iterator last = end() - 1;
    assert(vect_iter <= last);  // this_assert_OK
    
    
    // fix up the hash-table which maps keys to indices
    _Key& key_to_remove = vect_iter->first;
    int erased = mHash.erase(key_to_remove);
    assert(erased);  // this_assert_OK

    // fix up the vector my moving the last element over the
    // iterator so it stays packed.  This scrambles the order, but
    // hash-tables can never guarantee iteration order.
    if (last != vect_iter) {
      *vect_iter = *last;
      _Key& moved_key = vect_iter->first;
      mHash[moved_key] = vect_iter - first;
    }

    // finally, shrink the vector
    mVector.resize(last - first);
  } // void erase

  size_t erase(const _Key& key) {
    iterator vect_iter = find(key);
    if (vect_iter == end()) {
      return 0;
    }
    erase(vect_iter);
    return 1;
  }
  
private:
  typedef typename __gnu_cxx::hash_map<_Key, size_t,
                                       _Hash, _Eq> _GnuHashMap;
  typedef typename _GnuHashMap::iterator hash_iter_type;
  typedef std::pair<_Key, size_t> hash_pair_type;
  
  _GnuHashMap mHash;
  std::vector<value_type> mVector;
}; // class CarbonHashMap

#endif 
