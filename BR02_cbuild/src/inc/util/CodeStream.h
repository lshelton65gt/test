// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!  
 *
 *  \file CodeStream.h Annotated streams for output C++ code with HDL source and
 *  implementation location annotations. Codegen uses CodeStream rather than
 *  UtOStream for writing C++ code. CodeStream provides:
 *
 *  \li an indentation engine to help improve the readability of the generated C++.
 *
 *  \li tracking of output line and column number for annotations.
 *
 *  \li annotations so that HDL source locations and codegen implementation
 *  locations may decorate the generated code.
 *
 *  \li an annotation database that is written to libdesign.annotations so that
 *  the mapping from HDL source and codegen implementation to generated C++ code
 *  may be applied to encrypted files and generated files in which annotation
 *  visibility was suppressed.
 *
 *  In unencrypted generated code, an implementation annotation is a comment
 *  containing a string of the form:
 *
 *     "*%I emitAssign.cxx:767"
 *
 *  and an HDL source annotation comment contains a string of the form:
 *
 *     "%H impl-annot-02.v:11"
 *
 *  The annotations are intended to simplify debugging of codegen defects.
 *
 *  \note It would probably make more sense for this to live in src/codegen
 *  rather than src/util. However, I could not get a successfull link when
 *  subclassing CodeStream from UtOStream with the code living in codegen.
 *
 */

#ifndef _CodeStream_h_
#define _CodeStream_h_

#include "util/UtIOStream.h"
#include "util/UtHashMap.h"
#include "util/UtList.h"
#include "util/SourceLocator.h"

class CodeAnnotationManager;            // forward declaration
class CodeStream;                       // forward declaration

class ArgProc;                          // external declaration
class DynBitVector;                     // external declaration

//! \class CodeAnnotation

/*! Represents a single code annotation. Instances of CodeAnnotation are always
 *  allocated on the stack, indeed the intended use is as an instantiation within
 *  a stream output statement:
 *
 *  out << "foo " << CodeAnnotation (cIMPL, __FILE__, __LINE__) << "bar";
 *
 *  or, more concisely using the TAG macro:
 *
 *  out << "foo " << TAG << " bar";
 *
 *  Each annotation instance is assocation with a CodeAnnotationManager
 *  instance in which longer-lived configuration information is stored, along
 *  with a handle onto the persistent annotation store. The 
 */

#define TAG CodeAnnotation (CodeAnnotation::cIMPL, __FILE__, __LINE__)
#define ENDL TAG << "\n"

#define HDLTAG(_usedefnode_) CodeAnnotation (CodeAnnotation::cHDL_SOURCE, \
    (_usedefnode_)->getLoc ().getFile (), (_usedefnode_)->getLoc ().getLine ())

class CodeAnnotation {
public: CARBONMEM_OVERRIDES

  enum Flags {
    cHDL_SOURCE = 1<<0,                 //!< set iff it is an HDL source annotation
    cIMPL = 1<<1,                       //!< set iff it is an implementation annotation
    cVISIBLE = 1<<2,                    //!< iff the annotation is visible in the generated code
    cNEWLINE = 1<<3,                    //!< write a new line after each visible tag
    cLEADSLINE = 1<<4,                  //!< write a new
    cCOMPRESS_STORE = 1<<5,             //!< compress the store for these annotations
    cNO_STORE = 1<<6,                   //!< do not create an annotation store
    cDEFAULT_HDL = cHDL_SOURCE|cNEWLINE|cLEADSLINE,
    cDEFAULT_IMPL = cIMPL
  };

  //! Create a code annotation and bind the domain
  CodeAnnotation (UInt32 flags, const char *file, const int line) :
    mManager (NULL), mFlags (flags), mRange (), mDomain (file, line) {}
  
  ~CodeAnnotation () {}

  //! \struct Domain
  /*! The domain of an annotation is either an HDL source line or an
   *  implementation location. 
   */

  struct Domain {
    Domain () : mFile (0), mLine (0) {}
    Domain (const char *file, const UInt32 line) : mFile (file), mLine (line) {}
    const char *mFile;
    UInt32 mLine;
  };

  //!\ struct Range
  /*! The range of an annotation is a location in the generated C++ code. There
   *  may be many annotations on a single line of generated C++ code so the
   *  Range also includes a column number.
   */

  struct Range {
    Range () : mFile (0), mLine (0), mColumn (0) {}
    Range (const char *file, const UInt32 line, const UInt16 column) :
      mFile (file), mLine (line), mColumn (column) {}
    const char *mFile;
    UInt32 mLine;
    UInt16 mColumn;
    Range &operator = (const Range &x)
    { mFile = x.mFile; mLine = x.mLine; mColumn = x.mColumn; return *this; }
  };

  friend class CodeAnnotationManager;

  //! \return iff a set of bits are set in the flags
  bool test (const UInt8 x) const { return (mFlags & x) == x; }

  //! \return iff the annotation is visible in the generated C++ code
  bool isVisible () const { return mFlags & cVISIBLE; }

  //! Write the annotation as a comment to a stream
  void write (UtOStream &) const;

  //! \return the domain of the annotation
  const Domain &getDomain () const { return mDomain; }

  //! \return the range of the annotation
  const Range &getRange () const { return mRange; }

  //! Set the range
  /*! \note This is a const method, even though it is modifying a mutable field
   *  because it is ultimately called from one of the << operators. All sorts
   *  of grief happens at compile time is the arguments to << are not
   *  const. The easiest solution seems to be just abusing const with mutable.
   */
  void set (const Range range) const { mRange = range; }

private:

  // These fields are modified by the CodeAnnotation::bind method, which is
  // called by CodeStream::write. However, the CodeAnnotation argument of the
  // UtOStream::operator<<(const CodeAnnotation &) method needs to be a const
  // or else g++ whines about being unable to resolve and operator. The easiest
  // way to overcome this is to just make the fields mutable.
  mutable CodeAnnotationManager *mManager; //!< context of this annotation
  mutable UInt32 mFlags;                //!< bitmask formed from enum Flags
  mutable Range mRange;                 //!< a location in the generated C++

  Domain mDomain;                       //!< an HDL source location or an implementation location

};

//! \class CodeAnnotationStore
/*! The code annotation store is persistent store of a set of
 *  annotations. During execution annotation stores are written for the set of
 *  implementation code annotations and the set of HDL source code
 *  annotations. The store is used to diagnose codegen faults when the
 *  unencrypted generated model is not available.
 */

/*! \note This is very much still under construction. Currently, the
 *  annotations are just written to a single text file in a
 *  libdesign.foo-annotations directory. This class is not completed until the
 *  store is compressed and methods for reading and searching the store have
 *  been written.
 */

class CodeAnnotationStore { 
public: CARBONMEM_OVERRIDES

  enum Flags {
    cREADING = 1<<0,                    //!< store is open for reading
    cWRITING = 1<<1,                    //!< store is open for writing
    cCOMPRESSED = 1<<2,                 //!< write compressed database files
    cOPEN = 1<<3,                       //!< set when the store is open
    cNOISY = 1<<4                       //!< log the interning of strings into the file name table
  };

  class ErrorSink;                      // forward declaration

  //! ctor for CodeAnnotationStore
  /*! \param flags Bitmask formed from enum Flags
   *  \param error_sink Object for writing error messages from Lexer
   *  \note error_sink must be specified when opening for reading but
   *  should be NULL when writing
   */
  CodeAnnotationStore (const UInt32 flags, ErrorSink *error_sink);
  CodeAnnotationStore (const UInt32 flags, const char *name);

  virtual ~CodeAnnotationStore ();

  //! Open the persistent store
  /*! If writing, then this method creates the libdesign.name-annotations
   *  directory and creates the files used to store the annotations. If
   *  reading, it accesses the directory and opens the files for reading. If
   *  any files cannot be created or read, and error message is written and the
   *  method returns false.
   */
  bool open (const char *file_root, const char *name, const UInt32 file_map_options, UtString *errmsg);

  //! Close the store
  void close ();

  //! Write an annotation to the store
  void write (const CodeAnnotation &);

  //! Set bits in the options mask
  void set (const UInt32 flags) { mFlags |= flags; }

  //! \struct ErrorSink
  /*! When reading configuration files there is a need to output syntax error
   *  messages. These really should go via some MsgContext instance. However, a
   *  different message context is used in the standalone read and in cbuild,
   *  both of which link with this module. Furthermore, it appears that writing
   *  messages via a MsgContext from util is a bad idea.
   *
   *  This problem is sidestepped by passing an ErrorSink object to the
   *  CodeAnnotationStore constructor when reading. This class defines nothing
   *  but an operator that consumes syntax error messages. The annotation store
   *  reader specialised ErrorSink to write to a MsgContext deried from
   *  AnnotationStore.msg.
   */
  class ErrorSink {
  public:
    virtual ~ErrorSink () {}
    virtual void operator () (const char *, const UInt32, const char *) = 0;
  };

  //! \return the name of the store
  UtString getName () const { return mName; }

  struct DatabaseEntry {
    DatabaseEntry () : 
      mDomainFileIndex (0), mDomainLineno (0),
      mRangeFileIndex (0), mRangeLineno (0), mRangeColumn () 
    {}
    UInt32 mDomainFileIndex;
    UInt32 mDomainLineno;
    UInt32 mRangeFileIndex;
    UInt32 mRangeLineno;
    UInt32 mRangeColumn;
    void print (UtOStream &) const;
    void pr () const;
  };

  //! Read the next mapping from the database
  bool read (DatabaseEntry *);

  //! Look up a file in the file map
  bool findFileIndex (const UtString &filename, UInt32 *index) const;

  //! Build a map from file index to file name
  void reverseFileMap (UtHashMap <UInt32, UtString> *reversed) const
  { mFileMap.reverse (reversed); }

  // Diagnostics
  void print (UtOStream &) const;       //! print the configuration to s
  void pr () const;                     //! print the configuration to stdout

  //! Read and dump the DB file contents
  void dumpDB (UtOStream &);

private:

  UtString mName;
  ErrorSink *mErrorSink;                //!< scanner error sink when reading
  UInt32 mFlags;                        //!< options for this instance

  // Two files are written to the store directory. The annotations.cfg file
  // containing the configuration and the filename table and annotations.db
  // containing the annotation map.
  UtOStream *mDbStream;                 //!< output stream for annotations.db
  UtOStream *mCfgStream;                //!< output stream for annotations.cfg

  class Lexer;                          //!< forward decl of the scanner

  // When reading annotations.cfg and annotations.db, a Lexer is instantiation
  // that tokenises the input streams.
  Lexer *mDbInput;                      //!< tokenised input stream for annotations.db
  Lexer *mCfgInput;                     //!< tokenised input stream for annotations.cfg

  // Rather than writing a filename with each entry, CodeAnnotationStore
  // instances intern filenames an map to a single integer. This integer
  // representing the file is written to the store.

  //! \return a unique integer for a filename
  /*! \note ./foo and foo are considered the same file by intern.
   */
  UInt32 intern (const char *filename) { return mFileMap.intern (filename); }

  //! \class FileMap
  /*! When writing to annotations.db, the filename part of the annotation is
   *  replaced by a single integer that denotes the filename. There really is
   *  not much point writing the same filename thousands of times when it can
   *  be encoded as a small integer! FileMap implements a mapping from
   *  filenames to integers, and provides methods for reading and writing the
   *  map.
   */
  class FileMap : public UtHashMap <UtString, UInt32> {
  public:

    FileMap () : UtHashMap <UtString, UInt32> (), mFlags (0), mNextValue (0) {}
    
    ~FileMap ();

    //! Write the mapping to a stream
    bool write (UtOStream &) const;

    //! Read the mapping from a stream
    bool read (Lexer &, UtString *errmsg);

    //! Set option bits
    void set (const UInt32 mask) { mFlags |= mask; }

    //! Construct an inverted map
    /*! This maps filenames to integers. However, there is also a need for the
     *  reverse map when output entries from the database.  This method builds
     *  that reverse map.
     */
    void reverse (UtHashMap <UInt32, UtString> *) const;

    //! \return the integer index for a file
    UInt32 intern (const char *filename);

    // diagnostics
    void print (UtOStream &) const;     // print the map to a stream
    void pr () const;                   // print the map to stdout

  private:

    UInt32 mFlags;                      //!< options for the file map
    UInt32 mNextValue;                  //!< next value to allocation

  };

  FileMap mFileMap;                     //!< the mapping from filename to integer

  //! Open an output file in the annotation store directory
  /*! \return iff the output file was opened successfully
   *  \param dirname Name of the annotation directory (something like libdesign.foo-annotation)
   *  \param filename Name of the file to create (probably annotations.foo)
   *  \param stream The output stream for writing to the file
   *  \param compressible If set and if mFlags&cCOMPRESSED then a compressed file is created
   *  \param errmsg If opening failed and the method returns false, this is an error message
   */
  bool openOutputFile (UtString &dirname, const char *filename, UtOStream **stream,
    bool compressible, UtString *errmsg);

  //! Open an input file in the annotation store directory
  /*! \return iff the input file was opened successfully
   *  \param dirname Name of the annotation directory (something like libdesign.foo-annotation)
   *  \param filename Name of the file to open (probably annotations.foo)
   *  \param lexer The tokenised input stream for reading the file.
   *  \param compressed Set if the input file is compressed.
   *  \param lexer_options contact michael for more info
   *  \param errmsg If opening failed and the method returns false, this is an error message
   */
  bool openInputFile (UtString &dirname, const char *filename, Lexer **lexer,
    bool compressed, const UInt32 lexer_options, UtString *errmsg);

  //! Write the configuration files to mCfgStream
  bool writeCfg () const;
 
  //! Read the configuration files from a mCfgInput
  bool readCfg (UtString *errmsg);

  bool writeFlags (UtOStream &, const UInt32 flags) const;
  bool readFlags (Lexer &, UInt32 *flags, UtString *errmsg) const;

  //! \class Lexer
  /*! The Lexer class tokenised the input from configuration files.
   *  \note I did consider using a flex scanner here. However, it's a pain to
   *  get flex to read from streams (which is needed for compressed file). This
   *  scanner is pretty simple so hand rolling is not a big deal.
   */
  class Lexer {
  public: CARBONMEM_OVERRIDES

    enum Tokens {
      // keywords
      cCOMPRESSED,                      //!< %compressed keyword
      cEND,                             //!< %end keyword
      cFILES,                           //!< %files keyword
      cSTORE_NAME,                      //!< %store_name keyword
      cOPTIONS,                         //!< %options keyword
      // Newlines are returned as tokens when the cNEWLINE_TOKEN option bit is
      // set. Otherwise they are treated as whitespace. The cNEWLINE_TOKEN can
      // be set permanently in the constructor, or temporarily with pushFlags
      // (Lexer::cNEWLINE_TOKEN).
      cNEWLINE,                         //!< newline character
      // tokens with an associated value
      cSTRING,                          //!< a string, use the getString method to access the text
      cINTEGER,                         //!< an integer, use the getInteger method to access the value
      cEOF
    };

    //! Lexer option bits
    enum Options {
      cNOISY = 1<<0,                    //!< debug output whenever the scanner makes a new token
      cNEWLINE_TOKEN = 1<<1             //!< return cNEWLINE for \n rather than treating as whitespace
    };

    //! ctor for Lexer
    /* \param filename The filename of the input file; used only for error messages
     * \param input The input stream; \note this is deleted in ~Lexer
     * \param flags Lexer options formed as a bitmap over enum Options
     * \param error_sink Sink for sucking up lexer error messages
     */
    Lexer (const char *filename, UtIStream *input, const UInt32 flags, ErrorSink *error_sink);

    virtual ~Lexer ();

    //! Scan the next token in the input stream
    void nextToken ();
    
    //! \return the last token scanner
    Tokens currentToken () const { return mLastToken; }

    //! \return a string naming the last token scanned
    const char *currentTokenName () const { return image (mLastToken); }

    //! \return iff the last token scanned matches the argument
    bool lookingAt (const Tokens token) const { return mLastToken == token; }

    /*! \return iff the last token scanned matches the argument 
     *  \note If the token matches then nextToken is called to scan the next
     *  token in the stream. If the token does not match then an error message
     *  is written to the error sink.
     */
    bool expected (const Tokens token);

    /*! \return iff the last token scanned matches was cINTEGER
     *
     *  \note If the token was cINTEGER then nextToken is called to scan the
     *  next token, and value is set to the integer value.  If the token does
     *  not match then an error message is written to the error sink.
     */
    bool expected (UInt32 *value);

    //! Report an error to the error sink
    /*! \note filename: lineno is prepended to the error message */
    void error (const char *fmt, ...);

    //! \return If the last token scanned was cSTRING then return the string value
    /*! \note The value of the string is overwritten by the next call to
     *  nextToken. The onus is on the caller to make a copy of the string
     *  value.
     */
    const char *getString () const { return mLval.mString; }

    //! \return If the last token scanned was cINTEGER then return the integer value
    UInt32 getInteger () const { return mLval.mInteger; }

    //! \return a human readable name for the token
    static const char *image (const Tokens token);

    //! Set option bits in the scanner, return the original options
    /*! \note This is intended for temporarily setting options. For example,
     *  setting cNEWLINE_TOKEN:
     *
     *  UInt32 saved_lexer_flags;
     *  lexer->pushFlags (Lexer::cNEWLINE_TOKEN, &saved_lexer_flags);
     *  ... // processing that expected cNEWLINE tokens
     *  lexer->popFlags (saved_lexer_flags);
     */
    void pushFlags (const UInt32 flags, UInt32 *saved) { *saved = mFlags; mFlags |= flags; }

    //! Restore flags changed by pushFlags
    void popFlags (const UInt32 saved) { mFlags = saved; }

  private:

    UInt32 mFlags;                      //!< options for the lexer
    ErrorSink *mErrorSink;              //!< error message sink
    const char *mFilename;              //!< filename for error messages
    UtIStream *mInput;                  //!< input stream for the lexer
    UInt32 mLineno;                     //!< current line number in the input stream
    Tokens mLastToken;                  //!< last token scanned

    //! Lexical values for strings and integers
    struct LexicalValue {
      char *mString;
      UInt32 mInteger;
    } mLval;
 
    // Input characters are read from the input stream into an input
    // buffer. The scanner processes characters from the input buffer.
    enum {
      cBUFFER_SIZE = 1024*1024          //!< size of the input buffer
    };
    char mBuffer [cBUFFER_SIZE];        //!< the input buffers
    UInt32 mBufferLength;               //!< number of characters read into the buffer
    UInt32 mBufferIndex;                //!< index of current character in the buffer
    char mCurrent;                      //!< looking-at character (set by nextChar method)

    //! Fill the buffer from the input stream
    /*! \return true if any characters were read, false at EOF or for an error
     *  \note If an error occurs then a message is written to the error sink
     */
    bool fillBuffer ();

    //! Get the next character from the buffer
    void nextChar ();

    // Strings are acreted into a text buffer while scanning.
    enum {
      cTEXT_SIZE = 2048                 //!< size of the text buffer
    };
    char mText [cTEXT_SIZE];            //!< the text buffer
    UInt32 mTextIndex;                  //!< index into the text buffer

  };

};

//! \class CodeAnnotationManager
/*! For each flavour of generated code annotation a CodeAnnotationManager instance
 *  is created. The annotation manager contains:
 *  \li global options for that flavour of annotation
 *  \li the store for that flavour
 *  \li a bind method to associate a new annotation with the instance
*/

class CodeAnnotationManager {
public: CARBONMEM_OVERRIDES

  //! ctor for CodeAnnotationManager
  /*! \param flags option mask formed from enum CodeAnnotation::Flags bits
   */
  CodeAnnotationManager (const UInt32 flags);

  CodeAnnotationManager ();

  virtual ~CodeAnnotationManager ();

  //! \return the option flags for this manager
  UInt32 getFlags () const { return mFlags; }

  //! Bind a manager and a range to an annotation.
  /*! When the manager is bound to the annotation the flags in the manager
   *  instances are pushed into the annotation and the annotation is written to
   *  the persistent annotation store.
   */
  void bind (const CodeAnnotation &x);

  //! \return the prefix for visible comments in the generated code
  const char *getPrefix () const;

  //! \return the full name for the annotation type
  const char *getName () const;

  //! \return the filename part of a path
  /*! \note this is used for the filename part of visible implementation
   *  annotations so that filenames from src/codegen get written as just
   *  "emitFoo.cxx" rather than ".../sandbox/src/codegen/emitFoo.cxx". It just
   *  makes them easier to read.
   */
  const char *stripName (const char *name) const;

  //! Open the annotation store for reading or writing
  /*! \return true iff the store was open successfully
   *  \param file_map_options  undocumented
   *  \param errmsg If opening the store fails errmsg is set to an error message
   */
  bool openStore (const char *file_root, const UInt32 file_map_options, UtString *errmsg);

  //! Close the annotation store
  void closeStore () { mStore.close (); }

  //! \return iff the given options bits are set
  bool test (const UInt32 mask) const { return mFlags & mask; }

  //! Set bits in the options mask
  void set (const UInt32 flags) { mFlags |= flags; }

  //! Reset bits in the options mask
  void reset (const UInt32 flags) { mFlags &= ~flags; }

  // Diagnostics
  void print (UtOStream &) const;       //!< prints the configuration to a stream
  void pr () const;                     //!< prints the configuration to stdout

private:

  UInt32 mFlags;                        //!< option mask from enum CodeAnnotation::Flags
  CodeAnnotationStore mStore;           //!< persistent store for these annotations

  //! \return the file name prefix for the store
  /*! The store writes to libdesign.prefix-annotations. This method returns the
   *  prefix string that must be unique over all the different flavours of
   *  annotation.
   */
  const char *getStoreNamePrefix () const;

};

//! \class CodeStream
/*! CodeStream extends UtOStream with annotation writing and storage. All
 *  writing of the model must go through CodeStream.
 *
 *  CodeStream wraps an embedded UtOStream and equips it with:
 *  \li a write method for CodeAnnotation objects that writes all objects to
 *     a store and visibile annotations as comments in the stream.
 *  \li tracking of the line and column information to bind the range
 *     CodeAnnotation instances.
 *  \li a rudimentary indentation engine that makes the generated code somewhat
 *     easier to browse.
 */
class CodeStream : public UtOStream {
public: CARBONMEM_OVERRIDES

 class InducedFaultMap;                 // forward decl
 
  enum {
    cDO_INDENT = 1<<0,                  //!< indent new lines by current amount
    cHASH_LINES = 1<<1,                 //!< enable #line directives in the output
    cDO_NOT_CLOSE = 1<<2,               //!< the code stream does not own the UtOStream
    cNO_USER_HASH_LINES = 1<<3          //!< do nothing in CodeStream::line
  };

  //! ctor for CodeStream
  /*! \param filename name of the output file - this must be the name of sink
   *  \param sink the wrapped UtOStream - must be opened by the instantiator of this
   *  \param implementation_manager manager for implementation annotations
   *  \param hdl_manager manager for HDL source annotations
   */
  CodeStream (
    const char *filename,
    CodeAnnotationManager &impl_manager,
    CodeAnnotationManager &hdl_manager, 
    UtOStream *sink,
    InducedFaultMap *faults,
    const UInt32 options = 0);

  virtual ~CodeStream ();

  //! Implement superclass is_open predicate
  virtual bool is_open () const;
  
  //! Implement superclass close method
  virtual bool close ();

  //! Implement superclass flush method
  virtual bool flush();

  //! Implement superclass getErrmsg method
  virtual const char* getErrmsg () { return mSink->getErrmsg (); }

  //! \return the column number
  virtual UInt32 getColumn () const { return mColumn; }

  //! Tab to a given column number
  void tab (const UInt32 column);

  //! \return iff we are at the start of a line
  virtual bool isStartOfLine () const { return mColumn == 1; }
  
  //! get the filename associated with this stream, or NULL if none defined yet
  virtual const char * getFilename() const { return mFilename.c_str (); }

  //! get the current lineno for this stream
  virtual UInt32 getLineno () const { return mLine; }

  //! output an explicit #line directive
  void line (const SourceLocator);

  //! output a line directive that points back to the current location in the output file
  void line ();

  //! Write a buffer to the stream, returning false if an error is discovered
  /*! This write implementation tracks line and column information for annotations */
  virtual bool write (const char* buffer, UInt32 length);

  //! Write an annotation to the output stream
  virtual void write (const CodeAnnotation &);

  //! Output statistics on the stream
  virtual void print (UtOStream *) const;

  //! \class InducedFaultMap
  /*! Regression of the annotation-based diagnostics requires deliberately
   *  inducing faults in the emitted code. If a code stream has an
   *  InducedFaultMap installed, then defects are added at specific lines.
   */
  class InducedFaultMap : public UtHashMap <UInt32, UtString> {
  public:

    InducedFaultMap () : UtHashMap <UInt32, UtString> () {}

    //! \return iff a fault had been specified for the given line of the file
    /*! \note If a fault is specified then it is return in faul
     */
    bool isFaulted (const UInt32 line, UtString *fault) const;

    // Diagnostics
    void print (UtOStream &) const;     //!< print the map to a stream
    void pr () const;                   //!< print the map to stdout

    // Extend the appended list
    void append (UtString s) { mAppended.push_back (s); }

    // Extend the prepended list
    void prepend (UtString s) { mPrepended.push_back (s); }

    // Iterate over the appended lines
    UtList <UtString>::const_iterator begin_appended () const { return mAppended.begin (); }
    UtList <UtString>::const_iterator end_appended () const   { return mAppended.end (); }

    // Iterate over the prepended lines
    UtList <UtString>::const_iterator begin_prepended () const { return mPrepended.begin (); }
    UtList <UtString>::const_iterator end_prepended () const   { return mPrepended.end (); }

  private:

    UtList <UtString> mAppended;        //!< list of junk to append to a file
    UtList <UtString> mPrepended;       //!< list of junk to prepend to a file

  };

  //! Install a fault map onto this code stream
  /*! When the fault map is installed, at each newline the write method checks
   *  for an entry in the fault map the line. If so, then the text of the fault
   *  is written on a separate line before continuing.
   */
  void install (const InducedFaultMap *induced_faults) { mInducedFaults = induced_faults; }

  UInt32 operator ++ (int) { return indent (+2); }
  UInt32 operator -- (int) { return indent (-2); }

  //! \return the current indentation amount
  UInt32 getIndent () const { return mIndent; }

protected:

  //! manager for implementation annotations
  CodeAnnotationManager &mImplAnnotations;

  //! manager for HDL source annotations
  CodeAnnotationManager &mHDLAnnotations;

  //! The induced faults map is used to introduce faults for regression testing
  const InducedFaultMap *mInducedFaults;

  UtOStream *mSink;                     //!< wrapped output stream

  UInt32 mFlags;                        //!< options for the code stream
  UtString mFilename;                   //!< the name of the output file
  UInt32 mLine;                         //!< the line number of the output file
  UInt32 mColumn;                       //!< the column number in the output file
  UInt32 mIndent;                       //!< indent amount when mFlags & cDO_INDENT

  UInt32 indent (const SInt32 delta) 
  { 
    SInt32 new_indent = mIndent + delta;
    if (new_indent < 0) {
      // the new indent would be negative, let's not allow that
      mIndent = 0;
    } else {
      mIndent = new_indent;
    }
    return mIndent;
  }

private:

  bool mNeedHashLine;                   //!< set when a #line must be inserted

};

//! \class SimpleCodeStream
/*! SimpleCodeStream wraps degenerate annotation managers around a code
 *  stream. It is used internally by the N-expression schema compiler for
 *  writing C/C++ output files.
 */
class SimpleCodeStream : public CodeStream {
public: CARBONMEM_OVERRIDES

  SimpleCodeStream (UtOStream *stream, const UInt32 flags = 0) :
    CodeStream ("annonymous", mImplManager, mHdlManager, stream, NULL,
      cDO_INDENT|cDO_NOT_CLOSE|flags),
    mFlags (flags),
    mHdlManager (CodeAnnotation::cDEFAULT_HDL|CodeAnnotation::cVISIBLE|CodeAnnotation::cNO_STORE),
    mImplManager (0)
  {}

  SimpleCodeStream (const char *filename, const UInt32 flags = 0) :
    CodeStream (filename, mImplManager, mHdlManager, new UtOFStream (filename), NULL,
      cDO_INDENT|cDO_NOT_CLOSE|flags),
    mFlags (flags),
    mHdlManager (CodeAnnotation::cDEFAULT_HDL|CodeAnnotation::cVISIBLE|CodeAnnotation::cNO_STORE),
    mImplManager (0)
  {}

  virtual ~SimpleCodeStream ();

private:

  const UInt32 mFlags;
  UtOFStream *mOut;                      //!< the output stream
  CodeAnnotationManager mHdlManager;    //!< hdl annotation manager
  CodeAnnotationManager mImplManager;   //!< implementation annotation manager
};

//!\class IndentingCodeStream

/*! The IndentingCodeStream extends CodeStream with an indent engine
 *   implemented as a simple state machine.
 *
 *  \note While the indent engine does improve the readability of the generated
 *  code it blows out CodeGen execution time by a rather embarrassing amount
 *  and we're talking orders of magnitude here. If you want to try it, then use
 *  the -useIndentEngine command line argument.
 *
 *  One of these days I'll have to speed strip out most of the crap and make it
 *  fast enough to use... -michael
 */

class IndentingCodeStream : public CodeStream {
public: CARBONMEM_OVERRIDES

  //! ctor for IndentingCodeStream
  /*! \param filename name of the output file - this must be the name of sink
   *  \param sink the wrapped UtOStream - must be opened by the instantiator of this
   *  \param implementation_manager manager for implementation annotations
   *  \param hdl_manager manager for HDL source annotations
   */
  IndentingCodeStream (
    const char *filename,
    CodeAnnotationManager &impl_manager,
    CodeAnnotationManager &hdl_manager, 
    UtOStream *sink,
    InducedFaultMap *faults);

  virtual ~IndentingCodeStream () {}

  //! \return the column number
  virtual UInt32 getColumn () const { return isStartOfLine () ? mIndent : mColumn; }

  //! Write a buffer to the stream, returning false if an error is discovered
  /*! This write implementation tracks line and column information for
    annotations and will perform a limited amout of C++ prettification */
  virtual bool write (const char* buffer, UInt32 length);

  //! Implement superclass flush method
  virtual bool flush();

private:

  // Indent engine state variables
  UInt32 mIndent;                       //!< current indent level
  bool mIsFirst;                        //!< first character in a line
  bool mInComment;                      //!< output is in a comment
  bool mInQuotes;                       //!< output is in quotes
  bool mDoNotIndentNextBrace;           //!< don't indent at the next brace
  bool mNeedHashLine;                   //!< need a line directive at the end of this line

  enum {
    cBufferSize = 8192                  //!< size of indent engine output buffer
  };
  char mBuffer [cBufferSize];           //!< indent engine buffer
  UInt32 mBufferLength;                 //!< number of output characters in the buffer

  //! Write a string to the output buffer
  /*! \note The string is not scanned for newlines so the caller must ensure
   *  that mLine is updated if needed
   */
  void output (const char *s, const UInt32 length);

  //! Write a character to the output buffer
  void output (char c);

  //! Indent by n in the output buffer
  void indent (const int n);

};

//! Write a DynBitVector to the stream
CodeStream &operator << (CodeStream &, const DynBitVector &);

#endif
