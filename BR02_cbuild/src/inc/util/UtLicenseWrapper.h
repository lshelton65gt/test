// -*- C++ -*-
#ifndef __UtLicenseWrapper_h_
#define __UtLicenseWrapper_h_
/*****************************************************************************


 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/UtLicense.h"
#include "util/UtCLicense.h"
#include "util/UtLicenseMsg.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"

//! UtLicenseWrapper - a simplified interface to license checks
/*!
 *  Construct a UtLicenseWrapper to checkout a license, and delete it
 *  to release the license.
 *
 *  While the license is in use, the heartbeat() method should be called
 *  at least once per second if possible.
 *
 *  NOTE: USE THE C API in UtCLicense.h FROM MAXSIM TRANSACTOR ADAPTERS.
 *        Using this (C++) API will result in windows linking errors due
 *        to mismatch in how gcc/mingw mangles names compared to MSVC.
 */
class UtLicenseWrapper
{
public:
  CARBONMEM_OVERRIDES

  //! implicit license checkout
  /*!
   *  For example, this code would be used to checkout the PCI Express license:
   *    CARBON_LICENSE_FEATURE(featureName, "crbn_vsp_exec_xtor_pcie", 23);
   *    mLicense = new UtLicenseWrapper(featureName, "PCI Express Transactor");
   *
   *  The local variable 'featureName' is defined and initialized with the 
   *  encoded name of the feature.
   *
   *  The featureDescription string is used to report error messages.
   */
  UtLicenseWrapper(const char *featureName, const char *featureDescription);

  //! implicit license release
  ~UtLicenseWrapper();

  //! Let the license server know we are still using the license.
  //! Call at least once per second to prevent loss of license.
  //! For example, call this from the update() method of a MaxSim component.
  void heartbeat() { /* transactor adapter licenses not currently heartbeated */ }

protected:
  // calculate how many heartbeat requests we should receive before calling 
  // the server
  UInt32 computeRequestsPerHeartbeat();

  // Special string constructed using CARBON_LICENSE_FEATURE macro.
  UtString mFeatureName;

  // Descriptive name for license, used in error messages.
  UtString mFeatureDescription;

  // These static members share a single instance of UtLicense 
  // across all UtLicenseWrapper objects.
  static UInt32 mLicenseCount;     // number of instances sharing the UtLicense
  static UtLicense *mLicense;      // common instance of UtLicense
  static UtLicense::MsgCB *mMsgCB; // common message callback for UtLicense

  // number of heartbeat calls made since last UtLicense::hearbeat
  static UInt32 mHeartbeatRequestCount;

  // number of requests we should receive before making an actual heartbeat 
  // call to the server
  static UInt32 mRequestsPerHeartbeat;
};

#endif // __UtLicenseWrapper_h_
