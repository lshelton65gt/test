// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtOZStream_h_
#define __UtOZStream_h_

#include "util/UtIOStream.h"
#include "util/Zstream.h"

//! An output stream to a Zostream
/*!
  Zostream has no real formatting capabilities, so this class does the
  formatting and then passes it to the underlying Zostream.
*/
class UtOZStream : public UtOStream
{
  UtOZStream();
  UtOZStream (const UtOZStream&);
  UtOZStream& operator=(const UtOZStream&);

public: 
  CARBONMEM_OVERRIDES

  //! Opens the file as a Zostream
  UtOZStream(const char* fileName, void* key);
  //! Destructor - will close the file
  ~UtOZStream();


  //! UtOStream::is_open()
  virtual bool is_open() const;
  //! UtOStream::close()
  virtual bool close();
  //! UtOStream::flush()
  virtual bool flush();
  //! UtOStream::bad()
  virtual bool bad() const;

#define UTOZSTREAM_INIT_FUNC stdAlert
  inline void UTOZSTREAM_INIT_FUNC()
  {
    ZOSTREAM_PTR_INIT(mZFile);
  }
  
protected:
  //! Called by UtOStream
  virtual bool write(const char* s, UInt32 size);
  
private:
  Zostream* mZFile;
};


#define UTOZSTREAM_INIT(var) \
   var.UTOZSTREAM_INIT_FUNC()

#define UTOZSTREAM_PTR_INIT(var) \
   var->UTOZSTREAM_INIT_FUNC() \

//! UtOZStream var (name)
#define UTOZSTREAM(var, name) \
   UtOZStream var (name, ZSTREAM_KEY); \
   UTOZSTREAM_INIT(var)

//! var  = new UtOZStream(name)
#define UTOZSTREAM_ALLOC(var, name) \
   var  = new UtOZStream(name, ZSTREAM_KEY); \
   UTOZSTREAM_PTR_INIT(var)

#endif

