// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonTypeUtil_h_
#define __CarbonTypeUtil_h_



//! Utilities for the ClockEdge enum
extern ClockEdge ClockEdgeOppositeEdge( ClockEdge ce );
extern const char *ClockEdgeString(  ClockEdge ce );

//! Compute ceil(log2(v))
extern UInt32 Log2(UInt32 v);

//! Descriptively print the given netflags 
extern void carbonNetPrintFlags(NetFlags flags);

#endif
