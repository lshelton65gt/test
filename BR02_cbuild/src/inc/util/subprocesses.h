// -*-C++-*-

// This file is downloaded from http://stlplus.sourceforge.net, and edited
// by Josh to use Carbon's STL-compatible classes rather than std::.

#ifndef SUBPROCESSES_HPP
#define SUBPROCESSES_HPP

#include "util/CarbonPlatform.h"
#if pfMSVC
#define MSWINDOWS 1
#endif

#include "util/UtString.h"
#include "util/UtStringArray.h"
#include <utility> // for std::pair

/*----------------------------------------------------------------------------

  Author:    Andy Rushton
  Copyright: (c) Andy Rushton, 2004
  License:   BSD License, see ../docs/license.html

  Platform-independent wrapper around the very platform-specific handling of
  subprocesses. Uses the C++ convention that all resources must be contained in
  an object so that when a subprocess object goes out of scope the subprocess
  itself gets closed down.

  ------------------------------------------------------------------------------*/
//#include "os_fixes.hpp"
#ifdef MSWINDOWS
#define NOMINMAX 1
#include <windows.h>
#endif
//#include "textio.hpp"
//#include "string_vectorio.hpp"

////////////////////////////////////////////////////////////////////////////////
// Argument vector class
// allows manipulation of argv-like vectors
// includes splitting of command lines into argvectors as per the shell
// (removing quotes) and the reverse conversion (adding quotes where necessary)

class arg_vector
{
private:
  char** v;

public:
  // create an empty vector
  arg_vector (void);

  // copy constructor (yes it copies)
  arg_vector (const arg_vector&);

  // construct from an argv
  arg_vector (char**);

  // construct from a command-line string
  // includes de-quoting of values
  arg_vector (const UtString&);
  arg_vector (const char*);

  ~arg_vector (void);

  // assignment operators are compatible with the constructors
  arg_vector& operator = (const arg_vector&);
  arg_vector& operator = (char**);
  arg_vector& operator = (const UtString&);
  arg_vector& operator = (const char*);

  // add an argument to the vector
  arg_vector& operator += (const UtString&);
  arg_vector& operator -= (const UtString&);

  // insert/clear an argument at a certain index
  // adding is like the other array classes - it moves the current item at index
  // up one (and all subsequent values) to make room
  void insert (unsigned index, const UtString&);
  void clear (unsigned index);
  void clear (void);

  // number of values in the vector (including argv[0], the command itself
  unsigned size (void) const;

  // type conversion to the argv type
  operator char** (void) const;
  // function-based version of the above for people who don't like type conversions
  char** argv (void) const;

  // access individual values in the vector
  char* operator [] (unsigned index) const;

  // special-case access of the command name (e.g. to do path lookup on the command)
  char* argv0 (void) const;

  // get the command-line string represented by this vector
  // includes escaping of special characters and quoting
  UtString image (void) const;

  // text output, prints the vector using the image() format
#if 0
  friend otext& operator << (otext&, const arg_vector&);
#endif
};

////////////////////////////////////////////////////////////////////////////////
// Environment class
// Allows manipulation of an environment vector
// This is typically used to create an environment to be used by a subprocess
// It does NOT modify the environment of the current process

#ifdef MSWINDOWS
#define ENVIRON_TYPE char*
#else
#define ENVIRON_TYPE char**
#endif

class env_vector
{
private:
  ENVIRON_TYPE v;

public:
  // create an env_vector vector from the current process
  env_vector (void);
  env_vector (const env_vector&);
  ~env_vector (void);

  env_vector& operator = (const env_vector&);

  void clear (void);

  // manipulate the env_vector by adding or removing variables
  // adding a name that already exists replaces its value
  void add (const UtString& name, const UtString& value);
  bool remove (const UtString& name);

  // get the value associated with a name
  // the first uses an indexed notation (e.g. env["PATH"] )
  // the second is a function based form (e.g. env.get("PATH"))
  UtString operator [] (const UtString& name) const;
  UtString get (const UtString& name) const;

  // number of name=value pairs in the env_vector
  unsigned size (void) const;

  // get the name=value pairs by index (in the range 0 to size()-1)
  std::pair<UtString,UtString> operator [] (unsigned index) const;
  std::pair<UtString,UtString> get (unsigned index) const;

  // access the env_vector as an envp type - used for passing to subprocesses
  ENVIRON_TYPE envp (void) const;

#if 0
  // prints the env_vector in the same form as the shell env command
  friend otext& operator << (otext&, const env_vector&);
#endif
};

////////////////////////////////////////////////////////////////////////////////

#ifdef MSWINDOWS
#define PID_TYPE PROCESS_INFORMATION
#define PIPE_TYPE HANDLE
#else
#define PID_TYPE int
#define PIPE_TYPE int
#endif

////////////////////////////////////////////////////////////////////////////////
// Synchronous subprocess

class subprocess
{
private:
  PID_TYPE pid;
  PIPE_TYPE child_in;
  PIPE_TYPE child_out;
  PIPE_TYPE child_err;
  env_vector env;
  int err;
  UtString mErrmsg;
  int status;

public:
  subprocess(void);
  virtual ~subprocess(void);

  void add_variable(const UtString& name, const UtString& value);
  bool remove_variable(const UtString& name);

  bool spawn(const UtString& path, const arg_vector& argv,
             bool connect_stdin = false, bool connect_stdout = false,
             bool connect_stderr = false
// Temporary hack to allow compiling on Windows.  Ultimately we
// probably need to implement this.
#ifndef MSWINDOWS
            ,bool merge_stderr = false
#endif
             );
  bool spawn(const UtString& command_line,
             bool connect_stdin = false, bool connect_stdout = false, bool connect_stderr = false);

  virtual bool callback(void);
  bool kill(void);

  int write_stdin(UtString& buffer);
  int read_stdout(UtString& buffer);
  int read_stderr(UtString& buffer);

  void close_stdin(void);
  void close_stdout(void);
  void close_stderr(void);

  bool error(void) const;
  int error_number(void) const;
  UtString error_text(void) const;

  int exit_status(void) const;

  void setVerbose (const bool verbosity = true) { mVerbose = verbosity; }

private:

  bool mVerbose;

};

////////////////////////////////////////////////////////////////////////////////
// Preconfigured subprocess which executes a command and captures its output

class backtick_subprocess : public subprocess
{
public:
  typedef UtStringArray strvec;
private:
  strvec mBuffer;
public:
  backtick_subprocess(void);
  virtual bool callback(void);
  bool spawn(const UtString& path, const arg_vector& argv);
  bool spawn(const UtString& command_line);
  const strvec& text(void) const;
  strvec& text(void);
};

UtStringArray backtick(const UtString& path, const arg_vector& argv);
UtStringArray backtick(const UtString& command_line);

////////////////////////////////////////////////////////////////////////////////
// Asynchronous subprocess

class async_subprocess
{
private:
  PID_TYPE pid;
  PIPE_TYPE child_in;
  PIPE_TYPE child_out;
  PIPE_TYPE child_err;
  env_vector env;
  int err;
  int status;
  void set_error(int);

public:
  async_subprocess(void);
  virtual ~async_subprocess(void);

  void add_variable(const UtString& name, const UtString& value);
  bool remove_variable(const UtString& name);

  bool spawn(const UtString& path, const arg_vector& argv,
             bool connect_stdin = false, bool connect_stdout = false, bool connect_stderr = false);
  bool spawn(const UtString& command_line,
             bool connect_stdin = false, bool connect_stdout = false, bool connect_stderr = false);

  virtual bool callback(void);
  bool tick(void);
  bool kill(void);

  int write_stdin(UtString& buffer);
  int read_stdout(UtString& buffer);
  int read_stderr(UtString& buffer);

  void close_stdin(void);
  void close_stdout(void);
  void close_stderr(void);

  bool error(void) const;
  int error_number(void) const;
  UtString error_text(void) const;

  int exit_status(void) const;
};

////////////////////////////////////////////////////////////////////////////////
#endif
