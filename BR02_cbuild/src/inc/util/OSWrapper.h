// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#ifndef __OSWrapper_h_
#define __OSWrapper_h_

#ifndef __CarbonPlatform_h_
#include "util/CarbonPlatform.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif

#ifndef __UtString_h_
#include "util/UtString.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#include <sys/types.h>
#include <stdio.h>
#include <time.h>

#if pfMSVC
#include <direct.h>
#endif

#if pfWINDOWS
// make windows conform to POSIX
// Define stat.h bits not available on Windows.
#define S_IRUSR _S_IREAD
#define S_IRGRP 0
#define S_IROTH 0
#define S_IWUSR _S_IWRITE
#define S_IWGRP 0
#define S_IWOTH 0

#else  // not found on Unix
#define O_BINARY 0
#define O_TEXT 0		
#endif

// Alternative Stack Size used after SegFault Signal is Received
#define SIGSTKSZ 256

// These macros are to avoid using the deprecated POSIX non-ISO
// functions on Windows, which are defined in the old MSVCRT.DLL.  By
// calling the underscore-versions, we get the new symbols defined in
// the new CRT DLLs such as MSVCR71.DLL.
// Note that some of these macro names differ from functions we define
// in OSWrapper only by case.
// These aren't currently necessary, but may be useful in the future.
// #if pfWINDOWS
// #define CARBON_OLDNAME_UNDERSCORE(function) _ ## function
// #else
// #define CARBON_OLDNAME_UNDERSCORE(function) function
// #endif

// #define OSchdir CARBON_OLDNAME_UNDERSCORE(chdir)
// #define OSclose CARBON_OLDNAME_UNDERSCORE(close)
// #define OSfileno CARBON_OLDNAME_UNDERSCORE(fileno)
// #define OSgetcwd CARBON_OLDNAME_UNDERSCORE(getcwd)
// #define OSisatty CARBON_OLDNAME_UNDERSCORE(isatty)
// #define OSlseek CARBON_OLDNAME_UNDERSCORE(lseek)
// #define OSmkdir CARBON_OLDNAME_UNDERSCORE(mkdir)
// #define OSopen CARBON_OLDNAME_UNDERSCORE(open)
// #define OSputenv CARBON_OLDNAME_UNDERSCORE(putenv)
// #define OSread CARBON_OLDNAME_UNDERSCORE(read)
// #define OSrmdir CARBON_OLDNAME_UNDERSCORE(rmdir)
// #define OSstrdup CARBON_OLDNAME_UNDERSCORE(strdup)
// #define OSunlink CARBON_OLDNAME_UNDERSCORE(unlink)
// #define OSwrite CARBON_OLDNAME_UNDERSCORE(write)
 
#if pfMSVC
// Define POSIX symbols such as O_RDONLY (to be ANSI-compatible _O_RDONLY).
#include <fcntl.h>
// POSIX localtime_r() is just like Windows localtime_s() with parameters
// reversed.
#ifndef localtime_r
#define localtime_r(simple_time, result) localtime_s(result, simple_time)
#endif
#endif

/*!
  \file
  The general area where global functions that are even mildly
  OS-specific should go.
*/

//! Printf format modifier for 64 bit arguments vs. 32 bit args
#if pfLP64
#  define Format64(x) "%l" #x
#  define Formatw64(w,x) "%" #w "l" #x
#  define FormatSize_t "%lu"
#else
#  if pfWINDOWS
#    define Format64(x) "%I64" #x
#    define Formatw64(w,x) "%" #w "I64" #x
#  else
#    define Format64(x) "%ll" #x
#    define Formatw64(w,x) "%" #w "ll" #x
#  endif
#  define FormatSize_t "%u"
#endif

//! Printf/Scanf format for pointers
#  define FormatPtr "%p"


//! memory copy definition
#define MEMCPY memcpy

#if pfUNIX
//! Environment variable list separator
#  define ENVLIST_SEPARATOR ":"
#endif

#if pfWINDOWS
//! Environment variable list separator
#  define ENVLIST_SEPARATOR ";"
#endif

//! A directory traversal class
/*!
  This class uses the posix-compliant opendir/readdir/etc. routines to
  traverse the directories. This class does not currently recurse
  through directories, but you can do that rather simply by
  using another OSDirLoop.

  For example:

  \code
  void traverse(const char* dir)
  {
    for (OSDirLoop loop(dir, "*"); !loop.atEnd(); ++loop)
    {
      const char* entry = *loop;
      // assuming there is a function called isDirectory somewhere
      if (isDirectory(entry))
        // recurse into the directory
        traverse(entry);
      else
        // do something interesting with a file here
        ;
    }
  }
  \endcode
*/
class OSDirLoop
{
public: CARBONMEM_OVERRIDES
  //! Constructor with directory and wildcard pattern.
  /*!
    No matter what the pattern the standard, '.', and '..' are skipped.
  */
  OSDirLoop(const char* directory, const char* wildcardPattern);
  
  //! Destructor
  ~OSDirLoop();
  //! End of the directory entries?
  bool atEnd() const;
  //! Dereference directory entry
  const char* operator*() const;
  //! Go to next directory entry
  OSDirLoop& operator++();

  //! Get the full path to the entry
  /*!
    This simply prefixes the directory from which we started the
    iteration. It only uses the directory that was passed in, it does
    not qualify it. Returns NULL if the directory loop is at the end.
  */
  const char* getFullPath(UtString* buf) const;

  //! Were there any errors during traversal?
  bool isError() const;
    
  //! Get the error string. NULL if no errors.
  const char* getError() const;
  
private:
  class DirClosure;
  DirClosure* mDirClosure;
  
  // forbid 
  OSDirLoop();
  // DIR structures should not be passed around
  OSDirLoop(const OSDirLoop&);
  OSDirLoop& operator=(const OSDirLoop&);
};

//! A class enabling multiple stat queries on a single file
/*!
  This gets filled by OSStatFileEntry.
*/
class OSStatEntry
{
public: CARBONMEM_OVERRIDES
  //! Constructor
  OSStatEntry();
  //! Destructor
  ~OSStatEntry();

  //! Does the path exist?
  bool exists() const;

  //! readable?
  bool isReadable() const;

  //! writable?
  bool isWritable() const;

  //! regular file?
  bool isRegularFile() const;

  //! directory?
  bool isDirectory() const;

  //! Execute permission?
  bool isExecutable() const;

  //! Was an error encountered
  /*!
    This will be true if the file does not exist or is not readable.
    If this is true, use exists() and isReadable() to figure out what
    went wrong.
  */
  bool isError() const;

  //! Get the inode/device pair
  /*!
    Returns zero for both if an error occurred during the stat call in
    OSStatFileEntry. 
  */
  void getDevInodePair(UInt64* device, UInt64* inode) const;

  //! Get the size of the file. -1 if error.
  SInt64 getFileSize() const;

  UInt64 getModTime() const;    // can be passed to OSTimeToString

private:
  // Hide these
  OSStatEntry (const OSStatEntry&);
  OSStatEntry& operator=(const OSStatEntry&);

  struct StatClosure;
  StatClosure* mStatClosure;

  friend int OSStatFileEntry(const char*, OSStatEntry*, UtString*);
};

//! convert a 64-bit time (e.g. from OSStatEntry::getModTime()) to a string
const char* OSTimeToString(UInt64 time, UtString* buf);

//! Parse pathName into path and file
/*!
  For example, 
  \verbatim
  if pathName =  /home/foo/runme then
  path = /home/foo
  file = runme
  \endverbatim
*/
void OSParseFileName(const char* pathName, UtString* path, UtString* file);


//! Return the absolute filepath given a relative path
void OSGetRealPath(const char* relativePath, UtString* absoluteFilePath);

//! Replace directory delimiters in path to given character
void OSReplaceDirectoryDelim(UtString* path, const char c);

//! Get time of day formatted according to the format string
/*!
  \param format Format of time UtString as described in strftime(3)
  \param str String storage for result
  \returns Formatted current time of day 
*/
const char* OSGetTimeStr(const char* format, UtString* str);

//! Parse time string of form "yyyy-mm-dd 24h:min:sec" into tm struct
/*!
  Similar to strptime(), but with a fixed format.
  \param timeString Input string containing time in format "yyyy-mm-dd 24h:min:sec"
  \param tm Pointer to tm struct that will be filled in
  \returns True if string was parsable, with tm struct filled in.
*/
bool OSParseTime(const char* timeString, struct tm* tm);

//! Call stat directly and fill the entry object 
/*!
  This works underneath OSStatFile(). The entry will keep the stat buf
  around so multiple queries can be made about the file/directory.
  
  \retval 0 if successful
  \retval 1 if stat failed. The errMsg will have the reason in 'path:
  reason" form.
*/
int OSStatFileEntry(const char* path, OSStatEntry* entry, UtString* errMsg);

//! Stat a file and put the results in a string
/*!
  This is a very low-level wrapper over stat(2). Wrappers can be made
  on top of this that do very specific things. This is only meant to
  give the most functionality for stat usage as possible.
  
  The caller can pass in all of the options to get as much information
  as possible, except if the file doesn't exist or there isn't access
  to the file. Note that if conflicting options are passed in, "fd"
  for example, it is impossible to return 1, but the result flags will
  have the appropriate things set.

  \returns 1 if all the option flags are true, 0 if they are not, -1
  if there was a problem with stat.
  \param path File or directory path. The directory path must be
  accessible in order to do anything really useful.
  \param options String of 1 letter options for stat. They can be any
  of the following: 
  \li <tt> \e e - Exists. This has a very profound effect. If the file
  or directory does not exist, instead of returning -1, 0 will be
  returned and none of the result flags will be set. </tt>
  \li <tt> \e f - Regular file. A normal file (ie., not a fifo, dev,
  etc.). </tt>
  \li <tt> \e r - The file/directory is readable. If it doesn't exist
  -1 is returned. </tt>
  \li <tt> \e w - The file/directory is writable. </tt>
  \li <tt> \e x - The file/directory has execute permission. </tt>
  \li <tt> \e d - The path points to a directory </tt>
  \param result Pointer to buffer. The results of processing the
  options will be put in this buffer in order. If the option passes
  then a '1' is put in the buffer in the appropriate place; otherwise,
  a '0' is put there. If there was a problem with stat then result is
  an error message (path: strerrno). For example, if "fw" is the
  options string and the file is normal but not writable the result
  will have "10". If the file doesn't exist result will be "\<file\>: No
  such file or directory. Therefore, it is very important that the
  return status be monitored.
*/
int OSStatFile(const char* path, const char* options, UtString* result);

//! Get the current directory
/*!
  This looks at PWD, if set; otherwise calls getcwd().
  \param cwd Buffer in which to put the current directory string.
*/
void OSGetCurrentDir(UtString* cwd);

//! Returns true if the character is a directory delimiter
bool OSIsDirectoryDelimiter(const char s);

//! File path constructor
/*!
  This simply takes dir adds the directory delimiter, appends file,
  and puts the new path into result. This can be called multiple times
  to incrementally construct a path.
  
  \param result Where the resultant directory is placed
  \param dir Directory to begin with. Can be same pointer as result.
  \param file File in the directory dir. Can be the same pointer as result.
*/
void OSConstructFilePath(UtString* result, 
                         const char* dir,
                         const char* file);

//! Get the filesize of a file
/*!
  \param filename Name of file to get the file size 
  \param fileSize The returned size of the file
  \param errMsg In case of an error, the reason
  \returns true if successful, false otherwise
*/
bool OSGetFileSize(const char* filename, SInt64* fileSize, UtString* errMsg);

//! Run a command in a shell and return the exit status
/*!
  Works just like the 'system' call, but handles the exit status
  properly, so the caller doesn't have to use WIFEXITED and
  WEXITSTATUS, etc.
  
  \param cmd The command to run
  \param nonExitMsg A pointer to a string that will be updated if the
  cmd did not exit, but instead was signaled. The exit message will
  say "terminated with signal: 11", if the command seg faulted, for
  example.
  \returns The exit status of the command. If the command did not
  exit, the return value will be -1 \e AND nonExitMsg will not be
  empty. Beware that a command can exit with a status of -1, so the
  string should be checked.
*/
int OSSystem(const char* cmd, UtString* nonExitMsg);

//! Run a command in a shell in the background
/*!
  Works like OSSystem, but runs the command in the background.  In
  Unix of course you just append "&".  In Windows we use a different
  mechanism.
  
  \param cmd The command to run
  \param error_msg A pointer to a string that will be updated if
  there is an starting the command process. The exit message will
  say "terminated with signal: 11", if the command seg faulted, for
  example.
  \returns 0 if the command is successfully invoked, otherwise the exit
  status of the command (-1 on Windows).  In the event of an error,
  nonExitMsg will be updated with any available information on the cause
  of the failure.
*/
int OSSystemBackground(const char* cmd, UtString* error_msg);

//! Expand a filename with environment variable ($ENV) & username (~user)
/*!
  This routine does not check for existence of the file.  Tilde expansion
  is NYI.

  \returns false if any username or environment variable is not found
*/
bool OSExpandFilename(UtString* buf, const char* filename, UtString* errmsg);

//! Contract a filename -- remove current directory, if present, to simplify
//! error messages
void OSContractFilename(UtString* buf, const char* filename);


//! Change current working directory to given directory
/*!
  Works just like the chdir function call, but it blocks interrupts
  properly.
  \param dir Directory to change to
  \param reason If not NULL and the chdir fails, the sterror is put
  here in the form of perror, ie, dir: strerror.

  \returns 0 if successful, -1 otherwise.
*/
int OSChdir(const char* dir, UtString* reason);

//! Create directory to given directory
/*!
  Works just like the mkdir function call.

  \param dir Directory to create
  \param mode Mode mask that will be combined with the umask
  \param reason If not NULL and the mkdir fails, the sterror is put
  here in the form of perror, ie, dir: strerror.

  \returns 0 if successful, -1 otherwise.
*/
int OSMkdir(const char* dir, UInt32 mode, UtString* reason);

//! Rename a file
/*!
 *! returns 0 on success, -1 on failure, filling out 'reason' in the
 *! event of a failure
 */
int OSRenameFile(const char *oldpath, const char *newpath, UtString* reason);

//! Delete a name and possibly the file it refers to
/*!
  Works just like the unlink system call, but blocks interrupts.
  \param pathname file to unlink
  \param reason If unlink fails and reason is not NULL the strerror is
  put here in the form of perror, ie., pathname: strerror. 

  \returns 0 if successful, -1 otherwise.
*/
int OSUnlink(const char* pathname, UtString* reason);

//! Delete a directory -- the directory must already be empty
/*!
  Works just like the rmdir system call, but blocks interrupts.
  \param pathname directory to remove
  \param reason If rmdir fails and reason is not NULL the strerror is
  put here in the form of perror, ie., pathname: strerror. 

  \sa OSDeleteRecursive

  \returns 0 if successful, -1 otherwise.
*/
int OSRmdir(const char* pathname, UtString* reason);

//! Recursively remove the contents of the specified directory, and the
//! directory itself
int OSDeleteRecursive(const char* pathname, UtString* reason);

//! Copy a file
bool OSCopyFile(const char* srcFile, const char* dstFile, UtString* errmsg);

//! OSCompareFile status enumeration
enum OSCompareFileStatus {
  OSCompareFailed,
  OSCompareDifferent,
  OSCompareSame
};

//! Compare two files.  If failure, return OSCompareFailed and
//! populate errmsg
OSCompareFileStatus OSCompareFiles(const char* file1,
                                   const char* file2,
                                   UtString* errmsg);

//! Fork off this process
/*!
  Works just like fork() except that interrupts are blocked.
  \param reason If the fork fails and reason in not NULL the reason
  why it failed is put here.
  
  \returns Process id as in fork. fork returns a pid_t, but to avoid
  os differences in file inclusions for types the pid_t is cast to an
  SInt32.
*/
SInt32 OSFork(UtString* reason);

//! Exec the given program with args
/*!
  Works just like execv except that interrupts are blocked.
  
  \param program Program to execute
  \param argv Arguments to program
  \param reason If the execv failed and reason is not NULL the reason
  it failed is put here in the form of perror.
  \returns -1 if not successful; otherwise, does not return.
*/
int OSExecv(const char* program, char *const argv[], UtString* reason);

//! Wait for pid to exit and evaluate exit status
/*!
  Works just like waitpid() except interrupts are blocked and the
  return status is evaluated. If the process was signaled, -1 will be
  returned and the reason string will be non-empty.

  \param pid Pid to wait for. This will be cast to a pid_t internally.
  \param reason This cannot be NULL. If the waitpid fails or if the
  process is signaled, this will have the perror in it.
  \returns Exit status of pid, -1 If waitpid failed or the signal
  number if the process was signaled. In either case of failure or
  signaling, reason will be non-empty.
*/
int OSWaitPid(SInt32 pid, UtString* reason);

//! Open a file using fopen and handle errors
/*!
  In Solaris, fopen is interruptable. fclose is also interruptable,
  but cannot be called again. This function simply calls fopen in a
  loop looking for an error return status and checking for errno
  equalling EINTR
  
  \param fname Name of file to open
  \param mode File mode as described in the fopen documentation
  \param reason If not NULL and the open fails, the reason is here in
  the form of perror.
  \returns A valid file descriptor if no error occurred and NULL if an
  error occurred.
*/
FILE* OSFOpen(const char* fname, const char* mode, UtString* reason);

//! Open a file using the open system call
/*!
  This opens a file in an EINTR loop as stated in OSFopen.
  
  \param fname File name to open
  \param flags Flags as stated in the open documentation
  \param mode Mode of file as stated in the open documentation
  \param reason If not NULL and an error occurred the reason is put
  here in the form of perror.
  \returns A file descriptor if no error, -1 if there is an error.
*/
#if pfMSVC
typedef int mode_t;
#include <sys/stat.h>
#endif
int OSSysOpen(const char* fname, int flags, mode_t mode, UtString* reason);

//! Read a file using the read system call
/*!
  This reads a file in an EINTR loop
*/
int OSSysRead(int fd, char* buf, UInt32 size, UtString* errmsg);

//! Close a file using the close system call
/*!
  Wraps the close system call in an EINTR loop.

  \param fd Valid file descriptor to close
  \param reason If not NULL, the reason why the close failed. Unlike
  the OSSysOpen call, this error will just have the strerror in it,
  since the filename is not available.
  \returns 0 on success, -1 on failure
*/
int OSSysClose(int fd, UtString* reason);

//! Seek in a file using lseek
/*!
  Wraps the lseek system call.

  The lseek function repositions the offset of the file descriptor fildes
  to the argument offset according to the directive whence as follows:

  SEEK_SET
  The offset is set to offset bytes.
  
  SEEK_CUR
  The offset is set to its current location plus offset bytes.
  
  SEEK_END
  The offset is set to the size of the file plus offset bytes.

  \param fd open file descriptor
  \param offset desired file position, relative to whence parameter
  \param whence SEEK_SET, SEEK_CUR, SEEK_END
  \param errmsg A descriptive string if something goes wrong.
  \returns 0 on success, -1 on failure
*/
int OSSysSeek(int fd, SInt64 offset, int whence, UtString* errmsg);

//! Get current offset from file descriptor
/*!
  Uses lseek to fetch current offset from OS.

  \param fd open file descriptor
  \param errmsg A descriptive string if something goes wrong.
  \returns offset on success, -1 on failure
*/
SInt64 OSSysTell(int fd, UtString* errmsg);

//! Truncate file to given length
/*!
  Wraps ftruncate function.

  If the file previously was larger than this size, the extra data is
  lost. If the file previously was shorter, it is extended, and the
  extended part reads as zero bytes.

  The file pointer is not changed.

  \param fd file descriptor open for writing
  \param size new file size
  \param errmsg A descriptive string if something goes wrong.
  \returns 0 on success, -1 on failure
*/
int OSSysFTruncate(int fd, SInt64 size, UtString* errmsg);

//! Legalize a filename for the current OS
const char* OSLegalizeFilename(const char* filename, UtString* buf);

//! Find the full path to executable given argv[0]
/*! Modifies buf, returns the result for convenience */
const char* OSFindExecutableInPath(const char* argv0, UtString* buf);

//!Gets the user or sys time (in microseconds),
/*! if \a user is true then returns user time, else system time
 *  if \a self is true then the time returned is only for self,
 *  otherwise for terminated children 
 */
UInt64 OSGetRusageTime(bool user, bool self);

//! Converts string to unsigned long long
/*!
  \param nptr string to convert
  \param endptr where the conversion ends. Must be non-null (unlike
  the strtoull call).
  \param base Base of the conversion
  \param errMsg If *endptr == nptr the reason it failed is here
  \returns Conversion result if successful, 0 otherwise. Success
  depends on the usage. But, if nptr == *endptr no conversion is done
  and there is an error message in the passed-in string.
*/
UInt64 OSStrToU64(const char* nptr, char **endptr, int base, UtString* errMsg);

//! Converts string to unsigned long
/*!
  \param nptr string to convert
  \param endptr where the conversion ends. Must be non-null (unlike
  the strtoul call).
  \param base Base of the conversion
  \param errMsg If *endptr == nptr the reason it failed is here
  \returns Conversion result if successful, 0 otherwise. Success
  depends on the usage. But, if nptr == *endptr no conversion is done
  and there is an error message in the passed-in string.
*/
UInt32 OSStrToU32(const char* nptr, char **endptr, int base, UtString* errMsg);

//! Converts string to long
/*!
  \param nptr string to convert
  \param endptr where the conversion ends. Must be non-null (unlike
  the strtoul call).
  \param base Base of the conversion
  \param errMsg If *endptr == nptr the reason it failed is here
  \returns Conversion result if successful, 0 otherwise. Success
  depends on the usage. But, if nptr == *endptr no conversion is done
  and there is an error message in the passed-in string.
*/
SInt32 OSStrToS32(const char* nptr, char **endptr, int base, UtString* errMsg);

//! Converts string to signed long long
/*!
  \param nptr string to convert
  \param endptr where the conversion ends. Must be non-null (unlike
  the strtoul call).
  \param base Base of the conversion
  \param errMsg If *endptr == nptr the reason it failed is here. This
  can be null.
  \returns Conversion result if successful, 0 otherwise. Success
  depends on the usage. But, if nptr == *endptr no conversion is done
  and there is an error message in the passed-in string if not null.
*/
SInt64 OSStrToS64(const char* nptr, char **endptr, int base, UtString* errMsg);

//! Converts string to double
/*!
  \param nptr string to convert
  \param endptr where the conversion ends. Must be non-null (unlike
  the strtod call).
  \param errMsg If *endptr == nptr the reason it failed is here. This
  can be null.
  \returns Conversion result if successful, 0 otherwise. Success
  depends on the usage. But, if nptr == *endptr no conversion is done
  and there is an error message in the passed-in string if not null.
*/
double OSStrToD(const char* nptr, char **endptr, UtString* errMsg);

//! Get the current hostname
void OSGetHostname(UtString* hostname);

//! Get the current PID as a number
UInt32 OSGetPid();

//! Get the current PID into a string
void OSGetPidStr(UtString* pidStr);

//! Sleep for the given number of seconds
void OSSleep(UInt32 numSeconds);

typedef void (*OSSigHandler)(int);
void OSSigaction(int sigType, OSSigHandler sigFn);
void OSSigSegvaction(int sigType, OSSigHandler sigFn);
void OSKillMe(int sigType);


//! OSKillSeverity status enumeration
enum OSKillSeverity {
  eKillInterrupt,
  eKillTerminate,
  eKillAssasinate
};

void OSKillProcess(int pid, OSKillSeverity sigType);

//! Get the system error message string for the last error
/*!
  Use this instead of strerror for re-entrancy.

  Set errmsg to the string corresponding to the last system error.
  This should be called immediately after a system call or library
  function fails so GetLastError()/errno gets the correct error.
  \param errmsg Pointer to string where result is returned

  \code
  if (QueryPerformanceCounter(&mReal) == 0) {
    UtString msg;
    OSGetLastErrmsg(&msg);
    cerr << "SPSnapshot::snap(): QueryPerformanceCounter(): " << msg;
  }
  \endcode

  \returns errmsg->c_str()
*/
const char* OSGetLastErrmsg(UtString* errmsg);

//! Return true if we appear to be running on Wine, the Windows emulator.
bool OSIsWine();

//! Create a name for a temporary file.
//! Similar to mktemp(), but GCC does not know to complain about it.
//! NOTE: Has the same problem as mktemp (file may be created by
//!       someone else between the time you generate the name and the
//!       time you open the file).
//!       However, since some interfaces require a file name instead of 
//!       an open descriptor (e.g. Zostream), mkstemp() cannot always be 
//!       used.
void OSGetTempFileName(UtString* out);

//! get the number of seconds that have happened since January 1, 1970.
UInt64 OSGetSecondsSince1970();

//! Return whether 64-bit models are supported on this platform
bool OSSupports64BitModels();

struct OSStdio
{
  static FILE* mstdout;
  static FILE* mstderr;

  typedef size_t (*fwrite_typedef) (const void *DATA, size_t SIZE, size_t COUNT, FILE *STREAM);
  typedef int (*fflush_typedef) (FILE *STREAM);

  static fwrite_typedef pfwrite;
  static fflush_typedef pfflush;
};
#endif
