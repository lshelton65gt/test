// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __UtShellTok_h_
#define __UtShellTok_h_


#include "util/UtString.h"

//! class UtShellTok -- iterates through a string yielding tokens
/*! There are two styles of tokenization:
 *
 *  1. specialChars=true, where the following chars have special
 *     meaning: \t, \n, \r, <singlequote>, <backquote>, <doublequote>,
 *     and that the delimiter character is ignored if it appears
 *     within a quoted region, or escaped with the backslash.
 *
 *  2. specialChars=false, where all characters are taken as literal
 *     characters.
 */
class UtShellTok
{
public: CARBONMEM_OVERRIDES
  typedef const char* value_type;
  enum Status
    {
      eOk,
      eNote,
      eWarning,
      eError
    };

  //! constructor
  UtShellTok(const char* str, bool envVars = true, const char* delims = NULL, bool specialChars=true);

  //! have we come to the end of the string?
  bool atEnd() const;

  //! Did we reach the end of the input data when finding this token?
  bool eof() const { return mEOF; }

  //! get the current token
  const char* operator*() const;

  //! parse the current token as a based number, returning true for success
  bool parseNumber(UInt32* number, int base = 10) const;

  //! advance to the next token
  void operator++();

  // check for the end, get the current token, advance to the next one
  bool operator()(const char** ptr);
        
  //! get the current position in the string
  const char* curPos();

  //! Reset the token to the given string. ++ must follow this.
  void resetStr(const char* str);
  
  //! Is this token protected by quotes?
  bool isProtected() const;

  //! Add more characters (only use if you got eof() while parsing)
  void parseString(const char* ptr);

  //! How many characters were consumed from the input
  /*!
   *! Invalid to call if environment variables are translated
   */
  UInt32 numConsumed() const;

  //! given a string that needs to be interpreted as a token, quote it for csh if it needs embedded whitespace.
  /*! The returned const char* contains a pointer to the passed-in buf,
   *! so it will be invalidated if you overwrite buf.
   */
  static const char* quote(const char* str, UtString* buf, bool always_quote = false, const char* delims = NULL);

  //! Status after environment variable expansion
  /*!
   *! After calling resetStr(), check the status for any problems
   */
  Status getResetStrStatus() const;

  //! Clears the status flag
  /*!
   *! Makes sure the flag is reset
   */
  void clearResetStrStatus();

  //! The actual error detected after expanding environment variables
  /*!
   *! If an error is detected, the buffer will contain the message generated
   */
  const char* getErrorBuffer() const;

private:
  void scanInput();

  const char* mStr;
  const char* mNextTok;
  UtString mToken;
  UtString mBuffer;
  UtString mErrorBuf;
  bool mProtectedTok;
  bool mEOF;
  bool mEnvVars;
  bool mSpecialChars;
  bool mInSQuote;
  bool mInDQuote;
  Status mStatus;
  UtString mDelims;
};

#endif
