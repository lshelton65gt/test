// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __UtUniquify_h_
#define __UtUniquify_h_


#include "util/Util.h"
#include "util/UtHashSet.h"
#include "util/HashValue.h"
#include "util/CarbonTypes.h"

// UtUniquify class - maintains a set of unique names
class UtUniquify
{
public: CARBONMEM_OVERRIDES
  //! ctor
  UtUniquify();

  //! dtor
  ~UtUniquify();

  //! make a name unique within the set
  /*! guaranteed to return a name that is unique within the set,
   *! by appending integers onto the name until it does not clash
   *! with another name in the set.
   */
  const char* insert(const char* name);

private:
  typedef UtHashSet<UtString*, HashPointerValue<UtString*> > StringSet;
  StringSet* mNames;
  SInt32 mIndex;
};

#endif // __UtUniquify_h_
