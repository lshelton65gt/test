// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#ifndef __UtSubject_h_
#define __UtSubject_h_



#ifndef _CARBON_HASH_SET_
#include "util/UtHashSet.h"
#endif
#ifndef __UtObserver_h_
#include "util/UtObserver.h"
#endif


template <class T>
class UtSubject
{
public: CARBONMEM_OVERRIDES

  //! Constructor.
  UtSubject(void) { mSubject = reinterpret_cast<T *>(this); }

  //! Constructor.
  UtSubject(T * subject) { mSubject = subject; }

  //! Destructor.
  virtual ~UtSubject(void) {}


  //! Attach an observer to this subject.
  void attach (UtObserver<T> * observer)
  {
    INFO_ASSERT(observer, "Invalid observer.");
    mObservers.insert(observer);
  }


  //! Detach an observer to this subject.
  void detach (UtObserver<T > * observer)
  {
    INFO_ASSERT(observer, "Invalid observer.");
    mObservers.erase(observer);
  }


  //! Instruct the subject to notify its observers.
  void notify (void)
  {
    for (ObserverTableLoop i = ObserverTableLoop(mObservers); not i.atEnd(); ++i)
    {
      UtObserver<T>* pObserver = *i;
      pObserver->update(mSubject);
    }
  }


  //! Is anyone observing this subject?
  inline bool isObserved (void)
  {
    return (not mObservers.empty());
  }


  //! Initialize the subject
  void putSubject(T* subject) {mSubject = subject;}


private:
  typedef UtHashSet<UtObserver<T> *> ObserverTable;
  typedef typename ObserverTable::UnsortedLoop ObserverTableLoop;

  T * mSubject;
  ObserverTable mObservers;

private:
  UtSubject(const UtSubject& src); // forbid
  UtSubject& operator=(const UtSubject& src); // forbid
}; // class UtSubject



#endif
