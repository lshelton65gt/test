// -*-c++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __MUTEXWRAPPER_H__
#define __MUTEXWRAPPER_H__

#include "util/c_memmanager.h"
#include "util/CarbonPthread.h"
#include "util/MemManager.h"
#include <cassert>

//! Utility class for working with pthread mutexes
/*!
  This class encapsulates basic mutex functionality.  Example usage is
  below.

  \code

  // Declare/initialize mutex lock
  MUTEX_WRAPPER_DECLARE(sMutex);

  void func()
  {
    // Acquire the lock
    MutexWrapper mutex(&sMutex);
    doSomething();
    // Destructor releases the lock
  }

  \endcode
  
 */
class MutexWrapper
{
public:
  CARBONMEM_OVERRIDES

  //! Typedef of mutex structure
  typedef pthread_mutex_t Mutex;

  //! Constructor locks mutex
  MutexWrapper(Mutex* mutex)
    : mMutex(mutex)
  {
    int ret = pthread_mutex_lock(mMutex);
    // Need to use raw assert here.  This class is used in things
    // (e.g. PLI wrapper) that link with shared libcarbon, which
    // doesn't expose the C++ symbols for the Carbon assert class.
    assert(ret == 0);    // this_assert_OK
  }

  //! Destructor unlocks mutex
  ~MutexWrapper()
  {
    pthread_mutex_unlock(mMutex);
  }

protected:
  Mutex* const mMutex;

private:
  // Forbid
  MutexWrapper();
  MutexWrapper(const MutexWrapper&);
  MutexWrapper& operator=(const MutexWrapper&);
};

//! Mutex class that installs a custom memory pool
/*!
  There is a lot of data that is shared across multiple models.  This
  causes problems with memory allocation/freeing.  Let's say thread 0
  creates a model, followed by thread 1.  Thread 0 allocates the
  common data, and thread 1 uses it.  If thread 0 then deletes its
  model first, thread 1 will end up freeing the common data when it
  deletes its model.

  The Carbon memory manager tries to be thread-safe by giving each
  thread a pool of memory.  That works, as long as memory is only
  freed by the same thread that allocated it.  For much of Carbon's
  common data, that's not the case.  In the above example, when thread
  1 frees the common data, the blocks have to go back into thread 0's
  pool.  If thread 0 is allocating memory at the same time, we can
  crash.

  A solution is to install a custom memory pool for allocation of the
  shared data.  The allocating thread's pool is not used, so there's
  no chance of collisions when the memory is freed.  Binding the
  installation of this memory pool to a mutex simplifies things.  If
  the same mutex is used for allocating and freeing, it ensures
  sequential access to the pool.

  Obviously, the lifetime of the pool is greater than the life of a
  mutex instantiation.  To ensure that the same pool is created for
  each instantiation of a mutex, the address of the mutex variable is
  used as a unique identifier.
 */
class MutexWrapperMemPool : public MutexWrapper
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor locks mutex and installs custom memory pool
  MutexWrapperMemPool(Mutex* mutex)
    : MutexWrapper(mutex), mPrevPool(NULL)
  {
    // Base constructor runs first, so mutex is already locked
    mPrevPool = CarbonMem::createMemPool(mMutex);
  }

  //! Destructor restores original pool and unlocks mutex
  ~MutexWrapperMemPool()
  {
    // Base destructor runs last, so mutex is still locked
    CarbonMem::restoreMemPool(mPrevPool);
  }

private:
  MemPool* mPrevPool;           //!< The thread's previous memory pool

  // Forbid
  MutexWrapperMemPool();
  MutexWrapperMemPool(const MutexWrapperMemPool&);
  MutexWrapperMemPool& operator=(const MutexWrapperMemPool&);
};

//! Macro to declare and statically initialize a mutex
#define MUTEX_WRAPPER_DECLARE(var) static MutexWrapper::Mutex var = PTHREAD_MUTEX_INITIALIZER

#endif
