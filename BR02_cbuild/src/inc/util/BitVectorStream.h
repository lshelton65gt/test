// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  Provides support for bitvectors with streams.

  This has been placed in a separate file (from BitVector.h) so that it can be
  conditionally included since it requires the loading of UtIOStream.h
  which can make the compile times slow.

  See BitVector.h for details.
*/
#ifndef __BitVectorStream_h_
#define __BitVectorStream_h_

#ifndef __UtIOStream_h_
#include "util/UtIOStream.h"
#endif

template<UInt32 Width, bool _stype> static inline UtOStream& operator<<(UtOStream& f, const BitVector<Width, _stype>& bv) {
  f.formatBitvec(bv);
  return f;
}

#endif /* __BitVectorStream_h_ */

