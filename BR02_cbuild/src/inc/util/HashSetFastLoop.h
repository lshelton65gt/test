// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _CARBON_HASH_SET_FAST_LOOP_
#define _CARBON_HASH_SET_FAST_LOOP_


#include "util/UtHashSet.h"

//! version of HashSet that does fast sorted loops at the cost of memory
template<class _KeyType,
         class _HashType = HashPointer<_KeyType, 2> >
class HashSetFastLoop: public UtHashSet<_KeyType, _HashType> 
{
public: CARBONMEM_OVERRIDES
  typedef HashSet<_KeyType, _HashType> Base;
  typedef typename Base::SortedLoop SortedLoop;
  typedef typename Base::UnsortedCLoop UnsortedCLoop;
  typedef typename Base::iterator iterator;
  typedef typename Base::value_type value_type;
  typedef typename Base::reference reference;

protected:
  typename Base::StlHash StlHash;

public: // redundant CARBONMEM_OVERRIDES
  HashSetFastLoop(): mSortedImage(0) {}
  HashSetFastLoop(size_t sz): Base(sz), mSortedImage(0) {}
  ~HashSetFastLoop() {clearLoop();}

  HashSetFastLoop(const HashSetFastLoop& src):
    Base(src), mSortedImage(0)
  {
  }

  bool operator==(const HashSetFastLoop& other) const {
    return this->mHash == other.mHash;
  }

  HashSetFastLoop& operator=(const HashSetFastLoop& src)
  {
    if (&src != this)
    {
      clearLoop();
      this->mHash = src.mHash;
      mSortedImage = NULL;
    }
    return *this;
  }


  //! create a loop through a sorted image of the objects contained in the set
  /*! To use this method, you must have an operator< defined in the
    _KeyType. */
  SortedLoop loopSorted() const {
    if (mSortedImage == NULL)
      mSortedImage = (new HashSImage(this->mHash))->copy();
    HashSetLoopI<_KeyType, _HashType, value_type, reference, typename Base::StlHash> loop(mSortedImage);
    return SortedLoop(&loop);
  }

  std::pair<iterator,bool> insert(const _KeyType& k) {
    clearLoop();
    return Base::insert(k);
  }
  template<typename T> void insert(T b, T e) {
    clearLoop();
    return Base::insert(b, e);
  }
  iterator insert(iterator /*__position*/, const value_type& __x) {
    clearLoop();
    insert(__x);
    return this->begin();             // end()?
  }
  void clear() {
    clearLoop();
    Base::clear();
  }
  void erase(iterator i) {
    clearLoop();
    Base::erase(i);
  }
  void erase(const _KeyType& key) {
    clearLoop();
    Base::erase(key);
  }

  //! clear, deleting the contained pointers
  void clearPointers() {
    clearLoop();
    Base::clearPointers();
  }

  
private:

  void clearLoop() {
    if (mSortedImage != NULL)
    {
      mSortedImage->free();
      mSortedImage = NULL;
    }
  }

  typedef HashSetSortedImage<_KeyType, _HashType, typename Base::StlHash> HashSImage;
  mutable HashSImage* mSortedImage;
};


#endif // _CARBON_HASH_SET_FAST_LOOP_
