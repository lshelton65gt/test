// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtDeque_h_
#define __UtDeque_h_
#ifndef __MemManager_h_

#include "util/MemManager.h"
#endif

#include <deque>

//! alternate STL deque definition that uses the Carbon memory allocator
/*!
 *! Note that this implementation should not be used at runtime.  See
 *! UtQueue.h for an implementation of this, which lacks the ability
 *! to work with objects larger than 32 bits, but is painlessly linkable
 */
template<class _Obj>
class UtDeque: public std::deque<_Obj, CarbonSTLAlloc<_Obj> >
{
  typedef std::deque<_Obj, CarbonSTLAlloc<_Obj> > _Base;
public: CARBONMEM_OVERRIDES

  template<class _Iter> UtDeque(_Iter _begin, _Iter _end):
    _Base(_begin, _end)
    {}

  UtDeque() {}
};

#endif
