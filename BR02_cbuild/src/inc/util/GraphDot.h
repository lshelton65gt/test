// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  A generic interface for writing graphs in Dot format.
*/

#ifndef _GRAPHDOT_H_
#define _GRAPHDOT_H_


#include "util/Graph.h"
#include "util/UtIOStream.h"

class GraphDotWriter : public GraphWalker
{
public: CARBONMEM_OVERRIDES
  GraphDotWriter(UtOStream& os);
  GraphDotWriter(const UtString& filename);
  virtual void output(Graph* g, const char* name="G");
  virtual GraphWalker::Command visitTreeEdge(Graph* g, GraphNode* from, GraphEdge* edge);
  virtual GraphWalker::Command visitCrossEdge(Graph* g, GraphNode* from, GraphEdge* edge);
  virtual GraphWalker::Command visitBackEdge(Graph* g, GraphNode* from, GraphEdge* edge);
  ~GraphDotWriter();
protected:
  virtual void writeName(Graph*, GraphNode*);
  virtual void writeLabel(Graph*, GraphNode*);
  virtual void writeLabel(Graph*, GraphEdge*);
  virtual void printNode(Graph* g, GraphNode* node);
  virtual GraphWalker::Command printEdge(Graph* g, GraphNode* from, GraphEdge* edge);
  bool mIsFileStream;
  UtOStream* mOut;
};

#endif // _GRAPHDOT_H_
