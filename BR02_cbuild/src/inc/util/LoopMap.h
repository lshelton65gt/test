// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _LOOPMAP_H_
#define _LOOPMAP_H_

#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif
#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

//! LoopMap template class
/*!
  This class provides a mechanism for a class to expose iteration over its
  STL map containers, without exposing the container.  The idea is to give the
  user a terse, fast, forward iteration through a structure.  If the user
  wants anything more sophisticated than that, he can use the iterator to
  populate his own private, self-managed container.  Note the getKey()
  and getValue() methods which distinguish this class from Loop.
  
  \verbatim
  Example:

  class Schedule
  {
  ...
  typedef LoopMap<ScheduleMap> ClockLoop;
  ClockLoop getScheduleClocks() {return ClockLoop(mClockSchedules);}
  ...
  }

  ...
  for (Schedule::ClockLoop p = sched->getScheduleClocks(); !p.atEnd(); ++p)
  {
  Schedule::Clock* sc = p.getValue();
  NUNetElab* clk = p.getKey();
  ...
  }
  \endverbatim
*/
template <class _MapType, class _Iterator>
class LoopMap
{
public: CARBONMEM_OVERRIDES
  typedef typename _Iterator::value_type value_type;
  typedef typename _Iterator::reference reference;
  typedef _Iterator iterator;

  //! Default constructor
  /*!
    Needed for iterator returns such as
    \verbatim
    LoopMap<myObj> iter;
    if (getIter(&iter))
    ...
    \endverbatim
    \warning Behavior is undefined if methods are used after using
    this constructor and the iterator is not properly initialized.
  */
  LoopMap()
  {
  }

  //! Construct a LoopMap using the map's begin() and end() methods.
  LoopMap(_MapType& map):
    mPtr(map.begin()),
    mEnd(map.end())
  {
  }

  //! Construct a LoopMap using explicitly specified begin and end points.
  LoopMap(const _Iterator& begin, const _Iterator& end):
    mPtr(begin),
    mEnd(end)
  {
  }

  //! Copy constructor to help the user manage & pass around Loops.
  LoopMap(const LoopMap& src):
    mPtr(src.mPtr),
    mEnd(src.mEnd)
  {
  }

  //! Assignment operator to help the user manage & pass around Loops.
  LoopMap& operator=(const LoopMap& src)
  {
    if (&src != this)
    {
      mPtr = src.mPtr;
      mEnd = src.mEnd;
    }
    return *this;
  }

  //! Gets the value in a map or hash_map iterator
  const typename _Iterator::value_type::second_type& getValue()
    const
  {
    return (*mPtr).second;
  }

  //! Gets a non const version of the value
  typename _Iterator::value_type::second_type& getNonConstValue()
  {
    return (*mPtr).second;
  }
  
  //! Gets the key in a map or hash_map iterator
  const typename _Iterator::value_type::first_type& getKey()
    const
  {
    return (*mPtr).first;
  }
  
  //! Gets the value.  Works just like a native STL iterator
  reference operator*()
    const
  {
    return *mPtr;
  }

  //! Move forward through the iteration.  Just like STL
  _Iterator& operator++()
  {
    ++mPtr;
    return mPtr;
  }

  //! Detect if we are at the end.  Just like (p == container.end()) in STL.
  bool atEnd() const
  {
    return mPtr == mEnd;
  }

  //! combine *, ++, and atEnd()
  /*!
   * Note that this creates a copy of the value_type.
   */
  bool operator()(value_type* ptr)
  {
    if (atEnd())
      return false;
    *ptr = *mPtr;
    ++mPtr;
    return true;
  }

  //! combine *, ++, and atEnd()
  /*!
   * Note that this does not create a copy of the value_type,
   * but instead just deals with pointers to it.  This is useful
   * when you do not want the value_type to be copied (say, when
   * the value_type is a pair and one of the pairs is a Map).
   */
  bool operator()(const value_type** ptr)
  {
    if (atEnd())
      return false;
    const value_type& ref = *mPtr;
    *ptr = &ref;
    ++mPtr;
    return true;
  }

  //! Present value of iterator.  Equivalent to begin() if called before any
  // ++ operations.
  _Iterator& current()
  {
    return mPtr;
  }

  //! The tail of the loop
  _Iterator& end() const
  {
    return mEnd;
  }

private:
  _Iterator mPtr;
  _Iterator mEnd;
}; // template <class _MapType> class LoopMap

//! CLoopMap template class
/*!
  This class provides a mechanism for a class to expose iteration over its
  STL map containers, without exposing the container.  The idea is to give the
  user a terse, fast, forward iteration through a structure.  If the user
  wants anything more sophisticated than that, he can use the iterator to
  populate his own private, self-managed container.  Note the getKey()
  and getValue() methods which distinguish this class from Loop.
  
  \verbatim
  Example:

  class Schedule
  {
  ...
  typedef CLoopMap<ScheduleMap> ClockLoop;
  ClockLoop getScheduleClocks() {return ClockLoop(mClockSchedules);}
  ...
  }

  ...
  for (Schedule::ClockLoop p = sched->getScheduleClocks(); !p.atEnd(); ++p)
  {
  Schedule::Clock* sc = p.getValue();
  NUNetElab* clk = p.getKey();
  ...
  }
  \endverbatim
*/
template <class _MapType, class _Iterator>
class CLoopMap
{
public: CARBONMEM_OVERRIDES
  typedef typename _Iterator::value_type value_type;
  typedef typename _Iterator::reference reference;
  typedef _Iterator iterator;

  //! Default constructor
  /*!
    Needed for iterator returns such as
    \verbatim
    CLoopMap<myObj> iter;
    if (getIter(&iter))
    ...
    \endverbatim
    \warning Behavior is undefined if methods are used after using
    this constructor and the iterator is not properly initialized.
  */
  CLoopMap()
  {
  }

  //! Construct a CLoopMap using the map's begin() and end() methods.
  CLoopMap(const _MapType& map):
    mPtr(map.begin()),
    mEnd(map.end())
  {
  }

  //! Construct a CLoopMap using explicitly specified begin and end points.
  CLoopMap(const _Iterator& begin, const _Iterator& end):
    mPtr(begin),
    mEnd(end)
  {
  }

  //! Copy constructor to help the user manage & pass around Loops.
  CLoopMap(const CLoopMap& src):
    mPtr(src.mPtr),
    mEnd(src.mEnd)
  {
  }

  //! Assignment operator to help the user manage & pass around Loops.
  CLoopMap& operator=(const CLoopMap& src)
  {
    if (&src != this)
    {
      mPtr = src.mPtr;
      mEnd = src.mEnd;
    }
    return *this;
  }

  //! Gets the value in a map or hash_map iterator
  const typename _Iterator::value_type::second_type& getValue()
    const
  {
    return (*mPtr).second;
  }

  //! Gets a non const version of the value
  typename _Iterator::value_type::second_type& getNonConstValue()
  {
    return (*mPtr).second;
  }
  
  //! Gets the key in a map or hash_map iterator
  const typename _Iterator::value_type::first_type& getKey()
    const
  {
    return (*mPtr).first;
  }
  
  //! Gets the value.  Works just like a native STL iterator
  reference operator*()
    const
  {
    return *mPtr;
  }

  //! Move forward through the iteration.  Just like STL
  _Iterator& operator++()
  {
    ++mPtr;
    return mPtr;
  }

  //! Detect if we are at the end.  Just like (p == container.end()) in STL.
  bool atEnd() const
  {
    return mPtr == mEnd;
  }

  //! combine *, ++, and atEnd()
  /*!
   * Note that this creates a copy of the value_type.
   */
  bool operator()(value_type* ptr)
  {
    if (atEnd())
      return false;
    *ptr = *mPtr;
    ++mPtr;
    return true;
  }

  //! combine *, ++, and atEnd()
  /*!
   * Note that this does not create a copy of the value_type,
   * but instead just deals with pointers to it.  This is useful
   * when you do not want the value_type to be copied (say, when
   * the value_type is a pair and one of the pairs is a Map).
   */
  bool operator()(const value_type** ptr)
  {
    if (atEnd())
      return false;
    const value_type& ref = *mPtr;
    *ptr = &ref;
    ++mPtr;
    return true;
  }

  //! Present value of iterator.  Equivalent to begin() if called before any
  // ++ operations.
  _Iterator& current()
  {
    return mPtr;
  }

  //! The tail of the loop
  _Iterator& end() const
  {
    return mEnd;
  }

private:
  _Iterator mPtr;
  _Iterator mEnd;
}; // template <class _MapType> class CLoopMap

#endif // _LOOPMAP_H_
