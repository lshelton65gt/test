// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  Declaration of the GraphBuilder class, which is a standard helper class
  for building GenericDigraphs.
*/

#ifndef _GRAPH_BUILDER_H_
#define _GRAPH_BUILDER_H_

#include "util/GenericDigraph.h"

//! Implements a builder utility for constructing graphs
template<typename T>   // T is the graph type to construct, eg GenericDigraph<...,...>
class GraphBuilder
{
private:
  typedef UtMap<typename T::NodeDataType,typename T::Node*> NodeMap;

public:
  //! Starts with an empty graph
  GraphBuilder()  { mGraph = new T; mWeAlloced = true; }

  //! Builds on an existing graph (should be empty)
  GraphBuilder(T* graph)  { INFO_ASSERT(graph->size() == 0, "Graph must be empty."); mGraph = graph; mWeAlloced = false; }

  //! Destructor
  ~GraphBuilder() { if (mWeAlloced) delete mGraph; }
  
  //! Add a node to the graph for the given user data
  bool addNode(typename T::NodeDataType userData, UInt32 flags = 0)
  {
    // If this node has already been added, return false
    typename NodeMap::iterator pos = mNodeMap.find(userData);
    if (pos != mNodeMap.end())
      return false;

    // This is a new node, so add it
    typename T::Node* node = new typename T::Node(userData);
    mNodeMap[userData] = node;
    mGraph->addNode(node);
    mGraph->setFlags(node, flags);
    return true;
  }

  //! Add an edge to the graph between the two specified nodes
  void addEdge(typename T::NodeDataType from, typename T::NodeDataType to,
               typename T::EdgeDataType userData, UInt32 flags = 0)
  {
    // make sure the nodes exist
    addNode(from);
    addNode(to);
    typename T::Node* fromNode = mNodeMap[from];
    typename T::Node* toNode   = mNodeMap[to];

    // populate the edge
    typename T::Edge* edge = new typename T::Edge(fromNode, toNode, userData);
    mGraph->addEdge(fromNode, edge);
    mGraph->setFlags(edge, flags);
  }

  //! Add an edge to the graph between the two specified nodes, if none exists with this data
  bool addEdgeIfUnique(typename T::NodeDataType from, typename T::NodeDataType to,
                       typename T::EdgeDataType userData, UInt32 flags = 0)
  {
    // find from node
    typename NodeMap::iterator pos = mNodeMap.find(from);
    if (pos == mNodeMap.end())
    {
      addEdge(from, to, userData, flags);
      return true;
    }
    typename T::Node* fromNode = pos->second;

    // find to node
    pos = mNodeMap.find(to);
    if (pos == mNodeMap.end())
    {
      addEdge(from, to, userData, flags);
      return true;
    }
    typename T::Node* toNode = pos->second;

    // look for an edge (from -> to) with the same data
    for (Iter<GraphEdge*> loop = mGraph->edges(fromNode); !loop.atEnd(); ++loop)
    {
      typename T::Edge* edge = mGraph->castEdge(*loop);
      typename T::Node* node = mGraph->castNode(mGraph->endPointOf(edge));
      if ((node == toNode) && (edge->getData() == userData))
        return false; // this edge already exists, just return
    }

    // no edge found, add it
    addEdge(from, to, userData, flags);
    return true;
  }

  //! Look up a graph node based on the data used to construct it
  typename T::Node* lookup(typename T::NodeDataType data) const
  {
    typename NodeMap::const_iterator pos = mNodeMap.find(data);
    if (pos == mNodeMap.end())
      return NULL;
    return pos->second;
  }

  //! Look up a graph edge based on the data used to construct its endpoints
  typename T::Edge* lookup(typename T::NodeDataType fromData,
                           typename T::NodeDataType toData)
  {
    // find from node
    typename NodeMap::iterator pos = mNodeMap.find(fromData);
    if (pos == mNodeMap.end())
      return NULL;
    typename T::Node* from = pos->second;

    // find to node
    pos = mNodeMap.find(toData);
    if (pos == mNodeMap.end())
      return NULL;
    typename T::Node* to = pos->second;

    // return first (from -> to) edge
    for (Iter<GraphEdge*> loop = mGraph->edges(from); !loop.atEnd(); ++loop)
    {
      typename T::Edge* edge = mGraph->castEdge(*loop);
      typename T::Node* node = mGraph->castNode(mGraph->endPointOf(edge));
      if (node == to)
        return edge;
    }

    // no edge found, return NULL
    return NULL;
  }

  //! Retrieve the graph that has been constructed
  T* getGraph()
  {
    T* graph = mGraph;
    mGraph = NULL;
    mNodeMap.clear();
    return graph;
  }

private:
  T*      mGraph;     //!< the graph being constructed  
  bool    mWeAlloced; //!< true unless the graph being built was supplied by the caller
  NodeMap mNodeMap;   //!< an auxilliary map from user data to graph nodes
};

#endif // _GRAPH_BUILDER_H_
