// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#ifndef __UtTestPatternGen_h_
#define __UtTestPatternGen_h_



#ifndef RANDOMVALGEN_H
#include "util/RandomValGen.h"
#endif
#ifndef __UtString_h_
#include "util/UtString.h"
#endif


/*!
  \file
  This file contains a number of classes that are used to create
  UtStrings containing patterns of zeros and ones.  These objects
  will create the same sequence given the same seed.
*/


//! UtTestPatternGen class
/*!
  Create a completely random pattern.
 */
class UtTestPatternGen
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  /*!
    Create an object that will create various test patterns.  Given
    the same seed, the object will return the same sequence of patterns.
    \param width The number of "bits" in the pattern.
   */
  UtTestPatternGen(UInt32 width);

  //! Destructor.
  virtual ~UtTestPatternGen(void); 

  //! Get the current pattern.
  /*!
    Get the current pattern that the object has created.  This can be
    called as many times as desired and the pattern will not changed.
    \param pattern The object will deposit the current pattern in this
    UtString.
  */
  virtual void appendPattern(UtString* pattern) = 0;

  //! Create the next pattern.
  /*!
    Generate the next pattern in the sequence.
  */
  virtual void nextPattern(void) = 0;

 protected:
  UInt32 mWidth;
  virtual void generatePattern(UtString &pattern);

 private:
  UtTestPatternGen(void);
  UtTestPatternGen(const UtTestPatternGen&);
  UtTestPatternGen& operator=(const UtTestPatternGen&);
};

//! Allow use of << operator
//extern std::ostream &operator<<(std::ostream &out, UtTestPatternGen& t);

//! Allow use of << operator
//extern std::ostream &operator<<(std::ostream &out, UtTestPatternGen* t);

//! Generate the next pattern in the sequence.
/*!
  Generate the next pattern in the sequence.  This is equivalent to
  calling nextPattern().
 */
extern UtTestPatternGen &operator++(UtTestPatternGen& t);




//! UtRandomTestPatternGen class
/*!
  This class will create a series of random patterns.  Given
  the same seed, the same sequence of patterns will be returned.
 */
class UtRandomTestPatternGen : public UtTestPatternGen
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  /*!
    Create an object that will generate random test patterns.  Given
    the same seed, the object will return the same sequence of patterns.
    \param width The number of "bits" in the pattern.
    \param seed The value used to seed the random value generator.
    \param initPattern By default the constructor runs the
    randomizer. If this is false then the constructor will NOT run the
    randomizer until nextPattern() is called. This is mainly used by
    derivations of this class.
   */
  UtRandomTestPatternGen(UInt32 width, RandomValGen& valueGen, bool initPattern = true);

  //! Destructor.
  virtual ~UtRandomTestPatternGen(void);

  //! Get the current pattern.
  /*!
    \sa UtTestPatternGen::appendPattern.
   */
  virtual void appendPattern(UtString* pattern);

  //! Create the next pattern.
  /*!
    \sa UtTestPatternGen::nextPattern.
   */
  virtual void nextPattern(void);

protected:
  //! Compute a new pattern.
  virtual void generatePattern(UtString &pattern);
  
  RandomValGen& mValueGen;
  UtString mPattern;

 private:
  UtRandomTestPatternGen(void);
  UtRandomTestPatternGen(const UtRandomTestPatternGen&);
  UtRandomTestPatternGen& operator=(const UtRandomTestPatternGen&);
};


class UtConstrainedRandomIntGen : public UtRandomTestPatternGen
{
public:
  UtConstrainedRandomIntGen(UInt32 width, SInt32 left, SInt32 right, RandomValGen& valueGen);
  virtual ~UtConstrainedRandomIntGen() {}

protected:
  virtual void generatePattern(UtString &pattern);

private:
  SInt32 mMin;
  SInt32 mMax;
};

//! UtConstantTestPatternGen class
/*!
  This class creates a constant pattern that consists of a
  single character.  It is useful for debugging.
 */
class UtConstantTestPatternGen : public UtTestPatternGen
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  /*!
    Create an object that will create totally random patterns.  Given
    the same seed, the object will return the same sequence of patterns.
    \param width The number of "bits" in the pattern.
    \param pattern_constant This is either '0' or '1'.
   */
  UtConstantTestPatternGen(UInt32 width, char pattern_constant);

  //! Destructor.
  virtual ~UtConstantTestPatternGen(void);

  //! Get the current pattern.
  /*!
    \sa UtTestPatternGen::appendPattern.
   */
  virtual void appendPattern(UtString* pattern);

  //! Create the next pattern.
  /*!
    \sa UtTestPatternGen::nextPattern.
   */
  virtual void nextPattern(void);

 private:
  UtString mPattern;

 private:
  UtConstantTestPatternGen(void);
  UtConstantTestPatternGen(const UtConstantTestPatternGen&);
  UtConstantTestPatternGen& operator=(const UtConstantTestPatternGen&);
};




//! UtToggledTestPatternGen class
/*!
  This object will create an inital random pattern and then
  toggle the values of that pattern each time a new pattern
  is requested.
 */
class UtToggledTestPatternGen : public UtRandomTestPatternGen
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  /*!
    Create an object that will create toggled patterns.  Given
    the same seed, the object will return the same sequence of patterns.
    \param width The number of "bits" in the pattern.
    \param seed The value used to seed the random value generator.
   */
  UtToggledTestPatternGen(UInt32 width, RandomValGen& valueGen);

  //! Destructor.
  virtual ~UtToggledTestPatternGen(void);

  //! Get the current pattern.
  /*!
    \sa UtTestPatternGen::appendPattern.
   */
  virtual void appendPattern(UtString* pattern);

  //! Create the next pattern.
  /*!
    \sa UtTestPatternGen::nextPattern.
   */
  virtual void nextPattern(void);

 private:
  UInt32 mCount;
  UtString mPatternA;
  UtString mPatternB;

 private:
  UtToggledTestPatternGen(void);
  UtToggledTestPatternGen(const UtToggledTestPatternGen&);
  UtToggledTestPatternGen& operator=(const UtToggledTestPatternGen&);
};




//! UtStepTestPatternGen class
/*!
  This object returns a series of patterns that replicate the step
  function.
 */
class UtStepTestPatternGen : public UtTestPatternGen
{
 public: CARBONMEM_OVERRIDES
  //! Constructor.
  /*! 
    Create an object that will create a series of patterns that realize
    the step function.
    \param width The number of "bits" in the pattern.
    \param step_up Is this stepping up, or stepping down?
    \param wait_count The step occurs after this number of patterns
    have been generated.
   */
  UtStepTestPatternGen(UInt32 width, bool step_up, UInt32 wait_count);

  //! Destructor.
  virtual ~UtStepTestPatternGen(void);

  //! Get the current pattern.
  /*!
    \sa UtTestPatternGen::getPattern.
   */
  virtual void appendPattern(UtString* pattern);

  //! Create the next pattern.
  /*!
    \sa UtTestPatternGen::nextPattern.
   */
  virtual void nextPattern(void);

 private:
  UInt32 mWaitCount;
  UInt32 mCurrentCount;
  UtString mPatternA;
  UtString mPatternB;

 private:
  UtStepTestPatternGen(void);
  UtStepTestPatternGen(const UtStepTestPatternGen&);
  UtStepTestPatternGen& operator=(const UtStepTestPatternGen&);
};

#endif

