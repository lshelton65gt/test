// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file 
  Support macros and functions for stream data processing
*/

#ifndef __UtStreamSupport_h_
#define __UtStreamSupport_h_

#include "util/CarbonPlatform.h"
#include <algorithm>


// If this macro were written similarly to BYTESWAP4 and passed a
// double*, its behavior would be undefined due to aliasing problems.
// The optimizer assumes reinterpret_cast<UInt64*>(ptr) can't be an
// alias of the macro's argument (because they're different types),
// but they are aliases.  But C++ allows aliasing char objects with
// other types.
#define BYTESWAP8(ptr)                              \
   char* cp = reinterpret_cast<char*>(ptr);         \
   std::reverse(cp, cp+8)

//! Makes UInt32 little_endian
#define BYTESWAP4(ptr)                              \
   UInt32 cp = *reinterpret_cast<UInt32*>(ptr);     \
   *reinterpret_cast<UInt32*>(ptr) =                \
    (((cp & 0x000000FF) << 24) |                    \
     ((cp & 0x0000FF00) << 8)  |                    \
     ((cp & 0x00FF0000) >> 8)  |                    \
     ((cp & 0xFF000000) >> 24))

//! Makes UInt16 little_endian
#define BYTESWAP2(ptr)                              \
   UInt16 cp = *reinterpret_cast<UInt16*>(ptr);     \
   *reinterpret_cast<UInt16*>(ptr) =                \
    (((cp & 0x00FF) << 8) |                         \
     ((cp & 0xFF00) >> 8))


//! on little-endian platforms do a byte swap
#define BYTESWAPONLITTLEENDIAN8(ptr)  BYTESWAP8(ptr)
#define BYTESWAPONLITTLEENDIAN4(ptr)  BYTESWAP4(ptr)
#define BYTESWAPONLITTLEENDIAN2(ptr)  BYTESWAP2(ptr)

#endif  // __UtStreamSupport_h_
