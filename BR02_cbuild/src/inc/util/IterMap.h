// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _ITERMAP_H_
#define _ITERMAP_H_


#include "util/CarbonAssert.h"
#include "util/StlIterator.h"

//! IterMap template class
/*!
  This class provides a mechanism for a class to expose iteration over its
  STL containers, while completely hiding the container.  The idea is to
  take the abstraction provided by Loop one step further, and and expose
  only the type of object being iterated over.
 
  A user uses an IterMap exactly like a LoopMap, except that the container
  class is not evident in the public interface in any way.

  \verbatim
  Example:
 
  typedef IterMap<Net*, NetElab*> NetMapIter;

  class Module
  {
  public: CARBONMEM_OVERRIDES
    ...
    NetMapIter loopInputNets();
    NetMapIter loopUndrivenNets();
    ...
  private:
    UtMap<Net*, NetElab*> mInputNets;
    UtMap<Net*, NetElab*, CompNets> mUndrivenNets;
  }
 
  ...
  for (NetMapIter p = mod->loopInputNets(); !p.atEnd(); ++p)
  {
    Net* net = p.getKey();
    NetElab* netElab = p.getValue();
    ...
  }
  for (NetMapIter p = mod->loopUndrivenNets(); !p.atEnd(); ++p)
  {
    Net* net = p.getKey();
    NetElab* netElab = p.getValue();
    ...
  }
  \endverbatim

  On the class-implementor side, to provide an IterMap, you have
  several choices.  If you can generate a LoopMap then you can create
  your iteration by passing that to the static method IterMap<KeyType,
  ValueType>::create.  To implement the methods outlined above, we
  write:

  \verbatim
  NetMapIter loopInputNets() {return NetMapIter::create(LoopMap<UtMap<NUNet*, NUNetElab*> > mInputNets);}
  NetMapIter loopUndrivenNets() {return NetMapIter::create(LoopMap<UtMap<NUNet*, NUNetElab*, CmpNets> > mUndrivenNets);}
  \endverbatim  

  IterMap abstracts the looping over the different containers so they look the
  same to the caller.  So there is no exposure to the caller to the
  container-type being used to hold the nets.
  
  If the nets are not held in an STL container, but are in a more
  complex graph of some sort, then you might not be able to use the
  existing LoopMap* template classes to implement the iteration.  You
  must instead write your own class that, at a minimum, provides
  atEnd, operator++, operator*, getKey(), and getValue() operators.

  If your class holds a lot of data, then you may want to avoid writing
  a copy constructor, because in the STL methodology, iterators can copied
  around quite a bit.  You can avoid the innefficiencies associated with
  this by using reference counting.  IterMap provides a built-in mechanism
  to do this:  derive your new class from the class IterMap::RefCnt.

  \verbatim
    class MyNewLoopClass: public IterMap<MyKey,MyValue>::RefCnt
    {
    public: CARBONMEM_OVERRIDES
      MyNewLoopClass(...);
      virtual ~MyNewLoopClass() {}
      virtual bool atEnd() const {....}
      virtual void operator++() {...}
      TBD virtual reference operator*() const {....}
      virtual MyKey getKey() const {...}
      virtual MyValue getValue() const {...}
    private:
      MyNewLoopClass(const MyNewLoopClass&); // forbid
      MyNewLoopClass& operator=(const MyNewLoopClass&); // forbid
      ...
    };
  \endverbatim  

  Then you can construct an IterMap from a new'd instance of your new class.

  \verbatim
    IterMap<MyKey,MyValue> makeIter(...) {
      return new IterMap<MyKey,MyValue>(new MyNewLoopClass);
    }
  \endverbatim  

  The disadvantage of this approach is that all copies of the iterator
  will share state, and so you won't be able to run multiple algorithms
  on the iterator using the STL begin() and end() methods, unless you
  re-create it.

  If your class holds relatively little data, and you implement a
  copy constructor, then you have effectively created the same
  interface as the Loop* templates, and you can use IterMap::create to
  build your IterMap.

  The performance implications of using an IterMap<Net*, NetElab*>
  rather than a LoopMap<UtMap<Net*,NetElab*> > are small but non-zero.
  Every method call to IterMap will result in a virtual function call.
  So if you write your loop with ++, *, and atEnd, then you will make
  three virtual function calls per iteration.

  In the future we may solve this with operator() much the same as
  with Iter<> but for now, we have to pay the price of the virtual
  calls.
 */
template <class _KeyType, class _ValueType>
class IterMap
{
public: CARBONMEM_OVERRIDES
  // TBD typedef _ValueType reference;
  typedef _ValueType value_type;
  typedef _ValueType& reference;

  //! Base interface for abstract iteration
  class Base
  {
  public: CARBONMEM_OVERRIDES
    Base() {}
    virtual ~Base() {}
    virtual Base* copy() const = 0;
    virtual void free() = 0;
    virtual bool atEnd() const = 0;
    virtual void operator++() = 0;
    // TBD virtual reference operator*() = 0;
    virtual _KeyType getKey() const = 0;
    virtual _ValueType getValue() const = 0;
  };

  //! Class Iter::RefCnt implements the reference-counting to aid implementors
  class RefCnt: public Base
  {
  public: CARBONMEM_OVERRIDES
    RefCnt() {
      mRefCnt = 0;
    }
    virtual ~RefCnt() {
      INFO_ASSERT(mRefCnt == 0, "References to iterator still exist");
    }
    virtual Base* copy() const {
      ++mRefCnt;
      return const_cast<RefCnt*>(this);
    }
    void free() {
      --mRefCnt;
      INFO_ASSERT(mRefCnt >= 0, "Reference count underflow.");
      if (mRefCnt == 0)
      {
        delete this;
      }
    }

  private:
    mutable int mRefCnt;
  };

  //! Class Iter::Factory manages the container's iterators.
  /*!
    This class is what the implementor of an Iter interface must
    instantiate with full knowledge of the container type and iterator,
    as shown in the example for class Iter.
  */
  template <class _Looper>
  class Factory: public Base
  {
  public: CARBONMEM_OVERRIDES
    Factory(_Looper looper)
      : mLoop(looper)
    {
    }

    bool atEnd() const {return mLoop.atEnd();}
    void operator++() {++mLoop;}
    // TBD reference operator*() const {return *mLoop;}
    _KeyType getKey() const {return mLoop.getKey(); }
    _ValueType getValue() const { return mLoop.getValue(); }

    Base* copy() const {
      return new Factory(mLoop);
    }
    void free() {
      delete this;
    }
  private:
    Factory(const Factory& src); // forbid
    Factory& operator=(const Factory& src); // forbid

    _Looper mLoop;
  };

  //! Default constructor
  /*!
    Needed for iterator returns such as
    \verbatim
    Iter<myObj> iter;
    if (getIter(&iter))
     ...
    \endverbatim
    \warning Behavior is undefined if methods are used after using
    this constructor and the iterator is not properly initialized.
  */
  IterMap()
  {
    mRoot = NULL;
  }

  template <typename _Looper> static IterMap create(_Looper looper)
  {
    IterMap i;
    i.mRoot = new Factory<_Looper>(looper);
    return i;
  }

  //! Construct a IterMap given an IterMap::Base
  IterMap(Base* root)
  {
    if (root == NULL)
      mRoot = NULL;
    else
      mRoot = root->copy();
  }

  //! Copy constructor to help the user manage & pass around IterMaps.
  IterMap(const IterMap& src)
  {
    if (src.mRoot == NULL)
      mRoot = NULL;
    else
      mRoot = src.mRoot->copy();
  }

  //! Assignment operator to help the user manage & pass around IterMaps.
  IterMap& operator=(const IterMap& src)
  {
    if (&src != this)
    {
      if (mRoot != src.mRoot)
      {
        if (mRoot != NULL)
          mRoot->free();
        if (src.mRoot == NULL)
          mRoot = NULL;
        else
          mRoot = src.mRoot->copy();
      }
    }
    return *this;
  }

  //! Assignment operator to help the user manage & pass around IterMaps.
  IterMap& operator=(Base* root)
  {
    if (mRoot != root)
    {
      if (mRoot != NULL)
        mRoot->free();
      if (root == NULL)
        mRoot = NULL;
      else
        mRoot = root->copy();
    }
    return *this;
  }

  ~IterMap()
  {
    if (mRoot != NULL)
      mRoot->free();
  }

  //! Gets the value.  Works just like a native STL iterator
// TBD
//   reference operator*()
//     const
//   {
//     return **mRoot;
//   }

  //! Move forward through the iteration.  Just like STL
  void operator++()
  {
    ++(*mRoot);
  }

  //! Detect if we are at the end.  Just like (p == container.end()) in STL.
  bool atEnd() const
  {
    return mRoot->atEnd();
  }

  //! Get the map key
  _KeyType getKey() const
  {
    return mRoot->getKey();
  }

  //! Get the map value
  _ValueType getValue() const
  {
    return mRoot->getValue();
  }

  typedef StlIterator<IterMap<_KeyType, _ValueType> > iterator;
  typedef StlIterator<IterMap<_KeyType, _ValueType> > const_iterator;

  //! get the base
  Base* getBase() const { return mRoot; }

  //! Return an STL iterator for use in simple algorithms (e.g. find_if, not sort)
  iterator begin() {return iterator(this);}

  //! Return an STL iterator for use in algorithms (e.g. find_if, not sort)
  iterator end() {return iterator(NULL);}

private:
  Base* mRoot;
};


#endif // _ITERMAP_H_
