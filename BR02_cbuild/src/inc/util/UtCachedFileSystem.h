// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __UtCachedFileSystem_h_
#define __UtCachedFileSystem_h_

#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif
#ifndef __Util_h_
#include "util/Util.h"
#endif
#ifndef __CarbonTypes_h_
#include "util/CarbonTypes.h"
#endif

class UtIOStreamBase;
class UtICStream;
class UtOCStream;

//! UtOCStreamSystem class - maintains an arbitrarily large set of 
//! "virtually" open files, utilizing a finite set of physically open
//! files
class UtCachedFileSystem
{
public: CARBONMEM_OVERRIDES
  //! ctor
  UtCachedFileSystem(SInt32 numPhysicalFiles = 5);

  //! dtor
  ~UtCachedFileSystem();

  //! For statistical purposes, report the number of times we had to
  //! re-open a file within the system
  UInt32 getNumReopens() const {return mNumReopens;}

private:
  friend class UtOCStream;
  friend class UtICStream;

  //! declare that a cached-file has been created
  void registerFile(UtIOStreamBase*);

  //! declare that a cached-file has been permanently closed
  void unregisterFile(UtIOStreamBase*);

  //! declare that a cached-file has grabbed a new file descriptor
  void activate(UtIOStreamBase*);

  //! check if we are at our max physical file list and if so, close an old one
  void maybeRelease();

  //! internal function
  void markInactive(SInt32 index);

  //! bump up the timestamp index and return it
  UInt32 bumpTimestamp();

  // Keep track of open file descriptors, so maybeRelease() can discard old ones
  UtIOStreamBase** mOpenFiles;

  SInt32 mNumPhysicalFiles;
  SInt32 mNumOpenFiles;
  UInt32 mTimestamp;
  UInt32 mNumReopens;

  // Keep track of all the file descriptors, open & closed
#if pfLP64
  typedef UtHashSet<UtIOStreamBase*,
                    HashPointer<UtIOStreamBase*>,
                    HashObjectMgr<UtIOStreamBase*> > FileSet;
#else
  typedef UtHashSet<UtIOStreamBase*> FileSet;
#endif
  FileSet* mAllFiles;
};

#endif //  __UtCachedFileSystem_h_
