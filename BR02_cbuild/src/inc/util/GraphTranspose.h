// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  This file contains a class that can transpose a graph in place.
*/

#ifndef _GRAPHTRANSPOSE_H_
#define _GRAPHTRANSPOSE_H_

#include "util/UtMultiMap.h"
#include "util/LoopMap.h"

//! UtGraphTranspose class
/*! This class transposes a graph in place. It does this by removing
 *  all the edges and putting them in a map temporarily. Once they are
 *  remove, then they get reapplied as reverse edges.
 *
 *  Two alternatives were considered in creating this class. The first
 *  is transposing the graph in place. The second is creating a new
 *  transpose graph from the existing graph. The benefit of doing it
 *  in place is a reduction in memory allocation and deallocation. The
 *  benefit of creating a new graph is it could be done in a more
 *  generic way.
 *
 *  This class counts on a number of functions such as
 *  T::Node::removeEdges(), T::Edge::replaceToNode and
 *  T::Node::addEdge. These are defined in the GenericDigraph class
 *  but not in the more general Graph class. The reason is that Graph
 *  does not dictate a representation.
 *
 *  In the future, if there are more derivations off Graph other than
 *  GenericDigraph, it would be nice to create a root class that
 *  contains some of the above functions so that this transpose class
 *  works on the new graph type.
 *  
 */
template<typename T>  // T is the graph type for the graph
class UtGraphTranspose
{
public:
  //! constructor
  UtGraphTranspose(T* graph) : mGraph(graph) {}

  //! destructor
  ~UtGraphTranspose() {}

  //! Function to execute the transpose
  void transpose()
  {
    // Allocate space for and populate all the edges of the graph
    GraphEdges graphEdges;
    for (Iter<GraphNode*> n = mGraph->nodes(); !n.atEnd(); ++n) {
      // Add the edges to our map
      GraphNode* node = *n;
      TNode* tNode = mGraph->castNode(node);
      for (Iter<GraphEdge*> e = mGraph->edges(node); !e.atEnd(); ++e) {
        GraphEdge* edge = *e;
        TEdge* tEdge = mGraph->castEdge(edge);
        graphEdges.insert(GraphEdgesValue(tNode, tEdge));
      }

      // Clear the edges for this node
      mGraph->removeEdges(tNode);
    }

    // Add the edges back in reverse
    for (GraphEdgesLoop l(graphEdges); !l.atEnd(); ++l) {
      // Get the new from node
      TEdge* edge = l.getValue();
      GraphNode* node = mGraph->endPointOf(edge);
      TNode* newFrom = mGraph->castNode(node);

      // Replace the to node
      TNode* newTo = l.getKey();
      mGraph->transposeEdge(edge, newTo);

      // Add the edge to the graph
      mGraph->addEdge(newFrom, edge);
    }
  } // void transpose

private:
  //! Abstraction for a graph node
  typedef typename T::Node TNode;

  //! Abstraction for a graph edge
  typedef typename T::Edge TEdge;

  //! Abstraction for a map to store edges temporarily
  typedef UtMultiMap<TNode*, TEdge*> GraphEdges;

  //! Abstraction for the value type for the multi map
  typedef typename GraphEdges::value_type GraphEdgesValue;

  //! Abstraction for a loop over the edges
  typedef LoopMap<GraphEdges> GraphEdgesLoop;

  //! Graph to be transposed
  T* mGraph;
};

#endif // _GRAPHTRANSPOSE_H_
