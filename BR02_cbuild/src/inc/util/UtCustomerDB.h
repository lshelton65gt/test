// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtCustomerDB_h_
#define __UtCustomerDB_h_


#include "util/Iter.h"
#include "util/CarbonTypes.h"
#include "util/UtLicense.h"

// forward decls
class ZostreamDB;
class ZistreamDB;
class UtString;

//! Container class for customer database information
/*!
  This class holds the information contained in the customer database
  in addition to any licensed models used by the customer. 

  This class distinguishes between customer signatures and ip
  features. These fields have been repurposed over the years and so
  their names aren't perfect.  Essentially, the customer signature is
  an ordered list of license candidates to check out instead of the
  default runtime license.  These don't all need to be checked out -
  one successful checkout is sufficient.  The IP features are licenses
  to check out in addition to a runtime license.  All of them need to
  be checked out.

  The general flow for licensing is the following.
  
  1) cbuild is invoked.
  2) If cbuild is told to override the runtime license (see
     CarbonContext::doVSPLicensing() for gory details on this), the
     customer signature is set to the list of runtime licenses to
     attempt to check out.
  3) Any xtors that are compiled into the design may have a licensing
     pragma in its `protect field. If so, it is added to the list of
     IP features. The license feature that will be checked out during
     runtime is specified in the pragma.
  4) Upon runtime execution of the Carbon Model, the database is read
     and this object is used to checkout the required licenses.
 
*/
class UtCustomerDB
{
protected:
  //! class containing all the data structures needed for the db
  class CustDBHelper;
  //! Allow access to UtCustomerDB
  friend class CustDBHelper;

  struct SettingHelper
  {
    CARBONMEM_OVERRIDES
    SettingHelper() : mFileName(NULL), mReason(NULL), mCurrent(NULL)
    {}
    const char* mFileName;
    UtString* mReason;
    const char* mCurrent;
  };

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  UtCustomerDB();

  //! destructor
  ~UtCustomerDB();

  //! Iterator for string pointers
  typedef Iter<char*> StrIter;

   //! Signature class
  /*!
    This class describes a signature and all associated license types.

    The signature itself is a comma-separated ordered list of strings
    to be appended to the default runtime license fetaure name.  When
    the signature is present, license checkout attempts are made for
    these modified feature names, in order, until one succeeds,
    instead of for the default runtime license.

    For example, if the signature contained "foo,bar", we would first
    attempt to check out crbn_vsp_foo, then crbn_vsp_bar, and then
    fail.

    The keyword DEFAULT when present in the list indicates that the
    default license feature (with no suffix) should be checked out.
    This can be used to allow a fallback on the default runtime
    license, which is not done automatically.

    Other members of this class are the list of IP features (features
    required at runtime for certain IP like transactors) and a
    timebomb that bypasses traditional licensing.
  */
  class Signature
  {
    friend class UtCustomerDB::CustDBHelper;
    

  public:
    CARBONMEM_OVERRIDES

    //! Database read constructor
    Signature();

    //! constructor
    Signature(const char* sig);

    //! destructor
    ~Signature();

    //! Get the signature string
    const char* c_str() const;

    //! Get the signature as a UtString
    const UtString* getStr() const { return mSignature; }

    //! Add an ip feature license needed for runtime
    /*!
      If more than one of a particular license is required, simply
      added it as many times as it is needed.
    */
    void addIPFeature(const char* featureName);

    //! Loop the ip license feature names that require the signature
    /*!
      These feature names come from `protect blocks in hdl source.
    */
    StrIter loopIPFeatures() const;

    //! Get the time bomb setting for this signature
    /*!
      Note that a signed value is returned. This is due to the time_t
      type being signed. A negative value will never appear here.
      Also note that we return 64 bits. time_t is currently not 64
      bits, but most likely will be.

      \retval 0 If there isn't a time bomb setting.
      \retval >0 If there is a time bomb setting
    */
    SInt64 getTimeBomb() const;

    //! Set the time bomb
    /*!
      Asserts if the time bomb is less than 0.
      The time bomb is used for licensing purposes. If no time bomb
      exists, normal flexlm licensing should be used.
    */
    void putTimeBomb(SInt64 timeBomb);
    
    //! Write the customer info to a db file
    bool dbWrite(ZostreamDB& outDB) const;
    
    //! Read the customer info from a db file
    bool dbRead(ZistreamDB& inDB);

  protected:
    //! Change the signature string
    /*!
      This should NOT be called directly by a user. This is for
      internal use only.
    */
    void putName(const char* name);

  private:
    class Helper;
    Helper* mHelper;
    UtString* mSignature;

    // forbid
    Signature(const Signature&);
    Signature& operator=(const Signature&);
  };

  //! Iterator type for Signature objects
  typedef Iter<Signature*> SigIter;

  //! Get the customer signature
  const Signature* getCustomerSignature() const;
  //! Get the customer signature
  Signature* getCustomerSignature();
  //! Set the customer signature
  Signature* setCustomerSignature(const char* cust_key, UtString* reason);

  //! Write the customer licensing info to a db file
  bool dbWrite(ZostreamDB& outDB) const;

  //! Read the customer licensing info from a db file
  bool dbRead(ZistreamDB& inDB);
  
protected:
  //! Signature class has access.
  friend class Signature;
  
  //! Helper for setting internal variables
// struct UtCustomerDB::SettingHelper;

  //! The parser and dbRead fill in this structure
  CustDBHelper* mDBHelper;

private:
  UtCustomerDB(const UtCustomerDB&);
  UtCustomerDB& operator=(const UtCustomerDB&);
};

#endif
