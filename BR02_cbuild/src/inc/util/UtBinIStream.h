// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtFileBuf.h"
#include "util/OSWrapper.h"

// O_ and S_ flags
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> 

// Author: Mark Seneski

class UtBinIStream;

/*
  Callback handler for the BinIStream class.
*/
class UtBinIStreamCB
{
public:
  CARBONMEM_OVERRIDES
  
  UtBinIStreamCB()
  {}

  virtual ~UtBinIStreamCB() {}
  
  //! Gets called as soon as the actual eof is reached.
  virtual void reportEOF() {}
  //! Gets called as soon as an error occurs
  virtual void reportError(const char* /*errMsg*/) {}
};

/*
  Very fast inlined binary stream reader. This is used by the Replay
  engine to read the database without any virtual functions. Virtual
  functions only happen when the callback mechanism gets invoked.
*/
class UtBinIStream
{
  inline bool canRead(UInt32 len)
  {
    return ((mBufferIndex + len) <= mBufferSize);
  }
  
  inline void incrBuffer(UInt32 len)
  {
    mBuffer += len;
    mBufferIndex += len;
  }
  
public:
  CARBONMEM_OVERRIDES
  
  static const UInt32 cBufSize = 64 * 1024;
  
  UtBinIStream(const char* fileName, UtBinIStreamCB* streamCB) :
    mFilename(fileName),
    mFileBuf(cBufSize),
    mBuffer(NULL),
    mBufferSize(0),
    mBufferIndex(0),
    mFD(-1),
    mCallback(streamCB),
    mEOF(false),
    mBad(true),
    mStop(false)
  {
    mBuffer = mFileBuf.getBufferAll(&mBufferSize);
    open();
  }
  
  ~UtBinIStream()
  {
    close();
  }

  //! Stop the stream if stop is true
  /*!
    Useful for code that computes whether or not the stream needs to
    stop.
  */
  void maybeStop(bool stop)
  {
    if (stop)
      setStop();
  }
  
  //! Stop the stream.
  void setStop()
  {
    mStop = true;
    resetFileBuf();
  }
  
  const char* getFilename() const { return mFilename.c_str(); }

  //! Returns false if the file could not be closed
  bool close() { 
    bool ret = false;
    if (mFD != -1)
    {
      UtString errmsg;
      ret = OSSysClose(mFD, &errmsg) == 0;
      if (! ret)
        reportError(errmsg.c_str());
      mFD = -1;
    }
    return ret;
  }
  
  //! Returns true if the file is in a bad state.
  inline bool fail() const { return mBad; }
  //! Returns true if the buffer and file are at EOF.
  inline bool eof() const { return mEOF; }
  const char* getErrmsg() { return mErrmsg.c_str(); }

  //! Read len bytes into buf
  inline void read(char* buf, UInt32 len)
  {
    if (canRead(len))
    {
      carbon_duffcpy(buf, buf + len, mBuffer);
      incrBuffer(len);
    }
    else
    {
      const UInt32 left = mBufferSize - mBufferIndex;
      carbon_duffcpy(buf, buf + left, mBuffer);
      buf += left;
      
      fillBuffer();
      if (! mStop)
        read(buf, len - left);
    }
  }
  
  //! Read 1 byte into buf.
  void readChar(char* buf)
  {
    /*
      Many performance tests were done on this and many different
      ways of representing this logic were tried, including moving
      the if (! mStop) into the if above it (and returning if it
      were true). Also, optimistic branching was attempted. Nothing
      was faster than the current implementation. I believe that gcc
      does some sort of clever arithmetic trick and prepares the
      pipeline with the right branch predictions.
    */
    if (mBufferIndex == mBufferSize) 
      fillBuffer();
    if (! mStop)
    {
      *buf = *mBuffer;
      incrBuffer(1);
    }
  }
  
  //! Peek at the next byte without incrementing the buffer
  char peekChar()
  {
    if (mBufferIndex == mBufferSize) 
      fillBuffer();
    if (! mStop)
    {
      return *mBuffer;
    }
    return '\0';
  }
  
  //! Read a UInt32
  inline void readUInt32(UInt32* num)
  {
    char* buf = reinterpret_cast<char*>(num);
    read(buf, sizeof(UInt32));
  }
  
  //! Read a UInt64
  inline void readUInt64(UInt64* num)
  {
    char* buf = reinterpret_cast<char*>(num);
    read(buf, sizeof(UInt64));
  }
  
  //! Returns true if the stream is stopped.
  /*!
    The stream is stopped if eof() or bad() or if stop was explicitly
    set.
  */
  inline unsigned isStopped() const { return mStop; }
  
private:
  UtString mFilename;
  UtString mErrmsg;
  UtFileBuf mFileBuf;
  char* mBuffer;
  UInt32 mBufferSize;
  UInt32 mBufferIndex;
  int mFD;
  UtBinIStreamCB* mCallback;
  bool mEOF;
  bool mBad;
  bool mStop;

  void resetFileBuf()
  {
    mBufferSize = 0;
    mBufferIndex = 0;
  }

  void reportError(const char* errmsg)
  {
    mErrmsg.clear();
    mErrmsg << errmsg;
    mBad = true;
    mStop = true;
    resetFileBuf();
    mCallback->reportError(errmsg);
  }
  
  //! Fill the buffer with data from the file.
  /*!
    isStopped() will be true if the buffer cannot get more data.
  */
  void fillBuffer()
  {
    if (! mStop)
    {
      resetFileBuf();
        
      mBuffer = mFileBuf.getBufferAll(&mBufferSize);
      UtString errMsg;
      int sysRead = OSSysRead(mFD, mBuffer, mFileBuf.capacity(), &errMsg);
      if (sysRead < 0)
      {
        reportError(errMsg.c_str());
      }
      else if (sysRead == 0)
      {
        mEOF = true;
        mStop = true;
        mCallback->reportEOF();
      }
      else
      {
        mFileBuf.putWriteIndex(sysRead);
        mBufferSize = sysRead;
      }
    }
  }
  
  void open()
  {
    mBad = false;
    mStop = false;
    mEOF = false;
    INFO_ASSERT(mFD == -1, mFilename.c_str());
    int flags = O_RDONLY | O_BINARY;
    UtString errmsg;
    mFD = OSSysOpen(mFilename.c_str(), flags, 
                    S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH,
                    &errmsg);
    if (mFD == -1)
      reportError(errmsg.c_str());
  }
};
