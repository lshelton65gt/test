// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __UtStringUtil_h_
#define __UtStringUtil_h_

#ifndef __carbon_shelltypes_h_
#include "shell/carbon_shelltypes.h"
#endif

#ifndef __UtString_h_
#include "util/UtString.h"
#endif

// For the moment, this header must be included by UtString.h -- there
// is no point including it directly.  We should change this so that this
// header is not needed by all users of UtString.h, and it should be included
// only by .cxx files that need it

#include <cstring>		// Get strchr()
#include <cctype>               // for tolower

#if pfSPARCWORKS
extern "C" int strcasecmp (const char*, const char*);
#endif

//! StringUtil class -- a namespace for general pupose string functions
class StringUtil
{
public:
  CARBONMEM_OVERRIDES
  //! Allocate (using Carbon allocator) a string of numBytes bytes
  static char* alloc(size_t numBytes);

  //! Allocate (using Carbon allocator) and duplicate a zero-terminated string
  //! of strlen(s)+1 bytes
  static char* dup(const char* s);

  //! Allocate (using Carbon allocator) and duplicate an array of N characters.
  //! Zeros aren't special.
  static char* dup(const char* s, size_t numChars);

  //! Free a string allocated with alloc() or dup().  Assumes the array is one
  //! greater than strlen(s).
  static void free(char* s);

  //! Free a string allocated with alloc() or dup().  Don't forget to include
  //! any terminating null character in the count.
  static void free(char* s, size_t numBytes);


  //! Walk a character pointer past any delimiters at the beginning
  static const char* skip(const char* s, const char* delims = " \t\n\r");

  //! Walk a character pointer past any delimiters at the beginning
  static const char* skip(const char* s, size_t sz,
                          const char* delims = " \t\n\r");

  //! Strip leading and trailing delimiters from string 
  static void strip(UtString* str, const char* delims = " \t\n\r");

  //! parse the current token as a based number, returning true for success
  static bool parseNumber(const char* str, UInt32* number, int base = 10);

  //! parse the current token as a based number, returning true for success
  static bool parseNumber(const char* str, SInt32* number, int base = 10);

  //! parse the current token as a based number, returning true for success
  static bool parseNumber(const char* str, double* number);

  //! Format an integer into the specified # columns, using K/M/G notation
  static void formatInt(SInt32 num, UtString* buf, int numCols);

  //! Format a big integer into the specified # columns, using K/M/G notation
  static void formatInt(SInt64 num, UtString* buf, int numCols);

  //! Format an integer into the specified # columns, using K/M/G notation
  static void formatInt(UInt32 num, UtString* buf, int numCols);

  //! Format a big integer into the specified # columns, using K/M/G notation
  static void formatInt(UInt64 num, UtString* buf, int numCols);

  //! Hash a char*
  static size_t hash(const char* str) {return UtString::hash(str);}
  
  //! Hash a char* in a case-insensitive fashion, e.g. Foo and FOO hash the same
  static size_t hashInsensitive(const char* str)
  {
    size_t h = 0; 
    for ( ; *str; ++str) {
      size_t c = (size_t) (std::tolower(*str));
      h = 10*h + c;
    }
    return h;
  }
  

  //! Removes backslashes in a string read from a file
  static void deSlashify(UtString* buf);

  //! Find the first occurrence of a non alphanumeric character
  /*!
    \param name Null-terminated character array. Ignored if NULL, in
    which case NULL is also returned.
    \returns The pointer to the first occurrence of a non-alphanumeric
    character or NULL if one isn't found.
  */
  static const char* findFirstNonAlphanumeric(const char* name);

  //! Compute a string that represents the input as a C string literal.
  //! Basically, escape backslashes and quotes.
  static void escapeCString(const char* in, UtString* out, char newlineCode='n');

  //! Remove invalid symbol characters and replace them with $
  static void makeIdent(UtString* buf);
  
#define UTSTRINGUTIL_ENCRYPT_FUNC troffBuffer
  //! Encrypt given input string into buffer.
  /*!
    This \e cannot be called directly. Include Zstream.h and use the
    defined macro STRINGUTIL_ENCRYPT. 
  */
  static bool UTSTRINGUTIL_ENCRYPT_FUNC(const char *in, size_t len, UtString* out, void*);

#define UTSTRINGUTIL_DECRYPT_FUNC scanAsciiBuffer
  //! Decrypt given input string into buffer.
  /*!
    This \e cannot be called directly. Include Zstream.h and use the
    defined macro STRINGUTIL_DECRYPT. 
  */
  static bool UTSTRINGUTIL_DECRYPT_FUNC(const char *in, size_t len, UtString* out, void*);

  //! Convert given buffer (which may have binary characters)
  //! into a 7-bit clean representation.
  static void asciiEncode(const char *in, size_t len, UtString* out);

  //! Convert given ASCII-encoded binary string to binary form.
  //! This is the inverse of asciiEncode().
  static bool asciiDecode(const char *in, size_t len, UtString* out);

  //! Count the number of occurrences of a character c in string str
  static size_t count(const char* str, char c) {
     return UtString::count(str, c);
  }

  //! Return index of a char c in string str, or NULL if not found.
  static char *index(const char *str, char c);
};

struct StringHashInsensitive {
  CARBONMEM_OVERRIDES
  
  //! hash operator
  size_t hash(const char* var) const {
    return StringUtil::hashInsensitive(var);
  }

  //! equal function
  bool equal(const char* v1, const char* v2) const {
    return strcasecmp(v1, v2) == 0;
  }

  //! shallow less-than function -- for Microsoft hash tables
  bool lessThan1(const char* v1, const char* v2) const {
    return strcasecmp(v1, v2) < 0;
  }

  //! lessThan operator -- for sorted iterations
  bool lessThan(const char* v1, const char* v2) const {
    return strcasecmp(v1, v2) < 0;
  }
};


//! class StrToken -- iterates through a string yielding tokens
/*! Note that for many purposes, boost/tokenizer.hpp is much more powerful
 *! and is standardized.  It lacks a method, though, of retrieving the current
 *! character pointer, which is useful when tokenizing non-uniformly inside a
 *! line.  
 */
class StrToken
{
public: CARBONMEM_OVERRIDES
  typedef const char* value_type;

  //! constructor
  StrToken(const char* str, const char* delims = " \t\n\r");

  //! have we come to the end of the string?
  bool atEnd() const {return mNextTok == NULL;}

  //! get the current token
  const char* operator*() const;

  //! parse the current token as a based number, returning true for success
  bool parseNumber(UInt32* number, int base = 10)
    const
  {
    return StringUtil::parseNumber(**this, number, base);
  }

  //! parse the current token as a based number, returning true for success
  bool parseNumber(SInt32* number, int base = 10)
    const
  {
    return !atEnd() && StringUtil::parseNumber(**this, number, base);
  }

  //! advance to the next token
  StrToken&  operator++();

  //! change the set of delimeters
  void putDelim(const char* delims) {mDelims = delims;}

  // check for the end, get the current token, advance to the next one
  bool operator()(const char** ptr);
        
  //! get the current position in the string
  const char* curPos() {return mStr;}

  //! get the current position in the string
  char curDelim() {return *mNextTok;}

private:
  StrToken (const StrToken&);
  StrToken& operator=(const StrToken&);

  const char* mStr;
  const char* mNextTok;
  UtString mDelims;     // could make this a 256-element bool array for speed...
  mutable UtString mBuf;
};

//! Helper class for null-terminated argv arrays
/*! This class help programmatically build up argv arrays for
    passing to a routine that expects to see an argc/argv array.
*/
class UtStringArgv
{
public: CARBONMEM_OVERRIDES
  //! construct an empty argv array
  UtStringArgv();

  //! construct a copy of an argv array
  UtStringArgv(int argc, char** argv);

  //! copy ctor
  UtStringArgv(const UtStringArgv&);

  //! assign operator
  UtStringArgv& operator=(const UtStringArgv&);

  // join two UtStringArgv
  UtStringArgv& operator+(const UtStringArgv&);

  //! destructor
  ~UtStringArgv();

  //! append a new string to the argv array, maintaining null termination
  void push_back(const char*);

  //! replace the specified argument
  void replace(size_t index, const char* newVal);

  //! append a new string to the argv array, maintaining null termination
  void push_back(const UtString&);

  //! Get access to the argv array for standard command-line parsers.
  /*! It is OK to remove elements of the argv array.  After doing so,
   *! please call putArgc with the new, smaller size
   */
  char** getArgv();

  //! Get the number of strings in the argv array, not including null terminator
  int getArgc() const;

  //! Correct the number of strings after having mutated getArgv().
  void putArgc(int newArgc);

private:
  // reference array of storage chars
  typedef UtArray<char*> CharArray;
  CharArray* mArgv;
  UtStringArray* mStorage;
}; // class UtStringArgv

#endif
