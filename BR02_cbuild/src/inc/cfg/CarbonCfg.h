//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2011-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _CARBON_CFG_CXX_H_
#define _CARBON_CFG_CXX_H_

#include "util/CarbonPlatform.h"
#include "cfg/carbon_cfg.h"

#include <QObject>
#include <QtCore>
#include <QtScript>
#include <QtXml>
#include <QFile>

class CfgScriptingEngine;
class CarbonCfgErrorHandler;

QScriptValue ExpandFormatStringFunction(QScriptContext *ctx, QScriptEngine *eng);

class CcfgFlags : public QObject
{
  Q_OBJECT
  Q_CLASSINFO("FlagsPrefix", "CcfgFlags");
  Q_CLASSINFO("EnumPrefix", "CcfgFlags");

public:
  enum CarbonCfgMemoryEditFlag
  {                                             
    EditMemoryAll                             = 0x1fffffff,
    EditMemoryNone                            = 0,
    EditMemoryName                            = 0x1,
    EditMemoryWidth                           = 0x2,
    EditMemorySystemAddressMapping                  = 0x4,
    EditMemorySystemAddressESLPortName       = 0x8,
    EditMemorySystemAddressESLPortBaseAddress        = 0x10,
    EditMemoryInitializationReadmemFormat     = 0x20,
    EditMemoryInitializationReadmemFile       = 0x40,
    EditMemoryBlockName                       = 0x80, 
   
    EditMemoryMaxAddress                      = 0x100,
    EditMemoryDeletions                       = 0x200,
    EditMemoryAdditions                       = 0x400,

    EditMemoryBlockBaseAddress                = 0x800,
    EditMemoryBlockSize                       = 0x1000,
    EditMemoryBlockLocationAdditions          = 0x2000,
    EditMemoryBlockLocationDeletions          = 0x4000,
    EditMemoryBlockLocationRTLPath            = 0x8000,

    EditMemoryBlockLocationDisplayStartWordOffset = 0x10000,
    EditMemoryBlockLocationDisplayEndWordOffset   = 0x20000,
    EditMemoryBlockLocationDisplayStartLSB        = 0x40000,
    EditMemoryBlockLocationDisplayStartMSB        = 0x80000,
    
    EditMemoryBlockLocationRTLStartWordOffset     = 0x100000,
    EditMemoryBlockLocationRTLEndWordOffset       = 0x200000,
    EditMemoryBlockLocationRTLStartLSB            = 0x400000,
    EditMemoryBlockLocationRTLStartMSB            = 0x800000,

    EditMemoryBlockAdditions                      = 0x1000000,
    EditMemoryBlockDeletions                      = 0x2000000,

    EditMemoryCustomCodes                        = 0x4000000,
    EditMemoryBlockCustomCodes                   = 0x8000000,
  };

  Q_DECLARE_FLAGS(CarbonCfgMemoryEditFlags, CarbonCfgMemoryEditFlag)
  Q_FLAGS(CarbonCfgMemoryEditFlags);
  
  enum CarbonCfgComponentEditFlag
  {
    EditComponentAll                       = 0xff,
    EditComponentNone                      = 0,
    EditComponentName                      = 0x1,
    EditComponentAdditions                 = 0x2,
    EditComponentDeletions                 = 0x4,
    EditComponentDisassembler              = 0x8,
    EditComponentCustomCodes               = 0x10
  };

  Q_DECLARE_FLAGS(CarbonCfgComponentEditFlags, CarbonCfgComponentEditFlag)
  Q_FLAGS(CarbonCfgComponentEditFlags);

};

class CcfgEnum : public QObject
{
  Q_OBJECT
  Q_CLASSINFO("EnumPrefix", "CcfgEnum");

public:

#ifndef SWIG
  enum CarbonCfgElementGenerationType
  {
    User=eCarbonCfgElementUser,
    Generated=eCarbonCfgElementGenerated
  }
  Q_ENUMS(CarbonCfgElementGenerationType);


  enum CarbonCfgWaveType
  {
    WaveNone=eCarbonCfgNone,
    WaveVCD=eCarbonCfgVCD,
    WaveFSDB=eCarbonCfgFSDB
  };
  Q_ENUMS(CarbonCfgWaveType);

  enum CarbonCfgMemLocType 
  {
    MemLocRTL=eCfgMemLocRTL,
    MemLocPort=eCfgMemLocPort,
    MemLocUser=eCfgMemLocUser
  };
  Q_ENUMS(CarbonCfgMemLocType);

  enum CarbonCfgReadmemType 
  {
    Readmemh=eCarbonCfgReadmemh,
    Readmemb=eCarbonCfgReadmemb
  };
  Q_ENUMS(CarbonCfgReadmemType);

  enum CarbonCfgMemInitType 
  { 
    MemInitNone=eCarbonCfgMemInitNone,       
    MemInitProgPreload=eCarbonCfgMemInitProgPreload,
    MemInitReadmem=eCarbonCfgMemInitReadmem      
  };
  Q_ENUMS(CarbonCfgMemInitType);

  enum CarbonCfgRegisterLocKind
  {
    RegLocConstant=eCfgRegLocConstant,  
    RegLocRegister=eCfgRegLocRegister, 
    RegLocArray=eCfgRegLocArray, 
    RegLocUser=eCfgRegLocUser 
  };
  Q_ENUMS(CarbonCfgRegisterLocKind);

  enum CarbonCfgRegAccessType
  {
    RW=eCfgRegAccessRW,      
    RO=eCfgRegAccessRO,      
    WO=eCfgRegAccessWO      
  };
  Q_ENUMS(CarbonCfgRegAccessType);

  enum CarbonCfgRadix
  {
    BooleanActiveHigh=eCarbonCfgRadixBooleanActiveHigh,
    BooleanActiveLow=eCarbonCfgRadixBooleanActiveLow,
    Hex=eCarbonCfgRadixHex,
    SignedInt=eCarbonCfgRadixSignedInt,
    UnsignedInt=eCarbonCfgRadixUnsignedInt,
    Float=eCarbonCfgRadixFloat,
    String=eCarbonCfgRadixString
  }
  Q_ENUMS(CarbonCfgRadix);

  enum CarbonCfgRTLPortType
  {
    Input=eCarbonCfgRTLInput,
    Output=eCarbonCfgRTLOutput,
    InOut=eCarbonCfgRTLInout
  };
  Q_ENUMS(CarbonCfgRTLPortType);

  enum CarbonCfgRTLConnectionType
  { 
    ClockGen = eCarbonCfgClockGen,
    ResetGen = eCarbonCfgResetGen,
    Tie = eCarbonCfgTie,
    TieParam = eCarbonCfgTieParam,
    ESLPort = eCarbonCfgESLPort,
    XtorConn = eCarbonCfgXtorConn
  };
  Q_ENUMS(CarbonCfgRTLConnectionType);

  enum CarbonCfgStatus
  {
    Success=eCarbonCfgSuccess,
    Failure=eCarbonCfgFailure,
    Inconsistent=eCarbonCfgInconsistent
  }
  Q_ENUMS(CarbonCfgStatus);

  enum CarbonCfgMode
  { 
    Undefined = eCarbonCfgUNDEFINED,
    Arm = eCarbonCfgARM,
    CoWare = eCarbonCfgCOWARE,
    SystemC = eCarbonCfgSYSTEMC
  };
  Q_ENUMS(CarbonCfgMode);

  enum CarbonCfgParamDataType
  {
    XtorBool=eCarbonCfgXtorBool,
    XtorUInt32=eCarbonCfgXtorUInt32,
    XtorDouble=eCarbonCfgXtorDouble,
    XtorString=eCarbonCfgXtorString,
    XtorUInt64=eCarbonCfgXtorUInt64
  };
  Q_ENUMS(CarbonCfgParamDataType);

  enum CarbonCfgParamFlag
  {
    Runtime=eCarbonCfgXtorParamRuntime,
    Compile=eCarbonCfgXtorParamCompile,
    Both=eCarbonCfgXtorParamBoth,
    Init=eCarbonCfgXtorParamInit
  };
  Q_ENUMS(CarbonCfgParamFlag);

  enum CarbonCfgESLPortType
  {
    ESLUndefined=eCarbonCfgESLUndefined,
    ESLInput=eCarbonCfgESLInput,
    ESLOutput=eCarbonCfgESLOutput,
    ESLInout=eCarbonCfgESLInout
  };
  Q_ENUMS(CarbonCfgESLPortType);

  enum CarbonCfgESLPortMode
  {
    ESLPortUndefined=eCarbonCfgESLPortUndefined,
    ESLPortControl=eCarbonCfgESLPortControl,
    ESLPortClock=eCarbonCfgESLPortClock,
    ESLPortReset=eCarbonCfgESLPortReset
  };
  Q_ENUMS(CarbonCfgESLPortMode);

  enum CarbonCfgCustomCodePosition
  {
    Pre=eCarbonCfgPre,
    Post=eCarbonCfgPost
  };
  Q_ENUMS(CarbonCfgCustomCodePosition);

  // if you make a change here you must also change gCarbonCfgCompCustomCodeSection (in src/cfg/carbon_cfg.cxx)
  // and the definition of enum CarbonCfgCompCustomCodeSection (in src/inc/cfg/carbon_cfg.h)
  enum CarbonCfgCompCustomCodeSection
  {
    CompCppInclude=eCarbonCfgCompCppInclude,
    CompHInclude=eCarbonCfgCompHInclude,
    CompClass=eCarbonCfgCompClass,
    CompConstructor=eCarbonCfgCompConstructor,
    CompDestructor=eCarbonCfgCompDestructor,
    CompCommunicate=eCarbonCfgCompCommunicate,
    CompInterconnect=eCarbonCfgCompInterconnect,
    CompUpdate=eCarbonCfgCompUpdate,
    CompInit=eCarbonCfgCompInit,
    CompReset=eCarbonCfgCompReset,
    CompTerminate=eCarbonCfgCompTerminate,
    CompSetParameter=eCarbonCfgCompSetParameter,
    CompGetParameter=eCarbonCfgCompGetParameter,
    CompGetProperty=eCarbonCfgCompGetProperty,
    CompDebugTransaction=eCarbonCfgCompDebugTransaction,
    CompDebugAccess=eCarbonCfgCompDebugAccess,
    ProcStopAtDebuggablePoint=eCarbonCfgCompProcStopAtDebuggablePoint,
    ProcCanStop=eCarbonCfgCompProcCanStop,
    CompSaveData=eCarbonCfgCompSaveData,
    CompRestoreData=eCarbonCfgCompRestoreData,
    CompGetCB=eCarbonCfgCompGetCB,
    CompSaveStreamType=eCarbonCfgCompSaveStreamType,
    CompSaveDataArch=eCarbonCfgCompSaveDataArch,
    CompRestoreDataArch=eCarbonCfgCompRestoreDataArch,
    CompSaveDataBinary=eCarbonCfgCompSaveDataBinary,
    CompRestoreDataBinary=eCarbonCfgCompRestoreDataBinary,

    // These sections are for stand-alone components.
    CompLoadFile=eCarbonCfgCompLoadFile,
    CompStopAtDebuggablePoint=eCarbonCfgCompStopAtDebuggablePoint,
    CompCanStop=eCarbonCfgCompCanStop,
    CompRegisterDebugAccessCB=eCarbonCfgCompRegisterDebugAccessCB,
    CompRetrieveDebugAccessIF=eCarbonCfgCompRetrieveDebugAccessIF,
    CompDebugMemRead=eCarbonCfgCompDebugMemRead,
    CompDebugMemWrite=eCarbonCfgCompDebugMemWrite
  };
  Q_ENUMS(CarbonCfgCompCustomCodeSection);

  enum CarbonCfgRegCustomCodeSection
  {
    RegClass=eCarbonCfgRegClass,
    RegConstructor=eCarbonCfgRegConstructor,
    RegDestructor=eCarbonCfgRegDestructor,
    RegRead=eCarbonCfgRegRead,
    RegWrite=eCarbonCfgRegWrite,
    RegReset=eCarbonCfgRegReset
  };
  Q_ENUMS(CarbonCfgRegCustomCodeSection);

  enum CarbonCfgCadiCustomCodeSection
  {
    CadiCppInclude=eCarbonCfgCadiCppInclude,
    CadiHInclude=eCarbonCfgCadiHInclude,
    CadiClass=eCarbonCfgCadiClass,
    CadiConstructor=eCarbonCfgCadiConstructor,
    CadiDestructor=eCarbonCfgCadiDestructor,
    CadiCADIRegGetGroups=eCarbonCfgCadiCADIRegGetGroups,
    CadiCADIRegGetMap=eCarbonCfgCadiCADIRegGetMap,
    CadiCADIRegRead=eCarbonCfgCadiCADIRegRead,
    CadiCADIRegWrite=eCarbonCfgCadiCADIRegWrite,
    CadiCADIGetPC=eCarbonCfgCadiCADIGetPC,
    CadiCADIGetDisassembler=eCarbonCfgCadiCADIGetDisassembler,
    CadiCADIXfaceBypass=eCarbonCfgCadiCADIXfaceBypass,
    CadiCADIMemGetSpaces=eCarbonCfgCadiCADIMemGetSpaces,
    CadiCADIMemGetBlocks=eCarbonCfgCadiCADIMemGetBlocks,
    CadiCADIMemWrite=eCarbonCfgCadiCADIMemWrite,
    CadiCADIMemRead=eCarbonCfgCadiCADIMemRead,
    CadiCADIGetCommitedPCs=eCarbonCfgCadiCADIGetCommitedPCs
  };
  Q_ENUMS(CarbonCfgCadiCustomCodeSection);

  enum CarbonCfgMemoryBlockCustomCodeSection
  {
    MemoryBlockClass=eCarbonCfgMemoryBlockClass,
    MemoryBlockConstructor=eCarbonCfgMemoryBlockConstructor,
    MemoryBlockDestructor=eCarbonCfgMemoryBlockDestructor,
    MemoryBlockRead=eCarbonCfgMemoryBlockRead,
    MemoryBlockWrite=eCarbonCfgMemoryBlockWrite
  };
  Q_ENUMS(CarbonCfgMemoryBlockCustomCodeSection);

  enum CarbonCfgMemoryCustomCodeSection
  {
    MemoryClass=eCarbonCfgMemoryClass,
    MemoryConstructor=eCarbonCfgMemoryConstructor,
    MemoryDestructor=eCarbonCfgMemoryDestructor,
    MemoryRead=eCarbonCfgMemoryRead,
    MemoryWrite=eCarbonCfgMemoryWrite
  };
  Q_ENUMS(CarbonCfgMemoryCustomCodeSection);
#endif
};

template <typename Tp>
QScriptValue qScriptValueFromQObject(QScriptEngine *engine, Tp const
&qobject)
{
    return engine->newQObject(qobject);
}

template <typename Tp>
void qScriptValueToQObject(const QScriptValue &value, Tp &qobject)
{   
    qobject = qobject_cast<Tp>(value.toQObject());
}

template <typename Tp>
int qScriptRegisterQObjectMetaType(
    QScriptEngine *engine,
    const QScriptValue &prototype = QScriptValue()
#ifndef qdoc
    , Tp * /* dummy */ = 0
#endif
    )
{
    return qScriptRegisterMetaType<Tp>(engine, qScriptValueFromQObject,
                                       qScriptValueToQObject, prototype);
}

template <typename Tp>
QScriptValue qScriptValueFromEnum(QScriptEngine *engine, Tp const
&qobject)
{
  return QScriptValue(engine, (int)qobject);
}

template <typename Tp>
void qScriptValueToEnum(const QScriptValue &value, Tp &qobject)
{   
  qobject = static_cast<Tp>(value.toInt32());
}

template <typename Tp>
int qScriptRegisterEnumMetaType(
    QScriptEngine *engine,
    const QScriptValue &prototype = QScriptValue()
#ifndef qdoc
    , Tp * /* dummy */ = 0
#endif
    )
{
    return qScriptRegisterMetaType<Tp>(engine, qScriptValueFromEnum,
                                       qScriptValueToEnum, prototype);
}

#include "util/Util.h"
#include "util/UtString.h"
#include "util/UtHashSet.h"
#include "util/UtHashMap.h"
#include "cfg/carbon_cfg.h"
#include "util/UtIOEnum.h"
#include "shell/carbon_dbapi.h"
#include "util/DynBitVector.h"
#include "util/UtStringArray.h"
#include "shell/ShellNetMacroDefine.h"

#ifndef SWIG
UtOStream& operator<<(UtOStream&, const QString&);
UtString& operator<<(UtString&, const QString&);
#endif

#define carbonCfgMESSAGE_BASE 13000
#define carbonCfgMESSAGE_LAST 13999

#define carbonCfgERR_NOCARBONSIGNAL carbonCfgMESSAGE_BASE+0
#define carbonCfgERR_NOCARBONPORT carbonCfgMESSAGE_BASE+1
#define carbonCfgERR_NOTRANSPORT carbonCfgMESSAGE_BASE+2
#define carbonCfgERR_NEWCARBONPORT carbonCfgMESSAGE_BASE+3
#define carbonCfgERR_PORTCHANGED carbonCfgMESSAGE_BASE+4
#define carbonCfgERR_NOTCARBONNET carbonCfgMESSAGE_BASE+5
#define carbonCfgERR_NOTMEMORY carbonCfgMESSAGE_BASE+6
#define carbonCfgERR_COMPOSITEPORT carbonCfgMESSAGE_BASE+7
#define carbonCfgERR_NODB carbonCfgMESSAGE_BASE+8
#define carbonCfgERR_NOXTORCONN carbonCfgMESSAGE_BASE+9
#define carbonCfgERR_NOFLOWSUPPORT carbonCfgMESSAGE_BASE+10
#define carbonCfgERR_REQRTLCONN carbonCfgMESSAGE_BASE+11
#define carbonCfgERR_UNKNOWNXTOR carbonCfgMESSAGE_BASE+12
#define carbonCfgERR_ILLPARAM carbonCfgMESSAGE_BASE+13
#define carbonCfgERR_NOMXHOME carbonCfgMESSAGE_BASE+14
#define carbonCfgERR_PARSEERR carbonCfgMESSAGE_BASE+15
#define carbonCfgERR_MULTDISASS carbonCfgMESSAGE_BASE+16
#define carbonCfgERR_INVPARAMVAL carbonCfgMESSAGE_BASE+17

#define CARBON_DEFAULT_XTOR_LIB "CarbonDefault"

struct CarbonCfg;
struct CarbonCfgRTLConnection;
struct CarbonCfgXtorConn;
struct CarbonCfgClockGen;
struct CarbonCfgResetGen;
struct CarbonCfgTieGen;
struct CarbonCfgTieParam;
struct CarbonCfgESLPort;
struct CarbonCfgGroup;
struct CarbonCfgXtorParamInst;
class CarbonCfgSystemCClock;

#ifdef CARBON_PV
class PVContext;
class CarbonContext;
#endif


class UtParamFile;
struct CarbonCfgRegisterLocConstant;
struct CarbonCfgRegisterLocRTL;
struct CarbonCfgRegisterLocReg;
struct CarbonCfgRegisterLocArray;
struct CarbonCfgRegisterLocUser;
struct CarbonCfgMemoryLocRTL;
struct CarbonCfgMemoryLocPort;
struct CarbonCfgMemoryLocUser;
class CarbonCfgCompCustomCode;
class CarbonCfgCadiCustomCode;
class CarbonCfgMemoryCustomCode;
class CarbonCfgRegCustomCode;
class CarbonCfgMemoryBlockCustomCode;
class CfgXmlWriter;
class CarbonCfgXtorClassTemplateArg;
class CarbonCfgXtorPortWidthTemplateArg;
class CarbonCfgXtorTextTemplateArg;
class CfgScriptingCarbonDB;


#ifndef NO_EXTERN_STRINGS
extern const char* gCarbonRTLPortTypes[];
extern const char* gCarbonSystemCPortTypes[];
extern const char* gCarbonESLPortTypes[];
extern const char* gCarbonXtorDataTypes[];
extern const char* gCarbonXtorParamFlag[];
extern const char* gCarbonMaxsimRadix[];
extern const char* gCarbonRTLConnectionTypes[];
extern const char* gCarbonCfgColors[];
extern const char* gCarbonCfgReadmemTypes[];
extern const char* gCarbonCfgMemInitTypes[];
extern const char* gCarbonCfgFeature[];
extern const char* gCarbonCfgRegAccessTypes[];
extern const char* gCarbonCfgRegAccessShortTypes[];
extern const char* gCarbonCfgRegLocTypes[];
extern const char* gCarbonCfgMemLocTypes[];
extern const char* gCarbonESLPortModeTypes[];
extern const char *gCarbonCCFGModes[];
extern const char *gCarbonCfgEndianess[];
extern const char *gCarbonCfgCompCustomCodeSection[];
extern const char *gCarbonCfgCadiCustomCodeSection[];
extern const char *gCarbonCfgRegCustomCodeSection[];
extern const char *gCarbonCfgMemoryBlockCustomCodeSection[];
extern const char *gCarbonCfgMemoryCustomCodeSection[];
extern const char *gCarbonCfgSystemCTimeUnits[];
extern const char* gCarbonCfgElementGenerationType[];

#endif



typedef UtIOEnum<CarbonCfgRTLPortType,
                 gCarbonRTLPortTypes> CarbonCfgRTLPortTypeIO;
typedef UtIOEnum<CarbonCfgSystemCPortType,
                 gCarbonSystemCPortTypes> CarbonCfgSystemCPortTypeIO;
typedef UtIOEnum<CarbonCfgESLPortType,
                 gCarbonESLPortTypes> CarbonCfgESLPortTypeIO;
typedef UtIOEnum<CarbonCfgRadix,
                 gCarbonMaxsimRadix> CarbonCfgRadixIO;
typedef UtIOEnum<CarbonCfgParamDataType,
                 gCarbonXtorDataTypes> CarbonCfgParamDataTypeIO;
typedef UtIOEnum<CarbonCfgParamFlag,
                 gCarbonXtorParamFlag> CarbonCfgParamFlagIO;
typedef UtIOEnum<CarbonCfgRTLConnectionType,
                 gCarbonRTLConnectionTypes> CarbonCfgRTLConnectionTypeIO;
typedef UtIOEnum<CarbonCfgColor, gCarbonCfgColors> CarbonCfgColorIO;
typedef UtIOEnum<CarbonCfgReadmemType,
                 gCarbonCfgReadmemTypes> CarbonCfgReadmemTypeIO;
typedef UtIOEnum<CarbonCfgMemInitType,
                 gCarbonCfgMemInitTypes> CarbonCfgMemInitTypeIO;
typedef UtIOEnum<CarbonCfgFeature, gCarbonCfgFeature> CarbonCfgFeatureIO;
typedef UtIOEnum<CarbonCfgRegAccessType,
                 gCarbonCfgRegAccessTypes> CarbonCfgRegAccessTypeIO;
typedef UtIOEnum<CarbonCfgRegLocType,
                 gCarbonCfgRegLocTypes> CarbonCfgRegLocTypeIO;
typedef UtIOEnum<CarbonCfgMemLocType,
                 gCarbonCfgMemLocTypes> CarbonCfgMemLocTypeIO;
typedef UtIOEnum<CarbonCfgESLPortMode,
                 gCarbonESLPortModeTypes> CarbonCfgESLPortModeIO;
typedef UtIOEnum<CarbonCfgMode,
                 gCarbonCCFGModes> CarbonCfgModeIO;
typedef UtIOEnum<CarbonCfgEndianess,
                 gCarbonCfgEndianess> CarbonCfgEndianessIO;
typedef UtIOEnum<CarbonCfgCompCustomCodeSection,
                 gCarbonCfgCompCustomCodeSection> CarbonCfgCompCustomCodeSectionIO;
typedef UtIOEnum<CarbonCfgCadiCustomCodeSection,
                 gCarbonCfgCadiCustomCodeSection> CarbonCfgCadiCustomCodeSectionIO;
typedef UtIOEnum<CarbonCfgRegCustomCodeSection,
                 gCarbonCfgRegCustomCodeSection> CarbonCfgRegCustomCodeSectionIO;
typedef UtIOEnum<CarbonCfgMemoryBlockCustomCodeSection,
                 gCarbonCfgMemoryBlockCustomCodeSection> CarbonCfgMemoryBlockCustomCodeSectionIO;
typedef UtIOEnum<CarbonCfgMemoryCustomCodeSection,
                 gCarbonCfgMemoryCustomCodeSection> CarbonCfgMemoryCustomCodeSectionIO;
typedef UtIOEnum<CarbonCfgSystemCTimeUnits, gCarbonCfgSystemCTimeUnits> CarbonCfgSystemCTimeUnitsIO;
typedef UtIOEnum<CarbonCfgElementGenerationType, gCarbonCfgElementGenerationType> CarbonCfgElementGenerationTypeIO;


//! CarbonCfgClockFrequency class
/*!
  A clock frequency defined as a ratio of number of clock cycles in
  a number of component cycles.

  For example, a 2:1 frequency means there are two clock cycles in one
  component cycle.
 */
class CarbonCfgClockFrequency {

public:
  CARBONMEM_OVERRIDES

  CarbonCfgClockFrequency(unsigned int clockCycles,
                          unsigned int compCycles):
    mClockCycles(clockCycles), mCompCycles(compCycles) {}

  unsigned int getClockCycles() const { return mClockCycles; }
  void putClockCycles(unsigned int cycles) { mClockCycles = cycles; }

  unsigned int getCompCycles() const { return mCompCycles; }
  void putCompCycles(unsigned int cycles) { mCompCycles = cycles; }

  void read(UtIStream&);
  void read(QXmlStreamReader&);
  void write(CfgXmlWriter&);

private:
  UInt32 mClockCycles;
  UInt32 mCompCycles;
};

//! Class to manage error and warning message during a process
class CarbonCfgMsgHandler
{
public:
  //! Constructor
  CarbonCfgMsgHandler() : mWarnings(0), mErrors(0) {}

  //! Report a message
  /*! If file is NULL, no file is printed. If line is 0, no number is
   *  printed.
   */
  void reportMessage(CarbonMsgSeverity sev, const char* msg, 
                     const char* file = NULL, int lineno = 0);

  //! Put a message from another message facility (does not update counts)
  void putMsg(const char* msg);

  //! Clear any current messages
  void clear();

  //! Get the current buffered error message strings
  const char* getErrMsg() const { return mErrMsg.c_str(); }

  //! Get the number of warnings
  CarbonUInt32 getNumWarnings() const { return mWarnings; }

  //! Get the number of errors
  CarbonUInt32 getNumErrors() const { return mErrors; }

  //! Returns true if there are any messages
  bool hasMessages() const { return (mErrors != 0) || (mWarnings != 0); }

  //! Returns true if there is more than one error
  bool getFailure() const { return mErrors != 0; }

private:
  //! Storage for the current set of unprinted messages
  UtString mErrMsg;

  //! Keep track of the number of warnings reported
  CarbonUInt32 mWarnings;

  //! Keep track of the number of errors reported.
  CarbonUInt32 mErrors;
}; // class CarbonCfgMsgHandler

//! Class to replace variables with strings in a clone operation
/*! When creating components for a model kit, it is sometimes
 *  necessary to create multiple variants of a component part. For
 *  example a multi-processor needs multiple copies of the registers,
 *  one per core. This class manages that process
 */
class CarbonCfgTemplate : public QObject, public QScriptable
{
  Q_OBJECT

  Q_PROPERTY(quint32 NumVariables READ GetNumVariables)

private:
  UtString nv;

public slots:
#ifndef SWIG
  quint32 GetNumVariables() { return this->getNumVariables(); }
  QString GetValue(const QString& variable)
  {
    nv << variable;
    return this->getValue(nv.c_str());
  }

  QString GetVariable(quint32 index)
  {
    return getVariable(index);
  }

  bool AddVariable(const QString& variable, const QString& value)
  {
    UtString var, val;
    var << variable;
    val << value;
    return this->addVariable(var.c_str(), val.c_str());
  }

  //! Clear all the variables
  void Clear() { clear(); }
#endif
public:
  CARBONMEM_OVERRIDES

  //! constructor
  CarbonCfgTemplate(CarbonCfgMsgHandler* msgHandler) : 
    mMsgHandler(msgHandler)
  {}

  //! Destructor
  ~CarbonCfgTemplate() { clear(); }

  //! Add a variable to string translation to this template
  /*! The variables should be of the form $(unique-name). If not, this
   *  function will return false. It also must be a unique name. The
   *  resulting value can be any string.
   *
   *  Prints any warnings to the msg handler. This must be
   *  checked by the caller at some point.
   *
   *  Returns true on succesful, false otherwise.
   */
  bool addVariable(const char* variable, const char* value);

  void clear();
 
  //! Convert a string from template to resolved form
  /*! This function uses the map populated by addVariable to resolve
   *  the string.
   *
   *  Prints any warnings to the msg handler. This must be
   *  checked by the caller at some point.
   *
   *  Returns true on succesful, false otherwise.
   */
  bool resolveString(const char* src, UtString* dst) const;

  //! Get the number of variables in the map
  UInt32 getNumVariables(void) const { return mVariableMap.size(); }

  //! Get a variable by index (used by iteration)
  const char* getVariable(UInt32 index) const;

  //! Get the value associated with a variable
  const char* getValue(const char* variable) const;

  const char* getErrMsg(void) const { return mMsgHandler->getErrMsg(); }
private:
  //! Abstraction for a map from variables to values
  typedef UtHashMap<UtString, UtString> VariableMap;

  //! Abstraction for an array of variables
  /*! We keep an array so we can support iteration from Java
   */
  typedef UtArray<UtString*> Variables;

  //! Storage for the variable translation map
  VariableMap mVariableMap;

  //! Storage for the variables in this template
  Variables mVariables;

  //! Place to record warnings/errors
  CarbonCfgMsgHandler* mMsgHandler;
}; // class CarbonCfgTemplate

//! CarbonCfgXtorPort class
class CarbonCfgXtorPort  : public QObject, public QScriptable {
  Q_OBJECT

  Q_PROPERTY(QString Name READ GetName)
  Q_PROPERTY(quint32 Size READ GetSize)
  Q_PROPERTY(CcfgEnum::CarbonCfgRTLPortType Type READ GetType)

public slots:
#ifndef SWIG
  CcfgEnum::CarbonCfgRTLPortType GetType() const 
  {
    return static_cast<CcfgEnum::CarbonCfgRTLPortType>(this->getType()); 
  }
  QString GetName() const { return this->getName(); }
  quint32 GetSize() const { return this->getSize(); }
#endif

public:
  CARBONMEM_OVERRIDES

  //! ctor
  CarbonCfgXtorPort(int size, CarbonCfgRTLPortType type,
                    const char* name,
                    const char* description);

  //! get name of port
  const char* getName() const {return mName.c_str();}

  //! get size of port
  UInt32 getSize() const {return mSize;}

  //! get type of port
  CarbonCfgRTLPortType getType() const { return mType; }
  void putType(CarbonCfgRTLPortType newType) { mType=newType; }

  //! get the description of the port
  const char* getDescription() const {return mDescription.c_str();}

  //! get if port is optional
  bool getOptional() const { return mOptional; }
  void putOptional(bool optional) { mOptional = optional; }

  //! Use this method to determine if this port's event condition is also
  //! sensitive to clock edges.
  //! If it returns false, then events should only be queued when the value
  //! changes.  
  //! If it returns true, then events should also be queued if the value is
  //! nonzero and a new clock edge has occurred.
  bool isClockSensitive() { return mIsClockSensitive; }
  void putIsClockSensitive(bool isClockSensitive) { mIsClockSensitive = isClockSensitive; }
  
  //! Use this method to get the name of the event functor instance that is 
  //! queued in the event queue
  const char *getEventName() { return mEventName.c_str(); }
  void putEventName(const char *eventName) { mEventName = eventName; }
  
  //! Used to retrieve the SystemC type
  const char* getTypeDef() const {return mTypeDef.c_str();}

  //! Used to store the SystemC type
  void putTypeDef(const char* typeDef) {mTypeDef=typeDef;}

  //! Add a phase name for this pin
  void addPhaseName(const char* phaseName);

  //! Get the number of phases names
  UInt32 numPhaseNames(void) const { return mPhaseNames.size(); }

  //! Get a particular phase name - asserts if it is out of range
  const char* getPhaseName(UInt32 index) const;

  //! Used to retrieve the width expr if it has one, this uses Python notation
  const char* getWidthExpr() const {return mWidthExpr.c_str();}

  //! Used to store the wdith expression, this uses Python notation
  void putWidthExpr(const char* widthExpr) {mWidthExpr=widthExpr;}

private:
  UInt32 mSize;
  CarbonCfgRTLPortTypeIO mType;
  UtString mName;
  UtString mTypeDef;
  UtString mDescription;
  bool     mOptional;
  bool     mIsClockSensitive;
  UtString mEventName;
  UtStringArray mPhaseNames;    // TLM2 phases associated with this pin
  UtString mWidthExpr;          // Some port width's are based off the bus data width
}; // class CarbonCfgXtorPort

//! CarbonCfgXtorPort class
class CarbonCfgXtorSystemCPort {
public:
  CARBONMEM_OVERRIDES

  //! ctor
  CarbonCfgXtorSystemCPort(const char* name, CarbonCfgSystemCPortType type, const char* iface,
                           const char* description);

  //! get name of port
  const char* getName() const {return mName.c_str();}

  //! get interface of port
  const char* getInterface() const {return mInterface.c_str();}

  //! get type of port
  CarbonCfgSystemCPortType getType() const { return mType; }
  void putType(CarbonCfgSystemCPortType newType) { mType=newType; }

  //! get the description of the port
  const char* getDescription() const {return mDescription.c_str();}
  
private:
  UtString                   mName;
  CarbonCfgSystemCPortTypeIO mType;
  UtString                   mInterface;
  UtString                   mDescription;
};

//! CarbonCfgXtorParam class
class CarbonCfgXtorParam  : public QObject, public QScriptable {
  Q_OBJECT
  
  Q_PROPERTY(QString Name READ GetName);
  Q_PROPERTY(quint32 Size READ GetSize);
  Q_PROPERTY(CcfgEnum::CarbonCfgParamFlag Flag READ GetFlag);
  Q_PROPERTY(CcfgEnum::CarbonCfgParamDataType Type READ GetType);
  Q_PROPERTY(QString Description READ GetDescription);
  Q_PROPERTY(QString EnumChoices READ GetEnumChoices);
  Q_PROPERTY(QString DefaultValue READ GetDefaultValue);
  Q_PROPERTY(CcfgEnum::CarbonCfgElementGenerationType ElementGenerationType READ GetElementGenerationType WRITE SetElementGenerationType);


public slots:
#ifndef SWIG
  CcfgEnum::CarbonCfgElementGenerationType GetElementGenerationType() const { return CcfgEnum::CarbonCfgElementGenerationType(this->getElementGenerationType()); }
  void SetElementGenerationType(CcfgEnum::CarbonCfgElementGenerationType wt) { this->putElementGenerationType(CarbonCfgElementGenerationType(wt)); }

  QString GetName() const { return this->getName(); }
  quint32 GetSize() const { return this->getSize(); }
  CcfgEnum::CarbonCfgParamFlag GetFlag() const { return CcfgEnum::CarbonCfgParamFlag(this->getFlag()); }
  CcfgEnum::CarbonCfgParamDataType GetType() const { return CcfgEnum::CarbonCfgParamDataType(this->getType()); }
  QString GetDescription() const { return this->getDescription(); }
  QString GetEnumChoices() const { return this->getEnumChoices(); }
  QString GetDefaultValue() const { return this->getDefaultValue(); }
#endif
public:
  CARBONMEM_OVERRIDES

  //! ctor
  CarbonCfgXtorParam(int size,
                     CarbonCfgParamDataType type,
                     CarbonCfgParamFlag flag,
                     const char* name,
                     const char* description,
                     const char* defaultValue,
                     const char* enumChoices,
                     const char* min,
                     const char* max);

  //! Write parameter definition to .ccfg file (xml section)
  void write(CfgXmlWriter &xml);

  //! get name of parameter
  const char* getName() const {return mName.c_str();}

  //! get size of parameter
  UInt32 getSize() const {return mSize;}

  //! get scope of parameter
  CarbonCfgParamFlag getFlag() const {return mFlag; }

  //! get type of parameter
  CarbonCfgParamDataType getType() const { return mType; }

  //! get the description of the parameter
  const char* getDescription() const {return mDescription.c_str();}

  //! get the default value
  const char* getDefaultValue() const {return mDefaultValue.c_str();}
  void putDefaultValue(const char* value) { mDefaultValue = value; }

  //! get the enumChoices string (e.g. "|a|b|c|")
  const char* getEnumChoices() const { return mEnumChoices.c_str() ; }

  //! Number of enum choices
  UInt32 numEnumChoices() const { return mEnumChoiceArray.size(); }

  //! Get a particular enum choice
  const char* getEnumChoice(UInt32 index) const;

  void putElementGenerationType(CarbonCfgElementGenerationType t) {mElementGenerationType = t;}
  CarbonCfgElementGenerationType getElementGenerationType() const { return mElementGenerationType; }

  //! Check if a parameter value is in range
  bool inRange(const char* value) const;

  //! Get the minimum value (if it exists)
  const char* min() const { return mMin.c_str(); }

  //! Get the maximum value (if it exists)
  const char* max() const { return mMax.c_str(); }

private:
  int mSize;
  CarbonCfgParamDataType mType;
  CarbonCfgParamFlag mFlag;
  UtString mName;
  UtString mDescription;
  UtString mDefaultValue;
  UtStringArray mEnumChoiceArray; // temporary storage for convenience
  UtString mEnumChoices;
  CarbonCfgElementGenerationType mElementGenerationType;

  //! Optional minimum (kept as a string so that it can be converted to the right type
  UtString mMin;

  //! Optional minimum (kept as a string so that it can be converted to the right type
  UtString mMax;
};

//! CarbonCfgXtorParamInst class
class CarbonCfgXtorParamInst  : public QObject, public QScriptable {
  Q_OBJECT

  Q_PROPERTY(QString Value READ GetValue WRITE SetValue)
  Q_PROPERTY(QString DefaultValue READ GetDefaultValue)
  Q_PROPERTY(quint32 NumRTLPorts READ NumRTLPorts)
  Q_PROPERTY(quint32 NumEnumChoices READ GetNumEnumChoices)
  Q_PROPERTY(CarbonCfgXtorInstance* Instance READ GetInstance)
  Q_PROPERTY(CarbonCfgXtorParam* Param READ GetParam)
  Q_PROPERTY(CcfgEnum::CarbonCfgElementGenerationType ElementGenerationType READ GetElementGenerationType WRITE SetElementGenerationType);
  Q_PROPERTY(bool Hidden READ GetHidden WRITE SetHidden)

  typedef UtArray<CarbonCfgRTLPort*> RTLPortVec;

public slots:
#ifndef SWIG
  CcfgEnum::CarbonCfgElementGenerationType GetElementGenerationType() const { return CcfgEnum::CarbonCfgElementGenerationType(this->getElementGenerationType()); }
  void SetElementGenerationType(CcfgEnum::CarbonCfgElementGenerationType wt) { this->putElementGenerationType(CarbonCfgElementGenerationType(wt)); }

  CarbonCfgRTLPort* GetRTLPort(quint32 i) { return this->getRTLPort(i); }
  quint32 NumRTLPorts() { return this->numRTLPorts(); }
  QString GetDefaultValue() { return this->getDefaultValue(); }
  QString GetValue() { return this->getValue(); }
  bool SetValue(const QString& newVal)
  {
    UtString value;
    value << newVal;
    return this->putValue(value.c_str());
  }
  bool RemoveEnumChoice(const QString& choice)
  {
    UtString value;
    value << choice;
    return this->removeEnumChoice(value.c_str());
  }
  quint32 GetNumEnumChoices() 
  {
    return this->numEnumChoices();
  }
  QString GetEnumChoice(quint32 i) { return this->getEnumChoice(i);}
  
  CarbonCfgXtorInstance* GetInstance() const { return this->getInstance(); }
  CarbonCfgXtorParam* GetParam() const { return this->getParam(); }

  bool GetHidden() { return this->hidden(); }
  void SetHidden(bool hidden) { this->putHidden(hidden); }

#endif
public:
  CARBONMEM_OVERRIDES
 
  //! ctor
  CarbonCfgXtorParamInst(CarbonCfgXtorInstance*, CarbonCfgXtorParam*);

  CarbonCfgXtorInstance* getInstance() const { return mXtorInstance; }
  CarbonCfgXtorParam* getParam() const { return mXtorParam; }
  const char* getValue() { return mValue.c_str(); }
  bool putValue(const char* value);
  const char* getDefaultValue() { return mXtorParam->getDefaultValue(); }
  
  void putElementGenerationType(CarbonCfgElementGenerationType t) {mElementGenerationType = t;}
  CarbonCfgElementGenerationType getElementGenerationType() const { return mElementGenerationType; }

  void removeRTLPort(CarbonCfgRTLPort* rtlPort);
  UInt32 numRTLPorts() const {return mRTLPortVec.size();}
  CarbonCfgRTLPort* getRTLPort(UInt32 i) const {
    INFO_ASSERT(i < mRTLPortVec.size(), "RTLPortVec out of range");
    return mRTLPortVec[i];
  }
  CarbonCfgRTLPort* findRTLPort(const char*);
  //! An an RTL port if one by that name does not yet exist.
  /*! If one already exists of that name, return NULL
   */
  CarbonCfgRTLPort* addRTLPort(CarbonCfgRTLPort* rtlPort)
  {
    mRTLPortVec.push_back(rtlPort);
    return rtlPort;
  }

  //! Remove a supported enum parameter value - returns false if it does not exist
  /*! Note that this function is O(n) since the data is stored in an
   *  array. We expect the array sizes to be relatively small.
   */
  bool removeEnumChoice(const char* value);

  //! The number of instance enum choices (may be a subset of parameter enum choices)
  UInt32 numEnumChoices() const { return mEnumChoices.size(); }

  //! Get a particular enum choice
  const char* getEnumChoice(UInt32 index) const;

  //! Update the actual enum choices (used for file I/O)
  void putEnumChoices(const UtStringArray& enumChoices);

  //! Mark this parameter hidden or not
  void putHidden(bool hidden) { mHidden = hidden; }

  //! Is this parameter hidden?
  bool hidden(void) { return mHidden; }

  ~CarbonCfgXtorParamInst();

private:
  CarbonCfgXtorInstance* mXtorInstance;
  CarbonCfgXtorParam* mXtorParam;
  UtString mValue;
  RTLPortVec mRTLPortVec;
  CarbonCfgElementGenerationType mElementGenerationType;

  //! Storage for the enum choices supported (starts out with the full set for enums, empty for other params)
  UtStringArray mEnumChoices;

  //! If set, the parameter instance should not be displayed
  bool mHidden;
};


//! Class to represent a transactor template argument
class CarbonCfgXtorTemplateArg
{
public:
  CARBONMEM_OVERRIDES

  //! virtual destructor
  virtual ~CarbonCfgXtorTemplateArg() {}

  //! Type
  const char* getType(void) const { return mType.c_str(); }

  //! Value
  const char* getValue(void) const { return mValue.c_str(); }

  //! Default
  const char* getDefault(void) const { return mDefault.c_str(); }

  //! Cast to the class type template arg
  virtual CarbonCfgXtorClassTemplateArg* castClassTemplateArg(void) { return NULL; }

  //! Cast to the portWidth type template arg
  virtual CarbonCfgXtorPortWidthTemplateArg* castPortWidthTemplateArg(void) { return NULL; }

  //! Cast to the text type template arg
  virtual CarbonCfgXtorTextTemplateArg* castTextTemplateArg(void) { return NULL; }

protected:
  //! Constructor - use one of the derived classes
  CarbonCfgXtorTemplateArg(const char* type, const char* value, const char* defaultValue) :
    mType(type), mValue(value), mDefault(defaultValue)
  {
  }

private:
  UtString mType;
  UtString mValue;
  UtString mDefault;
}; // class CarbonCfgXtorTemplateArg

//! Class to represent a transactor template arg that is the transactor class name
class CarbonCfgXtorClassTemplateArg : public CarbonCfgXtorTemplateArg
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonCfgXtorClassTemplateArg(const char* value, const char* defaultValue) :
    CarbonCfgXtorTemplateArg("class", value, defaultValue)
  {}

  //! Cast to the class type template arg
  virtual CarbonCfgXtorClassTemplateArg* castClassTemplateArg(void) { return this; }
};

//! Class to represent a transactor template arg that is a port width
class CarbonCfgXtorPortWidthTemplateArg : public CarbonCfgXtorTemplateArg
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonCfgXtorPortWidthTemplateArg(const char* value, const char* defaultValue, bool busDataWidth) :
    CarbonCfgXtorTemplateArg("port-width", value, defaultValue),
    mBusDataWidth(busDataWidth)
  {}

  //! Get whether this port width represents the xtor bus data width
  bool isBusDataWidth(void) const { return mBusDataWidth; }

  //! Cast to the portWidth type template arg
  CarbonCfgXtorPortWidthTemplateArg* castPortWidthTemplateArg(void) { return this; }

private:
  bool mBusDataWidth;
};

//! Class to represent a transactor template arg that is just simple text
class CarbonCfgXtorTextTemplateArg : public CarbonCfgXtorTemplateArg
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonCfgXtorTextTemplateArg(const char* value, const char* defaultValue) :
    CarbonCfgXtorTemplateArg("text", value, defaultValue)
  {}

  //! Cast to the text type template arg
  virtual CarbonCfgXtorTextTemplateArg* castTextTemplateArg(void) { return this; }
};

//! CarbonCfgXtorAbstraction class
/*! This class stores information about a transactor which could be
 *  different for abstractions of the transactor. This allows us to have
 *  one transactor and have the user change the abstraction as needed.
 *
 *  This is needed for TLM2 where the transactor can have CT, LT, and
 *  PV abstractions.
 *
 *  The information that can be different for each abstraction is:
 *
 *   - The class name
 *   - The set of include files
 *   - The set of library files
 *   - The set of include paths
 *   - The set of library paths
 *   - The layer ID
 *   - flags (represented as strings)
 *
 *  This class is used as a member of CarbonCfgXtor to support the
 *  default behavior for non-TLM2 transactors. It is also in a list to
 *  support TLM2.
 *  
 */
class CarbonCfgXtorAbstraction
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonCfgXtorAbstraction(const char* name) : mName(name) {}

  //! Destructor
  ~CarbonCfgXtorAbstraction() {}

  //! The name for this abstraction (to put int GUI pull down)
  const char* getName() { return mName.c_str(); }

  //! Class name for the adapter
  const char* getClassName() { return mClassName.c_str(); }
  void putClassName(const char* className) { mClassName = className; }

  //! The set of include files for the adapter
  UInt32 numIncludeFiles() { return mIncludeFiles.size(); }
  const char* getIncludeFile(UInt32 index) { return mIncludeFiles[index]; }
  void addIncludeFile(const char* includeFile) { mIncludeFiles.push_back(includeFile); }

  //! This method should return the layer id for the xactor
  /*! This is used by TLM2 AMBA transactors to specify CT, LT, AT, or PV.
   */
  const char* getLayerId() { return mLayerId.c_str(); }
  void putLayerId(const char* layerId) { mLayerId = layerId; }

  //! Get the set of library files associated with this xactor
  UInt32 numLibFiles() { return mLibFiles.size(); }
  const char* getLibFile(UInt32 index) { return mLibFiles[index]; }
  void addLibFile(const char* libFile) { mLibFiles.push_back(libFile); }

  //! This method can be used to get a path to the header files
  //! for this transactor.  Should mainly be used in the makefile generation
  UInt32 numIncPaths() { return mIncPaths.size(); }
  const char* getIncPath(UInt32 index) { return mIncPaths[index]; }
  void addIncPath(const char* incPath) { mIncPaths.push_back(incPath); }

  //! If the xactors are static libraries, the makefile
  //! will need the path to those static libraries, which can be found
  //! using these methods
  UInt32 numLibPaths() { return mLibPaths.size(); }
  const char* getLibPath(UInt32 index) { return mLibPaths[index]; }
  void addLibPath(const char* libPath) { mLibPaths.push_back(libPath); }

  //! Transactor flags for this abstraction; it is the responsibility of the wrapper generators to interpret.
  UInt32 numFlags() { return mFlags.size(); }
  const char* getFlag(UInt32 index) { return mFlags[index]; }
  void addFlag(const char* flag) { mFlags.push_back(flag); }

private:
  UtString mName;
  UtString mClassName;
  UtStringArray mIncludeFiles;
  UtString mLayerId;
  UtStringArray mLibFiles;
  UtStringArray mIncPaths;
  UtStringArray mLibPaths;
  UtStringArray mFlags;
}; // class CarbonCfgXtorAbstraction


//! CarbonCfgXtor class
class CarbonCfgXtor : public QObject, public QScriptable
{
  Q_OBJECT

  Q_PROPERTY(QString Name READ GetName)
  Q_PROPERTY(quint32 NumPorts READ NumPorts)

  typedef UtHashMap<UtString,UInt32> PortMap;
  typedef UtArray<CarbonCfgXtorPort*> PortVec;
  typedef UtArray<CarbonCfgXtorSystemCPort*> SystemCPortVec;
  typedef UtHashMap<UtString,CarbonCfgXtorParam*> ParamMap;
  typedef UtArray<CarbonCfgXtorTemplateArg*> TemplateArgs;
  typedef UtArray<CarbonCfgXtorAbstraction*> Abstractions;

public slots:
#ifndef SWIG
  CarbonCfgXtorPort* GetPort(quint32 i) { return this->getPort(i); }
  CarbonCfgXtorPort* FindPort(const QString& portName)
  { 
    UtString name;
    name << portName;
    return this->findPort(name.c_str());
  }
  CarbonCfgXtorParam* FindParam(const QString& paramName)
  { 
    UtString name;
    name << paramName;
    return this->findParam(name.c_str());
  }
  QString GetName() const { return this->getName(); }
  quint32 NumPorts() const { return this->numPorts(); }
#endif

public:
  CARBONMEM_OVERRIDES

  //! ctor
  CarbonCfgXtor(const char* name, const char* libraryName, const char* variant="") :
    mName(name), 
    mLibraryName(libraryName),
    mVariant(variant),
    mHasWriteDebug(false), 
    mIsXtorClockMaster(false),
    mIsFlowThru(false),
    mIsSaveRestore(false),
    mIsMaster(false),
    mIsSlave(false),
    mIsAmba(false),
    mHasDebugTransaction(false),
    mIsSDXactor(false),
    mUseEventQueue(false),
    mPreservePortNameCase(false),
    mUseCommunicate(false),
    mUseESLClockMaster(false),
    mUseESLResetMaster(false),
    mXtorAbstraction("Default")
  {}

  //! dtor
  ~CarbonCfgXtor();

  //! get name of xtor
  const char* getName() const {return mName.c_str();}

  //! get number of ports
  UInt32 numPorts() const {return mPorts.size();}

  //! get the nth port
  CarbonCfgXtorPort* getPort(UInt32 index) const {
    INFO_ASSERT(index < mPortVec.size(), "port out of range");
    return mPortVec[index];
  }

  //! get number of ports
  UInt32 numSystemCPorts() const {return mSystemCPortVec.size();}

  //! get the nth port
  CarbonCfgXtorSystemCPort* getSystemCPort(UInt32 index) const {
    INFO_ASSERT(index < mSystemCPortVec.size(), "port out of range");
    return mSystemCPortVec[index];
  }

#ifndef SWIG
  //! type for looping over ports
  typedef PortMap::SortedLoop PortLoop;
  
  //! loop over ports
  PortLoop loopPorts() {return mPorts.loopSorted();}
#endif

  //! Does this transactor have the specified port name?
  bool hasPort(const char* portName) const {
    return mPorts.find(portName) != mPorts.end();
  }
  //! lookup a port by name, modifying *portIndex, returning success
#ifndef SWIG
  bool findPort(const char* port, UInt32* portIndex);
#endif

  //! look up a port by name, returning the port or NULL if failure
  CarbonCfgXtorPort* findPort(const char* port);

  //! Add a port description to the transactor
  void addPort(CarbonCfgXtorPort* port);

  //! Add a SystemC port description to the transactor
  void addSystemCPort(CarbonCfgXtorSystemCPort* port);

#ifndef SWIG
  //! type for looping over params
  typedef ParamMap::SortedLoop ParamLoop;
  
  //! loop over params
  ParamLoop loopParams() {return mParams.loopSorted();}
#endif
  //! Does this transactor have the specified param name?
  bool hasParam(const char* paramName) const {
    return mParams.find(paramName) != mParams.end();
  }

  //! lookup a param by name
  CarbonCfgXtorParam* findParam(const char* param);

  //! Add a port description to the transactor
  void addParam(CarbonCfgXtorParam* param) {
    mParams[param->getName()] = param;
  }

  void putHasWriteDebug(bool hasWriteDebug)
  {
    mHasWriteDebug = hasWriteDebug;
  }

  bool hasWriteDebug() const { return mHasWriteDebug; }

  const char *getClassName() { return mXtorAbstraction.getClassName(); }
  void putClassName(const char *className) { mXtorAbstraction.putClassName(className); }

  UInt32 numIncludeFiles() { return mXtorAbstraction.numIncludeFiles(); }
  const char *getIncludeFile(UInt32 index) { return mXtorAbstraction.getIncludeFile(index); }
  void addIncludeFile(const char *includeFile) { mXtorAbstraction.addIncludeFile(includeFile); }

  bool isXtorClockMaster() { return mIsXtorClockMaster; }
  void putIsXtorClockMaster(bool isXtorClockMaster) { mIsXtorClockMaster = isXtorClockMaster; }

  bool isFlowThru() { return mIsFlowThru; }
  void putIsFlowThru(bool isFlowThru) { mIsFlowThru = isFlowThru; }

  bool isSaveRestore() { return mIsSaveRestore; }
  void putIsSaveRestore(bool isSaveRestore) { mIsSaveRestore = isSaveRestore; }

  bool isMaster() { return mIsMaster; }
  void putIsMaster(bool isMaster) { mIsMaster = isMaster; }

  bool isSlave() { return mIsSlave; }
  void putIsSlave(bool isSlave) { mIsSlave = isSlave; }

  bool isAmba() { return mIsAmba; }
  void putIsAmba(bool isAmba) { mIsAmba = isAmba; }

  //! Use this method to determine if debugTransaction should be used
  bool hasDebugTransaction() const { return mHasDebugTransaction; }
  void putHasDebugTransaction(bool hasDebugTransaction) { mHasDebugTransaction = hasDebugTransaction; }

  //! This method should return the protocol name for the xactor
  const char *getProtocolName() { return mProtocolName.c_str(); }
  void putProtocolName(const char* protocolName) { mProtocolName = protocolName; }

  //! Use this method to get the xactor library name.
  const char* getLibraryName(void) const;

  //! Use this method to get the xactor variant (used for CoWare Master/Slave)
  const char* getVariant(void) const;

  //! Get the set of library files associated with this xactor
  UInt32 numLibFiles() { return mXtorAbstraction.numLibFiles(); }
  const char *getLibFile(UInt32 index) { return mXtorAbstraction.getLibFile(index); }
  void addLibFile(const char *libFile) { mXtorAbstraction.addLibFile(libFile); }

  //! This method can be used to get a path to the header files
  //! for this transactor.  Should mainly be used in the makefile generation
  UInt32 numIncPaths() { return mXtorAbstraction.numIncPaths(); }
  const char *getIncPath(UInt32 index) { return mXtorAbstraction.getIncPath(index); }
  void addIncPath(const char *incPath) { mXtorAbstraction.addIncPath(incPath); }

  //! If the xactors are static libraries, the makefile
  //! will need the path to those static libraries, which can be found
  //! using these methods
  UInt32 numLibPaths() { return mXtorAbstraction.numLibPaths(); }
  const char *getLibPath(UInt32 index) { return mXtorAbstraction.getLibPath(index); }
  void addLibPath(const char *libPath) { mXtorAbstraction.addLibPath(libPath); }

  //! Transactor flags for this transactor; it is the responsibility of the wrapper generators to interpret.
  UInt32 numFlags() { return mXtorAbstraction.numFlags(); }
  const char *getFlag(UInt32 index) { return mXtorAbstraction.getFlag(index); }
  void addFlag(const char *flag) { mXtorAbstraction.addFlag(flag); }

  //! Use this method to get a space-separated list of additional arguments
  //! that should be appended to the xactor constructor's argument list
  const char *getConstructorArgs() { return mConstructorArgs.c_str(); }
  void putConstructorArgs(const char *args) { mConstructorArgs = args; }

  //! Use this method to distinguish from xactors from official SoCDesigner protocol
  //! installations and custom user SoCDesigner xactors
  bool isSDXactor() { return mIsSDXactor; }
  void putIsSDXactor(bool isSDXactor) { mIsSDXactor = isSDXactor; }

  //! Use this method to determine whehter to use event queues/combinatorial/update or
  //! the old communicate/update
  bool useEventQueue() { return mUseEventQueue; }
  void putUseEventQueue(bool useEventQueue) { mUseEventQueue = useEventQueue; }

  //! Use this method to determine whether to preserve case in the
  //! logical port names of this transactor.  These are the names that
  //! will be passed to the port factory functions in the component.
  //! This can override the default behavior for SoCD-provided
  //! transactors, which is to convert all port names to uppercase.
  bool preservePortNameCase() { return mPreservePortNameCase; }
  void putPreservePortNameCase(bool preserve) { mPreservePortNameCase = preserve; }

  //! Use this method to determine whether to call communicate on the transactor
  bool useCommunicate() { return mUseCommunicate; }
  void putUseCommunicate(bool useCommunicate) { mUseCommunicate = useCommunicate; }

  //! Return true if an ESL port is to be used as a clock master, otherwise use a clock transactor
  bool useESLClockMaster() { return mUseESLClockMaster; }
  void putUseESLClockMaster(bool useESLClockMaster) { mUseESLClockMaster = useESLClockMaster; }

  //! Use this method to determine whether  to use an ESL port as a reset master
  bool useESLResetMaster() { return mUseESLResetMaster; }
  void putUseESLResetMaster(bool useESLResetMaster) { mUseESLResetMaster = useESLResetMaster; }

  //! Helper function to parse and add a template arguement
  void addTemplateArg(CarbonCfgXtorTemplateArg* arg) { mTemplateArgs.push_back(arg); }

  //! The number of template arguments
  UInt32 numTemplateArgs(void) const { return mTemplateArgs.size(); }

  //! Return a specific template arg -- asserts if it is out of range
  CarbonCfgXtorTemplateArg* getTemplateArg(UInt32 index) const;

  //! The number of abstractions
  UInt32 numAbstractions(void) const { return mAbstractions.size(); }

  //! Get a specific abstraction by index
  CarbonCfgXtorAbstraction* getAbstraction(UInt32 index) const;

  //! Add a new xtor abstraction
  void addAbstraction(CarbonCfgXtorAbstraction* xtorAbstraction);

private:
  UtString mName;
  UtString mLibraryName;
  UtString mVariant;
  PortMap mPorts;
  PortVec mPortVec;
  SystemCPortVec mSystemCPortVec;
  ParamMap mParams;
  bool mHasWriteDebug;
  bool mIsXtorClockMaster;
  bool mIsFlowThru;
  bool mIsSaveRestore;
  bool mIsMaster;
  bool mIsSlave;
  bool mIsAmba;

  bool mHasDebugTransaction;
  bool mIsSDXactor; 
  bool mUseEventQueue;
  bool mPreservePortNameCase;
  bool mUseCommunicate;
  bool mUseESLClockMaster;
  bool mUseESLResetMaster;
  UtString mProtocolName;
  UtString mConstructorArgs; 
  TemplateArgs mTemplateArgs;
  CarbonCfgXtorAbstraction mXtorAbstraction;
  Abstractions mAbstractions;
}; // class CarbonCfgXtor

//! CarbonCfgXtor library class
class CarbonCfgXtorLib  : public QObject, public QScriptable {
  Q_OBJECT

  Q_PROPERTY(quint32 NumXtors READ NumXtors)


  class NameVariantPair : public std::pair<UtString, UtString>
  {
  public:
    NameVariantPair(const char* name, const char* variant) :
      std::pair<UtString, UtString>(name, variant)
    {}

    //! hash function so that we can put this as a key in a hash set/map
    size_t hash() const { return first.hash() + (17 * second.hash()); }
  };

  typedef UtHashMap<NameVariantPair,CarbonCfgXtor*> XtorMap;
  typedef UtArray<CarbonCfgXtor*> XtorVec;

  friend class CfgXmlParserXtor;
  friend class CfgCowareXmlParserXtor;

public slots:
#ifndef SWIG
  quint32 NumXtors() const { return this->numXtors(); }
  CarbonCfgXtor* GetXtor(quint32 i) { return this->getXtor(i); }
  CarbonCfgXtor* FindXtor(const QString& xtorName)
  { 
    UtString name;
    name << xtorName;
    return this->findXtor(name.c_str());
  }
#endif
public:
  CARBONMEM_OVERRIDES

  //! ctor
  CarbonCfgXtorLib(const char* libName);

  //! dtor
  ~CarbonCfgXtorLib();

  //! read the transactor library file
  bool read(const char* libfile, UtString* errmsg);
  bool readXtorDefinitions(const char* libfile, UtString* errmsg);

  //! find a transactor given a name, return NULL if none exists
  CarbonCfgXtor* findXtor(const char* name, const char* variant="");

#ifndef SWIG
  typedef XtorMap::SortedLoop XtorLoop;

  //! loop over the transactors
  XtorLoop loopXtors();
#endif
  //! get number of xtors
  UInt32 numXtors() const {return mXtorVec.size();}

  //! get the nth xtor
  CarbonCfgXtor* getXtor(UInt32 index) const {
    INFO_ASSERT(index < mXtorVec.size(), "xtor out of range");
    return mXtorVec[index];
  }

  //! Returns true if this library hasn't been popuated yet
  bool empty(void) const { return mXtorVec.empty(); }

  //! Set the version
  /*! Note that there are no checks to make sure this isn't set more than once
   *  The problem is the MaxSim transactors are all read in one library right now.
   *  So for now it is done this way. If we can fix the MaxSim transactors then
   *  we can move the version to the constructor.
   */ 
  void putVersion(UInt32 major, UInt32 minor);

  //! Get the library name
  /*! Note that this cannot be changed because it is also stored in a
   *  CarbonCfg map for quick lookup.
   */
  const char* libName(void) const { return mLibName.c_str(); }

  //! Get the major version
  UInt32 majorVersion(void) const { return mMajorVersion; }

  //! Get the minor version
  UInt32 minorVersion(void) const { return mMinorVersion; }

private:
  void addXtor(CarbonCfgXtor *xtor);
  void addSynonym(CarbonCfgXtor *xtor, const char *synonym, const char* synonymVariant="");

  UtString mLibName;
  XtorVec mXtorVec;
  XtorMap mXtorMap;
  UInt32 mMajorVersion;
  UInt32 mMinorVersion;
};



//! class to represent a Carbon RTL Port
/*!
 *! This duplicates information in the Carbon database, and
 *! keeps track of what ESL entities it may be connected to:
 *!   - ESL signals (master or slave)
 *!   - Transactors ports
 *!   - tied (inputs or inouts)
 *!   - disconnects (outputs or inouts)
 *!   - generated clocks
 *!   - generated resets
 *!
 *! When the GUI creates a new configuration, it connects all
 *! RTL I/Os to ESL ports.
 *!
 *! An RTLPort with 0 connections is considered disconnected.
 *! It is not legal for an input port to be disconnected --
 *! it will force an RTL port of 0.
 *!
 *! RTL Ports are connected to CarbonCfgRTLConnections (a base class)
 *! Derivations include CarbonCfgESLPort, CarbonCfgClockGen, CarbonCfgResetGen,
 *! CarbonCfgXtorConn, CarbonCfgTie, CarbonCfgTieParam
 *!
 */
struct CarbonCfgRTLPort : public QObject, public QScriptable {
  Q_OBJECT

  Q_PROPERTY(QString Name READ GetName)
  Q_PROPERTY(quint32 Width READ GetWidth)
  Q_PROPERTY(CcfgEnum::CarbonCfgRTLPortType PortType READ GetPortType)
  Q_PROPERTY(quint32 NumConnections READ NumConnections)

public slots:
#ifndef SWIG
  bool IsDisconnected() { return this->isDisconnected(); }
  bool IsConnected() { return this->isConnected(); }
  bool IsResetGen() { return this->isResetGen(); }
  bool IsClockGen() { return this->isClockGen(); }
  bool IsTied() { return this->isTied(); }
  bool IsTiedToParam() { return this->isTiedToParam(); }
  bool IsConnectedToXtor() { return this->isConnectedToXtor(); }

  void Connect(QVariant c)
  {
    CarbonCfgClockGen* cg = qvariant_cast<CarbonCfgClockGen*>(c);
    if (cg)
      ConnectClockGen(cg);

    CarbonCfgResetGen* crg = qvariant_cast<CarbonCfgResetGen*>(c);
    if (crg)
      ConnectResetGen(crg);

    CarbonCfgTie* ctie = qvariant_cast<CarbonCfgTie*>(c);
    if (ctie)
      ConnectTie(ctie);

    CarbonCfgTieParam* ctiep = qvariant_cast<CarbonCfgTieParam*>(c);
    if (ctiep)
      ConnectTieParam(ctiep);

    CarbonCfgESLPort* ceslp = qvariant_cast<CarbonCfgESLPort*>(c);
    if (ceslp)
      ConnectESLPort(ceslp);

    CarbonCfgXtorConn* cxtor = qvariant_cast<CarbonCfgXtorConn*>(c);
    if (cxtor)
      ConnectXtorConn(cxtor);

    CarbonCfgRTLConnection* crtl = qvariant_cast<CarbonCfgRTLConnection*>(c);
    if (crtl)
      ConnectRTLConn(crtl);
  }

  void ConnectClockGen(CarbonCfgClockGen* conn);
  void ConnectResetGen(CarbonCfgResetGen* conn);
  void ConnectTie(CarbonCfgTie* conn);
  void ConnectTieParam(CarbonCfgTieParam* conn);
  void ConnectESLPort(CarbonCfgESLPort* conn);
  void ConnectXtorConn(CarbonCfgXtorConn* conn);
  void ConnectRTLConn(CarbonCfgRTLConnection* conn) { this->connect(conn); }

  CcfgEnum::CarbonCfgRTLPortType GetPortType() 
  {
    return static_cast<CcfgEnum::CarbonCfgRTLPortType>(this->getType());
  }
  QString GetName() { return this->getName(); }
  quint32 GetWidth() { return this->getWidth(); }
  quint32 NumConnections() { return this->numConnections(); }
  CarbonCfgRTLConnection* GetConnection(quint32 i) { return this->getConnection(i); }
#endif
public:
  CARBONMEM_OVERRIDES
  typedef UtArray<CarbonCfgRTLConnection*> ConnectionVec;

  CarbonCfgRTLPort(const char* name, UInt32 width, CarbonCfgRTLPortType type);
  ~CarbonCfgRTLPort();
  CarbonCfgRTLPort(UtIStream& f, CarbonCfg*, UtParamFile&,
                   CarbonCfgErrorHandler*, UInt32 ticksPerBeat);
  CarbonCfgRTLPort(QXmlStreamReader& f, CarbonCfg*, UtParamFile&,
                   CarbonCfgErrorHandler*, UInt32 ticksPerBeat);
  void write(CfgXmlWriter& xml);
  UInt32 numConnections() const {return mConnections.size();}
  CarbonCfgRTLConnection* getConnection(UInt32 i) const {
    INFO_ASSERT(i < mConnections.size(), "connection out of range");
    return mConnections[i];
  }
  void connect(CarbonCfgRTLConnection* c);
  void disconnect(CarbonCfgRTLConnection* c);
  const char* getName() const {return mName.c_str();}
  void clear();
  CarbonCfgRTLPortType getType() const {return mType;}

  UInt32 getWidth() const {return mWidth;}
  void putType(CarbonCfgRTLPortType type) { mType=type; }
  void putWidth(UInt32 width) { mWidth=width; }

  // Predicates
  bool isDisconnected() const { return numConnections() == 0; }
  bool isConnected() const { return !isDisconnected(); }
  bool isESLPort() const;
  bool isTied() const;
  bool isTiedToParam() const;
  bool isConnectedToXtor() const;
  bool isResetGen() const;
  bool isClockGen() const;
#ifndef SWIG
  bool isSystemCClock() const;
#endif

private:
  UtString mName;
  UInt32 mWidth;
  ConnectionVec mConnections;
  CarbonCfgRTLPortTypeIO mType;
};

struct CarbonCfgRTLConnection : public QObject, public QScriptable
{
  Q_OBJECT

  Q_PROPERTY(CcfgEnum::CarbonCfgRTLConnectionType Type READ GetType)
  Q_PROPERTY(CarbonCfgRTLPort* RtlPort READ GetRTLPort)
  Q_PROPERTY(CarbonCfgClockGen* ClockGen READ GetClockGen)
  Q_PROPERTY(CarbonCfgResetGen* ResetGen READ GetResetGen)
  Q_PROPERTY(CarbonCfgTie* Tie READ GetTie)
  Q_PROPERTY(CarbonCfgTieParam* TieParam READ GetTieParam)
  Q_PROPERTY(CarbonCfgXtorConn* XtorConn READ GetXtorConn)

public slots:
#ifndef SWIG
  CcfgEnum::CarbonCfgRTLConnectionType GetType() const
  {
    return static_cast<CcfgEnum::CarbonCfgRTLConnectionType>(getType());
  }
  CarbonCfgRTLPort* GetRTLPort() { return getRTLPort(); }
  CarbonCfgClockGen* GetClockGen() { return castClockGen(); }
  CarbonCfgResetGen* GetResetGen() { return this->castResetGen(); }
  CarbonCfgTie* GetTie() { return this->castTie(); }
  CarbonCfgTieParam* GetTieParam() { return this->castTieParam(); }
  CarbonCfgXtorConn* GetXtorConn() { return this->castXtorConn(); }
  CarbonCfgESLPort* GetESLPort() { return this->castESLPort(); }
#endif

public:
  CARBONMEM_OVERRIDES

  CarbonCfgRTLConnection(CarbonCfgRTLPort*);
  virtual ~CarbonCfgRTLConnection();

  //! read a connection from an input file, create it and return it
  /*! The CarbonCfg is needed to lookup xtors by name
   */
  static CarbonCfgRTLConnection* read(CarbonCfgRTLPort*, UtIStream&,
                                      CarbonCfg*, CarbonCfgErrorHandler*,
                                      UInt32 ticksPerBeat);
  static CarbonCfgRTLConnection* read(CarbonCfgRTLPort*, QXmlStreamReader&,
                                      CarbonCfg*, CarbonCfgErrorHandler*,
                                      UInt32 ticksPerBeat);

  //! Common interface to get a textual description of a connnection
  virtual void getText(UtString* buf) const = 0;

  //! comparison routine for sorting (uses getText)
  bool operator<(const CarbonCfgRTLConnection&) const;

  //! Can this co-exist with other connections, or is it exclusive?
  virtual bool isExclusive() const = 0;

  //! perform any connection-specific bookkeeping (see XtorConn)
  virtual void connect();

  //! perform any connection-specific bookkeeping (see XtorConn)
  virtual void disconnect();

  virtual CarbonCfgClockGen* castClockGen();
  virtual CarbonCfgResetGen* castResetGen();
  virtual CarbonCfgTie* castTie();
  virtual CarbonCfgTieParam* castTieParam();
  virtual CarbonCfgESLPort* castESLPort();
  virtual CarbonCfgXtorConn* castXtorConn();
  virtual CarbonCfgSystemCClock* castSystemCClock();

  virtual CarbonCfgRTLConnectionType getType() const = 0;
  virtual void write(CfgXmlWriter& os) = 0;

  CarbonCfgRTLPort* getRTLPort() const {return mRTLPort;}
protected:
  CarbonCfgRTLPort* mRTLPort;
};

struct CarbonCfgClockGen :  public CarbonCfgRTLConnection {
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES
  CarbonCfgClockGen(CarbonCfgRTLPort*,UInt32 init, UInt32 delay, UInt32 clockCycles, UInt32 compCycles, UInt32 duty);
  CarbonCfgClockGen(CarbonCfgRTLPort*, UtIStream&, CarbonCfg *, UInt32 ticksPerBeat);
  CarbonCfgClockGen(CarbonCfgRTLPort*, QXmlStreamReader&, CarbonCfg *, UInt32 ticksPerBeat);
  ~CarbonCfgClockGen();
  virtual void write(CfgXmlWriter& xml);
  virtual void getText(UtString* buf) const;
  virtual bool isExclusive() const;
  virtual CarbonCfgRTLConnectionType getType() const;
  virtual CarbonCfgClockGen* castClockGen();

  // 0 or 1 - this is the value to be deposited to the clock before
  // simulation begins.
  UInt32 mInitialValue;

  // percentage of clock cycle to delay before first clock cycle
  UInt32 mDelay;

  // frequency as a ratio of clock cycles to component cycles
  CarbonCfgClockFrequency mFrequency;

  // whole number (0-100) percentage of cycle where value = 1
  UInt32 mDutyCycle; 
};

struct CarbonCfgResetGen : CarbonCfgRTLConnection {
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES
  // Old Constructor for backward compatibility
#ifndef SWIG
  CarbonCfgResetGen(CarbonCfgRTLPort*,bool activeHigh, UInt32 clockCycles, UInt32 compCycles,
                    UInt32 cyclesBefore, UInt32 cyclesAsserted, UInt32 cyclesAfter);
#endif
  // New and improved constructor featuring arbitrary active and inactive values.
  CarbonCfgResetGen(CarbonCfgRTLPort*,UInt64 activeValue, UInt64 inactiveValue, UInt32 clockCycles, UInt32 compCycles,
                    UInt32 cyclesBefore, UInt32 cyclesAsserted, UInt32 cyclesAfter);
  CarbonCfgResetGen(CarbonCfgRTLPort*, UtIStream&, CarbonCfg *, UInt32 ticksPerBeat);
  CarbonCfgResetGen(CarbonCfgRTLPort*, QXmlStreamReader&, CarbonCfg *, UInt32 ticksPerBeat);
  ~CarbonCfgResetGen();
  virtual void getText(UtString* buf) const;
  virtual void write(CfgXmlWriter& xml);
  virtual bool isExclusive() const;
  virtual CarbonCfgRTLConnectionType getType() const;
  virtual CarbonCfgResetGen* castResetGen();

  UInt64 mActiveValue;
  UInt64 mInactiveValue;
  CarbonCfgClockFrequency mFrequency;
  UInt32 mCyclesBefore, mCyclesAsserted, mCyclesAfter;
};

//! Class to manage a SystemC clock
class CarbonCfgSystemCClock : public CarbonCfgRTLConnection
{
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES

  //! Constructor
  /*! Constructs a 1ns period clock with a 0.5 duty cycle and initial value of 0
   */
  CarbonCfgSystemCClock(CarbonCfgRTLPort* port);

  //! Cfg file read constructor
  CarbonCfgSystemCClock(CarbonCfgRTLPort* port, UtIStream& f);
  CarbonCfgSystemCClock(CarbonCfgRTLPort* port, QXmlStreamReader& f);

  //! Get the period (get the units separately)
  double getPeriod(void) const { return mPeriod; }

  //! Get the period units that goes with the period
  CarbonCfgSystemCTimeUnits getPeriodUnits(void) const { return mPeriodUnits; }

  //! Set the period and units
  void putPeriod(double period, CarbonCfgSystemCTimeUnits units)
  {
    mPeriod = period;
    mPeriodUnits = units;
  }

  //! Get the duty cycle (Indicates the period during which the clock is true)
  double getDutyCycle(void) { return mDutyCycle; }

  //! Set the duty cycle (Indicates the period during which the clock is true)
  void putDutyCycle(double dutyCycle) { mDutyCycle = dutyCycle; }

  //! Get the startTime (get the units separately)
  double getStartTime(void) const { return mStartTime; }

  //! Get the startTime units that goes with the startTime
  CarbonCfgSystemCTimeUnits getStartTimeUnits(void) const { return mStartTimeUnits; }

  //! Set the startTime and units
  void putStartTime(double startTime, CarbonCfgSystemCTimeUnits units)
  {
    mStartTime = startTime;
    mStartTimeUnits = units;
  }

  //! Get the initial value
  UInt32 getInitialValue(void) const { return mInitialValue; }

  //! Set the initial value
  void putInitialValue(UInt32 init) { mInitialValue = init; }

  //! Write the clock to the ccfg file
  void write(CfgXmlWriter& xml);

  //! Common interface to get a textual description of a connnection
  void getText(UtString* buf) const;

  //! Get the connection type
  CarbonCfgRTLConnectionType getType() const { return eCarbonCfgSystemCClock; }

  //! Can this co-exist with other connections, or is it exclusive?
  bool isExclusive() const { return true; }

  //! Cast to the SystemC clock
  CarbonCfgSystemCClock* castSystemCClock();

private:
  double mPeriod;
  CarbonCfgSystemCTimeUnits mPeriodUnits;
  double mDutyCycle;
  double mStartTime;
  CarbonCfgSystemCTimeUnits mStartTimeUnits;
  UInt32 mInitialValue;
}; // class CarbonCfgSystemCClock


struct CarbonCfgTie : CarbonCfgRTLConnection {
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES
  CarbonCfgTie(CarbonCfgRTLPort*, const DynBitVector& val);
  CarbonCfgTie(CarbonCfgRTLPort*, UtIStream&);
  CarbonCfgTie(CarbonCfgRTLPort*, QXmlStreamReader&);
  CarbonCfgTie(CarbonCfgRTLPort*, UInt32);
  CarbonCfgTie(CarbonCfgRTLPort*, UInt32, UInt32);
  CarbonCfgTie(CarbonCfgRTLPort*, UInt32, UInt32, UInt32, UInt32);
  ~CarbonCfgTie();
  virtual void getText(UtString* buf) const;
  virtual void write(CfgXmlWriter& xml);
  virtual bool isExclusive() const;
  virtual CarbonCfgRTLConnectionType getType() const;
  virtual CarbonCfgTie* castTie();

  DynBitVector mValue;
};

struct CarbonCfgTieParam : CarbonCfgRTLConnection {
  Q_OBJECT


  Q_PROPERTY(QString Param READ GetParam WRITE SetParam)
  Q_PROPERTY(QString XtorInstanceName READ GetXtorInstName WRITE SetXtorInstName)
  Q_PROPERTY(QString FullName READ GetFullName WRITE SetFullName)

public slots:
#ifndef SWIG
  QString GetParam() const { return this->mParam.c_str(); }
  void SetParam(const QString& newVal)
  {
    this->mParam.clear();
    this->mParam << newVal;
  }

  QString GetXtorInstName() const { return this->mXtorInstanceName.c_str(); }
  void SetXtorInstName(const QString& newVal)
  {
    this->mXtorInstanceName.clear();
    this->mXtorInstanceName << newVal;
  }

  QString GetFullName() const { return this->mFullParamName.c_str(); }
  void SetFullName(const QString& newVal)
  {
    this->mFullParamName.clear();
    this->mFullParamName << newVal;
  }
#endif

public:
  CARBONMEM_OVERRIDES
  CarbonCfgTieParam(CarbonCfgRTLPort*, const char* xtorInstName, const char* param);
  CarbonCfgTieParam(CarbonCfgRTLPort*, const UtString& param, const UtString& xtorInstName);
  CarbonCfgTieParam(CarbonCfgRTLPort*, UtIStream&);
  CarbonCfgTieParam(CarbonCfgRTLPort*, QXmlStreamReader&);
  ~CarbonCfgTieParam();
  virtual void getText(UtString* buf) const;
  virtual void write(CfgXmlWriter& xml);
  virtual bool isExclusive() const;
  virtual CarbonCfgRTLConnectionType getType() const;
  virtual CarbonCfgTieParam* castTieParam();

  UtString mParam;
  UtString mXtorInstanceName;
  UtString mFullParamName;
};

//! Connection to the ESL world.  This can co-exist with xtor connections
struct CarbonCfgESLPort : public CarbonCfgRTLConnection {
  Q_OBJECT

  Q_PROPERTY(QString Name READ GetName)
  Q_PROPERTY(QString Expr READ GetExpr WRITE SetExpr)
  Q_PROPERTY(QString TypeDef READ GetTypeDef)
  Q_PROPERTY(CarbonCfgRTLPort* RtlPort READ GetRtlPort)
  Q_PROPERTY(CarbonCfgESLPortType PortType READ GetPortType)
  Q_PROPERTY(CarbonCfgXtorParamInst* ParameterInstance READ GetParameterInstance WRITE SetParameterInstance)

public slots:
#ifndef SWIG
  QString GetName() { return this->getName(); }
  QString GetExpr() { return this->getExpr(); }
  CarbonCfgESLPortType GetPortType() { return this->getPortType(); }

  CarbonCfgXtorParamInst* GetParameterInstance() { return this->getParamInstance(); }
  void SetParameterInstance(CarbonCfgXtorParamInst* inst) { this->putParamInstance(inst); }

  void SetExpr(const QString& newVal)
  {
    UtString value;
    value << newVal;
    this->putExpr(value.c_str());
  }
  QString GetTypeDef() { return this->getTypeDef(); }
  CarbonCfgRTLPort* GetRtlPort() { return this->getRTLPort(); }
#endif
public:
  CARBONMEM_OVERRIDES

  CarbonCfgESLPort(CarbonCfgRTLPort*,const char*,CarbonCfgESLPortType,const char*,CarbonCfgESLPortMode);
  CarbonCfgESLPort(CarbonCfgRTLPort*,UtIStream&,CarbonCfgID);
  CarbonCfgESLPort(CarbonCfgRTLPort*,QXmlStreamReader&,CarbonCfgID);
  CarbonCfgESLPort(CarbonCfgESLPort*, CarbonCfgRTLPort*);

  ~CarbonCfgESLPort();
  virtual void getText(UtString* buf) const;
  virtual void write(CfgXmlWriter& xml);
  virtual bool isExclusive() const;
  const char* getName() const {return mName.c_str();}
  const char* getExpr() const {return mExpr.c_str();}
  const char* getTypeDef() const {return mTypeDef.c_str();}
  void putPortType(CarbonCfgESLPortType newVal) { mType=newVal; }
  CarbonCfgESLPortType getPortType() const { return mType; }
  CarbonCfgESLPortMode getMode() const {return mPortMode; }
  virtual CarbonCfgRTLConnectionType getType() const;
  virtual CarbonCfgESLPort* castESLPort();
  void putExpr(const char*);
  void putExpr(const QString& v);
  void putTypeDef(const char* typeDef) {mTypeDef=typeDef;}
  void putMode(CarbonCfgESLPortMode mode) {mPortMode=mode;}
#ifndef SWIG
  CarbonCfgESLPortTypeIO mType;
#endif
  CarbonCfgXtorParamInst* getParamInstance() { return mParamInstance; }
  void putParamInstance(CarbonCfgXtorParamInst* paramInstance) {
      mParamInstance = paramInstance;
      mParamName.clear();
      if (mParamInstance)
        mParamName = paramInstance->getParam()->getName();
  }
  const char* getParamName() const { return mParamName.c_str(); }
  void putParamName(const char* paramName) { mParamName << paramName; }
  const char* getParamXtorInstName() const { return mParamXtorInstName.c_str(); }
  void putParamXtorInstName(const char* transactorName) { mParamXtorInstName << transactorName; }
 
private:
  //! only the cfg can change the name, because it must maintain its maps
  friend struct CarbonCfg;
  void putName(const char* newName);
  
  UtString mName;
  UtString mExpr;
  UtString mTypeDef;
  CarbonCfgESLPortModeIO mPortMode;
  CarbonCfgXtorParamInst* mParamInstance;
  UtString mParamName;
  UtString mParamXtorInstName;
};

struct CarbonCfgXtorInstance  : public QObject, public QScriptable {
  Q_OBJECT

  Q_PROPERTY(QString Name READ GetName);
  Q_PROPERTY(quint32 NumConnections READ NumConnections);
  Q_PROPERTY(CarbonCfgXtorInstance* ClockMaster READ GetClockMaster WRITE SetClockMaster);
  Q_PROPERTY(CarbonCfgXtor* Xtor READ GetXtor);

public slots:
#ifndef SWIG
  CarbonCfgXtorInstance* GetClockMaster() { return this->getClockMaster(); }
  void SetClockMaster(CarbonCfgXtorInstance* clockMaster) { this->putClockMaster(clockMaster); }

  CarbonCfgXtorParamInst* FindParameter(const QString& paramName) 
  {
    UtString paramname;
    paramname << paramName;
    return this->findParameterInst(paramname.c_str());
  }

  // returns true if parameter was hidden
  bool HideParameter(const QString& paramName)
  {
    UtString paramname;
    paramname << paramName;
    return this->hideParameterInst(paramname.c_str());
  }
  void AddConnection(quint32 i, CarbonCfgXtorConn* conn) { this->putConnection(i, conn); }
  CarbonCfgXtorConn* GetConnection(quint32 i) { return this->getConnection(i); }
  QString GetName() const { return this->getName(); }
  quint32 NumConnections() const { return this->getType()->numPorts(); }
//  const quint32 NumParams() const { return this->numParams(); }
  void SetUseDebugAccess(bool val) { this->putUseDebugAccess(val); }
  CarbonCfgXtor* GetXtor() const { return this->getType(); }
#endif
public:
  CARBONMEM_OVERRIDES
  CarbonCfgXtorInstance(const char* name,
                        CarbonCfgXtor* type);
  CarbonCfgXtorInstance(UtIStream&, CarbonCfgID cfg);
  CarbonCfgXtorInstance(QXmlStreamReader&, CarbonCfgID cfg);
  ~CarbonCfgXtorInstance();
  void init();

  const char* getName() const {return mName.c_str();}
  CarbonCfgXtor* getType() const {return mXtorType;}
#ifndef SWIG
  void connect(CarbonCfgRTLPort*, UInt32 portIndex);
#endif
  void write(CfgXmlWriter&);
  CarbonCfgXtorConn* getConnection(UInt32 i) {
    INFO_ASSERT(i < mXtorType->numPorts(), "connection out of range");
    return mConnections[i];
  }
  void putConnection(UInt32 i, CarbonCfgXtorConn* conn) {
    INFO_ASSERT(i < mXtorType->numPorts(), "connection out of range");
    mConnections[i] = conn;
  }

  bool useDebugAccess() const {
    return mUseDebugAccess;
  }

  void putUseDebugAccess(bool val) {
    mUseDebugAccess = val;
  }
  typedef UtArray<CarbonCfgXtorParamInst*> ParamInstVec;
  typedef UtHashMap<UtString, CarbonCfgXtorParamInst*> ParamInstMap;
  void addParameter(CarbonCfgXtorInstance* inst, QXmlStreamReader& xr);

  bool addParameter(const UtString& name, const UtString& value)
  {
    CarbonCfgXtorParamInst *inst = new CarbonCfgXtorParamInst(this, mXtorType->findParam(name.c_str()));
    if (!inst->putValue(value.c_str())) {
      return false;
    }
    std::pair<ParamInstMap::iterator, bool> result;
    result = mParamInstMap->insert(ParamInstMap::value_type(name, inst));
    mParamInstVec.push_back(inst);
    return result.second;
  }
  bool addParameter(const UtString& name, const UtString& value, const UtStringArray& enumChoices)
  {
    CarbonCfgXtorParamInst *inst = new CarbonCfgXtorParamInst(this, mXtorType->findParam(name.c_str()));
    if (!enumChoices.empty()) {
      inst->putEnumChoices(enumChoices);
    }
    if (!inst->putValue(value.c_str())) {
      return false;
    }
    std::pair<ParamInstMap::iterator, bool> result;
    result = mParamInstMap->insert(ParamInstMap::value_type(name, inst));
    mParamInstVec.push_back(inst);
    return result.second;
  }
  
  const char* findParameter(const char* paramName)
  {
    UtString name;
    name << paramName;
    bool showHidden = true;

    ParamInstMap::iterator pos = mParamInstMap->find(name);
    if (pos != mParamInstMap->end()) {
      CarbonCfgXtorParamInst *inst = pos->second;
      if (showHidden || !inst->hidden()) {
        const char* value = inst->getValue();
        if (value == NULL || 0 == strlen(value))
          return inst->getDefaultValue();
        else
          return inst->getValue();
      }
    }
    return NULL;
  }

  const char* findParameter(const UtString& name, bool showHidden = true)
  {
    ParamInstMap::iterator pos = mParamInstMap->find(name);
    if (pos != mParamInstMap->end()) {
      CarbonCfgXtorParamInst *inst = pos->second;
      if (showHidden || !inst->hidden()) {
        const char* value = inst->getValue();
        if (value == NULL || 0 == strlen(value))
          return inst->getDefaultValue();
        else
          return inst->getValue();
      }
    }
    return NULL;
  }

  CarbonCfgXtorParamInst* findParameterInst(const char* paramName)
  {
    bool showHidden = true;
    UtString name;
    name << paramName;

    ParamInstMap::iterator pos = mParamInstMap->find(name);
    if (pos != mParamInstMap->end()) {
      CarbonCfgXtorParamInst *inst = pos->second;
      if (showHidden || !inst->hidden()) {
        return inst;
      }
    }
    return NULL;
  }


  CarbonCfgXtorParamInst* findParameterInst(const UtString& name, bool showHidden = true)
  {
    ParamInstMap::iterator pos = mParamInstMap->find(name);
    if (pos != mParamInstMap->end()) {
      CarbonCfgXtorParamInst *inst = pos->second;
      if (showHidden || !inst->hidden()) {
        return inst;
      }
    }
    return NULL;
  }

  // mark the named parameter as hidden, it will always exist
  bool hideParameterInst(const UtString& name)
  {
    // first make sure the parameter exists
    ParamInstMap::iterator pos = mParamInstMap->find(name);
    if (pos != mParamInstMap->end()) {
      CarbonCfgXtorParamInst *inst = pos->second;
      inst->putHidden(true);
      return true;
    } else {
      return false;
    }
  }

  unsigned int numParams()
  {
    return mParamInstVec.size();
  }

  CarbonCfgXtorParamInst* getParamInstance(unsigned int i, bool showHidden = true)
  {
    CarbonCfgXtorParamInst *inst = mParamInstVec[i];
    const UtString name = inst->getParam()->getName();
    if ( showHidden || !inst->hidden() ) {
      return inst;
    } else {
      return NULL;
    }
  }

  CarbonCfgXtorInstance* getClockMaster()
  {
    INFO_ASSERT(!mXtorType->useESLClockMaster(), "Transactor clock master not legal for this transactor type");
    return mClockMaster;
  }

  void putClockMaster(CarbonCfgXtorInstance *clockMaster)
  {
    INFO_ASSERT(!mXtorType->useESLClockMaster(), "Transactor clock master not legal for this transactor type");
    mClockMaster = clockMaster;
  }

  const char* getESLClockMaster() const
  {
    INFO_ASSERT(mXtorType->useESLClockMaster(), "ESL clock master not legal for this transactor type");
    return mESLClockMaster.c_str();
  }

  void putESLClockMaster(const char* eslClockMaster)
  {
    INFO_ASSERT(mXtorType->useESLClockMaster(), "ESL clock master not legal for this transactor type");
    mESLClockMaster = eslClockMaster;
  }

  const char* getESLResetMaster() const
  {
    INFO_ASSERT(mXtorType->useESLResetMaster(), "ESL reset master not legal for this transactor type");
    return mESLResetMaster.c_str();
  }

  void putESLResetMaster(const char* eslResetMaster)
  {
    INFO_ASSERT(mXtorType->useESLResetMaster(), "ESL reset master not legal for this transactor type");
    mESLResetMaster = eslResetMaster;
  }

  //! Set the interface name (msut be available in the CarbonCfgXtor::getAbstractions()->getName())
  void putAbstraction(const char* abstraction);

  //! Get the interface information for this instance (off the mXtorType)
  /*! Function returns NULL if the interface name does not match one in the xtor type
   */
  CarbonCfgXtorAbstraction* getAbstraction(void) const;

private:
  friend struct CarbonCfg;
  void putName(const char* newName);

  UtString mName;
  CarbonCfgXtor* mXtorType;
  UtString mAbstraction;
  CarbonCfgXtorConn** mConnections;
  ParamInstMap* mParamInstMap;
  ParamInstVec mParamInstVec;
  CarbonCfgXtorInstance *mClockMaster;
  int mUseDebugAccess;
  UtString mESLClockMaster;
  UtString mESLResetMaster;
};

struct CarbonCfgXtorConn : public CarbonCfgRTLConnection {
  Q_OBJECT
  
  Q_PROPERTY(CarbonCfgRTLPort* RtlPort READ GetRTLPort);
  Q_PROPERTY(QString Expr READ GetExpr WRITE SetExpr);

public slots:
#ifndef SWIG
  CarbonCfgRTLPort* GetRTLPort() const { return this->getRTLPort(); }
  QString GetExpr() { return this->getExpr(); }
  void SetExpr(const QString& newVal)
  {
    UtString value;
    value << newVal;
    this->putExpr(value.c_str());
  }
#endif
public:
  CARBONMEM_OVERRIDES
  CarbonCfgXtorConn(CarbonCfgRTLPort*,
                    CarbonCfgXtorInstance*,
                    UInt32 portIndex);
  CarbonCfgXtorConn(CarbonCfgRTLPort*,UtIStream&, CarbonCfg*);
  CarbonCfgXtorConn(CarbonCfgRTLPort*,QXmlStreamReader&, CarbonCfg*);

  ~CarbonCfgXtorConn();
  virtual void getText(UtString* buf) const;
  virtual void write(CfgXmlWriter& f);
  virtual bool isExclusive() const;
  virtual CarbonCfgRTLConnectionType getType() const;
  virtual CarbonCfgXtorConn* castXtorConn();
  virtual void connect();
  virtual void disconnect();
  CarbonCfgXtorPort* getPort() const;
  CarbonCfgXtorInstance* getInstance() const {return mInstance;}
  UInt32 getPortIndex() const {return mPortIndex;}
  const char* getExpr() const {return mExpr.c_str();}
  void putExpr(const char*);
  bool valid() const { return mValid; }

  //! Used to store the SystemC type
  void putTypeDef(const char* typeDef) {mTypeDef=typeDef;}

  //! Used to retrieve the SystemC type
  /*! Note that some transactors store the SystemC type by connection
   *  and others store it by defintion.  This function returns a value
   *  computed automatically. If you need the type defined in the XML
   *  file, then use CarbonCfgXtorPort::getTypeDef.
   */
  const char* getTypeDef() const { return mTypeDef.c_str(); };

private:
  CarbonCfgXtorInstance* mInstance;
  UInt32 mPortIndex;
  UtString mExpr;
  UtString mTypeDef;
  bool mValid;
}; // struct CarbonCfgXtorConn : public CarbonCfgRTLConnection

struct CarbonCfgGroup  : public QObject, public QScriptable  {
  Q_OBJECT

  Q_PROPERTY(QString Name READ GetName)

  
public slots:
#ifndef SWIG
  QString GetName() const { return getName(); }
#endif

public:
  CARBONMEM_OVERRIDES

  CarbonCfgGroup(const char* name): mName(name) {}
  const char* getName() const {return mName.c_str();}

private:
  friend class CarbonCfg;
  void putName(const char* name) {mName = name;}
  
  UtString mName;
};

struct CarbonCfgRegisterLoc  : public QObject, public QScriptable{
  Q_OBJECT
  
public:
  Q_PROPERTY(CarbonCfgRegisterLocConstant* CastConstant READ CastConstant)
  Q_PROPERTY(CarbonCfgRegisterLocRTL* CastRTL READ CastRTL)
  Q_PROPERTY(CarbonCfgRegisterLocReg* CastReg READ CastReg)
  Q_PROPERTY(CarbonCfgRegisterLocArray* CastArray READ CastArray)
  Q_PROPERTY(CarbonCfgRegisterLocUser* CastUser READ CastUser)
  Q_PROPERTY(CcfgEnum::CarbonCfgRegisterLocKind Type READ GetType)


public slots:
#ifndef SWIG
  CarbonCfgRegisterLocConstant* CastConstant() { return this->castConstant(); }
  CarbonCfgRegisterLocRTL* CastRTL() { return this->castRTL(); }
  CarbonCfgRegisterLocReg* CastReg() { return this->castReg(); }
  CarbonCfgRegisterLocArray* CastArray() { return this->castArray(); }
  CarbonCfgRegisterLocUser* CastUser() { return this->castUser(); }
  CcfgEnum::CarbonCfgRegisterLocKind GetType() { return CcfgEnum::CarbonCfgRegisterLocKind(this->getType()); }
#endif

public:
  CARBONMEM_OVERRIDES

  virtual ~CarbonCfgRegisterLoc();

  virtual void write(CfgXmlWriter &xml) = 0;

  virtual CarbonCfgRegisterLocConstant *castConstant();
  virtual CarbonCfgRegisterLocRTL *castRTL();
  virtual CarbonCfgRegisterLocReg *castReg();
  virtual CarbonCfgRegisterLocArray *castArray();
  virtual CarbonCfgRegisterLocUser *castUser();
  virtual CarbonCfgRegLocType getType() const = 0;

  //! Clone this register locator replacing any variables with values
  /*! This function must be overloaded by the derived class so that
   *  registers can be cloned.
   */
  virtual CarbonCfgRegisterLoc* clone(const CarbonCfgTemplate& templ) const = 0;

protected:
  CarbonCfgRegisterLoc();

private:
  // Hide the copy constructor
  CarbonCfgRegisterLoc(const CarbonCfgRegisterLoc&);

  //! Hide the assignment constructor
  CarbonCfgRegisterLoc& operator=(const CarbonCfgRegisterLoc&);
};

struct CarbonCfgRegisterLocConstant : public CarbonCfgRegisterLoc {
  Q_OBJECT

  Q_PROPERTY(quint64 Value READ GetValue WRITE SetValue)

public slots:
#ifndef SWIG
  quint64 GetValue() const { return getValue(); }
  void SetValue(quint64 newVal)
  {
    putValue(newVal);
  }
#endif
public:
  CARBONMEM_OVERRIDES

  CarbonCfgRegisterLocConstant(CarbonUInt64 constant_value);
  virtual ~CarbonCfgRegisterLocConstant();

  virtual void write(CfgXmlWriter &xml);

  CarbonUInt64 getValue() const {return mValue;}

  void putValue(CarbonUInt64 val) {mValue = val;}

  virtual CarbonCfgRegisterLocConstant *castConstant();
  virtual CarbonCfgRegLocType getType() const;

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the register location and replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  virtual CarbonCfgRegisterLoc* clone(const CarbonCfgTemplate& templ) const;

private:
  // Hide the copy constructor
  CarbonCfgRegisterLocConstant(const CarbonCfgRegisterLocConstant&);

  //! Hide the assignment constructor
  CarbonCfgRegisterLocConstant& operator=(const CarbonCfgRegisterLocConstant&);

  CarbonUInt64 mValue;
};

struct CarbonCfgRegisterLocRTL : public CarbonCfgRegisterLoc {
  Q_OBJECT

  Q_PROPERTY(bool HasPath READ HasPath)
  Q_PROPERTY(bool HasRange READ GetHasRange)
  Q_PROPERTY(QString Path READ GetPath WRITE SetPath)
  Q_PROPERTY(quint32 Left READ GetLeft WRITE SetLeft)
  Q_PROPERTY(quint32 Right READ GetRight WRITE SetRight)
  Q_PROPERTY(quint32 Lsb READ GetLSB WRITE SetLSB)
  Q_PROPERTY(quint32 Msb READ GetMSB WRITE SetMSB)
 
public slots:
#ifndef SWIG
  bool GetHasRange() const { return this->getHasRange(); }
  bool HasPath() const { return this->hasPath(); }
  QString GetPath() const { return this->getPath(); }
  void SetPath(const QString& newVal)
  {
    UtString value;
    value << newVal;
    this->putPath(value.c_str());
  }
  quint32 GetLeft() const { return this->getLeft(); }
  void SetLeft(quint32 newVal) { this->putLeft(newVal); }

  quint32 GetRight() const { return this->getRight(); }
  void SetRight(quint32 newVal) { this->putRight(newVal); }

  quint32 GetLSB() const { return this->getLSB(); }
  void SetLSB(quint32 newVal) { this->putLSB(newVal); }

  quint32 GetMSB() const { return this->getMSB(); }
  void SetMSB(quint32 newVal) { this->putMSB(newVal); }
#endif
public:
  CARBONMEM_OVERRIDES

  CarbonCfgRegisterLocRTL(const char *rtl_path);
  CarbonCfgRegisterLocRTL(const char *rtl_path,
                          CarbonUInt32 left, CarbonUInt32 right);
  virtual ~CarbonCfgRegisterLocRTL();

  bool hasPath() const { return mPath.length() > 0; }
  const char *getPath() const {return mPath.c_str();}
  bool getHasRange() const {return mHasRange;}
  CarbonUInt32 getLeft() const {return mLeft;}
  CarbonUInt32 getRight() const {return mRight;}
  CarbonUInt32 getLSB() const {return mLSB;}
  CarbonUInt32 getMSB() const {return mMSB;}

  void putPath(const char *val) {mPath = val;}
  void putHasRange(bool val) {mHasRange = val;}
  void putLeft(CarbonUInt32 val) {mLeft = val;}
  void putRight(CarbonUInt32 val) {mRight = val;}
  void putLSB(CarbonUInt32 val) {mLSB = val;}
  void putMSB(CarbonUInt32 val) {mMSB = val;}

  virtual CarbonCfgRegisterLocRTL *castRTL();

  //! Clone so that we can use in a different component/sub-component.
  /*! This function helps the derived classes clone members that are
   *  specific to an RTL location. It replaces template variables with
   *  the corresponding values from the CarbonCfgTemplate variable
   *  map.
   *
   *  If you add more fields here, please update this function.
   */
  void cloneHelper(CarbonCfgRegisterLocRTL* clone) const;

private:
  // Hide the copy constructor
  CarbonCfgRegisterLocRTL(const CarbonCfgRegisterLocRTL&);

  //! Hide the assignment constructor
  CarbonCfgRegisterLocRTL& operator=(const CarbonCfgRegisterLocRTL&);

  UtString mPath;
  //! Left index of this range within the physical register/array element
  CarbonUInt32 mLeft;
  //! Right index of this range within the physical register/array element
  CarbonUInt32 mRight;
  //! LSB of the physical register/array element
  CarbonUInt32 mLSB;
  //! MSB of the physical register/array element
  CarbonUInt32 mMSB;
  bool mHasRange;
};

struct CarbonCfgRegisterLocReg : public CarbonCfgRegisterLocRTL {
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES

  CarbonCfgRegisterLocReg(const char *rtl_path);
  CarbonCfgRegisterLocReg(const char *rtl_path,
                          CarbonUInt32 left, CarbonUInt32 right);
  virtual ~CarbonCfgRegisterLocReg();

  virtual void write(CfgXmlWriter &xml);

  virtual CarbonCfgRegisterLocReg *castReg();
  virtual CarbonCfgRegLocType getType() const;

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the register location and replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  virtual CarbonCfgRegisterLoc* clone(const CarbonCfgTemplate& templ) const;

private:
  // Hide the copy constructor
  CarbonCfgRegisterLocReg(const CarbonCfgRegisterLocReg&);

  //! Hide the assignment constructor
  CarbonCfgRegisterLocReg& operator=(const CarbonCfgRegisterLocReg&);
};

struct CarbonCfgRegisterLocArray : public CarbonCfgRegisterLocRTL {
  Q_OBJECT
  
  Q_PROPERTY(quint32 Index READ GetIndex WRITE SetIndex)

public slots:
#ifndef SWIG
  quint32 GetIndex() const { return getIndex(); }
  void SetIndex(quint32 newVal) { putIndex(newVal); }
#endif
public:
  CARBONMEM_OVERRIDES

  CarbonCfgRegisterLocArray(const char *rtl_path, CarbonUInt32 index);
  CarbonCfgRegisterLocArray(const char *rtl_path, CarbonUInt32 index,
                            CarbonUInt32 left, CarbonUInt32 right);
  virtual ~CarbonCfgRegisterLocArray();

  virtual void write(CfgXmlWriter &xml);

  CarbonUInt32 getIndex() const {return mIndex;}

  void putIndex(CarbonUInt32 val) {mIndex = val;}

  virtual CarbonCfgRegisterLocArray *castArray();
  virtual CarbonCfgRegLocType getType() const;

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the register location and replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  virtual CarbonCfgRegisterLoc* clone(const CarbonCfgTemplate& templ) const;

private:
  // Hide the copy constructor
  CarbonCfgRegisterLocArray(const CarbonCfgRegisterLocArray&);

  //! Hide the assignment constructor
  CarbonCfgRegisterLocArray& operator=(const CarbonCfgRegisterLocArray&);

  //! Index into the array to find the logical register
  CarbonUInt32 mIndex;
};

struct CarbonCfgRegisterLocUser : public CarbonCfgRegisterLoc
{
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES

  //! Constructor (user type has not data)
  CarbonCfgRegisterLocUser();

  //! Destructor
  ~CarbonCfgRegisterLocUser();

  //! Function to write to the XML file
  void write (CfgXmlWriter &xml);

  //! Only override the LocUser cast function
  CarbonCfgRegisterLocUser* castUser();

  //! get the type
  CarbonCfgRegLocType getType() const;

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the register location and replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  virtual CarbonCfgRegisterLoc* clone(const CarbonCfgTemplate& templ) const;

private:
  // Hide the copy constructor
  CarbonCfgRegisterLocUser(const CarbonCfgRegisterLocUser&);

  //! Hide the assignment constructor
  CarbonCfgRegisterLocUser& operator=(const CarbonCfgRegisterLocUser&);
};

struct CarbonCfgRegisterField : public QObject, public QScriptable {
  Q_OBJECT
    
  Q_PROPERTY(QString Name READ GetName WRITE SetName)
  Q_PROPERTY(quint32 Width READ GetWidth)
  Q_PROPERTY(quint32 High READ GetHigh WRITE SetHigh)
  Q_PROPERTY(quint32 Low READ GetLow WRITE SetLow)
  Q_PROPERTY(CcfgEnum::CarbonCfgRegAccessType Access READ GetAccess WRITE SetAccess)
  Q_PROPERTY(CarbonCfgRegisterLoc* Location READ GetLoc)
  Q_PROPERTY(CarbonCfgRegisterLocReg* RegLoc READ GetRegLoc)
  Q_PROPERTY(CarbonCfgRegisterLocConstant* ConstantLoc READ GetConstantLoc)
  Q_PROPERTY(CarbonCfgRegisterLocArray* ArrayLoc READ GetArrayLoc)
  Q_PROPERTY(CarbonCfgRegisterLocUser* UserLoc READ GetUserLoc)

#ifndef SWIG
public:
  static CcfgEnum::CarbonCfgRegAccessType toAccessType(const QString& typeName)
  {
    if (typeName == "RW")
      return CcfgEnum::RW;
    else if (typeName == "RO")
      return CcfgEnum::RO;
    else if (typeName == "WO")
      return CcfgEnum::WO;
    return CcfgEnum::RW;
  }
#endif

public slots:
#ifndef SWIG
  CarbonCfgRegisterLocReg* AddLocReg(const QString& path);
  CarbonCfgRegisterLocReg* AddLocReg(const QString& path, quint32 left, quint32 right);

  CarbonCfgRegisterLocArray* AddLocArray(const QString& path, quint32 index);
  CarbonCfgRegisterLocArray* AddLocArray(const QString& path, quint32 index, quint32 left, quint32 right);

  quint32 GetWidth() const { return this->getWidth(); }
 
  quint32 GetHigh() const { return this->getHigh(); }
  void SetHigh(quint32 nv) { this->putHigh(nv); }

  quint32 GetLow() const { return this->getLow(); }
  void SetLow(quint32 nv) { this->putLow(nv); }

  QString GetName() const { return this->getName(); }
  void SetName(const QString& newVal)
  {
    UtString value;
    value << newVal;
    this->putName(value.c_str());
  }
  CarbonCfgRegisterLoc* GetLoc() const { return this->getLoc(); }

  void SetLoc(QVariant loc)
  {
    CarbonCfgRegisterLocReg* lr = qvariant_cast<CarbonCfgRegisterLocReg*>(loc);
    if (lr)
      SetLocReg(lr);

    CarbonCfgRegisterLocConstant* lc = qvariant_cast<CarbonCfgRegisterLocConstant*>(loc);
    if (lc)
      SetLocConstant(lc);

    CarbonCfgRegisterLocArray* la = qvariant_cast<CarbonCfgRegisterLocArray*>(loc);
    if (la)
      SetLocArray(la);

    CarbonCfgRegisterLocUser* lu = qvariant_cast<CarbonCfgRegisterLocUser*>(loc);
    if (lu)
      SetLocUser(lu);
  }
  void SetLocReg(CarbonCfgRegisterLocReg* newVal) { this->putLoc(newVal); }
  void SetLocConstant(CarbonCfgRegisterLocConstant* newVal) { this->putLoc(newVal); }
  void SetLocArray(CarbonCfgRegisterLocArray* newVal) { this->putLoc(newVal); }
  void SetLocUser(CarbonCfgRegisterLocUser* newVal) { this->putLoc(newVal); }
  
  CarbonCfgRegisterLocReg* GetRegLoc() { return this->getLoc()->castReg(); }
  CarbonCfgRegisterLocConstant* GetConstantLoc() { return this->getLoc()->castConstant(); }
  CarbonCfgRegisterLocArray* GetArrayLoc() { return this->getLoc()->castArray(); }
  CarbonCfgRegisterLocUser* GetUserLoc() { return this->getLoc()->castUser(); }
  
  CcfgEnum::CarbonCfgRegAccessType GetAccess() const
  {
    CarbonCfgRegAccessTypeIO accType = this->getAccess();
    return static_cast<CcfgEnum::CarbonCfgRegAccessType>((int)accType);
  }
  void SetAccess(CcfgEnum::CarbonCfgRegAccessType newVal) 
  { 
    this->putAccess(static_cast<CarbonCfgRegAccessType>(newVal));
  }
#endif

public:
  CARBONMEM_OVERRIDES

  CarbonCfgRegisterField(CarbonUInt32 high, CarbonUInt32 low,
                         CarbonCfgRegAccessType access);
  virtual ~CarbonCfgRegisterField();

  virtual void write(CfgXmlWriter &xml);

  CarbonUInt32 getWidth() const {return CBITWIDTH((int)mHigh,(int)mLow);}
  CarbonUInt32 getHigh() const {return mHigh;}
  CarbonUInt32 getLow() const {return mLow;}
  CarbonCfgRegAccessType getAccess() const {return mAccess;}

  CarbonCfgRegisterLoc *getLoc() const {return mLoc;}
  const char* getName() const {return mName.c_str();}
  void putName(const char* newName) {mName = newName; }

  void putHigh(CarbonUInt32 val) {mHigh = val;}
  void putLow(CarbonUInt32 val) {mLow = val;}
  void putAccess(CarbonCfgRegAccessType val) {mAccess = val;}
  void putLoc(CarbonCfgRegisterLoc *val) {mLoc = val;}

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the register field and its subparts. It
   *  replaces any template variables with the corresponding values
   *  from the CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  CarbonCfgRegisterField* clone(const CarbonCfgTemplate& templ) const;

private:
  //! Hide the copy constructor
  CarbonCfgRegisterField(const CarbonCfgRegisterField& src);

  //! Hide the assignment constructor
  CarbonCfgRegisterField& operator=(const CarbonCfgRegisterField& src);

  UtString mName;
  //! High index of this range within the logical register
  CarbonUInt32 mHigh;
  //! Low index of this range within the logical register
  CarbonUInt32 mLow;
  CarbonCfgRegAccessType mAccess;
  CarbonCfgRegisterLoc *mLoc;
};

class CarbonCfgCustomCode : public QObject, public QScriptable
{
  Q_OBJECT

  Q_PROPERTY(CcfgEnum::CarbonCfgCustomCodePosition Position READ GetPosition WRITE SetPosition);
  Q_PROPERTY(QString Code READ GetCode WRITE SetCode);
  Q_PROPERTY(CcfgEnum::CarbonCfgElementGenerationType ElementGenerationType READ GetElementGenerationType WRITE SetElementGenerationType);


public slots:
#ifndef SWIG
  CcfgEnum::CarbonCfgElementGenerationType GetElementGenerationType() const { return CcfgEnum::CarbonCfgElementGenerationType(this->getElementGenerationType()); }
  void SetElementGenerationType(CcfgEnum::CarbonCfgElementGenerationType wt) { this->putElementGenerationType(CarbonCfgElementGenerationType(wt)); }

  CcfgEnum::CarbonCfgCustomCodePosition GetPosition()
  {
    return CcfgEnum::CarbonCfgCustomCodePosition(this->getPosition());
  }
  void SetPosition(CcfgEnum::CarbonCfgCustomCodePosition position)
  {
    this->putPosition(CarbonCfgCustomCodePosition(position));
  }
  QString GetCode() { return this->getCode(); }
  void SetCode(const QString& code)
  {
    UtString newCode;
    newCode << code;
    this->putCode(newCode.c_str());
  }
#endif
public:
  CARBONMEM_OVERRIDES

  //! constructor
  CarbonCfgCustomCode();

  //! Destructor
  virtual ~CarbonCfgCustomCode();

  //! Cast to a component custom code
  virtual CarbonCfgCompCustomCode* castComp() { return NULL; }

  //! Cast to a Cadi custom code
  virtual CarbonCfgCadiCustomCode* castCadi() { return NULL; }

  //! Cast to a register custom code
  virtual CarbonCfgRegCustomCode* castReg() { return NULL; }

  //! Cast to a memory block custom code
  virtual CarbonCfgMemoryBlockCustomCode* castMemoryBlock() { return NULL; }

  //! Cast to a memory block custom code
  virtual CarbonCfgMemoryCustomCode* castMemory() { return NULL; }

  //! Put the position (replaces existing string)
  /*! The position can either be PRE or POST
   */
  void putPosition(CarbonCfgCustomCodePosition position)
  {
    mPosition = position;
  }

  //! Put the code (replaces existing string)
  /*! The code can be any legal C++ code for this particular component
   *  section
   */
  void putCode(const char* code);

  //! Put the section as a string
  void putSectionString(const char* str) { mSection = str; }

  //! Get the section as a string
  virtual const char* getSectionString(void) const { return mSection.c_str(); }

  //! Write this custom code back out
  /*! This only writes the positon and code attributes. The derived
   *  class is responsible for writing the element and section
   *  attributes.
   */
  virtual void write(CfgXmlWriter& xml);

  //! Get the position (PRE or POST)
  CarbonCfgCustomCodePosition getPosition(void) const { return mPosition; }

  //! Get the custom code text
  const char* getCode(void) const { return mCode.c_str(); }
  
  //! Clone this custom code in place from another one
  void cloneInPlace(const CarbonCfgCustomCode& other,
                    const CarbonCfgTemplate& templ)
  {
    mSection = other.mSection;
    mPosition = other.mPosition;
    
    // Convert variables in the code
    templ.resolveString(other.getCode(), &mCode);
  }
  
  void putElementGenerationType(CarbonCfgElementGenerationType t) {mElementGenerationType = t;}
  CarbonCfgElementGenerationType getElementGenerationType() const { return mElementGenerationType; }

protected:
  //! Storage for the position
  CarbonCfgCustomCodePosition mPosition;

  //! Storage for the Section String
  UtString mSection;

  //! Storage for the code text
  UtString mCode;

private:
  //! Hide the copy cgonstructor
  CarbonCfgCustomCode(const CarbonCfgCustomCode& src);

  //! Hide the assignment constructor
  CarbonCfgCustomCode& operator=(const CarbonCfgCustomCode& src);

private:
  CarbonCfgElementGenerationType mElementGenerationType;
}; // class CarbonCfgCustomCode

class CarbonCfgMemoryBlockCustomCode : public CarbonCfgCustomCode
{
  Q_OBJECT

  Q_PROPERTY(CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection Section READ GetSection WRITE SetSection);

public slots:
#ifndef SWIG
  CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection GetSection()
  {
    return CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection(this->getSection());
  }
  void SetSection(CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection section)
  {
    this->putSection(CarbonCfgMemoryBlockCustomCodeSection(section));
  }
#endif

public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonCfgMemoryBlockCustomCode();

  //! Destructor
  ~CarbonCfgMemoryBlockCustomCode();

  //! Cast to the memory block type
  virtual CarbonCfgMemoryBlockCustomCode* castMemoryBlock() { return this; }

  //! Put the section
  void putSection(CarbonCfgMemoryBlockCustomCodeSection section);
  //! Get the section as an enum
  CarbonCfgMemoryBlockCustomCodeSection getSection(void) const;

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the custom code and replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  CarbonCfgMemoryBlockCustomCode* clone(const CarbonCfgTemplate& templ) const;

private:
  //! Hide the copy constructor
  CarbonCfgMemoryBlockCustomCode(const CarbonCfgMemoryBlockCustomCode& src);

  //! Hide the assignment constructor
  /*! If you add more fields here, please update this function.
   */
  CarbonCfgMemoryBlockCustomCode& operator=(const CarbonCfgMemoryBlockCustomCode& src);
}; // class CarbonCfgMemoryBlockCustomCode : public CarbonCfgCustomCode

class CarbonCfgMemoryCustomCode : public CarbonCfgCustomCode
{
  Q_OBJECT

  Q_PROPERTY(CcfgEnum::CarbonCfgMemoryCustomCodeSection Section READ GetSection WRITE SetSection);

public slots:
#ifndef SWIG
  CcfgEnum::CarbonCfgMemoryCustomCodeSection GetSection()
  {
    return CcfgEnum::CarbonCfgMemoryCustomCodeSection(this->getSection());
  }
  void SetSection(CcfgEnum::CarbonCfgMemoryCustomCodeSection section)
  {
    this->putSection(CarbonCfgMemoryCustomCodeSection(section));
  }
#endif

public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonCfgMemoryCustomCode();

  //! Destructor
  ~CarbonCfgMemoryCustomCode();

  //! Cast to the memory type
  virtual CarbonCfgMemoryCustomCode* castMemory() { return this; }

  //! Put the section
  void putSection(CarbonCfgMemoryCustomCodeSection section);

  //! Get the section as an enum
  CarbonCfgMemoryCustomCodeSection getSection(void) const;

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the custom code and replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  CarbonCfgMemoryCustomCode* clone(const CarbonCfgTemplate& templ) const;

private:
  //! Hide the copy constructor
  CarbonCfgMemoryCustomCode(const CarbonCfgMemoryCustomCode& src);

  //! Hide the assignment constructor
  /*! If you add more fields here, please update this function.
   */
  CarbonCfgMemoryCustomCode& operator=(const CarbonCfgMemoryCustomCode& src);
}; // class CarbonCfgMemoryCustomCode : public CarbonCfgCustomCode

class CarbonCfgCustomCodeContainer : public QObject, public QScriptable
{
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES
  
  //! Constructor
  CarbonCfgCustomCodeContainer(const char** sections) : QObject(0), mLegalSections(sections) {}

  //! Add a code sections to the container
  void addCustomCode(CarbonCfgCustomCode* cc)
  {
    mCustomCodeVec.push_back(cc);
  }

  //! Return number of code sections
  CarbonUInt32 numCustomCodes() const {return mCustomCodeVec.size();}

  //! Return the specified code section
  CarbonCfgCustomCode* getCustomCode(CarbonUInt32 i) const {
    INFO_ASSERT(i < mCustomCodeVec.size(), "CustomCodeList out of range");
    return mCustomCodeVec[i];
  }

  //! Remove specified code section from the container
  void removeCustomCode(CarbonCfgCustomCode* f)
  {
    mCustomCodeVec.remove(f);
    delete f;
  }

  //! Return Legal section names
  const char** getLegalSections()
  {
    return mLegalSections;
  }

  //! Write customs code to xml file
  void writeCustomCodes(CfgXmlWriter &xml);

  //! Remove all the custom codes in this containder
  void removeCustomCodes(void);

  //! Clear all the custom codes but don't delete the memory
  void clearCustomCodes(void);
  
protected:
  //! Storage for custom code sections
  UtArray<CarbonCfgCustomCode*> mCustomCodeVec;
  
  //! Defines the legal sections for this container
  //! Meant to be passed a static character array like the one
  //! returned by a UtIOEnum::getStrings()
  const char** mLegalSections;
};

class CarbonCfgCompCustomCode : public CarbonCfgCustomCode
{
  Q_OBJECT

  Q_PROPERTY(CcfgEnum::CarbonCfgCompCustomCodeSection Section READ GetSection WRITE SetSection);

public slots:
#ifndef SWIG
  CcfgEnum::CarbonCfgCompCustomCodeSection GetSection()
  {
    return CcfgEnum::CarbonCfgCompCustomCodeSection(this->getSection());
  }
  void SetSection(CcfgEnum::CarbonCfgCompCustomCodeSection section)
  {
    this->putSection(CarbonCfgCompCustomCodeSection(section));
  }
#endif

public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonCfgCompCustomCode();

  //! Destructor
  ~CarbonCfgCompCustomCode();

  //! Cast to a component custom code
  virtual CarbonCfgCompCustomCode* castComp() { return this; }

  //! Put the section
  void putSection(CarbonCfgCompCustomCodeSection section);

  //! Get the section as an enum
  CarbonCfgCompCustomCodeSection getSection(void) const;

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the custom code and replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  CarbonCfgCompCustomCode* clone(const CarbonCfgTemplate& templ) const;

private:
  //! Hide the copy constructor
  CarbonCfgCompCustomCode(const CarbonCfgCompCustomCode& src);

  //! Hide the assignment constructor
  CarbonCfgCompCustomCode& operator=(const CarbonCfgCompCustomCode& src);

  //! The custom code section
  CarbonCfgCompCustomCodeSection mSection;
}; // class CarbonCfgCompCustomCode : public CarbonCfgCustomCode

class CarbonCfgCadiCustomCode : public CarbonCfgCustomCode
{
  Q_OBJECT

  Q_PROPERTY(CcfgEnum::CarbonCfgCadiCustomCodeSection Section READ GetSection WRITE SetSection);

public slots:
#ifndef SWIG
  CcfgEnum::CarbonCfgCadiCustomCodeSection GetSection()
  {
    return CcfgEnum::CarbonCfgCadiCustomCodeSection(this->getSection());
  }
  void SetSection(CcfgEnum::CarbonCfgCadiCustomCodeSection section)
  {
    this->putSection(CarbonCfgCadiCustomCodeSection(section));
  }
#endif

public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonCfgCadiCustomCode();

  //! Destructor
  ~CarbonCfgCadiCustomCode();

  //! Cast to a Cadi custom code
  virtual CarbonCfgCadiCustomCode* castCadi() { return this; }

  //! Put the section
  void putSection(CarbonCfgCadiCustomCodeSection section);

  //! Get the section as an enum
  CarbonCfgCadiCustomCodeSection getSection(void) const;

  //! Clone so that we can use in a different Cadi class.
  /*! This function clones the custom code and replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  CarbonCfgCadiCustomCode* clone(const CarbonCfgTemplate& templ) const;

private:
  //! Hide the copy constructor
  CarbonCfgCadiCustomCode(const CarbonCfgCadiCustomCode& src);

  //! Hide the assignment constructor
  CarbonCfgCadiCustomCode& operator=(const CarbonCfgCadiCustomCode& src);

  //! The custom code section
  CarbonCfgCadiCustomCodeSection mSection;
}; // class CarbonCfgCadiCustomCode : public CarbonCfgCustomCode

class CarbonCfgRegCustomCode : public CarbonCfgCustomCode
{
  Q_OBJECT

  Q_PROPERTY(CcfgEnum::CarbonCfgRegCustomCodeSection Section READ GetSection WRITE SetSection);

public slots:
#ifndef SWIG
  CcfgEnum::CarbonCfgRegCustomCodeSection GetSection()
  {
    return CcfgEnum::CarbonCfgRegCustomCodeSection(this->getSection());
  }
  void SetSection(CcfgEnum::CarbonCfgRegCustomCodeSection section)
  {
    this->putSection(CarbonCfgRegCustomCodeSection(section));
  }
#endif

public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonCfgRegCustomCode();

  //! Destructor
  ~CarbonCfgRegCustomCode();

  //! Cast to a register custom code
  virtual CarbonCfgRegCustomCode* castReg() { return this; }

  //! Put the section
  void putSection(CarbonCfgRegCustomCodeSection section);

  //! Get the section as an enum
  CarbonCfgRegCustomCodeSection getSection(void) const;

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the custom code and replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  CarbonCfgRegCustomCode* clone(const CarbonCfgTemplate& templ) const;

private:
  //! Hide the copy constructor
  CarbonCfgRegCustomCode(const CarbonCfgRegCustomCode& src);

  //! Hide the assignment constructor
  /*! If you add more fields here, please update this function.
   */
  CarbonCfgRegCustomCode& operator=(const CarbonCfgRegCustomCode& src);

  //! The custom code section
  CarbonCfgRegCustomCodeSection mSection;
}; // class CarbonCfgRegCustomCode : public CarbonCfgCustomCode

struct CarbonCfgRegister : public CarbonCfgCustomCodeContainer
{
  Q_OBJECT
  
  Q_PROPERTY(QString Name READ GetName)
  Q_PROPERTY(quint32 Width READ GetWidth WRITE SetWidth)
  Q_PROPERTY(QString Comment READ GetComment WRITE SetComment)
  Q_PROPERTY(quint32 Offset READ GetOffset WRITE SetOffset)
  Q_PROPERTY(bool BigEndian READ GetBigEndian WRITE SetBigEndian)
  Q_PROPERTY(QString Port READ GetPort WRITE SetPort)
  Q_PROPERTY(quint32 NumFields READ NumFields)
  Q_PROPERTY(QString GroupName READ GetGroupName)
  Q_PROPERTY(CcfgEnum::CarbonCfgRadix Radix READ GetRadix WRITE SetRadix)
  Q_PROPERTY(quint32 NumCustomCodes READ NumCustomCodes)

  
public slots:
#ifndef SWIG
  CarbonCfgRegisterField* AddRegisterField(qint32 high, quint32 low, CcfgEnum::CarbonCfgRegAccessType accessType);

  void AddField(CarbonCfgRegisterField* field) { this->addField(field); }
  void RemoveField(CarbonCfgRegisterField* field) { this->removeField(field); }
  CcfgEnum::CarbonCfgRadix GetRadix() 
  {
    CarbonCfgRadixIO radix = this->getRadix();
    return static_cast<CcfgEnum::CarbonCfgRadix>((int)radix);
  }
  void SetRadix(CcfgEnum::CarbonCfgRadix newVal) { this->putRadix(static_cast<CarbonCfgRadix>(newVal)); }

  CarbonCfgRegisterField* GetField(quint32 index) { return this->getField(index); }

  quint32 NumFields() { return this->numFields(); }
  QString GetGroupName() { return this->getGroupName(); }

  QString GetPort() { return this->getPort(); }
  void SetPort(const QString& newVal)
  {
    UtString value;
    value << newVal;
    this->putPort(value.c_str()); 
  }

  bool GetBigEndian() { return this->getBigEndian(); }
  void SetBigEndian(bool nv) { this->putBigEndian(nv); }

  quint32 GetOffset() { return this->getOffset(); }
  void SetOffset(quint32 nv) { this->putOffset(nv); }

  QString GetComment() { return this->getComment(); }
  void SetComment(const QString& newVal)
  {
    UtString value;
    value << newVal;
    this->putComment(value.c_str()); 
  }

  quint32 GetWidth() { return this->getWidth(); }
  void SetWidth(quint32 nv) { this->putWidth(nv); }

  QString GetName() { return this->getName(); }

  quint32 NumCustomCodes() { return this->numCustomCodes(); }
  CarbonCfgRegCustomCode* AddCustomCode(CcfgEnum::CarbonCfgElementGenerationType genType = CcfgEnum::User)
  {
    CarbonCfgRegCustomCode* cc = this->addCustomCode();
    cc->putElementGenerationType(CarbonCfgElementGenerationType(genType));
    return cc;
  }
  CarbonCfgRegCustomCode* GetCustomCode(quint32 i) 
  {
    return this->getCustomCode(i)->castReg();
  }
  void RemoveCustomCode(CarbonCfgRegCustomCode* customCode)
  {
    this->removeCustomCode(customCode);
  }
#endif

public:
  CARBONMEM_OVERRIDES

  CarbonCfgRegister(const char* name, CarbonCfgGroup* group,
                    UInt32 width, bool big_endian,
                    CarbonCfgRadix radix, const char* comment, bool pc_reg);
  CarbonCfgRegister(UtIStream& f, UtString* groupName);
  ~CarbonCfgRegister();
  void write(CfgXmlWriter &xml);

  UInt32 getWidth() const {return mWidth;}
  const char* getName() const {return mName.c_str();}
  const char* getComment() const {return mComment.c_str();}
  unsigned int getOffset() const {return mOffset;}
  bool getBigEndian() const {return mBigEndian;}
  bool getPcReg() const {return mPcReg;}

#ifndef SWIG
  CarbonCfgRadixIO getRadix() const {return mRadix;}
#endif
  const char* getGroupName() const {return mGroup->getName();}
  const char* getPort() const {return mPort.c_str();}
  CarbonCfgGroup* getGroup() const {return mGroup;}
  CarbonUInt32 numFields() const {return mFields.size();}
  CarbonCfgRegisterField *getField(CarbonUInt32 i) const {
    INFO_ASSERT(i < mFields.size(), "FieldList out of range");
    return mFields[i];
  }
  void removeField(CarbonCfgRegisterField* f)
  {
    mFields.remove(f);
    delete f;
  }

  void putComment(const char* comment) {mComment = comment;}
  void putBigEndian(bool big_endian) {mBigEndian = big_endian;}
  void putPcReg(bool pc_reg) { mPcReg = pc_reg;}
  void putRadix(CarbonCfgRadix radix) {mRadix = radix;}
  void putGroup(CarbonCfgGroup* group) {mGroup = group;}
  void putWidth(UInt32 width) {mWidth = width;}
  void putOffset(UInt32 offset) {mOffset = offset;}
  void putPort(const char* port) {mPort = port;}
  void addField(CarbonCfgRegisterField *field) {mFields.push_back(field);}
  CarbonCfgRegCustomCode* addCustomCode();
  void addCustomCode(CarbonCfgRegCustomCode*);

  //! make sure all fields are within the bounds of the register and do not overlap
  bool checkFields();

  //! Loop over all RTL registers/memories used by this logical register
  class RTLLoop {
  public:
    RTLLoop(CarbonCfgRegister *reg);
    ~RTLLoop();

    void operator++();
    bool atEnd();
    CarbonCfgRegisterLocRTL *operator*();

  private:
    typedef UtArray<CarbonCfgRegisterLocRTL *> RTLList;
    RTLList mRTLList;
    RTLList::iterator mIter;
  };

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the register and its sub-parts. It replaces
   *  any template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  CarbonCfgRegister* clone(CarbonCfgGroup* group, const CarbonCfgTemplate& templ);

private:
  //! Hide the copy constructor, use clone since you need to pass a group in
  CarbonCfgRegister(const CarbonCfgRegister&);

  //! Hide the assign constructor, use clone since you need to pass a group in
  CarbonCfgRegister& operator=(const CarbonCfgRegister&);

  friend class CarbonCfg;
  friend class CarbonCfgRegister::RTLLoop;
  void putName(const char* name) {mName = name;}

  UtString mName;
  CarbonCfgGroup* mGroup;
  UInt32 mWidth;
  bool mBigEndian;
  bool mPcReg;
  CarbonCfgRadixIO mRadix;
  UtString mComment;
  UInt32 mOffset;
  UtString mPort;

  typedef UtArray<CarbonCfgRegisterField*> FieldList;
  FieldList mFields;
}; // struct CarbonCfgRegister

struct CarbonCfgMemoryLocAttr {
  CarbonCfgMemoryLocAttr():
    mStartWordOffset(0),
    mEndWordOffset(0),
    mMsb(0),
    mLsb(0)
  {}

  //! Clone so that we can use in a different component/sub-component.
  /*! This function helps clone functions from derived classes copy
   *  over the fields that are specific to this class. It replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  void cloneHelper(CarbonCfgMemoryLocAttr* clone) const;

  //! Compute the bit size.
  UInt32 getBitSize(void) const;

  UInt64 mStartWordOffset;
  UInt64 mEndWordOffset;
  UInt32 mMsb;
  UInt32 mLsb;
};

//! class used to manage memory errors.
class CarbonCfgMemoryMessages
{
public:
  CARBONMEM_OVERRIDES

  //! constructor
  CarbonCfgMemoryMessages() {}

  //! Clear the message state
  void clearMessages(void);
  
  //! report a warning
  void reportWarning(CarbonCfg* cfg, const UtString& msg);

  //! Check if there are any messages
  bool messageState(void) const { return !mMessages.empty(); }

  //! Return the set of messages
  const char* getMessage(void) const { return mMessages.c_str(); }

private:
  //! Place to store the messages
  UtString      mMessages;
}; // class CarbonCfgMemoryMessages

struct CarbonCfgMemoryLoc  : public QObject, public QScriptable {
  Q_OBJECT

  Q_PROPERTY(CcfgEnum::CarbonCfgMemLocType Type READ GetType);
  Q_PROPERTY(quint64 DisplayStartWordOffset READ GetDisplayStartWordOffset WRITE SetDisplayStartWordOffset);
  Q_PROPERTY(quint64 DisplayEndWordOffset READ GetDisplayEndWordOffset WRITE SetDisplayEndWordOffset);
  Q_PROPERTY(quint32 DisplayMSB READ GetDisplayMsb WRITE SetDisplayMsb);
  Q_PROPERTY(quint32 DisplayLSB READ GetDisplayLsb WRITE SetDisplayLsb);
  Q_PROPERTY(CarbonCfgMemoryLocRTL* CastRTL READ CastRTL);
  Q_PROPERTY(CarbonCfgMemoryLocPort* CastPort READ CastPort);
  Q_PROPERTY(CarbonCfgMemoryLocUser* CastUser READ CastUser);

public slots:
#ifndef SWIG
  CcfgEnum::CarbonCfgMemLocType GetType() { return CcfgEnum::CarbonCfgMemLocType(this->getType()); }

  quint64 GetDisplayStartWordOffset() { return this->getDisplayStartWordOffset(); }
  void SetDisplayStartWordOffset(quint64 newVal) { this->putDisplayStartWordOffset(newVal); }
 
  quint64 GetDisplayEndWordOffset() { return this->getDisplayEndWordOffset(); }
  void SetDisplayEndWordOffset(quint64 newVal) { this->putDisplayEndWordOffset(newVal); }

  quint32 GetDisplayMsb() { return this->getDisplayMsb(); }
  void SetDisplayMsb(quint32 newVal) { this->putDisplayMsb(newVal); }

  quint32 GetDisplayLsb() { return this->getDisplayLsb(); }
  void SetDisplayLsb(quint32 newVal) { this->putDisplayLsb(newVal); }

  CarbonCfgMemoryLocRTL* CastRTL() { return this->castRTL(); }
  CarbonCfgMemoryLocPort* CastPort() { return this->castPort(); }
  CarbonCfgMemoryLocUser* CastUser() { return this->castUser(); }
#endif

public:
  CARBONMEM_OVERRIDES
  
  virtual ~CarbonCfgMemoryLoc();

  virtual void write(CfgXmlWriter &xml) = 0;

  virtual CarbonCfgMemoryLocRTL*  castRTL();
  virtual CarbonCfgMemoryLocPort* castPort();
  virtual CarbonCfgMemoryLocUser* castUser();

  virtual CarbonCfgMemLocType     getType() const = 0;
  
  // The display attributes defines the locator's position
  // in the block of the memory display.
  // The offsets are specified as words and are relative to the block start
  // address.
  UInt64 getDisplayStartWordOffset() const        { return mDisplayAttr.mStartWordOffset; }
  void   putDisplayStartWordOffset(UInt64 offset) { mDisplayAttr.mStartWordOffset = offset; }
  UInt64 getDisplayEndWordOffset() const          { return mDisplayAttr.mEndWordOffset; }
  void   putDisplayEndWordOffset(UInt64 offset)   { mDisplayAttr.mEndWordOffset = offset; }
  UInt32 getDisplayMsb() const                    { return mDisplayAttr.mMsb; }
  void   putDisplayMsb(UInt32 msb)                { mDisplayAttr.mMsb = msb; }
  UInt32 getDisplayLsb() const                    { return mDisplayAttr.mLsb; }
  void   putDisplayLsb(UInt32 lsb)                { mDisplayAttr.mLsb = lsb; }

  //! get the bit size for this locator (depth * width)
  UInt32 getBitSize(void) const { return mDisplayAttr.getBitSize(); }

  const CarbonCfgMemoryLocAttr& getDisplayAttr() const { return mDisplayAttr; }
  void putDisplayAttr(const CarbonCfgMemoryLocAttr& attr) { mDisplayAttr = attr; }
  
  //! Clone this register locator replacing any variables with values
  /*! This function must be overloaded by the derived class so that
   *  memories can be cloned.
   */
  virtual CarbonCfgMemoryLoc* clone(const CarbonCfgTemplate& templ) const = 0;

  //! Clone so that we can use in a different component/sub-component.
  /*! This function helps the derived classes clone members that are
   *  specific to a memory location. It replaces template variables
   *  with the corresponding values from the CarbonCfgTemplate
   *  variable map.
   *
   *  If you add more fields here, please update this function.
   */
  void cloneHelper(CarbonCfgMemoryLoc* clone) const;

  //! Function to do an overlap test with another locator
  bool overlaps(const CarbonCfgMemoryLoc& other) const;

  //! Check this locator. Derivations should call this function for general checks
  /*! The cfg and memInfo parameters are used for warning messages. The db parameter
   *  is used to do RTL checks.
   */
  virtual void check(CarbonCfg* cfg, const char* memInfo, UInt32 bitWidth, CarbonDB* db = NULL) const;

  //! Returns true if this locator is in the message state
  bool messageState(void) const { return mMemoryMessages.messageState(); }

  //! Report a warning message
  void reportWarning(CarbonCfg* cfg, const char* memInfo, const UtString& msg) const
  {
    UtString fullMsg;
    fullMsg << memInfo << ": " << msg;
    mMemoryMessages.reportWarning(cfg, fullMsg);
  }

  //! Returns a string for all the messages corresponding to this memory locator
  const char* getMessage(void) const { return mMemoryMessages.getMessage(); }

  //! Turn the display range into a string (use string passed in as storage)
  const char* rangeString(UtString* str);

protected:
  CarbonCfgMemoryLoc();
  void writeAttr(const char* tag, const CarbonCfgMemoryLocAttr& attr, CfgXmlWriter &xml);
  
  CarbonCfgMemoryLocAttr mDisplayAttr;

  //! Message container
  mutable CarbonCfgMemoryMessages mMemoryMessages;

private:
  //! Hide the copy constructor
  CarbonCfgMemoryLoc(const CarbonCfgMemoryLoc&);

  //! Hide the assign constructor
  CarbonCfgMemoryLoc& operator=(const CarbonCfgMemoryLoc&);
};

struct CarbonCfgMemoryLocRTL : public CarbonCfgMemoryLoc {
  Q_OBJECT

  Q_PROPERTY(QString Path READ GetPath WRITE SetPath);
  Q_PROPERTY(quint32 StartWordOffset READ GetStartWordOffset WRITE SetStartWordOffset);
  Q_PROPERTY(quint32 EndWordOffset READ GetEndWordOffset WRITE SetEndWordOffset);
  Q_PROPERTY(quint32 MSB READ GetMsb WRITE SetMsb);
  Q_PROPERTY(quint32 LSB READ GetLsb WRITE SetLsb);
  Q_PROPERTY(quint32 BitWidth READ GetBitWidth);
  Q_PROPERTY(quint64 DisplayStartWordOffset READ GetDisplayStartWordOffset WRITE SetDisplayStartWordOffset);
  Q_PROPERTY(quint64 DisplayEndWordOffset READ GetDisplayEndWordOffset WRITE SetDisplayEndWordOffset);
  Q_PROPERTY(quint32 DisplayMSB READ GetDisplayMsb WRITE SetDisplayMsb);
  Q_PROPERTY(quint32 DisplayLSB READ GetDisplayLsb WRITE SetDisplayLsb);

public slots:
#ifndef SWIG
  quint64 GetDisplayStartWordOffset() { return this->getDisplayStartWordOffset(); }
  void SetDisplayStartWordOffset(quint64 newVal) { this->putDisplayStartWordOffset(newVal); }
 
  quint64 GetDisplayEndWordOffset() { return this->getDisplayEndWordOffset(); }
  void SetDisplayEndWordOffset(quint64 newVal) { this->putDisplayEndWordOffset(newVal); }

  quint32 GetDisplayMsb() { return this->getDisplayMsb(); }
  void SetDisplayMsb(quint32 newVal) { this->putDisplayMsb(newVal); }

  quint32 GetDisplayLsb() { return this->getDisplayLsb(); }
  void SetDisplayLsb(quint32 newVal) { this->putDisplayLsb(newVal); }

  QString GetPath() { return this->getPath(); }
  void SetPath(const QString& newVal) { UtString nv; nv << newVal; this->putPath(nv.c_str()); }

  quint64 GetStartWordOffset() { return this->getRTLStartWordOffset(); }
  void SetStartWordOffset(quint64 newVal) { this->putRTLStartWordOffset(newVal); }
 
  quint64 GetEndWordOffset() { return this->getRTLEndWordOffset(); }
  void SetEndWordOffset(quint64 newVal) { this->putRTLEndWordOffset(newVal); }

  quint32 GetMsb() { return this->getRTLMsb(); }
  void SetMsb(quint32 newVal) { this->putRTLMsb(newVal); }

  quint32 GetLsb() { return this->getRTLLsb(); }
  void SetLsb(quint32 newVal) { this->putRTLLsb(newVal); }

  quint32 GetBitWidth() { return this->getBitWidth(); }
#endif

public:
  CARBONMEM_OVERRIDES

  CarbonCfgMemoryLocRTL(const char *rtl_path, UInt32 bitWidth, UInt64 endWord);

  ~CarbonCfgMemoryLocRTL();

  void write(CfgXmlWriter &);

  const char* getPath() const {return mPath.c_str();}
  void        putPath(const char *val) {mPath = val;}
  
  // The RTL attributes defines the part of the RTL memory 
  // to be used within the memory display.
  // The start and end values are specified as memory words in the RTL memory.
  // MSB and LSB values are start and end bits of each word in the RTL memory
  UInt64 getRTLStartWordOffset() const        { return mRTLAttr.mStartWordOffset; }
  void   putRTLStartWordOffset(UInt64 offset) { mRTLAttr.mStartWordOffset = offset; }
  UInt64 getRTLEndWordOffset() const          { return mRTLAttr.mEndWordOffset; }
  void   putRTLEndWordOffset(UInt64 offset)   { mRTLAttr.mEndWordOffset = offset; }
  UInt32 getRTLMsb() const                    { return mRTLAttr.mMsb; }
  void   putRTLMsb(UInt32 msb)                { mRTLAttr.mMsb = msb; }
  UInt32 getRTLLsb() const                    { return mRTLAttr.mLsb; }
  void   putRTLLsb(UInt32 lsb)                { mRTLAttr.mLsb = lsb; }

  // here is a get/put for the full RTLAttr (instead of individual fields that are updated above)
  const CarbonCfgMemoryLocAttr& getRTLAttr() const { return mRTLAttr; }
  void putRTLAttr(const CarbonCfgMemoryLocAttr& attr) { mRTLAttr = attr; }


  UInt32 getBitWidth() const
  {
    if (mRTLAttr.mMsb > mRTLAttr.mLsb) {
      return mRTLAttr.mMsb-mRTLAttr.mLsb+1;
    } else {
      return mRTLAttr.mLsb-mRTLAttr.mMsb+1;
    }
  }

  UInt32 getDepth() const // this probably should return 64 bits, but memories that large are not currently supported so the upper bits would be unused
  {
    if (mRTLAttr.mStartWordOffset > mRTLAttr.mEndWordOffset) {
      return mRTLAttr.mStartWordOffset-mRTLAttr.mEndWordOffset+1;
    } else {
      return mRTLAttr.mEndWordOffset-mRTLAttr.mStartWordOffset+1;
    }
  }

  CarbonCfgMemoryLocRTL *castRTL();
  CarbonCfgMemLocType    getType() const {return eCfgMemLocRTL;}

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the RTL memory locator and replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  virtual CarbonCfgMemoryLoc* clone(const CarbonCfgTemplate& templ) const;

  //! Function to check this locator. Any derivations should call this function as well
  virtual void check(CarbonCfg* cfg, const char* memInfo, UInt32 bitWidth, CarbonDB* db = NULL) const;

private:
  //! Hide the copy constructor
  CarbonCfgMemoryLocRTL(const CarbonCfgMemoryLocRTL&);

  //! Hide the assign constructor
  CarbonCfgMemoryLocRTL& operator=(const CarbonCfgMemoryLocRTL&);

  UtString mPath;
  CarbonCfgMemoryLocAttr mRTLAttr;
};

struct CarbonCfgMemoryLocPort : public CarbonCfgMemoryLoc {
  Q_OBJECT

  Q_PROPERTY(QString Name READ GetPortName WRITE SetPortName);
  Q_PROPERTY(bool FixedAddress READ GetFixedAddress WRITE SetFixedAddress);
  Q_PROPERTY(quint64 DisplayStartWordOffset READ GetDisplayStartWordOffset WRITE SetDisplayStartWordOffset);
  Q_PROPERTY(quint64 DisplayEndWordOffset READ GetDisplayEndWordOffset WRITE SetDisplayEndWordOffset);
  Q_PROPERTY(quint32 DisplayMSB READ GetDisplayMsb WRITE SetDisplayMsb);
  Q_PROPERTY(quint32 DisplayLSB READ GetDisplayLsb WRITE SetDisplayLsb);

public slots:
#ifndef SWIG
  QString GetPortName() { return this->getPortName(); }
  void SetPortName(const QString& newVal) { UtString nv; nv << newVal; this->putPortName(nv.c_str()); }

  bool GetFixedAddress() { return this->hasFixedAddress(); }
  void SetFixedAddress(quint32 newVal) { this->putFixedAddress(newVal); }

  quint64 GetDisplayStartWordOffset() { return this->getDisplayStartWordOffset(); }
  void SetDisplayStartWordOffset(quint64 newVal) { this->putDisplayStartWordOffset(newVal); }
 
  quint64 GetDisplayEndWordOffset() { return this->getDisplayEndWordOffset(); }
  void SetDisplayEndWordOffset(quint64 newVal) { this->putDisplayEndWordOffset(newVal); }

  quint32 GetDisplayMsb() { return this->getDisplayMsb(); }
  void SetDisplayMsb(quint32 newVal) { this->putDisplayMsb(newVal); }

  quint32 GetDisplayLsb() { return this->getDisplayLsb(); }
  void SetDisplayLsb(quint32 newVal) { this->putDisplayLsb(newVal); }
#endif

public:
  CARBONMEM_OVERRIDES

  CarbonCfgMemoryLocPort(const char *port_name);

  ~CarbonCfgMemoryLocPort();

  void write(CfgXmlWriter &);

  const char* getPortName() const {return mPortName.c_str();}
  void        putPortName(const char *val) {mPortName = val;}

  bool        hasFixedAddress() const {return mFixedAddress;}
  void        putFixedAddress(bool on) {mFixedAddress = on;}

  CarbonCfgMemoryLocPort *castPort();
  CarbonCfgMemLocType    getType() const {return eCfgMemLocPort;}

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the port memory locator and replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  virtual CarbonCfgMemoryLoc* clone(const CarbonCfgTemplate& templ) const;

private:
  //! Hide the copy constructor
  CarbonCfgMemoryLocPort(const CarbonCfgMemoryLocPort&);

  //! Hide the assign constructor
  CarbonCfgMemoryLocPort& operator=(const CarbonCfgMemoryLocPort&);

  //! Instance name of the Master Port to access
  UtString mPortName;
  //! When true, use the base address and size from the memory block. When false use the address
  //! regions defined my the memory map of the system.
  bool     mFixedAddress;
};

struct CarbonCfgMemoryLocUser : public CarbonCfgMemoryLoc {
  Q_OBJECT

  Q_PROPERTY(QString Name READ GetName WRITE SetName);

public slots:
#ifndef SWIG
  QString GetName() const { return this->getName(); }
  void SetName(const QString& _name)
  {
    UtString name;
    name << _name;
    this->putName(name.c_str());
  }
#endif

public:
  CARBONMEM_OVERRIDES

  CarbonCfgMemoryLocUser(const char* name): mName(name) {}
  void write(CfgXmlWriter &);
  CarbonCfgMemoryLocUser *castUser() { return this; }
  CarbonCfgMemLocType    getType() const {return eCfgMemLocUser;}

  const char* getName() const { return mName.c_str();}
  void putName(const char* name) { mName = name; }

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the port memory locator and replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  virtual CarbonCfgMemoryLoc* clone(const CarbonCfgTemplate& templ) const;

private:
  UtString mName;
};

struct CarbonCfgMemoryBlock : public CarbonCfgCustomCodeContainer {
  Q_OBJECT
 
  Q_PROPERTY(QString Name READ GetName WRITE SetName);
  Q_PROPERTY(quint64 Base READ GetBase WRITE SetBase);
  Q_PROPERTY(quint64 Size READ GetSize WRITE SetSize);
  Q_PROPERTY(quint32 Width READ GetWidth);
  Q_PROPERTY(quint32 NumLocs READ NumLocs);
  Q_PROPERTY(quint32 NumCustomCodes READ NumCustomCodes)

 public slots:
#ifndef SWIG
  QString GetName() const { return this->getName(); }
  void SetName(const QString& _name)
  {
    UtString name;
    name << _name;
    this->putName(name.c_str());
  }
 
  quint64 GetBase() const { return this->getBase(); }
  void SetBase(quint64 newVal) { this->putBase(newVal); }

  quint64 GetSize() const { return this->getSize(); }
  void SetSize(quint64 newVal) { this->putSize(newVal); }

  quint32 GetWidth() const { return this->getWidth(); }

  CarbonCfgMemoryLocRTL* AddLocRTL(const QString path = QString(), quint32 width = 0)
  {
    UtString p;
    p << path;
    return this->addLocRTL(p.c_str(), width);
  }
  CarbonCfgMemoryLocPort* AddLocPort(const QString& port_name)
  {
    UtString pname;
    pname << port_name;
    return this->addLocPort(pname.c_str());
  }
  CarbonCfgMemoryLocUser* AddLocUser(const QString& _name)
  {
    UtString name;
    name << _name;
    return this->addLocUser(name.c_str());
  }

  void RemoveLoc(QVariant loc)
  {
    CarbonCfgMemoryLoc* ml = qvariant_cast<CarbonCfgMemoryLoc*>(loc);
    if (ml)
      RemoveLocMemoryLoc(ml);

    CarbonCfgMemoryLocPort* mlp = qvariant_cast<CarbonCfgMemoryLocPort*>(loc);
    if (mlp)
      RemoveLocPort(mlp);

    CarbonCfgMemoryLocRTL* mlr = qvariant_cast<CarbonCfgMemoryLocRTL*>(loc);
    if (mlr)
      RemoveLocRTL(mlr);

    CarbonCfgMemoryLocUser* mlu = qvariant_cast<CarbonCfgMemoryLocUser*>(loc);
    if (mlu)
      RemoveLocUser(mlu);
  }

  void RemoveLocMemoryLoc(CarbonCfgMemoryLoc* loc) { this->removeLoc(loc); }
  void RemoveLocPort(CarbonCfgMemoryLocPort* loc) { this->removeLoc(loc); }
  void RemoveLocRTL(CarbonCfgMemoryLocRTL* loc) { this->removeLoc(loc); }
  void RemoveLocUser(CarbonCfgMemoryLocUser* loc) { this->removeLoc(loc); }
 
  quint32 NumLocs() const { return this->numLocs(); }
  
  CarbonCfgMemoryLoc* GetLoc(quint32 index) { return this->getLoc(index); }

  quint32 NumCustomCodes() const { return this->numCustomCodes(); }
  CarbonCfgMemoryBlockCustomCode* AddCustomCode(CcfgEnum::CarbonCfgElementGenerationType genType = CcfgEnum::User)
  {
    CarbonCfgMemoryBlockCustomCode* cc = this->addCustomCode();
    cc->putElementGenerationType(CarbonCfgElementGenerationType(genType));
    return static_cast<CarbonCfgMemoryBlockCustomCode*>(cc);
  }
  CarbonCfgMemoryBlockCustomCode* GetCustomCode(quint32 i)
  {
    return static_cast<CarbonCfgMemoryBlockCustomCode*>(this->getCustomCode(i));
  }
  void RemoveCustomCode(CarbonCfgMemoryBlockCustomCode* customCode)
  {
    this->removeCustomCode(customCode);
  }
#endif

public:
  CARBONMEM_OVERRIDES
  
  //! Constructor. Width is inherited from the memory space, so should be known at construction time
  CarbonCfgMemoryBlock(UInt32 bitWidth) :
    CarbonCfgCustomCodeContainer(CarbonCfgMemoryBlockCustomCodeSectionIO().getStrings()),
    mBitWidth(bitWidth),
    mBaseAddr(0),
    mSize(0)
  { }
  
  //! Destructor
  ~CarbonCfgMemoryBlock();

  //! Write class to XML file
  void write(CfgXmlWriter &xml);

  //! Set Name of memory block
  void putName(const char* name)   {mName     = name;}

  //! Set Base Address of memory block, (Byte address)
  void putBase(UInt64 addr)        {mBaseAddr = addr;}

  //! Set Size of memory block in bytes
  void putSize(UInt64 size)        {mSize     = size;}

  //! Get Name of memory block
  const char* getName() const {return mName.c_str();}

  //! Get Base Address of memory block, (Byte address)
  UInt64 getBase() const   {return mBaseAddr;}

  //! Get Size of memory block in bytes
  UInt64 getSize() const  {return mSize;}

  //! Get Width. (Note that width can't be set since it's always the same size as the memory space.)
  UInt32 getWidth() const {return mBitWidth;}

  //! Add an RTL Locator object to the block. If width is set to 0, it will default to the width of the block
  CarbonCfgMemoryLocRTL* addLocRTL(const char* path="", UInt32 width = 0);
  CarbonCfgMemoryLocPort* addLocPort(const char* port_name);
  CarbonCfgMemoryLocUser* addLocUser(const char* name);

  //! Remove a locator from the list
  void removeLoc(CarbonCfgMemoryLoc* loc)
  {
    delete loc;
    mLocators.remove(loc);
  }

  int removeLocUndo(CarbonCfgMemoryLoc* loc)
  {
    for(UInt32 i=0; i<mLocators.size(); i++)
    {
      CarbonCfgMemoryLoc* l = mLocators[i];
      if (l == loc)
      {
        mLocators.remove(l);       // order N
        return i;
      }
    }
    return -1;
  }

  void insertLocUndo(int pos, CarbonCfgMemoryLoc* loc)
  {
    LocList temp;
    temp.push_back(loc);
    mLocators.insert(pos, temp.begin(), temp.end());
  }


  //! Number of locators
  UInt32 numLocs() const { return mLocators.size(); }

  //! Get a locator
  CarbonCfgMemoryLoc* getLoc(UInt32 index) const;

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the memory block and its sub-parts. It
   *  replaces any template variables with the corresponding values
   *  from the CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  CarbonCfgMemoryBlock* clone(const CarbonCfgTemplate& templ) const;

  CarbonCfgMemoryBlockCustomCode* addCustomCode()
  {
    CarbonCfgMemoryBlockCustomCode* customCode = new CarbonCfgMemoryBlockCustomCode();
    CarbonCfgCustomCodeContainer::addCustomCode(customCode);
    return customCode;
  }

  //! Abstraction for a set of locators
  typedef UtArray<CarbonCfgMemoryLoc*> LocList;

  //! Returns true if this locator is in the message state
  bool messageState(void) const { return mMemoryMessages.messageState(); }

  //! Returns a string for all the messages corresponding to this memory locator
  const char* getMessage(void) const { return mMemoryMessages.getMessage(); }

  //! Checks this block for problems. It clears any messages first
  void check(CarbonCfg* cfg, const char* memInfo, UInt64 maxAddr, CarbonDB* db = NULL) const;

  //! Report a warning message
  void reportWarning(CarbonCfg* cfg, const char* memInfo, const UtString& msg) const
  {
    UtString fullMsg;
    fullMsg << memInfo << ": " << msg;
    mMemoryMessages.reportWarning(cfg, fullMsg);
  }

private:
  //! Owner CarbonCfgMemory class can change the bit width; Need this for GUI updates
  friend class CarbonCfgMemory;

  //! Private function to override the bit width
  void putWidth(UInt32 width) { mBitWidth = width; }

  //! Hide the copy constructor
  CarbonCfgMemoryBlock(const CarbonCfgMemoryBlock&);

  //! Hide the assign constructor
  CarbonCfgMemoryBlock& operator=(const CarbonCfgMemoryBlock&);

  //! Message container
  mutable CarbonCfgMemoryMessages mMemoryMessages;

  //! Width of memory in bits, exists here only for convenience. Must correspond to width of memory
  UInt32   mBitWidth;

  //! Name of memory block
  UtString mName;
  //! Base Address of memory block, (Byte address)
  UInt64   mBaseAddr;
  //! Size of memory block (in bytes)
  UInt64   mSize;

  //! List of Memory Locators
  LocList mLocators;
};

// CarbonCfgMemoryEditFlags


class CarbonCfgMemory : public CarbonCfgCustomCodeContainer {

  Q_OBJECT

  Q_PROPERTY(quint32 NumMemoryBlocks READ NumMemoryBlocks);
  Q_PROPERTY(QString Name READ GetName);
  Q_PROPERTY(QString InitFile READ GetInitFile WRITE SetInitFile);
  Q_PROPERTY(QString Comment READ GetComment WRITE SetComment);
  Q_PROPERTY(quint32 Width READ GetWidth);
  Q_PROPERTY(CcfgEnum::CarbonCfgReadmemType ReadmemType READ GetReadmemType WRITE SetReadmemType);
  Q_PROPERTY(CcfgEnum::CarbonCfgMemInitType InitType READ GetInitType WRITE SetInitType);
  Q_PROPERTY(QString SystemAddressESLPortName READ GetSystemAddressESLPortName WRITE SetSystemAddressESLPortName);
  Q_PROPERTY(bool DisplayAtZero READ GetDisplayAtZero WRITE SetDisplayAtZero);
  Q_PROPERTY(quint32 MAU READ GetMAU WRITE SetMAU);
  Q_PROPERTY(bool BigEndian READ GetBigEndian WRITE SetBigEndian);
  Q_PROPERTY(bool ProgramMemory READ GetProgramMemory WRITE SetProgramMemory);
  Q_PROPERTY(quint32 NumCustomCodes READ NumCustomCodes)
  Q_PROPERTY(CcfgEnum::CarbonCfgElementGenerationType ElementGenerationType READ GetElementGenerationType WRITE SetElementGenerationType);
  Q_PROPERTY(CcfgFlags::CarbonCfgMemoryEditFlags EditFlags READ GetEditFlags WRITE SetEditFlags);
  Q_PROPERTY(quint32 NumSystemAddressESLPorts READ NumSystemAddressESLPorts);

public slots:
#ifndef SWIG
  CcfgFlags::CarbonCfgMemoryEditFlags GetEditFlags() const { return mEditFlags; }
  void SetEditFlags(CcfgFlags::CarbonCfgMemoryEditFlags newValue) { mEditFlags = newValue; }

  QString GetSystemAddressESLPortName(quint32 index)
  {
    QString value = getSystemAddressESLPortName(index);
    return value;
  }

  qint64 GetSystemAddressESLPortBaseAddress(quint32 index)
  {
    return getSystemAddressESLPortBaseAddress(index);
  }

  void SetSystemAddressESLPortName(quint32 index, const QString& eslPortName)
  {
    UtString port; port << eslPortName;
    putSystemAddressESLPortName(index, port.c_str());
  }
  void AddSystemAddressESLPort(const QString& eslPortName, qint64 baseAddress)
  {
    UtString port; port << eslPortName;
    addSystemAddressESLPort(port.c_str(), baseAddress);
  }
  void RemoveAllSystemAddressESLPorts()
  {
    removeAllSystemAddressESLPorts();
  }
  void RemoveSystemAddressESLPort(quint32 index)
  {
    removeSystemAddressESLPort(index);
  }
  void SetSystemAddressESLPortBaseAddress(quint32 index, qint64 baseAddress)
  {
    putSystemAddressESLPortBaseAddress(index, baseAddress);
  }

  quint32 NumMemoryBlocks() { return this->numMemoryBlocks(); }
  quint32 NumSystemAddressESLPorts() { return numSystemAddressESLPorts(); }

  CcfgEnum::CarbonCfgElementGenerationType GetElementGenerationType() const { return CcfgEnum::CarbonCfgElementGenerationType(this->getElementGenerationType()); }
  void SetElementGenerationType(CcfgEnum::CarbonCfgElementGenerationType wt) { this->putElementGenerationType(CarbonCfgElementGenerationType(wt)); }

  QString GetName() { return this->getName(); }
  
  QString GetInitFile()  { return this->getInitFile(); }
  void SetInitFile(const QString& newVal) { UtString nv; nv << newVal; this->putInitFile(nv.c_str()); }

  QString GetComment() const { return this->getComment(); }
  void SetComment(const QString& newVal) { UtString nv; nv << newVal; this->putComment(nv.c_str()); }

  quint32 GetWidth() const { return this->getWidth(); }

  CcfgEnum::CarbonCfgReadmemType GetReadmemType() { return CcfgEnum::CarbonCfgReadmemType(this->getReadmemType()); }
  void SetReadmemType(CcfgEnum::CarbonCfgReadmemType newVal) { this->putReadmemType(CarbonCfgReadmemType(newVal)); }

  CcfgEnum::CarbonCfgMemInitType GetInitType() { return CcfgEnum::CarbonCfgMemInitType(this->getMemInitType()); }
  void SetInitType(CcfgEnum::CarbonCfgMemInitType newVal) { this->putMemInitType(CarbonCfgMemInitType(newVal)); }

  QString GetSystemAddressESLPortName() { return this->getSystemAddressESLPortName(); }
  void SetSystemAddressESLPortName(const QString& newVal) { UtString nv; nv << newVal; this->putSystemAddressESLPortName(nv.c_str()); }

  QString GetDisassemblyName() const { return this->getMemoryDisassemblyName(); }
  void SetDisassemblyName(const QString& newVal) { UtString nv; nv << newVal; this->putMemoryDisassemblyName(nv.c_str()); }

  bool GetDisplayAtZero() { return this->getDisplayAtZero(); }
  void SetDisplayAtZero(bool newVal) { this->putDisplayAtZero(newVal); }

  quint32 GetMAU() { return this->getMAU(); }
  void SetMAU(quint32 newVal) { this->putMAU(newVal); }

  bool GetBigEndian() { return this->getBigEndian(); }
  void SetBigEndian(bool newVal) { this->putBigEndian(newVal); }

  bool GetProgramMemory() { return this->getProgramMemory(); }
  void SetProgramMemory(bool newVal) { this->putProgramMemory(newVal); }

  CarbonCfgMemoryBlock* AddMemoryBlock() { return this->addMemoryBlock(); }
  void RemoveMemoryBlock(CarbonCfgMemoryBlock* block) { this->removeMemoryBlock(block); }
  CarbonCfgMemoryBlock* GetMemoryBlock(quint32 index) { return this->getMemoryBlock(index); }

  quint32 NumCustomCodes() { return this->numCustomCodes(); }
  CarbonCfgMemoryCustomCode* AddCustomCode(CcfgEnum::CarbonCfgElementGenerationType genType = CcfgEnum::User)
  {
    CarbonCfgMemoryCustomCode* cc = this->addCustomCode();
    cc->putElementGenerationType(CarbonCfgElementGenerationType(genType));
    return static_cast<CarbonCfgMemoryCustomCode*>(cc);
  }
  CarbonCfgMemoryCustomCode* GetCustomCode(quint32 i)
  {
    return static_cast<CarbonCfgMemoryCustomCode*>(this->getCustomCode(i));
  }
  void RemoveCustomCode(CarbonCfgMemoryCustomCode* customCode)
  {
    this->removeCustomCode(customCode);
  }
#endif

public:
  CARBONMEM_OVERRIDES

  // ctor for legacy memory representation, legacyMem=1
  CarbonCfgMemory(CarbonCfg* parent, const char* signal, const char* name, const char* initFile,
                  UInt64 maxAddrs, UInt32 width, CarbonCfgReadmemType readmemType,
                  const char* comment, const char* disassemblyName = "", 
		  const char* disassemblerName = "");

  // ctor for non-legacy memory representation, legacyMem=0
  CarbonCfgMemory(CarbonCfg* parent, const char* name, UInt32 width, UInt64 maxAddr);

  ~CarbonCfgMemory();

  // ctor that reads memory definition from before the time when this was stored in xml.
  // note that the existence of xml representation does not identify it as legacyMem=1.
  // The xml representation can be used for legacyMem=1 and is always used for legacyMem=0
  CarbonCfgMemory(UtIStream& f, CarbonCfg* cfg);

  void write(CfgXmlWriter &xml);
  
  CarbonCfg*  getParent() const {return mParent; }
  const char* getName() const {return mName.c_str();}
  const char* getInitFile() const {return mInitFile.c_str();}
  const char* getComment() const {return mComment.c_str();}

  UInt32 getWidth() const {return mWidth;}
  CarbonCfgReadmemType getReadmemType() const {return mReadmemType;}
  CarbonCfgMemInitType getMemInitType() const {return mMemInitType;}
  const char* getReadmemTypeStr() const {return mReadmemType.getString();}
  const char* getMemInitTypeStr() const {return mMemInitType.getString();}
  const char* getSystemAddressESLPortName() const;
  const char* getSystemAddressESLPortName(quint32 index) const;
  qint64 getSystemAddressESLPortBaseAddress(quint32 index) const;

  const char* getMemoryDisassemblyName() const { return mDisassemblyName.c_str(); }
  const char* getMemoryDisassemblerName() const { return mDisassemblerName.c_str(); }
  bool        getDisplayAtZero() const { return mDisplayAtZero; }
  UInt32      getMAU() const {return mMAU; }
  bool        getBigEndian() const { return mBigEndian; }
  bool        getProgramMemory() const { return mProgramMemory; }

  void putElementGenerationType(CarbonCfgElementGenerationType t) {mElementGenerationType = t;}
  CarbonCfgElementGenerationType getElementGenerationType() const { return mElementGenerationType; }

  //! Function checks if the memory is correctly configured.
  /*! This function does the following checks:
   *    - Validates that locators do not overlap
   *    - Calls check on each location for additional checks
   *
   *  Note the DB is passed in if it exists, otherwise it is NULL and no DB checks are done
   */
  void check(CarbonCfg* cfg, CarbonDB* db = NULL) const;

  void putParent(CarbonCfg* parent)
  {
    INFO_ASSERT(parent, "Tried to put NULL parent in carbonCfgMemory object.");
    mParent = parent;
  }
  void putComment(const char* comment) {mComment = comment;}
  void putInitFile(const char* ifile) {mInitFile = ifile;}
  void putReadmemType(CarbonCfgReadmemType rt) {mReadmemType = rt;}
  void putMemInitType(CarbonCfgMemInitType mit) { mMemInitType = mit; }
  // deprecated
  void putSystemAddressESLPortName(const char* eslPortName);
  // use this one instead
  void putSystemAddressESLPortName(quint32 index, const char* eslPortName);
  void addSystemAddressESLPort(const char* eslPortName, qint64 baseAddress);
  void removeAllSystemAddressESLPorts();
  void removeSystemAddressESLPort(quint32 index);
  void putSystemAddressESLPortBaseAddress(quint32 index, qint64 baseAddress);

  void putMemoryDisassemblyName(const char* disassemblyName);
  void putMemoryDisassemblerName(const char* disassemblerName);
  void putDisplayAtZero(bool val) { mDisplayAtZero = val; }
  void putMAU(UInt32 val) { mMAU = val; }
  void putBigEndian(bool val) { mBigEndian = val; }
  void putProgramMemory(bool val) { mProgramMemory = val; }
  void putWidth(UInt32 val);
  //! Add a memory block
  CarbonCfgMemoryBlock* addMemoryBlock();

  //! Remove given memory block
  void removeMemoryBlock(CarbonCfgMemoryBlock* block);
  int removeMemoryBlockUndo(CarbonCfgMemoryBlock* block);
  void insertMemoryBlockUndo(int pos, CarbonCfgMemoryBlock* block);

  //! Number of memory blocks
  UInt32 numMemoryBlocks() const { return mBlocks.size(); }

  //! Number of System Address ESL Ports
  UInt32 numSystemAddressESLPorts() const { return mSystemAddressESLPortNames.size(); }
  
  //! Get a memory block
  CarbonCfgMemoryBlock* getMemoryBlock(UInt32 index) const;
  
  // These methods are legacy methods to keep the interface for backward compatilibilty
  // They are used when reading old versions of the ccfg file. The new way is to get these from the memory blocks.
  const char* getPath() const;
  UInt64 getMaxAddrs() const;
  UInt64 getBaseAddr() const;
  UInt32 getBaseAddrLo() const;
  UInt32 getBaseAddrHi() const;

  void putMaxAddrs(UInt64 maxAddrs);
  void putBaseAddr(UInt64 value);

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the memory space and its sub-parts. It
   *  replaces any template variables with the corresponding values
   *  from the CarbonCfgTemplate variable map.
   *
   *  If you add more fields here, please update this function.
   */
  CarbonCfgMemory* clone(CarbonCfg* parent, const CarbonCfgTemplate& templ) const;

  CarbonCfgMemoryCustomCode* addCustomCode()
  {
    CarbonCfgMemoryCustomCode* customCode = new CarbonCfgMemoryCustomCode();
    CarbonCfgCustomCodeContainer::addCustomCode(customCode);
    return customCode;
  }

  //! Returns true if this locator is in the message state
  bool messageState(void) const { return mMemoryMessages.messageState(); }

  //! Returns a string for all the messages corresponding to this memory locator
  const char* getMessage(void) const { return mMemoryMessages.getMessage(); }

  //! Report a warning message
  void reportWarning(CarbonCfg* cfg, const UtString& msg) const
  {
    UtString fullMsg;
    fullMsg << getName() << ": " << msg;
    mMemoryMessages.reportWarning(cfg, fullMsg);
  }

private:
  //! Hide the copy constructor
  CarbonCfgMemory(const CarbonCfgMemory&);

  //! Hide the assign constructor
  CarbonCfgMemory& operator=(const CarbonCfgMemory&);

  void restoreMemoryJournal(CarbonCfg* comp, const QDomElement& parent);

  //! only the cfg can change the name, because it must maintain its maps
  friend struct CarbonCfg;
  void putName(const char* newName) {mName = newName;}

  CarbonCfg* mParent;
  UtString mName;
  UtString mInitFile;
  UInt64 mMaxAddrs; // the maximum address in units of mWidth. If mMaxAddrs is 16, and mWidth is 32, then there are 16*(32/8) bytes in this memory. The memory is zero based so minimum address is 0.
  UInt32 mWidth;    // number of bits in a word of this memory
  UtString mComment;
  
  CarbonCfgReadmemTypeIO mReadmemType;
  CarbonCfgMemInitTypeIO mMemInitType;
  UtString mDisassemblyName;
  UtString mDisassemblerName;
  bool     mDisplayAtZero; // Set to true to match the legacy implementation, where all memory appears to start at location 0
  UInt32   mMAU;                // unused?
  bool     mBigEndian;
  bool     mProgramMemory;
  CarbonCfgElementGenerationType mElementGenerationType;
  CcfgFlags::CarbonCfgMemoryEditFlags mEditFlags;

  UtArray<CarbonCfgMemoryBlock*> mBlocks;
  mutable CarbonCfgMemoryMessages mMemoryMessages;

  // The next two arrays are managed in parallel(equal sizes, equal indexes)
  QList<UtString*> mSystemAddressESLPortNames;
  QList<qint64> mSystemAddressESLPortBaseAddresses;
}; // struct Memory

class CarbonCfgELFLoader : public QObject, public QScriptable  {
  Q_OBJECT
  
  Q_PROPERTY(quint32 NumSections READ NumSections);

public:
  CARBONMEM_OVERRIDES

  void addSection(const char* name, unsigned int space, const char* access);
  unsigned int numSections() const { return mSection.size();}
  const char* getSectionName(unsigned int index) const { return mSection[index]->name.c_str(); }
  unsigned int getSectionSpace(unsigned int index) const { return mSection[index]->space; }
  const char* getSectionAccess(unsigned int index) const { return mSection[index]->access.c_str(); }
  void write(CfgXmlWriter &xml);

  void removeSections(void);

public slots:
#ifndef SWIG
  void AddSection(const QString& name, quint32 space, const QString& access)
  {
    UtString uName;
    uName << name;
    UtString uAccess;
    uAccess << access;
    this->addSection(uName.c_str(), space, uAccess.c_str());
  }
  quint32 NumSections() { return this->numSections(); }
  QString GetSectionName(quint32 index) { return this->getSectionName(index); }
  quint32 GetSectionSpace(quint32 index) { return this->getSectionSpace(index); }
  QString GetSectionAccess(quint32 index) { return this->getSectionAccess(index); }
  void RemoveSections(void) { this->removeSections(); }
#endif

private:
  struct Section {
    CARBONMEM_OVERRIDES

    UtString     name;
    unsigned int space;
    UtString     access;
  };

  UtArray<Section*> mSection;
};  

class CarbonCfgProcInfo   : public QObject, public QScriptable {
  Q_OBJECT

  Q_PROPERTY(bool Processor READ GetIsProcessor WRITE SetIsProcessor);
  Q_PROPERTY(QString DebuggerName READ GetDebuggerName WRITE SetDebuggerName);
  Q_PROPERTY(quint32 PipeStages READ GetPipeStages WRITE SetPipeStages);
  Q_PROPERTY(quint32 HwThreads READ GetHwThreads WRITE SetHwThreads);
  Q_PROPERTY(quint32 NumProcessorOptions READ GetNumProcessorOptions);
  Q_PROPERTY(QString PcRegGroupName READ GetPCRegGroupName);
  Q_PROPERTY(QString PcRegName READ GetPCRegName);
  Q_PROPERTY(QString ExtendedFeaturesRegGroupName READ GetExtendedFeaturesRegGroupName);
  Q_PROPERTY(QString ExtendedFeaturesRegName READ GetExtendedFeaturesRegName);
  Q_PROPERTY(QString TargetName READ GetTargetName WRITE SetTargetName);
  Q_PROPERTY(bool DebuggablePoint READ GetDebuggablePoint WRITE SetDebuggablePoint);

public slots:
#ifndef SWIG
  bool GetIsProcessor() { return this->isProcessor(); }
  void SetIsProcessor(bool newVal) { this->putIsProcessor(newVal); }

  QString GetDebuggerName() { return this->getDebuggerName(); }
  void SetDebuggerName(const QString& newVal) { UtString nv; nv << newVal; this->putDebuggerName(nv.c_str()); }

  quint32 GetPipeStages() { return this->getPipeStages(); }
  void SetPipeStages(quint32 newVal) { this->putPipeStages(newVal); }

  quint32 GetHwThreads() { return this->getHwThreads(); }
  void SetHwThreads(quint32 newVal) { this->putHwThreads(newVal); }

  quint32 GetNumProcessorOptions(void)
  {
    return this->getNumProcessorOptions();
  }

  QString GetProcessorOption(quint32 i)
  {
    return this->getProcessorOption(i);
  }

  void AddProcessorOption(QString newOption)
  {
    UtString option;
    option << newOption;
    this->addProcessorOption(option.c_str());
  }

  QString GetPCRegGroupName(void)
  {
    return this->getPCRegGroupName();
  }

  QString GetPCRegName(void)
  {
    return this->getPCRegName();
  }

  QString GetExtendedFeaturesRegGroupName(void)
  {
    return this->getExtendedFeaturesRegGroupName();
  }

  QString GetExtendedFeaturesRegName(void)
  {
    return this->getExtendedFeaturesRegName();
  }

  QString GetTargetName(void)
  {
    return this->getTargetName();
  }

  void SetPCRegName(QString newGroupName, QString newRegName)
  {
    UtString groupName, regName;
    groupName << newGroupName;
    regName << newRegName;
    this->putPCRegName(groupName.c_str(), regName.c_str());
  }

  void SetExtendedFeaturesRegName(QString newGroupName, QString newRegName)
  {
    UtString groupName, regName;
    groupName << newGroupName;
    regName << newRegName;
    this->putExtendedFeaturesRegName(groupName.c_str(), regName.c_str());
  }

  void SetTargetName(QString newTargetName)
  {
    UtString targetName;
    targetName << newTargetName;
    this->putTargetName(targetName.c_str());
  }

  bool GetDebuggablePoint() { return this->getDebuggablePoint(); }
  void SetDebuggablePoint(bool newVal) { this->putDebuggablePoint(newVal); }
#endif

public:
  CARBONMEM_OVERRIDES

  CarbonCfgProcInfo() : mIsProcessor(false), mPipeStages(1), mHwThreads(1), mDebuggablePoint(false) { }
  ~CarbonCfgProcInfo();

  bool        isProcessor() const { return mIsProcessor; }
  const char* getDebuggerName() const { return mDebuggerName.c_str(); }
  unsigned int getPipeStages() const { return mPipeStages;}
  unsigned int getHwThreads() const { return mHwThreads;}
  const char* getPCRegGroupName() const { return mPCGroupName.c_str(); }
  const char* getPCRegName() const { return mPCName.c_str(); }
  const char* getExtendedFeaturesRegGroupName() const
  {
    return mExtendedFeaturesRegGroupName.c_str();
  }
  const char* getExtendedFeaturesRegName() const
  {
    return mExtendedFeaturesRegName.c_str();
  }
  const char* getTargetName() const { return mTargetName.c_str(); }

  //! Get the number of processor options
  UInt32 getNumProcessorOptions() const;

  //! Get a single option by index
  const char* getProcessorOption(UInt32 index) const;

  void putIsProcessor(bool val) { mIsProcessor = val; }
  void putDebuggerName(const char* name) { mDebuggerName = name; }
  void putPipeStages(unsigned int val) { mPipeStages = val; }
  void putHwThreads(unsigned int val) { mHwThreads = val; }
  void putPCRegName(const char* groupName, const char* pcName)
  {
    mPCGroupName = groupName;
    mPCName = pcName;
  }
  void putExtendedFeaturesRegName(const char* groupName, const char* regName)
  {
    mExtendedFeaturesRegGroupName = groupName;
    mExtendedFeaturesRegName = regName;
  }
  void putTargetName(const char* targetName) { mTargetName = targetName; }

  //! Add a single processor option (duplicates not pruned)
  void addProcessorOption(const char* option);

  //! Indicate if this processor supports debuggable point functions
  void putDebuggablePoint(bool debuggablePoint) { mDebuggablePoint = debuggablePoint; }

  //! Indicate if this processor supports debuggable point functions
  bool getDebuggablePoint(void) const { return mDebuggablePoint; }

  //! Function to clone this section
  /*! If you add any fields, fix this function
   */
  void cloneInPlace(const CarbonCfgProcInfo& other, CarbonCfgTemplate& templ);

  void write(CfgXmlWriter &xml);

private:
  // ***NOTE***: If you add any fields please fix cloneInPlace
  bool          mIsProcessor;
  UtString      mName;
  UtString      mDebuggerName;
  unsigned int  mPipeStages;
  unsigned int  mHwThreads;
  UtStringArray mProcessorOptions;
  UtString      mPCGroupName;
  UtString      mPCName;
  UtString      mExtendedFeaturesRegGroupName;
  UtString      mExtendedFeaturesRegName;
  bool          mDebuggablePoint;
  UtString      mTargetName;
  // ***NOTE***: If you add any fields please fix cloneInPlace
}; // class CarbonCfgProcInfo

class CarbonCfgPBucket
{
public:
  CARBONMEM_OVERRIDES

  CarbonCfgPBucket(const char* name): mIndex(0), mName(name) {}
  CarbonCfgPBucket(UtIStream& f);
  CarbonCfgPBucket(QXmlStreamReader& xr);
  ~CarbonCfgPBucket() {}

  void write(CfgXmlWriter& xml);

  const char* getName() const {return mName.c_str();}
  const char* getExpr() const {return mExpr.c_str();}
  CarbonCfgColor getColor() const {return mColor;}
  const char* getColorString() const {return mColor.getString();}

  void putExpr(const char* expr) {mExpr = expr;}
  void putColor(CarbonCfgColor color) {mColor = color;}

  void putIndex(UInt32 index) {mIndex = index;};
  UInt32 getIndex() const {return mIndex;}

private:
  friend class CarbonCfgPChannel;
  void putName(const char* name) {mName = name;}

  UInt32 mIndex;                // bucket-index across all triggers
  UtString mName;
  UtString mExpr;
  CarbonCfgColorIO mColor;
};

class CarbonCfgPNet {
public:
  CARBONMEM_OVERRIDES

  CarbonCfgPNet(const char* name, const char* path, UInt32 width) :
    mName(name),
    mPath(path),
    mWidth(width)
  {
  }
  CarbonCfgPNet(UtIStream& f);
  CarbonCfgPNet(QXmlStreamReader& f);
 void write(CfgXmlWriter& xml);

  const char* getName() const {return mName.c_str();}
  const char* getPath() const {return mPath.c_str();}
  UInt32 getWidth() const {return mWidth;}

private:
  friend class CarbonCfgPStream;
  void putName(const char* name) {mName = name;}

  UtString mName;
  UtString mPath;
  UInt32 mWidth;
};

class CarbonCfgPChannel {
public:
  CARBONMEM_OVERRIDES

  CarbonCfgPChannel(const char* name):
    mName(name), mExpr("0"), mNumTotalBuckets(0)
  {}
  CarbonCfgPChannel(UtIStream& line, UtParamFile& f, CarbonCfgErrorHandler*);
  CarbonCfgPChannel(QXmlStreamReader& xr, CarbonCfgErrorHandler*);

  ~CarbonCfgPChannel();
 void write(CfgXmlWriter& xml);

  const char* getName() const {return mName.c_str();}
  const char* getExpr() const {return mExpr.c_str();}
  UInt32 numBuckets() const {return mBuckets.size();}
  CarbonCfgPBucket* getBucket(UInt32 i) const {
    INFO_ASSERT(i < mBuckets.size(), "bucket out of range");
    return mBuckets[i];
  }

  void putExpr(const char* expr) {mExpr = expr;}
  CarbonCfgPBucket* addBucket(const char* name);

  void getUniqueBucketName(UtString*, const char*);
  void changeBucketName(CarbonCfgPBucket*, const char*);
  void removeBucket(CarbonCfgPBucket*);
  void putName(const char* name) {mName = name;}

  void putNumTotalBuckets(UInt32 i) {mNumTotalBuckets = i;}
  UInt32 getNumTotalBuckets() const {return mNumTotalBuckets;}

private:
  friend class CarbonCfgPStream;

  UtString mName;
  UtArray<CarbonCfgPBucket*> mBuckets;
  UtString mExpr;
  UtStringSet mBucketNames;
  UInt32 mNumTotalBuckets;
};

//! Class for a profiling trigger
class CarbonCfgPTrigger {
public:
  CARBONMEM_OVERRIDES

 void write(CfgXmlWriter& xml);

  const char* getTriggerExpr() const {return mTriggerExpr.c_str();}

  //! All triggers in a stream must have the same number of channels
  CarbonCfgPChannel* getChannel(UInt32 i) const {
    INFO_ASSERT(i < mChannels.size(), "channel out of range");
    return mChannels[i];
  }

  UInt32 numChannels() { return mChannels.size(); }

  void putTriggerExpr(const char* expr) {mTriggerExpr = expr;}

  CarbonCfgPChannel* addChannel(const char*);
  void removeChannel(UInt32 channelIndex);

private:
  // Only the ccfg manager can add/delete/rename pstreams
  friend class CarbonCfgPStream;

  CarbonCfgPTrigger(const char* expr);
  CarbonCfgPTrigger(UtParamFile& f, const char* expr, UInt32 numChannels,
                    CarbonCfgErrorHandler* err);
  CarbonCfgPTrigger(QXmlStreamReader& xr, const char* expr, UInt32 numChannels,
                    CarbonCfgErrorHandler* err);
  ~CarbonCfgPTrigger();

  UtString mTriggerExpr;
  UtArray<CarbonCfgPChannel*> mChannels;
}; // class CarbonCfgPTrigger
  
//! Collects channel information across all triggers
/*!
 *! Every trigger must have the same number of channels, and each
 *! channel has the same name.  Each channel also shares information
 *! about whether it is a bucket-channel or an expression-channel.
 *! For bucket-channels, the descriptor collects all the buckets
 *! used across each channel.
 *!
 *! This descriptor is not manipulated by the GUI, or stored in this ccfg
 *! file.  It's created on demand by the API and used in carmgr to generate
 *! collected bucket information
 */
class CarbonCfgPChannelDescriptor {
public:
  CARBONMEM_OVERRIDES

  //! ctor
  CarbonCfgPChannelDescriptor(const char* name): mName(name) {}

  //! dtor
  ~CarbonCfgPChannelDescriptor() {}
  
  //! Add a bucket
  void addBucket(CarbonCfgPBucket* bucket) {mAllBuckets.push_back(bucket);}

  //! Get the number of buckets
  UInt32 numBuckets() const {return mAllBuckets.size();}

  //! Get a bucket
  CarbonCfgPBucket* getBucket(UInt32 index) const {
    INFO_ASSERT(index < mAllBuckets.size(), "bucket out of range");
    return mAllBuckets[index];
  }

  //! Get the channel name
  const char* getName() const {return mName.c_str();}

private:
  UtString mName;
  UtArray<CarbonCfgPBucket*> mAllBuckets;
};

//! Class for a profiling stream
class CarbonCfgPStream {
public:
  CARBONMEM_OVERRIDES

 void write(CfgXmlWriter& xml);

  const char* getName() const {return mName.c_str();}

  UInt32 numTriggers() const {return mTriggers.size();}
  CarbonCfgPTrigger* getTrigger(UInt32 i) const {
    INFO_ASSERT(i < mTriggers.size(), "trigger out of range");
    return mTriggers[i];
  }
  CarbonCfgPTrigger* addTrigger();
  void removeTrigger(CarbonCfgPTrigger*);

  UInt32 numNets() const {return mNets.size();}
  CarbonCfgPNet* getNet(UInt32 i) const {
    INFO_ASSERT(i < mNets.size(), "net out of range");
    return mNets[i];
  }

  UInt32 numChannels() const {return mChannelNames.size();}
  const char* getChannelName(UInt32 i) const;

  //! returns -1 on failure
  SInt32 findChannelIndex(const char*);

  void addChannel(const char*);
  void removeChannel(UInt32 index);
  void changeChannelName(UInt32 index, const char* newName);
  void getUniqueChannelName(UtString*, const char*);

  CarbonCfgPNet* addNet(const char* netName, const char* leafName,
                        UInt32 width);
  void removeNet(CarbonCfgPNet*);
  void changeNetName(CarbonCfgPNet*, const char*);
  void getUniqueNetName(UtString*, const char*);

  // Is this net name (not path) part of the stream?
  bool isNetDeclared(const char* netName) const;

  void computeChannelDescriptors();

  CarbonCfgPChannelDescriptor* getChannelDescriptor(UInt32 index) const {
    INFO_ASSERT(index < mChannelDescriptors.size(),
                "channel descriptor out of range");
    return mChannelDescriptors[index];
  }

  bool isBucketChannel(UInt32 channelIndex) const;

private:
  // Only the ccfg manager can add/delete/rename pstreams
  friend class CarbonCfg;

  void clearChannelDescriptors();
  void putName(const char* name) {mName = name;}

public:
  CarbonCfgPStream(const char* name, const char* expr);
  CarbonCfgPStream(UtIStream& line, UtParamFile& f, CarbonCfgErrorHandler*);
  CarbonCfgPStream(QXmlStreamReader& xr, UtParamFile& f, CarbonCfgErrorHandler*);
  ~CarbonCfgPStream();

private:
  UtString mName;
  UtString mTriggerExpr;
  UtArray<CarbonCfgPNet*> mNets;
  UtArray<CarbonCfgPTrigger*> mTriggers;
  UtArray<CarbonCfgPChannelDescriptor*> mChannelDescriptors;
  UtStringSet mNetNames;
  UtStringSet mChannelNames;
}; // class CarbonCfgPStream
  
//! This class represents the Cadi class that is associated with each component
class CarbonCfgCadi : public CarbonCfgCustomCodeContainer
{
  Q_OBJECT

  Q_PROPERTY(QString DisassemblerName READ GetDisassemblerName WRITE SetDisassemblerName);
  Q_PROPERTY(quint32 NumCustomCodes READ NumCustomCodes)

#ifndef SWIG
public slots:
  QString GetDisassemblerName()
  { 
    return this->getDisassemblerName();
  }
  void SetDisassemblerName(const QString& newVal)
  {
    UtString nv; nv << newVal; this->putDisassemblerName(nv.c_str());
  }

  quint32 NumCustomCodes() { return this->numCustomCodes(); }
  CarbonCfgCadiCustomCode* AddCustomCode(CcfgEnum::CarbonCfgElementGenerationType genType = CcfgEnum::User)
  {
    CarbonCfgCadiCustomCode* cc = this->addCustomCode();
    cc->putElementGenerationType(CarbonCfgElementGenerationType(genType));
    return static_cast<CarbonCfgCadiCustomCode*>(cc);
  }
  CarbonCfgCadiCustomCode* GetCustomCode(quint32 i)
  {
    return static_cast<CarbonCfgCadiCustomCode*>(this->getCustomCode(i));
  }
  void RemoveCustomCode(CarbonCfgCadiCustomCode* customCode)
  {
    this->removeCustomCode(customCode);
  }
#endif

public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonCfgCadi();

  //! Destructor
  ~CarbonCfgCadi();

  //! Write class to XML file
  void write(CfgXmlWriter& xml);

  //! Clone so that we can use in a different component/sub-component.
  /*! This function clones the Cadi interface. It replaces any
   *  template variables with the corresponding values from the
   *  CarbonCfgTemplate variable map.
   *
   *  Note that this function clones the passed in cadi interface into
   *  this interface
   *
   *  If you add more fields here, please update this function.
   */
  void cloneInPlace(const CarbonCfgCadi& src, const CarbonCfgTemplate& templ);

  //! Add a Cadi specific custom code section and return it
  CarbonCfgCadiCustomCode* addCustomCode();

  //! Set dissasembler name
  void putDisassemblerName(const char* name)
  {
    mDisassemblerName = name;
  }

  //! Get dissasembler name
  const char* getDisassemblerName() const
  {
    return mDisassemblerName.c_str();
  }

private:
  //! Hide the copy constructor
  CarbonCfgCadi(const CarbonCfgCadi&);

  //! Hide the assign constructor
  CarbonCfgCadi& operator=(const CarbonCfgCadi&);

  //! Disassembler Name
  UtString mDisassemblerName;
}; // class CarbonCfgCadi : public CarbonCfgCustomCodeContainer

#ifndef SWIG
// Scripting Helpers
class CfgScriptingEngine :  public QScriptEngine
{
  Q_OBJECT

public:
  CfgScriptingEngine();
  ~CfgScriptingEngine();
  QScriptValue run(const QString& scriptFileName);
  QScriptValue runScript(const QString& scriptFileName);
  void setEnableDebugger(bool newVal=true);
  QScriptValue callScriptFunction(const QString& methodName, const QScriptValueList args);
  QScriptValue callOptionalScriptFunction(const QString& methodName, const QScriptValueList args);
  void addBuiltinFunctions();
  void registerProperty(const QString& propertyName, QObject* obj);

private slots:
  void scriptFinished();
  void executionHalted();
  void selected(const QModelIndex& index);

public:
  void createEnumerationWrappers(QScriptValue value);

private:
  bool mDebugger;
  QString mScriptFile;
};

struct CarbonEmbeddedModuleItems
{
  QString mScriptFileName;
  QList<CarbonDBNode*> mInstances;
  CfgScriptingEngine mScriptEngine;
  QScriptValue mItems;
  QScriptValue mReturnValue;
};

class CarbonCfgScriptExtension
{
public:
  CarbonCfgScriptExtension()
  {
  }

  virtual void registerTypes(QScriptEngine* engine) = 0;

};
#endif

//! CarbonCfg struct
/*!
 *! Defines an C++ interface to the ccfg file.
 *!
 *! There is also a C interface to this system, used for SWIG and
 *! Python, defined in carbon_cfg.h.
 *!
 *! The structure of the file is:
 *!
 *!  Simple name/value parameters
 *!  RTL ports
 *!  Xtor Instances (including null xtors)
 *!  Xtor Ports
 *!    Connects RTL Ports to Xtor Instances, both of which are
 *!    referenced by name.  Failure to name-match during file-read
 *!    results in a file-read error
 *!  ESL Signals
 *!  Clocks
 *!  Resets
 *!  Ties
 */
struct CarbonCfg : public QObject, public QScriptable {

  Q_OBJECT
  
  Q_CLASSINFO("ClassName", "carbon_cfg");

  Q_PROPERTY(CfgScriptingCarbonDB* Database READ GetDatabase WRITE SetDatabase)
  Q_PROPERTY(QString FilePath READ GetFilePath WRITE SetFilePath)
  Q_PROPERTY(quint32 NumSubComponents READ GetNumSubComponents)
  Q_PROPERTY(quint32 NumXtorInstances READ GetNumXtorInstances)
  Q_PROPERTY(quint32 NumESLPorts READ GetNumESLPorts)
  Q_PROPERTY(quint32 NumRTLPorts READ GetNumRTLPorts)
  Q_PROPERTY(quint32 NumParams READ GetNumParams)
  Q_PROPERTY(quint32 NumRegisters READ GetNumRegisters)
  Q_PROPERTY(quint32 NumGroups READ GetNumGroups)
  Q_PROPERTY(quint32 NumMemories READ GetNumMemories)
  Q_PROPERTY(quint32 NumCustomCodes READ GetNumCustomCodes);
  Q_PROPERTY(CarbonCfgXtorLib* XtorLib READ GetXtorLib)
  Q_PROPERTY(QString Description READ GetDescription WRITE SetDescription);
  Q_PROPERTY(QString CxxFlags READ GetCxxFlags WRITE SetCxxFlags);
  Q_PROPERTY(QString LinkFlags READ GetLinkFlags WRITE SetLinkFlags);
  Q_PROPERTY(QString SourceFiles READ GetSourceFiles WRITE SetSourceFiles);
  Q_PROPERTY(QString IncludeFiles READ GetIncludeFiles WRITE SetIncludeFiles);
  Q_PROPERTY(QString LibName READ GetLibName WRITE SetLibName);
  Q_PROPERTY(QString CompName READ GetCompName WRITE SetCompName);
  Q_PROPERTY(QString PersistentCompName READ GetPersistentCompName WRITE SetPersistentCompName);
  Q_PROPERTY(QString CompDisplayName READ GetCompDisplayName WRITE SetCompDisplayName);
  Q_PROPERTY(QString TopModuleName READ GetTopModuleName WRITE SetTopModuleName);
  Q_PROPERTY(QString WaveFile READ GetWaveFile WRITE SetWaveFile);
  Q_PROPERTY(CcfgEnum::CarbonCfgWaveType WaveType READ GetWaveType WRITE SetWaveType);
  Q_PROPERTY(CarbonCfgELFLoader* ELFLoader READ getELFLoader);
  Q_PROPERTY(CarbonCfgProcInfo* ProcInfo READ getProcInfo);
  Q_PROPERTY(CarbonCfgTemplate* Template READ getTemplate);
  Q_PROPERTY(CarbonCfgProcInfo* ProcInfo READ getProcInfo);
  Q_PROPERTY(QString Type READ GetType WRITE SetType);
  Q_PROPERTY(QString Version READ GetVersion WRITE SetVersion);
  Q_PROPERTY(CarbonCfgCadi* Cadi READ GetCadi);
  Q_PROPERTY(bool RequiresMkLibrary READ GetRequiresMkLibrary WRITE SetRequiresMkLibrary);
  Q_PROPERTY(bool UseVersionedMkLibrary READ GetUseVersionedMkLibrary WRITE SetUseVersionedMkLibrary);
  Q_PROPERTY(bool SubComponentFullName READ GetSubComponentFullName WRITE SetSubComponentFullName);
  Q_PROPERTY(QString PCTraceAccessor READ GetPCTraceAccessor WRITE SetPCTraceAccessor);
  Q_PROPERTY(QString ComponentTag READ GetComponentTag WRITE SetComponentTag);
  Q_PROPERTY(CcfgEnum::CarbonCfgElementGenerationType ElementGenerationType READ GetElementGenerationType WRITE SetElementGenerationType);
  Q_PROPERTY(CcfgFlags::CarbonCfgComponentEditFlags EditFlags READ GetEditFlags WRITE SetEditFlags);

public slots:
#ifndef SWIG
  CcfgFlags::CarbonCfgComponentEditFlags GetEditFlags() const { return mEditFlags; }
  void SetEditFlags(CcfgFlags::CarbonCfgComponentEditFlags newValue) { mEditFlags = newValue; }

  CcfgEnum::CarbonCfgStatus Write();
  CcfgEnum::CarbonCfgStatus Write(const QString& fileName);

  QString GetComponentTag() { return mComponentTag.c_str(); }
  void SetComponentTag(const QString& newVal) { UtString v; v << newVal; mComponentTag = v.c_str(); }

  CfgScriptingCarbonDB* GetDatabase();
  void SetDatabase(CfgScriptingCarbonDB* db) { mScriptingDB = db; }

  // Add a persistent component
  CarbonCfg* AddSubComp(const QString& componentName, CcfgEnum::CarbonCfgElementGenerationType genType, QString moduleName)
  {
    UtString name;
    name << componentName;
    
    if (!this->findSubComponent(name.c_str()))
    {
      CarbonCfg* comp = this->addSubComp(name.c_str());
      if (genType == CcfgEnum::Generated)
      {
        comp->mPersistentCompName.clear();
        comp->mPersistentCompName = name;
        comp->SetComponentTag(moduleName);
      }
      comp->putElementGenerationType(CarbonCfgElementGenerationType(genType));
      return comp;
    }
    else
      context()->throwError("Duplicate SubComponent: " + componentName);

    return 0;
  }

  CarbonCfg* AddSubComp(const QString& componentName)
  {
    UtString name;
    name << componentName;
    
    if (!this->findSubComponent(name.c_str()))
      return this->addSubComp(name.c_str());
    else
      context()->throwError("Duplicate SubComponent: " + componentName);

    return 0;
  }

  void RemoveSubComp(CarbonCfg* comp)
  {
    this->removeSubCompRecursive(comp);
    delete comp;
  }

  void RemoveSubComps()
  {
    this->removeSubCompsRecursive();
  }

  void RemoveRegisters()
  {
    this->removeRegistersRecursive();
  }

  void RemoveMemories()
  {
    this->removeMemoriesRecursive();
  }

  void RemoveCustomCodes()
  {
    this->removeCustomCodes();
  }

  void RemoveXtorInstances()
  {
    this->removeXtorInstancesRecursive();
  }

  void RemoveParameters()
  {
    this->removeParameters();
  }

  void RemoveGroups()
  {
    this->removeGroupsRecursive();
  }

  CarbonCfg* GetSubComponent(quint32 i)
  {
    return this->getSubComponent(i);
  }

  QString GetUniqueRegName(const QString& baseName)
  {
    UtString newName;
    UtString baseNameStr;
    baseNameStr << baseName;
    this->getUniqueRegName(&newName, baseNameStr.c_str());
    return newName.c_str();
  }
  QString GetUniqueGroupName(const QString& baseName)
  {
    UtString newName;
    UtString baseNameStr;
    baseNameStr << baseName;
    this->getUniqueGroupName(&newName, baseNameStr.c_str());
    return newName.c_str();
  }

  QString GetUniqueESLName(const QString& baseName)
  {
    UtString newName;
    UtString baseNameStr;
    baseNameStr << baseName;
    this->getUniqueESLName(&newName, baseNameStr.c_str());
    return newName.c_str();
  }

  quint32 NumGroups() const { return this->numGroups(); }
  CarbonCfgGroup* GetGroup(quint32 i) { return this->getGroup(i); }

  CarbonCfgGroup* FindGroup(const QString& groupName)
  {
    for (UInt32 i=0; i<this->numGroups(); i++)
    {
      CarbonCfgGroup* group = this->getGroup(i);
      QString gname = group->getName();
      if (gname == groupName)
        return group;
    }
    return NULL;
  }

  CarbonCfgGroup* AddGroup(const QString& groupName)
  {
    UtString name;
    name << groupName;
    return this->addGroup(name.c_str());
  }

  CarbonCfgXtorLib* GetXtorLib() { return this->findXtorLib(CARBON_DEFAULT_XTOR_LIB); }

  CarbonCfgESLPort* FindESLPort(const QString& portName)
  {
    UtString name;
    name << portName;
    return this->findESLPort(name.c_str()); 
  }
  CarbonCfgRTLPort* FindRTLPort(const QString& portName)
  {
    UtString name;
    name << portName;
    return this->findRTLPort(name.c_str()); 
  }
  CarbonCfgRegister* AddRegister(const QString& debugName, CarbonCfgGroup* group, 
    quint32 width, bool bigEndian, CcfgEnum::CarbonCfgRadix radixVal, const QString& commentStr, bool pcReg = false)
  {
    CarbonCfgRadix radix = static_cast<CarbonCfgRadix>(radixVal);
    UtString debugname;
    debugname << debugName;
    UtString comment;
    comment << commentStr;
    return this->addRegister(debugname.c_str(), group, width, bigEndian, radix, comment.c_str(), pcReg);
  }

  CarbonCfgCompCustomCode* AddCustomCode(CcfgEnum::CarbonCfgElementGenerationType genType = CcfgEnum::User)
  {
    CarbonCfgCompCustomCode* cc = this->addCustomCode();
    cc->putElementGenerationType(CarbonCfgElementGenerationType(genType));
    return cc;
  }
  CarbonCfgCompCustomCode* GetCustomCode(quint32 i) const
  {
    return this->getCustomCode(i);
  }
  void RemoveCustomCode(CarbonCfgCompCustomCode* customCode)
  {
    this->removeCustomCode(customCode);
  }
  CarbonCfgESLPort* AddESLPort(CarbonCfgRTLPort* rtlPort, const QString& name,
                               CcfgEnum::CarbonCfgESLPortType ptype,
                               const QString& typeDef,
                               CcfgEnum::CarbonCfgESLPortMode pmode)
    {
      UtString portName;
      portName << name;
      UtString portType;
      portType << typeDef;
      return this->addESLPort(rtlPort, portName.c_str(),
                                   CarbonCfgESLPortType(ptype), portType.c_str(),
                                   CarbonCfgESLPortMode(pmode));
    }


  CarbonCfgXtorParamInst* AddParam(const QString& Name, CcfgEnum::CarbonCfgParamDataType type,
    const QString& Value, CcfgEnum::CarbonCfgParamFlag scope, const QString& Description, const QString& EnumChoices = QString(), CcfgEnum::CarbonCfgElementGenerationType genType = CcfgEnum::User )
  {
    UtString name;
    name << Name;
    UtString value;
    value << Value;
    UtString description;
    description << Description;
    UtString enumChoices;
    enumChoices << EnumChoices;
    
    CarbonCfgXtorParamInst* newParam = this->addParam(name.c_str(), CarbonCfgParamDataType(type), 
                          value.c_str(), CarbonCfgParamFlag(scope),
                          description.c_str(), enumChoices.c_str());

    newParam->SetElementGenerationType(genType);
    newParam->getParam()->SetElementGenerationType(genType);

    return newParam;
  }

  CarbonCfgXtorInstance* AddXtor(const QString& instName, CarbonCfgXtor* type)
  {
    UtString name;
    name << instName;
    return this->addXtor(name.c_str(), type);
  }

  CarbonCfgRegister* GetRegister(quint32 i) const { return this->getRegister(i); }
  CarbonCfgXtorInstance* GetXtorInstance(quint32 i) const { return this->getXtorInstance(i); }
  CarbonCfgESLPort* GetESLPort(quint32 i) const { return this->getESLPort(i); }
  CarbonCfgRTLPort* GetRTLPort(quint32 i) const { return this->getRTLPort(i); }
  CarbonCfgXtorParamInst* GetParam(quint32 i) const { return this->getParam(i); }
  CarbonCfgXtor* FindXtor(const QString& xtorName)
  {
    UtString name;
    name << xtorName;
    return this->findXtor(name.c_str(), CARBON_DEFAULT_XTOR_LIB);
  }
  CarbonCfgXtorInstance* FindXtorInstance(const QString& xtorName)
  {
    UtString name;
    name << xtorName;
    return this->findXtorInstance(name.c_str());
  }
  void RemoveXtorInstance(CarbonCfgXtorInstance* xtorInst)
  {
    this->removeXtorInstanceRecursive(xtorInst);
  }

  CarbonCfgRegister* FindRegister(const QString& regName)
  {
    for (UInt32 i=0; i<this->numRegisters(); i++)
    {
      CarbonCfgRegister* reg = this->getRegister(i);
      if (reg->getName() == regName)
        return reg;
    }
    return NULL;
  }

  void RemoveRegister(CarbonCfgRegister* reg)
  {
    this->removeRegisterRecursive(reg);
  }

  void RemoveMemory(CarbonCfgMemory* mem)
  {
    this->removeMemoryRecursive(mem);
  }

  void Disconnect(CarbonCfgRTLConnection* conn)
  {
    this->disconnect(conn);
  }

  CarbonCfgMemory* AddMemory(const QString& name, quint32 width, quint64 maxAddr, CcfgEnum::CarbonCfgElementGenerationType genType)
  {
    UtString uName;
    uName << name;
    CarbonCfgMemory* mem = this->addMemory(uName.c_str(), width, maxAddr);

    mem->putElementGenerationType(CarbonCfgElementGenerationType(genType));

    return mem;
  }


  CarbonCfgMemory* AddMemory(const QString& name, quint32 width, quint64 maxAddr)
  {
    UtString uName;
    uName << name;
    return this->addMemory(uName.c_str(), width, maxAddr);
  }

  quint32 NumMemories() const { return this->numMemories(); }
  CarbonCfgMemory* GetMemory(quint32 i)
  {
    return this->getMemory(i);
  }

  CarbonCfgCadi* GetCadi() { return this->getCadi(); }

  QScriptValue Clone(QVariant v);

  CarbonCfgCompCustomCode* CloneCustomCode(CarbonCfgCompCustomCode* cc)
  {
    CarbonCfgCompCustomCode* newCc = this->clone(cc);
    if (this->hasMessages()) {
      UtIO::cout() << this->getErrmsg();
      this->clearMessages();
    }
    return newCc;
  }

  CarbonCfgRegister* Clone(CarbonCfgGroup* group, CarbonCfgRegister* cc)
  {
    CarbonCfgRegister* newReg = this->clone(group, cc);
    if (this->hasMessages()) {
      UtIO::cout() << this->getErrmsg();
      this->clearMessages();
    }
    return newReg;
  }

  CarbonCfgMemory* CloneMemory(CarbonCfgMemory* cc)
  {
    CarbonCfgMemory* newMem = this->clone(cc);
    if (this->hasMessages()) {
      UtIO::cout() << this->getErrmsg();
      this->clearMessages();
    }
    return newMem;
  }

  void CloneCadi(CarbonCfgCadi* cadi)
  {
    this->clone(cadi);
    if (this->hasMessages()) {
      UtIO::cout() << this->getErrmsg();
      this->clearMessages();
    }
  }

  void CloneProcInfo(CarbonCfgProcInfo* procInfo)
  {
    this->clone(procInfo);
    if (this->hasMessages()) {
      UtIO::cout() << this->getErrmsg();
      this->clearMessages();
    }
  }


  void SetRequiresMkLibrary(bool req)
  {
    this->putRequiresMkLibrary(req);
  }

  bool GetRequiresMkLibrary(void)
  {
    return this->getRequiresMkLibrary();
  }

  void SetUseVersionedMkLibrary(bool req)
  {
    this->putUseVersionedMkLibrary(req);
  }

  bool GetUseVersionedMkLibrary(void)
  {
    return this->getUseVersionedMkLibrary();
  }

  QString GetPCTraceAccessor() const { return this->getPCTraceAccessor(); }
  void SetPCTraceAccessor(const QString& newVal) { UtString nv; nv << newVal; this->putPCTraceAccessor(nv.c_str()); }

  QString GetFilePath() const { return mFilePath; }
  void SetFilePath(const QString& newVal) { mFilePath=newVal; }

  QString GetDescription() const { return this->getDescription(); }
  void SetDescription(const QString& newVal) { UtString nv; nv << newVal; this->putDescription(nv.c_str()); }

  QString GetCxxFlags() const { return this->getCxxFlags(); }
  void SetCxxFlags(const QString& newVal) { UtString nv; nv << newVal; this->putCxxFlags(nv.c_str()); }

  QString GetLinkFlags() const { return this->getLinkFlags(); }
  void SetLinkFlags(const QString& newVal) { UtString nv; nv << newVal; this->putLinkFlags(nv.c_str()); }

  QString GetSourceFiles() const { return this->getSourceFiles(); }
  void SetSourceFiles(const QString& newVal) { UtString nv; nv << newVal; this->putSourceFiles(nv.c_str()); }

  QString GetIncludeFiles() const { return this->getIncludeFiles(); }
  void SetIncludeFiles(const QString& newVal) { UtString nv; nv << newVal; this->putIncludeFiles(nv.c_str()); }

  QString GetLibName() const { return this->getLibName(); }
  void SetLibName(const QString& newVal) { UtString nv; nv << newVal; this->putLibName(nv.c_str()); }

  QString GetCompName() const { return this->getCompName(); }
  void SetCompName(const QString& newVal) { mCompName.clear(); mCompName << newVal; }

  QString GetPersistentCompName() const { return mPersistentCompName.c_str(); }
  void SetPersistentCompName(const QString& newVal) { mPersistentCompName.clear(); mPersistentCompName << newVal; }

  QString GetCompDisplayName() const { return this->getCompDisplayName(); }
  void SetCompDisplayName(const QString& newVal) { UtString nv; nv << newVal; this->putCompDisplayName(nv.c_str()); }

  QString GetTopModuleName() const { return this->getTopModuleName(); }
  void SetTopModuleName(const QString& newVal) { UtString nv; nv << newVal; this->putTopModuleName(nv.c_str()); }

  QString GetWaveFile() const { return this->getWaveFile(); }
  void SetWaveFile(const QString& newVal) { UtString nv; nv << newVal; this->putWaveFile(nv.c_str()); }

  CcfgEnum::CarbonCfgWaveType GetWaveType() const { return CcfgEnum::CarbonCfgWaveType(this->getWaveType()); }
  void SetWaveType(CcfgEnum::CarbonCfgWaveType wt) { this->putWaveType(CarbonCfgWaveType(wt)); }

  CcfgEnum::CarbonCfgElementGenerationType GetElementGenerationType() const { return CcfgEnum::CarbonCfgElementGenerationType(this->getElementGenerationType()); }
  void SetElementGenerationType(CcfgEnum::CarbonCfgElementGenerationType wt) { this->putElementGenerationType(CarbonCfgElementGenerationType(wt)); }

  QString GetType(void) const
  {
    return this->getCompType();
  }

  void SetType(const QString& name)
  {
    UtString n;
    n << name;
    this->putCompType(n.c_str());
  }

  QString GetVersion(void) const
  {
    return this->getCompVersion();
  }

  void SetVersion(const QString& name)
  {
    UtString n;
    n << name;
    this->putCompVersion(n.c_str());
  }

  QString GetDocFile(void) const
  {
    return this->getCompDocFile();
  }

  void SetDocFile(const QString& name)
  {
    UtString n;
    n << name;
    this->putCompDocFile(n.c_str());
  }

  QString GetLoadfileExtension() const { return this->getLoadfileExtension(); }
  void SetLoadfileExtension(const QString& newVal)
  {
    UtString nv;
    nv << newVal;
    this->putLoadfileExtension(nv.c_str());
  }

  bool GetUseStaticScheduling() const { return this->getUseStaticScheduling(); }
  void SetUseStaticScheduling(quint32 val) { this->putUseStaticScheduling(val); }

  int GetLegacyMemories() const { return this->useLegacyMemories(); }
  void SetLegacyMemories(qint32 val) { this->putLegacyMemories(val); }

  int GetIsStandaloneComp() const { return this->isStandAloneComp(); }
  void SetIsStandaloneComp(qint32 val) { this->putIsStandAloneComp(val); }

  int GetSubComponentFullName() const { return this->isSubComponentFullName(); }
  void SetSubComponentFullName(qint32 val) { this->putSubComponentFullName(val); }

  quint32 GetNumSubComponents() const { return this->numSubComponents(); }
  quint32 GetNumXtorInstances() const { return this->numXtorInstances(); }
  quint32 GetNumESLPorts() const { return this->numESLPorts(); }
  quint32 GetNumRTLPorts() const { return this->numRTLPorts(); }
  quint32 GetNumParams() const { return this->numParams(); }
  quint32 GetNumRegisters() const { return this->numRegisters(); }
  quint32 GetNumGroups() const { return this->numGroups(); }
  quint32 GetNumMemories() const { return this->numMemories(); }
  quint32 GetNumCustomCodes() const { return this->numCustomCodes(); }

#endif

public:
  CARBONMEM_OVERRIDES

public:
  //! constructor
  CarbonCfg();

  //! sub component constructor
  CarbonCfg(CarbonCfg* parent);

#ifdef CARBON_PV
  CarbonCfg(CarbonContext*); // called from cbuild only
#endif

  //! Tells which license to checkout when opening the db
  /*!
    This has interesting enum values just to make it ananoying
    to try to subvert the license checkout (not a big deal -- you're
    going to have to checkout a license even if you subvert it). 
  */
  enum Mode {
    eUnknown = 0,    //!< Undefined mode
    eRvsocd =  1,    //!< Realview mode (default)
    ePlatarch = 2,   //!< Platform architect mode
    eSystemC = 3     //!< SystemC mode
  };

  //! Construct based on mode
  CarbonCfg(Mode mode);

  virtual ~CarbonCfg();
  void clear();
  void clearRegister();
  void clearMemory();
  void clearCustomCode();
  CarbonCfgStatus read(const char* filename);
  CarbonCfgStatus readNoCheck(const char* filename);
  CarbonCfgStatus parseXMLCcfg(QXmlStreamReader& xr, UtString& lineErr, CarbonCfgErrorHandler* err, UtParamFile& f);

  //
  // Create a component from a Carbon Database, if the db is .gui.db
  // then we need the .designHierarchy file to supplement the port information
  // 
  void createDefaultComponent(const char* dbFileName, const char* designHierarchyFile, CarbonXtorsType xtorsType);

 private:  
   void createComponent(CarbonDB* carbonDB, const char* dbFileName, const char* designHierarchyFile, CarbonXtorsType xtorsType);
   void populateLoop(CarbonDB* carbonDB, CarbonCfgRTLPortType type, CarbonDBNodeIter* iter);
   CarbonCfgESLPort* addESLConnection(CarbonDB* db, CarbonCfgRTLPort* rtlPort, CarbonCfgESLPortType type);
   CarbonCfgESLPort* addConnection(CarbonDB* db, CarbonCfgRTLPort* rtlPort);

   void parseFeatures(QXmlStreamReader& xr, UtString& /*lineErr*/, CarbonCfgErrorHandler* /*err*/);

 public:
  CarbonCfgStatus write(const char* filename);
  const char* cbuildVersion();

  //! Remove all connections for this RTLPort
  void removeAllConnections(CarbonCfgRTLPort* port);
  //! get the last error message
#ifndef SWIG
  const char* carbonCfgGetErrmsg() const;
#endif

  // Remove the information on an item, if it exists already,
  // and insert it into the vector and name-map

#ifndef SWIG
  void resetItem(const char* name);
  void deleteItem(const char* name, bool removeFromVec);

  void putTicksPerBeat(UInt32 ticks);
#endif

  //! An an RTL port if one by that name does not yet exist.
  /*! If one already exists of that name, return NULL
   */
  void removeRTLPort(CarbonCfgRTLPort* rtlPort);
  CarbonCfgRTLPort* addRTLPort(const char* name, UInt32 width,
                               CarbonCfgRTLPortType);

  //! create a new ESL port, add it to the master list, connect it to an RTL port
  CarbonCfgESLPort* addESLPort(CarbonCfgRTLPort*,
                               const char* name, CarbonCfgESLPortType, 
                               const char* typeDef, CarbonCfgESLPortMode);

  //! Helper function to compute the default SystemC type
  const char* computeSystemCType(CarbonCfgRTLPort* rtlPort);

  //! Get the number of supported SystemC types
  UInt32 numSystemCTypes(void);

  //! Get an individual SystemC type
  const char* getSystemCType(UInt32 i);

  //! add a pre-created, pre-connected ESL port to the master list
  bool addESLPort(CarbonCfgESLPort*);
  UInt32 numESLPorts() const {return mESLPortVec.size();}
  CarbonCfgESLPort* getESLPort(UInt32 i) const {
    INFO_ASSERT(i < mESLPortVec.size(), "ESLPortVec out of range");
    return mESLPortVec[i];
  }
  CarbonCfgESLPort* findESLPort(const char*);

  UInt32 numRTLPorts() const {return mRTLPortVec.size();}
  CarbonCfgRTLPort* getRTLPort(UInt32 i) const {
    INFO_ASSERT(i < mRTLPortVec.size(), "RTLPortVec out of range");
    return mRTLPortVec[i];
  }
  CarbonCfgRTLPort* findRTLPort(const char*);
  
  UInt32 numXtorInstances() const {return mXtorVec.size();}
  CarbonCfgXtorInstance* getXtorInstance(UInt32 i) const {
    INFO_ASSERT(i < mXtorVec.size(), "XtorVec out of range");
    return mXtorVec[i];
  }
  CarbonCfgXtorInstance* findXtorInstance(const char*);

  UInt32 numGroups() const { return mGroupVec.size(); }
  CarbonCfgGroup* getGroup(UInt32 i) const { return mGroupVec[i]; }

  void addParam(CarbonCfgXtorParamInst* param);
  CarbonCfgXtorParamInst* addParam(const char* name, CarbonCfgParamDataType type, const char *value, CarbonCfgParamFlag scope, const char* description, const char* enumChoices = "")
  {
    // For some reason size is value
    UInt32 size = atoi(value);
    CarbonCfgXtorParam* param = new CarbonCfgXtorParam(size, type, scope, name, description, value, enumChoices, NULL, NULL);
    CarbonCfgXtorParamInst* inst = new CarbonCfgXtorParamInst(NULL, param);
    addParam(inst);
    return inst;
  }
  void removeParam(CarbonCfgXtorParamInst* param);
  UInt32 numParams() const {return mParamVec.size();}
  CarbonCfgXtorParamInst* getParam(UInt32 i) const {
    INFO_ASSERT(i < mParamVec.size(), "ParamVec out of range");
    return mParamVec[i];
  }

  void putDescription(const char* str);
  const char* getDescription() const;
  void putCxxFlags(const char* str);
  const char* getCxxFlags() const;
  void putLinkFlags(const char* str);
  const char* getLinkFlags() const;
  void putSourceFiles(const char* str);
  const char* getSourceFiles() const;
  void putIncludeFiles(const char* str);
  const char* getIncludeFiles() const;
  void putIsStandAloneComp(bool);
  bool isStandAloneComp() const;
  void putSubComponentFullName(bool);
  bool isSubComponentFullName() const;

  //! Get the Cadi class representation that matches this component
  CarbonCfgCadi* getCadi() { return &mCadi; }

  //! Add a subcomponent
  /*!
  This will use the component type of the parent by default.
  */
  CarbonCfg* addSubComp(const char* name);
  CarbonCfgRegister* addRegister(const char* debugName, CarbonCfgGroup* group,
                                 unsigned int width, bool big_endian,
                                 CarbonCfgRadix radix, const char* comment,
                                 bool pc_reg);
  CarbonCfgGroup* addGroup(const char* groupName);
  CarbonCfgPStream* addPStream(const char* name);
  CarbonCfgMemory* addMemory(const char* signal, const char* name,
                             const char* initFile,
                             UInt64 maxAddrs, UInt32 width,
                             CarbonCfgReadmemType rt,
                             const char* comment, 
			     const char* disassemblyName = "",
			     const char* disassemblerName = "");
  CarbonCfgMemory* addMemory(const char* name, UInt32 width, UInt64 maxAddr);

  //! Moves the source memory from where it currently resides to this component 
  void moveMemory(CarbonCfgMemory* srcMem);

  CarbonCfgCompCustomCode* addCustomCode();
  int useLegacyMemories() const;  // see wiki Eng/CadiMemories or search wiki for useLegacyMemories
  void putLegacyMemories(int val) { mLegacyMemories = val; }

#ifdef CARBON_PV
  void putPVContext(PVContext* pvc) { mPVContext = pvc; }
#endif

  //! An an xtor instance, and return it
  /*! If the transactor type is not found in the library, or the name
   *! is already used as an ESLPort or XtorInstance, return NULL
   */
  CarbonCfgXtorInstance* addXtor(const char* name, CarbonCfgXtor* type);

public:
  //! Find a transactor library by name (CoWare has more than one, SD may have more than one later)
  CarbonCfgXtorLib* findXtorLib(const char* libraryName);

  QString getComponentJournalXML() const { return mComponentJournalXML.c_str(); }
  void putComponentJournalXML(const QString& newVal) { mComponentJournalXML.clear(); mComponentJournalXML << newVal; }

  typedef UtHashMap<UtString, CarbonCfgXtorLib*> XtorLibs;

  bool addXtorLib(CarbonCfgXtorLib* xtorLib);
#ifndef SWIG
  XtorLibs::SortedLoop loopXtorLibs() { return mXtorLibs.loopSorted(); }
#endif

  //! This is only set at read time if we decided to change the ccfg.
  bool isModified() const { return mIsModified; }
  void putIsModified(bool modified) { mIsModified = modified; }

  CarbonCfgStatus readXtorLib(CarbonXtorsType xtorTypes);
  const char* getErrmsg() const {return mMsgHandler.getErrMsg();}
  CarbonUInt32 getNumWarnings() const { return mMsgHandler.getNumWarnings(); }
  CarbonUInt32 getNumErrors() const { return mMsgHandler.getNumErrors(); }
  bool hasMessages() const { return mMsgHandler.hasMessages(); }
  void clearMessages() { mMsgHandler.clear(); }
  void reportMessage(CarbonMsgSeverity sev, const char* msg, 
                     const char* file = NULL, int lineno = 0)
  {
    mMsgHandler.reportMessage(sev, msg, file, lineno);
  }
  void putLibName(const char* s) {mLibName = s;}
  const char* getLibName() const {return mLibName.c_str();}
  void putCompName(const char* s) {mCompName = s;}
  const char* getCompName() const {return mCompName.c_str();}
  void putCompDisplayName(const char* s) { mCompDisplayName = s; }
  const char* getCompDisplayName() const { return mCompDisplayName.c_str(); }
  void putTopModuleName(const char* s) {mTopModuleName = s;}
  const char* getTopModuleName() const {return mTopModuleName.c_str();}
  void putWaveFile(const char* s) {mWaveFile = s;}
  const char* getWaveFile() const {return mWaveFile.c_str();}
  void putWaveType(CarbonCfgWaveType t) {mWaveType = t;}

  void putElementGenerationType(CarbonCfgElementGenerationType t) {mElementGenerationType = t;}
  CarbonCfgElementGenerationType getElementGenerationType() const { return mElementGenerationType; }

  CarbonCfgWaveType getWaveType() const {return mWaveType;}

  void putLoadfileExtension(const char* s) {mLoadfileExtension = s;}
  const char* getLoadfileExtension() const {return mLoadfileExtension.c_str();}

  void putUseStaticScheduling(int use) {mUseStaticScheduling = use;}
  bool getUseStaticScheduling() const {return mUseStaticScheduling;}

  void putCcfgMode(CarbonCfgMode m) {mMode = static_cast<Mode>(m);}
  CarbonCfgMode getCcfgMode() const {return static_cast<CarbonCfgMode>(mMode);}
  void putIODBFileName(const char* s) {mIODBFile = s;}
  const char* getIODBFileName() const {return mIODBFile.c_str();}
  UInt32 numSubComponents() const {return mSubCompVec.size();}
  CarbonCfg* getSubComponent(UInt32 i) const {
    INFO_ASSERT(i < mSubCompVec.size(), "RegisterVec out of range");
    return mSubCompVec[i];
  }
  CarbonCfg* findSubComponent(const char* name);

  UInt32 numRegisters() const {return mRegisterVec.size();}
  CarbonCfgRegister* getRegister(UInt32 i) const {
    INFO_ASSERT(i < mRegisterVec.size(), "RegisterVec out of range");
    return mRegisterVec[i];
  }
  UInt32 numMemories() const {return mMemoryVec.size();}
  CarbonCfgMemory* getMemory(UInt32 i) const {
    INFO_ASSERT(i < mMemoryVec.size(), "MemoryVec out of range");
    return mMemoryVec[i];
  }
  UInt32 numCustomCodes() const;
  CarbonCfgCompCustomCode* getCustomCode(CarbonUInt32 i) const;
  const char** getLegalSections() { return mCustomCodes.getLegalSections(); }
  
  UInt32 numPStreams() const {return mPStreamVec.size();}
  CarbonCfgPStream* getPStream(UInt32 i) const {
    INFO_ASSERT(i < mPStreamVec.size(), "PStreamVec out of range");
    return mPStreamVec[i];
  }

  CarbonCfgELFLoader* getELFLoader() { return &mELFLoader; }
  CarbonCfgProcInfo*  getProcInfo() { return &mProcInfo; }

  //! construct a unique ESL name given the starting name
  /*!
   *! If the starting name is unique, use that, otherwise start
   *! appending consecutive integers onto the name until it is
   *! unique
   */
  void getUniqueESLName(UtString* outbuf, const char* name);
  void getUniqueMemName(UtString* outbuf, const char* name);
  void getUniqueRegName(UtString* outbuf, const char* name);
  void getUniqueGroupName(UtString* outbuf, const char* name);
  void getUniquePStreamName(UtString* outbuf, const char* name);

public:
  //! shareable utility function to help with uniquifying names
  static void getUniqueName(UtString* outbuf, UtStringSet* names,
                            const char* name,
                            const char* emptyName = "x");

  //! utility function to divide two integers and round the result
  template<typename T>
  static T divideAndRound(const T dividend, const T divisor)
  {
    T quotient = dividend / divisor;
    if ((dividend % divisor) >= (divisor / 2))
      ++quotient;

    return quotient;
  }

  void disconnect(CarbonCfgRTLConnection*);
  void changeESLName(CarbonCfgESLPort*, const char* name);
  void changePStreamName(CarbonCfgPStream*, const char* name);
  void changeXtorInstanceName(CarbonCfgXtorInstance*, const char* name);
  void changeMemoryName(CarbonCfgMemory*, const char* name);
  void changeRegName(CarbonCfgRegister*, const char* name);
  void changeGroupName(CarbonCfgGroup* group, const char* name);

  //! Remove a single sub component from this component
  /*! Removes the sub component only. It does not remove any sub
   *  structures. Use the removeSubCompRecursive to remove all sub
   *  structures.
   */
  void removeSubComp(CarbonCfg*);
  void removeSubCompUndo(CarbonCfg*); // Undo Version
  void restoreSubCompRedo(CarbonCfg*);    // Redo Version

  //! Temove a single transactor instance from this component
  /*! Removes the transactor instance only. It does not remove any sub
   *  structures. Use the removeXtorInstanceRecursive to remove all
   *  sub structures.
   */
  void removeXtorInstance(CarbonCfgXtorInstance*);

  //! Remove a single memory for this component
  /*! Removes the memory only. It does not remove any sub
   *  structures. Use the removeMemoryRecursive to remove all sub
   *  structures.
   */
  void removeMemory(CarbonCfgMemory* mem);
  int removeMemoryUndo(CarbonCfgMemory* mem);
  void insertMemoryUndo(int pos, CarbonCfgMemory* mem);

  void removePStream(CarbonCfgPStream* pstream);

  //! Remove a single register
  /*! Removes the register only. It does not remove any sub
   *  structures. Use the removeRegisterRecursive to remove all sub
   *  structures.
   */
  void removeRegister(CarbonCfgRegister* reg);

  //! Removes a group.
  /*! Removes the group only. It does not remove any registers
   *  associate with this group. This can be dangerous because
   *  registers will point to deleted memory. Use the
   *  removeGroupRecursive to remove the group and any associated
   *  registers.
   * 
   */
  void removeGroup(CarbonCfgGroup* reg);

  //! Removes a single component custom code section 
  void removeCustomCode(CarbonCfgCompCustomCode* customCode);

  //! Remove the sub component and all its sub components
  /*! This recursively removes all the parts of a sub component
   *  including any registers, memories, custom code sections etc.
   */
  void removeSubCompRecursive(CarbonCfg* cfg);

  //! Remove the transactor and all its connections
  void removeXtorInstanceRecursive(CarbonCfgXtorInstance* xtor);

  //! Remove a memory and all its sub blocks
  void removeMemoryRecursive(CarbonCfgMemory* mem);

  //! Remove a register and all its sub fields and custom codes
  void removeRegisterRecursive(CarbonCfgRegister* reg);

  //! Remove a group and all its registers recursively
  void removeGroupRecursive(CarbonCfgGroup* group);

  //! Remove all sub components recursively
  /*! Removes all sub components and all the sub structures of a component.
   */
  void removeSubCompsRecursive(void);

  //! Remove all transactor isntances and all their connections
  void removeXtorInstancesRecursive(void);

  //! Remove all registers in this component recursively
  /*! Removes all registers and its sub structures
   */
  void removeRegistersRecursive(void);

  //! Remove all memories in this component recursively
  /*! Removes all memories and its sub structures
   */
  void removeMemoriesRecursive(void);

  //! Remove all component custom code sections
  void removeCustomCodes(void);

  //! Remove all parameters
  void removeParameters(void);

  //! Remove all groups and any registers that are connect to them
  void removeGroupsRecursive(void);

  //! Change a register's group, creating a new group if needed
  void changeRegGroup(CarbonCfgRegister *reg, const char *group_name);

  UInt32 getVersion() const {return mVersion;}

  //! Does this version of the CCFG format include port exprs?
  bool hasPortExprs() const {
    return (mFeatureMask & (1 << eCfgFeaturePortExpr)) != 0;
  }

  //! Does this version of the CCFG format include transactor connection exprs?
  bool hasXtorConnExprs() const {
    return (mFeatureMask & (1 << eCfgFeatureXtorConnExpr)) != 0;
  }

  bool hasAbstractRegs() const {
    return (mFeatureMask & (1 << eCfgFeatureAbstractRegs)) != 0;
  }

  bool hasESLPortParameter() const {
    return (mFeatureMask & (1 <<eCfgFeatureEslPortParameter)) != 0;
  }

  bool hasESLPortModes() const {
      return (mFeatureMask & (1 << eCfgFeatureEslPortModes)) != 0;
  }

  bool hasDisassembly() const {
      return (mFeatureMask & (1 << eCfgFeatureDisassembly)) != 0;
  }

  //! Does this version of the CCFG format include clocked xtors
  /*!
   *! Note that we didn't have the 'feature' line when we first
   *! added clocked xtors
   */
  bool hasClockedXtors() const {
    return mVersion >= 5 && mVersion <= 10;
  }

  //! Does this version have a memory init enumeration?
  bool hasMemoryInitEnum() const {
    return (mFeatureMask & (1 << eCfgFeatureMemInitEnum)) != 0;
  }

  //! Does this version support transactors clocked by transactors?
  bool hasXtorClockMaster() const {
    return (mFeatureMask & (1 << eCfgFeatureXtorClockMaster)) != 0;
  }

  //! Does this version support transactors reseted by transactors?
  bool hasXtorTLM2Support() const {
    return (mFeatureMask & (1 << eCfgFeatureXtorTLM2Support)) != 0;
  }

  //! Does this version support transactors reseted by transactors?
  bool hasEmbeddedModelsSupport() const {
    return (mFeatureMask & (1 << eCfgFeatureEmbeddedModelSupport)) != 0;
  }


  //! Does this version configure clocking without using ticks?
  bool hasNoTicks() const {
    return (mFeatureMask & (1 << eCfgFeatureNoTicks)) != 0;
  }
  
  //! Does this version allow arbitrary active and inactive values for reset generator
  bool hasResetValues() const {
    return (mFeatureMask & (1 << eCfgFeatureResetValues)) != 0;
  }

  //! Does this version allow new debug access methods for transactors
  bool hasXtorDebugAccess() const {
    return (mFeatureMask & (1 << eCfgFeatureXtorDebugAccess)) != 0;
  }

  //! Does this version allow for library and variant name for the transactor
  bool hasXtorFullName() const {
    return (mFeatureMask & (1 << eCfgFeatureXtorFullName)) != 0;
  }

  //! All actions on templates go through the structure owned by this ccfg
  CarbonCfgTemplate* getTemplate(void) { return &mTemplateVariables; }

  //! Clone a component custom code section for use in a different component.
  /*! This function calls the clone function on the given custom code
   *  section using the template variable map for this component.
   */
  CarbonCfgCompCustomCode* clone(CarbonCfgCompCustomCode* cc);

  //! Clone a register for use in a different component.
  /*! This function calls the clone function on the given register
   *  using the template variable map for this component.
   *
   *  Returns NULL if the register already exists
   */
  CarbonCfgRegister* clone(CarbonCfgGroup* group, CarbonCfgRegister* reg);

  //! Clone a memory for use in a different component.
  /*! This function calls the clone function on the given memory
   *  using the template variable map for this component.
   *
   *  Returns NULL if the memory already exists
   */
  CarbonCfgMemory* clone(CarbonCfgMemory* mem);

  //! Clone a component cadi interface from another
  void clone(const CarbonCfgCadi* cadi);

  //! Clone the proc info from another
  void clone(const CarbonCfgProcInfo* procInfo);

  // Returns true if this is a toplevel component
  virtual bool isTopLevel() const=0;

  //! Get the DB
  /*!
   * Note this behavior is different for sub components than the top
   * component. This is because top components own the DB and have to
   * destruct it. Storing a copy in the sub components was difficult
   * because we couldn't guarantee that it was set correctly (too many
   * ways to set the DB). 
   *
   * See the CarbonCfg derived classes CarbonCfgTop and CarbonCfgSub
   */
  virtual CarbonDB* getDB() const = 0;

  //! Ownership is tranferred here, the destructor will free this
  /*! This is only valid for the top DB
   */
  virtual void putDB(CarbonDB* db) = 0;

  //! Put the type name for this component
  void putCompType(const char* name);

  //! get the type name for this component
  virtual const char* getCompType() const;

  //! Put the version for this component
  void putCompVersion(const char* version);

  //! Get the version for this component
  const char* getCompVersion(void) const;

  //! Put the doc file for this component
  void putCompDocFile(const char* docFile);

  //! Get the doc file for this component
  const char* getCompDocFile(void) const;

  //! Put whether this model requires the model kit library or not
  void putRequiresMkLibrary(bool req) { mRequiresMkLibrary = req; }

  //! Get whether this model requires the model kit library
  bool getRequiresMkLibrary(void) const { return mRequiresMkLibrary; }

  //! Put whether this model uses a versioned model kit library
  void putUseVersionedMkLibrary(bool req) { mUseVersionedMkLibrary = req; }

  //! Get whether this model uses a versioned model kit library
  bool getUseVersionedMkLibrary(void) const { return mUseVersionedMkLibrary; }

  //! Put the SoCDPCTraceReporter accessor expression for this component
  void putPCTraceAccessor(const char* name) { mPCTraceAccessor = name; }

  //! Gett the SoCDPCTraceReporter accessor expression for this component
  const char* getPCTraceAccessor() const { return mPCTraceAccessor.c_str(); }

  //! Helper function to convert an rtl port type to an esl port type
  static CarbonCfgESLPortType convertRTLPortType(CarbonCfgRTLPort* rtlPort);

  //! Get the component tag for this component
  const char* getComponentTag(void)
  {
    return mComponentTag.c_str();
  }

  void putRegisterBaseAddress(const char* baseAddr) { mRegisterBaseAddress = baseAddr; }
  const char* getRegisterBaseAddress() { return mRegisterBaseAddress.c_str(); }

  // Scripting functions
public:
    //! find a transactor from the library (not an instance)
  CarbonCfgXtor* findXtor(const char* xtorName, const char* libraryName, const char* variant="");
  CarbonCfgStatus processEmbeddedModules();
  static void registerTypes(QScriptEngine*);
  void addScriptExtension(CarbonCfgScriptExtension* ext)
  {
    mScriptExtensions.push_back(ext);
  }
 
  void clearScriptExtensions()
  {
    mScriptExtensions.clear();
  }

private:
  void findEmbeddedModules(CarbonDB* db, QMap<QString, CarbonEmbeddedModuleItems*>& moduleMap, const CarbonDBNode* parent);
  void restoreSubComponentsJournal(const QDomElement& parent);
  void restoreComponentJournal(const QDomElement& parent);
  void restoreMemoryJournal(CarbonCfg* comp, const QDomElement& parent);
  void findEmbeddedModuleScripts();
  QMap<QString, CarbonEmbeddedModuleItems*> mModuleMap;
  QList<CarbonCfgScriptExtension*> mScriptExtensions;

private:
  QString mFilePath;

  // A generated tag used to identify renamed components
  UtString mComponentTag;
  CcfgFlags::CarbonCfgComponentEditFlags mEditFlags;

  // map of modulename -> script path
  QMap<QString, QString> mScriptableModuleMap;

  CarbonCfgMsgHandler mMsgHandler;
  typedef UtHashSet<UtString> FileMap;

  bool readCowareLibs(const char* path, bool readXml);
  bool readCowareLibraryFile(FileMap& fileMap, const char* path, bool readXml);
  bool readCowareXtorDefinitions(const char* libfile, UtString* errmsg);

  void verifyCarbonPortLoop(CarbonDB* carbonDB, CarbonDBNodeIter* iter);
  void checkCarbonRTLPorts();
  bool loadAndCheckDatabase();
  void checkCcfgIntegrity();
  void checkCcfgPorts();
  void checkCcfgRegisters();
  void checkCcfgMemories();
  void checkCcfgPStreams();
  void checkCcfgXtorConnections();
  void checkCcfgSubComponents();
  const CarbonDBNode* findCarbonPort(CarbonCfgRTLPort* port);
  CarbonCfgRTLPortType computePortType(const CarbonDBNode* node);
  bool portTypesEqual(CarbonCfgRTLPortType cfgPortType, const CarbonDBNode* node);
  static eCarbonMsgCBStatus sMsgCallback(CarbonClientData, CarbonMsgSeverity,
                                         int number, const char* text,
                                         unsigned int len);
  static eCarbonMsgCBStatus sOpenDBMessageCB(CarbonClientData, CarbonMsgSeverity,
                                             int number, const char* text,
                                             unsigned int len);
  //! Adds a parameter to an xtor instance from a ccfg file.
  void addParameter(UtIStream& f);
  void addParameter(CarbonCfgXtorInstance*, QXmlStreamReader&);

  void write(CfgXmlWriter& xml);

  //! Writes the XML register description to a stream
  void writeXmlRegisters(CfgXmlWriter& xml);

  //! Writes the XML memory description to a stream
  void writeXmlMemories(CfgXmlWriter& xml);

  //! Writes the XML parameter description to a stream
  void writeXmlParamDef(CfgXmlWriter &s);

  //! Writes the XML subcomponent description to a stream
  void writeXmlComps(CfgXmlWriter &s);

#ifdef CARBON_PV
  //! Writes the XML PSD description to a stream
  void writeXmlPSD(UtOStream &s);
#endif
  //! initialize the object
  void init(Mode mode);

  //! Traverses a directory recursively looking for xactors.xml files.
  int findXtorDefs(const char* dir, UtStringArray* list);

private:
  typedef UtHashMap<UtString,CarbonCfgRTLPort*> RTLPortMap;
  typedef UtHashMap<UtString,CarbonCfgXtorInstance*> XtorMap;
  typedef UtHashMap<UtString,CarbonCfgESLPort*> ESLPortMap;

  RTLPortMap mRTLPortMap;
  XtorMap mXtorMap;
  ESLPortMap mESLPortMap;

  UtStringSet mESLPortNames;      // includes XtorInstance and ESLPort
  UtStringSet mSubCompNames;
  UtStringSet mMemNames;
  UtStringSet mRegNames;
  UtStringSet mGroupNames;
  UtStringSet mPStreamNames;

#ifdef CARBON_PV
  PVContext* mPVContext;
#endif

  typedef UtArray<CarbonCfg*> SubCompVec;
  typedef UtArray<CarbonCfgRegister*> RegisterVec;
  typedef UtHashMap<UtString, RegisterVec*> StringRegMap;
  typedef UtArray<CarbonCfgMemory*> MemoryVec;
  typedef UtArray<CarbonCfgRTLPort*> RTLPortVec;
  typedef UtArray<CarbonCfgXtorInstance*> XtorVec;
  typedef UtArray<CarbonCfgESLPort*> ESLPortVec;
  typedef UtArray<CarbonCfgGroup*> GroupVec;
  typedef UtArray<CarbonCfgPStream*> PStreamVec;
  typedef UtArray<CarbonCfgXtorParamInst*> ParamVec;

  CarbonCfgElementGenerationType mElementGenerationType;

  CarbonCfgCadi mCadi;

  RTLPortVec mRTLPortVec;
  XtorVec mXtorVec;
  ParamVec mParamVec;
  ESLPortVec mESLPortVec;
  GroupVec mGroupVec;
  PStreamVec mPStreamVec;
  
  CarbonCfgELFLoader mELFLoader;
  CarbonCfgProcInfo  mProcInfo;

  CarbonCfgCustomCodeContainer mCustomCodes;

  UtString mDescription;
  UtString mCxxFlags;
  UtString mLinkFlags;
  UtString mSourceFiles;
  UtString mIncludeFiles;

  UtString mCcfgFileName;
  UtString mCcfgFilePath;

  // Generated/Persistent Names
  UtString mPersistentCompName;
  UtString mComponentJournalXML;

  // Editable names
  UtString mLibName;
  UtString mCompName;
  UtString mCompDisplayName;
  UtString mTopModuleName;
  UtString mIODBFile;
  int      mLegacyMemories;
  RegisterVec mRegisterVec;
  MemoryVec mMemoryVec;
  SubCompVec mSubCompVec;
  UtString mRegisterBaseAddress;
  UtString mWaveFile;
  UtString mLoadfileExtension;
  CarbonCfgWaveType mWaveType;
  XtorLibs mXtorLibs;
  UInt32 mVersion;
  UInt32 mFeatureMask;          // mask of 1<<CarbonCfgFeature
  Mode mMode;

  void initHelper(Mode);


  typedef void (*DestroyHelperFn)(CarbonMsgCBDataID*);
    
  static void destroyHelper(CarbonMsgCBDataID* msg);

  CarbonMsgCBDataID* mMsgCallback;
  bool mReadXtorLibrary;

  //! If non NULL, this cfg is a sub component. We need this to return the DB.
  CarbonCfg* mParent;

  CarbonCfgTemplate mTemplateVariables;

  //! Storage for the type name (default is "Other")
  UtString mCompType;

  //! Storage for the version name (default is "1.0")
  UtString mCompVersion;

  //! Storage for the documentation file (default is "")
  UtString mCompDocFile;

  //! If set, this component requires the model kit library
  bool mRequiresMkLibrary;

  //! If set, this component uses a versioned model kit library
  bool mUseVersionedMkLibrary;

  //! Expression to access the SoCDPCTraceReporter instance
  /*!
  If this string is not empty, it contains the C++ expression for
  accessing the SoCDPCTraceReporter instance of the component, e.g.

    mCompHelper->getPCTraceReporter()

  This is used in the component's ObtainInterface implementation.
  */
  UtString mPCTraceAccessor;

  //! Generate a standalone component instead of a SoC Designer component.
  bool mIsStandAlone;

  //! Generate a full name for the sub component (using top component name with sub component name)
  /*! This is needed so that we can have multiple models in an SA environment */
  bool mIsSubComponentFullName;

  //! If set, the ccfg file is not up to date with memory
  /*! This is only set for CoWare today. It is not handled for all
   *  operations. It is a hack to fix a compatability issue where we
   *  used to requiere both ESL and transactor connections to RTL pins
   *  for the generator to work right. With the new GUI that is not
   *  longer necessary or possible.
   *
   *  see the CarbonCfg::readNoCheck for details.
   */
  bool mIsModified;

  //! The System C types
  UtStringArray mSystemCTypes;

  // The Script Wrapped CarbonDB object
  CfgScriptingCarbonDB* mScriptingDB;

  // Enable via setenv CARBON_CCFG_DEBUG
  bool mCcfgDebugOutput;

  //! If set, we use the legacy static scheduling
  int mUseStaticScheduling;
};

//! This is a top component derivation of a CarbonCfg
/*! We use CarbonCfg both to represent the top and sub component. But
 *  some things only belong in the top and vice-versa.
 *
 *  This break up of the structures helps enforce the fact that the DB
 *  can only be stored in the top component but can be retrieved from
 *  any component.
 *
 *  Note that this is just a start at trying to clean this up. More
 *  code should be moved from the CarbonCfg structure to here when we
Car *  have time (as needed).
 */
class CarbonCfgTop : public CarbonCfg
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonCfgTop();

  //! Constructor with a mode
  CarbonCfgTop(Mode mode);

  //! Destructor
  virtual ~CarbonCfgTop();

  //! Return true if toplevel
  bool isTopLevel() const { return true; }

  //! Put DB (transfers ownership of the DB
  virtual void putDB(CarbonDB*);

  //! Get the DB
  virtual CarbonDB* getDB(void) const;

private:
  //! The database which is now owned by this structure or NULL
  CarbonDB* mDB;
}; // class CarbonCfgTop : public CarbonCfg

//! This is a sub component derivation of a CarbonCfg
/*! We use CarbonCfg both to represent the top and sub component. But
 *  some things only belong in the top and vice-versa.
 *
 *  Note that this is just a start at trying to clean this up. More
 *  code should be moved from the CarbonCfg structure to here when we
 *  have time.
 */
class CarbonCfgSub : public CarbonCfg
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CarbonCfgSub(CarbonCfg* parent);

  //! Destructor
  virtual ~CarbonCfgSub();

  //! Return true if toplevel
  bool isTopLevel() const { return false; }

  //! This is illegal for sub components
  virtual void putDB(CarbonDB*);

  //! Get the DB from the parent
  virtual CarbonDB* getDB(void) const;

  //! Get the type name for this component
  /*!
  This is the parent's type if not set explicitly.
  */
  virtual const char* getCompType() const;

private:
  //! The parent pointer so we can find the DB
  CarbonCfg* mParent;
}; // class CarbonCfgSub : public CarbonCfg




// Carbon DB

//
// Class: CarbonDBNodeIter
//
// A CarbonDB iteration object which MUST be freed
// by calling <CarbonDB.freeNodeIter>
//
class ProtoCarbonDBNodeIter : public QObject, public QScriptable
{
  Q_OBJECT

public:
  ProtoCarbonDBNodeIter(QObject *parent = 0) : QObject(parent) {}
  CarbonDBNodeIter* pObj() const { return qscriptvalue_cast<CarbonDBNodeIter*>(thisObject()); }
};

//
// Class: CarbonDBNode
//
// A CarbonDB node object.
//
// See <CarbonDB> for API functions
//
class ProtoCarbonDBNode : public QObject, public QScriptable
{
  Q_OBJECT

public:
  ProtoCarbonDBNode(QObject *parent = 0) : QObject(parent) {}
  CarbonDBNode* pObj() const { return qscriptvalue_cast<CarbonDBNode*>(thisObject()); }
};


class CfgScriptingCarbonDB : public QObject, public QScriptable
{
  Q_OBJECT
 
  Q_PROPERTY(bool hasDatabase READ hasDatabase);
  Q_PROPERTY(QString databaseFileName READ getDatabaseFileName);

public:
  CfgScriptingCarbonDB(QObject *parent = 0, CarbonCfg* ccfg = 0, CarbonDB* db = 0);
  CarbonDB* getDB() { return mDB; }
#ifndef SWIG
  static void registerTypes(QScriptEngine*);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);
#endif

public slots:
#ifndef SWIG
  QString getDatabaseFileName() const { return mDBFileName; }
  bool hasDatabase() const { return mDB != NULL; }
#endif

// Method: findNode
//   Search for and returns the <CarbonDBNode> maching path
//
// Parameters: 
//  path - The fullpath String of the node being located.
//   
// Returns:
//   <CarbonDBNode>
//
  CarbonDBNode* findNode(const QString& path)
  {
    UtString name;
    name << path;
    return (CarbonDBNode*)carbonDBFindNode(mDB, name.c_str());
  }

// Method: loopPrimaryPorts
// Returns the <CarbonDBNodeIter> for all the
// primary ports for the design.
//
// Notes:
//   The returned <CarbonDBNodeIter> must be freed by calling
//   <CarbonDB.freeNodeIter>
//
// Returns:
//   <CarbonDBNodeIter>
//
  CarbonDBNodeIter* loopPrimaryPorts() { return carbonDBLoopPrimaryPorts(mDB); }
// Method: nodeGetFullName
//
// Parameters: 
//  node - A <CarbonDBNode> object.
//
// Returns the full path of the node passed in.
//
// Returns:
//   String full path name
//
// See Also:
//  <nodeGetLeafName>
//
  QString nodeGetFullName(CarbonDBNode* node) { return carbonDBNodeGetFullName(mDB, node); }
// Method: nodeGetLeafName
//
// Parameters: 
//  node - A <CarbonDBNode> object.
//
// Returns the leaf name of the node passed in
//
// Returns:
//   String leaf name
//
// See Also:
//  <nodeGetFullName>
//
  QString nodeGetLeafName(CarbonDBNode* node) { return carbonDBNodeGetLeafName(mDB, node); }

// Method: nodeIterNext
//  Returns the next node in the iteration object or null if at the end.
//
// Parameters: 
//  iter - A <CarbonDBNodeIter> iteration object.
//
// Returns:
//   <CarbonDBNode>
//
  CarbonDBNode* nodeIterNext(CarbonDBNodeIter* iter) { return (CarbonDBNode*)carbonDBNodeIterNext(iter); }

// Method: freeNodeIter
//   Frees an iteration object.
//
// Parameters: 
//  iter - A <CarbonDBNodeIter> object.
//
//
  void freeNodeIter(CarbonDBNodeIter* iter) { carbonDBFreeNodeIter(iter); }

// Method: loopChildren
//   Iterate the children of the iter object passed in.
//
// Parameters: 
//  node - A <CarbonDBNode> object.
//
// Notes:
//   The returned <CarbonDBNodeIter> must be freed by calling
//   <CarbonDB.freeNodeIter>
//
// Returns:
//   <CarbonDBNodeIter>
//
  CarbonDBNodeIter* loopChildren(CarbonDBNode* node) { return carbonDBLoopChildren(mDB, node); }
  
// Method: nodeGetParent
//   Returns the parent <CarbonDBNode> for the node passed in.
//
// Parameters: 
//  node - A <CarbonDBNode> object.
//   
// Returns:
//   <CarbonDBNode>
//
  CarbonDBNode* nodeGetParent(CarbonDBNode* node) { return (CarbonDBNode*)carbonDBNodeGetParent(mDB, node); }

// Method: findChild
//   Search for and returns the <CarbonDBNode> maching childName
//
// Parameters: 
//  parent - A <CarbonDBNode> object.
//  childName - The String of the child node being located.
//   
// Returns:
//   <CarbonDBNode>
//
  CarbonDBNode* findChild(CarbonDBNode* parent, const QString& childName)
  {
    UtString value;
    value << childName;
    return (CarbonDBNode*)carbonDBFindChild(mDB, parent, value.c_str());
  }

  QString getVersion() { return carbonDBGetVersion(); }
  QString getSoftwareVersion() { return carbonDBGetSoftwareVersion(mDB); }
  QString getIdString() { return carbonDBGetIdString(mDB); }
  QString getTopLevelModuleName() { return carbonDBGetTopLevelModuleName(mDB); }
  QString getInterfaceName() { return carbonDBGetInterfaceName(mDB); }
  QString getSystemCModuleName() { return carbonDBGetSystemCModuleName(mDB); }

  CarbonDBNodeIter* loopPrimaryInputs() { return carbonDBLoopPrimaryInputs(mDB); }
  CarbonDBNodeIter* loopPrimaryOutputs() { return carbonDBLoopPrimaryOutputs(mDB); }
  CarbonDBNodeIter* loopPrimaryBidis() { return carbonDBLoopPrimaryBidis(mDB); }
  CarbonDBNodeIter* loopPrimaryClks() { return carbonDBLoopPrimaryClks(mDB); }
  CarbonDBNodeIter* loopClkTree() { return carbonDBLoopClkTree(mDB); }
  CarbonDBNodeIter* loopDepositable() { return carbonDBLoopDepositable(mDB); }
  CarbonDBNodeIter* loopObservable() { return carbonDBLoopObservable(mDB); }
  CarbonDBNodeIter* loopScDepositable() { return carbonDBLoopScDepositable(mDB); }
  CarbonDBNodeIter* loopScObservable() { return carbonDBLoopScObservable(mDB); }
  CarbonDBNodeIter* loopLoopAsyncs() { return carbonDBLoopAsyncs(mDB); }
  CarbonDBNodeIter* loopAsyncOutputs() { return carbonDBLoopAsyncOutputs(mDB); }
  CarbonDBNodeIter* loopAsyncDeposits() { return carbonDBLoopAsyncDeposits(mDB); }
  CarbonDBNodeIter* loopAsyncPosResets() { return carbonDBLoopAsyncPosResets(mDB); }
  CarbonDBNodeIter* loopAsyncNegResets() { return carbonDBLoopAsyncNegResets(mDB); }
  CarbonDBNodeIter* loopPosedgeTriggers() { return carbonDBLoopPosedgeTriggers(mDB); }
  CarbonDBNodeIter* loopNegedgeTriggers() { return carbonDBLoopNegedgeTriggers(mDB); }
  CarbonDBNodeIter* loopBothEdgeTriggers() { return carbonDBLoopBothEdgeTriggers(mDB); }
  CarbonDBNodeIter* loopPosedgeTrigger() { return carbonDBLoopPosedgeTriggers(mDB); }

  QString nodeComponentName(CarbonDBNode* node) { return carbonDBComponentName(mDB, node); }
  QString nodeSourceLanguage(CarbonDBNode* node) { return carbonDBSourceLanguage(mDB, node); }
  QString nodeGetSourceFile(CarbonDBNode* node) { return carbonDBGetSourceFile(mDB, node); }

  int nodeGetWidth(CarbonDBNode* node) { return carbonDBGetWidth(mDB, node); }
  int nodeGetMSB(CarbonDBNode* node) { return carbonDBGetMSB(mDB, node); }
  int nodeGetLSB(CarbonDBNode* node) { return carbonDBGetLSB(mDB, node); }
  int nodeGetBitSize(CarbonDBNode* node) { return carbonDBGetBitSize(mDB, node); }
  int nodeGetSourceLine(CarbonDBNode* node) { return carbonDBGetSourceLine(mDB, node); }
  int nodeGetArrayLeftBound(CarbonDBNode* node) { return carbonDBGetArrayLeftBound(mDB, node); }
  int nodeGetArrayRightBound(CarbonDBNode* node) { return carbonDBGetArrayRightBound(mDB, node); }
  int nodeGetArrayDims(CarbonDBNode* node) { return carbonDBGetArrayDims(mDB, node); }
  
  bool nodeIsContainedByComposite(CarbonDBNode* node) { return carbonDBIsContainedByComposite(mDB, node); }
  bool nodeIsStruct(CarbonDBNode* node) { return carbonDBIsStruct(mDB, node); }
  bool nodeIsArray(CarbonDBNode* node) { return carbonDBIsArray(mDB, node); }
  bool nodeIsEnum(CarbonDBNode* node) { return carbonDBIsEnum(mDB, node); }
  bool nodeCanBeCarbonNet(CarbonDBNode* node) { return carbonDBCanBeCarbonNet(mDB, node); }
  bool nodeIsScalar(CarbonDBNode* node) { return carbonDBIsScalar(mDB, node); }
  bool nodeIsTristate(CarbonDBNode* node) { return carbonDBIsTristate(mDB, node); }
  bool nodeIsVector(CarbonDBNode* node) { return carbonDBIsVector(mDB, node); }
  bool nodeIsConstant(CarbonDBNode* node) { return carbonDBIsConstant(mDB, node); }
  bool nodeIs2DArray(CarbonDBNode* node) { return carbonDBIs2DArray(mDB, node); }
  bool nodeIsClkTree(CarbonDBNode* node) { return carbonDBIsClkTree(mDB, node); }
  bool nodeIsAsync(CarbonDBNode* node) { return carbonDBIsAsync(mDB, node); }
  bool nodeIsAsyncOutput(CarbonDBNode* node) { return carbonDBIsAsyncOutput(mDB, node); }
  bool nodeIsAsyncDeposit(CarbonDBNode* node) { return carbonDBIsAsyncDeposit(mDB, node); }
  bool nodeIsAsyncPosReset(CarbonDBNode* node) { return carbonDBIsAsyncPosReset(mDB, node); }
  bool nodeIsAsyncNegReset(CarbonDBNode* node) { return carbonDBIsAsyncNegReset(mDB, node); }
  
  bool nodeIsForcible(CarbonDBNode* node) { return carbonDBIsForcible(mDB, node); }
  bool nodeIsDepositable(CarbonDBNode* node) { return carbonDBIsDepositable(mDB, node); }

  bool nodeIsScDepositable(CarbonDBNode* node) { return carbonDBIsScDepositable(mDB, node); }

  bool nodeIsObservable(CarbonDBNode* node) { return carbonDBIsObservable(mDB, node); }
  bool nodeIsVisible(CarbonDBNode* node) { return carbonDBIsVisible(mDB, node); }
 
  bool nodeIsTied(CarbonDBNode* node) { return carbonDBIsTied(mDB, node); }

private:
  QString mDBFileName;
  CarbonDB* mDB;
  CarbonCfg* mCcfg;
};


#ifndef SWIG
Q_DECLARE_METATYPE(CarbonCfg*);
Q_DECLARE_METATYPE(CarbonCfgESLPort*);
Q_DECLARE_METATYPE(CarbonCfgXtorInstance*);
Q_DECLARE_METATYPE(CarbonCfgRTLPort*);
Q_DECLARE_METATYPE(CarbonCfgRTLConnection*);
Q_DECLARE_METATYPE(CarbonCfgTieParam*);
Q_DECLARE_METATYPE(CarbonCfgTie*);
Q_DECLARE_METATYPE(CarbonCfgResetGen*);
Q_DECLARE_METATYPE(CarbonCfgClockGen*);
Q_DECLARE_METATYPE(CarbonCfgXtorConn*);
Q_DECLARE_METATYPE(CarbonCfgXtor*);
Q_DECLARE_METATYPE(CarbonCfgXtorLib*);
Q_DECLARE_METATYPE(CarbonCfgXtorPort*);
Q_DECLARE_METATYPE(CarbonCfgXtorParam*);
Q_DECLARE_METATYPE(CarbonCfgXtorParamInst*);
Q_DECLARE_METATYPE(CarbonCfgRegister*);
Q_DECLARE_METATYPE(CarbonCfgGroup*);
Q_DECLARE_METATYPE(CarbonCfgRegisterField*);
Q_DECLARE_METATYPE(CarbonCfgRegisterLoc*);
Q_DECLARE_METATYPE(CarbonCfgRegisterLocConstant*);
Q_DECLARE_METATYPE(CarbonCfgRegisterLocUser*);
Q_DECLARE_METATYPE(CarbonCfgRegisterLocRTL*);
Q_DECLARE_METATYPE(CarbonCfgRegisterLocReg*);
Q_DECLARE_METATYPE(CarbonCfgRegisterLocArray*);
Q_DECLARE_METATYPE(CarbonCfgMemory*);
Q_DECLARE_METATYPE(CarbonCfgMemoryBlock*);
Q_DECLARE_METATYPE(CarbonCfgMemoryLoc*);
Q_DECLARE_METATYPE(CarbonCfgMemoryLocRTL*);
Q_DECLARE_METATYPE(CarbonCfgMemoryLocPort*);
Q_DECLARE_METATYPE(CarbonCfgMemoryLocUser*);
Q_DECLARE_METATYPE(CarbonCfgELFLoader*);
Q_DECLARE_METATYPE(CarbonCfgProcInfo*);
Q_DECLARE_METATYPE(CarbonCfgCustomCode*);
Q_DECLARE_METATYPE(CarbonCfgCompCustomCode*);
Q_DECLARE_METATYPE(CarbonCfgCadiCustomCode*);
Q_DECLARE_METATYPE(CarbonCfgRegCustomCode*);
Q_DECLARE_METATYPE(CarbonCfgMemoryBlockCustomCode*);
Q_DECLARE_METATYPE(CarbonCfgMemoryCustomCode*);
Q_DECLARE_METATYPE(CarbonCfgTemplate*);
Q_DECLARE_METATYPE(CarbonCfgCadi*);


// CarbonDB
Q_DECLARE_METATYPE(CfgScriptingCarbonDB*);
Q_DECLARE_METATYPE(CarbonDBNode*);
Q_DECLARE_METATYPE(CarbonDB*);
Q_DECLARE_METATYPE(CarbonDBNodeIter*);


// Enums
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgElementGenerationType);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRegisterLocKind);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRegAccessType);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRadix);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRTLPortType);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRTLConnectionType);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgStatus);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMode);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgParamDataType);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgParamFlag);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgESLPortType);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgESLPortMode);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMemInitType);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgReadmemType);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMemLocType);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgWaveType);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgCustomCodePosition);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgCompCustomCodeSection);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgCadiCustomCodeSection);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRegCustomCodeSection);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection);
Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMemoryCustomCodeSection);

Q_DECLARE_OPERATORS_FOR_FLAGS(CcfgFlags::CarbonCfgMemoryEditFlags);
Q_DECLARE_OPERATORS_FOR_FLAGS(CcfgFlags::CarbonCfgComponentEditFlags);

Q_DECLARE_METATYPE(CcfgFlags::CarbonCfgMemoryEditFlags);
Q_DECLARE_METATYPE(CcfgFlags::CarbonCfgComponentEditFlags);
#endif

#endif // _CARBON_CFG_CXX_H_
