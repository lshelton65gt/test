/*****************************************************************************

 Copyright (c) 2005-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBON_CFG_H__
#define __CARBON_CFG_H__

#ifndef __carbon_shelltypes_h_
#include "shell/carbon_shelltypes.h"
#endif

/*! carbon configuration handle */
typedef struct CarbonCfg* CarbonCfgID;

/*! Carbon CADI configuration handle */
typedef struct CarbonCfgCadi* CarbonCfgCadiID;

/*! carbon register handle */
typedef struct CarbonCfgRegister* CarbonCfgRegisterID;

/*! carbon register field handle */
typedef struct CarbonCfgRegisterField* CarbonCfgRegisterFieldID;
/*! carbon register base location handle */
typedef struct CarbonCfgRegisterLoc* CarbonCfgRegisterLocID;
/*! carbon register constant location handle */
typedef struct CarbonCfgRegisterLocConstant* CarbonCfgRegisterLocConstantID;
/*! carbon register (RTL register) location  handle */
typedef struct CarbonCfgRegisterLocReg* CarbonCfgRegisterLocRegID;
/*! carbon register (RTL array) location handle */
typedef struct CarbonCfgRegisterLocArray* CarbonCfgRegisterLocArrayID;
/*! carbon register (RTL user) location handle */
typedef struct CarbonCfgRegisterLocUser* CarbonCfgRegisterLocUserID;

/*! transactor port */
typedef struct CarbonCfgXtorPort      *CarbonCfgXtorPortID;
typedef struct CarbonCfgXtorTemplateArg *CarbonCfgXtorTemplateArgID;
typedef struct CarbonCfgXtorClassTemplateArg *CarbonCfgXtorClassTemplateArgID;
typedef struct CarbonCfgXtorPortWidthTemplateArg *CarbonCfgXtorPortWidthTemplateArgID;
typedef struct CarbonCfgXtorTextTemplateArg *CarbonCfgXtorTextTemplateArgID;
typedef struct CarbonCfgXtorSystemCPort *CarbonCfgXtorSystemCPortID;
typedef struct CarbonCfgXtorInstance  *CarbonCfgXtorInstanceID;
typedef struct CarbonCfgRTLPort       *CarbonCfgRTLPortID;
typedef struct CarbonCfgRTLConnection *CarbonCfgRTLConnectionID;
typedef struct CarbonCfgESLPort       *CarbonCfgESLPortID;
typedef struct CarbonCfgTie           *CarbonCfgTieID;
typedef struct CarbonCfgTieParam      *CarbonCfgTieParamID;
typedef struct CarbonCfgMemory        *CarbonCfgMemoryID;
typedef struct CarbonCfgCustomCode    *CarbonCfgCustomCodeID;
typedef struct CarbonCfgMemoryBlock   *CarbonCfgMemoryBlockID;
typedef struct CarbonCfgMemoryLoc     *CarbonCfgMemoryLocID;
typedef struct CarbonCfgMemoryLocRTL  *CarbonCfgMemoryLocRTLID;
typedef struct CarbonCfgMemoryLocPort *CarbonCfgMemoryLocPortID;
typedef struct CarbonCfgMemoryLocUser *CarbonCfgMemoryLocUserID;
typedef struct CarbonCfgClockGen      *CarbonCfgClockGenID;
typedef struct CarbonCfgResetGen      *CarbonCfgResetGenID;
typedef struct CarbonCfgXtor          *CarbonCfgXtorID;
typedef struct CarbonCfgXtorConn      *CarbonCfgXtorConnID;
typedef struct CarbonCfgSystemCClock  *CarbonCfgSystemCClockID;
typedef struct CarbonCfgELFLoader     *CarbonCfgELFLoaderID;
typedef struct CarbonCfgProcInfo      *CarbonCfgProcInfoID;
typedef struct CarbonCfgPStream       *CarbonCfgPStreamID;
typedef struct CarbonCfgPChannel      *CarbonCfgPChannelID;
typedef struct CarbonCfgPNet          *CarbonCfgPNetID;
typedef struct CarbonCfgPBucket       *CarbonCfgPBucketID;
typedef struct CarbonCfgPTrigger      *CarbonCfgPTriggerID;
typedef struct CarbonCfgPChannelDescriptor *CarbonCfgPChannelDescriptorID;
typedef struct CarbonCfgXtorLib      *CarbonCfgXtorLibID;
typedef struct CarbonCfgXtorParamInst *CarbonCfgXtorParamInstID;
typedef struct CarbonCfgXtorAbstraction *CarbonCfgXtorAbstractionID;

#include "carbon_cfg_enum.h"

#ifdef __cplusplus
extern "C" {
#endif

/*! create an empty cfg */
CarbonCfgID carbonCfgCreate();

/*! destroy a cfg */
void carbonCfgDestroy(CarbonCfgID);

/*! clear a cfg */
void carbonCfgClear(CarbonCfgID);

/*! load a config from a file, return 0 on success, 1 on failure */
/*! If an error occurs, get it with carbonCfgGetErrmsg(); */
CarbonCfgStatus carbonCfgRead(CarbonCfgID, const char* filename);

/*! write a config to a file, return 0 on success, 1 on failure */
/*! If an error occurs, get it with carbonCfgGetErrmsg(CarbonCfgID); */
CarbonCfgStatus carbonCfgWrite(CarbonCfgID, const char* filename);

/*! get the last error message, or NULL if there was never an error */
const char* carbonCfgGetErrmsg(CarbonCfgID);

/* get number of warnings in last check */
unsigned int carbonCfgGetNumWarnings(CarbonCfgID cfg);

/* get number of errors in last check */
unsigned int carbonCfgGetNumErrors(CarbonCfgID cfg);

/*! add a clock */
/*!
 *! Clocks are defined by the following parameters:
 *!
 *!  initialVal  -  start high (1) or low (0). 
 *!  delay       -  delay before first edge, as a percentge of one clock period
 *!  duty        -  percentage of one clock periiod that the clock value is 1
 *!
 *!  clockCycles/compCycles - 
 *!
 *!    The clock frequency is defined by a ratio of clock cycles to
 *!    component cycles.  If there are 3 clock cycles for each
 *!    component cycle, then clockCycles = 3, compCycles = 1.
*/
CarbonCfgRTLConnectionID carbonCfgClockGen(CarbonCfgRTLPortID,
                                           unsigned int initialVal,
                                           unsigned int delay,
                                           unsigned int clockCycles,
                                           unsigned int compCycles,
                                           unsigned int duty);

CarbonCfgRTLConnectionID carbonCfgTie(CarbonCfgRTLPortID,
                                      const unsigned int* vals,
                                      unsigned int numVals);

CarbonCfgRTLConnectionID carbonCfgTieParam(CarbonCfgRTLPortID,
                                           const char* xtorInstanceName,
                                           const char* defparam);

unsigned int carbonCfgNumRTLPorts(CarbonCfgID);
CarbonCfgRTLPortID carbonCfgGetRTLPort(CarbonCfgID, unsigned int);

CarbonCfgRTLPortID carbonCfgAddRTLPort(CarbonCfgID, const char* name,
                                       unsigned int width, CarbonCfgRTLPortType);

const char* carbonCfgRTLPortGetName(CarbonCfgRTLPortID rtlPort);
unsigned int carbonCfgRTLPortGetWidth(CarbonCfgRTLPortID rtlPort);
CarbonCfgRTLPortType carbonCfgRTLPortGetType(CarbonCfgRTLPortID rtlPort);
unsigned int carbonCfgRTLPortNumConnections(CarbonCfgRTLPortID rtlPort);
CarbonCfgRTLConnectionID carbonCfgRTLPortGetConnection(CarbonCfgRTLPortID rtlPort,
                                                       unsigned int i);
CarbonCfgESLPortID carbonCfgAddESLPort(CarbonCfgID, CarbonCfgRTLPortID,
                                       const char* name, CarbonCfgESLPortType);

unsigned int carbonCfgNumESLPorts(CarbonCfgID cfg);
CarbonCfgESLPortID carbonCfgGetESLPort(CarbonCfgID cfg, unsigned int i);
CarbonCfgESLPortID carbonCfgFindESLPort(CarbonCfgID cfg, const char* name);
unsigned int carbonCfgNumXtorInstances(CarbonCfgID cfg);
CarbonCfgXtorInstanceID carbonCfgGetXtorInstance(CarbonCfgID cfg, unsigned int i);
CarbonCfgXtorInstanceID carbonCfgFindXtorInstance(CarbonCfgID cfg,
                                                  const char* name);

unsigned int carbonCfgXtorInstanceNumConnections(CarbonCfgXtorInstanceID xi);
CarbonCfgXtorConnID carbonCfgXtorInstanceGetConnection(CarbonCfgXtorInstanceID xi, unsigned int i);

CarbonCfgXtorID carbonCfgXtorInstanceGetXtor(CarbonCfgXtorInstanceID xi);
const char* carbonCfgXtorInstanceGetName(CarbonCfgXtorInstanceID xi);
const char* carbonCfgXtorInstanceFindParameter(CarbonCfgXtorInstanceID xi,
                                               const char* paramName);
unsigned int carbonCfgXtorInstanceNumParams(CarbonCfgXtorInstanceID xi);
CarbonCfgXtorParamInstID carbonCfgXtorInstanceGetParam(CarbonCfgXtorInstanceID xi, unsigned int i);
unsigned int carbonCfgXtorInstanceUseDebugAccess(CarbonCfgXtorInstanceID xi);
unsigned int carbonCfgGetNumSystemCTypes(CarbonCfgID cfg);
const char* carbonCfgGetSystemCType(CarbonCfgID cfg, unsigned int);

unsigned int carbonCfgNumParams(CarbonCfgID cfg);
CarbonCfgXtorParamInstID carbonCfgGetParam(CarbonCfgID cfg, unsigned int i);
const char* carbonCfgParamGetName(CarbonCfgXtorParamInstID parm);
unsigned int carbonCfgParamGetSize(CarbonCfgXtorParamInstID parm);
const char* carbonCfgParamGetDefaultValue(CarbonCfgXtorParamInstID parm);
const char* carbonCfgParamGetValue(CarbonCfgXtorParamInstID parm);
const char* carbonCfgParamGetType(CarbonCfgXtorParamInstID parm);
const char* carbonCfgParamGetFlag(CarbonCfgXtorParamInstID parm);
const char* carbonCfgParamGetDescription(CarbonCfgXtorParamInstID parm);
const char* carbonCfgParamGetEnumChoices(CarbonCfgXtorParamInstID parm);
unsigned int carbonCfgParamNumEnumChoices(CarbonCfgXtorParamInstID param);
const char* carbonCfgParamGetEnumChoice(CarbonCfgXtorParamInstID param, unsigned int i);

CarbonCfgXtorInstanceID carbonCfgXtorInstanceGetClockMaster(CarbonCfgXtorInstanceID xi);
const char* carbonCfgXtorInstanceGetESLClockMaster(CarbonCfgXtorInstanceID xi);
const char* carbonCfgXtorInstanceGetESLResetMaster(CarbonCfgXtorInstanceID xi);
CarbonCfgXtorAbstractionID carbonCfgXtorInstanceGetAbstraction(CarbonCfgXtorInstanceID xi);

const char* carbonCfgXtorGetLibraryName(CarbonCfgXtorID xtor);
const char* carbonCfgXtorGetName(CarbonCfgXtorID xtor);
unsigned int carbonCfgXtorHasWriteDebug(CarbonCfgXtorID xtor); 
const char* carbonCfgXtorGetClassName(CarbonCfgXtorID xtor);
unsigned int carbonCfgXtorNumIncludeFiles(CarbonCfgXtorID xtor);
const char* carbonCfgXtorGetIncludeFile(CarbonCfgXtorID xtor, unsigned int index);
unsigned int carbonCfgXtorIsXtorClockMaster(CarbonCfgXtorID xtor);
unsigned int carbonCfgXtorIsFlowThru(CarbonCfgXtorID xtor);
unsigned int carbonCfgXtorIsSaveRestore(CarbonCfgXtorID xtor);
unsigned int carbonCfgXtorIsMaster(CarbonCfgXtorID xtor);
unsigned int carbonCfgXtorIsSlave(CarbonCfgXtorID xtor);
unsigned int carbonCfgXtorIsAmba(CarbonCfgXtorID xtor);
unsigned int carbonCfgXtorNumSystemCPorts(CarbonCfgXtorID xtor);
CarbonCfgXtorSystemCPortID carbonCfgXtorGetSystemCPort(CarbonCfgXtorID xtor, unsigned int i);
const char* carbonCfgXtorGetVariant(CarbonCfgXtorID xtor);
unsigned int carbonCfgXtorUseESLClockMaster(CarbonCfgXtorID xtor);
unsigned int carbonCfgXtorUseESLResetMaster(CarbonCfgXtorID xtor);

unsigned int carbonCfgXtorHasDebugTransaction(CarbonCfgXtorID xtor); 
const char* carbonCfgXtorGetProtocolName(CarbonCfgXtorID xtor); 
unsigned int carbonCfgXtorNumLibFiles(CarbonCfgXtorID xtor);
const char* carbonCfgXtorGetLibFile(CarbonCfgXtorID xtor, unsigned int index); 
unsigned int carbonCfgXtorNumIncPaths(CarbonCfgXtorID xtor);
const char* carbonCfgXtorGetIncPath(CarbonCfgXtorID xtor, unsigned int index); 
unsigned int carbonCfgXtorNumLibPaths(CarbonCfgXtorID xtor);
const char* carbonCfgXtorGetLibPath(CarbonCfgXtorID xtor, unsigned int index); 
unsigned int carbonCfgXtorNumFlags(CarbonCfgXtorID xtor);
const char* carbonCfgXtorGetFlag(CarbonCfgXtorID xtor, unsigned int index); 
const char* carbonCfgXtorGetConstructorArgs(CarbonCfgXtorID xtor); 
unsigned int carbonCfgXtorIsSDXactor(CarbonCfgXtorID xtor); 
unsigned int carbonCfgXtorUseEventQueue(CarbonCfgXtorID xtor); 
unsigned int carbonCfgXtorPreservePortNameCase(CarbonCfgXtorID xtor); 
unsigned int carbonCfgXtorUseCommunicate(CarbonCfgXtorID xtor); 
unsigned int carbonCfgXtorNumTemplateArgs(CarbonCfgXtorID xtor);
CarbonCfgXtorTemplateArgID carbonCfgXtorGetTemplateArg(CarbonCfgXtorID xtor, unsigned int index);
unsigned int carbonCfgXtorNumAbstractions(CarbonCfgXtorID xtor);
CarbonCfgXtorAbstractionID carbonCfgXtorGetAbstraction(CarbonCfgXtorID xtor, unsigned int index);

const char* carbonCfgXtorTemplateArgGetType(CarbonCfgXtorTemplateArgID arg);
const char* carbonCfgXtorTemplateArgGetValue(CarbonCfgXtorTemplateArgID arg);
const char* carbonCfgXtorTemplateArgGetDefault(CarbonCfgXtorTemplateArgID arg);
CarbonCfgXtorClassTemplateArgID carbonCfgXtorCastClassTemplateArg(CarbonCfgXtorTemplateArgID arg);
CarbonCfgXtorPortWidthTemplateArgID carbonCfgXtorCastPortWidthTemplateArg(CarbonCfgXtorTemplateArgID arg);

unsigned int carbonCfgXtorPortWidthTemplateArgGetBusDataWidth(CarbonCfgXtorPortWidthTemplateArgID arg);

const char* carbonCfgXtorAbstractionGetName(CarbonCfgXtorAbstractionID xtorAbstraction);
const char* carbonCfgXtorAbstractionGetClassName(CarbonCfgXtorAbstractionID xtorAbstraction);
unsigned int carbonCfgXtorAbstractionNumIncludeFiles(CarbonCfgXtorAbstractionID xtorAbstraction);
const char* carbonCfgXtorAbstractionGetIncludeFile(CarbonCfgXtorAbstractionID xtorAbstraction, unsigned int index);
unsigned int carbonCfgXtorAbstractionNumLibFiles(CarbonCfgXtorAbstractionID xtorAbstraction);
const char* carbonCfgXtorAbstractionGetLibFile(CarbonCfgXtorAbstractionID xtorAbstraction, unsigned int index); 
unsigned int carbonCfgXtorAbstractionNumIncPaths(CarbonCfgXtorAbstractionID xtorAbstraction);
const char* carbonCfgXtorAbstractionGetIncPath(CarbonCfgXtorAbstractionID xtorAbstraction, unsigned int index); 
unsigned int carbonCfgXtorAbstractionNumLibPaths(CarbonCfgXtorAbstractionID xtorAbstraction);
const char* carbonCfgXtorAbstractionGetLibPath(CarbonCfgXtorAbstractionID xtorAbstraction, unsigned int index); 
unsigned int carbonCfgXtorAbstractionNumFlags(CarbonCfgXtorAbstractionID xtorAbstraction);
const char* carbonCfgXtorAbstractionGetFlag(CarbonCfgXtorAbstractionID xtorAbstraction, unsigned int index); 
const char* carbonCfgXtorAbstractionGetLayerId(CarbonCfgXtorAbstractionID xtorAbstraction); 

/*
void carbonCfgAddRegister(CarbonCfgID cfg,
                          const char* signal, const char* debugName,
                          const char* groupName, unsigned int width,
                          CarbonCfgRadix radix, const char* comment);
void carbonCfgRemoveAllRegisterInfo(CarbonCfgID cfg);
*/

void carbonCfgAddMemory(CarbonCfgID cfg,
                        const char* signal, const char* name,
                        const char* initFile, unsigned long long maxAddrs,
                        unsigned int width,
                        CarbonCfgReadmemType rt,
                        const char* comment,
			const char* disassemblyName,
			const char* disassemblerName);
void carbonCfgRemoveAllMemoryInfo(CarbonCfgID cfg);
void carbonCfgRemoveAllCustomCode(CarbonCfgID cfg);

/*! add a transactor */
/*!
 *! This identifies a single port to represent an entire transactor
 *! interface to a Carbon model.  The individual Carbon model ports
 *! will have to be connected with carbonCfgConnectXtor
 */
CarbonCfgXtorInstanceID carbonCfgAddXtor(CarbonCfgID cfg,
                                         const char* xtorInstance,
                                         CarbonCfgXtorID xtorType);

/*! add a reset */
/*!
 *! add a reset signal, specifying initial value (asserted), plus minimum
 *! number of carbon beats that the signal must remain asserted
 */
CarbonCfgRTLConnectionID carbonCfgResetGen(CarbonCfgRTLPortID,
                                           CarbonUInt64 activeValue,
                                           CarbonUInt64 inactiveValue,
                                           unsigned int clockCycles,
                                           unsigned int compCycles,
                                           unsigned int cyclesBefore,
                                           unsigned int cyclesAsserted,
                                           unsigned int cyclesAfter);

#if 0
/*! add a transactor port */
/*!
 *! A transactor port connects a Maxsim port signal to a collection
 *! of RTL ports.  When a transactor port is added, the Maxsim port
 *! name is identified, and a handle is returned to connect to RTL
 */
CarbonCfgXtorPortID carbonCfgAddXtorPort(CarbonCfgID,
                                                    const char* maxsimSigName,
                                                    CarbonTransactorType);


/*! add an RTL connection to a transctor port */
void carbonCfgXtorAddConnection(CarbonCfgID,
                                           CarbonCfgXtorPortID,
                                           const char* xtorPortName,
                                           const char* rtlSignalName);
#endif                                           


/*CarbonCfgRTLConnectionType CarbonCfgRTLPortConnectionGetType(const );*/


unsigned int carbonCfgClockGetInitialVal(CarbonCfgClockGenID);
unsigned int carbonCfgClockGetClockCycles(CarbonCfgClockGenID);
unsigned int carbonCfgClockGetCompCycles(CarbonCfgClockGenID);
unsigned int carbonCfgClockGetDelay(CarbonCfgClockGenID clock);
unsigned int carbonCfgClockGetDutyCycle(CarbonCfgClockGenID clock);

double carbonCfgSystemCClockPeriod(CarbonCfgSystemCClockID scClock);
const char* carbonCfgSystemCClockPeriodUnits(CarbonCfgSystemCClockID scClock);
double carbonCfgSystemCClockDutyCycle(CarbonCfgSystemCClockID scClock);
unsigned int carbonCfgSystemCClockInitialValue(CarbonCfgSystemCClockID scClock);
double carbonCfgSystemCClockStartTime(CarbonCfgSystemCClockID scClock);
const char* carbonCfgSystemCClockStartTimeUnits(CarbonCfgSystemCClockID scClock);

CarbonUInt64 carbonCfgResetGetActiveValue(CarbonCfgResetGenID);
unsigned int carbonCfgResetGetActiveValueHigh(CarbonCfgResetGenID);
unsigned int carbonCfgResetGetActiveValueLow(CarbonCfgResetGenID);
CarbonUInt64 carbonCfgResetGetInactiveValue(CarbonCfgResetGenID);
unsigned int carbonCfgResetGetInactiveValueHigh(CarbonCfgResetGenID);
unsigned int carbonCfgResetGetInactiveValueLow(CarbonCfgResetGenID);
unsigned int carbonCfgResetGetClockCycles(CarbonCfgResetGenID);
unsigned int carbonCfgResetGetCompCycles(CarbonCfgResetGenID);
unsigned int carbonCfgResetGetCyclesBefore(CarbonCfgResetGenID);
unsigned int carbonCfgResetGetCyclesAsserted(CarbonCfgResetGenID);
unsigned int carbonCfgResetGetCyclesAfter(CarbonCfgResetGenID);


void carbonCfgPutDescription(CarbonCfgID, const char*);
const char* carbonCfgGetDescription(CarbonCfgID);
void carbonCfgPutCxxFlags(CarbonCfgID, const char*);
const char* carbonCfgGetCxxFlags(CarbonCfgID);
void carbonCfgPutLinkFlags(CarbonCfgID, const char*);
const char* carbonCfgGetLinkFlags(CarbonCfgID);
void carbonCfgPutSourceFiles(CarbonCfgID, const char*);
const char* carbonCfgGetSourceFiles(CarbonCfgID);
void carbonCfgPutIncludeFiles(CarbonCfgID, const char*);
const char* carbonCfgGetIncludeFiles(CarbonCfgID);
unsigned int carbonCfgIsStandAloneComp(CarbonCfgID cfg);
unsigned int carbonCfgIsSubComponentFullName(CarbonCfgID cfg);

void carbonCfgPutLibName(CarbonCfgID cfg, const char* str);
const char* carbonCfgGetLibName(CarbonCfgID cfg);
void carbonCfgPutCompName(CarbonCfgID cfg, const char* str);
void carbonCfgPutCompDisplayName(CarbonCfgID cfg, const char* str);
void carbonCfgPutLegacyMemories(CarbonCfgID cfg, int val);
int carbonCfgUseLegacyMemories(CarbonCfgID cfg);
const char* carbonCfgGetCompName(CarbonCfgID cfg);
const char* carbonCfgGetCompDisplayName(CarbonCfgID cfg);
void carbonCfgPutTopModuleName(CarbonCfgID cfg, const char* str);
const char* carbonCfgGetTopModuleName(CarbonCfgID cfg);
void carbonCfgPutIODBFile(CarbonCfgID cfg, const char* str);
const char* carbonCfgGetIODBFile(CarbonCfgID cfg);

unsigned int carbonCfgTieGetNumWords(CarbonCfgTieID);
unsigned int carbonCfgTieGetWord(CarbonCfgTieID, unsigned int index);

const char* carbonCfgTieParamGetFullParam(CarbonCfgTieParamID tie);
const char* carbonCfgTieParamGetParam(CarbonCfgTieParamID tie);
const char* carbonCfgTieParamGetXtorInstanceName(CarbonCfgTieParamID tie);

unsigned int carbonCfgNumSubComponents(CarbonCfgID cfg);
CarbonCfgID carbonCfgGetSubComponent(CarbonCfgID cfg, unsigned int);

unsigned int carbonCfgNumRegisters(CarbonCfgID cfg);
CarbonCfgRegisterID carbonCfgGetRegister(CarbonCfgID cfg, unsigned int);

unsigned int carbonCfgRegisterGetWidth(CarbonCfgRegisterID);
CarbonCfgRadix carbonCfgRegisterGetRadix(CarbonCfgRegisterID);
const char* carbonCfgRegisterGetPort(CarbonCfgRegisterID);
const char* carbonCfgRegisterGetComment(CarbonCfgRegisterID);
const char* carbonCfgRegisterGetGroup(CarbonCfgRegisterID);
void carbonCfgRegisterPutGroup(CarbonCfgID cfg, CarbonCfgRegisterID, const char* groupName);

unsigned int carbonCfgRegisterGetOffset(CarbonCfgRegisterID);
const char* carbonCfgRegisterGetName(CarbonCfgRegisterID);
unsigned int carbonCfgRegisterIsBigEndian(CarbonCfgRegisterID reg);
unsigned int carbonCfgRegisterIsPcReg(CarbonCfgRegisterID reg);
unsigned int carbonCfgRegisterNumFields(CarbonCfgRegisterID reg);
CarbonCfgRegisterFieldID carbonCfgRegisterGetField(CarbonCfgRegisterID reg, unsigned int index);
unsigned int carbonCfgRegisterNumCustomCodes(CarbonCfgRegisterID reg);
CarbonCfgCustomCodeID carbonCfgRegisterGetCustomCode(CarbonCfgRegisterID reg, unsigned int index);

  /* Register field functions */
unsigned int carbonCfgRegisterFieldGetHigh(CarbonCfgRegisterFieldID field);
unsigned int carbonCfgRegisterFieldGetLow(CarbonCfgRegisterFieldID field);
CarbonCfgRegAccessType carbonCfgRegisterFieldGetAccess(CarbonCfgRegisterFieldID field);
CarbonCfgRegisterLocID carbonCfgRegisterFieldGetLoc(CarbonCfgRegisterFieldID field);
  const char* carbonCfgRegisterFieldGetName(CarbonCfgRegisterFieldID field);

  /* Register location functions */
CarbonCfgRegLocType carbonCfgRegisterLocGetType(CarbonCfgRegisterLocID loc);
CarbonCfgRegisterLocConstantID carbonCfgRegisterLocCastConstant(CarbonCfgRegisterLocID loc);
CarbonCfgRegisterLocRegID carbonCfgRegisterLocCastReg(CarbonCfgRegisterLocID loc);
CarbonCfgRegisterLocArrayID carbonCfgRegisterLocCastArray(CarbonCfgRegisterLocID loc);
CarbonCfgRegisterLocUserID carbonCfgRegisterLocCastUser(CarbonCfgRegisterLocID loc);
unsigned long long carbonCfgRegisterLocConstantGetValue(CarbonCfgRegisterLocConstantID loc);
const char *carbonCfgRegisterLocRegGetPath(CarbonCfgRegisterLocRegID loc);
unsigned int carbonCfgRegisterLocRegGetHasRange(CarbonCfgRegisterLocRegID loc);
unsigned int carbonCfgRegisterLocRegGetLeft(CarbonCfgRegisterLocRegID loc);
unsigned int carbonCfgRegisterLocRegGetRight(CarbonCfgRegisterLocRegID loc);
unsigned int carbonCfgRegisterLocRegGetLSB(CarbonCfgRegisterLocRegID loc);
unsigned int carbonCfgRegisterLocRegGetMSB(CarbonCfgRegisterLocRegID loc);
const char *carbonCfgRegisterLocArrayGetPath(CarbonCfgRegisterLocArrayID loc);
unsigned int carbonCfgRegisterLocArrayGetHasRange(CarbonCfgRegisterLocArrayID loc);
unsigned int carbonCfgRegisterLocArrayGetLeft(CarbonCfgRegisterLocArrayID loc);
unsigned int carbonCfgRegisterLocArrayGetRight(CarbonCfgRegisterLocArrayID loc);
unsigned int carbonCfgRegisterLocArrayGetLSB(CarbonCfgRegisterLocArrayID loc);
unsigned int carbonCfgRegisterLocArrayGetMSB(CarbonCfgRegisterLocArrayID loc);
unsigned int carbonCfgRegisterLocArrayGetIndex(CarbonCfgRegisterLocArrayID loc);

unsigned int carbonCfgNumMemories(CarbonCfgID cfg);
CarbonCfgMemoryID carbonCfgGetMemory(CarbonCfgID cfg, unsigned int);

unsigned int carbonCfgNumCustomCodes(CarbonCfgID cfg);
CarbonCfgCustomCodeID carbonCfgGetCustomCode(CarbonCfgID cfg, unsigned int);

CarbonCfgCadiID carbonCfgGetCadi(CarbonCfgID cfg);
unsigned int carbonCfgCadiNumCustomCodes(CarbonCfgCadiID cadi);
CarbonCfgCustomCodeID carbonCfgCadiGetCustomCode(CarbonCfgCadiID cadi, unsigned int);
const char* carbonCfgCadiGetDisassemblerName(CarbonCfgCadiID cadi);

const char* carbonCfgMemoryGetSignal(CarbonCfgMemoryID);
const char* carbonCfgMemoryGetName(CarbonCfgMemoryID);
const char* carbonCfgMemoryGetInitFile(CarbonCfgMemoryID);
CarbonCfgReadmemType carbonCfgMemoryGetReadmemType(CarbonCfgMemoryID);
unsigned long long carbonCfgMemoryGetMaxAddrs(CarbonCfgMemoryID);
unsigned int carbonCfgMemoryGetWidth(CarbonCfgMemoryID);
unsigned int carbonCfgMemoryGetProgramMemory(CarbonCfgMemoryID);
const char* carbonCfgMemoryGetComment(CarbonCfgMemoryID);
CarbonCfgMemInitType carbonCfgMemoryGetInitType(CarbonCfgMemoryID);
unsigned long long carbonCfgMemoryGetBaseAddr(CarbonCfgMemoryID);
unsigned int carbonCfgMemoryGetBaseAddrHi(CarbonCfgMemoryID);
unsigned int carbonCfgMemoryGetBaseAddrLo(CarbonCfgMemoryID);
const char* carbonCfgMemoryGetProgPreloadEslPort(CarbonCfgMemoryID);
int carbonCfgMemoryGetDisplayAtZero(CarbonCfgMemoryID mem);
void carbonCfgMemoryPutDisplayAtZero(CarbonCfgMemoryID mem, int val);
int carbonCfgMemoryGetBigEndian(CarbonCfgMemoryID mem);
void carbonCfgMemoryPutBigEndian(CarbonCfgMemoryID mem, int val);

unsigned int carbonCfgMemoryNumSystemAddressESLPorts(CarbonCfgMemoryID mem);
const char* carbonCfgMemoryGetSystemAddressESLPortName(CarbonCfgMemoryID mem, unsigned int index);
long long carbonCfgMemoryGetSystemAddressESLPortBaseAddress(CarbonCfgMemoryID mem, unsigned int index);
void carbonCfgMemorySetSystemAddressESLPortName(CarbonCfgMemoryID mem, unsigned int index, const char* eslPortName);
void carbonCfgMemoryAddSystemAddressESLPort(CarbonCfgMemoryID mem, const char* eslPortName, long long baseAddress);
void carbonCfgMemoryRemoveAllSystemAddressESLPorts(CarbonCfgMemoryID mem);
void carbonCfgMemoryRemoveSystemAddressESLPort(CarbonCfgMemoryID mem, unsigned int index);
void carbonCfgMemorySetSystemAddressESLPortBaseAddress(CarbonCfgMemoryID mem, unsigned int index, long long baseAddress);

unsigned int carbonCfgMemoryNumBlocks(CarbonCfgMemoryID mem);
CarbonCfgMemoryBlockID carbonCfgMemoryGetBlock(CarbonCfgMemoryID mem, unsigned int index);

unsigned int carbonCfgMemoryNumCustomCodes(CarbonCfgMemoryID mem);
CarbonCfgCustomCodeID carbonCfgMemoryGetCustomCode(CarbonCfgMemoryID block, unsigned int index);

const char* carbonCfgMemoryBlockGetName(CarbonCfgMemoryBlockID block);
unsigned long long carbonCfgMemoryBlockGetBase(CarbonCfgMemoryBlockID block);
unsigned int carbonCfgMemoryBlockGetBaseLo(CarbonCfgMemoryBlockID block);
unsigned int carbonCfgMemoryBlockGetBaseHi(CarbonCfgMemoryBlockID block);
unsigned long long carbonCfgMemoryBlockGetSize(CarbonCfgMemoryBlockID block);
unsigned int carbonCfgMemoryBlockGetSizeLo(CarbonCfgMemoryBlockID block);
unsigned int carbonCfgMemoryBlockGetSizeHi(CarbonCfgMemoryBlockID block);
unsigned int carbonCfgMemoryBlockGetWidth(CarbonCfgMemoryBlockID block);
unsigned int carbonCfgMemoryBlockNumLocs(CarbonCfgMemoryBlockID block);
CarbonCfgMemoryLocID carbonCfgMemoryBlockGetLoc(CarbonCfgMemoryBlockID block, unsigned int index);
unsigned int carbonCfgMemoryBlockNumCustomCodes(CarbonCfgMemoryBlockID block);
CarbonCfgCustomCodeID carbonCfgMemoryBlockGetCustomCode(CarbonCfgMemoryBlockID block, unsigned int index);
CarbonCfgMemLocType carbonCfgMemoryLocGetType(CarbonCfgMemoryLocID loc);
CarbonCfgMemoryLocRTLID carbonCfgMemoryLocCastRTL(CarbonCfgMemoryLocID loc);
CarbonCfgMemoryLocPortID carbonCfgMemoryLocCastPort(CarbonCfgMemoryLocID loc);
CarbonCfgMemoryLocUserID carbonCfgMemoryLocCastUser(CarbonCfgMemoryLocID loc);
const char* carbonCfgMemoryLocRTLGetPath(CarbonCfgMemoryLocRTLID loc);
unsigned int carbonCfgMemoryLocRTLGetBitWidth(CarbonCfgMemoryLocRTLID loc);
unsigned long long carbonCfgMemoryLocGetDisplayStartWordOffset(CarbonCfgMemoryLocID loc);
void carbonCfgMemoryLocPutDisplayStartWordOffset(CarbonCfgMemoryLocID loc, unsigned long long val);
unsigned long long  carbonCfgMemoryLocGetDisplayEndWordOffset(CarbonCfgMemoryLocID loc);
void carbonCfgMemoryLocPutDisplayEndWordOffset(CarbonCfgMemoryLocID loc, unsigned long long val);
void carbonCfgMemoryLocPutDisplayMsb(CarbonCfgMemoryLocID loc, unsigned int val);
void carbonCfgMemoryLocPutDisplayLsb(CarbonCfgMemoryLocID loc, unsigned int val);
unsigned int carbonCfgMemoryLocGetDisplayLsb(CarbonCfgMemoryLocID loc);
unsigned int carbonCfgMemoryLocGetDisplayMsb(CarbonCfgMemoryLocID loc);

unsigned long long carbonCfgMemoryLocRTLGetStartWordOffset(CarbonCfgMemoryLocRTLID loc);
void carbonCfgMemoryLocRTLPutStartWordOffset(CarbonCfgMemoryLocRTLID loc, unsigned long long val);
unsigned long long carbonCfgMemoryLocRTLGetEndWordOffset(CarbonCfgMemoryLocRTLID loc);
void carbonCfgMemoryLocRTLPutEndWordOffset(CarbonCfgMemoryLocRTLID loc, unsigned long long val);
void carbonCfgMemoryLocRTLPutMsb(CarbonCfgMemoryLocRTLID loc, unsigned int val);
void carbonCfgMemoryLocRTLPutLsb(CarbonCfgMemoryLocRTLID loc, unsigned int val);
unsigned int carbonCfgMemoryLocRTLGetLsb(CarbonCfgMemoryLocRTLID loc);
unsigned int carbonCfgMemoryLocRTLGetMsb(CarbonCfgMemoryLocRTLID loc);

const char* carbonCfgMemoryLocPortGetPortName(CarbonCfgMemoryLocPortID loc);
unsigned int carbonCfgMemoryLocPortHasFixedAddr(CarbonCfgMemoryLocPortID loc);

CarbonCfgMode carbonCfgGetCcfgMode(CarbonCfgID cfg);
void carbonCfgPutCcfgMode(CarbonCfgID cfg, CarbonCfgMode);

const char* carbonCfgGetWaveFilename(CarbonCfgID cfg);
CarbonCfgWaveType carbonCfgGetWaveType(CarbonCfgID cfg);

void carbonCfgPutWaveFilename(CarbonCfgID cfg, const char*);
void carbonCfgPutWaveType(CarbonCfgID cfg, CarbonCfgWaveType);

const char* carbonCfgGetLoadfileExtension(CarbonCfgID cfg);
void carbonCfgPutLoadfileExtension(CarbonCfgID cfg, const char*);

unsigned int carbonCfgGetUseStaticScheduling(CarbonCfgID cfg);
void carbonCfgPutUseStaticScheduling(CarbonCfgID cfg, unsigned int);

CarbonCfgRTLConnectionType carbonCfgRTLConnectionGetType(CarbonCfgRTLConnectionID);

CarbonCfgClockGenID carbonCfgCastClockGen(CarbonCfgRTLConnectionID conn);
CarbonCfgResetGenID carbonCfgCastResetGen(CarbonCfgRTLConnectionID conn);
CarbonCfgTieID carbonCfgCastTie(CarbonCfgRTLConnectionID conn);
CarbonCfgTieParamID carbonCfgCastTieParam(CarbonCfgRTLConnectionID conn);
CarbonCfgESLPortID carbonCfgCastESLPort(CarbonCfgRTLConnectionID conn);
CarbonCfgXtorConnID carbonCfgCastXtorConn(CarbonCfgRTLConnectionID conn);
CarbonCfgSystemCClockID carbonCfgCastSystemCClock(CarbonCfgRTLConnectionID conn);

CarbonCfgXtorInstanceID carbonCfgXtorConnGetXtorInstance(CarbonCfgXtorConnID conn);
CarbonCfgXtorPortID carbonCfgXtorConnGetPort(CarbonCfgXtorConnID conn);
CarbonCfgRTLPortID carbonCfgXtorConnGetRTLPort(CarbonCfgXtorConnID conn);
unsigned int carbonCfgXtorConnGetPortIndex(CarbonCfgXtorConnID conn);
const char* carbonCfgXtorConnGetExpr(CarbonCfgXtorConnID conn);
const char* carbonCfgXtorConnGetTypeDef(CarbonCfgXtorConnID conn);
const char* carbonCfgXtorPortGetName(CarbonCfgXtorPortID port);
const char* carbonCfgXtorPortGetTypeDef(CarbonCfgXtorPortID port);
const char* carbonCfgXtorPortGetWidthExpr(CarbonCfgXtorPortID port);
const char* carbonCfgXtorPortGetDescription(CarbonCfgXtorPortID port);
CarbonCfgRTLPortType carbonCfgXtorPortGetType(CarbonCfgXtorPortID port);
unsigned int carbonCfgXtorPortGetSize(CarbonCfgXtorPortID port);

unsigned int carbonCfgXtorPortIsClockSensitive(CarbonCfgXtorPortID port); 
const char* carbonCfgXtorPortGetEventName(CarbonCfgXtorPortID port); 
unsigned int carbonCfgXtorPortNumPhaseNames(CarbonCfgXtorPortID port);
const char* carbonCfgXtorPortGetPhaseName(CarbonCfgXtorPortID port, unsigned int index);

const char* carbonCfgXtorSystemCPortGetName(CarbonCfgXtorSystemCPortID port);
CarbonCfgSystemCPortType carbonCfgXtorSystemCPortGetType(CarbonCfgXtorSystemCPortID port);
const char* carbonCfgXtorSystemCPortGetInterface(CarbonCfgXtorSystemCPortID port);
const char* carbonCfgXtorSystemCPortGetDescription(CarbonCfgXtorSystemCPortID port);

const char* carbonCfgESLPortGetName(CarbonCfgESLPortID eslPort);
const char* carbonCfgESLPortGetExpr(CarbonCfgESLPortID eslPort);
CarbonCfgESLPortType carbonCfgESLPortGetType(CarbonCfgESLPortID eslPort);
const char* carbonCfgESLPortGetTypeDef(CarbonCfgESLPortID eslPort);
void carbonCfgESLPortPutTypeDef(CarbonCfgESLPortID eslPort, const char* typeDef);
CarbonCfgESLPortMode carbonCfgESLPortGetMode(CarbonCfgESLPortID eslPort);
const char* carbonCfgESLPortGetParamName(CarbonCfgESLPortID eslPort);
const char* carbonCfgESLPortGetParamXtorInstName(CarbonCfgESLPortID eslPort);

CarbonCfgStatus carbonCfgReadXtorLib(CarbonCfgID cfg, CarbonXtorsType xtorTypes);

/*! load transactor definitions from a file */
/*! If an error occurs, get it with carbonCfgGetErrmsg(); */
CarbonCfgStatus carbonCfgReadXtorDefinitions(CarbonCfgID, const char* filename);

CarbonCfgXtorConnID carbonCfgConnectXtor(CarbonCfgRTLPortID,
                                         CarbonCfgXtorInstanceID,
                                         unsigned int portIndex);

CarbonCfgXtorID carbonCfgFindXtor(CarbonCfgID cfg, const char* name);
CarbonCfgXtorPortID carbonCfgXtorFindPort(CarbonCfgXtorID x,
                                          const char* pname);

CarbonCfgELFLoaderID carbonCfgGetELFLoader(CarbonCfgID cfg);
CarbonCfgProcInfoID carbonCfgGetProcInfo(CarbonCfgID cfg);
void carbonCfgELFLoaderAddSection(CarbonCfgELFLoaderID elf, const char* name, unsigned int space, const char* access);
unsigned int carbonCfgELFLoaderNumSections(CarbonCfgELFLoaderID elf);
const char* carbonCfgELFLoaderGetName(CarbonCfgELFLoaderID elf, unsigned int index);
unsigned int carbonCfgELFLoaderGetSpace(CarbonCfgELFLoaderID elf, unsigned int index);
const char* carbonCfgELFLoaderGetAccess(CarbonCfgELFLoaderID elf, unsigned int index);

int carbonCfgIsProcessor(CarbonCfgID cfg);
const char* carbonCfgProcInfoGetDebuggerName(CarbonCfgProcInfoID);
unsigned int carbonCfgProcInfoGetPipeStages(CarbonCfgProcInfoID);
unsigned int carbonCfgProcInfoGetHwThreads(CarbonCfgProcInfoID);
unsigned int carbonCfgProcInfoGetNumProcessorOptions(CarbonCfgProcInfoID);
const char* carbonCfgProcInfoGetProcessorOption(CarbonCfgProcInfoID, unsigned int);
const char* carbonCfgProcInfoGetPCRegGroupName(CarbonCfgProcInfoID);
const char* carbonCfgProcInfoGetPCRegName(CarbonCfgProcInfoID);
const char* carbonCfgProcInfoGetExtendedFeaturesRegGroupName(CarbonCfgProcInfoID);
const char* carbonCfgProcInfoGetExtendedFeaturesRegName(CarbonCfgProcInfoID);
unsigned int carbonCfgProcInfoGetDebuggablePoint(CarbonCfgProcInfoID);
const char* carbonCfgProcInfoGetTargetName(CarbonCfgProcInfoID);

const char* carbonCfgPBucketGetName(const CarbonCfgPBucketID pbucket);
unsigned int carbonCfgPBucketGetIndex(const CarbonCfgPBucketID pbucket);
const char* carbonCfgPBucketGetExpr(const CarbonCfgPBucketID pbucket);
CarbonCfgColor carbonCfgPBucketGetColor(CarbonCfgPBucketID pbucket);
const char* carbonCfgPBucketGetColorString(CarbonCfgPBucketID pbucket);
void carbonCfgPBucketPutExpr(CarbonCfgPBucketID pbucket, const char* expr);
void carbonCfgPBucketPutColor(CarbonCfgPBucketID pbucket,
                              CarbonCfgColor color);
const char* carbonCfgPNetGetName(const CarbonCfgPNetID pnet);
const char* carbonCfgPNetGetPath(const CarbonCfgPNetID pnet);
unsigned int carbonCfgPNetGetWidth(const CarbonCfgPNetID pnet);
const char* carbonCfgPChannelGetName(const CarbonCfgPChannelID pchannel);
const char* carbonCfgPChannelGetExpr(const CarbonCfgPChannelID pchannel);
unsigned int carbonCfgPChannelNumBuckets(const CarbonCfgPChannelID pchannel);
unsigned int carbonCfgPChannelNumTotalBuckets(const CarbonCfgPChannelID pchannel);
CarbonCfgPBucketID getBucket(const CarbonCfgPChannelID pchannel,
                             unsigned int i);
void carbonCfgPChannelPutExpr(CarbonCfgPChannelID pchannel,
                              const char* expr);
CarbonCfgPBucketID carbonCfgPChannelAddBucket(CarbonCfgPChannelID pchannel,
                                              const char* name);
void carbonCfgPChannelChangeBucketName(CarbonCfgPChannelID pchannel,
                                       CarbonCfgPBucketID pbucket,
                                       const char* name);
void carbonCfgPChannelRemoveBucket(CarbonCfgPChannelID pchannel,
                                   CarbonCfgPBucketID pbucket);

const char* carbonCfgPStreamGetName(const CarbonCfgPStreamID pstream);
unsigned int carbonCfgPStreamNumTriggers(const CarbonCfgPStreamID pstream);
CarbonCfgPTriggerID carbonCfgPStreamGetTrigger(const CarbonCfgPStreamID pstream,
                                               unsigned int idx);
const char* carbonCfgPTriggerGetExpr(const CarbonCfgPTriggerID ptrigger);
CarbonCfgPChannelID carbonCfgPTriggerGetChannel(const CarbonCfgPTriggerID ptrigger, unsigned int i);


unsigned int carbonCfgPStreamNumNets(const CarbonCfgPStreamID pstream);
CarbonCfgPNetID carbonCfgPStreamGetNet(const CarbonCfgPStreamID pstream,
                                       unsigned int i);
unsigned int carbonCfgPStreamNumChannels(const CarbonCfgPStreamID pstream);
void carbonCfgPTriggerPutExpr(CarbonCfgPTriggerID ptrigger, const char* expr);
void carbonCfgPStreamAddChannel(CarbonCfgPStreamID pstream,
                                const char* name);
void carbonCfgPStreamRemoveChannel(CarbonCfgPStreamID pstream,
                                   unsigned int idx);
void carbonCfgPStreamChangeChannelName(CarbonCfgPStreamID pstream,
                                       unsigned int idx,
                                       const char* name);
CarbonCfgPNetID carbonCfgPStreamAddNet(CarbonCfgPStreamID pstream,
                                       const char* path,
                                       const char* name,
                                       unsigned int width);
void carbonCfgPStreamRemoveNet(CarbonCfgPStreamID pstream,
                               CarbonCfgPNetID pnet);
void carbonCfgPStreamChangeNetName(CarbonCfgPStreamID pstream,
                                   CarbonCfgPNetID pnet,
                                   const char* name);
CarbonCfgPStreamID carbonCfgAddPStream(CarbonCfgID cfg, const char* name);
unsigned int carbonCfgNumPStreams(const CarbonCfgID cfg);
CarbonCfgPStreamID carbonCfgGetPStream(const CarbonCfgID cfg, unsigned int i);
void carbonCfgChangePStreamName(CarbonCfgID cfg, CarbonCfgPStreamID pstream,
                                const char* name);
void carbonCfgRemovePStream(CarbonCfgID cfg, CarbonCfgPStreamID pstream);

void carbonCfgPStreamComputeChannelDescriptors(CarbonCfgPStreamID pstream);
CarbonCfgPChannelDescriptorID
carbonCfgPStreamGetChannelDescriptor(CarbonCfgPStreamID pstream,
                                     unsigned int index);
const char* carbonCfgPChannelDescriptorGetName(CarbonCfgPChannelDescriptorID pcd);
unsigned int carbonCfgPChannelDescriptorNumBuckets(CarbonCfgPChannelDescriptorID pcd);
CarbonCfgPBucketID
carbonCfgPChannelDescriptorGetBucket(CarbonCfgPChannelDescriptorID pcd,
                                     unsigned int index);

// Custom code access functions
const char* carbonCfgCustomCodeGetSectionString(CarbonCfgCustomCodeID cc);
CarbonCfgCustomCodePosition carbonCfgCustomCodeGetPosition(CarbonCfgCustomCodeID cc);
const char* carbonCfgCustomCodeGetCode(CarbonCfgCustomCodeID cc);

// Type access for top component
void carbonCfgPutType(CarbonCfgID cfg, const char* name);
const char* carbonCfgGetType(CarbonCfgID cfg);

// Version access for top component
void carbonCfgPutVersion(CarbonCfgID cfg, const char* name);
const char* carbonCfgGetVersion(CarbonCfgID cfg);

// Documentation file access for top component
void carbonCfgPutDocFile(CarbonCfgID cfg, const char* name);
const char* carbonCfgGetDocFile(CarbonCfgID cfg);

// Flag if this component needs the model kit library
void carbonCfgPutRequiresMkLibrary(CarbonCfgID cfg, unsigned int req);
unsigned int carbonCfgGetRequiresMkLibrary(CarbonCfgID cfg);
void carbonCfgPutUseVersionedMkLibrary(CarbonCfgID cfg, unsigned int req);
unsigned int carbonCfgGetUseVersionedMkLibrary(CarbonCfgID cfg);

// SoCDPCTraceReporter accessor expression
void carbonCfgPutPCTraceAccessor(CarbonCfgID cfg, const char* accessor);
const char* carbonCfgGetPCTraceAccessor(CarbonCfgID cfg);

const char* carbonCfgGetComponentTag(CarbonCfgID cfg);

#ifdef __cplusplus
}
#endif

#endif /* __CARBON_CFG_H__ */
