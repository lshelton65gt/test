// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CARBON_CFG_MISC_H_
#define __CARBON_CFG_MISC_H_

#ifdef __cplusplus
extern "C" {
#endif

  /*! 
    Open a configuration file based on mode. The mode is casted to an
    enum defined in CarbonCfg.h. The mode determines which license to
    checkout.
  */
  CarbonCfgID carbonCfgModeCreate(int mode);
  
#ifdef __cplusplus
}
#endif

#endif
