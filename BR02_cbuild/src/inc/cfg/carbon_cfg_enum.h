#ifndef __CARBONCFG_ENUM_H__
#define __CARBONCFG_ENUM_H__

/*! 
  \brief What are the RTL port directions?  These are also used for
  xtor ports
*/
typedef enum {
  eCarbonCfgRTLInput,
  eCarbonCfgRTLOutput,
  eCarbonCfgRTLInout
} CarbonCfgRTLPortType;

typedef enum {
  eCarbonCfgScPort,
  eCarbonCfgScExport,
  eCarbonCfgTlm2Socket,
} CarbonCfgSystemCPortType;

typedef enum {
  eCarbonCfgClockGen,
  eCarbonCfgResetGen,
  eCarbonCfgTie,
  eCarbonCfgTieParam,
  eCarbonCfgESLPort,
  eCarbonCfgXtorConn,
  eCarbonCfgSystemCClock
} CarbonCfgRTLConnectionType;

/*! 
  \brief What are the ESL port directions?
*/
typedef enum {
  eCarbonCfgESLUndefined,
  eCarbonCfgESLInput,
  eCarbonCfgESLOutput,
  eCarbonCfgESLInout
} CarbonCfgESLPortType;

typedef enum {
  eCarbonCfgESLPortUndefined,
  eCarbonCfgESLPortControl,
  eCarbonCfgESLPortClock,
  eCarbonCfgESLPortReset
} CarbonCfgESLPortMode;

typedef enum {
  eCarbonCfgNone,
  eCarbonCfgVCD,
  eCarbonCfgFSDB
} CarbonCfgWaveType;

typedef enum {
  eCarbonCfgUNDEFINED,
  eCarbonCfgARM,
  eCarbonCfgCOWARE,
  eCarbonCfgSYSTEMC
} CarbonCfgMode;

/*! 
  \brief status 
*/
typedef enum {
  eCarbonCfgSuccess,
  eCarbonCfgFailure,
  eCarbonCfgInconsistent
} CarbonCfgStatus;

typedef enum {
  eCarbonXtorsMaxsim,
  eCarbonXtorsCoware,
  eCarbonXtorsSystemC
} CarbonXtorsType;

typedef enum {
  eCarbonCfgRadixBooleanActiveHigh,
  eCarbonCfgRadixBooleanActiveLow,
  eCarbonCfgRadixHex,
  eCarbonCfgRadixSignedInt,
  eCarbonCfgRadixUnsignedInt,
  eCarbonCfgRadixFloat,
  eCarbonCfgRadixString
} CarbonCfgRadix;

/*! 
  \brief Enum used to describe data types for parameters
*/
typedef enum {
  eCarbonCfgXtorBool,
  eCarbonCfgXtorUInt32,
  eCarbonCfgXtorDouble,
  eCarbonCfgXtorString,
  eCarbonCfgXtorUInt64
} CarbonCfgParamDataType;

/*! 
  \brief Enum to indicate whether a parameter is settable at runtime,
  compile-time or both.  
*/
typedef enum {
  eCarbonCfgXtorParamRuntime,
  eCarbonCfgXtorParamCompile,
  eCarbonCfgXtorParamBoth,
  eCarbonCfgXtorParamInit,
} CarbonCfgParamFlag;

/*! 
  \brief Enum for maxsim-colors for profiling
*/
typedef enum {
  eCarbonCfgRed,
  eCarbonCfgBlue,
  eCarbonCfgGreen,
  eCarbonCfgYellow,
  eCarbonCfgOrange,
  eCarbonCfgBrown,
  eCarbonCfgPurple,
  eCarbonCfgLightGray,
  eCarbonCfgLightBlue,
  eCarbonCfgLightCyan,
  eCarbonCfgLightSeaGreen,
  eCarbonCfgLightCoral,
  eCarbonCfgLightYellow1,
  eCarbonCfgLightSalmon1,
  eCarbonCfgLightGreen
} CarbonCfgColor;

/*! 
  \brief Enum for memory init file type
*/
typedef enum {
  eCarbonCfgReadmemh,
  eCarbonCfgReadmemb
} CarbonCfgReadmemType;

/*! 
  \brief Enum for memory init type
*/
typedef enum { 
  eCarbonCfgMemInitNone,        /*!< No initialization */
  eCarbonCfgMemInitProgPreload, /*!< byte-code init */
  eCarbonCfgMemInitReadmem      /*!< readmem init */
} CarbonCfgMemInitType;

typedef enum {
  eCfgFeaturePortExpr,        /*!< Port expressions */
  eCfgFeatureMemInitEnum,     /*!< Memory init enumeration */
  eCfgFeatureXtorConnExpr,    /*!< Transactor Connection expressions */
  eCfgFeatureXtorClockMaster, /*!< Transactor clocked by another xtor or ESL port */
  eCfgFeatureNoTicks,         /*!< Transactor Connection expressions */
  eCfgFeatureAbstractRegs,    /*!< Abstract registers */
  eCfgFeatureEslPortModes,    /*!< ESL Port Modes */
  eCfgFeatureDisassembly,     /*!< Disassembly */
  eCfgFeatureResetValues,     /*!< Arbitrary Reset Values */
  eCfgFeatureEslPortParameter,/*!< Parameter hanging off of port */
  eCfgFeatureXtorDebugAccess, /*!< Use debug access methods */
  eCfgFeatureXtorFullName,    /*!< File support xtor library/variant name */
  eCfgFeatureXtorTLM2Support,  //!< Support for TLM2 information
  eCfgFeatureEmbeddedModelSupport  //!< Support for Embedded Models
} CarbonCfgFeature;

/*! 
  \brief Enum for debug register access types
*/
typedef enum {
  eCfgRegAccessRW,        /*!< Read/write */
  eCfgRegAccessRO,        /*!< Read-only */
  eCfgRegAccessWO         /*!< Write-only */
} CarbonCfgRegAccessType;

/*! 
  \brief Enum for debug register field location types
*/
typedef enum {
  eCfgRegLocConstant,        /*!< Constant value */
  eCfgRegLocRegister,        /*!< RTL register */
  eCfgRegLocArray,           /*!< RTL array */
  eCfgRegLocUser             /*!< User defined contents */
} CarbonCfgRegLocType;

/*! 
  \brief Enum for debug memory location types
*/
typedef enum {
  eCfgMemLocRTL,        /*!< RTL memory */
  eCfgMemLocPort,       /*!< External memory */
  eCfgMemLocUser        /*!< User defined memory */
} CarbonCfgMemLocType;

typedef enum {
  eCarbonCfgLittleEndian,
  eCarbonCfgBigEndian,
} CarbonCfgEndianess;

/*!
  \brief Enum for custom code positions
*/
typedef enum {
  eCarbonCfgPre,
  eCarbonCfgPost
} CarbonCfgCustomCodePosition;

/*!
  \brief Enum for Component custom code sections

   if you make a change here you must also change gCarbonCfgCompCustomCodeSection (in src/cfg/carbon_cfg.cxx)
   and the definition of enum CarbonCfgCompCustomCodeSection (in src/cmm/SrcCcfg.h)
*/
typedef enum {
  eCarbonCfgCompCppInclude,
  eCarbonCfgCompHInclude,
  eCarbonCfgCompClass,
  eCarbonCfgCompConstructor,
  eCarbonCfgCompDestructor,
  eCarbonCfgCompCommunicate,
  eCarbonCfgCompInterconnect,
  eCarbonCfgCompUpdate,
  eCarbonCfgCompInit,
  eCarbonCfgCompReset,
  eCarbonCfgCompTerminate,
  eCarbonCfgCompSetParameter,
  eCarbonCfgCompGetParameter,
  eCarbonCfgCompGetProperty,
  eCarbonCfgCompDebugTransaction,
  eCarbonCfgCompDebugAccess,
  eCarbonCfgCompProcCanStop,
  eCarbonCfgCompProcStopAtDebuggablePoint,
  eCarbonCfgCompSaveData,
  eCarbonCfgCompRestoreData,
  eCarbonCfgCompGetCB,
  eCarbonCfgCompSaveStreamType,
  eCarbonCfgCompSaveDataArch,
  eCarbonCfgCompRestoreDataArch,
  eCarbonCfgCompSaveDataBinary,
  eCarbonCfgCompRestoreDataBinary,

  // These sections are for stand-alone components.
  eCarbonCfgCompLoadFile,
  eCarbonCfgCompStopAtDebuggablePoint,
  eCarbonCfgCompCanStop,
  eCarbonCfgCompRegisterDebugAccessCB,
  eCarbonCfgCompRetrieveDebugAccessIF,
  eCarbonCfgCompDebugMemRead,
  eCarbonCfgCompDebugMemWrite
} CarbonCfgCompCustomCodeSection;

/*!
 \brief Enum for Component Cadi custom code sections
*/
typedef enum {
  eCarbonCfgCadiCppInclude,
  eCarbonCfgCadiHInclude,
  eCarbonCfgCadiClass,
  eCarbonCfgCadiConstructor,
  eCarbonCfgCadiDestructor,
  eCarbonCfgCadiCADIRegGetGroups,
  eCarbonCfgCadiCADIRegGetMap,
  eCarbonCfgCadiCADIRegRead,
  eCarbonCfgCadiCADIRegWrite,
  eCarbonCfgCadiCADIGetPC,
  eCarbonCfgCadiCADIGetDisassembler,
  eCarbonCfgCadiCADIXfaceBypass,
  eCarbonCfgCadiCADIMemGetSpaces,
  eCarbonCfgCadiCADIMemGetBlocks,
  eCarbonCfgCadiCADIMemWrite,
  eCarbonCfgCadiCADIMemRead,
  eCarbonCfgCadiCADIGetCommitedPCs
} CarbonCfgCadiCustomCodeSection;

/*!
  \brief Enum for Register custom code sections
*/
typedef enum {
  eCarbonCfgRegClass,
  eCarbonCfgRegConstructor,
  eCarbonCfgRegDestructor,
  eCarbonCfgRegRead,
  eCarbonCfgRegWrite,
  eCarbonCfgRegReset
} CarbonCfgRegCustomCodeSection;

/*!
  \brief Enum for Memory Block custom code sections
*/
typedef enum {
  eCarbonCfgMemoryBlockClass,
  eCarbonCfgMemoryBlockConstructor,
  eCarbonCfgMemoryBlockDestructor,
  eCarbonCfgMemoryBlockRead,
  eCarbonCfgMemoryBlockWrite,
} CarbonCfgMemoryBlockCustomCodeSection;

/*!
  \brief Enum for Memory Block custom code sections
*/
typedef enum {
  eCarbonCfgMemoryClass,
  eCarbonCfgMemoryConstructor,
  eCarbonCfgMemoryDestructor,
  eCarbonCfgMemoryRead,
  eCarbonCfgMemoryWrite,
} CarbonCfgMemoryCustomCodeSection;

/*!
  \brief Enum for the SystemC time units
*/
typedef enum {
  eCarbonCfgScFS,
  eCarbonCfgScPS,
  eCarbonCfgScNS,
  eCarbonCfgScUS,
  eCarbonCfgScMS,
  eCarbonCfgScSec
} CarbonCfgSystemCTimeUnits;

/*! 
  \brief Enum used to describe data types for Elements
*/
typedef enum {
  eCarbonCfgElementUser,
  eCarbonCfgElementGenerated,
} CarbonCfgElementGenerationType;

#endif
