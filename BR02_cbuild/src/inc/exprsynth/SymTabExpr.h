// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __SymTabExpr_h_
#define __SymTabExpr_h_

/*!
 * \file
 * Interface for CarbonIdent subclass using SymbolTable nodes as identifiers.
 */

#include "exprsynth/Expr.h"
#include <cstddef>
#include "symtab/STSymbolTable.h"

class STAliasedLeafNode;
class SymTabIdentBP;
class ShellSymNodeIdent;
class ShellSymNodeIdentBP;

//! SymTabIdent class
/*!
 * This class represents a leaf node in all expressions. Identifiers
 * are symbol table nodes.
 */
class SymTabIdent : public CarbonIdent
{
#if !pfGCC_2
  using CarbonIdent::getNode;
#endif

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  SymTabIdent(STAliasedLeafNode* name, UInt32 bitSize);

  //! Constructor with usage mask
  SymTabIdent(STAliasedLeafNode * node, const DynBitVector& usageMask, UInt32 bitSize);

  //! destructor
  virtual ~SymTabIdent();

  //! Return class name
  const char* typeStr() const;

  SignT evaluate(ExprEvalContext*) const;

  AssignStat assign(ExprAssignContext* context);

  //! Compare function so that we can do caching
  /*! Note that this does an unsized comparison. It compares based on
   *  the actual size, not the expr size.
   */
  ptrdiff_t compare(const CarbonExpr* other) const;

  //! hash method
  size_t hash() const;

  const STAliasedLeafNode* getNode(DynBitVector* usageMask) const;

  const STAliasedLeafNode* getNode() const;

  STAliasedLeafNode* getNode() {
    const SymTabIdent* me = const_cast<const SymTabIdent*>(this);
    return const_cast<STAliasedLeafNode*>(me->getNode());
  }

  //! Dump myself to cout
  void print(bool recurse = true, int indent = 0) const;

  AssignStat assignRange(ExprAssignContext* context, 
                         const ConstantRange& range);

  //! Returns true if this ident represents the entire net
  virtual bool isWholeIdentifier() const;

  virtual const SymTabIdent* castSymTabIdent() const;

  virtual const SymTabIdentBP* castSymTabIdentBP() const;

  SymTabIdentBP* castSymTabIdentBP() {
    const SymTabIdent* me = const_cast<const SymTabIdent*>(this);
    return const_cast<SymTabIdentBP*>(me->castSymTabIdentBP());
  }

  virtual const ShellSymNodeIdent* castShellSymNodeIdent() const;

  ShellSymNodeIdent* castShellSymNodeIdent() {
    const SymTabIdent* me = const_cast<const SymTabIdent*>(this);
    return const_cast<ShellSymNodeIdent*>(me->castShellSymNodeIdent());
  }

  virtual const ShellSymNodeIdentBP* castShellSymNodeIdentBP() const;

  ShellSymNodeIdentBP* castShellSymNodeIdentBP() {
    const SymTabIdent* me = const_cast<const SymTabIdent*>(this);
    return const_cast<ShellSymNodeIdentBP*>(me->castShellSymNodeIdentBP());
  }
  
  //! compose based on given mode
  virtual void composeIdent(ComposeContext* context) const;

protected:
  //! The name of our identifier.
  STAliasedLeafNode * mNode;

  //! The usage mask of this identifier.
  DynBitVector* mUsageMask;
};

class SymTabIdentBP : public SymTabIdent
{
public:
  CARBONMEM_OVERRIDES
  
  //! constructor
  SymTabIdentBP(STAliasedLeafNode* name, UInt32 bitSize, CarbonExpr* origNetExpr);

  //! Constructor with usage mask
  SymTabIdentBP(STAliasedLeafNode * node, const DynBitVector& usageMask, UInt32 bitSize, CarbonExpr* origNetExpr);
  
  
  //! destructor
  virtual ~SymTabIdentBP();

  //! Return class name
  const char* typeStr() const;

  //! Compare function so that we can do caching
  /*! Note that this does an unsized comparison. It compares based on
   *  the actual size, not the expr size.
   */
  ptrdiff_t compare(const CarbonExpr* other) const;

  //! hash method
  size_t hash() const;

  //! Dump myself to cout
  void print(bool recurse = true, int indent = 0) const;

  //! Get the back pointer
  CarbonExpr* getBackPointer() const { return mBackPointer; }

  //! Return this
  virtual const SymTabIdentBP* castSymTabIdentBP() const;

  //! Sets up the back pointer for database writing/reading
  virtual void  nestedDBSetup(ESFactory* factory, 
                              ExprDBContext* dbContext, bool inclusiveNodes,
                              STSymbolTable::LeafAssoc *);

  virtual CbuildShellDB::CarbonIdentType addNestedToDB(CarbonExprVector* exprVec) const;

  //! compose based on given mode
  virtual void composeIdent(ComposeContext* context) const;

private:
  CarbonExpr* mBackPointer;
};

#endif // __SymTabExpr_h_
