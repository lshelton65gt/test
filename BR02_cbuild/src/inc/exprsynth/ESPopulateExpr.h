// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _ESPOPULATEEXPR_H_
#define _ESPOPULATEEXPR_H_

#include "flow/Flow.h"
#include "flow/FLNodeElab.h"
#if !pfGCC_4_2
#include "flow/FlowClassifier.h"
#endif
#include "exprsynth/Expr.h"
#include "nucleus/NUNetRefMap.h"
#include <cstddef>

class CarbonExpr;
class FLNode;
class STBranchNode;
class FLNodeElab;
class ESFactory;
class ESPopulateExpr;
class TranslateExpr;

#if pfGCC_4_2
template <typename T> class FLCaseClassifier; // forward declared
#endif

//! A handle class to work with expressions
class ESExprHndl
{
public: CARBONMEM_OVERRIDES
  //! constructor
  ESExprHndl(CarbonExpr* expr);

  //! Empty (invalid) expression handle constructor
  ESExprHndl() { mExpr = NULL; }

  //! destructor
  ~ESExprHndl();

  //! copy constructor
  ESExprHndl(const ESExprHndl& exprHndl);

  //! assign operator
  ESExprHndl& operator=(const ESExprHndl& exprHndl);

  //! equality operator
  bool operator==(const ESExprHndl& exprHndl) const
  {
    return mExpr == exprHndl.mExpr;
  }

  //! inequality operator
  bool operator!=(const ESExprHndl& exprHndl) const
  {
    return mExpr != exprHndl.mExpr;
  }

  //! less-than operator
  bool operator<(const ESExprHndl& exprHndl) const
  {
    return mExpr < exprHndl.mExpr;
  }

  //! Check if the constant is a simple 0
  bool isZero (void) const { return mExpr->isZero(); }

  //! Check if the constant is non-zero
  bool isNonZero () const { return mExpr->isNonZero(); }

  //! Return if the expression has more than N operators
  bool numOpsGreaterThan(int size);

  //! Print the expression
  void print(bool recurse = true, int indent = 0) const;

  //! Print the expression in Verilog syntax
  void printVerilog(int indent = 0) const;

  //! Return the underlying expression.
  /*! Note that reference counting is not updated. It is up to the
   *  caller to maintain the right reference count if you use this
   *  function.
   */
  CarbonExpr* getExpr() const { return mExpr; }

  //! hash function for UtHashSet and UtHashMap
  size_t hash() const
  {
    return ((size_t) mExpr) >> 2;
  }

private:
  CarbonExpr* mExpr;
}; // class ESExprHndl

//! An expression derived class for FLNode* leaf nodes
class ESFlowIdent : public CarbonIdent
{
#if !pfGCC_2
  using CarbonIdent::getNode;
#endif

public: CARBONMEM_OVERRIDES
  //! constructor
  ESFlowIdent(const FLNodeVector& flows, const NUNetRefHdl& netRef,
              UInt32 bitSize, bool isSigned);

  //! destructor
  ~ESFlowIdent();

  //! Get the flow node for this identifier
  virtual FLNode* getFlow() const;

  //! Returns a loop over the flows for this ident
  FLNodeVectorLoop loopFlows();

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! Compare function so that we can do caching
  /*! This function will make sure the expression types are the same
   *  (It does not compare the bit sizes). If that is the case then it
   *  calls the approriate leaf class compare function after doing a
   *  dynamic cast.
   */
  ptrdiff_t compare(const CarbonExpr* expr) const;

  //! Return the size this expression is "naturally".
  virtual UInt32 determineBitSize() const;

  //! Dump myself to cout
  virtual void print(bool recurse = true, int indent = 0) const;

  //! Return class name
  virtual const char* typeStr() const { return "FlowIdent"; }

  //! print verilog-syntax
  virtual void composeIdent(ComposeContext* context) const;

  const STAliasedLeafNode* getNode(DynBitVector* usageMask) const;

  //! returns CarbonExpr::eBadSign indicating that nothing was done
  virtual SignT evaluate(ExprEvalContext*) const;

  //! Asserts if called
  virtual AssignStat assign(ExprAssignContext*);
  //! Asserts if called
  virtual AssignStat assignRange(ExprAssignContext*, const ConstantRange&);

  //! Return the number of flows in this ident
  virtual int numFlows() const;

  //! Get the net ref that this ident represents
  const NUNetRefHdl& getNetRef() const;

  //! Returns true if this ident represents the entire net
  virtual bool isWholeIdentifier() const;

  //! Cast Expr to an ESFlowIdent
  const ESFlowIdent* castESFlowIdent() const;

private:
  //! Hide copy and assign constructors.
  ESFlowIdent(const ESFlowIdent&);
  ESFlowIdent& operator=(const ESFlowIdent&);

  // The flow node that represents this identifier
  FLNodeVector* mFlows;

  // The net ref that represents this identity
  NUNetRefHdl mNetRef;
}; // class ESFlowIdent : public CarbonIdent


//! An expression derived class for FLNodeElab* leaf nodes
class ESFlowElabIdent : public CarbonIdent
{
#if !pfGCC_2
  using CarbonIdent::getNode;
#endif

public: CARBONMEM_OVERRIDES
  //! constructor
  ESFlowElabIdent(const FLNodeElabVector* flowElabs, const NUNetRefHdl& netRef,
                  UInt32 bitSize, bool isSigned);

  //! destructor
  ~ESFlowElabIdent();

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! Compare function so that we can do caching
  /*! This function will make sure the expression types are the same
   *  (It does not compare bit sizes). If that is the case then it
   *  calls the approriate leaf class compare function after doing a
   *  dynamic cast.
   */
  ptrdiff_t compare(const CarbonExpr* expr) const;

  //! Get the FLNodeElab associated with this identifier
  virtual FLNodeElab* getFlowElab() const;

  //! Get the unelaborated flow associated with this identifier
  FLNode* getFlow() const;

  //! Returns a loop over the flows for this ident
  FLNodeElabVectorLoop loopFlows();

  //! Return the number of flows in this ident
  virtual int numFlows() const;

  //! Return the size this expression is "naturally".
  virtual UInt32 determineBitSize() const;

  //! Dump myself to cout
  virtual void print(bool recurse = true, int indent = 0) const;

  //! Return class name
  virtual const char* typeStr() const { return "FlowElabIdent"; }

  //! print verilog-syntax
  virtual void composeIdent(ComposeContext* context) const;

  //! does nothing, returns false
  virtual SignT evaluate(ExprEvalContext*) const;

  //! Asserts if called
  virtual AssignStat assign(ExprAssignContext*);
  //! Asserts if called
  virtual AssignStat assignRange(ExprAssignContext*, const ConstantRange&);

  const STAliasedLeafNode* getNode(DynBitVector* usageMask) const;

  //! Get the net ref that this ident represents
  const NUNetRefHdl& getNetRef() const;

  //! Returns true if this ident represents the entire net
  virtual bool isWholeIdentifier() const;

  //! Cast Expr to an ESFlowElabIdent
  const ESFlowElabIdent* castESFlowElabIdent() const;

private:
  //! Hide copy and assign constructors.
  ESFlowElabIdent(const ESFlowElabIdent&);
  ESFlowElabIdent& operator=(const ESFlowElabIdent&);

  // The flow node that represents this identifier
  FLNodeElabVector* mFlowElabs;

  // The net ref that represents this identity
  NUNetRefHdl mNetRef;
}; // class ESFlowElabIdent : public CarbonIdent

//! Structure to sort flow nodes by their net ref
struct ESCompareNetRef
{
  bool operator()(const NUNetRefHdl& netRef1, const NUNetRefHdl& netRef2) const
  {
    NU_ASSERT2(!netRef1->overlaps(*netRef2), netRef1, netRef2);
    NU_ASSERT2(netRef1->getNet() == netRef2->getNet(), netRef1, netRef2);
    return NUNetRef::compare(*netRef1, *netRef2, false) > 0;
  }
};

//! Type to sort expressions by their net ref
typedef UtMap<NUNetRefHdl, CarbonExpr*, ESCompareNetRef> ESSortedExprMap;

//! Abstraction for an iterator over sorted expression maps
typedef ESSortedExprMap::const_iterator ESSortedExprMapIter;

//! Type to break flow nodes into buckets by net ref
typedef UtMap<NUNetRefHdl, FLNodeVector> ESFlowBuckets;

//! Abstraction for an iterator over a map from net refs to flows
typedef ESFlowBuckets::const_iterator ESFlowBucketsIter;

//! A class that provides call backs to the user of the ESPopulateExpr
class ESCallback
{
public: CARBONMEM_OVERRIDES
  //! constructor
  ESCallback(ESPopulateExpr* populateExpr) : mPopulateExpr(populateExpr) {}

  //! destructor
  virtual ~ESCallback() {}

  //! Function to test whether we should stop at this flow node or not
  /*! No default function is provided. The caller must override this routine.
   *
   * \param flow The flow node that we should either traverse through
   * or stop at.
   * \param isContDriver True if flow's defnet is a continuous driver
   */
  virtual bool stop(FLNode* flow, bool isContDriver) = 0;

  //! Function to create a identifier (free variable) from a flow node.
  /*! The default creation routine can create an appropriate
   *  identifier. Any derived classes should override this function if
   *  something different should be done. The derived classes are free
   *  to create any expression.
   *
   *  \param flows All the flow nodes for which we should create an
   *  ident expression.
   *
   *  \param bitSize the size of the identity expression. That way we
   *  can create an expression with a different size than the flow(s)
   *  define.
   *
   *  \param hardStop If set, an identifier must be created for this.
   *  I would like to get the rid of this parameter.
   *
   *  \param resNetRef The resulting net ref for the ident expression.
   *  This could be a subset of the net if it is only partially driven.
   *  The caller needs to know if this happens.
   */
  virtual CarbonExpr* createIdentExpr(const FLNodeVector& flows,
                                      UInt32 bitSize, bool isSigned,
                                      bool hardStop, NUNetRefHdl* resNetRef);
  
protected:
  ESPopulateExpr* mPopulateExpr;

private:
  //! Hide copy and assign constructors.
  ESCallback(const ESCallback&);
  ESCallback& operator=(const ESCallback&);
}; // class ESCallback

//! A class that provides call backs to the user of the ESPopulateExpr
class ESCallbackElab
{
public: CARBONMEM_OVERRIDES
  //! constructor
  ESCallbackElab(ESPopulateExpr* populateExpr) : mPopulateExpr(populateExpr) {}



  //! destructor
  virtual ~ESCallbackElab() {}

  //! Function to test whether we should stop at this point or not
  /*! No default function is provided. The caller must override this routine.
   *
   * \param flows The set of elaborated flow nodes that we should
   * either traverse through or stop at.
   *
   * \param netRef the net ref for the vector of flows (accumulated)
   *
   * \param isContDriver A boolean indicating whethere the flow nodes
   * represent a continous driver or not.
   *
   * \returns true if we should stop and create an ident of these
   * elaborated flows, false if we should recurse creating the
   * expression.
   */
  virtual bool stop(FLNodeElabVector* flows, const NUNetRefHdl& netRef,
                    bool isContDriver) = 0;

  //! Function to create a identifier (free variable) from a flow node.
  /*! The default creation routine can create an appropriate
   *  identifier. Any derived classes should override this function if
   *  something different should be done. The derived classes are free
   *  to create any expression.
   *
   *  \param flowElabs The elaborated flow nodes for which we should
   *  create an elaborated ident expression.
   *
   *  \param bitSize the size of the identity expression. That way we
   *  can create an expression with a different size than the flow(s)
   *  define.
   *
   *  \param hardStop If set, an identifier must be created for this.
   *  I would like to get the rid of this parameter.
   */
  virtual ESExprHndl createIdentExpr(FLNodeElabVector* flowElabs, 
                                     UInt32 bitSize,
				     bool isSigned, bool hardStop);

  //! Callback method that allows for cycle detection
  virtual void addFlowsInProgress(FLNodeElabVector& /* flows */,
                                  FLNodeElabVector* /* addedFlows */)
  {
    // by default, do nothing
  }

  //! Callback method that allows for cycle detection
  virtual void removeFlowsInProgress(FLNodeElabVector& /* flows */)
  {
    // by default, do nothing
  }

  //! Callback method that intercepts the recursive create function
  /*! Override this to implement elaborated expression caching
   */
  virtual ESExprHndl create(FLNodeElab* flowElab);

protected:
  ESPopulateExpr* mPopulateExpr;

private:
  //! Hide copy and assign constructors.
  ESCallbackElab(const ESCallbackElab&);
  ESCallbackElab& operator=(const ESCallbackElab&);
}; // class ESCallbackElab

//! A class to populate unelaborated expressions from unelaborated flow
/*! The class currently also contains elaborated population but I will
 *  clean that up soon.
 */
class ESPopulateExpr
{
public: CARBONMEM_OVERRIDES
  //! constructor
  ESPopulateExpr(ESFactory* factory, NUNetRefFactory* netRefFactory);

  //! destructor
  ~ESPopulateExpr();

  //! Return the factory
  /*
    Return the factory. After expression population, it may be useful
    to amend the expression with another expression to invert
    polarity. For example, bidi enables all want to saved as driving
    when true, making the code to analyze enables simpler. So, this
    allows the user access to the factory to create another
    expression.
  */
  ESFactory* getFactory();

  //! Create an expression from an unelaborated flow node
  /*! Does a recursive flow traversal based on a callback that
   *  indicates when to stop. As the routine pops back up from the
   *  recursion it creates expressions that match the logic.
   *
   *  \param flow The starting flow node for which to create an
   *  expression
   *
   *  \param callback A class containing callback routines so that the
   *  caller can deal with creating identifiers (free variables).
   *
   *  \param newCovered If set, we start a new set for detecting
   *  cycles. This should not be false on the first call in a
   *  recursive call. The default is true.
   *
   *  \returns a handle to an expression that represents the nodes
   *  traversed.
   */
  ESExprHndl create(FLNode* flow, ESCallback* callback,
		    bool newCovered = true);

  //! Create an expression which includes edges from an unelaborated flow node
  /*! Does a recursive flow traversal based on a callback that
   *  indicates when to stop. As the routine pops back up from the
   *  recursion it creates expressions that match the logic.
   *
   *  If the flow involves edges, the edge expressions are included in
   *  the resulting expression.
   *
   *  \param flow The starting flow node for which to create an
   *  expression
   *
   *  \param callback A class containing callback routines so that the
   *  caller can deal with creating identifiers (free variables).
   *
   *  \param newCovered If set, we start a new set for detecting
   *  cycles. This should not be false on the first call in a
   *  recursive call. The default is true.
   *
   *  \returns a handle to an expression that represents the nodes
   *  traversed.
   */
  ESExprHndl createWithEdges(FLNode* flow, ESCallback* callback,
                             bool newCovered = true);

  //! Create an expression for the condition of an 'if' flow node
  /*! Does a recursive flow traversal based on a callback that
   *  indicates when to stop. As the routine pops back up from the
   *  recursion it creates expressions that match the logic.
   *
   *  The flow must be for an 'if' statement, and the returned expression
   *  will repesent only the condition of the if statement.
   *
   *  \param flow The starting flow node for which to create an
   *  expression (must an 'if' statement flow)
   *
   *  \param callback A class containing callback routines so that the
   *  caller can deal with creating identifiers (free variables).
   *
   *  \returns a handle to an expression that represents the
   *  condition of the if statement.
   */
  ESExprHndl createIfCondition(FLNode* flow, ESCallback* callback);

  //! Create an expression for the single edge expression of an always block
  /*! Does a recursive flow traversal based on a callback that
   *  indicates when to stop. As the routine pops back up from the
   *  recursion it creates expressions that match the logic.
   *
   *  The flow must be for an edge-sensitive always block, and the returned
   *  expression will repesent the expression in which the block is sensitive.
   *
   *  \param flow The starting flow node for which to create an
   *  expression (must an edge-sensitive always block flow)
   *
   *  \param callback A class containing callback routines so that the
   *  caller can deal with creating identifiers (free variables).
   *
   *  \returns a handle to an expression that represents the
   *  sensitivity expression of the always block.
   */
  ESExprHndl createSingleEdge(FLNode* flow, ESCallback* callback);

  //! Create an expression from an elaborated flow node
  /*! Does a recursive flow traversal based on a callback that
   *  indicates when to stop. As the routine pops back up from the
   *  recursion it creates expressions that match the logic.
   *
   *  \param flow The starting elaborated flow node for which to
   *  create an expression
   *
   *  \param callback A class containing callback routines so that the
   *  caller can deal with creating identifiers (free variables).
   *
   *  \returns a handle to an expression that represents the nodes
   *  traversed.
   */
  ESExprHndl create(FLNodeElab* flow, ESCallbackElab* callbackElab);

  //! Create an expression from the condition of an elaborated flow node for an 'if' statement
  /*! Does a recursive flow traversal based on a callback that
   *  indicates when to stop. As the routine pops back up from the
   *  recursion it creates expressions that match the logic.
   *
   *  \param flow The starting elaborated flow node for which to
   *  create an expression.  It must be an 'if' statement elaborated flow.
   *
   *  \param callback A class containing callback routines so that the
   *  caller can deal with creating identifiers (free variables).
   *
   *  \returns a handle to an expression that represents the condition
   *  expression of the if statement.
   */
  ESExprHndl createIfCondition(FLNodeElab* flow, ESCallbackElab* callbackElab);

  //! Create an expression from the edge expression of an elaborated flow node for an always block
  /*! Does a recursive flow traversal based on a callback that
   *  indicates when to stop. As the routine pops back up from the
   *  recursion it creates expressions that match the logic.
   *
   *  \param flow The starting elaborated flow node for which to
   *  create an expression.  It must be an edge-sensitive always
   *  block elaborated flow.
   *
   *  \param callback A class containing callback routines so that the
   *  caller can deal with creating identifiers (free variables).
   *
   *  \returns a handle to an expression that represents the edge
   *  expression for the always block.
   */
  ESExprHndl createSingleEdge(FLNodeElab* flow, ESCallbackElab* callbackElab);

  //! Return the enable of an expression
  /*! An enabled expression is of the form "en ? d : 1'bz". So an
   *  expression has an enable if it sometimes drives a Z. This
   *  function returns an expression that indicates when the
   *  expression is driving a real value (non-Z). If the expression is
   *  not enabled it returns a constant 1 expression.
   */
  ESExprHndl getEnable(ESExprHndl exprHndl);

  //! A function to adjust the size and signedness of an expression
  /*!
   *  NOTE This resize is unlike the NUExpr resize functions.
   *  The adjustments done here are only to the result of the
   *  expression and the signed/sizing information is not propagated
   *  down to the operands of the expression as is done 
   *  in the NUExpr resize functions.
   * 
   *  This function creates an adapter that takes as input the value
   *  produced by the expression defined by \a exprHndl and produces
   *  a result of the size and signedness defined by the \a bitSize
   *  and \a isSigned arguments.  This created adapter will manipulate
   *  the value of \a exprHndl using the "Carbon sizing rules" (which
   *  are similar to the verilog rules).  The expression itself will
   *  be handled as if it were self-determined so that no part of the
   *  expression will be resized or resigned.
   *  
   *  \param exprHndl handle to the expression to be resized
   *  \param bitSize the desired size
   *  \param isSigned the desired signedness.
   */
  ESExprHndl resize(ESExprHndl exprHndl, UInt32 bitSize, bool isSigned);

  //! return the netref factory
  NUNetRefFactory* getNetRefFactory() const {return mNetRefFactory;}

  //! Clear any statement to flow local caches
  /*! This routine clears any caches created during flow walks. These
   *  caches store maps from statements to flows for them. These
   *  caches could become invalid when nucleus or flow is modified. It
   *  is up to the caller to determine the appropriate time to clear
   *  the caches.
   *
   *  Note that this does not clear the expression caches. Those are
   *  local caches to each create call and get cleared on those calls.
   */
  void clearStmtFlowCaches(void);

  //! Clear the expression cache. 
  /*! This should only be done if the expressions produced are not
   *  stored for later use.
   */
  void clearExprCache(void);
  
  //! Create a concat to express a non-overlapping multidriver on a part of a net 
  /*!
    \param resultNetRef The part of the net that the multidriver. 
    corresponds to. This can be the entire net.
    \param accumNetRef The accumulated net references in the exprMap.
    \param bitSize Self-explanatory
    \param isSigned Self-explanatory
    \param exprMap A map of NUNetRefHdls to CarbonExprs. Each
    NUNetRefHdl corresponds to a range in the resultNetRef and the
    CarbonExpr describes the driver for that range. \e None of the
    NUNetRefs can overlap. No check is done for this.
    \param filler If there are holes in the concatenation, fill it
    with this value. Currently, only 0, X, and Z are
    supported. 
   */
  CarbonExpr* createNetRefMultiDriverConcat(const NUNetRefHdl& resultNetRef,
                                            const NUNetRefHdl& accumNetRef,
                                            UInt32 bitSize, bool isSigned,
                                            ESSortedExprMap* exprMap,
                                            Carbon4StateVal filler);

private:
  //! Hide copy and assign constructors.
  ESPopulateExpr(const ESPopulateExpr&);
  ESPopulateExpr& operator=(const ESPopulateExpr&);

  // The callbacks need to be a friend to get access to the factory
  friend class ESCallback;
  friend class ESCallbackElab;
  friend class ElabCallback;

  // Make the copy and replace walker classes as well as the translate
  // expression class friends of this class so that they can call the
  // create functions to do creation.
  friend class CopyWalker;
  friend class TranslateExpr;

  // Type to do input net to flow matching
  typedef NUNetRefFLNodeMultiMap InputSet;

  // Class to get the net ref associated with a flow node. The problem
  // is that for unelaborated traversals, we can simply get it from
  // the unelaborated flow. But for elaborated traversals we need to
  // normalize all the net refs to a single net since each elaborated
  // flow (and their corresponding unelaborated flow) may point to
  // different nets.
  class NetRefFunctor
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    NetRefFunctor() {}

    //! virtual destructor
    virtual ~NetRefFunctor() {}

    //! Routine to get the net ref for a given flow node
    virtual NUNetRefHdl getNetRef(FLNode* flow, NUNetRefFactory*);

  private:
    //! Hide copy and assign constructors.
    NetRefFunctor(const NetRefFunctor&);
    NetRefFunctor& operator=(const NetRefFunctor&);
  };

  // Routines to do translation of flow
  CarbonExpr* wrappedRecursiveCreate(FLNode* flow, ESCallback* callback,
				     bool nested);
  CarbonExpr* recursiveCreate(FLNode* flow, ESCallback* callback,
			      bool nested = false);
  CarbonExpr* translateExpr(FLNode* flow, NUExpr*);
  CarbonExpr* translateEdgeExpr(FLNode* flow, NUExpr*);
  CarbonExpr* translateIf(FLNode*, NUIf*, ESCallback* callback);
  CarbonExpr* translateCase(FLNode*, NUCase*, ESCallback* callback);
  CarbonExpr* translateEnabledDriver(FLNode*, NUEnabledDriver*);

  /*! Function to convert a set of flow nodes into net ref buckets
   * that look like a concat. The caller passes in the net ref they
   * want filled in and the code bucketizes the flows.
   *
   * If some of the requested net ref bits are not covered by flow nodes,
   * holes are created in the bucket map
   *
   * The function returns true if succesful. It returns false if there
   * is a problem creating the buckets. One example is if there is
   * overlap between two flow nodes
   */ 
  bool isConcat(const FLNodeVector&, const NUNetRefHdl&, NetRefFunctor&,
                ESFlowBuckets*, NUNetRefHdl*);
  bool createMultiExpr(const ESFlowBuckets& flowBuckets,
                       ESCallback* callback, ESSortedExprMap* exprMap,
                       bool nested, NUNetRefHdl* resNetRef);
  CarbonExpr* resize(CarbonExpr* expr, UInt32 bitSize, bool isSigned);
  CarbonExpr* multiDriver(FLNodeVector&, const NUNetRefHdl&, bool isSigned,
                          ESCallback*, bool nested = false,
                          NUNetRefHdl* resNetRef = NULL);
  CarbonExpr* createMultiDriver(bool isValidConcat,
                                const NUNetRefHdl& resultNetRef,
                                UInt32 bitSize, bool isSigned,
                                const ESSortedExprMap& exprMap);
  CarbonExpr* createMultiDriverConcat(const ESSortedExprMap& exprMap,
                                      const ConstantRange& range,
                                      UInt32 bitSize, bool isSigned);
  CarbonExpr* createNetExpr(NUNet*, const ConstantRange&, UInt32,
                            bool isSigned, InputSet*, ESCallback*);
  CarbonExpr* createVecNetRange(CarbonExpr*, FLNode*, const NUNetRefHdl&,
				NUVectorNet*, const ConstantRange&, UInt32,
                                bool isSigned);
  bool legalLvalues(FLNode* flow);
  bool translatePortConnection(FLNodeElab*, ESCallbackElab*, ESExprHndl&);
  bool illegalPortLvalue(NUUseDefNode* useDef) const;

  // For single drivers, this is simpler to call instead of the vector-version
  CarbonExpr* createIdent(FLNode* flow, ESCallback* callback,
			  bool isSigned, bool mustStop);

  // Elaborated creation helper functions
  static bool stop(FLNode* flow, void* data);
  static CarbonExpr* createExpr(FLNode* flow, bool hardStop, void* data);

  // Common helper functions
  void helperGetInputSet(FLNode* flow, InputSet& inputSet);
  CarbonExpr* findAndCreateIdent(NUNet*, const ConstantRange&, InputSet*,
				 ESCallback*);

  // Utility classes
  ESFactory* mFactory;
  TranslateExpr* mTranslateExpr;
  NUNetRefFactory* mNetRefFactory;

  // The following set helps us detect cycles when translating
  typedef UtVector<FLNodeSet*> SeenFlows;
  SeenFlows* mSeenFlows;
  void pushCovered(bool newCovered);
  void popCovered(bool newCovered);
  bool isCovered(FLNode* flow) const;
  void addCovered(FLNode* flow);
  void removeCovered(FLNode* flow);

  // If the user only cares about nets, not flow, and we reach a stopping
  // net that is multiply driven, then we can ignore the multiple
  // drivers.
  bool mIgnoreMultiDrivenStoppers;

  class CreateCacheKey {
  public: CARBONMEM_OVERRIDES
    CreateCacheKey(FLNode * flow, ESCallback * callback, bool nested) :
      mFlow(flow),
      mCallback(callback),
      mNested(nested) 
    {}
    bool operator<(const CreateCacheKey& other) const {
      if (mFlow==other.mFlow) {
	if (mCallback==other.mCallback) {
	  return (mNested < other.mNested);
	} else {
	  return (mCallback < other.mCallback);
	}
      } else {
	return (mFlow < other.mFlow);
      }
    }
  private:
    FLNode * mFlow;
    ESCallback * mCallback;
    bool mNested;
  };

  typedef UtMap<CreateCacheKey,CarbonExpr*> CarbonExprCache;
  CarbonExprCache mCache;

  // Case stmt to flow cache
  typedef FLCaseClassifier<FLNode> FLNodeCaseClassifier;
  typedef UtMap<NUUseDefNode*, FLNodeCaseClassifier*> CaseFlowCache;
  CaseFlowCache* mCaseFlowCache;
  FLNodeCaseClassifier* classifyCaseFlow(FLNode* flow);
  void clearCaseFlowCache(void);

}; // class ESPopulateExpr

#endif // _ESPOPULATEEXPR_H_
