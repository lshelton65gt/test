// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2003-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!  \file
 * This file defines all the expressions supported in
 * expression synthesis
 */

#ifndef __Expr_h_
#define __Expr_h_

#include <cstddef>
#ifndef _DynBitVector_h_
#include "util/DynBitVector.h"
#endif
#ifndef __UtIOStream_h_
#include "util/UtIOStream.h"
#endif
#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif
#ifndef CONSTANTRANGE_H_
#include "util/ConstantRange.h"
#endif

#ifndef __CBUILDSHELLDB_H_
#include "iodb/CbuildShellDB.h"
#endif

#ifndef _UT_INDENT_H_
#include "util/UtIndent.h"
#endif

#ifndef __STSYMBOLTABLE_H_
#include "symtab/STSymbolTable.h"
#endif

class STAliasedLeafNode;
class CarbonExprWalker;
class CarbonIdent;
class CarbonConst;
class CarbonConstXZ;
class CarbonConstReal;
class CarbonUnaryOp;
class CarbonPartsel;
class CarbonBinaryOp;
class CarbonTernaryOp;
class CarbonNaryOp;
class CarbonConcatOp;
class CarbonEdge;
class FLNode;
class FLNodeElab;
class SymTabIdent;
class ESFlowIdent;
class ESFlowElabIdent;
class ESFactory;
class ExprDBContext;
class CarbonNetIdent;
class CarbonMemIdent;
class CarbonMemWordIdent;
class UtIndent;

//! Expression assignment context
class ExprAssignContext
{
public: 
  CARBONMEM_OVERRIDES

  //! Constructor
  ExprAssignContext();
  //! virtual Destructor
  virtual ~ExprAssignContext();
  
  //! Set the starting value with UInt32 arrays
  void putAssigns(const UInt32* val,
                  const UInt32* drv,
                  size_t numWords,
                  size_t bitWidth);

  //! Set the starting value with DynBitVectors
  void putAssigns(const DynBitVector& val,
                  const DynBitVector& drv);

  //! Get the value array
  DynBitVector& getValue();
  //! Get the drive array
  DynBitVector& getDrive();

  //! Get the value array pointer
  UInt32* getValueArr();
  //! Get the drive array pointer
  UInt32* getDriveArr();

  //! Right shifts the value and drive 
  void rshift(size_t shift);
  
  //! Resizes the value and drive
  void resize(size_t bitSize);
  
  //! Negates the value
  void negate();

  //! Bit flips the value
  void flip();

  //! Changes the direction of the value and the drive
  /*!
    For example, if the binary value is 000111, then
    this function will make it 111000.

    This operation is performed on both the value and the drive
  */
  void flipDirection();

  //! At which bit index is the assign currently?
  UInt32 curBitIndex() const;
  
  //! Returns a new copy of this. 
  /*!
    Caller is responsible for deleting the copy.
  */
  virtual ExprAssignContext* copy() const;

protected:
  //! Copies the internals of the base class to copy
  void internalCopy(ExprAssignContext* copy) const;
  
private:
  DynBitVector mValue;
  DynBitVector mDrive;
  UInt32 mBitIndex;

  void cleanValue();
};

//! Expression assignment context
class ExprEvalContext
{
public: 
  CARBONMEM_OVERRIDES

  //! Constructor
  ExprEvalContext();
  //! Destructor
  virtual ~ExprEvalContext();
  
  //! Get the value array pointer
  DynBitVector* getValue();
  //! Get the value array reference
  DynBitVector& getValueRef();

  //! Get the drive array pointer
  DynBitVector* getDrive();
  //! Get the drive array reference
  DynBitVector& getDriveRef();

  //! Returns a new copy of this. 
  /*!
    Caller is responsible for deleting the copy.
  */
  virtual ExprEvalContext* copy() const;

protected:
  //! Copies the internals of the base class to copy
  void internalCopy(ExprEvalContext* copy) const;
  
private:
  DynBitVector mValue;
  DynBitVector mDrive;
};

//! CarbonExpr class
/*!
 * Abstract base class, an expression.
 */
class CarbonExpr
{
public: 
  CARBONMEM_OVERRIDES

  //! Sign Enumeration
  /*!
    This enumeration has 2 purposes. One is to express the actual sign
    of the expression and the other is to tell whether or not the
    expression has 'signed' v. unsigned semantics. A signed expression
    could be positive or negative, but an unsigned expression cannot
    be either of those. Therefore, ePos is aliased to eSigned to
    represent the beginning of 'signedness' in the enum. Note that
    checking whether or not an expression is signed requires checking
    against it being unsigned. For example:
    \code
    bool isSigned = (expr->getSignInfo() != CarbonExpr::eUnsigned);
    \endcode
    
  */
  enum SignT
  {
    eUnsigned,  //!< Not signed
    ePos,       //!< Positive
    eNeg,       //!< Negative
    eBadSign    //!< Invalid
  };

  //! Assignment status enum
  enum AssignStat {
    eFull,  //! All the bits were used in the assign
    ePartial,  //! Some of the bits were used
    eReadOnly //! None of the bits were used.
  };

  //! Expression types
  enum ExprT
  {
    eStart = 0,			//!< Unused

    eIdent,			//!< An identifier
    eConst,			//!< A constant with no X or Z
    eConstXZ,			//!< A constant with X and/or Z
    eConstReal,                 //!< A real-number constant

    eEdge,                      //!< An edge expression

    eOpStart,			//!< Start of the operations

    eUnStart = eOpStart,	//!< Start of unary operators
    eUnBuf = eUnStart,		//!< Unary buffer pass-through
    eUnMinus,			//!< Unary -
    eUnBitNeg,			//!< Unary Bitwise ~
    eUnVhdlNot,			//!< Unary VHDL Bitwise not
    eUnPartSel,			//!< Unary expr[const:const]
    eUnSignExt,                 //!< Unary sign-extend
    eUnChange,                  //!< Change-detect operator
    eUnEnd = eUnChange, 	//!< End of unary operators

    eBiStart,			//!< Start of binary operators
    eBiPlus = eBiStart,		//!< Binary +
    eBiMinus,			//!< Binary -
    eBiSMult,                   //!< Binary Signed *
    eBiUMult,			//!< Binary Unsigned *
    eBiSDiv,			//!< Binary /
    eBiUDiv,			//!< Binary /
    eBiSMod,			//!< Binary %
    eBiUMod,			//!< Binary %
    eBiEq,			//!< Binary ==
    eBiSLt,                     //!< Binary Signed <
    eBiULt,			//!< Binary Unsigned <
    eBiBitAnd,			//!< Binary Bitwise &
    eBiBitOr,			//!< Binary Bitwise |
    eBiBitXor,			//!< Binary Bitwise ^
    eBiRshift,			//!< Binary >>
    eBiLshift,			//!< Binary <<
    eBiBitSel,			//!< Binary expr[expr]
    eBiMemSel,			//!< Binary memexpr[expr]
    eBiDownTo,                  //!< VHDL dynamic range
    eBiEnd = eBiDownTo,		//!< End of binary operators

    eTeStart,			//!< Start of ternary operators
    eTeCond = eTeStart,		//!< Ternary ?:
    eTeEnd = eTeCond,		//!< End of ternary operators

    eNaStart,			//!< Start of n-ary operators
    eNaConcat = eNaStart,	//!< N-ary {}
    eNaMultiDriver,		//!< multiple drivers on one node
    eNaEnd = eNaMultiDriver,	//!< End of n-ary operators

    eOpEnd = eNaEnd,		//!< End of the operations

    eInvalid			//!< Bad operator
  };

  //! Enum for mode of expression composition for printing
  enum ComposeMode {
    eComposeNormal, //!< Normal verilog syntax
    eComposeC, //!< C syntax
    eComposeLeaf //!< Verilog syntax using only leaf names of idents
  };

  //! Compose context used by compose methods
  class ComposeContext
  {
  public:
    CARBONMEM_OVERRIDES

    //! Construct with UtIndent 
    ComposeContext(UtIndent* indent, ComposeMode mode) :
      mBuf(indent->getBuffer()), mIndent(indent), mMode(mode)
    {}
   
    //! Construct with buffer
    ComposeContext(UtString* buf, ComposeMode mode) :
      mBuf(buf), mIndent(NULL), mMode(mode)
    {}
    
    //! Get the buffer
    /*!
      This is either the buffer passed in with the UtString-based
      constructor
      or the buffer within the UtIndent from the UtIndent-based
      constructor.
    */
    UtString* getBuffer() { return mBuf; }
    
    //! Returns the UtIndent. NULL if there is no UtIndent
    UtIndent* getIndentObject() { return mIndent; }
    
    //! Get the compose mode.
    ComposeMode getMode() const { return mMode; }
    
  private:
    UtString* mBuf;
    UtIndent* mIndent;
    ComposeMode mMode;
  };
  
  //! constructor
  CarbonExpr(UInt32 bitSize, bool isSigned);

  //! destructor
  virtual ~CarbonExpr();

  //! Function to return the expression type
  virtual ExprT getType() const = 0;

  //! Return class name
  virtual const char* typeStr() const = 0;

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  // Casting functions for easier coding

  //! Cast to ident, NULL if it isn't
  CarbonIdent* castIdent() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonIdent*>(me->castIdent());
  }

  //! Cast to const, NULL if it isn't
  CarbonConst* castConst() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonConst*>(me->castConst());
  }

  //! Cast to constxz, NULL if it isn't
  CarbonConstXZ* castConstXZ() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonConstXZ*>(me->castConstXZ());
  }

  //! Cast to constreal, NULL if it isn't
  CarbonConstReal* castConstReal() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonConstReal*>(me->castConstReal());
  }

  //! Cast to unary, NULL if it isn't
  CarbonUnaryOp* castUnary() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonUnaryOp*>(me->castUnary());
  }

  //! Cast to binary, NULL if it isn't
  CarbonBinaryOp* castBinary() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonBinaryOp*>(me->castBinary());
  }

  //! Cast to ternary, NULL if it isn't
  CarbonTernaryOp* castTernary() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonTernaryOp*>(me->castTernary());
  }

  //! Cast to nary, NULL if it isn't
  CarbonNaryOp* castNary() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonNaryOp*>(me->castNary());
  }
  
  //! Cast to concat, NULL if it isn't
  CarbonConcatOp* castConcat() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonConcatOp*>(me->castConcat());
  }
  
  //! Cast to partsel, NULL if it isn't
  CarbonPartsel* castPartsel() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonPartsel*>(me->castPartsel());
  }

  //! Cast to edge, NULL if it isn't
  CarbonEdge* castEdge() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonEdge*>(me->castEdge());
  }

  //! Cast to SymTabIdent, NULL if it isn't
  SymTabIdent* castSymTabIdent() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<SymTabIdent*>(me->castSymTabIdent());
  }

  //! Cast to ESFlowIdent, NULL if it isn't
  ESFlowIdent* castESFlowIdent() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<ESFlowIdent*>(me->castESFlowIdent());
  }

  //! Cast to ESFlowElabIdent, NULL if it isn't
  ESFlowElabIdent* castESFlowElabIdent() { 
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<ESFlowElabIdent*>(me->castESFlowElabIdent());
  }

  //! Cast to CarbonNetIdent, NULL if it isn't
  CarbonNetIdent* castCarbonNetIdent() {
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonNetIdent*>(me->castCarbonNetIdent());
  }
  
  //! Cast to CarbonMemIdent, NULL if it isn't
  CarbonMemIdent* castCarbonMemIdent() {
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonMemIdent*>(me->castCarbonMemIdent());
  }
  
  //! Cast to CarbonMemWordIdent, NULL if it isn't
  CarbonMemWordIdent* castCarbonMemWordIdent() {
    const CarbonExpr* me = const_cast<const CarbonExpr*>(this);
    return const_cast<CarbonMemWordIdent*>(me->castCarbonMemWordIdent());
  }
  
  //! const version
  virtual const CarbonIdent* castIdent() const;
  //! const version
  virtual const CarbonConst* castConst() const;
  //! const version
  virtual const CarbonConstXZ* castConstXZ() const;
  //! const version
  virtual const CarbonConstReal* castConstReal() const;
  //! const version
  virtual const CarbonUnaryOp* castUnary() const;
  //! const version
  virtual const CarbonBinaryOp* castBinary() const;
  //! const version
  virtual const CarbonTernaryOp* castTernary() const;
  //! const version
  virtual const CarbonNaryOp* castNary() const;
  //! const version
  virtual const CarbonConcatOp* castConcat() const;
  //! const version
  virtual const CarbonPartsel* castPartsel() const;
  //! const version
  virtual const CarbonEdge* castEdge() const;
  //! const version
  virtual const SymTabIdent* castSymTabIdent() const;
  //! const version
  virtual const ESFlowIdent* castESFlowIdent() const;
  //! const version
  virtual const ESFlowElabIdent* castESFlowElabIdent() const;
  //! const version
  virtual const CarbonNetIdent* castCarbonNetIdent() const;
  //! const version
  virtual const CarbonMemIdent* castCarbonMemIdent() const;
  //! const version
  virtual const CarbonMemWordIdent* castCarbonMemWordIdent() const;

  //! Compare function so that we can do caching
  /*! This function will make sure the expression types are the same
   *  and the bit sizes are the same. If that is the case then it
   *  calls the approriate leaf class compare function after doing a
   *  dynamic cast.
   */
  bool operator==(const CarbonExpr& other) const;

  //! Compare function to be overloaded by each derived class
  virtual ptrdiff_t compare(const CarbonExpr* other) const = 0;

  //! Return the size of this expression
  /*! Currently, expressions cannot be resized. But when we can resize
   *  them, it will create new expressions.
   */
  UInt32 getBitSize() const;

  //! Return whether this expression is signed
  bool isSigned() const {return mIsSigned;}

  //! Evaluate the expression. Put result in value and drive
  /*!
    \param value The resultant value will be placed here.
    \param drive If there are any unknowns in the value the
    appropriate bits will be set here. Note that for conditional
    expressions unknowns evaluate to false, so both the value and the
    drive will be 0. Both value and drive will be the same size.
    \param index This is a hack for the shell-side. The shell may have
    multiple instances of a design, but only 1 node is associated with
    a given net in the design. Therefore, the instance id needs to be
    passed into the evaluation so that identifiers can be properly
    evaluated.

    \returns the sign of the expression. All values in the bit vectors
    are positive. The sign tells you whether or not to take the 2's
    complement of the value. If eBadSign is returned, the evaluation
    failed.
  */
  virtual SignT evaluate(ExprEvalContext* evalContext) const = 0;

  //! Assign a value and drive to the expression
  /*!
    \param context Contains the value to be assigned. This can be
    subclassed.
    \returns Assignment status. Either eFull, ePartial, or eReadOnly.

    Regardless of the assignment status, the assign is responsible for
    updating the context. So, if the status is ePartial or eReadOnly
    the assign must still shift the context value and drive by the
    size of its expression (in bits).

    Assignment assumes runtime deposit semantics. In other words, no
    x's can be assigned to a ident, only z/0/1. Therefore, operations
    such as negation and bit-flips would cause a cleaning of the
    value.
  */
  virtual AssignStat assign(ExprAssignContext* context) = 0;

  //! Dump myself to cout
  virtual void print(bool recurse = true, int indent = 0) const = 0;


  //! Static method to enable CE_ASSERT
  static void enableAsserts();
  
  //! Static method to disable CE_ASSERT (used to prevent recursive/nested asserts)
  static void disableAsserts();

  //! Test if NU_ASSERT is OK (used in CE_ASSERT macros)
  static bool assertOK();

  //! Dump the header for asserts
  static void printAssertHeader(const char* file, int line, const char* exprStr);

  //! Dump information for an assert
  void printAssertInfo() const;

  //! Dump the trailer for asserts
  static void printAssertTrailer(void);

  //! print verilog-syntax
  virtual void printVerilog() const;

  //! compose verilog-syntax
  void compose(UtString*) const;

  //! Compose this expression with leaf names for idents. 
  void composeLeaf(UtString* buf) const;
  
  //! compose C-syntax
  /*!
    Note that this will assert for non-C constructs.
  */
  void composeC(UtIndent*) const;

  //! Return true if this is an identifier
  static bool isIdent(ExprT op) { return op == eIdent; }

  //! Return true if this is a constant without X or Z
  static bool isConst(ExprT op) { return (op == eConst); }

  //! Return true if this is a constant without X or Z
  static bool isConstXZ(ExprT op) { return (op == eConstXZ); }

  //! Return true if this is a part select
  static bool isPartsel(ExprT op) { return (op == eUnPartSel); }

  //! Return true if this operator is a unary operator
  static bool isUnaryOp(ExprT op)
  {
    return ((op >= eUnStart) && (op <= eUnEnd));
  }

  //! Return true if the given operator is a binary operator.
  static bool isBinaryOp(ExprT op)
  {
    return ((op >= eBiStart) && (op <= eBiEnd));
  }

  //! Return true if the given operator is a ternary operator.
  static bool isTernaryOp(ExprT op)
  {
    return ((op >= eTeStart) && (op <= eTeEnd));
  }

  //! Return true if this is a concat op
  static bool isConcatOp(ExprT op) { return (op == eNaConcat); }

  //! Return true if the given operator is an n-ary operator.
  static bool isNaryOp(ExprT op)
  {
    return ((op >= eNaStart) && (op <= eNaEnd));
  }

  //! Returns true if the op is valid
  static bool isValidOp(ExprT op) {
    return ((op > eStart) && (op <= eOpEnd));
  }
  
  //! Get the enable for this expression if it has one (or NULL if it doesn't)
  /*! If this expression can sometimes drive a Z, it returns the
   *  enable for that driver. It also returns whether the enable
   *  should be true or false to drive a value.
   */
  virtual CarbonExpr* getEnable(bool&) const;

  //! Check if this expression drives a Z value
  virtual bool drivesZ() const;

  //! Check if this expression is a non-zero constant
  virtual bool isNonZero() const;

  //! Check if this expression is the constant zero
  virtual bool isZero() const;

  //! Increment the reference count
  void incRefCnt();

  //! Decrement the reference count and return the value
  UInt32 decRefCnt();

  //! Get the STAliasedLeafNode for this expression
  /*!
    Many expressions do not have STAliasedLeafNodes, and in some
    applications none may exist. This is here because in general,
    carbon is symboltablenode-based in the shell.

    \param usageMask Pointer to an DynBitVector instance. This will
    get updated with the bit usage of the expression.
  */
  virtual const STAliasedLeafNode* getNode(DynBitVector* usageMask) const;

  // Get the flow-node associated with this expr.  Must be CarbonIdent.
  virtual FLNode* getFlow() const;

  // Get the flow-node associated with this expr.  Must be CarbonIdent.
  virtual FLNodeElab* getFlowElab() const;

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual CarbonExpr* getArg(UInt32 index) const;

  //! Return number of arguments to this operator.
  virtual UInt32 getNumArgs() const;

  //! return value and drive
  virtual void getValue(DynBitVector* value,  DynBitVector* drive) const;

  //! Return just the value
  virtual void getValue(DynBitVector* value) const;

  //! Find the constant range, return false if there is no range
  virtual bool getDeclaredRange(ConstantRange* range) const;

  //! Compose version of the expression based on mode. 
  /*!
    Called by all the compose methods
  */
  virtual void composeHelper(ComposeContext* context) const = 0;

protected:

  void printSize(UtOStream&) const;

  //! Verilog-sized bit size.
  UInt32 mBitSize;

  //! Is this expression signed?
  bool mIsSigned;

  // The CarbonExprWalker has to be a friend to get to the private visit fn
  friend class CarbonExprWalker;

  // Visit routine used by the CarbonExprWalker to traverse an expression
  virtual void visit(CarbonExprWalker* exprWalker) = 0;

  //! This checks drive and sets the value appropriately
  /*!
    If drive is non-zero then drive is fully set and value is
    cleared, false is returned. Otherwise, nothing is done and true is
    returned.
  */
  static bool munchUnknown(DynBitVector* value, DynBitVector* drive);

private:
  //! Hide copy and assign constructors.
  CarbonExpr(const CarbonExpr&);
  CarbonExpr& operator=(const CarbonExpr&);

  // Reference count so that we know when to delete them
  UInt32 mRefCount;
} ;

// Note: these macros are written this way so that they can be invoked like
// functions, with the final semicolon supplied by the user.  A brace-delimited
// statement is no good, because it leads to syntax errors with code like:
// if (cond)
//   CE_ASSERT(predicate, expr);
// else
//   ...
#define CE_ASSERT(exp, carbonExpr) \
  do { \
    if (!(exp) && CarbonExpr::assertOK()) { \
      CarbonExpr::disableAsserts(); \
      CarbonExpr::printAssertHeader(__FILE__, __LINE__, __STRING(exp)); \
      (carbonExpr)->printAssertInfo(); \
      CarbonExpr::printAssertTrailer(); \
      CarbonExpr::enableAsserts(); \
    } \
  } while (0) /* no trailing semicolon -- it is supplied by the caller */


//! Abstraction for a vector of expressions
typedef UtArray<CarbonExpr*> CarbonExprVector;

//! An iterator over a vector of expressions
typedef CarbonExprVector::iterator CarbonExprVectorIter;
typedef CarbonExprVector::const_iterator CarbonExprVectorCIter;

//! CarbonIdent class
/*!
 * This class represents a leaf node in all expressions. This leaf can
 * have its own sub expression to make expressions of expressions.
 *
 * This is not a complete class. The user of this facility should
 * create derived classes for the various leaf node types.
 */
class CarbonIdent : public CarbonExpr
{
#if !pfGCC_2
  using CarbonExpr::castIdent;
#endif

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  CarbonIdent(bool isSigned, UInt32 bitSize);

  //! destructor
  virtual ~CarbonIdent();

  //! establish a vector range for this identifier
  void putDeclaredRange(const ConstantRange* r);

  //! CarbonExpr::castIdent()
  virtual const CarbonIdent* castIdent() const;

  //! Function to return the expression type
  virtual ExprT getType() const;

  //! Compare function so that we can do caching
  /*! Note that this does an unsized comparison. It compares based on
   *  the actual size, not the expr size.
   */
  virtual ptrdiff_t compare(const CarbonExpr* other) const = 0;

  virtual const STAliasedLeafNode* getNode(DynBitVector* usageMask) const = 0;

  STAliasedLeafNode* getNode(DynBitVector* usageMask) {
    const CarbonIdent* me = const_cast<const CarbonIdent*>(this);
    return const_cast<STAliasedLeafNode*>(me->getNode(usageMask));
  }

  //! Assign a range to the ident
  /*!
    This assumes the same responsibility as assign where it consumes
    the context bits that it is supposed to use (ie., shifts past them
    after using them)
  */
  virtual AssignStat assignRange(ExprAssignContext* context, 
                                 const ConstantRange& range) = 0;

  //! Evaluates a range of the ident
  /*!
    This is necessary only for CarbonMemIdent, since it's not
    reasonable to evaluate the entire memory.
  */
  virtual SignT evaluateRange(ExprEvalContext* context, 
                              const ConstantRange& range);

  virtual FLNode* getFlow() const;

  bool getDeclaredRange(ConstantRange* range) const;

  //! Get the number of flows associated with this ident
  virtual int numFlows() const;

  //! Returns true if this ident represents the entire net
  virtual bool isWholeIdentifier() const = 0;

  //! DB Setup derivations of CarbonIdent that have nested expressions
  /*!
    \sa addNestedToDB

    Some derivations of CarbonIdent (SymTabIdentBP, for example)
    contain expressions that also need to be saved to the
    database. This allows the derivation to call
    ESFactory::setupDBContext on the nested expressions. 

    By default, this function just returns. See
    SymTabIdentBP::nestedDBSetup for a good example of usage.

    \param factory The expression factory that is holding the ident
    \param dbContext The database context
    \param inclusiveNodes Boolean describing whether or not the ident
    symtab nodes are in the symtab contained in the dbContext. This is
    normally true, but enable expressions use a separate symtab when
    being added to the db.

    \warning ESFactory::setupDBContext should not be called for the
    CarbonIdent itself. This has already been done and, in fact, is
    the reason why this function is called. Doing so will result in
    an infinite loop. So, only call this on nested or contained
    expressions that have idents which eventually do not have any
    nested or contained expressions.
  */
  virtual void nestedDBSetup(ESFactory* factory, ExprDBContext* dbContext, 
                             bool inclusiveNodes, STSymbolTable::LeafAssoc *);

  //! DB Write derivations of CarbonIdent for nested expressions
  /*!
    \sa nestedDBSetup

    Some derivations of CarbonIdent (SymTabIdentBP, for example)
    contain expressions that also need to be saved to the
    database. The nested (or contained) expressions are written
    first to the database prior to this being called. Here, the ident
    fills the CarbonExprVector with any expressions that are needed to
    complete the ident. This also needs to return the basic type of
    ident that this is. See CbuildShellDB::CarbonIdentType.

    By default, this function returns CbuildShellDB::eSymTabIdent, and
    does nothing with the expression vector.

    See SymTabIdentBP::addNestedToDB() for a good example of usage.
  */
  virtual CbuildShellDB::CarbonIdentType addNestedToDB(CarbonExprVector* exprVec) const;

  //! Compose the ident based on mode
  virtual void composeIdent(ComposeContext* context) const = 0;
    
protected:
  //! The CarbonExprWalker has to be a friend to get to the private visit fn
  friend class CarbonExprWalker;

  //! CarbonExpr::composeHelper()
  virtual void composeHelper(ComposeContext* context) const;

  //! Visit routine used by the CarbonExprWalker to traverse an expression
  virtual void visit(CarbonExprWalker* exprWalker);

  //! The vector range (NULL if not a vector)
  ConstantRange* mRange;

private:
  //! Hide copy and assign constructors.
  CarbonIdent(const CarbonIdent&);
  CarbonIdent& operator=(const CarbonIdent&);
} ;

//! CarbonConst class
/*!
 * A constant with no x'z or z's.
 */
class CarbonConst : public CarbonExpr
{
#if !pfGCC_2
  using CarbonExpr::castConst;
#endif

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  /*!
    \param val The constant value.
    \param sign The sign of the value.
    \param bitSize Size of this constant.
   */
  CarbonConst(const DynBitVector& val, SignT sign, UInt32 bitSize);

  //! destructor
  virtual ~CarbonConst();

  //! CarbonExpr::castConst()
  virtual const CarbonConst* castConst() const;

  //! Function to return the expression type
  virtual ExprT getType() const;

  //! Return the signedness of the expression
  /*!
    To get the actual sign of an operator, one must evaluate the
    expression. This method is only meant to tell you whether the
    expression is signed or not, but gives the added benefit of
    getting the actual sign if you know you have an identifier or a
    constant.

    For constants and identifiers this will return eUnsigned, ePos, or
    eNeg. So, the easiest way to test for signedness is the
    following assuming that the expression is not invalid:
    \code
    bool isSigned = (expr->getSignInfo() != CarbonExpr::eUnsigned);
    \endcode

    If the expression could be invalid then:
    \code
    CarbonExpr::SignT sign = expr->getSignInfo();
    bool isSigned = (sign != CarbonExpr::eUnsigned) && (sign != CarbonExpr::eBadSign);
    \endcode

    but, isSigned has no meaning if the expression is invalid.
  */
  virtual SignT getSignInfo() const;

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! Compare function for two constants
  virtual ptrdiff_t compare(const CarbonExpr* other) const;

  //! CarbonExpr::evaluate()
  virtual SignT evaluate(ExprEvalContext* evalContext) const;

  //! CarbonExpr::assign()
  virtual AssignStat assign(ExprAssignContext* context);

  //! Dump myself to cout
  void print(bool recurse = true, int indent = 0) const;

  //! Return class name
  virtual const char* typeStr() const;

  //! Check if the constant is a simple 0
  virtual bool isZero (void) const;

  //! Check if the constant is non-zero
  virtual bool isNonZero () const;

  //! Puts the first 64 bits of this value (a ULL) into val 
  /*!
    This only gets the first 64 bits of a value. So, if a value is
    more than that the value is truncated. Always returns true
  */
  virtual bool getULL(UInt64* val) const;

  //! Puts the first 64 bits of this value (a UL) into val (which is signed)
  /*!
    This only gets the first 64 bits of a value. So, if a value is
    more than that the value is truncated. Always returns true.
  */
  virtual bool getLL(SInt64* val) const;

  //! always returns true, puts the first 32 bits of the unsigned constant into val
  virtual bool getUL(UInt32* val) const;

  //! always returns true, puts the first 32 bits of the signed constant into val
  virtual bool getL(SInt32* val) const;

  //! Get the value of the const, ignoring the drive 
  /*!
    This will only return the value and will set any bit that is z to 0.
    So,without the drive the value is not obvious as to what is really
    is, but codegen doesn't care about drive, except maybe in rare
    cases.
    For known constants, this will return the absolute value of the
    constant. To get the sign, call getSignInfo().

    The value will be resized to the size of the constant.
  */
  virtual void getValue(DynBitVector* value) const;

  //! Override CarbonExpr::getValue()
  virtual void getValue(DynBitVector* value,  DynBitVector* drive) const;

protected:
  //! Hide copy and assign constructors.
  CarbonConst(const CarbonConst&);
  CarbonConst& operator=(const CarbonConst&);

  // The CarbonExprWalker has to be a friend to get to the private visit fn
  friend class CarbonExprWalker;

  //! For C, decimal value. Assert if xz. otherwise, hex
  virtual void composeHelper(ComposeContext* context) const;

  // Visit routine used by the CarbonExprWalker to traverse an expression
  virtual void visit(CarbonExprWalker* exprWalker);

  // The constant value
  DynBitVector mValue;

  //! Sign info for this constant.
  SignT mSignInfo;

  //! Print the value to stdout
  void printVal() const;

  //! Compose the value to a string
  virtual void composeVal(UtString*) const;
  void composeValueByType(UtString*, const DynBitVector& val) const;
  
}; // class CarbonConst : public CarbonExpr


//! Class for constants with x and/or z
class CarbonConstXZ : public CarbonConst
{
#if !pfGCC_2
  using CarbonExpr::castConstXZ;
#endif

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  /*!
    \param val The constant value for this CarbonConst, but with x's = 1
    and z's = 0.
    \param drive The mask for the xz's. For each bit in val there is a
    corresponding bit in drive that if is set means that the bit in
    val represents an x or a z.
    \param sign The sign of the value.
    \param bitSize Natural size of this constant.
  */
  CarbonConstXZ(const DynBitVector& val, const DynBitVector& drive,
		SignT sign, UInt32 bitSize);

  //! virtual destructor
  virtual ~CarbonConstXZ();

  //! CarbonExpr::castConstXZ()
  virtual const CarbonConstXZ* castConstXZ() const;

  //! Function to return the expression type
  virtual ExprT getType() const;

  //! CarbonExpr::evaluate()
  virtual SignT evaluate(ExprEvalContext* evalContext) const;

  //! CarbonExpr::assign()
  virtual AssignStat assign(ExprAssignContext* context);

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! Compare function so that we can do caching
  ptrdiff_t compare(const CarbonExpr* other) const;

  //! puts the value and the drive of this constant into 2 args
  virtual void getValue(DynBitVector* value,  DynBitVector* drive) const;

  //! put the 'known' portion of this constant into value
  virtual void getValue(DynBitVector* value) const;
  
  //! Return true if this expression drives an all Z value, false otherwise.
  virtual bool drivesZ() const;

  //! puts the 'known' 64 bit value of this constant into the val (as unsigned), always returns true
  virtual bool getULL(UInt64* val) const;

  //! puts the 'known' 64 bit value of this constant into val (as signed), always returns true
  virtual bool getLL(SInt64* val) const;
  
  virtual bool getUL(UInt32* val) const;

  //! returns class name
  virtual const char* typeStr() const;

protected:
  //! Print the value and the drive
  virtual void composeVal(UtString*) const;

  // The CarbonExprWalker has to be a friend to get to the private visit fn
  friend class CarbonExprWalker;

  // Visit routine used by the CarbonExprWalker to traverse an expression
  virtual void visit(CarbonExprWalker* exprWalker);

private:
  // forbid
  CarbonConstXZ(const CarbonConstXZ&);
  CarbonConstXZ& operator=(const CarbonConstXZ&);

  //! The known/unknown bit mask
  /*!
    For each bit in mValue there is a corresponding bit in mDrive
    that, if set, means that the corresponding bit in mValue is an
    unknown, and if the bit in mValue is 0 it is a 'z' and if it is a 1
    it is an 'x'.
  */
  DynBitVector mDrive;
};


//! CarbonConstReal class
/*!
 * A real-valued constant.
 */
class CarbonConstReal : public CarbonExpr
{
#if !pfGCC_2
  using CarbonExpr::castConst;
#endif

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  /*!
    \param val The constant value.
    \param sign The sign of the value.
   */
  CarbonConstReal(const double val, SignT sign);

  //! destructor
  virtual ~CarbonConstReal();

  //! CarbonExpr::castConstReal()
  virtual const CarbonConstReal* castConstReal() const;

  //! Function to return the expression type
  virtual ExprT getType() const;

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! Compare function for two constants
  virtual ptrdiff_t compare(const CarbonExpr* other) const;

  //! CarbonExpr::getSignInfo()
  virtual SignT getSignInfo() const;

  //! CarbonExpr::evaluate()
  virtual SignT evaluate(ExprEvalContext* evalContext) const;

  //! CarbonExpr::assign()
  virtual AssignStat assign(ExprAssignContext* context);

  //! Dump myself to cout
  void print(bool recurse = true, int indent = 0) const;

  //! Return class name
  virtual const char* typeStr() const;

  //! Check if the constant is a simple 0
  virtual bool isZero (void) const;

  //! Check if the constant is non-zero
  virtual bool isNonZero () const;

  //! Puts the first 64 bits of this value (a double) into val 
  /*!
    This only gets the first 64 bits of a value. So, if a value is
    more than that the value is truncated. Always returns true
  */
  virtual bool getULL(UInt64* val) const;

  //! Puts the first 64 bits of this value (a double) into val (which is signed)
  /*!
    This only gets the first 64 bits of a value. So, if a value is
    more than that the value is truncated. Always returns true.
  */
  virtual bool getLL(SInt64* val) const;

  //! always returns true, casts the double constant into val
  virtual bool getUL(UInt32* val) const;

  //! always returns true, puts the double constant into val (which is signed)
  virtual bool getL(SInt32* val) const;

  //! Get the value of the const, ignoring the drive 
  /*!
    This will only return the value and will set any bit that is z to 0.
    So,without the drive the value is not obvious as to what is really
    is, but codegen doesn't care about drive, except maybe in rare
    cases.
    For known constants, this will return the absolute value of the
    constant. To get the sign, call getSignInfo().

    The value will be resized to the size of the constant.
  */
  virtual void getValue(DynBitVector* value) const;

  //! Override CarbonExpr::getValue()
  virtual void getValue(DynBitVector* value,  DynBitVector* drive) const;

  void getValue( double *val ) const { *val = mValue; }

protected:
  //! Hide copy and assign constructors.
  CarbonConstReal(const CarbonConstReal&);
  CarbonConstReal& operator=(const CarbonConstReal&);

  // The CarbonExprWalker has to be a friend to get to the private visit fn
  friend class CarbonExprWalker;

  //! CarbonExpr::composeHelper()
  virtual void composeHelper(ComposeContext* context) const;

  // Visit routine used by the CarbonExprWalker to traverse an expression
  virtual void visit(CarbonExprWalker* exprWalker);

  // The constant value
  double mValue;

  //! Sign info for this constant
  SignT mSignInfo;

  //! Print the value to stdout
  void printVal() const;

  //! Compose the value to a string
  virtual void composeVal(UtString*) const;
  void composeValueByType(UtString*, const DynBitVector& val) const;
}; // class CarbonConstReal : public CarbonExpr



//! CarbonOp class
/*!
 * Abstract base class for operators.
 */
class CarbonOp : public CarbonExpr
{
public: 
  CARBONMEM_OVERRIDES

  //! constructor
  /*!
    \param op The operator
   */
  CarbonOp(CarbonExpr::ExprT op, UInt32 bitSize, bool isSigned);

  //! destructor
  virtual ~CarbonOp();
  
  //! Function to return the expression type
  virtual ExprT getType() const;
  
  //! Hash function so that we can do caching
  virtual size_t hash() const;
  
  //! Return number of arguments to this operator.
  virtual UInt32 getNumArgs() const = 0;
  
  //! Return the specified argument; 0-based index, 0 is leftmost argument
  virtual CarbonExpr* getArg(UInt32 index) const = 0;
  
  //! Dump myself to cout
  void print(bool recurse = true, int indent = 0) const;
  
  //! Virtual function to get any attribute strings
  /*! This is used for printing. Ops can print extra attributes
   *  through this method. The default is to return the empty string
   *  so this does not need to be overriden.
   *
   *  The caller must provide the space for the string
   */
  virtual const char* attrStr(UtString*) const;
  
protected:
  ExprT mOp;

  //! CarbonExpr::composeHelper()
  virtual void composeHelper(ComposeContext* context) const = 0;

private:
  //! Hide copy and assign constructors.
  CarbonOp(const CarbonOp&);
  CarbonOp& operator=(const CarbonOp&);
};


//! CarbonUnaryOp class
/*!
 * Unary operation.
 */
class CarbonUnaryOp : public CarbonOp
{
#if !pfGCC_2
  using CarbonExpr::castUnary;
#endif

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  /*!
    \param op The operator
    \param expr Expression argument
    \param bitsize bit size using verilog rules
   */
  CarbonUnaryOp(CarbonExpr::ExprT op, CarbonExpr *expr,
                UInt32 bitSize, bool isSigned);

  //! destructor
  ~CarbonUnaryOp();

  //! CarbonExpr::castUnary()
  virtual const CarbonUnaryOp* castUnary() const;

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! Compare function so that we can do caching
  ptrdiff_t compare(const CarbonExpr* other) const;

  //! Return number of arguments to this operator.
  UInt32 getNumArgs() const { return 1;}

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  CarbonExpr *getArg(UInt32 index) const;

  //! Return class name
  const char* typeStr() const;

  //! CarbonExpr::evaluate()
  virtual SignT evaluate(ExprEvalContext* evalContext) const;

  //! CarbonExpr::assign()
  virtual AssignStat assign(ExprAssignContext* context);

protected:
  // The CarbonExprWalker has to be a friend to get to the private visit fn
  friend class CarbonExprWalker;

  //! CarbonExpr::composeHelper()
  virtual void composeHelper(ComposeContext* context) const;

  // Visit routine used by the CarbonExprWalker to traverse an expression
  virtual void visit(CarbonExprWalker* exprWalker);

  //! The subexpression.
  CarbonExpr* mExpr;

  //! Sign of the unary op
  SignT mSign;

private:
  //! Hide copy and assign constructors.
  CarbonUnaryOp(const CarbonUnaryOp&);
  CarbonUnaryOp& operator=(const CarbonUnaryOp&);

  //! Shared by compose and composeC
  void composePre(UtString* buf) const;
  //! Shared by compose and composeC
  void composePost(UtString* buf) const;

};

//! CarbonPartsel class
/*!
 *  Unary part select operation. We use the unary class because we
 *  assume the indices are always constants. If this changes in the
 *  future, we should rethink this.
 */
class CarbonPartsel : public CarbonUnaryOp
{
#if !pfGCC_2
  using CarbonExpr::castPartsel;
#endif

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  /*!
    \param expr Expression argument
    \param range The range of the part select
    \param bitSize the size of the part select in context
   */
  //
  CarbonPartsel(CarbonExpr* expr, const ConstantRange& range, UInt32 bitSize,
                bool isSigned);

  //! destructor
  ~CarbonPartsel();

  //! CarbonExpr::castPartsel()
  virtual const CarbonPartsel* castPartsel() const;

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! Compare function so that we can do caching
  ptrdiff_t compare(const CarbonExpr* other) const;

  //! Return class name
  const char* typeStr() const;

  //! Return the range for this part select
  const ConstantRange& getRange() const { return *mRange; }

  //! Virtual function to get any attribute strings
  const char* attrStr(UtString*) const;

  //! CarbonExpr::evaluate()
  virtual SignT evaluate(ExprEvalContext* evalContext) const;

  //! CarbonExpr::assign()
  virtual AssignStat assign(ExprAssignContext* context);

protected:
  // The CarbonExprWalker has to be a friend to get to the private visit fn
  friend class CarbonExprWalker;

  // Visit routine used by the CarbonExprWalker to traverse an expression
  virtual void visit(CarbonExprWalker* exprWalker);

private:
  //! Hide copy and assign constructors.
  CarbonPartsel(const CarbonPartsel&);
  CarbonPartsel& operator=(const CarbonPartsel&);

  // The range
  /*
    This needs to be a pointer in order for this to be saved to the
    database on a 64-bit platform. The range itself will be added
    to the database, which uses Zostream, which saves object pointers
    with a UtHashMap. UtHashMap shrinks and expands pointers on a 64
    bit platform so less memory is used. This makes adding pointers
    that have been allocated on a stack or not aligned to a 8 byte
    boundary in a class impossible to add to the map. The shrink will
    assert saying that the last 3 hex digits are not zero. If mRange
    is an object here it won't be aligned to an 8 byte boundary on a
    64 bit platform and will cause the assertion.
  */
  ConstantRange* mRange;
}; // class CarbonPartsel : public CarbonUnaryOp


//! CarbonBinaryOp class
/*!
 * Binary operation.
 */
class CarbonBinaryOp : public CarbonOp
{
#if !pfGCC_2
  using CarbonExpr::castBinary;
#endif

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  /*!
    \param op The operator
    \param expr1 First expression argument
    \param expr2 Second expression argument
    \param bitSize size of the resultant expression
    \param isSigned is the result signed? (ignored if computeSigned is set)
    \param computeSigned compute the signedness from the operands
   */
  CarbonBinaryOp(CarbonExpr::ExprT op, CarbonExpr *expr1, CarbonExpr *expr2,
		 UInt32 bitSize, bool isSigned, bool computeSigned);

  //! destructor
  ~CarbonBinaryOp();

  //! CarbonExpr::castBinary()
  virtual const CarbonBinaryOp* castBinary() const;

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! Compare function so that we can do caching
  ptrdiff_t compare(const CarbonExpr* other) const;

  //! Return number of arguments to this operator.
  UInt32 getNumArgs() const { return 2;}

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  CarbonExpr *getArg(UInt32 index) const;

  //! CarbonExpr::evaluate()
  virtual SignT evaluate(ExprEvalContext* evalContext) const;

  //! CarbonExpr::assign()
  virtual AssignStat assign(ExprAssignContext* context);

  //! Return class name
  const char* typeStr() const;

protected:
  // The CarbonExprWalker has to be a friend to get to the private visit fn
  friend class CarbonExprWalker;

  // Visit routine used by the CarbonExprWalker to traverse an expression
  virtual void visit(CarbonExprWalker* exprWalker);

  //! CarbonExpr::composeHelper()
  virtual void composeHelper(ComposeContext* context) const;

private:
  //! Hide copy and assign constructors.
  CarbonBinaryOp(const CarbonBinaryOp&);
  CarbonBinaryOp& operator=(const CarbonBinaryOp&);

  //! Specialization used by composeHelper
  /*!
    Special composition function for bit-selects since they may
    require corrective arithmetic.
    
    \param context Tells the composer how to compose the expression.
  */
  void composeBitSel(ComposeContext* context) const;
  
  //! Shared by compose and composeC
  /*!
    Non-bitsel binary operator group begin.
  */
  void composePre(UtString* buf) const;
  //! Shared by compose and composeC
  /*!
    Non-bitsel binary operator group end. The postStr could either be
    a ')' or a ']', depending on the op type.
  */
  void composePost(UtString* buf, const char* postStr) const;
  //! Shared by compose and composeC
  /*!
    Compose the binary operator. Returns what the post group string
    should be.
  */
  const char* composeOp(UtString* buf) const;
  
  //! The left subexpression.
  CarbonExpr* mExpr1;

  //! The right subexpression.
  CarbonExpr* mExpr2;

  //! Sign of the binary op
  SignT mSign;
};


//! CarbonTernaryOp class
/*!
 * Ternary operation.
 */
class CarbonTernaryOp : public CarbonOp
{
#if !pfGCC_2
  using CarbonExpr::castTernary;
#endif

public: CARBONMEM_OVERRIDES
  //! constructor
  /*!
    \param op The operator
    \param expr1 First expression argument
    \param expr2 Second expression argument
    \param expr3 Third expression argument
    \param bitSize size of hte expression
    \param isSigned is the result signed? (ignored if computeSigned is set)
    \param computeSigned compute the signedness from the operands
   */
  CarbonTernaryOp(CarbonExpr::ExprT op, CarbonExpr *expr1, CarbonExpr *expr2,
		  CarbonExpr *expr3, UInt32 bitSize, bool isSigned,
                  bool computeSigned);

  //! destructor
  ~CarbonTernaryOp();

  //! CarbonExpr::castTernary()
  virtual const CarbonTernaryOp* castTernary() const;

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! Compare function so that we can do caching
  ptrdiff_t compare(const CarbonExpr* other) const;

  //! Return number of arguments to this operator.
  UInt32 getNumArgs() const { return 3;}

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  CarbonExpr *getArg(UInt32 index) const;

  //! Return class name
  const char* typeStr() const;

  //! CarbonExpr::evaluate()
  virtual SignT evaluate(ExprEvalContext* evalContext) const;

  //! CarbonExpr::assign()
  virtual AssignStat assign(ExprAssignContext* context);

  //! Get the enable for this expression if it has one (or NULL if it doesn't)
  CarbonExpr* getEnable(bool& cond) const;

protected:
  // The CarbonExprWalker has to be a friend to get to the private visit fn
  friend class CarbonExprWalker;

  // Visit routine used by the CarbonExprWalker to traverse an expression
  virtual void visit(CarbonExprWalker* exprWalker);

  //! CarbonExpr::composeHelper()
  virtual void composeHelper(ComposeContext* context) const;

private:
  //! Hide copy and assign constructors.
  CarbonTernaryOp(const CarbonTernaryOp&);
  CarbonTernaryOp& operator=(const CarbonTernaryOp&);

  //! The left subexpression.
  CarbonExpr* mExpr1;

  //! The middle subexpression.
  CarbonExpr* mExpr2;

  //! The right subexpression.
  CarbonExpr* mExpr3;
}; // class CarbonTernaryOp : public CarbonOp



//! CarbonNaryOp class
/*!
 * N-ary operation, this is an abstract class.
 */
class CarbonNaryOp : public CarbonOp
{
public: 
  CARBONMEM_OVERRIDES

  //! constructor
  /*!
    \param op The operator
    \param exprs The expressions, the vector will be copied.  0 is leftmost.
   */
  CarbonNaryOp(CarbonExpr::ExprT op, const CarbonExprVector* exprs,
	       UInt32 bitSize, bool isSigned);

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! Compare function so that we can do caching
  ptrdiff_t compare(const CarbonExpr* other) const;

  //! Return number of arguments to this operator.
  virtual UInt32 getNumArgs() const;

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  CarbonExpr *getArg(UInt32 index) const;

  //! destructor
  ~CarbonNaryOp();

  virtual AssignStat assign(ExprAssignContext* context);

protected:
  //! Vector of expressions, 0 is leftmost.
  CarbonExprVector* mExprs;

  // The CarbonExprWalker has to be a friend to get to the private visit fn
  friend class CarbonExprWalker;

  //! CarbonExpr::composeHelper()
  virtual void composeHelper(ComposeContext* context) const = 0;


  // Visit routine used by the CarbonExprWalker to traverse an expression
  virtual void visit(CarbonExprWalker* exprWalker);

  //! CarbonExpr::castNary()
  virtual CarbonNaryOp* castNary();
  
  //! CarbonExpr::castNary()
  virtual const CarbonNaryOp* castNary() const;

private:
  //! Hide copy and assign constructors.
  CarbonNaryOp(const CarbonNaryOp&);
  CarbonNaryOp& operator=(const CarbonNaryOp&);
};


//! CarbonConcatOp class
/*!
 * Concatenation operation.
 */
class CarbonConcatOp : public CarbonNaryOp
{
#if !pfGCC_2
  using CarbonExpr::castConcat;
#endif

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  /*!
    \param exprs The expressions, the vector will be copied.  0 is leftmost.
    \param repeat_count The number of times to repeat the expression.
    Must be at least 1.
    \param bitSize The size of the result
   */
  CarbonConcatOp(const CarbonExprVector *exprs, UInt32 repeat_count,
		 UInt32 bitSize, bool isSigned);

  //! destructor
  ~CarbonConcatOp();

  //! CarbonExpr::castConcat()
  virtual const CarbonConcatOp* castConcat() const;

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! Compare function so that we can do caching
  ptrdiff_t compare(const CarbonExpr* other) const;

  //! Return the number of times to repeat the expression, is at least 1.
  UInt32 getRepeatCount() const { return mRepeatCount; }

  //! CarbonExpr::evaluate()
  virtual SignT evaluate(ExprEvalContext* evalContext) const;

  //! CarbonExpr::assign()
  virtual AssignStat assign(ExprAssignContext* context);

  //! Return class name
  const char* typeStr() const;

  //! Virtual function to get any attribute strings
  const char* attrStr(UtString*) const;

protected:
  // The CarbonExprWalker has to be a friend to get to the private visit fn
  friend class CarbonExprWalker;

  // Visit routine used by the CarbonExprWalker to traverse an expression
  virtual void visit(CarbonExprWalker* exprWalker);

  virtual void composeHelper(ComposeContext* context) const;
private:
  //! Hide copy and assign constructors.
  CarbonConcatOp(const CarbonConcatOp&);
  CarbonConcatOp& operator=(const CarbonConcatOp&);

  //! Number of repetitions.
  UInt32 mRepeatCount;
};

//! CarbonUnaryOp class
/*!
 * Unary operation.
 */
class CarbonEdge : public CarbonExpr
{
#if !pfGCC_2
  using CarbonExpr::castEdge;
#endif

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  /*!
    \param clockEdge the sensitivity
    \param expr Expression argument
    \param bitsize bit size using verilog rules
   */
  CarbonEdge(ClockEdge clockEdge, CarbonExpr *expr, UInt32 bitSize);

  //! destructor
  ~CarbonEdge();

  //! CarbonExpr::castEdge()
  virtual const CarbonEdge* castEdge() const;

  //! Function to return the expression type
  virtual ExprT getType() const;

  //! Return class name
  const char* typeStr() const;

  //! Hash function so that we can do caching
  virtual size_t hash() const;

  //! Compare function so that we can do caching
  ptrdiff_t compare(const CarbonExpr* other) const;

  //! to match virtual function in base class, so icc doesn't complain.
  virtual CarbonExpr* getArg(UInt32 index) const;

  //! Return the specified argument; 0-based index, 0 is leftmost argument
  CarbonExpr *getArg() const;

  //! Return the clock edge
  ClockEdge getEdge() const;

  //! CarbonExpr::evaluate()
  virtual SignT evaluate(ExprEvalContext* evalContext) const;

  //! CarbonExpr::assign()
  virtual AssignStat assign(ExprAssignContext* context);

  //! Dump myself to cout
  virtual void print(bool recurse = true, int indent = 0) const;

protected:
  // The CarbonExprWalker has to be a friend to get to the private visit fn
  friend class CarbonExprWalker;

  // Visit routine used by the CarbonExprWalker to traverse an expression
  virtual void visit(CarbonExprWalker* exprWalker);

  //! Compose verilog syntax. Asserts on C
  virtual void composeHelper(ComposeContext* context) const;

  //! The subexpression.
  CarbonExpr* mExpr;

  //! The clock edge
  ClockEdge mClockEdge;

private:
  //! Hide copy and assign constructors.
  CarbonEdge(const CarbonEdge&);
  CarbonEdge& operator=(const CarbonEdge&);
};


//! CarbonExprWalker class
/*! This class allows walking of an expression in a generic way. This
 *  class should be derived from to create the various callback
 *  functions for the expression types.
 */
class CarbonExprWalker
{
public: CARBONMEM_OVERRIDES
  //! constructor
  CarbonExprWalker() {}

  //! destructor
  virtual ~CarbonExprWalker() {}

  //! Virtual function to visit an identity
  virtual void visitIdent(CarbonIdent*) { return; }

  //! Virtual function to visit a constant without X or Z
  virtual void visitConst(CarbonConst*) { return; }

  //! Virtual function to visit a constant with X and/or Z
  virtual void visitConstXZ(CarbonConstXZ*) { return; }

  //! Virtual function to visit a real-number constant
  virtual void visitConstReal(CarbonConstReal*) { return; }

  //! Virtual function to visit a part select (special case unary operator)
  /*! This function is called after the part select input expression
   *  is called.
   */
  virtual void visitPartsel(CarbonPartsel*) { return; }

  //! Virtual function to pre-visit a part select
  /*! This function is called before the part select input expression
   *  is called.
   *
   *  The function returns whether the traversal should continue or
   *  not
   */
  virtual bool preVisitPartsel(CarbonPartsel*) { return true; }

  //! Virtual function to pre-visit an identifier
  /*!
    This is called before the visit ident expression  is called.
    
    The function returns whehter the traversal should continue or not.
  */
  virtual bool preVisitIdent(CarbonIdent*) { return true; }

  //! Virtual function to visit a unary operator
  /*! This function is called after the unary op input expression
   *  is called.
   */
  virtual void visitUnaryOp(CarbonUnaryOp*) { return; }

  //! Virtual function to pre-visit a unary op
  /*! This function is called before the unary op input expression is
   *  called.
   *
   *  The function returns whether the traversal should continue or
   *  not
   */
  virtual bool preVisitUnaryOp(CarbonUnaryOp*) { return true; }

  //! Virtual function to visit a binary operator
  /*! This function is called after the binary op input expression is
   *  called.
   */
  virtual void visitBinaryOp(CarbonBinaryOp*) { return; }

  //! Virtual function to pre-visit a binary op
  /*! This function is called before the binary op input expressions
   *  are called.
   *
   *  The function returns whether the traversal should continue or
   *  not
   */
  virtual bool preVisitBinaryOp(CarbonBinaryOp*) { return true; }

  //! Virtual function to visit a ternary operator
  /*! This function is called after the ternary op input expressions
   *  are called.
   */
  virtual void visitTernaryOp(CarbonTernaryOp*) { return; }

  //! Virtual function to pre-visit a ternary op
  /*! This function is called before the ternary op input expressions
   *  are called.
   *
   *  The function returns whether the traversal should continue or
   *  not
   */
  virtual bool preVisitTernaryOp(CarbonTernaryOp*) { return true; }

  //! Virtual function to visit an nary operator - not implemented yet
  /*! This function is called after the nary op input expressions
   *  are called.
   */
  virtual void visitNaryOp(CarbonNaryOp* op)
  {
    CE_ASSERT("visitNaryOp not implemented" == NULL, op);
  }

  //! Virtual function to pre-visit a nary op
  /*! This function is called before the nary op input expressions
   *  are called.
   *
   *  The function returns whether the traversal should continue or
   *  not
   */
  virtual bool preVisitNaryOp(CarbonNaryOp* op)
  {
    CE_ASSERT("preVisitNaryOp not implemented" == NULL, op);
    return true;
  }

  //! Virtual function to visit a concat op (special case Nary op)
  /*! This function is called after the conat input expressions
   *  are called.
   */
  virtual void visitConcatOp(CarbonConcatOp*) { return; }

  //! Virtual function to pre-visit a concat op
  /*! This function is called before the concat op input expressions
   *  are called.
   *
   *  The function returns whether the traversal should continue or
   *  not
   */
  virtual bool preVisitConcatOp(CarbonConcatOp*) { return true; }

  //! Virtual function to visit an edge expression
  /*! This function is called after the edge expression input expression
   *  is called.
   */
  virtual void visitEdge(CarbonEdge*) { return; }

  //! Virtual function to pre-visit an edge expression
  /*! This function is called before the edge expression input expression is
   *  called.
   *
   *  The function returns whether the traversal should continue or
   *  not
   */
  virtual bool preVisitEdge(CarbonEdge*) { return true; }

  //! Function to visit the sub expressions of an expression
  void visitExpr(CarbonExpr* expr) { expr->visit(this); }
}; // class CarbonExprWalker

//! A class to walk an expression recursively and transform it
/*!
 * The following class allows us to walk an expression and transform
 * it. This is a base class (derived off the general expression walker
 * above) that should be derived from. Each derivation can choose to
 * overload one of the transform routines so that it doesn't do a simple
 * copy but a more elaborate operation.
 *
 * Each derivation must also implement the transformXXX functions
*/
class CarbonTransformWalker : public CarbonExprWalker
{
public: CARBONMEM_OVERRIDES
  //! constructor
  CarbonTransformWalker()
  {
    mExprs = new CarbonExprVector;
  }

  //! destructor
  ~CarbonTransformWalker()
  {
    INFO_ASSERT(mExprs->empty(), "Deleting walker before it completed the walk");
    delete mExprs;
  }

  //! Transform a constant into any carbon expression
  /*!
    This is default to doing nothing, simply because this only makes
    sense if we are transforming an expression from one factory to
    another.
  */
  virtual CarbonExpr* transformConst(CarbonConst* expr) { return expr; }
 
  //! Transform a constant with unknowns into any carbon expression
  /*!
    This is default to doing nothing, simply because this only makes
    sense if we are transforming an expression from one factory to
    another.
  */
  virtual CarbonExpr* transformConstXZ(CarbonConstXZ* expr) { return expr; }

  //! Transform a part select into any carbon expression
  virtual CarbonExpr* transformPartsel(CarbonExpr* expr, CarbonPartsel* partselOrig) = 0;

  //! Transform a unary op into any carbon expression
  virtual CarbonExpr* transformUnaryOp(CarbonExpr* expr, CarbonUnaryOp* unaryOrig) = 0;

  //! Transform a binary op into any carbon expression
  virtual CarbonExpr* transformBinaryOp(CarbonExpr* expr1, CarbonExpr* expr2,
                                        CarbonBinaryOp* binaryOrig) = 0;
  
  //! Transform a ternary op into any carbon expression
  virtual CarbonExpr* transformTernaryOp(CarbonExpr* cond,
                                         CarbonExpr* exprTrue, 
                                         CarbonExpr* exprFalse,
                                         CarbonTernaryOp* ternaryOrig) = 0;
  
  //! Transform a concat op into any carbon expression
  virtual CarbonExpr* transformConcatOp(CarbonExprVector* exprVec, 
                                        UInt32 repeatCount,
                                        CarbonConcatOp* concatOrig) = 0;

  //! Transform an edge expression into any carbon expression
  virtual CarbonExpr* transformEdge(CarbonExpr* expr, CarbonEdge* edgeOrig) = 0;

  virtual CarbonExpr* transformIdent(CarbonIdent* ident) = 0;

  //! function to visit an identity
  virtual void visitIdent(CarbonIdent* ident) 
  { 
    CarbonExpr* retExpr = transformIdent(ident);
    pushResult(retExpr);
  }

  //! function to visit a constant without X or Z
  void visitConst(CarbonConst* cConst) { 
    CarbonExpr* retExpr = transformConst(cConst);
    pushResult(retExpr); 
  }

  // function to visit a constant with X and/or Z  
  void visitConstXZ(CarbonConstXZ* cConstXZ) { 
    CarbonExpr* retExpr = transformConstXZ(cConstXZ);
    pushResult(retExpr); 
  }
  
  //! function to visit a part select (special case unary operator)
  virtual void visitPartsel(CarbonPartsel* partsel)
  {
    // Pop the sub expression off the stack
    CarbonExpr* expr = popExpr();

    // Create the new part select
    CarbonExpr* retExpr;
    retExpr = transformPartsel(expr, partsel);
    pushResult(retExpr);
  }

  //! function to visit a unary operator
  virtual void visitUnaryOp(CarbonUnaryOp* unary)
  {
    // Pop the sub expression off the stack
    CarbonExpr* expr = popExpr();

    // Create the new unary op
    CarbonExpr* retExpr = transformUnaryOp(expr, unary);
    pushResult(retExpr);
  }

  //! function to visit a binary operator
  virtual void visitBinaryOp(CarbonBinaryOp* binary)
  {
    // Pop the two sub expressions of the stack. They are in reverse order
    CarbonExpr* expr2 = popExpr();
    CarbonExpr* expr1 = popExpr();

    // Create the new binary op
    CarbonExpr* retExpr = transformBinaryOp(expr1, expr2, binary);
    pushResult(retExpr);
  }

  //! function to visit a ternary operator
  virtual void visitTernaryOp(CarbonTernaryOp* ternary)
  {
    // Pop the three sub expressions of the stack. They are in reverse order
    CarbonExpr* expr3 = popExpr();
    CarbonExpr* expr2 = popExpr();
    CarbonExpr* expr1 = popExpr();

    // Create the new ternary op
    CarbonExpr* retExpr = transformTernaryOp(expr1, expr2, expr3, ternary);
    pushResult(retExpr);
  }

  //! function to visit a concat op (special case Nary op)
  virtual void visitConcatOp(CarbonConcatOp* concat)
  {
    // Pop the N arguments off the stack in the right order
    UInt32 numArgs = concat->getNumArgs();
    UInt32 stackSize = mExprs->size();
    CE_ASSERT(stackSize >= numArgs, concat);
    CarbonExprVector exprVec;
    for (UInt32 i = stackSize - numArgs; i < stackSize; ++i)
    {
      CarbonExpr* expr = (*mExprs)[i];
      exprVec.push_back(expr);
    }
    for (UInt32 i = 0; i < numArgs; ++i)
      mExprs->pop_back();

    // Create the new op (concats are special in that they have a
    // repeat count)
    UInt32 repeatCount = concat->getRepeatCount();
    CarbonExpr* retExpr = transformConcatOp(&exprVec, repeatCount, concat);
    pushResult(retExpr);
  }

  //! function to visit an edge expression
  virtual void visitEdge(CarbonEdge* edge)
  {
    // Pop the sub expression off the stack
    CarbonExpr* expr = popExpr();

    // Create the new edge
    CarbonExpr* retExpr = transformEdge(expr, edge);
    pushResult(retExpr);
  }

  //! Function to get the result of elaboration
  CarbonExpr* getResult()
  {
    CarbonExpr* retExpr = popExpr();
    CE_ASSERT(mExprs->empty(), retExpr);
    return retExpr;
  }

protected:
  // Function to push the result of a copy operation on the stack
  void pushResult(CarbonExpr* expr) { mExprs->push_back(expr); }

  // Function to pop an expression off the stack
  CarbonExpr* popExpr()
  {
    CarbonExpr* retExpr = mExprs->back();
    mExprs->pop_back();
    return retExpr;
  }

private:
  // The transform walker local data
  CarbonExprVector* mExprs;
}; // class CarbonTransformWalker : public CarbonExprWalker

#endif // __Expr_h_
