// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _EXPRFACTORY_H_
#define _EXPRFACTORY_H_

#ifndef __Expr_h_
#include "exprsynth/Expr.h"
#endif
#ifndef _CARBON_HASH_VALUE_
#include "util/HashValue.h"
#endif
#ifndef __UtHashSet_h_
#include "util/UtHashSet.h"
#endif
#ifndef __UtHashMap_h_
#include "util/UtHashMap.h"
#endif
#ifndef __UtArray_h_
#include "util/UtArray.h"
#endif
#ifndef __STSYMBOLTABLE_H_
#include "symtab/STSymbolTable.h"
#endif

class ZistreamDB;
class ZostreamDB;
class ExprDBContext;
class DynBitVectorFactory;

//! Factory class to create expressions from design flow traversals
class ESFactory
{
public: 
  CARBONMEM_OVERRIDES

  //! constructor
  ESFactory();

  //! destructor
  ~ESFactory();

  //! Create an expression identifier (free variable)
  CarbonIdent* createIdent(CarbonIdent* ident, bool& added);

  //! Create a constant expression from a signed value
  CarbonConst* createConst(SInt64 val, UInt32 bitSize, bool isSigned);

  //! Create a constant expression for a dynamic bit vector
  CarbonConst* createConst(DynBitVector&, CarbonConst::SignT, UInt32);

  //! Create a constant expression with X and Z
  CarbonConstXZ* createConstXZ(DynBitVector& val, DynBitVector& drive,
			    CarbonConst::SignT sign, UInt32 bitSize);

  //! Create a Z expression of a given size
  CarbonConstXZ* createConstZ(UInt32 bitSize, bool isSigned);

  //! Create an X expression of a given size
  CarbonConstXZ* createConstX(UInt32 bitSize, bool isSigned);

  //! Create a constant of all ones of a given size
  CarbonConst* createConstAllOnes(UInt32 bitSize, bool isSigned);

  //! Create a real-number constant
  CarbonConstReal* createConstReal(double val, CarbonConst::SignT sign);

  //! Create a unary op
  CarbonUnaryOp* createUnaryOp(CarbonExpr::ExprT, CarbonExpr*, UInt32,
                               bool isSigned);

  //! Create a part select op (special case unary op with range)
  CarbonPartsel* createPartsel(CarbonExpr*, const ConstantRange&, UInt32,
                               bool isSigned);

  //! Create a binary op
  CarbonBinaryOp* createBinaryOp(CarbonExpr::ExprT, CarbonExpr*, CarbonExpr*,
				 UInt32, bool isSigned, bool computeSigned);

  //! Create a ternary op
  CarbonTernaryOp* createTernaryOp(CarbonExpr::ExprT, CarbonExpr*, CarbonExpr*,
				   CarbonExpr*, UInt32, bool isSigned,
                                   bool computeSigned);

  //! Create a concat op (special case nary op with repeat count)
  /*!
    \param expr the msb->lsb list of expressions to
    concat. expr->begin() should point to the leftmost element in the
    concat.
    \param repeatCount Number of times to repeat the concat
    \param bitSize Contextual size of the concat
    \param isSigned True if the context of the concat expression is
    signed.
  */
  CarbonConcatOp* createConcatOp(const CarbonExprVector* expr, 
                                 UInt32 repeatCount, UInt32 bitSize,
                                 bool isSigned);
  
  //! Create an edge expr
  CarbonEdge* createEdge(ClockEdge, CarbonExpr*, UInt32);

  //! Given an expression, pad it with sufficient zeros
  /*!
    This takes an expr, and does the following:
    \verbatim
    { (bitSize-1)'b0, expr}
    \endverbatim
  */
  CarbonExpr* createZeroPad (CarbonExpr* expr, UInt32 bitSize);

  //! Given an expression, create a != 0 expression
  /*!
    This takes an expression, expr, and does the following:
    \verbatim
    (expr != 0)
    \endverbatim
  */
  CarbonExpr* createNeqZero(CarbonExpr* expr, UInt32 bitSize);


  //! Given an expression, create a == 0 expression
  /*!
    This takes an expression, expr, and does the following:
    \verbatim
    (expr == 0)
    \endverbatim
  */
  CarbonExpr* createEqZero(CarbonExpr* expr, UInt32 bitSize);

  //! Given an expression, create logical equiv of == ~0
  /*!
    This takes an expression, expr, and does the following:
    \verbatim
    (expr == sizeof(expr)'b1)
    \endverbatim
  */    
  CarbonExpr* createEqAllOnes(CarbonExpr* expr, UInt32 bitSize);

  //! Given an expression, create logical equiv of != ~0
  /*!
    This takes an expression, expr, and does the following:
    \verbatim
    (expr != sizeof(expr)'b1)
    \endverbatim
  */    
  CarbonExpr* createNeqAllOnes(CarbonExpr* expr, UInt32 bitSize);

  //! Clear the factory
  /*! Do not call this if any of the expressions that are produced get
   *  stored.
   */
  void clear();

  //! Write an expression the db
  /*!
    \param dbWriteContext An instance of the ExprDBContext class. A
    design walker will be used to fill this object.
    \param expr The expression and underlying expressions, if any, to
    prep for db writing.
    \param inclusiveNodes If this is true then the symboltable in the
    dbWriteContext already contains all the symboltable nodes found in
    any CarbonIdents in the expression. If this is false, then all the
    nodes in the idents must be transformed and added to the local
    symbol table in the context. The latter would be for such things
    as the enable expression writer, which depends on names from the
    source vcd waveform, and not necessarily the design that was
    compiled.
  */
  void setupDBContext(ExprDBContext* dbWriteContext,
                      const CarbonExpr* expr,
                      bool inclusiveNodes,
                      STSymbolTable::LeafAssoc *leafAssoc);
  
private:
  // Hide copy and assign constructors.
  ESFactory(const ESFactory&);
  ESFactory& operator=(const ESFactory&);

  // Convert a SignT type based on an isSigned flag
  CarbonConst::SignT convertSign(CarbonConst::SignT sign, bool isSigned,
                                 const DynBitVector& val);
  CarbonConst::SignT convertSign(CarbonConst::SignT sign, bool isSigned,
                                 double val);


  // Types and data for a cache of expressions
  typedef HashPointerValue<CarbonExpr*> CmpExpr;
  typedef UtHashSet<CarbonExpr*, CmpExpr> ExprCache;
  typedef ExprCache::iterator ExprCacheIter;
  ExprCache* mExprCache;

}; // class ESFactory

//! An object that knows how to read/write expressions to a db
/*!
  In a writing context, this object can be used as is. In a reading
  context, this object must be derived from and the createIdent()
  and destroyIdent methods must be overridden. 
  The purpose of this class is to make expression writing
  index-based. All unique expressions within an expression get written
  to disk in order as added by addIfUnique(). The caller, then, can
  simply get the index of the expression to be written. Upon reading
  all the unique expressions get read, and the caller can do an
  indexed lookup for the resultant expression.

  For writing, this object should be used in conjunction with an
  expression walker to get the ordering to be causal. The ESFactory
  implements a walker for db writing. 

  \sa ESFactory::dbWriteExpr(), ESFactory::dbReadExpr()
*/
class ExprDBContext
{
public: 
  CARBONMEM_OVERRIDES

  //! Constructor, called by a writer
  /*!
    \param symtab This is populated with all the ident nodes
  */
  ExprDBContext(STSymbolTable* symtab, DynBitVectorFactory* bvPool);

  //! virtual destructor, due to override for read
  virtual ~ExprDBContext();

  //! If not alreadky, add this expression for writing
  void addIfUnique(CarbonExpr* expr);

  //! Get the index for the given expression
  /*!
    Normally, a caller would pass in the expression that was to be
    written in order to get the resultant index.
    
    \warning This will assert if the expression was not added.
  */
  UInt32 getIndex(const CarbonExpr* expr) const;

  //! Given an index, get the associated expression.
  CarbonExpr* getExpr(UInt32 index);

  //! Write the expressions to the db
  /*! 
    Writes all the added expressions from addIfUnique() to the db.
  */
  void writeExprs(ZostreamDB& db) const;

  //! Read the expressions from a db
  /*!
    \warning The factory must be non-null
  */
  void readExprs(ZistreamDB& db, ESFactory* factory);

  //! Get the passed in SymbolTable
  STSymbolTable* getLocalSymTab();

  //! Clears out the db context structure for reuse
  void clear();

  //! Reserve a bitvector pointer
  /*!
    This is used by the db writer to prime the factory.
  */
  void reserveBV(const DynBitVector& bv);
  
protected:
  
  //! Create a CarbonIdent 
  /*!
    By default this returns NULL. A reader should override this
    method.
    
    \param node The associated aliased leaf node for an ident
    \param bitSize The size of the ident
    \param usageMask The bits of the value that are used in identifier
    are marked as 1 in this vector.
  */
  virtual CarbonIdent* createIdent(STAliasedLeafNode* node, 
                                   UInt32 bitSize,
                                   const DynBitVector* usageMask,
                                   CbuildShellDB::CarbonIdentType type,
                                   const CarbonExprVector* nestedExprs);
  
  //! Destroy an identifier
  /*!
    This gets called if an identifier is found to be already in the
    factory.
    By default, this does nothing. The caller must override this in a
    reading context
  */
  virtual void destroyIdent(CarbonIdent* doomedIdent);

private:
  STSymbolTable* mSymTab;
  CarbonExprVector mUniqExprVec;
  typedef UtHashMap<CarbonExpr*, UInt32> ExprMap;
  ExprMap mUniqueExprs;

  DynBitVectorFactory* mBVPool;

  typedef UtArray<ConstantRange*> RangeVec;
  mutable RangeVec mRanges;

  struct ReadClosure
  {
    ReadClosure(ZistreamDB& db, ESFactory* factory) 
      : mDB(db),
        mOp(CarbonExpr::eStart),
        mBitSize(0),
        mFactory(factory),
        mIsSigned(false)
    {}
    
    ZistreamDB& mDB;
    CarbonExpr::ExprT mOp;
    UInt32 mBitSize;
    ESFactory* mFactory;
    UInt32 mIsSigned; // reading & writing bools is perilous due to platforms
  };

  void finalizeExpr(ReadClosure& closure, CarbonExpr* newExpr);

  void writeCommon(const CarbonExpr* expr, ZostreamDB& db) const;

  void readCommon(ReadClosure& closure);

  /*!
    This writes out the following information:

    -# The type of ident that this CarbonIdent is. The supported
    types are in CbuildShellDB.h, as CbuildShellDB::CarbonIdentType.
    -# The number of nested (or contained) expressions
    -# The given number of expression indices. You get these by
    calling ExprDBContext::getIndex() on each expression.
  */
  void writeIdent(const CarbonIdent* ident, size_t i, ZostreamDB& db) const;

  void readIdent(ReadClosure& closure);
  
  void writeConst(const CarbonConst* constExpr, ZostreamDB& db) const;

  void readConst(ReadClosure& closure);

  void writeUnary(const CarbonUnaryOp* unary, size_t i, ZostreamDB& db) const;

  void readUnary(ReadClosure& closure);

  void writeBinary(const CarbonBinaryOp* binary, size_t i, ZostreamDB& db) const;

  void readBinary(ReadClosure& closure);

  void writeTernary(const CarbonTernaryOp* ternary, size_t i, ZostreamDB& db) const;

  void readTernary(ReadClosure& closure);

  void writeConcat(const CarbonConcatOp* concat, size_t i, ZostreamDB& db) const;

  void readConcat(ReadClosure& closure);

  // A causality checker for writing, returns index of expr
  UInt32 checkExpr(CarbonExpr* expr, size_t i) const;

  ConstantRange* createConstantRange() const;

  //! Release memory used for writing to the database
  /*!
    \warning <em>DO NOT CALL</em> this \e BEFORE closing the database
    file. The database may be using some of these pointers in its
    pointer map.
  */
  void postDBClean();

};

//! Walk an expression and copy it.
/*!
  The following class allows us to walk an expression and copy
  it. This is a base class (derived off the general expression
  walker) that should be derived from. Each derivation can choose to
  overload one of the copy routines so that it doesn't do a simple
  copy but a more elaborate operation. 
  See \link PopulateExpr.cxx \endlink for some uses.
*/
class CopyWalker : public CarbonTransformWalker
{
public: CARBONMEM_OVERRIDES
  //! constructor
  CopyWalker(ESFactory* factory) : mFactory(factory) {}

  //! destructor
  ~CopyWalker() {}

  //! Transform a constant 
  virtual CarbonExpr* transformConst(CarbonConst* expr) { 
    DynBitVector val;
    expr->getValue(&val);
    return mFactory->createConst(val, expr->getSignInfo(), expr->getBitSize());
  }
 
  //! Transform a constant with unknowns 
  virtual CarbonExpr* transformConstXZ(CarbonConstXZ* expr) { 
    DynBitVector val, drv;
    expr->getValue(&val, &drv);
    return mFactory->createConstXZ(val, drv, expr->getSignInfo(), expr->getBitSize());
  }

  //! Transform a part select into any carbon expression
  CarbonExpr* transformPartsel(CarbonExpr* expr, CarbonPartsel* partselOrig)
  {
    ConstantRange range = partselOrig->getRange();
    UInt32 bitSize = partselOrig->getBitSize();
    return mFactory->createPartsel(expr, range, bitSize,
                                   partselOrig->isSigned());
  }

  //! Transform a unary op into any carbon expression
  CarbonExpr* transformUnaryOp(CarbonExpr* expr, CarbonUnaryOp* unaryOrig)
  {
    CarbonExpr::ExprT type = unaryOrig->getType();
    UInt32 bitSize = unaryOrig->getBitSize();
    return mFactory->createUnaryOp(type, expr, bitSize, unaryOrig->isSigned());
  }
  
  //! Transform a binary op into any carbon expression
  CarbonExpr* transformBinaryOp(CarbonExpr* expr1,
                                CarbonExpr* expr2,
                                CarbonBinaryOp* binaryOrig)
  {
    CarbonExpr::ExprT type = binaryOrig->getType();
    UInt32 bitSize = binaryOrig->getBitSize();
    return mFactory->createBinaryOp(type, expr1, expr2, bitSize,
                                    binaryOrig->isSigned(), false);
  }
  
  //! Transform a ternary op into any carbon expression
  CarbonExpr* transformTernaryOp(CarbonExpr* expr1,
                                 CarbonExpr* expr2, 
                                 CarbonExpr* expr3,
                                 CarbonTernaryOp* ternaryOrig)
  {
    CarbonExpr::ExprT type = ternaryOrig->getType();
    UInt32 bitSize = ternaryOrig->getBitSize();
    return mFactory->createTernaryOp(type, expr1, expr2, expr3, bitSize,
                                     ternaryOrig->isSigned(), false);
  }
  
  //! Transform a concat op into any carbon expression
  CarbonExpr* transformConcatOp(CarbonExprVector* exprs, UInt32 repeatCount,
                                CarbonConcatOp* concatOrig)
  {
    UInt32 bitSize = concatOrig->getBitSize();
    return mFactory->createConcatOp(exprs, repeatCount, bitSize,
                                    concatOrig->isSigned());
  }

  //! Transform an edge expr into any carbon expression
  CarbonExpr* transformEdge(CarbonExpr* expr, CarbonEdge* edgeOrig)
  {
    ClockEdge clockEdge = edgeOrig->getEdge();
    UInt32 bitSize = edgeOrig->getBitSize();
    return mFactory->createEdge(clockEdge, expr, bitSize);
  }
  
  //! The ident is created by the caller. cannot copy.
  CarbonExpr* transformIdent(CarbonIdent* ident) {
    return ident;
  }

protected:
  // The copy walker local data
  ESFactory* mFactory;
}; // class CopyWalker : public CarbonExprWalker

//! Utility expression mapping class for CarbonExpr consumers
/*!
  This is just a wrapper over a UtHashMap of a
  CarbonIdent->CarbonExpr, but with a nice interface so CarbonExpr
  walkers with different mapping systems can share code.
*/
class CarbonIdentExprMap
{
public:
  CARBONMEM_OVERRIDES

  //! Typedef for the map
  typedef UtHashMap<CarbonIdent*, CarbonExpr*> IdentExprMap;

  //! Typedef for unsorted constant looping
  typedef IdentExprMap::UnsortedCLoop UnsortedCLoop;

  //! Typedef for unsorted looping
  typedef IdentExprMap::UnsortedLoop UnsortedLoop;

  //! Swap this map with another
  void swap(CarbonIdentExprMap& other);

  //! Map an identifier to an expression
  void mapExpr(CarbonIdent* ident, CarbonExpr* expr);

  //! Remove the mapped expression associated with ident
  /*!
    This makes the net that the ident represents not expressioned
    anymore. 
  */
  void removeMappedExpr(CarbonIdent* ident);
  
  //! Given an existing ident, get the describing expression
  /*!
    Returns NULL if the ident is not found or if there is a NULL
    expression describing the ident
  */
  CarbonExpr* getExpr(CarbonIdent* ident);

  //! const version of getExpr
  const CarbonExpr* getExpr(const CarbonIdent* ident) const;

  //! (constant) Loop the expressions
  UnsortedCLoop loopCUnsorted() const;

  //! Loop the expressions
  UnsortedLoop loopUnsorted();

private:

  IdentExprMap mExprMap;
};

#endif // _EXPRFACTORY_H_
