// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef EXPRCALLBACKS_H_
#define EXPRCALLBACKS_H_

#include "exprsynth/ESPopulateExpr.h"

//! Callback for expression building.
/*!
  This callback instructs expression population to build an expression
  back to continuous drivers. An output of an always block will have
  an expression in terms of inputs to that always block, even if there
  are intermediate variables present.
 */
class ContinuousDriverSynthCallback : public ESCallback
{
public: CARBONMEM_OVERRIDES
  //! Constructor
  /*!
    \param populateExpr The populateExpr object
    \param start_flow   The flow node defining the root of our expression construction.
   */
  ContinuousDriverSynthCallback(ESPopulateExpr * populateExpr, FLNode * start_flow);

  //! Destructor
  virtual ~ContinuousDriverSynthCallback();

  //! Determine when to stop the expansion.
  /*!
    Stop the expression expansion whenever we encounter a continuous
    driver or encounter the starting flow more than once (meaning
    we're in a cycle).

    \param flow         The current flow node.
    \param isContDriver Is this a continuous driver?

    \return true when we reach a stopping point; either continuous
            driver or multiple visits to the starting points.
   */
  virtual bool stop(FLNode * flow, bool isContDriver);

  //! overload
  virtual CarbonExpr* createIdentExpr(const FLNodeVector& flows,
                                      UInt32 bitSize, bool isSigned,
                                      bool hardStop, NUNetRefHdl* resNetRef);

private:
  //! The starting flow.
  FLNode * mStartFlow;
  //! Have we already encountered the starting flow? 
  bool mStartFlowEncountered;
};


//! Callback for expression building.
/*!
  This callback instructs expression population to immediately stop
  expansion, causing the creation of an ident for the starting flow
  node.

  This is used to verify that the expression built with
  ContinuousDriverSynthCallback actually accomplished the expression
  construction.
 */
class IdentitySynthCallback : public ESCallback
{
public: CARBONMEM_OVERRIDES
  //! Constructor
  IdentitySynthCallback(ESPopulateExpr * populateExpr) :
    ESCallback(populateExpr)
  {}

  //! Destructor
  ~IdentitySynthCallback() {}

  //! Always stop the expansion.
  bool stop(FLNode * /*flow*/, bool /*isContDriver*/) {
    return true;
  }
};

//! Callback that implements elaborated expression caching
class ESCachedCallbackElab : public ESCallbackElab
{
public:
  CARBONMEM_OVERRIDES
  //! Constructor
  ESCachedCallbackElab(ESPopulateExpr* populateExpr) : 
    ESCallbackElab(populateExpr)
  {}

  //! destructor
  ~ESCachedCallbackElab() {}

  //! Override the create function to implement the cache
  ESExprHndl create(FLNodeElab* flowElab)
  {
    // Check the cache first
    Cache::iterator pos = mCache.find(flowElab);
    if (pos != mCache.end()) {
      return pos->second;
    }

    // Recursively create the expression
    ESExprHndl hndl = ESCallbackElab::create(flowElab);

    // Store the result in the cache and return it
    mCache.insert(Cache::value_type(flowElab, hndl));
    return hndl;
  }

  //! Function to clear the cache
  void clearCache(void) { mCache.clear(); }

private:
  //! Abstraction for the cache
  typedef UtMap<FLNodeElab*, ESExprHndl> Cache;

  //! Storage for the cache
  Cache mCache;
}; // class ESCachedCallbackElab : public ESCallbackElab

#endif
