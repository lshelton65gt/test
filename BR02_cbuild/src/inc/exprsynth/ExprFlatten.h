/***************************************************************************************
  Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef EXPR_FLATTEN_H
#define EXPR_FLATTEN_H
/* \file Implement expression flattening after port splitting.
 */
#include "exprsynth/Expr.h"
#include "util/UtSet.h"

class IODBNucleus;
class CbuildSymTabBOM;


//! Walk a concat expression, flattening the expression and checking for ident loops.
/*!  Walk the concat expression.  If the ident of the primary expression
  is ever discovered while walking the expression, it is an error and
  we'll abort.  While walking, each nested concat is flattened into the
  parent expression.  The final expression will be a new concat
  expression with no nesting.
*/
class ExprFlatten : public CarbonTransformWalker
{
public: CARBONMEM_OVERRIDES
  ExprFlatten(CbuildSymTabBOM* bomManager, CarbonIdent *ident, 
              CarbonConcatOp* expr);
  
  virtual ~ExprFlatten() {}

  virtual CarbonExpr* transformPartsel(CarbonExpr*, CarbonPartsel* partselOrig);
  virtual CarbonExpr* transformUnaryOp(CarbonExpr*, CarbonUnaryOp* unaryOrig);
  virtual CarbonExpr* transformBinaryOp(CarbonExpr* , CarbonExpr* ,
                                        CarbonBinaryOp* binaryOrig);
  virtual CarbonExpr* transformTernaryOp(CarbonExpr* ,
                                         CarbonExpr* , 
                                         CarbonExpr* ,
                                         CarbonTernaryOp* ternaryOrig);
  virtual CarbonExpr* transformEdge(CarbonExpr*, CarbonEdge* edgeOrig);

  //! If there are any nested ConcatOps, flatten them into their parent
  //ConcatOp
  virtual CarbonExpr* transformConcatOp(CarbonExprVector*,
                                        UInt32,
                                        CarbonConcatOp* concatOrig);

  //! Check for ident cycles
  virtual CarbonExpr* transformIdent(CarbonIdent* ident);

private:
  //! Recursively descend through any nested CarbonConcatOps and gather up
  // the flattened and expanded set of idents.
  /*! This is a private helper function that recursively expands the
   *  concat arguments into a local vector.  It descends into any ident
   *  that is represented by a concat expression and expands that set of
   *  concat arguments too.  When a given level is completely expanded,
   *  the elements of the local vector are placed into the result vector
   *  \<n\> times, where \<n\> is the repeat count of the concatOp in
   *  question.
   *  \param concat The concatOp to expand the arguments of
   *  \param flattenedConcatArgs The result vector containing the flattened args
   */
  void collectConcatArgs( CarbonConcatOp* concat,
                          CarbonExprVector &flattenedConcatArgs );

  CbuildSymTabBOM* mBOMManager;

  //! Keep a set of the idents that expand to concats.  To prevent
  //cycles we need to make sure that a given expression doesn't
  //reference any member of its ident set.
  typedef UtSet<CarbonIdent*> CarbonIdentSet;
  CarbonIdentSet mIdentSet;

  CarbonConcatOp *mExpr;
  CarbonExprVector mFlattenedConcatArgs;

  ExprFlatten();
  ExprFlatten(const ExprFlatten&);
  ExprFlatten& operator=(const ExprFlatten&);
};

#endif
