// -*-C++-*-
/*****************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef EXPR_REDUCE_H_
#define EXPR_REDUCE_H_

#include "exprsynth/Expr.h"
#include "util/UtHashSet.h"

/*!
  \file
  A CarbonExpr expression reducer for normalized expressions. 

  The unit test for this file is located in
  testsuite/exprreduce.cxx. It is run in test/unit-expr.
*/

//! A CarbonExpr expression reducer for normalized expressions
/*!
  A CarbonExpr expression reducer for normalized expressions. It
  doesn't yet do serious logical reductions, but it finds trivial
  expression constructs that can be simplified. Identifier and
  partselects must be normalized. There is no check for that.
  Currently, bit/partselects must not have oob accesses that can be
  analyzed statically. This does not expect to produce extra x's
  and/or z's.
  
  This currently reduces:
  -# bitsels of bitsels (primary bitsel index must be 0)
  -# partsel of bitsel (partsel must be 0:0)
  -# bitsel of partsel
  -# bitsel/partsel of concat

  You cannot override expression transforms. The only transform that
  a derived class can do is an ident replacement via replaceIdent().

  This also does cycle detection. The algorithm for the cycle
  detection is very simple. As idents are replaced, a walk of the
  replacement is done looking for any occurrences of the original
  ident. If the original ident is found, the reducer asserts.

  To run the reduction, call reduce().
*/
class ExprReduce : private CarbonTransformWalker
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  /*!
    \param exprFactory The expression factory
    \param replaced The ident that the expression we are going to walk
    replaces. This is used for cycle detection. This should be NULL if
    the expression does not replace another expression.
  */
  ExprReduce(ESFactory* exprFactory, CarbonIdent* replaced);

  virtual ~ExprReduce();

  //! Reduce the given expression
  /*!
    \retval Reduced form of reduceMe, if it can be reduced
    \retval reduceMe, if reduceMe cannot be reduced.
  */
  CarbonExpr* reduce(CarbonExpr* reduceMe);
  
protected:
  //! The expression factory
  ESFactory* mExprFactory;
  //! The replaced expression - can be NULL
  CarbonIdent* mTopIdent;

  //! Optionally replace an ident
  /*!
    If the ident is to be replaced with another expression, return the
    new expression. Otherwise, return ident. 

    \warning Do \e NOT return NULL.

    This defaults to returning ident.
  */
  virtual CarbonExpr* replaceIdent(CarbonIdent* ident);
  
  
private:
  
  //! Returns true if the expr is a constant with no x/z's.
  /*!
    The value of the constant is returned in val, if this returns
    true.
  */
  bool isKnownConstant(const CarbonExpr* expr, DynBitVector* val);
  
  virtual CarbonExpr* transformPartsel(CarbonExpr*, CarbonPartsel* partselOrig);

  //! No non-partsel unary transforms are done
  /*!
    We could do buffer removal here as well as detect if the
    expression is constant.
  */
  virtual CarbonExpr* transformUnaryOp(CarbonExpr*, CarbonUnaryOp* unaryOrig);
  
  virtual CarbonExpr* transformBinaryOp(CarbonExpr* , CarbonExpr* ,
                                        CarbonBinaryOp* binaryOrig);
  virtual CarbonExpr* transformTernaryOp(CarbonExpr* ,
                                         CarbonExpr* , 
                                         CarbonExpr* ,
                                         CarbonTernaryOp* ternaryOrig);
  virtual CarbonExpr* transformEdge(CarbonExpr*, CarbonEdge* edgeOrig);

  //! No concat reductions are done, currently.
  /*!
    We could potentially find repeated patterns and reduce that to a
    repeat count and the pattern, but that is not yet done.
  */
  virtual CarbonExpr* transformConcatOp(CarbonExprVector*,
                                        UInt32 repeatCnt,
                                        CarbonConcatOp* concatOrig);

  virtual CarbonExpr* transformIdent(CarbonIdent* ident);
  
  ExprReduce(const ExprReduce&);
  ExprReduce& operator=(const ExprReduce&);  
  
  class IdentCycleDetect;
};


class CarbonIdentExprMap;

//! Specialization for exprs with SymTabIdents
class ExprReduceSymTab : public ExprReduce
{
public:
  CARBONMEM_OVERRIDES

  //! Type for a set of CarbonIdents
  typedef UtHashSet<CarbonIdent*> IdentSet;
  
  //! Mode enumeration
  enum Mode { 
    eStandard, //! Standard expr reduction
    eThruBackPointer //! Substitute idents with backpointers
  };

  //! Constructor
  /*!
    \param identExprMap Map of idents to replacement expressions
    \param exprFactory Expression factory
    \param ident Source ident that is replaced by the expression that
    will be walked. This is for cycle detection
    \param mode Reduction mode
    \param replacedIdents Set of idents that were replaced in the
    ident transformation. This can be NULL.
  */
  ExprReduceSymTab(CarbonIdentExprMap* identExprMap, ESFactory* exprFactory, 
                   CarbonIdent *ident, Mode mode, IdentSet* replacedIdents);

  //! Virtual destructor
  virtual ~ExprReduceSymTab();
  
protected:

  //! If ident is mapped replace it
  /*!
    If we are in backpointer mode then the backpointer of the ident's
    expression is substituted.
  */
  virtual CarbonExpr* replaceIdent(CarbonIdent* ident);
  
private:
  CarbonIdentExprMap* mIdentExprMap;
  IdentSet* mReplacedIdents;
  Mode mMode;

};

#endif
