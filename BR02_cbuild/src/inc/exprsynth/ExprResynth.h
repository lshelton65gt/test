// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _EXPR_RESYNTH_H_
#define _EXPR_RESYNTH_H_

#include "util/CarbonTypes.h"
#include "util/Util.h"
#include "nucleus/Nucleus.h"
#include "flow/Flow.h"
#include "nucleus/NUExpr.h"     // for NUOp::OpT
#include "nucleus/NUCopyContext.h"

class ESFactory;
class ESExprHndl;
class ESPopulateExpr;
class ESExprHndl;
class CarbonExpr;
class SourceLocator;
class Fold;
class ExprResynthCallback;
class ArgProc;
class MsgContext;
class AtomicCache;
class NUExprFactory;
class IODBNucleus;
class LFElabStateTest;

//! class ExprResynth
/*! This class uses the Expression Synthesis package to transform
 *! an NUExpr* that may reference nets that are locally defined
 *! in always blocks, into a potentially more complex NUExpr*.
 *! where the nets are all continuously defined.  ExprResynth
 *! "owns" all the synthesized expressions, and all the factories
 *! and other classes needed to maintain them.
*/
class ExprResynth
{
public: CARBONMEM_OVERRIDES
  enum Mode {
    eStopAtState      = 1,
    eStopAtContDriver = 2,
    eStopAtScalar     = 4,
    eStopAfterFirst   = 8
  };
  
  //! ctor
  ExprResynth(Mode mode, ESPopulateExpr* populateExpr,
	      NUNetRefFactory* netRefFactory, ArgProc* args,
	      MsgContext* msgContext, AtomicCache* cache,
	      IODBNucleus* iodb);

  //! dtor
  ~ExprResynth();
  
  //! transform an expression that may contain "temp" nets into one
  //! that only contains continously driven elaborated nets.
  /*! The returned NUExpr* is allocated on behalf of the caller and
   *! must be freed by the caller.
   */
  const NUExpr* buildContinuousExpr(const NUExpr*, FLNodeElab*, NUModule* mod, FLNodeElab **last_flow_used);

  // Convert a CarbonExpr to a nucleus expression, in the context of a module
  const NUExpr* exprToNucleus(CarbonExpr* ce, NUModule* mod,
                              const SourceLocator& loc);

  const NUExpr* synthFlow(FLNodeElab* flow);

  NUExpr* binOp(CarbonExpr* ce, NUModule* mod,
                const SourceLocator& loc, NUOp::OpT op);
  NUExpr* unOp(CarbonExpr* ce, NUModule* mod,
                  const SourceLocator& loc, NUOp::OpT op);
  NUExpr* ternOp(CarbonExpr* ce, NUModule* mod,
                 const SourceLocator& loc, NUOp::OpT op);
  NUExpr* nOp(CarbonExpr* ce, NUModule* mod,
              const SourceLocator& loc, NUOp::OpT op);

  static FLNodeElab* findFlowForNet(FLNodeElab* flow, NUNetElab* net);

  void putMode(Mode mode);

  //! When constructing expressions, map alias to master.
  void mapNet(NUNetElab* alias, NUNetElab* master);

  //! Create NUExprs using the specified factory
  void putExprFactory(NUExprFactory*);

  typedef UtHashMap<NUNetElab*,NUNetElab*> NetMap;

  //! Clear the expression cache
  void clearExprCache();

private:
  NUExpr* e2n(CarbonExpr* ce, NUModule* mod,
              const SourceLocator& loc);

  // Put an expression into the factory, if there is one
  const NUExpr* record(NUExpr*);

  NUExpr* translateIdent(CarbonExpr* ce, const SourceLocator& loc);

  typedef UtHashMap<ESExprHndl, const NUExpr*, HashValue<ESExprHndl> > ExprCache;
  ExprCache* mExprCache;
  NetMap mNetMap;

  ESPopulateExpr* mPopulateExpr;
  Fold* mFold;
  ExprResynthCallback* mCallback;
  NUExprFactory* mExprFactory;
  bool mOwnExprFactory;
  CopyContext mCopyContext;
  NUNetRefFactory* mNetRefFactory;
  LFElabStateTest* mElabStateTest;
};

#endif // _EXPR_RESYNTH_H_
