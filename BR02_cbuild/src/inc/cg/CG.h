// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _CG_H_
#define _CG_H_

#include "cg/CGConfig.h"

#if NEXPR_ENABLE

// external declarations
class ArgProc;
class CarbonContext;
class MsgContext;
class NUDesign;

//! \class CG

class CG {
public:
  
  enum {
    eVERBOSE = 1<<0,                    //!< verbose code generation
    eDUMPLIR = 1<<1                     //!< dump the low-level IR
  };

  CG (CarbonContext *, MsgContext *msg_context, const UInt32 options = 0);
  virtual ~CG ();

  //! Configure the CG-specific options
  static void addCommandLineArgs (ArgProc *);

  //! Generate the C/C++ model
  bool generate (NUDesign *);

  // predicates
  bool isVerbose () const { return mFlags & eVERBOSE; }
  bool isDumpLIR () const { return mFlags & eDUMPLIR; }

private:

  UInt32 mFlags;                        //!< configuration options
  CarbonContext *mContext;
  MsgContext *mMsgContext;              //!< message sink

  //! Set configuration options from the command line
  void configure (const ArgProc &);

  //! Set configuration options from a string
  void configure (const char *s);

  //! Configure the model directory
  bool configureModelDirectory ();

  //! Infer all the various paths
  bool configureModelPaths (const ArgProc &);

  //! Construct an absolute path name into the model directory
  const char *makeModelFilename (UtString filename, UtString *pathname) const;

  UtString mTargetDirectory;            //!< directory to contain the compiled model
  UtString mTargetFile;                 //!< filename part of the compiled model path
  UtString mTargetName;                 //!< filename part without suffix
  UtString mTargetStem;                 //!< stem of the target name e.g. "design" for libdesign.a
  UtString mModelDirectory;             //!< name of the model directory

};

#endif
#endif
