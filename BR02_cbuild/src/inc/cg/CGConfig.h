// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*! \file
 *  This header exists only to define a value for CG_BURS_ENABLE and CG_IBURG_ENABLE.
 */

//! Set to enable the BURS-based low-level codegen
#define CG_BURS_ENABLE 0

//! Set to enable the iburg-based low-level codegen
#define CG_IBURG_ENABLE 0

//! Enable CG if either BURS or iburg CG is enabled
#define CG_ENABLE (CG_BURS_ENABLE||CG_IBURG_ENABLE)
