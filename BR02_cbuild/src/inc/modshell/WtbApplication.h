// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WTBAPPLICATION_H_
#define __WTBAPPLICATION_H_

#ifndef __UtApplication_h_
#include "util/UtApplication.h"
#endif
#ifndef __AtomicCache_h_
#include "util/AtomicCache.h"
#endif
#ifndef __HDLID_H_
#include "hdl/HdlId.h"
#endif
#ifndef __UtLicense_h_
#include "util/UtLicense.h"
#endif
#ifndef __UtLicenseMsg_h_
#include "util/UtLicenseMsg.h"
#endif
#ifndef __UtHashSet_h_
#include "util/UtHashSet.h"
#endif

class WtbVCDDumpContext;
class Stats;
class WtbModel;
class WfAbsFileReader;
class IODBRuntime;
class STSymbolTableNode;
class STAliasedLeafNode;
class MdsModel;
class CarbonSystemSim;

/*!
  \file
  The WtbApplication class.
*/

//! WtbApplication class
/*!
  This is the top level application class for the Carbon Wave
  Testbench.  The primary responsibility of this class is to
  handle command line processing and the top level control flow.
*/
class WtbApplication : public UtApplication
{
public:
  CARBONMEM_OVERRIDES

  //! Configuration object 
  class Config
  {
  public:
    CARBONMEM_OVERRIDES

    //! constructor
    Config(IODBRuntime *iodb, MsgContext* msgContext);
    //! Destructor
    ~Config(void);
    
    //! Enumeration for status
    enum Status {
      eOK, //!< success
      eError, //!< Error(s) generated
      eWarning //!< Warning(s) generated
    };

    //! Structure to hold enable information
    struct EnableInfo
    {
      CARBONMEM_OVERRIDES

      //! EnableInfo constructor
      EnableInfo(const STSymbolTableNode* enable, const HdlId& enInfo, bool inverted) :
        mEnable(enable), mEnableInfo(enInfo), mInvertEnable(inverted)
      {}

      //! The enable net
      const STSymbolTableNode* mEnable;
      //! The part of the enable net that is used.
      HdlId mEnableInfo;
      //! Polarity of enable, if true, !enable means model drives
      bool mInvertEnable;
    };
    
    //! Bidi to Enable map type def
    typedef UtHashMap<const STSymbolTableNode*, EnableInfo*> SigToEnableMap;
    //! Loop type for SigToEnableMap
    typedef SigToEnableMap::SortedLoop SigEnableLoop;
    
    typedef UtHashSet<const STSymbolTableNode*> NodeSet;
    
    //! Read the config file.
    /*!
      \param filename File with config information
      \retval eOK if successful and no errors or warnings detected
      \retval eError if an error occurred.
      \retval eWarning if a warning occurred.
    */
    Status readFile(const char* filename);
    
    //! Given a signal, get its enable. Returns NULL if not found
    /*!
      \param signal The signal for needing the enable.
      \param enableInfo The bitrange/scalar/vector info of the enable for
      the signal. This must be a pointer to an instance. The actual
      value will be copied to this location.
      \param inverted If the the enable expression is inverted, this
      will be true.
      \returns the node for the enable net. NULL if not found.

      \warning The HdlId info is based on user input. So, if the HdlId
      signifies a scalar, it may just mean that the user didn't
      specify a range, in which case the entire identifier must be used.
    */
    const STAliasedLeafNode* getEnable(STSymbolTableNode* signal, HdlId* enableInfo, bool* inverted) const;
    
    //! Loop the signals and their enable expressions
    SigEnableLoop loopEnabledSignals() const;
    
    //! User specified as a clock?
    bool isClock(const STSymbolTableNode* node) const;
    //! User specified as data (non-clock)?
    bool isData(const STSymbolTableNode* node) const;
    //! User specified to ignore as input?
    bool isIgnore(const STSymbolTableNode* node) const;
    
  private:
    const STSymbolTableNode* lookupDesignSignal(const UtString &signal) const;
    const STSymbolTableNode* lookupEnableSignal(const UtString &signal);

    void maybePrependCarbonTopLevel(UtStringArray* ids) const;
    bool checkTypeExclusivity(const STSymbolTableNode* node, 
                              const char* desiredType) const;

  private:
    IODBRuntime* mIODB;
    MsgContext* mMsgContext;

    SigToEnableMap mEnables;
    NodeSet mIgnore;
    NodeSet mClocks;
    NodeSet mDataInputs;
    
  private:
    Config(void);
    Config(const Config&);
    Config& operator=(const Config&);
  };

 public:
  //! Constructor.
  /*!
    Construct a top level application object.
    \param argc Number of command line arguments.
    \param argv Array of command line arguments.
    \param cleanMemory If false then most of the memory used by the
    test bench will not be freed prior to exiting. This is for
    production mode. In debug mode we will check for leaks.
  */
  WtbApplication (int* argc, char **argv, bool cleanMemory);

  //! Destructor.
  virtual ~WtbApplication(void);

  //! Run the application.
  /*!
    Run the application.
    \return The exit status of program.
  */
  virtual SInt32 run();

  //! Sets up the application
  /*
    \param progName the invocation name, used in a licensing error message
   */
  SInt32 init( const char *progName );

  //! Get the MdsModel
  MdsModel* getModel();

  //! Register command line options.
  /*!
    Register the command line options.
  */
  void setupCommandLineOptions(void);

  //! check out a cwavetestbench license
  /*
    \param progName the invocation name, used in a licensing error message
    \returns true if a license was obtained, false otherwise
   */
  bool checkoutLicense( const char *progName );

  //! Parse command line options.
  /*!
    Parse the command line.  Verify that required options are present
    and that only legal combination occur.
    \retval true The command line is legal.
    \retval false The command line is not legal.
  */
  bool parseCommandLine(void);

  //! Print usage message.
  /*!
    Print usage message.  This is usually done when command line
    processing has failed.
  */
  void printUsageMessage(void);

  //! Destroy the stats object causing final statistics to be printed.
  void destroyStats(void);

  //! Arg option to treat all deposits as clocks.
  /*!
    This used to be cwave's default behavior. Now, deposits are
    separated into data and clocks, so that the data signals are
    run after the clocks when running no-input-flow vectors.
  */
  static const char* scAllDepositablesClocks;

  //! Returns true if replay is active
  bool isReplayEnabled() const;

  //! Returns true if OnDemand is active
  bool isOnDemandEnabled() const;

 private:
  //! Return the value of the -basePath option.
  bool getBasePath(UtString * base_path);

  //! Return the value of the -n option.
  UInt32 getNumberOfSimulations(void);

  //! Return the value of the -iodbfile option.
  bool getIODBFilename(UtString *iodb_filename);

  //! Indicate whether the user has requested VCD dumping.
  bool isDumpingVCD(void);

  void createVCDDumpContext(void);
  void destroyVCDDumpContext(void);
  void createModel();
  void destroyModel(void);
  bool createWavefile(void);
  void destroyWavefile(void);
  void createStats(void);
  void findCollapseClocks(void);

  //! Create the config file object, if applicable
  /*!
    This will check if the user specified a config file on the command
    line. This will return true if the user did specify the config
    file and false if he did not.
   
    The config file will be read. If any errors are encountered the
    config object (mConfig) will be deleted and NULLed.
    Warnings are ignored.
  */
  bool maybeCreateConfig(void);

 private:

  struct WtbLicCB : public virtual UtLicense::MsgCB
  {
    CARBONMEM_OVERRIDES

    WtbLicCB(MsgContext* msgContext);
    virtual ~WtbLicCB();
    virtual void waitingForLicense(const char* feature);
    virtual void queuedLicenseObtained(const char* feature);

    virtual void requeueLicense(const char* featureName);
    virtual void relinquishLicense(const char* featureName);
    virtual void exitNow(const char* reason);

    MsgContext* mMsgContext;
  };

  WtbModel *mModel;
  Stats *mStats;
  WtbVCDDumpContext *mVCDDumpContext;
  AtomicCache mStringCache;
  WtbLicCB mLicCB;
  UtLicense mLicense;
  CarbonSystemSim* mReplaySystem;
  bool mCleanMemory;
  
private:
  WtbApplication(void);
  WtbApplication(const WtbApplication&);
  WtbApplication& operator=(const WtbApplication&);
};


#endif
