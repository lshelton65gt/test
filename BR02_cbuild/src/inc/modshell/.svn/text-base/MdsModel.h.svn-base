// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#ifndef __MDSMODEL_H_
#define __MDSMODEL_H_

#ifndef __carbon_shelltypes_h_
#include "shell/carbon_shelltypes.h"
#endif
#ifndef __WFVCD_H_
#include "waveform/WfVCD.h"
#endif

class MsgStreamIO;
class CarbonNet;
class STSymbolTable;
class STSymbolTableNode;
class HdlHierPath;
class ShellSymTabBOM;
class IODBRuntime;
class AtomicCache;
class MsgContext;
class SCHScheduleFactory;
class WfFsdbFileReader;
class ESFactory;
class STAliasedLeafNode;
class DynBitVectorFactory;
class UtUInt64Factory;
class WfAbsControl;

/*!
  \file
  The MdsModel class.
*/


//! MdsModel class
/*!
  This class provides a unified object interface to the Carbon
  model.  It takes care of the work of bringing together the
  CarbonNet interface, the Carbon structure interface and the
  IODB interface.

  \note When the new Carbon C++ PI is implemented, this should
  disappear.
*/
class MdsModel
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct a model object.
    \param iodb_filename  The name of the iodb file this object
    will use.
   */
  MdsModel(const UtString &iodb_filename);
  
  //! Destructor.
  virtual ~MdsModel(void);

  //! typedef for MdsModel init fn
  typedef void (*UserInitFn)(CarbonObjectID *, void**);
  //! typedef for destroy fn
  typedef void (*DestroyFn)(CarbonObjectID *, void*);
  //! typedef for pre and post sched fn
  typedef void (*ScheduleFn)(CarbonObjectID*, void*, CarbonTime);
  //! typedef for fsdb init fn
  typedef CarbonWaveID* (*FSDBWaveInitFn)(CarbonObjectID* context, 
                                          const char* fileName, 
                                          CarbonTimescale timescale);
  
  typedef WfFsdbFileReader* (*FSDBReadCreateFn)(const char* fileName,
                                                AtomicCache* strCache,
                                                DynBitVectorFactory* 
                                                dynFactory,
                                                UtUInt64Factory* timePtrs,
                                                WfAbsControl* control);

  //! Call the pre schedule function
  void preSchedule(CarbonTime sim_time);

 
  //! Call the model's schedule.
  /*!
    Call the model's schedule.
    \param sim_time This is the time the schedule should be 
    updated to.
   */
  CarbonStatus schedule(CarbonTime sim_time);

  //! Call the model's clock schedule.
  /*!
    Call the model's clock schedule.
    \param sim_time This is the time the schedule should be 
    updated to.
   */
  CarbonStatus clkSchedule(CarbonTime sim_time);

  //! Call the model's data schedule.
  /*!
    Call the model's data schedule.
    \param sim_time This is the time the schedule should be 
    updated to.
   */
  CarbonStatus dataSchedule(CarbonTime sim_time);

  //! Does the model allow fsdb writing?
  bool allowFsdbWrite() const;

  //! Find the named net.
  /*!
    This finds the named net.  This net has to exist
    in the IODB.  The user is responsible for deleteing
    the returned net.
    \param net_name The name of the net.
    \retval !0 A pointer to a new CarbonNet.
    \retval 0 The net could not be found.
   */
  CarbonNet * findNet(const UtString & net_name);

  //! Find the net associated with the node.
  /*!
    This finds the net that has the same name as the
    specified node.  This net has to exist in the IODB. 
    The user is responsible for deleteing the returned net.
    \param net_name The name of the net.
    \retval !0 A pointer to a new CarbonNet.
    \retval 0 The net could not be found.
   */
  CarbonNet * findNet(const STSymbolTableNode * node);

  //! Get time.
  /*!
    Get the model's current simulation time.
   */
  CarbonTime getTime(void);


  //! Is this signal a clock?
  bool isClock(STSymbolTableNode* node);

  //! Is this signal a primary input?
  bool isInput(STSymbolTableNode* node);

  //! Is this signal drive synchronous data?
  /*!
    This predicate returns true if it drives
    sequential logic before reaching primary
    outputs.
   */
  bool isSyncData(STSymbolTableNode* node);

  //! Is this signal drive asynchronous data?
  /*!
    This predicate returns true if it drives
    only combinational logic before reaching
    primary outputs.
   */
  bool isAsyncData(STSymbolTableNode* node);

  //! Is this signal a reset?
  bool isReset(STSymbolTableNode* node);

  //! Is this signal a posedge reset?
  bool isPosedgeReset(STSymbolTableNode* node);

  //! Is this signal a negsedge reset?
  bool isNegedgeReset(STSymbolTableNode* node);

  //! Specify VCD file and time for dumping.  
  /*!
    Specify the filename and time scales for VCD
    dumping. This method is analagous to the Verilog
    system function $dumpfile.
   */
  CarbonStatus dumpFile(const UtString &filename, WfVCD::TimescaleUnit t_unit,
                        WfVCD::TimescaleValue t_value, 
                        CarbonWaveFileType waveType = eWaveFileTypeVCD);

  //! Specify scopes and level of hierarchy to dump
  /*!
    Specify the scopes and depths for which VCD will
    be dumped. This method is analagous to the Verilog
    system function $dumpall.
   */
  CarbonStatus dumpVars(UInt32 depth, const UtString &scopes);

  //! Same as dumpVars(), but dump only state and primary i/o
  CarbonStatus dumpStateIO(UInt32 depth, const UtString &scopes);
  
  //! Checkpoint all variables in VCD file.
  /*!
    Create a checkpoint of all variables that have
    been marked for VCD dumping. This method is analagous
    to the Verilog system function $dumpall.
   */
  CarbonStatus dumpAll(void);

  //! Turn VCD dumping on.
  /*!
    Turn VCD dumping on.  This method is analagous
    to the Verilog system function $dumpon.
   */
  CarbonStatus dumpOn(void);

  //! Turn VCD dumping off.
  /*!
    Turn VCD dumping off.  This method is analagous
    to the Verilog system function $dumpoff.
   */
  CarbonStatus dumpOff(void);

  //! Flush VCD dumping buffers.
  /*!
    Flush the VCD dumping buffers.  This method is analagous
    to the Verilog system function $dumpflush.
   */
  CarbonStatus dumpFlush(void);

  //! Get the CarbonModel context
  CarbonObjectID* getModelContext() {
    const MdsModel* me = const_cast<const MdsModel*>(this);
    return const_cast<CarbonObjectID*>(me->getModelContext());
  }

  const CarbonObjectID* getModelContext() const;

  //! Called by application to run a registered destroy callback
  void callUserDestroy();

  //! Get the local symbol table 
  STSymbolTable* getSymTab() {
    return mSymbolTable;
  }

  bool hasValidCarbonNet(const STAliasedLeafNode* node) const;

  void createIODB();

  IODBRuntime* getIODBRuntime() {
    const MdsModel* me = const_cast<const MdsModel*>(this);
    return const_cast<IODBRuntime*>(me->getIODBRuntime());
  }
  
  const IODBRuntime* getIODBRuntime() const {
    return mIODB;
  }

  void maybeCallUserInit();

  const char* getIODBFilename() const;
  void putIODBFilename(const char* name);

  void putFnInfo(CarbonObjectID* context,
                 UserInitFn initFn,
                 DestroyFn destroyFn,
                 ScheduleFn preSchedFn,
                 ScheduleFn postSchedFn,
                 FSDBWaveInitFn dumpFileFn,
                 FSDBReadCreateFn fsdbReadCreateFn,
                 bool allowFsdbWrite);

  FSDBReadCreateFn  getFsdbReadCreateFn();

  bool isDumpingWave() const;

 protected:
  ShellSymTabBOM* mShellBOM;
  IODBRuntime *mIODB;
  void* mUserData;
  UtString mIODBFilename;
  ESFactory* mExprFactory;
  CarbonObjectID *mContext;

 private:
  void destroyIODB(void);

 private:
  CarbonWaveID* mWaveContext;
  
  STSymbolTableNode * lookupSignal(const UtString &signal) const;
  void printSignalType(STSymbolTableNode *) const;


 private:
  CarbonTimescale convertWfTimescaleToCarbonTimescale(WfVCD::TimescaleUnit t_unit, WfVCD::TimescaleValue t_value);

 private:
  typedef UtHashMap<const STSymbolTableNode *, CarbonNet *> NodeToSignalMap;

 private:
  const UtString mFilename;
  AtomicCache *mAtomicCache;
  MsgStreamIO *mErrorStream;
  MsgContext *mMsgContext;
  SCHScheduleFactory *mScheduleFactory;
  HdlHierPath* mHdlPath;
  STSymbolTable* mSymbolTable;
  UserInitFn mUserInitFn;
  DestroyFn mDestroyFn;
  ScheduleFn mPreSchedFn;
  ScheduleFn mPostSchedFn;
  FSDBWaveInitFn mFSDBInitFn;
  FSDBReadCreateFn mFSDBReadCreateFn;

  NodeToSignalMap* mNodeToSignalMap;  
  bool mAllowFsdbWrite;  

 private:
  MdsModel(void);
  MdsModel(const MdsModel&);
  MdsModel& operator=(const MdsModel&);
};


#endif
