// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __MDSGENERATOR_H_
#define __MDSGENERATOR_H_


#include "util/UtIOStream.h"
#include "iodb/IODB.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"

class MsgContext;
class STSymbolTableNode;


/*!
  \file
  The MdsGenerator class.
*/

//! MdsGenerator class
/*!
  This class is used by applications that will want
  to use an MdsModel.  This class handles the chores
  involved with the dynamic code generation portion
  of creating a MdsModel.
*/
class MdsGenerator
{
 public:
  //! Constructor.
  /*!
    This constructor gathers all of the information that will
    be required to generate a new model.
    \param model_name The name of the model.  This is usually "design".
    \param install_dir The Carbon installation directory.
    \param target_dir The directory that the generated code should be written to.
    \param iodb The IODB that has the model information in it.
    \param config Configuration information from the command line and configuration files.
    \param message_context Message context.
   */
  MdsGenerator(const UtString &model_name, const UtString &install_dir, const UtString &target_dir, MsgContext *message_context, const char* presched = NULL, const char* postsched = NULL, const char* userInit = NULL, const char* userDestroy = NULL, bool allowFsdbWrite = false, bool allowFsdbRead = false);

  //! Destructor.
  ~MdsGenerator(void);

  //! Generate the code for a new model.
  void generate(void);

 private:
  typedef UtHashSet<STSymbolTableNode *> NodeSet;

 private:
#if 0
  void generateFindNetWorker(std::ostream &out, IODB::NameSetLoop loop, NodeSet *processed_nodes);
  void generateIsEnableWorker(std::ostream &out);
  void generateSetEnablesWorker(std::ostream &out);
  void generateSetEnablesWorker(std::ostream &out, IODB::NameSet *ns);
#endif

  SInt32 getSignalType(STSymbolTableNode *node) const;
  UtString mModelName;
  UtString mInstallDir;
  UtString mTargetDir;
  //  IODB &mIODB;
  //MdsConfig &mConfig;
  MsgContext *mMsgContext;
  const char* mPreSched;
  const char* mPostSched;
  const char* mUserInit;
  const char* mUserDestroy;
  bool mAllowFsdbWrite;
  bool mAllowFsdbRead;

 private:
  MdsGenerator(void);
  MdsGenerator(const MdsGenerator&);
  MdsGenerator& operator=(const MdsGenerator&);
};


#endif
