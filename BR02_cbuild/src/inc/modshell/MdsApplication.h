// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __MDSAPPLICATION_H_
#define __MDSAPPLICATION_H_


#include "util/UtApplication.h"
#include "iodb/IODBRuntime.h"
#include "exprsynth/ExprFactory.h"
#include <iostream>

class HdlHierPath;
class MdsConfig;
class MsgStreamIO;
class STSymbolTable;
class ShellSymTabBOM;
class UtLicense;


/*!
  \file
  The MdsApplication class.
*/

//! MdsApplication class
/*!
  This class provides features that need to access an
  IODB and that want to link together an executable
  based on a Carbon model.
*/
class MdsApplication : public UtApplication
{
 public:
  //! Constructor.
  /*!
    Construct a top level application object.
    \param argc Number of command line arguments.
    \param argv Array of command line arguments.
   */
  MdsApplication (int* argc, char **argv);

  //! Destructor.
  virtual ~MdsApplication(void);

  //! Run the application.
  /*!
    Run the application.
    \return The exit status of program.
  */
  virtual SInt32 run(void) = 0;

  //! Get the name of the IODB file.
  /*!
    Get the name of the IODB file.  The default is "libdesign.io.db".
    \retval true The filename is non-empty.
    \retval false The filename is empty.
  */
  bool getIODBFilename(UtString *iodb_filename);

  //! Get the name of the pre-schedule function
  /*!
    The default is no default.
    \retval true The name is non-empty.
    \retval false The name is empty.
  */
  bool getPreSchedFnName(UtString *fnName);


  //! Get the name of the post-schedule function
  /*!
    The default is no default.
    \retval true The name is non-empty.
    \retval false The name is empty.
  */
  bool getPostSchedFnName(UtString *fnName);

  //! Get the name of the user init function
  /*!
    The default is no default.
    \retval true The name is non-empty.
    \retval false The name is empty.
  */
  bool getUserInitFnName(UtString *fnName);

  //! Get the name of the user destroy function
  /*!
    The default is no default.
    \retval true The name is non-empty.
    \retval false The name is empty.
  */
  bool getUserDestroyFnName(UtString *fnName);

 protected:
  virtual bool parseCommandLine(void);
  virtual void setupCommandLineOptions(void);
  virtual bool checkCommandLineOptions(void);

  void createIODB(void);
  void destroyIODB(void);
  
  void destroyConfig(void);
  void linkExecutable(const char* exec_name, const char* lib_name, 
    TargetCompiler compiler, TargetCompilerVersion compiler_version, 
    const char* main_src, bool allowFsdbWrite, bool allowFsdbRead);

 private:
  //! Print usage message.
  /*!
    Print usage message.  This is usually done when command line
    processing has failed.
  */
  void printUsageMessage(void);

  bool callbackEnabled() const;

protected:
  UtString mModelName;
  UtString mTargetDir;
  ESFactory mExprFactory;
  ShellSymTabBOM* mShellBOM;
  IODBRuntime *mIODB;

  //! Doc Section Name
  static const char* scInputControl;
  //! Doc Section Name
  static const char* scFlowControl;
  //! Doc Section Name
  static const char* scOutputControl;

protected:
  AtomicCache *mAtomicCache;
  SCHScheduleFactory *mScheduleFactory;
  STSymbolTable* mSymbolTable;

  void generateCode(void);
  void generateCodeWorker(std::ostream &out, IODB::NameSetLoop loop);
  SInt32 getSignalType(STSymbolTableNode *node) const;
  
 private:
  
  class LicCB;
  LicCB* mLicCB;
  UtLicense* mLicense;

  MdsApplication(void);
  MdsApplication(const MdsApplication&);
  MdsApplication& operator=(const MdsApplication&);
};

#endif
