// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  Class to hold information used by schedule dump routines
*/

#ifndef _DUMP_H_
#define _DUMP_H_

//! Use for gathering statistics on combinational block scheduling
#define SCH_HISTOGRAM_SIZE	10

class HierName;

//! SCHDump class
class SCHDump
{
private:
  // Used to gather information on a schedule
  struct ScheduleMaskStats
  {
    UInt32	sequentialBlocks;
    UInt32	transitionComboBlocks;
    UInt32	sampleComboBlocks;
    UInt32	derivedSequentialBlocks;
    UInt32	derivedComboBlocks;
    UInt32	inputComboBlocks;
    UInt32	asyncComboBlocks;
  };

public:
  typedef UtHashMap<const SCHScheduleMask*, ScheduleMaskStats> ScheduleMaskTable;
  typedef UtMap<SCHScheduleBase*, UInt32> BlockInstCalls;
  typedef UtMap<HierName*, BlockInstCalls> BlockCalls;
  typedef UtHashMap<const NUUseDefNode*, BlockCalls> BlockStats;

private:
  

  struct AllScheduleStats
  {
    ScheduleMaskTable	maskTable;
    UInt32		totalBlocks;
    UInt32		totalDerivedClockLogicBlocks;
    UInt32		totalSequentialBlocks;
    UInt32		totalStateUpdateBlocks;
    UInt32		totalComboBlocks;
    BlockStats		sequentialBlocks;
    BlockStats		combinationalBlocks;
  };
  friend struct AllScheduleStats;

public:
  //! constructor
  SCHDump(SCHSchedule* sched, SCHScheduleFactory* factory, SCHMarkDesign* mark,
	  SCHUtil* util)
  {
    mSampleNodes = 0;
    mTransitionNodes = 0;
    mMustBeTransitionNodes = 0;
    mDrivenByClockNodes = 0;
    mMustBeAccurateNodes = 0;
    mLatchNodes = 0;
    mCycleNodes = 0;
    mLevelMemoryNodes = 0;
    mFeedsOutputPinNodes = 0;

    mClkStateUpdates = 0;
    mCycleStateUpdates = 0;

    mSched = sched;
    mFactory = factory;
    mMarkDesign = mark;
    mUtil = util;
  }

  //! destructor
  ~SCHDump() {}

  //! Abstraction to print multi schedule combinational blocks
  typedef UtVector<SCHCombinational*> CombSchedules;

  //! Function do dump all combinational blocks in multiple schedules
  void dumpMultiScheduleBlocks(FLNodeElab* flow, CombSchedules*);

  //! Function to increment the number of sample nodes
  void incrSampleNodes() { ++mSampleNodes; }

  //! Function to increment the number of transition nodes
  void incrTransitionNodes() { ++mTransitionNodes; }

  //! Function to increment the number of mustBeTransition nodes
  void incrMustBeTransitionNodes() { ++mMustBeTransitionNodes; }

  //! Function to increment the number of drivenByClockNodes nodes
  void incrDrivenByClockNodes() { ++mDrivenByClockNodes; }

  //! Function to increment the number of mustBeAccurateNodes nodes
  void incrMustBeAccurateNodes() { ++mMustBeAccurateNodes; }

  //! Function to increment the number of latch nodes
  void incrLatchNodes() { ++mLatchNodes; }

  //! Function to increment the number of cycle nodes
  void incrCycleNodes() { ++mCycleNodes; }

  //! Function to increment the number of levelMemory nodes
  void incrLevelMemoryNodes() { ++mLevelMemoryNodes; }

  //! Function to increment the number of nodes that feed output pins
  void incrFeedsOutputPinNodes() { ++mFeedsOutputPinNodes; }

  //! Function to store the number of state updates due to different clocks
  void putClkStateUpdates(UInt32 clkStateUpdates)
  { 
    mClkStateUpdates = clkStateUpdates;
  }

  //! Function to store the number of state updates due to cycles
  void putCycleStateUpdates(UInt32 cycleStateUpdates)
  { 
    mCycleStateUpdates = cycleStateUpdates;
  }

  //! Prints the schedule statistics
  void printScheduleStats(const char* fileRoot, bool output);

  //! Debug routine to print a combinational schedule
  void printCombSchedule(SCHCombinational* comb);

  //! Prints the schedules
  void print() const;

  //! Helper function to create a name from a flow block
  static void composeFlowName(const FLNodeElab* flow, UtString* name);

  //! Helper function to print info about a flow node
  void printFlowNode(FLNodeElab*, SCHScheduleType, int,
			    const char* = "") const;

private:
  // Used to print all the flow nodes in a schedule
  void printSchedule(SCHIterator&, SCHScheduleType, const char* = "") const;
  void printSequential(SCHSequential*, const char* = "") const;

  // A bunch of functions to gather and print statistics
  ScheduleMaskStats* getScheduleMaskEntry(AllScheduleStats* stats,
					  const SCHScheduleMask* mask);
  void gatherCombinationalStats(SCHCombinational* comb,
				SCHScheduleType type,
				AllScheduleStats* stats);
  void gatherSequentialEdgeStats(SCHSequential* seq,
				 SCHScheduleType type,
				 ClockEdge edge, const NUNetElab* clk,
				 AllScheduleStats* stats);
  void gatherSequentialStats(SCHSequential* seq,
			     SCHScheduleType type,
			     AllScheduleStats* stats);
  UInt32 printBlockStats(UtOStream& out, const BlockStats& blockStats,
                         const char* prefix);
  void printGatheredStats(UtOStream& out, const AllScheduleStats& scheduleStats);

  // Use to get to the scheduler data
  SCHSchedule* mSched;
  SCHScheduleFactory* mFactory;
  SCHMarkDesign* mMarkDesign;
  SCHUtil* mUtil;

  // Statistics on sample vs. transition based scheduling
  UInt32 mSampleNodes;
  UInt32 mTransitionNodes;
  UInt32 mMustBeTransitionNodes;
  UInt32 mMustBeAccurateNodes;
  UInt32 mDrivenByClockNodes;
  UInt32 mLatchNodes;
  UInt32 mCycleNodes;
  UInt32 mLevelMemoryNodes;
  UInt32 mFeedsOutputPinNodes;

  // Statistics on the type of state updates
  UInt32 mClkStateUpdates;
  UInt32 mCycleStateUpdates;
};

#endif // _DUMP_H_
