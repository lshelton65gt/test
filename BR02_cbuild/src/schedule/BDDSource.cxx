// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "schedule/BDDSource.h"

#include "nucleus/NUIf.h"
#include "nucleus/NUAlwaysBlock.h"
#include "flow/FLNodeElab.h"
#include "bdd/BDD.h"

//! Constructor with optional BDDContext
SimpleBDDSource::SimpleBDDSource(BDDContext* context)
{
  if (context)
  {
    mBDDContext = context;
    mWeOwnContext = false;
  }
  else
  {
    mBDDContext = new BDDContext();
    mWeOwnContext = true;
  }
}

//! Destructor
SimpleBDDSource::~SimpleBDDSource()
{
  if (mWeOwnContext)
    delete mBDDContext;
}

//! Return a BDD representing true
BDD SimpleBDDSource::trueBDD() const
{
  return mBDDContext->val1();
}

//! Return a BDD representing false
BDD SimpleBDDSource::falseBDD() const
{
  return mBDDContext->val0();
}

//! Return a representation of an invalid BDD
BDD SimpleBDDSource::invalid() const
{
  return mBDDContext->invalid();
}

//! Return the inversion of a BDD
BDD SimpleBDDSource::opNot(const BDD& bdd) const
{
  return mBDDContext->opNot(bdd);
}

//! Return the AND of two BDDs
BDD SimpleBDDSource::opAnd(const BDD& bdd1, const BDD& bdd2) const
{
  return mBDDContext->opAnd(bdd1,bdd2);
}

//! Return the OR of two BDDs
BDD SimpleBDDSource::opOr(const BDD& bdd1, const BDD& bdd2) const
{
  return mBDDContext->opOr(bdd1,bdd2);
}

//! Return true if one BDD implies another
bool SimpleBDDSource::implies(const BDD& assumption, const BDD& conclusion) const
{
  return mBDDContext->implies(assumption,conclusion);
}

//! Perform any necessary pre-analysis before computing a BDD
void SimpleBDDSource::seed(const FLNodeElab* /* flow */)
{
  return;
}

//! Construct a BDD for the condition of an elaborated flow
/*! This handles two cases -- flow for an NUIf and flow for an edge-sensitive always block */
BDD SimpleBDDSource::bdd(const FLNodeElab* flow)
{
  if (flow == NULL)
    return mBDDContext->invalid();

  NUUseDefNode* useDef = flow->getUseDefNode();
  NUExpr* expr = NULL;

  // For an if statement, return the BDD for the condition
  bool negate = false;
  NUIf* ifStmt = dynamic_cast<NUIf*>(useDef);
  if (ifStmt)
    expr = ifStmt->getCond();
  else
  {
    // For an always block, return the BDD for the edge expression
    NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
    if (always && always->isSequential())
    {
      NUEdgeExpr* edge= always->getEdgeExpr();
      expr = edge->getExpr();
      negate = (edge->getEdge() == eClockNegedge);
    }
  }

  BDD bdd = expr ? mBDDContext->bdd(expr,flow->getHier()) : mBDDContext->invalid();
  if (negate)
    bdd = mBDDContext->opNot(bdd);

  return bdd;
}
