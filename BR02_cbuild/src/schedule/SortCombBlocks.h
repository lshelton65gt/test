// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  A class to sort and simplify combinational blocks in a schedule
*/

#ifndef _SORTCOMBBLOCKS_H_
#define _SORTCOMBBLOCKS_H_

//! SCHSortCombBlocks class
/*! This implements a class that allows the scheduler to re-sort
 *  combinational blocks to get either data or code locality. It also
 *  reduces the schedule from a vector of vector of nodes to a simple
 *  vector of nodes. See the SCHReady class for details on how the
 *  vector of vector of nodes schedule is created.
 */
class SCHSortCombBlocks
{
public:
  //! constructor
  SCHSortCombBlocks(SCHUtil* util, SCHMarkDesign* mark);

  //! destructor
  ~SCHSortCombBlocks();

  //! Sorts a combinational schedule and removes duplicate nodes (same block)
  /*! This function sorts a combinational schedule either to improve
   *  data locality or code locality. While it is doing that it
   *  converts the schedule from a vector of vector of flow nodes back
   *  to a vector of flow nodes. It choses the first flow node in the
   *  vector of flow nodes to represent the entire block.
   */
  void sort(SCHCombinational* comb, bool sortByData);


  class BlockGroup;

  // Keep track of the last added block so that we can follow the
  // recursion when choosing the next node to schedule
  struct CmpNetHier
  {
    bool operator()(const BlockGroup* b1, const BlockGroup* b2) const;
  };

  // compare two blocks by hierarchy
  struct CmpHierNames
  {
    bool operator()(const BlockGroup* b1, const BlockGroup* b2) const;
  }; // struct CmpHierNames


  //! A class to maintain a group of nodes for a given block
  /*! This class allows iteration between groups of flow nodes. The
   *  groups of flow nodes are combinational flow nodes that will be
   *  scheduled together.
   */
  class BlockGroup
  {
  public:
    //! constructor
    BlockGroup(FLNodeElabVector* nodes);

    //! destructor
    ~BlockGroup();

    //! Get the set of nodes in this scheduled block
    FLNodeElabVector* getNodes() { return mNodes; }

    //! Get the set of nodes in this scheduled block (const)
    const FLNodeElabVector* getNodes() const { return mNodes; }

    //! Add fanin
    void addFaninBlock(BlockGroup* fanin);

    //! Abstraction for the fanin
    typedef UtSet<BlockGroup*, CmpHierNames> BlockFanin;

    //! Abstraction for a fanin iterator
    typedef Loop<BlockFanin> BlockFaninLoop;

    //! Abstraction for a const fanin iterator
    //    typedef Loop<const BlockFanin, BlockFanin::const_iterator>
    //CBlockFaninLoop;
    typedef CLoop<BlockFanin> CBlockFaninLoop;

    //! Iterate over the fanin of this block
    BlockFaninLoop loopBlockFanin();

    //! Iterate over the fanin of this block
    CBlockFaninLoop loopBlockFanin() const;

    //! Debug print routine
    void print() const;

    //! Get a representative flow (for assertions) (can return NULL)
    FLNodeElab* getRepresentativeFlow() const;

  private:
    // The nodes associated with this block
    FLNodeElabVector* mNodes;

    // The set of fanin for this block
    BlockFanin* mBlockFanin;
  }; // class BlockGroup


private:
  // Hide the copy and assign constructors
  SCHSortCombBlocks(const SCHSortCombBlocks&);
  SCHSortCombBlocks& operator=(const SCHSortCombBlocks&);

  // Types and data for the set of block groups
  typedef UtVector<BlockGroup*> BlockGroups;
  typedef Loop<BlockGroups> BlockLoop;
  BlockGroups* mBlockGroups;

  // Types for debugging code
  typedef UtSet<BlockGroup*> CoveredBlockGroups;
  typedef UtVector<BlockGroup*> BlockGroupStack;
  void findCycles(BlockGroup*, CoveredBlockGroups&, CoveredBlockGroups&,
		  BlockGroupStack&);

  // Functions to add and traverse block groups
  void createBlockFlow(SCHCombinational* comb);
  void addBlockGroup(FLNodeElabVector* nodes);
  BlockLoop loopBlockGroups();

  // Private helper functions
  int computeFanout();
  void addToReadyQueue(const BlockGroup* nodes);
  const BlockGroup* getNextBlock();
  void scheduleBlock(const BlockGroup* nodes, SCHCombinational* comb);

  // Abstraction and data to maintain the fanout count for a block. We
  // may want to make this a hash_map once we know it matters for
  // performance.
  typedef UtMap<const BlockGroup*, int, CmpHierNames> FanoutCountMap;
  FanoutCountMap* mFanoutCountMap;

  // compare two use def nodes
  struct CmpUseDefs
  {
    bool operator()(const NUUseDefNode* ud1, const NUUseDefNode* ud2) const;
  };

  // Abstraction and data to keep a set of ready FLNodeElabVector sorted by
  // use def node. This allows us to schedule by code locality.
  typedef UtSet<const BlockGroup*, CmpHierNames> BlockInstances;
  typedef UtMap<const NUUseDefNode*,BlockInstances*,CmpUseDefs> BlockNodesMap;
  BlockNodesMap* mBlockNodesMap;

  // Keep track of the last scheduled use def so we can sort by it
  const NUUseDefNode* mLastScheduled;

  typedef UtSet<const BlockGroup*, CmpNetHier> HierBlocks;
  struct CmpDepth { bool operator()(const int d1, const int d2) const; };
  typedef UtMap<int, HierBlocks*, CmpDepth> LastInsertedStack;
  typedef LastInsertedStack::value_type LastInsertedStackValue;
  LastInsertedStack* mLastInsertedStack;
  UInt32 mStackDepth;

  // See if we should sort by data first
  bool mSortByData;

  // Needed to help iterate skipping over cycles
  SCHUtil* mUtil;
  SCHMarkDesign* mMarkDesign;
}; // class SCHSortCombBlocks

#endif // _SORTCOMBBLOCKS_H_
