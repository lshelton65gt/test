// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  Utilities and class for sorting by use def and hierarchy
*/

#ifndef _USEDEFHIERPAIR_H_
#define _USEDEFHIERPAIR_H_

#include "symtab/STBranchNode.h"

//! Class to hold use def hier pairs -- used by maps below
class SCHUseDefHierPair
{
public:
  //! constructor given the use def and heirarchy
  SCHUseDefHierPair(const NUUseDefNode* useDef, const HierName* hierName) :
    mUseDef(useDef), mHierName(hierName) {}

  //! constructor given an elaborated flow node
  SCHUseDefHierPair(const FLNodeElab* flowElab) :
    mUseDef(flowElab->getUseDefNode()), mHierName(flowElab->getHier())
  {}

  //! destructor
  ~SCHUseDefHierPair() {}

  //! Compare function
  static int compare(const SCHUseDefHierPair& uhp1, const SCHUseDefHierPair& uhp2)
  {
    // Check for re-insertion (do not re-insert)
    if ((uhp1.mUseDef == uhp2.mUseDef) && (uhp1.mHierName == uhp2.mHierName))
      return 0;

    // Sort based on use def first
    int cmp = NUUseDefNode::compare(uhp1.mUseDef, uhp2.mUseDef);

    // Sort on the hierarchy next if they are the same use def
    if (cmp == 0) {
      cmp = HierName::compare(uhp1.mHierName, uhp2.mHierName);
    }

    return cmp;
  }

  //! Used for sorting in maps
  bool operator<(const SCHUseDefHierPair& uhp1) const
  {
    return compare(*this, uhp1) < 0;
  }

  //! Equality operator (for hashing)
  bool operator==(const SCHUseDefHierPair& uhp1) const
  {
    return (mUseDef == uhp1.mUseDef) && (mHierName == uhp1.mHierName);
  }

  //! Hash function
  size_t hash() const
  {
    return ((size_t)mUseDef >> 2) + ((size_t)mHierName >> 2);
  }

  //! Get use def node
  const NUUseDefNode* getUseDef() const { return mUseDef; }

private:
  const NUUseDefNode* mUseDef;
  const HierName* mHierName;
};

#endif // _USEDEFHIERPAIR_H_
