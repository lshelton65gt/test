// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  Class to hold data and functions to analyze the timing of a design.
*/

#ifndef _TIMINGANALYSIS_H_
#define _TIMINGANALYSIS_H_

#include "CombTimingWalker.h"

//! class SCHTimingAnalysis
/*! This class holds the data and functions to analyze the timing
 *  characteristics of the design. This process involves traversing
 *  the design and marking the flow nodes with a timing signature
 *  (SCHSignature). It also involves finding all the logic that is
 *  used to compute derived clocks.
 */
class SCHTimingAnalysis
{
public:
  //! constructor
  SCHTimingAnalysis(SCHUtil* util, SCHScheduleFactory* factory,
                    SCHMarkDesign* markDesign);

  //! destructor
  ~SCHTimingAnalysis();

  //! Compute the timing characteristics for all design components.
  bool computeTiming();

  //! Adjust the timing for newly created flow nodes
  void adjustTiming(FLNodeElabSet& outFlows, FLNodeElabSet& newInFlows);
  
  //! Computes the transition mask for a elaborated net
  /*! All drivers of the elaborated net must be transition scheduled!
   *  This routine can return NULL if the elaborated net is dead.
   */
  const SCHScheduleMask* getNetTransitionMask(NUNetElab* netElab) const;

  //! Function to accumulate transition masks
  /*! If a new mask needs to be made from a set of flow node's
   *  transition masks, this routine should be used. It can do the
   *  proper combination following all the transition mask calculation
   *  rules.
   *
   *  \param mask - the current accumulated mask. It can be NULL
   *
   *  \param flow - a flow node who's transition mask should be added
   *
   *  \param ignoreClocks - if set any clock flow nodes use their
   *  fanin timing instead of the timing they provide their fanou.
   *
   *  \returns the new accumulated mask. Note that if this needs to be
   *  stored anywhere the mask should be assigned (bumps the ref
   *  count).
   */
  const SCHScheduleMask* accumulateTransitionMask(const SCHScheduleMask* mask,
                                                  FLNodeElab* flow,
                                                  bool ignoreClocks = false);


  //! Converts a combinational flow timing to a flop timing
  /*! As part of merging combinational flow into sequential flow, the
   *  transition mask for converted combinational blocks must be made
   *  accurate.
   *
   *  It now transitions with the same mask as the other
   *  defs of the flop.
   *
   *  The sample mask can stay the same since it is read by the flops
   *  in the same block. It should not be sampled by anything outside
   *  the block because then it would not be mergable.
   *
   *  The caller needs to provide an elaborated flow to grab the
   *  timing from.
   */
  void convertToFlop(FLNodeElab* convertedFlow, FLNodeElab* timingFlow);

private:
  // Mark all the clocks with flags so that we know they are clocks.
  bool markClocks();
  void markBlockClock(FLNodeElab* flow);

  //! Compute when nodes in the design transition
  void calculateDesignSensitivity();
  
  //! Compute when nodes in the design are sampled
  void calculateDesignSampling();

  void setSignature(FLNodeElab* flow, const SCHSignature* signature);
  const SCHScheduleMask* getClockMask(FLNodeElab* flow) const;
  const SCHScheduleMask* getFlowSensitivity(FLNodeElab* flow,
					    const SCHScheduleMask* mask,
                                            bool ignoreClocks = false) const;

  // Private class to walk Design DAG in depth first order. The flops
  // and primary inputs in the design are the leaf nodes in the
  // DAG. All cycles introduced by loopTimingDependencies have been
  // remove by the stronly connectected components algorithm.
  class SensitivityWalker : public SCHCombTimingWalker
  {
  public:
    //! constructor
    SensitivityWalker(SCHMarkDesign* markDesign, SCHUtil* util,
                      SCHTimingAnalysis* timingAnalysis,
                      SCHScheduleFactory* scheduleFactory) :
      SCHCombTimingWalker(markDesign, util),
      mTimingAnalysis(timingAnalysis), mScheduleFactory(scheduleFactory)
    {}

    //! Get input net set callback 
    virtual UIntPtr getValue(FLNodeElab* flow);

    //! Merge input net sets callback
    virtual UIntPtr mergeValues(UIntPtr value1, UIntPtr value2);

    //! Store input net set callback
    virtual UIntPtr storeValue(FLNodeElab* flow, UIntPtr value);

  private:
    SCHTimingAnalysis* mTimingAnalysis;
    SCHScheduleFactory* mScheduleFactory;
  }; // class SensitivityWalker : public SCHCombTimingWalker

  // The sensitivity walker needs to be a friend to mark the flow
  // sensitivity
  friend class SensitivityWalker;

  // Private class to walk Design DAG in depth first order. The flops
  // and primary inputs in the design are the leaf nodes in the
  // DAG. All cycles introduced by loopTimingDependencies have been
  // remove by the stronly connectected components algorithm.
  class SampleWalker : public SCHCombTimingWalker
  {
  public:
    //! constructor
    SampleWalker(SCHMarkDesign* markDesign, SCHUtil* util,
                 SCHTimingAnalysis* timingAnalysis,
                 SCHScheduleFactory* scheduleFactory) :
      SCHCombTimingWalker(markDesign, util),
      mTimingAnalysis(timingAnalysis), mScheduleFactory(scheduleFactory)
    {}

    //! Get input net set callback 
    virtual UIntPtr getValue(FLNodeElab* flow);

    //! Merge input net sets callback
    virtual UIntPtr mergeValues(UIntPtr value1, UIntPtr value2);

    //! Store input net set callback
    virtual UIntPtr storeValue(FLNodeElab* flow, UIntPtr value);

  private:
    SCHTimingAnalysis* mTimingAnalysis;
    SCHScheduleFactory* mScheduleFactory;
  }; // class SampleWalker : public SCHCombTimingWalker

  // The sample walker needs to be a friend to mark the flow sample
  // mask
  friend class SampleWalker;

  // Private functions to compute transition and sampling masks
  void computeTransitionMaskFromFanin(FLNodeElab* flowElab);

  // These are used to add various timing information to a flow node
  const SCHScheduleMask* addSensitivity(FLNodeElab*, NUUseDefNode*);
  const SCHScheduleMask* addSensitivity(FLNodeElab*, const SCHScheduleMask*);
  const SCHScheduleMask* addSampling(FLNodeElab*, const SCHScheduleMask*);
  void addSamplingRecurse(FLNodeElab* flow, const SCHScheduleMask* mask);
  void addMustBeTransitionRecurse(FLNodeElab* flowElab);

  // If set, an error was found during timing analysis.
  bool mTimingError;

  // Used to access general scheduler data and functions
  SCHUtil* mUtil;
  SCHScheduleFactory* mScheduleFactory;
  SCHMarkDesign* mMarkDesign;
}; // class SCHTimingAnalysis

#endif // _TIMINGANALYSIS_H_
