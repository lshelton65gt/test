// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "util/UtIOStream.h"

#include "symtab/STBranchNode.h"

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUCycle.h"
#include "nucleus/NUScope.h"

#include "flow/FLNodeElab.h"

#include "schedule/Schedule.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Signature.h"
#include "schedule/ScheduleBase.h"

#include "Util.h"
#include "MarkDesign.h"

void SCHScheduleBase::dumpIndent(int indent)
{
  {
    for (int i = 0; i < indent; ++i) {
      UtIO::cout() << " ";
    }
  }
}

void 
SCHScheduleBase::dumpFlowNode(FLNodeElab* flow, SCHScheduleType type,
                              int index, int indent, SCHMarkDesign* mark)
{
  // Add the ident and flow number
  dumpIndent(indent);
  UtIO::cout() << "  " << index << "\t";

  // If it is a cycle, dump that and we are done
  NUCycle* cycle = flow->getCycle();
  if (cycle != NULL) {
    UtIO::cout() << "(cycle " << cycle->getID() << ")" << UtIO::endl;
    return;
  }

  // Dump the logic location
  UtString buf;
  NUUseDefNode* useDef = flow->getUseDefNode();
  if (useDef != NULL) {
    const SourceLocator& loc = useDef->getLoc();
    UtIO::cout() << loc.getFile() << ":" << loc.getLine();
  }

  // For sequential blocks we need an ordered set of output nets (used
  // in two places below).
  NUNetOrderedSet sortedNetSet;
  if (SCHUtil::isSequential(useDef)) {
    NUNetSet netSet;
    SCHUtil::getBlockNets(flow, &netSet);

    // Sort the nets so that the output is canonical
    sortedNetSet.insert(netSet.begin(), netSet.end());
  }

  // For sequential blocks, get a canonical output
  NUNet* net;
  if (SCHUtil::isSequential(useDef)) {
    net = *(sortedNetSet.begin());
  } else {
    net = flow->getDefNet()->getNet();
  }

  // Print a name for this flow node
  buf.clear();
  flow->getHier()->compose(&buf);
  buf += ".";
  buf += net->getName()->str();
  UtIO::cout() << "  " << buf << " ";

  // Print the use def block and strength (if it is a tristate)
  if (useDef != NULL) {
    StringAtom* name = useDef->getName();
    if (name == NULL)
      UtIO::cout() << " (" << useDef->typeStr() << ")";
    else
      UtIO::cout() << " " << name->str();
    NUNet* net = flow->getDefNet()->getNet();
    if (net->isTristate()) {
      Strength strength = useDef->getStrength();
      switch(strength) {
        case eStrPull:
          UtIO::cout() << ":pull";
          break;

        case eStrDrive:
          UtIO::cout() << ":drive";
          break;

        case eStrTie:
          UtIO::cout() << ":tie";
          break;
      }
    }
  }

  // Print out the sample or transition mask if there is one
  if (type == eCombSample) {
    // Print the transition mask since we are a sample schedule
    buf.clear();
    const SCHScheduleMask* transitionMask;
    transitionMask = mark->getSignature(flow)->getTransitionMask();
    if (transitionMask != NULL)
      transitionMask->compose(&buf,NULL);
    else
      buf = "*unknown*";
    UtIO::cout() << " (" << buf << ")";
  } else {
    // We are a transition flow, so print the sample mask if there is one
    buf.clear();
    const SCHSignature* sig = mark->getSignature(flow);
    const SCHScheduleMask* sampleMask = sig->getSampleMask();
    if (sampleMask != NULL) {
      sampleMask->compose(&buf,NULL);
      UtIO::cout() << " (" << buf << ")";
    }
  }

  // For sequential blocks, print if we double buffer
  if (SCHUtil::isSequential(useDef)) {
    for (NUNetOrderedSet::iterator pos = sortedNetSet.begin();
	 pos != sortedNetSet.end();
	 ++pos) {
      NUNet* net = *pos;
      if (net->isDoubleBuffered()) {
	buf.clear();
        // this is needed for test/schedule/seqloop8*
        buf << net->getScope()->getName()->str() << ".";
	net->compose(&buf,NULL);
	UtIO::cout() << " DB=" << buf;
      }
    }
  }

  // print depth - only useful for sequential blocks
  if (SCHUtil::isSequential(useDef)) {
    UtIO::cout() << " Depth=" << flow->getDepth();
  }

  UtIO::cout() << UtIO::endl;
}

SCHInputNetsCLoop SCHScheduleBase::loopBranchNets(void) const
{ 
  SCHED_ASSERT(mBranchNets, this);
  return SCHInputNetsCLoop(*mBranchNets); 
}

void 
SCHScheduleBase::printAssertHeader(const char* file, int line, const char* exprStr,
                                   const char* assertName)
{
  // Print the header information for an assert. This is broken up so
  // that information about multiple schedules can be printed.
  //
  // This assert prints all the info we need about an object in order
  // tor the user to find the problem. We'd like to give human
  // readable data if possible, which is different depending on the
  // type of object
  UtIO::cerr() << "\n\n******CARBON INTERNAL ERROR*******\n\n";
  UtIO::cerr() << file << ':' << line << " " << assertName <<"(" << exprStr
               << ") failed\n" << UtIO::flush;
}

void SCHScheduleBase::printAssertBody() const
{
  if (this == NULL) {
    UtIO::cerr() << "(null object)\n";

  } else {
    // Print information about the schedule
    print(true, NULL);
  }
}

void SCHScheduleBase::printAssertTrailer()
{
  // print stack trace ???
  UtIO::cerr() << UtIO::flush;
  abort();
} // SCHScheduleBase::printAssertInfo

void SCHScheduleBase::composeBranchNets(UtString* buf, SCHMarkDesign* mark) const
{
  const SCHInputNets* inputNets = getBranchNets();
  if (inputNets != NULL) {
    *buf += "BranchNets = (";
    mark->compose(inputNets, buf);
    *buf += ")";
  }
}
