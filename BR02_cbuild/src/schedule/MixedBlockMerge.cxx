// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implements sequential block merging
*/

#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUCycle.h"

#include "flow/Flow.h"
#include "flow/FLNodeElab.h"

#include "util/ArgProc.h"

#include "compiler_driver/CarbonDBWrite.h"

#include "schedule/Schedule.h"
#include "Util.h"
#include "Dump.h"
#include "MarkDesign.h"
#include "ScheduleData.h"
#include "MergeTest.h"
#include "MixedBlockMerge.h"
#include "TimingAnalysis.h"

SCHMixedBlockMerge::SCHMixedBlockMerge(SCHUtil* util, SCHMarkDesign* mark,
                                       SCHScheduleData* data,
                                       SCHTimingAnalysis* timingAnalysis) :
  SCHSequentialBlockMerge(util, mark, data, false), mMergeTest(NULL),
  mTimingAnalysis(timingAnalysis)
{
  mGroupToScheduleMap = new GroupToScheduleMap;
  mNewSequentials = new FLNodeElabSet;
  mCoveredCombinationals = new FLNodeElabSet;

  // Create artifical an combinational schedules to use as the
  // schedule for combo blocks that feed outputs. This makes the
  // mergability tests more homogeneous.
  const SCHScheduleMask* outputMask = mUtil->getOutputMask();
  mUnmergableComboOutput = new SCHCombinational(eCombTransition, outputMask, NULL);
}

SCHMixedBlockMerge::~SCHMixedBlockMerge()
{
  delete mUnmergableComboOutput;
  delete mGroupToScheduleMap;
  delete mNewSequentials;
  delete mCoveredCombinationals;
}

void
SCHMixedBlockMerge::merge(const NUNetElabSet* unmergableNets)
{
  // Create the merge test class for this pass
  mMergeTest = new SCHMergeTest(mMarkDesign, unmergableNets, true, true);

  // Only block merge if we don't want to preserve visibility. This
  // type of block merging reduces visibility because it makes
  // combinational logic inaccurate.
  bool doMerge = mUtil->isFlagSet(eMergeBlocks);
  bool doMixedMerge = !mUtil->isFlagSet(eNoMixedMerge);
  bool preserveNets = mUtil->getArgs()->getBoolValue("-g");
  if (doMerge && doMixedMerge && !preserveNets) {
    // Initialize merging
    initLocalData();

    // Do the actual merge by calling the SCHBlockMerge class to do it
    int dumpBlockMergeInfo = mUtil->getDumpMergedBlocks();
    if (dumpBlockMergeInfo != 0) {
      UtIO::cout() << "Merging sequential and combinational blocks:\n";
    }
    mergeBlocks(dumpBlockMergeInfo, SCHMergeMask(eMTFanin));

    // Print why some blocks are unmergable
    if (dumpBlockMergeInfo != 0) {
      printResults();
    }
  }

  // All done with the merge test memory
  delete mMergeTest;
  mMergeTest = NULL;
}

void SCHMixedBlockMerge::initLocalData()
{
  // Call the SCHBlockMerge::start function to have it initialize itself
  start();

  // Mark all combinational blocks that feed outputs as invalid for merging
  SCHMarkDesign::OutputPortsLoop o;
  for (o = mMarkDesign->loopOutputPorts(); !o.atEnd(); ++o) {
    FLNodeElab* flowElab = *o;
    recordUnmergableCombinationalBlockFanin(flowElab);
  }

  // Add all the sequential scheduled blocks to the merge process
  SCHSchedulesLoop p;
  for (p = mScheduleData->loopAllSequentials(); !p.atEnd(); ++p) {
    SCHSequential* seq = p.getSequentialSchedule();
    gatherScheduleBlocks(seq, eClockPosedge);
    gatherScheduleBlocks(seq, eClockNegedge);
  }
}

void 
SCHMixedBlockMerge::recordUnmergableCombinationalBlockFanin(FLNodeElab* flowElab)
{
  // Check if we have been here before
  if (!mCoveredCombinationals->insertWithCheck(flowElab)) {
    return;
  }

  // Only continue if this is a valid combinational
  if (!isValidCombinational(flowElab)) {
    return;
  }

  // Add this to the set of unmergable combinational blocks. We record
  // it by giving it a the unmergable combo schedule.
  createMergeBlock(flowElab, mUnmergableComboOutput);
  mMergeTest->recordCombFlow(flowElab, mUnmergableComboOutput, flowElab);

  // Visit the fanin to find more combinational blocks
  for (Fanin l = mUtil->loopFanin(flowElab); !l.atEnd(); ++l) {
    FLNodeElab* faninElab = *l;
    recordUnmergableCombinationalBlockFanin(faninElab);
  }
}

FLNodeElabVector*
SCHMixedBlockMerge::createMergeBlock(FLNodeElab* flowElab, SCHScheduleBase* sched)
{
  // Create the block for this flow
  FLNodeElabVector* nodes;
  nodes = SCHSequentialBlockMerge::createMergeBlock(flowElab, sched);

  // Gather the combinational block fanin for later creates for
  // sequentials. Also remember the schedule and edge for this group
  // of nodes
  if (SCHUtil::isSequential(flowElab) && !flowElab->isInactive()) {
    recordCombinationalBlockFanin(flowElab, sched, flowElab);

    SCHSequentialEdge* seqEdge = sched->castSequentialEdge();
    SCHED_ASSERT(seqEdge != NULL, sched);
    GroupToScheduleMap::iterator pos = mGroupToScheduleMap->find(nodes);
    if (pos == mGroupToScheduleMap->end()) {
      GroupToScheduleMap::value_type val(nodes, seqEdge);
      mGroupToScheduleMap->insert(val);
    } else {
      const SCHSequentialEdge* storedSeqEdge = pos->second;
      FLN_ELAB_ASSERT(seqEdge == storedSeqEdge, flowElab);
    }
  }
  return nodes;
}

void
SCHMixedBlockMerge::recordCombinationalBlockFanin(FLNodeElab* flowElab,
                                                  SCHScheduleBase* sched,
                                                  FLNodeElab* seqFlow)
{
  for (Fanin l = mUtil->loopFanin(flowElab); !l.atEnd(); ++l) {
    FLNodeElab* fanin = *l;
    recordCombinationalBlock(fanin, sched, seqFlow);
  }
}

void
SCHMixedBlockMerge::recordCombinationalBlock(FLNodeElab* flowElab,
                                             SCHScheduleBase* sched,
                                             FLNodeElab* seqFlow)
{
  // Only continue if this is a valid combinational
  if (!isValidCombinational(flowElab)) {
    return;
  }

  // If we haven't created a block for this do so now. We use the
  // first schedule we encounter. If this block is not mergable
  // because it feeds multiple ouputs we will find it through
  // SCHMergeTest, not through the block structures.
  if (mCoveredCombinationals->insertWithCheck(flowElab)) {
    createMergeBlock(flowElab, sched);
  }

  // Store information about this combinational blocks sequential
  // edge.  If we get more than two sequential and edge pairs or have
  // already recorded this sequential fanout then stop. We know this
  // because recordCombFlow returns false.
  if (!mMergeTest->recordCombFlow(flowElab, sched, seqFlow)) {
    return;
  }

  // Recurse to this combinational blocks fanin
  recordCombinationalBlockFanin(flowElab, sched, seqFlow);
}

bool SCHMixedBlockMerge::isValidCombinational(FLNodeElab* flowElab)
{
  // Stop at sequential blocks
  if (SCHUtil::isSequential(flowElab)) {
    return false;
  }

  // Stop at bound nodes
  if (flowElab->getUseDefNode() == NULL) {
    return false;
  }

  // Stop at initial blocks
  if (SCHUtil::isInitial(flowElab)) {
    return false;
  }

  return true;
}

bool
SCHMixedBlockMerge::validStartBlock(const NUUseDefNode* useDef, MergeBlock*) const
{
  return SCHUtil::isSequential(useDef);
}

bool 
SCHMixedBlockMerge::blockMergable(const NUUseDefNode* useDef, MergeBlock* block)
  const
{
  return mMergeTest->mergable(useDef, block->getGroups());
} // SCHMixedBlockMerge::blockMergable

void
SCHMixedBlockMerge::getBlockFanin(MergeBlock* block,
                                  BlockSet* faninSet) const
{
  // Visit the elaborated flow to create the block graph edges
  for (SCHFlowGroups::FlowsLoop f = block->loopFlowNodes(); !f.atEnd(); ++f) {
    FLNodeElab* flowElab = *f;
    if (SCHUtil::isSequential(flowElab)) {
      // Only visit the level fanin because only because it is the
      // only logic that can be merged in. Clock logic must be
      // precomputed.
      for (SCHMarkDesign::LevelFaninLoop l = mMarkDesign->loopLevelFanin(flowElab);
           !l.atEnd(); ++l) {
        FLNodeElab* faninElab = *l;
        addFaninBlock(block, faninElab, faninSet);
      }
    } else {
      // Use loop dependencies so that multi-strength nets are ordered
      // correctly.
      SCHMarkDesign::FlowDependencyLoop f;
      for (f = mMarkDesign->loopDependencies(flowElab); !f.atEnd(); ++f) {
        FLNodeElab* faninElab = *f;
        addFaninBlock(block, faninElab, faninSet);
      }
    }
  } // for
} // SCHMixedBlockMerge::getBlockFanin

void
SCHMixedBlockMerge::addFaninBlock(MergeBlock* fanoutBlock, FLNodeElab* faninElab,
                                  BlockSet* faninSet) const
{
  // Don't add sequential fanin because this will add cycles to the
  // graph. We are merging combos into sequentials so we just need
  // fanins up to and not including sequentials.
  if (!SCHUtil::isSequential(faninElab)) {
    for (BlocksLoop b = findFlowBlocks(faninElab); !b.atEnd(); ++b) {
      // Add this as a fanin as long as it isn't the same block
      MergeBlock* faninBlock = *b;
      if (faninBlock != fanoutBlock) {
        faninSet->insert(faninBlock);
      }
    }
  }
} // SCHMixedBlockMerge::getBlockFanin

void
SCHMixedBlockMerge::preMergeCallback(MergeBlock*, MergeBlock* block)
{
  // Mark all the defs unwaveable. Even the ones that don't reach
  // outside the block (and therefore don't have an elaborated
  // flow associated with it)
  for (SCHFlowGroupsLoop g = block->loopGroups(); !g.atEnd(); ++g) {
    FLNodeElabVector* nodes2 = *g;
    FLNodeElab* flow = nodes2->back();
    NUNetSet defs;
    SCHUtil::getBlockNets(flow, &defs);
    STBranchNode* hier = flow->getHier();
    for (NUNetSetIter n = defs.begin(); n != defs.end(); ++n) {
      // Elaborated the net into the flow's hierarchy
      NUNet* net = *n;
      NUNetElab* netElab = net->lookupElab(hier);
      if (netElab != NULL) {
        // Mark it an invalid wave net
        CbuildSymTabBOM::markInvalidWaveNet(netElab->getSymNode());
      }
    }
  }
} // SCHMixedBlockMerge::preMergeCallback

void SCHMixedBlockMerge::moveFlows(FLNodeElabVector* dst, FLNodeElabVector* src)
{
  // Get the sequential edge for the destination
  GroupToScheduleMap::iterator pos = mGroupToScheduleMap->find(dst);
  if (pos == mGroupToScheduleMap->end()) {
    FLNodeElab* flowElab = dst->back();
    bool mergeScheduleFound = false;
    FLN_ELAB_ASSERT(!mergeScheduleFound, flowElab);
  }
  SCHSequentialEdge* seqEdge = pos->second;

  // Walk the src nodes and put then in the schedule
  FLNodeElab* timingFlow = dst->back();
  for (FLNodeElabVectorLoop l(*src); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    seqEdge->addSequentialNode(flowElab);
    dst->push_back(flowElab);
    if (mNewSequentials->insertWithCheck(flowElab)) {
      mMarkDesign->addNewSequentialNode(flowElab);
      mTimingAnalysis->convertToFlop(flowElab, timingFlow);
    }
  }

  // Now we can clear the source
  src->clear();
} // void SCHMixedBlockMerge::moveFlows

void SCHMixedBlockMerge::printResults()
{
  // Print the invidiviual block stats
  mMergeTest->printStats();

  // Print info about adjacency failures
  printAdjacencyFailures();
}

void SCHMixedBlockMerge::recordCycleBlock(const MergeBlock*) const
{
  mMergeTest->recordCycleBlock();
}
