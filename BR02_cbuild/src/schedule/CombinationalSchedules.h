// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  Class to hold data and functions in the creation of combinational schedules
*/

#ifndef _COMBINATIONALSCHEDULES_H_
#define _COMBINATIONALSCHEDULES_H_

#include "InputNets.h"
#include "SortSchedules.h"

class SCHBlockFlowNodes;


//! class SCHCombinationalSchedules
/*! This class holds the data and functions to create the
 *  combinational schedules. The types of combinational schedules
 *  include the initial schedule, the input schedule, the async
 *  schedule, the debug schedule and a set of schedules using either
 *  sample or transition based scheduling.
 */
class SCHCombinationalSchedules
{
public:
  //! constructor
  SCHCombinationalSchedules(SCHSchedule* sched, SCHCreateSchedules* creat,
                            SCHUtil* util, SCHDump* dump,
                            SCHScheduleData* scheduleData,
                            SCHScheduleFactory* factory, SCHMarkDesign* mark);

  //! destructor
  ~SCHCombinationalSchedules();

  //! Create the combinational schedules
  /*!
   * Once this function is called all the nodes are in the
   * combinational schedules. This means we can tell in which schedule
   * every node of every block is in. But we have not optimized it
   * yet. This allows processes to do analysis on the un-optimized
   * schedules.
   */
  void create();

  //! Optimized the combinational schedules
  /*!
   * This function tries to reduce the overhead of the schedules by
   * reducing the number of calls to a combinational block. This can
   * be done by sorting them such that multiple nodes in a
   * combinational block can be called just once (node merging). It
   * also splits and merges blocks.
   *
   * \returns true if succesful, false if an error occured which
   * requires aborting the compiler.
   */
  bool optimize(const char* fileRoot);

  //! Mark all the comb. nodes that must be accurate (run with transition).
  void markDesignAccurateNodes();

  //! Schedule a combinational node
  void scheduleCombinationalNode(FLNodeElab*, SCHScheduleType);

  //! Add a node to the input, async, or initial schedules
  void addToSpecialSchedules(FLNodeElab*, const SCHSignature*,
                             bool onlyInitSchedule);

private:
  // Local definition of reasons why we mark nodes must be accurate
  enum AccurateReason
  {
    eCSNone,		// The node can run with sample
    eCSCycle,		// The node is in a cycle
    eCSFeedsCycle,	// The node feeds a cycle
    eCSLatch,		// The node is a latch
    eCSFeedsLatch,	// The node feeds a latch
    eCSMemWrite,	// The node is a memory write
    eCSFeedsMemWrite,	// The node feeds a memory write
    eCSFeedsOutput,	// The node feeds an output pin
    eCSFedByClock,	// The node's fanin has a clock pin or clock logic
    eCSFeedsClockNode,	// The node feeds a node that has clock on its fanin
    eCSFedByClockBlock, // The node is in same block as a fed by clock node
    eCSFeedsClockBlock,	// The node feeds a node in a clock block
    eCSReasonsSize
  };

  // The same set of reasons in strings
  UtString* mReasons;

  struct CmpMasks
  {
    bool operator()(const SCHScheduleMask* m1, const SCHScheduleMask* m2) const;
  };

  //! Abstraction for a set of combinational schedules
  typedef UtMap<const SCHScheduleMask*,
		SCHCombinational*,
		CmpMasks> CombinationalMap;

  // Get the set of transition or sample based combinational schedules
  CombinationalMap& getCombinationalMap(SCHScheduleType type)
  {
    if (type == eCombTransition)
      return mCombinationalScheduleMaps[0];
    else
    {
      INFO_ASSERT(type == eCombSample,
                  "Attempt to retrieve an unknown combinational schedule set");
      return mCombinationalScheduleMaps[1];
    }
  }

  AccurateReason isAccurateNode(FLNodeElab* flow);
  bool lookForClocks(FLNodeElab* flow, UInt32 pass,
                     const SCHBlockFlowNodes& blockFlowNodes);
  void lookForAccurateNodes(FLNodeElab*, AccurateReason, UInt32);
  void markAccurateNodes(FLNodeElab* flow, AccurateReason accurateReason,
                         const SCHBlockFlowNodes& blockFlowNodes);
  void markScheduleAccurateNodes(FLNodeElab* flow,
                                 const SCHBlockFlowNodes& blockFlowNodes);
  void dumpTransitionNode(FLNodeElab* flow, AccurateReason accurateReason);
  AccurateReason computeNextReason(AccurateReason curReason);
  void startCreateForceDepositSchedule(FLNodeElab* flow, UInt32 pass);
  bool createForceDepositSchedule(FLNodeElab* flow, UInt32 pass);

  // Routine to re sort the combinational schedule to get locality of
  // reference and remove duplicate combo flow nodes if possible.
  void mergeCombinationalNodes(SCHCombinational*, const char*);

  SCHCombinational* findCombinational(const SCHScheduleMask*, SCHScheduleType);

  // Routine to find and deal with combinational blocks in more than
  // one schedule.
  bool analyzeMultiTimingBlocks();

  // Types and routines to break up input and async schedules
  typedef UtMap<const SCHInputNets*,FLNodeElabVector,SCHCmpInputNets> NetSetNodes;
  typedef NetSetNodes::iterator NetSetNodesIter;
  void createInputSchedules(SCHScheduleType, FLNodeElabVectorLoop);
  void createInputSchedule(SCHScheduleType type,
                           int index,
                           const SCHScheduleMask* mask,
                           const SCHInputNets* netSet,
                           FLNodeElabVectorLoop nodeLoop);
			    
  // Record a flow's net elab as a deposit trigger if it is
  // depositable/forcible
  void recordDepositTrigger(FLNodeElab* flowElab);

  // Merge blocks process
  SCHBlockMerge* mBlockMerge;

  // Class to re sort the combinational schedules to merge combo
  // blocks with the same use def node
  SCHReady* mReady;

  // Class to re sort the combinational schedules to get locality of
  // reference (data or code)
  SCHSortCombBlocks* mSortCombBlocks;

  // Block split functions and data
  SCHBlockSplit* mBlockSplit;

  // Types, data, and functions to find places to branch around large
  // blocks of logic.
  void findDesignCondExecBranchPoints(Stats* stats);

  // Types and functions used to sort combinational schedules
  typedef UtMap<FLNodeElab*, SCHCombinational*> FlowToSchedMap;
  void sortSchedules(SCHScheduleType type);
  void addToFlowToSchedMap(SCHCombinational*, FlowToSchedMap&);
  static bool lessCombinational(SCHCombinational*, SCHCombinational*);

  //! Derive Class to print more info about cycles in the schedules
  class SortSchedules : public SCHSortSchedules
  {
  public:
    SortSchedules(SCHMarkDesign* mark, const FlowToSchedMap& flowToSchedMap) :
      SCHSortSchedules(mark), mMarkDesign(mark), mFlowToSchedMap(flowToSchedMap)
    {}

  protected:
  //! Override the print cycle function to give more detail
  void printCycle(const ScheduleVector& schedules) const;

  private:
    SCHMarkDesign* mMarkDesign;
    const FlowToSchedMap& mFlowToSchedMap;
  };
  
  //! Class has to be a friend to access private types
  friend class SortSchedules;

  // The combination schedules (transition and sample schedules). The
  // map is used to find if a schedule exists already. The schedule is
  // also store in the SCHScheduleData class.
  CombinationalMap mCombinationalScheduleMaps[2];

  // Used to access general scheduler data and functions
  SCHSchedule* mSched;
  SCHCreateSchedules* mCreate;
  SCHUtil* mUtil;
  SCHDump* mDump;
  SCHScheduleFactory* mScheduleFactory;
  SCHMarkDesign* mMarkDesign;
  SCHScheduleData* mScheduleData;
}; // class SCHCombinationalSchedules

#endif // _COMBINATIONALSCHEDULES_H_
