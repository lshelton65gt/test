// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implements sequential block merging
*/

#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUExpr.h"

#include "flow/Flow.h"
#include "flow/FLNodeElab.h"

#include "schedule/Schedule.h"
#include "Util.h"
#include "Dump.h"
#include "MarkDesign.h"
#include "ScheduleData.h"
#include "UseDefHierPair.h"
#include "MergeTest.h"
#include "SequentialBlockMerge.h"

SCHSequentialBlockMerge::SCHSequentialBlockMerge(SCHUtil* util,
                                                 SCHMarkDesign* mark,
                                                 SCHScheduleData* data,
                                                 bool reverseEdge) :
  SCHBlockMerge(util, mark, data, reverseEdge)
{
  mScheduledBlocks = new ScheduledBlocks;
  std::memset(mAdjMergeFails, 0, sizeof(mAdjMergeFails));
  mValidOrder = NULL;
  mMergeTest = new SCHMergeTest(mark, NULL, true, false);
}

SCHSequentialBlockMerge::~SCHSequentialBlockMerge()
{
  for (ScheduledBlocksLoop l(*mScheduledBlocks); !l.atEnd(); ++l) {
    FLNodeElabVector* nodes = l.getValue();
    delete nodes;
  }
  delete mScheduledBlocks;
  delete mMergeTest;
}

void SCHSequentialBlockMerge::merge(ValidOrder* validOrder)
{
  bool doMerge = mUtil->isFlagSet(eMergeBlocks);
  if (doMerge) {
    // Initialize merging
    mValidOrder = validOrder;
    initLocalData();

    // Do the actual merge by calling the SCHBlockMerge class to do it
    int dumpBlockMergeInfo = mUtil->getDumpMergedBlocks();
    if (dumpBlockMergeInfo != 0) {
      UtIO::cout() << "Merging sequential blocks:\n";
    }
    mergeBlocks(dumpBlockMergeInfo, SCHMergeMask(eMTDepthOrder));

    // Print why some blocks are unmergable
    if (dumpBlockMergeInfo != 0) {
      printResults();
    }
  }
}

void SCHSequentialBlockMerge::initLocalData()
{
  // Call the SCHBlockMerge::start function to have it initialize itself
  start();

  // Add all the scheduled blocks to the merge process
  SCHSchedulesLoop p;
  for (p = mScheduleData->loopAllSequentials(); !p.atEnd(); ++p) {
    SCHSequential* seq = p.getSequentialSchedule();
    gatherScheduleBlocks(seq, eClockPosedge);
    gatherScheduleBlocks(seq, eClockNegedge);
  }
}

void
SCHSequentialBlockMerge::gatherScheduleBlocks(SCHSequential* seq, ClockEdge edge)
{
  if (!seq->empty(edge)) {
    // Gather all the mergable blocks sorted by usedef
    SCHSequentialEdge* seqEdge = seq->getSequentialEdge(edge);
    for (SCHIterator f = seqEdge->getSchedule(); !f.atEnd(); ++f) {
      // Gather the data on this block
      FLNodeElab* flowElab = *f;
      createMergeBlock(flowElab, seqEdge);
    } // for
  } // if
} // SCHSequentialBlockMerge::gatherScheduleBlocks

FLNodeElabVector*
SCHSequentialBlockMerge::createMergeBlock(FLNodeElab* flowElab, 
                                          SCHScheduleBase* sched)
{
  // Gather the data on this block
  SCHUseDefHierPair udHierPair(flowElab);

  // Get or allocate space for this scheduled block
  FLNodeElabVector* nodes;
  ScheduledBlocks::iterator pos = mScheduledBlocks->find(udHierPair);
  if (pos == mScheduledBlocks->end()) {
    nodes = new FLNodeElabVector;
    mScheduledBlocks->insert(ScheduledBlocks::value_type(udHierPair, nodes));
    createBlock(flowElab, nodes, sched);
  } else {
    nodes = pos->second;
  }

  // Add the node to this block
  nodes->push_back(flowElab);
  return nodes;
}

bool 
SCHSequentialBlockMerge::blockMergable(const NUUseDefNode* useDef, MergeBlock* block)
  const
{
  return mMergeTest->mergable(useDef, block->getGroups());
} // SCHSequentialBlockMerge::blockMergable

bool
SCHSequentialBlockMerge::edgeMergable(const MergeEdge* edge,
                                      const MergeBlock* faninBlock,
                                      const MergeBlock* fanoutBlock,
                                      bool dumpMergedBlocks) const
{
  // The fanin block must be mergable, otherwise give up. Not for
  // sequential nodes we merge them sorted be reverse fanin. So the
  // fanout block would get prepended to the fanin block.
  if (!faninBlock->isMergable()) {
    return false;
  }

  // Check if the fanout is mergable
  AdjMergeFail reason = eNumAdjMergeFails;
  bool mergable = false;
  if (!fanoutBlock->isMergable()) {
    // Can't merge with an umergable block
    reason = eUnmergable;

  } else if (edge->getCrossesHierarchy()) {
    // The blocks are in different hierarchy (it would introduce a cycle)
    reason = eCrossesHier;

  } else if (MergeBlock::compareHierScheduleSets(fanoutBlock, faninBlock) != 0) {
    reason = eUnmatchHierSched;

  } else {
    mergable = true;
  }

  // Update the stats and return if it is mergable
  if (!mergable) {
    if (dumpMergedBlocks) {
      if (reversedEdges()) {
        recordFailedMerge(faninBlock, fanoutBlock, reason);
      } else {
        recordFailedMerge(fanoutBlock, faninBlock, reason);
      }
    }
    return false;
  } else {
    return true;
  }
} // SCHSequentialBlockMerge::edgeMergable

int
SCHSequentialBlockMerge::compareBuckets(const MergeBlock* block1,
                                        const MergeBlock* block2) const
{
  int cmp = MergeBlock::compareHierScheduleSets(block1, block2);
  return cmp;
}

void
SCHSequentialBlockMerge::getBlockFanin(MergeBlock* block,
                                       BlockSet* faninSet) const
{
  // Visit the elaborated flow to create the block flow
  for (SCHFlowGroups::FlowsLoop f = block->loopFlowNodes(); !f.atEnd(); ++f) {
    // Only visit the level fanin because clock ordering has already
    // been solved by the derived clock schedules code. Here we are
    // only adding the reverse edge for all level fanin because we
    // want to execute flops that feed each other in reverse order.
    FLNodeElab* flowElab = *f;
    for (SCHMarkDesign::LevelFaninLoop l = mMarkDesign->loopLevelFanin(flowElab);
         !l.atEnd(); ++l) {
      FLNodeElab* faninElab = *l;
      if (SCHUtil::isSequential(faninElab) &&
          (*mValidOrder)(flowElab, faninElab)) {
        for (BlocksLoop b = findFlowBlocks(faninElab); !b.atEnd(); ++b) {
          // Add this as a fanin as long as it isn't the same block
          MergeBlock* faninBlock = *b;
          if (faninBlock != block) {
            faninSet->insert(faninBlock);
          }
        }
      }
    }
  }
} // SCHSequentialBlockMerge::getBlockFanout

void 
SCHSequentialBlockMerge::postMergeCallback(MergeBlock* lastBlock, 
                                           MergeBlock* block)
{
  // Put the first set of groups in a map from hierarchy to the
  // group. There should only be one group for each hierarchy.
  typedef UtMap<const STBranchNode*, FLNodeElabVector*> GroupMap;
  GroupMap groupMap;
  const NUUseDefNode* useDef = NULL;
  for (SCHFlowGroupsLoop g = lastBlock->loopGroups(); !g.atEnd(); ++g) {
    // Gather the data
    FLNodeElabVector* nodes = *g;
    FLNodeElab* flow = *nodes->begin();
    const STBranchNode* hier = flow->getHier();

    // Store the block so we can make sure we are merging the right
    // flow nodes
    if (useDef == NULL)
      useDef = flow->getUseDefNode();
    else
      FLN_ELAB_ASSERT(useDef == flow->getUseDefNode(), flow);

    // Insert the information
    FLN_ELAB_ASSERT(groupMap.find(hier) == groupMap.end(), flow);
    groupMap.insert(GroupMap::value_type(hier, nodes));
  }

  // Move the nodes from the second block to the last block
  for (SCHFlowGroupsLoop g = block->loopGroups(); !g.atEnd(); ++g) {
    // Figure out which group it should be in
    FLNodeElabVector* nodes2 = *g;
    FLNodeElab* flow = *nodes2->begin();
    const STBranchNode* hier = flow->getHier();
    FLN_ELAB_ASSERT(useDef == flow->getUseDefNode(), flow);

    // Get the group
    GroupMap::iterator pos = groupMap.find(hier);
    if (pos != groupMap.end()) {
      // Move the flows from nodes2 to nodes1
      FLNodeElabVector* nodes1 = pos->second;
      moveFlows(nodes1, nodes2);
      nodes2->clear();
    }
  }
} // SCHSequentialBlockMerge::postMergeCallback

void
SCHSequentialBlockMerge::moveFlows(FLNodeElabVector* dst, FLNodeElabVector* src)
{
  dst->insert(dst->end(), src->begin(), src->end());
}

void
SCHSequentialBlockMerge::recordFailedMerge(const MergeBlock* faninBlock,
                                           const MergeBlock* fanoutBlock,
                                           AdjMergeFail reason) const
{
  // Gather the elaborated flow for both blocks and sort them
  FLNodeElabVector fanoutNodes;
  SCHBlockMerge::gatherBlockFlows(fanoutBlock, &fanoutNodes);
  FLNodeElabVector faninNodes;
  SCHBlockMerge::gatherBlockFlows(faninBlock, &faninNodes);
  std::sort(fanoutNodes.begin(), fanoutNodes.end(), SCHUtil::lessFlow);
  std::sort(faninNodes.begin(), faninNodes.end(), SCHUtil::lessFlow);

  // Record this as a new failed merge
  ++mAdjMergeFails[reason];

  // Print the fanout block
  UtIO::cout() << "Failed merge for Fanout Block:\n";
  printBlockNodes(fanoutNodes);

  // Print the fanin block
  UtIO::cout() << "Fanin Block (reason: " << adjFailReason(reason) << "):\n";
  printBlockNodes(faninNodes);
}

const char*
SCHSequentialBlockMerge::adjFailReason(AdjMergeFail adjMergeFail) const
{
  const char* reason = NULL;
  switch(adjMergeFail) {
    case eUnmergable:
      reason = "Unmergable";
      break;

    case eCrossesHier:
      reason = "CrossesHier";
      break;

    case eUnmatchHierSched:
      reason = "UnmatchHierSched";
      break;

    default:
      INFO_ASSERT(0, "Unknown adjacent merge fail reason");
      break;
  }

  return reason;
}

void
SCHSequentialBlockMerge::printBlockNodes(const FLNodeElabVector& nodes) const
{
  for (FLNodeElabVectorIter f = nodes.begin(); f != nodes.end(); ++f) {
    FLNodeElab* flow = *f;
    UtString flowName;
    SCHDump::composeFlowName(flow, &flowName);
    UtIO::cout() << "  " << flowName << "\n";
  }
}

void SCHSequentialBlockMerge::printResults()
{
  // Print info about starting mergable vs unmergable blocks
  mMergeTest->printStats();

  // Print info about adjacency failures
  printAdjacencyFailures();
} // void SCHSequentialBlockMerge::printResults

void SCHSequentialBlockMerge::printAdjacencyFailures() const
{
  int count = 0;
  for (int i = 0; i < eNumAdjMergeFails; ++i) {
    count += mAdjMergeFails[i];
  }
  if (count > 0) {
    UtIO::cout()
      << "Failed adjacency blocks:"
      << "\n  Unmergable      : " << mAdjMergeFails[eUnmergable]
      << "\n  CrossesHier     : " << mAdjMergeFails[eCrossesHier]
      << "\n  UnmatchHierSched: " << mAdjMergeFails[eUnmatchHierSched]
      << "\n";
  }
}

bool
SCHSequentialBlockMerge::validStartBlock(const NUUseDefNode*, MergeBlock*) const
{
  // All blocks are valid start points for merging. Only mixed block
  // merging cares about this.
  return true;
}

void SCHSequentialBlockMerge::recordCycleBlock(const MergeBlock*) const
{
  mMergeTest->recordCycleBlock();
}
