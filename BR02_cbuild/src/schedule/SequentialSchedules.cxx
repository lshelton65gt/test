// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/CarbonContext.h"

#include "util/UtIOStream.h"
#include "util/Stats.h"
#include "util/DynBitVector.h"
#include "util/CbuildMsgContext.h"
#include "util/CarbonTypeUtil.h"

#include "schedule/Schedule.h"
#include "schedule/Event.h"
#include "schedule/ScheduleMask.h"

#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"

#include "localflow/UD.h"

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUDesign.h"

#include "symtab/STAliasedLeafNode.h"

#include "reduce/RETransform.h"
#include "reduce/CleanElabFlow.h"

#include "Util.h"
#include "Depth.h"
#include "MarkDesign.h"
#include "ScheduleData.h"
#include "CreateSchedules.h"
#include "BlockSplit.h"
#include "Dump.h"
#include "InputNets.h"
#include "SortSchedules.h"
#include "AddBuffer.h"
#include "SequentialBlockMerge.h"
#include "MixedBlockMerge.h"
#include "ClkAnal.h"
#include "BranchNets.h"
#include "TimingAnalysis.h"
#include "SequentialSchedules.h"
#include "BlockFlowNodes.h"


/*!
  \file

  This file contains routines to create and order the sequential
  schedules. The main routine is create.
*/

// Local scratch flags
#define SS_IN_CYCLE	(1<<0)
#define SS_DONE		(1<<1)

// Constructor
SCHSequentialSchedules::SCHSequentialSchedules(SCHUtil* util,
					       SCHMarkDesign* markDesign,
                                               SCHScheduleData* scheduleData,
					       SCHDump* dump,
					       SCHTimingAnalysis* timing)
{
  mOrder = 0;
  mUtil = util;
  mMarkDesign = markDesign;
  mScheduleData = scheduleData;
  mDump = dump;
  mTimingAnalysis = timing;

  mBlockSplit = new SCHBlockSplit(util, markDesign, scheduleData);
  mBlocks = new Blocks;
  mFlowToBlockMap = new FlowToBlockMap;
  mArcStack = new ArcStack;
  mBlockInstMap = new BlockInstMap;
  mFlowInstMap = new FlowInstMap;
  mSequentialStack = new SequentialStack;
  mSequentialsInStack = new Sequentials;
  mDepthMap = new DepthMap;
  mSequentialFlow = new SequentialFlow;
  mDoubleBufferPairs = new DoubleBufferPairs;
  mDoubleBufferReason = new DoubleBufferReason;
}

// Destructor
SCHSequentialSchedules::~SCHSequentialSchedules()
{
  delete mBlockSplit;
  delete mBlocks;
  delete mFlowToBlockMap;
  delete mArcStack;
  delete mBlockInstMap;
  delete mFlowInstMap;
  delete mSequentialStack;
  delete mSequentialsInStack;
  delete mDepthMap;
  delete mSequentialFlow;
  delete mDoubleBufferPairs;
  delete mDoubleBufferReason;
}

// Add a sequential node to the right schedule
void
SCHSequentialSchedules::schedule(FLNodeElab* flow, SCHSequentials* sequentials,
                                 SCHDerivedClockLogic* parentDCL)
{
  // Don't schedule inactive flops
  if (flow->isInactive()) {
    return;
  }

  const SCHScheduleMask* extraMask;
  extraMask = mMarkDesign->findLevelFaninAsyncEdgeMask(flow);
  sequentials->scheduleSequentialNode(flow, flow->getUseDefNode(), parentDCL,
                                      extraMask, mMarkDesign);
}

// Create the sequential node schedules
bool SCHSequentialSchedules::create(const char* fileRoot)
{
  // If we are timing passes, start a new timer
  Stats* stats = mUtil->getStats();
  if (stats != NULL)
    stats->pushIntervalTimer();

  // Schedule all the nodes by putting them in the appropriate
  // sequential schedule
  SCHSequentials* sequentials = mScheduleData->getSequentials();
  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s) {
    FLNodeElab* flow = *s;

    // Don't schedule nodes that are part of the clock logic
    if (!flow->isDerivedClock() && !flow->isDerivedClockLogic())
      schedule(flow, sequentials, NULL);
  }
  if (stats != NULL) stats->printIntervalStatistics("Seq Sched");

  // Create a map from flow nodes to the sequential schedule and edge
  // they are in. This is used by the ordering code below. We need it
  // to order schedule and nodes within the schedule (determineOrder
  // and determineNodeOrder).
  createFlowToScheduleMap();

  // Class to compute branch nets for clocks
  SCHBranchNets branchNets(mMarkDesign, SCHBranchNetFlag(eIncludeClock));
  branchNets.init();

  // The sequential schedules have been created. Find an ordering
  // between the schedules. We must go through the derived clocks
  // first since they run before normal sequential schedules.
  createBlockInstMap();
  SCHSchedule::DerivedClockLogicLoop p;
  INFO_ASSERT(mOrder == 0, "Duplicate call to sequential schedules sorting");
  ++mOrder;
  for (p = mScheduleData->loopDerivedClockLogic(); !p.atEnd(); ++p) {
    SCHDerivedClockBase* dclBase = *p;
    SCHSequentialsVector sequentialsVector;
    dclBase->getSequentialSchedules(&sequentialsVector);
    for (SCHSequentialsVectorLoop l(sequentialsVector); !l.atEnd(); ++l) {
      SCHSequentials* seqs = *l;
      determineOrder(seqs, &branchNets);
    }
  }
  determineOrder(sequentials, &branchNets);
  deleteBlockInstMap();
  if (stats != NULL) stats->printIntervalStatistics("Seq Order");

  // At this point we need to check if a usedef node is in different
  // sequential schedules. This can occur if two flow nodes for the
  // same usedef node and hierarchy are in different schedules. The
  // only possible scenario is if one output of the always block
  // belongs in a derived clock logic schedule and the other
  // doesn't. This is currently an error but at some point we have to
  // support this.
  bool ok = sequentialBlocksInMultipleSchedules();
  if (stats != NULL) stats->printIntervalStatistics("Seq Multi");
  if (!ok)
  {
  if (stats != NULL)
    stats->popIntervalTimer();
    return false;
  }
  
  {
    // sanity check: ClkPI should not produce any dead elaborated nodes.
    FLNodeElabSet should_be_empty;
    CleanElabFlow cleanup(mUtil->getFlowElabFactory(),
			  mUtil->getSymbolTable(),
			  false);
    cleanup.design(mUtil->getDesign(),&should_be_empty, false);
    if (!should_be_empty.empty()) {
      // Get the first elaborated flow in the set to use for the assert
      FLNodeElab* flowElab = *(should_be_empty.begin());
      FLN_ELAB_ASSERT(("adding clk and pi buffers caused dead flow" == NULL),
                      flowElab);
    }
  }
  if (stats != NULL) stats->printIntervalStatistics("Seq Sanity");

  // Find an ordering for the nodes within a schedule. In doing so, we
  // decide which sequential nodes must be double buffered.
  NUNetElabSet delayedNets;
  determineNodeOrder(&delayedNets);
  if (stats != NULL) stats->printIntervalStatistics("State Upd1");

  // Since the nodes are ordered, we can now do block merging on the
  // various schedules. Start with sequential block merging
  bool dumpVerilog = mUtil->getArgs()->getBoolValue(CRYPT("-dumpVerilog"));
  MsgContext* msgContext = mUtil->getMsgContext();
  if (dumpVerilog) {
    mUtil->getDesign()->dumpVerilog(fileRoot, "schedulePreSeqMerge", msgContext);
  }
  ValidSeqOrder validSeqOrder(this);
  SCHSequentialBlockMerge blockMerge(mUtil, mMarkDesign, mScheduleData);
  blockMerge.merge(&validSeqOrder);
  if (dumpVerilog) {
    mUtil->getDesign()->dumpVerilog(fileRoot, "schedulePostSeqMerge", msgContext);
  }
  if (stats != NULL) stats->printIntervalStatistics("Seq Merge");

  // Do the mixed sequential/combinational merging now
  if (dumpVerilog) {
    mUtil->getDesign()->dumpVerilog(fileRoot, "schedulePreMixMerge", msgContext);
  }
  SCHMixedBlockMerge mixedBlockMerge(mUtil, mMarkDesign, mScheduleData,
                                     mTimingAnalysis);
  mixedBlockMerge.merge(&delayedNets);
  if (dumpVerilog) {
    mUtil->getDesign()->dumpVerilog(fileRoot, "schedulePostMixMerge", msgContext);
  }
  if (stats != NULL) stats->printIntervalStatistics("Mixed Merge");

  // Update the sequential schedule flop map
  deleteFlowToScheduleMap();
  createFlowToScheduleMap();

  // I'm not sure this is a good idea. But this code will try to
  // update any new combinational flow's signature. The problem is it
  // may or may not be consistent with the elaborated flow that is
  // already computed. So for now it is disabled. The real fix is to
  // make sure we don't do anything in the scheduler that creates new
  // elaborated flow in the graph.
#if 0
  // Fixup timing for all fanins of flops, mixed block merging can
  // create new elaborated combinational/bound node flows.
  fixupCombFlow();
#endif

  // Add buffers again to make up for merging
  determineNodeOrder(NULL);
  if (stats != NULL) stats->printIntervalStatistics("State Upd2");

  // Merging may change the depth of some nodes. Recompute it for
  // sortings
  recomputeBlockDepth();
  mDoubleBufferReason->clear();
  deleteFlowToScheduleMap();
  if (stats != NULL) stats->printIntervalStatistics("Seq Depth2");
  if (stats != NULL) stats->popIntervalTimer();
  return true;
} // SCHSequentialSchedules::create

void
SCHSequentialSchedules::finish(SCHSequentials* sequentials)
{
  // If we are timing passes, start a new timer
  Stats* stats = mUtil->getStats();
  if (stats != NULL)
    stats->pushIntervalTimer();

  // Now that we are done checking for the error cases and any newly
  // dead sequentials have been removed, we can clean up the various
  // sequential schedules such that we only have one flow node for the
  // same usedef/hier pair in any sequential schedule. We couldn't
  // eliminate the duplicates before because it was needed by the
  // check just above.
  removeDuplicateSequentialNodes();

  // Sort the sequential nodes by their ordering
  sequentials->sortNodes();
  SCHSchedule::DerivedClockLogicLoop p;
  for (p = mScheduleData->loopDerivedClockLogic(); !p.atEnd(); ++p) {
    SCHDerivedClockBase* dclBase = *p;
    SCHDerivedClockLogic::SequentialLoop s;
    for (s = dclBase->loopMultiSequential(); !s.atEnd(); ++s) {
      SCHSequential* seq = *s;
      seq->sortNodes();
    }
  }
  if (stats != NULL) stats->printIntervalStatistics("Seq Sort");

  // Remove any sequential schedules with inactive clocks. We do this
  // at the end of the sequential scheduling so that all the passes
  // work correctly. There may be a better way to do this.
  sequentials->removeInactiveSchedules();
  for (p = mScheduleData->loopDerivedClockLogic(); !p.atEnd(); ++p) {
    SCHDerivedClockBase* dclBase = *p;
    SCHSequentialsVector sequentials;
    dclBase->getSequentialSchedules(&sequentials);
    for (SCHSequentialsVectorLoop l(sequentials); !l.atEnd(); ++l) {
      SCHSequentials* sequentials = *l;
      sequentials->removeInactiveSchedules();
    }
  }
  if (stats != NULL) stats->printIntervalStatistics("Seq Inactive");

  // Make sure all the schedules have valid clocks
  SCHSchedulesLoop s;
  for (s = mScheduleData->loopAllSequentials(); !s.atEnd(); ++s) {
    // Make sure the clock is valid
    SCHSequential* seq = s.getSequentialSchedule();
    const NUNetElab* clkElab = seq->getEdgeNet();
    if (!mMarkDesign->isValidClock(clkElab)) {
      // Call a function that prints information and then aborts
      mMarkDesign->analyzeInvalidScheduleClk(seq, clkElab);
    }

    // make sure all the async resets are valid
    const SCHScheduleMask* asyncMask = seq->getAsyncMask();
    if (asyncMask != NULL) {
      for (SCHScheduleMask::UnsortedEvents e = asyncMask->loopEvents();
           !e.atEnd(); ++e) {
        const SCHEvent* event = *e;
        if (event->isClockEvent()) {
          const STSymbolTableNode* resetName = event->getClock();
          const NUNetElab* resetElab = SCHSchedule::clockFromName(resetName);
          NU_ASSERT(mMarkDesign->isValidClock(resetElab), resetElab);
        }
      }
    }

    // Make sure the extra conditions are valid
    const SCHScheduleMask* extraEdgesMask = seq->getExtraEdgesMask();
    if (extraEdgesMask != NULL) {
      for (SCHScheduleMask::UnsortedEvents e = extraEdgesMask->loopEvents();
           !e.atEnd(); ++e) {
        const SCHEvent* event = *e;
        if (event->isClockEvent()) {
          const STSymbolTableNode* extraEdgeName = event->getClock();
          const NUNetElab* extraEdge = SCHSchedule::clockFromName(extraEdgeName);
          NU_ASSERT(mMarkDesign->isValidClock(extraEdge), extraEdge);
        }
      }
    }
  } // for
  if (stats != NULL) stats->printIntervalStatistics("Seq Clk Verify");
  
  if (stats != NULL) stats->popIntervalTimer();
} // SCHSequentialSchedules::finish

bool SCHSequentialSchedules::sequentialBlocksInMultipleSchedules()
{
  // Use the block split class to find the blocks that are in multiple
  // schedules.
  mBlockSplit->findMultiTimingBlocks(mScheduleData->loopAllSequentials());

  // print errors and return failure
  return !printMultiScheduleBlocks();
}


void SCHSequentialSchedules::determineNodeOrder(NUNetElabSet* delayedNets)
{
  // If we are timing passes, start a new timer
  Stats* stats = mUtil->getStats();
  if (stats != NULL)
    stats->pushIntervalTimer();

  // The following passes use a sorted set of flow nodes for every
  // sequential flow node. That sorted set are all the flow nodes for
  // the same always block instance (usedef/hier pair).
  createBlockInstMap();
  createSortedSequentialFlowMap();
  if (stats != NULL) stats->printIntervalStatistics("SU Init");

  // Create an ordering for sequential nodes so that we can do as
  // little double buffering as possible. As part of this ordering
  // process we mark the sequential nodes that need double
  // buffering. This is done by setting their depth to
  // DEPTH_SEQ_DOUBLE_BUFFER.
  calculateSequentialOrdering();
  if (stats != NULL) stats->printIntervalStatistics("SU SeqOrder");

  // Find all the output flow nodes that need to read the double
  // buffered location and add the buffer
  addDoubleBuffers(delayedNets);
  if (stats != NULL) stats->printIntervalStatistics("SU Add Buffers");

  // We are all done with the sorted sequential schedules
  deleteSortedSequentialFlowMap();
  deleteBlockInstMap();

  // We don't need the flow to sequential schedule map anymore
  if (stats != NULL) {
    stats->printIntervalStatistics("SU Cleanup");
    stats->popIntervalTimer();
  }
}

void
SCHSequentialSchedules::createSortedSequentialFlowMap()
{
  INFO_ASSERT(mSortedSequentialFlowsMap.empty(),
              "Duplicate call to createSortedSequentialFlowMap()");

  // Create the map to get all the flow nodes from an always block
  SCHBlockFlowNodes blockFlowNodes(mUtil);

  // Go through all the sequential flow nodes
  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s) {
    // Only do the work if it is not in the map
    FLNodeElab* flow = *s;
    if (mSortedSequentialFlowsMap.find(flow) == mSortedSequentialFlowsMap.end()) {
      // Get all the flow nodes for this always block instance
      SortedSequentialFlows* ssf = new SortedSequentialFlows;
      FLNodeElabVector* nodeVector = ssf->getNodes();
      blockFlowNodes.getFlows(flow, nodeVector, &FLNodeElab::isLive);

      // Sort them for stable traversal
      std::sort(nodeVector->begin(), nodeVector->end(), SCHUtil::lessFlow);

      // Add them to the map for all outputs of the always block
      for (FLNodeElabVectorLoop l(*nodeVector); !l.atEnd(); ++l) {
        FLNodeElab* curFlow = *l;
        SortedSequentialFlowsMap::iterator location;
        location = mSortedSequentialFlowsMap.find(curFlow);
        FLN_ELAB_ASSERT(location == mSortedSequentialFlowsMap.end(), curFlow);
        mSortedSequentialFlowsMap[curFlow] = ssf;
        ssf->incRefCnt();
      }

      // Make sure we had the original node in the list
      FLN_ELAB_ASSERT(mSortedSequentialFlowsMap.find(flow) !=
                      mSortedSequentialFlowsMap.end(), flow);
    } // mSortedSequentialFlowsMap.end
  } // for
} // void SCHSequentialSchedules::createSortedSequentialFlowMap

void SCHSequentialSchedules::deleteSortedSequentialFlowMap()
{
  SortedSequentialFlowsMap::iterator pos;
  for (pos = mSortedSequentialFlowsMap.begin();
       pos != mSortedSequentialFlowsMap.end();
       ++pos)
  {
    SortedSequentialFlows* ssf = pos->second;
    if (ssf->decRefCnt() == 0)
      delete ssf;
  }
  mSortedSequentialFlowsMap.clear();
}

void SCHSequentialSchedules::calculateSequentialOrdering()
{
  // If we are timing passes, start a new timer
  Stats* stats = mUtil->getStats();
  if (stats != NULL)
    stats->pushIntervalTimer();

  // Traverse the list of sequential nodes and reset their depth to
  // uninitialized
  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s)
  {
    FLNodeElab* flow = *s;
    flow->putDepth(DEPTH_UNINITIALIZED);
  }
  if (stats != NULL) stats->printIntervalStatistics("Order Init");

  // Find and mark flow nodes that need to be double buffered which
  // are in different schedules. This occurs if one of the flow nodes
  // feeds the other and the fanin node's schedule runs first.
  // The double buffer nodes are put in the double buffer set
  findInterScheduleDoubleBuffers();
  if (stats != NULL) stats->printIntervalStatistics("SU InterSched");

  // Find and mark flow nodes that need to be double buffered within
  // one schedule due to block cycles.
  //
  // One this is done all sequential nodes will have a depth that
  // allows us to sort them.
  findBlockCycleDoubleBuffers();
  if (stats != NULL) stats->printIntervalStatistics("SU Cycle");

  // We don't need the block flow information anymore
  deleteSequentialBlockFlow();
  if (stats != NULL) {
    stats->printIntervalStatistics("Order Clean");
    stats->popIntervalTimer();
  }
} // bool SCHSequentialSchedules::calculateSequentialOrdering

void
SCHSequentialSchedules::findInterScheduleDoubleBuffers()
{
  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s)
  {
    FLNodeElab* flow = *s;
    ClockEdge edge;
    SCHSequential* sequential = findSequentialSchedule(flow, &edge);
    for (SCHMarkDesign::LevelFaninLoop p = mMarkDesign->loopLevelFanin(flow);
         !p.atEnd(); ++p)
    {
      FLNodeElab* fanin = *p;
      if (validSequentialFanin(flow, fanin) && !fanin->isInactive() &&
          !fanin->isClock())
      {
	ScheduleOrder order;
	order = determineScheduleOrder(fanin, edge, sequential);
	if (order == eOrderAfter)
	  // The flow node's schedule runs after this fanin's schedule
	  addDoubleBuffered(flow, fanin, eMultiClk);
      }
    }
  } // for
} // SCHSequentialSchedules::findInterScheduleDoubleBuffers

void
SCHSequentialSchedules::findBlockCycleDoubleBuffers()
{
  // Create the set of blocks and flow for all the sequential blocks.
  createSequentialBlockFlow();

  // Visit the blocks and break the cycles by marking flow nodes
  // double buffer
  for (BlocksIter b = mBlocks->begin(); b != mBlocks->end(); ++b)
  {
    Block* block = b->second;
    breakSequentialCycles(block);
  }

  // Traverse the set of blocks again and compute their depth.
  for (BlocksIter b = mBlocks->begin(); b != mBlocks->end(); ++b)
  {
    Block* block = b->second;
    computeBlockDepth(block);
  }
}

void
SCHSequentialSchedules::createSequentialBlockFlow()
{
  // Make sure we haven't done this already
  INFO_ASSERT(mBlocks->empty() && mFlowToBlockMap->empty(),
              "Duplicate call to createSequentialBlockFlow()");

  // Create the set of blocks and flow for all the sequential
  // blocks. We do this first so that they are all created when we
  // need to create the block flow below.
  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s)
  {
    // Find the block for this flow node
    FLNodeElab* flow = *s;
    SCHUseDefHierPair udhp(flow->getUseDefNode(), flow->getHier());
    BlocksIter pos = mBlocks->find(udhp);
    Block* block;
    if (pos == mBlocks->end())
    {
      // It hasn't been created yet, do so
      block = new Block(flow);
      mBlocks->insert(Blocks::value_type(udhp, block));
    }
    else
      // Found it
      block = pos->second;

    // Add to the reverse map. We use this for traversals
    mFlowToBlockMap->insert(FlowToBlockMap::value_type(flow, block));
  }

  // Create the block flow. We only create fanin if the two blocks
  // flow directly into each other and they are in the same schedule
  // and edge and the fanin is not marked double buffer already.
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s)
  {
    // Find the block for this flow node
    FLNodeElab* flow = *s;
    FlowToBlockMap::iterator pos = mFlowToBlockMap->find(flow);
    FLN_ELAB_ASSERT(pos != mFlowToBlockMap->end(), flow);
    Block* block = pos->second;

    // Add all the fanin for this flow node as appropriate
    ClockEdge edge = ClockEdge (0);
    SCHSequential* sequential = findSequentialSchedule(flow, &edge);
    for (SCHMarkDesign::LevelFaninLoop p = mMarkDesign->loopLevelFanin(flow);
         !p.atEnd(); ++p)
    {
      FLNodeElab* fanin = *p;
      if (validSequentialFanin(flow, fanin) &&
	  (determineScheduleOrder(fanin, edge, sequential) == eOrderSame) &&
          !bufferAdded(flow, fanin)) {
	pos = mFlowToBlockMap->find(fanin);
	FLN_ELAB_ASSERT(pos != mFlowToBlockMap->end(), fanin);
	Block* faninBlock = pos->second;
	block->addFanin(faninBlock, fanin);
      }
    }
  } // for
} // SCHSequentialSchedules::createSequentialBlockFlow

void SCHSequentialSchedules::deleteSequentialBlockFlow()
{
  for (BlocksIter b = mBlocks->begin(); b != mBlocks->end(); ++b)
    delete b->second;
  mBlocks->clear();
  mFlowToBlockMap->clear();
}

bool 
SCHSequentialSchedules::bufferAdded(FLNodeElab* flowElab, FLNodeElab* faninElab)
  const
{
  NUNetElab* netElab = faninElab->getDefNet();
  if (netElab->getNet()->isDoubleBuffered()) {
    return true;
  } else {
    return doubleBufferReason(flowElab, faninElab) != eNotDoubleBuffered;
  }
}

SCHSequentialSchedules::Block*
SCHSequentialSchedules::breakSequentialCycles(Block* block)
{
  // Check if we have already visited this blocks fanin
  if (block->getDone() == true)
    return NULL;

  // The following types and data are used to clean up the flow after
  // breaking cycles.
  typedef std::pair<Block*, FLNodeElab*> RemoveEntry;
  typedef UtVector<RemoveEntry> RemoveList;
  typedef RemoveList::iterator RemoveListIter;
  RemoveList removeList;
  FLNodeElabSet covered;

  // Visit this blocks fanin looking for cycles. If breakPoint is
  // non-NULL we have found a spot to break the cycle. The variable
  // points to the fanout block where we want to break the cycle. This
  // is computed based on arc weight so that we break cycles the most
  // effectively.
  FLN_ELAB_ASSERT(block->getBusy() == false, block->getFlow());
  block->putBusy(true);
  Block* breakPoint = NULL;
  for (Block::FaninLoop l = block->loopFanin();
       !l.atEnd() && (breakPoint == NULL);
       ++l)
  {
    // Compute the weight of this fanin, this also tells us if we can
    // remove any fanins
    Block* faninBlock = l.getKey();
    FLNodeElabSet* faninSet = l.getValue();
    FLNodeElabSet removeSet;
    UInt32 weight = computeWeight(block, faninSet, removeSet);
    for (FLNodeElabSetIter i = removeSet.begin(); i != removeSet.end(); ++i)
    {
      FLNodeElab* fanin = *i;
      if (covered.find(fanin) == covered.end()) {
        removeList.push_back(RemoveEntry(faninBlock, fanin));
        covered.insert(fanin);
      }
    }

    // Visit the fanin if the arc isn't completely removed
    if (weight > 0)
    {
      // Check for cycles. If this fanin is in a cycle then find the
      // break point. Otherwise traverse the fanin looking for
      // cycles. It may return that we or some block in our stack is
      // the break point.
      if (faninBlock->getBusy() == true)
	// We did find a cycle, find the optimal spot to break the cycle
	breakPoint = findBreakPoint(block, weight, faninBlock);
      else
      {
	// We are about to visit our fanin looking for cycles, put the
	// arc and weight on the stack so that we can choose an
	// optimal spot to break this. Visit the fanin. When it
	// returns we can pop the arc off the stack.
	mArcStack->push_back(ArcWeight(block, weight));
	breakPoint = breakSequentialCycles(faninBlock);
	mArcStack->pop_back();
      }

      // Check if we should break the cycle here
      if (block == breakPoint)
      {
	// We found a cycle, mark that fanins as double buffer
	addDoubleBufferedBlocks(block, faninSet);
	FLNodeElabSetIter i;
	for (i = faninSet->begin(); i != faninSet->end(); ++i)
	{
	  FLNodeElab* fanin = *i;
          if (covered.find(fanin) == covered.end()) {
            removeList.push_back(RemoveEntry(faninBlock, fanin));
            covered.insert(fanin);
          }
	}

	// We are done breaking this cycle, we can continue traversals
	breakPoint = NULL;
      }
    } // if
  }
  block->putBusy(false);

  // Remove any fanin from breaking cycles now. We can't do it above
  // because we can't remove fanin when interating.
  for (RemoveListIter i = removeList.begin(); i != removeList.end(); ++i)
  {
    Block* faninBlock = (*i).first;
    FLNodeElab* fanin = (*i).second;
    block->removeFanin(faninBlock, fanin);
  }

  // There are two reasons we may return from this call. The first is
  // that we found no cycle so we should just return. The second is
  // that we found a cycle and it is better to break it in an arc from
  // a caller so we return the spot to break it.
  if (breakPoint == NULL)
    // We are done with this block and we didn't find a cycle
    block->putDone(true);
  return breakPoint;
} // SCHSequentialSchedules::breakSequentialCycles

SCHSequentialSchedules::Block*
SCHSequentialSchedules::findBreakPoint(Block* block, UInt32 weight,
				       Block* endBlock)
{
  // Start by assuming the current block is the break point
  UInt32 minWeight = weight;
  Block* breakPoint = block;

  // Iterate as long as we don't hit the current block again. We know
  // we will hit it because we are in a cycle.
  bool endSeen = false;
  ArcStack::reverse_iterator p;
  for (p = mArcStack->rbegin(); (p != mArcStack->rend()) && !endSeen; ++p)
  {
    // If this arc's weight is less, use that
    Block* curBlock = p->first;
    UInt32 curWeight = p->second;
    if (curWeight < minWeight)
    {
      minWeight = curWeight;
      breakPoint = curBlock;
    }

    // Mark if we have seen the end. This way we iterate all the way
    // and including the end block.
    endSeen = (curBlock == endBlock);
  }

  return breakPoint;
} // SCHSequentialSchedules::findBreakPoint

UInt32
SCHSequentialSchedules::computeWeight(Block* block, FLNodeElabSet* faninSet,
				      FLNodeElabSet& removeSet)
{
  // Get the set of flow nodes for the output block
  FLNodeElab* flow = block->getFlow();
  SortedSequentialFlows* ssf = mSortedSequentialFlowsMap[flow];
  FLNodeElabVector* nodes = ssf->getNodes();

  // Find the flows which have a flow node in the fanin set in their
  // fanin (got that?)
  UInt32 weight = 0;
  FLNodeElabSet newSet;
  for (FLNodeElabVector::iterator p = nodes->begin(); p != nodes->end(); ++p)
  {
    FLNodeElab* flow = *p;
    for (Fanin f = mUtil->loopFanin(flow); !f.atEnd(); ++f)
    {
      FLNodeElab* fanin = *f;
      if (SCHUtil::isSequential(fanin) &&
	  (faninSet->find(fanin) != faninSet->end()))
      {
	if (isDoubleBuffered(flow, fanin))
	{
	  // This was double buffered, we can reduce the fanin set
	  removeSet.insert(fanin);
	}
	else
	{
	  // This counts as weight to this arc
	  newSet.insert(fanin);
	  ++weight;
	}
      }
    }
  } // for

  return weight;
}

int
SCHSequentialSchedules::computeBlockDepth(Block* block)
{
  // We should have no cycles at this point
  FLN_ELAB_ASSERT(block->getBusy() == false, block->getFlow());

  // Check if this blocks depth is already set
  int depth = block->getDepth();
  if (depth > 0)
    return depth;

  // Visit the blocks fanin computing depth from there
  block->putBusy(true);
  depth = 0;
  for (Block::FaninLoop l = block->loopFanin(); !l.atEnd(); ++l)
  {
    // Get the fanin block
    Block* faninBlock = l.getKey();
    FLNodeElabSet* faninSet = l.getValue();
    FLN_ELAB_ASSERT(!faninSet->empty(), faninBlock->getFlow());

    // Visit it and compute the max depth
    int newDepth = computeBlockDepth(faninBlock);
    if (newDepth > depth) depth = newDepth;
  }
  block->putBusy(false);

  // Set the blocks depth and return it for our caller
  ++depth;
  FLNodeElab* flow = block->getFlow();
  FLN_ELAB_ASSERT(flow->getDepth() == DEPTH_UNINITIALIZED, flow);
  SortedSequentialFlows* ssf = mSortedSequentialFlowsMap[flow];
  FLN_ELAB_ASSERT(ssf != NULL, flow);
  FLNodeElabVector* nodes = ssf->getNodes();
  for (FLNodeElabVector::iterator p = nodes->begin(); p != nodes->end(); ++p)
  {
    FLNodeElab* flow = *p;
    flow->putDepth(depth);
  }
  FLN_ELAB_ASSERT(flow->getDepth() == depth, flow);
  return depth;
} // SCHSequentialSchedules::computeBlockDepth

SCHSequentialSchedules::ScheduleOrder
SCHSequentialSchedules::determineScheduleOrder(FLNodeElab* fanout,
                                               FLNodeElab* fanin) const
{
  // Get the fanout schedule and edge
  ClockEdge edge = ClockEdge (0); // initialized for GCC4 
  SCHSequential* fanoutSeq = findSequentialSchedule(fanout, &edge);

  // return the order
  return determineScheduleOrder(fanin, edge, fanoutSeq);
}

SCHSequentialSchedules::ScheduleOrder
SCHSequentialSchedules::determineScheduleOrder(FLNodeElab* flow,
					       ClockEdge edge,
					       SCHSequential* sequential1) const
{
  // Get the schedule for this flow node
  ClockEdge thisEdge;
  SCHSequential* sequential2 = findSequentialSchedule(flow, &thisEdge);
  if (sequential2 == NULL)
    // Un-schedule flow node (must have a constant clock)
    return eExclusive;

  // Check if they are the same schedule
  if (sequential1 == sequential2)
  {
    if (edge != thisEdge)
      return eExclusive;
    else
      return eOrderSame;
  }

  // Must not be the same, see if there is an order
  if (sequential1->getOrder() < sequential2->getOrder())
    return eOrderBefore;
  else
    return eOrderAfter;
} // ScheduleOrder determineScheduleOrder

void SCHSequentialSchedules::addFlowsToScheduleMap(SCHSequential* seq)
{
  SCHIterator iter;
  SequentialAndEdge* data;
  if (!seq->empty(eClockPosedge))
  {
    data = new SequentialAndEdge(seq, eClockPosedge);
    for (iter = seq->getSchedule(eClockPosedge); !iter.atEnd(); ++iter)
    {
      FLNodeElab* flow = *iter;
      addFlowToScheduleMap(flow, data);
    }
  }
  if (!seq->empty(eClockNegedge))
  {
    data = new SequentialAndEdge(seq, eClockNegedge);
    for (iter = seq->getSchedule(eClockNegedge); !iter.atEnd(); ++iter)
    {
      FLNodeElab* flow = *iter;
      addFlowToScheduleMap(flow, data);
    }
  }
} // void SCHSequentialSchedules::addFlowsToScheduleMap

void
SCHSequentialSchedules::addFlowToScheduleMap(FLNodeElab* flow,
					     SequentialAndEdge* data)
{
  data->incRefCnt();
  mFlowToSeqScheduleMap[flow] = data;
}

// This routine marks a flow nodes nets as double buffered. It finds
// all nets for the given netelab and marks them.
void SCHSequentialSchedules::markFlowDoubleBuffered(FLNodeElab* flow)
{
  NUNetVector nets;
  NUNetElab* netElab = flow->getDefNet();
  netElab->getAliasNets(&nets, true);

  for (NUNetVectorIter i = nets.begin(); i != nets.end(); ++i)
    {
      NUMemoryNet * mem = NULL;
      if (NUTempMemoryNet* tmem = dynamic_cast<NUTempMemoryNet*>(*i)) {
        tmem->putIsDynamic ();
        mem = tmem->getMaster ();
      }
      else
        mem = dynamic_cast<NUMemoryNet*>(*i);

      NU_ASSERT (mem, netElab);

      mem->putWrittenInForLoop(true);
      mem->putIsDoubleBuffered(true);
    }

  // the driver list had better include _flow_.
  bool encountered_provided_flow = false;

  for (NUNetElab::DriverLoop loop = netElab->loopContinuousDrivers();
       not loop.atEnd();
       ++loop) {
    FLNodeElab * driver = (*loop);

    if (flow==driver) encountered_provided_flow=true;
  }
  FLN_ELAB_ASSERT(encountered_provided_flow, flow);
}

void SCHSequentialSchedules::removeDuplicateSequentialNodes()
{
  SCHSchedulesLoop p;
  for (p = mScheduleData->loopAllSequentials(); !p.atEnd(); ++p) {
    SCHSequential* seq = p.getSequentialSchedule();
    seq->removeDuplicateNodes();
  }
}

SCHSequential*
SCHSequentialSchedules::findSequentialSchedule(FLNodeElab* flow,
					       ClockEdge *edge) const
{
  // Make sure this sequential is scheduled
  FlowToSeqScheduleMap::const_iterator pos = mFlowToSeqScheduleMap.find(flow);
  if (pos == mFlowToSeqScheduleMap.end())
    // Must be a sequential node with a constant clock
    return NULL;

  SequentialAndEdge* schedInfo = pos->second;
  *edge = schedInfo->getEdge();
  FLN_ELAB_ASSERT(schedInfo->getSchedule() != NULL, flow);
  return schedInfo->getSchedule();
}

bool SCHSequentialSchedules::printMultiScheduleBlocks()
{
  bool foundError = false;

  // Use the pass counter to keep track of places we printed error
  UInt32 newPass = mUtil->bumpSchedulePass();

  // Go through the list of blocks and print errors
  SCHBlockSplit::UnSplitBlocksLoop l;
  for (l = mBlockSplit->loopUnSplitBlocks(); !l.atEnd(); ++l)
  {
    SCHBlockSplit::BlockInstanceGroups* blockGroups = *l;

    // Print the error on multi schedule blocks. It's the only reason
    // we should have anything in this data set. We print the error on
    // the node(s) in the earliest sorted schedule (should be the
    // derived clock schedule node).
    foundError = true;

    // First look for the latest sorted schedule
    SCHBlockSplit::BlockInstanceGroups::iterator p;
    SCHSequential* seqSched = NULL;
    for (p = blockGroups->begin(); p != blockGroups->end(); ++p)
    {
      // Get the schedule for this node
      FLNodeElabVector& nodes = *p;
      for (FLNodeElabVectorIter n = nodes.begin(); n != nodes.end(); ++n)
      {
	FLNodeElab* flow = *n;
	const SCHBlockSplit::Schedules* schedules;
	schedules = mBlockSplit->getFlowSchedules(flow);
	FLN_ELAB_ASSERT(schedules->size() == 1, flow);
	SCHScheduleBase* schedBase = *(schedules->begin());
	SCHSequential* seq = dynamic_cast<SCHSequential*>(schedBase);

	// Check if this is the node we should print an error on
	if (seqSched == NULL)
	  seqSched = seq;
	else if (seqSched->getOrder() < seq->getOrder())
	  seqSched = seq;
      }
    }

    // Now go through the list again and print the message on the
    // other flow nodes.
    for (p = blockGroups->begin(); p != blockGroups->end(); ++p)
    {
      // Get the schedule for this node
      FLNodeElabVector& nodes = *p;
      for (FLNodeElabVectorIter n = nodes.begin(); n != nodes.end(); ++n)
      {
	FLNodeElab* flow = *n;
	const SCHBlockSplit::Schedules* schedules;
	schedules = mBlockSplit->getFlowSchedules(flow);
	FLN_ELAB_ASSERT(schedules->size() == 1, flow);
	SCHScheduleBase* schedBase = *(schedules->begin());
	SCHSequential* seq = dynamic_cast<SCHSequential*>(schedBase);

	// Print the error if this is the clock path and we haven't
	// printed it before.
	if ((seq != seqSched) &&
	    !mUtil->equalSchedulePass(flow->getSchedulePass()))
	{
	  SCHDerivedClockLogic* dcl = seq->getParentSchedule();
          const NUNetElab* clk = dcl->getRepresentativeClock();
	  HierName* clkName = clk->getSymNode();
	  mUtil->getMsgContext()->SchMultiSchedBlock(flow, *clkName);
	  mUtil->markNetFlows(flow);
	}
      }
    } // for
  } // for

  // Make sure no-one bumped this while we were running
  INFO_ASSERT(mUtil->equalSchedulePass(newPass),
              "Schedule pass counter incremented while dumping multi schedule blocks");

  return foundError;
} // bool SCHSequentialSchedules::printMultiScheduleBlocks

SCHSequentialSchedules::Block::Block(FLNodeElab* flow)
{
  mFlow = flow;
  mFanin = new Fanin;
  mDone = false;
  mBusy = false;
}

SCHSequentialSchedules::Block::~Block()
{
  for (Fanin::iterator p = mFanin->begin(); p != mFanin->end(); ++p)
    delete p->second;
  delete mFanin;
}

void SCHSequentialSchedules::Block::addFanin(Block* block, FLNodeElab* flow)
{
  // Check if we have added this block before
  Fanin::iterator pos = mFanin->find(block);
  FLNodeElabSet* faninSet;
  if (pos == mFanin->end())
  {
    // First time here, add the info
    faninSet = new FLNodeElabSet;
    mFanin->insert(Fanin::value_type(block, faninSet));
  }
  else
    // Already added
    faninSet = pos->second;

  // Add the flow to the fanin set. We use this so we know why the
  // block fanin is there.
  faninSet->insert(flow);
}

void SCHSequentialSchedules::Block::removeFanin(Block* block, FLNodeElab* flow)
{
  // Find the fanin block first. It should exist
  Fanin::iterator pos = mFanin->find(block);
  FLN_ELAB_ASSERT(pos != mFanin->end(), flow);
  FLNodeElabSet* faninSet = pos->second;

  // Remove this fanin flow. If we have removed all the fanins, then
  // we can delete this block fanin
  faninSet->erase(flow);
  if (faninSet->empty())
  {
    mFanin->erase(pos);
    delete faninSet;
  }
}

SCHSequentialSchedules::Block::FaninLoop
SCHSequentialSchedules::Block::loopFanin() const
{
  return FaninLoop(*mFanin);
}

int
SCHSequentialSchedules::Block::getDepth() const
{
  return mFlow->getDepth();
}

void
SCHSequentialSchedules::Block::print(SCHUtil* util) const
{
  // Create the map to get all the flow nodes from an always block
  // This will be slow so use print with care
  SCHBlockFlowNodes blockFlowNodes(util);

  // Get all the elaborated flow for this block and print them
  UtIO::cout() << "Block (" << this << "):\n";
  FLNodeElabVector nodes;
  blockFlowNodes.getFlows(mFlow, &nodes, &FLNodeElab::isLive);
  for (FLNodeElabVectorCLoop l(nodes); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    flowElab->print(1,2);
  }

  // Loop the fanin and print those
  for (FaninLoop f = loopFanin(); !f.atEnd(); ++f) {
  // Get all the elaborated flow for this block and print them
    Block* faninBlock = f.getKey();
    UtIO::cout() << "Fanin (" << faninBlock << "):\n";
    FLNodeElabVector nodes;
    blockFlowNodes.getFlows(faninBlock->getFlow(), &nodes, &FLNodeElab::isLive);
    for (FLNodeElabVectorCLoop l(nodes); !l.atEnd(); ++l) {
      FLNodeElab* flowElab = *l;
      flowElab->print(1,2);
    }
  }
} // SCHSequentialSchedules::Block::print
    
void
SCHSequentialSchedules::determineOrder(SCHSequentials* sequentials,
                                       SCHBranchNets* branchNets)
{
  // Make sure there is work to do
  if (sequentials->loopSequential().atEnd())
    return;

  // Keep track of the schedules in our current set
  Sequentials inSet;
  SCHSequentials::SequentialLoop l;
  for (l = sequentials->loopSequential(); !l.atEnd(); ++l)
  {
    SCHSequential* seq = *l;
    inSet.insert(seq);
  }

  // Create links between dependent schedules
  for (l = sequentials->loopSequential(); !l.atEnd(); ++l) {
    // Find the posedge dependencies
    SCHSequential* seq = *l;
    FLNodeElabSet covered;
    for (SCHNodeSetLoop n = seq->loopNodes(eClockPosedge); !n.atEnd(); ++n) {
      FLNodeElab* flow = *n;
      determineSequentialDependencies(seq, flow, covered, inSet);
    }

    // Find the negedge dependencies
    for (SCHNodeSetLoop n = seq->loopNodes(eClockNegedge); !n.atEnd(); ++n) {
      FLNodeElab* flow = *n;
      determineSequentialDependencies(seq, flow, covered, inSet);
    }
  }
  inSet.clear();

  // Break any cycles in the order
  Sequentials covered;
  for (l = sequentials->loopSequential(); !l.atEnd(); ++l)
  {
    SCHSequential* seq = *l;
    Flow flow(seq, 0);
    breakSequentialCycles(&flow, covered);
  }
  covered.clear();

  // Sort the sequential schedules according to their flow, the branch
  // nets, and the schedule mask. Start by sort structure and
  // populating the schedule information.
  SCHSortSchedules sortSchedules(mMarkDesign);
  for (l = sequentials->loopSequential(); !l.atEnd(); ++l) {
    SCHSequential* seq = *l;
    const NUNetElab* clkElab = seq->getEdgeNet();

    // Get the input nets for this schedule. If this is one of those
    // special async reset with level fanin cases, we really should
    // not branch around it based just on clock's inputs so lets not
    // create the input nets.
    const SCHInputNets* inputNets = NULL;
    if (seq->getExtraEdgesMask() == NULL) {
      inputNets = branchNets->compute(clkElab);
    }

    seq->putBranchNets(inputNets);
    sortSchedules.addSched(seq, inputNets);
  }

  // Add the flow information
  for (l = sequentials->loopSequential(); !l.atEnd(); ++l)
  {
    SCHSequential* seq = *l;
    for (FlowLoop f = loopRunBeforeSchedules(seq); !f.atEnd(); ++f)
    {
      Flow* fanin = *f;
      if (fanin->getWeight() > 0)
      {
	SCHSequential* faninSeq = fanin->getSchedule();
	SCHED_ASSERT(seq != faninSeq, seq);
	sortSchedules.addFanout(faninSeq, seq);
      }
    }
  }

  // We are done with the sequential flow we created above.
  deleteFlow();

  // Compute the depth for all the sequentials from the resulting sort
  typedef UtVector<SCHSequential*> SeqVec;
  SeqVec seqVec;
  int depth = 0;
  SCHSortSchedules::ScheduleGroup schedGroup;
  while (sortSchedules.getNextSchedule(schedGroup))
  {
    // Assign this group the same depth since they can be scheduled together
    INFO_ASSERT(!schedGroup.empty(),
                "Found an empty schedule group when sorting schedules");
    ++depth;
    SCHSortSchedules::ScheduleGroupLoop l;
    for (l = sortSchedules.loopSchedules(schedGroup); !l.atEnd(); ++l)
    {
      SCHScheduleBase* sched = *l;
      SCHSequential* seq = dynamic_cast<SCHSequential*>(sched);
      SCHED_ASSERT(mDepthMap->find(seq) == mDepthMap->end(), sched);
      mDepthMap->insert(DepthMap::value_type(seq, depth));
      seqVec.push_back(seq);
    }
    schedGroup.clear();
  }

  // Now sort the sequentials and set their order based on that
  // sorting. We sort by depth, input nets, and schedule mask
  std::sort(seqVec.begin(), seqVec.end(), CmpSequentials(mDepthMap));
  for (SeqVec::iterator i = seqVec.begin(); i != seqVec.end(); ++i)
  {
    SCHSequential* seq = *i;
    seq->putOrder(++mOrder);
  }

  // Now we can set the order for the sequentials in this set
  sequentials->sort();
} // SCHSequentialSchedules::determineOrder

void
SCHSequentialSchedules::determineSequentialDependencies(SCHSequential* seq,
							FLNodeElab* flow,
							FLNodeElabSet& covered,
							Sequentials& inSet)
{
  // If this flow is inactive, then we don't care about its dependencies
  if (flow->isInactive())
    return;

  // Visit the fanin for this flow node to find any sequential nodes
  // on its level fanin. These must run after this flow node.
  for (SCHMarkDesign::LevelFaninLoop p = mMarkDesign->loopLevelFanin(flow);
       !p.atEnd(); ++p) {
    FLNodeElab* fanin = *p;
    if (SCHUtil::isSequential(fanin) && !SCHUtil::sameBlock(flow, fanin) &&
	(covered.find(fanin) == covered.end()) && !fanin->isInactive())
    {
      // Figure out the clock and edge for this flow node
      ClockEdge edge;
      SCHSequential* faninSeq = findSequentialSchedule(fanin, &edge);

      // Make sure we don't have the same schedule. No need to break
      // cycles in the same schedule. Also make sure this schedule is
      // in our scheduling set (DCL seqs are separate from normal
      // seqs)
      if ((seq != faninSeq) && (faninSeq != NULL) &&
	  (inSet.find(faninSeq) != inSet.end()))
      {
	// Figure out how many instances there are of this fanin. We
	// use this to adjust the weight.
	NUUseDefNode* useDef = fanin->getUseDefNode();
	FLNodeElabSet* faninSet = (*mBlockInstMap)[useDef];
	NU_ASSERT(faninSet != NULL, useDef);
	UInt32 weight = faninSet->size();

	// Make the fanin sequential run after the flow sequential
	addRunBeforeSchedule(faninSeq, seq, weight);
	covered.insert(fanin);
      }
    }
  }
} // SCHSequentialSchedules::determineSequentialDependencies

SCHSequentialSchedules::Flow*
SCHSequentialSchedules::breakSequentialCycles(Flow* flow,
					      Sequentials& covered)
{
  // If we have already covered this sequential than return
  SCHSequential* seq = flow->getSchedule();
  if (covered.find(seq) != covered.end())
    return NULL;

  // Push this edge from our caller on the stack
  SCHED_ASSERT(mSequentialsInStack->find(seq) == mSequentialsInStack->end(), seq);
  mSequentialsInStack->insert(seq);
  mSequentialStack->push_back(flow);

  // Visit this schedules fanin looking for cycles. We call this
  // routine recursively. If it ever returns a non-NULL sequential
  // schedule, that is the point where we should break the cycle.
  Flow* breakPoint = NULL;
  for (FlowLoop l = loopRunBeforeSchedules(seq);
       !l.atEnd() && (breakPoint == NULL);
       ++l)
  {
    // Check if this is a cycle. It can't occur if the weight is zero
    Flow* fanin = *l;
    if (fanin->getWeight() > 0)
    {
      SCHSequential* faninSeq = fanin->getSchedule();
      if (mSequentialsInStack->find(faninSeq) != mSequentialsInStack->end())
      {
	// We have a cycle. Find the spot to break it
	UInt32 minWeight = fanin->getWeight();
	breakPoint = fanin;
	bool endSeen = false;
	for (SequentialStack::reverse_iterator p = mSequentialStack->rbegin(); 
	     (p != mSequentialStack->rend()) && !endSeen; 
	     ++p)
	{
	  // Check if this arc's weight is less, then use that
	  Flow* flow = *p;
	  if ((flow->getWeight() < minWeight) && (flow->getWeight() > 0))
	  {
	    minWeight = flow->getWeight();
	    breakPoint = flow;
	  }

	  // Check if we hit the end, which is this starting node
	  endSeen = (flow->getSchedule() == faninSeq);
	}
	SCHED_ASSERT(breakPoint != NULL, faninSeq);
      }
      else
	// Visit our fanin
	breakPoint = breakSequentialCycles(fanin, covered);

      // Check if we should break the cycle here
      if (breakPoint == fanin)
      {
	// Clear the weight which effectively marks this fanin not there
	fanin->clearWeight();
	breakPoint = NULL;
      }
    }
  } // for

  // Pop our callers edge to us off the stack
  mSequentialsInStack->erase(seq);
  mSequentialStack->pop_back();

  // There are two reasons we may return from this call. The first is
  // that we found no cycle so we should just return. The second is
  // that we found a cycle and it is better to break it in an arc from
  // a caller so we return the spot to break it.
  if (breakPoint == NULL)
    // We are done with this sequential flow and we didn't find a cycle
    covered.insert(seq);
  return breakPoint;
}

UInt32
SCHSequentialSchedules::setSequentialDepth(SCHSequential* seq,
					   SCHSequentials* sequentials,
					   Sequentials& onStack)
{
  // Check if we hit a cycle. These should not exist at this point
  SCHED_ASSERT(onStack.find(seq) == onStack.end(), seq);

  // Check if this is already done. If so, return its depth
  // fanin's depth
  DepthMap::iterator pos = mDepthMap->find(seq);
  if (pos != mDepthMap->end())
    return pos->second;

  // Visit any sequentials that must run before and compute their
  // depth. This sequentials depth is the maximum of those depths + 1
  UInt32 depth = 0;
  onStack.insert(seq);
  for (FlowLoop l = loopRunBeforeSchedules(seq); !l.atEnd(); ++l)
  {
    Flow* fanin = *l;
    if (fanin->getWeight() > 0)
    {
      SCHSequential* faninSeq = fanin->getSchedule();
      UInt32 newDepth = setSequentialDepth(faninSeq, sequentials, onStack);
      if (newDepth > depth)
	depth = newDepth;
    }
  }
  onStack.erase(seq);

  // We are done with this sequential, we can mark its depth and
  // return it
  mDepthMap->insert(DepthMap::value_type(seq, ++depth));
  return depth;
}

void
SCHSequentialSchedules::createFlowToScheduleMap()
{
  // Fill the flow to schedule map that we need for the next code block
  SCHSchedulesLoop p;
  for (p = mScheduleData->loopAllSequentials(); !p.atEnd(); ++p) {
    SCHSequential* seq = p.getSequentialSchedule();
    addFlowsToScheduleMap(seq);
  }

} // SCHSequentialSchedules::createFlowToScheduleMap

void
SCHSequentialSchedules::deleteFlowToScheduleMap()
{
  FlowToSeqScheduleMap::iterator m;
  for (m = mFlowToSeqScheduleMap.begin(); m != mFlowToSeqScheduleMap.end();
       ++m)
  {
    SequentialAndEdge* data = m->second;
    int refCnt = data->decRefCnt();
    SCHED_ASSERT(refCnt >= 0, data->getSchedule());
    if (refCnt == 0)
      delete data;
  }
  mFlowToSeqScheduleMap.clear();
}

void
SCHSequentialSchedules::createBlockInstMap()
{
  // Create a map from blocks to elaborated flow nodes for sequential
  // nodes. This can be used to see how many instances of a flow node
  // there are or to find them all.
  INFO_ASSERT(mBlockInstMap->empty(), "Duplicate call to createBlockInstMap()");
  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s)
  {
    // Check if we have seen this block before
    FLNodeElab* flow = *s;
    NUUseDefNode* useDef = flow->getUseDefNode();
    BlockInstMap::iterator pos = mBlockInstMap->find(useDef);
    FLNodeElabSet* flows;
    if (pos == mBlockInstMap->end())
    {
      // No, so add it
      flows = new FLNodeElabSet;
      mBlockInstMap->insert(BlockInstMap::value_type(useDef, flows));
    }
    else
      // Yup, use it
      flows = pos->second;

    // Add this elaborated flow node
    flows->insert(flow);
  }
} // SCHSequentialSchedules::createBlockInstMap

void
SCHSequentialSchedules::deleteBlockInstMap()
{
  for (BlockInstMapIter p=mBlockInstMap->begin(); p!=mBlockInstMap->end(); ++p)
    delete p->second;
  mBlockInstMap->clear();
}

void
SCHSequentialSchedules::createFlowInstMap()
{
  // Create a map from unelaborated flows to elaborated flow nodes for
  // sequential nodes. This can be used to see how many instances of a
  // flow node there are or to find them all.
  INFO_ASSERT(mFlowInstMap->empty(), "Duplicate call to createFlowInstMap()");
  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s)
  {
    // Check if we have seen this flow before
    FLNodeElab* flow = *s;
    FLNode* unelabFlow = flow->getFLNode();
    FlowInstMap::iterator pos = mFlowInstMap->find(unelabFlow);
    FLNodeElabSet* flows;
    if (pos == mFlowInstMap->end())
    {
      // No, so add it
      flows = new FLNodeElabSet;
      mFlowInstMap->insert(FlowInstMap::value_type(unelabFlow, flows));
    }
    else
      // Yup, use it
      flows = pos->second;

    // Add this elaborated flow node
    flows->insert(flow);
  }
} // SCHSequentialSchedules::createFlowInstMap

FLNodeElabSet* SCHSequentialSchedules::getFlowInstances(FLNode* flow)
{
  FlowInstMap::iterator pos = mFlowInstMap->find(flow);
  FLN_ASSERT(pos != mFlowInstMap->end(), flow);
  return pos->second;
}

void
SCHSequentialSchedules::deleteFlowInstMap()
{
  for (FlowInstMapIter p=mFlowInstMap->begin(); p != mFlowInstMap->end(); ++p)
    delete p->second;
  mFlowInstMap->clear();
}

bool
SCHSequentialSchedules::CmpSequentials::operator()(const SCHSequential* s1,
						   const SCHSequential* s2)
  const
{
  // Check for easy case
  if (s1 == s2)
    return false;

  // First compare by depth
  UInt32 depth1 = (*mDepthMap)[s1];
  UInt32 depth2 = (*mDepthMap)[s2];
  SCHED_ASSERT(depth1 > 0, s1);
  SCHED_ASSERT(depth2 > 0, s2);
  int cmp = depth1 - depth2;
  if (cmp == 0)
  {
    // They have the same depth, sort by input nets
    const SCHInputNets* bn1 = s1->getBranchNets();
    const SCHInputNets* bn2 = s2->getBranchNets();
    if ((bn1 == NULL) && (bn2 == NULL))
      cmp = 0;
    else if (bn1 == NULL)
      cmp = -1;
    else if (bn2 == NULL)
      cmp = 1;
    else
      cmp = SCHInputNetsFactory::compare(bn1, bn2);
    if (cmp == 0)
      // They have the same input nets, sort by schedule (TBD - should
      // I add the branch net comparison to the general compare
      // function??)
      cmp = s1->compareSchedules(s2);
  }
  SCHED_ASSERT(cmp != 0, s1);
  return cmp < 0;
}

void
SCHSequentialSchedules::addRunBeforeSchedule(SCHSequential* after,
					     SCHSequential* before,
					     UInt32 weight)
{
  // Check if we have allocate space for this after schedule
  SequentialFlow::iterator posA = mSequentialFlow->find(after);
  FlowSet* flowSet;
  if (posA == mSequentialFlow->end())
  {
    // Need the space
    flowSet = new FlowSet;
    mSequentialFlow->insert(SequentialFlow::value_type(after, flowSet));
  }
  else
    // Have the space
    flowSet = posA->second;

  // Check if we have insert this before schedule
  Flow srchFlow(before, 0);
  Flow* flow;
  FlowSet::iterator posB = flowSet->find(&srchFlow);
  if (posB == flowSet->end())
  {
    // Need to create a new one
    flow = new Flow(before, weight);
    flowSet->insert(flow);
  }
  else
  {
    // Already have one, just update the weight
    flow = *posB;
    flow->addWeight(weight);
  }
}

SCHSequentialSchedules::FlowLoop
SCHSequentialSchedules::loopRunBeforeSchedules(SCHSequential* seq)
{
  // Find the flow set we need. If it isn't there, we need to create
  // any empty flow set. There can be some schedules with no run
  // before schedules.
  SequentialFlow::iterator pos = mSequentialFlow->find(seq);
  FlowSet* flowSet;
  if (pos == mSequentialFlow->end())
  {
    flowSet = new FlowSet;
    mSequentialFlow->insert(SequentialFlow::value_type(seq, flowSet));
  }
  else
    flowSet = pos->second;

  // Return the iterator
  return FlowLoop(*flowSet);
}

bool
SCHSequentialSchedules::CompareFlow::operator()(const Flow* f1, const Flow* f2)
  const
{
  SCHSequential* s1 = f1->getSchedule();
  SCHSequential* s2 = f2->getSchedule();
  int cmp = s1->compareSchedules(s2);
  return cmp < 0;
}

void
SCHSequentialSchedules::deleteFlow()
{
  SequentialFlow::iterator p;
  for (p = mSequentialFlow->begin(); p != mSequentialFlow->end(); ++p)
  {
    FlowSet* flowSet = p->second;
    for (FlowSet::iterator f = flowSet->begin(); f != flowSet->end(); ++f)
      delete *f;
    delete flowSet;
  }
  mSequentialFlow->clear();
}
  
void
SCHSequentialSchedules::addDoubleBufferedBlocks(Block* block,
						FLNodeElabSet* faninSet)
{
  // Get the set of flow nodes for the output block
  FLNodeElab* flow = block->getFlow();
  SortedSequentialFlows* ssf = mSortedSequentialFlowsMap[flow];
  FLNodeElabVector* nodes = ssf->getNodes();

  // Find the flows which have a flow node in the fanin set in their
  // fanin (got that?)
  for (FLNodeElabVector::iterator p = nodes->begin(); p != nodes->end(); ++p)
  {
    FLNodeElab* flow = *p;
    for (Fanin f = mUtil->loopFanin(flow); !f.atEnd(); ++f)
    {
      FLNodeElab* fanin = *f;
      if (SCHUtil::isSequential(fanin) &&
	  (faninSet->find(fanin) != faninSet->end()))
	addDoubleBuffered(flow, fanin, eCycle);
    }
  }
}

void
SCHSequentialSchedules::addDoubleBuffered(FLNodeElab* fanoutElab,
                                          FLNodeElab* faninElab,
					  DBReason reason)
{
  // Add this by def so that we mark all instances
  FLNode* fanout = fanoutElab->getFLNode();
  NUNetVector nets;
  findFaninNet(fanoutElab, faninElab, &nets);
  for (NUNetVectorLoop l(nets); !l.atEnd(); ++l) {
    NUNet* faninNet = *l;
    addDoubleBuffered(fanout, faninNet, reason);
  }
} // SCHSequentialSchedules::addDoubleBuffered

bool
SCHSequentialSchedules::isDoubleBuffered(FLNodeElab* fanoutElab,
                                         FLNodeElab* faninElab) const
{
  FLNode* fanout = fanoutElab->getFLNode();
  NUNetVector nets;
  findFaninNet(fanoutElab, faninElab, &nets);
  bool doubleBuffered = false;
  for (NUNetVectorLoop l(nets); !l.atEnd() && !doubleBuffered; ++l) {
    NUNet* faninNet = *l;
    doubleBuffered = isDoubleBuffered(fanout, faninNet) != eNotDoubleBuffered;
  }
  return doubleBuffered;
}

void
SCHSequentialSchedules::findFaninNet(FLNodeElab* fanoutElab,
                                     FLNodeElab* faninElab,
                                     NUNetVector* nets) const
{
  // Get the hierarchy for the fanout flop. That is where the buffer
  // will be added.
  STBranchNode* hier = fanoutElab->getHier();

  // Find the set of nets that match that hierarchy in the fanin
  // elaborated nets alias ring. Those are the possible fanin nets.
  NUNetElab* faninNetElab = faninElab->getDefNet();
  NUNetVector aliasNets;
  faninNetElab->getAliasNets(&aliasNets, true, hier);
  FLN_ELAB_ASSERT(!aliasNets.empty(), faninElab);

  // One last test, we have to make sure that the net feeds this
  // flop. We can check its uses.
  FLN_ELAB_ASSERT(nets->empty(), fanoutElab);
  NUUseDefNode* useDef = fanoutElab->getUseDefNode();
  NUNetRefSet uses(mUtil->getNetRefFactory());
  useDef->getUses(fanoutElab->getFLNode()->getDefNetRef(), &uses);
  for (NUNetVectorLoop l(aliasNets); !l.atEnd(); ++l) {
    NUNet* net = *l;
    NUNetRefHdl netRef = mUtil->getNetRefFactory()->createNetRef(net);
    if (uses.find(netRef, &NUNetRef::overlapsSameNet) != uses.end()) {
      nets->push_back(net);
    }
  }
  FLN_ELAB_ASSERT(!nets->empty(), fanoutElab);
} // SCHSequentialSchedules::findFaninNet


void
SCHSequentialSchedules::addDoubleBuffered(FLNode* fanout, NUNet* net,
                                          DBReason reason)
{
  DBEdge edge(fanout, net);
  mDoubleBufferPairs->insert(DoubleBufferPairs::value_type(edge, reason));
}

SCHSequentialSchedules::DBReason
SCHSequentialSchedules::isDoubleBuffered(FLNode* fanout, NUNet* net) const
{
  DBEdge edge(fanout, net);
  DoubleBufferPairs::iterator pos = mDoubleBufferPairs->find(edge);
  if (pos != mDoubleBufferPairs->end()) {
    return pos->second;
  } else {
    return eNotDoubleBuffered;
  }
}

class SCHMsgCallback : public SCHAddBuffer::Callback
{
public:
  //! constructor
  SCHMsgCallback(SCHSequentialSchedules* seqScheds) :
    mSeqScheds(seqScheds),
    mClkReason("between different schedules"),
    mCycleReason("a sequential block cycle")
  {}

  //! destructor
  ~SCHMsgCallback() {}

  //! Routine to get the reason
  const UtString* getReason(FLNodeElab* fanoutElab, FLNodeElab* faninElab)
    const
  {
    // Get the reason from the pair database
    SCHSequentialSchedules::DBReason reason;
    reason = mSeqScheds->doubleBufferReason(fanoutElab, faninElab);

    // Return a string for the reason or NULL if we are not printing
    // this pair.
    const UtString* strReason = NULL;
    switch (reason) {
      case SCHSequentialSchedules::eNotDoubleBuffered:
        strReason = NULL;
        break;

      case SCHSequentialSchedules::eMultiClk:
        strReason = &mClkReason;
        break;

      case SCHSequentialSchedules::eCycle:
        strReason = &mCycleReason;
        break;
    }
    return strReason;
  }
private:

  SCHSequentialSchedules* mSeqScheds;
  UtString mClkReason;
  UtString mCycleReason;
}; // class SCHMsgCallback : SCHAddBuffer::Callback


void SCHSequentialSchedules::addDoubleBuffers(NUNetElabSet* delayedNets)
{
  // Create a flow instance map so that we can mark all instances of a
  // flow pair as buffered. This is because we are adding the buffer
  // in an unelaborated way. By doing this we may reduce the amount of
  // buffers added.
  createFlowInstMap();

  // Go through the unelaborated double buffers and create the
  // elaborated double buffers for buffer insertion. But we still
  // don't support adding a buffer to a memory fanin (because it
  // involves buffering the address as well). So mark those as state
  // update.
  SCHAddBuffer::BufferedFlowsMap bufferedFlows;
  DoubleBufferPairs::iterator p;
  for (p = mDoubleBufferPairs->begin(); p != mDoubleBufferPairs->end(); ++p)
  {
    // Convert this to an elaborated map. Start by getting the
    // elaborated flows for this unelaborated flow pairs.
    DBEdge edge = p->first;
    FLNode* fanout = edge.first;
    NUNet* faninNet = edge.second;
    FLNodeElabSet* fanoutSet = getFlowInstances(fanout);

    // Visit all the edges from the fanout set to its fanin. If the
    // fanin is in our fanin set, we need to add it.
    FLNodeElabSetIter i;
    for (i = fanoutSet->begin(); i != fanoutSet->end(); ++i) {
      // Loop all the drivers for this fanin net. We use the output
      // flows hierarchy to elaborate it.
      FLNodeElab* fanoutFlow = *i;
      NUNetElab::DriverLoop l;
      for (l = loopElaboratedFanin(fanoutFlow, faninNet); !l.atEnd(); ++l) {
        // Check whether the fanin is a memory or not
        FLNodeElab* faninFlow = *l;
        if (faninNet->is2DAnything()) {
          // Mark it as state update
          markFlowDoubleBuffered(faninFlow);
        } else {
          // Add it to the SCHAddBuffer structure
          FLNodeElabSet& fanins = bufferedFlows[fanoutFlow];
          fanins.insert(faninFlow);

          // Remember why we added it
          DBReason reason = p->second;
          addDoubleBufferReason(fanoutFlow, faninFlow, reason);
        }
      }
    }
  } // for

  // Add the buffers
  bool verbose = mUtil->isFlagSet(eDumpBufferedSequentials);
  SCHAddBuffer addBuffer(mTimingAnalysis, mMarkDesign, mUtil, 
                         mUtil->getNetRefFactory(), verbose);
  SCHMsgCallback callback(this);
  addBuffer.addBuffers(bufferedFlows, &callback, mScheduleData, true, delayedNets);
  
  // All done with the flow instance map and double buffer reasons
  deleteFlowInstMap();

  // At this point the double buffer flag is set on most of the
  // NUNet*'s that need them. But it is not set on any aliases of all
  // the instances of a sequential always block. This is because there
  // is no way to go from an NUNet* to all the NUNetElab* for that
  // always block. So we now fix this by doing another pass over the
  // sequential blocks.
  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s) {
    // Get the aliases for this LHS
    FLNodeElab* flow = *s;
    NUNetElab* netElab = flow->getDefNet();
    NUNetVector nets;
    netElab->getAliasNets(&nets, true);

    // Check if any of the aliases are double buffered
    bool doubleBuffer = false;
    NUNetVectorIter i;
    for (i = nets.begin(); (i != nets.end()) && !doubleBuffer; ++i) {
      NUNet* net = *i;
      doubleBuffer = net->isDoubleBuffered();
    }

    // If any are double buffered, do them now
    if (doubleBuffer)
      markFlowDoubleBuffered(flow);
  }

  // All done with the double buffers
  mDoubleBufferPairs->clear();
} // void SCHSequentialSchedules::addDoubleBuffers

NUNetElab::DriverLoop
SCHSequentialSchedules::loopElaboratedFanin(FLNodeElab* fanoutFlow,
                                            NUNet* faninNet) const
{
  STBranchNode* hier = fanoutFlow->getHier();
  NUNetElab* faninNetElab = faninNet->lookupElab(hier);
  return faninNetElab->loopContinuousDrivers();
}

bool SCHSequentialSchedules::validSequentialFanin(FLNodeElab* flow,
						  FLNodeElab* fanin) const
{
  return (SCHUtil::isSequential(fanin) && !SCHUtil::sameBlock(flow, fanin) &&
	  !SCHUtil::sameSequentialBlock(flow, fanin) &&
          !mMarkDesign->isLatchClockUse(flow, fanin));
}

void SCHSequentialSchedules::addDoubleBufferReason(FLNodeElab* fanoutElab,
                                                   FLNodeElab* faninElab,
                                                   DBReason reason)
{
  FlowElabPair pair(fanoutElab, faninElab);
  mDoubleBufferReason->insert(DoubleBufferReason::value_type(pair, reason));
}

SCHSequentialSchedules::DBReason
SCHSequentialSchedules::doubleBufferReason(FLNodeElab* fanoutElab,
                                           FLNodeElab* faninElab) const
{
  FlowElabPair pair(fanoutElab, faninElab);
  DoubleBufferReason::iterator pos = mDoubleBufferReason->find(pair);
  if (pos == mDoubleBufferReason->end()) {
    return eNotDoubleBuffered;
  } else {
    return pos->second;
  }
}

bool
SCHSequentialSchedules::ValidSeqOrder::operator()(FLNodeElab* fanout,
                                                  FLNodeElab* fanin) const
{
  // Make sure this is a true ordering edge
  if (!mSequentialSchedules->validSequentialFanin(fanout, fanin)) {
    return false;
  }

  // Check if the schedule order makes this edge a required reverse
  // dependency
  ScheduleOrder order = mSequentialSchedules->determineScheduleOrder(fanout,fanin);
  if ((order == eExclusive) || (order == eOrderAfter)) {
    return false;
  }

  // Check if the fanin is a state update. We fix up those edges
  NUNetElab* netElab = fanin->getDefNet();
  if (netElab->getNet()->isDoubleBuffered()) {
    return false;
  }

  // Check if we cross hierarchy, if so, ignore the edge and fix up
  // any issues with buffer insertion. We used to care about these but
  // when we added mixed block merging, we also added a new buffer
  // insertion pass at the end. This allows us to be more aggressive
  // which helped a Hitachi test case. See bug7266.
  if (fanout->getHier() != fanin->getHier()) {
    return false;
  }

  return true;
}

void SCHSequentialSchedules::recomputeBlockDepth()
{
  // Clear the sequential node depth
  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s) {
    FLNodeElab* flow = *s;
    flow->putDepth(DEPTH_UNINITIALIZED);
  }

  // Create the set of blocks and flow for all the sequential blocks.
  createSequentialBlockFlow();
  createSortedSequentialFlowMap();

  // Traverse the set of blocks again and compute their depth.
  for (BlocksIter b = mBlocks->begin(); b != mBlocks->end(); ++b) {
    Block* block = b->second;
    computeBlockDepth(block);
  }

  // Done with the memory
  deleteSortedSequentialFlowMap();
  deleteSequentialBlockFlow();
} // void SCHSequentialSchedules::recomputeBlockDepth

void SCHSequentialSchedules::fixupCombFlow()
{
  // Gather all immediate flows that fanin into flops
  FLNodeElabSet outFlows;
  FLNodeElabSet inFlows;
  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s) {
    FLNodeElab* flowElab = *s;
    if ((flowElab->getSignature() != NULL) && flowElab->isLive()) {
      for (Fanin f = mUtil->loopFanin(flowElab); !f.atEnd(); ++f) {
        FLNodeElab* faninElab = *f;
        if (!SCHUtil::isSequential(faninElab) && 
            (faninElab->getSignature() == NULL)) {
          outFlows.insert(flowElab);
          inFlows.insert(faninElab);
          faninElab->putIsLive(true);
        }
      }
    }
  }

  // Adjust the timing of these if we have some
  if (!outFlows.empty()) {
    mTimingAnalysis->adjustTiming(outFlows, inFlows);
  }
} // void SCHSequentialSchedules::fixupCombFlow
