// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "schedule/Schedule.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Signature.h"

#include "iodb/ScheduleFactory.h"

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"

#include "nucleus/NUUseDefNode.h"

#include "util/CbuildMsgContext.h"
#include "util/UtDebugRecursion.h"
#include "util/Graph.h"
#include "util/GenericDigraph.h"
#include "util/Stats.h"
#include "util/GraphTranspose.h"

#include "symtab/STAliasedLeafNode.h"

#include "reduce/RETransform.h"

#include "Util.h"
#include "Depth.h"
#include "MarkDesign.h"
#include "ScheduleData.h"
#include "SequentialSchedules.h"
#include "CombinationalSchedules.h"
#include "TimingAnalysis.h"
#include "CreateSchedules.h"
#include "SortSchedules.h"
#include "AddBuffer.h"
#include "UseDefHierPair.h"
#include "BlockSplit.h"
#include "BranchNets.h"
#include "DerivedClockSchedules.h"

// Local scratch flags in elaborated flow
#define DCL_IN_GROUP    (1<<0)

/*!
  \file

  This file contains routines to create and order the derived clock
  logic schedules. This schedules generate the values for clocks upon
  which the main schedules decide how to execute.

  The main routines to do the work is create.
*/
  
SCHDerivedClockSchedules::SCHDerivedClockSchedules(
  SCHCreateSchedules* create,
  SCHScheduleFactory* factory,
  SCHMarkDesign* markDesign,
  SCHScheduleData* scheduleData,
  SCHTimingAnalysis* timingAnalysis,
  SCHCombinationalSchedules* combScheds,
  SCHSequentialSchedules* seqScheds,
  SCHUtil* util, SCHSchedule* sched)
{
  mCreate = create;
  mScheduleFactory = factory;
  mMarkDesign = markDesign;
  mScheduleData = scheduleData;
  mTimingAnalysis = timingAnalysis;
  mCombinationalSchedules = combScheds;
  mSequentialSchedules = seqScheds;
  mUtil = util;
  mSched = sched;
  mIDs = 0;
  mBlockFlowNodes = NULL;
}

SCHDerivedClockSchedules::~SCHDerivedClockSchedules()
{
}

// Create the derived clock logic schedules
bool SCHDerivedClockSchedules::create(const char* fileRoot)
{
  // If we are timing passes, start a new timer
  Stats* stats = mUtil->getStats();
  if (stats != NULL)
    stats->pushIntervalTimer();

  // Mark the derived clock logic. This sets the isDerivedClock or
  // isDerivedClockLogic flag in elaborated flow that is part of a DCL
  // schedule.
  bool ok = calculateDerivedClockLogic();
  if (stats != NULL) stats->printIntervalStatistics("DCS Calc");
  if (!ok) {
    if (stats != NULL) stats->popIntervalTimer();
    return false;
  }

  // Sort the derived clock schedules according to their order. We
  // want to sort this before we create the DCL schedules because the
  // order will dictate where any shared logic (between two derived
  // clock logic schedules) resides. The shared logic must be in the
  // master derived schedule of two dependent schedules.
  //
  // If we don't find a valid order, this code creates a DCL cycle
  // that runs until it settles.
  calculateDerivedClockLogicOrdering();
  if (stats != NULL) stats->printIntervalStatistics("DCS Order");

  // Traverse the list of clocks and create schedules for the derived
  // clock logic.
  //
  // Note that this marks all flow node's depth field with the
  // schedule id they are in. This should not get modified until after
  // the fixClkAndPIFlow function is called.
  scheduleDesignDerivedNodes();
  if (stats != NULL) stats->printIntervalStatistics("DCS Schedule");

  // Mark all the flows so we know in which schedule they are in. This
  // is used by findAndFixClkAndPIFlow as well as findCycleBreakNets.
  markAllDCLFlows();

  // Add buffers to all sequential blocks that use data computed in a
  // different derived clock schedule. We also fix PIs that are used
  // as data.
  findAndFixClkAndPIFlow();
  if (stats != NULL) stats->printIntervalStatistics("ClkPI Fix");

  // Find all the elaborated nets for which a DCL cycle node may use a
  // stale value. These are the nets at the DCL boundary for a node
  // that was used to break the cycle. These nets are important
  // because they indicate whether the DCL cycle needs to run another
  // iteration or not.
  findCycleBreakNets();

  // Dump the DCL cycles to the lib<design>.dclcycles file
  dumpDCLCycles(fileRoot);
  if (stats != NULL) stats->printIntervalStatistics("DCO Dump");

  // All done with the DCL flow
  clearAllDCLFlows();

  // Make sure we don't use a clock before it is defined
  verify();
  if (stats != NULL) stats->printIntervalStatistics("DCS Verify");

  if (stats != NULL) stats->popIntervalTimer();
  return true;
} // bool SCHDerivedClockSchedules::create

void
SCHDerivedClockSchedules::scheduleDerivedNodes(
  SCHDerivedClockLogic* dcl,
  const SCHBlockFlowNodes& blockFlowNodes)
{
  SCHUtil::DerivedClockFlowsLoop l;
  for (l = mUtil->loopDerivedClockFlows(dcl); !l.atEnd(); ++l) {
    FLNodeElab* flow = *l;
    if (flow->isDerivedClock())
      mCreate->scheduleNodes(flow, eCombTransition, blockFlowNodes, dcl, true);
  }
}

bool SCHDerivedClockSchedules::calculateDerivedClockLogic()
{
  // If we are timing passes, start a new timer
  Stats* stats = mUtil->getStats();
  if (stats != NULL)
    stats->pushIntervalTimer();

  // Walk the derived clock logic and create the minimum set of
  // elaborated flow that should be in the derived clock logic.
  FLNodeElabSet flowElabs;
  FindFlowCallback findCallback(&flowElabs);
  walkDerivedClockLogic(&findCallback);
  if (stats != NULL) stats->printIntervalStatistics("DCS Find");

  // Split the blocks where some of the logic should be part of the
  // DCL schedule and part should be part of the bulk of the design.
  bool ok = splitDerivedClockLogic(flowElabs);
  if (stats != NULL) stats->printIntervalStatistics("DCS Split");
  if (!ok) {
    if (stats != NULL) stats->popIntervalTimer();
    return false;
  }

  // Re-walk the derived clock logic and mark the true derived clock
  // logic. Any blocks that we could not split above will now be
  // considered derived clock logic. This applies to sequential blocks
  // which we cannot execute more than once. For combinational blocks
  // we don't want to introduce the pessemism because it can pull in
  // more logic which then creates a scenario where we have to retry
  // splitting. Running this type of algorithm until it settles may
  // have a negative compile and run-time performance impact.
  MarkFlowCallback markCallback;
  walkDerivedClockLogic(&markCallback);
  if (stats != NULL) stats->printIntervalStatistics("DCS Mark");

  // All done with calcualting derived clock logic
  if (stats != NULL) stats->popIntervalTimer();
  return true;
}

void SCHDerivedClockSchedules::walkDerivedClockLogic(FlowCallback* callback)
{
  // Create the map to get all the flow nodes from an always block
  mBlockFlowNodes = new SCHBlockFlowNodes(mUtil);

  // Go through the list of clocks we found and visit the logic that
  // drives derived clocks.
  for (SCHClocksLoop p = mMarkDesign->loopClocks(); !p.atEnd(); ++p) {
    const NUNetElab* clk = *p;
    SCHUtil::ConstDriverLoop l;
    for (l = mUtil->loopNetDrivers(clk); !l.atEnd(); ++l) {
      FLNodeElab* flow = *l;
      if (flow->isDerivedClock() && flow->isLive()) {
        // Visit this elaborated flow
        if (SCHUtil::isSequential(flow) && callback->visitFullSeqBlock()) {
	  walkBlockDerivedClockLogic(flow, callback);
        } else {
          // Just visit this node
          callback->visit(flow);
        }

        // Visit the fanin for combinational blocks
	if (!SCHUtil::isSequential(flow)) {
	  SCHMarkDesign::TrueFaninLoop p;
	  for (p = mMarkDesign->loopTrueFanin(flow); !p.atEnd(); ++p) {
	    FLNodeElab* fanin = *p;
	    if (flow != fanin) {
	      walkDerivedClockLogicHelper(fanin, clk, callback);
            }
	  }
	}
      }
    }
  } // for

  // Destroy it because it becomes stale
  delete mBlockFlowNodes;
  mBlockFlowNodes = NULL;
} // void SCHDerivedClockSchedules::walkDerivedClockLogic

void
SCHDerivedClockSchedules::walkDerivedClockLogicHelper(
  FLNodeElab* flow,
  const NUNetElab* clk,
  FlowCallback* callback)
{
  // Stop if we have marked this already or if we hit a clock. We don't
  // have to keep on marking.
  if (flow->isClock() || callback->isCovered(flow)) {
    return;
  }

  // Check if this logic is inactive
  if (flow->isInactive()) {
    return;
  }

  // We don't need to mark sequential pull drivers. They should not be
  // scheduled.
  if (mMarkDesign->isInvalidPullDriver(flow)) {
    return;
  }

  // visit this node
  callback->visit(flow);

  // If the callback dictates, we walk the entire sequential block
  if (SCHUtil::isSequential(flow) && callback->visitFullSeqBlock())
    walkBlockDerivedClockLogic(flow, callback);

  // if this is a combination cycle, the entire thing is part of the
  // clock logic.
  if (SCHMarkDesign::isCycle(flow)) {
    FLNodeElabVector flows;
    mUtil->getAcyclicFlows(flow, &flows);
    walkFlowsClockLogic(flows, callback);
  }

  // Continue towards the fanin unless this is a sequential
  // node. That is another stopping condition. But the sequential
  // device should be part of the derived clock logic.
  if (!SCHUtil::isSequential(flow)) {
    SCHMarkDesign::TrueFaninLoop p;
    for (p = mMarkDesign->loopTrueFanin(flow); !p.atEnd(); ++p) {
      FLNodeElab* fanin = *p;
      if (flow != fanin) {
	walkDerivedClockLogicHelper(fanin, clk, callback);
      }
    }
  }
} // SCHDerivedClockSchedules::walkDerivedClockLogicHelper

void 
SCHDerivedClockSchedules::walkBlockDerivedClockLogic(FLNodeElab* flow,
                                                     FlowCallback* callback)
{
  FLNodeElabVector nodes;
  mBlockFlowNodes->getFlows(flow, &nodes, &FLNodeElab::isLive);
  walkFlowsClockLogic(nodes, callback);
}

void 
SCHDerivedClockSchedules::walkFlowsClockLogic(const FLNodeElabVector& nodes,
                                              FlowCallback* callback)
{
  for (FLNodeElabVectorCLoop l(nodes); !l.atEnd(); ++l) {
    FLNodeElab* curFlow = *l;
    if (!curFlow->isDerivedClock()) {
      callback->visit(curFlow);
    }
  }
}

static const NUNetElab* nodeType(SCHDerivedClockBase* dcl, SCHUtil* util,
                                 bool* sequential, bool* combinational)
{
  // Check if it is sequential or combinational
  *sequential = false;
  *combinational = false;
  for (SCHUtil::DerivedClockFlowsLoop l = util->loopDerivedClockFlows(dcl);
       !l.atEnd(); ++l) {
    // Sequential or combinational?
    FLNodeElab* flowElab = *l;
    if (SCHUtil::isSequential(flowElab)) {
      *sequential = true;
    } else {
      *combinational = true;
    }
  }

  // Get a representative clock for it
  return dcl->getRepresentativeClock();
} // SCHDerivedClockSchedules::nodeType


void
SCHDerivedClockSchedules::FlowGraphDotWriter::writeLabel(Graph* graph,
                                                         GraphNode* node)
{
  FlowGraph* flowGraph = dynamic_cast<FlowGraph*>(graph);
  FlowGraph::Node* graphNode = flowGraph->castNode(node);
  FLNodeElab* flow = graphNode->getData();
  NUNetElab* netElab = flow->getDefNet();

  // Write the label prefix information
  *mOut << "[ ";

  // Write the representative net name
  UtString str;
  netElab->compose(&str, NULL);
  *mOut << "label=\"" << str << "\"";

  // Sequential flows are square, combinational are round
  if (SCHUtil::isSequential(flow)) {
    *mOut << ", shape=box";
  }

  // End the label
  *mOut << " ]";
} // void FlowGraphDotWriter::writeLabel

void
SCHDerivedClockSchedules::FlowGraphDotWriter::writeLabel(Graph* graph,
                                                         GraphEdge* edge)
{
  // Use a solid line but add a label for the edge type
  *mOut << "[ style=solid, label=\"";
  FlowGraph* flowGraph = dynamic_cast<FlowGraph*>(graph);
  FlowGraph::Edge* graphEdge = flowGraph->castEdge(edge);
  EdgeType type = graphEdge->getData();
  switch(type) {
    case eEdgeComb:
      *mOut << "C";
      break;

    case eEdgeDiv:
      *mOut << "D";
      break;

    case eEdgeSeq:
      *mOut << "S";
      break;

    case eEdgeNone:
      *mOut << "N";
      break;

    case eNumEdgeTypes:
      break;
  }
  *mOut << "\" ]";
} // SCHDerivedClockSchedules::FlowGraphDotWriter::writeLabel

void
SCHDerivedClockSchedules::DCLGraphDotWriter::writeLabel(Graph* graph,
                                                        GraphNode* node)
{
  DCLGraph* dclGraph = dynamic_cast<DCLGraph*>(graph);
  DCLGraph::Node* dclNode = dclGraph->castNode(node);

  // Check if this contains PI, combinational, sequential, or derived
  // clocks
  bool sequential, combinational;
  SCHDerivedClockBase* dclBase = dclNode->getData();
  const NUNetElab* netElab;
  netElab = nodeType(dclBase, mUtil, &sequential, &combinational);

  // Write the label prefix information
  *mOut << "[ ";

  // Write the representative net name
  UtString str;
  netElab->compose(&str, NULL);
  *mOut << "label=\"" << str << "\"";

  // For sequentials it is square, for combinational it is round
  // (normal), for both, use a hexagon
  if (sequential && combinational) {
    *mOut << ", shape=hexagon";
  } else if (sequential) {
    *mOut << ", shape=box";
  }

  // End the label
  *mOut << " ]";
} // void DCLGraphDotWriter::writeLabel

void
SCHDerivedClockSchedules::DCLGraphDotWriter::writeLabel(Graph* graph,
                                                        GraphEdge* edge)
{
  // Use a solid line but at a label for the edge type
  *mOut << "[ style=solid, label=\"";
  DCLGraph* dclGraph = dynamic_cast<DCLGraph*>(graph);
  DCLGraph::Edge* dclEdge = dclGraph->castEdge(edge);
  EdgeType type = dclEdge->getData();
  *mOut << SCHDerivedClockSchedules::getEdgeString(type) << "\" ]";
} // SCHDerivedClockSchedules::DCLGraphDotWriter::writeLabel

// This routine is responsible for creating an ordering between DCL
// schedules. In the process it decides which DCL schedules to treat
// as a cycle and which ones it can handle as a simple DCL cycle.
//
// The function does the following:
//
// 1. Walks the elaborated flow graph and creates a DCL graph between
//    derived clocks. Each edge is marked as either a combinational,
//    sequential, or divider path.  Simple DCL schedules are created
//    for each clock at this time.
//
// 2. Uses strongly connected components to find any DCL cycles.
//
// 3. Creates the cyclic DCL schedules, by creating DCL cycle schedules
//    and inserting the DCL sub-schedules in a sorted order.
//
// 4. Uses the SCC graph and the input nets to sort the acylic DCL
//    schedules as before.
void SCHDerivedClockSchedules::calculateDerivedClockLogicOrdering()
{
  // If we are timing passes, start a new timer
  Stats* stats = mUtil->getStats();
  if (stats != NULL)
    stats->pushIntervalTimer();

  // This block temporarily creates a set of sequential cycles
  // and reports any DCL cycles to the user, including the full
  // intermediate connectivity.  The cyclic graphs are then
  // deleted and the rest of the routine works off of recomputed
  // graphs that omit the intermediate nodes.
  {
    // Get the set of cyclic paths containing sequential nodes
    UtList<FlowGraph*> sequentialCycles;
    createSequentialCycleGraphs(&sequentialCycles);
         
    // Delete graph memory
    for (UtList<FlowGraph*>::iterator iter = sequentialCycles.begin();
         iter != sequentialCycles.end();
         ++iter) {
      FlowGraph* cycle = *iter;
      delete cycle;
    }
  }

  // Create a graph of the derived clocks and create simple DCL schedules
  DCLGraph* graph = createDCLGraph();
  if (stats != NULL) stats->printIntervalStatistics("DCO Graph");

  // Run strongly connected components on this
  GraphSCC* graphSCC = new GraphSCC;
  GraphSCC::CompGraph* compGraph = graphSCC->buildComponentGraph(graph);

  // Walk the SCC graph creating DCLCycle schedules
  CompScheds compScheds;
  createDCLCycleSchedules(graph, *graphSCC, compGraph, &compScheds);
  if (stats != NULL) stats->printIntervalStatistics("DCO BrkCycle");

  // If the user asked for it, print the cyclic DCL graph
  if (mUtil->isFlagSet(eDumpCycles) && graphSCC->hasCycle()) {
    printDCLCycles(*graphSCC, compScheds);
    if (stats != NULL) stats->printIntervalStatistics("DCO Print");
  }

  // Convert the connected component graph back to a DCL graph for the
  // remaining work.
  SCCToDCLGraph sccToDCLGraph(compGraph, &compScheds);
  DCLGraph* acyclicGraph = sccToDCLGraph.transform();
  delete graph;
  delete graphSCC;

  // At this point the combinational schedule for this derived clock
  // schedule is set. We need to know which nets this derived clock
  // schedule is sensitive to.
  // That way the combinational schedule gets called on the
  // changes of those nets. So add those nets to the derived clock
  // schedule nets list.
  //
  // Note that we must do this before we optimized the combinational
  // schedules.
  computeInputNets();

  // Merge schedules that have the same trigger as long as it doesn't
  // introduce cycles.
  DCLGraph* mergedGraph;
  if (mUtil->isFlagSet(eNoMergeDCL)) {
    mergedGraph = acyclicGraph;
  } else {
    mergedGraph = mergeSchedules(acyclicGraph);
    delete acyclicGraph;
    if (stats != NULL) stats->printIntervalStatistics("DCO Merge");
  }

  // Sort the DCL schedules using the component graph
  sortSchedules(mergedGraph);
  delete mergedGraph;
  if (stats != NULL) stats->printIntervalStatistics("DCO Sort");

  if (stats != NULL) stats->popIntervalTimer();
} // void SCHDerivedClockSchedules::calculateDerivedClockLogicOrdering

void SCHDerivedClockSchedules::printKey()
{
  UtIO::cout() << "DCL Cycle Dumping Key:\n"
               << " C : combinational dependency\n"
               << " S : sequential dependency\n"
               << " D : clock divider dependency\n"
               << " ? : unknown dependency\n"
               << " - : placeholder for top-level nodes\n"
               << " * : indicates a back edge\n";
}

void SCHDerivedClockSchedules::printClocks(DCLGraph* graph, int cycleNum)
{
  // Dump the start of the cycle
  UtIO::cout() << "============== Dumping DCL cycle #" << cycleNum
               << " ================\n";

  // Dump the cycle
  PrintWalker walker(graph, mUtil);
  SortNodes sortNodes(graph);
  SortEdges sortEdges(graph);
  walker.dump(graph, &sortNodes, &sortEdges, "-");

  // Dump the end of the cycle
  UtIO::cout() << "=============== End of DCL cycle #" << cycleNum
               << " ================\n";
} // void SCHDerivedClockSchedules::printClocks

void SCHDerivedClockSchedules::dumpDCLCycles(const char* fileRoot)
{
  // Gather the cyclic DCL schedules
  UtVector<SCHDerivedClockCycle*> dclCycles;
  SCHSchedule::DerivedClockLogicLoop l;
  for (l = mScheduleData->loopDerivedClockLogic(); !l.atEnd(); ++l) {
    SCHDerivedClockBase* dclBase = *l;
    SCHDerivedClockCycle* dclCycle = dclBase->castDCLCycle();
    if (dclCycle != NULL) {
      dclCycles.push_back(dclCycle);
    }
  }

  // Open a file to store the DCL cycles. We do this even if there are
  // no DCL cycles so that we delete any previous DCL cycles file.
  UtString filename;
  filename << fileRoot << ".dclcycles";
  UtOStream* out = new UtOBStream(filename.c_str());

  // Dump the data to the .dclcycles file
  int cycleNum = 0;
  for (UtVector<SCHDerivedClockCycle*>::iterator i = dclCycles.begin();
       i != dclCycles.end(); ++i) {
    SCHDerivedClockCycle* dclCycle = *i;
    dumpDCLCycle(dclCycle, ++cycleNum, *out);
  }

  // All done with the file
  delete out;
} // void SCHDerivedClockSchedules::dumpDCLCycles

void
SCHDerivedClockSchedules::dumpElabNet(UtOStream& out, const char* prefix,
                                      const NUNetElab* netElab)
{
  UtString buf;
  netElab->getSymNode()->compose(&buf);
  out << prefix << buf << "\n";
}

void 
SCHDerivedClockSchedules::dumpDCLCycle(SCHDerivedClockCycle* cycle, 
                                       int cycleNum, UtOStream& out)
{
  // Dump the DCL cycle header
  out << "{ DCL Cycle #" << cycleNum << ": " << cycle->size() << " schedules\n";

  // Dump the break nets for this cycle
  UtVector<const NUNetElab*> breakNets;
  for (SCHDerivedClockCycle::BreakNetsLoop b = cycle->loopBreakNets();
       !b.atEnd(); ++b) {
    const NUNetElab* breakNet = *b;
    breakNets.push_back(breakNet);
  }
  if (!breakNets.empty()) {
    std::sort(breakNets.begin(), breakNets.end(), NUNetElabCmp());
    out << "  BreakNets {\n";
    for (UtVector<const NUNetElab*>::iterator i = breakNets.begin();
         i != breakNets.end(); ++i) {
      const NUNetElab* breakNet = *i;
      dumpElabNet(out, "    ", breakNet);
    }
    out << "  }\n";
  }

  // Iterate over the sub schedules for dumping. They are in execution
  // sorted order so leave them that way. It should be a stable order.
  for (SCHDerivedClockCycle::SubSchedulesLoop l = cycle->loopSubSchedules();
       !l.atEnd(); ++l) {
    SCHDerivedClockLogic* dcl = *l;
    dumpDCLSchedule(dcl, out);
  }
  
  // Dump the DCL cycle footer
  out << "}\n";
} // void SCHDerivedClockSchedules::dumpClocks

void 
SCHDerivedClockSchedules::dumpDCLSchedule(SCHDerivedClockLogic* dcl,
                                          UtOStream& out)
{
  // Dump the schedule label 
  out << "  DCL Schedule " << dcl->getID() << " {\n";

  // Dump the clocks for this schedule
  out << "    Clocks {\n";
  for (SCHClocksLoop c = dcl->loopClocks(); !c.atEnd(); ++c) {
    const NUNetElab* clkElab = *c;
    dumpElabNet(out, "      ", clkElab);
  }
  out << "    }\n";

  // Gather the always blocks in this schedule. SCHUseDefHierPair will
  // sort the block instances so that we get stable output.
  typedef UtMap<SCHUseDefHierPair, FLNodeElabVector> BlockFlows;
  BlockFlows blockFlows;
  NUUseDefVector useDefs;
  for (SCHSequentials::SequentialLoop l = dcl->loopSequential(); !l.atEnd(); ++l) {
    SCHSequential* seq = *l;
    for (SCHNodeSetLoop n = seq->loopNodes(eClockPosedge); !n.atEnd(); ++n) {
      FLNodeElab* flowElab = *n;
      SCHUseDefHierPair udHierPair(flowElab);
      blockFlows[udHierPair].push_back(flowElab);
    }
    for (SCHNodeSetLoop n = seq->loopNodes(eClockNegedge); !n.atEnd(); ++n) {
      FLNodeElab* flowElab = *n;
      SCHUseDefHierPair udHierPair(flowElab);
      blockFlows[udHierPair].push_back(flowElab);
    }
  }
  for (SCHIterator i = dcl->getSchedule(); !i.atEnd(); ++i) {
    FLNodeElab* flowElab = *i;
    SCHUseDefHierPair udHierPair(flowElab);
    blockFlows[udHierPair].push_back(flowElab);
  }

  // Dump the schedule nodes and gather fanins
  out << "    Nodes {\n";
  UtSet<UIntPtr> fanins;
  for (LoopMap<BlockFlows> b(blockFlows); !b.atEnd(); ++b) {
    // Dump the def nets for this node and gather fanins
    FLNodeElabVector& flows = b.getNonConstValue();
    std::sort(flows.begin(), flows.end(), FLNodeElabCmp());
    STBranchNode* hier = NULL;
    for (FLNodeElabVectorLoop l(flows); !l.atEnd(); ++l) {
      // Dump the def net
      FLNodeElab* flowElab = *l;
      hier = flowElab->getHier();
      NUNetElab* netElab = flowElab->getDefNet();
      dumpElabNet(out, "      DefNet: ", netElab);

      // Gather the fanins to other schedules
      for (Fanin f = mUtil->loopFanin(flowElab); !f.atEnd(); ++f) {
        FLNodeElab* faninElab = *f;
        SCHDerivedClockLogic* faninDCL = findFlowSchedule(faninElab);
        if ((faninDCL != NULL) && (dcl != faninDCL)) {
          fanins.insert(faninDCL->getID());
        }
      }
    }
    
    // Dump the Nucleus statement(s)
    const SCHUseDefHierPair& udHierPair = b.getKey();
    const NUUseDefNode* useDef = udHierPair.getUseDef();
    UtString buf;
    useDef->compose(&buf, hier, 6);
    out << buf << "\n";
  }
  out << "    }\n";

  // Dump the schedule fanins
  out << "    Fanin { ";
  const char* sep = "";
  for (UtSet<UIntPtr>::iterator i = fanins.begin(); i != fanins.end(); ++i) {
    UIntPtr index = *i;
    out << sep << index;
    sep = ", ";
  }
  out << " }\n"
      << "  }\n";
} // SCHDerivedClockSchedules::dumpDCLSchedule
  
void
SCHDerivedClockSchedules::verify()
{
  bool dump = getenv("DERIVED_SPEW") != NULL;
  bool derivedClockDefBeforeUse = true;

  // Go through the DCL schedules and make a set of the derived clock
  // nets. We do this so that we can quickly test if a net is a
  // derived clock or non-derived clock.
  UtSet<const NUNetElab*> derivedClocks;
  SCHSchedule::DerivedClockLogicLoop l;
  for (l = mScheduleData->loopDerivedClockLogic(); !l.atEnd(); ++l) {
    SCHDerivedClockBase* dclBase = *l;
    for (SCHClocksLoop l = dclBase->loopClocks(); !l.atEnd(); ++l) {
      const NUNetElab* clk = *l;
      derivedClocks.insert(clk);
    }
  }

  // Go through the DCL schedules and make sure we don't use a clock
  // before it is derived.
  UtSet<const NUNetElab*> computedClocks;
  for (l = mScheduleData->loopDerivedClockLogic(); !l.atEnd(); ++l)
  {
    // Add this clock to the computed list (We allow clocks to feed on
    // themselves. Hardware guys do the strangest things with clocks!)
    SCHDerivedClockBase* dclBase = *l;
    for (SCHClocksLoop l = dclBase->loopClocks(); !l.atEnd(); ++l) {
      const NUNetElab* clk = *l;
      computedClocks.insert(clk);

      if (dump)
      {
        UtString buf;
        clk->compose(&buf, NULL);   // (we have no testcase in almostall that checks this line)
        UtIO::cout() << "Derived Clock: " << buf << ", depth "
                     << dclBase->getDepth() << UtIO::endl;
      }
    }

    // Check if this schedule is driven by any clocks we have not
    // computed and are not PIs. Start with the sequentials
    SCHDerivedClockBase::SequentialLoop s;
    for (s = dclBase->loopMultiSequential(); !s.atEnd(); ++s)
    {
      SCHSequential* seq = *s;
      const NUNetElab* clk = seq->getEdgeNet();
      if (derivedClocks.find(clk) != derivedClocks.end())
	// This is a derived clock, make sure it is computed
	NU_ASSERT(computedClocks.find(clk) != computedClocks.end(), clk);
    }

    // Now test the combinational mask
    const SCHScheduleMask* mask = dclBase->getMask();
    SCHScheduleMask::UnsortedEvents e;
    for (e = mask->loopEvents(); !e.atEnd(); ++e)
    {
      const SCHEvent* event = *e;
      if (event->isClockEvent())
      {
        const NUNetElab* origClock = SCHSchedule::clockFromName(event->getClock());
	const NUNetElab* clk = mMarkDesign->getClockMaster(origClock);
	if ((derivedClocks.find(clk) != derivedClocks.end()) &&
            (computedClocks.find(clk) == computedClocks.end()))
        {
          derivedClockDefBeforeUse = false;
          UtString buf;
          clk->compose(&buf, NULL);   // (we have no testcase in almostall that checks this line)
          UtIO::cout() << "ERROR: Derived Clock " << buf
                       << " used before being defined" << UtIO::endl;
        }
      }
    }
  } // for
  INFO_ASSERT(derivedClockDefBeforeUse,
              "Look at the preceding errors for more information");
} // SCHDerviedClockSchedules::verify

bool
SCHDerivedClockSchedules::isValidDerivedNode(FLNodeElab* flowElab)
{
  bool invalidPull = mMarkDesign->isInvalidPullDriver(flowElab);
  const SCHSignature* sig = mMarkDesign->getSignature(flowElab);
  bool constantFlow = (sig->getTransitionMask() == mUtil->getConstantMask());
  return (!invalidPull && !constantFlow);
}

void
SCHDerivedClockSchedules::scheduleDerivedNode(FLNodeElab* flow,
					      bool isSequential,
					      SCHDerivedClockLogic* dcl)
{
  // Add the block
  if (isSequential)
  {
    SCHSequentials* sequentials = dcl->getSequentialSchedules();
    mSequentialSchedules->schedule(flow, sequentials, dcl);
  }
  else
  {
    // Create the new mask for this combinational schedule
    const SCHScheduleMask* newMask;
    const SCHScheduleMask* oldMask = dcl->getMask();
    newMask = mTimingAnalysis->accumulateTransitionMask(oldMask, flow, true);
    if (oldMask != NULL) {
      oldMask->release();
    }
    dcl->setMask(newMask);
    newMask->assign();

    // Now we can add the schedule if this is the type of node we
    // schedule.
    bool validDerivedNode = isValidDerivedNode(flow);
    if (validDerivedNode) {
      dcl->addCombinationalNode(flow);
    }

    // Also add it to one of the input/async/initial schedules as
    // necessary. We don't do it if we schedule it as combinational
    // because it is already done there.
    const SCHSignature* sig = mMarkDesign->getSignature(flow);
    mCombinationalSchedules->addToSpecialSchedules(flow, sig, !validDerivedNode);
  } // else
} // SCHDerivedClockSchedules::scheduleDerivedNode

void
SCHDerivedClockSchedules::findAndFixClkAndPIFlow()
{
  // Find the set of sequential blocks that could have potential flow
  // issues because we execute them after we execute clock logic or
  // read PIs directly.
  ClkAndPIFlowMap clkAndPIFlowMap;
  BufferedFlows clkFlows;
  BufferedFlows piFlows;
  findClkAndPIFlow(clkAndPIFlowMap, clkFlows, piFlows);

  // Try to break the clock used as data
  bool printMsgs = !clkFlows.empty() || !piFlows.empty();
  SCHAddBuffer addBuffer(mTimingAnalysis, mMarkDesign, mUtil, 
                         mUtil->getNetRefFactory(), printMsgs);
  MsgCallback msgCallback(clkFlows, piFlows);
  FLNodeElabSet deletedElabFlows;
  addBuffer.addBuffers(clkAndPIFlowMap, &msgCallback, mScheduleData, false, NULL);
}

void SCHDerivedClockSchedules::markAllDCLFlows(void)
{
  // Visit all the schedules, keeping track of what schedule a flow is in
  SCHSchedule::DerivedClockLogicLoop l;
  for (l = mScheduleData->loopDerivedClockLogic(); !l.atEnd(); ++l) {
    // Mark all the sequential sub schedules
    SCHDerivedClockBase* dclBase = *l;
    SCHDerivedClockLogic* dcl = dclBase->castDCL();
    if (dcl != NULL) {
      markDCLFlows(dcl);
    } else {
      SCHDerivedClockCycle* dclCycle = dclBase->castDCLCycle();
      SCHDerivedClockCycle::SubSchedulesLoop s;
      for (s = dclCycle->loopSubSchedules(); !s.atEnd(); ++s) {
        SCHDerivedClockLogic* dcl = *s;
        markDCLFlows(dcl);
      }
    }
  }

  // Create the map to get all the flow nodes from an always block
  SCHBlockFlowNodes blockFlowNodes(mUtil);

  // Note that we don't put all combinational nodes in the DCL
  // schedule for any given block. If block splitting failed to split
  // it, we still keep the elaborated flow separate. This is because
  // pulling in the elaborated flow would cause us to potentially
  // bring in more fanin logic into the block which could be cause us
  // to need to split and so on.
  //
  // But because these additional nets are computed early, we have to
  // buffer them. So we artifically add them to the schedule map here.
  FlowToDCLScheduleMap tempMap; // since we can't insert during iteration
  for (FlowToDCLScheduleMapLoop l(mFlowToDCLScheduleMap); !l.atEnd(); ++l) {
    // Only need to process combinationals. Sequentials are all in
    FLNodeElab* flowElab = l.getKey();
    if (!SCHUtil::isSequential(flowElab)) {
      // Get the DCL schedule we will put it in
      SCHDerivedClockLogic* dcl = l.getValue();

      // Get all the elaborated flow in this block
      FLNodeElabVector nodes;
      blockFlowNodes.getFlows(flowElab, &nodes, &FLNodeElab::isLive);
      for (FLNodeElabVectorCLoop f(nodes); !f.atEnd(); ++f) {
        FLNodeElab* curFlow = *f;
        FlowToDCLScheduleMap::const_iterator pos;
        pos = tempMap.find(curFlow);
        if (pos == tempMap.end()) {
          tempMap[curFlow] = dcl;
        }
      }
    }
  } // for

  // Now transfer the entries from the temp map into the permanent map
  for (FlowToDCLScheduleMapLoop l(tempMap); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = l.getKey();
    SCHDerivedClockLogic* dcl = l.getValue();
    // Mark the elaborated flow as part of this DCL schedule. But we
    // can have DCL's in multiple schedules, just pick one so if
    // this is in the map already, don't add it again.
    if (findFlowSchedule(flowElab) == NULL) {
      markDCLFlow(flowElab, dcl);
    }
  } // for

} // void SCHDerivedClockSchedules::markAllDCLFlows

void SCHDerivedClockSchedules::markDCLFlows(SCHDerivedClockLogic* dcl)
{
  // Mark all the sequential schedules
  SCHSequentials::SequentialLoop l;
  for (l = dcl->loopSequential(); !l.atEnd(); ++l) {
    SCHSequential* seq = *l;
    for (SCHNodeSetLoop n = seq->loopNodes(eClockPosedge); !n.atEnd(); ++n) {
      FLNodeElab* flowElab = *n;
      markDCLFlow(flowElab, dcl);
    }
    for (SCHNodeSetLoop n = seq->loopNodes(eClockNegedge); !n.atEnd(); ++n) {
      FLNodeElab* flowElab = *n;
      markDCLFlow(flowElab, dcl);
    }
  }

  // Mark all the combinational schedules
  for (SCHIterator i = dcl->getSchedule(); !i.atEnd(); ++i) {
    FLNodeElab* flowElab = *i;
    markDCLFlow(flowElab, dcl);
  }
}

void
SCHDerivedClockSchedules::markDCLFlow(FLNodeElab* flowElab,
                                      SCHDerivedClockLogic* dcl)
{
  FLN_ELAB_ASSERT((mFlowToDCLScheduleMap.find(flowElab) == 
                   mFlowToDCLScheduleMap.end()),
                  flowElab);
  mFlowToDCLScheduleMap.insert(std::make_pair(flowElab, dcl));
}

void SCHDerivedClockSchedules::clearAllDCLFlows(void)
{
  mFlowToDCLScheduleMap.clear();
}

SCHDerivedClockLogic*
SCHDerivedClockSchedules::findFlowSchedule(FLNodeElab* flowElab) const
{
  FlowToDCLScheduleMap::const_iterator pos;
  pos = mFlowToDCLScheduleMap.find(flowElab);
  if (pos != mFlowToDCLScheduleMap.end()) {
    return pos->second;
  } else {
    return NULL;
  }
}

void
SCHDerivedClockSchedules::findClkAndPIFlow(ClkAndPIFlowMap& clkAndPIFlowMap,
					   BufferedFlows& clkFlows,
					   BufferedFlows& piFlows)
{
  // Check if we should fix this based on a switch
  bool fixClocksAsData = mUtil->isFlagSet(eFixClocksAsData);
  bool dumpClocksAsData = mUtil->isFlagSet(eDumpClocksAsData);
  bool dumpPIFlowFix = mUtil->isFlagSet(eDumpPIFlowFix);

  // Bump the schedule pass so that we don't print messages more than once
  UInt32 newPass = mUtil->bumpSchedulePass();

  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s) {
    // Get the DCL this flow is in or use NULL
    FLNodeElab* flow = *s;
    SCHDerivedClockLogic* flowDCL;
    if (flow->isDerivedClockLogic() || flow->isDerivedClock()) {
      flowDCL = findFlowSchedule(flow);
      FLN_ELAB_ASSERT(flowDCL != NULL, flow);
    }
    else
      flowDCL = NULL;

    for (FLNodeElabLoop p = flow->loopFanin(); !p.atEnd(); ++p) {
      // Don't treat the fanin as level data if the fanin is an
      // initial block (that always runs before anyway so we don't
      // need to delay it) or it is an async reset (those are really
      // asynchronous and we should see the new value).
      FLNodeElab* realFanin = *p;
      FLNodeElab* fanin = mUtil->getAcyclicNode(realFanin);
      if ((flow != fanin) &&
          !mMarkDesign->isSequentialClockOrAsyncPin(flow, fanin) &&
          !mMarkDesign->isLatchClockUse(flow, fanin) &&
          !SCHUtil::isInitial(fanin)) {
	// If this is derived clock logic and we came either from
	// normal logic or derived clock logic for a different clock,
	// then we have a misuse. We do this with the derived clock
	// logic id stored in the depth for the flow node.
        //
        // Note that primary input flows, bound node flows, and
        // invalid pull driver flows are not scheduled so we don't
        // treat them as derived clock logic. The bound nodes are
        // introduced by depositSignal.
	SCHDerivedClockLogic* faninDCL = findFlowSchedule(fanin);
	if ((fanin->isDerivedClock() || (faninDCL != NULL)) &&
	    (!mMarkDesign->isPrimaryInput(fanin) && !fanin->isBoundNode() &&
             isValidDerivedNode(fanin))) {
	  FLN_ELAB_ASSERT(faninDCL != NULL, fanin);
	} else {
	  faninDCL = NULL;
        }

	// Check if this is a clock as data case. This occurs if the
	// output flow is a any sequential node and the input flow is
	// a primary clock or clock logic in a different schedule.
        //
        // The one exception to this is if the the flow DCL and fanin
        // DCL are in a cycle. If that is the case we want to buffer
        // the data even if they are the same DCL schedule because the
        // schedule runs in a loop. And we want to avoid data
        // circulating through.
        bool clockAsData = false;
        if (fanin->isPrimaryClock()) {
          // primary clocks are always delayed
          clockAsData = true;

        } else if (faninDCL != NULL) {
          // The fanin is computed by a DCL schedule. If the fanout
          // DCL is not the same then we should delay the clock (it
          // could be a normal or different DCL schedule).
          //
          // But if they are the same and it is a cycle, then we
          // should buffer the net because multiple iterations could
          // cause data to flow into a flop.
          if (flowDCL != faninDCL) {
            clockAsData = true;
          } else if (flowDCL->isCycleNode()) {
            clockAsData = true;
          }

        } else if (mMarkDesign->isPrimaryInput(fanin) &&
                   fanin->isDerivedClockLogic()) {
          // This is a PI that feeds both a DCL schedule and a normal
          // flop, it has to be buffered.
          // It may be that the PI feeds combinational logic that was
          // merged into the always block.
          clockAsData = true;
        }

	// Check if we have a PI flow case. This occurs if we have
	// input flow and the fanin is not a clock (covered above) but
	// it is a PI. The sequential node must be a in a derived
	// clock schedule. Otherwise we fix the issue another way.
	bool piFlow = (mUtil->isFlagSet(eNoInputFlow) && (flowDCL != NULL) &&
		       mMarkDesign->isPrimaryInput(fanin) &&
		       !fanin->isPrimaryClock());

	// Deal with the two cases
	if (clockAsData || piFlow) {
	  // Only print a message if we haven't printed one before and
	  // if we are not fixing the problem automatically
	  if (!fixClocksAsData && clockAsData) {
	    if (flow->getSchedulePass() != newPass) {
	      // Print the warning
	      HierName* clkName = fanin->getDefNet()->getSymNode();
	      mUtil->getMsgContext()->SchClockAsData(flow, *clkName);

	      // Mark all drivers of this net elab as done so we don't
	      // print the message more than once.
	      mUtil->markNetFlows(flow, newPass);
	    }
	  } else {
            // If the fanin net is a memory, there isn't anything we can
            // currently do. Alert the user.
            if (clockAsData && isLargeMem(fanin)) {
              HierName* memName = fanin->getDefNet()->getSymNode();
              MsgContext* msgContext = mUtil->getMsgContext();
              msgContext->SchMemClockAsData(flow, *memName);
              for (SCHClocksLoop l = faninDCL->loopClocks(); !l.atEnd(); ++l) {
                const NUNetElab* clkElab = *l;
                msgContext->SchClock(clkElab, *memName);
              }
            } else {
              // Store the information for processing
              FLNodeElabSet& clkAndPIFanin = clkAndPIFlowMap[flow];
              clkAndPIFanin.insert(fanin);

              // Store the flows for message printing
              if (dumpClocksAsData && clockAsData) {
                addDelay(clkFlows, fanin);
              } else if (dumpPIFlowFix && piFlow) {
                addDelay(piFlows, fanin);
              }
            }
          }
        } // if
      } // if
    } // for
  } // for

  // Make sure no-one bumped this while we were running
  INFO_ASSERT(mUtil->equalSchedulePass(newPass),
              "The schedule pass counter was incremented in the middle of finding clocks used as data");
} // SCHDerivedClockSchedules::findClkAndPIFlow

bool SCHDerivedClockSchedules::isLargeMem(FLNodeElab* flow) const
{
  bool largeMem = false;
  NUNet* net = flow->getDefNet()->getNet();
  if (net->is2DAnything()) {
    NUMemoryNet* mem = net->getMemoryNet();
    UInt32 size = mem->getBitSize();
    if ((int)(size) > mUtil->getBufferedMemoryThreshold()) {
      largeMem = true;
    }
  }
  return largeMem;
}

// Comparison operator so we can sort derived clock logic schedules by
// their depth
bool
SCHDerivedClockSchedules::lessDerivedClockDepth(SCHDerivedClockBase* dcl1,
						SCHDerivedClockBase* dcl2)
{
  int depth1;
  int depth2;

  // Check the easy case, they are identical
  if (dcl1 == dcl2)
    return false;

  // Make sure the depths are valid. This should never happen
  depth1 = dcl1->getDepth();
  SCHED_ASSERT(depth1 > DEPTH_START, dcl1);
  depth2 = dcl2->getDepth();
  SCHED_ASSERT(depth2 > DEPTH_START, dcl2);

  // We sort in order of the depth
  int cmp = depth1 - depth2;

  // In the event of ties, we break ties based first on the set of
  // input nets, then on clock, and then on scheduleMask for
  // regression-test consistency
  if (cmp == 0) {
    // Compare by input nets, but be careful because they may be null
    const SCHInputNets* ns1 = dcl1->getBranchNets();
    const SCHInputNets* ns2 = dcl2->getBranchNets();
    if ((ns1 == NULL) && (ns2 == NULL))
      cmp = 0;
    else if (ns1 == NULL)
      cmp = -1;
    else if (ns2 == NULL)
      cmp = 1;
    else
      cmp = SCHInputNetsFactory::compare(ns1, ns2);

    if (cmp == 0) {
      // Compare by schedules
      cmp = dcl1->compareSchedules(dcl2);

      // Can't have two derived clocks schedules with the same clock!
      SCHED_ASSERT2(cmp != 0, dcl1, dcl2);
    }
  } // if
  return cmp < 0;
} // SCHDerivedClockSchedules::lessDerivedClockDepth

void
SCHDerivedClockSchedules::sortSchedules(DCLGraph* dclGraph)
{
  // Transfer the schedules from the map to a sortable vector. Make
  // sure we haven't don't this yet.
  INFO_ASSERT(!mScheduleData->hasDerivedClockSchedules(),
              "Derived clock schedule sorting called without any schedules");
  UtSet<SCHDerivedClockBase*> covered;
  for (SchedMapLoop l(mSchedules); !l.atEnd(); ++l) {
    SCHDerivedClockBase* dclBase = l.getValue();
    if (covered.find(dclBase) == covered.end()) {
      mScheduleData->addDerivedClockSchedule(dclBase);
      covered.insert(dclBase);
    }
  }

  // Check if we have any work to do
  if (!mScheduleData->hasDerivedClockSchedules()) {
    return;
  }

  // Class to compute branch nets
  SCHBranchNets branchNets(mMarkDesign, 0);
  branchNets.init();

  // Initiate the scheduling
  SCHSortSchedules sortSchedules(mMarkDesign);
  for (Iter<GraphNode*> n = dclGraph->nodes(); !n.atEnd(); ++n) {
    GraphNode* node = *n;
    SCHDerivedClockBase* dclBase = dclGraph->castNode(node)->getData();
    const SCHInputNets* inputNets = computeBranchNets(&branchNets, dclBase);
    sortSchedules.addSched(dclBase, inputNets);
    dclBase->putBranchNets(inputNets);

    // Add the schedules fanouts. The graph is a fanout graph
    for (Iter<GraphEdge*> e = dclGraph->edges(node); !e.atEnd(); ++e) {
      GraphEdge* edge = *e;
      GraphNode* to = dclGraph->endPointOf(edge);
      SCHDerivedClockBase* fanoutDCLBase = dclGraph->castNode(to)->getData();
      sortSchedules.addFanout(dclBase, fanoutDCLBase);
    }
  }

  // Sort the schedules
  int depth = 0;
  SCHSortSchedules::ScheduleGroup schedGroup;
  while (sortSchedules.getNextSchedule(schedGroup)) {
    // Assign this group the same depth since they can be scheduled together
    INFO_ASSERT(!schedGroup.empty(),
                "Found an empty schedule group when sorting schedules");
    ++depth;
    SCHSortSchedules::ScheduleGroupLoop l;
    for (l = sortSchedules.loopSchedules(schedGroup); !l.atEnd(); ++l) {
      SCHScheduleBase* sched = *l;
      SCHDerivedClockBase* dclBase = dynamic_cast<SCHDerivedClockBase*>(sched);
      dclBase->putDepth(depth);
    }
    schedGroup.clear();
  }

  // Sort by the depth, then clock characteristics
  mScheduleData->sortDerivedClockSchedules(lessDerivedClockDepth);
} // SCHDerivedClockSchedules::sortSchedules

SCHDerivedClockBase*
SCHDerivedClockSchedules::createDerivedClock(NUCNetElabSet& clocks,
                                             const SCHScheduleMask* mask)
{
  // Make sure we didn't already create it
  for (NUCNetElabLoop l(clocks); !l.atEnd(); ++l) {
    const NUNetElab* derivedClk = *l;
    NU_ASSERT(mSchedules.find(derivedClk) == mSchedules.end(), derivedClk);
  }

  // Create and store this derived clock schedule
  SCHDerivedClockBase* dclBase;
  int id = nextID();
  dclBase = new SCHDerivedClockLogic(clocks, mask, mUtil, id);
  storeDerivedClock(dclBase);
  return dclBase;
} // SCHDerivedClockSchedules::createDerivedClock

void 
SCHDerivedClockSchedules::getDCLFlows(const NUNetElab* clkElab,
                                      FLNodeElabVector* flows) const
{
  // Gather all the elaborated flows for this clock. But watch out for
  // combinational cycles with more than one clock in them. They
  // create a multi-clock schedule. So gather up the clocks in the
  // cycle to be in one schedule.
  SCHUtil::ConstDriverLoop l;
  for (l = mUtil->loopNetDrivers(clkElab); !l.atEnd(); ++l) {
    FLNodeElab* flow = *l;
    if (flow->isDerivedClock()) {
      if (SCHMarkDesign::isCycle(flow)) {
        FLNodeElabVector lclFlows;
        mUtil->getAcyclicFlows(flow, &lclFlows);
        for (FLNodeElabVectorLoop f(lclFlows); !f.atEnd(); ++f) {
          FLNodeElab* cycleFlow = *f;
          if (cycleFlow->isDerivedClock()) {
            flows->push_back(cycleFlow);
          }
        }
      } else {
        flows->push_back(flow);
      }
    }
  }
}

void
SCHDerivedClockSchedules::storeDerivedClock(SCHDerivedClockBase* dcl)
{
  // Create the derived clock logic schedule and remember it for all clocks
  for (SCHClocksLoop l = dcl->loopClocks(); !l.atEnd(); ++l) {
    // Use the [] operator because we want to replace the derived
    // clock. This occurs when we decide to replace the existing
    // simple schedule with the cycle schedule.
    const NUNetElab* derivedClk = *l;
    mSchedules[derivedClk] = dcl;
  }
}

void
SCHDerivedClockSchedules::moveDerivedClock(SCHDerivedClockBase* dcl,
                                           SCHDerivedClockBase* newDcl)
{
  for (SCHClocksLoop l = dcl->loopClocks(); !l.atEnd(); ++l) {
    // Use the [] operator because we want to replace the derived
    // clock. This occurs when we decide to replace the existing
    // simple schedule with the cycle schedule.
    const NUNetElab* derivedClk = *l;
    mSchedules[derivedClk] = newDcl;
  }
}

const UtString*
SCHDerivedClockSchedules::MsgCallback::getReason(FLNodeElab*,
						 FLNodeElab* fanin) const
{
  // We used unelaborated data because we will add the buffer to all
  // instances even if it is only needed in a subset of the
  // instances.
  FLNode* unelabFanin = fanin->getFLNode();

  // Check if it is a clock message
  if (isInBufferedFlows(mClkFlows, unelabFanin))
    return &mClkReason;

  // Check if it is a pi message
  if (isInBufferedFlows(mPIFlows, unelabFanin))
    return &mPIReason;

  // Don't need to print these
  return NULL;
}

bool
SCHDerivedClockSchedules::MsgCallback::isInBufferedFlows(
  BufferedFlows& bufferedFlows,
  FLNode* fanin)  const
{
  return bufferedFlows.find(fanin) != bufferedFlows.end();
}

void
SCHDerivedClockSchedules::addDelay(BufferedFlows& bufferedFlows,
				   FLNodeElab* fanin)
{
  // Get the unelaborated flows because when we add this buffers it is
  // done in an unelaborated way.
  FLNode* unelabFanin = fanin->getFLNode();
  bufferedFlows.insert(unelabFanin);
}

// We create the input nets by computing the input nets for all the
// DCL sub components. However this is done before we have scheduled
// any flow nodes. So there are no sub schedules to look at. Instead,
// look at the timing signature for all the drivers of the net elab
// and use those.
const SCHInputNets*
SCHDerivedClockSchedules::computeBranchNets(SCHBranchNets* branchNets,
                                            SCHDerivedClockBase* dclBase)
{
  // Iterate over the drivers of the clock net elab looking for any
  // inputs that affect this schedule.
  const SCHInputNets* inputNets = NULL;
  SCHUtil::DerivedClockFlowsLoop l;
  for (l = mUtil->loopDerivedClockFlows(dclBase); !l.atEnd(); ++l) {
    // Get the signature mask for this elaborated flow
    FLNodeElab* flowElab = *l;
    const SCHSignature* sig = mMarkDesign->getSignature(flowElab);

    // If this flow is affected by input nets, gather those
    if (mScheduleFactory->hasInputMask(sig)) {
      const SCHInputNets* subNets = mMarkDesign->getFlowInputNets(flowElab);
      inputNets = mMarkDesign->appendInputNets(inputNets, subNets);
    }
  }

  // Iterate over the clocks and compute the branch nets for each
  // clock this schedule produces
  bool nullInputNets = false;
  for (SCHClocksLoop c = dclBase->loopClocks();
       !c.atEnd() && !nullInputNets;
       ++c) {
    const NUNetElab* clkElab = *c;
    const SCHInputNets* subNets = branchNets->compute(clkElab);
    if (subNets == NULL) {
      // If any of them fail, they all fail
      inputNets = NULL;
      nullInputNets = true;
    } else {
      inputNets = mMarkDesign->appendInputNets(inputNets, subNets);
    }
  }
  return inputNets;
} // SCHDerivedClockSchedules::computeBranchNets
  

void SCHDerivedClockSchedules::createSequentialCycleGraphs(UtList<FlowGraph*>* cycles,
                                                           bool writeDotFiles)
{
  // Create a flow graph containing combinational and sequential-clock dependencies
  FlowGraph* graph = createFlowGraph();

  // Find the cycles in the sequential flow graph
  GraphSCC graphSCC;
  graphSCC.compute(graph);
  if (graphSCC.hasCycle()) {
    for (GraphSCC::ComponentLoop l = graphSCC.loopCyclicComponents();
         !l.atEnd();
         ++l) {
      GraphSCC::Component* comp = *l;
      bool isSequential = false;
      for (GraphSCC::Component::NodeLoop n = comp->loopNodes();
           !n.atEnd() && !isSequential;
           ++n) {
        FlowGraph::Node* node = graph->castNode(*n);
        FLNodeElab* flow = node->getData();
        isSequential = SCHUtil::isSequential(flow);
      }
      if (isSequential) {
        FlowGraph* subGraph = dynamic_cast<FlowGraph*>(graphSCC.extractComponent(comp));
        cycles->push_back(subGraph);
      }
    }
  }
  
  // delete the full flow graph now that we have copies of the cyclic portions
  delete graph;

  // Sort the list of cycles for stability
  FlowGraphOrder order;
  cycles->sort(order);

  // Report sequential cycle data to the user
  int cycleNum = 0;
  for (UtList<FlowGraph*>::iterator i = cycles->begin();
       i != cycles->end();
       ++i) {
    FlowGraph* cycle = *i;

    // gather the nodes into a set so that we can loop through them sorted
    FLNodeElabHashSet flows;
    int cycleSize = 0;
    for (Iter<GraphNode*> iter = cycle->nodes(); !iter.atEnd(); ++iter)
    {
      GraphNode* gn = *iter;
      FlowGraph::Node* node = cycle->castNode(gn);
      FLNodeElab* flow = node->getData();
      ++cycleSize;
      flows.insert(flow);
    }

    // now print the messages for the set of flows
    mMarkDesign->reportCycleFlows(flows, true, cycle->size(), cycle->size(), 0, 0);

    // Write it out as a dot file, if requested
    if (writeDotFiles)
    {
      UtString str;
      str << "DCLCycle" << ++cycleNum << ".dot";
      FlowGraphDotWriter gdw(str, mUtil);
      gdw.output(cycle);
    }
  }
} // void SCHDerivedClockSchedules::createSequentialCycleGraphs


SCHDerivedClockSchedules::FlowGraph* SCHDerivedClockSchedules::createFlowGraph()
{
  GraphBuilder<FlowGraph> builder;

  UtSet<NUCycle*> covered;

  // set up graph nodes for each live FLNodeElab
  FLDesignElabIter iter(mUtil->getDesign(), mUtil->getSymbolTable(),
                        FLIterFlags::eAllElaboratedNets,
                        FLIterFlags::eAll,
                        FLIterFlags::eNone,
                        FLIterFlags::eBranchAtAll,
                        FLIterFlags::eIterNodeOnce);
  for (; !iter.atEnd(); ++iter)
  {
    FLNodeElab* elab = *iter;

    if (!elab->isLive())
      continue;

    if (SCHUtil::isInitial(elab))
      continue;
    
    if (elab->isEncapsulatedCycle())
      continue;

    // Add this node
    builder.addNode(elab);

    // For a sequential node, add an edge to the clock
    if (SCHUtil::isSequential(elab))
    {
      // Populate from the clocks
      EdgeType subEdgeType = eEdgeSeq;
      if (mUtil->isClockDivider(elab)) {
        subEdgeType = eEdgeDiv;
      }

      const NUNetElab* clkElab = getSequentialNodeClock(elab);
      SCHUtil::ConstDriverLoop l;
      for (l = mUtil->loopNetDrivers(clkElab); !l.atEnd(); ++l) {
        FLNodeElab* faninElab = *l;
        if (faninElab->isDerivedClock())
        {
          if (faninElab->isEncapsulatedCycle())
            faninElab = mUtil->getCycleOriginalNode(faninElab);
          builder.addEdge(elab,faninElab,subEdgeType);
        }
      }
    }
    // For a combinational node, add edges using loopDependencies()
    else
    {
      SCHMarkDesign::TrueFaninLoop l;
      for (l = mMarkDesign->loopDependencies(elab, SCHMarkDesign::eInternal);
           !l.atEnd(); ++l) {
        // Don't add a dependency if this fanin is for pull
        // drivers. This occurs when we add artificial dependencies
        // between a strong driver and the pull driver on a net. Adding
        // this edge makes tristate clock DCL schedules look like a
        // cycle when they really aren't. This affected some
        // artificially created test cases with tri-state clocks.
        FLNodeElab* faninElab = *l;
        if (faninElab->isEncapsulatedCycle())
          faninElab = mUtil->getCycleOriginalNode(faninElab);
        if (!mUtil->isPullDriver(faninElab)) {
          NUNetElab* netElab = faninElab->getDefNet();
          builder.addEdge(elab,faninElab,eEdgeComb);

          // If the fanin is an equivalent clock, also add an edge to
          // the master clocks.
          if (netElab != NULL) {
            const NUNetElab* clkElab = mMarkDesign->getClockMaster(netElab);
            if ((clkElab != netElab) && (clkElab != NULL)) {
              for (SCHUtil::ConstDriverLoop n = mUtil->loopNetDrivers(clkElab);
                   !n.atEnd(); ++n) {
                faninElab = *n;
                if (faninElab->isDerivedClock())
                {
                  if (faninElab->isEncapsulatedCycle())
                    faninElab = mUtil->getCycleOriginalNode(faninElab);
                  builder.addEdge(elab,faninElab,eEdgeComb);
                }
              }
            }
          }
        }
      }
    }
  }

  return builder.getGraph();
}

SCHDerivedClockSchedules::DCLGraph* SCHDerivedClockSchedules::createDCLGraph()
{
  // Create a graph using the elaborated flow
  // to a DCL schedule graph.
  DCLFlowGraph* flowGraph = createDCLFlowGraph();

  // Translate the flow graph to one with just DCL schedules.
  DCLFlowToSchedGraph flowToSchedGraph(this, flowGraph);
  DCLGraph* graph = flowToSchedGraph.transform();

  // Destroy the flow graph and return the newly created DCL
  // graph. This is a potentially cyclic graph.
  destroyDCLFlowGraph(flowGraph);
  return graph;
} // SCHDerivedClockSchedules::createDCLGraph

SCHDerivedClockSchedules::DCLFlowGraph*
SCHDerivedClockSchedules::createDCLFlowGraph()
{
  // Create the graph and some auxilliary data
  DCLFlowGraph* graph = new DCLFlowGraph;
  FlowGraphMap flowGraphMap;

  // Create the clock graph nodes
  typedef UtMap<const NUNetElab*, DCLFlowGraph::Node*> ClockNodes;
  ClockNodes clockNodes;
  for (SCHClocksLoop p = mMarkDesign->loopClocks(); !p.atEnd(); ++p) {
    // Check if this is a derived clock
    bool isDerivedClock = false;
    const NUNetElab* clk = *p;
    SCHUtil::ConstDriverLoop l;
    for (l = mUtil->loopNetDrivers(clk); !l.atEnd() && !isDerivedClock; ++l) {
      FLNodeElab* flow = *l;
      isDerivedClock = flow->isDerivedClock();
    }

    // Create a graph node for derived clocks if we haven't
    // already. We could have seen this already if this clock is part
    // of a combinational cycle with other derived clocks.
    if (isDerivedClock) {
      DCLFlowGraph::Node* node = NULL;
      FLNodeElabSet* flowElabs = NULL;
      ClockNodes::iterator pos = clockNodes.find(clk);
      if (pos == clockNodes.end()) {
        // Create the graph node
        flowElabs = new FLNodeElabSet;
        node = new DCLFlowGraph::Node(flowElabs);
        graph->addNode(node);
        clockNodes.insert(ClockNodes::value_type(clk, node));
      } else {
        // Already created, get the space for all the elaborated flow
        node = pos->second;
        flowElabs = node->getData();
      }

      // Gather the flowElabs that are part of the clock logic. Also
      // populate the map so we can easily find a graph node from a
      // FLNodeElab
      FLNodeElabVector flows;
      getDCLFlows(clk, &flows);
      for (FLNodeElabVectorLoop l(flows); !l.atEnd(); ++l) {
        FLNodeElab* flow = *l;
        flowElabs->insert(flow);
        flowGraphMap.insert(FlowGraphMap::value_type(flow, node));

        // If this is a clock, make sure the map is populated so that
        // we don't create a different node in the graph for the same
        // clock node.
        NUNetElab* netElab = flow->getDefNet();
        if (mMarkDesign->isValidClock(netElab)) {
          clockNodes.insert(ClockNodes::value_type(netElab, node));
        }
      }
    } // if
  } // for

  // Visit the flow graph and populate the graph edges in a local
  // map. We do this because we want to mark the edges between clocks
  // as either combinational, sequential, or divider. Since there can
  // be multiple edges between two clocks of multiple types, we choose
  // a winner. So this code keeps track of the edges by priority. We
  // can then sumarize the data and add it to the actual graph.
  EdgeTypeMap edgeTypeMap;
  for (Iter<GraphNode*> n = graph->nodes(); !n.atEnd(); ++n) {
    GraphNode* node = *n;
    DCLFlowGraph::Node* dclNode = graph->castNode(node);
    FLNodeElabSet* flowElabs = dclNode->getData();
    GraphCovered covered;
    for (FLNodeElabSetLoop f(*flowElabs); !f.atEnd(); ++f) {
      FLNodeElab* flowElab = *f;
      populateDCLGraphRecurse(dclNode, flowElab, graph, &flowGraphMap,
                              &edgeTypeMap, &covered);
    }
  }

  // Make sure that edges exist whenever a DCL schedule's mask
  // references a derived clock.
  for (Iter<GraphNode*> n = graph->nodes(); !n.atEnd(); ++n) {
    GraphNode* node = *n;
    DCLFlowGraph::Node* dclNode = graph->castNode(node);
    FLNodeElabSet* flowElabs = dclNode->getData();
    const SCHScheduleMask* mask = computeMask(flowElabs);
    GraphCovered covered;
    for (SCHScheduleMask::SortedEvents e = mask->loopEvents(); !e.atEnd(); ++e)
    {
      const SCHEvent* event = *e;
      if (event->isClockEvent()) {
        // find the node for this clock
        const STSymbolTableNode* clock = event->getClock();
        NUNetElab* clkElab = NUNetElab::find(clock);
        ST_ASSERT(clkElab != NULL, clock);

        // add edges to represent the clock dependency
        populateDCLGraphFromClk(dclNode, clkElab, graph, &flowGraphMap,
                                &edgeTypeMap, eEdgeSeq, &covered);
      }
    }
  }
  
  // Now populate the edges in the graph. We populate the edges in the
  // direction of data flow (not fanin pointers). This is because the
  // graph queue works this way.
  for (EdgeTypeMapLoop l(edgeTypeMap); !l.atEnd(); ++l) {
    // Get the from and to nodes
    Edge edge = l.getKey();
    DCLFlowGraph::Node* to = edge.first;
    DCLFlowGraph::Node* from = edge.second;

    // Get the edge type and add it to the graph
    EdgeType type = l.getValue();
    DCLFlowGraph::Edge* graphEdge = new DCLFlowGraph::Edge(to, type);
    from->addEdge(graphEdge);
  }

  return graph;
} // SCHDerivedClockSchedules::createDCLFlowGraph

void
SCHDerivedClockSchedules::destroyDCLFlowGraph(DCLFlowGraph* flowGraph)
{
  // Delete all the FLNodeElabSets
  // All done
  for (Iter<GraphNode*> n = flowGraph->nodes(); !n.atEnd(); ++n) {
    GraphNode* node = *n;
    DCLFlowGraph::Node* dclNode = flowGraph->castNode(node);
    FLNodeElabSet* flowElabs = dclNode->getData();
    delete flowElabs;
  }
  delete flowGraph;
}

void
SCHDerivedClockSchedules::populateDCLGraphRecurse(
  DCLFlowGraph::Node* fanoutNode,
  FLNodeElab* flowElab,
  DCLFlowGraph* graph,
  FlowGraphMap* flowGraphMap,
  EdgeTypeMap* edgeTypeMap,
  GraphCovered* covered)
{
  // If this is a combinational node, we have to continue visiting
  // all fanin to create the graph. If it is a sequential node, we
  // only visit the clock path
  if (!SCHUtil::isSequential(flowElab)) {
    // Combinational
    SCHMarkDesign::TrueFaninLoop l;
    for (l = mMarkDesign->loopDependencies(flowElab, SCHMarkDesign::eInternal);
         !l.atEnd(); ++l) {
      // Don't add a dependency if this fanin is for pull
      // drivers. This occurs when we add artificial dependencies
      // between a strong driver and the pull driver on a net. Adding
      // this edge makes tristate clock DCL schedules look like a
      // cycle when they really aren't. This affected some
      // artificially created test cases with tri-state clocks.
      FLNodeElab* faninElab = *l;
      if (!mUtil->isPullDriver(faninElab)) {
        NUNetElab* netElab = faninElab->getDefNet();
        populateDCLGraph(fanoutNode, faninElab, graph, flowGraphMap, edgeTypeMap,
                         eEdgeComb, covered);

        // If the fanin is an equivalent clock, also visit the master
        // clocks.
        if (netElab != NULL) {
          const NUNetElab* clkElab = mMarkDesign->getClockMaster(netElab);
          if ((clkElab != netElab) && (clkElab != NULL)) {
            populateDCLGraphFromClk(fanoutNode, clkElab, graph, flowGraphMap,
                                    edgeTypeMap, eEdgeComb, covered);
          }
        }
      }
    }
  } else {
    // Sequential, get the clock pin
    const NUNetElab* clkElab = getSequentialNodeClock(flowElab);

    // Populate from the clocks
    EdgeType subEdgeType = eEdgeSeq;
    if (mUtil->isClockDivider(flowElab)) {
      subEdgeType = eEdgeDiv;
    }
    populateDCLGraphFromClk(fanoutNode, clkElab, graph, flowGraphMap,
                            edgeTypeMap, subEdgeType, covered);
  }
} // SCHDerivedClockSchedules::populateDCLGraphRecurse

void
SCHDerivedClockSchedules::populateDCLGraphFromClk(
  DCLFlowGraph::Node* fanoutNode,
  const NUNetElab* clkElab,
  DCLFlowGraph* graph,
  FlowGraphMap* flowGraphMap,
  EdgeTypeMap* edgeTypeMap,
  EdgeType edgeType,
  GraphCovered* covered)
{
  SCHUtil::ConstDriverLoop l;
  for (l = mUtil->loopNetDrivers(clkElab); !l.atEnd(); ++l) {
    FLNodeElab* faninElab = *l;
    if (faninElab->isDerivedClock())
    {
      populateDCLGraph(fanoutNode, faninElab, graph, flowGraphMap, edgeTypeMap,
                       edgeType, covered);
    }
  }
}

void
SCHDerivedClockSchedules::populateDCLGraph(DCLFlowGraph::Node* fanoutNode,
                                           FLNodeElab* faninElab,
                                           DCLFlowGraph* graph,
                                           FlowGraphMap* flowGraphMap,
                                           EdgeTypeMap* edgeTypeMap,
                                           EdgeType edgeType,
                                           GraphCovered* covered)
{
  // Make sure we haven't been here before
  FlowEdgePair flowEdgePair(faninElab, edgeType);
  if (covered->find(flowEdgePair) != covered->end()) {
    return;
  }
  covered->insert(flowEdgePair);

  // Check if this is a clock. If so, we stop here and add or modify
  // the edge in our map.
  FlowGraphMap::iterator pos1 = flowGraphMap->find(faninElab);
  if (pos1 != flowGraphMap->end()) {
    // Check if the edge already exists in our edge type map
    DCLFlowGraph::Node* node = pos1->second;
    Edge edge(fanoutNode, node);
    EdgeTypeMap::iterator pos2 = edgeTypeMap->find(edge);
    if (pos2 != edgeTypeMap->end()) {
      // It already exists, pick the edge type with higher preference
      // to keep
      EdgeType curEdgeType = pos2->second;
      if (edgeType < curEdgeType) {
        pos2->second = edgeType;
      }

    } else {
      // it doesn't exist so add it
      edgeTypeMap->insert(EdgeTypeMap::value_type(edge, edgeType));
    }
  } else {
    // It's not a clock; recurse.
    populateDCLGraphRecurse(fanoutNode, faninElab, graph, flowGraphMap,
                            edgeTypeMap, covered);
  }
} // SCHDerivedClockSchedules::populateDCLGraph

void
SCHDerivedClockSchedules::createDCLCycleSchedules(DCLGraph* graph,
                                                  const GraphSCC& graphSCC,
                                                  Graph* compGraph,
                                                  CompScheds* compScheds)
{
  // Iterate over the strongly connectected components. For all cyclic
  // strongly connected components (with more than one node?), create
  // a DCLCycleSchedule. Store the DCL schedules in the component
  // graph node's scratch field for use by schedule sorting.
  UInt32 numCycles = 0;
  for (Iter<GraphNode*> n = compGraph->nodes(); !n.atEnd(); ++n) {
    GraphNode* node = *n;
    SCHDerivedClockBase* dclBase = NULL;
    GraphSCC::Component* component = graphSCC.getComponent(node);
    if (component->isCyclic()) {
      // Build a cycle schedule
      dclBase = buildDerivedClockCycle(graphSCC, component);
      ++numCycles;
    } else {
      // For simple DCL schedules, get the data from the original DCL
      // graph. That is where we stored the simple DCL in the loop
      // above.
      GraphSCC::Component::NodeLoop l = component->loopNodes();
      GraphNode* subNode = *l;
      DCLGraph::Node* dclNode = graph->castNode(subNode);
      dclBase = dclNode->getData();
      SCHED_ASSERT(component->size() == 1, dclBase);
    }

    // Store it for our caller
    compScheds->insert(CompScheds::value_type(component, dclBase));
  }
} // SCHDerivedClockSchedules::createDCLCycleSchedules

// This routine compares edges so that the edges that are better for
// breaking cycles appear last. The sort order is first by edge type,
// and second by the likelyhood that it will break the cycle
// effectively (info on the to node).
bool
SCHDerivedClockSchedules::DCLCompareBreakableEdges::operator()(
  const FullEdge& e1,
  const FullEdge& e2) const
{
  // Check the easy case
  if (e1 == e2) {
    return false;
  }

  // We sort by the edge type first
  GraphEdge* edge1 = e1.second;
  GraphEdge* edge2 = e2.second;
  EdgeType type1 = mGraph->castEdge(edge1)->getData();
  EdgeType type2 = mGraph->castEdge(edge2)->getData();
  int cmp = type1 - type2;

  // If they have the same type, pick a node with the least highest
  // priority incident edge counts.
  if (cmp == 0) {
    GraphNode* n1 = mGraph->endPointOf(edge1);
    GraphNode* n2 = mGraph->endPointOf(edge2);
    ScratchData* scratch1 = (ScratchData*)mGraph->getScratch(n1);
    ScratchData* scratch2 = (ScratchData*)mGraph->getScratch(n2);
    for (int i = eEdgeHighestPreference;
         (i <= eEdgeLowestPreference) && (cmp == 0);
         ++i) {
      cmp = scratch2->mEdgeCounts[i] - scratch1->mEdgeCounts[i];
    }

    // If they still have the same incident counts, then compare by
    // to node schedule
    if (cmp == 0) {
      // Sort by the schedules for the node so we get stability in the algorithm
      const DCLGraph::Node* dclNode1 = mGraph->castNode(n1);
      const DCLGraph::Node* dclNode2 = mGraph->castNode(n2);
      SCHDerivedClockBase* dclBase1 = dclNode1->getData();
      SCHDerivedClockBase* dclBase2 = dclNode2->getData();
      cmp = dclBase1->compareSchedules(dclBase2);

      // If they are still the same, compare by from node
      // schedule. They can't be the same.
      if (cmp == 0) {
        dclNode1 = mGraph->castNode(e1.first);
        dclNode2 = mGraph->castNode(e2.first);
        SCHDerivedClockBase* dclBase1 = dclNode1->getData();
        SCHDerivedClockBase* dclBase2 = dclNode2->getData();
        cmp = dclBase1->compareSchedules(dclBase2);

        // The two nodes must be different
        SCHED_ASSERT2(cmp != 0, dclBase1, dclBase2);
      }
    }
  }

  return cmp < 0;
} // SCHDerivedClockSchedules::DCLCompareBreakableEdges::operator

// This routine compares nodes so that we get some stability in
// choosing nodes to process in the GraphQueue.
bool
SCHDerivedClockSchedules::DCLCompareNode::operator()(const GraphNode* n1,
                                                     const GraphNode* n2) const
{
  // Check the easy case
  if (n1 == n2) {
    return false;
  }

  // Sort by the schedules for the node so we get stability in the algorithm
  const DCLGraph::Node* dclNode1 = mGraph->castNode(n1);
  const DCLGraph::Node* dclNode2 = mGraph->castNode(n2);
  SCHDerivedClockBase* dclBase1 = dclNode1->getData();
  SCHDerivedClockBase* dclBase2 = dclNode2->getData();
  int cmp = dclBase1->compareSchedules(dclBase2);
  
  // The two nodes must be different
  SCHED_ASSERT2(cmp != 0, dclBase1, dclBase2);
  return cmp < 0;
}

SCHDerivedClockSchedules::DCLGraphQueue::~DCLGraphQueue()
{
  delete mNodeOrder;
  delete mEdgeOrder;
//   INFO_ASSERT(mBreakEdges->empty(),
//               "Destructing derived schedule graph queue before it has been completely scheduled");
  delete mBreakEdges;

  // Delete the scratch space in all the nodes
  for (Iter<GraphNode*> n = mGraph->nodes(); !n.atEnd(); ++n) {
    GraphNode* node = *n;
    ScratchData* scratch = (ScratchData*)mGraph->getScratch(node);
    delete scratch;
    mGraph->setScratch(node, 0);
  }
}

void
SCHDerivedClockSchedules::DCLGraphQueue::init(DCLGraph* graph,
                                              SCHDerivedClockCycle* dclCycle)
{
  // Initialize the cycle breaking code. We sort the edges by their
  // ability to break the cycle effectively but choose any order for
  // nodes.
  mGraph = graph;
  mDCLCycle = dclCycle;
  mNodeOrder = new DCLCompareNode(mGraph);
  mEdgeOrder = new UtGraphQueue::EdgeCmp;
  mBreakEdges = new BreakEdges;
  UtGraphQueue::init(graph, mNodeOrder, mEdgeOrder);

  // Sort the break edges from the graph
  DCLCompareBreakableEdges compareBreakableEdges(mGraph);
  std::sort(mBreakEdges->begin(), mBreakEdges->end(),  compareBreakableEdges);
}

void
SCHDerivedClockSchedules::DCLGraphQueue::scheduleNode(GraphNode* node,
                                                      SInt32 depth)
{
  // Store the data in the DCL Cycle
  DCLGraph::Node* dclNode = mGraph->castNode(node);
  SCHDerivedClockBase* dclBase = dclNode->getData();
  dclBase->putDepth(depth);
  SCHED_ASSERT(dclBase->castDCL() != NULL, dclBase);
  mDCLCycle->addSubSchedule(dclBase->castDCL());

  // If we are dumping cycles, print that we are scheduling this
  // simple DCL schedule
  if (mVerbose) {
    UtIO::cout() << "Scheduling: ";
    dclBase->print(false);
  }
}

void SCHDerivedClockSchedules::DCLGraphQueue::breakCycle()
{
  // Pick an edge to break from the back of the edge vector (they are
  // sorted in reverse order). We keep going until we get a target
  // node with a 0 fanin count.
  bool readyNode = false;
  while (!readyNode) {
    // Choose the next best edge
    INFO_ASSERT(!mBreakEdges->empty(),
              "Unable to find a candidate to break a DCL cycle");
    FullEdge fullEdge = mBreakEdges->back();
    mBreakEdges->pop_back();
    GraphEdge* edge = fullEdge.second;

    // Tell the base class to break this edge
    breakEdge(edge);

    // Check if the to node can be scheduled
    GraphNode* to = mGraph->endPointOf(edge);
    readyNode = getFaninCount(to) == 0;

    // If we are in verbose mode, print for the user
    if (mVerbose) {
      // Figure out the edge string
      EdgeType type = mGraph->castEdge(edge)->getData();
      const char* edgeStr = SCHDerivedClockSchedules::getEdgeString(type);
      UtIO::cout() << "Breaking " << edgeStr << " edge to (fanins = "
                   << getFaninCount(to) << "): ";
      DCLGraph::Node* dclNode = mGraph->castNode(to);
      SCHDerivedClockBase* dclBase = dclNode->getData();
      SCHED_ASSERT(dclBase->castDCL() != NULL, dclBase);
      dclBase->print(false);
    }
  }
}

void
SCHDerivedClockSchedules::DCLGraphQueue::putNodeData(GraphNode* node,
                                                     SIntPtr data)
{
  ScratchData* scratch = (ScratchData*)mGraph->getScratch(node);
  scratch->mData = data;
}

SIntPtr
SCHDerivedClockSchedules::DCLGraphQueue::getNodeData(GraphNode* node) const
{
  ScratchData* scratch = (ScratchData*)mGraph->getScratch(node);
  return scratch->mData;
}

void
SCHDerivedClockSchedules::DCLGraphQueue::initGraphNode(GraphNode* node)
{
  // Create the scratch data for this node
  ScratchData* scratch = new ScratchData;
  scratch->mData = 0;
  memset(scratch->mEdgeCounts, 0, sizeof(scratch->mEdgeCounts));
  mGraph->setScratch(node, (UIntPtr)scratch);

  // Initialize the depth to 0 so that we can tell when it has been
  // set.
  mGraph->castNode(node)->getData()->putDepth(0);
}

bool
SCHDerivedClockSchedules::DCLGraphQueue::initGraphEdge(GraphNode* from,
                                                       GraphEdge* edge,
                                                       GraphNode* to)
{
  // Process this edge if it isn't an edge to itself
  if (from != to) {
    // Increment the incident count for this edge type. 
    ScratchData* scratch = (ScratchData*)mGraph->getScratch(to);
    DCLGraph::Edge* dclEdge = mGraph->castEdge(edge);
    EdgeType type = dclEdge->getData();
    ++(scratch->mEdgeCounts[type]);

    // Remember this edge in the break edges. We sort them later
    mBreakEdges->push_back(FullEdge(from, edge));
    return true;
  } else {
    return false;
  }
}

// Using an SCC component, create a DCL cycle and populate it with an
// ordered set of simple DCL schedules. Those simple DCL schedules
// should have been created in advance.
SCHDerivedClockBase*
SCHDerivedClockSchedules::buildDerivedClockCycle(const GraphSCC& graphSCC,
                                                 GraphSCC::Component* comp)
{
  // Create the derived clock cycle
  int id = nextID();
  SCHDerivedClockCycle* dclCycle = new SCHDerivedClockCycle(id);

  // Extract the sub graph for this component. We want to use that
  // graph and schedule the nodes with the graph queue
  Graph* subGraph = graphSCC.extractComponent(comp, true);
  DCLGraph* dclSubGraph = dynamic_cast<DCLGraph*>(subGraph);
  DCLGraphQueue* dclQueue = new DCLGraphQueue(mUtil->isFlagSet(eDumpCycles));
  dclQueue->init(dclSubGraph, dclCycle);
  dclQueue->scheduleWalk();
  delete dclQueue;
  delete subGraph;

  // Store the schedule. We don't do this till after we schedule the
  // sub-schedules so that the clock set is complete.
  storeDerivedClock(dclCycle);
  return dclCycle;
}

const SCHScheduleMask*
SCHDerivedClockSchedules::computeMask(FLNodeElabSet* flowElabs)
{
  const SCHScheduleMask* mask = NULL;
  for (FLNodeElabSetLoop f(*flowElabs); !f.atEnd(); ++f) {
    // Add to the mask for the schedule
    FLNodeElab* flow = *f;
    const SCHSignature* sig = mMarkDesign->getSignature(flow);
    const SCHScheduleMask* newMask = sig->getTransitionMask();
    mask = mScheduleFactory->appendMasks(newMask, mask);
  }
  return mask;
}

SCHDerivedClockBase*
SCHDerivedClockSchedules::buildDerivedClockLogic(FLNodeElabSet* flowElabs)
{
  // Gather the clock nets
  NUCNetElabSet clocks;
  for (FLNodeElabSetLoop f(*flowElabs); !f.atEnd(); ++f) {
    // Add to the mask for the schedule
    FLNodeElab* flow = *f;
    clocks.insert(flow->getDefNet());
  }

  // Get the mask for this schedule
  const SCHScheduleMask* mask = computeMask(flowElabs);
  if (mask == NULL) {
    FLNodeElab* flowElab = *(flowElabs->begin());
    FLN_ELAB_ASSERT(mask != NULL, flowElab);
  }

  // Create the schedule
  SCHDerivedClockBase* dclBase = createDerivedClock(clocks, mask);
  mask->assign();
  if (dclBase == NULL) {
    FLNodeElab* flowElab = *(flowElabs->begin());
    FLN_ELAB_ASSERT(dclBase != NULL, flowElab);
  }
  return dclBase;
} // SCHDerivedClockSchedules::buildDerivedClockLogic

const NUNetElab*
SCHDerivedClockSchedules::getSequentialNodeClock(FLNodeElab* flowElab)
{
  NUUseDefNode* driver = flowElab->getUseDefNode();
  const NUNetElab* clkElab;
  mMarkDesign->getSequentialNodeClock(flowElab, driver, NULL, &clkElab);
  return clkElab;
}

void
SCHDerivedClockSchedules::printDCLCycles(const GraphSCC& graphSCC,
                                         const CompScheds& compScheds)
{
  if (graphSCC.hasCycle()) {
    int cycleNum = 0;

    // Gather the schedules in a vector that we can sort
    typedef UtVector<GraphSCC::Component*> Components;
    Components components;
    GraphSCC::ComponentLoop l;
    for (l = graphSCC.loopCyclicComponents(); !l.atEnd(); ++l) {
      GraphSCC::Component* comp = *l;
      components.push_back(comp);
    }

    // Sort them by the DCL cycle schedule they are in
    CmpComp cmp(compScheds);
    std::sort(components.begin(), components.end(), cmp);

    // Print the sorted schedules
    if (!components.empty()) {
      printKey();
    }
    typedef Loop<Components> ComponentsLoop;
    for (ComponentsLoop l(components); !l.atEnd(); ++l) {
      // Extract the sub graph
      GraphSCC::Component* comp = *l;
      Graph* cycleGraph = graphSCC.extractComponent(comp);

      // Write it out as a dot file
      UtString str;
      str << "DCLCycle" << ++cycleNum << ".dot";
      DCLGraphDotWriter gdw(str, mUtil);
      gdw.output(cycleGraph);

      // Get the graph and transpose it (to match other cycle dumping)
      DCLGraph* dclCycleGraph = dynamic_cast<DCLGraph*>(cycleGraph);
      UtGraphTranspose<DCLGraph> transpose(dclCycleGraph);
      transpose.transpose();

      // Print information about it
      printClocks(dclCycleGraph, cycleNum);
      delete cycleGraph;
    } // for
  }
} // SCHDerivedClockSchedules::printDCLCycles

void
SCHDerivedClockSchedules::SCCToDCLGraph::mapNodeData(GraphSCC::Component* comp,
                                                     SCHDerivedClockBase** dcl,
                                                     UInt32*)
{
  CompScheds::iterator pos = mCompScheds->find(comp);
  INFO_ASSERT(pos != mCompScheds->end(),
              "Unable to find corresponding DCL schedule for a DCL cycle node");
  *dcl = pos->second;
}

SCHDerivedClockSchedules::DCLFlowToSchedGraph::DCLFlowToSchedGraph(
  SCHDerivedClockSchedules* dclScheds, DCLFlowGraph* srcGraph) :
  DCLFlowToSchedGraphBase(srcGraph), mDCLSchedules(dclScheds)
{}

void
SCHDerivedClockSchedules::DCLFlowToSchedGraph::mapNodeData(
  DCLFlowGraph::NodeDataType srcNodeData,
  DCLGraph::NodeDataType* dstNodeData,
  UInt32*)
{
  // Check the cache first, we don't want to create duplicates
  Cache::iterator pos = mCache.find(srcNodeData);
  if (pos == mCache.end()) {
    *dstNodeData = mDCLSchedules->buildDerivedClockLogic(srcNodeData);
    mCache.insert(Cache::value_type(srcNodeData, *dstNodeData));
  } else {
    *dstNodeData = pos->second;
  }
}

SCHDerivedClockSchedules::DCLGraph*
SCHDerivedClockSchedules::mergeSchedules(DCLGraph* graph)
{
  // Make sure the number of IDs doesn't change through this process.
  int curNumIDs = numIDs();

  // Sort all the schedules by their mask
  MergableSchedules mergableSchedules;
  for (Iter<GraphNode*> n = graph->nodes(); !n.atEnd(); ++n) {
    // Cycles are not mergable
    GraphNode* node = *n;
    SCHDerivedClockBase* dclBase = getDCLSchedule(graph, node);
    if (dclBase->castDCLCycle() == NULL) {
      addMergableSchedule(&mergableSchedules, graph, node);
    }
  }

  // For each set of schedules with common trigger masks, merge
  // schedules where there is no path between them.
  TransClosure closure;
  for (MergableSchedulesLoop l(mergableSchedules); !l.atEnd(); ++l) {
    GraphNodes* subSchedules = l.getValue();
    mergeSubSchedules(graph, subSchedules, &closure);
  }

  // Clean up the graph to remove the duplicate nodes
  DCLGraph* newGraph = cleanupGraph(graph);

  // Clean up the allocated space
  for (MergableSchedulesLoop l(mergableSchedules); !l.atEnd(); ++l) {
    GraphNodes* subSchedules = l.getValue();
    delete subSchedules;
  }
  for (TransClosureLoop l(closure); !l.atEnd(); ++l) {
    DynBitVector* paths = l.getValue();
    delete paths;
  }

  INFO_ASSERT(curNumIDs == numIDs(),
              "The number of derived clock schedules changed while processing them");
  return newGraph;
} // SCHDerivedClockSchedules::mergeSchedules

void 
SCHDerivedClockSchedules::addMergableSchedule(
  MergableSchedules* mergableSchedules,
  DCLGraph* graph,
  GraphNode* node)
{
  // Get the schedule mask and DCL nets for this schedule
  SCHDerivedClockBase* dclBase = getDCLSchedule(graph, node);
  const SCHScheduleMask* mask = dclBase->getMask();
  const SCHInputNets* inputNets = dclBase->getDCLNets();
  DCLKey key(mask, inputNets);

  // Find the spot to store it. Allocate it if we haven't already
  GraphNodes* nodes;
  MergableSchedules::iterator pos = mergableSchedules->find(key);
  if (pos == mergableSchedules->end()) {
    nodes = new GraphNodes;
    mergableSchedules->insert(MergableSchedules::value_type(key, nodes));
  } else {
    nodes = pos->second;
  }

  // Add it to the set of mergable schedules. We count on not having
  // seen this graph node or schedule before.
  nodes->push_back(node);
} // SCHDerivedClockSchedules::addMergableSchedule

SCHDerivedClockSchedules::TransClosureWalker::Command
SCHDerivedClockSchedules::TransClosureWalker::visitNodeBefore(Graph*, 
                                                             GraphNode* node)
{
  // Check if we have computed this before. If so, we don't need to do
  // anything.
  TransClosure::iterator pos = mClosure->find(node);
  if (pos != mClosure->end()) {
    // Skip does not call the visitNodeAfter; it backtracks to the
    // caller. So we need to apply the closure data to our parent
    applyClosureData(node);
    return GW_SKIP;
  }

  // Allocate space for this transitive closure data. It will be
  // populated by continuing to walk.
  DynBitVector* edgeMask = new DynBitVector(mNumIDs);
  mClosure->insert(TransClosure::value_type(node, edgeMask));
  pushClosureData(edgeMask);
  return GW_CONTINUE;
}

void 
SCHDerivedClockSchedules::TransClosureWalker::pushClosureData(
  DynBitVector* edgeMask)
{
  mClosureStack.push_back(edgeMask);
}

void SCHDerivedClockSchedules::TransClosureWalker::popClosureData()
{
  INFO_ASSERT(!mClosureStack.empty(),
              "Transitive closure recursion finishing before it was completed");
  mClosureStack.pop_back();
}

void 
SCHDerivedClockSchedules::TransClosureWalker::applyClosureData(GraphNode* node)
{
  // If we are at the top of the stack, there is nothing to do. This
  // occurs if this node is the start of the graph walk.
  if (mClosureStack.empty())
    return;

  // Get the current parent from the stack
  DynBitVector* parentEdgeMask = mClosureStack.back();

  // Get this nodes edge mask
  TransClosure::iterator pos = mClosure->find(node);
  INFO_ASSERT(pos != mClosure->end(),
              "Unable to get the trigger condition for a DCL schedule");
  DynBitVector* edgeMask = pos->second;

  // Apply the edge mask and this nodes edge to the parent node
  *parentEdgeMask |= *edgeMask;
  int id = mGetGraphID(mGraph, node);
  parentEdgeMask->set(id);
}

SCHDerivedClockBase*
SCHDerivedClockSchedules::getDCLSchedule(DCLGraph* graph, GraphNode* node)
{
  return graph->castNode(node)->getData();
}

void
SCHDerivedClockSchedules::putDCLSchedule(DCLGraph* graph, GraphNode* node,
                                         SCHDerivedClockBase* dclBase)
{
  graph->castNode(node)->setData(dclBase);
}

int
SCHDerivedClockSchedules::getGraphID(DCLGraph* graph, GraphNode* node)
{
  SCHDerivedClockBase* dclBase = getDCLSchedule(graph, node);
  return dclBase->getID();
}

DynBitVector*
SCHDerivedClockSchedules::getFanoutMask(TransClosure* closure,
                                        DCLGraph* graph,
                                        GraphNode* node)
{
  // If we haven't computed this before, do so now
  TransClosure::iterator pos = closure->find(node);
  if (pos == closure->end()) {
    // Populate the data
    TransClosureWalker walker(graph, getGraphID, closure, numIDs());
    walker.walkFrom(graph, node);

    // It should be in the closure set now
    pos = closure->find(node);
    SCHED_ASSERT(pos != closure->end(), getDCLSchedule(graph, node));
  }

  // Return the data
  DynBitVector* edgeMask = pos->second;
  return edgeMask;
} // SCHDerivedClockSchedules::getFanoutMask

void 
SCHDerivedClockSchedules::mergeSubSchedules(DCLGraph* graph,
                                            GraphNodes* nodes,
                                            TransClosure* closure)
{
  // Allocate space to keep track of partitions.
  typedef UtVector<DCLPartition*> Partitions;
  typedef Loop<Partitions> PartitionsLoop;
  Partitions partitions;

  // Iterate over the set of nodes organizing them into merge
  // partitions. We merge the nodes if there is no path between them.
  //
  // Note that this algorithm is N**2, but we expect N to be
  // relatively small.
  for (GraphNodesLoop l(*nodes); !l.atEnd(); ++l) {
    // Figure out this nodes fanout mask
    GraphNode* node = *l;
    int id = getGraphID(graph, node);
    DynBitVector* fanoutMask = getFanoutMask(closure, graph, node);

    // Check if it belongs in any of the partitions
    bool added = false;
    for (PartitionsLoop p(partitions); !p.atEnd() && !added; ++p) {
      DCLPartition* partition = *p;
      added = partition->addNode(node, id, fanoutMask);
    }

    // If we didn't add it to a partition, create a new one. Since it
    // is new, we'd better add it.
    if (!added) {
      DCLPartition* partition = new DCLPartition(numIDs());
      added = partition->addNode(node, id, fanoutMask);
      SCHED_ASSERT(added, getDCLSchedule(graph, node));
      partitions.push_back(partition);
    }
  }

  // Merge all the partitions with more than one node
  for (PartitionsLoop p(partitions); !p.atEnd(); ++p) {
    DCLPartition* partition = *p;
    if (partition->size() > 1) {
      mergeNodes(graph, partition->loopNodes());
    }
    delete partition;
  } // while
} // SCHDerivedClockSchedules::mergeSubSchedules

void
SCHDerivedClockSchedules::mergeNodes(DCLGraph* graph, GraphNodesLoop l)
{
  // The first entry is the schedule we will merge into. Loop the
  // nodes and do the merge.
  SCHDerivedClockBase* mergeDclBase = NULL;
  for (; !l.atEnd(); ++l) {
    // Get the schedule for this node
    GraphNode* node = *l;
    SCHDerivedClockBase* dclBase = getDCLSchedule(graph, node);

    // For the first time, just set the merge Dcl. For others, merge
    // the other clocks in.
    if (mergeDclBase == NULL) {
      mergeDclBase = dclBase;
    } else {
      // Move the schedule in our database. This is done before the
      // actual merge because it needs the set of clock dclBase
      // defines. And those clocks get moved as part of the merge
      // process below.
      moveDerivedClock(dclBase, mergeDclBase);

      // Merge it with the other one
      mergeDclBase->mergeSchedule(dclBase);
      putDCLSchedule(graph, node, mergeDclBase);

      // We don't need the schedule anymore
      delete dclBase;
    }
  }
} // SCHDerivedClockSchedules::mergeNodes

SCHDerivedClockSchedules::DCLGraphCleanup::DCLGraphCleanup(DCLGraph* srcGraph):
  DCLGraphCleanupBase(srcGraph)
{}

void
SCHDerivedClockSchedules::DCLGraphCleanup::mapNodeData(
  SCHDerivedClockBase* srcGraphDclBase,
  SCHDerivedClockBase** dstGraphDclBase,
  UInt32*)
{
  *dstGraphDclBase = srcGraphDclBase;
}

bool
SCHDerivedClockSchedules::DCLGraphCleanup::mapEdgeData(
  DCLGraph::EdgeDataType srcGraphEdgeData,
  DCLGraph::EdgeDataType* dstGraphEdgeData,
  UInt32*)
{
  *dstGraphEdgeData = srcGraphEdgeData;
  return true;
}

SCHDerivedClockSchedules::DCLGraph*
SCHDerivedClockSchedules::cleanupGraph(DCLGraph* graph)
{
  DCLGraphCleanup cleanup(graph);
  DCLGraph* newGraph = cleanup.transform();
  return newGraph;
}

bool
SCHDerivedClockSchedules::DCLPartition::addNode(GraphNode* node,
                                                int id,
                                                DynBitVector* fanoutMask)
{
  // Check if this node is in the fanout mask of the parition
  if (mFanoutMask.test(id)) {
    return false;
  }

  // Check if this nodes' fanout mask points to any node in this partion
  if (fanoutMask->anyCommonBitsSet(mIDMask)) {
    return false;
  }

  // It can be added, do so
  mMergeGroup.push_back(node);
  mIDMask.set(id);
  mFanoutMask |= *fanoutMask;
  return true;
}

int SCHDerivedClockSchedules::DCLPartition::size()
{
  return mMergeGroup.size();
}

SCHDerivedClockSchedules::GraphNodesLoop
SCHDerivedClockSchedules::DCLPartition::loopNodes()
{
  return GraphNodesLoop(mMergeGroup);
}

SCHDerivedClockSchedules::PrintWalker::PrintWalker(DCLGraph* graph, 
                                                   SCHUtil* util) :
  UtGraphDumper(UtIO::cout()), mGraph(graph), mUtil(util)
{}

SCHDerivedClockSchedules::PrintWalker::~PrintWalker() {}

void
SCHDerivedClockSchedules::PrintWalker::nodeString(Graph*, GraphNode* node,
                                                  UtString* str)
{
  // Get the info about this node
  DCLGraph::Node* dclNode = mGraph->castNode(node);
  SCHDerivedClockBase* dclBase = dclNode->getData();
  const NUNetElab* netElab = dclBase->getRepresentativeClock();

  // Add to the string for this node
  str->clear();
  netElab->compose(str, NULL);
} // SCHDerivedClockSchedules::PrintWalker::nodeString

void SCHDerivedClockSchedules::PrintWalker::edgeString(Graph*, GraphNode*,
                                                       GraphEdge* edge,
                                                       UtString* str)
{
  // Get the edge type
  DCLGraph::Edge* dclEdge = mGraph->castEdge(edge);
  EdgeType type = dclEdge->getData();

  // Return the appropriate character
  *str = SCHDerivedClockSchedules::getEdgeString(type);
} // void SCHDerivedClockSchedules::PrintWalker::edgeString

void SCHDerivedClockSchedules::computeInputNets()
{
  // Compute the input nets for all combinational schedules within
  // the DCL schedules.
  for (SchedMapLoop l(mSchedules); !l.atEnd(); ++l) {
    SCHDerivedClockBase* dclBase = l.getValue();
    SCHDerivedClockLogic* dcl = dclBase->castDCL();
    if (dcl != NULL) {
      findInputNets(dcl);
    } else {
      SCHDerivedClockCycle* dclCycle = dclBase->castDCLCycle();
      SCHED_ASSERT(dclCycle != NULL, dclBase);
      SCHDerivedClockCycle::SubSchedulesLoop s;
      for (s = dclCycle->loopSubSchedules(); !s.atEnd(); ++s) {
        SCHDerivedClockLogic* dcl = *s;
        findInputNets(dcl);
      }
    }
  }

  // Compute the summary DCL input nets for any cycle schedules
  for (SchedMapLoop l(mSchedules); !l.atEnd(); ++l) {
    SCHDerivedClockBase* dclBase = l.getValue();
    SCHDerivedClockCycle* dclCycle = dclBase->castDCLCycle();
    if (dclCycle != NULL) {
      const SCHInputNets* inputNets = NULL;
      SCHCombinationals combs;
      dclBase->getCombinationalSchedules(&combs);
      for (SCHCombinationalsLoop l(combs); !l.atEnd(); ++l) {
        SCHCombinational* comb = *l;
        const SCHInputNets* thisInputNets = comb->getAsyncInputNets();
        inputNets = mMarkDesign->appendInputNets(inputNets, thisInputNets);
      }
      dclCycle->putDCLInputNets(inputNets);
    }
  }
} // SCHDerivedClockSchedules::computeInputNets

void SCHDerivedClockSchedules::findInputNets(SCHDerivedClockLogic* dcl)
{
  // Gather up the input nets
  const SCHInputNets* inputNets = NULL;
  for (SCHUtil::DerivedClockFlowsLoop l = mUtil->loopDerivedClockFlows(dcl);
       !l.atEnd(); ++l) {
    FLNodeElab* flow = *l;
    if (!SCHUtil::isSequential(flow)) {
      const SCHInputNets* newInputNets;
      newInputNets = mMarkDesign->getFlowInputNets(flow);
      if (inputNets == NULL)
        inputNets = newInputNets;
      else if (newInputNets != NULL)
        inputNets = mMarkDesign->appendInputNets(inputNets, newInputNets);
    }
  }

  // Store the information
  dcl->getCombinationalSchedule()->putAsyncInputNets(inputNets);
}

SCHDerivedClockSchedules::FindFlowCallback::FindFlowCallback(
  FLNodeElabSet* elabFlows) : mElabFlows(elabFlows)
{}

SCHDerivedClockSchedules::FindFlowCallback::~FindFlowCallback()
{}

void SCHDerivedClockSchedules::FindFlowCallback::visit(FLNodeElab* flowElab)
{
  mElabFlows->insert(flowElab);
}

bool 
SCHDerivedClockSchedules::FindFlowCallback::isCovered(FLNodeElab* flowElab)
  const
{
  return mElabFlows->find(flowElab) != mElabFlows->end();
}

bool SCHDerivedClockSchedules::FindFlowCallback::visitFullSeqBlock(void) const
{
  return false;
}

SCHDerivedClockSchedules::MarkFlowCallback::MarkFlowCallback()
{}

SCHDerivedClockSchedules::MarkFlowCallback::~MarkFlowCallback()
{}

void SCHDerivedClockSchedules::MarkFlowCallback::visit(FLNodeElab* flowElab)
{
  // We are visited for clocks and the logic that drives it. We only
  // mark the logic that drives it.
  if (!flowElab->isClock()) {
    // Mark this as part of the derived clock logic.
    flowElab->putIsDerivedClockLogic(true);
  }
}

bool
SCHDerivedClockSchedules::MarkFlowCallback::isCovered(FLNodeElab* flowElab)
  const
{
  return flowElab->isDerivedClockLogic();
}

bool SCHDerivedClockSchedules::MarkFlowCallback::visitFullSeqBlock(void) const
{
  return true;
}

bool
SCHDerivedClockSchedules::splitDerivedClockLogic(
  const FLNodeElabSet& flowElabs)
{
  // If we weren't asked to split the blocks, then don't do so
  if (!mUtil->isFlagSet(eSplitBlocks)) {
    return true;
  }

  // Organize the elaborated flow by block instances
  typedef UtMap<SCHUseDefHierPair, FLNodeElabVector> BlockInstances;
  typedef CLoop<const FLNodeElabSet> FLNodeElabConstLoop;
  BlockInstances blockInstances;
  for (FLNodeElabConstLoop l(flowElabs); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    flowElab = mUtil->getCycleOriginalNode(flowElab);
    SCHUseDefHierPair udhp(flowElab);
    FLNodeElabVector& nodes = blockInstances[udhp];
    nodes.push_back(flowElab);
  }

  // Create a block split class
  SCHBlockSplit blockSplit(mUtil, mMarkDesign, mScheduleData);

  // Create a class to get all elaborated flow for a block instances
  SCHBlockFlowNodes blockFlowNodes(mUtil);

  // Go through the block instances and create instructions for each
  // instance.
  typedef LoopMap<BlockInstances> BlockInstancesLoop;
  for (BlockInstancesLoop l(blockInstances); !l.atEnd(); ++l) {
    // Mark all the elaborated flow as in our set
    const FLNodeElabVector& dclGroup = l.getValue();
    FLNodeElab* flowElab = NULL;
    for (FLNodeElabVectorCLoop f(dclGroup); !f.atEnd(); ++f) {
      flowElab = *f;
      flowElab->setScratchFlags(DCL_IN_GROUP);
    }

    // Get all the elaborated flow for this block instance and see if
    // there are any not in our DCL group. These would need to get
    // split out.
    FLNodeElabVector nodes;
    FLNodeElabVector otherGroup;
    INFO_ASSERT(flowElab != NULL,
                "Failed to find a representative flow to a DCL schedule");
    blockFlowNodes.getFlows(flowElab, &nodes, &FLNodeElab::isLive);
    for (FLNodeElabVectorCLoop f(nodes); !f.atEnd(); ++f) {
      flowElab = *f;
      if (!flowElab->anyScratchFlagsSet(DCL_IN_GROUP)) {
        otherGroup.push_back(flowElab);
      }
    }

    // If the other group is not empty, we have to do a split
    if (!otherGroup.empty()) {
      SCHBlockSplit::BlockInstanceGroups blockInstanceGroups;
      blockInstanceGroups.push_back(dclGroup);
      blockInstanceGroups.push_back(otherGroup);
      blockSplit.markSplitBlock(&blockInstanceGroups);
    }

    // Clear the DCL_IN_GROUP flags
    for (FLNodeElabVectorCLoop f(dclGroup); !f.atEnd(); ++f) {
      flowElab = *f;
      flowElab->clearScratchFlags(DCL_IN_GROUP);
    }
  } // for

  // Ready to split the blocks
  if (mUtil->isFlagSet(eDumpSplitBlocks)) {
    UtIO::cout() << "Splitting derived clock logic blocks:\n";
  }
  FLNodeElabSet newFlows;
  bool ok = blockSplit.splitBlocks(&newFlows);
  if (!newFlows.empty()) {
    FLNodeElab* flowElab = *(newFlows.begin());
    FLN_ELAB_ASSERT(newFlows.empty(), flowElab);
  }
  return ok;
} // SCHDerivedClockSchedules::splitDerivedClockLogic

void SCHDerivedClockSchedules::scheduleDesignDerivedNodes(void)
{
  // Create the map to get all the flow nodes from an always block
  SCHBlockFlowNodes blockFlowNodes(mUtil);

  SCHSchedule::DerivedClockLogicLoop p;
  for (p = mScheduleData->loopDerivedClockLogic(); !p.atEnd(); ++p) {
    SCHDerivedClockBase* dclBase = *p;
    SCHDerivedClockLogic* dcl = dclBase->castDCL();
    if (dcl != NULL) {
      scheduleDerivedNodes(dcl, blockFlowNodes);
    } else {
      SCHDerivedClockCycle* dclCycle = dclBase->castDCLCycle();
      SCHDerivedClockCycle::SubSchedulesLoop s;
      for (s = dclCycle->loopSubSchedules(); !s.atEnd(); ++s) {
        SCHDerivedClockLogic* dcl = *s;
        scheduleDerivedNodes(dcl, blockFlowNodes);
      }

      // Now that we have scheduled everything and the schedule mask
      // for the individual DCL schedules is stable, we can compute
      // the mask for the Cycle of schedules.
      const SCHScheduleMask* mask = NULL;
      for (s = dclCycle->loopSubSchedules(); !s.atEnd(); ++s) {
        SCHDerivedClockLogic* dcl = *s;
        mask = mScheduleFactory->appendMasks(mask, dcl->getMask());
      }
      dclCycle->setMask(mask);
    }
  } // for
} // void SCHDerivedClockSchedules::scheduleDesignDerivedNodes

void SCHDerivedClockSchedules::findCycleBreakNets(void)
{
  SCHSchedule::DerivedClockLogicLoop p;
  for (p = mScheduleData->loopDerivedClockLogic(); !p.atEnd(); ++p) {
    SCHDerivedClockBase* dclBase = *p;
    SCHDerivedClockCycle* dclCycle = dclBase->castDCLCycle();
    if (dclCycle != NULL) {
      // Gather all the DCL's in this cycle
      DCLSet dclSet;
      SCHDerivedClockCycle::SubSchedulesLoop s;
      for (s = dclCycle->loopSubSchedules(); !s.atEnd(); ++s) {
        SCHDerivedClockLogic* dcl = *s;
        dclSet.insert(dcl);
      }

      for (s = dclCycle->loopSubSchedules(); !s.atEnd(); ++s) {
        SCHDerivedClockLogic* dcl = *s;
        findBreakNets(dclCycle, dcl, dclSet);
      }
    }
  }
} // void SCHDerivedClockSchedules::findCycleBreakNets

void SCHDerivedClockSchedules::findBreakNets(SCHDerivedClockCycle* dclCycle,
                                             SCHDerivedClockLogic* dcl,
                                             const DCLSet& dclSet)
{
  // Get this schedules depth. All nets computed by a different
  // schedule with an equal or higher depth will have a stale value
  // when read.
  int thisDepth = dcl->getDepth();

  // Check if any of the clocks this schedule uses are computed late
  const SCHScheduleMask* mask = dcl->getMask();
  SCHScheduleMask::UnsortedEvents l;
  for (l = mask->loopEvents(); !l.atEnd(); ++l) {
    const SCHEvent* event = *l;
    if (event->isClockEvent()) {
      const NUNetElab* clkElab = mUtil->getClock(event->getClock());
      SCHUtil::ConstDriverLoop d;
      for (d = mUtil->loopNetDrivers(clkElab); !d.atEnd(); ++d) {
        FLNodeElab* flowElab = *d;
        findBreakNet(dclCycle, thisDepth, flowElab, dclSet);
      }
    }
  }

  // Find any derived clock logic that is computed after it is
  // used. This is logic that crosses DCL boundaries.
  for (SCHIterator i = dcl->getSchedule(); !i.atEnd(); ++i) {
    // Visit the fanin looking for boundary flow
    FLNodeElab* flowElab = *i;
    for (Fanin f = mUtil->loopFanin(flowElab); !f.atEnd(); ++f) {
      // Only do this for elaborated flow not in this schedule
      FLNodeElab* faninElab = *f;
      SCHDerivedClockLogic* faninDCL = findFlowSchedule(faninElab);
      if (faninDCL != dcl) {
        findBreakNet(dclCycle, thisDepth, faninElab, dclSet);
      }
    }
  }
} // void SCHDerivedClockSchedules::findBreakNets

void SCHDerivedClockSchedules::findBreakNet(SCHDerivedClockCycle* dclCycle,
                                            int thisDepth,
                                            FLNodeElab* flowElab,
                                            const DCLSet& dclSet)
{
  // Get the depth for this elaborated flow. Make sure the DCL is in
  // our cycle.
  SCHDerivedClockLogic* dcl = findFlowSchedule(flowElab);
  if ((dcl != NULL) && (dclSet.find(dcl) != dclSet.end())) {
    int faninDepth = dcl->getDepth();
    if (faninDepth >= thisDepth) {
      // This fanin is computed after this DCL schedule executes
      const NUNetElab* netElab = flowElab->getDefNet();
      if (flowElab->isClock()) {
        dclCycle->addBreakClock(netElab);
      } else {
        dclCycle->addBreakNet(netElab);
        if (netElab->getNet()->is2DAnything()) {
          mUtil->getMsgContext()->SchMemoryBreakNet(netElab);
        }
      }
    }
  }
}
