// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implements a class to do fast lookups of all flow for an always block instance
*/

#include "nucleus/NUUseDefNode.h"

#include "flow/FLFactory.h"
#include "flow/FLNodeElab.h"

#include "BlockFlowNodes.h"
#include "Util.h"


SCHBlockFlowNodes::SCHBlockFlowNodes(SCHUtil* util) :
  mUtil(util)
{
  // Allocate space for the map
  mBlockFlows = new BlockFlows;
  init(util->getFlowElabFactory());
} // SCHBlockFlowNodes::SCHBlockFlowNodes

void SCHBlockFlowNodes::init(FLNodeElabFactory* factory)
{
  // Populate the map from the flow factory
  FLNodeElab* flowElab = NULL;
  for (FLNodeElabFactory::FlowLoop p = factory->loopFlows(); p(&flowElab); ) {
    // Only add this flow to the map if:
    // 1. It is not in a cycle (use encapsulated cycle instead)
    // 2. It is either a bound node, encapsualted cycle or continous
    //    driver (always block, cont assign, enabled driver, etc.)
    NUUseDefNode* useDef = flowElab->getUseDefNode();
    if (!mUtil->isInCycle(flowElab) &&
        (flowElab->isBoundNode() || flowElab->isEncapsulatedCycle() ||
         ((useDef != NULL) && useDef->isContDriver()))) {
      // Find the place to add it
      Key key(flowElab);
      FLNodeElabVector* flowElabs;
      BlockFlows::iterator pos = mBlockFlows->find(key);
      if (pos == mBlockFlows->end()) {
        // Allocate a new vector
        flowElabs = new FLNodeElabVector;
        mBlockFlows->insert(BlockFlows::value_type(key, flowElabs));
      } else {
        flowElabs = pos->second;
      }

      // Add the flow elab. We know it is not a duplicate because the
      // factory only visits each elaborated flow once.
      flowElabs->push_back(flowElab);
    }
  }
} // void SCHBlockFlowNodes::init

SCHBlockFlowNodes::~SCHBlockFlowNodes()
{
  for (LoopMap<BlockFlows> l(*mBlockFlows); !l.atEnd(); ++l) {
    FLNodeElabVector* flowElabs = l.getValue();
    delete flowElabs;
  }
  delete mBlockFlows;
}

void SCHBlockFlowNodes::getFlows(FLNodeElab* flowElab, FLNodeElabVector* nodes,
                                 bool (FLNodeElab::*keepTest)() const)
  const
{
  Key key(flowElab);
  FLNodeElabVector* flowElabs = (*mBlockFlows)[key];
  if (flowElabs != NULL) {
    for (FLNodeElabVectorLoop l(*flowElabs); !l.atEnd(); ++l) {
      FLNodeElab* flowElab = *l;
      if ((flowElab->*keepTest)()) {
        nodes->push_back(flowElab);
      }
    }
  }
}

SCHBlockFlowNodes::Key::Key(FLNodeElab* flowElab) : mFlowElab(flowElab) {}

SCHBlockFlowNodes::Key::~Key() {}

int SCHBlockFlowNodes::Key::compare(const Key& key1, const Key& key2)
{
  // Test for encapsulated cycles and bound nodes first
  FLNodeElab* flowElab1 = key1.mFlowElab;
  FLNodeElab* flowElab2 = key2.mFlowElab;
  bool singleton1 = flowElab1->isEncapsulatedCycle() || flowElab1->isBoundNode();
  bool singleton2 = flowElab2->isEncapsulatedCycle() || flowElab2->isBoundNode();
  int cmp = 0;
  if (singleton1 && !singleton2) {
    cmp = -1;
  } else if (!singleton1 && singleton2) {
    cmp = 1;
  } else {
    // They are both the same, compare based on type
    if (singleton1) {
      cmp = FLNodeElab::compare(flowElab1, flowElab2);
    } else {
      SCHUseDefHierPair pair1(flowElab1);
      SCHUseDefHierPair pair2(flowElab2);
      cmp = SCHUseDefHierPair::compare(pair1, pair2);
    }
  }
  return cmp;
}
