// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/CbuildMsgContext.h"
#include "util/UtIOStream.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCycle.h"
#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"
#include "schedule/Schedule.h"

//#ifdef CDB
//#define DEBUG_READY
//#include "util/StringAtom.h"
//#endif

#include "Util.h"
#include "Dump.h"
#include "MarkDesign.h"
#include "Ready.h"

// Set of scratch flags
#define IN_SCHEDULE_SET		(1<<0)

/*!
  \file
  Implementation for a class to schedule flow nodes using a ready queue
*/

void SCHReady::addFlowNode(FLNodeElab* inFlow)
{
  // use the cycle representative flow. But since we are going to use
  // one elab flow for all cycle elab flows, we have to prune
  // duplicates.
  FLNodeElab* flow = getRepresentativeFlow(inFlow);
  if (flow->isEncapsulatedCycle()) {
    if (mFanoutCountMap->find(flow) != mFanoutCountMap->end()) {
      return;
    }
  }

  const NUUseDefNode* useDef = flow->getUseDefNode();
  const HierName* hierName = flow->getHier();

  // Create the fanout map entry and mark it in this group
  FLN_ELAB_ASSERT(mFanoutCountMap->find(flow) == mFanoutCountMap->end(), flow);
  (*mFanoutCountMap)[flow] = 0;
  FLN_ELAB_ASSERT(mRemainingFanoutCountMap->find(flow) ==
                  mRemainingFanoutCountMap->end(), flow);
  (*mRemainingFanoutCountMap)[flow] = 0;
  FLN_ELAB_ASSERT(!flow->anyScratchFlagsSet(IN_SCHEDULE_SET), flow);
  flow->setScratchFlags(IN_SCHEDULE_SET);

  // Create/update the use def/hierarchy map entry
  SCHUseDefHierPair useDefHierPair(useDef, hierName);
  UseDefHierMap::iterator pos = mUseDefHierMap->find(useDefHierPair);
  if (pos == mUseDefHierMap->end())
  {
    // First time in
    UseDefInfo* useDefInfo = new UseDefInfo;
    useDefInfo->queueStatus = eInNone;
    useDefInfo->count = 1;
    useDefInfo->totalCount = 1;
    useDefInfo->scheduleCount = 0;
    useDefInfo->fanoutCount = 0;

    // Count the number of defs for weight. Be careful because cycles
    // don't have defs.
    if (!useDef->isCycle())
    {
      NUNetSet defs;
      useDef->getDefs(&defs);
      useDefInfo->defCount = defs.size();
    }
    else
      useDefInfo->defCount = 0;

    // Add this entry
    mUseDefHierMap->insert(UseDefHierMap::value_type(useDefHierPair,
						     useDefInfo));
  }
  else
  {
    UseDefInfo* useDefInfo = pos->second;
    ++useDefInfo->count;
#if ! (pfGCC_3_3 || pfGCC_3_4 || pfGCC_4)
    // gcc 3.3 rightly says this is always true due to the bit
    // chunking.
    NU_ASSERT(useDefInfo->totalCount < (1 << 16), useDef);
#endif
    useDefInfo->totalCount = useDefInfo->totalCount + 1; // silence gcc 2.95.3
  }
} // void SCHReady::addFlowNode

inline SCHReady::UseDefInfo*
SCHReady::getUseDefHierEntry(FLNodeElab* flow)
{
  const NUUseDefNode* useDef = flow->getUseDefNode();
  const HierName* hierName = flow->getHier();

  // Get the use def information so we can figure out where to put this node
  UseDefHierMap::iterator pos;
  pos = mUseDefHierMap->find(SCHUseDefHierPair(useDef, hierName));
  FLN_ELAB_ASSERT(pos != mUseDefHierMap->end(), flow);
  return pos->second;
}

void
SCHReady::findCycles(FLNodeElab* flow, FLNodeElabSet& covered,
		     FLNodeElabSet& seen, FLNodeElabVector& nodeStack)
{
  // Check if we are done
  if (covered.find(flow) != covered.end())
    return;

  // Check if we have a cycle
  if (seen.find(flow) != seen.end())
  {
    UtIO::cout() << "Cycle found:\n";
    bool endFound = false;
    for (FLNodeElabVector::reverse_iterator p = nodeStack.rbegin();
	 p != nodeStack.rend() && !endFound; ++p)
    {
      FLNodeElab* curFlow = *p;
      UtString flowName;
      mDump->composeFlowName(curFlow, &flowName);
      UtIO::cout() << "  " << flowName << "\n";
      endFound = (curFlow == flow);
    }
    return;
  }

  // Keep visiting
  seen.insert(flow);
  nodeStack.push_back(flow);
  SCHMarkDesign::FlowDependencyLoop p;
  for (p = mMarkDesign->loopDependencies(flow); !p.atEnd(); ++p)
  {
    FLNodeElab* fanin = getRepresentativeFlow(*p);
    if (fanin->anyScratchFlagsSet(IN_SCHEDULE_SET)) {
      findCycles(fanin, covered, seen, nodeStack);
    }
  }
  seen.erase(flow);
  nodeStack.pop_back();
  covered.insert(flow);
}
			  
void SCHReady::initiateSchedule(MsgContext* mc)
{
#ifdef DEBUG_READY
  UtIO::cout() << "Starting a new schedule in ready queue:\n";
#endif

  // Go through the list of flow nodes so that we can compute their fanout
  for (FanoutLoop p = getFanoutNodes(); !p.atEnd(); ++p)
  {
    FLNodeElab* flow = p.getKey();
#ifdef DEBUG_READY
    UtString flowName;
    mDump->composeFlowName(flow, &flowName);
    UtIO::cout() << flowName << ", fanin:\n";
#endif

    SCHMarkDesign::FlowDependencyLoop q;
    for (q = mMarkDesign->loopDependencies(flow); !q.atEnd(); ++q)
    {
      FLNodeElab* fanin = getRepresentativeFlow(*q);

      if (fanin->anyScratchFlagsSet(IN_SCHEDULE_SET))
      {
        ++(*mFanoutCountMap)[fanin];
        FanoutCountMap::iterator q = mRemainingFanoutCountMap->find(fanin);
        FLN_ELAB_ASSERT(q != mRemainingFanoutCountMap->end(), fanin);
        ++(q->second);

#ifdef DEBUG_READY
        UtString faninName;
        mDump->composeFlowName(fanin, &faninName);
        UtIO::cout() << "  " << faninName << "\n";
#endif
      }
      if (SCHUtil::sameBlock(flow, fanin))
      {
        mc->MultiCallBlock(flow);
        mc->MultiCallBlock(fanin);
      }
    } // for
  } // for

  // Make sure there are no cycles
  FLNodeElabSet covered;
  FLNodeElabSet seen;
  FLNodeElabVector nodeStack;
  for (FanoutLoop p = getFanoutNodes(); !p.atEnd(); ++p)
  {
    FLNodeElab* flow = p.getKey();
    findCycles(flow, covered, seen, nodeStack);
  }

  // For through the list of flow nodes that have zero fanout and add
  // them to the correct queue. We only do this once to initiate the
  // scheduling process. After this we will automatically find flow
  // nodes that are ready to schedule when we decrement their fanout
  // count.
  for (FanoutLoop p = getFanoutNodes(); !p.atEnd(); ++p)
  {
    int fanoutCount = p.getValue();
    if (fanoutCount == 0)
    {
      FLNodeElab* flow = p.getKey();
      addToReadyQueue(flow);
    }
  }
} // void SCHReady::initiateSchedule

void SCHReady::addToReadyQueue(FLNodeElab* flow)
{
  UseDefInfo* useDefHierInfo = getUseDefHierEntry(flow);

  const NUUseDefNode* useDef = flow->getUseDefNode();
  FLN_ELAB_ASSERT(useDefHierInfo->count > 0, flow);

  // Add the new flow node to the correct place
  FLNodeElabVector* nodes = NULL;
  switch (useDefHierInfo->queueStatus)
  {
  case eInNone:
  {
    // First of its kind, create it
    nodes = new FLNodeElabVector;
    nodes->push_back(flow);
    break;
  } // case eInNone:

  case eInIncompleteQueue:
  {
    // It is in the incomplete queue, remove it from the queue no
    // matter what because we are going to bump the priority if it
    // stays in this queue
    nodes = removeFromIncompleteQueue(flow, useDefHierInfo->count,
				      useDefHierInfo->totalCount,
				      useDefHierInfo->defCount,
				      useDefHierInfo->fanoutCount);
    nodes->push_back(flow);
    break;
  } // case eInIncompleteQueue:

  case eInCompleteQueue:
    // We can't be adding a node to something that is already
    // complete. This is because each of this nodes arrays is the set
    // of flow nodes for a use def/hier pair. And the complete queue
    // has all flow nodes for that pair.
    FLN_ELAB_ASSERT("Should not add new flow node to complete queue!" == NULL,
                    flow);
    break;

  default:
    // Either we have an invalid value or we are in the state
    // eInCompleteQueue. In the latter case we should not have
    // additional flow nodes for the use def/hier pair so we should
    // not get here.
    nodes = NULL;
    FLN_ELAB_ASSERT("The queue status is in an invalid state" == NULL, flow);
    break;
  } // switch

  // Update the fanout count for what is in the ready queue
  useDefHierInfo->fanoutCount += (*mFanoutCountMap)[flow];

  // Figure out which queue to add it to
  if (nodes != NULL)
  {
    if (useDefHierInfo->count == 1)
    {
      // Since the current count is one, we are adding the last node
      // for the block so we can add it to the complete queue.
      const HierName* hierName = flow->getHier();
      addToCompleteQueue(useDef, hierName, nodes);
      useDefHierInfo->queueStatus = eInCompleteQueue;
    } // if
    else
    {
      // Ok, we have to wait for its always block brothers
      addToIncompleteQueue(useDefHierInfo->count - 1,
			   useDefHierInfo->totalCount,
			   useDefHierInfo->defCount,
			   useDefHierInfo->fanoutCount,
			   nodes);
      useDefHierInfo->queueStatus = eInIncompleteQueue;
    }
  } // if

  // Update the use def map
  --useDefHierInfo->count;

  // If we are dumping merge information and we did a partial
  // schedule, dump information about what we are adding into the
  // ready queue
#ifdef DEBUG_READY_PERF
  if (mNumOutstandingBlocks > 0)
  {
    UInt32 total = useDefHierInfo->totalCount;
    UInt32 index = total - useDefHierInfo->count;
    printNode(flow, useDefHierInfo, index, total, "  added: ");
  }
#endif

} // void SCHReady::addToReadyQueue

void
SCHReady::addToIncompleteQueue(UInt32 remainingCount, UInt32 totalCount,
			       UInt32 defCount, UInt32 fanoutCount,
			       FLNodeElabVector* nodes)
{
  NodesSet* nodesSet;

  // Add this group of nodes to the set
  Priority priority(remainingCount, totalCount, defCount, fanoutCount);
  NodesSetPriorityQueue::iterator pos = mReadyIncompleteQueue->find(priority);
  if (pos == mReadyIncompleteQueue->end())
  {
    nodesSet = new NodesSet;
    (*mReadyIncompleteQueue)[priority] = nodesSet;
  }
  else
    nodesSet = pos->second;
  nodesSet->insert(nodes);
}

FLNodeElabVector*
SCHReady::removeFromIncompleteQueue(FLNodeElab* flow, UInt32 remainingCount,
				    UInt32 totalCount, UInt32 defCount,
				    UInt32 fanoutCount)
{
  NodesSet* nodesSet;
  FLNodeElabVector* nodes;

  // Get the nodes set, we may remove it
  FLN_ELAB_ASSERT(remainingCount > 0, flow);
  Priority priority(remainingCount, totalCount, defCount, fanoutCount);
  NodesSetPriorityQueue::iterator queuePos;
  queuePos = mReadyIncompleteQueue->find(priority);
  FLN_ELAB_ASSERT(queuePos != mReadyIncompleteQueue->end(), flow);
  nodesSet = queuePos->second;

  // Find the right nodes and remove it.
  FLNodeElabVector srchNodes;
  srchNodes.push_back(flow);
  NodesSet::iterator pos = nodesSet->find(&srchNodes);
  FLN_ELAB_ASSERT(pos != nodesSet->end(), flow);
  nodes = *pos;
  nodesSet->erase(pos);

  // If the nodes set is empty, clean up
  if (nodesSet->empty())
  {
    delete nodesSet;
    mReadyIncompleteQueue->erase(queuePos);
  }

  return nodes;
} // FLNodeElabVector* SCHReady::removeFromIncompleteQueue

void SCHReady::addToCompleteQueue(const NUUseDefNode* useDef,
				  const HierName* hierName,
				  FLNodeElabVector* nodes)
{
  // Find the hierarchy map for this usedef
  NodesHierMap* nodesHierMap = (*mReadyCompleteQueue)[useDef];
  if (nodesHierMap == NULL)
  {
    // It doesn't exist, create it
    nodesHierMap = new NodesHierMap;
    (*mReadyCompleteQueue)[useDef] = nodesHierMap;
  }

  // At this point we have a place to put it
  NU_ASSERT((*nodesHierMap)[hierName] == NULL, useDef);
  (*nodesHierMap)[hierName] = nodes;
} // void SCHReady::addToCompleteQueue

void
SCHReady::printNode(FLNodeElab* flow, UseDefInfo* useDefInfo,
		    UInt32 index, UInt32 size, const char* prefix)
{
  UtString flowName;
  SCHDump::composeFlowName(flow, &flowName);
  UtIO::cout() << prefix << flowName << " "
       << index << "/" << size << " out of " << useDefInfo->totalCount
       << " outputs scheduled, remain=" << useDefInfo->count
       << " defCount=" << useDefInfo->defCount
       << " fanoutCount=" << useDefInfo->fanoutCount
       << UtIO::endl;
}


void
SCHReady::printNodes(FLNodeElabVector* nodes, UseDefInfo* useDefInfo,
		     UInt32 size, const char* prefix)
{
  UInt32 index = 0;
  for (FLNodeElabVectorIter p = nodes->begin(); p != nodes->end(); ++p)
  {
    FLNodeElab* flow = *p;
    printNode(flow, useDefInfo, ++index, size, prefix);
  }
}

bool
SCHReady::getNodesToSchedule(FLNodeElabVector* returnNodes,
			     bool dumpUnmergedBlocks,
			     UtString* title)
{
  FLNodeElabVector* nodes;

  // First try the complete queue with complete flow nodes
  nodes = removeFromCompleteQueue();

  // If we failed, try the incomplete queue
  if (nodes == NULL)
    // Now this is bad news, we hope this doesn't happen often
    nodes = searchIncompleteQueue();

  // If we got it, return it to our caller
  if (nodes != NULL) {
    // Update statistics
    FLNodeElab* flow = *nodes->begin();
    UseDefInfo* useDefInfo = getUseDefHierEntry(flow);
    useDefInfo->scheduleCount = useDefInfo->scheduleCount + 1; // silence gcc 2.95.3

    // Maybe print out the file and line number of this block if it
    // wasn't completely merged.
    if (dumpUnmergedBlocks)
    {
      UInt32 schedSize = nodes->size();
      if (schedSize < useDefInfo->totalCount)
      {
	if (!title->empty())
	{
	  UtIO::cout() << *title;
	  title->clear();
	}
	printNodes(nodes, useDefInfo, schedSize, "");
#ifdef DEBUG_READY_PERF
	if ((useDefInfo->totalCount - schedSize) == useDefInfo->count)
	  ++mNumOutstandingBlocks;
	else if (useDefInfo->count == 0)
	  --mNumOutstandingBlocks;
#endif
      }
#ifdef DEBUG_READY_PERF
      else if (mNumOutstandingBlocks > 0)
      {
	if (!title->empty())
	{
	  UtIO::cout() << *title;
	  title->clear();
	}
	printNode(*nodes->begin(), useDefInfo, schedSize, schedSize,
		  "  complete: ");
      }
#endif
    }

    // If this is a cycle, we want to resurect all the flow for the
    // cycle. We should not prune the nodes yet. SortCombBlocks will
    // do that. All the nodes are needed to sort them correctly.
    FLN_ELAB_ASSERT(returnNodes->empty(), flow);
    if (flow->isEncapsulatedCycle()) {
      FLN_ELAB_ASSERT(nodes->size() == 1, flow);
      mUtil->getAcyclicFlows(flow, returnNodes);
    } else {
      // Swapping with our caller allows us to delete our memory now
      returnNodes->swap(*nodes);
    }

    // All done with the nodes memory; return that we found a node to
    // schedule.
    delete nodes;
    return true;
  }
  else
    return false;
} // FLNodeElabVector* SCHReady::getNodesToSchedule

void SCHReady::markAsScheduled(FLNodeElab* flow)
{
  // if this is a cycle that we choose not to be the canonical flow,
  // there is nothing to do. Only the chosen flow has to be marked as
  // scheduled.
  if (flow->isEncapsulatedCycle() && (flow != getRepresentativeFlow(flow))) {
    return;
  }

  // Mark this node and remove it from the fanout count maps
  flow->clearScratchFlags(IN_SCHEDULE_SET);
  FanoutCountMap::iterator pos;
  pos = mRemainingFanoutCountMap->find(flow);
  FLN_ELAB_ASSERT(pos->second == 0, flow);
  mRemainingFanoutCountMap->erase(pos);
  pos = mFanoutCountMap->find(flow);
  mFanoutCountMap->erase(pos);

  // Now all this nodes fanin should decrement their fanout counts
  SCHMarkDesign::FlowDependencyLoop p;
  for (p = mMarkDesign->loopDependencies(flow); !p.atEnd(); ++p)
  {
    FLNodeElab* fanin = getRepresentativeFlow(*p);
    decrementFanout(fanin);
  }
}

FLNodeElabVector* SCHReady::removeFromCompleteQueue()
{
  FLNodeElabVector* nodes;

  // If the queue isn't empty, just return any set of nodes. The order
  // doesn't matter.
  if (mReadyCompleteQueue->empty())
    nodes = NULL;
  else
  {
    // Get the first hierachy map
    NodesUseDefMap::iterator posUD = mReadyCompleteQueue->begin();
    INFO_ASSERT(posUD != mReadyCompleteQueue->end(),
                "Empty queue when schedule combinational nodes");
    NodesHierMap* nodesHierMap = posUD->second;

    // Get the first set of nodes in the hierarchy map
    NodesHierMap::iterator posHier = nodesHierMap->begin();
    INFO_ASSERT(posHier != nodesHierMap->end(),
                "Empty item in a combinational schedule queue");
    nodes = posHier->second;

    // Erase the nodes entry since we are done with it. If this makes
    // the hierarchy map empty, then delete that as well. This way the
    // empty test at the top of this function can tell if the queue is
    // empty.
    nodesHierMap->erase(posHier);
    if (nodesHierMap->empty())
    {
      delete nodesHierMap;
      mReadyCompleteQueue->erase(posUD);
    }
  } // else

  return nodes;
} // FLNodeElabVector* SCHReady::searchCompleteQueue

FLNodeElabVector* SCHReady::searchIncompleteQueue()
{
  // Make sure there are nodes
  if (mReadyIncompleteQueue->empty())
    return NULL;
  NodesSet* nodesSet = mReadyIncompleteQueue->begin()->second;
  INFO_ASSERT(!nodesSet->empty(),
              "Empty set of nodes in a combinational schedule queue");

  // We found something valid at this priority
  NodesSet::iterator pos = nodesSet->begin();
  FLNodeElabVector* nodes = *pos;
  nodesSet->erase(pos);

  // Check if we are done with this queue entry
  if (nodesSet->empty())
  {
    delete nodesSet;
    mReadyIncompleteQueue->erase(mReadyIncompleteQueue->begin());
  }

  // This use def/hier pair are no longer in the incomplete queue
  FLNodeElab* flow = nodes->back();
  UseDefInfo* useDefInfo = getUseDefHierEntry(flow);
  FLN_ELAB_ASSERT(useDefInfo->queueStatus == eInIncompleteQueue, flow);
  useDefInfo->queueStatus = eInNone;
  return nodes;
} // FLNodeElabVector* SCHReady::searchIncompleteQueue

void SCHReady::cleanUp()
{
#ifdef DEBUG_READY
  for (FanoutLoop p = getFanoutNodes(); !p.atEnd(); ++p)
  {
    FLNodeElab* flow = p.getKey();
    int count = p.getValue();
    UtString flowName;
    mDump->composeFlowName(flow, &flowName);
    UtIO::cout() << "FCM Entry: " << flowName << ", count = " << count << "\n";
  }
#endif

  // The fanout map should already be empty
  INFO_ASSERT(mRemainingFanoutCountMap->empty(),
              "Scheduling combinational nodes completed without scheduling all nodes");
  INFO_ASSERT(mFanoutCountMap->empty(),
              "Scheduling combinational nodes completed without scheduling all nodes");

  // The use def map is not empty, clean it up.
  for (UseDefHierMap::iterator p = mUseDefHierMap->begin();
       p != mUseDefHierMap->end(); ++p)
    delete p->second;
  mUseDefHierMap->clear();

  // The queues should be empty
  INFO_ASSERT(mReadyIncompleteQueue->empty(),
              "Scheduling combinational nodes completed without scheduling all nodes");
  INFO_ASSERT(mReadyCompleteQueue->empty(),
              "Scheduling combinational nodes completed without scheduling all nodes");
} // void SCHReady::cleanUp

// Constructor
SCHReady::SCHReady(SCHUtil* util, SCHMarkDesign* mark, SCHDump* dump)
{
  mUtil = util;
  mMarkDesign = mark;
  mDump = dump;
#ifdef DEBUG_READY_PERF
  mNumOutstandingBlocks = 0;
#endif
  mUseDefHierMap = new UseDefHierMap;
  mRemainingFanoutCountMap = new FanoutCountMap;
  mFanoutCountMap = new FanoutCountMap;
  mReadyCompleteQueue = new NodesUseDefMap;
  mReadyIncompleteQueue = new NodesSetPriorityQueue;
  mCycleRepresentativeFlow = new CycleRepresentativeFlow;
}


// Destructor has to clean up a little more
SCHReady::~SCHReady()
{
  // Clean up just in case it wasn't done by our caller.
  cleanUp();
  delete mUseDefHierMap;
  delete mRemainingFanoutCountMap;
  delete mFanoutCountMap;
  delete mReadyCompleteQueue;
  delete mReadyIncompleteQueue;
  delete mCycleRepresentativeFlow;
}

void SCHReady::decrementFanout(FLNodeElab* flow)
{
  // If this is in the schedule map, just count it
  if (flow->anyScratchFlagsSet(IN_SCHEDULE_SET))
  {
    FanoutCountMap::iterator p = mRemainingFanoutCountMap->find(flow);
    FLN_ELAB_ASSERT(p != mRemainingFanoutCountMap->end(), flow);
    int newCount = --(p->second);
    if (newCount == 0)
	// This is ready to schedule
      addToReadyQueue(flow);
  }
} // void SCHReady::decrementFanout

SCHReady::FanoutLoop SCHReady::getFanoutNodes()
{
  return FanoutLoop(*mRemainingFanoutCountMap);
}

bool
SCHReady::CmpFlowInstance::operator()(const FLNodeElabVector* ns1,
				      const FLNodeElabVector* ns2) const
{
  INFO_ASSERT(!ns1->empty() && !ns2->empty(),
              "Comparing two empty set of nodes during combinational scheduling");
  const FLNodeElab* flow1 = ns1->back();
  const FLNodeElab* flow2 = ns2->back();
  SCHUseDefHierPair udhp1(flow1);
  SCHUseDefHierPair udhp2(flow2);
  return SCHUseDefHierPair::compare(udhp1, udhp2) < 0;
}

bool
SCHReady::CmpUseDefs::operator()(const NUUseDefNode* ud1,
				 const NUUseDefNode* ud2) const
{
  int cmp = NUUseDefNode::compare(ud1, ud2);
  return (cmp < 0);
}

FLNodeElab* SCHReady::getRepresentativeFlow(FLNodeElab* flowElab)
{
  // If this is not a cycle, just return the one we have
  if (!flowElab->isEncapsulatedCycle()) {
    return flowElab;
  }

  // It is a cycle, check if we already know about this one
  CycleRepresentativeFlow::iterator pos = mCycleRepresentativeFlow->find(flowElab);
  if (pos != mCycleRepresentativeFlow->end()) {
    FLNodeElab* representativeFlowElab = pos->second;
    return representativeFlowElab;
  }

  // We have not processed this cycle before, do so now. Get a
  // canonical elaborated flow to represent it.
  FLNodeElabVector acyclicFlows;
  mUtil->getAcyclicFlows(flowElab, &acyclicFlows);
  FLNodeElab* representativeFlowElab = NULL;
  for (FLNodeElabVectorLoop l(acyclicFlows); !l.atEnd(); ++l) {
    FLNodeElab* curFlow = *l;
    if ((representativeFlowElab == NULL) ||
        (FLNodeElab::compare(curFlow, representativeFlowElab) < 0)) {
      representativeFlowElab = curFlow;
    }
  }

  // Set up the map for all the acyclic flows
  for (FLNodeElabVectorLoop l(acyclicFlows); !l.atEnd(); ++l) {
    // We shouldn't have processed this before
    FLNodeElab* curFlow = *l;
    CycleRepresentativeFlow::iterator p = mCycleRepresentativeFlow->find(curFlow);
    FLN_ELAB_ASSERT(p == mCycleRepresentativeFlow->end(), curFlow);

    // Insert it into the map
    CycleRepresentativeFlow::value_type value(curFlow, representativeFlowElab);
    mCycleRepresentativeFlow->insert(value);
  }

  return representativeFlowElab;
} // FLNodeElab* SCHReady::getRepresentativeFlow
