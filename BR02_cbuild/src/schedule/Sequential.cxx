// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "schedule/ScheduleMask.h"
#include "schedule/Schedule.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUAlwaysBlock.h"

#include "flow/FLNodeElab.h"

#include "Util.h"
#include "MarkDesign.h"
#include "BlockFlowNodes.h"

#include <cstddef>

/*!
  \file
  Implementation of the Sequential class
*/

SCHSequential::SCHSequential(SCHScheduleType type, const NUNetElab* netElab,
			     const SCHScheduleMask* asyncMask,
                             const SCHScheduleMask* extraMask,
			     SCHDerivedClockLogic* parent) :
  SCHScheduleBase(type), mEdgeNet(netElab), mAsyncMask(asyncMask),
  mExtraEdgesMask(extraMask), mOrder(0), mParent(parent)
{
  mSubSchedules[eClockPosedge] = new SCHSequentialEdge(this, eClockPosedge);
  mSubSchedules[eClockNegedge] = new SCHSequentialEdge(this, eClockNegedge);
}

SCHSequential::~SCHSequential()
{
  delete mSubSchedules[eClockPosedge];
  delete mSubSchedules[eClockNegedge];
}

static inline int
compareSequential(const NUNetElab* n1, const NUNetElab* n2,
		  const SCHScheduleMask* am1, const SCHScheduleMask* am2,
                  const SCHScheduleMask* em1, const SCHScheduleMask* em2)
{
  int cmp = NUNetElab::compare(n1, n2);
  if( cmp != 0 )
    return cmp;
    
  if ((am1 != NULL) && (am2 != NULL))
    cmp = SCHScheduleMask::compare(am1, am2);
  else if ((am1 == NULL) && (am2 == NULL))
    cmp = 0;
  else if (am1 == NULL)
    cmp = -1;
  else
    cmp = 1;
  if( cmp != 0 )
    return cmp;
    
  if ((em1 != NULL) && (em2 != NULL))
    cmp = SCHScheduleMask::compare(em1, em2);
  else if ((em1 == NULL) && (em2 == NULL))
    cmp = 0;
  else if (em1 == NULL)
    cmp = -1;
  else
    cmp = 1;
    
  return cmp;
} // compareSequential

typedef Loop<FLNodeElabVector> NodeLoop;

bool
SCHSequentials::SequentialSortFields::operator<(const SequentialSortFields& s)
  const
{
  return compareSequential(mNetElab, s.mNetElab, mAsyncMask, s.mAsyncMask,
                           mExtraMask, s.mExtraMask) < 0;
}

SCHIterator SCHSequential::getSchedule(ClockEdge edge) const
{
  return mSubSchedules[edge]->getSchedule();
}

void SCHSequential::removeDuplicateNodes()
{
  mSubSchedules[eClockPosedge]->removeDuplicateNodes();
  mSubSchedules[eClockNegedge]->removeDuplicateNodes();
}

// Comparison operator so we can sort sequential flow nodes by their depth
// and usedef node. We also sort by their UtString -- see below.
//
// The depth is set such that any sequential nodes that need to be
// double buffered have a depth of 0. Otherwise the depth is set so
// that we use the reverse order. This means a depth of 2 should run
// before a depth of 1. What this means is we have a funny sort
// routine. The order of the depths should be 0->4->3->2->1
static bool lessSequentialFlowDepth(FLNodeElab* flow1, FLNodeElab* flow2)
{
  int depth1 = flow1->getDepth();
  int depth2 = flow2->getDepth();

  // Sort by depth first, then by their use def node and finally by
  // name. The sort by name is used to make regression tests more
  // consistent. If this becomes a performance problem we can try to
  // do something else to make the tests consistent.
  if (depth1 == depth2)
    return FLNodeElab::compare(flow1, flow2) < 0;
  else if (depth1 == 0)
    return true;
  else if (depth2 == 0)
    return false;
  else
    // We want higher depths to go first.
    return (depth2 < depth1);
}

void SCHSequential::sortNodes()
{
  mSubSchedules[eClockPosedge]->sortNodes();
  mSubSchedules[eClockNegedge]->sortNodes();
}

void SCHSequential::addSequentialNode(FLNodeElab* node, ClockEdge edge)
{
  // We should not add more nodes if we are done creating the vector list
  mSubSchedules[edge]->addSequentialNode(node);
}


int SCHSequential::compareSchedules(const SCHScheduleBase* schedBase) const
{
  // The other schedule must be sequential
  const SCHSequential* other = dynamic_cast<const SCHSequential*>(schedBase);
  SCHED_ASSERT(other != NULL, this);

  // Check for the easy case
  if (this == other)
    return 0;

  // First compare by sequential type
  int cmp = getScheduleType() - other->getScheduleType();

  // Next compare by clock net
  if (cmp == 0) {
    cmp = NUElabBase::compare(mEdgeNet, other->getEdgeNet());

    // Next compare by async mask
    if (cmp == 0) {
      if ((getAsyncMask() != NULL) && (other->getAsyncMask() != NULL))
	// Both have async masks
	cmp = SCHScheduleMask::compare(getAsyncMask(), other->getAsyncMask());
      else if (getAsyncMask() != NULL)
	cmp = -1;
      else if (other->getAsyncMask() != NULL)
	cmp = 1;

      // If they have the same async mask, compare the edge masks
      if (cmp == 0)
      {
        const SCHScheduleMask* extraMask1 = getExtraEdgesMask();
        const SCHScheduleMask* extraMask2 = other->getExtraEdgesMask();
        if ((extraMask1 != NULL) && (extraMask2 != NULL))
          cmp = SCHScheduleMask::compare(extraMask1, extraMask2);
        else if (extraMask1 != NULL)
          cmp = -1;
        else if (extraMask2 != NULL)
          cmp = 1;

        // And finally if they have the same asynch/extra masks then
        // they must be part of different derived schedules.
        if (cmp == 0)
        {
          SCHED_ASSERT(getScheduleType() == eSequentialDCL, this);
          SCHED_ASSERT(other->getScheduleType() == eSequentialDCL, other);
          SCHDerivedClockLogic* dcl1 = getParentSchedule();
          SCHDerivedClockLogic* dcl2 = other->getParentSchedule();
          cmp = dcl1->compareSchedules(dcl2);
        }
      } // if
    } // if
  } // if
  SCHED_ASSERT(cmp != 0, this);
  return cmp;
}

SCHSequential*
SCHSequentials::findSequential(const NUNetElab* clk,
			       const SCHScheduleMask* asyncMask,
                               const SCHScheduleMask* extraMask)
{
  const SequentialSortFields srch(clk, asyncMask, extraMask);
  SequentialMap::iterator pos = mMap->find(srch);
  if (pos != mMap->end())
    return pos->second;
  else
    return NULL;
}

SCHSequential*
SCHSequentials::addSequential(const NUNetElab* clk,
			      const SCHScheduleMask* asyncMask,
                              const SCHScheduleMask* extraMask,
			      SCHDerivedClockLogic* parent)
{
  SCHSequential* sc = findSequential(clk, asyncMask, extraMask);
  if (sc == NULL)
  {
    sc = new SCHSequential(mScheduleType, clk, asyncMask, extraMask, parent);
    const SequentialSortFields srch(clk, asyncMask, extraMask);
    (*mMap)[srch] = sc;
    mVector->push_back(sc);
  }
  return sc;
}

void
SCHSequentials::scheduleSequentialNode(FLNodeElab* flow,
				       NUUseDefNode* driver,
				       SCHDerivedClockLogic* parent,
                                       const SCHScheduleMask* extraMask,
                                       SCHMarkDesign* markDesign)
{
  // Get the unique clock for this sequential block
  ClockEdge edge;
  const NUNetElab* clkElab;
  const SCHScheduleMask* mask;
  markDesign->getSequentialNodeClockAndMask(flow, driver, &edge, &clkElab, &mask);

  // Find the schedule and add the block.
  SCHSequential* clkSchedule;
  clkSchedule = addSequential(clkElab, mask, extraMask, parent);
  clkSchedule->addSequentialNode(flow, edge);

  // Make sure we aren't scheduling a sequential with an unknown
  // clock. It is run after adding the flop so that we print it if
  // possible
  if (!markDesign->isValidClock(clkElab)) {
    UtIO::cout() << "Invalid clock when adding a flop\n";
    markDesign->analyzeInvalidScheduleClk(clkSchedule, clkElab);
  }
}

void SCHSequentials::sortNodes()
{
  for (SequentialMap::iterator s = mMap->begin(); s != mMap->end(); ++s)
  {
    SCHSequential* seq = s->second;
    seq->sortNodes();
  }
}


SCHSequentials::SCHSequentials(SCHScheduleType type, SCHUtil* util)
{
  UInt32 typeMask = (1 << type);
  INFO_ASSERT((typeMask & eMaskAllSequentials) == typeMask,
              "Creating invalid sequential schedule type");
  mScheduleType = type;
  mUtil = util;
  mMap = new SequentialMap;
  mVector = new SequentialVector;
  mSUNets = new SUNets;
}

SCHSequentials::~SCHSequentials()
{
  for (SequentialMap::iterator s = mMap->begin(); s != mMap->end(); ++s)
  {
    SCHSequential* seq = s->second;
    delete seq;
  }
  mMap->clear();
  mVector->clear();
  delete mMap;
  delete mVector;
  delete mSUNets;
}

void SCHSequentials::findSUNets(SCHIterator i, NUNetElabSet* coveredNets,
                                NUNetElabSet* coveredSUNets,
                                const SCHBlockFlowNodes& blockFlowNodes)
{
  // Walk all the flops
  for (; !i.atEnd(); ++i) {
    // Gather all the flow nodes in this block instance
    FLNodeElab* flow = *i;
    FLNodeElabVector nodes;
    blockFlowNodes.getFlows(flow, &nodes, &FLNodeElab::isLive);

    // Check if this net is a state update
    for (FLNodeElabVectorCLoop i(nodes); !i.atEnd(); ++i) {
      // Make sure we haven't checked this elaborated net
      // before. Elaborated nets can be in multiple schedules because
      // async parts get split off.
      FLNodeElab* curFlow = *i;
      NUNetElab* netElab = curFlow->getDefNet();
      if (coveredNets->find(netElab) == coveredNets->end()) {
        // Gather all the aliases for this elaborated net
        coveredNets->insert(netElab);
        NUNetVector nets;
        netElab->getAliasNets(&nets, false);

        // Check if any of the aliases are double buffered
        bool isDoubleBuffered = false;
        for (NUNetVectorLoop n(nets); !n.atEnd() && !isDoubleBuffered; ++n) {
          NUNet* net = *n;
          isDoubleBuffered = net->isDoubleBuffered();
        }

        // Add it to the set of double buffered nets
        if (isDoubleBuffered &&
            (coveredSUNets->find(netElab) == coveredSUNets->end())) {
          mSUNets->push_back(netElab);
          coveredSUNets->insert(netElab);
        }
      }
    } // for
  } // for
} // void SCHSequentials::findSUNets

SCHSequentials::SULoop SCHSequentials::loopSUNets()
{
  // Return the iterator over the data
  return SULoop(*mSUNets);
} // SCHSequentials::SULoop SCHSequentials::loopSUNets

void SCHSequentials::findSUNets(const SCHBlockFlowNodes& blockFlowNodes)
{
  // Create sets to make sure we don't add a net twice and for
  // performance.
  NUNetElabSet coveredNets;
  NUNetElabSet coveredSUNets;

  // Iterate over all the flow nodes in these schedules
  for (SequentialLoop s = loopSequential(); !s.atEnd(); ++s) {
    // Grab the flows for both edge schedules
    SCHSequential* seq = *s;
    findSUNets(seq->getSchedule(eClockPosedge), &coveredNets, &coveredSUNets,
               blockFlowNodes);
    findSUNets(seq->getSchedule(eClockNegedge), &coveredNets, &coveredSUNets,
               blockFlowNodes);
  }

  // Sort the nets so that we get predictable output
  std::sort (mSUNets->begin(), mSUNets->end(), NUNetElabCmp());
}

void
SCHSequential::removeFlows(const SCHDeleteTest& deleteTest)
{
  mSubSchedules[eClockPosedge]->removeFlows(deleteTest);
  mSubSchedules[eClockNegedge]->removeFlows(deleteTest);
} // SCHSequential::removeFlows

SCHNodeSetLoop SCHSequential::loopNodes(ClockEdge edge)
{
  return mSubSchedules[edge]->loopNodes();
}

bool SCHSequential::empty(ClockEdge edge) const
{
  return mSubSchedules[edge]->empty();
}

UInt32 SCHSequential::size(ClockEdge edge) const
{
  return mSubSchedules[edge]->size();
}

void SCHSequential::print(bool printNets, SCHMarkDesign* mark) const
{
  UtIO::cout() << "Sequential(" << this << ") clock = "
	       << mEdgeNet->getSymNode()->str() << "\n";
  if (printNets) {
    mSubSchedules[eClockPosedge]->printSchedNets(mark);
    mSubSchedules[eClockNegedge]->printSchedNets(mark);
  }
}

void SCHSequential::dump(int indent, SCHMarkDesign* mark) const
{
  UtString asyncBuf;
  const SCHScheduleMask* asyncMask = getAsyncMask();
  if (asyncMask != NULL) {
    asyncBuf = "AsyncMask = (";
    asyncMask->compose(&asyncBuf,NULL);
    asyncBuf += ")";
  }

  UtString extraBuf;
  const SCHScheduleMask* extraMask = getExtraEdgesMask();
  if (extraMask != NULL) {
    extraBuf = "ExtraMask = (";
    extraMask->compose(&extraBuf,NULL);
    extraBuf += ")";
  }

  // Gather the branch nets string
  UtString branchNetsBuf;
  composeBranchNets(&branchNetsBuf, mark);

  if (!empty(eClockPosedge)) {
    dumpSchedule(indent, eClockPosedge, asyncBuf, extraBuf, branchNetsBuf, mark);
  }
  if (!empty(eClockNegedge)) {
    dumpSchedule(indent, eClockNegedge, asyncBuf, extraBuf, branchNetsBuf, mark);
  }
}

void 
SCHSequential::dumpSchedule(int indent, ClockEdge edge, UtString& asyncBuf,
                            UtString& extraBuf, UtString& branchNetsBuf,
                            SCHMarkDesign* mark) const
{
  // Print the header
  dumpIndent(indent);
  UtIO::cout() << "Sequential @(";
  switch (edge) {
    case eClockPosedge:
      UtIO::cout() << "posedge ";
      break;
    case eClockNegedge:
      UtIO::cout() << "negedge ";
      break;
    case eLevelHigh:
      UtIO::cout() << " ";
      break;
    case eLevelLow:
      UtIO::cout() << "not ";
      break;
  }
  UtString buf;
  const NUNetElab* edgeNet = getEdgeNet();
  edgeNet->compose(&buf,NULL);
  UtIO::cout() << buf << ")\n";
  if (!asyncBuf.empty()) {
    UtIO::cout() << "  " << asyncBuf << "\n";
  }
  if (!extraBuf.empty()) {
    UtIO::cout() << "  " << extraBuf << "\n";
  }
  if (!branchNetsBuf.empty()) {
    UtIO::cout() << "  " << branchNetsBuf << "\n";
  }

  // Print each flow node
  int index = 0;
  for (SCHIterator i = getSchedule(edge); !i.atEnd(); ++i) {
    FLNodeElab* flow = *i;
    dumpFlowNode(flow, getScheduleType(), ++index, indent, mark);
  }
} // SCHSequential::dumpSchedule

SCHSequentials::SequentialLoop SCHSequentials::loopSequential()
{
  return SequentialLoop(*mVector);
}

static bool lessSequentialOrder(const SCHSequential* s1,
				const SCHSequential* s2)
{
  SCHED_ASSERT((s1 == s2) || (s1->getOrder() != s2->getOrder()), s1);
  return s1->getOrder() < s2->getOrder();
}

void SCHSequentials::sort()
{
  std::sort(mVector->begin(), mVector->end(), lessSequentialOrder);
}

// Class to remove inactive flow
class SCHInactiveTest : public SCHDeleteTest
{
public:
  SCHInactiveTest() {}
  ~SCHInactiveTest() {}
  bool operator()(FLNodeElab* flow) const { return flow->isInactive(); }
};


void
SCHSequentials::removeInactiveSchedules(void)
{
  // Sequential flows may become inactive. Remove them from the
  // schedule here. If the schedule because empty, remove the
  // schedule.
  SequentialVector newSchedules;
  bool inactivesFound = false;
  SCHInactiveTest inactiveTest;
  for (SequentialLoop l = loopSequential(); !l.atEnd(); ++l) {
    // Visit the flow nodes and remove the inactive ones
    SCHSequential* seq = *l;
    seq->removeFlows(inactiveTest);

    // Check if the schedule is empty. If so we can remove it
    if (seq->empty(eClockPosedge) && seq->empty(eClockNegedge)) {
      inactivesFound = true;
    } else {
      newSchedules.push_back(seq);
    }
  }
  if (inactivesFound) {
    mVector->swap(newSchedules);
  }
}

SCHSequentialEdge::SCHSequentialEdge(SCHSequential* parent, ClockEdge edge) :
  SCHScheduleBase(eSequentialEdge),
  mScheduleCreated(false), mParent(parent), mEdge(edge)
{
  mSchedule = new FLNodeElabVector;
  mScheduleSet = new SCHNodeSet;
}

SCHSequentialEdge::~SCHSequentialEdge()
{
  delete mSchedule;
  delete mScheduleSet;
}

SCHIterator SCHSequentialEdge::getSchedule() const
{
  if (mScheduleCreated) {
    return SCHIterator::create(NodeLoop(*mSchedule));
  } else {
    return SCHIterator::create(SCHNodeSetLoop(*mScheduleSet));
  }
}

void SCHSequentialEdge::removeDuplicateNodes()
{
  // The vector lists should be clear. We are creating them now
  SCHED_ASSERT(mSchedule->empty(), this);

  // Go through the schedule set and insert the nodes back in if they
  // are unique. They may not be unique if we have multiple flow
  // nodes for the same always block instance because of multiple
  // outputs.
  SCHUniqueBlockNodeSet uniqueSet;
  for (SCHNodeSetLoop l(*mScheduleSet); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    if (uniqueSet.find(flowElab) == uniqueSet.end()) {
      // Doesn't exist yet, insert it
      mSchedule->push_back(flowElab);
      uniqueSet.insert(flowElab);
    }
  }

  // Now the schedule is ready for consumption for later schedule
  // passes. So we don't need the sets that help us keep the nodes in
  // the vector unique.
  mScheduleCreated = true;
  mScheduleSet->clear();
} // void SCHSequentialEdge::removeDuplicateNodes


void SCHSequentialEdge::addSequentialNode(FLNodeElab* flowElab)
{
  SCHED_ASSERT(!mScheduleCreated, this);
  mScheduleSet->insert(flowElab);
}

void SCHSequentialEdge::sortNodes()
{
  std::sort(mSchedule->begin(), mSchedule->end(), lessSequentialFlowDepth);
}

void SCHSequentialEdge::removeFlows(const SCHDeleteTest& deleteTest)
{
  // Check where the sequential nodes are stored
  if (mScheduleCreated) {
    // It's in the final storage, look for them there
    FLNodeElabVector newFlows;
    bool removedFlows = false;
    for (FLNodeElabVectorLoop l(*mSchedule); !l.atEnd(); ++l) {
      FLNodeElab* flowElab = *l;
      if (deleteTest(flowElab)) {
        removedFlows = true;
      } else {
        newFlows.push_back(flowElab);
      }
    }

    // Swap the non deleted flows with the vector so that the
    // deleted flows get removed
    if (removedFlows) {
      mSchedule->swap(newFlows);
    }

  } else {
    // It's in the temporary storage, look for the flow nodes in both edges
    SCHNodeSet newFlows;
    bool removedFlows = false;
    for (SCHNodeSetLoop l(*mScheduleSet); !l.atEnd(); ++l) {
      FLNodeElab* flowElab = *l;
      if (deleteTest(flowElab)) {
        removedFlows = true;
      } else {
        newFlows.insert(flowElab);
      }
    }

    // Swap the non deleted flows with the vector so that the
    // deleted flows get removed
    if (removedFlows) {
      mScheduleSet->swap(newFlows);
    }
  }
} // void SCHSequentialEdge::removeFlows

SCHNodeSetLoop SCHSequentialEdge::loopNodes()
{
  SCHED_ASSERT(!mScheduleCreated, this);
  return SCHNodeSetLoop(*(mScheduleSet));
}

bool SCHSequentialEdge::empty() const
{
  if (mScheduleCreated) {
    return mSchedule->empty();
  } else {
    return mScheduleSet->empty();
  }
}

UInt32 SCHSequentialEdge::size() const
{
  if (mScheduleCreated) {
    return mSchedule->size();
  } else {
    return mScheduleSet->size();
  }
}
  
void SCHSequentialEdge::printSchedNets(SCHMarkDesign* mark) const
{
  if (mScheduleCreated) {
    for (FLNodeElabVectorLoop l(*mSchedule); !l.atEnd(); ++l) {
      FLNodeElab* flowElab = *l;
      flowElab->pname(2, true);
      if (mark != NULL) {
        SCHMarkDesign::FlowDependencyLoop f;
        for (f = mark->loopDependencies(flowElab); !f.atEnd(); ++f) {
          FLNodeElab* fanin = *f;
          fanin->pname(4, true);
        }
      }
    }
  } else {
    typedef CLoop<SCHNodeSet> CNodeSetLoop;
    for (CNodeSetLoop l(*mScheduleSet); !l.atEnd(); ++l) {
      FLNodeElab* flowElab = *l;
      flowElab->pname(2, true);
      if (mark != NULL) {
        SCHMarkDesign::FlowDependencyLoop f;
        for (f = mark->loopDependencies(flowElab); !f.atEnd(); ++f) {
          FLNodeElab* fanin = *f;
          fanin->pname(4, true);
        }
      }
    }
  }
} // void SCHSequentialEdge::printSchedNets

int SCHSequentialEdge::compareSchedules(const SCHScheduleBase* schedBase) const
{
  // The other schedule must be sequential edge
  const SCHSequentialEdge* other = schedBase->castSequentialEdge();
  SCHED_ASSERT(other != NULL, this);

  // Check for the easy case
  if (this == other)
    return 0;

  // First compare by the parent schedule
  int cmp = getParentSchedule()->compareSchedules(other->getParentSchedule());
  if (cmp == 0) {
    // Same schedule, compare by edges
    cmp = (int)mEdge - (int)other->mEdge;
  }

  SCHED_ASSERT(cmp != 0, this);
  return cmp;
}

void SCHSequentialEdge::print(bool, SCHMarkDesign*) const
{
  bool unsupportedFunction = true;
  SCHED_ASSERT(!unsupportedFunction, this);
}

void SCHSequentialEdge::dump(int, SCHMarkDesign*) const
{
  bool unsupportedFunction = true;
  SCHED_ASSERT(!unsupportedFunction, this);
}

