// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file

  This file contains a simple cycle dumper class.  It writes out a cycle as
  a tree of flow nodes in depth-first order.  Each unique flow node is numbered,
  and the numbers are used to indicate back- and cross- connections in the graph.
  Each flow dependency is characterized as a data, clock, strength, etc. dependency
  in the dump.
*/

#include "CycleDumper.h"
#include "CycleGraph.h"
#include "Util.h"

CycleDumper::CycleDumper(UtOStream& out) : 
  UtGraphDumper(out), mOut(out), mCycleCount(0)
{}

CycleDumper::~CycleDumper() {}

void CycleDumper::printKey()
{
  mOut << "Cycle Dumping Key:\n";
  mOut << " D : data flow dependency\n";
  mOut << " M : multiple driver dependency\n";
  mOut << " W : weaker strength dependency\n";
  mOut << " ? : unknown dependency\n";
  mOut << " - : placeholder for top-level nodes\n";
  mOut << " * : indicates a back edge\n";
}

void CycleDumper::dumpCycle(DependencyGraph* graph)
{
  ++mCycleCount;

  UtString fileName;
  fileName << "cycle" << mCycleCount << ".dot";

  mOut << "Writing cycle #" << mCycleCount << " in dot format to '" << fileName << "'\n";
  CycleGraphDotWriter cgdw(fileName);
  cgdw.output(graph);

  mOut << "============== Dumping cycle #" << mCycleCount << " ================\n";
  NodeOrder nodeOrder(graph);
  EdgeOrder edgeOrder(graph);
  dump(graph, &nodeOrder, &edgeOrder, "-");
  mOut << "============== End of cycle #" << mCycleCount << " ================\n";
}

//! Overloaded function to compose a string for an edge
void CycleDumper::edgeString(Graph* graph, GraphNode* /* node */, GraphEdge* edge,
                             UtString* str)
{
  if (graph->anyFlagSet(edge, 0x1))
    *str = "D";
  else if (graph->anyFlagSet(edge, 0x2))
    *str = "W";
  else if (graph->anyFlagSet(edge, 0x4))
    *str = "M";
  else
    *str = "?";
}

//! Overloaded function to compose a string for a node
void CycleDumper::nodeString(Graph* graph, GraphNode* node, UtString* buf)
{
  DependencyGraph* depGraph = dynamic_cast<DependencyGraph*>(graph);
  DependencyGraph::Node* gdn = depGraph->castNode(node);
  FLNodeElab* flow = gdn->getData();
  NUNetElab* netElab = flow->getDefNet();
  netElab->compose(buf,NULL);
  *buf << " ";
  NUUseDefNode* useDef = flow->getUseDefNode();
  if (useDef)
  {
    *buf << useDef->typeStr() << " @";
    useDef->getLoc().compose(buf);
  }
}
