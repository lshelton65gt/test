// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
// this file is not currently compiled. it is work in progress.
// comments from josh: Q what are the CLkKey.cxx and .h files for
// That was the beginnings of a major overhaul I wanted to do to clock
// analysis, but never started.  The idea would be to have a more general
// purpose structure to use to as a key for clock equivalence lookups,
// rather than BDDs.  This would better accomodate things like multiply
// driven clocks (e.g. from a tristate bus).  However at the time I was
// doing that I was probably not thinking enough about how to deal withi
// clocks that go through vector nets, and I think before an overhaul is
// done we should have a design that does really well with those.


#ifndef _SCH_CLK_KEY_H_
#define _SCH_CLK_KEY_H_

//! SCHClkKey class -- used to uniquely key clocks by one of several methods
/*! 
 *! Simple gated and primary clocks are represented as BDDs.
 *!
 *! Clocks key from flops, including divided clocks, are represented
 *! by SCHDividers.  When those flops have async presets/restes, then
 *! there is more than one SCHDivider, so SCHClkKey accomodates a
 *! vector of SCHDividers.
 *!
 *! Clock-vectors -- that is, any vector net that includes one or more
 *! clocks, are represented by an array of SCHClkKey*.
 *!
 *! multiply driven clock bits are also reprented by an vector of SCHClkKey*.
 *! 
 *! clocks derived from dynamic bit-selects and arithmetic, are
 *! represented by semi-canonical expressions, which are compared
 *! using NUExpr::operator==.  The expressions have at their
 *! leaves NUIdentRvalues which are instances of class SCHClkNet,
 *! which are derived from NUNet*, and point to an SCHClock*.
 */
class SCHClkKey
{
public:
  //! ctor
  SCHClkKey();

  //! dtor
  virtual ~SCHClkKey();

  //! key types enumeration
  enum Type {eSimple, eDivider, eVector, eMultiDrive, eExpression};

  //! which type?
  virtual Type getType() const = 0;

  //! are two keys equivalent?
  bool operator==(const SCHClkKey&);

  //! compare two keys with the same type
  virtual bool isEqual(const SCHClkKey&) const = 0;

  //! compute hash function
  virtual size_t hash() const = 0;

  //! get the BDD
  virtual BDD getBDD() const;   // returns invalid() for eVector

  //! only valid for eVector and eMultiDrive
  virtual void push_back(SCHClkKey* key);

  static SCHClkKey* createSimple(BDD bdd);
  static SCHClkKey* createDivider(SCHClkAnal* ca, FLNodeElab* flow);
};

#endif // _SCH_CLK_KEY_H_
