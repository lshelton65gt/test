// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*! \file
 *  This file contains routines for extracting a scheduling dependency
 *  graph from the design.  The DependencyGraph is a representation of
 *  dependencies that can be used for enumerating the cycles and
 *  manipulating the dependency graph using standard graph algorithms.
 */

#ifndef _CYCLEGRAPH_H_
#define _CYCLEGRAPH_H_

#include "util/GenericDigraph.h"
#include "util/GraphDot.h"
#include "flow/Flow.h"
#include "bdd/BDD.h"
#include "flow/FLNodeElab.h"
#include "nucleus/NUUseDefNode.h"

// forward class declarations
class NUDesign;
class STSymbolTable;
class SCHMarkDesign;
class SCHUtil;

// This is the type of graph we use to represent ordering dependencies
typedef GenericDigraph<BDD,FLNodeElab*> DependencyGraph;

// these types are used to classify dependencies during writing
typedef enum { eFlowDependency=1, eStrengthDependency=2, eBrotherDependency=4, eCyclic=8 } DGEdgeTypeFlag;

/*! A class which encapsulates the functionality required to build
 *  a DependencyGraph from the elaborated flow of a design.
 */
class CycleGraph
{
public:
  //! constructor, builds a basic dependency graph from the design 
  CycleGraph(NUDesign* design, STSymbolTable* symtab, SCHMarkDesign* markDesign, SCHUtil* util, bool writeDotFiles=false);

  //! destructor, frees dependency graph resources
  ~CycleGraph() { delete mGraph; }

  //! get the current dependency graph -- NOT A COPY.  Do not alter or delete.
  DependencyGraph* getGraph() const { return mGraph; }

  //! add dependencies to ensure proper ordering for multiple drivers of the given nodes
  bool addMultiDriverDependencies(const FLNodeElabSet& flowSet);

  //! static method to write a dependency graph
  static void writeDependencyGraph(DependencyGraph* g, const char* filename, BDDContext* context = NULL);
private:
  //! determines which flow nodes can never be in a cycle
  bool includeFlow(const FLNodeElab* flow);

  //! the internal method that does the actual dependency graph creation
  DependencyGraph* extractDependencyGraph(NUDesign* design, STSymbolTable* symtab, SCHMarkDesign* markDesign, SCHUtil* util);
private:
  typedef UtMap<FLNodeElab*,DependencyGraph::Node*> NodeMap;
  DependencyGraph* mGraph; //!< the graph of cyclic dependencies
  NodeMap mNodeMap;        //!< a map from flow nodes back to graph nodes
  bool mWriteDotFiles;     //!< for debugging, we can write dot files along the way
};


//! A derived GraphDotWriter for writing a dependency graph in dot format
class CycleGraphDotWriter : public GraphDotWriter
{
public:
  CycleGraphDotWriter(UtOStream& os, BDDContext* context = NULL)
    : GraphDotWriter(os), mBDDContext(context) {};
  CycleGraphDotWriter(const UtString& filename, BDDContext* context = NULL)
    : GraphDotWriter(filename), mBDDContext(context) {};
private:
  void writeLabel(Graph* g, GraphNode* node);
  void writeLabel(Graph* g, GraphEdge* edge);
private:
  BDDContext* mBDDContext; //!< An optional BDDContext used for displaying edge conditions
};


//! used to sort graph nodes in order of the flow nodes they represent
/* We also want to ensure that higher strength drivers come before lower
 * strength drivers.  This is a neccessary ordering constraint when
 * scheduling cycles, to avoid breaking the cycle between a higher and a
 * lower strength driver and running the weaker driver after the stronger
 * one.
 */
class NodeOrder : public GraphSortedWalker::NodeCmp
{
public:
  NodeOrder(Graph* g) { mGraph = dynamic_cast<DependencyGraph*>(g); }
  bool operator()(const GraphNode* n1, const GraphNode* n2) const
  {
    DependencyGraph::Node* gdn = mGraph->castNode(const_cast<GraphNode*>(n1));
    FLNodeElab* flow1 = gdn->getData();
    gdn = mGraph->castNode(const_cast<GraphNode*>(n2));
    FLNodeElab* flow2 = gdn->getData();
    NUUseDefNode* useDef1 = flow1->getUseDefNode();
    NUUseDefNode* useDef2 = flow2->getUseDefNode();
    if (useDef1 == NULL)
    {
      if (useDef2 != NULL)
        return true;
    }
    else if (useDef2 == NULL)
    {
      return false;
    }
    else if (useDef1->getStrength() != useDef2->getStrength())
    {
      return (useDef1->getStrength() < useDef2->getStrength());
    }
    /* we get here if both UseDef nodes are equal strength (or NULL) */
    return (FLNodeElab::compare(flow1,flow2) < 0);
  }
private:
  DependencyGraph* mGraph;
};

//! used to sort graph edges in order of the flow nodes they point to
class EdgeOrder : public GraphSortedWalker::EdgeCmp
{
public:
  EdgeOrder(Graph* g) { mGraph = dynamic_cast<DependencyGraph*>(g); }
  bool operator()(const GraphEdge* e1, const GraphEdge* e2) const
  {
    GraphNode* node1 = mGraph->endPointOf(e1);
    GraphNode* node2 = mGraph->endPointOf(e2);
    DependencyGraph::Node* gdn = mGraph->castNode(node1);
    FLNodeElab* flow1 = gdn->getData();
    gdn = mGraph->castNode(node2);
    FLNodeElab* flow2 = gdn->getData();
    return (FLNodeElab::compare(flow1,flow2) < 0);
  }
private:
  DependencyGraph* mGraph;
};

//! Class to walk a cycle graph in sorted order and print the edge BDDs
/*! Note that this class can only print BDDs made up of elaborated
 *  nets. So it is not general purpose enough to make part of the BDD
 *  package.
 */
class PrintCycleBdds : public GraphSortedWalker
{
public:
  //! Constructor
  PrintCycleBdds(BDDContext* bddContext, DependencyGraph* graph) :
    mBDDContext(bddContext), mGraph(graph)
  {}

  //! Print the start node from visit node before
  Command visitNodeBefore(Graph*, GraphNode* graphNode);

  //! Print the edge BDD from the edge
  Command visitTreeEdge(Graph*, GraphNode*, GraphEdge* edge)
  {
    return visitEdge(edge);
  }

  //! Print the edge BDD from the edge
  Command visitCrossEdge(Graph*, GraphNode*, GraphEdge* edge)
  {
    return visitEdge(edge);
  }

  //! Print the edge BDD from the edge
  Command visitBackEdge(Graph*, GraphNode*, GraphEdge* edge)
  {
    return visitEdge(edge);
  }

private:
  //! Hide the copy constructor
  PrintCycleBdds(const PrintCycleBdds& src);

  //! Hide the assignment operator
  PrintCycleBdds& operator=(const PrintCycleBdds& src);

  //! Helper function to print information about an edge
  Command visitEdge(GraphEdge* edge);

  //! Helper function to print indent spaces
  void printIndent(int indent);

  //! Helper function to recursively print a BDD
  void printNetElabBdd(BDD bdd, int indent, int index);

  //! The current BDD index for the start node
  int mBddIndex;

  //! BDDContxt class to access data
  BDDContext* mBDDContext;

  //! Graph for casting
  DependencyGraph* mGraph;
}; // class PrintCycleBdds : public GraphSortedWalker

#endif // _CYCLEGRAPH_H_
