// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/CarbonContext.h"

#include "util/UtIOStream.h"
#include "util/CbuildMsgContext.h"
#include "util/Stats.h"
#include "util/ArgProc.h"

#include "schedule/Schedule.h"

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUDesign.h"

#include "flow/FLNodeElab.h"
#include "flow/FLNodeElabCycle.h"

#include "iodb/ScheduleFactory.h"
#include "iodb/IODBNucleus.h"

#include "Util.h"
#include "CombinationalBlockMerge.h"
#include "Ready.h"
#include "Dump.h"
#include "MarkDesign.h"
#include "ScheduleData.h"
#include "CreateSchedules.h"
#include "BlockSplit.h"
#include "SortCombBlocks.h"
#include "SortSchedules.h"
#include "BranchNets.h"
#include "CombinationalSchedules.h"

/*!
  \file

  This file is responsible for creating all the combinational
  schedules. The main routine that does the work is
  createCombinationalBlockSchedules.

  This file uses functions in Ready.cxx and BlockMerge.cxx to create a
  more efficient schedule.
*/

// Local scratch flags for flow
#define CS_IN_CYCLE	(1<<0)
#define CS_IN_SET	(1<<1)

// Constructor
SCHCombinationalSchedules::SCHCombinationalSchedules(SCHSchedule* sched,
                                                     SCHCreateSchedules* creat,
                                                     SCHUtil* util,
                                                     SCHDump* dump,
                                                     SCHScheduleData* data,
                                                     SCHScheduleFactory* f,
                                                     SCHMarkDesign* mark)
{
  mReady = new SCHReady(util, mark, dump);
  mBlockSplit = new SCHBlockSplit(util, mark, data);
  mSortCombBlocks = new SCHSortCombBlocks(util, mark);
  mSched = sched;
  mCreate = creat;
  mUtil = util;
  mDump = dump;
  mScheduleFactory = f;
  mMarkDesign = mark;
  mScheduleData = data;

  // Create the accurate reasons strings
  mReasons = new UtString[eCSReasonsSize];
  mReasons[eCSNone] = "None";
  mReasons[eCSCycle] = "is in a Cycle";
  mReasons[eCSFeedsCycle] = "feeds a Cycle node";
  mReasons[eCSLatch] = "is a Latch";
  mReasons[eCSFeedsLatch] = "feeds a Latch Node";
  mReasons[eCSMemWrite] = "is a Memory Write";
  mReasons[eCSFeedsMemWrite] = "feeds a Memory Write";
  mReasons[eCSFeedsOutput] = "feeds an Output Pin";
  mReasons[eCSFedByClock] = "is fed by a Clock or Clock Logic";
  mReasons[eCSFeedsClockNode] = "feeds a Clock Node";
  mReasons[eCSFedByClockBlock] = "is a Block fed by a Clock or Clock Logic";
  mReasons[eCSFeedsClockBlock] = "feeds a Clock Block";
  INFO_ASSERT(eCSFeedsClockBlock + 1 == eCSReasonsSize,
              "New transition schedule reason added without updating the table");
}

// Destructor
SCHCombinationalSchedules::~SCHCombinationalSchedules()
{
  delete mReady;
  delete mBlockSplit;
  delete mSortCombBlocks;
  delete[] mReasons;
}

// Create the combinational schedules
void
SCHCombinationalSchedules::create()
{
  // Create the map to get all the flow nodes from an always block
  SCHBlockFlowNodes blockFlowNodes(mUtil);

  // Visit the list of output ports and schedule them. We don't have
  // to handle sequential output ports because they are done below. We
  // only have to do combinational output ports that must be accurate
  // here.
  for (SCHMarkDesign::OutputPortsLoop p = mMarkDesign->loopOutputPorts();
       !p.atEnd(); ++p)
  {
    FLNodeElab* flow = *p;
    if (!SCHUtil::isSequential(flow))
      mCreate->scheduleNodes(flow, eCombTransition, blockFlowNodes);
  }

  // Traverse the list of sequential blocks and add any combinational
  // blocks that feed them to the schedule. This is a recursive
  // routine that starts at sequential blocks and ends at sequential
  // blocks.
  SCHScheduleType schedType;
  if (mUtil->isFlagSet(eUseSample))
    schedType = eCombSample;
  else
    schedType = eCombTransition;
  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s)
  {
    FLNodeElab* flow = *s;
    for (Fanin p = mUtil->loopFanin(flow); !p.atEnd(); ++p)
    {
      FLNodeElab* fanin = *p;
      if (fanin != flow)
      {
	if (!SCHUtil::isSequential(fanin))
	  mCreate->scheduleNodes(fanin, schedType, blockFlowNodes);
      }
    }
  }

  // Break up the input and async schedules into multiple schedules.
  const SCHScheduleMask* inputsMask = mMarkDesign->getInputsTransitionMask();
  if (inputsMask != NULL) {
    createInputSchedule(eCombInput, 1, inputsMask, NULL, mMarkDesign->loopInputScheduleNodes());
    createInputSchedule(eCombAsync, 1, inputsMask, NULL, mMarkDesign->loopAsyncScheduleNodes());
  } else {
    createInputSchedules(eCombInput, mMarkDesign->loopInputScheduleNodes());
    createInputSchedules(eCombAsync, mMarkDesign->loopAsyncScheduleNodes());
  }

  // Visit the nodes to create the debug/force schedule. Don't start
  // from inactive flops.
  UInt32 pass = mUtil->bumpSchedulePass();
  for (SCHMarkDesign::SchedStartLoop p(mMarkDesign); !p.atEnd(); ++p) {
    FLNodeElab* flow = *p;
    if (!SCHUtil::isSequential(flow)) {
      startCreateForceDepositSchedule(flow, pass);
    } else if (!flow->isInactive()) {
      for (Fanin p = mUtil->loopFanin(flow); !p.atEnd(); ++p) {
        FLNodeElab* fanin = *p;
        if ((fanin != flow)) {
          if (!SCHUtil::isSequential(fanin))
            startCreateForceDepositSchedule(fanin, pass);
        }
      }
    }
  }

  // Clear the set and find all deposit/force points that should be
  // marked combo run. We do this here, because although a net may be
  // frequently depositable and ignored, if there is a conflict, it
  // could still affect the deposit schedule. And comborun should be
  // accurate otherwise we could have what looks like glitchy behavior
  // (see bug 6642).
  SCHIterator i;
  for (i = mScheduleData->loopForceDepositSchedule(); !i.atEnd(); ++i) {
    // Clear the scratch flags
    FLNodeElab* flow = *i;
    flow->clearScratchFlags(CS_IN_SET);

    // Mark any fanin writable nets
    SCHMarkDesign::FlowDependencyLoop p;
    for (p = mMarkDesign->loopDependencies(flow, SCHMarkDesign::eInternal);
         !p.atEnd(); ++p) {
      FLNodeElab* fanin = *p;
      recordDepositTrigger(fanin);
    }

    // Also check if this flow is writable
    recordDepositTrigger(flow);
  } // for
} // SCHCombinationalSchedules::create

void SCHCombinationalSchedules::recordDepositTrigger(FLNodeElab* flowElab)
{
  // Only record it as a deposit trigger if it is depositable/forcible
  if (mMarkDesign->isWritable(flowElab)) {
    NUNetElab* netElab = flowElab->getDefNet();
    netElab->putIsComboRun(true);
  }
}

// Optimize the combinational schedules
bool SCHCombinationalSchedules::optimize(const char* fileRoot)
{
  // If we are timing passes, start a new timer
  bool ok = true;
  Stats* stats = mUtil->getStats();
  if (stats != NULL)
    stats->pushIntervalTimer();

  // Find the blocks we want to split up because of timing While we
  // are doing this we can also print information if the user
  // requested it
  ok &= analyzeMultiTimingBlocks();
  if (stats != NULL) stats->printIntervalStatistics("Comb MSched");
  if (!ok) {
    if (stats != NULL) stats->popIntervalTimer();
    return false;
  }
    
  // Sort the combinational schedules according to the input nets they
  // are sensitive to. We currently only do this for transition and
  // sample combinational schedules.
  //
  // Must be after block splitting but before node merging.
  sortSchedules(eCombTransition);
  sortSchedules(eCombSample);
  if (stats != NULL) stats->printIntervalStatistics("SchedSort");

  // At this point, all nodes within a combinational block are
  // represented by a flow node in the schedule. Since a flow node
  // represents a call to that block it means we would call the block
  // multiple times. So this next pass attempts to re-organize the
  // combinational schedules so that the nodes for the same block
  // instance are adjacent. This allows us to reduce the number of
  // calls.
  //
  // Once this pass is complete the schedule is no longer a vector of
  // flow nodes. It becomes a vector of vectors of flow nodes. All the
  // flow nodes from the same block instance that can be called
  // together will be together in a vector.
  SCHSchedulesLoop p;
  for (p = mScheduleData->loopAllCombinationals(); !p.atEnd(); ++p) {
    // Get the data for the next combinational schedule
    SCHCombinational* comb = p.getCombinationalSchedule();
    SCHScheduleType type = comb->getScheduleType();
    UtString buf;
    mSched->scheduleName(type, &buf);

    // Run the node merging function
    mergeCombinationalNodes(comb, buf.c_str());
  }
  if (stats != NULL) stats->printIntervalStatistics("Node Merge");

  // Now do the work to merge blocks
  bool dumpVerilog = mUtil->getArgs()->getBoolValue(CRYPT("-dumpVerilog"));
  MsgContext* msgContext = mUtil->getMsgContext();
  if (dumpVerilog) {
    mUtil->getDesign()->dumpVerilog(fileRoot, "schedulePreCombMerge", msgContext);
  }
  SCHCombinationalBlockMerge blockMerge(mUtil, mMarkDesign, mScheduleData);
  blockMerge.merge();
  if (dumpVerilog) {
    mUtil->getDesign()->dumpVerilog(fileRoot, "schedulePostCombMerge", msgContext);
  }
  if (stats != NULL) stats->printIntervalStatistics("Blk Merge");

  // Sort the combinational schedules so that we have good locality of
  // reference. This is done so that we can get more i-stream cache
  // hits. In this process we convert the schedule from a vector of
  // vectors of flow nodes to a simple vector of flow nodes. Each
  // vector of flow nodes is replaced with one of the flow nodes to
  // represent the set of flow nodes from the same block.
  for (p = mScheduleData->loopAllCombinationals(); !p.atEnd(); ++p) {
    // sort this combinational schedule
    SCHCombinational* comb = p.getCombinationalSchedule();
    mSortCombBlocks->sort(comb, mUtil->isFlagSet(eSortByData));
  }
  if (stats != NULL) stats->printIntervalStatistics("Blk Sort");

  if (stats != NULL) stats->popIntervalTimer();
  return ok;
} // SCHCombinationalSchedules::optimize

// Mark nodes that must be accurate
void SCHCombinationalSchedules::markDesignAccurateNodes()
{
  // Need a class to lookup all elaborated flow for an always block
  SCHBlockFlowNodes blockFlowNodes(mUtil);

  // Visit all clocks and visit the combinational fanin to mark them
  // must be accurate.
  UInt32 pass = mUtil->bumpSchedulePass();
  for (SCHClocksLoop p = mMarkDesign->loopClocks(); !p.atEnd(); ++p) {
    // Visit all the fanin flow for the clock
    const NUNetElab* clk = *p;
    for (SCHUtil::ConstDriverLoop n = mUtil->loopNetDrivers(clk);
         !n.atEnd(); ++n) {
      FLNodeElab* flowElab = *n;
      if (flowElab->isClock()) {
        markAccurateNodes(flowElab, eCSFeedsClockNode, blockFlowNodes);
      }
    }
  }

  // Visit the combinational flow nodes and look to see if any of them
  // are driven by clocks. These must be accurate otherwise the edge
  // detection code will not work correctly. We visit combinational
  // nodes by visiting the fanin of all sequential nodes.
  SCHMarkDesign::SequentialNodesLoop s;
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s)
  {
    FLNodeElab* flow = *s;
    SCHMarkDesign::FlowDependencyLoop p;
    for (p = mMarkDesign->loopDependencies(flow); !p.atEnd(); ++p) {
      FLNodeElab* fanin = *p;
      if ((fanin != flow) && !SCHUtil::isSequential(fanin))
 	lookForClocks(fanin, pass, blockFlowNodes);
    }
  }

  // Now visit the nodes again looking for must be accurate nodes
  pass = mUtil->bumpSchedulePass();
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s)
  {
    FLNodeElab* flow = *s;
    SCHMarkDesign::FlowDependencyLoop p;
    for (p = mMarkDesign->loopDependencies(flow); !p.atEnd(); ++p) {
      FLNodeElab* fanin = *p;
      if ((fanin != flow) && !SCHUtil::isSequential(fanin))
	lookForAccurateNodes(fanin, eCSNone, pass);
    }
  }

  // All outputs have to be accurate. Don't need to look for clocks
  // here because we always mark this must be accurate.
  for (SCHMarkDesign::OutputPortsLoop p = mMarkDesign->loopOutputPorts();
       !p.atEnd(); ++p)
  {
    FLNodeElab* flow = *p;
    if (!SCHUtil::isSequential(flow))
      markAccurateNodes(*p, eCSFeedsOutput, blockFlowNodes);
  }

  // All combinational logic feeding a sequential node that is a clock
  // must be accurate.
  // 
  // We used to do this by visiting all the sequential schedules in
  // the DCL schedules and applying markScheduleAccurateNodes to the
  // flow nodes in those schedules. But this code runs after the
  // sequential schedules have been pruned to have just one flow node
  // represent all the nets/statements in the block. So it is not as
  // accurate as visiting all sequential nodes here and testing if
  // they are part of the derived clock logic. See bug 2163 or
  // test/schedule/derviceclocks29.v for more info.
  //
  for (s = mMarkDesign->loopSequentialNodes(); !s.atEnd(); ++s) {
    FLNodeElab* flow = *s;
    if (flow->isDerivedClock() || flow->isDerivedClockLogic())
      markScheduleAccurateNodes(flow, blockFlowNodes);
  }
} // void SCHCombinationalSchedules::markDesignAccurateNodes

void 
SCHCombinationalSchedules::markScheduleAccurateNodes(
  FLNodeElab* flow,
  const SCHBlockFlowNodes& blockFlowNodes)
{
  SCHMarkDesign::FlowDependencyLoop p;
  for (p = mMarkDesign->loopDependencies(flow); !p.atEnd(); ++p) {
    FLNodeElab* fanin = *p;
    if (!SCHUtil::isSequential(fanin) && !fanin->isClock())
      markAccurateNodes(fanin, eCSFeedsClockNode, blockFlowNodes);
  }
}

SCHCombinationalSchedules::AccurateReason
SCHCombinationalSchedules::isAccurateNode(FLNodeElab* flow)
{
  // Check if it is a cycle
  if (flow->getCycle() != NULL)
    return eCSCycle;

  // Check if it is a primary input. This helps us avoid the NULL
  // check below
  NUUseDefNode* useDef = flow->getUseDefNode();
  if (useDef == NULL)
    return eCSNone;

  // Check if it is a memory. We are more pessemistic about memories
  // so that we don't try to split them. If any net in the block is a
  // memory, make all statements accurate.
  NUNetSet netSet;
  SCHUtil::getBlockNets(flow, &netSet);
  NUNetSet::iterator pos;
  AccurateReason accurateReason = eCSNone;
  for (pos = netSet.begin();
       (pos != netSet.end()) && (accurateReason == eCSNone);
       ++pos)
  {
    NUNet* net = *pos;
    if (net->is2DAnything()) {
      accurateReason = eCSMemWrite;
    }
  }

  // If there is no memory in the block, check if this net is a latch
  if ((accurateReason == eCSNone) && mMarkDesign->isLatch(flow)) {
    accurateReason = eCSLatch;
  }
  return accurateReason;
} // SCHCombinationalSchedules::isAccurateNode

// Look for any nodes that must be accurate. These include latches,
// memory writes, and cycles.
void
SCHCombinationalSchedules::lookForAccurateNodes(FLNodeElab* flow,
						AccurateReason accurateReason,
						UInt32 pass)
{
  // Don't need to handle sequential nodes
  if (SCHUtil::isSequential(flow))
    return;

  // If we are marking this accurate, then if it is already set we are done
  if (flow->mustBeTransition())
    return;

  // Check if we hit a cycle, if so give up
  if (flow->anyScratchFlagsSet(CS_IN_CYCLE))
    return;

  // Check if we are done marking this accurate
  if (accurateReason == eCSNone)
  {
    // We are searching for accurate nodes. We use a pass counter to
    // know we are done.
    if (flow->getSchedulePass() == pass)
      return;
    flow->putSchedulePass(pass);
  }

  // Calculate our own. Accurate nodes can only affect their
  // fanin. Only clocks can affect their fanout. So the accurate node
  // flag is the or condition of our parents and ours.
  AccurateReason curAccurateReason = accurateReason;
  if (curAccurateReason == eCSNone)
    curAccurateReason = isAccurateNode(flow);

  // Must not have been here before, visit the fanin looking for
  // accurate nodes or marking them.
  AccurateReason nextReason = computeNextReason(curAccurateReason);
  SCHMarkDesign::TrueFaninLoop l;
  flow->setScratchFlags(CS_IN_CYCLE);
  for (l = mMarkDesign->loopDependencies(flow, SCHMarkDesign::eInternal);
       !l.atEnd(); ++l)
  {
    FLNodeElab* fanin = *l;
    if ((fanin != flow) && !SCHUtil::isSequential(fanin))
      lookForAccurateNodes(fanin, nextReason, pass);
  }
  flow->clearScratchFlags(CS_IN_CYCLE);

  // If this is a must be accurate node, mark it
  if (curAccurateReason != eCSNone)
  {
    flow->putMustBeTransition(true);
    dumpTransitionNode(flow, curAccurateReason);
  }
} // SCHCombinationalSchedules::lookForAccurateNodes

// Mark a combinational block and all its fanin as must be accurate
void
SCHCombinationalSchedules::markAccurateNodes(
  FLNodeElab* flow,
  AccurateReason accurateReason,
  const SCHBlockFlowNodes& blockFlowNodes)
{
  // Don't need to handle sequential nodes
  if (SCHUtil::isSequential(flow)) {
    return;
  }

  // If there is no logic then nothing to do
  if (flow->getUseDefNode() == NULL) {
    return;
  }

  // If we should mark the block then do this for all nodes of the block
  if (accurateReason == eCSFedByClock)
  {
    // Iterate over the nodes we should process
    FLNodeElabVector nodes;
    blockFlowNodes.getFlows(flow, &nodes, &FLNodeElab::isLive);
    for (FLNodeElabVectorCLoop n(nodes); !n.atEnd(); ++n) {
      // Check if this was done already, if not, mark it
      FLNodeElab* curFlow = *n;
      if (!curFlow->mustBeTransition())
      {
	curFlow->putMustBeTransition(true);
	if (curFlow != flow)
	  accurateReason = eCSFedByClockBlock;
	dumpTransitionNode(curFlow, accurateReason);

	// Mark its fanin as well
	AccurateReason nextReason = computeNextReason(accurateReason);
	SCHMarkDesign::FlowDependencyLoop p;
	for (p = mMarkDesign->loopDependencies(curFlow,
                                               SCHMarkDesign::eInternal);
             !p.atEnd(); ++p)
	{
	  FLNodeElab* fanin = *p;
	  if ((fanin != curFlow) && !SCHUtil::isSequential(fanin))
	    markAccurateNodes(fanin, nextReason, blockFlowNodes);
	}
      }
    }
  } // if
  else if (!flow->mustBeTransition())
  {
    flow->putMustBeTransition(true);
    AccurateReason nextReason = computeNextReason(accurateReason);
    dumpTransitionNode(flow, nextReason);

    SCHMarkDesign::TrueFaninLoop l;
    for (l = mMarkDesign->loopTrueFanin(flow); !l.atEnd(); ++l)
    {
      FLNodeElab* fanin = *l;
      if ((flow != fanin) && !SCHUtil::isSequential(fanin))
	markAccurateNodes(fanin, nextReason, blockFlowNodes);
    }
  }

} // SCHCombinationalSchedules::markAccurateNodes

SCHCombinationalSchedules::AccurateReason
SCHCombinationalSchedules::computeNextReason(AccurateReason curReason)
{
  AccurateReason nextReason = eCSNone;
  switch (curReason)
  {
    case eCSNone:
    case eCSFeedsCycle:
    case eCSFeedsLatch:
    case eCSFeedsMemWrite:
    case eCSFeedsOutput:
    case eCSFeedsClockNode:
    case eCSFeedsClockBlock:
      // All of these reasons stay the same
      nextReason = curReason;
      break;
    
    case eCSCycle:
      // Switch to feeds a cycle
      nextReason = eCSFeedsCycle;
      break;
    
    case eCSLatch:
      // Switch to feeds a latch
      nextReason = eCSFeedsLatch;
      break;
    
    case eCSMemWrite:
      // Switch to feeds memory write
      nextReason = eCSFeedsMemWrite;
      break;
    
    case eCSFedByClock:
      // Switch to feeds a clock node
      nextReason = eCSFeedsClockNode;
      break;
    
    case eCSFedByClockBlock:
      // Switch to feeds a clock block
      nextReason = eCSFeedsClockBlock;
      break;

    case eCSReasonsSize:
      INFO_ASSERT(0, "Switch statement doesn't cover all enums");
      nextReason = eCSNone;
      break;
  } // switch
  return nextReason;
} // SCHCombinationalSchedules::computeNextReason

void
SCHCombinationalSchedules::dumpTransitionNode(FLNodeElab* flow,
					      AccurateReason accurateReason)
{
  if (accurateReason != eCSNone)
  {
    // Increment the correct statistic
    switch(accurateReason)
    {
      case eCSCycle:
      case eCSFeedsCycle:
        mDump->incrCycleNodes();
        break;
    
      case eCSLatch:
      case eCSFeedsLatch:
        mDump->incrLatchNodes();
        break;
    
      case eCSMemWrite:
      case eCSFeedsMemWrite:
        mDump->incrLevelMemoryNodes();
        break;
    
      case eCSFeedsOutput:
        mDump->incrFeedsOutputPinNodes();
        break;

      case eCSFedByClock:
      case eCSFeedsClockNode:
      case eCSFedByClockBlock:
      case eCSFeedsClockBlock:
        mDump->incrDrivenByClockNodes();
        break;

      case eCSNone:
      case eCSReasonsSize:
        INFO_ASSERT(0, "Switch statement doesn't cover all enums");
        break;
    } // switch
    mDump->incrMustBeAccurateNodes();

    // Optionally print the node that is run with transition
    if (mUtil->isFlagSet(eDumpTransitionNodes))
    {
      const char* reason = mReasons[accurateReason].c_str();
      mUtil->getMsgContext()->SCHTransitionNode(flow, reason);
    }
  }
}

bool
SCHCombinationalSchedules::lookForClocks(FLNodeElab* flow, UInt32 pass,
                                         const SCHBlockFlowNodes& blockFlowNodes)
{
  // If any flow in this block is a clock or part of a derived clock
  // schedule, then return that fact.  It means that this flow will be
  // executed early (even if it is not itself in a derived clock schedule).
  FLNodeElabVector nodes;
  blockFlowNodes.getFlows(flow, &nodes, &FLNodeElab::isLive);
  for (FLNodeElabVectorCLoop l(nodes); !l.atEnd(); ++l) {
    FLNodeElab* curFlow = *l;
    if (curFlow->isClock() || curFlow->isDerivedClockLogic())
      return true;
  }

  // Don't need to handle sequential nodes
  if (SCHUtil::isSequential(flow))
    return false;

  // Check if we are done looking for clocks
  if (flow->getSchedulePass() == pass)
    return false;
  flow->putSchedulePass(pass);

  // Must not have been here before, visit the fanin looking for clocks
  bool foundClock = false;
  SCHMarkDesign::FlowDependencyLoop p;
  for (p = mMarkDesign->loopDependencies(flow); !p.atEnd() && !foundClock; ++p) {
    FLNodeElab* fanin = *p;
    if (fanin != flow)
      foundClock = lookForClocks(fanin, pass, blockFlowNodes);
  }

  // If we found a clock, mark it as must be transition. Our caller
  // isn't fed directly by a clock so return false
  if (foundClock)
  {
    // We need to mark all outputs of the block as must be transition
    // because if not this entire block could be run with transition.
    markAccurateNodes(flow, eCSFedByClock, blockFlowNodes);
  }
  return false;
} // SCHCombinationalSchedules::lookForClocks

void
SCHCombinationalSchedules::scheduleCombinationalNode(FLNodeElab* flow,
						     SCHScheduleType schedType)
{
  const SCHSignature* signature = mMarkDesign->getSignature(flow);
  bool invalidPull = mMarkDesign->isInvalidPullDriver(flow);

  // We can either schedule this block with its sample mask or
  // transition mask.
  const SCHScheduleMask* constMask = mScheduleFactory->getConstantMask();
  const SCHScheduleMask* comboMask = NULL;
  if (schedType == eCombSample)
  {
    // Choose the sample schedule
    comboMask = signature->getSampleMask();
    FLN_ELAB_ASSERT(comboMask != NULL, flow);

    // Also put this in the debug schedule
    if (!mUtil->isFlagSet(eNoDebugSchedule) && !invalidPull) {
      mScheduleData->addCombinationalNode(eCombDebug, flow);
    }
  }
  else
  {
    // Must be the transition mask
    FLN_ELAB_ASSERT(schedType == eCombTransition, flow);

    // Add this block to the input/async/initial schedules as necessary
    addToSpecialSchedules(flow, signature, invalidPull);

    // Also add this block to the appropriate transition
    // combinational schedule if necessary
    const SCHScheduleMask* transitionMask = signature->getTransitionMask();
    if (transitionMask != constMask)
    {
      // We should not have the input event in the transition mask
      // anymore because inputs are handled by the input
      // schedule. So remove it, and if the mask is non-empty,
      // schedule the combo block again.
      comboMask = transitionMask;
      if (mScheduleFactory->hasInputMask(signature))
	comboMask = mScheduleFactory->removeInputMask(comboMask);
    }
  } // else

  // If we have a valid mask, we can schedule it now in the
  // appropriate type of schedule specified by our caller.
  if ((comboMask != NULL) && (comboMask != constMask) && !invalidPull) {
    SCHCombinational* comb = findCombinational(comboMask, schedType);
    comb->addCombinationalNode(flow);
  }
} // SCHCombinationalSchedules::scheduleCombinationalNode

// This routine takes a combinational schedule that has been sorted by
// some unknown method (today it is by depth-first search method) and
// re-sorts it to more efficient. It makes the schedule more efficient
// by combining flow nodes within the same always block instance if
// possible. Since cycles are not legal it does this only when it
// won't create a cycle.
void
SCHCombinationalSchedules::mergeCombinationalNodes(SCHCombinational* comb,
						   const char* schedName)
{
  // Go through the combinational blocks and add them to the ready queues
  for (SCHIterator iter = comb->getSchedule(); !iter.atEnd(); ++iter)
    mReady->addFlowNode(*iter);
  comb->clearSchedule();

  // If we are dumping unmerged blocks, print the schedule name
  UtString title;
  if (mUtil->isFlagSet(eDumpUnmerged))
  {
    title << "Unmerged blocks for schedule '" << schedName << "'";
    const SCHScheduleMask* mask = comb->getMask();
    if (mask != NULL)
    {
      UtString buf;
      mask->compose(&buf, NULL, true);   // include root in name (we have no testcase in almostall that checks this line)
      title << " (" << buf << ")";
    }
    title << ":\n";
  }

  // Initiate the schedule and then get blocks for scheduling. The set
  // we get from the ready queue all have the same use def and hier
  // name. That means it is the same always block. So we only have to
  // execute the first node in there.
  mReady->initiateSchedule(mUtil->getMsgContext());
  FLNodeElabVector nodes;
  bool dumpUnmerged = mUtil->isFlagSet(eDumpUnmerged);
  while (mReady->getNodesToSchedule(&nodes, dumpUnmerged, &title))
  {
    SCHED_ASSERT(!nodes.empty(), comb);

    // Get the nodes and schedule them
    comb->addCombinationalBlock(nodes);

    // Mark these flow nodes as scheduled
    for (FLNodeElabVectorIter pos = nodes.begin(); pos != nodes.end(); ++pos)
    {
      // Tell the ready queue we schedule this node
      FLNodeElab* flow = *pos;
      mReady->markAsScheduled(flow);
    }

    // All done with this memory, clear it for the next set.
    nodes.clear();
  } // while

  // We are done, tell the ready class so that it can clean up memory
  // that will not be used by the next re-schedule
  mReady->cleanUp();
} // SCHCombinationalSchedules::mergeCombinationalNodes


SCHCombinational*
SCHCombinationalSchedules::findCombinational(const SCHScheduleMask* mask,
					     SCHScheduleType type)
{
  INFO_ASSERT((type == eCombTransition) || (type == eCombSample),
              "Invalid combinational schedule type");
  CombinationalMap& combMap = getCombinationalMap(type);
  SCHCombinational* comb = combMap[mask];
  if (comb == NULL)
  {
    comb = new SCHCombinational(type, mask, NULL);
    combMap[mask] = comb;
    mScheduleData->addCombinationalSchedule(type, comb);
  }
  return comb;
}

void
SCHCombinationalSchedules::addToSpecialSchedules(FLNodeElab* flow,
						 const SCHSignature* signature,
                                                 bool onlyInitSchedule)
{
  // Check if this block belongs in the input or asynchronous
  // schedule as well. The asynchronous schedule is for blocks that
  // transition due to the input and are sampled by the output. The
  // input schedule is for blocks that is sensitive to inputs and
  // not sampled by outputs.
  if (mScheduleFactory->hasInputMask(signature) && !onlyInitSchedule)
  {
    if (mScheduleFactory->hasOutputMask(signature))
      // Belongs in the async schedule and not the input schedule
      mMarkDesign->addAsyncScheduleNode(flow);
    else
      // Belongs in the input schedule and not the async schedule
      mMarkDesign->addInputScheduleNode(flow);
  }

  // All transition combinational blocks go into the init settle
  // schedule. This is so that we propagate state devices and primary
  // inputs that have been initialized to zero in the first cycle.
  if (SCHUtil::isInitial(flow)) {
    mScheduleData->addCombinationalNode(eCombInitial, flow);
  } else if (!mUtil->isFlagSet(eDisableInitCombos)) {
    mScheduleData->addCombinationalNode(eCombInitSettle, flow);
  }
} // SCHCombinationalSchedules::addToSpecialSchedules

static inline void
dumpMultiTimingBlock(SCHBlockSplit::BlockInstanceGroups* blockGroups,
		     SCHBlockSplit* blockSplit, SCHDump* dump,
		     bool multiSchedBlock,
		     SCHUtil* util)
{
  SCHBlockSplit::BlockInstanceGroups::iterator g;
  for (g = blockGroups->begin(); g != blockGroups->end(); ++g)
  {
    FLNodeElabVector& nodes = *g;
    for (FLNodeElabVectorIter n = nodes.begin(); n != nodes.end(); ++n)
    {
      // Get the schedules for this node
      FLNodeElab* flow = *n;
      const SCHBlockSplit::Schedules* schedules;
      schedules = blockSplit->getFlowSchedules(flow);

      // Gather up all the schedules for the dumper. We don't want to
      // print the initial and debug schedules so ignore those.
      SCHDump::CombSchedules combSchedules;
      SCHBlockSplit::Schedules::const_iterator cp;
      for (cp = schedules->begin(); cp != schedules->end(); ++cp) {
	SCHCombinational* comb = dynamic_cast<SCHCombinational*>(*cp);
        SCHScheduleType schedType = comb->getScheduleType();
        SCHScheduleTypeMask typeMask = SCHCreateScheduleTypeMask(schedType);
	if ((typeMask & eMaskAllSimulationCombs) != 0) {
	  combSchedules.push_back(comb);
        }
      }

      // Give the data to the dump routine to print
      if (multiSchedBlock || (combSchedules.size() > 1))
      {
	// If we have a cycle node, then we need the outermost cycle
	FLNodeElab* curFlow = util->getAcyclicNode(flow);
	dump->dumpMultiScheduleBlocks(curFlow, &combSchedules);
      }
    }
  }
} // SCHCombinationalSchedules::dumpMultiTimingBlock

bool
SCHCombinationalSchedules::analyzeMultiTimingBlocks()
{
  // Figure out which blocks are in more than one schedule. This
  // occurs if a combinational block has more than one output and the
  // timing of the various outputs are different.
  //
  // We pass in a loop for all the combinational schedules.
  mBlockSplit->findMultiTimingBlocks(mScheduleData->loopAllCombinationals());

  // There are two reasons a block may be in more than once
  // schedule. One is because different nodes have different
  // timing. The other is because a single node is in multiple
  // schedules (e.g. input and transition schedule). We only want to
  // split for the former case but we want to print info for both
  // cases.
  bool dumpInfo = mUtil->isFlagSet(eDumpMultiSched);
  SCHBlockSplit::MultiSchedBlocksLoop ml;
  for (ml = mBlockSplit->loopMultiTimingBlocks(); !ml.atEnd(); ++ml)
  {
    SCHBlockSplit::BlockInstanceGroups* blockGroups = *ml;

    // If we want to split this block, mark it
    if (mBlockSplit->isMultiSchedBlock(blockGroups))
      mBlockSplit->markSplitBlock(blockGroups);
    else
      if (dumpInfo)
	// Dump the blocks that are not multi schedule but it has
	// nodes that are in more than one schedule.
	dumpMultiTimingBlock(blockGroups, mBlockSplit, mDump, false, mUtil);
  }

  // Now split the blocks we have marked.
  bool ok = true;
  if (mUtil->isFlagSet(eSplitBlocks))
  {
    FLNodeElabSet newFlows;
    if (mUtil->isFlagSet(eDumpSplitBlocks))
      UtIO::cout() << "Splitting combinational blocks due to timing:\n";
    ok = mBlockSplit->splitBlocks(&newFlows);
    if (!newFlows.empty()) {
      FLNodeElab* flowElab = *(newFlows.begin());
      FLN_ELAB_ASSERT(newFlows.empty(), flowElab);
    }
  }

  // And finally, dump information about the multi-schedule blocks
  // that we could not split.
  SCHBlockSplit::UnSplitBlocksLoop ul;
  for (ul = mBlockSplit->loopUnSplitBlocks(); dumpInfo && !ul.atEnd(); ++ul)
  {
    SCHBlockSplit::BlockInstanceGroups* blockGroups = *ul;
    dumpMultiTimingBlock(blockGroups, mBlockSplit, mDump, true, mUtil);
  }
  return ok;
} // SCHCombinationalSchedules::analyzeMultiTimingBlocks

void
SCHCombinationalSchedules::createInputSchedules(SCHScheduleType type,
                                                FLNodeElabVectorLoop nodeLoop)
{
  // Visit the nodes to sort them by the input nets that affect them
  NetSetNodes netSetNodes;
  for (;!nodeLoop.atEnd(); ++nodeLoop)
  {
    FLNodeElab* flow = *nodeLoop;
    flow->clearScratchFlags(CS_IN_SET);
    const SCHInputNets* netSet = mMarkDesign->getFlowInputNets(flow);
    FLN_ELAB_ASSERT(netSet != NULL, flow);
    netSetNodes[netSet].push_back(flow);
  }

  // Create the various schedules from our map
  int index = 0;
  const SCHScheduleMask* mask = mScheduleFactory->getInputMask();
  for (NetSetNodesIter p = netSetNodes.begin(); p != netSetNodes.end(); ++p)
  {
    // Create the schedule
    const SCHInputNets* netSet = p->first;
    FLNodeElabVector& nodes = p->second;
    FLNodeElabVectorLoop loop(nodes);
    createInputSchedule(type, ++index, mask, netSet, loop);
  }
} // SCHCombinationalSchedules::createInputSchedules

void
SCHCombinationalSchedules::createInputSchedule(SCHScheduleType type,
                                               int index,
                                               const SCHScheduleMask* mask,
                                               const SCHInputNets* netSet,
                                               FLNodeElabVectorLoop nodeLoop)
{
  // Create the schedule and add it to the list
  SCHCombinational* comb = new SCHCombinational(type, mask, NULL, index);
  mScheduleData->addCombinationalSchedule(type, comb);

  // Add the nets to it
  comb->putAsyncInputNets(netSet);

  // Add the nodes
  for (; !nodeLoop.atEnd(); ++nodeLoop) {
    FLNodeElab* flow = *nodeLoop;
    comb->addCombinationalNode(flow);
  }
} // SCHCombinationalSchedules::createInputSchedule


// Comparison operator so we can make a UtMap<SchduleMask*> that has
// consistent iteration.  We also could have made a hash_map and
// kept a sorted iteration array, but a map is probably good enough
// for the first cut.
bool 
SCHCombinationalSchedules::CmpMasks::operator()(const SCHScheduleMask* m1,
                                                const SCHScheduleMask* m2)
  const
{
  return SCHScheduleMask::compare(m1, m2) < 0;
}

void
SCHCombinationalSchedules::startCreateForceDepositSchedule(FLNodeElab* flow,
                                                           UInt32 pass)
{
  // Don't visit if this is a bound node or initial block. We don't
  // want deposit nets that don't feed any real combo logic to appear
  // as a combo run net.
  if (!flow->isBoundNode() && !SCHUtil::isInitial(flow)) {
    createForceDepositSchedule(flow, pass);
  }
}

bool
SCHCombinationalSchedules::createForceDepositSchedule(FLNodeElab* flow,
						      UInt32 pass)
{
  // Check if we have already been here, if so return whether this
  // flow node is in the force/deposit set.
  if (flow->getSchedulePass() == pass) {
    if (flow->anyScratchFlagsSet(CS_IN_SET) || 
        mMarkDesign->isWritable(flow, true)) {
      return true;
    } else {
      return false;
    }
  }
  flow->putSchedulePass(pass);

  // Visit the fanin to see if this flow node should be in the force
  // deposit schedule. Don't need to do this for sequentials
  bool hasDepositFanin = false;
  if (!SCHUtil::isSequential(flow)) {
    SCHMarkDesign::FlowDependencyLoop p;
    for (p = mMarkDesign->loopDependencies(flow, SCHMarkDesign::eInternal);
         !p.atEnd(); ++p) {
      FLNodeElab* fanin = *p;
      if (flow != fanin)
	hasDepositFanin |= createForceDepositSchedule(fanin, pass);
    }
  }

  // Check if this flow node represents a depositable or forcible net
  bool writable = mMarkDesign->isWritable(flow, true);
  if (writable) {
    NUNetElab* netElab = flow->getDefNet();
    netElab->putIsComboRun(true);
  }

  // Schedule this node if necessary
  // The valid conditions are:
  //
  // 1. It or one of its fanins is depositable or forcible.
  //
  //            and
  //
  // 2. It is not a flop
  //
  //            and
  //
  // 3. It is not an initial block
  //
  //            and
  //
  // 4. It isn't a pull driver that shouldn't be scheduled (see documentation)
  //    for isInvalidPullDriver.
  //
  if ((hasDepositFanin || writable) && !SCHUtil::isSequential(flow) &&
      ((flow->getUseDefNode() != NULL) && !SCHUtil::isInitial(flow)) &&
      !mMarkDesign->isInvalidPullDriver(flow)) {
    // Schedule this combinational node
    mScheduleData->addCombinationalNode(eCombForceDeposit, flow);
    mMarkDesign->addWritableFanout(flow);
    flow->setScratchFlags(CS_IN_SET);
  }

  return hasDepositFanin || mMarkDesign->isWritable(flow, true);
} // SCHCombinationalSchedules::createForceDepositSchedule

    
void 
SCHCombinationalSchedules::SortSchedules::printCycle(const ScheduleVector& scheds)
  const
{
  // For each schedule, figure out the fanin relationship with other
  // schedules and print the flow node that caused it.
  // Figure out the schedules for every flow
  for (CLoop<ScheduleVector> l(scheds); !l.atEnd(); ++l) {
    SCHScheduleBase* schedBase = *l;
    SCHCombinational* comb = schedBase->castCombinational();
    if (comb == NULL) {
      // Unexpected type, don't assert because we want to get more
      // info out. So just print a message.
      UtIO::cout() << "Expecting combinational schedule but got "
                   << schedBase->getScheduleType() << "\n";
    } else {
      for (SCHIterator i = comb->getSchedule(); !i.atEnd(); ++i) {
        FLNodeElab* flowElab = *i;
        SCHMarkDesign::FlowDependencyLoop p;
        for (p = mMarkDesign->loopDependencies(flowElab); !p.atEnd(); ++p) {
          FLNodeElab* faninElab = *p;
          FlowToSchedMap::const_iterator pos = mFlowToSchedMap.find(faninElab);
          if (pos != mFlowToSchedMap.end()) {
            SCHCombinational* faninComb = pos->second;
            if (faninComb != comb) {
              UtIO::cout() << "Edge from comb(" << comb << ") to fanin("
                           << faninComb << ") due to:\n";
              UtIO::cout() << "Fanout: ";
              flowElab->pname(0,1);
              flowElab->getSignature()->print();
              flowElab->getUseDefNode()->printVerilog(1,2,1);
              UtIO::cout() << "Fanin: ";
              faninElab->pname(0,1);
              faninElab->getSignature()->print();
            }
          }
        }
      }
    }
  }
}

void SCHCombinationalSchedules::sortSchedules(SCHScheduleType type)
{
  // Check if there is no work to do
  if (!mScheduleData->hasCombinationalSchedules(type)) {
    return;
  }

  // Class to compute branch nets for clocks
  SCHBranchNets branchNets(mMarkDesign, SCHBranchNetFlag(eIncludeClock));
  branchNets.init();

  // Create the data we need for the sort. This includes a flow to
  // schedule map, and the set of inputs these schedules are sensitive
  // to.
  FlowToSchedMap flowToSchedMap;
  SCHSchedule::CombinationalLoop l;
  for (l = mScheduleData->loopCombinational(type); !l.atEnd(); ++l) {
    // compute the input net sets
    SCHCombinational* comb = *l;
    const SCHScheduleMask* mask = comb->getMask();
    const SCHInputNets* inputNets = branchNets.compute(mask);
    comb->putBranchNets(inputNets);

    // Create the flow to combinational map
    addToFlowToSchedMap(comb, flowToSchedMap);
  }

  // Add the schedules to the sorting class
  SortSchedules* sortSchedules = new SortSchedules(mMarkDesign, flowToSchedMap);
  for (l = mScheduleData->loopCombinational(type); !l.atEnd(); ++l) {
    // use the mask to compute the input net sets
    SCHCombinational* comb = *l;
    const SCHInputNets* inputNets = comb->getBranchNets();
    sortSchedules->addSched(comb, inputNets);
  }

  // Compute the fanout for the schedules
  for (l = mScheduleData->loopCombinational(type); !l.atEnd(); ++l) {
    // use the mask to compute the input net sets
    SCHCombinational* comb = *l;
    for (SCHIterator iter = comb->getSchedule(); !iter.atEnd(); ++iter) {
      FLNodeElab* flow = *iter;
      SCHMarkDesign::FlowDependencyLoop p;
      for (p = mMarkDesign->loopDependencies(flow); !p.atEnd(); ++p) {
        // Don't include dependencies on derived clocks because it may
        // cause a cycle in the graph. This can only occur with single
        // DCL cycles because those are the only ones that will be
        // schedule in a transition schedule as well as a DCL cycle.
        //
        // TBD: When we treat single DCL cycles as cycles and get rid
        // of them from transition schedules, this if statement can be
        // removed!
	FLNodeElab* fanin = *p;
        if (!fanin->isDerivedClock()) {
          FlowToSchedMap::iterator pos = flowToSchedMap.find(fanin);
          if (pos != flowToSchedMap.end()) {
            SCHCombinational* faninComb = pos->second;
            if (comb != faninComb)
              sortSchedules->addFanout(faninComb, comb);
          }
	}
      }
    }
  } // for

  // Sort the schedules
  int index = 0;
  SortSchedules::ScheduleGroup schedGroup;
  while (sortSchedules->getNextSchedule(schedGroup))
  {
    // Assign this group the same depth since they can be scheduled together
    INFO_ASSERT(!schedGroup.empty(),
                "Found an empty schedule group when sorting schedules");
    ++index;
    SortSchedules::ScheduleGroupLoop l;
    for (l = sortSchedules->loopSchedules(schedGroup); !l.atEnd(); ++l)
    {
      SCHScheduleBase* sched = *l;
      SCHCombinational* comb = dynamic_cast<SCHCombinational*>(sched);
      comb->putIndex(index);
    }
    schedGroup.clear();
  }
  delete sortSchedules;

  // Sort by the index, input nets, and clock characteristics
  mScheduleData->sortCombinationalSchedules(type, lessCombinational);
}

void
SCHCombinationalSchedules::addToFlowToSchedMap(SCHCombinational* comb,
					       FlowToSchedMap& flowToSchedMap)
{
  for (SCHIterator iter = comb->getSchedule(); !iter.atEnd(); ++iter)
  {
    FLNodeElab* flow = *iter;
    FLN_ELAB_ASSERT(flowToSchedMap.find(flow) == flowToSchedMap.end(), flow);
    flowToSchedMap.insert(FlowToSchedMap::value_type(flow, comb));
  }
}

// Comparison operator so we can sort combinational schedules by their
// index
bool
SCHCombinationalSchedules::lessCombinational(SCHCombinational* comb1,
					     SCHCombinational* comb2)
{
  // Check the easy case, they are identical
  if (comb1 == comb2)
    return false;

  // Make sure the indecies are valid. This should never happen
  int index1 = comb1->getIndex();
  SCHED_ASSERT(index1 > 0, comb1);
  int index2 = comb2->getIndex();
  SCHED_ASSERT(index2 > 0, comb2);

  // We sort in order of the index
  int cmp = index1 - index2;

  // In the event of ties, we break ties based first on the set of
  // input nets, then on the schedule mask
  if (cmp == 0)
  {
    // Compare by input nets, but be careful because they may be null
    const SCHInputNets* ns1 = comb1->getBranchNets();
    const SCHInputNets* ns2 = comb2->getBranchNets();
    if ((ns1 == NULL) && (ns2 == NULL))
      cmp = 0;
    else if (ns1 == NULL)
      cmp = -1;
    else if (ns2 == NULL)
      cmp = 1;
    else
      cmp = SCHInputNetsFactory::compare(ns1, ns2);

    if (cmp == 0)
    {
      // Compare by schedule masks
      const SCHScheduleMask* mask1 = comb1->getMask();
      const SCHScheduleMask* mask2 = comb2->getMask();
      cmp = SCHScheduleMask::compare(mask1, mask2);
    }
  } // if
  SCHED_ASSERT2(cmp != 0, comb1, comb2);
  return cmp < 0;
} // SCHCombinationalSchedules::lessCombinational

