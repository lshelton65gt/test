// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "schedule/Schedule.h"

#include "MarkDesign.h"
#include "TimingAnalysis.h"
#include "ScheduleData.h"
#include "BlockFlowNodes.h"

SCHScheduleData::SCHScheduleData(SCHUtil* util, SCHMarkDesign* markDesign) :
  mUtil(util), mMarkDesign(markDesign)
{
  // Create space for the transition/sample combinational schedules
  mTransitionSchedules = new Combinationals;
  mSampleSchedules = new Combinationals;

  // Create space for the input/async schedules
  mInputSchedules = new SCHSchedule::Combinationals;
  mAsyncSchedules = new SCHSchedule::Combinationals;

  // Create space for the special schedules
  mInitialSchedule = new SCHCombinational(eCombInitial, NULL, NULL);
  mInitSettleSchedule = new SCHCombinational(eCombInitSettle, NULL, NULL);
  mDebugSchedule = new SCHCombinational(eCombDebug, NULL, NULL);
  mForceDepositSchedule = new SCHCombinational(eCombForceDeposit, NULL, NULL);

  // Create space for the normal sequential schedules
  mSequentials = new SCHSequentials(eSequential, util);

  // Create the derived clock schedules
  mDerivedClockSchedules = new SCHSchedule::DerivedClockLogicVector;

  // Create a set of elaborated nets that must be cleared at the end
  // of the schedule.
  mClearAtEndNets = new SCHSchedule::ClearAtEndNets;

  // Initialize the set of async clocks to NULL. It will be allocated
  // when it is valid. This catches any uses before it is valid bugs.
  mAsyncClocks = NULL;
}

SCHScheduleData::~SCHScheduleData()
{
  // Get rid of the transition and sample  combinational schedules
  deleteCombinationals(mTransitionSchedules);
  deleteCombinationals(mSampleSchedules);

  // Get rid of the input and async schedules
  deleteCombinationals(mInputSchedules);
  deleteCombinationals(mAsyncSchedules);

  // Get rid of the special schedules
  delete mInitialSchedule;
  delete mInitSettleSchedule;
  delete mDebugSchedule;
  delete mForceDepositSchedule;

  // Get rid of the normal sequential schedules
  delete mSequentials;

  // Get rid of the derived clock schedules
  SCHSchedule::DerivedClockLogicLoop l;
  for (l = loopDerivedClockLogic(); !l.atEnd(); ++l) {
    SCHDerivedClockBase* dclBase = *l;
    delete dclBase;
  }
  delete mDerivedClockSchedules;

  // Delete the set of elaborated nets that must be cleared at the end
  // of the schedule.
  for (SCHSchedule::ClearAtEndNetsLoop l(*mClearAtEndNets); !l.atEnd(); ++l) {
    NUNetElabVector* netElabs = l.getValue();
    delete netElabs;
  }
  delete mClearAtEndNets;

  // Delete the async clocks
  delete mAsyncClocks;
} // SCHScheduleData::~SCHScheduleData

void SCHScheduleData::deleteCombinationals(Combinationals* combinationals)
{
  for (SCHSchedule::CombinationalLoop p(*combinationals); !p.atEnd(); ++p) {
    SCHCombinational* comb = *p;
    delete comb;
  }
  delete combinationals;
}

//! Class to remove dead flow from schedules
class SCHDeadTest : public SCHDeleteTest
{
public:
  //! constructor
  SCHDeadTest(const FLNodeElabSet& flowSet) : SCHDeleteTest(), mFlowSet(flowSet) {}

  //! destructor
  ~SCHDeadTest() {}

  //! Function to test whether this should be deleted or not
  bool operator()(FLNodeElab* flow) const
  {
    return (mFlowSet.find(flow) != mFlowSet.end()) || !flow->isLive();
  }

private:
  const FLNodeElabSet& mFlowSet;
};

void
SCHScheduleData::removeDeadFlow(const FLNodeElabSet& deletedFlow)
{
  // Ask the design mark class to remove any copies of deleted or dead
  // flow.
  mMarkDesign->removeDeadFlow(deletedFlow);

  // Clean up the sequential schedules
  SCHDeadTest seqDeadTest(deletedFlow);
  for (SCHSchedulesLoop l = loopAllSequentials(); !l.atEnd(); ++l) {
    SCHSequential* seq = l.getSequentialSchedule();
    seq->removeFlows(seqDeadTest);
  }
   
  // Must also remove deleted flow from the combinational dcl.
  // test/schedule/clkalias.v
  SCHDeadTest deadTest(deletedFlow);
  for (SCHSchedulesLoop c = loopAllCombinationals(); !c.atEnd(); ++c) {
    SCHCombinational* comb = c.getCombinationalSchedule();
    comb->removeFlows(deadTest);
  }

  // Remove any derived clock cycle break nets that went dead.
  for (SCHSchedule::DerivedClockLogicLoop d = loopDerivedClockLogic();
       !d.atEnd(); ++d) {
    SCHDerivedClockBase* dclBase = *d;
    SCHDerivedClockCycle* dclCycle = dclBase->castDerivedClockCycle();
    if (dclCycle != NULL) {
      dclCycle->removeDeadBreakNets(mUtil);
    }
  }
}

SCHSequentials::SULoop SCHScheduleData::loopSUNets()
{
  return mSequentials->loopSUNets();
}

void SCHScheduleData::findSUNets()
{
  // Create the map to get all the flow nodes from an always block
  SCHBlockFlowNodes blockFlowNodes(mUtil);

  mSequentials->findSUNets(blockFlowNodes);
  for (SCHSchedule::DerivedClockLogicLoop l = loopDerivedClockLogic();
       !l.atEnd(); ++l) {
    SCHDerivedClockBase* dclBase = *l;
    dclBase->findSUNets(blockFlowNodes);
  }
}

void
SCHScheduleData::addClearAtEndNet(const SCHScheduleMask* mask,
                                  NUNetElab* netElab)
{
  // Find the spot to put it
  typedef SCHSchedule::ClearAtEndNets::value_type ClearAtEndNetsValue;
  SCHSchedule::ClearAtEndNets::iterator pos = mClearAtEndNets->find(mask);
  NUNetElabVector* netElabs = NULL;
  if (pos == mClearAtEndNets->end()) {
    // Allocate space
    netElabs = new NUNetElabVector;
    mClearAtEndNets->insert(ClearAtEndNetsValue(mask, netElabs));
  } else {
    netElabs = pos->second;
  }

  // Add it to the vector
  netElabs->push_back(netElab);
}

SCHSchedule::ClearAtEndNetsLoop SCHScheduleData::loopClearAtEndNets()
{
  return SCHSchedule::ClearAtEndNetsLoop(*mClearAtEndNets);
}

//! Iterator over the sequential schedules (doesn't include DCL sequentials)
SCHSequentials::SequentialLoop SCHScheduleData::loopSequential()
{
  return mSequentials->loopSequential();
}

//! Iterator for all sequential schedules
SCHSchedulesLoop SCHScheduleData::loopAllSequentials()
{
  return SCHSchedulesLoop(this, eMaskAllSequentials);
}

//! Iterator for all combinational schedules
SCHSchedulesLoop SCHScheduleData::loopAllCombinationals()
{
  return SCHSchedulesLoop(this, eMaskAllCombinationals);
}

//! Iterator for all simulation combinational schedule (no init or debug)
SCHSchedulesLoop SCHScheduleData::loopSimulationCombinationals()
{
  return SCHSchedulesLoop(this, eMaskAllSimulationCombs);
}

//! Iterator for all simulation schedules (no init or debug)
SCHSchedulesLoop SCHScheduleData::loopAllSimulationSchedules()
{
  return SCHSchedulesLoop(this, eMaskAllSimulationScheds);
}

void
SCHScheduleData::addDerivedClockSchedule(SCHDerivedClockBase* dclBase)
{
  mDerivedClockSchedules->push_back(dclBase);
}

void
SCHScheduleData::sortDerivedClockSchedules(LessDCLSchedules lessDCLSchedules)
{
  std::sort(mDerivedClockSchedules->begin(),
            mDerivedClockSchedules->end(),
            lessDCLSchedules);
}

void
SCHScheduleData::addCombinationalSchedule(SCHScheduleType schedType,
                                          SCHCombinational* comb)
{
  // Note that the support set of schedules must match between this
  // routine and hasCombinationalSchedules,
  // sortCombinationalSchedules, and loopCombinational.
  switch(schedType) {
    case eCombTransition:
      mTransitionSchedules->push_back(comb);
      break;
    case eCombSample:
      mSampleSchedules->push_back(comb);
      break;
    case eCombInput:
      mInputSchedules->push_back(comb);
      break;
    case eCombAsync:
      mAsyncSchedules->push_back(comb);
      break;
    default:
      SCHED_ASSERT("Unknown schedule type" == NULL, comb);
      break;
  }
}

bool SCHScheduleData::hasCombinationalSchedules(SCHScheduleType schedType)
  const
{
  // Note that the support set of schedules must match between this
  // routine and addCombinationalSchedule, sortCombinationalSchedules,
  // and loopCombinational.
  bool hasSchedule = false;
  switch(schedType) {
    case eCombTransition:
      hasSchedule = !mTransitionSchedules->empty();
      break;
    case eCombSample:
      hasSchedule = !mSampleSchedules->empty();
      break;
    case eCombInput:
      hasSchedule = !mInputSchedules->empty();
      break;
    case eCombAsync:
      hasSchedule = !mAsyncSchedules->empty();
      break;
    default:
      INFO_ASSERT(0, "Unknown schedule type");
      break;
  }
  return hasSchedule;
}

void
SCHScheduleData::sortCombinationalSchedules(SCHScheduleType schedType,
                                            LessCombSchedules lessCombScheds)
{
  // Note that the support set of schedules must match between this
  // routine and addCombinationalSchedule, hasCombinationalSchedules,
  // and loopCombinational.
  switch(schedType) {
    case eCombTransition:
      std::sort(mTransitionSchedules->begin(),
                mTransitionSchedules->end(),
                lessCombScheds);
      break;
    case eCombSample:
      std::sort(mSampleSchedules->begin(),
                mSampleSchedules->end(),
                lessCombScheds);
      break;
    case eCombInput:
      std::sort(mInputSchedules->begin(),
                mInputSchedules->end(),
                lessCombScheds);
      break;
    case eCombAsync:
      std::sort(mAsyncSchedules->begin(),
                mAsyncSchedules->end(),
                lessCombScheds);
      break;
    default:
      INFO_ASSERT(0, "Unknown sched type");
  }
}

SCHSchedule::CombinationalLoop
SCHScheduleData::loopCombinational(SCHScheduleType schedType)
{
  SCHSchedule::CombinationalLoop loop;
  switch(schedType) {
    case eCombTransition:
      loop = SCHSchedule::CombinationalLoop(*mTransitionSchedules);
      break;
    case eCombSample:
      loop = SCHSchedule::CombinationalLoop(*mSampleSchedules);
      break;
    case eCombInput:
      loop = SCHSchedule::CombinationalLoop(*mInputSchedules);
      break;
    case eCombAsync:
      loop = SCHSchedule::CombinationalLoop(*mAsyncSchedules);
      break;
    default:
      INFO_ASSERT(0, "Unknown sched type");
  }
  return loop;
}

void
SCHScheduleData::addCombinationalNode(SCHScheduleType schedType,
                                      FLNodeElab* flowElab)
{
  switch(schedType) {
    case eCombForceDeposit:
      mForceDepositSchedule->addCombinationalNode(flowElab);
      break;
    case eCombDebug:
      mDebugSchedule->addCombinationalNode(flowElab);
      break;
    case eCombInitSettle:
      mInitSettleSchedule->addCombinationalNode(flowElab);
      break;
    case eCombInitial:
      mInitialSchedule->addCombinationalNode(flowElab);
      break;
    default:
      FLN_ELAB_ASSERT("Unknown sched type" == NULL, flowElab);
      break;
  }
}

void SCHScheduleData::initAsyncClocks(void)
{
  if (mAsyncClocks == NULL) {
    mAsyncClocks = new SCHClocks;
  }
}

void
SCHScheduleData::addAsyncClock(const NUNetElab* clkElab)
{
  // Make sure we have allocated space for this data
  initAsyncClocks();

  // Add it to the async clocks for all schedules
  mAsyncClocks->insert(clkElab);
} // SCHScheduleData::addAsyncClock

bool SCHScheduleData::isAsyncClock(const NUNetElab* clkElab) const
{
  NU_ASSERT(mAsyncClocks != NULL, clkElab);
  return mAsyncClocks->count(clkElab) > 0;
}

SCHClocksLoop SCHScheduleData::loopAsyncClocks(void) const
{
  // Look for the set of clocks for this schedule. If there aren't
  // any, return an empty loop.
  INFO_ASSERT(mAsyncClocks != NULL,
              "Asynchronous clocks accessed before they were computed");
  return SCHClocksLoop(*mAsyncClocks);
}

