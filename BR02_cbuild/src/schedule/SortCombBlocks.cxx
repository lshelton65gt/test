// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "schedule/Schedule.h"
#include "nucleus/NUUseDefNode.h"
#include "flow/FLNodeElab.h"
#include "util/UtIOStream.h"
#include "Util.h"
#include "Dump.h"
#include "MarkDesign.h"
#include "SortCombBlocks.h"

SCHSortCombBlocks::SCHSortCombBlocks(SCHUtil* util, SCHMarkDesign* mark) :
  mUtil(util), mMarkDesign(mark)
{
  mBlockGroups = new BlockGroups;
  mFanoutCountMap = new FanoutCountMap;
  mBlockNodesMap = new BlockNodesMap;
  mLastInsertedStack = new LastInsertedStack;
}

SCHSortCombBlocks::~SCHSortCombBlocks()
{
  delete mBlockGroups;
  delete mFanoutCountMap;
  delete mBlockNodesMap;
  delete mLastInsertedStack;
}

void
SCHSortCombBlocks::sort(SCHCombinational* comb, bool sortByData)
{
  // Set the sort by data flag for this block
  mSortByData = sortByData;

  // Create a block version of the flow for this schedule. This groups
  // flow nodes from the same use def that will be scheduled as
  // one. It then creates the flow through these groups.
  createBlockFlow(comb);

  // Compute the fanout count for every block group created above. We
  // use the fanout count to decide what to schedule next.
  int blockCount = computeFanout();

  // Initiate the schedule by adding the zero fanout blocks. We start
  // with the outputs and schedule the logic in reverse order.
  FanoutCountMap::iterator p;
  mStackDepth = 0;
  for (p = mFanoutCountMap->begin(); p != mFanoutCountMap->end(); ++p)
    if (p->second == 0)
      addToReadyQueue(p->first);

  // Schedule the blocks groups. We have the zero fanout block groups
  // in our ready queue. We select the next zero fanout block and
  // schedule it. By scheduling it we can decrement the blocks fanin's
  // fanout count. If any of them get to zero, they can be added to
  // the ready queue.
  const BlockGroup* block;
  mLastScheduled = NULL;
  while ((block = getNextBlock()) != NULL)
    // Make sure this block wasn't merged away
    if (!block->getNodes()->empty())
    {
      scheduleBlock(block, comb);
      --blockCount;
    }

  // Make sure we cleared everything
  SCHED_ASSERT(mBlockNodesMap->empty(), comb);
  SCHED_ASSERT(mLastInsertedStack->empty(), comb);
  for (p = mFanoutCountMap->begin(); p != mFanoutCountMap->end(); ++p)
    SCHED_ASSERT(p->second == 0, comb);
  SCHED_ASSERT(blockCount == 0, comb);

  // All done, clear the memory
  mFanoutCountMap->clear();
  for (BlockLoop l = loopBlockGroups(); !l.atEnd(); ++l)
    delete *l;
  mBlockGroups->clear();
} // SCHSortCombBlocks::sort

int
SCHSortCombBlocks::computeFanout()
{
  // Clear the fanout count data for the blocks
  int blockCount = 0;
  for (BlockLoop sLoop = loopBlockGroups(); !sLoop.atEnd(); ++sLoop)
  {
    // Set the fanout count for this block to zero
    BlockGroup* blockFlow = *sLoop;
    FLN_ELAB_ASSERT(mFanoutCountMap->find(blockFlow) == mFanoutCountMap->end(),
                    blockFlow->getRepresentativeFlow());
    (*mFanoutCountMap)[blockFlow] = 0;
    ++blockCount;
  }

  // Compute the fanout count for the blocks by visiting the fanin. We
  // do this as a separate pass from the above so that we know the
  // schedule set flag is set on all the flow nodes we care about.
  for (BlockLoop sLoop = loopBlockGroups(); !sLoop.atEnd(); ++sLoop)
  {
    BlockGroup* blockFlow = *sLoop;
    BlockGroup::BlockFaninLoop dLoop;
    for (dLoop = blockFlow->loopBlockFanin(); !dLoop.atEnd(); ++dLoop)
    {
      // Increment the fanout count if we aren't pointing to ourselves
      BlockGroup* faninBlock = *dLoop;
      if (blockFlow != faninBlock)
	(*mFanoutCountMap)[faninBlock]++;
    }
  } // for
  return blockCount;
} // SCHSortCombBlocks::computeFanout

void
SCHSortCombBlocks::addToReadyQueue(const BlockGroup* block)
{
  // If we are sorting by data then we insert into a stack by the
  // depth from the output flows. We are trying to execute to get good
  // data locality. (A read of a net happens close to a write of a
  // net). Within the depth we also sort by hierarchy.
  //
  // If we are sorting by code then we try to execute different
  // instances of the blocks near each other so we sort by use def.
  if (mSortByData)
  {
    // Create the block instances set if it doesn't already exist
    LastInsertedStack::iterator pos = mLastInsertedStack->find(mStackDepth);
    HierBlocks* blocks;
    if (pos == mLastInsertedStack->end())
    {
      // Create a new set of block instances
      blocks = new HierBlocks;
      mLastInsertedStack->insert(LastInsertedStackValue(mStackDepth, blocks));
    }
    else
      // It already exists
      blocks = pos->second;

    // Add it to the set sorted by hierarchy. These all have the same
    // depth.
    blocks->insert(block);
  }
  else
  {
    // Get the spot to put it, we sort by usedef
    const FLNodeElabVector* nodes = block->getNodes();
    FLNodeElab* flow = *(nodes->begin());
    const NUUseDefNode* useDef = flow->getUseDefNode();

    // Create the block instances set if it doesn't already exist
    BlockNodesMap::iterator pos = mBlockNodesMap->find(useDef);
    BlockInstances* blocks;
    if (pos == mBlockNodesMap->end())
    {
      // Create a new set of block instances
      blocks = new BlockInstances;
      mBlockNodesMap->insert(BlockNodesMap::value_type(useDef, blocks));
    }
    else
      // It already exists
      blocks = pos->second;

    // Add it to the list 
    blocks->insert(block);
  }
}

const SCHSortCombBlocks::BlockGroup*
SCHSortCombBlocks::getNextBlock()
{
  const BlockGroup* block = NULL;
  if (mSortByData)
  {
    if (!mLastInsertedStack->empty())
    {
      // Get the set of blocks with the deepest depth. These are the
      // blocks that were last inserted. This means they are needed to
      // compute something already in the reverse execution list.
      //
      // We sort the stack by reverse depth.
      LastInsertedStack::iterator pos = mLastInsertedStack->begin();
      HierBlocks* blocks = pos->second;

      // Get the first entry of the block instances with the same
      // depth. These are sorted by hierarchy so that we execute flow
      // nodes from the same hierarchy together.
      HierBlocks::iterator iPos = blocks->begin();
      block = *iPos;

      // Update the depth so that we insert any new flow nodes into
      // the ready queue with one depth higher that this one
      mStackDepth = pos->first + 1;

      // Remove the data from the stack
      blocks->erase(iPos);
      if (blocks->empty())
      {
	delete blocks;
	mLastInsertedStack->erase(pos);
      }
    }
  }
  else
  {
    // Try to schedule the same type of block first
    BlockNodesMap::iterator pos;
    if (mLastScheduled != NULL)
      pos = mBlockNodesMap->find(mLastScheduled);
    else
      pos = mBlockNodesMap->end();

    // If we didn't succeed, then get any
    if (pos == mBlockNodesMap->end())
      pos = mBlockNodesMap->begin();

    // Schedule any block of the instances if we found one
    if (pos != mBlockNodesMap->end())
    {
      // Get the set of nodes
      BlockInstances* blocks = pos->second;
      BlockInstances::iterator iPos = blocks->begin();
      block = *iPos;

      // Remember what we did
      mLastScheduled = pos->first;

      // Remove this set of nodes
      blocks->erase(iPos);
      if (blocks->empty())
      {
	delete blocks;
	mBlockNodesMap->erase(pos);
      }
    }
  }

  return block;
} // SCHSortCombBlocks::getNextBlock

void
SCHSortCombBlocks::scheduleBlock(const BlockGroup* block,
				 SCHCombinational* comb)
{
  // Find the canonical flow node for this schedule
  FLNodeElab* schedFlow = NULL;
  const FLNodeElabVector* nodes = block->getNodes();
  FLNodeElabVector::const_iterator pos;
  for (pos = nodes->begin(); pos != nodes->end(); ++pos)
  {
    // See if this is the canonical flow
    FLNodeElab* flow = *pos;
    if (schedFlow == NULL)
      // First item, start with that
      schedFlow = flow;
    else if (FLNodeElab::compare(flow, schedFlow) < 0)
      schedFlow = flow;
  }

  // Schedule this block
  FLN_ELAB_ASSERT(schedFlow != NULL, block->getRepresentativeFlow());
  comb->addCombinationalNode(schedFlow);

  // Visit this blocks fanin and see if we get any more blocks to schedule
  BlockGroup::CBlockFaninLoop dLoop;
  for (dLoop = block->loopBlockFanin(); !dLoop.atEnd(); ++dLoop)
  {
    // Decrement the fanout count (if we are not pointing to
    // ourselves) and schedule it if possible
    BlockGroup* faninBlock = *dLoop;
    if (faninBlock != block)
    {
      (*mFanoutCountMap)[faninBlock]--;
      if ((*mFanoutCountMap)[faninBlock] == 0)
	addToReadyQueue(faninBlock);
      }
  }
} // SCHSortCombBlocks::scheduleBlock

void SCHSortCombBlocks::createBlockFlow(SCHCombinational* comb)
{
  // Make sure we have a clean container
  SCHED_ASSERT(mBlockGroups->empty(), comb);

  // Create the set of block groups for this schedule
  SCHCombinational::BlockScheduleLoop l;
  for (l = comb->getBlockSchedule(); !l.atEnd(); ++l)
  {
    // Add this set of flows to the block groups
    FLNodeElabVector* nodes = *l;
    if (!nodes->empty())
      addBlockGroup(nodes);
  }

  // Create the map from flow nodes to block groups so that we can
  // find which block group a flow node is in below.
  typedef UtMap<FLNodeElab*, BlockGroup*> NodeToBlockMap;
  typedef NodeToBlockMap::iterator NodeToBlockMapIter;
  NodeToBlockMap nodeToBlockMap;
  for (BlockLoop l = loopBlockGroups(); !l.atEnd(); ++l)
  {
    // Add the flows to the map
    BlockGroup* blockGroup = *l;
    FLNodeElabVector* nodes = blockGroup->getNodes();
    for (FLNodeElabVectorIter f = nodes->begin(); f != nodes->end(); ++f)
    {
      FLNodeElab* flow = *f;
      FLN_ELAB_ASSERT(nodeToBlockMap.find(flow) == nodeToBlockMap.end(), flow);
      nodeToBlockMap.insert(NodeToBlockMap::value_type(flow, blockGroup));
    }
  }

  // Go through the blocks again and visit their flow fanin. That way
  // we can create the block fanin.
  for (BlockLoop l = loopBlockGroups(); !l.atEnd(); ++l)
  {
    // Get the nodes in this schedule
    BlockGroup* blockGroup = *l;
    FLNodeElabVector* nodes = blockGroup->getNodes();
  
    // Visit the nodes in this block
    for (FLNodeElabVectorIter f = nodes->begin(); f != nodes->end(); ++f)
    {
      // Visit the fanin for this node
      FLNodeElab* flow = *f;
      SCHMarkDesign::FlowDependencyLoop p;
      for (p = mMarkDesign->loopDependencies(flow); !p.atEnd(); ++p) {
	// If this node is in our set, add it to the fanin for our
	// parent block. Make sure it isn't a dependency to keep the
	// fanin live
	FLNodeElab* fanin = *p;
        NodeToBlockMapIter pos = nodeToBlockMap.find(fanin);
        if (pos != nodeToBlockMap.end())
        {
          BlockGroup* blockFanin = pos->second;
          if (blockFanin != blockGroup)
            blockGroup->addFaninBlock(blockFanin);
        }
      }
    }
  } // for

  // Verify that there are no cycles in the block group flow. All
  // cycles should either be dealt with by the real cycle code
  // (encapsulated), or the false cycles should result in multiply
  // calling a block. If we get any cycles it means something go
  // through. We run this code so that we can find bugs more easily.
  CoveredBlockGroups coveredBlockGroups;
  CoveredBlockGroups inStack;
  BlockGroupStack blockGroupStack;
  for (BlockLoop l = loopBlockGroups(); !l.atEnd(); ++l)
  {
    BlockGroup* blockGroup = *l;
    findCycles(blockGroup, coveredBlockGroups, inStack, blockGroupStack);
  }
} // void SCHSortCombBlocks::createBlockFlow

SCHSortCombBlocks::BlockLoop
SCHSortCombBlocks::loopBlockGroups()
{
  return BlockLoop(*mBlockGroups);
}

void
SCHSortCombBlocks::findCycles(BlockGroup* blockGroup,
			      CoveredBlockGroups& coveredBlockGroups,
			      CoveredBlockGroups& inStack,
			      BlockGroupStack& blockGroupStack)
{
  // Check if we have done this before
  if (coveredBlockGroups.find(blockGroup) != coveredBlockGroups.end())
    return;

  // Check if we found a cycle
  if (inStack.find(blockGroup) != inStack.end())
  {
    // We did, print the error
#define DEBUG_CYCLES
#ifdef DEBUG_CYCLES
    BlockGroup* lastGroup = NULL;
    UtIO::cout() << "The following blocks are in a cycle:\n";
    for (BlockGroupStack::reverse_iterator p = blockGroupStack.rbegin();
	 p != blockGroupStack.rend() && (lastGroup != blockGroup);
	 ++p)
    {
      lastGroup = *p;
      lastGroup->print();
    }
#endif
    INFO_ASSERT(0, "Cycle found during block sort");
  }

  // Add this block group to the stack
  inStack.insert(blockGroup);
  blockGroupStack.push_back(blockGroup);

  // Visit the fanin
  BlockGroup::BlockFaninLoop l;
  for (l = blockGroup->loopBlockFanin(); !l.atEnd(); ++l)
    findCycles(*l, coveredBlockGroups, inStack, blockGroupStack);

  // Remove the block group from the stack
  inStack.erase(blockGroup);
  FLN_ELAB_ASSERT(blockGroupStack.back() == blockGroup,
                  blockGroup->getRepresentativeFlow());
  blockGroupStack.pop_back();

  // Mark this block covered
  coveredBlockGroups.insert(blockGroup);
} // SCHSortCombBlocks::findCycles

void
SCHSortCombBlocks::addBlockGroup(FLNodeElabVector* nodes)
{
  BlockGroup* blockGroup = new BlockGroup(nodes);
  mBlockGroups->push_back(blockGroup);
}

SCHSortCombBlocks::BlockGroup::BlockGroup(FLNodeElabVector* nodes) : mNodes(nodes)
{
  mBlockFanin = new BlockFanin;
}

SCHSortCombBlocks::BlockGroup::~BlockGroup()
{
  delete mBlockFanin;
}

void
SCHSortCombBlocks::BlockGroup::addFaninBlock(BlockGroup* fanin)
{
  mBlockFanin->insert(fanin);
}

SCHSortCombBlocks::BlockGroup::BlockFaninLoop
SCHSortCombBlocks::BlockGroup::loopBlockFanin()
{
  return BlockFaninLoop(*mBlockFanin);
}

SCHSortCombBlocks::BlockGroup::CBlockFaninLoop
SCHSortCombBlocks::BlockGroup::loopBlockFanin() const
{
  return CBlockFaninLoop(*mBlockFanin);
}

void SCHSortCombBlocks::BlockGroup::print() const
{
  UtIO::cout() << "Block (" << this << "):\n";
  for (FLNodeElabVectorIter i = mNodes->begin(); i != mNodes->end(); ++i)
  {
    FLNodeElab* flow = *i;
    UtString flowName;
    SCHDump::composeFlowName(flow, &flowName);
    UtIO::cout() << " - " << flowName << UtIO::endl;
  }

  UInt32 index = 0;
  UtIO::cout() << "Fanin: (";
  for (CBlockFaninLoop l = loopBlockFanin(); !l.atEnd(); ++l)
  {
    const BlockGroup* faninGroup = *l;
    if (index < 6)
    {
      if (index != 0)
	UtIO::cout() << ", ";
      UtIO::cout() << faninGroup;
      ++index;
    }
    else
    {
      UtIO::cout() << "\n       " << faninGroup;
      index = 1;
    }
  }
  UtIO::cout() << ")\n";
}

FLNodeElab* SCHSortCombBlocks::BlockGroup::getRepresentativeFlow() const
{
  if (mNodes->empty()) {
    return NULL;
  } else {
    return *(mNodes->begin());
  }
}

bool
SCHSortCombBlocks::CmpUseDefs::operator()(const NUUseDefNode* ud1,
					  const NUUseDefNode* ud2) const
{
  int cmp = NUUseDefNode::compare(ud1, ud2);
  return (cmp < 0);
}

bool
SCHSortCombBlocks::CmpHierNames::operator()(const BlockGroup* b1,
					    const BlockGroup* b2) const
{
  int cmp;

  // Easy case
  if (b1 == b2)
    cmp = 0;
  else
  {
    const FLNodeElabVector* n1 = b1->getNodes();
    const FLNodeElabVector* n2 = b2->getNodes();
    FLNodeElab* f1 = *(n1->begin());
    FLNodeElab* f2 = *(n2->begin());
    cmp = FLNodeElab::compare(f1, f2);
    FLN_ELAB_ASSERT(cmp != 0, f1);
  }
  return cmp < 0;
} // bool operator

bool
SCHSortCombBlocks::CmpDepth::operator()(const int d1, const int d2) const
{
  return d1 > d2;
}

bool
SCHSortCombBlocks::CmpNetHier::operator()(const BlockGroup* b1,
					  const BlockGroup* b2) const
{
  int cmp;

  // Easy case
  if (b1 == b2)
    cmp = 0;
  else
  {
    // First sort by the net hierarchy, then sort by the flow node
    const FLNodeElabVector* nodes1 = b1->getNodes();
    const FLNodeElabVector* nodes2 = b2->getNodes();
    FLNodeElab* f1 = *(nodes1->begin());
    FLNodeElab* f2 = *(nodes2->begin());
    NUNetElab* n1 = f1->getDefNet();
    NUNetElab* n2 = f2->getDefNet();
    cmp = HierName::compare(n1->getHier(), n2->getHier());
    if (cmp == 0)
      cmp = FLNodeElab::compare(f2, f1);
    FLN_ELAB_ASSERT(cmp != 0, f1);
  }
  return cmp < 0;
}
