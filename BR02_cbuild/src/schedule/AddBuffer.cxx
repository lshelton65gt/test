// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "schedule/Schedule.h"

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUDesign.h"

#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"

#include "localflow/ClockAndDriver.h"

#include "reduce/RETransform.h"

#include "util/CbuildMsgContext.h"
#include "util/Stats.h"

#include "Util.h"
#include "TimingAnalysis.h"
#include "MarkDesign.h"
#include "AddBuffer.h"
#include "ScheduleData.h"

SCHAddBuffer::SCHAddBuffer( SCHTimingAnalysis* timing, SCHMarkDesign* mark,
			    SCHUtil* util, NUNetRefFactory* netRefFactory,
                            bool verbose) :
  mVerbose(verbose), mTimingAnalysis(timing), mMarkDesign(mark), mUtil(util),
  mNetRefFactory(netRefFactory)
{}

SCHAddBuffer::~SCHAddBuffer()
{}

void
SCHAddBuffer::addBuffers(BufferedFlowsMap& bufferedFlowsMap,
			 Callback* callback,
                         SCHScheduleData* scheduleData,
                         bool replaceEdges,
                         NUNetElabSet* delayedNets)
{
  // If there is not work to do, just return
  if (bufferedFlowsMap.empty())
    return;

  // Initiate a statistics interval
  UtStatsInterval stats(mUtil->getStats() != NULL, mUtil->getStats());

  // Create the block to flow map that we need below
  SCHUtil::BlockToNodesMap blockToNodesMap;
  mUtil->findAllBlockNodes(blockToNodesMap);
  stats.printIntervalStatistics("AB Find");

  // First sort the cases by block
  typedef UtMap<NUUseDefNode*, FLNodeElabSet, NUUseDefNodeCmp> BlockToSeqFlowMap;
  BlockToSeqFlowMap blockToSeqFlowMap;
  BufferedFlowsMapIter c;
  for (c = bufferedFlowsMap.begin(); c != bufferedFlowsMap.end(); ++c)
  {
    FLNodeElab* seqFlow = c->first;
    NUUseDefNode* useDef = seqFlow->getUseDefNode();
    FLNodeElabSet& seqFlows = blockToSeqFlowMap[useDef];
    seqFlows.insert(seqFlow);
  }
  stats.printIntervalStatistics("AB Sort");

  // Visit the blocks and gather the input net ref sets we want to
  // buffer. This assumulates all the bits in the first pass. Then in
  // the second pass we can do the buffer insertion on the union of
  // all the require net ref uses.
  NUNetRefSet allBufferNetRefs(mNetRefFactory);
  BlockToSeqFlowMap::iterator s;
  for (s = blockToSeqFlowMap.begin(); s != blockToSeqFlowMap.end(); ++s) {
    // Traverse the sequential flows for this block
    FLNodeElabSet allSeqFlowInstances;
    FLNodeElabSet& seqFlows = s->second;
    for (FLNodeElabSetIter f = seqFlows.begin(); f != seqFlows.end(); ++f) {
      // Get the set of elaborated fanin for this net
      FLNodeElab* seqFlow = *f;
      FLNodeElabSet& instFaninSet = bufferedFlowsMap[seqFlow];

      // Gather the set of input net refs for this always block
      // instance
      getUnelabBufferNets(seqFlow, instFaninSet, replaceEdges, &allBufferNetRefs,
                          NULL);
    }
  }
  stats.printIntervalStatistics("AB Unelab");

  // Visit the blocks and process them one by one. We need a set of
  // sequential flow nodes and the unelaborated nets that need to be
  // buffered.
  //
  // We also store all the sequential flows affected for later
  // processing.
  //
  // This pass uses the information in allBufferNets to get the actual
  // size of the buffered net.
  RETransform* transform = mUtil->getTransform();
  for (s = blockToSeqFlowMap.begin(); s != blockToSeqFlowMap.end(); ++s) {
    FLNodeElabSet allSeqFlowInstances;
    NUNetRefSet inNetRefs(mNetRefFactory);

    // Traverse the sequential flows for this block
    FLNodeElabSet& seqFlows = s->second;
    for (FLNodeElabSetIter f = seqFlows.begin(); f != seqFlows.end(); ++f) {
      // Get the set of clocks used as data for this instance
      FLNodeElab* seqFlow = *f;
      FLNodeElabSet& instFaninSet = bufferedFlowsMap[seqFlow];

      // Gather the set of input net refs for this always block
      // instance
      getUnelabBufferNets(seqFlow, instFaninSet, replaceEdges, &inNetRefs,
                          &allBufferNetRefs);
    }

    // Find all the elaborated flows which use the input nets
    NUUseDefNode* useDef = s->first;
    FlowMatchingUses matchingUses;
    SCHUtil::BlockNodesLoop l;
    for (l = mUtil->loopAllBlockNodes(blockToNodesMap, useDef);
         !l.atEnd(); ++l) {
      // Test if any of the input net refs are used by this def of
      // the always block. If so, it is affected by the operation so
      // add it to the set of flow that must have buffers inserted.
      FLNodeElab* flowElab = *l;
      if (findMatchingUses(flowElab, inNetRefs, &matchingUses)) {
        allSeqFlowInstances.insert(flowElab);
      }
    } // for

    // Call the flow routine to add the intervening buffers. It returns
    // to us the set of new flows
    transform->addBuffer(allSeqFlowInstances, inNetRefs, replaceEdges);

    // We are done with those sets, clear them for the next block
    allSeqFlowInstances.clear();
    inNetRefs.clear();
  }
  stats.printIntervalStatistics("AB Add");

  // Gather clock aliasing information for the flow repair
  ClockAndDriver clockAndDriver;
  mMarkDesign->gatherClockAndDriver(&clockAndDriver);

  // Ask the transform class to fixup the UD and flow
  RETransform::NewFlows newFlows;
  FLNodeElabSet deletedElabFlows;
  NUDesign* design = mUtil->getDesign();
  transform->fixupBufferFlow(design, &newFlows, &deletedElabFlows,
                             &clockAndDriver, mUtil->getStats());
  stats.printIntervalStatistics("AB FlowFix");

  // Remove any deleted flow. This can happen because task inlining
  // can make previously visible nets, invisible.
  scheduleData->removeDeadFlow(deletedElabFlows);
  stats.printIntervalStatistics("AB Dead");

  // Gather the buffers added for fixups below and message printing
  FLNodeElabSet fixupFlows;
  FLNodeElabSet allSeqFlows;
  MsgContext* msgContext = mUtil->getMsgContext();
  SCHMarkDesign::SequentialNodesLoop p;
  NUNetElabMap *bufferedNetMap = mUtil->getDesign()->getBufferedNetMap();
  for (p = mMarkDesign->loopSequentialNodes(); !p.atEnd(); ++p) {
    FLNodeElab* outFlow = *p;
    for (Fanin l = mUtil->loopFanin(outFlow); !l.atEnd(); ++l) {
      FLNodeElab* fanin = *l;
      if (newFlows.find(fanin) != newFlows.end()) {
        FLN_ELAB_ASSERT(deletedElabFlows.find(fanin) == deletedElabFlows.end(),
                        fanin);
	fixupFlows.insert(fanin);
        allSeqFlows.insert(outFlow);

        // Find the original fanin. There may be more than one if
        // the node is driven by more than one flow node. So compare
        // it to the set passed in by our caller.
        FLNodeElabSet& faninSet = bufferedFlowsMap[outFlow];
        FLNodeElab* oldFanin = NULL;
        bool found = false;
        for (Fanin p = mUtil->loopFanin(fanin); !p.atEnd() && !found; ++p) {
          oldFanin = *p;
          found = (faninSet.find(oldFanin) != faninSet.end());
        }

        if (found) {
          // Save the buffered net and its original net.  OnDemand
          // needs this mapping to exclude buffered nets from its
          // state when their original nets have been explicitly
          // excluded by the user via directives.
          NUNetElab *buff = fanin->getDefNet();
          NUNetElab *orig = oldFanin->getDefNet();
          (*bufferedNetMap)[buff] = orig;

          // Check if a message should be printed on this instance
          if (mVerbose) {
            const UtString* str = callback->getReason(outFlow, oldFanin);
            if (str != NULL) {
              HierName* netName = oldFanin->getDefNet()->getSymNode();
              msgContext->SchDelayBufferAdded(outFlow, *netName, str->c_str());
            }
          }
        } // if
      } // if
    } // for
  } // for
  stats.printIntervalStatistics("AB Msg");

  // Fixup the live, input nets, and timing information
  mMarkDesign->computeFlowInputNets();
  mTimingAnalysis->adjustTiming(allSeqFlows, fixupFlows);
  stats.printIntervalStatistics("AB Adjust");

  // Return the delayed nets if they asked us for them
  if (delayedNets != NULL) {
    for (Loop<RETransform::NewFlows> l(newFlows); !l.atEnd(); ++l) {
      FLNodeElab* flowElab = *l;
      delayedNets->insert(flowElab->getDefNet());
    }
  }

  // Make sure everything is sane
  if (mUtil->isFlagSet(eSanity)) { mMarkDesign->sanityCheck(); }
  stats.printIntervalStatistics("AB Clean");

} // SCHAddBuffer::addBuffers

void
SCHAddBuffer::getUnelabBufferNets(FLNodeElab* seqFlow, 
                                  const FLNodeElabSet& instFaninSet,
                                  bool includeEdges,
                                  NUNetRefSet* inNetRefs,
                                  NUNetRefSet* netRefSetUnion)
{
  // Keep a set of input elaborated nets so we can find them
  // through the unelaborated flow below
  UtHashSet<NUNetElab*> netElabSet;
  getElabNets(instFaninSet, &netElabSet);

  // Get the set of net refs that are inputs to this block. We will be
  // replacing all uses of this net.
  NUNetRefSet netRefs(mNetRefFactory);
  getBlockInputs(seqFlow->getFLNode(), includeEdges, &netRefs);

  // Traverse the set of input nets and see which ones are the
  // buffered inputs, we insert them to the map
  STBranchNode* hier = seqFlow->getHier();
  getMatchingNetRefs(hier, netRefs, netElabSet, inNetRefs, netRefSetUnion);
}

void
SCHAddBuffer::getElabNets(const FLNodeElabSet& flowElabs, NUNetElabSet* netElabs)
{
  FLNodeElabSetConstIter p;
  for (p = flowElabs.begin(); p != flowElabs.end(); ++p) {
    FLNodeElab* fanin = *p;	
    netElabs->insert(fanin->getDefNet());
  }
}

void
SCHAddBuffer::getBlockInputs(FLNode* flow, bool includeEdges, NUNetRefSet* netRefs)
{
  // If the caller asked to include edges we walk from the top
  // flow. Otherwise we walk from the first nesting (block).
  FLNode* startFlow = flow;
  if (!includeEdges) {
    startFlow = flow->getSingleNested();
  }

  // Get the continous uses for this block
  NUUseDefNode* useDef = startFlow->getUseDefNode();
  if (useDef->useBlockingMethods()) {
    NUUseDefStmtNode* stmt = dynamic_cast<NUUseDefStmtNode*>(useDef);
    stmt->getBlockingUses(netRefs);
  } else {
    useDef->getUses(netRefs);
  }
} // SCHAddBuffer::getBlockInputs

void
SCHAddBuffer::getMatchingNetRefs(STBranchNode* hier, const NUNetRefSet& netRefs,
                                 const NUNetElabSet& netElabs,
                                 NUNetRefSet* retNetRefs,
                                 NUNetRefSet* netRefSetUnion)
{
  // Walk all the uses for the block
  for (NUNetRefSet::const_iterator n = netRefs.begin(); n != netRefs.end(); ++n) {
    // Check if the unelaborated net is an elaborated use of the
    // always block.
    const NUNetRefHdl& ref = *n;
    NUNet* inNet = ref->getNet();
    NUNetElab* inNetElab = inNet->lookupElab(hier);
    if (netElabs.find(inNetElab) != netElabs.end()) {
      // This is something we want to buffer
      if (netRefSetUnion != NULL) {
        // Get the union of the net refs from the union set
        NUNetRefHdl unionRef = netRefSetUnion->findNet(inNet);
        retNetRefs->insert(unionRef);
      } else {
        // Not taking the union, gather the actual buffered uses
        retNetRefs->insert(ref);
      }
    }
  }
} // SCHAddBuffer::getMatchingNetRefs

bool
SCHAddBuffer::findMatchingUses(FLNodeElab* flowElab, const NUNetRefSet& inNetRefs,
                               FlowMatchingUses* matchingUses)
{
  // See if we have processed this unelaborated flow before
  FLNode* flow = flowElab->getFLNode();
  FlowMatchingUses::iterator pos = matchingUses->find(flow);
  if (pos != matchingUses->end()) {
    return pos->second;
  }
  
  // Get the set of uses for just this def of the use def
  NUNetRefSet uses(mNetRefFactory);
  NUNetRefHdl defNetRef = flow->getDefNetRef();
  NUUseDefNode* useDef = flow->getUseDefNode();
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  NU_ASSERT(always != NULL, useDef);
  always->getBlock()->getBlockingUses(defNetRef, &uses);

  // Check if any of the uses are in the buffer net set
  bool used = false;
  for (NUNetRefSet::iterator n = uses.begin();
       n != uses.end() && !used; ++n) {
    const NUNetRefHdl& ref = *n;
    used = (inNetRefs.find(ref, &NUNetRef::overlapsSameNet) != inNetRefs.end());
  }

  // Remember this unelaborated flow
  matchingUses->insert(FlowMatchingUses::value_type(flow, used));
  return used;
}

