// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implements Sequential block merging
*/

#ifndef _SEQUENTIALBLOCKMERGE_H_
#define _SEQUENTIALBLOCKMERGE_H_

#include "util/LoopMap.h"

#include "BlockMerge.h"

class SCHMergeTest;

//! SCHSequentialBlockMerge class
/*! Implements sequential block merging by using the SCHBlockMerge
 *  class to do most of the work. This class provides the helper
 *  functions to create the block graph, determine what blocks are
 *  mergable, and update schedules after a merge.
 */
class SCHSequentialBlockMerge : public SCHBlockMerge
{
#if ! pfGCC_2
  using UtGraphMerge::nodeMergable;
  using UtGraphMerge::edgeMergable;
#endif

public:
  //! constructor
  SCHSequentialBlockMerge(SCHUtil*, SCHMarkDesign*, SCHScheduleData*,
                          bool reverseEdge = true);

  //! destructor
  ~SCHSequentialBlockMerge();

  //! Class to test if an edge between to flops is a true ordering requirement
  /*! Sequential schedules are executed in a given order. If two flops
   *  from different schedules have an edge that is the opposite of
   *  the scheduler order, then the edge doesn't matter. The
   *  scheduling order guarantees that data does not flow between the
   *  flops in one schedule call.
   */
  class ValidOrder
  {
  public:
    //! constructor
    ValidOrder() {}

    //! Virtual destructor
    virtual ~ValidOrder() {}

    //! Function to determine if the order is valid
    virtual bool operator()(FLNodeElab* fanout, FLNodeElab* fanin) const = 0;
  };

  //! Function to merge sequential blocks in the design
  /*! This function tests to make sure block merging is enabled,
   *  gathers all the combinational schedule blocks, creates the graph
   *  and merges them. If the dump flag is enabled it prints
   *  information about the succesful and failed merges.
   */
  void merge(ValidOrder* validOrder);

protected:
  // This section contains functions that are used/overriden by mixed
  // block merging.

  //! Create merge graph sequential blocks for a given schedule and edge
  void gatherScheduleBlocks(SCHSequential* seq, ClockEdge edge);

  //! Create a block in the merge graph
  virtual FLNodeElabVector* createMergeBlock(FLNodeElab* flowElab,
                                             SCHScheduleBase* sched);

  //! Print the adjacency failures to the output
  void printAdjacencyFailures() const;

  //! Function to move merged blocks from a src vector to a dst vector
  virtual void moveFlows(FLNodeElabVector* dst, FLNodeElabVector* src);

private:
  // This section contains abstract virtual functions that must be
  // defined for the SCHBlockMerge class to do its work.

  //! Override this function to test if a block is mergable
  bool blockMergable(const NUUseDefNode* useDef, MergeBlock* mergeBlock) const;

  //! Override this function to test if two blocks are mergable
  bool edgeMergable(const MergeEdge* edge, const MergeBlock* faninBlock,
                    const MergeBlock* fanoutBlock, bool dumpMergedBlocks) const;

  //! Override this function to sort blocks into mergable buckets
  /*! The two blocks are assumed to have the same depth, this is an
   *  additional sort routine to determine if two blocks can be
   *  merged.
   *
   *  Returns -1,0,1 for an ordering between to blocks
   */
  int compareBuckets(const MergeBlock* block1, const MergeBlock* block2) const;

  //! Override this function to get the block fanin
  void getBlockFanin(MergeBlock* block, BlockSet* faninSet) const;

  //! Override this function to indicate what blocks are valid start points
  /*! This function will be called once per block created. This is
   *  used by the merge by fanin pass to see if we should start from
   *  this node. All blocks are valid start nodes
   */
  bool validStartBlock(const NUUseDefNode*, MergeBlock*) const;

  //! No work to do for pre merge callback
  void preMergeCallback(MergeBlock*, MergeBlock*) { return; }

  //! Override virtual function to update some merged blocks
  /*! The schedules do not need updating, we don't have a block
   *  schedule to update. The flows should now point to new always
   *  blocks.
   */
  void postMergeCallback(MergeBlock* lastBlock, MergeBlock* block);

  //! Virtual function to record a broken up block
  /*! Record this for the dumped statistics
   */
  virtual void recordCycleBlock(const MergeBlock* block) const;

private:
  // This section contains private types and data to perform the
  // merge.

  //! Abstraction to keep track of all the sequential scheduled block
  typedef UtMap<SCHUseDefHierPair, FLNodeElabVector*> ScheduledBlocks;

  //! Abstraction to iterate over a vector of schedule blocks
  typedef LoopMap<ScheduledBlocks> ScheduledBlocksLoop;

  //! Map from block instance to all the elaborated flow for it
  ScheduledBlocks* mScheduledBlocks;

  //! Abstraction to keep track of a sequential schedule and edge
  typedef std::pair<SCHSequential*, ClockEdge> SeqAndEdge;

  //! Abstraction to keep track of what edges an elaborated flow is in
  typedef UtMap<FLNodeElab*, SeqAndEdge> ElabFlowSchedules;

  //! Map to quickly get the clock edge for any sequential elaborated flow
  ElabFlowSchedules* mElabFlowSchedules;

  //! Function to initialize local data needed before merging
  void initLocalData();

  //! Function to test if two blocks are in different sub schedules
  int compareHierSchedules(const MergeBlock* b1, const MergeBlock* b2) const;

  //! Abstraction to keep track of seq block instance schedule and edge
  typedef UtMap<STBranchNode*, SeqAndEdge> HierSchedules;

  //! Function to compute a blocks hier schedules
  void computeHierSchedules(const MergeBlock* block, HierSchedules* hierScheds)
    const;

  //! Enumeration of the possible adjacency merge failures
  enum AdjMergeFail {
    eUnmergable,        //!< The fanin block is unmergable
    eCrossesHier,       //!< The two blocks are in different hierarchies
    eUnmatchHierSched,  //!< The hierarchy schedule/edge pair don't match
    eNumAdjMergeFails   //!< The number of possible failures
  };

  //! Keep stats on failed adjacency merges
  mutable int mAdjMergeFails[eNumAdjMergeFails];

  //! Record and print info on the failed merge
  void recordFailedMerge(const MergeBlock* faninBlock,
                         const MergeBlock* fanoutBlock,
                         AdjMergeFail reason) const;

  //! Get printable text for a failed merge reason
  const char* adjFailReason(AdjMergeFail adjMergeFail) const;

  //! Print all the nodes in a given block
  void printBlockNodes(const FLNodeElabVector& nodes) const;

  //! Function to print statistics if the dump flag is set
  void printResults();

  //! Function to determine if a sequential edge is needed for ordering
  ValidOrder* mValidOrder;

  //! Class to compute/keep track of mergable/unmergable blocks
  SCHMergeTest* mMergeTest;
}; // class SCHSequentialBlockMerge : public SCHBlockMerge

#endif // _SEQUENTIALBLOCKMERGE_H_
