// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  A class to keep merge combinational nodes for the same block instance
*/

#ifndef _READY_H_
#define _READY_H_

#include "util/LoopMap.h"
#include "UseDefHierPair.h"

struct SCHComboScheduleStats;

//! SCHReady class
/*! This implements a class that allows the scheduler to re-sort
 *  combinational nodes such that nodes for the same always block can
 *  be merged.
 *
 *  It does this by using a ready queue that allows the caller to
 *  insert blocks for scheduling and only returns them to the
 *  scheduler when all flow nodes for an always block are ready to
 *  schedule.
 *
 *  This class can be re-used for many schedules. For each schedule
 *  the caller should do:
 *
 *  - call SCHReady::addFlowNode on all combinational blocks for the
 *    schedule.
 *
 *  - call SCHReady::initiateSchedule to start the process.
 *
 *  - call SCHReady::getNodesToSchedule in a loop until it returns
 *  false. This routine will return a group of nodes (FLNodeElabVector) to
 *  schedule. Each group contains flow nodes for the same always
 *  block. For each flow node in the stack the caller should call
 *  SCHReady::markAsScheduled so that the ready queue can add more
 *  nodes as available for scheduling.
 *
 *  - When SCHReady::getNodesToSchedule returns false, call
 *    SCHReady::cleanUp() to finish the process.
 */
class SCHReady
{
public:
  //! constructor
  SCHReady(SCHUtil* util, SCHMarkDesign* mark, SCHDump* dump);

  //! destructor
  ~SCHReady();

  //! adds a flow node to the set for scheduling.
  /*! This does not try to schedule anything yet. It is simply getting
   *  a list of all the flow nodes and putting them in the fanout
   *  count and usedef count maps.
   *
   *  \param flow The flow node to add.
   */
  void addFlowNode(FLNodeElab* flow);

  //! initiates the schedule set.
  /*! This should be done as soon as all the flow nodes have been
   *  added. It computes the fanout count map and puts the first set
   *  of flow nodes with zero fanout count into the queues.
  */
  void initiateSchedule(MsgContext* mc);

  //! Get a set of nodes to insert into the schedule.
  /*! This routine gets a set of nodes to be schedule in a
   *  FLNodeElabVector. All the nodes in this stack are guaranteed to have
   *  the same use def and hier name. This means only one of the flow
   *  nodes needs to be put in the schedule.
   *
   *  It is up to the caller to call markAsScheduled on every flow node
   *  in this stack. This initiates further scheduling.
   *
   *  \param returnNodes - an empty node stack that gets filled by
   *  this function using the container swap function.
   *
   *  \param dumpUnmergedBlocks - if set, print to stdout the set of
   *  flow blocks that were not merged
   *
   *  \param title - A title to print if dumpUnmergedBlocks is
   *  set. This is a pointer to a string. The function clears the
   *  UtString when it has printed it so that it knows it doesn't have
   *  to print it again.
   */
  bool getNodesToSchedule(FLNodeElabVector* returnNodes, bool dumpUnmergedBlocks,
			  UtString* title);

  //! mark a flow node as scheduled.
  /*! This routine should be called on all the nodes returned by
   *  getNodesToSchedule(). This clears the way for scheduling any new
   *  nodes that feed the flow node passed to this routine.
   *
   *  \param flow The flow node to mark as scheduled.
   */
  void markAsScheduled(FLNodeElab* flow);

  //! clean up after a schedule has been completed
  /*! This routine cleans up the memory used in this process. It
   *  should be called before the process is repeated on another
   *  schedule.
   */
  void cleanUp();

  // Abstraction for which queue a use def/hier pair is in
  enum QueueStatus
  {
    eInNone,
    eInIncompleteQueue,
    eInCompleteQueue
  };


private:
  // Hide copy and assign constructors.
  SCHReady(const SCHReady&);
  SCHReady& operator=(const SCHReady&);

  // compare two flow nodes by use def and hierarchy
  struct CmpFlowInstance
  {
    bool operator()(const FLNodeElabVector* ns1, const FLNodeElabVector* ns2) const;
  };

  // compare two use def nodes
  struct CmpUseDefs
  {
    bool operator()(const NUUseDefNode* ud1, const NUUseDefNode* ud2) const;
  };

  // Abstraction for a set of flow nodes with the same use def sorted by hier
  typedef UtSet<FLNodeElabVector*, CmpFlowInstance> NodesSet;

  // Class to sort incomplete nodes by a priority
  struct Priority
  {
    //! constructor
    Priority(UInt32 remainingCount, UInt32 totalCount, UInt32 defCount,
	     UInt32 fanoutCount)
    {
      mRemainingCount = remainingCount;
      mTotalCount = totalCount;
      mDefCount = defCount;
      mFanoutCount = fanoutCount;
    }

    //! less operator
    bool operator<(const Priority& p1) const
    {
      int cmp;

      // Want the smaller remaining count
      cmp = mRemainingCount - p1.mRemainingCount;
      if (cmp == 0)
      {
	// Don't want fanout counts of zero first
	cmp = p1.mFanoutCount - mFanoutCount;
	if ((cmp != 0) && (mFanoutCount != 0))
	    cmp = 0;

	if (cmp == 0)
	{
	  // Want the smaller def count
	  cmp = mDefCount - p1.mDefCount;
	  if (cmp == 0)
	  {
	    // Want the smaller total count
	    cmp = mTotalCount - p1.mTotalCount;
	  }
	}
      }
      return cmp < 0;
    } // bool operator<

    UInt32 mRemainingCount;
    UInt32 mTotalCount;
    UInt32 mDefCount;
    UInt32 mFanoutCount;
  };

  // Abstraction to keep a priority list of NodesSets (worst queue)
  typedef UtMap<Priority, NodesSet*> NodesSetPriorityQueue;

  // Abstraction to keep a set of FLNodeElabVector for the same use def and
  // hier. It assumes that the nodes in this map all have the same
  // usedef. We may want to make this a hash_map once we know it
  // matters for performance.
  typedef UtMap<const HierName*, FLNodeElabVector*, HierNameCmp> NodesHierMap;

  // Abstraction to map use def nodes to sets of flow nodes for
  // them. The flow nodes are organized by hierachy in NodesHierMap.
  // We may want to make this a hash_map once we know it matters for
  // performance.
  typedef UtMap<const NUUseDefNode*, NodesHierMap*, CmpUseDefs> NodesUseDefMap;

  // Abstraction to maintain the fanout count for a node(sorted). We may want
  // to make this a hash_map once we know it matters for performance.
  typedef UtMap<FLNodeElab*, int, FLNodeElabCmp> FanoutCountMap;

  // Abstraction to visit all the flow nodes for this schedule.
  typedef LoopMap<FanoutCountMap> FanoutLoop;

  friend struct UseDefInfo;

  // Abstraction to keep track of the set of flow nodes for a use def
  struct UseDefInfo
  {
    QueueStatus queueStatus : 8;
    UInt32 count : 24;
    UInt32 totalCount : 16;
    UInt32 scheduleCount : 16;
    UInt32 defCount : 16;
    UInt32 fanoutCount : 16;
  };

  // Abstraction to maintain the remaining flow nodes for usedef/hier pair
  typedef UtMap<SCHUseDefHierPair, UseDefInfo*> UseDefHierMap;

  // Function to get an iterator on all the flow nodes
  FanoutLoop getFanoutNodes();
  
  // adds a flow node to the correct ready queue
  void addToReadyQueue(FLNodeElab*);

  // adds a Node stack to the incomplete queue (for the first time)
  void addToIncompleteQueue(UInt32, UInt32, UInt32, UInt32, FLNodeElabVector*);

  // Remove the node stack because we are going to move it
  FLNodeElabVector* removeFromIncompleteQueue(FLNodeElab*, UInt32, UInt32, UInt32,
					  UInt32);

  // adds a node stack to the complete queue
  void addToCompleteQueue(const NUUseDefNode*, const HierName*, FLNodeElabVector*);

  // Routines to search and return a node stack if they find
  // one. There is one routine per queue.
  FLNodeElabVector* removeFromCompleteQueue();
  FLNodeElabVector* searchIncompleteQueue();

  // Those &&*#* bound nodes are making me write more code again... ;-)
  void countFanout(FLNodeElab*);
  void decrementFanout(FLNodeElab*);

  // Dumping functions
  static void printNode(FLNodeElab*, UseDefInfo*, UInt32, UInt32, const char*);
  static void printNodes(FLNodeElabVector*, UseDefInfo*, UInt32, const char*);

  // Debug functions
  void findCycles(FLNodeElab* flow, FLNodeElabSet& covered,
		  FLNodeElabSet& seen, FLNodeElabVector& nodeStack);

  // Functions to manage cycle represenative node
  FLNodeElab* getRepresentativeFlow(FLNodeElab* flowElab);

  // Function For getting a usedef hier entry's data
  inline UseDefInfo* getUseDefHierEntry(FLNodeElab* flow);

  // The following are maps to keep track of the remaining flow nodes
  // for a given use def node. We need to know how many are remaining
  // for one hierarchy (always block with multiple outputs) and how
  // many remaining for all hierarchies (multiple instances of the
  // always block and multi-output always blocks).
  UseDefHierMap* mUseDefHierMap;

  // This member is use to keep track of the fanout count remaining
  // per flow node. This allows us to tell when a flow node is ready
  // for scheduling.
  //
  // We could keep this information in the flow node but we are trying
  // to save space in them since there are so many of them. The idea
  // is that this map is smaller than the number of flow nodes in the
  // design since it is only for one combinational schedule at a
  // time. But if the time to look this up becomes expensive (log (n))
  // then we shold rethink this decision.
  FanoutCountMap* mRemainingFanoutCountMap;
  FanoutCountMap* mFanoutCountMap;

  // The following members are a two tiered set of queues to place
  // flow nodes that are ready to schedule (fanout count == 0). The
  // first queue is for flow nodes for which we have all the flow
  // nodes for its use def node/hiername pair. These can be scheduled
  // immediately. The second queue is for the flow nodes for which we
  // don't have all the nodes for their use def/hiername pair.
  //
  // The incomplete queue is a priority queues in that it is made up
  // of multiple sub-queues. Each sub-queue is indexed by the number
  // of remaining flow nodes for the use def. An index of 0 indicates
  // there are none remaining. The complete queue only has one sub
  // queue since there are no flow nodes remaining (that block
  // instance is complete).
  NodesUseDefMap* mReadyCompleteQueue;
  NodesSetPriorityQueue* mReadyIncompleteQueue;

  //! Map to keep track of a representative elaborated flow for every cycle
  /*! It is not a good idea to schedule cycles more than once. So
   *  using multiple elaborated flows to all defs of a cycle does not
   *  help. In fact it causes a compile performance problem and
   *  potentially a run-time performance problem.
   *
   *  This map causes the compiler to use a single elaborated flow to
   *  represent all elaborated flow in the cycle.
   */
  typedef UtMap<FLNodeElab*, FLNodeElab*> CycleRepresentativeFlow;

  //! Storage for the Cycle Representative Flow Map
  CycleRepresentativeFlow* mCycleRepresentativeFlow;

  // Utility classes
  SCHUtil* mUtil;
  SCHMarkDesign* mMarkDesign;
  SCHDump* mDump;

//#define DEBUG_READY_PERF
#ifdef DEBUG_READY_PERF
  // The number of unmerged blocks outstanding. This allows us to
  // print good nodes inbetween unmerged blocks.
  UInt32 mNumOutstandingBlocks;
#endif
}; // class SCHReady

#endif // _READY_H_
