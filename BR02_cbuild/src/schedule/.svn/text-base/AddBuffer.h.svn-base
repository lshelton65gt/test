// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _ADDBUFFER_H_
#define _ADDBUFFER_H_

class SCHTimingAnalysis;
class SCHMarkDesign;
class SCHUtil;
class NUNetRefFactory;

//! class SCHAddBuffer
/*! This is a utility class to add a buffer between two sets of flow
 *  nodes. This is needed to delay values so that we can schedule the
 *  logic correctly.
 */
class SCHAddBuffer
{
public:
  //! constructor
  SCHAddBuffer(SCHTimingAnalysis* timing, SCHMarkDesign* mark, SCHUtil* util,
               NUNetRefFactory* netRefFactory, bool verbose);

  //! destructor
  ~SCHAddBuffer();

  //! Abstraction to hold the flow nodes that need to be buffered
  /*! This is a map from all the input flows that need to be buffered
   *  to the sets of flow nodes that need to use the buffered value.
   */
  typedef UtMap<FLNodeElab*, FLNodeElabSet> BufferedFlowsMap;

  //! A message callback class
  /*! Use this class to get called on every buffer added. This is used
   *  to generate a reason when printing a message.
   */
  class Callback
  {
  public:
    //! constructor
    Callback() {}

    //! destructor
    virtual ~Callback() {}

    //! Routine to get a reason for a buffer added
    /*! Override this function to return the reason for this delay
     *  buffer. The function should return NULL if a message should
     *  not be printed.
     */
    virtual const UtString* getReason(FLNodeElab* fanout, FLNodeElab* fanin)
      const = 0;
  };

  //! Routine to add the buffers
  void addBuffers(BufferedFlowsMap& bufferedFlowsMap, Callback* callback,
                  SCHScheduleData* scheduleData, bool replaceEdges,
                  NUNetElabSet* delayedNets);

private:
  // Hide copy and assign constructors.
  SCHAddBuffer(const SCHAddBuffer&);
  SCHAddBuffer& operator=(const SCHAddBuffer&);

  //! Get all the unelaborated buffered net refs from the elaborated buffered nets.
  void getUnelabBufferNets(FLNodeElab* seqFlow, const FLNodeElabSet& instFaninSet,
                           bool includeEdges, NUNetRefSet* inNetRefs,
                           NUNetRefSet* netRefSetUnion);

  //! Helper: get all the elaborated nets for a given set of elaborated flow
  void getElabNets(const FLNodeElabSet& flowElabs, NUNetElabSet* netElabs);

  //! Helper: get all the uses for a given set of elaborated flow
  void getBlockInputs(FLNode* flow, bool includeEdges, NUNetRefSet* useNetRefs);

  //! Helper: return intersection of unelaborated net refs and elaboratd nets
  void getMatchingNetRefs(STBranchNode* hier, const NUNetRefSet& netRefs,
                          const NUNetElabSet& netElabs, NUNetRefSet* retNetRefs,
                          NUNetRefSet* netRefSetUnion);

  //! Abstraction to cache matching uses by unelaborated flow
  typedef UtMap<FLNode*, bool> FlowMatchingUses;

  //! Helper: test if a flowElab uses the nets that need to be buffered
  bool findMatchingUses(FLNodeElab* flowElab, const NUNetRefSet& inNetRefs,
                        FlowMatchingUses* matchingUses);

  // Local types and data
  typedef BufferedFlowsMap::iterator BufferedFlowsMapIter;
  bool mVerbose;

  // Utility classes
  SCHTimingAnalysis* mTimingAnalysis;
  SCHMarkDesign* mMarkDesign;
  SCHUtil* mUtil;
  NUNetRefFactory* mNetRefFactory;
}; // class SCHAddBuffer

#endif // _ADDBUFFER_H_
