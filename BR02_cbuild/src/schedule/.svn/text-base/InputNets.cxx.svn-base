// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/
#include "util/SetOps.h"
#include "util/UtIOStream.h"

#include "schedule/Schedule.h"

#include "InputNets.h"

SCHInputNetsFactory::SCHInputNetsFactory()
{
  mUniqueNetSets = new UniqueNetSets;
}

SCHInputNetsFactory::~SCHInputNetsFactory()
{
  UniqueNetSetsIter p;
  for (p = mUniqueNetSets->begin(); p != mUniqueNetSets->end(); ++p)
  {
    delete *p;
  }
  delete mUniqueNetSets;
}

const SCHInputNets* SCHInputNetsFactory::emptyInputNets()
{
  SCHInputNets srchNetSet;
  return getNetSet(&srchNetSet);
}

const SCHInputNets* SCHInputNetsFactory::buildInputNets(const NUNetElab* net)
{
  // Create a net set with this net in it. We use this to do a find
  SCHInputNets srchNetSet;
  srchNetSet.insert(net);

  // Find or create a unique net set
  return getNetSet(&srchNetSet);
}

const SCHInputNets*
SCHInputNetsFactory::appendInputNets(const SCHInputNets* n1,
                                     const SCHInputNets* n2)
{
  // Check for simple case where they are the same
  if (n1 == n2)
    return n1;

  // Check if either is NULL
  if (n1 == NULL)
    return n2;
  else if (n2 == NULL)
    return n1;

  // Check if either is empty
  if (n2->empty())
    return n1;
  else if (n1->empty())
    return n2;

  // Create a net set to search with
  SCHInputNets srchNetSet(n1->begin(), n1->end());
  srchNetSet.insert(n2->begin(), n2->end());

  // Find or create a unique net set
  return getNetSet(&srchNetSet);
}

const SCHInputNets*
SCHInputNetsFactory::inputNetsDifference(const SCHInputNets* ns1,
                                         const SCHInputNets* ns2)
{
  SCHInputNets diffNetSet;
  set_difference(*ns1, *ns2, &diffNetSet);
  return getNetSet(&diffNetSet);
}

const SCHInputNets*
SCHInputNetsFactory::getNetSet(const SCHInputNets* netSet)
{
  // Find if it exists, if not, create it
  UniqueNetSetsIter pos = mUniqueNetSets->find(netSet);
  if (pos == mUniqueNetSets->end())
  {
    // Create this new set
    SCHInputNets* newNetSet = new SCHInputNets(netSet->begin(), netSet->end());
    mUniqueNetSets->insert(newNetSet);
    return newNetSet;
  }
  else
    // Found it
    return *pos;
}


int
SCHInputNetsFactory::compare(const SCHInputNets* ns1, const SCHInputNets* ns2)
{
  if (ns1 == ns2)
    return 0;

  // Compare by sizes first
  size_t s1 = ns1->size();
  size_t s2 = ns2->size();
  if (s1 < s2)
    return -1;
  else if (s2 < s1)
    return 1;

  // Same number of nets, compare them
  SCHInputNets::const_iterator n1 = ns1->begin();
  SCHInputNets::const_iterator n2 = ns2->begin();
  int cmp = 0;
  while ((cmp == 0) && (n1 != ns1->end()))
  {
    // Compare these two nets
    const NUNetElab* net1 = *n1;
    const NUNetElab* net2 = *n2;
    cmp = NUElabBase::compare(net1, net2);

    // Move on to the next set
    ++n1;
    ++n2;
  }
  return cmp;
} // SCHInputNetsFactory::compare

void
SCHInputNetsFactory::print(const SCHInputNets* inputNets, bool newLine)
{
  const char* prefix = "(";
  if (inputNets != NULL) {
    for (SCHInputNetsCLoop p(*inputNets); !p.atEnd(); ++p) {
      UtIO::cout() << prefix;
      prefix = " or ";

      const NUNetElab* netElab = *p;
      NUNet* net = netElab->getNet();
      UtString str;
      net->compose(&str, netElab->getHier());
      UtIO::cout() << str;
    }
  }
  UtIO::cout() << ")";
  if (newLine) UtIO::cout() << "\n";
}

bool
SCHCmpInputNets::operator()(const SCHInputNets* ns1, const SCHInputNets* ns2)
  const
{
  return SCHInputNetsFactory::compare(ns1, ns2) < 0;
}

