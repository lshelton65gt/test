// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _SORTSCHEDULES_H_
#define _SORTSCHEDULES_H_

//! class SCHSortSchedules
/*! This class implements a ready queue to sort a bunch of schedules
 *  based on two things: the dependencies between the schedules
 *  (fanout map) and the input nets the schedule is sensitive to. This
 *  allows us to sort schedules so that we can add guards around a
 *  large set of them.
 */
class SCHSortSchedules
{
public:
  //! constructor
  SCHSortSchedules(SCHMarkDesign* mark);

  //! destructor
  virtual ~SCHSortSchedules();

  //! Routine to add a schedule to this sorting class.
  /*! Call this routine to initialize the data. Once all the schedules
   *  have been added, then add, the fanout for them.
   */
  void addSched(SCHScheduleBase* sched, const SCHInputNets* inputNets);

  //! Routine to add a fanout for a given schedule
  /*! This should be called after the schedule has been added above
   */
  void addFanout(SCHScheduleBase* sched, SCHScheduleBase* fanoutSched);

  //! Abstraction to return DCL schedules that are ready to schedule
  typedef UtVector<SCHScheduleBase*> ScheduleGroup;

  //! Abstraction to iterate over DCL schedules that are ready to schedule
  typedef Loop<ScheduleGroup> ScheduleGroupLoop;

  //! Routine to start scheduling schedules
  /*! Call this routine once all the information has been populated
   *  with addSched and addFanout. Call it until it returns NULL.
   */
  bool getNextSchedule(ScheduleGroup&);

  //! Return an iterator over a group
  ScheduleGroupLoop loopSchedules(ScheduleGroup&) const;

protected:
  //! Abstraction for a vector of schedules
  typedef UtVector<SCHScheduleBase*> ScheduleVector;

  //! Function to print information about a cycle
  virtual void printCycle(const ScheduleVector& schedules);

private:
  // Hide copy and assign constructors.
  SCHSortSchedules(const SCHSortSchedules&);
  SCHSortSchedules& operator=(const SCHSortSchedules&);

  // A set of schedules and iterator
  typedef UtSet<SCHScheduleBase*> Schedules;
  typedef Loop<Schedules> SchedulesLoop;

  // Types, data, and functions to keep track of the input nets and
  // fanout for each schedule
  struct ScheduleData
  {
    Schedules* mFanout;
    const SCHInputNets* mInputNets;
    int mFaninCount;
  };
  typedef UtMap<SCHScheduleBase*, ScheduleData*> SchedulesData;
  typedef LoopMap<SchedulesData> SchedulesDataLoop;
  typedef SchedulesData::value_type SchedulesDataValue;
  SchedulesData* mSchedulesData;
  void addScheduleInputNets(SCHScheduleBase*, const SCHInputNets*);
  const SCHInputNets* getScheduleInputNets(SCHScheduleBase*);
  SchedulesDataLoop loopSchedules();
  ScheduleData* findScheduleData(SCHScheduleBase*);
  void addScheduleFanout(SCHScheduleBase*, SCHScheduleBase*);
  int fanoutCount(SCHScheduleBase*);
  void incrementFaninCount(SCHScheduleBase*);
  int decrementFaninCount(SCHScheduleBase*);
  int getFaninCount(ScheduleData*);
  SchedulesLoop loopFanout(SCHScheduleBase*);

  // Types, data, and functions for keep track of the schedules by
  // input nets
  struct InputNetsData
  {
    Schedules* mSchedules;
    int mTotalCount;
    int mRemainingCount;
  };
  typedef UtMap<const SCHInputNets*, InputNetsData*> InputNetsSchedules;
  typedef InputNetsSchedules::value_type InputNetsSchedulesValue;
  InputNetsSchedules* mInputNetsSchedules;
  InputNetsData* findInputNetsData(const SCHInputNets*);
  void addInputNetsSchedule(const SCHInputNets*, SCHScheduleBase*);
  void removeInputNetsSchedules(const SCHInputNets*, SCHScheduleBase*,
				int*, int*);

  // Class to sort incomplete schedule sets by a priority
  struct Priority
  {
    //! constructor
    Priority(UInt32 remainingCount, UInt32 totalCount, UInt32 fanoutCount)
    {
      mRemainingCount = remainingCount;
      mTotalCount = totalCount;
      mFanoutCount = fanoutCount;
    }

    //! less operator
    bool operator<(const Priority& p1) const
    {
      int cmp;

      // Want the smaller remaining count
      cmp = mRemainingCount - p1.mRemainingCount;
      if (cmp == 0)
      {
	// Don't want fanout counts of zero first
	cmp = p1.mFanoutCount - mFanoutCount;
	if ((cmp != 0) && (mFanoutCount != 0))
	    cmp = 0;

	if (cmp == 0)
	{
	  // Want the smaller total count
	  cmp = mTotalCount - p1.mTotalCount;
	}
      }
      return cmp < 0;
    } // bool operator<

    //! Operator to decrement the remaining count
    void decrementRemaining()
    {
      INFO_ASSERT(mRemainingCount > 0,
                  "Failure while trying to sort combinational blocks");
      --mRemainingCount;
    }

    UInt32 mRemainingCount;
    UInt32 mTotalCount;
    UInt32 mFanoutCount;
  };

  // Type, data, and functions to keep track of the priority queue of
  // schedule sets
  struct CmpInputNets
  {
    bool operator()(const SCHInputNets* ns1, const SCHInputNets* ns2) const;
  };
  typedef UtMap<const SCHInputNets*, Schedules*, CmpInputNets> ReadyScheds;
  typedef ReadyScheds::value_type ReadySchedsValue;
  typedef UtMap<Priority, ReadyScheds*> PriorityQueue;
  typedef PriorityQueue::value_type PriorityQueueValue;
  PriorityQueue* mPriorityQueue;
  void addReadySchedule(SCHScheduleBase*, const SCHInputNets*);
  void removeReadySchedules(ScheduleGroup&);
  void initiateScheduling();
  void findCycle(SCHScheduleBase*, Schedules&, Schedules&, ScheduleGroup&);
#ifdef DEBUG_SORT_SCHED
  void printQueue(PriorityQueue* priorityQueue);
#endif

  // If set, the scheduling has started
  bool mSchedulingStarted;

  // Utility classes
  SCHMarkDesign* mMarkDesign;

}; // class SCHSortSchedules

#endif // _SORTSCHEDULES_H_
