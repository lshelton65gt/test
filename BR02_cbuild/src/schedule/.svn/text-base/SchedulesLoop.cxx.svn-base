// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "schedule/Schedule.h"

#include "CombinationalSchedules.h"
#include "ScheduleData.h"

/*!
  \file
  An iteration class to traverse through all the schedules
*/

SCHSchedulesLoop::SCHSchedulesLoop(SCHScheduleData* scheduleData,
				   SCHScheduleTypeMask typeMask)
{
  // Note: the ordering here is an attempt at ordering based on frequency
  // of execution.

  // Add the DCL combinationals
  if ((typeMask & (eMaskCombDCL|eMaskSequentialDCL)) != 0) {
    SCHSchedule::DerivedClockLogicLoop d;
    for (d = scheduleData->loopDerivedClockLogic(); !d.atEnd(); ++d) {
      SCHDerivedClockBase* dclBase = *d;
      if ((typeMask & eMaskSequentialDCL) != 0) {
        SCHDerivedClockLogic::SequentialLoop s;
        for (s = dclBase->loopMultiSequential(); !s.atEnd(); ++s) {
          SCHSequential* seq = *s;
          mSchedules.push_back(seq);
        }
      }
      if ((typeMask & eMaskCombDCL) != 0) {
        SCHCombinationals combs;
        dclBase->getCombinationalSchedules(&combs);
	mSchedules.insert(mSchedules.end(), combs.begin(), combs.end());
      }
    }
  }

  // Add the sequential schedules
  if ((typeMask & eMaskSequential) != 0)
  {
    SCHSequentials::SequentialLoop p;
    for (p = scheduleData->loopSequential(); !p.atEnd(); ++p)
      mSchedules.push_back(*p);
  }

  // Add the transition/sample combinationals
  SCHSchedule::CombinationalLoop p;
  if ((typeMask & eMaskCombTransition) != 0)
  {
    for (p = scheduleData->loopCombinational(eCombTransition); !p.atEnd(); ++p)
      mSchedules.push_back(*p);
  }
  if ((typeMask & eMaskCombSample) != 0)
  {
    for (p = scheduleData->loopCombinational(eCombSample); !p.atEnd(); ++p)
      mSchedules.push_back(*p);
  }

  // Add the input and async combinationals
  if ((typeMask & eMaskCombInput) != 0)
  {
    SCHSchedule::CombinationalLoop l;
    for (l = scheduleData->loopCombinational(eCombInput); !l.atEnd(); ++l)
    {
      SCHCombinational* input = *l;
      mSchedules.push_back(input);
    }
  }
  if ((typeMask & eMaskCombAsync) != 0)
  {
    SCHSchedule::CombinationalLoop l;
    for (l = scheduleData->loopCombinational(eCombAsync); !l.atEnd(); ++l)
    {
      SCHCombinational* async = *l;
      mSchedules.push_back(async);
    }
  }

  // Add the special run once schedules
  if ((typeMask & eMaskCombInitial) != 0)
    mSchedules.push_back(scheduleData->getInitialSchedule());
  if ((typeMask & eMaskCombInitSettle) != 0)
    mSchedules.push_back(scheduleData->getInitSettleSchedule());
  if ((typeMask & eMaskCombDebug) != 0)
    mSchedules.push_back(scheduleData->getDebugSchedule());
  if ((typeMask & eMaskCombForceDeposit) != 0)
    mSchedules.push_back(scheduleData->getForceDepositSchedule());

  // Start at the beginning
  mIndex = 0;
}

SCHCombinational* SCHSchedulesLoop::getCombinationalSchedule() const
{
  SCHCombinational* comb = dynamic_cast<SCHCombinational*>(getScheduleBase());
  return comb;
}

SCHSequential* SCHSchedulesLoop::getSequentialSchedule() const
{
  SCHSequential* seq = dynamic_cast<SCHSequential*>(getScheduleBase());
  return seq;
}

SCHSchedulesLoop::Iterator SCHSchedulesLoop::getSchedule() const
{
  // Create the iterator
  Iterator iter;

  // Check if we have a combinational
  SCHCombinational* comb = getCombinationalSchedule();
  if (comb != NULL)
    iter.pushLoop(comb->getSchedule());
  else
  {
    // Must be a sequential schedule
    SCHSequential* seq = getSequentialSchedule();
    SCHED_ASSERT(seq != NULL, getScheduleBase());

    // Get both schedules
    iter.pushLoop(seq->getSchedule(eClockPosedge));
    iter.pushLoop(seq->getSchedule(eClockNegedge));
  }

  return iter;
}
