// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  Implementation of the block splitting processes. Blocks may need to
  be split because of timing reasons (statements in the block are in
  different schedules) or because the combinational block is part of a
  false cycle and breaking up the block breaks the cycle.

  This implementation has routines to:

  1. Find if a block is in more than one schedule.

  2. Mark the blocks that should be split.

  3. Attempt to split the blocks that have been marked and remove them
     from the need to split list.

  4. Traverse the set of blocks that should be split.

  5. TBD
*/

#include "schedule/Schedule.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Signature.h"

#include "util/HierName.h"
#include "util/DynBitVector.h"
#include "util/UtIOStream.h"

#include "nucleus/NUUseDefNode.h"

#include "flow/FLNode.h"
#include "flow/FLIter.h"
#include "flow/FLNodeElab.h"
#include "flow/FLNodeElabCycle.h"

#include "reduce/Split.h"
#include "localflow/ClockAndDriver.h"

#include "Util.h"
#include "UseDefHierPair.h"
#include "BlockSplit.h"
#include "MarkDesign.h"
#include "ScheduleData.h"

// Local scratch flags
#define SPLIT_FLOW	(1<<0)

SCHBlockSplit::SCHBlockSplit(SCHUtil* util, SCHMarkDesign* mark, 
                             SCHScheduleData* scheduleData) :
  mMultiSchedInfoValid(false), mSplitBlockInstancesSorted(false),
  mUtil(util), mMarkDesign(mark), mScheduleData(scheduleData)
{
  mScheduleFactory = new ScheduleFactory;
  mFlowToSchedMap = new FlowToSchedMap;
  mIsMultiSchedBlock = new IsMultiSchedBlock;
  mUseDefHierFlows = new UseDefHierFlows;
  mMultiSchedBlocks = new MultiSchedBlocks;
  mSplitBlockInstances = new UnSplitBlocks;
  mSplitBlockDefs = new SplitBlockDefs;
  mSplitFlowNodeElabs = new FLNodeConstElabMap;
  mSplitNodes = new FLNodeElabSet;
  mBlockFlowNodes = NULL;
}

SCHBlockSplit::~SCHBlockSplit()
{
  // Clear out the multi timing information if it wasn't already
  releaseMultiSchedInfo();

  // Clear out the schedule set factory
  ScheduleFactory::iterator f;
  for (f = mScheduleFactory->begin(); f != mScheduleFactory->end(); ++f)
  {
    const Schedules* scheds = *f;
    delete scheds;
  }

  // Clear the split block instances
  for (UnSplitBlocks::iterator g = mSplitBlockInstances->begin();
       g != mSplitBlockInstances->end();
       ++g)
    delete *g;

  delete mScheduleFactory;
  delete mFlowToSchedMap;
  delete mIsMultiSchedBlock;
  delete mUseDefHierFlows;
  delete mMultiSchedBlocks;
  delete mSplitBlockInstances;
  delete mSplitBlockDefs;
  delete mSplitFlowNodeElabs;
  delete mSplitNodes;
  delete mBlockFlowNodes;
}

void
SCHBlockSplit::findMultiTimingBlocks(SCHSchedulesLoop l)
{
  // Create the various maps from the requested schedules
  for (; !l.atEnd(); ++l)
    addSchedToMap(l.getScheduleBase(), l.getSchedule());

  // Go through the list of blocks and gather the ones that are in
  // multiple schedules.
  UseDefHierFlows::iterator pos;
  for (pos = mUseDefHierFlows->begin(); pos != mUseDefHierFlows->end(); ++pos)
  {
    FLNodeElabVector& nodes = pos->second;

    // Assume these are not multi schedule nodes or blocks. We use the
    // multiple schedule block flag to split blocks. We use the
    // multiple schedule block or nodes flag to dump information.
    bool multiSchedBlock = false;
    bool multiSchedNodes = false;

    // Get the first node and its schedule set
    NU_ASSERT(!nodes.empty(), pos->first.getUseDef());
    FLNodeElabVectorIter pos = nodes.begin();
    FLNodeElab* flow = *pos;
    const Schedules* schedules = (*mFlowToSchedMap)[flow];

    // Check if this node is in more than one schedule
    multiSchedNodes = (schedules->size() > 1);

    // Go through the rest and compare it against this type. As soon
    // as we find a flow node in a different schedule, we can stop.
    for (++pos; pos != nodes.end() && !multiSchedBlock; ++pos)
    {
      flow = *pos;

      // Get the schedule set for this flow node
      const Schedules* nextSchedules = (*mFlowToSchedMap)[flow];

      // Check if it is a multi schedule node
      if (!multiSchedNodes && (nextSchedules->size() > 1))
	multiSchedNodes = true;

      // Check if this schedule is different than the first flow node
      if (schedules != nextSchedules)
	multiSchedBlock = true;
    }

    // If this is a multi schedule block mark it
    if (multiSchedBlock || multiSchedNodes)
    {
      // Create the information
      BlockInstanceGroups* blockGroups = new BlockInstanceGroups;
      mMultiSchedBlocks->push_back(blockGroups);

      // If this is a multi schedule block, mark it
      if (multiSchedBlock)
	(*mIsMultiSchedBlock)[blockGroups] = true;
      
      // Sort the block instance information so that we can create the
      // split entries.
      std::sort(nodes.begin(), nodes.end(), CmpScheduleNodes(mFlowToSchedMap));

      // Copy in the information
      const Schedules* schedules = NULL;
      int index = -1;
      for (pos = nodes.begin(); pos != nodes.end(); ++pos)
      {
	FLNodeElab* flow = *pos;

	// If this is a new schedule set, create a new set of flow nodes
	const Schedules* newSchedules = (*mFlowToSchedMap)[flow];
	if (newSchedules != schedules)
	{
	  ++index;
	  blockGroups->resize(index+1);
	  schedules = newSchedules;
	}

	// Ad this flow node to the current set of flow nodes
	FLN_ELAB_ASSERT(!flow->isEncapsulatedCycle(), flow);
	(*blockGroups)[index].push_back(flow);
      }
    } // if
  } // for

  // Sort the data so that we get consistent traversals between platforms
  sortMultiSchedBlocks(mMultiSchedBlocks);
  mMultiSchedInfoValid = true;
} // SCHBlockSplit::findMultiTimingBlocks

const SCHBlockSplit::Schedules*
SCHBlockSplit::getFlowSchedules(FLNodeElab* flow)
{
  FlowToSchedMap::iterator pos = mFlowToSchedMap->find(flow);
  FLN_ELAB_ASSERT(pos != mFlowToSchedMap->end(), flow);
  return pos->second;
}

bool
SCHBlockSplit::splitBlocks(FLNodeElabSet* newFlows, bool allowTaskInlining)
{
  // If we created multi-timing information, release it now, we are
  // done with it.
  releaseMultiSchedInfo();

  // Go through the instances of split blocks and convert them to the
  // defs structure. This allows us to simplify them.
  createSplitBlockDefs();

  // Go through the block definitions and simplify the split sets so
  // that it applies to all instances. This is only done if the caller
  // requested it.
  simplifyFlowNodeSets();

  // Go through the blocks again and call the nucleus code to do the split
  bool ret = splitNucleusBlocks(newFlows, allowTaskInlining);

  // Make sure everything is sane
  if (mUtil->isFlagSet(eSanity)) { mMarkDesign->sanityCheck(); }
  return ret;
} // SCHBlockSplit::splitBlocks

void
SCHBlockSplit::addSchedToMap(SCHScheduleBase* schedBase,
			     SCHSchedulesLoop::Iterator iter)
{
  // Add the set of flow nodes to the map
  for (; !iter.atEnd(); ++iter)
  {
    // Make sure we have the real FLNodeElab*. We can't split the
    // FLNodeElabCycle*'s.
    FLNodeElab* curFlow = *iter;
    FLNodeElab* flow;
    if (curFlow->isEncapsulatedCycle())
      flow = mUtil->getCycleOriginalNode(curFlow);
    else
      flow = curFlow;

    // Add this flow to the schedule map. It also returns whether this
    // flow was seen before or not.
    bool uniqueFlow = addNodeToMap(flow, schedBase);

    // Also add this flow to the use def hier map. Start by creating
    // the map search value.
    if (uniqueFlow)
    {
      const NUUseDefNode* useDef = flow->getUseDefNode();
      const HierName* hierName = flow->getHier();
      SCHUseDefHierPair udhp(useDef, hierName);

      // If this entry doesn't exist, create it
      FLNodeElabVector& nodes = (*mUseDefHierFlows)[udhp];

      // Insert the flow node in the set of nodes
      nodes.push_back(flow);
    }
  } // for
} // SCHBlockSplit::addSchedBlockToMap

bool
SCHBlockSplit::addNodeToMap(FLNodeElab* flow, SCHScheduleBase* schedBase)
{
  bool uniqueFlow;

  // Find if this flow is in the map
  FlowToSchedMap::iterator pos;
  pos = mFlowToSchedMap->find(flow);
  uniqueFlow = (pos == mFlowToSchedMap->end());

  // Create the schedule set and insert it
  const Schedules* schedules;
  if (uniqueFlow)
  {
    // First time in
    schedules = buildSchedules(schedBase);
    (*mFlowToSchedMap)[flow] = schedules;
  }
  else
  {
    // Make a new set from the old set and the new schedule
    schedules = buildSchedules(schedBase, pos->second);
    pos->second = schedules;
  }

  return uniqueFlow;
}

const SCHBlockSplit::Schedules*
SCHBlockSplit::buildSchedules(SCHScheduleBase* schedBase)
{
  Schedules scheds;
  scheds.insert(schedBase);
  return getSchedules(&scheds);
}

const SCHBlockSplit::Schedules*
SCHBlockSplit::buildSchedules(SCHScheduleBase* schedBase,
				  const Schedules* schedules)
{
  Schedules scheds;
  scheds.insert(schedBase);
  Schedules::const_iterator p;
  for (p = schedules->begin(); p != schedules->end(); ++p)
    scheds.insert(*p);
  return getSchedules(&scheds);
}

const SCHBlockSplit::Schedules*
SCHBlockSplit::getSchedules(const Schedules* schedules)
{
  const Schedules* retSchedules;

  // See if it already exists
  ScheduleFactory::iterator pos;
  pos = mScheduleFactory->find(schedules);
  if (pos == mScheduleFactory->end())
  {
    // Insert a new schedule set
    retSchedules = new Schedules(*schedules);
    mScheduleFactory->insert(retSchedules);
  }
  else
    retSchedules = *pos;

  return retSchedules;
}

void SCHBlockSplit::releaseMultiSchedInfo()
{
  // Get rid of the blocks in multiple schedules. We have copies of
  // the ones we want to split.
  MultiSchedBlocks::iterator b;
  for (b = mMultiSchedBlocks->begin(); b != mMultiSchedBlocks->end(); ++b)
    delete *b;
  mMultiSchedBlocks->clear();

  // The multi schedule flag is just a vector of information we
  // already deleted.
  mIsMultiSchedBlock->clear();
  mMultiSchedInfoValid = false;
}

void SCHBlockSplit::createSplitBlockDefs()
{
  INFO_ASSERT(mSplitBlockDefs->empty(),
              "Duplicate call to createSplitBlockDefs() routine");
  for (UnSplitBlocks::iterator p = mSplitBlockInstances->begin();
       p != mSplitBlockInstances->end();
       ++p)
  {
    // Get information on the block instance
    BlockInstanceGroups* blockGroups = *p;

    // Get the block/hierarchy information from the first node. This
    // should be identical for all nodes
    FLNodeElabVector& nodes1 = *blockGroups->begin();
    FLNodeElab* flow1 = *nodes1.begin();
    const NUUseDefNode* useDef = flow1->getUseDefNode();
    const HierName* hierName = flow1->getHier();

    // Find where to put it in the definitions data
    SplitData& splitData = (*mSplitBlockDefs)[useDef];
    BlockDefSplits& blockSplits = splitData[hierName];
    FLN_ELAB_ASSERT(blockSplits.empty(), flow1);

    // If we need to add any new flows to the split, add them
    // elaboratedly here, before we convert the elaborated flows
    // into unelaborated data.

    // Get a set of all currently requested elaborated split flows
    UtSet<FLNodeElab*> splitElabFlows;
    BlockInstanceGroups::iterator g;
    for (g = blockGroups->begin(); g != blockGroups->end(); ++g)
    {
      FLNodeElabVector& nodes = *g;
      splitElabFlows.insert(nodes.begin(), nodes.end());
    }

    // Make sure all known blocks are represented. There may be some
    // elaborated flow nodes that are not scheduled but we still need
    // to pass them to the block split code.
    //
    // This occured in bug 2839. The case was due to a cycle. One of
    // the nodes in the cycle is only reachable from the cycle and
    // since we modify the flow it does not get hit in a traversal. So
    // it is not scheduled. But it is implicitly schedule because it
    // is part of a scheduled cycle.
    //
    // These flow nodes should be included in the split. So we fix
    // this by creating a new split entry and adding them all
    // together.
    //
    // It would be nice to fix this bug in a different way. One idea
    // is to make all nodes in a cycle reachable from all their
    // fanout. This will make all cycle nodes live. Another is to only
    // have one FLNodeElabCycle represent all the nets/flows in the
    // cycle. We are unsure which is better/worse today.
    FLNodeElabVector* newGroup = NULL;
    FLNodeElabVector allNodes;
    getBlockFlowNodes(flow1, &allNodes);
    for (FLNodeElabVectorCLoop l(allNodes); !l.atEnd(); ++l) {
      FLNodeElab* flow = *l;
      // Check if this is an active elaborated flow node first. The
      // SCHBlockFlowNodes::getFlows() routine above filters dead flow
      // but not inactive flops.
      //
      // Also check if this is an unrepresented elaborated flow node.
      //
      // If both these conditions are true we need to add it to a new
      // split.
      if (!flow->isInactive() &&
          splitElabFlows.find(flow) == splitElabFlows.end()) {

        // Allocate space for this new split entry if we haven't already
        if (newGroup == NULL)
        {
          UInt32 num_groups = blockGroups->size();
          blockGroups->resize(num_groups + 1);
          newGroup = &((*blockGroups)[num_groups]);
        }

        // Add the elaborated flow to this new entry
        newGroup->push_back(flow);
      }
    }

    // Resize the block splits since we know how many groups we need
    blockSplits.resize(blockGroups->size());

    // Go through the elaborated flow block groups and convert them to
    // unelaborated flow.
    UInt32 i;
    for (g = blockGroups->begin(), i = 0; g != blockGroups->end(); ++g, ++i)
    {
      // Go through the elaborated flow nodes and convert them to
      // unelaborated flow nodes.
      FLNodeElabVector& nodes = *g;
      for (FLNodeElabVectorIter n = nodes.begin(); n != nodes.end(); ++n)
      {
        // Add the unelaborated flow node to the current split entry
        FLNodeElab* flow = *n;
        FLNode* flnode = flow->getFLNode();
        SplitEntry& splitEntry = blockSplits[i];
        splitEntry.push_back(flnode);
        
        // Make sure they all belong to the same block.
        FLN_ELAB_ASSERT(useDef == flnode->getUseDefNode(), flow);
      }
    }
  } // for
} // SCHBlockSplit::createSplitBlockDefs

void
SCHBlockSplit::simplifyFlowNodeSets()
{
  // Type and data to simplify the splits into one set of set of nodes
  // where a node only exists in one set.
  typedef UtMap<const FLNode*, DynBitVector*> SubSetMap;
  typedef UtMap<DynBitVector*, SplitEntry, DynBitVectorCmp> SubSetReverseMap;

  // Find all the multi-instance blocks and create a split that
  // matches all the instances.
  SplitBlockDefs::iterator p;
  for (p = mSplitBlockDefs->begin(); p != mSplitBlockDefs->end(); ++p)
  {
    SubSetMap subSetMap;
    SubSetReverseMap subSetReverseMap;

    // Get the split info for this block. If it has more than once
    // instance then we need to simplify it.
    SplitData& splitData = p->second;
    if (splitData.size() > 1)
    {
      // More than once instance, we have to find the correct sub sets
      INFO_ASSERT(subSetMap.empty(), "Trying to split an empty set of blocks");

      // Go through all the subsets and create a map from the flow
      // nodes to what subsets they are in. We use a simple unsigned
      // integer to represent the subsets.
      UInt32 subset = 0;
      SplitData::iterator s;
      for (s = splitData.begin(); s != splitData.end(); ++s)
      {
	// Go through the set of splits
	BlockDefSplits& blockSplits = s->second;
	BlockDefSplits::iterator b;
	for (b = blockSplits.begin(); b != blockSplits.end(); ++b)
	{
	  // Go through the set of flow nodes
	  SplitEntry& splitEntry = *b;
	  SplitEntry::iterator f;
	  for (f = splitEntry.begin(); f != splitEntry.end(); ++f)
	  {
	    // Either create or get the dynamic bit vector this this
	    // flow node
	    const FLNode* flnode = *f;
	    SubSetMap::iterator pos = subSetMap.find(flnode);
	    DynBitVector* subsets;
	    if (pos == subSetMap.end())
	    {
	      subsets = new DynBitVector;
	      subSetMap.insert(SubSetMap::value_type(flnode, subsets));
	    }
	    else
	      subsets = pos->second;

	    // Potentially resize the dynamic bit vector. They don't
	    // automatically resize.
	    if (subset >= subsets->size())
		subsets->resize(subset+1);

	    // Set this flow node to be in this subset
	    subsets->set(subset);
	  }

	  // Done with this subset, increment the subset number
	  ++subset;
	}
      } // for

      // Go through the flow nodes and add them to the reverse
      // map. This will give us the final subsets
      SubSetMap::iterator m;
      for (m = subSetMap.begin(); m != subSetMap.end(); ++m)
      {
	// Get the flow node and subsets it is in
	const FLNode* flnode = m->first;
	DynBitVector* subsets = m->second;

	// Get the split entry it belongs to and add the flow node
	SplitEntry& splitEntry = subSetReverseMap[subsets];
	splitEntry.push_back(flnode);
      }

      // Delete the data for all the hierarchies
      for (s = splitData.begin(); s != splitData.end(); ++s)
	s->second.clear();

      // Copy over the split data to the first entry
      BlockDefSplits& blockSplits = splitData.begin()->second;
      SubSetReverseMap::iterator r;
      for (r = subSetReverseMap.begin(); r != subSetReverseMap.end(); ++r)
      {
	SplitEntry& splitEntry = r->second;
	blockSplits.push_back(splitEntry);
      }

      // Delete the dynamic bit vectors we allocated
      for (m = subSetMap.begin(); m != subSetMap.end(); ++m)
      {
	DynBitVector* subsets = m->second;
	delete subsets;
      }
    } // if
  } // for
} // SCHBlockSplit::simplifyFlowNodeSets

bool
SCHBlockSplit::splitNucleusBlocks(FLNodeElabSet* newFlows, bool allowTaskInlining)
{
  // Traverse the set of blocks to be split and add the FLNode's we
  // find the to map to elaborated flow nodes. We fill this in below
  SplitBlockDefs::iterator p;
  for (p = mSplitBlockDefs->begin(); p != mSplitBlockDefs->end(); ++p)
  {
    // Go through the split data and find the unelaborated nodes
    SplitData& splitData = p->second;
    BlockDefSplits* blockSplits = &(splitData.begin()->second);
    BlockDefSplits::iterator b;
    for (b = blockSplits->begin(); b != blockSplits->end(); ++b)
    {
      // Go through the nodes in the split set
      SplitEntry& splitEntry = *b;
      SplitEntry::iterator f;
      for (f = splitEntry.begin(); f != splitEntry.end(); ++f)
      {
	// Add the flow node to the map
	const FLNode* flnode = *f;
	FLN_ASSERT(mSplitFlowNodeElabs->find(flnode)==mSplitFlowNodeElabs->end(),
                   flnode);
	FLNodeElabSet emptySet;
	(*mSplitFlowNodeElabs)[flnode] = emptySet;
      }
    }
  } // for

  // Do a design walk to find all the FLNodeElab structures that are
  // affected by this set of splits.
  findAllDesignElaboratedNodes();

  bool check_liveness = false;
  if (check_liveness) {
    // Make a fanout snapshot of the design
    mMarkDesign->createFanoutMap();
  }

  // Call the nucleus to do the splitting work. It returns the set of
  // split flow nodes (and any new flow nodes).
  FLNodeElabSet deletedFlows;
  FLNodeElabSet splitFlows;
  bool ok = callNucleus(&splitFlows, &deletedFlows, allowTaskInlining);
  if (not ok) {
    return false;
  }

  if (check_liveness) {
    // Call the mark design and update its information
    mMarkDesign->markLiveNodes();

    // Verify that the design is still valid
    bool errorsFound = false;
    errorsFound |= mMarkDesign->validateLiveFlows();
    errorsFound |= mMarkDesign->validateDeletedFlowIsDead(&deletedFlows);
    ok = !errorsFound;

    // clear sanity caches
    mMarkDesign->clearFanoutMap();
  }

  // Delete the data for flows that were succesfully split.
  //
  // TBD - we have to deal with new flow nodes at some point.
  cleanupDeletedFlows(deletedFlows);
  handleSplitFlows(splitFlows, newFlows);
  return ok;
} // SCHBlockSplit::splitNucleusBlocks

void
SCHBlockSplit::findAllDesignElaboratedNodes()
{
  FLNodeElab* flow;
  for (SCHUtil::AllFlowsLoop l = mUtil->loopAllFlowNodes(); l(&flow);)
  {
    // Check if it is a split flow
    FLNode* flnode = flow->getFLNode();
    FLNodeConstElabMap::iterator pos = mSplitFlowNodeElabs->find(flnode);
    if ((pos != mSplitFlowNodeElabs->end()) && flow->isLive())
    {
      // It is, insert it
      FLNodeElabSet& nodes = pos->second;
      FLN_ELAB_ASSERT(flow->isLive(), flow);
      nodes.insert(flow);
    }
  } // for
} // SCHBlockSplit::findAllDesignElaboratedNodes

bool 
SCHBlockSplit::callNucleus(FLNodeElabSet* splitFlows,
                           FLNodeElabSet* deletedFlows,
                           bool allowTaskInlining)
{
  // This is where we store the data for the nucleus
  BlockToSplitInstructionsMap blockToSplitSetsMap;

  // Go through our data and convert it
  SplitBlockDefs::iterator p;
  for (p = mSplitBlockDefs->begin(); p != mSplitBlockDefs->end(); ++p)
  {
    // Create the structure to hold the BlockSplitSets
    NUUseDefNode* useDef = const_cast<NUUseDefNode*>(p->first);
    NU_ASSERT(blockToSplitSetsMap.find(useDef) == blockToSplitSetsMap.end(),
              useDef);

    SplitInstructions * splits = new SplitInstructions(useDef);
    blockToSplitSetsMap[useDef] = splits;

    // Go through the split data and find the unelaborated nodes
    SplitData& splitData = p->second;
    BlockDefSplits* blockSplits = &(splitData.begin()->second);
    BlockDefSplits::iterator b;
    for (b = blockSplits->begin(); b != blockSplits->end(); ++b)
    {
      // Create the structure to hold the FLNodeSets
      FLNodeOrderedSet* nodeSet = new FLNodeOrderedSet;

      // Go through the nodes in the split set and add them
      SplitEntry& splitEntry = *b;
      SplitEntry::iterator f;
      for (f = splitEntry.begin(); f != splitEntry.end(); ++f)
      {
        // Add the flow node to the set
        FLNode* flnode = const_cast<FLNode*>(*f);
        nodeSet->insert(flnode);
      }

      // add the nodeSet after population so it can be properly ordered.
      splits->addGroup(nodeSet);
    }
  } // for

  NUDesign * design = mUtil->getDesign();
  INFO_ASSERT(design, "Block splitting run without access to the design");

  // Gather clock aliasing information for the flow repair
  ClockAndDriver clock_and_driver;
  mMarkDesign->gatherClockAndDriver(&clock_and_driver);

  for (SCHClockMastersAndInvertsLoop loop(mMarkDesign->loopClockMastersAndInverts());
       not loop.atEnd();
       ++loop) {
    clock_and_driver.addClock(*loop);
  }
  for (SCHSchedule::AsyncResetLoop l(mMarkDesign->loopAsyncResets()); 
       not l.atEnd(); ++l) {
    clock_and_driver.addClock((*l).first);
  }

  // Call the nucleus to do the split
  Split * split = mUtil->getSplit();
  Split::Status status = split->blocks(design,
                                       &clock_and_driver,
                                       blockToSplitSetsMap, 
                                       *mSplitFlowNodeElabs,
                                       splitFlows,
                                       deletedFlows,
                                       allowTaskInlining,
                                       mUtil->isFlagSet(eDumpSplitBlocks),
                                       mUtil->getStats());

  // Delete the memory we created for the nucleus
  BlockToSplitInstructionsMap::iterator m;
  for (m = blockToSplitSetsMap.begin(); m != blockToSplitSetsMap.end(); ++m)
  {
    // Delete the split instructions; it takes care of deleting the
    // individual partitions.
    SplitInstructions * instructions = m->second;
    delete instructions;
  }

  // Remove any deleted flow from our stored data
  mScheduleData->removeDeadFlow(*deletedFlows);

  // The always block instance flow node data is stale, delete it
  delete mBlockFlowNodes;
  mBlockFlowNodes = NULL;

  return status != Split::eSplitError;
} // void SCHBlockSplit::callNucleus

static inline bool
lessThanBlock(const SCHBlockSplit::BlockInstanceGroups* b1,
	      const SCHBlockSplit::BlockInstanceGroups* b2)
{
  // Each group is for a given block so sort it by the flow nodes
  const FLNodeElabVector& nodes1 = *(b1->begin());
  const FLNodeElabVector& nodes2 = *(b2->begin());
  FLNodeElab* flow1 = *(nodes1.begin());
  FLNodeElab* flow2 = *(nodes2.begin());
  return FLNodeElab::compare(flow1, flow2) < 0;
}

void
SCHBlockSplit::sortMultiSchedBlocks(MultiSchedBlocks* blocks)
{
  sort(blocks->begin(), blocks->end(), lessThanBlock);
}

void
SCHBlockSplit::sortUnSplitBlocks(UnSplitBlocks* blocks)
{
  sort(blocks->begin(), blocks->end(), lessThanBlock);
}

void
SCHBlockSplit::cleanupDeletedFlows(FLNodeElabSet& deletedFlows)
{
  // Clean up the split block instances
  for (UInt32 i = 0; i < mSplitBlockInstances->size(); ++i)  {
    // Go through the entire block to check if this block was split
    BlockInstanceGroups* blockGroups = (*mSplitBlockInstances)[i];
    BlockInstanceGroups::iterator b;
    for (b = blockGroups->begin(); b != blockGroups->end(); ++b) {
      // Go through the nodes in this group and look for deleted nodes
      FLNodeElabVector newNodes;
      bool foundDeleted = false;
      FLNodeElabVector& nodes = *b;
      for (FLNodeElabVectorIter n = nodes.begin(); n != nodes.end(); ++n) {
        FLNodeElab* flowElab = *n;
        if (deletedFlows.find(flowElab) != deletedFlows.end()) {
          foundDeleted = true;
        } else {
          newNodes.push_back(flowElab);
        }
      }
      
      // If we found deleted flows, clean up
      if (foundDeleted) {
        nodes.swap(newNodes);
      }
    }
  } // for

  // Cleanup the unelab flow to elab flow map
  FLNodeConstElabMap::iterator f;
  for (f=mSplitFlowNodeElabs->begin(); f != mSplitFlowNodeElabs->end(); ++f) {
    // Look for deleted flow nodes in this set
    FLNodeElabSet& nodes = f->second;
    FLNodeElabSet newNodes;
    bool foundDeleted = false;
    for (FLNodeElabSetLoop f(nodes); !f.atEnd(); ++f) {
      FLNodeElab* flowElab = *f;
      if (deletedFlows.find(flowElab) != deletedFlows.end()) {
        foundDeleted = true;
      } else {
        newNodes.insert(flowElab);
      }
    }

    // If we found deleted flows, clean up
    if (foundDeleted) {
      nodes.swap(newNodes);
    }
  }
} // SCHBlockSplit::cleanupDeletedFlows
      

void
SCHBlockSplit::handleSplitFlows(FLNodeElabSet& splitFlows,
                                FLNodeElabSet* newFlows)
{
  // First go through and mark the flow nodes that were sucessfully
  // split
  for (FLNodeElabSetIter f = splitFlows.begin(); f != splitFlows.end(); ++f)
  {
    FLNodeElab* flow = *f;
    flow->setScratchFlags(SPLIT_FLOW);
  }

  // Now go through the set of blocks we asked to split and clean it up
  FLNodeSet splitNodes;
  for (UInt32 i = 0; i < mSplitBlockInstances->size(); ++i)
  {
    // Go through the entire block to check if this block was split
    BlockInstanceGroups* blockGroups = (*mSplitBlockInstances)[i];
    BlockInstanceGroups::iterator b;
    bool blockSplit = false;
    bool firstNode = true;
    for (b = blockGroups->begin(); b != blockGroups->end(); ++b)
    {
      // Go through the nodes in this group
      FLNodeElabVector& nodes = *b;
      for (FLNodeElabVectorIter n = nodes.begin(); n != nodes.end(); ++n)
      {
        // We only ask to split live nodes
        FLNodeElab* flow = *n;
        FLN_ELAB_ASSERT(flow->isLive(), flow);
        
        // If this is the first set of nodes, then use it to check if it
        // was split. Otherwise, make sure all the flow nodes have the
        // same split info
        if (firstNode)
        {
          blockSplit = flow->anyScratchFlagsSet(SPLIT_FLOW);
          firstNode = false;
        }
        else if (blockSplit)
          FLN_ELAB_ASSERT(flow->anyScratchFlagsSet(SPLIT_FLOW), flow);
        
        // We also keep track of the unelaborated flows that got
        // split. The reason is that not all instances of a flow may
        // have needed splitting, but it gets split no matter what. So
        // the nucleus call tells us about those as well. And so we
        // need to clear the scratch flag to distinguish them from new
        // flows.
        if (blockSplit)
          splitNodes.insert(flow->getFLNode());
      }
    } // for
    
    // If this block was split we can delete it. We do this by
    // clearing the entry. We post process and remove the wholes right
    // after
    if (blockSplit)
    {
      (*mSplitBlockInstances)[i] = NULL;
      delete blockGroups;
    }
  }

  // Go through all elaborated flows nodes for the blocks we split and
  // clear their scratch flag.
  for (FLNodeSetIter f = splitNodes.begin(); f != splitNodes.end(); ++f)
  {
    // Go through the list of elaborated flows
    FLNode* node = *f;
    FLNodeElabSet& nodeSet = (*mSplitFlowNodeElabs)[node];
    for (FLNodeElabSetIter ef = nodeSet.begin(); ef != nodeSet.end(); ++ef)
    {
      FLNodeElab* flow = *ef;
      flow->clearScratchFlags(SPLIT_FLOW);
    }
  }

  // Remove the holes in the vector (we want to maintain the sorted
  // order
  UInt32 dst = 0;
  UInt32 src = 0;
  while ((dst < mSplitBlockInstances->size()) &&
	 (src < mSplitBlockInstances->size()))
  {
    if ((*mSplitBlockInstances)[dst] != NULL)
      // Valid entry, no holes
      src = ++dst;
    else
    {
      // We have found a whole, look for an entry to fill it
      INFO_ASSERT(src >= dst,
                  "Algorithm to remove NULL blocks during block splitting failed");
      if ((*mSplitBlockInstances)[src] == NULL)
        // Don't have it yet, move along
        ++src;
      else
      {
        // Move the data back
        (*mSplitBlockInstances)[dst++] = (*mSplitBlockInstances)[src];
        (*mSplitBlockInstances)[src++] = NULL;
      }
    }
  } // while

  // Delete the space we don't need. The difference between src and
  // dst is the number of wholes in the array.
  while (dst < src)
  {
    mSplitBlockInstances->pop_back();
    ++dst;
  }

  // Check if there are any new flow nodes. Pass this to our caller
  for (FLNodeElabSetIter f = splitFlows.begin(); f != splitFlows.end(); ++f)
  {
    FLNodeElab* flow = *f;
    if (flow->anyScratchFlagsSet(SPLIT_FLOW))
    {
      flow->clearScratchFlags(SPLIT_FLOW);
      newFlows->insert(flow);
    }
  }
}

void
SCHBlockSplit::markSplitBlock(BlockInstanceGroups* blockGroups)
{
  // Make a copy of the passed in block
  BlockInstanceGroups* blockGroupsCopy = new BlockInstanceGroups;
  *blockGroupsCopy = *blockGroups;

  // Insert it
  mSplitBlockInstances->push_back(blockGroupsCopy);

  // This list can't be sorted anymore
  mSplitBlockInstancesSorted = false;
}

void
SCHBlockSplit::markSplitNode(FLNodeElab* flow)
{
  if (flow->getUseDefNode() != NULL)
    mSplitNodes->insert(flow);
}

void
SCHBlockSplit::convertNodesToBlocks()
{
  // We need to sort the split flows into block instances. So insert
  // them into a map that sorts them that way.  The set of blocks that
  // must be broken, and how it should be done
  typedef UtMap<SCHUseDefHierPair, FLNodeElabVector> SplitNodes;
  SplitNodes splitNodes;
  for(FLNodeElabSetIter i = mSplitNodes->begin(); i != mSplitNodes->end(); ++i)
  {
    FLNodeElab* flow = *i;
    SCHUseDefHierPair udhp(flow->getUseDefNode(), flow->getHier());
    FLNodeElabVector& nodes = splitNodes[udhp];
    nodes.push_back(flow);
  }

  // Traverse the sorted split blocks and create the real split blocks
  // from them.
  for (SplitNodes::iterator p = splitNodes.begin(); p != splitNodes.end(); ++p)
  {
    FLNodeElabVector& nodes = p->second;
    createSplitBlock(&nodes);
  }
}

void
SCHBlockSplit::createSplitBlock(FLNodeElabVector* nodes)
{
  // Validate that we aren't creating a false cycle and get the rest
  // of the nodes for this block
  FLNodeElabVector remainingNodes;
  validateBlock(nodes, &remainingNodes);

  // Make sure we have work to do
  int size = nodes->size();
  if (!remainingNodes.empty()) ++size;
  if (size < 2)
    return;

  // Create the new block instance group we will use
  BlockInstanceGroups* blockInstanceGroups = new BlockInstanceGroups;
  blockInstanceGroups->resize(size);

  // Add the nodes in each individual group
  int index = 0;
  for (FLNodeElabVectorIter i = nodes->begin(); i != nodes->end(); ++i)
  {
    FLNodeElab* flow = *i;
    FLNodeElabVector& nodes = (*blockInstanceGroups)[index++];
    nodes.push_back(flow);
  }

  // If we have remaining nodes add them
  if (!remainingNodes.empty())
  {
    FLNodeElabVector& nodes = (*blockInstanceGroups)[index];
    nodes.swap(remainingNodes);
  }

  // Add this block instance
  mSplitBlockInstances->push_back(blockInstanceGroups);
  mSplitBlockInstancesSorted = false;
}

void
SCHBlockSplit::validateBlock(FLNodeElabVector* nodes, FLNodeElabVector* remainingNodes)
{
  // Mark the flows that are split
  NUUseDefNode* useDef = NULL;
  FLNodeElab* flow = NULL;
  for (FLNodeElabVectorLoop l(*nodes); !l.atEnd(); ++l) {
    // Make sure they belong to the same block
    flow = *l;
    if (useDef == NULL)
      useDef = flow->getUseDefNode();
    else
      FLN_ELAB_ASSERT(flow->getUseDefNode() == useDef, flow);

    // Mark the nodes so we know which ones are split
    FLN_ELAB_ASSERT(!flow->anyScratchFlagsSet(SPLIT_FLOW), flow);
    flow->setScratchFlags(SPLIT_FLOW);
  }

  // Get the set of nodes for this block
  FLNodeElabVector allNodes;
  getBlockFlowNodes(flow, &allNodes);
  flow = NULL;

  // Put the non split flows in the remaining area
  for (FLNodeElabVectorCLoop l(allNodes); !l.atEnd(); ++l) {
    FLNodeElab* curFlow = *l;
    if (!curFlow->anyScratchFlagsSet(SPLIT_FLOW))
      remainingNodes->push_back(curFlow);
  }

  // Verify that we can't get from one of the remaining nodes to
  // another of the remaining nodes through a split flow. This would
  // mean we have a false cycle within a block that we are going to
  // split. We solve this by splitting another node. The routine
  // findFalseBlockCycle will mark the node to be split with
  // SPLIT_FLOW. We also update the remaining nodes
  FLNodeElabVector newRemaining;
  for (FLNodeElabVectorLoop l(*remainingNodes); !l.atEnd(); ++l) {
    FLNodeElab* curFlow = *l;
    FLNodeElabSet covered;
    if (findFalseBlockCycle(curFlow, &covered, true))
    {
      curFlow->setScratchFlags(SPLIT_FLOW);
      nodes->push_back(curFlow);
    }
    else
      newRemaining.push_back(curFlow);
  }
  remainingNodes->swap(newRemaining);

  // We can clear the split flow marking
  for (FLNodeElabVectorLoop l(*nodes); !l.atEnd(); ++l) {
    FLNodeElab* curFlow = *l;
    FLN_ELAB_ASSERT(curFlow->anyScratchFlagsSet(SPLIT_FLOW), curFlow);
    curFlow->clearScratchFlags(SPLIT_FLOW);
  }
} // SCHBlockSplit::validateBlock

bool
SCHBlockSplit::findFalseBlockCycle(FLNodeElab* flow, FLNodeElabSet* covered,
				   bool startNode)
{
  // Check if we have been here before
  if (covered->find(flow) != covered->end())
    return false;
  covered->insert(flow);

  // Visit the fanin looking for the false cycle
  bool falseCycle = false;
  SCHMarkDesign::TrueFaninLoop l;
  for (l = mMarkDesign->loopTrueFanin(flow); !l.atEnd() && !falseCycle; ++l)
  {
    // Visit this fanin flow if it is the right type. If flow is a
    // start node or a split flow and in the same block keep
    // visiting. Otherwise if this is not the start node and not a
    // split flow, we found a false cycle.
    FLNodeElab* fanin = *l;
    if (SCHUtil::sameBlock(flow, fanin))
    {
      if (startNode || fanin->anyScratchFlagsSet(SPLIT_FLOW))
	falseCycle = findFalseBlockCycle(fanin, covered, false);
      else if (!startNode)
	// Found a cycle
	falseCycle = true;
    }
  }

  return falseCycle;
} // SCHBlockSplit::findFalseBlockCycle

void
SCHBlockSplit::getBlockFlowNodes(FLNodeElab* flowElab, FLNodeElabVector* nodes)
{
  // Create the lookup map if we haven't already. We do it on demand
  // so that we can construct the class early and then use the block
  // splitting code later.
  if (mBlockFlowNodes == NULL) {
    mBlockFlowNodes = new SCHBlockFlowNodes(mUtil);
  }

  // Return the set of flow nodes
  mBlockFlowNodes->getFlows(flowElab, nodes, &FLNodeElab::isLive);
}

