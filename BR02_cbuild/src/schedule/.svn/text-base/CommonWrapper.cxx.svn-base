// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Code common to the PLI wrapper and the systemC wrapper.
*/

#include "codegen/codegen.h"
#include "codegen/CGAuxInfo.h"
#include "codegen/CGPortIface.h"
#include "util/OSWrapper.h"

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "iodb/IODBNucleus.h"
#include "iodb/ScheduleFactory.h"
#include "exprsynth/ESPopulateExpr.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUModule.h"
#include "schedule/Schedule.h"
#include "util/ArgProc.h"
#include "util/Stats.h"
#include "Util.h"
#include "Dump.h"
#include "MarkDesign.h"
#include "TimingAnalysis.h"
#include "CycleDetection.h"
#include "Util.h"
#include "BranchNets.h"
#include "util/UtString.h"
#include "reduce/RETransform.h"
#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/CarbonDBWrite.h"

#include "schedule/CommonWrapper.h"


/*
  Given the NUNetElab of a clock and an event, update the sets of
  clocks used appropriately.

  For primary clocks, just add to the correct set.
  For derived clocks, seek out the primary input dependencies by 
  using the code that finds branch nets.
 */
static void sComputeClockEdges(const SCHEvent  *event,
                               const NUNetElab *clk,
                               CNetSet         &bothEdgeClks,
                               ClkToEvent      &clkEdgeUsed,
                               SCHMarkDesign   *markDesign,
                               SCHBranchNets*  branchNets)
{
  NUNet* clkNet = clk->getNet();
  if (event != NULL && (clkNet->isPrimaryPort() || markDesign->isWritable(clk)))
  {
    ClkToEvent::iterator q = clkEdgeUsed.find(clkNet);
    if (q == clkEdgeUsed.end()) {
      clkEdgeUsed[clkNet] = event;
    }
    else {
      const SCHEvent* storedEvent = q->second;
      if (storedEvent != event)
      {
        // both edges of the same clock are being used. So
        // we no longer care about this clock. Add it to the
        // bothEdge set
        clkEdgeUsed.erase(q);
        bothEdgeClks.insert(clkNet);
      }
    }
  }
  else
  {
    // We have a clock that is not a primary input (derived
    // clock).  Find out which primary inputs contribute to
    // the clock.  The inputs go into the sensitivity set,
    // and since we do not know which edges of the primary
    // inputs drive the clock, we must assume that any
    // change will affect the clock; thus we must add the clocks
    // to the bothEdgeClks set.

    // Note that we use the mark design version of get input
    // dependencies because it goes through the clock pins of
    // flops and all combo logic until it hits PIs. That is
    // what we want to determine clock sensitivity.
    const SCHInputNets* inputs = branchNets->compute(clk);
    if (inputs != NULL)
    {
      for (SCHInputNetsCLoop n(*inputs); !n.atEnd (); ++n)
      {
        const NUNetElab* netElab = *n;
        // Skip any nets that are not primary inputs.
        if (markDesign->isPrimaryInput(netElab) || 
            markDesign->isWritable(netElab)) {
          // We *may* have already seen this clock in one
          // context where only one edge was used. We'll
          // remove those nets from the clkEdgeUsed set by
          // doing a set intersection after all the outputs have
          // been processed.
          NUNet* inputNet = netElab->getNet();
          bothEdgeClks.insert(inputNet);
        }
      } // input net loop
    } // if inputs != null
  } // if not primary port  
}

static void sComputeOutputSensitivity(CGPortIfaceNet* net, SCHUtil* util, SCHScheduleFactory* scheduleFactory, 
                                      SCHSchedule* schedule,
                                      SCHBranchNets* branchNets,
                                      SCHMarkDesign* markDesign,
                                      PortNetSet& validOutputs, CNetSet& bothEdgeClks, ClkToEvent& clkEdgeUsed)
{
  // If this is a valid live output, add it to validOutputs, and
  // check the signatures to get the minimum set of primary clocks.

  NUNetElab* elab = net->getNetElab();
  
  // Compute a signature with all drivers of this output port
  // Visit the drivers and figure out the timing
  const SCHSignature* signature = NULL;
  for (SCHUtil::DriverLoop l = util->loopNetDrivers(elab); !l.atEnd(); ++l)
  {
    FLNodeElab* flow = *l;
    if (flow->isLive() && flow->hasFanin())
    {
      const SCHSignature* newSignature = flow->getSignature();
      if (signature == NULL)
        signature = newSignature;
      else if (newSignature != NULL)
        signature = scheduleFactory->mergeSignatures(signature, newSignature);
    }
  }
  
  if (signature == NULL)
  {
    // The output port does not appear to be driven by the design.
    // However, it may itself be depositable/forcible.  If so,
    // treat it as a 'live' output.
    if (net->isForcible() || net->isDeposit() || net->isObserve())
      validOutputs.insert(net);
  }
  else   // output port is driven by the design
  {
    validOutputs.insert(net);
    
    // if output port is part of some asynchronous logic. If this
    // output port is connected to a depositable then that depositable
    // will have its update sensitivity on both edges.
    if(scheduleFactory->hasOutputMask(signature) &&
       scheduleFactory->hasInputMask(signature))
    {
      // Use the CombinationalSchedules (through MarkDesign)
      // version of getting input dependencies because it stops at
      // flops and only returns the set of primary inputs that can
      // be reached through combinational logic.
      const SCHInputNets* inputs;
      inputs = markDesign->findNetInputNets(elab);
      if (inputs != NULL)
      {
        // Add each primary input to the bothEdgeClks set to
        // ensure we do not make the clock update edge sensitive
        for (SCHInputNetsCLoop n(*inputs); !n.atEnd (); ++n)
        {
          const NUNetElab* netElab = *n;
          NUNet* inputNet = netElab->getNet();
          
          bothEdgeClks.insert(inputNet);
        }
      }
    }
    
    // Gather the input nets for the clocks on this transition mask
    const SCHScheduleMask* transition = signature->getTransitionMask();
    for (SCHScheduleMask::UnsortedEvents l = transition->loopEvents();
         !l.atEnd(); ++l)
    {
      const SCHEvent* event = *l;
      if (event->isClockEvent())
      {
        const NUNetElab* clk = schedule->getClockMaster(event->getClock());
        // add all clock edges that affect the output to the sets
        sComputeClockEdges(event, clk, bothEdgeClks, clkEdgeUsed, markDesign,
                           branchNets);
      } // if is clock event
    } // loop transition events
  } // if signature 
}

static void sComputeInputSensitivity(CGPortIfaceNet* net, PortNetSet& validInputs)
{
  NUNetElab *elab = net->getNetElab();
  
  // if this is a valid live input, add it to validInputs
  bool isLive = false;
  for (NUNetElab::DriverLoop loop = elab->loopContinuousDrivers();
       ! loop.atEnd() && ! isLive; ++loop)
  {
    FLNodeElab* flElab = *loop;
    if (flElab->isLive())
    {
      isLive = true;
      validInputs.insert(net);
    }
  }
}

// Compute the alwaysCopy, sensitivity, input events, and output events
// 
// validInputs -
//   The set of valid inputs to the design
//
// validOutputs - 
//    The set of valid outputs from the design
//
// bothEdgeClks - A set of clock nets for which both edges must be
// used for calling schedule
//
// clksUsedAsData - A set of clock nets that are also used as data
// through sequential nodes. This means that the values of the clocks
// must be updated on each edge, but the schedule may only need to be
// called on one of the edges.
//
// clkEdgeUsed - Set of 1 edge only clock nets. Meaning, the scheduler
// only cares about 1 of the edges each of the clocks. Therefore, the
// scheduler can be called only on the significant edge.
static void sComputeSensitivityAndEventMaps(
  CGPortIface* portIface,
  SCHSchedule* schedule,
  SCHMarkDesign* markDesign,
  SCHUtil* util,
  SCHBranchNets* branchNets,
  CNetSet& bothEdgeClks,
  PortNetSet& validInputs,
  PortNetSet& validOutputs,
  PortNetSet& validTriOuts,
  PortNetSet& validBidis,
  CNetSet& clksUsedAsData,
  ClkToEvent& clkEdgeUsed )
{
  for (CGPortIface::NetIter p = portIface->loopIns(); ! p.atEnd(); ++p)
  {
    CGPortIfaceNet* net = *p;
    sComputeInputSensitivity(net, validInputs);
  } // loop ins
  

  SCHScheduleFactory *scheduleFactory = schedule->getFactory();

  for (CGPortIface::NetIter p = portIface->loopOuts(); ! p.atEnd(); ++p)
  {
    CGPortIfaceNet* net = *p;
    sComputeOutputSensitivity(net, util, scheduleFactory, schedule, branchNets,
                              markDesign, validOutputs, bothEdgeClks, clkEdgeUsed);
  } // loop outs

  for (CGPortIface::NetIter p = portIface->loopTriOuts(); ! p.atEnd(); ++p)
  {
    CGPortIfaceNet* net = *p;
    sComputeOutputSensitivity(net, util, scheduleFactory, schedule, branchNets,
                              markDesign, validTriOuts, bothEdgeClks, clkEdgeUsed);
  } // loop tristate outs

  for (CGPortIface::NetIter p = portIface->loopBidis(); ! p.atEnd(); ++p)
  {
    CGPortIfaceNet* net = *p;
    sComputeInputSensitivity(net, validBidis);
    sComputeOutputSensitivity(net, util, scheduleFactory, schedule, branchNets,
                              markDesign, validBidis, bothEdgeClks, clkEdgeUsed);
  } // loop bids
  

  // Visit all flops - Written by Rsayde.
  SCHMarkDesign::ClockMap clockMap;
  for (SCHMarkDesign::SequentialNodesLoop s = markDesign->loopSequentialNodes(); !s.atEnd(); ++s) {
    // Recursively visit all combo logic feeding the d pin. Return an
    // SCHInputNets pointer that represents the primary clocks that
    // are used as data.
    FLNodeElab* flow = *s;
    ClockEdge edge;
    const NUNetElab *clkElab;

    // look for derived clocks
    markDesign->getSequentialNodeClock(flow, flow->getUseDefNode(), &edge, &clkElab);        
    const SCHEvent *event = util->buildClockEdge(clkElab, edge, 0);
    sComputeClockEdges(event, clkElab, bothEdgeClks, clkEdgeUsed, markDesign,
                       branchNets);

    // look for clocks used as data
    for (SCHMarkDesign::LevelFaninLoop l = markDesign->loopLevelFanin(flow); 
         !l.atEnd(); ++l) {
      FLNodeElab* fanin = *l;
      const SCHInputNets* clockNets = markDesign->findClocksAsData(fanin,
                                                                   &clockMap);
      if (clockNets)
      {
        // Add each primary clock to the bothedges list. We have to
        // update the clock value whenever it changes since it is used
        // as data.
        for (SCHInputNetsCLoop n(*clockNets); !n.atEnd (); ++n)
        {
          const NUNetElab* netElab = *n;
          NUNet* inputNet = netElab->getNet();
          
          clksUsedAsData.insert(inputNet);
          bothEdgeClks.insert(inputNet);
        }
      } // if clocknets
    } // loop fanin 
  } // loop sequential nodes

  // Do set intersection. bothEdgeClks contains clocks that must be
  // removed from the clkEdgeUsed set.
  for (CNetSet::iterator clkIter = bothEdgeClks.begin(); 
       clkIter != bothEdgeClks.end(); ++clkIter)
  {
    const NUNet* clkNet = *clkIter;
    clkEdgeUsed.erase(clkNet);
  }
}

// Error on complex ports that we do not support in the systemc
// wrapper yet.
static bool sCheckPort(const CGPortIfaceNet* net, SCHUtil* util)
{
  MsgContext* msgContext = util->getMsgContext();

  bool unsupported = false;  

  if (net->isReal())
  {
    unsupported = true;
    msgContext->SchSysCWrapReal(net->getNetElab());
  }
  
  return unsupported;
}


static void sMaybeAddToValidSet(PortNetSet& validPorts, const CGPortIfaceNet* ifaceNet)
{
  if (ifaceNet->isExpressioned())
    validPorts.insert(ifaceNet);
}

bool SCHSchedule::wrapperSensitivity(
  CGPortIface* portIface,
  CNetSet &bothEdgeClks,
  PortNetSet &validOutPorts,
  PortNetSet &validInPorts,
  PortNetSet &validTriOutPorts,
  PortNetSet &validBidiPorts,
  CNetSet &clksUsedAsData,
  ClkToEvent &clkEdgeUses,
  bool generatingWrapper)
{
  // Compute the sensitivity list, input events, and output events

  // before computing sensitivity, add all expression nets to their
  // associated valid net set. The NUNets for these are seen as dead,
  // so they won't get in the set otherwise. It would be better to
  // actually compute which expression nets are needed, but could
  // prove to be difficult. For now, let's just assume expression nets
  // are valid.
  for (CGPortIface::NetIter p = portIface->loopIns(); ! p.atEnd(); ++p)
    sMaybeAddToValidSet( validInPorts, *p );
  for (CGPortIface::NetIter p = portIface->loopOuts(); ! p.atEnd(); ++p)
    sMaybeAddToValidSet( validOutPorts, *p );
  for (CGPortIface::NetIter p = portIface->loopTriOuts(); ! p.atEnd(); ++p)
    sMaybeAddToValidSet( validTriOutPorts, *p );
  for (CGPortIface::NetIter p = portIface->loopBidis(); ! p.atEnd(); ++p)
    sMaybeAddToValidSet( validBidiPorts, *p );
  
  // Class to compute branch nets for various clocks
  UInt32 flags = (SCHBranchNetFlag(eIncludeClock) | 
                  SCHBranchNetFlag(eMergeWritable) |
                  SCHBranchNetFlag(eIgnoreFastClock));
  SCHBranchNets branchNets(mMarkDesign, flags);
  branchNets.init();
  

  sComputeSensitivityAndEventMaps(portIface,
                                  this, mMarkDesign, mUtil,
                                  &branchNets,
                                  bothEdgeClks,
                                  validInPorts, validOutPorts,
                                  validTriOutPorts, validBidiPorts,
                                  clksUsedAsData,
                                  clkEdgeUses);
  

  bool hasComplexPort = false;
  if (generatingWrapper) {
    // Sanity check ports. Can't support real ports currently
    for (PortNetSet::iterator p = validOutPorts.begin(); p != validOutPorts.end(); ++p)
    {
      const CGPortIfaceNet* net = *p;
      hasComplexPort |= sCheckPort(net, mUtil);
    }
    for (PortNetSet::iterator p = validInPorts.begin(); p != validInPorts.end(); ++p)
    {
      const CGPortIfaceNet* net = *p;
      hasComplexPort |= sCheckPort(net, mUtil);
    }
    for (PortNetSet::iterator p = validTriOutPorts.begin(); p != validTriOutPorts.end(); ++p)
    {
      const CGPortIfaceNet* net = *p;
      hasComplexPort |= sCheckPort(net, mUtil);
    }
    for (PortNetSet::iterator p = validBidiPorts.begin(); p != validBidiPorts.end(); ++p)
    {
      const CGPortIfaceNet* net = *p;
      hasComplexPort |= sCheckPort(net, mUtil);
    }
  }
  return hasComplexPort;
}
