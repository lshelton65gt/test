// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUCycle.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUModule.h"

#include "flow/FLNodeElab.h"
#include "flow/FLFactory.h"
#include "flow/FLIter.h"

#include "iodb/ScheduleFactory.h"
#include "iodb/IODBNucleus.h"

#include "util/CarbonTypeUtil.h"
#include "util/CbuildMsgContext.h"
#include "util/SetOps.h"

#include "compiler_driver/CarbonDBWrite.h"

#include "symtab/STAliasedLeafNode.h"

#include "schedule/Schedule.h"

#include "Util.h"

/*!
  \file
  A set of functions that is useful to various scheduling activities.
*/

SCHUtil::SCHUtil(SCHSchedule* sched, 
                 SCHScheduleFactory* factory,
                 FLNodeFactory* flowFactory,
                 FLNodeElabFactory* flowElabFactory,
                 Split* split,
                 IODBNucleus* iodb, STSymbolTable* st, MsgContext* mc,
                 NUNetRefFactory* netref_factory, RETransform* transform,
                 ArgProc* args, ESPopulateExpr* populateExpr,
                 SourceLocatorFactory* srcloc_factory)                 
{
  mSched = sched;
  mFactory = factory;
  mSplit = split;
  mFlowFactory = flowFactory;
  mFlowElabFactory = flowElabFactory;
  mIODB = iodb;
  mSymbolTable = st;
  mMsgContext = mc;
  mNetRefFactory = netref_factory;
  mTransform = transform;
  mArgs = args;
  mPopulateExpr = populateExpr;
  mSourceLocatorFactory = srcloc_factory;

  mStats = NULL;
  mSchedulePass = 0;

  mDesign = NULL;

  // Initialize the flags to empty, we set them with create
  mFlags = SCHScheduleFlags(0);
  mFlagsSet = false;
  mDumpMergedBlocks = 0;
  mBufferedMemoryThreshold = 0;
  mNoPortCoercion = false;
}

SCHUtil::~SCHUtil()
{
}


NUAlwaysBlock* SCHUtil::getPriorityBlock(NUUseDefNode* useDef)
{
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  NU_ASSERT((always != NULL) && always->isSequential(), useDef);
  NUAlwaysBlock* priorityBlock = always->getPriorityBlock();
  return(priorityBlock);
}

NUAlwaysBlock* SCHUtil::getClockBlock(NUUseDefNode* useDef)
{
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  NU_ASSERT((always != NULL) && always->isSequential(), useDef);
  NUAlwaysBlock* clockBlock = always->getClockBlock();
  return(clockBlock);
}

void
SCHUtil::getSequentialClockPin(FLNodeElab* flow, ClockEdge* edge,
			       NUNetElab** clk)
{
  // There is a copy of this code in ConstantPropagation. I wanted to
  // put this routine in an area common to both but didn't really know
  // of a good place. We should re-organize this at some point.
  NUUseDefNode* useDef = flow->getUseDefNode();
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  FLN_ELAB_ASSERT((always != NULL) && always->isSequential(), flow);
  NUEdgeExpr *edge_expr = always->getEdgeExpr();
  FLN_ELAB_ASSERT(edge_expr != NULL, flow);
  *edge = edge_expr->getEdge();
  NUNet* clockNet = edge_expr->getNet();
  *clk = clockNet->lookupElab(flow->getHier());
}

void SCHUtil::markNetFlows(FLNodeElab* flow, UInt32 pass)
{
  for (DriverLoop l = loopNetDrivers(flow->getDefNet()); !l.atEnd(); ++l)
  {
    FLNodeElab* curFlow = *l;
    curFlow->putSchedulePass(pass);
  }
}

// TBD this is a cylical dependency. We need to figure out why we
// should remove the util class storing a pointer to mSched.
const SCHEvent*
SCHUtil::buildClockEdge(const NUNetElab* clk, ClockEdge edge, UInt32 priority)
{
  bool invert;
  const NUNetElab* newClk = mSched->getClockMaster(clk, &invert);
  NU_ASSERT(newClk != NULL, clk);
  if (invert) {
    edge = ClockEdgeOppositeEdge( edge );
  }
  return mFactory->buildClockEdge(newClk->getSymNode(), edge, priority);
}

FLNodeElab* SCHUtil::CycleMapper::operator()(FLNodeElab* flow) const
{
  return mFactory->getAcyclicNode(flow);
}

bool SCHUtil::getBlockNetDrivers(NUNetElab* net, FLNodeElab* flow,
                                 FLNodeElabSet* drivers,
                                 FlowCycleMode flowCycleMode)
{
  FLNodeElabSet covered;
  UInt32 num_non_continuous_drivers = 0;
  getBlockNetDriversHelper(net, flow, drivers, flowCycleMode,
                           &covered, &num_non_continuous_drivers);

  // this routine cannot reliably report multiple drivers unless they
  // are all continuous drivers
  if ((num_non_continuous_drivers != 0) && (drivers->size() > 1)) {
    drivers->clear();
    return false;
  }
  return true;
}

void SCHUtil::getBlockNetDriversHelper(NUNetElab* net, FLNodeElab* flow,
                                       FLNodeElabSet* drivers,
                                       FlowCycleMode flowCycleMode,
                                       FLNodeElabSet* covered,
                                       UInt32* num_non_continuous_drivers)
{
  if (flow == NULL)
  {
    if (flowCycleMode == eFlowOverCycles)
    {
      for (DriverLoop p = loopNetDrivers(net); !p.atEnd(); ++p)
        drivers->insert(*p);
    }
    else
    {
      for (NUNetElab::DriverLoop p = net->loopContinuousDrivers(); !p.atEnd(); ++p)
        drivers->insert(*p);
    }
  }
  else
  {
    if (covered != NULL)
      covered->insert(flow);

    FLNodeElabSet allDrivers;
    for (Fanin p = loopFanin(flow, flowCycleMode, 0);
         !p.atEnd(); ++p)
    {
      FLNodeElab* fanin = *p;
      if (fanin->getDefNet() == net) {
        NUUseDefNode* node = fanin->getUseDefNode();
        if ((node != NULL) && !node->isContDriver()) {
          ++*num_non_continuous_drivers;
        }
        drivers->insert(fanin);
      }
      else if ((covered != NULL) && // only recurse thru if there is a covered set
               !isSequential(fanin) &&
               (drivers->find(fanin) == drivers->end()) &&
               (covered->find(fanin) == covered->end()))
      {
        getBlockNetDriversHelper(net, fanin, drivers, flowCycleMode, covered,
                                 num_non_continuous_drivers);
      }
    }
  }
} // void SCHUtil::getBlockNetDriversHelper

void
SCHUtil::findAllBlockNodes(BlockToNodesMap& blockToNodesMap)
{
  FLNodeElab* flow;
  for (AllFlowsLoop p = loopAllFlowNodes(); p(&flow);)
  {
    FLNodeElabSet& nodeSet = blockToNodesMap[flow->getUseDefNode()];
    nodeSet.insert(flow);
  }
}


SCHUtil::BlockNodesLoop
SCHUtil::loopAllBlockNodes(BlockToNodesMap& blockToNodesMap,
			   const NUUseDefNode* useDef)
{
  BlockToNodesMap::iterator pos = blockToNodesMap.find(useDef);
  NU_ASSERT (pos != blockToNodesMap.end(), useDef);
  FLNodeElabSet& blockNodes = pos->second;
  return blockNodes.loopSorted();
}

FLNodeElab* SCHUtil::getCycleOriginalNode(FLNodeElab* flow) const
{
  return mFlowElabFactory->getCyclicNode(flow);
}

bool SCHUtil::sameBlock(FLNodeElab* flow1, FLNodeElab* flow2)
{
  // Check for simple case
  if (flow1 == flow2)
    return true;

  NUUseDefNode* useDef1 = flow1->getUseDefNode();
  NUUseDefNode* useDef2 = flow2->getUseDefNode();
  if (useDef1 == useDef2)
  {
    HierName* hier1 = flow1->getHier();
    HierName* hier2 = flow2->getHier();
    return (hier1 == hier2);
  }
  return false;
}


//! Get the internal dependencies on 'net_elab' by 'flow'. Return true if consistent.
static bool getInternalFlowDependenciesOnNet(FLNodeElab * flow, NUNetElab * net_elab, 
                                             FLNodeElabSet * dependencies)
{
  bool success = true;

  FLNodeElabIter * iter = flow->makeFaninIter(FLIterFlags::eAll,
                                              FLIterFlags::eBound,
                                              FLIterFlags::eBranchAtAll,
                                              false,
                                              FLIterFlags::eIterNodeOnce);
  for ( ; not iter->atEnd(); iter->next() ) {
    FLNodeElab * cur_flow = **iter;
    if (cur_flow->isBoundNode()) {
      continue;
    }

    NUUseDefNode * node = cur_flow->getUseDefNode();
    // If we see a continuous driver, we are about to leave the
    // current use-def node. Our search is restricted to one use-def node.
    if (node->isContDriver()) {
      iter->doNotExpand();
      continue;
    }

    // Nucleus constructs which prevent an accurate search.
    if ((node->getType() == eNUCModelCall) or
        (node->getType() == eNUTaskEnable) or
        (node->getType() == eNUFor)) {
      // In the future, we could allow these constructs if only one of
      // the two nets we are testing for cyclic dependency is defined.
      success = false;
      break;
    }

    // Nesting nodes are not analyzed during this
    // search; we care about the actual driving nodes.
    if (cur_flow->hasNestedBlock()) {
      continue;
    }
    
    NUNetElab * cur_elab = cur_flow->getDefNet();
    if (net_elab != cur_elab) {
      continue;
    }

    dependencies->insert(cur_flow);
  }
  delete iter;
  return success;
}

bool SCHUtil::dataDependencyIsReal(FLNodeElab * flow, FLNodeElab * fanin)
{
  if (flow == fanin) {
    return false;
  }

  bool same_block = SCHUtil::sameBlock(flow, fanin);
  if (not same_block) {
    return true;
  }

  bool valid;

  NUNetElab * fanin_elab = fanin->getDefNet();

  FLNodeElabSet discovered_fanin_drivers;
  valid = getInternalFlowDependenciesOnNet(flow,fanin_elab,&discovered_fanin_drivers);
  if (not valid){ 
    return true;
  }

  FLNodeElabSet fanin_drivers;
  valid = getInternalFlowDependenciesOnNet(fanin,fanin_elab,&fanin_drivers);
  if (not valid) {
    return true;
  }

  FLNodeElabSet uncovered_drivers;
  set_difference(fanin_drivers,discovered_fanin_drivers,&uncovered_drivers);

  return (not uncovered_drivers.empty());
}

SCHUtil::DriverLoop SCHUtil::loopNetDrivers(NUNetElab* net)
{
  return DriverLoop(net->loopContinuousDrivers(), mFlowElabFactory);
}

SCHUtil::ConstDriverLoop SCHUtil::loopNetDrivers(const NUNetElab* net) const
{
  return ConstDriverLoop(net->loopContinuousDrivers(), mFlowElabFactory);
}

bool SCHUtil::isCModel(FLNodeElab* flow)
{
  FLNodeElabSet covered;
  return isCModelRecurse(flow, &covered);
}

bool SCHUtil::isCModelRecurse(FLNodeElab* flow, FLNodeElabSet* covered)
{
  // Check if we have been here before. If so return false. When
  // checking for existance, if we have been here before and still
  // gotten here, returning true would not add any new information.
  if (covered->find(flow) != covered->end()) {
    return false;
  }
  covered->insert(flow);

  // If it is a cycle check the sub components. We have to treat it
  // special because we have to look at the insides of them to tell if
  // a c-model is called.
  if (flow->isEncapsulatedCycle()) {
    NUCycle* cycle = flow->getCycle();
    bool isCMod = false;
    for (NUCycle::FlowLoop l = cycle->loopFlows(); !l.atEnd() && !isCMod; ++l)
    {
      FLNodeElab* nested = *l;
      isCMod = isCModelRecurse(nested, covered);
    }
    return isCMod;
  }

  // Check if this flow represents a statement. If so we can test if
  // it is a c-model.
  if (!flow->hasNestedBlock()) {
    // Check if this is a c-model
    NUUseDefNode* useDef = flow->getUseDefNode();
    return (useDef && useDef->getType() == eNUCModelCall);
  }

  // Recursively walk the nested flow until we get to a statement
  bool isCMod = false;
  for (FLNodeElabLoop l = flow->loopNested(); !l.atEnd() && !isCMod; ++l) {
    FLNodeElab* nested = *l;
    isCMod = isCModelRecurse(nested, covered);
  }

  return isCMod;
} // bool SCHUtil::isCModel

bool SCHUtil::sameSequentialBlock(FLNodeElab* flow1, FLNodeElab* flow2)
{
  // Get the always blocks for these two flow nodes
  NUUseDefNode* useDef1 = flow1->getUseDefNode();
  NUUseDefNode* useDef2 = flow2->getUseDefNode();
  FLN_ELAB_ASSERT(useDef1->isSequential(), flow1);
  FLN_ELAB_ASSERT(useDef2->isSequential(), flow2);
  NUAlwaysBlock* always1 = dynamic_cast<NUAlwaysBlock*>(useDef1);
  NUAlwaysBlock* always2 = dynamic_cast<NUAlwaysBlock*>(useDef2);

  // Check if one of them is a priority block
  NUAlwaysBlock* clkBlock1 = always1->getClockBlock();
  NUAlwaysBlock* clkBlock2 = always2->getClockBlock();
  if ((clkBlock1 != NULL) && (clkBlock2 != NULL))
    // Both are priority blocks
    return (clkBlock1 == clkBlock2);
  else if (clkBlock1 != NULL)
    // flow1 is a priority block but flow2 is a clock block
    return (clkBlock1 == always2);
  else if (clkBlock2 != NULL)
    // flow2 is a priority block but flow1 is a clock block
    return (clkBlock2 == always1);
  else
    // They are both clock blocks so they must be different
    return false;
} // bool SCHUtil::sameSequentialBlock


Fanin
SCHUtil::loopFanin(FLNodeElab* flow, FlowCycleMode cycleMode,
		   unsigned int addFlags, unsigned int nestFlags,
		   bool sorted)
{
  return Fanin(mFlowElabFactory, flow, cycleMode, addFlags, nestFlags, sorted);
}

bool SCHUtil::isActiveDriver(FLNodeElab* flowElab) const
{
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  return ((useDef != NULL) && !useDef->isInitial() &&
          (useDef->getStrength() != eStrPull));
}

bool SCHUtil::hasActiveDrivers(const NUNetElab* netElab) const
{
  // Check if this elaborated net has any active drivers
  bool hasActiveDrivers = false;
  for (SCHUtil::ConstDriverLoop l = loopNetDrivers(netElab); !l.atEnd(); ++l) {
    FLNodeElab* flow = *l;
    if (isActiveDriver(flow)) {
      hasActiveDrivers = true;
    }
  }
  return hasActiveDrivers;
}

const CbuildSymTabBOM* SCHUtil::getBOMManager() const
{
  const CbuildSymTabBOM* bomM = dynamic_cast<const CbuildSymTabBOM*>(mSymbolTable->getFieldBOM());
  return bomM;
}

NUEnabledDriver* SCHUtil::findEnabledDriver(FLNodeElab* flow) const
{
  // Bound nodes don't have enabled drivers
  NUUseDefNode* useDef = flow->getUseDefNode();
  if (flow->isBoundNode() || (useDef == NULL)) {
    return NULL;
  }

  // Look for the enabled driver. It can either be a continous driver
  // or a blocking enabled driver in an always block.
  NUEnabledDriver* enabledDriver = NULL;
  if (useDef->getType() == eNUAlwaysBlock) {
    // Look for a nested statement for the enabled driver
    FLNodeElab* nested = flow;
    while (nested->hasNestedBlock() && (nested->numNestedFlows() == 1)) {
      FLNodeElabLoop loop = nested->loopNested();
      nested = *loop;
    }

    // The enable driver should be here
    NUUseDefNode* nestedUseDef = nested->getUseDefNode();
    if (nestedUseDef->getType() == eNUBlockingEnabledDriver) {
      enabledDriver = dynamic_cast<NUEnabledDriver*>(nestedUseDef);
    }

  } else if (useDef->getType() == eNUContEnabledDriver) {
    // It is a continous driver which means we can just cast to it
    enabledDriver = dynamic_cast<NUEnabledDriver*>(useDef);
  }

  return enabledDriver;
} // NUEnabledDriver* SCHSchedule::findEnabledDriver


NUNetRefHdl SCHUtil::getDefNetRef(FLNodeElab* flow) const
{
  // Get the original flow node (not the FLNodeElabCycle) first. This
  // is because the acyclic node does not have a FLNode* which is
  // necessary to get a net ref.
  FLNodeElab* realFlow = flow;
  if (flow->isEncapsulatedCycle()) {
    realFlow = mFlowElabFactory->getCyclicNode(flow);
  }

  return realFlow->getDefNetRef(mNetRefFactory);
}

void SCHUtil::getAcyclicFlows(const NUCycle* cycle, FLNodeElabVector* flows)
  const
{
  for (NUCycle::FlowLoop l = cycle->loopFlows(); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    FLNodeElab* acyclicFlow = getAcyclicNode(flowElab);
    flows->push_back(acyclicFlow);
  }
}

void SCHUtil::getAcyclicFlows(FLNodeElab* flowElab, FLNodeElabVector* flows)
  const
{
  FLN_ELAB_ASSERT(flowElab->isEncapsulatedCycle(), flowElab);
  const NUCycle* cycle = flowElab->getCycle();
  getAcyclicFlows(cycle, flows);
}

bool SCHUtil::isPullDriver(FLNodeElab* flow) const
{
  NUUseDefNode* useDef = flow->getUseDefNode();
  return ((useDef != NULL) && (useDef->getStrength() == eStrPull) &&
          !flow->hasFanin());
}

bool SCHUtil::isInputMask(const SCHScheduleMask* mask) const
{
  return mFactory->getInputMask() == mask;
}

bool SCHUtil::isClockDivider(FLNodeElab* flowElab) const
{
  // Check if all the bits that are defined are also used
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  NUNetRefHdl netRef = flowElab->getFLNode()->getDefNetRef();
  return useDef->queryUses(netRef, &NUNetRef::covers, mNetRefFactory);
}

SCHUtil::ConstDriverLoop
SCHUtil::FlowLoopGen::operator()(const NUNetElab* clkElab)
{
  return mUtil->loopNetDrivers(clkElab);
}

SCHUtil::DerivedClockFlowsLoop
SCHUtil::loopDerivedClockFlows(SCHDerivedClockBase* dclBase) const
{
  return DerivedClockFlowsLoop(dclBase->loopClocks(), FlowLoopGen(this));
}

const NUNetElab* SCHUtil::getClock(const STSymbolTableNode* node) const
{
  const NUBase* base = CbuildSymTabBOM::getNucleusObj(node);
  const NUNetElab* clkElab = dynamic_cast<const NUNetElab*>(base);
  NU_ASSERT(clkElab != NULL, base);
  return clkElab;
}

const SCHScheduleMask* SCHUtil::getInputMask(void) const
{
  return mFactory->getInputMask();
}

const SCHScheduleMask* SCHUtil::getOutputMask(void) const
{
  return mFactory->getOutputMask();
}

const SCHScheduleMask* SCHUtil::getConstantMask(void) const
{
  return mFactory->getConstantMask();
}

void SCHUtil::findOnDemandNets(FLNodeElab *fl_elab, NetToNetElabMap *ondemand_nets, NetToNetElabMap *excluded_nets, FLNodeElabSet *covered_nodes, bool check_type)
{
  bool save_net = false;        // do we need to save the net def'ed here?
  bool recurse = true;          // do we need to recurse over the fanin and nested blocks?

  // We need to exclude cycle nodes, since they're not valid in this context
  if (fl_elab->isEncapsulatedCycle())
    return;

  // We only care about normal nodes, primary inputs/bids, and deposits/forces
  FLTypeT type = fl_elab->getType();
  if ((type != eFLNormal) && (type != eFLBoundPort) && (type != eFLBoundProtect))
    return;

  bool cont_driver = isContDriver(fl_elab);
  if (check_type) {
    // When called on a node that is being scheduled, we need to
    // check its type to decide what to do
    if (isSequential(fl_elab)) {
      // Sequential scheduled
      // Definitely save the net def'ed here.
      save_net = true;
    } else if (!fl_elab->isSampled()) {
      // Transition scheduled
      // Definitely save the net def'ed here.
      save_net = true;
      // However, there's no need to go further with this node
      recurse = false;
    }
  } else {
    // When called recursively to analyze nested flow, we only save
    // nets that are not continous drivers
    save_net = !cont_driver;
  }

  if (save_net) {
    // This net should be part of the OnDemand state, so include it,
    // unless the user has explicitly requested that it be excluded.
    NUNetElab *net_elab = fl_elab->getDefNet();
    STAliasedLeafNode *st_node = net_elab->getSymNode();
    NUNet *net = net_elab->getStorageNet();
    if (!(mIODB->isOnDemandExcluded(st_node))) {
      addOnDemandNet(ondemand_nets, net, net_elab);
    } else {
      addOnDemandNet(excluded_nets, net, net_elab);
    }

    // Have we been here before?
    if (covered_nodes->insertWithCheck(fl_elab) == false)
      return;

    // Regardless of whether a net was excluded, we still need to
    // traverse its fanin.
    if (recurse) {
      // Loop over the fanin
      for (FLNodeElabLoop l(fl_elab->loopFanin()); !l.atEnd(); ++l) {
        findOnDemandNets(*l, ondemand_nets, excluded_nets, covered_nodes, false);
      }

      // Loop over any nested blocks
      for (FLNodeElabLoop l(fl_elab->loopNested()); !l.atEnd(); ++l) {
        findOnDemandNets(*l, ondemand_nets, excluded_nets, covered_nodes, false);
      }
    }
  }
}

void SCHUtil::addOnDemandNet(NetToNetElabMap *netMap, NUNet *net, NUNetElab *netElab)
{
  // Find/create the NUNetElabSet for this NUNet
  NUNetElabSet *netSet;
  NetToNetElabMap::iterator iter = netMap->find(net);
  if (iter == netMap->end()) {
    netSet = new NUNetElabSet;
    (*netMap)[net] = netSet;
  } else {
    netSet = iter->second;
  }
  // Add the elaborated net
  netSet->insert(netElab);
}

void SCHUtil::removeOnDemandNet(NetToNetElabMap *netMap, NUNet *net)
{
  // Deallocate the NUNetElab set associated with this NUNet, and
  // erase its entry in the map.
  NetToNetElabMap::iterator iter = netMap->find(net);
  if (iter != netMap->end()) {
    NUNetElabSet *netSet = iter->second;
    delete netSet;
    netMap->erase(iter);
  }
}
