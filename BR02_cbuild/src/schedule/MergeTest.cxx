// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  File to test whether a block is mergable or not
*/

#include "iodb/IODBNucleus.h"

#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUExpr.h"

#include "MarkDesign.h"
#include "FlowGroups.h"
#include "MergeTest.h"

SCHMergeTest::SCHMergeTest(SCHMarkDesign* mark, const NUNetElabSet* unmergableNets,
                           bool sequential, bool combinational) :
  mMarkDesign(mark), mUnmergableNets(unmergableNets),
  mSequential(sequential), mCombinational(combinational)
{
  std::memset(mMergeTypes, 0, sizeof(mMergeTypes));
  mCycleBlocks = new NUCUseDefSet;
  mMarkDesign->findCycleBlocks(mCycleBlocks);
  mFlowSeqFanouts = new FlowSeqFanouts;
}

SCHMergeTest::~SCHMergeTest()
{
  delete mCycleBlocks;
  delete mFlowSeqFanouts;
}

bool SCHMergeTest::mergable(const NUUseDefNode* useDef,
                            const SCHFlowGroups* flowGroups)
{
  MergeTypes type;
  if (SCHUtil::isSequential(useDef)) {
    type = sequentialBlockMergable(useDef, flowGroups);
  } else {
    type = combinationalBlockMergable(useDef, flowGroups);
  }

  // Update stats and return whether this is mergable
  ++mMergeTypes[type];
  return type == eMergable;
}

void SCHMergeTest::recordCycleBlock()
{
  ++mMergeTypes[eUnMergeFalseCycle];
}

void SCHMergeTest::printStats()
{
  // Count the number of blocks
  int unMergable = 0;
  for (int i = 0; i < eMergable; ++i) {
    unMergable += mMergeTypes[i];
  }
  int total = unMergable + mMergeTypes[eMergable];
  if (total > 0) {
    UtIO::cout()
      << "Mergable blocks = " << mMergeTypes[eMergable]
      << " (" << (mMergeTypes[eMergable] * 100) / total << "%)"
      << ", Umergable blocks = " << unMergable
      << " (" << (unMergable * 100) / total << "%)";
    for (int i = 0; i < eMergable; ++i) {
      if (mMergeTypes[i] != 0) {
        UtIO::cout() << "\n  " << mergeTypeStr(MergeTypes(i)) << ": " << mMergeTypes[i];
      }
    }
    UtIO::cout() << "\n";
  }
} // void SCHMergeTest::printStats

SCHMergeTest::MergeTypes
SCHMergeTest::sequentialBlockMergable(const NUUseDefNode* useDef,
                                      const SCHFlowGroups* flowGroups)
{
  // Check for control flow nets and state update nets. Control flow
  // nets cause problems when they make dead flow live. State update
  // nets can't be merged because any local reads of the memory will
  // see the new value. So we can't merge a state update with a reader
  // of it.
  NUNetSet nets;
  useDef->getDefs(&nets);
  for (NUNetSetIter i = nets.begin(); i != nets.end(); ++i) {
    NUNet* net = *i;
    if (net->isDoubleBuffered()) {
      return eUnMergeSeqStateUpdate;
    } else if (net->isControlFlowNet()) {
      return eUnMergeCntrlFlow;
    }
  }

  // Check if any edge is part of a vector. Disallow any merging
  // involving vectored clocks; we cannot differentiate between
  // different clock bits.
  const NUAlwaysBlock* always = dynamic_cast<const NUAlwaysBlock*>(useDef);
  if (SCHUtil::isSequential(useDef)) {
    NUEdgeExpr* edge_expr = always->getEdgeExpr();
    NUNet* edge_net = edge_expr->getNet();
    if (!edge_net->isBitNet()) {
      return eUnMergeSeqVecEdge;
    }
  }

  // Merging c-models breaks EMC, not sure why yet. If this becomes a
  // performance problem somewhere, we can investigate. The only
  // possibility is that we can optimize some of the logic to get the
  // data in and out of a c-model.
  if (always->isCModelCall()) {
    return eUnMergeCModel;
  }

  // Merging inactive sequentials doesn't make sense, they are going
  // to be removed from the schedule.
  for (SCHFlowGroups::FlowsLoop l = flowGroups->loopFlowNodes(); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    if (flowElab->isInactive()) {
      return eUnMergeSeqInactive;
    }
  }

  return eMergable;
} // SCHMixedBlockMerge::sequentialBlockMergable

SCHMergeTest::MergeTypes
SCHMergeTest::combinationalBlockMergable(const NUUseDefNode* useDef,
                                         const SCHFlowGroups* flowGroups)
{
  // If the use def is not mergable then we give up
  MergeTypes type;
  type = isCombUseDefMergable(useDef);
  if (type != eMergable) {
    return type;
  }

  // Check if there are any problems with any of the flow nodes
  type = areCombFlowsMergable(flowGroups);
  if (type != eMergable) {
    return type;
  }

  // Make sure that all groups have the same set of flow nodes in them
  if (!mSequential && !validCombFlowGroups(useDef, flowGroups)) {
    return eUnMergeCombGrouping;
  }

  // If we are running the mixed merge, check if the combinational
  // block feeds multiple outputs.
  if (mSequential && mCombinational) {
    type = multiOutputTest(flowGroups);
  }

  return type;
}

SCHMergeTest::MergeTypes
SCHMergeTest::isCombUseDefMergable(const NUUseDefNode* useDef)
{
  // Check some usedef based mergability issues
  MergeTypes type = eMergable;
  if (useDef->isInitial()) {
    // No reason to merge initial blocks since they should not execute
    // outside the initial schedule. Merging them into a normal combo
    // block would make it run when it shouldn't.
    type = eUnMergeCombInitial;

  } else if (useDef->isCycle() ||
             (mCycleBlocks->find(useDef) != mCycleBlocks->end())) {
    // Making cycles bigger doesn't make sense
    type = eUnMergeCombCycle;

  } else if (useDef->getStrength() != eStrDrive) {
    // Non-strong drivers are not mergable yet
    type = eUnMergeCombStrength;

  } else if (mSequential && (useDef->getType() == eNUContEnabledDriver)) {
    // Enabled drivers aren't mergable into flops
    type = eUnMergeCombEnabledDriver;

  } else {
    // Test for a c-model call
    const NUAlwaysBlock* always = dynamic_cast<const NUAlwaysBlock*>(useDef);
    if ((always != NULL) && always->isCModelCall()) {
      type = eUnMergeCModel;
    }
  }

  return type;
} // SCHMergeTest::MergeTypes SCHMergeTest::isUseDefMergable

SCHMergeTest::MergeTypes
SCHMergeTest::areCombFlowsMergable(const SCHFlowGroups* flowGroups)
{
  // Check if it is mergable. We can't merge cycles, drivers
  // of strength other than eStrDrive, and enabled drivers that only
  // write to part of a primary bid. This is because expression
  // synthesis cannot handle some configurations of merged enabled
  // drivers. See test/shell/tristate2 for a test case that exhibits
  // this problem.
  MergeTypes type = eMergable;
  IODBNucleus* iodb = mMarkDesign->getUtil()->getIODB();
  for (SCHFlowGroups::FlowsLoop l = flowGroups->loopFlowNodes();
       !l.atEnd() && (type == eMergable); ++l) {
    FLNodeElab* flowElab = *l;
    if (SCHMarkDesign::isCycle(flowElab)) {
      type = eUnMergeCombCycle;

    } else {
      NUUseDefNode* useDef = flowElab->getUseDefNode();
      if ((useDef != NULL) &&
          (useDef->getType() == eNUContEnabledDriver) &&
          isPartialPrimaryBid(flowElab)) {
        type = eUnMergeCombEnabledDriver;

      } else {
        // Check if this is a net we should not merge
        NUNetElab* netElab = flowElab->getDefNet();
        if ((mUnmergableNets != NULL) &&
            (mUnmergableNets->find(netElab) != mUnmergableNets->end())) {
          type = eUnMergeNet;

        } else if (netElab->getNet()->isControlFlowNet()) {
          // Can't merge control flow nets because it adds unexpected
          // control dependencies that weren't there before the merge.
          type = eUnMergeCntrlFlow;

        } else if (mSequential) {
          // Mixing sequentials and combinationals, can't merge
          // combinationals that must be accurate
          if (flowElab->mustBeTransition()) {
            type = eUnMergeCombTrans;
          } else if (flowElab->getFLNode()->getDefNet()->isProtected(iodb)) {
            type = eUnMergeCombProtected;
          } else {
            // We should never merge derived clock logic into
            // flops. This was found by bug5995 but fixed upstream.
            FLN_ELAB_ASSERT(!flowElab->isClock() &&
                            !flowElab->isDerivedClockLogic(),
                            flowElab);
          }
        }
      }
    }
  } // for

  return type;
} // SCHMergeTest::areCombFlowsMergable

bool 
SCHMergeTest::validCombFlowGroups(const NUUseDefNode* useDef,
                                  const SCHFlowGroups* flowGroups)
{
  // Find all the hidden flows for every instance of this block. We
  // need those because they should not count against a block group
  // not being complete.
  //
  // There are two types of hidden flows. Flows that represent
  // statements for which values are not read outside the block and
  // statements which compute dead values.
  //
  // This only matters if there are multiple instances of the block
  // and in once instance the flow is hidden and in the other it is
  // not. This results in the existence of an unelaborated flow for
  // the statement but no elaborated flows wherever it is hidden.
  HiddenFlows* hiddenFlows = findHiddenFlows(flowGroups);

  // Go through the various groups and populate the map of flow nodes
  // to the groups they are in. We also populate the full map. Every
  // group must match the full map.
  UInt32 group = 0;
  DynBitVector fullGroup;
  GroupMap groupMap;
  for (SCHFlowGroupsLoop g = flowGroups->loopGroups(); !g.atEnd(); ++g, ++group) {
    // Set the bit for the full group
    if (fullGroup.size() <= group)
      fullGroup.resize(group+1);
    fullGroup[group] = true;

    // Set the bit for the flow nodes in this group. While we are
    // there, gather the hierarchy for this group. It must be the same
    // for all elaborated flows.
    const FLNodeElabVector* nodes = *g;
    HierName* hierName = NULL;
    for (FLNodeElabVectorCLoop n(*nodes); !n.atEnd(); ++n) {
      // Add this group to the unelaborated flow
      FLNodeElab* flowElab = *n;
      const FLNode* flowUnelab = flowElab->getFLNode();
      addGroupFlow(group, flowUnelab, &groupMap);
      
      // Gather the hierarchy
      if (hierName == NULL) {
        hierName = flowElab->getHier();
      } else {
        FLN_ELAB_ASSERT(hierName == flowElab->getHier(), flowElab);
      }
    }

    // Add the hidden flow for this hierachy to the groups
    NU_ASSERT(hierName != NULL, useDef);
    HiddenFlowsLoop l;
    for (l = loopHiddenFlows(hierName, *hiddenFlows); !l.atEnd(); ++l) {
      FLNode* flowUnelab = *l;
      addGroupFlow(group, flowUnelab, &groupMap);
    }
  } // for

  // Go through all the groups and make sure they are full. This means
  // every instance has the same set of flows.
  bool mergable = true;
  for (GroupMapLoop l(groupMap); !l.atEnd() && mergable; ++l) {
    DynBitVector* flowGroups = l.getValue();
    if (*flowGroups != fullGroup) {
      mergable = false;
    }
  }

  // Don't need the memory anymore
  deleteGroupMap(&groupMap);
  deleteHiddenFlows(hiddenFlows);

  return mergable;
} // SCHMergeTest::validCombFlowGroups

void
SCHMergeTest::addGroupFlow(UInt32 group, const FLNode* flow, 
                           GroupMap* groups)
{
  // Get the dynamic bitvector for this unelaborated flow
  DynBitVector* flowGroups;
  GroupMap::iterator pos = groups->find(flow);
  if (pos == groups->end()) {
    flowGroups = new DynBitVector;
    groups->insert(GroupMap::value_type(flow, flowGroups));
  } else {
    flowGroups = pos->second;
  }

  // Set the bit in the dynamic bit vector. They don't resize
  // themselves correctly so make sure it has the right size.
  if (flowGroups->size() <= group) {
    flowGroups->resize(group+1);
  }
  flowGroups->set(group);
}

void SCHMergeTest::deleteGroupMap(GroupMap* groupMap)
{
  for (GroupMapLoop l(*groupMap); !l.atEnd(); ++l) {
    DynBitVector* flowGroups = l.getValue();
    delete flowGroups;
  }
  groupMap->clear();
}

bool SCHMergeTest::isPartialPrimaryBid(FLNodeElab* flow) const
{
  // If it is not a primary bid we can return false
  if (!mMarkDesign->isPrimaryBidi(flow))
    return false;

  // It is a primary bid, check if we are only writing some of the
  // bits
  NUNetRefHdl netRef = flow->getFLNode()->getDefNetRef();
  return !netRef->all();
}

SCHMergeTest::HiddenFlows*
SCHMergeTest::findHiddenFlows(const SCHFlowGroups* flowGroups) const
{
  // Find all the unelaborated flows for this block. Initialize the
  // hiddenFlows map.
  FLNodeSet unelabFlows;
  HiddenFlows* hiddenFlows = new HiddenFlows;
  for (SCHFlowGroups::FlowsLoop l = flowGroups->loopFlowNodes(); !l.atEnd(); ++l) {
    // Add this unelaborated flow noden
    FLNodeElab* flowElab = *l;
    FLNode* flowUnelab = flowElab->getFLNode();
    unelabFlows.insert(flowUnelab);

    // If we haven't created the set for this hierarchy, do so now
    HierName* hierName = flowElab->getHier();
    if (hiddenFlows->find(hierName) == hiddenFlows->end()) {
      FLNodeSet* flows = new FLNodeSet;
      hiddenFlows->insert(HiddenFlows::value_type(hierName, flows));
    }
  }

  // Add all the unelaborated flows to each hierarchy
  for (LoopMap<HiddenFlows> d(*hiddenFlows); !d.atEnd(); ++d) {
    FLNodeSet* flows = d.getValue();
    *flows = unelabFlows;
  }

  // Now remove the unelaborated flows that are live
  for (SCHFlowGroups::FlowsLoop l = flowGroups->loopFlowNodes(); !l.atEnd(); ++l) {
    // Find the flow set for this elaborated flow's hierarchy
    FLNodeElab* flowElab = *l;
    HierName* hierName = flowElab->getHier();
    HiddenFlows::iterator pos = hiddenFlows->find(hierName);
    FLN_ELAB_ASSERT(pos != hiddenFlows->end(), flowElab);
    FLNodeSet* flows = pos->second;

    // Remove the unelaborated flow from the hidden set; it is live
    FLNode* flowUnelab = flowElab->getFLNode();
    flows->erase(flowUnelab);
  }
  return hiddenFlows;
} // SCHMergeTest::findHiddenFlows

SCHMergeTest::HiddenFlowsLoop
SCHMergeTest::loopHiddenFlows(HierName* hierName,
                              const HiddenFlows& hiddenFlows) const
{
  HiddenFlows::const_iterator pos = hiddenFlows.find(hierName);
  FUNC_ASSERT(pos != hiddenFlows.end(), hierName->print());
  FLNodeSet* flows = pos->second;
  return HiddenFlowsLoop(*flows);
}

void SCHMergeTest::deleteHiddenFlows(HiddenFlows* hiddenFlows) const
{
  for (LoopMap<HiddenFlows> l(*hiddenFlows); !l.atEnd(); ++l) {
    FLNodeSet* flows = l.getValue();
    delete flows;
  }
  delete hiddenFlows;
}
  
const char* SCHMergeTest::mergeTypeStr(MergeTypes type)
{
  const char* retStr = "";
  switch(type) {
    case eUnMergeCombCycle:
      retStr = "Cycle           ";
      break;

    case eUnMergeCombStrength:
      retStr = "Strength        ";
      break;

    case eUnMergeCombEnabledDriver:
      retStr = "Enabled Driver  ";
      break;

    case eUnMergeCombInitial:
      retStr = "Initial         ";
      break;

    case eUnMergeCModel:
      retStr = "C-Model         ";
      break;

    case eUnMergeFalseCycle:
      retStr = "False Cycle     ";
      break;

    case eUnMergeCombGrouping:
      retStr = "Grouping        ";
      break;

    case eUnMergeCombOutput:
      retStr = "Feeds Output    ";
      break;

    case eUnMergeSeqInactive:
      retStr = "Inactive        ";
      break;

    case eUnMergeSeqStateUpdate:
      retStr = "State Update    ";
      break;

    case eUnMergeCntrlFlow:
      retStr = "Control Flow Net";
      break;

    case eUnMergeSeqVecEdge:
      retStr = "Vector Edge Net ";
      break;

    case eUnMergeCombFlowMulti:
      retStr = "Flow Multi Out  ";
      break;

    case eUnMergeCombBlockMulti:
      retStr = "Block Multi Out ";
      break;

    case eUnMergeCombProtected:
      retStr = "Protected       ";
      break;

    case eUnMergeNet:
      retStr = "Umergable Net   ";
      break;

    case eUnMergeCombTrans:
      retStr = "Transition Combo";
      break;

    case eMergable:
    case eNumMergeTypes:
      INFO_ASSERT(false, "Unmergable string not valid for mergable or type size");
      retStr = NULL;
      break;
  } // switch

  return retStr;
} // const char* SCHMergeTest::mergeTypeStr

// This routine records the sequential fanouts for every combinational
// flow. However, recording all fanouts is an N*M computation. All
// that is needed is to know whether a single flow or block fanouts
// out to more than one sequential block. So the code rturns once the
// fanout count exceeds 1.
bool
SCHMergeTest::recordCombFlow(FLNodeElab* flowElab, SCHScheduleBase* sched,
                             FLNodeElab* seqFlow)
{
  // Find the spot for this info or allocate it. If there are already
  // two entries, then we return because there is not need to keep
  // recording (a flow with two fanouts is already not mergable). This
  // is a compile performance optimization.
  SeqFanouts& seqFanouts = (*mFlowSeqFanouts)[flowElab];
  if (seqFanouts.size() > 1) {
    return false;
  }

  // Check it already exists. We can do a search of the vector because
  // we should not get here if this gets bigger than 1 entry.
  SCHUseDefHierPair seqUDHierPair(seqFlow);
  SCHSeqFanout seqFanout(seqUDHierPair, sched);
  bool found = false;
  FLN_ELAB_ASSERT(seqFanouts.size() <= 1, seqFlow);
  for (SeqFanoutsLoop l(seqFanouts); !l.atEnd() && !found; ++l) {
    const SCHSeqFanout& curSeqFanout = *l;
    found = (seqFanout == curSeqFanout);
  }

  // If it wasn't found, add it. Return true since the sequential
  // fanout should be pushed to all its fanin.
  if (!found) {
    seqFanouts.push_back(seqFanout);
    return true;
  } else {
    // We have already pushed this info down so no need to do it again
    return false;
  }
} // SCHMixedBlockMerge::recordCombFlow
    
// Check if any flow has multiple outputs, then the block is not
// mergable. If all flows have one output, then check if the block has
// multiple outputs, again the block is not mergable unless it is
// split.
SCHMergeTest::MergeTypes
SCHMergeTest::multiOutputTest(const SCHFlowGroups* flowGroups)
{
  // Check each flow on its own. Gather the block sequential fanouts
  // for the follow on computation.
  typedef UtSet<SCHSeqFanout> SeqFanoutSet;
  typedef UtMap<SCHUseDefHierPair, SeqFanoutSet> BlockSeqFanout;
  BlockSeqFanout blockSeqFanout;
  MergeTypes type = eMergable;
  for (SCHFlowGroups::FlowsLoop l = flowGroups->loopFlowNodes();
       !l.atEnd() && (type == eMergable); ++l) {
    // Test for a multi output flow
    FLNodeElab* flowElab = *l;
    const SeqFanouts& seqFanouts = (*mFlowSeqFanouts)[flowElab];
    FLN_ELAB_ASSERT(seqFanouts.size() > 0, flowElab);
    if (seqFanouts.size() > 1) {
      type = eUnMergeCombFlowMulti;
    }

    // Gather flow outputs to test a block instance's set of outputs
    SCHUseDefHierPair udHierPair(flowElab);
    SeqFanoutSet& seqFanoutSet = blockSeqFanout[udHierPair];
    seqFanoutSet.insert(seqFanouts.begin(), seqFanouts.end());
  }

  // Test to see if any blocks have multiple fanouts
  typedef LoopMap<BlockSeqFanout> BlockSeqFanoutLoop;
  for (BlockSeqFanoutLoop l(blockSeqFanout); 
       !l.atEnd() && (type == eMergable); ++l) {
    // Create a bunch of artifical seqFanout class for unmergable combos
    SeqFanoutSet& seqFanouts = l.getNonConstValue();
    if (seqFanouts.size() > 1) {
      type = eUnMergeCombBlockMulti;
    }
  }

  return type;
} // SCHMergeTest::multiOutputTest


