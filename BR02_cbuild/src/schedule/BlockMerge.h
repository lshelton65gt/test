// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  A class to analyze the combinational schedules and merge blocks when
  possible
*/

#ifndef _BLOCKMERGE_H_
#define _BLOCKMERGE_H_

#include "util/UniqueSet.h"
#include "util/GenericBigraph.h"
#include "util/GraphMerge.h"

#include "FlowGroups.h"

class HierName;

//! Abstraction that defines various merge types
enum SCHMergeTypes {
  eMTNetDepth,          //!< Merge blocks by net and depth
  eMTDepth,             //!< Merge blocks by depth
  eMTReverseDepth,      //!< Merge blocks by reverse depth
  eMTFanin,             //!< Merge blocks by fanin
  eMTDepthOrder,        //!< Merge blocks by depth in depth order
  eMTNumMerges          //!< The number of merge type
};

//! Define to create a merge mask
#define SCHMergeMask(type) (1 << (type))

//! Define for all merge types
#define SCHAllMerges (SCHMergeMask(eMTNumMerges) - 1)

//! Define to test for a merge flag
#define SCHMergePresent(mask, type) ((SCHMergeMask(type) & (mask)) != 0)

//! Test for multiple merges
#define SCHAnyMergesPresent(mask1, mask2) (((mask1) & (mask2)) != 0)

//! Define for all the merges that require an acyclic graph
#define SCHAcyclicMerges (SCHMergeTypes(SCHMergeMask(eMTNetDepth) | \
                                  SCHMergeMask(eMTDepth) | \
                                  SCHMergeMask(eMTReverseDepth) | \
                                  SCHMergeMask(eMTFanin)))

//! SCHBlockMerge class
/*! This implements a class that merges blocks together to reduce the
 *  number of functions, optimize blocks further and reduces the
 *  number of global nets. A global net is one which is written in one
 *  block and then read in another. By combining two blocks we can
 *  make a net written and read in the same block allowing us to
 *  declare the net on the stack instead of on the heap. This will
 *  allow the compiler to generate more optimal code.
 *
 *  The following must be true to merge two unelaborated blocks:
 *
 *  1. The two blocks must be in the same scope
 *
 *  2. The two blocks must be mergable in every instance of the block.
 *
 *  3. The two blocks must be in the same schedules
 *
 *  4. Potentially additional restrictions depending on the type of
 *     blocks being merged (sequential, combinational, or both).
 *
 *  To achieve the above every scheduled block instance for the same
 *  NUUseDefNode is put into one MergeBlock. Then a graph is created
 *  by mapping the elaborated flow graph onto the MergeBlock
 *  graph. This may introduce cycles which must be eliminated. Once
 *  that is done this class uses functions in the UtGraphMerge class
 *  to implement the merge.
 *
 *  See the start(), createBlock(), and mergeBlocks() routines for how
 *  to start the process.
 *
 *  See utilities: findFlowBlocks() to help create the graph,
 *  gatherBlockFlows() to print information about a merge operation,
 *  and breakUpBlock() to remove cycles in the graph (due to mapping
 *  elaborated graph onto unelaborated graph).
 *
 *  In addition, there are a number of abstract virtual functions that
 *  must be defined to run the merge process. They are:
 *  getBlockFanin(), blockMergable(), edgeMergable(),
 *  updateScheduledBlocks(), and compareBuckets().
 */
class SCHBlockMerge : public UtGraphMerge
{
public:
  //! constructor
  SCHBlockMerge(SCHUtil*, SCHMarkDesign*, SCHScheduleData*, bool reverseEdge);

  //! destructor
  ~SCHBlockMerge();

protected:
  // The following sections contain types and routines that should
  // only be accessed by the derived class.

  //! Function to initialize block merging
  /*! This function performs initialization such as starting any
   *  statistical timers.
   */
  void start();

  //! Function to add a block that could be potentially merged
  /*! This function should be called on every block that should be merged.
   *
   *  \param flowElab one of the elaborated flows for this block.
   *
   *  \param nodes a vector of elaborated flow for a given schedule
   *  block instance. In some cases this may have some missing
   *  elaborated flow (combinational blocks that could not get split
   *  to break a false cycle).
   *
   *  \param schedBase The schedule this block instance is in
   */
  void createBlock(FLNodeElab* flowElab, FLNodeElabVector* nodes,
                   SCHScheduleBase* schedBase);

  //! Function to start the block merge
  /*! This function performs some initialization and then merges the
   *  graph by depth and adjacent blocks that are mergable.
   *
   *  \param dumpMergedBlocks if true information will be printed
   *  about the merged blocks. This is also passed to the derived
   *  class to print information about failed merges.
   */
  void mergeBlocks(int dumpMergedBlocks, int mergeTypesMask = SCHAcyclicMerges);

  //! Function to return whether edges are reversed
  bool reversedEdges(void) const { return mReverseEdge; }

  //! Abstractions to keep a set of unique schedule sets.
  /*! This allows us to quickly determine if two blocks are in the
   *  same set of * schedules. A combinational block can be in more
   *  than one schedule * (e.g. input and combinational transition).
   */
  typedef UtUniqueSetFactory<SCHScheduleBase>::ItemSet ScheduleSet;
  typedef UtUniqueSetFactory<SCHScheduleBase>::CmpItemSets CmpScheduleSets;
  typedef UtMap<const STBranchNode*, const ScheduleSet*> ScheduleSets;
  typedef LoopMap<ScheduleSets> ScheduleSetsLoop;

  // The following type and data allows us to keep track of a unique
  // set of hierarchies and can be compared by a simple pointer.
  typedef UtUniqueSetFactory<const STBranchNode>::ItemSet HierSet;
  typedef UtUniqueSetFactory<const STBranchNode> UniqueHierSetFactory;
  typedef UtUniqueSetFactory<const STBranchNode>::ItemVector HierSetVec;

  //! MergeBlock class must be a friend to access types
  friend class MergeBlock;

  //! A class to organize sets of flow nodes by how they are scheduled
  class MergeBlock
  {
  public:
    //! constructor
    MergeBlock(FLNodeElabVector* nodes);

    //! destructor
    ~MergeBlock();

    //! Add another vector to this block
    /*! Note that this does not update flow. It is up to the caller to
     *  make sure that flow is computed after all vectors of flows have
     *  been added.
     */
    void addGroup(FLNodeElabVector* nodes);

    //! Iterate over the groups of elaborated flows
    SCHFlowGroupsLoop loopGroups() const;

    //! Iterator over the flow nodes that represent this block
    SCHFlowGroups::FlowsLoop loopFlowNodes() const;

    //! Check if this block is empty
    /*! A block can be empty after it has been merged with another
     *  block.
     */
    bool empty() const;

    //! Get the elaborated flow associated with this block
    /*! Note that after merging this block will have multiple
     *  elaborated flows with different use def nodes. So only use
     *  this if you need a representative block. To get the actual set
     *  of elaborated flow use loopFlowNodes().
     */
    FLNodeElab* getRepresentativeFlowElab() const;

    //! Walk all the elaborated block and pick one which is sorted first
    FLNodeElab* getSortedRepresentativeFlowElab(void) const;

    //! Get the use def node associated with this block
    /*! Note that after merging this block will have multiple use def
     *  nodes. So only use this if you need a representative block. To
     *  get the actual set of use def nodes use loopUseDefNodes().
     */
    NUUseDefNode* getRepresentativeUseDefNode() const;

    //! Get the schedule set for this block
    const ScheduleSet* getScheduleSet(const STBranchNode* hier) const;

    //! Get the schedule set for all instances
    const ScheduleSet* getAllInstancesScheduleSet() const;

    //! Get the hierarchies for this block
    const HierSet* getHierSet() const { return mHierSet; }

    //! Update the schedule set for this block
    void putScheduleSet(const STBranchNode* hier,
			const ScheduleSet* scheduleSet);

    //! Update the hier set for this block
    void putHierSet(const HierSet* hierSet) { mHierSet = hierSet; }

    //! Iterate over the schedule sets
    ScheduleSetsLoop loopScheduleSets() const;

    //! A function to test if a block is mergable
    bool isMergable() const { return mIsMergable; }

    //! Set a block to be unmergable
    void putUnmergable() { mIsMergable = false; }

    //! Debug routine to print the contents of the block
    void print(SCHMarkDesign* mark, bool printSignature = false);

    //! Routine to compare two blocks
    static int compare(const MergeBlock* b1, const MergeBlock* b2);

    //! Compare the hierarchy sets and associated schedule sets for two blocks.
    static int compareHierScheduleSets(const MergeBlock* b1,
                                       const MergeBlock* b2);

    //! Get an unmodifiable pointer to the block groups
    const SCHFlowGroups* getGroups() const { return mGroups; }

  private:
    // This bool indicates whether the block is mergable or not. We
    // need to keep it separate from the depth so that we can
    // recompute the depth and still have it be accurate through
    // unmerable blocks.
    bool mIsMergable;

    // The schedule sets for this block
    ScheduleSets* mScheduleSets;

    // The hier set for this block
    const HierSet* mHierSet;

    // If all the schedule sets are the same, this is the schedule set
    mutable const ScheduleSet* mScheduleSet;

    // The groups of elaborated flows that make up this block
    SCHFlowGroups* mGroups;
  }; // class MergeBlock : public SCHBlock

  //! Abstraction for all an array of blocks
  typedef UtVector<MergeBlock*> Blocks;

  //! Abstraction for an iterator over a vector of blocks
  typedef Loop<Blocks> BlocksLoop;

  //! Find the blocks for a given elaborated flow (returns an iterator)
  BlocksLoop findFlowBlocks(FLNodeElab* flowElab) const;

  //! Function to gather all the elaborated flow for a block
  static void gatherBlockFlows(const MergeBlock* block, FLNodeElabVector* flows);

  //! Function to break up a block into its individual pieces to break a cycle
  void breakUpBlock(MergeBlock* block);

  //! A class to keep track of information on edges
  class MergeEdge
  {
  public:
    //! constructor
    MergeEdge() : mCrossesHierarchy(false) {}

    //! destructor
    ~MergeEdge() {}

    //! Set that this edge crosses hierarchy
    void putCrossesHierarchy(bool cross) { mCrossesHierarchy = cross; }

    //! Get whether this edge crosses hierarchy or not
    bool getCrossesHierarchy(void) const { return mCrossesHierarchy; }

  private:
    //! Hide copy and assign constructors.
    MergeEdge(const MergeEdge&);
    MergeEdge& operator=(const MergeEdge&);

    //! A flag to keep track of whether this edge crosses hierarchy
    bool mCrossesHierarchy;
  }; // class MergeEdge

  // Utility classes
  SCHUtil* mUtil;
  SCHMarkDesign* mMarkDesign;
  SCHScheduleData* mScheduleData;

protected:
  // The following section contains abstract virtual functions that
  // must be implemented by the derived class as well as the necessary
  // types.

  //! Abstraction for a set of blocks
  typedef UtSet<MergeBlock*> BlockSet;

  //! Abstraction to loop over a set of blocks
  typedef Loop<BlockSet> BlockSetLoop;

  //! Abstract virtual function to get the fanin for a block
  /*! This is used to break false cycles and to set up the final graph
   *  for merging. Note that if the mReverse flag is set then a
   *  reverse edge is added given the fanin.
   */
  virtual void getBlockFanin(MergeBlock* block, BlockSet* faninSet) const = 0;

  //! Abstract virtual function to check if a block is a valid start block
  /*! This function will be called once per block created. This is
   *  used by the merge by fanin pass to see if we should start from
   *  this node.
   */
  virtual bool validStartBlock(const NUUseDefNode*, MergeBlock*) const = 0;

  //! Abstract virtual function to check if a block is mergable
  /*! This will be called once per block created during
   *  initialization. While the actual merging is going on, it will
   *  use the information cached in the MergeBlock class.  This means
   *  this function can be used to keep track of statistics.
   */
  virtual bool blockMergable(const NUUseDefNode* useDef,
                             MergeBlock* block) const = 0;

  //! Abstract virtual function to check if two blocks are mergable
  /*! The first block is the start block and the second is the to
   *  block as defined by getBlockFanin().  This is called when we are
   *  merging adjacent blocks and can be used to keep track of
   *  statistics.
   */
  virtual bool edgeMergable(const MergeEdge* edge, const MergeBlock* start,
                            const MergeBlock* to, bool dumpMergedBlocks) const = 0;

  //! Abstraction virtual function that is called before the merge
  /*! Once two blocks have been merged, there may be a need to update
   *  them. This gives the derived class the opportunity to do so.
   *
   *  \param lastBlock The block into which the merge occured.
   *
   *  \param block The block that was merged into lastBlock.
   */
  virtual void preMergeCallback(MergeBlock* lastBlock, MergeBlock* block) = 0;

  //! Abstract virtual function that is called after the merge
  /*! Once two blocks have been merged, there may be a need to update
   *  them. This gives the derived class the opportunity to do so.
   *
   *  \param lastBlock The block into which the merge occured.
   *
   *  \param block The block that was merged into lastBlock.
   */
  virtual void postMergeCallback(MergeBlock* lastBlock, MergeBlock* block) = 0;

  //! Abstract virtual function to sort blocks into mergable buckets
  /*! The two blocks are assumed to have the same depth, this is an
   *  additional sort routine to determine if two blocks can be
   *  merged.
   *
   *  Returns -1,0,1 for an ordering between to blocks
   */
  virtual int compareBuckets(const MergeBlock*, const MergeBlock*) const = 0;

  //! Abstract virtual function to record a cyclic block
  /*! This should be used to record when blocks are encapsulated to
   *  remove false cycles from the graph. These false cycles are there
   *  because we map the elaborated graph on to unelaborated blocks.
   */
  virtual void recordCycleBlock(const MergeBlock* /* block */) const = 0;

  void print(const GraphNode* node) const
  {
    mMergeGraph->castNode(node)->getData()->print(mMarkDesign, false);
  }

private:
  // This section contains the overrides for the virtual functions in
  // the UtGraphMerge class.

  //! Override virtual function to test if a node is a valid start node
  bool validStartNode(const GraphNode* node) const;

  //! Override virtual function to test if a node is mergable
  bool nodeMergable(const GraphNode* node) const;

  //! Override virtual function to test if an edge is mergable
  bool edgeMergable(const GraphEdge* edge) const;

  //! Override virtual function to merge a group of nodes
  /*! The nodes provided are in the order they should be merged. The
   *  LAST node in the vector is the one in which all the data should
   *  be placed. It will represent the list of nodes in the new graph.
   */
  void mergeNodes(const NodeVector& nodes);

  //! Override virtual function to merge two edges
  /*! This function updates the edge data. Currently that is only the
   *  crosses hierarchy boolean. The result should be the OR of the
   *  two booleans.
   */
  void mergeEdges(GraphEdge* dstEdge, const GraphEdge* srcEdge) const;

  //! Abstract virtual function to handle encapsulating a cycle in the graph
  virtual void encapsulateCycle(const NodeVector& nodes);

  //! Override virtual function to handle removing a node from the graph
  void removeMergedNode(GraphNode* node);

private:
  // This section contains private types and data for the merge
  // process.

  //! Abstraction for the set of blocks we are working on
  typedef UtHashMap<const NUUseDefNode*, MergeBlock*> BlockMap;

  //! A bidirected graph for the blocks to merge
  typedef GenericBigraph<MergeEdge*, MergeBlock*> MergeGraph;

  // The following type, data, and functions allows us to keep track
  // of unique sets of schedules that a block is in. That way simple
  // pointer comparisons can be used to see if two blocks are in the
  // same schedules.
  typedef UtUniqueSetFactory<SCHScheduleBase> UniqueScheduleSetFactory;
  UniqueScheduleSetFactory* mUniqueScheduleSetFactory;
  const ScheduleSet* createScheduleSet(const ScheduleSet*, SCHScheduleBase*);

  // The following type and data allows us to keep track of a unique
  // set of hierarchies and can be compared by a simple pointer.
  typedef UtUniqueSetFactory<const STBranchNode>::CmpItemSets CmpHierSets;
  UniqueHierSetFactory* mUniqueHierSetFactory;

  //! Abstraction to loop over all blocks (mergable and umergable)
  typedef LoopMulti<BlocksLoop> AllBlocksLoop;

  // The following are used to find blocks from flow nodes
  typedef UtMap<FLNodeElab*, Blocks*> FlowToBlocksMap;
  typedef FlowToBlocksMap::iterator FlowToBlocksMapIter;
  FlowToBlocksMap* mFlowToBlocksMap;

  // Functions to manage the flow to block map
  void createFlowToBlocksMap();
  void clearFlowToBlocksMap(MergeBlock* block);
  void updateFlowToBlocksMap(MergeBlock* block);
  void clearFlowToBlocksMap();

  //! Abstraction to deal with cycles of unmergable blocks during cycle breaking
  typedef UtMap<MergeBlock*, UInt32> BlockRankMap;

  //! Function to break cycles starting at a given block
  void breakCycles(MergeBlock* block, BlockSet* covered, BlockSet* busy);

  //! Recursive function to break false cycles
  MergeBlock* findAndBreakFalseCycles(MergeBlock* block, BlockSet* covered,
                                      BlockSet* busy, BlockRankMap* blockRankMap,
                                      UInt32 rank);

  //! Function to gather all the unique nodes for a block.
  /*! This is different from the gatherBlockFlows function defined
   *  above because it only gathers the flow nodes for the scheduled
   *  block. These may be different for combinational blocks that
   *  could not be split.
   */
  void gatherAllBlockFlows(MergeBlock* block, FLNodeElabVector* nodes) const;

  //! Function to print succesful merge results
  void printMergedNodes(MergeBlock* block, UIntPtr depth) const;

  // Count the number of merged blocks and groups
  UInt32 mMergedBlocks;
  UInt32 mMergeCount;

  //! Debug routine
  void printBlockFanin(MergeBlock* block);

  //! Function to iterate over mergable and unmergable blocks
  AllBlocksLoop loopAllBlocks();

  // Functions for the various block merging passes
  void initHierSets();
  void gatherMergableBlocks();
  void createBlockGraph();
  void breakFalseCycles();

  //! Function to compute all block pairs that cross hierarchy
  void computeAllCrossHierachyBlocks();

  //! Function to compute as single blocks fanin that crosss hierarchy
  void computeCrossHierachyBlocks(GraphNode* node, MergeBlock* block);

  //! Transpose the merge graph for reverse depth merging
  void transposeGraph(void);

  //! A class to organize mergable blocks into block buckets
  /*! Two blocks can be merged by depth if they are mergable and if
   *  they are in the same schedules and hierarchy. This class
   *  determines the latter. It uses the compareBuckets virtual
   *  function defined above to get additional information from the
   *  derived class.
   */
  class BlockBuckets : public UtGraphMerge::NodeCmp
  {
  public:
    //! constructor
    BlockBuckets(MergeGraph* mergeGraph, SCHBlockMerge* blockMerge) :
      mMergeGraph(mergeGraph), mBlockMerge(blockMerge) {}

    //! function to compare to blocks (so that they are put in buckets)
    bool operator()(const GraphNode*, const GraphNode*) const;

  private:
    MergeGraph* mMergeGraph;
    SCHBlockMerge* mBlockMerge;
  };

  //! BlockBuckets needs to be a friend to access compareBuckets
  friend class BlockBuckets;

  //! A class to organize mergable blocks into buckets by defs
  /*! Two blocks can be merged by depth if they are mergable and if
   *  they are in the same schedules and hierarchy. This class
   *  determines the latter. It additionally checks to see what nets
   *  are defed. Only if there is an itersection between the defs does
   *  it merge the blocks together. The theory is that if there are
   *  multiple defs to different bits of the same net, they might get
   *  vectorized.
   */
  class BlockNetBuckets : public UtGraphMerge::NodeCmp
  {
  public:
    //! constructor
    BlockNetBuckets(MergeGraph* mergeGraph, SCHBlockMerge* blockMerge) :
      mMergeGraph(mergeGraph), mBlockMerge(blockMerge) {}

    //! function to compare to blocks (so that they are put in buckets)
    bool operator()(const GraphNode*, const GraphNode*) const;

  private:
    MergeGraph* mMergeGraph;
    SCHBlockMerge* mBlockMerge;
  };

  //! BlockNetBuckets needs to be a friend to access compareBuckets
  friend class BlockNetBuckets;

  //! A class to sort mergable blocks
  /*! Blocks should be sorted to produce canonical results during
   *  merging.
   */
  class BlockOrder : public UtGraphMerge::NodeCmp
  {
  public:
    //! constructor
    BlockOrder(MergeGraph* mergeGraph) : mMergeGraph(mergeGraph) {}

    //! Function to compare to mergable blocks for order
    bool operator()(const GraphNode*, const GraphNode*) const;

  private:
    MergeGraph* mMergeGraph;
  };

  //! A class to sort edges in a graph of mergable blocks
  /*! There is no edge data so only sort by the end point
   */
  class EdgeOrder : public UtGraphMerge::EdgeCmp
  {
  public:
    //! constructor
    EdgeOrder(MergeGraph* mergeGraph) : mMergeGraph(mergeGraph) {}

    //! Function to compare two edges
    bool operator()(const GraphEdge*, const GraphEdge*) const;

  private:
    MergeGraph* mMergeGraph;
  };

  //! The block graph used for merging
  MergeGraph* mMergeGraph;

  //! The set of blocks in the design.
  /*! This structure is used to accumulate the blocks in the design.
   */
  BlockMap* mDesignBlockMap;

  //! An array of blocks that can be merged
  Blocks* mMergableBlocks;

  //! An array of blocks that cannot be merged
  Blocks* mUnMergableBlocks;

  //! Dump level (0: nothing, 1: merged blocks, other: everything)
  int mDumpMergedBlocks;

  //! If set, reverse edges are used in the graph
  bool mReverseEdge;

  //! If set, the cross hierarchy set has been populated
  bool mCrossesHierarchyValid;

  //! Used to find all flow nodes for a block filled by SCHUtil class
  SCHUtil::BlockToNodesMap* mBlockToNodesMap;

  //! Abstraction to keep track of all edges
  typedef UtVector<MergeEdge*> MergeEdges;

  //! Data to remember all allocated edges (for cleanup)
  MergeEdges* mMergeEdges;

}; // class SCHBlockMerge

#endif // _BLOCKMERGE_H_
