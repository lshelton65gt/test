// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/CarbonDBWrite.h"
#include "compiler_driver/CarbonContext.h"

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FLNodeElabCycle.h"

#include "iodb/IODBNucleus.h"
#include "iodb/ScheduleFactory.h"

#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAssign.h"

#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "exprsynth/ESPopulateExpr.h"
#include "exprsynth/ExprFactory.h"
#include "exprsynth/SymTabExpr.h"

#include "util/ArgProc.h"
#include "util/Stats.h"
#include "util/UtString.h"

#include "reduce/RETransform.h"

#include "schedule/Schedule.h"

#include "Util.h"
#include "ClkAnal.h"
#include "LatchAnal.h"
#include "Dump.h"
#include "MarkDesign.h"
#include "ScheduleData.h"
#include "TimingAnalysis.h"
#include "CreateSchedules.h"
#include "CycleDetection.h"
#include "BranchNets.h"
#include "BlockFlowNodes.h"

/*!
  \file
  Implementation of the Schedule class
  
  This is the main file that runs the scheduling process. The main
  routine to create the schedule is SCHSchedule::buildSchedule. See
  the function documentation for more details.
  
  The scheduling process is broken up into the following main parts:

  1. Marking the design (MarkDesign.cxx, ClkAnal.cxx)

  2. Cycle detection and breaking (CycleDetection.cxx CycleBreaker.cxx)

  3. Timing analysis (TimingAnalysis.cxx)

  4. Create the various schedules (CreateSchedules.cxx)

  5. Information printing routines (Dump.cxx)

  See those files for more details and information on other scheduler
  files.
*/

static const char* scSortByData = NULL;
static const char* scDisableInitCombos = NULL;
static const char* scAliasClocks = NULL;
static const char* scNoSampleSchedule = NULL;
static const char* scDumpUnmergedBlocks = NULL;
static const char* scDumpMultiSchedBlocks = NULL;
static const char* scNoMergeBlocks = NULL;
static const char* scDumpMergedBlocks = NULL;
static const char* scNoSplitBlocks = NULL;
static const char* scDumpSplitBlocks = NULL;
static const char* scNoFixClocksAsData = NULL;
static const char* scDumpClockAliases = NULL;
static const char* scVerboseClockAliases = NULL;
static const char* scDumpClockTree = NULL;
static const char* scDumpClockAsData = NULL;
static const char* scCondFlow = NULL;
static const char* scNoCondFlow = NULL;
static const char* scDumpStateUpdate = NULL;
static const char* scDumpTransitionNodes = NULL;
static const char* scNoScheduleSanity = NULL;
static const char* scScheduleMemorySanity = NULL;
static const char* scNoClockEquivalence = NULL;
static const char* scResolveBidis = NULL;
static const char* scDumpPIFlowFix = NULL;
static const char* scDebugExprSynthesis = NULL;
static const char* scDumpBufferedSequentials = NULL;
static const char* scDumpCycles = NULL;
static const char* scDumpCyclesEarly = NULL;
static const char* scNoCycleBreaking = NULL;
static const char* scNoSelfCycles = NULL;
static const char* scNoMergeDCL = NULL;
static const char* scBufferedMemoryThreshold = NULL;
static const char* scConvertLatches = NULL;
static const char* scDebugSchedule = NULL;
static const char* scNoMixedMerge = NULL;
static const char* scDumpCycleBdds = NULL;
static const char* scNoEnableExprSynthesis = NULL;
static const char* scFindClockLimit = NULL;

// Local flow scratch flags
#define SCH_CYCLE	(1<<0)

//! Class for Bidi enable expression synthesis
class SCHSchedule::SynthCB : public ESCallbackElab
{
public:
  SynthCB(FLNodeElab* starter, ESPopulateExpr* popExpr) : 
    ESCallbackElab(popExpr),
    mStartingPoint(starter),
    mMultiDriver(false)
  {}

  virtual bool stop(FLNodeElabVector* flows, const NUNetRefHdl& netRef, bool) {  
    // Check if we are at the starting flow, this means we hit a
    // nested unelaborated flow node, so we should continue.
    //
    // While we are looping, get the elab net for this set of flows
    bool isStart = false;
    NUNetElab* elabNet = NULL;
    for (FLNodeElabVectorIter i = flows->begin(); i != flows->end(); ++i)
    {
      FLNodeElab* flow = *i;
      isStart = (flow == mStartingPoint);
      if (elabNet == NULL)
        elabNet = flow->getDefNet();
      else
        FLN_ELAB_ASSERT(elabNet == flow->getDefNet(), flow);
    }
    
    if (!mMultiDriver && isStart)
      return false;
    
    STAliasedLeafNode* leaf = elabNet->getSymNode();
    bool stop = (leaf != NULL) || mMultiDriver;
    if (stop)
    {
      // make sure this leaf will be written to the db
      stop = (CbuildSymTabBOM::getNucleusObj(leaf) != NULL);
      // make sure that the associated net ref is not empty
      stop = stop && ! netRef->empty();
    }
    
    if (mMultiDriver)
      NU_ASSERT(stop, elabNet);
    return stop;
  }
  
private:
  FLNodeElab* mStartingPoint;
  bool mMultiDriver;
};

class SCHSchedule::ExprImplicitDriveWalker : public CarbonExprWalker
{
public:
  typedef UtHashSet<CarbonIdent*> IdentSet;

  ExprImplicitDriveWalker(IdentSet* identSet, CbuildSymTabBOM* bommanager) : 
    mIdentSet(identSet), mBomManager(bommanager)
  {}
  
  virtual ~ExprImplicitDriveWalker() {}
  
  virtual void visitIdent(CarbonIdent* ident) {
    SymTabIdent* symTabIdent = ident->castSymTabIdent();
    CE_ASSERT(symTabIdent, ident);
    
    // Get the describing expression, if any (could be optimized
    // away), and then walk that looking at each part of it finding
    // any primary outputs.
    CarbonExpr* describeExpr = mBomManager->getExpr(ident);
    if (describeExpr)
    {
      DriverSearchWalk walk(ident, mIdentSet);
      walk.visitExpr(describeExpr);
    }
  }
  
private:
  IdentSet* mIdentSet;
  CbuildSymTabBOM* mBomManager;

  class DriverSearchWalk : public CarbonExprWalker
  {
  public:
    DriverSearchWalk(CarbonIdent* fullIdent, IdentSet* identSet)
      : mFullIdent(fullIdent), mIdentSet(identSet)
    {}
    
    virtual ~DriverSearchWalk() {}
    
    virtual void visitIdent(CarbonIdent* ident) {
      DynBitVector useMask;
      const STAliasedLeafNode* leaf = ident->getNode(&useMask);
      NUNet* net = NUNet::find(leaf);
      if (net && net->isPrimaryOutput())
        mIdentSet->insert(ident);
    }

  private:
    CarbonIdent* mFullIdent;
    IdentSet* mIdentSet;
  };

  friend class DriverSearchWalk;
};


//! Object to facilitate bidi enable synthesis
/*!
  This is a helper object used by the populateIODB method. That method
  feed this calculator all the drivers. This tracks multi-drivers and
  strongly driven parts of a bidi and computes the overall enable
  expression for it.
*/
class SCHSchedule::EnableExprCalculator
{
private:

  //! Class to transform ESFlowIdents to SymtabIdents
  class FlowToSymtabExprTransform : public CopyWalker
  {
  public:
    FlowToSymtabExprTransform(ESFactory* exprFactory) : CopyWalker(exprFactory)
    {}
    
    virtual ~FlowToSymtabExprTransform() {}


    virtual CarbonExpr* transformIdent(CarbonIdent* ident)
    {
      DynBitVector useMask;
      STAliasedLeafNode* leaf = ident->getNode(&useMask);
      CE_ASSERT(leaf, ident);

      CarbonIdent* transformedIdent = NULL;
      CarbonIdent* nodeIdent = CbuildSymTabBOM::getIdent(leaf);
      if (nodeIdent)
      {
        // we could be looking at a node that represents an original
        // node (portsplit, for example).
        SymTabIdent* symtabIdent = nodeIdent->castSymTabIdent();
        ST_ASSERT(symtabIdent, leaf);
        SymTabIdentBP* symtabIdentBP = symtabIdent->castSymTabIdentBP();
        if (symtabIdentBP)
        {
          // This is a generated net that refers to the original net
          // Walk the backpointer and get the original net.
          CarbonExpr* bp = symtabIdentBP->getBackPointer();
          
          FlowToSymtabExprTransform nestedwalk(mFactory);
          nestedwalk.visitExpr(bp);
          // The factory already added the ident. Return here.
          return nestedwalk.getResult();
        }
        else
        {
          // This is an expressioned net, but the original net is used
          // in the enable synthesis. I don't think this is possible,
          // but no reason not to support it. Just make a SymTabIdent
          transformedIdent = new SymTabIdent(leaf, useMask, ident->getBitSize());
        }
      }
      else
      {
        // This is a simple unexpressioned net. Simply make a
        // SymTabIdent
        transformedIdent = new SymTabIdent(leaf, useMask, ident->getBitSize());
      }

      CE_ASSERT(transformedIdent, ident);
      bool added;
      CarbonIdent* ret = mFactory->createIdent(transformedIdent, added);
      if (! added)
        delete transformedIdent;
      return ret;
    }
    
  };

public:

  EnableExprCalculator(SCHUtil* util, NUNet* net) :
    mUtil(util), 
    mNet(net->getStorage()),
    mIsValidEnable(true)
  {
    mSynther = mUtil->getPopulateExpr();
    mExprFactory = mSynther->getFactory();
    mNetRefFactory = util->getNetRefFactory();
    mMsgContext = mUtil->getMsgContext();
    mAccumNetRef = mNetRefFactory->createEmptyNetRef();
  }

  ~EnableExprCalculator()
  {
    for (RefExprVecMap::iterator p = mNetRefToDrivers.begin(),
           e = mNetRefToDrivers.end(); p != e; ++p)
      delete p->second;
  }

  void processOutput(const DynBitVector& useMask)
  {
    // Here we are just running through the net and marking it
    // strongly driven
    NUNetRefHdl fullNetRef = mNetRefFactory->createNetRef(mNet);
    UInt32 netBitSize = mNet->getBitSize();
    NU_ASSERT(netBitSize == useMask.size(), mNet);
    for (UInt32 i = 0; i < netBitSize; ++i)
    {
      if (useMask.test(i))
      {
        NUNetRefHdl bitSlice = mNetRefFactory->sliceNetRef(fullNetRef, i);
        ConstantRange range(i,i);
        mapInternalDriverToNetRef(bitSlice, range);
      }
    }
    
  }
  
  void processDriver(FLNodeElab* fne)
  {
    NUEnabledDriver* ed = mUtil->findEnabledDriver(fne);
    NUNetRefHdl lhsFullNetRef = fne->getDefNetRef(mNetRefFactory);
    for (NUNetRefRangeLoop nrrl = lhsFullNetRef->loopRanges(mNetRefFactory);
         ! nrrl.atEnd() && isValid(); ++nrrl)
    {
      const ConstantRange& netRefRange = *nrrl;

      NUNetRefHdl lhsNetRef = nrrl.getCurrentNetRef();
      
      if (ed) {
        SynthCB synthcb(fne, mSynther);
        ESExprHndl elabExpr = mSynther->create(fne, &synthcb);
        CarbonExpr* driveExpr = elabExpr.getExpr();
        
        // If we can't get an expression this is a fatal problem for
        // cwavetestbench
        if (driveExpr == NULL) {
          mMsgContext->SchInvBidEnable(fne);
          // make sure a bidi enable expression is not created for
          // this
          mIsValidEnable = false;
        } else {
          
          bool drivesTrue;
          CarbonExpr* enable = driveExpr->getEnable(drivesTrue);
          
          // if there is no enable, the iodb reader caller should
          // consider the bidi undriven.
          if (enable != NULL)
          {
            if (! drivesTrue)
              enable = mExprFactory->createEqZero(enable, 1);
            else if (enable->getBitSize() > 1)
              enable = mExprFactory->createNeqZero(enable, 1);
            
            processEnableExpr(lhsNetRef, netRefRange, enable);
          }
          else
            mapExternalDriverToNetRef(lhsNetRef, netRefRange);
        }
      } // if
      else
      {
        NUContAssign* assign = dynamic_cast<NUContAssign*>(fne->getUseDefNode());
        if (assign && (assign->getStrength() == eStrDrive))
          mapInternalDriverToNetRef(lhsNetRef, netRefRange);
        // else assume undriven by the model
      }
    } // for netref ranges
  }
  

  //! Returns true if the enable for the net can be computed
  bool isValid() const { return mIsValidEnable; }

  //! Returns true if isValid() and at least one driver was processed
  bool hasEnableExpr() const {
    return isValid() && ! mNetRefToDrivers.empty();
  }
  
  //! Create the enable expression for the entire net. 
  /*!
    This assumes that hasEnableExpr() is true. Does not check. The
    caller should.
    
    The size of the expression will be the same size as the net. 
  */
  CarbonExpr* compute()
  {
    // First synthesize the multi-drivers into concats
    synthesizeMultiDrivers();

    // create an ESSortedExprMap. Note that this will assert if there
    // are any overlaps. So, multi-drivers must have been resolved and
    // synthesized at this point.
    ESSortedExprMap netRefConcatMap;
    for (RefExprVecMap::UnsortedLoop p = mNetRefToDrivers.loopUnsorted();
         ! p.atEnd(); ++p)
    {
      NUNetRefHdl netRef = p.getKey();
      CarbonExprVector* drivers = p.getValue();
      NU_ASSERT(drivers->size() == 1, netRef->getNet());
      CarbonExpr* expr = drivers->back();
      netRefConcatMap[netRef] = expr;
    }
    
    // create a concatenation of multidrive enables
    NUNetRefHdl completeNetRef = mNetRefFactory->createNetRef(mNet);
    CarbonExpr* flowBasedExpr = 
      mSynther->createNetRefMultiDriverConcat(completeNetRef, 
                                              mAccumNetRef,
                                              mNet->getBitSize(), false, 
                                              &netRefConcatMap, eValue0);
    // Transform the flow-based expression to a SymTabNode-based
    // expression
    FlowToSymtabExprTransform walker(mExprFactory);
    walker.visitExpr(flowBasedExpr);
    return walker.getResult();

  }
  
private:
  SCHUtil* mUtil;
  // The one and only net we are calculating the enable expression
  // for.
  NUNet* mNet;
  ESPopulateExpr* mSynther;
  ESFactory* mExprFactory;
  NUNetRefFactory* mNetRefFactory;
  MsgContext* mMsgContext;

  typedef UtHashMap<NUNetRefHdl, CarbonExprVector*> RefExprVecMap;
  // The map of netref bits to CarbonExprVectors of drivers
  /*!
    All enabled bits of a net are added to this map. Multi-drivers
    will have more than one element in the vector.
  */
  RefExprVecMap mNetRefToDrivers;
  
  // The bit ticker. Used to detect multi-drivers
  NUNetRefHdl mAccumNetRef;

  typedef UtHashSet<NUNetRefHdl> RefSet;
  // The set of strongly driven bits in this net
  RefSet mStronglyDrivenBits;
  
  bool mIsValidEnable;


  /*
    This calls addBitEnableExpr if the
    bit is not marked as strongly driven. 
  */
  void processBitDriver(NUNetRefHdl& bitSlice, CarbonExpr* enable)
  {
    if (mStronglyDrivenBits.find(bitSlice) == mStronglyDrivenBits.end())
      addBitEnableExpr(bitSlice, enable);
  }

  // Lowest level bit-to-enable mapping function
  void addBitEnableExpr(NUNetRefHdl& lhsNetRef, CarbonExpr* enable)
  {
    // lhsNetRef may have an alias net in it.
    NUNetRefHdl canonicalLhsNetRef = 
      mNetRefFactory->createNetRefImage(mNet, lhsNetRef);
    mAccumNetRef = mNetRefFactory->merge(mAccumNetRef, canonicalLhsNetRef);
    CarbonExprVector* exprVec = NULL;
    RefExprVecMap::iterator p = mNetRefToDrivers.find(canonicalLhsNetRef);
    if (p == mNetRefToDrivers.end())
    {
      exprVec = new CarbonExprVector;
      mNetRefToDrivers[canonicalLhsNetRef] = exprVec;
    }
    else
      exprVec = p->second;
    exprVec->push_back(enable);
  }

  /* Replaces the bit-enable driver expressions with a single new
     expression. Asserts if a mapping for the netref does not exist.
     This is called from synthesizeMultiDrivers. The new expression is
     a concat != 0 of the driver expression.
  */
  void replaceBitEnableExpr(NUNetRefHdl& lhsNetRef, CarbonExpr* newEnable)
  {
    RefExprVecMap::iterator p = mNetRefToDrivers.find(lhsNetRef);
    NU_ASSERT(p != mNetRefToDrivers.end(), lhsNetRef->getNet());
    CarbonExprVector* exprVec = p->second;
    exprVec->clear();
    exprVec->push_back(newEnable);
  }
  
  //! Process the enable for a range of a net.
  void processEnableExpr(NUNetRefHdl& lhsNetRef, const ConstantRange& netRefRange, 
                         CarbonExpr* enable)
  {
    for (SInt32 i = netRefRange.getLsb(); i <= netRefRange.getMsb(); ++i)
    {
      NUNetRefHdl bitSlice = mNetRefFactory->sliceNetRef(lhsNetRef, i);
      processBitDriver(bitSlice, enable);
    }
  }

  // Map a range of a net as never internally driven
  void mapExternalDriverToNetRef(NUNetRefHdl& lhsNetRef, const ConstantRange& netRefRange)
  {
    CarbonExpr* enable = mExprFactory->createConst(0, 1, false);
    processEnableExpr(lhsNetRef, netRefRange, enable);
  }

  // Map a range of a net as always internally (strongly) driven
  void mapInternalDriverToNetRef(NUNetRefHdl& lhsNetRef, const ConstantRange& netRefRange)
  {
    CarbonExpr* enable = mExprFactory->createConstAllOnes(1, false);
    // This is a strong driver and trumps all other enable
    // expressions. Once there is a strong driver on a bit, that bit
    // is no longer enabled.
    for (SInt32 i = netRefRange.getLsb(); i <= netRefRange.getMsb(); ++i)
    {
      NUNetRefHdl bitSlice = mNetRefFactory->sliceNetRef(lhsNetRef, i);

      // avoid overwriting a strong driver. Just keep the first one
      if (mStronglyDrivenBits.find(lhsNetRef) == mStronglyDrivenBits.end())
      {
        // if we have processed more than one driver we need to remove
        // this from the driver map.
        RefExprVecMap::iterator md = mNetRefToDrivers.find(bitSlice);
        if (md != mNetRefToDrivers.end())
        {
          CarbonExprVector* mdVec = md->second;
          mNetRefToDrivers.erase(md);
          delete mdVec;
        }
        
        mStronglyDrivenBits.insert(bitSlice);
        addBitEnableExpr(bitSlice, enable);
      }
    }
  }
  
  // Synthesizes any multidrivers that were found
  /*
    This creates a concat != 0 expression, where the concat is a
    concat of logical operators. This is done on each netref bit that
    is multiply-driven.
    So, for:
    assign       out[3:2] = en1 ? 2'b11 : 2'bz;
    assign       out[2] = en2 ? in : 1'bz;

    out[2]'s enable expression would be:
    {en1 == 0, en2 == 0} != 0

    This is called from compute() because we need to process all the
    drivers before we can synthesize multi-driven bits.
  */
  void synthesizeMultiDrivers()
  {
    for (RefExprVecMap::UnsortedLoop p = mNetRefToDrivers.loopUnsorted();
         ! p.atEnd(); ++p)
    {
      CarbonExprVector* bitEnables = p.getValue();      
      if (bitEnables->size() > 1)
      {
        // multi-driven enabled bit.
        
        // sanity check
        // Each expression in the multiEnables vector is size 1.
        for (CarbonExprVector::iterator q = bitEnables->begin(), r = bitEnables->end();
             q != r; ++q)
        {
          CarbonExpr* expr = *q;
          CE_ASSERT(expr->getBitSize() == 1, expr);
        }
        
        CarbonConcatOp* multiDriverConcat = 
          mExprFactory->createConcatOp(bitEnables, 1, bitEnables->size(), false);
        // create the logical operator (concat != 0)
        CarbonExpr* enable = mExprFactory->createNeqZero(multiDriverConcat, 1);
        
        NUNetRefHdl bitSlice = p.getKey();
        replaceBitEnableExpr(bitSlice, enable);      
      }
    }
  }
};

SCHCombinational* SCHSchedule::getInitialSchedule()
{
  return mScheduleData->getInitialSchedule();
}

SCHCombinational* SCHSchedule::getInitSettleSchedule()
{
  return mScheduleData->getInitSettleSchedule();
}

SCHCombinational* SCHSchedule::getDebugSchedule()
{
  return mScheduleData->getDebugSchedule();
}

SCHCombinational* SCHSchedule::getForceDepositSchedule()
{
  return mScheduleData->getForceDepositSchedule();
}

// This is multi-pass algorithm. The passes are as follows:
//
//   - Find the clocks and sequential blocks in a design
//
//   - Find any combinational cycles which are illegal.
//
//   - Calculate the conditions under which every node can change
//     (written)
//
//   - Calculate the conditions under which every node is sampled
//     (read)
//
//   - Figure out what logic must be run to produce the value for
//     a derived clock.
//
//   - Create the various schedules.
//
bool SCHSchedule::buildSchedule(NUDesign* design, Stats* stats,
                                bool phaseStats, bool dumpSchedule,
                                bool doCModelWrapper, 
                                TristateModeT tristateMode,
                                const char* fileRoot)
{
  // Start a new sub-interval timer for the scheduler
  if (phaseStats) {
    mUtil->putStats(stats);
  }
  UtStatsInterval statsInterval(phaseStats, stats);

  // Create the passes that are used in creating the schedule. We
  // allocate them here so that they get deleted when we are done.
  mCycleDetection = new SCHCycleDetection(mMarkDesign, mUtil, mScheduleData);
  mTimingAnalysis = new SCHTimingAnalysis(mUtil, mScheduleFactory,
					  mMarkDesign);
  mClkAnal = new SCHClkAnal(mUtil, mMarkDesign);
  mCreateSchedules = new SCHCreateSchedules(this, mScheduleFactory,
                                            mMarkDesign, mTimingAnalysis,
                                            mScheduleData, mUtil, mDump);

  // Set the flags requested by the caller
  bool preserveNets = mArgs->getBoolValue("-g");
  bool doAliasClocks = mArgs->getBoolValue(scAliasClocks) && 
    !preserveNets;

  // Set up the options for building schedules. The various schedule
  // passes can access what flags are set by calling the
  // SCHUtil::isFlagSet() function.
  //
  // TBD - not all flags have been moved to the SCHUtil class.
  SCHScheduleFlags flags = SCHScheduleFlags(0);
  if (!mArgs->getBoolValue(scNoSampleSchedule) && !preserveNets)
    flags = SCHScheduleFlags(flags | eUseSample);
  if (mArgs->getBoolValue(scSortByData))
    flags = SCHScheduleFlags(flags | eSortByData);
  if (mArgs->getBoolValue(scDisableInitCombos))
    flags = SCHScheduleFlags(flags | eDisableInitCombos);
  if (mArgs->getBoolValue(scDumpMultiSchedBlocks))
    flags = SCHScheduleFlags(flags | eDumpMultiSched);
  if (!mArgs->getBoolValue(scNoMergeBlocks))
    flags = SCHScheduleFlags(flags | eMergeBlocks);
  if (mArgs->getBoolValue(scDumpUnmergedBlocks))
    flags = SCHScheduleFlags(flags | eDumpUnmerged);
  if (!mArgs->getBoolValue(scNoSplitBlocks))
    flags = SCHScheduleFlags(flags | eSplitBlocks);
  if (mArgs->getBoolValue(scDumpSplitBlocks))
    flags = SCHScheduleFlags(flags | eDumpSplitBlocks);
  if (!mArgs->getBoolValue(scNoFixClocksAsData))
    flags = SCHScheduleFlags(flags | eFixClocksAsData);
  if (mArgs->getBoolValue(scDumpClockAsData))
    flags = SCHScheduleFlags(flags | eDumpClocksAsData);
  if (mArgs->getBoolValue(scDumpStateUpdate))
    flags = SCHScheduleFlags(flags | eDumpStateUpdate);
  if (mArgs->getBoolValue(scDumpTransitionNodes))
    flags = SCHScheduleFlags(flags | eDumpTransitionNodes);
  if (!mArgs->getBoolValue(scNoScheduleSanity)) {
    flags = SCHScheduleFlags(flags | eSanity);
  }
  if (mArgs->getBoolValue(scScheduleMemorySanity)) {
    flags = SCHScheduleFlags(flags | eMemorySanity);
  }
  if (mArgs->getBoolValue(scNoClockEquivalence))
    flags = SCHScheduleFlags(flags | eNoClockEquivalence);
  if (mArgs->getBoolValue(scResolveBidis) || (tristateMode == eTristateModeZ))
    flags = SCHScheduleFlags(flags | eResolveBidis);
  if (mArgs->getBoolValue(scDumpPIFlowFix))
    flags = SCHScheduleFlags(flags | eDumpPIFlowFix);
  if (mArgs->getBoolValue(scDumpBufferedSequentials))
    flags = SCHScheduleFlags(flags | eDumpBufferedSequentials);
  if (mArgs->getBoolValue(scDumpCycles))
    flags = SCHScheduleFlags(flags | eDumpCycles);
  if (mArgs->getBoolValue(scDumpCyclesEarly))
    flags = SCHScheduleFlags(flags | eDumpCyclesEarly);
  if (!mArgs->getBoolValue(scNoCycleBreaking))
    flags = SCHScheduleFlags(flags | eBreakCycles);
  if (mArgs->getBoolValue(scNoSelfCycles))
    flags = SCHScheduleFlags(flags | eNoSelfCycles);
  if (mArgs->getBoolValue(scNoMergeDCL))
    flags = SCHScheduleFlags(flags | eNoMergeDCL);
  if (!mArgs->getBoolValue(scDebugSchedule)) {
    flags = SCHScheduleFlags(flags | eNoDebugSchedule);
  }
  if (mArgs->getBoolValue(scNoMixedMerge)) {
    flags = SCHScheduleFlags(flags | eNoMixedMerge);
  }
  if (mArgs->getBoolValue(scDumpCycleBdds)) {
    flags = SCHScheduleFlags(flags | eDumpCycleBdds);
  }
  mUtil->putScheduleFlags(flags);

  // Get the buffered memories threshold and store it in the util class
  int threshold;
  ArgProc::OptionStateT state;
  state = mArgs->getIntLast(scBufferedMemoryThreshold, &threshold);
  INFO_ASSERT(state == ArgProc::eKnown,
              "Unexpected result when processing bufferMemoryThreshold switch");
  mUtil->putBufferedMemoryThreshold(threshold);

  // Get the dump level for block merging
  SInt32 mergeDetail;
  state = mArgs->getIntLast(scDumpMergedBlocks, &mergeDetail);
  INFO_ASSERT(state == ArgProc::eKnown,
              "Unexpected result when processing dumpMergedBlocks switch");
  mUtil->putDumpMergedBlocks(mergeDetail);

  // Block splitting needs to determine the top of the design.
  mUtil->setDesign(design);

  // MarkDesign needs to know if we are not coercing ports, which
  // changes how signals are declared in the db.
  mUtil->putNoPortCoercion(mArgs->getBoolValue(CRYPT("-noCoercePorts")));
  
  // If the user requested that we convert latches to flops,
  // we need to prepare for that now.
  SCHLatchAnal* latchAnal = NULL;
  if (mArgs->getBoolValue(scConvertLatches))
  {
    latchAnal = new SCHLatchAnal(mUtil, mMarkDesign, mClkAnal, mScheduleData);

    if (!latchAnal->prepare(phaseStats, stats))
    {
      delete latchAnal;
      return false;
    }
    statsInterval.printIntervalStatistics("SCHLatchPrepare");

    // Replace the SCHClkAnal instance, since it may contain
    // clock masters and other references to deleted nets.
    delete mClkAnal;
    mClkAnal = new SCHClkAnal(mUtil, mMarkDesign);
    latchAnal->putClkAnal(mClkAnal);
  }

  // Get the set of depositable net elabs. We store this here so that
  // we have quick access. It is not a simple flag test for deposit
  // nets.  It needs to happen before collapseClock directives are
  // processed.
  mMarkDesign->markDepositableNets(design, true);
  statsInterval.printIntervalStatistics("DepNets");

  // Process collapseClockDirectives before latch analysis
  mClkAnal->processCollapseClockDirectives();
  statsInterval.printIntervalStatistics("CAClpsClks");

  // If we are converting latches, do the analysis and conversion now
  if (latchAnal != NULL)
  {
    if (!latchAnal->convert(fileRoot, phaseStats, stats))
    {
      delete latchAnal;
      return false;
    }
    delete latchAnal;
    latchAnal = NULL;
    statsInterval.printIntervalStatistics("SCHLatchConvert");

    // Replace the SCHClkAnal instance, since it may contain clock
    // masters and other references to elaborated nets that will be
    // deleted by cycle detection.
    delete mClkAnal;
    mClkAnal = new SCHClkAnal(mUtil, mMarkDesign);

    // Process collapseClockDirectives after latch analysis
    mClkAnal->processCollapseClockDirectives();
    statsInterval.printIntervalStatistics("CAClpsClks");
  }

  if (mUtil->isFlagSet(eSanity) && !mMarkDesign->sanityCheck()) {
    return false;
  }

  /* The cycle detector find and encapsulates cycles in the flow
   * graph, determines a schedule order for flow nodes in cycles,
   * and reports the results to the user.
   */
  if (mCycleDetection->findDesignCycles())  {
    return false;
  }
  mCycleDetection->dumpCycles(fileRoot);
  if (dumpSchedule) {
    mCycleDetection->dumpCycles(NULL);
  }
  if (mUtil->isFlagSet(eSanity) && !mMarkDesign->sanityCheck()) {
    return false;
  }
  statsInterval.printIntervalStatistics("SCHCycle");

  // Find and mark the design components
  if (!mMarkDesign->mark(design, mClkAnal, doAliasClocks)) {
    return false;
  }
  if (phaseStats)
    stats->printIntervalStatistics("SCHMark");

  if (mUtil->isFlagSet(eSanity) && !mMarkDesign->sanityCheck()) {
    return false;
  }
  if (!mCycleDetection->sanityCheck()) {
    return false;
  }
  statsInterval.printIntervalStatistics("SCHSanity");

  // Check if there are any false block cycles. These can occur when
  // the cycles are not real when you look at flow, but they look like
  // cycles when the block flow is computed. We fix these by breaking
  // up the blocks that cause the cycles.
  if (mUtil->isFlagSet(eSplitBlocks)) {
    mCycleDetection->findFalseDesignCycles();
  }
  statsInterval.printIntervalStatistics("SCHFCycle");

  if (mUtil->isFlagSet(eSanity) && !mMarkDesign->sanityCheck()) {
    return false;
  }

  // Mark all combinational flow nodes with the set of input nets they
  // are sensitive to. This is used by various scheduling passes and
  // computing it once saves on creating the graph and computing the
  // data many times.
  //
  // This has to run after real cycle detection and needs to run
  // before cmodel wrapper generation. In the future we want to use
  // this in timing computation so I am putting it before that point.
  //
  // At some points during scheduling, new combinational logic may be
  // created. This data needs to be recomputed after those points.
  mMarkDesign->computeFlowInputNets();
  statsInterval.printIntervalStatistics("SCHFlowInputNets");

  // This block calculates when blocks should execute. It attempts to
  // determine when they change, when they are sampled and if they are
  // part of derived clock logic.
  bool timingSuccess = mTimingAnalysis->computeTiming();
  statsInterval.printIntervalStatistics("SCHTiming");
  if (!timingSuccess) {
    return false;
  }

  if (mUtil->isFlagSet(eSanity) && !mMarkDesign->sanityCheck()) {
    return false;
  }

  // Generate a directives file to treat this design as a c-model
  if (doCModelWrapper) {
    generateCModelDirectives(design, fileRoot);
    statsInterval.printIntervalStatistics("SCHGenCDir");
  }

  // dump aliases to stdout for regression tests
  bool verboseAliases = mArgs->getBoolValue(scVerboseClockAliases);
  if (mArgs->getBoolValue(scDumpClockAliases))
    mClkAnal->dumpAliases(NULL, verboseAliases);

  // Always dump aliases to $fileRoot.clocks
  mClkAnal->dumpAliases(fileRoot, verboseAliases);

  if (mArgs->getBoolValue(scDumpClockTree)) {
#ifdef CDB
    mClkAnal->genClockTree(NULL); // first one call to send output to stdout (this is for debugging )
#endif
    mClkAnal->genClockTree(fileRoot); // and in all cases send output to a file
  }

  // All done with clock analysis, remove the memory.
  delete mClkAnal;
  mClkAnal = NULL;

  // We now have all the data we need, create the various schedules.
  if (!mCreateSchedules->createDesignSchedule(fileRoot)) {
    return false;
  }
  statsInterval.printIntervalStatistics("SCHCreate");

  if (mUtil->isFlagSet(eSanity) && !mMarkDesign->sanityCheck()) {
    return false;
  }

  // Now that the schedules are created, we can add info to the io database
  populateIODB();

  // Possibly run some debug code to test expression synthesis
  if (mArgs->getBoolValue(scDebugExprSynthesis))
    dumpExprSynthesis();

  // Find and possibly dump all state updates. This needs to happen as
  // late as possible so that any dead logic optimizations are
  // done. It also has to happen before the back end because it
  // destroys UD pointed to by flow in the factory. This causes the
  // SCHBlockFlowNodes code to crash.
  mCreateSchedules->findAndDumpStateUpdates();

  // Destroy and clean up the passes which are not longer needed. Note
  // that we set them to NULL so that the SCHSchedule destructor
  // doesn't redelete them. We delete them in the destructor because
  // this routine has to many gotos above (return statemnts in the
  // middle of the function).
  delete mCycleDetection;
  mCycleDetection = NULL;
  delete mTimingAnalysis;
  mTimingAnalysis = NULL;
  delete mCreateSchedules;
  mCreateSchedules = NULL;
  return true;
} // bool SCHSchedule::buildSchedule

void SCHSchedule::addAsyncFanin(FLNodeElab* flowElab, FLNodeElab* output, FLNodeElabSet* covered)
{
  // If this is an async input, add it Note that we don't add it if it
  // is a self reference. The code is written in a way that the first
  // call is a self reference.
  STSymbolTableNode* faninNode = flowElab->getDefNet()->getSymNode();
  STSymbolTableNode* outputNode = output->getDefNet()->getSymNode();
  IODBNucleus* iodb = mUtil->getIODB();
  if ((flowElab != output) && iodb->isAsyncNode(faninNode)) {
    iodb->declareAsyncOutput(outputNode, faninNode);
  }

  // Walk any fanin, but skip any flops. Async flow throughs don't go
  // through flops.
  for (SCHMarkDesign::FlowDependencyLoop p = mMarkDesign->loopDependencies(flowElab, SCHMarkDesign::eInternal);
       !p.atEnd(); ++p) {
    FLNodeElab* faninElab = *p;
    if (!mUtil->isSequential(faninElab) && covered->insertWithCheck(faninElab)) {
      addAsyncFanin(faninElab, output, covered);
    }
  }
}

void SCHSchedule::populateIODB()
{
  // Add the async signals to the iodb
  IODBNucleus* iodb = mUtil->getIODB();
  STSymbolTable* symbolTable = mUtil->getSymbolTable();
  for (CombinationalLoop l = loopCombinational(eCombAsync); !l.atEnd(); ++l)
  {
    // Populate the async inputs
    SCHCombinational* async = *l;
    for (SCHInputNetsCLoop a = async->loopInputNets(); !a.atEnd(); ++a)
    {
      const NUNetElab* asyncElab = *a;
      if (asyncElab)
	iodb->declareAsync(asyncElab->getSymNode());
    }

    // Gather the async outputs
    FLNodeElabSet outputs;
    SCHBlockFlowNodes blockFlowNodes(mUtil);
    FLNodeElabSet covered;
    for (SCHIterator i = async->getSchedule(); !i.atEnd(); ++i) {
      FLNodeElab* asyncFlowElab = *i;
      if (covered.insertWithCheck(asyncFlowElab)) {
        // Get all the elaborated flow for this always block instance
        FLNodeElabVector flowElabs;
        blockFlowNodes.getFlows(asyncFlowElab, &flowElabs, &FLNodeElab::isLive);

        // Walk the set of flows and add any async outputs
        for (FLNodeElabVectorCLoop f(flowElabs); !f.atEnd(); ++f) {
          // Check if this flow represents a primary output
          FLNodeElab* flowElab = *f;
          const SCHSignature* sig = mMarkDesign->getSignature(flowElab);
          if (mMarkDesign->isOutputPort(flowElab) &&
              mScheduleFactory->hasInputMask(sig)) {
            outputs.insert(flowElab);
          }
        }
      }
    }

    // Walk the fanin creating the flow connections
    for (FLNodeElabSetLoop f(outputs); !f.atEnd(); ++f) {
      FLNodeElab* flowElab = *f;
      covered.clear();
      addAsyncFanin(flowElab, flowElab, &covered);
    }
  }
    
  // Walk the design outputs and look for depositables through
  // combinational paths.
  mMarkDesign->findAsyncDeposits(iodb);

  if (! mArgs->getBoolValue(scNoEnableExprSynthesis))
  {
    NUDesign* design = mUtil->getDesign();
    /* A set of idents that are partially referred to by bidis that are
       being processed. We want to walk the original expression, which
       contains all parts of the original net and find outputs. Those
       outputs need to be marked as always enabled so cwave won't try to
       drive them.
    */
    ExprImplicitDriveWalker::IdentSet exprBidiOuts;
    
    for (NUDesign::PortLoop p = design->loopBidPorts(); !p.atEnd(); ++p)
    {
      NUNet* net = *p;
      NUModuleElab* mod = net->getScope()->getModule()->lookupElab(symbolTable);
      NUNetElab *elab = net->lookupElab(mod->getHier());
      STAliasedLeafNode* elabLeaf = elab->getSymNode();
      const SymTabIdentBP* referrer = CbuildSymTabBOM::getSymTabIdentBP(elabLeaf);
      
      if (referrer)
      {
        // add any internal drivers to the exprBidiOuts set.
        ExprImplicitDriveWalker internalDriverWalker(&exprBidiOuts, mUtil->getBOMManager());
        internalDriverWalker.visitExpr(referrer->getBackPointer());
      }
      
      // Don't process this net if it has been deemed an output. In that
      // case, there is a strong driver and no reason for cwave to drive
      // it.
      if (exprBidiOuts.count(CbuildSymTabBOM::getIdent(elabLeaf)) == 0)
      {
        EnableExprCalculator enExprCalc(mUtil, net);
        
        for (NUNetElab::SortedDriverLoop bidiDrivers = elab->loopSortedContinuousDrivers();
             ! bidiDrivers.atEnd() && enExprCalc.isValid(); ++bidiDrivers) {
          FLNodeElab* fne = *bidiDrivers;
          enExprCalc.processDriver(fne);
        } // ! bidiDrivers.atEnd
        
        if (enExprCalc.hasEnableExpr())
        {
          CarbonExpr* driveCondition = enExprCalc.compute();
          iodb->declareEnableExpr(elabLeaf, driveCondition);
        }
      }
    }
    
    // All the elements of the IdentSet are parts of bidis that are
    // considered outputs. Create an always-enabled expression for them.
    for (ExprImplicitDriveWalker::IdentSet::UnsortedLoop p = exprBidiOuts.loopUnsorted();
         ! p.atEnd(); ++p)
    {
      CarbonIdent* output = *p;
      DynBitVector useMask;
      const STAliasedLeafNode* leaf = output->getNode(&useMask);
      NUNet* net = NUNet::find(leaf);
      EnableExprCalculator outputEnableCalc(mUtil, net);
      outputEnableCalc.processOutput(useMask);
      
      if (outputEnableCalc.hasEnableExpr())
      {
        CarbonExpr* driveCondition = outputEnableCalc.compute();
        iodb->declareEnableExpr(leaf, driveCondition);
      }
    }
  }

  // Now that all the signals are declared, we can initialize the
  // tag types in the storage bom's for these signals.
  iodb->initTagTypes();
  
  for (AsyncResetLoop resets = loopAsyncResets(); !resets.atEnd(); ++resets)
  {
    // Use the edge with the most uses
    ClockEdge edge;
    UInt32 *edge_counts = resets.getValue();
    // Find the edge with the max edge count.
    if( edge_counts[eClockPosedge] + edge_counts[eClockNegedge] >
        edge_counts[eLevelHigh] + edge_counts[eLevelLow] ) {
      if ( edge_counts[eClockPosedge] > edge_counts[eClockNegedge] )
        edge = eClockPosedge;
      else
        edge = eClockNegedge;
    }
    else {
      if( edge_counts[eLevelHigh] > edge_counts[eLevelLow] )
        edge = eLevelHigh;
      else
        edge = eLevelLow;
    }
    
    iodb->declareReset(resets.getKey()->getSymNode(), edge);
  }
} // void SCHMarkDesign::populateIODB

void SCHSchedule::setupOptions() {
  scSortByData = CRYPT("-sortByData");
  scDisableInitCombos = CRYPT("-disableInitCombos");
  scAliasClocks = CRYPT("-aliasClocks");
  scNoSampleSchedule = CRYPT("-noSampleSchedule");
  scDumpUnmergedBlocks = CRYPT("-dumpUnmergedBlocks");
  scDumpMultiSchedBlocks = CRYPT("-dumpMultiSchedBlocks");
  scNoMergeBlocks = CRYPT("-noMergeBlocks");
  scDumpMergedBlocks = CRYPT("-dumpMergedBlocks");
  scNoSplitBlocks = CRYPT("-noSplitBlocks");
  scDumpSplitBlocks = CRYPT("-dumpSplitBlocks");
  scNoFixClocksAsData = CRYPT("-noFixClocksAsData");
  scDumpClockAliases = CRYPT("-dumpClockAliases");
  scVerboseClockAliases = CRYPT("-verboseClockAliases");
  scDumpClockTree = CRYPT("-dumpClockTree");
  scDumpClockAsData = CRYPT("-dumpClocksAsData");
  scCondFlow = CRYPT("-condFlow");
  scNoCondFlow = CRYPT("-noCondFlow");
  scDumpStateUpdate = CRYPT("-dumpStateUpdate");
  scDumpTransitionNodes = CRYPT("-dumpTransitionNodes");
  scNoScheduleSanity = CRYPT("-noScheduleSanity");
  scScheduleMemorySanity = CRYPT("-scheduleMemorySanity");
  scNoClockEquivalence = CRYPT("-noClockEquivalence");
  scResolveBidis = CRYPT("-resolveBidis");
  scDumpPIFlowFix = CRYPT("-dumpPIFlowFix");
  scDebugExprSynthesis = CRYPT("-debugExprSynthesis");
  scDumpBufferedSequentials = CRYPT("-dumpBufferedSequentials");
  scDumpCycles = CRYPT("-dumpCycles");
  scDumpCyclesEarly = CRYPT("-dumpCyclesEarly");
  scNoCycleBreaking = CRYPT("-noCycleBreaking");
  scNoSelfCycles = CRYPT("-noSelfCycles");
  scNoMergeDCL = CRYPT("-noMergeDCL");
  scBufferedMemoryThreshold = CRYPT("-bufferedMemoryThreshold");
  scConvertLatches = CRYPT("-convertLatches");
  scDebugSchedule = CRYPT("-debugSchedule");
  scNoMixedMerge = CRYPT("-noMixedMerge");
  scDumpCycleBdds = CRYPT("-dumpCycleBdds");
  scNoEnableExprSynthesis = CRYPT("-noEnableExprSynthesis");
  scFindClockLimit = CRYPT("-findClockLimit");
  
  // Create the switches we need
  mArgs->addBool(scSortByData, CRYPT("Sort the combinational schedules by the data flow first and code locality second"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scDisableInitCombos, CRYPT("Don't run the combinational blocks at time 0"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scAliasClocks, CRYPT("Treat clock nets that are proved equivalent as a single net"), true, CarbonContext::ePassCarbon);
  mArgs->addBoolOverride("-noAliasClocks", scAliasClocks, "By default, the Carbon compiler treats clock nets that are equivalent as a single net; redundant images of clocks are discarded. This option overrides this behavior, that is, redundant images of clocks will be preserved even if proven equivalent. Using this option may casue a degradation in performance; however, if you do not use this option, clock trees may be missing in waveforms. The clock-aliasing table will be generated automatically and written to the file ./<design name>.clocks, by default ./libdesign.clocks.");
  //  mArgs->addToSection(CarbonContext::scNetControl, "-noAliasClocks");

  mArgs->addBool(scNoSampleSchedule, CRYPT("Turn's off sample-based scheduling (disables a performance enhancement)"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scDumpUnmergedBlocks, CRYPT("Dumps out the file and line number for combinational outputs that are not merged"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scDumpMultiSchedBlocks, CRYPT("Dumps out the file, line number, net and schedule for all combinational blocks in more than one schedule."), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scNoMergeBlocks, CRYPT("Disables merging blocks (a performance optimization)."), false, CarbonContext::ePassCarbon);
  mArgs->addInt(scDumpMergedBlocks, CRYPT("Dumps information on how well block merging could do"), 0, false, false, CarbonContext::ePassCarbon);
  mArgs->addBool(scNoSplitBlocks, CRYPT("Disables splitting blocks (a performance enhancement)."), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scDumpSplitBlocks, CRYPT("Dumps the set of blocks that were split and failed to get split"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scNoFixClocksAsData, CRYPT("Disables automatically fixing issues when clocks are used as data in sequential blocks"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scDumpClockAliases, CRYPT("Dump out the clock aliasing table"),
                 false, CarbonContext::ePassCarbon);
  mArgs->addBool(scVerboseClockAliases, CRYPT("Clock aliasing table printed with net annotations and storage information"),
                 false, CarbonContext::ePassCarbon);
  mArgs->addBool(scDumpClockTree, CRYPT("Dump out pseudo-verilog for the clock tree"),
                 false, CarbonContext::ePassCarbon);
  mArgs->addBool(scDumpClockAsData, CRYPT("Dumps the set of clocks that were buffered because they were used as data"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scCondFlow, CRYPT("Optimize for data-conditional execution"),
                 false, CarbonContext::ePassCarbon);
  mArgs->addBool(scNoCondFlow, CRYPT("Do not optimize for data-conditional execution"),
                 true, CarbonContext::ePassCarbon);
  // mArgs->addToSection(CarbonContext::scNetControl, scNoCondFlow);
  mArgs->addToSection(CarbonContext::scNetControl, scCondFlow);

  mArgs->addBool(CRYPT("-condFlowVerbose"),
                 CRYPT("Print out details about data-conditional execution"),
                 false, CarbonContext::ePassCarbon);
  mArgs->addBool(scDumpStateUpdate, CRYPT("Dumps the set of nets that are state update"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scDumpTransitionNodes, CRYPT("Dumps the set of nodes that are marked to execute with transition"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scNoScheduleSanity, CRYPT("Disables sanity checks every every phase of scheduling"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scScheduleMemorySanity, CRYPT("Enables a very slow memory allocation sanity pass during scheduling."), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scNoClockEquivalence, CRYPT("Disables the clock equivalence optimization."), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scResolveBidis, CRYPT("Turns on logic to make sure the bi-directional outputs are accurate with respect to input flow"), true, CarbonContext::ePassCarbon);
  mArgs->addBoolOverride(CRYPT("-noResolveBidis"), scResolveBidis);

  // This is only valid with -noInputFlow, which is deprecated.
  mArgs->addBool(scDumpPIFlowFix, CRYPT("Dumps the set of PIs that were buffered because they could have flowed directly into a sequential block"), false, CarbonContext::ePassCarbon);
  mArgs->putIsDeprecated(scDumpPIFlowFix, true);
  
  mArgs->addBool(scDebugExprSynthesis, CRYPT("Output debug information - don't run on big designs"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scDumpBufferedSequentials, CRYPT("Dumps the set delay buffers added to schedule sequential blocks correctly"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scDumpCycles, CRYPT("Dumps all cyclic flow"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scDumpCyclesEarly, CRYPT("Dumps all cyclic flow early (before cycle elimination measures)"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scNoCycleBreaking, CRYPT("Disables cycle breaking"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scNoSelfCycles, CRYPT("Disables treating a single net feedback as a cycle"), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scNoMergeDCL, CRYPT("Disables merging DCL schedules."), false, CarbonContext::ePassCarbon);
  mArgs->addInt(scBufferedMemoryThreshold, "Sets a new threshold in bits for choosing to buffer memories to fix scheduling conflicts. Use this to allow larger memories to get buffered.", 16*1024, false, false, CarbonContext::ePassCarbon);
  mArgs->addToSection(CarbonContext::scNetControl, scBufferedMemoryThreshold);

  mArgs->addBool(scConvertLatches, CRYPT("Convert latches to equivalent flops, when possible."), true, CarbonContext::ePassCarbon);
  mArgs->addBoolOverride("-noConvertLatches", scConvertLatches, "By default the compiler converts latches that behave as flops to flops. This switch disables the conversion.");
  mArgs->addBool(scDebugSchedule, CRYPT("Enables running the debug schedule and therefore dumping any sample scheduled nets."), false, CarbonContext::ePassCarbon);
  mArgs->addBool(scNoMixedMerge, CRYPT("Disable mixed block merging."), false,
                 CarbonContext::ePassCarbon);
  mArgs->addBool(scDumpCycleBdds, CRYPT("Dump the BDDs in cycle graphs."), false,
                 CarbonContext::ePassCarbon);

  mArgs->addBool(scNoEnableExprSynthesis, CRYPT("Disable enable expression synthesis into the database. This is used by cwave to figure out how to stimulate a design with bidirects."), false, CarbonContext::ePassCarbon);
  mArgs->addInt(scFindClockLimit, CRYPT("Set the limit on recursive nesting for the findClock method. Beyond this limit, findClock will stop looking for an equivalent clock. Default is -1, which implies no limit."), -1, false, false, CarbonContext::ePassCarbon);
  
} // void SCHSchedule::setupOptions

SCHSchedule::SCHSchedule(NUNetRefFactory *netref_factory,
                         MsgContext* mc,
                         STSymbolTable* st,
                         RETransform* transform,
                         Split * split,
                         FLNodeFactory * flowFactory,
                         FLNodeElabFactory* flowElabFactory,
                         ArgProc* args,
                         IODBNucleus* iodb,
                         ESPopulateExpr* populateExpr,
                         SourceLocatorFactory* sourceLocatorFactory)
{
  mArgs = args;
 
  mScheduleFactory = iodb->getSchedFactory();
  mUtil = new SCHUtil(this, mScheduleFactory, flowFactory, flowElabFactory,
                      split, iodb, st, mc, netref_factory, transform, args,
                      populateExpr, sourceLocatorFactory);
  mMarkDesign = new SCHMarkDesign(this, mUtil, mScheduleFactory);
  mDump = new SCHDump(this, mScheduleFactory, mMarkDesign, mUtil);
  mScheduleData = new SCHScheduleData(mUtil, mMarkDesign);

  // The following classes are not created until they are needed and
  // destroyed immediately after the schedule has been built
  mTimingAnalysis = NULL;
  mCreateSchedules = NULL;
  mCycleDetection = NULL;
  mClkAnal = NULL;
}

SCHSchedule::~SCHSchedule()
{
  delete mDump;
  delete mMarkDesign;
  delete mTimingAnalysis;
  delete mCreateSchedules;
  delete mCycleDetection;
  delete mUtil;
  delete mScheduleData;
}

const NUNetElab* SCHSchedule::getClockMaster(const NUNetElab* clk, bool* invert)
{
  return mMarkDesign->getClockMaster(clk, invert);
}

const NUNetElab*
SCHSchedule::getClockMaster(const STSymbolTableNode* clkName, bool* invert)
{
  const NUNetElab* clk = clockFromName(clkName);
  return getClockMaster(clk, invert);
}

/* static method */
const NUNetElab* SCHSchedule::clockFromName(const STSymbolTableNode* clkName)
{
  NUBase* base = CbuildSymTabBOM::getNucleusObj(clkName);
  NUNetElab* clk = dynamic_cast<NUNetElab*>(base);
  return clk;
}

void SCHSchedule::printScheduleStats(const char* fileRoot, bool output) const
{
  mDump->printScheduleStats(fileRoot, output);
}

void SCHSchedule::print() const
{
  mDump->print();
}

bool SCHCmpNodes::equal(const FLNodeElab* n1, const FLNodeElab* n2) const
{
  HierName* h1 = n1->getHier();
  HierName* h2 = n2->getHier();
  NUUseDefNode* d1 = n1->getUseDefNode();
  NUUseDefNode* d2 = n2->getUseDefNode();

  return ((d1 == d2) && (h1 == h2));
}

bool SCHCmpNodes::lessThan1 (const FLNodeElab* a, const FLNodeElab* b) const
{
  return a < b;
}

size_t SCHCmpNodes::hash(const FLNodeElab* n1) const
{
  return ((size_t) n1->getHier()) + ((size_t) n1->getUseDefNode());
}

SCHSchedule::CombinationalLoop
SCHSchedule::loopCombinational(SCHScheduleType type)
{
  return mScheduleData->loopCombinational(type);
}

SCHSchedule::DerivedClockLogicLoop
SCHSchedule::loopDerivedClockLogic()
{
  return mScheduleData->loopDerivedClockLogic();
}

void
SCHSchedule::observeNet(NUNetElab* net)
{
  mMarkDesign->observeNet(net);
}

SCHClocksLoop SCHSchedule::loopClocks()
{
  return mMarkDesign->loopClocks();
}

SCHSchedule::AsyncResetLoop SCHSchedule::loopAsyncResets()
{
  return mMarkDesign->loopAsyncResets();
}

SCHSequentials::SequentialLoop SCHSchedule::loopSequential()
{
  return mScheduleData->loopSequential();
}

// Iterator for all sequential schedules
SCHSchedulesLoop SCHSchedule::loopAllSequentials()
{
  return mScheduleData->loopAllSequentials();
}

//! Iterator for all combinational schedules
SCHSchedulesLoop SCHSchedule::loopAllCombinationals()
{
  return mScheduleData->loopAllCombinationals();
}

//! Iterator for all simulation combinational schedule (no init or debug)
SCHSchedulesLoop SCHSchedule::loopSimulationCombinationals()
{
  return mScheduleData->loopSimulationCombinationals();
}

//! Iterator for all simulation schedules (no init or debug)
SCHSchedulesLoop SCHSchedule::loopAllSimulationSchedules()
{
  return mScheduleData->loopAllSimulationSchedules();
}

SCHSchedule::CycleLoop SCHSchedule::loopCycles()
{
  return mMarkDesign->loopCycles();
}

bool SCHSchedule::isNUNetPrimaryClockInput(const NUNet& net) const
{
  return mMarkDesign->isPrimaryClockInput(net);
}

SCHSequentials::SULoop
SCHSchedule::loopSUNets()
{
  return mScheduleData->loopSUNets();
}

void SCHSchedule::scheduleName(SCHScheduleType type, UtString* buf)
{
  switch(type)
  {
  case eSequential:
  case eSequentialDCL:
    *buf = "Sequential";
    break;

  case eCombTransition:
    *buf = "Combinational [Transition]";
    break;

  case eCombSample:
    *buf = "Combinational [Sample]";
    break;

  case eCombDCL:
    *buf = "Combinational [Derived]";
    break;

  case eCombAsync:
    *buf = "Async";
    break;

  case eCombInput:
    *buf = "Input";
    break;

  case eCombInitial:
    *buf = "Initial";
    break;

  case eCombInitSettle:
    *buf = "InitSettle";
    break;

  case eCombDebug:
    *buf = "Debug";
    break;

  case eCombForceDeposit:
    *buf = "ForceDeposit";
    break;

  default:
    INFO_ASSERT(0, "Should not get out of range");
    break;
  }
} // void scheduleName

void SCHSchedule::markWaveableNets()
{
  mMarkDesign->markWaveableNets();
}

class Callback : public ESCallback
{
public:
  //! constructor
  Callback(FLNodeElab* flowElab, ESPopulateExpr* populateExpr, bool stopOne) :
    ESCallback(populateExpr), mFlowElab(flowElab), mStopOne(stopOne)
  {}

  //! Function to test whether we should stop at this flow node or not
  bool stop(FLNode* flow, bool)
  {
    if (mStopOne)
    {
      // Simple test to see if we started a new net
      NUNetRefHdl netRefHdl = flow->getDefNetRef();
      NUNet* net = netRefHdl->getNet();
      return net != mFlowElab->getDefNet()->getNet();
    }
    else
      // Always traverse
      return false;
  }

private:
  //! Hide copy and assign constructors.
  Callback(const Callback&);
  Callback& operator=(const Callback&);

  FLNodeElab* mFlowElab;
  bool mStopOne;
}; // class Callback : public ESCallback

class CallbackElab : public ESCallbackElab
{
public:
  //! constructor
  CallbackElab(FLNodeElab* flow, ESPopulateExpr* populateExpr, bool stopOne) :
    ESCallbackElab(populateExpr), mFlowElab(flow), mStopOne(stopOne)
  {}

  //! Function to test whether we should stop at this flow node or not
  bool stop(FLNodeElabVector* flows, const NUNetRefHdl&, bool)
  {
    // Get the elaborated net for these set of flows
    NUNetElab* netElab = NULL;
    for (FLNodeElabVectorIter i = flows->begin(); i != flows->end(); ++i)
    {
      FLNodeElab* flow = *i;
      if (netElab == NULL)
        netElab = flow->getDefNet();
      else
        FLN_ELAB_ASSERT(netElab == flow->getDefNet(), flow);
    }

    if (mStopOne)
      return netElab != mFlowElab->getDefNet();
    else
      return false;
  }

private:
  //! Hide copy and assign constructors.
  CallbackElab(const CallbackElab&);
  CallbackElab& operator=(const CallbackElab&);

  FLNodeElab* mFlowElab;
  bool mStopOne;
};

class CallbackElabIdent : public ESCallbackElab
{
public:
  //! Constructor
  CallbackElabIdent(ESPopulateExpr* populateExpr) :
    ESCallbackElab(populateExpr)
  {}

  //! Function to test whether we should stop at this flow node or not
  /*! We always stop since we create idents with this
   */
  bool stop (FLNodeElabVector*, const NUNetRefHdl&, bool) { return true; }
};

typedef UtMap<FLNodeElab*, ESExprHndl, SCHCmpFlow> ExprsMap;
typedef ExprsMap::iterator ExprsMapIter;

class ExprResynthDump
{
public:
  ExprResynthDump(SCHUtil* util, ArgProc* args)
    : mExprResynth(ExprResynth::eStopAtState, util->getPopulateExpr(),
                   util->getNetRefFactory(),
                   args, util->getMsgContext(),
                   util->getIODB()->getAtomicCache(),
		   util->getIODB())
  {
  }

  void dump(ESExprHndl& hndl, FLNodeElab* flow)
  {
    NUUseDefNode* node = flow->getUseDefNode();
    FLN_ELAB_ASSERT(node, flow);
    const NUExpr* expr =
      mExprResynth.exprToNucleus(hndl.getExpr(), NULL, node->getLoc());
    UtIO::cout() << "  Resynth: ";
    expr->printVerilog(1);
    UtIO::cout() << UtIO::endl;
  }

private:
  ExprResynth mExprResynth;
}; // class ExprResynthDump

void SCHSchedule::dumpExprSynthesis()
{
  // Data to hold unelaborated expressions sorted by the elaborated
  // flow nodes
  ExprsMap exprsMap;
  ExprsMap fullExprsMap;

  // Create the map to get all the flow nodes from an always block
  SCHBlockFlowNodes blockFlowNodes(mUtil);

  // Create the various expressions
  ESPopulateExpr* populateExpr = mUtil->getPopulateExpr();
  for (SCHSchedulesLoop l = loopAllCombinationals(); !l.atEnd(); ++l)
  {
    SCHCombinational* comb = l.getCombinationalSchedule();
    for (SCHIterator i = comb->getSchedule(); !i.atEnd(); ++i)
    {
      FLNodeElab* blockFlow = *i;
      FLNodeElabVector nodes;
      blockFlowNodes.getFlows(blockFlow, &nodes, &FLNodeElab::isLive);
      for (FLNodeElabVectorCLoop n(nodes); !n.atEnd(); ++n) {
	FLNodeElab* flow = *n;
	FLNode* unelabFlow = flow->getFLNode();

	// Build the complete expression tree if it hasn't been already
	if (fullExprsMap.find(flow) == fullExprsMap.end())
	{
	  Callback callback(flow, populateExpr, false);
	  ESExprHndl exprHndl = populateExpr->create(unelabFlow, &callback);
	  fullExprsMap.insert(ExprsMap::value_type(flow, exprHndl));
	}

	// Build the expression tree for one node if it hasn't been already
	if (exprsMap.find(flow) == exprsMap.end())
	{
	  Callback callback(flow, populateExpr, true);
	  ESExprHndl exprHndl = populateExpr->create(unelabFlow, &callback);
	  exprsMap.insert(ExprsMap::value_type(flow, exprHndl));
	}
      } // for
    } // for
  } // for

  ExprResynthDump resynth(mUtil, mArgs);

  // Print the various expressions
  ExprsMapIter i;
  for (i = fullExprsMap.begin(); i != fullExprsMap.end(); ++i)
  {
    FLNodeElab* flow = i->first;
    ESExprHndl exprHndl = i->second;
    UtIO::cout() << "Full Xlate: ";
    flow->getName()->print();
    exprHndl.printVerilog(2);
  }
  for (i = exprsMap.begin(); i != exprsMap.end(); ++i)
  {
    FLNodeElab* flow = i->first;
    ESExprHndl exprHndl = i->second;
    UtIO::cout() << "Simple Xlate: ";
    flow->getName()->print();
    exprHndl.printVerilog(2);
  }

  // Data to hold the elaborated expressions
  ExprsMap elabExprsMap;
  ExprsMap elabFullExprsMap;

  // Create the elaborated expressions
  for (i = fullExprsMap.begin(); i != fullExprsMap.end(); ++i)
  {
    FLNodeElab* flow = i->first;
    ESExprHndl exprHndl = i->second;
    CallbackElab callback(flow, populateExpr, false);
    ESExprHndl newHndl = populateExpr->create(flow, &callback);
    elabFullExprsMap.insert(ExprsMap::value_type(flow, newHndl));
  }
  for (i = exprsMap.begin(); i != exprsMap.end(); ++i)
  {
    FLNodeElab* flow = i->first;
    ESExprHndl exprHndl = i->second;
    CallbackElab callback(flow, populateExpr, true);
    ESExprHndl newHndl = populateExpr->create(flow, &callback);
    elabExprsMap.insert(ExprsMap::value_type(flow, newHndl));
  }

  // Print the various elaborated expressions
  for (i = elabFullExprsMap.begin(); i != elabFullExprsMap.end(); ++i)
  {
    FLNodeElab* flow = i->first;
    ESExprHndl exprHndl = i->second;
    UtIO::cout() << "Full Elab Xlate: ";
    flow->getName()->print();
    exprHndl.printVerilog(2);
    resynth.dump(exprHndl, flow);
  }
  for (i = elabExprsMap.begin(); i != elabExprsMap.end(); ++i)
  {
    FLNodeElab* flow = i->first;
    ESExprHndl exprHndl = i->second;
    UtIO::cout() << "Simple Elab Xlate: ";
    flow->getName()->print();
    exprHndl.printVerilog(2);
    resynth.dump(exprHndl, flow);
  }

  // Test the enable getting code
  for (i = elabFullExprsMap.begin(); i != elabFullExprsMap.end(); ++i)
  {
    FLNodeElab* flow = i->first;
    ESExprHndl exprHndl = i->second;
    ESExprHndl enHndl = populateExpr->getEnable(exprHndl);
    if (!enHndl.isNonZero())
    {
      // We found an enabled expression, print the enable for it
      UtIO::cout() << "Found enabled expression for ";
      flow->getName()->print();
      enHndl.printVerilog(2);
      resynth.dump(exprHndl, flow);
    }
  }
}

void
SCHSchedule::generateCModelDirectives(NUDesign* design, const char* fileRoot)
{
  // Open a file libdesign.cmodel.dir for now, we can figure out how
  // to get a better name later.
  UtString fname(fileRoot);
  fname << ".cmodel.dir";
  UtOBStream out(fname.c_str());

  // Get the top module, there should only be one
  NUModule* module = NULL;
  for (NUDesign::ModuleLoop l = design->loopTopLevelModules();
       !l.atEnd(); ++l)
  {
    NU_ASSERT(module == NULL, module);
    module = *l;
  }

  // Emit the c-module begin directive
  out << "cModuleBegin " << module->getName()->str() << "\n";

  // Class to compute branch nets for clocks
  SCHBranchNets branchNets(mMarkDesign, SCHBranchNetFlag(eIncludeClock));
  branchNets.init();

  // Loop the output ports, creating the directives
  STSymbolTable* symbolTable = mUtil->getSymbolTable();
  for (NUDesign::PortLoop p = design->loopOutputPorts(); !p.atEnd(); ++p)
  {
    // Emit the port directive
    NUNet* net = *p;
    out << "  cPortBegin " << net->getName()->str() << "\n";

    // Visit the drivers and figure out the timing
    NUModuleElab* mod = net->getScope()->getModule()->lookupElab(symbolTable);
    NUNetElab *elab = net->lookupElab(mod->getHier());
    const SCHSignature* signature = NULL;
    for (SCHUtil::DriverLoop l = mUtil->loopNetDrivers(elab); !l.atEnd(); ++l)
    {
      // Make sure the flow is live, because dead flows don't have
      // valid signatures. They could have a valid transition mask and
      // invalid sample mask. See test/bugs/bug503 for a test case
      FLNodeElab* flow = *l;
      if (flow->isLive()) {
        const SCHSignature* newSignature = mMarkDesign->getSignature(flow, true);
        if (signature == NULL)
          signature = newSignature;
        else if (newSignature != NULL)
          signature = mScheduleFactory->mergeSignatures(signature, newSignature);
      }
    }

    // Make sure this has a valid signature. We can get NULL
    // signatures for undriven outputs.
    if (signature == NULL) {
      out << "    # Output port is not actively driven\n";
    }
    else
    {
      // Emit an async port if it belongs in the async schedule
      if ((signature != NULL) && mScheduleFactory->hasOutputMask(signature) &&
          mScheduleFactory->hasInputMask(signature))
      {
        // Emit the async port and fanin.
        //
        // Use the CombinationalSchedules (through CreateSchedules)
        // version of getting input dependencies because it stops at
        // flops and only returns the set of primary inputs that can
        // be reached through combinational logic.
        const SCHInputNets* inputs;
        inputs = mMarkDesign->findNetInputNets(elab);
        if (inputs != NULL)
        {
          // These are the asynchronous paths through the device
          out << "    cTiming async\n";

          // Print warnings about non primary input nets
          for (SCHInputNetsCLoop n(*inputs); !n.atEnd (); ++n)
          {
            const NUNetElab* netElab = *n;
            NUNet* net = netElab->getNet();
            if (!net->isPrimaryInput())
            {
              STSymbolTableNode* node = netElab->getSymNode();
              UtString name;
              node->composeHelper(&name, true, true, ".", false);
              out << "      # Warning: input net `"
                  << name.c_str()
                  << "' is not a primary port.\n";
            }
          }

          // Print the fanin
          out << "    cFanin";
          for (SCHInputNetsCLoop n(*inputs); !n.atEnd (); ++n)
          {
            const NUNetElab* netElab = *n;
            out << " " << netElab->getNet()->getName()->str();
          }
          out << "\n";
        }
      } // if

      // Gather the input nets for the clocks on this transition mask
      const SCHScheduleMask* transition = signature->getTransitionMask();
      const SCHScheduleMask* syncMask = NULL;
      for (SCHScheduleMask::UnsortedEvents l = transition->loopEvents();
           !l.atEnd(); ++l)
      {
        const SCHEvent* event = *l;
        if (event->isClockEvent())
        {
          const NUNetElab* clk = getClockMaster(event->getClock());
          if (clk->getNet()->isPrimaryPort())
          {
            // Add this event
            const SCHScheduleMask* newMask;
            newMask = mScheduleFactory->buildMask(event);
            syncMask = mScheduleFactory->appendMasks(syncMask, newMask);
          }
          else
          {
            // Print the derived clock as a comment
            UtString clkName;
            clk->getSymNode()->composeHelper(&clkName, true, true, ".", false);
            out << "    # Inputs for derived clock: " << clkName.c_str()
                << "\n";

            // Visit the fanin looking for PIs and then convert them to
            // clock edges. We add them to a summary mask. That way we
            // get rid of repeats from various derived clocks.
            //
            // Note that we use the mark design version of get input
            // dependencies because it goes through the clock pins of
            // flops and all combo logic until it hits PIs. That is
            // what we want to determine clock sensitivity.
            const SCHInputNets* inputs = branchNets.compute(clk);
            if (inputs != NULL)
            {
              IODBNucleus* iodb = mUtil->getIODB();
              for (SCHInputNetsCLoop n(*inputs); !n.atEnd (); ++n)
              {
                // Print the input to the derived clock asa comment
                const NUNetElab* netElab = *n;
                STSymbolTableNode* node = netElab->getSymNode();
                UtString inputName;
                node->composeHelper(&inputName, true, true, ".", false);
                out << "    # => " << inputName.c_str();
                if (!netElab->getNet()->isPrimaryInput())
                  out << "; Warning: net is not a primary input!";
                out << "\n";

                // Generate a posedge event
                iodb->declareCModelSense(node);
                event = mScheduleFactory->buildClockEdge(node, eClockPosedge);
                const SCHScheduleMask* newMask;
                newMask = mScheduleFactory->buildMask(event);
                syncMask = mScheduleFactory->appendMasks(syncMask, newMask);

                // Generate a negedge event
                event = mScheduleFactory->buildClockEdge(node, eClockNegedge);
                newMask = mScheduleFactory->buildMask(event);
                syncMask = mScheduleFactory->appendMasks(syncMask, newMask);
              }
            } // if
            else
              out << "    # => (No inputs; Must be an undriven reg)\n";
          } // else
        } // if
      } // for

      // Emit the sync port and fanin
      if (syncMask != NULL)
      {
        for (SCHScheduleMask::SortedEvents l = syncMask->loopEventsSorted();
             !l.atEnd(); ++l)
        {
          const SCHEvent* event = *l;
          NU_ASSERT(event->isClockEvent(), elab);
          const STSymbolTableNode* clkSym = event->getClock();
          NUNetElab* clk = NUNetElab::find(clkSym);
          ClockEdge edge = event->getClockEdge();
          const char* edgeStr = NULL;
          if (edge == eClockPosedge)
            edgeStr = "rise ";
          else if (edge == eClockNegedge)
            edgeStr = "fall ";
          else
            NU_ASSERT("Unexpected edge" == NULL, clk);
          out << "    cTiming " << edgeStr << clk->getNet()->getName()->str()
              << "\n";
          out << "    cFanin *\n";
        }
      }
    } // else

    // Emit the port end directive
    out << "  cPortEnd\n";
  }

  // Loop the inouts and print a comment that they are not handled
  for (NUDesign::PortLoop p = design->loopBidPorts(); !p.atEnd(); ++p)
  {
    // Emit the port directive
    NUNet* net = *p;
    out << "#  inout not handled: " << net->getName()->str() << "\n";
  }

  // End the c-module directive
  out << "cModuleEnd\n";
} // void SCHSchedule::generateCModelDirectives

bool
SCHCmpFlow::operator()(const FLNodeElab* f1, const FLNodeElab* f2) const
{
  return FLNodeElab::compare(f1, f2) < 0;
}

bool SCHSchedule::isInputMask(const SCHScheduleMask* mask) const
{
  return mScheduleFactory->getInputMask() == mask;
}

SCHSchedule::ClearAtEndNetsLoop SCHSchedule::loopClearAtEndNets()
{
  return mScheduleData->loopClearAtEndNets();
}

bool SCHSchedule::hasDCLCycles(void)
{
  bool foundCycle = false;
  DerivedClockLogicLoop l;
  for (l = loopDerivedClockLogic(); !l.atEnd() && !foundCycle; ++l) {
    SCHDerivedClockBase* dclBase = *l;
    if (dclBase->castDCLCycle() != NULL) {
      foundCycle = true;
    }
  }
  return foundCycle;
}

bool SCHSchedule::isFrequentDepositable(const NUNetElab* netElab) const
{
  return mMarkDesign->isFrequentDepositable(netElab);
}

bool SCHSchedule::isWritable(const NUNetElab* netElab) const
{
  return mMarkDesign->isWritable(netElab);
}

bool SCHSchedule::isAsyncClock(const NUNetElab* clkElab) const
{
  return mScheduleData->isAsyncClock(clkElab);
}

SCHClocksLoop SCHSchedule::loopAsyncClocks() const
{
  return mScheduleData->loopAsyncClocks();
}


