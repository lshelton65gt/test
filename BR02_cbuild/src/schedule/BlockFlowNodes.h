// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implements a class to do fast lookups of all flow for an always block instance
*/

#ifndef _BLOCKFLOWNODES_H_
#define _BLOCKFLOWNODES_H_

#include "flow/Flow.h"
#include "flow/FLNodeElab.h"
#include "UseDefHierPair.h"

class FLNodeElabFactory;
class FLNodeElab;
class NUUseDefNode;
class SCHUtil;

class SCHBlockFlowNodes
{
public:
  //! Constructor
  SCHBlockFlowNodes(SCHUtil* util);

  //! destructor
  ~SCHBlockFlowNodes();

  //! Populate a vector of flow nodes for a block given a flow
  void getFlows(FLNodeElab* flowElab, FLNodeElabVector* nodeVector,
                bool (FLNodeElab::*keepTest)() const) const;

private:
  //! Class to use as a key for the block to elaborated flow map
  /*! There are three types of elaborated flow that we want to put in
   *  the map. They are: encapsulated cycle flow, bound nodes, and
   *  ordinary flow. The first two kind need to be in a vector by
   *  themselves, and the last needs to be in a vector with the same
   *  use def/hier pair. In fact, bound nodes have a NULL use def node.
   *
   *  So the key depends on the type of elaborated flow
   */
  class Key
  {
  public:
    //! Constructor from an elaborated flow
    Key(FLNodeElab* flowElab);

    //! Destructor
    ~Key();

    //! Compare function for two keys
    static int compare(const Key& key1, const Key& key2);

    //! Operator< used for map sorting
    bool operator<(const Key& other) const { return compare(*this, other) < 0; }

  private:
    //! We use a flow node as the key since all compares can be done from that
    FLNodeElab* mFlowElab;
  }; // class Key

  //! Abstraction for a map of blocks to flows
  typedef UtMap<Key, FLNodeElabVector*> BlockFlows;

  //! Function to initialize the data (no breakpoints in constructors)
  void init(FLNodeElabFactory* factory);

  //! Data for the block flow nodes
  BlockFlows* mBlockFlows;

  //! Helper class
  SCHUtil* mUtil;
};

#endif // _BLOCKFLOWNODES_H_
