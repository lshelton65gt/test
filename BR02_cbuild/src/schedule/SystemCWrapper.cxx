// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "codegen/codegen.h"
#include "codegen/CGAuxInfo.h"
#include "codegen/CGPortIface.h"
#include "util/OSWrapper.h"

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "iodb/IODBNucleus.h"
#include "iodb/ScheduleFactory.h"
#include "exprsynth/ESPopulateExpr.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUModule.h"
#include "schedule/Schedule.h"
#include "util/ArgProc.h"
#include "util/Stats.h"
#include "Util.h"
#include "Dump.h"
#include "MarkDesign.h"
#include "TimingAnalysis.h"
#include "CreateSchedules.h"
#include "CycleDetection.h"
#include "Util.h"
#include "util/UtString.h"
#include "reduce/RETransform.h"
#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/CarbonDBWrite.h"


enum WrapperPortType 
{
  e2State,
  e4State
};

/*
  returns port type of given net
*/
WrapperPortType sGetPortType(const CGPortIfaceNet *net, bool synthTristates)
{
  WrapperPortType portType;

  // By default all ports are generated as 2-state.
  portType = e2State;

  // The user may have marked the net with directives indicating desired
  // wrapper port type (wrapperPort2State, wrapperPort4State).
  // If so, apply them now.
  if (net->is2State())
    portType = e2State;
  else if (net->is4State())
    portType = e4State;

  // If the port interface thinks the port is a tristate or bidirect,
  // use a 4-state wrapper port.
  if ((synthTristates && net->isTri()) || net->isBid())
    portType = e4State;

  return portType;
}

/*
  Generates the SystemC type for the given net.

  The default mapping is like this:
   WIDTH = 1        -> bool
   2 <= WIDTH <= 64 -> sc_uint<WIDTH>
   WIDTH > 64       -> sc_biguint<WIDTH>

  Bidirectional ports use logic types:
   WIDTH = 1        -> sc_logic
   WIDTH > 1        -> sc_lv<WIDTH>

  If synthTristates is true, logic types are used for all tristate outputs.
*/
static void sGetSystemCType(const CGPortIfaceNet* net, UtString& result, 
                            bool synthTristates)
{
  UInt32 numBits = net->getWidth();
  WrapperPortType portType = sGetPortType(net, synthTristates);
  
  if (numBits == 1)
  {
    if (portType == e2State)
      result = "bool";
    else
      result = "sc_logic";
  }
  else if (portType == e4State)
    result << "sc_lv<" << numBits << "> ";
  else if (numBits <= 64)
  {
    if (! net->isSigned())
      result = "sc_uint<";
    else
      result = "sc_int<";
    result << numBits << "> ";
  }
  else // numBits > 64
  {
    if (! net->isSigned())
      result = "sc_biguint<";
    else
      result = "sc_bigint<";
    result << numBits << "> ";
  }
}

static const char * sIndent(int numSpaces)
{
  static char spaces[] = "                    ";  // max indent = 20
  if (numSpaces > 20)
    numSpaces = 20;
  return spaces + 20 - numSpaces;
}

static void sCodeSystemCPortType(UtOFStream& out, const CGPortIfaceNet* net,
                                 bool synthTristates, int indent)
{
  out << sIndent(indent);

  UtString type;
  sGetSystemCType(net, type, synthTristates);
  
  if (net->isNotAPort())
    out << "sc_signal<" << type << ">";
  else if (net->isInput())
    out << "sc_in<" << type << ">";
  else if (net->isOutput() || net->isTriOut())
    out << "sc_out<" << type << ">";
  else if (net->isBid())
    out << "sc_inout<" << type << ">";
}

static UtString sPortHandle(const CGPortIfaceNet* net,
                            const char* type = NULL)
{
  UtString buf;
  buf = "carbon_";
  UtString name;
  name = net->getGenerateName();
  buf << name;
  if (type)
    buf << "_" << type;
  buf << "_handle";
  return buf;
}

static UtString sSystemCBuf(const CGPortIfaceNet* net)
{
  UtString buf;
  buf = "carbon_";
  UtString name;
  name = net->getGenerateName();
  buf << name;
  buf << "_buffer";
  return buf;
}

static UtString sPortMethod(const CGPortIfaceNet *net)
{
  UtString buf;
  buf = "carbon_";
  UtString name;
  name = net->getGenerateName();
  buf << name;
  buf << "_update";
  return buf;
}

static UInt32 sGetNumWords(UInt32 numBits)
{
  return (numBits + 31)/32;
}

// memcpy's are only done on word boundaries
static UInt32 sGetNumWordBytes(UInt32 numBits)
{
  UInt32 numWords = sGetNumWords(numBits);
  return (numWords * sizeof(UInt32));
}

/*
  Emits code to copy the current value of the SystemC port to the 
  Carbon model input.

  Appears to be called only for non-bidirect nets with 64 or fewer bits width.
 */
static void sEmitPortAssign(UtOFStream &out,
                            const CGPortIfaceNet* ifaceNet, 
                            CGPortIface* ifaceObj,
                            const SCHEvent* clkEvent,
                            bool synthTristates,
                            UtHashSet<const STSymbolTableNode*>& outPortSet,
                            int indentLevel)
{
  UtString netName = ifaceNet->getGenerateName();
  UInt32 nbits = ifaceNet->getWidth();
  const char *indent = sIndent(indentLevel);
  out << indent;
  UtString buf;
  if (CGPortIface::sIsNetHandle(ifaceNet))
  {
    // declare a buffer for the incoming value
    out << "CarbonUInt32 valueN";
    if (nbits > 32)
      out << "[" << sGetNumWords(nbits) << "]";
    out << ";\n";

    WrapperPortType portType = sGetPortType(ifaceNet, synthTristates);
    if (portType == e4State)
    {
      // for logic ports (types sc_logic and sc_lv<N>) we convert to UInt32
      // with a helper if there's more than one bit.
      if (nbits == 1)
        out << indent << "valueN = " << netName << ".read() == sc_dt::Log_1;\n"; 
      else
      {
        out << indent << "carbon_lv_to_uint(" << netName << ".read(), ";
        if (nbits <= 32)
          out << "&";
        out << "valueN);\n";
      }
    }
    else  // 2-state ports
    {
      if (nbits > 64)
      {
        out << indent << netName << ".read().get_packed_rep(valueN);\n";
      }
      else if (nbits > 32)
      {
        // TODO: YUK! clean this up.
        out << indent << "CarbonUInt" << nbits << " valueNBuf = " << netName << ".read();\n";
        out << indent << "valueN[0] = valueNBuf & 0xffffffff;\n";
        out << indent << "valueN[1] = (valueNBuf >> 32) & 0xffffffff;\n";
      }
      else // nbits <= 32
      {
        out << indent << "valueN = ";
        if (clkEvent != NULL)
        {
          switch (clkEvent->getClockEdge())
          {
          case eClockPosedge:
          case eLevelHigh:
            out << "1;\n";
            break;
          case eClockNegedge:
          case eLevelLow:
            out << "0;\n";
          }
        }
        else // no clkEvent
          out << netName << ".read();\n";
      }
    }

    out << indent << "carbonDeposit(carbonModelHandle, " << sPortHandle(ifaceNet, "net");
    
    if (nbits > 32)
      out << ", valueN";
    else
      out << ", &valueN";
    out << ", NULL);\n";
  }
  else // not a netHandle (shell) net.  use port interface.
  {
    UtString uintPort;
    UtString buf2;

    // If carbon_write_outputs() will look at the shadow, we need to
    // set the shadow.
    if (outPortSet.count(ifaceNet->getSymNode()) > 0
        && !ifaceNet->isBid())
      // Update the shadow variable.
      uintPort << sPortHandle(ifaceNet) << " = ";
    uintPort << "*(mPorts." << ifaceObj->getExternalValue(ifaceNet, &buf)
             << ") = static_cast<" << ifaceObj->getCType(ifaceNet, &buf2) << ">(";
    if (clkEvent == NULL)
    {
      WrapperPortType portType = sGetPortType(ifaceNet, synthTristates);
      if (portType == e4State)
      {
        if (nbits == 1)
        {
          out << uintPort << netName << ".read() == sc_dt::Log_1);\n";
        }
        else // more than one bit, use helper function
          out << "  carbon_lv_to_uint(" << netName << ".read(), mPorts." << ifaceObj->getExternalValue(ifaceNet, &buf) << ");\n";
      }
      else
      {
        out << uintPort << netName << ".read());\n"; // just read the uint value into the port interface
      }
    }
    else // there is a clkEvent
    {
      switch (clkEvent->getClockEdge())
      {
      case eClockPosedge:
      case eLevelHigh:
        out << uintPort << "1);\n";
        break;
      case eClockNegedge:
      case eLevelLow:
        out << uintPort << "0);\n";
      }
    }
  }
}


static const char* sGetBidiBitSetFunc()
{
  return "carbon_setbidi_bitvalue";
}

static const char* sGetSetBidiValueSetFunc()
{
  return "carbon_setbidi_value";
}

// Fills the given array with list of interface signals that are not
// primary ports.
static void sGetNonPorts(CGPortIface *portIface, PortNetVec *nonPorts)
{
  for (CGPortIface::NetIter p = portIface->loopNonPorts(); ! p.atEnd(); ++p) {
    CGPortIfaceNet* net = *p;
    nonPorts->push_back(net);
  }
}

/*
  Emits code to copy the current SystemC port value to the Carbon model input.

  Returns whether or not the input causes a carbon_schedule
*/
static bool  sEmitInputCopy(UtOFStream &out,
                            const CGPortIfaceNet* ifaceNet, 
                            CGPortIface* ifaceObj,
                            const SCHEvent* clkEvent,
                            bool synthTristates,
                            UtHashSet<const STSymbolTableNode*>& outPortSet,
                            int indent = 2)
{
  UInt32 numBits = ifaceNet->getWidth();
  UtString netName = ifaceNet->getGenerateName();
  bool isNetHandle = CGPortIface::sIsNetHandle(ifaceNet);
  WrapperPortType portType = sGetPortType(ifaceNet, synthTristates);

  // Emit data to store the change info if we need to. The default
  // value is any change. If the net is a scalar we try to set it to
  // posedge/negedge when necessary.
  if (ifaceNet->getChangeIndex() > -1) {
    if (numBits == 1) {
      out << "    CarbonChangeType change_val = carbon_compute_change_type("
          << netName << ".read());\n";
    } else {
      out << "    CarbonChangeType change_val = CARBON_CHANGE_MASK;\n";
    }
  }

  if (! ifaceNet->isBid())
  {
    UtString buf;  
    if (numBits <= 64)
      sEmitPortAssign(out, ifaceNet, ifaceObj, clkEvent, synthTristates,
                      outPortSet, indent);
    else  // non-bidi input with > 64 bits
    {
      // Vector clocks don't exist currently, so we don't care about the
      // clkEvent variable here.
      if (portType == e4State)
      {
        if (isNetHandle)
        {
          out << sIndent(indent) << "CarbonUInt32 valueN[" << sGetNumWords(numBits) << "];\n";
          out << sIndent(indent) << "carbon_lv_to_uint(" << netName << ".read(), valueN);\n";
          out << sIndent(indent) << "carbonDeposit(carbonModelHandle, " << sPortHandle(ifaceNet, "net") << ", valueN, NULL);\n";
        }
        else
          out << sIndent(indent) << "carbon_lv_to_uint(" << netName << ".read(), mPorts." << ifaceObj->getExternalValue(ifaceNet, &buf) << ");\n";
      }
      else // 2-state wrapper port
      {
        // we're using a sc_biguint: declare a buffer, read into it, then memcpy/deposit the value
        out << sIndent(indent) << "long unsigned valueN[" << (numBits + (sizeof(long unsigned) * 8) -1)/(sizeof(long unsigned) * 8) << "];\n";
        out << sIndent(indent) << netName << ".read().get_packed_rep(valueN);\n";
        out << sIndent(indent);
        if (! isNetHandle)
        {
          out << "memcpy(mPorts." << ifaceObj->getExternalValue(ifaceNet, &buf)
              << ", valueN, " << sGetNumWordBytes(numBits) << ");\n";
          // If this input net is also an output, we need to keep its
          // shadow updated so carbon_write_outputs() will know that
          // the SystemC port needs to be written.
          if (outPortSet.count(ifaceNet->getSymNode()))
            out << sIndent(indent)
                << "memcpy(" << sPortHandle(ifaceNet) << ", valueN, "
                << sGetNumWordBytes(numBits) << ");\n";
        }
        else
          out << "carbonDeposit(carbonModelHandle, " << sPortHandle(ifaceNet, "net") << ", valueN, NULL);\n";
      }
    }
  }
  else
  {
    // it's a bidi. Completely different copying scheme. The shadow
    // needs to be updated at this point. The read() has already been
    // called on the io and placed into a buffer.
    UtString xdata, idata, xdrive;
    ifaceObj->getExternalValue(ifaceNet, &xdata);
    ifaceObj->getInternalValue(ifaceNet, &idata);
    ifaceObj->getExternalDrive(ifaceNet, &xdrive);
    int indent = 4;
    UtString shadow = sPortHandle(ifaceNet);
    
    out << sIndent(indent) << shadow << " = " << sSystemCBuf(ifaceNet) << ";\n";
    
    out << sIndent(indent);
    UtString argStr;
    if (isNetHandle)
    {
      argStr << "(" << shadow << ", " << sPortHandle(ifaceNet, "xdata") << ", " << sPortHandle(ifaceNet, "idata") << ", " << sPortHandle(ifaceNet, "xdrive") << ");\n";
      if (numBits == 1)
        out << sGetBidiBitSetFunc() << argStr;
      else
        // call the template function
        out << sGetSetBidiValueSetFunc() << argStr;

      out << sIndent(indent) << "carbonDeposit(carbonModelHandle, " << sPortHandle(ifaceNet, "net") << ", " << sPortHandle(ifaceNet, "xdata") << ", " << sPortHandle(ifaceNet, "xdrive") << ");\n";
    }
    else // not a netHandle. use port interface.
    {
      argStr << "(" << shadow << ", mPorts." << xdata << ", mPorts." << idata << ", mPorts." << xdrive << ");\n";
      if (numBits == 1)
        out << sGetBidiBitSetFunc() << argStr;
      else
        // call the template function
        out << sGetSetBidiValueSetFunc() << argStr;
    }
  }
  
  bool callSched = false;
  
  SInt32 chgArrIndex = ifaceNet->getChangeIndex();
  if (chgArrIndex > -1)
  {
    out << sIndent(indent) << "mPorts." << ifaceObj->getChangeArray() << "[" << chgArrIndex << "] = change_val;\n";
    callSched = true;
  }
  if (ifaceNet->isRunCombo())
  {
    out << sIndent(indent) << "*mPorts." << ifaceObj->getRunComboPtr() << " = true;\n";
    callSched = true;
  }
  return callSched;
}



static void sGenSensitivity(UtOFStream& hout, const UtString& netName, int numSpaces)
{
  hout << sIndent(numSpaces) << "sensitive(" << netName << ");\n";
}

static void sGenEdgeSensitivity(UtOFStream& hout, const UtString& netName, 
                                const SCHEvent* clkEvent, int numSpaces,
                                const CGPortIfaceNet* ifaceNet)
{
  hout << sIndent(numSpaces);
  hout << "sensitive << ";
  hout << netName;
  
  switch (clkEvent->getClockEdge())
  {
  case eClockPosedge:
  case eLevelHigh:
    if (ifaceNet->isNotAPort())
      hout << ".posedge_event()";
    else
      hout << ".pos()";
    break;
  case eClockNegedge:
  case eLevelLow:
    if (ifaceNet->isNotAPort())
      hout << ".negedge_event()";
    else
      hout << ".neg()";
    break;
  }
  
  hout << ";\n";
}

static const char* sGetBitsToStrFuncName(const CGPortIfaceNet* net)
{
  UInt32 numbits = net->getWidth();
  if (net->isBid())
  {
    if (numbits <= 64)
      return "carbon_calcBidi_smallval";
    else
      return "carbon_calcBidi_largeval";
  }
  else
  {
    if (numbits <= 64)
      return "carbon_smallval_to_str";
    else
      return "carbon_largeval_to_str";
  }
  return NULL;
}

static void sGetTriStringValue(UtOFStream& out, 
                               const CGPortIfaceNet* ifaceNet, 
                               CGPortIface* portIface,
                               int indent, UtString* portHandleStr)
{
  UInt32 nbits = ifaceNet->getWidth();
  *portHandleStr = sPortHandle(ifaceNet);

  *portHandleStr << "_value";
  out << sIndent(indent) << "char " << *portHandleStr << "[" << nbits + 1 << "];\n";
  out << sIndent(indent);
  
  if (! CGPortIface::sIsNetHandle(ifaceNet))
  {
    out << sGetBitsToStrFuncName(ifaceNet);
    
    out << "(" << *portHandleStr << ", ";
    
    UtString idrive;
    portIface->getInternalDrive(ifaceNet, &idrive);
    
    if (ifaceNet->isBid())
    {
      UtString idata;
      UtString xdrive;
      portIface->getInternalValue(ifaceNet, &idata);
      portIface->getExternalDrive(ifaceNet, &xdrive);
      
      if (nbits <= 64)
        out << "*";
      
      out << "mPorts." << idata << ", ";
      
      if (nbits <= 64)
        out << "*";
      
      out << "mPorts." << xdrive << ", ";
      
      if (nbits <= 64)
        out << "*";
      
      out << "mPorts." << idrive << ", ";
    }
    else
    {
      UtString xdata;
      portIface->getExternalValue(ifaceNet, &xdata);
      
      if (nbits <= 64)
        out << "*";
      
      out << "mPorts." << xdata << ", ";
      
      if (nbits <= 64)
        out << "*";
      
      out << "mPorts." << idrive << ", ";
    }
    
    if (ifaceNet->isPulled())
      out << "true";
    else
      out << "false";
    out << ", " << nbits << ");\n";
  }
  else
  {
    out << "carbonFormat(carbonModelHandle, " << sPortHandle(ifaceNet, "net") << ", " << *portHandleStr << ", " << (nbits + 1) << ", eCarbonBin);\n";
    out << sIndent(indent) << "carbon_toupper(" << *portHandleStr << ", " << nbits << ");\n";
  }
}

// Declare a SystemC port for primary ports, or a signal for
// scDepositSignal, scObserveSignal.
static void sDeclareSystemCPort(UtOFStream& hout, const CGPortIfaceNet* net,
                                bool synthTristates)
{
  sCodeSystemCPortType(hout, net, synthTristates, 2);
  
  UtString  name = net->getGenerateName();
  hout << " \t" << name << ";\t// " << net->getFullName();
  if (net->isDead())
    hout << ", not used";
  hout << "\n";
}

//  Generate SystemC declarations for all module ports.
static void sDeclareSystemCPorts(UtOFStream& hout, CGPortIface *portIface,
                                 bool synthTristates, IODBNucleus *db, PortNetVec &nonPorts)
{
  for (CGPortIface::NetIter iter = portIface->loopPrimaryPorts(db); !iter.atEnd(); ++iter) {
    sDeclareSystemCPort(hout, *iter, synthTristates);
  }
  
  for (PortNetVec::iterator p = nonPorts.begin(); p != nonPorts.end(); ++p) {
    sDeclareSystemCPort(hout, *p, synthTristates);
  }
}  

// Generate a member init for constructor with name string.
static void sNameSystemCPort(UtOFStream& hout, const CGPortIfaceNet* net)
{
  UtString  name = net->getGenerateName();

  hout << ",\n    " << name << "(\"" << name << "\")";
}

static void sAssignShadow(UtOFStream& hout, const CGPortIfaceNet* ifaceNet, 
                          const char* type, const UtString& memberName, 
                          int indent)
{
  UInt32 numBits = ifaceNet->getWidth();
  UtString portHandle = sPortHandle(ifaceNet, type);
  hout << sIndent(indent);
  if (numBits > 64)
  {
    hout << "memcpy(" << portHandle << ", mPorts." << memberName << ", " << sGetNumWordBytes(numBits) << ");\n";
  }
  else
    hout << portHandle << " = *mPorts." << memberName << ";\n";
}

static void sEmitInitializeStatement(UtOFStream& hout,
                                     const char *indent,
                                     const CGPortIfaceNet* ifaceNet, 
                                     const char *lhs,
                                     const char *rhs)
{
  if (ifaceNet->isNotAPort())
    hout << indent << lhs << ".write(" << rhs << ");\n";
  else
    hout << indent << lhs << ".initialize(" << rhs << ");\n";
}

static void sInitializeShadow(UtOFStream& hout, 
                              const CGPortIfaceNet* ifaceNet, 
                              CGPortIface* portIface, 
                              bool isUsed,
                              bool synthTristates)
{
  int indent = 2;
  const char* nameStr = ifaceNet->getGenerateName();
  UInt32 numBits = ifaceNet->getWidth();
  bool isNetHandle = CGPortIface::sIsNetHandle(ifaceNet);
  WrapperPortType portType = sGetPortType(ifaceNet, synthTristates);

  if (ifaceNet->isBid())
  {
    // Set schedchange to true. The initialize ends up calling our
    // input change routines.
    hout << sIndent(indent) << "m_" << nameStr << "_schedchange = true;\n";

    if (isNetHandle)
    {
      // initialize the data buffers that are used to update the bidis
      // after the schedule call. Just set them to 0. The character
      // based buffer will set these correctly.
      UInt32 wordBytes = sGetNumWordBytes(numBits);      
      hout << sIndent(indent) << "memset(" << sPortHandle(ifaceNet, "xdrive") << ", 0, " << wordBytes << ");\n";
      hout << sIndent(indent) << "memset(" << sPortHandle(ifaceNet, "idata") << ", 0, " << wordBytes << ");\n";
      hout << sIndent(indent) << "memset(" << sPortHandle(ifaceNet, "xdata") << ", 0, " << wordBytes << ");\n";
    }

    UtString portHandleValue;
    sGetTriStringValue(hout, ifaceNet, portIface, indent, &portHandleValue);
    
    hout << sIndent(indent) << sPortHandle(ifaceNet) << " = " << portHandleValue;
    if (numBits == 1)
      hout << "[0]";
    hout << ";\n";
    sEmitInitializeStatement(hout, sIndent(indent), ifaceNet, nameStr, sPortHandle(ifaceNet).c_str());
  }
  else // not a bidirectional net
  {
    UtString xdata;
    if (! isNetHandle)
    {
      if (ifaceNet->isBid()) // i don't think this can ever happen
        portIface->getInternalValue(ifaceNet, &xdata);
      else
        portIface->getExternalValue(ifaceNet, &xdata);
      
      if (isUsed)
        sAssignShadow(hout, ifaceNet, NULL, xdata, 2);
    }

    if (synthTristates && ifaceNet->isTriOut())
    {
      if (isUsed && ! isNetHandle)
      {
        UtString idrive;
        portIface->getInternalDrive(ifaceNet, &idrive);
        sAssignShadow(hout, ifaceNet, NULL, idrive, indent);
      }
      
      // need to initialize the port itself
      UtString portHandleStr;
      sGetTriStringValue(hout, ifaceNet, portIface, indent, &portHandleStr);
      
      hout << sIndent(indent);
      if (numBits == 1)
      {
        UtString bufName;
        bufName << nameStr << "_buf";
        hout << "sc_logic " << bufName << ";\n";
        hout << sIndent(indent) << bufName << " = " << portHandleStr << "[0];\n";
        sEmitInitializeStatement(hout, sIndent(indent), ifaceNet, nameStr, bufName.c_str());
      }
      else
        sEmitInitializeStatement(hout, sIndent(indent), ifaceNet, nameStr, portHandleStr.c_str());
    }
    else // not a triOut or synthTristates is not turned on
    {
      if (isNetHandle)
      {
        hout << sIndent(indent);
        hout << "carbonExamine(carbonModelHandle, " << sPortHandle(ifaceNet, "net") << ", " << sPortHandle(ifaceNet, "valbuf") << ", NULL);\n";
        UInt32 nbits = ifaceNet->getWidth();
        if (portType == e4State)
        {
          if (nbits == 1)
          {
            UtString valueStr;
            valueStr << "sc_logic(*" << sPortHandle(ifaceNet, "valbuf") << " == 1)";
            sEmitInitializeStatement(hout, sIndent(indent), ifaceNet, nameStr, valueStr.c_str());
          }
          else
          {
            // the port we are initializing is of type sc_lv<N>, so we
            // must convert the value from the port interface.
            UtString sysCType;
            sGetSystemCType(ifaceNet, sysCType, synthTristates);
            hout << sysCType << sSystemCBuf(ifaceNet) << ";\n";
            hout << sIndent(indent) << "carbon_uint_to_lv(" << sPortHandle(ifaceNet, "valbuf") << ", " << sSystemCBuf(ifaceNet) << ");\n";
            sEmitInitializeStatement(hout, sIndent(indent), ifaceNet, nameStr, sSystemCBuf(ifaceNet).c_str());
          }
        }          
        else // not a logic type
        {     
          if (nbits > 64)
          {
            UtString sysCType;
            sGetSystemCType(ifaceNet, sysCType, synthTristates);
            hout << sIndent(indent) << sysCType << sSystemCBuf(ifaceNet) << ";\n";

            hout << sIndent(indent) << sSystemCBuf(ifaceNet) << ".set_packed_rep((unsigned long*) " 
                 << sPortHandle(ifaceNet, "valbuf") << ");\n";
            sEmitInitializeStatement(hout, sIndent(indent), ifaceNet, nameStr, sSystemCBuf(ifaceNet).c_str());
          }
          else
          {
            UtString valueStr;
            valueStr << "*" << sPortHandle(ifaceNet, "valbuf");
            sEmitInitializeStatement(hout, sIndent(indent), ifaceNet, nameStr, valueStr.c_str());
          }
        }
      }
      else // not a netHandle net.  use port interface.
      {
        if (numBits <= 64)
        {
          UtString valueStr;
          if (portType == e4State)
          {
            // port is either sc_logic or sc_lv[X]
            // TODO: use convert func
            UtString sysCType;
            sGetSystemCType(ifaceNet, sysCType, synthTristates);
            valueStr << sysCType << "(*mPorts." << xdata << ")";
          }
          else
          {
            valueStr << "*mPorts." << xdata;
          }
          sEmitInitializeStatement(hout, sIndent(indent), ifaceNet, nameStr, valueStr.c_str());
        }
        else // width > 64 bits
        {
          hout << sIndent(indent);
          UtString sysCType;
          sGetSystemCType(ifaceNet, sysCType, false);
          hout << sysCType << " " << sSystemCBuf(ifaceNet) << ";\n";
          if (portType == e4State)
          {
            // the port we are initializing is of type sc_lv<N>, so we
            // must convert the value from the port interface.
            hout << sIndent(indent) << "carbon_uint_to_lv(mPorts." << xdata << ", " << sSystemCBuf(ifaceNet) << ");\n";
          }
          else // e2State - SystemC port type is sc_biguint<N>
          {
            hout << sIndent(indent) << sSystemCBuf(ifaceNet) << ".set_packed_rep((unsigned long*) mPorts." << xdata << ");\n";
          }
          sEmitInitializeStatement(hout, sIndent(indent), ifaceNet, nameStr, sSystemCBuf(ifaceNet).c_str());
        }
      }
    }
  }
}


static void sUpdateShadow(UtOFStream& out, 
                          const CGPortIfaceNet* ifaceNet, 
                          CGPortIface* portIface,
                          bool synthTristates)
{
  const char *indent = sIndent(2);

  WrapperPortType portType = sGetPortType(ifaceNet, synthTristates);
  UtString nameStr(ifaceNet->getGenerateName());
  
  if (! ifaceNet->isBid() && CGPortIface::sIsNetHandle(ifaceNet))
  {
    out << indent << "carbonExamine(carbonModelHandle, " << sPortHandle(ifaceNet, "net") << ", " << sPortHandle(ifaceNet, "valbuf") << ", NULL);\n";
    UInt32 nbits = ifaceNet->getWidth();

    if (portType == e4State)
    {
      if (nbits == 1)
        out << indent << nameStr << ".initialize(sc_logic(*" << sPortHandle(ifaceNet, "valbuf") << " == 1));\n";
      else
      {
        // the port we are initializing is of type sc_lv<N>, so we
        // must convert the value from the port interface.
        UtString sysCType;
        sGetSystemCType(ifaceNet, sysCType, synthTristates);
        out << sysCType << sSystemCBuf(ifaceNet) << ";\n";
        out << indent << "carbon_uint_to_lv(" << sPortHandle(ifaceNet, "valbuf") << ", " << sSystemCBuf(ifaceNet) << ");\n";
        out << indent << nameStr << ".write(" << sSystemCBuf(ifaceNet) << ");\n";
      }
    }          
    else // e2State - uint type
    {
      if (nbits > 64)
      {
        UtString sysCType;
        sGetSystemCType(ifaceNet, sysCType, synthTristates);
        out << indent << sysCType << sSystemCBuf(ifaceNet) << ";\n";
        out << indent << sSystemCBuf(ifaceNet) << ".set_packed_rep((unsigned long*) " 
            << sPortHandle(ifaceNet, "valbuf") << ");\n";
        out << indent << nameStr << ".write(" << sSystemCBuf(ifaceNet) << ");\n";
      }
      else
        out << nameStr << ".write(*" << sPortHandle(ifaceNet, "valbuf") << ");\n";
    }
  }
  else
  {
    UtString xdata;
    portIface->getExternalValue(ifaceNet, &xdata);
    UtString idrive;
    
    UInt32 nbits = ifaceNet->getWidth();
    const char* nameStr = ifaceNet->getGenerateName();
   
    if (ifaceNet->isBid())
    {
      // scope the declared char arrays for better lifetime management
      out << "  {\n"; // begin char scope

      UtString portHandle = sPortHandle(ifaceNet);
      UtString portHandleValue;
      sGetTriStringValue(out, ifaceNet, portIface, 4, &portHandleValue);
      out << "    " << portHandle << " = " << portHandleValue;
      if (nbits == 1)
        out << "[0]";
      out << ";\n";

      out << "  }\n"; // end char scope

      out << "  if (" << portHandle << " != " << nameStr << ".read()) {\n";
      out << "    m_" << nameStr << "_schedchange = true;\n";
      out << "    m_" << nameStr << "_event.notify(SC_ZERO_TIME);\n";
      out << "  }\n";
    }
    else // net is not bidirectional
    {
      bool isTriOut = synthTristates && ifaceNet->isTriOut();
      if (isTriOut)
        portIface->getInternalDrive(ifaceNet, &idrive);
    
      bool needGrouping = isTriOut;
  
      out << "  if (";
      if (needGrouping)
        out << "(";

      if (nbits <= 64)
      {
        out << sPortHandle(ifaceNet) << " != *mPorts." << xdata << ")";
        if (isTriOut)
          out << " || (" << sPortHandle(ifaceNet, "idrive") << " != *mPorts." << idrive << ")";
        if (needGrouping)
          out << ") ";
        out << "{\n";

        sAssignShadow(out, ifaceNet, NULL, xdata, 4);
        if (isTriOut)
        {
          sAssignShadow(out, ifaceNet, "idrive", idrive, 4);
      
          UtString portHandleStr;
          sGetTriStringValue(out, ifaceNet, portIface, 4, &portHandleStr);
      
          if (nbits == 1)
          {
            out << "    sc_logic " <<  nameStr << "_buf;\n";
            out << "    " << nameStr  << "_buf = " << portHandleStr << "[0];\n";
            out << "    " << nameStr << ".write(" << nameStr << "_buf);\n";
          }
          else
          {
            out << "    sc_lv<" << nbits << "> " << nameStr << "_buf;\n";
            out << "    " << nameStr  << "_buf = " << portHandleStr << ";\n";
            out << "    " << nameStr << ".write(" << nameStr << "_buf);\n";
          }
        }
        else // not a tristate output
        {
          if (portType == e4State) // if it is a logic value port
          {
            UtString sysCType;
            sGetSystemCType(ifaceNet, sysCType, false);
            out << "    " << nameStr << ".write(" << sysCType << "(" << sPortHandle(ifaceNet) <<  "));\n";
          }
          else
            out << "    " << nameStr << ".write(" << sPortHandle(ifaceNet) <<  ");\n";
        }
      }
      else // nbits > 64
      {
        UInt32 wordBytes = sGetNumWordBytes(nbits);
        out << "memcmp(" << sPortHandle(ifaceNet) << ", mPorts." << xdata << ", " << wordBytes << ") != 0)";
        if (isTriOut)
          out << " || (memcmp(" << sPortHandle(ifaceNet, "idrive") << ", mPorts." << portIface->getInternalDrive(ifaceNet, &idrive) << ", " << wordBytes << ") != 0)";
      
        if (needGrouping)
          out << ") ";
        out << "{\n";
      
        sAssignShadow(out, ifaceNet, NULL, xdata, 4);
        if (isTriOut)
        {
          sAssignShadow(out, ifaceNet, "idrive", idrive, 4);

          UtString portHandleStr;
          sGetTriStringValue(out, ifaceNet, portIface, 4, &portHandleStr);
          out << "    sc_lv<" << nbits << "> " << nameStr << "_buf;\n";
          out << "    " << nameStr  << "_buf = " << portHandleStr << ";\n";
          out << "    " << nameStr << ".write(" << nameStr << "_buf);\n";
        }
        else // not a tri out
        {
          UtString sysCType;
          sGetSystemCType(ifaceNet, sysCType, false);
          out << sysCType << " " << sSystemCBuf(ifaceNet) << ";\n";
          if (portType == e4State)
          {
            // the port we are initializing is of type sc_lv<N>, so we
            // must convert the value from the port interface.

            out << "    carbon_uint_to_lv(mPorts." << xdata << ", " << sSystemCBuf(ifaceNet) << ");\n";
            out << "    " << nameStr << ".write(" << sSystemCBuf(ifaceNet) << ");\n";
          }
          else // 2-state port: SystemC port type is sc_biguint<N>
          {
            out << "    " << sSystemCBuf(ifaceNet) << ".set_packed_rep((unsigned long*) mPorts." << xdata << ");\n";
            out << "    " << nameStr << ".write(" << sSystemCBuf(ifaceNet) << ");\n";
          }
        }
      } // nbits > 64
      out << "  }\n";
    } // else ! bidi
  }
}

static void sEmitInitializeMethod(UtOFStream &out,
                                  const char *moduleName,
                                  CGPortIface *portIface,
                                  PortNetSet &validOutPorts,
                                  PortNetSet &validTriOutPorts,
                                  PortNetSet &validBidiPorts,
                                  bool synthTristates)
{
  // implement initialization function
  // NOTE: This function is called by SystemC during initialization.
  //       We do not do this work in the constructor because 
  //       MTI does not allow us to call initialize() methods within the 
  //       constructor (bug 4015).
  out << 
    "void " << moduleName << "::end_of_elaboration()\n"
    "{\n"
    "  carbon_schedule_channel.setCarbonModel( carbonModelHandle );\n"
    "  carbonAdminAddControlCB(carbonModelHandle, carbonScSaveWrapperState, this, eCarbonSave);\n"
    "  carbonAdminAddControlCB(carbonModelHandle, carbonScRestoreWrapperState, this, eCarbonRestore);\n";
  
  // Must initialize all outputs. Some may be constant.
  for (CGPortIface::NetIter p = portIface->loopOutputDir(); ! p.atEnd(); ++p)
  {
    const CGPortIfaceNet* ifaceNet = *p;
    if (! ifaceNet->isBid())
    {
      bool isValid = (validOutPorts.find(ifaceNet) != validOutPorts.end())
        || (validTriOutPorts.find(ifaceNet) != validTriOutPorts.end());
      sInitializeShadow(out, ifaceNet, portIface, isValid, synthTristates);
    }
  }

  // bidi shadows are full blown systemc types, in order to be able to
  // determine the resolve value and whether or not the sensitive
  // method is going to be called after a schedule() call.
  // only valid bidis can be left. Invalid bidis become tristates
  for (PortNetSet::iterator p = validBidiPorts.begin(); p != validBidiPorts.end(); ++p)
  {
    const CGPortIfaceNet* ifaceNet = *p;
    sInitializeShadow(out, ifaceNet, portIface, true, synthTristates);
  }


  out << "}\n\n";
}

// sort each given net into the set of clock or non-clock (data) outputs
static void sSortOutputs(PortNetSet &netSet,
                         SCHMarkDesign* markDesign,
                         PortNetSet *clockOutputs, PortNetSet *dataOutputs)
{
  for (PortNetSet::iterator p = netSet.begin(); p != netSet.end(); ++p)
  {
    const CGPortIfaceNet* ifaceNet = *p;
    const NUNetElab *netElab = ifaceNet->getNetElab();
    bool isClock = markDesign->isClock(netElab);
    if (isClock) {
      clockOutputs->insert(ifaceNet);
    }
    else {
      dataOutputs->insert(ifaceNet);
    }
  }
}

// Output a method to write output port changes back to SystemC.
// If we are inserting a delta cycle between the clock outputs and data outputs, 
// (notifyNextMethod == true), generate a notify on the delayed outputs write event.
static void sEmitWriteOutputsMethod(UtOFStream &out,
                                    const char *moduleName,
                                    const char *methodName,
                                    CGPortIface *portIface,
                                    PortNetSet &ports,
                                    bool synthTristates,
                                    bool notifyNextMethod)
{
  // Declare entry point.
  out <<
    "void " << moduleName << "::" << methodName << "()\n"
    "{\n";

  // Update the output values. First, update the shadow variable then
  // write it to the systemc model if it changed
  for (PortNetSet::iterator p = ports.begin(); p != ports.end(); ++p)
  {
    const CGPortIfaceNet* ifaceNet = *p;
    sUpdateShadow(out, ifaceNet, portIface, synthTristates);
  }

  // if we need to create a delta cycle for non-clock output ports
  if (notifyNextMethod) {
    out << "  carbon_schedule_channel.carbon_write_delayed_outputs_event.notify(SC_ZERO_TIME);\n";
  }

  out << "}\n";
}

// Emit one or more methods to write values back to systemC.
static void sEmitWriteOutputsMethods(UtOFStream &out,
                                     const char *moduleName,
                                     CGPortIface *portIface,
                                     PortNetSet &clockOutputs,
                                     PortNetSet &dataOutputs,
                                     bool synthTristates)
{
  // if there are both clock and non-clock outputs, output 2 methods to insert a delta cycle between them.
  if (clockOutputs.size() && dataOutputs.size()) {
    sEmitWriteOutputsMethod(out, moduleName, "carbon_write_outputs", portIface, clockOutputs, synthTristates, true);
    sEmitWriteOutputsMethod(out, moduleName, "carbon_write_delayed_outputs", portIface, dataOutputs, synthTristates, false);
  }
  else {
    if (clockOutputs.size()) {
      // there are only clocks, just emit a single method
      sEmitWriteOutputsMethod(out, moduleName, "carbon_write_outputs", portIface, clockOutputs, synthTristates, false);
    }
    else {
      // there are only data outputs, just emit a single method
      sEmitWriteOutputsMethod(out, moduleName, "carbon_write_outputs", portIface, dataOutputs, synthTristates, false);
    }
  }
}

/*
 * Generates a line of code like this:
 *   carbonCheckpointWrite(carbonObject, &module->var, sizeof(module->var));
 *
 * the addrOf parameter determines whether we need the '&' operator to
 * get an address for the data buffer argument to
 * carbonCheckpointWrite/Read.
 */
static void sCheckpointReadWrite(bool isSave, 
                                 UtOFStream& out, 
                                 const char *variable,
                                 bool addrOf = true)
{
  const char *cApiFunc = isSave ? "carbonCheckpointWrite" : "carbonCheckpointRead";  
  out << "  " << cApiFunc 
      << "(carbonObject, "
      << (addrOf ? "&" : "") << "module->" << variable << ","
      << "sizeof(module->" << variable << "));\n";
}

/*
 * Emit code to save or restore the value of the given net.
 *
 * The names of buffers to be saved is derived from sDeclarePortHandle.
 */

static void sCheckpointPort(bool isSave,
                              UtOFStream& out, 
                              const CGPortIfaceNet* ifaceNet, 
                              bool synthTristates)
{
  UtString buf;
  UtString netName = ifaceNet->getGenerateName();
  UInt32 nbits = ifaceNet->getWidth();


  bool isNetHandle = CGPortIface::sIsNetHandle(ifaceNet);

  if (ifaceNet->isBid())
  {
    sCheckpointReadWrite(isSave, out, sPortHandle(ifaceNet).c_str());

    if (isNetHandle)
    {
      sCheckpointReadWrite(isSave, out, sPortHandle(ifaceNet, "xdrive").c_str(), false);
      sCheckpointReadWrite(isSave, out, sPortHandle(ifaceNet, "idata").c_str(), false);
      sCheckpointReadWrite(isSave, out, sPortHandle(ifaceNet, "xdata").c_str(), false);
    }
  }
  else
  {
    if (isNetHandle)
    {
      sCheckpointReadWrite(isSave, out, sPortHandle(ifaceNet, "valbuf").c_str(), false);
    }
    else
    {
      sCheckpointReadWrite(isSave, out, sPortHandle(ifaceNet).c_str(), nbits <= 64);
      
      if (synthTristates && ifaceNet->isTriOut())
      {
        sCheckpointReadWrite(isSave, out, sPortHandle(ifaceNet, "idrive").c_str(),  nbits <= 64);
      }
    }
  }
}

/*
 * Emit methods to save and restore the model (and wrapper) state.
 */
static void sEmitCheckpointMethod(bool isSave,
                                  UtOFStream &out,
                                  const char *moduleName,
                                  PortNetSet &validInPorts,
                                  PortNetSet &validOutPorts,
                                  PortNetSet &validTriOutPorts,
                                  PortNetSet &validBidiPorts,
                                  bool synthTristates)
{
  const char *method = isSave ? "carbonScSaveWrapperState" : "carbonScRestoreWrapperState";

  out <<
    "void " << moduleName << "::" << method << "(CarbonObjectID *carbonObject, \n"
    "    CarbonControlType callbackType, CarbonClientData clientData,\n"
    "    const char *fileName, int lineNumber)\n"
    "{\n"
    "  (void) carbonObject;\n"
    "  (void) callbackType;\n"
    "  (void) fileName;\n"    
    "  (void) lineNumber;\n"
    "  " << moduleName << " *module = (" << moduleName << "*) clientData;\n"
    "  (void) module;\n";

  for (PortNetSet::iterator p = validInPorts.begin(); p != validInPorts.end(); ++p) {
    if (CGPortIface::sIsNetHandle(*p)) {
      sCheckpointPort(isSave, out, *p, synthTristates);
    }
  }

  for (PortNetSet::iterator p = validOutPorts.begin(); p != validOutPorts.end(); ++p) {
    sCheckpointPort(isSave, out, *p, synthTristates);
  }

  for (PortNetSet::iterator p = validTriOutPorts.begin(); p != validTriOutPorts.end(); ++p) {
    sCheckpointPort(isSave, out, *p, synthTristates);
  }

  for (PortNetSet::iterator p = validBidiPorts.begin(); p != validBidiPorts.end(); ++p) {
    sCheckpointPort(isSave, out, *p, synthTristates);
  }

  out <<
    "}\n\n";
}

/*
 * NOTE: if you change this, please update sCheckpointPort
 */
static void sDeclarePortHandle(UtOFStream& hout, 
                               const CGPortIfaceNet* ifaceNet, 
                               CGPortIface* portIface,
                               bool synthTristates)
{
  UtString buf;
  UtString netName = ifaceNet->getGenerateName();
  UInt32 nbits = ifaceNet->getWidth();

  if (ifaceNet->isBid())
  {
    // declare a boolean to deal with changes to the inout causing an
    // infinite loop between the io_update method and the schedule
    // method
    hout << "  bool m_" << netName << "_schedchange;\n";
    // declare an event notifier to resolve the value
    hout << "  sc_event m_" << netName << "_event;\n";
  }

  bool isNetHandle = CGPortIface::sIsNetHandle(ifaceNet);

  if (ifaceNet->isBid())
  {
    sGetSystemCType(ifaceNet, buf, synthTristates);
    hout << "  " << buf << " " << sPortHandle(ifaceNet) << "; \t// " << netName << "\n";
    if (isNetHandle)
    {
      hout << "  CarbonNetID* " << sPortHandle(ifaceNet, "net") << ";\n";

      UInt32 numWords = sGetNumWords(nbits);
      hout << "  UInt32 " << sPortHandle(ifaceNet, "xdrive") << "[" << numWords << "];\n";
      hout << "  UInt32 " << sPortHandle(ifaceNet, "idata") << "[" << numWords << "];\n";
      hout << "  UInt32 " << sPortHandle(ifaceNet, "xdata") << "[" << numWords << "];\n";
    }
  }
  else
  {
    hout << "  ";
    if (isNetHandle)
    {
      hout << "CarbonNetID* " << sPortHandle(ifaceNet, "net") << "; \t// " << netName << "\n";
      hout << "  UInt32 " << sPortHandle(ifaceNet, "valbuf");
      hout << "[" << sGetNumWords(nbits) << "];\n";
    }
    else
    {
      hout << portIface->getCType(ifaceNet, &buf) << " " << sPortHandle(ifaceNet);
      if (nbits > 64)
        hout << "[" << sGetNumWords(nbits) << "]";
      
      hout << "; \t// " << netName << "\n";
      
      if (synthTristates && ifaceNet->isTriOut())
      {
        hout << "  " << buf << " " << sPortHandle(ifaceNet, "idrive");
        if (nbits > 64)
          hout << "[" << sGetNumWords(nbits) << "]";
        hout << ";\n";
      }
    }
  }
}

static void sDeclarePortHandles(UtOFStream &hout,
                                CGPortIface *portIface,
                                PortNetSet &validInPorts,
                                PortNetSet &validOutPorts,
                                PortNetSet &validTriOutPorts,
                                PortNetSet &validBidiPorts,
                                bool synthTristates,
                                PortNetSet *netHandles)
{
  for (PortNetSet::iterator p = validInPorts.begin(); p != validInPorts.end(); ++p)
  {
    const CGPortIfaceNet* net = *p;
    // net handle inputs have to be declared explicitly.
    if (CGPortIface::sIsNetHandle(net))
    {
      sDeclarePortHandle(hout, net, portIface, synthTristates);
      netHandles->insert(net);
    }
  }

  for (PortNetSet::iterator p = validOutPorts.begin(); p != validOutPorts.end(); ++p)
  {
    const CGPortIfaceNet* net = *p;
    sDeclarePortHandle(hout, net, portIface, synthTristates);
    if (CGPortIface::sIsNetHandle(net))
      netHandles->insert(net);
  }

  for (PortNetSet::iterator p = validTriOutPorts.begin(); p != validTriOutPorts.end(); ++p)
  {
    const CGPortIfaceNet* net = *p;
    sDeclarePortHandle(hout, net, portIface, synthTristates);
    if (CGPortIface::sIsNetHandle(net))
      netHandles->insert(net);
  }

  for (PortNetSet::iterator p = validBidiPorts.begin(); p != validBidiPorts.end(); ++p)
  {
    const CGPortIfaceNet* net = *p;
    sDeclarePortHandle(hout, net, portIface, synthTristates);
    if (CGPortIface::sIsNetHandle(net))
      netHandles->insert(net);
  }
}

/*
  This uses the PortInterface for most signals. For complex signals
  such as split ports or forcible nets, we use the old CarbonNetID*
  method of assigning and examining the value. 

  For expressioned nets (split ports) this may be just as fast or
  faster than if we used the port interface and somehow brought all
  those together in systemc. We would have to have a wrapper over the
  wrapper in that case to re-assemble the net. And that COULD be
  slower. So, using a CarbonNetID* for expression nets makes a lot of
  sense.

  Another mindblowing thing with expressioned nets is that we assume
  bidi on them. This is because the original NUNet's are dead and we
  don't have a conscise mechanism to run through the expression and
  figure out exactly how we should interpret it. We could use the
  expression walker, but running through the schedule containers
  figuring out what is active and so on, could prove to be difficult
  to keep consistent. So, treating them as bidi is pessimistic, but
  safe. And, implicit in this is that all expressioned nets are
  active, whether they really are or not. So, there will be update
  methods generated for them.

  For forcible and depositable tristate outputs we could and probably
  should use the port interface; however, we need stability in
  compilation. And, it is more unlikely that a customer would mark an
  internal output as depositable or even declare a forcible if they
  are using systemc. So, using CarbonNetID* for those currently as
  well.

  It is important to note that the change array must still be updated
  when using carbonDeposit on a clock. The deposit looks for changes,
  and if we are only updating the net on one edge, it will never
  change. So, we must force the schedule(s) to run by marking the
  change array appropriately.
*/
void
SCHSchedule::generateSystemCWrapper(
  const char* fileRoot,
  bool schedAllClkEdges,
  bool synthTristates,
  CNetSet &bothEdgeClks,
  PortNetSet &validOutPorts,
  PortNetSet &validInPorts,
  PortNetSet &validTriOutPorts,
  PortNetSet &validBidiPorts,
  CNetSet &clksUsedAsData,
  ClkToEvent &clkEdgeUses )
{
  gCodeGen->putInterfaceMode();

  // Get the top module, there should only be one
  NUDesign* design = mUtil->getDesign();
  NUModule* module = NULL;
  for (NUDesign::ModuleLoop l = design->loopTopLevelModules();
       !l.atEnd(); ++l)
  {
    NU_ASSERT(module == NULL, module);
    module = *l;
  }

  NUModuleElab* elabMod = module->lookupElab(mUtil->getSymbolTable());
  NU_ASSERT(elabMod, module);

  const char * carbonModuleName = module->getName()->str();
  const char * moduleName =
    mUtil->getArgs()->getStrValue("-systemCModuleName");
  // If user didn't specify module name, use Verilog/VHDL module name
  if (moduleName == NULL)
    moduleName = carbonModuleName;

  UtString ifaceTag;
  {
    const UtString* tmpStr = gCodeGen->getIfaceTag();
    ifaceTag.assign(tmpStr->c_str());
  }

  // sort outputs into two sets: clocks and non-clocks (data).
  PortNetSet clockOutputs;
  PortNetSet dataOutputs;
  sSortOutputs(validOutPorts, mMarkDesign, &clockOutputs, &dataOutputs);
  sSortOutputs(validTriOutPorts, mMarkDesign, &clockOutputs, &dataOutputs);
  sSortOutputs(validBidiPorts, mMarkDesign, &clockOutputs, &dataOutputs);

  // Open SystemC header for model wrapper.
  UtString hFileName;
  hFileName << fileRoot << ".systemc.h";
  UtOFStream hout(hFileName.c_str());

  // Emit header comment
  hout << "// -*- C++ -*-\n" 
       << "/* SystemC wrapper for Carbon model "
       << moduleName << " */\n\n";

  // Emit include guards
  UtString includeGuard;
  includeGuard << "_lib" << ifaceTag << "_systemc_h_";
  hout <<
    "#ifndef " << includeGuard << "\n"
    "#define " << includeGuard << "\n\n";


  // Emit #includes
  // are used together: cust/teradyne/maximus/cwtb/maximus_core
  hout <<
    "#include \"systemc.h\"\n";
  // Disable inclusion of sysIncludes.h.  Needed when -cppAPI and -systemCWrapper
  // are used together: cust/teradyne/maximus/cwtb/maximus_core
  hout <<
    "#define __CARBON_SYS_INCLUDE_H__\n"
    "#include \"" << *(gCodeGen->getTargetName()) << ".h\"\n"
    "#include <cctype>\n\n";  // cctype needed by carbon_valutil.h
  
  // Begin SC_MODULE module.  This is essentially a C++ class.
  hout <<
    "SC_MODULE(" << moduleName << ")\n"
    "{\n"
    "  #include \"carbon/carbon_scmodule.h\"\n"
    "  #include \"carbon/carbon_valutil.h\"\n\n"
    "  // request_update/update object used to call carbon_schedule() once per\n"
    "  // delta cycle.\n"
    "  class CarbonScheduleChannel: public sc_prim_channel\n"
    "  {\n"
    "    CarbonObjectID *mCarbonModelHandle;\n"
    "    public:\n"
    "    sc_event carbon_write_outputs_event;\n";

  // if we are inserting a delta cycle between clock outputs and data outputs
  if (clockOutputs.size() && dataOutputs.size()) {
    hout <<
    "    sc_event carbon_write_delayed_outputs_event;\n";
  }

  hout <<
    "    void setCarbonModel( CarbonObjectID *cm_handle ) { mCarbonModelHandle = cm_handle; }\n"
    "    void requestSchedule() { request_update(); }\n"
    "    virtual void update() { \n"
    "      carbonSchedule( mCarbonModelHandle, sc_time_stamp().value() );\n"
    "      carbon_write_outputs_event.notify( SC_ZERO_TIME );\n"
    "    }\n"
    "  };\n"
    "  void carbon_write_outputs();\n";

  // if we are inserting a delta cycle between clock outputs and data outputs
  if (clockOutputs.size() && dataOutputs.size()) {
    hout <<
    "  void carbon_write_delayed_outputs();\n";
  }

  hout <<
    "  CarbonScheduleChannel carbon_schedule_channel;\n\n";
    
  CGPortIface* portIface = gCodeGen->getPortIface();      

  // compute the set of interface 'ports' that are not primary ports.
  PortNetVec nonPorts;
  sGetNonPorts(portIface, &nonPorts);

  // Emit a member for the port interface.
  hout << "  carbon_" << ifaceTag << "_ports mPorts;\n";

  // Emit ports and handle declarations.  Once in optimal form, and
  // once with everything as a logic value (sc_logic or sc_lv<N>).
  PortNetSet netHandles;
  sDeclareSystemCPorts(hout, portIface, synthTristates, mUtil->getIODB(), nonPorts);
  hout << "\n";
  sDeclarePortHandles(hout, portIface, validInPorts, validOutPorts, validTriOutPorts, validBidiPorts, synthTristates, &netHandles);
    
  // Initialize wave stuff.
  hout << "\n";
  hout << "  /* The next 2 defines control waveform dumping. See carbon_scmodule.h\n";
  hout << "   * for details about carbonSCDumpVars.\n";
  hout << "   * define CARBON_DUMP_FSDB to be 1 if you want to dump fsdb waves.\n";
  hout << "   * define CARBON_DUMP_VCD to be 1 if you want to dump vcd.\n";
  hout << "   * FSDB takes precedence over VCD.\n";
  hout << "   * The name of the file is the instance name + .vcd or .fsdb appropriately\n";
  hout << "   * Note that you can define these on the gcc compile line with -D.\n";
  hout << "   * E.g., -DCARBON_DUMP_FSDB=1\n\n";
  hout << "   * Beware that defining this on the gcc compile line will dump waves\n";
  hout << "   * for all carbon wrapped designs.\n";
  hout << "   * All instances of a design can be dumped by manually changing the wrapper\n";
  hout << "   * to define one of the dump options.\n";
  hout << "   */\n\n";
  
  hout << "#ifndef CARBON_DUMP_FSDB\n";
  hout << "#define CARBON_DUMP_FSDB 0\n";
  hout << "#endif\n";
  hout << "#ifndef CARBON_DUMP_VCD\n";
  hout << "#define CARBON_DUMP_VCD 0\n";
  hout << "#endif\n";

  // initialize sensitivity handling (mainly for wave dumping)
  hout << "\n";
  hout << "  /* This define allows waveforms to be updated on every clock edge.\n";
  hout << "   * But, it can be used generally without waveforms. Note that use of\n";
  hout << "   * this define impacts performance, as normally an irrelevant\n";
  hout << "   * edge of a clock is ignored.\n";
  hout << "   *\n";
  hout << "   * define CARBON_SCHED_ALL_CLKEDGES to be 1 if you want the schedule\n";
  hout << "   * function to be called on every clock edge.\n";
  hout << "   * Note that you can define this on the gcc compile line with -D.\n";
  hout << "   * E.g., -DCARBON_SCHED_ALL_CLKEDGES=1\n";
  hout << "   *\n";
  hout << "   * Be aware that defining this on the gcc compile line will cause all\n";
  hout << "   * wrapped carbon designs to schedule on every clock edge.\n";
  hout << "   * Note also that if either CARBON_DUMP_FSDB or CARBON_DUMP_VCD are\n";
  hout << "   * defined this is automatically set to 1.\n";
  hout << "   */\n\n";
  hout << "#ifndef CARBON_SCHED_ALL_CLKEDGES\n";
  hout << "#define CARBON_SCHED_ALL_CLKEDGES ";

  if (schedAllClkEdges)
    hout << "1\n";
  else
    hout << "0\n";
  hout << "#endif\n\n";

  hout << "#if CARBON_DUMP_VCD || CARBON_DUMP_FSDB\n";
  hout << "#undef CARBON_SCHED_ALL_CLKEDGES\n";
  hout << "#define CARBON_SCHED_ALL_CLKEDGES 1\n";
  hout << "#endif\n";

  // Declare an event, used to invoke the schedule method
  PortNetSet edgeAsyncs;

  // sEmitPortAssign() needs to know if a signal that it's processing
  // represents both an input and an output.  Comparing CGPortIface
  // pointers isn't sufficient -- we need to compare the result of
  // getSymNode().  We make a set of the getSymNode() values so
  // sEmitPortAssign() can tell if the signal it's processing is an
  // output.  This is needed for signals marked both scDepositSignal
  // and scObserveSignal.
  UtHashSet<const STSymbolTableNode*> outPortSet;

  for (PortNetSet::iterator p = validOutPorts.begin();
       p != validOutPorts.end();
       ++p)
    outPortSet.insert((*p)->getSymNode());
  for (PortNetSet::iterator p = validTriOutPorts.begin();
       p != validTriOutPorts.end();
       ++p)
    outPortSet.insert((*p)->getSymNode());
  for (PortNetSet::iterator p = validBidiPorts.begin();
       p != validBidiPorts.end();
       ++p)
    outPortSet.insert((*p)->getSymNode());

  // For each input, define a method to deposit the new value when it changes.
  hout <<
    "\n"
    "  /* Each input port gets a method to copy the value to the \n"
    "     DesignPlayer whenever it changes value. Clocks are treated specially.\n"
    "     Bidis are resolved manually (without the rv type), then a \n"
    "     'resolved' function is called  */\n";
  for (PortNetSet::iterator p = validBidiPorts.begin(); p != validBidiPorts.end(); ++p)
  {
    const CGPortIfaceNet* ifaceNet = *p;
    UtString netName = ifaceNet->getGenerateName();
    hout << "  void " << sPortMethod(ifaceNet) << "();\n";
    hout << "  void " << sPortMethod(ifaceNet) << "_resolved() {\n";
    UtString portHandle = sPortHandle(ifaceNet);
    hout << "    " << netName << ".write(" << portHandle << ");\n";
    hout << "  }\n\n";
  }
  
  for (PortNetSet::iterator p = validInPorts.begin(); p != validInPorts.end(); ++p)
  {
    const CGPortIfaceNet* ifaceNet = *p;
    const NUNet* net = ifaceNet->getNUNet();
    // Check if the scheduler only cares about one edge of a clock or
    // async.
    const SCHEvent* clkEvent = NULL;
    ClkToEvent::iterator q = clkEdgeUses.find(net);
    if (q != clkEdgeUses.end())
      clkEvent = q->second;

    const SCHEvent* checkClkEvent = clkEvent;
    bool isNegEdgeTrigger = false;
    bool isPosEdgeTrigger = false;
    if (clkEvent)
    {
      // The schedule only cares about one edge; however, if the clock
      // is an edge-triggered async then we must have the value
      // change, but we only need to call the schedule on the
      // important edge.
      //
      // It is not legal for the clock to be used as data. In that
      // case we should have put the clock in the bothEdgeClks set.
      if (clksUsedAsData.find(net) != clksUsedAsData.end())
      {
        NU_ASSERT("Clocks used as data should trigger on both edges" == NULL,
                  net);
      }
      else
      {
        // It is an async edge trigger
        IODBNucleus* iodb = mUtil->getIODB();
        const STSymbolTableNode* clkNode = clkEvent->getClock();
        if (iodb->isNegedgeReset(clkNode))
          isNegEdgeTrigger = true;
        else if (iodb->isPosedgeReset(clkNode))
          isPosEdgeTrigger = true;
        
        if (isNegEdgeTrigger || isPosEdgeTrigger)
        {
          checkClkEvent = NULL;
          // store this knowledge. It is needed to decide on
          // sensitivity for the update method on this net.
          edgeAsyncs.insert(ifaceNet);
        }
      }
    }

    SInt32 chgArrIndex = ifaceNet->getChangeIndex();

    // Output two different methods for wave correction of 
    // clocks that would normally be optimized on 1 edge

    bool doingTwoVersions = false;
    if ((chgArrIndex > -1) && clkEvent)
    {
      doingTwoVersions = true;
      hout << "#if CARBON_SCHED_ALL_CLKEDGES\n";
      hout <<
        "\n"
        "  void " << sPortMethod(ifaceNet) << "_all()\n"
        "  {\n";
      
      
      bool callSched = sEmitInputCopy(hout, ifaceNet, portIface, NULL,
                                      synthTristates, outPortSet, 4);
      // This is a clock. It better call schedule.
      INFO_ASSERT(callSched,
                  "Missing call to schedule in generated c wrapper for a clock input");

      hout << "    carbon_schedule_channel.requestSchedule();\n";
      hout << "  }\n\n";
      hout << "#else\n";
    }
    
    hout <<
      "\n"
      "  void " << sPortMethod(ifaceNet) << "()\n"
      "  {\n";


    // checkClkEvent is NULL if the clock is used as data or if it is
    // an edge-triggered async. This has profound effects. See
    // sEmitInputCopy.

    bool callSched = sEmitInputCopy(hout, ifaceNet, portIface, checkClkEvent,
                                    synthTristates, outPortSet, 4);
    if ((chgArrIndex > -1) && 
        (clkEvent || (bothEdgeClks.find(net) != bothEdgeClks.end())))
    {
      int indent = 4;
      hout << sIndent(indent);
      if (isNegEdgeTrigger)
      {
        // only call schedule if the clock/async is 0
        UtString buf;
        hout << "if (*mPorts." << portIface->getExternalValue(ifaceNet, &buf) << " == 0)\n";
        indent += 2;
        hout << sIndent(indent);
      }
      else if (isPosEdgeTrigger)
      {
        // only call schedule if the clock/async is 1
        UtString buf;
        hout << "if (*mPorts." << portIface->getExternalValue(ifaceNet, &buf) << " == 1)\n";
        indent += 2;
        hout << sIndent(indent);
      }
      
      hout << "carbon_schedule_channel.requestSchedule();\n";
    } 
    else if (callSched)
    {
      hout << "    carbon_schedule_channel.requestSchedule();\n";
    }

    hout <<   "  }\n";
    
    if (doingTwoVersions)
      hout <<   "#endif\n";
  }
  
  /*
   * Emit SC_CTOR.  This is essentially the C++ class constructor.
   */

  hout <<
    "  /*\n"
    "   * To instantiate the model without running the initialization\n"
    "   * code (i.e. Verilog initial blocks) you should pass 'false' as the\n"
    "   * value for 'initialize'.  This is important if you are going to\n"
    "   * restore a simulation from a checkpoint file and have initialization\n"
    "   * code (e.g. open file for write) that will affect the restored\n"
    "   * simulation.\n"
    "   */\n"
    "  SC_HAS_PROCESS(" << moduleName << ");\n"
    "  " << moduleName << "(sc_module_name name, bool initialize = true)\n"
    "  : sc_module(name)";

  // initialize all ports with name strings.
  // NC needs these for elaboration (see bug 3134).
  {
    for (CGPortIface::NetIter iter = portIface->loopPrimaryPorts(mUtil->getIODB()); 
         !iter.atEnd(); ++iter) {
      sNameSystemCPort(hout, *iter);
    }

    for (PortNetVec::iterator p = nonPorts.begin(); p != nonPorts.end(); ++p) {
      sNameSystemCPort(hout, *p);
    }
  }


  hout <<
    "\n"
    "  {\n"
    "    carbonModelName = \"" << carbonModuleName << "\";\n"
    "    carbonWaveHandle = NULL; // No wave file yet defined.\n"
    "\n"
    "    // Instantiate the Carbon Model.\n"
    "    carbonModelHandle = carbon_" << ifaceTag << "_create(eCarbonFullDB, eCarbon_NoInit);\n"
    "    if (carbonModelHandle != NULL)\n"
    "    {\n"
    "      carbon_" << ifaceTag << "_portstruct_init(&mPorts, carbonModelHandle);\n"
    "      if (initialize)\n"
    "      {\n"
    "        carbonInitialize(carbonModelHandle, NULL, NULL, NULL);\n"
    "#if CARBON_DUMP_FSDB\n"
    "        carbonSCWaveInitFSDB();\n"
    "#endif\n"
    "#if CARBON_DUMP_FSDB || CARBON_DUMP_VCD\n"
    "        carbonSCDumpVars();\n"
    "#endif\n"
    "      }\n"
    "    }\n\n";

  if (! netHandles.empty())
  {
    hout << "\n    // Initialize CarbonNetID* variables.\n";
    for (PortNetSet::const_iterator a = netHandles.begin(); a != netHandles.end(); ++a)
    {
      const CGPortIfaceNet* ifaceNet = *a;
      hout << "      "  << sPortHandle(ifaceNet, "net") << " = carbonFindNet(carbonModelHandle, \"" << ifaceNet->getFullName() << "\");\n";
      hout << "      assert(" << sPortHandle(ifaceNet, "net") << ");\n"; // this_assert_OK
    }
  }
  hout <<
    "\n"
    "    // Register the output update method.\n"
    "    SC_METHOD( carbon_write_outputs );\n"
    "    sensitive( carbon_schedule_channel.carbon_write_outputs_event );\n"
    "    dont_initialize();\n";

  // if we are inserting a delta cycle between clock outputs and data outputs
  if (clockOutputs.size() && dataOutputs.size()) {
    hout << 
    "\n"
    "    SC_METHOD( carbon_write_delayed_outputs );\n"
    "    sensitive( carbon_schedule_channel.carbon_write_delayed_outputs_event );\n"
    "    dont_initialize();\n";
  }

  if (! validBidiPorts.empty())
  {
    hout << "\n    // Register the update methods for each bidi\n";
    for (PortNetSet::iterator p = validBidiPorts.begin(); p != validBidiPorts.end(); ++p)
    {
      int indent = 4;
      const CGPortIfaceNet* ifaceNet = *p;
      UtString netName = ifaceNet->getGenerateName();
      hout << sIndent(indent) << "SC_METHOD(" << sPortMethod(ifaceNet) << ");\n";
      sGenSensitivity(hout, netName, indent);
      hout << sIndent(indent) << "dont_initialize();\n";
      hout << sIndent(indent) << "SC_METHOD(" << sPortMethod(ifaceNet) << "_resolved);\n";
      hout << sIndent(indent) << "sensitive(m_" << netName << "_event);\n";
      hout << sIndent(indent) << "dont_initialize();\n";
    }
  }

  hout << "\n    // Register the update methods for each input\n";
  for (PortNetSet::iterator p = validInPorts.begin(); p != validInPorts.end(); ++p)
  {
    const CGPortIfaceNet* ifaceNet = *p;
    const NUNet* net = ifaceNet->getNUNet();
    UtString netName = ifaceNet->getGenerateName();
    const SCHEvent* clkEvent = NULL;
    ClkToEvent::iterator q = clkEdgeUses.find(net);
    bool hasClockEvent = false;
    if (q != clkEdgeUses.end())
    {
      clkEvent = q->second;
      hasClockEvent = true;
    }

    // For clocks that are not used as data, we want to minimize the
    // number of update calls. So, only call the update method on
    // clocks during the significant edge. Of course, if both edges
    // are used then there is no win.
    if (clkEvent && ((edgeAsyncs.find(ifaceNet) != edgeAsyncs.end())
                     || (clksUsedAsData.find(net) != clksUsedAsData.end())))
      clkEvent = NULL;
    
    int indent = 4;
    // use hasClockEvent, because this may be a reset, and we want 2
    // different methods for an edge reset, one optimal and one for
    // dumping waves
    if (hasClockEvent)
    {
      hout << "#if CARBON_SCHED_ALL_CLKEDGES\n";
      hout << sIndent(indent) << "SC_METHOD(" << sPortMethod(ifaceNet) << "_all);\n";
      sGenSensitivity(hout, netName, indent);
      hout << "#else\n";
      hout << sIndent(indent) << "SC_METHOD(" << sPortMethod(ifaceNet) << ");\n";
      if (clkEvent)
        sGenEdgeSensitivity(hout, netName, clkEvent, indent, ifaceNet);
      else
        sGenSensitivity(hout, netName, indent);
      hout << "#endif\n";
      hout << sIndent(indent) << "dont_initialize();\n";
    }
    else
    {
      // non-clock input
      hout << sIndent(indent) << "SC_METHOD(" << sPortMethod(ifaceNet) << ");\n";
      sGenSensitivity(hout, netName, indent);
      hout << sIndent(indent) << "dont_initialize();\n";
    }
    hout << "\n";
  }
  hout << "  }\n\n";   // end of SC_CTOR

  // Destructor
  hout <<
    "  // Release the net handles and Carbon Model when the \n"
    "  // module instance is deleted.\n"
    "  ~" << moduleName << "()\n"
    "  {\n"
    "    if (carbonModelHandle != NULL)\n"
    "    {\n";


  hout << 
    "      carbonDestroy(&carbonModelHandle);\n"
    "    }\n"
    "  }\n";

  hout << "\n";
  hout << "};\n";    // end of SC_MODULE

  // close include guard
  hout << "\n#endif  // " << includeGuard << "\n";

  // Now generate the .cpp file that holds the 'carbon_schedule'
  UtString cFileName;
  cFileName << fileRoot << ".systemc.cpp";
  UtOFStream out(cFileName.c_str());  

  // Emit header comment
  out << "/* SystemC wrapper for Carbon model " << moduleName << " */\n\n";

  // Include the SystemC header emitted above.
  // Do note use hFileName here because that is an absolute path (bug 4957)
  out << "#include \"" << *(gCodeGen->getTargetName()) << ".systemc.h\"\n\n";

  // Emit SC_MODULE_EXPORT macros for NC and MTI compilers
  out <<
    "#ifdef MTI_SYSTEMC\n"
    "  SC_MODULE_EXPORT(" << moduleName << ");\n"
    "#endif\n"
    "\n"
    "#ifdef NC_SYSTEMC\n"
    "  NCSC_MODULE_EXPORT(" << moduleName << ");\n"
    "#endif\n"
    "\n";

  sEmitInitializeMethod(out, moduleName, portIface, validOutPorts, validTriOutPorts, validBidiPorts, synthTristates);
  sEmitWriteOutputsMethods(out, moduleName, portIface, clockOutputs, dataOutputs, synthTristates);

  // Create the implementations for bidi updates. These are not in the
  // header file because they can be kind of heavy.
  for (PortNetSet::iterator p = validBidiPorts.begin(); p != validBidiPorts.end(); ++p)
  {
    const CGPortIfaceNet* ifaceNet = *p;    
    UtString netName = ifaceNet->getGenerateName();
    
    UtString sysCBuf = sSystemCBuf(ifaceNet);
    out << "void " << moduleName << "::" << sPortMethod(ifaceNet) << "() {\n";
    
    UtString syscType;
    
    sGetSystemCType(ifaceNet, syscType, synthTristates);
    
    out << "  " << syscType << " " << sysCBuf << ";\n";    
    out << "  " << sysCBuf << " = " << netName << ".read();\n";
    out << "  bool eval = false;\n";
    out << "  if (m_" << netName << "_schedchange) {\n";
    out << "    m_" << netName << "_schedchange = false;\n";    
    out << "    if (" << sPortHandle(ifaceNet) << " != " << sysCBuf << ")\n";
    out << "      eval = true;\n";
    out << "  }\n";
    out << "  else\n";
    out << "    eval = true;\n\n";
    out << "  if (eval) {\n";
    sEmitInputCopy(out, ifaceNet, portIface, NULL, synthTristates, outPortSet,
                   4);
    out << "    carbon_schedule_channel.requestSchedule();\n";
    out << "  }\n";
    out << "}\n";
  }

  sEmitCheckpointMethod(true /* save */, out, moduleName, validInPorts, 
                        validOutPorts, validTriOutPorts, validBidiPorts, 
                        synthTristates);
  sEmitCheckpointMethod(false /* restore */, out, moduleName, validInPorts, 
                        validOutPorts, validTriOutPorts, validBidiPorts,
                        synthTristates);
}
