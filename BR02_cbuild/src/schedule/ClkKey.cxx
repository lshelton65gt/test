// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "ClkKey.h"

// this file is not currently compiled. it is work in progress.
// comments from josh: Q what are the CLkKey.cxx and .h files for
// That was the beginnings of a major overhaul I wanted to do to clock
// analysis, but never started.  The idea would be to have a more general
// purpose structure to use to as a key for clock equivalence lookups,
// rather than BDDs.  This would better accomodate things like multiply
// driven clocks (e.g. from a tristate bus).  However at the time I was
// doing that I was probably not thinking enough about how to deal withi
// clocks that go through vector nets, and I think before an overhaul is
// done we should have a design that does really well with those.


// base class methods

SCHClkKey::SCHClkKey()
{
}

SCHClkKey::~SCHClkKey()
{
}

bool SCHClkKey::operator==(const SCHClkKey& that)
{
  if (this == &that)
    return true;
  if (getType() != that.getType())
    return false;
  return isEqual(that);
}

BDD SCHClkKey::BDD() {
  // would like to explicitly create ctx->invalid() but have no ctx.
  // this is really invalid to call anyway.
  INFO_ASSERT(0, "this is an invalid call");
  BDD bdd;
  return bdd;
}

void SCHClkKey::push_back(SCHClkKey*)
{
  INFO_ASSERT(0, "push_back is an invalid call");
}

// derived classes for eSimple, eDivider, eVector, eMultiDrive, eExpression

class SCHClkKeySimple: public SCHClkKey
{
public:
  SCHClkKeySimple(BDD bdd): mBDD(bdd) {}
  ~SCHClkKeySimple();
  Type getType() {return eSimple;}
  bool isEqual(const SCHClkKey& that) const
  {
    const SCHClkKeySimple* key = dynamic_cast<const SCHClkKeySimple*>(&that);
    INFO_ASSERT(key, "key is null");
    return key->mBDD == mBDD;
  }
  size_t hash() const {return mBDD.hash();}
private:
  BDD mBDD;
};

SCHClkKey* SCHClkKey::createSimple(BDD bdd)
{
  return SCHClkKeySimple(bdd);
}


class SCHClkKeyDivider: public SCHClkKey
{
public:
  SCHClkKeyDivider(SCHClkAnal* ca, FLNodeElab* flow)
  {
  }

  ~SCHClkKeyDivider();

  Type getType() {return eDivider;}
  bool isEqual(const SCHClkKey& that) const
  {
    const SCHClkKeyDivider* key = dynamic_cast<const SCHClkKeyDivider*>(&that);
    INFO_ASSERT(key, "key is null");
    return ((mClock == key->mClock) &&
            (mEdge == key->mEdge) &&
            (mFaninQeq1 == key->mFaninQeq1) &&
            (mFaninQeq0 == key->mFaninQeq0));
  }

  size_t hash() const {
    return (size_t) mClock + (size_t) mEdge + mFaninQeq1.hash()
      + mFaninQeq0.hash();
  }

  //! Because gdb cannot break in constructors, initialization is done here
  /*! This does a speculative BDD evaluation for a clock divider.  It
   *  gives two answer:  What would the next value for this clock-divider
   *  be if the current value is 0, and what would the next value for
   *  the clock-divider be if the current value is 1.  Those values can
   *  be a bdd function of another input into the divider logic (e.g. reset).
   */
  void init(SCHClkAnal* ca, FLNodeElab* flow)
  {
    // Find BDDs for the fanin of all the nets in the expression,
    // assuming a bdd for the output
    NUNetElab* clk = f->getDefNet();
    mDividedClock = clk;

    UtDEBUG(&sClkDivDebug, "Divider", clk->getSymNode(),
            &f->getUseDefNode()->getLoc(), "CLKDIV_SPEW") 

    // Mark a clock as being calculated by writing a zero bdd into it
    NU_ASSERT((not mBDDContext->isAssigned(clk)),clk );
    mBDDContext->assign(mBDDContext->val0(), clk);

    NUAlwaysBlock* a = dynamic_cast<NUAlwaysBlock*>(f->getUseDefNode());
    STBranchNode* scope = f->getHier();
    if (a != NULL)
    {
      NU_ASSERT(a->isSequential(), a);
      NUEdgeExpr* e = a->getEdgeExpr();
      NU_ASSERT(e, a);
      mEdge = e->getEdge();
      mClock = ca->findClock(e->getNet(), scope, NULL);
      if (mClock == NULL)
      {
        // We found a clock cycle.  Some of these can be handled by
        // scheduling the derived clock code in both the derived clock
        // schedule and the data schedule, to detect glitches at runtime.
        NU_ASSERT((mBDDContext->bdd(clk) == mBDDContext->val0()),clk);
        mClock = NULL;
      }
    }
    else
    {
      mEdge = eClockPosedge;
      mClock = NULL;
    }
    mValid = false;
    if ((a == NULL) || (mClock != NULL)) // initial blocks OK
    {
      FLNodeElab* exprFlow;
      const NUExpr* expr;

      // We ignore the return value here because we expect a circular
      // reference in the clock-divider logic to the clock that is
      // being defined.
      NUNetElabSet nets, cleanNets;
      ca->putResynthMode(ExprResynth::eStopAtState);
      bool allAssigned = ca->getProceduralAssignment(f, &exprFlow, &expr, &nets);

      if (expr != NULL)
      {
        // Ensure that all the nets have BDDs.  Temporarily assign BDDs to
        // any that were not assigned
        for (NUNetElabSet::SortedLoop p = nets.loopSorted(); !p.atEnd(); ++p)
        {
          NUNetElab* netElab = *p;
          if (allAssigned)
          {
            NU_ASSERT((mBDDContext->isAssigned(netElab)), netElab);
          }
          else if (!mBDDContext->isAssigned(netElab))
          {
            mBDDContext->bdd(netElab);
            cleanNets.insert(netElab);
          }
        }

        if (allAssigned)
        {
          // This is not really a clock divider -- this is just a clock derived
          // from state.  So we don't need to experiment with setting clk to 1
          // and to 0, and we don't need to turn off expression caching.
          mFaninQeq0 = mBDDContext->bdd(expr, scope);
          mFaninQeq1 = mFaninQeq0;
        }

        // We don't want to remember the BDDs we compute for the clock-divider
        // expressions, because they are based on assuming the clock is
        // a constant to compute the BDD for the cycle.
        else
        {
          // Assume clk (aka Q), is 0, build the fanin expression.
          mBDDContext->assign(mBDDContext->val0(), clk);
          if (expr->hasBdd(mBDDContext, scope))
          {
            mFaninQeq0 = mBDDContext->bdd(expr, scope);

            // Now assume (aka Q), is 1, and build the fanin expression
            mBDDContext->assign(mBDDContext->val1(), clk);
            mFaninQeq1 = mBDDContext->bdd(expr, scope);
          }
          else
          {
            mFaninQeq0 = mBDDContext->invalid();
            mFaninQeq1 = mBDDContext->invalid();
          }

          // Now we can use these BDDs to do similarity-checking on two
          // clock-dividers.  But first, de-assign any temporary BDDs
          // we had to create to deal with cyclic clock dividers.
          for (NUNetElabSet::iterator p = cleanNets.begin();
               p != cleanNets.end(); ++p)
          {
            NUNetElab* netElab = *p;
            mBDDContext->deassign(netElab);
          }
        } // else
      } // else
    } // else

    // Note that mFaninQeq0/1 get initialized to invalid by the constructor
    mValid = mFaninQeq0.isValid() && mFaninQeq1.isValid();
    mBDDContext->deassign(clk);
  } // void init

private:
  NUNetElab* mClock;
  NUNetElab* mDividedClock;
  BDDContext *mBDDContext;
  ClockEdge mEdge;
  BDD mFaninQeq1;               // D expression when Q == 1
  BDD mFaninQeq0;               // D expression when Q == 0
  bool mValid; 
};

SCHClkKey* SCHClkKey::createDivider(SCHClkAnal* ca, FLNodeElab* flow);
{
  return new SCHClkKeyDivider(ca, flow);
}

typedef UtVector<SCHClkKey*> KeyVector; // used by eMultiDriv & eVector

class SCHClkKeyVector: public SCHClkKey
{
public:
  SCHClkKeyVector(size_t size): mKeys(size) {
  }
  ~SCHClkKeyVector()
  {
    for (size_t i = 0; i < mKeys.size(); ++i)
    {
      SCHClkKey* key = mKeys[i];
      if (key != NULL)
        delete key;
    }
  }
  virtual Type getType() {return eVector;}
  bool isEqual(const SCHClkKey& that) const
  {
    const SCHClkKeyVector* key = dynamic_cast<const SCHClkKeyVector*>(&that);
    INFO_ASSERT(key, "key is null");
    if (mKeys.size() != key->size())
      return false;
    for (size_t i = 0; i < mKeys.size(); ++i)
      if (mKeys[i] != key->mKeys[i])
        return false;
    return false;
  }
  size_t hash() const
  {
    return mBDD.hash();
  }
  void push_back(SCHClkKey* key) {mKeys.push_back(key);}
private:
  KeyVector mKeys;
};

SCHClkKey* SCHClkKey::createVector(BDD bdd)
{
  return SCHClkKeyVector(bdd);
}



class SCHClkKeyMultiDrive: public SCHClkVector
{
public:
  SCHClkKeyMultiDrive(BDD bdd): mBDD(bdd) {}
  ~SCHClkKeyMultiDrive();
  Type getType() {return eMultiDrive;}
private:
  BDD mBDD;
};

SCHClkKey* SCHClkKey::createMultiDrive(BDD bdd)
{
  return SCHClkKeyMultiDrive(bdd);
}


class SCHClkKeyExpr: public SCHClkKey
{
public:
  SCHClkKeyExpr(BDD bdd): mBDD(bdd) {}
  ~SCHClkKeyExpr();
  Type getType() {return eExpr;}
  bool isEqual(const SCHClkKey& that) const
  {
    const SCHClkKeyExpr* key = dynamic_cast<const SCHClkKeyExpr*>(&that);
    INFO_ASSERT(key, "key is null");
    return key->mBDD == mBDD;
  }
  size_t hash() const {return mBDD.hash();}
private:
  BDD mBDD;
};

SCHClkKey* SCHClkKey::createExpr(BDD bdd)
{
  return SCHClkKeyExpr(bdd);
}

