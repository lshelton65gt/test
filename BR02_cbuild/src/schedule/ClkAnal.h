// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CLKANAL_H_
#define __CLKANAL_H_

#include "util/UtHashSet.h"
#include "util/UtHashMap.h"
#include "bdd/BDD.h"
#include "exprsynth/ExprResynth.h"
#include "reduce/Fold.h"
#include "util/LoopFunctor.h"
#include "MarkDesign.h"

class IODB;
class NUNetElab;
class NUNet;
class FLNodeElab;
class SCHClock;
class NUExprFactory;
class NUCostContext;

typedef UtHashSet<STAliasedLeafNode*> STALNSet;
typedef UtHashSet<SCHClock*> ClockSet;

class SCHClock
{
public:

  SCHClock(NUNetElab* master, bool isReset, bool isEdge,
           bool isInput, IODB*, ClockSet * clockSet, bool forceMaster,
           BDDContext* bddContext);
  ~SCHClock();

  //! add alias to this SCHClock without any checking, mSpeed is updated
  //! if the speed of alias is faster than current clocks in SCHClock
  /*!
   *! This should be accessed from SCHClkAnal::addAlias
   */
  void addAlias(NUNetElab* alias);

  //! remove alias from this SCHClock, mSpeed is not updated, so it may be too fast upon exit
  void removeAlias(NUNetElab* alias);

  typedef Loop<STALNSet> AliasNodeLoop;
  AliasNodeLoop loopNodeAliases() {return AliasNodeLoop(mAliases);}
  typedef CLoop<STALNSet> AliasNodeCLoop;
  AliasNodeCLoop loopNodeAliases() const {return AliasNodeCLoop(mAliases);}
  struct NodeToNet
  {
    typedef NUNetElab* value_type;
    typedef NUNetElab*& reference;

    NUNetElab* operator()(const STAliasedLeafNode* node) const {
      return NUNetElab::find(node);
    }
  };
  typedef LoopFunctor<AliasNodeCLoop, NodeToNet> AliasLoop;
  AliasLoop loopAliases() const {return AliasLoop(mAliases);}

  //! change the master to the specified net
  /*!
   *! Note, this does not add the old master as an alias.
   *!
   *! This should be accessed from SCHClkAnal::putMaster
   */
  void putMaster(NUNetElab* master, bool forceMaster);
  bool isReset() const {return mIsReset;}
  bool isEdge() const {return mIsEdgeCondition;}
  bool isInput() const {return mIsClkTreeInput;}
  bool isSlowClock() const {return mIsSlowClock;}
  bool isFastReset() const {return mIsFastReset;}
  void putIsReset();
  void putIsEdge();
  void putIsInput();
  NUNetElab* getMaster() const {return mMaster;}
  SInt32 numAliases() {return mAliases.size();}

  //! Does this clock group contain aliases?
  bool isAliased() const;

  //! Does this clock group contain aliases or equivalences?
  bool isAliasedOrEquivalenced() const;

  void clearBdds(BDDContext*);

  enum PrintFlags {eOneLine, eAllAliases, eVerbose};

  void print(FILE*, PrintFlags, BDD* bdd);
  UInt32 getSpeed() {return mSpeed;}

  //! Set the clock speed to the given speed.
  /*!
   * Note, the given speed is ignored if the clock
   * speed was specified in the IODB (the speed will
   * not be changed).
   */
  void putSpeed(UInt32 speed);

  bool isForcedMaster() {return mForcedMaster;}

  //! propagate the BDD to all the aliases, return previous BDD
  BDD putBDDOnAliases(const BDD& bdd);

  BDD getBDD() const {return mBDD;}

private:
  BDD mBDD;
  NUNetElab* mMaster;
  STALNSet mAliases;
  bool mIsClkTreeInput;
  bool mIsReset;
  bool mIsEdgeCondition;
  bool mForcedMaster;
  bool mSpeedFromIODB;
  bool mIsSlowClock;
  bool mIsFastReset;
  UInt32 mSpeed;
  IODB* mIODB;
  //SCHClock* mInversion;
  BDDContext* mBDDContext;

private:
  SCHClock();                   // forbid
  SCHClock(const SCHClock&);    // forbid
  SCHClock& operator=(const SCHClock&);    // forbid
};

class SCHClkAnal;
class MsgContext;
class STBranchNode;
class CopyContext;

//! SCHDivider class helps compare clock-dividers for similar functionality
/*! Data structure for each unique clock-divider.  Two clock dividers
 * are the same if, when you try to calculate the BDD's with Q held
 * at constant 1, are the same, and also the same with Q held at
 * at constant 0.  Also the base clock for the clock-dividers must
 * be the same. We use a hash function which captures all of 
 * this in the hash & compare functions.
 *
 * Note that this class is oriented toward clock dividers where there
 * is a feedback loop through a sequential block.  It also is used to represent
 * any state element in a clock tree.
 *
 * Also note that a derived clock is an ordered sequence of clock dividers.
 * This is needed to represent a resettable flop in the clock tree.  That
 * is encapsulated in SCHDerivedClock
 */
class SCHDivider
{
public:
  //! Constructor
  SCHDivider(SCHClkAnal* ca, FLNodeElab* flow);

  //! Destructor
  ~SCHDivider() {}

  //! Because gdb cannot break in constructors, initialization is done here
  /*! This does a speculative BDD evaluation for a clock divider.  It
   *  gives two answer:  What would the next value for this clock-divider
   *  be if the current value is 0, and what would the next value for
   *  the clock-divider be if the current value is 1.  Those values can
   *  be a bdd function of another input into the divider logic (e.g. reset).
   */
  void init(SCHClkAnal* ca, FLNodeElab* flow);

  //! Compare two clock dividers to see if they are functionally equivalent
  bool operator==(const SCHDivider& d1)
    const
  {
    return ((mClockBDD == d1.mClockBDD) &&
            (mEdge == d1.mEdge) &&
            (mFaninQeq1 == d1.mFaninQeq1) &&
            (mFaninQeq0 == d1.mFaninQeq0));
  }

  //! Compute a hash-value for this clock-divider, taking account only those fields included in operator==.
  size_t hash() const {
    return (mClockBDD.hash() + (size_t) mEdge +
            mFaninQeq1.hash() + mFaninQeq0.hash());
  }

  //! Get the final BDD assigned to the clock-divider
  BDD getBdd() const;

  //! Was this clock-divider successfully analyzed as such?
  bool isValid() const {return mValid;}

  //! get the speed of the clock divider (does not actually divide)
  UInt32 getSpeed() {return mSpeed;}

private:
  BDD mClockBDD;
  NUNetElab* mDividedClock;
  BDDContext *mBDDContext;
  ClockEdge mEdge;
  BDD mFaninQeq1;               // D expression when Q == 1
  BDD mFaninQeq0;               // D expression when Q == 0
  bool mValid; 
  //bdd mBdd;
  UInt32 mSpeed;
}; // class SCHDivider

class SCHDerivedClock
{
public:
  SCHDerivedClock(UInt32 num_initial_drivers, UInt32 num_funky_blocks) :
    mValid(true),
    mNumInitialDrivers(num_initial_drivers),
    mNumFunkyBlocks(num_funky_blocks)
  {}

  //! div should already be in canonical form when it gets pushed
  void push_back(SCHDivider* div)
  {
    mDividers.push_back(div);
    if (div != NULL) {
      mValid &= div->isValid();
    }
  }

  bool operator==(const SCHDerivedClock& other)
    const
  {
    return mValid && other.mValid &&
      (mNumInitialDrivers == other.mNumInitialDrivers) &&
      (mNumFunkyBlocks == other.mNumFunkyBlocks) &&
      (mDividers == other.mDividers);
  }

  size_t hash() const
  {
    size_t h = mNumFunkyBlocks + mNumInitialDrivers;
    for (UInt32 i = 0; i < mDividers.size(); ++i)
      h = 10*h + (size_t) mDividers[i]; //  canonical already so need to hash
    return h;
  }
      
  //! Get the final BDD assigned to the clock-divider
  BDD getBdd() {return mBDD;}

  //! Assign a bdd
  void putBdd(BDD bdd) {mBDD = bdd;}

private:
  bool mValid;
  UInt32 mNumInitialDrivers;
  UInt32 mNumFunkyBlocks;
  UtVector<SCHDivider*> mDividers;
  BDD mBDD;
}; // class SCHDerivedClock

//! SCHClkAnal class
class SCHClkAnal
{
private:
  typedef UtMap<BDD, SCHClock*> ClockMap;
  typedef UtHashMap<NUNetElab*, SCHClock*> CollapseMap;

  enum BitselType {
    eBitselDynamic,    //!< bitselect with dynamic index
    eBitselConstIndex, //!< bitselect with a constant index, 
    eBitselOutOfRange, //!< bitselect is outside the range of vector
    eBitselNet         //!< bitselect that also happens to be the whole net
  };

public:
  //! constructor
  SCHClkAnal(SCHUtil*, SCHMarkDesign*);

  //! destructor
  ~SCHClkAnal();

  //! Find the canonical clock net for (\a clockNet \a scope), and optionally its inversion
  /*! Given a local clock-net and a scope, find the canonical elaborated
   *  clock-net.  If there are several different clock-nets that are logically
   *  the same, then SCHClkAnal will pick one of the nets as the canonical
   *  net for that functional clock, and return that.  SCHClkAnal builds up
   *  its knowledge of the design clocks incrementally, so it may change its
   *  mind about which NUNetElab* is the canonical clock as it learns about
   *  others during design traversal.  For this reason, the scheduler's first
   *  pass explores all the design clocks using this method, without recording
   *  the results in its own data structures.  In later passes, the scheduler
   *  is then guaranteed to get consistent canonical clocks.
   *
   *  If pClkInv is non-null, and we find that this clock-net has an
   *  inversion, and that inversion is really the master, then set
   *  *pClkInv so that the calling code knows to schedule off the
   *  opposite edge of that clock instead
   *
   * \retval NUNetElab* the canonical clock net.
   * \retval NULL if clock cannot be found (caused by cycle of clock dividers)
   */
  NUNetElab* findClock(NUNet* clockNet, STBranchNode* scope,
                       NUNetElab** clkInv = NULL,
                       bool isClock = true,
		       bool isReset = false,
                       FLNodeElab* dstFlow = NULL);

  //! Find a clock master from an alias clock
  /*! If the input clock is an existing clock, then it returns the
   *  master for it. This may be the same net if the input net is the
   *  master. It returns NULL if this is not an alias clock.
   *
   *  This routine should not create new clocks in the maps
   */
  NUNetElab* findMaster(NUNetElab* alias) const;

  //! Typedef for an iterator to the list of clocks, including clock inversions
  typedef LoopMap<ClockMap> ClockLoop;

  //! Loop function to traverse the list of clocks in a design
  ClockLoop loopClocks();

  //! Get the canonical clock net associated with this bdd
  NUNetElab* getClock(const BDD& b);
  
  //! Check if a clock is really a constant
  /*! Often, test and scan clocks can be tied to a constant since it
   *  is not needed for system validation. The result is we should not
   *  execute any logic based off of this clock. The optimal thing to
   *  do is to remove this clock from any schedule or schedule mask.
   *
   *  To do this, we need a test to see if a clock is a constant.
   *
   *  This routine uses the BDD for the clock to determine if it is a
   *  constant.
   *
   *  If a value pointer is provided, the constant value is returned.
   */
  bool isConstantClock(NUNetElab* clkElab, int* value = NULL) const;

  //! Recursively traverse fanin of flow-node, constructing bdds & marking clocks as we go, stopping at state & primary inputs
  bool traverseFanin(FLNodeElab*, bool isClock, NUNetElab* clkElab, BDD*,
                     UInt32*);

  BDDContext* getBDDContext() {return mBDDContext;}

  //! Clear any expression caches (use if nets may have been deleted)
  void clearExpressionCaches();

  //! Compare two clocks to see which is the prefered master.
  /*! We take
   *! into account temp-ness, and logic depth as primary factors.  If
   *! those are equivalent, then we use lexical order to get consistent
   *! behavior, but only if compareNames is true.
   */
  int compareClocks(NUNetElab* c1, NUNetElab* c2, bool compareNames);

  bool isTempClock(NUNetElab*);

  //! register with the clock analysis code that two clocks were specified as equivalent (based on info from directives file)
  void collapseClocks(NUNetElab* master, NUNetElab* alias);

  //! Fix all the stale BDDs in the design that result from collapsing clocks
  void fixCollapsedBdds(STSymbolTable*);

  //! tell the clock analysis code that a clock alias is being killed
  void kill(NUNetElab*);

  //! is a clock aliased to another?
  bool isDead(NUNetElab*) const;

  //! dump all the clock aliaeses to cout
  void dumpAliases(const char* destDir, bool verbose);

  //! Generate a clock tree as a verilog file
  void genClockTree(const char* destDir);

  //! Alias all the clocks (loses some visibility)
  void aliasClocks(NUDesign*, SCHMarkDesign* markDesign);

  //! is this net a member of the clock path?
  bool isClockPath(NUNetElab*) const;

  //! Given an external flow FLNodeElab (\a flow), return the RHS of an internal assignment (in \a expr)
  /*! If the driver is too complex, then puts NULL in expr.  
   *  Returns false if it could not precompute BDDs for all of the flow's fanin.
   *   This would typically happen if we have cyclic clock dividers.
   *  \a exprFlow is set to follow the flow for the value in \a expr.
   *  if something is returned in expr then \a nets is the set of nets
   *   in the fanin of that expression, and \a speed is the speed of
   *   the clock.
   */
  bool getProceduralAssignment(FLNodeElab* flow,
                               FLNodeElab** exprFlow,
                               const NUExpr** expr,
                               NUNetElabSet* nets,
                               UInt32* speed,
                               bool goThruState);
  
  //! mark discovery as complete - do no further equivalence checking 
  void putDiscoveryComplete(bool flag);

  //! Mark all driven aliased as protected observable.
  void keepMastersAlive();

  //! walk all design flows connected to primary outputs, doing clock analysis processing
  bool walkConnectedFlowsForClkAnal();

  //! is this net in the clock tree?
  bool isClockInput(NUNetElab* net);

  //! Get the stored mark design class pointer (this function should never have been created!)
  SCHMarkDesign* getMarkDesign() const { return mMarkDesign; }

  void putResynthMode(ExprResynth::Mode);

  //! Tweak collapseClock directives so the causally earliest is the master
  void causallySortCollapseClocks();

  NUExpr* elabBitsel(const NUExpr* sel, FLNodeElab** exprFlow);
  const NUExpr* elabBitFlow(const NUVarselRvalue* bitsel, FLNodeElab** exprFlow);
  NUExpr* kbitSelect(const NUExpr* ident, const NUExpr* index,
                     SInt32 bit, UInt32 size);

  IODBNucleus* getIODB();

  //! Did clock analysis encounter a fatal error?
  bool abortScheduler() const {return mAbortScheduler;}

  //! Helper enum for private method walkBitExpr, needed by internal
  //! class HackExpr in ClkAnalVector.cxx
  enum  ClkAnalPhase {eClkOneBitAtATime, eClkConcatAssign};

  //! Process collapseClock directives (should be called before findClock())
  void processCollapseClockDirectives();

  //! Set the flag to print warnings
  /*! During some passes, clock analysis may be called on possible
   *  clocks instead of real clocks. This boolean gives the user the
   *  ability to disable the warnings.
   *
   *  The routine returns the previous value so that the caller can
   *  reset it.
   */
  bool putPrintWarnings(bool flag)
  {
    bool ret = mPrintWarnings;
    mPrintWarnings = flag;
    return ret;
  }

  //! Add the net as an alias to the clock, erasing old bdd from mClocks
  void addAlias(SCHClock*, NUNetElab*);

  //! Change the clock's master to the specified net, and add the old
  //! master as an alias
  void putMaster(SCHClock*, NUNetElab*, bool force);

  typedef UtHashMap<NUAlwaysBlock*,UInt32> BlockIntMap;
  
private:
  //! analyze net and index to determine if index selects a bit within
  //! the net and if so is is a special case.  If return value is
  //! eBitselConstIndex then constIndex will contain that 0-based offset
  BitselType analyzeBitsel(NUNet* net,
                           const NUExpr* index,
                           const ConstantRange *subrange,
                           SInt32* constIndex);

  /*! \brief loop over the \a nets of a use set of an expression,
   *   making sure each has been processed by the clock analysis code.
   *
   *  \retval false if any of the netElabs could not be handled as a clock,
   *
   *  \retval speed: the maximum clock speed among the nets that
   *   were clocks
   */
  bool traverseUses(const NUNetElabSet& nets, FLNodeElab* flow, UInt32* speed);

  /*! \brief given a \a flow for an assignment, attempt to generate and return a bdd 
   *  (in clkBdd) for the assignment.
   *
   *  \return true if assignment could be determined.
   *  *isGated is set to true if a valid bdd was defined
   */
  bool traverseBlock(FLNodeElab* flow, bool* isGated, int* numClkFanins, BDD* clkBdd,
                     UInt32* speed);
  bool isDerivedClock(NUNetElab*);
  int isClkTreeInput(NUNetElab* pi);

  /*! \brief examine drivers for \a clkElab looking for async reset signal
   *
   * \retval false if no async reset found,
   * \retval true if async reset found and load \a priorityQueue with
   * flownode drivers
   *
   * \var priorityQueue If an async reset is found then this vector
   *      will be filled with the FLNodeElabs from initial blocks 
   *      that drive \a clkElab followed by the priority ordering 
   *      of the always blocks that drive it.
   * \var malformedAsyncReset will normally be false, but will be set
   *      to true if multiple clock drivers are detected for any of
   *      the always blocks in the set of drivers for clkElab.
   * \var num_initial_drivers how many initial drivers are in the priority chain?
   * \var num_funky_blocks how many funky reset blocks (generated from async reset/presets)
   */ 
  bool getPriorityOrder(UtVector<FLNodeElab*>* priorityQueue,
                        NUNetElab* clkElab, FLNodeElab* dstFlow,
                        bool* malformedAsyncReset,
                        UInt32* num_initial_drivers,
                        UInt32* num_funky_blocks);
  UInt32 findFunkyBlocks(BlockIntMap* prio_map,
                         NUAlwaysBlock* clock_block);
  void putCanonical(NUNetElab* clk);
  SInt32 findDepth(FLNodeElab* flow);
  SInt32 findDepth(NUNetElab* net);
  SCHDivider* consDivider(FLNodeElab*);
  bool getElabBitSource(FLNodeElab* driver, NUNetElab* netElab,
                        UInt32 offset, FLNodeElab**, const NUExpr**);
  bool findElabBitSource(NUNetElab* netElab,
                         UInt32 offset, FLNodeElab*, FLNodeElab**,
                         const NUExpr**);
  SCHClock* recordClock(NUNetElab* net,
                        BDD* clkBdd, // read/write arg
                        bool forceCanonical, bool isClock,
                        bool isReset, UInt32 speed = 0,
                        bool preAssigned = false);
  void putClock(SCHClock*);
  void eraseClock(NUNetElab* net);

  void eraseClock(ClockMap::iterator p);
  void insertClock(BDD bdd, SCHClock* clock);

  /*!
    \brief tell the clock analysis code that two clocks are to be merged,
      based on user directives.  But only if the master of the src
      clock has been processed. Returns the new max speed of all the clocks
  */
  UInt32 mergeClocks(SCHClock* dst, SCHClock* src, UInt32 speed, bool isClock);

  //! Walk over an expression tree, simplifying and substituting bit-selects,
  //! and replacing local idents with elaborated ones based on the flow's
  //! hierarchy
  bool walkBitExpr(FLNodeElab* flow, const NUExpr** expr, ClkAnalPhase phase);

  //! Is this expression small enough to call Fold on without blowing up?
  bool isLowCost(const NUExpr* expr);


  ClockMap mClocks;             // map from bdd to SCHClock (some SCHClock(s) may not be included in this map,
                                // due to aliasing, or dead clocks, but they will be in mClockSet) 

  CollapseMap mCollapseMap;     // map of NUNetElab to SCHClock for user specified collapse clocks
  ClockSet mClockSet;           // set of all existing SCHClock-s
  BDDContext* mBDDContext;
  NUNetElab* mCanonicalClk;
  UtHashSet<BDD, HashValue<BDD> >* mDerivedClocks;
  const NUExpr* elaborateExpr(FLNodeElab* flow,
                              const NUExpr* expr, FLNodeElab**,
                              bool goThruState);
  bool elaborateAssign(FLNodeElab* driver, NUNetElab* net,
                       UInt32 offset, FLNodeElab** ret,
                       const NUExpr** newExpr);
  const NUExpr* dynamicBitSelect(FLNodeElab* flow,
                                 NUNetElab* net, const NUExpr* sel,
                                 FLNodeElab** bitFlow);

  const NUExpr* elaborateIdents(const NUExpr* expr, STBranchNode* hier);

  // Used to print clock dependency trees
  typedef UtMap<NUNetElab*, int> ClkTreeNets;
  void printClockDependencies(NUNetElab*, UtOFileStream&);
  void printClockDependenciesHelper(FLNodeElab*, UtOFileStream&, int,
				    FLNodeElabSet&);

  // We keep a set of all the sequential blocks that are clock-dividers.
  // The set-comparison function weeds out ones that have equivalent inputs,
  // so that we can merge their clock schedules.  e.g. if we have:
  //   always @(posedge clk) clkdiv1 = reset | ~div1;
  //   always @(posedge clk) clkdiv2 = reset | ~div2;
  // Then we can treat logic clocked on clkdiv1 and clkdiv2 as belonging
  // to the same schedule.  This will help reduce the number of schedules.
  //typedef hash_set<SCHDivider, SCHDividerHash, SCHDividerCmp> ClkDividerSet;
  typedef UtHashSet<SCHDivider*, HashPointerValue<SCHDivider*> > ClkDividerSet;
  ClkDividerSet* mDividers;   // Sequential nodes driving derived clocks
  typedef UtHashSet<SCHDerivedClock*,
                  HashPointerValue<SCHDerivedClock*> > DerivedClkSet;
  DerivedClkSet* mDerivedClks;   // Sequential nodes driving derived clocks

  NUNetElabSet* mCollapsedClocks; // nets that are referenced in collapseClock directives
  NUNetElabSet* mDeadClocks;
  NUNetElabSet* mTempClocks;    // nets that are redefined with non-clock
                                // values -- should not become masters

  NUNetElabSet* mComputingDivider; // clock dividers being computed
  SCHUtil* mUtil;
  SCHMarkDesign* mMarkDesign;

  // We have inconsistent naming conventions inside clock analysis right
  // now.  Hopefully this clarifies it.  We *always* do clock equivalence
  // checks.  We also allow the user to tell us about clock equivalances
  // that we haven't figured out programaticcally, using the collapseClocks
  // directives.  All this helps us merge schedules.
  //
  // We also have the "-aliasClocks" switch, which causes the compiler to
  // merge the net and flow data structures for all clocks deemed to be
  // equivalent, either due to equivalence testing or collapseClocks
  // directives.
  //
  // However, we have the -dumpClockAliases switch, which dumps all the
  // clocks proved equivalent, indepdent of whether we used -alias
  // or not.  But if we *do* use -aliasClocks, then we lose the NUNetElab*
  // structures for the aliases (because they get deleted), and so we cannot
  // print them in -dumpClockAliases.  So this map helps us print those
  // too
  typedef UtHashMap<NUNetElab*,SCHClock*> NodeMap;
  typedef UtHashMap<FLNodeElab*, SInt32> DepthMap;
  DepthMap mDepthMap;

  //! Populate clock and inversion maps for each clock.
  /*!
   * The mClocks structure is traversed. All edge SCHClocks are added
   * to the following maps:
   *
   * \param clks Map from NUNetElab to master SCHClock.
   * \param invs Map from NUNetElab to master inversion clock.
   */
  void populateClkAndInvMaps(NodeMap * clks, NodeMap * invs);

  // if true then the discovery phase of clock analysis is complete
  // (and no new clock equivalences will be found.)
  // \sa putDiscoveryComplete
  bool mDiscoveryComplete;

  //! If true, the collpase clocks have been processed.
  bool mCollapsedClocksProcessed;

  //! If true, warnings about encountered clocks should be printed
  /*! During some passes, clock analysis may be called on possible
   *  clocks instead of real clocks. This boolean gives the user the
   *  ability to disable the warnings.
   */
  bool mPrintWarnings;

  // expression synthesis to help us walk through vector bits
  // that have been temped.
  ExprResynth* mExprResynth;
  Fold* mFold;
  NUExprFactory* mExprFactory;

  CopyContext* mCopyContext;
  bool mStraightExprSynth;

  bool mAbortScheduler;

  // The number of dividers we have gone through in recursing through
  // clock logic. We limit ourselves so we don't use excessive stack
  // space and use too much CPU/memory in the analysis.
  //
  // This number is incremented, decremented and tested in consDivider
  int mNumDividersOnStack;
  int mMaxDividersOnStack;

  NUCostContext* mCostContext;

  // These variables keep track of the nesting depth of findClock, and
  // limit it to a user controllable value. See
  // comments in ClkAnal.cxx for more details (search for bug 16229).
  SInt32 mFindClockNestingLimit;
  SInt32 mFindClockNestingDepth;
  bool mFindClockNestingLimitHit;
  
  //! Memo-ize expression elaborations to avoid n^2 behavior on large designs
  typedef std::pair<const NUExpr*, FLNodeElab*> ExprFlow;

  //! helper class for UtHashMap so that we can map between expression-flow
  //! combinations to short-circuit elaborateExpr on identical inputs
  struct HashExprFlow {
    size_t hash(const ExprFlow& eh) {
      // multiply expr ptr by 17 because it's a random prime that's
      // worked well for me in the past.
      return (((size_t) eh.first) * 17) + (((size_t) eh.second));
    }
    bool equal(const ExprFlow& eh1, const ExprFlow& eh2) {
      return eh1 == eh2;
    }
    bool lessThan(const ExprFlow&, const ExprFlow&) {
      // We will not be sorting these so we don't need this one
      INFO_ASSERT(0, "did not expect to sort ExprFlow");
      return false;
    }
    bool lessThan1(const ExprFlow&, const ExprFlow&) {
      // We will not be using Microsoft hash tables we don't need this one
      INFO_ASSERT(0, "did not expect to use MS hash tables");
      return false;
    }
  };
  typedef UtHashMap<ExprFlow, ExprFlow, HashExprFlow> ExprFlowMap;
  ExprFlowMap* mExprFlowMap;
}; // class SCHClkAnal




#endif // __CLKANAL_H_
