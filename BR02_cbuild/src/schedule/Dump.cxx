// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "flow/FLNodeElabCycle.h"
#include "nucleus/NUScope.h"
#include "schedule/Schedule.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Signature.h"
#include "iodb/ScheduleFactory.h"
#include "symtab/STAliasedLeafNode.h"
#include "Util.h"
#include "UseDefHierPair.h"
#include "MarkDesign.h"
#include "Dump.h"
#include "util/UtIOStream.h"
#include <iomanip>
#include "util/StringAtom.h"
#include "util/UtIOStream.h"

/*!
  \file

  This file contains routines to dump information about the
  schedule. This includes dumping the schedule itself as well as
  statistics and other scheduling/performance related information.
*/

void
SCHDump::dumpMultiScheduleBlocks(FLNodeElab* flow, CombSchedules* schedules)
{
  // Create the prefix UtString for this flow node
  UtString flowName;
  composeFlowName(flow, &flowName);

  // Go through the list of schedules it is in and dump the info.
  CombSchedules::iterator p;
  for (p = schedules->begin(); p != schedules->end(); ++p)
  {
    SCHCombinational* comb = *p;

    // Print the flow information
    UtIO::cout() << flowName;

    // Print the schedule
    UtIO::cout() << " is in sched: ";
    UtString buf;
    mSched->scheduleName(comb->getScheduleType(), &buf);
    UtIO::cout() << buf.c_str();

    // And the schedule mask
    buf.clear();
    comb->getMask()->compose(&buf, NULL);
    UtIO::cout() << " (" << buf << ")";

    // And the other mask (transition or sample)
    const SCHScheduleMask* otherMask = NULL; // silence gcc
    switch (comb->getScheduleType())
    {
    case eCombTransition:
    case eCombInput:
    case eCombAsync:
    case eCombDCL:
      UtIO::cout() << ", sample=(";
      otherMask = mMarkDesign->getSignature(flow)->getSampleMask();
      break;
    case eCombSample:
      UtIO::cout() << ", transition=(";
      otherMask = mMarkDesign->getSignature(flow)->getTransitionMask();
      break;
    default:
      FLN_ELAB_ASSERT("Unhandled value for schedule type" == NULL, flow);
      break;
    }
    buf.clear();
    otherMask->compose(&buf, NULL);
    UtIO::cout() << buf << ")\n";
  } // for
} // SCHDump::dumpMultiScheduleBlocks

static inline double percent(UInt32 num, UInt32 total)
{
  double result;
  if (total == 0) return 0;
  if (((num * 100) / 100) == num)
    result = ((double)num * 100) / (double)total;
  else
    result = ((double)num / (double)total) * 100;
  return result;
}

SCHDump::ScheduleMaskStats*
SCHDump::getScheduleMaskEntry(AllScheduleStats* stats,
			      const SCHScheduleMask* mask)
{
  ScheduleMaskTable::iterator pos = stats->maskTable.find(mask);
  if (pos == stats->maskTable.end())
  {
    ScheduleMaskStats initStats;
    memset(&initStats, 0, sizeof(initStats));
    stats->maskTable[mask] = initStats;
    pos = stats->maskTable.find(mask);
  }
  return &(pos->second);
}

void
SCHDump::gatherCombinationalStats(SCHCombinational* comb,
				  SCHScheduleType type,
				  AllScheduleStats* stats)
{
  if (!comb->empty())
  {
    // Get the mask entry
    const SCHScheduleMask* mask = comb->getMask();
    ScheduleMaskStats* maskStats = getScheduleMaskEntry(stats, mask);

    // Set the schedule mask counts
    UInt32 blockCount = comb->size();
    switch (type)
    {
    case eCombTransition:
      maskStats->transitionComboBlocks += blockCount;
      break;

    case eCombSample:
      maskStats->sampleComboBlocks += blockCount;
      break;

    case eCombDCL:
      maskStats->derivedComboBlocks += blockCount;
      break;

    case eCombInput:
      maskStats->inputComboBlocks += blockCount;
      break;

    case eCombAsync:
      maskStats->asyncComboBlocks += blockCount;
      break;

    default:
      SCHED_ASSERT("Unkown combinational schedule type" == NULL, comb);
      break;
    }

    // Set the global counts
    stats->totalBlocks += blockCount;
    if (type == eCombDCL)
      stats->totalDerivedClockLogicBlocks += blockCount;
    stats->totalComboBlocks += blockCount;

    // Count the number of unique blocks called by inserting them into
    // a map of block to schedule to call count.
    for (SCHIterator iter = comb->getSchedule(); !iter.atEnd(); ++iter)
    {
      FLNodeElab* flow = *iter;
      const NUUseDefNode* useDef = flow->getUseDefNode();
      BlockCalls& blockCalls = stats->combinationalBlocks[useDef];
      BlockInstCalls& blockInstCalls = blockCalls[flow->getHier()];
      ++blockInstCalls[comb];
    }
  }
}

void
SCHDump::gatherSequentialEdgeStats(SCHSequential* seq,
				   SCHScheduleType type, ClockEdge edge,
				   const NUNetElab* clk,
				   AllScheduleStats* stats)
{
  if (!seq->empty(edge))
  {
    // Get the mask entry
    const SCHEvent* ev = mFactory->buildClockEdge(clk->getSymNode(), edge);
    const SCHScheduleMask* clkMask = mFactory->buildMask(ev);
    ScheduleMaskStats* maskStats = getScheduleMaskEntry(stats, clkMask);

    // Set the schedule mask counts
    UInt32 blockCount = seq->size(edge);
    switch (type)
    {
    case eSequential:
      maskStats->sequentialBlocks += blockCount;
      break;

    case eSequentialDCL:
      maskStats->derivedSequentialBlocks += blockCount;
      break;

    default:
      SCHED_ASSERT("Unkown sequential schedule type" == NULL, seq);
      break;
    }

    // Set the global counts
    stats->totalBlocks += blockCount;
    if (type == eSequentialDCL)
      stats->totalDerivedClockLogicBlocks += blockCount;
    stats->totalSequentialBlocks += blockCount;
    for (SCHIterator iter = seq->getSchedule(edge); !iter.atEnd(); ++iter)
    {
      FLNodeElab* flow = *iter;
      if (flow->getDefNet()->getNet()->isDoubleBuffered())
	++stats->totalStateUpdateBlocks;

      // Count the number of unique blocks called by inserting them
      // into a map of block to schedule to call count.
      const NUUseDefNode* useDef = flow->getUseDefNode();
      BlockCalls& blockCalls = stats->sequentialBlocks[useDef];
      BlockInstCalls& blockInstCalls = blockCalls[flow->getHier()];
      ++blockInstCalls[seq];
    }
  } // if
} // gatherSequentialEdgeStats

void
SCHDump::gatherSequentialStats(SCHSequential* seq,
			       SCHScheduleType type, AllScheduleStats* stats)
{
  // Gather the stats for the positive and negative edges edge
  const NUNetElab* clk = seq->getEdgeNet();
  gatherSequentialEdgeStats(seq, type, eClockPosedge, clk, stats);
  gatherSequentialEdgeStats(seq, type, eClockNegedge, clk, stats);
}

void SCHDump::printScheduleStats(const char* fileRoot, bool output)
{
  // Open the file
  UtString filename;
  filename << fileRoot << ".scheduleStatistics";
  UtOStream* out = new UtOBStream(filename.c_str());

  // Print the schedule mask order
  mFactory->printScheduleMaskOrder(*out);
  if (output) {
    UtOStream& cout = UtIO::cout();
    mFactory->printScheduleMaskOrder(cout);
  }

  // Gather information on the number of sequential and combinational
  // blocks -TBD

  // Allocate space to gather statistics
  AllScheduleStats scheduleStats;
  scheduleStats.totalBlocks = 0;
  scheduleStats.totalDerivedClockLogicBlocks = 0;
  scheduleStats.totalSequentialBlocks = 0;
  scheduleStats.totalStateUpdateBlocks = 0;
  scheduleStats.totalComboBlocks = 0;

  // Gather statistics for all the schedules (except initial and debug)
  SCHSchedulesLoop p;
  for (p = mSched->loopAllSimulationSchedules(); !p.atEnd(); ++p)
  {
    SCHScheduleType type = p.getScheduleBase()->getScheduleType();
    if (mSched->isSequentialSchedule(type))
    {
      SCHSequential* seq = p.getSequentialSchedule();
      gatherSequentialStats(seq, type, &scheduleStats);
    }
    else
    {
      SCHCombinational* comb = p.getCombinationalSchedule();
      gatherCombinationalStats(comb, type, &scheduleStats);
    }
  }

  // Print the information
  printGatheredStats(*out, scheduleStats);
  if (output) {
    printGatheredStats(UtIO::cout(), scheduleStats);
    UtIO::cout().flush();
  }

  // All done, close the file
  delete out;
} // void SCHDump::printScheduleStats

void
SCHDump::printGatheredStats(UtOStream& out, const AllScheduleStats& scheduleStats)
{
  UtString buf;
  out << UtIO::endl;
  out << "  Seq.    CombT.   ComboS.  DerSeq. DerComb.  Input   "
      << "  Async        Mask\n";
  out << "-------- -------- -------- -------- -------- -------- "
      << "-------- ----------------\n";
  for (ScheduleMaskTable::SortedLoop m = scheduleStats.maskTable.loopSorted();
       !m.atEnd(); ++m)
  {
    ScheduleMaskStats* maskEntry = &m.getValue();

    // Print the counts
    out << UtIO::setw(8) << maskEntry->sequentialBlocks
        << " " << UtIO::setw(8) << maskEntry->transitionComboBlocks
        << " " << UtIO::setw(8) << maskEntry->sampleComboBlocks
        << " " << UtIO::setw(8) << maskEntry->derivedSequentialBlocks
        << " " << UtIO::setw(8) << maskEntry->derivedComboBlocks
        << " " << UtIO::setw(8) << maskEntry->inputComboBlocks
        << " " << UtIO::setw(8) << maskEntry->asyncComboBlocks;
    
    // Print the mask
    buf.clear();
    const SCHScheduleMask* mask = m.getKey();
    mask->compose(&buf,NULL);
    out << " " << buf << UtIO::endl;
  }

  // Print the summary information
  out << "Totals:"
      << " All: " << UtIO::setw(8) << scheduleStats.totalBlocks
      << " Seq.: " << UtIO::setw(8) << scheduleStats.totalSequentialBlocks
      << " SU: " << UtIO::setw(8) << scheduleStats.totalStateUpdateBlocks
      << " Comb.: " << UtIO::setw(8) << scheduleStats.totalComboBlocks
      << " DCL: " << UtIO::setw(8)
      << scheduleStats.totalDerivedClockLogicBlocks
      << UtIO::endl;

  // Print the combinational block sample vs. transition schedule
  // statistics
  UInt32 totalNodes;
  out << UtIO::endl;
  totalNodes = mSampleNodes + mTransitionNodes + mMustBeTransitionNodes;
  out << "ComboStats: " << UtIO::fixed<< UtIO::setprecision(1)
      << "sample: " << mSampleNodes << "("
      << percent(mSampleNodes, totalNodes) << "%)"
      << ", transition: " << mTransitionNodes << "("
      << percent(mTransitionNodes, totalNodes) << "%)"
      << ", forced transition: " << mMustBeTransitionNodes << "("
      << percent(mMustBeTransitionNodes, totalNodes) << "%)"
      << UtIO::endl;
  out << "ComboStats:" << UtIO::fixed<< UtIO::setprecision(1)
      << " total: " << mMustBeAccurateNodes
      << ", clks: " << mDrivenByClockNodes
      << ", lat: " << mLatchNodes
      << ", cycle: " << mCycleNodes
      << ", mem: " << mLevelMemoryNodes
      << ", outpin: " << mFeedsOutputPinNodes
      << UtIO::endl;

  // Gather and print information about state update nets
  UInt32 normalStateUpdateNets = 0;
  for (SCHSequentials::SULoop l = mSched->loopSUNets(); !l.atEnd(); ++l)
    ++normalStateUpdateNets;
  UInt32 dclStateUpdateNets = 0;
  SCHSchedule::DerivedClockLogicLoop d;
  for (d = mSched->loopDerivedClockLogic(); !d.atEnd(); ++d)
  {
    SCHDerivedClockBase* dclBase = *d;
    for (SCHSequentials::SULoop l = dclBase->loopSUNets(); !l.atEnd(); ++l)
      ++dclStateUpdateNets;
  }
  if ((dclStateUpdateNets > 0) || (normalStateUpdateNets > 0))
  {
    out << "State Update Nets: clock logic: " << dclStateUpdateNets
        << ", normal: " << normalStateUpdateNets
        << ", total: " << normalStateUpdateNets + dclStateUpdateNets
        << UtIO::endl;
  }
  if ((mClkStateUpdates > 0) || (mCycleStateUpdates > 0))
  {
    out << "State Update Net Reason: multi-clk: " << mClkStateUpdates
        << ", cycles: " << mCycleStateUpdates
        << ", total: " << mClkStateUpdates + mCycleStateUpdates
        << UtIO::endl;
  }

  // Gather and print information about scheduled blocks
  UInt32 combSchedSize;
  UInt32 seqSchedSize;
  combSchedSize = printBlockStats(out, scheduleStats.combinationalBlocks,
				  "ComboStats: ");
  seqSchedSize = printBlockStats(out, scheduleStats.sequentialBlocks,
				 "SeqStats: ");
  out << "Schedule Size: Comb: " << combSchedSize
      << " Seq: " << seqSchedSize
      << " Total: " << combSchedSize + seqSchedSize
      << UtIO::endl;

} // void SCHDump::printScheduleStats

static inline void
updateHistogram(UInt32 value, UInt32* histogram, UInt32* max)
{
  if (value >= SCH_HISTOGRAM_SIZE)
    ++histogram[SCH_HISTOGRAM_SIZE];
  else
    ++histogram[value];
  if (value > *max)
    *max = value;
}

static inline void
printHistogram(UtOStream& out, const char* prefix, const char* histogramName,
	       UInt32* histogram, UInt32 max)
{
  if (max > 1)
  {
    out << prefix << histogramName << ", max = " << max << ":\n";
    for (UInt32 i = 0; i <= SCH_HISTOGRAM_SIZE; ++i)
      if (histogram[i] > 0)
      {
	out << "  ";
	if (i == SCH_HISTOGRAM_SIZE)
	  out << ">" << UtIO::setw(2) << i;
	else
	  out << UtIO::setw(3) << i;
	out << " = " << UtIO::setw(10) << histogram[i] << UtIO::endl;
      }
  }
}

UInt32
SCHDump::printBlockStats(UtOStream& out, const BlockStats& blockStats,
                         const char* prefix)
{
  // Structures for the various data we will accumulate
  UInt32 uniqueBlocks = 0;
  UInt32 blockInstances = 0;
  UInt32 scheduledBlocks = 0;
  UInt32 multiScheduleHistogram[SCH_HISTOGRAM_SIZE+1];
  memset(multiScheduleHistogram, 0, sizeof(multiScheduleHistogram));
  UInt32 maxScheduled = 0;
  UInt32 multiCalledBlocks = 0;
  UInt32 multiCallHistogram[SCH_HISTOGRAM_SIZE+1];
  memset(multiCallHistogram, 0, sizeof(multiCallHistogram));
  UInt32 maxCalled = 0;
  UInt32 scheduleSize = 0;

  // Go through the blocks and gather various statistics
  BlockStats::const_iterator s;
  for (s = blockStats.begin(); s != blockStats.end(); ++s)
  {
    // get and count the scheduling information for this block
    const BlockCalls& blockCalls = s->second;
    const NUUseDefNode* useDef = s->first;
    ++uniqueBlocks;

    // Count the number of defs for this block
    UInt32 defCount;
    if (useDef->isCycle())
      defCount = 1;
    else
    {
      // Loop through the defs. Count state update nets as double
      NUNetSet defs;
      useDef->getDefs(&defs);
      defCount = 0;
      for (NUNetSetIter i = defs.begin(); i != defs.end(); ++i)
      {
	NUNet* net = *i;
	if (net->isDoubleBuffered())
	  defCount += 2;
	else
	  ++defCount;
      }
    }

    // Go through the block instances and count them
    BlockCalls::const_iterator b;
    for (b = blockCalls.begin(); b != blockCalls.end(); ++b)
    {
      // Count this instance
      const BlockInstCalls& blockInstCalls = b->second;
      ++blockInstances;

      // Go through the schedules this block is in and count them
      UInt32 scheduleCount = 0;
      BlockInstCalls::const_iterator c;
      for (c = blockInstCalls.begin(); c != blockInstCalls.end(); ++c)
      {
	// Get and count this schedule info
	++scheduleCount;
	UInt32 callCount = c->second;

	// Update the call histogram, data, and maximum
	multiCalledBlocks += callCount;
	updateHistogram(callCount, multiCallHistogram, &maxCalled);

	// Compute the schedule size information
	scheduleSize += (callCount * defCount);
      }

      // Update the multi schedule histogram, data, and maximum
      scheduledBlocks += scheduleCount;
      updateHistogram(scheduleCount, multiScheduleHistogram, &maxScheduled);
    } // for
  } // for

  // Print the various statistics
  out << prefix
      << "Unique Blocks: " << uniqueBlocks
      << ", Instances: " << blockInstances
      << ", Scheduled: " << scheduledBlocks
      << ", Called: " << multiCalledBlocks
      << UtIO::endl;

  // Print the schedule histogram
  printHistogram(out, prefix, "Multi-schedule histogram", multiScheduleHistogram,
		 maxScheduled);
  printHistogram(out, prefix, "Multi-call histogram", multiCallHistogram,
		 maxCalled);

  // Return the schedule size
  return scheduleSize;
}

void SCHDump::print() const
{
  SCHSchedule::CombinationalLoop l;
  for (l = mSched->loopCombinational(eCombInput); !l.atEnd(); ++l) {
    SCHCombinational* input = *l;
    input->dump(0, mMarkDesign);
  } // for

  if (!mSched->getInitialSchedule()->empty()) {
    mSched->getInitialSchedule()->dump(0, mMarkDesign);
  }

  if (!mSched->getInitSettleSchedule()->empty()) {
    mSched->getInitSettleSchedule()->dump(0, mMarkDesign);
  }

  if (!mSched->getDebugSchedule()->empty()) {
    mSched->getDebugSchedule()->dump(0, mMarkDesign);
  }

  if (!mSched->getForceDepositSchedule()->empty()) {
    mSched->getForceDepositSchedule()->dump(0, mMarkDesign);
  }

  for (l = mSched->loopCombinational(eCombAsync); !l.atEnd(); ++l) {
    SCHCombinational* async = *l;
    async->dump(0, mMarkDesign);
  } // for

  SCHSchedule::DerivedClockLogicLoop d;
  for (d = mSched->loopDerivedClockLogic(); !d.atEnd(); ++d)
  {
    SCHDerivedClockBase* dclBase = *d;
    dclBase->dump(0, mMarkDesign);
  }

  // Print the sample based combinational schedules
  SCHSchedule::CombinationalLoop pc;
  for (pc = mSched->loopCombinational(eCombSample); !pc.atEnd(); ++pc)
  {
    SCHCombinational* comb = *pc;
    comb->dump(0, mMarkDesign);
  }

  SCHSequentials::SequentialLoop ps;
  for (ps = mSched->loopSequential(); !ps.atEnd(); ++ps)
  {
    SCHSequential* sc = *ps;
    sc->dump(0, mMarkDesign);
  }

  // Print the transition based combinational schedules
  for (pc = mSched->loopCombinational(eCombTransition); !pc.atEnd(); ++pc)
  {
    SCHCombinational* comb = *pc;
    comb->dump(0, mMarkDesign);
  }

  // Dump the flow...
  NUNetList nl;
  UtIO::cout() << UtIO::endl;

  FLNodeElab::NodeElabSet covered;
  for (SCHMarkDesign::OutputPortsLoop p = mMarkDesign->loopOutputPorts();
       !p.atEnd(); ++p)
    (*p)->dumpHelper(&covered, 0, eSensLevelOrEdge,
		     mUtil->getFlowElabFactory(),
                     false,
                     UtIO::cout());
} // void SCHDump::print


void
SCHDump::composeFlowName(const FLNodeElab* flow, UtString* name)
{
  // Create the unique name for this flow node
  NUUseDefNode* useDef = flow->getUseDefNode();
  name->clear();
  if (!useDef->isCycle())
  {
    // Create the location
    const SourceLocator& loc = useDef->getLoc();
    *name << loc.getFile() << ":" << loc.getLine();

    // Add the net name
    UtString buf;
    flow->getDefNet()->getSymNode()->compose(&buf, true );  // include root in name 
    *name << " " << buf;
  }
  else
  {
    // Cycles have no location or net name, but they do have a
    // unique name
    const FLNodeElabCycle* cycle = dynamic_cast<const FLNodeElabCycle*>(flow);
    *name << *(cycle->getName()) << ":";
  }
}
