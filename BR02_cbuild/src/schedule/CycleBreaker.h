// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*! \file
    This is the SCHCycleBreaker class definition.  The SCHCycleBreaker class
    is used to find non-looping schedules for cyclic flow.
*/

#ifndef _CYCLEBREAKER_H_
#define _CYCLEBREAKER_H_

#include "nucleus/Nucleus.h"
#include "flow/Flow.h"
#include "bdd/BDD.h"
#include "util/UtStack.h"
#include "CycleGraph.h"

class STBranchNode;
class SCHUtil;
class SCHMarkDesign;
class SCHConditionMap;

//! SCHCycleBreaker class will schedule cycles without loops, if possible
class SCHCycleBreaker
{
public:
  //! constructor
  SCHCycleBreaker(NUCycle* cycle, DependencyGraph* graph,
                  SCHUtil* util, SCHMarkDesign* mark,
                  UInt32 branchDepthLimit = 5);

  //! destructor
  ~SCHCycleBreaker() {}

  //! Analyze the cycle and schedule its flow nodes
  void execute(bool writeDotFile=false);
  //! Schedule the annotated flow graph - used as a callback
  void schedule(DependencyGraph* graph);
private:
  typedef UtMap<FLNodeElab*,DependencyGraph::Node*> FlowToDGNodeMap;

  //! Forward class for a covered class by BDD condition
  class BddCovered;

  //! True when cond depends on the output of flow
  bool dependsOn(const BDD& cond, FLNodeElab* flow);
  //! Builds a map from flow's fanin to conditions under which the fanin is used
  SCHConditionMap* gatherConditions(FLNodeElab* flow);
  //! Handles the nested fanin traversal for gatherConditions()
  void gatherConditionHelper(FLNodeElab* flow, FLNodeElabSet* faninSet,
                             UtStack<BDD>* condStack, SCHConditionMap* condMap,
                             BddCovered* covered, bool checkDepends=false);

  void gatherCaseConditionHelper(FLNodeElab * flow, 
                                 NUCase * caseStmt,
                                 FLNodeElabSet* faninSet,
                                 UtStack<BDD>* condStack,
                                 SCHConditionMap* condMap,
                                 BddCovered* covered);

  void gatherIfConditionHelper(FLNodeElab * flow, 
                               NUIf * ifStmt,
                               FLNodeElabSet* faninSet,
                               UtStack<BDD>* condStack,
                               SCHConditionMap* condMap,
                               BddCovered* covered);

  void gatherEnabledDriverConditionHelper(FLNodeElab * flow, 
                                          NUEnabledDriver * edStmt,
                                          FLNodeElabSet* faninSet,
                                          UtStack<BDD>* condStack,
                                          SCHConditionMap* condMap,
                                          BddCovered* covered);

  void gatherAssignConditionHelper(FLNodeElab * flow, 
                                   NUAssign * assign,
                                   FLNodeElabSet* faninSet,
                                   UtStack<BDD>* condStack,
                                   SCHConditionMap* condMap,
                                   BddCovered* covered);

  void gatherStatementConditionHelper(FLNodeElab * flow, 
                                      FLNodeElabSet* faninSet,
                                      UtStack<BDD>* condStack,
                                      SCHConditionMap* condMap,
                                      BddCovered* covered);

  void gatherExprConditionHelper(FLNodeElab * flow, 
                                 NUExpr * expr,
                                 FLNodeElabSet* faninSet,
                                 UtStack<BDD>* condStack,
                                 SCHConditionMap* condMap,
                                 BddCovered* covered);

  void gatherTernaryConditionHelper(FLNodeElab * flow, 
                                    NUTernaryOp * top,
                                    FLNodeElabSet* faninSet,
                                    UtStack<BDD>* condStack,
                                    SCHConditionMap* condMap,
                                    BddCovered* covered);

  //! Compute the condition for an edge due to multi-driver dependencies
  BDD brotherConditions(FLNodeElab* fromFlow, GraphNode* toNode,
                        FlowToDGNodeMap* nodeMap);
  //! Choose a good variable to the break the cycle with
  BDD chooseBreakVariable(DependencyGraph* graph);
  //! Schedule the graph as a loop
  void scheduleLoop(DependencyGraph* graph);
  //! Schedule a version of the graph reduced by the given variable assignment
  void scheduleReduced(DependencyGraph* graph, const BDD& var, const BDD& value);
  //! Reduce the graph based on the variable assignment
  DependencyGraph* reduceGraph(DependencyGraph* graph, const BDD& var, const BDD& value);
private:
  //! Helper function to dump the graph bdds
  void dumpGraphBdds(int cycleId, DependencyGraph* graph);


  NUCycle*         mCycle;            //!< The NUCycle we are working on
  DependencyGraph* mGraph;            //!< The flow dependency graph that describes the cycle
  SCHUtil*         mUtil;             //!< The scheduler utility instance
  SCHMarkDesign*   mMarkDesign;       //!< The mark design instance used to get true fanin 
  BDDContext       mBDDContext;       //!< The BDDContext used to represent flow conditions
  UInt32           mBranchDepth;      //!< The current branching depth
  UInt32           mBranchDepthLimit; //!< The maximum allowed branching depth
  UInt32           mReductionCount;   //!< The number of graph reductions performed
  bool             mWriteDotFiles;    //!< Set to true to enable dumping graphs
}; // class SCHCycleBreaker

#endif // _CYCLEBREAKER_H_
