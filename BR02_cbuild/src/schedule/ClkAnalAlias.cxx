// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "ClkAnal.h"
#include "ClkAnalAlias.h"
#include "flow/FLNodeElab.h"
#include "flow/FLNode.h"
#include "flow/FLIter.h"
#include "iodb/IODBNucleus.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUAlwaysBlock.h"
#include "Util.h"

//! ClockAlias class. Storage for alias candidates and their fanin and fanout relatives
struct ClockAlias
{
  ClockAlias(NUNetElab* master): mMaster(master), mIsAlias(true) {}
  // the entries in mDrivers and mFanout are matched pairs
  // (that is the mDrivers[j] is the driver for mFanout[j])
  // if mFanout[j] is null then we are looking at a primary output or the driver is a nested flow for fanout
  FLNodeElabVector mDrivers;
  FLNodeElabVector mFanout;
  NUNetElab* mMaster;
  //! if true (specified @ construction) then is an alias, if false then just an Equivalence
  bool mIsAlias;
  //! a list of the immediate fanin clocks of this
  UtVector<ClockAlias*> mLevelFanin;

  // When we discover that we cannot alias a clock due to its fanout
  // being non aliasable, then we must not alias any of the fanin
  // whose aliasability is dependent on our aliasiability.  I found
  // this situation hard to reproduce in a tiny testcase, but it
  // it occurs in test/cust/amcc/3450/gmacenv, resulting in comparescan
  // failures
  //! mark this ClockAlias as NOT being an alias, and recursively process its fanin in the same way
  void doNotAlias()
  {
    if (mIsAlias)
    {
      mIsAlias = false;
      for (size_t i = 0; i < mLevelFanin.size(); ++i)
        mLevelFanin[i]->doNotAlias();
    }
  }

  bool isAlias() {return mIsAlias;}

  //! add a fanin to the fanin vector of this ClockAlias
  void addLevelFanin(ClockAlias* levelFanin)
  {
    mLevelFanin.push_back(levelFanin);
  }
};


// Is this net the master of a set of clock aliases?  This is
// meant to be called after clock equivalence discovery has
// completed -- i.e. in ::aliasClocks()
bool ClkAnalAlias::isMaster(NUNetElab* net)
{
  bool ret = false;
  if (mBDDContext->isAssigned(net))
  {
    BDD bdd = mBDDContext->bdd(net);
    NU_ASSERT((bdd.isValid()), net);
    ClockMap::iterator p = mClocks.find(bdd);
    if (p != mClocks.end())
    {
      SCHClock* clock = p->second;
      ret = clock->getMaster() == net;
    }
  }
  return ret;
}

bool ClkAnalAlias::isNestedPair(FLNodeElab* fanout, FLNodeElab* driver)
{
  return (fanout->hasNestedBlock() &&
          (fanout->getDefNet() == driver->getDefNet()) &&
          (fanout->isNested(driver)));
}

void ClkAnalAlias::aliasClockNets(NUNetElab* master, NUNetElab* aliasNet)
{
  bool isPrimaryZ = master->getNet()->isPrimaryZ();
  aliasNet->alias(master);
  NUNet* masterNet = master->getNet();
  if (!isPrimaryZ && masterNet->isPrimaryZ())
  {
    // Don't allow primary-z flag on alias to propagate to master.
    //
    // primary-Z propagation does not need to occur when doing clock
    // aliasing, because in this phase, aliasing does not cause sharing
    // of storage.
    //
    // An example where this next line of code gets hit is
    // test/clock-aliasing/zclock2.v.
    //
    masterNet->putIsPrimaryZ(false);
  }
}

#ifdef CDB
static NUNetElab* sAliasNetDebug = NULL;
#endif


// constructor
ClkAnalAlias::ClkAnalAlias(SCHClkAnal * clkAnal, NUDesign* design,  BDDContext* bddContext, ClockMap & clocks, SCHUtil* util) :
  mDesign(design),
  mClocks(clocks)
{
  mClkAnal = clkAnal;
  mBDDContext = bddContext;
  mUtil = util;
  mVerbose = (getenv("CLK_ALIAS_SPEW") != NULL);
}

ClkAnalAlias::~ClkAnalAlias()
{
}

void ClkAnalAlias::preClkAliasSanityCheck()
{
  // Verify that all the aliases have BDDs that match the master
  for (ClockMap::iterator p = mClocks.begin(); p != mClocks.end(); ++p)
  {
    BDD bdd = p->first;
    SCHClock* clock = p->second;
    for (SCHClock::AliasLoop q = clock->loopAliases(); !q.atEnd(); ++q)
    {
      NUNetElab* alias = *q;
      NU_ASSERT((mBDDContext->isAssigned(alias)), alias);
      NU_ASSERT((mBDDContext->bdd(alias) == bdd), alias);
    }
  }
}

void ClkAnalAlias::findAliasCandidates( )
{
  UtOStream& cout = UtIO::cout();

  // poplulate the mAliasMap, and disqualify those ClockAlias that
  // should not be aliased(e.g. POs)
  //   Walk the design and put all the aliases, and the nodes
  // they fanout to, into the fanout map
  for (FLDesignElabIter iter(mDesign,
			     mUtil->getSymbolTable(),
			     FLIterFlags::eClockMasters, // start
			     FLIterFlags::eAll,          // visit
			     FLIterFlags::eNone,         // Stop at nothing
			     FLIterFlags::eBranchAtAll,  // @ nesting, go in and over
			     FLIterFlags::eIterFaninPairOnce);
       not iter.atEnd();
       iter.next())
  {
    FLNodeElab *driver = *iter;
    NUUseDefNode* driverNode = driver->getUseDefNode();
    NUNetElab* aliasNet = driver->getDefNet();
#ifdef CDB
    if (aliasNet == sAliasNetDebug)
      UtIO::cerr() << "Found net\n";
#endif
    if ((driverNode != NULL) && driverNode->isInitial()) {
      iter.doNotExpand();       // do not explore fanin from initial blocks
      continue;
    }
    if ( driver->isData() ){
      continue;                 // we are trying to process clocks only
    }
    if ( not mBDDContext->isAssigned(aliasNet) ){
      continue;                 // we need a bdd to do this analysis
    }
    
    FLNodeElab *fanout = iter.getCurParent();
    NUUseDefNode* fanoutNode = NULL;
    NUNetElab* fanoutNet = NULL;
    ClockAlias* fanoutAlias = NULL;
    if (fanout != NULL) {
      fanoutNet = fanout->getDefNet();
      fanoutNode = fanout->getUseDefNode();
      NU_ASSERT((!fanoutNode->isInitial()), fanoutNode);
      AliasMap::iterator p = mAliasMap.find(fanoutNet);
      if (p != mAliasMap.end()) {
        fanoutAlias = p->second;
      }
    }

    BDD bdd = mBDDContext->bdd(aliasNet);
    NU_ASSERT((bdd.isValid()), aliasNet);
    NUNetElab* masterNet = NULL;
    ClockMap::iterator p = mClocks.find(bdd);
    if (p != mClocks.end()) {
      // we have seen this bdd before
      masterNet = p->second->getMaster();
    }
    if ((masterNet != aliasNet) && (masterNet != NULL))
    {
      // this is a clock alias potential candidate, put it into mAliasMap if not already there
      ClockAlias* clockAlias = mAliasMap[aliasNet];
      if (clockAlias == NULL) {
        clockAlias = new ClockAlias(masterNet);
        mAliasMap[aliasNet] = clockAlias;
      } else {
        NU_ASSERT((masterNet == clockAlias->mMaster), masterNet);
      }

      if (clockAlias->isAlias()) {
        // first see if there is any reason that this should not be a clock alias
        // some reasons include:
        //  . the signal is level sensitive and it fans out to data,
        //  . the fanout is to something that we did not calculate a  bdd for
        //  . the fanout is already marked as not being a valid alias candidate
        //  . the aliasnet and fanoutnet are not both vectors or both scalars
        //  . the fanout is to a master 
        //  . 
        // the reason we do not want to alias a net that fans out
        // to a master is because we will need to calculate the net in
        // order to calculate the master.

        IODBNucleus* iodb = mClkAnal->getIODB();

        // first check the compatibility between master and alias nets
        if ( iodb->isForcibleNet(masterNet->getNet()) or
             iodb->isForcibleNet(aliasNet->getNet())    ) {
          // if either is forcible then we cannot alias bug4453
          clockAlias->doNotAlias();
        } else if (( aliasNet->getNet()->isVectorNet() ) xor
                   ( masterNet->getNet()->isVectorNet())    ) {
          // one net is a scalar while the other is a single bit vector
          // (these are also disqualified when doing assignment aliases)
          clockAlias->doNotAlias();
        }

        // now checks related to fanout
        if ((fanoutAlias != NULL) and  not fanoutAlias->isAlias()) {
          // clockAlias fans out to something that has already been
          // disqualified from being an alias, so clockAlias must be disqualified
          clockAlias->doNotAlias();
        }

        if ( fanout == NULL ) {
          // as of 10/27/04 there are only two situations where fanout is null:
          // 1. we are looking at a primary output
          //    (and in this case we do not want to alias away the primary output)
          // 2. we are looking at something that was marked as observable
          //    (which is handled as a primary ouput in most sections
          //    of cbuild, but we can allow it to be clock aliased. test/bugs/bug2751)
          NUNet *net = aliasNet->getNet();
          if (net->isPrimaryOutput() || net->isPrimaryBid()) {
            clockAlias->doNotAlias();            // do not alias away primary outputs.
          } else if ( not net->isProtectedObservable(iodb) ) {
            clockAlias->doNotAlias();            // not marked observable so do not alias away
          }
        } else {
          switch ( iter.getSensitivity() ) {
          case eSensLevel: {
            // the signal is level sensitive (not edge)
            if (fanout->isData() or isMaster(fanout->getDefNet())) {
              // If this clock-net fans out to data or to a clock-master
              // clock, then it cannot be an alias because it must be
              // calculated so that its fanout is correct
              clockAlias->doNotAlias();
            } else if ((fanoutNet != NULL) and (fanoutAlias == NULL) and not fanoutNet->getNet()->isBlockLocal()) {
              // see bug2415 & test/clock-aliasing/aliastree.v.  If we
              // have a level-sensitive fanout whose alias information is
              // unknown cause we punted computing BDDs or something,
              // then be pessimistic about aliasing.  But exclude block-locals
              // from this filter because that makes the aliasing for
              // test/clock-aliasing/clkbuf_redundant.v too
              // pessimistic, excluding clk1 from being aliased to clk just
              // cause clk2 has some rescoped fanin.  We've lost visibility
              // to that fanin so we don't need it to prevent us from aliasing
              // clk1.
              clockAlias->doNotAlias();
            }
            // what if the user has specified that fanoutNet is observable?

            break;
          }
          case eSensLevelOrEdge:
          case eSensEdge: {
            if (fanout->isData() and isNestedPair(fanout, driver)) {
              // normally one might think that a fanout to data would
              // disqualify a candidate for clock alias, but to avoid
              // disqualification of clock inputs to flops we only disqualify
              // if there is a nested relationship between the driver/fanout
              // see test/clock-aliasing/state_chain_b.v
              clockAlias->doNotAlias();
            }
            break;
          }
          default: {
            FLN_ELAB_ASSERT( 0, fanout); // "unhandled sensivity value:(!level && !edge) in clock analysis"
          }
          }
        }
      }

      if (clockAlias->isAlias()) {
        // if here then this appears to be a valid alias candidate.  Save the
        // fanin information on fanoutAlias in case we need to go back
        // and disqualify it if we eventually find that something
        // downstream from fanoutAlias cannot be made an alias.
        if (fanoutAlias != NULL ) {
          fanoutAlias->addLevelFanin(clockAlias);
        }
      }

      if (mVerbose) {
        UtString buf;
        buf.clear();
        aliasNet->compose(&buf, NULL, true);
        cout << "alias: " << buf;
        buf.clear();
        masterNet->compose(&buf, NULL, true);
        cout << " master: " << buf;
        if (fanout != NULL) {
          cout << "  fanout: ";
          fanout->pname(0, true);
        } else {
          cout << "  (no fanout)";
        }
      }
    }
  } // for
}

void ClkAnalAlias::findAliasFanoutInfo()
{
  // Now do another design walk that looks into intial blocks, so we
  // can get the complete set of driver/fanout pairs for each ClockAlias
  // (mDrivers and mFanout).  Only consider those ClockAlias that are already
  // in mAliasMap.
  // continue to disqualify anything we find that is not a valid candidate
  // (such as a PO)
  for (FLDesignElabIter iter(mDesign,
			     mUtil->getSymbolTable(),
			     FLIterFlags::eClockMasters,    // start
			     FLIterFlags::eAll,             // visit
			     FLIterFlags::eNone,            // Stop at nothing
			     FLIterFlags::eBranchAtAll,     // @ nesting go in and over
			     FLIterFlags::eIterFaninPairOnce);
       not iter.atEnd();
       iter.next())
  {
    FLNodeElab *driver = *iter;
    NUNetElab* aliasNet = driver->getDefNet();
#ifdef CDB
    if (aliasNet == sAliasNetDebug)
      UtIO::cerr() << "Found net\n";
#endif
    FLNodeElab *fanout = iter.getCurParent();

    AliasMap::iterator p = mAliasMap.find(aliasNet);
    if (p != mAliasMap.end())
    {
      ClockAlias* clockAlias = p->second;
      if (clockAlias->isAlias())
      {
        if ( fanout == NULL ) {
          // any situations that needed to be disqualified when fanout
          // is null were handled in findAliasCandidates, so just
          // ignore this clockAlias here.
        }
        else if (isNestedPair(fanout, driver))
        {
          // Driver is a nested flow for fanout.  If we have killed
          // the fanout, then we can kill the entire nested tree, at
          // least as far as this fanout is concerned.  For some
          // reason it appears that the sensitivity when coming into
          // a nested flow is not well defined.  We also expect that
          // the fanout should have been already inserted into the
          // list of drivers that should be killed.
          //
          // Note that we can't kill this nested if the fanout is
          // not in the drivers list, and that could occur if the
          // continous flow is in the data path.
          bool found = false;
          for (UInt32 i = 0; !found && (i < clockAlias->mDrivers.size()); ++i) {
            found = (fanout == clockAlias->mDrivers[i]);
          }
          if (found) {
            fanout = NULL;
          } else {
            clockAlias->doNotAlias();
          }
        }
      } // if

      clockAlias->mDrivers.push_back(driver);
      clockAlias->mFanout.push_back(fanout);
    } // if
  } // for
}

void ClkAnalAlias::logMasterStorage(ClockAlias* clockAlias)
{
    MasterStorage::iterator p = mMasterStorage.find(clockAlias->mMaster);
    if (p == mMasterStorage.end())
    {
      STAliasedLeafNode* masterNode = clockAlias->mMaster->getSymNode();
      STAliasedLeafNode* storage = masterNode->getInternalStorage();
      if (storage == NULL)
      {
        storage = masterNode;
        storage->setThisStorage();
      }
      mMasterStorage[clockAlias->mMaster] = storage;
    }
  
}
void ClkAnalAlias::makeAliasSubstitution()
{
    
  // track the dead fanout to avoid fixing the fanout that defs aliases we are killing
  FLNodeElabSet deadFanout;

  // Walk the fanout-map and correct all the pointers,
  // deleting the dead flow
  for (AliasMap::iterator p = mAliasMap.begin(); p != mAliasMap.end(); ++p)
  {
    NUNetElab* aliasNet = p->first;
    ClockAlias* clockAlias = p->second;
#ifdef CDB
    if (aliasNet == sAliasNetDebug)
      UtIO::cerr() << "Found net\n";
#endif


    // The master node defines where the storage should go, because
    // the others will be dead.  So keep track of that before aliasing
    // possibly corrupts it.  (bug1650)
    logMasterStorage(clockAlias);

    if (clockAlias->isAlias())
    {
      for (UInt32 i = 0; i < clockAlias->mDrivers.size(); ++i)
      {
        FLNodeElab* driver = clockAlias->mDrivers[i];
        FLNodeElab* fanout = clockAlias->mFanout[i];

        if ((fanout != NULL) && (deadFanout.find(fanout) == deadFanout.end()))
        {
	  bool faninEdge = fanout->isFanin( driver, eSensEdge );
	  bool faninLevel = fanout->isFanin( driver, eSensLevel );
          if ( faninEdge || faninLevel )
            fanout->removeFanin(driver);

          for (NUNetElab::DriverLoop q = clockAlias->mMaster->loopContinuousDrivers();
               !q.atEnd(); ++q) {
	    // Hookup fanin based on the type of fanin of the original.
	    // Needed for sync reset to work for testcases:
	    // cust/s3/columbia/carbon/cwtb/tb_c3d
	    // clock-aliasing/if_buffer.v
	    if( faninEdge )
              fanout->connectEdgeFanin(*q);
	    if( faninLevel )
              fanout->connectFanin(*q);
	  }
        }
        if (deadFanout.find(driver) == deadFanout.end())
        {
          mUtil->getFlowElabFactory()->destroy(driver);
          deadFanout.insert(driver);
        }
      }
      mBDDContext->deassign(aliasNet);
      //NU_ASSERT(!aliasNet->getNet()->isTemp(), aliasNet);

      aliasClockNets(clockAlias->mMaster, aliasNet);
      delete aliasNet;
    }
    delete clockAlias;
  } // for
}

void ClkAnalAlias::pruneUnreachableClocksAndMarkLiveFlow()
{
  FLNodeElab* flow;  
  UtHashSet<BDD, HashValue<BDD> > liveClocks;

  // Elaborated flow factory to get acyclic nodes
  FLNodeElabFactory* factory = mUtil->getFlowElabFactory();

  // See deadalias.v.  dclk2 is a clock master, but becomes dead because
  // it is only used to compute ddclk2, which is an alias of ddclk1 due
  // to a collapseClock directive in deadalias.directives.  To solve
  // this problem, after aliasing, do a dead-logic pass and prune out
  // any unreachable clocks.
  for (FLDesignElabIter iter(mDesign,
			     mUtil->getSymbolTable(),
			     FLIterFlags::eClockMasters,
			     FLIterFlags::eAll,
			     FLIterFlags::eNone, // Stop at nothing
			     FLIterFlags::eBranchAtAll,
			     FLIterFlags::eIterNodeOnce);
       not iter.atEnd();
       iter.next())
  {
    flow = *iter;
    NUUseDefNode* node = flow->getUseDefNode();
    flow->setScratchFlags(mIsLive);

    // Mark the acylic node live if this is part of a cycle
    FLNodeElab* acylicNode = factory->getAcyclicNode(flow);
    if (acylicNode != flow) {
      acylicNode->setScratchFlags(mIsLive);
    }

    if ((node) and (node->getType()==eNUAlwaysBlock)) {
      NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(node);
      NUAlwaysBlock * clockAlways = always->getClockBlock();
      if (not clockAlways) {
        clockAlways = always;
      }

      NUAlwaysBlockVector priorityTree;
      priorityTree.push_back(clockAlways);
      clockAlways->getClockBlockReferers(&priorityTree);

      for (NUAlwaysBlockVector::iterator iter = priorityTree.begin();
           iter != priorityTree.end();
           ++iter) {
        NUAlwaysBlock * priority = (*iter);
        NUEdgeExprList eelist;
        priority->getEdgeExprList(&eelist);
        for (NUEdgeExprList::iterator p = eelist.begin(); p != eelist.end(); ++p)
        {
          NUEdgeExpr* ee = *p;
          NUNet* localClkNet = ee->getNet();
          if (localClkNet != NULL)
          {
            NUNetElab* clk = localClkNet->lookupElab(flow->getHier());
            NU_ASSERT(clk, localClkNet);
            if (mBDDContext->isAssigned(clk)) // 'clk' in asyncreset/deadreset1b.v
            {
              BDD bdd = mBDDContext->bdd(clk);
              NU_ASSERT(bdd.isValid(), clk);
              if (mClocks.find(bdd) != mClocks.end()) {
                liveClocks.insert(bdd);
              }
            }
          }
        }
      } // for
    } // if
  } // for

  // Walk through all our clocks and remove any that are were not
  // reached in our design traversal.  Delay the deletion so we
  // Don't mutate the map we are iterating through.
  UtVector<BDD> deadClockBdds;
  for (ClockMap::iterator p = mClocks.begin(); p != mClocks.end(); ++p)
  {
    BDD clkBdd = p->first;
    if (liveClocks.find(clkBdd) == liveClocks.end()) {
      deadClockBdds.push_back(clkBdd);
    }
  }
  for (UInt32 i = 0; i < deadClockBdds.size(); ++i)
  {
    BDD clkBdd = deadClockBdds[i];
    ClockMap::iterator p = mClocks.find(clkBdd);
    INFO_ASSERT((p != mClocks.end()), "While deleting clocks, was unable to locate a clock for clkBdd");
    SCHClock* clock = p->second;
    NUNetElab* master = clock->getMaster();
    if (master->getNet()->isPrimaryPort() &&
        (clkBdd != mBDDContext->val0()) &&
        (clkBdd != mBDDContext->val1()))
    {
      IODBNucleus* iodb = mClkAnal->getIODB();
      if (clock->isEdge()) {
        iodb->declareClock(master->getSymNode());
      }
      else {
        iodb->declareClockTree(master->getSymNode());
      }
    }
    clock->clearBdds(mBDDContext);
    mClocks.erase(p);
  }
}


// Delete any dead flow that was used to complete clocks that were aliased away 
void ClkAnalAlias::pruneDeadClockFlows()
{

  FLNodeElabVector killFlows;
  FLNodeElab* flow = NULL;
#define DELETE_DEAD_NETS 1
#if DELETE_DEAD_NETS
  NUNetElabSet killNets;
#endif
  for (SCHUtil::AllFlowsLoop l = mUtil->loopAllFlowNodes(); l(&flow);)
  {
    if (flow->allScratchFlagsSet(mIsLive)) {
      flow->clearScratchFlags(mIsLive);
    } else {
      killFlows.push_back(flow);
      NUNetElab* net = flow->getDefNet();
      if (net != NULL)
      {
        net->removeContinuousDriver(flow);
#if DELETE_DEAD_NETS
        // Do not delete the NUNetElab for a primary port.  That leaves
        // testdriver in a bad state.
        if (net->getNet()->isPrimaryPort()) // search all aliases?
        {
          // Mark dead clocks as clocks so testdriver knows.  Otherwise, we
          // get mismatches on test/clock-collapse/kill_logic2.v
          if (mBDDContext->isAssigned(net))
          {
            BDD bdd = mBDDContext->bdd(net);
            ClockMap::iterator p = mClocks.find(bdd);
            if (p != mClocks.end())
            {
              SCHClock* clock = p->second;
              if (clock->isEdge())
                mClkAnal->getIODB()->declareClock(net->getSymNode());
            }
          }
        }
        else {
          killNets.insert(net);
        }
#endif
      }
    }
  }

#if DELETE_DEAD_NETS
  // We need to do another design walk to prune the killNets set
  // in case more than one flow def'd a net
  for (FLDesignElabIter iter(mDesign,
			     mUtil->getSymbolTable(),
			     FLIterFlags::eClockMasters,
			     FLIterFlags::eAll,
			     FLIterFlags::eNone, // Stop at nothing
			     FLIterFlags::eBranchAtAll,
			     FLIterFlags::eIterNodeOnce);
       not iter.atEnd();
       iter.next())
  {
    flow = *iter;
    NUNetElab* net = flow->getDefNet();
    NUNetElabSet::iterator p = killNets.find(net);
    if (p != killNets.end())
    {
      killNets.erase(p);
    }
  }

  // kill the nets first, then the flows
  for (NUNetElabSet::iterator p = killNets.begin();
       p != killNets.end(); ++p)
  {
    NUNetElab* net = *p;
    UtString buf;
    net->compose(&buf, NULL, true);   // include the top level
    bool removeFromIODB = true;
    if (mBDDContext->isAssigned(net))
    {
      // If this is a dead driver to a live net then kill the driver
      // but let the net live
      BDD netBdd = mBDDContext->bdd(net);
      ClockMap::iterator p = mClocks.find(netBdd);
      if (p != mClocks.end())
      {
        SCHClock* clock = p->second;

        // This clock might have been used to compute another clock
        // which was proved redundant.
        if (clock->getMaster() == net)
        {
          clock->clearBdds(mBDDContext);
          mClocks.erase(p);
          if (mVerbose)
            UtIO::cout() << "Deleting master net " << buf << "\n";
        }
        else
        {
          aliasClockNets(clock->getMaster(), net);
          removeFromIODB = false;
          if (mVerbose)
            UtIO::cout() << "Deleting alias net " << buf << "\n";
        }
      }
      else if (mVerbose)
        UtIO::cout() << "Deleting bdd but non-clock net " << buf << "\n";
    }
    else if (mVerbose)
      UtIO::cout() << "Deleting non-bdd net " << buf << "\n";
    mBDDContext->deassign(net);
    IODBNucleus* iodb = mClkAnal->getIODB();
    if (removeFromIODB)
    {
      STAliasedLeafNode* node = net->getSymNode();
      NU_ASSERT(node, net);
      iodb->removeNet(node);
    }
    else
      // The net was not aliased, we have to keep it around
      delete net;
  } // for
  IODBNucleus* iodb = mClkAnal->getIODB();
  iodb->flushRemovedNets();
#endif

  UtSet<NUCycle*> killCycles;
  for (UInt32 i = 0; i < killFlows.size(); ++i)
  {
    flow = killFlows[i];
    NUCycle* cycle = flow->getCycle();
    if (cycle)
      killCycles.insert(cycle);
    mUtil->getFlowElabFactory()->destroy(flow);
  } // for

  // When a flow in a cycle becomes dead, it means all flows in the cycle
  // must be dead, and the cycle structure which references the dead flow
  // must be deleted.
  for (UtSet<NUCycle*>::iterator c = killCycles.begin(); c != killCycles.end(); ++c)
  {
    NUCycle* cycle = *c;
    mClkAnal->getMarkDesign()->deleteCycleNode(cycle);
  } // for
}

// Sanitize the merged alias rings so only one node has storage
void ClkAnalAlias::cleanupAliasRings()
{
  for (MasterStorage::iterator p = mMasterStorage.begin();
       p != mMasterStorage.end(); ++p)
  {
    STAliasedLeafNode* storage = p->second;
    storage->setThisStorage();
  }
}


void ClkAnalAlias::doAliasing( )
{
  UtString buf;

  preClkAliasSanityCheck();

  // Clock aliasing is a series of design walks.  The first one validates
  // the set of candidate clock-aliases.  This first walk does not expand
  // through initial blocks, but then we need to do another one that does,
  // because initial blocks may have fanin that reaches clock nodes (that's
  // probably pessimistic, but we do need to fix up those pointers.


  // Clock aliasing is done in a series of steps:
  // 1. start by assuming that every clock signal is an alias candidate then
  // do two design walks to disqualify any clock that should not be an
  // alias. During this walk complete the population of ClockAlias in mAliasMap.
  //    a. design walk 1 (disqualify some candidates w/o looking in initial
  //                      blocks), populates mAliasMap with any valid candidates
  //
  //    b. design walk 2 determine mFanout and mDrivers for each qualified alias
  //                     candidate in mAliasMap.
  //                     Considers fanin/fanout from initial blocks
  //                     Some candidates might also be disqualified in this walk.
  //
  // mAliasMap now holds the valid ClockAlias candidates.
  //
  // 2. Process each entry of mAliasMap, making the necessary alias
  //    substitution, and deleting the dead flow.
  //
  // 3. Now that alias have been created, do a cleanup step that removes any
  //    clocks that are not reachable.
  // 
  // 4. remove any flows that were found to be dead
  // 
  // 5. Examine each alias ring (clocks) and make sure that only one node in
  //    ring has storage.


  // design walk 1a
  findAliasCandidates();
  // mAliasMap is now populated with candidates

  // design walk 1b
  findAliasFanoutInfo();


  // actually do the alias operation (also deletes dead flow)
  makeAliasSubstitution();

  // remove any unreachable clocks and sets the mIsLive scratch flag on flows that are live
  pruneUnreachableClocksAndMarkLiveFlow();

  // Delete any dead flow identified in previous step (mIsLive not set)
  pruneDeadClockFlows();

  // Sanitize the merged alias rings so only one node has storage
  cleanupAliasRings();

}
