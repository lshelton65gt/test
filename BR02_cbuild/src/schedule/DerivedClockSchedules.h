// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  Class to hold data and functions to create derived clock logic schedules.
*/

#ifndef _DERIVEDCLOCKSCHEDULES_H_
#define _DERIVEDCLOCKSCHEDULES_H_

class RETransform;
class SCHBranchNets;

#include "AddBuffer.h"
#include "util/GraphDot.h"
#include "util/GraphQueue.h"
#include "util/GraphTransform.h"
#include "util/GraphDumper.h"
#include "BlockFlowNodes.h"

//! class SCHDerivedClockSchedules
/*! This class holds the data and functions to create the derived
 *  clock logic schedules. This process involves creating sequential
 *  and combinational schedules so it uses the SCHSequentialSchedules
 *  and SCHCombinationalSchedules classes.
 */
class SCHDerivedClockSchedules
{
public:
  //! constructor
  SCHDerivedClockSchedules(SCHCreateSchedules* create,
			   SCHScheduleFactory* factory,
			   SCHMarkDesign* markDesign,
                           SCHScheduleData* scheduleData,
			   SCHTimingAnalysis* timingAnalysis,
			   SCHCombinationalSchedules* combScheds,
			   SCHSequentialSchedules* seqScheds,
			   SCHUtil* util, SCHSchedule* sched);

  //! destructor
  ~SCHDerivedClockSchedules();

  //! Create the derived clock logic schedules
  bool create(const char* fileRoot);

  //! Schedule a node into a derived clock logic schedule
  void scheduleDerivedNode(FLNodeElab*, bool, SCHDerivedClockLogic*);

private:
  //! Compute the set of input nets a dcl schedule is sensitive to
  const SCHInputNets* computeBranchNets(SCHBranchNets*, SCHDerivedClockBase* dcl);

  //! Test if this memory is too large (can be buffered or not)
  bool isLargeMem(FLNodeElab* flow) const;

  // Abstractions to store information about flow problems
  typedef SCHAddBuffer::BufferedFlowsMap ClkAndPIFlowMap;
  typedef UtSet<FLNode*> BufferedFlows;

  // Class to print clk/pi buffer messages
  class MsgCallback : public SCHAddBuffer::Callback
  {
  public:
    //! constructor
    MsgCallback(BufferedFlows& clkFlows, BufferedFlows& piFlows) :
      mClkFlows(clkFlows), mPIFlows(piFlows),
      mClkReason("clock logic used as data"),
      mPIReason("PI used in derived clock logic with -noInputFlow")
    {}

    //! destructor
    ~MsgCallback() {};

    //! Routine to get the reason
    const UtString* getReason(FLNodeElab* fanout, FLNodeElab* fanin) const;

  private:
    // Routine to check if the unelaborated flows are in the fixed set
    bool isInBufferedFlows(BufferedFlows& fixedFlows, FLNode* fanin) const;

    BufferedFlows& mClkFlows;
    BufferedFlows& mPIFlows;
    UtString mClkReason;
    UtString mPIReason;
  }; // class MsgCallback : SCHAddBuffer::Callback

  // Data and functions used to figure out when we have to buffer some
  // DCL flow
  typedef UtMap<FLNodeElab*, SCHDerivedClockLogic*> FlowToDCLScheduleMap;
  typedef LoopMap<FlowToDCLScheduleMap> FlowToDCLScheduleMapLoop;
  FlowToDCLScheduleMap mFlowToDCLScheduleMap;
  void markAllDCLFlows(void);
  void markDCLFlows(SCHDerivedClockLogic* dcl);
  void markDCLFlow(FLNodeElab* flowElab, SCHDerivedClockLogic* dcl);
  void clearAllDCLFlows(void);
  SCHDerivedClockLogic* findFlowSchedule(FLNodeElab* flowElab) const;

  // Find all derived clocks that are used as data.

  // Find and possible fix all sequential nodes that use clocks as data
  void findAndFixClkAndPIFlow();
  void findClkAndPIFlow(ClkAndPIFlowMap&, BufferedFlows&, BufferedFlows&);
  void addDelay(BufferedFlows& bufferedFlows, FLNodeElab* fanin);

  // Find and mark all nodes that are used to generate a derived clock
  bool calculateDerivedClockLogic();

  // Call back base class to walk the derived clock logic. This is an
  // argument to the walkDerivedClockLogic routine.
  class FlowCallback
  {
  public:
    //! constructor
    FlowCallback() {}

    //! virtual destructor
    virtual ~FlowCallback() {}

    //! Visit an elaborated flow that should be in a DCL schedule
    virtual void visit(FLNodeElab* flowElab) = 0;

    //! Check if an elaborated flow has been visitited before
    virtual bool isCovered(FLNodeElab* flowElab) const = 0;

    //! Check if all elaborated flow for a sequential block should be visited
    virtual bool visitFullSeqBlock(void) const = 0;
  };

  // Callback to find all the elaborated flow that should be in a DCL
  // schedule
  class FindFlowCallback : public FlowCallback
  {
  public:
    //! constructor
    FindFlowCallback(FLNodeElabSet* elabFlows);

    //! destructor
    ~FindFlowCallback();

    //! Visit an elaborated flow that should be in a DCL schedule
    /*! Adds the elaborated flow to the set provided by the caller
     */
    void visit(FLNodeElab* flowElab);

    //! Check if an elaborated flow has been visitited before
    /*! Checks if we have already added this elaborated flow to our set
     */
    bool isCovered(FLNodeElab* flowElab) const;

    //! Check if all elaborated flow for a sequential block should be visited
    /*! In this pass we return false because we are trying to find the
     *  minimum set.
     */
    bool visitFullSeqBlock(void) const;

  private:
    //! Storage for the elaborated flow in a DCL schedule
    FLNodeElabSet* mElabFlows;
  }; // class FindFlowCallback : public FlowCallback

  // Callback to mark all the elaborated flow that should be in a DCL
  // schedule
  class MarkFlowCallback : public FlowCallback
  {
  public:
    //! constructor
    MarkFlowCallback();

    //! destructor
    ~MarkFlowCallback();

    //! Visit an elaborated flow that should be in a DCL schedule
    /*! Marks the elaborated flow as derived clock or derived clock logic
     */
    void visit(FLNodeElab* flowElab);

    //! Check if an elaborated flow has been visitited before
    /*! Checks if we have already marked this elaborated flow
     */
    bool isCovered(FLNodeElab* flowElab) const;

    //! Check if all elaborated flow for a sequential block should be visited
    /*! In this pass we return true because we have tried to split everything
     *  we could.
     */
    bool visitFullSeqBlock(void) const;
  }; // class MarkFlowCallback : public FlowCallback

  // Routines to walk derived clock logic
  void walkDerivedClockLogic(FlowCallback* callback);
  void walkDerivedClockLogicHelper(FLNodeElab* flowElab,
                                   const NUNetElab* clkElab,
                                   FlowCallback* callback);
  void walkBlockDerivedClockLogic(FLNodeElab*, FlowCallback*);
  void walkFlowsClockLogic(const FLNodeElabVector& nodes, FlowCallback*);

  // Split derived clock logic flow from other flow in the same block
  bool splitDerivedClockLogic(const FLNodeElabSet& flowElabs);

  // Calculate an ordering between all the derived clock logic schedules
  void calculateDerivedClockLogicOrdering();
  void verify();

  // Schedule derived nodes
  void scheduleDesignDerivedNodes(void);
  void scheduleDerivedNodes(SCHDerivedClockLogic* dcl,
                            const SCHBlockFlowNodes& blockFlowNodes);
  bool isValidDerivedNode(FLNodeElab* flowElab);

  // Find the nets that are computed after they are used in a DCL
  // cycle
  typedef UtSet<SCHDerivedClockLogic*> DCLSet;
  void findCycleBreakNets(void);
  void findBreakNets(SCHDerivedClockCycle* dclCycle,
                     SCHDerivedClockLogic* dcl,
                     const DCLSet& dclSet);
  void findBreakNet(SCHDerivedClockCycle* dclCycle, int thisDepth,
                    FLNodeElab* flowElab, const DCLSet& dclSet);

  // The derived schedule which has sequential and combinational sub schedules
  typedef UtMap<const NUNetElab*,SCHDerivedClockBase*,NUNetElabCmp> SchedMap;
  typedef LoopMap<SchedMap> SchedMapLoop;
  SchedMap mSchedules;

  // Used to keep track of ids for the derived clock schedules
  int mIDs;
  int nextID() { return ++mIDs; }
  int numIDs() const { return mIDs+1; }

  // Used to access general scheduler data and functions
  SCHCreateSchedules* mCreate;
  SCHScheduleFactory* mScheduleFactory;
  SCHMarkDesign* mMarkDesign;
  SCHScheduleData* mScheduleData;
  SCHCombinationalSchedules* mCombinationalSchedules;
  SCHSequentialSchedules* mSequentialSchedules;
  SCHUtil* mUtil;
  SCHTimingAnalysis* mTimingAnalysis;
  SCHSchedule* mSched;

  // Functions to manage the visit map
  bool nodeVisited(FLNodeElab* flow, SCHDerivedClockLogic* dcl)
  {
    DCLSet& dclSet = mVisitMap[flow];
    return dclSet.find(dcl) != dclSet.end();
  }
  void markVisited(FLNodeElab* flow, SCHDerivedClockLogic* dcl)
  {
    DCLSet& dclSet = mVisitMap[flow];
    dclSet.insert(dcl);
  }

  // Types and data to deal with DCL ordering
  typedef UtHashMap<FLNodeElab*, DCLSet> VisitMap;
  VisitMap mVisitMap;

  // vector to help us produce derived clock error message
  FLNodeElabVector mClockCycle;

  class FlowGraphDotWriter : public GraphDotWriter
  {
  public:
    //! constructor
    FlowGraphDotWriter(const UtString& filename, SCHUtil* util) :
      GraphDotWriter(filename), mUtil(util)
    {}

    //! destructor
    ~FlowGraphDotWriter() {}

  private:
    SCHUtil* mUtil;
    void writeLabel(Graph* g, GraphNode* node);
    void writeLabel(Graph* g, GraphEdge* edge);
  };

  // Types, data and functions to deal with DCL cycles. The edge types
  // are in order from least to highest preference to keep the edge

  // Class to write out a DOT picture of the DCL graph
  class DCLGraphDotWriter : public GraphDotWriter
  {
  public:
    //! constructor
    DCLGraphDotWriter(const UtString& filename, SCHUtil* util) :
      GraphDotWriter(filename), mUtil(util)
    {}

    //! destructor
    ~DCLGraphDotWriter() {}

  private:
    SCHUtil* mUtil;
    void writeLabel(Graph* g, GraphNode* node);
    void writeLabel(Graph* g, GraphEdge* edge);
  };

  //! Must be a friend to convert and edge type to a string
  friend class DCLGraphDotWriter;

  // Types, data and functions to deal with DCL cycles. The edge types
  // are in order from least to highest preference to keep the edge
  // when breaking cycles.
  enum EdgeType
  {
    eEdgeComb,    //!< Combinational path, highest preference to keep
    eEdgeHighestPreference = eEdgeComb,
    eEdgeDiv,     //!< Divider sequential path, medium preference to keep
    eEdgeSeq,     //!< Non-divider sequential path, least preference to keep
    eEdgeLowestPreference = eEdgeSeq,
    eEdgeNone,    //!< Used by the acyclic graph
    eNumEdgeTypes //!< The number of edge types
  };

  //! Internal function to convert a DCL cycle edge to a representative string
  static const char* getEdgeString(EdgeType type)
  {
    const char* edgeStrings[eNumEdgeTypes] = { "C", "D", "S", "?" };
    return edgeStrings[type];
  }

  // The following types and functions are used to create a graph with
  // elaborated flow nodes as the data.
  typedef GenericDigraph<EdgeType, FLNodeElab*> FlowGraph;

  typedef GenericDigraph<EdgeType, FLNodeElabSet*> DCLFlowGraph;
  typedef UtMap<FLNodeElab*, DCLFlowGraph::Node*> FlowGraphMap;
  typedef std::pair<DCLFlowGraph::Node*, DCLFlowGraph::Node*> Edge;
  typedef UtMap<Edge, EdgeType> EdgeTypeMap;
  typedef LoopMap<EdgeTypeMap> EdgeTypeMapLoop;
  typedef std::pair<FLNodeElab*, EdgeType> FlowEdgePair;
  typedef UtSet<FlowEdgePair> GraphCovered;
  void populateDCLGraphRecurse(DCLFlowGraph::Node* fanoutNode,
                               FLNodeElab* flowElab, DCLFlowGraph* graph,
                               FlowGraphMap* flowGraphMap,
                               EdgeTypeMap* edgeTypeMap,
                               GraphCovered* covered);
  void populateDCLGraphFromClk(DCLFlowGraph::Node* fanoutNode,
                               const NUNetElab* clkElab,
                               DCLFlowGraph* graph,
                               FlowGraphMap* flowGraphMap,
                               EdgeTypeMap* edgeTypeMap,
                               EdgeType edgeType,
                               GraphCovered* covered);
  void populateDCLGraph(DCLFlowGraph::Node* fanoutNode, FLNodeElab* faninElab,
                        DCLFlowGraph* graph, FlowGraphMap* flowGraphMap,
                        EdgeTypeMap* edgeTypeMap,
                        EdgeType edgeType,
                        GraphCovered* covered);
  void getDCLFlows(const NUNetElab* clkElab, FLNodeElabVector* flows) const;
  DCLFlowGraph* createDCLFlowGraph();
  void destroyDCLFlowGraph(DCLFlowGraph* flowGraph);

  void createSequentialCycleGraphs(UtList<FlowGraph*>* cycles,
                                   bool writeDotFiles = false);
  FlowGraph* createFlowGraph();

  // Abstraction to map SCC components to the schedule that represents
  // them.
  typedef UtMap<const GraphSCC::Component*, SCHDerivedClockBase*> CompScheds;


  // Class to sort entire graphs by their size and flow contents
  class FlowGraphOrder
  {
  public:
    bool operator()(FlowGraph* fg1, FlowGraph* fg2)
    {
      // compare first by size
      int cmp = fg1->size() - fg2->size();
      if (cmp < 0)
        return true;
      else if (cmp > 0)
        return false;
      
      // compare next by contents
      FLNodeElab* flow1 = leastFlow(fg1);
      FLNodeElab* flow2 = leastFlow(fg2);
      ptrdiff_t cmp1 = FLNodeElab::compare(flow1,flow2);
      if (cmp1 < 0)
        return true;
      else if (cmp1 > 0)
        return false;
      
      // fall back on pointer comparison
      cmp = carbonPtrCompare(fg1,fg2);
      return (cmp < 0);
    }
  private:
    FLNodeElab* leastFlow(FlowGraph* graph)
    {
      FLNodeElab* least = NULL;
      for (Iter<GraphNode*> iter = graph->nodes(); !iter.atEnd(); ++iter)
      {
        FlowGraph::Node* node = graph->castNode(*iter);
        FLNodeElab* flow = node->getData();
        if ((least == NULL) || (FLNodeElab::compare(least,flow) < 0))
          least = flow;
      }
      return least;
    }
  };
  

  // Class to sort graph nodes by the corresponding derived clock
  // schedule.
  struct CmpComp
  {
    CmpComp(const CompScheds& compScheds) : mCompScheds(compScheds) {}
    bool operator()(const GraphSCC::Component* c1,
                    const GraphSCC::Component* c2) const
    {
      CompScheds::const_iterator pos = mCompScheds.find(c1);
      INFO_ASSERT(pos != mCompScheds.end(),
                  "Derived Clock Cycle Graph has been corrupted");
      const SCHDerivedClockBase* dcl1 = pos->second;
      pos = mCompScheds.find(c2);
      INFO_ASSERT(pos != mCompScheds.end(),
                  "Derived Clock Cycle Graph has been corrupted");
      const SCHDerivedClockBase* dcl2 = pos->second;
      return dcl1->compareSchedules(dcl2) < 0;
    }
    const CompScheds mCompScheds;
  };

  // The following types and functions are used to manage a flow graph
  // with SCHDerivedClockBase* as the node data.
  typedef GenericDigraph<EdgeType, SCHDerivedClockBase*> DCLGraph;
  DCLGraph* createDCLGraph();
  void printKey();
  void printClocks(DCLGraph* graph, int cycleNum);
  void printDCLCycles(const GraphSCC& graphSCC, const CompScheds&);
  void dumpDCLCycles(const char* fileRoot);
  void dumpDCLCycle(SCHDerivedClockCycle* cycle, int cycleNum, UtOStream& out);
  void dumpDCLSchedule(SCHDerivedClockLogic* dcl, UtOStream& out);
  void dumpElabNet(UtOStream& out, const char* prefix, const NUNetElab* netElab);

  // Abstraction for the base type
  typedef UtGraphTransform<GraphSCC::CompGraph, DCLGraph> SCCToDCLGraphBase;

  // Class to transform from a SCC graph to a DCL Graph
  class SCCToDCLGraph : public SCCToDCLGraphBase
  {
  public:
    //! constructor
    SCCToDCLGraph(GraphSCC::CompGraph* compGraph, CompScheds* compScheds) : 
      SCCToDCLGraphBase(compGraph), mCompScheds(compScheds)
    {}

    //! destructor
    ~SCCToDCLGraph() {}

    //! Function to translate the node data
    void mapNodeData(GraphSCC::Component*, SCHDerivedClockBase**, UInt32*);

    //! Function to translate the edge data
    bool mapEdgeData(GraphSCC::CompGraph::EdgeDataType,
                     DCLGraph::EdgeDataType* edgeData,
                     UInt32*)
    {
      *edgeData = eEdgeNone;
      return true;
    }

  private:
    CompScheds* mCompScheds;
  };

  // Abstraction for templatized base ttype
  typedef UtGraphTransform<DCLFlowGraph, DCLGraph> DCLFlowToSchedGraphBase;

  // Class to transform a DCL Flow Graph to a DCL Graph
  friend class DCLFlowToSchedGraph;
  class DCLFlowToSchedGraph : public DCLFlowToSchedGraphBase
  {
  public:
    //! constructor
    DCLFlowToSchedGraph(SCHDerivedClockSchedules* dclSchedules,
                        DCLFlowGraph* srcGraph);

    //! destructor
    ~DCLFlowToSchedGraph() {}

    //! Function to translate the node data
    void mapNodeData(DCLFlowGraph::NodeDataType srcData,
                     DCLGraph::NodeDataType*,
                     UInt32* flags);

    //! Function to translate the edge data
    bool mapEdgeData(DCLFlowGraph::EdgeDataType srcEdgeData,
                     DCLGraph::EdgeDataType* dstEdgeData,
                     UInt32*)
    {
      // They have the same edge data type and value
      *dstEdgeData = srcEdgeData;
      return true;
    }

  private:
    SCHDerivedClockSchedules* mDCLSchedules;
    typedef UtMap<DCLFlowGraph::NodeDataType, DCLGraph::NodeDataType> Cache;
    Cache mCache;
  }; // class DCLFlowToSchedGraph : public DCLFlowToSchedGraphBase

  // Types to order the derived clock schedules
  struct ScratchData
  {
    SIntPtr mData;                       //!< Data for the graph queue
    int     mEdgeCounts[eNumEdgeTypes];  //!< Counts of each incident edge type
  };
  class DCLCompareNode : public UtGraphQueue::NodeCmp
  {
  public:
    //! constructor
    DCLCompareNode(DCLGraph* graph) : mGraph(graph)
    {}

    //! destructor
    ~DCLCompareNode() {};

    //! compare two graph nodes
    bool operator()(const GraphNode* n1, const GraphNode* n2) const;

  private:
    DCLGraph* mGraph;
  }; // class DCLCompareNode : public UtGraphQueue::NodeCmp
  typedef std::pair<GraphNode*, GraphEdge*> FullEdge;
  class DCLCompareBreakableEdges : public UtGraphQueue::EdgeCmp
  {
#if ! pfGCC_2
    using UtGraphQueue::EdgeCmp::operator();
#endif
  public:
    //! constructor
    DCLCompareBreakableEdges(DCLGraph* graph) : mGraph(graph)
    {}

    //! destructor
    ~DCLCompareBreakableEdges() {};

    //! compare two graph edges
    bool operator()(const FullEdge& e1, const FullEdge& e2) const;

  private:
    DCLGraph* mGraph;
  };
  typedef UtVector<FullEdge> BreakEdges;
  class DCLGraphQueue : public UtGraphQueue
  {
  public:
    //! constructor
    DCLGraphQueue(bool verbose) :
      mDCLCycle(NULL), mGraph(NULL), mNodeOrder(NULL), mEdgeOrder(NULL),
      mVerbose(verbose)
    {}

    //! destructor
    ~DCLGraphQueue();

    //! Initialize the queue to sequence a new graph
    void init(DCLGraph* graph, SCHDerivedClockCycle* dclCycle);

  protected:
    //! Schedule a node at a given depth
    void scheduleNode(GraphNode* node, SInt32 depth);

    //! Break a cycle
    void breakCycle();

    //! Store data in the node
    void putNodeData(GraphNode* node, SIntPtr data);

    //! Get data from a node
    SIntPtr getNodeData(GraphNode* node) const;

    //! Initialize a graph node
    void initGraphNode(GraphNode* node);

    //! Initialize a graph edge
    bool initGraphEdge(GraphNode* from, GraphEdge* edge, GraphNode* to);

  private:
    SCHDerivedClockCycle* mDCLCycle;
    DCLGraph* mGraph;
    DCLCompareNode* mNodeOrder;
    UtGraphQueue::EdgeCmp* mEdgeOrder;
    BreakEdges* mBreakEdges;
    bool mVerbose;
  }; // class DCLGraphQueue : public UtGraphQueue

  //! Must be a friend to call function to convert an edge type to a string
  friend class DCLGraphQueue;

  // These are used to create a derived clock logic schedule
  void createDCLCycleSchedules(DCLGraph* graph, const GraphSCC& graphSCC,
                               Graph* compGraph, CompScheds* compScheds);
  SCHDerivedClockBase* buildDerivedClockCycle(const GraphSCC& graphSCC,
                                              GraphSCC::Component* comp);
  const SCHScheduleMask* computeMask(FLNodeElabSet* flowElabs);
  SCHDerivedClockBase* createDerivedClock(NUCNetElabSet& clocks,
                                          const SCHScheduleMask* mask);
  SCHDerivedClockBase* findDerivedClock(const NUNetElab* clk);
  void storeDerivedClock(SCHDerivedClockBase* dcl);
  void moveDerivedClock(SCHDerivedClockBase* dcl, SCHDerivedClockBase* newDcl);
  static SCHDerivedClockBase* getDCLSchedule(DCLGraph* graph, GraphNode* node);
  static void putDCLSchedule(DCLGraph*, GraphNode*, SCHDerivedClockBase*);
  static int getGraphID(DCLGraph* graph, GraphNode* node);
  void findInputNets(SCHDerivedClockLogic* dcl);
  void computeInputNets();

  // Work around a gcc 2.95 bug
public:
  SCHDerivedClockBase* buildDerivedClockLogic(FLNodeElabSet* flowElabs);
private:


  // Used to sort derived clocks by their depth
  void sortSchedules(DCLGraph* dclGraph);
  static int getDerivedClockDepth(SCHDerivedClockLogic* dcl);
  static bool lessDerivedClockDepth(SCHDerivedClockBase* dcl1,
				    SCHDerivedClockBase* dcl2);

  // Simplified version of routine to get a sequential nodes clock
  const NUNetElab* getSequentialNodeClock(FLNodeElab* flowElab);

  // Types and functions used to merge DCL schedules together
  typedef UtVector<GraphNode*> GraphNodes;
  typedef Loop<GraphNodes> GraphNodesLoop;
  typedef std::pair<const SCHScheduleMask*, const SCHInputNets*> DCLKey;
  typedef UtMap<DCLKey, GraphNodes*> MergableSchedules;
  typedef LoopMap<MergableSchedules> MergableSchedulesLoop;
  void addMergableSchedule(MergableSchedules*, DCLGraph*, GraphNode*);
  typedef UtMap<GraphNode*, DynBitVector*> TransClosure;
  typedef LoopMap<TransClosure> TransClosureLoop;
  DynBitVector* getFanoutMask(TransClosure*, DCLGraph*, GraphNode*);
  void mergeSubSchedules(DCLGraph*, GraphNodes*, TransClosure*);
  DCLGraph* mergeSchedules(DCLGraph* graph);
  static int getGraphId(DCLGraph* graph, GraphNode* node);
  void mergeNodes(DCLGraph* graph, GraphNodesLoop l);
  DCLGraph* cleanupGraph(DCLGraph* graph);

  // Class to compute transitive closure over a graph
  friend class TransClosureWalker;
  class TransClosureWalker : public GraphWalker
  {
  public:
    //! constructor
    TransClosureWalker(DCLGraph* graph, 
                       int (getGraphID)(DCLGraph*, GraphNode*),
                       TransClosure* closure, int numIDs) :
      mGraph(graph), mGetGraphID(getGraphID), mClosure(closure), 
      mNumIDs(numIDs)
    {}

    //! destructor
    ~TransClosureWalker() {}

    //! Before we visit the node, make sure we have allocated space for it
    Command visitNodeBefore(Graph* graph, GraphNode* node);

    //! After we visit, populate our callers information
    Command visitNodeAfter(Graph*, GraphNode* node)
    {
      popClosureData();
      applyClosureData(node);
      return GW_CONTINUE;
    }

    //! For cross edges, populate our callers information
    Command visitCrossEdge(Graph* graph, GraphNode*, GraphEdge* edge)
    {
      applyClosureData(graph->endPointOf(edge));
      return GW_CONTINUE;
    }

    //! The graph should be acyclic
    Command visitBackEdge(Graph* graph, GraphNode*, GraphEdge* edge)
    {
      GraphNode* node = graph->endPointOf(edge);
      DCLGraph::Node* dclNode = mGraph->castNode(node);
      SCHDerivedClockBase* dclBase = dclNode->getData();
      SCHED_ASSERT("DCL Graph should be acylic!" == NULL, dclBase);
      return GW_CONTINUE;
    }

  private:
    // A stack of data for nodes that are being visited.
    typedef UtVector<DynBitVector*> ClosureStack;
    ClosureStack mClosureStack;
    void pushClosureData(DynBitVector*);
    void popClosureData();

    // Function to apply data to the callers closure data. This is the
    // helper function for visitNodeAfter and visitCrossEdge
    void applyClosureData(GraphNode* node);

    // Information to create closure
    DCLGraph* mGraph;
    int (*mGetGraphID)(DCLGraph*, GraphNode*);
    TransClosure* mClosure;
    int mNumIDs;
  }; // class TransClosureWalker : public GraphWalker

  // The following type represents a single partition as we process
  // these sub schedules.
  class DCLPartition
  {
  public:
    //! constructor
    DCLPartition(int size) : mIDMask(size), mFanoutMask(size) {}

    //! destructor
    ~DCLPartition() {}

    //! Check if this node can be added if so add it
    bool addNode(GraphNode* node, int id, DynBitVector* fanoutMask);

    //! Get the size of the partition
    int size();

    //! Iterate over the nodes in the partition
    GraphNodesLoop loopNodes();

  private:
    GraphNodes    mMergeGroup;
    DynBitVector  mIDMask;
    DynBitVector  mFanoutMask;
  };

  // Types for cleaning up the DCL graph after merging.
  typedef UtGraphTransform<DCLGraph, DCLGraph> DCLGraphCleanupBase;
  class DCLGraphCleanup : public DCLGraphCleanupBase
  {
  public:
    //! constructor
    DCLGraphCleanup(DCLGraph* srcGraph);

    //! destructor
    ~DCLGraphCleanup() {}

    //! Function to translate the node data
    void mapNodeData(SCHDerivedClockBase*, SCHDerivedClockBase**, UInt32*);

    //! Function to translate the edge data
    bool mapEdgeData(DCLGraph::EdgeDataType, DCLGraph::EdgeDataType*, UInt32*);
  };

  // Structure to sort nodes
  struct SortNodes : public GraphSortedWalker::NodeCmp
  {
    SortNodes(DCLGraph* graph) : mGraph(graph) {}
    int compare(const GraphNode* n1, const GraphNode* n2) const 
    {
      const DCLGraph::Node* dclNode1 = mGraph->castNode(n1);
      const DCLGraph::Node* dclNode2 = mGraph->castNode(n2);
      SCHDerivedClockBase* dclBase1 = dclNode1->getData();
      SCHDerivedClockBase* dclBase2 = dclNode2->getData();
      return dclBase1->compareSchedules(dclBase2);
    }

    bool operator()(const GraphNode* n1, const GraphNode* n2) const
    {
      return compare(n1, n2) < 0;
    }

  private:
    DCLGraph* mGraph;
  };

  // Structure to sort edges
  struct SortEdges : public GraphSortedWalker::EdgeCmp
  {
    SortEdges(DCLGraph* graph) : mGraph(graph) {}
    bool operator()(const GraphEdge* e1, const GraphEdge* e2) const
    {
      // Sort by edge type first
      int cmp = 0;
      const DCLGraph::Edge* dclEdge1 = mGraph->castEdge(e1);
      const DCLGraph::Edge* dclEdge2 = mGraph->castEdge(e2);
      EdgeType type1 = dclEdge1->getData();
      EdgeType type2 = dclEdge2->getData();
      if (type1 < type2)
        cmp = -1;
      else if (type2 < type1)
        cmp = 1;
      else {
        // Compare by the destination node next
        GraphNode* n1 = mGraph->endPointOf(e1);
        GraphNode* n2 = mGraph->endPointOf(e2);
        SortNodes sortNodes(mGraph);
        cmp = sortNodes.compare(n1, n2);
      }
      return cmp < 0;
    }

  private:
    DCLGraph* mGraph;
  }; // struct SortEdges : public GraphSortedWalker::EdgeCmp
      
  // Class to walk the sorted graph
  class PrintWalker : public UtGraphDumper
  {
  public:
    //! constructor
    PrintWalker(DCLGraph* graph, SCHUtil* util);

    //! destructor
    ~PrintWalker();

  protected:
    //! Overloaded function to fill an edge string
    virtual void edgeString(Graph*, GraphNode*, GraphEdge*, UtString* str);

    //! Overloaded function to fill a node string
    virtual void nodeString(Graph*, GraphNode* node, UtString* str);

  private:
    // Utility classes
    DCLGraph* mGraph;
    SCHUtil* mUtil;
  }; // class PrintWalker : public GraphSortedWalker

  //! Must be a friend to call function to convert an edge type to a string
  friend class PrintWalker;

  //! Map to keep track of all elaborated flow per always block instance
  SCHBlockFlowNodes* mBlockFlowNodes;
}; // class SCHDerivedClockSchedules

#endif // _DERIVEDCLOCKSCHEDULES_H_
