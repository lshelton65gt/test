// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _DEPTH_H_
#define _DEPTH_H_

// The following defined some special depth values used in he various
// scheduling passes:
//
// - In cycle detection we use the depth to find real cycles. We use
//   the values DEPTH_UNINITIALIZED, DEPTH_START, and depths > 0 to
//   mark we are done.
//
// - In the sensitivity calculation we compute the actual depth of the
//   nodes grater than or equal to zero.
//
// - In creating derived clock logic schedules, we overload the depth
//   to indicate in which schedule a node is in. This way we can do
//   accurate detection of misused derived clocks.
//
// - In the sequential block ordering all sequential blocks are given
//   a depth of 0 or greater when the algorithm is done. This is used to
//   sort sequential blocks in a schedule.
#define DEPTH_UNINITIALIZED	-1	//!< Haven't set it yet

#define DEPTH_START		0	//!< The lowest depth is always 0

#define DEPTH_SEQ_DOUBLE_BUFFER	0	//!< Sequential blocks depth always
					// starts at 0 since the inputs to the
					// block are depth 0. But if we find a
					// sequential block with 0 as the depth
					// then it is part of a loop and we
					// will try to break it here.

#define DEPTH_SEQ_BUSY		-3	//!< We are searching through this node
					// (mark to avoid sequential cycles)
#define DEPTH_DERIVED_CYCLE     -4      //!< We have found a cycle in the
                                        // derived clock path and need to
                                        // produce a good error message
#endif // _DEPTH_H_
