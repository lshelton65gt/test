// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "schedule/Schedule.h"

#include "flow/FLNodeElab.h"

#include "CombTimingWalker.h"
#include "MarkDesign.h"
#include "BlockFlowNodes.h"

SCHCombTimingWalker::SCHCombTimingWalker(SCHMarkDesign* markDesign,
                                         SCHUtil* util) :
  mMarkDesign(markDesign), mUtil(util), mFlowGraph(NULL)
{
  mBlockFlowNodes = new SCHBlockFlowNodes(mUtil);
}

SCHCombTimingWalker::~SCHCombTimingWalker()
{
  delete mFlowGraph;
  delete mBlockFlowNodes;
}

void SCHCombTimingWalker::createFaninGraph(void)
{
  // Make sure the graph was not created before
  INFO_ASSERT(mFlowGraph == NULL, "Duplicate call to create a timing flow graph");

  // Create the combinational flow graph. The leafs are primary inputs
  // and sequential flow nodes.
  mFlowGraph = new CombFlowGraph;
  CombFlowNodeMap flowNodeMap;
  createCombFaninGraph(&flowNodeMap);
}


void SCHCombTimingWalker::createFanoutGraph(void)
{
  // Make sure the graph was not created before
  INFO_ASSERT(mFlowGraph == NULL, "Duplicate call to create a timing flow graph");

  // Create the combinational flow graph. The leafs are primary inputs
  // and sequential flow nodes.
  mFlowGraph = new CombFlowGraph;
  CombFlowNodeMap flowNodeMap;
  CombFlowNodeMap seqNodeMap;
  createCombFanoutGraph(&flowNodeMap, &seqNodeMap);
}


void SCHCombTimingWalker::walk(void)
{
  // This is only valid after the graph has been created
  INFO_ASSERT(mFlowGraph != NULL,
              "Trying to walk a timing flow graph that wasn't created");

  // Convert the graph to a strongly connected component graph. This
  // will treat cycles as a single unit.
  GraphSCC graphSCC;
  Graph* dag = graphSCC.buildComponentGraph(mFlowGraph);

  // Walk the acyclic graph and compute the input nets
  CombWalker walker(&graphSCC, mFlowGraph, this);
  walker.walk(dag);
}

void
SCHCombTimingWalker::createCombFaninGraph(CombFlowNodeMap* flowNodeMap)
{
  // Recursively visit the design starting from output ports and
  // sequentials and create the flow graph nodes and edges. Don't
  // visit the cyclic flow. We only create a graph of the acylic flow
  // (false passed to DesignIter)
  for (SCHMarkDesign::DesignIter i(mMarkDesign, false); !i.atEnd(); ++i) {
    // Add this node to the graph
    FLNodeElab* flowElab = *i;
    createCombFaninGraphNodes(flowElab, flowNodeMap);
  }
} // SCHCombTimingWalker::createCombFaninGraph

SCHCombTimingWalker::CombFlowNode*
SCHCombTimingWalker::createCombFaninGraphNodes(FLNodeElab* flowElab,
                                               CombFlowNodeMap* flowNodeMap)
{
  // If this flow node was done before, just return
  CombFlowNodeMap::iterator pos = flowNodeMap->find(flowElab);
  if (pos != flowNodeMap->end())
    return pos->second;

  // For c-models we don't want to break up the flow nodes for the
  // same c-model. Gather all of them. For others we create a node per
  // flow node in a block.
  FLNodeElabVector graphNodeFlows;
  getGraphFlows(flowElab, &graphNodeFlows);

  // Use the current flow node to represent the graph node
  CombFlowNode* node = new CombFlowNode(flowElab);
  mFlowGraph->addNode(node);

  // Add this graph node to the map for all flow nodes
  for (FLNodeElabVectorLoop l(graphNodeFlows); !l.atEnd(); ++l) {
    FLNodeElab* curFlow = *l;
    FLN_ELAB_ASSERT(flowNodeMap->find(curFlow) == flowNodeMap->end(), curFlow);
    flowNodeMap->insert(CombFlowNodeMap::value_type(curFlow, node));
  }

  // For combinational nodes, add the fanin to the graph
  if (!SCHUtil::isSequential(flowElab)) {
    // Visit the flow nodes fanin to recursively find all combinational
    // nodes
    UtSet<CombFlowNode*> covered;
    for (FLNodeElabVectorLoop l(graphNodeFlows); !l.atEnd(); ++l) {
      FLNodeElab* curFlow = *l;
      SCHMarkDesign::FlowTimingLoop f;
      f = mMarkDesign->loopTimingDependencies(curFlow, SCHMarkDesign::eInternal);
      for (; !f.atEnd(); ++f) {
        // Compute the graph node for this fanin
        FLNodeElab* faninElab = *f;
        CombFlowNode* fanin;
        fanin = createCombFaninGraphNodes(faninElab, flowNodeMap);
        
        // Add the edge if it is not the same node. It is superfluous
        // information to represent direct feedback edges
        if ((node != fanin) && (fanin != NULL)) {
          // Don't add an edge more than once
          if (covered.find(fanin) == covered.end())
          {
            covered.insert(fanin);
            CombFlowEdge* edge = new CombFlowEdge(fanin, NULL);
            node->addEdge(edge);
          }
        }
      }
    }
  }

  return node;
} // SCHCombTimingWalker::createCombFaninGraphNodes

void
SCHCombTimingWalker::createCombFanoutGraph(CombFlowNodeMap* flowNodeMap,
                                           CombFlowNodeMap* seqNodeMap)
{
  // Recursively visit the design starting from output ports and
  // sequentials and create the flow graph nodes and edges. Don't
  // visit the cyclic flow. We only create a graph of the acylic flow
  // (false passed to DesignIter)
  for (SCHMarkDesign::DesignIter i(mMarkDesign, false); !i.atEnd(); ++i) {
    // Add this node to the graph
    FLNodeElab* flowElab = *i;
    createCombFanoutGraphNodes(flowElab, flowNodeMap);

    // For sequentials, create a fanout version of the sequential, add
    // the fanin combo blocks to the graph and hook it up to this
    // special fanout sequential.
    if (SCHUtil::isSequential(flowElab)) {
      // Create the fanout sequential node
      CombFlowNodeMap::iterator pos = seqNodeMap->find(flowElab);
      CombFlowNode* seqFlowNode = NULL;
      if (pos != seqNodeMap->end()) {
        seqFlowNode = pos->second;
      } else {
        seqFlowNode = new CombFlowNode(flowElab);
        seqNodeMap->insert(CombFlowNodeMap::value_type(flowElab, seqFlowNode));
        mFlowGraph->addNode(seqFlowNode);
      }

      // Recurse creating the graph for the fanin combinational
      // nodes. But don't visit regular flow for inactive flops. They
      // are not scheduled.
      if (!flowElab->isInactive()) {
        for (Fanin f = mUtil->loopFanin(flowElab); !f.atEnd(); ++f) {
          FLNodeElab* faninElab = *f;
          createFanoutEdge(flowElab, faninElab, flowNodeMap, seqFlowNode);
        }

        // Recurse creating the graph for the edge fanin of this flop
        for (SCHMarkDesign::EdgeFaninLoop l = mMarkDesign->loopEdgeFanin(flowElab);
             !l.atEnd(); ++l) {
          FLNodeElab* faninElab = *l;
          createFanoutEdge(flowElab, faninElab, flowNodeMap, seqFlowNode);
        }
      }
    } // if
  } // for
} // SCHCombTimingWalker::createCombFanoutGraph

void
SCHCombTimingWalker::createFanoutEdge(FLNodeElab* flowElab,
                                      FLNodeElab* faninElab,
                                      CombFlowNodeMap* flowNodeMap,
                                      CombFlowNode* seqFlowNode)
{
  // Don't need to visit ourselves or sequential nodes. The
  // latter will be reached by the outer loop
  if (faninElab != flowElab) {
    // Add the fanin combo blocks
    CombFlowNode* fanin;
    fanin = createCombFanoutGraphNodes(faninElab, flowNodeMap);
          
    // Hook it up to the output sequential
    if (fanin != NULL) {
      FLN_ELAB_ASSERT(fanin != seqFlowNode, flowElab);
      CombFlowEdge* edge = new CombFlowEdge(seqFlowNode, NULL);
      fanin->addEdge(edge);
    }
  }
}

SCHCombTimingWalker::CombFlowNode*
SCHCombTimingWalker::createCombFanoutGraphNodes(FLNodeElab* flowElab,
                                                CombFlowNodeMap* flowNodeMap)
{
  // If this flow node was done before, just return
  CombFlowNodeMap::iterator pos = flowNodeMap->find(flowElab);
  if (pos != flowNodeMap->end())
    return pos->second;

  // For c-models we don't want to break up the flow nodes for the
  // same c-model. Gather all of them. For others we create a node per
  // flow node in a block.
  FLNodeElabVector graphNodeFlows;
  getGraphFlows(flowElab, &graphNodeFlows);

  // Use the current flow node to represent the graph node
  CombFlowNode* node = new CombFlowNode(flowElab);
  mFlowGraph->addNode(node);

  // Add this graph node to the map for all flow nodes
  for (FLNodeElabVectorLoop l(graphNodeFlows); !l.atEnd(); ++l) {
    FLNodeElab* curFlow = *l;
    FLN_ELAB_ASSERT(flowNodeMap->find(curFlow) == flowNodeMap->end(), curFlow);
    flowNodeMap->insert(CombFlowNodeMap::value_type(curFlow, node));
  }

  // For combinational nodes, add the fanout to the graph
  if (!SCHUtil::isSequential(flowElab)) {
    // Visit the flow nodes fanin to recursively find all combinational
    // nodes
    UtSet<CombFlowNode*> covered;
    for (FLNodeElabVectorLoop l(graphNodeFlows); !l.atEnd(); ++l) {
      FLNodeElab* curFlow = *l;
      SCHMarkDesign::FlowTimingLoop f;
      f = mMarkDesign->loopTimingDependencies(curFlow, SCHMarkDesign::eInternal);
      for (; !f.atEnd(); ++f) {
        // Compute the graph node for this fanin
        FLNodeElab* faninElab = *f;
        CombFlowNode* fanin;
        fanin = createCombFanoutGraphNodes(faninElab, flowNodeMap);

        // Add the edge if it is not the same node. It is superfluous
        // information to represent direct feedback edges
        if ((node != fanin) && (fanin != NULL)) {
          // Don't add an edge more than once
          if (covered.find(fanin) == covered.end())
          {
            covered.insert(fanin);
            CombFlowEdge* edge = new CombFlowEdge(node, NULL);
            fanin->addEdge(edge);
          }
        }
      }
    }
  }

  return node;
} // SCHCombTimingWalker::createCombFanoutGraphNodes

void
SCHCombTimingWalker::getGraphFlows(FLNodeElab* flowElab,
                                   FLNodeElabVector* graphNodeFlows) const
{
  if (flowElab->isEncapsulatedCycle()) {
    // We want to treat cycles as a single connected component
    mUtil->getAcyclicFlows(flowElab, graphNodeFlows);
    
  } else if (mMarkDesign->blockHasCModel(flowElab)) {
    // We want to treat all statements in blocks with c-models as a
    // single connected component. This is because we can't split them
    FLNodeElabVector blockNodes;
    mBlockFlowNodes->getFlows(flowElab, &blockNodes, &FLNodeElab::isLive);
    for (FLNodeElabVectorCLoop l(blockNodes); !l.atEnd(); ++l) {
      FLNodeElab* curFlow = *l;
      graphNodeFlows->push_back(curFlow);
    }
  } else {
    graphNodeFlows->push_back(flowElab);
  }
}

GraphWalker::Command
SCHCombTimingWalker::CombWalker::visitNodeBefore(Graph* graph, GraphNode* node)
{
  // Get the list of graph nodes for this component node.
  GraphSCC::Component* component = mGraphSCC->getComponent(node);
  
  // Walk the list of graph nodes and get the set the value from the
  // current nodes
  GraphSCC::Component::NodeLoop i;
  UIntPtr value = 0;
  for (i = component->loopNodes(); !i.atEnd(); ++i) {
    // Get the elaborated flow node that represents this component node
    GraphNode* compNode = *i;
    CombFlowNode* node = mFlowGraph->castNode(compNode);
    FLNodeElab* flowElab = node->getData();

    // Get all the elaborated flows that this graph node represents
    FLNodeElabVector graphNodeFlows;
    mCombTimingWalker->getGraphFlows(flowElab, &graphNodeFlows);

    // Visit all the elaborated flows for this component
    for (FLNodeElabVectorLoop l(graphNodeFlows); !l.atEnd(); ++l) {
      // Compute the value of this sub component
      FLNodeElab* curFlow = *l;
      UIntPtr subValue = mCombTimingWalker->getValue(curFlow);

      // Merge the values. We assume that if the current accumulated
      // value is 0, we can just clobber it with the sub value. This
      // catches the first case here
      if (value == 0) {
        value = subValue;
      } else {
        value = mCombTimingWalker->mergeValues(value, subValue);
      }
    }
  } // for

  // Store the information in the scratch pointer for the component
  graph->setScratch(node, value);

  // Initialize the last child value in anticipation of visiting the
  // children of this graph node.
  copyToWalker(0);

  return GW_CONTINUE;
} // SCHCombTimingWalker::CombWalker::visitNodeBefore

GraphWalker::Command
SCHCombTimingWalker::CombWalker::visitNodeAfter(Graph* graph, GraphNode* node)
{
  // Merge the value from the last child
  UIntPtr value = mergeFaninValue(graph, node);

  // Get the list of graph nodes for this component node.
  GraphSCC::Component* component = mGraphSCC->getComponent(node);
  
  // Walk the list of graph nodes and store the value for all the
  // nodes
  UIntPtr resultValue = 0;
  GraphSCC::Component::NodeLoop i;
  for (i = component->loopNodes(); !i.atEnd(); ++i) {
    // Get the elaborated flow node that represents this component node
    GraphNode* compNode = *i;
    CombFlowNode* node = mFlowGraph->castNode(compNode);
    FLNodeElab* flowElab = node->getData();

    // Get all the elaborated flows that this graph node represents
    FLNodeElabVector graphNodeFlows;
    mCombTimingWalker->getGraphFlows(flowElab, &graphNodeFlows);

    // Visit all the elaborated flows for this component
    for (FLNodeElabVectorLoop l(graphNodeFlows); !l.atEnd(); ++l) {
      // Store the value for this elaborated flow. It is finished
      FLNodeElab* curFlow = *l;
      UIntPtr subValue = mCombTimingWalker->storeValue(curFlow, value);

      // Compute the value to return to the parent node
      if (resultValue == 0)
        resultValue = subValue;
      else
        resultValue = mCombTimingWalker->mergeValues(resultValue, subValue);
    } // for
  } // for

  // Store it for our fanout
  graph->setScratch(node, resultValue);
  copyToWalker(resultValue);

  return GW_CONTINUE;
} // SCHCombTimingWalker::CombWalker::visitNodeAfter

GraphWalker::Command
SCHCombTimingWalker::CombWalker::visitNodeBetween(Graph* graph,
                                                  GraphNode* node)
{
  // Merge the value from the last child
  mergeFaninValue(graph, node);

  return GW_CONTINUE;
}
        
GraphWalker::Command
SCHCombTimingWalker::CombWalker::visitCrossEdge(Graph* graph, GraphNode*,
                                                GraphEdge* edge)
{
  // Copy the fanins data to the last child
  GraphNode* faninNode = graph->endPointOf(edge);
  UIntPtr value = graph->getScratch(faninNode);
  copyToWalker(value);

  return GW_CONTINUE;
}
        
GraphWalker::Command
SCHCombTimingWalker::CombWalker::visitBackEdge(Graph*, GraphNode* node, GraphEdge*)
{
  CombFlowNode* combNode = mFlowGraph->castNode(node);
  FLNodeElab* flowElab = combNode->getData();
  FLN_ELAB_ASSERT("Graph should be acylic" == NULL, flowElab);
  return GW_CONTINUE;
}
        
UIntPtr
SCHCombTimingWalker::CombWalker::mergeFaninValue(Graph* graph, GraphNode* node)
{
  UIntPtr value = graph->getScratch(node);
  value = mCombTimingWalker->mergeValues(value, mLastChildData);
  graph->setScratch(node, value);
  return value;
}

void SCHCombTimingWalker::CombWalker::copyToWalker(UIntPtr value)
{
  mLastChildData = value;
}
