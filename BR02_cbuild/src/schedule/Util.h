// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _SCHEDUTIL_H_
#define _SCHEDUTIL_H_

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUNetSet.h"
#include "flow/FLNodeElabCycle.h"
#include "flow/FLFactory.h"
#include "util/LoopFunctor.h"
#include "util/LoopMulti.h"
#include "util/LoopThunk.h"

#include "schedule/ScheduleTypes.h"

class Split;
class ESFactory;
class SCHSchedule;
class Stats;
class SCHEvent;
class SCHScheduleFactory;
class SCHClkAnal;
class IODBNucleus;
class STSymbolTable;
class SCHScheduleMask;
class SCHScheduleFactory;
class SCHDerivedClockBase;
class MsgContext;
class NUNetRefFactory;
class RETransform;
class ArgProc;
class ESPopulateExpr;
class CbuildSymTabBOM;

//! Abstraction for schedule control flags
enum SCHScheduleFlags
{
  eUseSample = 0x1,         //!< Use sample based scheduling
  eSortByData = 0x2,	    //!< Try to schedule to improve d-cache locality
  eDisableInitCombos = 0x4, //!< Don't schedule combos in initial schedule
  eDumpMultiSched = 0x8,    //!< Dump blocks that are in multiple schedules
  eDumpUnmerged = 0x10,	    //!< Dump blocks with multiple nodes in a schedule
  eUnused1 = 0x20,          //!< Available
  eNoInputFlow = 0x40,	    //!< NO LONGER USED
  eSplitBlocks = 0x80,	    //!< Attempt to split blocks when possible
  eDumpSplitBlocks = 0x100, //!< Dump the blocks that were split/not split
  eFixClocksAsData = 0x200, //!< If set, we try to fix clocks used as data
  eMergeBlocks = 0x400,	    //!< Merges blocks to reduce call overhead
  eDumpClocksAsData = 0x800,//!< Dump all the clocks used as data
  eDumpStateUpdate = 0x1000,//!< Dump the set of nets that are state update
  eDumpTransitionNodes=0x2000,//!< Dump the nodes executed with transition
  eNoClockEquivalence=0x4000,//!< Disables the clock equivalence analysis
  eDumpPIFlowFix=0x8000,    //!< Dumps the spots where we insert buffers to
			    //!< delay a PI into a DCL sequential node
  eResolveBidis=0x10000,    //!< If on, bidis are run in async schedule
  eDumpBufferedSequentials=0x20000, //!< Dump buffers added between sequentials
  eDumpCycles=0x40000,      //!< Dump all cyclic flow to stdout
  eBreakCycles=0x80000,     //!< Try to break cycles up into non-looping schedules
  eNoSelfCycles=0x100000,   //!< If set, don't treat a net feedback as a cycle
  eNoMergeDCL=0x200000,     //!< If set, we don't merge DCL schedules
  eNoDebugSchedule=0x400000,//!< If set, we don't create a debug schedule
  eDumpCyclesEarly=0x800000,//!< Dump all cyclic flow to stdout before eliminating cycles
  eNoMixedMerge=0x1000000,  //!< Disable mixed block merging
  eSanity=0x2000000,        //!< Run sanity passes at various points
  eMemorySanity=0x4000000,  //!< Run the expensive memory allocation sanity
  eDumpCycleBdds=0x8000000  //!< Dump the graph cycle BDDs
};


//! class SCHUtil
/*!
 * This class contains utility functions to the scheduling
 * routines. It is a class so that all scheduling classes can use this
 * data.
 */
class SCHUtil
{
public:
  //! constructor
  SCHUtil(SCHSchedule* sched, 
          SCHScheduleFactory* factory,
          FLNodeFactory* flowFactory,
          FLNodeElabFactory* flowElabFactory,
          Split* split,
          IODBNucleus* iodb,
          STSymbolTable* st,
          MsgContext* mc,
          NUNetRefFactory* netref_factory,
          RETransform* transform,
          ArgProc* args,
          ESPopulateExpr* populateExpr,
          SourceLocatorFactory* srcloc_factory);
  
  //! destructor
  ~SCHUtil();

  static inline bool isSequential(const NUUseDefNode* useDef)
  {
    return ((useDef != NULL) && useDef->isSequential());
  }

  static inline bool isSequential(const FLNodeElab* flow)
  {
    const NUUseDefNode* useDef = flow->getUseDefNode();
    return isSequential(useDef);
  }

  static inline bool isInitial(const NUUseDefNode* useDef)
  {
    return ((useDef != NULL) && useDef->isInitial());
  }

  static inline bool isInitial(const FLNodeElab* flow)
  {
    const NUUseDefNode* useDef = flow->getUseDefNode();
    return isInitial(useDef);
  }

  static inline bool isPriorityBlock(FLNodeElab* flow)
  {
    const NUUseDefNode* useDef = flow->getUseDefNode();
    return isPriority(useDef);
  }

  static inline bool isPriority(const NUUseDefNode* useDef)
  {
    return ((useDef != NULL) && useDef->isPriorityBlock());
  }

  static inline bool isContDriver(const NUUseDefNode* useDef)
  {
    return ((useDef == NULL) || useDef->isContDriver());
  }

  static inline bool isContDriver(const FLNodeElab* flowElab)
  {
    // For some reason NUCycle returns they are not a continous driver
    if (flowElab->isEncapsulatedCycle()) {
      return true;
    }

    // We should treat bound nodes as continous drivers, they are not
    // nested
    if (flowElab->isBoundNode()) {
      return true;
    }

    const NUUseDefNode* useDef = flowElab->getUseDefNode();
    return isContDriver(useDef);
  }

  static inline bool lessFlow(const FLNodeElab* f1, const FLNodeElab* f2)
  {
    return FLNodeElab::compare(f1, f2) < 0;
  }

  //! Get all the nets for a given always block.
  static inline void getBlockNets(FLNodeElab* flow, NUNetSet* defs)
  {
    NUUseDefNode* useDef = flow->getUseDefNode();
    if (useDef->useBlockingMethods())
    {
      NUUseDefStmtNode* stmtNode = dynamic_cast<NUUseDefStmtNode*>(useDef);
      stmtNode->getBlockingDefs(defs);
      // no non-blocking defs required -- this only happens after reordering
    }
    else
    {
      useDef->getDefs(defs);
    }
  }

  //! Checks if a flow node is for a cmodel
  static bool isCModel(FLNodeElab* flow);

  //! Get the original/real FLNodeElab* for a FLNodeElabCycle*
  FLNodeElab* getCycleOriginalNode(FLNodeElab* flow) const;

  //! Determine if two flow nodes belong to the same block instance.
  /*! This includes sequential blocks and combinational blocks.
   */
  static bool sameBlock(FLNodeElab* flow1, FLNodeElab* flow2);

  //! Determine if a fanin dependency in the flow graph is real
  /*!
   * It is assumed that the 'fanin' flow is in the fanin set of
   * 'flow'. All fanin arcs between different blocks are assumed to be
   * real dependencies.
   * 
   * If the two flow nodes are in the same block, check to see if any
   * live defs of the 'fanin' net are not visible to the definitions
   * of the 'flow' net.
   */
  static bool dataDependencyIsReal(FLNodeElab * flow, FLNodeElab * fanin);

  //! Create a clock edge event
  /*! First by getting the canonical form of the clock, and also
   *  mutating posedge(~clock) into negedge(clock).
   */
  const SCHEvent* buildClockEdge(const NUNetElab* clk, ClockEdge edge,
                                 UInt32 priority = 0);

  //! Get the actual clock pin.
  /*! This should be used to get the real clock pin for this
   *  sequential node but not to get the clock we should branch
   *  on. This does not call the clock analysis code to get the clock
   *  master.
   */
  void getSequentialClockPin(FLNodeElab*, ClockEdge*, NUNetElab**);

  //! Helper function to get the priority block for a sequential block
  static NUAlwaysBlock* getPriorityBlock(NUUseDefNode* useDef);

  //! Helper function to get the clock block for a sequential block
  static NUAlwaysBlock* getClockBlock(NUUseDefNode* useDef);

  //! Routine to get a new schedule pass. Make sure it doesn't wrap
  UInt32 bumpSchedulePass()
  {
    ++mSchedulePass;
    INFO_ASSERT(mSchedulePass != 0,
                "Schedule pass counter overflowed");
    return mSchedulePass;
  }

  //! Checks if a schedule pass is equal to the current pass
  bool equalSchedulePass(UInt32 pass) const { return mSchedulePass == pass; }

  //! Returns the current schedule pass
  UInt32 getSchedulePass() const { return mSchedulePass; }

  //! Mark all the flows that drive the net for a flow node
  void markNetFlows(FLNodeElab* flow, UInt32 pass);

  //! Mark all the flows that drive the net for a flow node
  void markNetFlows(FLNodeElab* flow) { markNetFlows(flow, mSchedulePass); }

  //! Set the schedule control flags
  void putScheduleFlags(SCHScheduleFlags flags)
  {
    mFlags = flags;
    mFlagsSet = true;
  }

  //! Check if a combinational scheduling flag is set
  bool isFlagSet(SCHScheduleFlags flag) const
  {
    INFO_ASSERT(mFlagsSet,
                "Test for a scheduling option before they have been gathered");
    return (mFlags & flag) != 0;
  }

  //! Set the dump level for block mergining
  void putDumpMergedBlocks(int level) { mDumpMergedBlocks = level; }

  //! Get the dump level for block merging
  int getDumpMergedBlocks(void) const { return mDumpMergedBlocks; }

  Split * getSplit() { return mSplit; }
  NUDesign * getDesign() { return mDesign; }
  void setDesign(NUDesign * design) { mDesign = design; }

  // A functor which maps FLNodeElabs to FLNodeElabCycles,
  // paramaterizable by flow factory
  struct CycleMapper {
    typedef FLNodeElab* value_type;
    typedef FLNodeElab* reference;
    CycleMapper(FLNodeElabFactory* factory = 0): mFactory(factory) {}
    FLNodeElab* operator()(FLNodeElab* flow) const;
    FLNodeElabFactory* mFactory;
  };

  typedef LoopFunctor<NUNetElab::DriverLoop, CycleMapper> DriverLoop;
  typedef LoopFunctor<NUNetElab::ConstDriverLoop,
                      CycleMapper> ConstDriverLoop;

  DriverLoop loopNetDrivers(NUNetElab* net);
  ConstDriverLoop loopNetDrivers(const NUNetElab* net) const;

  //! get the drivers of a net, including those in an always block
  /*! 
   *! Note that if there is more than one driver within an always
   *! block then this routine does not really produce any useful
   *! data, as it cannot tell whether the drivers are in the same
   *! block or not.  So it returns false if this is the case, and
   *! leaves drivers empty
   */
  bool getBlockNetDrivers(NUNetElab*, FLNodeElab* flow,
                          FLNodeElabSet* drivers,
                          FlowCycleMode);

  //! Set the statistics class
  /*! If set, the scheduler sub classes can measure themselves. If it
   *  is NULL, the switch to turn on the measurment is not there.
   */
  void putStats(Stats* stats) { mStats = stats; }

  //! Get the statistics class for measuring compile resources
  Stats* getStats() const { return mStats; }

  //! Get the flow factory
  FLNodeFactory* getFlowFactory() const { return mFlowFactory; }

  //! Get the flow elab factory
  FLNodeElabFactory* getFlowElabFactory() const { return mFlowElabFactory; }

  //! Is this flow node part of a combinational cycle?
  bool isInCycle(FLNodeElab* flow) const
  {
    return mFlowElabFactory->isInCycle(flow);
  }

  //! Get the FLNodeElabCycle for a given flow node
  FLNodeElab* getAcyclicNode(FLNodeElab* flow) const
  {
    return mFlowElabFactory->getAcyclicNode(flow);
  }

  //! Abstraction to loop over all elaborated flow nodes
  typedef FLNodeElabFactory::FlowLoop AllFlowsLoop;

  //! Return an iterator over all flow nodes in the design
  /*! Watch out because there are both live and dead flow nodes in
   *  this walk.
   */
  AllFlowsLoop loopAllFlowNodes() const
  {
    return mFlowElabFactory->loopFlows();
  }

  //! Used to keep track of all the flow nodes for a given block
  typedef UtMap<const NUUseDefNode*, FLNodeElabSet> BlockToNodesMap;

  //! Function to create a map from all usedef nodes to all flow node elabs
  /*! This function does a walk of the flow node elab factory to find
   *  all use def nodes and the flow node elabs for them. This should
   *  be used sparingly.
   *
   *  This call is necessary in order to call loopAllBlockNodes below.
   */
  void findAllBlockNodes(BlockToNodesMap&);

  //! Abstraction to visit all the flow nodes for a given block
  typedef FLNodeElabSet::SortedLoop BlockNodesLoop;

  //! Loop through the set of flow nodes for a given block
  /*! This routine returns all the flow nodes for all hierarchies for
   *  all defs of a block.
   */
  BlockNodesLoop loopAllBlockNodes(BlockToNodesMap&, const NUUseDefNode*);

  //! Get the symbol table
  /*! The SCHUtil class acts as an interface to all external classes
   *  that are useful in scheduling.
   */
  STSymbolTable* getSymbolTable() const { return mSymbolTable; }

  //! Get the IODB
  /*! The SCHUtil class acts as an interface to all external classes
   *  that are useful in scheduling.
   */
  IODBNucleus* getIODB() const { return mIODB; }

  //! Get the msg context
  /*! The SCHUtil class acts as an interface to all external classes
   *  that are useful in scheduling.
   */
  MsgContext* getMsgContext() const { return mMsgContext; }

  //! Get the argument processor
  /*! The SCHUtil class acts as an interface to all external classes
   *  that are useful in scheduling.
   */
  ArgProc* getArgs() const { return mArgs; }

  //! Get the transformation class
  /*! The SCHUtil class acts as an interface to all external classes
   *  that are useful in scheduling.
   */
  RETransform* getTransform() const { return mTransform; }

  //! Get the expression population class
  ESPopulateExpr* getPopulateExpr() const { return mPopulateExpr; }

  //! get the netref factory
  NUNetRefFactory* getNetRefFactory() const {return mNetRefFactory;}

  //! get the source locator factory
  SourceLocatorFactory* getSourceLocatorFactory() const {return mSourceLocatorFactory;}

  //! Check if two flow nodes are in the same sequential always block
  static bool sameSequentialBlock(FLNodeElab* flow1, FLNodeElab* flow2);

  //! Iterator over the fanin for a flow node
  Fanin loopFanin(FLNodeElab* flow, FlowCycleMode cycleMode = eFlowOverCycles,
		  unsigned int additionalFlags = 0,
		  unsigned int nestingFlags = 0, bool sorted = true);

  //! Check if an elaborated flow represents and active driver
  bool isActiveDriver(FLNodeElab* flowElab) const;

  //! Check if an elaborated net has active drivers
  bool hasActiveDrivers(const NUNetElab* netElab) const;

  //! Get the BOM Manager
  const CbuildSymTabBOM* getBOMManager() const;
  //! non-const bom manager
  CbuildSymTabBOM* getBOMManager()
  {
    const SCHUtil* me = const_cast<const SCHUtil*>(this);
    return const_cast<CbuildSymTabBOM*>(me->getBOMManager());
  }

  //! Find the enabled driver structure within the code for a given flow node
  NUEnabledDriver* findEnabledDriver(FLNodeElab* flow) const;

  //! Get a net ref for an elaborated flow now
  NUNetRefHdl getDefNetRef(FLNodeElab* flow) const;

  //! Get all the acyclic flows for an encapsulated cycle
  void getAcyclicFlows(const NUCycle* cycle, FLNodeElabVector* flows) const;

  //! Get all the acyclic flows for an encapsulated cycle
  void getAcyclicFlows(FLNodeElab* flowElab, FLNodeElabVector* flows) const;

  //! Returns true if the flow is a pull driver
  bool isPullDriver(FLNodeElab* flow) const;

  bool isInputMask(const SCHScheduleMask* mask) const;

  //! Get the input mask
  const SCHScheduleMask* getInputMask(void) const;

  //! Get the output mask
  const SCHScheduleMask* getOutputMask(void) const;

  //! Get the constant mask
  const SCHScheduleMask* getConstantMask(void) const;

  //! Tests if this sequential elaborated flow is a clock divider
  /*! This is currently not a precise test. It tests to see if the
   *  output is also used in the always block. We can improve this
   *  over time.
   */
  bool isClockDivider(FLNodeElab* flow) const ;

  // Structure to generate flow loops from clock elaborated nets
  struct FlowLoopGen
  {
    FlowLoopGen() : mUtil(NULL) {}
    FlowLoopGen(const SCHUtil* util) : mUtil(util) {}
    ConstDriverLoop operator()(const NUNetElab* clkElab);
    const SCHUtil* mUtil;
  };

  //! Iterator over all the elaborated flows for a derived clock
  typedef LoopThunk<SCHClocksLoop,
                    ConstDriverLoop,
                    FlowLoopGen> DerivedClockFlowsLoop;

  //! Loop over all the elaborated flows for a derived clock
  DerivedClockFlowsLoop loopDerivedClockFlows(SCHDerivedClockBase* dclBase)
    const;

  //! Set the buffered memory threshold
  void putBufferedMemoryThreshold(int threshold)
  {
    mBufferedMemoryThreshold = threshold;
  }

  //! Get the buffered memory threshold
  int getBufferedMemoryThreshold() const { return mBufferedMemoryThreshold; }

  //! Get an elaborated clock from a symbol table node
  const NUNetElab* getClock(const STSymbolTableNode* node) const;

  //! Test if a set of sequential elaborated flow for one usedef node is mergable
  bool sequentialBlockMergable(const NUUseDefNode* useDef, const FLNodeElabVector* flows);

  //! Test if a set of combinational elaborated flow for one usedef node is mergable
  bool combinationalBlockMergable(const NUUseDefNode* useDef, const FLNodeElabVector* flows,
                                  const NUUseDefSet& cycleBlocks);

  //! Set whether or not we are coercing (by default, we do)
  void putNoPortCoercion(bool noCoerce) { mNoPortCoercion = noCoerce; }

  //! Returns true if port coercion was turned off.
  bool isPortCoercionOff() const { return mNoPortCoercion; }
  
  typedef UtHashMap<NUNet*, NUNetElabSet*> NetToNetElabMap;
  //! Find all onDemand state nets (sequential/transition scheduled) in a flow
  /*!
    Loops over a flow node and its fanin and nested blocks, creating a
    map of all NUNets that are sequential or transition scheduled.
    Each NUNet is mapped to the corresponding set of NUNetElabs for
    each flow node.  These NUNets comprise the OnDemand state elements
    for the model.  A similar map of user-excluded nets is also
    created.

    Codegen uses this data when emitting OnDemand save/restore code
    and member declarations.  Only the nets in the included set are
    used.  The set of user-excluded nets is used to determine if any
    OnDemand state needs to be excluded at runtime.  This occurs if
    some elaborated instaces of a net are excluded while others are
    not.

    \param fl_elab The elaborated flow node
    \param ondemand_nets The map used to track OnDemand state nets
    \param excluded_nets The map used to track explicitly excluded nets
    \param covered_nodes The set of visited nodes, checked to avoid infinite recursion
    \param check_type If false, the flow type won't be checked (necessary when called recursively on nested flow)
   */
  void findOnDemandNets(FLNodeElab *fl_elab, NetToNetElabMap *ondemand_nets, NetToNetElabMap *excluded_nets, FLNodeElabSet *covered_nodes, bool check_type);

  //! Add a net and net elab to a NetToNetElabMap, allocating a new set if necessary.
  static void addOnDemandNet(NetToNetElabMap *netMap, NUNet *net, NUNetElab *netElab);

  //! Erase this key from the map, freeing the allocated value first.
  static void removeOnDemandNet(NetToNetElabMap *netMap, NUNet *net);

private:
  //! Checks if a flow node is for a cmodel call
  static bool isCModelRecurse(FLNodeElab* flow, FLNodeElabSet* covered);

  void getBlockNetDriversHelper(NUNetElab*, FLNodeElab* flow,
                                FLNodeElabSet* drivers,
                                FlowCycleMode,
                                FLNodeElabSet* covered,
                                UInt32* num_non_continuous_drivers);

  // Used for the various schedule passes
  UInt32 mSchedulePass;

  // Scheduling options
  SCHScheduleFlags mFlags;
  bool mFlagsSet;
  int mBufferedMemoryThreshold;
  SInt32 mDumpMergedBlocks;

  // Information about the compile 
  bool mNoPortCoercion;

  // Empty flow list for an any driverloop
  FLNodeElabList mEmptyFlowList;

  // Access to data structures
  SCHSchedule* mSched;
  SCHScheduleFactory* mFactory;
  FLNodeFactory* mFlowFactory;
  FLNodeElabFactory* mFlowElabFactory;
  Stats* mStats;
  IODBNucleus* mIODB;
  Split * mSplit;
  NUDesign * mDesign;
  STSymbolTable* mSymbolTable;
  MsgContext* mMsgContext;
  NUNetRefFactory* mNetRefFactory;
  RETransform* mTransform;
  ArgProc* mArgs;
  ESPopulateExpr* mPopulateExpr;
  SourceLocatorFactory* mSourceLocatorFactory;
}; // class SCHUtil

#endif // _SCHEDUTIL_H_
