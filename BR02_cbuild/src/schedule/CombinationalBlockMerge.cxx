// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implements combinational block merging
*/

#include "nucleus/NUCycle.h"
#include "nucleus/NUAssign.h"

#include "schedule/Schedule.h"
#include "Util.h"
#include "Dump.h"
#include "MarkDesign.h"
#include "ScheduleData.h"
#include "MergeTest.h"
#include "CombinationalBlockMerge.h"

SCHCombinationalBlockMerge::SCHCombinationalBlockMerge(SCHUtil* util,
                                                       SCHMarkDesign* mark,
                                                       SCHScheduleData* data) :
  SCHBlockMerge(util, mark, data, false)
{
  std::memset(mAdjMergeFails, 0, sizeof(mAdjMergeFails));
  mGroupToScheduleMap = new GroupToScheduleMap;
  mPrintedFailedMerges = new PrintedFailedMerges;
  mMergeTest = new SCHMergeTest(mark, NULL, false, true);
  mPartialMultiDefLatches = new PartialMultiDefLatches;
}

SCHCombinationalBlockMerge::~SCHCombinationalBlockMerge()
{
  delete mGroupToScheduleMap;
  delete mPrintedFailedMerges;
  delete mMergeTest;
  delete mPartialMultiDefLatches;
}

void SCHCombinationalBlockMerge::merge()
{
  bool doMerge = mUtil->isFlagSet(eMergeBlocks);
  if (doMerge) {
    // Initialize merging
    initLocalData();

    // Do the actual merge by calling the SCHBlockMerge class to do it
    int dumpBlockMergeInfo = mUtil->getDumpMergedBlocks();
    if (dumpBlockMergeInfo != 0) {
      UtIO::cout() << "Merging combinational blocks:\n";
    }
    mergeBlocks(dumpBlockMergeInfo);

    // Print why some blocks are un mergable
    if (dumpBlockMergeInfo != 0) {
      printResults();
    }
  }
}

void SCHCombinationalBlockMerge::initLocalData()
{
  // Call the SCHBlockMerge::start function to have it initialize itself
  start();

  // Add all the scheduled blocks to the merge process
  SCHSchedulesLoop p;
  for (p = mScheduleData->loopAllCombinationals(); !p.atEnd(); ++p) {
    // Get the data for the next combinational schedule
    SCHCombinational* comb = p.getCombinationalSchedule();
    addSchedule(comb);
  }
} // SCHCombinationalBlockMerge::initLocalData

void SCHCombinationalBlockMerge::addSchedule(SCHCombinational* comb)
{
  // Go through all the combo blocks and add them to our database.
  SCHCombinational::BlockScheduleLoop sLoop;
  for (sLoop = comb->getBlockSchedule(); !sLoop.atEnd(); ++sLoop)
  {
    // Create the block
    FLNodeElabVector* nodes = *sLoop;
    FLNodeElab* oneFlow = *nodes->begin();
    createBlock(oneFlow, nodes, comb);

    // Add this group to the group to schedule map
    FLN_ELAB_ASSERT(mGroupToScheduleMap->find(nodes) == mGroupToScheduleMap->end(),
                    oneFlow);
    mGroupToScheduleMap->insert(GroupToScheduleMap::value_type(nodes, comb));
  } // for
} // SCHBlockMerge::addSchedule

void SCHCombinationalBlockMerge::printResults()
{
  // Print info about starting mergable vs unmergable blocks
  mMergeTest->printStats();

  // Print info about adjacency failures
  int count = (mAdjMergeFails[eUnmergable] +
               mAdjMergeFails[eCrossesHier] +
               mAdjMergeFails[eUnmatchHierSched] +
               mAdjMergeFails[eMultiDefPartialLatch]);
  if (count > 0) {
    UtIO::cout()
      << "Failed adjacency blocks:"
      << "\n  Unmergable      : " << mAdjMergeFails[eUnmergable]
      << "\n  CrossesHier     : " << mAdjMergeFails[eCrossesHier]
      << "\n  UnmatchHierSched: " << mAdjMergeFails[eUnmatchHierSched]
      << "\n  MultiDefPartLat : " << mAdjMergeFails[eMultiDefPartialLatch]
      << "\n";
  }
} // void SCHCombinationalBlockMerge::printResults

bool
SCHCombinationalBlockMerge::edgeMergable(const MergeEdge* edge,
                                         const MergeBlock* fanoutBlock,
                                         const MergeBlock* faninBlock,
                                         bool dumpMergedBlocks) const
{
  // The fanout block must be mergable, otherwise give up
  if (!fanoutBlock->isMergable()) {
    return false;
  }

  // Check if the fanin is mergable
  AdjMergeFail reason = eNumAdjMergeFails;
  bool mergable = false;
  if (!faninBlock->isMergable()) {
    // Can't merge with an unmergable block
    reason = eUnmergable;

  } else if (edge->getCrossesHierarchy()) {
    // The blocks are in different hierarchy
    reason = eCrossesHier;

  } else if (MergeBlock::compareHierScheduleSets(fanoutBlock, faninBlock) != 0) {
    // The block/faninBlock instances are in different schedules or
    // hierarchies
    reason = eUnmatchHierSched;

  } else if (isMultiDefPartialLatch(fanoutBlock) ||
             isMultiDefPartialLatch(faninBlock)) {
    reason = eMultiDefPartialLatch;

  } else {
    mergable = true;
  }

  // Update the stats and return if it is mergable
  if (!mergable) {
    if (dumpMergedBlocks) {
      recordFailedMerge(fanoutBlock, faninBlock, reason);
    }
    return false;
  } else {
    return true;
  }
} // SCHCombinationalBlockMerge::edgeMergable

int
SCHCombinationalBlockMerge::compareBuckets(const MergeBlock* block1,
                                           const MergeBlock* block2) const
{
  // Compare by their hierarchy and schedule sets
  return MergeBlock::compareHierScheduleSets(block1, block2);
}

static inline bool
isConstAssign(const NUUseDefNode* useDef, IODBNucleus* iodb)
{
  const NUContAssign* assign = dynamic_cast<const NUContAssign*>(useDef);
  return (assign != NULL) && assign->isConstantAssign(iodb);
}

void
SCHCombinationalBlockMerge::getBlockFanin(MergeBlock* block, BlockSet* faninSet)
  const
{
  // Gather the flow nodes in this block so we can avoid adding arcs
  // to the same block. We can't do that using elaborated flow because
  // we may break up an always block into separate mergable/unergable
  // blocks.
  FLNodeElab* flowElab;
  FLNodeElabSet blockFlows;
  for (SCHFlowGroups::FlowsLoop f = block->loopFlowNodes(); !f.atEnd(); ++f) {
    // Visit this flows fanin to find dependencies
    flowElab = *f;
    blockFlows.insert(flowElab);
  }

  // Check if this is a cycle. There is a performance bug if we use
  // the code below. A cycle should be an unmerable block with a
  // single group. We can then get the fanin for a single flow
  // node. Otherwise the code below will do an N*M traversal where N
  // is the number of nodes in the cycle and M is the number of fanins
  // for the cycle.
  if (flowElab->isEncapsulatedCycle()) {
    getBlockFlowFanin(flowElab, blockFlows, faninSet);
  } else {
    // Visit the elaborated flow to create the block merging flow
    NU_ASSERT(faninSet->empty(), block->getRepresentativeUseDefNode());
    for (SCHFlowGroups::FlowsLoop f = block->loopFlowNodes(); !f.atEnd(); ++f) {
      FLNodeElab* flowElab = *f;
      getBlockFlowFanin(flowElab, blockFlows, faninSet);
    }
  }
} // void SCHCombinationalBlockMerge::getBlockFanin

void
SCHCombinationalBlockMerge::getBlockFlowFanin(FLNodeElab* flow, 
                                              const FLNodeElabSet& blockFlows,
                                              BlockSet* faninSet) const
{
  // Visit this flows fanin to find dependencies
  SCHMarkDesign::FlowDependencyLoop p;
  for (p = mMarkDesign->loopDependencies(flow); !p.atEnd(); ++p) {
    // Make sure we aren't adding a self cycle unless the two blocks
    // are different instances. Also check if this isn't a false
    // dependency need to keep some latches live.
    FLNodeElab* fanin = *p;
    if ((blockFlows.find(fanin) == blockFlows.end()) ||
        !SCHUtil::sameBlock(flow, fanin)) {
      // Walk the blocks for this node
      for (BlocksLoop l = findFlowBlocks(fanin); !l.atEnd(); ++l) {
        // Add this as fanin if it is not ourselves
        MergeBlock* faninBlock = *l;
        faninSet->insert(faninBlock);
      }
    }
  } // for
} // void SCHCombinationalBlockMerge::getBlockFlowFanin

// Test whether this block is mergable. There are various reasons why
// a block cannot be merged all on its own. This does not mean the
// block is mergable with other blocks because there are reasons for
// which two blocks cannot be merged. That is not handled here.
//
// This routine has two types of tests. It starts with tests that
// affect the block itself (use def node). Then there are tests for
// the flows that make up the block.
bool
SCHCombinationalBlockMerge::blockMergable(const NUUseDefNode* useDef,
                                          MergeBlock* block) const
{
  return mMergeTest->mergable(useDef, block->getGroups());
} // SCHCombinationalBlockMerge::blockMergable

void 
SCHCombinationalBlockMerge::recordFailedMerge(const MergeBlock* fanoutBlock,
                                              const MergeBlock* faninBlock,
                                              AdjMergeFail reason) const
{
  // Gather the elaborated flow for both blocks and sort them
  FLNodeElabVector fanoutNodes;
  SCHBlockMerge::gatherBlockFlows(fanoutBlock, &fanoutNodes);
  FLNodeElabVector faninNodes;
  SCHBlockMerge::gatherBlockFlows(faninBlock, &faninNodes);
  std::sort(fanoutNodes.begin(), fanoutNodes.end(), SCHUtil::lessFlow);
  std::sort(faninNodes.begin(), faninNodes.end(), SCHUtil::lessFlow);

  // Now that they are sorted, pick elaborated flow to represent the
  // pair (any is fine)
  FLNodeElab* fanoutElab = fanoutNodes.back();
  FLNodeElab* faninElab = faninNodes.back();

  // Check if we have printed for this pair before (when the fanin
  // block is umergable this can happen since we create a block for
  // every scheduled instance).
  PrintPair pair(fanoutElab, faninElab);
  if (mPrintedFailedMerges->find(pair) != mPrintedFailedMerges->end()) {
    return;
  }
  mPrintedFailedMerges->insert(pair);

  // Record this as a new failed merge
  ++mAdjMergeFails[reason];

  // Print the fanout block
  UtIO::cout() << "Failed merge for Fanout Block:\n";
  printBlockNodes(fanoutNodes);

  // Print the fanin block
  UtIO::cout() << "Fanin Block (reason: " << adjFailReason(reason) << "):\n";
  printBlockNodes(faninNodes);
}

const char*
SCHCombinationalBlockMerge::adjFailReason(AdjMergeFail adjMergeFail) const
{
  const char* reason = NULL;
  switch(adjMergeFail) {
    case eUnmergable:
      reason = "Unmergable";
      break;

    case eCrossesHier:
      reason = "CrossesHier";
      break;

    case eUnmatchHierSched:
      reason = "UnmatchHierSched";
      break;

    case eMultiDefPartialLatch:
      reason = "MultiDefPartialLatch";
      break;

    default:
      INFO_ASSERT(0, "Unknown adjacent merge fail reason");
      break;
  }

  return reason;
}

void
SCHCombinationalBlockMerge::printBlockNodes(const FLNodeElabVector& nodes) const
{
  for (FLNodeElabVectorIter f = nodes.begin(); f != nodes.end(); ++f)	{
    FLNodeElab* flow = *f;
    UtString flowName;
    SCHDump::composeFlowName(flow, &flowName);
    UtIO::cout() << "  " << flowName << "\n";
  }
}

void
SCHCombinationalBlockMerge::postMergeCallback(MergeBlock* lastBlock, 
                                              MergeBlock* block)
{
  // Put the first set of groups in a map from schedule and hierarchy
  // pair to the group. There should only be one group for each
  // schedule and hierarchy.
  typedef std::pair<const SCHCombinational*, const STBranchNode*> Key;
  typedef UtMap<Key, FLNodeElabVector*> GroupMap;
  GroupMap groupMap;
  const NUUseDefNode* useDef = NULL;
  for (SCHFlowGroupsLoop g = lastBlock->loopGroups(); !g.atEnd(); ++g) {
    // Gather the data
    FLNodeElabVector* nodes = *g;
    FLNodeElab* flow = *nodes->begin();
    const STBranchNode* hier = flow->getHier();
    SCHCombinational* comb = (*mGroupToScheduleMap)[nodes];
    FLN_ELAB_ASSERT(comb != NULL, flow);

    // Store the block so we can make sure we are merging the right
    // flow nodes
    if (useDef == NULL)
      useDef = flow->getUseDefNode();
    else
      FLN_ELAB_ASSERT(useDef == flow->getUseDefNode(), flow);

    // Insert the information
    Key key(comb, hier);
    FLN_ELAB_ASSERT(groupMap.find(key) == groupMap.end(), flow);
    groupMap.insert(GroupMap::value_type(key, nodes));
  }

  // Copy the nodes from the second block to the first block
  for (SCHFlowGroupsLoop g = block->loopGroups(); !g.atEnd(); ++g) {
    // Figure out which group it should be in
    FLNodeElabVector* nodes2 = *g;
    FLNodeElab* flow = *nodes2->begin();
    const STBranchNode* hier = flow->getHier();
    SCHCombinational* comb = (*mGroupToScheduleMap)[nodes2];
    FLN_ELAB_ASSERT(comb != NULL, flow);
    FLN_ELAB_ASSERT(useDef == flow->getUseDefNode(), flow);

    // Get the group
    Key key(comb, hier);
    GroupMap::iterator pos = groupMap.find(key);
    if (pos != groupMap.end())
    {
      // Move the flows over
      FLNodeElabVector* nodes1 = pos->second;
      nodes1->insert(nodes1->end(), nodes2->begin(), nodes2->end());
      nodes2->clear();
    }
  }
} // SCHCombinationalBlockMerge::postMergeCallback

void SCHCombinationalBlockMerge::recordCycleBlock(const MergeBlock*) const
{
  mMergeTest->recordCycleBlock();
}

bool
SCHCombinationalBlockMerge::validStartBlock(const NUUseDefNode*, MergeBlock*) const
{
  // All blocks are valid start points for merging. Only mixed block
  // merging cares about this.
  return true;
}

// Combination flows that def a part of a latch bit cannot be
// merged. Consider the following test case (bug5983):
//
//
//   always @ (*)
//     if (clk)
//       r1[0] = in;
//
//   always @ (*)
//     if (~clk)
//       r2 = r1[0];
//
//   always @ (*)
//     if (clk)
//       r1[1] = r2;
//
// If all three blocks are merged, then they would introduce a cycle
// r1 -> r2 -> r1 since a single flow is created for r1 at the always
// block level. Also, it needs to be a latch otherwise the fanin for
// r2 would actually be "in" and not "r1".
//
bool
SCHCombinationalBlockMerge::isMultiDefPartialLatch(FLNodeElab* flowElab) const
{
  // If this is not a partial def latch, it is ok
  if (!isPartialDefLatch(flowElab)) {
    return false;
  }

  // Test if we have seen this before
  PartialMultiDefLatches::iterator pos = mPartialMultiDefLatches->find(flowElab);
  if (pos != mPartialMultiDefLatches->end()) {
    return pos->second;
  }

  // Look to see if we have multiple partial def latches with
  // different blocks in the same module instance. The test for the
  // same module instance is done by comparing the FLNodeElab
  // hierarchy. They should be the same.
  //
  // Note that we don't have to worry about merging different blocks
  // with different liveness because we only merge blocks if they are
  // both in the same set of hierarchies (and each hierarchy is in the
  // same set of schedules)
  //
  // Don't use the SCHUtil::loopNetDrivers because it gives us
  // encapsulated flow and we want to do simple net ref tests.
  bool multiplePartialDefs = false;
  NUNetElab* netElab = flowElab->getDefNet();
  NUUseDefNode* firstUseDef = flowElab->getUseDefNode();
  SCHUtil* util = mMarkDesign->getUtil();
  STBranchNode* hier = flowElab->getHier();
  for (NUNetElab::DriverLoop l = netElab->loopContinuousDrivers();
       !l.atEnd() && !multiplePartialDefs; ++l) {
    // Check if this is an always block we haven't seen before. If so
    // it is a different always block.
    //
    // Verify that the always block is in the same module instance
    // (same elab hierarchy).
    //
    // Make sure this is not an initial or pull driver, we can ignore
    // those.
    FLNodeElab* curFlowElab = *l;
    NUUseDefNode* curUseDef = curFlowElab->getUseDefNode();
    STBranchNode* curHier = curFlowElab->getHier();
    if ((firstUseDef != curUseDef) && (hier == curHier) &&
        util->isActiveDriver(curFlowElab)) {
      // If this is a partial def latch, we found a problem
      multiplePartialDefs = isPartialDefLatch(curFlowElab);
    }
  }

  // Remember it in the cache. We don't need to store any latches that
  // are part of a cycle because they are unmergable anyway.
  //
  // Note that because we only compare the same hierarchy above, we
  // can only put elaborated flows with same hierarchy in the cache.
  for (NUNetElab::DriverLoop l = netElab->loopContinuousDrivers();
       !l.atEnd(); ++l) {
    FLNodeElab* curFlowElab = *l;
    STBranchNode* curHier = curFlowElab->getHier();
    if ((curHier == hier) && util->isActiveDriver(curFlowElab) &&
        isPartialDefLatch(curFlowElab) && !SCHMarkDesign::isCycle(curFlowElab)) {
      PartialMultiDefLatches::value_type value(curFlowElab, multiplePartialDefs);
      mPartialMultiDefLatches->insert(value);
    }
  }

  return multiplePartialDefs;
} // SCHCombinationalBlockMerge::isMultiDefPartialLatch

bool SCHCombinationalBlockMerge::isMultiDefPartialLatch(const MergeBlock* block)
  const
{
  // If any of the elaborated flows are partial def latches, then the
  // block is
  bool multiDefPartialLatch = false;
  for (SCHFlowGroups::FlowsLoop l = block->loopFlowNodes();
       !l.atEnd() && !multiDefPartialLatch; ++l) {
    FLNodeElab* flowElab = *l;
    multiDefPartialLatch = isMultiDefPartialLatch(flowElab);
  }
  return multiDefPartialLatch;
}

bool SCHCombinationalBlockMerge::isPartialDefLatch(FLNodeElab* flowElab) const
{
  // This should not be a cycle. The code below crashes on cycles
  // because it doesn't have unelaborated flow.
  FLN_ELAB_ASSERT(!SCHMarkDesign::isCycle(flowElab), flowElab);

  // Test if this is a partial def latch.
  NUNetRefHdl netRef = flowElab->getFLNode()->getDefNetRef();
  return (!netRef->all() && mMarkDesign->isLatch(flowElab));
}

