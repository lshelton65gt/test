// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file

  This file contains all the schedule data that is passed on to the
  code generator.
*/

#ifndef _SCHEDULEDATA_H_
#define _SCHEDULEDATA_H_

//! SCHScheduleData class
/*! This is a data management class that contains the final schedules
 *  and miscellaneous data that are provided to the code generator.
 */
class SCHScheduleData
{
public:
  //! constructor
  SCHScheduleData(SCHUtil* util, SCHMarkDesign* markDesign);

  //! destructor
  ~SCHScheduleData();

  //! Add a combinational schedule
  /*! Add a combinational schedule for one of the four types of
   *  schedules for which there is more than one schedule. These are:
   *  transition, sample, input, and async schedules.
   *
   *  \param type One of eCombTransition, eCombSample, eCombInput,
   *  eCombAsync.
   *
   *  \param comb The schedule to add to this database.
   *
   *  Note that if the type is not one of the above mentioned
   *  schedules, the routine will assert.
   */
  void addCombinationalSchedule(SCHScheduleType type, SCHCombinational* comb);

  //! Returns true if schedules of this type were created
  /*! Returns true if any of combinational schedules were added with
   *  the addCombinationalSchedule() function. This allows processes
   *  to get skipped if these schedules don't exist.
   *
   *  \param type One of eCombTransition, eCombSample, eCombInput,
   *  eCombAsync.
   */
  bool hasCombinationalSchedules(SCHScheduleType type) const;

  //! Abstraction for sorting combinational schedules
  typedef bool (*LessCombSchedules)(SCHCombinational*, SCHCombinational*);

  //! Sort the combinational schedules
  /*! Sorts the combinational schedules that were added with
   *  addCombinationalSchedule().
   *
   * \param type One of eCombTransition, eCombSample, eCombInput,
   *  eCombAsync.
   *
   * \param lessCombSchedules a routine that returns which of two
   * combinational schedules should be sorted first.
   */
  void sortCombinationalSchedules(SCHScheduleType type,
                                  LessCombSchedules lessCombSchedules);

  //! Returns an iterator over one of the a group of combinational schedules
  /*! Returns an iterator over combinational schedules for which there
   *  can be more than one. These are the ones added with
   *  addCombinationalSchedule().
   *
   * \param type One of eCombTransition, eCombSample, eCombInput,
   *  eCombAsync.
   *
   * \returns An iterator to visit every combinational schedule of the
   * above type.
   */
  SCHSchedule::CombinationalLoop loopCombinational(SCHScheduleType type);

  //! Add a combinational node to a schedule
  /*! Add a combinational elaborated flow node to a combinational
   *  schedule. This routine only works on combinational schedules for
   *  which there is only one. These are the force/deposit, debug,
   *  init settle, and initial schedules.
   *
   *  \param schedType One of eCombForceDeposit, eCombDebug,
   *  eCombInitSettle, eCombInitial.
   *
   *  \param flowElab The elaborated flow node to add to the schedule
   */
  void addCombinationalNode(SCHScheduleType schedType, FLNodeElab* flowElab);

  //! Add a sequential schedule
  /*! Adds a sequential schedule that is part of the top level
   *  schedule. DCL sequential sub schedules should not be added
   *  here. They should be added into their DCL parent schedule.
   *
   *  \param seq The sequential schedule to add
   */
  void addSequentialSchedule(SCHSequential* seq);

  //! Get the sequential schedules
  /*! Get the structure that manages a group of sequential schedules
   *  for the top level schedule (doesn't include DCL sequential
   *  subschedules).
   */
  SCHSequentials* getSequentials() const { return mSequentials; }

  //! Iterator over the sequential schedules (doesn't include DCL sequentials)
  /*! Returns an iterator over the group of sequential schedules for
   *  the top level schedule (doesn't include DCL sequential
   *  subschedules).
   */
  SCHSequentials::SequentialLoop loopSequential();

  //! Add a derived schedule
  /*! Adds a derived clock schedule.
   *
   *  \param dclBase The derived clock schedule. This can either be a
   *  simple schedule or DCL cycle schedule (classes derived off of
   *  SCHDerivedClockBase).
   */
  void addDerivedClockSchedule(SCHDerivedClockBase* dclBase);

  //! Check if there are derived clock schedules
  /*! Returns true if any derived clock schedules were added with
   *  addDerivedClockSchedule().
   */
  bool hasDerivedClockSchedules() const
  {
    return !mDerivedClockSchedules->empty();
  }

  //! Abstraction for sorting derived clock schedules
  typedef bool (*LessDCLSchedules)(SCHDerivedClockBase*, SCHDerivedClockBase*);

  //! Sort the derived clock schedules
  /*! Sort the derived clock schedules that were wadded with
   *  addDerivedClockSchedule().
   */
  void sortDerivedClockSchedules(LessDCLSchedules lessDCLSchedules);

  //! Returns an iterator over all the derived clock logic schedules.
  SCHSchedule::DerivedClockLogicLoop loopDerivedClockLogic()
  {
    return SCHSchedule::DerivedClockLogicLoop(*mDerivedClockSchedules);
  }

  //! Get the initial schedule
  SCHCombinational* getInitialSchedule() { return mInitialSchedule; }

  //! Returns an iterator over the initial schedule nodes
  SCHIterator loopInitialSchedule() { return mInitialSchedule->getSchedule(); }

  //! Get the initial settle schedule
  SCHCombinational* getInitSettleSchedule() { return mInitSettleSchedule; }

  //! Returns an iterator over the init settle schedule nodes
  SCHIterator loopInitSettleSchedule()
  {
    return mInitSettleSchedule->getSchedule();
  }

  //! Get the debug schedule
  SCHCombinational* getDebugSchedule() { return mDebugSchedule; }

  //! Returns an iterator over the debug schedule nodes
  SCHIterator loopDebugSchedule() { return mDebugSchedule->getSchedule(); }

  //! Get the debug schedule
  SCHCombinational* getForceDepositSchedule() { return mForceDepositSchedule; }

  //! Returns an iterator over the force deposit schedule nodes
  SCHIterator loopForceDepositSchedule()
  {
    return mForceDepositSchedule->getSchedule();
  }

  //! Iterator for all sequential schedules
  /*! This iterator should be used to visit a number of schedules of
   *  varying type. In this case the iterator visits all sequentials
   *  (top level and DCL sub schedules).
   */
  SCHSchedulesLoop loopAllSequentials();

  //! Iterator for all combinational schedules
  /*! This iterator should be used to visit a number of schedules of
   *  varying type. In this case the iterator visits all
   *  combinationals (top level and DCL sub schedules).
   */
  SCHSchedulesLoop loopAllCombinationals();

  //! Iterator for all simulation combinational schedule (no init or debug)
  /*! This iterator should be used to visit a number of schedules of
   *  varying type. In this case the iterator visits all
   *  combinationals that get run by the top schedule (does not
   *  include the initial initSettle, debug, and force/deposit
   *  schedules).
   */
  SCHSchedulesLoop loopSimulationCombinationals();

  //! Iterator for all simulation schedules (no init or debug)
  /*! This iterator should be used to visit a number of schedules of
   *  varying type. In this case the iterator visits all
   *  combinationals and sequentials that get run by the top schedule
   *  (does not include the initial initSettle, debug, and
   *  force/deposit schedules).
   */
  SCHSchedulesLoop loopAllSimulationSchedules();

  //! Iterator for all the state update elaborated nets in this schedule
  /*! Gets the state update nets for all the top level sequential
   *  schedules (does not include the DCL sequential sub schedules).
   */
  SCHSequentials::SULoop loopSUNets();

  //! Find all the state update elaborated nets in this schedule
  /*! Gets the state update nets for all the top level sequential
   *  schedules and DCL schedules. But it stores them in their
   *  respective places so that the DCL SU nets can be gathered
   *  separately. They have to be handled differently by codegen.
   */
  void findSUNets();

  //! Add an elaborated net to the clear at end set
  /*! Some elaborated nets need to be cleared at the end of the
   *  schedule. The code generator needs a set of these so that it can
   *  generate the clear code. Since it may be cheaper to group
   *  netElabs with the same schedule mask we store them that
   *  way. Codegen can then decide if the if statement is cheap enough
   *  to emit it.
   *
   *  \param mask The trigger conditions on when the net may be set
   *  which are the same conditions under which they should be
   *  cleared.
   *
   *  \param netElab The elaborated net that should be cleared.
   */
  void addClearAtEndNet(const SCHScheduleMask* mask, NUNetElab* netElab);

  //! Iterate over the clear at end nets found
  SCHSchedule::ClearAtEndNetsLoop loopClearAtEndNets();

  //! Remove flows from all the schedules
  /*! This should be called whenever an optimization has been
   *  performed that may deleted elaborated flow because it has become
   *  dead. It walks all the schedules in their current state as well
   *  as any elaborated flow that has been stored in the SCHMarkDesign
   *  class and removes them.
   */
  void removeDeadFlow(const FLNodeElabSet& deletedFlows);

  //! Init the async clocks for population
  void initAsyncClocks(void);

  //! Add a clock computed in an async schedule
  void addAsyncClock(const NUNetElab* clkElab);

  //! Test if a clock is computed in an async schedule
  bool isAsyncClock(const NUNetElab* clkElab) const;

  //! Loop over all the clocks in any async schedule
  SCHClocksLoop loopAsyncClocks(void) const;

private:
  // Hide copy and assign constructors.
  SCHScheduleData(const SCHScheduleData&);
  SCHScheduleData& operator=(const SCHScheduleData&);

  // The input, async, initial, initial settle, and debug schedules
  SCHCombinational* mInitialSchedule;
  SCHCombinational* mInitSettleSchedule;
  SCHCombinational* mDebugSchedule;
  SCHCombinational* mForceDepositSchedule;
  typedef SCHSchedule::Combinationals Combinationals;
  Combinationals* mInputSchedules;
  Combinationals* mAsyncSchedules;

  // The transition and sample mask combinationals (no DCL sub schedules)
  Combinationals* mTransitionSchedules;
  Combinationals* mSampleSchedules;

  // The sequential schedules (no DCL sub schedules)
  SCHSequentials* mSequentials;

  // The derived clock schedules (DCL schedules and their sub schedules)
  SCHSchedule::DerivedClockLogicVector* mDerivedClockSchedules;

  // Helper functions
  void deleteCombinationals(Combinationals* combinationals);
  void cleanupSequentialSchedules(FLNodeElabSet& deletedSeqs);

  // Data to hold the elaborated nets that must be cleared at the end
  // of the schedule.
  SCHSchedule::ClearAtEndNets* mClearAtEndNets;

  //! Data to hold all clocks computed in any asynchronous schedule
  SCHClocks* mAsyncClocks;

  // Utility classes
  SCHUtil* mUtil;
  SCHMarkDesign* mMarkDesign;
}; // class SCHScheduleData

#endif // _SCHEDULEDATA_H_
