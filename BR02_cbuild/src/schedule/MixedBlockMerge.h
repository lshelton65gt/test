// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implements Sequential/Combinational block merging.
*/

#ifndef _MIXEDBLOCKMERGE_H_
#define _MIXEDBLOCKMERGE_H_

#include "util/LoopMap.h"
#include "SequentialBlockMerge.h"

//! SCHMixedBlockMerge class
/*! Implements sequential/combinational block merging by using the
 *  SCHSequentialBlockMerge class to do most of the work. This class
 *  provides the helper functions to create the block graph, determine
 *  what blocks are mergable, and update schedules after a merge.
 *
 *  Note that the only merge that occur is fanin combinational blocks
 *  into sequential blocks. These fanin blocks must not fanout to any
 *  other logic and will no longer be accurate. The defined nets will
 *  be marked unwavable.
 */
class SCHMixedBlockMerge : public SCHSequentialBlockMerge
{
#if ! pfGCC_2
  using UtGraphMerge::validStartNode;
  using SCHSequentialBlockMerge::gatherScheduleBlocks;
#endif

public:
  //! constructor
  SCHMixedBlockMerge(SCHUtil*, SCHMarkDesign*, SCHScheduleData*, 
                     SCHTimingAnalysis*);

  //! destructor
  ~SCHMixedBlockMerge();

  //! Function to merge sequential blocks in the design
  /*! This function tests to make sure block merging is enabled,
   *  gathers all the combinational schedule blocks, creates the graph
   *  and merges them. If the dump flag is enabled it prints
   *  information about the succesful and failed merges.
   *
   *  \param unmergableNets - A set of elaborated nets that should not
   *  be merged. (This is used to avoid merging delayed nets that were
   *  inserted to remove scheduling conflicts).
   */
  void merge(const NUNetElabSet* unmergableNets);

private:
  // This section contains functions that are overriden over
  // SCHSequentialBlockMerge and SCHBlockMerge

  //! Override this function to test if a block is a valid start block
  bool validStartBlock(const NUUseDefNode* useDef, MergeBlock*) const;

  //! Override this function to test if a block is mergable
  bool blockMergable(const NUUseDefNode* useDef, MergeBlock* mergeBlock) const;

  //! Override this function to get the block fanin
  void getBlockFanin(MergeBlock* block, BlockSet* faninSet) const;

  //! Helper function to possibly add a fanin to the set for a fanout block
  void addFaninBlock(MergeBlock* fanoutBlock, FLNodeElab* faninElab,
                     BlockSet* faninSet) const;

  //! Mark combinational nets unwavable
  void preMergeCallback(MergeBlock* lastBlock, MergeBlock* block);

  //! Override virtual function to create a merged block to record combos too
  FLNodeElabVector* createMergeBlock(FLNodeElab* flowElab, SCHScheduleBase* sched);

  //! Function to move merged blocks from a src vector to a dst vector
  void moveFlows(FLNodeElabVector* dst, FLNodeElabVector* src);

  //! Virtual function to record a broken up block
  /*! Record this for the dumped statistics
   */
  virtual void recordCycleBlock(const MergeBlock* block) const;

private:
  // This section contains private types and data to perform the
  // merge.

  //! Type to figure out which schedule a vector of elaborated flow is in
  /*! The following type allow us to quickly go from a block group to
   *  schedule it is in. This helps in updating the groups after
   *  merging a combinational block into a sequential schedule.
   */
  typedef UtHashMap<FLNodeElabVector*, SCHSequentialEdge*> GroupToScheduleMap;

  //! Data to figure out which schedule a vector of elaborated flow is in
  /*! The following map is a way to get the schedule for any block
   *  group. This is needed so that the schedules can be updated when a
   *  merge occurs.
   */
  GroupToScheduleMap* mGroupToScheduleMap;

  //! Function to initialize local data needed before merging
  void initLocalData();

  //! Function to gather all the unmergable combinational blocks
  /*! Combinational blocks that feed an output port of observe point
   *  cannot be merged because they must remain accurate.
   */
  void recordUnmergableCombinationalBlockFanin(FLNodeElab* flowElab);

  //! Recurse the combinational block fanin to record it
  void recordCombinationalBlockFanin(FLNodeElab* flowElab, SCHScheduleBase* sched,
                                     FLNodeElab* seqFlow);

  //! Record the combinational block fanin and recurse
  void recordCombinationalBlock(FLNodeElab* flowElab, SCHScheduleBase* sched,
                                FLNodeElab* seqFlow);

  //! Function to mark a combinational flow with sequential and edge info
  bool recordCombFlow(FLNodeElab* flowElab, SCHScheduleBase* sched,
                      FLNodeElab* seqFlow);

  //! Gather all the combinational blocks for merging
  void gatherCombinationalBlocks();

  //! Test if this is a combo block that can be merged into a flop
  bool isValidCombinational(FLNodeElab* flowElab);

  //! Function to print statistics if the dump flag is set
  void printResults();

  //! Artifical schedule to hold all unmergable combo blocks due to output
  SCHCombinational* mUnmergableComboOutput;

  //! A pointer to a class to determine if blocks are mergable or not
  SCHMergeTest* mMergeTest;

  //! A covered set for flow elabs that have been converted to sequential
  FLNodeElabSet* mNewSequentials;

  //! A covered set for combo flows used to create combo merge blocks
  FLNodeElabSet* mCoveredCombinationals;

  //! Need timing analysis to adjust the timing on converted combos
  SCHTimingAnalysis* mTimingAnalysis;

}; // class SCHMixedBlockMerge : public SCHBlockMerge

#endif // _MIXEDBLOCKMERGE_H_
