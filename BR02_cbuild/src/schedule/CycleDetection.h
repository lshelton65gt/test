// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  Class that holds data and functions used to detect combinational cycles
*/

#ifndef _CYCLEDETECTION_H_
#define _CYCLEDETECTION_H_

#include "nucleus/Nucleus.h"
#include "util/UnionFind.h"
#include "util/SetOps.h"
#include "util/UtOrder.h"
#include "util/DynBitVector.h"
#include "util/GraphSCC.h"
#include "CycleGraph.h"

class UD;
class FLElabUseCache;

typedef UtVector<DependencyGraph*> SCHGraphList;

//! Utility class for doing stable statement comparisons
class UDNodeCmp
{
public:
  bool operator()(const NUUseDefNode* ud1, const NUUseDefNode* ud2) const;
};

typedef UtSet<NUStmt*,UDNodeCmp>                    OrderedStmtSet;
typedef UtMap<NUAlwaysBlock*,FLNodeElab*,UDNodeCmp> OrderedAlwaysBlockFlowMap;

/*! The SCHCycleDetection class encapsulates all of the
 *  cbuild-specific cycle detection functionality.  Generic graph
 *  cycle routines are in separate classes, and the logic for
 *  determining what is an ordering dependency in the first place
 *  is in CycleGraph.cxx.
 *
 *  The purpose of cycle detection is isolate the cyclic portions
 *  of the elaborated flow so that later compiler phases can deal
 *  treat the flow as a directed acylic graph without any cyclic
 *  ordering constraints.  This means that cycle detection must be
 *  at least as pessimistic as any later algorithm that is sensitive
 *  to cyclic dependencies.  But it should not be any more pessimistic,
 *  because this will degrade the capability of later passes and hurt
 *  run-time performance.
 *
 *  The result of cycle detection should be that:
 *    - NUCycle structures are created that describe the extent of each
 *      strongly-connected component in the flow dependency graph.
 *      Every flow node that participates in a cyclic ordering
 *      dependency should be referenced by one and only one NUCycle.
 *    - Every FLNodeElab that participates in a cyclic ordering
 *      dependency has an associated FLNodeElabCycle which represents
 *      it in the acyclic world.  Traversing the fanin of the
 *      FLNodeElabCycles will not lead to an cyclic ordering dependency
 *      that the later cbuild phases cannot handle.
 *    - The flow nodes in each cycle have been split out from other
 *      flow nodes in the same always blocks that do not participate
 *      in cycles.
 *    - The user has been informed about any cycles and the flow nodes
 *      that participate in them.
 */
class SCHCycleDetection
{
public:
  //! constructor
  SCHCycleDetection(SCHMarkDesign* mark, SCHUtil* util, SCHScheduleData* data);

  //! destructor
  ~SCHCycleDetection();

  //! Finds the real design combinational cycles and deals with them
  /*! This routine identifies combinational cycles and cycles through
   *  sequential blocks, processes them to encapsulate true cycles
   *  and prints warnings about each cycle it found.
   *
   *  \returns true if it was unable to encapsulate a real cycle
   */
  bool findDesignCycles();

  //! Does a sanity pass to make sure no cycles have been introduced
  /*! \returns false if there are sanity problems
   */
  bool sanityCheck();

  //! Finds the false design combinational cycles and deals with them
  /*! This routine finds the false combinational cycles that are
   *  introduced by assigning to multiple nets within the same
   *  block. This can introduce false cycles because we try to find a
   *  single ordering between all blocks. 
   *
   *  The routine deals with them by calling the nucleus to break up
   *  the blocks so that the false cycles no longer exist.
   */
  void findFalseDesignCycles();

  //! Dump all the cycles to $fileRoot.cycles 
  void dumpCycles(const char* fileRoot);

private:
#ifdef CDB
  static void printNodeVector(FLNodeElabVector* nodeVector);
#endif

  // Routines to find cycles through clock and asynchronous set/reset pins
  void addSequentialCycleNode(FLNodeElab* flow)
  {
    mSequentialCycleNodes.insert(flow);
  }
  bool isSequentialCycleNode(FLNodeElab* flow)
  {
    return mSequentialCycleNodes.find(flow) != mSequentialCycleNodes.end();
  }

  void addCycleNode(NUCycle* cycle, FLNodeElab* flow);

  // Routines to find and break cycles through combinational blocks

  typedef UtVector<OrderedStmtSet>                                PartitionVector;
  typedef UtMap<SCHUseDefHierPair,PartitionVector>                ElabSplitMap;
  typedef UtMap<const NUAlwaysBlock*,PartitionVector,UDNodeCmp>   SplitMap;
  typedef UtMap<FLNodeElab*,FLNodeElabSet>                        FlowToFlowSetMap;
  typedef UtMap<NUStmt*,OrderedStmtSet,UDNodeCmp>                 StmtToStmtSetMap;
  typedef UtMap<OrderedStmtSet,NUStmt*,SetOrder<OrderedStmtSet> > StmtSetToStmtMap;
  typedef UnionFind<NUStmt*,OrderedStmtSet,StmtToStmtSetMap>      StmtPartition;
  typedef UtMap<NUModule*,NUAlwaysBlockSet>                       ModuleBlockMap;


  SCHGraphList* findCombinationalCycles(bool writeDotFile = false);
  void freeCycleMemory(SCHGraphList* cycles);
  bool splitCycleBlocks(SCHGraphList* cycles);
  void getBlockSplitsForCycle(DependencyGraph* cycle, ElabSplitMap* elabSplitMap,
                              const SCHBlockFlowNodes& blockFlowNodes,
                              FLElabUseCache* levelCache);
  void getCyclicFlows(DependencyGraph* cycle, FLNodeElabSet* continuousFlows, FLNodeElabSet* internalFlows);
  static bool isLeaf(FLNodeElab* flow);
  void getLeafLevelContinuousFanin(FLNodeElabSet& continuousFlows, FLNodeElabSet& internalFlows, FlowToFlowSetMap* fanins,
                                   FLElabUseCache* levelCache);
  void refineFaninToLeafLevel(FlowToFlowSetMap* fanins, bool checkOverlap,
                              FLElabUseCache* cache);
  void contractNonContinuousFanins(FlowToFlowSetMap* fanins, FLNodeElabSet& internalFlows);
  void processOverlappingDefs(const NUUseDefNode* always,
                              NUStmtSet* allContinuousStmts,
                              StmtPartition* partitions);
  void processOverlappingDefsLoop(NUStmtCLoop stmtLoop,
                                  NUStmtSet* allContinuousStmts,
                                  StmtPartition* partitions);
  void getDataAndControlDependencies(FLNodeElab* flow,
                                     FlowToFlowSetMap* dataFanins,
                                     FlowToFlowSetMap* controlFanins,                                     
                                     UtVector<FLNodeElabSet>* controlStack);
  void convertElabSplits(ElabSplitMap& elabSplitMap, SplitMap* splitMap);
  NUAlwaysBlock* makeSplitBlock(UD * ud, const NUAlwaysBlock* orig, OrderedStmtSet& keep_stmts);
  NUNet* createReplacementNet(NUBlock* block, NUNet* original);
  void getLeafStmts(NUStmt* stmt, NUStmtSet* leaves);
  bool qualifyBlockForSplitting(NUAlwaysBlock* always);
  void buildAndScheduleCycles(SCHGraphList* cycles);
  bool reportCycleData(SCHGraphList* cycles,
                       bool writeDotFiles = false);
  void scheduleCycle(NUCycle* cycle, DependencyGraph* graph);
  void updateOutputPortsInCycle(NUCycle* cycle);
  void reportCycle(DependencyGraph* graph);
  void reportCycleFlow(FLNodeElab* flow, int count, int cycleSize);
  void suggestBlastNetDirectives();
  void warnAboutCombinationalSelfCycles();
  bool inlineCycleTasks(SCHGraphList* cycles);
  void performSplits(SplitMap& splitMap,
                     UtSet<const NUAlwaysBlock*>* oldAlwaysBlocks,
                     ModuleBlockMap* newModuleBlocks);
  void recreateDesignData(UtSet<const NUAlwaysBlock*>& oldAlwaysBlocks,
                          ModuleBlockMap& newModuleBlocks);
  void removeFlow();
  void restoreFlow(ModuleBlockMap& newModuleBlocks);  
  void checkFlowSanity();
  void deleteAlwaysBlocks(UtSet<const NUAlwaysBlock*>& toBeDeleted);
  void optimizeModifiedBlocks(ModuleBlockMap& newModuleBlocks);
  void calculateUnelaboratedFlow(NUModule * module);

  // The following class stores information about a false cycle
  // path. This is used in determining the optimal point to break a
  // false cycle.
  class Flow
  {
  public:
    //! constructor
    Flow(FLNodeElab* curFlow, FLNodeElab* hitFlow) :
      mCurFlow(curFlow), mHitFlow(hitFlow)
    {}

    //! destructor
    ~Flow() {}

    //! Get the current flow we are traversing from
    FLNodeElab* getCurFlow() const { return mCurFlow; }

    //! Get the other block flow we came into this block through
    FLNodeElab* getHitFlow() const { return mHitFlow; }

    //! Check if this is a potential break point
    /*! The break points occur when the hit flow and current starting
     *  traversal flow are not the same.
     */
    bool isBreakPoint() const { return mCurFlow != mHitFlow; }

  private:
    FLNodeElab* mCurFlow;
    FLNodeElab* mHitFlow;
  };
  typedef UtVector<Flow*> FlowStack;
  typedef FlowStack::const_reverse_iterator FlowStackRevIter;
  typedef FlowStack::const_iterator FlowStackIter;

  // The following class represents a combinational block made up of
  // flow nodes in the same (always) block. The false cycle detection
  // splits these blocks into smaller blocks. This is the
  // representation for the complete and partial blocks before
  // actually splitting them.
  class Block
  {
  public:
    //! constructor
    Block(const FLNodeElabVector& nodes, UInt32 id, bool split = false);

    //! destructor
    ~Block();

    //! Abstraction to loop the nodes in this block
    typedef Loop<FLNodeElabVector> NodesLoop;

    //! Loop the nodes in this block with the given FLNodeElab first
    NodesLoop loopNodes(FLNodeElab* flow, FLNodeElabVector& nodes);

    //! Loop the nodes in this block in any order
    NodesLoop loopNodes();

    //! Mark that we are traversing this block
    /*! A block may be in the stack multiple times so we need to
     *  create a new modified flag for every time we are looping over
     *  the nodes.
     */
    void pushBusy();

    //! Mark that we are done with the current traversal of this block
    void popBusy();

    //! Clear the modified flag for the current stack
    void clearModified();

    //! Check if this block has been modified
    bool isModified() const;

    //! Remove some nodes that are going to be split away
    void removeNodes(FLNodeElabSet* nodes);

    //! Check if this block is split
    bool isSplit() const { return mSplit; }

    //! Get the id for this block
    UInt32 getID() const { return mID; }

  private:
    // The nodes in this block
    FLNodeElabVector* mNodes;

    // Indicate if this block is being traversed and if it
    // got modifed while being traversed.  This requires two counters,
    // a current depth count and a "dirty-below-this-depth" counter.

    UInt32 mCurrentDepth;

    UInt32 mDirtyDepth;

    // Flag to indicate if this is a split
    bool mSplit;
    UInt32 mID;
  };

  // Data to keep track of combinational blocks for false cycle detection
  typedef UtVector<Block*> Blocks;
  typedef UtSet<Block*> BlockSet;
  Blocks* mBlocks;
  typedef UtMap<FLNodeElab*, Block*> FlowToBlockMap;
  FlowToBlockMap* mFlowToBlockMap;
  UInt32 mNumBlocks;

  //! Type to analyze block cycles before breaking them
  typedef GenericDigraph<void*, Block*> BlockGraph;

  //! Creates a graph on which to run strongly connected components
  BlockGraph* createBlockGraph();

  // Routines to manage combinational blocks
  void createBlocks();
  void createBlock(FLNodeElab* flow,
                   const SCHBlockFlowNodes& blockFlowNodes);
  Block* newBlock(FLNodeElab* flow,
                  const SCHBlockFlowNodes& blockFlowNodes);
  Block* findBlock(FLNodeElab* flow);
  void splitBlock(Block* block, FLNodeElabSet* splitNodes, BlockSet*);
  void freeBlocks();

  // Recursive routine to find and break false design cycles
  UInt32 breakFalseCycleBlocks();
  FLNodeElab* findFalseCycles(FLNodeElab*, UInt32, FlowStack*, UInt32*, UInt32,
                              BlockSet*);
  void splitFalseCycleBlocks();
  void breakBlock(Block*, FLNodeElab*, FLNodeElab*, FLNodeElab*, BlockSet*);
  void markSplitFlow(FLNodeElab* flow);
  bool splitBlocks();
  bool isSplitFlow(FLNodeElab* flow);

  // Routines to deal with the best spot to break a false cycle
  void pushFlow(FlowStack* flowStack, FLNodeElab*, FLNodeElab*);
  void popFlow(FlowStack* flowStack);
  FLNodeElab* findBreakPoint(const FlowStack& flowStack, FLNodeElab* curFlow);
  bool haveInternalFlow(FLNodeElab* flow1, FLNodeElab* flow2);

  // Routines for the cycle sanity pass
  bool checkCycleSanity(FLNodeElab* flow, FLNodeElabVector& nodes, UInt32 pass);
  void printUndetectedCycle(FLNodeElab* flow, FLNodeElabVector& nodes);

  // Used to keep the set of flow nodes in a block cached
  typedef UtVector<FLNodeElabVector*> AllocatedMemory;
  AllocatedMemory mAllocatedMemory;

  // Abstraction and data for the set of sequential nodes that are
  // part of an clock pin cycle.
  typedef UtHashSet<FLNodeElab*> SequentialCycleNodes;
  typedef SequentialCycleNodes::iterator SequentialCycleNodesIter;
  SequentialCycleNodes mSequentialCycleNodes;

  // Data for the set of flow nodes that should be split
  FLNodeElabSet* mSplitNodes;

  // Used to access general scheduler data and functions
  SCHMarkDesign* mMarkDesign;
  SCHUtil* mUtil;
  SCHScheduleData* mScheduleData;
  UInt32 mCycleIndex;
}; // class SCHCycleDetection

#endif // _CYCLEDETECTION_H_
