// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _CLOCKTREEGRAPH_H_
#define _CLOCKTREEGRAPH_H_

#include "util/GenericDigraph.h"

//! Class to represent a node in the clock tree graph
class SCHClockTreeGraphData
{
public:
  //! Empty constructor for invalid data
  SCHClockTreeGraphData() : mFlows(NULL) {}

  //! Constructor
  SCHClockTreeGraphData(const FLNodeElabVector& flows)
  {
    // Sort it so that getting a representative flow is consistent.
    mFlows = new FLNodeElabVector(flows);
    std::sort(mFlows->begin(), mFlows->end(), FLNodeElabCmp());
  }

  //! Destructor
  ~SCHClockTreeGraphData()
  {
    delete mFlows;
  }

  //! Return a representative FLNodeElab for this node (none for clocks)
  /*! For a flop or combinational node there is one flow, return
   *  that. For cycles, any flow will do since they have the same
   *  fanin.
   */
  FLNodeElab* getRepresentativeFlow(void) const
  {
    return mFlows->back();
  }

  //! Return an iterator over the elaborated flows
  FLNodeElabVectorCLoop loopFlows(void) const
  {
    return FLNodeElabVectorCLoop(*mFlows);
  }

  //! Returns all the elaborated flows for this node (none for clocks)
  void getFlows(FLNodeElabVector* flows) const
  {
    for (FLNodeElabVectorLoop l(*mFlows); !l.atEnd(); ++l) {
      FLNodeElab* flowElab = *l;
      flows->push_back(flowElab);
    }
  } // bool getFlows

  //! Compare function for sorting
  static int compare(const SCHClockTreeGraphData& n1,
                     const SCHClockTreeGraphData& n2)
  {
    FLNodeElab* flowElab1 = n1.getRepresentativeFlow();
    FLNodeElab* flowElab2 = n2.getRepresentativeFlow();
    return FLNodeElab::compare(flowElab1, flowElab2);
  }

  //! Sorting function
  bool operator<(const SCHClockTreeGraphData& other) const
  {
    return compare(*this, other) < 0;
  }

private:
  //! Hide the copy constructor
  SCHClockTreeGraphData(const SCHClockTreeGraphData& src);

  //! Hide the assignment constructor
  SCHClockTreeGraphData& operator=(const SCHClockTreeGraphData& src);

  //! Storage for elaborated flows
  FLNodeElabVector* mFlows;
};

//! Class to sort pointers to SCHClockTreeGraphData's
class SCHClockTreeGraphDataCmp
{
public:
  //! Compare function
  bool operator()(const SCHClockTreeGraphData* d1, const SCHClockTreeGraphData* d2)
    const
  {
    return *d1 < *d2;
  }
};


//! Abstraction to create a graph of the clocks and flops in the design.
typedef GenericDigraph<void*, const SCHClockTreeGraphData*> SCHClockTreeGraph;

#endif // _CLOCKTREEGRAPH_H_
