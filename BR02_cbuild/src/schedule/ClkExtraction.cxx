// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "ClkAnal.h"
#include "MarkDesign.h"
#include "Util.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "flow/FLIter.h"
#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "iodb/IODBNucleus.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCopyContext.h"
#include "nucleus/NUElabBase.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUExprFactory.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUUseDefNode.h"
#include "reduce/Fold.h"
#include "schedule/Schedule.h"
#include "symtab/STSymbolTable.h"
#include "util/UtHashSet.h"
#include "util/CbuildMsgContext.h"
#include "util/LoopMap.h"
#include "util/OSWrapper.h"
#include "util/StringAtom.h"
#include "util/UtDebugRecursion.h"
#include "util/UtIOStream.h"
#include "util/UtMap.h"
#include "util/UtSet.h"
#include <algorithm>

//uncomment the following to debug this
//#define CLKEXDEBUG 1

//! inserts a prefix on a verilog name, is aware of escaped names
/*! if pbuf is a verilog escaped named then the resulting string will be
   properly escaped when finished.
   \param pbuf  both source and destination of string.
   \param prefix  string to put onto front pbuf.
*/
static void sclkExtInsertPrefix ( UtString* pbuf, const char* prefix )
{
  UtString tbuf;
  tbuf.clear();
  
  if ( (*pbuf)[0] == '\\' )
  {
    tbuf << "\\";
    pbuf->erase(0,1);
  }

  tbuf << prefix << *pbuf;
  (*pbuf) = tbuf;
}

static void sclkExtCreateDepthForNetRegOrWire ( UtString * pbuf, const NUNetElab * pnet )
{
  NUMemoryNet* memoryNet = pnet->getNet()->getMemoryNet();
  if ( memoryNet != NULL )
  {
    *pbuf << "[" << memoryNet->getDepthRange()->getMsb() << ":" << memoryNet->getDepthRange()->getLsb() << "] ";
  }

}

static void sclkExtCreateWidthForNetRegOrWire ( UtString * pbuf, const NUNetElab * pnet )
{
  NUVectorNet* vn = dynamic_cast<NUVectorNet*>(pnet->getNet());
  NUMemoryNet* memoryNet = pnet->getNet()->getMemoryNet();
  if (vn != NULL)
  {
    vn->composeDeclarePrefix(pbuf);
  }
  else if ( memoryNet != NULL )
  {
    memoryNet->getWidthRange()->format(pbuf);
  }
}

static void sclkExtCreateNameForNetRegOrWire ( UtString * pbuf, const NUNetElab * pnet, bool includeRoot)
{
  pnet->compose(pbuf, NULL, includeRoot);
}

static void sclkExtCreateWidthNameDepthForNetRegOrWire ( UtString * pbuf, const NUNetElab * pnet, bool includeRoot )
{
  sclkExtCreateWidthForNetRegOrWire ( pbuf, pnet );
  *pbuf << " ";
  sclkExtCreateNameForNetRegOrWire ( pbuf, pnet, includeRoot );
  *pbuf << " ";
  sclkExtCreateDepthForNetRegOrWire ( pbuf, pnet );
}



//! creates a declaration line (puts it in pbuf which is first cleared).
static void sclkExtCreateDeclarationLine ( UtString * pbuf, const NUNetElab * pnet, bool includeRoot )
{
  NUNet *net = pnet->getNet();

  pbuf->clear();
  
  if ( net->isPrimaryInput() ) {
    *pbuf << "  input ";
  } else if ( net->isPrimaryBid() ){
    *pbuf << "  inout ";
  } else {
    net->composeDeclarePrefix(pbuf);
  }

  sclkExtCreateWidthNameDepthForNetRegOrWire ( pbuf, pnet, includeRoot );
  *pbuf << ';';
}


//! Extract from the current design, the clock tree
/*! Print this as a verilog design file, the design is flattened.
 * Signal names are printed as if they were hierarchical (but are
 * escaped) so that in debussy they look similar to the names that
 * would be used in the original design
 */
void SCHClkAnal::genClockTree(const char* fileRoot)
{
  FILE* f = stdout;
  if (fileRoot != NULL)
  {
    UtString fname;
    UtString openFailReason;
    fname << fileRoot << ".extracted_clocks.v";
    f = OSFOpen(fname.c_str(), "w", &openFailReason);
    if (f == NULL)
    {
      fprintf(stderr, "%s\n", openFailReason.c_str());
      return;
    }
  }
  UtOFileStream outfile(f);

  // do we need to worry about duplicates in the masters list?
  // is there a method that will just take the second entry from a
  // UtMap and put it into a NUNetElabVector?

  NUNetElabVector masters;

  for (ClockMap::iterator p = mClocks.begin(); p != mClocks.end(); ++p)
  {
    SCHClock* clock = p->second;
    masters.push_back(clock->getMaster());
  }

  // Order the master clock nets alphabetically, so we get some consistency
  // of output.  Causally would probably be better.  Later.
  std::sort(masters.begin(), masters.end(), NUNetElabCmp());

  FLNodeElabSet flowsWithinClockCones; // all the nets that are somewhere within the driving cone of one or more clocks
  NUNetElabSet itemsForDeclarationInputs; // set of inputs that require declarations. 
  NUNetElabSet itemsForDeclarationBid;    // set of inouts that require declarations. 
  NUNetElabSet itemsForDeclarationMemory; // set of memory arrays that require declarations. 
  NUNetElabSet itemsForDeclarationNewPOs; // set of clocks that are not PIs and thus we need to create a connection to output
  NUNetElabSet itemsForDeclarationRegs;   // set of registers that require declarations. 
  NUNetElabSet itemsForDeclarationWires;  // set of wires that require declarations. 
  FLNodeElabList flowsForAssignment; // list of flownodes that are assignment stmts
  FLNodeElabSet flowsForTaskDefinition; // list of tasks that must be defined.
  FLNodeElabList flowsForAlways;     // list of flownodes that are assigned within always block(s)
  FLNodeElabList flowsForInitial;    // list of flownodes that are assigned within initial block(s)

  UtString buf;

  // first make a pass over the design collecting the items and flows
  // that are needed for later dumping of the clock trees.

// gather all drivers found within the driving cone of any clock into flowsQueued2
// Also gather all 
  for (size_t i = 0; i < masters.size(); ++i)
  {
    NUNetElab* net = masters[i];

#ifdef CLKEXDEBUG
    STBranchNode* scope = net->getHier();
    buf.clear();
    net->compose(&buf, scope);
    fprintf(f, "processing clock: %s\n", buf.c_str());
#endif

    if ( ! ( net->getNet()->isPrimaryInput() ))
    {
      itemsForDeclarationNewPOs.insert(net);
    }

    for (NUNetElab::DriverLoop loop = net->loopContinuousDrivers(); not loop.atEnd(); ++loop) {
      FLNodeElab * flow = (*loop);

      for (FLNodeElabIter flow_iter(flow,
                                    FLIterFlags::eAll, // visit
                                    FLIterFlags::eNone, // stop at
                                    FLIterFlags::eBranchAtAll, // nesting
                                    true, // include flow
                                    FLIterFlags::eIterNodeOnce); // iteration
           not flow_iter.atEnd();
           ++flow_iter) {
        FLNodeElab * fanin = (*flow_iter);
        flowsWithinClockCones.insert(fanin);
      }
    }
  }

// // /// // // /// // // /// // // /// // // /// // // /// // // /// // // /// // // /// // // /// 

// now divide all the drivers that we put into flowsWithinClockCones into
// always, assignments or initial blocks, also save items that must be
// declared as wires regs inputs or outputs
  for (FLNodeElabSet::iterator p = flowsWithinClockCones.begin(); p != flowsWithinClockCones.end(); ++p)
  {
    FLNodeElab* driver = *p;
    NUNetElab* net = driver->getDefNet();

    // figure out what kind of driver it falls into , always block, initial block, assign...
    NUUseDefNode* const node = driver->getUseDefNode();
    if (node == NULL)
    {
      if ( net->getNet()->isPrimaryInput() )
      {
        itemsForDeclarationInputs.insert(net);
      }
      else if ( net->getNet()->isPrimaryBid() )
      {
        itemsForDeclarationBid.insert(net);
      }
      else if ( net->getNet()->is2DAnything() )
      {
        itemsForDeclarationMemory.insert(net);
      }
      else
      {
        FLN_ELAB_ASSERT(0, driver); // "Riddle: what is a driver with no UseDefNode and is not a PI or BID?"
      }
    }
    else
    {
      if (node->getType() == eNUTaskEnable)
      {
        flowsForTaskDefinition.insert(driver);
      }
      if ( node->isContDriver() )
      {
        NUAlwaysBlock* ab = dynamic_cast<NUAlwaysBlock*>(node);
        NUInitialBlock* ib = dynamic_cast<NUInitialBlock*>(node);
        NUAssign* assign = dynamic_cast<NUAssign*>(node);

        if ( assign != NULL )
        {
          flowsForAssignment.push_back(driver);
          itemsForDeclarationWires.insert(net);
        }
        else if (ab != NULL)
        {
          flowsForAlways.push_back(driver);
          if (node->isSequential()) { itemsForDeclarationRegs.insert(net);  }
          else                      { itemsForDeclarationWires.insert(net); }
        }
        else if (ib != NULL)
        {
          flowsForInitial.push_back(driver);
          itemsForDeclarationRegs.insert(net); // is this correct?
        }
        else
        {
          // do nothing here?  (what is it?)
          // flowsForAssignment.push_back(driver);
          // itemsForDeclarationWires.insert(net);
        }
      }
    }
  }
  // a second pass to pick up temporary registers, anything that is not a wire/input/bidi must be a register
  for (FLNodeElabSet::iterator p = flowsWithinClockCones.begin(); p != flowsWithinClockCones.end(); ++p)
  {
    FLNodeElab* driver = *p;
    NUNetElab* net = driver->getDefNet();

    if ( ( itemsForDeclarationWires.end()  == itemsForDeclarationWires.find(net)  ) &&
         ( itemsForDeclarationInputs.end() == itemsForDeclarationInputs.find(net) ) &&
         ( itemsForDeclarationMemory.end() == itemsForDeclarationMemory.find(net) ) &&
         ( itemsForDeclarationBid.end()    == itemsForDeclarationBid.find(net)    )    )
    {
      itemsForDeclarationRegs.insert(net);
    }
  }
  

  ////   //   //   //   //   //   //   //   //   //   //   //   //   // //   //   //   //   // 



  // find the name of the top module
  STBranchNode* topHier = NULL;
  buf.clear();
  buf << "Unknown";
  // find a PI and then get the name of its scope, there must be a better way to get this name
  for (NUNetElabSet::SortedLoop p = itemsForDeclarationInputs.loopSorted(); !p.atEnd(); ++p)
  {
    NUNetElab* net = *p;

    topHier = net->getHier();
    buf.clear();
    topHier->compose(&buf);
    break;
  }

  fprintf(f, "// extracted clock tree from module %s\n\n", buf.c_str());
  fprintf(f, "module %s( ", buf.c_str());
  const char* sepChars = "";

  // portlist for inputs
  for (NUNetElabSet::SortedLoop p = itemsForDeclarationInputs.loopSorted(); !p.atEnd(); ++p)
  {
    NUNetElab* net = *p;
    buf.clear();
    net->compose(&buf, NULL, false); // no need to include the root
    fprintf (f, "%s%s ", sepChars, buf.c_str());
    sepChars = ", ";
  }

  // portlist for bidi
  for (NUNetElabSet::SortedLoop p = itemsForDeclarationBid.loopSorted(); !p.atEnd(); ++p)
  {
    NUNetElab* net = *p;
    buf.clear();
    net->compose(&buf, NULL, false); // no need to include the root
    fprintf (f, "%s%s ", sepChars, buf.c_str());
    sepChars = ", ";
  }

  // items that are clocks that are not PIs will have a special output port
  // declared for them so that we can assign the clock to something
  // that will keep it alive.  Here we insert the name of this created output
  // (with CCO_ prefix)
  for (NUNetElabSet::SortedLoop p = itemsForDeclarationNewPOs.loopSorted(); !p.atEnd(); ++p)
  {
    NUNetElab* net = *p;
    buf.clear();
    net->compose(&buf, NULL, false); // no need to include the root

    sclkExtInsertPrefix ( &buf, "CCO_" );
    fprintf (f, "%s%s ",sepChars, buf.c_str());
    sepChars = ", ";
  }

  fprintf(f, " );\n");

  // print the PI declarations
  for (NUNetElabSet::SortedLoop p = itemsForDeclarationInputs.loopSorted(); !p.atEnd(); ++p)
  {
    NUNetElab* net = *p;

    sclkExtCreateDeclarationLine ( &buf, net, false );
    fprintf (f, "%s\n", buf.c_str());
  }

  // print the BID declarations
  for (NUNetElabSet::SortedLoop p = itemsForDeclarationBid.loopSorted(); !p.atEnd(); ++p)
  {
    NUNetElab* net = *p;

    sclkExtCreateDeclarationLine ( &buf, net, false );
    fprintf (f, "%s\n", buf.c_str());
  }

  fprintf(f, "\n");
  fprintf(f, "\n");
  fprintf (f, "  // Now a section for primary outputs\n  // these are part of this clock extraction design but were not part of the original design\n");

  // create a name for each new output
  for (NUNetElabSet::SortedLoop p = itemsForDeclarationNewPOs.loopSorted(); !p.atEnd(); ++p)
  {
    NUNetElab* net = *p;
    buf.clear();
    
    sclkExtCreateNameForNetRegOrWire ( &buf, net, false );  // no need to include the root
    sclkExtInsertPrefix ( &buf, "CCO_" );
    fprintf (f, "  output %s ;\n", buf.c_str());
  }

  // register declarations
  int regCount = 1;
  fprintf(f, "\n");
  for (NUNetElabSet::SortedLoop p = itemsForDeclarationRegs.loopSorted(); !p.atEnd(); ++p)
  {
    NUNetElab* net_elab = *p;
    NUNet * net = net_elab->getNet();
    buf.clear();

    net->composeType(&buf);
    net->composeDeclaration(&buf);

//    sclkExtCreateDeclarationLine ( &buf, net_elab, false );
    fprintf (f, "  %s  // regCount %d\n", buf.c_str(), regCount++);
  }

  // memory declarations
  for (NUNetElabSet::SortedLoop p = itemsForDeclarationMemory.loopSorted(); !p.atEnd(); ++p)
  {
    NUNetElab* net_elab = *p;
    NUNet *net = net_elab->getNet();

    //   sclkExtCreateDeclarationLine ( &buf, net, false );
    net->composeType(&buf);
    net->composeDeclaration(&buf);
    fprintf (f, "%s  // regCount %d\n", buf.c_str(), regCount++);
  }


  // wire declarations
  fprintf(f, "\n");
  for (NUNetElabSet::SortedLoop p = itemsForDeclarationWires.loopSorted(); !p.atEnd(); ++p)
  {
    NUNetElab* net = *p;

    buf.clear();

    sclkExtCreateWidthNameDepthForNetRegOrWire ( &buf, net, false );
    fprintf (f, "  wire   %s ;\n", buf.c_str());
  }

  fprintf(f, "\n\n");
  fprintf(f, "// Now a section for assignments that are part of this clock extraction but not the original design\n");
  fprintf(f, "// The LHS is an output created for this clock extraction\n");
  fprintf(f, "// The RHS is an input to a register that might not have been included in this clock extraction\n");
   
  for (size_t i = 0; i < masters.size(); ++i)
  {
    NUNetElab* net = masters[i];
    UtString buf2;

    if ( net->getNet()->isPrimaryInput() ) {continue;}
    buf.clear();
    sclkExtCreateNameForNetRegOrWire ( &buf, net, false );  // no need to include the root
    buf2 << buf;
    sclkExtInsertPrefix ( &buf2, "CCO_" );
    fprintf(f, "  assign     %s = %s ;\n", buf2.c_str(), buf.c_str());
  }

  int TS = 1;

  for (FLNodeElabSetIter p = flowsForTaskDefinition.begin(); p != flowsForTaskDefinition.end(); ++p)
  {
    FLNodeElab* driver = *p;
    NUUseDefNode* node = driver->getUseDefNode();
    NUTaskEnable* taskEnable = dynamic_cast<NUTaskEnable*>(node);
    FLN_ELAB_ASSERT (taskEnable, driver);
    NUTask * task = taskEnable->getTask();
    STBranchNode* scope = driver->getHier();

    buf.clear(); scope->compose(&buf, true); fprintf(f, "  // TS %d  %s\n", TS, buf.c_str()); // show the name of the hierarchy, this line can be deleted    

    buf.clear();
    task->compose(&buf, NULL, 4, true); // scope, indent, recurse
    fprintf(f, "%s", buf.c_str());
  }

  typedef UtHashSet<NUUseDefNode*> NUUseDefNodeSet;
  NUUseDefNodeSet printedUseDefNodes; // keep track of what has been printed
  int AS = 1;
  fprintf(f, "\n");
  while (!flowsForAssignment.empty())
  {
    FLNodeElab* driver = flowsForAssignment.back(); flowsForAssignment.pop_back();
    NUUseDefNode* node = driver->getUseDefNode();

    if ( ! printedUseDefNodes.insertWithCheck(node) )
    {
      continue;      // can get here if the LHS is a concat, we only need one copy
    }
    if ( ! node->isContDriver() )
    {
      
      continue; // we only print continious drivers, is this test redundant?
    }
    

    fprintf(f, " // AS %d\n", AS++); // unique id for assign stmts, this line can be deleted

    STBranchNode* scope = driver->getHier();

    buf.clear();
    node->compose(&buf, scope, 2, true); // todo: this should be limited to print only the parts that are in flowsWithinClockCones
    fprintf(f, "%s\n", buf.c_str());
  }

  // initial blocks
  fprintf(f, "\n");
  while (!flowsForInitial.empty())
  {
    FLNodeElab* driver = flowsForInitial.back(); flowsForInitial.pop_back();
    NUUseDefNode* node = driver->getUseDefNode();
    NUInitialBlock* ib = dynamic_cast<NUInitialBlock*>(node);
    FLN_ELAB_ASSERT (ib, driver);
  }

  NUUseDefNodeSet printedLowLevelDrivers;

  int abCounter = 0;
  fprintf(f, "\n");
  while (!flowsForAlways.empty())
  {
    abCounter++;
    FLNodeElab* driver = flowsForAlways.back(); flowsForAlways.pop_back();
    FLNodeElab* lowLevelDriver = driver;
    NUUseDefNode* node = driver->getUseDefNode();
    NUUseDefNode* lowLevelUseDefNode;
    NUAlwaysBlock* ab = dynamic_cast<NUAlwaysBlock*>(node);
    FLN_ELAB_ASSERT (ab, driver);
    if ( ! node->isContDriver() ) {continue;} // we only print continious drivers  // redundant

//    while (lowLevelDriver->hasNestedBlock())      // Get the deepest nesting, but if it was an if stmt, getNested will not work correctly
//    {
//      lowLevelDriver = lowLevelDriver->getNested();
//    }
//   if we go to the deepest nesting, then we will need to create the always #(?edge) text here

    lowLevelUseDefNode = lowLevelDriver->getUseDefNode();
    if ( ! printedLowLevelDrivers.insertWithCheck(lowLevelUseDefNode) ) { continue; }
    

    buf.clear(); driver->getHier()->compose(&buf, true); fprintf(f, "  // AB %d  %s\n", abCounter, buf.c_str()); // show the name of the hierarchy, this line can be deleted

    buf.clear();

    STBranchNode* scope = lowLevelDriver->getHier();
    lowLevelUseDefNode->compose(&buf, scope, 4, true); // this should be limited to print only the parts that are in flowsWithinClockCones
    fprintf(f, "%s", buf.c_str());
  }

  fprintf(f, "endmodule\n");

  if (fileRoot != NULL)
  {
    
    fclose(f);
  }
  

} // void SCHClkAnal::genClockTree
