// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file

  Compute the set of nets that if any change, a schedule should
  execute (could change).
*/

#ifndef _COMPUTEBRANCHNETS_H_
#define _COMPUTEBRANCHNETS_H_

#include "util/GraphSCC.h"
#include "util/GraphBuilder.h"

#include "schedule/ScheduleTypes.h"

#include "ClockTreeGraph.h"

//! Branch Net Option Flags
/*! These are various options for controlling the analysis. There
 *  are currently three uses of this process:
 *
 * 1. Computing branch nets for derived clock schedules. The clock
 *    itself is not a legal option for derived clock schedules. But it
 *    should pay attention to fast clock and not merge writable flows
 *    fanin
 *
 * 2. Computing branch nets for all other schedules. The same as
 *    derived clock schedules except it can include itself in
 *    the branch nets.
 *
 * 3. Computing sensitivity for model wrappers. This should allow
 *    itself, ignore the fast clock and merge the writable flows
 *    fanin into the branch nets.
 */
enum SCHBranchNetOptions {
  eIncludeClock,    //!< clocks can be a branch point for themselves
  eIgnoreFastClock, //!< Ignore the fast clock directive when analyzing
  eMergeWritable    //!< Merge writable point with its fanin
};

//! Create flags for a given option
#define SCHBranchNetFlag(option) (1 << (option))
#define SCHBranchNetFlagTest(flags, option) (((flags) & (1 << (option))) != 0)

//! Class SCHBranchNets
/*! This can be used to compute the branch nets for a set of
 *  schedules. It does a design walk to create a graph of the design
 *  clocks so it should be used in a way that amortizes that cost
 *  across many schedules.
 *
 *  The goal for the branch nets is to find a common set of primary
 *  inputs or root clocks for any schedule that can be used to branch
 *  around the schedule. The idea is that the schedule will only do
 *  something meaningful if one of the branch nets changes. If more
 *  than one schedule has the same set of branch nets, a single if
 *  statement can be used for the set of schedules. On some designs
 *  this will reduce the schedule overhead yielding better run-time
 *  performance.
 *
 *  For example:
 *
 *    always @ (posedge clk)
 *      begin
 *        en1 <= en1i;
 *        en2 <= en2i;
 *      end
 *    assign gclk1 = clk & en1;
 *    assign gclk2 = clk & en2;
 *
 *  Then any schedules based of gclk1 and gclk2 will do work only if
 *  clk changes. So the resulting generated schedule for a posedge
 *  gclk1 and posedge gclk2 schedule would be:
 *
 *    if (changed(clk)) {
 *      if (posedge(gclk1)) {
 *        sched_posedge_gclk1();
 *      }
 *    if (posedge(gclk2)) {
 *       sched_posedge_gclk2();
 *      }
 *    }
 *
 *  The branch nets are computed by walking from a given clock to the
 *  primary inputs. The walk goes through combinational logic and
 *  through the clock pins of any flops. However there are a couple of
 *  reasons why the walk might be more complex:
 *
 * 1. A deposit/force on an intermediate point in the graph stops the
 *    walk. This is because the deposit/force can cause the schedule
 *    to require execution.
 *
 * 2. Clocks that are marked as fast are not good branch points. This
 *    is because they tend to change every schedule call and so adding
 *    the branch does not help performance as much.
 *
 *    Take the following case:
 *
 *        always @ (posedge clk)
 *          dclk <= ~dclk;
 *
 *        always @ (posedge dclk)
 *          en1 <= en1_in;
 *
 *        assign gclk1 = dclk & en1;
 *
 *    If a walk is done to the primary inputs for gclk it will find
 *    clk (through dclk). But if that changes on every cycle, it is
 *    not much use. If dclk is used instead, the schedule will be
 *    called half the time. The user can indicate this by marking clk
 *    as a fast clock.
 */
class SCHBranchNets
{
public:
  //! constructor
  /*!
   *  \param flags - See the documentation for Options for details
   *  schedules except for derived clock schedules. This is because
   *  the derived clock schedules should not branch using the clock
   *  that it is computing.
   */
  SCHBranchNets(SCHMarkDesign* markDesign, UInt32 flags);

  //! destructor
  ~SCHBranchNets();

  //! Function to initialize the process (call first)
  void init(void);

  //! Get the set of branch nets for a clock
  /*! Compute or get the set of branch nets for a clock. This routine
   *  caches the information so that it is only computed once per
   *  clock.
   *
   *  \param clk The clock net for which to determine input
   *  dependencies whose changes we could branch on.
   *
   *  \returns a pointer to a set of elaborated nets that can be
   *  branch on or NULL if there are none. Note that this routine
   *  creates these in a factory such that pointer comparisons can be
   *  used to determine equivalence between two SCHInputNets. This is
   *  so that schedules with the same set of SCHInputNets can be
   *  branched around together.
   */
  const SCHInputNets* compute(const NUNetElab* clk);

  //! Get the set of branch nets for a schedule mask
  /*! Uses the NUNetElab function (above) to compute the branch nets
   *  for a set of clocks.
   */
  const SCHInputNets* compute(const SCHScheduleMask* mask);

private:
  //! Hide the copy constructor
  SCHBranchNets(const SCHBranchNets&);

  //! Hide the assign constructor
  SCHBranchNets& operator=(const SCHBranchNets&);

  //! Init function to create the clock tree graph
  void createClockTreeGraph();

  //! Utility function to create a node in the clock tree graph from multiple flows
  SCHClockTreeGraph::Node* createNode(GraphBuilder<SCHClockTreeGraph>* builder,
                                      const FLNodeElabVector& flows);

  //! Utility function to create a node in the clock tree graph from a single flow
  SCHClockTreeGraph::Node* createNode(GraphBuilder<SCHClockTreeGraph>* builder,
                                      FLNodeElab* flowElab);

  //! Utility function to add or return an existing node for the clock tree graph
  SCHClockTreeGraph::Node* addNode(GraphBuilder<SCHClockTreeGraph>* builder,
                                   const FLNodeElabVector& flows);

  //! Utility function to keep track of created graph tree node data
  void addNode(SCHClockTreeGraphData* data);

  //! Recursive function to create edges in the clock tree graph
  /*! This function is responsible for adding the edge if necessary
   *  or recursively calling addGraphEdgeFanin.
   */
  void addGraphEdges(SCHClockTreeGraph::Node* node, FLNodeElab* faninElab, 
                     GraphBuilder<SCHClockTreeGraph>* builder,
                     FLNodeElabSet* covered);

  //! Recursive function to create edges in the clock tree graph
  /*! This function is responsible for visiting the fanin for the
   *  given elaborated flow.
   */
  void addGraphEdgesRecurse(SCHClockTreeGraph::Node* node, FLNodeElab* flowElab, 
                            GraphBuilder<SCHClockTreeGraph>* builder,
                            FLNodeElabSet* covered);

  //! Utility function to create the reverse map of flow to clock tree nodes
  /*! This is used during the edge creation process to determine to
   *  find the node from the flow graph.
   */
  void addToCache(SCHClockTreeGraph::Node* node,
                  const SCHClockTreeGraphData* data);

  //! Utility function to add to the flow -> node map
  void addToCache(FLNodeElab* flowElab, SCHClockTreeGraph::Node* node);

  //! Utility function to find a node for a given flow if it exists
  SCHClockTreeGraph::Node* lookupCache(FLNodeElab* flowElab);

  //! Init function to create a reverse map from clocks to acyclic graph nodes
  /*! The user calls this class to get branch nets on elaborated
   *  nets. But the walker needs to walk the acyclic graph to find the
   *  data. The reverse map allows us to jump to the point of the
   *  graph and compute at that point.
   */
  void createComponentMap();

  //! Utility function to lookup the acyclic node for an elaborated net
  GraphNode* lookupAcylicNode(const NUNetElab* clkElab);

  //! Abstraction for the node data in the graph
  /*! The clock tree graph uses SCHClockTreeGraphData pointers so that
   *  we don't allocate/deallocate FLNodeElabVector's for cycles every
   *  time we copy them around. But to do some, we have to make a
   *  canonical set of SCHClockTreeGraphData pointers. This is that
   *  set.
   */
  typedef UtSet<SCHClockTreeGraphData*, SCHClockTreeGraphDataCmp> ClockTreeNodes;

  //! Storage for the set of clock tree nodes
  ClockTreeNodes* mClockTreeNodes;

  //! Abstraction to cache a map from flows to clock tree nodes
  typedef UtMap<FLNodeElab*, SCHClockTreeGraph::Node*> NodeCache;

  //! Storage for a node cache
  NodeCache* mNodeCache;

  //! Abstraction to cache the acyclic nodes for clocks (starting points)
  typedef UtMap<const NUNetElab*, GraphNode*> AcyclicNodeMap;

  //! Storage for clock to acyclic node map
  AcyclicNodeMap* mAcylicNodeMap;

  //! The clock tree graph used to compute the branch nets
  SCHClockTreeGraph* mClockTreeGraph;

  //! Class to compute strongly connected components
  GraphSCC mGraphSCC;

  //! The strongly connected component clock tree graph
  GraphSCC::CompGraph* mAcylicClockGraph;

  //! Utility class to find clocks and create branch nets
  SCHMarkDesign* mMarkDesign;

  //! Utility class for access to iterators
  SCHUtil* mUtil;

  //! Various option flags
  UInt32 mFlags;

  //! If set, the class has been initialized
  bool mInitPerformed;
}; // class SCHBranchNets

#endif // _COMPUTEBRANCHNETS_H_
