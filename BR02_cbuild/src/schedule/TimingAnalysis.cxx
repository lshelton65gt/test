// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "schedule/Schedule.h"

#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUCycle.h"

#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"

#include "iodb/ScheduleFactory.h"
#include "iodb/IODBNucleus.h"

#include "util/CbuildMsgContext.h"
#include "util/UtIOStream.h"
#include "util/Stats.h"
#include "util/UtDebugRecursion.h"

#include "compiler_driver/CarbonDBWrite.h"

#include "Util.h"
#include "MarkDesign.h"
#include "CycleDetection.h"
#include "TimingAnalysis.h"

// Set of scratch flags used in timing analysis
#define IN_TIMING_LOOP		(1<<0)

/*!
  \file

  This file contains routines to determine when every node can
  transition or is sampled with respect to the clocks, primary inputs,
  and primary outputs. The main routine is computeTiming.
*/

SCHTimingAnalysis::SCHTimingAnalysis(SCHUtil* util,
                                     SCHScheduleFactory* factory,
				     SCHMarkDesign* markDesign) :
  mTimingError(false), mUtil(util), mScheduleFactory(factory),
  mMarkDesign(markDesign)
{
}

SCHTimingAnalysis::~SCHTimingAnalysis()
{
}

bool SCHTimingAnalysis::computeTiming()
{
  Stats* stats = mUtil->getStats();

  // create a new timer region to contain this markStorage stuff,
  // which shouldn't be combined with the computeTiming computation.
  if (stats != NULL)
    stats->pushIntervalTimer();

  // Mark all storage nodes so that signatures can be added to them.
  CbuildSymTabBOM::markStorageNodes(mUtil->getIODB());
  if (stats != NULL) {
    stats->printIntervalStatistics("MarkStorage");
    stats->popIntervalTimer();
  }
  
  // If we are timing passes, start a new timer for computeTiming
  if (stats != NULL)
    stats->pushIntervalTimer();

  // Find all the clocks in the design
  bool status = markClocks();
  if (stats != NULL)
    stats->printIntervalStatistics("MarkClock");

  if (!status)
  {
    if (stats != NULL)
      stats->popIntervalTimer();
    return false;
  }

  // Mark when nodes transition
  calculateDesignSensitivity();
  if (stats != NULL)
    stats->printIntervalStatistics("TransMask");

  // Mark when nodes are sampled
  calculateDesignSampling();
  if (stats != NULL)
    stats->printIntervalStatistics("SampleMask");

  if (stats != NULL)
    stats->popIntervalTimer();

  // Tell the SCHMarkDesign sanity pass that it can check signatures
  mMarkDesign->checkSignatureSanity();
  return !mTimingError;
}

void SCHTimingAnalysis::adjustTiming(FLNodeElabSet& outFlows,
				     FLNodeElabSet& newInFlows)
{
  // Compute the sensitivity for the new flows
  for (FLNodeElabSetIter i = newInFlows.begin(); i != newInFlows.end(); ++i) {
    FLNodeElab* flow = *i;
    if (flow->isLive()) {
      computeTransitionMaskFromFanin(flow);
    }
  }

  // Compute the sample by starting from the out flows and apply any
  // must be transition flags.
  for (FLNodeElabSetIter i = outFlows.begin(); i != outFlows.end(); ++i) {
    FLNodeElab* flow = *i;
    if (flow->isLive() && !flow->isInactive()) {
      // Get the sample mask for this
      const SCHScheduleMask* sampleMask;
      if (SCHUtil::isSequential(flow)) {
	sampleMask = mMarkDesign->getSignature(flow)->getTransitionMask();
      } else {
	sampleMask = mMarkDesign->getSignature(flow)->getSampleMask();
      }

      // Apply the mask and flags
      for (Fanin p = mUtil->loopFanin(flow); !p.atEnd(); ++p) {
	FLNodeElab* fanin = *p;
	if (flow != fanin) {
	  addSamplingRecurse(fanin, sampleMask);
          if (flow->isDerivedClock() || flow->isDerivedClockLogic()) {
            addMustBeTransitionRecurse(fanin);
          }
        }
      }
    }
  }
} // void SCHTimingAnalysis::adjustTiming

void
SCHTimingAnalysis::calculateDesignSensitivity()
{
  // Calculate the design sensitivity through a DFS flow walk
  SensitivityWalker walker(mMarkDesign, mUtil, this, mScheduleFactory);
  walker.createFaninGraph();
  walker.walk();

  // Now walk the elaborated flow and assign constant timing to all
  // unreachable (dead elaborated flow). The reason is that there are
  // cases when we insert buffers where this flow may become
  // artifically live. This could be fixed in buffer insertion but it
  // makes that code much more complex. It requires an analysis of the
  // elaborated vs unelaborated liveness and to potentially create
  // multiple buffers for one net.
  //
  // So instead we mark the dead flow as having constant timing so
  // that it matches the fact that we don't schedule it.
  //
  // Note that this will not add timing to flow that might get created
  // as a result of scheduling. That type of flow should get timing on
  // it and we want to distinguish between that flow and flow that
  // becomes live due to unelaborated pessimism.
  const SCHScheduleMask* constMask = mScheduleFactory->getConstantMask();
  const SCHSignature* constSig = mScheduleFactory->buildSignature(constMask);
  FLNodeElab* flowElab = NULL;
  for (SCHUtil::AllFlowsLoop l = mUtil->loopAllFlowNodes(); l(&flowElab);) {
    // We only care about flow that represents an always block or
    // assign statement. This is marked as a continous driver. We also
    // only want elaborated flow without a signature
    //
    //   NOTE: some dead flow may get a signature because we can visit
    //   both the real clock pin and clock master when computing
    //   timing // through flops. We could change this but it seems
    //   easier to just // add constant timing to elaborated flow
    //   without timing.)
    NUUseDefNode* useDef = flowElab->getUseDefNode();
    const SCHSignature* sig = mMarkDesign->getSignature(flowElab, true);
    if ((sig == NULL) && ((useDef == NULL) || useDef->isContDriver())) {
      setSignature(flowElab, constSig);
    }
  }
} // void SCHTimingAnalysis::calculateDesignSensitivity

void SCHTimingAnalysis::calculateDesignSampling()
{
  // Calculate the design sampling by creating a transpose graph and
  // doing a DFS flow walk.
  SampleWalker walker(mMarkDesign, mUtil, this, mScheduleFactory);
  walker.createFanoutGraph();
  walker.walk();
} // void SCHTimingAnalysis::calculateDesignSampling

bool SCHTimingAnalysis::markClocks()
{
  bool ret = true;
  // Go through the list of clocks we found and figure out if they are
  // derived or primary clocks.
  for (SCHClocksLoop p = mMarkDesign->loopClocks(); !p.atEnd(); ++p)
  {
    // Check if this is a primary or derived clock. It is derived if
    // it has active drivers
    const NUNetElab* clk = *p;
    bool isDerived = mUtil->hasActiveDrivers(clk);

    // If this clock has active drivers it is a derived clock. Mark
    // those flow nodes to be derived clocks.
    SCHUtil::ConstDriverLoop l;
    for (l = mUtil->loopNetDrivers(clk); !l.atEnd(); ++l)
    {
      // If it has an active driver then it is a derived clock. Check
      // for two special cases. The first is the initial driver and
      // the second is an input with just a pull driver on it. Initial
      // drivers should never be part of the derived clock
      // schedule. Pull drivers should only be part of the derived
      // clock schedule if there are other active drivers. Otherwise
      // it should only be in the initial schedule.
      FLNodeElab* flow = *l;
      flow->putIsClock(true);
      NUUseDefNode* useDef = flow->getUseDefNode();
      if ((useDef != NULL) && !useDef->isInitial() &&
	  (isDerived || (useDef->getStrength() != eStrPull)))
	// Derived clock, mark the logic that drives it up to a flop
	flow->putIsDerivedClock(true);
      else if (!isDerived)
	flow->putIsPrimaryClock(true);
    }
  }
  return ret;
}

void SCHTimingAnalysis::setSignature(FLNodeElab* flow,
				     const SCHSignature* signature)
{
  // Mark this signature assigned
  signature->assign();

  // If a previous signature, exists, release it
  const SCHSignature* oldSignature = mMarkDesign->getSignature(flow, true);
  if (oldSignature != NULL)
    oldSignature->release();

  // Now we can set it
  flow->putSignature(signature);

  // Also set the cyclic node if this is an encapsulated cycle
  if (flow->isEncapsulatedCycle()) {
    setSignature(mUtil->getCycleOriginalNode(flow), signature);
  }
} // void SCHTimingAnalysis::setSignature

// Add sensitivity for a sequential node.
const SCHScheduleMask* SCHTimingAnalysis::addSensitivity(FLNodeElab* flow,
							 NUUseDefNode* driver)
{
  ClockEdge edge;
  const NUNetElab* clkElab;

  // Get and register this clock if we haven't already
  mMarkDesign->getSequentialNodeClock(flow, driver, &edge, &clkElab);

  // Create the transition mask. This is created from the edge and
  // clock provided by clock analysis. One caveat is that the clock
  // may have been tied to a constant. If so, we create a *constant*
  // event instead.
  const SCHScheduleMask* mask;
  bool isClockBlock = (mUtil->getClockBlock(driver) == NULL);
  if (!mMarkDesign->isInactiveClock(clkElab, edge, isClockBlock))
  {
    const SCHEvent* ev = mUtil->buildClockEdge(clkElab, edge);
    mask = mScheduleFactory->buildMask(ev);
  }
  else
    mask = mScheduleFactory->getConstantMask();

  // Create the new signture
  const SCHSignature* signature = mScheduleFactory->buildSignature(mask);
  FLN_ELAB_ASSERT(mMarkDesign->getSignature(flow, true) == NULL, flow);

  // Assign the signature
  setSignature(flow, signature);
  return mask;
} // const SCHScheduleMask* SCHTimingAnalysis::addSensitivity

// Add sensitivity for a combinational node.
const SCHScheduleMask*
SCHTimingAnalysis::addSensitivity(FLNodeElab* flow,
				  const SCHScheduleMask* mask)
{
  // Get the old mask
  const SCHSignature* old = mMarkDesign->getSignature(flow, true);
  const SCHScheduleMask* oldmask = NULL;
  if (old != NULL) {
    oldmask = old->getTransitionMask();
  }

  // Combine the masks and then make a signature
  mask = mScheduleFactory->appendMasks(oldmask, mask);
  const SCHSignature* signature = mScheduleFactory->buildSignature(mask);
  setSignature(flow, signature);
  return mask;
} // const SCHScheduleMask* SCHTimingAnalysis::addSensitivity

UIntPtr SCHTimingAnalysis::SampleWalker::getValue(FLNodeElab* flow)
{
  // For outputs ports, use *output*. For all others we determine the
  // timing from its fanout (fanin for the transpose graph).
  const SCHScheduleMask* sampleMask = NULL;
  if (mMarkDesign->isOutputPort(flow)) {
    sampleMask = mScheduleFactory->getOutputMask();
  }
  return (UIntPtr)sampleMask;
}

UIntPtr
SCHTimingAnalysis::SampleWalker::mergeValues(UIntPtr value1, UIntPtr value2)
{
  // Convert the UIntPtr to a schedule mask
  const SCHScheduleMask* mask1 = (const SCHScheduleMask*)value1;
  const SCHScheduleMask* mask2 = (const SCHScheduleMask*)value2;

  // Merge the masks and return it
  const SCHScheduleMask* resultMask;
  resultMask = mScheduleFactory->appendMasks(mask1, mask2);
  return (UIntPtr)resultMask;
}

UIntPtr
SCHTimingAnalysis::SampleWalker::storeValue(FLNodeElab* flow, UIntPtr value)
{
  // Convert the input sampling value
  const SCHScheduleMask* sampleMask = (const SCHScheduleMask*)value;

  // Add the sampling information if we go it. It can only be NULL for
  // sequentials. This occurs when the graph node is the source
  // version (leafs of the tree).
  if (sampleMask != NULL) {
    mTimingAnalysis->addSampling(flow, sampleMask);
  } else {
    FLN_ELAB_ASSERT(SCHUtil::isSequential(flow), flow);
  }

  // For sequentials the return value is the edge for the clock. This
  // is to make sequentials as a source pass back their timing.
  if (SCHUtil::isSequential(flow)) {
    const SCHSignature* sig = mMarkDesign->getSignature(flow);
    sampleMask = sig->getTransitionMask();
  }
  return (UIntPtr)sampleMask;
}

//! Recursive version of add sampling for buffer insertion
/*! Buffer insertion can add pessimism by creating a buffer that
 *  combines two fanins for an output flop. This pessimism must be
 *  pushed down until it can't go farther.
 */
void
SCHTimingAnalysis::addSamplingRecurse(FLNodeElab* flow,
                                      const SCHScheduleMask* mask)
{
  // Add the sampling, if it returns NULL we are done because the mask
  // is already there. Otherwise, recurse
  if (addSampling(flow, mask) == NULL) {
    return;
  }
 
  // If this is a flop, it is as far as we go
  if (SCHUtil::isSequential(flow)) {
    return;
  }

  // Must have gotten added, recurse
  for (Fanin p = mUtil->loopFanin(flow); !p.atEnd(); ++p) {
    FLNodeElab* fanin = *p;
    addSamplingRecurse(fanin, mask);
  }
}

//! addSampling adds the sampling information to a flow node.
/*  It manages the memory so that schedule masks are released when
 *  they are not used anymore. It also checks to see if we should
 *  continue applying the sampling information. We know that we can
 *  stop applying the sampling information when the sampling mask we
 *  are applying has already been applied to the current flow node.
 *
 *  Our caller needs to know when to terminate the algorithm so we
 *  return a NULL mask when we have gone far enough applying the
 *  sampling information. Otherwise we return the new combined sample
 *  mask for further application in the recursive algorithm.
 */
const SCHScheduleMask*
SCHTimingAnalysis::addSampling(FLNodeElab* flow, const SCHScheduleMask* mask)
{
  const SCHSignature* curSignature = mMarkDesign->getSignature(flow);
  const SCHScheduleMask* oldSampleMask = curSignature->getSampleMask();
  const SCHSignature* signature;

  // This nodes sampling mask is its current sampling mask as well
  // as what we are pushing down from our callers.
  if (oldSampleMask != NULL)
    // Create the combined mask
    mask = mScheduleFactory->appendMasks(oldSampleMask, mask);

  // Check if there is no need to continue because we have put this
  // sample mask here before.
  //
  // We can tell this because the new SCHScheduleMask mask is a
  // combination of the old mask and the input mask. If the old one is
  // equivalent it means we haven't added anything new.
  if (mask == oldSampleMask)
    return NULL;

  // Create the new SCHSignature and release the old one
  const SCHScheduleMask* transitionMask = curSignature->getTransitionMask();
  signature = mScheduleFactory->buildSignature(transitionMask, mask);
    
  // We can now update the sample mask for the current node
  setSignature(flow, signature);
  return mask;
} // const SCHScheduleMask* SCHTimingAnalysis::addSampling

void
SCHTimingAnalysis::computeTransitionMaskFromFanin(FLNodeElab* flowElab)
{
  SCHMarkDesign::FlowTimingLoop l;
  const SCHScheduleMask* mask = NULL;
  for (l = mMarkDesign->loopTimingDependencies(flowElab); !l.atEnd(); ++l) {
    // Get the return mask for this flow node
    FLNodeElab* fanin = *l;
    mask = accumulateTransitionMask(mask, fanin);
  }
  if (mask == NULL) {
    mask = mScheduleFactory->getConstantMask();
  }
  addSensitivity(flowElab, mask);
}

UIntPtr SCHTimingAnalysis::SensitivityWalker::getValue(FLNodeElab* flow)
{
  // Calculate the sensitivity based on the flow type
  const SCHScheduleMask* mask = NULL;
  NUUseDefNode* driver = flow->getUseDefNode();
  if (SCHUtil::isSequential(driver)) {
    mask = mTimingAnalysis->addSensitivity(flow, driver);

  } else if (mMarkDesign->isPrimaryBidi(flow) ||
             ((driver == NULL) && mMarkDesign->isPrimaryInput(flow))) {
    // It's a primary bidi.  Assign a special input mask.
    const SCHScheduleMask* inputMask = mScheduleFactory->getInputMask();
    mask = mTimingAnalysis->addSensitivity(flow, inputMask);

  } else if (((driver == NULL) &&
             !mMarkDesign->isPrimaryBidi(flow) &&
              !mMarkDesign->isPrimaryInput(flow)) ||
             driver->isInitial()) {
    // For bound nodes which are not primary ports, treat like a constant.
    // This can occur for deposits (bound nodes can represent deposit or
    // force points).
    // This can also occur for undriven net types which we don't create
    // initializers for (such as memories).
    //
    // For initial blocks we also mark this node as a constant
    const SCHScheduleMask* constMask = mScheduleFactory->getConstantMask();
    mask = mTimingAnalysis->addSensitivity(flow, constMask);

  } else if ((driver != NULL) && !flow->hasFanin()) {
    // If this is a constant assignment, then mark the mask as
    // such. This code is a little strange but it saves on detecting
    // constants inside all the variations of usedef nodes.
    //
    // Basically we are detecting that the flow node has no fanin but
    // it has a usedef node. The only possible cases for this are the
    // RHS is a constant or there is a bug in flow. We will be
    // optimistic and assume it is a constant.
    mask = mScheduleFactory->getConstantMask();
  }

  return (UIntPtr)mask;
}

UIntPtr
SCHTimingAnalysis::SensitivityWalker::mergeValues(UIntPtr value1,
                                                  UIntPtr value2)
{
  // Convert the UIntPtr to a schedule mask
  const SCHScheduleMask* mask1 = (const SCHScheduleMask*)value1;
  const SCHScheduleMask* mask2 = (const SCHScheduleMask*)value2;

  // Merge the masks and return it
  const SCHScheduleMask* resultMask;
  resultMask = mScheduleFactory->appendMasks(mask1, mask2);
  return (UIntPtr)resultMask;
}

UIntPtr
SCHTimingAnalysis::SensitivityWalker::storeValue(FLNodeElab* flow,
                                                 UIntPtr value)
{
  // Convert the value back to a schedule mask
  const SCHScheduleMask* mask = (const SCHScheduleMask*)value;

  // If the mask is still NULL it means this node has no fanin from
  // flops or inputs. It also can't be a flop itself. So assume it has
  // constant timing.
  if (mask == NULL) {
    FLN_ELAB_ASSERT(!SCHUtil::isSequential(flow), flow);
    mask = mScheduleFactory->getConstantMask();
  }

  // Store it in our database
  mask = mTimingAnalysis->addSensitivity(flow, mask);

  // Get the flow's computed sensitivty. This may be different than
  // the stored value. At this point it should not be NULL.
  mask = mTimingAnalysis->getFlowSensitivity(flow, mask);
  FLN_ELAB_ASSERT(mask != NULL, flow);
  return (UIntPtr)mask;
}

const SCHScheduleMask* SCHTimingAnalysis::getClockMask(FLNodeElab* flow) const
{
  // Make sure this isn't a constant clock
  NUNetElab* clkElab = flow->getDefNet();
  FLN_ELAB_ASSERT(clkElab != NULL, flow);
  if (mMarkDesign->isConstantClock(clkElab))
    return mScheduleFactory->getConstantMask();

  const SCHEvent* ev;
  const SCHScheduleMask* posMask;
  const SCHScheduleMask* negMask;

  // Create the posedge mask
  ev = mUtil->buildClockEdge(clkElab, eClockPosedge);
  posMask = mScheduleFactory->buildMask(ev);

  // And the negedge mask
  ev = mUtil->buildClockEdge(clkElab, eClockNegedge);
  negMask = mScheduleFactory->buildMask(ev);

  // And finally build both of them together
  return mScheduleFactory->appendMasks(posMask, negMask);
} // const SCHScheduleMask* SCHTimingAnalysis::getClockMask

// A flow nodes sensitivity to its fanout isn't always the same as its
// own sensitivity. We account for that in this routine.
const SCHScheduleMask*
SCHTimingAnalysis::getFlowSensitivity(FLNodeElab* flow,
				      const SCHScheduleMask* mask,
                                      bool ignoreClocks) const
{
  // We sometimes want to return different sensitivity to our fanout
  // that we store locally (the mask passed in as an argument to this
  // function). This includes (in this priority order):
  //
  // 1. clocks return a mask of posedge clk or negedge clk (unless
  //    ignoreClocks is set).
  //
  // 2. primary inputs return *input* or'ed with the stored mask. This
  //    is needed if an input/bid is locally driven because the driver
  //    of that input/bid won't necessarily have input timing, but its
  //    fanout would. This would cause a cycle in the timing graph.
  //    See test/bugs/bug2705 for an example.
  if (flow->isClock()) {
    if (!ignoreClocks) {
      // Get both edges of the clock 
      mask = getClockMask(flow);
    }

  } else if (mMarkDesign->isPrimaryInput(flow)) {
    // This is a primary input; we want to return *input* timing to
    // our caller.
    const SCHScheduleMask* inputMask = mScheduleFactory->getInputMask();
    mask = mScheduleFactory->appendMasks(mask, inputMask);
  }
  return mask;
} // SCHTimingAnalysis::getFlowSensitivity

const SCHScheduleMask*
SCHTimingAnalysis::getNetTransitionMask(NUNetElab* netElab) const
{
  const SCHScheduleMask* mask = NULL;
  SCHUtil::DriverLoop l;
  for (l = mUtil->loopNetDrivers(netElab); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    if (flowElab->isLive()) {
      FLN_ELAB_ASSERT(!flowElab->isSampled(), flowElab);
      const SCHSignature* sig = mMarkDesign->getSignature(flowElab);
      const SCHScheduleMask* subMask = sig->getTransitionMask();
      mask = mScheduleFactory->appendMasks(mask, subMask);
    }
  }
  return mask;
}

const SCHScheduleMask*
SCHTimingAnalysis::accumulateTransitionMask(const SCHScheduleMask* mask,
                                            FLNodeElab* flow,
                                            bool ignoreClocks)
{
  // Get the signature for the flow, if it isn't computed recurse
  const SCHSignature* sig = mMarkDesign->getSignature(flow, true);
  if (sig == NULL) {
    computeTransitionMaskFromFanin(flow);
  }

  // Get the sensitivity for the flow
  sig = mMarkDesign->getSignature(flow);
  const SCHScheduleMask* flowMask = sig->getTransitionMask();
  flowMask = getFlowSensitivity(flow, flowMask, ignoreClocks);

  // merge the masks from all the fanin
  if (mask == NULL) {
    mask = flowMask;
  } else {
    mask = mScheduleFactory->appendMasks(mask, flowMask);
  }

  return mask;
}

void 
SCHTimingAnalysis::convertToFlop(FLNodeElab* convertedFlow,
                                 FLNodeElab* timingFlow)
{
  // Get the transition mask from the timing flow
  const SCHSignature* sig = mMarkDesign->getSignature(timingFlow);
  const SCHScheduleMask* transMask = sig->getTransitionMask();

  // Create the new signature from the flops trans mask and the
  // converted flop's sample mask.
  const SCHSignature* convSig = mMarkDesign->getSignature(convertedFlow);
  const SCHScheduleMask* sampleMask = convSig->getSampleMask();
  const SCHSignature* newSig;
  newSig = mScheduleFactory->buildSignature(transMask, sampleMask);

  // Apply it to the converted flow
  setSignature(convertedFlow, newSig);
}

void SCHTimingAnalysis::addMustBeTransitionRecurse(FLNodeElab* flowElab)
{
  // If it is already marked, stop
  if (flowElab->mustBeTransition()) {
    return;
  }

  // Stop at flops. It only applies to combo logic
  if (SCHUtil::isSequential(flowElab)) {
    return;
  }

  // Mark it and recurse
  flowElab->putMustBeTransition(true);
  SCHMarkDesign::FlowDependencyLoop p;
  for (p = mMarkDesign->loopDependencies(flowElab, SCHMarkDesign::eInternal);
       !p.atEnd(); ++p) {
    FLNodeElab* faninElab = *p;
    addMustBeTransitionRecurse(faninElab);
  }
}
