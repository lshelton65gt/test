// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#ifndef _INPUTNETS_H_
#define _INPUTNETS_H_

//! A structure to sort by nets sets
struct SCHCmpInputNets
{
  bool operator()(const SCHInputNets* ns1, const SCHInputNets* ns2) const;
};

//! A factory of unique input net sets
class SCHInputNetsFactory
{
public:
  //! constructor
  SCHInputNetsFactory();

  //! destructor
  ~SCHInputNetsFactory();

  //! Create an empty input net set
  const SCHInputNets* emptyInputNets();

  //! Create an initial net set from a single net
  const SCHInputNets* buildInputNets(const NUNetElab* net);

  //! Create a new net set from an existing net set and a net
  const SCHInputNets* appendInputNets(const SCHInputNets* netSet1,
				      const SCHInputNets* netSet2);

  //! Compute the difference between to sets of input nets
  const SCHInputNets* inputNetsDifference(const SCHInputNets*,
                                          const SCHInputNets*);

  //! Function to compare two input nets
  static int compare(const SCHInputNets*, const SCHInputNets*);

  //! Function to print the input nets
  static void print(const SCHInputNets*, bool newLine);
private:
  // Structure to make unique net sets
  typedef UtSet<const SCHInputNets*, SCHCmpInputNets> UniqueNetSets;
  typedef UniqueNetSets::iterator UniqueNetSetsIter;
  UniqueNetSets* mUniqueNetSets;

  // Private functions
  const SCHInputNets* getNetSet(const SCHInputNets* netSet);
};

#endif // _INPUTNETS_H_
