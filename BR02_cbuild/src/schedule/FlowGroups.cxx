// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "flow/FLNodeElab.h"

#include "FlowGroups.h"

SCHFlowGroupsLoop SCHFlowGroups::loopGroups() const
{
  return SCHFlowGroupsLoop(*this);
}

SCHFlowGroups::GroupLoop 
SCHFlowGroups::FlowLoopGen::operator()(FLNodeElabVector* nodes) const
{
  return GroupLoop(*nodes);
}

SCHFlowGroups::FlowsLoop SCHFlowGroups::loopFlowNodes() const
{
  return FlowsLoop(loopGroups(), FlowLoopGen());
}
