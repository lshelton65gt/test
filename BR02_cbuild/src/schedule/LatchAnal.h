// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!\file
 * Interface definition for latch analysis and latch-to-flop conversion.
 */

#ifndef __LATCH_ANAL_H_
#define __LATCH_ANAL_H_

#include "util/UtString.h"
#include "util/UtIO.h"
#include "util/UtMap.h"

#include "nucleus/NUDesign.h"
#include "nucleus/NUExpr.h"

#include "flow/Flow.h"

#include "reduce/ControlTree.h"

#include "schedule/BDDSource.h"

#include "exprsynth/ESPopulateExpr.h"
#include "exprsynth/ExprFactory.h"

#include "ClkAnal.h"

class SCHUtil;
class NUNet;
class NUNetRefFactory;
class IODBNucleus;
class ExprResynth;
class Stats;
class NuToNuFn;

//! A simple class that describes a flop's edge conditions
class FlopDescription
{
public:
  //! Constructor
  FlopDescription(bool posedge, NUExpr* clock, NUExpr* origClock,
                  FlopDescription* priority)
    : mPosedge(posedge), mInverted(false), mClock(clock), mOrigClock(origClock),
      mPriority(priority)
  {
    if (mClock->getType() == NUExpr::eNUUnaryOp)
    {
      NUUnaryOp* op = dynamic_cast<NUUnaryOp*>(mClock);
      if (op && ((op->getOp() == NUOp::eUnLogNot) || 
                 (op->getOp() == NUOp::eUnBitNeg) ||
                 (op->getOp() == NUOp::eUnVhdlNot)))
      {
        mClock = op->getArg(0);
        mPosedge = !mPosedge;
        mInverted = true;
      }
    }
  }

  //! Destructor
  ~FlopDescription() { delete mOrigClock; }

  //! Static convenience constructor for posedge flops
  static FlopDescription* posedge(NUExpr* clock, NUExpr* origClock,
                                  FlopDescription* priority = NULL)
  {
    return new FlopDescription(true, clock, origClock, priority);
  }

  //! Static convenience constructor for negedge flops
  static FlopDescription* negedge(NUExpr* clock, NUExpr* origClock,
                                  FlopDescription* priority = NULL)
  {
    return new FlopDescription(false, clock, origClock, priority);
  }

  //! Is this a posedge flop?
  bool isPosedge() const { return mPosedge; }

  //! Is this a negedge flop?
  bool isNegedge() const { return !isPosedge(); }

  //! Get the clock expression for the flop -- can be an arbitrary expression, not just ident
  NUExpr* getClock() const { return mClock; }

  //! Get the original clock expression in the always block
  /*! This expression may not be made up of continously driven
   *  nets. So it can't be used to create a temporary clock. But it
   *  can be used to replace the clock with a constant in some of the
   *  flops.
   */
  NUExpr* getOrigClock() const { return mOrigClock; }

  //! Get the clock edge for this flop
  ClockEdge getEdge() const
  {
    return isPosedge() ? eClockPosedge : eClockNegedge;
  }

  //! Check if the clock was inverted
  bool wasInverted() const { return mInverted; }

  //! Does this flop have a priority block?
  bool hasPriorityBlock() const { return (mPriority != NULL); }

  //! Get the flop with next-higher priority
  FlopDescription* getPriorityBlock() const { return mPriority; }
  
  //! Print a string representation of the flop description
  void print(int indent = 0) const
  {
    UtString buf;
    compose(&buf,indent);
    UtIO::cout() << buf;
  }

  //! Compose a string representation of the flop description
  void compose(UtString* buf, int indent = 0, bool showPriority = true) const
  {
    if (indent > 0)
      buf->append(indent, ' ');
    if (mPosedge)
      buf->append("(posedge ");
    else
      buf->append("(negedge ");
    mClock->compose(buf,NULL);
    buf->append(")");
    if (mPriority && showPriority)
    {
      buf->append(" // Priority: ");
      mPriority->compose(buf, 0, false);
    }
  }
  
  //! Comparison operator
  bool operator<(const FlopDescription& other) const
  {
    ptrdiff_t cmp = mClock->compare(*other.mClock, false, false, false);
    if (cmp != 0)
      return (cmp < 0);

    if (mPosedge && !other.mPosedge)
      return true;
    else if (!mPosedge && other.mPosedge)
      return false;

    if (!mPriority)
      return (other.mPriority != NULL);

    if (other.mPriority)
      return ((*mPriority) < (*(other.mPriority)));

    return false;
  }

private:
  bool             mPosedge;   //! Is this a posedge flop?
  bool             mInverted;  //! Was the edge inverted due to a ~ in the clock?
  NUExpr*          mClock;     //! The clock expression of continous driven nets
  NUExpr*          mOrigClock; //! The actual expression in the always block
  FlopDescription* mPriority;  //! A higher priority flop, if there is one
};

//! Helper class used for ordering sets of pointers to FlopDescriptions
class FlopDescriptionOrder
{
public:
  bool operator()(const FlopDescription* flop1, const FlopDescription* flop2) const
  {
    return ((*flop1) < (*flop2));
  }
};

class ClkAnalBDDSource : public SimpleBDDSource
{
public:
  ClkAnalBDDSource(SCHClkAnal* clkAnal, Fold* fold,
                   ESPopulateExpr* populateExpr, ExprResynth* exprResynth)
    : SimpleBDDSource(), mClkAnal(clkAnal), mFold(fold),
      mPopulateExpr(populateExpr), mExprResynth(exprResynth)
  {
  }

  ~ClkAnalBDDSource();

  void seed(const FLNodeElab* flow);

  BDD bdd(const FLNodeElab* flow);

private:
  bool extractExpr(const FLNodeElab* flow, NUExpr** expr, CopyContext& cc);

  NUExpr* getExprForFlow(const FLNodeElab* flow, bool returnCopy = true);

  NUExpr* useSimpleExprForFlow(const FLNodeElab* flow);

  BDD addRestrictions(BDD& bdd, const FLNodeElab* flow);

  NUExpr* replaceLeaves(NUExpr* expr, NuToNuFn& translator);

private:
  SCHClkAnal*     mClkAnal;
  Fold*           mFold;
  ESPopulateExpr* mPopulateExpr;
  ExprResynth*    mExprResynth;

  typedef UtMap<const FLNodeElab*,NUExpr*> ExprCache;
  ExprCache       mExprCache;
};

typedef enum { COMPLEX_CONTROL = 0x1, COMPLEX_DATA = 0x2,
               NON_UNIFORM = 0x4, FED_BY_PI = 0x8, NON_EXCL_DRIVER = 0x10,
               BAD_CONTROL_TREE = (COMPLEX_CONTROL | COMPLEX_DATA)
             } LatchFlag;

class LatchStatus;
typedef UtSet<LatchStatus*> LatchStatusSet;


typedef UtPair<ControlTree*,FLNodeElab*> FaninID;
struct FaninIDCmp
{
  //! constructor
  FaninIDCmp() {}

  //! comapre two fanin ids
  bool operator()(const FaninID& fid1, const FaninID& fid2) const;
};
typedef UtSet<FaninID, FaninIDCmp> FaninIDSet;

class LatchStatus
{
public:
  LatchStatus(FLNodeElab* latchFlow)
    : mFlow(latchFlow), mControlTree(NULL), mInstances(NULL),
      mFlags(0), mHasControlTree(false)
  {}
  ~LatchStatus() { delete mControlTree; }
  FLNode* getFlow() const { return mFlow->getFLNode(); }
  FLNodeElab* getElabFlow() const { return mFlow; }
  size_t numInstances() const { return mInstances ? mInstances->size() : 0; }
  void setInstances(LatchStatusSet* instances) { mInstances = instances; }
  LatchStatusSet* getInstances() { return mInstances; }
  LatchStatusSet::iterator beginInstances() { return mInstances->begin();  }
  LatchStatusSet::iterator endInstances() { return mInstances->end(); }
  void setFlags(UInt32 flags) { mFlags |= flags; }
  UInt32  getFlags() const { return mFlags; }
  void setControlTree(ControlTree* tree) { mControlTree = tree; mHasControlTree = true; }
  bool getControlTree(ControlTree** tree) const
  {
    if (mHasControlTree)
      *tree = mControlTree;
    return mHasControlTree;
  }
  void deleteControlTree() { delete mControlTree; mControlTree = NULL; }
  void addNonExclusiveFanin(ControlTree* leaf, FLNodeElab* fanin)
  {
    mNonExclusiveFanins.insert(FaninID(leaf,fanin));
  }
  bool isNonExclusive() const { return (!mNonExclusiveFanins.empty()); }
  FaninIDSet::const_iterator beginNonExclusiveFanins() const { return mNonExclusiveFanins.begin(); }
  FaninIDSet::const_iterator endNonExclusiveFanins() const { return mNonExclusiveFanins.end(); }
private:
  FLNodeElab*     mFlow;
  ControlTree*    mControlTree;
  LatchStatusSet* mInstances;
  FaninIDSet      mNonExclusiveFanins;
  UInt32          mFlags;
  bool mHasControlTree;
};

typedef UtSet<FlopDescription*,FlopDescriptionOrder> FlopDescriptionSet;

typedef enum { BLOCK_OK = 0, NON_EXCLUSIVE_LATCH = 0x1, NO_CONTROL_TREE = 0x2,
               DIFFERENT_CONTROL_TREES = 0x4, NON_LATCH = 0x8,
               NON_SINGLE_SPINE = 0x10,
               CAN_NEVER_LOAD = 0x20, WILL_ALWAYS_LOAD = 0x40
             } BlockFlag;

class BlockStatus
{
public:
  BlockStatus(NUAlwaysBlock* always)
    : mAlways(always), mSpoiler(NULL), mFlags(0)
  {}
  ~BlockStatus()
  {
    for (FlopDescriptionSet::iterator iter = mFlops.begin(); iter != mFlops.end(); ++iter)
    {
      FlopDescription* flop = *iter;
      delete flop;
    }
  }
  NUAlwaysBlock* getBlock() const { return mAlways; }
  void addLatch(LatchStatus* latchStatus) { mLatches.insert(latchStatus); }
  size_t numLatches() const { return mLatches.size(); }
  LatchStatusSet::iterator beginLatches() { return mLatches.begin(); }
  LatchStatusSet::iterator endLatches() { return mLatches.end(); }
  void addFlop(FlopDescription* flop) { mFlops.insert(flop); }
  size_t numFlops() const { return mFlops.size(); }
  FlopDescriptionSet::iterator beginFlops() { return mFlops.begin(); }
  FlopDescriptionSet::iterator endFlops() { return mFlops.end(); }
  void setSpoiler(FLNodeElab* spoiler);
  FLNodeElab* getSpoiler() const { return mSpoiler; }
  void setFlags(UInt32 flags) { mFlags |= flags; }
  UInt32  getFlags() const { return mFlags; }
private:
  NUAlwaysBlock*     mAlways;
  LatchStatusSet     mLatches;
  FlopDescriptionSet mFlops;
  FLNodeElab*        mSpoiler;
  UInt32             mFlags;
};

typedef UtMap<const FLNodeElab*,LatchStatus*> LatchMap;
typedef UtMap<const NUAlwaysBlock*,BlockStatus*, NUUseDefNodeCmp> BlockMap;

typedef UtSet<ControlTreeNodeAddress> ControlTreeNodeAddressSet;
typedef UtMap<const ControlTree*,BDD> NodeBDDMap;

//! The latch analysis and conversion class
class SCHLatchAnal
{
public:
  //! Constructor
  SCHLatchAnal(SCHUtil* util, SCHMarkDesign* mark, SCHClkAnal* clkAnal,
               SCHScheduleData* scheduleData)
    : mUtil(util), mMarkDesign(mark), mClkAnal(clkAnal),
      mScheduleData(scheduleData), mBDDSource()
  {
    // Set up for expression resynthesis
    ESFactory* exprFactory = new ESFactory;
    mPopulateExpr = new ESPopulateExpr(exprFactory, mUtil->getNetRefFactory());
    mExprResynth = new ExprResynth(ExprResynth::eStopAtState, mPopulateExpr,
                                   mUtil->getNetRefFactory(), mUtil->getArgs(),
                                   mUtil->getMsgContext(), mUtil->getIODB()->getAtomicCache(),
                                   mUtil->getIODB());

    // Set up for folding
    mFold = new Fold(mUtil->getNetRefFactory(), mUtil->getArgs(), mUtil->getMsgContext(),
                     mUtil->getIODB()->getAtomicCache(), mUtil->getIODB(),
                     false, eFoldComputeUD);
    
    // Set up BDDSource
    mBDDSource = new ClkAnalBDDSource(clkAnal, mFold, mPopulateExpr, mExprResynth);

    // Set up Nucleus transformation data
    mNewAlwaysBlocks = new NewAlwaysBlocks;
  }

  //! Destructor
  ~SCHLatchAnal();

  //! Update the SCHClkAnal instance to use
  void putClkAnal(SCHClkAnal* clkAnal)
  {
    mClkAnal = clkAnal;
    delete mBDDSource;
    mBDDSource = new ClkAnalBDDSource(clkAnal, mFold, mPopulateExpr, mExprResynth);
  }

  //! Prepare for latch conversion
  /*! The preparation may cause nets to be deleted, so clock analysis
   *  and/or expression caches may need to be recreated or fixed up
   *  before proceeding.
   */
  bool prepare(bool phaseStats, Stats* stats);

  //! Convert latches in the design to flops
  /*! If a file name is provided data about converted latches is
   *  written to it.
   */
  bool convert(const char* fileRoot, bool phaseStats, Stats* stats);

  LatchStatus* getLatchStatus(FLNodeElab* flow)
  {
    LatchMap::iterator p = mLatchMap.find(flow);
    if (p != mLatchMap.end())
      return p->second;
    return NULL;
  }

  bool getControlTree(const FLNodeElab* latchFlow, ControlTree** tree);

  BDDSource* getBDDSource() { return mBDDSource; }

  void setAlwaysStable(ControlTree* node) { mBDDMap[node] = mBDDSource->trueBDD(); }
  void setNeverStable(ControlTree* node) { mBDDMap[node] = mBDDSource->falseBDD(); }
  void addStabilityRequirement(ControlTree* node, BDD& cond)
  { 
    NodeBDDMap::iterator p = mBDDMap.find(node);
    if (p == mBDDMap.end())
      mBDDMap[node] = cond;
    else
      mBDDMap[node] = mBDDSource->opAnd(cond,p->second);
  }
  bool getStabilityBDD(const ControlTree* node, BDD* bdd)
  {
    NodeBDDMap::iterator p = mBDDMap.find(node);
    if (p == mBDDMap.end())
      return false;

    *bdd = p->second;
    return true;
  }

  NUExpr* getTreeBranchClock(ControlTree* node, NUExpr** origClock);

  FLNodeElab* getTopLevelFlow(const FLNodeElab* flow);

private:

  //! Break up the latch flows
  bool splitLatchFlows();

  //! Convert complex latch If statements into simpler nested Ifs
  bool rewriteIfConditions();

  //! Find the block-level elaborated flow for each latch
  bool findLatchFlows();

  //! Build a ControlTree describing each latch
  bool buildControlTrees();

  //! Find which latches are exclsuive with all drivers
  bool testExclusivity();

  //! Minimize the control trees for exclusive latches
  bool minimizeControlTrees();

  //! Check that all control trees are well-formed.
  bool checkControlTreeConsistency();

  //! Find a branch frontier set that minimizes the tree while maintaining exclusivity
  void findExclusiveBranches(ControlTree* tree,
                             ControlTreeNodeAddressSet* frontier);

  //! Caching helper to compute stability conditions for sub-trees
  BDD computeSubtreeStability(ControlTree* tree);

  //! Reject latches in blocks that would have to be split
  bool rejectSplitBlocks();

  //! Create an edge transition set for each latch
  bool createEdgeSets();

  //! Add FlopDescriptions for all required edges
  void populateFlops(BlockStatus* blockStatus, ControlTree* tree,
                     FlopDescription* priority = NULL);

  //! Delete the control trees once flop descriptions have been created
  void deleteControlTrees();

  //! Convert latches to flops in Nucleus
  bool transformNucleus();

  //! Dump summary and detailed data about latch conversion
  void dumpLatchAndBlockData(const char* fileRoot = NULL, bool showBDDS = true);

#if 0
  //! Check latch analysis data structure invariants
  bool checkInvariants() const;
#endif

private:
  //! Populate the map from nested flow to top-level flow for latches & flops
  void makeTopFlowMap(FLNodeElabFactory* factory);

  //! Map of flop descriptions to the new always blocks
  /*! The flop to new always blocks is used to work on a single block
   *  with multiple flop descriptions. This information is created by
   *  createFlops and consumed by connectPointers and
   *  replaceConstantClocks.
   */
  typedef UtMap<const FlopDescription*, NUAlwaysBlock*> FlopToNewAlwaysMap;

  //! Create a set of flops for the flop descriptions
  void createFlops(BlockStatus* blockStatus);

  //! Record a clock for a latch to be converted
  /*! This must be called on all clocks before createFlop is called
   */
  void recordClock(NUAlwaysBlock* oldAlways, const FlopDescription* desc);

  //! Create a flop for a given description if it wasn't already
  NUAlwaysBlock* createFlop(NUAlwaysBlock* oldAlways, const FlopDescription* desc,
                            FlopToNewAlwaysMap* covered);

  //! Map of the old always blocks to the new flops created
  typedef UtMap<NUAlwaysBlock*, NUAlwaysBlockVector> NewAlwaysBlocks;

  //! Find all the clocks used as data for latches converted to flops
  /*! Because we can't store more than one trigger in NUEdgeExpr's for
   *  sequential always blocks, we can't dictate which nets we want
   *  the new value for and which ones we want the old value for.
   *
   *  This pass marks all the clocks for which we want the new value
   *  when inserting buffers. It stores the information in the
   *  SCHMarkDesign class.
   */
  void markLatchClocks();

  //! Test if an elaborated flow is a possible latch
  /*! This only tests the Nucleus markings. To be sure it is a
   *  convertable latch the statements have to be analyzed.
   */
  bool isLatch(FLNodeElab* flowElab) const;

  //! Test if a "latch" will always overwrite all of its bits (it is not really a latch)
  bool willAlwaysLoad(ControlTree* tree) const;

private:
  SCHUtil*        mUtil;         //! Scheduler utility class
  SCHMarkDesign*  mMarkDesign;   //! Scheduler storage class
  SCHClkAnal*     mClkAnal;      //! Clock analysis instance
  SCHScheduleData*mScheduleData; //! Schedule data needed by block splitting
  ESPopulateExpr* mPopulateExpr; //! Exprsynth instance
  ExprResynth*    mExprResynth;  //! Expr resynthesis instance
  Fold*           mFold;         //! Fold instance
  BDDSource*      mBDDSource;    //! A BDD source that uses clock analysis

  LatchMap mLatchMap;            //! Map latch flows to their LatchStatus
  BlockMap mBlockMap;            //! Map always blocks to their BlockStatus

  NodeBDDMap mBDDMap;             //! Map control tree nodes to BDDs for stability conditions

  NewAlwaysBlocks* mNewAlwaysBlocks; //! New always blocks created

  UtMap<const FLNodeElab*,FLNodeElab*> mTopFlowMap; //! Map nested latch/flop flow to top-level flow
};

#endif // __LATCH_ANAL_H_

