// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Compute the set of nets that if any change, a schedule should
  execute (could change).
*/

#include "util/Graph.h"
#include "util/GraphBuilder.h"

#include "schedule/Event.h"
#include "schedule/ScheduleMask.h"

#include "MarkDesign.h"
#include "ClockTreeGraph.h"
#include "BranchNets.h"

// Flags for the clock graph nodes
#define CB_WRITABLE_MASK                (1<<0)
#define CB_CLOCK_MASK                   (1<<1)
#define CB_PRIMARY_INPUT_MASK           (1<<2)
#define CB_PRIMARY_BID_MASK             (1<<3)

// Flags for the acyclic graph
#define CB_BRANCH_NETS_VALID_MASK       (1<<4)
#define CB_IS_CLOCK_MASK                (1<<5)
#define CB_FAST_CLOCK_MASK              (1<<6)
#define CB_ALL_ACYCLIC_FLAGS            (7<<4)

SCHBranchNets::SCHBranchNets(SCHMarkDesign* markDesign,
                             UInt32 flags) :
  mClockTreeGraph(NULL), mAcylicClockGraph(NULL), mMarkDesign(markDesign),
  mUtil(markDesign->getUtil()), mFlags(flags), mInitPerformed(false)
{
  mClockTreeNodes = new ClockTreeNodes;
  mNodeCache = new NodeCache;
  mAcylicNodeMap = new AcyclicNodeMap;
}

SCHBranchNets::~SCHBranchNets()
{
  for (Loop<ClockTreeNodes> l(*mClockTreeNodes); !l.atEnd(); ++l) {
    SCHClockTreeGraphData* data = *l;
    delete data;
  }
  delete mClockTreeNodes;
  delete mClockTreeGraph;
  delete mNodeCache;
  delete mAcylicNodeMap;

  // Don't need to delete the acyclic graph because the GraphSCC class
  // owns it.
}

void SCHBranchNets::init(void)
{
  // Create the clock tree graph
  INFO_ASSERT(!mInitPerformed, "Cannot call init() more than once!");
  createClockTreeGraph();

  // Create the acyclic graph from it and then a reverse map from
  // graph nodes to components.
  mAcylicClockGraph = mGraphSCC.buildComponentGraph(mClockTreeGraph);
  createComponentMap();
  mInitPerformed = true;
}

//! Class to walk the clock trees and create branch nets
/*! The goal of this class is to incrementally compute (as needed) the
 *  branch nets for various clocks in the design. These include both
 *  primary and derived clocks. See the SCHBranchNets class comments
 *  for more details.
 *
 *  Since this class is expected to run incrementally, it stores in
 *  each node whether it has been computed or not. That way subsequent
 *  calls to the same clocks or to clocks that share part of the clock
 *  tree, will not recompute values unnecessarily. One exception to
 *  that is if the eIncludeClock option is false. This is should be
 *  true if a clock cannot be in its own branch nets which is true
 *  when we are computing branch nets for derived clock schedules.
 *
 *  The walker stores the branch nets in three locations:
 *
 *  1. In the graph node scratch field. This is the location that is
 *     used to:
 *
 *     a. Accumulate the fanin branch nets while walking the fanin.
 *
 *     b. Store the final value for the node if this is not a clock
 *        node or the eIncludeClock flags is true.
 *
 *     c. Store the fanin node value if this is a clock and
 *        the eIncludeClock flag is false.
 *
 *  2. mLastNodeBranchData: This contains the branch nets data for the
 *     last fanin node visited. Note that the eIncludeClock flag does
 *     not affect this value because it is ok for a clock to use itself
 *     for its fanout.
 *
 *  3. mNodeBranchData: This contains the branch nets data for the
 *     last node visited. This is affected by the eIncludeClock flag and
 *     whether the node is a clock. It will be the value that should be
 *     returned if compute was called on the node.
 *
 *  See the documentation for visitNodeBefore, visitNodeBetween,
 *  visitCrossEdge, and visitNodeAfter for details on how the branch
 *  data is managed.
 */
class BNClockTreeWalker : public GraphWalker
{
public:
  //! constructor
  BNClockTreeWalker(SCHClockTreeGraph* clockTreeGraph,
                    GraphSCC* graphSCC,
                    SCHMarkDesign* markDesign,
                    UInt32 flags) :
    mEmptyBranchNets(markDesign->emptyInputNets()),
    mNodeBranchData(mEmptyBranchNets, false),
    mLastNodeBranchData(mEmptyBranchNets, false),
    mClockTreeGraph(clockTreeGraph), mGraphSCC(graphSCC),
    mMarkDesign(markDesign), mUtil(markDesign->getUtil()),
    mFlags(flags)
  {}

  //! Get the resulting branch net for the start node
  const SCHInputNets* getBranchNets(void)
  {
    return mNodeBranchData.getBranchNets();
  }

  //! Override the visit node before function to set the initial data
  /*! This function tests whether the node was done in a previous
   *  walk. If so, it returns that value. Otherwise, it initializes
   *  all the data to empty.
   */
  Command visitNodeBefore(Graph*, GraphNode* node);

  //! Override the visitNodeBetween to merge fanin data
  /*! This is called between fanins of this node. This call is used to
   *  merge the data into the current nodes value.
   */
  Command visitNodeBetween(Graph*, GraphNode* node);

  //! Override the visitCrossEdge to get a saved data point.
  /*! This is called for a node that has previously been visited
   *  during this walk. It is used to get the value previously
   *  committed and store it in the last node data.
   */
  Command visitCrossEdge(Graph*, GraphNode* node, GraphEdge* edge);

  //! Override the visit node after function to compute the resulting input nets
  /*! This function both merges in the last fanins value into this
   *  node to compute the final fanin data. It then computes the
   *  current nodes value based on the fanin and node details. It then
   *  stores the data so that any fanout nodes can get the data.
   */
  Command visitNodeAfter(Graph*, GraphNode* node);

  //! Override the visitBackEdge to make sure there are no cycles
  /*! This function asserts that it cannot happen.
   */
  Command visitBackEdge(Graph*, GraphNode* node, GraphEdge* edge);

private:
  //! Abstraction for the branch nets/fast clock pair
  class BranchNetsData
  {
  public:
    //! Constructor
    BranchNetsData(const SCHInputNets* branchNets, bool fastClock) :
      mBranchNets(branchNets), mFastClock(fastClock)
    {}

    //! Get the input nets
    const SCHInputNets* getBranchNets(void) const { return mBranchNets; }

    //! Get the fast clock flag
    bool hasFastClock(void) const { return mFastClock; }

  private:
    //! Store the branch nets (could be NULL)
    const SCHInputNets* mBranchNets;

    //! If true, the input nets contain a fast clock
    bool mFastClock;
  }; // class BranchNetsData

  //! Empty branch nets to start with (NULL means failure)
  const SCHInputNets* mEmptyBranchNets;

  //! The branch nets and fast clock boolean for the current node
  BranchNetsData mNodeBranchData;

  //! The branch nets and fast clock boolean for the fanin of the current node
  BranchNetsData mLastNodeBranchData;

  //! Function to compute a graph nodes branch nets given the fanin branch nets
  BranchNetsData computeNodeValue(GraphNode* graphNode,
                                  const BranchNetsData& faninData,
                                  bool* retIsClock);

  //! Function to merge the fanin value into the value for a fanout node
  BranchNetsData mergeFaninValue(Graph* graph, GraphNode* node);

  //! Function to merge two BranchNetsData values
  BranchNetsData mergeBranchNetsData(const BranchNetsData& one,
                                     const BranchNetsData& two);

  //! Function to get the cached data for a previously computed node
  /*! Returns true if it was previously computed. It also computes
   *  both the mLastNodeBranchData and mNodeBranchData. The former is
   *  the value that fanouts should use and the latter is the value
   *  for this node if it is a clock.
   */
  bool getCachedData(Graph* graph, GraphNode* node, BranchNetsData* data);

  //! Function to store the computed data for later retrieval by getCachedData
  void storeCachedData(Graph* graph, GraphNode* graphNode, bool isClock,
                       const BranchNetsData& faninData,
                       const BranchNetsData& data);

  //! Function to update the last node visited
  void copyToWalker(const BranchNetsData& branchNetsData);

  //! Function to store the branch net info into a node
  void putNodeData(Graph* graph, GraphNode* node,
                   const BranchNetsData& branchNetsData, bool isClock);

  //! Function to get the branch net info from a node if it is there
  bool getNodeData(Graph* graph, GraphNode* node, BranchNetsData* branchNetsData,
                   bool* isClock);

  //! Helper function to compute the branch nets data from a net elab
  BranchNetsData computeBranchNetsData(NUNetElab* netElab);

  //! The original clock tree graph (potentially with cycles)
  SCHClockTreeGraph* mClockTreeGraph;

  //! The GraphSCC class to access the SCC components
  GraphSCC* mGraphSCC;

  //! Utility class to manipulate const SCHInputNets*
  SCHMarkDesign* mMarkDesign;

  //! Utility class to iterate over the drivers of clocks
  SCHUtil* mUtil;

  //! Flags to control the analysis
  UInt32 mFlags;
}; // class BNClockTreeWalker : public GraphWalker

const SCHInputNets* SCHBranchNets::compute(const NUNetElab* netElab)
{
  // We must have initialized
  NU_ASSERT(mInitPerformed, netElab);

  // Make sure this is a valid clock, other wise return NULL. Also
  // make sure we are operating on the master.
  if (!mMarkDesign->isClock(netElab)) {
    return NULL;
  }
  const NUNetElab* clkElab = mMarkDesign->getClockMaster(netElab);

  // If this net is writable and we are not merging the fanin of
  // writable nets, there is no way we should branch on any input nets
  // that feed this net. This is because the deposit/force can
  // override this. But if it is a clock, we can return it
  const SCHInputNets* branchNets = NULL;
  if (!SCHBranchNetFlagTest(mFlags, eMergeWritable) &&
      mMarkDesign->isWritable(clkElab)) {
    if (SCHBranchNetFlagTest(mFlags, eIncludeClock) &&
        mMarkDesign->isClock(clkElab)) {
      branchNets = mMarkDesign->buildInputNets(clkElab);
    } else {
      branchNets = NULL;
    }
  } else {
    // Walk the design from this clock's node and compute the branch nets
    BNClockTreeWalker walker(mClockTreeGraph, &mGraphSCC, mMarkDesign, mFlags);
    GraphNode* graphNode = lookupAcylicNode(clkElab);
    walker.walkFrom(mAcylicClockGraph, graphNode);
    branchNets = walker.getBranchNets();
    if (branchNets == mMarkDesign->emptyInputNets()) {
      branchNets = NULL;
    }
  }
  return branchNets;
} // SCHBranchNets::computeBranchNets

const SCHInputNets* SCHBranchNets::compute(const SCHScheduleMask* mask)
{
  const SCHInputNets* branchNets = NULL;
  SCHScheduleMask::SortedEvents e;
  bool nullInputNets = false;
  for (e = mask->loopEvents(); !e.atEnd() && !nullInputNets; ++e) {
    const SCHEvent* event = *e;
    if (event->isClockEvent()) {
      // Get the elaborated clock for this event
      const NUNetElab* origClk = SCHSchedule::clockFromName(event->getClock());
      const NUNetElab* clkElab = mMarkDesign->getClockMaster(origClk);

      // Gather the input nets for this clock and append it to the
      // accumulated inputs.
      const SCHInputNets* thisInputNets = compute(clkElab);
      if (thisInputNets == NULL) {
        // If any of them fail, they all fail
	nullInputNets = true;
	branchNets = NULL;
      } else {
	branchNets = mMarkDesign->appendInputNets(branchNets, thisInputNets);
      }
    }
  }
  return branchNets;
}

void SCHBranchNets::createClockTreeGraph()
{
  // Walk all the clocks as the starting point
  mClockTreeGraph = new SCHClockTreeGraph;
  GraphBuilder<SCHClockTreeGraph> builder(mClockTreeGraph);
  FLNodeElabVector toDo;
  FLNodeElabSet covered;
  for (SCHClocksLoop l = mMarkDesign->loopClocks(); !l.atEnd(); ++l) {
    // Get the elaborated flow for this clock
    const NUNetElab* netElab = *l;
    const NUNetElab* clkElab = mMarkDesign->getClockMaster(netElab);
    FLNodeElabVector flows;
    for (SCHUtil::ConstDriverLoop n = mUtil->loopNetDrivers(clkElab);
         !n.atEnd(); ++n) {
      FLNodeElab* flowElab = *n;
      flows.push_back(flowElab);
    }
        
    // Create the node for this clock
    SCHClockTreeGraph::Node* node = createNode(&builder, flows);
    if (node != NULL) {
      // Create the starting point for walking the design to create the
      // graph if we should add the fanin for this clock
      const SCHClockTreeGraphData* data = node->getData();
      for (FLNodeElabVectorCLoop l = data->loopFlows(); !l.atEnd(); ++l) {
        FLNodeElab* flowElab = *l;
        if (covered.insertWithCheck(flowElab)) {
          toDo.push_back(flowElab);
        }
      }
    }
  } // for

  // Walk the design from this point and build the graph nodes
  while (!toDo.empty()) {
    // Graph the current element to work on
    FLNodeElab* flowElab = toDo.back();
    toDo.pop_back();

    // Create a node for this if it doesn't already exist and is a
    // graph node (clocks, flops, writables, cycles).
    SCHClockTreeGraph::Node* node = createNode(&builder, flowElab);
    const SCHClockTreeGraphData* data = NULL;
    if (node != NULL) {
      data = node->getData();
    }

    // Add the fanin to the to do list
    if (SCHUtil::isSequential(flowElab)) {
      SCHUtil::ConstDriverLoop f;
      for (f = mMarkDesign->loopSequentialClockDrivers(flowElab);
           !f.atEnd(); ++f) {
        FLNodeElab* faninElab = *f;
        if (covered.insertWithCheck(faninElab)) {
          toDo.push_back(faninElab);
        }
      }
    } else {
      for (Fanin f = mUtil->loopFanin(flowElab); !f.atEnd(); ++f) {
        FLNodeElab* faninElab = *f;
        if (covered.insertWithCheck(faninElab)) {
          toDo.push_back(faninElab);
        }
      }
    }
  } // while

  // Walk the graph nodes and add the edges recursively
  for (Iter<GraphNode*> n = mClockTreeGraph->nodes(); !n.atEnd(); ++n) {
    GraphNode* graphNode = *n;
    covered.clear();
    SCHClockTreeGraph::Node* node = mClockTreeGraph->castNode(graphNode);
    const SCHClockTreeGraphData* data = node->getData();
    for (FLNodeElabVectorCLoop l = data->loopFlows(); !l.atEnd(); ++l) {
      FLNodeElab* flowElab = *l;
      addGraphEdgesRecurse(node, flowElab, &builder, &covered);
    }
  }
} // SCHBranchNets::createClockTreeGraph


SCHClockTreeGraph::Node* 
SCHBranchNets::createNode(GraphBuilder<SCHClockTreeGraph>* builder,
                          const FLNodeElabVector& inFlows)
{
  // Check if a node was already created for a single flow. If not,
  // create it.
  FLNodeElab* flowElab = inFlows.back();
  SCHClockTreeGraph::Node* node = lookupCache(flowElab);
  if (node == NULL) {
    // Gather all the flows for this node in a loop. The reason this
    // has to be a loop is that a clock can have multiple combo cycles
    // driving it and we want all drivers of a combo cycle to be in
    // the same graph node. So getting the acyclic flows for a cycle
    // can bring in more clocks and visiting the net drivers of clocks
    // can bring in more cycles.
    //
    // One alternative is to use the cyclic graph to create this
    // graph. But this means redoing the combo cycle detection which
    // may be done differently than the real combo cycle detection. So
    // instead, this loop is used with multiple covered sets so that
    // we don't redo work. It takes a while to settle for crazy
    // (random test) cases but it should be linear because of the
    // covered sets.
    UtHashSet<NUUseDefNode*> coveredCycles;
    FLNodeElabSet coveredFlows;
    NUNetElabSet clkElabs;
    FLNodeElabVector nodeFlows;
    FLNodeElabVector startFlows(inFlows);
    while (!startFlows.empty()) {
      // Gather the elaborated flow for this node. Walk the flows, if it
      // is a regular flow just move it. If it is a cycle, add its
      // bretheren. But only if we haven't already seen another flow from
      // this cycle. We don't want to call getAcyclicFlows on each flow
      // encountered because this could be an N*N operation where N is the
      // number of flows in the cycle. Normally this is not a big deal but
      // we want predicatable compiles even where there are massive
      // cycles.
      FLNodeElabVector newFlows;
      for (FLNodeElabVectorCLoop l(startFlows); !l.atEnd(); ++l) {
        FLNodeElab* flowElab = *l;
        if (SCHMarkDesign::isCycle(flowElab)) {
          NUUseDefNode* useDef = flowElab->getUseDefNode();
          if (coveredCycles.insertWithCheck(useDef)) {
            mUtil->getAcyclicFlows(flowElab, &newFlows);
          }
        } else {
          newFlows.push_back(flowElab);
        }
      }

      // These start flows have been processed. Clear it so we can
      // add new ones for processing below.
      startFlows.clear();

      // Walk the flows again and copy them to the node flows
      // vector. Also gather any new clocks
      NUNetElabVector newClks;
      for (FLNodeElabVectorCLoop l(newFlows); !l.atEnd(); ++l) {
        FLNodeElab* flowElab = *l;
        if (coveredFlows.insertWithCheck(flowElab)) {
          // Add this to the final flow vector
          nodeFlows.push_back(flowElab);

          // Detect new clocks for processing
          NUNetElab* netElab = flowElab->getDefNet();
          if (mMarkDesign->isClock(netElab) && clkElabs.insertWithCheck(netElab)) {
            newClks.push_back(netElab);
          }
        }
      }

      // Now add the new flows found for any new clocks to be processed
      for (NUNetElabVectorLoop l(newClks); !l.atEnd(); ++l) {
        NUNetElab* clkElab = *l;
        SCHUtil::DriverLoop d;
        for (d = mUtil->loopNetDrivers(clkElab); !d.atEnd(); ++d) {
          FLNodeElab* flowElab = *d;
          if (coveredFlows.find(flowElab) == coveredFlows.end()) {
            startFlows.push_back(flowElab);
          }
        }
      }
    } // while

    // Create the node
    node = addNode(builder, nodeFlows);
  } // if

  return node;
} // SCHBranchNets::createNode

SCHClockTreeGraph::Node* 
SCHBranchNets::createNode(GraphBuilder<SCHClockTreeGraph>* builder,
                          FLNodeElab* flowElab)
{
  // For now reuse the create function that takes a vector. I don't
  // think this will be to expensive.
  FLNodeElabVector flows;
  flows.push_back(flowElab);
  return createNode(builder, flows);
} // SCHBranchNets::createNode

//! SCHClockTreeNodeHelper
/*! Helper class to manage the data for a potential clock tree graph
 *  node
 */
class SCHClockTreeNodeHelper
{
public:
  //! constructor
  SCHClockTreeNodeHelper(SCHMarkDesign* markDesign) :
    mWritable(false), mClock(false), mPrimaryInput(false), mPrimaryBid(false),
    mCycle(false), mSequential(false), mMarkDesign(markDesign)
  {}

  //! Returns whether this node is writable
  bool isWritable() const { return mWritable; }

  //! Returns whether this node is a clock
  bool isClock() const { return mClock; }

  //! Returns whether this node is a primary input
  bool isPrimaryInput() const { return mPrimaryInput; }

  //! Returns whether this node is a primary bid
  bool isPrimaryBid() const { return mPrimaryBid; }

  //! Returns whether this node is in a cycle
  bool isCycle() const { return mCycle; }

  //! Returns whether this node is sequential
  bool isSequential() const { return mSequential; }

  //! Returns whether this is a valid clock tree graph node
  /*! For this set of flows to be a valid clock tree graph node, at
   *  least one element must be either writable, a clock, a primary
   *  input, a primary bid, in a cycle, or sequential.
   */
  bool isValidNode() const;

  //! Get the node mask for these set of flags
  /*! The node mask is a function of the computed flags
   */
  UInt32 getNodeMask() const;

  //! Computes booleans for an element of this node
  void compute(FLNodeElab* flowElab);

private:
  //! Various node flags
  bool mWritable;
  bool mClock;
  bool mPrimaryInput;
  bool mPrimaryBid;
  bool mCycle;
  bool mSequential;

  //! Covered set of elaborated nets for this node
  NUNetElabSet mNetElabs;

  //! Utility class to compute flags
  SCHMarkDesign* mMarkDesign;
}; // class SCHClockTreeNodeHelper

bool SCHClockTreeNodeHelper::isValidNode() const
{
  return (isWritable() || isClock() || isPrimaryInput() || isPrimaryBid() ||
          isCycle() || isSequential());
}

UInt32 SCHClockTreeNodeHelper::getNodeMask() const
{
  UInt32 mask = 0;
  if (isWritable())     { mask |= CB_WRITABLE_MASK;      }
  if (isClock())        { mask |= CB_CLOCK_MASK;         }
  if (isPrimaryInput()) { mask |= CB_PRIMARY_INPUT_MASK; }
  if (isPrimaryBid())   { mask |= CB_PRIMARY_BID_MASK;   }
  return mask;
}

void SCHClockTreeNodeHelper::compute(FLNodeElab* flowElab)
{
  mCycle |= SCHMarkDesign::isCycle(flowElab);
  mSequential |= SCHUtil::isSequential(flowElab);
  NUNetElab* netElab = flowElab->getDefNet();
  if (mNetElabs.insertWithCheck(netElab)) {
    mWritable |= mMarkDesign->isWritable(netElab);
    mClock |= mMarkDesign->isClock(netElab);
    mPrimaryInput |= mMarkDesign->isPrimaryInput(netElab);
    mPrimaryBid |= mMarkDesign->isPrimaryBidi(netElab);
  }
}

SCHClockTreeGraph::Node* 
SCHBranchNets::addNode(GraphBuilder<SCHClockTreeGraph>* builder,
                       const FLNodeElabVector& flows)
{
  // Walk the flows and determine if this is a clock, writable,
  // primary input, primary bid, or multi net node (cycles can be
  // multi net nodes).
  SCHClockTreeNodeHelper nodeHelper(mMarkDesign);
  for (FLNodeElabVectorCLoop l(flows); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    nodeHelper.compute(flowElab);
  }

  // If this is a valid graph node, create the data for these flows
  // and add it to the graph
  SCHClockTreeGraph::Node* node = NULL;
  if (nodeHelper.isValidNode()) {
    // Create the node
    SCHClockTreeGraphData* data = new SCHClockTreeGraphData(flows);
    addNode(data);
    bool added = builder->addNode(data);
    FLN_ELAB_ASSERT(added, data->getRepresentativeFlow());
    node = builder->lookup(data);
    FLN_ELAB_ASSERT(node != NULL, data->getRepresentativeFlow());

    // Set the flags
    UInt32 mask = nodeHelper.getNodeMask();
    mClockTreeGraph->setFlags(node, mask);

    // Add this data to the cache for every elaborated flow
    addToCache(node, data);
  }

  return node;
} // bool SCHBranchNets::createNode

void SCHBranchNets::addNode(SCHClockTreeGraphData* data)
{
  // It should not already exist
  FLN_ELAB_ASSERT(mClockTreeNodes->find(data) == mClockTreeNodes->end(),
                  data->getRepresentativeFlow());
  mClockTreeNodes->insert(data);
}

void
SCHBranchNets::addGraphEdgesRecurse(SCHClockTreeGraph::Node* node,
                                    FLNodeElab* flowElab,
                                    GraphBuilder<SCHClockTreeGraph>* builder,
                                    FLNodeElabSet* covered)
{
  // Check if we have been here before, if so don't continue
  if (!covered->insertWithCheck(flowElab)) {
    return;
  }

  // If this is a cycle, add all its bretheren to the done set too
  if (SCHMarkDesign::isCycle(flowElab)) {
    FLNodeElabVector flows;
    mUtil->getAcyclicFlows(flowElab, &flows);
    for (FLNodeElabVectorLoop l(flows); !l.atEnd(); ++l) {
      FLNodeElab* curFlowElab = *l;
      covered->insert(curFlowElab);
    }
  }

  // Visit the fanin looking for graph nodes
  if (SCHUtil::isSequential(flowElab)) {
    SCHUtil::ConstDriverLoop f;
    for (f = mMarkDesign->loopSequentialClockDrivers(flowElab); !f.atEnd(); ++f) {
      FLNodeElab* faninElab = *f;
      addGraphEdges(node, faninElab, builder, covered);
    }
  } else {
    for (Fanin f = mUtil->loopFanin(flowElab); !f.atEnd(); ++f) {
      FLNodeElab* faninElab = *f;
      addGraphEdges(node, faninElab, builder, covered);
    }
  }
}

void
SCHBranchNets::addGraphEdges(SCHClockTreeGraph::Node* node,
                             FLNodeElab* faninElab,
                             GraphBuilder<SCHClockTreeGraph>* builder,
                             FLNodeElabSet* covered)
{
  // Check if this is an alias of a clock first. If so we redirect the
  // edge to the clock.
  NUNetElab* netElab = faninElab->getDefNet();
  const NUNetElab* clkElab = mMarkDesign->getClockMaster(netElab);
  if ((clkElab != NULL) && (clkElab != netElab)) {
    SCHUtil::ConstDriverLoop l = mUtil->loopNetDrivers(clkElab);
    NU_ASSERT(!l.atEnd(), clkElab);
    FLNodeElab* flowElab = *l;
    SCHClockTreeGraph::Node* faninNode = lookupCache(flowElab);
    FLN_ELAB_ASSERT(faninNode != NULL, flowElab);
    builder->addEdgeIfUnique(node->getData(), faninNode->getData(), NULL);
    return;
  }

  // If this is a graph node, add the edge, otherwise this is an
  // intermediate node: recurse.
  SCHClockTreeGraph::Node* faninNode = lookupCache(faninElab);
  if (faninNode != NULL) {
    builder->addEdgeIfUnique(node->getData(), faninNode->getData(), NULL);

  } else {
    // Recurse, but we should never recurse through a flop. It
    // should have been a graph node. This catches performance
    // issues.
    FLN_ELAB_ASSERT(!SCHUtil::isSequential(faninElab), faninElab);
    addGraphEdgesRecurse(node, faninElab, builder, covered);
  }
} // SCHBranchNets::addGraphEdges

void
SCHBranchNets::addToCache(SCHClockTreeGraph::Node* node,
                          const SCHClockTreeGraphData* data)
{
  // Add it to the cache for each flow.
  for (FLNodeElabVectorCLoop l = data->loopFlows(); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    addToCache(flowElab, node);
  }
} // SCHBranchNets::addToCache

void
SCHBranchNets::addToCache(FLNodeElab* flowElab,
                          SCHClockTreeGraph::Node* node)
{
  // This should not get added more than once. If so, we may be
  // creating multiple SCHClockTreeGraphData nodes for the elaborated
  // flow.
  FLN_ELAB_ASSERT(mNodeCache->find(flowElab) == mNodeCache->end(), flowElab);

  // now we can safely add it
  mNodeCache->insert(NodeCache::value_type(flowElab, node));
}

SCHClockTreeGraph::Node* SCHBranchNets::lookupCache(FLNodeElab* flowElab)
{
  NodeCache::iterator pos = mNodeCache->find(flowElab);
  if (pos != mNodeCache->end()) {
    return pos->second;
  } else {
    return NULL;
  }
}

void SCHBranchNets::createComponentMap()
{
  // Walk the components of the acyclic graph and create a reverse map
  // from any clocks in the component to the acyclic node for it.
  for (Iter<GraphNode*> i = mAcylicClockGraph->nodes(); !i.atEnd(); ++i) {
    // Walk the parts of the component and add them
    GraphNode* graphNode = *i;
    GraphSCC::Component* comp = mGraphSCC.getComponent(graphNode);
    for (GraphSCC::Component::NodeLoop n = comp->loopNodes(); !n.atEnd(); ++n) {
      // if this is a clock, get it, and add the reverse map.
      GraphNode* subGraphNode = *n;
      SCHClockTreeGraph::Node* node = mClockTreeGraph->castNode(subGraphNode);
      const SCHClockTreeGraphData* data = node->getData();
      for (FLNodeElabVectorCLoop l = data->loopFlows(); !l.atEnd(); ++l) {
        FLNodeElab* flowElab = *l;
        NUNetElab* netElab = flowElab->getDefNet();
        if (mMarkDesign->isClock(netElab)) {
          mAcylicNodeMap->insert(AcyclicNodeMap::value_type(netElab, graphNode));
        }
      }
    }
  }
} // void SCHBranchNets::createComponentMap

GraphNode* SCHBranchNets::lookupAcylicNode(const NUNetElab* clkElab)
{
  AcyclicNodeMap::iterator pos = mAcylicNodeMap->find(clkElab);
  NU_ASSERT(pos != mAcylicNodeMap->end(), clkElab);
  return pos->second;
}

BNClockTreeWalker::Command
BNClockTreeWalker::visitNodeBefore(Graph* graph, GraphNode* graphNode)
{
  // Check if this node has already been processed. If so we can
  // return and stop visiting
  BranchNetsData data(mEmptyBranchNets, false);
  if (getCachedData(graph, graphNode, &data)) {
    return GW_SKIP;
  }

  // Otherwise, compute the data from the fanin first. init with empty
  // inputNets and fastClkFound as false to start the accumulation of
  // the data.
  copyToWalker(data);
  return GW_CONTINUE;
} // BNClockTreeWalker::visitNodeBefore

BNClockTreeWalker::Command
BNClockTreeWalker::visitNodeAfter(Graph* graph, GraphNode* graphNode)
{
  // Merge the value from the last child. The result is the value from
  // the fanin nodes.
  BranchNetsData faninData = mergeFaninValue(graph, graphNode);

  // Compute the value for this node from the fanin. This either
  // chooses the fanin value as the set of branch nets or overrides it
  // with the current node's set of branch nets if they are better.
  bool isClock = false;
  BranchNetsData data = computeNodeValue(graphNode, faninData, &isClock);

  // Store the data in the cached area
  copyToWalker(data);
  storeCachedData(graph, graphNode, isClock, faninData, data);
  return GW_CONTINUE;
}

BNClockTreeWalker::Command
BNClockTreeWalker::visitNodeBetween(Graph* graph, GraphNode* node)
{
  // Merge the value from the last child
  mergeFaninValue(graph, node);
  return GW_CONTINUE;
}

BNClockTreeWalker::Command
BNClockTreeWalker::visitCrossEdge(Graph* graph, GraphNode*, GraphEdge* edge)
{
  // Copy the fanins data to the last child area
  GraphNode* faninNode = graph->endPointOf(edge);
  BranchNetsData data(NULL, false);
  bool found = getCachedData(graph, faninNode, &data);
  INFO_ASSERT(found, "Data not stored in cross edge");
  return GW_CONTINUE;
}

void
BNClockTreeWalker::storeCachedData(Graph* graph, GraphNode* graphNode,
                                   bool isClock, const BranchNetsData& faninData,
                                   const BranchNetsData& data)
{
  // Store the data and pass it to our fanout. If this is a clock node
  // and the eIncludeClock flag is false, we always store the fanin
  // value. Otherwise we store the local value (which might be the
  // fanin value). Clocks are special because when the eIncludeClock
  // flag is false, we don't want to return the clock as a branch net
  // for the clock. Since the local node value can be recomputed, we
  // always store the fanin value.
  if (isClock && !SCHBranchNetFlagTest(mFlags, eIncludeClock)) {
    putNodeData(graph, graphNode, faninData, true);
    mNodeBranchData = faninData;
  } else {
    putNodeData(graph, graphNode, data, isClock);
    mNodeBranchData = data;
  }
} // BNClockTreeWalker::storeCachedData

bool 
BNClockTreeWalker::getCachedData(Graph* graph, GraphNode* graphNode,
                                 BranchNetsData* data)
{
  bool isClock = false;
  if (getNodeData(graph, graphNode, data, &isClock)) {
    if (isClock && !SCHBranchNetFlagTest(mFlags, eIncludeClock)) {
      // For clocks, we store their fanin data but possibly return
      // locally computed data. See the comments in storeCacheData for
      // the reason.
      mNodeBranchData = *data;
      *data = computeNodeValue(graphNode, *data, NULL);
    } else {
      mNodeBranchData = *data;
    }
    copyToWalker(*data);
    return true;
  }
  return false;
}

BNClockTreeWalker::BranchNetsData
BNClockTreeWalker::computeNodeValue(GraphNode* graphNode,
                                    const BranchNetsData& faninData,
                                    bool* retIsClock)
{
  // Walk the nodes in this component and compute the combined set of
  // flags and get the elaborated nets
  bool writable = false;
  bool clock = false;
  bool primaryInput = false;
  bool primaryBid = false;
  int netCount = 0;
  GraphSCC::Component* comp = mGraphSCC->getComponent(graphNode);
  NUNetElabSet netElabs;
  NUNetElabSet writableNetElabs;
  NUNetElab* netElab = NULL;
  for (GraphSCC::Component::NodeLoop i = comp->loopNodes(); !i.atEnd(); ++i) {
    // Compute the combine flags
    GraphNode* subNode = *i;
    SCHClockTreeGraph::Node* node = mClockTreeGraph->castNode(subNode);
    writable |= mClockTreeGraph->anyFlagSet(node, CB_WRITABLE_MASK);
    clock |= mClockTreeGraph->anyFlagSet(node, CB_CLOCK_MASK);
    primaryInput |= mClockTreeGraph->anyFlagSet(node, CB_PRIMARY_INPUT_MASK);
    primaryBid |= mClockTreeGraph->anyFlagSet(node, CB_PRIMARY_BID_MASK);

    // Gather the elaborated nets and how many there are
    FLNodeElab* flowElab = node->getData()->getRepresentativeFlow();
    netElab = flowElab->getDefNet();
    if (netElabs.insertWithCheck(netElab)) {
      netCount++;
    }
    if (mClockTreeGraph->anyFlagSet(node, CB_WRITABLE_MASK)) {
      writableNetElabs.insert(netElab);
    }
  }

  // Set the input nets and fast clocks flag. If this node is writable
  // it is computed locally. If it is not writable but the fanin is a
  // fast clock then we prefer the local clock over the fast
  // clock. Otherwise we ignore this node and get the data from the
  // fanin. Start by assuming we use the data from the fanin
  BranchNetsData data = faninData;
  bool isClock = false;
  if (writable) {
    if (!SCHBranchNetFlagTest(mFlags, eMergeWritable)) {
      // Gather the elaborated nets for this node. If there is a single
      // one and it is a clock or PI then we can return that. Start by
      // determining if we have a single def and what it is.
      if ((netCount == 1) && (clock || primaryInput)) {
        data = computeBranchNetsData(netElab);
        isClock = clock;

      } else {
        // It must be a cycle and parts of the cycle are writable. For
        // now we give up.
        data = BranchNetsData(NULL, false);
      }
    }

  } else if (netCount == 1) {
    // This is a singleton net, it is easy to figure out what to do:
    //
    // 1. A primary bid should combine the fanin contribution and the
    //    local net value. The change can come from either outside the
    //    model or from internal to the model.
    //
    // 2. If this is a primary input that is what we should use. The
    //    fanin value should be NULL.
    //
    // 3. If the fanin is NULL or a fast clock and this is a clock, we
    //    should use this nodes net.
    //
    // 4. Otherwise use the fanin value
    //
    const SCHInputNets* branchNets = data.getBranchNets();
    if (primaryBid) {
      // Combine the fanin contribution with this net
      BranchNetsData nodeData = computeBranchNetsData(netElab);
      data = mergeBranchNetsData(data, nodeData);
      isClock = clock;

    } else if (primaryInput ||
        (clock && ((branchNets == NULL) || (branchNets == mEmptyBranchNets) ||
                   data.hasFastClock()))) {
      // Use the local node data and override the fanin data
      NU_ASSERT(!primaryInput || (data.getBranchNets() == NULL) ||
                (data.getBranchNets() == mEmptyBranchNets), netElab);
      data = computeBranchNetsData(netElab);
      isClock = clock;
    }

  } else {
    // We have a cycle. If the fanin is a valid set, use
    // that. Otherwise if it is NULL or a fast clock, only override it
    // if we find a singleton clock. In the future, we may want to
    // relax that. 
    //
    // Also note, that if any of the elaborated nets are bids we give
    // up. This is because we don't understand which fanins should be
    // ignored because they fanin to the bid. This is true no matter
    // what the value of the fanins.
    if (primaryBid) {
      data = BranchNetsData(NULL, false);

    } else if ((data.getBranchNets() == NULL) || data.hasFastClock()) {
      // Look for a singleton clock.
      NUNetElab* clkElab = NULL;
      bool singleClock = true;
      for (NUNetElabLoop l(netElabs); !l.atEnd() && singleClock; ++l) {
        // A primary input should never be in a cycle, I think. If we
        // can, then it should also be counted in the singleton below.
        NUNetElab* netElab = *l;
        NU_ASSERT(!mMarkDesign->isPrimaryInput(netElab), netElab);

        // Check if we have the one and only clock.
        if (mMarkDesign->isClock(netElab)) {
          if (clkElab == NULL) {
            clkElab = netElab;
          } else if (clkElab != netElab) {
            singleClock = false;
          }
        }
      }

      // If we have a singleton clock, override the fanin
      if (singleClock && (clkElab != NULL)) {
        data = computeBranchNetsData(clkElab);
        isClock = true;
      }
    } // } else if
  } // } else

  // If we are merging writable points, then do so now
  if (SCHBranchNetFlagTest(mFlags, eMergeWritable) && writable) {
    const SCHInputNets* branchNets = data.getBranchNets();
    for (Loop<NUNetElabSet> l(writableNetElabs); !l.atEnd(); ++l) {
      NUNetElab* netElab = *l;
      const SCHInputNets* branchNet = mMarkDesign->buildInputNets(netElab);
      branchNets = mMarkDesign->appendInputNets(branchNets, branchNet);
    }
    data = BranchNetsData(branchNets, data.hasFastClock());
  }

  // Return the clock if they asked for it, and the computed value for
  // this node. This is the value we would return to our fanout. Note
  // necessarily the value for this node.
  if (retIsClock != NULL) {
    *retIsClock = isClock;
  }
  return data;
} // BNClockTreeWalker::computeNodeValue

BNClockTreeWalker::BranchNetsData
BNClockTreeWalker::computeBranchNetsData(NUNetElab* netElab)
{
  const SCHInputNets* branchNets = mMarkDesign->buildInputNets(netElab);
  bool fastClock = false;
  if (!SCHBranchNetFlagTest(mFlags, eIgnoreFastClock) &&
      mMarkDesign->isClock(netElab)) {
    fastClock = mMarkDesign->isFastClock(netElab);
  }
  return BranchNetsData(branchNets, fastClock);
}

BNClockTreeWalker::Command
BNClockTreeWalker::visitBackEdge(Graph*, GraphNode*, GraphEdge*)
{
  INFO_ASSERT(false, "Cycle found in acyclic graph!");
  return GW_CONTINUE;
}

void
BNClockTreeWalker::putNodeData(Graph* graph, GraphNode* graphNode,
                               const BranchNetsData& data, bool isClock)
{
  // Store the information in the node.
  UInt32 mask = CB_BRANCH_NETS_VALID_MASK;
  if (data.hasFastClock()) {
    mask |= CB_FAST_CLOCK_MASK;
  }
  if (isClock) {
    mask |= CB_IS_CLOCK_MASK;
  }
  graph->clearFlags(graphNode, CB_ALL_ACYCLIC_FLAGS);
  graph->setFlags(graphNode, mask);
  graph->setScratch(graphNode, (UIntPtr)data.getBranchNets());
}

bool
BNClockTreeWalker::getNodeData(Graph* graph, GraphNode* graphNode,
                               BranchNetsData* data, bool* isClock)
{
  // Check if the node data has been stored. It is up to the caller to
  // assert if it should be valid.
  if (graph->anyFlagSet(graphNode, CB_BRANCH_NETS_VALID_MASK)) {
    UIntPtr value = graph->getScratch(graphNode);
    const SCHInputNets* branchNets = (const SCHInputNets*)value;
    bool fastClock = graph->anyFlagSet(graphNode, CB_FAST_CLOCK_MASK);
    *data = BranchNetsData(branchNets, fastClock);
    if (isClock != NULL) {
      *isClock = graph->anyFlagSet(graphNode, CB_IS_CLOCK_MASK);
    }
    return true;
  } else {
    return false;
  }
}

void BNClockTreeWalker::copyToWalker(const BranchNetsData& data)
{
  mLastNodeBranchData = data;
}

BNClockTreeWalker::BranchNetsData
BNClockTreeWalker::mergeFaninValue(Graph* graph, GraphNode* graphNode)
{
  // get the existing value.
  BranchNetsData data(mEmptyBranchNets, false);
  getNodeData(graph, graphNode, &data, NULL);

  // Combine the data
  data = mergeBranchNetsData(data, mLastNodeBranchData);

  // And store it back and return it
  putNodeData(graph, graphNode, data, false);
  return data;
}

BNClockTreeWalker::BranchNetsData
BNClockTreeWalker::mergeBranchNetsData(const BranchNetsData& one,
                                       const BranchNetsData& two)
{
  // Combine the data. If either the fanin or current node is NULL, it
  // is a failure ==> we give up
  const SCHInputNets* branchNets1 = one.getBranchNets();
  const SCHInputNets* branchNets2 = two.getBranchNets();
  bool fastClock;
  if ((branchNets1 == NULL) || (branchNets2 == NULL)) {
    branchNets1 = NULL;
    fastClock = false;
  } else {
    branchNets1 = mMarkDesign->appendInputNets(branchNets1, branchNets2);
    fastClock = one.hasFastClock() | two.hasFastClock();
  }
  return BranchNetsData(branchNets1, fastClock);
}
