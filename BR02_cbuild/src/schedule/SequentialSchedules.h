// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  Class to hold data in the creation of sequential schedules
*/

#ifndef _SEQUENTIALSCHEDULES_H_
#define _SEQUENTIALSCHEDULES_H_

#include "SequentialBlockMerge.h"
#include "AddBuffer.h"

class SCHBranchNets;

//! class SCHSequentialSchedules
class SCHSequentialSchedules
{
public:
  //! constructor
  SCHSequentialSchedules(SCHUtil* util, SCHMarkDesign* markDesign,
                         SCHScheduleData* data, SCHDump* dump,
                         SCHTimingAnalysis* timingAnalysis);

  ~SCHSequentialSchedules();

  //! Add a sequential node to be scheduled.
  void schedule(FLNodeElab*, SCHSequentials*, SCHDerivedClockLogic*);

  //! Create the sequential schedules from the sequential nodes
  bool create(const char* fileRoot);

  //! Put the finishing touches on the sequential schedules
  /*! This routine should only be called once the live/dead sequential
   *  nodes is stable. Combinational optimizations might make some
   *  sequentials dead and so there are things we can't optimize until
   *  that is complete. These are done in this routine.
   */
  void finish(SCHSequentials*);

private:
  //! Adjust timing on new combo elaborated flow on fanin
  void fixupCombFlow();

  typedef UtMap<const SCHSequential*, UInt32> DepthMap;

  // Type to keep track of a clock and edge pair
  class SeqClkAndEdge
  {
  public:
    //! constructor
    SeqClkAndEdge(SCHSequential* seq, NUNetElab* clk, ClockEdge edge) :
      mSequential(seq), mClk(clk), mEdge(edge) {}

    //! destructor
    ~SeqClkAndEdge() {}

    //! Get the sequential
    SCHSequential* getSequential() const { return mSequential; }

    //! Get the clock
    NUNetElab* getClock() const { return mClk; }

    //! Get the edge
    ClockEdge getEdge() const { return mEdge; }

  private:
    SCHSequential* mSequential;
    NUNetElab* mClk;
    ClockEdge mEdge;
  };

  // Private class to create sequential block ordering
  class Block
  {
  private:
    // A sorted set of flows so that traversal is consistent
    struct CmpBlock
    {
      bool operator()(const Block* b1, const Block* b2)	const
      {
	return SCHUtil::lessFlow(b1->mFlow, b2->mFlow);
      }
    };

    friend struct CmpBlock;

  public:
    //! constructor
    Block(FLNodeElab* flow);

    //! destructor
    ~Block();

    //! Add a fanin to this block
    void addFanin(Block* faninBlock, FLNodeElab* flow);

    //! Remove a fanin from this block
    void removeFanin(Block* faninBlock, FLNodeElab* flow);

    //! The block fanin set
    typedef UtMap<Block*, FLNodeElabSet*, CmpBlock> Fanin;

    //! Iterator for a flow vector
    typedef LoopMap<Fanin> FaninLoop;

    //! Loop the fanin of this block
    FaninLoop loopFanin() const;

    //! Set the busy flag
    void putBusy(bool busy) { mBusy = busy; }

    //! Get the busy flag
    bool getBusy() const { return mBusy; }

    //! Set the done flag
    void putDone(bool done) { mDone = done; }

    //! Get the done flag
    bool getDone() const { return mDone; }

    //! Get this blocks depth
    int getDepth() const;

    //! Get the flow node that we are using for this block
    FLNodeElab* getFlow() const { return mFlow; }

    //! Debug routine to print info
    void print(SCHUtil* util) const;

  private:
    FLNodeElab* mFlow;
    Fanin* mFanin;
    bool mBusy;
    bool mDone;
  }; // class Block
  friend class Block;


  // Class for an arc between two schedules. This is so that we
  // can order the sequentials such that we have less state updates
  friend class Flow;

  class Flow
  {
  public:
    //! constructor
    Flow(SCHSequential* seq, UInt32 weight) :
      mSeq(seq), mWeight(weight)
    {}

    //! Get the sequential schedule
    SCHSequential* getSchedule() const { return mSeq; }

    //! Get the weight for this arc
    UInt32 getWeight() const { return mWeight; }

    //! Set the weight for this arc
    void addWeight(UInt32 weight) { mWeight += weight; }

    //! Clear the weight (this is like removing the arc in a cheap way)
    void clearWeight() { mWeight = 0; }

  private:
    SCHSequential*	mSeq;	// The fanin/fanout sequential schedule
    UInt32		mWeight;// The number of flows that fanin/fanout
  };


  // Used to sort sequential schedules
  enum ScheduleOrder
  {
    eOrderBefore,	//!<The first schedule comes before the second
    eOrderSame,		//!<They are in the same schedule
    eOrderAfter,	//!<The first schedule comes after the second
    eExclusive		//!<They cannot execute at the same time
  };

  //! Private class to keep sequential schedule/edge pairs
  class SequentialAndEdge
  {
  public:
    //! constructor
    SequentialAndEdge(SCHSequential* seq, ClockEdge edge) :
      mSequential(seq), mEdge(edge) { mRefCnt = 0; }

    //! destructor
    ~SequentialAndEdge() {}

    //! Get the sequential schedule
    SCHSequential* getSchedule() const { return mSequential; }

    //! Get the edge
    ClockEdge getEdge() const { return mEdge; }

    //! Increment the reference count
    void incRefCnt() { ++mRefCnt; }

    //! Decrement and return the reference count
    int decRefCnt() { return --mRefCnt; }

  private:
    SCHSequential* mSequential;
    ClockEdge mEdge;
    int mRefCnt;
  };

  // Private type to help with sequential ordering. We need to mark
  // all elaborated flow nodes for a single unelaborated flow node as
  // state update.
  typedef UtMap<NUUseDefNode*, FLNodeElabSet*> BlockInstMap;
  typedef BlockInstMap::iterator BlockInstMapIter;
  BlockInstMap* mBlockInstMap;

  // Types and date to find all the elaborated flow nodes for a given
  // unelaborated flow. We use this to mark all instances of a flow
  // pair as double buffered.
  typedef UtMap<FLNode*, FLNodeElabSet*> FlowInstMap;
  typedef FlowInstMap::iterator FlowInstMapIter;
  FlowInstMap* mFlowInstMap;

  // Private class to keep sorted flow nodes for every sequential
  // always block instance
  class SortedSequentialFlows
  {
  public:
    //! constructor
    SortedSequentialFlows() { mRefCnt = 0; }

    //! destructor
    ~SortedSequentialFlows() {}

    //! Access function for node vector
    FLNodeElabVector* getNodes() { return &mNodeVector; }

    //! Bump the ref count
    void incRefCnt() { ++mRefCnt; }

    //! Decrement the ref count and return it
    int decRefCnt() { return --mRefCnt; }
  private:
    FLNodeElabVector mNodeVector;
    int mRefCnt;
  }; // class SortedSequentialFlows
  typedef UtMap<FLNodeElab*, SortedSequentialFlows*> SortedSequentialFlowsMap;

  // Types to keep track of the blocks is a cycle and the weight of
  // the arc
  typedef std::pair<Block*, UInt32> ArcWeight;
  typedef UtVector<ArcWeight> ArcStack;
  ArcStack* mArcStack;

  // The set of blocks in the various schedules
  typedef UtMap<SCHUseDefHierPair, Block*> Blocks;
  typedef Blocks::iterator BlocksIter;
  Blocks* mBlocks;

  // A map from flow nodes to the block that represents them
  typedef UtMap<const FLNodeElab*, Block*> FlowToBlockMap;
  FlowToBlockMap* mFlowToBlockMap;

  // The set of flow node pairs that need to be buffered and the reason
  enum DBReason {
    eNotDoubleBuffered,
    eMultiClk,
    eCycle
  };
  typedef std::pair<FLNode*, NUNet*> DBEdge;
  typedef UtMap<DBEdge, DBReason> DoubleBufferPairs;
  DoubleBufferPairs* mDoubleBufferPairs;
  DBReason isDoubleBuffered(FLNode* fanout, NUNet* net) const;
  bool isDoubleBuffered(FLNodeElab* flow, FLNodeElab* fanin) const;
  void addDoubleBuffered(FLNode* fanout, NUNet* net, DBReason reason);
  void addDoubleBuffered(FLNodeElab* flow, FLNodeElab* fanin, DBReason reason);
  void findFaninNet(FLNodeElab* fanoutElab, FLNodeElab* faninElab,
                    NUNetVector* nets) const;

  // Data structures to remember the reason why we added a buffer
  // between two elaborated flows
  typedef std::pair<FLNodeElab*, FLNodeElab*> FlowElabPair;
  typedef UtMap<FlowElabPair, DBReason> DoubleBufferReason;
  DoubleBufferReason* mDoubleBufferReason;
  void addDoubleBufferReason(FLNodeElab*, FLNodeElab*, DBReason);
  DBReason doubleBufferReason(FLNodeElab*, FLNodeElab*) const;
  bool bufferAdded(FLNodeElab* flowElab, FLNodeElab* faninElab) const;

  // Class to validate a flow order
  class ValidSeqOrder : public SCHSequentialBlockMerge::ValidOrder
  {
  public:
    //! constructor
    ValidSeqOrder(SCHSequentialSchedules* sequentialSchedules) :
      mSequentialSchedules(sequentialSchedules)
    {}

    //! Function to test if this is a valid dependency
    bool operator()(FLNodeElab* fanout, FLNodeElab* fanin) const;

  private:
    SCHSequentialSchedules* mSequentialSchedules;
  };
  friend class ValidSeqOrder;

  // Private functions
  void createSortedSequentialFlowMap();
  void deleteSortedSequentialFlowMap();
  bool sequentialBlocksInMultipleSchedules();
  void addFlowsToSet(SCHSequential* seq, SCHNodeSet* flowSet);
  bool findFlowsInSet(SCHSequential* seq, SCHNodeSet* flowSet);
  bool findFlowInSet(FLNodeElab* flow, SCHNodeSet* flowSet);
  void removeDuplicateSequentialNodes();
  void determineNodeOrder(NUNetElabSet* delayedNets);
  void calculateSequentialOrdering();
  void findInterScheduleDoubleBuffers();
  void findBlockCycleDoubleBuffers();
  void createSequentialBlockFlow();
  void deleteSequentialBlockFlow();
  void addAllResetBuffers(SCHClkAnal* clkAnal);
  void addDoubleBuffers(NUNetElabSet* delayedNets);
  Block* breakSequentialCycles(Block* block);
  Block* findBreakPoint(Block* block, UInt32 weight, Block* endBlock);
  void recomputeBlockDepth();
  int  computeBlockDepth(Block* block);
  void addDoubleBufferedBlocks(Block* block, FLNodeElabSet* faninSet);
  ScheduleOrder determineScheduleOrder(FLNodeElab*, ClockEdge, SCHSequential*) 
    const;
  ScheduleOrder determineScheduleOrder(FLNodeElab*, FLNodeElab*) const;
  void addFlowsToScheduleMap(SCHSequential* seq);
  void addFlowToScheduleMap(FLNodeElab*, SequentialAndEdge*);
  bool printMultiScheduleBlocks();
  void createFlowToScheduleMap();
  void deleteFlowToScheduleMap();
  void createBlockInstMap();
  void deleteBlockInstMap();
  void createFlowInstMap();
  FLNodeElabSet* getFlowInstances(FLNode* flow);
  NUNetElab::DriverLoop loopElaboratedFanin(FLNodeElab*, NUNet*) const;
  void deleteFlowInstMap();
  bool validSequentialFanin(FLNodeElab* flow, FLNodeElab* fanin) const;
  UInt32 computeWeight(Block* block, FLNodeElabSet* faninSet,
		       FLNodeElabSet& removeSet);

  // Marks all the nets for a flow node or an always block for a given
  // flow node as double buffered.
  void markFlowDoubleBuffered(FLNodeElab* flow);
  void markAllBlockOutputsDoubleBuffered(FLNodeElab* flow);

  // Use to find the (existing) sequential schedule for a sequential node
  SCHSequential* findSequentialSchedule(FLNodeElab* flow, ClockEdge* edge) const;


  friend struct CompareFlow;
  friend class SCHMsgCallback;

  // Compare structure to add remove arcs
  struct CompareFlow
  {
    bool operator()(const Flow* f1, const Flow* f2) const;
  };

  // Used to figure out the order of all sequential schedules. We do
  // this so that we can reduce the number of state updates
  UInt32 mOrder;
  typedef UtSet<SCHSequential*> Sequentials;
  typedef UtVector<Flow*> SequentialStack;
  SequentialStack* mSequentialStack;
  Sequentials* mSequentialsInStack;

  DepthMap* mDepthMap;
  typedef UtSet<Flow*, CompareFlow> FlowSet;
  typedef Loop<FlowSet> FlowLoop;
  typedef UtMap<SCHSequential*, FlowSet*> SequentialFlow;
  SequentialFlow* mSequentialFlow;

  // A sequential schedule sorting structure
  class CmpSequentials
  {
  public:
    CmpSequentials(DepthMap* depthMap) : mDepthMap(depthMap) {}
    bool operator()(const SCHSequential* s1, const SCHSequential* s2) const;
  private:
    DepthMap* mDepthMap;
  };

  // General utilities for various passes
  typedef UtMap<FLNodeElab*, SequentialAndEdge*> FlowToSeqScheduleMap;
  FlowToSeqScheduleMap mFlowToSeqScheduleMap;
  SortedSequentialFlowsMap mSortedSequentialFlowsMap;

  // Used to sort sequential schedules so taht we reduce state updates
  void determineOrder(SCHSequentials* sequentials, SCHBranchNets* branchNets);
  Flow* breakSequentialCycles(Flow*, Sequentials&);
  UInt32 setSequentialDepth(SCHSequential*, SCHSequentials*, Sequentials&);
  void determineSequentialDependencies(SCHSequential* seq, FLNodeElab* flow,
				       FLNodeElabSet&, Sequentials&);
  void addRunBeforeSchedule(SCHSequential* after, SCHSequential* before,
			    UInt32 weight);
  FlowLoop loopRunBeforeSchedules(SCHSequential* seq);
  void deleteFlow();



  // Use to compare schedule and edge.
  // Blocks with the same schedule and edge are considered to be identical
  // even if the clk pins are different (same canonical clk though).
  struct CmpSeqClkAndEdge
  {
    bool operator()(const SeqClkAndEdge* se1, const SeqClkAndEdge* se2) const
    {
      SCHSequential* s1 = se1->getSequential();
      SCHSequential* s2 = se2->getSequential();
      if ((se1 == se2) || ((s1 == s2) && (se1->getEdge() == se2->getEdge()))) {
	return false;
      }

      int cmp = s1->compareSchedules(s2);
      if (cmp == 0) {
	ClockEdge ce1 = se1->getEdge();
	ClockEdge ce2 = se2->getEdge();
	cmp = (int)ce1 - (int)ce2;
      }
      SCHED_ASSERT2(cmp != 0, s1, s2);
      return cmp < 0;
    }
  };

  // Types and data for sequential block merging
  typedef UtSet<NUUseDefNode*> MergeBlocks;
  typedef UtMap<int, MergeBlocks*> MergeSets;
  typedef UtMap<SeqClkAndEdge*, MergeSets*, CmpSeqClkAndEdge> SchedMergeSets;
  typedef UtMap<HierName*, SchedMergeSets*> HierMergeSets;
  typedef UtMap<const NUModule*, HierMergeSets*> ModuleMergeSets;
  ModuleMergeSets* mModuleMergeSets;
  typedef UtVector<MergeBlocks*> BlockSets;
  typedef UtSet<SeqClkAndEdge*, CmpSeqClkAndEdge> UniqueClockEdges;
  UniqueClockEdges* mUniqueClockEdges;

  // Iterator and value short cuts
  typedef MergeSets::iterator MergeSetsIter;
  typedef MergeSets::value_type MergeSetsValue;
  typedef SchedMergeSets::iterator SchedMergeSetsIter;
  typedef SchedMergeSets::value_type SchedMergeSetsValue;
  typedef HierMergeSets::iterator HierMergeSetsIter;
  typedef HierMergeSets::value_type HierMergeSetsValue;
  typedef ModuleMergeSets::iterator ModuleMergeSetsIter;
  typedef ModuleMergeSets::value_type ModuleMergeSetsValue;

  // Use to find/split sequential blocks in multiple schedules
  SCHBlockSplit* mBlockSplit;

  // Used to access general scheduler data and functions
  SCHUtil* mUtil;
  SCHMarkDesign* mMarkDesign;
  SCHScheduleData* mScheduleData;
  SCHCreateSchedules* mCreate;
  SCHDump* mDump;
  SCHTimingAnalysis* mTimingAnalysis;
}; // class SCHSequentialSchedules

#endif // _SEQUENTIALSCHEDULES_H_
