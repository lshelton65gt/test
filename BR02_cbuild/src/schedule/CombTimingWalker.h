// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _COMBTIMINGWALKER_H_
#define _COMBTIMINGWALKER_H_

#include "util/Graph.h"
#include "util/GenericDigraph.h"
#include "util/GraphSCC.h"

class SCHMarkDesign;
class SCHBlockFlowNodes;

//! SCHCombTimingWalker class
/*! This class provides a mechanism to do a DFS walk over an acyclic
 *  version of the timing-based combinational flow node graph. In
 *  doing so it can compute information for every flow node in the
 *  graph. This is accomplished by creating a potentially cyclic graph
 *  of all the combinational flow nodes, converting it to a strongly
 *  connected component graph, and then walking the graph.
 *
 *  Timing analysis is done either by propagating from fanin to
 *  fanouts or from fanout to fanins. There are two initiate
 *  functions, one for each graph direction.
 *
 *  For the fanout to fanin graph, the leafs of the graph can be
 *  either primary inputs or sequential flow nodes. The roots are
 *  combinational nodes with no combinational fanout.
 *
 *  For the fanin to fanout graph, the leafs are either outputs or
 *  sequential flow nodes. The roots are either inputs or sequential
 *  nodes. To avoid creating cycles in the graph through sequential
 *  nodes, the graph node s for sequential nodes are duplicated. One
 *  represents the root and the other represents the leaf.
 *
 *  After initiating the walker, the walk function should be used to
 *  traverse the graph and computing the timing for every node.
 *
 *  Three virtual callbacks must be overriden to do the work during
 *  the walk. They are getValue, mergeValues, and storeValue. The
 *  first computes the value for the given elaborated flow node and
 *  returns it as a UIntPtr. The second takes two values as UIntPtr's
 *  and returns the resulting merged value. The last stores the final
 *  result for a given elaborated flow node.
 *
 *  This class was not derived from the GraphWalker class because if
 *  we did so, the constructor would have to create the cyclic and
 *  acyclic graphs. This method allows all the work to be done in the
 *  main walk function.
 */
class SCHCombTimingWalker
{
public:
  //! constructor
  SCHCombTimingWalker(SCHMarkDesign* markDesign, SCHUtil* util);

  //! destructor
  virtual ~SCHCombTimingWalker();

  //! Create a graph from fanouts to fanins
  /*! This graph is created using
   *  SCHMarkDesign::loopTimingDependencies. It allows the caller to
   *  propagate information from fanins to fanouts through a DFS walk.
   *
   *  It is illegal to call this function multiple times or in
   *  combination with any other graph creation call. An assert checks
   *  for these conditions.
   */
  void createFaninGraph();

  //! Create a graph from fanins to fanouts
  /*! This graph is created using SCHMarkDesign::loopTrueFanin. Since
   *  it creates a graph from fanins to fanouts, the transpose of the
   *  graph is created. It allows the caller to propagate information
   *  from fanouts to fanins through a DFS walk.
   *
   *  It is illegal to call this function multiple times or in
   *  combination with any other graph creation call. An assert checks
   *  for these conditions.
   */
  void createFanoutGraph();

  //! Main function to initiate the walk
  void walk();

  //! getValue callback to compute a graph nodes value
  /*! The callback should compute the value for this flow node
   *  irrespective of its fanin. If the value for this flow node is
   *  purely a function of its fanin it should return a representation
   *  for the empty or uninitialized value. NULL is acceptible.
   *
   *  \param flow The elaborated flow for which to compute its value
   *
   *  \return The value for this elaborated flow
   */
  virtual UIntPtr getValue(FLNodeElab* flow) = 0;

  //! mergeValue callback to merge two values together
  /*! The callback should merge the current value for two values and
   *  return the result. This can get called when merging the value
   *  either because of a fanin or cyclic component relationship.
   *
   * \param value1 The first value to merge
   *
   * \param value2 The second value to merge
   *
   *  \returns the resulting merged value
   */
  virtual UIntPtr mergeValues(UIntPtr value1, UIntPtr value2) = 0;

  //! storeValue callback to store the final result for a graph node
  /*! Store the final result for an elaborated flow node. This routine
   *  returns the value that should be seen by parent nodes. In most
   *  cases this is the same as the input value. But some algorithms
   *  may want to store a different value than it returns to its
   *  parents.
   *
   *  \param flow The elaborated flow for which to store the value
   *
   *  \param value The value to store
   *
   *  \returns The value to pass to parent nodes
   */
  virtual UIntPtr storeValue(FLNodeElab* flow, UIntPtr value) = 0;

protected:
  // Local utility pointers. They are useful to the derived classes as
  // well.
  SCHMarkDesign* mMarkDesign;
  SCHUtil* mUtil;

private:
  // Local types used to create the graph
  typedef GenericDigraph<void*, FLNodeElab*> CombFlowGraph;
  typedef CombFlowGraph::Node CombFlowNode;
  typedef CombFlowGraph::Edge CombFlowEdge;
  typedef UtMap<FLNodeElab*, CombFlowNode*> CombFlowNodeMap;

  // Private class to walk the SCC graph
  class CombWalker : public GraphWalker
  {
  public:
    //! constructor
    CombWalker(GraphSCC* graphSCC, CombFlowGraph* flowGraph,
               SCHCombTimingWalker* combTimingWalker) :
      mGraphSCC(graphSCC), mFlowGraph(flowGraph),
      mCombTimingWalker(combTimingWalker)
    {
      mLastChildData = 0;
    }

    //! Override the visitNodeBefore to set the initial data
    Command visitNodeBefore(Graph* graph, GraphNode* node);

    //! Override the visitNodeAfter to merge fanin data and copy to last child
    Command visitNodeAfter(Graph* graph, GraphNode* node);

    //! Override the visitNodeBetween to merge fanin data
    Command visitNodeBetween(Graph* graph, GraphNode* node);

    //! Override the visitCrossEdge to copy to last child
    Command visitCrossEdge(Graph* graph, GraphNode* node, GraphEdge* edge);

    //! Override the visitBackEdge to make sure there are no cycles
    Command visitBackEdge(Graph* graph, GraphNode* node, GraphEdge* edge);

  private:
    // The last childs data
    UIntPtr mLastChildData;

    // Private utility functions that are called from various visit
    // routines above.
    UIntPtr mergeFaninValue(Graph* graph, GraphNode* node);
    void copyToWalker(UIntPtr value);

    // Utility pointers
    GraphSCC* mGraphSCC;
    CombFlowGraph* mFlowGraph;
    SCHCombTimingWalker* mCombTimingWalker;
  }; // class CombWalker : public GraphWalker
  friend class CombWalker;

  // Private functions to create the graph
  void createCombFaninGraph(CombFlowNodeMap* flowNodeMap);
  CombFlowNode* createCombFaninGraphNodes(FLNodeElab* flowElab,
                                          CombFlowNodeMap* flowNodeMap);
  void createCombFanoutGraph(CombFlowNodeMap* flowNodeMap,
                             CombFlowNodeMap* seqNodeMap);
  CombFlowNode* createCombFanoutGraphNodes(FLNodeElab* flowElab,
                                           CombFlowNodeMap* flowNodeMap);
  void createFanoutEdge(FLNodeElab* flowElab, FLNodeElab* faninElab,
                        CombFlowNodeMap* flowNodeMap,
                        CombFlowNode* seqFlowNode);
  void getGraphFlows(FLNodeElab* flowElab,
                     FLNodeElabVector* graphNodeFlows) const;

  // The graph created for traversal
  CombFlowGraph* mFlowGraph;

  //! Class to be able to go from an always block to all its flows
  SCHBlockFlowNodes* mBlockFlowNodes;
}; // class SCHCombTimingWalker
  
#endif // _COMBTIMINGWALKER_H_
