// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _SEQFANOUT_H_
#define _SEQFANOUT_H_

#include "UseDefHierPair.h"

//! Abstraction to keep track of a combos sequential fanout
/*! This class contains information about a sequential fanout. It is
 *  used by block splitting and block merging to reduce the number of
 *  blocks.
 *
 *  A sequential fanout is made up of the
 *  (NUUseDefNode*)/(STBranchNode*) pair, and the sequential schedule
 *  and edge. If a combinational block has only one sequential fanout
 *  using these characteristics, it is mergable with the sequential.
 */
class SCHSeqFanout
{
public:
  //! constructor
  SCHSeqFanout(SCHUseDefHierPair udHierPair, SCHScheduleBase* schedBase) :
    mUseDefHierPair(udHierPair), mSchedBase(schedBase)
  {}

  //! Empty constructor
  SCHSeqFanout() : mUseDefHierPair(SCHUseDefHierPair(NULL, NULL)),
                mSchedBase(NULL)
  {}

  //! Get the schedule
  SCHScheduleBase* getScheduleBase(void) const { return mSchedBase; }

  //! Comparison
  bool operator==(const SCHSeqFanout& sf) const
  {
    return ((mUseDefHierPair == sf.mUseDefHierPair) &&
            (mSchedBase == sf.mSchedBase));
  }

  //! Compare function for sorting
  static int compare(const SCHSeqFanout& sf1, const SCHSeqFanout& sf2)
  {
    int cmp = 0;
    cmp = SCHUseDefHierPair::compare(sf1.mUseDefHierPair, sf2.mUseDefHierPair);
    if (cmp == 0) {
      cmp = SCHScheduleBase::compare(sf1.mSchedBase, sf2.mSchedBase);
    }
    return cmp;
  }

  //! operator< for sets
  bool operator<(const SCHSeqFanout& other) const
  {
    return compare(*this, other) < 0;
  }

private:
  SCHUseDefHierPair mUseDefHierPair;
  SCHScheduleBase* mSchedBase;
};

#endif // _SEQFANOUT_H_
