// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
//
// To do (from code review 10/15/03)
//       (linenumbers are for version 1.1 of this file)
//
// ** Q3 you create copies of expressions but to not size them
//    immediately (bitExpr in some (perhaps all places) the sizing is
//    done but it is not done immediately, suggestion that the sizing be
//    done immediately so that hidden problems do not crop up later
//
// ** C1 line 411 the size info is lost The output of this function
//    (mExprFactory and elaborateExpr) is size 1 which makes this code
//    not general, if this code is converted to expression synthesis then
//    this must be addressed
//
//  ** Q4 line 393 why will the loop stop, is it needed? or should it
//     be handled by expr synthesis (with just a change to call back to
//     limit end of chain?)
//
//  ** Q5 line 339 (elabBitFlow "if(!local->isFlop())" lots of clock
//     specific code here, will need to be generalized for inclusion in
//     expr synthesis
//
//  ** Q6 is code line 420-45 same as 400-410?  Refactor A: must also
//     consider hackBitsel has different phases
//
//  ** lines 480-495 should be refactored
//     C method elaborateAssign does not handle concat
//     Also, we don't simplify part-selects on the RHS.
//
//  ** C line 68 should add an NU_ASSERT to catch case where *ret is
//     already same as driver.
//
//  ** C line 170 this code does not handle nested named blocks, and
//     at least some should be easy to handle
//
//  ** C line 190 dynamic bit select should be put somewhere else,
//     make it a special method of NU bitsel.
//
//  ** C about line 215 create expr but not sizing them. Resize on
//     create so code can be reused. This code is prob ok but just
//     do it near the source to eliminate confusion.
//
//  ** C general comment on returning more than one result from a
//     method, especially when you have a group of related methods.  use
//     a single structure, that holds all returned values and

#include "ClkAnal.h"
#include "MarkDesign.h"
#include "Util.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "iodb/IODBNucleus.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCopyContext.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUExprFactory.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUUseDefNode.h"
#include "reduce/Fold.h"
#include "schedule/Schedule.h"
#include "symtab/STSymbolTable.h"
#include "util/CbuildMsgContext.h"
#include "util/LoopMap.h"
#include "util/StringAtom.h"
#include "util/UtDebugRecursion.h"
#include "util/UtIOStream.h"
#include "util/UtMap.h"
#include <algorithm>

UtDEBUG_STACK(sClkVecDebug)

SCHClkAnal::BitselType SCHClkAnal::analyzeBitsel(NUNet* net,
                                                 const NUExpr* index,
                                                 const ConstantRange* subrange,
                                                 SInt32* constIndex)
{
  *constIndex = 0;
  NUVectorNet* vn = dynamic_cast<NUVectorNet*>(net);
  ConstantRange range(0, 0);
  if (vn != NULL) {
    range = *vn->getRange();
  }
  const NUConst* k = index->castConst();

  if (k == NULL) {
    // dynamic bitselect.
    if (range.getLength() == 1) {
      // bitselect of a scalar, e.g. test/clock-aliasing/bitsel_scalar.v
      // special case of a bitselect (of the LSB) of a scalar, select that
      // scalar value.  Note that the dynamic bitselect may be out of range
      // (it may even be provably *always* out of range) but we will still
      // treat the return value as equivalent to the 1-bit net without
      // the bit-select.  This is because Carbon chooses whatever value
      // is most convenient for X -- it doesn't have to return 0.
      return eBitselNet;
    }
    return eBitselDynamic;
  }
  SInt64 kindex;
  if ((k != NULL) && k->getLL(&kindex) && (kindex <= UtSINT32_MAX)) {
    kindex += subrange->getLsb ();
    *constIndex = (SInt32) kindex;
    if (!range.contains(kindex)) {
      return eBitselOutOfRange;
    }
    if (range.getLength() == 1) {
      return eBitselNet;
    }
    return eBitselConstIndex;
  }
  return eBitselDynamic;
} // SCHClkAnal::BitselType SCHClkAnal::analyzeBitsel

NUExpr* SCHClkAnal::kbitSelect(const NUExpr* ident, const NUExpr* index,
                               SInt32 bit, UInt32 size)
{
  NUExpr* expr;

  if (index == NULL)
    expr = mFold->createPartsel (ident, ConstantRange (bit, bit), ident->getLoc ());
  else
    expr = mFold->createBitsel(ident, index, ident->getLoc());

  expr->resize (size);
  return expr;
} // NUExpr* SCHClkAnal::kbitSelect

// sees if the specified bit of the specified net is driven by an
// assign statement in 'driver'.  If so, *ret is modified to get that
// driver.  Multiple drivers are not allowed, and if one is found
// (previous value of *ret is non-null) then false is returned.  false
// is also returned if it appears that net might be driven by the
// specified flow-node in some fashion besides a bit-select assign.
//
// If the elaboration fails, the return value may be true, but
// *newExpr will be NULL, so when you call this routine, you must
// check that the return value is true AND that *newExpr != NULL
// to be sure that the method succeeded.
bool SCHClkAnal::elaborateAssign(FLNodeElab* driver, NUNetElab* net,
                                 UInt32 offset, FLNodeElab** ret,
                                 const NUExpr** newExpr)
{
  // If this is a primary input/bidirect, then return an expression
  // for the net; it is a free variable and a stopping point.
  if (driver->getType() == eFLBoundPort) {
    const SourceLocator& loc = net->getNet()->getLoc();
    NUExpr *new_expr = new NUIdentRvalueElab(net, loc);
    new_expr = new NUVarselRvalue(new_expr, ConstantRange(offset, offset), loc);
    new_expr->resize(1);
    *newExpr = mExprFactory->insert(new_expr);
    *ret = driver;
    return true;
  } else if (driver->isBoundNode()) {
    // Any other bound types should cause the analysis to fail.
    return false;
  }
  NUUseDefNode* node = driver->getUseDefNode();
  FLN_ELAB_ASSERT(node, driver);
  NUAssign* assign = dynamic_cast<NUAssign*>(node);
  bool unexpectedWriter = true;

  if (assign != NULL)
  {
    NUVarselLvalue*  vsellv = dynamic_cast<NUVarselLvalue*>(assign->getLvalue());
    NUIdentLvalue*   ilv = dynamic_cast<NUIdentLvalue*>(assign->getLvalue());

    if ((vsellv != NULL) && vsellv->isArraySelect())
      return false;  // select of a memory row
    
    if ((vsellv != NULL) && (vsellv->getRange ()->getLength () == 1))
    {
      // this assignment drives a single bit

      if (not vsellv->isConstIndex ())
        return false;        // dynamic bit-select on LHS

      ConstantRange range (*vsellv->getRange());
      
      NUNet* ident = vsellv->getIdentNet();
      NU_ASSERT (ident->isVectorNet(), ident);

      // Now check to see that we are looking at the correct bit
      // The scope passed into lookupElab here must be the scope
      // of the flow-node (driver), not the scope of 'net', because
      // we are examining an identifier in a nucleus expression.
      // (cf. bug2556, bug1638)
      //
      if (((UInt32)range.getLsb () == offset) and
          (ident->lookupElab(driver->getHier()) == net))
      {
        if (*ret != NULL) {
          return false;       // multi-driver
        }
        unexpectedWriter = false; // we have a parsable x[K] = y;

        *newExpr = elaborateExpr(driver, assign->getRvalue(), ret, false);
      } // if
    } // if

    // Disallow a dynamic index on the lhs.
    else if ( ((vsellv != NULL) and vsellv->isConstIndex()) or (ilv != NULL) )
    {
      // if vsellv then this is an assignment to a partselect
      //        (which must include the bit we want) 
      // if ilv then this might drive a superset of the bits we want
      //        (so we can synthesize an NUBitselRvalue based on the
      //         RHS vector expression, plus the bit we want.)
      NUNet* lhs = 0;
      UInt32 relative_offset = offset;
      if (ilv) {
        lhs = ilv->getIdent();
      } else {
        // bug 4746 - The offset passed in to this routine is relative to
        // the whole net.  In order to get the correct part of the rhs,
        // need to make that offset relative this lhs.  For instance,
        // say we want clkvec[1], if we have:
        //   clkvec[2:1] = rvalue;
        // We need offset 0 of "rvalue", but the incoming offset is 1.
        const ConstantRange *range = vsellv->getRange();
        if (range->contains(offset)) {
          relative_offset = range->offsetBounded(offset);
          lhs = vsellv->getIdentNet();
        } else {
          // Not within range; the lhs doesn't drive the bit we are looking for.
          lhs = 0;
        }
      }

      if (lhs) {
        NUExpr* rhs = assign->getRvalue();
        if (rhs->getBitSize() > relative_offset) {
          // the bit we want is included in LHS and RHS
          // so we create the necessary bitselect
          NUExpr* bitsel = kbitSelect(rhs, NULL, relative_offset, 1);
          bitsel = mFold->fold(bitsel);
          FLNodeElab* bitselFlow = NULL;
          const NUExpr* nexpr = elaborateExpr(driver, bitsel,
                                              &bitselFlow, false);
          delete bitsel;

          // Did we find a potential driver?
          if ((bitselFlow != NULL) && (nexpr != NULL)) {
            // Check for multiple drivers
            if ((*newExpr != NULL && (nexpr != *newExpr)) ||
                ((*ret != NULL) && (bitselFlow != *ret)))
            {
              return false;       // multi-driver;
            }

            // Success -- pass back the flow and expr
            *newExpr = nexpr;
            *ret = bitselFlow;
            unexpectedWriter = false;
          }
        }
      }
    }
  } // if
  
  return !unexpectedWriter;
} // bool SCHClkAnal::elaborateAssign

// Given a vector bit, find an equivalent source net.  This is a very
// specialized routine, which is designed to handle the scan clocking
// structure in ATI Crayola, which has lots of cascaded cells that
// are mimiced in test/clock-aliasing/vector_ati3.v.  The particular
// construct handled by this routine is:
//
//   assign      ctlout[1] = clk;
//   assign      ctlout[0] = rst;
//
// So, given an elaborated ctlout[1] this routine will return an
// elaborated clk.  This routine will bail if it finds more than
// one non-initial driver, or a driver that is not an assign
// to a NUBitselLvalue or NUPartselLvalue
//
// Return value is false if there are multiple drivers or there is
// a situation where we cannot robustly determine all the drivers
// *newExpr and *newFlow will be modified if successful.
//
// If the elaboration fails, the return value may be true, but
// *newExpr will be NULL, so when you call this routine, you must
// check that the return value is true AND that *newExpr != NULL
// to be sure that the method succeeded.
bool SCHClkAnal::findElabBitSource(NUNetElab* netElab,
                                   UInt32 offset,
                                   FLNodeElab* flow,
                                   FLNodeElab** newFlow,
                                   const NUExpr** newExpr)
{
  bool driven = false;

  // If it's a temp-net, rely on the fanin from the passed in flow
  for (Fanin f = mUtil->loopFanin(flow, eFlowIntoCycles); !f.atEnd(); ++f)
  {
    FLNodeElab* fanin = *f;
    if (fanin == flow) {
      return false;             // probably a latch
    }
    if (getElabBitSource(fanin, netElab, offset, newFlow, newExpr))
    {
      if (*newExpr == NULL)
        return false;         // elaboration failed
      if (driven)
        return false;         // multi-driver
      driven = true;
    }
    else if (netElab == fanin->getDefNet())
    {
      // if this bit is being driven, then we have a multiple driver
      // situation where one of the drivers is not an assign.  It
      // might be an NUEnabledDriver, which gets created in
      // test/clock-aliasing/pull1.v even thought there is no explicit
      // pull.  I'm not entirely sure why it gets created, but it
      // definitely complicates bit expression synthesis, so give up.
      NUVectorNet* vn = dynamic_cast<NUVectorNet*>(netElab->getNet());
      NU_ASSERT(vn, netElab);
      SInt32 index = vn->getRange()->index(offset);
      NUNetRefFactory* factory = mUtil->getNetRefFactory();
      NUNetRefHdl ref = factory->createVectorNetRef(vn, index);
      NUNetRefHdl defNetRef = fanin->getDefNetRef(factory);
      if (ref->overlaps(*defNetRef))
        return false;
    }
  } // for
  return driven;
} // bool SCHClkAnal::findElabBitSource

// Recursively search driver and it's fanin for a driver for
// netElab[bit].  If we find one, return true with the flow 
// for that driver in *newFlow, and the corresponding elaborated
// expression in *newExpr
// if a unique driver cannot be found then return false
//
// If the elaboration fails, the return value may be true, but
// *newExpr will be NULL, so when you call this routine, you must
// check that the return value is true AND that *newExpr != NULL
// to be sure that the method succeeded.
bool SCHClkAnal::getElabBitSource(FLNodeElab* driver,
                                  NUNetElab* netElab,
                                  UInt32 offset,
                                  FLNodeElab** newFlow,
                                  const NUExpr** newExpr)
{
  UtDEBUG(&sClkVecDebug, "getElabBitSource", driver->getDefNet()->getSymNode(),
          &netElab->getNet()->getLoc(), "CLK_VEC_SPEW")

  if ((driver->getDefNet() != netElab) || SCHUtil::isSequential(driver)) {
    return false;    // don't follow through a flop
  }

  while (driver->hasNestedBlock() && (driver->numNestedFlows() == 1)) {
    FLNodeElabLoop loop = driver->loopNested();
    driver = *loop;
  }

  NUUseDefNode* node = driver->getUseDefNode();

  if (dynamic_cast<NUBlock*>(node) != NULL) {
    // blocks have assignments as their fanin
    for (Fanin f(NULL, driver, eFlowOverCycles); !f.atEnd(); ++f) {
      if (!elaborateAssign(*f, netElab, offset, newFlow, newExpr)) {
        return false;
      }
    }
  } else {
    // not a block, or NULL useDefNode(boundary)
    if (!elaborateAssign(driver, netElab, offset, newFlow, newExpr)) {
      return false;
    }
  }
  return true;
} // bool SCHClkAnal::getElabBitSource

// Handle dynamic bitselects by transforming vec[sel] into
//
//     vec[0]&(sel==0) | vec[1]&(sel==1) | vec[2]&(sel==2) ...
//
// recursing through vec[k].
const NUExpr* SCHClkAnal::dynamicBitSelect(FLNodeElab* flow,
                                           NUNetElab* net,
                                           const NUExpr* sel,
                                           FLNodeElab** collectedFlow)
{
  const NUExpr* ret = NULL;
  NUExpr* bitExpr = NULL;
  UInt32 selectWidth = sel->determineBitSize();
  *collectedFlow = NULL;
  if (selectWidth <= 31)        // Let's not handle arbitrary sized bit-selects
                                // right now -- might blow up...
  {
    UInt32 maxSelect = 1 << selectWidth;
    UInt32 netSize = net->getNet()->getBitSize();
    if (netSize < maxSelect)
      maxSelect = netSize;
    if (maxSelect < 1024)
    {
      const SourceLocator& loc = sel->getLoc();

      bool flowMatches = true;
      for (UInt32 i = 0; i < maxSelect; ++i)
      {
        const NUExpr* vec_k_ret = NULL;
        NUExpr* vec_k = NULL;
        FLNodeElab* bitFlow = NULL;
        if (findElabBitSource(net, i, flow, &bitFlow, &vec_k_ret))
        {
          if (flowMatches)
          {
            if (*collectedFlow == NULL)
              *collectedFlow = bitFlow;
            else if (*collectedFlow != bitFlow)
            {
              flowMatches = false;
              *collectedFlow = flow;
            }
          }
          if (vec_k_ret != NULL)
            vec_k = vec_k_ret->copy(*mCopyContext);
          else
          {
            // getElabBitSource did not yield an error, but it didn't
            // yield a scalar net, either -- probably a primary input.
            // So make the vector net be a canonical clock input.
            NUNetElab* clk = findClock(net->getNet(), net->getHier(), NULL,
                                       false, flow != NULL);
            if (clk != NULL)
            {
              NUExpr* clkExpr = new NUIdentRvalueElab(clk, loc);
              vec_k = new NUVarselRvalue(clkExpr,
                                         ConstantRange (i,i),
                                         loc);
            }
          }
        }
        if (vec_k == NULL)
        {
          if (bitExpr != NULL)
            delete bitExpr;
          return false;
        }
        if (*collectedFlow == NULL)
          *collectedFlow = flow;

        // Generate vec[k] & (sel==k)
        NUExpr* k = NUConst::create(false, i, 32, loc);
        k->resize(32);

        NUExpr* sel_eq_k = new NUBinaryOp(NUOp::eBiEq,
                                          sel->copy(*mCopyContext),
                                          k, loc);
        NUExpr* term = new NUBinaryOp(NUOp::eBiBitAnd,
                                      vec_k, sel_eq_k, loc);
        if (bitExpr == NULL)
          bitExpr = term;
        else
          bitExpr = new NUBinaryOp(NUOp::eBiBitOr, bitExpr, term,
                                   term->getLoc());
      } // for
    } // if
  } // if
  if (bitExpr != NULL)
  {
    bitExpr->resize(1);
    ret = mExprFactory->insert(mFold->fold(bitExpr));
  }
  return ret;
} // const NUExpr* SCHClkAnal::dynamicBitSelect

// Class to assist replaceLeaves in elaborating and folding bitselect
// operations nested in an expression.  This is called from in two
// phases.
//
// In the first phase (mPhase==0), we are trying to handle
// "clks[0]" where vector clks gets assigned one bit at a time, i.e.
// "clks[0] = clk; clks[1] = reset;".  This is complex as it requires
// following through a very pessimistic data flow graph, where we
// get a single flow for "clks" that has fanin for both the bit
// assignments, and we have to choose the right one.
//
// In the second phase, (mPhase==1), we are trying to handle
// "clks[0]" where vector clks gets assigned via a concatenation.
// This is simpler, as the flow is correct and adequate, and all
// we need to do is a substitution of "clks" with {clk, reset},
// to get {clks,reset}[0], and then fold it.
class HackExpr: public NuToNuFn
{
public:
  HackExpr(CopyContext& copyContext, SCHClkAnal* ca,
             FLNodeElab* flow, SCHClkAnal::ClkAnalPhase phase)
    : mCopyContext(copyContext), mClkAnal(ca), mFlow(flow), mPhase(phase)
  {}

  virtual NUExpr * operator()(NUExpr* expr, Phase phase)
  {
    NUExpr* ret = NULL;
    if (!isPre(phase))
    {
      NUVarselRvalue* partsel;
      if ((partsel = dynamic_cast<NUVarselRvalue*>(expr)) != NULL) {
        if (partsel->getRange ()->getLength () == 1) {
          FLNodeElab* exprFlow = mFlow;
          if (mPhase == SCHClkAnal::eClkOneBitAtATime)
          {
            const NUExpr* e = mClkAnal->elabBitFlow(partsel, &exprFlow);
            if (e != NULL) {
              ret = e->copy(mCopyContext);
            }
          }
          else {
            ret = mClkAnal->elabBitsel(partsel, &exprFlow);
          }
        }
      }
    } // if
    if (ret != NULL) {
      delete expr;
    }
    return ret;
  } // virtual NUExpr * operator

private:
  CopyContext& mCopyContext;
  SCHClkAnal* mClkAnal;
  FLNodeElab* mFlow;
  SCHClkAnal::ClkAnalPhase mPhase;
}; // class HackExpr: public NuToNuFn

// Functor to elaborate nucleus identifer expressions in a tree
class ClkElabIdent: public NuToNuFn
{
public:
  ClkElabIdent(STBranchNode* hier)
    : mHier(hier)
  {}
  virtual NUExpr * operator()(NUExpr* expr, Phase) {
    NUIdentRvalue* id = dynamic_cast<NUIdentRvalue*>(expr);
    NUExpr* ret = NULL;
    if (id != NULL) {
      if (dynamic_cast<NUIdentRvalueElab*>(expr) == NULL) {
        // If it's not already elaborated, elaborate based on the current flow
        NUNet* net = id->getIdent();
        NUNetElab* netElab = net->lookupElab(mHier);
        ret = new NUIdentRvalueElab(netElab, id->getLoc());
        delete expr;
      }
    } // if
    return ret;
  }

private:
  STBranchNode* mHier;
}; // class ClkElabIdent: public NuToNuFn


// replace NUIdentRvalues in an expresion with NUIdentRvalueElabs.
const NUExpr* SCHClkAnal::elaborateIdents(const NUExpr* expr,
                                          STBranchNode* hier)
{
  ClkElabIdent clkElabIdent(hier);
  NUExpr* e = mFold->fold(expr->copy(*mCopyContext));
  NUExpr* temp = clkElabIdent(e, NuToNuFn::ePost);
  if (temp != NULL) {
    expr = mExprFactory->insert(temp);
  }
  else {
    (void) e->replaceLeaves(clkElabIdent);
    expr = mExprFactory->insert(e);
  }
  return expr;
}


// Transform this construct
//     wire [1:0] vec;
//     assign vec[1] = clkin;
//     assign vec[0] = rstin;
//     wire clk = vec[1];
// to this construct
//     wire clk = clkin;
// by following the flow for only the bit that is desired.  Also handle
// dynamic bitselects, transforming
//     wire dclk = vec[sel];
// to
//     wire dclk = ((sel==0) & rstin) | ((sel==1) & clkin);
// note that we don't actually mutate the expression for code generation,
// only for BDD purposes.
const NUExpr* SCHClkAnal::elabBitFlow(const NUVarselRvalue* bitsel,
                                      FLNodeElab** flow)
{
  // Be sure we're looking at something that might be a clock
  ConstantRange r (*bitsel->getRange ());
  if (r.getLength () != 1) {
    return 0;
  }

  const NUExpr* origIdent = bitsel->getIdentExpr();
  const NUExpr* bitIndex = bitsel->getIndex();
  FLNodeElab* bitFlow = NULL;
  const NUExpr* bitExpr = NULL;
  if (origIdent->isWholeIdentifier())
  {
    NUNetElab* net = origIdent->getNetElab((*flow)->getHier());
    if (net != NULL)
    {
      if ( not mMarkDesign->isState(net) )
      {
        SInt32 kindex;

        BitselType bt = analyzeBitsel(net->getNet(), bitIndex, &r, &kindex);
        switch (bt) {
        case eBitselDynamic:
          bitExpr = dynamicBitSelect(*flow, net, bitIndex, &bitFlow);
          break;
        case eBitselConstIndex: {
          const NUExpr* elabBit = NULL;
          if (findElabBitSource(net, kindex, *flow, &bitFlow, &elabBit) &&
              (bitFlow != NULL) &&
              (elabBit != NULL))
          {
            FLNodeElab *last_flow = NULL;

            bitExpr = mExprResynth->buildContinuousExpr(elabBit, bitFlow, NULL, &last_flow);
            // since buildContinuousExpr can expand through intermediate temporaries,
            // bitFlow from findElabBitSource may now be be obsolete,
            // instead we use last_flow to get the proper flow (this works because we are
            // working on a clock, which is a single bit)
            if ( last_flow != NULL ){
              bitFlow = last_flow; // there was a substitution
            }
          }
          break;
        }
        case eBitselOutOfRange: {
          // out of range so we just return zero
          NUExpr* k = NUConst::create(false, 0, 1, bitsel->getLoc());
          bitExpr = mExprFactory->insert(k);
          bitFlow = *flow;
          return bitExpr;
        }
        case eBitselNet: {
          NUExpr* var = new NUIdentRvalue(net->getNet(), bitsel->getLoc());
          var->resize(1);
          bitExpr = mExprFactory->insert(var);
          bitFlow = *flow;
          return bitExpr;
        }
        } // switch
      } // if
    } // if
  } // if

  if (bitExpr != NULL) {
    *flow = bitFlow;
  }
  return bitExpr;
} // const NUExpr* SCHClkAnal::elabBitFlow

bool SCHClkAnal::isLowCost(const NUExpr* expr) {
  NUCost cost;
  mCostContext->calcExpr(expr, &cost);
  return cost.mOps < 200;
}

bool SCHClkAnal::walkBitExpr(FLNodeElab* flow, const NUExpr** expr,
                             ClkAnalPhase phase)
{
  HackExpr hackExpr(*mCopyContext, this, flow, phase);
  NUExpr* temp = (*expr)->copy(*mCopyContext);
  bool cont = false;
  NUExpr* e = hackExpr(temp, NuToNuFn::ePost);
  if (e != NULL) {
    const NUExpr* folded = mExprFactory->insert(mFold->fold(e));
    cont = (*expr != folded);
    *expr = folded;
  }
  else {
    if (temp->replaceLeaves(hackExpr) && isLowCost(temp)) {
      const NUExpr* newExpr = mExprFactory->insert(mFold->fold(temp));
      if (*newExpr != **expr) {
        *expr = newExpr;
        cont = true;
      }
    }
    else {
      delete temp;
    }
  }
  
  return cont;
} // bool SCHClkAnal::walkBitExpr

const NUExpr* SCHClkAnal::elaborateExpr(FLNodeElab* flow, const NUExpr* expr,
                                        FLNodeElab** exprFlow,
                                        bool goThruState)
{
  expr = elaborateIdents(expr, flow->getHier());

  // See if we've seen this flow/expr combination before.  Note that if
  // we look at just the expr and not the flow, we may have elaborated
  // the same expression earlier when looking for a different bit.  The
  // right expression would come out of the map, but the wrong flow, and
  // the net-refs would not match and the test for overlapsBit in
  // elaborateAssign would fail.  Example in test/clock-aliasing/fire1.v,
  // bug4884.
  ExprFlow exprFlowLookup(expr, flow);
  ExprFlowMap::iterator p = mExprFlowMap->find(exprFlowLookup);
  if (p != mExprFlowMap->end()) {
    ExprFlow& ef = p->second;
    expr = ef.first;
    *exprFlow = ef.second;
    return expr;
  }

  {
    // check for a cycle in clock expression expansion

    if ( flow->isSCHClkExprCycle() ){
      return NULL;
    }
    flow->putIsSCHClkExprCycle(true);
  }

  *exprFlow = flow;

  NUNet* clk = flow->getFLNode()->getDefNet();
  bool isState = clk->isFlop() || clk->isLatch();
  const NUVarselRvalue* bsel = NULL;

  if (!isState || goThruState) {
    // If the desired expression is a bit-select, and the bit is individually
    // assigned as a BitselRvalue, then it may be easier to follow the bit 
    // alone, rather than trying to follow all the vector bits.
    const NUExpr* bitExpr = NULL;
    if ((bsel = dynamic_cast<const NUVarselRvalue*>(expr)) != NULL) {
      bitExpr = elabBitFlow(bsel, exprFlow);
      if ((bitExpr != NULL) && (bitExpr != expr) && isLowCost(bitExpr)) {
        expr = bitExpr;
      }
    } else {
      while (walkBitExpr(flow, &expr, eClkOneBitAtATime))
        ;                       // empty
    }

    // If expression-synthesis finds a new expression, and it's different from
    // the old expression, use it and loop again.  Note that the use of
    // NUExprFactory allows us to do pointer-comparisons on expressions
    const NUExpr* e = mExprResynth->buildContinuousExpr(expr, flow, NULL, NULL);
    if ((e != NULL) && (e != expr) && isLowCost(e)) {
      expr = e;
    }

    // expresion synthesis does not follow through vector bit-selects
    // well, so pull those apart and recurse.  Needed for vector_ati2.v
    const NUVarselRvalue* varsel = dynamic_cast<const NUVarselRvalue*>(expr);
    if (varsel && (expr->determineBitSize() == 1)) {
      NUExpr* varExpr = elabBitsel(varsel, exprFlow);
      if (varExpr != NULL) {
        if (isLowCost(varExpr)) {
          expr = mExprFactory->insert(mFold->fold(varExpr));
        }
        else {
          delete varExpr; // avoid leak test/cust/sicortex/ice9/carbon/cwtb/m5kf
        }
      }
    }

    while (walkBitExpr(flow, &expr, eClkConcatAssign))
      ;                         // empty
  } // if

  expr = elaborateIdents(expr, flow->getHier());

  flow->putIsSCHClkExprCycle(false);

  // memo-ize the result so we don't have to do this again
  (*mExprFlowMap)[exprFlowLookup] = ExprFlow(expr, *exprFlow);

  return expr;
} // const NUExpr* SCHClkAnal::elaborateExpr
  
// Transform this construct
//     wire [4:0] vec;
//     assign vec = {a, b, c, d, e};
//     wire clk = vec[0];
// to this construct
//     wire clk = {a, b, c, d, e}[0];
// so that it can be folded to this:
//     wire clk = e;
//
// sel must be an elaborated expression, as it assumes any
// identifier in there points to an elaborated NUNetElab*
// (via NUIdentRvalueElab).
NUExpr* SCHClkAnal::elabBitsel(const NUExpr* sel,
                               FLNodeElab** exprFlow)
{
  const NUExpr* origIdent = NULL;
  NUExpr* bitExpr = NULL;

  const NUVarselRvalue *partsel = dynamic_cast<const NUVarselRvalue*>(sel);
  NU_ASSERT(partsel, sel);
  origIdent = partsel->getIdentExpr ();

  if (origIdent->isWholeIdentifier())
  {
    // expression synthesis returns continuously driven nets,
    // so it is safe to do a driver loop (I hope!).  To recursively
    // call expression synthesis we need to start with the flow
    // that drives the identifier of the bit-select.  In the case
    // of a concat we will get one driver for all the bits.  We
    // will construct a concatenated expression and then select
    // the bit that we want. 

    // origIdent is NUIdentRvalueElab, so it doesn't need its scope
    // argument.
    NUNetElab* net = origIdent->getNetElab(NULL);
    if (net != NULL)
    {
      FLNodeElab* flow = NULL;

      // Only consider driving flow that can impact the selected bits
      const NUExpr* index = partsel->getIndex();
      NUNet* idNet = origIdent->getWholeIdentifier();
      const ConstantRange* range = partsel->getRange();
      SInt32 bit = 0;
      BitselType bt = analyzeBitsel(idNet, index, range, &bit);
      NUNetRefFactory* factory = mUtil->getNetRefFactory();

      for (NUNetElab::DriverLoop p = net->loopContinuousDrivers(); !p.atEnd(); ++p)
      {
        FLNodeElab* driver = *p;
        if (bt == eBitselConstIndex) {
          // For simple bit-selects, weed out flow-nodes that drive the
          // wrong bits
          NUNetRefHdl ref = driver->getDefNetRef(factory);
          if (!ref->overlapsBit(bit)) {
            continue;
          }
        }

        if ( driver == *exprFlow) {
          continue;     // probably a latch, test/clkalaising/bug2573.v
        }

        NUUseDefNode* node = driver->getUseDefNode();
        if ((node == NULL) || !node->isInitial())
        {
          if (flow == NULL)
            flow = driver;
          else
          {
            // multiple drivers -- bail out
            flow = NULL;
            break;
          }
        }
      }
      if (flow != NULL)
      {
        SInt32 bit = 0;
        FLNodeElab* identFlow;
        const NUExpr* ident = elaborateExpr(flow, origIdent, &identFlow,
                                            false);

        // g++ has a problem with (*ident != *origIdent), despite the
        // what seems to Josh to be a correct definition of
        // NUExpr::operator!=.  This works around that problem.
        const NUExpr* origIdentCmp = origIdent;
        const NUExpr* index = NULL;

        if ((ident != NULL) && (*ident != *origIdentCmp))
        {
          index = partsel->getIndex ();
          NUNet* idNet = origIdent->getWholeIdentifier();
          BitselType bt = analyzeBitsel(idNet, index, partsel->getRange (), &bit);
          switch (bt) {
          case eBitselDynamic:
            break;
          case eBitselConstIndex:
            index = NULL;     // causes kbitSelect to use 'bit'
            break;
          case eBitselOutOfRange: {
            // out of range so we just return zero
            NUExpr* k = NUConst::create( false, 0, 1, partsel->getLoc());
            return k;
          }
          case eBitselNet: {
            NUExpr* var = new NUIdentRvalue(idNet, partsel->getLoc());
            var->resize(1);
            return var;
          } // case eBitselNet
          } // switch

          bitExpr = kbitSelect(ident, index, bit, sel->getBitSize());
          *exprFlow = flow;
        } // if
        else {
          // If elaborating just the ident didn't get us anywhere
          // try elaborating the part-select as a whole, to get through
          // top.lower_1 in test/clock-aliasing/concat_rewrite2.v,
          // which has no single flow that covers all 3 bits of the
          // net.  But in fact we only want bit 1, so we get it here:
          const NUExpr* e = elaborateExpr(flow, sel, &identFlow, false);
          if ( ( e != NULL ) and ( *e != *sel ) ) {
            bitExpr = e->copy(*mCopyContext);
            bitExpr->resize(sel->getBitSize());
          }
        }
      } // if
    } // if
  } // if
  return bitExpr;
} // NUExpr* SCHClkAnal::elabBitsel
