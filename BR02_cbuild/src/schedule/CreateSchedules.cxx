// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "flow/FLNodeElab.h"
#include "flow/FLNodeElabCycle.h"

#include "util/ArgProc.h"
#include "util/CbuildMsgContext.h"
#include "util/Stats.h"

#include "nucleus/NUDesign.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUModuleInstance.h"

#include "iodb/ScheduleFactory.h"

#include "schedule/Schedule.h"

#include "Util.h"
#include "MarkDesign.h"
#include "ScheduleData.h"
#include "SequentialSchedules.h"
#include "CombinationalSchedules.h"
#include "DerivedClockSchedules.h"
#include "Dump.h"
#include "TimingAnalysis.h"
#include "CreateSchedules.h"

/*!
  \file

  This is the part of the scheduler that creates the actual
  schedules. At this point we know when all logic can transition or is
  sampled. So we have to determine the order requirements and try to
  create an efficient schedule.

  The main routine called by the scheduler is
  SCHCreateSchedules::createDesignSchedule().

  This file uses functions in SequentialSchedules.cxx,
  CombinationalSchedules.cxx, and DerivedClockSchedules.cxx to do the
  scheduling.
*/

SCHCreateSchedules::SCHCreateSchedules(SCHSchedule* sched,
                                       SCHScheduleFactory* f,
                                       SCHMarkDesign* mark,
                                       SCHTimingAnalysis* timingAnalysis,
                                       SCHScheduleData* scheduleData,
                                       SCHUtil* util, SCHDump* dump)
{
  mSequentialSchedules =
    new SCHSequentialSchedules(util, mark, scheduleData, dump,
                               timingAnalysis);
  mCombinationalSchedules =
    new SCHCombinationalSchedules(sched, this, util, dump, scheduleData, f,
                                  mark);
  mDerivedClockSchedules =
    new SCHDerivedClockSchedules(this, f, mark, scheduleData,
				 timingAnalysis, mCombinationalSchedules,
				 mSequentialSchedules, util, sched);
  mSched = sched;
  mUtil = util;
  mDump = dump;
  mMarkDesign = mark;
  mScheduleData = scheduleData;
  mScheduleFactory = f;
  mTimingAnalysis = timingAnalysis;
}

SCHCreateSchedules::~SCHCreateSchedules()
{
  delete mSequentialSchedules;
  delete mCombinationalSchedules;
  delete mDerivedClockSchedules;
}

bool SCHCreateSchedules::createDesignSchedule(const char* fileRoot)
{
  // If we are timing passes, start a new timer
  Stats* stats = mUtil->getStats();
  UtStatsInterval statsInterval(stats != NULL, stats);
    
  // Create the derived clock logic schedules. This must be first
  // (before combinational and sequential schedules below). since it
  // is the highest priority set of schedules.
  bool status;
  status = mDerivedClockSchedules->create(fileRoot);
  statsInterval.printIntervalStatistics("DCL Creat");
  if (!status) {
    return false;
  }

  // Mark all nodes that must be accurate so that we schedule them
  // correctly. This consists of logic driven by clocks, and any logic
  // that feeds a visibility point.
  mCombinationalSchedules->markDesignAccurateNodes();
  statsInterval.printIntervalStatistics("Comb Accurate");

  // Create the sequential schedules
  status = mSequentialSchedules->create(fileRoot);
  statsInterval.printIntervalStatistics("Seq Create");
  if (!status) {
    return false;
  }

  // Create the combinational schedules
  mCombinationalSchedules->create();
  statsInterval.printIntervalStatistics("Comb Creat");

  // Now we can optimize the combinational schedules since we are done
  // finding the input nets the schedules are sensitive to.
  bool ok = mCombinationalSchedules->optimize(fileRoot);
  statsInterval.printIntervalStatistics("Comb Opt");

  // Finish the sequential schedules now that combinational
  // optimizations are done.
  mSequentialSchedules->finish(mScheduleData->getSequentials());
  statsInterval.printIntervalStatistics("Seq Finish");

  // Mark all PIs that feed either derived clock logic schedules, data
  // schedules (sequential or sample based combinational blocks) or
  // both.
  markPIs();
  statsInterval.printIntervalStatistics("Mark PI");

  // Find all elaborated nets that need to be cleared at the end of
  // the schedule. We add that information to the general schedule so
  // that the code generator can emit the clear statements.
  findClearAtEndNets();
  statsInterval.printIntervalStatistics("ClearNets");

  // Gather the set of elaborated nets that will be used for activity
  // monitoring by OnDemand.
  if (mUtil->getArgs()->getBoolValue("-onDemand")) {
    buildOnDemandNetSet();
    statsInterval.printIntervalStatistics("ODNets");
  }

  // Find all the asynchronous schedule clocks
  findAsyncClocks();
  statsInterval.printIntervalStatistics("AsyncClks");

  // Print information about any trace nets to its fanout flops
  mMarkDesign->printTraceNets();
  return ok;
}

SCHScheduleType
SCHCreateSchedules::scheduleNodes(FLNodeElab* flow, SCHScheduleType schedType,
                                  const SCHBlockFlowNodes& blockFlowNodes,
				  SCHDerivedClockLogic* dcl, bool startClk)
{
  NUUseDefNode* driver;

  // Check if this is a valid flow block
  driver = flow->getUseDefNode();
  if (driver == NULL)
    // Not a valid block
    return eCombTransition;

  // Make sure we haven't been schedule before. This should not happen
  // too often but it can because many blocks have lots of loads
  // within a design.
  if (flow->isScheduled())
    return flow->isSampled() ? eCombSample : eCombTransition;

  // For derived clock logic we stop if we have hit a non-derived
  // clock logic node. The first call in for derived clock logic, this
  // node is not marked derived clock logic but just derived clock.
  //
  // But since we can't tell the difference between the first call or
  // if we traversed and hit another derived clock from this one, our
  // caller tells us if this is the start of scheduling for a derived
  // clock by passing in a value for startClk.
  bool isDerivedClockLogic = (dcl != NULL);
  if (isDerivedClockLogic && !flow->isDerivedClockLogic() && !startClk)
    return flow->isSampled() ? eCombSample : eCombTransition;

  // For derived clock sequential blocks, schedule all the flows in
  // this block because we have already tried to split them. If that
  // failed, then we want to treat all flows as part of this derived
  // clock schedule. Same goes for cycles. We mark them all scheduled
  // so that we don't encounter cycles in this walk. So we mark them
  // all done here and schedule them all at the bottom of the routine.
  FLNodeElabVector nodes;
  bool isSequential = SCHUtil::isSequential(driver);
  if (isSequential) {
    blockFlowNodes.getFlows(flow, &nodes, &FLNodeElab::isLive);

  } else if (SCHMarkDesign::isCycle(flow)) {
    mUtil->getAcyclicFlows(flow, &nodes);

  } else {
    nodes.push_back(flow);
  }

  // Mark nodes schedule now. This avoids cycles in this code. They
  // will be handled by DCL scheduling later.
  for (FLNodeElabVectorLoop l(nodes); !l.atEnd(); ++l) {
    FLNodeElab* curFlow = *l;
    curFlow->putIsScheduled(true);
  }

  // Figure out our scheduling preference
  SCHScheduleType thisSchedType;
  if ((schedType == eCombTransition) || flow->mustBeTransition())
  {
    // Our caller or we prefer transition so use that
    thisSchedType = eCombTransition;

    // Count the number of forced combinational blocks
    if (!isSequential) {
      if (flow->mustBeTransition())
	mDump->incrMustBeTransitionNodes();
      else
	mDump->incrTransitionNodes();
    }
  }
  else
  {
    const SCHSignature* flowSig = mMarkDesign->getSignature(flow);
    switch (mScheduleFactory->compareSignatureMasks(flowSig))
    {
    case -1:
      thisSchedType = eCombTransition;

      // Count the number of forced combinational blocks
      if (!isSequential)
	mDump->incrTransitionNodes();
      break;

    case 0:
      FLN_ELAB_ASSERT(schedType == eCombSample, flow);
    case 1:
      thisSchedType = eCombSample;
      FLN_ELAB_ASSERT(!isSequential, flow);
      mDump->incrSampleNodes();
      break;

    default:
      thisSchedType = schedType;
      FLN_ELAB_ASSERT("Should not get anyting but -1, 0, or 1 from compare"==NULL,
                      flow);
    }
  }

  // If the user asked for it, print the nodes we chose to run with
  // transition due to a simpler transition mask
  if (mUtil->isFlagSet(eDumpTransitionNodes) &&
      !isSequential && !flow->mustBeTransition() &&
      (thisSchedType == eCombTransition))
  {
    MsgContext* msgContext = mUtil->getMsgContext();
    if (isDerivedClockLogic)
      msgContext->SCHTransitionNode(flow, "is derived clock logic");
    else
      msgContext->SCHTransitionNode(flow, "has a simpler transition mask");
  }

  // If this is a sequential node we stop here so don't visit the
  // fanin. For combinational schedules we never schedule
  // sequentials. For derived clock schedules we stop at sequentials
  if (!isSequential)
  {
    // Visit the flow's fanin. This is at least fanin list for this
    // flow. For tri-states we also have to make sure we schedule lower
    // strength drivers first. We use a
    SCHMarkDesign::FlowDependencyLoop p;
    for (p = mMarkDesign->loopDependencies(flow, SCHMarkDesign::eInternal);
         !p.atEnd(); ++p)
    {
      FLNodeElab* fanin = *p;
      if (fanin != flow)
      {
	// For combinational logic we stop at sequential blocks for
	// derived clock logic we stop after sequential blocks.  Also
	// for derived clock logic we stop at clocks.
	//
	// We check for a special case when driver is NULL. This is an
	// indication that there is a constant that needs to be
	// scheduled.
	if (!SCHUtil::isSequential(fanin) ||
	    (isDerivedClockLogic && !fanin->isClock()))
	{
	  SCHScheduleType newType;
	  newType = scheduleNodes(fanin, thisSchedType, blockFlowNodes, dcl);
	  if (newType == eCombSample)
	    thisSchedType = eCombSample;
	}
      }
    }
  } // if

  // Now we can schedule this node. In this pass we only schedule
  // combinational nodes unless we are scheduling derived clock logic
  if (!isSequential || flow->isDerivedClockLogic() || flow->isDerivedClock())
  {
    // Schedule all the nodes, for cycles and flops we schedule all
    // the flow to avoid cycles.
    for (FLNodeElabVectorIter i = nodes.begin(); i != nodes.end(); ++i)
    {
      FLNodeElab* curFlow = *i;
      scheduleNode(curFlow, thisSchedType, dcl);
      curFlow->putIsScheduled(true);
    }

    // Mark this flow sample if we are sampling it
    if (thisSchedType == eCombSample)
    {
      // This will not happen for derived clock logic so we don't need
      // to worry about all the flow nodes in a block).
      FLN_ELAB_ASSERT(nodes.size() == 1, flow);
      flow->putIsSampled(true);
    }
  }

  return flow->isSampled() ? eCombSample : eCombTransition;
} // void SCHCreateSchedules::scheduleNodes

// Places a node in one or more schedules. Sequential blocks only go
// in one schedule but combinational blocks may be in many schedules.
void SCHCreateSchedules::scheduleNode(FLNodeElab* flow,
				      SCHScheduleType schedType,
				      SCHDerivedClockLogic* dcl)
{
  NUUseDefNode* driver = flow->getUseDefNode();
  bool isSeq = SCHUtil::isSequential(driver);

  // Hierarchical boundary.  Do not schedule it.
  if (driver == NULL)
  {}
  
  // Block used to compute a derived clock
  else if ((dcl != NULL) && !driver->isInitial())
    mDerivedClockSchedules->scheduleDerivedNode(flow, isSeq, dcl);

  // Sequential block
  else if (isSeq)
    mSequentialSchedules->schedule(flow, mScheduleData->getSequentials(),
                                   NULL);

  else
  {
    // Combinational block.  Filter out module instances.  Such
    // nodes were being created prior to Aron's fix for bug1879,
    // "extra elab bound node with undriven output & deposite".
    // So let's assert they don't show up in the schedule.  It
    // seems as of 2/26/04 they are still showing up, so we have
    // to filter, we cannot assert.
    // asserting.
    bool isModInstance =
      (dynamic_cast<NUModuleInstance*>(flow->getUseDefNode()) != NULL);
    //FLN_ELAB_ASSERT(!isModInstance, flow);
    if (!isModInstance)
      mCombinationalSchedules->scheduleCombinationalNode(flow, schedType);
  }
} // void SCHCreateSchedules::scheduleNode


void SCHCreateSchedules::markPIs()
{
  // Need a class to get all the flow elabs for an always blocks
  SCHBlockFlowNodes blockFlowNodes(mUtil);

  // Visit all the derived clock logic combinational schedules
  SCHSchedule::DerivedClockLogicLoop p;
  for (p = mScheduleData->loopDerivedClockLogic(); !p.atEnd(); ++p) {
    SCHDerivedClockBase* dclBase = *p;
    SCHCombinationals combs;
    dclBase->getCombinationalSchedules(&combs);
    for (SCHCombinationalsLoop l(combs); !l.atEnd(); ++l) {
      SCHCombinational* comb = *l;
      markPIsSub(comb->getSchedule(), true, blockFlowNodes);
    }
  } // for

  // Mark the PIs that feed data logic. This is the sequential
  // schedules ...
  SCHSchedulesLoop l;
  for (l = mScheduleData->loopAllSequentials(); !l.atEnd(); ++l) {
    SCHSequential* seq = l.getSequentialSchedule();
    markPIsSub(seq->getSchedule(eClockPosedge), false, blockFlowNodes);
    markPIsSub(seq->getSchedule(eClockNegedge), false, blockFlowNodes);
  }

  // ... and the combinational sample schedules.
  SCHSchedule::CombinationalLoop pc;
  for (pc = mScheduleData->loopCombinational(eCombSample);
       !pc.atEnd(); ++pc)
  {
    SCHCombinational* comb = *pc;
    markPIsSub(comb->getSchedule(), false, blockFlowNodes);
  }
}

void
SCHCreateSchedules::markPIsSub(SCHIterator i, bool markClk, 
                               const SCHBlockFlowNodes& blockFlowNodes)
{
  FLNodeElabSet covered;
  IODBNucleus* iodb = mMarkDesign->getUtil()->getIODB();
  for (; !i.atEnd(); ++i) {
    // Get all the flow for this block
    FLNodeElab* flow = *i;
    FLNodeElabVector flowElabs;
    blockFlowNodes.getFlows(flow, &flowElabs, &FLNodeElab::isLive);

    // Find all the PIs feeding this block
    for (FLNodeElabVectorCLoop l(flowElabs); !l.atEnd(); ++l) {
      FLNodeElab* curFlow = *l;
      if (covered.insertWithCheck(curFlow)) {
        for (SCHMarkDesign::LevelFaninLoop n = mMarkDesign->loopLevelFanin(curFlow);
             !n.atEnd(); ++n) {
          // Mark any PIs
          FLNodeElab* fanin = *n;
          if (mMarkDesign->isPrimaryInput(fanin) || mMarkDesign->isDepositable(fanin)) {
            NUNetVector netVector;
            NUNetElab* defNet = fanin->getDefNet();
            if (markClk)
              iodb->declarePrimaryClock(defNet->getSymNode());
            defNet->getAliasNets(&netVector, true);
            NUNetVector::iterator pos;
            for (pos = netVector.begin(); pos != netVector.end(); ++pos) {
              NUNet* net = *pos;
              if (markClk) {
                net->putInClkPath(true);
              } else {
                net->putInDataPath(true);
              }
            }
          }
        }
      } // if
    } // for
  } // for
} // void SCHCreateSchedules::markPIsSub

void SCHCreateSchedules::findClearAtEndNets()
{
  // Walk the symbol table looking for all elaborated nets
  STSymbolTable* symTab = mUtil->getSymbolTable();
  STSymbolTable::NodeLoop p;
  for (p = symTab->getNodeLoop(); !p.atEnd(); ++p) {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf != NULL) {
      // Check if it is live and a clear at end net
      NUNetElab* netElab = NUNetElab::find(leaf);
      if ((netElab != NULL) && !netElab->getNet()->isDead() &&
          netElab->getNet()->isClearAtEnd()) {
        // We have an elaborated net. Get the schedule mask for all
        // the live drivers.
        const SCHScheduleMask* mask;
        mask = mTimingAnalysis->getNetTransitionMask(netElab);
        if (mask != NULL) {
          mScheduleData->addClearAtEndNet(mask, netElab);
        }
      }
    }
  }
} // SCHCreateSchedules::findClearAtEndNets

void SCHCreateSchedules::findAndDumpStateUpdates()
{
  // Find all the state updates
  mScheduleData->findSUNets();

  if (mUtil->isFlagSet(eDumpStateUpdate)) {
    MsgContext* msgContext = mUtil->getMsgContext();

    // Print the state updates in the clock logic
    SCHSchedule::DerivedClockLogicLoop p;
    for (p = mScheduleData->loopDerivedClockLogic(); !p.atEnd(); ++p) {
      SCHDerivedClockBase* dclBase = *p;
      for (SCHSequentials::SULoop l = dclBase->loopSUNets(); !l.atEnd(); ++l)
      {
        NUNetElab* net = *l;
        msgContext->SCHStateUpdateClkLogic(net);
      }
    }

    // Print the state updates in the normal logic
    SCHSequentials* sequentials = mScheduleData->getSequentials();
    for (SCHSequentials::SULoop l = sequentials->loopSUNets(); !l.atEnd(); ++l)
    {
      NUNetElab* net = *l;
      msgContext->SCHStateUpdate(net);
    }
  } // if
}

void SCHCreateSchedules::buildOnDemandNetSet()
{
  FLNodeElabSet covered_nodes;
  NUConstNetSet *ondemand_nets = mUtil->getDesign()->getOnDemandNets();
  // Build unelab->elab maps for explicitly excluded and programatically included nets
  SCHUtil::NetToNetElabMap excludedNets, includedNets;

  for (SCHMarkDesign::DesignIter i(mMarkDesign); !i.atEnd(); ++i) {
    FLNodeElab *fl_elab = *i;
    mUtil->findOnDemandNets(fl_elab, &includedNets, &excludedNets, &covered_nodes, true);
  }

  // If the scheduler created a buffered net for a user-excluded net,
  // remove it as well.
  NUNetElabMap *buffered_nets = mUtil->getDesign()->getBufferedNetMap();
  for (NUNetElabMap::SortedLoop l = buffered_nets->loopSorted(); !l.atEnd(); ++l) {
    NUNetElab *buff_net_elab = (*l).first;
    NUNetElab *orig_net_elab = (*l).second;
    NUNet *buff_net = buff_net_elab->getStorageNet();
    NUNet *orig_net = orig_net_elab->getStorageNet();
    // See if the unelaborated net is in the excluded map
    SCHUtil::NetToNetElabMap::iterator iter = excludedNets.find(orig_net);
    if (iter != excludedNets.end()) {
      // See if this particular elaborated instance was excluded
      NUNetElabSet *netSet = iter->second;
      if (netSet->count(orig_net_elab)) {
        // It was.  Excluded the buffered net as well.
        SCHUtil::addOnDemandNet(&excludedNets, buff_net, buff_net_elab);
      }
    }
  }

  // Now, loop over the excluded list, checking for intersection with
  // the included list.  If this exists, we have a minor problem.
  //
  // Normally, OnDemand state inclusion/exclusion is done on an
  // unelaborated basis by codegen when generating code for each
  // module.  If a net is included in the OnDemand state, it has
  // special OnDemand save/restore code generated for it.  If it's not
  // part of the state (whether due to user exclusion or otherwise),
  // no code is generated.
  //
  // If some instances of a net are included and others are excluded,
  // this no longer works.  Instead, codegen will treat the net as if
  // it is part of OnDemand state.  We will then record in the DB the
  // elaborated instances that are to be excluded, and mask their
  // values from the OnDemand state buffer at runtime.
  MsgContext* msgContext = mUtil->getMsgContext();
  for (SCHUtil::NetToNetElabMap::SortedLoop l = excludedNets.loopSorted(); !l.atEnd(); ++l) {
    NUNet *net = (*l).first;
    SCHUtil::NetToNetElabMap::iterator mapIter = includedNets.find(net);
    // If the excluded net isn't in the included set, do nothing.
    // Otherwise, we have more work to do.
    if (mapIter != includedNets.end()) {
      // Get the sets of excluded/included elaborated instances of this net
      NUNetElabSet *excludedNetElabs = (*l).second;
      NUNetElabSet *includedNetElabs = mapIter->second;
      // Remove each excluded net from the included set, then see what's left.
      includedNetElabs->eraseSet(*excludedNetElabs);
      if (includedNetElabs->empty()) {
        // Good news!  All elaborated instances of this net were
        // excluded.  Remove the entry from the map.
        SCHUtil::removeOnDemandNet(&includedNets, net);
      } else {
        // We have a problem.  Not every instance of the net was
        // excluded, so we'll have to do runtime exclusion.  Issue a
        // warning because this will impact performance.
        UtString name;
        net->compose(&name);
        msgContext->SchOnDemandExcludedCheck(net, name.c_str());
        // Save all the nodes in the IODB so we can exclude them at
        // runtime.
        IODBNucleus *iodb = mUtil->getIODB();
        for (NUNetElabSet::SortedLoop ll = excludedNetElabs->loopSorted(); !ll.atEnd(); ++ll) {
          NUNetElab *netElab = *ll;
          STAliasedLeafNode *leaf = netElab->getStorageSymNode();
          iodb->putOnDemandRuntimeExcluded(leaf);
        }
      }
    }
  }
  
  // Codegen only needs the unelaborated nets, so create that simplified set now.
  for (SCHUtil::NetToNetElabMap::SortedLoop l = includedNets.loopSorted(); !l.atEnd(); ++l) {
    const NUNet *net = (*l).first;
    ondemand_nets->insert(net);
  }

  // Free the memory allocated for the maps
  for (SCHUtil::NetToNetElabMap::SortedLoop l = excludedNets.loopSorted(); !l.atEnd(); ++l) {
    NUNet *net = (*l).first;
    SCHUtil::removeOnDemandNet(&excludedNets, net);
  }
  for (SCHUtil::NetToNetElabMap::SortedLoop l = includedNets.loopSorted(); !l.atEnd(); ++l) {
    NUNet *net = (*l).first;
    SCHUtil::removeOnDemandNet(&includedNets, net);
  }
}

void SCHCreateSchedules::findAsyncClocks()
{
  // Helper class to do an efficient lookup of all elaborated flow
  // given any elaborated flow of an always block instance.
  SCHBlockFlowNodes blockFlowNodes(mUtil);
  FLNodeElabSet covered;

  // Walk the list of async schedules and look for any clocks in all
  // defs for the always block instances.
  mScheduleData->initAsyncClocks();
  SCHSchedule::CombinationalLoop l;
  for (l = mScheduleData->loopCombinational(eCombAsync); !l.atEnd(); ++l) {
    // Walk the set of flow to find always block instances
    SCHCombinational* async = *l;
    for (SCHIterator i = async->getSchedule(); !i.atEnd(); ++i) {
      FLNodeElab* asyncFlowElab = *i;
      if (covered.insertWithCheck(asyncFlowElab)) {
        // Get all the elaborated flow for this always block instance
        FLNodeElabVector flowElabs;
        blockFlowNodes.getFlows(asyncFlowElab, &flowElabs, &FLNodeElab::isLive);

        // Walk the set of flows and add any async clocks
        for (FLNodeElabVectorCLoop f(flowElabs); !f.atEnd(); ++f) {
          // Add it if it defs a clock with active logic (not bound
          // nodes for primary clocks).
          FLNodeElab* flowElab = *f;
          const NUNetElab* netElab = flowElab->getDefNet();
          if (mUtil->isActiveDriver(flowElab) &&
              mMarkDesign->isValidClock(netElab)) {
            mScheduleData->addAsyncClock(netElab);
          }

          // Remember that we have seen this node to avoid N*N code
          covered.insert(flowElab);
        }
      }
    } // for
  } // for
} // void SCHCreateSchedules::findAsyncClocks
       
