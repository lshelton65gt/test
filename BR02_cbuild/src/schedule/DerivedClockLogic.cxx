// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/UtIOStream.h"

#include "nucleus/Nucleus.h"

#include "schedule/Schedule.h"

#include "flow/FLNodeElab.h"

#include "Util.h"

/*!
  \file
  Implementation for the DerivedClockLogic class
*/

SCHDerivedClockBase::SCHDerivedClockBase(int id) :
  SCHScheduleBase(eDCL), mID(id), mDepth(-1)
{
  mClocks = new SCHClocks;
}

SCHDerivedClockBase::~SCHDerivedClockBase()
{
  delete mClocks;
}

SCHClocksLoop SCHDerivedClockBase::loopClocks() const
{
  return SCHClocksLoop(*mClocks);
}

const NUNetElab* SCHDerivedClockBase::getRepresentativeClock() const
{
  // It is a sorted set, so we can just return the first one
  SCHED_ASSERT(!mClocks->empty(), this);
  return *(mClocks->begin());
}

bool SCHDerivedClockBase::generatesClock(const NUNetElab* clkElab) const
{
  return mClocks->find(clkElab) != mClocks->end();
}

int
SCHDerivedClockBase::compareSchedules(const SCHDerivedClockBase* dcl) const
{
  // Check for easy case
  if (this == dcl)
    return 0;

  // Sort by the clock nets. First, lets check the clock count.
  int size1 = mClocks->size();
  int size2 = dcl->mClocks->size();
  int cmp = size1 - size2;
  if (cmp != 0) {
    return cmp;
  }

  // The sets are sorted so we can just iterate and compare until we
  // get a difference.
  SCHClocksLoop l1 = loopClocks();
  SCHClocksLoop l2 = dcl->loopClocks();
  for (; !l1.atEnd() && !l2.atEnd() && (cmp == 0); ++l1, ++l2) {
    const NUNetElab* clk1 = *l1;
    const NUNetElab* clk2 = *l2;
    cmp = NUNetElab::compare(clk1, clk2);
  }
  
  // We cannot have two DCL schedules that are identical
  NU_ASSERT(cmp != 0, *(loopClocks()));
  return cmp;
}

int
SCHDerivedClockBase::compareSchedules(const SCHScheduleBase* schedBase) const
{
  // The other schedule must be a DCL
  const SCHDerivedClockBase* other;
  other = dynamic_cast<const SCHDerivedClockBase*>(schedBase);
  SCHED_ASSERT(other != NULL, schedBase);

  // use the compare routine
  return compareSchedules(other);
}

void
SCHDerivedClockBase::mergeSchedule(SCHDerivedClockBase* otherSched)
{
  // Make sure we are not merging cycles
  SCHED_ASSERT(castDCLCycle() == NULL, this);
  SCHED_ASSERT(otherSched->castDCLCycle() == NULL, otherSched);

  // Bring the others clocks here
  for (SCHClocksLoop l = otherSched->loopClocks(); !l.atEnd(); ++l) {
    const NUNetElab* clkElab = *l;
    mClocks->insert(clkElab);
  }
  otherSched->mClocks->clear();
}

//! constructor
SCHDerivedClockLogic::SCHDerivedClockLogic(NUCNetElabSet& clocks,
					   const SCHScheduleMask* mask,
					   SCHUtil* util, int id) :
  SCHDerivedClockBase(id), mBusy(false), mCycleNode(false)
{
  mClocks->insert(clocks.begin(), clocks.end());
  mCombinational = new SCHCombinational(eCombDCL, mask, this);
  mSequentials = new SCHSequentials(eSequentialDCL, util);
}

//! destructor
SCHDerivedClockLogic::~SCHDerivedClockLogic()
{
  delete mCombinational;
  delete mSequentials;
}

SCHIterator SCHDerivedClockLogic::getSchedule()
{
  return mCombinational->getSchedule();
}

SCHInputNetsCLoop SCHDerivedClockLogic::loopDCLNets() const
{
  return mCombinational->loopInputNets();
}

const SCHInputNets* SCHDerivedClockLogic::getDCLNets() const
{
  return mCombinational->getAsyncInputNets();
}

SCHSequentials::SULoop
SCHDerivedClockLogic::loopSUNets() const
{
  return mSequentials->loopSUNets();
}

void
SCHDerivedClockLogic::findSUNets(const SCHBlockFlowNodes& blockFlowNodes)
{
  mSequentials->findSUNets(blockFlowNodes);
}

void SCHDerivedClockLogic::setMask(const SCHScheduleMask* mask)
{
  SCHED_ASSERT(mask != NULL, this);
  mCombinational->putMask(mask);
}

const SCHScheduleMask* SCHDerivedClockLogic::getMask() const
{
  return mCombinational->getMask();
}

SCHSequentials::SequentialLoop SCHDerivedClockLogic::loopSequential() const
{
  return mSequentials->loopSequential();
}

void SCHDerivedClockLogic::addCombinationalNode(FLNodeElab* node)
{
  mCombinational->addCombinationalNode(node);
}

void SCHDerivedClockLogic::print(bool printNets, SCHMarkDesign* mark) const
{
  UtIO::cout() << "Derived Clock(" << this << ") clks = \n";
  for (SCHClocksLoop l(*mClocks); !l.atEnd(); ++l) {
    const NUNetElab* clk = *l;
    UtString buf;
    clk->getSymNode()->compose(&buf);
    UtIO::cout() << " => " << buf << "\n";
  }
  if (printNets) {
    for (SCHSequentials::SequentialLoop l=loopSequential(); !l.atEnd(); ++l) {
      SCHSequential* seq = *l;
      seq->print(true, mark);
    }
    mCombinational->print(true, mark);
  }
}

void SCHDerivedClockLogic::dump(int indent, SCHMarkDesign* mark) const
{
  // Print the header for this simple DCL schedule
  dumpIndent(indent);
  UtIO::cout() << "Derived Clock Logic for ";
  const char* sep = "";
  for (SCHClocksLoop l = loopClocks(); !l.atEnd(); ++l) {
    const NUNetElab* clk = *l;
    UtString buf;
    clk->compose(&buf, NULL);
    UtIO::cout() << sep << buf;
    sep = ", ";
  }
  UtIO::cout() << " (depth=" << getDepth() << "):" << UtIO::endl;

  // Print any branch nets
  UtString branchNetsBuf;
  composeBranchNets(&branchNetsBuf, mark);
  if (!branchNetsBuf.empty()) {
    UtIO::cout() << "  " << branchNetsBuf << "\n";
  }

  // Print the sequential sub schedules
  SCHSequentials::SequentialLoop ps;
  for (ps = loopSequential(); !ps.atEnd(); ++ps)
  {
    SCHSequential* sc = *ps;
    sc->dump(indent+2, mark);
  }

  // Print the combinational schedule
  mCombinational->dump(indent+2, mark);
}

SCHDerivedClockLogic* SCHDerivedClockLogic::castDCL()
{
  return this;
}

SCHDerivedClockCycle* SCHDerivedClockLogic::castDCLCycle()
{
  return NULL;
}

const SCHDerivedClockLogic* SCHDerivedClockLogic::castDCL() const
{
  return this;
}

const SCHDerivedClockCycle* SCHDerivedClockLogic::castDCLCycle() const
{
  return NULL;
}

SCHDerivedClockBase::SequentialLoop
SCHDerivedClockLogic::loopMultiSequential() const
{
  SequentialLoop loop;
  loop.pushLoop(loopSequential());
  return loop;
}

void 
SCHDerivedClockLogic::getCombinationalSchedules(SCHCombinationals* combs) const
{
  combs->push_back(mCombinational);
}

void
SCHDerivedClockLogic::getSequentialSchedules(SCHSequentialsVector* seqs) const
{
  seqs->push_back(getSequentialSchedules());
}

SCHDerivedClockCycle::SCHDerivedClockCycle(int id) : SCHDerivedClockBase(id)
{
  mSubSchedules = new SubSchedules;
  mSUNets = new SCHSequentials::SUNets;
  mDCLInputNets = NULL;
  mBreakClocks = new SCHClocks;
  mBreakNets = new BreakNets;
}

SCHDerivedClockCycle::~SCHDerivedClockCycle()
{
  for (SubSchedulesLoop l = loopSubSchedules(); !l.atEnd(); ++l) {
    SCHDerivedClockLogic* dcl = *l;
    delete dcl;
  }
  delete mSubSchedules;
  delete mSUNets;
  delete mBreakClocks;
  delete mBreakNets;
}

void 
SCHDerivedClockCycle::addSubSchedule(SCHDerivedClockLogic* dcl)
{
  // Add the schedule
  mSubSchedules->push_back(dcl);

  // Add all the clocks it generates to the cycle. This gets iterated
  // frequently so we keep it up to date.
  for (SCHClocksLoop l = dcl->loopClocks(); !l.atEnd(); ++l) {
    const NUNetElab* clkElab = *l;
    mClocks->insert(clkElab);
  }

  // Mark that the sub schedule is part of a cycle
  SCHED_ASSERT(!dcl->isCycleNode(), dcl);
  dcl->putCycleNode(true);
}

static bool
lessDCLDepth(SCHDerivedClockLogic* dcl1, SCHDerivedClockLogic* dcl2)
{
  int depth1 = dcl1->getDepth();
  int depth2 = dcl2->getDepth();
  SCHED_ASSERT(depth1 >= 0, dcl1);
  SCHED_ASSERT(depth2 >= 0, dcl2);
  return depth1 < depth2;
}

SCHDerivedClockCycle::SubSchedulesLoop SCHDerivedClockCycle::loopSubSchedules()
  const
{
  return SubSchedulesLoop(*mSubSchedules);
}

SCHDerivedClockCycle::SubSchedulesLoop
SCHDerivedClockCycle::loopSortedSubSchedules() const
{
  // Make sure we have sorted it first
  std::sort(mSubSchedules->begin(), mSubSchedules->end(), lessDCLDepth);
  return loopSubSchedules();
}

SCHSequentials::SULoop SCHDerivedClockCycle::loopSUNets() const
{
  return SCHSequentials::SULoop(*mSUNets);
}

void SCHDerivedClockCycle::findSUNets(const SCHBlockFlowNodes& blockFlowNodes)
{
  // Create the set of SU nets
  NUNetElabSet covered;
  for (SubSchedulesLoop l = loopSubSchedules(); !l.atEnd(); ++l) {
    SCHDerivedClockLogic* dcl = *l;
    dcl->findSUNets(blockFlowNodes);
    for (SCHSequentials::SULoop s = dcl->loopSUNets(); !s.atEnd(); ++s) {
      NUNetElab* netElab = *s;
      if (covered.find(netElab) == covered.end()) {
        mSUNets->push_back(netElab);
        covered.insert(netElab);
      }
    }
  }
}

SCHInputNetsCLoop SCHDerivedClockCycle::loopDCLNets() const
{
  // Compute the DCL input

  if (mDCLInputNets == NULL) {
    return SCHInputNetsCLoop();
  } else {
    return SCHInputNetsCLoop(*mDCLInputNets);
  }
}

const SCHInputNets* SCHDerivedClockCycle::getDCLNets() const
{
  return mDCLInputNets;
}


SCHDerivedClockLogic* SCHDerivedClockCycle::castDCL()
{
  return NULL;
}

SCHDerivedClockCycle* SCHDerivedClockCycle::castDCLCycle()
{
  return this;
}

const SCHDerivedClockLogic* SCHDerivedClockCycle::castDCL() const
{
  return NULL;
}

const SCHDerivedClockCycle* SCHDerivedClockCycle::castDCLCycle() const
{
  return this;
}

SCHDerivedClockBase::SequentialLoop
SCHDerivedClockCycle::loopMultiSequential() const
{
  SequentialLoop loop;
  for (SubSchedulesLoop l = loopSubSchedules(); !l.atEnd(); ++l) {
    SCHDerivedClockLogic* dcl = *l;
    loop.pushLoop(dcl->loopSequential());
  }
  return loop;
}

void 
SCHDerivedClockCycle::getCombinationalSchedules(SCHCombinationals* combs) const
{
  for (SubSchedulesLoop l = loopSubSchedules(); !l.atEnd(); ++l) {
    SCHDerivedClockLogic* dcl = *l;
    combs->push_back(dcl->getCombinationalSchedule());
  }
}

void
SCHDerivedClockCycle::getSequentialSchedules(SCHSequentialsVector* seqs) const
{
  for (SubSchedulesLoop l = loopSubSchedules(); !l.atEnd(); ++l) {
    SCHDerivedClockLogic* dcl = *l;
    seqs->push_back(dcl->getSequentialSchedules());
  }
}

void SCHDerivedClockCycle::dump(int indent, SCHMarkDesign* mark) const
{
  // Print the header for this DCL cycle schedule
  dumpIndent(indent);
  UtIO::cout() << "Derived Clock Cycle for ";
  const char* sep = "";
  for (SCHClocksLoop l = loopClocks(); !l.atEnd(); ++l) {
    const NUNetElab* clk = *l;
    UtString buf;
    clk->compose(&buf, NULL);
    UtIO::cout() << sep << buf;
    sep = ", ";
  }
  UtIO::cout() << " (depth=" << getDepth() << "):" << UtIO::endl;

  // Print any branch nets
  UtString branchNetsBuf;
  composeBranchNets(&branchNetsBuf, mark);
  if (!branchNetsBuf.empty()) {
    UtIO::cout() << "  " << branchNetsBuf << "\n";
  }

  // Print the sequential sub schedules
  for (SubSchedulesLoop l = loopSortedSubSchedules(); !l.atEnd(); ++l) {
    SCHDerivedClockLogic* dcl = *l;
    dcl->dump(indent+2, mark);
  }
}

void SCHDerivedClockCycle::print(bool printNets, SCHMarkDesign* mark) const
{
  UtIO::cout() << "Derived Clock Cycle(" << this << ") clks = \n";
  for (SCHClocksLoop l(*mClocks); !l.atEnd(); ++l) {
    const NUNetElab* clk = *l;
    UtString buf;
    clk->getSymNode()->compose(&buf);
    UtIO::cout() << " => " << buf << "\n";
  }
  for (SubSchedulesLoop l = loopSortedSubSchedules(); !l.atEnd(); ++l) {
    SCHDerivedClockLogic* dcl = *l;
    dcl->print(printNets, mark);
  }
}

void SCHDerivedClockCycle::setMask(const SCHScheduleMask* mask)
{
  mMask = mask;
}

const SCHScheduleMask* SCHDerivedClockCycle::getMask() const
{
  return mMask;
}

void SCHDerivedClockCycle::putDCLInputNets(const SCHInputNets* inputNets)
{
  mDCLInputNets = inputNets;
}

bool SCHDerivedClockCycle::isBreakClock(const NUNetElab* clkElab) const
{
  return mBreakClocks->find(clkElab) != mBreakClocks->end();
}

SCHDerivedClockCycle::BreakNetsLoop SCHDerivedClockCycle::loopBreakNets() const
{
  return BreakNetsLoop(*mBreakNets);
}

void SCHDerivedClockCycle::addBreakClock(const NUNetElab* clkElab)
{
  mBreakClocks->insert(clkElab);
}

void SCHDerivedClockCycle::addBreakNet(const NUNetElab* netElab)
{
  mBreakNets->insert(netElab);
}

void SCHDerivedClockCycle::removeDeadBreakNets(SCHUtil* util)
{
  // Gather the live break nets
  BreakNets liveBreakNets;
  bool deadBreakNetFound = false;
  for (BreakNetsLoop l = loopBreakNets(); !l.atEnd(); ++l) {
    // Check if this netElab has live drivers
    const NUNetElab* netElab = *l;
    bool live = false;
    for (SCHUtil::ConstDriverLoop d = util->loopNetDrivers(netElab);
         !d.atEnd() && !live; ++d) {
      FLNodeElab* flowElab = *d;
      live = flowElab->isLive();
    }

    // If it doesn't have live drivers don't add it to the temporary set
    if (live) {
      liveBreakNets.insert(netElab);
    } else {
      deadBreakNetFound = true;
    }
  }

  // If we found dead break nets then remove them
  if (deadBreakNetFound) {
    mBreakNets->swap(liveBreakNets);
  }
} // void SCHDerivedClockCycle::removeDeadBreakNets
