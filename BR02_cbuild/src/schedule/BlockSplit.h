// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  A class to analyze and potentially break up blocks into smaller
  blocks when possible.
*/

#ifndef _BLOCKSPLIT_H_
#define _BLOCKSPLIT_H_

#include <cstddef>
#include "UseDefHierPair.h"
#include "BlockFlowNodes.h"

#ifndef _UT_ORDER_H_
#include "util/UtOrder.h"       // For carbonPtrCompare()
#endif

//! SCHBlockSplit class
/*! This class contains a set of data and functions to manage breaking
 *  up blocks. There are two reasons for needing to break up a block:
 *  because the nodes in the block have different timing or because
 *  the nodes in the block are part of a false combinational cycle.
 *
 *  This class provides routines to:
 *
 *  1. Find the blocks where nodes are in multiple schedules:
 *     findMultiTimingBlocks.
 *
 *  2. Get the list of multi timing blocks.
 *
 *  3. Add more blocks for splitting found externally (false cycles)
 *
 *  4. Attempt to split the blocks
 *
 *  5. Get a list of the blocks that were not split.
 */
class SCHBlockSplit
{
private:
  // Used to sort schedules
  struct CmpSchedule
  {
    bool operator()(const SCHScheduleBase* sb1, const SCHScheduleBase* sb2)
    {
      // First compare by type
      ptrdiff_t cmp = sb1->getScheduleType() - sb2->getScheduleType();
      if (cmp == 0)
	// Next compare by pointer
	cmp = carbonPtrCompare(sb1, sb2);
      return cmp < 0;
    }
  };

public:
  //! constructor
  SCHBlockSplit(SCHUtil* util, SCHMarkDesign* mark,
                SCHScheduleData* scheduleData);

  //! destructor
  ~SCHBlockSplit();

  //! Abstraction for a set of schedules
  typedef UtSet<SCHScheduleBase*, CmpSchedule> Schedules;

  //! Abstraction for a block instance that needs to be split
  typedef UtVector<FLNodeElabVector> BlockInstanceGroups;

  //! Abstraction for the set of blocks in more than one schedule
  typedef UtVector<BlockInstanceGroups*> MultiSchedBlocks;

  //! Iterator over the set of blocks that are in more than one schedule
  typedef CLoop<MultiSchedBlocks> MultiSchedBlocksLoop;

  //! Abstraction for a list of blocks that need to be split
  typedef UtVector<BlockInstanceGroups*> UnSplitBlocks;

  //! Iterator over the set of blocks that need to be split
  typedef CLoop<UnSplitBlocks> UnSplitBlocksLoop;

  //! Function to find blocks in multiple schedules
  /*! Finds all the blocks in multiple schedules and marks them for
   *  splitting. It finds blocks that have nodes in multiple schedules
   *  or it finds nodes that are in multiple schedules.
   *
   *  Use the iterator to loop over the set of blocks this routine
   *  found. If a block should be split then add it using
   *  markSplitBlock.
   */
  void findMultiTimingBlocks(SCHSchedulesLoop l);

  //! Get an iterator over the multi-timing blocks
  /*! These are the blocks found by findMultiTimingBlocks
   */
  MultiSchedBlocksLoop loopMultiTimingBlocks() const
  {
    INFO_ASSERT(mMultiSchedInfoValid,
                "Iterator over multi-timing blocks called before analysis was done");
    return MultiSchedBlocksLoop(*mMultiSchedBlocks);
  }

  //! Returns true if the block instance groups is a multi schedule block
  bool isMultiSchedBlock(BlockInstanceGroups* blockGroups)
  {
    INFO_ASSERT(mMultiSchedInfoValid,
                "Test for multi-timing blocks called before analysis was done");
    return (*mIsMultiSchedBlock)[blockGroups];
  }

  //! Get the schedule set and multi-timing flags for a flow node
  const Schedules* getFlowSchedules(FLNodeElab* flow);

  //! Add a block instance to the set of split blocks
  /*! This block instance is added to the set of blocks we want to
   *  split. Once we have the entire set has been provided, then the
   *  blocks can be split with splitBlocks.
   */
  void markSplitBlock(BlockInstanceGroups* blockGroups);

  //! Add this node to the set of split blocks.
  /*! This node should be split from the rest of the nodes in the
   *  block. Once all of the nodes to split have been added, call the
   *  routine convertNodesToBlocks() to complete the blocks to be
   *  split.
   */
  void markSplitNode(FLNodeElab* flow);

  //! Create the set of split blocks from a set of split nodes
  void convertNodesToBlocks();

  //! Get an iterator over the un-split block instances
  /*! These are the blocks marked with markSplitBlocks.
   */
  UnSplitBlocksLoop loopUnSplitBlocks()
  {
    // Make sure they are sorted before we loop
    if (!mSplitBlockInstancesSorted)
    {
      sortUnSplitBlocks(mSplitBlockInstances);
      mSplitBlockInstancesSorted = true;
    }

    // Now we can loop through them
    return UnSplitBlocksLoop(*mSplitBlockInstances);
  }

  //! Calls the nucleus function to split blocks
  /*! When we decide to split a block, we do so by the net they
   *  define. This routine:
   * 
   * - Analyzes and simplifies the data such that there is a split
   *   set that applies to all instances of the block.
   *
   * - Takes the resulting sets of sets of nets and calls the nucleus
   *   to break up the blocks.
   *
   * - If the block is broken up, then the block is
   *   deleted from the class. Otherwise, it is left behind. This way,
   *   the caller can find the set of blocks that were not succesfully
   *   split using loopSplitBlocks().
   *
   *   The caller also gets a set of flow nodes that were created as a
   *   result of this process. It is up to the caller to deal with
   *   them appropriately (schedule them, etc.)
   *
   *  \param newFlows A place to store any newly created flow due to
   *  the split.
   *
   *  \param allowTaskInlining Set to false to make splitting reject
   *  splits that would require task inlining.
   *
   *  \returns true if succesful, false if an error occured. The
   *  compile should abort immediately on an error.
   */
  bool splitBlocks(FLNodeElabSet* newFlows, bool allowTaskInlining = true);

  static inline int
  compareScheduleSets(const Schedules* cs1, const Schedules* cs2)
  {
    // Check based on size first
    int size1 = cs1->size();
    int size2 = cs2->size();
    if (size1 != size2)
      return size1 - size2;

    // Check based on the schedules
    Schedules::const_iterator p1 = cs1->begin();
    Schedules::const_iterator p2 = cs2->begin();
    int cmp = 0;
    while ((p1 != cs1->end()) && (cmp == 0))
    {
      SCHScheduleBase* sb1 = *p1++;
      SCHScheduleBase* sb2 = *p2++;
      cmp = sb1->compareSchedules(sb2);
    }
    return cmp;
  }

  // Used to sort schedule sets
  struct CmpSchedules
  {
    bool operator()(const Schedules* cs1, const Schedules* cs2)
      const
    {
      int cmp = compareScheduleSets(cs1, cs2);
      return cmp < 0;
    }
  };

  typedef UtMap<const FLNodeElab*, const Schedules*> FlowToSchedMap;

private:
  // Function to get the block flow nodes from the lookup cache
  void getBlockFlowNodes(FLNodeElab* flowElab, FLNodeElabVector* nodes);

  // Functions to add a schedule to our maps
  void addSchedToMap(SCHScheduleBase*, SCHSchedulesLoop::Iterator);
  bool addNodeToMap(FLNodeElab* flow, SCHScheduleBase* schedBase);

  // Functions to create a new unique schedule set
  const Schedules* buildSchedules(SCHScheduleBase* schedBase);
  const Schedules* buildSchedules(SCHScheduleBase* schedBase,
				  const Schedules* schedules);
  const Schedules* getSchedules(const Schedules* schedules);

  // Functions to create split blocks from split nodes
  void createSplitBlock(FLNodeElabVector* nodes);
  void validateBlock(FLNodeElabVector* nodes, FLNodeElabVector* remainingNodes);
  bool findFalseBlockCycle(FLNodeElab*, FLNodeElabSet*, bool);

  // Functions to create the info the nucleus needs and call the API.
  void releaseMultiSchedInfo();
  void createSplitBlockDefs();
  void simplifyFlowNodeSets();
  bool splitNucleusBlocks(FLNodeElabSet* newFlows, bool allowTaskInlining);
  bool callNucleus(FLNodeElabSet * splitFlows, FLNodeElabSet * deletedFlows, bool allowTaskInlining);
  void cleanupDeletedFlows(FLNodeElabSet& deletedFlows);
  void handleSplitFlows(FLNodeElabSet& splitFlows, FLNodeElabSet* newFlows);

  // Functions to find all elaborated nodes for the blocks we want to split
  void findAllDesignElaboratedNodes();
  void findAllElaboratedNodes(FLNodeElab* flow, UInt32 pass, bool start);

  // Functions to sort a vector/list of BlockInstanceGroups so that
  // any output using them is canonical.
  void sortMultiSchedBlocks(MultiSchedBlocks* blocks);
  void sortUnSplitBlocks(UnSplitBlocks* blocks);

  // A class to keep the flow between groups of split nodes. This is
  // used for merging some flow nodes back together after we've
  // decided to atomize portions of a block

  // Used to keep track of the schedules for each flow node
  typedef UtSet<const Schedules*, CmpSchedules> ScheduleFactory;
  ScheduleFactory* mScheduleFactory;
  FlowToSchedMap* mFlowToSchedMap;

  // Used to sort multi schedule nodes by schedule set
  friend struct CmpScheduleNodes;

  struct CmpScheduleNodes
  {
    CmpScheduleNodes(FlowToSchedMap* flowToSchedMap)
    {
      mFlowToSchedMap = flowToSchedMap;
    }

    bool operator()(const FLNodeElab* f1, const FLNodeElab* f2) const
    {
      // Sort by the schedules they are in first
      FlowToSchedMap::const_iterator p1 = mFlowToSchedMap->find(f1);
      FlowToSchedMap::const_iterator p2 = mFlowToSchedMap->find(f2);
      FLN_ELAB_ASSERT((p1 != mFlowToSchedMap->end()), f1);
      FLN_ELAB_ASSERT((p2 != mFlowToSchedMap->end()), f2);
      int cmp = compareScheduleSets(p1->second, p2->second);

      // Then sort by the flow node themselves
      if (cmp == 0)
	cmp = FLNodeElab::compare(f1, f2);
      return cmp < 0;
    }

    FlowToSchedMap* mFlowToSchedMap;
  };

  // Used to keep track of whether the block is in multiple schedules
  typedef UtMap<BlockInstanceGroups*, bool> IsMultiSchedBlock;
  IsMultiSchedBlock* mIsMultiSchedBlock;

  // Data to keep track of all block instances when figuring out if
  // blocks are in more than one schedule.
  typedef UtMap<SCHUseDefHierPair, FLNodeElabVector> UseDefHierFlows;
  UseDefHierFlows* mUseDefHierFlows;

  // Data to keep track of the multi schedule blocks
  MultiSchedBlocks* mMultiSchedBlocks;
  bool mMultiSchedInfoValid;

  // Data to keep track of the set of blocks we want to split
  bool mSplitBlockInstancesSorted;
  UnSplitBlocks* mSplitBlockInstances;

  // Type to describe a single split and set of splits of a block
  // definition. This is used to create one split from the various
  // instances.
  typedef UtVector<const FLNode*> SplitEntry;
  typedef UtVector<SplitEntry> BlockDefSplits;
  typedef UtMap<const HierName*, BlockDefSplits> SplitData;

  // Data to hold all the split information for all blocks
  typedef UtMap<const NUUseDefNode*, SplitData> SplitBlockDefs;
  SplitBlockDefs* mSplitBlockDefs;

  // Data to hold all the FLNodeElabs for the set of blocks we are
  // going to split.
  FLNodeConstElabMap* mSplitFlowNodeElabs;

  // Data to hold nodes that are going to be split
  FLNodeElabSet* mSplitNodes;

  //! Holds a map from always block instances to all their elaborated flow
  SCHBlockFlowNodes* mBlockFlowNodes;

  // Helper classes
  SCHUtil* mUtil;
  SCHMarkDesign* mMarkDesign;
  SCHScheduleData* mScheduleData;
}; // class SCHBlockSplit

#endif // _BLOCKSPLIT_H_
