// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  Implementation of a class to analyze the combinational schedules and
  merge blocks when possible
*/

#include "util/UtIOStream.h"
#include "util/Stats.h"
#include "util/GraphBuilder.h"
#include "util/GraphTranspose.h"

#include "schedule/Schedule.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Signature.h"

#include "localflow/ClockAndDriver.h"

#include "reduce/RETransform.h"

#include "Dump.h"
#include "MarkDesign.h"
#include "ScheduleData.h"
#include "BlockMerge.h"

class NUUseDefNode;

SCHBlockMerge::SCHBlockMerge(SCHUtil* util, SCHMarkDesign* mark, 
                             SCHScheduleData* data, bool reverseEdge) :
  mUtil(util), mMarkDesign(mark), mScheduleData(data),
  mMergedBlocks(0), mMergeCount(0), mDumpMergedBlocks(0),
  mReverseEdge(reverseEdge), mCrossesHierarchyValid(false)
{
  mUniqueScheduleSetFactory = new UniqueScheduleSetFactory;
  mUniqueHierSetFactory = new UniqueHierSetFactory;
  mDesignBlockMap = new BlockMap;
  mMergableBlocks = new Blocks;
  mUnMergableBlocks = new Blocks;
  mBlockToNodesMap = new SCHUtil::BlockToNodesMap;
  mFlowToBlocksMap = new FlowToBlocksMap;
  mMergeEdges = new MergeEdges;
}

SCHBlockMerge::~SCHBlockMerge()
{
  delete mUniqueScheduleSetFactory;
  delete mUniqueHierSetFactory;
  delete mDesignBlockMap;
  delete mMergableBlocks;
  delete mUnMergableBlocks;
  delete mBlockToNodesMap;
  delete mFlowToBlocksMap;
  delete mMergeEdges;
}

void SCHBlockMerge::start()
{
  // Start a new stats counter
  Stats* stats = mUtil->getStats();
  if (stats != NULL)
    stats->pushIntervalTimer();
}

void
SCHBlockMerge::createBlock(FLNodeElab* flowElab, FLNodeElabVector* nodes,
                           SCHScheduleBase* schedBase)
{
  // Check if a block already exists
  MergeBlock* block;
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  BlockMap::iterator pos = mDesignBlockMap->find(useDef);
  if (pos != mDesignBlockMap->end()) {
    // Found it
    block = pos->second;
    block->addGroup(nodes);
  } else {
    // Need to create it
    block = new MergeBlock(nodes);
    mDesignBlockMap->insert(BlockMap::value_type(useDef, block));
  }

  // This must be a SCHSequentialEdge or SCHCombinational object or
  // the MergeBlock:compareHierScheduleSets will not work correctly.
  FLN_ELAB_ASSERT((schedBase->castSequentialEdge() != NULL) ||
                  (schedBase->castCombinational() != NULL), flowElab);

  // Update the schedule set for this group and instance
  const STBranchNode* hier = flowElab->getHier();
  const ScheduleSet* scheduleSet = block->getScheduleSet(hier);
  scheduleSet = createScheduleSet(scheduleSet, schedBase);
  block->putScheduleSet(hier, scheduleSet);
} // SCHBlockMerge::addSchedule

void
SCHBlockMerge::mergeBlocks(int dumpMergedBlocks, int mergeTypesMask)
{
  // Set up the hier sets for all blocks
  initHierSets();

  // Remember if we should dump statistics
  mDumpMergedBlocks = dumpMergedBlocks;

  // Create the block to flow map that we need below
  mUtil->findAllBlockNodes(*mBlockToNodesMap);

  // Gather the number of single instance, single flow use def nodes
  gatherMergableBlocks();
  Stats* stats = mUtil->getStats();
  if (stats != NULL)
    stats->printIntervalStatistics("MB Init");

  // Break any false cycles through mergable blocks. This can occur if
  // the same block in two instances feed each other. We do this early
  // before we create flow and depth below.
  createFlowToBlocksMap();
  breakFalseCycles();
  if (stats != NULL)
    stats->printIntervalStatistics("MB Brk Cycles");

  // Create a block graph the flow graph
  createBlockGraph();
  clearFlowToBlocksMap();
  if (stats != NULL) stats->printIntervalStatistics("MB Graph");

  // Initialize the graph merge package
  if (!init(mMergeGraph)) {
    INFO_ASSERT(0, "Cycle found in block graph");
  }

  // Merge by depth and def net
  BlockOrder blockOrder(mMergeGraph);
  if (SCHMergePresent(mergeTypesMask, eMTNetDepth)) {
    BlockNetBuckets blockNetBuckets(mMergeGraph, this);
    mergeByDepth(&blockNetBuckets, &blockOrder);
    if (stats != NULL) stats->printIntervalStatistics("MB NetDepth");
  }

  // Compute which blocks cross hierarchy. We do this up front to
  // avoid n N*M algorithm
  computeAllCrossHierachyBlocks();
  if (stats != NULL) stats->printIntervalStatistics("MB XHier");

  // Merge by fanin
  if (SCHMergePresent(mergeTypesMask, eMTFanin)) {
    EdgeOrder edgeOrder(mMergeGraph);
    mergeFanin(&blockOrder, &edgeOrder);
    if (stats != NULL) stats->printIntervalStatistics("MB Fanin");
  }

  // Merge by depth
  BlockBuckets blockBuckets(mMergeGraph, this);
  if (SCHMergePresent(mergeTypesMask, eMTDepth)) {
    mergeByDepth(&blockBuckets, &blockOrder);
    if (stats != NULL) stats->printIntervalStatistics("MB Depth");
  }

  // Merge by reverse depth, transpose the graph
  if (SCHMergePresent(mergeTypesMask, eMTReverseDepth)) {
    transposeGraph();
    mergeByDepth(&blockBuckets, &blockOrder);
    transposeGraph();
    if (stats != NULL) stats->printIntervalStatistics("MB RDepth");
  }

  // Merge blocks by depth in depth order, this may introduce
  // cycles. It is up to the caller to deal with that.
  if (SCHMergePresent(mergeTypesMask, eMTDepthOrder)) {
    mergeByDepthOrder(&blockBuckets, &blockOrder);
    if (stats != NULL) stats->printIntervalStatistics("MB ADepth");
  }

  // All done with the graph
  delete mMergeGraph;
  mMergeGraph = NULL;

  // Ask the nucleus to clean itself up
  RETransform* transform = mUtil->getTransform();
  transform->cleanUpMergedFlows();
  if (stats != NULL)
    stats->printIntervalStatistics("MB Cleanup");

  // Try and improve the runtime of these flows.
  transform->optimizeMergedFlows(mUtil->getDesign(), mDumpMergedBlocks >= 1);
  if (stats != NULL)
    stats->printIntervalStatistics("MB Optimize");

  bool check_liveness = false;
  if (check_liveness) {
    // Make a fanout snapshot of the design
    mMarkDesign->createFanoutMap();
  }

  // Gather clock aliasing information for the flow repair
  ClockAndDriver clockAndDriver;
  mMarkDesign->gatherClockAndDriver(&clockAndDriver);

  // Update internal flow.
  FLNodeElabSet deletedFlow;
  transform->repairMergedFlow(mUtil->getDesign(), &clockAndDriver, &deletedFlow,
                              stats);
  if (stats != NULL) stats->printIntervalStatistics("MB Repair");
  
  // Call the mark design and update its information
  mScheduleData->removeDeadFlow(deletedFlow);

  if (check_liveness) {
    // Verify that the design is still valid
    bool errorsFound = mMarkDesign->validateLiveFlows();
    INFO_ASSERT(!errorsFound, "Check the preceding message for more information");

    // clear sanity caches
    mMarkDesign->clearFanoutMap();
  }

  // The true fanin cache must be invalidated
  mMarkDesign->clearTrueFaninCaches();

  // Print the final results
  if ((mDumpMergedBlocks != 0) && (mMergedBlocks > 0))
    UtIO::cout() << "Total blocks merged = " << mMergedBlocks << UtIO::endl;

  // Delete the block information since we are done
  for (AllBlocksLoop l = loopAllBlocks(); !l.atEnd(); ++l) {
    MergeBlock* mergeBlock = *l;
    delete mergeBlock;
  }
  for (Loop<MergeEdges> l(*mMergeEdges); !l.atEnd(); ++l) {
    MergeEdge* mergeEdge = *l;
    delete mergeEdge;
  }
  mMergableBlocks->clear();
  mUnMergableBlocks->clear();
  mBlockToNodesMap->clear();
  mUniqueScheduleSetFactory->clear();
  mUniqueHierSetFactory->clear();
  if (stats != NULL) stats->printIntervalStatistics("MB Delete");

  // Make sure everything is sane
  if (mUtil->isFlagSet(eSanity)) {
    mMarkDesign->sanityCheck();
    if (stats != NULL) stats->printIntervalStatistics("MB Sanity");
  }

  if (stats != NULL)
    stats->popIntervalTimer();
} // SCHBlockMerge::mergeBlocks

const SCHBlockMerge::ScheduleSet*
SCHBlockMerge::createScheduleSet(const ScheduleSet* oldSet,
				 SCHScheduleBase* schedBase)
{
  return mUniqueScheduleSetFactory->createUniqueSet(oldSet, schedBase);
} // SCHBlockMerge::createScheduleSet

void
SCHBlockMerge::gatherMergableBlocks()
{
  // Go through the design block groups and figure out which ones are
  // mergeble. Those that are can be moved to the mergeable set as is
  // Go through the blocks and create the flow to block map
  BlockMap::iterator p;
  for (p = mDesignBlockMap->begin(); p != mDesignBlockMap->end(); ++p)
  {
    // Check if this block is mergable. The various reasons for
    // avoiding merging are documented in the routine.
    const NUUseDefNode* useDef = p->first;
    MergeBlock* block = p->second;
    bool mergable = blockMergable(useDef, block);

    // If it is mergable, add it to the merge set. If it is not
    // mergable, break it up and add it to the unmergable set.
    if (mergable) {
      mMergableBlocks->push_back(block);

    } else {
      // Go through the groups and put them in separate blocks
      breakUpBlock(block);

      // This block is no longer needed since it has been broken
      // up. We clear the map below.
      delete block;
    }
  }

  // We are done with the design blocks
  mDesignBlockMap->clear();
} // SCHBlockMerge::gatherMergableBlocks

void SCHBlockMerge::breakFalseCycles()
{
  // Break the cycles by calling the derived classes break routine
  BlockSet covered;
  BlockSet busy;
  for (BlocksLoop l (*mMergableBlocks); !l.atEnd(); ++l) {
    MergeBlock* block = *l;
    breakCycles(block, &covered, &busy);
  }

  // Go through the list again and delete the ones that became
  // unmergable
  Blocks mergable;
  for (BlocksLoop l (*mMergableBlocks); !l.atEnd(); ++l) {
    MergeBlock* block = *l;
    if (!block->isMergable()) {
      delete block;
    } else {
      mergable.push_back(block);
    }
  }
  mMergableBlocks->swap(mergable);
} // void SCHBlockMerge::breakFalseCycles

void 
SCHBlockMerge::breakCycles(MergeBlock* block, BlockSet* covered, BlockSet* busy)
{
  BlockRankMap blockRankMap;
  findAndBreakFalseCycles(block, covered, busy, &blockRankMap, 1);
}

SCHBlockMerge::MergeBlock*
SCHBlockMerge::findAndBreakFalseCycles(MergeBlock* block, BlockSet* covered, 
                                       BlockSet* busy, BlockRankMap* blockRankMap,
                                       UInt32 rank)
{
  // If we have been here before, stop
  if (covered->find(block) != covered->end())
    return NULL;

  // We keep track of "rank" which counts the number of mergable blocks
  // in a path.  If we reach an unmergable block with a rank assigned,
  // we know we have hit a cycle, and if the rank is the same as the
  // rank we received from the caller, then we know the cycle has no
  // mergable block in it (or else the rank would have been incremented).
  // When we are finished processing an unmergable block, we restore its
  // rank to what it had when we entered the function.
  UInt32 prevRank = 0;

  // We only mark busy if this is a mergable block. That way we only
  // find cycles on mergable blocks. The others will just be in the
  // stack. We want to ignore unmergable blocks in cycles, unless
  // they are in a cycle with no mergeable blocks at all.  Then we
  // need to detect that case and not continue to our fanin, to
  // prevent an infinite recursion.
  bool mergable = block->isMergable();
  if (mergable) {
    // If we found a cycle on a mergable block, break it here
    if (busy->find(block) != busy->end())
      return block;
    busy->insert(block);

    // increase the rank to show that there is a mergable block in this path
    rank += 1; 

  } else {
    // Lookup the curent rank for this block
    BlockRankMap::iterator p = blockRankMap->find(block);
    if (p != blockRankMap->end())
      prevRank = p->second;

    // If the block's rank is the same as the rank from the caller, it
    // means we have found a cycle containing only unmergable blocks.
    // Return NULL to stop the recursion and ignore this kind of cycle.
    if (rank == prevRank)
      return NULL;
      
    // Update the rank for this block
    (*blockRankMap)[block] = rank;
  }

  // Visit our fanin. If the fanin returns a non-NULL break block it
  // means we found a cycle and we picked the given block as the spot
  // to break the cycle. This will cause us to return the recursion
  // until we find the spot where it should be broken.
  //
  // If this block is to be broken, then it not only gets broken but
  // we restart the recursion again. This is so that we find any
  // remaining cycles through the newly broken up blocks.
  bool breakHere = false;
  MergeBlock* breakBlock = NULL;
  do {
    BlockSet faninSet;
    getBlockFanin(block, &faninSet);
    breakBlock = NULL;
    breakHere = false;
    for (BlockSetLoop l(faninSet); !l.atEnd() && (breakBlock == NULL); ++l) {
      MergeBlock* fanin = *l;
      breakBlock = findAndBreakFalseCycles(fanin,covered,busy,blockRankMap,rank);

      // Check if we should break up this block
      if (breakBlock == fanin) {
        breakHere = true;
        breakUpBlock(fanin);
      }
    }
  } while(breakHere);

  // No longer busy
  if (mergable) {
    busy->erase(block);
  } else {
    (*blockRankMap)[block] = prevRank; // Restore previous rank
  }

  // If we are going to break a cycle, return it to our caller. We can
  // only mark this block done if we didn't find any cycles through
  // it.
  if (breakBlock == NULL) {
    covered->insert(block);
  }
  return breakBlock;
}

void SCHBlockMerge::breakUpBlock(MergeBlock* block)
{
  clearFlowToBlocksMap(block);
  for (SCHFlowGroupsLoop g = block->loopGroups(); !g.atEnd(); ++g)
  {
    // Create a new block for this set of nodes
    FLNodeElabVector* nodes = *g;
    MergeBlock* newBlock = new MergeBlock(nodes);
    mUnMergableBlocks->push_back(newBlock);
    updateFlowToBlocksMap(newBlock);
    newBlock->putUnmergable();
  }

  // Mark the original block unmergable so we find it and delete it
  block->putUnmergable();
}

void SCHBlockMerge::clearFlowToBlocksMap(MergeBlock* block)
{
  for (SCHFlowGroups::FlowsLoop f = block->loopFlowNodes(); !f.atEnd(); ++f) {
    FLNodeElab* flow = *f;
    FlowToBlocksMap::iterator pos = mFlowToBlocksMap->find(flow);
    if (pos != mFlowToBlocksMap->end()) {
      Blocks* blocks = pos->second;
      blocks->clear();
    }
  }
}

void SCHBlockMerge::updateFlowToBlocksMap(MergeBlock* block)
{
  for (SCHFlowGroups::FlowsLoop f = block->loopFlowNodes(); !f.atEnd(); ++f) {
    FLNodeElab* flow = *f;
    FlowToBlocksMap::iterator pos = mFlowToBlocksMap->find(flow);
    Blocks* blocks;
    if (pos == mFlowToBlocksMap->end()) {
      blocks = new Blocks;
      mFlowToBlocksMap->insert(FlowToBlocksMap::value_type(flow, blocks));
    } else {
      blocks = pos->second;
    }
    blocks->push_back(block);
  }
}

void SCHBlockMerge::createBlockGraph()
{
  // Create the graph and add all the nodes to it
  GraphBuilder<MergeGraph> builder;
  for (AllBlocksLoop l = loopAllBlocks(); !l.atEnd(); ++l) {
    MergeBlock* block = *l;
    builder.addNode(block);
  }

  // Go through the blocks and compute their edges
  for (AllBlocksLoop l = loopAllBlocks(); !l.atEnd(); ++l) {
    // Connect the fanin
    MergeBlock* block = *l;
    BlockSet faninSet;
    getBlockFanin(block, &faninSet);
    for (BlockSetLoop f(faninSet); !f.atEnd(); ++f) {
      MergeBlock* faninBlock = *f;
      if (block != faninBlock) {
        MergeEdge* mergeEdge = new MergeEdge;
        bool added;
        if (mReverseEdge) {
          added = builder.addEdgeIfUnique(faninBlock, block, mergeEdge);
        } else {
          added = builder.addEdgeIfUnique(block, faninBlock, mergeEdge);
        }
        if (added) {
          mMergeEdges->push_back(mergeEdge);
        } else {
          delete mergeEdge;
        }
      }
    }
  } // for

  mMergeGraph = builder.getGraph();
} // SCHBlockMerge::createBlockGraph

void SCHBlockMerge::createFlowToBlocksMap()
{
  // Go through the blocks and create the flow to block map
  for (AllBlocksLoop l = loopAllBlocks(); !l.atEnd(); ++l) {
    MergeBlock* block = *l;
    updateFlowToBlocksMap(block);
  }
}

void SCHBlockMerge::clearFlowToBlocksMap()
{
  for (FlowToBlocksMap::iterator p = mFlowToBlocksMap->begin();
       p != mFlowToBlocksMap->end(); ++p)
  {
    Blocks* blocks = p->second;
    delete blocks;
  }
  mFlowToBlocksMap->clear();
}

static inline void
updateBlockMap(FLNodeElabVectors& nodeVectors,
	       SCHUtil::BlockToNodesMap& blockToNodesMap)
{
  FLNodeElab* flow = *(nodeVectors[0].begin());
  NUUseDefNode* useDef = flow->getUseDefNode();
  FLNodeElabSet& flowSet = blockToNodesMap[useDef];
  for (FLNodeElabVectorsIter v = nodeVectors.begin(); v != nodeVectors.end(); ++v)
  {
    FLNodeElabVector& nodes = *v;
    for (FLNodeElabVectorIter f = nodes.begin(); f != nodes.end(); ++f)
    {
      FLNodeElab* curFlow = *f;
      flowSet.insert(curFlow);
    }
  }
}

void
SCHBlockMerge::gatherAllBlockFlows(MergeBlock* block, FLNodeElabVector* allNodes)
  const
{
  // Use any of the flows to gather all of them
  FLNodeElabVector* nodes = *(block->loopGroups());
  FLNodeElab* flow = *nodes->begin();
  NUUseDefNode* useDef = flow->getUseDefNode();
  SCHUtil::BlockNodesLoop l;
  for (l = mUtil->loopAllBlockNodes(*mBlockToNodesMap, useDef);
       !l.atEnd(); ++l)
  {
    FLNodeElab* flow = *l;
    FLN_ELAB_ASSERT(flow->getUseDefNode() == useDef, flow);
    allNodes->push_back(flow);
  }
}

SCHBlockMerge::MergeBlock::MergeBlock(FLNodeElabVector* nodes) :
  mIsMergable(true), mHierSet(NULL), mScheduleSet(NULL)
{
  mScheduleSets = new ScheduleSets;
  mGroups = new SCHFlowGroups;
  mGroups->push_back(nodes);
}

SCHBlockMerge::MergeBlock::~MergeBlock()
{
  delete mScheduleSets;
  delete mGroups;
}

void SCHBlockMerge::MergeBlock::addGroup(FLNodeElabVector* nodes)
{
  mGroups->addGroup(nodes);
}

SCHFlowGroups::FlowsLoop SCHBlockMerge::MergeBlock::loopFlowNodes() const
{
  return mGroups->loopFlowNodes();
}

SCHFlowGroupsLoop SCHBlockMerge::MergeBlock::loopGroups() const
{
  return mGroups->loopGroups();
}

bool SCHBlockMerge::MergeBlock::empty() const
{
  return mGroups->empty();
}

FLNodeElab* SCHBlockMerge::MergeBlock::getRepresentativeFlowElab() const
{
  // The first groups use def node is probably the best since it was
  // what the group was created with.
  INFO_ASSERT(!mGroups->empty(), "Accessing data in an empty schedule block");
  FLNodeElabVector* nodes = *(mGroups->begin());
  INFO_ASSERT(!nodes->empty(),
              "Accessing data in an empty group of a schedule block");
  FLNodeElab* flowElab = *(nodes->begin());
  return flowElab;
}

FLNodeElab* SCHBlockMerge::MergeBlock::getSortedRepresentativeFlowElab() const
{
  // Walk all the flows and get one that is sorted less
  FLNodeElab* flowElab = NULL;
  for (SCHFlowGroups::FlowsLoop l = loopFlowNodes(); !l.atEnd(); ++l) {
    FLNodeElab* curFlowElab = *l;
    if (flowElab == NULL) {
      flowElab = curFlowElab;
    } else if (FLNodeElab::compare(curFlowElab, flowElab) < 0) {
      flowElab = curFlowElab;
    }
  }
  INFO_ASSERT(flowElab != NULL, "Should not have an empty mergable block");
  return flowElab;
}

NUUseDefNode* SCHBlockMerge::MergeBlock::getRepresentativeUseDefNode() const
{
  FLNodeElab* flowElab = getRepresentativeFlowElab();
  return flowElab->getUseDefNode();
}

void SCHBlockMerge::MergeBlock::print(SCHMarkDesign* mark, bool printSignature)
{
  // Print the block pointer
  UtIO::cout() << "MergeBlock(" << this << ":\n";

  // Print the groups and contents
  UInt32 groupNum = 0;
  for (SCHFlowGroupsLoop l = loopGroups(); !l.atEnd(); ++l) {
    // Print the group header and size
    FLNodeElabVector* nodes = *l;
    UtIO::cout() << "Group " << ++groupNum << " size=" << nodes->size()
                 << "\n";

    // Print the group nodes
    for (FLNodeElabVectorIter n = nodes->begin(); n != nodes->end(); ++n)
    {
      FLNodeElab* flow = *n;
      UtString flowName;
      SCHDump::composeFlowName(flow, &flowName);
      UtIO::cout() << " - " << flowName;
      if (printSignature)
      {
	UtIO::cout() << " (";
	mark->getSignature(flow)->print();
	UtIO::cout() << ")\n";
      }
      else
	UtIO::cout() << UtIO::endl;
    }
  }

  // TBD Can't print the fanin, does that matter?
} // void SCHBlockMerge::MergeBlock::print

const SCHBlockMerge::ScheduleSet*
SCHBlockMerge::MergeBlock::getScheduleSet(const STBranchNode* hier) const
{
  const ScheduleSet* scheduleSet;
  ScheduleSets::iterator pos = mScheduleSets->find(hier);
  if (pos == mScheduleSets->end())
    scheduleSet = NULL;
  else
    scheduleSet = pos->second;
  return scheduleSet;
}

const SCHBlockMerge::ScheduleSet*
SCHBlockMerge::MergeBlock::getAllInstancesScheduleSet() const
{
  if (mScheduleSet == NULL)
  {
    bool diff = false;
    for (ScheduleSetsLoop l = loopScheduleSets(); !l.atEnd() && !diff; ++l)
    {
      const ScheduleSet* scheduleSet = l.getValue();
      if (mScheduleSet == NULL)
	mScheduleSet = scheduleSet;
      else if (mScheduleSet != scheduleSet)
      {
	mScheduleSet = NULL;
	diff = true;
      }
    }
  }
  return mScheduleSet;
}

void
SCHBlockMerge::MergeBlock::putScheduleSet(const STBranchNode* hier,
				     const ScheduleSet* scheduleSet)
{
  // Add this hierarchy schedule set
  ScheduleSets::iterator pos = mScheduleSets->find(hier);
  if (pos == mScheduleSets->end())
    mScheduleSets->insert(ScheduleSets::value_type(hier, scheduleSet));
  else
    pos->second = scheduleSet;
}

SCHBlockMerge::ScheduleSetsLoop
SCHBlockMerge::MergeBlock::loopScheduleSets() const
{
  return ScheduleSetsLoop(*mScheduleSets);
}

int
SCHBlockMerge::MergeBlock::compare(const MergeBlock* b1, const MergeBlock* b2)
{
  // Check for the easy case first
  int cmp = 0;
  if (b1 == b2) {
    cmp = 0;
  } else {
    // Pick a representative flow node
    FLNodeElab* f1 = b1->getSortedRepresentativeFlowElab();
    FLNodeElab* f2 = b2->getSortedRepresentativeFlowElab();

    // Compare by elaborated net first
    NUNetElab* netElab1 = f1->getDefNet();
    NUNetElab* netElab2 = f2->getDefNet();
    cmp = NUNetElab::compare(netElab1, netElab2);
    if (cmp == 0) {
      // Compare by elaborated flow which does a usedef compare first
      cmp = FLNodeElab::compare(f1, f2);
    }
  }

//#define MB_DEBUG_BLOCK_COMPARE
#ifdef MB_DEBUG_BLOCK_COMPARE
  // This code is useful if you suspect that the comparison is
  // inconsistent (meaning that compare(f1,f2) and compare(f2,f1)
  // might be giving the same answer other than 0).  To use it,
  // uncomment it, run the testcase and grep for the
  // MergeBlock::compare() lines.  Pipe that through:
  //
  //   cut -d' ' -f2- | tsort 2> tsort.errors
  //
  // If there are cycles (meaning the order was not consistent), tsort
  // will print the pointers involved to stderr and you can use them
  // to set breakpoints in the debugger.
  if (cmp != 0) {
    UtIO::cout() << "MergeBlock::compare(): ";
    if (cmp < 0)
      UtIO::cout() << b1 << " " << b2 << "\n";
    else
      UtIO::cout() << b2 << " " << b1 << "\n";
    UtIO::cout().flush();
  }
#endif

  return cmp;
} // int SCHBlockMerge::MergeBlock::compare


int 
SCHBlockMerge::MergeBlock::compareHierScheduleSets(const MergeBlock * b1, 
                                                   const MergeBlock * b2)
{
  int cmp = 0;

  // Compare by hierarchy sets
  const HierSet* h1 = b1->getHierSet();
  const HierSet* h2 = b2->getHierSet();
  cmp = CmpHierSets::compare(h1, h2);
  if (cmp==0) {
    // Hierarchy lines up; compare by schedules
    for (HierSet::const_iterator iter = h1->begin();
	 (cmp==0) and iter != h1->end();
	 ++iter) {
      const STBranchNode * hier = (*iter);
      const ScheduleSet * s1 = b1->getScheduleSet(hier);
      const ScheduleSet * s2 = b2->getScheduleSet(hier);
      NU_ASSERT(s1 != NULL, b1->getRepresentativeUseDefNode());
      NU_ASSERT(s2 != NULL, b2->getRepresentativeUseDefNode());
      cmp = CmpScheduleSets::compare(s1, s2);
    }
  }
  return cmp;
}

SCHBlockMerge::BlocksLoop SCHBlockMerge::findFlowBlocks(FLNodeElab* flowElab) const
{
  FlowToBlocksMapIter pos = mFlowToBlocksMap->find(flowElab);
  if (pos != mFlowToBlocksMap->end()) {
    Blocks* blocks = pos->second;
    return BlocksLoop(*blocks);
  } else {
    return BlocksLoop();
  }
}

SCHBlockMerge::AllBlocksLoop SCHBlockMerge::loopAllBlocks()
{
  AllBlocksLoop loop;
  loop.pushLoop(BlocksLoop(*mMergableBlocks));
  loop.pushLoop(BlocksLoop(*mUnMergableBlocks));
  return loop;
}

void SCHBlockMerge::printBlockFanin(MergeBlock* block)
{
  UtIO::cout() << "Flow Fanin:\n";
  FLNodeElabVector nodes;
  SCHBlockMerge::gatherBlockFlows(block, &nodes);
  for (FLNodeElabVectorIter f = nodes.begin(); f != nodes.end(); ++f)
  {
    FLNodeElab* flow = *f;
    UtString flowName;
    SCHDump::composeFlowName(flow, &flowName);
    UtIO::cout() << " - " << flowName << " (UD = " << flow->getUseDefNode()
		 << "):\n";
    SCHMarkDesign::FlowDependencyLoop p;
    for (p = mMarkDesign->loopDependencies(flow); !p.atEnd(); ++p) {
      FLNodeElab* fanin = *p;
      flowName.clear();
      SCHDump::composeFlowName(fanin, &flowName);
      UtIO::cout() << " - " << flowName << " (UD = " << flow->getUseDefNode()
                   << "):\n";
    }
  }
} // void SCHBlockMerge::printBlockFanin

void SCHBlockMerge::initHierSets()
{
  BlockMap::iterator p;
  for (p = mDesignBlockMap->begin(); p != mDesignBlockMap->end(); ++p)
  {
    // Gather the hierarchies
    HierSetVec hierSetVec;
    MergeBlock* block = p->second;
    for (ScheduleSetsLoop l = block->loopScheduleSets(); !l.atEnd(); ++l)
    {
      const STBranchNode* hier = l.getKey();
      hierSetVec.push_back(hier);
    }

    // Create the new hier set
    const HierSet* hierSet;
    hierSet = mUniqueHierSetFactory->createUniqueSet(hierSetVec);
    block->putHierSet(hierSet);
  }
}

void
SCHBlockMerge::gatherBlockFlows(const MergeBlock* block, FLNodeElabVector* flows)
{
  FLNodeElabSet covered;
  for (SCHFlowGroups::FlowsLoop l = block->loopFlowNodes(); !l.atEnd(); ++l) {
    FLNodeElab* flow = *l;
    if (covered.find(flow) == covered.end()) {
      covered.insert(flow);
      flows->push_back(flow);
    }
  }
}

bool 
SCHBlockMerge::BlockBuckets::operator()(const GraphNode* gn1, const GraphNode* gn2)
  const
{
  // Get the MergeBlock for each node and call the derived classes compare
  MergeBlock* block1 = mMergeGraph->castNode(gn1)->getData();
  MergeBlock* block2 = mMergeGraph->castNode(gn2)->getData();
  return mBlockMerge->compareBuckets(block1, block2) < 0;
}

bool 
SCHBlockMerge::BlockNetBuckets::operator()(const GraphNode* gn1,
                                           const GraphNode* gn2)
  const
{
  // Get the MergeBlock for each node and call the derived classes compare
  MergeBlock* block1 = mMergeGraph->castNode(gn1)->getData();
  MergeBlock* block2 = mMergeGraph->castNode(gn2)->getData();
  int cmp = mBlockMerge->compareBuckets(block1, block2);
  if (cmp == 0) {
    // Gather the defs for the first block, we give up if there is more than one
    NUNetElab* def1 = NULL;
    bool intersect = true;
    for (SCHFlowGroups::FlowsLoop l = block1->loopFlowNodes();
         !l.atEnd() && intersect; ++l) {
      FLNodeElab* flowElab = *l;
      NUNetElab* netElab = flowElab->getDefNet();
      if (def1 == NULL) {
        def1 = netElab;
      } else if (def1 != netElab) {
        intersect = false;
      }
    }

    // Gather the defs for the second block
    NUNetElab* def2 = NULL;
    for (SCHFlowGroups::FlowsLoop l = block2->loopFlowNodes();
         !l.atEnd() && intersect; ++l) {
      FLNodeElab* flowElab = *l;
      NUNetElab* netElab = flowElab->getDefNet();
      if (def2 == NULL) {
        def2 = netElab;
      } else if (def2 != netElab) {
        intersect = false;
      }
    }

    // If there is no intersection, don't merge and choose ordering
    intersect &= (def1 == def2);
    if (!intersect) {
      cmp = MergeBlock::compare(block1, block2);
    }
  } // if
  return cmp < 0;
}

bool
SCHBlockMerge::BlockOrder::operator()(const GraphNode* gn1, const GraphNode* gn2)
  const
{
  // Get the MergeBlock for each node
  MergeBlock* block1 = mMergeGraph->castNode(gn1)->getData();
  MergeBlock* block2 = mMergeGraph->castNode(gn2)->getData();

  // Compare the blocks
  return MergeBlock::compare(block1, block2) < 0;
}

bool
SCHBlockMerge::EdgeOrder::operator()(const GraphEdge* ge1, const GraphEdge* ge2)
  const
{
  // Get the endpoints of the edges and convert them to MergeBlock's
  const GraphNode* gn1 = mMergeGraph->endPointOf(ge1);
  const GraphNode* gn2 = mMergeGraph->endPointOf(ge2);
  MergeBlock* block1 = mMergeGraph->castNode(gn1)->getData();
  MergeBlock* block2 = mMergeGraph->castNode(gn2)->getData();

  // Compare the blocks
  return MergeBlock::compare(block1, block2) < 0;
}

bool SCHBlockMerge::validStartNode(const GraphNode* node) const
{
  MergeBlock* block = mMergeGraph->castNode(node)->getData();
  const NUUseDefNode* useDef = block->getRepresentativeUseDefNode();
  return validStartBlock(useDef, block);
}

bool SCHBlockMerge::nodeMergable(const GraphNode* node) const
{
  return mMergeGraph->castNode(node)->getData()->isMergable();
}

bool 
SCHBlockMerge::edgeMergable(const GraphEdge* edge) const
{
  // Get the MergeEdge and MergeBlock for each node
  MergeEdge* mergeEdge = mMergeGraph->castEdge(edge)->getData();
  GraphNode* fanout = mMergeGraph->startPointOf(edge);
  GraphNode* fanin = mMergeGraph->endPointOf(edge);
  MergeBlock* fanoutBlock = mMergeGraph->castNode(fanout)->getData();
  MergeBlock* faninBlock = mMergeGraph->castNode(fanin)->getData();

  return edgeMergable(mergeEdge, fanoutBlock, faninBlock, mDumpMergedBlocks > 1);
}

void SCHBlockMerge::mergeNodes(const NodeVector& nodes)
{
  // Don't do the work unless we were asked to do it
  INFO_ASSERT(nodes.size() > 1,
              "Attempting to block merge a single block");

  // Update the stat counts
  mMergedBlocks += nodes.size();
  ++mMergeCount;

  // Gather the groups for merging and possibly print merged nodes
  FLNodeElabVectors nodeVectors;
  nodeVectors.resize(nodes.size());
  int i = 0;
  MergeBlock* lastBlock = NULL;
  for (NodeVectorLoop l(nodes); !l.atEnd(); ++l) {
    GraphNode* graphNode = *l;
    MergeBlock* block = mMergeGraph->castNode(graphNode)->getData();
    lastBlock = block;
    FLNodeElabVector& groupNodes = nodeVectors[i++];
    gatherAllBlockFlows(block, &groupNodes);

    // Print the merged nodes if requested
    if (mDumpMergedBlocks >= 1) {
      printMergedNodes(block, getDepth(graphNode));
    }
  }

  // Pre merge updates
  for (NodeVectorLoop l(nodes); !l.atEnd(); ++l) {
    GraphNode* graphNode = *l;
    MergeBlock* block = mMergeGraph->castNode(graphNode)->getData();
    if (block != lastBlock) {
      // Update the block groups in the various combinational schedules
      preMergeCallback(lastBlock, block);
    }
  }

  // Do the merge
  mUtil->getTransform()->mergeFlows(nodeVectors);

  // Post merge updates
  for (NodeVectorLoop l(nodes); !l.atEnd(); ++l) {
    GraphNode* graphNode = *l;
    MergeBlock* block = mMergeGraph->castNode(graphNode)->getData();
    if (block != lastBlock) {
      // Update the block groups in the various combinational schedules
      postMergeCallback(lastBlock, block);
    }
  }
  updateBlockMap(nodeVectors, *mBlockToNodesMap);
}

void 
SCHBlockMerge::mergeEdges(GraphEdge* dstGraphEdge, const GraphEdge* srcGraphEdge)
  const
{
  // Get the edge data for both
  MergeEdge* dstEdge = mMergeGraph->castEdge(dstGraphEdge)->getData();
  const MergeEdge* srcEdge = mMergeGraph->castEdge(srcGraphEdge)->getData();

  // If the src crosses hierachy boolean is set, set the dst boolean
  // (equivalent to ORing the two).
  if (srcEdge->getCrossesHierarchy()) {
    dstEdge->putCrossesHierarchy(true);
  }
}

void SCHBlockMerge::printMergedNodes(MergeBlock* block, UIntPtr depth) const
{
  // gather all the nodes in this block and sort them so we get
  // consistend output
  FLNodeElabVector nodes;
  SCHBlockMerge::gatherBlockFlows(block, &nodes);
  std::sort(nodes.begin(), nodes.end(), SCHUtil::lessFlow);

  // Print them
  for (FLNodeElabVectorIter f = nodes.begin(); f != nodes.end(); ++f)
  {
    FLNodeElab* mergeFlow = *f;
    UtString flowName;
    SCHDump::composeFlowName(mergeFlow, &flowName);
    if (SCHUtil::isSequential(mergeFlow)) {
      flowName << " (sequential)";
    }
    UtIO::cout() << flowName << " in merge set " << mMergeCount
                 << ", depth=" << depth+1 << "\n";
  }
}

void SCHBlockMerge::removeMergedNode(GraphNode* /* node */)
{
  return;
}

void SCHBlockMerge::encapsulateCycle(const NodeVector& nodes)
{
  // Mark the nodes unmergable
  for (NodeVectorLoop l(nodes); !l.atEnd(); ++l) {
    GraphNode* graphNode = *l;
    MergeBlock* block = mMergeGraph->castNode(graphNode)->getData();
    block->putUnmergable();
    recordCycleBlock(block);
  }
}

void SCHBlockMerge::computeAllCrossHierachyBlocks()
{
  // We need the flow to hierarchy map to find the right blocks
  createFlowToBlocksMap();

  // Compute it for each block and its fanin
  for (Iter<GraphNode*> i = mMergeGraph->nodes(); !i.atEnd(); ++i) {
    // Iterate over the groups
    GraphNode* node = *i;
    MergeBlock* block = mMergeGraph->castNode(node)->getData();
    computeCrossHierachyBlocks(node, block);
  }
  mCrossesHierarchyValid = true;

  // All done with the map
  clearFlowToBlocksMap();
}

void SCHBlockMerge::computeCrossHierachyBlocks(GraphNode* node, MergeBlock* block)
{
  // Storage to find an edge for a block fanin
  typedef UtMap<MergeBlock*, MergeEdge*> EdgeMap;
  EdgeMap edgeMap;

  // Walk this blocks edges so we can map merge block fanin to the
  // edge data
  if (mReverseEdge) {
    for (Iter<GraphEdge*> i = mMergeGraph->reverseEdges(node); !i.atEnd(); ++i) {
      GraphEdge* graphEdge = *i;
      GraphNode* toNode = mMergeGraph->startPointOf(graphEdge);
      MergeBlock* fanin = mMergeGraph->castNode(toNode)->getData();
      MergeEdge* edge = mMergeGraph->castEdge(graphEdge)->getData();
      edgeMap.insert(EdgeMap::value_type(fanin, edge));
    }
  } else {
    for (Iter<GraphEdge*> i = mMergeGraph->edges(node); !i.atEnd(); ++i) {
      GraphEdge* graphEdge = *i;
      GraphNode* toNode = mMergeGraph->endPointOf(graphEdge);
      MergeBlock* fanin = mMergeGraph->castNode(toNode)->getData();
      MergeEdge* edge = mMergeGraph->castEdge(graphEdge)->getData();
      edgeMap.insert(EdgeMap::value_type(fanin, edge));
    }
  }

  // Walk this blocks fanin
  FLNodeElabSet covered;
  for (SCHFlowGroups::FlowsLoop l = block->loopFlowNodes(); !l.atEnd(); ++l) {
    FLNodeElab* flow = *l;
    if (covered.find(flow) == covered.end()) {
      covered.insert(flow);
      SCHMarkDesign::FlowDependencyLoop p;
      for (p = mMarkDesign->loopDependencies(flow); !p.atEnd(); ++p) {
        FLNodeElab* fanin = *p;
        if (flow->getHier() != fanin->getHier()) {
          for (BlocksLoop b = findFlowBlocks(fanin); !b.atEnd(); ++b) {
            MergeBlock* faninBlock = *b;
            EdgeMap::iterator pos = edgeMap.find(faninBlock);
            if (pos != edgeMap.end()) {
              MergeEdge* edge = pos->second;
              edge->putCrossesHierarchy(true);
            }
          }
        }
      }
    }
  }
} // void SCHBlockMerge::computeCrossHierachyBlocks

void SCHBlockMerge::transposeGraph(void)
{
  UtGraphTranspose<MergeGraph> transpose(mMergeGraph);
  transpose.transpose();
}
