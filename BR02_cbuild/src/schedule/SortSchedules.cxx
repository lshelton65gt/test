// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "schedule/Schedule.h"

#include "util/UtIOStream.h"

//#define DEBUG_SORT_SCHED
#include "InputNets.h"
#include "SortSchedules.h"

SCHSortSchedules::SCHSortSchedules(SCHMarkDesign* mark) : mMarkDesign(mark)
{
  mSchedulesData = new SchedulesData;
  mInputNetsSchedules = new InputNetsSchedules;
  mPriorityQueue = new PriorityQueue;
  mSchedulingStarted = false;
}

SCHSortSchedules::~SCHSortSchedules()
{
  for (SchedulesDataLoop l = loopSchedules(); !l.atEnd(); ++l)
  {
    ScheduleData* scheduleData = l.getValue();
    delete scheduleData->mFanout;
    delete scheduleData;
  }
  delete mSchedulesData;

  bool errors = false;
  for (InputNetsSchedules::iterator p = mInputNetsSchedules->begin();
       p != mInputNetsSchedules->end(); ++p)
  {
    InputNetsData* inputNetsData = p->second;
    if (!inputNetsData->mSchedules->empty())
    {
      Schedules* schedules = inputNetsData->mSchedules;
      UtIO::cout() << "Schedules left over:\n";
      for (SchedulesLoop l(*schedules); !l.atEnd(); ++l)
      {
	SCHScheduleBase* sched = *l;
	sched->print(false);
      }
      errors = true;
    }
    delete inputNetsData->mSchedules;
    delete inputNetsData;
  }
  INFO_ASSERT(!errors, "Look at the preceding messages for more information");
  delete mInputNetsSchedules;
  delete mPriorityQueue;
} // SCHSortSchedules::~SCHSortSchedules

void
SCHSortSchedules::addSched(SCHScheduleBase* sched,
			   const SCHInputNets* inputNets)
{
  addScheduleInputNets(sched, inputNets);
  addInputNetsSchedule(inputNets, sched);
#ifdef DEBUG_SORT_SCHED
  UtIO::cout() << "Added: ";
  sched->print();
#endif
}

void
SCHSortSchedules::addFanout(SCHScheduleBase* sched,
			    SCHScheduleBase* fanoutSched)
{
  addScheduleFanout(sched, fanoutSched);
}

#ifdef DEBUG_SORT_SCHED
void SCHSortSchedules::printQueue(PriorityQueue* priorityQueue)
{
  UtIO::cout() << "** Queue has: ***\n";
  for (PriorityQueue::iterator p = priorityQueue->begin();
       p != priorityQueue->end(); ++p)
  {
    const Priority& priority = p->first;
    ReadyScheds* readyScheds = p->second;
    UtIO::cout() << "  Priority, rem = " << priority.mRemainingCount
		 << ", total = " << priority.mTotalCount
		 << ", fanout = " << priority.mFanoutCount << "\n";
    for (ReadyScheds::iterator q = readyScheds->begin();
	 q != readyScheds->end(); ++q)
    {
      const SCHInputNets* inputNets = q->first;
      Schedules* schedules = q->second;
      UtIO::cout() << "    Input nets = " << inputNets << "\n";
      for (SchedulesLoop l(*schedules); !l.atEnd(); ++l)
      {
	SCHScheduleBase* sched = *l;
	UtIO::cout() << "      Schedule: ";
	sched->print();
      }
    }
  } // for
}
#endif


bool SCHSortSchedules::getNextSchedule(ScheduleGroup& schedGroup)
{
  // Make sure scheduling has started
  initiateScheduling();

  // Get the next set of schedules, check if we already have some
#ifdef DEBUG_SORT_SCHED
  UtIO::cout() << "Getting schedules called:\n";
#endif
  removeReadySchedules(schedGroup);

  // If we have a set of schedules, return the first one
  for (ScheduleGroupLoop l(schedGroup); !l.atEnd(); ++l)
  {
    // Maybe print that we are scheduling this
    SCHScheduleBase* sched = *l;
#ifdef DEBUG_SORT_SCHED
    UtIO::cout() << "  Scheduling ";
    sched->print();
#endif

    // Now that it is going to be scheduled, decrement the fanin count
    // for any of its fanout. If that results in a zero fanin count,
    // that fanout schedule is ready for scheduling.
    for (SchedulesLoop s = loopFanout(sched); !s.atEnd(); ++s)
    {
      SCHScheduleBase* fanoutSched = *s;
#ifdef DEBUG_SORT_SCHED
      UtIO::cout() << "  decrementing: ";
      fanoutSched->print();
#endif
      int faninCount = decrementFaninCount(fanoutSched);
      if (faninCount == 0)
      {
	const SCHInputNets* inputNets = getScheduleInputNets(fanoutSched);
	addReadySchedule(fanoutSched, inputNets);
      }
    }
  } // for

#ifdef DEBUG_SORT_SCHED
  printQueue(mPriorityQueue);
#endif

  return !schedGroup.empty();
} // SCHScheduleBase* SCHSortSchedules::getNextSchedule

SCHSortSchedules::ScheduleGroupLoop
SCHSortSchedules::loopSchedules(ScheduleGroup& schedGroup) const
{
  return ScheduleGroupLoop(schedGroup);
}

void
SCHSortSchedules::addScheduleInputNets(SCHScheduleBase* sched,
				       const SCHInputNets* inputNets)
{
  SCHED_ASSERT(mSchedulesData->find(sched) == mSchedulesData->end(), sched);
  ScheduleData* scheduleData = new ScheduleData;
  scheduleData->mFanout = new Schedules;
  scheduleData->mInputNets = inputNets;
  scheduleData->mFaninCount = 0;
  mSchedulesData->insert(SchedulesDataValue(sched, scheduleData));
}

SCHSortSchedules::ScheduleData*
SCHSortSchedules::findScheduleData(SCHScheduleBase* sched)
{
  SchedulesData::iterator pos = mSchedulesData->find(sched);
  SCHED_ASSERT(pos != mSchedulesData->end(), sched);
  ScheduleData* scheduleData = pos->second;
  return scheduleData;
}

const SCHInputNets*
SCHSortSchedules::getScheduleInputNets(SCHScheduleBase* sched)
{
  ScheduleData* scheduleData = findScheduleData(sched);
  return scheduleData->mInputNets;
}

SCHSortSchedules::SchedulesDataLoop SCHSortSchedules::loopSchedules()
{
  return SchedulesDataLoop(*mSchedulesData);
}

void
SCHSortSchedules::addScheduleFanout(SCHScheduleBase* sched,
				    SCHScheduleBase* fanoutSched)
{
  ScheduleData* scheduleData = findScheduleData(sched);
  scheduleData->mFanout->insert(fanoutSched);
}

int SCHSortSchedules::fanoutCount(SCHScheduleBase* sched)
{
  ScheduleData* scheduleData = findScheduleData(sched);
  return scheduleData->mFanout->size();
}

void SCHSortSchedules::incrementFaninCount(SCHScheduleBase* sched)
{
  ScheduleData* scheduleData = findScheduleData(sched);
  ++(scheduleData->mFaninCount);
}

int SCHSortSchedules::decrementFaninCount(SCHScheduleBase* sched)
{
  ScheduleData* scheduleData = findScheduleData(sched);
  return --(scheduleData->mFaninCount);
}

int SCHSortSchedules::getFaninCount(ScheduleData* scheduleData)
{
  return scheduleData->mFaninCount;
}

SCHSortSchedules::SchedulesLoop
SCHSortSchedules::loopFanout(SCHScheduleBase* sched)
{
  ScheduleData* scheduleData = findScheduleData(sched);
  return SchedulesLoop(*scheduleData->mFanout);
}

SCHSortSchedules::InputNetsData*
SCHSortSchedules::findInputNetsData(const SCHInputNets* inputNets)
{
  // Find if it already exists
  InputNetsData* inData = NULL;
  InputNetsSchedules::iterator pos = mInputNetsSchedules->find(inputNets);
  if (pos == mInputNetsSchedules->end())
  {
    // Create it
    inData = new InputNetsData;
    inData->mSchedules = new Schedules;
    inData->mTotalCount = 0;
    inData->mRemainingCount = 0;
    mInputNetsSchedules->insert(InputNetsSchedulesValue(inputNets,inData));
  }
  else
    inData = pos->second;
  return inData;
}

void
SCHSortSchedules::addInputNetsSchedule(const SCHInputNets* inputNets,
				       SCHScheduleBase* sched)
{
  InputNetsData* inputNetsData = findInputNetsData(inputNets);
  inputNetsData->mSchedules->insert(sched);
  inputNetsData->mTotalCount++;
  inputNetsData->mRemainingCount++;
}

void
SCHSortSchedules::removeInputNetsSchedules(const SCHInputNets* inputNets,
					   SCHScheduleBase* sched,
					   int* totalCount,
					   int* remainingCount)
{
  // Get the information about this set of schedules
  InputNetsData* inputNetsData = findInputNetsData(inputNets);
  *totalCount = inputNetsData->mTotalCount;
  *remainingCount = inputNetsData->mRemainingCount;

  // Find the schedule and remove it
  Schedules* schedules = inputNetsData->mSchedules;
  SCHED_ASSERT(schedules->find(sched) != schedules->end(), sched);
  schedules->erase(sched);
  inputNetsData->mRemainingCount--;
}

void
SCHSortSchedules::addReadySchedule(SCHScheduleBase* sched,
				   const SCHInputNets* inputNets)
{
  // Find the current priority for this schedule
  int totalCount;
  int remainingCount;
  removeInputNetsSchedules(inputNets, sched, &totalCount, &remainingCount);
  int fanout = fanoutCount(sched);
  Priority priority(remainingCount, totalCount, fanout);

#ifdef DEBUG_SORT_SCHED
  UtIO::cout() << "Adding ready: ";
  sched->print();
  UtIO::cout() << "Removing from input nets (" << inputNets << "):\n";
  UtIO::cout() << "  Priority: rem = " << remainingCount
	       << ", total = " << totalCount
	       << ", fanout = " << fanout << "\n";
#endif

  // Check if we already have schedules with this set of input nets in
  // the priority queue. If so, remove it
  PriorityQueue::iterator p = mPriorityQueue->find(priority);
  Schedules* schedules = NULL;
  if (p != mPriorityQueue->end())
  {
    // Check if it is in the ready schedules for this priority
    ReadyScheds* readyScheds = p->second;
    ReadyScheds::iterator pos = readyScheds->find(inputNets);
    if (pos != readyScheds->end())
    {
      schedules = pos->second;
      readyScheds->erase(inputNets);
      if (readyScheds->empty())
      {
	delete readyScheds;
	mPriorityQueue->erase(p);
      }
    }
  }

  // If we didn't find it, we need a new one
  if (schedules == NULL)
    schedules = new Schedules;
  schedules->insert(sched);

  // Allocate space on the priority queue again but with one less
  // remaining schedule
  priority.decrementRemaining();
  p = mPriorityQueue->find(priority);
  ReadyScheds* readyScheds;
  if (p == mPriorityQueue->end())
  {
    // Doesn't exist yet
    readyScheds = new ReadyScheds;
    mPriorityQueue->insert(PriorityQueueValue(priority,	readyScheds));
#ifdef DEBUG_SORT_SCHED
    UtIO::cout() << "New priority added to queue\n";
#endif
  }
  else
    readyScheds = p->second;

  // Insert this schedule set into this part of the queue
  SCHED_ASSERT(readyScheds->find(inputNets) == readyScheds->end(), sched);
  readyScheds->insert(ReadySchedsValue(inputNets, schedules));

#ifdef DEBUG_SORT_SCHED
  UtIO::cout() << "Adding to input nets (" << inputNets << "):\n";
  UtIO::cout() << "  Priority: rem = " << remainingCount-1
	       << ", total = " << totalCount
	       << ", fanout = " << fanout << "\n";
  UtIO::cout() << "Queue size = " << mPriorityQueue->size() << "\n";
#endif
} // SCHSortSchedules::addReadySchedule

void SCHSortSchedules::removeReadySchedules(ScheduleGroup& schedGroup)
{
  // Make sure we have schedules in the queue
  PriorityQueue::iterator p = mPriorityQueue->begin();
  if (p == mPriorityQueue->end())
    return;

  // Grab all the schedules with zero remaining schedules for a set of
  // input nets. These can all be scheduled together. We grab them all
  // so that our caller can sort within this schedules by another
  // mechanism.
  while ((p != mPriorityQueue->end()) && (p->first.mRemainingCount == 0))
  {
    // Get the set of schedules that are ready to go
    ReadyScheds* readyScheds = p->second;
    INFO_ASSERT(!readyScheds->empty(),
                "An empty set of schedules found when sorting schedules");

#ifdef DEBUG_SORT_SCHED
    const Priority* priority = &(p->first);
    UtIO::cout() << "  Removed ready priority, rem = "
		 << priority->mRemainingCount
		 << ", total = " << priority->mTotalCount
		 << ", fanout = " << priority->mFanoutCount << "\n";
    UtIO::cout() << "  ReadyScheds size = " << readyScheds->size()
		 << ", queue size = " << mPriorityQueue->size()
		 << "\n";
#endif

    // Get all the schedules from this group
    ReadyScheds::iterator pos;
    for (pos = readyScheds->begin(); pos != readyScheds->end(); ++pos)
    {
      Schedules* schedules = pos->second;
      INFO_ASSERT(!schedules->empty(),
                  "An empty set of schedules found while sorting them");
#ifdef DEBUG_SORT_SCHED
      UtIO::cout() << "  Input nets is " << pos->first << "\n";
#endif
      for (SchedulesLoop l(*schedules); !l.atEnd(); ++l)
      {
	SCHScheduleBase* sched = *l;
	schedGroup.push_back(sched);
      }
      delete schedules;
    }

    // All done with this set of schedules
    delete readyScheds;
    mPriorityQueue->erase(p);
    p = mPriorityQueue->begin();
  } // while

  // If we didn't get any schedules it means we don't have any
  // schedules with a zero remaining count. Get the next best thing.
  if (schedGroup.empty())
  {
    // We must have given up in the while loop above!
    const Priority* priority = &(p->first);
    INFO_ASSERT(priority->mRemainingCount > 0,
                "A problem was found when sorting a set of schedules");

    // Get the first set of ready schedules
    ReadyScheds* readyScheds = p->second;
    ReadyScheds::iterator pos = readyScheds->begin();
    Schedules* schedules = pos->second;
#ifdef DEBUG_SORT_SCHED
    UtIO::cout() << "  Removed not-ready priority, rem = "
		 << priority->mRemainingCount
		 << ", total = " << priority->mTotalCount
		 << ", fanout = " << priority->mFanoutCount << "\n";
    UtIO::cout() << "  ReadyScheds size = " << readyScheds->size()
		 << ", queue size = " << mPriorityQueue->size()
		 << "\n";
    UtIO::cout() << "  Input nets is " << pos->first << "\n";
#endif

    // Remove this set of schedules from the queue
    readyScheds->erase(pos);
    if (readyScheds->empty())
    {
      delete readyScheds;
      mPriorityQueue->erase(p);
    }

    // Copy the groups over
    for (SchedulesLoop l(*schedules); !l.atEnd(); ++l)
    {
      SCHScheduleBase* sched = *l;
      schedGroup.push_back(sched);
    }
    delete schedules;
  }

  INFO_ASSERT(!schedGroup.empty(),
              "A cycle was found in the order of a set of schedules");
}

void
SCHSortSchedules::initiateScheduling()
{
  // Only initiate if we haven't before
  if (mSchedulingStarted)
    return;
  mSchedulingStarted = true;

  // Make sure there are no cycles
  Schedules covered;
  Schedules busy;
  ScheduleGroup scheduleStack;
  for (SchedulesDataLoop l = loopSchedules(); !l.atEnd(); ++l)
  {
    SCHScheduleBase* sched = l.getKey();
    findCycle(sched, covered, busy, scheduleStack);
  }

  // Compute the fanin count for all schedules
  for (SchedulesDataLoop l = loopSchedules(); !l.atEnd(); ++l)
  {
    ScheduleData* scheduleData = l.getValue();
#ifdef DEBUG_SORT_SCHED
    UtIO::cout() << "from: ";
    l.getKey()->print();
#endif
    for(SchedulesLoop p(*scheduleData->mFanout); !p.atEnd(); ++p)
    {
      SCHScheduleBase* fanoutSched = *p;
      incrementFaninCount(fanoutSched);
#ifdef DEBUG_SORT_SCHED
      UtIO::cout() << "  incrementing ";
      fanoutSched->print();
#endif
    }
  }
  Schedules schedules;
  for (SchedulesDataLoop l = loopSchedules(); !l.atEnd(); ++l)
  {
    ScheduleData* scheduleData = l.getValue();
    if (getFaninCount(scheduleData) == 0)
    {
      SCHScheduleBase* sched = l.getKey();
      schedules.insert(sched);
    }
  }

  // Add the zero fanin schedules to the ready queue
  INFO_ASSERT(!schedules.empty(), "Not all schedules were correctly sorted");
  for (SchedulesLoop p(schedules); !p.atEnd(); ++p)
  {
    SCHScheduleBase* sched = *p;
    const SCHInputNets* inputNets = getScheduleInputNets(sched);
    addReadySchedule(sched, inputNets);
  }

#ifdef DEBUG_SORT_SCHED
  printQueue(mPriorityQueue);
#endif
} // SCHSortSchedules::initiateScheduling

void SCHSortSchedules::findCycle(SCHScheduleBase* sched, Schedules& covered,
				 Schedules& busy, ScheduleGroup& scheduleStack)
{
  // Check if we found a cycle
  if (busy.find(sched) != busy.end()) {
    // Gather the schedules in the cycle
    ScheduleVector schedules;
    schedules.push_back(sched);
    ScheduleGroup::reverse_iterator p = scheduleStack.rbegin();
    for (; (p != scheduleStack.rend()) && (*p != sched); ++p) {
      SCHScheduleBase* cycleSched = *p;
      schedules.push_back(cycleSched);
    }

    // Print the cycle and return (we assert at the end of processing
    // so we see every cycle
    printCycle(schedules);
    return;
  }

  // Check if we have been here before
  if (covered.find(sched) != covered.end()) {
    return;
  }
  covered.insert(sched);

  // Keep looking through the fanout
  busy.insert(sched);
  scheduleStack.push_back(sched);
  for (SchedulesLoop l = loopFanout(sched); !l.atEnd(); ++l) {
    SCHScheduleBase* faninSched = *l;
    findCycle(faninSched, covered, busy, scheduleStack);
  }
  scheduleStack.pop_back();
  busy.erase(sched);
}

void SCHSortSchedules::printCycle(const ScheduleVector& schedules)
{
  // Gather the schedudules in a cycle into a searchable map with ids
  typedef UtMap<SCHScheduleBase*, int> ScheduleIDs;
  ScheduleIDs scheduleIDs;
  int id = 0;
  for (CLoop<ScheduleVector> l(schedules); !l.atEnd(); ++l) {
    SCHScheduleBase* cycleSched = *l;
    scheduleIDs[cycleSched] = ++id;
  }

  // Print the cycle
  UtIO::cout() << "Found a schedule cycle:\n";
  for (CLoop<ScheduleVector> l(schedules); !l.atEnd(); ++l) {
    SCHScheduleBase* cycleSched = *l;
    int id = scheduleIDs[cycleSched];
    UtIO::cout() << id << ": ";
    cycleSched->print(true, mMarkDesign);

    // Dump the relevant fanout
    UtIO::cout() << "Fanouts:";
    for (SchedulesLoop f = loopFanout(cycleSched); !f.atEnd(); ++f) {
      SCHScheduleBase* fanout = *f;
      ScheduleIDs::iterator pos = scheduleIDs.find(fanout);
      if (pos != scheduleIDs.end()) {
        UtIO::cout() << " " << pos->second;
      }
    }
    UtIO::cout() << "\n";
  }
}

bool
SCHSortSchedules::CmpInputNets::operator()(const SCHInputNets* ns1,
                                           const SCHInputNets* ns2) const
{
  // Check for NULL pointers
  if (ns1 == ns2) {
    return false;
  } else if (ns1 == NULL) {
    return true;
  } else if (ns2 == NULL) {
    return false;
  } else {
    return SCHInputNetsFactory::compare(ns1, ns2) < 0;
  }
}
