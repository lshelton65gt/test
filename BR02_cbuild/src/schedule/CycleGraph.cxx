// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Contains method implementations for the CycleGraph class for
  representing and writing flow dependencies for cycle detection.
*/

#include "util/Graph.h"
#include "util/GenericDigraph.h"
#include "util/GraphDot.h"
#include "CycleGraph.h"
#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"
#include "nucleus/NUNetRef.h"
#include "bdd/BDD.h"
#include "util/UtMap.h"
#include "util/UtIOStream.h"
#include "MarkDesign.h"
#include "Util.h"

class NUDesign;
class STSymbolTable;

// The constructor just calls extractDependencyGraph() to initialize the graph
CycleGraph::CycleGraph(NUDesign* design,
                       STSymbolTable* symtab,
                       SCHMarkDesign* markDesign,
                       SCHUtil* util,
                       bool writeDotFiles)
  : mGraph(NULL), mNodeMap(), mWriteDotFiles(writeDotFiles)
{
  mGraph = extractDependencyGraph(design, symtab, markDesign, util);
  if (mWriteDotFiles)
    writeDependencyGraph(mGraph, "dependencies.dot");
}

// Dead flow, bound nodes and flow from initial or sequential blocks
// can never be in a cycle
bool CycleGraph::includeFlow(const FLNodeElab* flow)
{
  return (flow->isReachable() && !flow->isBoundNode() &&
          !SCHUtil::isInitial(flow) && !SCHUtil::isSequential(flow) &&
          SCHUtil::isContDriver(flow));
}

/* This is the main routine for extracting dependency information from
 * the design flow and creating a graph from it.  It creates a graph
 * node for each flow node that could be in a cycle and build a map
 * from flow nodes to graph nodes.  Then it adds an edge between graph
 * nodes to represent combinational driver dependencies.  Finally, it
 * adds dependencies for drivers of a net onto weaker drivers of the
 * same net.
 */
DependencyGraph* CycleGraph::extractDependencyGraph(NUDesign* design,
                                                    STSymbolTable* symtab,
                                                    SCHMarkDesign* markDesign,
                                                    SCHUtil* util)
{
  DependencyGraph* graph = new DependencyGraph;

  static const UInt32 visitedFlag = 1;

  // set up graph nodes for each FLNodeElab
  FLDesignElabIter iter(design, symtab,
                        FLIterFlags::eAllElaboratedNets,
                        FLIterFlags::eAll,
                        FLIterFlags::eNone,
                        FLIterFlags::eBranchAtAll,
                        FLIterFlags::eIterNodeOnce);
  for (; !iter.atEnd(); ++iter)
  {
    FLNodeElab* elab = *iter;

    if (!includeFlow(elab))
      continue;

    FLN_ELAB_ASSERT(mNodeMap.find(elab) == mNodeMap.end(),elab);
    DependencyGraph::Node* node = new DependencyGraph::Node(elab);
    graph->addNode(node);
    mNodeMap[elab] = node;
  }

  // add edges for each FLNodeElab
  UtSet<const NUNetElab*> coveredNets;
  UtSet<std::pair<DependencyGraph::Node*,DependencyGraph::Node*> > coveredEdges;
  for (iter.begin(); !iter.atEnd(); ++iter)
  {
    FLNodeElab* elab = *iter;

    if (!includeFlow(elab))
      continue;

    NodeMap::iterator n = mNodeMap.find(elab);
    if (n == mNodeMap.end())
      continue;

    DependencyGraph::Node* node = n->second;

    // we have to guarantee that we don't process a node more than once, since the iterator won't do it
    if (graph->anyFlagSet(node,visitedFlag))
      continue;
    graph->setFlags(node,visitedFlag);

    // We want to add all "true fanin" for combination flow
    for (SCHMarkDesign::TrueFaninLoop loop = markDesign->loopTrueFanin(elab); !loop.atEnd(); ++loop)
    {
      FLNodeElab* fanin = *loop;
      if (!includeFlow(fanin))
        continue;
      n = mNodeMap.find(fanin);
      FLN_ELAB_ASSERT(n != mNodeMap.end(),fanin);
      DependencyGraph::Node* to = n->second;
      DependencyGraph::Edge* edge = new DependencyGraph::Edge(to,BDD());
      graph->setFlags(edge, eFlowDependency);
      node->addEdge(edge);
    }
    
    // loopTrueFanin() ignores self cycles because it introduces
    // pessimism. But we sometimes want to treat a self cycle as an
    // encapsulated cycle. This is because to match simulation we
    // have to run the block until it settles. An example is:
    //
    //        always @ (vec or in)
    //          begin
    //            vec[0] <= ~in;
    //            vec[1] <= ~vec[0];
    //          end
    //
    // Reordering will change the order of the statements requiring
    // us to run the block twice to get the right answer. But since
    // we only have one encapsulated flow for vec, we don't see the
    // problem.
    //
    // So for now, we encapsulate these. In the future, we will try
    // to reorder the statements to reduce the cycles in local
    // analysis.
    //
    // Since this can have a negative performance impact, we allow
    // the user to disable it.
    if (elab->isFanin(elab, eSensLevelOrEdge) &&
        !util->isFlagSet(eNoSelfCycles)) {
      DependencyGraph::Edge* edge = new DependencyGraph::Edge(node, BDD());
      graph->setFlags(edge, eFlowDependency);
      node->addEdge(edge);
    }

    // There is an implied ordering constraint that says that when
    // there are multiple drivers for a net, the lower strength driver
    // must run before the higher strength driver.  We make this
    // explicit by adding a dependency from stronger drivers to
    // weakers drivers of the same net.
    const NUNetElab* netElab = elab->getDefNet();
    const NUUseDefNode* useDef = elab->getUseDefNode();
    if (useDef != NULL && netElab->isMultiplyDriven() &&
        (coveredNets.find(netElab) == coveredNets.end()))
    {
      coveredNets.insert(netElab);

      // collect all drivers of this net and sort by strength
      UtVector<Strength> strengths;
      UtMultiMap<Strength,FLNodeElab*> drivers;
      for (NUNetElab::ConstDriverLoop loop = netElab->loopContinuousDrivers(); !loop.atEnd(); ++loop)
      {
        FLNodeElab* driver = *loop;
        if (!includeFlow(driver))
          continue;
        NUUseDefNode* useDef = driver->getUseDefNode();
        if (useDef == NULL)
          continue;
        Strength strength = useDef->getStrength();
        strengths.push_back(strength);
        drivers.insert(UtMultiMap<Strength,FLNodeElab*>::value_type(strength,driver));
      }

      // add dependencies between nodes at each level
      std::sort(strengths.begin(), strengths.end());
      UtVector<Strength>::iterator newEnd = std::unique(strengths.begin(), strengths.end());
      bool firstIteration = true;
      Strength last = eStrDrive;        // Just to avoid gcc uninitialized warnings
      for (UtVector<Strength>::iterator l = strengths.begin(); l != newEnd; ++l)
      {
        Strength strength = *l;
        if (firstIteration == false)
        {
          for (UtMultiMap<Strength,FLNodeElab*>::iterator p = drivers.lower_bound(strength);
               p != drivers.upper_bound(strength);
               ++p)
          {
            FLNodeElab* f = p->second;
            DependencyGraph::Node* from = mNodeMap[f];

            for (UtMultiMap<Strength,FLNodeElab*>::iterator q = drivers.lower_bound(last);
                 q != drivers.upper_bound(last);
                 ++q)
            {
              FLNodeElab* t = q->second;
              DependencyGraph::Node* to = mNodeMap[t];
              DependencyGraph::Edge* edge = new DependencyGraph::Edge(to,BDD());
              graph->setFlags(edge, eStrengthDependency);
              from->addEdge(edge);
            }
          }
        }
        last = strength;
        firstIteration = false;
      }
    }
  }

  return graph;
}

/* There is an implied ordering constraint between drivers of
 * multiply-driven nets that are in cycles.  If one driver gets
 * re-executed, then we must ensure that all other drivers of lesser
 * strength were also re-executed.  This means that if a driver gets
 * put into a cycle, then all its brother drivers of lesser strength
 * must be in the cycle as well.
 *
 * Here we try to encode this implied constraint explicitly in the
 * dependency graph by adding a dependency u->v, from all u which are
 * brother drivers of lesser strength of a node x to all v which are
 * fanin of x.  These edges ensure that if x is in a cycle, then all
 * brother drivers of x of lesser strength will also be in the cycle.
 */
bool CycleGraph::addMultiDriverDependencies(const FLNodeElabSet& flowSet)
{
  bool modified = false;

  // propagate dependencies for each flow node in the flowSet
  UtSet<std::pair<DependencyGraph::Node*,DependencyGraph::Node*> > coveredEdges;
  for (FLNodeElabSet::const_iterator f = flowSet.begin();
       f != flowSet.end(); ++f)
  {
    FLNodeElab* flow = *f;

    if (SCHUtil::isSequential(flow))
      continue; // only propagate combinational dependencies

    const NUNetElab* netElab = flow->getDefNet();
    const NUUseDefNode* useDef = flow->getUseDefNode();
    if (useDef == NULL || !netElab->isMultiplyDriven())
      continue; // there is nothing to do for this flow node

    /* For each flow node, we follow a 3-step process:
     *  1. Collect all brother drivers of lesser strength into the
     *     'brothers' set.
     *  2. Collect all dependencies for the node from the existing graph,
     *     excluding dependencies on our brothers.
     *  3. Add the dependencies to each brother in our 'brother' set.
     */

    Strength originalStrength = useDef->getStrength();
        
    // find all brother drivers of  lesser strength
    NUNetRefHdl netRef = flow->getFLNode()->getDefNetRef();
    UtSet<FLNodeElab*> brothers;
    for (NUNetElab::ConstDriverLoop loop = netElab->loopContinuousDrivers(); !loop.atEnd(); ++loop)
    {
      FLNodeElab* driver = *loop;
      if (driver == flow)
        continue; // skip ourselves
      if (!includeFlow(driver))
        continue; // can't be in a loop
      if (SCHUtil::isSequential(driver))
        continue; // only propagate to combinational drivers
      NUUseDefNode* bud = driver->getUseDefNode();
      if (bud == NULL)
        continue; // exclude NULL use def node
      Strength brotherStrength = bud->getStrength();
      if (brotherStrength >= originalStrength)
        continue; // not lesser strength
      NUNetRefHdl driverNetRef = driver->getFLNode()->getDefNetRef();
      if (!netRef->overlaps(*driverNetRef,false))
        continue; // the driver doesn't affect the same bits
      brothers.insert(driver);
    }

    // get the dependencies for this flow node
    UtList<GraphNode*> deps;
    NodeMap::iterator n = mNodeMap.find(flow);
    FLN_ELAB_ASSERT(n != mNodeMap.end(),flow);
    GraphNode* node = n->second;
    for (Iter<GraphEdge*> iter = mGraph->edges(node); !iter.atEnd(); ++iter)
    {
      GraphEdge* edge = *iter;
      GraphNode* to = mGraph->endPointOf(edge);
      // don't include dependencies on our brothers
      DependencyGraph::Node* dgn = mGraph->castNode(to);
      FLNodeElab* fn = dgn->getData();
      if (brothers.find(fn) == brothers.end())
        deps.push_front(to);
    }
        
    // propagate dependencies to all brother drivers of lower strength
    for (UtSet<FLNodeElab*>::iterator l = brothers.begin(); l != brothers.end(); ++l)
    {
      FLNodeElab* driver = *l;
      
      n = mNodeMap.find(driver);
      FLN_ELAB_ASSERT(n != mNodeMap.end(),driver);
      DependencyGraph::Node* brother = n->second;

      // propagate dependencies
      for (UtList<GraphNode*>::iterator t = deps.begin(); t != deps.end(); ++t)
      {
        DependencyGraph::Node* to = mGraph->castNode(*t);
        if (coveredEdges.find(std::make_pair(brother,to)) != coveredEdges.end())
          continue; // no duplicate edges
        coveredEdges.insert(std::make_pair(brother,to));
        DependencyGraph::Edge* edge = new DependencyGraph::Edge(to,BDD());
        mGraph->setFlags(edge, eBrotherDependency);
        brother->addEdge(edge);
        modified = true;
      }
    }
  }

  if (modified && mWriteDotFiles)
    writeDependencyGraph(mGraph, "dependencies_md.dot");

  return modified;
}


/*
 * These functions are used to write DependencyGraphs in dot format.
 * This is very useful for debugging small testcases, but I wouldn't
 * recommend using them on customer designs or any testcase more than
 * 100 lines long.
 */

//! write a dependency graph to a given file in dot format
void CycleGraph::writeDependencyGraph(DependencyGraph* g, const char* filename, BDDContext* context)
{
  CycleGraphDotWriter cgdw(filename, context);
  cgdw.output(g);
}

//! label each node with its elaborated name
void CycleGraphDotWriter::writeLabel(Graph* g, GraphNode* node)
{
  DependencyGraph* fg = dynamic_cast<DependencyGraph*>(g);
  DependencyGraph::Node* n = fg->castNode(node);
  FLNodeElab* flow = n->getData();
  UtString buf;
  flow->getName()->compose(&buf, true, true);
  *mOut << "[ label=\"" << buf << " (" << flow << ")\", ";
  switch (flow->getType())
  {
    case eFLBoundHierRef:
      *mOut << "style=dashed, color=blue";
      break;
    case eFLBoundPort:
      *mOut << "style=dashed, color=red";
      break;
    case eFLBoundProtect:
      *mOut << "style=dashed, color=green";
      break;
    case eFLBoundUndriven:
      *mOut << "style=dashed, color=yellow";
      break;
    case eFLCycle:
      *mOut << "style=solid, color=gray75";
      break;
    case eFLNormal:
    default:
      *mOut << "style=solid, color=black";
  }
  *mOut << " ]";
}

//! set the edge attributes based on the edge type
void CycleGraphDotWriter::writeLabel(Graph* g, GraphEdge* edge)
{
  if (g->anyFlagSet(edge, eFlowDependency))
  {
    *mOut << "[ style=solid";
  }
  else if (g->anyFlagSet(edge, eStrengthDependency))
  {
    *mOut << "[ style=dotted";
  }
  else if (g->anyFlagSet(edge, eBrotherDependency))
  {
    *mOut << "[ style=dashed";
  }
  else
  {
    *mOut << "[ style=solid";
  }

  if (mBDDContext)
  {
    DependencyGraph::Edge* dge = dynamic_cast<DependencyGraph::Edge*>(edge);
    INFO_ASSERT(dge, "Incompatible graph type for writer");
    BDD cond = dge->getData();
    if (cond.isValid() && cond != mBDDContext->val1())
    {
      UtString bddString;
      cond.compose(&bddString,true);
      *mOut << ", label=\"" << bddString << "\"";
    }
  }

  if (g->anyFlagSet(edge, eCyclic))
    *mOut << ", color=red ]";
  else
    *mOut << ", color=black ]";
}

PrintCycleBdds::Command
PrintCycleBdds::visitNodeBefore(Graph*, GraphNode* graphNode)
{
  // Get the elaborated flow for this node
  DependencyGraph::Node* node = mGraph->castNode(graphNode);
  FLNodeElab* flowElab = node->getData();
  mBddIndex = 0;

  // Gather the information for this elaborated flow
  UtString buf;
  NUNetElab* netElab = flowElab->getDefNet();
  netElab->compose(&buf,NULL);
  buf << " ";
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  if (useDef) {
    buf << useDef->typeStr() << " @";
    useDef->getLoc().compose(&buf);
  }

  // Print the info
  UtIO::cout() << "Edges for " << buf << ":\n";
  return GW_CONTINUE;
}

PrintCycleBdds::Command PrintCycleBdds::visitEdge(GraphEdge* graphEdge)
{
  // Get the BDD for this edge
  DependencyGraph::Edge* edge = mGraph->castEdge(graphEdge);
  BDD bdd = edge->getData();

  // Print the BDD. Don't visit the target of the edge, we visit nodes
  // from the start from only.
  printNetElabBdd(bdd, 0, mBddIndex);
  return GW_SKIP;
}

void PrintCycleBdds::printIndent(int indent)
{
  for (int i = 0; i < indent; ++i) {
    UtIO::cout() << " ";
  }
}

void PrintCycleBdds::printNetElabBdd(BDD bdd, int indent, int index)
{
  // print the indentation
  printIndent(indent);

  // If the index is >= 0 then this is the start, print the
  // label.
  if (index >= 0) {
    UtIO::cout() << " " << UtIO::Width(2) << index << ": ";
    indent += 5;
  }

  // Check for terminals
  if (bdd == mBDDContext->val0()) {
    UtIO::cout() << "0\n";
    return;
  }
  if (bdd == mBDDContext->val1()) {
    UtIO::cout() << "1\n";
    return;
  }

  // Make sure it isn't an invalid BDD
  INFO_ASSERT(bdd != mBDDContext->invalid(),
              "Found an invalid BDD while dumping the cycle graph BDDs!");

  // Get the elaborated net for this if statement
  BDD var = mBDDContext->topVariable(bdd);
  NUNetElab* netElab;
  SInt32 bitIndex;
  mBDDContext->decodeVariable(var, &netElab, &bitIndex);

  // It must be an if, print it
  UtString buf;
  netElab->getSymNode()->verilogCompose(&buf);
  UtIO::cout() << "If " << buf;
  if (netElab->getNet()->getBitSize() > 1) {
    UtIO::cout() << "[" << bitIndex << "]\n";
  } else {
    UtIO::cout() << "\n";
  }

  // Print the then condition
  printNetElabBdd(mBDDContext->thenVariable(bdd), indent+2, -1);

  // Print the else condition
  printIndent(indent);
  UtIO::cout() << "Else\n";
  printNetElabBdd(mBDDContext->elseVariable(bdd), indent+2, -1);
} // void PrintCycleBdds::printNetElabBdd
