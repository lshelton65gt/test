// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file

  This file contains routines to find real and false combinational
  cycles. It prints a warning to the user and uses the code in
  CycleBreaker.cxx to break up the cycles.
*/

#include "util/CbuildMsgContext.h"
#include "util/LoopSorted.h"
#include "util/UtIOStream.h"
#include "util/OSWrapper.h"
#include "util/ArgProc.h"
#include "util/Stats.h"
#include "util/UtOrder.h"       // For carbonPtrCompare()
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUCycle.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "symtab/STAliasedLeafNode.h"
#include "compiler_driver/CarbonContext.h"

#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"
#include "flow/FlowClassifier.h"
#include "flow/FLElabUseCache.h"

#include "localflow/UD.h"
#include "localflow/UnelabFlow.h"
#include "localflow/Elaborate.h"
#include "localflow/TFRewrite.h"
#include "reduce/AllocAlias.h"
#include "reduce/Fold.h"
#include "reduce/Inference.h"
#include "reduce/CodeMotion.h"
#include "reduce/DeadNets.h"
#include "reduce/RemoveDeadCode.h"
#include "reduce/RemoveElabFlow.h"
#include "reduce/RemoveUnelabFlow.h"
#include "reduce/CleanElabFlow.h"
#include "reduce/SanityUnelab.h"
#include "reduce/SanityElab.h"
#include "reduce/LocalConstProp.h"
#include "reduce/LocalCopyProp.h"
#include "schedule/Schedule.h"

#include "util/GraphSCC.h"
#include "util/GraphBuilder.h"

#include "Util.h"
#include "BlockSplit.h"
#include "CycleDetection.h"
#include "Depth.h"
#include "MarkDesign.h"
#include "CycleGraph.h"
#include "CycleDumper.h"
#include "CycleBreaker.h"

// Set this to true to enable dumping of pre- and post- split blocks
#define DEBUG_SPLIT 0

// Set this to true to enable dumping of stmt-level fanin / fanout data
#define DEBUG_SPLIT_STMT_PARTITION 0

//! Type used to track replacement net to original parts.
typedef UtMap<NUNet*,NUNetRefHdl,NUNetCmp> NUNetToNUNetRefMap;

//! Type used to track statements that def net regions
typedef NUNetRefMultiMap<NUStmt*> NetRefStmtMultiMap;

// Helper function must be defined to use NUNetRefMultiMap
template<>
void NetRefStmtMultiMap::helperTPrint(NUStmt* const & stmt, int indent) const
{
  stmt->printVerilog(true,indent,true);
}


//! Comparison operation for stable NUUseDefNode ordering with def net fallback
bool UDNodeCmp::operator()(const NUUseDefNode* ud1, const NUUseDefNode* ud2) const
{
  int cmp = 0;
  
  if (ud1 == ud2)
    return false;     // same pointer
  else if (!ud1)  
    cmp = -1;    // non-null vs. null
  else if (!ud2)
    cmp = +1;    // null vs. non-null
  
  // try source locator
  if (cmp == 0)
    cmp = SourceLocator::compare(ud1->getLoc(), ud2->getLoc());
  
  // try node type
  if (cmp == 0)
    cmp = ((int) ud1->getType()) - ((int) ud2->getType());          
  
  // fall back to def comparison - only as a last resort
  if (cmp == 0)
  {
    NUNetSet ud1_net_defs;
    NUNetSet ud2_net_defs;
  
    if (ud1->useBlockingMethods())
    {
      const NUUseDefStmtNode* stmt = dynamic_cast<const NUUseDefStmtNode*>(ud1);
      stmt->getBlockingDefs(&ud1_net_defs);
      stmt->getNonBlockingDefs(&ud1_net_defs);
    }
    else
    {
      ud1->getDefs(&ud1_net_defs);
    }

    if (ud2->useBlockingMethods())
    {
      const NUUseDefStmtNode* stmt = dynamic_cast<const NUUseDefStmtNode*>(ud2);
      stmt->getBlockingDefs(&ud2_net_defs);
      stmt->getNonBlockingDefs(&ud2_net_defs);
    }
    else
    {
      ud2->getDefs(&ud2_net_defs);
    }

    cmp = ud1_net_defs.size() - ud2_net_defs.size();
    if (cmp == 0) {
      // sort nets so we don't rely on ptr order.
      NUNetOrderedSet ud1_ordered_defs;
      NUNetOrderedSet ud2_ordered_defs;
      ud1_ordered_defs.insert(ud1_net_defs.begin(),ud1_net_defs.end());
      ud2_ordered_defs.insert(ud2_net_defs.begin(),ud2_net_defs.end());

      for (NUNetOrderedSet::const_iterator iter1 = ud1_ordered_defs.begin(), iter2 = ud2_ordered_defs.begin();
           (cmp==0) and iter1 != ud1_ordered_defs.end() and iter2 != ud2_ordered_defs.end();
           ++iter1, ++iter2) {
        const NUNet * ud1_def = (*iter1);
        const NUNet * ud2_def = (*iter2);

        cmp = NUNet::compare(ud1_def,ud2_def);
      }
    }
  }

  // last resort -- use pointer order
  if (cmp == 0)
    cmp = carbonPtrCompare<NUUseDefNode>(ud1,ud2);

  return (cmp < 0);
}

// used by the cycle sanity checker for cycle detection
#define IN_LOOP	(1<<0)

SCHCycleDetection::SCHCycleDetection(SCHMarkDesign* mark, SCHUtil* util,
                                     SCHScheduleData* scheduleData)
{
  mMarkDesign = mark;
  mUtil = util;
  mScheduleData = scheduleData;
  mSplitNodes = new FLNodeElabSet;
  mBlocks = new Blocks;
  mFlowToBlockMap = new FlowToBlockMap;
  mNumBlocks = 0;
  mCycleIndex = 0;
}

SCHCycleDetection::~SCHCycleDetection()
{
  delete mSplitNodes;
  INFO_ASSERT(mBlocks->empty(), "Ending cycle detection without freeing blocks");
  delete mBlocks;
  INFO_ASSERT(mFlowToBlockMap->empty(), "Ending cycle detection without freeing blocks");
  delete mFlowToBlockMap;
}

// used to order cycle graphs to improve stability
struct CycleGraphOrder
{
  bool operator()(DependencyGraph* g1, DependencyGraph* g2)
  {
    if (g1 == g2)
      return false;
    int cmp = g1->size() - g2->size();
    if (cmp == 0)
    {
      // order by the smallest flow node in the graph
      FLNodeElab* flow1 = smallestFlow(g1);
      FLNodeElab* flow2 = smallestFlow(g2);
      cmp = FLNodeElab::compare(flow1, flow2);
    }
    if (cmp == 0)
    {
      ptrdiff_t ptrDiff = carbonPtrCompare(g1, g2); // order by pointer as a last resort
      return (ptrDiff < 0);
    }
    return (cmp < 0);
  }
private:
  FLNodeElab* smallestFlow(DependencyGraph* graph)
  {
    FLNodeElab* smallest = NULL;
    for (Iter<GraphNode*> iter = graph->nodes(); !iter.atEnd(); ++iter)
    {
      GraphNode* node = *iter;
      DependencyGraph::Node* gdn = graph->castNode(node);
      FLNodeElab* flow = gdn->getData();
      if (smallest == NULL || (FLNodeElab::compare(smallest, flow) > 0))
        smallest = flow;
    }
    return smallest;
  }
};

//! Build the NUCycle structures for all cycles, and report cycle-related errors and warnings
/*! This routine encapsulates the flow cycle detection logic in
 *  cbuild.  It must be at least as pessimistic as all cycle-sensitive
 *  traversals later in the compiler, to avoid missing something that
 *  these traversals consider a cycle.
 *
 *  Cbuild treats flow cycles differently depending on whether or not
 *  they contain sequential nodes.  Cycles with a single sequential
 *  flow node are not treated as cycles!  They are handled by the
 *  scheduler.  Cycles with mutliple sequential nodes are treated as
 *  errors.  Purely combinational cycles are encapsulated as NUCycle
 *  structures.
 */
bool SCHCycleDetection::findDesignCycles()
{
  Stats* stats = mUtil->getStats();

  // create a sub-interval for cycle detection
  if (stats)
    stats->pushIntervalTimer();

  bool errors = false;

  // get all combinational cycles in the design
  SCHGraphList* cycles = findCombinationalCycles();

  if (stats) stats->printIntervalStatistics("CD First Pass");

  if (!cycles->empty())
  {
    // if the user requested it, print out the cyclic paths now
    if (mUtil->isFlagSet(eDumpCyclesEarly))
    {
      CycleDumper dumper(UtIO::cout());
      dumper.printKey();
      for (SCHGraphList::iterator g = cycles->begin(); g != cycles->end(); ++g)
      {
        DependencyGraph* graph = *g;
        dumper.dumpCycle(graph);
      }
    }

    // Inline any tasks that def/use non-local nets that are part of
    // always blocks in a cycle.
    bool tasksInlined = inlineCycleTasks(cycles);
    if (stats) stats->printIntervalStatistics("CD TaskInline");

    // Recompute the cycles now that tasks have been inlined
    if (tasksInlined) {
      freeCycleMemory(cycles);
      cycles = findCombinationalCycles();
      if (stats) stats->printIntervalStatistics("CD Task Pass");
    }

    // If there still are some cycles continue to the next pass
    if (!cycles->empty()) {
      // sort the cycle list (to improve stability of splitting)
      std::sort(cycles->begin(), cycles->end(), CycleGraphOrder());

      if (stats) stats->printIntervalStatistics("CD Sorting");

      // split cycle nodes out of the blocks they are in.  this can
      // change flow and invalidate everything we have computed before
      // it.
      if (mUtil->isFlagSet(eDumpSplitBlocks))
        UtIO::cout() << "Splitting combinational blocks due to cycles:\n";
      if (!splitCycleBlocks(cycles)) {
        if (stats) stats->popIntervalTimer();
        return true;
      }
      if (stats) stats->printIntervalStatistics("CD Splitting");

      // free pre-splitting cycle memory
      freeCycleMemory(cycles);

      // recompute the cycles based on the modified flow
      cycles = findCombinationalCycles();

      if (stats) stats->printIntervalStatistics("CD Last Pass");

      if (!cycles->empty())
      {
        // sort the cycle list (to improve stability of output messages)
        std::sort(cycles->begin(), cycles->end(), CycleGraphOrder());

        if (stats) stats->printIntervalStatistics("CD Sorting");

        // if the user requested it, print out the cyclic paths now
        if (mUtil->isFlagSet(eDumpCycles))
        {
          CycleDumper dumper(UtIO::cout());
          dumper.printKey();
          for (SCHGraphList::iterator g = cycles->begin(); g != cycles->end(); ++g)
          {
            DependencyGraph* graph = *g;
            dumper.dumpCycle(graph);
          }
        }

        // build an NUCycle structure and schedule the nodes in each combinational cycle.
        buildAndScheduleCycles(cycles);

        if (stats) stats->printIntervalStatistics("CD Scheduling");

        // print warnings and information about all cycles
        errors |= reportCycleData(cycles);
      }
    } // if
  }
  freeCycleMemory(cycles);

  // also suggest use of blastNet directives that could break the cycle
  suggestBlastNetDirectives();

  // finally, look for combinational flow nodes that are in their own fanin and print a warning
  warnAboutCombinationalSelfCycles();

  // finish the sub-interval for cycle detection
  if (stats)
    stats->popIntervalTimer();

  return errors;
}

void SCHCycleDetection::freeCycleMemory(SCHGraphList* cycles)
{
  for (SCHGraphList::iterator g = cycles->begin(); g != cycles->end(); ++g)
    delete *g;
  delete cycles;
}

/*!
 * Return a list of all cyclic subgraphs of the flow dependency graph
 * for the design.  The cycles in this graph will include all
 * non-sequential dependencies: combinational driver dependencies,
 * strength dependencies from drivers to weaker drivers of the same net,
 * and dependencies necessary to ensure that when one driver of a
 * multiply-driven net is in a cycle, all other drivers of the net of
 * equal or lower strength will also be in the cycle.
 */
SCHGraphList* SCHCycleDetection::findCombinationalCycles(bool writeDotFile)
{
  SCHGraphList* cycles = new SCHGraphList;
 
  // Extract a dependency graph from the design. This graph will have
  // combinational dependencies, strength dependencies, and clock
  // dependencies.
  CycleGraph cycleGraph(mUtil->getDesign(),
                        mUtil->getSymbolTable(),
                        mMarkDesign,
                        mUtil,
                        writeDotFile);
  DependencyGraph* graph = cycleGraph.getGraph();

  // Compute the strongly connected components of the dependency graph
  GraphSCC scc;
  scc.compute(graph);

  // If there were cycles, incorporate brother driver dependencies into the cycles
  if (scc.hasCycle())
  {
    // get the set of nodes in cycles
    FLNodeElabSet cycleNodes;
    for (GraphSCC::ComponentLoop loop = scc.loopCyclicComponents(); !loop.atEnd(); ++loop)
    {
      GraphSCC::Component* comp = *loop;
      for (GraphSCC::Component::NodeLoop iter = comp->loopNodes(); !iter.atEnd(); ++iter)
      {
        DependencyGraph::Node* node = graph->castNode(*iter);
        cycleNodes.insert(node->getData());
      }
    } 

    // add brother driver dependencies where necessary, and recompute the cycles
    if (cycleGraph.addMultiDriverDependencies(cycleNodes))
    {
      graph = cycleGraph.getGraph();
      scc.compute(graph);
      INFO_ASSERT(scc.hasCycle(), "Adding dependencies cannot remove a cycle");
    }
  }

  // if there are cycles, add each one to the list to return to the caller
  if (scc.hasCycle())
  {
    // add a graph extracted from each component onto the cycle list
    for (GraphSCC::ComponentLoop loop = scc.loopCyclicComponents(); !loop.atEnd(); ++loop)
    {
      GraphSCC::Component* comp = *loop;
      INFO_ASSERT(comp->isCyclic(), "Inconsistency in GraphSCC -- cyclic component set includes non-cyclic component");
      DependencyGraph* cycle = dynamic_cast<DependencyGraph*>(scc.extractComponent(comp));
      cycles->push_back(cycle);
    }
  }

  // return the list of cyclic subgraphs
  return cycles;
}

/*!
 * This function will split always blocks that contain potentially cyclic nodes.
 *
 * There are three reasons for this:
 *  1. Splitting non-cyclic logic out of the block will reduce the amount of
 *     logic that gets executed multiple times as the loop settles.
 *  2. Splitting cyclic defs from each other may allow a better ordering
 *     that does not multi-call blocks in the cycle.
 *  3. Splitting internal defs to the same net can eliminate false cycles
 *     due to flow aggregation.
 *
 * It performs the split by calling statement-level splitting routines,
 * as opposed to the net-based splitting routines used elsewhere in
 * cbuild.
 */
bool SCHCycleDetection::splitCycleBlocks(SCHGraphList* cycles)
{
  if (!mUtil->isFlagSet(eSplitBlocks))
    return true;

  INFO_ASSERT(mSplitNodes->empty(), "splitCycleBlocks called multiple times without clearing split node set");

  Stats* stats = mUtil->getStats();

  // create a sub-interval for splitting.
  if (stats) 
    stats->pushIntervalTimer();
    
  // Need to lookup all elaborated flow for a given always block
  SCHBlockFlowNodes blockFlowNodes(mUtil);

  // first, we need to figure out which nodes to split and how to split them
  ElabSplitMap elabSplitMap;
  {
    FLElabUseCache levelCache(mUtil->getNetRefFactory(), false);
    for (SCHGraphList::iterator g = cycles->begin(); g != cycles->end(); ++g)
    {
      DependencyGraph* cycle = *g;
      getBlockSplitsForCycle(cycle, &elabSplitMap, blockFlowNodes, &levelCache);
    }
    if (stats)
      stats->printIntervalStatistics("CDS Discovery");
  }

  // convert the elaborated splits into unelaborated splits
  SplitMap splitMap;
  convertElabSplits(elabSplitMap, &splitMap);
  if (stats)
    stats->printIntervalStatistics("CDS Unelab Convert");

  // then we split the nucleus for the targetted blocks
  UtSet<const NUAlwaysBlock*> oldAlwaysBlocks;
  ModuleBlockMap newModuleBlocks;
  performSplits(splitMap, &oldAlwaysBlocks, &newModuleBlocks);
  splitMap.clear();
  if (stats)
    stats->printIntervalStatistics("CDS Nucleus XForm");

  if (not newModuleBlocks.empty()) {
    // Recreate UD, flow and elaborated flow
    recreateDesignData(oldAlwaysBlocks, newModuleBlocks);
    if (stats)
      stats->printIntervalStatistics("CDS Recreate");

    oldAlwaysBlocks.clear();
  }

  if (stats) 
    stats->popIntervalTimer();

  return true;
}

void SCHCycleDetection::performSplits(SplitMap& splitMap,
                                      UtSet<const NUAlwaysBlock*>* oldAlwaysBlocks,
                                      ModuleBlockMap* newModuleBlocks)
{
  bool verboseUD = mUtil->getArgs()->getBoolValue(CRYPT("-verboseUD")); 
  UD ud (mUtil->getNetRefFactory(),
         mUtil->getMsgContext(),
         mUtil->getIODB(),
         mUtil->getArgs(),
         verboseUD);

  for (SplitMap::iterator l = splitMap.begin(); l != splitMap.end(); ++l)
  {
    const NUAlwaysBlock* always = l->first;
    PartitionVector& partition = l->second;
    NUAlwaysBlockList splitBlocks;
    for (UInt32 idx = 0; idx < partition.size(); ++idx)
    {
      OrderedStmtSet& stmts = partition[idx];
      NUAlwaysBlock* newBlock = makeSplitBlock(&ud,always,stmts);
      if (newBlock)
        splitBlocks.push_back(newBlock);
      else
        break;
    }
    
    // check if the split produced all good blocks
    bool splitSucceeded = (splitBlocks.size() == partition.size());
    if (splitSucceeded)
    {
      // remove the original block from its parent and add all the split blocks
      NUScope* parent = always->getBlock()->getParentScope();
      NUModule* module = dynamic_cast<NUModule*>(parent);
      NU_ASSERT(module,always);
      module->removeAlwaysBlock(const_cast<NUAlwaysBlock*>(always));
      
#if DEBUG_SPLIT
      UtIO::cout() << "Original block:\n";
      always->printVerilog(true,2,true);
      UtIO::cout() << "Was split into blocks:\n";
#endif

      for (NUAlwaysBlockList::iterator i = splitBlocks.begin(); i != splitBlocks.end(); ++i)
      {
        NUAlwaysBlock* splitBlock = *i;
        module->addAlwaysBlock(splitBlock);
        (*newModuleBlocks)[module].insert(splitBlock);
#if DEBUG_SPLIT
        splitBlock->printVerilog(true,2,true);
#endif
      }
      
      // schedule the original always block for deletion
      oldAlwaysBlocks->insert(always);
    }
    else
    {
      // the split was no good, so delete any new blocks created
      for (NUAlwaysBlockList::iterator p = splitBlocks.begin(); p != splitBlocks.end(); ++p)
      {
        NUAlwaysBlock* split = *p;
        delete split;
      }
    }

    // Provide block split info if the user requested it
    if (mUtil->isFlagSet(eDumpSplitBlocks))
    {
      if (splitSucceeded)
        UtIO::cout() << "  Successfully split:\n";
      else
        UtIO::cout() << "  Failed to split:\n";
      for (UInt32 idx = 0; idx < partition.size(); ++idx)
      {
        OrderedStmtSet& stmts = partition[idx];
        UtIO::cout() << "    Group " << (idx + 1) << ":\n";
        for (OrderedStmtSet::iterator s = stmts.begin(); s != stmts.end(); ++s)
        {
          NUStmt* stmt = *s;
          stmt->printVerilog(true,6,false);
        }        
      }      
    }
  }
}

void 
SCHCycleDetection::recreateDesignData(UtSet<const NUAlwaysBlock*>& oldAlwaysBlocks,
                                      ModuleBlockMap& newModuleBlocks)
{
  Stats* stats = mUtil->getStats();
  if (stats)
    stats->pushIntervalTimer();

  // Remove the existing flow (must be done before deleting always blocks, netElabs, etc.)
  removeFlow();
  if (stats)
    stats->printIntervalStatistics("CDS RemoveFlow");

  // Now delete existing always blocks and remove NUNetElabs for block-local nets
  deleteAlwaysBlocks(oldAlwaysBlocks);
  if (stats)
    stats->printIntervalStatistics("CDS DeleteOldAlways");

  // Perform optimizations and recompute UD bottom-up
  optimizeModifiedBlocks(newModuleBlocks);
  if (stats)
    stats->printIntervalStatistics("CDS Optimize");

  // Restore the unelab and elab flow for the entire design
  restoreFlow(newModuleBlocks);
  if (stats)
    stats->printIntervalStatistics("CDS RestoreFlow");

  // Do unelab and elab flow sanity checks
  checkFlowSanity();
  if (stats)
    stats->printIntervalStatistics("CDS SanityUnelab");

  if (stats)
    stats->popIntervalTimer();
} // void SCHCycleDetection::recreateDesignData

//! Remove the unelaborated and elaborated flow from the design
void SCHCycleDetection::removeFlow()
{
  RemoveUnelabFlow remove_unelab(mUtil->getFlowFactory());
  RemoveElabFlow remove_elab(mUtil->getFlowElabFactory(), mUtil->getSymbolTable());
  remove_unelab.remove();
  remove_elab.remove();
}

//! Recreate the unelab and elab flow, and elaborate local nets in new blocks
void SCHCycleDetection::restoreFlow(ModuleBlockMap& newModuleBlocks)
{
  Stats* stats = mUtil->getStats();
  if (stats)
    stats->pushIntervalTimer();

  bool verboseFlow    = mUtil->getArgs()->getBoolValue(CRYPT("-verboseFlow")); 
  bool verboseCleanup = mUtil->getArgs()->getBoolValue(CRYPT("-verboseCleanup")); 

  // Recompute unelab flow for the entire design -- UD should be accurate already
  UnelabFlow unelab_flow(mUtil->getFlowFactory(),
                         mUtil->getNetRefFactory(),
                         mUtil->getMsgContext(),
                         mUtil->getIODB(),
                         verboseFlow);
  unelab_flow.design(mUtil->getDesign());
  if (stats)
    stats->printIntervalStatistics("CDS UnelabFlow");

  // Recompute elaborated flow for the entire design
  Elaborate elab_flow(mUtil->getSymbolTable(),
                      mUtil->getSymbolTable()->getAtomicCache(),
                      mUtil->getSourceLocatorFactory(),
                      mUtil->getFlowFactory(),
                      mUtil->getFlowElabFactory(),
                      mUtil->getNetRefFactory(),
                      mUtil->getMsgContext(),
                      mUtil->getArgs(),
                      mUtil->getIODB());

  // Gather all hierarchies that may need new temp nets elaborated. We
  // can't do it in a single loop because we should not modify the
  // symbol table while iterating over it.
  UtVector<STBranchNode*> hiers;
  for (STSymbolTable::NodeLoop i = mUtil->getSymbolTable()->getNodeLoop();
       !i.atEnd(); ++i) {
    STSymbolTableNode* node = *i;
    STBranchNode* hier = node->castBranch();
    if (hier) {
      NUModule* module = NUModule::lookup(hier);
      if (module) {
        ModuleBlockMap::iterator pos = newModuleBlocks.find(module);
        if (pos != newModuleBlocks.end()) {
          hiers.push_back(hier);
        }
      }
    }
  }

  // elaborate declarations of the new blocks, which will probably
  // contain new temp nets
  for (UtVector<STBranchNode*>::iterator i = hiers.begin();
       i != hiers.end(); ++i) {
    STBranchNode* hier = *i;
    NUModule* module = NUModule::lookup(hier);
    ST_ASSERT(module != NULL, hier);
    UtMap<NUModule*,NUAlwaysBlockSet>::iterator pos = newModuleBlocks.find(module);
    ST_ASSERT(pos != newModuleBlocks.end(), hier);
    NUAlwaysBlockSet& newBlocks = pos->second;

    // Note: newly created blocks may be deleted because they were dead, so
    // rely on the block lists in the module, and only use the newBlocks set
    // for confirmation that an existing block is a newly created one.
    for (NUModule::AlwaysBlockLoop l = module->loopAlwaysBlocks(); !l.atEnd(); ++l)
    {
      NUAlwaysBlock* always = *l;
      if (newBlocks.find(always) != newBlocks.end()) {
        NUBlock* block = always->getBlock();
        elab_flow.elabScopeDecl(hier, block);
      }
    }
  }
  if (stats)
    stats->printIntervalStatistics("CDS ElabDeclarations");

  // We have to cleanup extra flow that can be created for
  // non-continuous drivers whose nested flow is used but whose
  // higher-level flow is not.
  RemoveDeadCode dead_code(mUtil->getNetRefFactory(),
                           mUtil->getFlowFactory(),
                           mUtil->getIODB(),
                           mUtil->getArgs(),
                           mUtil->getMsgContext(),
                           false,  // do not start at local nets
                           true,   // delete dead code
                           false,  // do not delete module instances
                           verboseCleanup);
  DeadNetCallback dead_net_callback(mUtil->getSymbolTable());
  dead_code.design(mUtil->getDesign(), &dead_net_callback);
  if (stats)
    stats->printIntervalStatistics("CDS RemoveDeadCode");

  // elaborate the flow for the entire design
  elab_flow.designFlow(mUtil->getDesign(), false); /* false == do not elaborate port flow */
  if (stats)
    stats->printIntervalStatistics("CDS ElabFlow");

  CleanElabFlow cleanup_elab(mUtil->getFlowElabFactory(),
                             mUtil->getSymbolTable(),
                             verboseCleanup);
  cleanup_elab.design(mUtil->getDesign(),NULL,true);
  if (stats)
    stats->printIntervalStatistics("CDS CleanElab");

  // Invalidate any cached flow information
  mMarkDesign->clearTrueFaninCaches();
  mMarkDesign->clearPorts();

  if (stats)
    stats->popIntervalTimer();
}

//! Perform unelab and elab sanity passes
void SCHCycleDetection::checkFlowSanity()
{
  Stats* stats = mUtil->getStats();
  if (stats)
    stats->pushIntervalTimer();

  bool verboseSanity = mUtil->getArgs()->getBoolValue(CRYPT("-verboseSanity"));

  // Do unelaborated sanity check.
  SanityUnelab unelab_sanity(mUtil->getMsgContext(), mUtil->getNetRefFactory(),
                             mUtil->getFlowFactory(), true, verboseSanity);
  unelab_sanity.design(mUtil->getDesign());
  if (stats)
    stats->printIntervalStatistics("CDS SanityUnelab");

  // Do elaborated sanity check.
  SanityElab elab_sanity(mUtil->getMsgContext(), mUtil->getSymbolTable(),
                         mUtil->getNetRefFactory(),
                         mUtil->getFlowFactory(), mUtil->getFlowElabFactory(),
                         true, verboseSanity);
  elab_sanity.design(mUtil->getDesign());
  if (stats)
    stats->printIntervalStatistics("CDS SanityElab");

  if (stats)
    stats->popIntervalTimer();
}

//! Delete the always blocks in the given set and remove their block-local nets
void SCHCycleDetection::deleteAlwaysBlocks(UtSet<const NUAlwaysBlock*>& toBeDeleted)
{
  NUNetList deletedLocalNets;
  for (UtSet<const NUAlwaysBlock*>::iterator p = toBeDeleted.begin();
       p != toBeDeleted.end(); ++p)
  {
    const NUAlwaysBlock* always = *p;
    always->getBlock()->getAllNets(&deletedLocalNets);
    delete always;
  }
  DeadNetCallback dead_nets(mUtil->getSymbolTable());
  dead_nets(&deletedLocalNets);
  deletedLocalNets.clear();
}


void SCHCycleDetection::calculateUnelaboratedFlow(NUModule * module)
{
  bool verboseFlow     = mUtil->getArgs()->getBoolValue(CRYPT("-verboseFlow")); 
  UnelabFlow unelab_flow(mUtil->getFlowFactory(),
                         mUtil->getNetRefFactory(),
                         mUtil->getMsgContext(),
                         mUtil->getIODB(),
                         verboseFlow);
  unelab_flow.module(module, false);
}


//! Optimize the modified blocks -- dead code, inference, and code motion and leave UD correct
void SCHCycleDetection::optimizeModifiedBlocks(ModuleBlockMap& newModuleBlocks)
{
  bool do_copy_prop   = (not mUtil->getArgs()->getBoolValue(CRYPT("-noCopyPropagation")));
  bool do_const_prop  = true;
  bool do_code_motion = (not mUtil->getArgs()->getBoolValue(CRYPT("-noCodeMotion")));

  bool verboseCleanup    = mUtil->getArgs()->getBoolValue(CRYPT("-verboseCleanup")); 
  bool verboseUD         = mUtil->getArgs()->getBoolValue(CRYPT("-verboseUD")); 
  bool verboseFold       = mUtil->getArgs()->getBoolValue(CRYPT("-verboseFold")); 
  bool verboseCodeMotion = mUtil->getArgs()->getBoolValue(CRYPT("-verboseCodeMotion")); 

  bool verbose_copy_prop  = mUtil->getArgs()->getBoolValue(CRYPT("-verboseCopyPropagation"));
  SInt32 verboseConstProp = 0;
  mUtil->getArgs()->getIntLast(CRYPT("-verboseConstantPropagation"), &verboseConstProp);
  bool verbose_const_prop = verboseConstProp >= 2;
  bool verbose_prop       = mUtil->getArgs()->getBoolValue(CRYPT("-verboseLocalPropagation"));

  NUModuleList orderedModules;
  mUtil->getDesign()->getModulesBottomUp(&orderedModules);
  
  UD ud (mUtil->getNetRefFactory(),
         mUtil->getMsgContext(),
         mUtil->getIODB(),
         mUtil->getArgs(),
         verboseUD);
  RemoveUnelabFlow remove_unelab(mUtil->getFlowFactory());
  RemoveDeadCode dead_code(mUtil->getNetRefFactory(),
                           mUtil->getFlowFactory(),
                           mUtil->getIODB(),
                           mUtil->getArgs(),
                           mUtil->getMsgContext(),
                           false,  // do not start at local nets
                           true,   // delete dead code
                           false,  // do not delete module instances
                           verboseCleanup);
  DeadNetCallback dead_net_callback(mUtil->getSymbolTable());
  Fold fold(mUtil->getNetRefFactory(),
            mUtil->getArgs(),
            mUtil->getMsgContext(),
            mUtil->getSymbolTable()->getAtomicCache(),
            mUtil->getIODB(),
            verboseFold,
            eFoldUDKiller);
  InferenceStatistics inference_stats;
  Inference::Params params (mUtil->getArgs (), mUtil->getMsgContext ());
  Inference inference(&fold, 
    mUtil->getNetRefFactory(), 
    mUtil->getMsgContext(), 
    mUtil->getSymbolTable()->getAtomicCache(),
    mUtil->getIODB(), 
    mUtil->getArgs (),
    params, &inference_stats);

  ElabAllocAlias alloc_alias(AllocAlias::eMatchedPortSizes);
  alloc_alias.compute(mUtil->getDesign(), mUtil->getSymbolTable());

  SInt32 expr_motion_threshold = CodeMotion::cDefExprMotionThreshold;
  mUtil->getArgs()->getIntLast(CRYPT("-expressionMotionThreshold"), &expr_motion_threshold);

  CodeMotionStatistics code_motion_stats;
  CodeMotion code_motion(mUtil->getSymbolTable()->getAtomicCache(),
                         mUtil->getNetRefFactory(),
                         mUtil->getMsgContext(),
                         mUtil->getIODB(),
                         mUtil->getArgs(),
                         &alloc_alias,
                         &fold,
                         mUtil->getArgs()->getBoolValue(CRYPT("-expressionMotion")),
                         expr_motion_threshold,
                         verboseCodeMotion,
                         &code_motion_stats);
  LocalPropagationStatistics const_prop_statistics;
  RELocalConstProp const_prop(mUtil->getIODB(), &ud, &fold, 
                              &const_prop_statistics, 
                              verbose_const_prop,
                              false); // do not use flow order
  LocalPropagationStatistics copy_prop_statistics;
  RELocalCopyProp copy_prop(mUtil->getIODB(), &ud, &fold, 
                            &copy_prop_statistics,
                            verbose_copy_prop);

  for (NUModuleList::iterator m = orderedModules.begin(); m != orderedModules.end(); ++m)
  {
    NUModule* module = *m;
    ud.module(module);  // recompute UD

    // if this is a modified module, perform optimizations, keeping UD accurate for each one
    if (newModuleBlocks.find(module) != newModuleBlocks.end())
    {
      // WARNING: dead code removal on a module only works when the flow factory
      // only contains unelab flow for that module.  Any other unelab flow
      // not reachable from the module will cause its Nucleus to be deleted!
      // Therefore, we must create flow for the module, perform the dead code pass
      // and then remove the unelab flow immediately afterward.
      INFO_ASSERT(mUtil->getFlowFactory()->numAllocated() == 0,
                  "Module-level dead code removal attempted with module-external flow");
      calculateUnelaboratedFlow(module);
      dead_code.module(module, &dead_net_callback);
      remove_unelab.remove();
      
      // when nets are deleted, the fold object's expr factory must be cleared
      fold.clearExprFactory(); 
      
      ud.module(module); 
      inference.module(module);   // inference does not fix up UD
      ud.module(module); 

      if (do_code_motion) {
        code_motion.module(module); // code motion fixes up UD
      }

      if (do_const_prop) { 
        calculateUnelaboratedFlow(module);
        const_prop.module(module);
        remove_unelab.remove();
      }

      if (do_copy_prop)  { 
        calculateUnelaboratedFlow(module);
        copy_prop.module(module);
        remove_unelab.remove();
      }
    }
  }

  if (do_const_prop and verbose_prop) {
    const_prop_statistics.print("Local Constant Propagation");
  }

  if (do_copy_prop and verbose_prop) {
    copy_prop_statistics.print("Local Copy Propagation");
  }
}

/*!
 * Given a cyclic subgraph, determine which nodes need to be split and
 * how they should be partitioned.
 *
 * Every combinational always block is split such that non-cyclic nets
 * go into one partition class and each cyclic net goes into its own
 * partition class.
 *
 * Some combinational always blocks are split more finely.  They may
 * have separate defs of the same net split into different partition
 * classes to try to eliminate a false cycle.  But the split should
 * not create a new continuous driver.
 */
void
SCHCycleDetection::getBlockSplitsForCycle(DependencyGraph* cycle,
                                          ElabSplitMap* elabSplitMap,
                                          const SCHBlockFlowNodes& blockFlowNodes,
                                          FLElabUseCache* levelCache)
{
  // To choose the right splits, we need leaf-level fanin and fanout
  // information which accounts for control dependencies.  We need the
  // information in terms of continuous drivers, since we only want to
  // perform splits that do not create new non-continuous drivers.
  FLNodeElabSet continuousFlows;
  FLNodeElabSet internalFlows;
  getCyclicFlows(cycle, &continuousFlows, &internalFlows);

  FlowToFlowSetMap fanins;
  getLeafLevelContinuousFanin(continuousFlows,internalFlows,&fanins, levelCache);
  internalFlows.clear();

  // convert fanin data from flow-level to statement level and build fanout map
  StmtToStmtSetMap stmtFanins;
  StmtToStmtSetMap stmtFanouts;
  for (FlowToFlowSetMap::iterator i = fanins.begin(); i != fanins.end(); ++i)
  {
    FLNodeElab*   flow = i->first;
    FLNodeElabSet& faninSet = i->second;

    NUUseDefNode* def = flow->getUseDefNode();
    NUStmt* defStmt = dynamic_cast<NUStmt*>(def);
    NU_ASSERT(defStmt,def);
    for (FLNodeElabSet::iterator l = faninSet.begin(); l != faninSet.end(); ++l)
    {
      FLNodeElab* fanin = *l;
      NUUseDefNode* use = fanin->getUseDefNode();
      NUStmt* useStmt = dynamic_cast<NUStmt*>(use);
      NU_ASSERT(useStmt,use);
      stmtFanins[defStmt].insert(useStmt);
      stmtFanouts[useStmt].insert(defStmt);
    }
  }
  fanins.clear();
  
  // Partition the leaf-level flows in each block into one partition class for
  // non-cyclic flows and one partition class for each cyclic flow with the same
  // def net and either the same fanin set or the same fanout set.
  OrderedAlwaysBlockFlowMap alwaysBlocks;
  for (FLNodeElabSet::iterator l = continuousFlows.begin(); l != continuousFlows.end(); ++l)
  {
    FLNodeElab* flow = *l;
    NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(flow->getUseDefNode());
    if (!always)
      continue;
    alwaysBlocks[always] = flow;
  }

  for (OrderedAlwaysBlockFlowMap::iterator l = alwaysBlocks.begin(); l != alwaysBlocks.end(); ++l)
  {
    NUAlwaysBlock* always = l->first;
    FLNodeElab* flow = l->second;

    // qualify the block as splittable, or else skip it
    if (!qualifyBlockForSplitting(always))
      continue;

    // Go through each leaf-level flow in the block, building up partition
    // classes based on common fanin and fanout sets.

    // gather all leaf-level statements in a sorted vector
    FLNodeElabVector nodes;
    NUStmtVector sortedLeafStmts;
    NUStmtSet allContinuousStmts;
    blockFlowNodes.getFlows(flow, &nodes, &FLNodeElab::isReachable);
    for (FLNodeElabVectorCLoop f(nodes); !f.atEnd(); ++f) {
      FLNodeElab* blockFlow = *f;
      FLNodeElabIter* iter = blockFlow->makeFaninIter(FLIterFlags::eAll,
                                                      FLIterFlags::eLeafNode,
                                                      FLIterFlags::eIntoAll,
                                                      true);
      while (!iter->atEnd())
      {
        FLNodeElab* nested = iter->getCur(); 
        NUStmt* stmt = dynamic_cast<NUStmt*>(nested->getUseDefNode());
        if (stmt)
          allContinuousStmts.insert(stmt);

        if (stmt && isLeaf(nested))
          sortedLeafStmts.push_back(stmt);
        iter->next();
      }
      delete iter;
    }
    std::sort(sortedLeafStmts.begin(), sortedLeafStmts.end(), UDNodeCmp());

    // Go through the leaf-level statements in sorted order building the partition.
    // Sorting the statements is important because the UnionFind representative
    // calculation is order-sensitive.
    StmtSetToStmtMap faninPartitionMap;
    StmtSetToStmtMap fanoutPartitionMap;        
    NUStmt* nonCyclicStmt = NULL;
    StmtPartition partition;
    for (NUStmtVector::iterator s = sortedLeafStmts.begin(); s != sortedLeafStmts.end(); ++s)
    {
      NUStmt* stmt = *s;

      // This is a leaf, add it to the correct partition class
      StmtToStmtSetMap::iterator pin = stmtFanins.find(stmt);
      StmtToStmtSetMap::iterator pout = stmtFanouts.find(stmt);

#if DEBUG_SPLIT_STMT_PARTITION
      UtIO::cout() << "Statement: ";
      stmt->printVerilog(true,0,true);
      UtIO::cout() << "Fanin set:\n";
      if (pin != stmtFanins.end())
      {
        OrderedStmtSet& faninSet = pin->second;
        for (OrderedStmtSet::iterator q = faninSet.begin(); q != faninSet.end(); ++q)
          (*q)->printVerilog(true,4,true);
      }
      UtIO::cout() << "Fanout set:\n";
      if (pout != stmtFanouts.end())
      {
        OrderedStmtSet& fanoutSet = pout->second;
        for (OrderedStmtSet::iterator q = fanoutSet.begin(); q != fanoutSet.end(); ++q)
          (*q)->printVerilog(true,4,true);
      }
#endif

      if (pin == stmtFanins.end() && pout == stmtFanouts.end())
      {
        // not a cyclic stmt -- add to non-cycle partition class
        if (nonCyclicStmt == NULL)
        {
          nonCyclicStmt = stmt;
          partition.add(stmt);
        }
        else
        {
          partition.equate(stmt, nonCyclicStmt);
        }
      }
      else
      {
        // this is a cyclic stmt -- build up equivalence classes
        NUStmt* equivFanin = NULL;
        NUStmt* equivFanout = NULL;
        
        // find other stmts in the block with the same fanin or fanout set
        if (pin != stmtFanins.end())
        {
          StmtSetToStmtMap::iterator p = faninPartitionMap.find(pin->second);
          if (p != faninPartitionMap.end())
            equivFanin = p->second;
          else
            faninPartitionMap[pin->second] = stmt;
        }
        if (pout != stmtFanouts.end())
        {
          StmtSetToStmtMap::iterator p = fanoutPartitionMap.find(pout->second);
          if (p != fanoutPartitionMap.end())
            equivFanout = p->second;
          else
            fanoutPartitionMap[pout->second] = stmt;
        }
        
        // if no equivalent stmts, create a new partition class, otherwise
        // equate existing partition classes
        if (!equivFanin && !equivFanout)
          partition.add(stmt);
        else
        {
          if (equivFanin)
            partition.equate(stmt,equivFanin);
          if (equivFanout)
            partition.equate(stmt,equivFanout);
        }
      }
    }    
    faninPartitionMap.clear();
    fanoutPartitionMap.clear();

    // Statements with overlapping continuous defs which are not in exclusive 
    // legs of a control statement must be in the same partition class due to 
    // def-after-def ordering constraints.
    processOverlappingDefs(always, &allContinuousStmts, &partition);
    allContinuousStmts.clear();

    // Translate partition into elabSplitMap
    StmtToStmtSetMap* part = partition.getPartition();
    if (part->size() > 1)
    {
      SCHUseDefHierPair udhp(flow);
      PartitionVector& partitionVec = (*elabSplitMap)[udhp];
      partitionVec.resize(part->size());
      UInt32 idx = 0;
      for (StmtToStmtSetMap::iterator i = part->begin(); i != part->end(); ++i)
      {
        OrderedStmtSet& partitionStmts = i->second;
        partitionVec[idx].insert(partitionStmts.begin(), partitionStmts.end());
        ++idx;
      }
    }
    delete part;
  }
}

/* Here is some information about nested flow that is useful for
 * understanding the design of this code.  If a block has defs of a
 * net which is killed later in the block, the nested flow for those
 * defs will not be part of the nested flow tree of the continuous
 * drivers of the net.  The only way to reach those nested flows is by
 * a fanin + nested flow walk from continuous drivers of the always
 * block (not necessarily for the same net).
 *
 * The scheduler deals only with top-level flow nodes, so those
 * non-continuous nested flows cannot be used as scheduling elements.
 * Also, they cannot be used in splitting definitions, because
 * separating them from the killing defs would create new continuous
 * drivers.  Instead, we want to ignore them and look at their fanin.
 * This requires being able to distinguish a nested flow which is part
 * of the continuous driving nested flow tree and one which is not,
 * which is not possible by looking at the flow node itself.
 */
void SCHCycleDetection::getCyclicFlows(DependencyGraph* cycle,
                                       FLNodeElabSet* continuousFlows,
                                       FLNodeElabSet* internalFlows)
{
  // The cycle will not contain internal flows (by the nature of loopTrueFanin())
  for (Iter<GraphNode*> graph_iter = cycle->nodes(); !graph_iter.atEnd(); ++graph_iter)
  {
    GraphNode* gn = *graph_iter;
    DependencyGraph::Node* node = cycle->castNode(gn);
    FLNodeElab* flow = node->getData();
    FLN_ELAB_ASSERT(flow->isReachable(),flow); // this is a live, top-level flow node

    // add all flows in this nesting tree as continuous flows
    FLNodeElabIter* iter = flow->makeFaninIter(FLIterFlags::eAll,
                                               FLIterFlags::eLeafNode,
                                               FLIterFlags::eIntoAll,
                                               true);
    while (!iter->atEnd())
    {
      FLNodeElab* nested = iter->getCur();
      continuousFlows->insert(nested);
      iter->next();
    }
    delete iter;
  }

  // Look for internal flows as fanin of continuous flows
  FLNodeElabSet toCheck;
  FLNodeElabSet alreadyChecked;
  toCheck.insert(continuousFlows->begin(), continuousFlows->end());
  while (!toCheck.empty())
  {
    FLNodeElabSet::iterator pos = toCheck.begin();
    FLNodeElab* flow = *pos;
    toCheck.erase(pos);

    if (alreadyChecked.find(flow) != alreadyChecked.end())
      continue;
    alreadyChecked.insert(flow);

    for (FLNodeElabLoop l = flow->loopFanin(); !l.atEnd(); ++l)
    {
      FLNodeElab* fanin = *l;
      NUUseDefNode* useDef = fanin->getUseDefNode();
      if (useDef == NULL || useDef->isContDriver())
        continue; // not an internal fanin
      if (continuousFlows->find(fanin) != continuousFlows->end())
        continue; // known cyclic fanin 
      if (internalFlows->find(fanin) != internalFlows->end())
        continue; // entire nesting tree will already be processed

      // add all flows in this nesting tree as internal flows
      FLNodeElabIter* iter = fanin->makeFaninIter(FLIterFlags::eAll,
                                                  FLIterFlags::eLeafNode,
                                                  FLIterFlags::eIntoAll,
                                                  true);
      while (!iter->atEnd())
      {
        FLNodeElab* nested = iter->getCur();
        internalFlows->insert(nested);
        toCheck.insert(nested);
        iter->next();
      }
      delete iter;
    }
  }
}

bool SCHCycleDetection::isLeaf(FLNodeElab* flow)
{
  // if there is no nesting, it is a leaf
  if (!flow->hasNestedBlock() && !flow->hasNestedHier())
    return true;

  // with nesting, it is only a leaf if it is a For loop
  NUUseDefNode* useDef = flow->getUseDefNode();
  return (useDef && (useDef->getType() == eNUFor));
}


void SCHCycleDetection::getLeafLevelContinuousFanin(FLNodeElabSet& continuousFlows,
                                                    FLNodeElabSet& internalFlows,
                                                    FlowToFlowSetMap* fanins,
                                                    FLElabUseCache* levelCache)
{
  // We build it up in stages, computing the fanin + control
  // dependencies for each leaf-level statement, refining the
  // higher-level fanins to leaf-level fanins, contracting
  // non-continuous fanins, filtering out fanins that are not in the
  // cycle and finally computing the fanout sets from the fanin sets.

  // First, gather all of the roots of the nesting trees we know about
  FLNodeElabSet roots;
  FLNodeElabSet nonRoots;
  roots.insert(continuousFlows.begin(), continuousFlows.end());
  roots.insert(internalFlows.begin(), internalFlows.end());
  for (FLNodeElabSet::iterator i = roots.begin(); i != roots.end(); ++i)
  {
    FLNodeElab* flow = *i;
    if (flow->hasNestedBlock())
    {
      for (FLNodeElabLoop l = flow->loopNested(); !l.atEnd(); ++l)
      {
        FLNodeElab* nested = *l;
        nonRoots.insert(nested);
      }
    }
  }
  set_subtract(nonRoots, &roots);
  nonRoots.clear();

  // build the data and control dependencies 
  FlowToFlowSetMap dataFanins;
  FlowToFlowSetMap controlFanins;
  UtVector<FLNodeElabSet> controlStack;
  for (FLNodeElabSet::iterator i = roots.begin(); i != roots.end(); ++i)
  {
    FLNodeElab* flow = *i;
    getDataAndControlDependencies(flow, &dataFanins, &controlFanins, &controlStack);
    FLN_ELAB_ASSERT(controlStack.empty(), flow);
  }

  // Refine higher-level fanins to leaf-level
  refineFaninToLeafLevel(&dataFanins, true, levelCache);     // requires overlap
  refineFaninToLeafLevel(&controlFanins, false, NULL); // does not require overlap

  // Now combine data and control dependencies
  FlowToFlowSetMap allFanins;
  for (FlowToFlowSetMap::iterator i = dataFanins.begin(); i != dataFanins.end(); ++i)
  {
    FLNodeElab* flow = i->first;
    FLNodeElabSet& faninSet = i->second;
    FLNodeElabSet& destinationSet = allFanins[flow];
    for (FLNodeElabSet::iterator l = faninSet.begin(); l != faninSet.end(); ++l)
    {
      FLNodeElab* fanin = *l;
      destinationSet.insert(fanin);
    }
  }   
  dataFanins.clear();
  for (FlowToFlowSetMap::iterator i = controlFanins.begin(); i != controlFanins.end(); ++i)
  {
    FLNodeElab* flow = i->first;
    FLNodeElabSet& faninSet = i->second;
    FLNodeElabSet& destinationSet = allFanins[flow];
    for (FLNodeElabSet::iterator l = faninSet.begin(); l != faninSet.end(); ++l)
    {
      FLNodeElab* fanin = *l;
      destinationSet.insert(fanin);
    }
  }   
  controlFanins.clear();

  // Replace internal fanin with continuous fanin
  contractNonContinuousFanins(&allFanins, internalFlows);

  // Filter out non-cyclic and internal flow
  for (FlowToFlowSetMap::iterator i = allFanins.begin(); i != allFanins.end(); ++i)
  {
    FLNodeElab* flow = i->first;
    FLNodeElabSet& faninSet = i->second;
    
    if (internalFlows.find(flow) != internalFlows.end())
      continue; // do not populate internal flow

    FLNodeElabSet* destinationSet = NULL;
    for (FLNodeElabSet::iterator l = faninSet.begin(); l != faninSet.end(); ++l)
    {
      FLNodeElab* fanin = *l;
      if (continuousFlows.find(fanin) != continuousFlows.end())
      {
        if (destinationSet == NULL)
          destinationSet = &((*fanins)[flow]);
        destinationSet->insert(fanin);
      }
    }
  }   
}

// Replace non-leaf level fanins in the map with leaf-level fanins
void
SCHCycleDetection::refineFaninToLeafLevel(FlowToFlowSetMap* fanins,
                                          bool checkOverlap,
                                          FLElabUseCache* cache)
{
  for (FlowToFlowSetMap::iterator i = fanins->begin(); i != fanins->end(); ++i)
  {
    FLNodeElab* flow = i->first;
    FLNodeElabSet& faninSet = i->second;
    
    FLNodeElabSet leafFanin;

    for (FLNodeElabSet::iterator l = faninSet.begin(); l != faninSet.end(); ++l)
    {
      FLNodeElab* fanin = *l;
      FLNodeElabIter* iter = fanin->makeFaninIter(FLIterFlags::eAll,
                                                  FLIterFlags::eLeafNode,
                                                  FLIterFlags::eIntoAll,
                                                  true);
      while (!iter->atEnd())
      {
        FLNodeElab* nested = iter->getCur();
        if (isLeaf(nested))
        {
          if (!checkOverlap || 
              flow->checkFaninOverlap(nested, mUtil->getNetRefFactory(), cache))
            leafFanin.insert(nested);
          iter->doNotExpand();
        }
        iter->next();
      }
      delete iter;
    }
    faninSet.swap(leafFanin);
  }
}

//! Replace non-continuous fanins in the map with their continuous fanins
void SCHCycleDetection::contractNonContinuousFanins(FlowToFlowSetMap* fanins,
                                                    FLNodeElabSet& internalFlows)
{
  for (FlowToFlowSetMap::iterator i = fanins->begin(); i != fanins->end(); ++i)
  {
    FLNodeElabSet& faninSet = i->second;
    FLNodeElabSet nonContinuousFanin;
    for (FLNodeElabSet::iterator l = faninSet.begin(); l != faninSet.end(); ++l)
    {
      FLNodeElab* fanin = *l;
      if (internalFlows.find(fanin) != internalFlows.end())
      {
        // This is a non-continuous driver in the same block
        FLN_ELAB_ASSERT(fanin->getUseDefNode(), fanin);
        FLN_ELAB_ASSERT(!fanin->getUseDefNode()->isContDriver(), fanin);
        nonContinuousFanin.insert(fanin);
      }
    }
    // Remove non-continuous fanin and replace them with continuous fanin
    FLNodeElabSet covered;
    while (!nonContinuousFanin.empty())
    {
      FLNodeElabSet::iterator pos = nonContinuousFanin.begin();
      FLNodeElab* fanin = *pos;
      nonContinuousFanin.erase(pos);
      if (covered.find(fanin) != covered.end())
        continue;
      covered.insert(fanin);
      pos = faninSet.find(fanin);
      if (pos != faninSet.end())
        faninSet.erase(pos);

      // process fanins from this 
      FlowToFlowSetMap::iterator n = fanins->find(fanin);
      if (n != fanins->end())
      {
        FLNodeElabSet& newFaninSet = n->second;
        for (FLNodeElabSet::iterator l = newFaninSet.begin(); l != newFaninSet.end(); ++l)
        {
          FLNodeElab* faninsFanin = *l;
          if (internalFlows.find(faninsFanin) != internalFlows.end())
          {
            // this is another non-continuous nested flow, so work on its fanin
            if (covered.find(faninsFanin) == covered.end())
              nonContinuousFanin.insert(faninsFanin);
          }
          else
          {
            // this is a continuous driver, so add it to the fanin set
            faninSet.insert(faninsFanin);
          }
        }
      }
    }
  }
}

//! Joins partition classes which contain two statements with overlapping defs 
void SCHCycleDetection::processOverlappingDefs(const NUUseDefNode* useDef,
                                               NUStmtSet* allContinuousStmts,
                                               StmtPartition* partition)
{
  switch (useDef->getType())
  {
    case eNUAlwaysBlock:
    {
      const NUAlwaysBlock* always = dynamic_cast<const NUAlwaysBlock*>(useDef);
      processOverlappingDefs(always->getBlock(), allContinuousStmts, partition);
      break;
    }
    case eNUBlock:
    {
      const NUBlock* block = dynamic_cast<const NUBlock*>(useDef);
      processOverlappingDefsLoop(block->loopStmts(), allContinuousStmts, partition);
      break;
    }
    case eNUIf:
    {
      const NUIf* ifStmt = dynamic_cast<const NUIf*>(useDef);
      processOverlappingDefsLoop(ifStmt->loopThen(), allContinuousStmts, partition);
      processOverlappingDefsLoop(ifStmt->loopElse(), allContinuousStmts, partition);
      break;
    }
    case eNUCase:
    {
      const NUCase* caseStmt = dynamic_cast<const NUCase*>(useDef);
      for (NUCase::ItemCLoop l = caseStmt->loopItems(); !l.atEnd(); ++l)
      {
        const NUCaseItem* caseItem = *l;
        processOverlappingDefsLoop(caseItem->loopStmts(), allContinuousStmts, partition);        
      }
      break;
    }
    default:
    {
      NU_ASSERT("Unexpected use-def node" == NULL, useDef);
    }
  }
}

void SCHCycleDetection::processOverlappingDefsLoop(NUStmtCLoop stmtLoop,
                                                   NUStmtSet* allContinuousStmts,
                                                   StmtPartition* partition)
{
  // Iterate over all stmts in the loop.  For each statement get its
  // defs and compare against already seen def ranges.  If it overlaps
  // with previous statements, join their partition classes.  Only do this
  // for continuously driving statements.
  NetRefStmtMultiMap defRanges(mUtil->getNetRefFactory());
  for (; !stmtLoop.atEnd(); ++stmtLoop)
  {
    NUStmt* stmt = *stmtLoop;

    if (allContinuousStmts->find(stmt) == allContinuousStmts->end())
      continue;

    NUNetRefSet defs(mUtil->getNetRefFactory());
    stmt->getBlockingDefs(&defs);
    
    for (NUNetRefSet::iterator n = defs.begin(); n != defs.end(); ++n)
    {
      const NUNetRefHdl& defNetRef = *n;
      for (NetRefStmtMultiMap::CondLoop l = defRanges.loop(defNetRef, &NUNetRef::overlapsSameNet);
           !l.atEnd();
           ++l)
      {
        const NUNetRefHdl& ref = (*l).first;
        NUStmt* otherStmt = (*l).second;

        // If we get here, it means stmt and otherStmt have overlapping continuous defs
        // and are not in exclusive legs of the same control structure.  We want to join
        // all leaf-level statements below stmt and otherStmt which overlap this def range
        // into the same partition class.
        const NUNetRefHdl& overlap = mUtil->getNetRefFactory()->intersect(ref,defNetRef);
        NU_ASSERT2(overlap->getNumBits() > 0, stmt, otherStmt);
        
        NUStmtSet leaves;
        getLeafStmts(stmt, &leaves);
        getLeafStmts(otherStmt, &leaves);

        OrderedStmtSet overlappingLeaves;
        for (NUStmtSet::iterator s = leaves.begin(); s != leaves.end(); ++s)
        {
          NUStmt* leafStmt = *s;
          if (leafStmt->queryBlockingDefs(overlap, &NUNetRef::overlapsSameNet, mUtil->getNetRefFactory()))
            overlappingLeaves.insert(leafStmt);
        }
        partition->equate(overlappingLeaves);
      }
      defRanges.insert(defNetRef,stmt);
    }
  }
}

class LeafCounterCallback : public NUDesignCallback
{
public:
  LeafCounterCallback(NUStmtSet* leaves)
    : mLeaves(leaves), mLast(NULL)
  {}

  // Default behavior
  virtual NUDesignCallback::Status operator()(Phase /* phase */, NUBase* /* node */)
  {
    return eNormal;
  }

  // No point looking for leaf statements inside expressions
  virtual NUDesignCallback::Status operator()(Phase /* phase */, NUExpr* /* expr */)
  {
    return eSkip;
  }

  // Detect a leaf because on the way up, it will
  // still be in the mLast field set on the way down.
  virtual NUDesignCallback::Status operator()(Phase phase, NUStmt* stmt)
  {
    if (phase == ePre)
      mLast = stmt;
    else if (mLast == stmt)
      mLeaves->insert(stmt);
    return eNormal;
  }
private:
  NUStmtSet* mLeaves;
  NUStmt*    mLast;
};

void SCHCycleDetection::getLeafStmts(NUStmt* stmt, NUStmtSet* leaves)
{
  // We use an NUDesignWalker to populate a set of leaves under the given stmt
  LeafCounterCallback callback(leaves);
  NUDesignWalker walker(callback, false);
  walker.stmt(stmt);
}

void SCHCycleDetection::getDataAndControlDependencies(FLNodeElab* flow,
                                                      FlowToFlowSetMap* dataFanins,
                                                      FlowToFlowSetMap* controlFanins,
                                                      UtVector<FLNodeElabSet>* controlStack)
{
  // If this is a leaf-level flow, add all fanin and control dependencies
  if (isLeaf(flow))
  {
    FLNodeElabSet& dataFaninSet = (*dataFanins)[flow];
    FLNodeElabSet& controlFaninSet = (*controlFanins)[flow];
    for (FLNodeElabLoop l = flow->loopFanin(); !l.atEnd(); ++l)
    {
      FLNodeElab* dataFanin = *l;
      dataFaninSet.insert(dataFanin);
    }
    for (UtVector<FLNodeElabSet>::iterator i = controlStack->begin();
         i != controlStack->end();
         ++i)
    {
      FLNodeElabSet& controlDeps = *i;
      for (FLNodeElabSet::iterator n = controlDeps.begin();
           n != controlDeps.end();
           ++n)
      {
        FLNodeElab* controlFanin = *n;
        controlFaninSet.insert(controlFanin);
      }
    }
    return;
  }

  // otherwise, this is not a leaf-level flow so check for control dependencies  
  bool addedControlDeps = false;
  NUUseDefNode* useDef = flow->getUseDefNode();
  switch (useDef->getType())
  {
    case (eNUAlwaysBlock):
    {
      // For an always block, only the edge fanin (if any) is a control dependency.
      if (flow->hasEdgeFanin())
      {
        UInt32 idx = controlStack->size();
        controlStack->resize(idx+1);
        for (Iter<FLNodeElab*> loop = flow->loopEdgeFanin(); !loop.atEnd(); ++loop)
        {
          FLNodeElab* fanin = *loop;
          controlStack->back().insert(fanin);
          addedControlDeps = true;
        }
      }

      // Recurse into nested flow
      for (FLNodeElabLoop n = flow->loopNested(); !n.atEnd(); ++n)
      {
        FLNodeElab* nested = *n;
        getDataAndControlDependencies(nested, dataFanins, controlFanins, controlStack);
      }
      break;
    }
    case (eNUBlock):
      // A block adds no control dependencies.
      // Recurse into nested flow
      for (FLNodeElabLoop n = flow->loopNested(); !n.atEnd(); ++n)
      {
        FLNodeElab* nested = *n;
        getDataAndControlDependencies(nested, dataFanins, controlFanins, controlStack);
      }
      break;
    case (eNUIf):
    {
      // For an if statement, the condition provides control dependencies.
      NUIf* stmt = dynamic_cast<NUIf*>(useDef);
      NUExpr* cond = stmt->getCond();
      FLExprFaninClassifier<FLNodeElab> classifier(flow,cond);
      for (FLExprFaninClassifier<FLNodeElab>::FlowLoop loop = classifier.loopExprFanin();
           !loop.atEnd();
           ++loop)
      {
        FLNodeElab* fanin = *loop;
        if (!addedControlDeps)
        {
          UInt32 idx = controlStack->size();
          controlStack->resize(idx+1);
          addedControlDeps = true;
        }
        controlStack->back().insert(fanin);
      }

      // Recurse into nested flow
      for (FLNodeElabLoop n = flow->loopNested(); !n.atEnd(); ++n)
      {
        FLNodeElab* nested = *n;
        getDataAndControlDependencies(nested, dataFanins, controlFanins, controlStack);
      }
      break;
    }
    case (eNUCase):
    {
      // For a case statement, the select and case conditions provide control dependencies.
      // The select expression provides a control dependency for each case item, and
      // the case conditions provide control dependencies for their case item
      // ***and all subsequent case items***.
      NUCase* stmt = dynamic_cast<NUCase*>(useDef);
      NUExpr* sel = stmt->getSelect();
      FLExprFaninClassifier<FLNodeElab> classifier(flow,sel);
      for (FLExprFaninClassifier<FLNodeElab>::FlowLoop loop = classifier.loopExprFanin();
           !loop.atEnd();
           ++loop)
      {
        FLNodeElab* fanin = *loop;
        if (!addedControlDeps)
        {
          UInt32 idx = controlStack->size();
          controlStack->resize(idx+1);
          addedControlDeps = true;
        }
        controlStack->back().insert(fanin);
      }
      FLCaseClassifier<FLNodeElab> caseClassifier(useDef);
      caseClassifier.classifyFlow(flow);
      for (NUCase::ItemLoop l = stmt->loopItems(); !l.atEnd(); ++l)
      {
        NUCaseItem* caseItem = *l;
        for (NUCaseItem::ConditionLoop c = caseItem->loopConditions(); !c.atEnd(); ++c)
        {
          NUCaseCondition* caseCond = *c;
          NUExpr* cond = caseCond->getExpr();
          FLExprFaninClassifier<FLNodeElab> classifier(flow,cond);
          for (FLExprFaninClassifier<FLNodeElab>::FlowLoop loop = classifier.loopExprFanin();
               !loop.atEnd();
               ++loop)
          {
            FLNodeElab* fanin = *loop;
            if (!addedControlDeps)
            {
              UInt32 idx = controlStack->size();
              controlStack->resize(idx+1);
              addedControlDeps = true;
            }
            controlStack->back().insert(fanin);
          }
        }
        // Recurse into nested flow for this case item
        for (FLCaseClassifier<FLNodeElab>::FlowLoop loop = caseClassifier.loopCaseItem(caseItem,flow);
             !loop.atEnd();
             ++loop)
        {
          FLNodeElab* nested = *loop;
          getDataAndControlDependencies(nested, dataFanins, controlFanins, controlStack);
        }
      }
      break;
    }
    case (eNUFor):
    {
      // for a for loop, the loop condition provides a control dependency for the
      // loop body and increment statement.
      NU_ASSERT("Not Implemented!" == NULL, useDef);
      break;
    }
    default:
    {
      NU_ASSERT("Unexpected non-leaf construct!" == NULL, useDef);
      break;
    }
  }

  // pop the control dependencies off the stack, if any were added
  if (addedControlDeps)
    controlStack->pop_back();
}

//! Walker callback class for identifying disqualifying elements in a block
class SplitQualifierCallback : public NUDesignCallback
{
public:
  SplitQualifierCallback() : mOK(true) {}

  //! Read the status of the callback
  bool isOK() const { return mOK; }

  // Default behavior is allow everything
  virtual NUDesignCallback::Status operator()(Phase /* phase */, NUBase* /* node */)
  {
    return eNormal;
  }

  // Disqualify if a $stop or $finish system task is encountered.
  virtual NUDesignCallback::Status operator()(Phase /* phase */, NUSysTask* node)
  {
    NUControlSysTask* controlTask = dynamic_cast<NUControlSysTask*>(node);
    mOK =!controlTask;
    return mOK ? eNormal : eStop;
  }

  // Disqualify if a memory row is written in the block
  virtual NUDesignCallback::Status operator()(Phase /* phase */, NUMemselLvalue* /* node */)
  {
    mOK = false;
    return eStop;
  }  

private:
  bool mOK;
};

//! Return true if the given block is safe to split
bool SCHCycleDetection::qualifyBlockForSplitting(NUAlwaysBlock* always)
{
  // Current disqualifications:
  //  1. no $stop or $finish
  //  2. no NUMemselLvalue (partial memory writes)
  SplitQualifierCallback callback;
  NUDesignWalker walker(callback, false);
  walker.alwaysBlock(always);
  if (!callback.isOK()) {
    return false;
  }

  // Don't split blocks with the OST net. Those statements have side
  // effects.
  NUModule* module = always->findParentModule();
  NUNetRefFactory* netRefFactory = mUtil->getNetRefFactory();
  NUNet* ostNet = module->getOutputSysTaskNetAsNUNet();
  NUNetRefHdl ostNetRef = netRefFactory->createNetRef(ostNet);
  NUBlock* block = always->getBlock();
  if (block->queryBlockingDefs(ostNetRef, &NUNetRef::overlapsSameNet,
                               netRefFactory)) {
    return false;
  }
  
  // Things like for-loops and task enables are treated as leaf-level
  // statements, and therefore we can split them as a whole but will
  // not try to split statements within a for loop or within a task.
  return true;
}

/*!
 * Convert the set of splits for each instance of a block in a cycle
 * to a set of splits for each unelaborated block.  The unelaborated
 * splits may be more fine-grained because they combine splits from
 * mutliple instances.
 */
void SCHCycleDetection::convertElabSplits(ElabSplitMap& elabSplitMap,
                                          SplitMap* splitMap)
{
  typedef UtSet<OrderedStmtSet*> CrossSection;
  UtMap<NUStmt*, CrossSection> stmtCrossSections;
  UtMap<CrossSection, NUStmtSet, SetOrder<CrossSection> > newPartitions;
  UtMap<const NUAlwaysBlock*, NUStmtSet> blockStmts;

  // First compute the cross sections for each statement
  for (ElabSplitMap::iterator l = elabSplitMap.begin();
       l != elabSplitMap.end();
       ++l)
  {
    SCHUseDefHierPair udhp = l->first;
    const NUAlwaysBlock* always = dynamic_cast<const NUAlwaysBlock*>(udhp.getUseDef());
    PartitionVector& partitionVec = l->second;
    for (PartitionVector::iterator i = partitionVec.begin();
         i != partitionVec.end();
         ++i)
    {
      OrderedStmtSet& stmtSet = *i;
      for (OrderedStmtSet::iterator s = stmtSet.begin();
           s != stmtSet.end();
           ++s)
      {
        NUStmt* stmt = *s;
        blockStmts[always].insert(stmt);
        stmtCrossSections[stmt].insert(&stmtSet);
      }
    }
  }

  // Then compute the sets of stmts with the same cross section
  for (UtMap<NUStmt*,CrossSection>::iterator i = stmtCrossSections.begin();
       i != stmtCrossSections.end();
       ++i)
  {
    NUStmt* stmt = i->first;
    CrossSection& section = i->second;
    newPartitions[section].insert(stmt);
  }

  // Finally, populate the unelaborated split map
  for (UtMap<const NUAlwaysBlock*,NUStmtSet>::iterator i = blockStmts.begin();
       i != blockStmts.end();
       ++i)
  {
    const NUAlwaysBlock* always = i->first;
    NUStmtSet& stmts = i->second;

    PartitionVector& partitionVec = (*splitMap)[always];

    NUStmtSet covered;
    for (NUStmtSet::SortedLoop l = stmts.loopSorted(); !l.atEnd(); ++l) {
      NUStmt* stmt = *l;
      if (covered.find(stmt) != covered.end())
        continue;

      // we have not seen this statement, so add its partition class
      CrossSection& section = stmtCrossSections[stmt];
      NUStmtSet& partition = newPartitions[section];
      
      partitionVec.resize(partitionVec.size() + 1);
      partitionVec.back().insert(partition.begin(), partition.end());
      
      covered.insert(partition.begin(), partition.end());
    }
  }
}

class StmtMappingCallback : public NUDesignCallback
{
public:
  StmtMappingCallback() : mStmtSequence(), mMap(NULL) {}
  
  ~StmtMappingCallback()
  {
    INFO_ASSERT(mStmtSequence.empty(), "Imbalance in StmtMappingCallback!");
  }

  void populateMap(NUStmtStmtMap* stmtMap) { mMap = stmtMap; }

  NUDesignCallback::Status operator()(Phase /* phase */, NUBase* /* node */)
  { 
    return eNormal;
  }

  NUDesignCallback::Status operator()(Phase /* phase */, NUTask * /* tf */)
  {
    return eSkip;
  }

  NUDesignCallback::Status operator()(Phase /* phase */, NUExpr* /* expr */)
  { 
    return eSkip;
  }

  NUDesignCallback::Status operator()(Phase phase, NUStmt* stmt)
  { 
    if (phase == ePre)
    {
      if (mMap)
      {
        NUStmt* orig = mStmtSequence.front();
        mStmtSequence.pop_front();
        (*mMap)[stmt] = orig;
      }
      else
      {
        mStmtSequence.push_back(stmt);
      }
    }
    return eNormal;
  }

private:
  UtList<NUStmt*> mStmtSequence;
  NUStmtStmtMap*  mMap;
};

class StmtAddingCallback : public NUDesignCallback
{
public:
  StmtAddingCallback(NUNetRefFactory* factory, OrderedStmtSet& keep_stmts,
                     NUStmtStmtMap& stmt_to_original,
                     NUNetToNUNetRefMap & net_replacements)
    : mNetRefFactory(factory), mKeepStmts(keep_stmts),
      mOrigStmtMap(stmt_to_original), mOrigNetRefMap(net_replacements)
  {}

  NUDesignCallback::Status operator()(Phase /* phase */, NUBase* /* node */)
  { 
    return eNormal;
  }

  NUDesignCallback::Status operator()(Phase /* phase */, NUTask * /* tf */)
  {
    return eSkip;
  }

  NUDesignCallback::Status operator()(Phase /* phase */, NUExpr* /* expr */)
  { 
    return eSkip;
  }

  NUDesignCallback::Status operator()(Phase phase, NUBlock* block)
  { 
    if (phase == ePost)
    {
      // check the statement list for replacement candidates
      NUStmtList* replacementList = getReplacementList(block->loopStmts());
      if (replacementList != NULL)
      {
        block->replaceStmtList(*replacementList);
        delete replacementList;
      }
    }
    return eNormal;
  }

  NUDesignCallback::Status operator()(Phase phase, NUIf* ifStmt)
  { 
    if (phase == ePost)
    {
      // check the statement lists for replacement candidates
      NUStmtList* replacementList = getReplacementList(ifStmt->loopThen());
      if (replacementList != NULL)
      {
        ifStmt->replaceThen(*replacementList);
        delete replacementList;
      }
      replacementList = getReplacementList(ifStmt->loopElse());
      if (replacementList != NULL)
      {
        ifStmt->replaceElse(*replacementList);
        delete replacementList;
      }
    }
    return eNormal;
  }

  NUDesignCallback::Status operator()(Phase phase, NUCaseItem* caseItem)
  { 
    if (phase == ePost)
    {
      // check the statement list for replacement candidates
      NUStmtList* replacementList = getReplacementList(caseItem->loopStmts());
      if (replacementList != NULL)
      {
        caseItem->replaceStmts(*replacementList);
        delete replacementList;
      }
    }
    return eNormal;
  }

private:
  NUStmtList* getReplacementList(NUStmtLoop stmtLoop)
  {
    bool doReplace = false;
    NUStmtList* newStmts = new NUStmtList;
    for (; !stmtLoop.atEnd(); ++stmtLoop)
    {
      NUStmt* stmt = *stmtLoop;

      NUStmtStmtMap::iterator pos = mOrigStmtMap.find(stmt);
      if (pos == mOrigStmtMap.end())
      {
        newStmts->push_back(stmt);
        continue;
      }

      NUStmt* orig_stmt = pos->second;
      bool keep = mKeepStmts.find(orig_stmt) != mKeepStmts.end();

      if (!keep)
      {
        // If this is a statement that only defs the $ostnet and is not in the keep set,
        // then skip it.
        NUNetSet defs;
        if (stmt->useBlockingMethods()) {
          NUUseDefStmtNode* stmtNode = dynamic_cast<NUUseDefStmtNode*>(stmt);
          stmtNode->getBlockingDefs(&defs);
        } else {
          stmt->getDefs(&defs);
        }
        if (defs.size() == 1)
        {
          NUNet* defNet = *defs.begin();
          if (defNet->isSysTaskNet())
          {
            delete stmt;
            doReplace = true;
            continue;
          }
        }
      }

      // Otherwise, include the statement and copy out the temp value if we want to keep it
      newStmts->push_back(stmt);

      if (keep) {
        // If we get here, this is a statement we want to copy the def'ed values from
        NUAssign* assign = dynamic_cast<NUAssign*>(stmt);
        if (assign)
          doReplace |= copyAssignmentLHS(assign, newStmts);
        else
          doReplace |= copyStmtDefs(stmt, newStmts);
      }
    }

    if (doReplace)
      return newStmts;
    else {
      delete newStmts;
      return NULL;
    }
  }

  bool copyAssignmentLHS(NUAssign* assign, NUStmtList* newStmts)
  {
    // use the assignment's LHS as our RHS
    NULvalue* temp_lhs = assign->getLvalue();
    NUExpr* rhs = temp_lhs->NURvalue();

    // use the assignment's LHS as our LHS, but replace temp nets with the originals
    CopyContext cc(NULL,NULL);
    NULvalue* lhs = temp_lhs->copy(cc);
    NUNetSet defs;
    lhs->getDefs(&defs);
    for (NUNetSet::iterator l = defs.begin(); l != defs.end(); ++l)
    {
      NUNet* new_net = *l;
      NUNetToNUNetRefMap::iterator p = mOrigNetRefMap.find(new_net); 
      NU_ASSERT(p != mOrigNetRefMap.end(), new_net);
      NUNetRefHdl& ref = p->second;
      NUNet* orig_net = ref->getNet();
      lhs->replaceDef(new_net,orig_net);
    }

    NUBlockingAssign* newAssign = new NUBlockingAssign(lhs,rhs,false,assign->getLoc());
    newStmts->push_back(newAssign);     

    return true;
  }

  bool copyStmtDefs(NUStmt* stmt, NUStmtList* newStmts)
  {
    bool modified = false;

    // Copy all nets defed in the statement from the temporary back to the original net
    NUNetRefSet defs(mNetRefFactory);
    if (stmt->useBlockingMethods()) {
      NUUseDefStmtNode* stmtNode = dynamic_cast<NUUseDefStmtNode*>(stmt);
      stmtNode->getBlockingDefs(&defs);
      // no non-blocking defs at this stage in compiler
    } else {
      stmt->getDefs(&defs);
    }
    for (NUNetRefSet::iterator l = defs.begin(); l != defs.end(); ++l)
    {
      const NUNetRefHdl& temp_net_ref = *l;
      NUNet* temp_net = temp_net_ref->getNet();

      if (temp_net->isVirtualNet())
        continue;

      NUNetToNUNetRefMap::iterator p = mOrigNetRefMap.find(temp_net); 
      NU_ASSERT(p != mOrigNetRefMap.end(), temp_net);
      NUNetRefHdl& orig_net_ref = p->second;
      NUNet* orig_net = orig_net_ref->getNet();

      UtList<NULvalue*> lvalues;
      temp_net_ref->createLvalues(mNetRefFactory, stmt->getLoc(), &lvalues);
      for (UtList<NULvalue*>::iterator n = lvalues.begin(); n != lvalues.end(); ++n)
      {
        NULvalue* lhs = *n;
        NUExpr* rhs = lhs->NURvalue();
        lhs->replaceDef(temp_net,orig_net);
        NUBlockingAssign* newAssign = new NUBlockingAssign(lhs,rhs,false,stmt->getLoc());
        newStmts->push_back(newAssign);
        modified = true;
      }
    }

    return modified;
  }


private:
  NUNetRefFactory*     mNetRefFactory;
  OrderedStmtSet&      mKeepStmts;
  NUStmtStmtMap&       mOrigStmtMap;
  NUNetToNUNetRefMap & mOrigNetRefMap;
};

//! Create a split version of an always block based on a statement partition class
NUAlwaysBlock* SCHCycleDetection::makeSplitBlock(UD * ud, const NUAlwaysBlock* orig, OrderedStmtSet& keep_stmts)
{
  /* Creating the split block is a 5 step process:
   *  1. Duplicate the original always block
   *  2. Replace all nets defed in the block with temp versions
   *  3. Add assignments to the beginning of the block to initialize
   *     the temps with their continuous values from outside the block.
   *     These assignments should be bit-blasted to minimize extraneous
   *     dependencies.
   *  4. For each statement in the new block which corresponds to a
   *     statement in the partition class, add an assignment of the original
   *     defnetref from the temp net directly after the new statement.
   *  5. Apply dead code removal and other optimizations.
   */

  // Duplicate the original block
  NUScope* parent = orig->getBlock()->getParentScope();
  NUModule* module = dynamic_cast<NUModule*>(parent);
  NU_ASSERT(module,orig);
  CopyContext copy_context(module, mUtil->getSymbolTable()->getAtomicCache(), true, "splatlocal");
  NUAlwaysBlock* new_always = orig->clone(copy_context, true);

  // build a map from new statements to original statements
  NUStmtLoop orig_stmt_loop = orig->getBlock()->loopStmts();
  NUStmtLoop new_stmt_loop  = new_always->getBlock()->loopStmts();
  NUStmtStmtMap stmt_to_original;
  {
    StmtMappingCallback mapping_callback;
    NUDesignWalker walker(mapping_callback,false);
    for (; !orig_stmt_loop.atEnd(); ++orig_stmt_loop)
    {
      NUStmt* stmt = *orig_stmt_loop;
      walker.stmt(stmt);
    }
    mapping_callback.populateMap(&stmt_to_original);
    for (; !new_stmt_loop.atEnd(); ++new_stmt_loop)
    {
      NUStmt* stmt = *new_stmt_loop;
      walker.stmt(stmt);
    }
  }

  // replace all nets defed in the block with temp versions
  NUNetToNUNetRefMap net_replacements;
  NUNetRefSet defs(mUtil->getNetRefFactory());
  orig->getBlock()->getBlockingDefs(&defs); // must use original (new block has no UD yet)
  NUBlock* new_block = new_always->getBlock();
  for (NUNetRefSet::SortedLoop n = defs.loopSorted(); not n.atEnd(); ++n)
  {
    NUNetRefHdl ref = *n;
    NUNet* orig_net = ref->getNet();

    if (orig_net->isSysTaskNet() || orig_net->isControlFlowNet())
      continue;

    // If this is a temp memory, we can use it directly, with no need for another temp
    if (orig_net->isNonStatic() && orig_net->is2DAnything())
    {
      NUTempMemoryNet* temp_mem = dynamic_cast<NUTempMemoryNet*>(copy_context.mNetReplacements[orig_net]);
      if (temp_mem)
      {
        temp_mem->putIsDynamic();
        continue;
      }
    }

    // If this is a block-local net, we need to use the replacement
    // created during the copy operation.
    if (orig_net->isNonStatic())
    {
      orig_net = copy_context.mNetReplacements[orig_net];
      ref = mUtil->getNetRefFactory()->createNetRef(orig_net);
    }

    NUNet* new_net = createReplacementNet(new_block, orig_net);
    new_block->replace(orig_net, new_net);
    net_replacements[new_net] = ref;
  }

  // Add initializers for temps (in reverse order so the added
  // statements match declaration order).
  const SourceLocator & loc = new_block->getLoc();
  for (NUNetToNUNetRefMap::reverse_iterator n = net_replacements.rbegin();
       n != net_replacements.rend();
       ++n)
  {
    NUNet* temp_net = n->first;
    NUNetRefHdl& ref = n->second;
    NUNet* orig_net = ref->getNet();
    
    if (orig_net->isNonStatic())
      continue;  // these don't require initializers
    
    UtList<NULvalue*> lvalues;
    ref->createLvalues(mUtil->getNetRefFactory(), loc, &lvalues, true); // blast vector assigns
    for (UtList<NULvalue*>::iterator l = lvalues.begin(); l != lvalues.end(); ++l)
    {
      NULvalue* lhs = *l; // in terms of original net
      NUExpr* rhs = lhs->NURvalue(); 
      lhs->replaceDef(orig_net,temp_net); // switch to temp net
      NUBlockingAssign * assign = new NUBlockingAssign(lhs,rhs,new_block->getUsesCFNet(),loc);
      new_block->addStmtStart(assign);
    }
  }

  // Compute UD (needed to correctly add out-copies).
  ud->structuredProc(new_always);
  
  // add out-copies directly after preserved statements
  {
    StmtAddingCallback adding_callback(mUtil->getNetRefFactory(), keep_stmts,
                                       stmt_to_original, net_replacements);
    NUDesignWalker walker(adding_callback, false);
    walker.alwaysBlock(new_always);
  }

  return new_always;
}

NUNet* SCHCycleDetection::createReplacementNet(NUBlock* block, NUNet* original)
{
  NUNet * replacement = NULL;

  if (original->isVectorNet()) {
    NUVectorNet * vector_net = dynamic_cast<NUVectorNet*>(original);
    NetFlags net_flags = eNoneNet;
    if ( vector_net->isInteger() ) {
      if (vector_net->isSigned())
        net_flags = NetRedeclare(net_flags, eDMIntegerNet|eSigned);
      else
        net_flags = NetRedeclare(net_flags, eDMIntegerNet); 
    }
    if ( vector_net->isTime() ) {
      net_flags = NetRedeclare(net_flags, eDMTimeNet);
    }
    StringAtom* sym = block->gensym("splat", NULL, original);
    replacement = block->createTempVectorNet ( sym, *vector_net->getRange(),
                                               vector_net->isSigned(), 
                                               original->getLoc(), net_flags,
                                               vector_net->getVectorNetFlags ());
  } else if (original->isBitNet()) {
    StringAtom* sym = block->gensym("splat", NULL, original);
    replacement = block->createTempBitNet ( sym, original->isSigned(),
                                            original->getLoc());
  } else if (original->is2DAnything()) {
    replacement = block->createTempNetFromImage(original, "splat");
    NUTempMemoryNet* temp_mem = dynamic_cast<NUTempMemoryNet*>(replacement);
    NU_ASSERT(temp_mem, replacement);
    temp_mem->putIsDynamic();
  } else {
    NU_ASSERT("Original net has unknown type" == NULL, original);
  }

  NU_ASSERT(replacement,original);
  return replacement;
}

/*!
 * This routine is called after the final cycles have been determined,
 * to create the Nucleus structures for the cycles and the FLNodeElabCycle
 * nodes, and to determine an execution order for the flow nodes in each cycle.
 */
void SCHCycleDetection::buildAndScheduleCycles(SCHGraphList* cycles)
{
  // Build and populate an NUCycle structure for each cyclic graph
  for (SCHGraphList::iterator g = cycles->begin(); g != cycles->end(); ++g)
  {
    DependencyGraph* graph = *g;
    NUCycle* cycle = NULL;
    for (Iter<GraphNode*> iter = graph->nodes(); !iter.atEnd(); ++iter)
    {
      GraphNode* gn = *iter;
      DependencyGraph::Node* node = graph->castNode(gn);
      FLNodeElab* flow = node->getData();
      if (cycle == NULL) // create the NUCycle on the first iteration
      {
        cycle = new NUCycle(flow->getDefNet()->getNet()->getLoc(), mMarkDesign->numCycles() + 1);
        mMarkDesign->newCycleNode(cycle);
      }
      addCycleNode(cycle, flow);
    } // for each cycle node

    // update output ports to point to the cycle node, not the original flow node
    updateOutputPortsInCycle(cycle);

    // Schedule the nodes in the cycle
    scheduleCycle(cycle, graph);
  } // for each combinational cycle

  // We have added cycle nodes, which changes the flow
  mMarkDesign->clearTrueFaninCaches();
}

//! Add a flow node to the NUCycle and creates an FLNodeElabCycle for it
void SCHCycleDetection::addCycleNode(NUCycle* cycle, FLNodeElab* flow)
{
  // don't add a node twice
  if (cycle->isNodeEncapsulated(flow))
    return;

  // Add the flow node to the cycle
  cycle->addFlow(flow);

  // Create an FLNodeElabCycle for it, associated with the given cycle.
  FLNodeElabCycle* cycleNode = mUtil->getFlowElabFactory()->createCycle(cycle, flow,
                                                                        mCycleIndex++);
  cycleNode->putDepth(DEPTH_UNINITIALIZED);
} // void SCHCycleDetection::addCycleNode

/*!
 * Given an NUcycle structure and the graph of ordering dependencies for
 * flow nodes in the cycle, this routine will produce a schedule for the
 * flow within the cycle that attempts to minimize multiple flow executions.
 */
void SCHCycleDetection::scheduleCycle(NUCycle* cycle, DependencyGraph* graph)
{
  // try to find a way to break the cycle so that it can run without looping
  SCHCycleBreaker breaker(cycle, graph, mUtil, mMarkDesign);
  breaker.execute();
}

/*!
 * This routine reports cycle information and warnings to the user.
 */
bool SCHCycleDetection::reportCycleData(SCHGraphList* cycles, bool writeDotFiles)
{
  bool errors = false;

  int numCycles = 0;

  // If the user requested we write the cycle files, do it now
  if (writeDotFiles)
  {
    // write combinational cycles
    for (SCHGraphList::iterator g = cycles->begin();
         g != cycles->end();
         ++g)
    {
      DependencyGraph* graph = *g;
      UtString name;
      name << "cycleC" << ++numCycles << ".dot";
      CycleGraph::writeDependencyGraph(graph, name.c_str());
    }
  }

  // Report data for combinational cycles
  if (!mUtil->isFlagSet(eBreakCycles))
  {
    // if no cycle breaking, then report based on the cycle graphs
    for (SCHGraphList::iterator g = cycles->begin();
         g != cycles->end();
         ++g)
    {
      DependencyGraph* graph = *g;
      reportCycle(graph);
    }
  }
  else
  {
    // if cycle breaking, report based on the schedules determined
    for (SCHSchedule::CycleLoop l = mMarkDesign->loopCycles(); !l.atEnd(); ++l)
    {
      NUCycle* cycle = *l;
      size_t numNodes = cycle->numNodes();
      size_t numUnbroken = cycle->getSettledFlowCount();
      UInt32 numBranches = cycle->getBranchCount();
      FLNodeElabHashSet flows;
      for (NUCycle::SortedLoop loop = cycle->loopSettledFlows(); !loop.atEnd(); ++loop)
      {
        FLNodeElab* flow = *loop;
        flows.insert(flow);
      }
      mMarkDesign->reportCycleFlows(flows, false, numNodes, numUnbroken, numBranches, cycle->getID());
    }
  }

  return errors;
}

//! Print a warning for each combinational flow node that depends on itself
void SCHCycleDetection::warnAboutCombinationalSelfCycles()
{
  FLNodeElabSet selfCycles;
  FLDesignElabIter iter(mUtil->getDesign(), mUtil->getSymbolTable(),
                        FLIterFlags::eAllElaboratedNets,
                        FLIterFlags::eAll,
                        FLIterFlags::eNone,
                        FLIterFlags::eBranchAtAll,
                        FLIterFlags::eIterNodeOnce);
  for (; !iter.atEnd(); ++iter)
  {
    FLNodeElab* flow = *iter;

    if (SCHUtil::isSequential(flow))
      continue; // we are only interested in combinational flow nodes
    if (!flow->isReachable())
      continue; // only look at live flow
    if (!SCHUtil::isContDriver(flow)) {
      continue; // Only look at continous flow (not nested flow)
    }

    // Check if this flow node is self referential, this can lead to
    // simulation failures
    for (Fanin p = mUtil->loopFanin(flow); !p.atEnd(); ++p)
    {
      FLNodeElab* fanin = *p;
      if (fanin == flow)
        selfCycles.insert(flow);
    }
  }

  // print warnings for each one, ordered for stability
  for (FLNodeElabSet::SortedLoop loop = selfCycles.loopSorted(); !loop.atEnd(); ++loop)
  {
    FLNodeElab* flow = *loop;
    mUtil->getMsgContext()->SchAlwaysBlockCycle(flow);
  }
}

//! Print warnings for all cycle nodes
void SCHCycleDetection::reportCycle(DependencyGraph* graph)
{
  // gather the nodes into a set so that we can loop through them sorted
  FLNodeElabHashSet flows;
  int cycleSize = 0;
  for (Iter<GraphNode*> iter = graph->nodes(); !iter.atEnd(); ++iter)
  {
    GraphNode* gn = *iter;
    DependencyGraph::Node* node = graph->castNode(gn);
    FLNodeElab* flow = node->getData();
    ++cycleSize;
    flows.insert(flow);
  }

  // now print the messages for the set of flows
  mMarkDesign->reportCycleFlows(flows, false, graph->size(), graph->size(), 0, 0);
}

void SCHCycleDetection::suggestBlastNetDirectives()
{
  NUNetSet suggested;
  for (SCHSchedule::CycleLoop l = mMarkDesign->loopCycles(); !l.atEnd(); ++l)
  {
    NUCycle* cycle = *l;

    for (NUCycle::SortedLoop loop = cycle->loopSorted(); !loop.atEnd(); ++loop)
    {
      FLNodeElab* flow = *loop;
      NUNetElab* netElab = flow->getDefNet();
      if (netElab == NULL)
        continue;
      
      NUNet* net = netElab->getNet();
      if ((net->isVectorNet()) && (!net->isPort()) && (!net->isTemp()) && (net->getNetRefWidth() > 1))
      {
        // this vector net appears in the cycle, suggest blasting it if it hasn't been suggested yet
        if (suggested.find(net) == suggested.end())
        {
          UtString netName;
          net->composeUnelaboratedName(&netName);
          mUtil->getMsgContext()->SchTryBlastingNet(netName);
          suggested.insert(net);
        }
      }
    }
  }
}

#ifdef CDB
// Print a node vector - useful for debugging split blocks
void SCHCycleDetection::printNodeVector(FLNodeElabVector* nodeVector)
{
  for (size_t i = 0; i < nodeVector->size(); ++i)
  {
    FLNodeElab* flow = (*nodeVector)[i];
    if (flow != NULL)
    {
      UtString buf;
      flow->compose(&buf, NULL, 2, true);
      UtIO::cout() << i << ".\n" << buf;
    }
  }
}
#endif

//! Write cycles out to lib<design>.cycles file
void SCHCycleDetection::dumpCycles(const char* fileRoot)
{
  FILE* f = stdout;
  if (fileRoot != NULL)
  {
    UtString fname(fileRoot);
    fname << ".cycles";
    UtString reason;
    f = OSFOpen(fname.c_str(), "w", &reason);
    if (f == NULL)
    {
      fprintf(stderr, "%s\n", reason.c_str());
      return;
    }
  }

  for (SCHSchedule::CycleLoop l = mMarkDesign->loopCycles(); !l.atEnd(); ++l)
  {
    NUCycle* cycle = *l;
    UtString buf;
    cycle->compose(&buf, NULL, 0, true);
    fprintf(f, "%s", buf.c_str());
  }
  if (f != stdout)
    fclose(f);
} // void SCHCycleDetection::dumpCycles

void SCHCycleDetection::findFalseDesignCycles()
{
  // If we are timing passes, start a new timer
  Stats* stats = mUtil->getStats();
  if (stats != NULL)
    stats->pushIntervalTimer();

  // Create the combinational blocks for the design
  createBlocks();
  if (stats)
    stats->printIntervalStatistics("FCD Create");

  // Break the false cycles
  breakFalseCycleBlocks();
  if (stats)
    stats->printIntervalStatistics("FCD Break 1");

  // Break the false cycles again. If we found any then the algorithm
  // is missing some nodes to break.
  UInt32 count = breakFalseCycleBlocks();
  INFO_ASSERT(count == 0, "False cycle detection missed some cycles");
  if (stats)
    stats->printIntervalStatistics("FCD Break 2");

  // Go through the blocks and create the splits
  if (mUtil->isFlagSet(eDumpSplitBlocks))
    UtIO::cout() << "Splitting combinational blocks to break false cycles:\n";
  splitFalseCycleBlocks();
  if (stats)
    stats->printIntervalStatistics("FCD Split");

  // Free the memory we don't need anymore
  freeBlocks();

  if (stats)
    stats->popIntervalTimer();
}

void SCHCycleDetection::createBlocks()
{
  // Need a map from always blocks to all the flow nodes
  SCHBlockFlowNodes blockFlowNodes(mUtil);

  // Visit all the combinational nodes recursively from the design
  // starting points (output ports and sequential nodes). We create
  // the block representation for all the combinational blocks.
  INFO_ASSERT(mBlocks->empty() && mFlowToBlockMap->empty(), "createBlocks called multiple times without clearing block sets");
  for (SCHMarkDesign::DesignIter i(mMarkDesign, false); !i.atEnd(); ++i) {
    FLNodeElab* flowElab = *i;
    if (!SCHUtil::isSequential(flowElab)) {
      createBlock(flowElab, blockFlowNodes);
    }
  }
}

void
SCHCycleDetection::createBlock(FLNodeElab* flow,
                               const SCHBlockFlowNodes& blockFlowNodes)
{
  // Check if we are done with this node
  FlowToBlockMap::iterator pos = mFlowToBlockMap->find(flow);
  if (pos != mFlowToBlockMap->end())
    return;

  // Create the block for this flow node and all its siblings. This
  // marks this flow node and all its siblings done.
  newBlock(flow, blockFlowNodes);
}

UInt32 SCHCycleDetection::breakFalseCycleBlocks()
{
  // We use the pass counter to make sure we don't visit nodes more
  // than once
  INFO_ASSERT(mSplitNodes->empty(),
              "breakFalseCycleBlocks called multiple times without clearing split node set");
  UInt32 newPass = mUtil->bumpSchedulePass();
//#define DEBUG_FALSE_CYCLES
#ifdef DEBUG_FALSE_CYCLES
  Stats* stats = mUtil->getStats();
  if (stats) {
    stats->pushIntervalTimer();
  }
#endif

  // Create a graph of the blocks and find the cycles. Then limit the
  // scope of the breaking algorithm to just those blocks.
  BlockGraph* graph = createBlockGraph();
  GraphSCC scc;
  scc.compute(graph);
  if (!scc.hasCycle()) {
    // Nothing to be done
    delete graph;
    return 0;
  }

  // Visit all the strongly connected components and analyze them. We
  // gather all the blocks in the cycle and only analyze them.
  UInt32 splitCount = 0;
  for (GraphSCC::ComponentLoop l = scc.loopCyclicComponents(); !l.atEnd(); ++l) {
    // Gather all the Block's in this cycle
    GraphSCC::Component* comp = *l;
    BlockSet cyclicBlocks;
    for (GraphSCC::Component::NodeLoop n = comp->loopNodes(); !n.atEnd(); ++n) {
      GraphNode* graphNode = *n;
      Block* block = graph->castNode(graphNode)->getData();
      cyclicBlocks.insert(block);
    }

    // Gather all the start flow for the blocks. We can't count on the
    // blocks being correct as we traverse because they get split by
    // the findFalseCycles function.
    FLNodeElabSet startFlows;
    for (Loop<BlockSet> b(cyclicBlocks); !b.atEnd(); ++b) {
      Block* block = *b;
      for (Block::NodesLoop n = block->loopNodes(); !n.atEnd(); ++n) {
        FLNodeElab* flowElab = *n;
        startFlows.insert(flowElab);
      }
    }

    // Break the cycles starting at the flows for all the blocks
    FlowStack flowStack;
    for (FLNodeElabSetLoop l(startFlows); !l.atEnd(); ++l) {  // SORT THIS?
      FLNodeElab* flowElab = *l;
      FLNodeElab* breakPoint = findFalseCycles(flowElab, newPass, &flowStack,
                                                 &splitCount, 0, &cyclicBlocks);
#ifdef DEBUG_FALSE_CYCLES
      stats->printIntervalStatistics("CombDone");
#endif

      // All cycles should be broken by the time this returns
      FLN_ELAB_ASSERT(breakPoint == NULL,breakPoint);
    }
  } // for

#ifdef DEBUG_FALSE_CYCLES
  if (stats) {
    stats->popIntervalTimer();
  }
#endif

  // Make sure no-one bumped this while we were running
  INFO_ASSERT(mUtil->equalSchedulePass(newPass), "Phases mixed during execution");
  delete graph;
  return splitCount;
} // UInt32 SCHCycleDetection::breakFalseCycleBlocks


void
SCHCycleDetection::pushFlow(FlowStack* flowStack, FLNodeElab* curFlow,
			    FLNodeElab* hitFlow)
{
  Flow* flow = new Flow(curFlow, hitFlow);
  flowStack->push_back(flow);
}

void
SCHCycleDetection::popFlow(FlowStack* flowStack)
{
  INFO_ASSERT(!flowStack->empty(), "Attempt to pop from an empty stack");
  Flow* flow = flowStack->back();
  flowStack->pop_back();
  delete flow;
}

FLNodeElab* SCHCycleDetection::findBreakPoint(const FlowStack& flowStack,
					      FLNodeElab* endFlow)
{
  // Find the potential break points. These are all the spots where a
  // cycle was found by jumping from one flow node in a block to
  // another when visiting fanin. We evaluate if they are good or bad
  // break points based on if there is flow between the two nodes
  // (internally). Bad ones have flow.
  bool endFound = false;
  FlowStack goodBreakPoints;
  FlowStack lastResortBreakPoints;
  FlowStackRevIter r;
  for (r = flowStack.rbegin(); (r != flowStack.rend()) && !endFound; ++r)
  {
    // Check if this is a valid break point
    Flow* flow = *r;
    FLNodeElab* hitFlow = flow->getHitFlow();
    if (flow->isBreakPoint())
    {
      // It is valid, check if there is any flow between these nodes
      // (internal flow only)
      FLNodeElab* curFlow = flow->getCurFlow();
      if (!haveInternalFlow(curFlow, hitFlow) || isSplitFlow(curFlow))
	goodBreakPoints.push_back(flow);
      else
	lastResortBreakPoints.push_back(flow);
    }

    // Check if we are at the end of the line
    endFound = (hitFlow == endFlow);
  }

  // Evaluate the found break points. If there are good breakpoints do
  // those, otherwise, we use the last resort break points. At this
  // point if we have more than one break point to judge, we do it
  // based on the size of the block. It is better to split smaller
  // blocks.
  FLNodeElab* breakPoint = NULL;
  FlowStack* breakPoints;
  if (goodBreakPoints.empty())
    breakPoints = &lastResortBreakPoints;
  else
    breakPoints = &goodBreakPoints;
  int size = 0;
  for (FlowStackIter i = breakPoints->begin(); i != breakPoints->end(); ++i)
  {
    // Compute the size
    Flow* flow = *i;
    FLNodeElab* hitFlow = flow->getHitFlow();
    FLNodeElab* curFlow = flow->getCurFlow();
    NUNetSet defs;
    SCHUtil::getBlockNets(curFlow, &defs);
    int thisSize = defs.size();

    // Check if this is a better break point than the last (or the
    // first break point)
    if ((size == 0) || (thisSize < size) || isSplitFlow(curFlow))
    {
      breakPoint = hitFlow;
      size = thisSize;
    }
  }
  
  // if we didn't find a break point a real cycle crept through the
  // cycle detector. print that cycle.
  if (breakPoint == NULL)
  {
    FLNodeElabVector nodes;
    for (FlowStackIter i = flowStack.begin(); i != flowStack.end(); ++i)
    {
      Flow* flow = *i;
      FLNodeElab* hitFlow = flow->getHitFlow();
      nodes.push_back(hitFlow);
    }
    printUndetectedCycle(endFlow, nodes);
  }

  INFO_ASSERT(breakPoint != NULL, "Real cycle encountered during false cycle detection");
  return breakPoint;
} // FLNodeElab* SCHCycleDetection::findBreakPoint

bool
SCHCycleDetection::haveInternalFlow(FLNodeElab* flow1, FLNodeElab* flow2)
{
  bool internalFlow = false;

  // Visit from flow1 looking for flow2
  SCHMarkDesign::TrueFaninLoop l;
  for (l = mMarkDesign->loopTrueFanin(flow1); !l.atEnd() && !internalFlow; ++l)
  {
    FLNodeElab* fanin = *l;
    internalFlow = (fanin == flow2);
  }

  // Visit from flow2 looking for flow1
  for (l = mMarkDesign->loopTrueFanin(flow2); !l.atEnd() && !internalFlow; ++l)
  {
    FLNodeElab* fanin = *l;
    internalFlow = (fanin == flow1);
  }
  return internalFlow;
} // SCHCycleDetection:haveInternalFlow

FLNodeElab*
SCHCycleDetection::findFalseCycles(FLNodeElab* flow, UInt32 pass,
				   FlowStack* flowStack, UInt32* splitCount,
				   UInt32 depth, BlockSet* limitBlocks)
{
  // Make sure we are only traversing combinational logic
  // Sequential blocks can't be in a cycle
  if (SCHUtil::isSequential(flow))
    return NULL;

  // If this node is in a cycle, then we found a cycle due to visting
  // the brother of a flow node somewhere in our call stack. Return to
  // find that spot.
  if (flow->isInCyclePath())
  {
    FLNodeElab* breakFlow = findBreakPoint(*flowStack, flow);
#ifdef DEBUG_FALSE_CYCLES
    UtIO::cout() << "* Found a cycle, breaking at ";
    breakFlow->getName()->print();
#endif
    return breakFlow;
  }
 
  // Check if we are done with this node
  if (flow->getSchedulePass() == pass) {
    return NULL;
  }

  // Return if this is a bound node. This must be a primary input
  if (flow->getUseDefNode() == NULL) {
    return NULL;
  }

#ifdef DEBUG_FALSE_CYCLES
  UtIO::cout() << "Visiting flow (depth = " << depth << ") ";
  flow->getName()->print();
#endif

  // Make sure this is a block we care about
  Block* block = findBlock(flow);
  FLN_ELAB_ASSERT(block, flow);
  if (limitBlocks->find(block) == limitBlocks->end()) {
    return NULL;
  }

  // Visit the fanin for this block. If the block is modified, we
  // re-start the traversal. The outer loop is responsible for
  // restarting the traversal on splits to this block. Note that a
  // split may cause the block for this flow node to change so make
  // sure we get the block below.
  flow->putIsInCyclePath(true);
  FLNodeElab* breakPoint = NULL;
  bool isModified = false;
  do
  {
    // Get the block for this flow node and mark that we are traversing it
    Block* block = findBlock(flow);
    FLN_ELAB_ASSERT(block,flow);
    block->pushBusy();
#ifdef DEBUG_FALSE_CYCLES
    UtIO::cout() << "- Block (id = " << block->getID() << ") ";
    flow->getName()->print();
#endif

    // Visit all the nodes in this block
    FLNodeElabVector nodes;
    for (Block::NodesLoop l = block->loopNodes(flow, nodes);
	 !l.atEnd() && !block->isModified() && (breakPoint == NULL);
	 ++l)
    {
      FLNodeElab* curFlow = *l;
#ifdef DEBUG_FALSE_CYCLES
      UtIO::cout() << "- Jumping to sibling ";
      curFlow->getName()->print();
#endif

      // Visit the fanin looking for cycles
      FLNodeElab* fanin = NULL;
      SCHMarkDesign::FlowDependencyLoop f;
      for (f = mMarkDesign->loopDependencies(curFlow);
	   !f.atEnd() && (breakPoint == NULL) && !block->isModified();
	   ++f) {
	// Find false cycles
	fanin = *f;
        pushFlow(flowStack, curFlow, flow);
        breakPoint = findFalseCycles(fanin, pass, flowStack, splitCount,
                                     depth+1, limitBlocks);
        popFlow(flowStack);
      }

      // If this block was modified, then clear the breakpoint. We are
      // going to re-start the search because changes in the traversal
      // below may have messed things up.
      if (block->isModified())
	breakPoint = NULL;

      // If we found a cycle, check if this is the spot it should be
      // broken. This happens when breakPoint is not NULL
      if (flow == breakPoint)
      {
	// It is the spot, break it and continue visiting the drivers
	// again. The block is modified so we have to restart
        //
        // Note that the act of splitting creates new blocks so we
        // have to add it to our limit set. So it gets passed into the
        // breakBlock function as the place to put new blocks.
#ifdef DEBUG_FALSE_CYCLES
	UtIO::cout() << "Breaking at flow ";
        flow->getName()->print();
#endif
	breakBlock(block, curFlow, flow, fanin, limitBlocks);
	breakPoint = NULL;
	++(*splitCount);
      }
    } // for

    // Get the modified flag so that we can see if we should
    // restart. We have to do this before popping the busy flag since
    // we lose the data. And we have to pop because the block may have
    // changed (splitting changes which block some flow nodes are in)
    isModified = block->isModified();
    block->popBusy();

  } while (isModified && (breakPoint == NULL));
  flow->putIsInCyclePath(false);

#ifdef DEBUG_FALSE_CYCLES
  UtIO::cout() << "Leaving flow (depth = " << depth << ") ";
  flow->getName()->print();
  if (breakPoint != NULL) {
    UtIO::cout() << "* Breakpoint is ";
    breakPoint->getName()->print();
  }
#endif

  // If we found a cyle we have to return to our caller to try
  // again. We can't mark this flow node done yet.
  if (breakPoint == NULL)
    // We are done with this flow node
    flow->putSchedulePass(pass);
  return breakPoint;
} // SCHCycleDetection::findFalseCycles

void SCHCycleDetection::breakBlock(Block* block, FLNodeElab* flow,
				   FLNodeElab* startFlow, FLNodeElab* fanin,
                                   BlockSet* newBlocks)
{
#ifdef DEBUG_FALSE_CYCLES
  UtIO::cout() << "* Breaking block " << block->getID() << "\n";
#endif

  // Find all the flow nodes that have the same fanin and put them
  // into a separate block. This is because if they have the same
  // fanin, then they have the same false cycle.
  FLNodeElabSet nodes;
  bool flowFound = false;
  UInt32 splitCount = 0;
  UInt32 totalCount = 0;
  for (Block::NodesLoop l = block->loopNodes(); !l.atEnd(); ++l)
  {
    // Loop the fanins looking for our fanin. We use a loop so that we
    // avoid issues caused by some fanin not being in the set (see
    // cycle detection and conditional flow).
    FLNodeElab* curFlow = *l;
    bool found = false;
    SCHMarkDesign::FlowDependencyLoop f;
    for (f = mMarkDesign->loopDependencies(curFlow); !f.atEnd() && !found; ++f)
    {
      FLNodeElab* curFanin = *f;
      found = (curFanin == fanin);
    }

    // We are splitting flow from startFlow so make sure they don't
    // have the same fanin. If so the start flow must point to itself
    if (found && (curFlow != startFlow))
    {
      nodes.insert(curFlow);
      ++splitCount;

      // Make sure that the flow we want to split was found. We do
      // this by looking for it here and asserting below.
      flowFound |= (curFlow == flow);

#ifdef DEBUG_FALSE_CYCLES
      UtIO::cout() << "- Split Flow: ";
      curFlow->getName()->print();
#endif
    }
#ifdef DEBUG_FALSE_CYCLES
    else {
      UtIO::cout() << "- Unsplit Flow: ";
      curFlow->getName()->print();
    }
#endif

    ++totalCount;
  }

  // Make sure we are not making a mess here
  INFO_ASSERT(flowFound && (splitCount > 0) && (splitCount < totalCount),
              "False cycle detection splitting is inconsistent");

  // Split the block
  splitBlock(block, &nodes, newBlocks);
} // void SCHCycleDetection::breakBlock

void SCHCycleDetection::splitFalseCycleBlocks()
{
  SCHBlockSplit blockSplit(mUtil, mMarkDesign, mScheduleData);
  typedef UtMap<SCHUseDefHierPair, SCHBlockSplit::BlockInstanceGroups>
    SplitBlocks;
  SplitBlocks blocks;

  // Gather the split blocks
  for (Blocks::iterator p = mBlocks->begin(); p != mBlocks->end(); ++p)
  {
    Block* block = *p;

    if (block->isSplit())
    {
      // Get the set of nodes
      FLNodeElabVector nodes;
      FLNodeElab* flow = NULL;
      for (Block::NodesLoop l = block->loopNodes(); !l.atEnd(); ++l)
      {
	flow = *l;
	nodes.push_back(flow);
      }

      // Find the spot to store it
      SCHUseDefHierPair udhp(flow->getUseDefNode(), flow->getHier());
      SCHBlockSplit::BlockInstanceGroups& groups = blocks[udhp];

      // Add this set of nodes
      groups.push_back(nodes);
    }
  } // for

  // Go through the blocks and mark them
  for (SplitBlocks::iterator p = blocks.begin(); p != blocks.end(); ++p)
  {
    SCHBlockSplit::BlockInstanceGroups& groups = p->second;
    blockSplit.markSplitBlock(&groups);
  }

  // Split the blocks
  FLNodeElabSet newFlows;
  blockSplit.splitBlocks(&newFlows);
  if (!newFlows.empty())
  {
    FLNodeElab* unexpectedFlow = *(newFlows.begin());
    FLN_ELAB_ASSERT(newFlows.empty(), unexpectedFlow);
  }
} // void SCHCycleDetection::splitFalseCycleBlocks

void SCHCycleDetection::freeBlocks()
{
  for (Blocks::iterator p = mBlocks->begin(); p != mBlocks->end(); ++p)
  {
    Block* block = *p;
    delete block;
  }
  mBlocks->clear();
  mFlowToBlockMap->clear();
}

void
SCHCycleDetection::markSplitFlow(FLNodeElab* flow)
{
  // We Can't handle splitting sequentials with clock or priority
  // blocks. Give up for now. When we get rid of the clock/priority
  // pointers, this can be re-enabled.
  if (SCHUtil::isSequential(flow)) {
    NUUseDefNode* useDef = flow->getUseDefNode();
    if ((SCHUtil::getClockBlock(useDef) != NULL) ||
        (SCHUtil::getPriorityBlock(useDef) != NULL)) {
      return;
    }
  }

  mSplitNodes->insert(flow);
}

bool
SCHCycleDetection::isSplitFlow(FLNodeElab* flow)
{
  return mSplitNodes->find(flow) != mSplitNodes->end();
}

bool
SCHCycleDetection::splitBlocks()
{
  SCHBlockSplit blockSplit(mUtil, mMarkDesign, mScheduleData);

  // Add the data to the block split sub class
  FLNodeElabSetIter i;
  for (i = mSplitNodes->begin(); i != mSplitNodes->end(); ++i)
  {
    FLNodeElab* flow = *i;
    blockSplit.markSplitNode(flow);
  }
  blockSplit.convertNodesToBlocks();

  // Now that the information was added, do the work to split the blocks
  FLNodeElabSet newFlows;
  bool ok = blockSplit.splitBlocks(&newFlows);
  if (!newFlows.empty())
  {
    FLNodeElab* unexpectedFlow = *(newFlows.begin());
    FLN_ELAB_ASSERT(newFlows.empty(), unexpectedFlow);
  }

  // Free the memory we don't need anymore
  AllocatedMemory::iterator m;
  for (m = mAllocatedMemory.begin(); m != mAllocatedMemory.end(); ++m)
  {
    delete *m;
  }
  mAllocatedMemory.clear();
  mSplitNodes->clear();
  return ok;
} // SCHCycleDetection::splitBlocks

// update output ports that are in cycles
void SCHCycleDetection::updateOutputPortsInCycle(NUCycle* cycle)
{
  for (NUCycle::FlowLoop p = cycle->loopFlows(); !p.atEnd(); ++p)
  {
    FLNodeElab* iflow = *p;
    FLNodeElab* cycleFlow = mUtil->getAcyclicNode(iflow);
    FLN_ELAB_ASSERT(cycleFlow, iflow);
    INFO_ASSERT(cycleFlow != iflow, "cycle contains acyclic flow node");
    FLN_ELAB_ASSERT(mUtil->isInCycle(iflow), iflow);
    FLN_ELAB_ASSERT(cycleFlow->getCycle() == cycle, cycleFlow);
    if (mMarkDesign->removeOutputPort(iflow))
      mMarkDesign->addOutputPort(cycleFlow);
  }
} // SCHCycleDetection::updateOutputPortsInCycle

SCHCycleDetection::Block*
SCHCycleDetection::newBlock(FLNodeElab* flow, 
                            const SCHBlockFlowNodes& blockFlowNodes)
{
  // We build the block for all the nodes in this block
  FLN_ELAB_ASSERT(!SCHUtil::isSequential(flow), flow);
  FLNodeElabVector nodes;
  blockFlowNodes.getFlows(flow, &nodes, &FLNodeElab::isLive);
  if (nodes.empty())
    return NULL; // The flow was dead
  Block* block = new Block(nodes, ++mNumBlocks);

  // Insert the block in our data structures
  mBlocks->push_back(block);
  for (FLNodeElabVectorIter i = nodes.begin(); i != nodes.end(); ++i)
  {
    FLNodeElab* curFlow = *i;
    FLN_ELAB_ASSERT(mFlowToBlockMap->find(curFlow) == mFlowToBlockMap->end(), curFlow);
    mFlowToBlockMap->insert(FlowToBlockMap::value_type(curFlow, block));
  }

  return block;
}

SCHCycleDetection::Block* SCHCycleDetection::findBlock(FLNodeElab* flow)
{
  FlowToBlockMap::iterator pos = mFlowToBlockMap->find(flow);
  if (pos == mFlowToBlockMap->end())
    return NULL;
  else
    return pos->second;
}

void
SCHCycleDetection::splitBlock(Block* block, FLNodeElabSet* splitNodes,
                              BlockSet* newBlocks)
{
  // Remove the flow nodes form the original block
  block->removeNodes(splitNodes);

  // Create a new block with the new nodes
  FLNodeElabVector nodes;
  for (FLNodeElabSetIter i = splitNodes->begin(); i != splitNodes->end(); ++i)
  {
    FLNodeElab* curFlow = *i;
    nodes.push_back(curFlow);
  }
  Block* splitBlock = new Block(nodes, ++mNumBlocks, true);
  mBlocks->push_back(splitBlock);
  newBlocks->insert(splitBlock);

  // Update our map
  for (FLNodeElabVectorIter i = nodes.begin(); i != nodes.end(); ++i)
  {
    FLNodeElab* curFlow = *i;
    FlowToBlockMap::iterator pos = mFlowToBlockMap->find(curFlow);
    FLN_ELAB_ASSERT(pos != mFlowToBlockMap->end(), curFlow);
    INFO_ASSERT(pos->second == block, "FlowToBlockMap is corrupt");
    pos->second = splitBlock;
  }
}

SCHCycleDetection::Block::Block(const FLNodeElabVector& nodes, UInt32 id,
                                bool split)
{
  mNodes = new FLNodeElabVector(nodes);
  mCurrentDepth = mDirtyDepth = 0;
  mSplit = split;
  mID = id;
}

SCHCycleDetection::Block::~Block()
{
  delete mNodes;
  INFO_ASSERT(mCurrentDepth == 0,"Block deleted while still busy");
}

SCHCycleDetection::Block::NodesLoop
SCHCycleDetection::Block::loopNodes(FLNodeElab* flow, FLNodeElabVector& nodes)
{
  // Fill the node vector from our nodes with flow first
  INFO_ASSERT(nodes.empty(), "node vector to fill already contains some nodes");
  nodes.push_back(flow);
  for (FLNodeElabVectorIter i = mNodes->begin(); i != mNodes->end(); ++i)
  {
    FLNodeElab* curFlow = *i;
    if (curFlow != flow)
      nodes.push_back(curFlow);
  }

  // Iterator over these nodes
  return NodesLoop(nodes);
}

SCHCycleDetection::Block::NodesLoop
SCHCycleDetection::Block::loopNodes()
{
  return NodesLoop(*mNodes);
}

void SCHCycleDetection::Block::pushBusy()
{
  ++mCurrentDepth;
}

void SCHCycleDetection::Block::popBusy()
{
  --mCurrentDepth;
  mDirtyDepth = std::min (mDirtyDepth, mCurrentDepth);
}

void SCHCycleDetection::Block::clearModified()
{
  if (mDirtyDepth == mCurrentDepth)
    --mDirtyDepth;
}

bool SCHCycleDetection::Block::isModified() const
{
  INFO_ASSERT(mCurrentDepth, "processing block which is not marked busy");
  return (mDirtyDepth == mCurrentDepth);
}

void SCHCycleDetection::Block::removeNodes(FLNodeElabSet* nodes)
{
  // Traverse the list and create a new vector without the passed in
  // nodes. This can be swapped with the stored nodes
  FLNodeElabVector newNodes;
  for (FLNodeElabVectorIter i = mNodes->begin(); i != mNodes->end(); ++i)
  {
    FLNodeElab* flow = *i;
    if (nodes->find(flow) == nodes->end())
      newNodes.push_back(flow);
  }
  mNodes->swap(newNodes);

  // Mark that everything below the current depth is now dirty
  mDirtyDepth = mCurrentDepth;

  mSplit = true;
}

// Go through the design and make sure no real combinational cycles
// have crepts in.
bool SCHCycleDetection::sanityCheck()
{
  // Use the pass counter to mark nodes done
  UInt32 pass = mUtil->bumpSchedulePass();
  
  // Use a vector to keep track of our stack in case we do find an
  // issue
  FLNodeElabVector nodes;

  // Visit all the combinational nodes starting at outputs and sequentials
  bool errorsFound = false;
  for (SCHMarkDesign::SchedStartLoop p(mMarkDesign); !p.atEnd(); ++p)
  {
    FLNodeElab* flow = *p;
    if (!SCHUtil::isSequential(flow))
      errorsFound |= checkCycleSanity(flow, nodes, pass);
    else
    {
      for (Fanin f = mUtil->loopFanin(flow); !f.atEnd(); ++f)
      {
        FLNodeElab* fanin = *f;
        if (flow != fanin)
          errorsFound |= checkCycleSanity(fanin, nodes, pass);
      }
    }
  }

  return !errorsFound;
} // void SCHCycleDetection::sanityCheck

bool SCHCycleDetection::checkCycleSanity(FLNodeElab* flow, FLNodeElabVector& nodes,
					 UInt32 pass)
{
  // Sequentials can't be in a cycle (at least not for this case)
  if (SCHUtil::isSequential(flow))
    return false;

  // Check if this node is done
  if (flow->getSchedulePass() == pass)
    return false;

  // Check if we hit a cycle
  if (flow->anyScratchFlagsSet(IN_LOOP))
  {
    // Print an error about an undetected cycle
    printUndetectedCycle(flow, nodes);
    return true;
  }

  // No cycle found yet, keep looking
  bool errorsFound = false;
  SCHMarkDesign::TrueFaninLoop l;
  flow->setScratchFlags(IN_LOOP);
  nodes.push_back(flow);
  for (l = mMarkDesign->loopTrueFanin(flow); !l.atEnd() && !errorsFound; ++l)
  {
    FLNodeElab* fanin = *l;
    errorsFound |= checkCycleSanity(fanin, nodes, pass);
  }
  nodes.pop_back();
  flow->clearScratchFlags(IN_LOOP);
  flow->putSchedulePass(pass);
  return errorsFound;
}

void SCHCycleDetection::printUndetectedCycle(FLNodeElab* flow,
                                             FLNodeElabVector& nodes)
{
  MsgContext* msgContext = mUtil->getMsgContext();
  msgContext->SchInternalCycleError(flow);

  // Gather the nodes in the cycle. We do this so that we can
  // determine the size of the cycle before printing the nodes.  Note
  // that the input nodes contains the flows in the order they were
  // encountered. Also, it contains the entire stack, not just the
  // cycle nodes. So visit in reverse to find just the cycle nodes.
  FLNodeElabVector cycleNodes;
  bool endFound = false;
  FLNodeElabVectorRevIter i;
  int size = 0;
  for (i = nodes.rbegin(); i != nodes.rend() && !endFound; ++i, ++size)
  {
    FLNodeElab* curFlow = *i;
    endFound = (curFlow == flow);
    cycleNodes.push_back(curFlow);
  }

  // Print the nodes (it is in reverse order because we want them in
  // the order they flow which is the opposite of how we gathered
  // them above)
  int index = 1;
  for (i = cycleNodes.rbegin(); i != cycleNodes.rend(); ++i, ++index)
  {
    FLNodeElab* curFlow = *i;
    msgContext->SCHCycleNode(curFlow, index, size);
  }
} // void SCHCycleDetection::printUndetectedCycle

bool SCHCycleDetection::inlineCycleTasks(SCHGraphList* cycles)
{
  // Gather all the always blocks in a cycle
  NUAlwaysBlockSet alwaysBlocks;
  for (SCHGraphList::iterator g = cycles->begin(); g != cycles->end(); ++g) {
    DependencyGraph* cycle = *g;
    for (Iter<GraphNode*> n = cycle->nodes(); !n.atEnd(); ++n) {
      GraphNode* graphNode = *n;
      DependencyGraph::Node* node = cycle->castNode(graphNode);
      FLNodeElab* flowElab = node->getData();
      NUUseDefNode* useDef = flowElab->getUseDefNode();
      NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
      if (always != NULL) {
        alwaysBlocks.insert(always);
      }
    }
  }

  // Inline the tasks
  bool taskInlined = false;
  ModuleBlockMap newModuleBlocks;
  TFRewrite tf_rewrite(mUtil->getNetRefFactory(), mUtil->getMsgContext(),
                       mUtil->getSymbolTable()->getAtomicCache(), 
                       false, false, false,
                       mUtil->getIODB(), mUtil->getArgs());
  for (NUAlwaysBlockSetLoop l(alwaysBlocks); !l.atEnd(); ++l) {
    NUAlwaysBlock* always = *l;
    if (tf_rewrite.alwaysBlock(always)) {
      taskInlined = true;
      NUModule* module = always->findParentModule();
      newModuleBlocks[module].insert(always);
    }
  }

  // Create UD, flow and elaborated flow
  if (taskInlined) {
    UtSet<const NUAlwaysBlock*> emptyBlockSet;
    recreateDesignData(emptyBlockSet, newModuleBlocks);
  }
  return taskInlined;
} // void SCHCycleDetection::inlineCycleTasks

SCHCycleDetection::BlockGraph* SCHCycleDetection::createBlockGraph()
{
  // use the Graph builder since it checks for duplicates
  GraphBuilder<BlockGraph> builder;

  // Walk the entire design looking for combo blocks. We can just add
  // all the edges because the graph builder will create all the nodes
  // for us.
  for (SCHMarkDesign::DesignIter i(mMarkDesign, false); !i.atEnd(); ++i) {
    FLNodeElab* flowElab = *i;
    if (!SCHUtil::isSequential(flowElab)) {
      // Add its fanin edges
      Block* fanoutBlock = findBlock(flowElab);
      FLN_ELAB_ASSERT(fanoutBlock, flowElab);
      SCHMarkDesign::FlowDependencyLoop f;
      for (f = mMarkDesign->loopDependencies(flowElab); !f.atEnd(); ++f) {
        // Add this edge if it is a combo block
        FLNodeElab* faninElab = *f;
        if (!SCHUtil::isSequential(faninElab)) {
          Block* faninBlock = findBlock(faninElab);
          FLN_ELAB_ASSERT(faninBlock, faninElab);
          builder.addEdgeIfUnique(fanoutBlock, faninBlock, NULL);
        }
      }
    }
  }
  BlockGraph* graph = builder.getGraph();
  return graph;
} // SCHCycleDetection::BlockGraph* SCHCycleDetection::createBlockGraph
        
