// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CLKANALALIAS_H_
#define __CLKANALALIAS_H_

struct ClockAlias;

//! ClkAnalAlias class, processing related to making clock aliases
/*! merge net and flow data structures of all clocks that are marked
 *  to be equivalent.  No checking is done here.
 */
class ClkAnalAlias
{
private:
  typedef UtMap<BDD, SCHClock*> ClockMap;

public:
  //! constructor
  ClkAnalAlias(SCHClkAnal* clkAnal, NUDesign* design,  BDDContext* bddContext, ClockMap & clocks, SCHUtil* util);

  //! destructor
  ~ClkAnalAlias();

  //! perform the aliasing processing
  void doAliasing( );

private:
  //! Hide copy and assign constructors.
  ClkAnalAlias(const ClkAnalAlias&);
  ClkAnalAlias& operator=(const ClkAnalAlias&);

  //! returns true if net is master of a set of clock aliases
  bool isMaster(NUNetElab* net);

  //! returns true if fanout and driver have a nesting relationship
  bool isNestedPair(FLNodeElab* fanout, FLNodeElab* driver);

  //! make aliasNet an alias of master, transfer z flag 
  void aliasClockNets(NUNetElab* master, NUNetElab* aliasNet);

  //! perform sanity check before trying any clock aliasing
  void preClkAliasSanityCheck();

  //! initial pass to find the Alias candidates
  void findAliasCandidates();

  //! second pass to find find alias candidates and their fanin/fanout relationships
  void findAliasFanoutInfo();

  //! make the aliases, and delete dead flows
  void makeAliasSubstitution();

  typedef UtHashMap<NUNetElab*,STAliasedLeafNode*> MasterStorage;
  //! keep track of master storage for clockAlias.
  void logMasterStorage(ClockAlias* clockAlias);

  //! remove any clocks that are not unreachable and mark live flows
  void pruneUnreachableClocksAndMarkLiveFlow();

  //! remove any flows that are dead because they were used to complete clocks that were aliased away
  void pruneDeadClockFlows();

  //! Sanitize the merged alias rings so only one node has storage
  void cleanupAliasRings();
  
  //! a scratch flag used internally to determine if flow is live.
  static const UInt8 mIsLive = 1;

  //! track the location that will be used for the master of aliases
  MasterStorage mMasterStorage;

  //! pointer back to SCHClkAnal structures
  SCHClkAnal * mClkAnal;


  //! the netElabs (and their associated ClockAlias) that are considered for aliasing
  typedef UtHashMap<NUNetElab*,ClockAlias*> AliasMap;
  AliasMap mAliasMap;

  //! the design we are working on 
  NUDesign* mDesign;
  //! if true then spew much information
  bool mVerbose;

  ClockMap & mClocks;
  BDDContext* mBDDContext;
  SCHUtil* mUtil;

}; // class ClkAnalAlias

#endif // __CLKANALALIAS_H_
