// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file

  A simple cycle-dumping class to write cycles in a a format that is
  useful in the field.
*/

#ifndef __CYCLE_DUMPER_H__
#define __CYCLE_DUMPER_H__

#include "util/CarbonTypes.h"
#include "util/Graph.h"
#include "util/GraphDumper.h"

#include "CycleGraph.h"

class UtString;

/*! The CycleDumper is used to write cyclic dependency graphs in a format
 *  that is useful in the field.
 */
class CycleDumper : public UtGraphDumper
{
public:
  //! constructor
  CycleDumper(UtOStream& os);

  //! destructor
  virtual ~CycleDumper();

  //! Write out a key describing the dependency type codes used in the dump
  void printKey();

  //! Write out a description of the cyclic graph
  void dumpCycle(DependencyGraph* graph);

protected:
  //! Overloaded function to compose a string for an edge
  virtual void edgeString(Graph*, GraphNode*, GraphEdge*, UtString* str);

  //! Overloaded function to compose a string for a node
  virtual void nodeString(Graph*, GraphNode*, UtString* str);
  
private:
  UtOStream& mOut;    //!< The output stream to dump to
  UInt32 mCycleCount; //!< The number of cycles dumped so far
};

#endif // __CYCLE_DUMPER_H__
