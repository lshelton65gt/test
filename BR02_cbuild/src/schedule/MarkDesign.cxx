// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/CarbonContext.h"

#include "schedule/Schedule.h"

#include "flow/FLNodeElab.h"
#include "flow/FLFactory.h"
#include "flow/FLIter.h"

#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUCycle.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUBlock.h"

#include "iodb/IODBNucleus.h"
#include "iodb/CbuildShellDB.h"
#include "iodb/ScheduleFactory.h"

#include "localflow/ClockAndDriver.h"
#include "localflow/ElabStateTest.h"

#include "compiler_driver/CarbonDBWrite.h"

#include "util/CarbonTypeUtil.h"
#include "util/DynBitVector.h"
#include "util/UtIOStream.h"
#include "util/CbuildMsgContext.h"
#include "util/Stats.h"

#include "UseDefHierPair.h"
#include "Util.h"
#include "ClkAnal.h"
#include "MarkDesign.h"
#include "InputNets.h"
#include "BlockFlowNodes.h"

/*!
  \file

  This file contains routines to mark the design. The main routine is
  mark(). There are also some exported routines to modify the output
  port list (used by cycle detection and breaking).
*/


// The following are the set of exported routines

//! Class to handle alias rings as a unit.
/*!
  Used for marking constants
*/
class SCHMarkDesign::AliasRingClosure
{
public:
  //! Adds leaf to visited set and returns true if not seen before
  bool visited(STAliasedLeafNode* leaf)
  {
    return ! mVisited.insertWithCheck(leaf->getStorage());
  }

private:
  typedef UtHashSet<STAliasedLeafNode*> LeafSet;
  LeafSet mVisited;
};

SCHMarkDesign::SCHMarkDesign(SCHSchedule* sched, SCHUtil* util,
                             SCHScheduleFactory* factory)
{
  mSched = sched;
  mUtil = util;
  mDesign = NULL;
  mScheduleFactory = factory;

  // We start off with invalid output ports and sequential nodes
  mOutputPortsValid = false;
  mSequentialNodesValid = false;
  mClocksValid = false;

  // Allocate space for the various class members
  mOutputPorts = new UtHashSet<FLNodeElab*>;
  mSequentialNodes = new FLNodeElabVector;
  mClocks = new SCHInputNets;
  mClockInverts = new SCHInputNets;
  mUnelabPrimaryClks = new ConstNUNetOrderedSet;
  mAsyncResets = new SCHSchedule::AsyncResetMap;
  mCycleNodes = new OrderedCycleSet;
  mFanoutMap = new FanoutMap;
  mTrueFaninCache = new TrueFaninCache;
  mTrueFaninCycleCache = new TrueFaninCycleCache;
  mMultiStrengthNets = new MultiStrengthNets;
  mMarkLiveNodesDone = false;
  mInputNetsFactory = new SCHInputNetsFactory;
  mDepositNets = new DepositNets;
  mFrequentDepositNets = new DepositNets;
  mDepositNetsValid = false;
  mFrequentDepositNetsValid = false;
  mLevelFaninAsyncs = new LevelFaninAsyncs;
  mLevelFaninAsyncsValid = false;
  mOutputsTimingMask = NULL;
  mInputsTimingMask = NULL;
  mFlowNetSets = NULL;
  mBlockToHasCModelMap = new BlockToHasCModelMap;
  mWritableFanouts = new NUNetElabSet;
  mLatchClockUses = new LatchClockUses;
  mInputScheduleNodes = new FLNodeElabVector;
  mAsyncScheduleNodes = new FLNodeElabVector;
  mMasterClockMap = new MasterClockMap;
  mConstantClocks = new ConstantClocks;
  mSignaturesValid = false;
  mFlopAlwaysBlocks = new FlopAlwaysBlocks;
  mElabStateTest = new LFElabStateTest;
}

bool SCHMarkDesign::mark(NUDesign* design, SCHClkAnal* clkAnal, bool doAliasClocks)
{
  // If we are timing passes, start a new timer
  Stats* stats = mUtil->getStats();
  if (stats != NULL)
    stats->pushIntervalTimer();

  // Get the list of outputs.  Store that in the Schedule object
  // as a vector because we are going to mutate it.
  markPorts(design, false);
  if (stats != NULL) stats->printIntervalStatistics("Ports1");

  // Walk the design and find all clocks that are used as data. We
  // need to disable clock equivalence on these clocks because it
  // creates a scenario where we can't correctly schedule the logic.
  // See bug 14095.
  findRedundantUseClocks();

  // Mark all the nets that are part of the data path, and assert that
  // all the nets that are part of the clock path are marked as such.
  // We do this in preparation for clock aliasing, which should not
  // be done when a net also appears as part of the data path.
  bool success = markClockAndDataPaths(clkAnal, doAliasClocks);
  if (stats != NULL) stats->printIntervalStatistics("ClockData");
  if (!success) {
    if (stats != NULL) stats->popIntervalTimer();
    return false;
  }

  // Mark all the live flow-nodes in the design so that we avoid
  // traversing to a dead ones. The thing to be careful of here is
  // that a node may be live but inside an encapsulated combinational
  // cycle.
  //
  // This needs to run before finding sequential nodes because it
  // counts on the live flag to do its work.
  markLiveNodes();
  mMarkLiveNodesDone = true;
  clearTrueFaninCaches();
  if (stats != NULL) stats->printIntervalStatistics("Live1");

  // This block just finds the sequential nodes in the design. Once
  // this is done mSequentialNodes is fully populated.
  //
  // During this pass we can also determine if some clock signals are
  // really asynchronous set/reset signals. If so, we add them to the
  // asynchronous reset list.
  //
  // This code assumes that all flow nodes start with depth = 0, and
  // at the end all the nodes have DEPTH_CLOCKS_DETECTED.
  //
  // Once we have all the sequential nodes, we can get the final
  // answer from clock analysis for all the master clocks. Create a
  // local cache so that we don't have circular dependencies.
  //
  // This pass can actually make some parts of the logic no longer
  // live. This is because here is where we find inactive flops. So
  // re-run the mark live nodes to update the info.
  findDesignSequentialNodes();
  if (stats != NULL) stats->printIntervalStatistics("Flops");
  markPorts(design, true);
  if (stats != NULL) stats->printIntervalStatistics("Ports2");
  markLiveNodes();
  if (stats != NULL) stats->printIntervalStatistics("Live2");

  // After marking the sequential nodes, we may have marked some of
  // the sequential nodes as inactive. This means some of the clocks
  // might have become dead because they no longer feed any live
  // data. So visit the clocks and remove any dead ones. While we are
  // at it we print a warning.
  //
  // We have to do this after findDesignSequentialNodes() and
  // markLiveNodes().
  removeDeadClocks();
  if (stats != NULL) stats->printIntervalStatistics("DeadClks");

  // Validate that all deposit net and force net directives are valid
  validateForceDepositNets();
  if (stats != NULL) stats->printIntervalStatistics("ForceDep");

  // Find all multi-strength elaborated nets in the design. This is
  // used by the scheduler iterators to maintain the appropriate order
  // and scheduling by strength.
  markMultiStrengthNets();
  if (stats != NULL) stats->printIntervalStatistics("MultStr");

  // Mark all the sequential nodes that have to be schedule with
  // multiple edges because they are asynchronous reset blocks with
  // level fanin. This is so that we match simulation. We do this as a
  // pass because all outputs of a block have to get put in the same
  // schedule if any of the output nets have level fanin.
  findLevelFaninResets();
  if (stats != NULL) stats->printIntervalStatistics("NonConstRst");
  if (stats != NULL) stats->popIntervalTimer();
  return true;
} // void SCHMarkDesgin::mark

SCHMarkDesign::OutputPortsLoop SCHMarkDesign::loopOutputPorts()
{
  INFO_ASSERT(mOutputPortsValid,
              "Iterating over the design before output ports have been found");
  mOutputPortsMutated = false;
  return mOutputPorts->loopSorted();
}

SCHMarkDesign::SequentialNodesLoop
SCHMarkDesign::loopSequentialNodes()
{
  INFO_ASSERT(mSequentialNodesValid, "Looping flops before flop discovery pass");
  return SequentialNodesLoop(*mSequentialNodes);
}

void SCHMarkDesign::observeNet(NUNetElab* elab)
{
  for (SCHUtil::DriverLoop l = mUtil->loopNetDrivers(elab); !l.atEnd(); ++l)
  {
    // We used to exclude outputs with no use def. This was because it
    // did not add to scheduling. However, if the output is partially
    // driven, there are some algorithms that will hit this node
    // (those that visit all drivers of a net). So it is better to
    // include it as a start point.
    //
    // We check the strength of the driver. Pull strength drivers
    // don't normally make it to the outside unless the user put the
    // -resolveBidis switch on. (not yet ready)
    FLNodeElab* flow = *l;
    if (!flow->isInactive()
#if 0
	// Not yet ready
	&& ((useDef->getStrength() != eStrPull) ||
	    mUtil->isFlagSet(eResolveBidis))
#endif
      )
      mOutputPorts->insert(flow); 
  }
}

bool SCHMarkDesign::removeOutputPort(FLNodeElab* flow)
{
  UtHashSet<FLNodeElab*>::iterator p = mOutputPorts->find(flow);
  if (p != mOutputPorts->end())
  {
    mOutputPortsMutated = true;
    mOutputPorts->erase(p);
    return true;
  }
  return false;
}

bool SCHMarkDesign::removeSequentialNode(FLNodeElab* flow)
{
  // Find the flow-node specified
  for (size_t i = 0; i < mSequentialNodes->size(); ++i)
  {
    if ((*mSequentialNodes)[i] == flow)
    {
      // Move the last element to the position of flow
      size_t last = mSequentialNodes->size() - 1;
      if (i != last)
        (*mSequentialNodes)[i] = (*mSequentialNodes)[last];
      mSequentialNodes->pop_back();
      return true;
    }
  }
  return false;
}

void SCHMarkDesign::addOutputPort(FLNodeElab* flow)
{
  mOutputPorts->insert(flow);
  mOutputPortsMutated = true;
}


// The following are the set of private routines

NetFlags SCHMarkDesign::getBidiDirection(const NUNet* net)
{
  // First, check with codegen whether or not it considers
  // this a bidi
  NetFlags ret = eNoneNet;
  if (NUNet::isCodegenBidirect(net))
    ret = eBidNet;
  else if (net->isWritten())
    ret = eOutputNet;
  else
    // must be an input
    ret = eInputNet;
  return ret;
}

void SCHMarkDesign::markPorts(NUDesign* design, bool reportCoercion)
{
  // This routine is re-callable
  clearPorts();

  STSymbolTable* symbolTable = mUtil->getSymbolTable();
  STFieldBOM* fieldBOM =  symbolTable->getFieldBOM();
  CbuildSymTabBOM* symTabBOM = dynamic_cast<CbuildSymTabBOM*>(fieldBOM);
  INFO_ASSERT(symTabBOM, "Symbol table has invalid BOM");
  IODBNucleus* iodb = mUtil->getIODB();
  
  // First, need to loop through the top level locals in order to find
  // any split ports. The rhs (concat) must not show up in the db as
  // top level design ports
  NUNetSet tmpSplits;
  for (NUDesign::ModuleLoop topModLoop = design->loopTopLevelModules();
       !topModLoop.atEnd(); ++topModLoop)
  {
    const NUModule* topMod = *topModLoop;
    for (NUNetCLoop topLevelLocals = topMod->loopLocals(); 
         ! topLevelLocals.atEnd(); ++topLevelLocals)
    {
      NUNet* topNet = *topLevelLocals;
      NUModuleElab* topModElab = topMod->lookupElab(symbolTable);
      NUNetElab* topNetElab = topNet->lookupElab(topModElab->getHier(), true);
      if (topNetElab)
      {
        // Grab the leaf nodes for this symbol table node expr
        STAliasedLeafNode* topNode = topNetElab->getSymNode();
        STAliasedLeafNodeVector nodes;
        bool isExpr = symTabBOM->getMappedExprLeafNodes(topNode, &nodes);

        // Get the net flags for the original net from the vector of
        // split nets. We do this for primary ports only. We also get
        // the set of aliases to make sure they don't get declared in
        // the iodb
        NetFlags netFlags;
        NUNetElab::getPrimaryNetsAndFlags(nodes, &netFlags, &tmpSplits);

        // If it is an expression, declare the I/O signals
        if (isExpr)
        {
          if (netFlags == eInputNet)
            iodb->declareIOSignal(topNode, eInputNet);
          else if (netFlags == eOutputNet)
          {
            observeNet(topNetElab);
            iodb->declareIOSignal(topNode, eOutputNet);
          }
          else if (netFlags == eBidNet)
          {
            observeNet(topNetElab);
            iodb->declareIOSignal(topNode, eBidNet);
          }
        } // if
      } // if
    } // for
  } // for
  
  for (NUDesign::PortLoop p = design->loopOutputPorts(); !p.atEnd(); ++p)
  {
    NUNet* net = *p;
    NUModuleElab* mod = net->getScope()->getModule()->lookupElab(symbolTable);
    NUNetElab *elab = net->lookupElab(mod->getHier());

    // Do not add to IODB if this is a replacement net
    if (tmpSplits.find(net) == tmpSplits.end()) {
      iodb->declareIOSignal(elab->getSymNode(), eOutputNet);
    }

    // still add to output ports so the scheduler uses this as a
    // starting point.
    observeNet(elab);            // add the net to mOutputPorts
  }
  for (NUDesign::PortLoop p = design->loopBidPorts(); !p.atEnd(); ++p)
  {
    NUNet* net = *p;
    NUModule* unelabMod = net->getScope()->getModule();
    NUModuleElab* mod = unelabMod->lookupElab(symbolTable);
    NUNetElab *elab = net->lookupElab(mod->getHier());

    NetFlags direction = eBidNet;

    if (mUtil->isPortCoercionOff())
    {
      // prior to port coercion, we relied on codegen to tell us if
      // something was a bidi or not. Mindspeed vreg_program had this
      // problem where a bidi was really an output.
      direction = getBidiDirection(net);
    }
    
      
    // Do not add to IODB if this is a replacement net
    if (tmpSplits.find(net) == tmpSplits.end()) {
      iodb->declareIOSignal(elab->getSymNode(), direction);
      
      if (reportCoercion && (direction != eBidNet))
      {
        const SourceLocator& loc = net->getLoc();
        UtString name(unelabMod->getOriginalName()->str());
        name << "." << net->getName()->str();
        
        switch(direction)
        {
        default:
        case eNoneNet:
        case eBidNet: NU_ASSERT(0, elab); break;
        case eOutputNet:
          mUtil->getMsgContext()->CoerceBidToOutput(&loc, name.c_str());
          break;
        case eInputNet:
          mUtil->getMsgContext()->CoerceBidToInput(&loc, name.c_str());
          break;
        }
      }
    }
    
    // still add to output ports so the scheduler uses this as a
    // starting point.
    if (direction != eInputNet)
      observeNet(elab);            // add the net to mOutputPorts
  }
  for (NUDesign::PortLoop p = design->loopInputPorts(); !p.atEnd(); ++p)
  {
    NUNet* net = *p;
    // Do not add if this is a replacement net
    if (tmpSplits.find(net) == tmpSplits.end())
    {
      NUModule* mod = net->getScope()->getModule();
      NUModuleElab* modElab = mod->lookupElab(symbolTable);
      NUNetElab *elab = net->lookupElab(modElab->getHier(), true);
      
      // An input net could be deleted if it's dead
      if (elab != NULL)
        iodb->declareIOSignal(elab->getSymNode(), eInputNet);
    }
  }

  // now make sure that all nets that were marked observable are
  // included in the scheduler output ports
  for (NUDesign::NameSetLoop p = design->loopObservable(); !p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    NUNetElab* net = NUNetElab::find(node);

    // no one should ever remove a net that was observable
    ST_ASSERT(net != NULL, node);

    observeNet(net); // add the net to mOutputPorts
  }

  mDesign = design;
  mOutputPortsValid = true;
} // void SCHMarkDesign::markPorts

void SCHMarkDesign::clearPorts()
{
  mOutputPorts->clear();
  mOutputPortsValid = false;
}

void
SCHMarkDesign::markDepositableNet(CbuildSymTabBOM* symTabBOM,
                                  NUNetElab* netElab)
{
  // Get the nodes that represent this deposit. It can be more
  // than one if the net got split.
  STAliasedLeafNode* leafNode = netElab->getSymNode();
  STAliasedLeafNodeVector nodes;
  bool isExpr = symTabBOM->getMappedExprLeafNodes(leafNode, &nodes);
  if (!isExpr) {
    nodes.push_back(leafNode);
  }

  // Check if the set of nodes is a primary input or bid, we should
  // not treat these as depositable since they already are.
  //
  // NOTE: They are no longer removed from the IODB sets so that we
  // represent what the user requested in the runtime databases.
  NetFlags netFlags;
  NUNetElab::getPrimaryNetsAndFlags(nodes, &netFlags, NULL);
  if (!NetIsPortType(netFlags, eInputNet) && !NetIsPortType(netFlags, eBidNet)) {
    STAliasedLeafNodeVector::const_iterator i;
    for (i = nodes.begin(); i != nodes.end(); ++i) {
      const STAliasedLeafNode* node = *i;
      const NUNetElab* curNetElab = NUNetElab::find(node);
      if (curNetElab != NULL) {
        mDepositNets->insert(curNetElab->getSymNode()->getStorage());
      }
    }
  }
} // SCHMarkDesign::markDepositableNet

// Find and & mark all the depositable net elabs so that we can look
// them up quickly. This does not do closure on the alias rings
// because it would add too much logic to the deposit schedule.
void SCHMarkDesign::markDepositableNets(NUDesign* design, bool printFrequentNets)
{
  // To make this re-callable, we clear the set initially
  mDepositNets->clear();
  mFrequentDepositNets->clear();
  mFrequentDepositNetsValid = false;

  STSymbolTable* symbolTable = mUtil->getSymbolTable();
  STFieldBOM* fieldBOM =  symbolTable->getFieldBOM();
  CbuildSymTabBOM* symTabBOM = dynamic_cast<CbuildSymTabBOM*>(fieldBOM);
  INFO_ASSERT(symTabBOM, "Symbol table has invalid BOM");

  // Find all the deposit net elabs in the design
  IODBNucleus* iodb = mUtil->getIODB();
  for (IODB::NameSetLoop p = iodb->loopDeposit(); !p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    NUNetElab* netElab = iodb->getNet(node);
    if (netElab != NULL)
      markDepositableNet(symTabBOM, netElab);
  }
  mDepositNetsValid = true;

  // Mark the ports since we need output ports for finding frequently
  // depositable nets
  markPorts(design, false);
  markFrequentDepositableNets(printFrequentNets);
} // void SCHMarkDesign::markDepositableNets

void sDeclarePrimaryClocks(NUDesign::PortLoop p, IODBNucleus* iodb, 
                           SCHClkAnal* clkAnal, STSymbolTable* symbolTable)
{
  for (; !p.atEnd(); ++p)
  {
    NUNet* net = *p;
    NUModuleElab* mod = net->getScope()->getModule()->lookupElab(symbolTable);
    NUNetElab *elab = net->lookupElab(mod->getHier(), true);

    // An input net could be deleted if it's dead
    if ((elab != NULL) && clkAnal->isClockInput(elab))
      iodb->declarePrimaryClock(elab->getSymNode());
  }
}

// Find & mark all the data paths from primary outputs in.  Note
// that we don't use sequential nodes as a stopping point in this
// depth-first traversal, as "clock-ness" and "data-ness" flows through
// state.  Also it seems easier for this one to use an explicit stack
// rather than recursing.
bool SCHMarkDesign::markClockAndDataPaths(SCHClkAnal* clkAnal, bool doAliasClocks)
{
  IODBNucleus* iodb = mUtil->getIODB();
  STSymbolTable* symbolTable = mUtil->getSymbolTable();

  Stats* stats = mUtil->getStats();
  if (stats != NULL) stats->pushIntervalTimer();

  // identify all flows connected to primary outputs
  if ( not clkAnal->walkConnectedFlowsForClkAnal() ){
    if (stats != NULL) stats->popIntervalTimer();
    return false;               // some problem encountered
  }
  if (stats != NULL) stats->printIntervalStatistics("CAIDClkData");


  // Mark driven aliased nets as protected observable; keeps them unelaboratedly live.
  clkAnal->keepMastersAlive();
  if (stats != NULL) stats->printIntervalStatistics("CAAliveMaster");


  if (doAliasClocks) {
    // Alias clocks, recompute the depositable net set because
    // aliasing can change the storage nodes.
    aliasClocks(clkAnal);
    markDepositableNets(mUtil->getDesign(), false);
    if (stats != NULL) stats->printIntervalStatistics("CAaliasClk");
  }

  // The sensitivity analysis above may identify lots of different
  // nets as clock-nets.  But we don't really know the canonical
  // ones until after that traversal is done.  SCHClkAnal keeps
  // track of the clocks by bdd, and will also help us when there
  // is a clock/~clock pair, which one to treat as ~clock, and
  // to then merge ~clock's posedge schedule into clock's negedge
  // schedule, and to merge ~clock's negedge schedule into clock's
  // posedge schedule.  We then get a master canonical set of
  // clocks in mClocks.  
  clkAnal->putDiscoveryComplete(true);
  for (SCHClkAnal::ClockLoop p = clkAnal->loopClocks(); !p.atEnd(); ++p)
  {
    SCHClock* clock = p.getValue();
    if (clock->isEdge())
    {
      NUNetElab* clk = clock->getMaster(), *inv;
      NUNet* unelabClk = clk->getNet();
      if (unelabClk->isPrimaryInput() || unelabClk->isPrimaryBid())
        mUnelabPrimaryClks->insert(const_cast<const NUNet*>(unelabClk));
      else if (isWritable(clk))
      {
        NUNetVector nets;
        clk->getAliasNets(&nets, true);
        for (NUNetVectorIter i = nets.begin(); (i != nets.end()); ++i) {
          const NUNet* net = *i;
          mUnelabPrimaryClks->insert(net);
        }
      }
	
      // Mark the clock
      clk = clkAnal->findClock(unelabClk, clk->getHier(), &inv);
      if (inv != NULL) {
        // The provided clock is an inversion of the master.
        mClockInverts->insert(clk);
        clk = inv;
      }
	
      iodb->declareClock(clk->getSymNode());
      mClocks->insert(clk);
    }
  }
  mClocksValid = true;

  addClocksRequiredByLatches();

  // Record all the clocks we have encountered
  markClocks(clkAnal);

  bool ret;
  ret = !clkAnal->abortScheduler();

  // Mark the primary clock inputs to the chip (which are not necessarily
  // edge-nets, but might feed them combinationally).
  sDeclarePrimaryClocks(mDesign->loopInputPorts(), iodb, clkAnal, symbolTable);
  sDeclarePrimaryClocks(mDesign->loopBidPorts(), iodb, clkAnal, symbolTable);

  // Check if any outputsTiming directives were provided. These mark
  // the timing of output pins
  ret &= gatherIOTiming("outputsTiming", 
                        iodb->loopOutputsTimingEvents(),
                        &mOutputsTimingMask);
  ret &= gatherIOTiming("inputsTiming", 
                        iodb->loopInputsTimingEvents(),
                        &mInputsTimingMask);

  if (stats != NULL) stats->printIntervalStatistics("CAMkClkandDP");
  if (stats != NULL) stats->popIntervalTimer();

  // Can we just delete the ClkAnal object at this point?  It seems
  // that ClkAnal should "know" when its expr cache is no longer needed
  clkAnal->clearExpressionCaches();

  return ret;
  
} // bool SCHMarkDesign::markClockAndDataPaths

bool
SCHMarkDesign::gatherIOTiming(const char* directive,
                              IODBNucleus::ClockEventsLoop l,
                              const SCHScheduleMask** schedMask)
{
  bool ret = true;
  *schedMask = NULL;
  for (; !l.atEnd(); ++l) {
    // Convert the string to an elaborated net
    IODBNucleus::ClockEvent& clockEvent = *l;
    STSymbolTableNode* node = clockEvent.second;
    NUNetElab* netElab = NUNetElab::find(node);
    if (netElab == NULL) {
      UtString str;
      node->composeHelper(&str, true, true, ".", false);
      mUtil->getMsgContext()->SchInvNet(directive, str.c_str());
      ret = false;
    } else {
      // Make sure it is a clock
      const NUNetElab* clkElab = getClockMaster(netElab);
      if (clkElab == NULL) {
        mUtil->getMsgContext()->SchNotClock(netElab, directive);
        ret = false;
      } else {
        // Valid clock, add it to the output mask
        ClockEdge edge = clockEvent.first;
        const SCHEvent* event = mUtil->buildClockEdge(clkElab, edge);
        const SCHScheduleMask* mask = mScheduleFactory->buildMask(event);
        *schedMask = mScheduleFactory->appendMasks(*schedMask, mask);
      }
    }
  } // for
  return ret;
} // SCHMarkDesign::gatherIOTiming

bool SCHMarkDesign::isPrimaryClockInput(const NUNet& net) const
{
  ConstNUNetOrderedSet::const_iterator p = mUnelabPrimaryClks->find(&net);
  return (p != mUnelabPrimaryClks->end());
}

void SCHMarkDesign::findDesignSequentialNodes()
{
  // Use a pass counter to make sure we don't traverse logic more than once
  UInt32 pass = mUtil->bumpSchedulePass();

  mSequentialNodes->clear();

  // Traverse the design's output ports and calculate their clock
  // sensitivity, and insert them in the ready-queue for scheduling.
  for (OutputPortsLoop p = loopOutputPorts(); !p.atEnd(); ++p)
    findSequentialNodes(*p, pass);

  // Traverse the fanin of all the sequential nodes and calculate
  // sensitivity Note that mSequentialNodes will grow inside the loop,
  // so we must not use the vector iterator, and we must not
  // precompute the size.
  for (unsigned int i = 0; i < mSequentialNodes->size(); ++i)
  {
    FLNodeElab* flow = (*mSequentialNodes)[i];
    for (Fanin p = mUtil->loopFanin(flow); !p.atEnd(); ++p) {
      FLNodeElab* fanin = *p;
      if (flow != fanin) {
	findSequentialNodes(fanin, pass);
      }
    }

    // Check the clock analysis equiavlent clock fanin
    const NUNetElab* clk;
    NUUseDefNode* useDef = flow->getUseDefNode();
    getSequentialNodeClock(flow, useDef, NULL, &clk);
    for (SCHUtil::ConstDriverLoop l = mUtil->loopNetDrivers(clk);
         !l.atEnd(); ++l)
    {
      FLNodeElab* fanin = *l;
      if (flow != fanin) {
        findSequentialNodes(fanin, pass);
      }
    }
  } // for

  // Sort the nodes so we get predicable results on various platforms.
  std::sort(mSequentialNodes->begin(), mSequentialNodes->end(),
	    SCHUtil::lessFlow);
  mSequentialNodesValid = true;

  // Make sure no-one bumped the schedule pass while we were running
  INFO_ASSERT(mUtil->equalSchedulePass(pass),
              "Schedule pass counter was incremented in the middle of the flop discovery pass");
} // void SCHMarkDesign::findDesignSequentialNodes

void SCHMarkDesign::markClocks(SCHClkAnal* clkAnal)
{
  // Walk the design and record all the master clocks
  for (DesignIter i(this); !i.atEnd(); ++i) {
    // Record any clocks if this is a flop
    FLNodeElab* flowElab = *i;
    if (SCHUtil::isSequential(flowElab)) {
      // Get all the always block for this flop and walk all of them
      // to transfer the clock information
      const NUAlwaysBlockVector& alwaysBlocks = getFlopAlwaysBlocks(flowElab);
      STBranchNode* hier = flowElab->getHier();
      for (CLoop<NUAlwaysBlockVector> l(alwaysBlocks); !l.atEnd(); ++l) {
        // Get the clock net and scope and call clock analysis
        NUAlwaysBlock* curAlways = *l;
        NUEdgeExpr* edgeExpr = curAlways->getEdgeExpr();
        NUNet* clkNet = edgeExpr->getNet();
        markClock(clkAnal, clkNet, hier, true, false);
      }
    }
  } // while
} // void SCHMarkDesign::markClocks

void
SCHMarkDesign::markClock(SCHClkAnal* clkAnal, NUNet* clkNet, STBranchNode* hier,
                         bool clock, bool reset)
{
  // Call clock analysis on this clock
  NUNetElab* invClk = NULL;
  NUNetElab* clkElab = clkAnal->findClock(clkNet, hier, &invClk, clock, reset);

  // Record this clock if it is different
  NUNetElab* netElab = clkNet->lookupElab(hier);
  MasterClock masterClock;
  bool insert = false;
  if (invClk != NULL) {
    masterClock = MasterClock(invClk, true);
    NU_ASSERT2(isValidClock(invClk), invClk, clkNet);
    insert = true;
  } else if (netElab != clkElab) {
    masterClock = MasterClock(clkElab, false);
    NU_ASSERT2(isValidClock(clkElab), clkElab, clkNet);
    insert = true;
  }
  if (insert) {
    MasterClockMap::iterator pos = mMasterClockMap->find(netElab);
    if (pos == mMasterClockMap->end()) {
      mMasterClockMap->insert(MasterClockMap::value_type(netElab, masterClock));
    } else {
      // Make sure clock analysis doesn't change its mind at this point
      NU_ASSERT2(masterClock == pos->second, netElab, masterClock.first);
    }
  }

  // Record any constant clocks
  int value;
  if (clkAnal->isConstantClock(netElab, &value)) {
    mConstantClocks->insert(ConstantClocks::value_type(netElab, value));
  }
  if ((netElab != clkElab) && clkAnal->isConstantClock(clkElab, &value)) {
    mConstantClocks->insert(ConstantClocks::value_type(clkElab, value));
  }
  if ((invClk != NULL) && clkAnal->isConstantClock(invClk, &value)) {
    mConstantClocks->insert(ConstantClocks::value_type(invClk, value));
  }
} // SCHMarkDesign::markClock

void SCHMarkDesign::aliasClocks(SCHClkAnal* clkAnal)
{
  clkAnal->aliasClocks(mDesign, this);

  // We need to re-figure the top level ports because the
  // flow-nodes for the primary-outputs may have been mutated
  // by the aliasing process.
  markPorts(mDesign, false);
}

void SCHMarkDesign::markLiveNodes()
{
  // Sweep over all the allocated flow nodes and clear the live flag
  FLNodeElab* flow = NULL;
  for (SCHUtil::AllFlowsLoop p = mUtil->loopAllFlowNodes(); p(&flow);) {
    flow->putIsLive(false);
  }

  // Walk the design using the design iterator
  for (DesignIter i(this); !i.atEnd(); ++i) {
    FLNodeElab* flowElab = *i;
    flowElab->putIsLive(true);
  }
}

void SCHMarkDesign::newCycleNode(NUCycle* cycle)
{
  mCycleNodes->insert(cycle);
}

void SCHMarkDesign::deleteCycleNode(NUCycle* cycle)
{
  OrderedCycleSet::iterator pos = mCycleNodes->find(cycle);
  NU_ASSERT(pos != mCycleNodes->end(), cycle);
  mCycleNodes->erase(pos);
  delete cycle;
}

int SCHMarkDesign::numCycles()
{
  return mCycleNodes->size();
}

SCHSchedule::CycleLoop SCHMarkDesign::loopCycles() const
{
  return SCHSchedule::CycleLoop(*mCycleNodes);
}

// This is a recursive routine that walks fanin trees looking for
// sequential nodes and clocks.  It is probably OK to leave this
// recursive for the forseeable future, as it stops at sequential
// nodes.  In a fully latch-based designs this will result in
// recursing over the entire design, at which point we will need to
// use an explicit stack or rethink the algorithm.
void SCHMarkDesign::findSequentialNodes(FLNodeElab* flow, UInt32 pass)
{
  NUUseDefNode* driver = flow->getUseDefNode();

  // Make sure we haven't done this before
  if (flow->getSchedulePass() != pass)
  {
    // Mark that we are done, this avoids cycles
    flow->putSchedulePass(pass);

    if (SCHUtil::isSequential(driver))
    {
      // Check if this flop is inactive. If so, mark it. Note that for
      // it to be inactive, all clock edges must be inactive.
      bool isInactive = true;
      ClockEdge edge;
      const NUNetElab* clkElab;
      getSequentialNodeClock(flow, driver, &edge, &clkElab);
      bool isClockBlock = (mUtil->getClockBlock(driver) == NULL);
      if (!isInactiveClock(clkElab, edge, isClockBlock))
	isInactive = false;

      // Check if this node has asynchronous resets. If so, add them
      // to the async reset set.
      NUAlwaysBlock* priorityBlock = SCHUtil::getPriorityBlock(driver);
      if (priorityBlock != NULL)
      {
	ClockEdge edge;
	const NUNetElab* clkElab;
 	getSequentialNodeClock(flow, priorityBlock, &edge, &clkElab);
        addAsyncReset(clkElab, edge);
      }

      // If this is an inactive flop, then don't add it to the set of
      // sequential nodes
      if (isInactive) {
	flow->putIsInactive(true);
        HierName* clkName = clkElab->getSymNode();
	mUtil->getMsgContext()->SchInactiveFlop(flow, *clkName);
      } else {

        // Add this to the sequential list. Note that our caller is
        // iterating over this list. It is using the integer index
        // method into the vector so that it can handle an increasing
        // size array.
        mSequentialNodes->push_back(flow);
      }
    }
    else
    {
      for (Fanin p = mUtil->loopFanin(flow); !p.atEnd(); ++p)
      {
        FLNodeElab* fanin = *p;
        if (fanin != flow)
          findSequentialNodes(fanin, pass);
      }
    }
  } // if
} // void SCHMarkDesign::findSequentialNodes

void SCHMarkDesign::addAsyncReset(const NUNetElab* clk, ClockEdge edge)
{
  SCHSchedule::AsyncResetMap::iterator p = mAsyncResets->find(clk);
  UInt32* edgeCounts;
  if (p == mAsyncResets->end())
  {
    // Insert it
    edgeCounts = new UInt32[ eClockEdgeStop + 1 ];
    for (int i=eClockEdgeStart; i <= eClockEdgeStop; i++) {
      edgeCounts[i] = 0;
    }
    (*mAsyncResets)[clk] = edgeCounts;
  }
  else
    edgeCounts = p->second;

  // Increment this edge
  edgeCounts[edge]++;
}

SCHMarkDesign::~SCHMarkDesign()
{
  // Get rid of the list of reset information
  for (SCHSchedule::AsyncResetLoop p = loopAsyncResets(); !p.atEnd(); ++p)
    delete[] p.getValue();

  // Delete the cycle nodes
  OrderedCycleSet::iterator p;
  for (p = mCycleNodes->begin(); p != mCycleNodes->end(); ++p)
    delete *p;

  // Delete the space we created
  delete mOutputPorts;
  delete mSequentialNodes;
  delete mClocks;
  delete mClockInverts;
  delete mUnelabPrimaryClks;
  delete mAsyncResets;
  delete mCycleNodes;
  clearFanoutMap();
  delete mFanoutMap;
  clearTrueFaninCaches();
  delete mTrueFaninCache;
  delete mTrueFaninCycleCache;
  delete mMultiStrengthNets;
  delete mInputNetsFactory;
  delete mDepositNets;
  delete mFrequentDepositNets;
  delete mLevelFaninAsyncs;
  if (mFlowNetSets != NULL)
    delete mFlowNetSets;
  delete mBlockToHasCModelMap;
  delete mWritableFanouts;
  for (LatchClockUses::iterator i = mLatchClockUses->begin();
       i != mLatchClockUses->end(); ++i) {
    NUNetSet* clockUses = i->second;
    delete clockUses;
  }
  delete mLatchClockUses;
  delete mInputScheduleNodes;
  delete mAsyncScheduleNodes;
  delete mMasterClockMap;
  delete mConstantClocks;
  clearFlopAlwaysBlocksCache();
  delete mFlopAlwaysBlocks;
  delete mElabStateTest;
}

void 
SCHMarkDesign::clearLiveNodes()
{
  mLiveNodes.clear();
}


void
SCHMarkDesign::rememberLiveNodes()
{
  clearLiveNodes();

  FLNodeElab * flow_elab;
  FLNodeElabFactory * factory = mUtil->getFlowElabFactory();
  for (FLNodeElabFactory::FlowLoop iter = factory->loopFlows();
       iter(&flow_elab);) {
    if (flow_elab->isLive()) {
      mLiveNodes.insert(flow_elab);
    }
  }
}


bool SCHMarkDesign::validateDeletedFlowIsDead(FLNodeElabSet * deleted_flow_elabs) const
{
  bool errorsFound = false;
  UtOStream& screen = UtIO::cout();
  for (FLNodeElabSet::iterator iter = deleted_flow_elabs->begin();
       iter != deleted_flow_elabs->end();
       ++iter) {
    FLNodeElab * flow_elab = (*iter);
    if (mLiveNodes.find(flow_elab)!=mLiveNodes.end()) {
      screen << "Deleted flow was expected to remain live; (class FLNodeElab*)"
		<< flow_elab << UtIO::endl;
      screen << "Flow fanout is:\n";
      dumpFlowFanout(flow_elab,false);
      errorsFound = true;
    }
  }
  return errorsFound;
}


void
SCHMarkDesign::createFanoutMap()
{
  // Delete the memory from the previous calls
  clearFanoutMap();

  // Create a fanout map from the factory
  FLNodeElabFactory * factory = mUtil->getFlowElabFactory();
  FLNodeElab* flowElab;
  for (FLNodeElabFactory::FlowLoop i = factory->loopFlows(); i(&flowElab);) {
    if (SCHUtil::isContDriver(flowElab)) {
      for (FlowDependencyLoop f = loopDependencies(flowElab); !f.atEnd(); ++f) {
        FLNodeElab* faninElab = *f;
        addToFanoutMap(flowElab, faninElab);
      }
    }
  }
} // SCHMarkDesign::dumpFlowFanout

void
SCHMarkDesign::dumpFlowFanout(FLNodeElab* flow, bool dumpLive) const
{
  UtOStream& screen = UtIO::cout();
  for (FLNodeElabVectorCLoop l = loopFlowFanout(flow); !l.atEnd(); ++l) {
    FLNodeElab* fanout = *l;
    screen << "  (class FLNodeElab*)" << fanout << UtIO::endl;
    if (dumpLive)
      screen << "    Live = " << fanout->isLive() << UtIO::endl;
  }
}

FLNodeElabVectorCLoop SCHMarkDesign::loopFlowFanout(FLNodeElab* flow) const
{
  FanoutMap::iterator pos = mFanoutMap->find(flow);
  FLN_ELAB_ASSERT(pos != mFanoutMap->end(), flow);
  FLNodeElabVector* nodes = pos->second;
  return FLNodeElabVectorCLoop(*nodes);
}

void
SCHMarkDesign::addToFanoutMap(FLNodeElab* fanout, FLNodeElab* fanin)
{
  FanoutMap::iterator pos = mFanoutMap->find(fanin);
  FLNodeElabVector* nodes;
  if (pos == mFanoutMap->end())
  {
    // Create a new one
    nodes = new FLNodeElabVector;
    mFanoutMap->insert(FanoutMap::value_type(fanin, nodes));
  }
  else
    nodes = pos->second;

  // mark the fanout
  nodes->push_back(fanout);
}


void
SCHMarkDesign::clearFanoutMap()
{
  FanoutMap::iterator i;
  for (i = mFanoutMap->begin(); i != mFanoutMap->end(); ++i)
    delete i->second;
  mFanoutMap->clear();
}

bool
SCHMarkDesign::validateLiveFlows() const
{
  // Anything in our fanout map should be live. If it isn't print
  // information on its expected fanout
  FanoutMap::iterator i;
  bool errorsFound = false;
  UtOStream& screen = UtIO::cout();
  for (i = mFanoutMap->begin(); i != mFanoutMap->end(); ++i)
  {
    FLNodeElab* flow = i->first;
    if (!flow->isLive())
    {
      screen << "Flow lost its liveness, flow = (class FLNodeElab*)"
		<< flow << UtIO::endl;
      screen << "Flow fanout is:\n";
      dumpFlowFanout(flow, true);
      errorsFound = true;
    }
  }
  return errorsFound;
}

bool SCHMarkDesign::isOutputPort(FLNodeElab* flow) const
{
  return mOutputPorts->find(flow) != mOutputPorts->end();
}

void SCHMarkDesign::markWaveableNets()
{
  IODBNucleus* iodb = mUtil->getIODB();
  STSymbolTable* symTab = mUtil->getSymbolTable();
  STSymbolTable::NodeLoop p;
  AliasRingClosure ringClosure;
  ObserveNodes observeNodes;
  
  // Walk the scheduled design and set the signature for all live
  // elaborated nets and figure out which ones may be sampled. This is
  // needed for the symbol table walk below.
  NetElabSignatures netElabSignatures;
  NUNetElabSet sampledNets;
  markNetElabSignatures(&netElabSignatures, &sampledNets);

  // Walk the symbol table looking for all elaborated nets to mark the
  // constants and unwaveable nets.
  for (p = symTab->getNodeLoop(); !p.atEnd(); ++p) {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf != NULL) {
      NUNetElab* netElab = NUNetElab::find(leaf);
      if (netElab != NULL) {
        // We have an elaborated net. Check if it is actively driven
        // and how many drivers it has. If it has one driver, we get
        // that driver as well for constant analysis.
        bool isLive = false;
        SCHUtil::DriverLoop l;
        int numDrivers = 0;
        FLNodeElab* flowElab = NULL;
        for (l = mUtil->loopNetDrivers(netElab); !l.atEnd(); ++l) {
          flowElab = *l;
          isLive |= flowElab->isLive();
          ++numDrivers;
        }

        // If we are not creating a debug schedule, marked any sampled
        // nets as inaccurate.
        bool accurate = true;
        if (mUtil->isFlagSet(eNoDebugSchedule) && 
            (sampledNets.count(netElab) != 0)) {
          accurate = false;
        }

        // Assume we will set the signature for this netelab if we
        // have one. We may decide to clear it below.
        NetElabSignatures::iterator pos = netElabSignatures.find(netElab);
        const SCHSignature* sig = NULL;
        if (pos != netElabSignatures.end()) {
          sig = pos->second;
        }
        CbuildSymTabBOM::setSCHSignature(leaf, sig);

        // Symbol table nodes should either be marked as constant, as
        // not waveable if they are not live or left alone so that
        // they can be wave dumped.
        //
        // If it is not multiply driven, check if it is a constant and
        // mark it for the waveforms
        NUNet* net = netElab->getNet();
        if ((numDrivers != 1) || !markConstant(netElab, flowElab, &ringClosure) || 
            !accurate) {
          // It is not a constant, check if it is live. This is true
          // if isLive is false and it is not a primary port nor protected.
          //
          // If it isn't live, then this is not waveable.
          if (!accurate || (!isLive && !net->isPrimaryPort())) {
            if (! accurate && 
                ! net->isProtectedObservable(iodb) && 
                net->isProtectedMutable(iodb)) {
              // It isn't accurate but it is depositable. Add it to
              // the unwaveable set 
              iodb->declareInvalidWaveNet(leaf);

            } else if (!isObservable(leaf, iodb, &observeNodes)) {
              // This, in effect, has been optimized away.
              // We mark it by clearing the BOM signature
              CbuildSymTabBOM::setSCHSignature(leaf, NULL);
            }
          }
        }

        // If this is a tie net, then it should be marked as a constant.
        if (net->isPrimaryInput())  {
          const DynBitVector* tie_value = iodb->isTieNet(node);
          if (tie_value != NULL) {
            // Create a temporary constant to set override mask for tie net.
            NUConst* temp_const =
              NUConst::create(false, *tie_value, net->getBitSize(), net->getLoc());
            iodb->assignOverrideMask(temp_const, leaf);
            delete temp_const;
          }
        } // if
      } // if
    } // if
  } // for
} // void SCHMarkDesign::markWaveableNets

// See the header file for details on this function
void
SCHMarkDesign::markNetElabSignatures(NetElabSignatures* netElabSignatures,
                                     NUNetElabSet* sampledNets)
{
  UtStatsInterval stats(mUtil->getStats() != NULL, mUtil->getStats());

  // First walk the design and gather all combinational elaborated
  // flow that is transition scheduled. Those are all accurate. Some
  // may be both sample and transition scheduled (nested flows that
  // feed both top level sample and transition flows). That does not
  // matter since if it is transitioned at all, it is accurate.
  //
  // This is used to determine if a net elab is sample scheduled at
  // all. The way to determine that is, find all the flows that are
  // only sample scheduled and set those net elabs to be sample
  // scheduled. This is done in a later pass.
  FLNodeElabSet transFlows;
  for (DesignIter i(this, mUtil); !i.atEnd(); ++i) {
    FLNodeElab* flowElab = *i;
    if (SCHUtil::isSequential(flowElab) ||
        (!flowElab->isSampled() && !SCHUtil::isInitial(flowElab) && !flowElab->isEncapsulatedCycle())) {
      // Gather all fanin flow until continous drivers as transition flows
      gatherTransitionFlows(flowElab, &transFlows);
    } // if
  } // for
  stats.printIntervalStatistics("MC Trans");

  // Rewalk the design and push the top level signature down to its
  // nested flow and fanin within the block. The marking will be
  // stored on the elaborated net. But is uses the elaborated flow's
  // signature pointer as a cache.
  //
  // Sequentials are always transitioned and have the same signature
  // so the data is always pushed down. Top level combinational flow
  // is marked with their given signature. Nested combinational flow
  // gets its transition mask from its fanin and sample mask from its
  // fanout. So the transition mask is pulled up and the sample mask
  // is pushed down.
  for (DesignIter i(this, mUtil); !i.atEnd(); ++i) {
    FLNodeElab* flowElab = *i;
    const SCHSignature* sig = getSignature(flowElab);
    if (SCHUtil::isSequential(flowElab)) {
      // Push the top level signature down, all flows have the same
      // signature
      const SCHScheduleMask* transMask = sig->getTransitionMask();
      const SCHScheduleMask* sampleMask = sig->getSampleMask();
      pushTimingDownRecurse(flowElab, transMask, sampleMask);

    } else if (!flowElab->isEncapsulatedCycle()) {
      // Pull the transition mask up (from fanin)
      FLNodeElabSet covered;
      pullTransitionUpRecurse(flowElab, &covered);

      // Push the sample mask down (from fanout to fanin)
      const SCHScheduleMask* sampleMask = sig->getSampleMask();
      FLNodeElab* cyclicFlowElab = mUtil->getCycleOriginalNode(flowElab);
      pushTimingDownRecurse(cyclicFlowElab, NULL, sampleMask);
    }
  }
  stats.printIntervalStatistics("MC SetSig");

  // Walk the nested flow again and gather all the summary signature
  // and sampled flag information for all elaborated nets. This uses
  // the signature information stored in each nested elaborated flow
  // as well as the transFlows set constructed above.
  //
  // Note that we gather the signatures into a map to a set of
  // signatures. This is because merging signatures is expensive. It
  // is cheaper to gather the set for any given elaborated net and
  // then do one merge pass per net later.
  NetSigSetMap netSignatureMap;
  for (DesignIter i(this, mUtil); !i.atEnd(); ++i) {
    FLNodeElab* flowElab = *i;
    if (!flowElab->isEncapsulatedCycle()) {
      gatherNetElabTiming(flowElab, transFlows, &netSignatureMap, sampledNets);
    } // if
  } // for
  stats.printIntervalStatistics("MC Get Sig");

  // Adjust the transition mask for all nets that are affected by
  // deposit/force points. The shell needs to see the *input* mask to
  // wave dump them correctly.
  const SCHScheduleMask* inputMask = mScheduleFactory->getInputMask();
  const SCHSignature* inputSig = mScheduleFactory->buildSignature(inputMask, NULL);
  for (NUNetElabLoop l(*mWritableFanouts); !l.atEnd(); ++l) {
    // Only update nets that we consider waveable. They have valid
    // signatures.
    NUNetElab* netElab = *l;
    netSignatureMap[netElab].insert(inputSig);
  }
  stats.printIntervalStatistics("MC Write");

  // Also add signatures for all primary inputs. This is because we
  // want to have signatures for depositable points even if they are
  // not live. This is because depositable implies observable for
  // nets with no drivers (PIs).
  //
  // Note that all we do is add the input mask to the set. This is
  // because some of these inputs may have become dead due to clock
  // aliasing so they have no elaborated flow anymore. But this also
  // gets all dead primary inputs. This is a change from the previous
  // behavior.
  //
  // Also note we do not have to loop bidis because they are live
  // because they are both an input and output.
  IODBNucleus* iodb = mUtil->getIODB();
  for (IODB::NameSetLoop l = iodb->loopInputs(); !l.atEnd(); ++l) {
    STSymbolTableNode* node = *l;
    NUNetElab* netElab = NUNetElab::find(node);
    if (netElab != NULL) {
      netSignatureMap[netElab].insert(inputSig);
    }
  }
  stats.printIntervalStatistics("MC Inp");

  // Walk the signature map and accumulate all the signatures in one
  // shot. This has better performance than merging signature by
  // signature. This data is then accumulated in the map provided by
  // our caller.
  for (LoopMap<NetSigSetMap> l(netSignatureMap); !l.atEnd(); ++l) {
    NUNetElab* netElab = l.getKey();
    const SigSet& sigSet = l.getValue();
    (*netElabSignatures)[netElab] = mScheduleFactory->mergeSignatures(sigSet);
  }
  stats.printIntervalStatistics("MC Store");
} // void SCHMarkDesign::markNetElabSignatures

void
SCHMarkDesign::gatherTransitionFlows(FLNodeElab* flowElab,
                                     FLNodeElabSet* transFlows)
{
  // This is a transition flow
  transFlows->insert(flowElab);

  // We don't need to visit the fanin of top level flow because it is
  // part of a different block. So walk the nested and start from
  // there. This helps with compile speed
  for (FLNodeElabLoop l = flowElab->loopNested(); !l.atEnd(); ++l) {
    FLNodeElab* nestedElab = *l;
    FLNodeElabIter* iter = nestedElab->makeFaninIter(FLIterFlags::eAll,
                                                     FLIterFlags::eNone,
                                                     FLIterFlags::eBranchAtAll,
                                                     true);
    for (; !iter->atEnd(); iter->next()) {
      // If this is a nested flow mark it as a transition node. If
      // we have crossed to another block don't continue. Also don't
      // continue if we have seen this elaborated flow before.
      FLNodeElab* fanin = iter->getCur();
      NUUseDefNode* useDef = fanin->getUseDefNode();
      if ((useDef == NULL) || useDef->isContDriver()) {
        // Bound nodes and continous drivers must be outside this block.
        iter->doNotExpand();

      } else {
        // This must be a nested flow, mark it as a transition. If we
        // had already seen this, don't continue. This can happen when
        // there is internal flow that fanouts out to multiple always
        // block defs.
        if (!transFlows->insertWithCheck(fanin)) {
          iter->doNotExpand();
        }
      }
    }
    delete iter;
  } // for
} // SCHMarkDesign::gatherTransitionFlows


void
SCHMarkDesign::pushTimingDownRecurse(FLNodeElab* flowElab,
                                     const SCHScheduleMask* transMask,
                                     const SCHScheduleMask* sampleMask)
{
  // Start at the nested flow because we don't need to visit the fanin
  // of top level flow. We are just trying to populate information for
  // the nested and internal fanin of nested flow. By looping the
  // nested flow we are avoiding some recursion which helps with
  // performance.
  for (FLNodeElabLoop l = flowElab->loopNested(); !l.atEnd(); ++l) {
    FLNodeElab* nestedElab = *l;

    // Push down and stop at continous drivers or nodes that already
    // have the given timing.
    FLNodeElabIter* iter = nestedElab->makeFaninIter(FLIterFlags::eAll,
                                                     FLIterFlags::eNone,
                                                     FLIterFlags::eBranchAtAll,
                                                     true);
    for (; !iter->atEnd(); iter->next()) {
      // Check for stopping points first (bound nodes, continous
      // drivers)
      FLNodeElab* faninElab = iter->getCur();
      NUUseDefNode* useDef = faninElab->getUseDefNode();
      if ((useDef == NULL) || useDef->isContDriver()) {
        iter->doNotExpand();

      } else {
        // Gather the previous data, assume there are no masks
        const SCHSignature* sig = getSignature(faninElab, true);
        bool changed = false;

        // if the signature is NULL, apply it as is
        if (sig == NULL) {
          sig = mScheduleFactory->buildSignature(transMask, sampleMask);
          changed = true;

        } else {
          const SCHScheduleMask* oldTransMask = sig->getTransitionMask();
          const SCHScheduleMask* oldSampleMask = sig->getSampleMask();

          // Apply the transition mask, if one was provided
          const SCHScheduleMask* newTransMask = NULL;
          if (transMask != NULL) {
            newTransMask = mScheduleFactory->appendMasks(oldTransMask, transMask);
            changed |= (transMask != oldTransMask);
          } else {
            // Get this for building the signature if the sample mask changed.
            newTransMask = oldTransMask;
          }

          // Apply the sample mask if one was provided
          const SCHScheduleMask* newSampleMask = NULL;
          if (sampleMask != NULL) {
            newSampleMask = mScheduleFactory->appendMasks(sampleMask, 
                                                          oldSampleMask);
            changed |= (sampleMask != oldSampleMask);
          } else {
            newSampleMask = oldSampleMask;
          }

          // Update the signature if necessary
          if (changed) {
            sig = mScheduleFactory->buildSignature(newTransMask, newSampleMask);
          }
        } // } else

        // If nothing changed, don't proceed, if it did, update the
        // signature
        if (!changed) {
          iter->doNotExpand();
        } else {
          faninElab->putSignature(sig);
        }
      } // } else
    } // for
    delete iter;
  } // for
} // SCHMarkDesign::pushTimingDownRecurse

const SCHScheduleMask*
SCHMarkDesign::pullTransitionUpRecurse(FLNodeElab* flowElab,
                                       FLNodeElabSet* covered)
{
  // Visit the flow's fanin and process them.
  SCHScheduleMaskSet masks;
  for (FLNodeElab::AllFaninLoop l = flowElab->loopAllFanin(); !l.atEnd(); ++l) {
    FLNodeElab* faninElab = *l;
    if (flowElab != faninElab) {
      const SCHScheduleMask* transMask = pullTransitionUp(faninElab, covered);
      if (transMask != NULL) {
        masks.insert(transMask);
      }
    }
  }

  // Visit the flow's nested flow and process them. It can't add any
  // timing because any fanin of a nested flow has to be a fanin of
  // this flow.
  for (FLNodeElabLoop l = flowElab->loopNested(); !l.atEnd(); ++l) {
    FLNodeElab* nestedElab = *l;
    pullTransitionUp(nestedElab, covered);
  }

  // Combine the masks and return the new mask
  if (!masks.empty()) {
    return mScheduleFactory->mergeScheduleMasks(masks);
  } else {
    // If we didn't find any fanin, assume the constant mask. This
    // gets pruned if it is the only mask anyway.
    return mScheduleFactory->getConstantMask();
  }
}

const SCHScheduleMask*
SCHMarkDesign::pullTransitionUp(FLNodeElab* flowElab,
                                FLNodeElabSet* covered)
{
  // If this elaborated flow is already done, return that
  const SCHSignature* sig = getSignature(flowElab, true);
  if (sig != NULL) {
    const SCHScheduleMask* transMask = sig->getTransitionMask();
    FLN_ELAB_ASSERT(transMask != NULL, flowElab);
    return transMask;
  }

  // Check if we have hit this flow before. This indicates a cycle
  // which can occur with for loop statements. In this case we don't
  // care because the timing comes from the fanin from the always
  // block which cannot be a in a cycle. So just return NULL for the
  // timing. The caller will ignore this routine.
  //
  // This code counts on the fact that the fanin for our caller is
  // always a superset of the fanin of this node. This is true if this
  // is a nested flow from our caller or a fanin.
  //
  // Note that this test occurs after the above getSignature code
  // because fanin's outside the block have the timing.
  if (!covered->insertWithCheck(flowElab)) {
    return NULL;
  }

  // This must be a nested/fanin flow that hasn't been done
  // before. Otherwise, we have reached flow that should have had
  // timing computing but has not been done.
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  FLN_ELAB_ASSERT((useDef != NULL) && !useDef->isContDriver(), flowElab);

  // Gather it's fanin/nested flow's transition mask and store it
  const SCHScheduleMask* transMask = pullTransitionUpRecurse(flowElab, covered);
  FLN_ELAB_ASSERT(transMask != NULL, flowElab);
  sig = mScheduleFactory->buildSignature(transMask);
  flowElab->putSignature(sig);

  // Return the computed transition mask for our caller to use
  return transMask;
} // const SCHScheduleMask* SCHMarkDesign::pullTransitionUp

void
SCHMarkDesign::gatherNetElabTiming(FLNodeElab* flowElab,
                                   const FLNodeElabSet& transFlows,
                                   NetSigSetMap* netSignatureMap,
                                   NUNetElabSet* sampledNets)
{
  // Figure out if this flow is sampled or not. This along with the
  // transFlows set is used to determine if any part of an elaborated
  // net is sample scheduled.
  bool isSampled = !SCHUtil::isSequential(flowElab) && flowElab->isSampled();

  // Walk all the nested flow, stop at continous drivers
  FLNodeElabIter* iter = flowElab->makeFaninIter(FLIterFlags::eAll,
                                                 FLIterFlags::eNone,
                                                 FLIterFlags::eBranchAtAll,
                                                 true);
  for (; !iter->atEnd(); iter->next()) {
    // Test whether we hit a stopping point or not. The start node
    // can't be a stopping point.
    FLNodeElab* faninElab = iter->getCur();
    NUUseDefNode* useDef = faninElab->getUseDefNode();
    if ((flowElab != faninElab) &&
        ((useDef == NULL) || useDef->isContDriver())) {
      // Bound nodes and continous drivers must be outside this
      // block. So don't process them or continue.
      iter->doNotExpand();

    } else {
      // If this is a valid net elab, store the information
      NUNetElab* netElab = faninElab->getDefNet();
      if (!netElab->getNet()->isTempBlockLocalNonStatic()) {
        // The signature is a summary so put it in a set
        const SCHSignature* sig = getSignature(faninElab);
        (*netSignatureMap)[netElab].insert(sig);

        // The sampled set includes all nodes where the parent is
        // sampled and we didn't mark this as a transition flow.
        if ((transFlows.count(faninElab) == 0) && isSampled) {
          sampledNets->insert(netElab);
        }
      }
    }
  } // if
  delete iter;
} // SCHMarkDesign::gatherNetElabTiming

bool 
SCHMarkDesign::isObservable(STAliasedLeafNode* leaf, IODBNucleus* iodb,
                            ObserveNodes* observeNodes)
{
  // Check if we have process this before. use the storage net so we
  // don't have to store all the aliases.
  STAliasedLeafNode* storage = leaf->getStorage();
  ObserveNodes::iterator pos = observeNodes->find(storage);
  if (pos != observeNodes->end()) {
    return pos->second;
  }

  // Compute the observability for the entire ring
  bool observable = false;
  for (STAliasedLeafNode::AliasLoop l(leaf); !l.atEnd() && !observable; ++l) {
    STAliasedLeafNode* curLeaf = *l;
    observable = iodb->isObservable(curLeaf);
  }

  // Store the information for the entire ring by using the storage
  observeNodes->insert(ObserveNodes::value_type(storage, observable));
  return observable;
} // SCHMarkDesign::isObservable

bool SCHMarkDesign::markConstant(NUNetElab* netElab, FLNodeElab* flowElab, 
                                 AliasRingClosure* ringClosure)
{
  // Flops can't be constants
  if (SCHUtil::isSequential(flowElab)) {
    return false;
  }

  // Make sure the net is killed, otherwise it can't be a constant
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  if (always != NULL) {
    NUBlock* block = always->getBlock();
    NUNetRefFactory* netRefFactory = mUtil->getNetRefFactory();
    const NUNetRefHdl& netRef = flowElab->getFLNode()->getDefNetRef();
    if (!block->queryBlockingKills(netRef, &NUNetRef::covered, netRefFactory)) {
      // Not killed
      return false;
    }
  }

  // Get the elaborated flow for the assign statement if we can find it
  FLNodeElab* bottomFlow = flowElab;
  while (bottomFlow->hasNestedBlock() && (bottomFlow->numNestedFlows() == 1))
    bottomFlow = bottomFlow->getSingleNested();
  
  // Get the assign statement and figure out if it is a constant
  bool isConst = false;
  useDef = bottomFlow->getUseDefNode();
  if (useDef != NULL) {
    NUAssign* assign = dynamic_cast<NUAssign*>(useDef);
    if (assign) {
      NUConst* rval = assign->getRvalue()->castConst();
      if (rval) {
        // We don't have to worry about Z values here. Mixed constants
        // have been split. Completely undriven constants are handled
        // with the override mask in the iodb.
        
        // hey we got a constant
        NUNet* lvalNet = NULL;
        NULvalue* lval = assign->getLvalue();
        if (lval->isWholeIdentifier())
          lvalNet = lval->getWholeIdentifier();
          
        if (lvalNet && ! lvalNet->isUndriven()) {
          // If this is multiply driven we can't do anything
          // If this net is depositable we can't do anything
          // If this net is a memory, we can't do anything, because the DB can't represent memories as constants.
          if (! netElab->isMultiplyDriven() && !isWritable(netElab) && ! lvalNet->is2DAnything())
          {
            STAliasedLeafNode* lvalStorage = netElab->getSymNode()->getStorage();
            NU_ASSERT(lvalStorage, netElab);
            // see if we already handled this alias ring
            if (! ringClosure->visited(lvalStorage))
            {
              // if this is a constant with x's and z's create an
              // override mask. The shell will just use that. But, in
              // addition, create the constant information as
              // below. That is useful for categorization and
              // debugging.
              
              if (rval->castConstXZ ())
              {
                IODBNucleus* db = mUtil->getIODB();
                db->assignOverrideMask(rval, lvalStorage);
              }
              
              UInt32 numWordsNeeded = 0;
              if (rval->isReal())
                numWordsNeeded = 2;
              else
              {
                DynBitVector value;
                
                rval->getSignedValue(&value);
                numWordsNeeded = value.getUIntArraySize();
                if (numWordsNeeded == 2)
                {
                  UInt64 valtmp;
                  rval->getULL(&valtmp);
                  UInt32 numBits = NUConst::determineOptimalBitSize(valtmp, rval->isSignedResult(), rval->determineBitSize());
                  numWordsNeeded = (numBits + 31)/32;
                }
              }

              if (numWordsNeeded == 1)
                CbuildSymTabBOM::setTypeTag(lvalStorage, CbuildShellDB::eConstValWordId);
              else
                CbuildSymTabBOM::setTypeTag(lvalStorage, CbuildShellDB::eConstValId);
              
              CbuildSymTabBOM* bomMgr = mUtil->getBOMManager();
              bomMgr->putConstantVal(lvalStorage, rval);
              isConst = true;
            } // if
          } // if
        } // if
      } // if
    } // if
  } // if

  return isConst;
} // void SCHMarkDesign::markConstant

void
SCHMarkDesign::removeDeadFlow(const FLNodeElabSet& deletedFlow)
{
  // Clear the flop always block cache just in case
  clearFlopAlwaysBlocksCache();

  // Recompute the ports and live nodes. This will make sure we don't
  // have deleted flow in the ports and figure out if anything became
  // dead.
  markPorts(mDesign, false);
  markLiveNodes();

  // Remove any deleted or dead sequential flows. Make a new flow
  // vector of the flow nodes that are not deleted if we have
  // populated the sequential node vector.
  FLNodeElabVector newNodes;
  if (mSequentialNodesValid) {
    for (SequentialNodesLoop l = loopSequentialNodes(); !l.atEnd(); ++l) {
      FLNodeElab* flow = *l;
      if ((deletedFlow.find(flow) == deletedFlow.end()) && flow->isLive()) {
        newNodes.push_back(flow);
      }
    }

    // Swap the non deleted flow vector with the full vector.
    mSequentialNodes->swap(newNodes);
  }

  // Remove deleted flow from the input nodes vector. These are not
  // the final schedules stored in the SCHScheduleData class.
  newNodes.clear();
  for (FLNodeElabVectorLoop l = loopInputScheduleNodes(); !l.atEnd(); ++l) {
    FLNodeElab* flow = *l;
    if ((deletedFlow.find(flow) == deletedFlow.end()) && flow->isLive()) {
        newNodes.push_back(flow);
    }
  }
  mInputScheduleNodes->swap(newNodes);

  // Same with the async node vector
  newNodes.clear();
  for (FLNodeElabVectorLoop l = loopAsyncScheduleNodes(); !l.atEnd(); ++l) {
    FLNodeElab* flow = *l;
    if ((deletedFlow.find(flow) == deletedFlow.end()) && flow->isLive()) {
        newNodes.push_back(flow);
    }
  }
  mAsyncScheduleNodes->swap(newNodes);

  // Delete any input net sets placed on deleted flow.
  removeDeadFlowInputNets(deletedFlow);

  // Remove any cycles that use deleted flow. Note that we don't need
  // to deal with dead flow here because we are not removing flow. We
  // are just deleting NUCycles that reference all deleted flow. If
  // the cycle really becomes dead the FLNodeElabCycle's will get
  // removed from the schedule.
  if (!mCycleNodes->empty()) {
    OrderedCycleSet liveCycles;
    for (OrderedCycleSet::iterator c = mCycleNodes->begin();
         c != mCycleNodes->end();
         ++c) {
      NUCycle* cycle = *c;
      int liveNodes = 0;
      int deadNodes = 0;
      for (NUCycle::FlowLoop loop = cycle->loopFlows(); !loop.atEnd(); ++loop) {
        FLNodeElab* flow = *loop;
        if ((deletedFlow.find(flow) != deletedFlow.end())) {
          deadNodes++;
        } else {
          liveNodes++;
        }
      }
      // this assert verifies that a cycle cannot contain a mix of live and dead nodes
      NU_ASSERT((liveNodes == 0) || (deadNodes == 0), cycle);
      // if there are live nodes, we add it to the vector of cycles to keep around
      if (liveNodes != 0) {
        liveCycles.insert(cycle);
      } else {
        delete cycle;
      }
    }
    mCycleNodes->swap(liveCycles);
  }

  // The true fanin cache must be invalidated
  clearTrueFaninCaches();
} // SCHMarkDesign::removeDeadFlow

SCHClocksLoop SCHMarkDesign::loopClocks() const
{
  return SCHClocksLoop(*mClocks);
}

SCHClocksLoop SCHMarkDesign::loopClockInverts() const
{
  return SCHClocksLoop(*mClockInverts);
}

SCHClockMastersAndInvertsLoop SCHMarkDesign::loopClockMastersAndInverts() const
{
  return SCHClockMastersAndInvertsLoop(loopClocks(),loopClockInverts());
}

SCHSchedule::AsyncResetLoop SCHMarkDesign::loopAsyncResets() const
{
  return SCHSchedule::AsyncResetLoop(*mAsyncResets);
}

//! Function to create a loop over a flow nodes timing dependency flow nodes
SCHMarkDesign::FlowTimingLoop
SCHMarkDesign::loopTimingDependencies(FLNodeElab* flow,
                                      InternalFlowFlags internalFlow)
{
  FlowTimingLoop loop;

  // For tri-states the timing dependencies are the same for all
  // drivers of the net.
  NUNetElab* netElab = flow->getDefNet();
  if (flow->getUseDefNode()    &&
      (netElab != NULL)        &&
      isMultiStrength(netElab) &&
      not SCHUtil::isInitial(flow)) {
    SCHUtil::DriverLoop l;
    for (l = mUtil->loopNetDrivers(netElab); !l.atEnd(); ++l) {
      FLNodeElab* newFlow = *l;
      if (newFlow->isLive() && !newFlow->isInactive()) {
        Fanin fanin = mUtil->loopFanin(newFlow);
        loop.pushLoop(FLNodeElabLoop::create(fanin));
      }
    }
  } else {
    if (internalFlow == eInternal) {
      TrueFaninLoop trueFanin = loopTrueFanin(flow);
      loop.pushLoop(FLNodeElabLoop::create(trueFanin));
    } else {
      // The normal net just uses its fanin as its dependencies.
      Fanin fanin = mUtil->loopFanin(flow);
      loop.pushLoop(FLNodeElabLoop::create(fanin));
    }
  }

  return loop;
} // SCHMarkDesign::loopTimingDependencies

SCHMarkDesign::TrueFaninLoop SCHMarkDesign::loopTrueFanin(FLNodeElab* flow)
{
  TrueFaninLoop loop;
  FLNodeElabSet* faninSet = gatherTrueFanin(flow);
  loop.pushLoop(SCHIterator::create(faninSet->loopSorted()));
  return loop;
}

FLNodeElabSet*
SCHMarkDesign::gatherTrueFanin(FLNodeElab* flow)
{
  NUCycle* cycle = flow->getCycle();
  if (cycle != NULL)
  {
    // fanin is computed and cached on a per-cycle basis
    return cycleFanin(cycle);
  }

  // This is not an FLNodeElabCycle

  // Check the cache first
  TrueFaninCache::iterator pos = mTrueFaninCache->find(flow);
  if (pos != mTrueFaninCache->end())
    return pos->second;

  // It doesn't exist, create it
  FLNodeElabSet* faninSet = new FLNodeElabSet;
  mTrueFaninCache->insert(TrueFaninCache::value_type(flow, faninSet));
  fillFaninSet(flow, faninSet);

  return faninSet;
} // SCHMarkDesign::gatherTrueFanin

FLNodeElabSet*
SCHMarkDesign::cycleFanin(NUCycle* cycle)
{
  // Check if we have this information in the cache
  TrueFaninCycleCache::iterator pos = mTrueFaninCycleCache->find(cycle);
  if (pos != mTrueFaninCycleCache->end())
    return pos->second;

  // Not in the cache, so compute it and store it
  FLNodeElabSet* faninSet = new FLNodeElabSet;
  mTrueFaninCycleCache->insert(TrueFaninCycleCache::value_type(cycle, faninSet));

  // add fanin from all nodes in the cycle
  FLNodeElabSet tempSet;
  for (NUCycle::FlowLoop loop = cycle->loopFlows(); !loop.atEnd(); ++loop)
  {
    FLNodeElab* flow = *loop;
    fillFaninSet(flow, &tempSet);
  }

  // promote all nodes to the outermost cycle while populating faninSet
  for (FLNodeElabSet::iterator n = tempSet.begin(); n != tempSet.end(); ++n)
  {
    FLNodeElab* flow = *n;
    FLNodeElab* outer = mUtil->getAcyclicNode(flow);
    NUCycle* fcycle = outer->getCycle();
    if (fcycle != cycle) // omit nodes inside this cycle
      faninSet->insert(outer);
  }
  
  return faninSet;
}

void
SCHMarkDesign::fillFaninSet(FLNodeElab* flow, FLNodeElabSet* faninSet)
{
  FLN_ELAB_ASSERT(!flow->isEncapsulatedCycle(), flow);

  // This is the useDefNode for the starting flow node. We use it to determine
  // the outermost flow node of our fanin nodes inside the while loop.
  NUUseDefNode* originalUseDef = flow->getUseDefNode();
  if (originalUseDef == NULL)
    return;
  NU_ASSERT(originalUseDef->isContDriver(), originalUseDef);

  UtSet<FLNodeElab*> covered; // Prevents looping through cycles internal to a block
  UtVector<FLNodeElab*> toDoStack; // Keeps track of fanin for internal flow
  toDoStack.push_back(flow);
  covered.insert(flow);
  while (!toDoStack.empty())
  {
    FLNodeElab* curFlow = toDoStack.back();
    toDoStack.pop_back();

    // Gather all of the nested fanin for this flow node
    FLNodeElabSet* nestedFanin = new FLNodeElabSet;
    gatherNestedFanin(curFlow, nestedFanin);

    // Replace any fanin that defs other nets in the block with their outermost flow node
    for (FLNodeElabSet::iterator n = nestedFanin->begin(); n != nestedFanin->end(); ++n)
    {
      FLNodeElab* fanin = *n;
      if ((fanin == flow) || (fanin == curFlow))
        continue; // ignore self-cycles
      if (SCHUtil::isContDriver(fanin)) {
        if (!mMarkLiveNodesDone || fanin->isLive()) {
          faninSet->insert(fanin); // it must already be an outermost flow node
        }
        continue;  
      }

      // OK, now we have a flow node for an internal def,
      // so find the outermost flow node (it will have the same UseDefNode
      // as our original outermost flow node).
      FLNodeElab* outer = outerFlowNode(fanin, originalUseDef);
      if (outer == NULL)
      {
        // The fanin is not live outside the block, so recurse on its fanin
        if (covered.find(fanin) == covered.end())
        {
          covered.insert(fanin);
          toDoStack.push_back(fanin);
        }
      }
      else if (outer != flow)
      {
        // We have a fanin node that is live outside the block
        FLNodeElab* cycleFlow = mUtil->getAcyclicNode(outer);
        faninSet->insert(cycleFlow);
      }
    }
    delete nestedFanin;
  }
} // SCHMarkDesign::fillFaninSet


void
SCHMarkDesign::gatherNestedFanin(FLNodeElab* flow, FLNodeElabSet* nestedFanin)
{
  FLN_ELAB_ASSERT(!flow->isEncapsulatedCycle(), flow);

  // Add the fanin at the current level
  unsigned int addFlags = FLIterFlags::eOverAll;
  for (Fanin loop = mUtil->loopFanin(flow, eFlowOverCycles, 0, addFlags, false);
       !loop.atEnd(); ++loop)
  {
    FLNodeElab* fanin = *loop;
    nestedFanin->insert(fanin);
  }

  // If there is nested fanin below us, recurse
  if (flow->hasNestedBlock() || flow->hasNestedHier())
  {
    for (FLNodeElabLoop loop = flow->loopNested(); !loop.atEnd(); ++loop)
    {
      FLNodeElab* nested = *loop;
      gatherNestedFanin(nested, nestedFanin);
    }
  }
} // SCHMarkDesign::gatherNestedFanin


FLNodeElab*
SCHMarkDesign::outerFlowNode(FLNodeElab* flow, NUUseDefNode* useDefNode)
{
  // Note, we don't use the SCHUtil::loopNetDriver routine here
  // because we want to compare the UseDefNode. That is because
  // the use def node for a cycle node is an NUCycle. Instead we
  // use loopContinuousDrivers and compare useDefNodes.
  // This bug was found by test/bugs/bug2147.
  // The caller is responsible for converting the flow node we
  // return to the outermost cycle node, if that is what they want.
  NUNetElab* net = flow->getDefNet();
  for (NUNetElab::DriverLoop l = net->loopContinuousDrivers(); !l.atEnd(); ++l)
  {
    FLNodeElab* nextFlow = *l;
    if (SCHUtil::isContDriver(nextFlow) &&                       // top-level node
        (!mMarkLiveNodesDone || nextFlow->isLive()) &&           // live flow
        (nextFlow->getUseDefNode() == useDefNode) &&             // the right block
        ((nextFlow == flow) || (nextFlow->isNested(flow,true)))) // in the right nesting tree
      return nextFlow;
  }

  // this can happen if the def is not live outside of the always block
  return NULL;
}

void
SCHMarkDesign::clearTrueFaninCaches()
{
  for (TrueFaninCache::iterator c = mTrueFaninCache->begin(); c != mTrueFaninCache->end(); ++c)
    delete c->second;
  for (TrueFaninCycleCache::iterator c = mTrueFaninCycleCache->begin(); c != mTrueFaninCycleCache->end(); ++c)
    delete c->second;
  mTrueFaninCache->clear();
  mTrueFaninCycleCache->clear();
}

SCHMarkDesign::FlowDependencyLoop
SCHMarkDesign::loopDependencies(FLNodeElab* flow,
                                InternalFlowFlags internalFlow,
                                FlowCycleMode cycleMode)
{
  // Create the multiloop
  FlowDependencyLoop loop;
  RealDepCmp realDepCmp(flow);

  // Cycles pull in all combinational drivers, so the tri-state
  // dependencies are not needed for cycle flows
  if (!flow->isEncapsulatedCycle())
  {
    // Create the optional tri-state brother fanin loop. We create this
    // first so that it runs last (multi-loop is a stack).
    NUNetElab* netElab = flow->getDefNet();
    NUUseDefNode* useDef = flow->getUseDefNode();
    if ((netElab != NULL) && (useDef != NULL) && isMultiStrength(netElab) && !SCHUtil::isInitial(flow))
    {
      StrengthFilter filter(useDef->getStrength(), flow);
      CycleFunctor functor(cycleMode == eFlowOverCycles ?
                           mUtil->getFlowElabFactory(): 0);
      StrengthFiltered1 filtered1(netElab->loopContinuousDrivers(), filter);
      StrengthFilteredFlow filteredFlow(filtered1, functor);
      RealDepFilter realDepFilter(FLNodeElabLoop::create(filteredFlow),
                                  realDepCmp);
      loop.pushLoop(FLNodeElabLoop::create(realDepFilter));
    }
  }

  // Create the normal fanin loop
  if (internalFlow == eInternal) {
    FLNodeElabSet* faninSet = gatherTrueFanin(flow);
    loop.pushLoop(SCHIterator::create(faninSet->loopSorted()));
  } else {
    Fanin iter = mUtil->loopFanin(flow, cycleMode);
    RealDepFilter realDepFilter(FLNodeElabLoop::create(iter), realDepCmp);
    loop.pushLoop(FLNodeElabLoop::create(realDepFilter));
  }

  return loop;
} // SCHMarkDesign::loopDependencies

void
SCHMarkDesign::markMultiStrengthNets()
{
  // Clear the set so that we can recompute it. This function is
  // re-callable in case we ever change the set of multi-strength
  // nets.
  mMultiStrengthNets->clear();

  // Visit all flow nodes in the factory looking for multi-strength nets
  typedef UtMap<NUNetElab*, Strength> FoundStrength;
  FoundStrength foundStrength;
  FLNodeElab* flow = NULL;
  for (SCHUtil::AllFlowsLoop l = mUtil->loopAllFlowNodes(); l(&flow);)
  {
    // We only have to worry about live nodes with drivers
    NUUseDefNode* useDef = flow->getUseDefNode();
    if (flow->isLive() && (useDef != NULL))
    {
      // If this is the first time we encounter this net, record the
      // driver. Otherwise, check if this strength is different than
      // the recorded strengths. We only have to find two strengths
      // for it to be a multi strength net.
      NUNetElab* netElab = flow->getDefNet();
      Strength strength = useDef->getStrength();
      FoundStrength::iterator pos = foundStrength.find(netElab);
      if (pos == foundStrength.end())
	// Record this strength
	foundStrength.insert(FoundStrength::value_type(netElab, strength));
      else if (strength != pos->second)
	// We found a multi-strength net
	mMultiStrengthNets->insert(netElab);
    }
  } // for
} // SCHMarkDesign::MarkMultiStrengthNets

bool
SCHMarkDesign::isMultiStrength(NUNetElab* netElab)
{
  return mMultiStrengthNets->find(netElab) != mMultiStrengthNets->end();
}

static bool sanityStopHere(FLNodeElab* flow)
{
  UtIO::cout() << "Sanity check failed for (class FLNodeElab*) " << flow
               << UtIO::endl;
  flow->pname(0,1);
  return false;
}

// sanity check for ensuring that all reachable flow is completely connected
// and there are no dangling pointers.   We will iterate over the design
// using several methods to make sure all views are unobstructed.
bool SCHMarkDesign::sanityCheck()
{
  // Make a hash-set of all the FLNodeElab's that have been freed.
  MemoryPoolPtrSet freeNodes;
  bool memSanity = mUtil->isFlagSet(eMemorySanity);
  if (memSanity) {
    mUtil->getFlowElabFactory()->getFreeSet(&freeNodes);

    // First, a full design iteration as performed by clock aliasing
    // and port reduction.  This will hit all the nodes that are
    // inside cycles, but not the encapsulated cycle nodes.  The later
    // traversals will hit the cycle nodes. Since we hit nodes that
    // are not scheduled through this pass, we don't check some of the
    // scheduler data, just the memory.
    NUDesign* design = mUtil->getDesign();
    FLNodeElabSet covered;
    for (FLDesignElabIter iter(design,
                               mUtil->getSymbolTable(),
                               FLIterFlags::eIOOnly,
                               FLIterFlags::eAll,
                               FLIterFlags::eNone, // Stop at nothing
                               FLIterFlags::eBranchAtAll,
                               FLIterFlags::eIterNodeOnce);
         not iter.atEnd();
         iter.next()) {
      FLNodeElab* flowElab = *iter;
      if (covered.insertWithCheck(flowElab)) {
        if (!sanityCheckFlowMemory(flowElab, freeNodes)) {
          return sanityStopHere(*iter);
        }
      }
    }
  } // if

  // Now, do a state-aware analysis, where we start out design
  // output ports and state, and traverse recursively to the next
  // level of state.  This should also check the whole design.
  if (mOutputPortsValid) {
    // Do a scheduler design walk and make sure everything is ok. In
    // this pass we also check liveness and the signature. Note we
    // don't need a covered set becaus DesignIter takes care of that
    // for us.
    for (DesignIter i(this); !i.atEnd(); ++i) {
      // Make sure it has valid scheduler data
      FLNodeElab* flowElab = *i;
      if (memSanity && sanityCheckFlowMemory(flowElab, freeNodes)) {
        return sanityStopHere(flowElab);
      }
      if (!sanityCheckFlowSchedData(flowElab)) {
        return sanityStopHere(flowElab);
      }
    }
  }

  return true;
} // bool SCHMarkDesign::sanityCheck

bool
SCHMarkDesign::sanityCheckFlowMemory(FLNodeElab* flowElab, 
                                     const MemoryPoolPtrSet& freeNodes)
{
  // examine the flow pointer.  As far as the memory allocator is
  // concerned, this is likely to be a chunk of 4096 bytes used
  // by the flow-factory's memory pool.
  size_t size;
  bool isAllocated;
  bool success = false;
  if (!CarbonMem::getPtrInfo(flowElab, &isAllocated, &size)) {
    UtIO::cout() << "flow-node " << flowElab
                 << " is not known to the memory allocator" << UtIO::endl;
  } else if (!isAllocated) {
    UtIO::cout() << "flow-node " << flowElab
                 << " is part of a freed pool" << UtIO::endl;
  } else if (size != 4096) {
    UtIO::cout() << "flow-node " << flowElab
                 << " is part of pool of unexpected size " << size
                 << UtIO::endl;
  } else if (freeNodes.find(flowElab) != freeNodes.end()) {
    UtIO::cout() << "flow-node " << flowElab
                 << " has been destroyed" << UtIO::endl;
  } else {
    success = true;
  }

  if (success) {
    // Validate that our NUNetElab* is allocated as such, and that it
    // points back to this flow.
    NUNetElab* net = flowElab->getDefNet();
    if (net != NULL) {
      success = false;
      if (!CarbonMem::getPtrInfo(net, &isAllocated, &size)) {
        UtIO::cout() << "net " << net
                     << " is not known to the memory allocator" << UtIO::endl;
      } else if (!isAllocated) {
        UtIO::cout() << "net " << net << " has been freed" << UtIO::endl;
      } else if (size != sizeof(NUNetElab)) {
        UtIO::cout() << "net " << net
                     << " is of unexpected size " << size
                     << UtIO::endl;
      } else {
        success = true;
      }
    }
  }
  return success;
}

bool
SCHMarkDesign::sanityCheckFlowSchedData(FLNodeElab* flowElab)
{
  // All reachable flow from this iterator must be a top level
  // flow. But don't check encapsulated cycles because they are not
  // marked top level.
  bool success = true;
  if (!flowElab->isEncapsulatedCycle()) {
    if (!sanityMustBeTopLevel(flowElab)) {
      // Return immediately because the rest of the tests require top
      // level flow, so they are meaningless.
      return false;
    }
  }

  // All top level flow should be live
  if (mMarkLiveNodesDone && !flowElab->isLive()) {
    UtIO::cout() << "flow-node " << flowElab
                 << " is a continous driver but is not marked live"
                 << UtIO::endl;
    success = false;
  }

  // All top level flow should have a valid signature
  if (mSignaturesValid && (getSignature(flowElab, true) == NULL)) {
    // Note, this is a slow routine
    analyzeSignatureProblem(flowElab);
    success = false;
  }

  // Validate that we don't have some schedule ordering bugs. The two
  // rules we check here are:
  //
  // 1. A sample scheduled combinational block cannot fanout to a
  //   transition scheduled combinational block. (Guaranteed by
  //   walking from flops to mark sample logic. As soon as it hits a
  //   node that must be transition scheduled, it stops).
  //
  // 2. A sample scheduled combinational block cannot fanout to a
  //    derived clock logic flop. (Guaranteed by the mustBeTransition
  //    flag put on nodes that feed DCL flops).
  bool sequential = SCHUtil::isSequential(flowElab);
  if (sequential && !flowElab->isInactive()) {
    success &= sanityCheckSequentialFanin(flowElab);
  } else if (!sequential) {
    success &= sanityCheckCombinationalFanin(flowElab);
  }

  // Ensure that only storage nodes are scheduled
  NUNetElab *netElab = flowElab->getDefNet();
  NUNet *storageNet = netElab->getStorageNet();
  if (storageNet == NULL) {
    STAliasedLeafNode *leaf = netElab->getSymNode();
    UtIO::cout() << "flow-node " << flowElab << " has no storage" << UtIO::endl;
    UtIO::cout() << "symtab node:" << UtIO::endl;
    leaf->print();
    UtIO::cout() << "alias ring:" << UtIO::endl;
    leaf->printAliases();
    success = false;
  }

  return success;
} // bool SCHMarkDesign::sanityCheckFlow

bool SCHMarkDesign::sanityCheckSequentialFanin(FLNodeElab* flowElab)
{
  // Make sure any derived clock flops have transition schedule combo
  // blocks on the fanin.
  bool success = true;
  if (flowElab->isDerivedClock() || flowElab->isDerivedClockLogic()) {
    for (Fanin l = mUtil->loopFanin(flowElab); !l.atEnd(); ++l) {
      FLNodeElab* faninElab = *l;
      if (faninElab->isSampled()) {
        printTimingViolation("Sample combo block feeds a derived clock flop",
                             flowElab);
        success = false;
      }
    }
  }

  return success;
}

bool SCHMarkDesign::sanityCheckCombinationalFanin(FLNodeElab* flowElab)
{
  // Can't have a sample combo feed a transition combo
  bool success = true;
  if (!flowElab->isSampled()) {
    for (FlowDependencyLoop l = loopDependencies(flowElab); !l.atEnd(); ++l) {
      FLNodeElab* faninElab = *l;
      if (faninElab->isSampled()) {
        printTimingViolation("Sample combo block feeds a transition combo block",
                             flowElab);
        success = false;
      }
    }
  }
  return success;
}

void
SCHMarkDesign::printTimingViolation(const char* errorStr, FLNodeElab* flowElab)
{
  UtIO::cout() << "Sanity Error: " << errorStr << "\n";
  UtString name;
  NUNetElab* netElab = flowElab->getDefNet();
  netElab->getSymNode()->compose(&name);
  printTraceNet(name, netElab);
}

// Check that the given flow is a continuous driver and report sanity
// failure otherwise
bool SCHMarkDesign::sanityMustBeTopLevel(FLNodeElab* flowElab)
{
  bool success = true;
  NUUseDefNode* node = flowElab->getUseDefNode();
  if ((node != NULL) && !node->isContDriver())
  {
    const char* reason = NULL;
    if (isOutputPort(flowElab)) {
      reason = "output port";
    } else if (SCHUtil::isSequential(flowElab)) {
      reason = "sequential node";
    } else {
      reason = "combinational node";
    }
    UtIO::cout() << "flow-node " << flowElab
                 << " is a " << reason << " which is not at the top level."
                 << UtIO::endl;
    success = false;
  }
  return success;
}

// Check if an elaborated net is a primary input
bool SCHMarkDesign::isPrimaryInput(const NUNetElab* net)
{
  return net->isPrimaryInput() || isFrequentDepositable(net);
}

// Check if an elaborated net is a primary input
bool SCHMarkDesign::isPrimaryBidi(const NUNetElab* net)
{
  return net->isPrimaryBid();
}

// Check if an elaborated net is a primary input
bool SCHMarkDesign::isPrimaryInput(FLNodeElab* flow)
{
  // Make sure it isn't a cycle
  if (isCycle(flow))
    return false;

  // Check if we should have should treat this flow node as a primary
  // input. We should in the following scenarios:
  //
  //  - This is a bound node that represents a PI
  //
  //  - This is a bound node that represents a bi-directional pin
  //
  //  - The user asked us to resolve bidis and this flow is any
  //    driver of the bidi net.
  NUNetElab* net = flow->getDefNet();
  NUUseDefNode* useDef = flow->getUseDefNode();
  return ((net != NULL) &&
	  (isPrimaryInput(net) ||
	   (isPrimaryBidi(net) &&
	    ((useDef == NULL) ||
	     mUtil->isFlagSet(eResolveBidis)))));
}

// Check if an elaborated net is a primary bidi
bool SCHMarkDesign::isPrimaryBidi(FLNodeElab* flow)
{
  // Make sure it isn't a cycle
  if (isCycle(flow))
    return false;

  NUNetElab* net = flow->getDefNet();
  return (net != NULL) && isPrimaryBidi(net);
}

bool SCHMarkDesign::isCycle(FLNodeElab* flow)
{
  return flow->isEncapsulatedCycle();
}

bool SCHMarkDesign::isWritable(FLNodeElab* flow, bool ignoreFrequentDeposits) const
{
  // Check if the netelab for this flow node is depositable or
  // forcible.
  return isWritable(flow->getDefNet(), ignoreFrequentDeposits);
}

bool
SCHMarkDesign::isWritable(const NUNetElab* netElab, bool ignoreFrequentDeposits)
  const
{
  // Check if the netelab is depositable or forcible. Note that the
  // forcible flag is propagated using closure. But the deposit flag
  // is not. So we created a set of netElabs that are depositable
  // early in the scheduler. See markPorts.
  bool writable = false;
  if (netElab->getStorageNet()->isForcible() || isDepositable(netElab)) {
    if (!ignoreFrequentDeposits || !isFrequentDepositable(netElab)) {
      writable = true;
    }
  }
  return writable;
}

bool SCHMarkDesign::isDepositable(const NUNetElab* netElab) const
{
  // Check if the net is depositable. Use isWritable if you want to
  // check both depositable and forcible.
  NU_ASSERT(mDepositNetsValid, netElab);
  const STAliasedLeafNode* node = netElab->getSymNode()->getStorage();
  return mDepositNets->find(node) != mDepositNets->end();
}

bool SCHMarkDesign::isDepositable(FLNodeElab* flow) const
{
  return isDepositable(flow->getDefNet());
}

bool SCHMarkDesign::isFrequentDepositable(const NUNetElab* netElab) const
{
  // Check if the net is depositable. Use isWritable if you want to
  // check both depositable and forcible.
  NU_ASSERT(mFrequentDepositNetsValid, netElab);
  const STAliasedLeafNode* node = netElab->getSymNode()->getStorage();
  DepositNets::iterator pos = mFrequentDepositNets->find(node);
  bool found = pos != mFrequentDepositNets->end();
  if (found) {
    NU_ASSERT(isDepositable(netElab), netElab);
  }
  return found;
}

bool SCHMarkDesign::isClock(const NUNetElab* netElab) const
{
  // Loop the drivers to see if it is a clock. We mark the flows not
  // the net elabs
  bool foundClock = false;
  SCHUtil::ConstDriverLoop p;
  for (p = mUtil->loopNetDrivers(netElab); !p.atEnd() && !foundClock; ++p) {
    FLNodeElab* flowElab = *p;
    foundClock = flowElab->isClock();
  }
  return foundClock;
}

const SCHInputNets* SCHMarkDesign::emptyInputNets()
{
  return mInputNetsFactory->emptyInputNets();
}

const SCHInputNets* SCHMarkDesign::buildInputNets(const NUNetElab* netElab)
{
  return mInputNetsFactory->buildInputNets(netElab);
}

const SCHInputNets*
SCHMarkDesign::appendInputNets(const SCHInputNets* n1, const SCHInputNets* n2)
{
  return mInputNetsFactory->appendInputNets(n1, n2);
}

const SCHInputNets*
SCHMarkDesign::inputNetsDifference(const SCHInputNets* n1,
                                   const SCHInputNets* n2)
{
  return mInputNetsFactory->inputNetsDifference(n1, n2);
}

void
SCHMarkDesign::validateForceDepositNets()
{
  IODBNucleus* iodbNucleus = mUtil->getIODB();
  MsgContext* msgContext = mUtil->getMsgContext();

  // Loop over the deposit nets and check if it has a combinational
  // driver
  UtHashSet<const STAliasedLeafNode*> covered;
  for (DepositNets::SortedLoop p = mDepositNets->loopSorted(); !p.atEnd(); ++p) {
    const STAliasedLeafNode* node = *p;
    const NUNetElab* netElab = NUNetElab::find(node);
    if (netElab != NULL) {
      bool hasCombDrivers = false;
      for (SCHUtil::ConstDriverLoop l = mUtil->loopNetDrivers(netElab);
	   !l.atEnd() && !hasCombDrivers; ++l)
      {
	FLNodeElab* flow = *l;
        if (mUtil->isActiveDriver(flow) && !mUtil->isSequential(flow)) {
	  hasCombDrivers = true;
        }
      }
      if (hasCombDrivers) {
        // Walk the aliases and find the hierarchical names with the
        // deposit directive.
        STAliasedLeafNodeVector nodes;
        iodbNucleus->getDepositableAliases(netElab, &nodes);
        std::sort(nodes.begin(), nodes.end(), HierNameCmp());
        for (STAliasedLeafNodeVector::const_iterator i = nodes.begin();
             i != nodes.end(); ++i) {
          const STAliasedLeafNode* alias = *i;
          if (covered.insertWithCheck(alias)) {
            NUNet* net = NUNet::find(alias);
            UtString buf;
            alias->compose(&buf);
            if (isLatch(netElab)) {
              msgContext->SchLatchDeposit(net, buf.c_str());
            } else {
              msgContext->SchCombDeposit(net, buf.c_str());
            }
          }
        }
      }
    }
  }

  // Do a design walk and make sure that every forced or deposited net
  // elab reaches some logic. We do this by filling a set with the
  // deposit and force nets and removing them when we find them.
  //
  // Start by filling the deposit and force net sets
  NUCNetElabSet depositNets;
  NUCNetElabSet forceNets;
  for (DepositNets::UnsortedLoop p = mDepositNets->loopUnsorted(); !p.atEnd(); ++p)
  {
    const STAliasedLeafNode* node = *p;
    const NUNetElab* netElab = NUNetElab::find(node);
    if (netElab != NULL) {
      if (! mUtil->getBOMManager()->isExprMappedNode(netElab->getSymNode()))
        depositNets.insert(netElab);
    }
  }
  
  for (IODB::NameSetLoop p = iodbNucleus->loopForced(); !p.atEnd(); ++p)
  {
    const STSymbolTableNode* symNode = *p;
    if (! mUtil->getBOMManager()->isExprMappedNode(symNode))
    {
      NUNetElab* netElab = iodbNucleus->getNet(symNode);
      if (netElab != NULL)
        forceNets.insert(netElab);
    }
  }

  // Visit all the live flow in the design and check the fanin for the
  // deposit and force nets.
  UInt32 pass = mUtil->bumpSchedulePass();
  FLNodeElab* flow = NULL;
  for (SCHUtil::AllFlowsLoop l = mUtil->loopAllFlowNodes(); l(&flow);)
  {
    if (flow->isLive())
    {
      // Check its fanin for deposit or force nets
      for (Fanin n = mUtil->loopFanin(flow); !n.atEnd(); ++n)
      {
	FLNodeElab* fanin = *n;
	if (fanin->getSchedulePass() != pass)
	{
	  NUNetElab* netElab = fanin->getDefNet();
	  depositNets.erase(netElab);
	  forceNets.erase(netElab);
	  fanin->putSchedulePass(pass);
	}
      }

      // Since the current flow node is alive, we can remove the def
      // net from the sets.  For example, when an undriven output
      // is depositable or forcible.
      NUNetElab* netElab = flow->getDefNet();
      depositNets.erase(netElab);
      forceNets.erase(netElab);
    }
  }

  // Now visit the force/deposit nets looking for ignored forces and
  // deposits
  for (NUCNetElabSet::SortedLoop l = depositNets.loopSorted(); !l.atEnd(); ++l)
  {
    const NUNetElab* netElab = *l;
    msgContext->SchNoDepositFanout(netElab);
  }
  for (NUCNetElabSet::SortedLoop l = forceNets.loopSorted(); !l.atEnd(); ++l)
  {
    const NUNetElab* netElab = *l;
    msgContext->SchNoForceFanout(netElab);
  }
} // SCHMarkDesign::validateForceDepositNets

bool SCHMarkDesign::isLiveClock( const NUNetElab* clk )
{
  bool isLive = false;
  bool isPI = true;
  SCHUtil::ConstDriverLoop l;
  for (l = mUtil->loopNetDrivers(clk); !l.atEnd(); ++l)
  {
    FLNodeElab* flow = *l;
    isLive |= flow->isLive();
    isPI = false;
  }
  return isLive || isPI;
}

void SCHMarkDesign::removeDeadClocks()
{
  // Visit the clocks and record the ones that are live.
  SCHInputNets liveClocks;
  SCHInputNets deadClocks;
  for (SCHClocksLoop p = loopClocks(); !p.atEnd(); ++p)
  {
    // Check if this clock is live
    const NUNetElab* clk = *p;

    // Put this clock in the right bucket
    if( isLiveClock( clk ) )
      liveClocks.insert(clk);
    else
      deadClocks.insert(clk);
  }

  // If we found any dead clocks, print a warning and update our set
  // of clocks
  if (!deadClocks.empty())
  {
    MsgContext* msgContext = mUtil->getMsgContext();
    for (SCHInputNetsLoop l(deadClocks); !l.atEnd(); ++l)
    {
      const NUNetElab* clk = *l;
      msgContext->SchDeadClock(clk);
    }
    mClocks->swap(liveClocks);

    // Walk the master clock map and remove any edges from clocks to
    // dead clock masters.
    MasterClockMap liveMasters;
    bool deadMastersFound = false;
    for (LoopMap<MasterClockMap> l(*mMasterClockMap); !l.atEnd(); ++l) {
      MasterClock masterClock = l.getValue();
      const NUNetElab* masterElab = masterClock.first;
      if (deadClocks.find(masterElab) == deadClocks.end()) {
        const NUNetElab* netElab = l.getKey();
        liveMasters.insert(MasterClockMap::value_type(netElab, masterClock));
      } else {
        deadMastersFound = true;
      }
    }
    if (deadMastersFound) {
      mMasterClockMap->swap(liveMasters);
    }
  }
}

void SCHMarkDesign::findLevelFaninResets()
{
  // We pre-set this flag so we can add an assert in the find routine
  mLevelFaninAsyncsValid = true;

  // Create the map to get all the flow nodes from an always block
  SCHBlockFlowNodes blockFlowNodes(mUtil);

  // Visit all the sequential nodes to check if they are async resets
  // with level fanin.
  FLNodeElabSet covered;
  for (SequentialNodesLoop s = loopSequentialNodes(); !s.atEnd(); ++s) {
    // Because we process all flows for a block at the same time, use
    // a covered set.
    FLNodeElab* flowElab = *s;
    if (covered.insertWithCheck(flowElab)) {
      // Check if this or any of its brothers has level fanin or is
      // depositable. Note that we do care about self referential
      // statements. They should execute at all sensitivity edges
      FLNodeElabVector nodes;
      blockFlowNodes.getFlows(flowElab, &nodes, &FLNodeElab::isLive);
      bool extraEdges = false;
      for (FLNodeElabVectorCLoop l(nodes); !l.atEnd() && !extraEdges; ++l) {
        FLNodeElab* curFlowElab = *l;
        covered.insert(curFlowElab);
        if (curFlowElab->hasLevelFanin() ||
            isDepositable(curFlowElab->getDefNet())) {
          extraEdges = true;
        }
      }

      // If we should run it with extra edges and we haven't done it
      // before, then remember this flow.
      if (extraEdges && (findLevelFaninAsyncEdgeMask(flowElab) == NULL)) {
        // Check if this is an async reset block with a clock block
        // pointer.
        const SCHScheduleMask* mask = getExtraEdges(flowElab);
        if (mask != NULL) {
          // Add this mask to all this flow and all its brothers in the
          // block
          for (FLNodeElabVectorIter i = nodes.begin(); i != nodes.end(); ++i) {
            FLNodeElab* curFlow = *i;
            addLevelFaninAsyncEdgeMask(curFlow, mask);
          }
        }
      }
    }
  } // for
}

const SCHScheduleMask*
SCHMarkDesign::findLevelFaninAsyncEdgeMask(FLNodeElab* flow) const
{
  FLN_ELAB_ASSERT(mLevelFaninAsyncsValid, flow);
  LevelFaninAsyncs::const_iterator pos = mLevelFaninAsyncs->find(flow);
  if (pos != mLevelFaninAsyncs->end())
  {
    const SCHScheduleMask* mask = pos->second;
    return mask;
  }
  else
    return NULL;
}

void
SCHMarkDesign::addLevelFaninAsyncEdgeMask(FLNodeElab* flow,
                                          const SCHScheduleMask* mask)
{
  FLN_ELAB_ASSERT(mLevelFaninAsyncs->find(flow) == mLevelFaninAsyncs->end(), flow);
  mLevelFaninAsyncs->insert(LevelFaninAsyncs::value_type(flow, mask));
}

void
SCHMarkDesign::appendAsyncMask(const SCHScheduleMask** mask, FLNodeElab* flow,
                               NUAlwaysBlock* block, UInt32 *priority)
{
  // Get this async event but make sure it isn't a constant clock
  ClockEdge asyncEdge;
  const NUNetElab* asyncElab;
  getSequentialNodeClock(flow, block, &asyncEdge, &asyncElab);
  bool isClockBlock = (mUtil->getClockBlock(block) == NULL);
  if (!isInactiveClock(asyncElab, asyncEdge, isClockBlock))
  {
    // It is a valid clock
    const SCHEvent* asyncEv = mUtil->buildClockEdge(asyncElab, asyncEdge,
                                                    *priority);
    const SCHScheduleMask* newMask = mScheduleFactory->buildMask(asyncEv);
    ++(*priority);
    *mask = mScheduleFactory->appendMasks(*mask, newMask);
  }
}

void
SCHMarkDesign::getSequentialNodeClockAndMask(FLNodeElab* flow,
                                             NUUseDefNode* driver,
                                             ClockEdge* edge,
                                             const NUNetElab** clkElab,
                                             const SCHScheduleMask** asyncMask)
{
  // Get the clock information first
  getSequentialNodeClock(flow, driver, edge, clkElab);
  
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(driver);
  NU_ASSERT(always && always->isSequential(), driver);

  UInt32 priority = 1;
  *asyncMask = NULL;
  
  NUAlwaysBlock* priorityBlock = mUtil->getPriorityBlock(driver);
  while (priorityBlock != NULL)
  {
    // Get this async event but make sure it isn't a constant clock
    ClockEdge asyncEdge;
    const NUNetElab* asyncElab;
    getSequentialNodeClock(flow, priorityBlock, &asyncEdge, &asyncElab);
    if (!isInactiveClock(asyncElab, asyncEdge, false))
    {
      appendAsyncMask(asyncMask, flow, priorityBlock, &priority);
    }
    // See if we have another async event
    priorityBlock = priorityBlock->getPriorityBlock();
  }
}

const SCHScheduleMask* SCHMarkDesign::getExtraEdges(FLNodeElab* flow)
{
  // Check if it should have an extra mask. This occurs if this
  // sequential node is an async reset (has a clock block). It is
  // assumed that the flow node has some reason why it should run on
  // the extra edges (level fanin or is depositable)
  const SCHScheduleMask* extraMask = NULL;
  NUUseDefNode* useDef = flow->getUseDefNode();
  NUAlwaysBlock* clockBlock = mUtil->getClockBlock(useDef);
  if (clockBlock != NULL)
  {
    // Make sure this is not one of those special inverse edge blocks
    // we create to make the logic work correctly when an async reset
    // goes away (also known as funky cases (bug 238)).
    //
    // We can tell we have this type of async reset block because the
    // clock block points to a real block but we can't find it by
    // following from the clock block to its priority blocks.
    NUAlwaysBlock* priorityBlock = mUtil->getPriorityBlock(clockBlock);
    while ((priorityBlock != NULL) && (priorityBlock != useDef))
      priorityBlock = priorityBlock->getPriorityBlock();
    if (priorityBlock == useDef)
    {
      // Ok, we have one of these special simulation cases. Now lets
      // re-start at the clock block and create an edge mask from it.
      priorityBlock = clockBlock;
      UInt32 priority = 1;
      while (priorityBlock != useDef)
      {
        // Create a mask from this
        appendAsyncMask(&extraMask, flow, priorityBlock, &priority);
        
        // See if we have another async event
        priorityBlock = priorityBlock->getPriorityBlock();
      }
    }
  }
  return extraMask;
} // const SCHScheduleMask* SCHMarkDesign::getExtraEdges

bool
SCHMarkDesign::isInactiveClock(const NUNetElab* clkElab,
                               ClockEdge edge,
                               bool isClockBlock) const
{
  // Check if it is writable from the outside, then it is definitely
  // active
  if (isWritable(clkElab))
    return false;

  // Check if it is a constant clock
  int value;
  if (isConstantClock(clkElab, &value))
  {
    // For clock blocks, constant clocks are always inactive
    if (isClockBlock)
      return true;

    // Must be an async block, it is only inactive if the edge does
    // not match the clock value.
    if (((value == 0) && (edge == eClockPosedge)) ||
        ((value == 1) && (edge == eClockNegedge)))
      return true;
  }

  // Otherwise for clock blocks make sure it has active drivers
  if (isClockBlock) {
    NU_ASSERT(mMarkLiveNodesDone, clkElab);
    bool isLive = false;
    int driverCount = 0;
    for (SCHUtil::ConstDriverLoop l = mUtil->loopNetDrivers(clkElab);
         !l.atEnd() && !isLive; ++l)
    {
      FLNodeElab* flow = *l;
      isLive = flow->isLive() && !SCHUtil::isInitial(flow);
      ++driverCount;
    }
    return !isLive && (driverCount != 0);
  } else {
    return false;
  }
}


const SCHInputNets* 
SCHMarkDesign::findClocksAsData(FLNodeElab* flow, ClockMap* clockMap)
{
  // For sequentials there are no clocks as data
  if (SCHUtil::isSequential(flow))
    return NULL;
  
  // Check if we have been here before
  ClockMap::iterator pos = clockMap->find(flow);
  if (pos != clockMap->end()) {
    return pos->second;
  }
  
  // Compute the input nets for this combinational block
  const SCHInputNets* clockNets = NULL;
  if (flow->isPrimaryClock()) {
    // It is a primary clock; create an input nets
    clockNets = buildInputNets(flow->getDefNet());
  } else {
    // build the clocks from its fanin
    for (Fanin l = mUtil->loopFanin(flow); !l.atEnd(); ++l) {
      FLNodeElab* fanin = *l;
      if (fanin != flow)
      {
        const SCHInputNets* subClockNets = findClocksAsData(fanin,
                                                            clockMap);
        clockNets = appendInputNets(clockNets, subClockNets);
      }
    }
  }

  // Store the resolved clock nets for our cache
  (*clockMap)[flow] = clockNets;
  
  return clockNets;
} // SCHMarkDesign::findClocksAsData


SCHMarkDesign::SchedStartLoop::SchedStartLoop(SCHMarkDesign* markDesign)
{
  mOutLoop = markDesign->loopOutputPorts();
  mSeqLoop = markDesign->loopSequentialNodes();
  SCHUtil* util = markDesign->getUtil();
  mClockLoop = ClockFlowLoop(markDesign->loopClocks(), ClkFaninGen(util));
  mSchedStartType = eOutputPorts;
  nextLoop();
}

SCHMarkDesign::SchedStartLoop::ClkFaninGen::ClkFaninGen(SCHUtil* util) :
  mUtil(util)
{}

SCHMarkDesign::SchedStartLoop&
SCHMarkDesign::SchedStartLoop::operator=(const SchedStartLoop& src)
{
  if (&src != this)
  {
    mOutLoop = src.mOutLoop;
    mSeqLoop = src.mSeqLoop;
    mClockLoop = src.mClockLoop;
    mSchedStartType = src.mSchedStartType;
  }
  return *this;
}

void SCHMarkDesign::SchedStartLoop::operator++()
{
  switch(mSchedStartType)
  {
    case eOutputPorts:
      ++mOutLoop;
      break;

    case eSeqNodes:
      ++mSeqLoop;
      break;

    case eClockMasters:
      ++mClockLoop;
      break;

    case eEndLoop:
      INFO_ASSERT(0, "incrementing past the end of the loop");
      break;
  } // switch
  nextLoop();
} // void SCHMarkDesign::SchedStartLoop::operator++

void SCHMarkDesign::SchedStartLoop::nextLoop()
{
  switch(mSchedStartType)
  {
    case eOutputPorts:
      if (mOutLoop.atEnd())
        // Assume sequential nodes and fall through
        mSchedStartType = eSeqNodes;
      else
        break;

    case eSeqNodes:
      if (mSeqLoop.atEnd())
        // Assume clock flows and fall through
        mSchedStartType = eClockMasters;
      else
        break;

    case eClockMasters:
      if (mClockLoop.atEnd())
        // End of the loop
        mSchedStartType = eEndLoop;
      break;

    case eEndLoop:
      INFO_ASSERT(0, "incrementing past the end of the loop");
      break;
  } // switch
} // void SCHMarkDesign::SchedStartLoop::nextLoop

FLNodeElab* SCHMarkDesign::SchedStartLoop::operator*()
{
  FLNodeElab* flow = NULL;
  switch(mSchedStartType)
  {
    case eOutputPorts:
      flow = *mOutLoop;
      break;

    case eSeqNodes:
       flow = *mSeqLoop;
       break;

    case eClockMasters:
      flow = *mClockLoop;
      break;

    case eEndLoop:
      INFO_ASSERT(0, "dereference past the end of the loop");
      break;
  }
  return flow;
}

bool SCHMarkDesign::SchedStartLoop::atEnd() const
{
  return mSchedStartType == eEndLoop;
}

SCHMarkDesign::SchedStartType
SCHMarkDesign::SchedStartLoop::getSchedStartType() const
{
  return mSchedStartType;
}

SCHUtil::ConstDriverLoop
SCHMarkDesign::SchedStartLoop::ClkFaninGen::operator()(const NUNetElab* clk)
{
  return mUtil->loopNetDrivers(clk);
}

void SCHMarkDesign::computeFlowInputNets()
{
  // Either reset or create the cached data
  if (mFlowNetSets != NULL) {
    mFlowNetSets->clear();
  } else {
    mFlowNetSets = new FlowNetSets;
  }

  // Create and walk the acyclic graph and compute the input nets
  InputNetsWalker walker(mFlowNetSets, this, mUtil);
  walker.createFaninGraph();
  walker.walk();
}

UIntPtr
SCHMarkDesign::InputNetsWalker::getValue(FLNodeElab* flowElab)
{
  // Check if this is a primary input. If so, it needs to be added to
  // the input set.
  const SCHInputNets* flowNetSet = NULL;
  NUNetElab* net = flowElab->getDefNet();
  if ((net != NULL) &&
      (mMarkDesign->isPrimaryInput(net) ||
       SCHMarkDesign::isPrimaryBidi(net))) {
    // It is a primary input, build the net set for this net
    flowNetSet = mMarkDesign->buildInputNets(net);
  }

  return (UIntPtr)flowNetSet;
}

UIntPtr
SCHMarkDesign::InputNetsWalker::mergeValues(UIntPtr value1, UIntPtr value2)
{
  // Convert the values back to net set pointers
  const SCHInputNets* inputNets1 = (const SCHInputNets*)value1;
  const SCHInputNets* inputNets2 = (const SCHInputNets*)value2;

  // Compute the merged net sets
  const SCHInputNets* resultInputNets;
  resultInputNets = mMarkDesign->appendInputNets(inputNets1, inputNets2);
  return (UIntPtr)resultInputNets;
}

UIntPtr
SCHMarkDesign::InputNetsWalker::storeValue(FLNodeElab* flowElab, UIntPtr value)
{
  // Convert the value back to a net set pointer
  const SCHInputNets* inputNets = (const SCHInputNets*)value;

  // Store it in our database
  FLN_ELAB_ASSERT(mFlowNetSets->find(flowElab) == mFlowNetSets->end(), flowElab);
  mFlowNetSets->insert(FlowNetSets::value_type(flowElab, inputNets));

  return (UIntPtr)value;
}

const SCHInputNets* SCHMarkDesign::getFlowInputNets(FLNodeElab* flow)
{
  // This is only legal after we populate the set and on combinational
  // flow.
  FLN_ELAB_ASSERT(mFlowNetSets != NULL, flow);
  FLN_ELAB_ASSERT(!SCHUtil::isSequential(flow), flow);

  // If it is not in our set, it must be NULL. The caller should
  // determine if that is a programming error or not.
  const SCHInputNets* inputNets = NULL;
  FlowNetSets::iterator pos = mFlowNetSets->find(flow);
  if (pos != mFlowNetSets->end()) {
    inputNets = pos->second;
  }
  return inputNets;
}

void SCHMarkDesign::removeDeadFlowInputNets(const FLNodeElabSet& deletedFlow)
{
  // If we haven't allocated them yet, don't do this
  if ((mFlowNetSets == NULL) || mFlowNetSets->empty()) {
    return;
  }

  // Walk the flow and look for dead flow
  FLNodeElabVector deadFlow;
  for (FlowNetSetsLoop l(*mFlowNetSets); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = l.getKey();
    if ((deletedFlow.find(flowElab) != deletedFlow.end()) || !flowElab->isLive()) {
      deadFlow.push_back(flowElab);
    }
  }

  // Erase the dead flow we found in the set
  for (FLNodeElabVectorLoop l(deadFlow); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    mFlowNetSets->erase(flowElab);
  }
}

bool SCHMarkDesign::blockHasCModel(FLNodeElab* flow)
{
  // If it is a cycle check the sub components. We have to treat it
  // special because we have to look at the insides of them to tell if
  // a c-model is called.
  bool hasCmodel = false;
  if (flow->getUseDefNode() != NULL) {
    hasCmodel = blockHasCModel(flow->getUseDefNode());
  }
  return hasCmodel;
}

bool SCHMarkDesign::blockHasCModel(NUUseDefNode* useDef)
{
  bool hasCModel = false;
  switch(useDef->getType()) {
    case eNUCycle:
    {
      NUCycle* cycle = dynamic_cast<NUCycle*>(useDef);
      NU_ASSERT(cycle != NULL, useDef);
      NUCycle::FlowLoop l;
      for (l = cycle->loopFlows(); !l.atEnd() && !hasCModel; ++l) {
        FLNodeElab* nested = *l;
        hasCModel = blockHasCModel(nested);
      }

      // Remember it for next time
      mBlockToHasCModelMap->insert(BlockToHasCModelMapValue(cycle, hasCModel));
      break;
    }

    case eNUAlwaysBlock:
    case eNUInitialBlock:
    {
      // Check the cache first
      NUStructuredProc* proc = dynamic_cast<NUStructuredProc*>(useDef);
      NU_ASSERT(proc != NULL, useDef);
      BlockToHasCModelMap::iterator pos = mBlockToHasCModelMap->find(proc);
      if (pos == mBlockToHasCModelMap->end()) {
        // Not yet done, investigate
        hasCModel = proc->isCModelCall();

        // Remember it for next time
        mBlockToHasCModelMap->insert(BlockToHasCModelMapValue(proc,hasCModel));
      } else {
        hasCModel = pos->second;
      }
      break;
    }

    case eNUContAssign:
    case eNUContEnabledDriver:
    case eNUTriRegInit:
      // Can't have a c-model call. We don't cache these because it is
      // not worth the space. Determining whether it has a c-model is
      // fast.
      break;

    default:
      NU_ASSERT("Unexpected use def type" == NULL, useDef);
      break;
  }
  return hasCModel;
}

void SCHMarkDesign::addWritableFanout(FLNodeElab* flowElab)
{
  NUNetElab* netElab = flowElab->getDefNet();
  mWritableFanouts->insert(netElab);
}

const SCHInputNets* SCHMarkDesign::findNetInputNets(NUNetElab* netElab)
{
  const SCHInputNets* inputNets = NULL;
  for (SCHUtil::DriverLoop l = mUtil->loopNetDrivers(netElab); !l.atEnd(); ++l)
  {
    FLNodeElab* flow = *l;
    if (!SCHUtil::isSequential(flow)) {
      const SCHInputNets* thisInputNets;
      thisInputNets =  getFlowInputNets(flow);
      if (inputNets == NULL)
        inputNets = thisInputNets;
      else if (thisInputNets != NULL)
        inputNets = appendInputNets(inputNets, thisInputNets);
    }
  }
  return inputNets;
}


void SCHMarkDesign::addLatchClockUse(NUUseDefNode* useDef, NUNet* clkNet)
{
  // Find or allocate space to store this clock use
  NU_ASSERT(useDef->isSequential(), useDef);
  LatchClockUses::iterator pos = mLatchClockUses->find(useDef);
  NUNetSet* clockUses;
  if (pos == mLatchClockUses->end()) {
    clockUses = new NUNetSet;
    mLatchClockUses->insert(LatchClockUses::value_type(useDef, clockUses));
  } else {
    clockUses = pos->second;
  }

  // Store the clock use
  clockUses->insert(clkNet);
}

bool SCHMarkDesign::isLatchClockUse(FLNodeElab* flop, FLNodeElab* fanin)
{
  // Check if this is a flop that was converted from a latch
  NUUseDefNode* useDef = flop->getUseDefNode();
  FLN_ELAB_ASSERT(useDef->isSequential(), flop);
  bool isClockUse = false;
  LatchClockUses::iterator pos = mLatchClockUses->find(useDef);
  if (pos != mLatchClockUses->end()) {
    // It is, check all aliases of the fanin to see if it is a clock.
    NUNetElab* netElab = fanin->getDefNet();
    NUNetSet* clockUses = pos->second;
    NUNetVector nets;
    netElab->getAliasNets(&nets, true, flop->getHier());
    FLN_ELAB_ASSERT(!nets.empty(), flop);
    for (NUNetVectorLoop n(nets); !n.atEnd() && !isClockUse; ++n) {
      NUNet* net = *n;
      isClockUse = clockUses->find(net) != clockUses->end();
    }
  }

  return isClockUse;
} // bool SCHMarkDesign::isLatchClockUse

//! Add clocks required by latches
void SCHMarkDesign::addClocksRequiredByLatches()
{
  // Do a walk over the elaborated flow factory, finding the clock nets
  // associated with each flop flow's use def node.  Elaborate the nets in
  // the flow node's hierarchy and insert the NUNetElab in the set of latch
  // clock nets.
  FLNodeElabFactory* factory = mUtil->getFlowElabFactory();
  FLNodeElab* flow_elab = NULL;
  for (FLNodeElabFactory::FlowLoop iter = factory->loopFlows();
       iter(&flow_elab);) {
    if (SCHUtil::isSequential(flow_elab)) {
      NUUseDefNode* useDef = flow_elab->getUseDefNode();
      LatchClockUses::iterator pos = mLatchClockUses->find(useDef);
      if (pos != mLatchClockUses->end()) {
        NUNetSet* clockUses = pos->second;
        STBranchNode* hier = flow_elab->getHier();
        for (NUNetSet::iterator iter = clockUses->begin();
             iter != clockUses->end();
             ++iter) {
          NUNet* net = *iter;
          NUNetElab* netElab = net->lookupElab(hier);
          mClocks->insert(netElab);
        }
      }
    }
  }
} // void SCHMarkDesign::addClocksRequiredByLatches

void SCHMarkDesign::gatherClockAndDriver(ClockAndDriver* clockAndDriver) const
{
  for (SCHClockMastersAndInvertsLoop l(loopClockMastersAndInverts());
       not l.atEnd(); ++l) {
    const NUNetElab* clkElab = *l;
    clockAndDriver->addClock(clkElab);
  }
  for (SCHSchedule::AsyncResetLoop l(loopAsyncResets()); not l.atEnd(); ++l) {
    clockAndDriver->addClock((*l).first);
  }
}

void SCHMarkDesign::addAsyncScheduleNode(FLNodeElab* flow)
{
  FLN_ELAB_ASSERT(!SCHUtil::isSequential(flow), flow);
  mAsyncScheduleNodes->push_back(flow);
}

void SCHMarkDesign::addInputScheduleNode(FLNodeElab* flow)
{
  FLN_ELAB_ASSERT(!SCHUtil::isSequential(flow), flow);
  mInputScheduleNodes->push_back(flow);
}

FLNodeElabVectorLoop SCHMarkDesign::loopAsyncScheduleNodes()
{
  return SequentialNodesLoop(*mAsyncScheduleNodes);
}

FLNodeElabVectorLoop SCHMarkDesign::loopInputScheduleNodes()
{
  return SequentialNodesLoop(*mInputScheduleNodes);
}

// We sometimes get pull drivers on flops. This may come from nucleus
// analysis that isn't as accurate as it could be but it could also be
// the customer using pullups or pulldowns on a flop (strange,
// huh?). We should never schedule these because they cause the wrong
// simulation results. We detect this case here and if we found it,
// don't schedule the node.
// 
// We also get pull drivers on netElabs that have been marked
// depositable. This can happen when two undriven netElabs share some
// hierachy and one of them is depositable but the other isn't. So if
// the net elab doesn't have active drivers and it is marked
// depositable, don't schedule the pull driver.
bool SCHMarkDesign::isInvalidPullDriver(FLNodeElab* flowElab) const
{
  // Make sure it is a pull driver, there are few of these so it is
  // cheaper to do that test first.
  if (!mUtil->isPullDriver(flowElab)) {
    return false;
  }

  // It is a pull driver, check if there are any sequential
  // drivers. Also check if there are any active drivers.
  bool hasSequentialDriver = false;
  bool hasActiveDrivers = false;
  const NUNetElab* netElab = flowElab->getDefNet();
  for (SCHUtil::ConstDriverLoop l = mUtil->loopNetDrivers(netElab);
       ( (not hasSequentialDriver) and (not l.atEnd()) );
       ++l)
  {
    FLNodeElab* curFlowElab = *l;
    if (mUtil->isActiveDriver(curFlowElab)) {
      hasActiveDrivers = true;
      if (mUtil->isSequential(curFlowElab)) {
        hasSequentialDriver = true;
      }
    }
  }
  if (hasSequentialDriver) {
    return true;
  }

  // If it doesn't have active drivers, check if it is depositable
  return !hasActiveDrivers && isWritable(netElab);
} // bool SCHMarkDesign::isInvalidPullDriver

void SCHMarkDesign::makeEdgeExprClock(NUEdgeExpr *edgeExpr, FLNodeElab* flow,
                                      ClockEdge* edge, const NUNetElab** clkElab)
{
  FLN_ELAB_ASSERT(edgeExpr != NULL, flow);
  if (edge != NULL) {
    *edge = edgeExpr->getEdge();
  }
  NUNet* clockNet = edgeExpr->getNet();
  FLN_ELAB_ASSERT(clockNet != NULL, flow);
  
  const NUNetElab* netElab = clockNet->lookupElab(flow->getHier());
  bool invert;
  *clkElab = getClockMaster(netElab, &invert);
  if (invert) {
    if (edge != NULL) {
      *edge = ClockEdgeOppositeEdge(*edge);
    }
  }
}

// For a given sequential node get the clock pin and make it a clock
void
SCHMarkDesign::getSequentialNodeClock(FLNodeElab* flow, NUUseDefNode* driver,
                                      ClockEdge* edge, const NUNetElab** clkElab)
{
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(driver);
  FLN_ELAB_ASSERT((always != NULL) && always->isSequential(), flow);
  NUEdgeExpr* edgeExpr = always->getEdgeExpr();
  makeEdgeExprClock(edgeExpr, flow, edge, clkElab);
} 

SCHUtil::ConstDriverLoop
SCHMarkDesign::loopSequentialClockDrivers(FLNodeElab* flowElab)
{
  // get the master clock
  FLN_ELAB_ASSERT(SCHUtil::isSequential(flowElab), flowElab);
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  ClockEdge edge;
  const NUNetElab* clkElab;
  getSequentialNodeClock(flowElab, useDef, &edge, &clkElab);
  return mUtil->loopNetDrivers(clkElab);
}

//! Print warnings for a cycle and all of the flows in the cycle
void SCHMarkDesign::reportCycleFlows(FLNodeElabHashSet& flows,
                                     bool isSequentialCycle, size_t cycleSize,
                                     size_t settled, size_t branches,
                                     UInt32 cycleID)
{
  MsgContext* msgContext = mUtil->getMsgContext();
  if (isSequentialCycle)
    msgContext->SchDerivedClockCycle(cycleSize);
  else if (settled == 0)
    msgContext->SCHCycleBroken(cycleID, cycleSize, branches);
  else if (settled == cycleSize)
    msgContext->SchCombCycle(cycleSize);
  else
    msgContext->SCHCyclePartiallyBroken(cycleID, cycleSize, settled, branches);
  
  if (settled > 0)
  {
    size_t i = 0;
    for (FLNodeElabHashSet::SortedLoop loop = flows.loopSorted();
         !loop.atEnd(); ++loop)
    {
      FLNodeElab* flow = *loop;
      reportCycleFlow(flow, ++i, settled);
    }
  }
}

//! Print the warnings for a single flow node in a cycle
void SCHMarkDesign::reportCycleFlow(FLNodeElab* flow, int count, int cycleSize)
{
  MsgContext* msgContext = mUtil->getMsgContext();
  if (SCHUtil::isSequential(flow))
  {
    ClockEdge edge;
    const NUNetElab* clkElab;
    getSequentialNodeClock(flow, flow->getUseDefNode(), &edge, &clkElab);
    HierName* clkName = clkElab->getSymNode();
    msgContext->SCHSequentialCycleNode(flow, *clkName, count, cycleSize);
  }
  else
    msgContext->SCHCycleNode(flow, count, cycleSize);

  // If this flow node is for a c-model, print an additional warning
  if (SCHUtil::isCModel(flow))
    msgContext->SCHCModelInCycle(flow);
}

const NUNetElab*
SCHMarkDesign::getClockMaster(const NUNetElab* clkElab, bool* invert) const
{
  // Look for a clock in our clock cache. This is created as part of
  // the clock analysis pass.
  const NUNetElab* retClkElab = clkElab;
  MasterClockMap::iterator pos = mMasterClockMap->find(clkElab);
  if (pos != mMasterClockMap->end()) {
    MasterClock masterClock = pos->second;
    retClkElab = masterClock.first;
    if (invert != NULL) {
      *invert = masterClock.second;
    }
  } else {
    // Use the original clock (we haven't run clock analysis yet or
    // there is no master). And set the inversion if requested
    if (invert != NULL) {
      *invert = false;
    }
  }

  return retClkElab;
}

bool SCHMarkDesign::isConstantClock(const NUNetElab* clkElab, int* value) const
{
  ConstantClocks::iterator pos = mConstantClocks->find(clkElab);
  if (pos != mConstantClocks->end()) {
    if (value != NULL) {
      *value = pos->second;
    }
    return true;
  } else {
    return false;
  }
}

bool SCHMarkDesign::isValidClock(const NUNetElab* clkElab) const
{
  return mClocks->find(clkElab) != mClocks->end();
}

SCHMarkDesign::LevelFaninFilter::LevelFaninFilter(SCHMarkDesign* mark,
                                                  FLNodeElab* flow) :
  mMarkDesign(mark), mFlow(flow)
{}

bool
SCHMarkDesign::LevelFaninFilter::operator()(FLNodeElab* fanin) const
{
  return (!SCHUtil::isSequential(mFlow) ||
          !mMarkDesign->isSequentialClockOrAsyncPin(mFlow, fanin));
}

SCHMarkDesign::LevelFaninLoop SCHMarkDesign::loopLevelFanin(FLNodeElab* flow)
{
  LevelFaninFilter filt(this, flow);
  return LevelFaninLoop(mUtil->loopFanin(flow), filt);
}

bool
SCHMarkDesign::matchEdgeNet(STBranchNode* hier, NUAlwaysBlock* always,
                            const NUNetElab* faninNet)
{
  // Get the elaborated clock pin
  NUEdgeExpr *edge_expr = always->getEdgeExpr();
  NU_ASSERT(edge_expr != NULL, always);
  NUNet* clockNet = edge_expr->getNet();
  const NUNetElab* clockNetElab = getClockMaster(clockNet->lookupElab(hier));

  // Return if the given net matches it
  return (clockNetElab == faninNet);
}

bool
SCHMarkDesign::matchEdgeNet(FLNodeElab* flowElab,
                            const NUAlwaysBlockVector& alwaysBlocks,
                            FLNodeElab* faninElab)
{
  bool matchFound = false;
  STBranchNode* hier = flowElab->getHier();
  const NUNetElab* faninNetElab = getClockMaster(faninElab->getDefNet());
  for (CLoop<NUAlwaysBlockVector> l(alwaysBlocks); !l.atEnd() && !matchFound; ++l) {
    NUAlwaysBlock* curAlways = *l;
    matchFound = matchEdgeNet(hier, curAlways, faninNetElab);
  }
  return matchFound;
}

bool 
SCHMarkDesign::isSequentialClockOrAsyncPin(FLNodeElab* flow, FLNodeElab* fanin)
{
  // Get the always block
  const NUAlwaysBlockVector& alwaysBlocks = getFlopAlwaysBlocks(flow);
  return matchEdgeNet(flow, alwaysBlocks, fanin);
}

SCHMarkDesign::DesignIter::DesignIter(SCHMarkDesign* mark, bool visitCyclicFlow) :
  mMarkDesign(mark), mUtil(mark->getUtil()), mVisitCyclicFlow(visitCyclicFlow)
{
  // Allocate space
  mStack = new UtStack<FLNodeElab*>;
  mFanoutMap = new FanoutMap;
  mCovered = new FLNodeElabSet;

  // Populate the output ports
  for (OutputPortsLoop p = mark->loopOutputPorts(); !p.atEnd(); ++p) {
    FLNodeElab* flowElab = *p;
    addFanin(NULL, flowElab);
  }
}

SCHMarkDesign::DesignIter::~DesignIter()
{
  delete mStack;
  delete mFanoutMap;
  delete mCovered;
}

FLNodeElab* SCHMarkDesign::DesignIter::operator*() const
{
  INFO_ASSERT(!mStack->empty(), "Invalid operation during design walk");
  return mStack->top();
}

void SCHMarkDesign::DesignIter::operator++()
{
  // Remove the current node
  FLNodeElab* flowElab = mStack->top();
  mStack->pop();

  // Add the fanins to the stack
  if (!flowElab->isInactive()) {
    // Add the flow fanins
    for (Fanin f = mUtil->loopFanin(flowElab); !f.atEnd(); ++f) {
      FLNodeElab* faninElab = *f;
      addFanin(flowElab, faninElab);
    }

    // If it is sequential add the master clock for this sequential's
    // clocks and all higher priority block's clocks.
    if (SCHUtil::isSequential(flowElab)) {
      for (SCHMarkDesign::EdgeFaninLoop e = mMarkDesign->loopEdgeFanin(flowElab);
           !e.atEnd(); ++e) {
        FLNodeElab* faninElab = *e;
        addFanin(flowElab, faninElab);
      }
    }

    // If this is a cycle, then don't forget the encpasulated nodes
    if (mVisitCyclicFlow) {
      NUCycle* cycle = flowElab->getCycle();
      if (cycle != NULL) {
        for (NUCycle::FlowLoop p = cycle->loopFlows(); !p.atEnd(); ++p)	{
          FLNodeElab* faninElab = *p;
          addFanin(flowElab, faninElab);
        }
      }
    }
  } // if
} // void SCHMarkDesign::DesignIter::operator++

bool SCHMarkDesign::DesignIter::atEnd() const
{
  return mStack->empty();
}

FLNodeElab* SCHMarkDesign::DesignIter::getFanout(FLNodeElab* flowElab) const
{
  FLNodeElab* fanoutElab = NULL;
  FanoutMap::iterator pos = mFanoutMap->find(flowElab);
  if (pos != mFanoutMap->end()) {
    fanoutElab = pos->second;
  }
  return fanoutElab;
}

void
SCHMarkDesign::DesignIter::addFanin(FLNodeElab* fanoutElab, FLNodeElab* faninElab)
{
  // Only add it if we haven't seen it before (covered set is faster
  // than fanout map) and this iterator gets used a lot.
  if (mCovered->insertWithCheck(faninElab)) {
    // If the fanin is an encpasulated cycle, then treat all nodes in
    // the cycle as a fanin of the fanout. This is because some of the
    // flow may only be reachable through another cycle node if it
    // doesn't have fanout outside the cycle. We do it here to avoid
    // an N*N algorithm in the operator++ function. We want it to be
    // within the insertWithCheck call.
    FLNodeElabVector nodes;
    if (faninElab->isEncapsulatedCycle()) {
      mUtil->getAcyclicFlows(faninElab, &nodes);
    } else {
      nodes.push_back(faninElab);
    }

    for (FLNodeElabVectorLoop l(nodes); !l.atEnd(); ++l) {
      FLNodeElab* curFlowElab = *l;
      mFanoutMap->insert(FanoutMap::value_type(curFlowElab, fanoutElab));
      mStack->push(curFlowElab);
      mCovered->insert(curFlowElab);
    }
  }
} // SCHMarkDesign::DesignIter::addFanin

void SCHMarkDesign::markFrequentDepositableNets(bool printFrequentNets)
{
  // This function is recallable.
  mFrequentDepositNets->clear();

  // Find all the deposit nets that the user said should be frequently
  // depositable.
  IODBNucleus* iodb = mUtil->getIODB();
  for (IODB::NameSetLoop p = iodb->loopFrequentDeposit(); !p.atEnd(); ++p) {
    const STSymbolTableNode* node = *p;
    const NUNetElab* netElab = iodb->getNet(node);
    if (netElab != NULL) {
      mFrequentDepositNets->insert(netElab->getSymNode()->getStorage());
    }
  }

  // Find all deposits that we believe may be deposited
  // frequently. The idea is that if it feeds clock logic or is a
  // clock, then it will be deposited frequently. We don't traverse
  // the clock pin of async resets because we don't expect them to
  // change often. But if it shares logic with the a clock pin, then
  // it still gets marked.
  FLNodeElabSet covered;
  for (DesignIter i(this); !i.atEnd(); ++i) {
    // Make sure it is a clock block
    FLNodeElab* flowElab = *i;
    if (SCHUtil::isSequential(flowElab)) {
      NUUseDefNode* useDef = flowElab->getUseDefNode();
      if (mUtil->getClockBlock(useDef) == NULL) {
        const NUNetElab* clkElab;
        getSequentialNodeClock(flowElab, useDef, NULL, &clkElab);
        for (SCHUtil::ConstDriverLoop n = mUtil->loopNetDrivers(clkElab);
             !n.atEnd(); ++n) {
          FLNodeElab* faninElab = *n;
          markFrequentDepositableNetsRecurse(faninElab, &covered, iodb);
        }
      }
    }
  }
  mFrequentDepositNetsValid = true;

  // Tell the user what is assumed to be frequently deposited. Don't
  // print any deposits that the user told us are frequently
  // deposited.
  if (printFrequentNets) {
    MsgContext* msgContext = mUtil->getMsgContext();
    for (DepositNets::SortedLoop l = mFrequentDepositNets->loopSorted();
         !l.atEnd(); ++l) {
      const STAliasedLeafNode* node = *l;
      const NUNetElab* netElab = NUNetElab::find(node);
      if (!iodb->isFrequentDepositable(netElab->getSymNode())) {
        msgContext->SchFrequentDeposit(netElab);
      }
    }
  }
}

void
SCHMarkDesign::markFrequentDepositableNetsRecurse(FLNodeElab* flowElab,
                                                  FLNodeElabSet* covered,
                                                  IODBNucleus* iodb)
{
  // Make sure we haven't been here before
  if (!covered->insertWithCheck(flowElab)) {
    // Not inserted, already been here
    return;
  }

  // Check if this flow represents a deposited net that the user did
  // not say was infrequent, if so, treat it as frequently deposited.
  const NUNetElab* netElab = flowElab->getDefNet();
  const STAliasedLeafNode* node = netElab->getSymNode();
  if (isDepositable(netElab) && !iodb->isInfrequentDepositable(node)) {
    mFrequentDepositNets->insert(node->getStorage());
  }

  // Keep traversing if this is a combinational node
  if (!SCHUtil::isSequential(flowElab)) {
    for (Fanin l = mUtil->loopFanin(flowElab); !l.atEnd(); ++l) {
      FLNodeElab* faninElab = *l;
      markFrequentDepositableNetsRecurse(faninElab, covered, iodb);
    }
  }
} // SCHMarkDesign::markFrequentDepositableNetsRecurse

  
void SCHMarkDesign::addNewSequentialNode(FLNodeElab* flowElab)
{
  FLN_ELAB_ASSERT(mSequentialNodesValid, flowElab);
  FLN_ELAB_ASSERT(SCHUtil::isSequential(flowElab), flowElab);
  mSequentialNodes->push_back(flowElab);
}

void SCHMarkDesign::findCycleBlocks(NUCUseDefSet* cycleBlocks) const
{
  for (SCHSchedule::CycleLoop l = loopCycles(); !l.atEnd(); ++l) {
    NUCycle* cycle = *l;
    for (NUCycle::FlowLoop f = cycle->loopFlows(); !f.atEnd(); ++f) {
      FLNodeElab* flow = *f;
      cycleBlocks->insert(flow->getUseDefNode());
    }
  }
}

const SCHSignature* SCHMarkDesign::getSignature(FLNodeElab* flow, bool allowNull)
{
  const SCHSignature* sig = flow->getSignature();
  if (!allowNull && (sig == NULL)) {
    analyzeSignatureProblem(flow);
    FLN_ELAB_ASSERT("Invalid Signature Found" == NULL, flow);
  }
  return sig;
}

void SCHMarkDesign::analyzeSignatureProblem(FLNodeElab* flowElab)
{
  // Create the fanout map for analysis
  createFanoutMap();

  // Walk the fanout map looking for the live flows with a signature
  bool nested = getenv(CRYPT("CARBON_PRINT_NESTED_FLOW")) != NULL;
  FLNodeElabSet covered;
  FLNodeElabVector flowStack;
  flowStack.push_back(flowElab);
  while (!flowStack.empty()) {
    // Print information about the current block
    FLNodeElab* curFlow = flowStack.back();
    flowStack.pop_back();
    const SCHSignature* sig = curFlow->getSignature();
    if (sig == NULL) {
      UtIO::cout() << "\nFlow(" << curFlow << ") with invalid signature:\n";
    } else {
      UtIO::cout() << "\nFlow(" << curFlow << ") with signature: ";
      sig->print();
    }

    // Print the two elaborated nets (master net and code hierarchy net)
    FLNodeElab* printFlow = curFlow;
    if (curFlow->isEncapsulatedCycle()) {
      printFlow = mUtil->getCycleOriginalNode(curFlow);
    }
    UtIO::cout() << "  ElabNet: ";
    printFlow->getDefNet()->getSymNode()->print();
    UtString buf;
    printFlow->getHier()->compose(&buf);
    buf += ".";
    buf += printFlow->getFLNode()->getDefNet()->getName()->str();
    UtIO::cout() << "  CodeNet: " << buf << "\n";

    // Print the block
    UtIO::cout() << "  Individual driver of elaborated net:\n";
    printVerilog(printFlow, nested, 2);

    // If the signature is invalid, print all the drivers
    if (sig == NULL) {
      bool found = false;
      UtIO::cout() << "  All Driver of elaborated net:\n";
      NUNetElab* netElab = printFlow->getDefNet();
      for (SCHUtil::DriverLoop l = mUtil->loopNetDrivers(netElab); !l.atEnd(); ++l) {
        FLNodeElab* driver = *l;
        UtIO::cout() << "Flow(" << driver << " with signature: ";
        const SCHSignature* sig = driver->getSignature();
        if (sig == NULL) {
          UtIO::cout() << "<NULL>\n";
        } else {
          sig->print();
        }
        printVerilog(driver, nested, 4);

        // Check if we found the driver with the invalid sig
        found |= (driver == curFlow);
      }
      if (!found) {
        UtIO::cout() << "Didn't find individual driver in net elab driver list!\n";
      }
    }

    // Now gather and print the fanout if we still have an invalid signature
    if (sig == NULL) {
      for (FLNodeElabVectorCLoop l = loopFlowFanout(curFlow); !l.atEnd(); ++l) {
        FLNodeElab* fanout = *l;
        UtIO::cout() << "  Fanout Flow: " << fanout << "\n";
        if (covered.insertWithCheck(fanout)) {
          flowStack.push_back(fanout);
        }
      }
    }
  } // while

  // All done with the map
  clearFanoutMap();
}

bool SCHMarkDesign::isFastClock(const NUNetElab* clkElab) const
{
  const NUNetElab* masterClkElab = getClockMaster(clkElab);
  NU_ASSERT2(masterClkElab == clkElab, clkElab, masterClkElab);
  return mUtil->getIODB()->isFastClock(clkElab->getSymNode());
}

void SCHMarkDesign::printVerilog(FLNodeElab* flowElab, bool nested, int indent)
{
  // If this is a bound node, print it as such
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  if (useDef == NULL) {
    UtIO::cout() << "  <Bound Node>: type = " << flowElab->getType() << "\n\n";
    return;
  }

  // Either print this flow or the nested flow
  if (nested) {
    // Walk the single nesting until we get the lowest level possible
    FLNodeElab* curFlow = flowElab;
    while (curFlow != NULL) {
      // get the block and then walk the nesting
      useDef = curFlow->getUseDefNode();
      curFlow = curFlow->getSingleNested();

      // If the next nesting is NULL, then print the verilog
      if (curFlow == NULL) {
        useDef->printVerilog(true, indent, true);
      }
    }
  } else {
    FLNodeElab* curFlowElab = mUtil->getCycleOriginalNode(flowElab);
    NUNetRefHdl netRef = curFlowElab->getFLNode()->getDefNetRef();
    UtString buf;
    netRef->compose(&buf);
    for (int i = 0; i < indent; ++i) {
      UtIO::cout() << " ";
    }
    UtIO::cout() << "NetRef: " << buf << "\n";
    useDef->printVerilog(true, indent, true);
  }
}

void SCHMarkDesign::compose(const SCHInputNets* inputNets, UtString* buf)
{
  const char* sep = "";
  for (CLoop<const SCHInputNets> l(*inputNets); !l.atEnd(); ++l) {
    const NUNetElab* netElab = *l;
    *buf += sep;
    sep = " or ";
    netElab->compose(buf, NULL);
  }
}


void
SCHMarkDesign::analyzeInvalidScheduleClk(SCHSequential* seq, 
                                         const NUNetElab* clkElab)
{
  // Print the full path to the clock
  UtIO::cout() << "Found and invalid clock: ";
  clkElab->getSymNode()->print();

  // Test if the sequential schedule is empty
  int posSize = seq->size(eClockPosedge);
  int negSize = seq->size(eClockNegedge);
  UtIO::cout() << "  The schedule posedge count is " << posSize << "\n";
  UtIO::cout() << "  The schedule negedge count is " << negSize << "\n";

  // Test if the clock is live or a constant
  if (!clkElab->isContinuouslyDriven()) {
    UtIO::cout() << "  The clock is undriven!\n";
  } else {
    UtIO::cout() << "  The clock is driven!\n";
  }
  int value;
  if (isConstantClock(clkElab, &value)) {
    UtIO::cout() << " The clock has a constant value = " << value << "\n";
  }
  if (isLiveClock(clkElab)) {
    UtIO::cout() << "  The clock is live!\n";
  } else {
    UtIO::cout() << "  The clock is not live!\n";
  }

  // Test if the clock has a better master
  const NUNetElab* master = getClockMaster(clkElab);
  if (clkElab != master) {
    UtIO::cout() << "Invalid clock is an equivalent of ";
    master->getSymNode()->print();
  }

  // Check if all elaborated flow is live and pick a flop and print
  // the Verilog for the code
  FLNodeElab* flowElab = NULL;
  if (posSize > 0) {
    for (SCHIterator i = seq->getSchedule(eClockPosedge); !i.atEnd(); ++i) {
      flowElab = *i;
      if (!flowElab->isLive()) {
        UtIO::cout() << "    Found dead flow in schedule:\n";
        flowElab->getUseDefNode()->printVerilog(true, 4, true);
      }
    }
  }
  if (negSize > 0) {
    for (SCHIterator i = seq->getSchedule(eClockNegedge); !i.atEnd(); ++i) {
      flowElab = *i;
      if (!flowElab->isLive()) {
        UtIO::cout() << "    Found dead flow in schedule:\n";
        flowElab->getUseDefNode()->printVerilog(true, 4, true);
      }
    }
  }
  if (flowElab == NULL) {
    // This is strange, it can't be empty, can it?
    UtIO::cout() << "  No sample flop was found for this schedule!\n";
  } else {
    UtIO::cout() << "  The following is a sample flop for this clock:\n";
    flowElab->getUseDefNode()->printVerilog(true, 4, true);

    // Test if it is in the sequential nodes
    for (SequentialNodesLoop l = loopSequentialNodes(); !l.atEnd(); ++l) {
      FLNodeElab* curFlowElab = *l;
      if (curFlowElab == flowElab) {
        UtIO::cout() << "Found the flop in the registered vector!\n";
      }
      if (curFlowElab->getDefNet() == flowElab->getDefNet()) {
        UtIO::cout() << "Found the flop net in the registered vector.\n";
      }
    }
  }

  NU_ASSERT(isValidClock(clkElab), clkElab);
} // SCHMarkDesign::analyzeInvalidScheduleClk

const NUAlwaysBlockVector& SCHMarkDesign::getFlopAlwaysBlocks(FLNodeElab* flowElab)
{
  // Get the always block for this flop
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  FLN_ELAB_ASSERT(always != NULL, flowElab);

  // Get the clock block for this flop. By storing the data by clock
  // block, we make the map smaller.
  NUAlwaysBlock* clockBlock = always->getClockBlock();
  if (clockBlock == NULL) {
    clockBlock = always;
  }

  // Check if it is in our cache. If so, return it
  FlopAlwaysBlocks::iterator pos = mFlopAlwaysBlocks->find(clockBlock);
  if (pos != mFlopAlwaysBlocks->end()) {
    NUAlwaysBlockVector* alwaysBlocks = pos->second;
    return *alwaysBlocks;
  }

  // It's not there, add it and return it
  NUAlwaysBlockVector* alwaysBlocks = new NUAlwaysBlockVector;
  clockBlock->getClockBlockReferers(alwaysBlocks);
  alwaysBlocks->push_back(clockBlock);
  FlopAlwaysBlocks::value_type value(clockBlock, alwaysBlocks);
  mFlopAlwaysBlocks->insert(value);
  return *alwaysBlocks;
} // const NUAlwaysBlockVector* SCHMarkDesign::getFlopAlwaysBlocks

void SCHMarkDesign::clearFlopAlwaysBlocksCache()
{
  for (LoopMap<FlopAlwaysBlocks> l(*mFlopAlwaysBlocks); !l.atEnd(); ++l) {
    NUAlwaysBlockVector* alwaysBlocks = l.getValue();
    delete alwaysBlocks;
  }
  mFlopAlwaysBlocks->clear();
}


void SCHMarkDesign::printTraceNets()
{
  // Loop over the trace nets
  IODBNucleus* iodb = mUtil->getIODB();
  for (IODB::NameSetLoop l = iodb->loopTraced(); !l.atEnd(); ++l) {
    const STSymbolTableNode* node = *l;
    const NUNetElab* netElab = iodb->getNet(node);
    UtString name;
    node->compose(&name);
    if (netElab != NULL) {
      printTraceNet(name, netElab);
    } else {
      UtIO::cout() << "Could not find a net for '" << name << "' to trace.\n"
                   << "It must have been optimized away.\n";
    }
  }
}

void SCHMarkDesign::printTraceNet(const UtString& name, const NUNetElab* netElab)
{
  FLNodeElabSet covered;
  UtIO::cout() << "Tracing '" << name << "':\n";
  for (SCHUtil::ConstDriverLoop d = mUtil->loopNetDrivers(netElab);
       !d.atEnd(); ++d) {
    FLNodeElab* flowElab = *d;
    printTraceRecurse(flowElab, &covered, 0);
  }
}

void
SCHMarkDesign::printTraceRecurse(FLNodeElab* flowElab, FLNodeElabSet* covered,
                                 int depth)
{
  // Make sure we haven't been here before
  if (!covered->insertWithCheck(flowElab)) {
    return;
  }

  // Print the driver info and depth
  UtIO::cout() << "  Driver (depth = " << depth << "):\n";
  UtIO::cout() << "  Elaborated Net: ";
  flowElab->getDefNet()->getSymNode()->print();
  const SCHSignature* sig = flowElab->getSignature();
  UtIO::cout() << "  Signature: ";
  if (sig == NULL) {
    UtIO::cout() << "<NULL>\n";
  } else {
    sig->print();
  }
  if (!SCHUtil::isSequential(flowElab)) {
    if (flowElab->isSampled()) {
      UtIO::cout() << "  Logic is sample scheduled\n";
    } else {
      UtIO::cout() << "  Logic is transition scheduled\n";
    }
  }
  if (flowElab->isDerivedClock() || flowElab->isDerivedClockLogic()) {
    UtIO::cout() << "  Logic is part of the derived clock schedules\n";
  }
  printVerilog(flowElab, false, 4);

  // Stop at flops at depths more than 1
  if ((depth > 0) && SCHUtil::isSequential(flowElab)) {
    return;
  }

  // Print information about the fanin
  for (FlowDependencyLoop l = loopDependencies(flowElab); !l.atEnd(); ++l) {
    FLNodeElab* faninElab = *l;
    printTraceRecurse(faninElab, covered, depth+1);
  }
} // SCHMarkDesign::printTraceRecurse

bool SCHMarkDesign::isLatch(const NUNetElab* netElab) const
{
  return mElabStateTest->isLatch(netElab);
}

bool SCHMarkDesign::isLatch(FLNodeElab* flowElab) const
{
  return mElabStateTest->isLatch(flowElab);
}

bool SCHMarkDesign::isState(const NUNetElab* netElab) const
{
  return mElabStateTest->isState(netElab);
}

bool SCHMarkDesign::isState(FLNodeElab* flowElab) const
{
  return mElabStateTest->isState(flowElab);
}

SCHMarkDesign::EdgeFaninLoop SCHMarkDesign::loopEdgeFanin(FLNodeElab* flowElab)
{
  // Create a multi loop to add each clock to
  EdgeFaninLoop loop;

  // Get the always block, it must be one
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  FLN_ELAB_ASSERT(SCHUtil::isSequential(useDef), flowElab);
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);

  // Walk the always block and higher priority blocks
  bool clockBlock = (always->getClockBlock() == NULL);
  for (; always != NULL; always = always->getPriorityBlock()) {
    // Get the clock
    const NUNetElab* clkElab;
    ClockEdge edge;
    getSequentialNodeClock(flowElab, always, &edge, &clkElab);

    // Add its drivers if it isn't an inactive reset
    if (clockBlock || !isInactiveClock(clkElab, edge, false)) {
      SCHUtil::ConstDriverLoop netLoop = mUtil->loopNetDrivers(clkElab);
      loop.pushLoop(FLNodeElabLoop::create(netLoop));
    }

    // The remaining always blocks are not clock blocks
    clockBlock = false;
  }
  return loop;
} // SCHMarkDesign::EdgeFaninLoop SCHMarkDesign::loopEdgeFanin

void SCHMarkDesign::findAsyncDeposits(IODBNucleus* iodb)
{
  // Walk the outputs looking for combinational logic
  FLNodeElabSet covered;
  for (OutputPortsLoop l = loopOutputPorts(); !l.atEnd(); ++l) {
    // Walk the fanin of the flow elab looking for deposits. We don't
    // want to mark nets that are both deposit and observable as one
    // of these deposits because there is no combinational path.
    FLNodeElab* flowElab = *l;
    findAsyncDepositRecurse(flowElab, &covered, iodb);
  }
}

void
SCHMarkDesign::findAsyncDepositRecurse(FLNodeElab* flowElab,
                                       FLNodeElabSet* covered,
                                       IODBNucleus* iodb)
{
  // Stop if this is covered
  if (!covered->insertWithCheck(flowElab)) {
    return;
  }

  // Stop if this is a flop
  if (SCHUtil::isSequential(flowElab)) {
    return;
  }

  // Recurse
  for (Fanin l = mUtil->loopFanin(flowElab); !l.atEnd(); ++l) {
    FLNodeElab* faninElab = *l;
    findAsyncDeposit(faninElab, covered, iodb);
  }
}

void
SCHMarkDesign::findAsyncDeposit(FLNodeElab* flowElab,
                                FLNodeElabSet* covered,
                                IODBNucleus* iodb)
{
  // Stop if this is covered
  if (!covered->insertWithCheck(flowElab)) {
    return;
  }

  // Check if this is an infrequently depositable net
  NUNetElab* netElab = flowElab->getDefNet();
  if (isDepositable(netElab) && !isFrequentDepositable(netElab)) {
    iodb->declareAsyncDeposit(netElab->getSymNode());
  }

  // Recurse
  findAsyncDepositRecurse(flowElab, covered, iodb);
}

void SCHMarkDesign::findRedundantUseClocks()
{
  // Walk the design looking for flops
  for (DesignIter i(this); !i.atEnd(); ++i) {
    // Check if this is a flop
    FLNodeElab* flowElab = *i;
    if (SCHUtil::isSequential(flowElab)) {
      // Get the FLNodeElab for the nested block because it's fanin only includes the level fanin
      FLN_ELAB_ASSERT(flowElab->hasNestedBlock(), flowElab);
      FLNodeElab* flowElabNested = flowElab->getSingleNested();
      FLN_ELAB_ASSERT(flowElabNested != NULL, flowElab);

      // Walk the fanin for this block. Only look at the continous
      // drivers. There probably shouldn't be any nested flow in the
      // fanin, but I don't remember for sure.
      for (Fanin f = mUtil->loopFanin(flowElabNested); !f.atEnd(); ++f) {
        FLNodeElab* faninElab = *f;
        if (mUtil->isContDriver(faninElab) && isSequentialClockOrAsyncPin(flowElab, faninElab)) {
          mRedundantUseClocks.insert(faninElab->getDefNet());
        }
      }
    }
  }
} // void SCHMarkDesign::findRedundantUseClocks

bool SCHMarkDesign::isRedundantClockUse(const NUNetElab* clkElab) const
{
  return mRedundantUseClocks.count(clkElab) != 0;
}
