// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implements combinational block merging
*/

#ifndef _COMBINATIONALBLOCKMERGE_H_
#define _COMBINATIONALBLOCKMERGE_H_

#include "BlockMerge.h"

class SCHMergeTest;

class SCHCombinationalBlockMerge : public SCHBlockMerge
{
#if ! pfGCC_2
  using UtGraphMerge::nodeMergable;
  using UtGraphMerge::edgeMergable;
#endif

public:
  //! constructor
  SCHCombinationalBlockMerge(SCHUtil*, SCHMarkDesign*, SCHScheduleData*);

  //! destructor
  ~SCHCombinationalBlockMerge();

  //! Function to merge combinational blocks in the design
  /*! This function tests to make sure block merging is enabled,
   *  gathers all the combinational schedule blocks, creates the graph
   *  and merges them. If the dump flag is enabled it prints
   *  information about the succesful and failed merges.
   */
  void merge();

private:
  // This section contains abstract virtual functions that must be
  // defined for the SCHBlockMerge class to do its work.

  //! Override this function to test if a block is mergable
  bool blockMergable(const NUUseDefNode* useDef, MergeBlock* mergeBlock) const;

  //! Override this function to test if two blocks are mergable
  bool edgeMergable(const MergeEdge* edge, const MergeBlock* fanoutBlock,
                    const MergeBlock* faninBlock, bool dumpMergedBlocks) const;

  //! Override this function to sort blocks into mergable buckets
  /*! The two blocks are assumed to have the same depth, this is an
   *  additional sort routine to determine if two blocks can be
   *  merged.
   *
   *  Returns -1,0,1 for an ordering between to blocks
   */
  int compareBuckets(const MergeBlock* block1, const MergeBlock* block2) const;

  //! Override this function to get the block fanin
  void getBlockFanin(MergeBlock* block, BlockSet* faninSet) const;

  //! Override this function to indicate what blocks are valid start points
  /*! This function will be called once per block created. This is
   *  used by the merge by fanin pass to see if we should start from
   *  this node. All blocks are valid start nodes
   */
  bool validStartBlock(const NUUseDefNode*, MergeBlock*) const;

  //! Virtual function to record a broken up block
  /*! Record this for the dumped statistics
   */
  virtual void recordCycleBlock(const MergeBlock* block) const;

private:
  // This section contains private types and data to perform the
  // merge.

  //! Function to initialize local data needed before merging
  void initLocalData();

  //! Function to add a schedule to the block merging class
  void addSchedule(SCHCombinational* comb);

  //! Function to print statistics if the dump flag is set
  void printResults();

  //! Type to figure out which schedule a vector of elaborated flow is in
  /*! The following type allow us to quickly go from a block group to
   *  schedule it is in. This helps in updating the groups after
   *  merging within each schedule.
   */
  typedef UtHashMap<FLNodeElabVector*, SCHCombinational*> GroupToScheduleMap;

  //! Data to figure out which schedule a vector of elaborated flow is in
  /*! The following map is a way to get the schedule for any block
   *  group. This is needed so that the schedules can be updated when a
   *  merge occurs.
   */
  GroupToScheduleMap* mGroupToScheduleMap;

  //! No work to do for pre merge callback
  void preMergeCallback(MergeBlock*, MergeBlock*) { return; }

  //! Function to update the schedules after a merge
  void postMergeCallback(MergeBlock* lastBlock, MergeBlock* block);

  //! Function to get block fanin
  /*! This is used for breaking false cycles and block graph creation.
   */
  void getBlockFlowFanin(FLNodeElab* flow, const FLNodeElabSet& blockFlows,
                         BlockSet* faninSet) const;

  //! Types and functions to keep track of adjacency merge stats
  enum AdjMergeFail {
    eUnmergable,                //!< The fanin block is not mergable
    eCrossesHier,               //!< Different hierarchies
    eUnmatchHierSched,          //!< Different sched/hier per instance
    eMultiDefPartialLatch,      //!< Fanin or fanout is a partial def latch
    eNumAdjMergeFails
  };
  mutable int mAdjMergeFails[eNumAdjMergeFails];
  const char* adjFailReason(AdjMergeFail adjMergeFail) const;

  // Data to keep track of block pairs that have been printed
  typedef std::pair<FLNodeElab*, FLNodeElab*> PrintPair;
  typedef UtSet<PrintPair> PrintedFailedMerges;
  mutable PrintedFailedMerges* mPrintedFailedMerges;

  //! Function to record and print information about failed adjacency merges
  void recordFailedMerge(const MergeBlock* fanoutBlock,
                         const MergeBlock* faninBlock,
                         AdjMergeFail reason) const;

  //! Helper function to print elaborated flow in an unmergable blocks
  void printBlockNodes(const FLNodeElabVector& nodes) const;

  //! Class to compute/keep track of mergable/unmergable blocks
  SCHMergeTest* mMergeTest;

  //! A cache of elaborated flows that def a partial latches in multiple blocks
  /*! The cache is used to avoid N*N testing where N is the number of
   *  always blocks that def parts of a latch.
   */
  typedef UtMap<const FLNodeElab*, bool> PartialMultiDefLatches;
  mutable PartialMultiDefLatches* mPartialMultiDefLatches;

  //! Utility function to test if a flow represents a partial def latch
  bool isPartialDefLatch(FLNodeElab* flowElab) const;

  //! Utility function to test if a flow represents a partial def latch
  bool isMultiDefPartialLatch(FLNodeElab* flowElab) const;

  //! Utility function to test if a block contains a partial def latch
  bool isMultiDefPartialLatch(const MergeBlock* block) const;

}; // class SCHCombinationalBlockMerge : public SCHBlockMerge

#endif // _COMBINATIONALBLOCKMERGE_H_
