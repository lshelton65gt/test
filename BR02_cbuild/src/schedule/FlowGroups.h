// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _FLOWGROUPS_H_
#define _FLOWGROUPS_H_

#include "util/Loop.h"
#include "util/LoopThunk.h"

class SCHFlowGroups;

//! Iterator over the groups of elaborated flows
typedef CLoop<SCHFlowGroups> SCHFlowGroupsLoop;

//! This class is an abstraction over a vector of vectors of elaborated flow
/*! This class simplifies using a vector of vectors of elaborated
 *  flow. It provides iteration of all the groups (FLNodeElabVector*)
 *  or over all the elaborated flow in this class.
 */
class SCHFlowGroups : public UtVector<FLNodeElabVector*>
{
public:
  //! Constructor
  SCHFlowGroups() {}

  //! Destructor
  ~SCHFlowGroups() {}

  //! Add a group of flows
  void addGroup(FLNodeElabVector* nodes) { push_back(nodes); }

  //! Iterate over the groups of elaborated flows
  SCHFlowGroupsLoop loopGroups() const;

  //! Abstraction to iterate over the flow nodes for a group
  typedef CLoop<FLNodeElabVector> GroupLoop;

  //! structure to support visiting flow nodes for this block
  struct FlowLoopGen
  {
    GroupLoop operator()(FLNodeElabVector* nodes) const;
  };

  //! Abstraction to iterate over the flow nodes for this block
  /*! Note that this iterator does not remove duplicates if they exist
   *  across the groups.
   */
  typedef LoopThunk<SCHFlowGroupsLoop, GroupLoop, FlowLoopGen> FlowsLoop;

  //! Iterator over the flow nodes that represent this block
  FlowsLoop loopFlowNodes() const;
}; // class SCHFlowGroups : UtVector<FLNodeElabVector*>

#endif // _FLOWGROUPS_H_
