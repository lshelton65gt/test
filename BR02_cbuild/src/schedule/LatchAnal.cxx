// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!\file
 * An implementation of latch analysis and latch-to-flop conversion.
 * The analysis is based on building ControlTree data structures that
 * describe the control conditions for latches, and using the structure
 * of those trees to decide what flops are required to recreate all
 * of the transitions allowed by the latch.
 */

/* TODO:
 *
 * - Implement Nucleus transformations
 * - Build BDDs using ClkAnal
 * - Split complex clock expressions with low fanout
 * - Optimize edge expressions using stability conditions
 * - Merge FlopDescriptions for the same edge
 *   This could happen with a block like:
 *     if (cond)
 *     begin
 *       if (en)
 *         l1 = a;
 *     end
 *     else
 *       if (en)
 *         l1 = b;
 * - Disqualify blocks if they def a net used in the
 *   control conditions
 * - Collect/report histogram information on
 *   - latches / block
 *   - instances / latch
 *   - control tree sizes, frequency, and counts
 *   - clock condition sizes and count
 * - Improve libdesign.latches output
 */

// Set this to true to enable dumping of data on latch analysis
#define DEBUG_LATCH_ANALYSIS false

#include "LatchAnal.h"
#include "BlockSplit.h"

#include "util/OSWrapper.h"
#include "util/SetOps.h"
#include "util/Stats.h"

#include "nucleus/NUNet.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUAlwaysWalker.h"
#include "nucleus/NULvalue.h"

#include "flow/Flow.h"
#include "flow/FlowClassifier.h"
#include "flow/FLIter.h"

#include "reduce/ControlTree.h"
#include "reduce/RETransform.h"
#include "reduce/CleanElabFlow.h"

#include "exprsynth/Expr.h"
#include "exprsynth/ExprFactory.h"
#include "exprsynth/ESPopulateExpr.h"
#include "exprsynth/ExprCallbacks.h"
#include "exprsynth/ExprResynth.h"

#include "localflow/UD.h"

#include "Util.h"

//! Get a BDD describing when a state device is guaranteed to be stable
BDD getStabilityCondition(SCHLatchAnal* latchAnal, const FLNodeElab* flow)
{
  NUUseDefNode* useDef = flow->getUseDefNode();
  FLN_ELAB_ASSERT(useDef, flow);

  BDDSource* bddSource = latchAnal->getBDDSource();
    
  if (useDef->isSequential())
  {
    // this is a flop, the stability condition is the inverse of its clock
    return bddSource->opNot(bddSource->bdd(flow));
  }
  else
  {
    // this is a latch, the stability condition is the inverse of the
    // driving condition for the latch's control tree
    ControlTree* tree;
    bool hasControlTree = latchAnal->getControlTree(flow, &tree);
    if (!hasControlTree)
      return bddSource->falseBDD(); // assume never stable
    if (!tree)
      return bddSource->trueBDD();  // NULL control tree means it is always stable
    return bddSource->opNot(tree->getDrivingCondition(bddSource));
  }
}

//! Helper function to add the immediately implicated fanin of a flow to a flow set
// This is used when we want to traverse through the flow without traversing 
// arcs which skip over intermediate flow because they summarize some compound
// flow path underneath them in the nesting hierarchy.
void addImmediateUses(FLNodeElab* flow, FLNodeElabSet* q)
{
  NUUseDefNode* useDef = flow->getUseDefNode();

  if (!useDef)
    return;  // bound node has no uses

  if (useDef->getType() == eNUAlwaysBlock)
  {
    // For an always block, only add edge fanin
    for (FLNodeElabLoop loop = flow->loopEdgeFanin(); !loop.atEnd(); ++loop)
    {
      FLNodeElab* fanin = *loop;
      q->insert(fanin);
    }
  }
  else if (useDef->getType() == eNUBlock)
  {
    // For a block, add no edges -- just recurse
  }
  else
  {
    // For other statement types, add level fanin
    // We can make this more accurate by adding the level fanin for terminal nodes
    // and only adding the fanin due to the condition for an if node, case node, etc.
    for (FLNodeElabLoop loop = flow->loopFanin(); !loop.atEnd(); ++loop)
    {
      FLNodeElab* fanin = *loop;
      q->insert(fanin);
    }
  }

  // recurse into nesting
  for (FLNodeElabLoop loop = flow->loopNested(); !loop.atEnd(); ++loop)
  {
    FLNodeElab* nested = *loop;
    addImmediateUses(nested,q);
  }
}

//! Callback class for expression synthesis back to PIs and state
class ClockSynthCallback : public ESCachedCallbackElab
{
public:
  //! constructor
  ClockSynthCallback(ESPopulateExpr* populateExpr)
    : ESCachedCallbackElab(populateExpr), mPreviousNet(NULL)
  {}

  //! destructor
  ~ClockSynthCallback() {}

  //! Callback method that allows for cycle detection
  void addFlowsInProgress(FLNodeElabVector& flows, FLNodeElabVector* addedFlows)
  {
    for (FLNodeElabVectorLoop l(flows); !l.atEnd(); ++l) {
      FLNodeElab* flowElab = *l;
      if (mFlowsInProgress.find(flowElab) == mFlowsInProgress.end()) {
        mFlowsInProgress.insert(flowElab);
        addedFlows->push_back(flowElab);
      }
    }
  }

  //! Callback method that allows for cycle detection
  void removeFlowsInProgress(FLNodeElabVector& flows)
  {
    for (FLNodeElabVector::iterator iter = flows.begin(); iter != flows.end(); ++iter)
    {
      FLNodeElab* flow = *iter;
      mFlowsInProgress.erase(flow);
    }
  }

  //! Routine to indicate when to stop translation
  bool stop(FLNodeElabVector* flows, const NUNetRefHdl& ref, bool isContDriver)
  {
    // We should only be called for continous drivers
    NU_ASSERT(isContDriver, ref->getNet());

    bool isStop = false;
    NUNetElab* defNet = NULL;

    for (FLNodeElabVector::iterator iter = flows->begin();
         (iter != flows->end()) && !isStop;
         ++iter)
    {
      FLNodeElab* flow = *iter;

      // Stop if this is a primary input
      isStop |= flow->isBoundNode();

      // Stop if this is a flop
      NUUseDefNode* useDef = flow->getUseDefNode();
      isStop |= useDef->isSequential();

      // Stop if this is a latch
      NUNet* net = flow->getFLNode()->getDefNet();
      isStop |= net->isLatch();

      // Stop if this is an initial block
      isStop |= useDef->isInitial();

      // Stop if we have encountered a cyclic path
      if (!defNet)
        defNet = flow->getDefNet();
      // We only check for a cycle on the first call with a new net, because
      // we can get called mutliple times and later calls will have the flow
      // in the mFlowsInProgress set but we have not returned to the net
      // through a cyclic path (we haven't left it yet).
      isStop |= ((defNet != mPreviousNet) && (mFlowsInProgress.find(flow) != mFlowsInProgress.end()));
    }

    mPreviousNet = defNet;

    return isStop;
  }

  //! Routine to create the identity expression for a flow node
  ESExprHndl createIdentExpr(FLNodeElabVector* flowElabs, UInt32 bitSize,
                             bool isSigned, bool hardStop)
  {
    // Use clock analysis to determing the clock master for this
    // elaborated flow.  Create an ident expression using the clock
    // master.
    
    return ESCachedCallbackElab::createIdentExpr(flowElabs, bitSize, isSigned,
                                                 hardStop);
  }

private:
  // Hide copy and assign constructors.
  ClockSynthCallback(const ClockSynthCallback&);
  ClockSynthCallback& operator=(const ClockSynthCallback&);

private:
  FLNodeElabSet mFlowsInProgress;
  NUNetElab*    mPreviousNet;
};

class ElabCanonicalExpr: public NuToNuFn
{
public:
  ElabCanonicalExpr(CopyContext& copyContext,
                    STBranchNode* hier,
                    SCHClkAnal* clkAnal,
                    bool seedMode = false)
    : mCopyContext(copyContext),
      mHier(hier),
      mClkAnal(clkAnal),
      mSeedMode(seedMode)
  {}

  virtual NUExpr * operator()(NUExpr* expr, Phase phase)
  {
    if (isPre(phase))
      return 0;
    NUExpr* replacement = find(expr);
    if (replacement != NULL)
      delete expr;
    return replacement;
  }

  NUExpr* find(const NUExpr* expr)
  {
    const NUIdentRvalue* rv = dynamic_cast<const NUIdentRvalue*>(expr);
    if (rv == NULL)
      return 0;
    NU_ASSERT(rv->isWholeIdentifier(), expr);
    return netElabToExpr(rv->getNetElab(mHier), ePrePost);
  }

  NUExpr* netToExpr(NUNet* net, Phase phase)
  {
    return netElabToExpr(net->lookupElab(mHier), phase);
  }

  NUExpr* netElabToExpr(NUNetElab* netElab, Phase phase)
  {
    if (!isPost(phase))
      return 0;

    // Don't call clock analysis on block local temp nets. They have
    // no drivers so there is nothing it can do. Also, it ends up
    // causing a crash if that net is deleted which often happens with
    // temp nets.
    NUNet* net = netElab->getNet();
    NUNetElab* canonical = netElab;
    if (!net->isTempBlockLocalNonStatic()) {
      canonical = mClkAnal->findClock(net, netElab->getHier(),
                                      NULL, false, false, NULL);
    }
    if (mSeedMode)
      return NULL;
    else
      return new NUIdentRvalueElab(canonical, canonical->getNet()->getLoc());
  }

private:
  CopyContext&  mCopyContext;
  STBranchNode* mHier;
  SCHClkAnal*   mClkAnal;
  bool          mSeedMode;
};

ClkAnalBDDSource::~ClkAnalBDDSource()
{
  for (ExprCache::iterator iter = mExprCache.begin(); iter != mExprCache.end(); ++iter)
  {
    NUExpr* expr = iter->second;
    delete expr;
  }
}

void ClkAnalBDDSource::seed(const FLNodeElab* flow)
{
  if (flow)
  {
    NUExpr* expr = getExprForFlow(flow, false);
    if (expr)
    {
      // Call clock analysis on leaf nets to achieve stable clock
      // masters before building BDDs.  This is done using a version
      // of ElabCanonicalExpr that calls SCHLatchAnal::findClock() but
      // doesn't do any expression replacement.
      INFO_ASSERT(mClkAnal != NULL,
                  "Seed should not be run without a clk anal class");
      CopyContext cc(0,0);
      ElabCanonicalExpr elabCanonical(cc, flow->getHier(), mClkAnal, true);
      replaceLeaves(expr, elabCanonical);
    }
  }
}

bool ClkAnalBDDSource::extractExpr(const FLNodeElab* flow, NUExpr** expr, CopyContext& cc)
{
  if (flow == NULL)
  {
    *expr = NULL;
    return false;
  }

  NUUseDefNode* useDef = flow->getUseDefNode();

  // For an if statement, extract the condition
  NUIf* ifStmt = dynamic_cast<NUIf*>(useDef);
  if (ifStmt)
  {
    *expr = ifStmt->getCond()->copy(cc);
    return false;
  }
  else
  {
    // For an always block, extract the edge expression
    NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
    if (always && always->isSequential())
    {      
      NUEdgeExpr* edge= always->getEdgeExpr();
      *expr = edge->getExpr()->copy(cc);
      if (edge->getEdge() == eClockNegedge)
      {
        *expr = new NUUnaryOp(NUOp::eUnLogNot, *expr, (*expr)->getLoc());
        (*expr)->resize((*expr)->determineBitSize());
      }
      return true;
    }
  }

  return false;
}
 
NUExpr* ClkAnalBDDSource::getExprForFlow(const FLNodeElab* flow, bool returnCopy)
{
  CopyContext cc(0,0);

  // first, check the cache
  ExprCache::iterator p = mExprCache.find(flow);
  if (p != mExprCache.end())
  {
    if (returnCopy)
      return p->second->copy(cc);
    else
      return p->second;
  }

  // not in cache, build the expression back to state and PIs
  NUExpr* originalExpr = NULL;
  bool edge = extractExpr(flow, &originalExpr, cc);

  ESExprHndl hndl;
  {
    ClockSynthCallback callback(mPopulateExpr);
    FLNodeElab* nonConstFlow = const_cast<FLNodeElab*>(flow);
    if (edge)
      hndl = mPopulateExpr->createSingleEdge(nonConstFlow, &callback);
    else
      hndl = mPopulateExpr->createIfCondition(nonConstFlow, &callback);
  }

  NUExpr* expr = NULL;
  CarbonExpr* cExpr = hndl.getExpr();
  if ((cExpr != NULL) && !hndl.numOpsGreaterThan(1000))
  {
    // Use expression resynthesis to convert the CarbonExpr back to an NUExpr
    const SourceLocator& loc = flow->getUseDefNode()->getLoc();
    expr = const_cast<NUExpr*>(mExprResynth->exprToNucleus(cExpr, NULL, loc));
    if (expr)
      expr = expr->copy(cc);
  }

  // if the expression manipulation failed, fallback to original
  if (expr == NULL)
    expr = originalExpr;
  else
    delete originalExpr;

  if (!expr)
    return NULL;

  // Apply folding to simplify the expression
  expr = mFold->fold(expr);

  // update the cache
  mExprCache[flow] = expr;

  if (returnCopy)
    return expr->copy(cc);
  else
    return expr;
}

NUExpr* ClkAnalBDDSource::useSimpleExprForFlow(const FLNodeElab* flow)
{
  CopyContext cc(0,0);

  // remove any existing cached expression
  ExprCache::iterator p = mExprCache.find(flow);
  if (p != mExprCache.end())
  {
    delete p->second;
    mExprCache.erase(p);
  }

  // Just extract the expression and use it directly
  NUExpr* expr = NULL;
  extractExpr(flow, &expr, cc);

  if (!expr)
    return NULL;

  // Apply folding to simplify the expression
  expr = mFold->fold(expr);

  // update the cache
  mExprCache[flow] = expr;

  return expr->copy(cc);
}

BDD ClkAnalBDDSource::bdd(const FLNodeElab* flow)
{
  if (flow == NULL)
    return mBDDContext->invalid();
  
  NUExpr* expr = getExprForFlow(flow);

  // replace NUIdentRvalues in the expression with NUIdentRvalueElabs
  // and convert the nets to the canonical net as determined by
  // SCHLatchAnal::findClock().
  CopyContext cc(0,0);
  ElabCanonicalExpr elabCanonical(cc, flow->getHier(), mClkAnal);
  if (mClkAnal != NULL) {
    if (expr) {
      expr = replaceLeaves(expr, elabCanonical);
    }
  }
  
  // Create the BDD
  BDD bdd = expr ? mBDDContext->bdd(expr,flow->getHier()) : mBDDContext->invalid();

  // If the BDD creation failed, try again with just a simple expression
  if (!bdd.isValid())
  {
    delete expr; // delete previous expression
    expr = useSimpleExprForFlow(flow);
    if ((mClkAnal != NULL) && expr) {
      expr = replaceLeaves(expr, elabCanonical);
    }
    bdd = expr ? mBDDContext->bdd(expr,flow->getHier()) : mBDDContext->invalid();
  }

  // Free the expression memory
  delete expr;

  return bdd;
}

NUExpr* ClkAnalBDDSource::replaceLeaves(NUExpr* expr, NuToNuFn& translator)
{
  NUExpr* retExpr = expr;
  NUExpr* newExpr = translator(retExpr, NuToNuFn::ePre);
  if (newExpr != NULL) {
    retExpr = newExpr;
  }
  retExpr->replaceLeaves(translator);
  newExpr = translator(retExpr, NuToNuFn::ePost);
  if (newExpr != NULL) {
    retExpr = newExpr;
  }
  return retExpr;
}

//! Callback class for running conditions through clock analysis
class SeedBDDSourceCallback : public ControlTreeCallback
{
public:
  SeedBDDSourceCallback(BDDSource* bddSource)
    : mBDDSource(bddSource)
  {
  }

  virtual ~SeedBDDSourceCallback() {}

  //! Seed the BDD source for this control tree node
  virtual bool visit(ControlTree* node)
  {
    if (node->isBranch())
    {
      // at branches, seed the if condition flow
      FLNodeElab* ifFlow = node->getBranchFlow();
      mBDDSource->seed(ifFlow);
    }
    else
    {
      // at leaves, traverse back looking for flops to seed
      for (FLNodeElabSet::const_iterator loop = node->beginLeafFlows();
           loop != node->endLeafFlows();
           ++loop)
      {
        FLNodeElab* leafFlow = *loop;

        // We want to search fanin in a way that does not skip over
        // flow, meaning we only want to use fanin arcs that are due
        // to immediate uses, not summary arcs.
        FLNodeElabSet q;
        FLNodeElabSet done;
        addImmediateUses(leafFlow,&q);

        while (!q.empty())
        {
          FLNodeElab* flow = *(q.begin());
          q.erase(flow);
          if (done.find(flow) != done.end())
            continue;
          done.insert(flow);

          NUUseDefNode* useDef = flow->getUseDefNode();
          
          NUNet* net = flow->getFLNode()->getDefNet();
          // stop at bounds.
          if (!useDef)
            continue;

          // seed and stop at flops
          if (useDef->isSequential())
          {
            mBDDSource->seed(flow);
            continue;
          }

          // stop at latches and initial blocks
          if (useDef->isInitial() || net->isLatch())
            continue;
          
          addImmediateUses(flow,&q);
        }
      }
    }
    
    return true;
  }
  
private:
  BDDSource* mBDDSource;
};

//! Callback class for determining stability conditions for all fanins
class StabilityCallback : public ControlTreeCallback
{
public:
  StabilityCallback(SCHLatchAnal* latchAnal)
    : mLatchAnal(latchAnal)
  {
  }

  virtual ~StabilityCallback() {;}

  //! Traverse flow fanin of this node accumulating driving BDDs
  virtual bool visit(ControlTree* node)
  {
    // Begin assuming the inputs are always stable (no fanin ==> constant inputs)
    mLatchAnal->setAlwaysStable(node);

    if (node->isBranch())
    {
      FLNodeElab* flow = node->getBranchFlow();
      NUIf* ifStmt = dynamic_cast<NUIf*>(flow->getUseDefNode());
      FLN_ELAB_ASSERT(ifStmt, flow);
      visitExpr(node,flow,ifStmt->getCond());
    }
    else  // node is leaf
    {
      for (FLNodeElabSet::const_iterator loop = node->beginLeafFlows();
           loop != node->endLeafFlows();
           ++loop)
      {
        FLNodeElab* leafFlow = *loop;
        visitFlow(node,leafFlow,false);
      }
    }

    // always continue because we want to compute stability for all leaves
    return true;
  }

private:

  //! Visit the flow's fanin for the given expression, accumulating stability information
  bool visitExpr(ControlTree* node, FLNodeElab* startFlow, NUExpr* expr)
  {
    // Iterate over the immediate fanin flows for the expression
    bool neverStable = false;
    FLExprFaninClassifier<FLNodeElab> exprFanin(startFlow, expr);
    for (FLExprFaninClassifier<FLNodeElab>::FlowLoop loop = exprFanin.loopExprFanin();
         !loop.atEnd() && !neverStable;
         ++loop)
    {
      FLNodeElab* fanin = *loop;
      neverStable = visitFlow(node, fanin, true);
    }

    return neverStable;
  }

  //! Visit the flow's fanin, accumulating stability information
  bool visitFlow(ControlTree* node, FLNodeElab* startFlow, bool includeStart)
  {
    // We want to search fanin in a way that does not skip over
    // flow, meaning we only want to use fanin arcs that are due
    // to immediate uses, not summary arcs.
    FLNodeElabSet q;
    FLNodeElabSet done;
    if (includeStart)
      q.insert(startFlow);
    else
      addImmediateUses(startFlow,&q);

    while (!q.empty())
    {
      FLNodeElab* flow = *(q.begin());
      q.erase(flow);
      if (done.find(flow) != done.end())
        continue;
      done.insert(flow);

      NUNet* net = flow->getFLNode()->getDefNet();
      NUUseDefNode* useDef = flow->getUseDefNode();

      // A bound node is never stable
      if (!useDef)
      {
        mLatchAnal->setNeverStable(node);
        return true; 
      }
      // Ignore initial blocks
      else if (useDef->isInitial()) {
        continue;
      }
      // Is the flow for a state device? Then get its stability condition.
      else 
      {
        const FLNodeElab* topFlow = mLatchAnal->getTopLevelFlow(flow);
        // no top level flow, don't stop the expansion, even if this net is marked latch.
        if (topFlow != NULL)
        {
          NUUseDefNode * topUseDef = topFlow->getUseDefNode();

          if (net->isLatch() || topUseDef->isSequential())
          {
            // don't compute the condition if we've got a non-always block.
            if (topUseDef->getType() == eNUAlwaysBlock) {
              BDD stable = getStabilityCondition(mLatchAnal,topFlow);

              // update the stability condition BDD in the map 
              mLatchAnal->addStabilityRequirement(node,stable);

              // if the fanins are never all stable, then abort the traversal
              if (stable == mLatchAnal->getBDDSource()->falseBDD())
                return true;
            }
          
            // state devices are stopping points for traversal
            continue;
          }
        }
      }
      
      addImmediateUses(flow,&q);
    }
  
    return false;
  }
  
private:
  SCHLatchAnal* mLatchAnal;
};

//! Callback class for determining if a control tree is exclusive with all fanin
class ExclusivityCallback : public ControlTreeCallback
{
public:
  ExclusivityCallback(LatchStatus* latchStatus, SCHLatchAnal* latchAnal)
    : mLatchStatus(latchStatus), mLatchAnal(latchAnal)
  {}

  virtual ~ExclusivityCallback() {}

  //! Test if the transparency condition ==> stability condition for the leaf
  virtual bool visit(ControlTree* leaf)
  {
    INFO_ASSERT(leaf->isLeaf(),
                "Leaf walk visit called on a non leaf node during latch conversion");

    // Compute the transparency condition for the leaf
    BDD transparent = leaf->getReadCondition(mLatchAnal->getBDDSource());

    // Lookup the stability condition for the leaf in the BDD map
    BDD stable;
    bool hasStability = mLatchAnal->getStabilityBDD(leaf, &stable);
    
    // Exclusive when transparency condition implies stability
    bool isExclusive = hasStability && mLatchAnal->getBDDSource()->implies(transparent, stable);
    
    // If it is not exclusive, traverse back through fanin enumerating all non
    // exclusive fanins.
    if (!isExclusive)
      enumerateNonExclusiveFanins(leaf, transparent);

    return true;
  }

private:
  void enumerateNonExclusiveFanins(ControlTree* leaf, BDD& transparent)
  {
    // We want to search fanin in a way that does not skip over
    // flow, meaning we only want to use fanin arcs that are due
    // to immediate uses, not summary arcs.
    for (FLNodeElabSet::const_iterator iter = leaf->beginLeafFlows();
         iter != leaf->endLeafFlows();
         ++iter)
    {
      FLNodeElab* leafFlow = *iter;

      FLNodeElabSet q;
      FLNodeElabSet done;

      addImmediateUses(leafFlow,&q);

      while (!q.empty())
      {
        FLNodeElab* flow = *(q.begin());
        q.erase(flow);
        if (done.find(flow) != done.end())
          continue;
        done.insert(flow);

        NUNet* net = flow->getFLNode()->getDefNet();
        NUUseDefNode* useDef = flow->getUseDefNode();

        // A bound node is never stable
        if (!useDef)
        {
          mLatchStatus->setFlags(FED_BY_PI);
          mLatchStatus->addNonExclusiveFanin(leaf,flow);
          continue; 
        }

        // Ignore initial blocks
        if (useDef->isInitial()) {
          continue;
        }

        const FLNodeElab* topFlow = mLatchAnal->getTopLevelFlow(flow);
        // no top level flow, don't stop the expansion, even if this net is marked latch.
        if (topFlow != NULL)
        {
          NUUseDefNode * topUseDef = topFlow->getUseDefNode();

          if (net->isLatch() || topUseDef->isSequential())
          {
            // don't compute the condition if we've got a non-always block.
            if (topUseDef->getType() == eNUAlwaysBlock) {
              BDD stable = getStabilityCondition(mLatchAnal,topFlow);
        
              // if the transparency condition for the leaf does not imply the
              // stability condition for the fanin, report the non-exclusive driver.
              if (!mLatchAnal->getBDDSource()->implies(transparent,stable))
              {
                mLatchStatus->setFlags(NON_EXCL_DRIVER);
                mLatchStatus->addNonExclusiveFanin(leaf,flow);
              }
            }
            
            // state devices are stopping points for traversal
            continue;
          }
        }
      
        addImmediateUses(flow,&q);
      }
    }    
  }

private:
  LatchStatus*  mLatchStatus;
  SCHLatchAnal* mLatchAnal;
};


//! A design walker subclass that breaks up compound If conditions
/*! This class is used to convert always blocks with if conditions like:
 * \verbatim
 *   if (clk & en)
 *     ...
 * \endverbatim
 * into nested if statements:
 * \verbatim
 *   if (clk)
 *     if (en)
 *       ...
 * \endverbatim
 *
 * It also applies to the dual structure:
 * \verbatim
 *  if (clk | en)
 *    // nothing
 *  else
 *    ...
 * \endverbatim
 * which becomes:
 * \verbatim
 *   if (clk)
 *     // nothing
 *   else
 *     if (en)
 *       // nothing
 *     else
 *       ...
 * \endverbatim
 *
 * Note that this only rewrites one outermost AND or OR.  It does not
 * attempt to apply this transformation globally because it would
 * likely create too many clocks on unnecessary signals.
 */
class RewriteIfConditionCallback : public NUDesignCallback
{
public:
  RewriteIfConditionCallback(NUNetRefFactory* netRefFactory)
    : mNetRefFactory(netRefFactory), mCurrentBlock(NULL)
  {
  }

  //! Do nothing for most constructs
  virtual Status operator()(Phase, NUBase*) { return eNormal; }

  //! Process non-sequential always blocks
  virtual Status operator()(Phase phase, NUAlwaysBlock* always)
  {
    if (phase == ePre)
    {
      if (always->isSequential())
        return eSkip; // only interested in non-sequential always blocks
      mCurrentBlock = always;
    }
    else
    {
      mCurrentBlock = NULL;
    }
    return eNormal;
  }

  //! If an if statement under a non-sequential block matches the pattern, rewrite it
  virtual Status operator()(Phase phase, NUIf* ifStmt)
  {
    if (phase == ePre)
    {
      if (qualify(ifStmt))
        return eDelete;
    }
    return eSkip;
  }

  //! Don't walk inside case statements
  virtual Status operator()(Phase /* phase */, NUCase* /* caseStmt */)
  {
    return eSkip;
  }

  //! Don't walk inside for loops
  virtual Status operator()(Phase /* phase */, NUFor* /* forStmt */)
  {
    return eSkip;
  }

  //! Don't walk inside tasks
  virtual Status operator()(Phase /* phase */, NUTask* /* task */)
  {
    return eSkip;
  }

  //! This is called when we return eDelete, to generate the replacement If stmt
  virtual NUStmt* replacement(NUStmt* stmt)
  {
    NUIf* ifStmt = dynamic_cast<NUIf*>(stmt);
    NU_ASSERT(ifStmt,stmt);

    NUExpr* cond = ifStmt->getCond();
    NUBinaryOp* op = dynamic_cast<NUBinaryOp*>(cond);
    NU_ASSERT(op, cond);

    // copy the arguments out of the topmost AND or OR
    CopyContext cc(0,0);
    NUExpr* arg0 = op->getArg(0)->copy(cc);
    NUExpr* arg1 = op->getArg(1)->copy(cc);

    // move (not copy) the statement lists from the original If stmt
    NUStmtList* stmts1 = ifStmt->getThen();
    NUStmtList* stmts2 = ifStmt->getElse();

    // create the nested If stmt using just arg1
    NUIf* newIf = new NUIf(arg1, *stmts1, *stmts2,
                           mNetRefFactory, ifStmt->getUsesCFNet(), ifStmt->getLoc());

    stmts1->clear();
    stmts2->clear();
    stmts1->push_back(newIf);
    // stmts2 remains empty

    // create the outer If stmt using just arg0 and the newly created nested If stmt
    if ((op->getOp() == NUOp::eBiLogAnd) ||
        ((op->getOp() == NUOp::eBiBitAnd) && (op->getBitSize() == 1)))
    {
      // if (clk & ...) stmts   =>   if (clk)  if (...)  stmts
      newIf = new NUIf(arg0, *stmts1, *stmts2, mNetRefFactory, ifStmt->getUsesCFNet(), ifStmt->getLoc());
    }
    else
    {
      // if (clk | ...) /* empty */ else stmts   =>   if (clk) /* empty */ else if (...) /* empty */ else stmts
      newIf = new NUIf(arg0, *stmts2, *stmts1, mNetRefFactory, ifStmt->getUsesCFNet(), ifStmt->getLoc());
    }

    // record that we modified this block
    mModifiedBlocks.insert(mCurrentBlock);

    // delete the list structures and remove the statements from the original If stmt
    ifStmt->replaceThen(*stmts2);  // stmts2 is empty
    ifStmt->replaceElse(*stmts2);  // stmts2 is empty
    delete stmts1;
    delete stmts2;
    
    return newIf;
  }

  //! Iterator over blocks with re-written if statments
  NUAlwaysBlockSet::iterator beginModifiedBlocks() { return mModifiedBlocks.begin(); }

  //! End of iterator over blocks with re-written if statments  
  NUAlwaysBlockSet::iterator endModifiedBlocks() { return mModifiedBlocks.end(); }

  //! Get the set of all blocks with re-written if statements
  NUAlwaysBlockSet* getModifiedBlocks() { return &mModifiedBlocks; }
private:

  //! Check the If stmt to see if it matches the pattern we can re-write
  bool qualify(NUIf* ifStmt)
  {
    // check all defs to see if any are latches
    bool defsLatch = false;
    NUNetSet defs;
    ifStmt->getBlockingDefs(&defs);
    for (NUNetSet::iterator iter = defs.begin(); iter != defs.end(); ++iter)
    {
      NUNet* net = *iter;
      if (net->isLatch())
      {
        defsLatch = true;
        break;
      }
    }
    if (!defsLatch)
      return false;

    // the if statement must have the form:
    // if (clk & ...)
    //   stmts
    // or
    // if (clk | ...)
    // else
    //   stmts
    NUExpr* cond = ifStmt->getCond();
    if (cond->getType() != NUExpr::eNUBinaryOp)
      return false;
    
    NUBinaryOp* op = dynamic_cast<NUBinaryOp*>(cond);
    if ((op->getOp() == NUOp::eBiLogAnd) ||
        ((op->getOp() == NUOp::eBiBitAnd) && (op->getBitSize() == 1)))
    {
      // must have empty else clause
      if (!ifStmt->emptyElse())
        return false;
    }
    else if ((op->getOp() == NUOp::eBiLogOr) || (op->getOp() == NUOp::eBiBitOr))
    {
      // must have empty then clause
      if (!ifStmt->emptyThen())
        return false;
    }
    else
      return false; // not an acceptable condition expression

    return true;
  }

private:
  NUNetRefFactory* mNetRefFactory;  //!< Used when creating new Nucleus objects
  NUAlwaysBlock*   mCurrentBlock;   //!< Which NUAlwaysBlock are we currently processing?
  NUAlwaysBlockSet mModifiedBlocks; //!< Set of all rewritten NUAlwaysBlocks
};

SCHLatchAnal::~SCHLatchAnal()
{
  // Free latch status data and latch instance sets
  UtSet<LatchStatusSet*> covered;
  for (LatchMap::iterator iter = mLatchMap.begin(); iter != mLatchMap.end(); ++iter)
  {
    LatchStatus* latchStatus = iter->second;
    LatchStatusSet* instanceSet = latchStatus->getInstances();
    if (instanceSet && (covered.find(instanceSet) == covered.end()))
    {
      delete instanceSet;
      covered.insert(instanceSet);
    }
    delete latchStatus;
  }

  // Free block status data
  for (BlockMap::iterator iter = mBlockMap.begin();
       iter != mBlockMap.end();
       ++iter)
  {
    BlockStatus* blockStatus = iter->second;
    delete blockStatus;
  }

  // Free nucleus transformation data
  delete mNewAlwaysBlocks;

  // Free expr synth/resynth objects
  ESFactory* exprFactory = mPopulateExpr->getFactory();
  delete mExprResynth;
  delete mPopulateExpr;
  delete exprFactory;

  // Free fold object
  delete mFold;

  // Free BDDSource -- clear map containing BDDs first
  mBDDMap.clear();
  delete mBDDSource;

  // Re-enable expression caching in SCHClkAnal
  // why is this in the destructor? -RS
//  mClkAnal->setAllowCaching(true);
}

bool SCHLatchAnal::prepare(bool phaseStats, Stats* stats)
{
  bool success = true;
  UtStatsInterval statsInterval(phaseStats, stats);

  // Split all the latches from each other and other defs in the block
  success &= splitLatchFlows();
  statsInterval.printIntervalStatistics("Latch Split");

  // Rewrite complex latch If conditions into multiple nested Ifs
  success &= rewriteIfConditions();
  statsInterval.printIntervalStatistics("Latch Cond Rewrite");

  return success;
}

bool SCHLatchAnal::convert(const char* fileRoot, bool phaseStats, Stats* stats)
{
  bool success = true;
  UtStatsInterval statsInterval(phaseStats, stats);

  // Disable printing warnings. We only want to print warnings on
  // clocks that stick after latch conversion.
  bool savedPrintWarnings = mClkAnal->putPrintWarnings(false);

  // Find all elaborated block-level flows for latches
  success &= findLatchFlows();
  statsInterval.printIntervalStatistics("Latch Find");

  // Build control trees for latches
  success &= buildControlTrees();
  success &= checkControlTreeConsistency();
  statsInterval.printIntervalStatistics("Cntrl Trees");

  // Test exclusivity with drivers
  success &= testExclusivity();
  success &= checkControlTreeConsistency();
  statsInterval.printIntervalStatistics("Latch Excl");

  // Minimize control trees
  success &= minimizeControlTrees();
  success &= checkControlTreeConsistency();
  statsInterval.printIntervalStatistics("Min Cntrl Trees");

  // Do not convert latches that require splitting
  success &= rejectSplitBlocks();
  statsInterval.printIntervalStatistics("Rej Split");

  // Create and optimize edge sets
  success &= createEdgeSets();
  statsInterval.printIntervalStatistics("Edge Sets");

  // Write the latch data if requested
  if (fileRoot != NULL) {
    bool showBDDs = DEBUG_LATCH_ANALYSIS;
    const char* envVar = getenv("SHOW_LATCH_BDDS");
    if (envVar)
    {
      if (!strcasecmp(envVar,"off") || !strcasecmp(envVar,"no") || !strcasecmp(envVar,"false"))
        showBDDs = false;
      else
        showBDDs = true;
    }
    dumpLatchAndBlockData(fileRoot,showBDDs);
  }

  // Control tree will reference invalid data after we transform
  // nucleus, so clean it up now.
  deleteControlTrees();

  // Convert latches to flops in Nucleus
  success &= transformNucleus();
  statsInterval.printIntervalStatistics("Latch Xform");

  // Re-enable warning printing
  mClkAnal->putPrintWarnings(savedPrintWarnings);

  return success;
}

bool SCHLatchAnal::getControlTree(const FLNodeElab* latchFlow, ControlTree** tree)
{
  LatchMap::iterator p1 = mLatchMap.find(latchFlow);
  if (p1 == mLatchMap.end())
    return false;
  LatchStatus* latchStatus = p1->second;
  return latchStatus->getControlTree(tree);
}


void SCHLatchAnal::deleteControlTrees()
{
  // Free control tree data
  for (LatchMap::iterator iter = mLatchMap.begin(); iter != mLatchMap.end(); ++iter)
  {
    LatchStatus* latchStatus = iter->second;
    latchStatus->deleteControlTree();
  }
}

bool SCHLatchAnal::isLatch(FLNodeElab* flowElab) const
{
  NUUseDefNode* useDef = flowElab->getUseDefNode();
  if (useDef && (useDef->getType() == eNUAlwaysBlock) && !useDef->isSequential()) {
    NUNet* net = flowElab->getFLNode()->getDefNet();
    if (mMarkDesign->isLatch(flowElab) and not net->isVirtualNet()) {
      return true;
    }
  }
  return false;
}
  

bool SCHLatchAnal::splitLatchFlows()
{
  // Make sure we are allowed to split
  if (!mUtil->isFlagSet(eSplitBlocks)) {
    return true;
  }
  if (mUtil->isFlagSet(eDumpSplitBlocks)) {
    UtIO::cout() << "Splitting latches to improve latch-to-flop conversion:\n";
  }

  // We have to mark live nodes for block splitting to work
  mMarkDesign->markDepositableNets(mUtil->getDesign(), false);
  mMarkDesign->markLiveNodes();

  // Mark all the elaborated flows that are latches
  SCHBlockSplit blockSplit(mUtil, mMarkDesign, mScheduleData);
  FLNodeElab* flow = NULL;
  FLNodeElabFactory* factory = mUtil->getFlowElabFactory();
  NUNetRefFactory* netRefFactory = mUtil->getNetRefFactory();
  for (FLNodeElabFactory::FlowLoop loop = factory->loopFlows(); loop(&flow);) {
    if (flow->isLive() && isLatch(flow)) {
      // Check if we can create a control tree for this flow. If we
      // can't then it it not worth doing the split.  Compute the
      // ordering information for the block
      NucleusOrderCallback callback;
      NUDesignWalker walker(callback,false);
      walker.putWalkTasksFromTaskEnables(false);
      NUUseDefNode* useDef = flow->getUseDefNode();
      NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
      NU_ASSERT(always != NULL, useDef);
      walker.alwaysBlock(always);
      NucleusOrderMap* orderMap = callback.getOrderMap();

      // Compute the control tree
      ControlTree* controlTree;
      controlTree = ControlTree::controlTreeFromFlow(flow, *orderMap,
                                                     netRefFactory);
      if ((controlTree != NULL) && controlTree->isBranch()) {
        // This could be converted, split it
        blockSplit.markSplitNode(flow);
      }

      // Delete the space
      delete orderMap;
      delete controlTree;
    }
  }

  // Convert the flow nodes to block descriptions and split them
  blockSplit.convertNodesToBlocks();
  FLNodeElabSet newFlows;
  bool ok = blockSplit.splitBlocks(&newFlows, false); // do not split if task inlining is required

  // Clear the expression caches in the fold object, since
  // splitting may delete nets.
  mFold->clearExprFactory();

  return ok;
} // bool SCHLatchAnal::splitLatchFlows

bool SCHLatchAnal::rewriteIfConditions()
{
  // Do a design walk looking for non-sequential always block
  // top-level If statements that def latches and have
  // conditions of the form if (clk & ...) and rewrite
  // the nucleus to be:
  //   if (clk)
  //     if (...)
  RewriteIfConditionCallback callback(mUtil->getNetRefFactory());
  NUDesignWalker ifWalker(callback,false);
  ifWalker.design(mUtil->getDesign());
  
  // repair the UD & flow for every modified block
  UD ud(mUtil->getNetRefFactory(),
        mUtil->getMsgContext(),
        mUtil->getIODB(),
        mUtil->getArgs(),
        false);
  for (NUAlwaysBlockSet::iterator iter = callback.beginModifiedBlocks();
       iter != callback.endModifiedBlocks();
       ++iter)
  {
    NUAlwaysBlock* always = *iter;
    ud.structuredProc(always);    
  }

  FLNodeElabSet dummy;
  RETransform* transform = mUtil->getTransform();
  transform->repairBlockFlows(mUtil->getDesign(),
                              callback.getModifiedBlocks(),
                              &dummy, NULL, mUtil->getStats());

  return true;
} // bool SCHLatchAnal::rewriteIfConditions

bool SCHLatchAnal::findLatchFlows()
{
  UtMap<FLNode*,LatchStatusSet*> instances;

  FLNodeElab* flow = NULL;
  FLNodeElabFactory* factory = mUtil->getFlowElabFactory();
  for (FLNodeElabFactory::FlowLoop loop = factory->loopFlows(); loop(&flow);)
  {
    if (isLatch(flow)) {
      // Create a LatchStatus for this latch
      LatchStatus* latchStatus = new LatchStatus(flow);
      mLatchMap[flow] = latchStatus;

      // Record the instance
      LatchStatusSet* instanceSet;
      UtMap<FLNode*,LatchStatusSet*>::iterator p = instances.find(flow->getFLNode());
      if (p == instances.end())
      {
        instanceSet = new LatchStatusSet;
        instances[flow->getFLNode()] = instanceSet;
      }
      else
        instanceSet = p->second;
      instanceSet->insert(latchStatus);
      latchStatus->setInstances(instanceSet);

      // Record the block
      BlockStatus* blockStatus;
      NUUseDefNode* useDef = flow->getUseDefNode();
      NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
      BlockMap::iterator q = mBlockMap.find(always);
      if (q == mBlockMap.end())
      {
        blockStatus = new BlockStatus(always);
        mBlockMap[always] = blockStatus;
      }
      else
        blockStatus = q->second;
      blockStatus->addLatch(latchStatus);
    }
  }

  return true;
} // bool SCHLatchAnal::findLatchFlows

//! Callback class for checking if all branches uses externally countinous nets
/*! If the clock uses a net that is driven inside the block or is a
 *  block local temp then we can't create a temp clock for it because
 *  computing its reaching value is difficult. We may want to relax
 *  this in the future.
 */
class ContDrivenCallback : public ControlTreeCallback
{
public:
  //! constructor
  ContDrivenCallback(SCHLatchAnal* latchAnal, NUUseDefNode* useDef) :
    mLatchAnal(latchAnal)
  {
    useDef->getDefs(&mDefs);
  }

  //! destructor
  virtual ~ContDrivenCallback() {}

  //! override the visit routine to check branches
  virtual bool visit(ControlTree* node);

private:
  SCHLatchAnal* mLatchAnal;
  NUNetSet mDefs;
};

bool ContDrivenCallback::visit(ControlTree* node)
{
  // If this is a leaf it can't be a problem. we are only checking
  // branch conditions.
  if (node->isLeaf()) {
    return true;
  }

  // Get the clock expression we would use
  NUExpr* clock = mLatchAnal->getTreeBranchClock(node, NULL);

  // Make sure there are valid uses. This counts on the fact that
  // expression uses are self determined.  Also make sure the clock
  // nets are not locally defined. We can't tell if they are defined
  // before or after their use so we have to assume the worst case
  // (after).
  NUNetSet uses;
  clock->getUses(&uses);
  bool validUses = true;
  for (NUNetSetIter i = uses.begin(); (i != uses.end()) && validUses; ++i) {
    NUNet* net = *i;
    if (net->isTempBlockLocalNonStatic()) {
      validUses = false;
    } else if (mDefs.find(net) != mDefs.end()) {
      validUses = false;
    }
  }
  return validUses;
} // bool ContDrivenCallback::visit

bool SCHLatchAnal::buildControlTrees()
{
  // Push a new interval timer
  UtStatsInterval statsInterval(mUtil->getStats() != NULL, mUtil->getStats());

  // Build the control tree for each latch
  NucleusOrderCallback callback;
  NUDesignWalker walker(callback,false);
  walker.putWalkTasksFromTaskEnables(false);

  // Disable clock analysis for speed. It doesn't help much anyway. In
  // fact, in one case it disables the conversion because the clocks
  // are different for different instances. Clock analysis is disabled
  // by passing in a NULL pointer.
  ClkAnalBDDSource bddSource(NULL, mFold, mPopulateExpr, mExprResynth);

  NUNetRefFactory* netRefFactory = mUtil->getNetRefFactory();
  for (BlockMap::iterator iter = mBlockMap.begin();
       iter != mBlockMap.end();
       ++iter)
  {
    BlockStatus* blockStatus = iter->second;

    NUAlwaysBlock* always = blockStatus->getBlock();

    // Compute the ordering information for the block
    walker.alwaysBlock(always);
    NucleusOrderMap* orderMap = callback.getOrderMap();

    // Compute control trees for each latch in each instance of the block
    for (LatchStatusSet::iterator loop = blockStatus->beginLatches();
         loop != blockStatus->endLatches();
         ++loop)
    {
      LatchStatus* latchStatus = *loop;
      FLNodeElab* latchFlow = latchStatus->getElabFlow();

      ControlTree* controlTree;
      controlTree = ControlTree::controlTreeFromFlow(latchFlow, *orderMap,
                                                     netRefFactory);
      if (controlTree == NULL)
      {
        latchStatus->setFlags(COMPLEX_CONTROL);
        continue;
      }

      /* Note: if the latch will never load, this can return NULL.
       * We have to be sure that we properly handle a NULL control tree.
       */
      bool isValid = controlTree->pruneUse(latchFlow->getFLNode()->getDefNet());
      if (isValid)
        controlTree = controlTree->reduce(&bddSource);
      else
        controlTree = NULL;

      // If the control tree leaves do not reach continuous drivers, it indicates
      // that we stopped short due to complex data flow
      if (controlTree && !controlTree->isContinuouslyDriven())
      {
        latchStatus->setFlags(COMPLEX_DATA);
      }

      // If the control tree control flows use non-block-local nets defed in
      // the block, reject the latch candidate.
      ContDrivenCallback contDrivenCallback(this, latchFlow->getUseDefNode());
      if (controlTree && !controlTree->forAllBranches(&contDrivenCallback))
      {
        latchStatus->setFlags(COMPLEX_CONTROL);
      }

      // If we got here, we think the latch is OK
      latchStatus->setControlTree(controlTree);
    }

    delete orderMap;
  }
  statsInterval.printIntervalStatistics("Constr Trees");

  // If the control trees do not match across latch instances, mark
  // all instances of the latch
  FLNodeSet covered;
  for (LatchMap::iterator iter = mLatchMap.begin(); iter != mLatchMap.end(); ++iter)
  {
    LatchStatus* latchStatus = iter->second;
    if (covered.find(latchStatus->getFlow()) != covered.end())
      continue;
    covered.insert(latchStatus->getFlow());

    bool hasReference = false;
    ControlTree* mustMatch = NULL;
    bool reject = false;
    for (LatchStatusSet::iterator loop = latchStatus->beginInstances();
         loop != latchStatus->endInstances();
         ++loop)
    {
      LatchStatus* instanceStatus = *loop;
      ControlTree* tree;
      bool hasTree = instanceStatus->getControlTree(&tree);

      if (!hasTree)
      {
        reject = true;
        break;
      }
      if (hasReference)
      {
        if ((tree && !tree->sameControlStructure(mustMatch)) ||
            (!tree && mustMatch))
        {
          reject = true;
          break;
        }
      }
      else
      {
        mustMatch = tree;
        hasReference = true;
      }
    }

    if (reject)
    {
      // Not the same in all instances, so record this in the
      // LatchStatus for each instance
      for (LatchStatusSet::iterator loop = latchStatus->beginInstances();
           loop != latchStatus->endInstances();
           ++loop)
      {
        LatchStatus* instanceStatus = *loop;
        instanceStatus->setFlags(NON_UNIFORM);
      }
    }
  }
  statsInterval.printIntervalStatistics("Elab Trees");

  return true;
} // bool SCHLatchAnal::buildControlTrees


bool SCHLatchAnal::checkControlTreeConsistency()
{
  // Check that all trees are well-formed.
  bool all_consistent = true;
  for (LatchMap::iterator iter = mLatchMap.begin(); iter != mLatchMap.end(); ++iter)
  {
    LatchStatus* latchStatus = iter->second;

    ControlTree* tree = NULL;
    bool hasControlTree = latchStatus->getControlTree(&tree);

    if (!hasControlTree || (tree == NULL))
      continue;

    ControlTreeConsistencyCallback consistency_callback;
    bool consistent = tree->forAllNodes(&consistency_callback);

    FLNodeElab* latchFlow = latchStatus->getElabFlow();
    FLN_ELAB_ASSERT(consistent,latchFlow);
    all_consistent &= consistent;
  }
  return all_consistent;
}


bool SCHLatchAnal::testExclusivity()
{
  // Set up mTopFlowMap to find top-level flows for latches and flops
  // This has to be done here because flow is destroyed and recreated by
  // earlier passes.
  makeTopFlowMap(mUtil->getFlowElabFactory());

  // We have to notify ClkAnal about every condition we will be testing
  // before we use them to build BDDs, or else results will not be stable
  // We do this by pre-translating and all of the control tree conditions,
  // using them to seed ClkAnal and caching the translations for subsequent
  // use when building BDDs.
  SeedBDDSourceCallback seedBDDSourceCallback(mBDDSource);
  for (LatchMap::iterator iter = mLatchMap.begin(); iter != mLatchMap.end(); ++iter)
  {
    LatchStatus* latchStatus = iter->second;

    ControlTree* tree = NULL;
    bool hasControlTree = latchStatus->getControlTree(&tree);

    if (!hasControlTree || (tree == NULL))
      continue;

    tree->forAllBranches(&seedBDDSourceCallback);
    tree->forAllLeaves(&seedBDDSourceCallback);
  }
  
  // Compute stability conditions for all control tree leaves and use this
  // to determine which latches are exclusive at all leaves.
  for (LatchMap::iterator iter = mLatchMap.begin(); iter != mLatchMap.end(); ++iter)
  {
    LatchStatus* latchStatus = iter->second;

    // Don't compute exclusivity if the control tree is no good
    if (latchStatus->getFlags() & BAD_CONTROL_TREE)
      continue;

    ControlTree* tree = NULL;
    bool hasControlTree = latchStatus->getControlTree(&tree);
    FLN_ELAB_ASSERT(hasControlTree, latchStatus->getElabFlow());
    if (tree == NULL)
      continue;

    // Compute the stability condition for each leaf
    StabilityCallback callback2(this);
    tree->forAllLeaves(&callback2);

    // Determine if the latch is exclusive, meaning all leaves have
    // a stability condition which is implied by the path transparency
    // condition to the leaf.      
    ExclusivityCallback callback3(latchStatus, this);
    tree->forAllLeaves(&callback3);      
  }
  
  return true;
} // bool SCHLatchAnal::testExclusivity

//! Minimize the control trees for exclusive latches
bool SCHLatchAnal::minimizeControlTrees()
{  
  // Minimize the control trees required to maintain exclusivity.
  // This is done by finding the branches in the tree closest to the
  // root for which the read condition at the branch implies the
  // stability condition of the subtree rooted at the branch,
  // across all instances of the latch.  The chosen branches
  // are then turned into leaves with NUIf values.
  LatchStatusSet covered;
  for (LatchMap::iterator iter = mLatchMap.begin(); iter != mLatchMap.end(); ++iter)
  {
    LatchStatus* latchStatus = iter->second;
    if (covered.find(latchStatus) != covered.end())
      continue;

    UInt32 latchFlags = latchStatus->getFlags();
    if ((latchFlags & BAD_CONTROL_TREE) || (latchFlags & NON_UNIFORM))
      continue;

    bool any_instance_non_exclusive = false;

    // choose a minimal branch set across the control trees for all instances
    ControlTreeNodeAddressSet frontier;
    for (LatchStatusSet::iterator loop = latchStatus->beginInstances();
         loop != latchStatus->endInstances();
         ++loop)
    {
      LatchStatus* instanceStatus = *loop;
      covered.insert(instanceStatus);

      if (instanceStatus->isNonExclusive() or any_instance_non_exclusive) {
        any_instance_non_exclusive = true;
        continue;
      }

      ControlTree* tree = NULL;
      bool hasControlTree = instanceStatus->getControlTree(&tree);
      FLN_ELAB_ASSERT(hasControlTree, instanceStatus->getElabFlow());
      if (!tree)
        continue;

      // Get the set of branches along the exclusivity frontier
      findExclusiveBranches(tree, &frontier);
    }

    if (any_instance_non_exclusive) {
      continue;
    }

    // The frontier set now contains the union of the frontiers for all instances.
    // Convert this to be the "lowest-common-denominator" frontier.
    ControlTreeNodeAddressSet frontierCopy(frontier);
    for (ControlTreeNodeAddressSet::iterator loop = frontierCopy.begin();
         loop != frontierCopy.end();
         ++loop)
    {
      ControlTreeNodeAddress addr(*loop);
      ControlTreeNodeAddressSet::iterator p = frontier.find(addr);
      if (p == frontier.end())
        continue;
      UInt32 len = addr.length();
      while (len > 0)
      {
        --len;
        addr.truncate(len);
        frontier.erase(addr);
      }
    }
    frontierCopy.clear();

    // Use the set of node addresses to reduce each control tree.
    for (LatchStatusSet::iterator loop = latchStatus->beginInstances();
         loop != latchStatus->endInstances();
         ++loop)
    {
      LatchStatus* instanceStatus = *loop;

      ControlTree* tree = NULL;
      bool hasControlTree = instanceStatus->getControlTree(&tree);
      FLN_ELAB_ASSERT(hasControlTree, instanceStatus->getElabFlow());
      if (!tree)
        continue;

      // Convert branches along frontier to leaves
      for (ControlTreeNodeAddressSet::iterator l = frontier.begin();
           l != frontier.end();
           ++l)
      {
        const ControlTreeNodeAddress& addr = *l;
        ControlTree* node = addr.lookup(tree);
        FLN_ELAB_ASSERT(node, instanceStatus->getElabFlow());
        if (node->isLeaf())
          continue;
        ControlTree* trueLeg = node->getTrueBranch();
        if (trueLeg && trueLeg->isBranch())
          trueLeg->makeLeaf();
        ControlTree* falseLeg = node->getFalseBranch();
        if (falseLeg && falseLeg->isBranch())
          falseLeg->makeLeaf();
      }
    }   
    frontier.clear();
  }
  
  return true;
} // bool SCHLatchAnal::minimizeControlTrees

void SCHLatchAnal::findExclusiveBranches(ControlTree* tree,
                                         ControlTreeNodeAddressSet* frontier)
{
  if (!tree)
    return;

  bool recursed = false;
  if (tree->isBranch())
  {
    ControlTree* trueLeg = tree->getTrueBranch();
    if (trueLeg)
    {
      BDD transparent = trueLeg->getReadCondition(mBDDSource);
      BDD stable = computeSubtreeStability(trueLeg);
      if (!mBDDSource->implies(transparent,stable))
      {
        // look one level deeper
        findExclusiveBranches(trueLeg, frontier);
        recursed = true;
      }
    }
    
    ControlTree* falseLeg = tree->getFalseBranch();
    if (falseLeg)
    {
      BDD transparent = falseLeg->getReadCondition(mBDDSource);
      BDD stable = computeSubtreeStability(falseLeg);
      if (!mBDDSource->implies(transparent,stable))
      {
        // look one level deeper
        findExclusiveBranches(falseLeg, frontier);
        recursed = true;
      }
    }
  }

  if (!recursed) // end of recursion -- record as a frontier node
    frontier->insert(ControlTreeNodeAddress(tree));
}

BDD SCHLatchAnal::computeSubtreeStability(ControlTree* tree)
{
  INFO_ASSERT(tree, "computeSubtreeStability() called on an invalid control tree");

  // check cache
  NodeBDDMap::iterator p = mBDDMap.find(tree);
  if (p != mBDDMap.end())
  {
    BDD bdd = p->second;
    return bdd;
  }
  INFO_ASSERT(!tree->isLeaf(), // all leaves should have been in the cache
              "Unexpected leaf node in computing subtree stability");

  // compute stable condition for tree
  // (all branches must be stable, and branch condition must also be stable)
  BDD stable = mBDDSource->trueBDD();

  ControlTree* trueLeg = tree->getTrueBranch();
  if (trueLeg)
    stable = mBDDSource->opAnd(stable, computeSubtreeStability(trueLeg));

  ControlTree* falseLeg = tree->getFalseBranch();
  if (falseLeg)
    stable = mBDDSource->opAnd(stable, computeSubtreeStability(falseLeg));

  StabilityCallback callback(this);
  callback.visit(tree); // will store value in map, which is then used and overwritten  
  stable = mBDDSource->opAnd(stable, mBDDMap[tree]);

  // update cache
  mBDDMap[tree] = stable;

  return stable;
}


bool SCHLatchAnal::rejectSplitBlocks()
{
  // For each block containing latches, if the block:
  //  - mixes latch continuous drivers and non-latch continuous drivers, or
  //  - has multiple latch continuous drivers with unequal control structures, or
  //  - contains any non-exclusive latches, or
  //  - does not contain any latch which is in a path between other latches
  // then we do not want to try to convert the block.
  for (BlockMap::iterator iter = mBlockMap.begin();
       iter != mBlockMap.end();
       ++iter)
  {
    BlockStatus* blockStatus = iter->second;
    NUAlwaysBlock* always = blockStatus->getBlock();

    // get all of the continuously driven nets defed in the block
    NUNetSet remainingDefs;
    always->getDefs(&remainingDefs);

    ControlTree* mustMatch = NULL;
    FLNodeElab* mustMatchFlow = NULL;
    ControlTree* tree = NULL;
    bool firstNet = true;

    // check that all latch instances in the block are exclusive
    // and have the same control structure.
    for (LatchStatusSet::iterator loop = blockStatus->beginLatches();
         loop != blockStatus->endLatches();
         ++loop)
    {
      LatchStatus* latchStatus = *loop;
      FLNodeElab* latchFlow = latchStatus->getElabFlow();
  
      // remove the latch net from the remaining defs
      NUNet* defNet = latchFlow->getFLNode()->getDefNet();
      remainingDefs.erase(defNet);

      // the latch must be exclusive
      if (latchStatus->isNonExclusive())
      {
        blockStatus->setFlags(NON_EXCLUSIVE_LATCH);
        blockStatus->setSpoiler(latchStatus->getElabFlow());
        continue;
      }

      // the latch must have a control tree without complex data or
      // control (rescoped nets)
      bool hasControlTree = latchStatus->getControlTree(&tree);
      bool complexData = (latchStatus->getFlags() & COMPLEX_DATA) != 0;
      bool complexControl = (latchStatus->getFlags() & COMPLEX_CONTROL) != 0;
      if (!hasControlTree || complexData || complexControl)
      {
        blockStatus->setFlags(NO_CONTROL_TREE);
        blockStatus->setSpoiler(latchStatus->getElabFlow());
        continue;
      }

      // and it must match all others in the block
      if (firstNet)
      {
        // first net -- store as reference for future comparison
        mustMatch = tree;
        firstNet = false;
        mustMatchFlow = latchStatus->getElabFlow();
      }
      else
      {
        // check against reference control tree
        if ((tree && !tree->sameControlStructure(mustMatch)) ||
            ((tree == NULL) && (mustMatch != NULL)))
        {
          blockStatus->setFlags(DIFFERENT_CONTROL_TREES);
          blockStatus->setSpoiler(mustMatchFlow);
          blockStatus->setSpoiler(latchStatus->getElabFlow());
          continue;
        }
      }
    }

    // if the latches did not cover all continuous drivers, reject it
    for (NUNetSet::iterator n = remainingDefs.begin(); n != remainingDefs.end(); ++n)
    {
      NUNet* net = *n;
      if (net->isContinuouslyDriven() && !net->isDead())
      {
        blockStatus->setFlags(NON_LATCH);
        break;
      }
    }
  }

  return true;
} // bool SCHLatchAnal::rejectSplitBlocks

bool SCHLatchAnal::createEdgeSets()
{
  // For blocks with exclusive latches that don't require splitting,
  // create a map from block to set of flop descriptions.
  // Each flop description is an edge expression and a priority pointer.
  for (BlockMap::iterator iter = mBlockMap.begin();
       iter != mBlockMap.end();
       ++iter)
  {
    BlockStatus* blockStatus = iter->second;
    NUAlwaysBlock* always = blockStatus->getBlock();

    if (blockStatus->getFlags() != BLOCK_OK)
      continue;

    // Get any control tree for the block.
    // (We know all latches in the block have the same contol structure)
    LatchStatus* latchStatus = *(blockStatus->beginLatches());
    ControlTree* tree = NULL;
    bool hasControlTree = latchStatus->getControlTree(&tree);
    NU_ASSERT(hasControlTree,always);

    // If the latch will never load, don't convert it (for now...)
    if (!tree)
    {
      blockStatus->setFlags(CAN_NEVER_LOAD);
      continue;
    }

    // If the latch will always load, we can't convert it at all
    if (willAlwaysLoad(tree))
    {
      blockStatus->setFlags(WILL_ALWAYS_LOAD);
      continue;
    }

    // If the control tree is not single-spine, don't convert it (for now...)
    // This due to restrictions created by our clock/priority block system.
    if (!tree->isSingleSpine())
    {
      blockStatus->setFlags(NON_SINGLE_SPINE);
      continue;
    }

    // Ok, we have an always block we want to convert to flops.
    // We need to create a flop description for each edge required
    populateFlops(blockStatus, tree);
  }

  return true;
} // bool SCHLatchAnal::createEdgeSets

//! Design walker callback for finding dynamic lvalues
class DynamicLvalueChecker : public NUDesignCallback
{
public:
  DynamicLvalueChecker() : mFoundDynamicLvalue(false) {}

  bool foundDynamicLvalue() const { return mFoundDynamicLvalue; }

  //! Do nothing for most constructs
  virtual Status operator()(Phase, NUBase*) { return eNormal; }

  //! Disallow a non-constant bit or part select
  virtual Status operator()(Phase phase, NUVarselLvalue* varsel)
  {
    if ((phase == ePre) && !varsel->isConstIndex())
    {
      mFoundDynamicLvalue = true;
      return eStop;
    }

    return eNormal;
  }

  //! Disallow any memory access
  virtual Status operator()(Phase /* phase */, NUMemselLvalue* /* memsel */)
  {
    mFoundDynamicLvalue = true;
    return eStop;
  }

private:
  bool mFoundDynamicLvalue;
};

//! Class used to determine if all leaves of a control tree def the same bits
class AllLeafDefTester : public ControlTreeCallback
{
public:
  AllLeafDefTester(NUNetRefFactory* netRefFactory)
    : mNetRefFactory(netRefFactory), mDefs(netRefFactory),
      mFirstLeaf(true), mWillAlwaysLoad(true)
  {}

  bool visit(ControlTree* node)
  {
    INFO_ASSERT(node->isLeaf(), "AllLeafDefTester should only be used on leaves!");

    // Collect defs and check for dynamic Lvalues in all leaf flows
    NUNetRefSet leafDefs(mNetRefFactory);
    for (FLNodeElabSet::const_iterator i = node->beginLeafFlows();
         i != node->endLeafFlows();
         ++i)
    {
      FLNodeElab* flow = *i;
      NUStmt* stmt = dynamic_cast<NUStmt*>(flow->getUseDefNode());
      if (stmt)
      {
        DynamicLvalueChecker callback;
        NUDesignWalker walker(callback,false);
        walker.stmt(stmt);
        if (callback.foundDynamicLvalue())
        {
          // found dynamic lvalue, we can abort right away
          mWillAlwaysLoad = false;
          return false;
        }
        // no dynamic lvalue, so we can collect the defnet refs
        if (stmt->useBlockingMethods())
        {
          stmt->getBlockingDefs(&leafDefs);
          stmt->getNonBlockingDefs(&leafDefs);
        }
        else
        {
          stmt->getDefs(&leafDefs);
        }
      }
      else
      {
        // this was unexpected leaf flow
        FLN_ELAB_ASSERT("Leaf flow is not for a statement!" == NULL,flow);
        mWillAlwaysLoad = false;
        return false;
      }
    }

    // If we get here, we have a set defnet refs that indicate only actually
    // written bits (there is no dynamic lvalue).  Check that all subsequent
    // leaves have the same defnet refs as the first.
    if (mFirstLeaf)
    {
      // Copy defs
      mDefs.insert(leafDefs.begin(),leafDefs.end());
      mFirstLeaf = false;
    }
    else
    {
      // Check against previous defs
      if (leafDefs != mDefs)
      {
        mWillAlwaysLoad = false;
        return false;
      }
    }

    // All the leaves must have defed the same bits, with no dynamic lvalues
    return true;
  }

  bool willAlwaysLoad() const { return mWillAlwaysLoad; }
private:
  NUNetRefFactory* mNetRefFactory;
  NUNetRefSet      mDefs;
  bool             mFirstLeaf;
  bool             mWillAlwaysLoad;
};

//! Test if a "latch" will always overwrite all of its bits
bool SCHLatchAnal::willAlwaysLoad(ControlTree* tree) const
{
  // A leaf is definitely not a latch
  if (tree->isLeaf())
    return true;

  // It must be fully populated so that it loads under all conditions
  if (!tree->isFullyPopulated())
    return false;

  // Each leaf must def all of the bits
  // We test this by ensuring that they all have the same defnetrefs
  // and that none of the statements has a dynamic select in the lvalue.
  AllLeafDefTester tester(mUtil->getNetRefFactory());
  tree->forAllLeaves(&tester);
  return tester.willAlwaysLoad();
}

void SCHLatchAnal::populateFlops(BlockStatus* blockStatus, ControlTree* tree,
                                 FlopDescription* priority)
{
  if (!tree || tree->isLeaf())
    return;

  // get clock expression -- synthesize back to continuous drivers
  NUExpr* origClock;
  NUExpr* clock;
  clock = getTreeBranchClock(tree, &origClock);

  // build posedge and negedge clock flops as needed
  FlopDescription* pos = NULL;
  FlopDescription* neg = NULL;
  ControlTree* trueBranch = tree->getTrueBranch();
  if (trueBranch)
  {
    // We make a copy of the original clock because it might get deleted
    // by the conversion process. It is up to the flop description
    // destruction to delete it.
    CopyContext cntxt(NULL, NULL);
    NUExpr* origClockCopy = origClock->copy(cntxt);
    if (clock == origClock) {
      clock = origClockCopy;
    }

    pos = FlopDescription::posedge(clock, origClockCopy, priority);
    blockStatus->addFlop(pos);
  }

  ControlTree* falseBranch = tree->getFalseBranch();
  if (falseBranch)
  {
    // We make a copy of the original clock because it might get deleted
    // by the conversion process. It is up to the flop description
    // destruction to delete it.
    CopyContext cntxt(NULL, NULL);
    NUExpr* origClockCopy = origClock->copy(cntxt);
    if (clock == origClock) {
      clock = origClockCopy;
    }

    neg = FlopDescription::negedge(clock, origClockCopy, priority);
    blockStatus->addFlop(neg);
  }
  
  // recurse down tree to add edges, propagating priority blocks
  populateFlops(blockStatus,trueBranch,neg);
  populateFlops(blockStatus,falseBranch,pos);
}

NUExpr* SCHLatchAnal::getTreeBranchClock(ControlTree* tree, NUExpr** origClock)
{
  // Get the original clock from the always block. If the caller wants
  // the original expression as well, return that.
  FLNode* flow = tree->getBranchFlow()->getFLNode();
  NUExpr* clock = tree->getIfCondExpr();
  if (origClock != NULL) {
    *origClock = clock;
  }

  // Resynthesize the expression in terms of continous drivers. If
  // that fails, we return the original clock.
  ContinuousDriverSynthCallback callback(mPopulateExpr, flow);
  ESExprHndl hndl = mPopulateExpr->createIfCondition(flow, &callback);
  const SourceLocator& loc = flow->getUseDefNode()->getLoc();
  CarbonExpr* cExpr = hndl.getExpr();
  if (cExpr)
    clock = const_cast<NUExpr*>(mExprResynth->exprToNucleus(cExpr, NULL, loc));
  return clock;
} // NUExpr* SCHLatchAnal::getTreeBranchClock

// Return the top-level flow given nested flow for a latch or flop
FLNodeElab* SCHLatchAnal::getTopLevelFlow(const FLNodeElab* flow)
{
  UtMap<const FLNodeElab*,FLNodeElab*>::iterator p = mTopFlowMap.find(flow);
  return (p != mTopFlowMap.end()) ? p->second : NULL;
}

bool SCHLatchAnal::transformNucleus()
{
  for (BlockMap::iterator iter = mBlockMap.begin();
       iter != mBlockMap.end();
       ++iter)
  {
    BlockStatus* blockStatus = iter->second;

    if (blockStatus->getFlags() != BLOCK_OK)
      continue;

    NU_ASSERT(blockStatus->numFlops() > 0, blockStatus->getBlock());

    createFlops(blockStatus);
  }

  // Optimize the nucleus code and redo unelaborated and elaborated
  // flow
  mUtil->getTransform()->fixupNewFlops(mUtil->getDesign(), mUtil->getStats());
  
  // Tell the rest of the scheduler about all the clocks used as data
  // for which we should not insert delays. This must occur after the
  // above fixups because we want all optimizations of clocks as
  // constants to propagate.
  markLatchClocks();

  return true;
} // bool SCHLatchAnal::transformNucleus


class LatchCmp
{
public:
  bool operator()(const LatchStatus* l1, const LatchStatus* l2)
  {
    ptrdiff_t cmp = FLNodeElab::compare(l1->getElabFlow(), l2->getElabFlow());
    return (cmp < 0);
  }
};

class BlockCmp
{
public:
  bool operator()(const BlockStatus* b1, const BlockStatus* b2)
  {
    int cmp = NUUseDefNode::compare(b1->getBlock(), b2->getBlock());
    return (cmp < 0);
  }
};

//! Dump lots of debugging data
void SCHLatchAnal::dumpLatchAndBlockData(const char* fileRoot, bool showBDDs)
{
  // set up for output to stdout or file
  FILE* f = stdout; 
  if (fileRoot != NULL)
  {
    UtString fname(fileRoot);
    fname << ".latches";
    UtString reason;
    f = OSFOpen(fname.c_str(), "w", &reason);
    if (f == NULL)
    {
      fprintf(stderr, "%s\n", reason.c_str());
      return;
    }
  }
  UtOFileStream out(f);
  UtString buf;

  // collect and write summary data

  UInt32 numLatches = mLatchMap.size();
  UInt32 numBlocks  = mBlockMap.size();
  UInt32 numConvertedBlocks = 0;
  UInt32 numConvertedLatches = 0;
  UInt32 numFlopsCreated = 0;
  UInt32 numLatchesBadControlTree = 0;
  UInt32 numLatchesFedByPI = 0;
  UInt32 numLatchesNonExcl = 0;
  UInt32 numLatchesExcl = 0;

  for (BlockMap::iterator iter = mBlockMap.begin(); iter != mBlockMap.end(); ++iter)
  {
    BlockStatus* blockStatus = iter->second;
    
    if (blockStatus->numFlops() > 0)
    {
      ++numConvertedBlocks;
      numConvertedLatches += blockStatus->numLatches();
      numFlopsCreated += blockStatus->numLatches() * blockStatus->numFlops();
    }

    for (LatchStatusSet::iterator loop = blockStatus->beginLatches();
         loop != blockStatus->endLatches();
         ++loop)
    {
      LatchStatus* latchStatus = *loop;
      UInt32 latchFlags = latchStatus->getFlags();

      if (latchFlags & BAD_CONTROL_TREE)
        ++numLatchesBadControlTree;
      else if (latchFlags & FED_BY_PI)
        ++numLatchesFedByPI;
      else if (latchFlags & NON_EXCL_DRIVER)
        ++numLatchesNonExcl;
      else
        ++numLatchesExcl;
    }
  }

  out << "Converted " << numConvertedBlocks << " always blocks ";
  if ( numBlocks != 0 ) {
    out << "(" << (100.0 * numConvertedBlocks / numBlocks) << "%) ";
  }
  out << "containing " << numConvertedLatches << " latches ";
  if ( numLatches != 0 ) {
    out << "(" << (100.0 * numConvertedLatches / numLatches) << "%) ";
  }
  out << "into " << numFlopsCreated << " flops.\n\n";

  out << numLatches << " total latches in the design.\n";
  out << numLatchesBadControlTree << " latches ";
  if  (numLatches != 0) {
    out << "(" << (100.0 * numLatchesBadControlTree / numLatches) << "%) ";
  }
  out << "cannot be analyzed because of their control or data path complexity.\n";
  out << numLatchesFedByPI << " latches ";
  if ( numLatches != 0 ) {
    out << "(" << (100.0 * numLatchesFedByPI / numLatches) << "%) ";
  }
  out << "cannot be converted because they are fed by a primary input.\n";
  out << numLatchesNonExcl << " latches ";
  if ( numLatches != 0 ) {
    out << "(" << (100.0 * numLatchesNonExcl / numLatches) << "%) ";
  }
  out << "cannot be converted because they have non-exclusive drivers.\n";
  out << numLatchesExcl << " latches ";
  if ( numLatches != 0 ) {
    out << "(" << (100.0 * numLatchesExcl / numLatches) << "%) ";
  }
  out << "are exclusive with all of their drivers.\n";

  // created sorted vectors for stability

  UtVector<LatchStatus*> sortedLatches;
  UtVector<BlockStatus*> sortedBlocks;

  for (LatchMap::iterator iter = mLatchMap.begin(); iter != mLatchMap.end(); ++iter)
  {
    LatchStatus* latchStatus = iter->second;
    sortedLatches.push_back(latchStatus);
  }

  for (BlockMap::iterator iter = mBlockMap.begin(); iter != mBlockMap.end(); ++iter)
  {
    BlockStatus* blockStatus = iter->second;
    sortedBlocks.push_back(blockStatus);
  }

  LatchCmp latchCmp;
  std::sort(sortedLatches.begin(), sortedLatches.end(), latchCmp);

  BlockCmp blockCmp;
  std::sort(sortedBlocks.begin(), sortedBlocks.end(), blockCmp);

  // write latch details

  out << "\n========================================================\n";
  out << "                      LATCH DUMP\n";
  out << "========================================================\n";

  for (UtVector<LatchStatus*>::iterator iter = sortedLatches.begin();
       iter != sortedLatches.end();
       ++iter)
  {
    LatchStatus* latchStatus = *iter;
    FLNodeElab* latchFlow = latchStatus->getElabFlow();
    STBranchNode* latchHier = latchFlow->getHier();
    NUUseDefNode* useDef = latchFlow->getUseDefNode();
    NUNet* net = latchFlow->getFLNode()->getDefNet();

    buf.clear();
    net->compose(&buf, latchHier);
    out << "Latch '" << buf << "' in block:\n";
    buf.clear();
    useDef->compose(&buf, NULL, 2, true);
    out << buf;

    UInt32 latchFlags = latchStatus->getFlags();

    // Report control tree
    ControlTree* tree = NULL;
    bool hasControlTree = false;
    if (latchFlags & BAD_CONTROL_TREE)
    {
      out << "Could not build a control tree because ";
      if (latchFlags & COMPLEX_CONTROL)
        out << "the latch control structure is too complex";
      else if (latchFlags & COMPLEX_DATA)
        out << "the latch data path is too complex";
      out << ".\n";
    }
    else
    {
      hasControlTree = latchStatus->getControlTree(&tree);
      FLN_ELAB_ASSERT(hasControlTree, latchStatus->getElabFlow());
      if (tree)
      {
        out << "Has control tree:\n";
        buf.clear();
        tree->compose(&buf,2);
        out << buf;
      }
      else
      {
        out << "Will never load.\n";
      }
    }
    if (latchFlags & NON_UNIFORM)
      out << "This control tree differs from some other instance of this latch!\n";

    // Report exclusivity data
    if (latchStatus->isNonExclusive())
    {
      if ((latchFlags & (FED_BY_PI | NON_EXCL_DRIVER)) == (FED_BY_PI | NON_EXCL_DRIVER))
        out << "This latch is fed by primary inputs AND has non-exclusive drivers.\n";
      else if (latchFlags & FED_BY_PI)
        out << "This latch is fed by a primary input.\n";
      else
        out << "This latch has a non-exclusive driver.\n";

      const ControlTree* lastLeaf = NULL;
      BDD transparent;
      for (FaninIDSet::const_iterator loop = latchStatus->beginNonExclusiveFanins();
           loop != latchStatus->endNonExclusiveFanins();
           ++loop)
      {
        const FaninID& faninID = *loop;
        
        const ControlTree* leaf = faninID.first;
        const FLNodeElab* fanin = faninID.second;

        // describe leaf if it has changed
        if (leaf != lastLeaf)
        {
          lastLeaf = leaf;
          out << "In the assignment:\n";
          for (FLNodeElabSet::const_iterator l = leaf->beginLeafFlows();
               l != leaf->endLeafFlows();
               ++l)
          {
            FLNodeElab* leafFlow = *l;
            NUUseDefNode* leafUseDef = leafFlow->getUseDefNode();
            buf.clear();
            leafUseDef->compose(&buf, NULL, 2, true);
            out << buf;
          }
          if (showBDDs)
          {
            transparent = leaf->getReadCondition(mBDDSource);
            out << "which the latch reads when:\n";
            buf.clear();
            transparent.compose(&buf);
            out << buf << "\n";
          }
        }

        // describe fanin
        NUUseDefNode* faninUseDef = fanin->getUseDefNode();
        NUNet* net = fanin->getFLNode()->getDefNet();
        STBranchNode* hier = fanin->getHier();
        buf.clear();
        net->compose(&buf, hier);
        bool isPI = (faninUseDef == NULL);
        bool isFlop = faninUseDef && (faninUseDef->isSequential());
        if (isPI)
          out << "the fanin reaches the primary input '" << buf << "'\n";
        else
        {
          if (isFlop)
            out << "the fanin reaches the flop '" << buf << "'";
          else
            out << "the fanin reaches the latch '" << buf << "'";
          if (showBDDs)
          {
            const FLNodeElab* topFlow = getTopLevelFlow(fanin);            
            BDD stable = getStabilityCondition(this,topFlow);
            out << " which is stable when:\n";
            buf.clear();
            stable.compose(&buf);
            out << buf << "\n";
            BDD unstableRead = mBDDSource->opAnd(transparent,mBDDSource->opNot(stable));
            out << "meaning the assignment can be read while unstable when:\n";
            buf.clear();
            unstableRead.compose(&buf);
            out << buf << "\n";
          }
          else
            out << "\n";
        }
      }
    }
    else if (!(latchFlags & BAD_CONTROL_TREE))
    {
      out << "This latch is exclusive with all of its drivers.\n";
    }

    out << "========================================================\n";
  }

  // write block details

  out << "\n========================================================\n";
  out << "                      BLOCK DUMP\n";
  out << "========================================================\n";

  for (UtVector<BlockStatus*>::iterator iter = sortedBlocks.begin();
       iter != sortedBlocks.end();
       ++iter)
  {
    BlockStatus* blockStatus = *iter;
    NUAlwaysBlock* always = blockStatus->getBlock();
    
    UInt32 blockFlags = blockStatus->getFlags();

    // report block
    out << "The following block accounting for " << blockStatus->numLatches() << " latch instances:\n";
    buf.clear();
    always->compose(&buf, NULL, 2, true);
    out << buf;

    if (blockFlags == BLOCK_OK)
    {
      out << "Was converted to a set of " << blockStatus->numFlops() << " flops:\n";
      for (FlopDescriptionSet::iterator f = blockStatus->beginFlops();
           f != blockStatus->endFlops();
           ++f)
      {
        FlopDescription* flopDesc = *f;
        buf.clear();
        flopDesc->compose(&buf,2,true);
        out << buf << "\n";
      }
    }
    else
    {
      UtString buf;
      FLNodeElab* spoiler = blockStatus->getSpoiler();
      if (spoiler)
        spoiler->getFLNode()->getDefNet()->compose(&buf, spoiler->getHier());

      out << "Was NOT converted because ";
      if (blockFlags & NON_LATCH)
      {
        out << "it mixes latch and non-latch nets.\n";
      }
      else if (blockFlags & NO_CONTROL_TREE)
      {
        out << "it contains a latch (" << buf << ")\n"
            << "whose control structure could not be analyzed.\n";
      }
      else if (blockFlags & NON_EXCLUSIVE_LATCH)
      {
        out << "it contains a latch (" << buf << ")\n"
            << "which is not exclusive with all drivers.\n";
        int numExclusive = 0;
        int numNonExclusive = 0;
        for (LatchStatusSet::iterator loop = blockStatus->beginLatches();
             loop != blockStatus->endLatches();
             ++loop) {
          LatchStatus* latchStatus = *loop;
          if (latchStatus->isNonExclusive()) {
            ++numNonExclusive;
          } else {
            ++numExclusive;
          }
        }
        out << "  there were " << numNonExclusive << " non-exclusive latches and "
            << numExclusive << " exclusive latches.\n";
      }
      else if (blockFlags & DIFFERENT_CONTROL_TREES)
      {
        out << "it contains latches (" << buf << " is one)\n"
            << "with a control structure different from other latches in the block.\n";
      }
      else if (blockFlags & NON_SINGLE_SPINE)
      {
        out << "its control structure is not single-spined.\n";
      }
      else if (blockFlags & CAN_NEVER_LOAD)
      {
        out << "it will never load.\n";
      }
      else if (blockFlags & WILL_ALWAYS_LOAD)
      {
        out << "it does not describe a latch (it will always load a value).\n";
      }
    }

    out << "========================================================\n";
  }
  if (fileRoot != NULL) {
    fclose(f);
  }
}

#if 0
bool SCHLatchAnal::checkInvariants() const
{
  UInt32 latchFlows = 0;
  for (LatchFlowMap::const_iterator iter = mLatchFlowMap.begin();
       iter != mLatchFlowMap.end();
       ++iter)
  {
    const FLNodeElabSet& flowSet = iter->second;
    latchFlows += flowSet.size();
  }
  if (latchFlows != mNumLatches - mPartialNetLatches)
  {
    INFO_ASSERT(0, "latch count does not match flow node total");
    return false;
  }

  if (mNumLatches < mBlockLatchMap.size())
  {
    INFO_ASSERT(0, "Fewer latch instances than always blocks containing latches!");
    return false;
  }
  
  if (mNumLatches < mLatchFlowMap.size())
  {
    INFO_ASSERT(0, "Fewer latch instances than latch nets!");
    return false;
  }

  if (mLatchFlowMap.size() < mControlMap.size())
  {
    INFO_ASSERT(0, "Fewer latch nets than nets with control trees!");
    return false;
  }

  if (mNumLatches < mComplexControlLatches)
  {
    INFO_ASSERT(0, "Fewer latch instances than complex control latch instances!");
    return false;
  }

  UInt32 numSimpleControlLatches = mNumLatches - mComplexControlLatches;
  if (numSimpleControlLatches < mControlMap.size())
  {
    INFO_ASSERT(0, "Fewer latch instances with control trees than nets with control trees!");
    return false;
  }

  if (mConvertable.size() > mNumLatches)
  {
    INFO_ASSERT(0, "More convertable latches than total latches!");
    return false;
  }

  if (mRequireSplit.size() > mBlockLatchMap.size())
  {
    INFO_ASSERT(0, "More blocks that require splitting than total blocks with latches!");
    return false;
  }
  
  return true;
} // bool SCHLatchAnal::checkInvariants
#endif

//! Populate the map from nested flow to top-level flow for latches & flops
void SCHLatchAnal::makeTopFlowMap(FLNodeElabFactory* factory)
{
  FLNodeElab* flow = NULL;
  for (FLNodeElabFactory::FlowLoop loop = factory->loopFlows(); loop(&flow);)
  {
    NUUseDefNode* useDef = flow->getUseDefNode();
    if (useDef && (useDef->getType() == eNUAlwaysBlock))
    {
      FLNodeElabIter* iter = flow->makeFaninIter(FLIterFlags::eAll,
                                                 FLIterFlags::eLeafNode,
                                                 FLIterFlags::eIntoAll,
                                                 true,
                                                 FLIterFlags::eIterNodeOnce);
      while (!iter->atEnd())
      {
        FLNodeElab* nested = iter->getCur();
        mTopFlowMap[nested] = flow;
        iter->next();
      }
      delete iter;
    }
  }
}

void 
SCHLatchAnal::createFlops(BlockStatus* blockStatus)
{
  // Record all the clocks for this latch
  for (FlopDescriptionSet::iterator iter = blockStatus->beginFlops();
       iter != blockStatus->endFlops();
       ++iter) {
    const FlopDescription* flopDesc = *iter;
    recordClock(blockStatus->getBlock(), flopDesc);
  }

  // Create a new flop for every description. Create the priority
  // blocks first so that we can pass that to the creation code.
  FlopToNewAlwaysMap covered;
  for (FlopDescriptionSet::iterator iter = blockStatus->beginFlops();
       iter != blockStatus->endFlops();
       ++iter) {
    // Create a flop for this description if it hasn't been done before.
    const FlopDescription* flopDesc = *iter;
    createFlop(blockStatus->getBlock(), flopDesc, &covered);
  }
} // SCHLatchAnal::createFlops

void
SCHLatchAnal::recordClock(NUAlwaysBlock* oldAlways,
                          const FlopDescription* flopDesc)
{
  NUExpr* clk = flopDesc->getClock();
  NUExpr* origClk = flopDesc->getOrigClock();
  bool inverted = flopDesc->wasInverted();
  mUtil->getTransform()->recordLatchClock(oldAlways, clk, origClk, inverted);
}

NUAlwaysBlock*
SCHLatchAnal::createFlop(NUAlwaysBlock* oldAlways,
                         const FlopDescription* flopDesc,
                         FlopToNewAlwaysMap* covered)
{
  // Check if we have created this before. If so, we must have created
  // its priority as well. So just return the saved always block.
  NUAlwaysBlock* always;
  FlopToNewAlwaysMap::iterator pos = covered->find(flopDesc);
  if (pos != covered->end()) {
    always = pos->second;
  } else {
    // Create any priority blocks first
    NUAlwaysBlock* priority = NULL;
    FlopDescription* priorityDesc = flopDesc->getPriorityBlock();
    if (priorityDesc != NULL) {
      priority = createFlop(oldAlways, priorityDesc, covered);
    }

    // Create this flop
    NUExpr* clk = flopDesc->getClock();
    ClockEdge edge = flopDesc->getEdge();
    RETransform* transform = mUtil->getTransform();
    always = transform->convertLatchToFlop(oldAlways, edge, clk, priority);

    // Add to the covered set so that any lower priority blocks can find it
    covered->insert(FlopToNewAlwaysMap::value_type(flopDesc, always));

    // Remember this as a latch so that we don't add delays to the
    // clocks. The right way to fix this is to have the edge
    // expression reflect the nets we want to see the new value
    // for. But this requires redoing how we store this info
    // (clock/priority block pointers). So for now remember these
    // always blocks so we can use the new values for clocks.
    (*mNewAlwaysBlocks)[oldAlways].push_back(always);
  }
  return always;
} // SCHLatchAnal::createFlop

void SCHLatchAnal::markLatchClocks()
{
  typedef LoopMap<NewAlwaysBlocks> NewAlwaysBlocksLoop;

  // Dead code analysis may have determined that some of the
  // constructed always blocks are now dead. Usually, this happens
  // when different branches of the control tree defines different
  // parts of the original net. 
  //
  // For example:
  //
  // always @(ctl1 or ctl2) begin
  //   if (ctl1) tmp[0] = 1'b0;
  //   else 
  //     if (ctl2) 
  //       tmp[1] = 1'b0;
  // end
  // assign out0 = tmp[0];
  //
  // Here, only 'tmp[0]' is live, yet this is not realized until the
  // always block is converted to multiple sequential blocks.

  NUAlwaysBlockSet remaining_latch_blocks;
  {
    NUAlwaysBlockSet created_latch_blocks;
    for (NewAlwaysBlocksLoop l(*mNewAlwaysBlocks); !l.atEnd(); ++l) {
      const NUAlwaysBlockVector& alwaysBlocks = l.getValue();
      created_latch_blocks.insert(alwaysBlocks.begin(),alwaysBlocks.end());
    }

    NUAlwaysBlockDiscoveryCallback callback(created_latch_blocks,
                                            remaining_latch_blocks);
    NUDesignWalker walker(callback,false);
    walker.design(mUtil->getDesign());
  }

  // use the map from old to new always blocks to get the set of new
  // always blocks created.
  for (NewAlwaysBlocksLoop l(*mNewAlwaysBlocks); !l.atEnd(); ++l) {
    // Gather all the clocks for this set of always blocks. We need
    // all of them in a searchable set before we test each always
    // block.
    const NUAlwaysBlockVector& alwaysBlocks = l.getValue();
    NUNetSet clocks;
    for (NUAlwaysBlockVectorCLoop a(alwaysBlocks); !a.atEnd(); ++a) {
      NUAlwaysBlock* always = *a;
      if (remaining_latch_blocks.find(always) == remaining_latch_blocks.end()) {
        continue;
      }

      NUEdgeExpr* edgeExpr = always->getEdgeExpr();
      edgeExpr->getUses(&clocks);
    }

    // Walk the always blocks again and look for any clock uses. All
    // of those should be recorded for buffer insertion to avoid them.
    for (NUAlwaysBlockVectorCLoop a(alwaysBlocks); !a.atEnd(); ++a) {
      NUAlwaysBlock* always = *a;
      if (remaining_latch_blocks.find(always) == remaining_latch_blocks.end()) {
        continue;
      }

      NUBlock* block = always->getBlock();
      NUNetSet uses;
      block->getBlockingUses(&uses);
      for (NUNetSetIter n = clocks.begin(); n != clocks.end(); ++n) {
        NUNet* clkNet = *n;
        if (uses.find(clkNet) != uses.end()) {
          // Remember this clock use by a latch
          mMarkDesign->addLatchClockUse(always, clkNet);
        }
      }
    }
  } // for
} // void SCHLatchAnal::markLatchClocks

bool FaninIDCmp::operator()(const FaninID& fid1, const FaninID& fid2) const
{
  // Compare the control trees first, then by the elaborated flow
  int cmp = ControlTree::compare(fid1.first, fid2.first);
  if (cmp == 0) {
    ptrdiff_t cmp1 = FLNodeElab::compare(fid1.second, fid2.second);
    if (cmp1 < 0) {
      cmp = -1;
    } else if (cmp1 > 0) {
      cmp = 1;
    }
  }
  return cmp < 0;
}

void BlockStatus::setSpoiler(FLNodeElab* spoiler)
{
  if (mSpoiler == NULL) {
    mSpoiler = spoiler;
  } else if (FLNodeElab::compare(spoiler, mSpoiler) < 0) {
    mSpoiler = spoiler;
  }
}
