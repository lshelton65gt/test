// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  File to test whether a block is mergable or not
*/

#ifndef _MERGETEST_H_
#define _MERGETEST_H_

class SCHFlowGroups;

#include "SeqFanout.h"

//! Class to analyze blocks to see if they are mergable
/*! This class is used to analyze sequential and combinational blocks
 *  to see if they are mergable. It also keeps track of statistics on
 *  those blocks for printing verbose information.
 *
 *  The class can be used in one of three modes: sequential only,
 *  combinational only, or combinatinational merged into
 *  sequential. Some of the tessts done are unique to the mode, and
 *  some are general. See the enum MergeTypes for details.
 *
 *  One test cannot be done within this code: all blocks that are part
 *  of a false cycle introduced by mapping the elaborated flow graph
 *  onto an unelaborated view of the design. These have to be reported
 *  by the caller through the recordCycleBlock() function. The only
 *  benefit to that function is to keep the statistics accurate.
 *
 *  A block to be merged is a combinational of an NUUseDefNode and an
 *  array of flow vectors. Each flow vector represents a different
 *  instance of the NUUseDefNode. See the SCHFlowGroups class for
 *  details on how to construct the vectors.
 *
 *  To keep the statistics accurate it is left up to the caller to
 *  only call the mergable() function once per block.
 *
 *  An optional unmergableNets set can be passed in to mark elaborated
 *  nets that cannot be merged.
 *
 *  The recordCombFlow() function is to record information for the
 *  mixed mode (combinational into sequential merging). It allows the
 *  caller to register a sequential fanout for a combinational
 *  flow. This can then be used to determine if the block is mergable
 *  with its sequential fanout.
*/
class SCHMergeTest
{
public:
  //! constructor
  SCHMergeTest(SCHMarkDesign* mark, const NUNetElabSet* unmergableNets,
               bool sequential, bool combinational);

  //! destructor
  ~SCHMergeTest();

  //! Record a combinational flow and its sequential fanout
  /*! This is needed if combinational blocks are going to be merged
   *  into a sequential fanout. This records sequential fanout for a
   *  combinational flow.
   *
   *  Use this routine in a design walk from sequential flow
   *  nodes. The return value indicates whether the iteration should
   *  continue or not. The function will return false if the
   *  combinational flow and therefore all its fanin is already
   *  unmergable because it fans out to multiple sequential flows.
   */
  bool recordCombFlow(FLNodeElab* flowElab, SCHScheduleBase* sched,
                      FLNodeElab* seqFlow);

  //! Increment the count for blocks that got broken up because of a cycle
  /*! When merging if, mapping the elaborated graph onto the
   *  unelaborated graph introduces cycles, then those blocks in the
   *  cycle are not mergable. This function allows the caller to
   *  record that fact. This class only keeps track of the merging
   *  statistics and can determine very local mergability only.
   */
  void recordCycleBlock();

  //! Test a block to see whether it is mergable and update the stats
  bool mergable(const NUUseDefNode* useDef, const SCHFlowGroups* flowGroups);

  //! Print statistics for the mergable/unmergable blocks
  void printStats();

private:
  //! Various reasons for blocks being unmergable
  enum MergeTypes {
    // Pure combinational reasons
    eUnMergeCombCycle,         //!< The block is part of a combo cycle
    eUnMergeCombStrength,      //!< The block does not drive strong
    eUnMergeCombEnabledDriver, //!< The block is a bit enabled driver
    eUnMergeCombInitial,       //!< No benefit to merging initial blocks
    eUnMergeCombGrouping,      //!< Instances are not in the same schedules
    eUnMergeCombOutput,        //!< Combinational feeds an output

    // Pure sequential reasons
    eUnMergeSeqInactive,       //!< The seq. node is inactive (no benefit)
    eUnMergeSeqStateUpdate,    //!< The seq. node is a state update (func problem)
    eUnMergeSeqVecEdge,        //!< Vector edge net (not sure why)

    // Mixed combinational into sequential reasons
    eUnMergeCombFlowMulti,     //!< Combinational flow feeds multiple sequentials
    eUnMergeCombBlockMulti,    //!< Combinational block feeds multiple sequentials
    eUnMergeCombProtected,     //!< Combo block is marked protected
    eUnMergeNet,               //!< The block defs an unmergable net
    eUnMergeCombTrans,         //!< Combo block must be run with trans. schedule

    // Applies to all
    eUnMergeFalseCycle,        //!< Block is part of a false cycle
    eUnMergeCModel,            //!< C-model merging has caused problems with EMC
    eUnMergeCntrlFlow,         //!< The control flow net is present (func problem)

    eMergable,                 //!< Block is mergable
    eNumMergeTypes
  };

  //! Convert the MergeTypes enum to a print string
  const char* mergeTypeStr(MergeTypes type);

  //! Keep track of the various block statistics
  int mMergeTypes[eNumMergeTypes];

  //! Abstraction for a vector of sequential schedules and edges
  typedef UtVector<SCHSeqFanout> SeqFanouts;

  //! Abstraction for an iterator over the sequential schedules and edges
  typedef Loop<SeqFanouts> SeqFanoutsLoop;

  //! Abstraction to keep track of what seq. schedules a combo flow feeds
  typedef UtMap<FLNodeElab*, SeqFanouts> FlowSeqFanouts;

  //! Abstraction for an iterator over all the combinational flow' seq fanout
  typedef LoopMap<FlowSeqFanouts> FlowSeqFanoutsLoop;

  //! Storage to keep track of combinational blocks sequential fanout
  FlowSeqFanouts* mFlowSeqFanouts;

  //! Function to determine/record if a sequential block is mergable
  MergeTypes sequentialBlockMergable(const NUUseDefNode* useDef,
                                     const SCHFlowGroups* flowGroups);

  //! Function to determine/record if a combinational block is mergable
  MergeTypes combinationalBlockMergable(const NUUseDefNode* useDef,
                                        const SCHFlowGroups* flowGroups);

  //! Function to determine if a combinational use def is mergable
  MergeTypes isCombUseDefMergable(const NUUseDefNode* useDef);

  //! Function to test/record if a set of combo flows are mergable
  MergeTypes areCombFlowsMergable(const SCHFlowGroups* flowGroups);

  //! Function to make sure combinational blocks fanout to one sequential
  MergeTypes multiOutputTest(const SCHFlowGroups* flowGroups);

  //! Test if the various comb flow groups cover different defs
  bool validCombFlowGroups(const NUUseDefNode* useDef,
                           const SCHFlowGroups* flowGroups);

  //! Utility class
  SCHMarkDesign* mMarkDesign;

  //! Set of elaborated nets that cannot be merged (provided by caller)
  const NUNetElabSet* mUnmergableNets;

  //! If set, the caller wants to merge sequential blocks (provided by caller)
  bool mSequential;

  //! If set, the caller wants to merge combinational blocks (provided by caller)
  bool mCombinational;

  //! The set of blocks that are part of a cycle
  /*! When part of a block is in a cycle it doesn't make sense to
   *  merge any def in the block even if it isn't part of the
   *  cycle. That is because the merge brings in all statements, even
   *  those that are in the cycle.
   *
   *  The set of cycle blocks are gathered at construction
   */
  NUCUseDefSet* mCycleBlocks;

  //! Tests whether every schedule instance is the same
  /*! A block is only mergable if every scheduled instance has the
   *  same elaborated flow.
   */
  bool validFlowGroups(const SCHFlowGroups* flowGroups) const;

  // Types and functions used to figure out if every flow node is
  // represented in each block instance.
  typedef UtMap<const FLNode*, DynBitVector*> GroupMap;
  typedef LoopMap<GroupMap> GroupMapLoop;
  void addGroupFlow(UInt32 group, const FLNode* flow, GroupMap* groupMap);
  void deleteGroupMap(GroupMap* groupMap);

  // The following types and functions are used to store the hidden
  // unelaborated flows for every instance of a block. Hidden flows
  // are unelaborated flows where not every instance is live. They are
  // hidden in the hierarchy where they are not live. These types and
  // code are used to improve when a block is mergable or not.
  //
  // This is used to improve the validFlowGroups() test.
  typedef UtMap<HierName*, FLNodeSet*> HiddenFlows;
  typedef Loop<FLNodeSet> HiddenFlowsLoop;
  HiddenFlows* findHiddenFlows(const SCHFlowGroups* flowGroups) const;
  HiddenFlowsLoop loopHiddenFlows(HierName* hierName,
                                  const HiddenFlows& hiddenFlows) const;
  void deleteHiddenFlows(HiddenFlows* hiddenFlows) const;

  //! Function to test if the flow drives a partial bid
  bool isPartialPrimaryBid(FLNodeElab* flow) const;

}; // class SCHMergeTest


#endif // _MERGETEST_H_
