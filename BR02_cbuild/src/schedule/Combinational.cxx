// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "schedule/ScheduleMask.h"
#include "schedule/Schedule.h"

#include "nucleus/NUUseDefNode.h"

#include "flow/FLNodeElab.h"

#include "MarkDesign.h"
#include "Util.h"

/*!
  \file
  Implementation of SCHCombinational class
*/

SCHCombinational::SCHCombinational(SCHScheduleType type,
				   const SCHScheduleMask* mask,
				   SCHDerivedClockLogic* parent,
				   int inputAsyncIndex) :
  SCHScheduleBase(type), mMask(mask), mAsyncInputNets(0),
  mInputAsyncIndex(inputAsyncIndex), mIndex(0), mParent(parent)
{
  mBlockSchedule = new BlockSchedule;
  mSchedule = new FLNodeElabVector;
}

SCHCombinational::~SCHCombinational()
{
  clearBlockSchedule();
  delete mBlockSchedule;
  delete mSchedule;
}

typedef RLoop<FLNodeElabVector> NodeRLoop;

SCHIterator SCHCombinational::getSchedule() const
{
  return SCHIterator::create(NodeRLoop(mSchedule->rbegin(), mSchedule->rend()));
}

int SCHCombinational::compareSchedules(const SCHScheduleBase* schedBase) const
{
  // The other schedule must be combinational
  const SCHCombinational* other =
    dynamic_cast<const SCHCombinational*>(schedBase);
  SCHED_ASSERT(other != NULL, schedBase);

  // Check for easy case
  if (this == other)
    return 0;

  // First compare by the combinational type
  int cmp = getScheduleType() - other->getScheduleType();

  // Next Compare by schedule mask or input nets
  if (cmp == 0)
  {
    if ((getScheduleType() == eCombInput) || (getScheduleType() == eCombAsync))
      cmp = getInputAsyncIndex() - other->getInputAsyncIndex();
    else
      cmp = SCHScheduleMask::compare(mMask, other->getMask());

    // If they are still the same, they must be in different derived
    // schedules
    if (cmp == 0)
    {
      SCHED_ASSERT(getScheduleType() == eCombDCL, this);
      SCHED_ASSERT(other->getScheduleType() == eCombDCL, other);
      SCHDerivedClockLogic* dcl1 = getParentSchedule();
      SCHDerivedClockLogic* dcl2 = other->getParentSchedule();
      cmp = dcl1->compareSchedules(dcl2);
    }
  }
  SCHED_ASSERT2(cmp != 0, this, schedBase);
  return cmp;
}

SCHInputNetsCLoop SCHCombinational::loopInputNets() const
{
  if (mAsyncInputNets == NULL)
    return SCHInputNetsCLoop();
  else
    return SCHInputNetsCLoop(*mAsyncInputNets);
}

bool SCHCombinational::empty() const { return mSchedule->empty(); }

UInt32 SCHCombinational::size() const { return mSchedule->size(); }

void SCHCombinational::addCombinationalNode(FLNodeElab* node)
{
  mSchedule->push_back(node);
}

void SCHCombinational::removeFlows(const SCHDeleteTest& deleteTest,
                                   FLNodeElabVector* flows)
{
  FLNodeElabVector replacement;
  for (FLNodeElabVectorLoop l(*flows); !l.atEnd(); ++l) {
    FLNodeElab* flowElab = *l;
    if (!deleteTest(flowElab)) {
      replacement.push_back(flowElab);
    }
  }
  flows->swap(replacement);
}

void SCHCombinational::removeFlows(const SCHDeleteTest& deleteTest)
{
  // Replace the elaborated flow in the flow schedules
  removeFlows(deleteTest, mSchedule);

  // Replace the elaborated flow in the block schedules
  for (BlockScheduleLoop l = getBlockSchedule(); !l.atEnd(); ++l) {
    FLNodeElabVector* flows = *l;
    removeFlows(deleteTest, flows);
  }
}

void SCHCombinational::addCombinationalBlock(FLNodeElabVector& nodes)
{
  FLNodeElabVector* newNodes = new FLNodeElabVector;
  *newNodes = nodes;
  mBlockSchedule->push_back(newNodes);
}

SCHCombinational::BlockScheduleLoop SCHCombinational::getBlockSchedule()
{
  return BlockScheduleLoop(*mBlockSchedule);
}

void SCHCombinational::clearBlockSchedule()
{
  for (BlockScheduleLoop l = getBlockSchedule(); !l.atEnd(); ++l)
    delete *l;
  mBlockSchedule->clear();
}

void SCHCombinational::clearSchedule() { mSchedule->clear(); }

void SCHCombinational::print(bool printNets, SCHMarkDesign* mark) const
{
  const char* str;
  if (getScheduleType() == eCombSample)
    str = "sample";
  else
    str = "transition";
  UtIO::cout() << "Combinational (" << this << ") " << str << ", mask = ";
  if (mMask != NULL)
    mMask->print();
  else
    UtIO::cout() << "<none>\n";
  if (printNets) {
    for (FLNodeElabVectorLoop l(*mSchedule); !l.atEnd(); ++l) {
      FLNodeElab* flow = *l;
      flow->pname(2, true);
      if (mark != NULL) {
        SCHMarkDesign::FlowDependencyLoop f;
        for (f = mark->loopDependencies(flow); !f.atEnd(); ++f) {
          FLNodeElab* fanin = *f;
          fanin->pname(4, true);
        }
      }
    }
  }
}

void SCHCombinational::dump(int indent, SCHMarkDesign* mark) const
{
  // Figure out the combinational type
  const char* combType = NULL;
  switch(getScheduleType()) {
    case eCombInput:
      combType = "Input";
      break;
    case eCombAsync:
      combType = "Asynchronous";
      break;
    case eCombDCL:
      combType = "DCL";
      break;
    case eCombSample:
      combType = "Sample";
      break;
    case eCombTransition:
      combType = "Transition";
      break;
    case eCombInitial:
      combType = "Initial";
      break;
    case eCombInitSettle:
      combType = "InitSettle";
      break;
    case eCombDebug:
      combType = "Debug";
      break;
    case eCombForceDeposit:
      combType = "ForceDeposit";
      break;
    default:
      SCHED_ASSERT("Unknown combinational schedule type" == NULL, this);
      break;
  } // switch

  // Dump the header
  dumpIndent(indent);
  UtIO::cout() << "Combinational[" << combType << "]";
  
  // Dump either the schedule mask, input nets or nothing
  const SCHScheduleMask* mask = getMask();
  if ((mask != NULL) && !mark->getUtil()->isInputMask(mask)) {
    UtString buf;
    mask->compose(&buf,NULL);
    UtIO::cout() << " @(" << buf << ")\n";
  } else if (getAsyncInputNets() != NULL) {
    const char* sep = "( ";
    for (SCHInputNetsCLoop n = loopInputNets(); !n.atEnd(); ++n) {
      const NUNetElab* netElab = *n;
      UtString buf;
      netElab->compose(&buf, NULL);
      UtIO::cout() << sep << buf;
      sep = " or ";
    }
    UtIO::cout() << " ):" << UtIO::endl;
  } else {
    UtIO::cout() << UtIO::endl;
  }

  // Print any branch nets
  UtString branchNetsBuf;
  composeBranchNets(&branchNetsBuf, mark);
  if (!branchNetsBuf.empty()) {
    UtIO::cout() << "  " << branchNetsBuf << "\n";
  }

  // Dump the flow nodes in this schedule
  int index = 0;
  for (SCHIterator i = getSchedule(); !i.atEnd(); ++i) {
    FLNodeElab* flow = *i;
    dumpFlowNode(flow, getScheduleType(), ++index, indent, mark);
  }
}
