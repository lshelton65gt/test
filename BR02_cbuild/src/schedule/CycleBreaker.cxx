// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*! \file
    This is the implementation of the SCHCycleBreaker class.
    The role of the class is to use additional information not captured by
    the elaborated flow graph to determine a set of non-cyclic schedules
    for a cycle found in the flow graph.  The NUCycle structure for the
    cycle remains, because later passes still want to treat the flow as
    cyclic.  Cycle-breaking affects the generated code for the schedule
    and reduces the number of warning messages we show to the user.
 */

#include "schedule/Schedule.h"
#include "util/CbuildMsgContext.h"
#include "util/UtOrder.h"
#include "util/UtMap.h"
#include "util/GraphSCC.h"
#include "util/UnionFind.h"
#include "util/GraphBuilder.h"
#include "bdd/BDD.h"
#include "nucleus/NUCycle.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUAssign.h"
#include "flow/FLNodeElab.h"
#include "flow/FlowClassifier.h"
#include "CycleBreaker.h"
#include "CycleGraph.h"
#include "Util.h"
#include "MarkDesign.h"

// define this as true to dump intermediate cycle graphs during breaking 
#define DEBUG_CYCLE_BREAKING false

/*! An SCHConditionMap object maintains a map from elaborated flow nodes to
 *  BDDs that represent conditions under which those flows are used.  It is
 *  used when computing fanin conditions for flow-control statements such as
 *  If, Case, and EnabledDrivers.
 */
class SCHConditionMap
{
private:
  typedef UtMap<FLNodeElab*,BDD> ConditionMap;
public:
  typedef ConditionMap::iterator iterator;
  SCHConditionMap(BDDContext* context);
  ~SCHConditionMap();
  BDD lookup(FLNodeElab* flow) const;
  void add(FLNodeElab* flow, const NUExpr* expr, STBranchNode* scope);
  void add(FLNodeElab* flow, BDD cond);
  void merge(SCHConditionMap* other);
  void require(const NUExpr* expr, STBranchNode* scope);
  void require(BDD cond);
  iterator begin() { return mConditions.begin(); }
  iterator end() { return mConditions.end(); }
private:
  BDDContext* mBDDContext;    //!< The BDDContext that manages the BDDs for the NUExprs
  ConditionMap mConditions;   //!< A map from flow nodes to their enabling BDD
}; // class SCHConditionMap

//! Constructor, creates an empty condition map
SCHConditionMap::SCHConditionMap(BDDContext* context)
  : mBDDContext(context), mConditions()
{
}

//! Destructor, releases condition map resources
SCHConditionMap::~SCHConditionMap()
{
}

//! Get the BDD for a flow node, or True (val1) if it is not found
BDD SCHConditionMap::lookup(FLNodeElab* flow) const
{
  ConditionMap::const_iterator p = mConditions.find(flow);
  if (p == mConditions.end())
    return mBDDContext->val1();
  else
    return p->second;
}

//! Add a new condition under which the flow is used.
/*! The new condition will be ORed with any existing conditions for this flow.
 */
void SCHConditionMap::add(FLNodeElab* flow, const NUExpr* expr, STBranchNode* scope)
{
  add(flow, expr->bdd(mBDDContext, scope));
}

//! Add a new condition under which the flow is used.
/*! The new condition will be ORed with any existing conditions for this flow.
 */
void SCHConditionMap::add(FLNodeElab* flow, BDD cond)
{
  // look up the existing conditions for this flow
  ConditionMap::iterator p = mConditions.find(flow);

  // if there are none, initialize it with false
  if (p == mConditions.end())
  {
    mConditions[flow] = mBDDContext->val0();
    p = mConditions.find(flow);
    FLN_ELAB_ASSERT(p != mConditions.end(),flow);
  }  
  INFO_ASSERT(p->second.isValid(), "Invalid BDD in map");

  if (cond.isValid())
    cond = mBDDContext->opOr(p->second, cond);

  // If the result is valid, use it.  It not, assume the worst and use True.
  if (cond.isValid())
    p->second = cond;
  else
    p->second = mBDDContext->val1();
}

//! Merge the conditions from another map into this one.
/*! The conditions from the other map will be ORed with any matching
 *  conditions in this map.  If no condition exists for a flow in this
 *  map, False is assumed.
 */
void SCHConditionMap::merge(SCHConditionMap* other)
{
  // Iterator over the entries in the other map
  for (ConditionMap::iterator p = other->begin(); p != other->end(); ++p)
  {
    FLNodeElab* flow = p->first;
    INFO_ASSERT(p->second.isValid(), "Invalid BDD in map");
    // Look up the corresponding entry in this map, if there is one
    ConditionMap::iterator n = mConditions.find(flow);
    if (n == mConditions.end())
      mConditions[flow] = p->second; // no entry, just copy the other's condition
    else // there is an entry, so OR the two conditions together
    {
      INFO_ASSERT(n->second.isValid(), "Invalid BDD in map");
      BDD cond = mBDDContext->opOr(n->second, p->second);
      if (cond.isValid())
        n->second = cond;
      else
        n->second = mBDDContext->val1();
    }    
  }
}

//! Require the condition to hold for all flows in the map.
/*! The required condition will be ANDed with all conditions already in the map.
 */
void SCHConditionMap::require(const NUExpr* expr, STBranchNode* scope)
{
  // Build the condition for the required expression
  BDD cond = expr->bdd(mBDDContext, scope);
  require(cond); // use the BDD-based require method
}

//! Require the condition to hold for all flows in the map.
/*! The required condition will be ANDed with all conditions already in the map.
 */
void SCHConditionMap::require(BDD cond)
{
  // Loop over all entries in the map, ANDing in the required condition
  for (ConditionMap::iterator p = mConditions.begin(); p != mConditions.end(); ++p)
  {
    BDD anded = mBDDContext->opAnd(p->second, cond);
    if (anded.isValid())
      p->second = anded;
    else
      p->second = mBDDContext->val0();
  }
}

/*! Internally, this class manages the ordering requirements on nodes
    in cycles related to strength and latch glitches.  We schedule
    nodes in post DFS order, with starting points ordered so that
    weaker drivers precede stronger drivers.  While walking, we stop at
    edges that are multi-driver dependency edges, edges to a driver
    whose strength is greater than our own, and edges to latches.

    All of the nodes are starting points for traversal, but they are
    ordered so that one which need to be generated first are walked
    first.  When we risk walking into a node that needs to be
    generated after a node we are currently computing, we "cut" the
    dataflow path and use the stale value.  The cut node will be
    visited later because it is still on the list of starting points
    for traversal.
 */
class DependencyGraphScheduler : public GraphSortedWalker
{
public:
  DependencyGraphScheduler(NUCycle* cycle)
    : GraphSortedWalker(), mCycle(cycle) {};
private:
  //! schedule a flow node AFTER all dependencies are scheduled
  GraphWalker::Command visitNodeAfter(Graph* g, GraphNode* node)
  {
    DependencyGraph* graph = dynamic_cast<DependencyGraph*>(g);
    DependencyGraph::Node* gdn = graph->castNode(node);
    FLNodeElab* flow = gdn->getData();
    mCycle->scheduleFlow(flow);
    return GW_CONTINUE;
  }

  /*! Says whether to ignore, use the existing value (ie., cut the data path),
   *  or compute an up-to-date value
   */
  typedef enum { SKIP, CUT, OK } EdgeCmd;

  /*! Ignore edges due to multi-driver dependencies.
   *  They are needed to determine what to put in the cycle, but
   *  should be ignored when choosing ordering because they may conflict
   *  with data and strength edges.  If we reach a cut point, skip the edge
   *  and record that we will be using a stale value.
   */
  GraphWalker::Command visitTreeEdge(Graph* g, GraphNode* node, GraphEdge* edge)
  {
    EdgeCmd cmd = edgeCommand(g,node,edge);
    if (cmd == SKIP)
        return GW_SKIP;
    else if (cmd == CUT)
    {
      // record this as a cut point in the graph
      DependencyGraph* graph = dynamic_cast<DependencyGraph*>(g);
      DependencyGraph::Node* gdn = graph->castNode(graph->endPointOf(edge));
      FLNodeElab* flow = gdn->getData();
      mCycle->addCutPoint(flow);
      return GW_SKIP;
    }
    else
      return GW_CONTINUE;
  }
  /*! Ignore multi-driver dependency edges and edges considered cut point.
   *  We don't need to record the cut point because the value is not stale
   *  (since this is a cross edge).
   */
  GraphWalker::Command visitCrossEdge(Graph* g, GraphNode* node, GraphEdge* edge)
  {
    EdgeCmd cmd = edgeCommand(g,node,edge);
    if (cmd == SKIP || cmd == CUT)
      return GW_SKIP;
    else
      return GW_CONTINUE;
  }
  /*! Ignore multi-driver dependency edges.  Any other edge is a cut point
   *  that must be recorded, since this is a back edge.
   */
  GraphWalker::Command visitBackEdge(Graph* g, GraphNode* node, GraphEdge* edge)
  {
    EdgeCmd cmd = edgeCommand(g,node,edge);
    if (cmd == SKIP)
        return GW_SKIP;
    else
    {
      // record this as a cut point in the graph
      DependencyGraph* graph = dynamic_cast<DependencyGraph*>(g);
      DependencyGraph::Node* gdn = graph->castNode(graph->endPointOf(edge));
      FLNodeElab* flow = gdn->getData();
      mCycle->addCutPoint(flow);
      return GW_CONTINUE;
    }
  }

private:
  EdgeCmd edgeCommand(Graph* g, GraphNode* node, GraphEdge* edge)
  {
    // Ignore brother dependencies entirely
    if (g->anyFlagSet(edge, eBrotherDependency))
      return SKIP;

    // If this is an edge from weak to strong, an edge to a latch, or
    // an edge from a changebool (from a change detect net), then cut
    // here.
    DependencyGraph* graph = dynamic_cast<DependencyGraph*>(g);
    DependencyGraph::Node* fromNode = graph->castNode(node);
    DependencyGraph::Node* toNode = graph->castNode(graph->endPointOf(edge));
    FLNodeElab* from = fromNode->getData();
    FLNodeElab* to = toNode->getData();

    // Test for the strength ordering
    NUUseDefNode* fromUD = from->getUseDefNode();
    NUUseDefNode* toUD = to->getUseDefNode();
    if (fromUD->getStrength() < toUD->getStrength())
      return CUT;

    // Test for the latch cut point
    NUNet* toNet = to->getFLNode()->getDefNet();
    if (toNet->isLatch())
      return CUT;

    // Test for the change bool cut point
    NUNet* fromNet = from->getFLNode()->getDefNet();
    if (fromNet->isClearAtEnd()) {
      return CUT;
    }

    return OK;
  }

private:
  NUCycle* mCycle; //!< the cycle we are scheduling flow nodes in
};


typedef UtSet<FLNodeElab*, DerefOrder<FLNodeElab> > SortedFlowElabSet;
typedef GenericDigraph<void*,SortedFlowElabSet*> ScheduleGraph;

//! An ordered depth-first graph walker to schedule flow nodes in cycles
/*! Internally, this class manages the ordering requirements on nodes
    in cycles related to strength and latch glitches.  We schedule
    nodes in post DFS order, with starting points ordered so that
    weaker drivers precede stronger drivers.  While walking, we stop at
    edges that are multi-driver dependency edges, edges to a driver
    whose strength is greater than our own, and edges to latches.

    All of the nodes are starting points for traversal, but they are
    ordered so that one which need to be generated first are walked
    first.  When we risk walking into a node that needs to be
    generated after a node we are currently computing, we "cut" the
    dataflow path and use the stale value.  The cut node will be
    visited later because it is still on the list of starting points
    for traversal.
 */
class FlowSetGraphScheduler : public GraphSortedWalker
{
public:
  FlowSetGraphScheduler(NUCycle* cycle, DependencyGraph* cycleGraph)
    : GraphSortedWalker(), mCycle(cycle), mCycleGraph(cycleGraph) {};

  //! used to sort graph nodes (with FLNodeElabSet data) in a stable order
  class NodeCmp : public GraphSortedWalker::NodeCmp
  {
  public:
    NodeCmp(Graph* g) { mGraph = dynamic_cast<ScheduleGraph*>(g); }
    virtual bool operator()(const GraphNode* n1, const GraphNode* n2) const
    {
      ScheduleGraph::Node* gdn = mGraph->castNode(const_cast<GraphNode*>(n1));
      SortedFlowElabSet* flowSet1 = gdn->getData();
      gdn = mGraph->castNode(const_cast<GraphNode*>(n2));
      SortedFlowElabSet* flowSet2 = gdn->getData();
      
      SortedFlowElabSet::iterator iter1 = flowSet1->begin();
      SortedFlowElabSet::iterator iter2 = flowSet2->begin();
      while (iter1 != flowSet1->end() && iter2 != flowSet2->end())
      {
        FLNodeElab* flow1 = *iter1;
        FLNodeElab* flow2 = *iter2;
        NUUseDefNode* useDef1 = flow1->getUseDefNode();
        NUUseDefNode* useDef2 = flow2->getUseDefNode();
        if (useDef1 == NULL)
        {
          if (useDef2 != NULL)
            return true;
        }
        else if (useDef2 == NULL)
        {
          return false;
        }
        else if (useDef1->getStrength() != useDef2->getStrength())
        {
          return (useDef1->getStrength() < useDef2->getStrength());
        }
        /* we get here if both UseDef nodes are equal strength (or NULL) */
        int cmp = FLNodeElab::compare(flow1,flow2);
        if (cmp < 0)
          return true;
        else if (cmp > 0)
          return false;

        ++iter1; ++iter2;
      }
    
      /* all flow nodes compared equal -- check for set size */
      return (iter2 != flowSet2->end());
    }
  private:
    ScheduleGraph* mGraph;
  };

  //! used to sort graph edges in order of the flow nodes they point to
  class EdgeCmp : public GraphSortedWalker::EdgeCmp
  {
  public:
    EdgeCmp(Graph* g) { mGraph = dynamic_cast<ScheduleGraph*>(g); }
    virtual bool operator()(const GraphEdge* e1, const GraphEdge* e2) const
    {
      GraphNode* node1 = mGraph->endPointOf(e1);
      GraphNode* node2 = mGraph->endPointOf(e2);
      ScheduleGraph::Node* gdn = mGraph->castNode(node1);
      SortedFlowElabSet* flowSet1 = gdn->getData();
      gdn = mGraph->castNode(node2);
      SortedFlowElabSet* flowSet2 = gdn->getData();

      SortedFlowElabSet::iterator iter1 = flowSet1->begin();
      SortedFlowElabSet::iterator iter2 = flowSet2->begin();
      while (iter1 != flowSet1->end() && iter2 != flowSet2->end())
      {
        FLNodeElab* flow1 = *iter1;
        FLNodeElab* flow2 = *iter2;
        int cmp = FLNodeElab::compare(flow1,flow2);
        if (cmp < 0)
          return true;
        else if (cmp > 0)
          return false;
        ++iter1; ++iter2;
      }
    
      /* all flow nodes compared equal -- check for set size */
      return (iter2 != flowSet2->end());
    }
  private:
    ScheduleGraph* mGraph;
  };

private:
  //! Helper function to schedule the nodes within a flow set
  void scheduleFlowSet(SortedFlowElabSet* flowSet)
  {
    /* If you are keeping track, this is the *third* level of scheduling within
     * the cycle.  First, we order the connected components, then the flow sets,
     * then the flows within each set.  There has to be a better way...
     */
    INFO_ASSERT(!flowSet->empty(), "Cycle node has empty flow set!");

    // If there is only one flow, we can schedule it directly.
    if (flowSet->size() == 1)
    {
      mCycle->scheduleFlow(*flowSet->begin());
      return;
    }

    // For the multiple flows case, we have to create an ordering graph
    // for the flows and schedule them in a dependency-directed manner.
    GraphNodeSet keepNodes;
    for (Iter<GraphNode*> loop = mCycleGraph->nodes(); !loop.atEnd(); ++loop)
    {
      GraphNode* node = *loop;
      DependencyGraph::Node* dgn = mCycleGraph->castNode(node);
      FLNodeElab* flow = dgn->getData();
      if (flowSet->find(flow) != flowSet->end())
        keepNodes.insert(node);
    }
    INFO_ASSERT(!keepNodes.empty(), "Flow group did not match any cycle flows!");    
    Graph* g = mCycleGraph->subgraph(keepNodes);
    keepNodes.clear();
    DependencyGraphScheduler sched(mCycle);
    NodeOrder nodeCmp(g);
    EdgeOrder edgeCmp(g);
    sched.walk(g, &nodeCmp, &edgeCmp);    
    delete g;
  }

  //! schedule a flow node AFTER all dependencies are scheduled
  GraphWalker::Command visitNodeAfter(Graph* g, GraphNode* node)
  {
    ScheduleGraph* graph = dynamic_cast<ScheduleGraph*>(g);
    ScheduleGraph::Node* gdn = graph->castNode(node);
    scheduleFlowSet(gdn->getData());
    return GW_CONTINUE;
  }

  /*! Says whether to ignore, use the existing value (ie., cut the data path),
   *  or compute an up-to-date value
   */
  typedef enum { SKIP, CUT, OK } EdgeCmd;

  /*! Ignore edges due to multi-driver dependencies.
   *  They are needed to determine what to put in the cycle, but
   *  should be ignored when choosing ordering because they may conflict
   *  with data and strength edges.  If we reach a cut point, skip the edge
   *  and record that we will be using a stale value.
   */
  GraphWalker::Command visitTreeEdge(Graph* g, GraphNode* node, GraphEdge* edge)
  {
    EdgeCmd cmd = edgeCommand(g,node,edge);
    if (cmd == SKIP)
        return GW_SKIP;
    else if (cmd == CUT)
    {
      // record this as a cut point in the graph
      ScheduleGraph* graph = dynamic_cast<ScheduleGraph*>(g);
      ScheduleGraph::Node* gdn = graph->castNode(graph->endPointOf(edge));
      SortedFlowElabSet* flowSet = gdn->getData();
      for (SortedFlowElabSet::iterator f = flowSet->begin(); f != flowSet->end(); ++f)
      {
        FLNodeElab* flow = *f;
        mCycle->addCutPoint(flow);
      }
      return GW_SKIP;
    }
    else
      return GW_CONTINUE;
  }
  /*! Ignore multi-driver dependency edges and edges considered cut point.
   *  We don't need to record the cut point because the value is not stale
   *  (since this is a cross edge).
   */
  GraphWalker::Command visitCrossEdge(Graph* g, GraphNode* node, GraphEdge* edge)
  {
    EdgeCmd cmd = edgeCommand(g,node,edge);
    if (cmd == SKIP || cmd == CUT)
      return GW_SKIP;
    else
      return GW_CONTINUE;
  }
  /*! Ignore multi-driver dependency edges.  Any other edge is a cut point
   *  that must be recorded, since this is a back edge.
   */
  GraphWalker::Command visitBackEdge(Graph* g, GraphNode* node, GraphEdge* edge)
  {
    EdgeCmd cmd = edgeCommand(g,node,edge);
    if (cmd == SKIP)
        return GW_SKIP;
    else
    {
      // record this as a cut point in the graph
      ScheduleGraph* graph = dynamic_cast<ScheduleGraph*>(g);
      ScheduleGraph::Node* gdn = graph->castNode(graph->endPointOf(edge));
      SortedFlowElabSet* flowSet = gdn->getData();
      for (SortedFlowElabSet::iterator f = flowSet->begin(); f != flowSet->end(); ++f)
      {
        FLNodeElab* flow = *f;
        mCycle->addCutPoint(flow);
      }
      return GW_CONTINUE;
    }
  }

private:
  EdgeCmd edgeCommand(Graph* g, GraphNode* node, GraphEdge* edge)
  {
    bool hadEdge = false;

    // If this is an edge from weak to strong or an edge to a latch
    // then cut here.
    ScheduleGraph* graph = dynamic_cast<ScheduleGraph*>(g);
    ScheduleGraph::Node* fromNode = graph->castNode(node);
    ScheduleGraph::Node* toNode = graph->castNode(graph->endPointOf(edge));
    SortedFlowElabSet* fromSet = fromNode->getData();
    SortedFlowElabSet* toSet = toNode->getData();
    for (SortedFlowElabSet::iterator f = fromSet->begin(); f != fromSet->end(); ++f)
    {
      FLNodeElab* from = *f;
      for (SortedFlowElabSet::iterator g = toSet->begin(); g != toSet->end(); ++g)
      {
        // This edge is ok as long as it isn't a strength ordering and
        // it isn't an edge to a latch or from a changebool
        // (clearAtEnd net).
        FLNodeElab* to = *g;
        NUUseDefNode* fromUD = from->getUseDefNode();
        NUUseDefNode* toUD = to->getUseDefNode();
        NUNet* toNet = to->getFLNode()->getDefNet();
        NUNet* fromNet = from->getFLNode()->getDefNet();
        if ((fromUD->getStrength() >= toUD->getStrength()) &&
            !toNet->isLatch() && !fromNet->isClearAtEnd()) {
          return OK;
        }
        hadEdge = true;
      }
    }

    return hadEdge ? CUT : OK;
  }

private:
  NUCycle*         mCycle;      //!< the cycle we are scheduling flow nodes in
  DependencyGraph* mCycleGraph; //!< the dependency graph for the full cycle
};

//! An ordered depth-first graph walker to schedule a component graph
class ComponentGraphScheduler : public GraphWalker
{
public:
  ComponentGraphScheduler(GraphSCC* scc, NUCycle* cycle, SCHCycleBreaker* breaker)
    : GraphWalker(), mSCC(scc), mCycle(cycle), mBreaker(breaker) {};

  //! used to sort component graph nodes in a stable order
  struct NodeOrder
  {
    NodeOrder(GraphSCC* scc) { mSCC = scc; }
    bool operator()(const GraphNode* n1, const GraphNode* n2) const
    {
      if (n1 == n2)
        return false;
      GraphSCC::Component* c1 = mSCC->getComponent(n1);
      GraphSCC::Component* c2 = mSCC->getComponent(n2);
      SInt32 cmp = c1->size() - c2->size();
      if (cmp == 0)
      {
        if (c1->isCyclic() && !c2->isCyclic())
          cmp = -1;
        else if (!c1->isCyclic() && c2->isCyclic())
          cmp = 1;

        // compare based on the first flow node in the component 
        if (cmp == 0)
        {
          GraphSCC::Component::NodeLoop l = c1->loopNodes();
          DependencyGraph::Node* dgn1 = dynamic_cast<DependencyGraph::Node*>(*l);
          l = c2->loopNodes();
          DependencyGraph::Node* dgn2 = dynamic_cast<DependencyGraph::Node*>(*l);
          cmp = FLNodeElab::compare(dgn1->getData(), dgn2->getData());

          // last-resort is pointer comparison
          if (cmp == 0)
            cmp = carbonPtrCompare(c1,c2);
        }
      }
      INFO_ASSERT(cmp != 0, "NodeOrder failed to distinguish two nodes");
      return (cmp < 0);
    }
    GraphSCC* mSCC;
  };

  //! used to sort component graph edges in a stable order
  struct EdgeOrder
  {
    EdgeOrder(GraphSCC* scc, Graph* g) { mSCC = scc; mGraph = g; }
    bool operator()(const GraphEdge* e1, const GraphEdge* e2) const
    {
      GraphNode* node1 = mGraph->endPointOf(e1);
      GraphNode* node2 = mGraph->endPointOf(e2);
      if (node1 == node2)
        return false;
      GraphSCC::Component* c1 = mSCC->getComponent(node1);
      GraphSCC::Component* c2 = mSCC->getComponent(node2);
      SInt32 cmp = c1->size() - c2->size();
      if (cmp == 0)
      {
        if (c1->isCyclic() && !c2->isCyclic())
          cmp = -1;
        else if (!c1->isCyclic() && c2->isCyclic())
          cmp = 1;

        // compare based on the first flow node in the component 
        if (cmp == 0)
        {
          GraphSCC::Component::NodeLoop l = c1->loopNodes();
          DependencyGraph::Node* dgn1 = dynamic_cast<DependencyGraph::Node*>(*l);
          l = c2->loopNodes();
          DependencyGraph::Node* dgn2 = dynamic_cast<DependencyGraph::Node*>(*l);
          cmp = FLNodeElab::compare(dgn1->getData(), dgn2->getData());

          // last-resort is pointer comparison
          if (cmp == 0)
            cmp = carbonPtrCompare(c1,c2);
        }
      }
      INFO_ASSERT(cmp != 0, "EdgeOrder failed to distinguish two edges");
      return (cmp < 0);
    }
    GraphSCC* mSCC;
    Graph* mGraph;
  };

  //! overloading this function lets us determine the node-traversal order
  void nodes(Nodes* nodeVector, Graph* g)
  {
    // produce the nodes in sorted order so that the scheduler output is stable
    for (Iter<GraphNode*> iter = g->nodes(); !iter.atEnd(); ++iter)
      nodeVector->push_back(*iter);
    std::sort(nodeVector->begin(), nodeVector->end(), NodeOrder(mSCC));
  }

  //! overloading this function lets us determine the edge-traversal order
  void edges(Edges* edgeVector, Graph* g, GraphNode* node)
  {
    // produce the edges in sorted order so that the scheduler output is stable
    for (Iter<GraphEdge*> iter = g->edges(node); !iter.atEnd(); ++iter)
      edgeVector->push_back(*iter);
    std::sort(edgeVector->begin(), edgeVector->end(), EdgeOrder(mSCC,g));
  }

  //! schedule a component AFTER all dependencies are scheduled
  GraphWalker::Command visitNodeAfter(Graph*, GraphNode* node)
  {
    GraphSCC::Component* component = mSCC->getComponent(node);
    if (component->isCyclic())
    {
      DependencyGraph* subgraph = dynamic_cast<DependencyGraph*>(mSCC->extractComponent(component));
      mBreaker->schedule(subgraph);
      delete subgraph;
    }
    else
    {
      // NOTE: a non-cyclic component will only have 1 node
      GraphSCC::Component::NodeLoop p = component->loopNodes();
      GraphNode* n = *p;
      DependencyGraph::Node* dgn = dynamic_cast<DependencyGraph::Node*>(n);
      FLNodeElab* flow = dgn->getData();
      mCycle->scheduleFlow(flow);
      ++p;
      INFO_ASSERT(p.atEnd(), "Non-cyclic component contains mutliple nodes");
    }
    return GW_CONTINUE;
  }
private:
  GraphSCC* mSCC;  //!< The strongly-connected-component object associated with this component graph
  NUCycle* mCycle; //!< The cycle we are scheduling flow nodes in
  SCHCycleBreaker* mBreaker; //! The cycle breaker to use to schedule sub-cycles
};



//! The main cycle creaking class
/* The SCHCycleBreaker class is the top-level cycle breaking class.
   Given an NUCycle and the elaborated flow for the cycle, it augments
   the flow dependencies with BDDs representing the conditions under which
   the dependency applies.  It then tries to use these conditions to break
   the cycle and find a non-looping schedule for the cycle.
 */
SCHCycleBreaker::SCHCycleBreaker(NUCycle* cycle, DependencyGraph* graph,
                                 SCHUtil* util, SCHMarkDesign* mark, UInt32 branchDepthLimit)
  : mCycle(cycle), mGraph(graph), mUtil(util), mMarkDesign(mark), mBDDContext(),
    mBranchDepth(0), mBranchDepthLimit(branchDepthLimit), mReductionCount(0)
{
  mWriteDotFiles = DEBUG_CYCLE_BREAKING;
}

void SCHCycleBreaker::execute(bool writeDotFile)
{
  bool savedWriteState = mWriteDotFiles;
  mWriteDotFiles |= writeDotFile;

  if (mUtil->isFlagSet(eBreakCycles))
  {
    // first, compute BDD's to represent conditions on each edge
    FlowToDGNodeMap nodeMap;
    for (Iter<GraphNode*> loop = mGraph->nodes(); !loop.atEnd(); ++loop)
    {
      GraphNode* node = *loop;
      DependencyGraph::Node* dgn = mGraph->castNode(node);
      FLNodeElab* flow = dgn->getData();
      nodeMap[flow] = dgn;
      
      // compute the fanin->BDD map
      SCHConditionMap* condMap = gatherConditions(flow);
      
      // populate the edge data with BDDs for each edge
      for (Iter<GraphEdge*> iter = mGraph->edges(node); !iter.atEnd(); ++iter)
      {
        GraphEdge* edge = *iter;
        GraphNode* to = mGraph->endPointOf(edge);
        DependencyGraph::Edge* dge = mGraph->castEdge(edge);
        DependencyGraph::Node* dgto = mGraph->castNode(to);
        FLNodeElab* toFlow = dgto->getData();
        if (mGraph->anyFlagSet(dge,eFlowDependency))
          dge->setData(condMap->lookup(toFlow));
        else if (mGraph->anyFlagSet(dge,eStrengthDependency))
        {
          // These edges are always needed -- even when the pull value is not used
          // the edge is required to ensure the pull driver does not run after the
          // stronger drivers.
            dge->setData(mBDDContext.val1());
        }
        else if (mGraph->anyFlagSet(dge,eBrotherDependency))
        {
          dge->setData(mBDDContext.val0()); // this will get set later
        }
        else
        {
          INFO_ASSERT(false,"Found an edge without a known flag");
        }
      }

      // release the map data
      delete condMap;
    }

    // compute the edge conditions for brother driver dependencies
    for (Iter<GraphNode*> loop = mGraph->nodes(); !loop.atEnd(); ++loop)
    {
      GraphNode* node = *loop;
      DependencyGraph::Node* dgFrom = mGraph->castNode(node);
      FLNodeElab* fromFlow = dgFrom->getData();
      for (Iter<GraphEdge*> iter = mGraph->edges(node); !iter.atEnd(); ++iter)
      {
        GraphEdge* edge = *iter;
        GraphNode* to = mGraph->endPointOf(edge);
        DependencyGraph::Edge* dge = mGraph->castEdge(edge);
        if (mGraph->anyFlagSet(dge,eBrotherDependency))
        {
          BDD brotherCond = brotherConditions(fromFlow, to, &nodeMap);
          dge->setData(mBDDContext.opOr(dge->getData(), brotherCond));
        }
      }
    }
    
    // write the cycle graph including BDDs on edges, if the caller requested it
    if (mWriteDotFiles)
    {
      UtString filename;
      filename << "annotated_cycle_" << mCycle->getID() << ".dot";
      CycleGraphDotWriter cgdw(filename, &mBDDContext);
      cgdw.output(mGraph);
    }
  }

  // If dumping is turned on, print the BDDs in graph
  if (mUtil->isFlagSet(eDumpCycleBdds)) {
    dumpGraphBdds(mCycle->getID(), mGraph);
  }

  // schedule the nodes in the cycle
  scheduleReduced(mGraph, mBDDContext.invalid(), mBDDContext.invalid());

  // invalidate the BDDs in the cycle graph
  for (Iter<GraphNode*> loop = mGraph->nodes(); !loop.atEnd(); ++loop)
  {
    GraphNode* node = *loop;
    for (Iter<GraphEdge*> iter = mGraph->edges(node); !iter.atEnd(); ++iter)
    {
      GraphEdge* edge = *iter;
      DependencyGraph::Edge* dge = mGraph->castEdge(edge);
      dge->setData(mBDDContext.invalid());
    }
  }

  mWriteDotFiles = savedWriteState;
}

//! Compute the BDD on a dependency due to brother drivers
BDD SCHCycleBreaker::brotherConditions(FLNodeElab* fromFlow, GraphNode* toNode,
                                       FlowToDGNodeMap* nodeMap)
{
  BDD cond = mBDDContext.val0();
  NUUseDefNode* useDef = fromFlow->getUseDefNode();
  if (useDef == NULL)
    return cond;
  Strength originalStrength = useDef->getStrength();
  
  // find all brother drivers of equal or greater strength
  NUNetRefHdl netRef = fromFlow->getFLNode()->getDefNetRef();
  const NUNetElab* netElab = fromFlow->getDefNet();
  FLNodeElabSet brothers;
  for (NUNetElab::ConstDriverLoop loop = netElab->loopContinuousDrivers(); !loop.atEnd(); ++loop)
  {
    FLNodeElab* driver = *loop;
    if (driver == fromFlow)
      continue; // skip ourselves
    NUUseDefNode* bud = driver->getUseDefNode();
    if (bud == NULL)
      continue; // exclude NULL use def node
    Strength brotherStrength = bud->getStrength();
    if (brotherStrength < originalStrength)
        continue; // not equal or greater strength
    NUNetRefHdl driverNetRef = driver->getFLNode()->getDefNetRef();
    if (!netRef->overlaps(*driverNetRef,false))
      continue; // the driver doesn't affect the same bits
    brothers.insert(driver);
  }
  
  // For each brother, if it has an edge to toNode, OR in the condition on that edge
  for (FLNodeElabSet::iterator l = brothers.begin(); l != brothers.end(); ++l)
  {
    FLNodeElab* driver = *l;
    FlowToDGNodeMap::iterator p = nodeMap->find(driver);
    if (p == nodeMap->end())
      continue;
    DependencyGraph::Node* brotherNode = p->second;
    for (Iter<GraphEdge*> iter = mGraph->edges(brotherNode); !iter.atEnd(); ++iter)
    {
      GraphEdge* edge = *iter;
      if (mGraph->endPointOf(edge) == toNode)
      {
        DependencyGraph::Edge* dge = mGraph->castEdge(edge);
        cond = mBDDContext.opOr(cond, dge->getData());
      }
    }
  }

  return cond;
}

//! Test if the value of the BDD depends on the given flow's output
bool SCHCycleBreaker::dependsOn(const BDD& cond, FLNodeElab* flow)
{
  if (!cond.isValid())
    return true;
  NUNetElab* netElab = flow->getDefNet();
  NUNet* net = netElab->getNet();
  if (net->isBitNet())
  {
    // This is the case were netElab is a scalar
    BDD var = mBDDContext.bdd(netElab);
    return mBDDContext.dependsOn(cond, var);
  }
  else if (net->isVectorNet())
  {
    // Get the ranges actually used
    const NUNetRefHdl& netRef = flow->getDefNetRef(mUtil->getNetRefFactory());
    for (NUNetRefRangeLoop rangeIter = netRef->loopRanges(mUtil->getNetRefFactory()); ! rangeIter.atEnd(); ++rangeIter)
    {
      const ConstantRange& range = *rangeIter;
      // netElab is a vector. Test each bit. As soon as a dependency is found, return true;
      for (SInt32 bit = range.rightmost(); bit <= range.leftmost(); ++bit)
      {
	BDD bitVar = mBDDContext.bdd(netElab, bit);
	if (mBDDContext.dependsOn(cond, bitVar))
	  return true;
      }
    }
    return false;
  }
  else
  {
    // For everything else (memories, composite nets, etc) assume that a dependency exists.
    return true;
  }
}

//! Class to keep track if a condition was covered for a given path point
/*! For each flow in the graph we only push the BDD through if we
 *  haven't pushed that condition through before
 */
class SCHCycleBreaker::BddCovered
{
public:
  //! Constructor
  BddCovered() {}

  //! Abstraction for a set of BDDs
  typedef UtHashSet<BDD> BddSet;

  //! Abstraction for a covered set of BDDs for a given path point (elab flow)
  typedef UtMap<FLNodeElab*, BddSet> Covered;

  //! Insert a condition for a flow and return if it was inserted
  bool insertWithCheck(FLNodeElab* flowElab, BDD cond)
  {
    // Get the hash set of bdds for this flow, it creates it if it
    // didn't exist before
    BddSet& bddSet = mCovered[flowElab];

    // Add it and return true if it was inserted (first time it was seen).
    return bddSet.insertWithCheck(cond);
  }

private:
  //! Storage for the covered set
  Covered mCovered;
}; // class BddCovered

//! Build a map from flow's fanin to conditions under which the fanin is used
SCHConditionMap* SCHCycleBreaker::gatherConditions(FLNodeElab* flow)
{
  FLNodeElabSet* faninSet = mMarkDesign->gatherTrueFanin(flow);
  
  UtStack<BDD> conditionStack;
  conditionStack.push(mBDDContext.val1());

  SCHConditionMap* condMap = new SCHConditionMap(&mBDDContext);
  
  BddCovered covered;
  gatherConditionHelper(flow, faninSet, &conditionStack, condMap, &covered);

  return condMap;
}

//! A helper function that does the traversal of nested fanin to build the condition map
void SCHCycleBreaker::gatherConditionHelper(FLNodeElab* flow,
                                            FLNodeElabSet* faninSet,
                                            UtStack<BDD>* condStack,
                                            SCHConditionMap* condMap,
                                            BddCovered* covered,
                                            bool checkDepends)
{
  // Add this condition to all fanin we are interested in
  bool matched = false;
  for (NUNetElab::DriverLoop l = flow->getDefNet()->loopContinuousDrivers(); !l.atEnd(); ++l)
  {
    FLNodeElab* nextFlow = *l;
    if (faninSet->find(nextFlow) != faninSet->end())
    {
      // this is a matching fanin, see if our condition depends on it      
      if (checkDepends)
      {
        if (dependsOn(condStack->top(),nextFlow))
          condMap->add(nextFlow,condStack->top());
        else
          condMap->add(nextFlow,mBDDContext.val0());
      }
      else
        condMap->add(nextFlow,condStack->top());
      matched = true;
    }
  }
  // If we matched, it means we hit a boundary for this traversal
  if (matched)
    return;

  NUUseDefNode* useDef = flow->getUseDefNode();
  if (useDef == NULL)
    return;

  // If this node is in our covered set, don't recurse to its fanin
  BDD cond = condStack->top();
  if (!covered->insertWithCheck(flow, cond)) {
    return;
  }

  // If we get here, we have a non-matching flow node that
  // we want to traverse through searching for fanin, and we have determined
  // that it is not part of an internal cycle.  

  // Handle control flow constructs
  switch (useDef->getType())
  {
    case eNUIf:
    {
      NUIf* ifStmt = dynamic_cast<NUIf*>(useDef);
      NU_ASSERT(ifStmt,useDef);

      gatherIfConditionHelper(flow, ifStmt, faninSet, condStack, condMap, covered);
    }
    break;
    case eNUCase:
    {
      NUCase* caseStmt = dynamic_cast<NUCase*>(useDef);
      NU_ASSERT(caseStmt,useDef);

      gatherCaseConditionHelper(flow, caseStmt, faninSet, condStack, condMap,
                                covered);
    }
    break;
    case eNUContEnabledDriver:
    {
      NUContEnabledDriver* edStmt = dynamic_cast<NUContEnabledDriver*>(useDef);
      NU_ASSERT(edStmt, useDef);
      gatherEnabledDriverConditionHelper(flow, edStmt, faninSet,
                                         condStack, condMap, covered);
    }
    break;
    case eNUBlockingAssign:
    case eNUContAssign:
    {
      NUAssign * assignStmt = dynamic_cast<NUAssign*>(useDef);
      NU_ASSERT(assignStmt,useDef);

      gatherAssignConditionHelper(flow, assignStmt, faninSet, condStack, condMap,
                                  covered);
    }
    break;
    default:
    {
      // This is not a control flow construct, so just continue normally
      gatherStatementConditionHelper(flow, faninSet, condStack, condMap, covered);
    }
    break;
  }
}


void SCHCycleBreaker::gatherEnabledDriverConditionHelper(FLNodeElab * flow, 
                                                         NUEnabledDriver * edStmt,
                                                         FLNodeElabSet* faninSet,
                                                         UtStack<BDD>* condStack,
                                                         SCHConditionMap* condMap,
                                                         BddCovered* covered)
{
  // add the enable condition to the condition stack and look for fanin among the drivers
  NUExpr* enableExpr = edStmt->getEnable();
  BDD cond = enableExpr->bdd(&mBDDContext,flow->getHier());
  condStack->push(mBDDContext.opAnd(condStack->top(), cond));
  NUExpr * driverExpr = edStmt->getDriver();
  gatherExprConditionHelper(flow, driverExpr, faninSet,
                            condStack, condMap, covered);
  condStack->pop();

  // look for fanin in the enable
  gatherExprConditionHelper(flow, enableExpr, faninSet,
                            condStack, condMap, covered);
}


void SCHCycleBreaker::gatherCaseConditionHelper(FLNodeElab * flow, 
                                                NUCase * caseStmt,
                                                FLNodeElabSet* faninSet,
                                                UtStack<BDD>* condStack,
                                                SCHConditionMap* condMap,
                                                BddCovered* covered)
{
  NUExpr* selectExpr = caseStmt->getSelect();

  FLCaseClassifier<FLNodeElab> classifier(flow->getUseDefNode());
  classifier.classifyFlow(flow);
  FLExprFaninClassifier<FLNodeElab> select(flow, selectExpr);

  // for each case item, add the case condition to the condition stack and
  // then look for fanin within the case item 
  NUCaseItem* defaultItem = NULL;
  BDD defaultCondition = mBDDContext.val1();
  for (NUCase::ItemLoop l = caseStmt->loopItems(); !l.atEnd(); ++l)
  {
    NUCaseItem* caseItem = *l;
    if (caseItem->isDefault())
    {
      INFO_ASSERT(defaultItem == NULL, "Case statement has mutliple default items");
      defaultItem = caseItem;
    }
    else
    {
      // compute the case condition
      NUExpr* condExpr = caseItem->buildConditional(selectExpr);
      BDD cond = condExpr->bdd(&mBDDContext,flow->getHier());
      delete condExpr;
      defaultCondition = mBDDContext.opAnd(defaultCondition,mBDDContext.opNot(cond));
      // look for fanin in the case item
      condStack->push(mBDDContext.opAnd(condStack->top(), cond));
      for (FLCaseClassifier<FLNodeElab>::FlowLoop p = classifier.loopCaseItem(caseItem, flow); !p.atEnd(); ++p)
      {
        FLNodeElab* stmtFlow = *p;
        gatherConditionHelper(stmtFlow,faninSet,condStack,condMap,covered);
      }
      // look for fanin in the select condition
      for (FLExprFaninClassifier<FLNodeElab>::FlowLoop l = select.loopExprFanin(); !l.atEnd(); ++l)
      {
        FLNodeElab* fanin = *l;
        gatherConditionHelper(fanin,faninSet,condStack,condMap,covered,true);
      }
      condStack->pop();
    }
  }
      
  // look for fanin in the default case item
  NU_ASSERT(caseStmt->hasDefault() == (defaultItem != NULL), caseStmt);
  if (defaultItem)
  {
    condStack->push(mBDDContext.opAnd(condStack->top(), defaultCondition));
    for (FLCaseClassifier<FLNodeElab>::FlowLoop p = classifier.loopCaseItem(defaultItem, flow); !p.atEnd(); ++p)
    {
      FLNodeElab* stmtFlow = *p;
      gatherConditionHelper(stmtFlow,faninSet,condStack,condMap,covered);
    }
    // look for fanin in the select condition
    for (FLExprFaninClassifier<FLNodeElab>::FlowLoop l = select.loopExprFanin(); !l.atEnd(); ++l)
    {
      FLNodeElab* fanin = *l;
      gatherConditionHelper(fanin,faninSet,condStack,condMap,covered,true);
    }
    condStack->pop();
  }
}

void SCHCycleBreaker::gatherStatementConditionHelper(FLNodeElab * flow, 
                                                     FLNodeElabSet* faninSet,
                                                     UtStack<BDD>* condStack,
                                                     SCHConditionMap* condMap,
                                                     BddCovered* covered)
{
  // If there is nested fanin below us, recurse
  if (flow->hasNestedBlock() || flow->hasNestedHier())
  {
    for (FLNodeElabLoop loop = flow->loopNested(); !loop.atEnd(); ++loop)
    {
      FLNodeElab* nested = *loop;
      gatherConditionHelper(nested,faninSet,condStack,condMap,covered);
    }
  }
  else
  {
    // This is a leaf node, so look at its fanin
    for (Fanin loop = mUtil->loopFanin(flow, eFlowIntoCycles, 0, FLIterFlags::eOverAll, false);
         !loop.atEnd(); ++loop)
    {
      FLNodeElab* fanin = *loop;
      gatherConditionHelper(fanin,faninSet,condStack,condMap,covered);
    }
  }
}

void SCHCycleBreaker::gatherAssignConditionHelper(FLNodeElab * flow, 
                                                  NUAssign * assignStmt,
                                                  FLNodeElabSet* faninSet,
                                                  UtStack<BDD>* condStack,
                                                  SCHConditionMap* condMap,
                                                  BddCovered* covered)
{
  NUExpr * rhs = assignStmt->getRvalue();
  if (rhs->getType()==NUExpr::eNUTernaryOp) {
    NUTernaryOp * top = dynamic_cast<NUTernaryOp*>(rhs);
    NU_ASSERT(top, assignStmt);

    gatherTernaryConditionHelper(flow, top, faninSet,
                                 condStack, condMap, covered);
  } else {
    // Not a processed ternary statement.
    gatherStatementConditionHelper(flow, faninSet, condStack, condMap, covered);
  }
}


void SCHCycleBreaker::gatherIfConditionHelper(FLNodeElab * flow, 
                                              NUIf * ifStmt,
                                              FLNodeElabSet* faninSet,
                                              UtStack<BDD>* condStack,
                                              SCHConditionMap* condMap,
                                              BddCovered* covered)
{
  NUExpr* conditionExpr = ifStmt->getCond();

  FLIfClassifier<FLNodeElab> classifier(flow);

  // search for fanin among the then clause
  BDD conditionBDD = conditionExpr->bdd(&mBDDContext,flow->getHier());
  condStack->push(mBDDContext.opAnd(condStack->top(), conditionBDD));
  for (FLIfClassifier<FLNodeElab>::FlowLoop l = classifier.loopThen(flow);
       not l.atEnd(); 
       ++l) {
    FLNodeElab* driver = *l;
    gatherConditionHelper(driver,faninSet,condStack,condMap,covered);
  }
  condStack->pop();

  // search for fanin among the else clause
  conditionBDD = mBDDContext.opNot(conditionBDD);
  condStack->push(mBDDContext.opAnd(condStack->top(), conditionBDD));
  for (FLIfClassifier<FLNodeElab>::FlowLoop l = classifier.loopElse(flow);
       not l.atEnd(); 
       ++l) {
    FLNodeElab* driver = *l;
    gatherConditionHelper(driver,faninSet,condStack,condMap,covered);
  }
  condStack->pop();

  // search for fanin in the select condition
  gatherExprConditionHelper(flow, conditionExpr, faninSet,
                            condStack, condMap, covered);
}



void SCHCycleBreaker::gatherExprConditionHelper(FLNodeElab * flow, 
                                                NUExpr * expr,
                                                FLNodeElabSet* faninSet,
                                                UtStack<BDD>* condStack,
                                                SCHConditionMap* condMap,
                                                BddCovered* covered)
{
  if (expr->getType()==NUExpr::eNUTernaryOp) {
    // recurse and process nested ternary.
    NUTernaryOp * top = dynamic_cast<NUTernaryOp*>(expr);
    gatherTernaryConditionHelper(flow, top, faninSet,
                                 condStack, condMap, covered);
  } else {
    FLExprFaninClassifier<FLNodeElab> classifier(flow, expr);
    for (FLExprFaninClassifier<FLNodeElab>::FlowLoop l = classifier.loopExprFanin(); 
         not l.atEnd(); 
         ++l) {
      FLNodeElab* fanin = *l;
      gatherConditionHelper(fanin,faninSet,condStack,condMap,covered);
    }      
  }
}


void SCHCycleBreaker::gatherTernaryConditionHelper(FLNodeElab * flow, 
                                                   NUTernaryOp * top,
                                                   FLNodeElabSet* faninSet,
                                                   UtStack<BDD>* condStack,
                                                   SCHConditionMap* condMap,
                                                   BddCovered* covered)
{
  NUExpr * selectExpr = top->getArg(0);

  NUExpr * arg1 = top->getArg(1);
  NUExpr * arg2 = top->getArg(2);

  // search for fanin among the then clause
  BDD conditionBDD = selectExpr->bdd(&mBDDContext,flow->getHier());
  condStack->push(mBDDContext.opAnd(condStack->top(), conditionBDD));
  gatherExprConditionHelper(flow, arg1, faninSet,
                            condStack, condMap, covered);
  condStack->pop();


  // search for fanin among the else clause
  conditionBDD = mBDDContext.opNot(conditionBDD);
  condStack->push(mBDDContext.opAnd(condStack->top(), conditionBDD));
  gatherExprConditionHelper(flow, arg2, faninSet,
                            condStack, condMap, covered);
  condStack->pop();

  // search for fanin in the select condition
  gatherExprConditionHelper(flow, selectExpr, faninSet,
                            condStack, condMap, covered);
}


//! Choose a good variable to try to break the cycle
/* Right now, we choose the variable that appears most often as the
   top-variable over all BDDs in the graph.  It is likely that we
   could do better using more expensive tests, such as looking at
   all variables in the BDDs, looking at the topology of the graph,
   etc.
 */
BDD SCHCycleBreaker::chooseBreakVariable(DependencyGraph* graph)
{
  typedef UtMap<BDD,UInt32> BDDCountMap;
  BDDCountMap counts;
  NUNetElabSet written;
  // Loop over all nodes, counting the top-variables in all BDDs
  for (Iter<GraphNode*> loop = graph->nodes(); !loop.atEnd(); ++loop)
  {
    GraphNode* node = *loop;
    DependencyGraph::Node* dgn = graph->castNode(node);
    FLNodeElab* flow = dgn->getData();

    // We cannot use a variable that is written in the same block, because
    // we will run into problems with rescoped nets, and we cannot be sure
    // that we will test the correct value of the net.
    NUUseDefNode* useDef = flow->getUseDefNode();
    STBranchNode* hier = flow->getHier();
    NUNetSet defSet;
    useDef->getDefs(&defSet);
    for (NUNetSet::iterator p = defSet.begin(); p != defSet.end(); ++p)
    {
      NUNet* net = *p;
      NUNetElab* netElab = net->lookupElab(hier);
      netElab = netElab->getStorageNet()->lookupElab(netElab->getStorageHier());
      written.insert(netElab);
    }

    // Loop over all edges from this node and find the variable for each edge
    for (Iter<GraphEdge*> iter = graph->edges(node); !iter.atEnd(); ++iter)
    {
      GraphEdge* edge = *iter;
      DependencyGraph::Edge* dge = graph->castEdge(edge);
      BDD edgeCondition = dge->getData();
      if (edgeCondition != mBDDContext.val0() &&
          edgeCondition != mBDDContext.val1())
      {
        BDD var = mBDDContext.topVariable(edgeCondition);
        if (counts.find(var) == counts.end())
          counts[var] = 1;
        else
          counts[var] += 1;
      }
    }
  }

  // choose the most-used variable (break ties based on the NUNetElab for stability)
  BDD useThisVar = mBDDContext.invalid();
  NUNetElab* useThisNetElab = NULL;
  UInt32 numUses = 0;
  for (BDDCountMap::iterator p = counts.begin(); p != counts.end(); ++p)
  {
    UInt32 count = p->second;
    bool newMax = false;

    NUNetElab* netElab = NULL;
    SInt32 bitIndex;

    if (count > numUses)
    {
      newMax = true;
      mBDDContext.decodeVariable(p->first, &netElab, &bitIndex);
    }
    else if (count == numUses)
    {
      // Break a tie using NUNetElab::compare -- it improves stability
      mBDDContext.decodeVariable(p->first, &netElab, &bitIndex);
      newMax = (NUNetElab::compare(netElab,useThisNetElab) < 0);
    }

    if (newMax)
    {
      INFO_ASSERT(netElab, "Failed to decode BDD variable");
      if (written.find(netElab) == written.end())
      {
        useThisVar = p->first;
        useThisNetElab = netElab;
        numUses = count;
      }
    }
  }

  return useThisVar;
}

//! A driver function to schedule the flow nodes in a cyclic graph
void SCHCycleBreaker::schedule(DependencyGraph* graph)
{
  BDD useThisVar = mBDDContext.invalid();
  if ((mBranchDepth < mBranchDepthLimit) && mUtil->isFlagSet(eBreakCycles))
  {
    // choose a variable to break on
    useThisVar = chooseBreakVariable(graph);
  }

  // If we are not breaking on any more variables, give up and make it a loop
  if (!useThisVar.isValid())
  {
    scheduleLoop(graph);
    return;
  }

  // Schedule this as a branch on the chosen variable
  NUNetElab* netElab = NULL;
  SInt32 bitIndex;
  mBDDContext.decodeVariable(useThisVar, &netElab, &bitIndex);
  INFO_ASSERT(netElab, "Failed to decode BDD variable");
  ++mBranchDepth;
  mCycle->scheduleBranch(netElab, bitIndex);
  if (mWriteDotFiles)
    UtIO::cout() << "Scheduling " << netElab->getNet()->getName()->str() << "[" << bitIndex << "] = 0\n";
  scheduleReduced(graph, useThisVar, mBDDContext.val0());
  mCycle->scheduleJoin();
  if (mWriteDotFiles)
    UtIO::cout() << "Scheduling " << netElab->getNet()->getName()->str() << "[" << bitIndex << "] = 1\n";
  scheduleReduced(graph, useThisVar, mBDDContext.val1());
  mCycle->scheduleJoin();
  --mBranchDepth;
  if (mWriteDotFiles)
    UtIO::cout() << "Finished " << netElab->getNet()->getName()->str() << "[" << bitIndex << "]\n";
}

typedef UnionFind<FLNodeElab*,SortedFlowElabSet>  FlowGrouper;
typedef NUNetRefMultiMap<FLNodeElab*> NetRefFlowMap;

//! Helper functions to facilitate printing of templatized value in (key,value) map
template<>
void NetRefFlowMap::helperTPrint(FLNodeElab* const & v, int indent) const
{
  v->print(false, indent);
}

//! Schedule the graph as a loop -- we've given up on breaking it
void SCHCycleBreaker::scheduleLoop(DependencyGraph* graph)
{
  mCycle->scheduleLoop();

  /* The items in the loop should be executed in an order that preserves
     correctness:

     - weaker drivers must run before stronger drivers that overlap them
     - no flow should be scheduled when it would use a value which has
       had some, but not all, of its drivers run
     - if a latch's enable is stale when the latch is computed, its
       data should also be stale.
     
     There are additional conditions that are desirable but not mandatory:

     - we want to minimize the number of cut points in the graph. 

     We meet these requirements by building a graph for scheduling
     which merges multiple overlapping flows into one node, removes
     edges from weak drivers to strong, and eliminates edges due to
     brother dependencies.  When we walk the graph, we walk edges to
     latch enables before walking to the latch data, and if the enable
     is a cut point, we make the data a cut point as well.
   */

  // first, group the flow nodes with overlapping defs
  NetRefFlowMap defMap(mUtil->getNetRefFactory());  // multi-aspect is not used
  FlowGrouper groups;
  for (Iter<GraphNode*> loop = graph->nodes(); !loop.atEnd(); ++loop)
  {
    DependencyGraph::Node* node = graph->castNode(*loop);
    FLNodeElab* flow = node->getData();
    groups.add(flow);
    const NUNetRefHdl& defNetRef = flow->getDefNetRef(mUtil->getNetRefFactory());
    for (NetRefFlowMap::CondLoop l = defMap.loop(defNetRef, &NUNetRef::overlapsBits); !l.atEnd(); ++l)
    {
      FLNodeElab* overlappingFlow = (*l).second;
      groups.equate(flow, overlappingFlow);
    }
    defMap.insert(defNetRef, flow);
  }  
  defMap.clear();
  FlowGrouper::EquivalencePartition* partition = groups.getPartition();
  
  // next, build the schedule graph from the dependency graph
  GraphBuilder<ScheduleGraph> builder;
  for (Iter<GraphNode*> loop = graph->nodes(); !loop.atEnd(); ++loop)
  {
    DependencyGraph::Node* node = graph->castNode(*loop);
    FLNodeElab* flow = node->getData();
    FLNodeElab* rep = groups.find(flow);
    SortedFlowElabSet& equivSet = (*partition)[rep];
    builder.addNode(&equivSet);
    for (Iter<GraphEdge*> iter = graph->edges(node); !iter.atEnd(); ++iter)
    {
      DependencyGraph::Edge* edge = graph->castEdge(*iter);

      // Ignore brother dependencies entirely
      if (graph->anyFlagSet(edge, eBrotherDependency))
        continue;

      DependencyGraph::Node* to = graph->castNode(graph->endPointOf(edge));
      FLNodeElab* toFlow = to->getData();
      FLNodeElab* toRep = groups.find(toFlow);
      SortedFlowElabSet& toEquivSet = (*partition)[toRep];
      builder.addEdge(&equivSet, &toEquivSet,NULL);
    }
  }
  groups.clear();
  ScheduleGraph* g = builder.getGraph();
  
  // finally, walk the schedule graph to build a schedule order
  FlowSetGraphScheduler sched(mCycle, graph);
  FlowSetGraphScheduler::NodeCmp nodeCmp(g);
  FlowSetGraphScheduler::EdgeCmp edgeCmp(g);
  sched.walk(g, &nodeCmp, &edgeCmp);
  delete partition;
  delete g;

  mCycle->scheduleJoin();
}

//! Reduce the graph using the given variable value and schedule the reduced graph
void SCHCycleBreaker::scheduleReduced(DependencyGraph* graph, const BDD& var, const BDD& value)
{
  // apply the value to the variable to get a (hopefully) reduced graph
  DependencyGraph* reduced = reduceGraph(graph, var, value);

  if (mWriteDotFiles)
  {
    UtString filename;
    filename << "reduced_cycle_" << mCycle->getID() << "_" << mBranchDepth << "_" << mReductionCount << ".dot";
    CycleGraphDotWriter cgdw(filename, &mBDDContext);
    cgdw.output(reduced);
    UtIO::cout() << "Wrote reduced graph: " << filename << "\n";
  }

  // compute the component graph of the reduced graph
  GraphSCC scc;
  Graph* cg = scc.buildComponentGraph(reduced);

  // walk the component graph scheduling flow and building sub-schedules as necessary
  ComponentGraphScheduler sched(&scc, mCycle, this);
  sched.walk(cg);

  delete reduced;
}

//! Break the cycles in the graph using the given value for the variable.
/*! Build the subgraph of the graph in which edges whose conditions are incompatible with
    the variable assignment are removed.
 */
DependencyGraph* SCHCycleBreaker::reduceGraph(DependencyGraph* graph, const BDD& var, const BDD& value)
{
  DependencyGraph* newGraph = new DependencyGraph;

  // copy the graph's nodes
  UtMap<DependencyGraph::Node*,DependencyGraph::Node*> nodeMap;
  for (Iter<GraphNode*> loop = graph->nodes(); !loop.atEnd(); ++loop)
  {
    GraphNode* node = *loop;
    DependencyGraph::Node* oldNode = graph->castNode(node);
    DependencyGraph::Node* newNode = new DependencyGraph::Node(oldNode->getData());
    newGraph->addNode(newNode);
    newGraph->setFlags(newNode,graph->getFlags(oldNode));
    nodeMap[oldNode] = newNode;
  }

  // copy edges that are compatible with the variable assignment
  for (Iter<GraphNode*> loop = graph->nodes(); !loop.atEnd(); ++loop)
  {
    GraphNode* node = *loop;
    DependencyGraph::Node* newNode = nodeMap[graph->castNode(node)];
    for (Iter<GraphEdge*> iter = graph->edges(node); !iter.atEnd(); ++iter)
    {
      GraphEdge* edge = *iter;
      DependencyGraph::Edge* dge = graph->castEdge(edge);
      BDD edgeCondition = dge->getData();
      if (var.isValid() && value.isValid())
        edgeCondition = mBDDContext.substitute(edgeCondition,var,value);
      if (edgeCondition == mBDDContext.val0())
      {
        INFO_ASSERT(!graph->anyFlagSet(edge, eStrengthDependency), "Breaking link to pull driver!");
        continue; // skip this edge - it will never flow
      }
      if (!edgeCondition.isValid())
        edgeCondition = mBDDContext.val1();
      GraphNode* toNode = graph->endPointOf(edge);
      DependencyGraph::Node* newTo = nodeMap[graph->castNode(toNode)];
      DependencyGraph::Edge* newEdge = new DependencyGraph::Edge(newTo,edgeCondition);
      newNode->addEdge(newEdge);
      newGraph->setFlags(newEdge,graph->getFlags(edge));
    }
  }

  ++mReductionCount;

  return newGraph;
}

void SCHCycleBreaker::dumpGraphBdds(int cycleId, DependencyGraph* graph)
{
  // Do a sorted walk and print the BDDS
  NodeOrder nodeOrder(graph);
  EdgeOrder edgeOrder(graph);
  PrintCycleBdds printBdds(&mBDDContext, graph);
  UtIO::cout() << "BDDs for cycle " << cycleId << ":\n";
  printBdds.walk(graph, &nodeOrder, &edgeOrder);
}
