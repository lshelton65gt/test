# Copy important files from sandbox to $CARBON_HOME.  This is done through the file command,
# with a custom target so we can copy all of them first.  This is because there might be
# files in the list that are not explicit dependencies of other targets.

add_custom_target(PrepCarbonHome)

# Other generated files

# Macro to copy files from a bom file
macro(copy_bom_files bomFile srcRoot destRoot keepHier)
  file(STRINGS ${CMAKE_SOURCE_DIR}/${bomFile} bom)
  foreach(line ${bom})
    # Filter comments and the variable setting stuff at the top.
    # We don't support that.
    if (NOT((${line} MATCHES "^#") OR (${line} MATCHES "=")))
      set(srcFile ${srcRoot}/${line})
      # Build the destination based on whether we're keeping the hierarchy
      if(${keepHier})
        set(destFile ${destRoot}/${line})
      else()
        get_filename_component(fileName ${srcFile} NAME)
        set(destFile ${destRoot}/${fileName})
      endif()
      get_filename_component(destDir ${destFile} PATH)
      # Do the copy
      file(COPY ${srcFile} DESTINATION ${destDir})
    endif()
  endforeach()
endmacro()

# Python files are required for all platforms.  The target location is
# slightly different on Windows.
if(${WIN32})
  copy_bom_files(python_wrappers.bom ${CMAKE_SOURCE_DIR}/python ${CARBON_HOME}/${ARCH}/python/Lib/site-packages TRUE)
else()
  copy_bom_files(python_wrappers.bom ${CMAKE_SOURCE_DIR}/python ${CARBON_HOME}/${ARCH}/python/lib/python2.4/site-packages TRUE)
endif()

add_dependencies(PrepCarbonHome MsgCompilerOutput)
add_dependencies(PrepCarbonHome CodegenGeneratedFiles)

# The rest of these files should have been set up by the Linux build, so skip
# on Windows.
if(${WIN32})
  return()
endif()

# Headers from Makefile.top
file(STRINGS ${CMAKE_SOURCE_DIR}/usrincludes.bom usrIncludes)
foreach(f ${usrIncludes})
  set(srcRoot ${CMAKE_SOURCE_DIR}/inc)
  if (${f} MATCHES "^carbonsim" OR ${f} MATCHES "^eslapi")
    # carbonsim/eslapi directory is included
    set(thisInclude ${CARBON_HOME}/include/${f})
  else(${f} MATCHES "^carbonsim" OR ${f} MATCHES "^eslapi")
    # directory is stripped, and file is placed in $CARBON_HOME/include/carbon
    get_filename_component(baseFile ${f} NAME)
    set(thisInclude ${CARBON_HOME}/include/carbon/${baseFile})
    if (${f} MATCHES "^modshell" OR ${f} MATCHES "^wavetestbench" OR ${f} MATCHES "^modelvalidation")
      # These come from src, not src/inc
      set(srcRoot ${CMAKE_SOURCE_DIR})
    endif(${f} MATCHES "^modshell" OR ${f} MATCHES "^wavetestbench" OR ${f} MATCHES "^modelvalidation")
  endif(${f} MATCHES "^carbonsim" OR ${f} MATCHES "^eslapi")

  get_filename_component(destDir ${thisInclude} PATH)
  file(COPY ${srcRoot}/${f} DESTINATION ${destDir})
endforeach(f)

# Other .bom files that used to be handled by carbon_config
copy_bom_files(images.bom ${CMAKE_SOURCE_DIR} ${CARBON_HOME}/lib/images FALSE)
copy_bom_files(makefiles.bom ${CMAKE_SOURCE_DIR} ${CARBON_HOME}/makefiles FALSE)
copy_bom_files(cmm/Templates/templates.bom ${CMAKE_SOURCE_DIR} ${CARBON_HOME}/lib/templates FALSE)
copy_bom_files(xactors_com.bom ${CMAKE_SOURCE_DIR} ${CARBON_HOME}/lib TRUE)
copy_bom_files(xactors_usr.bom ${CMAKE_SOURCE_DIR} ${CARBON_HOME}/include TRUE)
copy_bom_files(socdsysc/templates.bom ${CMAKE_SOURCE_DIR} ${CARBON_HOME}/lib/templates FALSE)
# Special case makefile
add_custom_command(OUTPUT ${CARBON_HOME}/makefiles/Modshell.mc
                   COMMAND mkdir -p ${CARBON_HOME}/makefiles
                   COMMAND ${COPY} ${CMAKE_SOURCE_DIR}/modshell/Makefile.mc ${CARBON_HOME}/makefiles/Modshell.mc
                   DEPENDS ${CMAKE_SOURCE_DIR}/modshell/Makefile.mc
                   )
add_custom_target(modshellmc ALL DEPENDS ${CARBON_HOME}/makefiles/Modshell.mc)

# VHDL library links
add_custom_target(VHDLLib ALL ${CMAKE_COMMAND} -E make_directory ${CARBON_HOME}/lib
                  COMMAND ${CMAKE_COMMAND} -E remove ${CARBON_HOME}/lib/vhdl
                  COMMAND ${CMAKE_COMMAND} -E remove ${CARBON_HOME}/lib/vhdl64
                  COMMAND ${CMAKE_COMMAND} -E create_symlink ${MVV_BASE}/linux/auxi/vhdl_libs ${CARBON_HOME}/lib/vhdl
                  COMMAND ${CMAKE_COMMAND} -E create_symlink ${MVV_BASE}/linux64/auxi/vhdl_libs ${CARBON_HOME}/lib/vhdl64
                  DEPENDS ${CARBON_HOME}/scripts/mvv_release_env
                  COMMAND mkdir -p ${CARBON_HOME}/lib/verific/vhdl_packages
                  COMMAND ${CMAKE_COMMAND} -E remove ${CARBON_HOME}/lib/verific/vhdl_packages/vdbs
                  COMMAND ${CMAKE_COMMAND} -E remove ${CARBON_HOME}/lib/verific/vhdl_packages/vdbs_2008
                  COMMAND ${CMAKE_COMMAND} -E remove ${CARBON_HOME}/lib/verific/vhdl_packages/ieee_1993
                  COMMAND ${CMAKE_COMMAND} -E remove ${CARBON_HOME}/lib/verific/vhdl_packages/ieee_2008
                  COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_SOURCE_DIR}/Vendors/verific/vhdl_packages/ieee_1993 ${CARBON_HOME}/lib/verific/vhdl_packages/ieee_1993
                  COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_SOURCE_DIR}/Vendors/verific/vhdl_packages/ieee_2008 ${CARBON_HOME}/lib/verific/vhdl_packages/ieee_2008
                  )


# SystemVerilog library links
add_custom_target(SystemVerilogLib ALL ${CMAKE_COMMAND} -E make_directory ${CARBON_HOME}/lib
                  COMMAND mkdir -p ${CARBON_HOME}/lib/verific/verilog_packages
                  COMMAND ${CMAKE_COMMAND} -E remove ${CARBON_HOME}/lib/verific/verilog_packages/sdbs
                  COMMAND ${CMAKE_COMMAND} -E remove ${CARBON_HOME}/lib/verific/verilog_packages/std.sv
                  COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_SOURCE_DIR}/Vendors/verific/verilog_packages/sdbs ${CARBON_HOME}/lib/verific/verilog_packages/sdbs
                  COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_SOURCE_DIR}/Vendors/verific/verilog_packages/std.sv ${CARBON_HOME}/lib/verific/verilog_packages/std.sv
                  )

