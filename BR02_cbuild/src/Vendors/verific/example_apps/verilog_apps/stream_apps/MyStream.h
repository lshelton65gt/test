/*
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
*/

#ifndef _VERIFIC_MY_STREAM_H_
#define _VERIFIC_MY_STREAM_H_

#include "VerificStream.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------- */

class VFC_DLL_PORT my_stream : public verific_stream
{
public:
    explicit my_stream(const char *file) ;

    virtual ~my_stream() ;

private:
    // Prevent compiler from defining the following
    my_stream() ;                             // Purposely leave unimplemented
    my_stream(const my_stream &) ;            // Purposely leave unimplemented
    my_stream& operator=(const my_stream &) ; // Purposely leave unimplemented

public:
    virtual unsigned          eof() const  { return _error ; }
    virtual unsigned          fail() const { return _error ; }
    virtual unsigned          bad() const  { return _error ; }

    virtual unsigned          read(char *buf, long max_size) ;

protected:
    FILE      *_file ;
    unsigned   _error ;
} ; // class my_stream

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_MY_STREAM_H_

