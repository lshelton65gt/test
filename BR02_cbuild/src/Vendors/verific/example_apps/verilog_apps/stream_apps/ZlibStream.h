/*
 *
 * [ File Version : 1.7 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_ZLIB_STREAM_H_
#define _VERIFIC_ZLIB_STREAM_H_

#include <zlib.h>

#include "VerificStream.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------- */

class VFC_DLL_PORT verific_zip_stream : public verific_stream
{
public:
    explicit verific_zip_stream(const char *gzip_file) ;

    virtual ~verific_zip_stream() ;

private:
    // Prevent compiler from defining the following
    verific_zip_stream() ;                                      // Purposely leave unimplemented
    verific_zip_stream(const verific_zip_stream &) ;            // Purposely leave unimplemented
    verific_zip_stream& operator=(const verific_zip_stream &) ; // Purposely leave unimplemented

public:
    virtual unsigned          eof() const  { return _error ; }
    virtual unsigned          fail() const { return _error ; }
    virtual unsigned          bad() const  { return _error ; }

    virtual unsigned          read(char *buf, long max_size) ;

protected:
    gzFile     _file ;
    unsigned   _error ;
} ; // class verific_zip_stream

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_ZLIB_STREAM_H_

