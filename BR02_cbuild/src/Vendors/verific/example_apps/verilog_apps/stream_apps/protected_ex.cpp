/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Array.h"          // Make dynamic array class Array available
#include "Strings.h"        // String utilities
#include "Protect.h"        // Make base protection class available to include decryption algorithm

#include "Message.h"        // Make message handlers available, not used in this example

#include "veri_file.h"      // Make Verilog reader available

#include <string>           // for memset

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
******************************* Protect EXAMPLE #1 **************************************
*****************************************************************************************

                                NOTES

    The following example is for instructional purposes only.  The code demonstrates
    how a decryption algorithm can be plugged into the Verific Analyzer.

*****************************************************************************************/

// The cipher class is derived from base class Protect (util/Protect.h)
// It implements the decryption algorithm and is plugged into the parser
// During analysis we execute this algorithm on the encrypted blocks and
// use the returned (decrypted) string to build the parse tree.
// The ModuleItem nodes created from such encrypted text is marked to
// prevent any accidental output of the protected code

class VFC_DLL_PORT Cipher : public Protect
{
public:
    Cipher() : Protect(64) {}
    virtual ~Cipher() {}

    // "protected.v" uses simple encryption. Each ASCII character between
    // `protected and `endprotected is shifted by one. Here apply the
    // reverse algorithm...
    //
    // This class already provides another decrypt method that works on
    // the "data_block" from the directive_map and uses the same decryption
    // algorithm (as no encryption method is present in the directives). So
    // in the context of this application, following method is redundant.
    // But if provided, this will execute for the `protected block.
    virtual unsigned decrypt(const char *in_buf, char *out_buf, unsigned size) {
        if (!in_buf || !out_buf) return 0 ;
        unsigned in_len = Strings::len(in_buf) ;
        if (in_len > size) return 0 ;

        memset(out_buf, 0, size) ; // set all bytes to '\0'
        unsigned i = 0 ;
        for (i = 0 ; i < in_len ; ++i) {
            out_buf[i] = in_buf[i]-1 ; // decrement the ASCII value
        }

        return size ;
    }

    // If RTL uses `pragma protect style as per section 34 in the
    // IEEE 1800 (2009) LRM, decrypt routine should check for the
    // encryption directives and return a decrypted string
    virtual char *decrypt(void) {
        // _directive_map has the <directive, value> pair for
        // all the directives in the same order they appear in
        // protection envelop. Use FOREACH_MAP_ITEM to iterate
        // over them and do the needful.
        //
        // For our simple application, we have a single directive
        // and fixed simple encryption. Just fetch the encrypted
        // text by the directive name.
        const char *in_buf = GetDirectiveValue("data_block") ;
        if (!in_buf) return 0 ;

        unsigned in_len = Strings::len(in_buf) ;
        char *out_buf = Strings::allocate(in_len+1) ;
        memset(out_buf, 0, in_len+1) ; // set all bytes to '\0'
        unsigned i = 0 ;
        for (i = 0 ; i < in_len ; ++i) {
            out_buf[i] = in_buf[i]-1 ; // decrement the ASCII value
        }

        return out_buf ; // The string will be free'd outside
    }
} ;

int main(int argc, char **argv)
{
    /*
       Steps for Verilog parse tree creation

       -- data structure:
        assumption: file x library

        1) read in all saved libraries: standard, libmap libraries.

        2) sort order of files

        3) analyze all vhdl code to nominated library (file -> library map)

        --check for switch
        4) library saving stage:
        save all analyzed code to libraries

        5) if (compile Only) {
        return with analyzed status;
        }

        6) elaborate from top level module

     */
    //1) read in all saved libraries: standard, libmap libraries.
    //Adding library with name A in library path in current libdesign.A directory wich already created
    //Verific::veri_file::AddLibraryPath("A", "libdesign.A");
    veri_file veri_reader ;
    if (!veri_reader.Analyze("test2.v", 1, "work")) return 1 ;
    //`Verific::veri_file::Save("A","test2");
    //Verific::veri_file::Restore("A","test2.v");

    // Now analyze the protected Verilog file "protected.v" (into the work library).
    // In case on any error do not process further.
    // "protected.v" has protected RTL using both the styles
    //    veri_file veri_reader ;
    //    if (!veri_reader.Analyze("protected.v", 1)) return 1 ;


    // Print out the parse tree..
    // Please note that many parse tree nodes are protected, they cannot be printed out
    //veri_file::PrettyPrint("analyze.v", 0) ;

    // The RTL has protected region. Binary dump is not safe, as other
    // applications using Verific front-end can read it back. Verific
    // issues WARNING message in this case.
    //
    // To avoid this risk, please plug in an encryption algorithm to
    // encrypt the entire binary dump. The way to do this would be
    // using veri_file::SetProtectionObject API that encrypts the
    // information before binary dump or decrypts the data before
    // restoring it. The encrypt/decrypt virtual methods need to
    // implement the algorithms in the plug-in object class.
    //(void) veri_file::SetDefaultLibraryPath(".") ;
   // (void) veri_file::Save("work", "test") ;

    return 0 ; // status OK.
}
