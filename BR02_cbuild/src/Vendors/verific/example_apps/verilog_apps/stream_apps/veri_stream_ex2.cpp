/*
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
*/

#include "Message.h"   // Make message handlers available
#include "veri_file.h" // Make Verilog reader available
#include "MyStream.h"  // Make my own stream class visible

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

verific_stream *
GetMyStream(const char *file_name)
{
    if (!file_name) return 0 ;

    // Return a newly allocated object of 'verific_zip_stream' class:
    verific_stream *stream = new my_stream(file_name) ;

    if (stream->fail()) {
        // Something bad happned:
        delete stream ;
        stream = 0 ;
    }

    return stream ; // This object will be absorbed by Verific lexer.
}

int main(int argc, char **argv)
{
#ifndef VERILOG_FLEX_READ_FROM_STREAM
    #pragma message("This application example requires VERILOG_FLEX_READ_FROM_STREAM to be enabled in order to buid.")
    #pragma message("Please enable VERILOG_FLEX_READ_FROM_STREAM.")
    Message::PrintLine("This application example requires the compile-flag VERILOG_FLEX_READ_FROM_STREAM (located in verilog/VeriCompileFlags.h) to be active in order to run!") ;
#else
    const char *file_name = "enc_test.v" ; // Stores the file name
    if (argc > 1) file_name = argv[1] ;

    // Register the call back routine to return my stream:
    veri_file::RegisterFlexStreamCallBack(GetMyStream) ;

    // Now analyze the encrypted file (into the work library):
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veri_file::Analyze(file_name, veri_file::VERILOG_2K /* analyze in Verilog 2K mode */)) return 1 ;

    // Pretty print the parse tree:
    veri_file::PrettyPrint("veri_stream_ex2_out.v", 0 /* pretty print all modules */) ;
#endif

    return 0 ; // Status OK.
}

