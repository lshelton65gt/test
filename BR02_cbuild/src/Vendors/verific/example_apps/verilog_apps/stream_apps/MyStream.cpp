/*
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
*/

#include <cstdio>
#include "MyStream.h"

using namespace std ;

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

my_stream::my_stream(const char *file) :
    verific_stream(),
    _file(),
    _error(0)
{
    _file = fopen(file, "r") ;
    if (_file == NULL) _error = 1 ;
}

my_stream::~my_stream()
{
    if (_file != NULL) fclose(_file) ;
}

unsigned
my_stream::read(char *buf, long max_size)
{
    if (_error || !buf) return 0 ;

    *buf = '\0' ;
    _gcount = fread(buf, sizeof(char), max_size, _file) ;
    if (_gcount < 0) _error = 1 ;

    // My decrypt unshift by one:
    for (unsigned i=0; i<_gcount; i++) buf[i]-- ;

    return 1 ;
}

