/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Array.h"          // Make class Array available
#include "Set.h"            // Make associated hash table class Set available

#include "Message.h"        // Make message handlers available

#include "ExampleVisitor.h" // Make visitor pattern available

#include "veri_file.h"      // Make verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriId.h"         // Definitions of all verilog identifier nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "Strings.h"        // Definition of class to manipulate copy, concatenate, create etc...

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
********************** VERILOG STATIC ELABORATION EXAMPLE #1 ****************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is an example which demonstrates the following :

        1) how to parse (analyze) a verilog design
        2) how to statically elaborate the verilog parse tree
        3) how to traverse the resulting statically elaborated
           parse-tree for uniquified instantiations

    This example makes use of a visitor class, which is well-known SW design
    pattern for traversing a data-structure.  The base verilog visitor class
    is located in the VeriVisitor.* files, and this base class already defines
    the default traversing algorithm to walk a verilog parse-tree.  A newly
    derived class, called ExampleVisitor, was created to collect certain verilog
    parse-tree constructs very easily.  Please take a look at the ExampleVisitor.*
    files for details.

    The example can accept the name of a Verilog design to be parsed from the
    user as an optional command line argument without which it works on the
    default design ie. on "ex1_top.v".

    Lastly, I recommend running through this example in a debugger, since
    you'll be able to look at the state of the objects and see everything
    that is there.

    As a disclaimer, this example was handmade specifically for the design
    file that is being read in.  Because of this, many NULL pointer checks
    are not needed.  Ideally there should be NULL pointer checks everywhere.

*****************************************************************************************/

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    if (argc < 2) Message::PrintLine("Default input file: ex1_top.v. Specify command line argument to override") ;

    const char *file_name = 0 ;
    if (argc>1) {
        file_name = argv[1] ; // Set the file name as specified by the user
    } else {
        file_name = "ex1_top.v" ; // Set default file name
    }

    // |/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\|
    // |             ANALYSIS             |
    // |\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/|

    // Now analyze the top-level design (in verilog2k mode)
    // Return in case of failure.
    if (!veri_reader.Analyze(file_name, 1/*v2k*/)) return 1 ;

    // Get all the top level modules
    Array *all_top_modules = veri_file::GetTopModules("work") ;
    // Get a handle of the first top level module, if any.
    VeriModule *mod = (all_top_modules && all_top_modules->Size()) ? (VeriModule *)all_top_modules->GetFirst() : 0 ;
    delete all_top_modules ; // Free dynamically allocated memory
    if (!mod) {
        // This should never happen for this example
        Message::Msg(VERIFIC_ERROR, 0, 0, "an error occurred with this example") ;
        return 0 ;
    }

    //---------------------------------------
    //  Pretty Print (Pre Static Elaboration)
    //---------------------------------------
    char *before_print = Strings::save(mod->Name(), "_before_pp.v") ;

    // Pretty-print the design (the entire library)
    veri_reader.PrettyPrint(before_print, 0, 0) ;
    Strings::free(before_print) ; // Free dynamically allocated memory no longer in use

    // |/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\|
    // |        STATIC ELABORATION        |
    // |\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/|

    // Ok now let's elaborate this module
    // Return in case of failure.
    if (!veri_reader.ElaborateAllStatic()) return 1 ;

    //---------------------------------------
    //  Pretty Print (Post Static Elaboration)
    //---------------------------------------
    char *after_print = Strings::save(mod->Name(), "_after_pp.v") ;

    // Pretty-print the design again, to see the difference between pre
    // static-elaboration and post static-elaboration ...
    veri_reader.PrettyPrint(after_print, 0, 0) ;
    // You'll now see that all instantiations have been uniquified.

    Strings::free(after_print) ; // Free dynamically allocated memory no longer in use

    // Use a visitor pattern to easily accumulate all module instantiations.
    // Please take a look in ExampleVisitor.h and ExampleVisitor.cpp for details.
    ExampleVisitor visitor ;

    // Traverse parse-tree and collect what we're looking for
    mod->Accept(visitor) ;

    // Now let's look at all module instantiations, which are uniquified, that we collected
    VeriModuleInstantiation *module_inst ;
    SetIter si ;
    Set *module_inst_set = visitor.GetModuleInsts() ;
    FOREACH_SET_ITEM(module_inst_set , si, &module_inst) {
        // Get module's name
        const char *name = module_inst->GetModuleName() ;
        (void) name ; // use the name
        // Get strength (if it exists)
        VeriStrength *strength = module_inst->GetStrength() ;
        (void) strength ; // use VeriStrength API on this
        // Get parameter values (if they exist)
        unsigned i ;
        VeriExpression *param ;
        Array *parm_val_arr = module_inst->GetParamValues() ;
        FOREACH_ARRAY_ITEM(parm_val_arr , i, param) {
            // Do what you want here ...
        }
        // Get instances (the indentifiers)
        VeriInstId *inst ;
        Array *inst_arr = module_inst->GetInstances() ;
        FOREACH_ARRAY_ITEM(inst_arr, i, inst) {
            // Get instance's name
            const char *inst_name = inst->Name() ;
            (void)inst_name ;// use the name.
            // Get handle to uniquified module description
            VeriModule *instantiated_module = inst->GetInstantiatedModule() ;
            (void)instantiated_module ; // use VeriModule API on this.

            // The following information can be attained from a VeriModule, so
            // do what you want with it here :
            //
            //      const char *GetName()
            //      VeriIdDef *GetId()
            //      Array *GetPorts()
            //      Array *GetParameters() or VeriIdDef *GetParam(const char *name)
            //      VeriScope *GetScope()
            //      Array *GetParameterConnects()
            //      Array *GetPortConnects()
            //      Array *GetModuleItems()
            //      unsigned IsCellDefine()
            //      unsigned GetDefaultNetType()
            //      unsigned GetUnconnectedDrive()
            //      const char *GetTimeScale()
            //      Map *GetAttributes()
            //      VeriLibrary *GetLibrary()

            // Get instance's range (if it exists)
            VeriRange *range = inst->GetRange() ;
            (void)range ; // use the range
            // Get port connects
            unsigned j ;
            VeriPortConnect *port_connect ;
            Array *port_arr = inst->GetPortConnects() ;
            FOREACH_ARRAY_ITEM(port_arr, j, port_connect) {
                // Do what you want here ...
            }
        }
    }

    // All done. Wasn't that easy!

    return 1 ;
}

