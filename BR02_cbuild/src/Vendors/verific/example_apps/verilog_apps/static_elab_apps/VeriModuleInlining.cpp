/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Message.h"        // Make message handlers available
#include "veri_file.h"      // Make verilog reader available

#include "VeriModuleInliningVisitor.h"     // Make visitor pattern available

#include "Strings.h"
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive

using namespace std ;

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*******************************************************************************
******************************* VERILOG EXAMPLE ********************************
********************************************************************************

                                NOTE TO THE READER:

    The following is an example demonstrates the following :

    Given a module, under some conditions it will be inlined
    Followings are the conditions for module inlining:
        1) Check instantiated module:
           i) Module should contain attribute "module inlining"

           ii) Module should not contain:
               a) Any reg/unpacked type port in port declarations.
               b) Any local reg type declarations.
               c) Parameter declarations.
               d) Module instantiations.
               e) Table declarations.
        2) Check instantiating module: Do not inline if:
            a) Actual port of the instance is an expression and the corresponding
               formal port in instantiated module is inout.

    This check is done by visting the analysed verfic parse tree with a visitor
    class ModuleInliningVisitor. If routine for checking for module inlining
    'CheckForModuleInlining()' returns true we collect the module instance vrs
    the instantiated module in a map '_module_insts'.

    Now FOREACH_MAP_ITEM of '_module_insts' we create an array of new module items
    with routine 'ModuleInlining()' and replace the module instantiation with that

********************************************************************************
********************************************************************************
*******************************************************************************/

int main (int argc, char **argv)
{
    if (argc < 2) Message::PrintLine("Default input file: test.v. Specify command line argument to override: [-L <lib_name>] [-y <Ydir_name>] [-v <v_file_name>] [-incdir <incdir_name>] [input_file_name]") ;

    char *file_name = 0 ; // Stores the file name
    Array file_names(2) ;

    int i = 1 ;
    while(i<argc) {
        if (Strings::compare(argv[i], "-L")) { // Add -L option specified libraries
            char *lib_name = (i+1 < argc) ? argv[i+1] : 0 ;
            if (!lib_name) {
                Message::Info(0, "Specify library name") ;
                return 1 ;
            }
            veri_file::AddLOption(lib_name) ;
            i = i+2 ;
        } else if (Strings::compare(argv[i], "-y")) { // Add y directory names
            char *dir_name = (i+1 < argc) ? argv[i+1] : 0 ;
            if (!dir_name) {
                Message::Info(0, "Specify y directory name") ;
                return 1 ;
            }
            veri_file::AddYDir(dir_name) ;
            i = i+2 ;
        } else if (Strings::compare(argv[i], "-v")) { // Add file name
            char *v_file_name = (i+1 < argc) ? argv[i+1] : 0 ;
            if (!v_file_name) {
                Message::Info(0, "Specify v_file name") ;
                return 1 ;
            }
            veri_file::AddVFile(v_file_name) ;
            i = i+2 ;
        } else if (Strings::compare(argv[i], "-incdir")) { // Add file name
            char *incdir_name = (i+1 < argc) ? argv[i+1] : 0 ;
            if (!incdir_name) {
                Message::Info(0, "Specify include directory name") ;
                return 1 ;
            }
            veri_file::AddIncludeDir(incdir_name) ;
            i = i+2 ;
        } else {
            file_name = argv[i] ;
            i = i+1 ;
            file_names.InsertLast(file_name) ;
        }
    }

    // If no file name is supplied then take the default file name
    if (!file_name) {
        file_name = Strings::save("test.v") ;
        file_names.InsertLast(file_name) ;
    }

    // |/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\|
    // |             ANALYSIS             |
    // |\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/|

    // Now analyze the Verilog file (into the work library). In case of any error do not process further.
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veri_file::AnalyzeMultipleFiles(&file_names, 3)) return 1 ;
    VeriTreeNode::SetStaticElab() ; // set static elab flag

    // Get the list of top modules
    Array *top_mod_array = veri_file::GetTopModules() ;
    if (!top_mod_array) {
        // If there is no top level module then issue error
        Message::Error(0,"Cannot find any top level module. Check for recursive instantiation") ;
        return 1 ;
    }

    VeriModule *module ;
    unsigned ai ;
    // Comment the following line if you want to look for a module by its name and hence have uncommented the previous line
    FOREACH_ARRAY_ITEM(top_mod_array, ai, module) {
        if (!module) {
            // This should never happen
            Message::Msg(VERIFIC_ERROR, 0, 0, "an error occurred with this example") ;
            return 1 ;
        }

        VeriModuleInliningVisitor visitor ;
        // This routine will check conditions for module_inlining and collect module instances vrs
        // instantiating module in a map and return the map.
        Map *mod_insts = visitor.CollectModuleInstances(module) ;

        MapIter mi ;
        VeriModuleInstantiation *item ;
        VeriModule *mod ;
        FOREACH_MAP_ITEM(mod_insts, mi, &item, &mod) {
            if (!item || !mod) continue ;
            // This routine will create an array of new module items for each module instantiation 'item'
            Array *child_items = visitor.ModuleInlining(item, mod->GetScope()) ;
            (void) mod->ReplaceChildBy(item, child_items) ;
            delete child_items ;
        }
    }
    delete top_mod_array ;

    // ouput file:
    char *out_file_name = Strings::save("pp_", file_name);

    (void) veri_file::PrettyPrint(out_file_name, 0, 0) ;
    Strings::free(out_file_name) ;

    return 0 ;
}
