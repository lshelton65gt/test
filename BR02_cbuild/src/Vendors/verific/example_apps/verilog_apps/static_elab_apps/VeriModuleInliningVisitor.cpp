/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <iostream>

#include "VeriModuleInliningVisitor.h"     // Make visitor pattern available

#include "Map.h"

#include "VeriModule.h"
#include "VeriExpression.h"
#include "VeriId.h"
#include "VeriStatement.h"
#include "VeriMisc.h"
#include "VeriConstVal.h"
#include "VeriCopy.h"

#include "veri_tokens.h"

using namespace std ;

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*-----------------------------------------------------------------*/
//                      Constructor / Destructor
/*-----------------------------------------------------------------*/

VeriModuleInliningVisitor::VeriModuleInliningVisitor()
  : VeriVisitor(),
    _module_insts(POINTER_HASH),
    _already_checked_list(POINTER_HASH),
    _module(0)
{
}

VeriModuleInliningVisitor::~VeriModuleInliningVisitor()
{
    _module_insts.Reset() ;
    _already_checked_list.Reset() ;
    _module = 0 ;
}

/*-----------------------------------------------------------------*/
//        Visit Methods : Class details in VeriModuleItem.h
/*-----------------------------------------------------------------*/

// Overwrite visitor routine of VeriModule
void VeriModuleInliningVisitor::VERI_VISIT(VeriModule, node)
{
    VeriModule *old_module = _module ;
    _module = &node ;

    // Traverse module items
    TraverseArray(node.GetModuleItems()) ;

    _module = old_module ;
}

// Overwrite visitor routine of VeriModuleInstantiation
void VeriModuleInliningVisitor::VERI_VISIT(VeriModuleInstantiation, node)
{
    // Check for module inlining conditions and collect the module instantiations
    if (!VeriModuleInliningVisitor::CheckForModuleInlining(&node)) return ;

    (void) _module_insts.Insert(&node, _module) ;  // Store this node

    // We don't need to traverse any of this class's members.
}

/*----------------------------------------------------------------*/

// Given a module, under some conditions it will be inlined
// Followings are the conditions for module inlining:
//     1) Check instantiated module:
//        i) Module should contain attribute "module inlining"
//
//        ii) Module should not contain:
//            a) Any reg/unpacked type port in port declarations.
//            b) Any local reg type declarations.
//            c) Parameter declarations.
//            d) Module instantiations.
//            e) Table declarations.
//     2) Check instantiation : Do not inline if:
//         a) Actual port of the instance is an expression and the corresponding
//            formal port in instantiated module is inout.
//
// This check is done by visting the analyzed Verfic parse tree with a visitor
// class ModuleInliningVisitor. If routine for checking for module inlining
// 'CheckForModuleInlining()' returns true we collect the module instance vrs
// the instantiated module in a map '_module_insts'.
//
// Now FOREACH_MAP_ITEM of '_module_insts' we create an array of new module items
// with routine 'ModuleInlining()' and replace the module instantiation with that

// Check conditions for module-inlining:
unsigned VeriModuleInliningVisitor::CheckForModuleInlining(const VeriModuleInstantiation *instantiation)
{
    // Get the instantiated module
    VeriModule *instantiated_module = instantiation->GetInstantiatedModule() ;
    if (!instantiated_module) return 0 ;

    unsigned i ;
    // A module can be instantiated more than once, so keep the instantiated modules in a
    // map, so that onece a module is checked for inlining should not be checked again and
    // again for all the instantiations.
    MapItem *item = _already_checked_list.GetItem(instantiated_module) ;
    if (item) { // already processed module
        if ((unsigned long)item->Value() == 0) return 0 ; // problem in instantiated module
    } else {
        // Attributes checking on instantiated module
        // Check for attribute "module_inlining" on the module
        if (!instantiated_module->GetAttribute("module_inlining")) {
            (void) _already_checked_list.Insert(instantiated_module, (void*)0L) ; // collect modules that are already checked for inlining
            Message::Warning(0, "attribute 'module_inlining' is not specified") ;
            return 0 ;
        }

        // Check ports of instantiated module
        Array *ports = instantiated_module->GetPorts() ; // Get the formal ports
        VeriIdDef *port ;
        FOREACH_ARRAY_ITEM(ports, i, port) {
            if (!port) continue ;
            if (port->IsReg() || port->IsUnpacked()) { // port is reg type/unpacked type
                (void) _already_checked_list.Insert(instantiated_module, (void*)0L) ; // collect modules that are already checked for inlining
                Message::Warning(0, "reg/unpacked type ports are not supported in module inlining") ;
                return 0 ;
            }
        }

        // Check declarations of the instantiated module:
        Array *module_items = instantiated_module->GetModuleItems() ; // collect the module items
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(module_items, i, item) {
            if (!item) continue ;
            // check for parameter/table declarations or module instantiations
            if (item->IsInstantiation() || item->IsTable() || item->IsParamDecl()) {
                (void) _already_checked_list.Insert(instantiated_module, (void*)0L) ; // collect modules that are already checked for inlining
                Message::Warning(0, "parameter/table declarations or instantiations are not supported in module inlining") ;
                return 0 ; // do not inline if the module contains table and instantiations
            }

            if (item->IsDataDecl()) {
                // check for reg declarations
                VeriDataType *data_type = item->GetDataType() ;
                unsigned type = data_type ? data_type->Type() : 0 ;
                if (type == VERI_REG) { // issue warning for local reg type declarations
                    (void) _already_checked_list.Insert(instantiated_module, (void*)(unsigned long)0) ; // collect modules that are already checked for inlining
                    Message::Warning(0, "reg type declarations are not supported in module inlining") ;
                    return 0 ;
                }
            }
        }

        (void) _already_checked_list.Insert(instantiated_module, (void*)1L) ; // collect modules that are already checked for inlining
    }

    // Check for instantiated module
    Array *ports = instantiated_module->GetPorts() ; // Get the formal ports

    Array *instances = instantiation->GetIds() ;
    VeriInstId *inst_id ;
    FOREACH_ARRAY_ITEM(instances, i, inst_id) {
        if (!inst_id) continue ;
        // Get the port connections of this instance:
        Array *actual_ports = inst_id->GetPortConnects() ;
        if (!actual_ports) continue ;

        VeriIdDef *formal ;
        VeriExpression *actual ;
        unsigned j ;
        FOREACH_ARRAY_ITEM(actual_ports, j, actual) {
            if (!actual) continue ;

            // Flag dot-star actuals, and skip. We will visit them later.
            if (actual->IsDotStar()) {
                return 0 ;
            }
            formal = 0 ;
            if (actual->NamedFormal()) {
                // This is a (SV-style) named formal.
                formal = instantiated_module->GetPort(actual->NamedFormal()) ;
            } else {
                // This is a ordered formal
                if (j>=((ports) ? ports->Size() : 0)) {
                    break ; // no use checking more.
                }
                formal = (ports) ? (VeriIdDef*)ports->At(j) : 0 ;
            }
            if (!formal || !formal->IsInout()) continue ;

            VeriExpression *actual_connc = actual->GetConnection() ;
            VeriIdDef *actual_id = actual_connc->FullId() ;
            // Issue warning if actual port is a concat or complex expression and the formal port is inout
            if (!actual_id) { // expression
                Message::Warning(0, "inout ports are not supported in module inlining") ;
                return 0 ;
            }
        }
    }

    return 1 ;
}

// This routine will check conditions for module_inlining and collect module instances vrs
// instantiating module in a map and return the map.
Map *VeriModuleInliningVisitor::CollectModuleInstances(VeriModule *module)
{
    // Use a visitor pattern to easily accumulate the following. Please take a look
    // in Ex7Visitor.h for details.
    // This visitor will collect all the module instances with attribute 'module inlining'
    // Traverse parse-tree and collect what we're looking for
    module->Accept(*this) ;
    return &_module_insts ; // return the map
}

// This routine will create an array of new module items for each module instantiations of
// the map '_module_insts' and return the array.
Array *VeriModuleInliningVisitor::ModuleInlining(const VeriModuleInstantiation *mod_inst, VeriScope *scope) const
{
    if (!mod_inst) return 0 ;

    VeriMapForCopy old2new(POINTER_HASH) ; // map for copy
    Array *child_items = new Array() ; // array to contain new module items

    VeriModule *mod = (VeriModule*)mod_inst->GetInstantiatedModule() ;
    if (!mod) return 0 ;

    Array *ports = mod->GetPorts() ; // get the formal ports

    Array *instances = mod_inst->GetIds() ;
    VeriInstId *inst_id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(instances, i, inst_id) {
        if (!inst_id) continue ;

        // Get the port connections of this instance:
        Array *actual_ports = inst_id->GetPortConnects() ;

        VeriIdDef *formal ;
        VeriExpression *actual ;
        unsigned j ;
        FOREACH_ARRAY_ITEM(actual_ports, j, actual) {
            if (!actual) continue ;

            formal = 0 ;
            if (actual->NamedFormal()) {
                // This is a (SV-style) named formal.
                formal = mod->GetPort(actual->NamedFormal()) ;
            } else {
                // This is a ordered formal
                if (j>=((ports) ? ports->Size() : 0)) {
                    break ; // no use checking more.
                }
                formal = (ports) ? (VeriIdDef*)ports->At(j) : 0 ;
            }
            if (!formal) continue ;

            VeriExpression *actual_connc = actual->GetConnection() ;
            VeriIdDef *actual_id = actual_connc->FullId() ;

            if (!actual_id) { // actual port is an expression: complex or concatenated expression
                // Create new local id and data declaration. After that create a assignment statement
                char *name = VeriModuleInliningVisitor::CreateUniqueName("local_var", scope) ; // create unique name
                VeriVariable *local_id = new VeriVariable(name) ;
                (void) scope->Declare(local_id) ;
                Array *ids = new Array() ;
                ids->InsertLast(local_id) ;

                // Create new data declaration
                int formal_size = formal->StaticSizeSign() ;
                unsigned size = (formal_size < 0) ? (unsigned)(-formal_size) : (unsigned)formal_size ;

                VeriRange *range = 0 ;
                if (size > 1) {
                    // create new range
                    VeriExpression *left = new VeriIntVal((int)(size-1)) ;
                    VeriExpression *right = new VeriIntVal((int)0) ;
                    range = new VeriRange(left, right) ;
                }

                VeriDataType *data_type = new VeriDataType(VERI_WIRE, (formal_size < 0 ) ? VERI_SIGNED : 0, range) ;
                VeriNetDecl *data_decl = new VeriNetDecl(data_type, 0, 0, ids) ;
                child_items->InsertLast(data_decl) ;

                actual_id = local_id ;
                // Create new assignment statement
                VeriIdRef *lval = new VeriIdRef(actual_id) ;
                VeriExpression *rval = (VeriExpression*) actual_connc->CopyExpression(old2new) ;
                VeriNetRegAssign *net_reg_assign = 0 ;
                if (formal->IsInput()) {
                    net_reg_assign = new VeriNetRegAssign(lval, rval) ;
                } else if (formal->IsOutput()) {
                    net_reg_assign = new VeriNetRegAssign(rval, lval) ;
                }
                Array *assmnt_stmt_arr = new Array() ;
                assmnt_stmt_arr->InsertLast(net_reg_assign) ;
                VeriContinuousAssign *assign_stmt = new VeriContinuousAssign(0, 0, assmnt_stmt_arr) ;
                child_items->InsertLast(assign_stmt) ;
            }
            (void) old2new.Insert(formal, actual_id) ; // insert formal id vrs actual id in map for copy id
        }

        // Copy items of the instantiated module
        Array *mod_items = mod->GetModuleItems() ;
        VeriModuleItem *mod_item ;
        FOREACH_ARRAY_ITEM(mod_items, j, mod_item) {
            if (!mod_item) continue ;

            if (mod_item->IsDataDecl()) { // copy declarations
                Array *ids = mod_item->GetIds() ;

                VeriIdDef *new_id = 0 ;
                unsigned k ;
                VeriIdDef *id ;
                FOREACH_ARRAY_ITEM(ids, k, id) {
                    if(!id) continue ;
                    char *name = VeriModuleInliningVisitor::CreateUniqueName(id->GetName(), scope) ; // check and create unique name
                    if (!Strings::compare(name, id->GetName())) {
                        char *tmp = name ;
                        new_id = id->CopyWithName(name, old2new) ;
                        Strings::free(tmp) ;
                    } else {
                        new_id = id->CopyId(old2new, 0) ;
                        Strings::free(name) ;
                    }
                    (void) scope->Declare(new_id) ;
                }
                VeriModuleItem *data_decl = mod_item->CopyModuleItem(old2new) ; // copy declarations
                child_items->InsertLast(data_decl) ;
            } else { // copy other module items
                VeriModuleItem *new_item = (VeriModuleItem*) mod_item->CopyModuleItem(old2new) ;
                if (new_item) {
                    child_items->InsertLast(new_item) ;
                }
            }
        }
    }
    return child_items ; // return array of new module items
}

// Create unique name for variables:
// If find the variable declaration in the present scope or in the  upper scopes,
// create a new name and return the new name:
char* VeriModuleInliningVisitor::CreateUniqueName(const char *prefix, VeriScope *present_scope) const
{
    char *name = Strings::save(prefix) ;
    VeriScope *scope = present_scope ;
    unsigned number = 0 ;
    while(scope && scope->FindLocal(name))
    {
        char *num = Strings::itoa((int)number) ;
        Strings::free(name) ;
        name = Strings::save(prefix, "_", num) ;
        number++ ;
        Strings::free(num) ;
    }
    return name ;
}
