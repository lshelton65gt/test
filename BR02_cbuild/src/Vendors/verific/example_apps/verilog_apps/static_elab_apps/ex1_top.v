

// Property of Electra Design Automation/Verific Design Automation Inc.
// Use, disclosure or distribution is prohibited
// without prior written permission of 
// Electra Design Automation/Verific Design Automation Inc.


// In this testcase the top level module is module 'top'.
// It instantiates module 'child' twice (I and I2) with
// different parameter values. 
// After static elaboration module 'top' will instantiate
// two new modules which are looking similar with module
// 'child' having proper parameter values (as passed from
// the module 'top'). 

module top;
    child #(4) I();
    child #(5) I1();
endmodule

module child;
parameter p = 10;

initial
  $display(p);
endmodule



