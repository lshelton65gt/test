/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_EXAMPLE_VISITOR_H_
#define _VERIFIC_VERI_EXAMPLE_VISITOR_H_

#include "VeriVisitor.h"    // Visitor base class definition

#include "Set.h"
#include "Map.h"
#include "VeriScope.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// The following visitor class is used for the folowwing purpose:
// Check for some conditions for module inlining, and at the same time
// collecting the module instantiations.
/* -------------------------------------------------------------------------- */

class VeriModuleInliningVisitor : public VeriVisitor
{
public:
    VeriModuleInliningVisitor();
    virtual ~VeriModuleInliningVisitor();

    // The following Visit methods need to be redefined for our purpose :

    // The following class definitions can be found in VeriModule.h and in VeriModuleItem.h
    virtual void VERI_VISIT(VeriModule, node);
    virtual void VERI_VISIT(VeriModuleInstantiation, node);

    unsigned    CheckForModuleInlining(const VeriModuleInstantiation *node) ; // Check for module inlining conditions
    Map*        CollectModuleInstances(VeriModule *module) ; // collect module instantiations
    Array*      ModuleInlining(const VeriModuleInstantiation *item, VeriScope *scope) const ; // inlining module
    char*       CreateUniqueName(const char *prefix, VeriScope *present_scope) const ; // create unique name

private:
    Map _module_insts ;          // Container of VeriModuleInstantiation*'s
    Map _already_checked_list ;  // set containg modules that are already visited
    VeriModule *_module ;        // containg module

     // Prevent the compiler from implementing the following
    VeriModuleInliningVisitor(const VeriModuleInliningVisitor &node);
    VeriModuleInliningVisitor& operator=(const VeriModuleInliningVisitor &rhs);
} ;

/* -------------------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_EXAMPLE_VISITOR_H_
