/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Message.h"        // Make message handlers available
#include "veri_file.h"      // Make verilog reader available

#include "VeriModuleInliningVisitor.h"     // Make visitor pattern available

#include "Strings.h"
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive

using namespace std ;

using namespace std ;

// Verific utilities
#include "Array.h"          // Make class Array available
#include "Set.h"            // Make class Set available
#include "Message.h"        // Make message handlers available
#include "Strings.h"        // Definition of class to manipulate copy, concatenate, create etc...

// Verific command line interface
#include "Commands.h"       // Make command line interface available
#include "ComRead.h"        // Using Verific command 'analyze' as template for command line parsing

// Verific Verilog parser
#include "veri_file.h"      // Make verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definition of VeriName
#include "VeriId.h"         // Definitions of all verilog identifier nodes
#include "VeriScope.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
************************* VERILOG STATIC ELABORATION EXAMPLE ****************************
*****************************************************************************************

                                NOTES TO THE READER:

    The following is an example which demonstrates the following :

    How to analyze and statically elaborate Verilog language designs and traverse the
    hierarchy.

    This application takes in a list of Verilog top-level unit preceded by the "-top" option.

           traverse_hierarchy-linux ex2_test.v -top top

    Apart from the required file name arguments, and the -top option, this application
    also accepts the a large number of options that guide design analysis.

    Call the compiled command line application with -help option for a full overview of
    all the options this application accepts and processes.

*****************************************************************************************/

// Routines for traversing the module/entity instances in DFS order and print them.
static void TraverseVerilog(const VeriModule *top) ;

int main(int argc, const char **argv)
{
    // Set-up main line command line.
    // This makes all options on the TCL command "analyze" available on this main command line.
    Command *mainline = new ComAnalyze() ;

    // Add the following options used in this application :
    mainline->Add(new StringArg("top","name of top-level module",0/*not optional*/)) ;

    // Default command line if no arguments given :
    const char *default_argv[5] = {"traverse_hierarchy","ex2_top.v","-top","top",0} ;
    if (argc==1) {
        argc = 4 ;
        argv = default_argv ;
    }

    // Now parse the command line
    if (!mainline->ParseArgs(argc, argv)) {
        mainline->Usage() ;
        delete mainline ;
        return 1 ; /* Command line failed */
    }
    // Check if we need to print help (usage)
    if (mainline->BoolVal("help")) {
        mainline->Usage() ;
        delete mainline ;
        return 0 ; // success. User only wanted 'help' on command line options
    }

    // Same for SystemVerilog
//    veri_file::SetDefaultLibraryPath(lib_path) ;

    // Parse the 'analyze' command :
    if (!mainline->Process(0/*no tcl interpreter needed*/)) {
        mainline->Usage() ;
        delete mainline ;
        return 2 ; // failure in analysis
    }

    // Now get top design name :
    const char *top_name = mainline->StringVal("top") ;
    VERIFIC_ASSERT(top_name) ; // 'top' is not optional, so ParseArgs would already have failed.

    // First get the library (from the 'analyze' command) :
    const char *work_lib = mainline->StringVal("work") ;
    if (!work_lib) work_lib = "work" ; // default library name

    if (veri_file::GetModule(top_name)) {
        // Top level unit is a Verilog module
        // Statically elaborate all the Verilog modules. Return if any error shows up.
        if (!veri_file::ElaborateStatic(top_name)) return 0 ; // statically elaborates all verilog modules in the "work" libarary

        VeriModule *top_module = veri_file::GetModule(top_name) ; // Get the pointer to the top-level module
        if (!top_module) {
            return 0 ; // Exit from application if there is no top module by the given name in the given Verilog designs
        }

        top_module->Info("Start hierarchy traversal here at Verilog top level module '%s'", top_module->Name()) ;
        TraverseVerilog(top_module) ; // Traverse top level module and the hierarchy under it
    }

    return 0 ; // all good
}

// Traverse a module and the hierarchy under it:
static void
TraverseVerilog(const VeriModule *module)
{
    if (!module) return ;
    // Get the scope of the module:
    VeriScope *scope = module->GetScope() ;
    // Find all the declared ids in this scope:
    Map *ids = scope ? scope->DeclArea() : 0 ;
    MapIter mi ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(ids, mi, 0, &id) { // Traverse declared ids
        if (!id || !id->IsInst()) continue ; // Consider only the instance ids
        VeriModuleInstantiation *mod_inst = id->GetModuleInstance() ; // Take the moduleinstance
        VeriModule *mod = mod_inst ? mod_inst->GetInstantiatedModule() : 0 ; // The module instance is a module
        Message::PrintLine("Processing instance : ", id->Name()) ;
        if (mod) { // This is verilog module insatantiation: need to go into that module
            Message::PrintLine("instantiated module : ", mod->Name()) ;
            Message::PrintLine("\n") ;
            TraverseVerilog(mod) ; // Traverse the instantiated module
        }
    }
}
