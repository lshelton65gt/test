module top (input i, output wire o, output wire o1, output wire o2, output wire o3) ;
    bot I(i,o) ;
    bot I1(i+1,o1) ;
    bot1 I2(i,o2) ;
    bot2 I3(i,o3) ;
endmodule


(* module_inlining=1 *) module bot ( input in, output out) ;
    integer i ; 
    assign out = in ; 
endmodule


(* module_inlining=1 *) module bot1 (in, out) ;
    integer i ; 
    reg j ; 
    input in ; 
    output out ; 
    assign out = in ; 
    assign out = in ; 
endmodule


(* module_inlining=1 *) module bot2 ( input in, output out) ;
    integer i ;
    assign out = in ;
endmodule
