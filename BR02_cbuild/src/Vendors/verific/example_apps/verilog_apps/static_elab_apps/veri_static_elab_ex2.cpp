/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VeriCopy.h"       // Make class VeriMapForCopy available
#include "Map.h"            // Make class Map available
#include "Message.h"        // Make message handlers available
#include "veri_file.h"      // Make verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriLibrary.h"
#include "Array.h"          // Make class Array available
#include "VeriId.h"         // Definitions of all verilog identifier nodes
#include "Strings.h"        // Definition of class to manipulate copy, concatenate, create etc...

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
********************** VERILOG STATIC ELABORATION EXAMPLE #2 ****************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is an example which demonstrates the following :

        How to copy a top level module with different names and elaborate those
        separately with different parameters settings.

    This example makes use of class VeriMapForCopy and class Map along with CopyWithName
    function of VeriModule class and ElaborateStatic function of veri_file .
    CopyWithName copies the top level module twice with different names and
    ElaborateStatic function elaborates those with different parameters settings.

    The example can accept the name of a Verilog design to be parsed from the
    user as an optional command line argument without which it works on the
    default design ie. on "ex2_top.v".

*****************************************************************************************/

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    if (argc < 2) Message::PrintLine("Default input file: ex2_top.v. Specify command line argument to override") ;

    const char *file_name = 0 ;
    if (argc>1) {
        file_name = argv[1] ; // Set the file name as specified by the user
    } else {
        file_name = "ex2_top.v" ; // Set default file name
    }

    // |/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\|
    // |             ANALYSIS             |
    // |\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/|

    // Now analyze the top-level design (in verilog2k mode)
    // Return in case of failure.
    if (!veri_reader.Analyze(file_name, 1/*v2k*/)) return 1 ;

    // |/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\|
    // |        STATIC ELABORATION        |
    // |\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/|

    // Get all the top level modules
    Array *all_top_modules = veri_file::GetTopModules("work") ;
    // Get a handle of the first top level module, if any.
    VeriModule *mod = (all_top_modules && all_top_modules->Size()) ? (VeriModule *)all_top_modules->GetFirst() : 0 ;
    delete all_top_modules ; // Free dynamically allocated memory
    if (!mod) {
        // This should never happen for this example
        Message::Msg(VERIFIC_ERROR, 0, 0, "an error occurred with this example") ;
        return 0 ;
    }

    //---------------------------------------
    //  Pretty Print Before Copying module
    //---------------------------------------
    char *before_copy = Strings::save(mod->Name(), "_before_copy.v") ;

    // Pretty-print the design (the entire library)
    veri_reader.PrettyPrint(before_copy, 0, 0) ;
    Strings::free(before_copy) ; // Free dynamically allocated memory no longer in use

    //Now copy of the top level module
    VeriMapForCopy id_map_table(POINTER_HASH) ;

    char *copy_name = Strings::save("copy_", mod->Name()) ;
    VeriModuleItem *new_mod = mod->CopyWithName(copy_name, id_map_table, 1 /* add copied module to library containing 'mod'*/) ;

    // Now creating the Map class to pass new parameter settings for the copied module
    Map *param_map = new Map(STRING_HASH, 1) ;
    unsigned i ;
    VeriIdDef *param_id ;
    Array *param_id_arr = mod->GetParameters() ;
    FOREACH_ARRAY_ITEM(param_id_arr, i, param_id) {
        if (!param_id) continue ;
        char *x = Strings::itoa((int)i) ; // Convert to string
        if (!param_map->Insert(param_id->Name(), x)) Strings::free(x) ;
    }
    // Elaborate the copied module
    // The parameters in the copied module will get the new values which will be propagated
    // hierarchically downwards.
    // Return in case of failure.
    if (!veri_reader.ElaborateStatic(copy_name, "work", param_map)) {
        // free memory
        MapIter mi ;
        char *x ;
        FOREACH_MAP_ITEM(param_map, mi, 0, &x) Strings::free(x) ;
        delete param_map ;
        return 1 ;
    }
    Strings::free(copy_name) ; // Free dynamically allocated memory no longer in use

    //---------------------------------------
    //  Pretty Print After Copying module
    //---------------------------------------
    char *after_copy1 = Strings::save(mod->Name(), "_after_copy1.v") ;

    // Pretty-print the design again, to see the difference between pre
    // static-elaboration and post static-elaboration ...
    veri_reader.PrettyPrint(after_copy1, 0, 0) ;
    // You'll now see that all instantiations have been uniquified.
    Strings::free(after_copy1) ; // Free dynamically allocated memory no longer in use

    // Second set of parameter
    FOREACH_ARRAY_ITEM(param_id_arr, i, param_id) {
        if (!param_id) continue ;
        char *x = Strings::itoa((int)(i*10 + i)) ; // Convert to string
        char *exist = (char *)param_map->GetValue(param_id->Name()) ;
        (void) param_map->Insert(param_id->Name(), x, 1 /* force overwrite */) ;
        Strings::free(exist) ;
    }
    id_map_table.Reset() ;

    char *copy_second_name = Strings::save("copy2_", mod->Name()) ;
    // Make another copy of the top level module
    new_mod = mod->CopyWithName(copy_second_name, id_map_table, 1 /* add copied module in library containing 'mod'*/) ;

    // Elaborate the module copied module
    // The parameters in the copied module will get the new values which will be propagated
    // hierarchically downwards.
    veri_reader.ElaborateStatic(copy_second_name, "work", param_map) ;
    Strings::free(copy_second_name) ; // Free dynamically allocated memory no longer in use

    // Pretty-print the copied design again,
    char *after_copy2 = Strings::save(mod->Name(), "_after_copy2.v") ;
    veri_reader.PrettyPrint(after_copy2, 0, 0) ;

    Strings::free(after_copy2) ; // Free dynamically allocated memory no longer in use

    // free memory
    MapIter mi ;
    char *x ;
    FOREACH_MAP_ITEM(param_map, mi, 0, &x) Strings::free(x) ;
    delete param_map ;

    // All done.  Wasn't that easy!

    return 1 ;
}

