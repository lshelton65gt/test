# Microsoft Developer Studio Project File - Name="veri_static_elab_ex2" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=veri_static_elab_ex2 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "veri_static_elab_ex2.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "veri_static_elab_ex2.mak" CFG="veri_static_elab_ex2 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "veri_static_elab_ex2 - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe
# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "veri_static_elab_ex2___Win32_Debug"
# PROP BASE Intermediate_Dir "veri_static_elab_ex2___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "veri_static_elab_ex2___Win32_Debug"
# PROP Intermediate_Dir "veri_static_elab_ex2___Win32_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "WINDOWS" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od 
# ADD CPP /I "../../../containers"
# ADD CPP /I "../../../verilog"
# ADD CPP /I "../../../vhdl"
# ADD CPP /I "../../../vhdl_sort"
# ADD CPP /I "../../../util"
# ADD CPP /D "WIN32" /D "WINDOWS" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 tcl84.lib /libpath:"../../../tclmain" 
# ADD LINK32 containers.lib /libpath:"../../../containers" /libpath:"../../../containers/Debug" /libpath:"../../../containers/Release"
# ADD LINK32 verilog.lib /libpath:"../../../verilog" /libpath:"../../../verilog/Debug" /libpath:"../../../verilog/Release"
# ADD LINK32 vhdl.lib /libpath:"../../../vhdl" /libpath:"../../../vhdl/Debug" /libpath:"../../../vhdl/Release"
# ADD LINK32 vhdl_sort.lib /libpath:"../../../vhdl_sort" /libpath:"../../../vhdl_sort/Debug" /libpath:"../../../vhdl_sort/Release"
# ADD LINK32 util.lib /libpath:"../../../util" /libpath:"../../../util/Debug" /libpath:"../../../util/Release"
# ADD LINK32 /nologo /debug /machine:IX86 /pdbtype:sept

# SUBTRACT LINK32 /pdb:none
# Begin Target

# Name "veri_static_elab_ex2 - Win32 Debug"
# Begin Source File

SOURCE=.\veri_static_elab_ex2.cpp
# End Source File
# End Target
# End Project
