module test (clk, data, out) ;
    input clk ;
    input [3:0] data ;
    output reg [3:0] out ;

    always@(posedge clk)
        out = data ;

endmodule

