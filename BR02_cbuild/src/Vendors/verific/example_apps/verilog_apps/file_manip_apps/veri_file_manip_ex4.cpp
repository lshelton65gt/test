/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

// containers used :
#include "Array.h" // a dynamic array
#include "Set.h"   // a simple hash table
#include "Map.h"   // a hash table class

// utilities used :
#include "Strings.h"        // Definition of class to easily create/concatenate char*s.
#include "Message.h"        // Message handler
#include "TextBasedDesignMod.h"  // Text-Based Design-Modification (TextBasedDesignMod) utility

// Verilog parser, main interface :
#include "veri_file.h"      // Make Verilog reader available
#include "VeriVisitor.h"    // Make the parse-tree visitor pattern available

// Verilog parse tree API :
#include "VeriModule.h"     // Definition of a VeriModule
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriId.h"         // Definitions of all identifier definition tree nodes

#include "veri_tokens.h"    // Definition of port direction VERI_OUTPUT, etc ...

/*
    BUILD NOTE!!! : This file needs to be compiled with the same verilog compile switches,
                    that the verilog lib was built with, otherwise strange behavior may occur!
*/

/****************************************************************************************
******************************* SCAN INSERT VERILOG EXAMPLE  ****************************
*****************************************************************************************

                                APPLICATION NOTE:

    The following is an example to demonstrate parse tree manipulation utility using
    user friendly APIs. The name of the design file to be modified is taken from command
    line argument.

    The example will show the following operations on analyzed verilog design.

        0) Parse the input file.
        1) Find the top-level module.
        2) Get the port-connections, we need to know whether it is ANSI or non-ANSI.
        3) Add 'si' (scan-in), 'so' (scan-out), 'sc' (scan-control) ports to the module.
           in the appropriate form of ANSI/non-ANSI. Add port declarations if it is non-ANSI.
        4) Find the clocked always statements in the module
        5) For a clocked always statement, find the 'synchronous' statement in the always statement
           (the one executed under the clock edge)
        6) Collect all the target (names) assigned under this synchronous statement. There will be the registers.
        7) Insert the scan controled if statement, the original clocked statement may be a
           sequential block or it may be a single statement, re-use the block if it is so.
        8) Create the 'shifted' assignments to all the target names (registers).
           Start with 'si' (scan-in) and end with 'so' (scan-out).
        9) Insert the original clocked statement string after 'else'
       10) Write the (modified) design to an output file.

    If file 'ex4_top1.v' and 'ex4_top2.v' is given in command line argument, file 'ex4_top1.v.out.v' and 'ex4_top2.out.v' will be printed in step 10.

*****************************************************************************************/

// First, define visitor patterns which we will use :

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// A visitor patteren, which will find the event control statements in a parse tree :
// VeriVisitor is a general purpose Verific Verilog parse tree traversal pattern.
// It is finetuned here to collect event control statements inside always constructs
// (skip initial blocks) :
class EventControlVisitor : public VeriVisitor
{
public :
    EventControlVisitor() : VeriVisitor(), _event_control_stmts() { } ;
    ~EventControlVisitor() {} ;

    virtual void Visit(VeriInitialConstruct &node) {
        node.Info("skipping this initial block") ;
    }

    // Collect event control statements :
    virtual void Visit(VeriEventControlStatement &node) {
        node.Info("visiting this event control statement") ;
        _event_control_stmts.InsertLast(&node) ;
    }

public :
    Array _event_control_stmts ; // array of VeriEventControlStatement *'s, collected by this visitor
} ;

// A visitor patteren, which will find the assignments in a parse tree
// Collect all target and values from Blocking and Non-Blocking assignments :
class AssignmentVisitor : public VeriVisitor
{
public :
    AssignmentVisitor() : VeriVisitor(), _assignments(POINTER_HASH) { } ;
    ~AssignmentVisitor() {} ;

    // Collect blocking assignments :
    virtual void Visit(VeriBlockingAssign &node) {
        // Get the assigned target (expression) :
        VeriExpression *target = node.GetLVal() ;
        // Get the assigned value (expression) :
        VeriExpression *value = node.GetValue() ;
        // Insert the target and the value into the Map
        if (target && value) _assignments.Insert(target, value) ;
    }
    // Collect non-blocking assignments :
    virtual void Visit(VeriNonBlockingAssign &node) {
        // Get the assigned target (expression) :
        VeriExpression *target = node.GetLVal() ;
        // Get the assigned value (expression) :
        VeriExpression *value = node.GetValue() ;
        // Insert the target and the value into the Map
        if (target && value) _assignments.Insert(target, value) ;
    }

public :
    Map _assignments ; // Map of VeriExpression *'s hashed by VeriExpression *'s, collected by this visitor
} ;

#ifdef VERIFIC_NAMESPACE
} ; // end definitions in verific namespace
#endif

/*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
using namespace Verific ; // start using Verific namespace
#endif

int main(int argc, char **argv)
{
#ifndef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    #pragma message("This application example requires VERIFIC_LINEFILE_INCLUDES_COLUMNS to be enabled in order to run correctly.")
    #pragma message("Please enable VERIFIC_LINEFILE_INCLUDES_COLUMNS.")
    Message::PrintLine("This application example requires the compile-flag VERIFIC_LINEFILE_INCLUDES_COLUMNS (located in util/VerificSystem.h) to be active in order to run!") ;
#else
   if (argc < 2) Message::PrintLine("Default input file: ex4_top2.v. Specify command line argument to override") ;

     // Get the file name to work on. If not specified as command line arg use default file
     // We recommend that you use ex4_top1.v and ex4_top2.v for this example.
    const char *file_name = (argc > 1) ? argv[1] : "ex4_top2.v" ;

    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    // Now analyze the above example Verilog file : (Recommended files, ex4_top1.v and ex4_top2.v)
    // If there is any error, don't proceed further
    if (!veri_reader.Analyze(file_name)) return 1 ;

    // Instantiate text based design modification utility
    TextBasedDesignMod tbdm(0) ; // Can modify all files

    // Step (1) : Pick up the top-level module from the design just analyzed :
    VeriModule *top = veri_reader.GetModule(veri_reader.TopModule()) ;
    if (!top) {
        Message::Msg(VERIFIC_ERROR, 0, 0, "no top level design found") ;
        return 2 ;
    }

    // Step (2) : Get the port-connects of this module we want to check
    // whether the ports were declared in ANSI style on in non-ANSI style.
    Array *ports = top->GetPortConnects() ;
    if (!ports || !ports->Size()) {
        top->Error("no port found on the top level design") ;
        return 3 ;
    }

    // Get the last declared port, we will insert 3 ports after this one:
    const VeriExpression *last_port = (const VeriExpression *)ports->GetLast() ;
    if (!last_port) {
        top->Error("last port of the top level design is not a valid port") ;
        return 4 ;
    }

    // Step (3) : Add 'si' (scan-in), 'so' (scan-out), 'sc' (scan-control) ports to the module.
    if (last_port->IsAnsiPortDecl()) {
        // It is an ANSI style port declaration, just add the 3 ANSI port declaration after it.
        // But don't add it after this port-connects, we may add them to a wrong place. When
        // ANSI ports are declared as 'input a, b', we store the linefile of 'input a' for the
        // whole declaration, so we will insert it after the last port id:
        // Get the last port id via VeriModule::GetPorts() routine
        Array *port_ids = top->GetPorts() ;
        VeriIdDef *last_port_id = (port_ids) ? (VeriIdDef *)port_ids->GetLast() : 0 ;
        if (!last_port_id) {
            top->Error("last port of the top level design is not a valid port") ;
            return 5 ;
        }
        // Insert the ports after the last port id:
        tbdm.InsertAfter(last_port_id->EndingLinefile(), ", input si, output reg so, input sc") ;
    } else {
        // It is a non-ANSI style port declaration, add the 3 non-ANSI port after it:
        tbdm.InsertAfter(last_port->EndingLinefile(), ", si, so, sc") ;

        // We have to add the declaration of these ports in the module,
        // so, get the modules items from the top module:
        Array *items = top->GetModuleItems() ;
        // Get the first module item from the module items Array:
        VeriModuleItem *first_item = (items && items->Size()) ? (VeriModuleItem*)items->GetFirst() : 0 ;
        if (!first_item) {
            top->Error("cannot find any module item in this module") ;
            return 6 ;
        }

        // Insert the port declarations before the first module item:
        linefile_type first_item_lf = first_item->StartingLinefile() ;
        tbdm.InsertBefore(first_item_lf, "input si ;\noutput so ;\ninput sc ;\nreg so ;\n") ;
    }

    // Step (4) : Find the clocked always statements in the module
    // We use a visitor pattern for that.
    // Collect all the 'event_control_statements' :
    // This will collect the statements appearing immediately inside a always construct : "always @(...) stmt" :
    EventControlVisitor event_control_visitor ;
    top->Accept( event_control_visitor ) ; // traverse and collect..

    // Check each event control statement found in the design :
    VeriEventControlStatement *event_control_stmt ;
    unsigned i, j ;
    FOREACH_ARRAY_ITEM(&(event_control_visitor._event_control_stmts), i, event_control_stmt) {
        if (!event_control_stmt) continue ; // null-pointer check

        // Check if this event control statement is clocked :
        // Look at the sensitivity list (the "@()" clause), and check if it has 'edge' expressions :
        Array *sens_list = event_control_stmt->GetAt() ;
        unsigned is_clocked = 0 ;
        VeriExpression *event_expr ;
        FOREACH_ARRAY_ITEM(sens_list, j, event_expr) {
            if (!event_expr) break ; // invalid sensitivity list
            if (event_expr->IsEdge(0/*any edge (pos or neg)*/)) {
                is_clocked = 1 ;
                break ; // no need to check the other sensitivity list items. This statement is clocked !
            }
        }
        if (!is_clocked) continue ;

        // issue a message :
        event_control_stmt->Info("found one clocked event control statement here") ;

        // Step (5) : For a clocked always statement, find the 'synchronous' statement in the always statement

        // If there is only one edge in the sensitivity list, the whole statement is clocked
        VeriStatement *clocked_stmt = event_control_stmt->GetStmt() ;

        // Check if the actual clocked statement is nested under asynchronous if-statements :
        unsigned nesting = 1 ;
        VeriConditionalStatement *if_stmt = 0 ;
        while (sens_list->Size() > nesting) {
            if (!clocked_stmt) break ; // invalid, or non-existing clocked statement

            // Otherwise, there are a-synchronouos (re)set edges, so we need to traverse to find the 'else'
            // part of the (N-1)th nested if statement inside this statement.

            // Can do this with a visitor, or otherwise, but for explanation purposes,
            // Do this the 'quick-and-dirty' way, using GetClassId and static casts to traverse into the statement :

            // There should be an 'if' statement here, or else this is not a synthesizable event control statement.
            // But, there could be a 'sequential' block statement (begin/end) with one if statement in there :
            if (clocked_stmt->GetClassId() == ID_VERISEQBLOCK) {
                VeriSeqBlock *block = (VeriSeqBlock*)clocked_stmt ;
                Array *stmts = block->GetStatements() ;
                // Find the if statement in here :
                VeriStatement *tmp_stmt ;
                FOREACH_ARRAY_ITEM(stmts, j, tmp_stmt) {
                    if (!tmp_stmt) continue ;
                    if (tmp_stmt->GetClassId() == ID_VERICONDITIONALSTATEMENT) {
                        clocked_stmt = tmp_stmt ;
                        break ;
                    }
                }
                // Now drop through to the if-statement processing :
            }

            if (clocked_stmt->GetClassId() == ID_VERICONDITIONALSTATEMENT) {
                if_stmt = (VeriConditionalStatement*)clocked_stmt ;

                // Set the clocked statement to be the 'else' part of the if-statement :
                clocked_stmt = if_stmt->GetElseStmt() ;
            } else {
                clocked_stmt->Error("cannot find clocked statement in this always construct") ;
                clocked_stmt = 0 ;
            }

            /* else : run out of 'if' statements. Most be complicated edge conditions */
            nesting++ ;
        }
        if (!clocked_stmt) continue ; // invalid, or nonexisting clocked statement

        // Step (6) : Collect all the target (names) assigned under this synchronous statement.

        // Use a second pattern for this :
        AssignmentVisitor target_id_collector ;
        clocked_stmt->Accept(target_id_collector) ;
        // Now, all synchronously assigned identifiers are in Set "target_id_collector._assignments".

        // Get the string of the clocked statement (this must be freed), we will use it later:
        char *clocked_stmt_str = tbdm.GetText(clocked_stmt->StartingLinefile(), clocked_stmt->EndingLinefile(), 1 /*including modifications*/) ;

        // Step (7) : Insert the scan controled if statement.
        // First check whether it already has a begin...end block, we will re-use it:
        if (clocked_stmt->GetClassId() == ID_VERISEQBLOCK) {
            // Its a begin...end block, just insert the if condition:
            tbdm.InsertBefore(clocked_stmt->StartingLinefile(), "if (sc)\n") ;
            VeriSeqBlock *block = (VeriSeqBlock*)clocked_stmt ;
            Array *stmts = block->GetStatements() ;
            VeriStatement *first_stmt = (stmts) ? (VeriStatement*)stmts->GetFirst() : 0 ;
            // Insert the "so = si" line before the first statement of the begin...end block
            tbdm.InsertBefore((first_stmt) ? first_stmt->StartingLinefile() : clocked_stmt->StartingLinefile(), "so = si ;\n") ;
        } else {
            // It only has a single statement which is not a begin...end block, so insert a begin...end block also:
            tbdm.InsertBefore(clocked_stmt->StartingLinefile(), "if (sc)\nbegin\n    so = si ;\n    ") ;
        }

        // Step (8) : Create the 'shifted' assignments to all the target names (registers).

        MapIter miter ; // a Map iterator
        VeriExpression *target ;
        VeriExpression *value ;

        // Then, for each assigned identifier, create an blocking assignment so that a 'scan-shifter' occurs :
        // Create an assignment of the form : " {so, <id>} = {<id>, so} ;"
        FOREACH_MAP_ITEM(&(target_id_collector._assignments), miter, &target, &value) {
            // Message :
            VeriIdDef *assigned_id = target->GetId() ;
            if (assigned_id) {
                clocked_stmt->Info("identifier %s is assigned under this clocked statement", assigned_id->Name()) ;
                assigned_id->Info("%s is declared here", assigned_id->Name()) ;
            }

            // Insert the appropriate text before/after the target value of the assignemnts:
            tbdm.InsertBefore(target->StartingLinefile(), "{so, ") ;
            tbdm.InsertAfter(target->EndingLinefile(), "}") ;
            tbdm.InsertBefore(value->StartingLinefile(), "{") ;
            tbdm.InsertAfter(value->EndingLinefile(), ", so}") ;
        }

        // Step (9) : Now insert the original clocked statement string after 'else'
        tbdm.InsertAfter(clocked_stmt->EndingLinefile(), clocked_stmt_str, 0 /* don't indent, it is aleady indented! */) ;
        Strings::free(clocked_stmt_str) ;
        if (clocked_stmt->GetClassId() == ID_VERISEQBLOCK) {
            // For a begin...end block, just insert the 'else':
            tbdm.InsertAfter(clocked_stmt->EndingLinefile(), "\nelse\n") ;
        } else {
            // Otherwise, we have inserted a 'begin', now insert the matching 'end' and then insert 'else':
            tbdm.InsertAfter(clocked_stmt->EndingLinefile(), "\nend\nelse\n    ") ;
        }
    }

    // Step (10) : Write the (modified) design to an output file.
    char *out_file_name = Strings::save(file_name, ".out.v") ;
    Message::PrintLine("Writing the design to file ", out_file_name) ;
    tbdm.WriteFile(file_name, out_file_name) ;
    Strings::free(out_file_name) ;

#endif // #ifndef VERIFIC_LINEFILE_INCLUDES_COLUMNS

    return 0 ; // Leave with success status
}

/* ------------------------------------------------------- */

