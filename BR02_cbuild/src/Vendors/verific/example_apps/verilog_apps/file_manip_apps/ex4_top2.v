module test (input clk, en, input [3:0] data, output reg [3:0] out1, out2) ;

    always@(posedge clk)
    begin
        out1 = data ;
        if (en) out2 = ~data ;
    end

endmodule

