/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/
#include "VerificSystem.h"

#include "Array.h"          // Make class Array available
#include "Strings.h"        // Make class Strings available

#include "Message.h"        // Make message handlers available

#include "TextBasedDesignMod.h"  // Text-Based Design-Modification (TextBasedDesignMod) utility

#include "veri_file.h"      // Make verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Make VeriCaseStatement class available

#include "VeriVisitor.h"    // For visitor patterns

/****************************************************************************************

                                NOTE TO THE READER:

    The following is a more advanced example to demonstrate file manipulation utility
    using APIs of 'TextBasedDesignMod' class. For a simple example, please see the other
    example application.

    * We will not change the parse tree itself, but will write out modified verilog
      of the source file using the file manipulation utility.

    * We do the following:
        1. Add a comment in the file in a specific location.
        2. Remove a particular construct from the first top level module.

    * The name of the design file and the output file are taken from the command line
      argument along with the secure directory path. If no arguments are specified,
      we default to hard-coded values.

*****************************************************************************************/

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif
class TextBasedDesignMod ;

// Visit class to visit all case statements of a module
class VeriCaseStmtVisit : public VeriVisitor
{
public:
    VeriCaseStmtVisit(TextBasedDesignMod *file_obj) ;
    virtual ~VeriCaseStmtVisit() ;

    // Overwrite only the visit routine of case statement
    virtual void Visit(VeriCaseStatement &node) ;

private:
    TextBasedDesignMod *_tbdm ;
} ;

// Visit class to visit assignment so that it can be converted
// to gate instantiation
class VeriAssignReplace : public VeriVisitor
{
public:
    VeriAssignReplace(TextBasedDesignMod *file_obj, VeriModule *mod) ;
    virtual ~VeriAssignReplace() ;

    // Overwrite only the visit routine of VeriContinuousAssign
    virtual void Visit(VeriContinuousAssign &node) ;

private:
    TextBasedDesignMod *_tbdm ;
    VeriModule   *_mod ;
} ;

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

////////////////////////////////////////////////////////////////////////////
////////////              VeriCaseStmtVisit class               ////////////
////////////////////////////////////////////////////////////////////////////

VeriCaseStmtVisit::VeriCaseStmtVisit(TextBasedDesignMod *file_obj) :
    VeriVisitor(),
    _tbdm(file_obj)
{
}

VeriCaseStmtVisit::~VeriCaseStmtVisit()
{
}

void VeriCaseStmtVisit::Visit(VeriCaseStatement &node)
{
    // Get the starting location of the case statement. We will use this information
    // to put comment before case statement
    linefile_type start_location = node.StartingLinefile() ;

    // Call InsertBefore routine of 'TextBasedDesignMod' class to insert comment before case statement
    _tbdm->InsertBefore(start_location, "// synopsys parallel_case\n") ;
}

////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
#ifndef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    #pragma message("This application example requires VERIFIC_LINEFILE_INCLUDES_COLUMNS to be enabled in order to run correctly.")
    #pragma message("Please enable VERIFIC_LINEFILE_INCLUDES_COLUMNS.")
    Message::PrintLine("This application example requires the compile-flag VERIFIC_LINEFILE_INCLUDES_COLUMNS (located in util/VerificSystem.h) to be active in order to run!") ;
#else
    if (argc < 2) Message::PrintLine("Default input file: example.v, default output file: ex2_out.v, default secure directory: src. Specify command line argument to override") ;

    // The following operations will be done by this application on the file:
    // 1 . File specified in the first command line argument will be analyzed.
    // 2 . Handle for file modification utility (TextBasedDesignMod) will be created.
    // 3 . Using linefile information from analyzed parse tree, file modification utility
    //     will modify the design file (not the parse tree). We perform these modifications:
    //       (a). Add a comment attribute before the case statements to make them parallel.
    //       (b). Remove the last module item from the first top level module.
    // 4 . Modified file will be written in another file that can be specified as the second
    //     command line argument, if not specified, the original file is overwritten!

    // Get the file name to work on
    const char *file_name = (argc > 1) ? argv[1] : "example.v" ;

    // Analyze the design file(in verilog2k mode), if there is any error, don't proceed further!
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veri_file::Analyze(file_name, 1/*v2k*/)) return 1 ;

    // Instantiate the class 'TextBasedDesignMod', which is file modification utility.
    // It takes the directory path in which we can modify the files. If no directory path
    // is specified, all files can be modified and overwritten. The directory 'src' is
    // specified as the secure directory here. So we can modify and overwrite the files only
    // under this directory tree. Any other file outside of that tree can be modified but
    // cannot be overwritten!
    TextBasedDesignMod file_base_comment((argc > 3) ? argv[3] : "src") ;

    // We will perform the manipulations as said before on the first top level module of the specified file:

    // Get all the top level modules
    Array *all_top_modules = veri_file::GetTopModules() ;
    // Get a handle of the first top level module, if any.
    VeriModule *mod = (all_top_modules && all_top_modules->Size()) ? (VeriModule *)all_top_modules->GetFirst() : 0 ;
    if (!mod) {
        Message::PrintLine("Cannot find a top level module") ;
        delete all_top_modules ;
        return 1 ;
    }
    delete all_top_modules ;

    /* /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ *
     *              1.  Make case statements parallel                     *
     * \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/ */

    // Instantiate visit class object which makes case statement parallel. This visitor class
    // is written to visit every case statement of the module. Please look into
    // VeriCaseStmtVisit::Visit(VeriCaseStatement &node) function in this file.
    VeriCaseStmtVisit visit_obj(&file_base_comment) ;
    mod->Accept(visit_obj) ;

    /* /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ *
     *                 2. Remove the last module item                     *
     * \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/ */

    // We want to remove a specific module item from the module. To remove it we should
    // know the starting location and ending location of this item in the design file.
    // We will remove the last module item.

    // Get the module item list of module.
    Array *items = mod->GetModuleItems() ;

    // Get the last module item, it is the item we want to remove
    VeriModuleItem *mod_item = (items && items->Size()) ? (VeriModuleItem*)items->GetLast() : 0 ;
    if (mod_item) {
        // Get the starting location and ending location of this module item.
        linefile_type start_linefile = mod_item->StartingLinefile() ;
        linefile_type end_linefile = mod_item->EndingLinefile() ;

        // Now we want to remove the section occupied by the last module item
        // from the file. We know the starting location and ending location of
        // this item. Call 'Replace' routine of 'TextBasedDesignMod' utility for this.
        file_base_comment.Replace(start_linefile, end_linefile, 0 /* no replace, only remove */) ;
    } else {
        Message::PrintLine("No module item found in module ", mod->Name()) ;
    }

    /* /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ *
     *                Write modified source file to a file                *
     * \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/ */

    const char *out_file_name = (argc > 2) ? argv[2] : "ex2_out.v" ;
    Message::PrintLine("Writing the design to file ", out_file_name) ;

    // Write the modified file
    file_base_comment.WriteFile(file_name, out_file_name) ;

    // Remove all analyzed modules
    veri_file::RemoveAllModules() ;

#endif // #ifndef VERIFIC_LINEFILE_INCLUDES_COLUMNS

    return 0 ; // Status OK
}

