/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/
#include "VerificSystem.h"

#include "Array.h"          // Make class Array available
#include "Strings.h"        // Make class Strings available

#include "Message.h"        // Make message handlers available

#include "TextBasedDesignMod.h"  // Text-Based Design-Modification (TextBasedDesignMod) utility

#include "veri_file.h"      // Make verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************

                                NOTE TO THE READER:

    The following is an advanced example to demonstrate file manipulation utility using
    APIs of 'TextBasedDesignMod' class. For a simple example, please see other applications.

    * We will not change the parse tree itself, but will write out modified verilog
      of the source file using the file manipulation utility.

    * We only add a port in the port list at the last position (after all the ports)
      of all the top level modules. If it is a non-ANSI port declaration then we also
      add its declaration in the module item.

    * The name of the design files are taken from the command line arguments. Depending on
      the secure directory specified when instantiating the 'TextBasedDesignMod' class, either
      a file will be overwritten or will be left unchanged with an warning if they do not
      belong to the secure directory. If no command line arguments are specified, we
      default to hard-coded values.

*****************************************************************************************/

int main(int argc, char **argv)
{
#ifndef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    #pragma message("This application example requires VERIFIC_LINEFILE_INCLUDES_COLUMNS to be enabled in order to run correctly.")
    #pragma message("Please enable VERIFIC_LINEFILE_INCLUDES_COLUMNS.")
    Message::PrintLine("This application example requires the compile-flag VERIFIC_LINEFILE_INCLUDES_COLUMNS (located in util/VerificSystem.h) to be active in order to run!") ;
#else
    if (argc < 2) Message::PrintLine("Default input file: example.v, default secure directory: src. Specify command line argument to override") ;

    // The following operations will be done by this application on the file:
    // 1 . File specified in the command line arguments will be analyzed.
    // 2 . Handle for file modification utility (TextBasedDesignMod) will be created.
    // 3 . Using linefile information from analyzed parse tree, file modification utility
    //     will modify the design file (not the parse tree). We find all the top level
    //     modules and insert a port at the end of its port list, if any.
    // 4 . Modified file will be overwritten on the source file if they belongs to the
    //     secure directory specified (here it is "src"). Otherwise, there will be an
    //     warning message and the file will be left unchanged!

    // Analyze the design file(in verilog2k mode), if there is any error, don't proceed further!
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veri_file::Analyze(((argc > 1) ? argv[1] : "example.v"), 1/*v2k*/)) return 1 ;

    // Instantiate the class 'TextBasedDesignMod', which is the file modification utility.
    // It takes the directory path in which we can modify the files. If no directory path
    // is specified, all files can be modified and overwritten. The directory 'src' is
    // specified as the secure directory here. So we can modify and overwrite the files only
    // under this directory tree. Any other file outside of that tree can be modified but
    // cannot be overwritten!
    TextBasedDesignMod tbdm((argc > 2) ? argv[2] : "src") ;

    // We will only insert a port 'new_port' at the end of the port list of all the top
    // level modules. So, get all the top level modules.

    // Get all the top level modules
    Array *all_top_modules = veri_file::GetTopModules() ;
    if (!all_top_modules || !all_top_modules->Size()) {
        Message::PrintLine("Cannot find a top level module") ;
        delete all_top_modules ;
        return 1 ;
    }

    // Now iterate over all the top level modules and insert the port
    unsigned j ;
    VeriModule *mod ;
    FOREACH_ARRAY_ITEM(all_top_modules, j, mod) {
        if (!mod) continue ;

        /* /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ *
         *             1.   Insert port 'new_port' in port list             *
         * \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/ */

        // Get the port connection array of this top level module.
        Array *port_connects = mod->GetPortConnects() ;

        // Get the port reference after which new port ref is to be inserted. We want to insert a new port
        // after all the existing ports, so we need ending location of the last port of the module.
        VeriExpression *last_port = (port_connects && port_connects->Size()) ? (VeriExpression*)port_connects->GetLast() : 0 ;
        if (!last_port) {
            Message::PrintLine("Cannot find any port of the top level module '", mod->Name(), "'") ;
            continue ;
        }

        // Now we have to insert required text for new port addition after specific location in file.
        // As we want to add text after all the existing ports, we will use the linefile of the last
        // port and call 'InsertAfter' routine of 'TextBasedDesignMod' with that linefile.

        // Get the ending line file information of the last port, so that we can enter text after
        // that location in file
        linefile_type ending_linefile = last_port->EndingLinefile() ;

        // Here we are inserting a port reference after a specific port, so we have to insert a ','
        // before the new port name. Please note that we don't check whether a port with this name
        // (new_port) is already there in the module or not.
        if (last_port->IsAnsiPortDecl()) {
            // It's an ANSI port decl, so just insert another ANSI port
            tbdm.InsertAfter(ending_linefile, ", input new_port /* new port inserted */");
        } else {
            // Its a non-ANSI port decl, so insert the decl both in port list and module item
            tbdm.InsertAfter(ending_linefile, ", new_port /* new port inserted */");

            // To insert input declaration in file as the first module item we have to get handle
            // to module items and location of starting point of existing first module item.

            // Get the module item list of the module
            Array *items = mod->GetModuleItems() ;

            // We will use the line file information of first module item as we want to add new port
            // declaration before it. Get the first module item before which new text is to be inserted.
            VeriModuleItem *first_item = (items && items->Size()) ? (VeriModuleItem*)items->GetFirst() : 0 ;
            if (!first_item) {
                Message::PrintLine("Cannot find any module item in ", mod->Name()) ;
                continue ;
            }

            // Now we have to add string representation of 'port declaration of new_port'.

            // Insert the string representation of input declaration in the file based class
            // passing the starting linefile of original first module item. From parser linefile
            // information of any parse tree node does not include comments around any construct.
            // So for the following 'InsertBefore' call text 'input new_port ;' will be inserted
            // just before other port declarations line (which is, probably, the first module item)
            // not before the comment attached with first module item, if any.

            // Get the starting location of first module item
            linefile_type starting_linefile = first_item->StartingLinefile() ;
            // Call InsertBefore of 'TextBasedDesignMod' utility
            tbdm.InsertBefore(starting_linefile, "input new_port ;\n");
        }
    }
    // Delete the array
    delete all_top_modules ;

    /* /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ *
     *                   Overwrite the modified sources                   *
     * \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/ */

    Message::PrintLine("Overwriting the design files") ;

    // Overwrite all the modified files: if they do not belong to the secure diretory this
    // will not overwrite the file, instead will generate an warning message, otherwise it
    // will overwrite the file as usual.
    tbdm.WriteDesign() ;

    // Remove all analyzed modules
    veri_file::RemoveAllModules() ;

#endif // #ifndef VERIFIC_LINEFILE_INCLUDES_COLUMNS

    return 0 ; // Status OK
}

