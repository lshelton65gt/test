/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/
#include "VerificSystem.h"

#include "Array.h"          // Make class Array available
#include "Strings.h"        // Make class Strings available

#include "Message.h"        // Make message handlers available

#include "TextBasedDesignMod.h"  // Text-Based Design-Modification (TextBasedDesignMod) utility

#include "veri_file.h"      // Make verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************

                                NOTE TO THE READER:

    The following is a simple example to demonstrate file manipulation utility using
    APIs of 'DesignModDir' class. For a more complex example, please see the other
    example applications.

    * We will not change the parse tree itself, but will write out modified verilog
      of the source file using the file manipulation utility.

    * We only add a port in the port list at the last position (after all the ports)
      of the first top level module. If it is a non-ANSI port declaration then we also
      add its declaration in the module item.

    * The name of the design file is taken from the command line argument. Also the name
      of the output file can be specified in the command line argument. If not specified
      we overwrite the source file if certain conditions are met. If no arguments are
      specified, we default to hard-coded values.

*****************************************************************************************/

int main(int argc, char **argv)
{
#ifndef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    #pragma message("This application example requires VERIFIC_LINEFILE_INCLUDES_COLUMNS to be enabled in order to run correctly.")
    #pragma message("Please enable VERIFIC_LINEFILE_INCLUDES_COLUMNS.")
    Message::PrintLine("This application example requires the compile-flag VERIFIC_LINEFILE_INCLUDES_COLUMNS (located in util/VerificSystem.h) to be active in order to run!") ;
#else
    if (argc < 2) Message::PrintLine("Default input file: example.v, default output file: ex1_out.v. Specify command line argument to override") ;

    // The following operations will be done by this application on the file:
    // 1 . File specified in the first command line argument will be analyzed.
    // 2 . Handle for file modification utility (DesignModDir) will be created.
    // 3 . Using linefile information from analyzed parse tree, file modification utility
    //     will modify the design file (not the parse tree). We find the first top level
    //     module and insert a port at the end of its port list, if any.
    // 4 . Modified file will be written in another file that can be specified as the second
    //     command line argument, if not specified, the original file is overwritten!

    // Get the file name to work on
    const char *file_name = (argc > 1) ? argv[1] : "example.v" ;

    // Analyze the design file(in verilog2k mode), if there is any error, don't proceed further!
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veri_file::Analyze(file_name, 1/*v2k*/)) return 1 ;

    // Instantiate the class 'TextBasedDesignMod', which is the Text-Based Design Modificaiton utility.
    // It takes the directory path in which we can modify the files. If no directory path
    // is specified, all files can be modified and overwritten.
    TextBasedDesignMod tbdm(0) ;

    // We will only insert a port 'new_port' at the end of the port list of the first
    // top level module. So, get the first top level module.

    // Get all the top level modules
    Array *all_top_modules = veri_file::GetTopModules() ;
    // Get a handle of the first top level module, if any.
    VeriModule *mod = (all_top_modules && all_top_modules->Size()) ? (VeriModule *)all_top_modules->GetFirst() : 0 ;
    if (!mod) {
        Message::PrintLine("Cannot find a top level module") ;
        delete all_top_modules ;
        return 1 ;
    }
    delete all_top_modules ;

    /* /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ *
     *             1.   Insert port 'new_port' in port list             *
     * \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/ */

    // Get the port connection array of the first top level module.
    Array *port_connects = mod->GetPortConnects() ;

    // Get the port reference after which new port ref is to be inserted. We want to insert a new port
    // after all the existing ports, so we need ending location of the last port of the module.
    VeriExpression *last_port = (port_connects && port_connects->Size()) ? (VeriExpression*)port_connects->GetLast() : 0 ;
    if (!last_port) {
        Message::PrintLine("Cannot find any port of the first top level module '", mod->Name(), "'") ;
        return 1 ;
    }

    // Now we have to insert required text for new port addition after specific location in file.
    // As we want to add text after all the existing ports, we will use the linefile of the last
    // port and call 'InsertAfter' routine of 'DesignModDir' with that linefile.

    // Get the ending line file information of the last port, so that we can enter text after
    // that location in file, which is the right place of insertion of another port.
    linefile_type ending_linefile = last_port->EndingLinefile() ;

    // Here we are inserting a port reference after a specific port, so we have to insert a ','
    // before the new port name. Please note that we don't check whether a port with this name
    // (new_port) is already there in the module or not.
    if (last_port->IsAnsiPortDecl()) {
        // It's an ANSI port declaration, so just insert another ANSI port
        tbdm.InsertAfter(ending_linefile, ", input new_port /* new port inserted */");
    } else {
        // Its a non-ANSI port declaration, so insert the declaration both in port list and module item
        tbdm.InsertAfter(ending_linefile, ", new_port /* new port inserted */");

        // To insert input declaration in file as the first module item we have to get handle
        // to module items and location of starting point of existing first module item.

        // Get the module item list of the module
        Array *items = mod->GetModuleItems() ;

        // We will use the line file information of first module item as we want to add new port
        // declaration before it. Get the first module item before which new text is to be inserted.
        VeriModuleItem *first_item = (items && items->Size()) ? (VeriModuleItem*)items->GetFirst() : 0 ;
        if (!first_item) {
            Message::PrintLine("Cannot find any module item in this module") ;
            return 1 ;
        }

        // Now we have to add string representation of 'port declaration of new_port'.

        // Insert the string representation of input declaration in the file based class
        // passing the starting linefile of original first module item. From parser linefile
        // information of any parse tree node does not include comments around any construct.
        // So for the following 'InsertBefore' call text 'input new_port ;' will be inserted
        // just before other port declarations line (which is, probably, the first module item)
        // not before the comment attached with first module item, if any.

        // Get the starting location of first module item
        linefile_type starting_linefile = first_item->StartingLinefile() ;
        // Call InsertBefore of 'DesignModDir' utility
        tbdm.InsertBefore(starting_linefile, "input new_port ;\n");
    }

    /* /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ *
     *                Write modified source file to a file                *
     * \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/ */

    // Get the output file name, it may be specified in the command line.
    const char *out_file_name = (argc > 2) ? argv[2] : "ex1_out.v" ;
    Message::PrintLine("Writing the design to file ", out_file_name) ;

    // Write the modified file
    tbdm.WriteFile(file_name, out_file_name) ;

    // Remove all analyzed modules
    veri_file::RemoveAllModules() ;

#endif // #ifndef VERIFIC_LINEFILE_INCLUDES_COLUMNS

    return 0 ; // Status OK
}

