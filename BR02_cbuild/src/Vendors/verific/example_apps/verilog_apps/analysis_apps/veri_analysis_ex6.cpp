/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Ex6Visitor.h"     // Make visitor base class available

#include "Map.h"            // Make associated hash table class Map available
#include "Strings.h"        // Definition of string class

#include "Message.h"        // Make message handlers available

#include "veri_file.h"      // Make verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriConstVal.h"   // Definitions of VeriIntVal
#include "VeriId.h"         // Definitions of all verilog identifier tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "VeriLibrary.h"    // Definition of VeriLibrary class
#include "VeriScope.h"      // Definition of VeriScope class

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
******************************* VERILOG EXAMPLE #6 **************************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is an example that demonstrates the following :

        ** How to get the sensitivity list of an event control statement with an implicit
           event expression list i.e a statement of the form :
                           @(*) <statement>
    This example makes use of a visitor class, which is a well-known SW design
    pattern for traversing a data-structure. The base verilog visitor class
    is located in the VeriVisitor.* files, and this base class already defines
    the default traversing algorithm to walk a verilog parse-tree.  Some newly
    derived classes viz. AlwaysVisitor and UsedIdVisitor have been created to
    modify the data structure so that the sensitivity list for implicit event
    expressions gets popoulated.

    This application can be run with an optional argument specifying a file name, without
    which it is going to work on the default file ie. "sensitivity.v".

    Limitation :
    This application will produce illegal verilog output for input designs having memori(es).
    For example the application will produce illegal verilog for module described below.
                   module test ;
                       parameter size = 8 ;
                       int a[0 : size-1] ;
                       int s ;
                       always @(*)
                            s = a[i] ;
                   enmodule

*****************************************************************************************/

/* -------------------------------------------------------------------------- */
int main (int argc, char **argv)
{
    // Create a place-holder for the verilog reader
    veri_file veri_reader ;

    char *file_nm ; // Stores the file name

    // If no file name is supplied then take the default file name
    switch (argc) {
    // That is if the user does not specify any option
    case 1: file_nm = "ex6_sensitivity.v" ; break ; // Sets the file name to the default name
    case 2: file_nm = argv[1] ; break ; // Sets file name to name supplied by the user
    default: Message::Error(0, "Too many options.") ; return 1 ;
    }

    // |/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\|
    // |             ANALYSIS             |
    // |\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/|

    // Now analyze the Verilog file (into the work library). In case of any error do not process further.
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veri_reader.Analyze(file_nm, 1)) return 1 ;

    MapIter mi, mi1 ;
    VeriLibrary *lib ;
    // This section of the code iterates through all libraries in the parse-tree
    // and elaborates all implicit event expression lists.
    FOREACH_VERILOG_LIBRARY(mi, lib) {
        VeriModule *module ;
        FOREACH_VERILOG_MODULE_IN_LIBRARY(lib, mi1, module) { // Iterate through all the modules of library 'lib'
            // This section of the code looks for implicit event expression
            // lists in the module 'module' and elaborates them.
            if (!module) continue ;

            // Use a visitor pattern to catch the 'always' construct and then
            // do relevant processing.
            AlwaysVisitor al_visitor ;

            // Traverse parse-tree and convert @(*) style event control
            module->Accept(al_visitor) ;
        }
    }
    veri_reader.PrettyPrint("converted_output.v", 0) ;

    return 1 ;
}
/*--------------------------------------------------------------*/
