/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>         // strcpy
#include <iostream>         // cout
using std::cout ;

#include "veri_file.h"      // Make Verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "Message.h"        // Make message handlers available, not used in this example
#include "Array.h"          // Make dynamic array class Array available

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
******************************* VERILOG EXAMPLE #3 **************************************
*****************************************************************************************

                                NOTES:

    As a continuation from example #2, the following example demonstrates a way
    to pretty_print a verilog parse-tree while traversing it.  This is done
    via polymorphism.  It is much cleaner than the methods implemented in
    examples #1 and #2.

    A pure virtual method "void PrettyPrint(...) const = 0 ;" was declared in
    the VeriNode class ("VeriTreeNode.h").  This forces any new classes to
    implement their version of this method.

    All verilog tree node pretty print code has been copied to this file for easy
    viewing, but you won't be able to step into the code during debug,
    since the libs you received do not contain debug information.

    Because polymorphism is being used, GetClassId() is no longer needed.  Once again
    GetClassId() is Verific's RTTI mechanism.  We implemented this method instead
    of using ANSI's C++ RTTI mechanism, because we felt their mechanism is too
    expensive in terms of performance.

    This application can be run with an optional argument specifying a file name, without
    which it is going to work on the default file ie. "ex3_reg.v".

    Examples #4 will show better ways how to traverse a parse-tree using
    a Visitor design pattern.

    Unfortunately with the default example, you won't be able to step through the debugger.

*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veriReader ;   // Read the intro in veri_file.h to see the details of this object.

    // The design "ex3_reg.v" is to be read in only if the user doesn't supply any other file name:

    char *file_nm ; // Stores the name of the file to be analyzed

    // If no file name is supplied then take the default file name which is "ex3_reg.v"
    switch (argc) {
    case 1: file_nm = "ex3_reg.v" ; break ; // Sets the file name to the default name
    case 2: file_nm = argv[1] ; break ; // Sets file name to name supplied by the user
    default: Message::Error(0, "Too many options.") ; return 1 ;
    }

    // Now analyze the Verilog file (into the work library). In case of any error do not process further.
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veriReader.Analyze(file_nm, 1)) return 1 ;

    VeriModule *pModule ; // Stores the name of the module to be worked with

    // Get the list of top modules
    Array *top_mod_array = veriReader.GetTopModules() ;
    if (!top_mod_array) {
        // If there is no top level module then issue error
        Message::Error(0,"Cannot find any top level module. Check for recursive instantiation") ;
        return 1 ;
    }
    // Get a handle (by name) to the module that was just analyzed provided the name is known.
    // To do so uncomment the following line.
    // VeriModule *module = veriReader.GetModule("<Insert the name of the concerned module>");

    pModule = (VeriModule *) top_mod_array->GetFirst() ; // Get the first top level module
    // Comment the above line if the module is being found out by searching its name
    delete top_mod_array ; top_mod_array = 0 ; // Cleanup, it is not required anymore

    VeriIdDef *module_id = pModule->Id() ;
    if(!module_id) {
        Message::Error(0, "module should always have an identifier") ;
    }

    pModule->PrettyPrint(cout, 0) ;

    return 1 ;
}

/* ------------------------------------------------------- */
/* --------------- Pretty Print Functionality ------------ */
/* ------------------------------------------------------- */

#if 0    /* The following code is for viewing only.  If you try to compile it in, you will
            receive "multiple definitions" link errors.
          */

/************************** VeriModule Pretty Print ********************************/

void VeriModule::PrettyPrint(ostream &f, unsigned level) const {
    /* Start myself on a new line */
    f << endl ;

    // Print the predefined directives :
    if (_default_nettype && _default_nettype!=VERI_WIRE) f << "`default_nettype " << PrintToken(_default_nettype) << endl ;
    if (_is_celldefine) f << "`celldefine" << endl ;
    if (_unconnected_drive) f << "`unconnected_drive " << PrintToken(_unconnected_drive) << endl ;
    if (_timescale) f << "`timescale " << _timescale << endl ;

    /* Print the indent */
    f << PrintLevel(level) ;

    /* Keyword */
    if (_id->IsUdp()) {
        f << "primitive " ;
    } else {
        f << "module " ;
    }

    /* module name */
    _id->PrettyPrint(f, level) ; f << " " ;

    unsigned i ;

    /* list of parameters */
    if (_parameter_connects) {
        f << "#(" ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(_port_connects, i, item) {
            if (i) f << ", " ;
            // FIX ME : this prints old-style param decls. Not ANSI-complient ones.
            if (item) item->PrettyPrint(f,level+1) ;
        }
        f << ") " ;
    }

    /* list of ports */
    if (_port_connects) {
        f << "(" ;
        VeriExpression *pc ;
        FOREACH_ARRAY_ITEM(_port_connects, i, pc) {
            if (i) f << ", " ;
            if (pc) pc->PrettyPrint(f,level+1) ;
        }
        f << ")" ;
    }
    f << " ;" << endl ;

    /* VeriModule Items */
    VeriModuleItem *mi ;
    FOREACH_ARRAY_ITEM(_module_items, i, mi) {
        /* Write them with one level indent deeper */
        if (mi) mi->PrettyPrint(f,level+1) ;
    }

    /* Close module */
    f << PrintLevel(level) ;
    f << "endmodule" << endl << endl ;

    /* close flag-directives if we can : */
    if (_is_celldefine) f << "`endcelldefine" << endl ;
    if (_unconnected_drive) f << "`nounconnected_drive" << endl ;

    f << endl ; // extra linefeed.
}

/************************** VeriIdDef Pretty Print ********************************/

void VeriIdDef::PrettyPrint(ostream &f, unsigned /*level*/) const {
    // NOTE: The following function VeriNode::PrintIdentifier is now the primary
    // way to pretty print identifiers.  This function does a hierarchical check
    // and escaped identifier check, and prints accordingly.
    PrintIdentifier(f, Name()) ;
}
void VeriVariable::PrettyPrint(ostream &f, unsigned level) const {
    // Print the id name with optional initial value and dimensions */
    VeriIdDef::PrettyPrint(f, level) ; // Print _name
    if (_dimensions) {
        f << " " ;
        _dimensions->PrettyPrint(f,level) ;
    }
    if (_initial_value) {
        f << " " << PrintToken(VERI_EQUAL) << " " ;
        _initial_value->PrettyPrint(f, level) ;
    }
}

void VeriParamId::PrettyPrint(ostream &f, unsigned level) const {
    // Print the id name with initial value */
    VeriIdDef::PrettyPrint(f, level) ; // Print _name
    if (_initial_value) {
        f << " " << PrintToken(VERI_EQUAL) << " " ;
        _initial_value->PrettyPrint(f, level) ;
    }
}

/************************** VeriExpression Pretty Print ********************************/

void VeriIdRef::PrettyPrint(ostream &f, unsigned /*level*/) const {
    PrintIdentifier(f, (_name) ? _name : _id->Name()) ;
}
void VeriIndexedId::PrettyPrint(ostream &f, unsigned level) const {
    if (_prefix) _prefix->PrettyPrint(f,level) ;
    f << "[" ;
    _idx->PrettyPrint(f,level) ;
    f << "]" ;
}
void VeriSelectedName::PrettyPrint(ostream &f, unsigned level) const {
    if (_prefix) _prefix->PrettyPrint(f,level) ;
    f << "." << _suffix ;
}
void VeriIndexedMemoryId::PrettyPrint(ostream &f, unsigned level) const {
    if (_prefix) _prefix->PrettyPrint(f,level) ;
    unsigned i ;
    VeriExpression *index ;
    FOREACH_ARRAY_ITEM(_indexes, i, index) {
        f << "[" ;
        index->PrettyPrint(f, level) ;
        f << "]" ;
    }
}
void VeriConcat::PrettyPrint(ostream &f, unsigned level) const {
    f << "{" ;
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (i) f << "," ;
        if (!expr) continue ;
        expr->PrettyPrint(f,level) ;
    }
    f << "}" ;
}
void VeriMultiConcat::PrettyPrint(ostream &f, unsigned level) const {
    f << "{" ;
    _repeat->PrettyPrint(f,level) ;
    f << "{" ;
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (i) f << "," ;
        if (!expr) continue ;
        if (expr) expr->PrettyPrint(f,level) ;
    }
    f << "}}" ;
}
void VeriFunctionCall::PrettyPrint(ostream &f, unsigned level) const {
    if (_func_name) _func_name->PrettyPrint(f,level) ;
    if (_args) {
        f << "(" ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(_args, i, expr) {
            if (i) f << "," ;
            if (!expr) continue ;
            expr->PrettyPrint(f,level) ;
        }
        f << ")" ;
    }
}
void VeriSystemFunctionCall::PrettyPrint(ostream &f, unsigned level) const {
    // System function call names can't be escaped or hierarchical, so don't call VeriNode::PrintIdentifier.
    f << "$" << _name ;
    if (_args) {
        f << "(" ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(_args, i, expr) {
            if (i) f << "," ;
            if (!expr) continue ;
            expr->PrettyPrint(f,level) ;
        }
        f << ")" ;
    }
}
void VeriMinTypMaxExpr::PrettyPrint(ostream &f, unsigned level) const {
    f << "(" ;
    if (_min_expr) _min_expr->PrettyPrint(f,level) ;
    f << ":" ;
    if (_typ_expr) _typ_expr->PrettyPrint(f,level) ;
    f << ":" ;
    if (_max_expr) _max_expr->PrettyPrint(f,level) ;
    f << ")" ;
}
void VeriUnaryOperator::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintToken(_oper_type) ;
    /* Parenthesize the result to get precedence right */
    f << "(" ;
    if (_arg) _arg->PrettyPrint(f,level) ;
    f << ")" ;
}
void VeriBinaryOperator::PrettyPrint(ostream &f, unsigned level) const {
    /* Parenthesize the result to get precedence right */
    f << "(" ;
    if (_left) _left->PrettyPrint(f,level) ;
    f << " " << PrintToken(_oper_type) << " " ;
    if (_right) _right->PrettyPrint(f,level) ;
    f << ")" ;
}
void VeriQuestionColon::PrettyPrint(ostream &f, unsigned level) const {
    /* Parenthesize the result to get precedence right */
    f << "(" ;
    if (_if_expr) _if_expr->PrettyPrint(f,level) ;
    f << " ? " ;
    if (_then_expr) _then_expr->PrettyPrint(f,level) ;
    f << " : " ;
    if (_else_expr) _else_expr->PrettyPrint(f,level) ;
    f << ")" ;
}
void VeriEventExpression::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintToken(_edge) << " " ;
    if (_expr) _expr->PrettyPrint(f,level) ;
}
void VeriPortConnect::PrettyPrint(ostream &f, unsigned level) const {
    if (_port_ref_name) {
        f << "." ; PrintIdentifier(f, _port_ref_name) ; f << "(" ;
    }
    if (_connection) {
        _connection->PrettyPrint(f,level) ;
    }
    // named connection always has ()
    if (_port_ref_name) f << ")" ;
}
void VeriPortOpen::PrettyPrint(ostream & /*f*/, unsigned /*level*/) const {
    // Verilog prints nothing for an open port
}

void VeriAnsiPortDecl::PrettyPrint(ostream &f, unsigned level) const {
    // Start on a newline :
    f << endl ;
    f << PrintLevel(level) ;
    f << PrintToken(_dir) << " " ;
    if (_io_type) f << PrintToken(_io_type) << " " ;
    if (_signed_type) f << PrintToken(_signed_type) << " " ;
    if (_range) _range->PrettyPrint(f,level) ;
    unsigned i ;
    unsigned first = 1 ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!first) f << ", " ;
        id->PrettyPrint(f, level) ;
    }
}

void VeriTimingCheckEvent::PrettyPrint(ostream &f, unsigned level) const {
    if (_edge) {
        f << PrintToken(_edge) << " " ;
        if (_edge_desc_str) {
            // Print edge control specifiers
            if (_edge_desc_str) f << _edge_desc_str[0] << _edge_desc_str[1] ;
            if (_edge_desc_str[2]) f << "," << _edge_desc_str[2] << _edge_desc_str[3] ;
            f << " " ;
        }
    }
    if (_terminal_desc) _terminal_desc->PrettyPrint(f, 0) ;

    if (_condition) {
        f << " " << PrintToken(VERI_EDGE_AMPERSAND) << " " ;
        _condition->PrettyPrint(f, 0) ;
    }
}

/********************** VeriStatement Pretty Print *************************/

void VeriBlockingAssign::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    /* target */
    if (_lval) _lval->PrettyPrint(f,level) ;
    f << " " << PrintToken(VERI_EQUAL) << " " ;

    /* options */
    if (_control) {
        _control->PrettyPrint(f,level) ;
        f << " " ;
    }

    /* value */
    if (_val) _val->PrettyPrint(f,level) ;

    f << " ;" << endl ;
}
void VeriBlockingAssign::PrettyPrintWOSemi(ostream &f, unsigned level) const {
    // This routine should only be called by VeriFor::PrettyPrint.
    // We don't want a ";" and endl to be printed.

    f << PrintLevel(level) ;

    /* target */
    if (_lval) _lval->PrettyPrint(f,level) ;
    f << " " << PrintToken(VERI_EQUAL) << " " ;

    /* options */
    if (_control) {
        _control->PrettyPrint(f,level) ;
        f << " " ;
    }

    /* value */
    if (_val) _val->PrettyPrint(f,level) ;
}
void VeriNonBlockingAssign::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    /* target */
    if (_lval) _lval->PrettyPrint(f,level) ;
    f << " " << PrintToken(VERI_LEQ) << " " ;

    /* options */
    if (_control) {
        _control->PrettyPrint(f,level) ;
        f << " " ;
    }

    /* separate options and value */
    f << " ";

    /* value */
    if (_val) _val->PrettyPrint(f,level) ;
    f << " ;" << endl ;
}
void VeriGenVarAssign::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    /* target */
    PrintIdentifier(f, (_name) ? _name : _id->Name()) ;
    f << " " << PrintToken(VERI_EQUAL) << " " ;

    /* value */
    if (_value) _value->PrettyPrint(f,level) ;
    f << " ;" << endl ;
}
void VeriAssign::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_ASSIGN) << " " ;
    if (_assign) _assign->PrettyPrint(f,level) ;
    f << " ;" << endl ;
}
void VeriDeAssign::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_DEASSIGN) << " " ;
    if (_lval) _lval->PrettyPrint(f,level) ;
    f << " ;" << endl ;
}
void VeriForce::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_FORCE) << " " ;
    if (_assign) _assign->PrettyPrint(f,level) ;
    f << " ;" << endl ;
}
void VeriRelease::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_RELEASE) << " " ;
    if (_lval) _lval->PrettyPrint(f,level) ;
    f << " ;" << endl ;
}
void VeriTaskEnable::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    if (_task_name) _task_name->PrettyPrint(f,level) ;
    f << " " ;

    if (_args) {
        f << "(" ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(_args, i, expr) {
            if (i) f << "," ;
            if (!expr) continue ;
            expr->PrettyPrint(f,level) ;
        }
        f << ")" ;
    }
    f << " ;" << endl ;
}
void VeriSystemTaskEnable::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    // PrintIdentifier is not called here, since system call names can't be escaped.or hierarchical
    f << "$" << _name << " " ;

    if (_args) {
        f << "(" ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(_args, i, expr) {
            if (i) f << "," ;
            if (!expr) continue ;
            expr->PrettyPrint(f,level) ;
        }
        f << ")" ;
    }
    f << " ;" << endl ;
}
void VeriDelayControlStatement::PrettyPrint(ostream &f, unsigned level) const {
    if (_delay) {
        f << PrintLevel(level) ;
        /* Its one expression. Not a normal delay structure */
        f << "#" ;
        _delay->PrettyPrint(f,level) ;
        f << " ";
    }
    if (_stmt) _stmt->PrettyPrint(f,level) ;
    else f << ";" << endl;
}
void VeriEventControlStatement::PrettyPrint(ostream &f, unsigned level) const {
    if (_at) {
        f << PrintLevel(level) ;
        f << "@(" ;

        /* 'or' separated list of expressions */
        if (!_at || _at->Size() == 0) {
            // Implicit event_expression list
            f << "*" ;
        } else {
            unsigned i ;
            VeriExpression *event_expr ;
            FOREACH_ARRAY_ITEM(_at, i, event_expr) {
                if (i) f << " or " ;
                if (event_expr) event_expr->PrettyPrint(f,level) ;
            }
        }
        f << ")" ;
        if (_stmt) f << endl ;
    }

    if (_stmt) _stmt->PrettyPrint(f,level) ;
    else f << ";" << endl;
}
void VeriConditionalStatement::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    f << PrintToken(VERI_IF) << " (" ;
    if (_if_expr) _if_expr->PrettyPrint(f,level) ;
    f << ") " << endl ; /* statements start on a new line */
    if (_then_stmt) _then_stmt->PrettyPrint(f,level+1) ;

    if (_else_stmt) {
        f << PrintLevel(level) << "else" << endl ;
        _else_stmt->PrettyPrint(f,level+1) ;
    }
}
void VeriCaseStatement::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    f << PrintToken(_case_style) << " (" ;
    if (_cond) _cond->PrettyPrint(f,level) ;
    f << ")" << endl ;

    /* VeriCaseItems has a prettyprint of its own, even though its an Array and not a VeriTreeNode */
    if (_case_items) _case_items->PrettyPrint(f,level+1) ;

    f << PrintLevel(level) ;
    f << PrintToken(VERI_ENDCASE) << endl ;
}
void VeriForever::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    f << PrintToken(VERI_FOREVER) << endl ;
    if (_stmt) _stmt->PrettyPrint(f, level+1) ;
}
void VeriRepeat::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    f << PrintToken(VERI_REPEAT) << " (" ;
    if (_cond) _cond->PrettyPrint(f,level) ;
    f << ")" << endl ;
    if (_stmt) _stmt->PrettyPrint(f, level+1) ;
}
void VeriWhile::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    f << PrintToken(VERI_WHILE) << " (" ;
    if (_cond) _cond->PrettyPrint(f,level) ;
    f << ")" << endl ;
    if (_stmt) _stmt->PrettyPrint(f, level+1) ;
}
void VeriFor::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    f << PrintToken(VERI_FOR) << " (" ;

    // NOTE: Both _initial and _rep will always be VeriBlockingAssign's!

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_initials, i, item) {
        // comma-separated list of assignments..
        if (i) f << ", " ;
        // Must call this special pretty print function so that semicolons and
        // endl's don't get printed.
        if (item) item->PrettyPrintWOSemi(f,0) ;
    }
    f << " ; " ;
    if (_cond) _cond->PrettyPrint(f, 0) ;
    f << " ; " ;
    FOREACH_ARRAY_ITEM(_reps, i, item) {
        // comma-separated list of assignments..
        if (i) f << ", " ;
        // Must call this special pretty print function so that semicolons and
        // endl's don't get printed.
        if (item) item->PrettyPrintWOSemi(f,0) ;
    }
    f << ")" << endl ;

    if (_stmt) _stmt->PrettyPrint(f, level+1) ;
}
void VeriWait::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    f << PrintToken(VERI_WAIT) << " (" ;
    if (_cond) _cond->PrettyPrint(f,level) ;
    f << ") " ;
    if (_stmt) _stmt->PrettyPrint(f, level+1) ;
    else f << ";" << endl;
}
void VeriDisable::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_DISABLE) << " " ;
    if (_task_block_name) _task_block_name->PrettyPrint(f,level) ;
    f << " ;" << endl ;
}
void VeriEventTrigger::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_RIGHTARROW) << " " ;
    if (_event_name) _event_name->PrettyPrint(f,level) ;
    f << " ;" << endl ;
}
void VeriSeqBlock::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_BEGIN) ;

    if (_label) {
        f << ": " ;
        _label->PrettyPrint(f, level) ;
    }
    f << endl ;
    f << PrintLevel(level) ;

    unsigned i ;
    VeriModuleItem *decl ;
    FOREACH_ARRAY_ITEM(_decl_items, i, decl) {
        if (decl) decl->PrettyPrint(f,level) ;
    }

    unsigned i ;
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        if (stmt) stmt->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) ;
    f << PrintToken(VERI_END) << endl ;
}
void VeriParBlock::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_FORK) ;

    if (_label) {
        f << ": " ;
        _label->PrettyPrint(f, level) ;
    }
    f << endl ;
    f << PrintLevel(level) ;

    unsigned i ;
    VeriModuleItem *decl ;
    FOREACH_ARRAY_ITEM(_decl_items, i, decl) {
        if (decl) decl->PrettyPrint(f,level) ;
    }

    unsigned i ;
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        if (stmt) stmt->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) ;
    f << PrintToken(VERI_JOIN) << endl ;
}

/********************** VeriModuleItem Pretty Print *************************/

void VeriParamDecl::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_PARAMETER) << " " ;

    if (_param_type) f << PrintToken(_param_type) << " " ;
    if (_signed_type) f << PrintToken(_signed_type) << " " ;
    if (_range) {
        _range->PrettyPrint(f,level) ;
        f << " " ;
    }

    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (i) {
            f << "," << endl ;
            f << PrintLevel(level+1) ;
        }
        id->PrettyPrint(f,level) ;
    }
    f << " ; " << endl ;
}
void VeriLocalParamDecl::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_LOCALPARAM) << " " ;

    if (_param_type) f << PrintToken(_param_type) << " " ;
    if (_signed_type) f << PrintToken(_signed_type) << " " ;
    if (_range) {
        _range->PrettyPrint(f,level) ;
        f << " " ;
    }

    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (i) {
            f << "," << endl ;
            f << PrintLevel(level+1) ;
        }
        id->PrettyPrint(f,level) ;
    }
    f << " ; " << endl ;
}
void VeriIODecl::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    f << PrintToken(_dir) << " " ;
    if (_io_type) f << PrintToken(_io_type) << " " ;
    if (_signed_type) f << PrintToken(_signed_type) << " " ;

    if (_range) _range->PrettyPrint(f,level) ;
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (i) f << ", " ;
        id->PrettyPrint(f, level) ;
    }
    f << " ; " << endl ;
}
void VeriRegDecl::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(_reg_type) << " " ;

    if (_signed_type) f << PrintToken(_signed_type) ;
    if (_range) _range->PrettyPrint(f,level) ;

    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (i) f << ", " ;
        id->PrettyPrint(f, level) ;
    }
    f << " ; " << endl ;
}
void VeriNetDecl::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(_net_type) << " " ;

    if (_signed_type) f << PrintToken(_signed_type) ;
    if (_strength) _strength->PrettyPrint(f, level) ;
    if (_range) _range->PrettyPrint(f,level) ;

    unsigned i ;
    if (_delay) {
        f << "#(" ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(_delay, i, expr) {
            if (i) f << "," ;
            if (expr) expr->PrettyPrint(f,level) ;
        }
        f << ")" ;
    }

    VeriTreeNode *net_decl ;
    FOREACH_ARRAY_ITEM(_ids, i, net_decl) {
        if (i) f << ", " ;
        net_decl->PrettyPrint(f,level) ;
    }
    f << " ; " << endl ;
}
void VeriGenVarDecl::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_GENVAR) << " " ;

    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (i) f << ", " ;
        id->PrettyPrint(f, level) ;
    }
    f << " ; " << endl ;
}
void VeriFunctionDecl::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_FUNCTION) << " " ;

    if (_range) _range->PrettyPrint(f,level) ;
    if (_type) f << PrintToken(_type) << " " ;

    _func->PrettyPrint(f, level) ;
    f << " ; " << endl ;

    // print the declarations
    unsigned i ;
    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) {
        if (decl_item) decl_item->PrettyPrint(f,level+1) ;
    }

    // and the statements
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        if (stmt) stmt->PrettyPrint(f,level+1) ;
    }

    // if there were no statements, print a noopt (a semicolon) to print legal Verilog syntax
    if (!_stmts) f << PrintLevel(level+1) << "; // NOOP" << endl ;

    f << PrintLevel(level) << PrintToken(VERI_ENDFUNCTION) << endl ;
}
void VeriTaskDecl::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_TASK) << " " ;

    _task->PrettyPrint(f, level) ;
    f << " ; " << endl ;

    // print the declarations
    unsigned i ;
    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) {
        decl_item->PrettyPrint(f,level+1) ;
    }

    // and the statements
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        if (stmt) stmt->PrettyPrint(f,level+1) ;
    }

    // if there were no statements, print a noopt (a semicolon) to print legal Verilog syntax
    if (!_stmts) f << PrintLevel(level+1) << "; // NOOP" << endl ;

    f << PrintLevel(level) << PrintToken(VERI_ENDTASK) << endl ;
}
void VeriDefParam::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_DEFPARAM) << " " ;

    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (i) {
            f << "," << endl ;
            f << PrintLevel(level+1) ;
        }
        id->PrettyPrint(f,level+1) ;
    }
    f << " ; " << endl ;
}
void VeriContinuousAssign::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_ASSIGN) << " " ;

    if (_strength) _strength->PrettyPrint(f,level) ;

    unsigned i ;
    if (_delay) {
        f << "#(" ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(_delay, i, expr) {
            if (i) f << "," ;
            if (expr) expr->PrettyPrint(f,level) ;
        }
        f << ")" ;
    }

    VeriNetRegAssign *assign ;
    FOREACH_ARRAY_ITEM(_net_assigns, i, assign) {
        if (i) {
            f << "," << endl ;
            f << PrintLevel(level+1) ;
        }
        assign->PrettyPrint(f,level+1) ;
    }

    f << " ; " << endl ;
}
void VeriGateInstantiation::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(_inst_type) << " " ;

    if (_strength) _strength->PrettyPrint(f,level) ;

    unsigned i ;
    if (_delay) {
        f << "#(" ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(_delay, i, expr) {
            if (i) f << "," ;
            if (expr) expr->PrettyPrint(f,level) ;
        }
        f << ")" ;
    }

    VeriInstId *inst ;
    FOREACH_ARRAY_ITEM(_insts, i, inst) {
        if (i) {
            f << "," << endl ;
            f << PrintLevel(level+1) ;
        }
        if (inst) inst->PrettyPrint(f,level+1) ;
    }

    f << " ; " << endl ;
}
void VeriModuleInstantiation::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    PrintIdentifier(f, _module_name) ;
    f << " " ;

    if (_strength) _strength->PrettyPrint(f,level) ;

    unsigned i ;
    if (_param_values) {
        f << "#(" ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(_param_values, i, expr) {
            if (i) f << "," ;
            if (expr) expr->PrettyPrint(f,level) ;
        }
        f << ")" ;
    }

    VeriInstId *inst ;
    FOREACH_ARRAY_ITEM(_insts, i, inst) {
        if (i) {
            f << "," << endl ;
            f << PrintLevel(level+1) ;
        }
        if (inst) inst->PrettyPrint(f,level+1) ;
    }
    f << " ; " << endl ;
}
void VeriSpecifyBlock::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_SPECIFY) << endl ;

    VeriModuleItem *mi ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_specify_items, i, mi) {
        mi->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << PrintToken(VERI_ENDSPECIFY) << endl ;
}
void VeriSpecParamDecl::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_SPECPARAM) << " " ;

    if (_range) _range->PrettyPrint(f,level) ;

    f << endl << PrintLevel(level+1) ;

    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (i) {
            f << "," << endl ;
            f << PrintLevel(level+1) ;
        }
        id->PrettyPrint(f,level+1) ;
    }
    f << " ; " << endl ;
}
void VeriPathDecl::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    if (_cond) {    // state-dependent path
        f << "if (";
        _cond->PrettyPrint(f, level) ;
        f << ") " ;
    } else if (_ifnone) {   // ifnone statement
        f << PrintToken(VERI_IFNONE) << " " ;
    }

    // Print path declaration
    if (_path) _path->PrettyPrint(f, level) ;

    f << PrintToken(VERI_EQUAL) << " (" ;

    // Print delays
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_delays, i, expr) {
        if (i) f << "," ;
        expr->PrettyPrint(f,level+1) ;
    }

    f << ") ; " << endl ;
}
void VeriSystemTimingCheck::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    // System task names can't be escaped or hierarchical, so don't call VeriNode::PrintIdentifier here.
    f << "$" << _task_name << " " ;

    if (_args) {
        f << "(" ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(_args, i, expr) {
            if (i) f << "," ;
            if (!expr) continue ;
            expr->PrettyPrint(f,level) ;
        }
        f << ")" ;
    }
    f << " ; " << endl ;
}
void VeriInitialConstruct::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_INITIAL) << endl ;
    if (_stmt) _stmt->PrettyPrint(f,level+1) ;
}
void VeriAlwaysConstruct::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_ALWAYS) << endl ;
    if (_stmt) _stmt->PrettyPrint(f,level+1) ;
}
void VeriGenerateConstruct::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_GENERATE) << endl ;
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << PrintToken(VERI_ENDGENERATE) << endl ;
}
void VeriGenerateConditional::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    f << PrintToken(VERI_IF) << " (" ;
    _if_expr->PrettyPrint(f,level) ;
    f << ") " << endl ; /* statements start on a new line */
    if (_then_item) _then_item->PrettyPrint(f,level+1) ;

    if (_else_item) {
        f << PrintLevel(level) << "else" << endl ;
        _else_item->PrettyPrint(f,level+1) ;
    }
}
void VeriGenerateCase::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    f << PrintToken(_case_style) << " (" ;
    if (_cond) _cond->PrettyPrint(f,level) ;
    f << ")" << endl ;

    /* VeriCaseItems has a prettyprint of its own, even though its an Array and not a VeriTreeNode */
    if (_case_items) _case_items->PrettyPrint(f,level+1) ;

    f << PrintLevel(level) ;
    f << PrintToken(VERI_ENDCASE) << endl ;
}
void VeriGenerateFor::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;

    // Print 'for' items on one line : FIX ME : printing too many ';'s.
    f << PrintToken(VERI_FOR) << " (" ;
    if (_initial) _initial->PrettyPrint(f,0) ;
    f << ";" ;
    if (_cond) _cond->PrettyPrint(f,0) ;
    f << ";" ;
    if (_rep) _rep->PrettyPrint(f,0) ;
    f << ")" << endl ;

    // Print the generate block :
    f << PrintLevel(level) ;
    f << PrintToken(VERI_BEGIN) ;

    if (_block_id) {
        f << " : " ; _block_id->PrettyPrint(f, level) ;
    }
    f << endl ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) ;
    f << PrintToken(VERI_END) << endl ;
}
void VeriGenerateBlock::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_BEGIN) ;

    if (_block_id) {
        f << " : " ; _block_id->PrettyPrint(f, level) ;
    }
    f << endl ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) ;
    f << PrintToken(VERI_END) << endl ;
}
void VeriTable::PrettyPrint(ostream &f, unsigned level) const {
    f << PrintLevel(level) ;
    f << PrintToken(VERI_TABLE) << endl ;

    unsigned i ;
    const char *entry ;
    FOREACH_ARRAY_ITEM(_table_entries, i, entry) {
        f << PrintLevel(level) ;
        f << entry << " ;" << endl ;
    }

    f << PrintLevel(level) << PrintToken(VERI_ENDTABLE) << endl ;
}

/********************** Other(Misc) VeriTreeNode Pretty Print *************************/

void VeriRange::PrettyPrint(ostream &f, unsigned level) const {
    f << "[" ;
    if (_left) _left->PrettyPrint(f,level) ;
    if (!_part_select_token) f << ":" ; // Verilog 95
    if (_part_select_token==VERI_PARTSELECT_UP) f << " +: " ; // Verilog 2000
    if (_part_select_token==VERI_PARTSELECT_DOWN) f << " -: " ; // Verilog 2000
    if (_right) _right->PrettyPrint(f,level) ;
    f << "]" ;

    // Also, print subsequent dimensions (if there) :
    if (_next) _next->PrettyPrint(f,level) ;
}
void VeriStrength::PrettyPrint(ostream &f, unsigned /*level*/) const {
    f << "(" << PrintToken(_lval) ;
    if (_rval) {
        /* If rval is not there, this will be a charge_value or single drive_value */
        f << "," << PrintToken(_rval) ;
    }
    f << ")" ;
}
void VeriNetRegAssign::PrettyPrint(ostream &f, unsigned level) const {
    if (_lval) _lval->PrettyPrint(f,level) ;
    if (_val) {
        f << " " << PrintToken(VERI_EQUAL) << " " ;
        _val->PrettyPrint(f,level) ;
    }
}
void VeriInstId::PrettyPrint(ostream &f, unsigned level) const {
    if (_name) f << _name << " " ; }
    if (_dimensions) {
        _dimensions->PrettyPrint(f,level) ;
        f << " " ;
    }
    /* Port connection list is always there. Even when empty */
    f << "(" ;
    unsigned i ;
    VeriExpression *pc ;
    FOREACH_ARRAY_ITEM(_port_connects, i, pc) {
        if (i) f << ", " ;
        if (!pc) continue ;
        pc->PrettyPrint(f,level) ;
    }
    f << ")" ;
}
void VeriCaseItems::PrettyPrint(ostream &f, unsigned level) const {
    /* Print Synopsys pragmas if needed */
    if (_full_case || _parallel_case) {
        f << VeriTreeNode::PrintLevel(level) << "// synopsys" ;
        if (_full_case) f << " full_case" ;
        if (_parallel_case) f << " parallel_case" ;
        f << endl ;
    }
    unsigned i ;
    VeriCaseItem *ci ;
    /* The object itself is the array */
    FOREACH_ARRAY_ITEM(this, i, ci) {
        /* There is no separator */
        if (ci) ci->PrettyPrint(f,level) ;
    }
}
void VeriCaseItem::PrettyPrint(ostream &f, unsigned level) const {
    /* Each item starts at a new line */
    f << PrintLevel(level) ;

    if (_conditions) {
        unsigned i ;
        VeriExpression *condition ;
        FOREACH_ARRAY_ITEM(_conditions, i, condition) {
            if (i) f << ", " ;
            if (condition) condition->PrettyPrint(f,level) ;
        }
    } else {
        /* 'default' case */
        f << PrintToken(VERI_DEFAULT) ;
    }
    /* Print statement on new line, one level deeper */
    f << " : " << endl ;
    if (_stmt) {
        _stmt->PrettyPrint(f,level+1) ;
        /* VeriStatement already ends with a newline */
    } else {
        /* Print semicolon only (null), and end with a newline (as a normal statement) */
        f << PrintLevel(level+1) << "; " << endl ;
    }
}
void VeriGenerateCaseItem::PrettyPrint(ostream &f, unsigned level) const {
    /* Each item starts at a new line */
    f << PrintLevel(level) ;

    if (_conditions) {
        unsigned i ;
        VeriExpression *condition ;
        FOREACH_ARRAY_ITEM(_conditions, i, condition) {
            if (i) f << ", " ;
            if (condition) condition->PrettyPrint(f,level) ;
        }
    } else {
        /* 'default' case */
        f << PrintToken(VERI_DEFAULT) ;
    }
    /* Print statement on new line, one level deeper */
    f << " : " << endl ;
    if (_item) {
        _item->PrettyPrint(f,level+1) ;
        /* VeriStatement already ends with a newline */
    } else {
        /* Print semicolon only (null), and end with a newline (as a normal statement) */
        f << PrintLevel(level+1) << "; " << endl ;
    }
}
void VeriPath::PrettyPrint(ostream& f, unsigned level) const {
    unsigned i ;
    VeriExpression *expr ;

    int bSimplePath = (_data_source) ? 0 : 1 ;
    int bComplexPath = !bSimplePath ;

    f << "(" ;
    if (bComplexPath && _edge) f << PrintToken(_edge) << " ";
    FOREACH_ARRAY_ITEM(_in_terminals, i, expr) {
        if (i) f << "," ;
        if (expr) expr->PrettyPrint(f,level) ;
    }

    f << " " ;
    if (bSimplePath && _polarity_operator) f << PrintToken(_polarity_operator) ;
    f << PrintToken(_path_token) << " " ;

    if (bComplexPath) f << "(" ;
    FOREACH_ARRAY_ITEM(_out_terminals, i, expr) {
        if (i) f << "," ;
        if (expr) expr->PrettyPrint(f,level) ;
    }

    if (bComplexPath) {
        f << " " ;
        if (_polarity_operator) f << PrintToken(_polarity_operator) ;
        f << ": ";
        _data_source->PrettyPrint(f, level) ;
        f << ")" ;
    }

    f << ") " ;
}
void VeriDelayOrEventControl::PrettyPrint(ostream &f, unsigned level) const {
    if (_delay_control) {
        f << "#" ;
        _delay_control->PrettyPrint(f,level) ;
    }
    if (_event_control) {
        if (_repeat_event) {
            f << "repeat (" ;
            _repeat_event->PrettyPrint(f,level) ;
            f << ") " ;
        }
        f << "@ (" ;

        /* 'or' separated list of expressions */
        unsigned i ;
        VeriExpression *event_expr ;
        FOREACH_ARRAY_ITEM(_event_control, i, event_expr) {
            if (i) f << " or " ;
            if (event_expr) event_expr->PrettyPrint(f,level) ;
        }
        f << ")" ;
    }
}

/*************************** VeriConstVal Pretty Print ********************************/

void VeriIntVal::PrettyPrint(ostream &f, unsigned /*level*/) const {
    f << _num ;
}
void VeriRealVal::PrettyPrint(ostream &f, unsigned /*level*/) const {
    char *image = Strings::dtoa(_num) ;
    f << image ;
    Strings::free(image) ;
}

#define GET_BIT(S,B) ((S)?(((S)[(B)/8]>>(B)%8)&1):0)

void VeriConstVal::PrettyPrint(ostream &f, unsigned /*level*/) const {
    /* Print as a sized bit-array */
    /* Note : If originally a 'string' constant was used, it will now
     * be written as a bit-pattern.
    */

    if (_is_string) {
        char *str = Image() ;
        f << str ;
        Strings::free(str) ;
        return ;
    }

    if (_is_signed) {
        f << _size << "'sb" ;
    } else {
        f << _size << "'b" ;
    }

    /* Go MSB to LSB. MSB is highest index. 0 the lowest. */
    unsigned i = _size ;
    while (i-- != 0) {
        /* Determine the value of this bit */
        if (GET_BIT(_xvalue,i)) {
            f << 'x' ;
        } else if (GET_BIT(_zvalue,i)) {
            f << 'z' ;
        } else if (GET_BIT(_value,i)) {
            f << '1' ;
        } else {
            f << '0' ;
        }
    }
}

#endif // #if 0
