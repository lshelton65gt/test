module gate(a, b, o) ;
    input a,b;
    output [1:0] o ;
    assign o = {a && b, a || b, a ^ b};
endmodule
