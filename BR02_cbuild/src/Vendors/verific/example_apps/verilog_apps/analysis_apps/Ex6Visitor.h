/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_ALWAYS_VISITOR_H_
#define _VERIFIC_VERI_ALWAYS_VISITOR_H_

#include "VeriVisitor.h"    // Visitor base class definition
#include "Set.h"
#include "VeriScope.h"      // Definition of VeriScope class

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------------------- */

// This visitor pattern is used to catch the 'always' block and determine the
// containing scope of the 'always' construct.
class AlwaysVisitor : public VeriVisitor
{
public:
    AlwaysVisitor() : _scope(0) {}
    virtual ~AlwaysVisitor() {}

    // The following Visit methods need to be redefined for our purpose :
    // The following class definitions can be found in VeriModuleItem.h
    virtual void Visit(VeriAlwaysConstruct &node) ;
    virtual void Visit(VeriGenerateConditional &node) ;
    virtual void Visit(VeriGenerateCase &node) ;
    virtual void Visit(VeriGenerateFor &node) ;
    virtual void Visit(VeriGenerateBlock &node) ;
    virtual void Visit(VeriModule &node) ;

private :
    VeriScope *_scope ; // contains the scope which contains the 'always' construct
} ;
/* -------------------------------------------------------------------------- */

// This class is used to populate the list of identifiers which are used in the
// event control statement having implicit event expression list i.e. "@(*)" and
// also replace the implicit event expression list with the used variable list.
class EventControlIdVisitor : public VeriVisitor
{
public:
    EventControlIdVisitor(VeriScope *scope) : _ids(POINTER_HASH), _scope(scope) {}
    virtual ~EventControlIdVisitor() {}

    // The following Visit methods need to be redefined for our purpose :
    // The following class definitions can be found in VeriModuleItem.h
    virtual void Visit(VeriBlockingAssign &node) ;
    virtual void Visit(VeriNonBlockingAssign &node) ;
    virtual void Visit(VeriTaskEnable &node) ;
    virtual void Visit(VeriIdRef &node) ;
    virtual void Visit(VeriFunctionCall &node) ;
    virtual void Visit(VeriEventControlStatement &node) ;
    virtual void Visit(VeriSeqBlock &node) ;
    virtual void Visit(VeriParBlock &node) ;

    // Accessor methods
    Set* GetIds()          { return &_ids ; }

private:
    Set _ids ;      // Container(Set) of ids
    VeriScope *_scope ; // contains the scope of the member elements of the 'always' construct.
} ;

/* -------------------------------------------------------------------------- */

// This class is used to visit the left hand side of any assignment statement
// present in the statement part of event control statement having implicit event
// expression. Variables those are assigned in the event control statement will
// not contribute to the sensitivity list, but if bit/part select expression is
// used in left hand side of assignment, index/range part of that select expression
// should contribute to the sensitivity list. So it is clear that we should handle
// assigned identifiers differently than used identifiers when both are used in
// same expression. This class exacly does this. Within this class 'EventControlIdVisitor'
// is stored and used to visit index/range part of select expressions, so that any
// identifiers used as index/range can be used in sensitivity list.
class LhsVisitor : public VeriVisitor
{
public :
    LhsVisitor(EventControlIdVisitor *used_part_visitor) { _used_part_visitor = used_part_visitor ; }
    virtual ~LhsVisitor() { _used_part_visitor = 0 ; }

    // The following Visit methods need to be redefined for our purpose :
    virtual void Visit(VeriIndexedId &node) ;
    virtual void Visit(VeriIndexedMemoryId &node) ;

private :
    EventControlIdVisitor *_used_part_visitor ; // Visitor to visit part of expression that are used (not assigned)
} ;

/* -------------------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_ALWAYS_VISITOR_H_
