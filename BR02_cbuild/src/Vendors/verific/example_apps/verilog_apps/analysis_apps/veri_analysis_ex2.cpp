/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Array.h"          // Make dynamic array class Array available
#include "Map.h"            // Make associated hash table class Map available
#include "Strings.h"        // A string utility/wrapper class

#include "Message.h"        // Make message handlers available, not used in this example

#include "veri_file.h"      // Make Verilog reader available

#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriId.h"         // Definitions of all identifier definition tree nodes
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "VeriConstVal.h"   // Definitions of parse-tree nodes representing constant values in Verilog.
#include "VeriScope.h"      // Symbol table of locally declared identifiers
#include "VeriLibrary.h"    // Definition of VeriLibrary

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Some global functions for this example
static void CustomTraverseExpr(VeriExpression *expression) ;
static void CustomTraverseModuleItem(VeriModuleItem *module_item) ;
static void CustomTraverseStmt(VeriStatement *statement) ;
static void CustomTraverseIdDef(VeriIdDef *id_def) ;
static void CustomTraverseMisc(VeriTreeNode *tree_node) ;

/****************************************************************************************
******************************* VERILOG EXAMPLE #2 **************************************
*****************************************************************************************

                                    NOTES

    As a continuation of example #1, the following code demonstrates a cleaner
    method for traversing a verilog parse-tree.  No longer is it a flat & ugly
    approach, as in example #1.  Five CustomTraverse global static functions have
    been introduced to aid in doing the look-up on GetClassId().

    Once again, GetClassId() is Verific's RTTI mechanism.  We implemented this method
    in place of using ANSI's C++ RTTI mechanism, because we felt their mechanism is too
    expensive in terms of performance.

    Each of these functions is basically a switch statement.  Each function accepts
    a certain typed argment (VeriExpressions, VeriModuleItems, VeriStatements, VeriIdDefs,
    and certain extraneous VeriTreeNode objects that don't fall into the previous
    categories).  I could have made just a single large function that catches everything,
    but I felt it's more readable and understandable this way.

    Examples #3 and #4 will show better ways how to traverse a parse-tree using
    polymorphism and then by using a Visitor design pattern.

    This application can be run with an optional argument specifying a file name, without
    which it is going to work on the default file ie. "ex2_test.v".

    As before, I recommend running through this example in a debugger, since
    you'll be able to look at the state of the objects and see everything
    that is there.  The following example only half-heartedly goes through
    everything that it can do.

*****************************************************************************************/

// This main() function is a test program for the verilog analyzer only.  No
// elaboration is done here.

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veriReader ;   // Read the intro in veri_file.h to see the details of this object.

    char *file_nm ; // Stores the file name

    // If no file name is supplied then take the default file name which is "ex2_test.v"
    switch (argc) {
    // That is if the user do not specify any option
    case 1: file_nm = "ex2_test.v" ; break ; // Sets the file name to the default name
    case 2: file_nm = argv[1] ; break ; // Sets file name to name supplied by the user
    default: Message::Error(0, "Too many options.") ; return 1 ;
    }

    // Now analyze the System Verilog file (into the work library). In case of any error do not process further.
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veriReader.Analyze(file_nm, 3)) return 1 ;

    // Get the list of top modules
    Array *top_mod_array = veriReader.GetTopModules() ;
    if (!top_mod_array) {
        // If there is no top level module then issue error
        Message::Error(0,"Cannot find any top module. Check for recursive instantiation") ;
        return 1 ;
    }

    // Get a handle (by name) to the module that was just analyzed by specifying the module name
    // like this :
    // VeriModule *module = veriReader.GetModule("<Insert the name of the concerned module>") ;

    // Comment the following line if you want to look for a module by its name and hence have uncommented the previous line
    VeriModule *module = (VeriModule *) top_mod_array->GetFirst() ; // Get the first top level module
    delete top_mod_array ; top_mod_array = 0 ; // Cleanup, it is not required anymore

    CustomTraverseModuleItem(module) ; // Traverse the module

    return 1 ;
}

/* ------------------------------------------------------- */

static void CustomTraverseExpr(VeriExpression *expression)
{
    if (!expression) return ;

    // --------------------------------------------------------------------------------------------------------------
    // The following are all the types of VeriExpressions:
    // --------------------------------------------------------------------------------------------------------------
    // VeriAnsiPortDecl        VeriBinaryOperator      VeriConcat             VeriStructUnion         VeriCast
    // VeriEventExpression     VeriUnaryOperator       VeriFunctionCall       VeriDotStar             VeriNew
    // VeriIdRef               VeriIndexedId           VeriIndexedMemoryId    VeriIfOperator          VeriConcatItem
    // VeriMinTypMaxExpr       VeriMultiConcat         VeriPortConnect        VeriSequenceConcat      VeriNull
    // VeriPortOpen            VeriQuestionColon       VeriSelectedName       VeriClockedSequence     VeriDollar
    // VeriSystemFunctionCall  VeriTimingCheckEvent    VeriRange              VeriAssignInSequence    VeriAssignmentPattern
    // VeriConst               VeriConstVal            VeriIntVal             VeriDistOperator        VeriMultiAssignmentPattern
    // VeriRealVal             VeriDataType            VeriTypeRef            VeriSolveBefore         VeriForeachOperator
    // VeriStreamingConcat     VeriCondPredicate       VeriDotName            VeriTaggedUnion         VeriWithExpr
    // VeriInlineConstraint    VeriOpenRangeBinValue   VeriTransBinValue      VeriDefaultBinValue
    // VeriTransSet            VeriTransRangeList      VeriSelectCondition    VeriSelectBinValue
    // --------------------------------------------------------------------------------------------------------------
    // Details about expressions can be found in VeriExpression.h
    switch (expression->GetClassId()) {
    case ID_VERIIDREF:
        { // Identifier reference
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriIdRef. Avoid dynamic_cast overhead.
        VeriIdRef *id_ref = (VeriIdRef*)expression ;

        // Get unresolved prefix name
        const char *name = id_ref->GetName() ;

        // Get the resolved identifier definition that is referred here
        VeriIdDef *id_def = id_ref->FullId() ;

        // You can do another GetIdClass on id_def to get the specific
        // IdDef, but we'll just get some generic info as follows:
        if (id_def) {
            unsigned is_port = id_def->IsPort() ;  // Should return as true in this case

            unsigned port_dir = id_def->Dir() ;    // returns VERI_INPUT, VERI_OUTPUT, VERI_INOUT

            unsigned port_type = id_def->Type() ;  // returns VERI_WIRE, VERI_REG, etc ...
        }
        break ;
        }
    case ID_VERIANSIPORTDECL:
        { // Ansi port declaration
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriAnsiPortDecl. Avoid dynamic_cast overhead.
        VeriAnsiPortDecl *ansi_ports = static_cast<VeriAnsiPortDecl*>(expression) ;

        // Get data type for this declaration
        VeriDataType *data_type = ansi_ports->GetDataType() ; // VeriDataType contains the 'type' (token VERI_REG, VERI_WIRE, etc ...), array dimension(s), and 'signing'.

        // Get the ports direction (as a token VERI_INPUT, VERI_OUTPUT, VERI_INOUT..)
        unsigned port_dir = ansi_ports->GetDir() ;

        // Iterate through all ids that pertain to this Ansi decl
        VeriIdDef *id_def ;
        unsigned j ;
        FOREACH_ARRAY_ITEM(ansi_ports->GetIds(), j, id_def) {
            if (!id_def) continue ;
            const char *name = id_def->Name() ;
            CustomTraverseIdDef(id_def) ;    // Traverse (if you like ...)
        }
        break ;
        }
    case ID_VERIINTVAL:
        { // Integer value
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriIntVal. Avoid dynamic_cast overhead.
        VeriIntVal *int_value = static_cast<VeriIntVal*>(expression) ;

        // Get actual int value
        int val = int_value->GetNum() ;

        // Get an char image of this value (NOTE: memory is allocated)
        char *image = int_value->Image() ;
        // ... make sure to either absorb this image somewhere, or eventual delete it.
        Strings::free(image) ;

        // Get some other info
        unsigned size = int_value->Size(0) ;  // Size in bits
        unsigned signed_token = int_value->Sign() ; // Is this signed
        break ;
        }
    case ID_VERICONSTVAL:
        {  // Constant value
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriConstVal. Avoid dynamic_cast overhead.
        VeriConstVal *constant_value = static_cast<VeriConstVal*>(expression) ;

        // Get a char image of this value (NOTE: memory is allocated)
        char *image = constant_value->Image() ;
        // ... make sure to either absorb this image somewhere, or eventual delete it.
        Strings::free(image) ;

        // Get some other info
        unsigned size = constant_value->Size(0) ; // Size in bits
        unsigned signed_token = constant_value->Sign() ; // Is this signed
        unsigned has_x_or_z = constant_value->HasXZ() ; // See if constant has x or z.
        unsigned is_string  = constant_value->IsString() ; // See if it was created from a string
        break ;
        }
    case ID_VERIFUNCTIONCALL:
        {  // Function call
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriFunctionCall. Avoid dynamic_cast overhead.
        VeriFunctionCall *function_call = static_cast<VeriFunctionCall*>(expression) ;

        // Get unresolved name of the function call
        const char *name = function_call->GetName() ;

        // Get resolved function identifier
        VeriIdDef *id_def = function_call->GetId() ;
        CustomTraverseIdDef(id_def) ;

         // Get array of VeriExpression*. The function arguments
        Array *args = function_call->GetArgs() ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(args, i, expr) {
            CustomTraverseExpr(expr) ; // Traverse (if you like ...)
        }
        break ;
        }
    case ID_VERIDATATYPE:
        { // An entire data type on a data declaration. Contains type token, dimensions and signing
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriTreeNode-> VeriDataType. Avoid dynamic_cast overhead.
        VeriDataType *data_type = static_cast<VeriDataType*>(expression) ;

        // Traverse the array range(s) (if there) :
        CustomTraverseExpr(data_type->GetDimensions()) ;

        // Get the data 'type' token :
        unsigned type = data_type->GetType() ; // VERI_REG, VERI_WIRE, etc

        // Get the 'signing' token (VERI_SIGNED, VERI_UNSIGNED)
        unsigned signing = data_type->GetSigning() ;
        break ;
        }
    case ID_VERIRANGE:
        { // Range object
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriTreeNode-> VeriRange. Avoid dynamic_cast overhead.
        VeriRange *range = static_cast<VeriRange*>(expression) ;

        // Get bounds to range
        VeriExpression *left_expr  = range->GetLeft() ;
        VeriExpression *right_expr = range->GetRight() ;

        // Traverse them
        CustomTraverseExpr(left_expr) ;
        CustomTraverseExpr(right_expr) ;

        // Also traverse multi-dimensional array ranges (range has a Next range pointer to LSB range) :
        CustomTraverseExpr(range->GetNext()) ;
        break ;
        }
    case ID_VERIBINARYOPERATOR:
        { // For Binary operator
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriBinaryOperator. Avoid dynamic_cast overhead.
        VeriBinaryOperator *bin_op = static_cast<VeriBinaryOperator*>(expression) ;

        // Get the type of operator
        unsigned op_type = bin_op->OperType() ;

        // Get the left and right operands
        VeriExpression *left = bin_op->GetLeft() ;
        CustomTraverseExpr(left) ; // Traverse if you want to...

        VeriExpression *right = bin_op->GetRight() ;
        CustomTraverseExpr(right) ; // Traverse if you want to...
        break ;
        }
    case ID_VERICONCAT:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriConcat. Avoid dynamic_cast overhead.
        VeriConcat * concat = static_cast<VeriConcat*>(expression) ;

        // Gets the array of expressions
        Array *expr_arr = concat->GetExpressions() ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(expr_arr, i, expr) {
            CustomTraverseExpr(expr) ;    // Traverse (if you like ...)
        }
        break ;
        }
    case ID_VERIEVENTEXPRESSION:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriEventExpression. Avoid dynamic_cast overhead.
        VeriEventExpression * evt_expr = static_cast<VeriEventExpression*>(expression) ;

        unsigned edge_token = evt_expr->GetEdgeToken() ; // VERI_POSEDGE, VERI_NEGEDGE or 0 (for both edges).

        VeriExpression * expr = evt_expr->GetExpr() ; // Expression from which the edges are taken.^M
        CustomTraverseExpr(expr) ; // Traverse if you want...

        VeriExpression *if_cond = evt_expr->GetIffCondition() ; // A level condition under which the edge is tested. Only set for SV.^M
        CustomTraverseExpr(if_cond) ; // Traverse if you want...
        break ;
        }
    case ID_VERIUNARYOPERATOR:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriUnaryOperator. Avoid dynamic_cast overhead.
        VeriUnaryOperator * un_op = static_cast<VeriUnaryOperator*>(expression) ;

        unsigned oper_type = un_op->OperType() ;
        VeriExpression *arg_expr = un_op->GetArg() ;
        CustomTraverseExpr(arg_expr) ; // Traverse (if you like ...)
        break ;
        }
    case ID_VERIINDEXEDID:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriIndexedId. Avoid dynamic_cast overhead.
        VeriIndexedId *indexed_id = static_cast<VeriIndexedId*>(expression) ;

        VeriName *prefix_name = indexed_id->GetPrefix() ; // The prefix name^M
        CustomTraverseExpr(prefix_name) ; // Traverse if you want...

        VeriExpression *index = indexed_id->GetIndexExpr() ; // Get index expression
        CustomTraverseExpr(index) ; // Traverse if you want...

        const char *name = indexed_id->GetName() ; // Get prefix name in char* form

        VeriIdDef *prefix_id = indexed_id->GetId() ; // Get resolved referenced (prefix) identifier

        // Here you can put more code to get more info about the port_id.
        // The full set of routines that are allowed on a VeriIdDef is in VeriId.h.
        break ;
        }
    case ID_VERIINDEXEDMEMORYID:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriIndexedMemoryId. Avoid dynamic_cast overhead.
        VeriIndexedMemoryId *indx_mem_id = static_cast<VeriIndexedMemoryId*>(expression) ;

        VeriName *name = indx_mem_id->GetPrefix() ; // Get the prefix name
        CustomTraverseExpr(name) ; // Traverse if you want...

        Array *indx_arr = indx_mem_id->GetIndexes() ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(indx_arr, i, expr) {
            CustomTraverseExpr(expr) ; // Traverse (if you like ...)
        }
        break ;
        }
    case ID_VERIMINTYPMAXEXPR:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriMinTypMaxExpr. Avoid dynamic_cast overhead.
        VeriMinTypMaxExpr *min_type_max_expr = static_cast<VeriMinTypMaxExpr*>(expression) ;

        // Get the min, typ and max expressions and traverse them
        VeriExpression *min_expr = min_type_max_expr->GetMinExpr() ;
        CustomTraverseExpr(min_expr) ; // Traverse (if you like ...)

        VeriExpression *typ_expr = min_type_max_expr->GetTypExpr() ;
        CustomTraverseExpr(typ_expr) ; // Traverse (if you like ...)

        VeriExpression *max_expr = min_type_max_expr->GetMaxExpr() ;
        CustomTraverseExpr(max_expr) ; // Traverse (if you like ...)
        break ;
        }
    case ID_VERIMULTICONCAT:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriMultiConcat. Avoid dynamic_cast overhead.
        VeriMultiConcat *multi_concat = static_cast<VeriMultiConcat*>(expression) ;

        VeriExpression *repeat_expr = multi_concat->GetRepeat() ; // Get repeat VeriExpression
        CustomTraverseExpr(repeat_expr) ; // Traverse (if you like ...)

        Array *expr_arr = multi_concat->GetExpressions() ; // Get array of VeriExpressions
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(expr_arr, i, expr) {
            CustomTraverseExpr(expr) ; // Traverse (if you like ...)
        }
        break ;
        }
    case ID_VERIPORTCONNECT:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriPortConnect. Avoid dynamic_cast overhead.
        VeriPortConnect *port_connect = static_cast<VeriPortConnect*>(expression) ;

        VeriExpression *conn = port_connect->GetConnection() ; // Return the 'actual' expression
        CustomTraverseExpr(conn) ; // Traverse (if you like ...)

        const char *name = port_connect->NamedFormal() ; // Return the formal name
        break ;
        }
    case ID_VERIPORTOPEN:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriPortOpen. Avoid dynamic_cast overhead.
        VeriPortOpen *port_open = static_cast<VeriPortOpen*>(expression) ;

        unsigned flag_open = port_open->IsOpen() ; // Check if this expression is an open port association
        break ;
        }
    case ID_VERIQUESTIONCOLON:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriQuestionColon. Avoid dynamic_cast overhead.
        VeriQuestionColon *question_colon = static_cast<VeriQuestionColon*>(expression) ;

        VeriExpression *if_expr = question_colon->GetIfExpr() ; // Gets the condition
        CustomTraverseExpr(if_expr) ; // Traverse (if you like ...)

        VeriExpression *then_expr = question_colon->GetThenExpr() ; // Gets the then part
        CustomTraverseExpr(then_expr) ; // Traverse (if you like ...)

        VeriExpression *else_expr = question_colon->GetElseExpr() ; // Gets the else part
        CustomTraverseExpr(else_expr) ; // Traverse (if you like ...)
        break ;
        }
    case ID_VERISYSTEMFUNCTIONCALL:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriSystemFunctionCall. Avoid dynamic_cast overhead.
        VeriSystemFunctionCall *sys_fn_call = static_cast<VeriSystemFunctionCall*>(expression) ;
        const char *name = sys_fn_call->GetName() ; // Get unresolved system function name

        Array *args_arr = sys_fn_call->GetArgs() ; // Get array of VeriExpression*. Should not contain VeriPortConnect. The system function arguments
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(args_arr, i, expr) {
            CustomTraverseExpr(expr) ;    // Traverse (if you like ...)
        }

        unsigned fn_type = sys_fn_call->GetFunctionType() ; // Get a VERI token of the function of the system call. 0 if not known
        break ;
        }
    case ID_VERITIMINGCHECKEVENT:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriTimingCheckEvent. Avoid dynamic_cast overhead.
        VeriTimingCheckEvent *timing_chk_evt = static_cast<VeriTimingCheckEvent*>(expression) ;

        unsigned token = timing_chk_evt->GetEdgeToken() ;

        char *edge_desc_str = timing_chk_evt->GetEdgeDescStr() ; // If defined, it's length will be 2 bytes or 4 bytes

        VeriExpression *terminal_desc = timing_chk_evt->GetTerminalDesc() ;
        CustomTraverseExpr(terminal_desc) ; // Traverse (if you like ...)

        VeriExpression *cond = timing_chk_evt->GetCondition() ; // Get the timing check condition
        CustomTraverseExpr(cond) ; // Traverse (if you like ...)
        break ;
        }
    case ID_VERICONST:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriConst. Avoid dynamic_cast overhead.
        VeriConst *const_expr = static_cast<VeriConst*>(expression) ;

        unsigned flag_xz = const_expr->HasXZ() ; // See if constant has x or z

        int int_val = const_expr->Integer() ; // Return an integer representation (correct if <32 bits and no 'x'or'z'

        unsigned sign = const_expr->Sign() ; // Return sign of expression
        break ;
        }
    case ID_VERIREALVAL:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriRealVal. Avoid dynamic_cast overhead.
        VeriRealVal *real_val = static_cast<VeriRealVal*>(expression) ;

        unsigned sign = real_val->Sign() ; // Return sign of expression

        int int_val = real_val->Integer() ; // Return an integer representation (correct if <32 bits and no 'x'or'z'

        double num = real_val->GetNum() ;
        break ;
        }
    case ID_VERISELECTEDNAME:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriSelectedName. Avoid dynamic_cast overhead.
        VeriSelectedName *sel_name = static_cast<VeriSelectedName*>(expression) ;
        const char *name = sel_name->GetName() ; // Gets the name in const char* form

        VeriName *veri_prefix_name = sel_name->GetPrefix() ; // Get the prefix name
        CustomTraverseExpr(veri_prefix_name) ; // Traverse if you want...

        const char *suffix_name = sel_name->GetSuffix() ; // Get the suffix name
        break ;
        }
    // Now analyze the System Verilog file (into the work library). In case on any error do not process further.
    case ID_VERITYPEREF:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression-> VeriTypeRef. Avoid dynamic_cast overhead.
        VeriTypeRef *type_ref = static_cast<VeriTypeRef*>(expression) ;

        VeriName *veri_type_name = type_ref->GetTypeName() ; // the name (can be a hierarchical type name, or a simple identifier reference)
        CustomTraverseExpr(veri_type_name) ; // Traverse if you want...

        VeriIdDef *ref_id = type_ref->GetId() ; // Get resolved referenced (prefix) identifier
        const char *prefix_name = type_ref->GetName() ; // Get prefix name in char* form
        break ;
        }
    case ID_VERISTRUCTUNION:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression ->VeriStructUnion. Avoid dynamic_cast overhead.
        VeriStructUnion *struct_union = static_cast<VeriStructUnion*>(expression) ;

        Array *decl_arr = struct_union->GetDecls() ; // Array of VeriDataDecl (VeriModuleItem) which contains the member declarations
        unsigned i ;
        VeriDataDecl *data_decl ;
        FOREACH_ARRAY_ITEM(decl_arr, i, data_decl) {
            CustomTraverseModuleItem(data_decl) ; // Traverse if you want to ...
        }

        Array *mem_id_arr = struct_union->GetIds() ; // Get array of VeriIdDef. The declared struct/union members
        VeriIdDef *mem_id ;
        FOREACH_ARRAY_ITEM(mem_id_arr, i, mem_id) {
            CustomTraverseIdDef(mem_id) ; // Traverse if you want to ...
        }
        break ;
        }
    case ID_VERIENUM:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression ->VeriEnum. Avoid dynamic_cast overhead.
        VeriEnum *enum_expr = static_cast<VeriEnum*>(expression) ;

        Array *mem_arr = enum_expr->GetEnums() ; // Return the array of enumeration identifiers. Only for enums (and typerefs of enums)
        unsigned i ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(mem_arr, i, id) {
            CustomTraverseIdDef(id) ; // Traverse if you want to ...
        }
        break ;
        }
    case ID_VERIDOTSTAR:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriDotStar. Avoid dynamic_cast overhead.
        VeriDotStar *dot_star = static_cast<VeriDotStar*>(expression) ;

        unsigned flag = dot_star->IsDotStar() ; // Should always return 1
        break ;
        }
    case ID_VERIIFOPERATOR:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriIfOperator. Avoid dynamic_cast overhead.
        VeriIfOperator *if_oper = static_cast<VeriIfOperator*>(expression) ;

        VeriExpression *if_expr = if_oper->GetIfExpr() ; // Get the if expression
        CustomTraverseExpr(if_expr) ; // Traverse if you want to...

        VeriExpression *then_expr = if_oper->GetThenExpr() ; // Get the then expression
        CustomTraverseExpr(then_expr) ; // Traverse if you want to...

        VeriExpression *else_expr = if_oper->GetElseExpr() ; // Get the else expression
        CustomTraverseExpr(else_expr) ; // Traverse if you want to...

        // Control reaches here for System Verilog 'if' 'else' operator,
        // used for constraints and SVA expressions
        break ;
        }
    case ID_VERISEQUENCECONCAT:// A VeriSequenceConcat implements the expression " <sequence> ##(num_or_range) <sequence> "
        // Both must be a sequence. left side is optional. If missing, it represents sequence '1' (true)
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriSequenceConcat. Avoid dynamic_cast overhead.
        VeriSequenceConcat *seq_concat_op = static_cast<VeriSequenceConcat*>(expression) ;

        // Get the type of operator
        unsigned op_type = seq_concat_op->OperType() ;

        VeriExpression *left = seq_concat_op->GetLeft() ; // Get the left operand
        CustomTraverseExpr(left) ; // Traverse if you want to...

        VeriExpression *right = seq_concat_op->GetRight() ; // Get the right operand
        CustomTraverseExpr(right) ; // Traverse if you want to...

        // Control reaches here for expressions of the form " <sequence> ##(num_or_range) <sequence> "
        // Both must be a sequence. left side is optional. If missing, it represents sequence '1' (true).
        break ;
        }
    case ID_VERICLOCKEDSEQUENCE:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriClockedSequence. Avoid dynamic_cast overhead.
        VeriClockedSequence *clocked_seq = static_cast<VeriClockedSequence*>(expression) ;

        VeriExpression *evt_expr = clocked_seq->GetEventExpression() ; // The clocking event for the sequence (argument)..

        CustomTraverseExpr(evt_expr) ; // Traverse if you want to ...

        VeriExpression *arg_expr = clocked_seq->GetArg() ;
        CustomTraverseExpr(arg_expr) ; // Traverse (if you like ...)
        break ;
        }
    case ID_VERIASSIGNINSEQUENCE: // VeriAssignmentsInSequence implements the "," operation as a sequence : " <expr> , a=5, c++ "
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression ->VeriAssignInSequence. Avoid dynamic_cast overhead.
        VeriAssignInSequence *assign_in_seq = static_cast<VeriAssignInSequence*>(expression) ;

        VeriExpression *arg_expr = assign_in_seq->GetArg() ;
        CustomTraverseExpr(arg_expr) ; // Traverse (if you like ...)

        unsigned i ;
        Array *stmt_arr = assign_in_seq->GetStatements() ; // The assignment statements to be executed when 'expr' holds
        VeriStatement *stmt ;
        FOREACH_ARRAY_ITEM(stmt_arr, i, stmt) {
            CustomTraverseStmt(stmt) ; // Traverse if you want to ...
        }
        break ;
        }
    case ID_VERIDISTOPERATOR: // VeriDistOperator implements a 'dist' operation : " <expr> dist { dist_list } "
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression ->VeriDistOperator. Avoid dynamic_cast overhead.
        VeriDistOperator *dist_op = static_cast<VeriDistOperator*>(expression) ;

        Array *dist_arr = dist_op->GetDistList() ; // The dist items (Array of VeriExpression) on RHS of the dist token
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(dist_arr, i, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want to ...
        }
        break ;
        }
    case ID_VERISOLVEBEFORE: // VeriSolveBefore implements the variable ordering " solve <id_list> before <id_list> " construct.
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression ->VeriSolveBefore. Avoid dynamic_cast overhead.
        VeriSolveBefore *solve_before = static_cast<VeriSolveBefore*>(expression) ;

        Array *solve_arr = solve_before->GetSolveList() ; // Array of VeriName's, indicating the variables which need to be solved
        unsigned i ;
        VeriName *name ;
        FOREACH_ARRAY_ITEM(solve_arr, i, name) {
            CustomTraverseExpr(name) ; // Traverse if you want...
        }

        Array *before_arr = solve_before->GetBeforeList() ; // Array of VeriName's, indicating the variables which need to be solved before the solved list
        FOREACH_ARRAY_ITEM(before_arr, i, name) {
            CustomTraverseExpr(name) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERICAST:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression ->VeriCast. Avoid dynamic_cast overhead.
        VeriCast *cast = static_cast<VeriCast*>(expression) ;

        VeriExpression *tgt_type_expr = cast->GetTargetType() ; // The type to which we will cast (or numeric constant for 'number of bits')
        CustomTraverseExpr(tgt_type_expr) ;

        VeriExpression *expr = cast->GetExpr() ; // The expression which is casted
        CustomTraverseExpr(expr) ;
        break ;
        }
    case ID_VERINEW:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriNew. Avoid dynamic_cast overhead.
        VeriNew *new_expr = static_cast<VeriNew*>(expression) ;

        VeriExpression *size_expr = new_expr->GetSizeExpr() ; // The number of elements to create this object..
        CustomTraverseExpr(size_expr) ; // Traverse if you want to ...

        Array *args_arr = new_expr->GetArgs() ; // The arguments (to the 'new' operator)
        unsigned i ;
        VeriExpression *arg ;
        FOREACH_ARRAY_ITEM(args_arr, i, arg) {
            CustomTraverseExpr(arg) ; // Traverse if you want to ...
        }
        break ;
        }
    case ID_VERICONCATITEM:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriConcatItem. Avoid dynamic_cast overhead.
        VeriConcatItem *concat_item = static_cast<VeriConcatItem*>(expression) ;

        VeriExpression *mem_lbl = concat_item->GetMemberLabel() ; // The expression which identifies the member label. If 0, is is the 'default' label.
        CustomTraverseExpr(mem_lbl) ;

        VeriExpression *expr = concat_item->GetExpr() ; // The expression which is casted.
        CustomTraverseExpr(expr) ;
        break ;
        }
    case ID_VERINULL:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriNull. Avoid dynamic_cast overhead.
        VeriNull *null_expr = static_cast<VeriNull*>(expression) ;

        unsigned flag_null = null_expr->IsNull() ; // Check if this expression is a null literal
        break ;
        }
    case ID_VERIDOLLAR:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriDollar. Avoid dynamic_cast overhead.
        VeriDollar *dollar_expr = static_cast<VeriDollar*>(expression) ;

        unsigned flag_dollar = dollar_expr->IsDollar() ; // Check if this expression is a dollar expression
        break ;
        }
    case ID_VERIASSIGNMENTPATTERN:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriAssignmentPattern. Avoid dynamic_cast overhead.
        VeriAssignmentPattern *assign_pattern = static_cast<VeriAssignmentPattern*>(expression) ;
        // <data_type> '{ <concat_elements> }

        // Get the type of the assignment pattern expression
        VeriExpression *type = assign_pattern->GetTargetType() ;
        CustomTraverseExpr(type) ; // Traverse if you want...
        break ;
        }
    case ID_VERIMULTIASSIGNMENTPATTERN:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriMultiAssignmentPattern. Avoid dynamic_cast overhead.
        VeriMultiAssignmentPattern *multi_assgn_pattern = static_cast<VeriMultiAssignmentPattern*>(expression) ;

        // Get the type of the multiassignment pattern expression
        VeriExpression *type = multi_assgn_pattern->GetTargetType() ;
        CustomTraverseExpr(type) ; // Traverse if you want...
        break ;
        }
    case ID_VERIFOREACHOPERATOR:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriForeachOperator. Avoid dynamic_cast overhead.
        VeriForeachOperator *foreach_operator = static_cast<VeriForeachOperator*>(expression) ;

        // Get the name of the array
        VeriName *arr_name = foreach_operator->GetArrayName() ;
        CustomTraverseExpr(arr_name) ; // Traverse if you want ...

        // Get the loop indexes
        Array *indexes = foreach_operator->GetLoopIndexes() ;
        unsigned i ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(indexes, i, id) {
            CustomTraverseIdDef(id) ; // Traverse if you want ...
        }

        // Get the constraint set
        VeriExpression *constraint_set = foreach_operator->GetConstraintSet() ;
        CustomTraverseExpr(constraint_set) ; // Traverse if you want ...
        break ;
        }
    case ID_VERISTREAMINGCONCAT:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriStreamingConcat. Avoid dynamic_cast overhead.
        VeriStreamingConcat *streaming_conc_expr = static_cast<VeriStreamingConcat*>(expression) ;
        // { stream_operator [ slice_size] { stream_expression {, stream_expression}}}

        // Get the token
        unsigned oper_type = streaming_conc_expr->OperType() ; // Token VERI_LSHIFT or VERI_RSHIFT

        // Get the number of bits in each slice of data to be streamed
        VeriExpression *slice_size = streaming_conc_expr->GetSliceSize() ;
        CustomTraverseExpr(slice_size) ; // Traverse if you want ...
        break ;
        }
    case ID_VERICONDPREDICATE:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriCondPredicate. Avoid dynamic_cast overhead.
        VeriCondPredicate *cond_predicate = static_cast<VeriCondPredicate*>(expression) ;
        //  <expression_or_cond_pattern> {"&&&" <expression_or_cond_pattern>}"

        // Get the left operand
        VeriExpression *left = cond_predicate->GetLeft() ;
        CustomTraverseExpr(left) ; // Traverse if you want ...

        // Get the right operand
        VeriExpression *right = cond_predicate->GetRight() ;
        CustomTraverseExpr(right) ; // Traverse if you want ...
        break ;
        }
    case ID_VERIDOTNAME:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriDotName. Avoid dynamic_cast overhead.
        VeriDotName *cond_predicate = static_cast<VeriDotName*>(expression) ;
        // ".<variable_identifier>"

        // Get the name
        VeriName *name = cond_predicate->GetVarName() ;
        CustomTraverseExpr(name) ; // Traverse if you want ...
        break ;
        }
    case ID_VERITAGGEDUNION:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriTaggedUnion. Avoid dynamic_cast overhead.
        VeriTaggedUnion *tagged_union = static_cast<VeriTaggedUnion*>(expression) ;
        // "tagged <member_identifier> [ <expression>]"

        // Get the name
        VeriName *member = tagged_union->GetMember() ;
        CustomTraverseExpr(member) ; // Traverse if you want ...

        // Get the expression
        VeriExpression *expr = tagged_union->GetExpr() ;
        CustomTraverseExpr(expr) ; // Traverse if you want ...
        break ;
        }
    case ID_VERIWITHEXPR:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriWithExpr. Avoid dynamic_cast overhead.
        VeriWithExpr *with_expr = static_cast<VeriWithExpr*>(expression) ;
        //  "<function_call> with ( <expression>)"
        //  "<expression> with [ <array_range_expression>]"

        // Get the R.H.S expression
        VeriExpression *right = with_expr->GetRight() ;
        CustomTraverseExpr(right) ; // Traverse if you want ...
        break ;
        }
    case ID_VERIINLINECONSTRAINT:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriInlineConstraint. Avoid dynamic_cast overhead.
        VeriInlineConstraint *inline_constraint_expr = static_cast<VeriInlineConstraint*>(expression) ;
        //  "<function_call> with <constraint_block>"

        // Get the R.H.S of 'with'
        Array *constraint_block =inline_constraint_expr->GetConstraintBlock() ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(constraint_block, i, expr) {
            CustomTraverseExpr(expr) ;
        }
        break ;
        }
    case ID_VERIOPENRANGEBINVALUE:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriOpenRangeBinValue. Avoid dynamic_cast overhead.
        VeriOpenRangeBinValue *open_range_bin = static_cast<VeriOpenRangeBinValue*>(expression) ;
        //   { <open_range_list> } [ iff (expression)]

        // Get the array of open ranges
        Array *open_range_list = open_range_bin->GetOpenRangeList() ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(open_range_list, i, expr) {
            CustomTraverseExpr(expr) ;
        }
        break ;
        }
    case ID_VERITRANSBINVALUE:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriTransBinValue. Avoid dynamic_cast overhead.
        VeriTransBinValue *trans_bin_value = static_cast<VeriTransBinValue*>(expression) ;
        //   <trans_list> [ iff (expression)]

        Array *trans_list = trans_bin_value->GetTransList() ;
        unsigned i ;
        VeriTransSet *trans ;
        FOREACH_ARRAY_ITEM(trans_list, i, trans) {
            CustomTraverseExpr(trans) ;
        }
        break ;
        }
    case ID_VERIDEFAULTBINVALUE:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriDefaultBinValue. Avoid dynamic_cast overhead.
        VeriDefaultBinValue *default_bin_value = static_cast<VeriDefaultBinValue*>(expression) ;
        //   default [ iff (expression)]
        //   default sequence [ iff (expression)]

        // Check if the keyword 'default sequence' is specified in the bin value
        unsigned sequence = default_bin_value->IsDefaultSequence() ;
        break ;
        }
    case ID_VERISELECTBINVALUE:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriSelectBinValue. Avoid dynamic_cast overhead.
        VeriSelectBinValue *select_bin_value = static_cast<VeriSelectBinValue*>(expression) ;
        // <select_expr> [iff (expression)]

        // Get the select expression specified in selection bin
        VeriExpression *select_expr = select_bin_value->GetSelectExpr() ;
        CustomTraverseExpr(select_expr) ; // Traverse if you want ...
        break ;
        }
    case ID_VERITRANSSET:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriTransSet. Avoid dynamic_cast overhead.
        VeriTransSet *trans_set = static_cast<VeriTransSet*>(expression) ;
        //  <trans_range_list> { => <trans_range_list> }

        // Get the range list
        Array *trans_range_list = trans_set->GetTransRangeList() ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(trans_range_list, i, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want ...
        }
        break ;
        }
    case ID_VERITRANSRANGELIST:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriTransRangeList. Avoid dynamic_cast overhead.
        VeriTransRangeList *trans_range_list = static_cast<VeriTransRangeList*>(expression) ;
        //    <trans_item>
        //  | <trans_item> [*  <repeat_range> ]
        //  | <trans_item> [-> <repeat_range> ]
        //  | <trans_item> [=  <repeat_range> ]

        // Get the range list
        Array *range_list = trans_range_list->GetTransItem() ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(range_list, i, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want ...
        }

        // Get the repetition of transition item
        VeriExpression *repetition = trans_range_list->GetRepetition() ;
        CustomTraverseExpr(repetition) ; // Traverse if you want ...
        break ;
        }
    case ID_VERISELECTCONDITION:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriSelectCondition. Avoid dynamic_cast overhead.
        VeriSelectCondition *select_cond = static_cast<VeriSelectCondition*>(expression) ;

        // Get the bins expression
        VeriExpression *bins_expr = select_cond->GetBinsExpr() ;
        CustomTraverseExpr(bins_expr) ; // Traverse if you want ...

        // Get the range list
        Array *range_list = select_cond->GetOpenRangeList() ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(range_list, i, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want ...
        }
        break ;
        }
    case ID_VERITYPEOPERATOR:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriTypeOperator. Avoid dynamic_cast overhead.
        VeriTypeOperator *type_op = static_cast<VeriTypeOperator*>(expression) ;
        //      type (<expression>)
        //      type (<data_type>)

        // Get the data type or expression for the concerned VeriTypeOperator
        VeriExpression *expr_or_data_type = type_op->GetArg() ;
        break ;
        }
    case ID_VERIPATTERNMATCH:
        {
        // ------- Look in "VeriExpression.h" for class description of this object -------
        // Downcast VeriExpression->VeriPatternMatch. Avoid dynamic_cast overhead.
        VeriPatternMatch *pattern_match = static_cast<VeriPatternMatch*>(expression) ;
        //      <expr> matches <pattern>

        // Get the left expression
        VeriExpression *left = pattern_match->GetLeft() ;
        CustomTraverseExpr(left) ; // Traverse if you want ...

        // Get the right expression
        VeriExpression *right = pattern_match->GetRight() ;
        CustomTraverseExpr(right) ; // Traverse if you want ...
        break ;
        }

    default: Message::Error(expression->Linefile(),"unknown expression found") ;  // ERROR: This should never occur!
    }
}

/* ------------------------------------------------------- */
/* -------------- Global Function Definitions ------------ */
/* ------------------------------------------------------- */

static void CustomTraverseModuleItem(VeriModuleItem *module_item)
{
    if (!module_item) return ;

    // -------------------------------------------------------------------------------------------------------------------------------
    // The following are all the types of VeriModuleItems: (base class VeriModuleItem)
    // -------------------------------------------------------------------------------------------------------------------------------
    // VeriAlwaysConstruct         VeriContinuousAssign    VeriDataDecl                VeriPropertyDecl       VeriClockingDecl
    // VeriDefParam                VeriFunctionDecl        VeriGateInstantiation       VeriSequenceDecl
    // VeriGenerateBlock           VeriGenerateCase        VeriGenerateConditional     VeriModport
    // VeriGenerateConstruct       VeriGenerateFor         VeriInitialConstruct        VeriModportDecl
    // VeriModuleInstantiation     VeriTaskDecl            VeriNetDecl                 VeriClass
    // VeriPathDecl                VeriSpecifyBlock        VeriSystemTimingCheck       VeriBindDirective
    // VeriTable                   VeriInterface           VeriProgram                 VeriConstraintDecl
    // -------------------------------------------------------------------------------------------------------------------------------
    // Details about module items can be found out in the file VeriModuleItem.h
    switch (module_item->GetClassId()) {
    case ID_VERIMODULE:
        {
        VeriModule *module = static_cast<VeriModule*>(module_item) ;
        // Iterate through the module's list of ports
        Array *port_exprs = module->GetPortConnects() ;
        VeriExpression *port_expr ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(port_exprs, i, port_expr) {
            CustomTraverseExpr(port_expr) ;
        }

        // Iterate through the module items (such as various object declarations, assignments,
        // instantiations, constructs, and so forth).
        Array *module_items = module->GetModuleItems() ;
        VeriModuleItem *module_item ;
        FOREACH_ARRAY_ITEM(module_items, i, module_item) {
            CustomTraverseModuleItem(module_item) ;
        }

        // Get a handle to the scope that this module was declared in
        VeriScope *scope = module->GetScope() ;
        // A VeriScope is a symbol table of locally declared identifiers.  These VeriIdDef's are stored
        // as (char*->VeriIdDef*) in a Map container.  The upper level scope (containing this one) is accessible
        // with VeriScope::Upper(), and the lower level scopes is accessible with VeriScope::SubScope(char* name).
        // The identifier that owns (created) this scope is available with VeriScope::Owner(), where the
        // owner is a module, a task, a function, a named block, or 0 for an unnamed block.

        // Regular search (direct visibility) is done with Find(name). This looks through
        // the scope for the give identifier by name. Returns 0 if not found.

        // Iterate through the present-level scope table (Map)
        MapIter mapIter ;
        Map *pDeclArea = (scope) ? scope->DeclArea() : 0 ;
        char *pIdName ;
        VeriIdDef *id_def ;
        FOREACH_MAP_ITEM(pDeclArea, mapIter, &pIdName, &id_def) {
            CustomTraverseIdDef(id_def) ;
        }
        break ;
        }
    case ID_VERIDATADECL:
        { // I/O Declaration
        // ------- Look in "VeriModuleItem.h" for class description of this object --------
        // Any data declaration (io decl, reg decl, parameter decl etc), except net decl.
        // Downcast VeriModuleItem-> VeriDataDecl. Avoid dynamic_cast overhead.
        VeriDataDecl *data_decl = static_cast<VeriDataDecl*>(module_item) ;

        unsigned dir_token = data_decl->GetDir() ; // returns VERI_INPUT, VERI_OUTPUT, VERI_INOUT

        // Get the data type :
        VeriDataType *data_type = data_decl->GetDataType() ; // VeriDataType contains the 'type' (token VERI_REG, VERI_WIRE, etc ...), array dimension(s), and 'signing'.
        // Traverse the data type (... if you like)
        CustomTraverseExpr(data_type) ;

        // Iterate through all ids that pertain to this data decl
        Array *ids = (data_decl) ? data_decl->GetIds() : 0 ;
        VeriIdDef *id_def ;
        unsigned j ;
        FOREACH_ARRAY_ITEM(ids, j, id_def) {
            if (!id_def) continue ;
            const char *name = id_def->Name() ;
            // Here you can put more code to get more info about the id_def ;
            if (id_def->IsArray()) { /* This idDef is an array */ }
            CustomTraverseIdDef(id_def) ;
        }
        break ;
        }
    case ID_VERINETDECL:
        { // Net(wire) Declaration
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem-> VeriNetDecl. Avoid dynamic_cast overhead.
        VeriNetDecl *net_decl = static_cast<VeriNetDecl*>(module_item) ;

        unsigned net_type = net_decl->GetNetType() ; // returns VERI_WIRE, VERI_TRIREG, etc ...

        unsigned signed_token = net_decl->GetSignedType() ; // returns VERI_SIGNED, VERI_UNSIGNED

        VeriRange *range = net_decl->GetRange() ; // Get the range, if it exists
        // Traverse the range (if you wish)
        CustomTraverseExpr(range) ;

        VeriStrength *strength = net_decl->GetStrength() ; // Get net strength, if it exists
        // Traverse the strength (if you wish)
        CustomTraverseMisc(strength) ;

        Array *delays = net_decl->GetDelay() ; // Get array of delays, if they exist
        // Iterate through all ids that pertain to this Ansi decl
        VeriIdDef *id_def ;
        unsigned j ;
        FOREACH_ARRAY_ITEM(net_decl->GetIds(), j, id_def) {
            if (!id_def) continue ;
            const char *name = id_def->Name() ;
            // Here you can put more code to get more info about the id_def ;
            CustomTraverseIdDef(id_def) ;
        }
        break ;
        }
    case ID_VERIALWAYSCONSTRUCT:
        { // Always construct
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem-> VeriAlwaysConstruct. Avoid dynamic_cast overhead.
        VeriAlwaysConstruct *always_stmt = static_cast<VeriAlwaysConstruct*>(module_item) ;

        VeriStatement *stmt = always_stmt->GetStmt() ; // Get the statement
        CustomTraverseStmt(stmt) ;
        break ;
        }
    case ID_VERIINITIALCONSTRUCT:
        { // Initial construct
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem-> VeriInitialConstruct. Avoid dynamic_cast overhead.
        VeriInitialConstruct *initial_stmt = static_cast<VeriInitialConstruct*>(module_item) ;

        VeriStatement *stmt = initial_stmt->GetStmt() ; // Get the statement
        CustomTraverseStmt(stmt) ;
        break ;
        }
    case ID_VERIFUNCTIONDECL:
        {  // Function declaration
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem-> VeriFunctionDecl. Avoid dynamic_cast overhead.
        VeriFunctionDecl *func_decl = static_cast<VeriFunctionDecl*>(module_item) ;

        // Get the function id
        VeriIdDef *func_id = func_decl->GetFunctionId() ;
        // NOTE: you don't want to traverse the function id again, because that is
        // how execution got here in the first place.  If you traverse it now,
        // an infinite loop will occur.

        // Get the (return) data type of this function :
        VeriDataType *data_type = func_decl->GetDataType() ; // VeriDataType contains the 'type' (token VERI_REG, VERI_WIRE, etc ...), array dimension(s), and 'signing'. If there.
        // Traverse the data type (... if you like)
        CustomTraverseExpr(data_type) ;

        // Get array of ports (VeriAnsiPortDecl)
        unsigned i ;
        Array *port_decls = func_decl->GetAnsiIOList() ; // list of formal ansi-port decls (id's (with direction,datatype etc))
        VeriExpression *decl ;
        FOREACH_ARRAY_ITEM(port_decls, i, decl) {
            CustomTraverseExpr(decl) ;
        }

        // Get declarations (array of VeriModuleItems)
        Array *decls = func_decl->GetDeclarations() ;
        VeriModuleItem *module_item ;
        FOREACH_ARRAY_ITEM(decls, i, module_item) {
            CustomTraverseModuleItem(module_item) ;
        }

        // Get the function (body) statements
        Array *stmts = func_decl->GetStatements() ;
        VeriStatement *stmt ;
        FOREACH_ARRAY_ITEM(stmts, i, stmt) {
            CustomTraverseStmt(stmt) ;
        }

        // One more piece of info (a token)
        unsigned is_automatic = func_decl->GetAutomaticType() ;         // 0 for Verilog 95.  VERI_AUTOMATIC for Verilog 2000
        break ;
        }
    case ID_VERIMODULEINSTANTIATION:
        { // Module instantiation
          // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem-> VeriFunctionDecl. Avoid dynamic_cast overhead.
        VeriModuleInstantiation *mod_inst = static_cast<VeriModuleInstantiation*>(module_item) ;

        // eg. : test(i, j) // where test is the name of a module

        const char *mod_name =  mod_inst->GetModuleName() ;

        VeriModule *module = mod_inst->GetInstantiatedModule() ; // Get the instantiated module

        VeriName *instantiated_unit = mod_inst->GetModuleNameRef() ; // unresolved instantiated module name (resolving to VeriIdDef* happens during elaboration)

        unsigned j ;
        VeriInstId *inst_id ;
        FOREACH_ARRAY_ITEM(mod_inst->GetIds(), j, inst_id) {
            if (!inst_id) continue ;
            VeriRange *range = inst_id->GetRange() ; // Get the instantiation range
            Array *port_conn_arr = inst_id->GetPortConnects() ; // Get an array of port connects
            VeriExpression *expr ;
            unsigned k ;
            FOREACH_ARRAY_ITEM(port_conn_arr, k, expr) {
                CustomTraverseExpr(expr) ;    // Traverse (if you like ...)
            }
            const char *inst_name = inst_id->InstName() ; // Get the name of the instance
        }
        break ;
        }
    case ID_VERIPATHDECL:
        { // For path declarations
        VeriPathDecl *path_decl = static_cast<VeriPathDecl*>(module_item) ;
        // eg. :
        // (A => Q) = 10 ; // Parallel connection
        // (C, D *> Q) = 18 ; // Full connection

        Array *arr_delay = path_decl->GetDelay() ; // Array of VeriExpression*. The path-delay-value : (<delay1>,<delay2>,...)
        unsigned j ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(arr_delay, j, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you like ...
        }

        VeriExpression *cond = path_decl->GetCondition() ; // Non-zero if state-dependent path
        CustomTraverseExpr(cond) ;    // Traverse (if you like ...)

        VeriPath *path = path_decl->GetPath() ; // Get the path
        CustomTraverseMisc(path) ; // Traverse if you want
        break ;
        }
    case ID_VERITABLE:
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriTable. Avoid dynamic_cast overhead.
        { // For table

        // As table resides in UDPs only and here we are dealing with top level
        // modules so the control is never going to reach here unless we process
        // UDPs as well instead of only one top level module

        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem-> VeriTable. Avoid dynamic_cast overhead.
        VeriTable *table = static_cast<VeriTable*>(module_item) ;
        // eg. :
        // table
        // control dataA  dataB   mux
        //    0      1      0 :    1 ;
        //    0      1      1 :    1 ;
        // endtable

        Array *table_entries = table->GetTableEntries() ; // Array of char* rows in table
        unsigned j ;
        char *entry ;
        FOREACH_ARRAY_ITEM(table_entries, j, entry) {
            if (!entry) continue ;
            // Insert code to obtain more information about the table entries
        }
        VeriModule *module = table->GetTheModule() ; // Back-pointer to the module in which the table appears
        break ;
        }
    case ID_VERICONTINUOUSASSIGN:
        { // For continuous assignments
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem-> VeriContinuousAssign. Avoid dynamic_cast overhead.
        VeriContinuousAssign *cont_assign = static_cast<VeriContinuousAssign*>(module_item) ;

        VeriStrength *strength = cont_assign->GetStrength() ;
        CustomTraverseMisc(strength) ; // Traverse if you want ...

        unsigned j ;
        VeriExpression *expr ;
        Array *delay_arr = cont_assign->GetDelay() ; // Get an array of delay expressions
        FOREACH_ARRAY_ITEM(delay_arr, j, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want ...
            // Insert code to obtain more information about the delay expressions
        }

        Array *net_assign_arr = cont_assign->GetNetAssigns() ; // Get an array of VeriNetAssigns
        VeriNetRegAssign *net_reg_assign ;
        FOREACH_ARRAY_ITEM(net_assign_arr, j, net_reg_assign) {
            CustomTraverseMisc(net_reg_assign) ;
        }
        break ;
        }
    case ID_VERISPECIFYBLOCK:
        {
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem-> VeriContinuousAssign. Avoid dynamic_cast overhead.
        VeriSpecifyBlock *spec_blk = static_cast<VeriSpecifyBlock*>(module_item) ;
        // eg. :
        // specify
        //     specparam tRise_clk_q = 150, tFall_clk_q = 200 ;
        //     specparam tSetup = 70 ;
        //     (clk => q) = (tRise_clk_q, tFall_clk_q) ;
        //     $setup(d, posedge clk, tSetup) ;
        // endspecify

        Array *spec_arr = spec_blk->GetSpecifyItems() ; // Gets the array of specify items
        unsigned j ;
        VeriModuleItem *spec_item ;
        FOREACH_ARRAY_ITEM(spec_arr, j, spec_item) {
            CustomTraverseModuleItem(spec_item) ; // Traverse if you want ...
        }
        break ;
        }
    case ID_VERITASKDECL:
        {
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem-> VeriTaskDecl. Avoid dynamic_cast overhead.
        VeriTaskDecl *task_decl = static_cast<VeriTaskDecl*>(module_item) ;
        // eg. :
        // task mytask2 ;
        //     output x ;
        //     input y ;
        //     int x ;
        //     logic y ;
        //     ...
        // endtask

        VeriIdDef *task_id = task_decl->GetTaskId() ; // Gets the task id

        Array *task_port_arr = task_decl->GetAnsiIOList() ; // Array of VeriAnsiPortDecl*
        unsigned j ;
        VeriExpression *decl ;
        FOREACH_ARRAY_ITEM(task_port_arr, j, decl) {
            CustomTraverseExpr(decl) ; // Traverse if you want ...
        }

        Array *task_decl_arr = task_decl->GetDeclarations() ; // Gets an array of VeriModuleItem
        VeriModuleItem *decl_item ;
        FOREACH_ARRAY_ITEM(task_decl_arr, j, decl_item) {
            CustomTraverseModuleItem(decl_item) ; // Traverse if you want ...
        }

        Array *task_stmt_arr = task_decl->GetStatements() ; // Gets an array of statement
        VeriStatement *stmt ;
        FOREACH_ARRAY_ITEM(task_stmt_arr, j, stmt) {
            CustomTraverseStmt(stmt) ; // Traverse if you want ...
        }
        break ;
        }
    case ID_VERIDEFPARAM:
        {
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriDefParam. Avoid dynamic_cast overhead.
        VeriDefParam *def_param = static_cast<VeriDefParam*>(module_item) ;
        // eg. :
        // defparam f1.A = 3.1415 ;

        Array *def_arr = def_param->GetDefParamAssigns() ; // Array of VeriDefParamAssign*. The assignments to an (external) parameter, from a local expression
        VeriDefParamAssign *def_item ;
        unsigned j ;
        FOREACH_ARRAY_ITEM(def_arr, j, def_item) {
            CustomTraverseMisc(def_item) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERIGATEINSTANTIATION:
        { // For gate instantiations
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriGateInstantiation. Avoid dynamic_cast overhead.
        VeriGateInstantiation *gate_inst = static_cast<VeriGateInstantiation*>(module_item) ;
        // eg. :
        // nor (highz1,strong0) n1(out1,in1,in2) ;

        // The different values that inst_type can take up are as follows:
        // VERI_ANDD, VERI_NAND, VERI_OR, VERI_NOR, VERI_XOR, VERI_XNOR,
        // VERI_BUF, VERI_NOT, VERI_BUFIF0, VERI_BUFIF1, VERI_NOTIF0,
        // VERI_NOTIF1, VERI_NMOS, VERI_PMOS, VERI_RNMOS, VERI_RPMOS,
        // VERI_TRAN, VERI_RTRAN, VERI_TRANIF0,VERI_TRANIF1, VERI_RTRANIF0
        // VERI_RTRANIF1, VERI_CMOS, VERI_RCMOS, VERI_PULLUP, VERI_PULLDOWN
        unsigned inst_type = gate_inst->GetInstType() ; // Gets the instantiation type

        VeriStrength * gate_str = gate_inst->GetStrength() ;
        CustomTraverseMisc(gate_str) ; // Traverse if you want...

        Array *delay_arr = gate_inst->GetDelay() ; // Gets array of delay expressions
        unsigned j ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(delay_arr, j, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want...
        }

        Array *inst_arr = gate_inst->GetInstances() ; // Gets array of VeriInst
        VeriInstId *inst_id ;
        FOREACH_ARRAY_ITEM(inst_arr, j, inst_id) {
            if (!inst_id) continue ;
            VeriRange *range = inst_id->GetRange() ; // Get the instantiation range
            Array *port_conn_arr = inst_id->GetPortConnects() ; // Get an array of port connects
            VeriExpression *expr ;
            unsigned k ;
            FOREACH_ARRAY_ITEM(port_conn_arr, k, expr) {
                CustomTraverseExpr(expr) ;    // Traverse (if you like ...)
            }
            const char *inst_name = inst_id->InstName() ; // Get the name of the instance
        }
        break ;
        }
    case ID_VERISYSTEMTIMINGCHECK:
        { // For system check functions
        VeriSystemTimingCheck *sys_timing_chk = static_cast<VeriSystemTimingCheck*>(module_item) ;
        // eg.:
        // $timeskew (posedge CP &&& MODE, negedge CPN, 50) ;

        char *task_name = sys_timing_chk->GetTaskName() ;

        Array *arg_arr = sys_timing_chk->GetArgs() ; // Gets an array of VeriExpressions
        unsigned j ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(arg_arr, j, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want...
        }
        break ;
        }
// System Verilog specific constructs
    case ID_VERICLOCKINGDECL:
        { // For clocking declarations
        VeriClockingDecl *clocking_decl = static_cast<VeriClockingDecl*>(module_item) ;
        // eg.:
        // clocking bus @(posedge clock1) ;
        //     default input #10ns output #2ns ;
        //     input data, ready, enable = top.mem1.enable ;
        //     output negedge ack ;
        //     input #1step addr ;
        // endclocking

        VeriIdDef *id = clocking_decl->GetId() ; // Get the id of the decl

        VeriExpression *clock_expr = clocking_decl->GetClockingEvent() ; // Gets the clocking expression

        Array *clocking_item_arr = clocking_decl->GetClockingItems() ; // Gets the clocking items
        unsigned j ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(clocking_item_arr, j, item) {
            CustomTraverseModuleItem(item) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERIINTERFACE:
        { // For interface
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriInterface. Avoid dynamic_cast overhead.
        VeriInterface *interface = static_cast<VeriInterface*>(module_item) ;
        // eg.:
        // interface bus_A (input clk) ;
        //     logic [15:0] data ;
        //     logic write ;
        // endinterface

        VeriIdDef *id = interface->GetId() ; // Get the id of the interface

        Array *interface_parameter_arr = interface->GetParameterConnects() ;
        unsigned j ;
        VeriModuleItem *param_decl ;
        FOREACH_ARRAY_ITEM(interface_parameter_arr, j, param_decl) {
            CustomTraverseModuleItem(param_decl) ; // Traverse if you want...
            if (!param_decl) continue ;
            // Insert code to obtain more information about the interface parameters
        }

        Array *interface_port_arr = interface->GetPortConnects() ; // Gets an array of VeriAnsiPortDecl/VeriIdRef/VeriIndexedId/VeriConcat
        VeriExpression *port_expr ;
        FOREACH_ARRAY_ITEM(interface_port_arr, j, port_expr) {
            CustomTraverseExpr(port_expr) ; // Traverse if you want...
        }

        Array *interface_item_arr = interface->GetItems() ; // Gets the interface items
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(interface_item_arr, j, item) {
            CustomTraverseModuleItem(item) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERIMODPORTDECL:
        { // For modport declarations
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriModportDecl. Avoid dynamic_cast overhead.
        VeriModportDecl *modport_decl = static_cast<VeriModportDecl*>(module_item) ;
        // eg.:
        // interface bus_A (input clk) ;
        //     logic [15:0] data ;
        //     logic write ;
        //     modport test (input data, output write) ;
        //     modport dut (output data, input write) ;
        // endinterface

        VeriIdDef *id = modport_decl->GetId() ; // Get the prototype id's with a direction

        Array *modport_decl_arr = modport_decl->GetModportPortDecls() ; // Gets the clockign items
        unsigned j ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(modport_decl_arr, j, item) {
            CustomTraverseModuleItem(item) ;  // Traverse if you want...
        }

        VeriIdDef *port ;
        Array *modport_port_arr = modport_decl->GetPorts() ;  // Gets an array of ports
        FOREACH_ARRAY_ITEM(modport_port_arr, j, port) {
            CustomTraverseIdDef(port) ; // Traverse if you want...
        }
        break ;
        }
   case ID_VERIPROGRAM:
        { // For programs
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriProgram. Avoid dynamic_cast overhead.
        VeriProgram *program = static_cast<VeriProgram*>(module_item) ;
        // eg.:
        // program test (input clk, input [16:1] addr, inout [7:0] data) ;
        // ...
        // endprogram

        VeriIdDef *prog_id = program->GetId() ; // Gets the id of the decl

        unsigned j ;
        Array *param_arr = program->GetParameterConnects() ; // Gets an array of VeriDataDecl (a VeriModuleItem)
        VeriModuleItem *param ;
        FOREACH_ARRAY_ITEM(param_arr, j, param) {
            CustomTraverseModuleItem(param) ;  // Traverse if you want to ...
        }

        VeriExpression *port_expr ;
        Array *port_arr = program->GetPortConnects() ;  // Gets an array of VeriAnsiPortDecl/VeriIdRef/VeriIndexedId/VeriConcat
        FOREACH_ARRAY_ITEM(port_arr, j, port_expr) {
            CustomTraverseExpr(port_expr) ;  // Traverse if you want to ...
        }

        Array *prog_item_arr = program->GetItems() ; // Gets the program items
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(prog_item_arr, j, item) {
            CustomTraverseModuleItem(item) ; // Traverse if you want to ...
        }
        break ;
        }
   case ID_VERICLASS:
        { // For class
        VeriClass *class_decl = static_cast<VeriClass*>(module_item) ;
        // eg. :
        // class test ;
        // ...
        // endclass

        VeriIdDef *class_id = class_decl->GetId() ; // Gets the id of the class

        Array *param_conn_arr = class_decl->GetParameterConnects() ; // declaration of parameters of this class.. Array of VeriModuleItem
        unsigned j ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(param_conn_arr, j, item) {
            CustomTraverseModuleItem(item) ; // Traverse if you want...
        }

        VeriName *veri_base_name = class_decl->GetBaseClassName() ; // Gets the name of the base class
        CustomTraverseExpr(veri_base_name) ; // Traverse if you want...

        Array *item_arr = class_decl->GetItems() ; // Gets class items (declarations etc). Array of VeriModuleItem
        FOREACH_ARRAY_ITEM(item_arr, j, item) {
            CustomTraverseModuleItem(item) ; // Traverse if you want to ...
        }
        break ;
        }
    case ID_VERIPROPERTYDECL:
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriPropertyDecl. Avoid dynamic_cast overhead.
        { // For modproperty declarations
        VeriPropertyDecl *property_decl = static_cast<VeriPropertyDecl*>(module_item) ;

        // eg. :
        // property data_end ;
        //     @(posedge mclk)
        //     data_phase |-> ((irdy==0) && ($fell(trdy) || $fell(stop))) ;
        // endproperty

        VeriIdDef *prop_id = property_decl->GetId() ; // Gets the id of the property

        unsigned j ;
        Array *port_arr = property_decl->GetPorts() ; // Gets the array of ports of the property
        VeriIdDef *port_id ;
        FOREACH_ARRAY_ITEM(port_arr, j, port_id) {
            CustomTraverseIdDef(port_id) ; // Traverse if you want to ...
        }

        Array *decl_arr = property_decl->GetDeclarations() ; // Gets the array of declarations
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(decl_arr, j, item) {
            CustomTraverseModuleItem(item) ; // Traverse if you want to ...
        }

        VeriExpression *spec = property_decl->GetSpec() ; // Gets the (property) spec
        break ;
        }
    case ID_VERIMODPORT:
        { // For modports
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriModport. Avoid dynamic_cast overhead.
        VeriModport *modport = static_cast<VeriModport*>(module_item) ;
        // eg.:
        // interface bus_A (input clk) ;
        //     ...
        //     modport test (input data, output write) ;
        //     ...
        // endinterface

        Array *modport_decl_arr = modport->GetModportDecls() ; // Get the array of VeriModportDecl
        VeriModuleItem *modport_decl ;
        unsigned j ;
        FOREACH_ARRAY_ITEM(modport_decl_arr, j, modport_decl) {
            CustomTraverseModuleItem(modport_decl) ; // Traverse if you want to ...
        }
        break ;
        }
    case ID_VERISEQUENCEDECL:
        { // For sequence declarations
        VeriSequenceDecl *seq_decl = static_cast<VeriSequenceDecl*>(module_item) ;
        // eg. :
        // sequence s1 ;
        //     @(posedge clk) a ##1 b ##1 c ;
        // endsequence

        VeriIdDef *seq_id = seq_decl->GetId() ; // Gets the id of the property

        unsigned j ;
        Array *port_arr = seq_decl->GetPorts() ; // Gets the array of ports of the sequence
        VeriIdDef *port_id ;
        FOREACH_ARRAY_ITEM(port_arr, j, port_id) {
            CustomTraverseIdDef(port_id) ; // Traverse if you want to ...
        }

        Array *decl_arr = seq_decl->GetDeclarations() ; // Gets the array of declarations
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(decl_arr, j, item) {
            CustomTraverseModuleItem(item) ; // Traverse if you want to ...
        }

        VeriExpression *spec = seq_decl->GetSpec() ; // Gets the (property) spec
        break ;
        }
    case ID_VERIBINDDIRECTIVE:
        { // Bind directive
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriBindDirective. Avoid dynamic_cast overhead.
        VeriBindDirective *bind_dir = static_cast<VeriBindDirective*>(module_item) ;
        // eg. :
        // bind cpu fpu_props fpu_rules_1(a,b,c) ;
        //                or
        // bind cpu : cpu1, cpu2  fpu_props fpu_rules_1(a,b,c) ;

        // Get the target module name, if present
        const char *target_mod_name = bind_dir->GetTargetModuleName() ;

        unsigned i ;
        Array *target_module_inst = bind_dir->GetTargetInstanceList() ;
        VeriName *veri_hier_name ; // Stores the reference to the hierarchical module or instance path
        FOREACH_ARRAY_ITEM(target_module_inst, i, veri_hier_name) {
            if (!veri_hier_name) continue ;
            CustomTraverseExpr(veri_hier_name) ; // Traverse if you want...
        }

        VeriModuleItem *inst = bind_dir->GetInstantiation() ; // Get the instantiation of the concerned module, program or interface
        CustomTraverseModuleItem(inst) ; // Traverse if you want to ...
        break ;
        }
    case ID_VERICONSTRAINTDECL:
        { // Constraint declaration
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriConstraintDecl. Avoid dynamic_cast overhead.
        VeriConstraintDecl *constr_decl = static_cast<VeriConstraintDecl*>(module_item) ;
        // eg. :
        // constraint XYPair::c { x < y ; }

        VeriIdDef *constr_id = constr_decl->GetId() ; // Get the id of the constraint

        Array *constr_blk_arr = constr_decl->GetConstraintBlocks() ; // Get the array of expressions
        unsigned j ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(constr_blk_arr, j, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERIGENERATECONSTRUCT:
        {
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriGenerateConstruct. Avoid dynamic_cast overhead.
        VeriGenerateConstruct *gen_constr = static_cast<VeriGenerateConstruct*>(module_item) ;
        Array *gen_items = gen_constr->GetItems() ; // Gets the generate items

        // Iterate through all ids that pertain to this generate construct
        VeriModuleItem *mod_item ;
        unsigned j, k ;
        FOREACH_ARRAY_ITEM(gen_items, j, mod_item) {
            CustomTraverseModuleItem(mod_item) ; // Traverse if you want to ...
        }
        break ;
        }
    case ID_VERIGENERATEFOR: { // For generate for
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriGenerateFor. Avoid dynamic_cast overhead.
        VeriGenerateFor *gen_for = static_cast<VeriGenerateFor*>(module_item) ;

        VeriModuleItem *initial = gen_for->GetInitial() ; // Gets a VeriGenVarAssign only for pure Verilog 2000 (not for SV)
        CustomTraverseModuleItem(initial) ; // Traverse if you want...

        VeriExpression *cond = gen_for->GetCondition() ; // Gets the condition
        CustomTraverseExpr(cond) ; // Traverse if you want...

        VeriModuleItem *repetition = gen_for->GetRepetition() ; // Gets a VeriGenVarAssign only for pure Verilog 2000 (not for SV)
        CustomTraverseModuleItem(initial) ; // Traverse if you want...

        VeriIdDef *blk_id = gen_for->GetBlockId() ; // Get the 'generate for block' id
        CustomTraverseIdDef(blk_id) ; // Traverse if you want...

        Array *item_arr = gen_for->GetItems() ; // Gets an array of VeriModuleItem*. The generate items
        VeriModuleItem *item ;
        unsigned j ;
        FOREACH_ARRAY_ITEM(item_arr, j, item) {
        CustomTraverseModuleItem(initial) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERIGENERATECASE:
        { // For generate case
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriGenerateCase. Avoid dynamic_cast overhead.
        VeriGenerateCase *gen_case = static_cast<VeriGenerateCase*>(module_item) ;

        VeriGenerateCaseItem *case_item ;
        unsigned k ;
        FOREACH_ARRAY_ITEM(gen_case->GetCaseItems(), k, case_item) {
            CustomTraverseMisc(case_item) ; // Traverse if you want to ...
        }
        break ;
        }
    case ID_VERIGENERATECONDITIONAL:
        { // For generate conditional
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriGenerateConditional. Avoid dynamic_cast overhead.
        VeriGenerateConditional *gen_conditional = static_cast<VeriGenerateConditional*>(module_item) ;

        VeriExpression *if_expr = gen_conditional->GetIfExpr() ; // Gets the condition
        CustomTraverseExpr(if_expr) ; // Traverse if you want...

        VeriModuleItem *then_mod_item = gen_conditional->GetThenItem() ; // Get the items following the 'if' condition
        CustomTraverseModuleItem(then_mod_item) ; // Traverse if you want...

        VeriModuleItem *else_mod_item = gen_conditional->GetElseItem() ; // Get the items following the 'else' condition
        CustomTraverseModuleItem(else_mod_item) ; // Traverse if you want...
        break ;
        }
    case ID_VERIGENERATEBLOCK: { // For generate block
        // ------- Look in "VeriModuleItem.h" for class description of this object -------
        // Downcast VeriModuleItem->VeriGenerateBlock. Avoid dynamic_cast overhead.
        VeriGenerateBlock *gen_blk = static_cast<VeriGenerateBlock*>(module_item) ;

        VeriIdDef *gen_blk_id = gen_blk->GetBlockId() ; // Get the id of the generate block

        Array *gen_blk_item_arr = gen_blk->GetItems() ; // Get the items within a generate block
        VeriModuleItem *gen_blk_item ;
        unsigned k ;
        FOREACH_ARRAY_ITEM(gen_blk_item_arr, k, gen_blk_item) {
            CustomTraverseModuleItem(gen_blk_item) ; // Traverse if you want to ...
        }
        break ;
        }
        // VeriStatement has been derived from VeriModuleItem. Now a verimoduleitem can also be a statement.
        // eg.:
        // for (int i =0; i<size; i++)  // in this case 'int i = 0 ;' is a VeriModuleItem
        // for (i =0; i<size; i++) // in this case however 'i=0' is a VeriStatement
        // This default case covers all those module items which can be statements
        case ID_VERIBLOCKINGASSIGN:
        case ID_VERINONBLOCKINGASSIGN:
        case ID_VERIGENVARASSIGN:
        case ID_VERIASSIGN:
        case ID_VERIDEASSIGN:
        case ID_VERIFORCE:
        case ID_VERIRELEASE:
        case ID_VERITASKENABLE:
        case ID_VERISYSTEMTASKENABLE:
        case ID_VERIDELAYCONTROLSTATEMENT:
        case ID_VERIEVENTCONTROLSTATEMENT:
        case ID_VERICONDITIONALSTATEMENT:
        case ID_VERICASESTATEMENT:
        case ID_VERIFOREVER:
        case ID_VERIREPEAT:
        case ID_VERIWHILE:
        case ID_VERIFOR:
        case ID_VERIWAIT:
        case ID_VERIDISABLE:
        case ID_VERIEVENTTRIGGER:
        case ID_VERISEQBLOCK:
        case ID_VERIPARBLOCK:
        case ID_VERIASSERTION:
        case ID_VERIJUMPSTATEMENT:
        case ID_VERIDOWHILE:
        case ID_VERIFOREACH:
        case ID_VERIWAITORDER:
        case ID_VERIARRAYMETHODCALL:
        case ID_VERIINLINECONSTRAINTSTMT:
        case ID_VERIRANDSEQUENCE:
        case ID_VERICODEBLOCK:
        {
        // Downcast VeriModuleItem->VeriStatement. Avoid dynamic_cast overhead.
        VeriStatement *stmt = static_cast<VeriStatement*>(module_item) ;
        CustomTraverseStmt(stmt) ; // Traverse if you want...
        break ;
        }
        // This should not get triggered!
        default: Message::Error(module_item->Linefile(), "unexpected module item found") ;
    }
}

/* ------------------------------------------------------- */

static void CustomTraverseStmt(VeriStatement *statement)
{
    if (!statement) return ;

    // ---------------------------------------------------------------------------------------------------------
    // The following are all the types of VeriStatements:
    // ---------------------------------------------------------------------------------------------------------
    //    VeriAssertion                 VeriConditionalStatement        VeriCaseStatement     VeriAssign
    //    VeriBlockingAssign            VeriDelayControlStatement       VeriDisable           VeriForeach
    //    VeriDeAssign                  VeriEventTrigger                VeriFor               VeriWaitOrderer
    //    VeriEventControlStatement     VeriForever                     VeriGenVarAssign      VeriArrayMethodCall
    //    VeriForce                     VeriParBlock                    VeriRelease           VeriInlineConstraintStmt
    //    VeriNonBlockingAssign         VeriSeqBlock                    VeriStatement         VeriRandsequence
    //    VeriRepeat                    VeriTaskEnable                  VeriWait              VeriCodeBlock
    //    VeriSystemTaskEnable          VeriWhile                       VeriDoWhile
    // ---------------------------------------------------------------------------------------------------------
    switch(statement->GetClassId()) {
    case ID_VERIASSERTION:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement-> VeriAssertion. Avoid dynamic_cast overhead.
        VeriAssertion *assertion_stmt = static_cast<VeriAssertion*>(statement) ;
        // eg. :
        // assert(foo) $display("%m passed") ; else $display("%m failed") ;

        // 'cover_property' is 0 if this is an immediate assertion, or VERI_ASSERT / VERI_COVER
        // if this is a (concurrent) 'assert property' or 'cover property' statement
        unsigned cover_property = assertion_stmt->GetAssertCoverProperty() ;

        VeriExpression *prop_spec = assertion_stmt->GetPropertySpec() ; // Get the property spec
        CustomTraverseExpr(prop_spec) ; // Traverse if you want...

        VeriStatement *then_stmt = assertion_stmt->GetThenStmt() ; // Get the property true statement
        CustomTraverseStmt(then_stmt) ; // Traverse if you want...

        VeriStatement *else_stmt = assertion_stmt->GetElseStmt() ; // Get the property else statement
        CustomTraverseStmt(else_stmt) ; // Traverse if you want...
        break ;
        }
    case ID_VERIJUMPSTATEMENT:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriJumpStatement. Avoid dynamic_cast overhead.
        VeriJumpStatement *jump_stmt = static_cast<VeriJumpStatement*>(statement) ;
        // eg :
        // return ret_val ; // returns value from a function
        // break ; // used to break out of a loop
        // continue ; // used to break out of a loop

        unsigned jump_token = jump_stmt->GetJumpToken() ; // Can take up the values VERI_RETURN, VERI_BREAK or VERI_CONTINU

        VeriExpression *returned_expr = jump_stmt->GetExpr() ; // Get the returned expression in case of a return statement
        CustomTraverseExpr(returned_expr) ; // Traverse if you want...

        VeriIdDef *id = jump_stmt->GetJumpId() ; // Get the id of the (loop label or subprogram) to which this statement jumps
        CustomTraverseIdDef(id) ; // Traverse if you want...
        break ;
        }
    case ID_VERIDOWHILE:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriRelease. Avoid dynamic_cast overhead.
        VeriDoWhile *do_while_stmt = static_cast<VeriDoWhile*>(statement) ;

        VeriExpression *cond = do_while_stmt->GetCondition() ; // Gets the condition
        CustomTraverseExpr(cond) ; // Traverse if you want...

        VeriStatement *do_while_content = do_while_stmt->GetStmt() ; // Gets 'do while' statement
        CustomTraverseStmt(do_while_content) ; // Traverse if you want...
        break ;
        }
    case ID_VERIASSIGN:
        {
        VeriAssign *assign_stmt = static_cast<VeriAssign*>(statement) ;
        // eg. :
        // r = a ;

        VeriNetRegAssign *net_reg_assign = assign_stmt->GetAssign() ;
        CustomTraverseMisc(assign_stmt) ; // Traverse if you want...
        break ;
        }
    case ID_VERIEVENTCONTROLSTATEMENT:
        { // Event control statement
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement-> VeriEventControlStatement. Avoid dynamic_cast overhead.
        VeriEventControlStatement *event_control = static_cast<VeriEventControlStatement*>(statement) ;

        // eg. :
        // Get "@(posedge clk)"

        unsigned j ;
        VeriExpression *expr ;
        Array *at_ids = event_control->GetAt() ;
        FOREACH_ARRAY_ITEM(at_ids, j, expr) {
            CustomTraverseExpr(expr) ;
        }
        // Get event statement:
        VeriStatement *event_stmt = event_control->GetStmt() ;
        // Now Traverse event statement
        CustomTraverseStmt(event_stmt) ;
        break ;
        }
    case ID_VERICONDITIONALSTATEMENT:
        {  // Conditional statement
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement-> VeriConditionalStatement. Avoid dynamic_cast overhead.
        VeriConditionalStatement *if_stmt = static_cast<VeriConditionalStatement*>(statement) ;

        // Get the condition
        VeriExpression *if_expr = if_stmt->GetIfExpr() ;
        CustomTraverseExpr(if_expr) ; // Traverse if you want...

        // Get the true statement
        VeriStatement *then_stmt = if_stmt->GetThenStmt() ;
        CustomTraverseStmt(then_stmt) ; // Traverse if you want...

        // Get the else statement
        VeriStatement *else_stmt = if_stmt->GetElseStmt() ;
        CustomTraverseStmt(else_stmt) ; // Traverse if you want...
        break ;
        }
    case ID_VERISEQBLOCK:
        {   // Sequential block
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement-> VeriSeqBlock. Avoid dynamic_cast overhead.
        VeriSeqBlock *seq_block = static_cast<VeriSeqBlock*>(statement) ;

        // Get a handle to the block declarations
        unsigned j ;
        Array *block_decl_arr = seq_block->GetDeclItems() ;
        VeriModuleItem *decl ;
        FOREACH_ARRAY_ITEM(block_decl_arr, j, decl) {
            CustomTraverseModuleItem(decl) ; // Traverse if you want...
        }

        // Get a handle to the block statements
        Array *statements = seq_block->GetStatements() ;
        VeriStatement *stmt ;
        FOREACH_ARRAY_ITEM(statements, j, stmt) {
            CustomTraverseStmt(stmt) ; // Traverse if you want...
        }

        // Get a handle to the block's scope
        VeriScope *scope = seq_block->GetScope() ;  // It should have a scope
        // More VeriScope discussion toward the end of the main() function.
        break ;
        }
    case ID_VERICASESTATEMENT:
        { // Cass statement
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement-> VeriCaseStatement. Avoid dynamic_cast overhead.
        VeriCaseStatement *case_stmt = static_cast<VeriCaseStatement*>(statement) ;

        // Check the user-defined pragma's on this case statement
        unsigned is_full_case = case_stmt->IsFullCase() ;
        unsigned is_parallel_case = case_stmt->IsParallelCase() ;

        // Get some info:
        unsigned case_style = case_stmt->GetCaseStyle() ; // VERI_CASE, VERI_CASEX, VERI_CASEZ..

        // the case expression
        VeriExpression *condition = case_stmt->GetCondition() ;
        // Traverse the conditin (if you wish...)
        CustomTraverseExpr(condition) ;

        // Iterate over all case items
        Array *items = case_stmt->GetCaseItems() ;
        unsigned i ;
        VeriCaseItem *item ;
        FOREACH_ARRAY_ITEM(items, i, item) {
            CustomTraverseMisc(item) ;
        }
        break ;
        }
    case ID_VERIBLOCKINGASSIGN:
        {    // Blocking assignment
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement-> VeriBlockingAssign. Avoid dynamic_cast overhead.
        VeriBlockingAssign *assign_stmt = static_cast<VeriBlockingAssign*>(statement) ;

        VeriExpression *lhs_expr = assign_stmt->GetLVal() ; // Get LHS
        CustomTraverseExpr(lhs_expr) ; // Traverse if you want...

        VeriDelayOrEventControl *control = assign_stmt->GetControl() ; // Get Delay or event control (if it exist)
        CustomTraverseMisc(control) ; // Traverse (if you want...)

        VeriExpression *rhs_expr = assign_stmt->GetValue() ; // Get Value
        CustomTraverseExpr(rhs_expr) ; // Traverse (if you want...)
        break ;
        }
    case ID_VERINONBLOCKINGASSIGN:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriNonBlockingAssign. Avoid dynamic_cast overhead.
        VeriBlockingAssign *blocking_stmt = static_cast<VeriBlockingAssign*>(statement) ;

        // eg. :
        // r <= a ;

        VeriExpression *lval = blocking_stmt->GetLVal() ; // Gets the LHS value
        CustomTraverseExpr(lval) ; // Traverse if you want...

        VeriDelayOrEventControl *del_or_evt_ctrl = blocking_stmt->GetControl() ; // Gets the delay or event control expression
        CustomTraverseMisc(del_or_evt_ctrl) ; // Traverse if you want...

        VeriExpression *val = blocking_stmt->GetValue() ; // Gets the value
        CustomTraverseExpr(val) ; // Traverse if you want...
        break ;
        }
    case ID_VERIDEASSIGN:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriDeAssign. Avoid dynamic_cast overhead.
        VeriDeAssign *deassign_stmt = static_cast<VeriDeAssign*>(statement) ;
        // eg. :
        // deassign q ;

        VeriExpression *lval = deassign_stmt->GetLVal() ;
        CustomTraverseExpr(lval) ; // Traverse if you want...
        break ;
        }
    case ID_VERIDISABLE:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriDisable. Avoid dynamic_cast overhead.
        VeriDisable *disable_stmt = static_cast<VeriDisable*>(statement) ;
        // eg. :
        // disable block_name ;

        VeriName *task_blk_name = disable_stmt->GetTaskBlockName() ; // Get the task or block name
        CustomTraverseExpr(task_blk_name) ; // Traverse if you want...
        break ;
        }
    case ID_VERIDELAYCONTROLSTATEMENT:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriDelayControlStatement. Avoid dynamic_cast overhead.
        VeriDelayControlStatement *delay_ctrl_stmt = static_cast<VeriDelayControlStatement*>(statement) ;

        // eg. :
        // #10 d = (a | b | c) ;

        VeriExpression *delay = delay_ctrl_stmt->GetDelay() ; // Get the delay
        CustomTraverseExpr(delay) ; // Traverse if you want...

        VeriStatement *stmt = delay_ctrl_stmt->GetStmt() ; // Get the delay statement
        CustomTraverseStmt(stmt) ; // Traverse if you want...
        break ;
        }
    case ID_VERIFORCE:
        {
        VeriForce *force_stmt = static_cast<VeriForce*>(statement) ;
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriForce. Avoid dynamic_cast overhead.
        // eg. :
        // force d = (a | b | c) ;

        VeriNetRegAssign *net_reg_assign = force_stmt->GetAssign() ;
        if (net_reg_assign) {
            VeriExpression *lval_expr = net_reg_assign->GetLValExpr() ; // Gets the LHS value
            CustomTraverseExpr(lval_expr) ; // Traverse if you want...
            VeriExpression *rval_expr = net_reg_assign->GetRValExpr() ; // Gets the RHS value
            CustomTraverseExpr(rval_expr) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERIEVENTTRIGGER:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriEventTrigger. Avoid dynamic_cast overhead.
        VeriEventTrigger *evt_trigger = static_cast<VeriEventTrigger*>(statement) ;
        // eg. :
        // -> trig // trigger is an event

        VeriName *evt_trigger_name = evt_trigger->GetEventName() ; // Gets the name
        CustomTraverseExpr(evt_trigger_name) ; // Traverse if you want...
        break ;
        }
    case ID_VERIGENVARASSIGN:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriGenVarAssign. Avoid dynamic_cast overhead.
        VeriGenVarAssign *gen_var_assign = static_cast<VeriGenVarAssign*>(statement) ;
        // eg. :
        // genvar e, i ;

        const char *gen_var_name = gen_var_assign->GetName() ; // Get the name

        VeriIdDef *gen_var_id = gen_var_assign->GetId() ; // Get the id
        CustomTraverseIdDef(gen_var_id) ; // Traverse if you want...

        VeriExpression * value = gen_var_assign->GetValue() ; // Get the value of the expression
        CustomTraverseExpr(value) ; // Traverse if you want...
        break ;
        }
    case ID_VERIFOR:
        {
        VeriFor *for_stmt = static_cast<VeriFor*>(statement) ;
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriFor. Avoid dynamic_cast overhead.
        // eg :
        // for (i=0 ; i<size ; i++)
        // begin
        // ...
        // end

        Array *initials_arr = for_stmt->GetInitials() ; // Gets the array of VeriModuleItem :the comma separated list of assignments and declarations
        unsigned j ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(initials_arr, j, item) {
            CustomTraverseModuleItem(item) ; // Traverse if you want to ...
        }

        VeriExpression *cond = for_stmt->GetCondition() ; // Gets the test-condition (to stay or leave the loop)
        CustomTraverseExpr(cond) ;  // Traverse if you want...

        Array *repetitions_arr = for_stmt->GetRepetitions() ; // Gets the array of VeriStatement : the comma separated list of assignment statements
        FOREACH_ARRAY_ITEM(repetitions_arr, j, item) {
            CustomTraverseModuleItem(item) ; // Traverse if you want...
        }

        VeriStatement *for_content = for_stmt->GetStmt() ; // Gets 'for' statement
        CustomTraverseStmt(for_content) ; // Traverse if you want...
        break ;
        }
    case ID_VERIFOREVER:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriForever. Avoid dynamic_cast overhead.
        VeriForever *forever_stmt = static_cast<VeriForever*>(statement) ;
        // eg. :
        // forever
        // begin
        // ...
        // end

        VeriStatement *forever_content = forever_stmt->GetStmt() ; // Gets 'forever' statement
        CustomTraverseStmt(forever_content) ; // Traverse if you want...
        break ;
        }
    case ID_VERIWHILE:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriWhile. Avoid dynamic_cast overhead.
        VeriWhile *while_stmt = static_cast<VeriWhile*>(statement) ;
        // eg. :
        // while (cond) {
        // ...
        // }
        VeriExpression *while_cond = while_stmt->GetCondition() ; // Gets the condition of the while loop
        CustomTraverseExpr(while_cond) ; // Traverse if you want...

        VeriStatement *while_content = while_stmt->GetStmt() ; // Gets 'while' statement
        CustomTraverseStmt(while_content) ; // Traverse if you want...
        break ;
        }
    case ID_VERIREPEAT:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriRepeat. Avoid dynamic_cast overhead.
        VeriRepeat *repeat_stmt = static_cast<VeriRepeat*>(statement) ;
        // eg. :
        // repeat (tics) @ (posedge clock) ;

        VeriExpression *repeat_cond = repeat_stmt->GetCondition() ; // Gets the condition of the while loop
        CustomTraverseExpr(repeat_cond) ; // Traverse if you want...

        VeriStatement *repeat_content = repeat_stmt->GetStmt() ; // Gets 'repeat' statement
        CustomTraverseStmt(repeat_content) ; // Traverse if you want...
        break ;
        }
    case ID_VERIRELEASE:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriRelease. Avoid dynamic_cast overhead.
        VeriRelease *release_stmt = static_cast<VeriRelease*>(statement) ;
        // eg. :
        // release var ;

        VeriExpression *lval = release_stmt->GetLVal() ; // Gets the left hand side value of the 'force' statement
        CustomTraverseExpr(lval) ; // Traverse if you want...
        break ;
        }
    case ID_VERISYSTEMTASKENABLE:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriSystemTaskEnable. Avoid dynamic_cast overhead.
        VeriSystemTaskEnable *sys_task_en_stmt = static_cast<VeriSystemTaskEnable*>(statement) ;

        const char *name = sys_task_en_stmt->GetName() ; // Get the name in char* form

        Array *args_arr = sys_task_en_stmt->GetArgs() ; // Get an array of arguments
        unsigned j ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(args_arr, j, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want to ...
        }

        unsigned func_type = sys_task_en_stmt->GetFunctionType() ; // Get (function) token for this system task. 0 if not known
        break ;
        }
    case ID_VERITASKENABLE:
        {
        VeriTaskEnable *task_en_stmt = static_cast<VeriTaskEnable*>(statement) ;

        VeriName *name = task_en_stmt->GetTaskName() ; // Gets the name
        CustomTraverseExpr(name) ; // Traverse if you want to ...

        Array *args_arr = task_en_stmt->GetArgs() ; // Array of VeriExpression*. Can contain VeriPortConnect. The task-enable arguments
        unsigned j ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(args_arr, j, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want to ...
        }
        break ;
        }
    case ID_VERIWAIT:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriWait. Avoid dynamic_cast overhead.
        VeriWait *wait_stmt = static_cast<VeriWait*>(statement) ;
        // eg. :
        // wait (!enable) #10 a = b ;

        VeriExpression *cond = wait_stmt->GetCondition() ; // Gets the wait condition
        CustomTraverseExpr(cond) ; // Traverse if you want...

        VeriStatement *wt_stmt = wait_stmt->GetStmt() ; // Gets the wait statement
        CustomTraverseStmt(wt_stmt) ; // Traverse if you want to ...
        break ;
        }
    case ID_VERIPARBLOCK:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriParBlock. Avoid dynamic_cast overhead.
        VeriParBlock *par_blk_stmt = static_cast<VeriParBlock*>(statement) ;
        // eg. :
        // fork
        // ...
        // join

        VeriIdDef *label_id = par_blk_stmt->GetLabel() ; // Gets the block label
        CustomTraverseIdDef(label_id) ; // Traverse if you want...

        Array *decl_item_arr = par_blk_stmt->GetDeclItems() ; // Gets the array of VeriModuleItem.  The declaration items^M
        unsigned j ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(decl_item_arr, j, item) {
            CustomTraverseModuleItem(item) ; // Traverse if you want...
        }

        Array *stmt_arr = par_blk_stmt->GetStatements() ; // Gets the array of VeriStatement. The block statements
        VeriStatement *par_stmt ;
        FOREACH_ARRAY_ITEM(decl_item_arr, j, par_stmt) {
            CustomTraverseStmt(par_stmt) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERIFOREACH:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriForeach. Avoid dynamic_cast overhead.
        VeriForeach *foreach_stmt = static_cast<VeriForeach*>(statement) ;
        // eg. :
        // foreach(A[i,j,k]) ...

        // Get the name of array identifier to be traversed
        VeriName *arr_name =  foreach_stmt->GetArrayName() ;
        CustomTraverseExpr(arr_name) ; // Traverse if you want...

        // Get the array of indexes
        Array *loop_indexes = foreach_stmt->GetLoopIndexes() ;
        unsigned i ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(loop_indexes, i, id) {
            CustomTraverseIdDef(id) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERIWAITORDER:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriWaitOrder. Avoid dynamic_cast overhead.
        VeriWaitOrder *wait_order_stmt = static_cast<VeriWaitOrder*>(statement) ;
        // eg. :
        // "wait_order ( <hierarchical_identifier {, hierarchical_identifier}>) <action_block> "
        //
        // action_block : <statement_or_null>
        //              | [ statement] else <statement>

        // Get the list of events to be triggered in order
        Array *events = wait_order_stmt->GetEvents() ;
        unsigned i ;
        VeriName *name ;
        FOREACH_ARRAY_ITEM(events, i, name) {
            CustomTraverseExpr(name) ; // Traverse if you want...
        }

        // Get the statement to be executed if events are triggered in prescribed order
        VeriStatement *then_stmt = wait_order_stmt->GetThenStmt() ;
        CustomTraverseStmt(then_stmt) ; // Traverse if you want...

        // Get the statement to be executed if events are not triggered in prescribed order
        VeriStatement *else_stmt = wait_order_stmt->GetElseStmt() ;
        CustomTraverseStmt(then_stmt) ; // Traverse if you want...
        break ;
        }
    case ID_VERIARRAYMETHODCALL:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriArrayMethodCall. Avoid dynamic_cast overhead.
        VeriArrayMethodCall *array_meth_call_stmt = static_cast<VeriArrayMethodCall*>(statement) ;
        // eg. :
        //  "<tf_call> with (<expression>)"

        // Get the R.H.S of the 'with' keyword
        VeriExpression *right = array_meth_call_stmt->GetRight() ;
        CustomTraverseExpr(right) ; // Traverse if you want...
        break ;
        }
    case ID_VERIINLINECONSTRAINTSTMT:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriInlineConstraintStmt. Avoid dynamic_cast overhead.
        VeriInlineConstraintStmt *inline_constr_stmt = static_cast<VeriInlineConstraintStmt*>(statement) ;
        // eg. :
        // "<tf_call> with <constraint_block>"

        // Get the constraint block
        Array *constraint_block = inline_constr_stmt->GetConstraintBlock() ;
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(constraint_block, i, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERIRANDSEQUENCE:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriRandsequence. Avoid dynamic_cast overhead.
        VeriRandsequence *randseq_stmt = static_cast<VeriRandsequence*>(statement) ;
        // eg. :
        // VeriRandsequence implements randsequence statement :
        // "randsequence ([<production_identifier>])
        //     <production> {<production>}
        //  endsequence"

        // Get the name of the production from which sequence starts
        VeriName *start_prod = randseq_stmt->GetStartingProduction() ;
        CustomTraverseExpr(start_prod) ; // Traverse if you want...

        // Get the list of productions
        Array *productions = randseq_stmt->GetProductions() ;
        unsigned i ;
        VeriProduction *prod ;
        FOREACH_ARRAY_ITEM(productions, i, prod) {
            CustomTraverseMisc(prod) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERICODEBLOCK:
        {
        // ------- Look in "VeriStatement.h" for class description of this object -------
        // Downcast VeriStatement->VeriCodeBlock. Avoid dynamic_cast overhead.
        VeriCodeBlock *code_block = static_cast<VeriCodeBlock*>(statement) ;
        // VeriCodeBlock implements code block of production  :
        //  { { <data_declarations>} { statement_or_null}}

        // Get the list of declarations
        Array *decls = code_block->GetDecls() ;
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(decls, i, item) {
            CustomTraverseModuleItem(item) ; // Traverse if you want...
        }

        // Get the list of statements
        Array *stmts = code_block->GetStmts() ;
        VeriStatement *stmt ;
        FOREACH_ARRAY_ITEM(stmts, i, stmt) {
            CustomTraverseStmt(stmt) ; // Traverse if you want...
        }
        break ;
        }
    // The control shouldn't reach here
    default: Message::Error(statement->Linefile(),"unknown statement found") ; // For the default testcase, this should never get executed.
    }
}

/* ------------------------------------------------------- */

static void CustomTraverseIdDef(VeriIdDef *id_def)
{
    if (!id_def) return ;

    // ------------------------------------------------------------------------------------------------------------------
    // The following are all the types of VeriIdDefs:
    // ------------------------------------------------------------------------------------------------------------------
    // VeriBlockId         VeriFunctionId         VeriInterfaceId        VeriProgramId             VeriProductionId
    // VeriGenVarId        VeriVariable           VeriInstId             VeriTypeId                VeriNamedPort
    // VeriModuleId        VeriParamId            VeriModportId          VeriTaskId
    // VeriTaskId          VeriUdpId              VeriClockingId         VeriCovergroupId
    // ------------------------------------------------------------------------------------------------------------------
    switch(id_def->GetClassId()) {
    case ID_VERIVARIABLE:
        {  // Variable identifier
        // ------- Look in "VeriId.h" for class description of this object -------
        // Downcast VeriIdDef-> VeriVariable. Avoid dynamic_cast overhead.
        VeriVariable *variable = static_cast<VeriVariable*>(id_def) ;

        const char *name = variable->Name() ;

        // Test the type of variable:
        unsigned decl_type = variable->Type() ;
        // ... or ...
        unsigned is_var     = variable->IsVar() ;    // Is it a variable
        unsigned is_port    = variable->IsPort() ;   // Is it a port
        unsigned is_net     = variable->IsNet() ;    // Is it a wire (net)
        unsigned is_reg     = variable->IsReg() ;    // Is it a register
        unsigned is_array   = variable->IsArray() ;

        // Some other tests can also done
        unsigned is_signed  = variable->IsSigned() ;
        unsigned can_be_onehot    = variable->CanBeOnehot() ;  // Can it be one-hot encoded

        // Get the initial value, if it exists.
        VeriExpression *expr = variable->GetInitialValue() ;
        // Traverse it if you like
        CustomTraverseExpr(expr) ;
        // View VeriId.h for the definition of VeriVariable for more info ...
        VeriRange *dim = variable->GetDimensions() ; // Gets the dimensions
        CustomTraverseExpr(dim) ; // Traverse if you want...
        break ;
        }
    case ID_VERIFUNCTIONID:
        {    // Function identifier
        // ------- Look in "VeriId.h" for class description of this object -------
        // Downcast VeriIdDef-> VeriFunctionId. Avoid dynamic_cast overhead.
        VeriFunctionId *func_id = static_cast<VeriFunctionId*>(id_def) ;

        // Get the function body
        VeriModuleItem *body = func_id->GetModuleItem() ;

        // Get the local scope
        VeriScope *scope = func_id->LocalScope() ;
        // More VeriScope discussion toward the end of the main() function.
        break ;
        }
    case ID_VERIBLOCKID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriBlockId. Avoid dynamic_cast overhead.
        VeriBlockId *blk_id = static_cast<VeriBlockId*>(id_def) ;

        VeriModuleItem *item = blk_id->GetModuleItem() ; // Get the backpointer to the tree
        break ;
        }
    case ID_VERIGENVARID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriGenVarId. Avoid dynamic_cast overhead.
        VeriGenVarId *gen_var_id = static_cast<VeriGenVarId*>(id_def) ;

        unsigned sign_flag = gen_var_id->IsSigned() ;
        break ;
        }
    case ID_VERIMODULEID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriModuleId. Avoid dynamic_cast overhead.
        VeriModuleId *module_id = static_cast<VeriModuleId*>(id_def) ;

        VeriModule *item =module_id->GetModule() ; // Get the module
        Array *port_arr = module_id->GetPorts() ; // Get array of IdDefs
        VeriIdDef *id ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(port_arr, i, id) {
            CustomTraverseIdDef(id) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERITASKID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriTaskId. Avoid dynamic_cast overhead.
        VeriTaskId *task_id = static_cast<VeriTaskId*>(id_def) ;

        VeriModule *module = task_id->GetModule() ; // Get the module

        Array *port_arr = task_id->GetPorts() ; // Get array of IdDef
        VeriIdDef *id ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(port_arr, i, id) {
            CustomTraverseIdDef(id) ; // Traverse if you want...
        }
        break ;
        }
    case ID_VERIINSTID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriInstId. Avoid dynamic_cast overhead.
        VeriInstId *inst_id = static_cast<VeriInstId*>(id_def) ;

        VeriModuleInstantiation *mod_inst = inst_id-> GetModuleInstance() ;

        VeriRange *range = inst_id->GetRange() ; // Get the instantiation range

        Array *port_conn_arr = inst_id->GetPortConnects() ; // Get an array of port connects
        VeriExpression *expr ;
        unsigned k ;
        FOREACH_ARRAY_ITEM(port_conn_arr, k, expr) {
            CustomTraverseExpr(expr) ;    // Traverse (if you like ...)
        }
        break ;
        }
    case ID_VERIPARAMID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriParamId. Avoid dynamic_cast overhead.
        VeriParamId *param_id = static_cast<VeriParamId*>(id_def) ;

        VeriDataType *data_type = param_id->GetDataType() ; // Get the data type of the parameter
        CustomTraverseExpr(data_type) ; // Traverse if you want...

        VeriExpression *init_val = param_id->GetInitialValue() ; // Get the initial value
        CustomTraverseExpr(init_val) ; // Traverse if you want...
        break ;
        }
    case ID_VERIUDPID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriUdpId. Avoid dynamic_cast overhead.
        VeriUdpId *udp_id = static_cast<VeriUdpId*>(id_def) ;
        // The control will reach here for an udp but because we are only inspecting
        // a top level module, this will not occur.

        VeriModule *item =udp_id->GetModule() ; // Get the module

        Array *port_arr = udp_id->GetPorts() ; // Get array of IdDefs
        VeriIdDef *id ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(port_arr, i, id) {
            CustomTraverseIdDef(id) ; // Traverse if you want...
        }
        break ;
        }
    // Now analyze the System Verilog file (into the work library). In case on any error do not process further.
    case ID_VERIINTERFACEID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriInterfaceId. Avoid dynamic_cast overhead.
        VeriInterfaceId *interface_id = static_cast<VeriInterfaceId*>(id_def) ;

        VeriModuleItem *item = interface_id->GetModuleItem() ;
        break ;
        }
    case ID_VERIPROGRAMID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriProgramId. Avoid dynamic_cast overhead.
        VeriProgramId *program_id = static_cast<VeriProgramId*>(id_def) ;

        VeriModuleItem *item = program_id->GetModuleItem() ;
        break ;
        }
    case ID_VERITYPEID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriTypeId. Avoid dynamic_cast overhead.
        VeriTypeId *type_id = static_cast<VeriTypeId*>(id_def) ;

        // VeriTypeId is used for two constructs:
        //  1. Forward declarations: typedef T ;
        //  2. Class decalrations: class C ; ... endclass
        // Here both T and C will be objects of VeriTypeId.
        // Please note that GetModuleItem will return a valid pointer only for the
        // class ids. It will be zero for ids that are typedefs (but not class).
        VeriModuleItem *item = type_id->GetModuleItem() ;
        break ;
        }
    case ID_VERIMODPORTID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriModportId. Avoid dynamic_cast overhead.
        VeriModportId *modport_id = static_cast<VeriModportId*>(id_def) ;

        VeriModuleItem *item = modport_id->GetModuleItem() ;
        break ;
        }
    case ID_VERIOPERATORID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriOperatorId. Avoid dynamic_cast overhead.
        VeriOperatorId *oper_id = static_cast<VeriOperatorId*>(id_def) ;

        unsigned oper_type = oper_id->GetOperatorType() ; // token representing the operator used (VERI_PLUS, VERI_MIN etc.)
        VeriModuleItem *item = oper_id->GetModuleItem() ;
        break ;
        }
    case ID_VERICLOCKINGID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriClockingId. Avoid dynamic_cast overhead.
        VeriClockingId *clocking_id = static_cast<VeriClockingId*>(id_def) ;

        VeriModuleItem *item = clocking_id->GetModuleItem() ;
        break ;
        }
    case ID_VERIPRODUCTIONID:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriProductionId. Avoid dynamic_cast overhead.
        VeriProductionId *prod_id = static_cast<VeriProductionId*>(id_def) ;

        VeriProduction *prod = prod_id->GetProduction() ;
        break ;
        }
    case ID_VERINAMEDPORT:
        {
        // ------- Look in "VeriIdDef.h" for class description of this object -------
        // Downcast VeriIdDef->VeriNamedPort. Avoid dynamic_cast overhead.
        VeriNamedPort *named_port = static_cast<VeriNamedPort*>(id_def) ;

        VeriExpression *port_expr = named_port->GetPortExpression() ;
        break ;
        }
    // The control should not reach here.
    default : Message::Error(id_def->Linefile(),"unknown identifier ", id_def->Name(), " found") ; // For the default testcase, this should never get executed.
    }
}

/* ------------------------------------------------------- */

static void CustomTraverseMisc(VeriTreeNode *tree_node)
{
    if (!tree_node) return ;

    // ------------------------------------------------------------------------------------------------------------------------------
    // The following are remaining, miscellaneous VeriTreeNode classes
    // ------------------------------------------------------------------------------------------------------------------------------
    // VeriCaseItem                    VeriDefParamAssign             VeriCellConfig                VeriProduction
    // VeriDelayOrEventControl         VeriGenerateCaseItem           VeriDefaultConfig             VeriProductionItem
    // VeriNetRegAssign                VeriPath                       VeriUseClause
    // VeriStrength                    VeriInstanceConfig             VeriClockingDirection
    // ------------------------------------------------------------------------------------------------------------------------------
    switch(tree_node->GetClassId()) {
    case ID_VERICASEITEM:
        {  // Case statement item
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriCaseItem. Avoid dynamic_cast overhead.
        VeriCaseItem *item = static_cast<VeriCaseItem*>(tree_node) ;

        unsigned is_default = item->IsDefault() ;

        // Get array of conditions (VeriExpressions)
        Array *conditions = item->GetConditions() ;
        VeriExpression *condition ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(conditions, i, condition) {
            CustomTraverseExpr(condition) ;
        }

        // Get statement
        VeriStatement *stmt = item->GetStmt() ;
        CustomTraverseStmt(stmt) ;
        break ;
        }
    case ID_VERIGENERATECASEITEM:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriGenerateCaseItem. Avoid dynamic_cast overhead.
        VeriGenerateCaseItem *gen_case_item = static_cast<VeriGenerateCaseItem*>(tree_node) ;

        Array *cond_arr = gen_case_item->GetConditions() ; // Get an array of expressions
        unsigned i ;
        VeriExpression *cond ;
        FOREACH_ARRAY_ITEM(cond_arr, i, cond) {
            CustomTraverseExpr(cond) ; // Traverse if you want to ...
        }

        VeriModuleItem *item = gen_case_item->GetItem() ;
        CustomTraverseModuleItem(item) ;
        break ;
        }
    case ID_VERIPATH:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriPath. Avoid dynamic_cast overhead.
        VeriPath *path = static_cast<VeriPath*>(tree_node) ;

       // Get the polarity operator which can take up the values VERI_PLUS, VERI_MINUS,
       // VERI_PARTSELECT_UP and VERI_PARTSELECT_DOWN
        unsigned pol_op = path->GetPolarityOperator() ; // Gets the polarity operator

        // Get the path token which can take up the values VERI_LEADTO and VERI_ALLPATH
        unsigned token = path->GetPathToken() ; // Gets the path token

        // Get the edge which can take up the values VERI_POSEDGE or VERI_NEGEDGE
        unsigned edge = path->GetEdge() ; // Gets the edge

        Array *in_term_arr = path->GetInTerminals() ; // Gets an array of input terminals to the path
        unsigned i ;
        VeriName *name ;
        FOREACH_ARRAY_ITEM(in_term_arr, i, name) {
            CustomTraverseExpr(name) ; // Traverse if you want...
        }

        Array *out_term_arr = path->GetOutTerminals() ; // Gets an array of output terminals to the path
        FOREACH_ARRAY_ITEM(out_term_arr, i, name) {
            if (!name) continue ;
            const char *char_name = name->GetName() ; // Gets the name in char* form
        }

        VeriExpression *src = path->GetDataSource() ;
        CustomTraverseExpr(src) ; // Traverse if you want...
        break ;
        }
    case ID_VERISTRENGTH:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriStrength. Avoid dynamic_cast overhead.
        VeriStrength *strength = static_cast<VeriStrength*>(tree_node) ;

        // The different drive values that VeriStrength can take up are :
        // VERI_STRONG0, VERI_PULL0, VERI_WEAK0, VERI_SUPPLY1, VERI_STRONG1,
        // VERI_PULL1, VERI_WEAK1, VERI_HIGHZ1 and VERI_HIGHZ0
        // The differnt charge values that VeriStrength can take up are :
        // VERI_SMALL, VERI_MEDIUM, VERI_LARGE
        unsigned lval = strength->GetLVal() ;  // Gets the LHS value
        unsigned rval = strength->GetRVal() ;  // Gets the RHS value
        break ;
        }
    case ID_VERIDELAYOREVENTCONTROL:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriDelayOrEventControl. Avoid dynamic_cast overhead.
        VeriDelayOrEventControl *del_or_evt_ctrl = static_cast<VeriDelayOrEventControl*>(tree_node) ;

        VeriExpression *del_ctrl = del_or_evt_ctrl->GetDelayControl() ; // Gets the delay control expression
        CustomTraverseExpr(del_ctrl) ; // Traverse if you want...

        VeriExpression *repeat_evt = del_or_evt_ctrl->GetRepeatEvent() ; // Gets the repeat event expression
        CustomTraverseExpr(repeat_evt) ; // Traverse if you want...

        Array *evt_ctrl_arr = del_or_evt_ctrl->GetEventControl() ; // Get an array of VeriExpressions
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(evt_ctrl_arr, i, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want to ...
        }
        break ;
        }
    case ID_VERINETREGASSIGN:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriNetRegAssign. Avoid dynamic_cast overhead.
        VeriNetRegAssign *net_reg_assign = static_cast<VeriNetRegAssign*>(tree_node) ;

        VeriExpression *lval = net_reg_assign->GetLValExpr() ; // Gets the LHS value
        CustomTraverseExpr(lval) ; // Traverse if you want...

        VeriExpression *rval = net_reg_assign->GetRValExpr() ; // Gets the RHS value
        CustomTraverseExpr(rval) ; // Traverse if you want...
        break ;
        }
    case ID_VERIDEFPARAMASSIGN:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriDefParamAssign. Avoid dynamic_cast overhead.
        VeriDefParamAssign *def_param_assign = static_cast<VeriDefParamAssign*>(tree_node) ;

        // Get the left-hand value
        VeriName *lval = def_param_assign->GetLVal() ;
        CustomTraverseExpr(lval) ; // Traverse if you want...

        // Get the right-hand value
        VeriExpression *rval = def_param_assign->GetRVal() ;
        CustomTraverseExpr(rval) ; // Traverse if you want...
        break ;
        }
    case ID_VERIINSTANCECONFIG:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriInstanceConfig. Avoid dynamic_cast overhead.
        VeriInstanceConfig *inst_config = static_cast<VeriInstanceConfig*>(tree_node) ;

        // Get the instance name
        VeriName *inst_ref = inst_config->GetInstanceName() ;
        CustomTraverseExpr(inst_ref) ; // Traverse if you want...

        // Get the use clause
        VeriUseClause *use_clause = inst_config->GetUseClause() ;
        CustomTraverseMisc(use_clause) ; // Traverse if you want...
        break ;
        }
    case ID_VERICELLCONFIG:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriCellConfig. Avoid dynamic_cast overhead.
        VeriCellConfig *cell_config = static_cast<VeriCellConfig*>(tree_node) ;

        // Get the instance name
        VeriName *cell_name = cell_config->GetCellName() ;
        CustomTraverseExpr(cell_name) ; // Traverse if you want...

        // Get the use clause
        VeriUseClause *use_clause = cell_config->GetUseClause() ;
        CustomTraverseMisc(use_clause) ; // Traverse if you want...
        break ;
        }
    case ID_VERIDEFAULTCONFIG:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriDefaultConfig. Avoid dynamic_cast overhead.
        VeriDefaultConfig *default_config = static_cast<VeriDefaultConfig*>(tree_node) ;
        // Insert code
        break ;
        }
    case ID_VERIUSECLAUSE:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriDefaultConfig. Avoid dynamic_cast overhead.
        VeriUseClause *use_clause = static_cast<VeriUseClause*>(tree_node) ;
        // Get the cell name
        VeriName *cell_name = use_clause->GetCellName() ;
        CustomTraverseExpr(cell_name) ; // Traverse if you want...
        break ;
        }
    case ID_VERICLOCKINGDIRECTION:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriClockingDirection. Avoid dynamic_cast overhead.
        VeriClockingDirection *clocking_dir = static_cast<VeriClockingDirection*>(tree_node) ;
        // Get the delay control
        VeriDelayOrEventControl *delay_control = clocking_dir->GetDelayControl() ;
        CustomTraverseMisc(delay_control) ; // Traverse if you want ...
        break ;
        }
    case ID_VERIPRODUCTION:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriProduction. Avoid dynamic_cast overhead.
        VeriProduction *prod = static_cast<VeriProduction*>(tree_node) ;
        // Get the data type
        VeriDataType *data_type = prod->GetDataType() ;
        // Get the production id
        VeriIdDef *prod_id = prod->GetId() ;
        // Get the production items
        Array *items = prod->GetItems() ;
        unsigned i ;
        VeriProductionItem *prod_item ;
        FOREACH_ARRAY_ITEM(items, i, prod_item) {
            CustomTraverseMisc(prod_item) ; // Traverse if you want ...
        }
        // Get the port array
        Array *ports = prod->GetPorts() ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(ports, i, expr) {
            CustomTraverseExpr(expr) ; // Traverse if you want ...
        }
        break ;
        }
    case ID_VERIPRODUCTIONITEM:
        {
        // ------- Look in "VeriMisc.h" for class description of this object -------
        // Downcast VeriTreeNode->VeriProductionItem. Avoid dynamic_cast overhead.
        VeriProductionItem *prod_item = static_cast<VeriProductionItem*>(tree_node) ;
        // Get the rand-join expression
        VeriExpression *expr = prod_item->GetExpression() ;
        // Get the array of statements
        Array *items = prod_item->GetItems() ;
        unsigned i ;
        VeriStatement *stmt ;
        FOREACH_ARRAY_ITEM(items, i, stmt) {
            CustomTraverseStmt(stmt) ; // Traverse if you want ...
        }
        // Get the weight spec
        VeriExpression *weight_spec = prod_item->GetWeightSpec() ;
        CustomTraverseExpr(weight_spec) ; // Traverse if you want ...

        // Get the code block
        VeriStatement *code_block = prod_item->GetCodeBlock() ;
        CustomTraverseStmt(code_block) ; // Traverse if you want ...
        break ;
        }

    // For this given test case, no other parse tree node types should get passed through
    // a full list starts in VeriTreeNode.h. All class ids are given in VeriClassIds.h
    default: Message::Error(tree_node->Linefile(),"unknown parse tree node found") ;
    }
}

/* ------------------------------------------------------- */

