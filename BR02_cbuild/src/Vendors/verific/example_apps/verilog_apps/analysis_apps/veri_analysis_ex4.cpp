/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Map.h"            // Make associated hash table class Map available
#include "Set.h"            // Make associated hash table class Set available

#include "Message.h"        // Make message handlers available

#include "veri_file.h"      // Make verilog reader available

#include "Ex4Visitor.h"     // Make visitor pattern available

#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriId.h"         // Definitions of all verilog identifier tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
******************************* VERILOG EXAMPLE #4 **************************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is an example demonstrates the following :

        1) how to parse (analyze) a verilog design
        2) how to traverse the resulting veriog parse tree looking for
           assign statements and any behaviorial code within an always block

    This example makes use of a visitor class, which is well-known SW design
    pattern for traversing a data-structure.  The base verilog visitor class
    is located in the VeriVisitor.* files, and this base class already defines
    the default traversing algorithm to walk a verilog parse-tree.  A newly
    derived class, called ExampleVisitor, was created to collect certain verilog
    parse-tree constructs very easily.  Please take a look at the Ex4Visitor.*
    files for details.

    This application can be run with an optional argument specifying a file name, without
    which it is going to work on the default file ie. "r4000.v".

    Lastly, I recommend running through this example in a debugger, since
    you'll be able to look at the state of the objects and see everything
    that is there.

    As a disclaimer, this example was handmade specifically for the design
    file that is being read in.  Because of this, many NULL pointer checks
    are not needed.  Ideally there should be NULL pointer checks everywhere.

*****************************************************************************************/

int main (int argc, char **argv)
{
    // Create a place-holder for the verilog reader
    veri_file veriReader ;

    char *file_nm ; // Stores the file name

    // If no file name is supplied then take the default file name which is "../../../example_designs/verilog/r4000.v"
    switch (argc) {
    case 1: // That is if the user does not specify any option
        file_nm = "../../../example_designs/verilog/r4000.v" ; // Sets the file name to the default name
        // The default top-level design we're going to read in has some `include
        // directives, so we need to set the include path now.
        veriReader.AddIncludeDir("../../../example_designs/verilog") ;
        break ;
    case 2:
        file_nm = argv[1] ; // Sets file name to name supplied by the user
        break ;
    default:
        Message::Error(0, "Too many options.") ;
        return 1 ;
    }

    VeriModule *pModule ; // Stores the name of the module to be worked with

    // |/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\|
    // |             ANALYSIS             |
    // |\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/|

    // Now analyze the Verilog file (into the work library). In case of any error do not process further.
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veriReader.Analyze(file_nm, 1)) return 1 ;

    // Get the list of top modules
    Array *top_mod_array = veriReader.GetTopModules() ;
    if (!top_mod_array) {
        // If there is no top level module then issue error
        Message::Error(0,"Cannot find any top level module. Check for recursive instantiation") ;
        return 1 ;
    }

    // Get a handle (by name) to the module that was just analyzed by specifying the module name
    // like this :
    // VeriModule *module = veriReader.GetModule("<Insert the name of the concerned module>") ;

    // Comment the following line if you want to look for a module by its name and hence have uncommented the previous line
    VeriModule *module = (VeriModule *) top_mod_array->GetFirst() ; // Get the first top level module

    delete top_mod_array ; top_mod_array = 0 ; // Cleanup, it is not required anymore

    if (!module) {
        // This should never happen
        Message::Msg(VERIFIC_ERROR, 0, 0, "an error occurred with this example") ;
        return 0 ;
    }

    // Use a visitor pattern to easily accumulate the following. Please take a look
    // in Ex4Visitor.h and Ex4Visitor.cpp for details.
    //   1) all assign statements
    //   2) all always blocks
    //   3) all instantiations
    ExampleVisitor visitor ;

    // Traverse parse-tree and collect what we're looking for
    module->Accept(visitor) ;

    // Now let's look at all continuous assign statements that we collected
    SetIter si ;
    VeriContinuousAssign *assign ;
    FOREACH_SET_ITEM(visitor.GetAssignStmts(), si, &assign) {
        // Get strength (if it exists)
        VeriStrength *strength = assign->GetStrength() ; (void)strength ;
        // Get delays (if they exist)
        unsigned i ;
        VeriExpression *delay ;
        FOREACH_ARRAY_ITEM(assign->GetDelay(), i, delay) {
            // Do what you want with the delay (which is a expression) here
        }
        // Get lhs and rhs of continuous assignments
        VeriNetRegAssign *netassign ;
        VeriExpression *lhs, *rhs  ;
        FOREACH_ARRAY_ITEM(assign->GetNetAssigns(), i, netassign) {
            lhs = netassign->GetLValExpr() ;
            rhs = netassign->GetRValExpr() ;
            // Do what you want with them ...
        }
    }

    // Now let's look at all module instantiations that we collected
    VeriModuleInstantiation *module_inst ;
    FOREACH_SET_ITEM(visitor.GetModuleInsts(), si, &module_inst) {
        // Get module's name
        const char *name = module_inst->GetModuleName() ; (void)name ;
        VeriModule *instantiated_module = module_inst->GetInstantiatedModule() ; (void) instantiated_module ;

        // Get strength (if it exists)
        VeriStrength *strength = module_inst->GetStrength() ; (void) strength ;
        // Get parameter values (if they exist)
        unsigned i ;
        VeriExpression *param ;
        FOREACH_ARRAY_ITEM(module_inst->GetParamValues(), i, param) {
            // Do what you want here ...
        }
        // Get instances (identifiers) themselves
        VeriInstId *inst ;
        FOREACH_ARRAY_ITEM(module_inst->GetInstances(), i, inst) {
            // Get instance name
            const char *inst_name = inst->Name() ; (void)inst_name ;
            // Get instance's range (if it exists)
            VeriRange *range = inst->GetRange() ; (void)range ;
            // Get port connects (the actuals of the instantiation)
            unsigned j ;
            VeriExpression *port_connect ;
            FOREACH_ARRAY_ITEM(inst->GetPortConnects(), j, port_connect) {
                if (!port_connect) continue ; // open actual
                if (port_connect->NamedFormal()) {
                     // named association
                } else {
                     // ordered association
                }
            }
        }
    }

    // Now let's look at all always contructs that we collected
    VeriAlwaysConstruct *always ;
    FOREACH_SET_ITEM(visitor.GetAlwaysConstructs(), si, &always) {
        VeriStatement *stmt = always->GetStmt() ;
        // Always constructs most of the time have event controls or delay controls
        if (stmt->GetClassId() == ID_VERIDELAYCONTROLSTATEMENT) {
            VeriDelayControlStatement *delay_control = (VeriDelayControlStatement*)stmt ;
            // Delay control
            VeriExpression *delay = delay_control->GetDelay() ; (void)delay ;
            // Get statement
            stmt = delay_control->GetStmt() ;
            // Now do what you want with the statement's contents
        } else if (stmt->GetClassId() == ID_VERIEVENTCONTROLSTATEMENT) {
            // Event control
            VeriEventControlStatement *event_control = (VeriEventControlStatement*)stmt ;
            // Iterate of '@' event expressions
            VeriExpression *expr ;
            unsigned i ;
            FOREACH_ARRAY_ITEM(event_control->GetAt(), i, expr) {
                // Do what you want here with each event expression
            }
            // Get statement
            stmt = event_control->GetStmt() ;
            // Now do what you want with the statement's contents
        } else {
            // Now do what you want with the statement's contents
        }
    }

    return 1 ;
}

/*--------------------------------------------------------------*/
