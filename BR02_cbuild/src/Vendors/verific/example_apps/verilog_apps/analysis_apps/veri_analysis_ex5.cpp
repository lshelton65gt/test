/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Ex5Visitor.h"   // Make VeriPrettyPrint visitor class available
#include "veri_file.h"    // Make Verilog reader available
#include "VeriModule.h"
#include "Message.h"      // Verific message handler
#include <string.h>       // strcpy, strchr ...
#include <ctype.h>        // isalpha, etc ...
#include "Array.h"        // Make dynamic array class Array available

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*
    BUILD NOTE!!! : This file needs to be compiled with the same verilog compile switches,
                    that the verilog lib was built with, otherwise strange behavior may occur!
*/

/****************************************************************************************
******************************* VERILOG EXAMPLE #5 **************************************
*****************************************************************************************

                                NOTES

    As a continuation from example #3, the following code demonstrates a Visitor
    pattern approach.

    A Visitor class lets you define a new operation on a tree (or data structure)
    without changing the classes of the elements on which it operates (tree nodes).
    In essence, it lets you keep related operations together by defining them in
    one class.  To get this to work, do the following :

    1) Declare an abstract base Visitor class which contains a "void Visit(<type>&)"
       method for every <type> of element (VeriNode) the Visitor will traverse.
       This has already been done, and is located in "VeriVisitor.h".  Please
       take a look at its skeleton.

    2) Declare a "void Accept(VeriVisitor &)" method for every element (VeriNode) class.

    3) To create your particular Visitor class, derive from VeriVisitor, and
       implement all of the Visit() methods accordingly.

    For this example, we have implemented a PrettyPrintVisitor class that has the
    same behavior as the PrettyPrint() methods.  One thing to note here is that the
    original PrettyPrint() methods used polymorphism.  With a visitor, since
    the visitor object is always the caller of the operation, polymorphism will not
    work.  A CallCorrectVisit() method has been introduced that calls the GetClassId()
    on the &ref argument, and then calls the correct Visit() method, casting
    appropriately.  This is our fake polymorphism technique.  It's not pretty, but
    it gets the job done with minimal overhead when a polymorphism-based algorithm is
    trying to be implemented.

    Once again, GetClassId() is Verific's RTTI mechanism.  We implemented this method
    in place of using ANSI's C++ RTTI mechanism, because we felt their mechanism is too
    expensive in terms of performance.

    Please take a look at PrettyPrintVisitor.h/cpp for the details.  All pretty-
    print related methods and objects are contained within this visitor object,
    making it a nice self-contained object.

    This application can be run with an optional argument specifying a file name, without
    which it is going to work on the default file ie. "ex5_alu.v".

    Once again, I recommend running through this example in a debugger, since
    you'll be able to look at the state of the objects and see everything
    that is there.

*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veri_reader ; // Read the intro in veri_file.h to see the details of this object.

    char *file_nm ; // Stores the file name

    // If no file name is supplied then take the default file name which is "ex5_alu.v"
    switch (argc) {
    case 1: file_nm = "ex5_alu.v" ; break ; // Sets the file name to the default name
    case 2: file_nm = argv[1] ; break ; // Sets file name to name supplied by the user
    default: Message::Error(0, "Too many options.") ; return 1 ;
    }

    // Now analyze the Verilog file (into the work library). In case of any error do not process further.
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veri_reader.Analyze(file_nm, 1)) return 1 ;

    // Get a handle (by name) to the module that was just analyzed by specifying the module name
    // Get the list of top modules
    Array *top_mod_array = veri_reader.GetTopModules() ;
    if (!top_mod_array) {
        // If there is no top level module then it has to be an error
        return 1 ;
    }

    // like this :
    // VeriModule *module = veri_reader.GetModule("<Insert the name of the concerned module>") ;

    // Comment the following line if you want to look for a module by its name and hence have uncommented the previous line
    VeriModule *module = (VeriModule *) top_mod_array->GetFirst() ; // Get the first top level module

    delete top_mod_array ; top_mod_array = 0 ; // Cleanup, it is not required anymore

    char ppfile_nm[32] = "pp_";
    strcat(ppfile_nm, file_nm);

    // Now pretty print this module via a visitor object
    PrettyPrintVisitor ppv(ppfile_nm) ;
    // Check to make sure the file stream is good
    if (!ppv.IsFileGood()) return 0 ;

    module->Accept(ppv);
    Message::Info(0,"Pretty_print to output file ", ppfile_nm) ;
    return 1 ;
}

/* ------------------------------------------------------- */

