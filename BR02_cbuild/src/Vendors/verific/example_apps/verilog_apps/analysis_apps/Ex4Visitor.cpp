/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Ex4Visitor.h" // Visitor base class definition

#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*-----------------------------------------------------------------*/
//                      Constructor / Destructor
/*-----------------------------------------------------------------*/

ExampleVisitor::ExampleVisitor()
  : _assign_stmts(POINTER_HASH),
    _always_constructs(POINTER_HASH),
    _module_insts(POINTER_HASH)
{
}

ExampleVisitor::~ExampleVisitor()
{
}

/*-----------------------------------------------------------------*/
//        Visit Methods : Class details in VeriModuleItem.h
/*-----------------------------------------------------------------*/

void ExampleVisitor::Visit(VeriModuleInstantiation &node)
{
    _module_insts.Insert(&node) ;  // Store this node
    // We don't need to traverse any of this class's members.
}

void ExampleVisitor::Visit(VeriAlwaysConstruct &node)
{
    _always_constructs.Insert(&node) ;  // Store this node
    // We don't need to traverse any of this class's members.
}

void ExampleVisitor::Visit(VeriContinuousAssign &node)
{
    _assign_stmts.Insert(&node) ;   // Store this node
    // We don't need to traverse any of this class's members.
}

/*---------------------------------------------*/
