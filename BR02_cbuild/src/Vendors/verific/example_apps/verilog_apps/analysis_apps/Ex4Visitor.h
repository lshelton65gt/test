/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_EXAMPLE_VISITOR_H_
#define _VERIFIC_VERI_EXAMPLE_VISITOR_H_

#include "VeriVisitor.h"    // Visitor base class definition
#include "Set.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------------------- */

class ExampleVisitor : public VeriVisitor
{
public:
    ExampleVisitor();
    virtual ~ExampleVisitor();

    // The following Visit methods need to be redefined for our purpose :

    // The following class definitions can be found in VeriModuleItem.h
    virtual void Visit(VeriModuleInstantiation &node);
    virtual void Visit(VeriAlwaysConstruct &node);
    virtual void Visit(VeriContinuousAssign &node);

    // Accessor methods
    Set* GetAssignStmts()          { return &_assign_stmts ; }
    Set* GetAlwaysConstructs()     { return &_always_constructs ; }
    Set* GetModuleInsts()          { return &_module_insts ; }

private:
    Set _assign_stmts ;      // Container (Set) of VeriAssign*'s
    Set _always_constructs ; // Container (Set) of VeriAlwaysConstruct*'s
    Set _module_insts ;      // Container (Set) of VeriModuleInstantiation*'s

     // Prevent the compiler from implementing the following
    ExampleVisitor(ExampleVisitor &node);
    ExampleVisitor& operator=(const ExampleVisitor &rhs);
} ;

/* -------------------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_EXAMPLE_VISITOR_H_

