module test ;
    reg a, b, c ;
    reg[0:7] d ;
    reg e, f, g ;
    always @(*)
    begin
        a = b;
        @(*)
        begin
            d[c] = b+c;
            @(*) if (xyz(a,b,c)) a =  a + 1 ;
            abc(a,c,b) ;
        end
    end
    task abc(input x, y, output z) ;
        z = e + f ;
    endtask
    function xyz (input x,y, z) ;
        xyz = x +y +z;
    endfunction
endmodule

