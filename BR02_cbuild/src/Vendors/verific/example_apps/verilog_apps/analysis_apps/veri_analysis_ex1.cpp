/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Array.h"          // Make dynamic array class Array available
#include "Map.h"            // Make associated hash table class Map available

#include "Message.h"        // Make message handlers available, not used in this example

#include "veri_file.h"      // Make Verilog reader available

#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriId.h"         // Definitions of all identifier definition tree nodes
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "VeriScope.h"      // Symbol table of locally declared identifiers
#include "VeriLibrary.h"    // Definition of VeriLibrary

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
******************************* VERILOG EXAMPLE #1 **************************************
*****************************************************************************************

                                NOTES

    The following example is for instructional purposes only.  The code demonstrates
    a brute-force way of traversing a parse-tree in search of particular node
    information.  In essence, it's a large switch statement based on a node's
    GetClassId().  GetClassId() is Verific's RTTI mechanism.  We implemented this method
    in place of using ANSI's C++ RTTI mechanism, because we felt their mechanism is too
    expensive in terms of performance.

    There are cleaner ways on how to traverse a tree, such as declaring a pure
    virtual function at either the VeriNode or VeriTreeNode level, and implementing
    the function for all tree nodes.  You can also implement a Visitor design
    pattern to get the same effect.

    This brute-force approach hopefully will give a first-timer a better
    overall picture on how things are constructed than the other two approaches,
    since all the code is right in front of them, and there are no other files
    or functions that need to be viewed initially that pertain to the traversing.

    "Veri_main_ex2.cpp" contains a second brute-force example, but some of the
    functionality there has been modularized to make it easier to read and follow.
    Please look at that example after going through this one.

    This application can be run with an optional argument specifying a file name, without
    which it is going to work on the default file ie. "ex1_dff.v".

    One last thing, I recommend running through this example in a debugger, since
    you'll be able to look at the state of the objects and see everything
    that is there.  The following example only half-heartedly goes through
    everything that it can do.

*****************************************************************************************/

// This main() function is a test program for the verilog analyzer only.  No
// elaboration is done here.

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veri_reader ; // Read the intro in veri_file.h to see the details of this object.

    // The design "ex1_dff.v" is to be read in only if the user doesn't specify any other file name:

    char *file_nm ; // Stores the file name

    // If no file name is specified then process the default file name which is "ex1_dff.v"
    switch (argc) {
    // That is if the user does not specify any optional file name
    case 1: file_nm = "ex1_dff.v" ; break ; // Sets the file name to the default name
    case 2: file_nm = argv[1] ; break ; // Sets file name to name supplied by the user
    default: Message::Error(0, "Too many options.") ; return 1 ;
    }

    // Now analyze the System Verilog file (into the work library). In case on any error do not process further.
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veri_reader.Analyze(file_nm, 3)) return 1 ;

    // Get the list of top modules
    Array *top_mod_array = veri_reader.GetTopModules() ;
    if (!top_mod_array) {
        // If there is no top level module then issue error
        Message::Error(0,"Cannot find any top module. Check for recursive instantiation") ;
        return 1 ;
    }
    VeriModule *module  = (VeriModule *) top_mod_array->GetFirst() ; // Get the first top level module
    delete top_mod_array ; top_mod_array = 0 ; // Cleanup, it is not required anymore

    // Just to see that this is a module, and not a primitive
    VeriIdDef *module_id = (module) ? module->Id() : 0 ;
    if(!module_id) {
        Message::Error(0, "module should always have an 'identifier'.") ;
    }

    if (module_id->IsUdp()) {
        /* This is a Verilog UDP, a primitive */
    } else {
        /* This is a module */
    }

    // Iterate through the module's list of ports ('95 style port or ANSI port declarations)
    Array *ports = module->GetPortConnects() ;
    VeriExpression *port ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(ports, i, port) {
        if (!port) continue ; // Check for NULL pointer

        // All API routines allowed on a VeriExpression are in VeriExpression.h

        // --------------------------------------------------------------------------------------------------------------
        // The following are all the types of VeriExpressions:
        // --------------------------------------------------------------------------------------------------------------
        // VeriAnsiPortDecl        VeriBinaryOperator      VeriConcat             VeriStructUnion         VeriCast
        // VeriEventExpression     VeriUnaryOperator       VeriFunctionCall       VeriDotStar             VeriNew
        // VeriIdRef               VeriIndexedId           VeriIndexedMemoryId    VeriIfOperator          VeriConcatItem
        // VeriMinTypMaxExpr       VeriMultiConcat         VeriPortConnect        VeriSequenceConcat      VeriNull
        // VeriPortOpen            VeriQuestionColon       VeriSelectedName       VeriClockedSequence     VeriDollar
        // VeriSystemFunctionCall  VeriTimingCheckEvent    VeriRange              VeriAssignInSequence    VeriAssignmentPattern
        // VeriConst               VeriConstVal            VeriIntVal             VeriDistOperator        VeriMultiAssignmentPattern
        // VeriRealVal             VeriDataType            VeriTypeRef            VeriSolveBefore         VeriForeachOperator
        // VeriStreamingConcat     VeriCondPredicate       VeriDotName            VeriTaggedUnion         VeriWithExpr
        // VeriInlineConstraint    VeriOpenRangeBinValue   VeriTransBinValue      VeriDefaultBinValue
        // VeriTransSet            VeriTransRangeList      VeriSelectCondition    VeriSelectBinValue
        // --------------------------------------------------------------------------------------------------------------
        //    In Verilog'95, 'port' (a port expression on a module) can be a
        //    'id' (VeriIdRef),   such as : D
        //    a bit-select (VeriIndexedId with expression index) such as : A[5]
        //    a part-select (VeriIndexedId with range index) such as : B[4:0]
        //    a concatenation of these (VeriConcat) such as : {a,B[3:0],c[4]}
        //    or a 'named' port : (VeriPortConnect).  such as :  .p(a)
        //    In Verilog'2001, it can also be an
        //    ansi-port declaration (VeriAnsiPortDecl) such as : input reg [0:4] a, b, ...

        //    In System Verilog, it can also be an assignment pattern or multiassignment pattern.
        //    We will find out in the loop, by calling  'GetClassId()' on the port expression :

        switch(port->GetClassId()) {
        case ID_VERIANSIPORTDECL:
            {
            // Downcast VeriExpression->VeriAnsiPortDecl. now that we know it is a VeriAnsiPortDecl
            VeriAnsiPortDecl *ansi_port = static_cast<VeriAnsiPortDecl*>(port) ;

            // eg. :
            // input reg [5:0] a, b, c ...

            // Get data type for this declaration
            VeriDataType *data_type = ansi_port->GetDataType() ; // VeriDataType includes 'type' (VERI_REG, VERI_WIRE, VERI_STRUCT etc), array dimension(s), signing ...

            unsigned port_dir = ansi_port->GetDir() ; // a token : VERI_INPUT, VERI_OUTPUT or VERI_INOUT..

            unsigned j ;
            VeriIdDef *port_id ;
            // Iterate through all ids declared in this Ansi port decl
            FOREACH_ARRAY_ITEM(ansi_port->GetIds(), j, port_id) {
                if (!port_id) continue ;
                const char *port_name = port_id->Name() ;
                Message::Info(port_id->Linefile(),"here is an ansi port ", port_name) ;
                // Here you can put more code to get more info about the port_id.
                // The full set of routines that are allowed on a VeriIdDef is in VeriId.h.
            }
            break ;
            }
        case ID_VERIINDEXEDID:
            {
            // Downcast VeriExpression->VeriIndexedId. now that we know it is a VeriIndexedId
            VeriIndexedId *indexed_id = static_cast<VeriIndexedId*>(port) ;
            // eg. :
            // module split_ports (a[7:4]) ;
            // Here 'a' has a direction (always), an optional type and a range.
            // Here you can test all that :

            unsigned port_dir = indexed_id->PortExpressionDir() ; // a token : VERI_INPUT, VERI_OUTPUT or VERI_INOUT..

            const char *port_name = indexed_id->GetName() ; // Get port name
            Message::Info(indexed_id->Linefile(),"here '", port_name, "' is an indexed id in a port declaration") ;

            const char *prefix_name = indexed_id->GetName() ; // Get prefix name in char* form

            VeriIdDef *prefix_id = indexed_id->GetId() ; // Get resolved referenced (prefix) identifier

            // Here you can put more code to get more info about the port_id.
            // The full set of routines that are allowed on a VeriIdDef is in VeriId.h.
            break ;
            }
        case ID_VERIPORTCONNECT:
            {
            // Downcast VeriExpression->VeriPortConnect. now that we know it is a  VeriPortConnect
            VeriPortConnect *port_conn = static_cast<VeriPortConnect*>(port) ;

            // eg.:
            // module same_port (.a(i)) ;
            // Name 'i' is declared within the module as a normal declaration with a direction, type and an optional range
            // Here you can test all that :

            VeriExpression *conn = port_conn->GetConnection() ; // Return the 'actual' expression

            Message::Info(port_conn->Linefile(),"here '", port_conn->GetNamedFormal(), "' is a named port declaration") ;
            // Here you can put more code to get more info about the port_conn.
            // The full set of routines that are allowed on a VeriPortConnect is in VeriExpression.h
            break ;
            }
        case ID_VERICONCAT:
            {
            // Downcast VeriExpression->VeriConcat. now that we know it is a  VeriConcat
            VeriConcat *concat = static_cast<VeriConcat*>(port) ;

            // eg. :
            // module same_port ({i, j[2]}) ;
            // Net 'i' and 'j' are declared within the module as a normal declaration with a direction, type and an optional range
            // Here you can test all that :

            unsigned port_dir = concat->PortExpressionDir() ; // Can take up values: VERI_INPUT, VERI_OUTPUT or VERI_INOUT

            Array *expr_arr = concat->GetExpressions() ; // Get an array of expressions for the concatenation
            unsigned j ;
            // Iterate through all expressions
            VeriExpression *expr ;
            FOREACH_ARRAY_ITEM(expr_arr, j, expr) {
                if (!expr) continue ;
                const char *port_name = expr->GetName() ;
            // Here you can put more code to get more info about the port_conn.
            // The full set of routines that are allowed on a VeriPortConnect can be found in VeriExpression.h
            }
            Message::Info(concat->Linefile(),"here is a 'concat' port declaration") ;
            break ;
            }
        case ID_VERIIDREF:
            {
            // Downcast VeriExpression->VeriIdRef. now that we know it is a VeriIdRef
            VeriIdRef *id_ref = static_cast<VeriIdRef*>(port) ;

            // eg. :
            // module same_port (i) ;

            // Net 'i' is declared within the module as a normal declaration with a direction, type and an optional range
            // An IdRef is a 'reference' to an identifier. When we parse it, it is just a name.
            // That is the 'unresolved' name :
            // Get unresolved name of this IdRef.
            const char *name = id_ref->GetName() ;
            // 'name' will be '0', since the id-reference is resolved in the analyzer.
            // This means that the (port)reference now has a pointer directly to the identifier that it refers to.
            // Here is how you pick that one up :

            // Get the resolved identifier definition that is referred here
            VeriIdDef *id = id_ref->FullId() ;

            // Now you can test a lot about this identifier.
            // For example : print where it came from :
            Message::Info(id_ref->Linefile(),"here is a reference to the port list, which refers to this port :") ;
            Message::Info(id->Linefile(),"here is port ", id->Name()) ;

            // You can do another GetIdClass on it to get the specific
            // IdDef, but we'll just use the wide spectrum of API routines which are
            // available directly off a VeriIdDef (check out class VeriIdDef in file verilog/VeriId.h).
            unsigned is_port    = id->IsPort() ;  // Should return true in this case
            unsigned port_dir   = id->Dir() ;     // returns VERI_INPUT, VERI_OUTPUT, VERI_INOUT
            unsigned port_type  = id->Type() ;    // returns VERI_WIRE, VERI_REG, etc ...
            // All tokens are #defined unsigned integers. The parser generated them into file 'veri_yacc.h',
            // and they are visible by including 'tokens.h'.
            break ;
            }
        default:
            // Issue a message through the message handler as the control
            // shouldn't have reached here. Note that we issue it with
            // file/line info from the Verilog file where this (VeriIdRef)
            // tree-node was created (port->Linefile()).
            Message::Error(port->Linefile(),"unknown port found") ;
        }
    }

    // Iterate through the module items (such as various object declarations, assignments,
    // instantiations, constructs, and so forth).
    Array *module_items = module->GetModuleItems() ;
    VeriModuleItem *module_item ;
    FOREACH_ARRAY_ITEM(module_items, i, module_item) {
        if (!module_item) continue ; // null-pointer check.

        VeriStatement *stmt = 0 ; // Stores an always or initial statement

        switch(module_item->GetClassId()) {
        // -------------------------------------------------------------------------------------------------------------------------------
        // The following are all the types of VeriModuleItems: (base class VeriModuleItem)
        // -------------------------------------------------------------------------------------------------------------------------------
        // VeriAlwaysConstruct         VeriContinuousAssign    VeriDataDecl                VeriPropertyDecl       VeriClockingDecl
        // VeriDefParam                VeriFunctionDecl        VeriGateInstantiation       VeriSequenceDecl
        // VeriGenerateBlock           VeriGenerateCase        VeriGenerateConditional     VeriModport
        // VeriGenerateConstruct       VeriGenerateFor         VeriInitialConstruct        VeriModportDecl
        // VeriModuleInstantiation     VeriTaskDecl            VeriNetDecl                 VeriClass
        // VeriPathDecl                VeriSpecifyBlock        VeriSystemTimingCheck       VeriBindDirective
        // VeriTable                   VeriInterface           VeriProgram                 VeriConstraintDecl
        // -------------------------------------------------------------------------------------------------------------------------------
        // Details about different module items can be found in the file VeriModuleItem.h

        case ID_VERIDATADECL: // Any data declaration (I/O, reg,parameter etc) other than net decl.
            {
            VeriDataDecl *data_decl = static_cast<VeriDataDecl*>(module_item) ;
            // eg. :
            // reg     [CARDINALITY-1:0] Q ;
            // output  [CARDINALITY-1:0] Q ;
            // input   [CARDINALITY-1:0] D ;
            // input   clk, rst ;
            // parameter CARDINALITY = 1 ;

            unsigned dir = data_decl->GetDir() ; // port direction (if this is a io decl)
            VeriDataType *data_type = data_decl->GetDataType() ; // VeriDataType includes 'type' (VERI_REG, VERI_WIRE, VERI_STRUCT, etc), array dimension(s), and signing ...

            // Iterate through all ids that pertains to this data decl
            VeriIdDef *id ;
            unsigned j ;
            FOREACH_ARRAY_ITEM(data_decl->GetIds(), j, id) {
                if (!id) continue ; // null pointer check
                const char *name = id->Name() ;
                // Here you can put more code to get more info about the id ;
                if (id->IsArray())   { /* This idDef is an array */ }
            }
            Message::Info(data_decl->Linefile(),"here is a 'data declaration' module item") ;
            break ;
            }
        case ID_VERIGENERATECONSTRUCT:
            {
            VeriGenerateConstruct *gen_constr = static_cast<VeriGenerateConstruct*>(module_item) ;
            // eg. :
            // generate // generate construct
            // genvar e, i ;
            //  for(i=0; i<width; i++) // generate for
            //         begin: for_gen_i
            //                 int j ;
            //         end
            //   case (width) // generate case
            //   1: bit x ;
            //   2: int y ;
            //   default: integer xx ;
            //   endcase
            //   if (width) bit b ; // generate conditional
            //   begin:gen_blk // generate block
            //   int gen_blk_int ;
            //   end
            // endgenerate

            Array *gen_items = gen_constr->GetItems() ; // Gets the generate items
            // Iterate through all the generate items that pertain to this generate construct
            VeriModuleItem *mod_item ;
            unsigned j, k ;
            FOREACH_ARRAY_ITEM(gen_items, j, mod_item) {
                if (!mod_item) continue ; // null pointer check

                switch(mod_item->GetClassId()) {
                case ID_VERIGENERATEFOR:
                    { // For generate for
                    VeriGenerateFor *gen_for = static_cast<VeriGenerateFor*>(mod_item) ;

                    VeriModuleItem *initial = gen_for->GetInitial() ; // Gets a VeriGenVarAssign only for pure Verilog 2000 (not for SV)

                    VeriExpression *cond = gen_for->GetCondition() ; // Gets the condition

                    VeriModuleItem *repetition = gen_for->GetRepetition() ; // Gets a VeriGenVarAssign only for pure Verilog 2000 (not for SV)

                    VeriIdDef *blk_id = gen_for->GetBlockId() ; // Get the 'generate for block' id
                    if (blk_id) {
                        Message::Info(gen_for->Linefile(),"here '", blk_id->Name(), "' is a 'named for generate block' in generate") ;
                    } else {
                        Message::Info(gen_for->Linefile(),"here is an 'unnamed for generate block'in generate") ;
                    }

                    Array *item_arr = gen_for->GetItems() ; // Gets an array of VeriModuleItem*. The generate items
                    VeriModuleItem *item ;
                    FOREACH_ARRAY_ITEM(item_arr, j, item) {
                        if (!item) continue ;
                            // Get more information about module items in VeriModuleItem.h
                            // and also in the current switch construct
                    }
                    break ;
                    }
                case ID_VERIGENERATECASE:
                    { // For generate case
                    VeriGenerateCase *gen_case = static_cast<VeriGenerateCase*>(mod_item) ;

                    VeriExpression *cond_expr = gen_case->GetCondition() ; // Gets the condition

                    Array *gen_case_item_arr = gen_case->GetCaseItems() ; // Gets an array of VeriGenerateCaseItem
                    VeriGenerateCaseItem *case_item ;
                    FOREACH_ARRAY_ITEM(gen_case_item_arr, k, case_item) {
                        if (!case_item) continue ;
                        Array *cond_arr = case_item->GetConditions() ; // Array of VeriExpression : values under which this case item hits
                        VeriModuleItem *gen_case_mod_item = case_item->GetItem() ; // The case item itself
                        // Insert code to iterate through the case items
                    }
                    Message::Info(gen_case->Linefile(),"here is a 'generate case' in generate") ;
                    break ;
                    }
                case ID_VERIGENERATECONDITIONAL:
                    { // For generate conditional
                    VeriGenerateConditional *gen_conditional = static_cast<VeriGenerateConditional*>(mod_item) ;

                    VeriExpression *if_expr = gen_conditional->GetIfExpr() ; // Gets the condition

                    Message::Info(gen_conditional->Linefile(),"here is a 'generate conditional' in generate") ;

                    VeriModuleItem *then_mod_item = gen_conditional->GetThenItem() ; // Get the items following the 'if' condition
                    if (!then_mod_item) break ; // If 'if then' is not present then break out of switch statement

                    switch(then_mod_item->GetClassId()) { // For the 'if' part of the generate conditional block
                    // Insert code to iterate through the module items in the generate conditional block
                    // Get more information about module items in VeriModuleItem.h and in the previous switch construct
                    default : break ;
                    }

                    VeriModuleItem *else_mod_item = gen_conditional->GetElseItem() ; // Get the items following the 'else' condition
                    if (!else_mod_item) break ; // If 'else' not present then break out of switch

                    switch(else_mod_item->GetClassId()) { // For the 'else' part of the generate conditional block
                    // Insert code to iterate through the module items in the generate conditional block
                    // Get more information about module items in VeriModuleItem.h and in the previous switch construct
                    default : break ;
                    }
                    break ;
                    }
                case ID_VERIGENERATEBLOCK:
                    { // For generate block
                    VeriGenerateBlock *gen_blk = static_cast<VeriGenerateBlock*>(mod_item) ;

                    VeriIdDef *gen_blk_id = gen_blk->GetBlockId() ; // Get the id of the generate block
                    if (gen_blk_id) {
                        if (gen_blk_id) Message::Info(gen_blk_id->Linefile(),"here '", gen_blk_id->Name(), "' is a named generate block") ;
                    } else {
                        if (gen_blk_id) Message::Info(gen_blk->Linefile(),"here is an unnamed generate block") ;
                    }
                    Array *gen_blk_item_arr = gen_blk->GetItems() ; // Get the items within a generate block
                    VeriModuleItem *gen_blk_item ;
                    FOREACH_ARRAY_ITEM(gen_blk_item_arr, k, gen_blk_item) {
                        if (!gen_blk_item) continue ;
                        // Insert code to iterate through the generate block items
                    }
                    break ;
                    }

                default: {
                    // Normal module items like always, initial, instantiation, etc.
                    // Details for these items are explained below
                    }
                }
            }
            break ;
            }
        case ID_VERIMODULEINSTANTIATION:
            { // Module instantiation
            VeriModuleInstantiation *mod_inst = static_cast<VeriModuleInstantiation*>(module_item) ;
            // eg. : test inst_name(actual, actual2) // where test is the name of a module

            VeriModule *module = mod_inst->GetInstantiatedModule() ; // Get the instantiated module
            // Properties of instantiated module declared in VeriModule.h

            VeriName *instantiated_unit = mod_inst->GetModuleNameRef() ; // unresolved instantiated module name (resolving to VeriIdDef* happens during elaboration)
            const char *mod_name =  mod_inst->GetModuleName() ;

            Message::Info(mod_inst->Linefile(),"here '", mod_name, "' is the instantiated module") ;

            Array *param_val_arr = mod_inst->GetParamValues() ;  // Array of VeriExpression*. The parameter assoociations. #(<paramexpr1>,<paramexpr2>, ..). In Verilog 2000 : could contain PortConnects too. : #(.paramname(<paramexpr>), ...)

            unsigned j ;
            VeriInstId *inst_id ;
            // Iterate over instances
            FOREACH_ARRAY_ITEM(mod_inst->GetIds(), j, inst_id) {
                if (!inst_id) continue ;
                const char *inst_name = inst_id->InstName() ; // Get the name of the instance
                if (inst_id) Message::Info(inst_id->Linefile(),"here '", inst_name, "' is the name of an instance") ;
                VeriRange *range = inst_id->GetRange() ; // Get the range of the instance
                Array *port_conn_arr = inst_id->GetPortConnects() ; // Get an array of port connects
                VeriExpression *expr ;
                unsigned k ;
                // Iterate over port connects
                FOREACH_ARRAY_ITEM(port_conn_arr, k, expr) {
                    if (!expr) continue ;
                    // Insert code to iterate through the port connects
                }
                // Insert code to get more information about each instance
            }
            break ;
            }
        case ID_VERIPATHDECL:
            { // For path declarations
            VeriPathDecl *path_decl = static_cast<VeriPathDecl*>(module_item) ;
            // eg. :
            // (A => Q) = 10 ; // Parallel connection
            // (C, D *> Q) = 18 ; // Full connection

            Array *arr_delay = path_decl->GetDelay() ; // Array of VeriExpression*. The path-delay-value : (<delay1>,<delay2>,...)
            unsigned j ;
            VeriExpression *expr ;
            FOREACH_ARRAY_ITEM(arr_delay, j, expr) {
                if (!expr) continue ;
                // Insert code to iterate through the delay expressions
            }

            VeriExpression *cond = path_decl->GetCondition() ; // Non-zero if state-dependent path

            VeriPath *path = path_decl->GetPath() ; // Get the path
            if (path) {
                // Get the edge which can take up the values VERI_POSEDGE or VERI_NEGEDGE
                unsigned edge = path->GetEdge() ;

                // Get the polarity operator which can take up the values VERI_PLUS, VERI_MINUS,
                // VERI_PARTSELECT_UP and VERI_PARTSELECT_DOWN
                unsigned polarity = path->GetPolarityOperator() ;

                // Get the path token which can take up the values VERI_LEADTO and VERI_ALLPATH
                unsigned path_token = path->GetPathToken() ;

                VeriExpression *data_src = path->GetDataSource() ;

                Array *in_terms = path->GetInTerminals() ; // Gets an array of inputs to the path
                VeriName *name ;
                FOREACH_ARRAY_ITEM(in_terms, j, name) {
                    const char *in_term_name = name->GetName() ;
                    // Insert code to obtain more information about the inputs
                }

                Array *out_terms = path->GetOutTerminals() ; // Gets an array of outputs to the path
                FOREACH_ARRAY_ITEM(out_terms, j, name) {
                    const char *out_term_name = name->GetName() ;
                    // Insert code to obtain more information about the outputs
                }
                Message::Info(path_decl->Linefile(),"here is a 'path declaration' module item") ;
            }
            break ;
            }
        case ID_VERITABLE:
            { // For table
            // As table resides in UDPs only and here we are dealing with top level
            // modules so the control is never going to reach here unless we process
            // UDPs as well instead of only one top level module

            VeriTable *table = static_cast<VeriTable*>(module_item) ;
            // eg. :
            // table
            // control dataA  dataB   mux
            //    0      1      0 :    1 ;
            //    0      1      1 :    1 ;
            // endtable

            Array *table_entries = table->GetTableEntries() ; // Array of char* rows in table
            unsigned j ;
            char *entry ;
            FOREACH_ARRAY_ITEM(table_entries, j, entry) {
                if (!entry) continue ;
                // Insert code to obtain more information about the table entries
            }

            VeriModule *module = table->GetTheModule() ; // Back-pointer to the module in which the table appears

            Message::Info(table->Linefile(),"here is a 'table' module item") ;
            break ;
            }
        case ID_VERICONTINUOUSASSIGN:
            { // For continuous assignments
            VeriContinuousAssign *cont_assign = static_cast<VeriContinuousAssign*>(module_item) ;
            //eg. :
            // assign {a, b} = c + d + d ;

            VeriStrength *strength = cont_assign->GetStrength() ;

            // The different drive values that VeriStrength can take up are :
            // VERI_STRONG0, VERI_PULL0, VERI_WEAK0, VERI_SUPPLY1, VERI_STRONG1,
            // VERI_PULL1, VERI_WEAK1, VERI_HIGHZ1 and VERI_HIGHZ0
            // The differnt charge values that VeriStrength can take up are :
            // VERI_SMALL, VERI_MEDIUM, VERI_LARGE
            if (strength) {
                unsigned lval = strength->GetLVal() ;
                unsigned rval = strength->GetRVal() ;
            }

            VeriExpression *expr ;
            unsigned j ;
            Array *arr_delay = cont_assign->GetDelay() ; // Get an array of delay expressions
            FOREACH_ARRAY_ITEM(arr_delay, j, expr) {
                if (!expr) continue ;
                // Insert code to obtain more information about the delay expressions
            }

            Array *net_assign_arr = cont_assign->GetNetAssigns() ; // Get an array of VeriNetAssigns
            VeriNetRegAssign *net_reg_assign ;
            FOREACH_ARRAY_ITEM(net_assign_arr, j, net_reg_assign) {
                if (!net_reg_assign) continue ;
                VeriExpression *lval_expr = net_reg_assign->GetLValExpr() ; // Get the target
                VeriExpression *rval_expr = net_reg_assign->GetRValExpr() ; // Get the value
                // Insert code to obtain more information about the left and
                // right net assign expressions
            }
            Message::Info(cont_assign->Linefile(),"here is a 'continuous assignment' module item") ;
            break ;
            }
        case ID_VERIFUNCTIONDECL:
            {
            VeriFunctionDecl *func_decl = static_cast<VeriFunctionDecl*>(module_item) ;
            // eg. :
            // function logic [15:0] myfunc2 ;
            //     input int x ;
            //     input int y ;
            //     ...
            // endfunction

            VeriIdDef *func_id = func_decl->GetFunctionId() ; // Gets the function id

            VeriDataType *data_type = func_decl->GetDataType() ; // Gets 'range', 'signing' and 'type' info for the return type of the function

            Array *func_ports_arr = func_decl->GetAnsiIOList() ; // Gets the list of formal ansi-port decls ie. array of VeriAnsiPortDecl (id's (with direction,datatype etc.))
            VeriExpression *port_decl ;
            unsigned j ;
            FOREACH_ARRAY_ITEM(func_ports_arr, j, port_decl) {
                if (!port_decl) continue ;
                // Insert code to obtain more information about the ports
                // Information about ansi declarations can be obtained from VeriExpression.h
            }

            Array *decl_arr = func_decl->GetDeclarations() ; // Gets local declarations (VeriModuleItems)
            VeriModuleItem *decl_item ;
            FOREACH_ARRAY_ITEM(decl_arr, j, decl_item) {
                if (!decl_item) continue ;
                // Insert code to obtain more information about the local declarations
                // Information about declarations can be obtained from VeriModuleItem.h
                // Find more about module items in case item of this switch
            }

            Array *func_stmt_arr = func_decl->GetStatements() ; // Gets the statements
            VeriStatement *stmt ;
            FOREACH_ARRAY_ITEM(func_stmt_arr, j, stmt) {
                if (!stmt) continue ;
                // Insert code to obtain more information about the function statements
                // Information about statements can be obtained from VeriStatement.h
                // Details about each statement is described below within the
                // always construct's description
            }
            if (func_id) Message::Info(func_id->Linefile(),"here is a '", func_id->Name(), "' 'function declaration' module item") ;
            break ;
            }
        case ID_VERISPECIFYBLOCK:
            {
            VeriSpecifyBlock *spec_blk = static_cast<VeriSpecifyBlock*>(module_item) ;
            // eg. :
            // specify
            //     specparam tRise_clk_q = 150, tFall_clk_q = 200 ;
            //     specparam tSetup = 70 ;
            //     (clk => q) = (tRise_clk_q, tFall_clk_q) ;
            //     $setup(d, posedge clk, tSetup) ;
            // endspecify

            Array *spec_arr = spec_blk->GetSpecifyItems() ; // Gets the array of specify items
            unsigned j ;
            VeriModuleItem *spec_item ;
            FOREACH_ARRAY_ITEM(spec_arr, j, spec_item) {
                if (!spec_item) continue ;
                // Insert code to obtain more information about the specify items
                // Information about module items can be obtained from VeriModuleItem.h
                // Details about the module items are described within this switch statement
            }
            Message::Info(spec_blk->Linefile(),"here is a 'specify block' module item") ;
            break ;
            }
        case ID_VERITASKDECL:
            {
            VeriTaskDecl *task_decl = static_cast<VeriTaskDecl*>(module_item) ;
            // eg. :
            // task mytask2 ;
            //     output x ;
            //     input y ;
            //     int x ;
            //     logic y ;
            //     ...
            // endtask

            VeriIdDef *task_id = task_decl->GetTaskId() ; // Gets the identifier

            unsigned j ;
            Array *port_decl_arr = task_decl->GetAnsiIOList() ; // Array of VeriAnsiPortDecl*
            VeriAnsiPortDecl *decl ;
            FOREACH_ARRAY_ITEM(port_decl_arr, j, decl) {
                if (!decl) continue ;
                // Insert code to obtain more information about the function statements
                // Information about statements can be obtained from VeriStatement.h
                // Details about module items described in the current switch construct
            }

            Array *task_decl_arr = task_decl->GetDeclarations() ; // Gets an array of declarations
            VeriModuleItem *decl_item ;
            FOREACH_ARRAY_ITEM(task_decl_arr, j, decl_item) {
                if (!decl_item) continue ;
                // Insert code to obtain more information about the local declarations
                // Information about declarations can be obtained from VeriModuleItem.h
                // Find more about module items in case item of this switch
            }

            Array *task_stmt_arr = task_decl->GetStatements() ;
            VeriStatement *stmt ;
            FOREACH_ARRAY_ITEM(task_stmt_arr, j, stmt) {
                if (!stmt) continue ;
                // Insert code to obtain more information about the function statements
                // Information about statements can be obtained from VeriStatement.h
                // Details about each statement is described below within the
                // always construct's description
            }
            if (task_id) Message::Info(task_id->Linefile(),"here is a '", task_id->Name(), "' 'task declaration' module item") ;
            break ;
            }
        case ID_VERIDEFPARAM:
            {
            VeriDefParam *def_param = static_cast<VeriDefParam*>(module_item) ;
            // eg. :
            // defparam f1.A = 3.1415 ;

            Array *def_arr = def_param->GetDefParamAssigns() ; // Array of VeriDefParamAssign*. The assignments to an (external) parameter, from a local expression
            VeriDefParamAssign *def_item ;
            unsigned j ;
            FOREACH_ARRAY_ITEM(def_arr, j, def_item) {
                if (!def_item) continue ;
                VeriName *lval = def_item->GetLVal() ; // Get parameter name
                VeriExpression *rval = def_item->GetRVal() ; // Get overridden value
                // Insert code to obtain more information about defparam assignments
            }
            Message::Info(def_param->Linefile(),"here is a 'defparam' module item") ;
            break ;
            }
        case ID_VERINETDECL:
            {   // Net(wire) Declaration
            VeriNetDecl *net_decl = static_cast<VeriNetDecl*>(module_item) ;
            // eg. : wire    [CARDINALITY-1:0] D ;

            unsigned net_type      = net_decl->GetNetType() ;     // returns VERI_REG, VERI_INTEGER, etc ...

            unsigned signed_type   = net_decl->GetSignedType() ;  // returns VERI_SIGNED, VERI_UNSIGNED

            VeriRange *range       = net_decl->GetRange() ;       // Get the range, if it exists

            VeriStrength *strength = net_decl->GetStrength() ;    // Get net strength, if it exists

            Array *delays          = net_decl->GetDelay() ;       // Get array of delays, if they exist
            // Iterate through all ids that pertain to this Ansi decl
            VeriIdDef *id ;
            unsigned j ;
            FOREACH_ARRAY_ITEM(net_decl->GetIds(), j, id) {
                if (!id) continue ; // null pointer check
                const char *name = id->Name() ;
                // Here you can put more code to get more info about the id ;
            }
            Message::Info(net_decl->Linefile(),"here is a 'net declaration' module item") ;
            break ;
            }
        case ID_VERIGATEINSTANTIATION:
            { // For gate instantiations
            VeriGateInstantiation *gate_inst = static_cast<VeriGateInstantiation*>(module_item) ;
            // eg. :
            // nor (highz1,strong0) n1(out1,in1,in2) ;

            // The different values that inst_type can take up are as follows:
            // VERI_ANDD, VERI_NAND, VERI_OR, VERI_NOR, VERI_XOR, VERI_XNOR,
            // VERI_BUF, VERI_NOT, VERI_BUFIF0, VERI_BUFIF1, VERI_NOTIF0,
            // VERI_NOTIF1, VERI_NMOS, VERI_PMOS, VERI_RNMOS, VERI_RPMOS,
            // VERI_TRAN, VERI_RTRAN, VERI_TRANIF0,VERI_TRANIF1, VERI_RTRANIF0
            // VERI_RTRANIF1, VERI_CMOS, VERI_RCMOS, VERI_PULLUP, VERI_PULLDOWN
            unsigned inst_type = gate_inst->GetInstType() ; // Gets the instantiation type

            VeriStrength * gate_str = gate_inst->GetStrength() ;
            // The different drive values that VeriStrength can take up are :
            // VERI_STRONG0, VERI_PULL0, VERI_WEAK0, VERI_SUPPLY1, VERI_STRONG1,
            // VERI_PULL1, VERI_WEAK1, VERI_HIGHZ1 and VERI_HIGHZ0
            // The differnt charge values that VeriStrength can take up are :
            // VERI_SMALL, VERI_MEDIUM, VERI_LARGE
            if (gate_str) {
                unsigned lval = gate_str->GetLVal() ;
                unsigned rval = gate_str->GetRVal() ;
            }

            Array *delay_arr = gate_inst->GetDelay() ; // Gets array of delay expressions
            unsigned j ;
            VeriExpression *expr ;
            FOREACH_ARRAY_ITEM(delay_arr, j, expr) {
                if (!expr) continue ;
                // Insert code to iterate through the delay expressions
            }

            Array *inst_arr = gate_inst->GetInstances() ; // Gets array of VeriInstId
            VeriInstId *inst_id ;
            FOREACH_ARRAY_ITEM(inst_arr, j, inst_id) {
                if (!inst_id) continue ;
                VeriRange * range = inst_id->GetRange() ; // Gets the range of the instance
                Array *port_conn_arr = inst_id->GetPortConnects() ; // Gets an array of VeriExpressions (VeriPortConnect/VeriIdRef/VeriExpressions)
                unsigned k ;
                VeriExpression *expr ;
                FOREACH_ARRAY_ITEM(port_conn_arr, k, expr) {
                    if (!expr) continue ;
                    // Insert code to iterate through the port connect expressions
                }
                const char *inst_name = inst_id->InstName() ; // Gets the name of the instance
                // Insert code to obtain more information about the delay expressions
            }
            Message::Info(gate_inst->Linefile(),"here is a 'gate instantiation' module item") ;
            break ;
            }
        case ID_VERISYSTEMTIMINGCHECK:
            { // For system check functions
            VeriSystemTimingCheck *sys_timing_chk = static_cast<VeriSystemTimingCheck*>(module_item) ;
            // eg.:
            // $timeskew (posedge CP &&& MODE, negedge CPN, 50) ;

            char *task_name = sys_timing_chk->GetTaskName() ; // Gets the name

            Array *arg_arr = sys_timing_chk->GetArgs() ; // Gets an array of VeriExpressions
            unsigned j ;
            VeriExpression *expr ;
            FOREACH_ARRAY_ITEM(arg_arr, j, expr) {
                if (!expr) continue ;
                // Insert code to obtain more information about the system timing check arguments
            }
            Message::Info(sys_timing_chk->Linefile(),"here is a 'system timing check' module item") ;
            break ;
            }
// System Verilog specific constructs
        case ID_VERIASSERTION:
            {
            VeriAssertion *assert_stmt = static_cast<VeriAssertion*>(module_item) ;
            // Assertion can be used either in concurrent area or in procedural area for both
            // concurrent assertion and immediate assertion this class is created
            Message::Info(assert_stmt->Linefile(),"here is a 'concurrent assertion statement'") ;
            break ;
            }
        case ID_VERICLOCKINGDECL:
            { // For clocking declarations
            VeriClockingDecl *clocking_decl = static_cast<VeriClockingDecl*>(module_item) ;
            // eg.:
            // clocking bus @(posedge clock1) ;
            //     default input #10ns output #2ns ;
            //     input data, ready, enable = top.mem1.enable ;
            //     output negedge ack ;
            //     input #1step addr ;
            // endclocking

            VeriIdDef *id = clocking_decl->GetId() ; // Get the id of the decl

            VeriExpression *clock_expr = clocking_decl->GetClockingEvent() ; // Gets the clocking expression

            Array *clocking_item_arr = clocking_decl->GetClockingItems() ; // Gets the clockign items
            unsigned j ;
            VeriModuleItem *item ;
            FOREACH_ARRAY_ITEM(clocking_item_arr, j, item) {
                if (!item) continue ;
                // Insert code to obtain more information about the clocking items
            }
            if (id) Message::Info(id->Linefile(),"here '", id->Name(), "' is a 'clocking declaration' module item") ;
            break ;
            }
        case ID_VERIINTERFACE:
            { // For interface
            VeriInterface *interface = static_cast<VeriInterface*>(module_item) ;
            // eg.:
            // interface bus_A (input clk) ;
            //     logic [15:0] data ;
            //     logic write ;
            // endinterface

            VeriIdDef *id = interface->GetId() ; // Gets the identifier of this construct (ie. the name)

            unsigned j ;
            Array *interface_parameter_arr = interface->GetParameterConnects() ; // Gets an array of VeriDataDecl (a VeriModuleItem)
            VeriModuleItem *param_decl ;
            FOREACH_ARRAY_ITEM(interface_parameter_arr, j, param_decl) {
                if (!param_decl) continue ;
                // Insert code to obtain more information about the interface parameters
                // Information about interface parameters can be obtained from VeriModuleItem.h
                // Details about all module items are described within this switch statement
            }

            Array *interface_port_arr = interface->GetPortConnects() ; // Gets an array of VeriAnsiPortDecl/VeriIdRef/VeriIndexedId/VeriConcat
            VeriExpression *port_expr ;
            FOREACH_ARRAY_ITEM(interface_port_arr, j, port_expr) {
                if (!port_expr) continue ;
                // Insert code to obtain more information about the interface ports
                // Information about interface ports can be obtained from VeriExpression.h
            }

            Array *interface_item_arr = interface->GetItems() ; // Gets the interface items
            VeriModuleItem *item ;
            FOREACH_ARRAY_ITEM(interface_item_arr, j, item) {
                if (!item) continue ;
                // Insert code to obtain more information about the interface items
                // Information about interface items can be obtained from VeriModuleItem.h
                // Details about all module items are described within this switch statement
            }
            if (id) Message::Info(interface->Linefile(),"here '", id->Name(), "' is an 'interface' module item") ;
            break ;
            }
        case ID_VERIMODPORTDECL:
            { // For modport declarations
            VeriModportDecl *modport_decl = static_cast<VeriModportDecl*>(module_item) ;
            // eg.:
            // modport test (input data, output write) ;

            VeriIdDef *id = modport_decl->GetId() ; // Get the prototype id's with a direction
            Array *modport_decl_arr = modport_decl->GetModportPortDecls() ; // Array of VeriModuleItem.. (mostly DataDecl's of ModportPort's (prototype id's with a direction)
            unsigned j ;
            VeriModuleItem *item ;
            FOREACH_ARRAY_ITEM(modport_decl_arr, j, item) {
                if (!item) continue ;
                // Insert code to obtain more information about the modport declarations
                // More information about module items can be obtained from VeriModuleItem.h
                // Details about the module items are also described within this switch statement
            }

            Message::Info(modport_decl->Linefile(),"here is a 'modport declaration' module item") ;
            break ;
            }
        case ID_VERIPROGRAM:
            { // For programs
            VeriProgram *program = static_cast<VeriProgram*>(module_item) ;
            // eg.:
            // program test (input clk, input [16:1] addr, inout [7:0] data) ;
            // ...
            // endprogram

            VeriIdDef *prog_id = program->GetId() ; // Gets the id of the decl

            VeriModuleItem *param ;
            unsigned j ;
            Array *param_arr = program->GetParameterConnects() ; // Gets an array of VeriDataDecl (a VeriModuleItem)
            FOREACH_ARRAY_ITEM(param_arr, j, param) {
                if (!param) continue ;
                // Insert code to obtain more information about the program parameters
                // More information about program parameters can be obtained from VeriModuleItem.h
                // Details about the module items  are also described within this switch statement
            }

            VeriExpression *port_expr ;
            Array *port_arr = program->GetPortConnects() ;  // Gets an array of VeriAnsiPortDecl/VeriIdRef/VeriIndexedId/VeriConcat
            FOREACH_ARRAY_ITEM(port_arr, j, port_expr) {
                if (!port_expr) continue ;
                // Insert code to obtain more information about the program ports
                // More information about module ports can be obtained from VeriExpression.h
            }

            Array *prog_item_arr = program->GetItems() ; // Gets the program items
            VeriModuleItem *item ;
            FOREACH_ARRAY_ITEM(prog_item_arr, j, item) {
                if (!item) continue ;
                // Insert code to obtain more information about the program items
                // More information about program items can be obtained from VeriModuleItem.h
                // Details about the module items  are also described within this switch statement
            }
            if (prog_id) Message::Info(prog_id->Linefile(),"here '", prog_id->Name(), "' is a 'program' module item") ;
            break ;
            }
        case ID_VERICLASS:
            { // For class
            VeriClass *class_decl = static_cast<VeriClass*>(module_item) ;
            // eg. :
            // class test ;
            // ...
            // endclass

            VeriIdDef *class_id = class_decl->GetId() ; // Gets the id of the class

            Array *param_conn_arr = class_decl->GetParameterConnects() ; // declaration of parameters of this class.. Array of VeriModuleItem
            unsigned j ;
            VeriModuleItem *item ;
            FOREACH_ARRAY_ITEM(param_conn_arr, j, item) {
                if (!item) continue ;
                // Insert code to obtain more information about the class items
                // Details about class parameters is given in file VeriModuleItem.h
                // and also in a case item of this switch construct
            }

            VeriName *veri_base_name = class_decl->GetBaseClassName() ; // Gets the name of the base class
            const char *name = (veri_base_name) ? veri_base_name->GetName() : 0 ; // Gets the name in char* form

            Array *item_arr = class_decl->GetItems() ; // Gets class items (declarations etc). Array of VeriModuleItem
            FOREACH_ARRAY_ITEM(item_arr, j, item) {
                if (!item) continue ;
                // Insert code to obtain more information about the class items
                // Details about class items is given in file VeriModuleItem.h
                // and also in a case item of this switch construct
            }
            if (class_id) Message::Info(class_id->Linefile(),"here '", class_id->Name(), "' is a 'class' module item") ;
            break ;
            }
        case ID_VERIPROPERTYDECL:
            { // For modproperty declarations
            VeriPropertyDecl *property_decl = static_cast<VeriPropertyDecl*>(module_item) ;
            // eg. :
            // property data_end ;
            //     @(posedge mclk)
            //     data_phase |->((irdy==0) && ($fell(trdy) || $fell(stop))) ;
            // endproperty

            VeriIdDef *prop_id = property_decl->GetId() ; // Gets the id of the property

            Array *port_arr = property_decl->GetPorts() ; // Gets the array of ports of the property
            unsigned j ;
            VeriIdDef *port_id ;
            FOREACH_ARRAY_ITEM(port_arr, j, port_id) {
                if (!port_id) continue ;
                // Insert code to learn more about the formals
            }

            Array *decl_arr = property_decl->GetDeclarations() ; // Gets the array of declarations
            VeriModuleItem *item ;
            FOREACH_ARRAY_ITEM(decl_arr, j, item) {
                if (!item) continue ;
                // Insert code to learn more about the formals
                // These module items are mostly VeriDataDecl ie.
                // local variable declarations
            }

            VeriExpression *spec = property_decl->GetSpec() ; // Gets the (property) spec

            if (prop_id) Message::Info(prop_id->Linefile(),"here '", prop_id->Name(), "' is a 'property' module item") ;
            break ;
            }
        case ID_VERISEQUENCEDECL:
            { // For sequence declarations
            VeriSequenceDecl *seq_decl = static_cast<VeriSequenceDecl*>(module_item) ;
            // eg. :
            // sequence s1 ;
            //      @(posedge clk) a ##1 b ##1 c ;
            // endsequence

            VeriIdDef *seq_id = seq_decl->GetId() ; // Gets the id of the property

            unsigned j ;
            Array *port_arr = seq_decl->GetPorts() ; // Gets the array of formal ids of the sequence
            VeriIdDef *port_id ;
            FOREACH_ARRAY_ITEM(port_arr, j, port_id) {
                if (!port_id) continue ;
                // Insert code to learn more about the ports
            }

            Array *decl_arr = seq_decl->GetDeclarations() ; // Gets the array of declarations
            VeriModuleItem *item ;
            FOREACH_ARRAY_ITEM(decl_arr, j, item) {
                if (!item) continue ;
                // Insert code to learn more about the formals
                // These module items are mostly VeriDataDecl ie.
                // local variable declarations
            }

            VeriExpression *spec = seq_decl->GetSpec() ; // The (sequence) spec (implementation of this named sequence)

            if (seq_id) Message::Info(seq_id->Linefile(),"here '", seq_id->Name(), "' is a 'sequence' module item") ;
            break ;
            }
        case ID_VERIMODPORT:
            { // For modports
            VeriModport *modport = static_cast<VeriModport*>(module_item) ;
            // eg.:
            // interface bus_A (input clk) ;
            // ...
            // modport test (input data, output write) ;
            // ...
            // endinterface

            Array *modport_decl_arr = modport->GetModportDecls() ; // Get the array of VeriModportDecl
            VeriModportDecl *decl ;
            unsigned j ;
            FOREACH_ARRAY_ITEM(modport_decl_arr, j, decl) {
                if (!decl) continue ;
                // Insert code to learn more about the Modport decls
                // Details about Modport decl is given in file VeriModuleItem.h
                // and also in a case item of this switch
            }
            Message::Info(modport->Linefile(),"here is a 'modport' module item") ;
            break ;
            }
        case ID_VERIBINDDIRECTIVE:
            { // Bind directive
            VeriBindDirective *bind_dir = static_cast<VeriBindDirective*>(module_item) ;
            // eg. :
            // bind cpu fpu_props fpu_rules_1(a,b,c) ;
            //                or
            // bind cpu : cpu1, cpu2  fpu_props fpu_rules_1(a,b,c) ;

            // Get the target module name, if present
            const char *target_mod_name = bind_dir->GetTargetModuleName() ;

            unsigned i ;
            Array *target_module_inst = bind_dir->GetTargetInstanceList() ;
            VeriName *veri_hier_name ; // Stores the reference to the hierarchical module or instance path
            FOREACH_ARRAY_ITEM(target_module_inst, i, veri_hier_name) {
                if (!veri_hier_name) continue ;
                const char *hier_name = (veri_hier_name) ? veri_hier_name->GetName() : 0 ;
            }

            VeriModuleItem *inst = bind_dir->GetInstantiation() ; // Get the instantiation of the concerned module, program or interface
            // Insert code to learn more about bind directive

            Message::Info(bind_dir->Linefile(),"here is a 'bind directive' module item") ;
            break ;
            }
        case ID_VERICONSTRAINTDECL:
            { // Constraint declaration
            VeriConstraintDecl *constr_decl = static_cast<VeriConstraintDecl*>(module_item) ;
            // eg. :
            // constraint XYPair::c { x < y ; }

            VeriIdDef *constr_id = constr_decl->GetId() ; // Get the id of the constraint

            Array *constr_blk_arr = constr_decl->GetConstraintBlocks() ; // Get the array of expressions
            unsigned j ;
            VeriExpression *expr ;
            FOREACH_ARRAY_ITEM(constr_blk_arr, j, expr) {
                if (!expr) continue ;
                // Insert code to learn more about the constraints
            }
            Message::Info(constr_decl->Linefile(),"here is a 'constraint declaration' module item") ;
            break ;
            }
// End of System Verilog specific module items
        case ID_VERIINITIALCONSTRUCT:
            {  // Initial construct
            // eg. intial begin ... end
            VeriInitialConstruct *initial_stmt = static_cast<VeriInitialConstruct*>(module_item) ;
            stmt = initial_stmt->GetStmt() ; // Gets the statement
            Message::Info(initial_stmt->Linefile(),"here is an 'initial construct' module item") ;
            }
            // No break. Let the cursor fall through
        case ID_VERIALWAYSCONSTRUCT:
            { // Always construct
            // ie. always @(posedge clk)
            VeriAlwaysConstruct *always_stmt ;

            if (!stmt) { // Enter this block only if module item is not an initial construct
                always_stmt = static_cast<VeriAlwaysConstruct*>(module_item) ;
                stmt = always_stmt->GetStmt() ; // If not initial stmt then do GetStmt()
                Message::Info(always_stmt->Linefile(),"here is an 'always construct' module item") ;
            }
            // Determine what statement it is
            switch(stmt->GetClassId()) {
            // ------------------------------------------------------------------------------------
            // The following are all the types of VeriStatements:
            // ------------------------------------------------------------------------------------
            //    VeriAssertion                 VeriConditionalStatement        VeriCaseStatement
            //    VeriBlockingAssign            VeriDelayControlStatement       VeriDisable
            //    VeriDeAssign                  VeriEventTrigger                VeriFor
            //    VeriEventControlStatement     VeriForever                     VeriGenVarAssign
            //    VeriForce                     VeriParBlock                    VeriRelease
            //    VeriNonBlockingAssign         VeriSeqBlock                    VeriStatement
            //    VeriRepeat                    VeriTaskEnable                  VeriWait
            //    VeriSystemTaskEnable          VeriWhile                       VeriDoWhile
            // ------------------------------------------------------------------------------------
            // Details about different statements can be found in VeriStatement.h

    // System Verilog specific statements
            case ID_VERIASSERTION:
                {
                VeriAssertion *assertion_stmt = static_cast<VeriAssertion*>(stmt) ;
                // eg. :
                // assert(foo) $display("%m passed") ; else $display("%m failed") ;

                // 'cover_property' is 0 if this is an immediate assertion, or VERI_ASSERT / VERI_COVER
                // if this is a (concurrent) 'assert property' or 'cover property' statement
                unsigned cover_property = assertion_stmt->GetAssertCoverProperty() ;

                VeriExpression *prop_spec = assertion_stmt->GetPropertySpec() ; // Gets property spec

                VeriStatement *then_stmt = assertion_stmt->GetThenStmt() ; // Gets the true statement

                VeriStatement *else_stmt = assertion_stmt->GetElseStmt() ; // Gets the else statement

                // Insert code to learn more about assertion statements
                Message::Info(assertion_stmt->Linefile(),"here is an 'assertion' statement") ;
                break ;
                }
            case ID_VERIJUMPSTATEMENT:
                {
                VeriJumpStatement *jump_stmt = static_cast<VeriJumpStatement*>(stmt) ;
                // eg :
                // return ret_val ; // returns value from a function
                // break ; // used to break out of a loop
                // continue ; // used to break out of a loop

                unsigned jump_token = jump_stmt->GetJumpToken() ; // Can take up the values VERI_RETURN, VERI_BREAK or VERI_CONTINUE

                VeriExpression *returned_expr = jump_stmt->GetExpr() ; // Get the returned expression in case of a return statement

                VeriIdDef *id = jump_stmt->GetJumpId() ; // Gets the id of the (loop label or subprogram) to which this statement jumps

                Message::Info(jump_stmt->Linefile(),"here is a 'jump' statement") ;
                // Insert code to learn more about the different jump statements
                break ;
                }
            case ID_VERIDOWHILE:
                {
                VeriDoWhile *do_while_stmt = static_cast<VeriDoWhile*>(stmt) ;

                VeriExpression *cond = do_while_stmt->GetCondition() ; // Gets the condition
                VeriStatement *do_while_content = do_while_stmt->GetStmt() ; // Gets 'do while' statement

                Message::Info(do_while_stmt->Linefile(),"here is a 'do while' statement") ;
                break ;
                }
            case ID_VERIFOREACH:
                {
                // ------- Look in "VeriStatement.h" for class description of this object -------
                // Downcast VeriStatement->VeriForeach. Avoid dynamic_cast overhead.
                VeriForeach *foreach_stmt = static_cast<VeriForeach*>(stmt) ;
                // eg. :
                // foreach(A[i,j,k]) ...

                // Get the name of array identifier to be traversed
                VeriName *arr_name =  foreach_stmt->GetArrayName() ;
                // Enter your code here ...

                // Get the array of indexes
                Array *loop_indexes = foreach_stmt->GetLoopIndexes() ;
                unsigned i ;
                VeriIdDef *id ;
                FOREACH_ARRAY_ITEM(loop_indexes, i, id) {
                    // Enter your code here ...
                }
                break ;
                }
            case ID_VERIWAITORDER:
                {
                // ------- Look in "VeriStatement.h" for class description of this object -------
                // Downcast VeriStatement->VeriWaitOrder. Avoid dynamic_cast overhead.
                VeriWaitOrder *wait_order_stmt = static_cast<VeriWaitOrder*>(stmt) ;
                // eg. :
                // "wait_order ( <hierarchical_identifier {, hierarchical_identifier}>) <action_block> "
                //
                // action_block : <statement_or_null>
                //              | [ statement] else <statement>

                // Get the list of events to be triggered in order
                Array *events = wait_order_stmt->GetEvents() ;
                unsigned i ;
                VeriName *name ;
                FOREACH_ARRAY_ITEM(events, i, name) {
                    // Enter your code here ...
                }

                // Get the statement to be executed if events are triggered in prescribed order
                VeriStatement *then_stmt = wait_order_stmt->GetThenStmt() ;
                // Enter your code here ...

                // Get the statement to be executed if events are not triggered in prescribed order
                VeriStatement *else_stmt = wait_order_stmt->GetElseStmt() ;
                // Enter your code here ...
                break ;
                }
            case ID_VERIARRAYMETHODCALL:
                {
                // ------- Look in "VeriStatement.h" for class description of this object -------
                // Downcast VeriStatement->VeriArrayMethodCall. Avoid dynamic_cast overhead.
                VeriArrayMethodCall *array_meth_call_stmt = static_cast<VeriArrayMethodCall*>(stmt) ;
                // eg. :
                //  "<tf_call> with (<expression>)"

                // Get the R.H.S of the 'with' keyword
                VeriExpression *right = array_meth_call_stmt->GetRight() ;
                // Enter your code here ...
                break ;
                }
            case ID_VERIINLINECONSTRAINTSTMT:
                {
                // ------- Look in "VeriStatement.h" for class description of this object -------
                // Downcast VeriStatement->VeriInlineConstraintStmt. Avoid dynamic_cast overhead.
                VeriInlineConstraintStmt *inline_constr_stmt = static_cast<VeriInlineConstraintStmt*>(stmt) ;
                // eg. :
                // "<tf_call> with <constraint_block>"

                // Get the constraint block
                Array *constraint_block = inline_constr_stmt->GetConstraintBlock() ;
                unsigned i ;
                VeriExpression *expr ;
                FOREACH_ARRAY_ITEM(constraint_block, i, expr) {
                    // Enter your code here ...
                }
                break ;
                }
            case ID_VERIRANDSEQUENCE:
                {
                // ------- Look in "VeriStatement.h" for class description of this object -------
                // Downcast VeriStatement->VeriRandsequence. Avoid dynamic_cast overhead.
                VeriRandsequence *randseq_stmt = static_cast<VeriRandsequence*>(stmt) ;
                // eg. :
                // VeriRandsequence implements randsequence statement :
                // "randsequence ([<production_identifier>])
                //     <production> {<production>}
                //  endsequence"

                // Get the name of the production from which sequence starts
                VeriName *start_prod = randseq_stmt->GetStartingProduction() ;
                // Enter your code here ...

                // Get the list of productions
                Array *productions = randseq_stmt->GetProductions() ;
                unsigned i ;
                VeriProduction *prod ;
                FOREACH_ARRAY_ITEM(productions, i, prod) {
                    // Enter your code here ...
                }
                break ;
                }
            case ID_VERICODEBLOCK:
                {
                // ------- Look in "VeriStatement.h" for class description of this object -------
                // Downcast VeriStatement->VeriCodeBlock. Avoid dynamic_cast overhead.
                VeriCodeBlock *code_block = static_cast<VeriCodeBlock*>(stmt) ;
                // VeriCodeBlock implements code block of production  :
                //  { { <data_declarations>} { statement_or_null}}

                // Get the list of declarations
                Array *decls = code_block->GetDecls() ;
                unsigned i ;
                VeriModuleItem *item ;
                FOREACH_ARRAY_ITEM(decls, i, item) {
                    // Enter your code here ...
                }

                // Get the list of statements
                Array *stmts = code_block->GetStmts() ;
                VeriStatement *stmt ;
                FOREACH_ARRAY_ITEM(stmts, i, stmt) {
                    if (!stmt) continue ;
                    // Enter your code here ...
                    Message::Info(stmt->Linefile(),"here is a 'code block' module item") ;
                }
                break ;
                }
    // End of System Verilog statements
            case ID_VERIASSIGN:
                {
                VeriAssign *assign_stmt = static_cast<VeriAssign*>(stmt) ;
                // eg. :
                // r = a ;

                VeriNetRegAssign *net_reg_assign = assign_stmt->GetAssign() ;
                // Insert code to learn more about assertion statements
                Message::Info(assign_stmt->Linefile(),"here is an 'assignment' statement") ;
                break ;
                }
            case ID_VERIBLOCKINGASSIGN:
                {
                VeriBlockingAssign *blocking_stmt = static_cast<VeriBlockingAssign*>(stmt) ;
                // eg. :
                // r = #10 a ;

                VeriExpression *lval = blocking_stmt->GetLVal() ; // Gets the LHS value

                VeriDelayOrEventControl *del_or_evt_ctrl = blocking_stmt->GetControl() ; // Gets the delay or event control expression

                VeriExpression *val = blocking_stmt->GetValue() ; // Gets the value

                // Insert code to learn more about blocking assignment statements
                Message::Info(blocking_stmt->Linefile(),"here is a 'blocking assignment' statement") ;
                break ;
                }
            case ID_VERINONBLOCKINGASSIGN:
                {
                VeriBlockingAssign *nonblocking_stmt = static_cast<VeriBlockingAssign*>(stmt) ;
                // eg. :
                // r <= a ;

                VeriExpression *lval = nonblocking_stmt->GetLVal() ; // Gets the LHS value

                VeriDelayOrEventControl *del_or_evt_ctrl = nonblocking_stmt->GetControl() ; // Gets the delay or event control expression

                VeriExpression *val = nonblocking_stmt->GetValue() ; // Gets the value

                Message::Info(nonblocking_stmt->Linefile(),"here is a 'non-blocking assignment' statement") ;
                // Insert code to learn more about non-blocking assignment statements
                break ;
                }
            case ID_VERICASESTATEMENT:
                {
                VeriCaseStatement *case_stmt = static_cast<VeriCaseStatement*>(stmt) ;
                // eg. :
                // r <= a ;

                unsigned case_style = case_stmt->GetCaseStyle() ; // Can take up the values : VERI_CASEX, VERI_CASEZ and VERI_CASE

                VeriExpression *cond_expr = case_stmt->GetCondition() ; // Gets the condition expression

                Array *case_item_arr = case_stmt->GetCaseItems() ; // Gets the case items
                unsigned j ;
                VeriCaseItem *case_item ;
                FOREACH_ARRAY_ITEM(case_item_arr, j, case_item) {
                    if (!case_item) continue ;
                    unsigned default_flag = case_item->IsDefault() ; // Returns 1 if this is the default case item
                    Array *cond_arr = case_item->GetConditions() ; // Gets the array of conditions for case items
                    VeriExpression *cond_expr ;
                    unsigned k ;
                    FOREACH_ARRAY_ITEM(cond_arr, k, cond_expr) {
                        if (!cond_expr) continue ;
                        // Write code here
                    }
                    VeriStatement *case_stmt = case_item->GetStmt() ;
                }
                Message::Info(case_stmt->Linefile(),"here is a 'case' statement") ;
                // Insert code to learn more about case statements
                break ;
                }
            case ID_VERIDEASSIGN:
                {
                VeriDeAssign *deassign_stmt = static_cast<VeriDeAssign*>(stmt) ;
                // eg. :
                // deassign q ;

                VeriExpression *lval = deassign_stmt->GetLVal() ;

                Message::Info(deassign_stmt->Linefile(),"here is a 'deassign' statement") ;
                // Insert code to learn more about deassign statements
                break ;
                }
            case ID_VERIDISABLE:
                {
                VeriDisable *disable_stmt = static_cast<VeriDisable*>(stmt) ;
                // eg. :
                // disable block_name ;

                VeriName *task_blk_name = disable_stmt->GetTaskBlockName() ; // Get the task or block name
                const char *name = task_blk_name->GetName() ;

                Message::Info(disable_stmt->Linefile(),"here is a 'disable' statement") ;
                // Insert code to learn more about disable statements
                break ;
                }
            case ID_VERIDELAYCONTROLSTATEMENT:
                {
                VeriDelayControlStatement *delay_ctrl_stmt = static_cast<VeriDelayControlStatement*>(stmt) ;
                // eg. :
                // #10 d = (a | b | c) ;

                VeriExpression *delay = delay_ctrl_stmt->GetDelay() ; // Get the delay

                VeriStatement *stmt = delay_ctrl_stmt->GetStmt() ; // Get the delay statement

                Message::Info(delay_ctrl_stmt->Linefile(),"here is a 'delay control' statement") ;
                // Insert code to learn more about delay contol statements
                break ;
                }
            case ID_VERIFORCE:
                {
                VeriForce *force_stmt = static_cast<VeriForce*>(stmt) ;
                // eg. :
                // force d = (a | b | c) ;

                VeriNetRegAssign *net_reg_assign = force_stmt->GetAssign() ;
                if (net_reg_assign){
                    VeriExpression *lval_expr = net_reg_assign->GetLValExpr() ; // Gets the LHS expression
                    VeriExpression *rval_expr = net_reg_assign->GetRValExpr() ; // Gets the RHS expression
                }

                Message::Info(force_stmt->Linefile(),"here is a 'force' statement") ;
                // Insert code to learn more about force statements
                break ;
                }
            case ID_VERIEVENTTRIGGER:
                {
                VeriEventTrigger *evt_trigger = static_cast<VeriEventTrigger*>(stmt) ;
                // eg. :
                //->trig // trigger is an event

                VeriName *evt_trigger_name = evt_trigger->GetEventName() ; // Gets the name
                const char *name = evt_trigger_name->GetName() ; // Gets the name in char* form

                Message::Info(evt_trigger->Linefile(),"here is an 'event trigger' statement") ;
                // Insert code to learn more about event triggers
                break ;
                }
            case ID_VERIGENVARASSIGN:
                {
                VeriGenVarAssign *gen_var_assign = static_cast<VeriGenVarAssign*>(stmt) ;
                // eg. :
                // genvar e, i ;

                const char *gen_var_name = gen_var_assign->GetName() ; // Gets the name

                VeriIdDef *gen_var_id = gen_var_assign->GetId() ; // Gets the id

                VeriExpression *value = gen_var_assign->GetValue() ; // Gets the value of the expression

                Message::Info(gen_var_assign->Linefile(),"here is a 'generate variable assignment' statement") ;
                // Insert code to learn more about generate variables
                break ;
                }
            case ID_VERIFOR:
                {
                VeriFor *for_stmt = static_cast<VeriFor*>(stmt) ;
                // eg :
                // for (i=0; i<size; i++)
                // begin
                // ...
                // end

                Array *initials_arr = for_stmt->GetInitials() ; // Gets the array of VeriModuleItem :the comma separated list of assignments and declarations
                unsigned j ;
                VeriModuleItem *item ;
                FOREACH_ARRAY_ITEM(initials_arr, j, item) {
                    if (!item) continue ;
                    // Insert code to Learn more about for statement initials
                }

                VeriExpression *cond = for_stmt->GetCondition() ; // Gets the test-condition (to stay or leave the loop)

                Array *repetitions_arr = for_stmt->GetRepetitions() ; // Gets the array of VeriStatement : the comma separated list of assignment statements
                FOREACH_ARRAY_ITEM(repetitions_arr, j, item) {
                    if (!item) continue ;
                    // Insert code to Learn more about for statement
                }

                VeriStatement *for_content = for_stmt->GetStmt() ; // Gets 'for' statement
                // Details about statements is given in VeriStatement.h and
                // also in the different case items of the current switch
                Message::Info(for_stmt->Linefile(),"here is a 'for' statement") ;
                break ;
                }
            case ID_VERIFOREVER:
                {
                VeriForever *forever_stmt = static_cast<VeriForever*>(stmt) ;
                // eg. :
                // forever
                // begin
                // ...
                // end

                VeriStatement *forever_content = forever_stmt->GetStmt() ; // Gets 'forever' statement

                // Details about statements is given in VeriStatement.h and
                // also in the different case items of the current switch
                Message::Info(forever_stmt->Linefile(),"here is a 'forever' statement") ;
                break ;
                }
            case ID_VERIWHILE:
                {
                VeriWhile *while_stmt = static_cast<VeriWhile*>(stmt) ;
                // eg. :
                // while (cond) {
                // ...
                // }

                VeriExpression *while_cond = while_stmt->GetCondition() ; // Gets the condition of the while loop

                VeriStatement *while_content = while_stmt->GetStmt() ; // Gets 'while' statement

                // Details about statements is given in VeriStatement.h and
                // also in the different case items of the current switch
                Message::Info(while_stmt->Linefile(),"here is a 'while' statement") ;
                break ;
                }
            case ID_VERIREPEAT:
                {
                VeriRepeat *repeat_stmt = static_cast<VeriRepeat*>(stmt) ;
                // eg. :
                // repeat (tics) @ (posedge clock) ;

                VeriExpression *repeate_cond = repeat_stmt->GetCondition() ; // Gets the condition of the repeat loop

                VeriStatement *repeat_content = repeat_stmt->GetStmt() ; // Gets 'repeat' statement

                // Details about statements are given in VeriStatement.h and
                // also in the different case items of the current switch
                Message::Info(repeat_stmt->Linefile(),"here is a 'repeat' statement") ;
                break ;
                }
            case ID_VERIRELEASE:
                {
                VeriRelease *release_stmt = static_cast<VeriRelease*>(stmt) ;
                // eg. :
                // release var ;

                VeriExpression *lval = release_stmt->GetLVal() ; // Gets the left hand side value of the 'force' statement

                Message::Info(release_stmt->Linefile(),"here is a 'release' statement") ;
                break ;
                }
            case ID_VERISYSTEMTASKENABLE:
                {
                VeriSystemTaskEnable *sys_task_en_stmt = static_cast<VeriSystemTaskEnable*>(stmt) ;

                const char *name = sys_task_en_stmt->GetName() ; // Gets the name

                Array *args_arr = sys_task_en_stmt->GetArgs() ; // Gets an array of VeriExpression
                unsigned j ;
                VeriExpression *expr ;
                FOREACH_ARRAY_ITEM(args_arr, j, expr) {
                    if (!expr) continue ;
                    // Details about expressions are given in VeriExpression.h and
                    // also in the different case items of the current switch
                    // Insert code to Learn more system task enabling
                }

                unsigned func_type = sys_task_en_stmt->GetFunctionType() ; // Get (function) token for this system task. 0 if not known

                Message::Info(sys_task_en_stmt->Linefile(),"here is a 'system task enable' statement") ;
                break ;
                }
            case ID_VERITASKENABLE:
                {
                VeriTaskEnable *task_en_stmt = static_cast<VeriTaskEnable*>(stmt) ;

                VeriName *name = task_en_stmt->GetTaskName() ; // Gets the name

                Array *args_arr = task_en_stmt->GetArgs() ; // Array of VeriExpression*. Can contain VeriPortConnect. The task-enable arguments
                unsigned j ;
                VeriExpression *expr ;
                FOREACH_ARRAY_ITEM(args_arr, j, expr) {
                    if (!expr) continue ;
                    // Details about expressions are given in VeriExpression.h and
                    // also in the different case items of the current switch
                    // Insert code to Learn more system task enabling
                }
                Message::Info(task_en_stmt->Linefile(),"here is a 'task enable' statement") ;
                break ;
                }
            case ID_VERIWAIT:
                {
                VeriWait *wait_stmt = static_cast<VeriWait*>(stmt) ;
                // eg. :
                // wait (!enable) #10 a = b ;

                VeriExpression *cond = wait_stmt->GetCondition() ; // Gets the wait condition

                VeriStatement *wt_stmt = wait_stmt->GetStmt() ; // Gets the wait statement

                Message::Info(wait_stmt->Linefile(),"here is a 'wait' statement") ;
                break ;
                }
            case ID_VERIPARBLOCK:
                {
                VeriParBlock *par_blk_stmt = static_cast<VeriParBlock*>(stmt) ;
                // eg. :
                // fork
                // ...
                // join

                VeriIdDef *label_id = par_blk_stmt->GetLabel() ; // Gets the block label

                Array *decl_item_arr = par_blk_stmt->GetDeclItems() ; // Gets the array of VeriModuleItem.  The declaration items^M
                unsigned j ;
                VeriModuleItem *item ;
                FOREACH_ARRAY_ITEM(decl_item_arr, j, item) {
                    if (!item) continue ;
                    // Insert code to Learn more about parallel block declarations
                }

                Array *stmt_arr = par_blk_stmt->GetStatements() ; // Gets the array of VeriStatement. The block statements
                VeriStatement *par_stmt ;
                FOREACH_ARRAY_ITEM(stmt_arr, j, par_stmt) {
                    if (!par_stmt) continue ;
                    // Insert code to Learn more about parallel block statements
                }

                Message::Info(par_blk_stmt->Linefile(),"here is a 'parallel block' statement") ;
                break ;
                }
            case ID_VERICONDITIONALSTATEMENT:
                {
                VeriConditionalStatement *if_statement = static_cast<VeriConditionalStatement*>(stmt) ;
                // eg.    if (rst!==0)
                //            #1 Q=D ;
                //        else
                //            begin
                //               wait (rst==0) ;
                //               Q=0 ;
                //               wait (rst==1) ;
                //            end

                // Get the "if (rst!==0)" expression
                VeriExpression *if_expr = if_statement->GetIfExpr() ;

                // Get the "#1 Q=D ;" statement
                VeriStatement *then_stmt = if_statement->GetThenStmt() ;

                // Get the else statement
                VeriStatement *else_stmt = if_statement->GetElseStmt() ;

                Message::Info(if_statement->Linefile(),"here is a 'conditional' statement") ;
                break ;
                }
            case ID_VERISEQBLOCK: // Sequential block
                {
                VeriSeqBlock *seq_block = static_cast<VeriSeqBlock*>(stmt) ;
                // eg.:
                //      begin
                //          wait (rst==0) ;
                //          Q=0 ;
                //          wait (rst==1) ;
                //      end

                // Get a handle to the block declarations
                // Array of VeriModuleItem)

                Array *block_decl = seq_block->GetDeclItems() ;

                // Get a handle to the block statements
                // Array of VeriStatement
                Array *statements = seq_block->GetStatements() ;
                VeriStatement *block_stmt ;
                unsigned j ;
                FOREACH_ARRAY_ITEM(statements, j, block_stmt) {
                    // Do what you want with the statement here.
                }

                // Get a handle to the block's scope
                VeriScope *scope = seq_block->GetScope() ;  // should have a scope
                if (!scope) {
                    Message::Error(seq_block->Linefile(),"found a block without a scope, expected one with scope!") ;
                }

                Message::Info(seq_block->Linefile(),"here is a 'sequential block' statement") ;
                // More VeriScope discussion toward the end of this test example.
                break ;
                }
            case ID_VERIEVENTCONTROLSTATEMENT:
                {
                // ie. @(posedge clk) event_stmt

                VeriEventControlStatement *event_control = static_cast<VeriEventControlStatement*>(stmt) ;

                // Get the list of events
                Array *atIds = event_control->GetAt() ;

                // Get event statement:
                VeriStatement *event_stmt = event_control->GetStmt() ;

                // More information about statements can be obtained from
                Message::Info(event_control->Linefile(),"here is an 'event control' statement") ;
                break ;
                }
            default:
                Message::Error(stmt->Linefile(),"unknown statement found") ;
            }
            break ;
            }
        case ID_VERIMODULE:
            {
            // eg.:
            // module test ;
            //     module inner ;
            //     endmodule
            // endmodule
            // VeriModule appears in module item for nested module.
            VeriModule *module = static_cast<VeriModule*>(module_item) ;
            Message::Info(module->Linefile(),"here is a module as module item") ;
            // Properties of module have been described earlier
            break ;
            }
        default:
            // For this testcase, this should not get triggered!
            Message::Error(module_item->Linefile(),"unknown module item found") ;
        }
    }

    // Get a handle to the scope that his module was declared in
    VeriScope *scope = module->GetScope() ;
    if(!scope) {
        Message::Error(0, "module shall always have a scope");
    }
    // A VeriScope is a symbol table of locally declared identifiers.
    // These VeriIdDef's are stored as (char*->VeriIdDef*) in a Map (associated hash table) container.
    // The identifier that owns (created) this scope is available with VeriScope::Owner(),
    // where the owner is a module, a task, a function or a named block.

    // The upper level scopes are (containing this one) is accessible with VeriScope::Upper(),
    // and the lower level scopes are accessible with VeriScope::SubScope(char* name).
    // For Verilog a module, has no upper level scope, but can have lower-level scopes
    // for blocks and subprograms and such created within the module.

    // For a System Verilog module, if declared in a library, the upper level scope
    // is the scope of $unit. Otherwise if the module is a nested module then the upper
    // level scope is the scope of the parent module.

    // Iterate through the present-level scope table (Map)
    MapIter map_iterator ; // keeps track of where we are in the Map.
    Map *decl_area = scope->DeclArea() ;
    char *id_name ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(decl_area, map_iterator, &id_name, &id) {
        // Each identifier that is declared in the scope is here :
        // Its VeriIdDef*structure is in 'id'.
    }

    // Regular search (direct visibility) is done with VeriScope::Find(name). This looks through
    // the scope for the given identifier by name. Returns 0 if not found.
    // Find object by name, traverse upward if not found local.
    // VeriIdDef *id_q = scope->Find("Insert the name of the variable whose scope you want to look up over here") ;
    // Find object if declared in this immediate, local scope
    // id_q = scope->FindLocal("Insert the name of the variable whose scope you want to look up over here") ;

    return 0 ; // status OK.
}
