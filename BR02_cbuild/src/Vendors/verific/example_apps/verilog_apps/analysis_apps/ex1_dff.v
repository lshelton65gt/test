// -*- verilog -*-
/******************************************************/   
/*   module dff                                       */   
/******************************************************/   
// D flip-flop   
// -------------

module dff(D, Q, clk, rst);
    parameter CARDINALITY = 1;

    output  [CARDINALITY-1:0] Q; 
    input   [CARDINALITY-1:0] D;
    input   clk, rst;   

    reg     [CARDINALITY-1:0] Q;   
    wire    [CARDINALITY-1:0] D;   

    always @(posedge clk)
        if (rst!==0) 
            #1 Q=D;
        else
            begin 
                wait (rst==0); 
                Q=0; 
                wait (rst==1); 
            end    
endmodule
