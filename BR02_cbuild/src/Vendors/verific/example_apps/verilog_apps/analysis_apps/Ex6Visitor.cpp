/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Ex6Visitor.h"     // Visitor base class definition
#include "Set.h"            // Make associated hash table class Set available

#include "Message.h"        // Make message handlers available

#include "veri_file.h"      // Make verilog reader available

#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriConstVal.h"   // Definitions of VeriIntVal
#include "VeriId.h"         // Definitions of all verilog identifier tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "Strings.h"        // Definition of string class
#include "VeriLibrary.h"    // Definition of VeriLibrary class
#include "VeriScope.h"      // Definition of VeriScope class

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/* -------------------------------------------------------------------------- */

void AlwaysVisitor::Visit(VeriModule &node)
{
    VeriScope *old_scope = _scope ; // Save previous scope
    _scope = node.GetScope() ; // Set the scope to the scope of the module
    Array *items = node.GetItems() ;
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(items, i, item) { // Visit each of the module items
        if (item) item->Accept(*this) ;
    }
    _scope = old_scope ; // Reset scope
}

void AlwaysVisitor::Visit(VeriGenerateConditional &node)
{
    VeriScope *old_scope = _scope ; // Save previous scope
    if (node.GetThenItem()) {
        _scope = node.GetThenScope() ; // Set the scope to the scope of the 'if' part
        node.GetThenItem()->Accept(*this) ;
    }
    if (node.GetElseItem()) {
        _scope = node.GetElseScope() ; // Set the scope to the scope of the 'else' part
        node.GetElseItem()->Accept(*this) ;
    }
    _scope = old_scope ; // Reset scope to previous scope
}

void AlwaysVisitor::Visit(VeriGenerateCase &node)
{
    Array *items = node.GetCaseItems() ;
    unsigned i ;
    VeriGenerateCaseItem *case_item ;
    FOREACH_ARRAY_ITEM(items, i, case_item) {
        if (!case_item) continue ;
        VeriScope *old_scope = _scope ; // Save previous scope
        _scope = case_item->GetScope() ;
        VeriModuleItem* mod_item = case_item->GetItem() ;
        if (mod_item) mod_item->Accept(*this) ;
        _scope = old_scope ; // Reset scope to previous scope
    }
}

void AlwaysVisitor::Visit(VeriGenerateFor &node)
{
    VeriScope *old_scope = _scope ; // Save previous scope
    _scope = node.GetScope() ; // Set the scope to the scope of the for-generate construct
    Array *items = node.GetItems() ;
    VeriModuleItem *item ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(items, i, item) {
        if (item) item->Accept(*this) ; // Visit each of the 'for-generate' items
    }
    _scope = old_scope ; // Reset scope to previous scope
}

void AlwaysVisitor::Visit(VeriGenerateBlock &node)
{
    VeriScope *old_scope = _scope ; // Save previous scope
    _scope = node.GetScope() ; // Set the scope to the scope of the generate block
    Array *items = node.GetItems() ;
    VeriModuleItem *item ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(items, i, item) {
        if (item) item->Accept(*this) ; // Visit each of the 'generate-block' items
    }
    _scope = old_scope ; // Reset scope to previous scope
}

void AlwaysVisitor::Visit(VeriAlwaysConstruct &node)
{
    // Create a new UserId class object and pass the parent scope of the
    // 'always' construct to it.
    EventControlIdVisitor uid_visitor(_scope) ;

    // Visit the statements within the always block. Modify sensitivity list
    // only if the 'always' construct has an implicit event expression list.
    node.Accept(uid_visitor) ;
}

/* -------------------------------------------------------------------------- */

void EventControlIdVisitor::Visit(VeriSeqBlock &node)
{
    VeriScope *old_scope = _scope ; // Save previous scope
    Array *stmts = node.GetStatements() ;
    _scope = node.GetScope() ? node.GetScope() : old_scope ; // Set the scope to the current scope
    unsigned i ;
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(stmts, i, stmt) {
        if (stmt) stmt->Accept(*this) ; // Visit each statement of the sequence block
    }
    _scope = old_scope ; // Reset scope to previous scope
}

void EventControlIdVisitor::Visit(VeriParBlock &node)
{
    VeriScope *old_scope = _scope ; // Save previous scope
    Array *stmts = node.GetStatements() ;
    _scope = node.GetScope() ? node.GetScope() : old_scope ; // Set the scope to the current scope
    unsigned i ;
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(stmts, i, stmt) {
        if (stmt) stmt->Accept(*this) ; // Visit each statement of the parallel block
    }
    _scope = old_scope ; // Reset scope to previous scope
}

void EventControlIdVisitor::Visit(VeriEventControlStatement &node)
{
    Array *at = node.GetAt() ;
    // For an expression like @(*) the array 'at' is empty.
    if (at && at->Size()) return ; // expression is not of type '@ *', retur
    VeriStatement *stmt = node.GetStmt() ; // Get the statement under implicit event
    if (!stmt) return ;

    EventControlIdVisitor id_visitor (_scope) ;
    stmt->Accept(id_visitor) ; // Traverse statements under '@(*)' to populate used identifiers

    // Now collect ids
    Set *ids = id_visitor.GetIds() ;
    VeriIdDef *id ;
    SetIter iter ;
    FOREACH_SET_ITEM(ids, iter, &id ) {
        if (!id) continue ;
        // If the id does not exist in the current scope then it shouldn't be
        // in the sensitivity list as it is declared in the local scope of the
        // event control statement.
        VeriIdDef *found_id = (_scope) ? _scope->Find(id->GetName()) : 0 ;
        if (found_id != id)  continue ;

        VeriIdRef *ref = new VeriIdRef(id) ; // Create a new id ref
        if (!ref) return ; // In case of an error return from function
        ref->SetLinefile(node.Linefile()) ; // Set the line file info

        // Create a new event expression
        VeriEventExpression *ev_expr = new VeriEventExpression(0, ref) ;
        if (ev_expr) {
            ev_expr->SetLinefile(node.Linefile()) ; // Set the line file info
            // Insert the new event expression in the list of arguments for at
            at->Insert(ev_expr) ;
        }
    }
}

void EventControlIdVisitor::Visit(VeriBlockingAssign &node)
{
    VeriExpression *lval = node.GetLVal() ; // Get the L.H.S expression
    // If there is a bit or part-select expression in the L.H.S, then the index(es)
    // needs to be considered to create the sensitivity list. Moreover indexed
    // expression can be used in L.H.S as prefix of another expression or as an
    // element of concatenation.
    LhsVisitor obj(this) ; // Create visitor to visit L.H.S, so that we can consider index(es)
    if (lval) lval->Accept(obj) ;

    VeriDelayOrEventControl *control = node.GetControl() ; // Get control and visit that
    if (control) control->Accept(*this) ;
    VeriExpression *val = node.GetValue() ;
    if (val) val->Accept(*this) ; // Traverse the R.H.S
}

void EventControlIdVisitor::Visit(VeriNonBlockingAssign &node)
{
    VeriExpression *lval = node.GetLVal() ; // Get the L.H.S expression
    // If there is a bit or part-select expression in the L.H.S, then the index(es)
    // needs to be considered to create the sensitivity list. Moreover indexed
    // expression can be used in L.H.S as prefix of another expression or as an
    // element of concatenation.
    LhsVisitor obj(this) ; // Create visitor to visit L.H.S, so that we can consider index(es)
    if (lval) lval->Accept(obj) ;

    VeriDelayOrEventControl *control = node.GetControl() ; // Get control and visit that
    if (control) control->Accept(*this) ;
    VeriExpression *val = node.GetValue() ;
    if (val) val->Accept(*this) ; // Traverse the R.H.S
}

void EventControlIdVisitor::Visit(VeriIdRef &node)
{
    VeriIdDef *id = node.GetId() ;
    if (id && (id->IsNet() || id->IsVar())) _ids.Insert(id) ; // Insert the id in the set
}

void EventControlIdVisitor::Visit(VeriTaskEnable &node)
{
    // Find out if the task has been resolved or not
    VeriName *task_name = node.GetTaskName() ;
    // Pick-up the resolved task identifier
    VeriIdDef *task_id = (task_name) ? task_name->GetId() : 0 ;
    // Now get the formal parameters and the scope of the task
    Array *formals = (task_id) ? task_id->GetPorts() : 0 ;
    VeriScope *formal_scope = (task_id) ? task_id->LocalScope() : 0 ; // Get the formal scope
    VeriIdDef *formal ;
    Array *args = node.GetArgs() ; // Get the actual arguments
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(args, i, expr ) {
        if (!expr) continue ; // unconnected formal
        // Find the formal
        formal = 0 ;
        if (expr->NamedFormal()) {
            // This is a named formal.
            formal = (formal_scope) ? formal_scope->FindLocal(expr->NamedFormal()) : 0 ;
        } else {
            // This is an ordered connection
            formal = (formals && formals->Size() > i) ? (VeriIdDef*)formals->At(i) : 0 ;
        }

        if (formal && formal->IsOutput()) { // Formal is output
            // 'expr' is like left hand side of an assignment, so we should not
            // consider assigned identifiers, but we should consider the index/range (if any)
            // for the sensitivity list
            LhsVisitor obj(this) ; // Visit as left hand side
            expr->Accept(obj) ;
            continue ;
        }
        expr->Accept(*this) ;
    }
}

void EventControlIdVisitor::Visit(VeriFunctionCall &node)
{
    // Pick-up the resolved func identifier
    VeriIdDef *func_id = node.GetId() ;
    // Now get the formal parameters and the scope of the func
    Array *formals = (func_id) ? func_id->GetPorts() : 0 ;
    VeriScope *formal_scope = (func_id) ? func_id->LocalScope() : 0 ; // Get the formal scope
    VeriIdDef *formal ;
    Array *args = node.GetArgs() ; // Get the actual arguments
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(args, i, expr ) {
        if (!expr) continue ; // unconnected formal
        // Find the formal
        formal = 0 ;
        if (expr->NamedFormal()) {
            // This is a named formal.
            formal = (formal_scope) ? formal_scope->FindLocal(expr->NamedFormal()) : 0 ;
        } else {
            // This is an ordered connection
            formal = (formals && formals->Size() > i) ? (VeriIdDef*)formals->At(i) : 0 ;
        }
        // If the formal exists and is not an 'output port' then visit it.
        if (formal && formal->IsOutput()) {
            // 'expr' is like left hand side of an assignment, so we should not
            // consider assigned identifiers, but we should consider the index/range (if any)
            // for the sensitivity list
            LhsVisitor obj(this) ; // Visit as left hand side
            expr->Accept(obj) ;
        }
        expr->Accept(*this) ;
    }
}

/* -------------------------------------------------------------------------- */

void LhsVisitor::Visit(VeriIndexedId &node)
{
    // Get prefix and visit that
    VeriName *prefix = node.GetPrefix() ;
    if (prefix) prefix->Accept(*this) ;

    // Indexed part is used and should contribute in sensitivity list. So visit
    // that using EventControlIdVisitor
    VeriExpression *idx = node.GetIndexExpr() ;
    if (idx) idx->Accept(*_used_part_visitor) ;
}

void LhsVisitor::Visit(VeriIndexedMemoryId &node)
{
    // Get prefix and visit that
    VeriName *prefix = node.GetPrefix() ;
    if (prefix) prefix->Accept(*this) ;

    // Indexed part is used and should contribute in sensitivity list. So visit
    // that using EventControlIdVisitor
    Array *indexes = node.GetIndexes() ;
    unsigned i ;
    VeriExpression *idx ;
    FOREACH_ARRAY_ITEM(indexes, i,idx) {
        if (idx) idx->Accept(*_used_part_visitor) ;
    }
}

/* -------------------------------------------------------------------------- */

