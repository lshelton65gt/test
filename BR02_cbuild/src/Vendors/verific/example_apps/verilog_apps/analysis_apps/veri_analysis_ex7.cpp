/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>         // strcpy, strchr ...
#include <ctype.h>          // isalpha, etc ...
#include <sstream>

#include "Message.h"        // Make message class available
#include "veri_file.h"      // Make Verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriTreeNode.h"   // Definition of VeriTreeNode
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "VeriId.h"         // Definitions of all identifier definition tree nodes
#include "VeriScope.h"      // Symbol table of locally declared identifiers
#include "Strings.h"        // Definition of class to manipulate copy, concatenate, create etc...
#include "LineFile.h"

#include "veri_tokens.h"    // Definition of port direction VERI_OUTPUT, etc ...

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*
    BUILD NOTE!!! : This file needs to be compiled with the same verilog compile switches,
                    that the verilog lib was built with, otherwise strange behavior may occur!
*/

/****************************************************************************************
******************************* VERILOG EXAMPLE #7 **************************************
****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    if (argc < 2) Message::PrintLine("Default input file: ex7_top.v. Specify command line argument to override") ;

    // Get the file name to work on. If not specified as command line arg use default file
    const char *file_name = (argc > 1) ? argv[1] : "ex7_top.v" ;

    // Analyze the design file(in verilog2k mode), if there is any error, don't proceed further!
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veri_file::Analyze(file_name, 1/*Verilog 2000*/)) return 1 ;

    // Get all the top level modules
    Array *all_top_modules = veri_file::GetTopModules("work") ;
    // Get a handle of the first top level module, if any.
    VeriModule *mod = (all_top_modules && all_top_modules->Size()) ? (VeriModule *)all_top_modules->GetFirst() : 0 ;
    delete all_top_modules ;
    if (!mod) {
        Message::PrintLine("Cannot find a top level module") ;
        return 1 ;
    }

    //---------------------------------------
    //  Pretty Print (Pre Parse-tree Modification)
    //---------------------------------------

    char *before_print = Strings::save(mod->Name(), "_before_pp.v") ;

    // Pretty print current design
    veri_reader.PrettyPrint(before_print, 0, "work") ;
    Strings::free(before_print) ;

    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
     |     Change the expression of type logical and to logical or   |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    //---------------------------------------------------------------------
    // Get a handle to the top module that was just analyzed
    //---------------------------------------------------------------------
    // Module 'mod' is the handle to the top level module
    Array *mitems = mod->GetModuleItems() ;
    VeriModuleItem *item ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(mitems, i, item) {
        if (!item) continue ;
        if (item->GetClassId() == ID_VERICONTINUOUSASSIGN) {
            Array* net_assigns = ((VeriContinuousAssign*)item)->GetNetAssigns() ;
            VeriNetRegAssign *assign ;
            unsigned j ;
            FOREACH_ARRAY_ITEM(net_assigns, j, assign) {
                if (!assign) continue ;
                VeriExpression *rval = assign->GetRValExpr() ;
                if (rval->IsConcat()) {
                    Array *exprs = rval->GetExpressions() ;
                    Array *new_exprs = new Array(exprs->Size()) ;
                    unsigned k ;
                    VeriExpression *expr ;
                    FOREACH_ARRAY_ITEM(exprs, k, expr) {
                        if (!expr) continue ;
                        if (expr->GetLeft() && expr->GetRight()) {
                            std::stringstream stream1 ;
                            expr->GetLeft()->PrettyPrint(stream1, 0) ;
                            char *expr_left = Strings::save(stream1.str().c_str()) ;

                            std::stringstream stream2 ;
                            expr->GetRight()->PrettyPrint(stream2, 0) ;
                            char *expr_right = Strings::save(stream2.str().c_str()) ;
                            char *expr_str = Strings::save(expr_left, " && ", expr_right) ;

                            Strings::free(expr_left) ; // mem cleanup
                            Strings::free(expr_right) ; // mem cleanup

                            VeriExpression * new_expr = veri_file::AnalyzeExpr(expr_str, 1) ;
                            Strings::free(expr_str) ; // mem cleanup

                            if (!new_expr) continue ; // Something wrong happened
                            VeriScope *scope = mod->GetScope() ;
                            new_expr->Resolve(scope, VeriTreeNode::VERI_EXPRESSION) ;
                            new_exprs->InsertLast(new_expr) ;
                        }
                    }

                    VeriExpression *old_expr ;
                    FOREACH_ARRAY_ITEM(exprs, k, old_expr) delete old_expr ;
                    exprs->Reset() ;

                    FOREACH_ARRAY_ITEM(new_exprs, k, expr) exprs->InsertLast(expr) ;
                    delete new_exprs ;
                }
            }
        }
    }

    //---------------------------------------
    // Pretty Print (Post Parse-tree Modification)
    //---------------------------------------
    // To make sure that everything went as planned, pretty print the current
    // parse-tree's output and compare with the previous pretty printed output.

    char *after_print = Strings::save(mod->Name(), "_after_pp.v") ;
    veri_reader.PrettyPrint(after_print, 0, "work") ;

    // ALL DONE!!!

    Strings::free(after_print) ; // Free the dynamically allocated strings

    return 1 ;
}

/* ------------------------------------------------------- */

