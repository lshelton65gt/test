/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

// containers used :
#include "Array.h" // a dynamic array
#include "Set.h"   // a simple hash table
#include "Map.h"   // an associated hash table

// utilities used :
#include "Strings.h"        // Definition of class to easily create/concatenate char*s.
#include "Message.h"        // Message handler
#include "TextBasedDesignMod.h"  // Text-Based Design-Modification (TextBasedDesignMod) utility

// Verilog parser, main interface :
#include "veri_file.h"      // Make Verilog reader available
#include "VeriVisitor.h"    // Make the parse-tree visitor pattern available

// Verilog parse tree API :
#include "VeriModule.h"     // Definition of a VeriModule
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriId.h"         // Definitions of all identifier definition tree nodes

#include "veri_tokens.h"    // Definition of port direction VERI_OUTPUT, etc ...

/*
    BUILD NOTE!!! : This file needs to be compiled with the same verilog compile switches,
                    that the verilog lib was built with, otherwise strange behavior may occur!
*/

/****************************************************************************************
******************************* SCAN INSERT VERILOG EXAMPLE  ****************************
*****************************************************************************************

                                APPLICATION NOTE:

    This example shows how 'clock' and 'reset' can be extracted from an analyzed Verilog parse tree.

    This example assumes that the Verilog design is written in an RTL coding style, but
    since no elaboration is necessary, it is much faster and less memory intensive than
    extracting clocks from an (RTL) elaborated model.

    To find clock and reset signals in a Verilog design, this example traveres the parse trees to
    finds the 'event-control' statements and the conditional statements underneight.

    The example will show the following operations on analyzed verilog design.

        0) Parse the input file.
        1) For each module in the design, perform the following steps :
        2) Traverse the parse tree to look for event-control statements under always statements in the module
        3) traverse the conditional (if) statements under the event-control statement, and
        4) Prune down the sensitivity list to find set/reset conditions, so that
        5) the last 'else' statement only executes under the single remaining 'clock' signal.
        6) Print out the set/reset condition signals and the clock signal for each always statement.

    This application automatically chooses a default input file when none is given

*****************************************************************************************/

// First, define visitor patterns which we will use :

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// A visitor patteren, which will find the event control statements in a parse tree :
// VeriVisitor is a general purpose Verific Verilog parse tree traversal pattern.
// It is finetuned here to collect event control statements inside always constructs
// (skip initial blocks) :
class EventControlVisitor : public VeriVisitor
{
public :
    EventControlVisitor() : VeriVisitor(), _event_control_stmts() { } ;
    ~EventControlVisitor() {} ;

    virtual void Visit(VeriInitialConstruct &node) {
        node.Info("skipping this initial block") ;
    }

    // Collect event control statements :
    virtual void Visit(VeriEventControlStatement &node) {
        node.Info("visiting this event control statement") ;
        _event_control_stmts.InsertLast(&node) ;
    }

public :
    Array _event_control_stmts ; // array of VeriEventControlStatement *'s, collected by this visitor
} ;

// A visitor patteren, which will find identifiers referenced in an expression,
// and store them on the _referenced_ids Set.
class FindReferencedIds : public VeriVisitor
{
public :
    FindReferencedIds() : VeriVisitor(), _referenced_ids(POINTER_HASH) { } ;
    ~FindReferencedIds() {} ;

    // Catch VeriIdRefs :
    virtual void Visit(VeriIdRef &node) {
        // Get the identifier
        VeriIdDef *id = node.GetId() ;
        // And store in the Set :
        _referenced_ids.Insert(id) ;
    }

public :
    Set _referenced_ids ; // Set of VeriIdDef *'s collected by this visitor
} ;

#ifdef VERIFIC_NAMESPACE
} ; // end definitions in verific namespace
#endif

/*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
using namespace Verific ; // start using Verific namespace
#endif

int main(int argc, char **argv)
{
   if (argc < 2) Message::PrintLine("Default input file: pre3.v. Specify command line argument to override") ;

     // Get the file name to work on. If not specified as command line arg use default file
    const char *file_name = (argc > 1) ? argv[1] : "prep3.v" ;

    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    // Now analyze the above example Verilog file :
    // If there is any error, don't proceed further
    if (!veri_reader.Analyze(file_name)) return 1 ;

    // Step (1) : Loop over all analyzed modules :
    VeriModule *module ;
    MapIter mi ;
    FOREACH_VERILOG_MODULE(mi, module) {
        if (!module) continue ;

        module->Info("search module %s for clocked statements", module->Name()) ;

        // Step (2) : Find the clocked always statements in the module
        // We use a visitor pattern for that.
        // Collect all the 'event_control_statements' :
        // This will collect the statements appearing immediately inside a always construct : "always @(...) stmt" :
        EventControlVisitor event_control_visitor ;
        module->Accept( event_control_visitor ) ; // traverse and collect..

        // Check each event control statement found in the design :
        VeriEventControlStatement *event_control_stmt ;
        unsigned i, j ;
        FOREACH_ARRAY_ITEM(&(event_control_visitor._event_control_stmts), i, event_control_stmt) {
            if (!event_control_stmt) continue ; // null-pointer check

            // Check if this event control statement is clocked :
            // Look at the sensitivity list (the "@()" clause), and check if it has 'edge' expressions :
            Array *sens_list = event_control_stmt->GetAt() ;
            unsigned is_clocked = 0 ;
            VeriExpression *event_expr ;
            FOREACH_ARRAY_ITEM(sens_list, j, event_expr) {
                if (!event_expr) break ; // invalid sensitivity list
                if (event_expr->IsEdge(0/*any edge (pos or neg)*/)) {
                    is_clocked = 1 ;
                    break ; // no need to check the other sensitivity list items. This statement is clocked !
                }
            }
            if (!is_clocked) continue ; // not a clocked event-control statement

            // Found an event-control statement (@(sens_list) <statement>)
            event_control_stmt->Info("found one clocked event control statement here") ;

            // If there is more than one signal on the sensitivity list, then
            // the actual clocked statement should have nested if-statements with
            // (asynchronous) conditions matching the sensitivity list edges.

            // Set up a visitor that will collect asynchronous set/reset signals (if any) from the condition expressions :
            FindReferencedIds condition_visitor ;

            // Start with the immediate event-controlled statement itself.
            VeriStatement *clocked_stmt = event_control_stmt->GetStmt() ;

            // Step (3) traverse the conditional (if) statements under the event-control statement, and
            // Decent into the nested if statements until we found all BUT ONE (the clock)
            // of the asynchronous 'conditions' from the sensitivity list.
            while (sens_list->Size() > condition_visitor._referenced_ids.Size()+1) {
                if (!clocked_stmt) break ; // invalid, or non-existing clocked statement

                // If there are multiple signals on the sensitivity list, there will be (should be)
                // multiple 'asynchronous' nested (set/reset) if-conditions in the always statement.
                //
                // Here, find these nested 'if' statements (and their conditions).

                // Can do this with a visitor, or otherwise, but for explanation purposes,
                // we do this the 'quick-and-dirty' way, using GetClassId and static casts to traverse into the statement :

                // There could be a 'sequential' block statement (begin/end) with one if statement in there :
                if (clocked_stmt->GetClassId() == ID_VERISEQBLOCK) {
                    VeriSeqBlock *block = (VeriSeqBlock*)clocked_stmt ;
                    Array *stmts = block->GetStatements() ;
                    // Find the if statement in here :
                    VeriStatement *tmp_stmt ;
                    FOREACH_ARRAY_ITEM(stmts, j, tmp_stmt) {
                        if (!tmp_stmt) continue ;
                        if (tmp_stmt->GetClassId() == ID_VERICONDITIONALSTATEMENT) {
                            // found the if-statement in this seq block.
                            // There should actually only be one statement any way...(fix me : test that?)
                            clocked_stmt = tmp_stmt ;
                            break ;
                        }
                    }
                    // Now drop through to the if-statement processing :
                }

                // There should be an 'if' statement here, or else this is not a synthesizable event control statement.
                if (clocked_stmt->GetClassId() == ID_VERICONDITIONALSTATEMENT) {
                    // Cast to the if-statement class :
                    VeriConditionalStatement *if_stmt = (VeriConditionalStatement*)clocked_stmt ;
                    // Get the if-condition :
                    VeriExpression *if_condition = if_stmt->GetIfExpr() ;
                    // Here, knock out the sens list identifier (reset signal(s)) that occur in the if-condition expression:
                    // Collect the signals that are referenced in the condition, since these
                    // will be the 'asynchronous' set/reset conditions :
                    if_condition->Accept(condition_visitor) ;

                    // Decent into 'else' part of the if-statement, since there may be more conditions there :
                    clocked_stmt = if_stmt->GetElseStmt() ;
                } else {
                    // No 'if' statement, but multiple signals on sense-list.
                    // This may be a mistake in the algorithm, or a faulty design
                    clocked_stmt->Error("cannot find clocked statement in this event-control statement") ;
                    clocked_stmt = 0 ;
                }
            }
            if (!clocked_stmt) {
                continue ; // invalid, or nonexisting clocked statement
            }

            // Step (4) Prune down the sensitivity list to find set/reset conditions, so that
            // Step (5) the last 'else' statement only executes under the single remaining 'clock' signal.

            // Now that we collected the asynchronous set/reset signals, find out which signal
            // on the sensitivity list is the clock.
            VeriIdDef *clock = 0 ;
            FOREACH_ARRAY_ITEM(sens_list, j, event_expr) {
                if (!event_expr) continue ;
                // Get the identifier in this sensitivity list event expression :
                VeriIdDef *sense_id = event_expr->FullId() ;
                if (!sense_id) {
                     // event expr not sensitive to a single identifier. Can't do that right now.
                     continue ;
                }

                // Step (6) Print out the set/reset condition signals and the clock signal for each always statement.
                // Check if this identifier is in the extracted if-conditions :
                if (condition_visitor._referenced_ids.GetItem(sense_id)) {
                    // Present in the conditions, so this is a 'asynchronous' set/reset signal
                    event_control_stmt->Info("   signal %s is an asynchronous set/reset condition of this statement", sense_id->Name()) ;
                } else {
                    event_control_stmt->Info("   signal %s is a clock of this statement", sense_id->Name()) ;
                    // check if we already found a clock :
                    if (clock) {
                        // We did. This may be a mistake in the algorithm, or a faulty design
                        event_control_stmt->Error("multiple clocks found on this statement : %s and %s", sense_id->Name(), clock->Name()) ;
                    }
                    clock = sense_id ;
                }
            }

            if (!clock) {
                // Cannot find the clock signal in this always statement.
                // This may be a mistake in the algorithm, or a faulty design
                event_control_stmt->Error("cannot find a clock on this statement") ;
            }
        }
    }

    return 0 ; // Leave with success status
}

/* ------------------------------------------------------- */

