/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

// string/stringsteam used :
#include <string>
#include <sstream>

// containers used :
#include "Array.h" // a dynamic array
#include "Set.h"   // a simple hash table
#include "Map.h"   // Associated hash table

// utilities used :
#include "Strings.h"        // Definition of class to easily create/concatenate char*s.
#include "Message.h"        // Message handler

// Verilog parser, main interface :
#include "veri_file.h"      // Make Verilog reader available
#include "VeriVisitor.h"    // Make the parse-tree visitor pattern available

// Verilog parse tree API :
#include "VeriModule.h"     // Definition of a VeriModule
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriId.h"         // Definitions of all identifier definition tree nodes
#include "VeriMisc.h"       // Definitions of VeriCaseItem tree node
#include "VeriScope.h"      // Definitions of the 'VeriScope' class.

#include "veri_tokens.h"    // Definition of port direction VERI_OUTPUT, etc ...

/*
    BUILD NOTE!!! : This file needs to be compiled with the same verilog compile switches,
                    that the verilog lib was built with, otherwise strange behavior may occur!
*/

/****************************************************************************************
**************************** FSM EXTRACTION VERILOG EXAMPLE  ****************************
*****************************************************************************************

                                APPLICATION NOTE:

    The following is an example to demonstrate FSM state machine information from an analysed Verilog parse tree.

    Under the VERILOG_CREATE_ONEHOT_FSMS compile flag (see VeriCompileFlags.h) Verific's analyser
    flags identifiers that behave as a state variables. This information is used in RTL elaboration
    to create Fsm objects, but this example shows that the basic state constants and state transitions
    can also be extracted directly from the parse tree even in absense of RTL elaboration.

    Specifically, this application program extracts the following FSM information for a wide variety of HDL coding styles :
    - Identify state variables for FSMs in the design,
    - Report the used state constants for each FSM
    - Find the state transitions that each FSM can go through, including the conditions for these transitions
      FIX ME: Extraction code for async reset and clock signals is still omitted.
    - Report the above information in messages to the screen
    As with all Verific application examples, this is example code, which can be used and modified freely by Verific
    licensees to serve the purpose of their own application.

    The example will show the following operations on analyzed verilog design.

        0) Parse the input file.
        1) For each module in the file, do the following :
        2) Find identifiers that were flagged by the analyzer as FSM variables.
        3) Report the constants and variables for each of the FSMs in the design.
        4) Traverse the 'always' statements in the design to find the clock, reset and state-transitions conditions.
        5) For each state transition, report the transition (old-state versus new state) and the condition under which this transition happens.

    The code in this application example closely follows the heuristics used for
    FSM extraction as described under the VERILOG_CREATE_ONEHOT_FSMS compile flag.
    Thus it will correctly function for a large number of (Mealy and Moore) single, double or tripple-variable FSM coding styles.

*****************************************************************************************/

// First, define visitor patterns which we will use :

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// A visitor patteren, which will find traverse statements and identify FSM transitions.
// Currently, it prints it's output, but you are free to create data structures instead.
// Called from the main routine at the bottom of this file
class TransitionVisitor : public VeriVisitor
{
public :
    TransitionVisitor() : VeriVisitor(),
        _present_state_var(0),
        _present_state(0),
        _is_clocked(0),
        _conditions(POINTER_HASH) { } ;

    ~TransitionVisitor() {} ;

    virtual void Visit(VeriInitialConstruct &node) {
        node.Info("skipping this initial block") ;
    }

    // Collect event control statements :
    virtual void Visit(VeriEventControlStatement &event_control_stmt) {
        event_control_stmt.Info("visiting this event control statement") ;

        // Check if this event control statement is clocked :
        _is_clocked = 0 ;
        // Look at the sensitivity list (the "@()" clause), and check if it has 'edge' expressions :
        Array *sens_list = event_control_stmt.GetAt() ;
        unsigned i ;
        VeriExpression *event_expr ;
        FOREACH_ARRAY_ITEM(sens_list, i, event_expr) {
            if (!event_expr) break ; // invalid sensitivity list
            if (event_expr->IsEdge(0/*any edge (pos or neg)*/)) {
                _is_clocked = 1 ;
                break ; // no need to check the other sensitivity list items. This statement is clocked !
            }
        }

        // Dive into the statement
        VeriStatement *stmt = event_control_stmt.GetStmt() ;
        if (stmt) stmt->Accept(*this) ;

        // reset clocking information
        _is_clocked = 0 ;
    }

    // Catch IF-statements :
    virtual void Visit(VeriConditionalStatement &if_statement) {
        // Get the condition expression :
        VeriExpression *if_condition = if_statement.GetIfExpr() ;
        if (!if_condition) return ; // something bad happened
        VeriStatement *then_stmt = if_statement.GetThenStmt() ;
        VeriStatement *else_stmt = if_statement.GetElseStmt() ;

        // FIX ME: Check if this is a :
        //   - set/reset/clocked condition (if _present_state is not set and no state var compare)
        //       also we could put limit on set/reset condition depth..
        //       and check if set/reset is async or sync.

        // Check if this is a state-compare :'if (statevar==stateconstant) ..'
        if (if_condition->OperType()==VERI_LOGEQ) {
            // This is a == compare condition.
            // Check if it is a fsm-state compare :
            VeriExpression *left = if_condition->GetLeft() ;
            VeriExpression *right = if_condition->GetRight() ;
            VeriIdDef *left_id = (left) ? left->FullId() : 0 ;
            VeriIdDef *right_id = (right) ? right->FullId() : 0 ;

            if ((left_id && left_id->CanBeOnehot()) || (right_id && right_id->CanBeOnehot())) {
                // Yes. This is a state variable compare.
                // If we are already under a state compare, then warn :
                if (_present_state_var) {
                    // If these is already a present_state_var set, then we two nested state compares.
                    // That is suspicious. Warn about this (and consider below that this second compare is a 'normal' (input-condition) case compare
                     if_condition->Warning("multiple nested FSM state conditions detected, on variable %s", _present_state_var->Name()) ;
                }

                // Set as the 'present_state' for the 'if' part.
                _present_state_var = (left_id->CanBeOnehot()) ? left_id : right_id ;
                _present_state = (left_id->CanBeOnehot()) ? right : left ;

                // Traverse 'then_stmt' with this :
                if (then_stmt) then_stmt->Accept(*this) ;

                _present_state_var = 0 ;
                _present_state = 0 ;

                // Now traverse 'else_stmt' without this state compare set.
                if (else_stmt) else_stmt->Accept(*this) ;

                // done
                return ;
            }
        }

        // This is not a state-comparing 'if'. Treat as normal condition area :
        // Generate a string representation of the 'if' condition :

        // Push the condition on the stack
        PushIfCondition(if_condition) ;

        // Traverse the 'then' statements :
        if (then_stmt) then_stmt->Accept(*this) ;

        // Pop the condition :
        PopCondition() ;

        // Now push the 'NOT' condition on the stack
        PushElseCondition(if_condition) ;

        // if_statement.Info("found else condition %s",ns.c_str()) ;

        // Traverse the 'else' statements :
        if (else_stmt) else_stmt->Accept(*this) ;

        // Pop the condition :
        PopCondition() ;
    }

    // Catch CASE statements :
    virtual void Visit(VeriCaseStatement &case_statement) {
        // case_statement.Info("found case statement") ;

        // Get the case condition :
        VeriExpression *case_condition = case_statement.GetCondition() ;

        // Check if this is a state-comparing case statement.
        // The case expression should be a IdRef of the 'present_state' variable :
        VeriIdDef *present_state_id = (case_condition) ? case_condition->FullId() : 0 ;
        if (present_state_id && present_state_id->CanBeOnehot()) {
            // Yes. This is a present_state comparing case statement.
            if (_present_state_var) {
                // If these is already a present_state_var set, then we two nested state compares.
                // That is suspicious. Warn about this (and consider below that this second compare is a 'normal' (input-condition) case compare
                 case_condition->Warning("multiple nested FSM state conditions detected, on variable %s and %s", present_state_id->Name(), _present_state_var->Name()) ;
            }
            // Each case-item will (should) contain the present_state constant.
        } else {
            // No. This is a regular case statement.
            if (!_present_state_var) {
                // If there is no '_present_state_var' set, then this case statement is NOT part of an FSM.
                // So don't analyse it.
                return ; // done.
            }
        }

        // Get the case items :
        Array *case_items = case_statement.GetCaseItems() ;
        // and iterate over them
        unsigned i ;
        VeriCaseItem *case_item ;
        FOREACH_ARRAY_ITEM(case_items, i, case_item) {
            if (!case_item) continue ; // something wrong..

            // Get the statement and the (OR-ed) conditions for this item :
            VeriStatement *stmt = case_item->GetStmt() ;
            if (!stmt) continue ; // no statement, no action

            Array *conditions = case_item->GetConditions() ;
            // and iterate over them :
            unsigned j ;
            VeriExpression *case_item_value ;
            FOREACH_ARRAY_ITEM(conditions, j, case_item_value) {
                if (!case_item_value) continue ;

                // Now execute 'stmt' under condition 'case_item_value'=='case_condition'.
                // Check if this case statement is a present-state comparison,
                // or if it is a normal case statement.
                if (!_present_state_var && present_state_id) {
                    // This is a new present_state comparing case statement.
                    // The present state (of the statement here) is 'case_item_value'.
                    _present_state = case_item_value ;
                    _present_state_var = present_state_id ;
                    // recur into the statement :
                    stmt->Accept(*this) ;
                    // reset the state comparison
                    _present_state = 0 ;
                    _present_state_var = 0 ;
                } else {
                    // This is a case statement comparing input variables.
                    // Push the case condition 'case_item_value'==='case_condition' on the track
                    PushCaseCondition(case_condition,case_item_value) ;

                    stmt->Accept(*this) ;

                    PopCondition() ;
                }
            }
        }
    }

    // Catch non-Blocking assignments :
    virtual void Visit(VeriNonBlockingAssign &assign_statement) {
        // See if there is a state variable on the LHS of this assignment :
        VeriExpression *lval = assign_statement.GetLVal() ;
        VeriIdDef *lhs_id = (lval) ? lval->FullId() : 0 ;
        if (lhs_id && lhs_id->CanBeOnehot()) {
            // OK. We are assigning to state variable.
            // assign_statement.Info("found non-blocking assign to onehot var %s",lhs_id->Name()) ;

            // Get the (new state) on the rhs.
            VeriExpression *rhs = assign_statement.GetValue() ;

            VeriIdDef *rhs_id = (rhs) ? rhs->FullId() : 0 ;
            if ((rhs && rhs->IsConst()) || (rhs_id && rhs_id->IsParam())) {
                // This is an async reset assignment if there is no 'current-state'
                // This is a FSM state transition if there is a 'current-state'

                // Get the name and the (integer) value for this state :
                const char *next_state_name = (rhs_id) ? rhs_id->Name() : 0 ; // state only has a name if it is a parameter.
                int next_state_value = (rhs_id && rhs_id->GetInitialValue()) ? rhs_id->GetInitialValue()->Integer() : rhs->Integer() ; // value of parameter is in its initial value.

                if (_present_state) {
                    // Check if present state is a 'parameter' (or a constant)
                    VeriIdDef *present_id = _present_state->FullId() ;
                    if (present_id && !present_id->IsParam()) present_id = 0 ;

                    // Get the name and the (integer) value for this state :
                    const char *present_state_name = (present_id) ? present_id->Name() : 0 ; // state only has a name if it is a parameter.
                    int present_state_value = (present_id && present_id->GetInitialValue()) ? present_id->GetInitialValue()->Integer() : _present_state->Integer() ; // value of parameter is in its initial value.

                    char *condition = PrintCondition() ; // Get the current overall condition in string form.
                    assign_statement.Info("TRANSITION variable %s from state %s(%d) to %s(%d) under condition \"%s\"",lhs_id->Name(),(present_state_name)?present_state_name:"",present_state_value,(next_state_name)?next_state_name:"",next_state_value, (condition)?condition:"") ;
                    Strings::free(condition) ;
                } else {
                    assign_statement.Info("RESET variable %s to state %s(%d)",lhs_id->Name(),(next_state_name)?next_state_name:"",next_state_value) ;
                }
            } else if (rhs_id && rhs_id->IsVar()) {
                // This is a variable to variable assignment.

                // Verific's fsm extraction guarantees that 'rhs_id' is an fsm variable.
                VERIFIC_ASSERT(rhs_id->CanBeOnehot()) ;

                // This assignment makes 'lhs_id' the 'present-state' variable and 'rhs_id' the 'next-state' variable in this FSM.
                if (_is_clocked) {
                    assign_statement.Info("CLOCKED assignment from %s to %s",rhs_id->Name(), lhs_id->Name()) ;
                } else {
                    assign_statement.Info("UNCLOCKED assignment from %s to %s",rhs_id->Name(), lhs_id->Name()) ;
                }

                // Check me : There should be no 'current_state' ?
            }
        }
    }

    // Catch blocking assignments :
    virtual void Visit(VeriBlockingAssign &assign_statement) {
        // See if there is a state variable on the LHS of this assignment :
        VeriExpression *lval = assign_statement.GetLVal() ;
        VeriIdDef *lhs_id = (lval) ? lval->FullId() : 0 ;
        if (lhs_id && lhs_id->CanBeOnehot()) {
            // OK. We are assigning to state variable.
            // assign_statement.Info("found blocking assign to onehot var %s",lhs_id->Name()) ;

            // Get the (new state) on the rhs.
            VeriExpression *rhs = assign_statement.GetValue() ;

            VeriIdDef *rhs_id = (rhs) ? rhs->FullId() : 0 ;
            if ((rhs && rhs->IsConst()) || (rhs_id && rhs_id->IsParam())) {
                // This is an async reset assignment if there is no 'current-state'
                // This is a FSM state transition if there is a 'current-state'

                // Get the name and the (integer) value for this state :
                const char *next_state_name = (rhs_id) ? rhs_id->Name() : 0 ; // state only has a name if it is a parameter.
                int next_state_value = (rhs_id && rhs_id->GetInitialValue()) ? rhs_id->GetInitialValue()->Integer() : rhs->Integer() ; // value of parameter is in its initial value.

                if (_present_state) {
                    // Check if present state is a 'parameter' (or a constant)
                    VeriIdDef *present_id = _present_state->FullId() ;
                    if (present_id && !present_id->IsParam()) present_id = 0 ;

                    // Get the name and the (integer) value for this state :
                    const char *present_state_name = (present_id) ? present_id->Name() : 0 ; // state only has a name if it is a parameter.
                    int present_state_value = (present_id && present_id->GetInitialValue()) ? present_id->GetInitialValue()->Integer() : _present_state->Integer() ; // value of parameter is in its initial value.

                    char *condition = PrintCondition() ; // Get the current overall condition in string form.
                    assign_statement.Info("TRANSITION variable %s from state %s(%d) to %s(%d) under condition \"%s\"",lhs_id->Name(),(present_state_name)?present_state_name:"",present_state_value,(next_state_name)?next_state_name:"",next_state_value, (condition)?condition:"") ;
                    Strings::free(condition) ;
                } else {
                    assign_statement.Info("RESET variable %s to state %s(%d)",lhs_id->Name(),(next_state_name)?next_state_name:"",next_state_value) ;
                }
            } else if (rhs_id && rhs_id->IsVar()) {
                // This is a variable to variable assignment.

                // Verific's fsm extraction guarantees that 'rhs_id' is an fsm variable.
                VERIFIC_ASSERT(rhs_id->CanBeOnehot()) ;

                // This assignment makes 'lhs_id' the 'present-state' variable and 'rhs_id' the 'next-state' variable in this FSM.
                if (_is_clocked) {
                    assign_statement.Info("CLOCKED assignment from %s to %s",rhs_id->Name(), lhs_id->Name()) ;
                } else {
                    assign_statement.Info("UNCLOCKED assignment from %s to %s",rhs_id->Name(), lhs_id->Name()) ;
                }

                // Check me : There should be no 'current_state' ?
            }
        }
    }

    // The 'input' condition stack.
    // This is modeled as a Map, so we can insert 'if', 'else' and 'case' conditions.
    void PushIfCondition(VeriExpression *cond) {
        VERIFIC_ASSERT(cond) ;
        (void) _conditions.Insert(cond,0,0,1) ; // expression goes to the 'key'
    }
    void PushElseCondition(VeriExpression *not_cond) {
        VERIFIC_ASSERT(not_cond) ;
        (void) _conditions.Insert(0,not_cond,0,1) ; // expression goes to the 'value'
    }
    void PushCaseCondition(VeriExpression *case_cond, VeriExpression *case_item) {
        VERIFIC_ASSERT(case_cond && case_item) ;
        (void) _conditions.Insert(case_cond,case_item,0,1) ; // case-expression and item go 'key' and 'value' respectively.
    }
    void PopCondition() {
        // Blindly remove the last inserted item. Will assert if you attempt to pop an empty stack.
        (void) _conditions.Remove(_conditions.GetItemAt(_conditions.Size()-1)) ;
    }

    char *PrintCondition() {
        // Print the condition stack as a char* image. Take the AND of all conditions in the stack.

        // Build the result in a stringstream, because we want to use PrettyPrint() to print the expressions.
        std::ostringstream ss ;

        // Iterate over the conditions :
        MapIter mi ;
        VeriExpression *condition ;
        VeriExpression *other_condition ;
        FOREACH_MAP_ITEM(&_conditions,mi,&condition,&other_condition) {
             if (ss.tellp()) {
                 // There is already a condition in this stream.
                 // So put an AND operator (&&) before writing the new one :
                 ss << " && " ;
             }

             // Check if this was a 'if' condition, and 'else' condition or a 'case' condition :
             if (condition && !other_condition) {
                 // regular single 'if' condition :
                 condition->PrettyPrint(ss,0) ;
             } else if (!condition && other_condition) {
                 // single 'else' condition. Put NOT operator (!) in front and parenthesize :
                 ss << "!(" ;
                 other_condition->PrettyPrint(ss,0) ;
                 ss << ")" ;
             } else if (condition && other_condition) {
                 // This is a case condition. Put a case-equal operator (===) in between :
                 condition->PrettyPrint(ss,0) ;
                 ss << "===" ;
                 other_condition->PrettyPrint(ss,0) ;
             } else {
                 VERIFIC_ASSERT(0) ; // should have have 0 - 0 item.
             }
        }

        // Now turn the stream into a string and return a 'saved' char* :
        std::string s = ss.str() ;
        return Strings::save(s.c_str()) ;
    }

public :
    VeriIdDef *_present_state_var ; // The present state variable
    VeriExpression *_present_state ; // The condition (state) in which the state variable currently is.
    unsigned _is_clocked:1 ; // flag if current event control statement is clocked.
    // input conditions :
    Map _conditions ; // Map of pair of VeriExpression conditions, indicating the condition under which statements execute.
} ;

#ifdef VERIFIC_NAMESPACE
} ; // end definitions in verific namespace
#endif

/*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
using namespace Verific ; // start using Verific namespace
#endif

int main(int argc, char **argv)
{
   if (argc < 2) Message::PrintLine("Default input file: pre3.v. Specify command line argument to override") ;

     // Get the file name to work on. If not specified as command line arg use default file^M
    const char *file_name = (argc > 1) ? argv[1] : "prep3.v" ;

    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    // Now analyze the above example Verilog file :
    // If there is any error, don't proceed further
    if (!veri_reader.Analyze(file_name)) return 1 ;

    // Step (1) : Loop over all analyzed modules :
    VeriModule *module ;
    MapIter mi ;
    FOREACH_VERILOG_MODULE(mi, module) {
        if (!module) continue ;

        module->Info("search module %s for FSMs", module->Name()) ;

        // Step (2) : Find the FSM variables in the module
        // We loop through the identifiers declared in this module scope for that :
        // Collect FSM state variables in a set :
        Set fsm_vars(POINTER_HASH) ;
        VeriScope *module_scope = module->GetScope() ;
        MapIter mi ;
        VeriIdDef *id ;
        FOREACH_MAP_ITEM(module_scope->DeclArea(), mi, 0, &id) {
            if (!id) continue ;

            // Check only for 'variables' that were flagged as 'onehot'-encodable :
            if (!id->CanBeOnehot()) continue ;
            if (!id->IsVar()) continue ;

            // Here, we found a real one-hot encodable (FSM) variable :
            if (!fsm_vars.Insert(id)) {
                // This variable was already in the list of FSM variables.
                // So must be a 'linked' variable from a FSM variable that we already visited.
                // So it's already reported
                continue ;
            }

            // Report FSM :
            id->Info("FSM variable %s found here", id->Name()) ;

#ifdef VERILOG_CREATE_ONEHOT_FSMS
            // Report variables that are 'linked' in the same FSM (For example, 'state' and 'next_state' variables).
            VeriVariable *runner = (VeriVariable*)id ; // ugly cast
            runner = runner->GetLinkVar() ;
            while (runner && (runner != id)) {
                runner->Info("  FSM variable %s is part of the same FSM as variable %s",runner->Name(),id->Name()) ;
                // Insert that variable into the table also, so we don't hit it separately
                fsm_vars.Insert(runner) ;
                // Go to next variable in the same FSM :
                runner = runner->GetLinkVar() ;
            }

            // Analyse the state constants related to this state variable,
            // using the id->GetFsmConstants() Map ( a (long)num -> (VhdlIdDef)constant ) associated hash table.
            MapIter mii ;
            long int num_encoding ;
            VeriExpression *state_constant ;
            runner = (VeriVariable*)id ; // ugly cast
            FOREACH_MAP_ITEM(runner->GetFsmConstants(), mii, &num_encoding, &state_constant) {
                // 'state_constant' can be a idref (of a parameter) or a literal.
                // state_constant->Linefile() will point at where that constant is defined.
                // num_encoding is the value (in integer form) of that constant expression
                if (!state_constant) continue ; // something bad happened.
                // print message :
                VeriIdDef *constant_id = state_constant->GetId() ;
                state_constant->Info("  FSM constant %s(%d) of FSM variable %s defined here",(constant_id)?constant_id->Name():"", num_encoding, id->Name()) ;
            }
#endif // VERILOG_CREATE_ONEHOT_FSMS
        }

        // If no fsm state variables were found in the module, then leave.
        if (!fsm_vars.Size()) continue ;
        
        // Now do the actual traversal of the design.
        // Use visitor pattern TransitionVisitor for that.
        TransitionVisitor transitions ;

        // This will implement the remaining steps, and extract the fsm state transition information.
        module->Accept(transitions) ;
    }

    return 0 ; // Leave with success status
}

/* ------------------------------------------------------- */

