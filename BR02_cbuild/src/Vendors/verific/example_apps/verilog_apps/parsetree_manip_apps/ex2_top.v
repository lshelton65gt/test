module TOP(a, b, c, d, out1, out2, out3) ;
    input a, b, c, d ;
    output out1, out2, out3 ;
    and inst1 (out1, a, b, c), inst2(out2, a, b, c) ;
    and inst3 (out3, a | c, d) ;
endmodule
