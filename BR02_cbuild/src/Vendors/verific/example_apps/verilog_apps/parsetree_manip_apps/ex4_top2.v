module top (in1,in2,clk,out);

input in1, in2, clk;
output out;
reg out;

always@(posedge clk)
    begin 
        out <= in1 & in2;
    end

endmodule