/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>         // strcpy, strchr ...

#include "Message.h"        // Make message class available
#include "veri_file.h"      // Make Verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriTreeNode.h"   // Definition of VeriTreeNode
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "VeriId.h"         // Definitions of all identifier definition tree nodes
#include "VeriConstVal.h"   // Definitions of all constant expression tree nodes
#include "VeriScope.h"      // Symbol table of locally declared identifiers
#include "Strings.h"        // Definition of class to manipulate copy, concatenate, create etc...

#include "veri_tokens.h"    // Definition of port direction VERI_OUTPUT, etc ...

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*
    BUILD NOTE!!! : This file needs to be compiled with the same verilog compile switches,
                    that the verilog lib was built with, otherwise strange behavior may occur!
*/

/****************************************************************************************
******************************* VERILOG EXAMPLE #7 **************************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is an example to demonstrate parse tree manipulation utility using
    user friendly APIs. The name of the design file to be modified is taken from command
    line argument.
    The example will try to show the following modifications on an analyzed parse tree.

        1) How to add port to module declaration.
        2) How to remove port from module declaration.
        3) How to remove port ref from module instantiation.
        4) How to add port ref to module instantiation.
        5) How to add net declaration to module.
        6) How to remove net declaration from module.
        7) How to add generic to module.
        8) How to remove generic from module.
        9) How to remove instantiation from module.

    The application can take an optional command line argument as the file name in
    absence  of which it analyzes the default file viz. 'ex3_top.v'. The top level module
    is extracted from the analyzed parse tree and above mentioned parse tree manipulation
    operations are performed on it. On failure it provides appropriate error messages.
*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    if (argc < 2) Message::PrintLine("Default input file: ex3_top.v. Specify command line argument to override") ;

    // Get the file name to work on. If not specified as command line arg use default file
    const char *file_name = (argc > 1) ? argv[1] : "ex3_top.v" ;

    // Analyze the design file(in verilog2k mode), if there is any error, don't proceed further!
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veri_file::Analyze(file_name, 1/*Verilog 2000*/)) return 1 ;

    // Get all the top level modules
    Array *all_top_modules = veri_file::GetTopModules("work") ;
    // Get a handle for the first top level module, if any.
    VeriModule *mod = (all_top_modules && all_top_modules->Size()) ? (VeriModule *)all_top_modules->GetFirst() : 0 ;
    delete all_top_modules ; // Delete the array of top level modules as it is no longer required
    if (!mod) {
        Message::PrintLine("Cannot find a top level module") ;
        return 1 ;
    }

    //---------------------------------------
    //  Pretty Print (Pre Parse-tree Modification)
    //---------------------------------------
    char *before_print = Strings::save(mod->Name(), "_before_ex3_pp.v") ;
    // Pretty print current design
    veri_reader.PrettyPrint(before_print, 0, "work") ;
    // Free dynamically allocated storage
    Strings::free(before_print) ;

    // In parse tree manipulation we need to add or remove port, parameter, signal,
    // instance to module declaration. Remove APIs take care of keeping parse-tree
    // consistent, and bringing it back in pre-insertion condition, but in appilications
    // if we want to remove port/parameter/signal declaration, we need to remove their
    // references first.
    // Since we can have any file specified by the user, to remove a port/signal
    // we need to traverse the entire parse tree in order to remove their references.
    // To avoid this we are removing only those ports/signals which have been inserted
    // by this application.
    //
    // Now the following example will try to use all types of Add/Remove APIs.

    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
     |     ADD PORTS 'X' and 'Y' TO TOP LEVEL MODULE      |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To add port to the top levelmodule the following procedure is followed.
    /****************************************************************************************
     Call 'AddPort' routine. This routine will return 1 on success or 0 on failure.
    ****************************************************************************************/
    VeriIdDef *new_port_id1 = mod->AddPort("X" /* port to be added*/, VERI_OUTPUT /* direction*/, 0 /* data type */) ;
    if (!new_port_id1) { // Can't add port 'X'
        Message::PrintLine("Cannot add port 'X' to the top level module") ;
    }

    VeriIdDef *new_port_id2 = mod->AddPort("Y" /* port to be added*/, VERI_INPUT /* direction*/, 0 /* data type */) ;

    if (!new_port_id2) { // Cannot add port 'Y'
        Message::PrintLine("Cannot add port 'Y' to the top level module") ;
    }
    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
     |       REMOVE PORT 'X' FROM TOP LEVEL MODULE       |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */
    // To remove port from top level module the following procedure is followed.
    /****************************************************************************************
     Call 'RemovePort' routine. This routine will return 1 on success
     and 0 on failure.
    ****************************************************************************************/
    // Call 'RemovePort' routine. This routine will remove specific port declaration
    // from port list (For ansi style only from port list and for non-ansi port
    // expression from port list and declarations from module item list), remove
    // port identifier from module scope.
    // Here the 'RemovePort' routine has been called on the port added by calling the 'AddPort'
    // routine ie. on "X" instead of some existing port. This has been done as the 'RemovePort'
    // routine only removes the port but leaves any usage of the port untouched. Hence, such
    // a remove operation may yield a syntactically incorrect System Verilog code. Hence we
    // perform the remove port operation on the newly added port "X" which has not been used
    // anywhere else.
    if (new_port_id1) { // Don't call 'RemovePort' routine in case "X" couldn't be added
        unsigned is_removed = mod->RemovePort("X" /* port to be removed */) ;

        if (!is_removed) { // Can't remove port 'X'
            Message::PrintLine("Cannot remove port 'X' from the module") ;
        }
    }

    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
     |      ADD NET 'in_clk', 'wire_to_be_removed' TO TOP LEVEL MODULE     |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To add net (signal) to the top level module the following procedure is followed.
    /****************************************************************************************
     Call 'AddSignal' routine. This routine will return created net identifier
        on success and 0 on failure.
    ****************************************************************************************/

    // Call 'AddSignal' routine. This routine will create signal identifier and add it to
    // module scope, create signal declaration and add it to module item list.
    VeriIdDef *new_signal1 = mod->AddSignal("in_clk" /* signal to be added */, new VeriDataType(VERI_WIRE, 0, 0) /* data type */, 0 /* optional initial value*/) ;

    if (!new_signal1) { // Can't add signal 'in_clk'
        Message::PrintLine("Cannot add signal 'in_clk' to module, '", mod->Name(), "'") ;
    }

    VeriIdDef *new_signal2 = mod->AddSignal("wire_to_be_removed" /* signal to be added */, new VeriDataType(VERI_WIRE, 0, 0) /* data type */, 0 /* optional initial value*/) ;

    if (!new_signal2) { // Can't add signal 'wire_to_be_removed'
        Message::PrintLine("Cannot add signal 'wire_to_be_removed' to module '", mod->Name(), "'") ;
    }

    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
     |    REMOVE SIGNAL "wire_to_be_removed" FROM TOP LEVEL MODULE   |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To remove signal from a module the following procedure is followed.
    /****************************************************************************************
        Call 'RemoveSignal' routine to remove signal. This routine will
        return 1 after successful removal, otherwise return 0.
    ****************************************************************************************/

    // Call 'RemoveSignal' to remove signal 'wire_to_be_removed' from top level module. This will return
    // signal declaration from module item list.
    if (new_signal2) {
        unsigned is_removed = mod->RemoveSignal("wire_to_be_removed" /* signal to be removed */) ;
        if (!is_removed) { // Cannot remove signal 'wire_to_be_removed'
            Message::PrintLine("Cannot remove signal 'wire_to_be_removed' from module '", mod->Name(), "'") ;
        }
    }

   /*
     /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
    |  ADD & REMOVE PORT REFS TO INSTANCE IN TOP LEVEL MODULE  |
     \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
   */
    // Iterate through the module items to get an instantiation.

    Array *mod_item_arr = mod->GetModuleItems() ; // Get the module items
    unsigned i ;
    VeriModuleItem *item ;
    unsigned is_port_ref_removed = 0 ;
    unsigned is_inst_removed = 0 ;
    FOREACH_ARRAY_ITEM(mod_item_arr, i, item) { // Iterate through the module item array
        if (!item || !item->IsInstantiation()) continue ;
        Array *inst_arr = item->GetIds() ; // Get an array of instances in the form VeriInstId*
        VeriIdDef *inst_id ;
        unsigned j ;
        const char *inst_name ;
        FOREACH_ARRAY_ITEM(inst_arr, j,inst_id) { // Iterate through the instance array
            if (!inst_id) continue ;
            inst_name = inst_id->GetName() ; // Get the name of the instantiation
            break ;
        }
        if (!is_port_ref_removed) {
            VeriModule *master = item->GetInstantiatedModule() ; // Get the master
            if (!master) continue ;
            Array *master_port_arr = master->GetPorts() ; // Get the ports
            unsigned k ;
            VeriIdDef *port_id ;
            const char *formal_name = 0 ; // Stores the name of an input port of instantiated module
            FOREACH_ARRAY_ITEM(master_port_arr, k, port_id) { // Iterate through the port list of the master
                if (port_id && port_id->Dir() == VERI_INPUT) { // Find an input port to replace
                   formal_name = port_id->GetName() ; // Get the formal name
                   break ; // Get the first input port name
                }
            }
            /****************************************************************************************
              Call 'RemovePortRef' routine. This routine will return 1 on success or 0 on failure.
            ****************************************************************************************/
            if (formal_name) { // If the formal name exists only then try to remvoe the port reference
                is_port_ref_removed = mod->RemovePortRef(inst_name /* instance name */, formal_name /* formal port name */) ;
                if (!is_port_ref_removed) { // Can't remove port ref
                    Message::PrintLine("Cannot remove port ref for formal '", formal_name, "' from instance '", inst_name, "'") ;

                } else {
                    /***************************************************************************************************
                      Call 'AddPortRef' routine. This routine will return 1 on success or 0 on failure.
                    ***************************************************************************************************/
                    // Call 'AddPortRef' routine to add actual 'Y' for formal port of a module. The port 'Y' has already
                    // been added previously
                    // Add this port ref only if the port 'Y' has been added successfully
                    unsigned is_port_ref_added = 0 ;
                    if (new_port_id2) is_port_ref_added = mod->AddPortRef(inst_name /* instance name */, formal_name /* formal port name */, new VeriIdRef(Strings::save("Y")) /* actual */) ;

                    if (!is_port_ref_added) { // Can't add port ref 'Y'
                        Message::PrintLine("Cannot add  port ref 'Y' for formal '", formal_name, "' to instance '", inst_name, "'") ;
                    }
                    continue ; // If everything works out fine then look for other instances and hence 'continue'
                }
            }
        }
        // In case the port ref cannot be removed due to some reason or the other, then
        // try to remove that instance. Else remove some other instance for which the
        // 'RemovePortRef' routine has not been called yet.
        if (!is_inst_removed) { // If removal of port ref from the module item is unsuccessfull

            /*
              /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
             |    REMOVE INSTANCE FROM TOP LEVEL MODULE    |
              \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
            */

            // To remove instantiation from a module the following procedure is followed.
            /****************************************************************************************
                Call 'RemoveInstance' routine to remove instance. This routine will
                return 1 after successful removal, otherwise return 0.
            ****************************************************************************************/

            // Call 'RemoveInstance' to remove a module instance from a module
            is_inst_removed = mod->RemoveInstance(inst_name /* instance to be removed*/) ;

            if (!is_inst_removed) { // Cannot remove instance
                Message::PrintLine("Cannot remove instance '", inst_name, "' from module '", mod->Name(), "'") ;
            }
        }
        if (is_inst_removed && is_port_ref_removed) break ; // Break out of the for loop if both port reference and instance have been successfully removed
    }

    // Give proper Error Message if either or both of 'RemovePortRef' and 'RemoveInstance' routines fail
    if (!is_port_ref_removed) Message::PrintLine("Cannot remove any port reference") ;
    if (!is_inst_removed) Message::PrintLine("Cannot remove any instance") ;

    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
     |    ADD NEW PARAMETERS "new_p1", "new_p2" TO TOP LEVEL MODULE  |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To add a new parameter to a module, the following procedure is followed.
    /****************************************************************************************
        Call 'AddParameter' routine of module to add parameter. This routine
        will return created parameter identifier if it can add parameter
        properly, otherwise this routine will return 0.
    ****************************************************************************************/

    // Call 'AddParameter' routine to add parameter 'new_p1' to the top level module. This routine
    // will create parameter identifier and add that to module scope, create parameter
    // declaration and add that to parameter port list or module item list.
    VeriIdDef *new_parameter_id1 =  mod->AddParameter("new_p1" /* parameter to be added*/, 0 /* data type */, new VeriIntVal(10) /* initial value */) ;

    if (!new_parameter_id1) { // Cannot add parameter
        Message::PrintLine("Cannot add parameter 'new_p1' to module '", mod->Name(), "'") ;
    }
    // Add another paramter
    VeriIdDef *new_p2 =  mod->AddParameter("new_p2" /* parameter to be added*/, 0 /* data type */, new VeriIntVal(100) /* initial value */) ;

    if (!new_p2) { // Cannot add parameter
        Message::PrintLine("Cannot add parameter 'new_p2' to module '", mod->Name(), "'") ;
    }
    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
     |    REMOVE PARAMETER 'new_p2' FROM  TOP LEVELMODULE     |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To remove parameter 'new_p2' from the top level module the following procedure is followed.
    /****************************************************************************************
     Call 'RemoveParameter' routine. This routine will return 1 on success and 0 on failure.
    ****************************************************************************************/
    if (new_p2) {
        // Call 'RemoveParameter' routine to remove the first parameter in the
        // top level module. This routine will remove parameter declaration
        // from parameter port list/module item list.
        unsigned is_param_removed = mod->RemoveParameter("new_p2" /* parameter to be removed */) ;

        if (!is_param_removed) { // Can't remove the parameter
            Message::PrintLine("Cannot remove parameter 'new_p2' from the top level module '", mod->Name(), "'") ;
        }
    }
    //---------------------------------------
    //  Pretty Print (Post Parse-tree Modification)
    //---------------------------------------
    // To make sure that everything went as planned, pretty print the current
    // parse-tree's out and compare with the previous pretty printed output.
    char *after_print = Strings::save(mod->Name(), "_after_ex3_pp.v") ;
    veri_reader.PrettyPrint(after_print, 0, "work") ;

    Strings::free(after_print) ; // Free dynamically allocated storage space
    return 1 ;
}

/* ------------------------------------------------------- */

