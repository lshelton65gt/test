/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

// containers used :
#include "Array.h" // a dynamic array
#include "Set.h"   // a simple hash table

// utilities used :
#include "Strings.h"        // Definition of class to easily create/concatenate char*s.
#include "Message.h"        // Message handler

// Verilog parser, main interface :
#include "veri_file.h"      // Make Verilog reader available
#include "VeriVisitor.h"    // Make the parse-tree visitor pattern available

// Verilog parse tree API :
#include "VeriModule.h"     // Definition of a VeriModule
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriId.h"         // Definitions of all identifier definition tree nodes
#include "VeriCopy.h"       // Parse tree copying interface

#include "veri_tokens.h"    // Definition of port direction VERI_OUTPUT, etc ...

/*
    BUILD NOTE!!! : This file needs to be compiled with the same verilog compile switches,
                    that the verilog lib was built with, otherwise strange behavior may occur!
*/

/****************************************************************************************
******************************* SCAN INSERT VERILOG EXAMPLE  ****************************
*****************************************************************************************

                                APPLICATION NOTE:

    The following is an example to demonstrate parse tree manipulation utility using
    user friendly APIs. The name of the design file to be modified is taken from command
    line argument.

    The example will show the following operations on analyzed verilog design.

        0) Parse the input file
        1) Find the top-level module
        2) Add 'si' (scan-in), 'so' (scan-out), 'sc' (scan-control) ports to the module.
        3) Find the clocked always statements in the module
        4) For a clocked always statement, find the 'synchronous' statement in the always statement
           (the one executed under the clock edge)
        5) Collect all the target (names) assigned under this synchronous statement. There will be the registers.
        6) Create a sequential block with 'shifted' assignments to all the target names (registers).
           Start with 'si' (scan-in) and end with 'so' (scan-out).
        7) Create a 'if' statement, which switches under the 'sc' (scan-control) input.
        8) The 'then' part of the if statement becomes the shift-assignments from (6)
        9) The 'else' part of the if statement becomes the existing synchonous statement (from (4)).
       10) Replace statement (4) by the if-statement if (7).
       11) Pretty-print the (modified) design to an output file.

    If file 'ex4_top1.v' and ex4_top2.v is given in command line argument, file 'ex4_top1.v.out.v' and 'ex4_top2.v.out.v' will be printed in step 11.

*****************************************************************************************/

// First, define visitor patterns which we will use :

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// A visitor patteren, which will find the event control statements in a parse tree :
// VeriVisitor is a general purpose Verific Verilog parse tree traversal pattern.
// It is finetuned here to collect event control statements inside always constructs
// (skip initial blocks) :
class EventControlVisitor : public VeriVisitor
{
public :
    EventControlVisitor() : VeriVisitor(), _event_control_stmts() { } ;
    ~EventControlVisitor() {} ;

    virtual void Visit(VeriInitialConstruct &node) {
        node.Info("skipping this initial block") ;
    }

    // Collect event control statements :
    virtual void Visit(VeriEventControlStatement &node) {
        node.Info("visiting this event control statement") ;
        _event_control_stmts.InsertLast(&node) ;
    }

public :
    Array _event_control_stmts ; // array of VeriEventControlStatement *'s, collected by this visitor
} ;

// A visitor patteren, which will find the target names in assignments in a parse tree
// Collect all assignment targets from Blocking and Non-Blocking assignments :
class AssignmentVisitor : public VeriVisitor
{
public :
    AssignmentVisitor() : VeriVisitor(), _assigned_ids(POINTER_HASH) { } ;
    ~AssignmentVisitor() {} ;

    // Collect blocking assignments :
    virtual void Visit(VeriBlockingAssign &node) {
        // Go to the assigned target (expression) :
        VeriExpression *target = node.GetLVal() ;
        // Find the assigned identifier(s) :
        VeriIdDef *target_id = (target) ? target->GetId() : 0 ;
        if (target_id) _assigned_ids.Insert(target_id) ;
    }
    // Collect non-blocking assignments :
    virtual void Visit(VeriNonBlockingAssign &node) {
        // Go to the assigned target (expression) :
        VeriExpression *target = node.GetLVal() ;
        // Find the assigned identifier(s) :
        VeriIdDef *target_id = (target) ? target->GetId() : 0 ;
        if (target_id) _assigned_ids.Insert(target_id) ;
    }

public :
    Set _assigned_ids ; // Set of VeriIdDef *'s (to avoid overlap in targets), collected by this visitor
} ;

#ifdef VERIFIC_NAMESPACE
} ; // end definitions in verific namespace
#endif

/*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
using namespace Verific ; // start using Verific namespace
#endif

int main(int argc, char **argv)
{
    if (argc < 2) Message::PrintLine("Default input file: ex4_top2.v. Specify command line argument to override") ;

     // Get the file name to work on. If not specified as command line arg use default file
     // We recommend that you use ex4_top1.v and ex4_top2.v for this example.
    const char *file_name = (argc > 1) ? argv[1] : "ex4_top2.v" ;

    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    // Now analyze the above example Verilog file :
    // If there is any error, don't proceed further
    if (!veri_reader.Analyze(file_name)) return 1 ;

    // Step (1) : Pick up the top-level module from the design just analyzed :
    VeriModule *top = veri_reader.GetModule(veri_reader.TopModule()) ;
    if (!top) {
        Message::Msg(VERIFIC_ERROR,0,0,"no top level design found") ;
        return 2 ;
    }

    // Step (2) Add 'si' (scan-in), 'so' (scan-out), 'sc' (scan-control) ports to the module.
    VeriIdDef *si = top->AddPort("si",VERI_INPUT,0) ;
    VeriIdDef *so = top->AddPort("so",VERI_OUTPUT,new VeriDataType(VERI_REG,0,0)) ; // Has to be a 'reg' since we will assign to it from within an always block
    VeriIdDef *sc = top->AddPort("sc",VERI_INPUT,0) ;

    // Step (3) Find the clocked always statements in the module
    // We use a visitor pattern for that.
    // Collect all the 'event_control_statements' :
    // This will collect the statements appearing immediately inside a always construct : "always @(...) stmt" :
    EventControlVisitor event_control_visitor ;
    top->Accept( event_control_visitor ) ; // traverse and collect..

    // Check each event control statement found in the design :
    VeriEventControlStatement *event_control_stmt ;
    unsigned i, j ;
    FOREACH_ARRAY_ITEM(&(event_control_visitor._event_control_stmts), i, event_control_stmt) {
        if (!event_control_stmt) continue ; // null-pointer check

        // Check if this event control statement is clocked :
        // Look at the sensitivity list (the "@()" clause), and check if it has 'edge' expressions :
        Array *sens_list = event_control_stmt->GetAt() ;
        unsigned is_clocked = 0 ;
        VeriExpression *event_expr ;
        FOREACH_ARRAY_ITEM(sens_list, j, event_expr) {
            if (!event_expr) break ; // invalid sensitivity list
            if (event_expr->IsEdge(0/*any edge (pos or neg)*/)) {
                is_clocked = 1 ;
                break ; // no need to check the other sensitivity list items. This statement is clocked !
            }
        }
        if (!is_clocked) continue ;

        // issue a message :
        event_control_stmt->Info("found one clocked event control statement here") ;

        // Step (4) For a clocked always statement, find the 'synchronous' statement in the always statement

        // If there is only one edge in the sensitivity list, the whole statement is clocked
        VeriStatement *clocked_stmt = event_control_stmt->GetStmt() ;

        // Check if the actual clocked statement is nested under asynchronous if-statements :
        unsigned nesting = 1 ;
        VeriConditionalStatement *if_stmt = 0 ;
        while (sens_list->Size() > nesting) {
            if (!clocked_stmt) break ; // invalid, or non-existing clocked statement

            // Otherwise, there are a-synchronouos (re)set edges, so we need to traverse to find the 'else'
            // part of the (N-1)th nested if statement inside this statement.

            // Can do this with a visitor, or otherwise, but for explanation purposes,
            // Do this the 'quick-and-dirty' way, using GetClassId and static casts to traverse into the statement :

            // There should be an 'if' statement here, or else this is not a synthesizable event control statement.
            // But, there could be a 'sequential' block statement (begin/end) with one if statement in there :
            if (clocked_stmt->GetClassId() == ID_VERISEQBLOCK) {
                VeriSeqBlock *block = (VeriSeqBlock*)clocked_stmt ;
                Array *stmts = block->GetStatements() ;
                // Find the if statement in here :
                VeriStatement *tmp_stmt ;
                FOREACH_ARRAY_ITEM(stmts, j, tmp_stmt) {
                    if (!tmp_stmt) continue ;
                    if (tmp_stmt->GetClassId() == ID_VERICONDITIONALSTATEMENT) {
                        clocked_stmt = tmp_stmt ;
                        break ;
                    }
                }
                // Now drop through to the if-statement processing :
            }

            if (clocked_stmt->GetClassId() == ID_VERICONDITIONALSTATEMENT) {
                if_stmt = (VeriConditionalStatement*)clocked_stmt ;

                // Set the clocked statement to be the 'else' part of the if-statement :
                clocked_stmt = if_stmt->GetElseStmt() ;
            } else {
                clocked_stmt->Error("cannot find clocked statement in this always construct") ;
                clocked_stmt = 0 ;
            }

            /* else : run out of 'if' statements. Most be complicated edge conditions */
            nesting++ ;
        }
        if (!clocked_stmt) continue ; // invalid, or nonexisting clocked statement

        // Step (5) Collect all the target (names) assigned under this synchronous statement.

        // Use a second pattern for this :
        AssignmentVisitor target_id_collector ;
        clocked_stmt->Accept(target_id_collector) ;
        // Now, all synchronously assigned identifiers are in Set "target_id_collector._assigned_ids".

        // Step (6) Create a sequential block with 'shifted' assignments to all the target names (registers).

        Array *shift_stmts = new Array() ; // will contain all shift-assign statements for the sequential block
        SetIter siter ; // a Set iterator
        VeriIdDef *assigned_id ;

        // First insert a normal blocking assignment : "so = si ;"
        // so that 'so' becomes the scan bit between the registers:
        VeriStatement *stmt = new VeriBlockingAssign(new VeriIdRef(so), 0, new VeriIdRef(si)) ;
        shift_stmts->InsertLast(stmt) ; // the first statement

        // Then, for each assigned identifier, create an blocking assignment so that a 'scan-shifter' occurs :
        // Create an assignment of the form : " {so,<id>} = {<id>, so} ;"
        FOREACH_SET_ITEM(&(target_id_collector._assigned_ids), siter, &assigned_id) {
            // Message :
            clocked_stmt->Info("identifier %s is assigned under this clocked statement", assigned_id->Name()) ;
            assigned_id->Info("%s is declared here",assigned_id->Name()) ;

            // Create shifted assignment simply by calling parse-tree node constructors :
            Array *lhs_concat = new Array() ;
            lhs_concat->InsertLast(new VeriIdRef(so)) ;
            lhs_concat->InsertLast(new VeriIdRef(assigned_id)) ;
            VeriExpression *lhs = new VeriConcat(lhs_concat) ;

            Array *rhs_concat = new Array() ;
            rhs_concat->InsertLast(new VeriIdRef(assigned_id)) ;
            rhs_concat->InsertLast(new VeriIdRef(so)) ;
            VeriExpression *rhs = new VeriConcat(rhs_concat) ;

            // Now create the assignment with that :
            stmt = new VeriBlockingAssign(lhs, 0, rhs) ;

            // insert as the next assignment in the sequential block.
            shift_stmts->InsertLast(stmt) ;
        }

        // Finally, put all these statements in a sequential block :
        // Create a sequential block without a label, without declarations, and without a scope.
        VeriStatement *then_stmt = new VeriSeqBlock(0,0,shift_stmts,0) ;

        // Step (7) Create a 'if' statement, which switches under the 'sc' (scan-control) input.
        // Step (8) The 'then' part of the if statement becomes the shift-assignments 'then_stmt' from (6)
        // Step (9) The 'else' part of the if statement becomes the existing 'clocked_stmt' (from (4)).
        //  Make a copy of the 'else' statement, because ReplaceChild (below) will delete the original..
        VeriMapForCopy id_map_table ; // need this for copy routine
        VeriStatement *else_stmt = clocked_stmt->CopyStatement(id_map_table) ;
        VeriConditionalStatement *new_if = new VeriConditionalStatement(new VeriIdRef(sc), then_stmt, else_stmt) ;

        // Step (10) Replace statement (4) by the if-statement if (7).
        unsigned success = 0 ;
        if (if_stmt) {
             // clocked statement was nested under 'if' statement(s) :
             // Replace the old 'else' part (the 'clocked_stmt') by the new 'if' statement :
             success = if_stmt->ReplaceChildStmt(clocked_stmt, new_if) ;
        } else if (event_control_stmt) {
             // The whole event control statement is clocked :
             success = event_control_stmt->ReplaceChildStmt(clocked_stmt, new_if) ;
        }
        VERIFIC_ASSERT(success) ; // Otherwise the previous code made a mistake
    }

    // Step (11) Pretty-print the (modified) design to an output file.
    char *out_file_name = Strings::save(file_name,".out.v") ;
    veri_reader.PrettyPrint(out_file_name, 0/*print all modules*/);
    Strings::free(out_file_name) ;

    return 0; // leave with success status
}

/* ------------------------------------------------------- */

