module top (q,d,clk);

input clk, d;
output q;
reg q;

always @(clk)
begin
    q = d;
end

endmodule
    