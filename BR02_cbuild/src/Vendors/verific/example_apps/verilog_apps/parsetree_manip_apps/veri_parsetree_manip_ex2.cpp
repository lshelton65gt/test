/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>         // strcpy, strchr ...
#include <ctype.h>          // isalpha, etc ...

#include "Message.h"        // Make message class available
#include "veri_file.h"      // Make Verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriTreeNode.h"   // Definition of VeriTreeNode
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "VeriId.h"         // Definitions of all identifier definition tree nodes
#include "VeriScope.h"      // Symbol table of locally declared identifiers
#include "Strings.h"        // Definition of class to manipulate copy, concatenate, create etc...
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriVisitor.h"    // Visitor base class definition
#include "Set.h"
#include "VeriCopy.h"       // Definition of VeriMapForCopy

#include "veri_tokens.h"    // Definition of port direction VERI_OUTPUT, etc ...

/*
    BUILD NOTE!!! : This file needs to be compiled with the same verilog compile switches,
                    that the verilog lib was built with, otherwise strange behavior may occur!
*/

/****************************************************************************************
******************************* VERILOG EXAMPLE #6 **************************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is a parse tree modification example that shows:

         How to replace an existing 'AND' gate instantiation by concurrent assignment.

*****************************************************************************************/

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------------------- */

    // This example makes use of the visitor class, which is a well-known SW design
    // pattern for traversing a data-structure.  The base verilog visitor class
    // is located in the VeriVisitor.* files, and this base class already defines
    // the default traversing algorithm to walk a verilog parse-tree.  A newly
    // derived class, called AndGateVisitor, was created to collect certain verilog
    // parse-tree constructs very easily.

class AndGateVisitor : public VeriVisitor
{
public:
    AndGateVisitor() : _gate_insts(POINTER_HASH) {}
    virtual ~AndGateVisitor() {}

    // The following Visit methods need to be redefined for our purpose :

    // The following class definition can be found in VeriModuleItem.h
    virtual void Visit(VeriGateInstantiation &node) ;
    // Accessor methods
    Set* GetGateInsts() { return &_gate_insts ; }

private:
    Set _gate_insts ;      // Container (Set) of VeriGateInstantiation*'s

    // Prevent the compiler from implementing the following
    AndGateVisitor(const AndGateVisitor &node) ;
    AndGateVisitor& operator=(const AndGateVisitor &rhs) ;
} ;

/* -------------------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif
#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif
void AndGateVisitor::Visit(VeriGateInstantiation &node)
{
    if (node.GetInstType() != VERI_AND) return ; // If the gate is not an AND gate then return
    _gate_insts.Insert(&node) ;  // Store this node
    // We don't need to traverse any of this class's members.
}
/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    if (argc < 2) Message::PrintLine("Default input file: ex2_top.v. Specify command line argument to override") ;

    // To replace instantiation the following procedure is followed.
    /****************************************************************************************
     1) Get the handle to the top level module of the analyzed file
     2) Use Visitor pattern to visit the desired VeriGateInstantiation class
     3) Copy the actuals of the instantiation so that the first terminal forms the LHS
        of the assignment while the rest are connected with the binary AND operator
        forming the RHS of the assignment
     4) Create an object of VeriNetRegAssign with the LHS and RHS created in the previous step
     5) Create an array of assignments and insert the assignment made in the
        previous step.
     6) Create an instance of VeriContinuousAssign with the created array.
     7) Call module->ReplaceChildModuleItem to replace the instantiation by continuous
        assignment
    ****************************************************************************************/

    // Get the file name to work on. If not specified as command line arg use default file
    const char *file_name = (argc > 1) ? argv[1] : "ex2_top.v" ;

    // Analyze the design file(in verilog2000 mode), if there is any error, don't proceed further!
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veri_reader.Analyze(file_name, 1/*Verilog 2000*/)) return 1 ;

    // Get all the top level modules
    Array *all_top_modules = veri_file::GetTopModules() ;
    // Get a handle of the first top level module, if any.
    VeriModule *mod = (all_top_modules && all_top_modules->Size()) ? (VeriModule *)all_top_modules->GetFirst() : 0 ;
    delete all_top_modules ; // Delete the array
    if (!mod) {
        Message::PrintLine("Cannot find a top level module") ;
        return 1 ;
    }

    //---------------------------------------
    //  Pretty Print (Pre Parse-tree Modification)
    //---------------------------------------
    char *before_print = Strings::save(mod->Name(), "_before_ex2_pp.v") ;
    // Pretty print current design
    veri_reader.PrettyPrint(before_print, 0, "work") ;
    Strings::free(before_print) ; // Free the dynamically allocated strings

    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
     |    REPLACE INSTANTIATION BY CONTINUOUS ASSIGNMENT  |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    //---------------------------------------------------------------------
    // 1) Get the handle to the module that was just analyzed
    //---------------------------------------------------------------------
    // Module 'mod' is the handle to the top level module

    //--------------------------------------------------------------------------
    // 2) Use Visitor pattern to visit the desired VeriGateInstantiation class
    //--------------------------------------------------------------------------
    AndGateVisitor visitor ; // Declare an object of the AndGateVisitor class derived from the VeriVisitor class
    // Traverse parse-tree and collect what we're looking for viz. 'AND' gate instantiations
    mod->Accept(visitor) ;

    // Now let's look at all gate instantiations that we collected
    SetIter si ;
    VeriGateInstantiation *inst ;
    Set *and_gate_inst_set = visitor.GetGateInsts() ;

    // If no 'AND' instantiation present then abort
    if (!and_gate_inst_set->Size()) {
        Message::PrintLine("Cannot find any suitable 'AND' gate instantiation to replace") ;
        return 1 ;
    }
    // Iterate through the array of collected instances
    FOREACH_SET_ITEM(and_gate_inst_set, si, &inst) {
        if (!inst) continue ;

        // Create an array of assignments to store the created assignments
        Array *assign_list = new Array (1) ; // Stores a list of assignments
        Array *inst_arr = inst->GetInstances() ; // Array of VeriInstId*
        unsigned j ;
        VeriInstId *inst_id ;
        // Iterate through all the 'AND' gate instances and create
        // VeriNetRegAssign for each instance
        // eg. :
        //      and i1(o1, a, b), i2 (o2, c, d) ;
        // is replaced by
        //      assign o1 = a & b, o2 = c & d ;
        FOREACH_ARRAY_ITEM(inst_arr, j, inst_id) {
            if (!inst_id) continue ;
            Array *port_arr = inst_id->GetPortConnects() ; // Array of VeriExpressions (VeriPortConnect/VeriIdRef/VeriExpressions)
            VeriExpression *prev = 0 ;
            VeriExpression *new_expr = 0 ;
            unsigned k ;
            VeriExpression *expr ;
            VeriMapForCopy id_map_table ;
            //---------------------------------------------------------------------
            // 3) Copy the actuals of instantiation so that the first terminal forms the LHS
            // of the assignment while the rest are connected with the binary AND operator
            // forming the RHS of the assignment
            //---------------------------------------------------------------------
            // Create an instance of VeriBinaryOperator for the given 'AND' gate
            FOREACH_ARRAY_ITEM(port_arr, k, expr) {
                if (!k || !expr) continue ; // As the 0th port (ie. k=0) is the lhs

                // Copy the kth port of the 'AND' gate
                new_expr = expr->CopyExpression(id_map_table) ;

                if (prev) { // If there are at least two inputs to the 'AND' gate
                    prev = new VeriBinaryOperator (VERI_REDAND, prev, new_expr) ;
                } else { // Initially when value of prev is '0'
                    prev = new_expr ;
                }
            }

            //---------------------------------------------------------------------
            // 4) Create an instance of VeriNetRegAssign
            //---------------------------------------------------------------------
            VeriExpression *lhs_of_assignment =  (port_arr && port_arr->Size()) ? (VeriExpression *)port_arr->GetFirst() : 0 ; // Get the first port of the port array
            VeriNetRegAssign *assign = new VeriNetRegAssign (lhs_of_assignment ? lhs_of_assignment->CopyExpression(id_map_table) : 0, prev) ; // Copy it

            //---------------------------------------------------------------------
            // 5) Insert the created assignment
            //---------------------------------------------------------------------
            assign_list->InsertLast(assign) ;
        }
        //---------------------------------------------------------------------
        //6) Create an instance of VeriContinuousAssign
        //---------------------------------------------------------------------
        VeriContinuousAssign  *continuous_assignment = new  VeriContinuousAssign(0, 0, assign_list) ;
        // Resolve reference, links VeriIdRef to VeriIdDef
        continuous_assignment->Resolve(mod->GetScope(), VeriTreeNode::VERI_UNDEF_ENV) ;

        //---------------------------------------------------------------------
        // 7) Replaces the instantiation by the created continuous assignment
        //---------------------------------------------------------------------
        mod->ReplaceChildModuleItem(inst, continuous_assignment) ;
    }

    //---------------------------------------
    //  Pretty Print (Post Parse-tree Modification)
    //---------------------------------------
    // To make sure that everything went as planned, pretty print the current
    // parse-tree's out and compare with the previous pretty printed output.
    char *after_print = Strings::save(mod->Name(), "_after_ex2_pp.v") ;
    veri_reader.PrettyPrint(after_print, 0, "work") ;

    // ALL DONE!!!
    Strings::free(after_print) ; // Free the dynamically allocated strings

    return 1 ;
}

/* ------------------------------------------------------- */

