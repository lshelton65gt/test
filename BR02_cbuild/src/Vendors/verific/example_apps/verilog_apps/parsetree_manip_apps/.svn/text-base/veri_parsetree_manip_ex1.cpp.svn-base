/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>         // strcpy, strchr ...
#include <ctype.h>          // isalpha, etc ...

#include "Message.h"        // Make message class available
#include "veri_file.h"      // Make Verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriTreeNode.h"   // Definition of VeriTreeNode
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "VeriId.h"         // Definitions of all identifier definition tree nodes
#include "VeriScope.h"      // Symbol table of locally declared identifiers
#include "Strings.h"        // Definition of class to manipulate copy, concatenate, create etc...

#include "veri_tokens.h"    // Definition of port direction VERI_OUTPUT, etc ...

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*
    BUILD NOTE!!! : This file needs to be compiled with the same verilog compile switches,
                    that the verilog lib was built with, otherwise strange behavior may occur!
*/

/****************************************************************************************
******************************* VERILOG EXAMPLE #5 **************************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is a parse tree modification example that shows:

        1) How to add an extra output port ("X" in this example) to a module (specified
           as a command line argument or the one found by anlyzing the default file)
        2) How to add an instance of a module  ("NEW_MODULE") to the top level module
        3) How to make a connection between the newly created port ("X") and the newly created
           instance's output port.

*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    if (argc < 2) Message::PrintLine("Default input file: ex1_top.v. Specify command line argument to override") ;

    // The following operations will be done by this application on the file:
    // 1. File specified in the first command line argument (or the default file) will be analyzed.
    // 2. Get a handle to the top level module that was analyzed
    // 3. Pretty print the pre-modified parse tree
    // 4. Call AddPort to add a port 'X'
    // 5. The file containing the instantiated module ie. NEW_MODULE.v to be analyzed
    // 6. Call AddInstance to add a new module instantiation of "NEW_MODULE"
    // 7. Call AddPortRef to connect the output port of NEW_MODULE with "X"
    // 8. Pretty print the modified parse tree

    // Get the file name to work on. If not specified as command line arg use default file
    const char *file_name = (argc > 1) ? argv[1] : "ex1_top.v" ;

    // Analyze the design file(in verilog2k mode), if there is any error, don't proceed further!
    if (!veri_file::Analyze(file_name, 1/*Verilog 2000*/)) return 1 ;

    // Get all the top level modules
    Array *all_top_modules = veri_file::GetTopModules("work") ;
    // Get a handle of the first top level module, if any.
    VeriModule *mod = (all_top_modules && all_top_modules->Size()) ? (VeriModule *)all_top_modules->GetFirst() : 0 ;
    delete all_top_modules ;
    if (!mod) {
        Message::PrintLine("Cannot find a top level module") ;
        return 1 ;
    }

    //---------------------------------------
    //  Pretty Print (Pre Parse-tree Modification)
    //---------------------------------------
    char *before_print = Strings::save(mod->Name(), "_before_ex1_pp.v") ;

    // Pretty print current design
    veri_reader.PrettyPrint(before_print, 0, "work") ;
    Strings::free(before_print) ;

    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
     |     ADD NEW OUTPUT PORT "X" TO TOP LEVEL MODULE   |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // Verilog LRM defines a module items list consisting of items in the module body.Addition of
    // a new port takes place in the module items list.
    //---------------------------------------------------------------------
    // Get a handle to the top module that was just analyzed
    //---------------------------------------------------------------------
    // Module 'mod' is the handle to the top level module

    //---------------------------------------------------------------------
    // Call 'AddPort' to add port 'X'
    //---------------------------------------------------------------------
    VeriIdDef *new_port_id = mod->AddPort("X", VERI_OUTPUT, 0) ; // To store created port identifier

    if(!new_port_id) { // Cannot add port X
        Message::PrintLine("Cannot add port 'X' to module '", mod->Name(), "'") ;
        return 1 ;
    }

    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
     | INSERT NEW INSTANTIATION INTO TOP LEVEL MODULE |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // As defined in the Verilog LRM, a module consists of a name, parameters, ports and module items.
    // To add a new instance to a module, the module instantiation object needs to be added to
    // the module items list

    // Now analyze the file ex1_newmodule.v containing the module to be instantiated here in a seperate library
    // Second argument: VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5
    if (!veri_reader.Analyze("ex1_newmodule.v", 1/*Verilog 2000*/, "lib1")) return 1 ;
    VeriModuleInstantiation *new_instance = 0 ; // To hold created instantiation

    new_instance = mod->AddInstance("i2", "NEW_MODULE") ;

    if (new_instance) { // Instance is ready, add actuals for ports
        // Add actual for formal 'out'
        mod->AddPortRef("i2", "out", new VeriIdRef(Strings::save("X"))) ;
    } else {
        Message::PrintLine("Cannot add instance of module 'NEW_MODULE' to module '", mod->Name(), "'") ;
    }

    //---------------------------------------
    // Pretty Print (Post Parse-tree Modification)
    //---------------------------------------
    // To make sure that everything went as planned, pretty print the current
    // parse-tree's output and compare with the previous pretty printed output.
    char *after_print = Strings::save(mod->Name(), "_after_ex1_pp.v") ;
    veri_reader.PrettyPrint(after_print, 0, "work") ;

    // ALL DONE!!!

    Strings::free(after_print) ; // Free the dynamically allocated strings

    return 1 ;
}

/* ------------------------------------------------------- */

