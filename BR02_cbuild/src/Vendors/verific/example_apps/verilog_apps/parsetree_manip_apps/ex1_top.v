module TOP(a, b, o1) ;
    input a,b;
    output o1 ;
    and2 i1(o1, a, b) ;
endmodule

module and2 (output out, input in1, in2) ;
    assign out = in1 & in2 ;
endmodule
