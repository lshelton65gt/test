/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Ex4Visitor.h"    // Visitor base class definition

#include "Array.h"              // Make dynamic array class Array available
#include "Strings.h"            // A string utility/wrapper class
#include "Message.h"            // Make message handlers available, not used in this example

#include "VhdlUnits.h"          // Definition of a libraries and design units
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations
#include "VhdlExpression.h"     // Definitions of all vhdl expressions
#include "VhdlStatement.h"      // Definitions of all vhdl statements
#include "VhdlIdDef.h"          // Definitions of all vhdl identifiers
#include "VhdlName.h"           // Definitions of all vhdl names
#include "VhdlConfiguration.h"  // Definitions of all vhdl configurations
#include "VhdlSpecification.h"  // Definitions of all vhdl specifications
#include "VhdlMisc.h"           // Definitions of miscellaneous vhdl constructrs
#include "vhdl_tokens.h"

#include <stdio.h>              // sprintf
#include <string.h>             // strcpy, strchr ...
#include <ctype.h>              // isalpha, etc ...

using namespace std ;

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*-----------------------------------------------------------------*/
//                      Constructor / Destructor
/*-----------------------------------------------------------------*/

VhdlPrettyPrintVisitor::VhdlPrettyPrintVisitor(char *pFileName)
    : _ofs(pFileName, ios::out),
      _bFileGood(true),
      _nLevel(0)
{
    if (!_ofs.rdbuf()->is_open()){
        Message::Error(0, "cannot open file ", pFileName) ;
        _bFileGood = false;
    }
}

VhdlPrettyPrintVisitor::~VhdlPrettyPrintVisitor()
{
    _ofs.close();
    _bFileGood = false;
}

/*-----------------------------------------------------------------*/
//                          Utility Methods
/*-----------------------------------------------------------------*/

// static
const char* VhdlPrettyPrintVisitor::PrintLevel(unsigned level)
{
    // 80 spaces
    static const char *pSpaces = "                                                                                " ;
    static const unsigned nTabSize = 4;
    // Don't overflow on the fixed-length spaces constant
    unsigned space = Strings::len(pSpaces) ;
    if ((nTabSize * level) > space) return pSpaces ;
    return pSpaces + space - (nTabSize * level) ;
}

/*-----------------------------------------------------------------*/
//        Visit Methods : Class details in VhdlTreeNode.h
/*-----------------------------------------------------------------*/

// VhdlTreeNode : Don't need to override

/*-----------------------------------------------------------------*/
//        Visit Methods : Class details in VhdlUnits.h
/*-----------------------------------------------------------------*/

// VhdlDesignUnit : Don't need to override

// VhdlPrimaryUnit : Don't need to override

// VhdlSecondaryUnit : Don't need to override

void VhdlPrettyPrintVisitor::Visit(VhdlEntityDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

        unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(node.GetContextClause(),i,elem) {
        elem->Accept(*this) ;
    }

    _ofs << PrintLevel(_nLevel) << "entity " << node.Id()->Name() << " is " << endl ;

    IncTabLevel(1) ;

    // Print generic clause contents (if it exists)
    if (node.GetGenericClause()) {
        _ofs << PrintLevel(_nLevel) << "generic (" ;
        IncTabLevel(1) ;
        FOREACH_ARRAY_ITEM(node.GetGenericClause(),i,elem) {
            _ofs << endl << PrintLevel(_nLevel) ; // newline
            elem->Accept(*this) ;
            if (i!=node.GetGenericClause()->Size()-1) _ofs << "; " ;
        }
        DecTabLevel(1) ;
        _ofs << ") ;" << endl ;
    }

    // Print port clause contents (if it exists)
    if (node.GetPortClause()) {
        _ofs << PrintLevel(_nLevel) << "port (" ;
        IncTabLevel(1) ;
        FOREACH_ARRAY_ITEM(node.GetPortClause(),i,elem) {
            _ofs << endl << PrintLevel(_nLevel) ; // newline
            elem->Accept(*this) ;
            if (i!=node.GetPortClause()->Size()-1) _ofs << "; " ;
        }
        DecTabLevel(1) ;
        _ofs << ") ;" << endl ;
    }

    // Print declarations
    FOREACH_ARRAY_ITEM(node.GetDeclPart(),i,elem) {
        elem->Accept(*this) ;
    }

    // Print statements
    if (node.GetStatementPart()) {
        _ofs << PrintLevel(_nLevel) << "begin" << endl ;
        FOREACH_ARRAY_ITEM(node.GetStatementPart(),i,elem) {
            elem->Accept(*this) ;
        }
    }
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "end " << node.Id()->Name() << " ;" << endl ;
    _ofs << endl ;

    // Print all secondary units
    MapIter mi ;
    FOREACH_VHDL_SECONDARY_UNIT(&node,mi,elem) {
        elem->Accept(*this) ;
    }
}

void VhdlPrettyPrintVisitor::Visit(VhdlConfigurationDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Print context clause
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(node.GetContextClause(),i,elem) {
        elem->Accept(*this) ;
    }

    _ofs << PrintLevel(_nLevel) << "configuration " << node.Id()->Name() << " of " ;

    // Print entity name
    IncTabLevel(1) ;
    if (node.GetEntityName()) {
        node.GetEntityName()->Accept(*this) ;
    }
    _ofs << " is " << endl ;

    // Print declarations
    FOREACH_ARRAY_ITEM(node.GetDeclPart(),i,elem) {
        elem->Accept(*this) ;
    }

    // Print block configuration
    if (node.GetBlockConfiguration()) {
        node.GetBlockConfiguration()->Accept(*this) ;
    }
    DecTabLevel(1) ;

    _ofs << PrintLevel(_nLevel) << "end " << node.Id()->Name() << " ;" << endl ;
    _ofs << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlPackageDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Print context clause
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(node.GetContextClause(),i,elem) {
        elem->Accept(*this) ;
    }

    _ofs << PrintLevel(_nLevel) << "package " << node.Id()->Name() << " is" << endl ;

    // Print declarations
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetDeclPart(),i,elem) {
        elem->Accept(*this) ;
    }
    DecTabLevel(1) ;

    _ofs << PrintLevel(_nLevel) << "end " << node.Id()->Name() << " ;" << endl ;
    _ofs << endl ;
    // Print all secundary units
    MapIter mi ;
    FOREACH_VHDL_SECONDARY_UNIT(&node,mi,elem) {
        elem->Accept(*this) ;
    }
}

void VhdlPrettyPrintVisitor::Visit(VhdlArchitectureBody &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Print context clause
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(node.GetContextClause(),i,elem) {
        elem->Accept(*this) ;
    }

    _ofs << PrintLevel(_nLevel) << "architecture " << node.Id()->Name() << " of " ;

    IncTabLevel(1) ;
    // Print entity name
    if (node.GetEntityName()) {
        node.GetEntityName()->Accept(*this) ;
    }
    _ofs << " is " << endl ;

    // Print declarations
    FOREACH_ARRAY_ITEM(node.GetDeclPart(),i,elem) {
        elem->Accept(*this) ;
    }
    DecTabLevel(1) ;

    _ofs << PrintLevel(_nLevel) << "begin" << endl ;

    // Print statements
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetStatementPart(),i,elem) {
        elem->Accept(*this) ;
    }
    DecTabLevel(1) ;

    _ofs << PrintLevel(_nLevel) << "end " << node.Id()->Name() << " ;" << endl ;
    _ofs << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlPackageBody &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Print context clause
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(node.GetContextClause(),i,elem) {
        elem->Accept(*this) ;
    }

    _ofs << PrintLevel(_nLevel) << "package body " << node.Id()->Name() << " is" << endl ;
    // Print declarations
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetDeclPart(),i,elem) {
        elem->Accept(*this) ;
    }
    DecTabLevel(1) ;

    _ofs << PrintLevel(_nLevel) << "end " << node.Id()->Name() << " ;" << endl ;
    _ofs << endl ;
}

/*-----------------------------------------------------------------*/
//        Visit Methods : Class details in VhdlConfiguration.h
/*-----------------------------------------------------------------*/

// VhdlConfigurationItem : Don't need to override

void VhdlPrettyPrintVisitor::Visit(VhdlBlockConfiguration &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "for " ;
    IncTabLevel(1) ;
    // Print block specification
    if (node.GetBlockSpec()) {
        node.GetBlockSpec()->Accept(*this) ;
    }

    _ofs << endl ;
    // Print use clause
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetUseClauseList(),i,item) {
        item->Accept(*this) ;
    }

    // Print configuration item list
    FOREACH_ARRAY_ITEM(node.GetConfigurationItemList(),i,item) {
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "end for ;" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlComponentConfiguration &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "for " ;
    IncTabLevel(1) ;
    // Print component spec
    if (node.GetComponentSpec()) {
        node.GetComponentSpec()->Accept(*this) ;
    }
    _ofs << " " ;

    // Print binding indication
    if (node.GetBinding()) {
        node.GetBinding()->Accept(*this) ;
        _ofs << ";" ;
    }
    _ofs << " " ;

    // Print block configuration
    if (node.GetBlockConfiguration()) {
        node.GetBlockConfiguration()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << " end for ; " << endl ;
}

/*-----------------------------------------------------------------*/
//        Visit Methods : Class details in VhdlDeclaration.h
/*-----------------------------------------------------------------*/

// VhdlDeclaration : Don't need to override

// VhdlTypeDef : Don't need to override

void VhdlPrettyPrintVisitor::Visit(VhdlScalarTypeDef &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "range " ;

    // Print scalar range
    if (node.GetScalarRange()) {
        IncTabLevel(1) ;
        node.GetScalarRange()->Accept(*this) ;
        DecTabLevel(1) ;
    }
}

void VhdlPrettyPrintVisitor::Visit(VhdlArrayTypeDef &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "array (" ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    // Print index constraint
    FOREACH_ARRAY_ITEM(node.GetIndexConstraint(),i,item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    _ofs << ") of " ;

    // Print subtype indication
    if (node.GetSubtypeIndication()) {
        node.GetSubtypeIndication()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlRecordTypeDef &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    _ofs << "record " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetElementDeclList(), i, item) {
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "end record " ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlAccessTypeDef &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    _ofs << "access " ;
    if (node.GetSubtypeIndication()) {
        node.GetSubtypeIndication()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlFileTypeDef &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    _ofs << "file of " ;
    if (node.GetFileTypeMark()) {
        node.GetFileTypeMark()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlEnumerationTypeDef &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "(" ;
    unsigned i;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetEnumLiteralList(),i,item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ")" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlPhysicalTypeDef &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "range " ;
    IncTabLevel(1) ;
    if (node.GetRangeConstraint()) {
        node.GetRangeConstraint()->Accept(*this) ;
    }
    _ofs << " units " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetPhysicalUnitDeclList(), i, item) {
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "end units " ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlElementDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetIds(),i,item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    _ofs << " : " ;
    if (node.GetSubtypeIndication()) {
        node.GetSubtypeIndication()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlPhysicalUnitDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << node.GetId()->Name() ;
    if (node.GetPhysicalLiteral()) {
        _ofs << " = " ;
        IncTabLevel(1) ;
        node.GetPhysicalLiteral()->Accept(*this) ;
        DecTabLevel(1) ;
    }
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlUseClause &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "use " ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetSelectedNameList(),i,item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlLibraryClause &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "library " ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetIds(),i,item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlInterfaceDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    if (node.GetInterfaceKind()) {
        _ofs << VhdlNode::EntityClassName(node.GetInterfaceKind()) << " " ;
    }
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetIds(),i,item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    _ofs << " : " ;
    if (node.GetMode()) _ofs << VhdlNode::EntityClassName(node.GetMode()) << " " ;
    if (node.GetSubtypeIndication()) {
        node.GetSubtypeIndication()->Accept(*this) ;
    }
    if (node.GetSignalKind()) _ofs << " " << VhdlNode::EntityClassName(node.GetSignalKind()) ;
    if (node.GetInitAssign()) {
        _ofs << " := " ;
        node.GetInitAssign()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlSubprogramDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    if (node.GetSubprogramSpec()){
        node.GetSubprogramSpec()->Accept(*this) ;
    }
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlSubprogramBody &node)
{
    if (!_bFileGood) return ; // file stream is not good

    if (node.GetSubprogramSpec()) {
        node.GetSubprogramSpec()->Accept(*this) ;
    }
    _ofs << " is " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetDeclPart(),i,item) {
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;

    _ofs << PrintLevel(_nLevel) << "begin" << endl ;

    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetStatementPart(),i,item) {
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "end ;" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlSubtypeDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "subtype " << node.GetId()->Name() << " is " ;
    if (node.GetSubtypeIndication()) {
        IncTabLevel(1) ;
        node.GetSubtypeIndication()->Accept(*this) ;
        DecTabLevel(1) ;
    }
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlFullTypeDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "type " << node.GetId()->Name() << " is " ;
    if (node.GetTypeDef()) {
        IncTabLevel(1) ;
        node.GetTypeDef()->Accept(*this) ;
        DecTabLevel(1) ;
    }
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlIncompleteTypeDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "type " << node.GetId()->Name() ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlConstantDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "constant " ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetIds(),i,item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    _ofs << " : " ;
    if (node.GetSubtypeIndication()) {
        node.GetSubtypeIndication()->Accept(*this) ;
    }
    if (node.GetInitAssign()) {
        _ofs << " := " ;
        node.GetInitAssign()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlSignalDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "signal " ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetIds(),i,item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    _ofs << " : " ;
    if (node.GetSubtypeIndication()) {
        node.GetSubtypeIndication()->Accept(*this) ;
    }
    if (node.GetSignalKind()) {
        _ofs << ((node.GetSignalKind()==VHDL_bus) ? " bus" : " register") ;
    }
    if (node.GetInitAssign()) {
        _ofs << " := " ;
        node.GetInitAssign()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlVariableDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << ((node.IsShared()) ? "shared " : "") << "variable " ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetIds(),i,item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    _ofs << " : " ;
    if (node.GetSubtypeIndication()) {
        node.GetSubtypeIndication()->Accept(*this) ;
    }
    if (node.GetInitAssign()) {
        _ofs << " := " ;
        node.GetInitAssign()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlFileDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "file " ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetIds(),i,item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    _ofs << " : " ;
    if (node.GetSubtypeIndication()) {
        node.GetSubtypeIndication()->Accept(*this) ;
    }
    if (node.GetFileOpenInfo()) {
        _ofs << " " ;
        node.GetFileOpenInfo()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlAliasDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "alias " << node.GetDesignator()->Name() ;
    IncTabLevel(1) ;
    if (node.GetSubtypeIndication()) {
        _ofs << " : " ;
        node.GetSubtypeIndication()->Accept(*this) ;
    }
    _ofs << " is " ;
    if (node.GetAliasTargetName()) {
        node.GetAliasTargetName()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlComponentDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "component " << node.GetId()->Name() << endl ;
    unsigned i ;
    VhdlTreeNode *elem ;
    IncTabLevel(1) ;
    if (node.GetGenericClause()) {
        _ofs << PrintLevel(_nLevel) << "generic (" ;
        IncTabLevel(1) ;
        FOREACH_ARRAY_ITEM(node.GetGenericClause(),i,elem) {
            _ofs << endl << PrintLevel(_nLevel) ; // newline
            elem->Accept(*this) ;
            if (i!=node.GetGenericClause()->Size()-1) _ofs << "; " ;
        }
        DecTabLevel(1) ;
        _ofs << ") ;" << endl ;
    }
    if (node.GetPortClause()) {
        _ofs << PrintLevel(_nLevel) << "port (" ;
        IncTabLevel(1) ;
        FOREACH_ARRAY_ITEM(node.GetPortClause(),i,elem) {
            _ofs << endl << PrintLevel(_nLevel) ; // newline
            elem->Accept(*this) ;
            if (i!=node.GetPortClause()->Size()-1) _ofs << "; " ;
        }
        DecTabLevel(1) ;
        _ofs << ") ;" << endl ;
    }
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "end component;" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlAttributeDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "attribute " << node.GetId()->Name() << " : " ;
    IncTabLevel(1) ;
    if (node.GetTypeMark()) {
        node.GetTypeMark()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlAttributeSpec &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "attribute " ;
    IncTabLevel(1) ;
    if (node.GetDesignator()) {
        node.GetDesignator()->Accept(*this) ;
    }
    _ofs << " of " ;
    if (node.GetEntitySpec()) {
        node.GetEntitySpec()->Accept(*this) ;
    }
    _ofs << " is " ;
    if (node.GetValue()) {
        node.GetValue()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlConfigurationSpec &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "for " ;
    IncTabLevel(1) ;
    if (node.GetComponentSpec()) {
        node.GetComponentSpec()->Accept(*this) ;
        _ofs << " " ;
    }
    if (node.GetBinding()) {
        node.GetBinding()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlDisconnectionSpec &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "disconnect " ;
    IncTabLevel(1) ;
    if (node.GetGuardedSignalSpec()) {
        node.GetGuardedSignalSpec()->Accept(*this) ;
    }
    _ofs << " after " ;
    if (node.GetAfter()) {
        node.GetAfter()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlGroupTemplateDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "group " << node.GetId()->Name() << " is (" ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetEntityClassEntryList(), i, item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ") ;" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlGroupDecl &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "group " << node.GetId()->Name() << " : " ;
    IncTabLevel(1) ;
    if (node.GetGroupTemplateName()) {
        node.GetGroupTemplateName()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

/*-----------------------------------------------------------------*/
//        Visit Methods : Class details in VhdlExpression.h
/*-----------------------------------------------------------------*/

// VhdlExpression : Don't need to override

// VhdlDiscreteRange : Don't need to override

// VhdlSubtypeIndication : Don't need to override

void VhdlPrettyPrintVisitor::Visit(VhdlExplicitSubtypeIndication &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.GetResolutionFunction()) {
        node.GetResolutionFunction()->Accept(*this) ;
        _ofs << " " ;
    }
    if (node.GetTypeMark()) {
        node.GetTypeMark()->Accept(*this) ;
        _ofs << " " ;
    }
    if (node.GetRangeConstraint()) {
        _ofs << "range " ;
        node.GetRangeConstraint()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlRange &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.GetLeftExpression()) {
        node.GetLeftExpression()->Accept(*this) ;
    }
    if (node.GetDir()) _ofs << " " << ((node.GetDir()==VHDL_to) ? "to" : "downto") << " " ;
    if (node.GetRightExpression()) {
        node.GetRightExpression()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlBox &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "<>" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlAssocElement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.FormalPart()) {
        node.FormalPart()->Accept(*this) ;
        _ofs << " => " ;
    }
    if (node.ActualPart()) {
        node.ActualPart()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlOperator &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "(" ; // Parenthesize the operator call
    IncTabLevel(1) ;
    if (!node.GetRightExpression()) {
        // Unary operator
        _ofs << VhdlNode::OperatorSimpleName(node.GetOperatorToken()) << " " ;
        if (node.GetLeftExpression()) {
            node.GetLeftExpression()->Accept(*this) ;
        }
    } else {
        // Binary operator
        if (node.GetLeftExpression()) {
            node.GetLeftExpression()->Accept(*this) ;
        }
        _ofs << " " << VhdlNode::OperatorSimpleName(node.GetOperatorToken()) << " " ;
        if (node.GetRightExpression()) {
            node.GetRightExpression()->Accept(*this) ;
        }
    }
    DecTabLevel(1) ;
    _ofs << ")" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlAllocator &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "new " ;
    IncTabLevel(1) ;
    if (node.GetSubtype()) {
        node.GetSubtype()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlAggregate &node)
{
    if (!_bFileGood) return ; // file stream is not good

    unsigned i ;
    VhdlTreeNode *item ;
    _ofs << "(" ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetElementAssocList(),i,item) {
        if (i!=0) _ofs << "," ;
        if (item) {
            item->Accept(*this) ;
        } else {
            _ofs << "open" ;
        }
    }
    DecTabLevel(1) ;
    _ofs << ")" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlQualifiedExpression &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.GetPrefix()) {
        node.GetPrefix()->Accept(*this) ;
    }
    _ofs << "'(" ;
    if (node.GetAggregate()) {
        node.GetAggregate()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ")" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlElementAssoc &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.GetChoices()) {
        unsigned i ;
        VhdlTreeNode *item ;
        FOREACH_ARRAY_ITEM(node.GetChoices(), i, item) {
            if (i!=0) _ofs << "|" ;
            item->Accept(*this) ;
        }
        _ofs << " => " ;
    }
    if (node.GetExpression()) {
        node.GetExpression()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlWaveformElement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.GetValue()) {
        node.GetValue()->Accept(*this) ;
    }
    if (node.GetAfter()) {
        _ofs << " after " ;
        node.GetAfter()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

/*-----------------------------------------------------------------*/
//        Visit Methods : Class details in VhdlIdDef.h
/*-----------------------------------------------------------------*/

void VhdlPrettyPrintVisitor::Visit(VhdlIdDef &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << node.OrigName() ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlLibraryId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlGroupId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlGroupTemplateId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlAttributeId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlComponentId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlAliasId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlFileId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlVariableId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlSignalId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlConstantId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlTypeId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlUniversalInteger &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlUniversalReal &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlAnonymousType &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlSubtypeId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlSubprogramId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlOperatorId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlInterfaceId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlEnumerationId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlElementId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlPhysicalUnitId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlEntityId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlArchitectureId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlConfigurationId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlPackageId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlPackageBodyId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlLabelId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlBlockId &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Call VhdlIdDef's method
    Visit((VhdlIdDef&)node) ;
}

/*-----------------------------------------------------------------*/
//        Visit Methods : Class details in VhdlMisc.h
/*-----------------------------------------------------------------*/

void VhdlPrettyPrintVisitor::Visit(VhdlSignature &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "[" ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetTypeMarklist(), i, item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    if (node.GetReturnType()) {
        _ofs << " return " ;
        node.GetReturnType()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << "]" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlFileOpenInfo &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.GetFileOpen()) {
        // VHDL 93 file open mode
        _ofs << " open " ;
        node.GetFileOpen()->Accept(*this) ;
    }

    _ofs << " is " ;
    if (node.GetOpenMode()) {
        // VHDL 87 file open mode
        _ofs << VhdlNode::EntityClassName(node.GetOpenMode()) ;
        _ofs << " " ;
    }
    if (node.GetFileLogicalName()) {
        node.GetFileLogicalName()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlBindingIndication &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    //if (node.EntityAspect()) {
    // VIPER #4312 : Use parse tree node instead of resolved entity name to
    // print unit name even when parse tree is not resolved
    if (node.GetEntityAspectNameNode()) {
        _ofs << " use " ;
        //node.EntityAspect()->Accept(*this) ;
        node.GetEntityAspectNameNode()->Accept(*this) ;
    }
    unsigned i ;
    VhdlTreeNode *item ;
    if (node.GetGenericBinding()) {
        _ofs << " generic map (" ;
        FOREACH_ARRAY_ITEM(node.GetGenericBinding(), i, item) {
            if (i!=0) _ofs << ", " ;
            if (item) {
                item->Accept(*this) ;
            } else {
                _ofs << "open" ;
            }
        }
        _ofs << ")" ;
    }
    if (node.GetPortBinding()) {
        _ofs << " port map (" ;
        FOREACH_ARRAY_ITEM(node.GetPortBinding(), i, item) {
            if (i!=0) _ofs << ", " ;
            if (item) {
                item->Accept(*this) ;
            } else {
                _ofs << "open" ;
            }
        }
        _ofs << ")" ;
    }
    DecTabLevel(1) ;
}

// VhdlIterScheme : Don't need to override

void VhdlPrettyPrintVisitor::Visit(VhdlWhileScheme &node)
{
    if (!_bFileGood) return ; // file stream is not good

    if (node.GetCondition()) {
        IncTabLevel(1) ;
        _ofs << "while " ;
        node.GetCondition()->Accept(*this) ;
        DecTabLevel(1) ;
    }
}

void VhdlPrettyPrintVisitor::Visit(VhdlForScheme &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    _ofs << "for " ;
    if (node.GetId()) {
        node.GetId()->Accept(*this) ;
    }
    _ofs << " in " ;
    if (node.GetRange()) {
        node.GetRange()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlIfScheme &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "if " ;
    IncTabLevel(1) ;
    if (node.GetCondition()) {
        node.GetCondition()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

// VhdlDelayMechanism : Don't need to override

void VhdlPrettyPrintVisitor::Visit(VhdlTransport &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "transport " ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlInertialDelay &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.GetRejectExpression()) {
        _ofs << "reject " ;
        node.GetRejectExpression()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << " inertial" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlOptions &node)
{
    if (!_bFileGood) return ; // file stream is not good

    if (node.IsGuarded()) {
        _ofs << "guarded " ;
    }
    IncTabLevel(1) ;
    if (node.GetDelayMechanism()) {
        node.GetDelayMechanism()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlConditionalWaveform &node)
{
    if (!_bFileGood) return ; // file stream is not good

    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetWaveform(), i, item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    if (node.Condition()) {
        _ofs << " when " ;
        node.Condition()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlSelectedWaveform &node)
{
    if (!_bFileGood) return ; // file stream is not good

    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetWaveform(), i, item) {
        if (i!=0) _ofs << "," ;
        item->Accept(*this) ;
    }
    _ofs << " when " ;
    FOREACH_ARRAY_ITEM(node.GetWhenChoices(), i, item) {
        if (i!=0) _ofs << "|" ;
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlBlockGenerics &node)
{
    if (!_bFileGood) return ; // file stream is not good

    unsigned i ;
    VhdlTreeNode *item ;
    if (node.GetGenericClause()) {
        _ofs << PrintLevel(_nLevel) << "generic ("  ;
        IncTabLevel(1) ;
        FOREACH_ARRAY_ITEM(node.GetGenericClause(),i,item) {
            item->Accept(*this) ;
            if (i!=node.GetGenericClause()->Size()-1) _ofs << "; " ;
        }
        DecTabLevel(1) ;
        _ofs << ") ;" << endl ;
    }
    if (node.GetGenericMapAspect()) {
        _ofs << PrintLevel(_nLevel) << "generic map (" ;
        IncTabLevel(1) ;
        FOREACH_ARRAY_ITEM(node.GetGenericMapAspect(), i, item) {
            if (i!=0) _ofs << "," ;
            if (item) {
                item->Accept(*this) ;
            } else {
                _ofs << "open" ;
            }
        }
        DecTabLevel(1) ;
        _ofs << ") ;" << endl;
    }
}

void VhdlPrettyPrintVisitor::Visit(VhdlBlockPorts &node)
{
    if (!_bFileGood) return ; // file stream is not good

    unsigned i ;
    VhdlTreeNode *item ;
    if (node.GetPortClause()) {
        _ofs << PrintLevel(_nLevel) << "port (" ;
        IncTabLevel(1) ;
        FOREACH_ARRAY_ITEM(node.GetPortClause(),i,item) {
            item->Accept(*this) ;
            if (i!=node.GetPortClause()->Size()-1) _ofs << "; " ;
        }
        DecTabLevel(1) ;
        _ofs << ") ; " << endl ;
    }
    if (node.GetPortMapAspect()) {
        _ofs << PrintLevel(_nLevel) << "port map (" ;
        IncTabLevel(1) ;
        FOREACH_ARRAY_ITEM(node.GetPortMapAspect(), i, item) {
            if (i!=0) _ofs << "," ;
            if (item) {
                item->Accept(*this) ;
            } else {
                _ofs << "open" ;
            }
        }
        DecTabLevel(1) ;
        _ofs << ") ;" << endl;
     }
}

void VhdlPrettyPrintVisitor::Visit(VhdlCaseStatementAlternative &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "when " ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetChoices(), i, item) {
        if (i!=0) _ofs << "|" ;
        item->Accept(*this) ;
    }
    _ofs << " => " << endl ;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, item) {
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlElsif &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) << "elsif " ;
    IncTabLevel(1) ;
    if (node.Condition()) {
        node.Condition()->Accept(*this) ;
    }
    _ofs << " then" << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, item) {
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlEntityClassEntry &node)
{
    if (!_bFileGood) return ; // file stream is not good

    if (node.GetEntityClass()) _ofs << VhdlNode::EntityClassName(node.GetEntityClass()) << " " ;
    if (node.GetBox()) _ofs << "<>" ;
}

/*-----------------------------------------------------------------*/
//        Visit Methods : Class details in VhdlName.h
/*-----------------------------------------------------------------*/

// VhdlName : Don't need to override

// VhdlDesignator : Don't need to override

// VhdlLiteral : Don't need to override

void VhdlPrettyPrintVisitor::Visit(VhdlAll &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "all" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlOthers &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "others" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlUnaffected &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "unaffected" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlInteger &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << (long)node.GetValue() ; // RD cast to long needed for VC++ (cannot handle 64bit scalar stream redirect)
}

void VhdlPrettyPrintVisitor::Visit(VhdlReal &node)
{
    if (!_bFileGood) return ; // file stream is not good

    char *image = Strings::dtoa(node.GetValue()) ;
    _ofs << image ;
    Strings::free(image) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlStringLiteral &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // String is stored in _name, with "'s

    // First walk _name and determine number of occurrences of "
    unsigned count = 0 ;
    const char *p = node.Name() ;
    while((p = strchr(p, '"'))) {  p++ ; count++ ; }
    count -= 2 ;    // Subtract opening and closing double quote

    if (count) {
        // Create a new temp buffer and write _name with double quotes preceded with double quotes
        p = node.Name() + 1;
        char *tmp = Strings::allocate(Strings::len(node.Name()) + count + 1) ;
        tmp[0] = '"' ;  // Write opening double quote
        unsigned i ;
        for(i=1; *p; i++) {
            tmp[i] = *p++ ;
            if (tmp[i] == '"' && *p) {
                tmp[++i] = '"' ; // Write extra double quote
            }
        }
        tmp[i] = '\0' ;
        _ofs << tmp ;
        Strings::free(tmp) ;
    } else {
        _ofs << node.Name() ;
    }
}

void VhdlPrettyPrintVisitor::Visit(VhdlBitStringLiteral &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "b" << node.Name() ; // String is stored in _name, with "'s
}

void VhdlPrettyPrintVisitor::Visit(VhdlCharacterLiteral &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << node.Name() ; // character is stored in _name, with ''s
}

void VhdlPrettyPrintVisitor::Visit(VhdlPhysicalLiteral &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.GetValueExpr()) {
        node.GetValueExpr()->Accept(*this) ;
    }
    _ofs << " " ;
    if (node.GetUnit()) {
        node.GetUnit()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlNull &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "null" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlIdRef &node)
{
    if (!_bFileGood) return ; // file stream is not good

    // Print the normal name reference
    _ofs << node.Name() ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlSelectedName &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.GetPrefix()) {
        node.GetPrefix()->Accept(*this) ;
    }
    _ofs << "." ;
    if (node.GetSuffix()) {
        node.GetSuffix()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlIndexedName &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.GetPrefix()) {
        node.GetPrefix()->Accept(*this) ;
    }
    _ofs << "(" ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetAssocList(), i, item) {
        if (i!=0) _ofs << "," ;
        if (item) {
            item->Accept(*this) ;
        } else {
            _ofs << "open" ;
        }
    }
    DecTabLevel(1) ;
    _ofs << ")" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlSignaturedName &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.GetName()) {
        node.GetName()->Accept(*this) ;
    }
    if (node.GetSignature()) {
        node.GetSignature()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlAttributeName &node)
{
    if (!_bFileGood) return ; // file stream is not good

    IncTabLevel(1) ;
    if (node.GetPrefix()) {
        node.GetPrefix()->Accept(*this) ;
    }
    _ofs << "'" ;
    if (node.GetDesignator()) {
        node.GetDesignator()->Accept(*this) ;
    }
    if (node.GetExpression()) {
        _ofs << "(" ;
        node.GetExpression()->Accept(*this) ;
        _ofs << ")" ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlOpen &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "open" ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlEntityAspect &node)
{
    if (!_bFileGood) return ; // file stream is not good

    if (node.GetEntityClass()) _ofs << VhdlNode::EntityClassName(node.GetEntityClass()) << " " ;
    IncTabLevel(1) ;
    if (node.GetName()) {
        node.GetName()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlInstantiatedUnit &node)
{
    if (!_bFileGood) return ; // file stream is not good

    if (node.GetEntityClass()) _ofs << VhdlNode::EntityClassName(node.GetEntityClass()) << " " ;
    IncTabLevel(1) ;
    if (node.GetName()) {
        node.GetName()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

/*-----------------------------------------------------------------*/
//        Visit Methods : Class details in VhdlSpecification.h
/*-----------------------------------------------------------------*/

// VhdlSpecification : Don't need to override

void VhdlPrettyPrintVisitor::Visit(VhdlProcedureSpec &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << "procedure " ;
    IncTabLevel(1) ;
    if (node.GetDesignator()) {
        node.GetDesignator()->Accept(*this) ;
    }
    unsigned i ;
    VhdlTreeNode *item ;
    if (node.GetFormalParamList()) {
        _ofs << "(" ;
        FOREACH_ARRAY_ITEM(node.GetFormalParamList(), i, item) {
            item->Accept(*this) ;
            if (i!=node.GetFormalParamList()->Size()-1) _ofs << "; " ;
        }
        _ofs << ")" ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlFunctionSpec &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    IncTabLevel(1) ;
    if (node.IsPure()) _ofs << "pure " ;
    if (node.IsImpure()) _ofs << "impure " ;
    _ofs << "function " ;
    if (node.GetDesignator()) {
        node.GetDesignator()->Accept(*this) ;
    }
    unsigned i ;
    VhdlTreeNode *item ;
    if (node.GetFormalParamList()) {
        _ofs << "(" ;
        FOREACH_ARRAY_ITEM(node.GetFormalParamList(), i, item) {
            item->Accept(*this) ;
            if (i!=node.GetFormalParamList()->Size()-1) _ofs << "; " ;
        }
        _ofs << ")" ;
    }
    _ofs << " return " ;
    if (node.GetReturnType()) {
        node.GetReturnType()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlComponentSpec &node)
{
    if (!_bFileGood) return ; // file stream is not good

    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetIds(), i, item) {
        if (i!=0) _ofs << ", " ;
        item->Accept(*this) ;
    }
    _ofs << ": " ;
    if (node.GetName()) {
        node.GetName()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlGuardedSignalSpec &node)
{
    if (!_bFileGood) return ; // file stream is not good

    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetSignalList(), i, item) {
        if (i!=0) _ofs << ", " ;
        item->Accept(*this) ;
    }
    _ofs << ": " ;
    if (node.GetTypeMark()) {
        node.GetTypeMark()->Accept(*this) ;
    }
    DecTabLevel(1) ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlEntitySpec &node)
{
    if (!_bFileGood) return ; // file stream is not good

    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetEntityNameList(), i, item) {
        if (i!=0) _ofs << ", " ;
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ": " << VhdlNode::EntityClassName(node.GetEntityClass()) ;
}

/*-----------------------------------------------------------------*/
//        Visit Methods : Class details in VhdlStatement.h
/*-----------------------------------------------------------------*/

// VhdlStatement : Don't need to override

void VhdlPrettyPrintVisitor::Visit(VhdlNullStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    _ofs << "null ;" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlReturnStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    _ofs << "return " ;
    IncTabLevel(1) ;
    if (node.GetExpression()){
        node.GetExpression()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlExitStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    _ofs << "exit " ;
    IncTabLevel(1) ;
    if (node.GetTarget()) {
        node.GetTarget()->Accept(*this) ;
    }
    if (node.GetCondition()) {
        if (node.GetTarget())   _ofs << " when " ;
        else           _ofs << "when " ;
        node.GetCondition()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlNextStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    IncTabLevel(1) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    _ofs << "next " ;
    if (node.GetTarget()) {
        node.GetTarget()->Accept(*this) ;
    }
    if (node.GetCondition()) {
        if (node.GetTarget())    _ofs << " when " ;
        else            _ofs << "when " ;
        node.GetCondition()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlLoopStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    IncTabLevel(1) ;
    if (node.GetIterScheme()) {
        node.GetIterScheme()->Accept(*this) ;
    }
    _ofs << " loop " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, item) {
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "end loop; " << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlCaseStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    _ofs << "case " ;
    IncTabLevel(1) ;
    if (node.GetExpression()) {
        node.GetExpression()->Accept(*this) ;
    }
    _ofs << " is " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetAlternatives(), i, item) {
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "end case ; " << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlIfStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    _ofs << "if " ;
    IncTabLevel(1) ;
    if (node.GetIfCondition()) {
        node.GetIfCondition()->Accept(*this) ;
    }
    _ofs << " then " << endl ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(node.GetIfStatements(), i, elem) {
        elem->Accept(*this) ;
    }
    DecTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetElsifList(), i, elem) {
        elem->Accept(*this) ;
    }
    if (node.GetElseStatments()) {
        _ofs << PrintLevel(_nLevel) << "else " << endl ;
        IncTabLevel(1) ;
        FOREACH_ARRAY_ITEM(node.GetElseStatments(), i, elem) {
            elem->Accept(*this) ;
        }
        DecTabLevel(1) ;
    }
    _ofs << PrintLevel(_nLevel) << "end if;" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlVariableAssignmentStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    IncTabLevel(1) ;
    if (node.GetTarget()) node.GetTarget()->Accept(*this) ;
    _ofs << " := " ;
    if (node.GetValue()) node.GetValue()->Accept(*this) ;
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlSignalAssignmentStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    IncTabLevel(1) ;
    if (node.GetTarget()) node.GetTarget()->Accept(*this) ;
    _ofs << " <= " ;
    if (node.GetDelayMechanism()) node.GetDelayMechanism()->Accept(*this) ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(node.GetWaveform(), i, elem) {
        if (i!=0) _ofs << "," ;
        elem->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlWaitStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    _ofs << "wait " ;
    unsigned i ;
    VhdlTreeNode *item ;
    IncTabLevel(1) ;
    if (node.GetSensitivityClause()) {
        _ofs << "on " ;
        FOREACH_ARRAY_ITEM(node.GetSensitivityClause(), i, item) {
            if (i!=0) _ofs << "," ;
            item->Accept(*this) ;
        }
    }
    if (node.UntilClause()) {
        _ofs << " until " ;
        node.UntilClause()->Accept(*this) ;
    }
    if (node.GetTimeoutClause()) {
        _ofs << " for " ;
        node.GetTimeoutClause()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlReportStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    _ofs << "report " ;
    IncTabLevel(1) ;
    if (node.GetReport()) node.GetReport()->Accept(*this) ;
    if (node.GetSeverity()) {
        _ofs << " severity " ;
        IncTabLevel(1) ;
        node.GetSeverity()->Accept(*this) ;
        DecTabLevel(1) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlAssertionStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    _ofs << "assert " ;
    IncTabLevel(1) ;
    if (node.GetCondition()) node.GetCondition()->Accept(*this) ;
    if (node.GetReport()) {
        _ofs << " report " ;
        node.GetReport()->Accept(*this) ;
    }
    if (node.GetSeverity()) {
        _ofs << " severity " ;
        node.GetSeverity()->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlProcessStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    _ofs << "process " ;
    unsigned i ;
    VhdlTreeNode *elem ;
    IncTabLevel(1) ;
    if (node.GetSensitivityList()) {
        _ofs << "(" ;
        FOREACH_ARRAY_ITEM(node.GetSensitivityList(), i, elem) {
            if (i!=0) _ofs << ", " ;
            elem->Accept(*this) ;
        }
        _ofs << ")" ;
    }
    _ofs << endl ;
    FOREACH_ARRAY_ITEM(node.GetDeclPart(), i, elem) elem->Accept(*this) ;
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "begin " << endl ;
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetStatementPart(), i, elem) elem->Accept(*this) ;
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "end process ; " << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlBlockStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    _ofs << "block " ;
    IncTabLevel(1) ;
    if (node.GetGuard()) {
        _ofs << "(" ;
        node.GetGuard()->Accept(*this) ;
        _ofs << ")" ;
    }
    _ofs << endl ;
    if (node.GetGenerics()) node.GetGenerics()->Accept(*this) ;
    if (node.GetPorts()) node.GetPorts()->Accept(*this) ;

    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetDeclPart(), i, item) item->Accept(*this) ;
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "begin" << endl ;

    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, item) item->Accept(*this) ;
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "end block ; " << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlGenerateStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;
    IncTabLevel(1) ;
    node.GetScheme()->Accept(*this) ;
    DecTabLevel(1) ;
    _ofs << " generate " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    if (node.GetDeclPart()) {
        IncTabLevel(1) ;
        FOREACH_ARRAY_ITEM(node.GetDeclPart(), i, item) item->Accept(*this) ;
        _ofs << PrintLevel(_nLevel) << "begin" << endl ;
        DecTabLevel(1) ;
    }
    IncTabLevel(1) ;
    FOREACH_ARRAY_ITEM(node.GetStatementPart(), i, item) item->Accept(*this) ;
    DecTabLevel(1) ;
    _ofs << PrintLevel(_nLevel) << "end generate ; " << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlComponentInstantiationStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;

    IncTabLevel(1) ;
    if (node.GetInstantiatedUnitNameNode()) node.GetInstantiatedUnitNameNode()->Accept(*this) ;
    unsigned i ;
    VhdlTreeNode *item ;
    if (node.GetGenericMapAspect()) {
        _ofs << " generic map (" ;
        FOREACH_ARRAY_ITEM(node.GetGenericMapAspect(), i, item) {
            if (i!=0) _ofs << ", " ;
            if (item) {
                item->Accept(*this) ;
            } else {
                _ofs << "open" ;
            }
        }
        _ofs << ")" ;
    }
    if (node.GetPortMapAspect()) {
        _ofs << " port map (" ;
        FOREACH_ARRAY_ITEM(node.GetPortMapAspect(), i, item) {
            if (i!=0) _ofs << ", " ;
            if (item) {
                item->Accept(*this) ;
            } else {
                _ofs << "open" ;
            }
        }
        _ofs << ")" ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlProcedureCallStatement &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;
    IncTabLevel(1) ;
    if (node.GetCall()) node.GetCall()->Accept(*this);
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlConditionalSignalAssignment &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;
    IncTabLevel(1) ;
    if (node.GetTarget()) node.GetTarget()->Accept(*this) ;
    _ofs << " <= " ;
    if (node.GetOptions()) node.GetOptions()->Accept(*this) ;
    VhdlTreeNode *item ;
    unsigned i ;
    // Conditional waveforms are stacked from back
    FOREACH_ARRAY_ITEM_BACK(node.GetConditionalWaveforms(), i, item) {
        if (i!=node.GetConditionalWaveforms()->Size()-1) _ofs << " else " ;
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

void VhdlPrettyPrintVisitor::Visit(VhdlSelectedSignalAssignment &node)
{
    if (!_bFileGood) return ; // file stream is not good

    _ofs << PrintLevel(_nLevel) ;
    if (node.GetLabel()) _ofs << node.GetLabel()->Name() << " : " ;
    if (node.IsPostponed()) _ofs << "postponed " ;
    _ofs << "with " ;
    IncTabLevel(1) ;
    if (node.GetExpression()) node.GetExpression()->Accept(*this) ;
    _ofs << " select " ;
    if (node.GetTarget()) node.GetTarget()->Accept(*this) ;
    _ofs << " <= " ;
    if (node.GetOptions()) node.GetOptions()->Accept(*this) ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetSelectedWaveforms(), i, item) {
        if (i!=0) _ofs << "," << endl << PrintLevel(_nLevel) ;
        item->Accept(*this) ;
    }
    DecTabLevel(1) ;
    _ofs << ";" << endl ;
}

/*---------------------------------------------*/
