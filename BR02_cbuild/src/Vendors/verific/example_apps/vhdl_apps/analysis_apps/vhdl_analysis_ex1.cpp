/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Array.h"              // Make dynamic array class Array available
#include "Map.h"                // Make associated hash table class Map available

#include "Message.h"            // Make message handlers available, not used in this example

#include "vhdl_file.h"          // Make VHDL reader available

#include "VhdlUnits.h"          // Definitions of a VhdlLibrary and VhdlDesignUnits and their subclasses
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations constructs
#include "VhdlExpression.h"     // Definitions of all vhdl expresssion constructs
#include "VhdlSpecification.h"  // Definitions of all vhdl specification constructs
#include "VhdlScope.h"          // Symbol table of locally declared identifiers

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
********************************* VHDL EXAMPLE #1 ***************************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following example is for instructional purposes only.  The code demonstrates
    a brute-force way of traversing a parse-tree in search of particular node
    information.  In essence, it's a large switch statement based on a node's
    GetClassId().  GetClassId() is Verific's RTTI mechanism.  We implemented this method
    in place of using ANSI's C++ RTTI mechanism, because we felt their mechanism is too
    expensive in terms of performance.

    There are cleaner ways on how to traverse a tree, such as declaring a pure
    virtual function at either the VhdlTreeNode, and implementing
    the function for all tree nodes.  You can also implement a Visitor design
    pattern to get the same effect.

    This brute-force approach hopefully will give a first-timer a better
    overall picture on how things are constructed than the other two approaches,
    since all the code is right in front of them, and there are no other files
    or functions that need to be viewed initially that pertain to the traversing.

    "Vhdl_main_ex2.cpp" contains a second brute-force example, but some of the
    functionality there has been modularized to make it easier to read and follow.
    Please look at that example after going through this one.

    One last thing, I recommend running through this example in a debugger, since
    you'll be able to look at the state of the objects and see everything
    that is there.  The following example only half-heartedly goes through
    everything that it can do.  All the switch statements have been trimmed
    specifically for the example test file.

*****************************************************************************************/

// This main() function is a test program for the verilog analyzer only.  No
// elaboration is done here.

int main()
{
    // Create a vhdl reader object
    vhdl_file vhdlReader;   // Read the intro in vhdl_file.h to see the details of this object.

    // The following VHDL standard package ("standard.vhd") is to be read in:
    /*
        --
        --
        -- This is Package STANDARD as defined in the VHDL 1993 Language Reference Manual.
        -- It will be compiled into VHDL library 'std'
        --
        -- Version information: @(#)standard.vhd
        --

        package standard is
            type boolean is (false,true);
            type bit is ('0', '1');
            type character is (
                nul, soh, stx, etx, eot, enq, ack, bel,
                bs,  ht,  lf,  vt,  ff,  cr,  so,  si,
                dle, dc1, dc2, dc3, dc4, nak, syn, etb,
                can, em,  sub, esc, fsp, gsp, rsp, usp,

                ' ', '!', '"', '#', '$', '%', '&', ''',
                '(', ')', '*', '+', ',', '-', '.', '/',
                '0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', ':', ';', '<', '=', '>', '?',

                '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
                'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
                'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
                'X', 'Y', 'Z', '[', '\', ']', '^', '_',

                '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
                'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
                'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
                'x', 'y', 'z', '{', '|', '}', '~', del,

                c128, c129, c130, c131, c132, c133, c134, c135,
                c136, c137, c138, c139, c140, c141, c142, c143,
                c144, c145, c146, c147, c148, c149, c150, c151,
                c152, c153, c154, c155, c156, c157, c158, c159,

                -- the character code for 160 is there (NBSP),
                -- but prints as no char

                '�', '�', '�', '�', '�', '�', '�', '�',
                '�', '�', '�', '�', '�', '�', '�', '�',
                '�', '�', '�', '�', '�', '�', '�', '�',
                '�', '�', '�', '�', '�', '�', '�', '�',

                '�', '�', '�', '�', '�', '�', '�', '�',
                '�', '�', '�', '�', '�', '�', '�', '�',
                '�', '�', '�', '�', '�', '�', '�', '�',
                '�', '�', '�', '�', '�', '�', '�', '�',

                '�', '�', '�', '�', '�', '�', '�', '�',
                '�', '�', '�', '�', '�', '�', '�', '�',
                '�', '�', '�', '�', '�', '�', '�', '�',
                '�', '�', '�', '�', '�', '�', '�', '�' );

            type severity_level is (note, warning, error, failure);
            type integer is range -2147483647 to 2147483647;
            type real is range -1.0E308 to 1.0E308;
            type time is range -2147483647 to 2147483647
                units
                    fs;
                    ps = 1000 fs;
                    ns = 1000 ps;
                    us = 1000 ns;
                    ms = 1000 us;
                    sec = 1000 ms;
                    min = 60 sec;
                    hr = 60 min;
                end units;
            subtype delay_length is time range 0 fs to time'high;
            impure function now return delay_length;
            subtype natural is integer range 0 to integer'high;
            subtype positive is integer range 1 to integer'high;
            type string is array (positive range <>) of character;
            type bit_vector is array (natural range <>) of bit;
            type file_open_kind is (
                read_mode,
                write_mode,
                append_mode);
            type file_open_status is (
                open_ok,
                status_error,
                name_error,
                mode_error);
            attribute foreign : string;

        end standard;
    */
    //---------------------------------------

    // Now analyze the above example VHDL file "standard.vhd" (into the std library)
    if (!vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std")) {
        // An error occurred during analysis
        Message::Error(0, "The design was not analyzed in the library 'std'") ;
    }

    // First we need to get a handle to the "std" library
    VhdlLibrary *pLib = vhdlReader.GetLibrary("std") ;
    if (!pLib) {
        Message::Error(0, "The design was not analyzed in the library 'std'") ;
    }

    // Get a handle (by name) to the "standard" package that was just analyzed.  Libraries
    // have-a Map of primary units, where a VhdlPrimaryUnit can-be a VhdlEntityDecl, a
    // VhdlConfigurationDecl, or a VhdlPackageDecl.
    VhdlPrimaryUnit *pPrimaryUnit = pLib->GetPrimUnit("standard") ;
    if (!pPrimaryUnit) {
        Message::Error(0, "Primary unit 'standard' not found") ;
    }

    // Verify that this handle points to a VhdlPackageDecl (like it's supposed to)
    if (pPrimaryUnit->GetClassId() != ID_VHDLPACKAGEDECL) {
        Message::Error(0, "Class ID for the primary unit 'standard' not found") ;
    }

    VhdlPackageDecl *pPackageDecl = static_cast<VhdlPackageDecl*>(pPrimaryUnit) ;

    // Get a handle from this package declaration to the VhdlLibrary in which
    // it resides.  It should be the same as pLib!
    if (pPackageDecl->GetOwningLib() != pLib) {
        Message::Error(0, "Owning library not found for the package") ;
    }

    // All primary units have a map (char*->VhdlSecondaryUnits) of their
    // secondary units.  A VhdlSecondaryUnit can-be either a VhdlArchitectureBody
    // or a VhdlPackageBody.
    Map *pSecUnits = pPackageDecl->AllSecondaryUnits() ;

    // Because the "standard" package only contains declarations, and not
    // implementations, there is no corresponding package body (VhdlSecondaryUnit).
    // Because of this, the foreach loop below is currently useless.  It is shown
    // just for instructional purpose only.
    MapIter mi ;
    char *pName ;
    VhdlSecondaryUnit *pSecUnit ;
    FOREACH_MAP_ITEM(pSecUnits, mi, &pName, &pSecUnit) {
        // For this testcase, this foreach loop won't be executed.
    }

    // Now lets loop over all declarations that occurred in the package
    unsigned i ;
    VhdlDeclaration *pDecl ;
    FOREACH_ARRAY_ITEM(pPackageDecl->GetDeclPart(), i, pDecl){
        switch(pDecl->GetClassId()){
        // ------------------------------------------------------------------------------------
        // The following are all the types of VhdlDeclarations :
        // ------------------------------------------------------------------------------------
        // VhdlUseClause           VhdlLibraryClause       VhdlInterfaceDecl
        // VhdlSubprogramDecl      VhdlSubprogramBody      VhdlSubtypeDecl
        // VhdlFullTypeDecl        VhdlIncompleteTypeDecl  VhdlConstantDecl
        // VhdlSignalDecl          VhdlVariableDecl        VhdlFileDecl
        // VhdlAliasDecl           VhdlComponentDecl       VhdlAttributeDecl
        // VhdlAttributeSpec       VhdlConfigurationSpec   VhdlDisconnectionSpec
        // VhdlGroupTemplateDecl   VhdlGroupDecl
        // ------------------------------------------------------------------------------------
        case ID_VHDLFULLTYPEDECL:{
        // ------- Look in "VhdlDeclaration.h" for class description of this object -------
            // Downcast VhdlDeclaration -> VhdlFullTypeDecl.  Avoid dynamic_cast overhead.
            VhdlFullTypeDecl *pFullTypeDecl = static_cast<VhdlFullTypeDecl*>(pDecl);
            // Get identifier that belongs to this type
            VhdlIdDef *pId = pFullTypeDecl->GetId() ;
            // Get type definition of this type
            VhdlTypeDef *pTypeDef = pFullTypeDecl->GetTypeDef() ;
        }
        break ;

        case ID_VHDLATTRIBUTEDECL:{
        // ------- Look in "VhdlDeclaration.h" for class description of this object -------
            // Downcast VhdlDeclaration -> VhdlAttributeDecl.  Avoid dynamic_cast overhead.
            VhdlAttributeDecl *pAttrDecl = static_cast<VhdlAttributeDecl*>(pDecl);
            // Get identifier that belongs to this attribute
            VhdlIdDef *pId = pAttrDecl->GetId() ;
            // Get type mark of this attribute decl
            VhdlName *pTypeMark = pAttrDecl->GetTypeMark() ;
        }
        break ;

        case ID_VHDLSUBPROGRAMDECL:{
        // ------- Look in "VhdlDeclaration.h" for class description of this object -------
            // Downcast VhdlDeclaration -> VhdlSubprogramDecl.  Avoid dynamic_cast overhead.
            VhdlSubprogramDecl *pSubProgDecl = static_cast<VhdlSubprogramDecl*>(pDecl);
            // Get subprogram specification
            VhdlSpecification *pSpec = pSubProgDecl->GetSubprogramSpec() ;
            if (pSpec) {
                switch (pSpec->GetClassId()) {
                // ------------------------------------------------------------------------------------
                // The following are all the types of VhdlSpecifications :
                // ------------------------------------------------------------------------------------
                // VhdlProcedureSpec       VhdlFunctionSpec    VhdlComponentSpec
                // VhdlGuardedSignalSpec   VhdlEntitySpec
                // ------------------------------------------------------------------------------------
                case ID_VHDLFUNCTIONSPEC:{
                    // Downcast VhdlSpecification -> VhdlFunctionSpec.  Avoid dynamic_cast overhead.
                    VhdlFunctionSpec *pFunctionSpec = static_cast<VhdlFunctionSpec*>(pSpec);
                    // Is this function pure?
                    if (pFunctionSpec->IsPure()) {
                        // ...
                    } else if (pFunctionSpec->IsImpure()) {
                        // ...
                    } else {
                        // ...
                    }
                    // Get designator
                    VhdlIdDef *pDesignator = pFunctionSpec->GetDesignator() ;
                    // Get formal parameter list (may be empty)
                    Array *paramList = pFunctionSpec->GetFormalParamList() ;
                    // Get return type
                    VhdlName *pReturnType = pFunctionSpec->GetReturnType() ;
                    // Get funtion's local scope
                    VhdlScope *pLocalScope = pFunctionSpec->LocalScope() ;
                    // VhdlScope is the scope class which describes the scope of a declaration.  This class contains maps
                    // which contain the scopes who depend on this scope, as well as the scopes which this scope
                    // depends on.  Identifiers of use clauses that pertain to this scope are stored here.
                    // The following VHDL objects have a scope (as defined by the VHDL LRM):
                    //      entity decl w/arch. body,           configuration decl,             subprogram decl w/body,
                    //      package decl w/body,                record type decl,               component decl,
                    //      block statement,                    process statement,              loop statement,
                    //      block configuration,                componenet configuration,       generate statement
                }
                break ;

                default:
                   // For this testcase, this should never get executed.
                    Message::Error(0, "Some problem occured in the case condition evaluation. Check if you are using custom package 'standard' for this application") ;
                }
            }
        }
        break ;

        case ID_VHDLSUBTYPEDECL:{
        // ------- Look in "VhdlDeclaration.h" for class description of this object -------
            // Downcast VhdlDeclaration -> VhdlSubtypeDecl.  Avoid dynamic_cast overhead.
            VhdlSubtypeDecl *pSubtypeDecl = static_cast<VhdlSubtypeDecl*>(pDecl);
            // Get identifier that belongs to this subtype
            VhdlIdDef *pId = pSubtypeDecl->GetId() ;
            // Get subtypeindication of this subtype
            VhdlSubtypeIndication *pSubtypeInd = pSubtypeDecl->GetSubtypeIndication() ;

            // Now determine what type of subtypeindication this is :
            switch(pSubtypeInd->GetClassId()){
            // ------------------------------------------------------------------------------------
            // The following are all the types of VhdlSubtypeIndications :
            // ------------------------------------------------------------------------------------
            // VhdlExplicitSubtypeIndication  VhdlName
            //
            // ... where the following are all the types of VhdlNames :
            //
            // VhdlDesignator      VhdlLiteral         VhdlSelectedName
            // VhdlIndexedName     VhdlSignaturedName  VhdlAttributeName
            // VhdlOpen            VhdlEntityAspect    VhdlInstantiatedUnit
            //
            // (Please make note that some of these classes have derived classes)
            // ------------------------------------------------------------------------------------
            case ID_VHDLEXPLICITSUBTYPEINDICATION:{
            // ------- Look in "VhdlDeclaration.h" for class description of this object -------
                // Downcast VhdlSubtypeIndication -> VhdlExplicitSubtypeIndication.  Avoid dynamic_cast overhead.
                VhdlExplicitSubtypeIndication *pExplicitSI = static_cast<VhdlExplicitSubtypeIndication*>(pSubtypeInd);
                // Get its resolution function (this may not exist)
                VhdlName *pResFunc = pExplicitSI->GetResolutionFunction() ;
                // Get its type mark
                VhdlName *pTypeMark = pExplicitSI->GetTypeMark() ;
                // Get its discrete range
                VhdlDiscreteRange *pRangeConstraint = pExplicitSI->GetRangeConstraint() ;
                if (pRangeConstraint && pRangeConstraint->GetClassId() == ID_VHDLRANGE) {
                    VhdlRange *pRange = static_cast<VhdlRange*>(pRangeConstraint) ;
                    // Get LHS
                    VhdlExpression *pLHS = pRange->GetLeftExpression() ;
                    // Get range direction
                    unsigned dir = pRange->GetDir() ;
                    // Get RHS
                    VhdlExpression *pRHS = pRange->GetRightExpression() ;
                }

                // Get its range type
                VhdlIdDef *pRangeType = pExplicitSI->GetRangeType() ;
            }
            break;

            default:
                // For this testcase, this should never get executed.
                Message::Error(0, "Some problem occured in the case condition evaluation. Check if you are using custom package 'standard' for this application") ;
            }
        }
        break ;

        // Please note that not all possible case statement
        default:
            // For this testcase, this should never get executed.
             Message::Error(0, "Some problem occured in the case condition evaluation. Check if you are using custom package 'standard' for this application") ;
        }
    }

    // We are done traversing the parse-tree for this testcase.  As you can see, things
    // can get messy with have imbedded switch statements.

    return 1 ;
}
