/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Array.h"              // Make dynamic array class Array available
#include "Map.h"                // Make associated hash table class Map available
#include "Strings.h"            // A string utility/wrapper class

#include "Message.h"            // Make message handlers available, not used in this example

#include "vhdl_file.h"          // Make VHDL reader available

#include "VhdlUnits.h"          // Definitions of a VhdlLibrary and VhdlDesignUnits and their subclasses
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations constructs
#include "VhdlExpression.h"     // Definitions of all vhdl expresssion constructs
#include "VhdlSpecification.h"  // Definitions of all vhdl specification constructs
#include "VhdlStatement.h"      // Definitions of all vhdl statement constructs
#include "VhdlName.h"           // Definitions of all vhdl name (expression) constructs
#include "VhdlMisc.h"           // Definitions of all extraneous vhdl constructs
#include "VhdlScope.h"          // Symbol table of locally declared identifiers

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Some global functions for this example
static void TraverseDecl(VhdlDeclaration *pDecl);
static void TraverseSpec(VhdlSpecification *pSpec);
static void TraverseStmt(VhdlStatement *pStmt);
static void TraverseExpr(VhdlExpression *pExpr);

/****************************************************************************************
********************************* VHDL EXAMPLE #2 ***************************************
*****************************************************************************************

                                NOTE TO THE READER:

    As a continuation from example #1, the following code demonstrates a cleaner
    method for traversing a parse-tree.  No longer is it a flat & ugly
    approach, as in example #1.  Five Traverse global static functions have
    been introduced to aid in doing the look-up on GetClassId().

    Once again, GetClassId() is Verific's RTTI mechanism.  We implemented this method
    in place of using ANSI's C++ RTTI mechanism, because we felt their mechanism is too
    expensive in terms of performance.

    Each of these functions is basically a switch statement.  Each function accepts
    a certain typed argment (VhdlDeclarations, VhdlSpecifications, VhdlStatements,
    and VhdlExpressions).  I could have made just a single large function that catches
    everything, but I felt it's more readable and understandable this way.

    Examples #3 and #4 will show better ways how to traverse a parse-tree using
    polymorphism and then by using a Visitor design pattern.

    As before, I recommend running through this example in a debugger, since
    you'll be able to look at the state of the objects and see everything
    that is there.  The following example only half-heartedly goes through
    everything that it can do.  All the switch statements have been trimmed
    specifically for the example test file.

*****************************************************************************************/

// This main() function is a test program for the verilog analyzer only.  No
// elaboration is done here.

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main()
{
    // Create a vhdl reader object
    vhdl_file vhdlReader;   // Read the intro in vhdl_file.h to see the details of this object.

    // The following design ("ex2_prep6.vhd") is to be read in:

    //-------------- "ex2_prep6.vhd" -------------
    /*
        -- PREP Benchmark 6, Sixteen Bit Accumulator
        -- PREP6 contains a sixteen bit accumulator

        -- Copyright (c) 1994-1996 Synplicity, Inc.
        -- You may distribute freely, as long as this header remains attached.

        library ieee;
        use ieee.std_logic_1164.all;
        use ieee.std_logic_unsigned.all;
        use ieee.std_logic_arith.all;

        entity prep6 is
            port(
                  clk, rst : in std_logic;
                  d : in std_logic_vector (15 downto 0);
                  q : out std_logic_vector (15 downto 0)
            );
        end prep6;

        architecture behave of prep6 is

            signal q_i: std_logic_vector (15 downto 0);

        begin
            -- register the adder output
            accum: process (rst, clk)
            begin
                if  rst = '1' then
                    q_i <= (others => '0');
                elsif  rising_edge(clk) then
                    q_i <= q_i + d;
                end if;
            end process accum;
            q <= q_i;
        end behave;
    */
    //---------------------------------------

    // First we need to analyze the VHDL packages "ex2_prep6.vhd" depends on.
    // They are located in the "../../vhdl_packages".
    vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");   // store in std library
    vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/std_1164.vhd", "ieee");  // store in ieee library
    vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/syn_arit.vhd", "ieee");  // store in ieee library
    vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/syn_unsi.vhd", "ieee");  // store in ieee library

    // Now analyze the above example VHDL file "ex2_prep6.vhd" (into the work library) :
    if (!vhdlReader.Analyze("ex2_prep6.vhd", "work")) {
        // An error occurred during analysis
        Message::Error(0, "The design was not analyzed in the library 'work'") ;
    }

    // First we need to get a handle to the "work" library
    VhdlLibrary *pLib = vhdlReader.GetLibrary("work") ;
    if (!pLib) {
        Message::Error(0, "The library 'work' not found") ;
    }

    // Get a handle (by name) to the "prep6" entity that was just analyzed
    VhdlPrimaryUnit *pPrimaryUnit = pLib->GetPrimUnit("prep6") ;
    if (!pPrimaryUnit) {
        Message::Error(0, "Primary unit 'prep6' not found in the library work") ;
    }

    // Now double-check to see that we have a VhdlEntityDecl
    if (pPrimaryUnit->GetClassId() != ID_VHDLENTITYDECL) {
        Message::Error(0, "Class ID for the primary unit 'prep6' not found") ;
    }

    VhdlEntityDecl *pEntityDecl = static_cast<VhdlEntityDecl*>(pPrimaryUnit) ;

    unsigned i ;

    // Traverse over entity's generic clause
    VhdlDeclaration *pDecl ;
    FOREACH_ARRAY_ITEM(pEntityDecl->GetGenericClause(), i, pDecl) {
        TraverseDecl(pDecl) ;
    }

    // Traverse over entity's port clause
    FOREACH_ARRAY_ITEM(pEntityDecl->GetPortClause(), i, pDecl) {
        TraverseDecl(pDecl) ;
    }

    // Traverse over entity's declarations
    FOREACH_ARRAY_ITEM(pEntityDecl->GetDeclPart(), i, pDecl) {
        TraverseDecl(pDecl) ;
    }

    // Traverse over entity's statements
    VhdlStatement *pStmt ;
    FOREACH_ARRAY_ITEM(pEntityDecl->GetStatementPart(), i, pStmt) {
        TraverseStmt(pStmt) ;
    }

    // Now traverse the secondary units (architecture body)
    MapIter mi ;
    char *pArchName ;
    VhdlSecondaryUnit *pSecondaryUnit ;
    FOREACH_MAP_ITEM(pEntityDecl->AllSecondaryUnits(), mi, &pArchName, &pSecondaryUnit) {
        // For this testcase, this these secondary units should be architecture bodies.
        if (pSecondaryUnit->GetClassId() != ID_VHDLARCHITECTUREBODY) {
            Message::Error(0, "Class ID for the secondary unit not found") ;
        }
        // Downcast VhdlSecondaryUnit -> VhdlArchitectureBody.  Avoid dynamic_cast overhead.
        VhdlArchitectureBody *pArch = static_cast<VhdlArchitectureBody*>(pSecondaryUnit) ;

        // Get identifier reference to entity's name
        VhdlIdRef *idref = pArch->GetEntityName() ;

        // Traverse over architectures's declarations
        FOREACH_ARRAY_ITEM(pArch->GetDeclPart(), i, pDecl) {
            TraverseDecl(pDecl) ;
        }

        // Traverse over architectures's statements
        FOREACH_ARRAY_ITEM(pArch->GetStatementPart(), i, pStmt) {
            TraverseStmt(pStmt) ;
        }
    }

    return 1;
}

/* ------------------------------------------------------- */

static void TraverseExpr(VhdlExpression *pExpr)
{
    if (!pExpr) return;

    switch(pExpr->GetClassId()){
    // ------------------------------------------------------------------------------------
    // The following are all the types of VhdlExpressions:
    // ------------------------------------------------------------------------------------
    //    VhdlSubtypeIndication       VhdlAssocElement        VhdlOperator
    //    VhdlAllocator               VhdlAggregate           VhdlQualifiedExpression
    //    VhdlElementAssoc            VhdlWaveformElement     VhdlPhysicalLiteral
    //    VhdlDesignator              VhdlLiteral             VhdlSelectedName
    //    VhdlIndexedName             VhdlSignaturedName      VhdlAttributeName
    //    VhdlOpen                    VhdlEntityAspect        VhdlInstantiatedUnit
    //    VhdlAll                     VhdlOthers              VhdlCharacterLiteral
    //    VhdlInteger                 VhdlReal                VhdlStringLiteral
    //    VhdlNull                    VhdlUnaffected          VhdlBitStringLiteral
    //    VhdlIdRef
    // ------------------------------------------------------------------------------------
    case ID_VHDLOPERATOR:{
    // ------- Look in "VhdlExpression.h" for class description of this object -------
            // Downcast VhdlExpression -> VhdlOperator.  Avoid dynamic_cast overhead.
            VhdlOperator *pOper = static_cast<VhdlOperator*>(pExpr);
            // Get operator type
            unsigned type = pOper->GetOperatorToken() ;
            // Traverse LHS
            TraverseExpr(pOper->GetLeftExpression()) ;
            // Traverse RHS
            TraverseExpr(pOper->GetRightExpression()) ;
            // Get operator identifier
            VhdlIdDef *id = pOper->GetOperator() ;
        }
        break;

    case ID_VHDLIDREF:{
    // ------- Look in "VhdlName.h" for class description of this object -------
            // Downcast VhdlExpression -> VhdlIdRef.  Avoid dynamic_cast overhead.
            VhdlIdRef *pIdRef = static_cast<VhdlIdRef*>(pExpr);
            // Get handle to actual identifier
            VhdlIdDef *pId = pIdRef->GetSingleId() ;
            // Get name of identifer
            const char *name = pIdRef->Name() ;
        }
        break;

    case ID_VHDLCHARACTERLITERAL:{
    // ------- Look in "VhdlName.h" for class description of this object -------
            // Downcast VhdlExpression -> VhdlCharacterLiteral.  Avoid dynamic_cast overhead.
            VhdlCharacterLiteral *pCharLit = static_cast<VhdlCharacterLiteral*>(pExpr);
            // Get handle to actual identifier
            VhdlIdDef *pId = pCharLit->GetSingleId() ;
            // Get name of identifer
            const char *name = pCharLit->Name() ;
        }
        break;

    case ID_VHDLAGGREGATE:{
    // ------- Look in "VhdlName.h" for class description of this object -------
            // Downcast VhdlExpression -> VhdlAggregate.  Avoid dynamic_cast overhead.
            VhdlAggregate *pAggregate = static_cast<VhdlAggregate*>(pExpr);
            // Loop over element association list
            unsigned i ;
            VhdlDiscreteRange *pDiscreteRange ; // NOTE: VhdlDiscreteRange is the parent
                                                // class of a VhdlExpression
            FOREACH_ARRAY_ITEM(pAggregate->GetElementAssocList(), i, pDiscreteRange) {
                // Do what you want here ... you could traverse it if you want ...
            }
            // Get aggregate type identifier
            VhdlIdDef *pId = pAggregate->GetAggregateType() ;
        }
        break;

    case ID_VHDLINDEXEDNAME:{
    // ------- Look in "VhdlName.h" for class description of this object -------
            // Downcast VhdlExpression -> VhdlIndexedName.  Avoid dynamic_cast overhead.
            VhdlIndexedName *pIndexedName = static_cast<VhdlIndexedName*>(pExpr);
            // Traverse prefix expression
            TraverseExpr(pIndexedName->GetPrefix()) ;
            // Loop over associatation list
            unsigned i ;
            VhdlDiscreteRange *pDiscreteRange ; // NOTE: VhdlDiscreteRange is the parent
                                                // class of a VhdlExpression
            FOREACH_ARRAY_ITEM(pIndexedName->GetAssocList(), i, pDiscreteRange) {
                // Do what you want here ... you could traverse it if you want ...
            }
            // Get prefix identifier
            VhdlIdDef *pId = pIndexedName->GetPrefixId() ;
            // Here are some informational methods we can call
            (unsigned) pIndexedName->IsArrayIndex() ;
            (unsigned) pIndexedName->IsArraySlice() ;
            (unsigned) pIndexedName->IsFunctionCall() ;
            (unsigned) pIndexedName->IsIndexConstraint() ;
            (unsigned) pIndexedName->IsTypeConversion() ;
        }
        break;

    // For this given test case, the following VhdlExpressions were not implemented:.
    case ID_VHDLSUBTYPEINDICATION:
    case ID_VHDLASSOCELEMENT:
    case ID_VHDLALLOCATOR:
    case ID_VHDLQUALIFIEDEXPRESSION:
    case ID_VHDLELEMENTASSOC:
    case ID_VHDLWAVEFORMELEMENT:
    case ID_VHDLDESIGNATOR:
    case ID_VHDLLITERAL:
    case ID_VHDLSELECTEDNAME:
    case ID_VHDLSIGNATUREDNAME:
    case ID_VHDLATTRIBUTENAME:
    case ID_VHDLOPEN:
    case ID_VHDLENTITYASPECT:
    case ID_VHDLINSTANTIATEDUNIT:
    case ID_VHDLALL:
    case ID_VHDLOTHERS:
    case ID_VHDLUNAFFECTED:
    case ID_VHDLINTEGER:
    case ID_VHDLREAL:
    case ID_VHDLSTRINGLITERAL:
    case ID_VHDLBITSTRINGLITERAL:
    case ID_VHDLPHYSICALLITERAL:
    case ID_VHDLNULL:
    default:
        // ERROR: For this testcase, this should never occur!
        Message::Error(0, "Problem occured during the evaluation of case statement, Check if you are using custom design 'ex2_prep2.vhd' for this application") ;
    }
}

/* ------------------------------------------------------- */
/* -------------- Global Function Definitions ------------ */
/* ------------------------------------------------------- */

static void TraverseDecl(VhdlDeclaration *pDecl)
{
    if (!pDecl) return;

    switch(pDecl->GetClassId()){
    // ------------------------------------------------------------------------------------
    // The following are all the types of VhdlDeclarations :
    // ------------------------------------------------------------------------------------
    // VhdlUseClause           VhdlLibraryClause       VhdlInterfaceDecl
    // VhdlSubprogramDecl      VhdlSubprogramBody      VhdlSubtypeDecl
    // VhdlFullTypeDecl        VhdlIncompleteTypeDecl  VhdlConstantDecl
    // VhdlSignalDecl          VhdlVariableDecl        VhdlFileDecl
    // VhdlAliasDecl           VhdlComponentDecl       VhdlAttributeDecl
    // VhdlAttributeSpec       VhdlConfigurationSpec   VhdlDisconnectionSpec
    // VhdlGroupTemplateDecl   VhdlGroupDecl
    // ------------------------------------------------------------------------------------
    case ID_VHDLINTERFACEDECL:{
    // ------- Look in "VhdlDeclaration.h" for class description of this object -------
        // Downcast VhdlDeclaration -> VhdlInterfaceDecl.  Avoid dynamic_cast overhead.
        VhdlInterfaceDecl *pIntDecl = static_cast<VhdlInterfaceDecl*>(pDecl);
        // Get 'type' of interface
        unsigned nInterfaceKind = pIntDecl->GetInterfaceKind() ;
        // Get mode
        unsigned mode = pIntDecl->GetMode() ;
        // Get signal kind
        unsigned kind = pIntDecl->GetSignalKind() ;
        // Get array of identifiers
        unsigned i ;
        VhdlIdDef *pId ;
        FOREACH_ARRAY_ITEM(pIntDecl->GetIds(), i, pId) {
            // Do something here ... maybe traverse the id ...
        }
        // Get subtype indication
        VhdlSubtypeIndication *si = pIntDecl->GetSubtypeIndication() ;
        // Traverse initial assignment
        TraverseExpr(pIntDecl->GetInitAssign()) ;
    }
    break ;

    case ID_VHDLSIGNALDECL:{
    // ------- Look in "VhdlDeclaration.h" for class description of this object -------
        // Downcast VhdlDeclaration -> VhdlSignalDecl.  Avoid dynamic_cast overhead.
        VhdlSignalDecl *pSignalDecl = static_cast<VhdlSignalDecl*>(pDecl);
        // Get array of identifiers
        unsigned i ;
        VhdlIdDef *pId ;
        FOREACH_ARRAY_ITEM(pSignalDecl->GetIds(), i, pId) {
            // Do something here ... maybe traverse the id ...
        }
        // Get subtype indication
        VhdlSubtypeIndication *si = pSignalDecl->GetSubtypeIndication() ;
        // Get kind of signal
        unsigned signalKind = pSignalDecl->GetSignalKind() ;
        // Traverse initial assignment
        TraverseExpr(pSignalDecl->GetInitAssign()) ;
    }
    break ;

    case ID_VHDLFULLTYPEDECL:{
    // ------- Look in "VhdlDeclaration.h" for class description of this object -------
        // Downcast VhdlDeclaration -> VhdlFullTypeDecl.  Avoid dynamic_cast overhead.
        VhdlFullTypeDecl *pFullTypeDecl = static_cast<VhdlFullTypeDecl*>(pDecl);
        // Get identifier that belongs to this type
        VhdlIdDef *pId = pFullTypeDecl->GetId() ;
        // Get type definition of this type
        VhdlTypeDef *pTypeDef = pFullTypeDecl->GetTypeDef() ;
    }
    break ;

    case ID_VHDLATTRIBUTEDECL:{
    // ------- Look in "VhdlDeclaration.h" for class description of this object -------
        // Downcast VhdlDeclaration -> VhdlAttributeDecl.  Avoid dynamic_cast overhead.
        VhdlAttributeDecl *pAttrDecl = static_cast<VhdlAttributeDecl*>(pDecl);
        // Get identifier that belongs to this attribute
        VhdlIdDef *pId = pAttrDecl->GetId() ;
        // Get type mark of this attribute decl
        VhdlName *pTypeMark = pAttrDecl->GetTypeMark() ;
    }
    break ;

    case ID_VHDLSUBPROGRAMDECL:{
    // ------- Look in "VhdlDeclaration.h" for class description of this object -------
        // Downcast VhdlDeclaration -> VhdlSubprogramDecl.  Avoid dynamic_cast overhead.
        VhdlSubprogramDecl *pSubProgDecl = static_cast<VhdlSubprogramDecl*>(pDecl);
        // Get subprogram specification
        VhdlSpecification *pSpec = pSubProgDecl->GetSubprogramSpec() ;
        if (pSpec) {
        }
    }
    break ;

    case ID_VHDLSUBTYPEDECL:{
    // ------- Look in "VhdlDeclaration.h" for class description of this object -------
        // Downcast VhdlDeclaration -> VhdlSubtypeDecl.  Avoid dynamic_cast overhead.
        VhdlSubtypeDecl *pSubtypeDecl = static_cast<VhdlSubtypeDecl*>(pDecl);
        // Get identifier that belongs to this subtype
        VhdlIdDef *pId = pSubtypeDecl->GetId() ;
        // Get subtypeindication of this subtype
        VhdlSubtypeIndication *pSubtypeInd = pSubtypeDecl->GetSubtypeIndication() ;

        // Now determine what type of subtypeindication this is :
        switch(pSubtypeInd->GetClassId()){
        // ------------------------------------------------------------------------------------
        // The following are all the types of VhdlSubtypeIndications :
        // ------------------------------------------------------------------------------------
        // VhdlExplicitSubtypeIndication  VhdlName
        //
        // ... where the following are all the types of VhdlNames :
        //
        // VhdlDesignator      VhdlLiteral         VhdlSelectedName
        // VhdlIndexedName     VhdlSignaturedName  VhdlAttributeName
        // VhdlOpen            VhdlEntityAspect    VhdlInstantiatedUnit
        //
        // (Please make note that some of these classes have derived classes)
        // ------------------------------------------------------------------------------------
        case ID_VHDLEXPLICITSUBTYPEINDICATION:{
        // ------- Look in "VhdlDeclaration.h" for class description of this object -------
            // Downcast VhdlSubtypeIndication -> VhdlExplicitSubtypeIndication.  Avoid dynamic_cast overhead.
            VhdlExplicitSubtypeIndication *pExplicitSI = static_cast<VhdlExplicitSubtypeIndication*>(pSubtypeInd);
            // Get its resolution function (this may not exist)
            VhdlName *pResFunc = pExplicitSI->GetResolutionFunction() ;
            // Get its type mark
            VhdlName *pTypeMark = pExplicitSI->GetTypeMark() ;
            // Get its discrete range
            VhdlDiscreteRange *pRangeConstraint = pExplicitSI->GetRangeConstraint() ;
            if (pRangeConstraint && pRangeConstraint->GetClassId() == ID_VHDLRANGE) {
                VhdlRange *pRange = static_cast<VhdlRange*>(pRangeConstraint) ;
                // Get LHS
                VhdlExpression *pLHS = pRange->GetLeftExpression() ;
                // Get range direction
                unsigned dir = pRange->GetDir() ;
                // Get RHS
                VhdlExpression *pRHS = pRange->GetRightExpression() ;
            }

            // Get its range type
            VhdlIdDef *pRangeType = pExplicitSI->GetRangeType() ;
        }
        break;

        default:
            // For this testcase, this should never get executed.
            Message::Error(0, "Problem occured during the evaluation of case statement, Check if you are using custom design 'ex2_prep2.vhd' for this application") ;
        }
    }

    // For this given test case, the following VhdlDeclarations were not implemented:.
    case ID_VHDLUSECLAUSE:
    case ID_VHDLLIBRARYCLAUSE:
    case ID_VHDLINCOMPLETETYPEDECL:
    case ID_VHDLSUBPROGRAMBODY:
    case ID_VHDLCONSTANTDECL:
    case ID_VHDLVARIABLEDECL:
    case ID_VHDLFILEDECL:
    case ID_VHDLALIASDECL:
    case ID_VHDLCOMPONENTDECL:
    case ID_VHDLATTRIBUTESPEC:
    case ID_VHDLCONFIGURATIONSPEC:
    case ID_VHDLDISCONNECTIONSPEC:
    case ID_VHDLGROUPTEMPLATEDECL:
    case ID_VHDLGROUPDECL:
    default:
        // For this testcase, this should not get triggered!
        Message::Error(0, "Problem occured during the evaluation of case statement, Check if you are using custom design 'ex2_prep2.vhd' for this application") ;
    }
}

/* ------------------------------------------------------- */

static void TraverseStmt(VhdlStatement *pStmt)
{
    if (!pStmt) return;

    // ------------------------------------------------------------------------------------
    // The following are all the types of VhdlStatements:
    // ------------------------------------------------------------------------------------
    //    VhdlNullStatement       VhdlReturnStatement     VhdlProcedureCallStatement
    //    VhdlNextStatement       VhdlLoopStmt            VhdlConditionalSignalAssignment
    //    VhdlWaitStatement       VhdlIfStatement         VhdlVariableAssignmentStatement
    //    VhdlReportStatement     VhdlAssertionStatement  VhdlSignalAssignmentStatement
    //    VhdlProcessStatement    VhdlBlockStatement      VhdlGenerateStatement
    //    VhdlCaseStatement       VhdlExitStatement       VhdlComponentInstantiationStatement
    //    VhdlSelectedSignalAssignment
    // ------------------------------------------------------------------------------------
    switch(pStmt->GetClassId()){
    case ID_VHDLPROCESSSTATEMENT:{ // process statement
    // ------- Look in "VhdlStatement.h" for class description of this object -------
            // Downcast VhdlStatement -> VhdlProcessStatement.  Avoid dynamic_cast overhead.
            VhdlProcessStatement *pProcessStmt = static_cast<VhdlProcessStatement*>(pStmt);

            // Traverse sensitivity list
            unsigned i ;
            VhdlName *pName; // Note: VhdlName is a type of VhdlExpression
            FOREACH_ARRAY_ITEM(pProcessStmt->GetSensitivityList(), i, pName) {
                TraverseExpr(pName) ;
            }

            // Traverse declarations
            VhdlDeclaration *pDecl ;
            FOREACH_ARRAY_ITEM(pProcessStmt->GetDeclPart(), i, pDecl) {
                TraverseDecl(pDecl) ;
            }

            // Traverse statements
            VhdlStatement *pStmt;
            FOREACH_ARRAY_ITEM(pProcessStmt->GetStatementPart(), i, pStmt) {
                TraverseStmt(pStmt) ;
            }

            // Get process's scope
            VhdlScope *scope = pProcessStmt->LocalScope() ;
        }
        break;

    case ID_VHDLIFSTATEMENT:{ // if statement
    // ------- Look in "VhdlStatement.h" for class description of this object -------
            // Downcast VhdlStatement -> VhdlIfStatement.  Avoid dynamic_cast overhead.
            VhdlIfStatement *pIfStmt = static_cast<VhdlIfStatement*>(pStmt);

            // Traverse 'if' condition
            VhdlExpression *pIfCond = pIfStmt->GetIfCondition() ;
            if (pIfCond) TraverseExpr(pIfCond) ;

            // Traverse 'if' statements
            unsigned i ;
            VhdlStatement *pStmt2 ;
            FOREACH_ARRAY_ITEM(pIfStmt->GetIfStatements(), i, pStmt2) {
                TraverseStmt(pStmt2) ;
            }

            // Traverse 'elsif' statements
            VhdlElsif *pElsif ;
            FOREACH_ARRAY_ITEM(pIfStmt->GetElsifList(), i, pElsif) {
                if (!pElsif) continue ;
                // Traverse condition
                TraverseExpr(pElsif->Condition()) ;
                // Traverse statements
                FOREACH_ARRAY_ITEM(pElsif->GetStatements(), i, pStmt2) {
                    TraverseStmt(pStmt2) ;
                }
            }

            // Traverse 'else' statements
            FOREACH_ARRAY_ITEM(pIfStmt->GetElseStatments(), i, pStmt2) {
                TraverseStmt(pStmt2) ;
            }
        }
        break;

    case ID_VHDLSIGNALASSIGNMENTSTATEMENT:{
    // ------- Look in "VhdlStatement.h" for class description of this object -------
            // Downcast VhdlStatement -> VhdlSignalAssignmentStatement.  Avoid dynamic_cast overhead.
            VhdlSignalAssignmentStatement *pAssign = static_cast<VhdlSignalAssignmentStatement*>(pStmt);
            // Traverse target expression
            TraverseExpr(pAssign->GetTarget()) ;
            // Get delay mechanism
            VhdlDelayMechanism *pDelay = pAssign->GetDelayMechanism() ;
            // Traverse waveform expression
            VhdlExpression *pWaveform ;
            unsigned i ;
            FOREACH_ARRAY_ITEM(pAssign->GetWaveform(), i, pWaveform) {
                TraverseExpr(pWaveform) ;
            }
        }
        break;

    case ID_VHDLCONDITIONALSIGNALASSIGNMENT:{
    // ------- Look in "VhdlStatement.h" for class description of this object -------
            // Downcast VhdlStatement -> VhdlConditionalSignalAssignment.  Avoid dynamic_cast overhead.
            VhdlConditionalSignalAssignment *pCondSigAssign = static_cast<VhdlConditionalSignalAssignment*>(pStmt);
            // Traverse target expression
            TraverseExpr(pCondSigAssign->GetTarget()) ;
            // Get options
            VhdlOptions *pOptions = pCondSigAssign->GetOptions() ;
            // Get conditional waveform expressions
            VhdlConditionalWaveform *pWaveform ; // NOTE : this is NOT a VhdlExpression!
            unsigned i ;
            FOREACH_ARRAY_ITEM(pCondSigAssign->GetConditionalWaveforms(), i, pWaveform) {
                // do what you want here
            }
            // Get guard identifier
            VhdlIdDef *pId = pCondSigAssign->GetGuardId() ;
        }
        break;

    // For this given test case, the following VhdlStatements were not implemented:.
    case ID_VHDLNULLSTATEMENT:
    case ID_VHDLRETURNSTATEMENT:
    case ID_VHDLEXITSTATEMENT:
    case ID_VHDLNEXTSTATEMENT:
    case ID_VHDLLOOPSTATEMENT:
    case ID_VHDLCASESTATEMENT:
    case ID_VHDLVARIABLEASSIGNMENTSTATEMENT:
    case ID_VHDLWAITSTATEMENT:
    case ID_VHDLREPORTSTATEMENT:
    case ID_VHDLASSERTIONSTATEMENT:
    case ID_VHDLBLOCKSTATEMENT:
    case ID_VHDLGENERATESTATEMENT:
    case ID_VHDLCOMPONENTINSTANTIATIONSTATEMENT:
    case ID_VHDLPROCEDURECALLSTATEMENT:
    case ID_VHDLSELECTEDSIGNALASSIGNMENT:

    default:
        // For this testcase, this should never get executed.
        Message::Error(0, "Problem occured during the evaluation of case statement, Check if you are using custom design 'ex2_prep2.vhd' for this application") ;
    }
}

/* ------------------------------------------------------- */

static void TraverseSpec(VhdlSpecification *pSpec)
{
    if (!pSpec) return;

    switch (pSpec->GetClassId()) {
    // ------------------------------------------------------------------------------------
    // The following are all the types of VhdlSpecifications :
    // ------------------------------------------------------------------------------------
    // VhdlProcedureSpec       VhdlFunctionSpec    VhdlComponentSpec
    // VhdlGuardedSignalSpec   VhdlEntitySpec
    // ------------------------------------------------------------------------------------
    case ID_VHDLFUNCTIONSPEC:{
        // Downcast VhdlSpecification -> VhdlFunctionSpec.  Avoid dynamic_cast overhead.
        VhdlFunctionSpec *pFunctionSpec = static_cast<VhdlFunctionSpec*>(pSpec);
        // Is this function pure?
        if (pFunctionSpec->IsPure()) {
            // ...
        } else if (pFunctionSpec->IsImpure()) {
            // ...
        } else {
            // ...
        }
        // Get designator
        VhdlIdDef *pDesignator = pFunctionSpec->GetDesignator() ;
        // Get formal parameter list (may be empty)
        Array *paramList = pFunctionSpec->GetFormalParamList() ;
        // Get return type
        VhdlName *pReturnType = pFunctionSpec->GetReturnType() ;
        // Get funtion's local scope
        VhdlScope *pLocalScope = pFunctionSpec->LocalScope() ;
        // VhdlScope is the scope class which describes the scope of a declaration.  This class contains maps
        // which contain the scopes who depend on this scope, as well as the scopes which this scope
        // depends on.  Identifiers of use clauses that pertain to this scope are stored here.
        // The following VHDL objects have a scope (as defined by the VHDL LRM):
        //      entity decl w/arch. body,           configuration decl,             subprogram decl w/body,
        //      package decl w/body,                record type decl,               component decl,
        //      block statement,                    process statement,              loop statement,
        //      block configuration,                componenet configuration,       generate statement
    }
    break ;

    // For this given test case, the following VhdlSpecifications were not implemented:.
    case ID_VHDLPROCEDURESPEC:
    case ID_VHDLCOMPONENTSPEC:
    case ID_VHDLGUARDEDSIGNALSPEC:
    case ID_VHDLENTITYSPEC:
    default:
        // For this testcase, this should never get executed.
        Message::Error(0, "Problem occured during the evaluation of case statement, Check if you are using custom design 'ex2_prep2.vhd' for this application") ;
    }
}

/* ------------------------------------------------------- */

