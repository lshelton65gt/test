/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Ex4Visitor.h"  // Make PrettyPrint visitor class available
#include "vhdl_file.h"   // Make VHDL reader available
#include "VhdlUnits.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
********************************* VHDL EXAMPLE #4 ***************************************
*****************************************************************************************

                                NOTE TO THE READER:

    As a continuation from example #3, the following code demonstrates a Visitor
    pattern approach.

    A Visitor class lets you define a new operation on a tree (or data structure)
    without changing the classes of the elements on which it operates (tree nodes).
    In essence, it lets you keep related operations together by defining them in
    one class.  To get this to work, the following must occur:

    1) Declare a abstract base Visitor class which contains a "void Visit(<type>&)"
       method for every <type> of element (VhdlTreeNode) the Visitor will traverse.
       This has already been done, and is located in "VhdlVisitor.h".  Please
       take a look at its skeleton.

    2) Declare a "void Accept(VhdlVisitor &)" method for every element (VhdlTreeNode) class.

    3) To create your particular Visitor class, derive from VhdlVisitor, and
       implement all of the Visit() methods accordingly.

    For this example, we have implemented a VhdlPrettyPrintVisitor class that has the
    same behavior as the PrettyPrint() methods.  One thing to note here is that the
    original PrettyPrint() methods used polymorphism.  With a visitor, since
    the visitor object is always the caller of the operation, polymorphism won't
    work.  A CallCorrectVisit() method has been introduced that calls the GetClassId()
    on the &ref argument, and then calls the correct Visit() method, casting
    appropriately.  This is our fake polymorphism technique.  It's not pretty, but
    it gets the job done with minimal overhead when a polymorphism-based algorithm is
    trying to be implemented.

    Once again, GetClassId() is Verific's RTTI mechanism.  We implemented this method
    in place of using ANSI's C++ RTTI mechanism, because we felt their mechanism is too
    expensive in terms of performance.

    Please take a look at Ex4Visitor.h/cpp for the details.  All pretty-
    print related methods and objects are contained within this visitor object,
    making it a nice self-contained object.

    Once again, I recommend running through this example in a debugger, since
    you'll be able to look at the state of the objects and see everything
    that is there.

*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main()
{
    // Create a vhdl reader object
    vhdl_file vhdlReader;   // Read the intro in vhdl_file.h to see the details of this object.

    // The following design ("ex4_prep2.vhd") is to be read in:

    //------------- "ex4_prep2.vhd" ------------
    /*
        -- PREP Benchmark 2, Timer/Counter
        -- PREP2 contains 8 bit registers, a mux, counter and comparator

        -- Copyright (c) 1994-1996 Synplicity, Inc.
        -- You may distribute freely, as long as this header remains attached.

        library ieee;
        use ieee.std_logic_1164.all;
        use ieee.std_logic_unsigned.all;
        use ieee.std_logic_arith.all;

        entity prep2 is
            port(
                  clk, rst, sel, ldcomp, ldpre : in std_logic;
                  data1, data2   : in std_logic_vector (7 downto 0);
                  data0   : out std_logic_vector (7 downto 0)
            );
        end prep2;

        architecture behave of prep2 is

            signal equal: std_logic;
            signal mux_output: std_logic_vector (7 downto 0);
            signal lowreg_output: std_logic_vector (7 downto 0);
            signal highreg_output: std_logic_vector (7 downto 0);
            signal data0_i: std_logic_vector (7 downto 0);

        begin

             equal <= '1' when data0_i = lowreg_output else
                         '0';

             mux_output <= highreg_output when sel = '0' else
                              data1 when sel = '1' else
                              (others => 'X');

            registers: process  (rst, clk)
            begin
                if  rst = '1' then
                    highreg_output <= (others => '0');
                    lowreg_output <= (others => '0');
                elsif  rising_edge(clk) then
                    if  ldpre = '1' then
                        highreg_output <= data2;
                    end if;
                    if  ldcomp = '1' then
                        lowreg_output <= data2;
                    end if;
                end if;
            end process registers;

            counter: process  (rst, clk)
            begin
                if  rst = '1' then
                    data0_i <= (others => '0');
                elsif rising_edge(clk) then
                    if  equal = '1' then
                        data0_i <= mux_output;
                    elsif  equal = '0' then
                        data0_i <= data0_i + "00000001";
                    end if;
                end if;
            end process counter;
            data0 <= data0_i;
        end behave;
    */
    //---------------------------------------

    // First we need to analyze the VHDL packages "ex1_prep6.vhd" depends on.
    // They are located in the "../../vhdl_packages".
    vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");   // store in std library
    vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/std_1164.vhd", "ieee");  // store in ieee library
    vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/syn_arit.vhd", "ieee");  // store in ieee library
    vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/syn_unsi.vhd", "ieee");  // store in ieee library

    // Now analyze the above example VHDL file "ex4_prep2.vhd" (into the work library) :
    vhdlReader.Analyze("ex4_prep2.vhd", "work");

    // First we need to get a handle to the "work" library
    VhdlLibrary *pLib = vhdlReader.GetLibrary("work") ;

    // Now we can get a handle (by name) to the "prep7" entity that was just analyzed
    VhdlPrimaryUnit *pPrimaryUnit = pLib->GetPrimUnit("prep2") ;

    // Now pretty print this module via a visitor object
    VhdlPrettyPrintVisitor ppv("pp_prep2.vhd");
    // Check to make sure the file stream is good
    if (!ppv.IsFileGood()) return 0 ;

    pPrimaryUnit->Accept(ppv);

    return 1;
}

/* ------------------------------------------------------- */

