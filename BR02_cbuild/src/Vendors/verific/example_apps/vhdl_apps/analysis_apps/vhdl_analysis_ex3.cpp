/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <iostream>         // cout
using std::cout ;

#include "vhdl_file.h"      // Make VHDL reader available
#include "VhdlUnits.h"      // Definitions of a VhdlLibrary and VhdlDesignUnits and their subclasses

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
********************************* VHDL EXAMPLE #3 ***************************************
*****************************************************************************************

                                NOTE TO THE READER:

    As a continuation from example #2, the following example demonstrates a way
    to pretty_print a parse-tree while traversing it.  This is done
    via polymorphism.  It is much cleaner than the methods implemented in
    examples #1 and #2.

    A pure virtual method "void PrettyPrint(...) const = 0 ;" was declared in
    the VhdlTreeNode class ("VhdlTreeNode.h").  This forces any new classes to
    implement their version of this method.

    All vhdl tree node pretty print code has been copied to this file for easy
    viewing, but you won't be able to step into the code during debug,
    since the libs you received do not contain debug information.

    Because polymorphism is being used, GetClassId() is no longer need.  Once again
    GetClassId() is Verific's RTTI mechanism.  We implemented this method in place
    of using ANSI's C++ RTTI mechanism, because we felt their mechanism is too
    expensive in terms of performance.

    Examples #4 will show better ways how to traverse a parse-tree using
    a Visitor design pattern.

    Unfortunately with this example, you won't be able to step through the debugger.

*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main()
{
    // Create a vhdl reader object
    vhdl_file vhdlReader;   // Read the intro in vhdl_file.h to see the details of this object.

    // The following design ("ex3_prep7.vhd") is to be read in:

    //------------- "ex3_prep7.vhd" ------------
    /*
        -- PREP Benchmark 7, Sixteen Bit Counter
        -- PREP7 contains a sixteen bit counter

        -- Copyright (c) 1994-1996 Synplicity, Inc.
        -- You may distribute freely, as long as this header remains attached.

        library ieee;
        use ieee.std_logic_1164.all;
        use ieee.std_logic_unsigned.all;
        use ieee.std_logic_arith.all;

        entity prep7 is
            port(
                  clk, rst, ld, ce : in std_logic;
                  d : in std_logic_vector (15 downto 0);
                  q : out std_logic_vector (15 downto 0)
            );
        end prep7;

        architecture behave of prep7 is

            signal q_i: std_logic_vector (15 downto 0);

        begin
            -- register the adder output
            accum: process (rst, clk)
            begin
                if  rst = '1' then
                    q_i <= (others => '0');
                elsif rising_edge(clk) then
                    if ld = '1' then
                        q_i <= d;
                    elsif ce = '1' then
                        q_i <= q_i + '1';
                    end if;
                end if;
            end process accum;
            q <= q_i;
        end behave;
    */
    //---------------------------------------

    // First we need to analyze the VHDL packages "ex1_prep6.vhd" depends on.
    // They are located in the "../../vhdl_packages".
    vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");   // store in std library
    vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/std_1164.vhd", "ieee");  // store in ieee library
    vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/syn_arit.vhd", "ieee");  // store in ieee library
    vhdlReader.Analyze("../../../vhdl_packages/ieee_1993/src/syn_unsi.vhd", "ieee");  // store in ieee library

    // Now analyze the above example VHDL file "ex3_prep7.vhd" (into the work library) :
    vhdlReader.Analyze("ex3_prep7.vhd", "work");

    // First we need to get a handle to the "work" library
    VhdlLibrary *pLib = vhdlReader.GetLibrary("work") ;

    // Now we can get a handle (by name) to the "prep7" entity that was just analyzed
    VhdlPrimaryUnit *pPrimaryUnit = pLib->GetPrimUnit("prep7") ;

    // Now pretty print this module
    pPrimaryUnit->PrettyPrint(cout, 0);

    return 1;
}

/* ------------------------------------------------------- */
/* --------------- Pretty Print Functionality ------------ */
/* ------------------------------------------------------- */

#if 0    /* The following code is for viewing only.  If you try to compile it in, you will
            receive "multiple definitions" link errors.
          */

/************************** VhdlConfiguration Pretty Print ********************************/

void VhdlBlockConfiguration::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "for " ;
    if (_block_spec) _block_spec->PrettyPrint(f,level+1) ;
    f << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_use_clause_list,i,item) item->PrettyPrint(f,level+1) ;
    FOREACH_ARRAY_ITEM(_configuration_item_list,i,item) item->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end for ;" << endl ;
}

void VhdlComponentConfiguration::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "for " ;
    if (_component_spec) _component_spec->PrettyPrint(f,level+1) ;
    f << " " ;
    if (_binding) {
        _binding->PrettyPrint(f,level+1) ;
        f << ";" ;
    }
    f << " " ;
    if (_block_configuration) _block_configuration->PrettyPrint(f,level+1) ;
    f << " end for ; " << endl ;
}

/************************** VhdlDeclaration.cpp Pretty Print ********************************/

void VhdlScalarTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    f << "range " ;
    if (_scalar_range) _scalar_range->PrettyPrint(f,level+1) ;
}

void VhdlArrayTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    f << "array (" ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_index_constraint,i,item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ") of " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
}

void VhdlRecordTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    f << "record " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_element_decl_list, i, item) item->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end record " ;
}

void VhdlAccessTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    f << "access " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
}

void VhdlFileTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    f << "file of " ;
    if (_file_type_mark) _file_type_mark->PrettyPrint(f,level+1) ;
}

void VhdlEnumerationTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    f << "(" ;
    unsigned i;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_enumeration_literal_list,i,item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ")" ;
}

void VhdlPhysicalTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    f << "range " ;
    if (_range_constraint) _range_constraint->PrettyPrint(f,level+1) ;
    f << " units " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_physical_unit_decl_list, i, item) item->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end units " ;
}

void VhdlElementDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " : " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

void VhdlPhysicalUnitDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << _id->Name() ;
    if (_physical_literal) {
        f << " = " ;
        _physical_literal->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlUseClause::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "use " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_selected_name_list,i,item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlLibraryClause::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "library " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlInterfaceDecl::PrettyPrint(ostream &f, unsigned level) const
{
    if (_interface_kind) {
        f << EntityClassName(_interface_kind) << " " ;
    }
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " : " ;
    if (_mode) f << EntityClassName(_mode) << " " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    if (_signal_kind) f << " " << EntityClassName(_signal_kind) ;
    if (_init_assign) {
        f << " := " ;
        _init_assign->PrettyPrint(f,level+1) ;
    }
}

void VhdlSubprogramDecl::PrettyPrint(ostream &f, unsigned level) const
{
    if (_subprogram_spec) _subprogram_spec->PrettyPrint(f,level) ;
    f << ";" << endl ;
}

void VhdlSubprogramBody::PrettyPrint(ostream &f, unsigned level) const
{
    if (_subprogram_spec) _subprogram_spec->PrettyPrint(f,level) ;
    f << " is " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_decl_part,i,item) item->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "begin" << endl ;
    FOREACH_ARRAY_ITEM(_statement_part,i,item) item->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end ;" << endl ;
}

void VhdlSubtypeDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "subtype " << _id->Name() << " is " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

void VhdlFullTypeDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "type " << _id->Name() << " is " ;
    if (_type_def) _type_def->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

void VhdlIncompleteTypeDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "type " << _id->Name() ;
    f << ";" << endl ;
}

void VhdlConstantDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "constant " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " : " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    if (_init_assign) {
        f << " := " ;
        _init_assign->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlSignalDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "signal " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " : " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    if (_signal_kind) {
        f << ((_signal_kind==VHDL_bus) ? " bus" : " register") ;
    }
    if (_init_assign) {
        f << " := " ;
        _init_assign->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlVariableDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << ((_shared) ? "shared " : "") << "variable " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " : " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    if (_init_assign) {
        f << " := " ;
        _init_assign->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlFileDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "file " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " : " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    if (_file_open_info) {
        f << " " ;
        _file_open_info->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlAliasDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "alias " << _designator->Name() ;
    if (_subtype_indication) {
        f << " : " ;
        _subtype_indication->PrettyPrint(f,level+1) ;
    }
    f << " is " ;
    if (_alias_target_name) _alias_target_name->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

void VhdlComponentDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "component " << _id->Name() << endl ;
    unsigned i ;
    VhdlTreeNode *elem ;
    if (_generic_clause) {
        f << PrintLevel(level+1) << "generic (" ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,elem) {
            f << endl << PrintLevel(level+2) ; // newline
            elem->PrettyPrint(f,level+2) ;
            if (i!=_generic_clause->Size()-1) f << "; " ;
        }
        f << ") ;" << endl ;
    }
    if (_port_clause) {
        f << PrintLevel(level+1) << "port (" ;
        FOREACH_ARRAY_ITEM(_port_clause,i,elem) {
            f << endl << PrintLevel(level+2) ; // newline
            elem->PrettyPrint(f,level+2) ;
            if (i!=_port_clause->Size()-1) f << "; " ;
        }
        f << ") ;" << endl ;
    }
    f << PrintLevel(level) << "end component;" << endl ;
}

void VhdlAttributeDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "attribute " << _id->Name() << " : " ;
    if (_type_mark) _type_mark->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

void VhdlAttributeSpec::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "attribute " ;
    if (_designator) _designator->PrettyPrint(f,level+1) ;
    f << " of " ;
    if (_entity_spec) _entity_spec->PrettyPrint(f,level+1) ;
    f << " is " ;
    if (_value) _value->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

void VhdlConfigurationSpec::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "for " ;
    if (_component_spec) {
        _component_spec->PrettyPrint(f,level+1) ;
        f << " " ;
    }
    if (_binding) _binding->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

void VhdlDisconnectionSpec::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "disconnect " ;
    if (_guarded_signal_spec) _guarded_signal_spec->PrettyPrint(f,level+1) ;
    f << " after " ;
    if (_after) _after->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

void VhdlGroupTemplateDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "group " << _id->Name() << " is (" ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_entity_class_entry_list, i, item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ") ;" << endl ;
}

void VhdlGroupDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "group " << _id->Name() << " : " ;
    if (_group_template_name) _group_template_name->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

/************************** VhdlExpression.cpp Pretty Print ********************************/

void VhdlExplicitSubtypeIndication::PrettyPrint(ostream &f, unsigned level) const
{
    if (_res_function) {
        _res_function->PrettyPrint(f,level+1) ;
        f << " " ;
    }
    if (_type_mark) {
        _type_mark->PrettyPrint(f,level+1) ;
        f << " " ;
    }
    if (_range_constraint) {
        f << "range " ;
        _range_constraint->PrettyPrint(f,level+1) ;
    }
}

void VhdlRange::PrettyPrint(ostream &f, unsigned level) const
{
    if (_left) _left->PrettyPrint(f,level+1) ;
    if (_dir) f << " " << ((_dir==VHDL_to) ? "to" : "downto") << " " ;
    if (_right) _right->PrettyPrint(f,level+1) ;
}

void VhdlBox::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << "<>" ;
}

void VhdlAssocElement::PrettyPrint(ostream &f, unsigned level) const
{
    if (_formal_part) {
        _formal_part->PrettyPrint(f,level+1) ;
        f << " => " ;
    }
    if (_actual_part) {
        _actual_part->PrettyPrint(f,level+1) ;
    }
}

void VhdlOperator::PrettyPrint(ostream &f, unsigned level) const
{
    f << "(" ; // Parenthesize the operator call
    if (!_right) {
        // Unary operator
        f << OperatorSimpleName(_oper) << " " ;
        if (_left) _left->PrettyPrint(f,level+1) ;
    } else {
        // Binary operator
        if (_left) _left->PrettyPrint(f,level+1) ;
        f << " " << OperatorSimpleName(_oper) << " " ;
        if (_right) _right->PrettyPrint(f,level+1) ;
    }
    f << ")" ;
}

void VhdlAllocator::PrettyPrint(ostream &f, unsigned level) const
{
    f << "new " ;
    if (_subtype) _subtype->PrettyPrint(f,level+1) ;
}

void VhdlAggregate::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *item ;
    f << "(" ;
    FOREACH_ARRAY_ITEM(_element_assoc_list,i,item) {
        if (i!=0) f << "," ;
        if (item) {
            item->PrettyPrint(f,level+1) ;
        } else {
            f << "open" ;
        }
    }
    f << ")" ;
}

void VhdlQualifiedExpression::PrettyPrint(ostream &f, unsigned level) const
{
    if (_prefix) _prefix->PrettyPrint(f,level+1) ;
    f << "'(" ;
    if (_aggregate) _aggregate->PrettyPrint(f,level+1) ;
    f << ")" ;
}

void VhdlElementAssoc::PrettyPrint(ostream &f, unsigned level) const
{
    if (_choices) {
        unsigned i ;
        VhdlTreeNode *item ;
        FOREACH_ARRAY_ITEM(_choices, i, item) {
            if (i!=0) f << "|" ;
            item->PrettyPrint(f,level+1) ;
        }
        f << " => " ;
    }
    if (_expr) _expr->PrettyPrint(f,level+1) ;
}

void VhdlWaveformElement::PrettyPrint(ostream &f, unsigned level) const
{
    if (_value) _value->PrettyPrint(f,level+1) ;
    if (_after) {
        f << " after " ;
        _after->PrettyPrint(f,level+1) ;
    }
}

/************************** VhdlIdDef.cpp Pretty Print ********************************/

void VhdlIdDef::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << OrigName() ;
}

/************************** VhdlMisc.cpp Pretty Print ********************************/

void VhdlSignature::PrettyPrint(ostream &f, unsigned level) const
{
    f << "[" ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_type_mark_list, i, item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    if (_return_type) {
        f << " return " ;
        _return_type->PrettyPrint(f,level+1) ;
    }
    f << "]" ;
}

void VhdlFileOpenInfo::PrettyPrint(ostream &f, unsigned level) const
{
    if (_file_open) {
        // VHDL 93 file open mode
        f << " open " ;
        _file_open->PrettyPrint(f,level+1) ;
    }

    f << " is " ;
    if (_open_mode) {
        // VHDL 87 file open mode
        f << EntityClassName(_open_mode) ;
        f << " " ;
    }
    if (_file_logical_name) _file_logical_name->PrettyPrint(f,level+1) ;
}

void VhdlBindingIndication::PrettyPrint(ostream &f, unsigned level) const
{
    if (_entity_aspect) {
        f << " use " ;
        _entity_aspect->PrettyPrint(f,level+1) ;
    }
    unsigned i ;
    VhdlTreeNode *item ;
    if (_generic_map_aspect) {
        f << " generic map (" ;
        FOREACH_ARRAY_ITEM(_generic_map_aspect, i, item) {
            if (i!=0) f << ", " ;
            if (item) {
                item->PrettyPrint(f,level+1) ;
            } else {
                f << "open" ;
            }
        }
        f << ")" ;
    }
    if (_port_map_aspect) {
        f << " port map (" ;
        FOREACH_ARRAY_ITEM(_port_map_aspect, i, item) {
            if (i!=0) f << ", " ;
            if (item) {
                item->PrettyPrint(f,level+1) ;
            } else {
                f << "open" ;
            }
        }
        f << ")" ;
    }
}

void VhdlWhileScheme::PrettyPrint(ostream &f, unsigned level) const
{
    if (_condition) {
        f << "while " ;
        _condition->PrettyPrint(f,level+1) ;
    }
}

void VhdlForScheme::PrettyPrint(ostream &f, unsigned level) const
{
    f << "for " ;
    if (_id) _id->PrettyPrint(f,level+1) ;
    f << " in " ;
    if (_range) _range->PrettyPrint(f,level+1) ;
}

void VhdlIfScheme::PrettyPrint(ostream &f, unsigned level) const
{
    f << "if " ;
    if (_condition) _condition->PrettyPrint(f,level+1) ;
}

void VhdlOptions::PrettyPrint(ostream &f, unsigned level) const
{
    if (_guarded) {
        f << "guarded " ;
    }
    if (_delay_mechanism) _delay_mechanism->PrettyPrint(f,level+1) ;
}

void VhdlTransport::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << "transport " ;
}

void VhdlInertialDelay::PrettyPrint(ostream &f, unsigned level) const
{
    if (_reject_expression) {
        f << "reject " ;
        _reject_expression->PrettyPrint(f,level+1) ;
    }
    f << " inertial " ;
}

void VhdlConditionalWaveform::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_waveform, i, item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    if (_when_condition) {
        f << " when " ;
        _when_condition->PrettyPrint(f,level+1) ;
    }
}

void VhdlSelectedWaveform::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_waveform, i, item) {
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " when " ;
    FOREACH_ARRAY_ITEM(_when_choices, i, item) {
        if (i!=0) f << "|" ;
        item->PrettyPrint(f,level+1) ;
    }
}

void VhdlBlockGenerics::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *item ;
    if (_generic_clause) {
        f << PrintLevel(level) << "generic ("  ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,item) {
            item->PrettyPrint(f,level+1) ;
            if (i!=_generic_clause->Size()-1) f << "; " ;
        }
        f << ") ;" << endl ;
    }
    if (_generic_map_aspect) {
        f << PrintLevel(level) << "generic map (" ;
        FOREACH_ARRAY_ITEM(_generic_map_aspect, i, item) {
            if (i!=0) f << "," ;
            if (item) {
                item->PrettyPrint(f,level+1) ;
            } else {
                f << "open" ;
            }
        }
        f << ") ;" << endl;
     }
}

void VhdlBlockPorts::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *item ;
    if (_port_clause) {
        f << PrintLevel(level) << "port (" ;
        FOREACH_ARRAY_ITEM(_port_clause,i,item) {
            item->PrettyPrint(f,level+1) ;
            if (i!=_port_clause->Size()-1) f << "; " ;
        }
        f << ") ; " << endl ;
    }
    if (_port_map_aspect) {
        f << PrintLevel(level) << "port map (" ;
        FOREACH_ARRAY_ITEM(_port_map_aspect, i, item) {
            if (i!=0) f << "," ;
            if (item) {
                item->PrettyPrint(f,level+1) ;
            } else {
                f << "open" ;
            }
        }
        f << ") ;" << endl;
     }
}

void VhdlCaseStatementAlternative::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "when " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_choices, i, item) {
        if (i!=0) f << "|" ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " => " << endl ;
    FOREACH_ARRAY_ITEM(_statements, i, item) item->PrettyPrint(f,level+1) ;
}

void VhdlElsif::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) << "elsif " ;
    if (_condition) _condition->PrettyPrint(f,level+1) ;
    f << " then" << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_statements, i, item) item->PrettyPrint(f,level+1) ;
}

void VhdlEntityClassEntry::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    if (_entity_class) f << EntityClassName(_entity_class) << " " ;
    if (_box) f << "<>" ;
}

/************************** VhdlName.cpp Pretty Print ********************************/

void VhdlAll::PrettyPrint(ostream &f, unsigned /*level*/) const         { f << "all" ; }
void VhdlOthers::PrettyPrint(ostream &f, unsigned /*level*/) const      { f << "others" ; }
void VhdlUnaffected::PrettyPrint(ostream &f, unsigned /*level*/) const  { f << "unaffected" ; }
void VhdlInteger::PrettyPrint(ostream &f, unsigned /*level*/) const     { f << _value ;
}
void VhdlReal::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    char *image = Strings::dtoa(_value) ;
    f << image ;
    Strings::free(image) ;
}

void VhdlStringLiteral::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    // String is stored in _name, with "'s

    // First walk _name and determine number of occurrences of "
    unsigned count = 0 ;
    char *p = _name ;
    while((p = strchr(p, '"'))) {  p++ ; count++ ; }
    count -= 2 ;    // Subtract opening and closing double quote

    if (count) {
        // Create a new temp buffer and write _name with double quotes preceded with double quotes
        p = _name + 1;
        char *tmp = Strings::allocate(Strings::len(_name) + count + 1) ;
        tmp[0] = '"' ;  // Write opening double quote
        unsigned i ;
        for(i=1; *p; i++) {
            tmp[i] = *p++ ;
            if (tmp[i] == '"' && *p) {
                tmp[++i] = '"' ; // Write extra double quote
            }
        }
        tmp[i] = '\0' ;
        f << tmp ;
        Strings::free(tmp) ;
    } else {
        f << _name ;
    }
}

void VhdlBitStringLiteral::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << "b" << _name ; // String is stored in _name, with "'s
}

void VhdlCharacterLiteral::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << _name ; // character is stored in _name, with ''s
}

void VhdlPhysicalLiteral::PrettyPrint(ostream &f, unsigned level) const
{
    if (_value) _value->PrettyPrint(f,level+1) ;
    f << " " ;
    if (_unit) _unit->PrettyPrint(f,level+1) ;
}

void VhdlNull::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << "null" ;
}

void VhdlIdRef::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    // Print the normal name reference
    f << _name ;
}

void VhdlSelectedName::PrettyPrint(ostream &f, unsigned level) const
{
    if (_prefix) _prefix->PrettyPrint(f,level+1) ;
    f << "." ;
    if (_suffix) _suffix->PrettyPrint(f,level+1) ;
}

void VhdlIndexedName::PrettyPrint(ostream &f, unsigned level) const
{
    if (_prefix) _prefix->PrettyPrint(f,level+1) ;
    f << "(" ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, item) {
        if (i!=0) f << "," ;
        if (item) {
            item->PrettyPrint(f,level+1) ;
        } else {
            f << "open" ;
        }
    }
    f << ")" ;
}

void VhdlSignaturedName::PrettyPrint(ostream &f, unsigned level) const
{
    if (_name) _name->PrettyPrint(f,level+1) ;
    if (_signature) _signature->PrettyPrint(f,level+1) ;
}

void VhdlAttributeName::PrettyPrint(ostream &f, unsigned level) const
{
    if (_prefix) _prefix->PrettyPrint(f,level+1) ;
    f << "'" ;
    if (_designator) _designator->PrettyPrint(f,level+1) ;
    if (_expression) {
        f << "(" ;
        _expression->PrettyPrint(f,level+1) ;
        f << ")" ;
    }
}

void VhdlOpen::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << "open" ;
}

void VhdlEntityAspect::PrettyPrint(ostream &f, unsigned level) const
{
    if (_entity_class) f << EntityClassName(_entity_class) << " " ;
    if (_name) _name->PrettyPrint(f,level+1) ;
}

void VhdlInstantiatedUnit::PrettyPrint(ostream &f, unsigned level) const
{
    if (_entity_class) f << EntityClassName(_entity_class) << " " ;
    if (_name) _name->PrettyPrint(f,level+1) ;
}

/************************** VhdlSpecification.cpp Pretty Print ********************************/

void VhdlProcedureSpec::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    f << "procedure " ;
    if (_designator) _designator->PrettyPrint(f,level+1) ;
    unsigned i ;
    VhdlTreeNode *item ;
    if (_formal_parameter_list) {
        f << "(" ;
        FOREACH_ARRAY_ITEM(_formal_parameter_list, i, item) {
            item->PrettyPrint(f,level+1) ;
            if (i!=_formal_parameter_list->Size()-1) f << "; " ;
        }
        f << ")" ;
    }
}

void VhdlFunctionSpec::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_pure_impure) f << EntityClassName(_pure_impure) << " " ;
    f << "function " ;
    if (_designator) _designator->PrettyPrint(f,level+1) ;
    unsigned i ;
    VhdlTreeNode *item ;
    if (_formal_parameter_list) {
        f << "(" ;
        FOREACH_ARRAY_ITEM(_formal_parameter_list, i, item) {
            item->PrettyPrint(f,level+1) ;
            if (i!=_formal_parameter_list->Size()-1) f << "; " ;
        }
        f << ")" ;
    }
    f << " return " ;
    if (_return_type) _return_type->PrettyPrint(f,level+1) ;
}

void VhdlComponentSpec::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list, i, item) {
        if (i!=0) f << ", " ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ": " ;
    if (_name) _name->PrettyPrint(f,level+1) ;
}

void VhdlGuardedSignalSpec::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_signal_list, i, item) {
        if (i!=0) f << ", " ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ": " ;
    if (_type_mark) _type_mark->PrettyPrint(f,level+1) ;
}

void VhdlEntitySpec::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_entity_name_list, i, item) {
        if (i!=0) f << ", " ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ": " << EntityClassName(_entity_class) ;
}

/************************** VhdlStatement.cpp Pretty Print ********************************/

void VhdlNullStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    f << "null ;" << endl ;
}

void VhdlReturnStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    f << "return " ;
    if (_expr) _expr->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

void VhdlExitStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    f << "exit " ;
    if (_target) _target->PrettyPrint(f,level+1) ;
    if (_condition) {
        if (_target)   f << " when " ;
        else           f << "when " ;
        _condition->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlNextStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    f << "next " ;
    if (_target) _target->PrettyPrint(f,level+1) ;
    if (_condition) {
        if (_target)    f << " when " ;
        else            f << "when " ;
        _condition->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlLoopStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    if (_iter_scheme) _iter_scheme->PrettyPrint(f,level+1) ;
    f << " loop " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_statements, i, item) item->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end loop; " << endl ;
}

void VhdlCaseStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    f << "case " ;
    if (_expr) _expr->PrettyPrint(f,level+1) ;
    f << " is " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_alternatives, i, item) item->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end case ; " << endl ;
}

void VhdlIfStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    f << "if " ;
    if (_if_cond) _if_cond->PrettyPrint(f,level+1) ;
    f << " then " << endl ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_if_statements, i, elem) elem->PrettyPrint(f,level+1) ;
    FOREACH_ARRAY_ITEM(_elsif_list, i, elem) elem->PrettyPrint(f,level) ;
    if (_else_statements) {
        f << PrintLevel(level) << "else " << endl ;
        FOREACH_ARRAY_ITEM(_else_statements, i, elem) elem->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end if;" << endl ;
}

void VhdlVariableAssignmentStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " := " ;
    if (_value) _value->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

void VhdlSignalAssignmentStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " <= " ;
    if (_delay_mechanism) _delay_mechanism->PrettyPrint(f,level+1) ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_waveform, i, elem) {
        if (i!=0) f << "," ;
        elem->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlWaitStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    f << "wait " ;
    unsigned i ;
    VhdlTreeNode *item ;
    if (_sensitivity_clause) {
        f << "on " ;
        FOREACH_ARRAY_ITEM(_sensitivity_clause, i, item) {
            if (i!=0) f << "," ;
            item->PrettyPrint(f,level+1) ;
        }
    }
    if (_condition_clause) {
        f << " until " ;
        _condition_clause->PrettyPrint(f,level+1) ;
    }
    if (_timeout_clause) {
        f << " for " ;
        _timeout_clause->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlReportStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    f << "report " ;
    if (_report) _report->PrettyPrint(f,level+1) ;
    if (_severity) {
        f << " severity " ;
        _severity->PrettyPrint(f,level+2) ;
    }
    f << ";" << endl ;
}

void VhdlAssertionStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    f << "assert " ;
    if (_condition) _condition->PrettyPrint(f,level+1) ;
    if (_report) {
        f << " report " ;
        _report->PrettyPrint(f,level+1) ;
    }
    if (_severity) {
        f << " severity " ;
        _severity->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlProcessStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    f << "process " ;
    unsigned i ;
    VhdlTreeNode *elem ;
    if (_sensitivity_list) {
        f << "(" ;
        FOREACH_ARRAY_ITEM(_sensitivity_list, i, elem) {
            if (i!=0) f << ", " ;
            elem->PrettyPrint(f,level+1) ;
        }
        f << ")" ;
    }
    f << endl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, elem) elem->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "begin " << endl ;
    FOREACH_ARRAY_ITEM(_statement_part, i, elem) elem->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end process ; " << endl ;
}

void VhdlBlockStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;

    f << "block " ;
    if (_guard) {
        f << "(" ;
        _guard->PrettyPrint(f,level+1) ;
        f << ")" ;
    }
    f << endl ;
    if (_generics) _generics->PrettyPrint(f,level+1) ;
    if (_ports) _ports->PrettyPrint(f,level+1) ;

    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_decl_part, i, item) item->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "begin" << endl ;

    FOREACH_ARRAY_ITEM(_statements, i, item) item->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end block ; " << endl ;
}

void VhdlGenerateStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;
    _scheme->PrettyPrint(f,level+1) ;
    f << " generate " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    if (_decl_part) {
        FOREACH_ARRAY_ITEM(_decl_part, i, item) item->PrettyPrint(f,level+1) ;
        f << PrintLevel(level) << "begin" << endl ;
    }
    FOREACH_ARRAY_ITEM(_statement_part, i, item) item->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end generate ; " << endl ;
}

void VhdlComponentInstantiationStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;
    if (_instantiated_unit) _instantiated_unit->PrettyPrint(f,level+1) ;
    unsigned i ;
    VhdlTreeNode *item ;
    if (_generic_map_aspect) {
        f << " generic map (" ;
        FOREACH_ARRAY_ITEM(_generic_map_aspect, i, item) {
            if (i!=0) f << ", " ;
            if (item) {
                item->PrettyPrint(f,level+1) ;
            } else {
                f << "open" ;
            }
        }
        f << ")" ;
    }
    if (_port_map_aspect) {
        f << " port map (" ;
        FOREACH_ARRAY_ITEM(_port_map_aspect, i, item) {
            if (i!=0) f << ", " ;
            if (item) {
                item->PrettyPrint(f,level+1) ;
            } else {
                f << "open" ;
            }
        }
        f << ")" ;
    }
    f << ";" << endl ;
}

void VhdlProcedureCallStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;
    if (_call) _call->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

void VhdlConditionalSignalAssignment::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;
    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " <= " ;
    if (_options) _options->PrettyPrint(f,level+1) ;
    VhdlTreeNode *item ;
    unsigned i ;
    // Conditional waveforms are stacked from back
    FOREACH_ARRAY_ITEM_BACK(_conditional_waveforms, i, item) {
        if (i!=_conditional_waveforms->Size()-1) f << " else " ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void VhdlSelectedSignalAssignment::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    if (_label) f << _label->Name() << " : " ;
    if (_postponed) f << "postponed " ;
    f << "with " ;
    if (_expr) _expr->PrettyPrint(f,level+1) ;
    f << " select " ;
    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " <= " ;
    if (_options) _options->PrettyPrint(f,level+1) ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_selected_waveforms, i, item) {
        if (i!=0) f << "," << endl << PrintLevel(level+1) ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

/************************** VhdlUnit.cpp Pretty Print ********************************/

void VhdlEntityDecl::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_context_clause,i,elem) elem->PrettyPrint(f,level) ;

    f << PrintLevel(level) << "entity " << _id->Name() << " is " << endl ;
    if (_generic_clause) {
        f << PrintLevel(level+1) << "generic (" ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,elem) {
            f << endl << PrintLevel(level+2) ; // newline
            elem->PrettyPrint(f,level+2) ;
            if (i!=_generic_clause->Size()-1) f << "; " ;
        }
        f << ") ;" << endl ;
    }
    if (_port_clause) {
        f << PrintLevel(level+1) << "port (" ;
        FOREACH_ARRAY_ITEM(_port_clause,i,elem) {
            f << endl << PrintLevel(level+2) ; // newline
            elem->PrettyPrint(f,level+2) ;
            if (i!=_port_clause->Size()-1) f << "; " ;
        }
        f << ") ;" << endl ;
    }
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) elem->PrettyPrint(f,level+1) ;
    if (_statement_part) {
        f << PrintLevel(level) << "begin" << endl ;
        FOREACH_ARRAY_ITEM(_statement_part,i,elem) elem->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end " << _id->Name() << " ;" << endl ;

    f << endl ;
    // Print all secondary units
    MapIter mi ;
    FOREACH_VHDL_SECONDARY_UNIT(this,mi,elem) {
        elem->PrettyPrint(f,level) ;
    }
}

void VhdlArchitectureBody::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_context_clause,i,elem) elem->PrettyPrint(f,level) ;

    f << PrintLevel(level) << "architecture " << _id->Name() << " of " ;
    if (_entity_name) _entity_name->PrettyPrint(f,level+1) ;
    f << " is " << endl ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) elem->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "begin" << endl ;
    FOREACH_ARRAY_ITEM(_statement_part,i,elem) elem->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end " << _id->Name() << " ;" << endl ;
    f << endl ;
}

void VhdlConfigurationDecl::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_context_clause,i,elem) elem->PrettyPrint(f,level) ;

    f << PrintLevel(level) << "configuration " << _id->Name() << " of " ;
    if (_entity_name) _entity_name->PrettyPrint(f,level+1) ;
    f << " is " << endl ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) elem->PrettyPrint(f,level+1) ;
    if (_block_configuration) _block_configuration->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end " << _id->Name() << " ;" << endl ;
    f << endl ;
}
void VhdlPackageDecl::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_context_clause,i,elem) elem->PrettyPrint(f,level) ;

    f << PrintLevel(level) << "package " << _id->Name() << " is" << endl ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) elem->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end " << _id->Name() << " ;" << endl ;
    f << endl ;
    // Print all secundary units
    MapIter mi ;
    FOREACH_VHDL_SECONDARY_UNIT(this,mi,elem) {
        elem->PrettyPrint(f,level) ;
    }
}

void VhdlPackageBody::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_context_clause,i,elem) elem->PrettyPrint(f,level) ;

    f << PrintLevel(level) << "package body " << _id->Name() << " is" << endl ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) elem->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end " << _id->Name() << " ;" << endl ;
    f << endl ;
}

#endif // #if 0
