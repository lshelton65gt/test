/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_PRETTYPRINT_VISITOR_H_
#define _VERIFIC_VHDL_PRETTYPRINT_VISITOR_H_

#include "VhdlVisitor.h"    // Visitor base class definition

#include <fstream>

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------------------- */

class VhdlPrettyPrintVisitor : public VhdlVisitor
{
public:
    VhdlPrettyPrintVisitor(char *pFileName);
    virtual ~VhdlPrettyPrintVisitor();

/* ================================================================= */
/*                         VISIT METHODS                             */
/* ================================================================= */

    // The following class definitions can be found in VhdlUnits.h
    virtual void Visit(VhdlEntityDecl &node) ;
    virtual void Visit(VhdlConfigurationDecl &node) ;
    virtual void Visit(VhdlPackageDecl &node) ;
    virtual void Visit(VhdlArchitectureBody &node) ;
    virtual void Visit(VhdlPackageBody &node) ;

    // The following class definitions can be found in VhdlConfiguration.h
    virtual void Visit(VhdlBlockConfiguration &node) ;
    virtual void Visit(VhdlComponentConfiguration &node) ;

    // The following class definitions can be found in VhdlDeclaration.h
    virtual void Visit(VhdlScalarTypeDef &node) ;
    virtual void Visit(VhdlArrayTypeDef &node) ;
    virtual void Visit(VhdlRecordTypeDef &node) ;
    virtual void Visit(VhdlAccessTypeDef &node) ;
    virtual void Visit(VhdlFileTypeDef &node) ;
    virtual void Visit(VhdlEnumerationTypeDef &node) ;
    virtual void Visit(VhdlPhysicalTypeDef &node) ;
    virtual void Visit(VhdlElementDecl &node) ;
    virtual void Visit(VhdlPhysicalUnitDecl &node) ;
    virtual void Visit(VhdlUseClause &node) ;
    virtual void Visit(VhdlLibraryClause &node) ;
    virtual void Visit(VhdlInterfaceDecl &node) ;
    virtual void Visit(VhdlSubprogramDecl &node) ;
    virtual void Visit(VhdlSubprogramBody &node) ;
    virtual void Visit(VhdlSubtypeDecl &node) ;
    virtual void Visit(VhdlFullTypeDecl &node) ;
    virtual void Visit(VhdlIncompleteTypeDecl &node) ;
    virtual void Visit(VhdlConstantDecl &node) ;
    virtual void Visit(VhdlSignalDecl &node) ;
    virtual void Visit(VhdlVariableDecl &node) ;
    virtual void Visit(VhdlFileDecl &node) ;
    virtual void Visit(VhdlAliasDecl &node) ;
    virtual void Visit(VhdlComponentDecl &node) ;
    virtual void Visit(VhdlAttributeDecl &node) ;
    virtual void Visit(VhdlAttributeSpec &node) ;
    virtual void Visit(VhdlConfigurationSpec &node) ;
    virtual void Visit(VhdlDisconnectionSpec &node) ;
    virtual void Visit(VhdlGroupTemplateDecl &node) ;
    virtual void Visit(VhdlGroupDecl &node) ;

    // The following class definitions can be found in VhdlExpression.h
    virtual void Visit(VhdlExplicitSubtypeIndication &node) ;
    virtual void Visit(VhdlRange &node) ;
    virtual void Visit(VhdlBox &node) ;
    virtual void Visit(VhdlAssocElement &node) ;
    virtual void Visit(VhdlOperator &node) ;
    virtual void Visit(VhdlAllocator &node) ;
    virtual void Visit(VhdlAggregate &node) ;
    virtual void Visit(VhdlQualifiedExpression &node) ;
    virtual void Visit(VhdlElementAssoc &node) ;
    virtual void Visit(VhdlWaveformElement &node) ;

    // The following class definitions can be found in VhdlIdDef.h
    virtual void Visit(VhdlIdDef &node) ;
    virtual void Visit(VhdlLibraryId &node) ;
    virtual void Visit(VhdlGroupId &node) ;
    virtual void Visit(VhdlGroupTemplateId &node) ;
    virtual void Visit(VhdlAttributeId &node) ;
    virtual void Visit(VhdlComponentId &node) ;
    virtual void Visit(VhdlAliasId &node) ;
    virtual void Visit(VhdlFileId &node) ;
    virtual void Visit(VhdlVariableId &node) ;
    virtual void Visit(VhdlSignalId &node) ;
    virtual void Visit(VhdlConstantId &node) ;
    virtual void Visit(VhdlTypeId &node) ;
    virtual void Visit(VhdlUniversalInteger &node) ;
    virtual void Visit(VhdlUniversalReal &node) ;
    virtual void Visit(VhdlAnonymousType &node) ;
    virtual void Visit(VhdlSubtypeId &node) ;
    virtual void Visit(VhdlSubprogramId &node) ;
    virtual void Visit(VhdlOperatorId &node) ;
    virtual void Visit(VhdlInterfaceId &node) ;
    virtual void Visit(VhdlEnumerationId &node) ;
    virtual void Visit(VhdlElementId &node) ;
    virtual void Visit(VhdlPhysicalUnitId &node) ;
    virtual void Visit(VhdlEntityId &node) ;
    virtual void Visit(VhdlArchitectureId &node) ;
    virtual void Visit(VhdlConfigurationId &node) ;
    virtual void Visit(VhdlPackageId &node) ;
    virtual void Visit(VhdlPackageBodyId &node) ;
    virtual void Visit(VhdlLabelId &node) ;
    virtual void Visit(VhdlBlockId &node) ;

    // The following class definitions can be found in VhdlMisc.h
    virtual void Visit(VhdlSignature &node) ;
    virtual void Visit(VhdlFileOpenInfo &node) ;
    virtual void Visit(VhdlBindingIndication &node) ;
    virtual void Visit(VhdlWhileScheme &node) ;
    virtual void Visit(VhdlForScheme &node) ;
    virtual void Visit(VhdlIfScheme &node) ;
    virtual void Visit(VhdlTransport &node) ;
    virtual void Visit(VhdlInertialDelay &node) ;
    virtual void Visit(VhdlOptions &node) ;
    virtual void Visit(VhdlConditionalWaveform &node) ;
    virtual void Visit(VhdlSelectedWaveform &node) ;
    virtual void Visit(VhdlBlockGenerics &node) ;
    virtual void Visit(VhdlBlockPorts &node) ;
    virtual void Visit(VhdlCaseStatementAlternative &node) ;
    virtual void Visit(VhdlElsif &node) ;
    virtual void Visit(VhdlEntityClassEntry &node) ;

    // The following class definitions can be found in VhdlName.h
    virtual void Visit(VhdlAll &node) ;
    virtual void Visit(VhdlOthers &node) ;
    virtual void Visit(VhdlUnaffected &node) ;
    virtual void Visit(VhdlInteger &node) ;
    virtual void Visit(VhdlReal &node) ;
    virtual void Visit(VhdlStringLiteral &node) ;
    virtual void Visit(VhdlBitStringLiteral &node) ;
    virtual void Visit(VhdlCharacterLiteral &node) ;
    virtual void Visit(VhdlPhysicalLiteral &node) ;
    virtual void Visit(VhdlNull &node) ;
    virtual void Visit(VhdlIdRef &node) ;
    virtual void Visit(VhdlSelectedName &node) ;
    virtual void Visit(VhdlIndexedName &node) ;
    virtual void Visit(VhdlSignaturedName &node) ;
    virtual void Visit(VhdlAttributeName &node) ;
    virtual void Visit(VhdlOpen &node) ;
    virtual void Visit(VhdlEntityAspect &node) ;
    virtual void Visit(VhdlInstantiatedUnit &node) ;

    // The following class definitions can be found in VhdlSpecification.h
    virtual void Visit(VhdlProcedureSpec &node) ;
    virtual void Visit(VhdlFunctionSpec &node) ;
    virtual void Visit(VhdlComponentSpec &node) ;
    virtual void Visit(VhdlGuardedSignalSpec &node) ;
    virtual void Visit(VhdlEntitySpec &node) ;

    // The following class definitions can be found in VhdlStatement.h
    virtual void Visit(VhdlNullStatement &node) ;
    virtual void Visit(VhdlReturnStatement &node) ;
    virtual void Visit(VhdlExitStatement &node) ;
    virtual void Visit(VhdlNextStatement &node) ;
    virtual void Visit(VhdlLoopStatement &node) ;
    virtual void Visit(VhdlCaseStatement &node) ;
    virtual void Visit(VhdlIfStatement &node) ;
    virtual void Visit(VhdlVariableAssignmentStatement &node) ;
    virtual void Visit(VhdlSignalAssignmentStatement &node) ;
    virtual void Visit(VhdlWaitStatement &node) ;
    virtual void Visit(VhdlReportStatement &node) ;
    virtual void Visit(VhdlAssertionStatement &node) ;
    virtual void Visit(VhdlProcessStatement &node) ;
    virtual void Visit(VhdlBlockStatement &node) ;
    virtual void Visit(VhdlGenerateStatement &node) ;
    virtual void Visit(VhdlComponentInstantiationStatement &node) ;
    virtual void Visit(VhdlProcedureCallStatement &node) ;
    virtual void Visit(VhdlConditionalSignalAssignment &node) ;
    virtual void Visit(VhdlSelectedSignalAssignment &node) ;

public:
    // Did the file open for output correctly?
    bool IsFileGood() const { return _bFileGood; }

private:
    std::ofstream    _ofs;         // Output file stream
    bool            _bFileGood;    // States whether the file was opened correctly
    unsigned        _nLevel;       // Indentation level - used for output blank spaces

    // _nLevel modifiers
    void IncTabLevel(unsigned nIncVal)     { _nLevel += nIncVal ; }    // Increase indentation level
    void DecTabLevel(unsigned nDecVal)     { _nLevel -= nDecVal ; }    // Decrease indentation level

    /* ================================================================= */
    /*                    OUTPUT UTILITY METHODS                         */
    /* ================================================================= */

    // Print tab indentation based on level
    static const char* PrintLevel(unsigned level);

    // Prevent the compiler from implementing the following
    VhdlPrettyPrintVisitor(VhdlPrettyPrintVisitor &ref);
    VhdlPrettyPrintVisitor& operator=(const VhdlPrettyPrintVisitor &rhs);
} ;

/* -------------------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_PRETTYPRINT_VISITOR_H_

