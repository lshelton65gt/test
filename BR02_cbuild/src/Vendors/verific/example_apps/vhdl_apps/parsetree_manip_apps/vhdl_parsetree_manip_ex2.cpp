/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Array.h"              // Make dynamic array class Array available
#include "Map.h"                // Make associated hash table class Map available
#include "Strings.h"            // A string utility/wrapper class

#include "Message.h"            // Make message handlers available, not used in this example

#include "vhdl_file.h"          // Make VHDL reader available

#include "VhdlUnits.h"          // Definitions of a VhdlLibrary and VhdlDesignUnits and their subclasses
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations constructs
#include "VhdlExpression.h"     // Definitions of all vhdl expresssion constructs
#include "VhdlSpecification.h"  // Definitions of all vhdl specification constructs
#include "VhdlStatement.h"      // Definitions of all vhdl statement constructs
#include "VhdlName.h"           // Definitions of all vhdl name (expression) constructs
#include "VhdlMisc.h"           // Definitions of all extraneous vhdl constructs
#include "VhdlScope.h"          // Symbol table of locally declared identifiers
#include "VhdlIdDef.h"          // Symbol table of locally declared identifiers
#include "vhdl_tokens.h"        // Symbol table of locally declared identifiers

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
********************************* VHDL EXAMPLE #2 ***************************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is an example to demonstrate parse tree manipulation utility using
    user friendly APIs.
    The name of the design file to be modified is taken from command line argument.
    The example will try to show the following modifications on analyzed parse tree:

        1) How to remove port refs (actuals) from instantiation.
        2) How to remove ports from entity declaration.
        3) How to add ports to entity declaration.
        4) How to add signal declaration to architecture.
        5) How to add port refs (actuals) to instantiation.
        6) How to remove generic ref (actual) from instantiation.
        7) How to remove generic from entity declaration.
        8) How to add generic to an entity declaration.
        9) How to remove instantiation from architecture.
        10) How to remove signal declaration from architecture.
        11) How to remove component declaration from architecture.
        12) How to add a component declaration to architecture.
        13) How to add an instance to architecture.

    If file 'ex2_test.vhd' is given in command line argument, the above modifications
    will be done successfully. But if other files i.e 'neg_ex2_test.vhd' is given, example
    will produce appropriate error messages.

*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main()
{
    // Create a vhdl reader object
    vhdl_file vhdl_reader;   // Read the intro in vhdl_file.h to see the details of this object.

    // First we need to analyze the standard VHDL package design file depends on.
    // They are located in the "../../vhdl_packages".
    vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");   // store in std library

    // Now analyze the above example VHDL file (into the work library) :
    // If there is any error, don't proceed further
    if (!vhdl_reader.Analyze("ex2_test.vhd", "work")) return 1 ;

    //---------------------------------------
    // Pretty Print (Pre Parse-tree Modification)
    //---------------------------------------
    // Pretty print current design
    vhdl_reader.PrettyPrint("design_before_pp_ex2.vhd", 0, "work") ;

    // In parse tree manipulation we need to add or remove port, generic, signal,
    // component declaration, instance to primary or secondary units. Remove APIs
    // take care of keeping parse-tree consistent, and bringing it back in pre-
    // insertion condition, but in appilications if we want to remove port/generic/
    // signal/component declaration, we need to remove their references first.

    // Now the following example will try to use all types of Add/Remove APIs.
    // Declaration will be removed only after removing references.
    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
      |  REMOVE PORT REFS (ACTUALS) FROM INSTANCE 'i1'  |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To remove port refs from instantiation the following procedure is followed

    /******************************************************************************
    1) Get handle to secondary unit (architecture 'arch') where instantiation resides.
    2) Call routine 'RemovePortRef' to remove each port ref at a time. This routine
       will return 1 on successful removal, otherwise it will return 0.
    *****************************************************************************/

    // First we need to get a handle to the "work" library
    VhdlLibrary *lib = vhdl_reader.GetLibrary("work") ;

    // Get a handle (by name) to the "top" entity that was just analyzed
    VhdlPrimaryUnit *primary_unit = lib ? lib->GetPrimUnit("top"): 0 ;

    // Get a handle (by name) to the "arch" architecture that was just analyzed
    VhdlSecondaryUnit *secondary_unit = primary_unit ? primary_unit->GetSecondaryUnit("arch") : 0 ;

    if (!secondary_unit || !primary_unit) { // "top" and "arch" are not analyzed, don't proceed further
        Message::PrintLine("Primary unit 'top' or secondary unit 'arch' is not yet analyzed") ;
        return 1 ;
    }

    // Remove actual of formal 'q' from instance 'i1'
    unsigned is_removed = secondary_unit->RemovePortRef("i1" /* instance name*/, "q" /* formal */) ;

    if (!is_removed) { // Can't remove port ref for formal 'q'
        Message::PrintLine("Can't remove port ref for formal 'q' from instance 'i1'") ;
    }

    // Remove actual of formal 'rst' from instance 'i1'
    is_removed = secondary_unit->RemovePortRef("i1" /* instance name*/, "rst" /* formal */) ;

    if (!is_removed) { // Can't remove port ref for formal 'rst'
        Message::PrintLine("Cannot remove port ref for formal 'rst' from instance 'i1'") ;
    }

    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |        REMOVE PORT 'reset' FROM ENTITY 'top'        |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To remove port declaration from entity the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the entity from which port 'reset' is to be removed.
    2) Call 'RemovePort' routine to remove port. This routine will return 1 for successful
       removal and 0 for failure.
    ***********************************************************************************/

    // We already have a handle to entity 'top' (primary_unit)

    // Call 'RemovePort' to remove port 'reset'. This routine will remove port declaration
    // of port 'reset' from port clause, remove port identifier from scope and delete port
    // identifier
    is_removed = primary_unit->RemovePort("reset" /* port to be removed */) ;

    if (!is_removed) { // Can't remove port 'reset'
        Message::PrintLine("Cannot remove port 'reset' from entity 'top'") ;
    }

    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |           ADD PORT 'rst' TO ENTITY 'top'            |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To add port declaration to entity the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the entity 'top' where port 'rst' is to be inserted.
    2) Call 'AddPort' routine to add port. This routine will return the created
       port identifier on success and 0 on failure.
    ***********************************************************************************/

    // We already have a handle to entity 'top'

    // Call 'AddPort' routine. This routine will create port identifier, add it to
    // entity scope, create port declaration, add created declaration to port clause.
    VhdlIdDef *new_port = primary_unit->AddPort("rst" /* port name*/, VHDL_in /* mode*/, new VhdlIdRef(Strings::save("bit"))/* subtype indication*/) ;

    if (!new_port) { // Can't add port 'rst'
        Message::PrintLine("Cannot add port 'rst' to entity 'top'") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |    ADD SIGNAL 'in_clk' TO ARCHITECTURE 'arch'       |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To add signal declaration to architecture the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the architecture 'arch' where signal 'in_clk' is to be inserted.
    2) Call 'AddSignal' routine. This routine will return the created signal identifier
       on success and 0 on failure.
    ***********************************************************************************/
    // We already have a handle to architecture 'arch' (secondary_unit)

    // Call 'AddSignal' routine. This routine will create signal identifier, create
    // signal declaration, type infer created declaration and add it to declarative
    // region of architecture
    VhdlIdDef *new_signal_id = secondary_unit->AddSignal("in_clk" /* signal name*/, new VhdlIdRef(Strings::save("bit")) /* subtype indication*/, 0 /* optional initial value*/) ;

    if (!new_signal_id) { // Can't create signal 'in_clk'
        Message::PrintLine("Cannot add signal 'in_clk' to architecture 'arch'") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |   ADD PORT REFS TO INSTANCE 'i1' IN ARCHITECTURE    |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To add port refs (actuals) to instance 'i1' the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the architecture 'arch' where instance 'i1' resides.
    2) Call 'AddPortRef' to add each port ref. This routine will return 1 on success and
       0 on failure.
    ***********************************************************************************/
    // We already have a handle to architecture 'arch' (secondary_unit)

    // Call 'AddPortRef' routine. This routine create association, check validity
    // of created port ref and add it to port map aspect of instance 'i1'
    // We will connect created signal 'in_clk' to formal 'q'
    unsigned is_added = secondary_unit->AddPortRef("i1" /* instance name*/, "q" /* formal */, new VhdlIdRef(Strings::save("in_clk")) /* actual */) ;

    if (!is_added) { // Cannot connect 'q'
        Message::PrintLine("Cannot connect signal 'in_clk' as actual of 'q' for instance 'i1'") ;
    }

    // Connect created port 'rst' to formal 'rst' of instance 'i1'
    is_added = secondary_unit->AddPortRef("i1" /* instance name*/, "rst" /* formal*/, new VhdlIdRef(Strings::save("rst"))/* actual*/) ;

    if (!is_added) { // Cannot connect 'rst'
        Message::PrintLine("Cannot connect port 'rst' as actual of 'rst' for instance 'i1'") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |  REMOVE GENERIC REF FROM INSTANCE 'i1' IN ARCHITECTURE |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To remove generic refs (actuals) from instance 'i1' the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the architecture 'arch' where instance 'i1' resides.
    2) Call 'RemoveGenericRef' to remove generic reference. This routine will return 1
       on success and 0 on failure.
    ***********************************************************************************/
    // We already have a handle to architecture 'arch' (secondary_unit)

    // Call 'RemoveGenericRef' to remove association for generic 'd1' from instance 'i1'
    is_removed = secondary_unit->RemoveGenericRef("i1" /* instance name*/, "d1" /* formal name */) ;

    if (!is_removed) { // Can't remove generic ref for 'd1'
        Message::PrintLine("cannot remove generic ref for formal 'd1' from instance 'i1'") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |  REMOVE GENERIC 'd1' FROM ENTITY 'top' |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To remove generic from entity declaration the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the entity 'top' from which generic 'd1' is to be removed.
    2) Call 'RemoveGeneric' to remove generic declaration. This routine will return 1
       on success and 0 on failure.
    ***********************************************************************************/
    // We already have a handle to entity 'top' (primary_unit)

    // Call 'RemoveGeneric' routine. This will remove generic declaration from the
    // generic clause of entity 'top', remove generic identifier from entity scope
    // and delete the generic identifier.
    is_removed = primary_unit->RemoveGeneric("g" /* generic to be removed*/) ;

    if (!is_removed) { // Can't remove generic
        Message::PrintLine("Cannot remove generic 'g' from entity 'top'") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |     ADD GENERIC 'd1' TO ENTITY 'top'    |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To add generic 'd1' to entity declaration the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the entity 'top' where generic 'd1' is to be inserted.
    2) Call 'AddGeneric' routine. This routine will return the created generic identifier
       on success and 0 on failure.
    ***********************************************************************************/
    // We already have a handle to entity 'top' (primary_unit)

    // Call 'AddGeneric' routine. This routine will create generic identifier, create
    // generic declaration and add created declaration to generic clause of entity
    // declaration.
    VhdlIdDef *new_generic = primary_unit->AddGeneric("d1" /* generic to be added*/, new VhdlIdRef(Strings::save("integer")) /* subtype indication*/, new VhdlInteger(10)/* initial value*/) ;

    if (!new_generic) { // Can't add generic 'd1'
        Message::PrintLine("Cannot add generic 'd1' to entity 'top'") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       | REMOVE INSTANCE 'i2' FROM ARCHITECTURE  |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To remove instance 'i2' from architecture declaration the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the architecture 'arch' where instance 'i2' resides.
    2) Call 'RemoveInstance' routine. This routine will return 1 on success and 0
       on failure.
    ***********************************************************************************/
    // We already have a handle to architecture 'arch' (secondary_unit)

    // Call 'RemoveInstance' routine to remove instance 'i2' from statement list of
    // architecture 'arch'
    is_removed = secondary_unit->RemoveInstance("i2" /* instance to be removed */) ;

    if (!is_removed) { // Can't remove instance 'i2'
        Message::PrintLine("Cannot remove instance 'i2' from architecture 'arch'") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |  REMOVE SIGNAL 'tmp' FROM ARCHITECTURE  |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */
    // To remove signal 'tmp' from architecture declaration the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the architecture 'arch' where signal 'tmp' resides.
    2) Call 'RemoveSignal' routine. This routine will return 1 on success and 0
       on failure.
    ***********************************************************************************/
    // We already have a handle to architecture 'arch' (secondary_unit)

    // Call 'RemoveSignal' routine. This routine will remove particular signal
    // declaration from declarative part of architecture, remove signal identifier
    // from architecture's scope and delete signal identifier.
    is_removed = secondary_unit->RemoveSignal("tmp" /* signal to be removed */) ;

    if (!is_removed) { // Can't remove signal
        Message::PrintLine("Cannot remove signal 'tmp' from architecture 'arch'") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |  REMOVE COMPONENT DECLARATION FROM ARCHITECTURE |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */
    // To remove component 'and2' from architecture declaration the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the architecture 'arch' where component 'and2' resides.
    2) Call 'RemoveComponentDecl' routine. This routine will return 1 on success and 0
       on failure.
    ***********************************************************************************/
    // We already have a handle to architecture 'arch' (secondary_unit)

    // Call 'RemoveComponentDecl' routine. This routine will remove component
    // declaration 'and2' from declarative part of architecture and delete it.
    is_removed = secondary_unit->RemoveComponentDecl("and2" /* component to be removed*/) ;

    if (!is_removed) { // Can't remove component 'and2'
        Message::PrintLine("Cannot remove component 'and2' from architecture 'arch'") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |    ADD COMPONENT DECLARATION TO ARCHITECTURE    |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */
    // To add component 'dff' to architecture declaration the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the architecture 'arch' where component 'dff' is to be declared.
    2) Call 'AddComponentDecl' routine. This routine will return created component
       identifier on success and 0 on failure.
    3) Call 'AddPort' and 'AddGeneric' to add ports and generic to created component
       declaration.
    ***********************************************************************************/
    // We already have a handle to architecture 'arch' (secondary_unit)

    // Call 'AddComponent' routine to create component declaration. This routine will
    // create component identifier, add it to architure's scope, create component
    // declaration and add created declaration to declarative part of architecture.
    VhdlIdDef *new_component_id = secondary_unit->AddComponentDecl("dff" /* component to be added */) ;

    if (!new_component_id) { // Cannot add component
        Message::PrintLine("Cannot add component 'dff' to architecture 'arch'") ;
    }

    // Get pointer to created component declaration from previous routine created
    // component id 'new_component_id'
    VhdlComponentDecl *new_component_decl = new_component_id ? new_component_id->GetComponentDecl() : 0 ;

    // 3) Add ports and generic to created component
    if (new_component_decl) {
        // Add input port "d" to component
        new_component_decl->AddPort("d" /* port name*/, VHDL_in /* mode*/, new VhdlIdRef(Strings::save("bit"))/* subtype indication*/) ;

        // Add input port "rst" to component
        new_component_decl->AddPort("rst" /* port name*/, VHDL_in /* mode*/, new VhdlIdRef(Strings::save("bit")) /* subtype indication*/) ;

        // Add input port "set" to component
        new_component_decl->AddPort("set" /* port name*/, VHDL_in /* mode*/, new VhdlIdRef(Strings::save("bit")) /* subtype indication*/) ;

        // Add input port "clk" to component
        new_component_decl->AddPort("clk" /* port name*/, VHDL_in /* mode*/, new VhdlIdRef(Strings::save("bit")) /* subtype indication*/) ;

        // Add output port "q" to component
        new_component_decl->AddPort("q" /* port name*/, VHDL_out /* mode*/, new VhdlIdRef(Strings::save("bit")) /* subtype indication*/) ;

        // Add generic "g" with initial value '10' to component
        new_component_decl->AddGeneric("g" /* generic name*/, new VhdlIdRef(Strings::save("integer")) /* subtype indication*/, new VhdlInteger(10) /* initial value*/) ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       | INSERT NEW COMPONENT INSTANTIATION INTO ARCHITECTURE |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To insert new component instantiation the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the architecture where component instantiation is to be added.
    2) Call 'AddInstance' routine to create component instantiation. This routine will
       return created component instantiation if it can add instance properly, otherwise
       it will return 0.
    3) Call 'AddPortRef'/'AddGenericRef' routine of created connection list. These routines
       will return 1 on success and 0 on failure.
    ***********************************************************************************/

    // We want to add component instantiation in archirtecture 'arch' of entity
    // 'top'. Since we already extract that for previous step, we have no need to
    // get it further. 'arch' is the desired secondary unit (architecture)

    // Create component instantiation
    VhdlComponentInstantiationStatement *new_instance = secondary_unit->AddInstance("i3" /* instance name */, "dff" /* component to be instantiated */) ;

    // Add port connections to created instance

    // Add actual for formal 'd' of component 'new_component'
    secondary_unit->AddPortRef("i3" /* instance name */, "d" /* formal */, new VhdlIdRef(Strings::save("d")) /* actual */) ;

    // Add actual for formal 'q' of component 'new_component'
    secondary_unit->AddPortRef("i3" /* instance name */, "q" /* formal */, new VhdlIdRef(Strings::save("q")) /* actual */) ;

    // Add actual for formal 'set' of component 'new_component'
    secondary_unit->AddPortRef("i3" /* instance name */, "set" /* formal */, new VhdlIdRef(Strings::save("set")) /* actual */) ;

    // Add actual for formal 'rst' of component 'new_component'
    secondary_unit->AddPortRef("i3" /* instance name */, "rst" /* formal */, new VhdlIdRef(Strings::save("rst")) /* actual */) ;

    // Add actual for formal 'clk' of component 'new_component'
    secondary_unit->AddPortRef("i3" /* instance name */, "clk" /* formal */, new VhdlIdRef(Strings::save("in_clk")) /* actual */) ;

    // Add actual for generic 'g' of component 'new_component'
    secondary_unit->AddGenericRef("i3" /* instance name */, "g" /* formal */, new VhdlInteger(20) /* actual */) ;

    //---------------------------------------
    // Pretty Print (Post Parse-tree Modification)
    //---------------------------------------
    // To make sure that everything went as planned, pretty print the current
    // parse-tree's out and compare with the previous pretty printed output.
    vhdl_reader.PrettyPrint("design_after_pp_ex2.vhd", 0, "work");

    return 1;
}
