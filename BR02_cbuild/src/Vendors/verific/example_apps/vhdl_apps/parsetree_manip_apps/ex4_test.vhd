library ieee;
use ieee.std_logic_1164.all;

entity ILogic is
  port ( a, b : in std_logic;
         logicout : out std_logic_vector(1 downto 0));
end ILogic;

architecture arch of ILogic is
begin  
  logicout(0) <= a xor b;
  logicout(1) <= logicout(0); -- error VHDL-1033 downgraded to warning
end arch;

