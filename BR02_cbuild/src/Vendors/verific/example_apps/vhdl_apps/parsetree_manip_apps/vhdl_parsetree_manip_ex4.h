/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_OP_BUFFER_VISITOR_H_
#define _VERIFIC_VHDL_OP_BUFFER_VISITOR_H_

#include "VhdlVisitor.h"    // Visitor base class definition
#include "Map.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class VhdlOutputReplaceVisitor : public VhdlVisitor
{
 public:
    VhdlOutputReplaceVisitor(Map *output_port_sig_map_) ;
    virtual ~VhdlOutputReplaceVisitor() ;

    virtual void Visit(VhdlIdRef &node) ;

 private:
    Map *output_port_sig_map ;
} ;

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_OP_BUFFER_VISITOR_H_
