/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Array.h"              // Make dynamic array class Array available
#include "Map.h"                // Make associated hash table class Map available
#include "Strings.h"            // A string utility/wrapper class

#include "Message.h"            // Make message handlers available, not used in this example

#include "vhdl_file.h"          // Make VHDL reader available

#include "VhdlUnits.h"          // Definitions of a VhdlLibrary and VhdlDesignUnits and their subclasses
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations constructs
#include "VhdlExpression.h"     // Definitions of all vhdl expresssion constructs
#include "VhdlSpecification.h"  // Definitions of all vhdl specification constructs
#include "VhdlStatement.h"      // Definitions of all vhdl statement constructs
#include "VhdlName.h"           // Definitions of all vhdl name (expression) constructs
#include "VhdlMisc.h"           // Definitions of all extraneous vhdl constructs
#include "VhdlScope.h"          // Symbol table of locally declared identifiers
#include "VhdlIdDef.h"          // Symbol table of locally declared identifiers
#include "vhdl_tokens.h"          // Symbol table of locally declared identifiers

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
********************************* VHDL EXAMPLE #1 ***************************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is a parse tree modification example that shows:

        1) How to replace a entity instantiation by conditional signal assignment.
        2) How to add an extra output port at a particular position to a entity.
        3) How to add an entity instantiation to the architecture.

*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main()
{
    // Create a vhdl reader object
    vhdl_file vhdl_reader;   // Read the intro in vhdl_file.h to see the details of this object.

    // The following design ("ex1_test.vhd") is to be read in:

    //-------------- "ex1_test.vhd" -------------
    /*
        entity XOR2 is
            port (A, B : in bit;
                  S : out bit);
        end ;

        architecture arch of XOR2 is
        begin
           S <= A xor B ;
        end;

        entity test is
           port (S : out bit ;
                 A, B : in bit) ;
        end ;

        architecture structural of test is
        begin
           I : entity work.XOR2 port map(A, B, S) ;

        end ;

    */
    //---------------------------------------

    // First we need to analyze the VHDL packages "ex1_test.vhd" depends on.
    // They are located in the "../../vhdl_packages".
    vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");   // store in std library

    // Now analyze the above example VHDL file "ex1_test.vhd" (into the work library) :
    vhdl_reader.Analyze("ex1_test.vhd", "work") ;

    //---------------------------------------
    // Pretty Print (Pre Parse-tree Modification)
    //---------------------------------------
    // Pretty print current design
    vhdl_reader.PrettyPrint("test_before_pp_ex1.vhd", 0, "work") ;

    //-------------"test_before_pp_ex1.vhd" ------------
    /*
        library std,work;
        use std.standard.all;
        entity XOR2 is
            port (
                A,B : in bit;
                S : out bit) ;
        end XOR2 ;

        library std,work;
        architecture arch of xor2 is
        begin
            s <= (a xor b);
        end arch ;

        library std,work;
        use std.standard.all;
        entity test is
            port (
                S : out bit;
                A,B : in bit) ;
        end test ;

        library std,work;
        architecture structural of test is
        begin
            I : entity work.xor2 port map (a, b, s);
        end structural ;

     */
    //------------------------------------------------

    /*
     /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
     |  REPLACE INSTANTIATION BY CONCURRENT ASSIGNMENT |
     \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
     */

     // To replace the instantiation, the following procedure is followed.

     /***************************************************************************************
     1) Get a handle (by name) to the "TEST" entity that was just analyzed
     2) Create an instance of VhdlIdRef to hold target signal "S".
        Type infer the target
     3) Create two instances of VhdlIdRef to hold ports "A" and "B"
     4) Create an instance of VhdlOperator to absorb previous step
        created VhdlIdRefs. Type infer the operator to link VhdlIdRefs
        to proper VhdlIdDefs
     5) Create an instance of VhdlWaveformElement to hold binary operator
     6) Create an array of waveform elements and insert the waveform element
     7) Create an instance of VhdlConditionalWaveform and absorb the waveform
        list
     8) Create an array of conditional waveforms and insert the conditional
        waveform.
     9) Create an instance of VhdlConditionalSignalAssignment with step 2 and
        step 8 created elements.
     10) Get the statement list of architecture where assignment is to be
         inserted
     11) Get the entity instantiation from architecture's statement list
     12) Replace instantiation by concurrent assignment

      ****************************************************************************************/
    // First we need to get a handle to the "work" library
    VhdlLibrary *lib = vhdl_reader.GetLibrary("work") ;

    // 1) Get a handle (by name) to the "TEST" entity that was just analyzed
    VhdlPrimaryUnit *primary_unit = lib->GetPrimUnit("TEST") ;
    VhdlEntityDecl *entity_test = static_cast<VhdlEntityDecl*>(primary_unit) ;

    // Get a handle (by name) to the architecture "structural" that was just analyzed
    VhdlSecondaryUnit *secondary_unit = entity_test->GetSecondaryUnit("structural")  ;
    // Downcast VhdlSecondaryUnit -> VhdlArchitectureBody.  Avoid dynamic_cast overhead.
    VhdlArchitectureBody *arch_of_test = static_cast<VhdlArchitectureBody*>(secondary_unit) ;

    // 2) Create an instance of VhdlIdRef to hold target signal "S".
    //    Type infer the target
    VhdlIdRef *target = new VhdlIdRef (Strings::save("S")) ;

    // Set global scope pointer _present_scope as architecture scope.
    // Type infer will link VhdlIdRef with VhdlIdDef finding VhdlIdRef
    // from VhdlTreeNode::_present_scope.
    VhdlTreeNode::_present_scope = arch_of_test->LocalScope() ;
    VhdlIdDef *target_type = target->TypeInfer(0, 0, VHDL_SIGUPDATE, arch_of_test->LocalScope()) ;

    // 3) Create two instances of VhdlIdRef to hold ports "A" and "B"
    VhdlIdRef *a_ref = new VhdlIdRef (Strings::save("A")) ;
    VhdlIdRef *b_ref = new VhdlIdRef (Strings::save("B")) ;

    // 4) Create an instance of VhdlOperator to absorb previous step
    //    created VhdlIdRefs. Type infer the operator to link VhdlIdRefs
    //    to proper VhdlIdDefs
    VhdlOperator *bin_op = new VhdlOperator(a_ref, VHDL_xor, b_ref) ;
    bin_op->TypeInfer(target_type, 0, 0, arch_of_test->LocalScope()) ;

    // 5) Create an instance of VhdlWaveformElement to hold binary operator
    VhdlWaveformElement *waveform_ele = new VhdlWaveformElement(bin_op, 0) ;

    // 6) Create an array of waveform elements and insert the waveform element
    Array *waveform_list = new Array(1) ;
    waveform_list->InsertLast(waveform_ele) ;

    // 7) Create an instance of VhdlConditionalWaveform and absorb the waveform
    //    list
    VhdlConditionalWaveform *cond_waveform = new VhdlConditionalWaveform(waveform_list, 0) ;

    // 8) Create an array of conditional waveforms and insert the conditional
    //    waveform.
    Array *cond_waveforms = new Array(1) ;
    cond_waveforms->InsertLast(cond_waveform) ;

    // 9) Create an instance of VhdlConditionalSignalAssignment with step 2 and
    //    step 8 created elements.
    VhdlConditionalSignalAssignment *assignment = new VhdlConditionalSignalAssignment(target, 0, cond_waveforms) ;

    // 10) Get the statement list of architecture where assignment is to be
    //     inserted
    Array *stmts = arch_of_test->GetStatementPart() ;

    // 11) Get the entity instantiation from architecture's statement list
    VhdlStatement *instantiation = (VhdlStatement*)stmts->At(0) ;

    // 12) Replace instantiation by concurrent assignment
    arch_of_test->ReplaceChildStmt(instantiation, assignment) ;

    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
      |  ADD NEW OUTPUT PORT "C" TO ENTITY "TEST"  |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
     */

     // To add a new output port the following procedure is followed

     /***************************************************************************************
     1) Create an instance of VhdlInterfaceId to represent port identifier "C"
     2) Declare the created port identifier to the scope of entity
     3) Mark the created identifier as port
     4) Create an array of identifiers and insert the new port there
     5) Create an instance of VhdlIdRef to represent the subtype of port
        declaration
     6) Create an instance of VhdlInterfaceDecl to absorb created idlist
        and subtype
     7) Get the port declaration list of entity "TEST" and extract the
        first port.
     8) Call InsertAfterDecl routine of entity to add new port declaration
        as second port declaration of port declaration list
      ***************************************************************************************/

    // 1) Create an instance of VhdlInterfaceId to represent port identifier "C"
    VhdlInterfaceId *port_id = new VhdlInterfaceId(Strings::save("C")) ;

    // 2) Declare the created port identifier to the scope of entity
    entity_test->LocalScope()->Declare(port_id) ;

    // 3) Mark the created identifier as port
    port_id->SetObjectKind(VHDL_port) ;

    // 4) Create an array of identifiers and insert the new port there
    Array *id_list = new Array(1) ;
    id_list->InsertLast(port_id) ;

    // 5) Create an instance of VhdlIdRef to represent the subtype of port
    //    declaration
    VhdlIdRef *data_type = new VhdlIdRef(Strings::save("bit")) ;

    // 6) Create an instance of VhdlInterfaceDecl to absorb created idlist
    //    and subtype
    VhdlInterfaceDecl *port_decl = new VhdlInterfaceDecl(0, id_list, VHDL_out, data_type, 0, 0) ;

    // 7) Get the port declaration list of entity "TEST" and extract the
    //    first port.
    Array *ports = entity_test->GetPortClause() ;
    VhdlDeclaration *decl = (VhdlDeclaration*)ports->At(0) ;

    // 8) Call InsertAfterDecl routine of entity to add new port declaration
    //    as second port declaration of port declaration list
    entity_test->InsertAfterDecl(decl, port_decl) ;

    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       | INSERT NEW ENTITY INSTANTIATION INTO ARCHITECTURE |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
     */

    // To insert new entity instantiation the following procedure is to be followed:
    /************************************************************************************
     1) Create an instance of VhdlLabelId to represent the level "I2" of
        entity instantiation to be created
     2) Declare the label identifier to the scope of architecture
     3) Create an instance of VhdlIdRef to hold the name of library
        from which the entity "XOR2" will be instantiated
     4) Create an instance of VhdlIdRef to hold the name of
        instantiated entity
     5) Create an instance of VhdlSelectedName to represent the
        full name of to be instantiated entity.
     6) Create an instance of VhdlInstantiatedUnit to absorb the full
        name of to be instantiated entity
     7) Create three instances of VhdlIdRef to hold the actuals of the
        instantiation
     8) Create an array of actuals (port map aspect) and insert the actuals
     9) Create an instance of VhdlComponentInstantiationStatement to
        represent the entity instantiation
     10) Set the step 1 created label to the entity instantiation
     11) Add the entity instantiation as the last statement
     ***********************************************************************************/

    // 1) Create an instance of VhdlLabelId to represent the level "I2" of
    //    entity instantiation to be created
    VhdlLabelId *label_id = new VhdlLabelId(Strings::save("I2")) ;

    // 2) Declare the label identifier to the scope of architecture
    arch_of_test->LocalScope()->Declare(label_id) ;

    // 3) Create an instance of VhdlIdRef to hold the name of library
    //    from which the entity "XOR2" will be instantiated
    VhdlIdRef *prefix = new VhdlIdRef(Strings::save("work")) ;

    // 4) Create an instance of VhdlIdRef to hold the name of
    //    instantiated entity
    VhdlIdRef *suffix = new VhdlIdRef(Strings::save("XOR2")) ;

    // 5) Create an instance of VhdlSelectedName to represent the
    //    full name of to be instantiated entity.
    VhdlSelectedName *selected_name = new VhdlSelectedName(prefix, suffix) ;

    // 6) Create an instance of VhdlInstantiatedUnit to absorb the full
    //    name of to be instantiated entity
    VhdlInstantiatedUnit *instantiated_unit = new VhdlInstantiatedUnit(VHDL_entity, selected_name) ;

    // 7) Create three instances of VhdlIdRef to hold the actuals of the
    //    instantiation
    VhdlIdRef *actual1 = new VhdlIdRef(Strings::save("A")) ;
    VhdlIdRef *actual2 = new VhdlIdRef(Strings::save("B")) ;
    VhdlIdRef *actual3 = new VhdlIdRef(Strings::save("C")) ;

    // 8) Create an array of actuals (port map aspect) and insert the actuals
    Array *port_map_aspect = new Array(3) ;
    port_map_aspect->InsertLast(actual1) ;
    port_map_aspect->InsertLast(actual2) ;
    port_map_aspect->InsertLast(actual3) ;

    // 9) Create an instance of VhdlComponentInstantiationStatement to
    //    represent the entity instantiation
    VhdlComponentInstantiationStatement *entity_instantiation = new VhdlComponentInstantiationStatement(instantiated_unit, 0, port_map_aspect) ;

    // 10) Set the step 1 created label to the entity instantiation
    entity_instantiation->SetLabel(label_id) ;

    // 11) Add the entity instantiation as the last statement
    stmts->InsertLast(entity_instantiation) ;

    //---------------------------------------
    // Pretty Print (Post Parse-tree Modification)
    //---------------------------------------
    // To make sure that everything went as planned, pretty print the current
    // parse-tree's out and compare with the previous pretty printed output.
    vhdl_reader.PrettyPrint("test_after_pp_ex1.vhd", 0, "work");

    //----------------"test_after_pp_ex1.vhd" -----------
    /*
        library std,work;
        use std.standard.all;
        entity XOR2 is
            port (
                A,B : in bit;
                S : out bit) ;
        end XOR2 ;

        library std,work;
        architecture arch of xor2 is
        begin
            s <= (a xor b);
        end arch ;

        library std,work;
        use std.standard.all;
        entity test is
            port (
                S : out bit;
                C : out bit;
                A,B : in bit) ;
        end test ;

        library std,work;
        architecture structural of test is
        begin
            s <= (a xor b);
            I2 : entity work.xor2 port map (a, b, c);
        end structural ;

     */
    return 1;
}
