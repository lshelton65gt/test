--library ieee;
--use  ieee.std_logic_1164.all;
--use  ieee.std_logic_arith.all;
--use  ieee.std_logic_unsigned.all;
entity XOR2 is
    port (A, B : in bit;
          S : out bit);
end ;

architecture arch of XOR2 is
begin
   S <= A xor B ;
end;

entity test is
   port (S : out bit ;
         --C : out std_logic ;
         A, B : in bit) ;
end ;

architecture structural of test is
begin
   I : entity work.XOR2 port map(A, B, S) ;

end ;

