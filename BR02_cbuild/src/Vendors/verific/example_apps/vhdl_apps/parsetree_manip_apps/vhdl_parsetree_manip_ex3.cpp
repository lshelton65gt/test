/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Array.h"              // Make dynamic array class Array available
#include "Map.h"                // Make associated hash table class Map available
#include "Strings.h"            // A string utility/wrapper class

#include "Message.h"            // Make message handlers available, not used in this example

#include "vhdl_file.h"          // Make VHDL reader available

#include "VhdlUnits.h"          // Definitions of a VhdlLibrary and VhdlDesignUnits and their subclasses
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations constructs
#include "VhdlExpression.h"     // Definitions of all vhdl expresssion constructs
#include "VhdlSpecification.h"  // Definitions of all vhdl specification constructs
#include "VhdlStatement.h"      // Definitions of all vhdl statement constructs
#include "VhdlName.h"           // Definitions of all vhdl name (expression) constructs
#include "VhdlMisc.h"           // Definitions of all extraneous vhdl constructs
#include "VhdlScope.h"          // Symbol table of locally declared identifiers
#include "VhdlIdDef.h"          // Symbol table of locally declared identifiers
#include "vhdl_tokens.h"        // Symbol table of locally declared identifiers

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
********************************* VHDL EXAMPLE #3 ***************************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is an example to demonstrate block statement manipulation utility using
    user friendly APIs.
    The name of the design file to be modified can be provided from command line argument.
    But to add/remove port/generic one must specify thier names, so this application will
    not run for any test case. It is mainly designed for test case 'ex3_test.vhd'
    The example will try to show the following modifications on analyzed parse tree:

        1) How to remove port refs (actuals) from port map of block statement.
        2) How to remove ports from block.
        3) How to add ports to block statement.
        4) How to add port refs (actuals) to port map of block statement.
        5) How to add signal declaration to block statement.
        6) How to remove signal declaration from block statement.
        7) How to remove generic ref (actual) from generic map of block statement.
        8) How to remove generic from block statement.
        9) How to add generic to block statement.
        10) How to add generic refs (actuals) to generic map of block statement.

    If file 'ex3_test.vhd' is given in command line argument, the above modifications
    will be done successfully. But if other files is given, example will produce
    appropriate error messages.

*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main(int argc, char** argv)
{
    // Create a vhdl reader object
    vhdl_file vhdl_reader;   // Read the intro in vhdl_file.h to see the details of this object.

    if (argc < 2) Message::PrintLine("Default input file: ex3_test.vhd. Specify command line argument to override") ;

    // Get the file name to work on. If not specified as command line arg use default file
    const char *file_name = (argc > 1) ? argv[1] : "ex3_test.vhd" ;

    // First we need to analyze the standard VHDL packages
    // They are located in the "../../../vhdl_packages".
    vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");
    vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/std_1164.vhd", "ieee");

    // Now analyze the above example VHDL file (into the work library) :
    // If there is any error, don't proceed further
    if (!vhdl_reader.Analyze(file_name, "work")) return 1 ;

    //---------------------------------------
    // Pretty Print (Pre Parse-tree Modification)
    //---------------------------------------
    // Pretty print current design
    vhdl_reader.PrettyPrint("design_before_pp_ex3.vhd", 0, "work") ;

    // In parse tree manipulation we need to add or remove port, generic, signal,
    // port map, generic map to block statements. Remove APIs take care of keeping
    // parse-tree consistent, and bringing it back in pre-insertion condition,
    // but in appilications if we want to remove port/generic/signal
    // declaration, we need to remove their references first.

    // Now the following example will try to use all types of Add/Remove APIs.
    // Declarations should be removed only after removing references.

    /*
      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
      |  REMOVE PORT REFS (ACTUALS) FROM BLOCK 'bl' |
      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To remove port refs from port map aspect of block statement the following
    //  procedure is followed

    /******************************************************************************
    1) Get handle to secondary unit (architecture 'arch') where block statement resides.
    2) Get the handle to block statement.
    2) Call routine 'RemovePortRef' to remove each port ref at a time. This routine
       will return 1 on successful removal, otherwise it will return 0.
    *****************************************************************************/

    // First we need to get a handle to the "work" library
    VhdlLibrary *lib = vhdl_reader.GetLibrary("work") ;

    // Get a handle (by name) to the "test" entity that was just analyzed
    VhdlPrimaryUnit *primary_unit = lib ? lib->GetPrimUnit("test"): 0 ;

    // Get a handle (by name) to the "arch" architecture that was just analyzed
    VhdlSecondaryUnit *secondary_unit = primary_unit ? primary_unit->GetSecondaryUnit("arch") : 0 ;

    if (!secondary_unit || !primary_unit) { // "test" and "arch" are not analyzed, don't proceed further
        Message::PrintLine("Primary unit 'test' or secondary unit 'arch' is not yet analyzed. Cannot try Add/Remove APIs") ;
        return 1 ;
    }
    VhdlArchitectureBody *arch = static_cast<VhdlArchitectureBody*>(secondary_unit) ;
    VhdlBlockStatement *block_stmt = 0 ;
    // Iterate over statements of architecture to get block statement
    unsigned i ;
    Array *stmts = (arch) ? arch->GetStatementPart() : 0 ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(stmts, i, stmt) {
        if (!stmt) continue ;
        if (stmt->GetClassId() != ID_VHDLBLOCKSTATEMENT) continue ; // Ignore other statements
        VhdlIdDef *label = stmt->GetLabel() ; // Get label of block statement
        // Compare label name
        if (!label || !Strings::compare(label->Name(), "bl")) continue ;
        block_stmt = static_cast<VhdlBlockStatement*>(stmt) ;
        break ;
    }
    if (!block_stmt) {
        Message::PrintLine("Architecture does not contain block 'bl', cannot try Add/Remove APIs") ;
        return 1 ;
    }
    // Remove actual of formal 'c22'
    unsigned is_removed = block_stmt->RemovePortRef("c22" /* formal */) ;

    if (!is_removed) { // Can't remove port ref for formal 'c22'
        Message::PrintLine("Can't remove port ref for formal 'c22' from block statement") ;
    }

    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |        REMOVE PORT 'c22' FROM BLOCK STATEMENT       |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To remove port declaration from block the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the block from which port 'c22' is to be removed.
    2) Call 'RemovePort' routine to remove port. This routine will return 1 for successful
       removal and 0 for failure.
    ***********************************************************************************/

    // We already have a handle to block statement (block_stmt)

    // Call 'RemovePort' to remove port 'c22'. This routine will remove port declaration
    // of port 'c22' from port clause, remove port identifier from scope and delete port
    // identifier
    is_removed = block_stmt->RemovePort("c22" /* port to be removed */) ;

    if (!is_removed) { // Can't remove port 'c22'
        Message::PrintLine("Cannot remove port 'c22' from block statement") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |           ADD PORT 'rst' TO BLOCK 'bl'              |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To add port declaration to block the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the block where port 'rst' is to be inserted.
    2) Call 'AddPort' routine to add port. This routine will return the created
       port identifier on success and 0 on failure.
    ***********************************************************************************/

    // We already have a handle to block statement

    // Call 'AddPort' routine. This routine will create port identifier, add it to
    // entity scope, create port declaration, add created declaration to port clause.
    VhdlIdDef *new_port = block_stmt->AddPort("rst" /* port name*/, VHDL_in /* mode*/, new VhdlIdRef(Strings::save("bit"))/* subtype indication*/) ;

    if (!new_port) { // Can't add port 'rst'
        Message::PrintLine("Cannot add port 'rst' to block 'bl'") ;
    }

    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |     ADD PORT REF TO BLOCK 'bl'      |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To add port ref to block statement the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the block statement
    2) Call 'AddPortRef' routine. This routine will return the created generic identifier
       on success and 0 on failure.
    ***********************************************************************************/
    // We already have a handle to block statement 'bl'

    // Call 'AddPortRef' routine. This routine will add specified port reference
    // to port association list of block statement
    unsigned is_added = block_stmt->AddPortRef("rst" /* formal */, new VhdlIdRef(Strings::save("in4")) /* actual */) ;

    if (!is_added) { // Can't add port ref
        Message::PrintLine("Cannot add port ref to block 'bl'") ;
    }

    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |    ADD SIGNAL 'in_clk' TO BLOCK 'bl'               |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To add signal declaration to block the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the block where signal 'in_clk' is to be inserted.
    2) Call 'AddSignal' routine. This routine will return the created signal identifier
       on success and 0 on failure.
    ***********************************************************************************/
    // We already have a handle to block 'bl'

    // Call 'AddSignal' routine. This routine will create signal identifier, create
    // signal declaration, type infer created declaration and add it to declarative
    // region of block
    VhdlIdDef *new_signal_id = block_stmt->AddSignal("in_clk" /* signal name*/, new VhdlIdRef(Strings::save("bit")) /* subtype indication*/, 0 /* optional initial value*/) ;

    if (!new_signal_id) { // Can't create signal 'in_clk'
        Message::PrintLine("Cannot add signal 'in_clk' to architecture 'arch'") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |  REMOVE SIGNAL 'tmp' FROM BLOCK 'bl'    |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */
    // To remove signal 'tmp' from block statement the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the block 'bl' where signal 'tmp' resides.
    2) Call 'RemoveSignal' routine. This routine will return 1 on success and 0
       on failure.
    ***********************************************************************************/
    // We already have a handle to block 'bl'

    // Call 'RemoveSignal' routine. This routine will remove particular signal
    // declaration from declarative part of block, remove signal identifier
    // from block's scope and delete signal identifier.
    is_removed = block_stmt->RemoveSignal("tmp" /* signal to be removed */) ;

    if (!is_removed) { // Can't remove signal 'tmp'
        Message::PrintLine("Cannot remove signal 'tmp' from block 'bl'") ;
    }

    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |  REMOVE GENERIC REF FROM BLOCK STATEMENT 'bl' |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To remove generic refs (actuals) from block statement 'bl' the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the block statement 'bl'
    2) Call 'RemoveGenericRef' to remove generic reference. This routine will return 1
       on success and 0 on failure.
    ***********************************************************************************/
    // We already have a handle to block statement 'bl'

    // Call 'RemoveGenericRef' to remove association for generic 'q2'
    is_removed = block_stmt->RemoveGenericRef("q2" /* formal name */) ;

    if (!is_removed) { // Can't remove generic ref for 'q2'
        Message::PrintLine("cannot remove generic ref for formal 'q2' of block 'bl'") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |  REMOVE GENERIC 'q2' FROM BLOCK 'bl'   |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To remove generic from block statement the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the block 'bl' from which generic 'd1' is to be removed.
    2) Call 'RemoveGeneric' to remove generic declaration. This routine will return 1
       on success and 0 on failure.
    ***********************************************************************************/
    // We already have a handle to block 'bl'

    // Call 'RemoveGeneric' routine. This will remove generic declaration from the
    // generic clause of block 'bl', remove generic identifier from block scope
    // and delete the generic identifier.
    is_removed = block_stmt->RemoveGeneric("q2" /* generic to be removed*/) ;

    if (!is_removed) { // Can't remove generic
        Message::PrintLine("Cannot remove generic 'q2' from block 'bl'") ;
    }
    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |     ADD GENERIC 'g1' TO BLOCK 'bl'      |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To add generic 'g1' to block statement the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the block statement where generic 'g1' is to be inserted.
    2) Call 'AddGeneric' routine. This routine will return the created generic identifier
       on success and 0 on failure.
    ***********************************************************************************/
    // We already have a handle to block statement 'bl'

    // Call 'AddGeneric' routine. This routine will create generic identifier, create
    // generic declaration and add created declaration to generic clause of block
    // statement.
    VhdlIdDef *new_generic = block_stmt->AddGeneric("g1" /* generic to be added*/, new VhdlIdRef(Strings::save("integer")) /* subtype indication*/, new VhdlInteger(10)/* initial value*/) ;

    if (!new_generic) { // Can't add generic 'g1'
        Message::PrintLine("Cannot add generic 'g1' to block 'bl'") ;
    }

    /*
       /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
       |     ADD GENERIC REF TO BLOCK 'bl'      |
       \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    */

    // To add generic ref to block statement the following procedure is to be followed:
    /************************************************************************************
    1) Get a handle to the block statement
    2) Call 'AddGenericRef' routine. This routine will return the created generic identifier
       on success and 0 on failure.
    ***********************************************************************************/
    // We already have a handle to block statement 'bl'

    // Call 'AddGenericRef' routine. This routine will add specified generic reference
    // to generic association list of block statement
    is_added = block_stmt->AddGenericRef("g1" /* formal */, new VhdlIdRef(Strings::save("p")) /* actual */) ;

    if (!is_added) { // Can't add generic ref
        Message::PrintLine("Cannot add generic ref to block 'bl'") ;
    }

    //---------------------------------------
    // Pretty Print (Post Parse-tree Modification)
    //---------------------------------------
    // To make sure that everything went as planned, pretty print the current
    // parse-tree's out and compare with the previous pretty printed output.
    vhdl_reader.PrettyPrint("design_after_pp_ex3.vhd", 0, "work");

    return 1;
}
