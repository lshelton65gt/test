/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Array.h"              // Make dynamic array class Array available
#include "Map.h"                // Make associated hash table class Map available
#include "Strings.h"            // A string utility/wrapper class

#include "Message.h"            // Make message handlers available, not used in this example

#include "vhdl_file.h"          // Make VHDL reader available

#include "VhdlUnits.h"          // Definitions of a VhdlLibrary and VhdlDesignUnits and their subclasses
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations constructs
#include "VhdlExpression.h"     // Definitions of all vhdl expresssion constructs
#include "VhdlSpecification.h"  // Definitions of all vhdl specification constructs
#include "VhdlStatement.h"      // Definitions of all vhdl statement constructs
#include "VhdlName.h"           // Definitions of all vhdl name (expression) constructs
#include "VhdlMisc.h"           // Definitions of all extraneous vhdl constructs
#include "VhdlScope.h"          // Symbol table of locally declared identifiers
#include "VhdlIdDef.h"          // Symbol table of locally declared identifiers
#include "vhdl_tokens.h"        // Symbol table of locally declared identifiers

#include "vhdl_parsetree_manip_ex4.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
********************************* VHDL EXAMPLE #1 ***************************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is a parse tree modification example that shows:

        1) How to create internal signals
        2) How to replace signal references
        3) How to add concurrent assignment statements

*****************************************************************************************/

/* ------------------------------------------------------- */
/* ------------------- Program main() -------------------- */
/* ------------------------------------------------------- */

int main()
{
    // Create a vhdl reader object
    vhdl_file vhdl_reader;   // Read the intro in vhdl_file.h to see the details of this object

    // First we need to analyze the VHDL packages.
    // They are located in the "../../vhdl_packages".
    vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");  // store in std library
    vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/std_1164.vhd", "ieee");  // store in ieee library

    // If the design already has "read-from-output" downgrade the severity of the message
    Message::SetMessageType("VHDL-1033", VERIFIC_WARNING) ;

    // Now analyze the example VHDL file "ex4_test.vhd" (into the work library) :
    vhdl_reader.Analyze("ex4_test.vhd", "work") ; // still hard code

    //---------------------------------------
    // Pretty Print (Pre Parse-tree Modification)
    //---------------------------------------
    // Pretty print current design
    vhdl_reader.PrettyPrint("ex4_test_before_change.vhd", 0, "work") ;

    // First we need to get a handle to the "work" library
    VhdlLibrary *lib = vhdl_reader.GetLibrary("work") ;

    // Iterate over all the entities in the library
    MapIter mi ;
    VhdlPrimaryUnit *unit ;
    FOREACH_VHDL_PRIMARY_UNIT(lib, mi, unit) {
        if (!unit) continue ;

        VhdlIdDef *id = unit->Id() ;
        if (!id->IsEntity()) continue ;

        VhdlEntityDecl *entity_decl = static_cast<VhdlEntityDecl*>(unit) ;

        // Now enter all the architectures for the entity
        VhdlSecondaryUnit *sec_unit ;
        MapIter sec_unit_iter ;
        FOREACH_VHDL_SECONDARY_UNIT(unit,sec_unit_iter,sec_unit) {
            if (!sec_unit) continue ;

            // This Map stores the mapping between the output signal
            // and the new _internal signal to be generated for it
            Map output_port_sig_map(POINTER_HASH, 64) ;

            VhdlInterfaceDecl *iface_elem = 0 ;
            unsigned i ;
            FOREACH_ARRAY_ITEM(entity_decl->GetPortClause(),i,iface_elem) {
                if (!iface_elem) continue ;
                if (!iface_elem->IsOutput()) continue ;

                VhdlSubtypeIndication *sti = iface_elem->GetSubtypeIndication() ;

                Array *port_ids = iface_elem->GetIds() ;

                unsigned j ;
                VhdlInterfaceId *iface_id = 0 ;
                FOREACH_ARRAY_ITEM(port_ids,j,iface_id) {
                    if (!iface_id) continue ;

                    const char *name = iface_id->Name() ;
                    char new_name[1024*512] ;
                    sprintf(new_name, "%s_internal", name) ;

                    // Create the _internal signal
                    VhdlIdDef *sig_interal = sec_unit->AddSignal(new_name, sti, 0) ;

                    // Insert it in the map
                    output_port_sig_map.Insert(iface_id, sig_interal) ;
                } // foreach_interface_id
            } // foreach_interface_element

            // Now replace all the references of the output signals
            // inside this architecture body with the corresponding
            // _internal signal
            VhdlOutputReplaceVisitor visitor(&output_port_sig_map) ;
            sec_unit->Accept(visitor) ;

            // Now generate concurrent assignment output <= outpput_internal
            // for each output signals
            MapIter iter ;
            VhdlIdDef *ldef, *rdef ;
            FOREACH_MAP_ITEM(&output_port_sig_map, iter, &ldef, &rdef) {
                VhdlIdRef *lref = new VhdlIdRef(Strings::save(ldef->Name())) ;
                lref->TypeInfer(0, 0, VHDL_SIGUPDATE, sec_unit->LocalScope()) ;
                VhdlIdRef *rref = new VhdlIdRef(Strings::save(rdef->Name())) ;
                rref->TypeInfer(0, 0, VHDL_SIGUPDATE, sec_unit->LocalScope()) ;

                VhdlWaveformElement *waveform_ele = new VhdlWaveformElement(rref, 0) ;
                Array *waveform_list = new Array(1) ;
                waveform_list->InsertLast(waveform_ele) ;

                VhdlConditionalWaveform *cond_waveform = new VhdlConditionalWaveform(waveform_list, 0) ;
                Array *cond_waveforms = new Array(1) ;
                cond_waveforms->InsertLast(cond_waveform) ;

                VhdlConditionalSignalAssignment *assignment = new VhdlConditionalSignalAssignment(lref, 0, cond_waveforms) ;

                sec_unit->AddStatement(assignment) ;
            } // foreach_output_signal_generate_assignment
        } // foreach_architecture_in_entity
    } // foreach_entity_in_library

    vhdl_reader.PrettyPrint("ex4_test_after_change.vhd", 0, "work") ;

    return 1 ;
}

////////////////----- Simple Visitor Application -----//////////////////

VhdlOutputReplaceVisitor::VhdlOutputReplaceVisitor(Map *output_port_sig_map_)
    : output_port_sig_map(output_port_sig_map_)
{
}

VhdlOutputReplaceVisitor::~VhdlOutputReplaceVisitor()
{
    output_port_sig_map = 0 ;
}

// Replace the ref of output with output_internal
void VhdlOutputReplaceVisitor::Visit(VhdlIdRef &node)
{
    if (!output_port_sig_map) return ;

    VhdlIdDef *id_def = node.GetSingleId() ;

    // Get the _internal IdDef for this IdDef
    MapItem *item = output_port_sig_map->GetItem(id_def) ;

    if (!item) return ; // node is not output IdDef

    VhdlIdDef *new_id_def = (VhdlIdDef *)item->Value() ;

    node.SetId(new_id_def) ;
    node.SetName(Strings::save(new_id_def->Name())) ;
    return ;
}
