entity test is
  generic (p : integer := 4) ;
  port (in1, in2 : in bit_vector((p-1) downto 0) ;
        in4 : in bit ;
        out1, out2 : out bit_vector((p-1) downto 0);
        out3 : out bit) ;
end ;

architecture arch of test is

begin
  out1 <= in1 and in2 ;
bl : block 
     generic (q : integer := 4; q2 : integer := 5) ;
     generic map (q=>p, q2=>(p+1)) ;
     port (a1, b1 : in bit_vector((q-1) downto 0) ;
           c11 : out bit_vector((q-1) downto 0);
           c22 : out bit) ;
     port map (a1=>in1, b1=>in2, c11=>out2, c22=>out3) ;
     signal tmp : bit ;
     begin
        c11 <= a1 or b1 ;
     end block;
end ;
