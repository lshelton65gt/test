entity dff is
	generic (d1 : bit) ;
    port (d, rst, set, clk : in bit;
          q : out bit);
end entity dff;

architecture a1 of dff is
begin
   process(clk, rst, set)
   begin
        if(clk'event and clk ='1') then
            if(rst ='1') then
                q <= '0';
            elsif(set = '1') then
                q <= '1';
            else 
                q <= d;
			end if;
		end if;
  end process ;
end architecture;

entity and2 is
    port (a, b : in bit;
          c: out bit); 
end entity and2;

architecture a2 of and2 is
begin
    c <= a and b;
end architecture a2;

entity top is
   generic (ref : bit := '1';
			g: bit);
   port (q : out bit ;
         c : out bit ; -- port to be removed
         d, reset, set, clk : in bit) ;
end ;

architecture arch of top is
signal tmp: bit;
	component and2 
	port (a, b : in bit;
			 c : out bit); 
	end component;
begin
   i1 : entity work.dff generic map (d1 =>ref) port map(d=>d, rst=>reset, set=>set, clk=>clk, q=>q) ;
   i2 : entity work.dff generic map (d1 =>ref) port map(d=>d, rst=>reset, set=>set, clk=>clk, q=>q) ; -- instantiation to be removed
end ;

