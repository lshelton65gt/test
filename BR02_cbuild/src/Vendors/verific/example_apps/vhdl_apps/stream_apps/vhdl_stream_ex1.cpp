/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Message.h"        // Make message handlers available, not used in this example

#include "vhdl_file.h"      // Make VHDL reader available

#include "ZlibStream.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
******************************* VHDL STREAM EXAMPLE #1 **********************************
*****************************************************************************************

                                NOTES

    The following example is for instructional purposes only. The code demonstrates
    how to make Verific tool read from gzipped files.

    Note that to build this application, the compile switch VHDL_FLEX_READ_FROM_STREAM
    must be enabled in vhdl/VhdlCompileFlags.h file.

*****************************************************************************************/

verific_stream *
GetGZippedStream(const char *file_name)
{
    if (!file_name) return 0 ;

    // Return a newly allocated object of 'verific_zip_stream' class:
    verific_stream *zip_stream = new verific_zip_stream(file_name) ;

    if (zip_stream->fail()) {
        // Something bad happned:
        delete zip_stream ;
        zip_stream = 0 ;
    }

    return zip_stream ; // This object will be absorbed by Verific lexer.
}

int main(int argc, char **argv)
{
#ifndef VHDL_FLEX_READ_FROM_STREAM
    #pragma message("This application example requires VHDL_FLEX_READ_FROM_STREAM to be enabled in order to buid.")
    #pragma message("Please enable VHDL_FLEX_READ_FROM_STREAM.")
    Message::PrintLine("This application example requires the compile flag VHDL_FLEX_READ_FROM_STREAM (located in vhdl/VhdlCompileFlags.h) to be active in order to run!") ;
#else
    // Create a vhdl reader object
    vhdl_file vhdl_reader ; // Read the intro in vhdl_file.h to see the details of this object.
    vhdl_reader.SetDefaultLibraryPath("../../../vhdl_packages/vdbs") ;

    // The design "test.v.gz" is to be read in only if the user doesn't specify any other file name.

    char *file_nm ; // Stores the file name

    // If no file name is specified then process the default file name which is "test.v.gz"
    switch (argc) {
    // That is if the user does not specify any optional file name
    case 1: file_nm = "test.vhd.gz" ; break ; // Sets the file name to the default name
    case 2: file_nm = argv[1] ; break ; // Sets file name to name supplied by the user
    default: Message::Error(0, "Too many options.") ; return 1 ;
    }

    // Register the call back routine to return my stream capable of reading from gzipped files:
    vhdl_file::RegisterFlexStreamCallBack(GetGZippedStream) ;

    // Now analyze the VHDL file (into the work library). In case on any error do not process further.
    if (!vhdl_reader.Analyze(file_nm)) return 1 ;

    // Pretty print the parse tree:
    vhdl_reader.PrettyPrint("vhdl_stream_ex1_out.v", 0 /* pretty print all modules */) ;
#endif
    return 0 ; // Status OK.
}

