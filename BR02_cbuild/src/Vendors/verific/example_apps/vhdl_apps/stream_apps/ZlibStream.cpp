/*
 *
 * [ File Version : 1.7 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "ZlibStream.h"

using namespace std ;

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

verific_zip_stream::verific_zip_stream(const char *gzip_file) :
    verific_stream(),
    _file(),
    _error(0)
{
    _file = gzopen(gzip_file, "rb") ;
    if (_file == NULL) _error = 1 ;
}

verific_zip_stream::~verific_zip_stream()
{
    gzclose(_file) ;
}

unsigned
verific_zip_stream::read(char *buf, long max_size)
{
    if (_error) return 0 ;

    *buf = '\0' ;
    _gcount = gzread(_file, buf, max_size) ;
    if (_gcount < 0) _error = 1 ;

    return 1 ;
}

