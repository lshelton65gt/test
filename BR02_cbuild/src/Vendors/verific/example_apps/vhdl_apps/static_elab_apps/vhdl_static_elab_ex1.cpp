/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Map.h"         // Make associated hash table class Map available
#include "VhdlCopy.h"

#include "vhdl_file.h"   // Make vhdl reader available
#include "VhdlUnits.h"   // Make vhdl parse tree structures available for VHDL units
#include "VhdlIdDef.h"   // Make vhdl parse tree structures available for VHDL identifiers

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
******************************* VHDL STATIC ELABORATION EXAMPLE #1 *************************************
*****************************************************************************************

                                NOTE TO THE READER:
    The following is an example which demonstrates the following :

        How to copy a top level entity with different names and elaborate those
        separately with different parameters settings.

    This example makes use of class VhdlMapForCopy and class Map along with CopyPrimaryUnit
    function of VhdlPrimaryUnit class and Elaborate function of vhdl_file .
    CopyPrimaryUnit copies the top level entity and its architectures twice with different names and
    Elaborate function elaborates those with different parameters settings.

*****************************************************************************************/

int main ()
{
    // General purpose Map iterator :
    MapIter mi ;

    // Create a place-holder for the VHDL reader
    vhdl_file vhdl ;

    // Always analyze package 'standard' first, from file 'standard.vhd' :
    vhdl.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd","std") ;

    // The following design  is to be read
    /*----------------ex1_top.vhd------------------------
        entity top is
            generic (M:INTEGER := 6) ;
            port ( INPUT: in BIT_VECTOR(M downto 0);
                 OUTPUT: out BIT ) ;
        end ;

        architecture top_arch of top is
            component REDUCE_NAND_GATE
                    generic (N:INTEGER) ;
                port(A: in BIT_VECTOR(N downto 0); Z:out BIT) ;
            end component ;

                begin
                N1:REDUCE_NAND_GATE generic map(M) port map(INPUT, OUTPUT) ;
        end top_arch;

        entity REDUCE_NAND_GATE is
            generic(N:INTEGER := 2);
            port (A: in BIT_VECTOR(N downto 0); Z:out BIT) ;
        end REDUCE_NAND_GATE ;

        architecture nand_arch of REDUCE_NAND_GATE is

            begin
                process (A) is
              variable temp: BIT ;
                begin
               temp := '0' ;
               for i in A'range loop
                 temp := temp nand A(i) ;
              end loop ;
                   Z <= temp ;
                 end process;

        end nand_arch;
        --------------------------------------------------------*/
    // Now analyze your own VHDL file(s) (into the work library) :
    vhdl.Analyze("ex1_top.vhd") ;

    // Get the current work library
    VhdlLibrary *lib = vhdl.GetWorkLib() ;

    // Get the top level entity "top"
    VhdlPrimaryUnit *entity = lib ? lib->GetPrimUnit("top"): 0 ;
    // First copy of the top level entity - copy_top1
    VhdlMapForCopy old2new1 ;
    VhdlPrimaryUnit *new_entity = entity ? entity->CopyPrimaryUnit("copy_top1", old2new1, 0): 0 ;
    // Add to the library
    if (lib) lib->AddPrimUnit(new_entity) ;

    // Now creating the Map class to pass new parameter settings for copy_top1
    Map *param = new Map(STRING_HASH, 1) ;
    param->Insert("M", "4") ;
    // Elaborate the entity copy_top1
    // The parameter in copy_top1 will get the new value (4) which will be propagated
    // hierarchically downwards.
    vhdl.Elaborate("copy_top1", "work", 0, param, 1) ;

    // Pretty-print the design again, to see the difference between pre
    //  static-elaboration and post static-elaboration ...
    vhdl.PrettyPrint("ex1_top1_pp.vhd", 0) ;

    // Next copy of the top level entity - copy_top2
    VhdlMapForCopy old2new2 ;
    new_entity = entity ? entity->CopyPrimaryUnit("copy_top2", old2new2, 0): 0 ;
    // Add to the library
    if (lib) lib->AddPrimUnit(new_entity) ;

    // Second set of parameter
    param->Insert("M", "16", 1 /* force overwrite */) ;
    // Elaborate the entity copy_top1
    // The parameters in copy_top1 will get the new values (4) which will be propagated
    // hierarchically downwards.
    vhdl.Elaborate("copy_top2", "work", 0, param, 1) ;
    delete param ;

    // Pretty-print the copied design again,
    vhdl.PrettyPrint("ex1_top2_pp.vhd", 0) ;

    return 1 ;
}

