entity top is
  generic (M:INTEGER := 6) ;
  port ( INPUT: in BIT_VECTOR(M downto 0);
         OUTPUT: out BIT ) ;
end ;

architecture top_arch of top is
    component REDUCE_NAND_GATE
        generic (N:INTEGER) ;
        port(A: in BIT_VECTOR(N downto 0); Z:out BIT) ;
    end component ;

    begin
        N1:REDUCE_NAND_GATE generic map(M) port map(INPUT, OUTPUT) ;
end top_arch;

entity REDUCE_NAND_GATE is
    generic(N:INTEGER := 2);
    port (A: in BIT_VECTOR(N downto 0); Z:out BIT) ;
end REDUCE_NAND_GATE ;

architecture nand_arch of REDUCE_NAND_GATE is

begin
    process (A) is
      variable temp: BIT ;
    begin
       temp := '0' ;
       for i in A'range loop
           temp := temp nand A(i) ;
       end loop ;
       Z <= temp ;
    end process;
        
end nand_arch;
