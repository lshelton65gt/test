module top (input [3:0] in, output [3:0] o1, o2, output [4:0]o3, o4, input [4:0] in2) ; // top-level module
   
    child I(o1, o2, in);
    child #(4) I2(o3, o4, in2);

    test #(3) I3() ; 
    test #(2) I4() ;
endmodule 
    
module test ;
parameter p = 10 ;
wire [3:0] o1, o2 ;
wire [4:0] o3, o4 ;
reg [3:0] in ;
reg [4:0] in2 ;

    child #(3) I(o1, o2, in);
    child #(4) I2(o3, o4, in2);
endmodule

module xor_gate (ModIn1, ModIn2, ModOut); // Instantiated in 'comp' entity in  Vhdl file test.vhd
     parameter p = 10 ;
     input [3:0]ModIn1;
     input [3:0]ModIn2;
     output ModOut;
endmodule

