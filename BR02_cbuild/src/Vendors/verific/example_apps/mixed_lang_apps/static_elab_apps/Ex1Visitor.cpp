/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>  // for strchr
#include "Ex1Visitor.h"     // Visitor base class definition
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriExpression.h" // Definitions of class VeriName
#include "VeriModule.h"     // Definitions of class VeriModule
#include "VhdlStatement.h"  // Definitions of all vhdl entity item tree nodes
#include "VhdlName.h"       // Definition of VhdlName
#include "VhdlUnits.h"      // Definition of VhdLibrary
#include "VhdlIdDef.h"      // Definition of VhdLibrary
#include "Strings.h"        // Definition of class to manipulate copy, concatenate, create etc...
#include "vhdl_file.h"
#include "veri_file.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;//  { // start definitions in verific namespace
#endif

/*-----------------------------------------------------------------*/
//               Local Static Function
/*-----------------------------------------------------------------*/
// To make escaped verilog names legal, the VhdlTreeNode::CreateLegalIdentifier
// routine is called which converts these names to legal Vhdl names. The resultant
// names however, are not legal Verilog names.
// The following function takes in a name and if it is enclosed withing back-slashes
// i.e is of the form "\\string\\" then removes these back-slashes and returns a legal
// Verilog name. If the input is already a legal Verilog name then it returns a
// string after copying the input name to it.
static char* CreateLegalVerilogIdentifier(const char *str) {
    if (!str) return 0 ; // Nothing to do

    char *ret_str = Strings::save (str) ; // Copy the same string

    // First check if str already contains a back-slash^M
    if (strchr(str, '\\')) {
        const char *p = str ;
        char *tmp = ret_str ;

        while(*p) {
            if (*p == '\\') p++ ; // Skip any other back-slash character as the VhdlTreeNode::CreateLegalIdentifier routine introduces a back-slash character for one already present
            *(tmp++) = *(p++) ;
        }

        if (*(tmp-1) == '\\') {
            *(tmp-1) = '\0' ; // Skip terminating '\' if any
        } else {
            tmp = '\0' ; // Terminate the string
        }
    }
    return ret_str ;
}

/*-----------------------------------------------------------------*/
//                Constructors / Destructors
/*-----------------------------------------------------------------*/
NewVerilogVisitor::NewVerilogVisitor(Set *master_modules, Set *master_entities)
    : _master_modules(master_modules),
      _master_entities(master_entities)
{}

NewVerilogVisitor::~NewVerilogVisitor()
{}

NewVhdlVisitor::NewVhdlVisitor(Set *master_modules, Set *master_entities)
    : _master_modules(master_modules),
      _master_entities(master_entities)
{}

NewVhdlVisitor::~NewVhdlVisitor()
{}

/*-----------------------------------------------------------------*/
//                      Visit Methods
/*-----------------------------------------------------------------*/

void NewVerilogVisitor::Visit(VeriModuleInstantiation &node)
{
    VeriModule *master = node.GetInstantiatedModule() ;  // Find the instantiated VeriModule

    if (!master) { // If the master of the instantiation does not reside in the Verilog design then
        // get the name of the instantiation and call the Accept routine on it
        VeriName *veri_name = node.GetModuleNameRef() ;
        const char *mod_name = (veri_name) ? veri_name->GetName() : 0 ; // Get the instance name
        VhdlLibrary *vhdl_lib = vhdl_file::GetWorkLib() ; // Get the current vhdl library

        VhdlPrimaryUnit *entity = vhdl_lib ? vhdl_lib->GetPrimUnit(mod_name) : 0 ; // Search for mod_name in current vhdl library
        if (entity) { // The master entity entity resides in the Vhdl design
            NewVhdlVisitor new_vhdl_visitor(_master_modules, _master_entities) ;
            entity->Accept(new_vhdl_visitor) ; // Traverse the entity in the Vhdl design
            _master_entities->Insert(entity) ; // Insert the primary unit in the set
        }
    }
    if (master) {
        master->Accept(*this) ; // Traverse the members of the master module in the Verilog design
        _master_modules->Insert(master) ; // Insert the master module in a set
    }
}

void NewVhdlVisitor::Visit(VhdlComponentInstantiationStatement &node)
{
    VhdlIdDef *master_id = node.GetInstantiatedUnit() ;
    if (master_id) {
        VhdlName *vhdl_name = node.GetInstantiatedUnitNameNode() ;
        VhdlLibrary *vhdl_lib = vhdl_file::GetWorkLib() ; // Get the current vhdl library
        const char *entity_name = (vhdl_name) ? vhdl_name->Name() : 0 ;
        VhdlPrimaryUnit *entity = vhdl_lib ? vhdl_lib->GetPrimUnit(entity_name) : 0 ; // Search for entity_name in current vhdl library
        if (!master_id->IsEntity() && !master_id->IsConfiguration()) { // If the master_id is neither an entity nor a configuration then
                                                                       // look for its master in the Vhdl library
            // Convert Vhdl name to legal Verilog name
            char *veri_name = CreateLegalVerilogIdentifier (entity_name) ;
            // If entity_name is not found in the Vhdl library then look for it in the Verilog design
            VeriModule *mod = (!entity) ? veri_file::GetModule(veri_name,1) : 0 ;
            Strings::free(veri_name) ; // Free the newly allocated string

            if (mod) { // The master module mod resides in the Verilog design
                NewVerilogVisitor new_veri_visitor(_master_modules, _master_entities) ;
                mod->Accept(new_veri_visitor) ; // Traverse the master module
                _master_modules->Insert(mod) ; // Store the master module
            }
        }
        if(entity) {
            _master_entities->Insert(entity) ; // Insert the master entity in the set for entities
            entity->Accept(*this) ;  // Traverse the members of the master entity in the Vhdl design
        }
    }
}

