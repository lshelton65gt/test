/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <fstream>
using namespace std ;
#include "Set.h"            // Make class Set available
#include "Message.h"        // Make message handlers available
#include "veri_file.h"      // Make verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definition of VeriName
#include "Array.h"          // Make class Array available
#include "VeriId.h"         // Definitions of all verilog identifier nodes
#include "Strings.h"        // Definition of class to manipulate copy, concatenate, create etc...
#include "Ex1Visitor.h"     // Make visitor pattern available
#include "vhdl_file.h"      // Make verilog reader available
#include "VhdlStatement.h"  // Make verilog reader available
#include "VhdlName.h"       // Definition of VhdlName
#include "VhdlUnits.h"      // Definition of VhdLibrary

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
********************** MIXED LANGUAGE STATIC ELABORATION EXAMPLE #1 ****************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is an example which demonstrates the following :

    How to analyze and statically elaborate mixed language designs and traverse the
    hierarchy.
    This application takes in a list of verilog and vhdl files (with .v and .vhd
    extensions respectively), the top-level unit preceded by the "-top" option and
    another keyword (verilog/vhdl) indicating whether the top-level unit resides in
    the verilog or in the vhdl design.
    On successfull processing it writes the elaborated Verilog modules in a file named
    out.v and the Vhdl units in out.vhd, that are contained in the hierarchy owned by
    the top-level unit.

    Eg. :

      (i)  To make this example run with a verilog module as the top-level unit use a
           command similar like the one below:

           mixed_static_elab_ex1-linux ex1_test.v ex1_test.vhd -top top -verilog

           Here ex1_test.v and ex1_test.vhd are the input design files, top is the top-level
           verilog module and the last option '-verilog' specifies that the top-level
           unit is a verilog module named 'top'.

      (ii) To make this example run with a vhdl entity as the top-level unit use a
           command similar to the one below:

           mixed_static_elab_ex1-linux ex1_test.v ex1_test.vhd -top comp -vhdl

           Here ex1_test.v and ex1_test.vhd are the input design files, top is the top-level
           vhdl unit and the last option '-vhdl' specifies that the top-level unit is
           a vhdl unit named 'comp'.

*****************************************************************************************/

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    // Create a vhdl reader object
    vhdl_file vhdl_reader ; // Read the intro in vhdl_file.h to see the details of this object.

    const char *top_design_unit_name = 0 ;

    // Initialize the top module flags.
    int is_top_vhdl = 0 ;
    int is_top_verilog =0 ;

    if (argc>4) {
        // Look at the input files and see if they are verilog or vhdl
        // (verilog files should end in .v while vhdl files in .vhd)
        // If Vhdl then analyze in Vhdl or else do so in Verilog.
        for (int i=1; i<=argc-4; i++) {
            if (strstr(argv[i], ".vhd")) { // If true then the file is a vhdl design
                // Always analyze package 'standard' first, from file 'standard.vhd' :
                vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");   // store in std library
                vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/std_1164.vhd", "ieee");  // store in ieee library
               // Analyze the design (in vhdl mode) Return in case of failure.
                if (!vhdl_reader.Analyze(argv[i])) return 1 ;
            } else if (strstr(argv[i], ".v")) { // If true then the file is a verilog design
                // Analyze the design (in verilog2k mode). Return in case of failure.
                if (!veri_reader.Analyze(argv[i], 1/* Verilog2K Mode*/)) return 1 ;
            } else {
                printf("File %s has got an invalid extension. \n", argv[i]) ;
            }
        }

        // The penultimate argument should be  the top-level unit preceded by the '-top'
        // option. Check that.
        if (Strings::compare(argv[argc-3], "-top")) {
            top_design_unit_name  = argv[argc-2] ;
            // Look at the last argument to determine if the top unit resides in verilog or in vhdl design
            // and set the flag accordingly.
            if (Strings::compare(argv[argc-1], "-verilog")) is_top_verilog = 1 ;
            if (Strings::compare(argv[argc-1], "-vhdl")) is_top_vhdl = 1 ;

            if (!is_top_vhdl && !is_top_verilog) { // If none of the flags is set then the option specified is wrong
                Message::Msg(VERIFIC_ERROR, 0, 0, "the last argument can be either -verilog or -vhdl") ;
                return 0 ;
            }

            // Set the top module flags properly. They indicate as to whether the top
            // module is a Verilog module or a Vhdl entity.
            if (Strings::compare(argv[4], "vhdl")) {
                is_top_vhdl = 1 ;
            } else if (Strings::compare(argv[4], "verilog")) {
                is_top_verilog = 1 ;
            }
        } else { // If the "-top" option has not been specified
            Message::Msg(VERIFIC_ERROR, 0, 0, "the top-level units has to specified preceded by the -top option") ;
            return 0 ;
        }
    } else { // If the number of arguments is lesser than 6 then one or more arguments are missing. So assign default values.
        Message::PrintLine("Default input file : ex1_test.v ex1_test.vhd. Default top-level unit: top. \nSpecify command-line argument to override default values") ;

        // Analyze the default files.
        if (!veri_reader.Analyze("ex1_test.v", 1/* Verilog2K Mode*/)) return 1 ; // Analyze the default verilog file viz. 'ex1_test.v'.
        // Always analyze package 'standard' first, from file 'standard.vhd' :
         vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");   // store in std library
         vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/std_1164.vhd", "ieee");  // store in ieee library
        // Analyze the design (in vhdl mode). Return in case of failure.
        if (!vhdl_reader.Analyze("ex1_test.vhd")) return 1 ; // Analyze the default vhdl file viz. 'ex1_test.vhd'.
        is_top_verilog = 1 ; // By default the top module is a Verilog module.
        top_design_unit_name = "top" ; // Set the top design unit name to the Verilog top level module.
    }

    // Get the current work library  for Vhdl.
    VhdlLibrary *lib = vhdl_reader.GetWorkLib() ;

    // The following sets act as containers for those verilog modules and vhdl
    // units that are part of the hierarchy owned by the top-level unit.
    Set master_mods(POINTER_HASH) ;
    Set master_entities(POINTER_HASH) ;

    if (is_top_verilog) { // If the top level unit is a Verilog module
        // Statically elaborate all the Verilog modules. Return if any error shows up.
        if (!veri_reader.ElaborateStatic(top_design_unit_name)) return 0 ; // statically elaborates all verilog modules in the "work" libarary

        VeriModule *top_module = veri_reader.GetModule(top_design_unit_name) ; // Get the pointer to the top-level module
        if (!top_module) return 0 ; // Exit from application if there is no top module by the given name in the given Verilog designs
        master_mods.Insert(top_module) ; // Insert the top-level module
        NewVerilogVisitor veri_visitor(&master_mods, &master_entities) ; // Create an object of the custom-made class derived from VeriVisitor
        top_module->Accept(veri_visitor) ; // Use Visitor Pattern to collect the master modules and entities in the Verilog and Vhdl designs
    } else if (is_top_vhdl) {
        // Statically elaborate all Vhdl design units. Return if any error shows up.
        if (!vhdl_reader.Elaborate(top_design_unit_name, "work", 0, 0, 1)) return 0 ; // statically elaborates all vhdl modules in the "work" libarary

        VhdlPrimaryUnit *top_entity = 0 ;
        top_entity = lib ? lib->GetPrimUnit(top_design_unit_name) : 0 ; // Get the top-level entity
        if (!top_entity) return 0 ; // Exit from application if there is no top entity by the given name in the given Vhdl design
        master_entities.Insert(top_entity) ; // Insert the top-level entity
        NewVhdlVisitor vhdl_visitor (&master_mods, &master_entities) ;
        top_entity->Accept(vhdl_visitor) ; // Use Visitor Pattern to collect the master modules and entities in the Verilog and Vhdl designs
    }

    VhdlEntityDecl *entity ;
    SetIter si1 ;
    if (master_entities.Size()) { // Open a file stream for output only if the size of the set is non-zero.
        ofstream vhdl_out("out.vhd") ; // Open a filestream for writing
        FOREACH_SET_ITEM(&master_entities, si1, &entity) { // Iterate through all the vhdl entities collected by the visitor patterns
            if(!entity) continue ;
            entity->PrettyPrint(vhdl_out, 0) ; // Pretty print the entity in the file "out.vhd"
        }
    }

    SetIter si2 ;
    VeriModule *mod ;
    if (master_mods.Size()) { // Open a file stream for output only if the size of the set is non-zero.
        ofstream veri_out("out.v") ; // Open a filestream for writing
        FOREACH_SET_ITEM(&master_mods, si2, &mod) { // Iterate through all the vhdl entities collected by the visitor patterns
            if (!mod) continue ;
            mod->PrettyPrint(veri_out, 0) ; // Pretty print the entity in the file "out.v"
        }
    }

    // All done.  Wasn't that easy!

    return 1 ;
}

