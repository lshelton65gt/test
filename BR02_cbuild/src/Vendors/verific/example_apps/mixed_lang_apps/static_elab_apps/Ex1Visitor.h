/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_NEW_VISITOR_H_
#define _VERIFIC_VERI_NEW_VISITOR_H_

#include "Set.h"
#include "VeriVisitor.h"    // Verilog Visitor base class definition
#include "VhdlVisitor.h"    // Vhdl Visitor base class definition

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------------------- */

class NewVerilogVisitor : public VeriVisitor
{
public:
    NewVerilogVisitor(Set *master_modules, Set *master_entities) ;
    virtual ~NewVerilogVisitor() ;
    virtual void Visit(VeriModuleInstantiation &node) ;
    // Accessor method
    Set* GetMasterEntities()         { return _master_entities ; }
    Set* GetMasterModules()          { return _master_modules ; }
private:
    Set *_master_modules ;       // Container (Set) of VeriModule*'s
    Set *_master_entities ;      // Container (Set) of VhdlEntityDecl*'s
} ;

/* -------------------------------------------------------------------------- */

class NewVhdlVisitor : public VhdlVisitor
{
public:
    NewVhdlVisitor(Set *master_modules, Set *master_entities) ;
    virtual ~NewVhdlVisitor() ;
    virtual void Visit(VhdlComponentInstantiationStatement &node) ;
    // Accessor method
    Set* GetMasterModules()          { return _master_modules ; }
    Set* GetMasterEntities()         { return _master_entities ; }
private:
    Set *_master_modules ;      // Container (Set) of VerilogModules
    Set *_master_entities ;     // Container (Set) of VhdlEntityDecl*'s
} ;

/* -------------------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_NEW_VISITOR_H_
