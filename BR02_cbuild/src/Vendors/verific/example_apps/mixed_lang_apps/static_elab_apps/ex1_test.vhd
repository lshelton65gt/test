entity comp is -- top-level unit

   port(X, Y: in BIT_VECTOR(3 DOWNTO 0);
        Sum, Carry: out BIT);

end;

architecture Structure of comp is

    component xor_gate is
        generic (p : integer) ;
        port (CompIn1, CompIn2: in BIT_VECTOR(3 DOWNTO 0);
              CompOut : out BIT);
    end component;

-- xor_gate is a Verilog module with port names not as CompIn1, CompIn2 and CompOut.

    --for L1: xor_gate use entity WORK.xor_gate
            --port map (CompIn1, CompIn2, OPEN);

begin

   L1: xor_gate generic map (4)
       --port map (X, Y, Sum);
       port map (CompIn1 => X, CompIn2 => Y, CompOut => Sum);

end;


entity child is -- instantiated in test module in Verilog design
   generic (p : integer := 3);
   port (S1, S2: out bit_vector (p downto 0);
         I1 : in bit_vector (p downto 0));
end ;

architecture arch of child is
begin
    S1 <= I1 ;
    S2 <= not I1 ;
end ;

