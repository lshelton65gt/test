/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Set.h"            // Make class Set available
#include "Message.h"        // Make message handlers available
#include "veri_file.h"      // Make verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definition of VeriName
#include "Array.h"          // Make class Array available
#include "VeriId.h"         // Definitions of all verilog identifier nodes
#include "VeriScope.h"      // Definition of VhdLibrary
#include "Strings.h"        // Definition of class to manipulate copy, concatenate, create etc...
#include "Ex1Visitor.h"     // Make visitor pattern available
#include "vhdl_file.h"      // Make verilog reader available
#include "VhdlIdDef.h"      // Make verilog reader available
#include "VhdlScope.h"      // Definition of VhdLibrary
#include "VhdlStatement.h"  // Make verilog reader available
#include "VhdlName.h"       // Definition of VhdlName
#include "VhdlUnits.h"      // Definition of VhdLibrary

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
********************** MIXED LANGUAGE STATIC ELABORATION EXAMPLE #2 ****************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is an example which demonstrates the following :

    How to analyze and statically elaborate mixed language designs and traverse the
    hierarchy.
    This application takes in a list of verilog and vhdl files (with .v and .vhd
    extensions respectively), the top-level unit preceded by the "-top" option and
    another keyword (verilog/vhdl) indicating whether the top-level unit resides in
    the verilog or in the vhdl design.

    Eg. :

      (i)  To make this example run with a verilog module as the top-level unit use a
           command similar like the one below:

           mixed_static_elab_ex2-linux ex2_test.v ex2_test.vhd -top top -verilog

           Here ex2_test.v and ex2_test.vhd are the input design files, top is the top-level
           verilog module and the last option '-verilog' specifies that the top-level
           unit is a verilog module named 'top'.

      (ii) To make this example run with a vhdl entity as the top-level unit use a
           command similar to the one below:

           mixed_static_elab_ex2-linux ex2_test.v ex2_test.vhd -top comp -vhdl

           Here ex2_test.v and ex2_test.vhd are the input design files, top is the top-level
           vhdl unit and the last option '-vhdl' specifies that the top-level unit is
           a vhdl unit named 'comp'.

*****************************************************************************************/

// Routines for traversing the module/entity instances in DFS order and print them.
static void TraverseVerilog(VeriModule *top) ;
static void TraverseVhdl(VhdlPrimaryUnit *top, const char *arch_name) ;

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    // Create a vhdl reader object
    vhdl_file vhdl_reader ; // Read the intro in vhdl_file.h to see the details of this object.

    const char *top_design_unit_name = 0 ;

    // Initialize the top module flags.
    int is_top_vhdl = 0 ;
    int is_top_verilog =0 ;

    if (argc>4) {
        // Look at the input files and see if they are verilog or vhdl
        // (verilog files should end in .v while vhdl files in .vhd)
        // If Vhdl then analyze in Vhdl or else do so in Verilog.
        for (int i=1; i<=argc-4; i++) {
            if (strstr(argv[i], ".vhd")) { // If true then the file is a vhdl design
                // Always analyze package 'standard' first, from file 'standard.vhd' :
                vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");   // store in std library
                vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/std_1164.vhd", "ieee");  // store in ieee library
                vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/mixed_lang_vltype.vhd", "vl");  // store in ieee library
               // Analyze the design (in vhdl mode) Return in case of failure.
                if (!vhdl_reader.Analyze(argv[i])) return 1 ;
            } else if (strstr(argv[i], ".v")) { // If true then the file is a verilog design
                // Analyze the design (in verilog2k mode). Return in case of failure.
                if (!veri_reader.Analyze(argv[i], 1/* Verilog2K Mode*/)) return 1 ;
            } else {
                printf("File %s has got an invalid extension. \n", argv[i]) ;
            }
        }

        // The penultimate argument should be  the top-level unit preceded by the '-top'
        // option. Check that.
        if (Strings::compare(argv[argc-3], "-top")) {
            top_design_unit_name  = argv[argc-2] ;
            // Look at the last argument to determine if the top unit resides in verilog or in vhdl design
            // and set the flag accordingly.
            if (Strings::compare(argv[argc-1], "-verilog")) is_top_verilog = 1 ;
            if (Strings::compare(argv[argc-1], "-vhdl")) is_top_vhdl = 1 ;

            if (!is_top_vhdl && !is_top_verilog) { // If none of the flags is set then the option specified is wrong
                Message::Msg(VERIFIC_ERROR, 0, 0, "the last argument can be either -verilog or -vhdl") ;
                return 0 ;
            }

            // Set the top module flags properly. They indicate as to whether the top
            // module is a Verilog module or a Vhdl entity.
            if (Strings::compare(argv[4], "vhdl")) {
                is_top_vhdl = 1 ;
            } else if (Strings::compare(argv[4], "verilog")) {
                is_top_verilog = 1 ;
            }
        } else { // If the "-top" option has not been specified
            Message::Msg(VERIFIC_ERROR, 0, 0, "the top-level units has to specified preceded by the -top option") ;
            return 0 ;
        }
    } else { // If the number of arguments is lesser than 6 then one or more arguments are missing. So assign default values.
        Message::PrintLine("Default input file : ex2_test.v ex2_test.vhd. Default top-level unit: top. \nSpecify command-line argument to override default values") ;

        // Analyze the default files.
        if (!veri_reader.Analyze("ex2_test.v", 1/* Verilog2K Mode*/)) return 1 ; // Analyze the default verilog file viz. 'ex2_test.v'.
        // Always analyze package 'standard' first, from file 'standard.vhd' :
         vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");   // store in std library
         vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/std_1164.vhd", "ieee");  // store in ieee library
        // Analyze the design (in vhdl mode). Return in case of failure.
        if (!vhdl_reader.Analyze("ex2_test.vhd")) return 1 ; // Analyze the default vhdl file viz. 'ex2_test.vhd'.
        is_top_verilog = 1 ; // By default the top module is a Verilog module.
        top_design_unit_name = "top" ; // Set the top design unit name to the Verilog top level module.
    }

    // Get the current work library  for Vhdl.
    VhdlLibrary *lib = vhdl_reader.GetWorkLib() ;

    if (is_top_verilog) { // If the top level unit is a Verilog module
        // Statically elaborate all the Verilog modules. Return if any error shows up.
        if (!veri_reader.ElaborateStatic(top_design_unit_name)) return 0 ; // statically elaborates all verilog modules in the "work" libarary

        VeriModule *top_module = veri_reader.GetModule(top_design_unit_name) ; // Get the pointer to the top-level module
        if (!top_module) return 0 ; // Exit from application if there is no top module by the given name in the given Verilog designs
        Message::PrintLine("\n") ;
        Message::PrintLine("Top level module : ", top_module ? top_module->Name() : 0) ;
        Message::PrintLine("\n") ;
        TraverseVerilog(top_module) ; // Traverse top level module and the hierarchy under it
    } else if (is_top_vhdl) {
        // Statically elaborate all Vhdl design units. Return if any error shows up.
        if (!vhdl_reader.Elaborate(top_design_unit_name, "work", 0, 0, 1)) return 0 ; // statically elaborates all vhdl modules in the "work" libarary

        VhdlPrimaryUnit *top_entity = 0 ;
        top_entity = lib ? lib->GetPrimUnit(top_design_unit_name) : 0 ; // Get the top-level entity
        if (!top_entity) return 0 ; // Exit from application if there is no top entity by the given name in the given Vhdl design
        Message::PrintLine("\n") ;
        Message::PrintLine("Top level unit : ", top_entity ? top_entity->Name() : 0) ;
        Message::PrintLine("\n") ;
        TraverseVhdl(top_entity, 0 /*architecture name*/) ; // Traverse top level unit and the hierarchy under it
    }
    return 1 ;
}

// Traverse a module and the hierarchy under it:
static void
TraverseVerilog(VeriModule *module)
{
    if (!module) return ;
    // Get the scope of the module:
    VeriScope *scope = module->GetScope() ;
    // Find all the declared ids in this scope:
    Map *ids = scope ? scope->DeclArea() : 0 ;
    MapIter mi ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(ids, mi, 0, &id) { // Traverse declared ids
        if (!id || !id->IsInst()) continue ; // Consider only the instance ids
        VeriModuleInstantiation *mod_inst = id->GetModuleInstance() ; // Take the moduleinstance
        VeriModule *mod = mod_inst ? mod_inst->GetInstantiatedModule() : 0 ; // The module instance is a module
        Message::PrintLine("Processing instance : ", id->Name()) ;
        if (mod) { // This is verilog module insatantiation: need to go into that module
            Message::PrintLine("instantiated module : ", mod->Name()) ;
            Message::PrintLine("\n") ;
            TraverseVerilog(mod) ; // Traverse the instantiated module
        } else { // The module instance is a vhdl unit instance: need to go into that unit
            VeriName *instantiated_unit_name = mod_inst ? mod_inst->GetModuleNameRef() : 0 ;
            const char *entity_name = instantiated_unit_name ? instantiated_unit_name->GetName() : 0 ;

            // Find the lib_name from the attribute of the instance
            VeriExpression *attr = mod_inst ? mod_inst->GetAttribute(" vhdl_unit_lib_name") : 0 ;
            char *lib_name = attr ? attr->Image() : 0 ;
            // Image() routine on attr returns the string image of the VeriConsVal expression of attr
            // ie "/work/" so we take only the name of the lib "work" trancting the other portion:
            char *lib = lib_name ;
            if (lib && lib[0]=='"') {
                lib++ ;
                lib[Strings::len(lib)-1] = 0 ;
            }
            // Find the vhdl unit for verilog instance
            char *arch_name = 0 ;
            VhdlPrimaryUnit *vhdl_unit = VeriNode::GetVhdlUnitForVerilogInstance(entity_name, lib, 0/*search_from_loptions*/, &arch_name) ;
            Strings::free(lib_name) ;
            if (!vhdl_unit) {
                Message::PrintLine("black-box instance") ;
                continue ;
            }
            Message::PrintLine("instantiated vhdl unit : ", vhdl_unit->Name()) ;
            if (arch_name) Message::PrintLine("architecture name : ", arch_name) ;
            Message::PrintLine("\n") ;
            TraverseVhdl(vhdl_unit, arch_name) ; // Traverse instantiated unit
        }
    }
}

// Traverse an entity and the hierarchy under it:
static void
TraverseVhdl(VhdlPrimaryUnit *unit, const char *arch_name)
{
    // Find the architecture of the unit:
    VhdlSecondaryUnit *arch = unit? unit->GetSecondaryUnit(arch_name) : 0 ;
    // Find the scope:
    VhdlScope *scope = arch ? arch->LocalScope() : 0 ;

    // Get the declared ids from that scope:
    Map *ids = scope ? scope->DeclArea() : 0 ;
    MapIter mi ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(ids, mi, 0, &id) {
        VhdlStatement *stmt = id ? id->GetStatement() : 0 ;
        VhdlIdDef *inst_unit = stmt ? stmt->GetInstantiatedUnit() : 0 ; // Get the instantiated unit id for instances only
        if (!inst_unit) continue ; // Not an instance level
        Message::PrintLine("Processing instance : ", id->Name()) ;
        if (inst_unit->IsComponent() || inst_unit->IsConfiguration()) {
             Message::PrintLine("black-box instance") ;
             continue ; // black-box or not bind yet
        }

        if (inst_unit->IsEntity()) { // This is direct entity insatantiation
            VhdlPrimaryUnit *prim_unit = inst_unit ? inst_unit->GetPrimaryUnit() : 0 ; // Get the primary unit
            if (!prim_unit) continue ;

            if (prim_unit->IsVerilogModule()) { // instance of verilog module
                // Get instantiated verilog module
                VeriModule *veri_module = vhdl_file::GetVerilogModuleFromlib(inst_unit->GetContainingLibraryName(), inst_unit->Name()) ;
                if (!veri_module) continue ;
                Message::PrintLine("instantiated module : ", veri_module->Name()) ;
                Message::PrintLine("\n") ;
                TraverseVerilog(veri_module) ; // Traverse instantiated module
            } else { // Instance of vhdl unit
                // Find the architecture name and traverse the vhdl design
                VhdlName *inst_name = stmt ? stmt->GetInstantiatedUnitNameNode() : 0 ;
                const char *arch_name = inst_name ? inst_name->ArchitectureNameAspect() : 0 ;
                Message::PrintLine("instantiated vhdl unit : ", prim_unit->Name()) ;
                Message::PrintLine("architecture name : ", arch_name ? arch_name : 0) ;
                Message::PrintLine("\n") ;
                TraverseVhdl(prim_unit, arch_name) ; // Traverse the vhdl unit
            }
        }
    }
}
