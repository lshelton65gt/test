/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

// Verific utilities
#include "Array.h"          // Make class Array available
#include "Set.h"            // Make class Set available
#include "Message.h"        // Make message handlers available
#include "Strings.h"        // Definition of class to manipulate copy, concatenate, create etc...

// Verific command line interface
#include "Commands.h"       // Make command line interface available
#include "ComRead.h"        // Using Verific command 'analyze' as template for command line parsing

// Verific Verilog parser
#include "veri_file.h"      // Make verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definition of VeriName
#include "VeriId.h"         // Definitions of all verilog identifier nodes
#include "VeriScope.h"      // Definition of VhdLibrary

// Verific VHDL parser
#include "vhdl_file.h"      // Make verilog reader available
#include "VhdlIdDef.h"      // Make verilog reader available
#include "VhdlScope.h"      // Definition of VhdLibrary
#include "VhdlStatement.h"  // Make verilog reader available
#include "VhdlName.h"       // Definition of VhdlName
#include "VhdlUnits.h"      // Definition of VhdLibrary

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
********************** MIXED LANGUAGE STATIC ELABORATION EXAMPLE #2 ****************************
*****************************************************************************************

                                NOTE TO THE READER:

    The following is an example which demonstrates the following :

    How to analyze and statically elaborate mixed language designs and traverse the
    hierarchy.
    This application takes in a list of verilog and vhdl files (with .v and .vhd
    extensions respectively), the top-level unit preceded by the "-top" option.

    This application will automatically determine if the top-level unit is a
    VHDL entity or a Verilog module, and will set that as the top level design from
    which to start traversal of the hierarchy.

    Eg. :

      (i)  To make this example run with a verilog module as the top-level unit use a
           command similar like the one below:

           traverse_hierarchy-linux ex2_test.v ex2_test.vhd -top top

      (ii) To make this example run with a vhdl entity as the top-level unit use a
           command similar to the one below:

           traverse_hierarchy-linux ex2_test.v ex2_test.vhd -top comp

    Apart from the required file name arguments, and the -top option, this application
    also accepts the a large number of options that guide design analysis.

    Call the compiled command line application with -help option for a full overview of
    all the options this application accepts and processes.

*****************************************************************************************/

// Routines for traversing the module/entity instances in DFS order and print them.
static void PrintDesignUnit(const VhdlStatement *stmt) ;
static void TraverseVerilog(const VeriModule *top) ;
static void TraverseVhdl(const VhdlPrimaryUnit *top, const char *arch_name) ;

int main(int argc, const char **argv)
{
    // Set-up main line command line.
    // This makes all options on the TCL command "analyze" available on this main command line.
    Command *mainline = new ComAnalyze() ;

    // Add the following options used in this application :
    mainline->Add(new StringArg("lib_path","path (directory) for VHDL standard libraries")) ;
    mainline->Add(new StringArg("top","name of top-level module",0/*not optional*/)) ;

    // Default command line if no arguments given :
    const char *default_argv[6] = {"traverse_hierarchy","ex2_test.v","ex2_test.vhd","-top","top",0} ;
    if (argc==1) {
        argc = 5 ;
        argv = default_argv ;
    }

    // Now parse the command line
    if (!mainline->ParseArgs(argc, argv)) {
        mainline->Usage() ;
        delete mainline ;
        return 1 ; /* Command line failed */
    }
    // Check if we need to print help (usage)
    if (mainline->BoolVal("help")) {
        mainline->Usage() ;
        delete mainline ;
        return 0 ; // success. User only wanted 'help' on command line options
    }

    // Set Default library path (to search for VHDL standard packages) :
    const char *lib_path = mainline->StringVal("lib_path") ;
    if (!lib_path) lib_path = "../../../vhdl_packages/vdbs" ;

    // Note : this command will fail to compile if you don't have binary save/restore on VHDL.
    vhdl_file::SetDefaultLibraryPath(lib_path) ;

    // Same for SystemVerilog
//    veri_file::SetDefaultLibraryPath(lib_path) ;

    // Parse the 'analyze' command :
    if (!mainline->Process(0/*no tcl interpreter needed*/)) {
        mainline->Usage() ;
        delete mainline ;
        return 2 ; // failure in analysis
    }

    // Now get top design name :
    const char *top_name = mainline->StringVal("top") ;
    VERIFIC_ASSERT(top_name) ; // 'top' is not optional, so ParseArgs would already have failed.

    // Check if this top level design is from VHDL or Verilog
    // First get the library (from the 'analyze' command) :
    const char *work_lib = mainline->StringVal("work") ;
    if (!work_lib) work_lib = "work" ; // default library name

    if (veri_file::GetModule(top_name)) {
        // Top level unit is a Verilog module
        // Statically elaborate all the Verilog modules. Return if any error shows up.
        if (!veri_file::ElaborateStatic(top_name)) return 0 ; // statically elaborates all verilog modules in the "work" libarary

        VeriModule *top_module = veri_file::GetModule(top_name) ; // Get the pointer to the top-level module
        if (!top_module) {
            return 0 ; // Exit from application if there is no top module by the given name in the given Verilog designs
        }

        top_module->Info("Start hierarchy traversal here at Verilog top level module '%s'", top_module->Name()) ;
        TraverseVerilog(top_module) ; // Traverse top level module and the hierarchy under it
    } else {
        // Statically elaborate all Vhdl design units. Return if any error shows up.
        if (!vhdl_file::Elaborate(top_name, work_lib, 0, 0, 1)) {
            return 0 ; // statically elaborates design starting from top unit 'top_name' all the way down the hierarchy
        }

        VhdlLibrary *lib = vhdl_file::GetLibrary(work_lib) ;
        VhdlPrimaryUnit *top_entity = lib ? lib->GetPrimUnit(top_name) : 0 ; // Get the top-level entity
        if (!top_entity) {
            return 0 ; // Exit from application if there is no top entity by the given name in the given Vhdl design
        }

        top_entity->Info("Start hierarchy traversal here at VHDL top level unit '%s'", top_entity->Name()) ;
        TraverseVhdl(top_entity, 0 /*architecture name*/) ; // Traverse top level unit and the hierarchy under it
    }
    return 0 ; // all good
}

// Traverse a module and the hierarchy under it:
static void
TraverseVerilog(const VeriModule *module)
{
    if (!module) return ;
    // Get the scope of the module:
    VeriScope *scope = module->GetScope() ;
    // Find all the declared ids in this scope:
    Map *ids = scope ? scope->DeclArea() : 0 ;
    MapIter mi ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(ids, mi, 0, &id) { // Traverse declared ids
        if (!id || !id->IsInst()) continue ; // Consider only the instance ids
        VeriModuleInstantiation *mod_inst = id->GetModuleInstance() ; // Take the moduleinstance
        VeriModule *mod = mod_inst ? mod_inst->GetInstantiatedModule() : 0 ; // The module instance is a module
        Message::PrintLine("Processing instance : ", id->Name()) ;
        if (mod) { // This is verilog module insatantiation: need to go into that module
            Message::PrintLine("instantiated module : ", mod->Name()) ;
            Message::PrintLine("\n") ;
            TraverseVerilog(mod) ; // Traverse the instantiated module
        } else { // The module instance is a vhdl unit instance: need to go into that unit
            VeriName *instantiated_unit_name = mod_inst ? mod_inst->GetModuleNameRef() : 0 ;
            const char *entity_name = instantiated_unit_name ? instantiated_unit_name->GetName() : 0 ;

            // Find the lib_name from the attribute of the instance
            VeriExpression *attr = mod_inst ? mod_inst->GetAttribute(" vhdl_unit_lib_name") : 0 ;
            char *lib_name = attr ? attr->Image() : 0 ;
            // Image() routine on attr returns the string image of the VeriConsVal expression of attr
            // ie "/work/" so we take only the name of the lib "work" trancting the other portion:
            char *lib = lib_name ;
            if (lib && lib[0]=='"') {
                lib++ ;
                lib[Strings::len(lib)-1] = 0 ;
            }
            // Find the vhdl unit for verilog instance
            char *arch_name = 0 ;
            VhdlPrimaryUnit *vhdl_unit = VeriNode::GetVhdlUnitForVerilogInstance(entity_name, lib, 0/*search_from_loptions*/, &arch_name) ;
            Strings::free(lib_name) ;
            if (!vhdl_unit) {
                Message::PrintLine("black-box instance") ;
                continue ;
            }
            Message::PrintLine("instantiated vhdl unit : ", vhdl_unit->Name()) ;
            if (!arch_name) {
                VhdlSecondaryUnit *arch = vhdl_unit->GetSecondaryUnit(arch_name) ;
                arch_name = arch ? Strings::save(arch->Name()) : 0 ;
            }
            if (arch_name) Message::PrintLine("architecture name : ", arch_name) ;
            Message::PrintLine("\n") ;
            TraverseVhdl(vhdl_unit, arch_name) ; // Traverse instantiated unit
        }
    }
}

// Traverse an entity and the hierarchy under it:
static void
TraverseVhdl(const VhdlPrimaryUnit *unit, const char *arch_name)
{
    // Find the architecture of the unit:
    VhdlSecondaryUnit *arch = unit? unit->GetSecondaryUnit(arch_name) : 0 ;
    // Find the scope:
    VhdlScope *scope = arch ? arch->LocalScope() : 0 ;

    // Get the declared ids from that scope:
    Map *ids = scope ? scope->DeclArea() : 0 ;
    MapIter mi ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(ids, mi, 0, &id) {
        if (!id) continue ;
        VhdlStatement *stmt = id->GetStatement() ;
        if (!stmt) continue ;
        // Routine to print the hierarchy:
        PrintDesignUnit(stmt) ;
    }
}

// Print the hierarchy
static void PrintDesignUnit(const VhdlStatement *stmt)
{
    if (!stmt) return ;

    // Get teh array of statements, for loop/block statemnets
    // we get a array of statements and recursively go to the
    // inner most statement and print the hierarchy.
    Array *stmts = stmt->GetStatements() ;
    unsigned ai ;
    VhdlStatement *inner_stmt ;
    FOREACH_ARRAY_ITEM(stmts, ai, inner_stmt) {
        PrintDesignUnit(inner_stmt) ;
    }

    VhdlIdDef *inst_unit = stmt->GetInstantiatedUnit() ; // Get the instantiated unit id for instances only
    if (!inst_unit) return ; // Not an instance level

    // Processing instance
    VhdlIdDef *id = stmt->GetLabel() ;
    Message::PrintLine("Processing instance : ", id ? id->Name() : 0) ;

    VhdlPrimaryUnit *prim_unit = 0 ;
    // VIPER #7632: If preserve component is on
    if (inst_unit->IsComponent() && stmt->GetInstantiatedUnitName()) {
        VhdlLibrary *inst_lib = vhdl_file::GetLibrary(stmt->GetLibraryOfInstantiatedUnit(), 1) ;
        if (!inst_lib) inst_lib = vhdl_file::GetWorkLib() ;
        prim_unit = inst_lib ? inst_lib->GetPrimUnit(stmt->GetInstantiatedUnitName()) : 0 ;
    }

    if (!prim_unit && (inst_unit->IsComponent() || inst_unit->IsConfiguration())) {
         Message::PrintLine("black-box instance") ;
         return ; // black-box or not bind yet
    }

    if(!prim_unit) prim_unit = inst_unit ? inst_unit->GetPrimaryUnit() : 0 ; // Get the primary unit
    if (!prim_unit) return ;

    if (prim_unit->IsVerilogModule()) { // instance of verilog module
        // Get instantiated verilog module
        VeriModule *veri_module = vhdl_file::GetVerilogModuleFromlib(inst_unit->GetContainingLibraryName(), inst_unit->Name()) ;
        if (!veri_module) return ;
        Message::PrintLine("instantiated module : ", veri_module->Name()) ;
        Message::PrintLine("\n") ;
        TraverseVerilog(veri_module) ; // Traverse instantiated module
    } else { // Instance of vhdl unit
        // Find the architecture name and traverse the vhdl design
        const char *arch_name = stmt->GetArchitectureOfInstantiatedUnit() ;
        Message::PrintLine("instantiated vhdl unit : ", prim_unit->Name()) ;
        Message::PrintLine("architecture name : ", arch_name ? arch_name : 0) ;
        Message::PrintLine("\n") ;
        TraverseVhdl(prim_unit, arch_name) ; // Traverse the vhdl unit
    }
}
