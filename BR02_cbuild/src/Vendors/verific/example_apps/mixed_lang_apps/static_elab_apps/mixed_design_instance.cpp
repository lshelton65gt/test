/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <iostream>
using namespace std ;
#include "Set.h"            // Make class Set available
#include "Array.h"          // Make class Array available
#include "Message.h"        // Make message handlers available

#include "veri_file.h"      // Make verilog reader available
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h" // Definition of VeriName
#include "VeriId.h"         // Definitions of all verilog identifier nodes
#include "VeriScope.h"      // Definition of VhdLibrary

#include "vhdl_file.h"      // Make verilog reader available
#include "VhdlIdDef.h"      // Make verilog reader available
#include "VhdlScope.h"      // Definition of VhdLibrary
#include "VhdlStatement.h"  // Make verilog reader available
#include "VhdlName.h"       // Definition of VhdlName
#include "VhdlUnits.h"      // Definition of VhdLibrary

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/****************************************************************************************
************** MIXED LANGUAGE STATIC ELABORATION EXAMPLE #mixed_design_instance *********
*****************************************************************************************

                                NOTE TO THE READER:

    The following is an example which demonstrates the following :

    How to analyze and statically elaborate mixed language designs and traverse the
    hierarchy.
    This application takes in a list of verilog and vhdl files (with .v and .vhd
    extensions respectively), the top-level unit preceded by the "-top" option and
    another keyword (verilog/vhdl) indicating whether the top-level unit resides in
    the verilog or in the vhdl design.

    Eg. :

      (i)  To make this example run with a verilog module as the top-level unit use a
           command similar like the one below:

           mixed_static_elab_ex2-linux ex2_test.v ex2_test.vhd -top top -verilog

           Here ex2_test.v and ex2_test.vhd are the input design files, top is the top-level
           verilog module and the last option '-verilog' specifies that the top-level
           unit is a verilog module named 'top'.

      (ii) To make this example run with a vhdl entity as the top-level unit use a
           command similar to the one below:

           mixed_static_elab_ex2-linux ex2_test.v ex2_test.vhd -top comp -vhdl

           Here ex2_test.v and ex2_test.vhd are the input design files, top is the top-level
           vhdl unit and the last option '-vhdl' specifies that the top-level unit is
           a vhdl unit named 'comp'.

    This application traverse the parsetree in DFS order and print all the instances,
    instantiated module/unit name, original module/unit name and the formal=>actual port list.

*****************************************************************************************/

// Routines for traversing the module/entity instances in DFS order and print them.
static unsigned PrintDesignUnit(VhdlStatement *stmt, Set *already_traversed, unsigned count, unsigned level) ;
static void TraverseVerilog(const VeriModule *top, Set *already_traversed, unsigned count, unsigned level) ;
static void TraverseVhdl(const VhdlPrimaryUnit *top, const char *arch_name, Set *already_traversed, unsigned count, unsigned level) ;

// Traverse a module and the hierarchy under it:
static void
TraverseVerilog(const VeriModule *module, Set *already_traversed, unsigned count, unsigned level)
{
    if (!module) return ;

    // Keep the indentation level
    unsigned old_level = level ;
    unsigned old_count = count ;

    // Processing verilog module:
    cout << VeriTreeNode::PrintLevel(level+1) << "-- Processing verilog module: " << module->Name() << endl ;

    unsigned has_instance = 0 ;
    //unsigned count = 0 ;
    // Get the scope of the module:
    VeriScope *scope = module->GetScope() ;
    // Find all the declared ids in this scope:
    Map *ids = scope ? scope->DeclArea() : 0 ;
    MapIter mi ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(ids, mi, 0, &id) { // Traverse declared ids
        if (!id || !id->IsInst()) continue ; // Consider only the instance ids

        VeriModuleInstantiation *mod_inst = id->GetModuleInstance() ; // Take the moduleinstance
        VeriModule *mod = mod_inst ? mod_inst->GetInstantiatedModule() : 0 ; // The module instance is a module
        cout << VeriTreeNode::PrintLevel(level+1) ; // indentation
        cout << ++count << ". instance : " << id->Name() << endl ;

        char *arch_name = 0 ;
        has_instance = 1 ;

        if (mod) { // This is verilog module insatantiation: need to go into that module
            // Get original module name:
            VeriModule *orig_module = mod->GetOriginalModule() ;
            cout << VeriTreeNode::PrintLevel(level+1) ;
            cout << "-- instantiated verilog module :" << "'" << mod->Name() << "'" << "  '" << "(orig name: " << (orig_module ? orig_module->Name() : mod->Name()) << ")" << "' " << endl ;

            // Print port connects:
            Array *ports = mod->GetPorts() ; // Get the formal ports
            // Get the port connections of this instance:
            Array *actual_ports = id->GetPortConnects() ;

            if (!ports || !ports->Size() || !actual_ports || !actual_ports->Size()) {
                cout << VeriTreeNode::PrintLevel(level+1) ;
                cout << "-- empty/no port list" << endl << endl ;
                continue ;
            }

            cout << VeriTreeNode::PrintLevel(level+1) ;
            cout << "-- formal=>actual: " ;

            VeriIdDef *formal ;
            VeriExpression *actual ;
            unsigned j ;
            FOREACH_ARRAY_ITEM(actual_ports, j, actual) {
                if (!actual) continue ;

                formal = 0 ;
                if (actual->NamedFormal()) {
                    // This is a (SV-style) named formal.
                    formal = mod->GetPort(actual->NamedFormal()) ;
                } else {
                    // This is a ordered formal
                    if (j>=((ports) ? ports->Size() : 0)) break ; // no use checking more.
                    formal = (ports) ? (VeriIdDef*)ports->At(j) : 0 ;
                }
                if (!formal) continue ;

                VeriExpression *actual_connc = actual->GetConnection() ;
                cout << formal->Name() << "=>" ;
                actual_connc->PrettyPrint(cout, 0) ;
                if (j<actual_ports->Size()-1) cout << ", " ;
            }
            cout << endl << endl ;

            // Check for already traversed module:
            if (!already_traversed->Insert((void*)mod)) {
                cout << VeriTreeNode::PrintLevel(level+1) ;
                cout << "-- module " << mod->Name() << " is already traversed" << endl << endl ;
                continue ;
            }

            TraverseVerilog(mod, already_traversed, 0, level+1) ; // Traverse the instantiated module
        } else { // The module instance is a vhdl unit instance: need to go into that unit
            VeriName *instantiated_unit_name = mod_inst ? mod_inst->GetModuleNameRef() : 0 ;
            const char *entity_name = instantiated_unit_name ? instantiated_unit_name->GetName() : 0 ;

            // Find the lib_name from the attribute of the instance
            VeriExpression *attr = mod_inst ? mod_inst->GetAttribute(" vhdl_unit_lib_name") : 0 ;
            char *lib_name = attr ? attr->Image() : 0 ;
            // Image() routine on attr returns the string image of the VeriConsVal expression of attr
            // ie "/work/" so we take only the name of the lib "work" trancting the other portion:
            char *lib = lib_name ;
            if (lib && lib[0]=='"') {
                lib++ ;
                lib[Strings::len(lib)-1] = 0 ;
            }

            // Find the vhdl unit for verilog instance
            VhdlPrimaryUnit *vhdl_unit = VeriNode::GetVhdlUnitForVerilogInstance(entity_name, lib, 0/*search_from_loptions*/, &arch_name) ;
            Strings::free(lib_name) ;
            if (!vhdl_unit) {
                cout << VeriTreeNode::PrintLevel(level+1) ;
                cout << "-- black-box instance" << endl << endl ;
                continue ;
            }

            // Find architecture name if not found before:
            if (!arch_name) {
                VhdlSecondaryUnit *arch = vhdl_unit->GetSecondaryUnit(arch_name) ;
                arch_name = arch ? Strings::save(arch->Name()) : 0 ;
            }

            // Get original unit name:
            VhdlDesignUnit *orig_unit = vhdl_unit->GetOriginalUnit() ;

            // Instantiated unit, original unit, architecture name:
            cout << VeriTreeNode::PrintLevel(level+1) ;
            cout << "-- instantiated vhdl unit: " << "'" << vhdl_unit->Name() << "'" << " '" << "(orig name: " << (orig_unit ? orig_unit->Name() : vhdl_unit->Name()) << ")" << "'"
                    << " '" << "(arch name: " << arch_name << ")" << "'" << endl ;

            unsigned j ;
            VhdlIdDef *unit_id = vhdl_unit->Id() ;
            if (!unit_id) continue ;

            // Get the port connections of this instance:
            Array *actual_ports = id->GetPortConnects() ;
            if (!actual_ports || !actual_ports->Size()) {
                cout << VeriTreeNode::PrintLevel(level+1) ;
                cout << "-- empty/no port list" << endl << endl ;
                continue ;
            }

            // Print formal=>actual porlist
            cout << VeriTreeNode::PrintLevel(level+1) ;
            cout << "-- formal=>actual: " ;

            VhdlIdDef *formal ;
            VeriExpression *actual ;
            FOREACH_ARRAY_ITEM(actual_ports, j, actual) {
                if (!actual) continue ;

                formal = 0 ;
                if (actual->NamedFormal()) {
                    // This is a (SV-style) named formal.
                    formal = unit_id->GetPort(actual->NamedFormal()) ;
                } else {
                    // This is a ordered formal
                    if (j>=unit_id->NumOfPorts()) break ; // no use checking more.
                    formal = (VhdlIdDef*)unit_id->GetPortAt(j) ;
                }
                if (!formal) continue ;

                VeriExpression *actual_connc = actual->GetConnection() ;
                cout << formal->Name() << "=>" ;
                actual_connc->PrettyPrint(cout, 0) ;
                if (j<actual_ports->Size()-1) cout << ", " ;
            }
            cout << endl << endl ;

            // Check for already traversed unit:
            if (!already_traversed->Insert((void*)vhdl_unit)) {
                cout << VeriTreeNode::PrintLevel(level+1) ;
                cout << "-- unit " << vhdl_unit->Name() << " is already traversed" << endl << endl ;
                continue ;
            }

            TraverseVhdl(vhdl_unit, arch_name, already_traversed, 0, level+1) ; // Traverse instantiated unit
        }
    }
    if (!has_instance) cout << VeriTreeNode::PrintLevel(level+1) << "-- module " << module->Name() << " has no instance" << endl << endl ;
    level = old_level ; // retrieve the previous indentation level
    count = old_count ; // retrieve the previous count
}

// Traverse an entity and the hierarchy under it:
static void
TraverseVhdl(const VhdlPrimaryUnit *unit, const char *architecture_name, Set *already_traversed, unsigned count, unsigned level)
{
    if (!unit) return ;

    // Keep the indentation level
    unsigned old_level = level ;
    unsigned old_count = count ;

    // Processing vhdl unit:
    cout << VhdlTreeNode::PrintLevel(level+1) << "-- Processing vhdl unit: " << unit->Name() << endl ;

    unsigned has_instance = 0 ;
    // Find the architecture of the unit:
    VhdlSecondaryUnit *arch = unit->GetSecondaryUnit(architecture_name) ;
    // Find the scope:
    VhdlScope *scope = arch ? arch->LocalScope() : 0 ;
    // Get the declared ids from that scope:
    Map *ids = scope ? scope->DeclArea() : 0 ;
    MapIter mi ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(ids, mi, 0, &id) {
        if (!id) continue ;
        //cout << id->Name() << endl ;
        VhdlStatement *stmt = id->GetStatement() ;
        if (!stmt) continue ;
        // Routine to print the hierarchy:
        unsigned instance_exist = PrintDesignUnit(stmt, already_traversed, count++, level) ;
        if (!has_instance) has_instance = instance_exist ;
    }
    if (!has_instance) cout << VhdlTreeNode::PrintLevel(level+1) << "-- unit " << unit->Name() << " has no instance" << endl << endl ;
    level = old_level ; // retrieve the previous indentation level
    count = old_count ; // retrieve the previous count
}

// Print the hierarchy:
static unsigned PrintDesignUnit(VhdlStatement *stmt, Set *already_traversed, unsigned count, unsigned level)
{
    if (!stmt) return 0 ;

    unsigned new_count = 0 ;
    // Get the array of statements, for loop/block statemnets
    // we get a array of statements and recursively go to the
    // inner most statement and print the hierarchy.
    Array *stmts = stmt->GetStatements() ;
    if (stmts) {
        VhdlIdDef *label = stmt->GetLabel() ;
        cout << VhdlTreeNode::PrintLevel(level+1) ;
        cout << count+1 << ".block name: " << label->Name() << endl ;
    }

    unsigned ai ;
    VhdlStatement *inner_stmt ;
    FOREACH_ARRAY_ITEM(stmts, ai, inner_stmt) {
        if (!inner_stmt) continue ;
        (void) PrintDesignUnit(inner_stmt, already_traversed, new_count++, level+1) ;
    }

    VhdlIdDef *inst_unit = stmt->GetInstantiatedUnit() ; // Get the inst unit
    if (!inst_unit) return 0 ; // Not an instance label

    // Processing instance
    VhdlIdDef *id = stmt->GetLabel() ;
    cout << VhdlTreeNode::PrintLevel(level+1) ;
    cout << ++count << ". instance: " << (id ? id->Name() : 0) << endl ;

    VhdlPrimaryUnit *prim_unit = 0 ;
    if (inst_unit->IsComponent() && stmt->GetInstantiatedUnitName()) {
        VhdlLibrary *inst_lib = vhdl_file::GetLibrary(stmt->GetLibraryOfInstantiatedUnit(), 1) ;
        if (!inst_lib) inst_lib = vhdl_file::GetWorkLib() ;
        prim_unit = inst_lib ? inst_lib->GetPrimUnit(stmt->GetInstantiatedUnitName()) : 0 ;
    }

    if (!prim_unit && (inst_unit->IsComponent() || inst_unit->IsConfiguration())) {
        cout << VhdlTreeNode::PrintLevel(level+1) ;
        cout << "-- black-box instance" << endl << endl ;
        return 0 ; // black-box or not bind yet
    }

    const char *arch_name = 0 ;

    if (!prim_unit) prim_unit = inst_unit ? inst_unit->GetPrimaryUnit() : 0 ; // Get the primary unit
    if (!prim_unit) return 0 ;

    VeriModule *veri_module = 0 ;
    if (prim_unit->IsVerilogModule()) { // instance of verilog module
        // Get instantiated verilog module
        veri_module = vhdl_file::GetVerilogModuleFromlib(inst_unit->GetContainingLibraryName(), inst_unit->Name()) ;
        if (!veri_module) return 0 ;

        // Get original module name:
        VeriModule *orig_module = veri_module->GetOriginalModule() ;

        // Instantiated module
        cout << VhdlTreeNode::PrintLevel(level+1) ;
        cout << "-- instantiated verilog module: " << "'" << veri_module->Name() << "'" << " '" << "(orig name: " << (orig_module ? orig_module->Name() : veri_module->Name()) << ")" << "'" << endl ;
    } else { // Instance of vhdl unit
        // Find the architecture name and traverse the vhdl design
        arch_name = stmt->GetArchitectureOfInstantiatedUnit() ;

        // Find architecture name if not found before:
        if (!arch_name) {
            VhdlSecondaryUnit *arch = prim_unit->GetSecondaryUnit(arch_name) ;
            arch_name = arch ? Strings::save(arch->Name()) : 0 ;
        }

        // Get original unit name:
        VhdlDesignUnit *orig_unit = prim_unit->GetOriginalUnit() ;

        // Instantiated vhdl unit, original unit, architecture name:
        cout << VhdlTreeNode::PrintLevel(level+1) ;
        cout << "-- instantiated vhdl unit: " << "'" << prim_unit->Name() << "'" << " '" << "(orig name: " << (orig_unit ? orig_unit->Name() : prim_unit->Name()) << ")" << "' "
                << "'" << "(arch name: " << arch_name << ")" << "'" << endl ;
    }

    VhdlIdDef *prim_id = prim_unit->Id() ;

    // Print formal=>actual port list
    VhdlComponentInstantiationStatement *comp_stmt = static_cast<class VhdlComponentInstantiationStatement*>(stmt) ;
    Array *actual_ports = comp_stmt->GetPortMapAspect() ;
    if (!actual_ports || !actual_ports->Size()) {
        cout << VhdlTreeNode::PrintLevel(level+1) ;
        cout << "-- empty/no port list" << endl << endl ;
    } else {
        cout << VhdlTreeNode::PrintLevel(level+1) ;
        cout << "-- formal=>actual: " ;
    }

    unsigned i ;
    VhdlDiscreteRange *assoc, *actual_part ;
    VhdlIdDef *formal ;
    FOREACH_ARRAY_ITEM(actual_ports, i, assoc) {
        if (!assoc || assoc->IsOpen()) continue ;

        actual_part = assoc->ActualPart() ;
        if (!actual_part || actual_part->IsOpen()) continue ; // named a

        formal = 0 ;
        if (assoc->IsAssoc() ) {
            // Find the formal :
            formal = assoc->FindFormal() ; // named
        } else if (prim_id) {
            formal = prim_id->GetPortAt(i) ; // positional
        }
        if (!formal) continue ;
        cout << formal->Name() << "=>";
        actual_part->PrettyPrint(cout, 0) ;
        if (i<actual_ports->Size()-1) cout << ", " ;
    }
    cout << endl << endl ;

    if (prim_unit->IsVerilogModule()) { // instance of verilog module
        // Check for already traversed module:
        if (!veri_module) return 0 ;
        if (!already_traversed->Insert((void*)veri_module)) {
            cout << VhdlTreeNode::PrintLevel(level+1) ;
            cout << "-- module " << veri_module->Name() << " is already traversed" << endl << endl ;
            return 1 ;
        }

        TraverseVerilog(veri_module, already_traversed, 0, level+1) ; // Traverse instantiated module
    } else {
        // Check for already traversed unit:
        if (!already_traversed->Insert((void*)prim_unit)) {
            cout << VhdlTreeNode::PrintLevel(level+1) ;
            cout << "-- unit " << prim_unit->Name() << " is already traversed" << endl << endl ;
            return 1 ;
        }

        TraverseVhdl(prim_unit, arch_name, already_traversed, 0, level+1) ; // Traverse the vhdl unit
    }

    return 1 ;
}

int main(int argc, char **argv)
{
    // Create a verilog reader object
    veri_file veri_reader ;   // Read the intro in veri_file.h to see the details of this object.

    // Create a vhdl reader object
    vhdl_file vhdl_reader ; // Read the intro in vhdl_file.h to see the details of this object.

    const char *top_design_unit_name = 0 ;

    // Initialize the top module flags.
    int is_top_vhdl = 0 ;
    int is_top_verilog =0 ;

    if (argc>4) {
        // Look at the input files and see if they are verilog or vhdl
        // (verilog files should end in .v while vhdl files in .vhd)
        // If Vhdl then analyze in Vhdl or else do so in Verilog.
        for (int i=1; i<=argc-4; i++) {
            if (strstr(argv[i], ".vhd")) { // If true then the file is a vhdl design
                // Always analyze package 'standard' first, from file 'standard.vhd' :
                vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");   // store in std library
                vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/std_1164.vhd", "ieee");  // store in ieee library
                vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/mixed_lang_vltype.vhd", "vl");
               // Analyze the design (in vhdl mode) Return in case of failure.
                if (!vhdl_reader.Analyze(argv[i])) return 1 ;
            } else if (strstr(argv[i], ".v")) { // If true then the file is a verilog design
                // Analyze the design (in verilog2k mode). Return in case of failure.
                if (!veri_reader.Analyze(argv[i], 1/* Verilog2K Mode*/)) return 1 ;
            } else {
                printf("File %s has got an invalid extension. \n", argv[i]) ;
            }
        }

        // The penultimate argument should be  the top-level unit preceded by the '-top'
        // option. Check that.
        if (Strings::compare(argv[argc-3], "-top")) {
            top_design_unit_name  = argv[argc-2] ;
            // Look at the last argument to determine if the top unit resides in verilog or in vhdl design
            // and set the flag accordingly.
            if (Strings::compare(argv[argc-1], "-verilog")) is_top_verilog = 1 ;
            if (Strings::compare(argv[argc-1], "-vhdl")) is_top_vhdl = 1 ;

            if (!is_top_vhdl && !is_top_verilog) { // If none of the flags is set then the option specified is wrong
                Message::Msg(VERIFIC_ERROR, 0, 0, "the last argument can be either -verilog or -vhdl") ;
                return 0 ;
            }

            // Set the top module flags properly. They indicate as to whether the top
            // module is a Verilog module or a Vhdl entity.
            if (Strings::compare(argv[4], "vhdl")) {
                is_top_vhdl = 1 ;
            } else if (Strings::compare(argv[4], "verilog")) {
                is_top_verilog = 1 ;
            }
        } else { // If the "-top" option has not been specified
            Message::Msg(VERIFIC_ERROR, 0, 0, "the top-level units has to specified preceded by the -top option") ;
            return 0 ;
        }
    } else { // If the number of arguments is lesser than 6 then one or more arguments are missing. So assign default values.
        Message::PrintLine("Default input file : ex2_test.v ex2_test.vhd. Default top-level unit: top. \nSpecify command-line argument to override default values") ;

        // Analyze the default files.
        if (!veri_reader.Analyze("ex2_test.v", 1/* Verilog2K Mode*/)) return 1 ; // Analyze the default verilog file viz. 'ex2_test.v'.
        // Always analyze package 'standard' first, from file 'standard.vhd' :
        vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/standard.vhd", "std");   // store in std library
        vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/std_1164.vhd", "ieee");  // store in ieee library
        vhdl_reader.Analyze("../../../vhdl_packages/ieee_1993/src/mixed_lang_vltype.vhd", "vl");
        // Analyze the design (in vhdl mode). Return in case of failure.
        if (!vhdl_reader.Analyze("ex2_test.vhd")) return 1 ; // Analyze the default vhdl file viz. 'ex2_test.vhd'.
        is_top_verilog = 1 ; // By default the top module is a Verilog module.
        top_design_unit_name = "top" ; // Set the top design unit name to the Verilog top level module.
    }

    // Get the current work library  for Vhdl.
    VhdlLibrary *lib = vhdl_reader.GetWorkLib() ;

    Set already_traversed(POINTER_HASH) ; // for keep track of the already traversed modules/units for not to print them repeatedly if instantiated more than once

    if (is_top_verilog) { // If the top level unit is a Verilog module
        // Statically elaborate all the Verilog modules. Return if any error shows up.
        VeriModule *top_module = veri_reader.GetModule(top_design_unit_name) ; // Get the pointer to the top-level module
        if (!top_module) return 0 ; // Exit from application if there is no top module by the given name in the given Verilog designs

        Array top_mods ;
        top_mods.InsertLast(top_module) ;
        Array *elab_top_mods = veri_reader.ElaborateMultipleTopStatic(&top_mods, 0) ;

        unsigned i ;
        VeriModule *mod ;
        FOREACH_ARRAY_ITEM(elab_top_mods, i, mod) {
            if (!mod) continue ;
            cout << endl << "Top level module : " << mod->Name() << endl << endl ;
            TraverseVerilog(mod, &already_traversed, 0, 0) ; // Traverse top level module and the hierarchy under it
        }
        delete elab_top_mods ;
    } else if (is_top_vhdl) {
        // Statically elaborate all Vhdl design units. Return if any error shows up.
        VhdlPrimaryUnit *top_entity = 0 ;
        top_entity = lib ? lib->GetPrimUnit(top_design_unit_name) : 0 ; // Get the top-level entity
        if (!top_entity) return 0 ; // Exit from application if there is no top entity by the given name in the given Vhdl design
        VhdlPrimaryUnit *prim_unit = top_entity->StaticElaborate(0, 0, 0, 0, 0) ;
        if (!prim_unit) return 0 ;

        cout << endl << "Top level unit : " << prim_unit->Name() << endl << endl ;
        TraverseVhdl(prim_unit, 0 /*architecture name*/, &already_traversed, 0, 0) ; // Traverse top level unit and the hierarchy under it
    }
    return 1 ;
}
