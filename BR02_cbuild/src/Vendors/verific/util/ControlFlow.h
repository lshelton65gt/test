/*
 *
 * [ File Version : 1.13 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_CFG_H_
#define _VERIFIC_CFG_H_

#include "VerificSystem.h"
#include "LineFile.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Map ;
class Set ;
class Array ;

/* -------------------------------------------------------------- */

typedef enum _node_type {
    CFG_DECL_NODE, CFG_STMT_NODE, CFG_STATE_NODE, CFG_CONDITION_NODE, CFG_SCOPE_END_NODE, CFG_NODE_TYPE_MAX
} cfg_node_t ;

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VerificCFGRootNode
{
protected: // Abstract class
    VerificCFGRootNode() ;

public:
    virtual ~VerificCFGRootNode() ;

private:
    // Prevent compiler from defining the following
    VerificCFGRootNode(const VerificCFGRootNode &) ; // Purposely leave unimplemented
    VerificCFGRootNode& operator=(const VerificCFGRootNode &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Determine the type of the node
    virtual cfg_node_t GetClassId() const = 0 ;
    virtual unsigned  IsDeclarationNode() const       { return 0 ; }
    virtual unsigned  IsSimpleStatementNode() const   { return 0 ; }
    virtual unsigned  IsConditionalNode() const       { return 0 ; }
    virtual unsigned  IsScopeEndNode() const          { return 0 ; }
    virtual unsigned  IsStateNode() const             { return 0 ; }

    // Following is only for the declaration node
    virtual unsigned  NumDeclaration() const          { return 0 ; }
    virtual void      *GetDeclaration(unsigned /*pos*/) const  { return 0 ; }
    virtual void      *GetScope(unsigned /*pos*/) const        { return 0 ; }

    // Following is only for the statement node
    virtual unsigned  AddStatement(const void *, const void *)   { return 0 ; }
    virtual void      *GetStatement(unsigned /*pos*/) const      { return 0 ; }
    virtual Map       *GetStatementList() const                  { return 0 ; }

    // Following is only for the state node
    virtual void      SetState(unsigned /*state*/)      { }
    virtual unsigned  GetState()                        { return 0 ; }

    virtual void      AddEventExprList(Array* /*event_arr*/) { }
    virtual Array     *GetEventExprList() const              { return 0 ; }

    virtual void             SetLinefile(linefile_type /*line_file*/)    { }
    virtual linefile_type    GetLinefile()                               { return 0 ; }

    // VIPER #7422:
    virtual void      SetConversionStatus(unsigned converted)       { _converted = converted ; }
    virtual unsigned  GetConversionStatus() const                   { return _converted ; }

    // Following is only for the conditional node
    virtual unsigned  AddBranch(Array *, VerificCFGRootNode *)      { return 0 ; }
    virtual Array    *GetBranchConditions(unsigned ) const          { return 0 ; }
    virtual VerificCFGRootNode *GetBranchStatement(unsigned ) const { return 0 ; }
    virtual Array    *GetBranchConditions() const                   { return 0 ; }
    virtual void     *GetConditionExpression() const                { return 0 ; }

    // If this node is for a scoped block, get the end of the scope
    virtual VerificCFGRootNode *GetScopeEnd() const           { return 0 ; }
    virtual unsigned  SetScopeEnd(VerificCFGRootNode *)       { return 1 ; }
    virtual VerificCFGRootNode *GetScopeBegin() const         { return 0 ; }
    virtual unsigned  SetScopeBegin(VerificCFGRootNode *)     { return 1 ; }

    // Manipulate the successor and predecessors
    virtual VerificCFGRootNode *GetChild() const ;
    virtual VerificCFGRootNode *GetChild(unsigned pos) const ;
    virtual unsigned  AddChild(VerificCFGRootNode *child) ;

    virtual VerificCFGRootNode *GetParent() const ;
    virtual VerificCFGRootNode *GetParent(unsigned pos) const ;
    virtual unsigned  AddParent(VerificCFGRootNode *parent) ;

    virtual unsigned NumChildren() const ;
    virtual unsigned NumParents() const ;

    virtual unsigned NumBranch() const   { return 0 ; }

    virtual Set*     GetChildren()       { return _children ; }
    virtual Set*     GetParents()        { return _parents ; }

    // For cleanup
    unsigned ResetNodeInParents(VerificCFGRootNode const *child_node) ;

    // Cfg manipulation routines:
    unsigned    InsertNodeBetween(VerificCFGRootNode *next_node, VerificCFGRootNode *new_node) ; // insert a node between two given nodes
    unsigned    InsertNode(VerificCFGRootNode *new_node) ; // insert a node after a given node
    unsigned    DeleteNode() ; // dele a node
    unsigned    ReplaceNode(VerificCFGRootNode *parent, VerificCFGRootNode *current) ; // replace with a graph

    // print the graph : pass the function pointer that understands
    // the stored void * object type and can print it
    virtual void PrintThisNode(void (*f)(void *)) const = 0 ;
    virtual void PrintNode(void (*f)(void *), Set *printed) const ;

    virtual unsigned GetId() const { return _node_id ; }

private:
    virtual unsigned AddChild_(VerificCFGRootNode *child) ;
    virtual unsigned AddParent_(VerificCFGRootNode *parent) ;

protected:
    Set   *_children ;
    Set   *_parents ;
    unsigned _node_id ; // unique id for the node
    unsigned _converted ; // VIPER #7422
} ; // class VerificCFGRootNode

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VerificCFGDeclarationNode : public VerificCFGRootNode
{
public:
    VerificCFGDeclarationNode() ;
    virtual ~VerificCFGDeclarationNode() ;

    virtual cfg_node_t GetClassId() const        { return CFG_DECL_NODE ; }
    virtual unsigned   IsDeclarationNode() const { return 1 ; }

    unsigned AddDeclaration(const void *decl, const void *scope) ;
    virtual unsigned NumDeclaration() const ;
    virtual void    *GetDeclaration(unsigned pos) const ;
    virtual void    *GetScope(unsigned pos) const ;
    virtual void PrintThisNode(void (*print_decl)(void *)) const ;

private:
    // Prevent compiler from defining the following
    VerificCFGDeclarationNode(const VerificCFGDeclarationNode &) ; // Purposely leave unimplemented
    VerificCFGDeclarationNode& operator=(const VerificCFGDeclarationNode &) ; // Purposely leave unimplemented

private:
    Array *_declaration_list ;   // of type void *
    Array *_scope_list ;         // of type void *

} ; // class VerificCFGDeclarationNode

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VerificCFGStatementNode : public VerificCFGRootNode
{
public:
    VerificCFGStatementNode() ;
    virtual ~VerificCFGStatementNode() ;

    virtual cfg_node_t GetClassId() const               { return CFG_STMT_NODE ; }
    virtual unsigned   IsSimpleStatementNode() const    { return 1 ; }

    virtual unsigned   AddStatement(const void *stmt, const void *scope) ;
    unsigned           NumStatement() const ;
    void               *GetStatement(unsigned pos) const ;
    virtual Map        *GetStatementList() const        { return _statement_list ; }

    virtual void       PrintThisNode(void (*print_stmt)(void *)) const ;

    // If this node is for a scoped block, get the end of the scope
    virtual VerificCFGRootNode *GetScopeEnd() const     { return _scope_end ; }
    virtual unsigned            SetScopeEnd(VerificCFGRootNode *se) ;

private:
    // Prevent compiler from defining the following
    VerificCFGStatementNode(const VerificCFGStatementNode &) ; // Purposely leave unimplemented
    VerificCFGStatementNode& operator=(const VerificCFGStatementNode &) ; // Purposely leave unimplemented

private:
    Map    *_statement_list ; // of type void *: contains statement vrs its scope
    VerificCFGRootNode *_scope_end ;      // if statements here are inside a seq-scope
} ; // class VerificCFGStatementNode

/* -------------------------------------------------------------- */

// VerificCFGConditionalStatementNode captures conditional statements
// For case_statement:
//     _branch_conditions: Array with each element holding case choices for
//                         each branch (which itself is an array). There wont
//                         be any entry for the default branch
//     Hence for case statements, number of children of ConditionalStatementNode
//     is equal or one more (if default is present) than the number of elements
//     in _branch_conditions
//
// For if_statement:
//     _branch_conditions: Array is null. GetChild(0) returns true branch
//                         GetChild(1) returns false branch.
//
// ConditionalStatementNode is also used to represent loops. The tail of the
// true branch polints back to this conditional node while the false branch
// proceeds beyond the loop statement when the loop condition is false.
//
class VFC_DLL_PORT VerificCFGConditionalStatementNode : public VerificCFGRootNode
{
public:
    VerificCFGConditionalStatementNode() ;
    virtual ~VerificCFGConditionalStatementNode() ;

    virtual cfg_node_t GetClassId() const        { return CFG_CONDITION_NODE ; }
    virtual unsigned   IsConditionalNode() const { return 1 ; }

    virtual void PrintThisNode(void (*print)(void *)) const ;
    virtual void PrintNode(void (*print)(void *), Set *stop) const ;

    virtual unsigned  AddBranch(Array *branch_condition_array, VerificCFGRootNode *branch) ;
    virtual Array              *GetBranchConditions(unsigned pos) const ;
    virtual VerificCFGRootNode *GetBranchStatement(unsigned pos) const ;
    virtual unsigned            NumBranch() const ;

    // The caller routine must typecast back the return value
    virtual void *GetConditionExpression() const { return _condition_expr ; }

    // If this node is for a scoped block, get the end of the scope
    virtual VerificCFGRootNode *GetScopeEnd() const    { return _scope_end ; }
    virtual unsigned            SetScopeEnd(VerificCFGRootNode *se) ;

    // Following must be set immediately after constructing this node
    void SetConditionExpression(void *) ;

private:
    // Prevent compiler from defining the following
    VerificCFGConditionalStatementNode(const VerificCFGConditionalStatementNode &) ; // Purposely leave unimplemented
    VerificCFGConditionalStatementNode& operator=(const VerificCFGConditionalStatementNode &) ; // Purposely leave unimplemented

private:
    // _branch_conditions is null for if statement
    // For if statement child at position '0' is then branch, position '1' is else branch
    // No condition is present for the default branch / others branch of case statement
    // default/others branch must be the last child
    void     *_condition_expr ;
    Array    *_branch_conditions ; // Array of branch condition list (array)
    VerificCFGRootNode *_scope_end ;
} ; // class VerificCFGConditionalStatementNode

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VerificCFGStateNode : public VerificCFGStatementNode
{
public:
    VerificCFGStateNode() ;
    virtual ~VerificCFGStateNode() ;

    virtual cfg_node_t GetClassId() const                      { return CFG_STATE_NODE ; }
    virtual unsigned   IsStateNode() const                     { return 1 ; }

    virtual void             SetState(unsigned state)                { _state = state ; }
    virtual unsigned         GetState()                              { return _state ; }
    virtual void             SetLinefile(linefile_type line_file)    { _line_file = line_file ; }
    virtual linefile_type    GetLinefile()                           { return _line_file ; }
    virtual void             AddEventExprList(Array *event_arr)      { _event_expr_list = event_arr ; }
    virtual Array            *GetEventExprList() const               { return _event_expr_list ; }

    virtual void PrintThisNode(void (*print)(void *)) const ;

private:
    // Prevent compiler from defining the following
    VerificCFGStateNode(const VerificCFGStateNode &) ; // Purposely leave unimplemented
    VerificCFGStateNode& operator=(const VerificCFGStateNode &) ; // Purposely leave unimplemented

    unsigned SetEventExpression(void *event) ;

private:
    void           *_event_expression ;
    unsigned       _state ;
    linefile_type  _line_file ;
    Array          *_event_expr_list ;
} ; // class VerificCFGStateNode

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VerificCFGScopeEndNode : public VerificCFGRootNode
{
public:
    explicit VerificCFGScopeEndNode(VerificCFGRootNode *) ;
    virtual ~VerificCFGScopeEndNode() ;

    virtual cfg_node_t GetClassId() const      { return CFG_SCOPE_END_NODE ; }
    virtual unsigned   IsScopeEndNode() const  { return 1 ; }

private:
    // Prevent compiler from defining the following
    VerificCFGScopeEndNode() ; // Purposely leave unimplemented
    VerificCFGScopeEndNode(const VerificCFGScopeEndNode &) ; // Purposely leave unimplemented
    VerificCFGScopeEndNode& operator=(const VerificCFGScopeEndNode &) ; // Purposely leave unimplemented

public:
    // the start of this scope
    virtual VerificCFGRootNode *GetScopeBegin() const     { return _scope_begin ; }
    virtual unsigned  SetScopeBegin(VerificCFGRootNode *) ;

    virtual void PrintThisNode(void (*)(void *)) const ;

private:
    VerificCFGRootNode *_scope_begin ;
} ; // class VerificCFGScopeEndNode

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_CFG_H_

