/*
 *
 * [ File Version : 1.59 - 2014/01/09 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_MESSAGE_H_
#define _VERIFIC_MESSAGE_H_

#include <stdarg.h> // for var-args
#include <time.h>   // time_t

#include "VerificSystem.h"
#include "LineFile.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Map ;

/*
    Message is a class with static members only, to push errors,
    warnings and info messages to various outlets.
    Normally stdout/stderr, but we can also direct them into a
    logfile and direct them to callback routines.
*/

// Enumerate the type of message :
enum msg_type_t {
    VERIFIC_NONE,         // print no prefix
    VERIFIC_ERROR,        // print ERROR:
//CARBON_BEGIN
    VERIFIC_ALERT,        // print ALERT:
//CARBON_END
    VERIFIC_WARNING,      // print WARNING:
    VERIFIC_IGNORE,       // ignore message (do not print message):
    VERIFIC_INFO,         // print INFO:
    VERIFIC_COMMENT,      // print --
    VERIFIC_PROGRAM_ERROR // print PROGRAM_ERROR
} ;

/* -------------------------------------------------------------- */

class VFC_DLL_PORT Message
{
private :
    Message() ; // This class should never get instantiated (make this private)
    ~Message() ;

    // Prevent compiler from defining the following
    Message(const Message &) ;            // Purposely leave unimplemented
    Message& operator=(const Message &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Issue any (formatted) message (called by all Verific code to issue a message to the user)
    static void         Msg(msg_type_t msg_type, const char *message_id, linefile_type linefile, const char *msg, ...) ;

    // Alternative call. Similar to the message function you can register. With va_list object rather than variable num args. Named different from previous call, to avoid overloading problems in compiler.
    static void         MsgVaList(msg_type_t msg_type, const char *message_id, linefile_type linefile, const char *msg, va_list args) ;

    // Control how many errors can be printed until we stop reporting (default is 20) :
    static void         SetMaxErrorCount(unsigned count) ;

    // Control if messages go to console (default is yes) :
    static void         SetConsoleOutput(unsigned console_output) ;

    // Control if messages go to a logfile :
    static unsigned     OpenLogFile(const char *log_file) ;
    static void         CloseLogFile() ;

    // Control if Message Id's get printed (default is no) :
    static void         PrintIds(unsigned print_ids = 0) ;

    // Control to force/unforce message types
    static unsigned     SetMessageType(const char *msg_id, msg_type_t type) ;   // Force a message type by message id
    static msg_type_t   GetMessageType(const char *msg_id) ;                    // Get the message type by message id
    static void         ClearMessageType(const char *msg_id) ;                  // Clear a message type by message id
    static void         SetAllMessageType(msg_type_t orig, msg_type_t type) ;   // Force all messages of type 'orig' to behave as type 'type'. VIPER 2695: allows to suppress all messages of some type.
    static void         ClearAllMessageTypes() ;                                // Clear all forced message types

    // Modify the message (formatted string) of a message id.
    // This feature is useful for language internationalization or other re-formatting of messages.
    static void         SetNewFormattedString(const char *msg_id, const char *new_msg) ; // reformat a message : Associate a message ID with a new formatted string.
    static void         ClearNewFormattedString(const char *msg_id) ; // clear new formatted message-id -> string association
    static void         ClearAllNewFormattedStrings() ; // clear all formatted message_id->string associations

    // Check and clear error count :
    static unsigned     ErrorCount() ;
    static void         ClearErrorCount() ;

    // Register a Message callback function (to be called from within Msg).
    // This callback function allows any application to re-direct Verific messages anywhere.
    // The callback function receives all info of the message separately :
    //   msg_type   : ERROR/WARNING/INFO etc from enumeration type msg_type_t
    //   message_id : a string ("VHDL-1067" or so) that uniquely identifies the message.
    //   linefile   : line and file info (extract linenr and filename with the Linefile class routines).
    //   msg, args  : formatted string (%s/%d in there etc) plus the arguments. Can be printed with vsprintf/vfprintf or so.
    static void         RegisterCallBackMsg(void(*msg_func)(msg_type_t msg_type, const char *message_id, linefile_type linefile, const char *msg, va_list args)) ;

    // Old style messages : no formatted messages, and no id's :
    // These routines still work since internally they now go through the new Msg function :

    // Old style : Print a comment (prefixed with --)
    static void         PrintLine(const char *txt1=0, const char *txt2=0, const char *txt3=0, const char *txt4=0, const char *txt5=0) ;

    // Old style messages, without ID, and up to 5 args in line:
    static void         Info(linefile_type linefile, const char *txt1=0, const char *txt2=0, const char *txt3=0, const char *txt4=0, const char *txt5=0) ;
    static void         Warning(linefile_type linefile, const char *txt1=0, const char *txt2=0, const char *txt3=0, const char *txt4=0, const char *txt5=0) ;
    static void         Error(linefile_type linefile, const char *txt1=0, const char *txt2=0, const char *txt3=0, const char *txt4=0, const char *txt5=0) ;

    // Old style message callback routines go to Register error/warning/info routines
    // These routines receive an expanded message ("<filename>(<linenr>): ERROR: <message>")
    static void         RegisterCallBackError(void(*error_func)(const char*)) ;
//CARBON_BEGIN
    static void         RegisterCallBackAlert(void(*alert_func)(const char*)) ;
//CARBON_END
    static void         RegisterCallBackWarning(void(*warning_func)(const char*)) ;
    static void         RegisterCallBackInfo(void(*info_func)(const char*)) ;

    // Embedded Verific SW Release Information (VIPER #2125)
    static time_t       ReleaseDate() ;   // Return time_t when Verific made SW Release to Customer
    static const char * ReleaseString() ; // Return SW Release string of when Verific made SW Release to Customer

    // Verific internal routine to control all messages as once.
    // They maintain a stack like mechanism so that multiple calls to them is handled properly.
    static void         StartIgnoreAllMessages()    { _ignore_all_messages++ ; }
    static void         StopIgnoreAllMessages()     { if (_ignore_all_messages) _ignore_all_messages-- ; }

    // Clear outputted message table
    static void         ClearOutputtedMessages() ;

private :
    // Error count
    static unsigned   _error_count ;
    static unsigned   _max_error_count ;

    // Table of (char*)message_id->(unsigned)message_type mappings.  Used to force particular
    // message formats to be outputted a certain way (Error, Warning, Info).
    static Map       *_forced_message_types ; // (char*)message_id->(unsigned)message_type mapping
    static Map       *_forced_all_message_types ; // msg_type_t -> msg_type_t Map. Let all messages of the key bahave as a message of the value. Handy to suppress all messages of some class.

    // Associations of message-id -> formatted messages.
    static Map       *_formatted_messages ; // associations between a message-id and a new formatted message.

    // Table of (char*)message->(void*)linefile entries.  Used to prevent redundant messages.
    static Map       *_outputted_messages ;

    // Flags :
    static unsigned   _console_output ;
    static unsigned   _print_ids ;

    // Callback routines
    static void     (*_msg_callback_func)(msg_type_t msg_type, const char *message_id, linefile_type linefile, const char *msg, va_list args) ;

    // Old style callback :
    static void     (*_error_func)(const char*) ;
//CARBON_BEGIN
    static void     (*_alert_func)(const char*) ;
//CARBON_END
    static void     (*_warning_func)(const char*) ;
    static void     (*_info_func)(const char*) ;

    // VIPER #4237: Linefile compare routine: exactly compare two linefiles, hash with file-id, right-col and leftline.
    static unsigned long  LineFileCompare(const void *key1, const void *key2) ;

    static unsigned   _ignore_all_messages ;
} ; // class Message

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_MESSAGE_H_

