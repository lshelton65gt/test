/*
 *
 * [ File Version : 1.84 - 2014/01/29 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <stdio.h>     // for sprintf and sscanf
#include <stdlib.h>    // for strtoul/atofi
#include <errno.h>     // for errno
#include <string.h>    // for memcpy
#include <ctype.h>     // tolower, toupper
#include <math.h>      // for pow

#include "Strings.h"
#include "Set.h"
#include "Array.h"

#define STRING_SET_SIZE 58

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif
/*------------------------------------------------*/
// String set is an efficient way of storing sets of strings
// it contains an array of size STRING_SET_SIZE(58) each string is hashed to
// on of the set depending on its first letter. The motivation
// is because djb2 hashing algorithm have collitions for strings
// that usually differs in the first letter
class VFC_DLL_PORT StringSet
{
public :
    // Constructors
    StringSet() ;
    // Destructor
    ~StringSet() ;

private :
    // Prevent compiler from defining the following
    StringSet(const StringSet &) ;            // Purposely leave unimplemented
    StringSet& operator=(const StringSet &) ; // Purposely leave unimplemented
public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS
    // Insertion method
    void        Insert(const char *key,             // Key value to insert
                           unsigned force_overwrite=0) const ;// If key exists, overwrite existing value with new value (rather than refuse insertion)
    // Retrieval methods
    void *      Get(const char *key) const ;        // Get item's key from table by key

private:

    Set   *_buckets[STRING_SET_SIZE] ;     // Allocated memory heap
} ; // class StringSet
/*------------------------------------------------*/

#ifdef VERIFIC_NAMESPACE
}
using namespace Verific ;
#endif

#define STRING_CHAIN_SIZE 4090

StringSet::StringSet()
{
     // Initialize _buckets with a new Set
     unsigned i = 0 ;
     for (;i<STRING_SET_SIZE;i++) {
         _buckets[i] = new Set(STRING_HASH) ;
     }
}

/*------------------------------------------------*/

StringSet::~StringSet()
{
    unsigned i = 0 ;
    for (;i<STRING_SET_SIZE;i++) {
         if (_buckets[i]) delete _buckets[i] ;
    }
}
/*------------------------------------------------*/
void
StringSet::Insert(const char* key, unsigned force_overwrite) const
{
    register unsigned idx = 0 ;
    if (!key) return ; // Insert failed
    idx = (unsigned)(key[0] - 'A') ;
    idx %= STRING_SET_SIZE ;
    _buckets[idx]->Insert(key, force_overwrite) ;
}
/*------------------------------------------------*/
void *
StringSet::Get(const char *key) const
{
    if (!key) return 0 ; // It's not there
    register unsigned idx = 0 ;
    idx = (unsigned)(key[0] - 'A') ;
    idx %= STRING_SET_SIZE ;
    return (_buckets[idx]->Get(key)) ;
}
/*------------------------------------------------*/

/*------------------------------------------------*/
// String Chain is a chain of strings of static char* array
// saving to string chain is basically coping the string to
// this larger string.

typedef struct str_data{
     char _str[STRING_CHAIN_SIZE] ;
     unsigned _allocate_index ;
     struct str_data *_next ;
} string_holder;

class VFC_DLL_PORT StringChain
{
public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    StringChain() ;
    ~StringChain() ;
    static char *     SavetoChain(const char *str) ;
    static void       CleanChain() ;

private :
    static Array *_large_strs ; // Store strings of size greater than STRING_CHAIN_SIZE
    static string_holder *_chain_head ;
    static string_holder *_curr_chain_head ;
} ; // class StringChain
/*------------------------------------------------*/

StringSet *Strings::_static_string_set = 0 ;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Strings::Strings()  { }
Strings::~Strings() { }

/*------------------------------------------------*/

/*static*/ unsigned long
Strings::len(const char *s)
{
    if (!s) return 0 ;
    return (unsigned long)strlen(s) ;
}

/*------------------------------------------------*/

/*static*/ const char *
Strings::CreateConstantString(const char *s)
{
    if (!s) return 0 ;
    if (!_static_string_set) _static_string_set = new StringSet() ;

    char *existing_string = (char *)_static_string_set->Get(s) ;
    if (existing_string) return existing_string ;

    // Save the Strings
    existing_string = StringChain::SavetoChain(s) ;
    //existing_string = Strings::save(s) ;
    _static_string_set->Insert(existing_string) ;

    return existing_string ;
}

/*------------------------------------------------*/

/*static*/ void
Strings::CleanupStaticStringSet()
{
    StringChain::CleanChain() ;
    if (!_static_string_set) return ;
/*
    SetIter si ;
    char *static_string ;
    FOREACH_SET_ITEM(_static_string_set, si, &static_string) {
        _static_string_set->Remove(static_string) ; // this does not invalidate iterator
        Strings::free(static_string) ;
    }
*/
    delete _static_string_set ;
    _static_string_set = 0 ;
}

/*------------------------------------------------*/

/*static*/ char *
Strings::save(const char *s)
{
    if (!s) return 0 ;
    // Save the Strings
    register unsigned size = (unsigned)strlen(s) + 1 ;
    register char *result = VFC_ARRAY_NEW(char, size) ;
    if (!result) return 0 ;
    return (char*)memcpy(result,s,size) ;
}

/*static*/ char *
Strings::save(const char *s1, const char *s2, const char *s3, const char *s4, const char *s5, const char *s6, const char *s7, const char *s8)
{
    if (!s1) return 0 ; // expect at least the first string to be valid

    // Concatenate and save the strings
    register unsigned len1 = (unsigned)strlen(s1) ; // s1 is always defined
    register unsigned len2 = (s2) ? (unsigned)strlen(s2) : 0 ;
    register unsigned len3 = (s3) ? (unsigned)strlen(s3) : 0 ;
    register unsigned len4 = (s4) ? (unsigned)strlen(s4) : 0 ;
    register unsigned len5 = (s5) ? (unsigned)strlen(s5) : 0 ;
    register unsigned len6 = (s6) ? (unsigned)strlen(s6) : 0 ;
    register unsigned len7 = (s7) ? (unsigned)strlen(s7) : 0 ;
    register unsigned len8 = (s8) ? (unsigned)strlen(s8) : 0 ;

    // Allocate the memory :
    register char *result = VFC_ARRAY_NEW(char, len1 + len2 + len3 + len4 + len5 + len6 + len7 + len8 + 1) ;

    // Concatenate the strings :
    register char* runner = result ;
    (void) memcpy(runner,s1,len1) ; runner += len1 ; // s1 is always defined
    if (s2) (void) memcpy(runner,s2,len2) ; runner += len2 ;
    if (s3) (void) memcpy(runner,s3,len3) ; runner += len3 ;
    if (s4) (void) memcpy(runner,s4,len4) ; runner += len4 ;
    if (s5) (void) memcpy(runner,s5,len5) ; runner += len5 ;
    if (s6) (void) memcpy(runner,s6,len6) ; runner += len6 ;
    if (s7) (void) memcpy(runner,s7,len7) ; runner += len7 ;
    if (s8) (void) memcpy(runner,s8,len8) ; runner += len8 ;

    *runner = '\0' ; // close the string
    return result ;
}

/*------------------------------------------------*/

/*static*/ void
Strings::free(char *s)
{
#if 0
    if (s && _static_string_set) {
        // This is redundant, as all entries in _static_string_set are
        // returned as const char *. Since we do not have any casting
        // (by our coding practice), Strings::free cannot be called on
        // those strings (compiler will shout loud)
        char *existing_string = (char *)_static_string_set->Get(s) ;
        if (existing_string == s) {
            // This string was created by CreateConstantString routine.
            // Someone did cast away the constant-ness.. This is not good
            // Should not free the string
            return ;
        }
    }
#endif
    if (!s) return ;
    VFC_ARRAY_DELETE(s) ;
}

/*------------------------------------------------*/

/*static*/ char *
Strings::BinToOct(const char *s)
{
    if (!s) return 0 ;

    unsigned size = (unsigned)Strings::len(s) ;
    unsigned oct_size = (size % 3) ? (size / 3 + 1) : size / 3 ;

    char *ret_ptr = Strings::allocate(oct_size + 1) ;
    ret_ptr[oct_size] = '\0' ;

    unsigned bit_2, bit_1, bit_0, idx ;
    for (idx = 0 ; idx < oct_size ; ++idx) {
        bit_2 = (idx*3+3 > size) ? 0 : (unsigned)(s[size-(3+idx*3)] - '0') ;
        bit_1 = (idx*3+2 > size) ? 0 : (unsigned)(s[size-(2+idx*3)] - '0') ;
        bit_0 = (idx*3+1 > size) ? 0 : (unsigned)(s[size-(1+idx*3)] - '0') ;
        if (bit_2 > 1 || bit_1 > 1 || bit_0 > 1) {
            Strings::free(ret_ptr) ; // 's' is not binary
            return 0 ;
        }
        ret_ptr[(oct_size-1)-idx] = (char)('0' + bit_2*4 + bit_1*2 + bit_0) ;
    }

    return ret_ptr ;
}

/*------------------------------------------------*/

/*static*/ char *
Strings::BinToHex(const char *s)
{
    if (!s) return 0 ;

    unsigned size = (unsigned)Strings::len(s) ;
    unsigned hex_size = (size % 4) ? (size / 4 + 1) : size / 4 ;

    char *ret_ptr = Strings::allocate(hex_size + 1) ;
    ret_ptr[hex_size] = '\0' ;

    unsigned bit_3, bit_2, bit_1, bit_0, idx ;
    for (idx = 0 ; idx < hex_size ; ++idx) {
        bit_3 = (idx*4+4 > size) ? 0 : (unsigned)(s[size-(4+idx*4)] - '0') ;
        bit_2 = (idx*4+3 > size) ? 0 : (unsigned)(s[size-(3+idx*4)] - '0') ;
        bit_1 = (idx*4+2 > size) ? 0 : (unsigned)(s[size-(2+idx*4)] - '0') ;
        bit_0 = (idx*4+1 > size) ? 0 : (unsigned)(s[size-(1+idx*4)] - '0') ;
        if (bit_3 > 1 || bit_2 > 1 || bit_1 > 1 || bit_0 > 1) {
            Strings::free(ret_ptr) ;
            return 0 ;
        }

        unsigned num = bit_3*8 + bit_2*4 + bit_1*2 + bit_0 ;
        ret_ptr[(hex_size-1)-idx] = num < 10 ? (char)('0' + num) : (char)('A' + (num-10)) ;
    }

    return ret_ptr ;
}

/*------------------------------------------------*/

/*static*/ verific_int64
Strings::BinToDec(const char* bin)
{
    unsigned length = Strings::len(bin) ;
    unsigned i ;
    double dec = 0 ;
    for (i = 0 ; i < length ; i++)
    {
        unsigned val = (bin[i] == '1') ? 1 : 0 ;
        dec = dec + (val * pow((double)2, (double)i)) ;
    }
    return (verific_int64)dec ;
}

/*static*/ char *
Strings::itoa(int i)
{
    // Translate integer to ascii. Allocate memory
    char buffer[16] ;
    sprintf(buffer,"%d",i) ;
    return save(buffer) ;
}

/*static*/ int
Strings::atoi(const char *s, unsigned *err)
{
    if (!s) return 0 ;
    // Translate ascii Strings to integer. No memory allocation.
    // We don't use ::atoi here, because we found there is a behavioral
    // platform difference between atoi on linux vs windows/solaris.  Thus,
    // the following solution is used instead.

    errno = 0 ; // Need to clear this, because on linux, errno seems to be sticky.

    // VIPER #5120 : If coming number is greater than 32 bits, try to return 32 bits
    // from lsb side of the number.
    // Generally 2**32 is a 10 digit number, so if coming number contains 10 digits
    // it can be a number greater than 32 bits. In that case convert the number
    // to 64 bits to return 32 bits from lsb.
    verific_uint64 num = 0 ;
    if (Strings::len(s) > 9) {
#ifdef WINDOWS
        // VC++ 6.0 does not support in the runtime C library.
        // Found online that _MSC_VER >= 1300 is needed for this function to be there.
#if (_MSC_VER >= 1300)
            num = _strtoui64(s, 0/*endPtr*/, 10/*base*/) ;
#else
            num = ::strtoul(s, 0 /*endPtr*/, 10/*base*/) ; // default to the 32 bit version
#endif
#else
        // UNIX platforms
        num = ::strtoull(s, 0 /*endPtr*/, 10/*base*/) ;
#endif
    } else {
        num = ::strtoul(s, 0 /*endPtr*/, 10/*base*/) ;
    }

    // Check to see if overflow/underflow occurred
    if (err) {
        *err = 0 ; // Clear this, just to be safe.
        if (errno == ERANGE) *err = 1 ;
    }

    // VIPER #3969: Check for overflow in 64bit machines where sizeof(long) > sizeof(int):
    verific_uint64 int_num = (unsigned int)num ;
    if ((int_num != num) && err) *err = 1 ; // overflow

    return (int)(num & 0xFFFFFFFF) ;
}
/*static*/ unsigned long
Strings::atoul(const char *s, unsigned *err)
{
    if (!s) return 0 ;
    // VIPER #3548.
    // Function atoul needed because we sometimes store addresses in
    // ascii strings, and a long is the only datatype wide enough
    // to acomidate both 32-bit and 64-bit addresses.

    errno = 0 ; // Need to clear this, because on linux, errno seems to be sticky.

    // VIPER #5120 : If coming number is greater than 32 bits, try to return 32 bits
    // from lsb side of the number in 32 bit machine and 64 bits from lsb in 64 bit machines.
    // Generally 2**32 is a 10 digit number, so if coming number contains 10 digits
    // it can be a number greater than 32 bits. In that case convert the number
    // to 64 bits to return proper value.
    verific_uint64 num = 0 ;
    if (Strings::len(s) > 9) {
#ifdef WINDOWS
        // VC++ 6.0 does not support _strtoui64 in the runtime C library.
        // Found online that _MSC_VER >= 1300 is needed for this function to be there.
#if (_MSC_VER >= 1300)
            num = _strtoui64(s, 0/*endPtr*/, 10/*base*/) ;
#else
            num = ::strtoul(s, 0 /*endPtr*/, 10/*base*/) ; // default to the 32 bit version
#endif
#else
        // UNIX platforms
        num = ::strtoull(s, 0 /*endPtr*/, 10/*base*/) ;
#endif
    } else {
        num = ::strtoul(s, 0 /*endPtr*/, 10/*base*/) ;
    }

    // Check to see if overflow/underflow occurred
    if (err) {
        *err = 0 ; // Clear this, just to be safe.
        if (errno == ERANGE) *err = 1 ;
    }

    // Check if there was overflow on the 'unsigned long' number :
    verific_uint64 long_num = (unsigned long)num ;
    if ((long_num != num) && err) *err = 1 ; // overflow

    // Return the number itself for 64 bit machines, no need to extract bits
    return (sizeof(long) > sizeof(int)) ? (unsigned long)num : (unsigned long)(num & 0xFFFFFFFF) ;
}
/*static*/ char *
Strings::dtoa(double d)
{
    // The following logic is needed to ensure that a real value
    // gets printed properly, not losing any precision or width,
    // and eliminating any useless trailing zeros, which always
    // seems to occur.
    char buf[30];
    char *p = buf;
    char *zero_mark;

    sprintf(buf, "%#.15G", d);
    while((*p) && (*p != '.')) p++;

    if ((*p=='.') && !(*(p+1))) { *(p+1) = '0' ; *(p+2) = '\0' ; }
    if (*p) p += 2; // Move to second digit after decimal.  We always want
                    // at-least one decimal digit to be printed.

    while (p && (*p)) {
        // Find next zero digit
        // VIPER #7068: Do not go past the 'E' if there for scientifc notation:
        while(*p && (*p != '0') && (*p != 'E')) p++;
        // Record current zero digit location
        zero_mark = p;
        // Find next non-zero digit
        while(*p && *p == '0') p++;

        if (*p == 'E') { // Are we at the end of a scientifc notation number?
            // VIPER #7068: strcpy() is not safe for overlapping strings, use memmove() instead:
            //strcpy(zero_mark, p);
            (void) memmove(zero_mark, p, strlen(p)+1 /* include \0 too */) ;
            p = 0;  // Exit
        } else if (*p && *p != '0') { // Is there more info to parse?
            // Do nothing.  Loop around
        } else {
            *zero_mark = 0;
            p = 0;  // exit
        }
    }

    return Strings::save(buf) ;
}

/*static*/ double
Strings::atof(const char *s, unsigned *err /*=0*/)
{
    //return ::atof(s);
    if (!s) return 0 ;

    // Translate ascii Strings to double. No memory allocation.
    // We don't use ::atof here, because it does not detect overflow/underflow.
    // Thus, the following solution is used instead.

    errno = 0 ; // Need to clear this, because on linux, errno seems to be sticky.

#ifdef WINDOWS
    // Windows version of strtod cannot properly convert the string "inf" or
    // hexadecimal strings starting with 0x. We do not need that, though.
#endif
    // VIPER #7715: Use strtod() instead of atof() to detect overflow/underflow:
    double num = ::strtod(s, 0 /*endPtr*/) ;

    // Check to see if overflow/underflow occurred
    if (err) {
        *err = 0 ; // Clear this, just to be safe.
        if (errno == ERANGE) *err = 1 ;
    }

    return num ;
}

/* static */ char *
Strings::strtolower(char *s)
{
    if (!s) return 0;
    unsigned char c ;
    for (char *tmp = s ; (*tmp); tmp++) {
        // VIPER #3353: Need to consider extended upper case character set in ISO 8859-1
        // (8-bit character set) also. So, cannot simply use tolower for all. Use character
        // codes from ISO 8859-1 character set instead of character literals itself:
        c = (unsigned char)(*tmp) ; // Cast to unsigned char to do value compares
        if ((c >= 0xC0 /* � */) && (c <= 0xDE /* � */) && (c != 0xD7 /* � */)) {
            *tmp = *tmp + 32 ; // ('�' - '�') = 0xE0 - 0xC0 = 0x20
        } else {
            // Normal and all other characters go via library tolower:
            *tmp = (char)::tolower(*tmp) ;
        }
    }

    return s ;
}

/* static */ char *
Strings::strtoupper(char *s)
{
    if (!s) return 0;
    unsigned char c ;
    for (char *tmp = s ; (*tmp); tmp++) {
        // VIPER #3353: Need to consider extended lower case character set in ISO 8859-1
        // (8-bit character set) also. So, cannot simply use tolower for all. Use character
        // codes from ISO 8859-1 character set instead of character literals itself:
        c = (unsigned char)(*tmp) ; // Cast to unsigned char to do value compares
        if ((c >= 0xE0 /* � */) && (c <= 0xFE /* � */) && (c != 0xF7 /* � */)) {
            *tmp = *tmp - 32 ; // ('�' - '�') = 0xE0 - 0xC0 = 0x20
        } else {
            // Normal and all other characters go via library toupper:
            *tmp = (char)::toupper(*tmp) ;
        }
    }

    return s ;
}

/* static */ unsigned
Strings::ispurelower(char *s)
{
    unsigned char c ;
    for (char *tmp = s ; (*tmp); tmp++) {
        c = (unsigned char)(*tmp) ; // Cast to unsigned char to do value compares
        if ((c > 'z') || (c < 'a'))
           return 0 ;
    }
    return 1 ;
}

/* static */ unsigned
Strings::containupper(char *s)
{
    unsigned char c ;
    for (char *tmp = s ; (*tmp); tmp++) {
        c = (unsigned char)(*tmp) ; // Cast to unsigned char to do value compares
        if ((c <= 'Z') && (c >= 'A'))
           return 1 ;
    }
    return 0 ;
}

/*------------------------------------------------*/

/*static*/ unsigned
Strings::compare(const char *str1, const char *str2)
{
    if (str1 == str2) return 1 ;
    if (!str1 || !str2) return 0 ;
    while (*str1 == *(str2++)) {
        if (*(str1++)=='\0') return 1 ; // They are the same
    }
    return 0 ; // They are different
}

/*static*/ unsigned
Strings::compare_nocase(const char *str1, const char *str2)
{
    if (str1 == str2) return 1 ;
    if (!str1 || !str2) return 0 ;
    while (::tolower(*str1) == ::tolower(*(str2++))) {
        if (*(str1++)=='\0') return 1 ; // They are the same
    }
    return 0 ; // They are different
}

/*------------------------------------------------*/

/*static*/ char *
Strings::allocate(unsigned long size)
{
    // RD:
    // This routine actually allocated one byte more that 'size'.
    // Not sure why that was done, but some application code uses that extra byte.
    // Also, a NULL byte is set as the first character.
    // Not sure why that was done, but some application code uses that info.
    // So, for historical reasons (backward compatibility) we leave this code like this.
    char *str = VFC_ARRAY_NEW(char, size+1) ;
    str[0] = '\0';
    return str;
}

/*------------------------------------------------*/
string_holder * StringChain::_chain_head = 0 ;
string_holder * StringChain::_curr_chain_head = 0 ;
Array * StringChain::_large_strs = 0 ;

// StringChain APIs. Class defined above
StringChain::StringChain() { }
StringChain::~StringChain() { }

/*static*/ void
StringChain::CleanChain()
{
    string_holder *curr = _chain_head ;
    while (curr) {
        _chain_head = _chain_head->_next ;
        delete (curr) ;
        curr = _chain_head ;
    }
    if (_large_strs) {
        unsigned i ;
        char *str ;
        FOREACH_ARRAY_ITEM(_large_strs, i, str) {
            if (str) Strings::free(str) ;
        }
        delete _large_strs ;
        _large_strs = 0 ;
    }
}

/*static*/ char *
StringChain::SavetoChain(const char *str)
{
    if (!str) return 0 ;
    unsigned len = strlen(str) ;
    if (len + 1 > STRING_CHAIN_SIZE) {
        if (!_large_strs) _large_strs = new Array() ;
        char *s = Strings::save(str) ;
        _large_strs->Insert(s) ;
        return s ;
    }
    if (!_chain_head) {
        _chain_head = new string_holder ;
        _chain_head->_allocate_index = 0 ;
        _chain_head->_str[0] = '\0' ;
        _chain_head->_next = 0 ;
        _curr_chain_head = _chain_head ;
    }
    if (_curr_chain_head->_allocate_index + len + 1> STRING_CHAIN_SIZE) {
        _curr_chain_head->_next = new string_holder ;
        _curr_chain_head = _curr_chain_head->_next ;
        _curr_chain_head->_allocate_index = 0 ;
        _curr_chain_head->_str[0] = '\0' ;
        _curr_chain_head->_next = 0 ;
    }
    strcpy(_curr_chain_head->_str + _curr_chain_head->_allocate_index, str) ;
    char *ret_str = _curr_chain_head->_str + _curr_chain_head->_allocate_index ;
    _curr_chain_head->_allocate_index += len + 1 ;
    return (ret_str) ;
}
