/*
 *
 * [ File Version : 1.7 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_STREAM_H_
#define _VERIFIC_STREAM_H_

#include <istream>

#include "VerificSystem.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------- */

// Abstract base class for reading from streams for flex.
class VFC_DLL_PORT verific_stream
{
protected: // Abstract class
    verific_stream() : _gcount(0) { }

public:
    virtual ~verific_stream() { }

private:
    // Prevent compiler from defining the following
    verific_stream(const verific_stream &) ;            // Purposely leave unimplemented
    verific_stream& operator=(const verific_stream &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    virtual unsigned          eof()  const = 0 ;
    virtual unsigned          fail() const = 0 ;
    virtual unsigned          bad()  const = 0 ;

    virtual unsigned          read(char *buf, long max_size) = 0 ;
    virtual long              gcount() const { return _gcount ; } // Every call to read() must set _gcount

protected:
    long       _gcount ; // The number of characters read by the last 'read()'
} ; // class verific_stream

/* -------------------------------------------------------------- */

// Class to read from any istream object. The passed stream object is absorbed.
class VFC_DLL_PORT verific_istream : public verific_stream
{
public:
    explicit verific_istream(std::istream *stream) : verific_stream(), _istream(stream) { }

    virtual ~verific_istream() { delete _istream ; /* absorb the stream! */ }

private:
    // Prevent compiler from defining the following
    verific_istream() ;                                   // Purposely leave unimplemented
    verific_istream(const verific_istream &) ;            // Purposely leave unimplemented
    verific_istream& operator=(const verific_istream &) ; // Purposely leave unimplemented

public:
    // Route all the APIs to the underlying istream object:
    virtual unsigned          eof()  const { return (_istream) ? _istream->eof()  : 1 ; }
    virtual unsigned          fail() const { return (_istream) ? _istream->fail() : 1 ; }
    virtual unsigned          bad()  const { return (_istream) ? _istream->bad()  : 1 ; }

    virtual unsigned          read(char *buf, long max_size)
                              {
                                  if (!_istream) return 0 ;

                                  // Read from the underlying istream:
                                  (void) _istream->read(buf, max_size) ;
                                  _gcount = _istream->gcount() ;
                                  return((_istream->bad()) ? 0 : 1) ;
                              }

protected:
    std::istream    *_istream ;
} ; // class verific_istream

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_STREAM_H_

