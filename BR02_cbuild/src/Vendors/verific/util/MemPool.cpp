/*
 *
 * [ File Version : 1.35 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

//#define VERIFIC_DEBUG_MEMORY_MANAGER

#ifdef VERIFIC_DEBUG_MEMORY_MANAGER
#include <stdio.h>  // For fprintf/FILE *
#include <map>      // For STL 'map'
#endif

#include <memory.h> // For memcpy/memset

#include "MemPool.h"
#include "Message.h"

#define MM_BLOCK_SIZE 1048576  // Block size (1M)
//#define MM_BLOCK_SIZE 65536  // Block size (64k)
#define MM_NUM_SIZE_SLOTS 256  // Number of (size) slots that can be encoded with the 1 byte we have available.

// Define when a non-active block should be put on the 'recycle' list.
// Default is 1/4 the memory of the block should have been deleted.
#define MM_START_RECYCLE ((3*sizeof(MemBlock))/sizeof(void*)/4)
#define MM_RECYCLE_BLOCKS_TO_SCAN 4 // maximum number of recycled blocks on list to try, if we are about to rellocate

#ifdef VERIFIC_DEBUG_MEMORY_MANAGER
// The file pointer to write debug information:
static FILE *_debug_fp = 0 ;

// This 'map' stores every allocated memory pointer vs. an unique number:
static std::map <unsigned long /* pointer */, unsigned long /* number */> *_allocated_memory = 0 ;

// The total number of MemPool::New() calls:
static unsigned long _num_allocations = 0 ;

// The path/name of the debug file:
#define DEBUG_LOG "/tmp/verific-mm-debug.log"

// Macro to debug log MemPool::New() call:
#define DEBUG_LOG_MM_NEW(result, size) \
    if (_debug_fp && result) { \
        fprintf(_debug_fp, "%d\n", size) ; \
        unsigned long res = (unsigned long)result ; \
        (*_allocated_memory)[res] = _num_allocations ; \
        _num_allocations++ ; \
    }
#else
// We are not debugging the memory manager, so do nothing:
#define DEBUG_LOG_MM_NEW(result, size)
#endif // VERIFIC_DEBUG_MEMORY_MANAGER

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
namespace Verific { // start definitions in verific namespace
#endif

// Initialize the global mempool single object:
/* static */ GlobalMemPool *GlobalMemPool::_global_pool = 0 ;

/*------------------------------------------------*/

class MemBlock
{
public:
    MemBlock(MemPool *pool,unsigned block_nr, MemBlock *next) ;
    ~MemBlock() ;

private:
    // Prevent compiler from defining the following
    MemBlock() ;                            // Purposely leave unimplemented
    MemBlock(const MemBlock &) ;            // Purposely leave unimplemented
    MemBlock& operator=(const MemBlock &) ; // Purposely leave unimplemented

public:
    // Catch new/delete of a MemPool, in case global operator new/delete are overridden
    // VIPER #7585: Use 'new', 'delete' to allocate/deallocate memory instead of malloc/free:
    //static void * operator new(size_t size)  { return ::malloc(size) ; }
    //static void   operator delete(void *p)   { ::free(p) ; }
    static void * operator new(size_t size)  { return (void *)(new /* array */ char [size]) ; }
    static void   operator delete(void *p)   { delete [] (char *)p ; }

    void          Reset() ;
    char *        Allocate(unsigned index) ;
    void          Free(void *p) ;

public:
    MemPool    *_pool ; // memory pool that this block is under
    MemBlock   *_next ; // list of blocks with same block number
    unsigned    _block_nr ;      // block number
    unsigned    _used_space ;    // space given out (4-byte chucks)
#if 0 // no longer need '_largest_garbage' field
    unsigned    _largest_garbage:10 ; // space of largest unbroken garbage item (4-byte chucks)
#endif

    // the 'recycle' list of blocks : a FIFO: push into recycle_in, remove from recycle_out.
    unsigned    _is_on_recycle_list:1 ; // flag that tells if block is on the recycle list
    MemBlock   *_recycle_in ;
    MemBlock   *_recycle_out ;

    char       *_free ;            // pointer to current free space in the block
    char       *_block_end ;       // points to end of block (next char after data)
    // garbage
    void       *_garbage[MM_NUM_SIZE_SLOTS] ; // 256 garbage lists (index on size (/4))
    // data
    char        _empty[4] ;            // empty bytes before data (needed for first fragment).
    char        _data[MM_BLOCK_SIZE] ; // data in char*'s.
} ; // class MemBlock

/*------------------------------------------------*/

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

/*------------------------------------------------*/

MemBlock::MemBlock(MemPool *pool,unsigned block_nr, MemBlock *next)
  : _pool(pool),
    _next(next),
    _block_nr(block_nr),
    _used_space(0),
#if 0 // no longer need '_largest_garbage' field
    _largest_garbage(0),
#endif
    _is_on_recycle_list(0),
    _recycle_in(0),
    _recycle_out(0),
    _free(0),
    _block_end(0)
{
    // Clear the block :
    Reset() ;
}

MemBlock::~MemBlock()
{
    // remove myself from the pool :
    // First check where I am in the list of blocks :
    // It is not the first one. Take it out halfway. Find the previous block :
    MemBlock *header = _pool->_blocks[_block_nr] ;
    if (this==header) {
        // It is the first one in the list. Take it out (of the list):
        _pool->_blocks[_block_nr] = _next ;
    } else {
        // It's not the first in the list. Traverse :
        // FIX ME : create a double-link list so we don't have to traverse the list to find the 'previous'
        while (header->_next != this) header = header->_next ;
        // Got previous block. Take blk out of the list :
        header->_next = _next ;
    }

    if (_is_on_recycle_list) {
        _pool->StopRecycle(this) ; // take me off the recycle list (if there)
    }
}

/*------------------------------------------------*/

void
MemBlock::Reset()
{
    // Clear out entire block memory. Similar to deleting and re-allocating entire block
    // Initialize garbage lists with 0's :
    ::memset(_garbage, 0, MM_NUM_SIZE_SLOTS*sizeof(void*)) ;

    // JJ 130110 initialize _empty w/ 0's
    ::memset(_empty, 0, 4) ;

    // Set freespace pointer :
    _free = _data ;
    // Set block_end pointer :
    _block_end = _free + MM_BLOCK_SIZE ;

    // Align the first free pointer to the alignment type :
    // For example, Solaris has align-type double, which is 8 bytes, which means 3 LSB bits must be 0, which means mask should be 7.
    while ((long)_free & (sizeof(MM_ALIGN_TYPE)-1)) _free++ ;

    // Set to no data allocated :
    _used_space = 0 ;
#if 0 // no longer need '_largest_garbage' field
    _largest_garbage = 0 ;
#endif

    // And take it off the recycle list (if there)
    if (_is_on_recycle_list) {
        _pool->StopRecycle(this) ;
    }
}

/*------------------------------------------------*/

char *
MemBlock::Allocate(unsigned index)
{
    // if (size==0) return 0 ;

    // See if we have a garbage element for this guy :
    void **garbage_slot = _garbage + index ;
    if (*garbage_slot) {
        // _space_in_garbage -= use_size ;
        // yep. we have a garbage element for this guy.
        // Take it off the garbage list :
        void **item = (void**)*garbage_slot ; // Item is currently a void* to 'next' item in garbage list.
        *garbage_slot = *item ; // let garbage slot itself point to that next item in garbage list
        // The index is already set at index -1, so no need to do that again.
        _used_space += index ; // amount of space in this item.
        // Return as a string :
        return (char*)item ;
    }

    // Try to get it off the heap.
    // Calculate where the next pointer would go if we take memory from the (free) heap :
    char *new_free = _free + index*sizeof(void*) ;
    if (new_free < _block_end) {
        // take it off the heap.
        // Result is current _free pointer :
        char *result = _free ;
        // Set index into byte before this :
        result[-1] = index ; // Store index (into garbage list) in the byte before item
        result[-2] = _block_nr ; // Store block number one char before that.

        // Set free pointer in the heap to next free space :
        _free = new_free ;
        _used_space += index ;

        return result ; // return that (free space)
    }

    // RD: disabled the following section because :
    // Even though this method will allow us to stay within one block a little longer,
    // it will chop up the largest fragments, which are most valuable at recycling time.
    // Besides that, the search for a large slot can be somewhat time consuming.
#if 0
    // Try to salvage it from a larger fragment on the garbage list :
    // Go to that slot :
    garbage_slot = _garbage + _largest_garbage ;

    // If there is no garbage at this slot, iterate down to find the current largest garbage fragment.
    // This is all a bit slow, but should not happen often.
    while (!(*garbage_slot)) {
        if (_largest_garbage <= index) break ;
        garbage_slot-- ;
        _largest_garbage-- ;
    } ;

    // If the largest slot in the garbage list is easily large enough, then go and chop it up :
    if (*garbage_slot && (_largest_garbage > index+2)) {
        // Got biggest garbage item, and it is large enough for this item
        // Take this item off the garbage list :
        void **item = (void**)*garbage_slot ; // Item is currently a void* to 'next' item in garbage list.
        *garbage_slot = *item ; // let garbage slot itself point to that next item in garbage list

        // Change the setting of the 'size' of this item to the requested size :
        ((unsigned char*)item)[-1] = index ;

        // Create a valid item for this remaining chuck of memory :
        void **remaining = item + index ;
        ((unsigned char*)remaining)[-1] = _largest_garbage - index ;  // its new size
        ((unsigned char*)remaining)[-2] = _block_nr ;  // its block number
        // Find the garbage slot for this
        garbage_slot = _garbage + (_largest_garbage - index) ;

        // Push 'remaining' back on the garbage list :
        *remaining = *garbage_slot ;
        *garbage_slot = remaining ;

        // Message::Msg(VERIFIC_INFO,0,0,"salvaged %d from %d slot", index, i) ;

        // used 'index' space. Rest pushed back on garbage list
        _used_space += index ; // amount of space in this item.

        // return the original item
        return (char*)item ;
    }
#endif
    // Here, failed to find space for this chuck in this block.
    // Message::Msg(VERIFIC_INFO,0,0,"failed garbage collection asking for a %d slot",index) ;

    // Need a new block. Tell this to the manager :
    return 0 ;
}

/*------------------------------------------------*/

void
MemBlock::Free(void *p)
{
    // unsigned char *str = (unsigned char*)p ;

    // Get the index from the byte before this one :
    unsigned char index = ((unsigned char*)p)[-1] ;

    // Push this thing on the garbage list :

    // Find the garbage list entry for this fragment (with this size index)
    // VERIFIC_ASSERT(index) ;
    // void **item = (void**)p ;
    void **garbage_slot = _garbage + index ; // index ;

    // Push this item on the garbage list :
    *((void**)p) = *garbage_slot ; // Set the current head of the list as next in this item
    *garbage_slot = p ; // Set the head of the list to the item

#if 0 // no longer need '_largest_garbage' field
    // update 'largest garbage item'
    if (_largest_garbage < index) _largest_garbage = index ;
#endif

    // update space given out :
    _used_space -= index ;
}

/*------------------------------------------------*/

// initialize memory manager
MemPool::MemPool()
  : _current(0),
    _free_slot(0),
    _block_count(1),
    _peak_block_count(1),
    _passed_through(0), // space ((void*)size-byte chunks) passed through to OS.
    _recycle_in(0),
    _recycle_out(0)
{
    // Initialize blocks with all 0's :
    ::memset(_blocks, 0, MM_NUM_BLOCK_SLOTS*sizeof(void*)) ;

    _free_slot = 1 ;

    // Create one block and make it current :
    _blocks[_free_slot] = new MemBlock(this,_free_slot,_blocks[_free_slot]) ;

    _current = _blocks[_free_slot] ;

    _free_slot++ ;

#ifdef VERIFIC_DEBUG_MEMORY_MANAGER
    if (!_debug_fp) {
        // Open the file in read/write mode (truncate existing), if it is not already opened:
        _debug_fp = fopen(DEBUG_LOG, "w+") ;
        if (_debug_fp) {
            Message::Msg(VERIFIC_INFO, 0, 0, "All New/Delete calls to Verific Memory Manager are logged in file %s", DEBUG_LOG) ;
            // Write number of allocation wth size 32 at the beginning as place holder, we would like
            // to update the total number of MemPool::New() calls here in this position at the end again.
            fprintf(_debug_fp, "%32ld\n", _num_allocations) ;
        }
    }
    // Create the map, if it is not already created:
    if (!_allocated_memory) _allocated_memory = new std::map<unsigned long, unsigned long> ;
#endif
}

/*------------------------------------------------*/

MemPool::~MemPool()
{
    // Delete all blocks.
    unsigned i ;
    MemBlock *blk, *prev ;
    for (i=0; i<MM_NUM_BLOCK_SLOTS; i++) {
        blk = _blocks[i] ;
        while (blk) {
            // Walk down the linked list and delete all blocks in this slot.
            prev = blk ;
            blk = blk->_next ;
            delete prev ;
        }
    }

#ifdef VERIFIC_DEBUG_MEMORY_MANAGER
    if (_debug_fp) {
        // Done writing the debug information, now we need to write the number of times
        // MemPool::New() was called at the beginning, so, rewind the file pointer now:
        rewind(_debug_fp) ;
        // Write the number with size 32 so that we overwrite all the 0's written there:
        fprintf(_debug_fp, "%32ld\n", _num_allocations) ;
        // Close the file and reset it so that we do not use it anymore.
        // Note that there can be more than one object of MemPool for ColLineFile:
        fclose(_debug_fp) ;
        _debug_fp = 0 ;
    }
    // Reset the map as well:
    delete _allocated_memory ;
    _allocated_memory = 0 ;
#endif
}

void
MemPool::Reset()
{
    // Delete all blocks.
    // (Same as what we do in destructor)
    unsigned i ;
    MemBlock *blk, *prev ;
    for (i=0; i<MM_NUM_BLOCK_SLOTS; i++) {
        blk = _blocks[i] ;
        while (blk) {
            // Walk down the linked list and delete all blocks in this slot.
            prev = blk ;
            blk = blk->_next ;
            delete prev ;
        }
    }

    // Now re-initialize
    // (Same as what we do in constructor)
    _current = 0 ;
    _free_slot = 0 ;
    _block_count = 1 ;
    _peak_block_count = 1 ;
    _passed_through = 0 ; // space ((void*)size-byte chunks) passed through to OS.
    _recycle_in = 0 ;
    _recycle_out = 0 ;

    // Initialize blocks with all 0's :
    ::memset(_blocks, 0, MM_NUM_BLOCK_SLOTS*sizeof(void*)) ;

    _free_slot = 1 ;

    // Create one block and make it current :
    _blocks[_free_slot] = new MemBlock(this,_free_slot,_blocks[_free_slot]) ;

    _current = _blocks[_free_slot] ;

    _free_slot++ ;
}

/*------------------------------------------------*/

void *
MemPool::New(unsigned size)
{
    // if (size==0) return 0 ;

    // Code guarantees that there is always a 'current' block to allocate from :
    // VERIFIC_ASSERT(_current) ;

    // Calculate index (number of void* of space we need :
    unsigned index = (size+1)/sizeof(void*) ; // size 0,1,2 gets us index 0 (allocate 1 void*). 3rd and 4th byte is for blk number and size index for next fragment.
    if (!index) index++ ; // need 4 bytes minimum for garbage 'next' pointer. So cannot handle index==0.
    index++ ; // need one void* on top of index for size adjustment (0 index means 0,1,2 bytes (allocate 1 void*), 1 means 3,4,5,6 bytes (allocate 2 void*))

    // Now, index is the exact number of void*'s that needs to get allocated from the block.

    // Alignment : C++ does define a fixed alignment type. Differs per hardware platform.
    // Most platforms allow pointer-aligned chunks of memory to be returned by new/delete.
    // However, Solaris requires 'double' alignment.
    // Check the alignment type, and make sure that 'index' is always allocating fragments of alignment type size.
    // That way, all memory fragments will be alignment type aligned.
    // Not sure if there is a faster way to do this.
    while ( (index*sizeof(void*)) & (sizeof(MM_ALIGN_TYPE)-1) ) index++ ; // Example : double is 8 bytes, which means 3 LSB bits must be 0, which means mask should be 7.

    // See if we can handle this one :
    if ((index >= MM_NUM_SIZE_SLOTS) || !_current) {
        // Too large an object (or this memory manager is not initialized yet)
        // Go to the system to get this memory.
        // Allocate one void* more than needed (since we still need to store our 2 bytes extra data)
        // AND we need to return a alignment-type aligned memory chunk.
        // VIPER #7585: We do not care about 'new' being overridden as long as it allocates/returns the chunk:
        //char *result = (char*) ::malloc(size + sizeof(MM_ALIGN_TYPE)) ; // don't use 'new' since it can be overridden
        char *result = new /* array */ char [(size + sizeof(MM_ALIGN_TYPE))] ; // So, use 'new' here for VIPER #7585
        result = result + sizeof(MM_ALIGN_TYPE) ;
        // Set index into byte before this :
        result[-1] = 0 ; // Store 0 index (into garbage list) in the byte before item
        result[-2] = 0 ; // Store block number 0 to indicate that this is a system-allocated piece of data.
        _passed_through += index ;
        DEBUG_LOG_MM_NEW(result, size) ; // Debug log this MemPool::New() call
        return result ;
    }

    // Try to get this from current block :
    char *result = _current->Allocate(index) ;
    DEBUG_LOG_MM_NEW(result, size) ; // Debug log this MemPool::New() call
    if (result) return result ;

    // Cannot get from current block. Means this block is full.

    // Find a block on the recycle list that has enough space :
    MemBlock *recycle = _recycle_out ;

    // Since we continue to use a new block until its heap is all chopped up, all blocks will now be fragmented.
    // But some blocks may have a lot of garbage (freed) fragments.
    // VIPER 5858 shows how that causes memory degration if we don't switch back to an older block.
    // So, find a block that has a lot of garbage and find the piece there.
    // Do that by using a "recycle" list of blocks on the mem pool.
    // If we find it, make that block "current".

    // We could iterate over all the blocks in the recycle list,
    // but that creates an O(N!) complexity problem if the requested fragment is not in any of the recycled blocks.
    // So take only the first MM_RECYCLE_BLOCKS_TO_SCAN (oldest, and probably emptiest) recycled blocks.
    // Experiments show that this (fast) method does not noticably increase new block-allocations
    // even in heavy recycle requests (free..allocate..free..allocate cycles) on the memory pool.
    // In fact, since past memory fragment size allocation patterns tend to repeat in cycles,
    // recycled blocks, once made current, tend be re-used almost completely.
    unsigned count = 0 ;
    while (recycle) {
        // try this block
        result = recycle->Allocate(index) ;
        if (result) {
            // success. Make this block 'current' and take it off the recyle list
            _current = recycle ;
            StopRecycle(recycle) ;
            DEBUG_LOG_MM_NEW(result, size) ; // Debug log this MemPool::New() call
            return result ;
        }
        // try other blocks in the recycle list:
        recycle = recycle->_recycle_out ;
        if (++count > MM_RECYCLE_BLOCKS_TO_SCAN) break ; // put limit on how many blocks we try
    }

    // Create a new block and make it current :
    _current = new MemBlock(this,_free_slot,_blocks[_free_slot]) ;
    // VIPER #7585: Following code is never executed since in case of out-of-memory, the constructor
    // is still called which core dumps. No use of keeping the code alive. Commenting it out:
    //if (!_current) {
    //    // Oops. system is out of memory
    //    // There may be a bit memory left over, but not enough for a new block.
    //    //
    //    // FIX ME :Could try to salvage this chunk from garbage in another block,
    //    // but we are definitely in crisis mode here.

    //    // Issue a message :
    //    Message::Msg(VERIFIC_ERROR,0,0,"system memory manager is out of memory") ;
    //    // And print memory statistics :
    //    MemStats() ;
    //
    //    // Have to return 0.
    //    return 0 ;
    //}

    _blocks[_free_slot] = _current ;
    _free_slot++ ;
    _block_count++ ;
    if (_block_count > _peak_block_count) _peak_block_count = _block_count ;
    //MemStats() ;

    // Check 256Mb rotation limit :
    if (_free_slot == MM_NUM_BLOCK_SLOTS) {
        // Simply go back to slot 1 (and build next layer of blocks)
        // FIX ME : Could do a bit better here by finding slot with least amount of blocks.
        _free_slot = 1 ;
    }

    // Get the data from there :
    result = _current->Allocate(index) ;
    DEBUG_LOG_MM_NEW(result, size) ; // Debug log this MemPool::New() call
    return result ;
}

/*------------------------------------------------*/

void
MemPool::Delete(void *p)
{
    // cast to unsigned char :
    unsigned char *str = (unsigned char*)p ;
    if (!str) return ;

#ifdef VERIFIC_DEBUG_MEMORY_MANAGER
    if (_debug_fp && _allocated_memory) {
        // Write down the -ve of the unique number associated with this pointer:
        fprintf(_debug_fp, "-%ld\n", (*_allocated_memory)[(unsigned long)p]) ;
        (*_allocated_memory).erase((unsigned long)p) ; // Remove it to free memory
    }
#endif

    // Get the block number from the byte before this one :
    // unsigned index = (unsigned)str[-2] ;

    // Push this thing on the garbage list :
    // Check which block this is :
    MemBlock *blk = _blocks[str[-2]] ;

    // Check if we have multiple blocks (with same blockid) in this slot.
    // If so, then we need to get to the right block (where this data came from) :
    while (blk && blk->_next) {
        if (p > blk && (p < blk->_block_end)) break ; // address is within this block. Take it.
        blk = blk->_next ; // Check next block
    }

    if (!blk) {
        // Must be allocated from the system.
        // Check that it went through this memory manager :
        VERIFIC_ASSERT(str[-2]==0) ; // block number should be 0.
        VERIFIC_ASSERT(str[-1]==0) ; // size should be set to 0.

        // Go back sizeof(MM_ALIGN_TYPE) bytes (since we allocated that prefix during New()) :
        str = str - sizeof(MM_ALIGN_TYPE) ;
        // Delete that using the system :
        // VIPER #7585: We used 'new' to allocate memory, use 'delete' here:
        //::free(str) ; // call system delete. Was allocated with malloc.
        delete /* array */ [] str ;
        return ;
    }

    // Free from this block :
    blk->Free(p) ;

    if (blk != _current && !blk->_is_on_recycle_list && (blk->_used_space < MM_START_RECYCLE)) {
        // block is not the current block (so it has once been allocated in full)
        // and now it reached the 'START_RECYCLE' boundary.
        // Put blk on recycle list
        StartRecycle(blk) ;
    }

    if (!blk->_used_space) {
        // Block is empty.
        if (_current == blk) {
            // Keep it around (and current), but reset it :
            blk->Reset() ;
        } else {
            // Delete if it is not 'current'.

            // Now the block is free to go. Delete it:
            delete blk ;
            _block_count-- ;
        }
        //MemStats() ;
    }
}

/*------------------------------------------------*/

// Recycle list management
void
MemPool::StartRecycle(MemBlock *block)
{
    if (!block) return ;

    VERIFIC_ASSERT(!block->_is_on_recycle_list) ; // make sure this block is not yet in recycle list.
    block->_is_on_recycle_list = 1 ;

    // Insert this block at the 'in' side the pool's recycle list :
    if (_recycle_in) {
        // There are already blocks in the recycle list
        VERIFIC_ASSERT(!_recycle_in->_recycle_out) ; // make sure that the 'out' of the head of the 'in' list is 0 (end of 'out' list)
         _recycle_in->_recycle_out = block ; // let previous head of list point to block
        block->_recycle_in = _recycle_in ; // and block point to that previous block.
        _recycle_in = block ; // set the current 'in' pointer to this last inserted block
    } else {
        // This is the first block in the list.
        VERIFIC_ASSERT(!_recycle_out) ; // out list pointer should be 0 too.
        // Set both in and out pointer from the pool to me :
        _recycle_out = block ;
        _recycle_in = block ;
        // The 'block's in and out pointers (end of list) both remain at 0.
    }
}

void
MemPool::StopRecycle(MemBlock *block)
{
    if (!block) return ;

    // Remove block from the recycle list (if it is there)
    VERIFIC_ASSERT(block->_is_on_recycle_list) ; // Assert that it's on the recycle list

    // Let 'recycle_out' side pointer to the block skip the block :
    if (block->_recycle_out) {
        // recycle_out side of block is a block
        block->_recycle_out->_recycle_in = block->_recycle_in ;
    } else {
        VERIFIC_ASSERT(_recycle_in==block) ; // if it was on the recycle list, than this should hold
        // recycle_out side of block is the pool
        _recycle_in = block->_recycle_in ;
    }

    // Let 'recycle_in' side pointer to the block skip the block :
    if (block->_recycle_in) {
        // recycle_in side of block is a block
        block->_recycle_in->_recycle_out = block->_recycle_out ;
    } else {
        VERIFIC_ASSERT(_recycle_out==block) ; // if it was on the recycle list, than this should hold
        // recycle_in side of block is the pool
        _recycle_out = block->_recycle_out ;
    }

    // Reset flag and reset the block's recycle-in/out pointers :
    block->_is_on_recycle_list = 0 ;
    block->_recycle_in = 0 ;
    block->_recycle_out = 0 ;
}

/*------------------------------------------------*/

void
MemPool::MemStats()
{
//        if (!_system_space) return ;
    // Print statistics :
    Message::Msg(VERIFIC_INFO,0,0,"********* Verific memory manager statistics ***********") ;
    Message::Msg(VERIFIC_INFO,0,0,"current system used : %d blocks : %d Mbytes", _block_count, _block_count*sizeof(MemBlock)/(1<<20)) ;
    Message::Msg(VERIFIC_INFO,0,0,"peak system used : %d blocks : %d Mbytes", _peak_block_count,  _peak_block_count*sizeof(MemBlock)/(1<<20)) ;

    // Check memory truly used :
    unsigned i ;
    MemBlock *blk ;
    unsigned total_used = 0 ;
    unsigned min_used = MM_NUM_BLOCK_SLOTS ;
    for (i=0;i<MM_NUM_BLOCK_SLOTS;i++) {
        blk = (MemBlock*)_blocks[i] ;
        if (!blk) continue ;
        while (blk) {
            total_used += blk->_used_space ;
            if (blk->_used_space < min_used) min_used = blk->_used_space ;
            // go to next block with this blk number
            blk = blk->_next ;
        }
    }

    // space used was in blocks of 4 bytes :
    total_used *= sizeof(void*) ;
    min_used *= sizeof(void*) ;

    Message::Msg(VERIFIC_INFO,0,0,"currently using %d Mbytes (%d bytes)", total_used/(1<<20),total_used) ;

    // space in a block is MM_BLOCK_SIZE characters.
    unsigned space_in_block = MM_BLOCK_SIZE ;
    unsigned space_in_blocks = space_in_block * _block_count ;

    // garbage is space_in_block*numblocks - space used / space_in_block :
    Message::Msg(VERIFIC_INFO,0,0,"current average garbage : %d percent", (space_in_blocks - total_used)/(space_in_blocks/100)) ;
    // Single most garbage block :
    Message::Msg(VERIFIC_INFO,0,0,"current maximum garbage : %d percent", (space_in_block - min_used)/(space_in_block/100)) ;
    // OS related :
    Message::Msg(VERIFIC_INFO,0,0,"passed through to OS : %d Mbytes", _passed_through*sizeof(void*)/(1<<20)) ;
}

/*------------------------------------------------*/

/* static */ void
MemPool::GlobalMemStats()
{
    GlobalMemPool::GetRef().MemStats() ;
}

/*------------------------------------------------*/

