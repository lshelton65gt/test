/*
 *
 * [ File Version : 1.72 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_LINE_FILE_H_
#define _VERIFIC_LINE_FILE_H_

#include <limits.h> // ULONG_MAX

#include "VerificSystem.h"

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#include "MemPool.h" // for local memory manager

#ifndef VERIFIC_MEMORY_MANAGER
#include <stdlib.h> // for 'size_t'
#endif

#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || define(VERIFIC_LARGE_LINEFILE)

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;

/*
    Verific uses the 'LineFile' manager to preserve line/file origination
    information from HDL source files. This info is annotated on all
    objects in parse trees and netlist databases, as well as used in the
    message handler, so that from any object that Verific creates from a
    RTL design, information is available as to where it came from in the
    original user RTL designs.
*/

/* -------------------------------------------------------------- */

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
/*
    This compile switch will set the 'linefile_type' to a structure
    which contains the following details :

        - starting line number
        - ending line number
        - starting column number
        - ending column number
        - file name

    This info cannot be encoded into just 32 bits, so we do use a structure
    for it. This obviously uses more memory than the default.

    Since many duplicates will be created for such info, we decided to turn
    the linefile_type to a POINTER to the structure, while the structures
    (their memory storage) themselves are managed by the LineFile object.
    API for the structure is described in LineFile.h.
*/

class ColLineFile ;
typedef ColLineFile* linefile_type ;

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
// Settings for bits needed for line/file and (if applicable) column position info :
#define NUM_LINE_BITS 22 // Setting this to 32 will cause compile warnings
#define NUM_COL_BITS  10 // Only applicable if VERIFIC_LINEFILE_INCLUDES_COLUMNS is set.
#define NUM_FILE_BITS 31 // Setting this to 32 will cause compile warnings
#else
#define NUM_LINE_BITS 31 // Setting this to 32 will cause compile warnings
#define NUM_FILE_BITS 31 // Setting this to 32 will cause compile warnings
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS

#else // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)

/*
    Default : Store only file name and a line number in a single unsigned
    (scalar) integer.  We do this by numbering the files read, and storing
    the file names themselves in a static hash table.

    Use NUM_LINE_BITS bits to store a line number for lineno.
    and use the rest (8*sizeof(unsigned) - NUM_LINE_BITS) for file names
    This means we can store 2**20 (== 1 Million) lines for a file
    and maintain correct file names for 2**12 (== 4000) files
    on a standard 32 bit unsigned.
    That should be enough for a while again....
    (we did overflow the previous setting of 10 bits for files previously).
*/

typedef unsigned long linefile_type ;

// Settings for bits needed for line/file and (if applicable) column position info :
// Note that, to fit line and file info in a unsigned long, LINE_NO_BITS + NUM_FILE_BITS <= sizeof(long)
#if (ULONG_MAX & 0xffffffff00000000UL)
    // 64bit longs
    #define NUM_LINE_BITS 32
    #define NUM_FILE_BITS 32
#else
    // 32bit longs
    #define NUM_LINE_BITS 20
    #define NUM_FILE_BITS 12
#endif

#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)

// Determine maximum values for linefile fields
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    #define MAX_COL_VALUE  ((unsigned)~(~0L << NUM_COL_BITS))
#endif
#define MAX_LINE_VALUE ((unsigned)~(~0L << NUM_LINE_BITS))
#define MAX_FILE_VALUE ((unsigned)~(~0L << NUM_FILE_BITS))

/* -------------------------------------------------------------- */

// General (static) linefile_type info manager :
class VFC_DLL_PORT LineFile
{
private :
    // Filename identification : system wide
    LineFile() ; // This class should never get instantiated (make this private)
    ~LineFile() ;

    // Prevent compiler from defining the following
    LineFile(const LineFile &) ;            // Purposely leave unimplemented
    LineFile& operator=(const LineFile &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Encode a filename and linenumber into one 'unsigned' number. Possibly register the filename to an id internally.
    static linefile_type    EncodeLineFile(const char *filename, unsigned lineno) ;

    // Increment the line number for a linefile object.
    static linefile_type    IncrLineNo(linefile_type line_file) ;

    // Force a line number into the 'linefile' object.
    static linefile_type    SetLineNo(linefile_type linefile, unsigned lineno) ;
    // Force a file id into the 'linefile' object.
    static linefile_type    SetFileId(linefile_type linefile, unsigned file_id) ;

    // Get filename or line number back from an encoded linefile number :
    static const char *     GetFileName(const linefile_type line_file) ;
    static const char *     GetAbsFileName(const linefile_type line_file) ;
    static unsigned         GetLineNo(const linefile_type line_file) ;

    // Get filename or absolute filename back from file id :
    static const char *     GetFileNameFromId(unsigned id) ;
    static const char *     GetAbsFileNameFromId(unsigned id) ;

    // Get file identification number (typically for system use only)
    static unsigned         GetFileId(const linefile_type line_file) ;
    static unsigned         GetFileId(const char *filename) ;

    // VIPER #4175: Cleans up all the allocated linefile structure when we preserve column info:
    // WARNING: Please be absolutely sure that there is no reference of linefile
    // anymore in the memory before calling this routine. Otherwise we will corrupt.
    static void             DeleteAllLineFiles() ;

    // VIPER #4665: API to reset all fields needed for file_id vs file name handling.
    static void             ResetFileIdMaps() ;
    // Linefile comparison:
    static unsigned         Compare(const linefile_type left, const linefile_type right) ; // Returns 1 if left and right points to same location, 0 otherwise
    static unsigned         LessThan(const linefile_type left, const linefile_type right, unsigned less_than_equal=0) ; // Returns 1 if left comes before right (or is same if less_than_equal==1) in same file, 0 otherwise
    static unsigned         IsProtectedLinefile(const linefile_type linefile) ; // Viper #8188 / 8208

} ; // class LineFile

/* -------------------------------------------------------------- */

// By default, we store line/file in a scalar unsigned.
// If column info is needed, we need a structure :
#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
// ColLineFile stores a column/line touple (left and right) and file info
// This structure does not fit in a normal (32bit) field, so we will work with
// pointers to this structure :
class VFC_DLL_PORT ColLineFile
{
public :
    // Constructor to implement LineFile::EncodeLineFile() :
    // Constructors insert themselves into a static array.
    ColLineFile(const char *filename, unsigned lineno) ; // create a ColLineFile starting at filename/lineno.
    ~ColLineFile() ;

private :
//CARBON_BEGIN
//     void init();	// used by ctors
//CARBON_END
    // Prevent compiler from defining the following
    ColLineFile() ;                               // Had to define this under VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK for the overloaded 'new' operator
    ColLineFile(const ColLineFile &) ;            // Purposely leave unimplemented
    ColLineFile& operator=(const ColLineFile &) ; // Purposely leave unimplemented

public :
#ifdef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
    // Do not insert memory hooks (for Verific Memory Manager), we will define them separately:
    // Define the two non-array version of the new/delete operator:
    static void *   operator new(size_t size) ;
    static void     operator delete(void *p) ;
#ifdef VERIFIC_MEMORY_MANAGER
    // The following two array versions remain same as in other classes for Verific Memory Manager:
    static void *   operator new[](size_t size)   { return GlobalMemPool::GetRef().New(size) ; }
    static void     operator delete[](void *p)    { GlobalMemPool::GetRef().Delete(p) ; }
#endif
#else
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    // INSERT_MM_HOOKS
    // ColLinefile has it's own memory manager
    static void * operator new(size_t size)     { return ColLineFileMemManager().New(size) ; }
    static void * operator new[](size_t size)   { return ColLineFileMemManager().New(size) ; }
    static void   operator delete(void *p)      { ColLineFileMemManager().Delete(p) ; }
    static void   operator delete[](void *p)    { ColLineFileMemManager().Delete(p) ; }
#endif

    // Make a Copy() routine, because we are working with structures, not scalars.
    // This routine needs to be called with care (since memory is allocated),
    // typically it should be called when a tree node is constructed.
    // We don't use a copy constructor, since that would require that
    // the class name (ColLineFile) needs to be called explicitly in application code.
    // And we prefer to work with just 'linefile_type.
    ColLineFile *   Copy() const ;

    // First the plain, full-detailed access routines :
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    unsigned        GetLeftCol() const                  { return _left_col ; }
    unsigned        GetRightCol() const                 { return _right_col ; }
    unsigned        GetLeftLine() const                 { return _left_line ; }
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS
    unsigned        GetRightLine() const                { return _right_line ; }
    unsigned        GetFileId() const                   { return _file_id ; }
    const char *    GetFileName() const ;    // File name in a const char* form (as it was read in)
    const char *    GetAbsFileName() const ; // File name in a const char* form (absolute path)

    // Set line/file/column info.
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    void            SetLeftCol(unsigned left_col)       { if (!left_col) return ;   _left_col   = (left_col   > MAX_COL_VALUE)  ? MAX_COL_VALUE  : left_col ; }
    void            SetRightCol(unsigned right_col)     { if (!right_col) return ;  _right_col  = (right_col  > MAX_COL_VALUE)  ? MAX_COL_VALUE  : right_col ; }
    void            SetLeftLine(unsigned left_line)     { if (!left_line) return ;  _left_line  = (left_line  > MAX_LINE_VALUE) ? MAX_LINE_VALUE : left_line ; }
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS
    void            SetRightLine(unsigned right_line)   { if (!right_line) return ; _right_line = (right_line > MAX_LINE_VALUE) ? MAX_LINE_VALUE : right_line ; }
    void            SetFileId(unsigned file_id)         { if (!file_id) return ;    _file_id    = (file_id    > MAX_FILE_VALUE) ? 0 /* do not use max-value */ : file_id ; }

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // Other API Routines that operate on a ColLineFile :
    // Merge col/line info from 'other_side' into this.
    // Use the maximum reach of left->right info of both as a result.
    void            MergeMaxSpan(const ColLineFile *other_side) ;
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS

    // Managing the dedicated Linefile/column memory pool :
    static void     MemStats()              { ColLineFileMemManager().MemStats() ; } // Print statistics on line/file/column memory usage
    static void     DeleteAllLineFiles() ; // Release all allocated line/file/column info to the OS.
    static MemPool &ColLineFileMemManager() { if (!_mem_manager) _mem_manager = new MemPool() ; return *_mem_manager ; } // Initialize (if not done already) and return the reference

private : // member fields :
    // Smart setting will get this in 3 unsigned fields on a 32 bit machine :
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    unsigned    _left_col   : NUM_COL_BITS ;
    unsigned    _right_col  : NUM_COL_BITS ;
    unsigned    _left_line  : NUM_LINE_BITS ;
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS
    unsigned    _right_line : NUM_LINE_BITS ;
    unsigned    _file_id    : NUM_FILE_BITS ;

protected : // ColLineFile memory manager :
    static MemPool *_mem_manager ;
    static Array   *_col_line_structs ;
} ; // class ColLineFile

/* -------------------------------------------------------------- */

#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_LINE_FILE_H_

