/*
 *
 * [ File Version : 1.60 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_SYSTEM_H_
#define _VERIFIC_SYSTEM_H_

// This header file is intended as a system header file for all Verific code.
// Basic operations like assertion, new and delete can be defined here.
//
// This system header should (can) be included in every application cpp
// files that needs to use Verific code.

/*-------------------------------------------------------------------------------------
    VERIFIC_NAMESPACE Compile Flag : (active in all projects)
  -------------------------------------------------------------------------------------
    To avoid potential name collisions between Verific classes and customer
    classes, a "Verific" namespace will be created if this flag is set.  By
    default it is ifdef'ed out.  All Verific classes will obtain the Verific::
    prefix if this flag is set, which makes them unique within your application.
    To make all Verific class names directly visible (without a Verific::
    prefix), use header file VerificSystem.h in your application code, or type
    "using namespace Verific". Otherwise, class names are still visible by
    selection with Verific:: prefix.

    The following #ifndef block allows the build user to force on the "verific"
    namespace, regardless of flag settings in the Makefiles/(Msdev project
    settings). Comment out the #define if you want to control this flag from the
    Makefiles/(Msdev project settings).
*/

#define VERIFIC_NAMESPACE // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERIFIC_LARGE_LINEFILE Compile Flag : (active in all projects)
  -------------------------------------------------------------------------------------

    // VIPER #8260:
    Verific uses the 'LineFile' manager to preserve line/file origination
    information info from HDL source files. This info is annotated on all
    objects in parse trees and netlist databases, as well as used in the
    message hangler, so that from any object that Verific creates from a
    RTL design, information is available as to where it came from in the
    original user RTL designs.

    Line/file origination info is of type 'linefile_type', declared in LineFile.h.
    By default, the line/file info stores linenumber and filename info,
    and this is encrypted in a 32 bit (unsigned) number, set on objects
    in the database and parse-trees.

    This compile switch will change the 'linefile_type' to a structure
    which contains more details :
        line number
        file name
    This info cannot be encoded into just 32 bits, so we do use a structure
    for it. This obviously uses more memory than the default.

    Since many duplicates will be created for such info, we decided to turn
    the linefile_type to a POINTER to the structure, while the structures
    (their memory storage) themselves are managed by the LineFile object.
    API for the structure is described in LineFile.h.

    This compile switch will force flex/bison to calculate line info
    for all tokens and rules, so that accurate info is annotated on
    the parse tree nodes (and netlist objects after elaboration).
*/

//#define VERIFIC_LARGE_LINEFILE // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERIFIC_LINEFILE_INCLUDES_COLUMNS Compile Flag : (active in all projects)
  -------------------------------------------------------------------------------------
    Verific uses the 'LineFile' manager to preserve line/file origination
    information info from HDL source files. This info is annotated on all
    objects in parse trees and netlist databases, as well as used in the
    message hangler, so that from any object that Verific creates from a
    RTL design, information is available as to where it came from in the
    original user RTL designs.

    Line/file origination info is of type 'linefile_type', declared in LineFile.h.
    By default, the line/file info stores linenumber and filename info,
    and this is encrypted in a 32 bit (unsigned) number, set on objects
    in the database and parse-trees.

    This compile switch will change the 'linefile_type' to a structure
    which contains more details :
        starting line number
        ending line number
        starting column number
        ending column number
        file name
    This info cannot be encoded into just 32 bits, so we do use a structure
    for it. This obviously uses more memory than the default.

    Since many duplicates will be created for such info, we decided to turn
    the linefile_type to a POINTER to the structure, while the structures
    (their memory storage) themselves are managed by the LineFile object.
    API for the structure is described in LineFile.h.

    This compile switch will force flex/bison to calculate column info
    for all tokens and rules, so that accurate info is annotated on
    the parse tree nodes (and netlist objects after elaboration).
*/

#define VERIFIC_LINEFILE_INCLUDES_COLUMNS // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK Compile Flag : (active in all projects)
  -------------------------------------------------------------------------------------
    This flag depends on VERIFIC_LINEFILE_INCLUDES_COLUMNS and has no effect if that
    flag is not turned on.

    Verific stores all the linefile object created under the compile flag
    VERIFIC_LINEFILE_INCLUDES_COLUMNS in a static Array to avoid leak. By default
    each single linefile object created are inserted into this Array.

    What this compile switch does is that it allocates a chunk of linefile objects
    and insert it into the Array. Susequent linefile objects are allocated from
    that chunk until it gets filled up. Then again another chunk is allocated.
    This avoids fragmentation and thus saves memory for big designs.
*/

#define VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERIFIC_MEMORY_MANAGER Compile Flag : (active in all projects)
  -------------------------------------------------------------------------------------
    Verific has a memory-pool based memory manager called a MemPool.

    You can use a dedicated MemPool to manage memory for a specific C++
    class or tree of derived classes, by overriding the new and delete
    operators for that (base) classes

    If this compile switch (VERIFIC_MEMORY_MANAGER) is set, then you will
    automatically route ALL new and delete operations FROM VERIFIC CODE
    through one (global) Verific memory pool (GlobalMemPool).

    This memory pool class is extremely efficient (fast and minimum
    overhead) for small memory fragments.  Any memory chunk requested that
    are larger than 256 void*'s (1Kbyte on a 32 bit machine) are
    automatically routed through to the OS (both at new and delete time).

    MemPool allocates large blocks (about 1Mbyte) from the OS and distributes
    fragments from within that block to the user. It manages a garbage list
    of fragments per block, and returns the block to the OS once all fragments
    have been freed by the user.  Memory overhead is only 2 bytes per fragment
    (while the OS typically needs 8 bytes per fragment).  Because of this,
    program memory usage typically goes down about 20% for application programs.
    Speed of MemPool for small memory fragments is much faster than OS memory
    manager, and we often see runtime improvements up to 25% using MemPool.
*/

//#define VERIFIC_MEMORY_MANAGER // <--- Comment this line out to control flag from build environment, or uncomment to force on.
/*-------------------------------------------------------------------------------------
    VERIFIC_TEMPLATED_ITERATOR Compile Flag : (active in all projects)
  -------------------------------------------------------------------------------------
    Compiler with optimization level -O2 or above, C99 'strict-aliasing' rules are
    enforced. Under 'strict-aliasing' rule it is illegal to cast a pointer to one
    data type to a pointer of another. We violate that rule in every iterator of
    our container classes (Array, Map, Set). That is because our containers are void*
    based, and not type-templated. Consequently, the iterators do not work correctly,
    and a core-dump results. A work around to this issue is to compile with the
    'no-strict-aliasing' compile option. Alternatively defining the following
    VERIFIC_TEMPLATED_ITERATOR compile flag will use templated routines to avo id
    strict aliasing issue. Turn off the following flag in case of any issue with
    templated member functions. If turned off, use of 'no-strict-aliasing' is
    advised during compilation to avoid potential core dump.

    Update (100120): Because Visual C++ 6.0 doesn't support templating, this flag will
    cause build failures on that compiler. Mut use VC++ 2003 .NET or above.
*/

#define VERIFIC_TEMPLATED_ITERATOR // <--- Comment this line out to control flag from build environment, or uncomment to force on

/*-------------------------------------------------------------------------------------
    64 bit signed and unsigned integer : (active in all projects)
  -------------------------------------------------------------------------------------
    HDL data type like 'time' is a 64 bit unsigned integer. So, to work with this
    type of data structure we need to have a platform/architecture independent 64
    bit wide type defined. We do that here using #define.

    For Windows we use its predefined __int64 and for other platforms we use standard
    'long long' data type. This makes the new type platform independent afterwards.

    Windows does not support long long. So we had to use its predefined type __int64.
    In 64 bit machines also long long is 64 bit. And __int64 is by name/definition is
    always 64 bit in size. So, we don't have any problem for 32bit or 64bit architecture.

    Both signed and unsigned 64 bit integer types are defined here.
*/

#ifdef WINDOWS
#if defined( __MINGW32__ ) || defined( __MINGW64__ )
    #include <_mingw.h> // for msvc specific __int64 definition.
#endif
    // For Windows use predefined __int64:
    typedef  unsigned __int64    verific_uint64 ;
    typedef  __int64             verific_int64 ;
#else
    // For other platforms use equivalent long long:
    typedef  unsigned long long  verific_uint64 ;
    typedef  long long           verific_int64 ;
#endif

// LLONG_MAX and LLONG_MIN should be included from <limits.h>
// But on some OS/compiler they are not defined. Also, customer
// may want to modify the definition of verific_int64.
// So, defining the MAX/MIN macros here.
#define VERIFIC_INT64_MAX ((verific_int64)(~((verific_uint64)0)>>1))
#define VERIFIC_INT64_MIN ((verific_int64)(-VERIFIC_INT64_MAX)-1)

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
// Define that the following system functions are in Verific namespace
namespace Verific {
#endif

// Verific specific global (C) system functions that Verific code calls go here.
// Presently, we have none.

#ifdef VERIFIC_NAMESPACE
}
#endif

// Windows DLL import/export support
#define VFC_DLL_PORT

// Verific specific global macro's :

#ifndef VERIFIC_ASSERT
// All Verific code calls this ASSERT macro. By default, we use regular system 'assert()'.
// Feel free to re-define it. _LINE_ and _FILE_ can be used in the re-definition.
#include <assert.h>
#define VERIFIC_ASSERT(exp) assert(exp)
#endif

#ifdef VERIFIC_MEMORY_MANAGER
    #include "MemPool.h"
    // Define macro for easy insertion of overrided new and delete routines.
    #define INSERT_MM_HOOKS \
    static void * operator new(size_t size)     { return GlobalMemPool::GetRef().New(size) ; }\
    static void * operator new[](size_t size)   { return GlobalMemPool::GetRef().New(size) ; }\
    static void   operator delete(void *p)      { GlobalMemPool::GetRef().Delete(p) ; }\
    static void   operator delete[](void *p)    { GlobalMemPool::GetRef().Delete(p) ; }

    #define VFC_ARRAY_NEW(TYPE,SIZE)  (TYPE*)GlobalMemPool::GetRef().New(sizeof(TYPE)*(SIZE))
    #define VFC_ARRAY_DELETE(P)       GlobalMemPool::GetRef().Delete((void*)(P))
#else
    #define INSERT_MM_HOOKS
    #define VFC_ARRAY_NEW(TYPE,SIZE)  new TYPE [(SIZE)]
    #define VFC_ARRAY_DELETE(P)       delete [] (P)
#endif // #ifdef VERIFIC_MEMORY_MANAGER

/* -------------------------------------------------------------- */

#endif // #ifndef _VERIFIC_SYSTEM_H_
