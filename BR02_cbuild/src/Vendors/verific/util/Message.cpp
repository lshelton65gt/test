/*
 *
 * [ File Version : 1.72 - 2014/01/09 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <stdio.h>   // Because there is no vfprintf equivalent for iostreams,
                     // we are forced to used the old-fashioned FILE object.
//CARBON_BEGIN
#include <cstring>          // strncmp
//CARBON_END
#include "Message.h"

#include "Strings.h"
#include "Map.h"

#define MESSAGE_OLD_STYLE_LINEFILE_CHECK // By default old behaviour is active

#ifndef MESSAGE_OLD_STYLE_LINEFILE_CHECK
#include "Set.h"
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Static Initialization
/*static*/ Map     *Message::_forced_message_types = 0 ; // (char*)message_id->(unsigned)message_type mapping
/*static*/ Map     *Message::_forced_all_message_types = 0 ; // msg_type_t -> msg_type_t Map. Let all messages of the key bahave as a message of the value. Handy to suppress all messages of some class.
/*static*/ Map     *Message::_formatted_messages = 0 ; // const char * (message-id) -> const char * new formatted string. Internationalizations.

// NOTE: The following map is supposed to be a POINTER_HASH, not a STRING_HASH,
// eventhough the key is a char*.  This is because we're storing message ids
// as the keys.
/*static*/ Map     *Message::_outputted_messages = 0 ;  // char*->(void*)int  (message-to-linefile multi-key mapping)

/*static*/ unsigned Message::_error_count = 0 ;
/*static*/ unsigned Message::_max_error_count = 20 ;
/*static*/ unsigned Message::_console_output = 1 ;
/*static*/ unsigned Message::_print_ids = 1 ;

/*static*/ unsigned Message::_ignore_all_messages = 0 ;

/*static*/ void   (*Message::_msg_callback_func)(msg_type_t msg_type, const char *message_id, linefile_type linefile, const char *msg, va_list args) = 0 ;

/*static*/ void   (*Message::_error_func)(const char*) = 0 ;
//CARBON_BEGIN
/*static*/ void   (*Message::_alert_func)(const char*) = 0 ;
//CARBON_END
/*static*/ void   (*Message::_warning_func)(const char*) = 0 ;
/*static*/ void   (*Message::_info_func)(const char*) = 0 ;

// Buffer size for (Old style) registered callback routines :
static const unsigned MAX_MESSAGE_SIZE = 4096 ;
static char message_buffer[MAX_MESSAGE_SIZE] ;

static FILE *logfile = 0 ;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Message::Message()  { }
Message::~Message() { }

/*------------------------------------------------*/

/*static*/ void
Message::SetMaxErrorCount(unsigned count)
{
    _max_error_count = count ;
}

/*------------------------------------------------*/

/*static*/ void
Message::SetConsoleOutput(unsigned console_output)
{
    _console_output = console_output ;
}

/*------------------------------------------------*/

/*static*/ void
Message::PrintIds(unsigned print_ids)
{
    _print_ids = print_ids ;
}

/*------------------------------------------------*/

/*static*/ unsigned
Message::ErrorCount()
{
    return _error_count ;
}

/*static*/ void
Message::ClearErrorCount()
{
    _error_count = 0 ;

    ClearOutputtedMessages() ;
}

/*static*/ void
Message::ClearOutputtedMessages()
{
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // Need to delete value fields since they are all a Copy() of original linefile info
    // VIPER #4175: Do not delete the linefile, otherwise we have a stale
    // pointer in the '_col_line_structs' static array in LineFile.cpp file:
    //MapIter mi ;
    //linefile_type lf ;
    //FOREACH_MAP_ITEM(_outputted_messages, mi, 0, &lf) delete lf ;
#endif

#ifndef MESSAGE_OLD_STYLE_LINEFILE_CHECK
    // Clean up Set of linefiles
    MapIter mi ;
    Set *linefiles ;
    FOREACH_MAP_ITEM(_outputted_messages, mi, 0, &linefiles) delete linefiles ;
#endif

    // Clear the outputted messages table :
    delete _outputted_messages ;
    _outputted_messages = 0 ;
}

/*------------------------------------------------*/

/*static*/ unsigned
Message::OpenLogFile(const char *log_file_name)
{
    if (!log_file_name) return 0 ;
    logfile = ::fopen(log_file_name,"w") ;
    if (!logfile) {
        Warning(0,"cannot open log file ",log_file_name) ;
        return 0 ;
    }
    PrintLine("all messages logged in file ",log_file_name) ;

    return 1 ; // Log file is open
}

/*static*/ void
Message::CloseLogFile()
{
    if (!logfile) return ;
    ::fclose(logfile) ;
    logfile = 0 ;
}

/*------------------------------------------------*/

/*static*/ void
Message::RegisterCallBackMsg(void(*msg_func)(msg_type_t msg_type, const char *message_id, linefile_type linefile, const char *msg, va_list args))
{
    _msg_callback_func = msg_func ;
}

/*static*/ void
Message::RegisterCallBackError(void (*error_func)(const char*))
{
    _error_func = error_func ;
}

//CARBON_BEGIN
/*static*/ void
Message::RegisterCallBackAlert(void (*alert_func)(const char*))
{
    _alert_func = alert_func ;
}
//CARBON_END

/*static*/ void
Message::RegisterCallBackWarning(void (*warning_func)(const char*))
{
    _warning_func = warning_func ;
}

/*static*/ void
Message::RegisterCallBackInfo(void (*info_func)(const char*))
{
    _info_func = info_func ;
}

/*------------------------------------------------*/

// Embedded Verific SW Release Information
/*static*/ time_t Message::ReleaseDate()         { return (time_t)1396307032 ; } // Mon Mar 31 16:03:52 2014
/*static*/ const char * Message::ReleaseString() { return "Mar14_SW_Release" ; }

/*------------------------------------------------*/

/*static*/ void
Message::PrintLine(const char *txt1, const char *txt2, const char *txt3, const char *txt4, const char *txt5)
{
    // Print a comment, without line/file info.
    Msg(VERIFIC_COMMENT, 0, 0, "%s%s%s%s%s", (txt1)?txt1:"", (txt2)?txt2:"", (txt3)?txt3:"", (txt4)?txt4:"", (txt5)?txt5:"") ;
}

/*------------------------------------------------*/

// Old style INFO, no id, with line/file/class
/*static*/ void
Message::Info(linefile_type linefile, const char *txt1, const char *txt2, const char *txt3, const char *txt4, const char *txt5)
{
    // Call basic Msg with var args
    Msg(VERIFIC_INFO, 0, linefile, "%s%s%s%s%s", (txt1)?txt1:"", (txt2)?txt2:"", (txt3)?txt3:"", (txt4)?txt4:"", (txt5)?txt5:"") ;
}

// Old style WARNING, no id, with line/file/class
/*static*/ void
Message::Warning(linefile_type linefile, const char *txt1, const char *txt2, const char *txt3, const char *txt4, const char *txt5)
{
    // Call basic Msg with var args
    Msg(VERIFIC_WARNING, 0, linefile, "%s%s%s%s%s", (txt1)?txt1:"", (txt2)?txt2:"", (txt3)?txt3:"", (txt4)?txt4:"", (txt5)?txt5:"") ;
}

// Old style ERROR, no id, with line/file/class
/*static*/ void
Message::Error(linefile_type linefile, const char *txt1, const char *txt2, const char *txt3, const char *txt4, const char *txt5)
{
    // Call basic Msg with var args
    Msg(VERIFIC_ERROR, 0, linefile, "%s%s%s%s%s", (txt1)?txt1:"", (txt2)?txt2:"", (txt3)?txt3:"", (txt4)?txt4:"", (txt5)?txt5:"") ;
}

/*------------------------------------------------*/

// New style messages :
// message type is enumerated, message id is added, and
// messages are formatted with variable number of arguments.

/*static*/ void
Message::Msg(msg_type_t msg_type, const char *message_id, linefile_type linefile, const char *msg, ...)
{
    va_list args ;
    va_start(args, msg) ;
    MsgVaList(msg_type, message_id, linefile, msg, args) ;
    va_end(args) ;
}

/*------------------------------------------------*/

#ifndef MESSAGE_OLD_STYLE_LINEFILE_CHECK
/* static */ unsigned long
Message::LineFileCompare(const void *key1, const void *key2)
{
    // Cast to linefile_type:
    register linefile_type tmp1 = (linefile_type)key1 ;
    register linefile_type tmp2 = (linefile_type)key2 ;

    if (tmp1 && tmp2) {
        // Comparing two linefiles:
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
        if (tmp1 == tmp2) return 1 ; // Equal by pointer match

        // Do exact comparison: start with file id.
        if (tmp1->GetFileId() != tmp2->GetFileId()) return 0 ;

        // Compare the line and columns:
        if (tmp1->GetRightLine() != tmp2->GetRightLine()) return 0 ;
        if (tmp1->GetLeftLine()  != tmp2->GetLeftLine())  return 0 ;
        if (tmp1->GetLeftCol()   != tmp2->GetLeftCol())   return 0 ;
        if (tmp1->GetRightCol()  != tmp2->GetRightCol())  return 0 ;

        return 1 ;
#else
#ifdef VERIFIC_LARGE_LINEFILE
        if (LineFile::GetFileId(tmp1) != LineFile::GetFileId(tmp2) return 0 ;
        if (LineFile::GetLineNo(tmp1) != LineFile::GetLineNo(tmp2) return 0 ;
        return 1 ;
#else
        return (tmp1==tmp2) ? 1 : 0 ;
#endif
#endif
    } else if (tmp1) {
        // Hashing a single linefile:
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
        // Hash with left-line, right-col and file-id. Right-col being MSB and file-id being LSB.
        // Shift and OR right-col, left-line and file-id in that order:
        register unsigned long hash = tmp1->GetRightCol() ;
        hash <<= 11 ;
        hash |= tmp1->GetLeftLine() ;
        hash <<= 11 ;
        hash |= tmp1->GetFileId() ;
        return hash ;
#else
#ifdef VERIFIC_LARGE_LINEFILE
        register unsigned long hash = tmp1->GetRightLine() ;
        hash <<= 11 ;
        hash |= tmp1->GetFileId() ;
        return hash ;
#else
        return tmp1 ;
#endif
#endif
    } else {
        // 0-key hashing
        return 0 ;
    }
}
#endif // MESSAGE_OLD_STYLE_LINEFILE_CHECK

/*------------------------------------------------*/

/*static*/ void
Message::MsgVaList(msg_type_t msg_type, const char *message_id, linefile_type linefile, const char *msg, va_list args)
{
//CARBON_BEGIN
#ifdef CARBON
  bool carbon_forced_severity = false;
#endif  
//CARBON_END
    if (_ignore_all_messages) msg_type = VERIFIC_IGNORE ;

    // RD: 9/2007: First check if there is a 'general' change of all messages of the original type.
    // AFTER that, check if there are EXPLICITLY messages that received an explicit message-type.
    // DO the checks in that order, so that user can suppress all errors EXCEPT a few explicit ones :
    // For example :
    //    SetAllMessageType(VERIFIC_ERROR, VERIFIC_IGNORE) ;   // ignore all errors...
    //    SetMessageType("VERI-1000", VERIFIC_ERROR) ;  // ...except message VERI-1000
    // this will cause all errors to be ignored, except message VERI-1000 (which will still come through as an error).

    // Check to see if all messages of this type should behave as another type :
    if (_forced_all_message_types && _forced_all_message_types->GetItem((void*)(unsigned long)msg_type)) {
        msg_type = (msg_type_t)((unsigned long)_forced_all_message_types->GetValue((void*)(unsigned long)msg_type)) ;
    }

    // Check to see if this message should be forced to a certain message_type.
    if (message_id && _forced_message_types && _forced_message_types->GetItem(message_id)) {
        msg_type = (msg_type_t)((unsigned long)_forced_message_types->GetValue(message_id)) ;
//CARBON_BEGIN
#ifdef CARBON
        carbon_forced_severity = true;
#endif  
//CARBON_END
    }

    // Check to see if this message should be ignored.
    if (msg_type == VERIFIC_IGNORE) return ;

    // Check if the message ID has an entry in the message internationalization table :
    if (message_id && _formatted_messages) {
        const char *new_formatted_string = (const char*)_formatted_messages->GetValue(message_id) ;
        if (new_formatted_string) {
            // Here, there is a new formatted string for this message ID.
            //
            // Potentially, here we can do a quick scan here to double-check that original and new formatted strings
            // contain the same number and order of formatters (%s,%d etc).
            // This is a bit time-consuming (at run-time) so we rely on the application user to insert correct strings.

            // Replace the original string with this one :
            msg = new_formatted_string ;
        }
    }

    // To prevent redundant messages, gate outputting warnings/errors if they
    // have already been outputted for the given message/linefile combination.
    if (linefile && message_id) { // Only deal with valid linefiles with valid message ids
        switch (msg_type) {
        case VERIFIC_WARNING :
//CARBON_BEGIN
        case VERIFIC_ALERT :
//CARBON_END
        case VERIFIC_ERROR :
        {
            // Create an outputted messages table (char*message_id -> linefile) if not already there :
            if (!_outputted_messages) _outputted_messages = new Map(POINTER_HASH, 383) ;

            // Now check if this message was already printed :
#ifdef MESSAGE_OLD_STYLE_LINEFILE_CHECK
            MapItem *item = _outputted_messages->GetItem(message_id) ;
            linefile_type lf ;
            while (item) {
                // Return early if this error has already been outputted for this linefile
                lf = (linefile_type)item->Value() ;
                if (linefile == lf) return ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
                // If VERIFIC_LINEFILE_INCLUDES_COLUMNS is enabled, need to check that each
                // individual linefile component is equal in order to do an early exit. (VIPER #2090)
                if (lf->GetFileId()    == linefile->GetFileId() &&
                    lf->GetLeftCol()   == linefile->GetLeftCol() &&
                    lf->GetRightCol()  == linefile->GetRightCol() &&
                    lf->GetLeftLine()  == linefile->GetLeftLine() &&
                    lf->GetRightLine() == linefile->GetRightLine())
                {
                    return ;
                }
#else
#ifdef VERIFIC_LARGE_LINEFILE
                if ((LineFile::GetFileId(lf) == LineFile::GetFileId(linefile)) &&
                    (LineFile::GetLineNo(lf) == LineFile::GetLineNo(linefile))) return ;
#endif
#endif
                // Else ... get next item with same key.
                item = _outputted_messages->GetNextSameKeyItem(item) ;
            }
            // Store that this message has been outputted for this _linefile.  Used to prevent redundant messaging.
#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
            (void) _outputted_messages->Insert(message_id, (void*)linefile->Copy(), 0 /*force_overwrite*/, 1 /*force_insert*/) ;
#else
            (void) _outputted_messages->Insert(message_id, (void*)linefile, 0 /*force_overwrite*/, 1 /*force_insert*/) ;
#endif
#else // MESSAGE_OLD_STYLE_LINEFILE_CHECK
            // VIPER #4237: We keep a Set of linefiles against the char *message id. The Set
            // is hashed with special hashing function that exactly compares two linefiles:
            Set *linefiles = (Set *)_outputted_messages->GetValue(message_id) ;
            if (!linefiles) {
                // No message with this id has been outputted till now, this is the first one.
                // Create the Set and insert it into the Map against the message id:
                linefiles = new Set(LineFileCompare) ;
                _outputted_messages->Insert(message_id, linefiles) ;
            }
            // Try to insert the linefile into the Set, if we can't that means it already exists:
#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
            if (!linefiles->Insert((void*)linefile->Copy())) return ;
#else
            if (!linefiles->Insert((void*)linefile)) return ;
#endif
#endif // MESSAGE_OLD_STYLE_LINEFILE_CHECK
            break ;
        }
        default : break ;
        }
    }

//CARBON_BEGIN
    if ((msg_type==VERIFIC_ERROR) || (msg_type==VERIFIC_ALERT)) {
//CARBON_END
        // Error
        _error_count++ ;
        // Check if we exceed maximum error count
        if (_error_count == _max_error_count) {
            Msg(VERIFIC_COMMENT,0,0,"Sorry, too many errors..") ;
            _error_count++ ; // to make sure we shut-up all later messages.
        }
    }

    // Don't print more messages if we exceeded max error count :
    if (_error_count > _max_error_count) return ;

    // Determine what kind of message this is :
    const char *msg_type_string = 0 ;
//CARBON_BEGIN
#ifdef CARBON
    switch (msg_type) {
    case VERIFIC_INFO :          msg_type_string = "Note" ; break ; // Verific INFO mapped to carbon Note
    case VERIFIC_WARNING :       msg_type_string = "Warning" ; break ;
    case VERIFIC_ALERT :         msg_type_string = "Alert" ; break ;
    // Most Verific errors are mapped to carbon Alerts(unless explicitly force to Error).  Alerts could be demoted to warning by the user      
    case VERIFIC_ERROR :         msg_type_string = carbon_forced_severity ? "Error" : "Alert" ; break ; 
    case VERIFIC_COMMENT :       msg_type_string = "Note" ; break ; // Verific COMMENT mapped to carbon Note
    case VERIFIC_PROGRAM_ERROR : msg_type_string = "Fatal" ; break ;
    default : break ;
    }
#else
//CARBON_END
    switch (msg_type) {
    case VERIFIC_INFO :          msg_type_string = "INFO:" ; break ;
    case VERIFIC_WARNING :       msg_type_string = "WARNING:" ; break ;
    case VERIFIC_ERROR :         msg_type_string = "ERROR:" ; break ;
    case VERIFIC_COMMENT :       msg_type_string = "--" ; break ;
    case VERIFIC_PROGRAM_ERROR : msg_type_string = "PROGRAM ERROR:" ; break ;
    default : break ;
    }

#endif
    // Get line/file :
    const char *file_name = LineFile::GetFileName(linefile) ;
    unsigned lineno = LineFile::GetLineNo(linefile) ;

    // See if we need to send the message to the console :
    if (_console_output) {
        // Send errors to stderr, all other messages to stdout :
        FILE *console_stream = (msg_type==VERIFIC_ERROR) ? stderr : stdout ;
        // Print the filename/linenumber :

//CARBON_BEGIN
#ifdef CARBON
        //if (file_name) ::fprintf(console_stream,"%s(%d): ", file_name, lineno) ;  OldVerificVersion
        if (file_name) ::fprintf(console_stream,"%s:%d: ", file_name, lineno) ;
#else
        if (file_name) ::fprintf(console_stream,"%s(%d): ", file_name, lineno) ;
#endif
//CARBON_END

        // Print the msg type string :
        if (msg_type_string) ::fprintf(console_stream,"%s ",msg_type_string) ;
        
//CARBON_BEGIN
#ifdef CARBON
        if (message_id && _print_ids) {
          // pull apart message_id to get VHDL- or VERI- prefix and the number, then convert VERI- to 5 and VHDL- to 6
          char my_prefix = ' ';
          const char *my_message_id = message_id; // default will show whole message ID

          if ( strncmp( message_id, "VERI-", 5 ) == 0 ){
            // replace prefix with single character '5'
            my_message_id = (message_id+5);
            my_prefix = '5';
          } else if ( strncmp( message_id, "VHDL-", 5 ) == 0 ){
            // replace prefix with single character '6'
            my_message_id = (message_id+5);
            my_prefix = '6';
          }

          ::fprintf(console_stream, "%c%s: ", my_prefix, my_message_id) ;
        }
#endif
        
//CARBON_END
// Print the message :
#if defined(va_copy)
        va_list args_tmp ;
        va_copy(args_tmp, args);
        (void) ::vfprintf(console_stream, msg, args_tmp) ;
        va_end(args_tmp) ;
#else
        (void) ::vfprintf(console_stream, msg, args) ;
#endif
#ifndef CARBON
        // Print the Message ID :
        if (message_id && _print_ids) ::fprintf(console_stream, " (%s)", message_id) ;
#endif  // CARBON
        // Print newline :
        ::fprintf(console_stream, "\n") ;
        // Flush the buffer :
        (void) ::fflush(console_stream) ;
    }

    // See if we need to print to the log file :
    if (logfile) {
        // Print the filename/linenumber :
        if (file_name) ::fprintf(logfile,"%s(%d): ", file_name, lineno) ;
        // Print the msg type string :
        if (msg_type_string) ::fprintf(logfile,"%s ",msg_type_string) ;
        // Print the message :
#if defined(va_copy)
        va_list args_tmp ;
        va_copy(args_tmp, args);
        (void) ::vfprintf(logfile, msg, args_tmp) ;
        va_end(args_tmp) ;
#else
        (void) ::vfprintf(logfile, msg, args) ;
#endif
        // Print the Message ID :
        if (message_id && _print_ids) ::fprintf(logfile, " (%s)", message_id) ;
        // Print newline :
        ::fprintf(logfile, "\n") ;
        // Flush the buffer :
        (void) ::fflush(logfile) ;
    }

    // See if we need to call the callback function :
    if (_msg_callback_func) {
#if defined(va_copy)
        va_list args_tmp ;
        va_copy(args_tmp, args);
        _msg_callback_func(msg_type, message_id, linefile, msg, args_tmp) ;
        va_end(args_tmp) ;
#else
        _msg_callback_func(msg_type, message_id, linefile, msg, args) ;
#endif
    }

    // See if we need to call the old style callback functions :
//CARBON_BEGIN
    if (_error_func || _alert_func || _warning_func || _info_func) {
//CARBON_END
        // Old-style callback.
        // Format 'message_buffer'
        unsigned tmp_point = 0 ;
        // Print the filename/linenumber :
        if (file_name) tmp_point += (unsigned)::sprintf(message_buffer+tmp_point, "%s(%d): ", file_name, lineno) ;
        // Print the msg type string :
        if (msg_type_string) tmp_point += (unsigned)::sprintf(message_buffer+tmp_point, "%s ",msg_type_string) ;
        // Print the message itself :
#if defined(va_copy)
        va_list args_tmp ;
        va_copy(args_tmp, args);
        tmp_point += (unsigned)::vsprintf(message_buffer+tmp_point, msg, args_tmp) ;
        va_end(args_tmp) ;
#else
        tmp_point += (unsigned)::vsprintf(message_buffer+tmp_point, msg, args) ;
#endif
        // Print the Message ID :
        if (message_id && _print_ids) ::sprintf(message_buffer+tmp_point, " (%s)", message_id) ;
        // Print the newline :
        tmp_point += (unsigned)::sprintf(message_buffer+tmp_point, "\n") ;

        if (tmp_point > MAX_MESSAGE_SIZE) {
            // FIX ME : assert ?
        }

        // Call the right call-back function :
        if (msg_type==VERIFIC_ERROR && _error_func)     _error_func(message_buffer) ;
//CARBON_BEGIN
        if (msg_type==VERIFIC_ALERT && _alert_func)     _alert_func(message_buffer) ;
//CARBON_END
        if (msg_type==VERIFIC_WARNING && _warning_func) _warning_func(message_buffer) ;
        if (msg_type==VERIFIC_INFO && _info_func)       _info_func(message_buffer) ;
    }
}

/* ========================================================================= */
// Control to assign/deassign message id handlers
/* ========================================================================= */

/*static*/ unsigned
Message::SetMessageType(const char *msg_id, msg_type_t type)
{
    // Clear previous setting (if it exists)
    ClearMessageType(msg_id) ;

    // Create a 'forced_message_type' Map or reasonable size, if not yet done :
    if (!_forced_message_types) _forced_message_types = new Map(STRING_HASH, 383) ;

    // Insert the message. This will aways return 1 (successfully inserted) since we clear message type first.
    return _forced_message_types->Insert(Strings::save(msg_id), (void*)(unsigned long)type) ;
}

/*static*/ void
Message::ClearMessageType(const char *msg_id)
{
    if (!_forced_message_types) return ;
    // clear existing entry (if there).
    MapItem *mi = _forced_message_types->GetItem(msg_id) ;
    if (!mi) return ;
    // The key is an allocated char*, so we need to free that.
    char *key = (char*)mi->Key() ;
    (void) _forced_message_types->Remove(key) ;
    Strings::free(key) ;
}

/*static*/ msg_type_t
Message::GetMessageType(const char *msg_id)
{
    return (_forced_message_types ? (msg_type_t)((unsigned long)_forced_message_types->GetValue(msg_id)) : VERIFIC_NONE) ;
}

/* static */ void
Message::SetAllMessageType(msg_type_t orig, msg_type_t type)
{
    // Change the type of an entire class of message types.
    // Needed for VIPER 2695.
    // For example : SetAllMessageType(VERIFIC_INFO, VERIFIC_IGNORE) will suppress all Info messages.

    // Create a numeric-hash table for _forced_all_message_types if not already there.
    if (!_forced_all_message_types) _forced_all_message_types = new Map(NUM_HASH) ;

    // Force all messages of type 'orig' to behave as type 'type'. VIPER 2695: allows to suppress all messages of some type.
    // Insert in force overwrite mode so that the last called type remains active:
    _forced_all_message_types->Insert((void*)(unsigned long)orig, (void*)(unsigned long)type, 1 /* overwrite */) ;
}

/*static*/ void
Message::ClearAllMessageTypes()
{
    if (_forced_message_types) {
        char *key ;
        MapIter mi ;
        FOREACH_MAP_ITEM(_forced_message_types, mi, &key, 0) {
            Strings::free(key) ;
        }
        delete _forced_message_types ;
        _forced_message_types = 0 ;
    }

    // Also clear all message type re-directions :
    if (_forced_all_message_types) {
        delete _forced_all_message_types ;
        _forced_all_message_types = 0 ;
    }
}

/* ========================================================================= */
// Message internationalization.
/* ========================================================================= */

// Messages with a message-id can be assigned a new formatted string.
// This is useful for message internationalization.
// For example, message ID "VHDL-1088" has by default the message string :
//     "function %s does not take type %s as argument"
// Using API Message::SetNewFormattedString("VHDL-1088",<new message>) we can change the formatted string to Dutch, by setting <new message> to :
//     "de functie %s heeft niet type %s als argument"
//
// There are a few guidelines in using this API correctly :
//   (1) This API does NOT do string copies. It stores both the msg_id and the new_msg as pointers !
//       That means that the application code is responsible for maintaining the string memory that the pointer points to.
//       In most cases these pointers will simply be constant string literals, in which case there is no poblem with maintaining the string.
//   (2) The new message string should have the same number of formatters (%s, %d etc) and in the same ORDER as the original string.
//       The API does NOT check if this is the case !! That is because it does not have access to the original formatted string for a message-id.
//       A run-time check (for formatter-consistency between original and new string) is possible when the message actually gets triggered (in Message::Msg).
//       That run-time check is not enabled by default, since it does considerably slow down message issuing.
//       So application code should be careful to maintain the correct number and order of formatters for each message-id.
//   (3) Likewise, this API cannot check if a new 'msg_id' actually exists (and thus can be called) in code that uses the Message class.
//

/*static*/ void
Message::SetNewFormattedString(const char *msg_id, const char *new_msg)
{
    // Associate a message ID with a new formatted string. Useful for interationalisation of messages.
    if (!_formatted_messages) _formatted_messages = new Map(STRING_HASH,383) ; // give it a decent initial size

    // Insert new entry (store only pointers!!) do NOT make string copies.
    (void) _formatted_messages->Insert(msg_id,new_msg,1) ; // Do a 'force-overwrite' so later ones override earlier ones.
}

/*static*/ void
Message::ClearNewFormattedString(const char *msg_id)
{
    // clear new formatted message-id -> string associations
    if (!_formatted_messages) return ;

    // Shouldn't it clear the map here ?
    (void) _formatted_messages->Remove(msg_id) ;
}

/*static*/ void
Message::ClearAllNewFormattedStrings()
{
    // clear all formatted message_id->string associations
    if (!_formatted_messages) return ;

    // We store only pointers (in key AND in value) so simply delete the table
    delete _formatted_messages ;
    _formatted_messages = 0 ;
}

/*------------------------------------------------*/
