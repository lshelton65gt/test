/*
 *
 * [ File Version : 1.16 - 2014/02/26 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_PROTECT_H_
#define _VERIFIC_PROTECT_H_

#include "LineFile.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Map ;
class verific_stream ; // VIPER #6665

/* -------------------------------------------------------------- */

class VFC_DLL_PORT Protect
{
public:
    /**
     * IMPORTANT: For now we ASSUMED that the encryption/decryption
     * does not change the length of the string.
     */

    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    /**
     * Encrypt an input buffer in_buf and write the encrypted output
     * to output buffer out_buf. out_buf is allocated by the caller.
     * size is length in bytes of in_buf. out_buf should be at least
     * as big as in_buf. Also size MUST be a multiple of _block_size.
     */
    virtual unsigned encrypt(const char *in_buf, char *out_buf, unsigned size) ;

    /**
     * Decrypt an input buffer in_buf and write the un encrypted output
     * to output buffer out_buf. out_buf is allocated by the caller.
     * size is length in bytes of in_buf. out_buf should be at least
     * as big as in_buf. Also size MUST be a multiple of _block_size.
     */
    virtual unsigned decrypt(const char *in_buf, char *out_buf, unsigned size) ;
    //CARBON_BEGIN
    /**
     * Decrypt an input buffer in_buf and write the un encrypted output
     * to output buffer out_buf. out_buf is allocated by the caller.
     * size is length in bytes of in_buf. out_buf should be at least
     * as big as in_buf. Also size MUST be a multiple of _block_size.
     * lf - starting linefile of `protected section (needed for decrypt error/warning report)
     */
    virtual unsigned decrypt(const char *in_buf, char *out_buf, unsigned size, linefile_type lf) ;
    //CARBON_END
    /**
     * VIPER 6665: Same function as above but for block-by-block decryption
     * using streams. User should create a 'verific_stream' object with
     * the given encrypted text and Verific will read the decrypted text
     * block-by-block from that stream in its lexer.
     */
    virtual verific_stream *get_decryption_stream_for_string(const char *in_buf, unsigned size) ;

    /**
     * Decrypt the data_block as set using the `protect directive of
     * the RTL using the other directives stored in _directive_map.
     * This API returns an allocated string, which will be free'd by
     * the caller using Strings::free.
     */
    virtual char *decrypt() ;
    /**
     * VIPER 6665: Same function as above but for block-by-block decryption
     * using streams. User should create a 'verific_stream' object with
     * the encrypted text and other related information stored in _directive_map
     * and Verific will read the decrypted text block-by-block from that stream
     * in its lexer.
     */
    virtual verific_stream *get_decryption_stream() ;

    virtual ~Protect() ;

    explicit Protect(unsigned block_size) : _block_size(block_size), _directive_map(0) { VERIFIC_ASSERT(_block_size) ; /* must be non-zero */ }

    /**
     * base buffer size unit for encrypt/decrypt methods, the size argument
     * passed to the encrypt/decrypt methods must be multiple of this number
     */
    virtual unsigned GetBlockSize() { return _block_size ; }

    /**
     * This key uniquely identifies an algorithm (for encryption/decryption)
     * The derived classes from this base class is responsible to return
     * unique value to identify the algorithm that they implement
     */
    virtual unsigned GetUniqueKey() { return 0 ; }

private :
    // Prevent compiler from defining the following
    Protect() ;                           // Purposely leave unimplemented
    Protect(const Protect &) ;            // Purposely leave unimplemented
    Protect& operator=(const Protect &) ; // Purposely leave unimplemented

private:
    unsigned _block_size ; // base buffer size unit for encrypt/decrypt methods

public:
    // VHDL LRM 1076_2008 section 24.1 provides a lot of directives in the
    // encryption or decryption envelopes. Here is the placeholder to store
    // the data to be used by the virtual decrypt routine in the derived class.
    //
    // _directive_map : the directives and their values
    //                  e.g. encrypted key is under key_block directive
    //                  data under data_block and digital signature
    //                  is under digest_block key in the Map
    //
    // For multi layer directive, e.g.
    //   encoding = (enctype = "base64", line_length = 64, bytes = 128)
    // the map key would be "encoding enctype", "encoding line_length"
    // and "encoding bytes"
    //
    // The directives are stored in order they appear in the RTL.
    // There can be multiple entries with same key, e.g. encoding
    // for key_block and again encoding for data_block. The order
    // entries in the Map would resolve this conflict.
    //
    // No sanity check is done for the absence of mandatory directives
    // or multiple values of singular directives. The derived classes
    // that decrypts the data need to understand such directives and
    // perform the necessary semantic checks.

    // Set the directive in the map
    virtual unsigned    SetDirective(const char *directive, const char *value) ;

    virtual const char *GetDirectiveValue(const char *directive) ;

    // Reset the directive map and the data/key/digest blocks
    void                ResetProtectData() ;

private:
    Map *_directive_map ;
} ; // class Protect

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_PROTECT_H_
