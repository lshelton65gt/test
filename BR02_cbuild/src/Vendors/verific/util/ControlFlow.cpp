/*
 *
 * [ File Version : 1.13 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <iostream>

#include "ControlFlow.h"
#include "Map.h"
#include "Set.h"
#include "Array.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

static unsigned cfg_root_node_identifier = 0 ;

/* -------------------------------------------------------------- */
// Set the members null... They will be added using member APIs
VerificCFGRootNode::VerificCFGRootNode()
    : _children(0), _parents(0), _node_id(cfg_root_node_identifier++), _converted(0)
{
}

VerificCFGRootNode::~VerificCFGRootNode()
{
    // clean up any potential stale pointer to be left out after
    // deleting this node.. This also ensures we do not visit this
    // node again while deleting a graph with cycles / loops
    SetIter si ;
    VerificCFGRootNode *temp ;
    FOREACH_SET_ITEM(_parents, si, &temp) {
        if (temp) (void) temp->ResetNodeInParents(this) ;
    }
    delete _parents ;

    // cleanup the graph recursively
    FOREACH_SET_ITEM(_children, si, &temp) delete temp ;
    delete _children ;
}

// Manipulate the successor and predecessors
VerificCFGRootNode *VerificCFGRootNode::GetChild() const
{
    // returns null for no-child or multiple children
    if (!_children || _children->Size() != 1) return 0 ;
    return (VerificCFGRootNode *)_children->GetAt(0) ;
}

VerificCFGRootNode *VerificCFGRootNode::GetChild(unsigned pos) const
{
    if (!_children || _children->Size() <= pos) return 0 ;
    return (VerificCFGRootNode *)_children->GetAt(pos) ;
}

unsigned VerificCFGRootNode::AddChild(VerificCFGRootNode *child)
{
    if (!child) return 0 ;

    (void) child->AddParent_(this) ;
    return VerificCFGRootNode::AddChild_(child) ;
}

unsigned VerificCFGRootNode::AddChild_(VerificCFGRootNode *child)
{
    if (!child) return 0 ;
    if (!_children) _children = new Set(POINTER_HASH) ;

    return _children->Insert(child) ;
}

VerificCFGRootNode *VerificCFGRootNode::GetParent() const
{
    if (!_parents || _parents->Size() != 1) return 0 ;
    return (VerificCFGRootNode *)_parents->GetAt(0) ;
}

VerificCFGRootNode *VerificCFGRootNode::GetParent(unsigned pos) const
{
    if (!_parents || _parents->Size() <= pos) return 0 ;
    return (VerificCFGRootNode *)_parents->GetAt(pos) ;
}

unsigned VerificCFGRootNode::AddParent(VerificCFGRootNode *parent)
{
    if (!parent) return 0 ;

    (void) parent->AddChild_(this) ;
    return VerificCFGRootNode::AddParent_(parent) ;
}

unsigned VerificCFGRootNode::AddParent_(VerificCFGRootNode *parent)
{
    if (!parent) return 0 ;
    if (!_parents) _parents = new Set(POINTER_HASH) ;

    return _parents->Insert(parent) ;
}

unsigned VerificCFGRootNode::NumChildren() const
{
    return _children ? _children->Size() : 0 ;
}

unsigned VerificCFGRootNode::NumParents() const
{
    return _parents ? _parents->Size() : 0 ;
}

unsigned VerificCFGRootNode::ResetNodeInParents(VerificCFGRootNode const *child_node)
{
    // This gets called before child_node is deleted
    // The child node may have multiple parents
    // Clean-up child_node in the _children list
    // to avoid stale pointer
    if (_children) return _children->Remove(child_node) ;
    return 0 ;
}

// Insert a node between two given nodes
unsigned VerificCFGRootNode::InsertNodeBetween(VerificCFGRootNode *next_node, VerificCFGRootNode *new_node)
{
    if (!next_node || !new_node) return 0 ;

    if (_children) _children->Remove(next_node) ;

    Set *parents = next_node->GetParents() ;
    if (parents) parents->Remove(this) ;

    (void) new_node->AddParent(this) ;
    (void) new_node->AddChild(next_node) ;

    return 0 ;
}

// Insert a node between a given node and all its children
unsigned VerificCFGRootNode::InsertNode(VerificCFGRootNode *new_node)
{
    if (!new_node) return 0 ;

    SetIter si ;
    Set this_children(POINTER_HASH) ;
    VerificCFGRootNode *child ;
    FOREACH_SET_ITEM(_children, si, &child) {
        if (!child) continue ;
        this_children.Insert(child) ;

        Set *this_parents = child->GetParents() ;
        if (this_parents) this_parents->Remove(this) ;
    }

    if (_children) _children->Reset() ;
    (void) new_node->AddParent(this) ;

    FOREACH_SET_ITEM(&this_children, si, &child) {
        if (!child) continue ;
        (void) child->AddParent(new_node) ;
    }

    this_children.Reset() ;
    return 0 ;
}

// Delete a give node
unsigned VerificCFGRootNode::DeleteNode()
{
    SetIter si ;
    VerificCFGRootNode *parent ;
    FOREACH_SET_ITEM(_parents, si, &parent) {
        if (parent) (void) parent->ResetNodeInParents(this) ;
    }

    VerificCFGRootNode *child ;
    FOREACH_SET_ITEM(_children, si, &child) {
        if (!child) continue ;

        Set *this_parents = child->GetParents() ;
        if (this_parents) this_parents->Remove(this) ;
    }

    FOREACH_SET_ITEM(_parents, si, &parent) {
        SetIter sj ;
        VerificCFGRootNode *this_child ;
        FOREACH_SET_ITEM(_children, sj, &this_child) {
            if (this_child) (void) this_child->AddParent(parent) ;
        }
    }

    if (_parents) _parents->Reset() ;
    if (_children) _children->Reset() ;
    delete this ;

    return 0 ;
}

// Replace a node with a given graph. The parent_node is the top node of the
// graph and child_node is the last node of the graph.
unsigned VerificCFGRootNode::ReplaceNode(VerificCFGRootNode *parent_node, VerificCFGRootNode *child_node)
{
    if (!parent_node) return 0 ;

    SetIter si ;
    VerificCFGRootNode *parent ;
    FOREACH_SET_ITEM(_parents, si, &parent) {
        if (parent) (void) parent->ResetNodeInParents(this) ;
    }

    VerificCFGRootNode *child ;
    FOREACH_SET_ITEM(_children, si, &child) {
        if (!child) continue ;

        Set *this_parents = child->GetParents() ;
        if (this_parents) this_parents->Remove(this) ;
    }

    FOREACH_SET_ITEM(_parents, si, &parent) {
        if (!parent) continue ;
        (void) parent_node->AddParent(parent) ;
    }

    FOREACH_SET_ITEM(_children, si, &child) {
        if (!child) continue ;
        (void) child->AddParent(child_node) ;
    }

    if (_parents) _parents->Reset() ;
    if (_children) _children->Reset() ;
    delete this ;

    return 0 ;
}

void VerificCFGRootNode::PrintNode(void (*f)(void *), Set *stop) const
{
    PrintThisNode(f) ;

    if (stop && stop->GetItem(this)) return ;

    Set stop_set(POINTER_HASH) ;
    if (!stop) stop = &stop_set ;

    (void) stop->Insert(this) ;

    SetIter si ;
    VerificCFGRootNode *child ;
    FOREACH_SET_ITEM(_children, si, &child) {
        if (child) child->PrintNode(f, stop) ;
    }

    VerificCFGRootNode *scope_end = VerificCFGRootNode::GetScopeEnd() ;
    if (scope_end && !scope_end->NumParents()) scope_end->PrintNode(f, stop) ;
}

/* -------------------------------------------------------------- */

VerificCFGDeclarationNode::VerificCFGDeclarationNode()
    : VerificCFGRootNode(), _declaration_list(0), _scope_list(0)
{
}

VerificCFGDeclarationNode::~VerificCFGDeclarationNode()
{
    delete _declaration_list ;
    delete _scope_list ;
}

unsigned VerificCFGDeclarationNode::AddDeclaration(const void *decl, const void *scope)
{
    if (!decl) return 1 ;

    if (!_declaration_list) _declaration_list = new Array() ;
    _declaration_list->Insert(decl) ;

    if (!_scope_list) _scope_list = new Array() ;
    _scope_list->Insert(scope) ;
    return 0 ;
}

unsigned VerificCFGDeclarationNode::NumDeclaration() const
{
    return _declaration_list ? _declaration_list->Size() : 0 ;
}

void *VerificCFGDeclarationNode::GetDeclaration(unsigned pos) const
{
    if (!_declaration_list || _declaration_list->Size() <= pos) return 0 ;
    return _declaration_list->At(pos) ;
}

void *VerificCFGDeclarationNode::GetScope(unsigned pos) const
{
    if (!_scope_list || _scope_list->Size() <= pos) return 0 ;
    return _scope_list->At(pos) ;
}

void VerificCFGDeclarationNode::PrintThisNode(void (*print_decl)(void *)) const
{
    std::cout << std::endl ;
    std::cout << VerificCFGRootNode::GetId() << " DECLARATION_NODE: " ;

    unsigned i ;
    void *decl ;
    FOREACH_ARRAY_ITEM(_declaration_list, i, decl) print_decl(decl) ;
}

/* -------------------------------------------------------------- */

VerificCFGStatementNode::VerificCFGStatementNode()
    : VerificCFGRootNode(), _statement_list(0), _scope_end(0)
{
}

VerificCFGStatementNode::~VerificCFGStatementNode()
{
    delete _statement_list ;

    if (_scope_end) (void) _scope_end->SetScopeBegin(0) ;
    if (_scope_end && !_scope_end->NumParents()) delete _scope_end ;
    _scope_end = 0 ;
}

unsigned VerificCFGStatementNode::AddStatement(const void *stmt, const void *scope)
{
    if (!stmt) return 0 ;

    if (!_statement_list) _statement_list = new Map(POINTER_HASH) ;
    _statement_list->Insert(stmt, scope) ;
    return 1 ;
}

unsigned VerificCFGStatementNode::NumStatement() const
{
    return _statement_list ? _statement_list->Size() : 0 ;
}

void *VerificCFGStatementNode::GetStatement(unsigned pos) const
{
    if (!_statement_list || _statement_list->Size() <= pos) return 0 ;
    MapItem *item = _statement_list->GetItemAt(pos) ;
    return item ? item->Key() : 0 ;
}

void VerificCFGStatementNode::PrintThisNode(void (*print_stmt)(void *)) const
{
    std::cout << std::endl ;
    std::cout << VerificCFGRootNode::GetId() << " STATEMENT_NODE: " ;

    MapIter mi ;
    void *stmt ;
    FOREACH_MAP_ITEM(_statement_list, mi, &stmt, 0) print_stmt(stmt) ;
}

unsigned VerificCFGStatementNode::SetScopeEnd(VerificCFGRootNode *se)
{
    if (_scope_end && se && (_scope_end != se)) return 0 ;
    _scope_end = se ;
    return 1 ;
}

/* -------------------------------------------------------------- */

VerificCFGConditionalStatementNode::VerificCFGConditionalStatementNode()
         : VerificCFGRootNode(), _condition_expr(0), _branch_conditions(0), _scope_end(0)
{
}

VerificCFGConditionalStatementNode::~VerificCFGConditionalStatementNode()
{
    unsigned i ;
    Array *conditions ;
    FOREACH_ARRAY_ITEM(_branch_conditions, i, conditions) delete conditions ;
    delete _branch_conditions ;

    if (_scope_end) (void) _scope_end->SetScopeBegin(0) ;
    _scope_end = 0 ;
    _condition_expr = 0 ;
}

unsigned VerificCFGConditionalStatementNode::NumBranch() const
{
    return _branch_conditions ? _branch_conditions->Size() : 0 ;
}

void VerificCFGConditionalStatementNode::SetConditionExpression(void *cond)
{
    VERIFIC_ASSERT(!_condition_expr && "condition cannot be set twice") ;
    _condition_expr = cond ;
}

void VerificCFGConditionalStatementNode::PrintThisNode(void (*print)(void *)) const
{
    if (!_condition_expr || !_scope_end) return ;

    std::cout << std::endl ;
    std::cout << VerificCFGRootNode::GetId() << " CONDITION_NODE: condition_expr: " ;
    print(_condition_expr) ;
    std::cout << std::endl ;

    unsigned i, j ;
    for (i = 0 ; i < VerificCFGRootNode::NumChildren() ; ++i) {
        std::cout << "condition for branch: " << i << " " ;
        if (!_branch_conditions) {
            if (i) {
                std::cout << "false: " ;
            } else {
                std::cout << "true: " ;
            }
        } else {
            if (_branch_conditions->Size() > i) {
                Array *condition_expr_list = (Array *)_branch_conditions->At(i) ;
                void *expr ;
                unsigned is_first = 1 ;
                FOREACH_ARRAY_ITEM(condition_expr_list, j, expr) {
                    if (!is_first) std::cout << ", " ;
                    print(expr) ;
                    is_first = 0 ;
                }
                std::cout << " : " ;
            } else {
                std::cout << "default: " ;
            }
        }
    }
}

void VerificCFGConditionalStatementNode::PrintNode(void (*print)(void *), Set *stop) const
{
    VerificCFGConditionalStatementNode::PrintThisNode(print) ;

    if (stop && stop->GetItem(this)) return ;

    Set stop_set(POINTER_HASH) ;
    if (!stop) stop = &stop_set ;

    (void) stop->Insert(this) ;
    if (_scope_end) (void) stop->Insert(_scope_end) ;

    // printing all these branches stop at _scope_end (if it leads to it)
    SetIter si ;
    VerificCFGRootNode *child ;
    FOREACH_SET_ITEM(_children, si, &child) {
        if (child) child->PrintNode(print, stop) ;
    }

    if (_scope_end && _scope_end->NumChildren()) {
        // now resume printing from _scope_end
        (void) stop->Remove(_scope_end) ;
        VerificCFGRootNode *scope_end_child = _scope_end->GetChild() ;
        VERIFIC_ASSERT(scope_end_child) ;
        scope_end_child->PrintNode(print, stop) ;
    }
}

unsigned VerificCFGConditionalStatementNode::AddBranch(Array *branch_condition_array, VerificCFGRootNode *branch)
{
    if (!branch) return 0 ;

    if (!VerificCFGRootNode::AddChild(branch)) return 0 ;

    if (!branch_condition_array) {
        if (!_branch_conditions) {
            // if this is if statement, it can at most have two branches
            VERIFIC_ASSERT(VerificCFGRootNode::NumChildren() <= 2) ;
        } else {
            // if this is case statement, only the last branch may be default
            VERIFIC_ASSERT(VerificCFGRootNode::NumChildren() == _branch_conditions->Size() + 1) ;
        }
        return 1 ;
    }

    if (!_branch_conditions) _branch_conditions = new Array() ;
    _branch_conditions->Insert(branch_condition_array) ;

    // only the last branch may have no condition (default)
    VERIFIC_ASSERT(VerificCFGRootNode::NumChildren() == _branch_conditions->Size()) ;

    return 1 ;
}

Array *VerificCFGConditionalStatementNode::GetBranchConditions(unsigned pos) const
{
    if (!_branch_conditions || _branch_conditions->Size() <= pos) return 0 ;
    return (Array *)_branch_conditions->At(pos) ;
}

VerificCFGRootNode *VerificCFGConditionalStatementNode::GetBranchStatement(unsigned pos) const
{
    return VerificCFGRootNode::GetChild(pos) ;
}

unsigned VerificCFGConditionalStatementNode::SetScopeEnd(VerificCFGRootNode *se)
{
    if (_scope_end && se && (_scope_end != se)) return 0 ;
    _scope_end = se ;
    return 1 ;
}

/* -------------------------------------------------------------- */

VerificCFGStateNode::VerificCFGStateNode()
    : VerificCFGStatementNode(), _event_expression(0), _state(0), _line_file(0), _event_expr_list(0)
{
}

VerificCFGStateNode::~VerificCFGStateNode()
{
    _event_expression = 0 ;
    _state = 0 ;
    _line_file = 0 ;
    _event_expr_list = 0 ;
}

void VerificCFGStateNode::PrintThisNode(void (*print)(void *)) const
{
    std::cout << std::endl ;
    std::cout << VerificCFGRootNode::GetId() << " STATE_NODE: " ;
    std::cout << _state ;
    print(_event_expression) ;
}

unsigned VerificCFGStateNode::SetEventExpression(void *event)
{
    if (_event_expression && _event_expression != event) return 0 ;
    _event_expression = event ;
    return 1 ;
}

/* -------------------------------------------------------------- */

VerificCFGScopeEndNode::VerificCFGScopeEndNode(VerificCFGRootNode *sb)
    : VerificCFGRootNode(), _scope_begin(sb)
{
    VERIFIC_ASSERT(_scope_begin && "begin_scope may not be null") ;
}

VerificCFGScopeEndNode::~VerificCFGScopeEndNode()
{
    if (_scope_begin) (void) _scope_begin->SetScopeEnd(0) ;
    _scope_begin = 0 ;
}

unsigned VerificCFGScopeEndNode::SetScopeBegin(VerificCFGRootNode *sb)
{
    if (_scope_begin && sb && (_scope_begin != sb)) return 0 ;
    _scope_begin = sb ;
    return 1 ;
}

void VerificCFGScopeEndNode::PrintThisNode(void (*)(void *)) const
{
    std::cout << std::endl ;
    std::cout << VerificCFGRootNode::GetId() << " SCOPE_END_NODE: " ;
}

/* -------------------------------------------------------------- */
