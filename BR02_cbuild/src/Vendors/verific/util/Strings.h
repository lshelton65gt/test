/*
 *
 * [ File Version : 1.59 - 2014/01/09 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_STRINGS_H_
#define _VERIFIC_STRINGS_H_

#include "VerificSystem.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Set ;
class StringSet ;
class Array ;

/*
   Strings is a class with static members only, to manipulate char*.
   Copy, Concatenate, create from integer, compare, etc...
*/

/* -------------------------------------------------------------- */

class VFC_DLL_PORT Strings
{
private :
    Strings() ;  // This class should never get instantiated (make this private)
    ~Strings() ;

    // Prevent compiler from defining the following
    Strings(const Strings &) ;            // Purposely leave unimplemented
    Strings& operator=(const Strings &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    static unsigned long len(const char *s) ;

    // Save and concatenate Strings
    static char *        save(const char *s) ; // save a single string to new memory
    static char *        save(const char *s1, const char *s2, const char *s3=0, const char *s4=0, const char *s5=0, const char *s6=0, const char *s7=0, const char *s8=0) ; // concatenate strings and return in new memory
    static const char *  CreateConstantString(const char *s) ; // This method returns a constant string for the given input. The constant string is cached in a static set. If the string already exists in the set, the same string is returned. Naturally the string should not be modified or free'd as it is shared by many objects

    static char *        allocate(unsigned long size);

    // Free strings
    static void          free(char *s) ;
    static void          CleanupStaticStringSet() ; // Method to free the static string set including its elements, typically when deleting the parse tree

    // Translate integer to saved Strings or opposite
    static char *        itoa(int i) ;
    static int           atoi(const char *s, unsigned *err = 0) ;
    static char *        dtoa(double d) ; // Double-2-ascii
    static double        atof(const char *s, unsigned *err = 0) ;
    static unsigned long atoul(const char *s, unsigned *err = 0) ;

    // Translate Strings inplace to lower or opposite
    static char *        strtolower(char *s) ;
    static char *        strtoupper(char *s) ;
    // Check if only lower case name
    static unsigned      ispurelower(char *s) ;
    // Check if contains upper case name
    static unsigned      containupper(char *s) ;

    static char *        BinToHex(const char *s) ;
    static char *        BinToOct(const char *s) ;
    static verific_int64 BinToDec(const char* bin) ;

    // Compare Strings. Return 1 if equal. 0 if not
    static unsigned      compare(const char *str1, const char *str2) ;
    static unsigned      compare_nocase(const char *str1, const char *str2) ;

private:
    static StringSet    *_static_string_set ; // To cache constant char pointers in the parse tree
} ; // class Strings

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_STRINGS_H_

