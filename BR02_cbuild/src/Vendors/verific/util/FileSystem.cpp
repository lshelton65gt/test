/*
 *
 * [ File Version : 1.11 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <cstdlib>          // _MAX_PATH
#include <sys/stat.h>       // stat
#include <cstring>          // strcpy
#include <cerrno>           // errno

#ifdef WINDOWS
    #define VFC_MAX_PATH _MAX_PATH
    #include <direct.h>     // mkdir
    #include <ctype.h>      // tolower
    #include <windows.h>    // FindFirstFile/FindNextFile
#else
    #define VFC_MAX_PATH PATH_MAX
    #include <dirent.h>     // struct dirent
    #include <unistd.h>     // getcwd
#endif

#ifdef WINDOWS
    #define  getcwd _getcwd
#endif

#include "FileSystem.h"
#include "Strings.h"
#include "Array.h"
#include "Message.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*static*/ char* FileSystem::_cwd = 0 ;

/*------------------------------------------------*/

/*static*/
unsigned
FileSystem::PathExists(const char *path)
{
    if (!path) return 0 ;

    struct stat statBuf ;
    if (stat(path, &statBuf) || !(statBuf.st_mode & S_IFMT)) {
        return 0 ;
    }

    return 1 ;
}

/*------------------------------------------------*/

/*static*/
unsigned
FileSystem::IsFile(const char *path)
{
    if (!path) return 0 ;

    struct stat statBuf ;
    if (stat(path, &statBuf) || !(statBuf.st_mode & S_IFREG)) {
        return 0 ;
    }

    return 1 ;
}

/*------------------------------------------------*/

/*static*/
unsigned
FileSystem::IsDir(const char *path)
{
    if (!path) return 0 ;

    struct stat statBuf ;
    if (stat(path, &statBuf) || !(statBuf.st_mode & S_IFDIR)) {
        return 0 ;
    }

    return 1 ;
}

/*------------------------------------------------*/

/*static*/
unsigned
FileSystem::IsAbsolutePath(const char *path)
{
    if (!path) return 0 ;

    if (path[0] == '/') return 1 ;

#ifdef WINDOWS
    // VIPER #3575: Windows supports both '\\' and '/' as the path separator:
    if ((path[0] == '\\') || strchr(path, ':')) return 1 ;
#endif

    return 0 ;
}

/*------------------------------------------------*/

/*static*/
unsigned
FileSystem::Mkdir(const char *path)
{
    char *dir = Strings::save(path) ; // Save does NULL ptr check
    if (!dir) return 0 ;

    // Now create (via mkdir) the directory path
    char *sep = strchr(dir, '/') ;
    do {
        if (sep) *sep = '\0' ;
        if (Strings::len(dir)) {
#ifdef WINDOWS
            switch(_mkdir(dir))       // Create directory (Windows)
#else
            switch(mkdir(dir, 0775))  // Create directory (Unix)
#endif
            {
            case 0:     // Directory successfully created
                break;

            case -1:    // _mkdir failed
                switch(errno) {
                case EEXIST:    // Directory already exists (this is fine)
                    break;

                // VIPER #4482: Handle 'access denied' error case:
                case EACCES:
#ifdef WINDOWS
                    // Check whether it is the root directory of some existing drive like C:
                    // Windows sets EACCES in case the 'pwd' is the root directory and
                    // we want to 'mkdir' the root directory itself. If it is the root
                    // directory then it always exists and we should skip it:
                    if (!strchr(dir, '/')) break; // It is the root directory, break here
                    // Otherwise, fall through and produce error below for mkdir failure.
                    // Please note that for non-exising drives Windows sets 'ENOENT'.
#else
                    // For non-Windows, it seems to only happen if the directory does not have
                    // enough permission to create directory. We may choose to push out a
                    // different and very specific error message, but leave it for mow as
                    // it is not incorrect and we we cannot save the file anyway in this case:
                    //tmp_print_node.Error("permission denied for directory %s", dir) ;
#endif

                case ENOENT:    // Path is not valid (this is not fine, since it should've been caught earlier)
                default:
                    Message::Error(0, "cannot create '%s' due to mkdir failure", dir) ;
                    Strings::free(dir) ;
                    return 0;
                }
                break;

            default:
                Message::Error(0, "cannot create '%s' due to mkdir failure", dir) ;
                Strings::free(dir) ;
                return 0;
            }
        }

        if (sep) {
            // Reintroduce separator
            *sep = '/' ;
            // Find next directory
            sep = strchr(++sep, '/') ;
        } else {
            Strings::free(dir) ;
            dir = 0 ;
        }
    } while (dir) ;

    Strings::free(dir) ;

    return 1 ;
}

/*------------------------------------------------*/

/*static*/
void
FileSystem::ReadDirectory(const char *path, Array *files)
{
    // Store results (allocated char* file names in 'files' Array).
    // Caller is responsible for freeing Array contents.
    if (!path || !files) return ;

#ifdef WINDOWS
    char dir_name[MAX_PATH] ;
    (void) strncpy(dir_name, path, MAX_PATH-3) ;
    (void) strcat(dir_name, "/*") ; // Add /* to find all the files

    // Find the first file in the directory.
    WIN32_FIND_DATA fd ;
    HANDLE hFind = FindFirstFile(dir_name, &fd) ;
    if (hFind != INVALID_HANDLE_VALUE) {
        do {
            // Store file name in Array 'files'
            char *n = Strings::save(fd.cFileName) ;
            if (n) files->Insert((void*)n) ;
        } while (FindNextFile(hFind, &fd)) ; // Find subsequent files

        FindClose(hFind) ; // Close the find handle
    }
#else
    DIR *d = opendir(path) ;
    if (d) {
        struct dirent *entry = 0 ;
        // Iterate over all entries of directory
        entry = readdir(d) ;
        while (entry) {
            // Store file name in Array 'files'
            char *n = Strings::save(entry->d_name) ;
            if (n) files->Insert((void*)n) ;
            entry = readdir(d) ; // Read the next entry
        }

        (void) closedir(d) ; // Close the directory
    }
#endif // WINDOWS

    return ;
}

/*------------------------------------------------*/

/*static*/
char *
FileSystem::GetCwd()
{
    char buffer[VFC_MAX_PATH] = "" ;
    // getcwd can fail if buffer size was not large enough:
    if (getcwd(buffer, VFC_MAX_PATH) == 0) return 0 ;
    return Strings::save(buffer) ;
}

/*------------------------------------------------*/

/* static */
char *
FileSystem::DirectoryPath(const char *filename)
{
    char *path = Strings::save(filename) ; // Save does NULL ptr check
    if (!path) return 0 ;

    // Return the path where filename resides.  Returns NULL if no path separators exist.

    // Find last directory separator
    char *sep = strrchr(path, '/') ;
#ifdef WINDOWS
    char *win_sep = strrchr(path, '\\') ;
    if (win_sep > sep) sep = win_sep ;
#endif

    // Now trim off filename and path separator
    if (sep) {
        *sep = '\0' ;
    } else {
        // No path separators exist, so return 0.
        Strings::free(path) ;
        return 0 ;
    }

    return path ;
}

/*------------------------------------------------*/

/*static*/
unsigned
FileSystem::CompactAbsolutePath(char *filename)
{
    // Compact the path in-place:
    // Compacting means searching the path for "/../" occurrences,
    // and eliminating them appropriately.
    if (!filename || !IsAbsolutePath(filename)) return 0 ;

#ifdef WINDOWS
    // For consistency across platforms, convert all '\' to '/' on windows.  If the 1st
    // two characters are "\\", skip them, since this is a network path.
    char *q = filename ;
    // VIPER #3575: Windows supports both "\\\\" and "//" as start of a network path.
    // So, convert '\\' to '/' in the starting position also, no need to check this:
    //if (q[0] == '\\' && q[1] == '\\') q += 2 ;
    while (*q) {
        if (*q == '\\') {
            *q = '/' ;
        // VIPER #7139: Do not use tolower, it may create issues with multibyte strings:
        // Instead, adjust caller of this routine to not assume lowered path names.
        //} else {
        //    *q = ::tolower(*q) ; // Force to lowercase all windows paths.
        }
        q++ ;
    }
#endif

    // Now walk absolute path and make any "//" adjustments (replace with single "/")
    // VIPER #5529: Do this before processing /./ and /../, otherwise they may misbehave.
    char *src = 0 ;
    char *dest = filename ; // Start with the absolute path
#ifdef WINDOWS
    // For Windows, start from second character, if the first two characters
    // are itself "//", we don't want to replace that with a single "/" becasue
    // that points to a network address and it must not be modified:
    dest++ ;
#endif
    while ((dest = strstr(dest, "//"))) {
        // Determine source pointer (address)
        src = dest + 1 ; // 1 is the offset to get to the ending '/' character
        // Now adjust path appropriately (have to use memmove, not strcpy, since src/dest strings are the same).
        memmove(dest, src, Strings::len(src) + 1) ;
    }
    // Now walk absolute path and make any "/./" adjustments (eliminate them)
    dest = strstr(filename, "/./") ;
    while (dest) {
        // Determine source pointer (address)
        src = dest + 2 ; // 2 is the offset to get to the ending '/' character
        // Now adjust path appropriately (have to use memmove, not strcpy, since src/dest strings are the same).
        memmove(dest, src, Strings::len(src) + 1) ;
        // Find next occurrence of "/./"
        dest = strstr(dest, "/./") ;
    }
    // Now walk absolute path and make any "/../" adjustments
    dest = strstr(filename, "/../") ;
    while (dest) {
        // Determine source pointer (address)
        src = dest + 3 ; // 3 is the offset to get to the ending '/' character
        // Determine destination pointer (address).  Walk back from offset and
        // find previous '/' separator (which will act as the destination address.
        if (dest != filename) dest -= 1 ; // 1st need to skip immediate previous separator
        while ((dest >= filename) && (*dest != '/')) dest-- ;
        // filename is not a valid filename path.  Return NULL.
        if (dest < filename) return 0 ;
        // Now adjust path appropriately (have to use memmove, not strcpy, since
        // src/dest strings are the same).
        memmove(dest, src, Strings::len(src) + 1) ;
        // Find next occurrence of "../"
        dest = strstr(dest, "/../") ;
    }
    // Now check for possible trailing "/." and remove
    unsigned len = (unsigned)Strings::len(filename) ;
    if ((filename[len-2] == '/') && (filename[len-1] == '.')) {
        filename[len-2] = '\0' ;
    }

    return 1 ;
}

/*------------------------------------------------*/

/*static*/
char *
FileSystem::Convert2AbsolutePath(const char *filename, unsigned compact)
{
    if (!filename) return 0 ;

    // ASSUMPTION : filename is a valid file name path.

    char *abs_path = 0 ;

    // Is the filename already an absolute path?
    if (IsAbsolutePath(filename)) {
        // This is already an absolute path name.
        abs_path = Strings::save(filename) ;
    } else {
        // This is a relative path name.  Append to CWD.
        if (_cwd) {
            // Use already cached cwd here:
            abs_path = Strings::save(_cwd, "/", filename) ;
        } else {
            // Cannot cache cwd on our own, since user may call chdir():
            char buffer[VFC_MAX_PATH] = "" ;
            if (getcwd(buffer, VFC_MAX_PATH) == 0) {
                return 0 ; // getcwd failed because buffer size was not large enough
            }
            abs_path = Strings::save(buffer, "/", filename) ;
        }
    }

    if (compact && !CompactAbsolutePath(abs_path)) {
        Strings::free(abs_path) ;
        return 0 ;
    }

    // Return newly-allocated string
    return abs_path ;
}

/*------------------------------------------------*/

// VIPER #6358: Convert absolute path to relative path from the given directory:
/*static*/
char *
FileSystem::Convert2RelativePath(const char *filename, const char *from_dir)
{
    if (!filename) return 0 ;

    // ASSUMPTIONS:
    // filename is already an absolute path.
    // We have to make it relative from 'from_dir' only.
    // Interpret NULL 'from_dir' as cwd.

    // Is the filename an absolute path?
    if (!IsAbsolutePath(filename)) {
        // This is not an absolute path name, cannot do anything:
        // Return 0 from here because returning the filename itself
        // may be incorrect since it depends on cwd and cwd can be changed!
        return 0 ;
    }

    // Convert the given 'from_dir' to absolute path
    char *abs_from_dir = Convert2AbsolutePath(((from_dir)?from_dir:"."), 1 /* compact */) ;
    // If the path is not set here, we cannot do anything:
    if (!abs_from_dir) return 0 ;

    // Find the first differing point between the two paths:
    unsigned i = 0 ;
    unsigned last_dir_separator = 0 ;
    while (abs_from_dir[i] && filename[i]) {
        // Check if the character at this position matches:
#ifdef WINDOWS
        // VIPER #7139: Convert2AbsolutePath do not lower the path names anymore, use tolower here:
        if (::tolower(abs_from_dir[i]) != ::tolower(filename[i])) break ;
#else
        if (abs_from_dir[i] != filename[i]) break ;
#endif
        if (filename[i] == '/') last_dir_separator = i ;
        i++ ;
    }

    // If we reached the end of the 'from_dir' but there is no "/" at the end,
    // treat it as the position of the last path separator there:
    if (i && (((abs_from_dir[i] == '\0') && (filename[i] == '/')) ||
              ((abs_from_dir[i] == '/') && (filename[i] == '\0')))) {
        // Move up last separator marker
        last_dir_separator = i ;
    }

    if (!last_dir_separator) {
        // If this path cannot be made relative, return absolute path:
        Strings::free(abs_from_dir) ;
        return Strings::save(filename) ;
    }

    // Allocate enough space for the new relative path:
    char *relative_path = Strings::allocate(VFC_MAX_PATH) ;

    // Now, insert "../" in the beginning of the relative path for every
    // non-matching sub-directory in the 'to_dir' path:
    char *rel_path_idx = relative_path ;
    const char *idx = abs_from_dir+(last_dir_separator) ;
    if ((*idx) == '/') idx++ ; // Ignore the first separator in directory path
    while (*idx) {
        if ((*idx) == '/') {
            // Insert "../" for this sub-directory:
            (void) ::strcpy(rel_path_idx, "../") ;
            rel_path_idx += 3 ;
        }
        idx++ ;
    }

    // Check if the last character in 'to_dir' was a path separator.
    // If not, we need to append another "../" in relative path:
    if (abs_from_dir[last_dir_separator] /* there were some item to be processed */ &&
        ((*(idx-1)) != '/') /* last item was not a path separator */) {
        (void) ::strcpy(rel_path_idx, "../") ;
        rel_path_idx += 3 ;
    }

    // Now, append the rest/non-matching filename into this relative path:
    (void) ::strcpy(rel_path_idx, filename+last_dir_separator+1) ;

    // Cleanup the locally allocated paths:
    Strings::free(abs_from_dir) ;

    return relative_path ;
}

/*------------------------------------------------*/

/*static*/
unsigned
FileSystem::CacheCwd()
{
    if (_cwd) return 0 ; // Not cached or no need to cache

    // Set the _cwd:
    char buffer[VFC_MAX_PATH] = "" ;
    // getcwd can fail if buffer size was not large enough:
    if (getcwd(buffer, VFC_MAX_PATH) == 0) return 0 ;
    _cwd = Strings::save(buffer) ;

    return 1 ; // Cached
}

/*------------------------------------------------*/

/*static*/
void
FileSystem::ResetCacheCwd()
{
    Strings::free(_cwd) ;
    _cwd = 0 ;
}

/*------------------------------------------------*/

// Construct a temporary file name that does not exist in the current working directory
/* static */ char *
FileSystem::GetTemporaryFileName()
{
    static unsigned count = 1 ; // static counter to index/suffix
    const char *prefix = "verific_temp" ; // Start with this name
    char *tmp_name = Strings::save(prefix) ;

    // Check whether this file exists in the current directory.
    while (FileSystem::PathExists(tmp_name)) {
        // Create a temporary name suffixing and index after it
        Strings::free(tmp_name) ;
        char *idx = Strings::itoa((int)count++) ;
        tmp_name = Strings::save(prefix, idx) ;
        Strings::free(idx) ;
    }

    // Return the name, this files does not exist
    return tmp_name ;
}

/*------------------------------------------------*/

