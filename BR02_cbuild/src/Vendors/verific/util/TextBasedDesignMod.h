/*
 *
 * [ File Version : 1.43 - 2014/01/09 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_TBDM_H_
#define _VERIFIC_TBDM_H_

#include "VerificSystem.h"
#include "LineFile.h"

#include <iosfwd> // iostream inclusion needed for PrettyPrint
using std::ostream ;

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class FileMod ;
class FileModMngr ;
class InputFile ;

class Map ;
class Array ;

/* -------------------------------------------------------------------------- */

/*
    The TextBasedDesignMod class is used to accomplish Text-Based Design Modification.
    Text-Based Design Modification is where the contents of a file can be modified when
    outputting the file (i.e. while writing out a file, text can be inserted into
    the stream at certain LineFile locations, as well as having certain file sections
    not written to the stream, as specified by beginning and ending LineFile locations).

    The main reasons for introducing this TextBasedDesignMod functionality is due to
    Verific's whitespace and comment limitations during analysis.  More specifically,
    during analysis, a parse-tree is created, and while we do our best to associate
    comments to their appropriate tree nodes, the association is at best crude,
    in addition to the fact that all whitespace is disregarded.  If the parse-tree
    should then pretty-printed, although the logic is identical to the original HDL
    file, the spacing and position of text is vastly different.

    Certain customers wish to instrument/modify the original HDL design at the
    parse-tree level, output the design (pretty-print), and then diff the results
    to hopefully only see the instrumentation/modification changes and nothing else.
    This is not possible via pretty-print, and that's where this TextBasedDesignMod
    class should be used instead.

    The main operations (modifications) supported on a file are the following :

        - InsertAfter  // Insert text after a specified LineFile location
        - InsertBefore // Insert text before a specified LineFile location
        - Remove       // Remove text between two specified LineFile locations
        - Replace      // Replace text between two specified LineFile locations

    Please note, when VERIFIC_LINEFILE_INCLUDES_COLUMNS is not enabled :

        1) Doing an InsertAfter will insert text at the very end of the line
           (after the carriage return).
        2) Doing a Remove/Replace where the starting and ending line information
           are the same will result in the specified line to be removed/replaced.

    When outputting a modified file, you have the choice to :

        1) Overwrite the original file
        2) Write file with a renamed file name (original file name + suffix)
        3) Write file with original filename but to specified output directory

    If a TextBasedDesignMod object is contructed specifying a "secure_directory", then
    any files residing in underneath stated directory can be overwritten.  If no
    "secure_directory" is stated at construction time, then all modified files may
    be overwritten.  This security feature was introduced so that customers can
    prevent original design files from accidentally being overwritten.
*/

class VFC_DLL_PORT TextBasedDesignMod
{
public:
    explicit TextBasedDesignMod(const char *secure_directory = 0) ; // Flag that files under a design directory can be modified. If 0, any file can be modified.
    virtual ~TextBasedDesignMod() ; // Clear/delete design modifications

private :
    // Prevent compiler from defining the following
    TextBasedDesignMod(const TextBasedDesignMod &) ;            // Purposely leave unimplemented
    TextBasedDesignMod& operator=(const TextBasedDesignMod &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // API to modify files (each routine checks if their file (in linefile_type) is in the secure directory.
    unsigned        InsertAfter(const linefile_type pos, const char *text, unsigned ident=1) const ;  // Inserts text after the given location
    unsigned        InsertBefore(const linefile_type pos, const char *text, unsigned ident=1) const ; // Inserts text before the given location

    unsigned        Remove(const linefile_type pos) const ;                                   // Remove section block
    unsigned        Remove(const linefile_type start, const linefile_type end) const ;        // Remove section block

    unsigned        Replace(const linefile_type pos, const char *text, unsigned ident=1) const ; // Replace given section with the text, if text is '0', the section is removed
    unsigned        Replace(const linefile_type start, const linefile_type end, const char *text, unsigned ident=1) const ; // Replace given section with the text, if text is '0', the section is removed

    char *          GetText(const linefile_type pos, unsigned including_mods) const ;         // Retrieve newly allocated string of original file text (with or without any modifications)
    char *          GetText(const linefile_type start, const linefile_type end, unsigned including_mods) const ; // Retrieve newly allocated string of original file text (with or without any modifications)
    char *          GetInsertedText(const linefile_type pos, unsigned before_the_pos) const ; // VIPER #7453: Returns the inserted text (before or after) of the given location. The text is newly allocated, so caller should free it

    unsigned        WriteDesign(const char *suffix=0, const char *output_directory=0) const ; // Writes out all modified files to original file name path appended with suffix (if specified). If output_directory is defined, then output file gets written to this directory under the same directory structure as the file path name.
    unsigned        WriteFile(const char *design_file, const char *out_file=0) const ;        // Writes out a single modified file.

    void            ClearModifications(unsigned file_id) ;          // Remove any modifications which exist for this file.
    void            CloseFileHandles() const ;                      // VIPER #3758: Closes all the file handles kept open for faster access.

    unsigned        AddSourceSearchPath(const char *path) ; // VIPER #5914: Search files in this paths if not found from current working directory, returns 1 on success

    // Message handling support : Message id table, output formatted, varargs message functions
    static const Map *  GetMessageMap()    { (void) GetMessageId("") ; return _msgs ; } // Return the entire map : (char*)formatted_string -> (char*)message_id
    static const char * GetMessageId(const char *msg) ;     // Aid to get message ID from formatted string. Accessed by Error/Warning/Error
    static void         ResetMessageMap() ;
    static void         Comment(linefile_type linefile, const char *format, ...) ;
    static void         Error(linefile_type linefile, const char *format, ...) ;
    static void         Warning(linefile_type linefile, const char *format, ...) ;
    static void         Info(linefile_type linefile, const char *format, ...) ;

private:
    char *          Indent(const linefile_type pos, const char *text) const ; // Retrieve newly allocated string of text, where each line was idented by specified number of spaces.

    unsigned        IsSecure(const linefile_type lf) const ;        // Check if this location is in the secure directory (and thus we can modify)
    unsigned        IsSecure(const char *file_name) const ;         // Check if this file is in the secure directory (and thus we can modify)

    FileModMngr *   GetFileModMngr(const linefile_type lf, unsigned create_if_needed=1) const ;  // Returns file modification manager for file specified by 'lf'

    static unsigned WriteToStream(InputFile &in_file, ostream &os, unsigned start_line, unsigned start_col, unsigned end_line, unsigned end_col) ;

private:
    static Map *_msgs ;             // VIPER #6035: Message id map: const char *message format vs const char *message id
    char    *_secure_dir ;          // Path to the directory in which we can modify files
    Map     *_modifications ;       // Map of file name (unsigned)file_id -> modifications (FileModMngr*) associations
    Map     *_ifstreams ;           // Map of open input file name (unsigned)file_id -> (InputFile*) associations
    Array   *_source_search_paths ; // VIPER #5914: Search paths for files
} ; // class TextBasedDesignMod

/* -------------------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_TBDM_H_
