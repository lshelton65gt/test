/*
 *
 * [ File Version : 1.48 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>

#include "NameSpace.h"

#include "Strings.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// Give the hash-table some decent initial size, so that we will not
// fragment memory too much once we start inserting massive amounts
// of names

NameSpace::NameSpace(unsigned case_sensitive)
    // Create a renaming hash table with some reasonable size.
  : Map((case_sensitive) ? STRING_HASH : STRING_HASH_CASE_INSENSITIVE, 191),
    _case_sensitive(case_sensitive),
    _illegal_chars(0),
    _illegal_start_chars(0),
    _illegal_end_chars(0),
    _illegal_substring(0),
    _substring_replacement(0),
    _reserved_words(0),
    _next_space(0),
    _replacement_char(0),
    _start_replacement_char(0),
    _end_replacement_char(0),
    _renamed_words(new Map((case_sensitive) ? STRING_HASH : STRING_HASH_CASE_INSENSITIVE))
{
    _escape_word[0] = 0 ;
    _escape_word[1] = 0 ;
}

/*------------------------------------------------*/

NameSpace::~NameSpace()
{
    // Delete all the new names. They are in the value field of the Map.
    // The 'key' into the map is a pointer to a not-owned string,
    // so we will not delete that.
    MapIter i ;
    char *new_name ;
    FOREACH_MAP_ITEM(this, i, 0, &new_name) {
        Strings::free(new_name) ;
    }
    delete _reserved_words ;
    // 'Map's destructor will delete the map itself

    // Zero-out the following to prevent compiler warnings
    _illegal_chars         = 0 ;
    _illegal_start_chars   = 0 ;
    _illegal_end_chars     = 0 ;
    _illegal_substring     = 0 ;
    _substring_replacement = 0 ;
    _next_space            = 0 ;

    delete _renamed_words ; // Just delete this Map
}

/*------------------------------------------------*/

NameSpace *
NameSpace::Copy()
{
    NameSpace *result = new NameSpace(_case_sensitive) ;

    result->_illegal_chars = _illegal_chars ;
    result->_replacement_char = _replacement_char ;

    result->_escape_word[0] = _escape_word[0] ;
    result->_escape_word[1] = _escape_word[1] ;

    result->_illegal_start_chars = _illegal_start_chars ;
    result->_start_replacement_char = _start_replacement_char ;
    result->_illegal_end_chars = _illegal_end_chars ;
    result->_end_replacement_char = _end_replacement_char ;
    result->_illegal_substring = _illegal_substring ;
    result->_substring_replacement = _substring_replacement ;

    result->_reserved_words = (_reserved_words) ? new Map(*_reserved_words) : 0 ;
    return result ;
}

/*------------------------------------------------*/

void
NameSpace::LinkSpace(NameSpace *other)
{
    if (!other || other==this) return ;

    // Cannot link name spaces after names are inserted in both
    VERIFIC_ASSERT(Size()==0 || other->Size()==0) ;

    LinkSpaceWithoutSizeCheck(other) ;
}

/*------------------------------------------------*/

void
NameSpace::LinkSpaceWithoutSizeCheck(NameSpace *other)
{
    if (!other || other==this) return ;

    // Link these two name spaces
    NameSpace *tmp ;
    if (_next_space && other->_next_space) {
        // Only if we BOTH have a space loop, we
        // need to check if other is in my loop (or I am in 'other's loop).
        // If one of us has no space loop, its definitely not yet part
        // of ANY space loop, so we can safely insert.
        tmp = _next_space ;
        while (tmp != this) {
            if (tmp == other) return ; // We are already connected
            tmp = tmp->_next_space ;
        }
    }

    // Now, hook both loops into one name space loop
    tmp = _next_space ;
    _next_space = (other->_next_space) ? other->_next_space : other ;
    other->_next_space = (tmp) ? tmp : this ;
}

/*------------------------------------------------*/

void
NameSpace::ResetLinkedSpaces()
{
    // VIPER #4160: Just reset the linked name-spaces with this name-space:
    _next_space = 0 ;
}

/*------------------------------------------------*/

void
NameSpace::SetIllegalChars(const char *illegal, const char replacement)
{
    _illegal_chars = illegal ;
    _replacement_char = replacement ;
}

void
NameSpace::SetIllegalStartChars(const char *illegal, const char replacement)
{
    _illegal_start_chars = illegal ;
    _start_replacement_char = replacement ;
}

void
NameSpace::SetIllegalEndChars(const char *illegal, const char replacement)
{
    _illegal_end_chars = illegal ;
    _end_replacement_char = replacement ;
}

void
NameSpace::SetIllegalSubstring(const char *illegal, const char *replacement)
{
    _illegal_substring = illegal ;
    _substring_replacement = replacement ;
}

/*------------------------------------------------*/

void
NameSpace::AddReservedWord(const char *word, const char *replacement)
{
    if (!_reserved_words) _reserved_words = new Map((_case_sensitive) ? STRING_HASH : STRING_HASH_CASE_INSENSITIVE) ;
    (void) _reserved_words->Insert(word, replacement) ;
}

/*------------------------------------------------*/

void
NameSpace::EscapeWord(const char prefix, const char suffix)
{
    _escape_word[0] = prefix ;
    _escape_word[1] = suffix ;
}

/*------------------------------------------------*/

char *
NameSpace::ReName(const char *orig)
{
    if (!orig) return 0 ;

    // Return 0 if no renaming was required. If the word is escaped, it's legal
    if (_escape_word[0] && (orig[0] == _escape_word[0] && orig[Strings::len(orig)-1] == _escape_word[1])) {
        return 0 ;
    }

    // Return a newly allocated string (renamed) if renaming was required.
    char *result = 0 ;
    char *tmp ;
    if (!orig[0]) {
        // Empty string : replace by 'illegal_characters' char replacement
        result = Strings::save(".\0") ;
        if (_replacement_char) result[0] = _replacement_char ;

        // Run this through the renaming process again
        tmp = ReName(result) ;
        if (tmp) { Strings::free(result) ; result = tmp ; }
        return result ;
    }

    if (_illegal_chars) {
        // Check if any illegal character is in this word
        if (::strpbrk(orig, _illegal_chars)) {
            if (!_replacement_char) {
                // No substring replacement : escape the word
                result = Escape(orig) ;
                VERIFIC_ASSERT(result) ; // or else there is something wrong with the renaming rules
                return result ;
            }
            result = Strings::save(orig) ;
        }

        while (result) {
            // Check for illegal characters in the result string, and replace
            tmp = ::strpbrk(result, _illegal_chars) ;
            if (!tmp) break ;
            *tmp = _replacement_char ;
        }
        if (result) {
            // Run this through the renaming process again
            tmp = ReName(result) ;
            if (tmp) { Strings::free(result) ; result = tmp ; }
            return result ;
        }
    }

    // Next rules : 'result' could be there already..
    VERIFIC_ASSERT(!result) ;
    if (_illegal_start_chars) {
        // Check if the starting character is in the illegal start character list
        if (::strchr(_illegal_start_chars,orig[0])) {
            if (_start_replacement_char) {
                // Replace first character by replacement
                result = Strings::save(orig) ;
                result[0] = _start_replacement_char ;
            } else if (_escape_word[0]) {
                // Else, use escaped word
                result = Escape(orig) ;
                VERIFIC_ASSERT(result) ;
            } else {
                // Else, resolve by getting a unique name with the collision name generator
                result = Collision(orig) ;
            }
            // Run this through the renaming process again
            tmp = ReName(result) ;
            if (tmp) { Strings::free(result) ; result = tmp ; }
            return result ;
        }
    }

    VERIFIC_ASSERT(!result) ;
    if (_illegal_end_chars) {
        // Check if the end character is in the illegal character list :
        if (::strchr(_illegal_end_chars,orig[Strings::len(orig)-1])) {
            result = Strings::save(orig) ;
            result[Strings::len(result)-1] = _end_replacement_char ;

            // Run this through the renaming process again
            tmp = ReName(result) ;
            if (tmp) { Strings::free(result) ; result = tmp ; }
            return result ;
        }
    }

    VERIFIC_ASSERT(!result) ;
    if (_illegal_substring) {
        // Check if this substring is in the word
        if (::strstr(orig, _illegal_substring)) {
            if (!_substring_replacement) {
                // No substring replacement : escape the word
                result = Escape(orig) ;
                VERIFIC_ASSERT(result) ; // or else there is something wrong with the renaming rules
                return result ;
            }
            result = Strings::save(orig) ;
        }

        while (result) { // work on result
            // Find the substring in 'result'
            tmp = ::strstr(result, _illegal_substring) ;
            if (!tmp) break ; // Done

            // Create the new result
            *tmp = '\0' ; // 'result' now contains the leading string up till the illegal substring.
            char *last = tmp + Strings::len(_illegal_substring) ; // 'last' now contains the substring after the illegal substring

            // Create the new result
            tmp = Strings::save(result, _substring_replacement, last) ;
            Strings::free(result) ;
            result = tmp ;
        }

        if (result) {
            // Run this through the renaming process again
            tmp = ReName(result) ;
            if (tmp) { Strings::free(result) ; result = tmp ; }
            return result ;
        }
    }

    VERIFIC_ASSERT(!result) ;
    if (_reserved_words) {
        MapItem *x = _reserved_words->GetItem(orig) ;
        if (x) {
            if (x->Value()) {
                // Use replacement word if there
                result = Strings::save((char*)x->Value()) ;
            } else if (_escape_word[0]) {
                // Else, use escaped word on original
                result = Escape(orig) ;
            } else {
                // Else, resolve by getting a unique name with the collision name generator
                result = Collision(orig) ;
            }
            // No need to run through renaming again : replacement word should be valid any way.
            return result ;
        }
    }

    return 0 ; // No renaming happened
}

/*------------------------------------------------*/

char *
NameSpace::Escape(const char *orig) const
{
    // Escape this word, and return a newly allocated one
    if (!orig || !_escape_word[0]) return 0 ;

    char *result = Strings::save(" ",orig," ") ;
    result[0] = _escape_word[0] ;
    result[Strings::len(result)-1] = _escape_word[1] ;

    // Actually before we leave, one more thing :
    // if the original name contains the closing escape character, then we need to do something
    if (_escape_word[1] && strchr(orig,_escape_word[1])) {
        // So, we need to do something with that closing escape character.
        // Lets take it out.
        // That is safe, since we will do collision check later any way.
        char *tmp = result+1 ; // skip the opening escape character
        char *runner = tmp ;
        while (tmp && *tmp!='\0') {
            if (*tmp==_escape_word[1] && *(tmp+1)!='\0') {
                // this is a closing escape character which is not the one we just added at the end.
                // Take it out (skip it)
                tmp++ ;
                continue ;
            }
            // otherwise, copy the character
            *runner++ = *tmp++ ;
        }
        *runner = '\0' ; // closing character
    }
    return result ;
}

/*------------------------------------------------*/

char *
NameSpace::Collision(const char *orig) const
{
    // If orig is escaped, first strip escape characters before renaming
    if (orig[0]==_escape_word[0] && orig[Strings::len(orig)-1]==_escape_word[1]) {
        // This is an escaped name.
        char *tmp = Strings::save(orig+1) ; // Skip first character
        // Remove trailing escape character
        tmp[Strings::len(tmp)-1] = '\0' ;
        // For output reasons, we need to turn escape characters into strings
        char b[2] ; b[0] = _escape_word[0] ; b[1] = '\0' ;
        char e[2] ; e[0] = _escape_word[1] ; e[1] = '\0' ;
        // Now return renamed name
        char *tmp2 = Strings::save(b, tmp, "_c", e) ;
        Strings::free(tmp) ;
        return tmp2 ;
    }
    return Strings::save(orig,"_c") ;
}

/*------------------------------------------------*/

void
NameSpace::InsertName(const char *orig)
{
    if (!orig) return ;

    // 'orig' is used as 'key' in the table. Never freed

    // VIPER #2381: Check whether we have renamed some word with the name 'orig':
    // This situation come with this sequence of words:
    // Call               : 'this' Map     : _renamed_words
    // InsertName("a[0]") : a[0] -> a_0_   : a_0_ -> a[0]
    // InsertName("a_0_") : We are in this situation here with 'orig' : 'a_0_'
    MapItem *prev_renamed_item = _renamed_words->GetItem(orig) ;
    if (prev_renamed_item) {
        // We have a renamed word with this name. So, remove the old entries from
        // both the maps and insert this 'orig' name first. Since this is a valid
        // user defined name, we want to insert it normally.
        const char *prev_orig = (const char *)prev_renamed_item->Value() ;
        char *prev_renamed = (char *)prev_renamed_item->Key() ;
        (void) Remove(prev_orig) ; // Remove 'a[0] -> a_0_' from base Map
        (void) _renamed_words->Remove(prev_renamed) ; // Remove 'a_0_ -> a[0]' also
        Strings::free(prev_renamed) ; // Free allocated 'a_0_'
        // Now insert 'orig' first so that it gets in without renamed:
        InsertName(orig) ;
        // Then only, insert 'prev_orig' it might be renamed with some other name:
        InsertName(prev_orig) ;
        return ; // Done, we have inserted both
    }

    // Check collision (if it collides, it's better to get original escaped name
    // rather than renamed escaped name).
    char *result = ResolveCollision(orig) ;

    // Run the renaming rules
    char *tmp = ReName((result)?result:orig) ;
    if (tmp) {
        Strings::free(result) ;
        result = tmp ;
    }

    if (!result) {
        // Original name can go in as-is, with 0 value field.
        // It's already checked for collisions, and there are none.
        (void) Insert(orig,0) ;
    } else {
        // Check if new name is already in use
        tmp = ResolveCollision(result) ;
        if (tmp) {
            Strings::free(result) ;
            result = tmp ;
        }

        // Now insert it, orig->renamed. We don't want to force-insert here, since the
        // last one will be taken
        // VIPER #2507 & 2526: Force repeat insert for case-insensitive namespace:
        unsigned repeat_insert = 0 ;
        if (!_case_sensitive) {
            // By default, case insensitive NameSpace always insert:
            repeat_insert = 1 ;
            // But don't insert the exactly same case element again:
            MapItem *item ;
            FOREACH_SAME_KEY_MAPITEM(this, orig, item) {
                // Exactly match the name with the key of this item:
                if (Strings::compare(orig, (char *)item->Key())) {
                    // Found the key:
                    repeat_insert = 0 ; // No, need to force insert, it's already there
                    break ;
                }
            }
        }
        if (!Insert(orig, result, 0, repeat_insert) && !repeat_insert) {
            // For case-insensitive namespace we might have interted the item.
            // So, we free the string under the condition of '!repeat_insert':
            Strings::free(result) ;
        } else {
            // Also insert the (renaming) result so we can check for collision later
            (void) _renamed_words->Insert(result, orig) ;
        }
    }
}

/*------------------------------------------------*/

char *
NameSpace::ResolveCollision(const char *name)
{
    // Return a collision-free allocated string if 'name' causes a collision

    // Check in all connected namespaces to make sure this
    // name is free. If it is not, generate a madeup name
    // and make sure that one IS collision free in all linked
    // name spaces.
    // If it IS collision free from the start, return 0.
    char *result = 0 ;
    unsigned done = 0 ;
    while (!done) {
        // We are done once we completed a full check
        // through all linked name spaces, with one name.
        done = 1 ;
        NameSpace *runner = this ;
        do {
            while (runner->GetItem((result)?result:name)) {
                done = 0 ;
                // Create a unique name
                char *tmp = Collision((result)?result:name) ;
                if (tmp) {
                    Strings::free(result) ;
                    result = tmp ;
                }
            }
            runner = runner->_next_space ;
        } while (runner && runner != this) ;
    }
    return result ;
}

/*------------------------------------------------*/

const char *
NameSpace::GetName(const char *orig) const

{
    const char *result = 0 ;
    if (_case_sensitive) {
        result = (const char *)GetValue(orig) ;
    } else {
        // VIPER #2507 & 2526: We have repeat inserted names for case-insensitive namespace:
        MapItem *item ;
        // Here match all the same key element for exact name in case-sensitive way:
        FOREACH_SAME_KEY_MAPITEM(this, orig, item) {
            // Exactly match the name with the key of this item:
            if (Strings::compare(orig, (char *)item->Key())) {
                // Found the value:
                result = (char *)item->Value() ;
                break ;
            }
        }
    }
    if (!result) return orig ;
    return result ;
}

/*------------------------------------------------*/

MapItem *
NameSpace::GetItem(const void *key) const
{
    // First check the base store:
    MapItem *item = Map::GetItem(key) ;

    // If can't find in base store check the renamed words:
    if (!item) item = _renamed_words->GetItem(key) ;

    return item ;
}

/*------------------------------------------------*/
