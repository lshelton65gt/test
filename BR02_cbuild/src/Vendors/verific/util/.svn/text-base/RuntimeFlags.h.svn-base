/*
 *
 * [ File Version : 1.12 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_RUNTIME_FLAGS_H_
#define _VERIFIC_RUNTIME_FLAGS_H_

#include "VerificSystem.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Map ;

/*
    Central Runtime flags mechanism implemented with utility "RuntimeFlags", under VIPER 7208.

    Verific software is highly customizable with a variety of compile
    and run-time switches that determine how the software treats incoming designs.

    The class 'RuntimeFlags' in Verific software was created to provide a central template to
    manage (initialize, change and get) globally available run-time switches throughout
    component-oriented software.

    RuntimeFlags is a 'static' utility, which means the APIs can be called from anywhere in the
    system, as long this header file is included.

    A run-time switch is uniquely identified by a string (name of the variable),
    which is used to either Get or Set the global variable.

    To create (declare) a run-time variable use :

           RuntimeFlags::AddVar(<name>,<init-value>) ;

    This API routine is typically called once for each variable that will be used in the system,
    and called from a routine that is called during initialization of the program,
    such as in the RuntimeFlags::Initialize() routine.

    Within the Verific system, separate software components (such as VHDL, Verilog, database etc) typically
    have their own variable initialization routine, which also initializes variables in this RuntimeFlags class.

    If during run-time, you want to re-set all variables to their 'initial' state, simply
    call RuntimeFlags::Initialize() (or the appropriate component-specific Initialize() routines) directly.

    Typical use of this RuntimeFlags utility are the two routines to set/get the value of a previously declared variable :

           RuntimeFlags::GetVar(<name>)
           RuntimeFlags::SetVar(<name>,<value>) ;

    If you call GetVar/SetVar with a variable name that is not previously declared (with AddVar()),
    the routines will issue a PROGRAM_ERROR to the message handler, and return 0.
    This is intended to prevent that accidental programming mistake (mis-spelling of run-time flag name)
    gets caught at least at run-time.

    If your application is not sure if the variable exists or not (for example, if you let your
    user try to set a run-time flag) then we recommend that protect against mis-spellings using
    the HasVar() API :

           char *var_name = .. // user provided
           unsigned long value = ... // user provided
           if (RuntimeFlags::HasVar(var_name)) {
               RuntimeFlags::SetVar(var_name,value) ;
           } else {
               // issue a message to your user, for example via the Verific Message handler :
               Message::Msg(VERIFIC_ERROR,0,"run-time variable '%s' is unknown",var_name) ;
           }

    As a Verific source-code customer, you are free to change the initial values of the
    existing variables, and are free to add run-time variables yourself (on which you then
    can rely in the source code that you want to customize).
    Either way, if you make changes to source code, please share these with Verific for
    merge into your companies' code branch, so that we can make sure that we are running
    the software in the same way as you do. This (share you source code modifications with us)
    would make it much easier for you to integrate new monthly releases from Verific into your system.

    Some of our existing customers have shared their own run-time flag mechanism with us,
    before Verific implemented the 'RuntimeFlags' utility. For these customers, we left your
    existing run-time flags in place (and removed Verific 'RuntimeFlags' calls).
    We would be happy to assist you merging your custom run-time flag mechanism
    with Verific's 'RuntimeFlags' utility, so that you obtain a consistent and extendable
    unified run-time variable management system.

    Thus, any runtime flag from anywhere in the Verific system can be controlled and observed
    from this central place, using the static API routines on this gloabl static RuntimeFlags class.
*/

// Class to manage runtime switches : system-wide utility
class VFC_DLL_PORT RuntimeFlags
{
private :
    // All 'RuntimeFlags' API routines are 'static', constructor need not be called.

    // Prevent compiler from defining the following
    RuntimeFlags() ;                                // Purposely leave unimplemented
    ~RuntimeFlags() ;                               // Purposely not virtual, and leave unimplemented
    RuntimeFlags(const RuntimeFlags &) ;            // Purposely leave unimplemented
    RuntimeFlags& operator=(const RuntimeFlags &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Manage 'unsigned long' variables (non-negative integer values (and boolean flags)) :
    static unsigned      HasVar(const char *var_name) ; // Test : return 1 if global variable exists, 0 if not.
    static unsigned      AddVar(const char *var_name, unsigned long val) ; // Create variable if it does not exist. Set variable to value 'val'. Return 0 if variable was created. 1 if it existed.
    static unsigned      SetVar(const char *var_name, unsigned long val) ; // Set variable 'var_name' to value 'val'. If variable does not exist, issue a PROGRAM_ERROR message and return 0
    static unsigned long GetVar(const char *var_name) ; // Get value of variable 'var_name'. If variable does not exist, issue a PROGRAM_ERROR message and return 0 ;

    // Get the entire Map of existing variables and their setting.
    static const Map    *GetVarMap() ; // Map of existing variables and their value :  const char *'var_name' -> unsigned long 'value'

    // Initialization of all known global variables.  This routine is called automatically, the first time
    // any of the (RuntimeFlags::) API routines is called.  If during run-time, you want to re-set all
    // variables to their 'initial' state, simply call RuntimeFlags::Initialize() directly.
    static void          Initialize() ;

    // Delete flags map (free memory)
    static void          DeleteAllFlags() ;

protected :
    static Map *_unsigned_vars ; // store const char* -> unsigned long variables.
} ; // class RuntimeFlags

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_RUNTIME_FLAGS_H_

