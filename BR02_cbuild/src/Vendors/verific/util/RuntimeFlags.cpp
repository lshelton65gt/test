/*
 *
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "RuntimeFlags.h"
#include "../verilog/VeriRuntimeFlags.h"
#include "../vhdl/VhdlRuntimeFlags.h"

#include "Map.h"
#include "Message.h"
#include "Strings.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Initialize global var tables :
/* static */ Map *RuntimeFlags::_unsigned_vars = 0 ;

//////////////////////////////////////////////////////////////////////
// API to access and maintain global variables (of type "unsigned long") :
//////////////////////////////////////////////////////////////////////

/* static */
unsigned
RuntimeFlags::HasVar(const char *var_name)
{
    if (!_unsigned_vars) Initialize() ;

    // Test : return 1 if global variable exists, 0 if not.
    return (_unsigned_vars->GetItem(var_name)) ? 1 : 0 ;
}

/* static */
unsigned
RuntimeFlags::AddVar(const char *var_name, unsigned long val)
{
    if (!_unsigned_vars) Initialize() ;

    // Create variable if it does not exist, or overwrite the variable if it does :
    return _unsigned_vars->Insert(var_name,(void*)val, 1 /*force-overwrite*/) ;
}

/* static */
unsigned
RuntimeFlags::SetVar(const char *var_name, unsigned long val)
{
    if (!_unsigned_vars) Initialize() ;

    // Set (or create) existing variable to value 'val'.
    // Return 1 if successfully inserted, 0 if not.

    // Pick up the MapItem under this key :
    MapItem *mi = _unsigned_vars->GetItem(var_name) ;

    // Safety check :
    if (!mi) {
        Message::Msg(VERIFIC_PROGRAM_ERROR,"SYS-1001",0,"runtime variable '%s' is not declared",var_name) ;
        return 0 ;
    }

    // Simply change the value of this item :
    (void) _unsigned_vars->Insert(mi->Key(),(void*)val, 1 /*force-overwrite*/) ;
    return 1 ;
}

/* static */
unsigned long
RuntimeFlags::GetVar(const char *var_name)
{
    if (!_unsigned_vars) Initialize() ;

    // Pick up the MapItem under this key :
    MapItem *mi = _unsigned_vars->GetItem(var_name) ;

    // Safety check :
    if (!mi) {
        Message::Msg(VERIFIC_PROGRAM_ERROR,"SYS-1001",0,"runtime variable '%s' is not declared",var_name) ;
        return 0 ;
    }

    // Get value of existing variable. Will assert if variable does not exist.
    return (unsigned long)mi->Value() ;
}

/* static */
const Map *
RuntimeFlags::GetVarMap()
{
    if (!_unsigned_vars) Initialize() ;

    return _unsigned_vars ;
}

/* static */
void
RuntimeFlags::Initialize()
{
    // VIPER #7234: Use STRING_HASH instead of POINTER_HASH, as keys are char*
    if (!_unsigned_vars) _unsigned_vars = new Map(STRING_HASH,127) ; // create new hash table, and initialize with reasonable space.

    // Set run-time flags to compile time settings
    // Use AddVar() so that variable will get created if it was so far non-existing.
    // Currently, there are no global system runtime flags yet.
    // Separate software components (such as vhdl, verilog and database) register
    // and initialize their own variables using this 'global' static RuntimeFlags manager.

    // Initialize Verilog flags
    VeriRuntimeFlags::Initialize() ;

    // Initialize VHDL flags
    VhdlRuntimeFlags::Initialize() ;
}

/* static */
void
RuntimeFlags::DeleteAllFlags()
{
    // Remove all global vars
    delete _unsigned_vars ;
    _unsigned_vars = 0 ;
}

