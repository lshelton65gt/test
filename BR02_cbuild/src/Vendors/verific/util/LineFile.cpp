/*
 *
 * [ File Version : 1.98 - 2014/03/03 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "LineFile.h"

#include <stdlib.h>
#include <string.h>

#include "FileSystem.h"
#include "Strings.h"
#include "Array.h"
#include "Map.h"

#ifndef WINDOWS
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// VIPER #3704: We need to create file-id from absolute file path. Relative filename vs. file-id
// may create different file ids for same file, or same file id for different files.
static Map *abs_filename_to_id = 0 ; // Absolute filename to id mapping
static Map *id_to_abs_filename = 0 ; // id to absolute filename reverse mapping
static Map *rel_filename_to_id = 0 ; // Relative filename to id mapping
static Map *id_to_rel_filename = 0 ; // id to relative filename reverse mapping for messages
static unsigned file_id = 0 ;

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
// RD: 5/2010: replaced col_line_structs by memory pool.
// (when the compile flag VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK is not defined?)
/* static */ Array *ColLineFile::_col_line_structs = 0 ; // Array of ColLineFile structs
#define COL_LINE_STRUCTS_INIT_SIZE 4096 // Reasonable initial size for _col_line_structs Array

// Initialize the ColLineFile memory manager
/* static */ MemPool *ColLineFile::_mem_manager = 0 ; // linefile/col memory manager
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

LineFile::LineFile()  { }
LineFile::~LineFile() { }

//////////////////////////////////////////////////////////////////////
// Delete all code that encodes line/file in a (unsigned) scalar :
//////////////////////////////////////////////////////////////////////

#if !defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) && !defined(VERIFIC_LARGE_LINEFILE)
// Default setting : linefile_type is a unsigned long.

/*static*/ linefile_type
LineFile::EncodeLineFile(const char *filename, unsigned lineno)
{
    if (!filename) return (linefile_type)lineno ;

    // VIPER #7139: Use case insensitive hashing under Windows (STRING_HASH_FILE_PATH):
    if (!abs_filename_to_id) abs_filename_to_id = new Map(STRING_HASH_FILE_PATH) ;
    if (!id_to_abs_filename) id_to_abs_filename = new Map(NUM_HASH) ;
    if (!rel_filename_to_id) rel_filename_to_id = new Map(STRING_HASH) ;
    if (!id_to_rel_filename) id_to_rel_filename = new Map(NUM_HASH) ;

    // Logic in this routine assume both line and file information can't be
    // larger than 32 bits each.

    // Find if this file name has an id already
    // VIPER #3704: Check the absolute file path for the mapped id:
    char *abs_filename = FileSystem::Convert2AbsolutePath(filename, 1 /*compact*/) ;
    unsigned long long_id = (unsigned long)abs_filename_to_id->GetValue(abs_filename) ;
    unsigned id = (unsigned)long_id;
    VERIFIC_ASSERT(id == long_id); // Verify that this is the case.
    if (!id) {
        // This filename has not been registered yet.  Do it now.

        // Check to see if we are at the NUM_FILE_BITS limit.  If we are, reset file_id to
        // a low number (earlier files are now forgotton).  Yes, I know, this a hack.  Use
        // the new filename, assuming that the old file will not be used any more.
        if (file_id >= MAX_FILE_VALUE) {
            // FIX ME : Should we push out a Warning message here?
            file_id = 20 ;
        }

        file_id++ ; // System-wide counter ; counts only up for each new encounted file.

        // Insert file name <-> file id association (guaranteed one-to-one mapping):
        (void) abs_filename_to_id->Insert(abs_filename, (void*)(unsigned long)file_id) ;
        (void) id_to_abs_filename->Insert((void*)(unsigned long)file_id, abs_filename) ;
        // Also insert the relative file name <-> file id association (both are one-to-many mapping):
        char *rel_filename = Strings::save(filename) ;
        (void) rel_filename_to_id->Insert(rel_filename, (void*)(unsigned long)id, 0, 1 /* force insert */) ;
        (void) id_to_rel_filename->Insert((void*)(unsigned long)id, rel_filename, 0, 1 /* force insert */) ;

        id = file_id ;
    } else {
        Strings::free(abs_filename) ; // Not used, free it
    }

    // Need to check the relative mapping also (for messages). We will output the last relative
    // file path in the next message if the same physical file is analyzed multiple times:
    if (!rel_filename_to_id->GetValue(filename)) {
        // This is a new relative path that never came before, need to store it in the mapping:
        char *rel_filename = Strings::save(filename) ;
        (void) rel_filename_to_id->Insert(rel_filename, (void*)(unsigned long)id, 0, 1 /* force insert */) ;
        (void) id_to_rel_filename->Insert((void*)(unsigned long)id, rel_filename, 0, 1 /* force insert */) ;
    }

    // To encode : take the lineno, and use left bits for the file id.
    // This way we can increment the result to increment the lineno in the same file.
    return (linefile_type)((linefile_type)lineno | ((linefile_type)id << NUM_LINE_BITS)) ;
}

/*------------------------------------------------*/

/*static*/ linefile_type
LineFile::IncrLineNo(linefile_type linefile)
{
    // Increment only if we do not exceed the maximum line number :
    if (GetLineNo(linefile) >= MAX_LINE_VALUE) return linefile ;
    // Return the increment.
    return (linefile_type)(linefile + 1) ;
}

/*static*/ linefile_type
LineFile::SetLineNo(linefile_type linefile, unsigned lineno)
{
    if (!lineno) return linefile ; // 0 line no is invalid

    // VIPER #3017: If the given line number exceeds the maximum possible line number
    // then set the line number to the maximum value so that we don't overflow:
    if (lineno > MAX_LINE_VALUE) lineno = MAX_LINE_VALUE ;
    // Mask out the old line number :
    linefile_type new_linefile = (linefile & (~0L << NUM_LINE_BITS)) ;
    // Add the new line number, to obtain the new result :
    return (linefile_type)(new_linefile + lineno) ;
}

/* static */ linefile_type
LineFile::SetFileId(linefile_type linefile, unsigned fileid)
{
    if (!fileid) return linefile ; // 0 file id is invalid

    if (fileid > MAX_FILE_VALUE) fileid = MAX_FILE_VALUE ;
    // Mask out the old file id:
    linefile_type new_linefile = (linefile & (~0UL >> NUM_FILE_BITS)) ;
    // Add the new file id to obtain the new result:
    return (new_linefile + ((linefile_type)fileid << NUM_LINE_BITS)) ;
}

/*------------------------------------------------*/

/*static*/ const char *
LineFile::GetFileName(const linefile_type linefile)
{
    // Retrieve original (as it was read in) filename
    linefile_type id = (linefile >> NUM_LINE_BITS) ;
    if (!id) return 0 ; // No filename available
    return (id_to_rel_filename) ? (char*)id_to_rel_filename->GetValue((void*)id) : 0 ;
}

/*static*/ const char *
LineFile::GetAbsFileName(const linefile_type linefile)
{
    // Retrieve absolute (path) filename
    linefile_type id = (linefile >> NUM_LINE_BITS) ;
    if (!id) return 0 ; // No filename available
    return (id_to_abs_filename) ? (char*)id_to_abs_filename->GetValue((void*)id) : 0 ;
}

/*static*/ unsigned
LineFile::GetLineNo(const linefile_type linefile)
{
    // Mask the fileid bits, (return right most NUM_LINE_BITS portion)
    return (unsigned)(linefile & MAX_LINE_VALUE) ;
}

/*static*/ unsigned
LineFile::GetFileId(const linefile_type linefile)
{
    // Return the file id portion. (small number will be returned).
    return (unsigned)(linefile >> NUM_LINE_BITS) ;
}

/*static*/ unsigned
LineFile::GetFileId(const char *filename)
{
    // Return the file id portion.
    // Can't use relative file name <-> id mapping because same relative file may have
    // different file ids. Note that if current directory is changed this may fail.
    char *abs_filename = FileSystem::Convert2AbsolutePath(filename, 1 /*compact*/) ;
    unsigned id = (abs_filename_to_id) ? (unsigned)(unsigned long)abs_filename_to_id->GetValue(abs_filename) : 0 ;
    Strings::free(abs_filename) ;
    return id ;
}

/*------------------------------------------------*/

#else // !defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) && !defined(VERIFIC_LARGE_LINEFILE)

// Optional : linefile_type is a structure with column info :

//////////////////////////////////////////////////////////////////////
// Re-implement the LineFile interface :
//////////////////////////////////////////////////////////////////////

/*static*/ linefile_type
LineFile::EncodeLineFile(const char *filename, unsigned lineno)
{
    // Create a new ColLineFile object :
    ColLineFile *linefile = new ColLineFile(filename, lineno) ;

    // ColLineFile objects are owned here, 'static' and
    // memory management is done inside the constructors.
    return linefile ;
}

/*------------------------------------------------*/

/*static*/ linefile_type
LineFile::IncrLineNo(linefile_type linefile)
{
    if (!linefile) return 0 ;

    // For backward compatibility with only line info : increment '_right_line' :
    linefile->SetRightLine(linefile->GetRightLine() + 1) ;

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // We keep the current position in the _right_pos, so reset that too :
    linefile->SetRightCol(1) ;
#endif

    return linefile ;
}

/*static*/ linefile_type
LineFile::SetLineNo(linefile_type linefile, unsigned lineno)
{
    if (!lineno) return linefile ; // 0 line no is invalid

    // This routine is essentially called only for scalar linefile_type.
    // For this (structure one), we will set the right line number only ?
    if (!linefile) return 0 ;
    linefile->SetRightLine(lineno) ;
    return linefile ;
}

/* static */ linefile_type
LineFile::SetFileId(linefile_type linefile, unsigned fileid)
{
    if (!fileid) return linefile ; // 0 file id is invalid

    // Call the built-in reoutine on the linefile type:
    if (linefile) linefile->SetFileId(fileid) ;
    return linefile ;
}

/*------------------------------------------------*/

/*static*/ const char *
LineFile::GetFileName(const linefile_type linefile)
{
    // Retrieve original (as it was read in) filename
    return (linefile) ? linefile->GetFileName() : 0 ;
}

/*static*/ const char *
LineFile::GetAbsFileName(const linefile_type linefile)
{
    // Retrieve absolute (path) filename
    return (linefile) ? linefile->GetAbsFileName() : 0 ;
}

/*static*/ unsigned
LineFile::GetLineNo(const linefile_type linefile)
{
    return (linefile) ? linefile->GetRightLine() : 0 ;
}

/*static*/ unsigned
LineFile::GetFileId(const linefile_type linefile)
{
    return (linefile) ? linefile->GetFileId() : 0 ;
}

/*static*/ unsigned
LineFile::GetFileId(const char *filename)
{
    // Return the file id portion.
    // Can't use relative file name <-> id mapping because same relative file may have
    // different file ids. Note that if current directory is changed this may fail.
    char *abs_filename = FileSystem::Convert2AbsolutePath(filename, 1 /*compact*/) ;
    unsigned id = (abs_filename_to_id) ? (unsigned)(unsigned long)abs_filename_to_id->GetValue(abs_filename) : 0 ;
    Strings::free(abs_filename) ;
    return id ;
}

//////////////////////////////////////////////////////////////////////
// ColLineFile implementation
//////////////////////////////////////////////////////////////////////

#ifdef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
// Dec-2008: Change in behaviour in column-line-file object ALLOCATION.
// After the above date we allocate a chunk of linefile C-array and
// use individual positions for the single linefile objects. Here is
// what we do now...
//
//   1. Over loaded operator 'new' (non-array version) for class ColLineFile
//      a) It allocates a chunk of memory of ColLineFile object.
//      b) Inserts this chunk pointer into the '_col_line_structs' Array.
//      c) Keeps a static unsigned index into the chunk allocated.
//      d) Returns the next avaiable index position from the chunk.
//
//   2. Over loaded operator 'delete' (non-array version) for class ColLineFile
//      that does nothing. ColLineFile objects should not be deleted
//      one by one. That will leave stale pointers in _col_line_structs.
//      We suggest to use LineFile::DeleteAllLineFiles() API to free
//      allocated linefile objects.
//
//   3. Had to define the 'default' constructor for ColLineFile class.
//      Normally we do not define it but it is required to compile the code.
//
//   4. Array version of new/delete operators are overloaded only under the
//      Verific Memory Manager flag and it does not do anything special
//      for the ColLineFile class. The behaviour is same for other classes
//      under Verific Memory Manager flag.
//
// The reason we switched to this approach is that it significantly reduces
// memory usage. The memory usage decreases because it allocates a big chunk
// instead of large number of small pieces, so it does not have the overhead
// associated with allocating large number of small memory blocks. linefile
// objects are used for all parse tree nodes, so it is used in a large number
// for all practical designs.
//
// Some statistics with box-100 design having column info ON and chunk size 1024:
//
//   Verific Memory Manager OFF and default 'new' operator    : 171 MB
//   Verific Memory Manager ON  and default 'new' operator    : 160 MB
//   Verific Memory Manager OFF and overloaded 'new' operator : 146 MB
//   Verific Memory Manager ON  and overloaded 'new' operator : 135 MB
//
// So, we are achieving around 15% decrease in memory usage with this new approach than before.

// Define the size of the memory chunk we allocate. Larger the chunk size, less the overhead
// of allocation and larger the chance of unused memory. Memory remains unused when remainder
// of the division (total number of linefile objects created) by COL_LINEFILE_CHUNK_SIZE is a
// small number. We choose 1024, so atmost (sizeof(ColLineFile)*1023) bytes may remain unused.
#define COL_LINEFILE_CHUNK_SIZE 1024
void *ColLineFile::operator new(size_t /* size */)
{
    // Keep a static index into the memory chunk allocated.
    static unsigned chunk_idx = 0 ;
    if (!_col_line_structs) _col_line_structs = new Array(COL_LINE_STRUCTS_INIT_SIZE) ;

    ColLineFile *col_linefile_chunk = 0 ;
    // Even if chunk index is valid, if there is no chunk already allocated, we need
    // to allocate a new chunk. This can happen the first time a linefile is being
    // created or after a call to to the routine LineFile::DeleteAllLineFiles().
    if ((chunk_idx >= COL_LINEFILE_CHUNK_SIZE) || !_col_line_structs->Size()) {
        // We are out of space in the allocated chunk, allocate a new chunk:
        // Goes through Verific Memory Manager if it is defined:
        col_linefile_chunk = VFC_ARRAY_NEW(ColLineFile, COL_LINEFILE_CHUNK_SIZE) ;
        _col_line_structs->Insert(col_linefile_chunk) ; // Insert it into the Array
        chunk_idx = 0 ; // Back to the first index position for the new chunk
    }

    // Get the last inserted chunk from the Array:
    col_linefile_chunk = (ColLineFile *)_col_line_structs->GetLast() ;

    // Return the next available position from the chunk and increase the index:
    return (void *)(&col_linefile_chunk[chunk_idx++]) ;
}

void ColLineFile::operator delete(void * /* p */)
{
    // Do nothing. A linefile should never be deleted this way.
    // If deleted it will leave stale pointers in the _col_line_structs.
    // Use the LineFile::DeleteAllLineFiles() routine to delete them.
}

// Normally we don't define the 'default' constructor, but this constructor had
// to be defined as it is required for the overloaded 'new' operator to compile.
ColLineFile::ColLineFile()
  :
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    _left_col(0),
    _right_col(0),
    _left_line(0),
#endif
    _right_line(0),
    _file_id(0)
{
}
#endif // VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK

ColLineFile::ColLineFile(const char *filename, unsigned lineno)
  :
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    _left_col(1),
    _right_col(1),
    _left_line(lineno),
#endif
    _right_line(lineno),
    _file_id(0)
{
    // Create a ColLineFile starting at filename/lineno.
    // VIPER #7139: Use case insensitive hashing under Windows (STRING_HASH_FILE_PATH):
    if (!abs_filename_to_id) abs_filename_to_id = new Map(STRING_HASH_FILE_PATH) ;
    if (!id_to_abs_filename) id_to_abs_filename = new Map(NUM_HASH) ;
    if (!rel_filename_to_id) rel_filename_to_id = new Map(STRING_HASH) ;
    if (!id_to_rel_filename) id_to_rel_filename = new Map(NUM_HASH) ;

    // First, obtain the file identifier :
    // Cast via 'long' done to support 64 bit compile
    if (filename) {
        // VIPER #3704: Check the absolute file path for the mapped id:
        char *abs_filename = FileSystem::Convert2AbsolutePath(filename, 1 /*compact*/) ;
        unsigned long id = (unsigned long)abs_filename_to_id->GetValue(abs_filename) ;
        if (!id) {
            // File not yet encountered before : Create a new identifier :
            file_id++ ; // System-wide counter ; counts only up for each new encounted file.

            // Insert file_name <-> file_id association (guaranteed one-to-one mapping):
            (void) abs_filename_to_id->Insert(abs_filename, (void*)(unsigned long)file_id) ;
            (void) id_to_abs_filename->Insert((void*)(unsigned long)file_id, abs_filename) ;
            // Also insert the relative file name <-> file id association (both are one-to-many mapping):
            char *rel_filename = Strings::save(filename) ;
            (void) rel_filename_to_id->Insert(rel_filename, (void*)(unsigned long)id, 0, 1 /* force insert */) ;
            (void) id_to_rel_filename->Insert((void*)(unsigned long)id, rel_filename, 0, 1 /* force insert */) ;

            id = file_id ;
        } else {
            Strings::free(abs_filename) ; // Not used, free it
        }

        // Need to check the relative mapping also (for messages). We will output the last relative
        // file path in the next message if the same physical file is analyzed multiple times:
        if (!rel_filename_to_id->GetValue(filename)) {
            // This is a new relative path that never came before, need to store it in the mapping:
            char *rel_filename = Strings::save(filename) ;
            (void) rel_filename_to_id->Insert(rel_filename, (void*)(unsigned long)id, 0, 1 /* force insert */) ;
            (void) id_to_rel_filename->Insert((void*)(unsigned long)id, rel_filename, 0, 1 /* force insert */) ;
        }

        // Set the file id :
        // Do not directly assign, set it via API to handle overflow.
        //_file_id = (unsigned) id ;
        SetFileId(id) ;
    } else {
        _file_id = 0 ; // No file name (yet).
    }

    // Store in a static array :
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
    // Do not insert individual linefile objects into this Array. We now changed to
    // a new approach where we allocate a large chunk and insert it into the Array.

// RD: 5/2010: do NOT insert into col_line_structs array.
// Instead, we simply will use the dedicated memory pool to 'store' (and free) line/file/columns structs.
#endif
}

ColLineFile *
ColLineFile::Copy() const
{
    ColLineFile *result = new ColLineFile(0,0) ;
    // Copy all fields :
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    result->SetLeftCol(GetLeftCol()) ;
    result->SetRightCol(GetRightCol()) ;
    result->SetLeftLine(GetLeftLine()) ;
#endif
    result->SetRightLine(GetRightLine()) ;
    result->SetFileId(GetFileId()) ;
    return result ;
}

ColLineFile::~ColLineFile()
{
    // Nothing to do :
    // Presently, we do not attempt to clean up individual structures, since we
    // do not know how many users there are of a specific ColLineFile object.
}

/*------------------------------------------------*/

const char *
ColLineFile::GetFileName() const
{
    // Retrieve original (as it was read in) filename
    if (!_file_id) return 0 ; // no filename available
    return (id_to_rel_filename) ? (const char*)id_to_rel_filename->GetValue((void*)(unsigned long)_file_id) : 0 ;
}

const char *
ColLineFile::GetAbsFileName() const
{
    // Retrieve absolute (path) filename
    if (!_file_id) return 0 ; // no filename available
    return (id_to_abs_filename) ? (const char*)id_to_abs_filename->GetValue((void*)(unsigned long)_file_id) : 0 ;
}

/*------------------------------------------------*/

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
void
ColLineFile::MergeMaxSpan(const ColLineFile *other_side)
{
    // Here, we find the left-most and right-most position/line
    // from both 'this' and 'other_side' and set it in 'this'.
    // This routine creates a left->right 'span' which covers both touples.
    if (!other_side) return ;

    if (other_side == this) return ; // they are the same. Nothing to do.

    if (GetFileId() != other_side->GetFileId()) {
        // Files are different. Should not happen.
        // Not sure what to do (warn? assert?)
        // leave this unchanged for now.
        return ;
    }

    // Find the left-most line/column :
    if (GetLeftLine() == other_side->GetLeftLine()) {
        // Both lines are the same. This happens very often. Lowest column becomes left :
        _left_col = (GetLeftCol() <= other_side->GetLeftCol()) ? GetLeftCol() : other_side->GetLeftCol() ;
    } else if (GetLeftLine() < other_side->GetLeftLine()) {
        // Multi-line span : 'this' side is going to be the left side.
        // 'this' column automatically wins. So nothing changes
    } else {
        // Multi-line span : 'other_side' side is going to be the lowest line number :
        _left_col = other_side->GetLeftCol() ;
        _left_line = other_side->GetLeftLine() ;
    }

    // Find the right-most line/column :
    if (GetRightLine() == other_side->GetRightLine()) {
        // Both lines are the same. This happens very often. Highest column becomes left :
        _right_col = (GetRightCol() >= other_side->GetRightCol()) ? GetRightCol() : other_side->GetRightCol() ;
    } else if (GetRightLine() > other_side->GetRightLine()) {
        // Multi-line span : 'this' side is the highest line number.
        // 'this' column automatically wins. So nothing changes.
    } else {
        // Multi-line span : 'other_side' side is going to be the highest :
        _right_col = other_side->GetRightCol() ;
        _right_line = other_side->GetRightLine() ;
    }
}
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS

/*------------------------------------------------*/

#endif // !defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) && !defined(VERIFIC_LARGE_LINEFILE)

/*static*/ const char *
LineFile::GetFileNameFromId(unsigned id)
{
    // Retrieve original (as it was read in) filename
    if (!id) return 0 ; // No filename available
    return (id_to_rel_filename) ? (char*)id_to_rel_filename->GetValue((void*)(unsigned long)id) : 0 ;
}

/*static*/ const char *
LineFile::GetAbsFileNameFromId(unsigned id)
{
    // Retrieve absolute (path) filename
    if (!id) return 0 ; // No filename available
    return (id_to_abs_filename) ? (char*)id_to_abs_filename->GetValue((void*)(unsigned long)id) : 0 ;
}

/*------------------------------------------------*/

/* static */ void
LineFile::DeleteAllLineFiles()
{
#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS)
    // Empty out the memory pool, and re-initialize :
    ColLineFile::DeleteAllLineFiles() ;
#else
    // If we are not preserving the column info then linefile is a simple 'unsigned'
    // In that case it gets automatically 'deleted' along with the parse tree nodes.
    // So we don't have to do anything special here to clean them up.
#endif

    // Since all linefiles are deleted now, we can safely reset the file-id maps:
    ResetFileIdMaps() ;
}

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
/* static */ void
ColLineFile::DeleteAllLineFiles()
{
    // VIPER #4175: Delete all the allocated linefile objects under this compile flag:
    // WARNING: Please be absolutely sure that there is no reference of linefile
    // anymore in the memory before calling this routine. Otherwise we will corrupt.
    unsigned i ;
    linefile_type lf ;
    FOREACH_ARRAY_ITEM(_col_line_structs, i, lf) {
#ifdef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
        VFC_ARRAY_DELETE(lf) ;
#else
        delete lf ;
#endif
    }
    delete _col_line_structs ;
    _col_line_structs = 0 ;

    // Release all allocated line/file/column info to the OS.
    delete _mem_manager ;
    _mem_manager = 0 ;
}
#endif

/* static */ void
LineFile::ResetFileIdMaps()
{
    MapIter mi ;
    char *file_name ;

    // Clear absolute file name vs file id maps
    if (abs_filename_to_id)
    FOREACH_MAP_ITEM(abs_filename_to_id, mi, &file_name, 0) {
        Strings::free(file_name) ;
    }
    delete abs_filename_to_id ;
    abs_filename_to_id = 0 ;

    delete id_to_abs_filename ;
    id_to_abs_filename = 0 ;

    // Now clear relative file-name vs file id maps
    FOREACH_MAP_ITEM(rel_filename_to_id, mi, &file_name, 0) {
        Strings::free(file_name) ;
    }
    delete rel_filename_to_id ;
    rel_filename_to_id = 0 ;

    delete id_to_rel_filename ;
    id_to_rel_filename = 0 ;

    // Now reset the file_id field:
    file_id = 0 ;
}

/* static */ unsigned
LineFile::Compare(const linefile_type left, const linefile_type right)
{
    // Return 1 if left and right points to same location, 0 otherwise
    if (!left || !right) return 0 ;

    if (GetFileId(left) != GetFileId(right)) return 0 ; // Different file

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
    if (left==right) return 1 ; // Pointer equal

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    if (left->GetLeftCol()   != right->GetLeftCol())   return 0 ;
    if (left->GetRightCol()  != right->GetRightCol())  return 0 ;
    if (left->GetLeftLine()  != right->GetLeftLine())  return 0 ;
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS
    if (left->GetRightLine() != right->GetRightLine()) return 0 ;
#else
    if (GetLineNo(left) != GetLineNo(right)) return 0 ;
#endif

    return 1 ; // Equal
}

/* static */ unsigned
LineFile::LessThan(const linefile_type left, const linefile_type right, unsigned less_than_equal /*=0*/)
{
    // Return 1 if left (in full) comes before right (or is same if less_than_equal==1) in same file, 0 otherwise
    // For protected RTL, we don't know the line no..
    if (!left || !right || IsProtectedLinefile(left) || IsProtectedLinefile(right)) return 0 ;

    if (GetFileId(left) != GetFileId(right)) return 0 ; // Different file

    if (less_than_equal && Compare(left, right)) return 1 ; // Equal

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    if (left->GetRightLine()  >  right->GetLeftLine()) return 0 ;
    if ((left->GetRightLine() == right->GetLeftLine()) &&
        (left->GetRightCol()  >= right->GetLeftCol())) return 0 ;
#else
    if (GetLineNo(left) >= GetLineNo(right)) return 0 ;
#endif

    return 1 ;
}

/* static */ unsigned
LineFile::IsProtectedLinefile(const linefile_type linefile)
{
    if (!linefile) return 0 ;

    // Viper #8188 / 8208
    // Probe linenumber first (faster than file name) :
    if (LineFile::GetLineNo(linefile)) return 0 ;

    // No line number. Probe file name :
    return (LineFile::GetFileName(linefile)) ? 1 : 0 ;
}

