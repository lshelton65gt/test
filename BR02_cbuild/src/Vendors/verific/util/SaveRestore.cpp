/*
 *
 * [ File Version : 1.131 - 2014/03/01 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

//////////////////////////////////////////////////////////////////////
// SaveRestore.cpp: implementation of the SaveRestore class.
//////////////////////////////////////////////////////////////////////

#include <time.h>     // Needed for timestamp
#include <limits.h>   // ULONG_MAX
#include <string.h>   // For strcpy
#include <ctype.h>    // For tolower, isupper, isalnum and isdigit

#include "SaveRestore.h"

#include "Message.h"
#include "FileSystem.h"
#include "Strings.h"
#include "Protect.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*------------------------------------------------*/
// Version History : Introduced on 5/25/04
/*------------------------------------------------*/

// Version 0xA0000008 (11/15/13) :
//      - Modified integer save/restore.
//      - Saving/restoring of compile switch settings is introduced.
//        We now use a 32 bit int to represent 32 individual compile
//        settings.
//      - Saving/restoring of number of bits used for line, file in Linefile.

// Version 0xA0000007 (12/08/10) :
//      - Modified integer save/restore, save significant disk-space for small integers.
//        Modification reduces saverestore files by an average of 30%.

// Version 0xA0000006 (01/05/08) :
//      - For encryption support. If version is older than this, we save an
//        additional (known) integer to figure out if the decrypted integer
//        matches the known saved integer value. This is a quick check for
//        encryption-decryption consistency.

// Version 0xA0000005 (08/23/07) :
//      - For VIPER #3502, changed save restore to use register pointer machanism
//        to save/restore linefile object. This helps to avoid duplicate linefile
//        object for same values. Also restoring null linefile for null saved object.

// Version 0xA0000004 (10/18/05) :
//      - Added _sizeof_long member for 64bit restore support.  Added SaveLong()
//        and RestoreLong() routines, and changed SaveInteger()/RestoreInteger()
//        to save/restore int's instead of long's.  Added 32<->64 bit support.

// Version 0xA0000003 (4/15/05) :
//      - Added primitive encryption to SaveString/RestoreString
//        routines.

// Version 0xA0000002 (3/25/05) :
//      - We now save/restore a timestamp field.

// Version 0xA0000001 (6/29/04) :
//      - Made SaveRestore version number a full unsigned now (32 bits)
//        so the new version number is 0xA0000001, from 0xA001.

// Version 0xA001 (5/24/04) :
//      - Backward compatibility is now made possible with this version.
//      - Saving/restoring of compile switch settings is introduced.
//        We now use a 32 bit int to represent 32 individual compile
//        settings.
//      - Saving/restoring of number of bits used for line, file,
//        and column information in Linefile.

// Version 22 (Previous version) :
//      This versions contains absolutely no extra information, such
//      as compile-switch settings or Linefile bit size settings, in
//      it stream.  This means that backward compatibility is not
//      supported in this version.

/*------------------------------------------------*/

#define SAVE_RESTORE_ENCRYPTED_SIGNATURE  0x031F6B12
#define SAVE_RESTORE_SIGNATURE            0x031F6C12
#define SAVE_RESTORE_VERSION              0xA0000008

// Save this integer to confirm encryption/decryption consistency
#define ENCRYPTION_CHECKSUM               0x031F7AE4

// Marker used during line/file info save/restore, which indicates that a new file name is coming up (this is illegal linefile info)
#define LINEFILE_FILENAME      -1
#define LINEFILE_OBJECT        -2

// VIPER #3541 & #3790: Long, very long file name support:
#define MAP_FILE_SIGNATURE      0x21D47039
#define MAP_FILE_VER_NUM        0x00AB0001
#define DEFAULT_MAP_FILE_NAME   "_verific_info"

// The (fixed) size of the buffer (64 KB):
#define READ_WRITE_BUFFER_SIZE 65536

/* static */ char *SaveRestore::_map_file_name = 0 ;
/* static */ Map *SaveRestore::_msgs = 0 ;

/*------------------------------------------------*/

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#if 0
// This function returns the approximate number of tree nodes in a binary dump file.
// Using this will reduce number of rehash for the Maps _pointer2num and _num2pointer.
// The number 24 is used from experiments. It can varry from design to design.

unsigned
GetApproxPointerNumber(const char *filename)
{
    if (!FileSystem::IsFile(filename)) return 2 ; // Default is 2 when the file cannot be found

    // Approximate number of pointers that are going to be registered:
    return (statBuf.st_size/24) ;
}
#endif

SaveRestore::SaveRestore(const char *filename, unsigned restore, unsigned consider_file_mapping /* = 1 */, Protect *cipher /* = 0 */)
  : _file(0),
    _file_name(Strings::save(filename)),
    _pointer2num(NUM_HASH),
    _num2pointer(NUM_HASH),
    _filename2id(STRING_HASH),
    _id2filename(NUM_HASH),
    _pointer_number(0),
    _version(0),
    _timestamp(0),
    _restore(restore),
    _endian_shuffle(0),
    _systems_go(1),
    _encrypt(1),
    _new_linefile(0),
    _column_active(0),
    _large_linefile(0),
    _line_num_bits(0),
    _file_num_bits(0),
    _col_num_bits(0),
    _user_flags(0),
    _restore_version(0),
    _sizeof_long(4), // Set to 4, not 8, even for 64bit builds
    _restore_little_endian(0)
#ifdef USE_BUFFERED_READ_WRITE
    , _read_write_buffer(0)
    , _buffered_data_size(0)
    , _buffer_index(0)
    , _read_write_buffer_size(READ_WRITE_BUFFER_SIZE)
    , _cipher(cipher)
#endif
    , _buffer(0)
    , _buffer_size(256) // VIPER #7930: _buffer_size is now a member (initial size is 256)
{
#ifdef USE_BUFFERED_READ_WRITE
    if (_cipher) _read_write_buffer_size -= _read_write_buffer_size % _cipher->GetBlockSize() ;
    _read_write_buffer = Strings::allocate(_read_write_buffer_size) ;
#endif

    (void) cipher ;

    if (!filename) {
        _systems_go = 0 ;
        return ;
    }

    // VIPER #3541 & #3790: Long, very long file name support:
    const char *map_file_name = GetMapFileName() ; // Get the user specified map file name
    // If mapping file name was not specified, use the default map file name:
    if (!map_file_name) map_file_name = DEFAULT_MAP_FILE_NAME ;
    // Now check whether this file is/should be mapped with another file:
    char *mapped_file = (consider_file_mapping) ? GetMappedFile(filename, map_file_name, restore) : 0 ;
    if (mapped_file) {
        // Use the mapped file instead of the given file:
        Strings::free(_file_name) ;
        _file_name = mapped_file ; // Absorbe the allocated string
    }

    // First check to see if this file exists (only for restore)
    if (_restore) {
        if (!FileSystem::PathExists(_file_name)) {
            Error("cannot find file \"%s\"", _file_name) ;
            _systems_go = 0 ;
            return ;
        }
        if (!FileSystem::IsFile(_file_name)) {
            Error("\"%s\" is not a file", _file_name) ;
            _systems_go = 0 ;
            return ;
        }
    }

    // Make sure to open the file as a Binary file
    _file = ::fopen(_file_name, (restore) ? "rb" : "wb") ;
    if (!_file) {
        if (restore) {
            Error("cannot open file \"%s\" for reading", _file_name) ;
        } else {
            Error("cannot open file \"%s\" for writing", _file_name) ;
        }
        _systems_go = 0 ;
        return ;
    }

    if (!restore) {
        //-------------------------------------------
        // SAVE
        //-------------------------------------------
        // Push out the signature
        int sr_signature = SAVE_RESTORE_SIGNATURE ;
#ifdef USE_BUFFERED_READ_WRITE
        if (_cipher) sr_signature = SAVE_RESTORE_ENCRYPTED_SIGNATURE ;
#endif
        SaveInteger(sr_signature, 1) ; // no encryption writing this info, buffer is also empty

        // Push out the version number
        SaveInteger(SAVE_RESTORE_VERSION, 1) ; // no encryption writing this info, buffer is also empty

        // Save the encryption algorithm key
        SaveInteger(cipher ? cipher->GetUniqueKey() : 0, 1) ; // no encryption

        // Save an additional integer to confirm encryption/decryption consistency
        // We must successfully restore this saved integer ENCRYPTION_CHECKSUM
        SaveInteger(ENCRYPTION_CHECKSUM) ; // this is the first encrypted data, through the buffer

        // Push out sizeof(long).  Needed at restore time for 64bit support..
        SaveChar(sizeof(long)) ;

        // Push out the timestamp
        _timestamp = (unsigned long)::time(0) ;
        SaveLong(_timestamp) ;

        // Push out string encryption setting
        SaveChar(_encrypt) ;

        // Push out compile switch settings.  These settings are encoded in an
        // 32 bit unsigned, where each bit position represents a compile flag.
        unsigned compile_flags = 0 ;

        // bit 0 : VERIFIC_LINEFILE_INCLUDES_COLUMNS
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
        compile_flags |= 0x1 << 0 ;
#else
#ifdef VERIFIC_LARGE_LINEFILE
        compile_flags |= 0x2 ;
#endif
#endif

        SaveInteger(compile_flags) ;

        // Dump Linefile constant sizes
        SaveInteger(NUM_LINE_BITS) ;
        SaveInteger(NUM_FILE_BITS) ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
        SaveInteger(NUM_COL_BITS) ;
#else
        SaveInteger(0) ;
#endif
    } else {
        //-------------------------------------------
        // RESTORE
        //-------------------------------------------
        // Parse the header : <signature>.  We'll figure endianness of stream by how
        // signature was saved.
        int expected_signature = SAVE_RESTORE_SIGNATURE ;
#ifdef USE_BUFFERED_READ_WRITE
        if (_cipher) expected_signature = SAVE_RESTORE_ENCRYPTED_SIGNATURE ;
#endif

        int signature = RestoreInteger(1) ; // no need to consider encryption, buffer is also empty
        unsigned encr_diff = 0 ;
        unsigned encr_diff_shuffle = 0 ;
        if (signature != expected_signature) {
            // It could be that signature was saved as a long on a 64 bit machine.  In that case,
            // we want to make sure and issue an appropriate message.

            // Check if the restored signature (before or after endian shuffle) matches
            // encrypted or base signature. Then :
            // - database was saved with encryption ON and restore has decrypt OFF -> This is an error
            // - database was saved with encryption OFF and restore has decrypt ON -> This is an warning
            encr_diff = (signature == SAVE_RESTORE_ENCRYPTED_SIGNATURE  || signature == SAVE_RESTORE_SIGNATURE) ;
            if (!encr_diff) {
                // Shuffle it
                EndianShuffle(signature) ;
                if (signature != expected_signature) {
                    encr_diff_shuffle = (signature == SAVE_RESTORE_ENCRYPTED_SIGNATURE  || signature == SAVE_RESTORE_SIGNATURE) ;
                    if (!encr_diff_shuffle) {
                        // Now try one last thing.  Restore another int and make a 64 long out of it.
                        signature = RestoreInteger(1) ;
                        if (signature != expected_signature) {
                            encr_diff = (signature == SAVE_RESTORE_ENCRYPTED_SIGNATURE  || signature == SAVE_RESTORE_SIGNATURE) ;
                            if (!encr_diff) {
                                EndianShuffle(signature) ;
                                if (signature != expected_signature) {
                                    encr_diff_shuffle = (signature == SAVE_RESTORE_ENCRYPTED_SIGNATURE  || signature == SAVE_RESTORE_SIGNATURE) ;
                                    if (!encr_diff_shuffle) {
                                        Error("file %s is probably not a Verific binary database file", _file_name) ;
                                        _systems_go = 0 ;
                                        return ;
                                    }
                                }
                                _endian_shuffle = 1 ;
                            }
                        }

                        //if (sizeof(long) != 8) {
                        //    // At this point we know that we're on a 32bit platfrom and the vdb was produced from a 64bit machine
                        //    Warning("file %s was created with a sizeof(long) larger than current machine ; attempting to restore", _file_name) ;
                        //}
                        _sizeof_long = 8 ;
                    }
                } else {
                    // Set the endian shuffle flag
                    _endian_shuffle = 1 ;
                }
            }
        }

        if ((encr_diff || encr_diff_shuffle) && (expected_signature == SAVE_RESTORE_ENCRYPTED_SIGNATURE)) {
            // database file is not encrypted.. turn off _cipher
            Warning("file %s is not an encrypted database, turning off decryption before restoring it", _file_name) ;
#ifdef USE_BUFFERED_READ_WRITE
            _cipher = 0 ;
#endif
            if (encr_diff_shuffle) _endian_shuffle = 1 ;

            // If customer upgrades the above warning to an error, exit right now, but of course
            // we do not have any message id for util messages, and customer cannot change the
            // message severity at runtime
            if (Message::ErrorCount()) {
                _systems_go = 0 ;
                return ;
            }
        } else if ((encr_diff || encr_diff_shuffle) && (expected_signature == SAVE_RESTORE_SIGNATURE)) {
            // This is an error.. Customer must plug in decryption algorithm to read
            // an encrypted database
            Error("file %s is an encrypted database", _file_name) ;
            _systems_go = 0 ;
            return ;
        }

        // Do an endian test on the vdb.  This will be needed for 64bit restoring of linefile info correctly.
        int i = 1 ;
        char little_endian = *((char*)&i) ;
        _restore_little_endian = (_endian_shuffle) ? ~(little_endian & 1) : little_endian ;

        // Parse the version.  Don't store value in _version member field, because this field
        // is typically used (overwritten by) application (the user/caller of this object)
        _restore_version = (unsigned)RestoreInteger(1) ; // No encryption AND buffer is empty
        if (!_restore_version) {
            // If _restore_version is zero, then this means the vdb is most likely a 64bit vdb.  In this
            // case we need to restore another integer to get the restore version number.
            _restore_version = (unsigned)RestoreInteger(1) ; // No encryption AND buffer is empty
        }

        // Test version.  The last supportable backward compatible version is 0xA001 (16-bits).
        // As of 6/29/04, this version number was increased to 32-bits, (0xA0000001).
//        if (_restore_version < 0xA001) {
        // RD: 12/2010: removed support for restore of versions older than 0xA0000004 (saved before 10/2005)
        if (_restore_version < 0xA0000004) {
            Error("file %s was created with SaveRestore version %x which is no longer supported", _file_name, _restore_version) ;
            Info("contact support@verific.com if retrieval is essential") ;
            _systems_go = 0 ;
            return ;
        }

        // if restore version is newer than or equals 0xA0000006 we have saved an additional integer
        // to confirm encryption/decryption consistency (if encrypted). We saved ENCRYPTION_CHECKSUM
        // in the database and we must be able to restore it as is.
        if (_restore_version >= 0xA0000006) {
            // Restore the encryption algorithm key (the key is not encrypted, of course!!)
            (void) RestoreInteger(1) ; // We do not use this key at this time, for future extension

            if (RestoreInteger() != ENCRYPTION_CHECKSUM) {
                Error("file %s is encrypted with a different algorithm; cannot restore with the provided decryption plug-in", _file_name) ;
                _systems_go = 0 ;
                return ;
            }
        }

        // Now parse the sizeof(long) value.  Needed for 64bit restore support.
        if (_restore_version >= 0xA0000004) {
            _sizeof_long = (unsigned)RestoreChar() ;
            // Issue error if vdb was saved on 64bit machine, but is being restored on a 32bit machine.
            //if ((size_t)_sizeof_long > sizeof(long)) {
            //    Warning("file %s was created with a sizeof(long) larger than current machine ; attempting to restore", _file_name) ;
            //}
        }

        // Now parse the timestamp, which specifies when this file was created
        if (_restore_version >= 0xA0000002) {
            _timestamp = (_restore_version >= 0xA0000004) ? (unsigned long)RestoreLong() : (unsigned long)RestoreInteger() ;
        }

        // Parse string encryption setting
        _encrypt = 0 ;
        if (_restore_version >= 0xA0000003) {
            _encrypt = (_restore_version >= 0xA0000004) ? (unsigned)RestoreChar() : (unsigned)RestoreInteger() ;
        }

        // Check for linefile saving mechanism. With version 0xA0000005, we changed to a different
        // linefile save/restore mechanism, need this flag to check for which way to restore that:
        if (_restore_version >= 0xA0000005) _new_linefile = 1 ;

        // Now parse the compile flags information
        unsigned compile_flags = (unsigned)RestoreInteger() ;

        // Bit 0 : VERIFIC_LINEFILE_INCLUDES_COLUMNS
        _column_active = compile_flags & (0x1 << 0) ;

        // Bit 1 : VERIFIC_LARGE_LINEFILE
        _large_linefile = (compile_flags & 0x2) ? 1 : 0 ;

        // Restore Linefile constants
        _line_num_bits = (unsigned)RestoreInteger() ;
        _file_num_bits = (unsigned)RestoreInteger() ;
        _col_num_bits  = (unsigned)RestoreInteger() ; // If _col_num_bits==0, then column info was not enabled at save time
    }
}

/*------------------------------------------------*/

SaveRestore::~SaveRestore()
{
    if (IsOK()) {
        // Check that the file is properly parsed (signature is last integer in the file)
        if (_restore) {
            // RD: 5/2007: take out this (final signature) check.
            // It assumes that files are always fully restored and read until the end.
            // But that is no longer the case for special-purpose restores like the ones in VIPER 2944.
            // By taking out this last check we can use SaveRestore to restore only a part of the file,
            // which allows more flexible use of SaveRestore object.

            // When debugging mode, and when a file is fully restored, we might want to enable the check again,
            // because it still IS saved at save time.
#if 0
            int sig = RestoreInteger() ;
            (void)sig ; // Need this to remove unused variable build warning
            VERIFIC_ASSERT(sig == SAVE_RESTORE_SIGNATURE) ;
#endif

#if 0
            if (!feof(_file)) {
                // Even after we are done restoring we didn't hit the EOF!
                Error("%s is not fully restored", _file_name) ;
            }
#endif
        } else {
            // save signature at the end of the file.
#ifdef USE_BUFFERED_READ_WRITE
            if (_cipher) {
                SaveInteger(SAVE_RESTORE_ENCRYPTED_SIGNATURE) ;
            } else
#endif
            {
                SaveInteger(SAVE_RESTORE_SIGNATURE) ;
            }

#ifdef USE_BUFFERED_READ_WRITE
            // Flush the waiting-to-be-written buffer, if any, to the disk before we close the file:
            (void) FlushBuffer() ;
#endif
        }
    }
    if (_file) {
        ::fclose(_file) ;
    }
    Strings::free(_file_name);

    if (_buffer) Strings::free(_buffer); // JJ 130110

#ifdef USE_BUFFERED_READ_WRITE
    Strings::free(_read_write_buffer) ;
#endif

#if 0
    // RD: some 'debug' messages to check sizes of the hash tables used
    Message::Msg(VERIFIC_INFO,0,0,"_pointer2num uses %d bytes, with %d entries",_pointer2num.MemUsage(),_pointer2num.Size()) ;
    Message::Msg(VERIFIC_INFO,0,0,"_num2pointer uses %d bytes, with %d entries",_num2pointer.MemUsage(),_num2pointer.Size()) ;
    Message::Msg(VERIFIC_INFO,0,0,"_filename2id uses %d bytes, with %d entries",_filename2id.MemUsage(),_filename2id.Size()) ;
    Message::Msg(VERIFIC_INFO,0,0,"_id2filename uses %d bytes, with %d entries",_id2filename.MemUsage(),_id2filename.Size()) ;

    static unsigned max_size = 0 ;

    if (max_size<_pointer2num.Size()) max_size = _pointer2num.Size() ;
    if (max_size<_num2pointer.Size()) max_size = _num2pointer.Size() ;
    Message::Msg(VERIFIC_INFO,0,0,"MAX size : %d entries",max_size) ;
#endif
}

/*------------------------------------------------*/
unsigned
SaveRestore::HasProtectionObject() const
{
    unsigned cipher_set = 0 ;
#ifdef USE_BUFFERED_READ_WRITE
    if (_cipher) cipher_set = 1 ;
#endif
    return cipher_set ;
}

/*------------------------------------------------*/

/*static*/ const char *
SaveRestore::GetMessageId(const char *msg)
{
    if (!msg) return 0 ;

    if (!_msgs) {
        _msgs = new Map(STRING_HASH, 37) ; // Table size : choose prime number for good hashing. Enough initial storage until message VBSR-1037.

        _msgs->Insert("cannot find file \"%s\"", "VBSR-1000") ;
        _msgs->Insert("\"%s\" is not a file", "VBSR-1001") ;
        _msgs->Insert("cannot open file \"%s\" for reading", "VBSR-1002") ;
        _msgs->Insert("cannot open file \"%s\" for writing", "VBSR-1003") ;
        _msgs->Insert("file %s is probably not a Verific binary database file", "VBSR-1004") ;
        //_msgs->Insert("file %s was created with a sizeof(long) larger than current machine ; attempting to restore", "VBSR-1005") ;
        _msgs->Insert("file %s is an encrypted database", "VBSR-1006") ;
        //_msgs->Insert("file %s was created with a SaveRestore version which is not supported ; cannot restore", "VBSR-1007") ;
        _msgs->Insert("file %s is encrypted with a different algorithm; cannot restore with the provided decryption plug-in", "VBSR-1008") ;
        _msgs->Insert("%s is not fully restored", "VBSR-1009") ;
        _msgs->Insert("%s does not contain a known mapping file format", "VBSR-1010") ;
        _msgs->Insert("file %s is not an encrypted database, turning off decryption before restoring it", "VBSR-1011") ;
        _msgs->Insert("contact support@verific.com if retrieval is essential", "VBSR-1012") ;
        _msgs->Insert("file %s was created with SaveRestore version %x which is no longer supported", "VBSR-1013") ;
        _msgs->Insert("file %s was created with SaveRestore version %x on a 64 bit machine which is no longer supported", "VBSR-1014") ;
        _msgs->Insert("file %s contains a long integer value which is larger than what a long integer on current machine can represent ; cannot restore", "VBSR-1015") ;
    }

    return (const char*)_msgs->GetValue(msg) ;
}

/*------------------------------------------------*/

/*static*/ void
SaveRestore::ResetMessageMap()
{
    delete _msgs ;
    _msgs = 0 ;
}

/*------------------------------------------------*/

/*static*/ void
SaveRestore::Error(const char *format, ...)
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_ERROR, GetMessageId(format), 0, format, args) ;
    va_end(args) ;
}

/*------------------------------------------------*/

/*static*/ void
SaveRestore::Warning(const char *format, ...)
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_WARNING, GetMessageId(format), 0, format, args) ;
    va_end(args) ;
}

/*------------------------------------------------*/

/*static*/ void
SaveRestore::Info(const char *format, ...)
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_INFO, GetMessageId(format), 0, format, args) ;
    va_end(args) ;
}

/*------------------------------------------------*/

// Returns a new file path name by taking directory part from 'path' and 'name' as name.
// If 'include_ext' is set, it takes the extension of the file from 'path'.
/* static */ char *
SaveRestore::GetFilePathName(const char *path, const char *name, unsigned include_ext)
{
    if (!name) return 0 ;
    if (!path) return Strings::save(name) ;

    // Get the directory from the 'path' into 'file_dir':
    char *file_dir = Strings::save(path) ;
    char *s = file_dir ;
    char *last_slash = 0 ; // Last position of '/' for directory
    char *last_dot = 0 ; // Last position of '.' for extension
    while (s && (*s != '\0')) {
        if (*s == '/') {
            last_slash = s ;
            last_dot = 0 ; // extension became invalid
        } else if (*s == '.') {
            last_dot = s ;
        }
        s++ ;
    }
    if (last_slash) *last_slash = '\0' ; // Terminate the path

    // Prepare the file name with the extension, if required:
    char *file_path_name = Strings::save(file_dir, "/", name, (include_ext) ? last_dot : 0) ;
    Strings::free(file_dir) ;

    return file_path_name ; // Caller is responsible to free the allocated string using Strings::free.
}

/*------------------------------------------------*/

// Restore the file name mapping from file which stores the mapping.
/* static */ void
SaveRestore::RestoreMapping(const char *from_file, Map &long2short_filename, Map &short2long_filename)
{
    // Check whether the mapping file exist, nothing to do if it does not exist:
    if (!FileExists(from_file, 0 /* do not process mapping again */)) return ;

    // Now restore the file via SaveRestore class itself:
    SaveRestore map_restore(from_file, 1 /* restore */, 0 /* do not process mapping again */) ;
    if (!map_restore.IsOK()) return ; // Something went wrong

    // Verify that file contains a mapping info
    if ((unsigned)map_restore.RestoreInteger() != MAP_FILE_SIGNATURE) {
        Error("%s does not contain a known mapping file format", map_restore.FileName()) ;
        return ;
    }

    // Restore mapping file version number
    unsigned ver_num = (unsigned) map_restore.RestoreInteger() ;
    if (ver_num != MAP_FILE_VER_NUM) {
        // Should not happen, this is for future when we advance the version number.
        //Message::Error(0, map_restore.FileName(), " was created using an incompatible version_number") ;
        //Message::Error(0, "backward compatibility is not supported yet") ;
        return ;
    }

    // Override SaveRestore version with VERI binary save/restore version.
    // This is needed for backward compatibility.
    map_restore.SetVersion(ver_num) ;

    // Now restore the number of entries in the file:
    unsigned num_items = (unsigned) map_restore.RestoreInteger() ;
    if (!num_items) return ; // Nothing to restore

    char *original_file ;
    char *mapped_file ;
    for (unsigned i=0; i<num_items; i++) {
        // Restore the original & the mapped file names:
        original_file = map_restore.RestoreString() ;
        mapped_file = map_restore.RestoreString() ;

        // Insert the name of the two files into the two way maps:
        (void) long2short_filename.Insert(original_file, mapped_file) ;
        (void) short2long_filename.Insert(mapped_file, original_file) ;
    }
}

/*------------------------------------------------*/

// Save the file name mapping to file which stores the mapping.
/* static */ void
SaveRestore::SaveMapping(const char *to_file, const Map &long2short_filename, const Map & /* short2long_filename */)
{
    // First check whether we need to save something or not:
    if (!to_file || !long2short_filename.Size()) return ;

    // Now save the file via SaveRestore class itself:
    SaveRestore map_save(to_file, 0 /* save */, 0 /* do not process mapping again */) ;
    if (!map_save.IsOK()) return ; // Something went wrong

    // Dump the mapping file identification:
    map_save.SaveInteger(MAP_FILE_SIGNATURE) ;

    // Dump the mapping file version number:
    map_save.SaveInteger(MAP_FILE_VER_NUM) ;

    // Dump the number of items we are going to save:
    map_save.SaveInteger((int)long2short_filename.Size()) ;

    MapIter mi ;
    char *long_name ;
    char *short_name ;
    FOREACH_MAP_ITEM(&long2short_filename, mi, &long_name, &short_name) {
        // Save both the long and short file names:
        map_save.SaveString(long_name) ;
        map_save.SaveString(short_name) ;
    }
}

/*------------------------------------------------*/

// Returns the path name of the file mapped against 'file_name'.
// In save mode it creates the mapping if it can't open the file.
/* static */ char *
SaveRestore::GetMappedFile(const char *file_name, const char *map_file_name, unsigned restore_mode)
{
    if (!file_name) return 0 ; // Can't do anything without this file name

    // Get the path name of the mapping file we need to look into for mapping info:
    char *mapping_file = GetFilePathName(file_name, map_file_name, 0 /* don't use extension */) ;
    if (!mapping_file) return 0 ;

    // First restore the mapping from the physical map file into in-memory Maps:
    Map long2short_filename(STRING_HASH) ;
    Map short2long_filename(STRING_HASH) ;
    RestoreMapping(mapping_file, long2short_filename, short2long_filename) ;

    // Check whether the file is already mapped or not:
    char *mapped_file = (char *)long2short_filename.GetValue(file_name) ;

    // If we are restoring we only need to find the mapping, no need to create it:
    if (restore_mode || mapped_file) {
        // Save the mapped file name, the original will be cleaned up later:
        mapped_file = Strings::save(mapped_file) ;

        // Need to clean up the two maps here:
        MapIter mi ;
        char *long_name ;
        char *short_name ;
        FOREACH_MAP_ITEM(&long2short_filename, mi, &long_name, &short_name) {
            Strings::free(long_name) ;
            Strings::free(short_name) ;
        }
        Strings::free(mapping_file) ; // Free the name of the mapping file as well

        return mapped_file ; // Caller is responsible to free this allocated string using Strings::free
    }

    unsigned need_to_map_this_file = 0 ;
    // Check whether the given file is already exists as a mapped file for another file:
    if (short2long_filename.GetValue(file_name)) need_to_map_this_file = 1 ;

    FILE *fp = 0 ;
    if (!need_to_map_this_file) {
        // Check whether we need to map this file because of the fact that the system
        // is unable to open the file (possibly because of long file name):

        // If file already exists, no need to map it, it can be created
        if (!FileExists(file_name, 0 /* do not process mapping again */)) {
            // Try to open the file and check whether we could open it or not:
            fp = ::fopen(file_name, "wb") ;
            if (fp) {
                // File can be created, no need to map it:
                ::fclose(fp) ;
            } else {
                // So, we may need to map the file, set the flag:
                need_to_map_this_file = 1 ;
            }
        }
    }

    if (need_to_map_this_file) {
        // Use string-hash to create the mapping:
        unsigned long hash = Hash::StringCompare(file_name, 0) ;

        // Create the file name with the same extension as the original filename:
        char buf[32] = "" ;
        sprintf(buf, "%lu", hash) ;
        mapped_file = GetFilePathName(file_name, buf, 1 /* include extension */) ;
        // Check whether it is in turn already a mapped file or not:
        while (short2long_filename.GetValue(mapped_file)) {
            // Re-create the mapping by the next number of the has :-(
            sprintf(buf, "%lu", ++hash) ;
            Strings::free(mapped_file) ;
            mapped_file = GetFilePathName(file_name, buf, 1 /* include extension */) ;
        }

        // Try to open the file to check the mapping actually helps:
        fp = ::fopen(mapped_file, "a+b") ; // Open in append mode so that we don't truncate it
        if (fp) {
            // Could open the file, so close and map it:
            ::fclose(fp) ;

            // Insert the mapping into the map for future reference:
            long2short_filename.Insert(Strings::save(file_name), mapped_file) ;
            // Save the mapped file name, the original will be cleaned up later:
            mapped_file = Strings::save(mapped_file) ;

            // Save the modified mapping of files:
            SaveMapping(mapping_file, long2short_filename, short2long_filename) ;
        } else {
            // Mapping didn't actually help, issue is something else?
            Strings::free(mapped_file) ;
            mapped_file = 0 ;
        }
    }

    // Need to clean up the two maps here:
    MapIter mi ;
    char *long_name ;
    char *short_name ;
    FOREACH_MAP_ITEM(&long2short_filename, mi, &long_name, &short_name) {
        Strings::free(long_name) ;
        Strings::free(short_name) ;
    }
    Strings::free(mapping_file) ; // Free the name of the mapping file as well

    return mapped_file ; // Caller is responsible to free this allocated string using Strings::free
}

/*------------------------------------------------*/

// Sets the name (only name, no directory part) of the map file:
/* static */ void
SaveRestore::SetMapFileName(const char *name)
{
    if (!name) return ;

    // Find last the directory separator:
    const char *last_slash = name ;
    const char *s = name ;
    while (*s != '\0') {
        if (*s == '/') last_slash = s+1 ;
        s++ ;
    }

    if (last_slash && *last_slash) {
        // Set the map file name with the name only:
        Strings::free(_map_file_name) ;
        _map_file_name = Strings::save(last_slash) ;
    }
}

/*------------------------------------------------*/

// VIPER #3498: Problem with escape identifiers '\' (and case sesitivity) in save-restore:
// Convert the design name to a name which will be unique even in case insensitive filesystems.
/* static */ char *
SaveRestore::GetDumpFileName(const char *design_name, const char *extension)
{
    if (!design_name) return 0 ;
    //if (!extension) extension = ".vdb" ; // CHECKME: Use 'vdb' as default extension?

    // Get the length of the name of the design:
    unsigned name_len = Strings::len(design_name) ;

    // Allocate enough space for the dump file name. Every character in the name
    // can be replaced by 4 characters (@ and 3 digit ASCII value of the character).
    // Also keep enough space for the extension specified to the routine:
    char *dump_file_name = Strings::allocate((name_len * 4) + Strings::len(extension) + 2) ;

    char *destination = dump_file_name ;
    for (unsigned idx = 0; idx < name_len; idx++) {
        // Viper #8270 : make 'c' unsigned char
        char c = design_name[idx] ;
        if (isupper(c) || (c == '@')) {
            // Upper case characters: @ followed by the corresponding lowercase character:
            *destination++ = '@' ;
            *destination++ = (char)tolower(c) ;
        } else if (isalnum(c)) { // Upper case is already checked above
            // Digits lower case characters: go as it is:
            *destination++ = c ;
        } else {
            switch (c) {
                case '!':
                case '$':
                case '%':
                case '&':
                case '\'':
                case '(':
                case ')':
                case '+':
                case ',':
                case '-':
                case '.':
                case ';':
                case '=':
                case '[':
                case ']':
                case '^':
                case '_':
                case '`':
                case '{':
                case '}':
                case '~':
                    // These characters go without modification too:
                    *destination++ = c ; break ;
                default:
                    // All other charcters: @ followed by the ASCII value of the character:
                    *destination++ = '@' ;
                    char ascii_str[8] = "" ;
                    sprintf(ascii_str, "%03o", (unsigned char)c) ; // Print the octal value of the character c
                    char *pos = ascii_str ;
                    while (pos && (*pos)) *destination++ = *pos++ ; // Copy it over
                    break ;
            }
        }
    }

    *destination = '\0' ; // Close the dump file name

    if (extension) {
        // If the extension does not contain the separating dot, insert it here:
        if (*extension != '.') *destination++ = '.' ;
        strcpy(destination, extension) ;
    }

    return dump_file_name ; // Caller is responsible to free this with Strings::free()
}

/*------------------------------------------------*/

/* static */ char *
SaveRestore::GetDesignName(const char *dump_file_name)
{
    if (!dump_file_name) return 0 ;

    // Get the length of the name of the design minus the suffix:
    unsigned name_len = Strings::len(dump_file_name) ;

    // Need to strip off the suffix characters from this length:
    unsigned i ;
    for (i=name_len-1; i>0; i--) {
        if (dump_file_name[i] == '.') break ; // Got the '.' separator for extension
    }
    if (i) name_len = i ; // name_len, now, is the size of the dump file minus the extension

    // Allocate enough space for the design name which should be greater than or equal to the dump file name:
    char *design_name = Strings::allocate(name_len + 1) ;

    char *destination = design_name ;
    for (unsigned idx = 0; idx < name_len; idx++) {
        char c = dump_file_name[idx] ;
        char lookahead = dump_file_name[idx+1] ;
        if (c == '@') {
            // If there are two '@' chars then replace them by one '@'
            if (lookahead=='@') *destination++ = '@' ;
            // If the first '@' character is followed by a letter then convert it to uppercase
            if (isalpha(lookahead))*destination++ = (char)toupper(lookahead) ;

            // If the '@' character is followed by a 3 character octal value then
            if (isdigit(lookahead)) {
                int num = 0 ;
                sscanf(dump_file_name+idx+1, "%3o", &num) ;
                *destination++ = (char)num ;
                idx = idx + 2 ;
            }
            idx ++ ; // Skip the next character
        } else if (isalnum(c)) { // Upper case is already checked above
            // Digits lower case characters: go as it is:
            *destination++ = c ;
        } else {
            switch (c) {
                case '!':
                case '$':
                case '%':
                case '&':
                case '\'':
                case '(':
                case ')':
                case '+':
                case ',':
                case '-':
                case '.':
                case ';':
                case '=':
                case '[':
                case ']':
                case '^':
                case '_':
                case '`':
                case '{':
                case '}':
                case '~':
                    // These characters go as it is too:
                    *destination++ = c ; break ;
                default:
                    break ;
            }
        }
    }
    *destination = '\0' ;

    return design_name ; // Caller is responsible to free this with Strings::free()
}

/*------------------------------------------------*/

// VIPER #6358: Get the directory into which the _file_name resides:
char *
SaveRestore::GetDumpDirectory() const
{
    return FileSystem::DirectoryPath(_file_name) ;
}

/*------------------------------------------------*/

void
SaveRestore::Reset()
{
    // reset save/restore stream to initial state, without closing the file.
    // Used when 'multiple' separate objects (parse trees or so) should go into a single file.
    _pointer2num.Reset() ;      // Keeps track of pointer -> number assoc during save or restore
    _num2pointer.Reset() ;      // Keeps track of number -> pointer assoc during save or restore
    _filename2id.Reset() ;      // During save : keeps track of files associated with saved linefile objects.
    _id2filename.Reset() ;      // During Restore : find filename back from old fileid.
    _pointer_number = 0 ;       // Number used for pointer identification while saving. Increments with each RegisterPointer call.
    // reset isok flag ? Mmmm. Probably not. Once file is not OK, we are essentially lost in it. Cannot then simply pick up where we left it.
}

long
SaveRestore::GetPos()
{
    // return the current character file position
    if (!IsOK()) return 0 ;

#ifdef USE_BUFFERED_READ_WRITE
    // First flush the buffer (for save mode):
    (void) FlushBuffer() ;
#endif

    // Get current position using ftell() from the _file FILE object.
    long pos = ftell(_file) ;

#ifdef USE_BUFFERED_READ_WRITE
    if (_restore) {
        // Adjustment for the already read buffer (restore mode):
        pos = (pos - (long)_buffered_data_size) + (long)_buffer_index ;
    }
#endif

    return pos ;
}

void
SaveRestore::SetPos(long pos)
{
    if (!IsOK()) return ; // do not set position upon error

#ifdef USE_BUFFERED_READ_WRITE
    // First flush the buffer (for save mode):
    (void) FlushBuffer() ;
#endif

    // set the character file position (w.r.t. start of file)
    (void) fseek(_file, pos, SEEK_SET) ;

#ifdef USE_BUFFERED_READ_WRITE
    if (_restore) {
        // Clear out the already read buffer (restore mode):
        _buffered_data_size = 0 ;
        _buffer_index = 0 ;
    }
#endif
}

/*------------------------------------------------*/

/* static */
unsigned
SaveRestore::FileExists(const char *filename, unsigned consider_file_mapping /* = 1 */)
{
    if (!filename) return 0 ;

    // Check whether the file exists and is a regular file:
    if (FileSystem::IsFile(filename)) return 1 ;

    // Here either file does not exist or is not a regular file, check via mapping:
    if (!consider_file_mapping) return 0 ; // Mapping need not be checked

    // VIPER #3541 & #3790: Long, very long file name support:
    const char *map_file_name = GetMapFileName() ; // Get the user specified map file name
    // If mapping file name was not specified, use the default map file name:
    if (!map_file_name) map_file_name = DEFAULT_MAP_FILE_NAME ;
    // Now check whether this file is/should be mapped with another file:
    char *mapped_file = GetMappedFile(filename, map_file_name, 1 /* restore mode: don't create mapping */) ;

    // Use the mapped file to check whether it exists:
    unsigned exists = FileExists(mapped_file, 0 /* do not process mapping again */) ;
    Strings::free(mapped_file) ;

    return exists ;
}

/*------------------------------------------------*/

void
SaveRestore::FileNotOk()
{
#ifdef USE_BUFFERED_READ_WRITE
    // Flush the waiting to be written buffer, if any (for save mode):
    (void) FlushBuffer() ;
#endif

    // Specify that file contains an error, such that when the destructor
    // Gets called, it won't assert on the signature check.
    _systems_go = 0 ;
}

/*------------------------------------------------*/

unsigned
SaveRestore::IsRegistered(const void *pointer) const
{
    if (!IsOK()) { return 0; }

    return (_pointer2num.GetItem(pointer)) ? 1 : 0 ;
}

/*------------------------------------------------*/

void
SaveRestore::RegisterPointer(const void *pointer)
{
    if (!IsOK() || !pointer) { return; }

    unsigned is_new ;
    if (_restore) {
        // Restore : Insert the present pointer against the number into the table
        _pointer_number++ ;

        // Insert the pointer in the table
        is_new = _num2pointer.Insert((void*)_pointer_number, pointer) ;
        if (!is_new) { FileNotOk() ; return ; } // It should not exist yet

        // Also insert the other way (table used for ReRegister and IsRegistered() calls during 'restore').
        is_new = _pointer2num.Insert(pointer, (void*)_pointer_number) ;
        if (!is_new) { FileNotOk() ; return ; }
    } else {
        // Save : Insert the present pointer against the number into the table
        _pointer_number++ ;

        // Register the pointer against this number.
        is_new = _pointer2num.Insert(pointer, (void*)_pointer_number) ;
        if (!is_new) { FileNotOk() ; return ; } // It should not have been registered before.
    }
}

/*------------------------------------------------*/

void
SaveRestore::ReRegister(const void *old_pointer, const void *new_pointer)
{
    // Used during restore, after a RegisterPointer() call.
    // 'old_pointer' is already set, under some 'number'.
    // Now, we want to use 'new_pointer' instead of the old one,
    // for any later references to the old number.
    if (!IsOK() || !old_pointer || !new_pointer) { return; }

    // Get the registration number from the old pointer.
    void *num = _pointer2num.GetValue(old_pointer) ;

    // In pointer2num, remove the old entry, and enter the new one.
    unsigned testflag ;
    testflag = _pointer2num.Remove(old_pointer) ;
    if (!testflag) { FileNotOk() ; return ; } // old_pointer was not registered before. Cannot ReRegister.

    // Insert the new one
    testflag = _pointer2num.Insert(new_pointer, num) ;
    if (!testflag) { FileNotOk() ; return ; } // It should not already be existing

    // In num2pointer, overwrite the old entry under 'num'  ; force overwrite.
    testflag = _num2pointer.Insert(num, new_pointer, 1) ;
    if (testflag)  { FileNotOk() ; return ; } // It should have been existing...
}

/*------------------------------------------------*/

void
SaveRestore::UnRegister(const void *pointer)
{
    if (!IsOK() || !pointer) return;

    // Remove the pointer from the registry
    // Do not complain if it was not there. Should be harmless.

    // remove the entry from num2pointer, by picking up the 'num' from pointer2num (if there)
    void *num = _pointer2num.GetValue(pointer) ;
    (void) _num2pointer.Remove(num) ;

    // Remove pointer->num entry from the table :
    (void) _pointer2num.Remove(pointer) ;
}

/*------------------------------------------------*/

unsigned long
SaveRestore::Pointer2Id(const void *pointer) const
{
    if (!IsOK()) { return 0; }

    // Note: this routine only works during 'save'. Assert on that :
    VERIFIC_ASSERT(!_restore) ;

    // Returns id of a registered pointer during 'save'. Or 0 if not registered.
    return (unsigned long) _pointer2num.GetValue(pointer) ;
}

/*------------------------------------------------*/

void *
SaveRestore::Id2Pointer(unsigned long id) const
{
    if (!IsOK()) { return 0; }

    // Note: this routine only works during 'restore'. Assert on that :
    VERIFIC_ASSERT(_restore) ;

    // Returns pointer of a registered 'id' during 'restore'. Or 0 if not registered.
    return (void*) _num2pointer.GetValue((void*)id) ;
}

/*------------------------------------------------*/

void
SaveRestore::EndianShuffle(int &integer) const
{
    // Big Endian <-> Little Endian conversion

    if (sizeof(int) != 4) return ; // Refuse to shuffle if this doesn't hold
    integer = ((integer<<24) & (0xff000000))|
              ((integer<<8)  & (0x00ff0000))|
              ((integer>>8)  & (0x0000ff00))|
              ((integer>>24) & (0x000000ff)) ;
}

/*------------------------------------------------*/

void
SaveRestore::EndianShuffle(long &integer) const
{
    // Big Endian <-> Little Endian conversion

    // Because size(long) is different on 32bit vs. 64bit machines,
    // we need to have the following check.

#if (ULONG_MAX & 0xffffffff00000000UL)
    // 64bit longs
    integer = ((integer<<56) & (0xff00000000000000))|
              ((integer<<40) & (0x00ff000000000000))|
              ((integer<<24) & (0x0000ff0000000000))|
              ((integer<<8)  & (0x000000ff00000000))|
              ((integer>>8)  & (0x00000000ff000000))|
              ((integer>>24) & (0x0000000000ff0000))|
              ((integer>>40) & (0x000000000000ff00))|
              ((integer>>56) & (0x00000000000000ff)) ;
#else
    // 32bit longs
    integer = ((integer<<24) & (0xff000000))|
              ((integer<<8)  & (0x00ff0000))|
              ((integer>>8)  & (0x0000ff00))|
              ((integer>>24) & (0x000000ff)) ;
#endif
}

/*------------------------------------------------*/

void
SaveRestore::EndianShuffle64(verific_int64 &integer) const
{
    // Big Endian <-> Little Endian conversion

    // Refuse to shuffle if this doesn't hold
    if (sizeof(verific_int64) != 8) return ;
    integer =
#ifdef WINDOWS
              // Windows does not allow LL suffix for literals:
              ((integer<<56) & (0xff00000000000000L))|
              ((integer<<40) & (0x00ff000000000000L))|
              ((integer<<24) & (0x0000ff0000000000L))|
              ((integer<<8)  & (0x000000ff00000000L))|
              ((integer>>8)  & (0x00000000ff000000L))|
              ((integer>>24) & (0x0000000000ff0000L))|
              ((integer>>40) & (0x000000000000ff00L))|
              ((integer>>56) & (0x00000000000000ffL)) ;
#else
              // For others, use LL suffix for these literals:
              ((integer<<56) & (0xff00000000000000LL))|
              ((integer<<40) & (0x00ff000000000000LL))|
              ((integer<<24) & (0x0000ff0000000000LL))|
              ((integer<<8)  & (0x000000ff00000000LL))|
              ((integer>>8)  & (0x00000000ff000000LL))|
              ((integer>>24) & (0x0000000000ff0000LL))|
              ((integer>>40) & (0x000000000000ff00LL))|
              ((integer>>56) & (0x00000000000000ffLL)) ;
#endif
}

/* ---------------------------------------------- */
/*              Save Methods                      */
/* ---------------------------------------------- */

void
SaveRestore::SaveString(const char *string)
{
    if (!IsOK()) { return; }

    // First save the length of the string
    // Save a 0 char pointer as integer -1
    int len = (string) ? (int)Strings::len(string) : -1 ;
    SaveInteger(len) ;

    if (_encrypt) {
        // data member _buffer for string encryption
        if (!_buffer) _buffer = Strings::allocate(_buffer_size) ;

        // Is _buffer large enough to contain string?
        if (len >= (int)_buffer_size) {
            // Grow _buffer
            while (len >= (int)_buffer_size) _buffer_size *= 2 ;
            Strings::free(_buffer) ;
            _buffer = Strings::allocate(_buffer_size) ;
        }

        if (len > 0) {
            // Now save the data
            _buffer[len] = '\0' ;
            // Based on len, use particular offset (Must be same values as in SaveRestore::RestoreString)
            char offset = (len & 0x1) ? (-33) : (+33) ;
            for (int i = 0 ; i < len ; i++) {
                _buffer[(len - 1) - i] = (((string[i] + offset) & 0xAA) >> 1) | // Swap bits
                                        (((string[i] + offset) & 0x55) << 1) ;
            }
            (void) BufferedWrite(_buffer, (size_t)len) ;
        }
    } else {
        // Now save the data
        if (len>0) {
            (void) BufferedWrite(string, (size_t)len) ;
        }
    }
}

/*------------------------------------------------*/

void
SaveRestore::SaveChar(char c)
{
    if (!IsOK()) { return; }

    // Push out one (1 byte) char
    (void) BufferedWrite((void*)&c, sizeof(char)) ;
}

/*------------------------------------------------*/

void
SaveRestore::SaveInteger(int integer, unsigned skip_buffer)
{
    if (!IsOK()) { return; }

    // How many bytes are going to be saved
    unsigned num_bytes = sizeof(int) ;

    // Version 0xA0000007 adjustments :
    if (!skip_buffer) { // Only apply if 'skip_buffer' is not set.
        // First byte : use two MSB bits as encoding for what's coming up :
        //   00 | 11 : This is a small integer that fits in the remaining 6 bits
        //   01 : There are 4 bytes (32 bits) coming up that contain the number.
        //   10 : There are 8 bytes (64 bits) coming up that contain the number (this is currently not used).
        int t1 = (integer & ~(0x3F)) ; // small integers (0-63 fit in a 6 bits.
        int t2 = ~(integer | 0x3F) ; // small integeres (-1 - -64 also fit in 6 bits.
        if (t1==0 || t2==0) {
            // small integer fits in 6 bits. Put out as single character :
            char num = integer ; // VIPER 6318 : make sure to write a char to avoid endian-ness problem
            (void) BufferedWrite((void*)&num, 1, skip_buffer) ;
            return ; // done
        } else {
            // Put out number to indicate how many bytes will be saved. Store that in MSB 4 bits :
            char num = 0x40 ; // 0100 0000 (left bits (01) indicats a 32 bit number is coming up
            (void) BufferedWrite((void*)&num, 1, skip_buffer) ;
        }
    }

    // Push out one (4 byte) integer
    (void) BufferedWrite((void*)&integer, num_bytes, skip_buffer) ;
}

/*------------------------------------------------*/

void
SaveRestore::SaveLong(long integer)
{
    if (!IsOK()) { return; }

    // Push out one long integer
    // Note that the 'long integer' is 8 bytes on a 64 bit architecture!
    (void) BufferedWrite((void*)&integer, sizeof(long)) ;
}

/*------------------------------------------------*/

void
SaveRestore::SaveDouble(double dDouble/*floating_point*/)
{
    if (!IsOK()) { return; }

    // Save a 'double' as a formatted string, to stay platform independent
    char buf[30];
    sprintf(buf, "%#.15G", dDouble);
    SaveString(buf);
}

/*------------------------------------------------*/

void
SaveRestore::SaveVerific64(verific_int64 integer /* 64 bit integer */)
{
    if (!IsOK()) { return; }

    // Push out a 64 bit integer
    (void) BufferedWrite((void*)&integer, sizeof(verific_int64)) ;
}

/*------------------------------------------------*/

void
SaveRestore::SavePointer(const void *pointer)
{
    if (!IsOK()) { return; }

    // Save a REFERENCE TO a pointer.
    // We save the number that is registered against this pointer.
    long pointer_num = (long)_pointer2num.GetValue(pointer) ;
    if (!pointer_num) { FileNotOk() ; return ; } // It must have been registered before.
    SaveLong(pointer_num) ;
}

/*------------------------------------------------*/

void
SaveRestore::SaveLinefile(linefile_type linefile)
{
    if (!IsOK()) { return; }

    if (linefile) {
#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
        // Check whether this linefile 'object' is already registered or not:
        if (!IsRegistered((const void *)linefile)) {
            // Its not registered, register it:
            RegisterPointer((const void *)linefile) ;

            // Save the object, first save an integer (as long) to indicate that we are saving the object:
            SaveLong(LINEFILE_OBJECT) ;
            // Get the filename from the linefile object:
            const char *filename = LineFile::GetFileName(linefile) ;
            // First check/register the file associated with this linefile object
            if (filename && _filename2id.Insert(filename, 0 /*fileid not interesting*/)) {
                // It was never saved yet. Push out this filename.
                // First push out the flag that the filename is coming up :
                SaveInteger(LINEFILE_FILENAME) ;
                SaveString(filename) ;
            }
            // Now save the linefile/column information
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
            SaveInteger((int)linefile->GetLeftCol()) ;
            SaveInteger((int)linefile->GetRightCol()) ;
            SaveInteger((int)linefile->GetLeftLine()) ;
            SaveInteger((int)linefile->GetRightLine()) ;
            SaveInteger((int)linefile->GetFileId()) ;
#else
            SaveInteger((int)LineFile::GetLineNo(linefile)) ;
            SaveInteger((int)LineFile::GetFileId(linefile)) ;
#endif
        } else {
            // Its registered, just get the registered id for this object:
            unsigned long id = Pointer2Id((const void*)linefile) ;
            // And save the id here:
            SaveLong(id) ;
        }
#else
        // Get the file name from this scalar linefile:
        const char *filename = LineFile::GetFileName(linefile) ;

        // First check/register the file associated with this linefile:
        if (filename && _filename2id.Insert(filename, 0 /*fileid not interesting*/)) {
            // It was not saved yet. Push out this filename.
            // First push out the flag that the filename is coming up:
            SaveLong(LINEFILE_FILENAME) ;
            SaveString(filename) ;
        }

        // Save the linefile as a number when we are not storing column info:
        SaveLong((long)linefile) ;
#endif
    } else {
        // Null linefile always gets saved as integer zero:
        SaveLong(0) ;
    }
}

/*------------------------------------------------*/

unsigned
SaveRestore::IsSaved(const void *pointer) const
{
    if (!IsOK()) { return 0; }

    return (_pointer2num.GetItem(pointer)) ? 1 : 0 ;
}

/* ---------------------------------------------- */
/*              Restore Methods                   */
/* ---------------------------------------------- */

char *
SaveRestore::RestoreString()
{
    if (!IsOK()) { return 0; }

    // First get the length of the string
    int len = RestoreInteger() ;
    if (len == -1) {
        return 0 ; // It was a 0 pointer
    }
    // At this point if len is negative, then there's a problem!
    if (len < 0) { FileNotOk() ; return 0 ; }

    char *result ;

    if (_encrypt) {
        // JJ: 130110 _buffer for string encryption
        if (!_buffer) _buffer = Strings::allocate(_buffer_size) ;

        // Is _buffer large enough to contain string?
        if ((unsigned)len >= _buffer_size) {
            // Grow _buffer
            while ((unsigned)len >= _buffer_size) _buffer_size *= 2 ;
            Strings::free(_buffer) ;
            _buffer = Strings::allocate(_buffer_size) ;
        }

        if (len > 0) {
            // Now restore the data
            (void) BufferedRead(_buffer, (size_t)len) ;
        }

        // Now un-encrypt _buffer into 'result' string
        result = Strings::allocate((unsigned)len+1) ; // Create enough space to store the string and the \0 character
        char offset = (len & 0x1) ? (+33) : (-33) ;
        for (int i = 0 ; i < len ; i++) {
            result[(len - 1) - i] = (((_buffer[i] & 0xAA) >> 1) | ((_buffer[i] & 0x55) << 1)) + offset ;
        }
    } else {
        // Create enough space to store the string and the \0 character
        result = Strings::allocate((unsigned)len+1) ;
        if (len > 0) {
            // restore the data
            (void) BufferedRead(result, (size_t)len) ;
        }
    }

    // append the \0 character
    result[len] = '\0' ;

    return result ;
}

/*------------------------------------------------*/

char
SaveRestore::RestoreChar()
{
    if (!IsOK()) { return 0; }

    // pop 1 byte off the stack and into the result
    char result ;
    (void) BufferedRead((void *)&result, sizeof(char)) ;

    return result ;
}

/*------------------------------------------------*/

int
SaveRestore::RestoreInteger(unsigned skip_buffer)
{
    if (!IsOK()) { return 0; }

#if 0 // RD: 12/2010: removed support for versions before 0xA0000004
    // Backward-compatibility support for version < 0xA0000004.
#if (ULONG_MAX & 0xffffffff00000000UL)
    // This is only of concern if we're running on a 64bit compiler.
    // This is needed because before SaveInteger() used to save a 'long', not an 'int'.
    if (_restore_version && (_restore_version < 0xA0000004) && (_sizeof_long > sizeof(int))) {
        // This means that SaveInteger() actually saved a long on a 64bit machine.
        // In this case, we need to read the sizeof(long) bytes.
        long result ;
        (void) BufferedRead((void *)&result, sizeof(long), skip_buffer) ;

        if (_endian_shuffle) EndianShuffle(result) ;

        return (int)result ; // Now truncate
    }
#endif
#endif

    // Pop sizeof(int) bytes off the stack and into the result
    int result ;
    if (skip_buffer || (_restore_version && (_restore_version < 0xA0000007))) {
        // expect sizeof(int) bytes, into an 'int'.
        (void) BufferedRead((void *)&result, sizeof(int), skip_buffer) ;

        if (_endian_shuffle) EndianShuffle(result) ;
    } else {
        // Version 0xA0000007 adjustments :
        // First expect only one character :
        char c ;
        (void) BufferedRead((void *)&c, 1, skip_buffer) ;

        // Check the two left-most bits in this character :
        switch (c & 0xC0) {
        case 0x00 : // 00......
        case 0xC0 : // 11......
            // here, c contains the integer value of the result (signed) as is :
            result = c ;
            break ;
        default :
            unsigned num_bytes = (c>>4) & 0xF ; // number of bytes saved (4 or 8).
            // restore 32 bits :
            (void) BufferedRead((void *)&result, num_bytes, skip_buffer) ;

            // FIX ME : endian shuffle fails when restoring 32 bit into 64 bit.
            if (_endian_shuffle) EndianShuffle(result) ;
        }
    }

    return result ;
}

/*------------------------------------------------*/

long
SaveRestore::RestoreLong()
{
    if (!IsOK()) { return 0; }

    // Pop _sizeof_long (as it was saved) bytes off the stack and into the result
    long result = 0 ;
    if (_sizeof_long == sizeof(long)) {
        (void) BufferedRead((void *)&result, _sizeof_long) ;
        if (_endian_shuffle) EndianShuffle(result) ;
    } else if (_sizeof_long == sizeof(verific_int64)) {
        // This is a 64bit long being restored on a 32bit system.  Check to see if value fits within 32bits.
        verific_int64 tmp ;
        (void) BufferedRead((void *)&tmp, _sizeof_long) ;
        if (_endian_shuffle) EndianShuffle64(tmp) ;
        /* coverity[constant_expression_result] */
        if (tmp <= LONG_MAX) {
            // Value fits in 32bits
            result = (long)tmp ;
        } else {
            Error("file %s contains a long integer value which is larger than what a long integer on current machine can represent ; cannot restore", _file_name) ;
            FileNotOk() ;
            return 0 ;
        }
    } else {
        // Since _sizeof_long doesn't equal sizeof(long) of current machine, restore as int.
        // We need to do this or else placement of data on different endian machine will be wrong.
        int tmp = 0 ;
        (void) BufferedRead((void *)&tmp, _sizeof_long) ;
        if (_endian_shuffle) EndianShuffle(tmp) ;
        result = (long)(unsigned)tmp ; // We need to cast to unsigned before casting to long!!!
    }

    return result ;
}

/*------------------------------------------------*/

double
SaveRestore::RestoreDouble()
{
    if (!IsOK()) { return 0.0; }

    // Restore the formatted string, and translate to a double
    char *str = RestoreString();
    double val = (str) ? Strings::atof(str) : 0 ;
    Strings::free(str);

    return val;
}

/*------------------------------------------------*/

verific_int64
SaveRestore::RestoreVerific64()
{
    if (!IsOK()) { return 0; }

    // Pop 8 bytes off the stack and into the result
    verific_int64 result ;
    (void) BufferedRead((void *)&result, sizeof(verific_int64)) ;

    if (_endian_shuffle) EndianShuffle64(result) ;

    return result ;
}

/*------------------------------------------------*/

void *
SaveRestore::RestorePointer()
{
    if (!IsOK()) { return 0; }

    long pointer_num = RestoreLong() ;
    if (!pointer_num) return 0 ; // It's a 0 pointer ;

    // Get the pointer from the table, by number
    void *pointer = _num2pointer.GetValue((void*)pointer_num) ;
    if (!pointer) { FileNotOk() ; return 0 ; } // It must have been registered earlier
    return pointer ;
}

/*------------------------------------------------*/

linefile_type
SaveRestore::RestoreLinefile()
{
    if (!IsOK()) { return 0; }

    // Get the number from the stream :
    linefile_type result = 0 ;
    unsigned new_file_id ;
    unsigned register_old_fileid = 0 ;
    const char *filename = 0 ;
    
    // Restore the first (long) integer for this linefile (it's a long coming up).
    // RD: Note : Older versions (before 0xA0000007) saved an 'integer' here in some cases, and sometimes a long.
    // Now, saving a 'long' always. However, RestoreLong is always 'unsigned' (another legacy problem in RestoreLong)
    // while RestoreInteger is 'signed'.
    // We need to account for that difference at least when comparing LINEFILE_OBJECT (which was defined as -1),
    // otherwise 32 bit saved files won't restore correct on 64 bit machines. That's why we cast 'flag' to an int before comparing
    //
    // FIX ME: This code (linefile restore) is getting way to messy. Need to rethink the process.

    verific_int64 flag = 0 ; // flag needs to be 64 bits to handle a 64bit restore on a 32bit platform.
    if (_sizeof_long > sizeof(long)) {
        // Restoring 64bit encoding on 32bit platform.  Handle appropriately.
        (void) BufferedRead((void *)&flag, _sizeof_long) ;
        if (_endian_shuffle) EndianShuffle64(flag) ;
    } else {
        flag = RestoreLong() ;
    }

    if (!flag && _new_linefile) {
        // Saved linefile was a null using new mechanism.
        return result ; // Return null linefile to be backward compatible
    }

    // First check, whether we need to restore an object or an (already restored) id:
    unsigned new_linefile_object = 0 ;
    if (((int)flag) == LINEFILE_OBJECT) {
       // We know that a linefile object is coming, need to restore the object itself:
       // Check whether the filename also needs to be retored:
       flag = (long int)RestoreInteger() ;
       new_linefile_object = 1 ; // We need to restore a new linefile object
    }

    // Check whether we need to register new filename
    if (((int)flag) == LINEFILE_FILENAME) {
        // Read the filename from the stream :
        char *restored_filename = RestoreString() ;
        // Register this filename in the system.
        result = LineFile::EncodeLineFile(restored_filename, 0) ;
        // Use the filename pointer from the system, so that we can free the restored string.
        filename = LineFile::GetFileName(result) ;
        Strings::free(restored_filename) ;
        register_old_fileid = 1 ;
        // Insert filename->new_file_id into map
        (void) _filename2id.Insert(filename, (void*)(unsigned long)LineFile::GetFileId(result)) ;
    }

    if (_new_linefile && (_column_active || _large_linefile) && !new_linefile_object) {
        // We are restoring a linefile saved in the new mechanism and the restored
        // flag says that it is an id of an already restored linefile object, get it:
        result = (linefile_type)Id2Pointer((unsigned)flag) ; // flag could be zero for NULL linefile
    } else {
        // Parse stream for linefile info
        unsigned left_col, right_col, left_line, right_line, old_file_id ;
        left_col = right_col = left_line = right_line = old_file_id = 0;
        if (_column_active) {
            // This stream was saved with VERIFIC_LINEFILE_INCLUDES_COLUMNS enabled

            // If result is already valid here, then you have to restore an
            // int for the left_col val, else use the already parsed value of flag
            left_col    = (result) ? (unsigned)RestoreInteger() : (unsigned)flag ;
            right_col   = (unsigned)RestoreInteger() ;
            left_line   = (unsigned)RestoreInteger() ;
            right_line  = (unsigned)RestoreInteger() ;
            old_file_id = (unsigned)RestoreInteger() ;
        } else if (_large_linefile) {
            // This stream was saved with VERIFIC_LARGE_LINEFILE enabled
            left_line  = (result) ? (unsigned)RestoreInteger() : (unsigned)flag ;
            old_file_id = (unsigned)RestoreInteger() ;
        } else {
            // If result is already valid here, then you have to restore an
            // long for the linefile val, else use the already parsed value of flag
            verific_int64 linefile = 0 ;
            if (result) {
                if (_sizeof_long > sizeof(long)) {
                    // Restoring 64bit encoding on 32bit platform.  Handle appropriately.
                    (void) BufferedRead((void *)&linefile, _sizeof_long) ;
                    if (_endian_shuffle) EndianShuffle64(linefile) ;
                } else {
                    linefile = (unsigned long)RestoreLong() ;
                }
            } else {
#if (ULONG_MAX & 0xffffffff00000000UL)
                // In versions before 0xA0000007, an inconsistency between Save/Restore (integer versus long)
                // was created on 64bit platforms, which made restore at this point very, very messy.
                // Cannot support that any more (now that we also on 32 bits saving integer is different than
                // saving long. So decided to error out at this point :
                if (_sizeof_long > sizeof(int) && _restore_version < 0xA0000007) {
                    // We have just restored a 64 bit number where a 32 bit number was saved.
                    // This messes up the 'flag' and thus cannot trust it any more.
                    Error("file %s was created with SaveRestore version %x on a 64 bit machine which is no longer supported", _file_name, _restore_version) ;
                    Info("contact support@verific.com if retrieval is essential") ;
                    FileNotOk() ;
                }
#endif
                linefile = (verific_int64)flag ;
            }

            // Get the old file id based on previous linefile constant settings
            old_file_id = (unsigned)(linefile >> _line_num_bits) ;
            // Get the old line info based on previous linefile constant settings
            // We're using left_line as its placeholder.
            left_line   = (unsigned)(linefile & (~((verific_int64)~0UL << _line_num_bits))) ;
        }

        if (register_old_fileid) {
            // And register the old file id against the filename, for next time this fileid hits.
            // Use the filename pointer as it is in the system,
            (void) _id2filename.Insert((void*)(unsigned long)old_file_id, filename) ;
        } else {
            // This is the old file id. Get the filename from it :
            filename = (const char *)_id2filename.GetValue((void*)(unsigned long)old_file_id) ;
        }

        // Get new file id
        new_file_id = (unsigned)(unsigned long)_filename2id.GetValue((void*)filename) ;

        // VIPER #3502 (backward compatibility): Check whether a valid linefile was saved or not:
        if (!left_col && !right_col && !left_line && !right_line && !new_file_id) {
            // No valid linefile was actually saved, set result to be 0:
            result = 0 ;
        } else {
            // Now set result data
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
            // Create a linefile object with NULL filename to avoid full processing again:
            if (!result) result = new ColLineFile(0, 0) ;
            if (_column_active) {
                result->SetLeftCol(left_col) ;
                result->SetRightCol(right_col) ;
                result->SetLeftLine(left_line) ;
                result->SetRightLine(right_line) ;
            } else {
                result->SetLeftLine(left_line) ;
                result->SetRightLine(left_line) ;
            }
            result->SetFileId(new_file_id) ;
#else
#ifdef VERIFIC_LARGE_LINEFILE
            // Create a linefile object with NULL filename to avoid full processing again:
            if (!result) result = new ColLineFile(0, 0) ;
            (void) LineFile::SetLineNo(result, left_line) ;
#else
            // Create a linefile object with NULL filename to avoid full processing again:
            result = LineFile::EncodeLineFile(0, left_line) ;
#endif
            result = LineFile::SetFileId(result, new_file_id) ;
#endif
        }

        // Need to register this new restored object here for others to get it from the id:
        // Only if the stream was saved in new mechanism and with column active:
        if (_new_linefile && (_column_active || _large_linefile)) {
            // Next time this structure comes up, the stream will show a pointer number (in flag) (and !new_linefile_object)
            // and we need to pick-up 'result' from that pointer number. We use the id2pointer table for that.
            // However, if we currently do not have VERIFIC_LINEFILE_INCLUDES_COLUMNS set, then 'result'
            // is not a real pointer. It is the linefile info itself (with only linenumber info).
            // Since the source stream can have multiple linefile_type objects with the same linenumber,
            // we cannot call RegisterPointer() (it would error out).
            // Instead, Register only under id2pointer, NOT under pointer2num.
#if !defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) && !defined(VERIFIC_LARGE_LINEFILE)
            // Restore : Insert the present pointer against the number into the table
            _pointer_number++ ;

            // Insert the pointer in the table
            unsigned is_new = _num2pointer.Insert((void*)_pointer_number, (const void *)result) ;
            VERIFIC_ASSERT(is_new) ; // double-check that we did not mess up the pointer numbering.
#else
            // Just register this pointer the normal way.
            RegisterPointer((const void *)result) ;
#endif
        }
    }

    return result ;
}

/*------------------------------------------------*/

size_t
SaveRestore::BufferedRead(void *store, size_t nmemb, unsigned skip_buffer)
{
    VERIFIC_ASSERT(store) ;

#ifdef USE_BUFFERED_READ_WRITE
    if (skip_buffer) {
        size_t num = ::fread(store, 1, nmemb, _file) ;
        if (num != nmemb) {
            FileNotOk() ; // flag this SaveResore object as invalid
            return 0 ;
        }
        return num ;
    }

#if 0
    if (nmemb > _read_write_buffer_size) {
        // Requested data cannot be buffered because it is bigger than the buffer size itself.
        // Use direct file read. First push the file position back to already buffered data:
        long back_off_size = (long)(_buffered_data_size - _buffer_index) ;
        (void) fseek(_file, -back_off_size /* back-off from current pos, so use -ve value */, SEEK_CUR) ;
        // Reset these so that we refill the buffer on the next call of this routine:
        _buffered_data_size = 0 ;
        _buffer_index = 0 ;
        return ::fread(store, 1, nmemb, _file) ;
    }

    VERIFIC_ASSERT(_read_write_buffer_size >= nmemb) ;

    // Check whether we have the requested data available in the buffer:
    if ((_buffer_index + nmemb) > _buffered_data_size) {
        // Available buffer is shorter than requested data. Need to buffer more.
        if (_buffer_index < _buffered_data_size) {
            // Some data are still available. First move that in the beginning of the buffer.
            // CHECKME: Use 'memcpy'? Can we ensure that they will not overlap? Use memmove for now:
            memmove(_read_write_buffer, (_read_write_buffer + _buffer_index), (_buffered_data_size - _buffer_index)) ;
            _buffer_index = _buffered_data_size - _buffer_index ; // The index from the beginning
        } else {
            _buffer_index = 0 ;
        }

        // Now, read from the file to fill the buffer:
        _buffered_data_size = _buffer_index ;
        _buffered_data_size += ::fread((_read_write_buffer + _buffer_index), 1, (_read_write_buffer_size - _buffer_index), _file) ;
        _buffer_index = 0 ; // Reset the buffer index for the read below
    }

    // Now copy the data over from buffer to the given storage pointer:
    size_t read_size = (_buffered_data_size >= nmemb) ? nmemb : _buffered_data_size ;
    // CHECKME: For char/int/long/double simple assignment might be faster than memcpy.
    memcpy(store, (_read_write_buffer + _buffer_index), read_size) ;
    _buffer_index += read_size ; // Update the index
    return read_size ;
#endif

    // For decryption or protected data, ALWAYS read through buffer
    size_t n_elements = nmemb ; // n_elements to read
    char *s = (char *)store ;
    while (n_elements) {
        if (!_buffer_index) {
            // _read_write_buffer_size is guranteed to be multiple of cipher block size
            char *buffer_copy = _cipher ? Strings::allocate(_read_write_buffer_size) : _read_write_buffer ;
            _buffered_data_size = ::fread(buffer_copy, 1, _read_write_buffer_size, _file) ;

            if (_cipher) {
                // _buffered_data_size could be less than buffer_size, if end-of-file is reached
                _buffered_data_size = _cipher->decrypt(buffer_copy, _read_write_buffer, _buffered_data_size) ;
                Strings::free(buffer_copy) ;
            }
        }

        size_t n_element_to_read = n_elements ;
        if ((_buffer_index + n_elements) > _read_write_buffer_size) {
            n_element_to_read = _read_write_buffer_size - _buffer_index ;
        }

        memcpy(s, (_read_write_buffer + _buffer_index), n_element_to_read) ;
        _buffer_index += n_element_to_read ;

        n_elements -= n_element_to_read ;
        s = s + n_element_to_read ;

        if (_buffer_index == _read_write_buffer_size) _buffer_index = 0 ;
    }

    return nmemb ;
#else
    (void) skip_buffer ; // suppress warning in compiler

    // Read this many bytes :
    size_t num = ::fread(store, 1, nmemb, _file) ;
    if (num != nmemb) {
        FileNotOk() ; // flag this SaveResore object as invalid
        return 0 ;
    }
    return num ; // return number of bytes read
#endif
}

/*------------------------------------------------*/

size_t
SaveRestore::BufferedWrite(const void *source, size_t nmemb, unsigned skip_buffer)
{
    VERIFIC_ASSERT(source) ;

#ifdef USE_BUFFERED_READ_WRITE
    if (skip_buffer) {
        size_t num = ::fwrite(source, 1, nmemb, _file) ;
        if (num != nmemb) {
            FileNotOk() ; // flag this SaveResore object as invalid
            return 0 ;
        }
        return num ;
    }

#if 0
    if (nmemb > _read_write_buffer_size) {
        // Requested data cannot be buffered because it is bigger than the buffer size itself.
        // Use direct file write. First flush the already buffered data:
        (void) FlushBuffer() ;
        // Now write the data directly to the file:
        return ::fwrite(source, 1, nmemb, _file) ;
    }

    VERIFIC_ASSERT(_read_write_buffer_size >= nmemb) ;

    // Check whether we have the requested buffer space available for the data:
    if ((_buffer_index + nmemb) > _read_write_buffer_size) {
        // Available buffer is shorter than the given data. Need to flush the buffer:
        (void) FlushBuffer() ;
    }

    // Now copy the data over from source to the buffer for writing later:
    // CHECKME: For char/int/long/double simple assignment might faster than memcpy.
    memcpy((_read_write_buffer + _buffer_index), source, nmemb) ;
    _buffered_data_size += nmemb ; // Update size
    _buffer_index += nmemb ; // Update the index
#endif

    // For encryption of protected data, ALWAYS write through FlushBuffer
    size_t n_elements = nmemb ; // n_elements to write
    const char *s = (const char *)source ;
    while (n_elements) {
        size_t n_element_to_write = n_elements ;
        if ((_buffer_index + n_elements) > _read_write_buffer_size) {
            n_element_to_write = _read_write_buffer_size - _buffer_index ;
        }

        memcpy((_read_write_buffer + _buffer_index), s, n_element_to_write) ;
        _buffered_data_size += n_element_to_write ;
        _buffer_index += n_element_to_write ;

        n_elements -= n_element_to_write ;
        s = s + n_element_to_write ;

        if (_buffered_data_size == _read_write_buffer_size) (void) FlushBuffer() ;
    }

    return nmemb ;
#else
    (void) skip_buffer ; // suppress warming in compiler

    // write this many bytes
    size_t num = ::fwrite(source, 1, nmemb, _file) ;
    if (num != nmemb) {
        FileNotOk() ; // flag this SaveResore object as invalid
        return 0 ;
    }
    return num ;
#endif
}

/*------------------------------------------------*/

#ifdef USE_BUFFERED_READ_WRITE
size_t
SaveRestore::FlushBuffer()
{
    if (!_buffered_data_size || _restore) return 0 ; // No data to be flushed or in restore mode

#if 0
    for (unsigned int i = 0 ; i < _read_write_buffer_size ; ++i) {
        printf ("<%d %d> ", i, _read_write_buffer[i]) ;
    }
    printf("\n") ;
#endif

    unsigned n_data_to_write = _buffered_data_size ;
    char *buffer_copy = _read_write_buffer ;
    if (_cipher) {
        if (_buffered_data_size % _cipher->GetBlockSize()) {
            // To make n_data_to_write multiple of BlockSize
            n_data_to_write += (_cipher->GetBlockSize() - _buffered_data_size % _cipher->GetBlockSize()) ;
        }
        for (unsigned int i = _buffered_data_size ; i < n_data_to_write && i < _read_write_buffer_size ; ++i) {
            _read_write_buffer[i] = 0 ; // pad the remaining bits for half full buffer
        }

        buffer_copy = Strings::allocate(n_data_to_write) ;
        n_data_to_write = _cipher->encrypt(_read_write_buffer, buffer_copy, n_data_to_write) ;
    }

    // Flush writes the waiting-to-be-written buffer into the disk file:
    size_t num = ::fwrite(buffer_copy, 1, n_data_to_write, _file) ;

    // After we are done writing, reset these two:
    // CHECKME: Check num == _buffered_data_size to check that the full data is written.
    _buffered_data_size = 0 ;
    _buffer_index = 0 ;

    if (_cipher) Strings::free(buffer_copy) ;

    return num ;
}
#endif

/*------------------------------------------------*/

