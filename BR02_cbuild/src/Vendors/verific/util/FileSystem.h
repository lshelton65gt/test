/*
 *
 * [ File Version : 1.8 - 2014/01/09 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_FILESYSTEM_H_
#define _VERIFIC_FILESYSTEM_H_

#include "VerificSystem.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;

/*
    Utility class providing certain file system operations.
*/

/* -------------------------------------------------------------- */

class VFC_DLL_PORT FileSystem
{
    // This class should never get instantiated (make construction/destruction private)
private :
    // Prevent compiler from defining the following
    FileSystem() ;                              // Purposely leave unimplemented
    ~FileSystem() ;                             // Purposely leave unimplemented
    FileSystem(const FileSystem &) ;            // Purposely leave unimplemented
    FileSystem& operator=(const FileSystem &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Path checks
    static unsigned     PathExists(const char *path) ;
    static unsigned     IsFile(const char *path) ;
    static unsigned     IsDir(const char *path) ;
    static unsigned     IsAbsolutePath(const char *path) ;

    // Directory-based operations
    static unsigned     Mkdir(const char *path) ;
    static void         ReadDirectory(const char *path, Array *files) ;
    static char *       GetCwd() ; // Does not use the cache, need to free the returned string

    // Path operations
    static char *       DirectoryPath(const char *filename) ;
    static unsigned     CompactAbsolutePath(char *filename) ; // Compact the path in-place
    static char *       Convert2AbsolutePath(const char *filename, unsigned compact) ;
    static char *       Convert2RelativePath(const char *filename, const char *from_dir) ;

    // CWD cache support
    static unsigned     CacheCwd() ;
    static void         ResetCacheCwd() ;
    static const char * GetCachedCwd()      { return _cwd ; } // May be NULL if not cached

    static char *       GetTemporaryFileName() ; // Returns a (temporary) file name which does not exist in the curreent working directory

private:
    static char *_cwd ; // Current working directory (cache)
} ; // class FileSystem

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_FILESYSTEM_H_

