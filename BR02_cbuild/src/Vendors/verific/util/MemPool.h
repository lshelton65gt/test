/*
 *
 * [ File Version : 1.22 - 2014/01/30 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_MEMPOOL_H_
#define _VERIFIC_MEMPOOL_H_

#include <stdlib.h> // for 'size_t, malloc and free

#include "VerificSystem.h"

#ifdef VERIFIC_NAMESPACE
// Define that the following system functions are in Verific namespace
namespace Verific {
#endif

/*
    MemPool is Verific's pool-based memory manager.

    You can use a dedicated MemPool to manage memory for a specific C++ class or tree of derived classes,
    by overriding the new and delete operators for that (base) class.

    If compile switch (VERIFIC_MEMORY_MANAGER) is set, then you will automatically route
    ALL new and delete operations for all new and delete calls that can see this system header file
    through a single (global) Verific memory pool.

    This memory pool class is extremely efficient (fast and minimum overhead) for small memory fragments.
    Any memory chunk requested that are larger than 256 void*'s (1Kbyte on a 32 bit machine) are
    automatically routed through to the OS (both at new and delete time).

    MemPool allocates large blocks (about 1Mbyte) from the OS and distributes fragments from
    within that block to the user. It manages a garbage list of fragments per block, and returns
    the block to the OS once all fragments have been freed by the user.
    Memory overhead is only 2 bytes per fragment (while the OS typically needs 8 bytes per fragment).
    Because of this, program memory usage typically goes down about 20% for application programs.
    Speed of MemPool for small memory fragments is much faster than OS memory manager, and we often
    see runtime improvements up to 25% using MemPool.
*/

// Memory manager
#define MM_NUM_BLOCK_SLOTS 256 // Number of (block) slots that can be encoded with the 1 byte we have available.

// Define the largest scalar type with which the memory chuncks should align.
// On Linux and Windows 32 bit that is type 'void*' (pointer-aligned) and on Solaris sparc machines it is 'double'.
// There does not seem to be any standardized way to find out this alignment size on the various platforms.
// So, I guess we just need to find this with trial and error..
#ifdef __sparc
#define MM_ALIGN_TYPE double
#else
#define MM_ALIGN_TYPE void*
#endif

class MemBlock ; // blocks of memory allocated by the memory manager

/* -------------------------------------------------------------- */

// Verific memory manager.
class VFC_DLL_PORT MemPool
{
    friend class MemBlock ; // Need to make the MemBlock give access to my data members
public:
    // Constructor(s)/Destructor
    MemPool() ;  // constructor.
    ~MemPool() ; // destructor. Free all memory allocated.

private:
    // Prevent compiler from defining the following
    MemPool(const MemPool &) ;            // Purposely leave unimplemented
    MemPool& operator=(const MemPool &) ; // Purposely leave unimplemented

public:
    // if MemPool itself is allocated, use malloc/free :
    static void * operator new(size_t size)         { return ::malloc(size) ; }
    static void   operator delete(void *p)          { ::free(p) ; }

    // New and Delete of fragments through a MemPool
    void *        New(unsigned size) ;
    void          Delete(void *p) ;

    // The block recycle list :
    void          StartRecycle(MemBlock *block) ; // add block to the recycle list
    void          StopRecycle(MemBlock *block) ; // remove block from the recycle list

    // Delete (Release to OS) all memory allocated by this pool, and re-initialize pool (as if we started fresh).
    void          Reset() ;

    // Utilities
    void          MemStats() ;       // memstats for this memory manager
    static void   GlobalMemStats() ; // memstats for global memory manager

protected:
// new list
    MemBlock *_blocks[MM_NUM_BLOCK_SLOTS] ; // Databank of up to 256 block pointers
    MemBlock *_current ;                    // Pointer to current (most active) block
    unsigned  _free_slot ;                  // Index of next 'free' slot (this rotates).
    unsigned  _block_count ;                // Amount of blocks in use
    unsigned  _peak_block_count ;           // Maximum blocks-in-use-count encountered
    unsigned  _passed_through ;             // Space (void*-size chunks) passed through to OS.
    // the 'recycle' list of blocks : a FIFO: push into recycle_in, remove from recycle_out.
    MemBlock *_recycle_in ;
    MemBlock *_recycle_out ;
} ; // class MemPool

/* -------------------------------------------------------------- */

class VFC_DLL_PORT GlobalMemPool : public MemPool
{
private:
    // Constructor
    GlobalMemPool() : MemPool() { }  // constructor.

    // No need to declare destructor.  If you do, you must make it public

private:
    // Prevent compiler from defining the following
    GlobalMemPool(const GlobalMemPool &) ;            // Purposely leave unimplemented
    GlobalMemPool& operator=(const GlobalMemPool &) ; // Purposely leave unimplemented

public:
    // Public accessor routine to return the one-and-only GlobalMemPool object.
    // Initialize (if not done already) and return the reference to the single object:
    static GlobalMemPool & GetRef() { if (!_global_pool) _global_pool = new GlobalMemPool() ; return *_global_pool ; }

    // Delete (Release to OS) and nullify global pool
    void ResetRef() { delete _global_pool ; _global_pool = 0 ; }

private:
    static GlobalMemPool *_global_pool ;
} ; // class GlobalMemPool

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
}
#endif

#endif // #ifndef _VERIFIC_MEMPOOL_H_
