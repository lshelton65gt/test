/*
 *
 * [ File Version : 1.39 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_NAMESPACE_H_
#define _VERIFIC_NAMESPACE_H_

#include "VerificSystem.h"
#include "Map.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------- */

/*
    Package to perform renaming of identifier name clashes in
    a name space.
    'NameSpace' object constitutes a name space, within which names
    must be unique. NameSpace
    Renaming rules are provided to identify and change illegal
    names in the name space.

    Call InsertName() to insert a new (unique) name into the name space.
    This name will be tested against the renaming rules, which will make the
    name 'legal' by creating a replacement.

    It depends on the renaming rule as to what the replacement is.
    For example, the IllegalCharacters() rule allows a replacement character for every
    illegal character in an inserted name.
    Word-Escape characters (provided by the user) are used to make a
    name 'legal' if a replacement character is 0.

    If inside 'InsertName' for some reason (renaming) two originally
    different names become the same, the Collision routine will be called
    to create a unique name for the last one inserted.
    The default Collision routine adds "_c" to a name.
    After each renaming (or collision resolve), the re-naming process is
    started all over again, until the resulting name is legal, and
    does not collide with a previously inserted name.
    All this happens in InsertName(), so should be transparent to the
    user.

    User just has to create the namespace (case-sensitive collisions
    or not) and add the renaming rules (list illegal characters, etc)
    and set the replacement action (replacement character, or use word-escape
    to make a name legal).

    NameSpaces can be 'linked' too. Any collisions between names from
    two namespaces will be resolved between linked name spaces.
    For example, if nets and ports end up in the same name space in the
    output format, a net and port with the same name should be named differently
    in the output format. So create a NameSpace for nets and a NameSpace for ports,
    and link them.
    There is no limit to the number of NameSpaces that can be linked this way.
    If you link namespace A to B and then B to C and then C to D,
    collision checks between all of them will be done for every word inserted
    in any of these name spaces.

    Renaming rule routines are provided for illegal characters, illegal
    start/end characters, illegal substrings and illegal words (reserved words).
    Additional renaming rules can be added, even without changing the
    source code of the NameSpace class itself. For this reason, the
    ReName() and Collision() routines are make 'virtual' (derived name space
    classes could have more renaming rules).
*/

class VFC_DLL_PORT NameSpace : public Map
{
public :
    explicit NameSpace(unsigned case_sensitive = 1);
    // Note: Destructor of base class Map is not virtual, do not delete a NameSpace object by Map *
    virtual ~NameSpace();

private :
    // Prevent compiler from defining the following
    NameSpace(const NameSpace &) ;            // Purposely leave unimplemented
    NameSpace& operator=(const NameSpace &) ; // Purposely leave unimplemented

public :
    // Some renaming rules
    void            SetIllegalChars(const char *illegal, const char replacement) ;      // Illegal character anywhere in the word
    void            SetIllegalStartChars(const char *illegal, const char replacement) ; // Illegal starting characters
    void            SetIllegalEndChars(const char *illegal, const char replacement) ;   // Illegal ending characters
    void            SetIllegalSubstring(const char *illegal, const char *replacement) ; // Illegal substring anywhere in the word

    void            AddReservedWord(const char *word, const char *replacement = 0) ;    // Reserved words
    void            EscapeWord(const char prefix, const char suffix = 0) ;              // Escape characters for the word

    // To create (subspace) with same rules. (Don't copy the contents of the inserted names; just copy the rules)
    NameSpace *     Copy() ;

    // Link another space : Make spaces sub-space of one resulting namespace
    // name collisions will be resolved between linked name spaces
    void            LinkSpace(NameSpace *other) ;
    // VIPER #6628: Link two namespaces wihtout size check: use with caution, if there is actually
    // any conflicting entry before the linking, this utility will fail to rename it properly.
    void            LinkSpaceWithoutSizeCheck(NameSpace *other) ;

    // VIPER #4160: Reset the linked name-spaces with this name-space.
    void            ResetLinkedSpaces() ;

    // Insert a name into the name space. Renaming will happen internally, if needed.
    // To save memory, we don't store copies inside.
    // 'orig' pointer is stored in a hash table inside, but is never modified or freed.
    void            InsertName(const char *orig) ;

    // Get a renamed word, by original name. If it's renamed, return a (const)
    // pointer to the renamed string. Otherwise, return the original const pointer.
    const char *    GetName(const char *orig) const ;

    // These are normally not used, but could be, if needed....
    char *          ReName(const char *orig) ;            // Rename return allocated string if renaming happened. 0 if not.
    char *          Collision(const char *orig) const ;   // Create a allocated new name, derived from 'orig'.
    char *          ResolveCollision(const char *name) ;  // Create a allocated collision-free new name if 'name' collides with other names in the linked name spaces. Return 0 if collision-free.
    char *          Escape(const char *orig) const ;      // Create an escaped word from the original. Return 0 if there are no escape characters

    // Overridden retrieval methods:
    MapItem *       GetItem(const void *key) const ;      // Checks both the base Map as well as the _renamed_words Map

private :
    unsigned     _case_sensitive ;
    const char  *_illegal_chars ;       // String of illegal characters
    const char  *_illegal_start_chars ; // String of illegal starting characters
    const char  *_illegal_end_chars ;   // String of illegal ending characters
    const char  *_illegal_substring ;   // String of illegal sub-strings
    const char  *_substring_replacement ;
    Map         *_reserved_words ;

    NameSpace   *_next_space ;

    char         _replacement_char ;
    char         _start_replacement_char ;
    char         _end_replacement_char ;
    char         _escape_word[2] ;

    Map         *_renamed_words ;       // Map of renamed words -> original name
} ; // class NameSpace

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_NAMESPACE_H_

