/*
 *
 * [ File Version : 1.68 - 2014/01/09 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_SAVERESTORE_H_
#define _VERIFIC_SAVERESTORE_H_

#include <stdio.h> // Use old file handles (FILE*) to save/restore

#include "VerificSystem.h"
#include "LineFile.h"
#include "Map.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Protect ;

/* -------------------------------------------------------------- */

// Temporary compile flag to enable buffered read/write in disk file access:
#define USE_BUFFERED_READ_WRITE

class VFC_DLL_PORT SaveRestore
{
public :
    explicit SaveRestore(const char *file_name, unsigned restore=1, unsigned consider_file_mapping=1, Protect *cipher = 0) ;
    virtual ~SaveRestore() ;

private :
    // Prevent compiler from defining the following
    SaveRestore() ;                               // Purposely leave unimplemented
    SaveRestore(const SaveRestore &) ;            // Purposely leave unimplemented
    SaveRestore& operator=(const SaveRestore &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    unsigned            IsOK() const              { return _systems_go ; } // Check if the SaveRestore object is ready to parse the file
    unsigned            IsRestoring() const       { return _restore ; }
    unsigned            IsSaving() const          { return !IsRestoring() ; }
    const char *        FileName() const          { return _file_name ; }
    char *              GetDumpDirectory() const ; // VIPER #6358: Get the directory into which the _file_name resides
    void                FileNotOk() ; // Specify that file contains an error, such that when the destructor
                                  // Gets called, it won't assert on the signature check.
    // Get/Set version
    unsigned            GetVersion() const        { return _version ; }
    void                SetVersion(unsigned ver)  { _version = ver ; }

    unsigned long       GetTimestamp() const      { return _timestamp ; }

    unsigned            HasProtectionObject() const ;

    // Save objects
    void                SaveChar(char c) ;
    void                SaveInteger(int integer, unsigned skip_buffer = 0) ;
    void                SaveLong(long integer) ;
    void                SavePointer(const void *pointer) ;
    unsigned            IsSaved(const void *pointer) const ;   // Return 1 if the pointer was already saved
    void                SaveDouble(double floating_point) ;
    void                SaveVerific64(verific_int64 integer) ;
    void                SaveString(const char *string) ;
    void                SaveLinefile(linefile_type linefile) ; // Save line/file info (from system utility LineFile).

    // Pointer registration (for save and restore)
    unsigned            IsRegistered(const void *pointer) const ; // Check if this pointer is registered
    void                RegisterPointer(const void *pointer) ; // Register pointer to an object
    void                ReRegister(const void *old_pointer, const void *new_pointer) ; // Re-Register a pointer. This does NOT read any data from the file. It just changes the pointer that will be returned by RestorePointer()
    void                UnRegister(const void *pointer) ;      // Unregister a pointer

    // Restore objects
    char                RestoreChar() ;     // Restore a char value
    int                 RestoreInteger(unsigned skip_buffer = 0) ;  // Restore an integer (int) value
    long                RestoreLong() ;     // Restore an integer (long) value
    void *              RestorePointer() ;  // Restore a registered pointer ; map it back to the registered pointer.
    double              RestoreDouble() ;   // Restore a double value
    verific_int64       RestoreVerific64() ;// Restore a 64 bit value (verific_int64)
    char *              RestoreString() ;   // Restore a char string
    linefile_type       RestoreLinefile() ; // Restore line/file info (from system utility LineFile).

    // Registered pointers are saved as a 'id' number in the stream.
    // Here are two routines that provide the relation between them.
    unsigned long       Pointer2Id(const void *pointer) const ; // Returns id of a registered pointer during 'save'. Or 0 if not registered.
    void *              Id2Pointer(unsigned long id) const ;    // Returns pointer of a registered 'id' during 'restore'. Or 0 if not registered.

    // User flags get/set methods
    unsigned            GetUserFlags() const              { return _user_flags ; }
    void                SetUserFlags(unsigned flags)      { _user_flags = flags ; }

    unsigned            GetSizeofLong() const             { return _sizeof_long ; }

    // Reset the pointer registration settings :
    void                Reset() ; // reset save/restore stream to initial state, without closing the file.

    // Get/Set the current character position in the file (only works for files smaller than 1Gb or so).
    long                GetPos() ; // return the current character file position
    void                SetPos(long pos) ; // set the character file position

    // Check if a file exists (and is a file) on disk. This is not related to save/restore info, so can be used as a regular utility routine :
    static unsigned     FileExists(const char *filename, unsigned consider_file_mapping=1) ;

    // VIPER #3541 & #3790: Long, very long file name support:
    static void         SetMapFileName(const char *name) ; // Static routine to set the name (only name, no directory part) of the map file
    static const char * GetMapFileName()               { return _map_file_name ; }

    // VIPER #3498: Problem with escape identifiers '\' in save-restore:
    static char *       GetDumpFileName(const char *unit_name, const char *extension) ; // unit name -> dump file name convertion
    static char *       GetDesignName(const char *dump_file_name) ; // reverse convertion from dump file name -> unit name

    // Message handling support : Message id table, output formatted, varargs message functions
    static const Map *  GetMessageMap()    { (void) GetMessageId("") ; return _msgs ; } // Return the entire map : (char*)formatted_string -> (char*)message_id
    static const char * GetMessageId(const char *msg) ;     // Aid to get message ID from formatted string. Accessed by Error/Warning/Error
    static void         ResetMessageMap() ;
    static void         Error(const char *format, ...) ;
    static void         Warning(const char *format, ...) ;
    static void         Info(const char *format, ...) ;

private :
    void                EndianShuffle(int &integer) const ;   // 32bit shuffle the endian (big<->small) of this number
    void                EndianShuffle(long &integer) const ;  // 32 or 64bit shuffle the endian (big<->small) of this number
    void                EndianShuffle64(verific_int64 &integer) const ;  // 64bit shuffle the endian (big<->small) of this number

    // VIPER #3541 & #3790: Long, very long file name support:
    static char *       GetMappedFile(const char *file_name, const char *map_file_name, unsigned restore_mode) ; // Main routine to get a mapped file
    static void         RestoreMapping(const char *from_file, Map &long2short_filename, Map &short2long_filename) ; // Restore the mapping from file
    static void         SaveMapping(const char *to_file, const Map &long2short_filename, const Map &short2long_filename) ; // Saves the mapping into file
    static char *       GetFilePathName(const char *path, const char *name, unsigned include_ext) ; // Utility routine to get a file name with path/extension

    // File access in chunk (buffered read/write using fread/fwrite):
    // Note: The following routines use memcpy for memory copying. However we saw,
    // by experiments, that memcpy is slower for copying small memory (like a simple
    // integer, long, double or char) than direct copying by dereferencing them.
    // Currently we are always using memcpy.
    size_t              BufferedRead(void *store, size_t nmemb, unsigned skip_buffer=0) ;
    size_t              BufferedWrite(const void *source, size_t nmemb, unsigned skip_buffer=0) ;
#ifdef USE_BUFFERED_READ_WRITE
    size_t              FlushBuffer() ; // Flush writes the waiting-to-be-written buffer into the disk file
#endif

    FILE         *_file ;
    char         *_file_name;
    Map           _pointer2num ;      // Keeps track of pointer -> number assoc during save or restore
    Map           _num2pointer ;      // Keeps track of number -> pointer assoc during save or restore
    Map           _filename2id ;      // During save : keeps track of files associated with saved linefile objects.
    Map           _id2filename ;      // During Restore : find filename back from old fileid.
    unsigned long _pointer_number ;   // Number used for pointer identification while saving. Increments with each RegisterPointer call.

    // Version flag
    unsigned      _version ;          // Stream version number of the file we are restoring (used for backward compatibility)

    // Timestamp
    unsigned long _timestamp ;        // The time this file was created.

    // General bit flags
    unsigned      _restore:1 ;        // Flag indicating save or restore
    unsigned      _endian_shuffle:1 ; // Flag indicating that integer endian need reshuffling (restore only) (platform difference between save/restore)
    unsigned      _systems_go:1 ;     // Flag indicating that the object is ready for data processing
    unsigned      _encrypt:1 ;        // Flag indicating whether stream contains encrypted strings

    // Linefile save/restore mechanism flags
    unsigned      _new_linefile:1 ;   // Flag indicating whether we need to restore linefile saved with new register pointer mechanism

    // Compile switch flags
    unsigned      _column_active:1 ;  // Flag indicating whether linefile in restore stream was saved with column info.
    unsigned      _large_linefile:1 ; // Flag indicating whether linefile in restore stream was saved without column info under compile flag "VERIFIC_LARGE_LINEFILE"

    // Linefile sizes
    unsigned      _line_num_bits ;    // Number of bits used for line number   (corresponds to LineFile.h settings)
    unsigned      _file_num_bits ;    // Number of bits used for file number   (corresponds to LineFile.h settings)
    unsigned      _col_num_bits ;     // Number of bits used for column number (corresponds to LineFile.h settings)

    // General flag holder
    unsigned      _user_flags ;       // A 32-bit flag holder which may be used by the SaveRestore application.  It's up
                                      // to the user to set the individual bits appropriately.

    // Backward-compatibility support
    unsigned      _restore_version ;  // Vdb version variable needed for backward-compatibility support during restore

    // 64bit support
    unsigned      _sizeof_long ;      // Value of sizeof(long) which is needed for 64bit restore support
    unsigned      _restore_little_endian ; // States whether the restored vdb came from a little-endian machine

    // VIPER #3541 & #3790: Long, very long file name support:
    static char  *_map_file_name ;    // The name (no directory part) of the map file to be used for mapping files

#ifdef USE_BUFFERED_READ_WRITE
    // File access in chunks (buffered read/write):
    char         *_read_write_buffer ;      // The buffer itself
    unsigned      _buffered_data_size ;     // The size of the buffer filled in
    unsigned      _buffer_index ;           // Index into the buffer
    unsigned      _read_write_buffer_size ; // Must always be multiple of _cipher block size

    Protect      *_cipher ;                 // For encrypt/decrypt
#endif

    static Map   *_msgs ;                   // List of all messages. message->ID table. Accessed only from GetMessageId routine.
    char         *_buffer ;                 // JJ 130110: modifying RestoreString to use the data member optimizes memory and removes memory leak
    unsigned      _buffer_size ;            // VIPER #7930: Need to add this as member too, static member local to function causes memory corruptions.
} ; // class SaveRestore

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_SAVERESTORE_H_

