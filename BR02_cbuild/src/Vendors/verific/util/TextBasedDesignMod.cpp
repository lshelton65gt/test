/*
 *
 * [ File Version : 1.119 - 2014/02/26 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "TextBasedDesignMod.h"

#include <string.h>   // for strstr
#include <stdio.h>    // for rename
#include <sstream>    // for ostringstream
#include <fstream>    // for ostream and ifstream

using namespace std ;

#include "FileSystem.h"
#include "Strings.h"
#include "Map.h"
#include "Array.h"
#include "Message.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

////////////////////////////////////////////////////////////////////////////
////////////              Internal class definition             ////////////
////////////////////////////////////////////////////////////////////////////

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* static */ Map *TextBasedDesignMod::_msgs = 0 ; // VIPER #6035: Message id store

/* -------------------------------------------------------------- */

/*
    Class FileModMngr is a manager class controlling FileMod objects.  FileModMngr
    is-an Array, and provides search and insertion functionality customized to
    the algorithms used by TextBasedDesignMod.  TextBasedDesignMod will create one
    FileModMngr per modified design file.  Insert into this class will be done such
    that the contents of the array are sorted based on line offset information.  If
    a FileMod attempts to get inserted into its FileModMngr, and there is already
    a FileMod entry of the same line_offset, then if the new FileMod's column offset
    is less then the corresponding FileMod already store, it will overwrite the
    later, else, the new FileMod won't be inserted.
*/

class FileModMngr : public Array
{
public :
    FileModMngr() ;
    // Note: Destructor of base class Array is not virtual, do not delete a FileModMngr object by Array *
    ~FileModMngr() ;

private :
    // Prevent compiler from defining the following
    FileModMngr(const FileModMngr &) ;            // Purposely leave unimplemented
    FileModMngr& operator=(const FileModMngr &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    FileMod *   GetFileSection(const linefile_type lf) ; // Get file section via binary search
    void        InsertSection(const FileMod *section) ;  // Insert file section via walking from _last_idx

private :
    unsigned    _last_idx ; // last index marker set in GetFileSection and used by InsertSection for quick insertion
} ; // class FileModMngr

/* -------------------------------------------------------------- */

/*
    Class FileMod represents a file modification at a specific line and
    column offset.  It contains text arrays which are filled when inserting new
    text into a file, before or after a given line/column offset.  A couple
    of "print" flags are used to prevent certain sections of the file from being
    printed (i.e. Remove/Replace functionality).  This class is only accessed
    by the TextBasedDesignMod (Text-Based Design Modification) class.  FileMod objects
    are guaranteed by TextBasedDesignMod to be linked in increasing line/column
    offset order.
*/

class FileMod
{
    friend class FileModMngr ;
    friend class TextBasedDesignMod ;

private :
    FileMod(unsigned line, unsigned column) ;
    ~FileMod() ;

private :
    // Prevent compiler from defining the following
    FileMod() ;                           // Purposely leave unimplemented
    FileMod(const FileMod &) ;            // Purposely leave unimplemented
    FileMod& operator=(const FileMod &) ; // Purposely leave unimplemented

private :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    unsigned       WriteToStream(ostream &out_file) const ; // Write modifications to steam

    unsigned       InsertAfter(const char *text) ;    // Inserts text into _insert_after Array
    unsigned       InsertBefore(const char *text) ;   // Inserts text into _insert_before Array

    unsigned       PrintBefore() const              { return _print_before ; }
    unsigned       PrintAfter() const               { return _print_after ; }
    void           SetPrintBefore(unsigned print)   { _print_before = print ; }
    void           SetPrintAfter(unsigned print)    { _print_after = print ; }

    unsigned       LineOffset() const               { return _line_off ; }
    unsigned       ColOffset() const                { return _col_off ; }
    void           SetLineOffset(unsigned line)     { _line_off = line ; }
    void           SetColOffset(unsigned col )      { _col_off = col; }

    FileMod *      Next() const                     { return _next ; }
    void           SetNext(FileMod *next)           { _next = next ; }

    const Array *  InsertBeforeText() const         { return _insert_before ; }
    const Array *  InsertAfterText() const          { return _insert_after ; }

private :
    Array      *_insert_before ;
    Array      *_insert_after ;

    unsigned    _print_before:1 ; // Specifies whether insertions before section should be printed
    unsigned    _print_after:1 ;  // Specifies whether span after section should be printed

    unsigned    _line_off ;       // Line offset
    unsigned    _col_off  ;       // Column offset

    FileMod    *_next ;           // Pointer to next file modification
} ; // class FileMod

/* -------------------------------------------------------------- */

/*
    Class InputFile is a data structure class associating a given input file stream
    (ifstream) object to its current byte, line, and column offsets which have already
    been read.  This class if only used directly by the FileMod::WriteToFile method.
*/

class InputFile : public ifstream
{
public :
    InputFile()
      : ifstream(),
        _byte_offset(0),
        _last_newline_byte_offset(0),
        _line_offset(1),
        _column_offset(1) { } ;
    ~InputFile() { } ;

    void Clear() { clear() ; _byte_offset = 0 ; _last_newline_byte_offset = 0 ; _line_offset = 1 ; _column_offset = 1 ; }

    // VIPER #5914: Open file, if not found search in the 'search_paths' and open it if found:
    void SearchOpenFile(const char *file_name, ios::openmode mode = ios::in, const Array *search_paths = 0) ;

private :
    // Prevent compiler from defining the following
    InputFile(const InputFile &) ;            // Purposely leave unimplemented
    InputFile& operator=(const InputFile &) ; // Purposely leave unimplemented

public :
    unsigned long _byte_offset ;              // Byte offset corresponding to current line/column value pair
    unsigned long _last_newline_byte_offset ; // Byte offset of last newline character read
    unsigned      _line_offset ;
    unsigned      _column_offset ;
} ; // class InputFile

/* -------------------------------------------------------------- */

void
InputFile::SearchOpenFile(const char *file_name, ios::openmode mode /* = in */, const Array *search_paths /* = 0 */)
{
    open(file_name, mode) ;
    if (!fail()) return ; // Opened

    unsigned i ;
    const char *path ;
    char *file_path_name ;
    FOREACH_ARRAY_ITEM(search_paths, i, path) {
        if (!path) continue ;
        file_path_name = Strings::save(path, "/", file_name) ;
        open(file_path_name, mode) ;
        Strings::free(file_path_name) ;
        if (!fail()) break ; // Opened, done
    }
}

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

////////////////////////////////////////////////////////////////////////////
////////////              Constructor/Destructor                ////////////
////////////////////////////////////////////////////////////////////////////

TextBasedDesignMod::TextBasedDesignMod(const char *secure_directory) :
    _secure_dir(Strings::save(secure_directory)),
    _modifications(new Map(NUM_HASH)),
    _ifstreams(new Map(NUM_HASH)),
    _source_search_paths(0)
{
}

TextBasedDesignMod::~TextBasedDesignMod()
{
    // Free secure directory path
    Strings::free(_secure_dir) ;

    // Delete all the modifications for all the files
    MapIter mi ;
    FileModMngr *mod_mngr ;
    FOREACH_MAP_ITEM(_modifications, mi, 0, &mod_mngr) {
        // This will delete the whole linked list!
        delete mod_mngr ;
    }
    // Delete the map
    delete _modifications ;

    // Need to close and delete all open ifstreams
    CloseFileHandles() ;
    delete _ifstreams ;

    // VIPER #5914: Clear search paths:
    unsigned i ;
    char *path ;
    FOREACH_ARRAY_ITEM(_source_search_paths, i, path) Strings::free(path) ;
    delete _source_search_paths ;
}

/////////////////////////////////////////////////////////////////////////////
////////                      Internal  Utilities                      //////
/////////////////////////////////////////////////////////////////////////////

unsigned
TextBasedDesignMod::IsSecure(const linefile_type lf) const
{
    // Get the file name from the linefile info
    const char *file_name = LineFile::GetFileName(lf) ;

    // Call the routine that takes a file name with this file
    return IsSecure(file_name) ;
}

/* -------------------------------------------------------------------------- */

// Check whether this file resides inside the secure directory
unsigned
TextBasedDesignMod::IsSecure(const char *file_name) const
{
    if (!file_name) return 0 ; // Unnamed file is not secured!

    if (!_secure_dir) return 1 ; // No secure directory, all files are secured.

    // Convert _secure_dir and file_name to absolute paths
    char *abs_path_dir  = FileSystem::Convert2AbsolutePath(_secure_dir, 1) ;
    char *abs_path_file = FileSystem::Convert2AbsolutePath(file_name, 1) ;

#ifdef WINDOWS
    // Windows is case-insensitive, so lower case paths for strstr comparison only
    (void) Strings::strtolower(abs_path_dir) ;  // lower-casing string in place
    (void) Strings::strtolower(abs_path_file) ; // lower-casing string in place
#endif

    // Determine if 'file_name' resides underneath _secure_dir directory.
    if (abs_path_file == strstr(abs_path_file, abs_path_dir)) {
        Strings::free(abs_path_dir) ;
        Strings::free(abs_path_file) ;
        return 1 ; // file_name resides in the secure directory
    }

    Strings::free(abs_path_dir) ;
    Strings::free(abs_path_file) ;

    // Otherwise it's not secured
    return 0 ;
}

/* -------------------------------------------------------------------------- */

FileModMngr *
TextBasedDesignMod::GetFileModMngr(const linefile_type lf, unsigned create_if_needed /*=1*/) const
{
    VERIFIC_ASSERT(_modifications) ;

    if (!lf) return 0 ;

    unsigned long file_id = LineFile::GetFileId(lf) ;
    FileModMngr *mod_mngr = (FileModMngr*)_modifications->GetValue((void*)file_id) ;
    if (mod_mngr || !create_if_needed) return mod_mngr ;

    // Since no modification manager exists for this file, create one now.
    mod_mngr = new FileModMngr() ;
    (void) _modifications->Insert((void*)file_id, mod_mngr) ;
    return mod_mngr ;
}

/* -------------------------------------------------------------------------- */

// Insert a number of blank space character after each newline to indent it properly
char *
TextBasedDesignMod::Indent(const linefile_type pos, const char *text) const
{
    if (!pos || !text) return Strings::save(text) ;

    // Adjust linefile to represent a single line.  This is needed for the subsequent GetText call.
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    linefile_type lf = pos->Copy() ;
    lf->SetRightLine(lf->GetLeftLine() + 1) ;
    lf->SetLeftCol(1) ;
    lf->SetRightCol(1) ;
#else
#ifdef VERIFIC_LARGE_LINEFILE
    linefile_type lf = pos->Copy() ;
    lf->SetRightLine(LineFile::GetLineNo(lf) + 1) ;
#else
    linefile_type lf = pos ;
#endif
#endif

    // Get current line to determine identation count
#ifdef TBMD_DEBUG
    unsigned old_print_value = debug_print ;
    debug_print = 0 ;
#endif

    char *line = GetText(lf, 0) ;

#ifdef TBMD_DEBUG
    debug_print = old_print_value ;
#endif

    if (!line) return Strings::save(text) ;
    // VIPER #4155: Find the spaces for indentation:
    char *p = line ;
    while (*p && isspace(*p)) {
        if ((*p == '\n') || (*p == '\r')) break ; // VIPER #8467: Break at the newline
        p++ ;
    }
    *p = '\0' ; // Terminate at first no-space character
    unsigned num_spaces = Strings::len(line) ;
    if (!num_spaces) {
        // No valid character found, it seems to be an empty line:
        Strings::free(line) ;
        return Strings::save(text) ;
    }

    // First, quickly count the number of newline characters:
    unsigned num_newlines = 0 ;
    const char *start = text ;
    const char *newline = strchr(start, '\n') ;
    while (newline) {
        num_newlines++ ;
        start = newline+1 ;
        newline = strchr(start, '\n') ;
    }

    // Allocate enough space for the new text with blank spaces:
    // Also handles the situation when there is no newline in it!
    char *ret_text = Strings::allocate(Strings::len(text) + (num_spaces * num_newlines) + 1) ;

    // Now copy the string 'text' into 'ret_text' and insert
    // blank spaces after each newline character:
    p = ret_text ;
    const char *idx = text ;
    while ((*p++ = *idx) != '\0') { // Includes the '\0' assignment also!
        // If it was a newline character:
        if (*idx++ == '\n') { // Always increments idx by 1
            // VIPER #4155: Insert blank spaces:
            (void) ::strcpy(p, line) ;
            p = p + num_spaces ;
        }
    }
    Strings::free(line) ; // Need to free this up

    // Return the indented text (it must be freed by the caller):
    return ret_text ;
}

/////////////////////////////////////////////////////////////////////////////
////////                    Manipulation  Utilities                    //////
/////////////////////////////////////////////////////////////////////////////

unsigned
TextBasedDesignMod::InsertBefore(const linefile_type pos, const char *text, unsigned ident) const
{
    if (!pos || !text) return 0 ;

    unsigned insert_line, insert_col ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    insert_line = pos->GetLeftLine() ;
    insert_col  = pos->GetLeftCol() ;
#else
    // Insert on line 'pos', column 1
    insert_line = LineFile::GetLineNo(pos) ;
    insert_col  = 1 ;
#endif

#ifdef TBMD_DEBUG
    if (debug_print) {
        fprintf(debug_fp, "InsertBefore - (%s)  line:%d  col:%d  text:\"%s\"\n", LineFile::GetFileName(pos), insert_line, insert_col, text) ;
    }
#endif

    // If either the line value is zero, exit early.
    if (!insert_line) return 0 ;

    // Ident all new lines of 'text' by indentation of line specified by linefile 'pos'
    char *text_to_insert = (ident) ? Indent(pos, text) : Strings::save(text) ;

    // Find the section where argument specific text is to be inserted
    FileModMngr *mod_mngr = GetFileModMngr(pos) ;
    FileMod *file_mod = (mod_mngr) ? mod_mngr->GetFileSection(pos) : 0; // Get/create the file section for this file
    FileMod *next_section = 0 ;
    unsigned cur_line, cur_col, next_line, next_col ;
    while (file_mod) {
        VERIFIC_ASSERT(mod_mngr) ;
        // Get current section's offset info
        cur_line     = file_mod->LineOffset() ;
        cur_col      = file_mod->ColOffset() ;
        // Get next section's offset info
        next_section = file_mod->Next() ;
        next_line    = (next_section) ? next_section->LineOffset() : 0 ;
        next_col     = (next_section) ? next_section->ColOffset() : 0 ;

        if ((cur_line == insert_line) && (cur_col == insert_col)) {
            // This section already exists.
            if (!file_mod->PrintBefore()) {
                Error(pos, "cannot insert text at this location, since file segment was already removed") ;
                Strings::free(text_to_insert) ;
                return 0 ;
            }
            return file_mod->InsertBefore(text_to_insert) ; // text is absorbed
        } else if (!next_section ||
           ((cur_line < insert_line)   && (insert_line < next_line)) ||
           ((cur_line == insert_line)  && (cur_col < insert_col) && !((insert_line == next_line) && (insert_col >= next_col))) ||
           ((insert_line == next_line) && (insert_col < next_col))) {
            // Check to see if file_mod allows something to be inserted here
            if (!file_mod->PrintAfter() || (next_section && !next_section->PrintBefore())) {
                Error(pos, "cannot insert text at this location, since file segment was already removed") ;
                Strings::free(text_to_insert) ;
                return 0 ;
            }
            // Since FileMod boundary for 'pos' doesn't exist, create it now
            FileMod *new_section = new FileMod(insert_line, insert_col) ;
            mod_mngr->InsertSection(new_section) ;
            new_section->SetNext(file_mod->Next()) ;
            file_mod->SetNext(new_section) ;

            // Insert text now
            return new_section->InsertBefore(text_to_insert) ; // text is absorbed
        }

        // Go to next (non-text) section
        file_mod = file_mod->Next() ;
    }

    Strings::free(text_to_insert) ;
    return 0 ;
}

/* -------------------------------------------------------------------------- */

unsigned
TextBasedDesignMod::InsertAfter(const linefile_type pos, const char *text, unsigned ident) const
{
    if (!pos || !text) return 0 ;

    unsigned insert_line, insert_col ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    insert_line = pos->GetRightLine() ;
    insert_col  = pos->GetRightCol() ;
#else
    // Insert on the line following 'pos' (i.e. line pos+1,column 0)
    insert_line = LineFile::GetLineNo(pos) + 1 ; // Insert on next line
    insert_col  = 0 ; // Set column to zero, specifying "before" the first character
#endif

#ifdef TBMD_DEBUG
    if (debug_print) {
        fprintf(debug_fp, "InsertAfter  - (%s)  line:%d  col:%d  text:\"%s\"\n", LineFile::GetFileName(pos), insert_line, insert_col, text) ;
    }
#endif

    // If either the line value is zero, exit early.
    if (!insert_line) return 0 ;

    // Ident all new lines of 'text' by indentation of line specified by linefile 'pos'
    char *text_to_insert = (ident) ? Indent(pos, text) : Strings::save(text) ;

    // Find the section where argument specific text is to be inserted

    FileModMngr *mod_mngr = GetFileModMngr(pos) ;
    FileMod *file_mod = (mod_mngr) ? mod_mngr->GetFileSection(pos) : 0 ; // Get/create the file section for this file
    FileMod *next_section = 0 ;
    unsigned cur_line, cur_col, next_line, next_col ;
    while(file_mod) {
        VERIFIC_ASSERT(mod_mngr) ;
        // Get current section's offset info
        cur_line     = file_mod->LineOffset() ;
        cur_col      = file_mod->ColOffset() ;
        // Get next section's offset info
        next_section = file_mod->Next() ;
        next_line    = (next_section) ? next_section->LineOffset() : 0 ;
        next_col     = (next_section) ? next_section->ColOffset() : 0 ;

        if ((cur_line == insert_line) && (cur_col == insert_col)) {
            // This section already exists.
            if (!file_mod->PrintAfter()) {
                Error(pos, "cannot insert text at this location, since enclosing file segment was already removed") ;
                Strings::free(text_to_insert) ;
                return 0 ;
            }
            return file_mod->InsertAfter(text_to_insert) ; // text is absorbed
        } else if (!next_section ||
           ((cur_line < insert_line)   && (insert_line < next_line)) ||
           ((cur_line == insert_line)  && (cur_col < insert_col) && !((insert_line == next_line) && (insert_col >= next_col))) ||
           ((insert_line == next_line) && (insert_col < next_col))) {
            // Check to see if file_mod allows something to be inserted here
            if (!file_mod->PrintAfter() || (next_section && !next_section->PrintBefore())) {
                Error(pos, "cannot insert text at this location, since enclosing file segment was already removed") ;
                Strings::free(text_to_insert) ;
                return 0 ;
            }
            // Since FileMod boundary for 'pos' doesn't exist, create it now
            FileMod *new_section = new FileMod(insert_line, insert_col) ;
            mod_mngr->InsertSection(new_section) ;
            new_section->SetNext(file_mod->Next()) ;
            file_mod->SetNext(new_section) ;

            // Insert text now
            return new_section->InsertAfter(text_to_insert) ; // text is absorbed
        }

        // Go to next (non-text) section
        file_mod = file_mod->Next() ;
    }

    Strings::free(text_to_insert) ;
    return 0 ;
}

/* -------------------------------------------------------------------------- */

unsigned
TextBasedDesignMod::Remove(const linefile_type pos) const
{
    return Remove(pos, pos) ;
}

/* -------------------------------------------------------------------------- */

// VIPER #3932: Recoded TextBasedDesignMod::Remove to fix the issues with 'InsertAfter':
// With CVS version 1.22 'TextBasedDesignMod::Remove' is re-implemented for VIPER #3932.
unsigned
TextBasedDesignMod::Remove(const linefile_type start, const linefile_type end) const
{
    if (!start || !end) return 0 ; // Failed

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    unsigned sl = start->GetLeftLine() ;
    unsigned sc = start->GetLeftCol() ;
    unsigned el = end->GetRightLine() ;
    unsigned ec = end->GetRightCol() ;
#else
    // Remove (line:start, column:1) -> (line:end, column:0):
    unsigned sl = LineFile::GetLineNo(start) ;
    unsigned sc = 1 ;
    unsigned el = LineFile::GetLineNo(end) + 1 ;
    unsigned ec = 0 ;
#endif

    // Sanity check:
    if ((sl > el) || ((sl == el) && (sc >= ec))) return 0 ;

#ifdef TBMD_DEBUG
    if (debug_print) {
        debug_print = 0 ;
        char *removed_text = GetText(start, end, 0 /* don't include modifications */) ;
        debug_print = 1 ;
        fprintf(debug_fp, "Remove       - (%s)  line:[%d,%d]  col:[%d,%d)  removed_text:\"%s\"\n", LineFile::GetFileName(start), sl, el, sc, ec, (removed_text) ? removed_text : "") ;
        Strings::free(removed_text) ;
    }
#endif

    // Find the node containing left/starting line-column of 'start' specific location
    // and delete upto 'end' specific location. First get the file section for this file:
    FileModMngr *mod_mngr = GetFileModMngr(start) ;
    VERIFIC_ASSERT(mod_mngr) ;
    FileMod *mod = mod_mngr->GetFileSection(start) ;
    if (!mod) return 0 ; // Can't remove

    unsigned cl = 0 ;
    unsigned cc = 0 ;
    // First, find the starting location of the section to be removed...
    FileMod *prev_mod = mod ;
    while (mod) {
        cl = mod->LineOffset() ;
        cc = mod->ColOffset() ;

        // Match with starting line-col:
        if ((sl > cl) || ((sl == cl) && (sc > cc))) {
            prev_mod = mod ;    // Need to save the previous modification
            mod = mod->Next() ; // Go to the next modification
            continue ;
        }

        break ; // Got the starting point
    }

    VERIFIC_ASSERT(prev_mod) ; // Must be valid
    // VIPER #3981: Modification that determines whether we are inside a deleted (not-printing) section:
    FileMod *printing_mod = (mod) ? mod : prev_mod ;
    unsigned printing = printing_mod->PrintAfter() ; // Keep the status too.

    // Here we have to insert a section after 'mod', if valid, or create a new modification.
    // Note that 'mod' can fully match with 'start'.
    if (mod && (sl == cl) && (sc == cc)) {
        // No need to create a new section, it matches with an existing one:
        mod->SetPrintAfter(0) ; // Do not print this modification itself
        // VIPER #4054 : If start of the portion to be removed is matched exactly
        // with an existing section, we should update the 'mod' to its next. So
        // that we can start searching the end point to be removed in the remaining
        // sections.
        prev_mod = mod ;
        mod = mod->Next() ;
    } else {
        // Here we need to create a new section:
        FileMod *break_at = new FileMod(sl, sc) ;
        mod_mngr->InsertSection(break_at) ;
        break_at->SetPrintAfter(0) ; // Do not print this new modification
        break_at->SetNext(mod) ;
        prev_mod->SetNext(break_at) ;
        prev_mod = break_at ;
    }

    // Next, find the ending location of the section to be removed...
    cl = 0 ;
    cc = 0 ;
    while (mod) {
        cl = mod->LineOffset() ;
        cc = mod->ColOffset() ;

        // Match with ending line-col:
        if ((el > cl) || ((el == cl) && (ec > cc))) {
            // Sanity check:
            if (!mod->PrintAfter()) {
                Warning(start, "text at this location was already removed") ;
            } else if (mod->InsertBeforeText() || mod->InsertAfterText()) {
                Warning(start, "TBDM inserted text is going to be removed") ;
            }
            // VIPER #3981: Update with this section and the status too, we are going to set the printing status to 0!
            printing_mod = mod ;
            printing = printing_mod->PrintAfter() ;
            mod->SetPrintAfter(0) ; // Do not print this intermediate modification
            prev_mod = mod ;        // Need to save the previous modification
            mod = mod->Next() ;     // Go to the next modification
            continue ;
        }

        break ;
    }

    VERIFIC_ASSERT(prev_mod) ; // Must hold this

    // Here we have to insert a section before 'mod' (after 'prev_mod').
    // Note that 'mod' can fully match with 'end', in that case we don't
    // have to create any new modification at this point.
    if ((el != cl) || (ec != cc)) {
        // Since it does not match with an existing section, create one:
        FileMod *break_at = new FileMod(el, ec) ;
        mod_mngr->InsertSection(break_at) ;
        // VIPER #3981 && #4035: Set the section to be deleted/not printing if the existing section
        // just before this (not the one just inserted) is overlapping and is also deleted:
        if ((printing_mod->LineOffset() < el) ||
           ((printing_mod->LineOffset() == el) && (printing_mod->ColOffset() < ec))) {
            // Since these sections overlaps, we should set the print status from the overlapping section:
            break_at->SetPrintAfter(printing) ;
        } else {
            // Otherwise we need to print this section (not inside a deleted section):
            break_at->SetPrintAfter(1) ;
        }
        break_at->SetNext(mod) ;
        prev_mod->SetNext(break_at) ;
    }
    // If the last node matches, we have nothing more left to do.

    return 1 ; // Remove always happens (may be with some warnings)
}

/* -------------------------------------------------------------------------- */

unsigned
TextBasedDesignMod::Replace(const linefile_type pos, const char *text, unsigned ident) const
{
    return Replace(pos, pos, text, ident) ;
}

/* -------------------------------------------------------------------------- */

unsigned
TextBasedDesignMod::Replace(const linefile_type start, const linefile_type end, const char *text, unsigned ident) const
{
    if (!start || !end) return 0 ;

#ifdef TBMD_DEBUG
    unsigned start_line, start_col, end_line, end_col ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    start_line  = start->GetLeftLine() ;
    start_col   = start->GetLeftCol() ;
    end_line    = end->GetRightLine() ;
    end_col     = end->GetRightCol() ;
#else
    // Remove (line start,column 1) -> (line end, column 0) :
    start_line  = LineFile::GetLineNo(start) ;
    start_col   = 1 ;
    end_line    = LineFile::GetLineNo(end) + 1;
    end_col     = 0 ;
#endif
    debug_print = 0 ; // Turn OFF subsequent debug printing
    char *old_text = GetText(start, end, 0/*don't include mods*/) ;
    fprintf(debug_fp, "Replace      - (%s)  line:[%d,%d]  col:[%d,%d)  old_text:\"%s\"  new_text:\"%s\"\n", LineFile::GetFileName(start), start_line, end_line, start_col, end_col, (old_text) ? old_text : "", text) ;
    Strings::free(old_text) ;
#endif

    // First remove specified span
    unsigned ret_val = Remove(start, end) ;

    // Now insert new code at start of removed span
    if (ret_val && text) {
        ret_val = InsertBefore(start, text, ident) ;
    }

#ifdef TBMD_DEBUG
    debug_print = 1 ; // Turn ON subsequent debug printing
#endif

    return ret_val ;
}

/* -------------------------------------------------------------------------- */

void
TextBasedDesignMod::ClearModifications(unsigned file_id)
{
    VERIFIC_ASSERT(_modifications) ;

    if (!file_id) return ;

    // Get the modifications corresponding to this design file from the Map
    FileModMngr *mod_mngr = (FileModMngr*)_modifications->GetValue((void*)(unsigned long)file_id) ;
    if (!mod_mngr) return ;

    (void) _modifications->Remove((void*)(unsigned long)file_id) ;

    // The FileModMngr destructor delete all of its FileMods.
    delete mod_mngr ;
}

// VIPER #3758: API to close all the open file handles.
void
TextBasedDesignMod::CloseFileHandles() const
{
    // Close all open file handles and reset the Map:
    InputFile *ifs ;
    MapIter mi ;
    FOREACH_MAP_ITEM(_ifstreams, mi, 0, &ifs) {
        ifs->close() ;
        delete ifs ;
    }

    if (_ifstreams) _ifstreams->Reset() ;
}

// VIPER #5914: Search files in this paths if not found from current working directory
unsigned
TextBasedDesignMod::AddSourceSearchPath(const char *path)
{
    if (!path) return 0 ;

    // Check if the path exists:
    if (!FileSystem::IsDir(path)) return 0 ; // Path does not exist

    if (!_source_search_paths) _source_search_paths = new Array() ;
    _source_search_paths->InsertLast(Strings::save(path)) ;

    return 1 ; // Added
}

/////////////////////////////////////////////////////////////////////////////
//////////                       Write Utility                    ///////////
/////////////////////////////////////////////////////////////////////////////

char *
TextBasedDesignMod::GetText(const linefile_type pos, unsigned including_mods) const
{
    return GetText(pos, pos, including_mods) ;
}

char *
TextBasedDesignMod::GetText(const linefile_type start, const linefile_type end, unsigned including_mods) const
{
    VERIFIC_ASSERT(_modifications && _ifstreams) ;

    // Determine file id 'start' linefile pertains to
    unsigned long file_id = LineFile::GetFileId(start) ;

    // Check to see if an ifs stream has already been opened for this file
    InputFile *ifs = (InputFile*)_ifstreams->GetValue((void*)file_id) ;
    if (!ifs) {
        // Need to create a new ifstream for this file
        const char *design_file = LineFile::GetFileName(start) ;
        ifs = new InputFile() ;
        // VIPER #5914: Call SearchOpenFile() instead of open():
        ifs->SearchOpenFile(design_file, ios::in | ios::binary, _source_search_paths) ;
        if (!ifs->is_open() || ifs->bad()) {
            // So we're assuming that this means "too many open files", which is OS specific.
            // Clear out _ifstreams Map appropriately and try opening the file again
            delete ifs ;
            CloseFileHandles() ;
            // Now try opening the file again.
            ifs = new InputFile() ;
            // VIPER #5914: Call SearchOpenFile() instead of open():
            ifs->SearchOpenFile(design_file, ios::in | ios::binary, _source_search_paths) ;
            if (!ifs->is_open() || ifs->bad()) {
                Error(0, "cannot open %s for reading", design_file) ;
                delete ifs ;
                return 0 ;
            }
        }
        // Insert ifstream into map container
        _ifstreams->Insert((void*)file_id, ifs) ;
    }

    unsigned start_line, start_col, end_line, end_col ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    start_line  = start->GetLeftLine() ;
    start_col   = start->GetLeftCol() ;
    end_line    = end->GetRightLine() ;
    end_col     = end->GetRightCol() ;
#else
    start_line  = LineFile::GetLineNo(start) ;
    start_col   = 1 ;
    end_line    = LineFile::GetLineNo(end) + 1 ;
    end_col     = 0 ;
#endif

#ifdef TBMD_DEBUG
    if (debug_print) {
        fprintf(debug_fp, "GetText      - (%s)  line:[%d,%d]  col:[%d,%d)\n", LineFile::GetFileName(start), start_line, end_line, start_col, end_col) ;
    }
#endif

    ostringstream os ; // Dynamically allocated string buffer

    FileModMngr *mod_mngr = (FileModMngr*)_modifications->GetValue((void*)file_id) ;
    FileMod *file_mod = (mod_mngr) ? mod_mngr->GetFileSection(start) : 0 ;
    if (including_mods && file_mod) {
        // Find starting FileMod section (if it exists)
        unsigned print = 1 ;
        while (file_mod && ((start_line > file_mod->LineOffset()) ||
              ((start_line == file_mod->LineOffset()) && (start_col > file_mod->ColOffset())))) {
            print = file_mod->PrintAfter() ;
            file_mod = file_mod->Next() ;
        }

        if (!file_mod) {
            if (print && !TextBasedDesignMod::WriteToStream(*ifs, os, start_line, start_col, end_line, end_col)) {
                return 0 ;
            }
            return Strings::save(os.str().c_str()) ;
        }

        if (file_mod->PrintAfter()) {
            // VIPER #5853: Only print the part of the string requested:
            unsigned el = end_line ;
            unsigned ec = end_col ;
            // Keep track whether we printed in full the required section:
            unsigned done = 1 ;
            if ((end_line > file_mod->LineOffset()) ||
                ((end_line == file_mod->LineOffset()) && (end_col > file_mod->ColOffset()))) {
                el = file_mod->LineOffset() ;
                ec = file_mod->ColOffset() ;
                done = 0 ; // Not yet done, need to print some more
            }
            if (!TextBasedDesignMod::WriteToStream(*ifs, os, start_line, start_col, el, ec)) {
                return 0 ;
            }
            if (done) return Strings::save(os.str().c_str()) ; // Done
        }

        // Write the modifications into the output file : source file is the ifs
        unsigned sl, sc, el, ec ;
        while (1) {
            VERIFIC_ASSERT(file_mod) ;

            // First print file modifications
            (void) file_mod->WriteToStream(os) ;

            sl = file_mod->LineOffset() ;
            sc = file_mod->ColOffset() ;
            el = (file_mod->Next()) ? file_mod->Next()->LineOffset() : 0 ;
            ec = (file_mod->Next()) ? file_mod->Next()->ColOffset() : 0 ;

            // Determine end range?
            if (!el || (end_line < el) || ((end_line == el) && (end_col < ec))) {
                // Print from this mod section to final end limit
                if (file_mod->PrintAfter()) {
                    if (!TextBasedDesignMod::WriteToStream(*ifs, os, sl, sc, end_line, end_col)) return 0 ;
                }
                break ; // We are done!
            } else {
                // Print from this mod section to next mod section
                if (file_mod->PrintAfter()) {
                    if (!TextBasedDesignMod::WriteToStream(*ifs, os, sl, sc, el, ec)) return 0 ;
                }
            }

            file_mod = file_mod->Next() ;
        }
    } else {
        // Write original file text (not including modifications)
        if (!TextBasedDesignMod::WriteToStream(*ifs, os, start_line, start_col, end_line, end_col)) {
            return 0 ;
        }
    }

    return Strings::save(os.str().c_str()) ;
}

/* -------------------------------------------------------------------------- */

// VIPER #7453: Returns the inserted text (before or after) of the given location.
// The text is newly allocated, so caller should free it.
char *
TextBasedDesignMod::GetInsertedText(const linefile_type pos, unsigned before_the_pos) const
{
    if (!pos) return 0 ;

    FileModMngr *mod_mngr = GetFileModMngr(pos, 0 /* do not create */) ;
    if (!mod_mngr || !mod_mngr->Size()) return 0 ;

    FileMod *file_mod = mod_mngr->GetFileSection(pos) ;
    if (!file_mod) return 0 ;

    unsigned line ;
    unsigned col ;
    do {
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
        line = (before_the_pos) ? pos->GetLeftLine() : pos->GetRightLine() ;
        col = (before_the_pos) ? pos->GetLeftCol() : pos->GetRightCol() ;
#else
        line = LineFile::GetLineNo(pos) ;
        col = 1 ;
#endif
        if ((file_mod->LineOffset() == line) && (file_mod->ColOffset() == col)) break ;
    } while ((file_mod = file_mod->Next()) && ((file_mod->LineOffset() < line) ||
             ((file_mod->LineOffset() == line) && (file_mod->ColOffset() < col)))) ;

    if (!file_mod) return 0 ;
    if ((file_mod->LineOffset() != line) || (file_mod->ColOffset() != col)) return 0 ;

    char *result = 0 ;
    unsigned i ;
    const char *text ;
    const Array *texts = (before_the_pos) ? file_mod->InsertBeforeText() : file_mod->InsertAfterText() ;
    FOREACH_ARRAY_ITEM(texts, i, text) {
        if (!text) continue ;
        if (!result) {
            result = Strings::save(text) ;
        } else {
            char *tmp = result ;
            result = Strings::save(result, text) ;
            Strings::free(tmp) ;
        }
    }

    return result ;
}

/* -------------------------------------------------------------------------- */

unsigned
TextBasedDesignMod::WriteDesign(const char *suffix, const char *output_directory) const
{
    unsigned design_written = 0 ;
    MapIter mi ;
    unsigned long file_id ;
    const char *design_file ;
    char *output_file_name ;
    // Write all the modified design files
    FOREACH_MAP_ITEM(_modifications, mi, &file_id, 0) {
        design_file = LineFile::GetFileNameFromId((unsigned)file_id) ;
        if (!design_file) continue ;
        if (output_directory || suffix) {
            // If output_directory is specified, construct new output file path (VIPER #2805)
            output_file_name = 0 ;
            if (output_directory) {
                char *cwd  = FileSystem::Convert2AbsolutePath(".", 1);
                char *file = FileSystem::Convert2AbsolutePath(design_file, 1) ;
                if (!cwd || !file) {
                    Strings::free(cwd) ;
                    Strings::free(file) ;
                    return 0 ; // Something wrong happened
                }
#ifdef WINDOWS
                // Windows is case-insensitive, so lower case paths for strstr comparison only
                (void) Strings::strtolower(cwd) ;  // lower-casing string in place
                (void) Strings::strtolower(file) ; // lower-casing string in place
#endif
                // Determine if 'design_file' resides underneath CWD directory tree.
                if (file == strstr(file, cwd)) {
                    // File resides under CWD
                    output_file_name = Strings::save(output_directory, "/", file + Strings::len(cwd) + 1) ;
                } else {
                    // This file resides external to CWD directory tree.  Because of this, we need
                    // to create a new path.  Lop off network mount specifier and add "EXTERNAL".
#ifdef WINDOWS
                    if (file[0] == '\\' && file[1] == '\\') {
                        output_file_name = Strings::save(output_directory, "/EXTERNAL/", file + 2) ;
                    } else if (file[1] == ':') {
                        output_file_name = Strings::save(output_directory, "/EXTERNAL/", file + 3) ;
                    } else {
                        VERIFIC_ASSERT(0) ;
                    }
#else
                    output_file_name = Strings::save(output_directory, "/EXTERNAL/", file + 1) ;
#endif
                }

                Strings::free(cwd) ;
                Strings::free(file) ;

                // Now create the directory path
                char *dir = FileSystem::DirectoryPath(output_file_name) ;
                if (!FileSystem::Mkdir(dir)) {
                    Error(0, "cannot create '%s' due to mkdir failure", dir) ;
                    Strings::free(output_file_name);
                    Strings::free(dir);
                    return 0 ;
                }
                Strings::free(dir);
            }
            // If suffix is specified, append it
            if (suffix) {
                char *tmp = output_file_name ;
                output_file_name = Strings::save(((tmp) ? tmp : design_file), suffix) ;
                Strings::free(tmp) ;
            }
            // Write out file.
            Comment(0, "Writing instrumented design %s to %s", design_file, output_file_name) ;
            if (!WriteFile(design_file, output_file_name)) {
                Strings::free(output_file_name); //JJ: clean up before return
                return 0 ;
            }
            Strings::free(output_file_name);
        } else {
            // Overwrite original file with the modifications applied
            Comment(0, "Overwriting instrumented design file %s", design_file) ;
            if (!WriteFile(design_file, 0)) return 0 ;
        }
        design_written = 1 ;
    }

    if (!design_written) Info(0, "no file was modified, design not written out") ;

    return design_written ;
}

/* -------------------------------------------------------------------------- */

unsigned
TextBasedDesignMod::WriteFile(const char *design_file, const char *out_file) const
{
    VERIFIC_ASSERT(_modifications && _ifstreams) ;

    if (!design_file) return 0 ; // Nothing to print from/to!

    // Check if the design and the output files are same! We should not open the same file
    // both for reading and writing at the same time! So it is necessary to check the files.
    // Actually, if they are same we will set the out_file to 0. So that we can overwrite it
    // later and also check whether we actually should overwrite it at all. Because, if the
    // file does not belongs to the secure directory we will not overwrite it!
    if (out_file) {
        // Set the 'out_file' to 0, if both the input and the output files are same!
        if (Strings::compare(design_file, out_file)) out_file = 0 ;
#ifdef WINDOWS
        // For Windows, file names are case insensitive!
        else if (Strings::compare_nocase(design_file, out_file)) out_file = 0 ;
#endif
    }

    // Get the modifications corresponding to this design file from the Map
    unsigned long file_id = LineFile::GetFileId(design_file) ;
    FileModMngr *mod_mngr = (FileModMngr *)_modifications->GetValue((void*)file_id) ;
    FileMod *file_mod = (mod_mngr) ? (FileMod*)mod_mngr->GetFirst() : 0 ;
    if (!file_mod && !out_file) {
        // File is not modified and output file is not specified!
        Info(0, "file %s was not modified, left unchanged", design_file) ;
        return 0 ;
    }

    if (!out_file && !IsSecure(design_file)) {
        // We need to over-write the original file, but this file is not secure, so produce an warning message:
        Warning(0, "%s does not belong to the secure directory %s, cannot modify", design_file, _secure_dir) ;
        return 0 ;
    }

    // Check to see if an ifs stream has already been opened for this file
    InputFile *ifs = (InputFile*)_ifstreams->GetValue((void*)file_id) ;
    if (!ifs) {
        // Need to create a new ifstream for this file
        ifs = new InputFile() ;
        // VIPER #5914: Call SearchOpenFile() instead of open():
        ifs->SearchOpenFile(design_file, ios::in | ios::binary, _source_search_paths) ;
        if (!ifs->is_open() || ifs->bad()) {
            // So we're assuming that this means "too many open files", which is OS specific.
            // Clear out _ifstreams Map appropriately and try opening the file again
            delete ifs ;
            CloseFileHandles() ;
            // Now try opening the file again.
            ifs = new InputFile() ;
            // VIPER #5914: Call SearchOpenFile() instead of open():
            ifs->SearchOpenFile(design_file, ios::in | ios::binary, _source_search_paths) ;
            if (!ifs->is_open() || ifs->bad()) {
                Error(0, "cannot open %s for reading", design_file) ;
                delete ifs ;
                return 0 ;
            }
        }
        // Insert ifstream into map container
        _ifstreams->Insert((void*)file_id, ifs) ;
    }

    // Open the output file
    // Use 'ofstream *' instead of automatic ofstream object so that we can delete
    // and create a new object in case of failure to open files. In some compiler,
    // for example with g++ 3.2.3, after failure to open a file it cannot open a
    // valid file anymore unless we use clear(). But since clear() may also
    // misbehave in some other compiler we decided to use ofstream * as we do in
    // other places to be in the safe side.
    ofstream *ofs = 0 ;
    char *temp_file = 0 ;
    if (out_file) {
        // Open this out file
        ofs = new ofstream(out_file, ios::out) ;
    } else if (file_mod) {
        // Output file is not specified, so we have to overwrite the source, write into a temporary file
        temp_file = FileSystem::GetTemporaryFileName() ;
        ofs = new ofstream(temp_file, ios::out) ;
    } else {
        VERIFIC_ASSERT(0) ; // We already checked it above with "if (!file_mod && !out_file) { ... ; return 0 ; }"
    }

    // VIPER #3360: Check whether we have failed to open the output file. This failure might be
    // caused by too many open files for input in the '_ifstreams' Map. Check and close them:
    if (!ofs->rdbuf()->is_open()) {
        // Can't open the file, may be because of 'too many open files'.
        // Try to close all the open files that we have:
        // Remove the just opened input file, otherwise it will be closed along.
        _ifstreams->Remove((void *)file_id) ;
        CloseFileHandles() ;
        // Re-insert this file, we have kept it open:
        _ifstreams->Insert((void*)file_id, ifs) ;

        // Now try to open the file again:
        delete ofs ;
        if (out_file) {
            // Open this out file
            ofs = new ofstream(out_file, ios::out) ;
        } else {
            // Output file is not specified, so we have to overwrite the source, write into a temporary file
            Strings::free(temp_file) ;
            temp_file = FileSystem::GetTemporaryFileName() ;
            ofs = new ofstream(temp_file, ios::out) ;
        }
    }

    // Check whether output file is opened
    if (!ofs->rdbuf()->is_open()) {
        Error(0, "cannot open %s for writing", (temp_file) ? temp_file : out_file) ;
        Strings::free(temp_file) ;
        delete ofs ;
        return 0 ;
    }

    if (!file_mod) {
        // VIPER #6026: Copy the original source file to out_file as is:
#if 0
        static const unsigned CHUNK_SIZE = 4096 ;
        char chunk_buffer[CHUNK_SIZE] = "" ;
        while (!ifs->eof()) {
            // Read a string chunk from the file and write it out:
            (void) ifs->get(chunk_buffer, CHUNK_SIZE, EOF) ;
            (*ofs) << chunk_buffer ;
        }
#else
        // Specify 1 and start line/col and 0 as end line/col to write the whole file:
        (void) TextBasedDesignMod::WriteToStream(*ifs, *ofs, 1/* start line*/, 1/*start col*/, 0/*end line*/, 0/*end col*/) ;
#endif
        // Fall-through below to do the other works...
    }

    // Write the modifications into the output file : source file is the ifs
    unsigned start_line, start_col, end_line, end_col ;
    while (file_mod) {
        // Determine file section starting at this modification to the next one
        start_line = file_mod->LineOffset() ;
        start_col  = (file_mod->ColOffset()) ? file_mod->ColOffset() : 1 ;
        end_line   = (file_mod->Next()) ? file_mod->Next()->LineOffset() : 0 ;
        end_col    = (file_mod->Next()) ? file_mod->Next()->ColOffset() : 0 ;
        // First print file modifications
        (void) file_mod->WriteToStream(*ofs) ;
        // Now print original file section (if permitted)
        if (file_mod->PrintAfter()) {
            if (!TextBasedDesignMod::WriteToStream(*ifs, *ofs, start_line, start_col, end_line, end_col)) {
                break ;
            }
        }

        file_mod = file_mod->Next() ;
    }

    // Close output file
    ofs->close() ;
    delete ofs ;

    // If 'file_mod' is non-NULL, then WriteToFile failed.
    if (file_mod) return 0 ;

    // Check whether we have to rename the file
    if (temp_file) {
        // We have to move the temporary file to the source file.
        if (rename(temp_file, design_file)) {
            Error(0, "cannot overwrite %s; output file remains in %s", design_file, temp_file) ;
            Strings::free(temp_file) ;
            return 0 ;
        }
    }

    Strings::free(temp_file) ;

    return 1 ;
}

/* -------------------------------------------------------------------------- */

unsigned
TextBasedDesignMod::WriteToStream(InputFile &in_file, ostream &os, unsigned start_line, unsigned start_col, unsigned end_line, unsigned end_col)
{
    // Check if there is a range to print
    if ((start_line == end_line) && (start_col == end_col)) {
        return 1 ;
    }

    // Check whether we need to print till the end-of-file.
    unsigned upto_eof = (end_line) ? 0 : 1 ;

    // SANITY CHECK : Check if range makes sense!
    if (!upto_eof && ((start_line > end_line) || ((start_line == end_line) && (start_col > end_col)))) {
        return 0 ;
    }

    // Clear the eof bit if it is already reached previously, we have set the read pointer to the beginning!
    if (in_file.eof()) in_file.Clear() ;

    // Check if we need to reset InputFile offset.
    if ((start_line < in_file._line_offset) || ((start_line == in_file._line_offset) && (start_col < in_file._column_offset))) {
        // Need to reset InputFile offsets to start walking from beginning
        in_file._byte_offset              = 0 ;
        in_file._line_offset              = 1 ;
        in_file._column_offset            = 1 ;
    }

    // Reset the read position back to the current byte_offset of the stream.
    (void) in_file.seekg((streamoff)in_file._byte_offset, ios::beg) ;

    // The size of the chunk we read from the source file
    static const unsigned CHUNK_SIZE = 4096 ;
    char chunk_buffer[CHUNK_SIZE] ;
    char *starting_position = 0, *ending_position ;
    char *current_line, *next_line ;

    // Now get into printing if there is something available in the file to print!
    while (!in_file.eof()) {
        // Read a string chunk from the file
        // VIPER #6116: Do not use istream::get() with 'EOF' as delimiting character
        // since that cannot handle files with ÿ (ASCII 255) character in it. This
        // is -1 in (singed) char domain which is equal to EOF (defined as -1 too):
        //(void) in_file.get(chunk_buffer, CHUNK_SIZE, EOF) ;
        (void) in_file.read(chunk_buffer, CHUNK_SIZE-1) ; // Use istream::read() instead
        // istream::read() is an unformatted input function and what is extracted
        // is not stored as a c-string format, therefore no ending null-character
        // is appended at the end of the character sequence. So, append it now:
        chunk_buffer[in_file.gcount()] = '\0' ;

        // If starting position hasn't been determined yet, calculate it now.
        if (!starting_position) {
            // We don't have a starting position yet; find that here:
            next_line = chunk_buffer ;

            // -----------------------------------------------------------------------
            // 1st) Find starting line offset
            // -----------------------------------------------------------------------
            current_line = 0 ;
            while (next_line && (in_file._line_offset < start_line)) {
                // Get the position of the next newline character.
                current_line = next_line ;
                next_line = strchr(current_line, '\n') ;
                // If we have found a newline in the chunk:
                if (next_line) {
                    next_line++ ; // Go past the newline character
                    in_file._byte_offset += (unsigned long)(next_line - current_line) ;
                    in_file._line_offset++ ;     // Increase the current line number
                    in_file._column_offset = 1 ; // Reset column offset
                }
            }

            // If we are out of string, we need to loop again.
            if (!next_line) {
                in_file._byte_offset += Strings::len(current_line) ; // Update current file offset
                continue ; // Get the next chunk from the file
            }

            starting_position = next_line ; // Ok, we found starting line offset.
            VERIFIC_ASSERT(in_file._line_offset == start_line) ;

            // -----------------------------------------------------------------------
            // 2nd) Find starting column offset
            // -----------------------------------------------------------------------
            // Get the position of the next newline character to check whether this line has
            // that many columns!
            next_line = strchr(next_line, '\n') ;
            if (next_line) {
                // Check the position of this newline from the starting_position, whether
                // it is more than the starting column number:
                if (start_col <= (in_file._column_offset + (unsigned long)(next_line - starting_position))) {
                    // Ok, it has that many columns, update the starting position here:
                    starting_position      += start_col - in_file._column_offset ;
                    in_file._byte_offset   += start_col - in_file._column_offset ;
                    in_file._column_offset += start_col - in_file._column_offset ;
                } else {
                    // This is bad.  This means source file has changed in the meantime, OR linefile
                    // data has been manually changed.
                    Error(0, "starting line #%d does not contain %d columns", start_line, start_col) ;
                    return 0 ; // Return error
                    // Set the starting position to the next line, since we don't
                    // have the requested number of columns in this line: is it OK?
                    //starting_position = next_line + 1;
                    //in_file._line_offset++ ; // Also update the current line number
                }
            } else {
                // Ok, this line chunk has no more newline characters, so do direct offset check.
                unsigned this_chunk_len = (unsigned)Strings::len(starting_position) ;
                // Check whether we have to read more.
                if (start_col <= (in_file._column_offset + this_chunk_len)) {
                    // Ok, we can get the starting position from this chunk, update it:
                    starting_position      += start_col - in_file._column_offset ;
                    in_file._byte_offset   += start_col - in_file._column_offset ;
                    in_file._column_offset += start_col - in_file._column_offset ;
                } else {
                    // We need to read more to get to the starting position, reset it:
                    starting_position = 0 ;
                    in_file._column_offset += this_chunk_len ; // Also update the current column number
                    in_file._byte_offset   += this_chunk_len ; // Update current file offset
                    continue ;
                }
            }
        }

        VERIFIC_ASSERT(starting_position) ;

        // If we need to print upto eof, just print the whole chunk starting from
        // this postion pointed by starting_position and continue for the next chunk.
        if (upto_eof) {
            os << starting_position ;
            // Update offsets
            in_file._byte_offset += Strings::len(starting_position) ;
            in_file._line_offset = in_file._column_offset = 0 ;
            // Need to reset starting_position to chunk_buffer now.
            starting_position = chunk_buffer ;
            continue ; // Get the next chunk from the file
        }

        if (in_file._line_offset == (end_line + 1)) {
            VERIFIC_ASSERT(0) ; // FIXME : I don't know if this is needed anymore
            break ;
        }

        // -----------------------------------------------------------------------
        // 3rd) Find ending line offset (ending line offset is exclusive, meaning
        //      DON'T print ending line.)
        // -----------------------------------------------------------------------
        current_line = 0 ;
        next_line = starting_position ;
        while (next_line && (in_file._line_offset < end_line)) {
            // Get the position of the next newline character.
            current_line = next_line ;
            next_line = strchr(current_line, '\n') ;
            // If we have found a newline in the chunk:
            if (next_line) {
                next_line++ ; // Go past the newline character
                in_file._line_offset++ ;     // Increase the current line number
                in_file._column_offset = 1 ; // Reset column offset
                in_file._last_newline_byte_offset = in_file._byte_offset + (unsigned long)(next_line - starting_position) ;
            }
        }

        // If we are out of string, we need to loop again.
        if (!next_line) {
            // We are out of string chunk, print the whole chunk
            os << starting_position ;
            // Update offsets
            in_file._byte_offset += Strings::len(starting_position) ;
            in_file._column_offset += (unsigned)Strings::len(current_line) ;
            // Need to reset starting_position to chunk_buffer now.
            starting_position = chunk_buffer ;
            continue ; // Get the next chunk from the file
        }

        ending_position = next_line ; // Ok, we found ending line offset.
        //VERIFIC_ASSERT(in_file._line_offset == end_line) ;

        // -----------------------------------------------------------------------
        // 4th) Find ending column offset
        // -----------------------------------------------------------------------
        // Get the position of the next newline character to check whether this
        // line has that many columns!
        next_line = strchr(next_line, '\n') ;
        if (next_line) {
            if (end_col <= (in_file._column_offset + (unsigned long)(next_line - ending_position))) {
                // Ok, it has that many columns, print up to this
                if (end_col) ending_position += end_col  - in_file._column_offset ;
                *(ending_position) = '\0' ; // Replace it with '\0': will print till this
            } else {
                // This is bad.  This means source file has changed in the meantime, OR linefile
                // data has been manually changed.
                Error(0, "ending line #%d does not contain %d columns", end_line, end_col) ;
                return 0 ; // Return error
            }
            VERIFIC_ASSERT(Strings::len(starting_position) == (unsigned)(ending_position - starting_position)) ;
            // Print the chunk
            os << starting_position ;
            in_file._byte_offset += (unsigned long)(ending_position - starting_position) ; // Update current file offset
            if (end_col) in_file._column_offset = end_col ; // Update current file offset
            // Don't print further, we are done with this section!
            break ;
        } else {
            // Check whether we have to read more.
            unsigned this_chunk_len = (unsigned)Strings::len(ending_position) ;
            if (end_col < (in_file._column_offset + this_chunk_len)) {
                // Ok, we can print up to the ending position from this chunk.
                if (end_col) ending_position += end_col - in_file._column_offset ;
                *(ending_position) = '\0' ;  // Replace it with '\0': will print till this
                // Print the chunk
                os << starting_position ;
                in_file._byte_offset += (unsigned long)(ending_position - starting_position) ;
                if (end_col) in_file._column_offset = end_col ; // Update current file offset
                // Don't print further, we are done with this section!
                break ;
            } else {
                // We need to read more to get to the ending position, print the whole chunk now
                os << starting_position ;
                in_file._byte_offset += Strings::len(starting_position) ;
                in_file._column_offset += (unsigned)Strings::len(ending_position) ;
                // Need to reset starting_position to chunk_buffer now.
                starting_position = chunk_buffer ;
                continue ; // Get the next chunk from the file
            }
        }
    }

    return 1 ;
}

// VIPER #6035: Output formatted, varargs message functions with message ids:
/* static */ void
TextBasedDesignMod::Comment(linefile_type linefile, const char *format, ...)
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_COMMENT, GetMessageId(format), linefile, format, args) ;
    va_end(args) ;
}

/* static */ void
TextBasedDesignMod::Error(linefile_type linefile, const char *format, ...)
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_ERROR, GetMessageId(format), linefile, format, args) ;
    va_end(args) ;
}

/* static */ void
TextBasedDesignMod::Warning(linefile_type linefile, const char *format, ...)
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_WARNING, GetMessageId(format), linefile, format, args) ;
    va_end(args) ;
}

/* static */ void
TextBasedDesignMod::Info(linefile_type linefile, const char *format, ...)
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_INFO, GetMessageId(format), linefile, format, args) ;
    va_end(args) ;
}

/* static */ const char *
TextBasedDesignMod::GetMessageId(const char *msg)
{
    if (!msg) return 0 ;
    if (!_msgs) {
        _msgs = new Map(STRING_HASH, 19) ; // Table size : choose prime number for good hashing. Enough initial storage until message TBDM-1019.

        _msgs->Insert("cannot insert text at this location, since file segment was already removed", "TBDM-1001") ;
        _msgs->Insert("cannot insert text at this location, since enclosing file segment was already removed", "TBDM-1002") ;
        _msgs->Insert("text at this location was already removed", "TBDM-1003") ;
        _msgs->Insert("TBDM inserted text is going to be removed", "TBDM-1004") ;
        _msgs->Insert("cannot open %s for reading", "TBDM-1005") ;
        _msgs->Insert("cannot create %s due to mkdir failure", "TBDM-1006") ;
        _msgs->Insert("Writing instrumented design %s to %s", "TBDM-1007") ;
        _msgs->Insert("Overwriting instrumented design file %s", "TBDM-1008") ;
        _msgs->Insert("file %s was not modified, left unchanged", "TBDM-1009") ;
        _msgs->Insert("%s does not belong to the secure directory %s, cannot modify", "TBDM-1010") ;
        _msgs->Insert("cannot open %s for writing", "TBDM-1011") ;
        _msgs->Insert("cannot overwrite %s; output file remains in %s", "TBDM-1012") ;
        _msgs->Insert("starting line #%d does not contain %d columns", "TBDM-1013") ;
        _msgs->Insert("ending line #%d does not contain %d columns", "TBDM-1014") ;
        _msgs->Insert("no file was modified, design not written out", "TBDM-1015") ;
        _msgs->Insert("cannot create '%s' due to mkdir failure", "TBDM-1016") ;
    }

    return (const char *)_msgs->GetValue(msg) ;
}

/* static */ void
TextBasedDesignMod::ResetMessageMap()
{
    delete _msgs ;
    _msgs = 0 ;
}

////////////////////////////////////////////////////////////////////////////
////////////             FileModMngr Implementation             ////////////
////////////////////////////////////////////////////////////////////////////

FileModMngr::FileModMngr()
  : Array(313),
    _last_idx(0)
{
}

/* -------------------------------------------------------------------------- */

FileModMngr::~FileModMngr()
{
    // Delete all the modifications individually:
    FileMod *section ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(this, i, section) delete section ;
}

/* -------------------------------------------------------------------------- */

FileMod *
FileModMngr::GetFileSection(const linefile_type lf)
{
    if (!lf) return 0 ;

    // Check to make sure this Array is has at least one entry.
    FileMod *section ;
    if (!Size()) {
        section = new FileMod(1 /*line*/, 0 /*column*/) ;
        InsertFirst(section) ;
        return section ;
    }

    // Now do binary search on line_offset
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    unsigned line_offset = lf->GetLeftLine() ;
#else
    unsigned line_offset = LineFile::GetLineNo(lf) ;
#endif

    register unsigned low = 0 ;
    register unsigned high = Size() - 1 ;
    register unsigned idx = high ;

    // First check for the last item, most probably it will match:
    section = (FileMod*)At(idx) ; // Note: idx is set to high above
    if (section->_line_off == line_offset) {
        _last_idx = idx ; // Record _last_idx for InsertSection()
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
        if ((lf->GetLeftCol() < section->_col_off)) {
            // At this point, start walking back to find a section who's linefile
            // is less than current section.
            while (idx) {
                section = (FileMod*)At(--idx) ;
                if ((lf->GetLeftLine() > section->_line_off) ||
                   ((lf->GetLeftLine() == section->_line_off) && (lf->GetLeftCol() >= section->_col_off))) {
                    break ;
                }
            } ;
            _last_idx = idx ; // Record _last_idx for InsertSection()
        }
#endif
        return section ;
    }

    while (low <= high) {
        idx = low + ((high - low) >> 1) ;
        section = (FileMod*)At(idx) ;
        if (section->_line_off > line_offset) {
            high = idx - 1 ;
        } else if (section->_line_off < line_offset) {
            low = idx + 1 ;
        } else {
            _last_idx = idx ; // Record _last_idx for InsertSection()
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
            if ((lf->GetLeftCol() < section->_col_off)) {
                // At this point, start walking back to find a section who's linefile
                // is less than current section.
                while (idx) {
                    section = (FileMod*)At(--idx) ;
                    if ((lf->GetLeftLine() > section->_line_off) ||
                       ((lf->GetLeftLine() == section->_line_off) && (lf->GetLeftCol() >= section->_col_off))) {
                        break ;
                    }
                } ;
                _last_idx = idx ; // Record _last_idx for InsertSection()
            }
#endif
            return section ;
        }
    }

    _last_idx = high ; // Record _last_idx for InsertSection()
    return (FileMod*)At(high) ;
}

/* -------------------------------------------------------------------------- */

void
FileModMngr::InsertSection(const FileMod *section)
{
    if (!section) return ;

    // Start walking from _last_idx, and find appropriate place to insert section (if applicable)
    FileMod *cur_section ;
    unsigned i ;
    for (i = _last_idx; i < Size()-1 ; i++) {
        cur_section = (FileMod*)At(i) ;
        if (section->_line_off == cur_section->_line_off) {
            if (section->_col_off < cur_section->_col_off) {
                // Since line offsets are the same, but column offset of 'inserting' section
                // is less than what's already there, overwrite stored section.
                Insert(i, section) ;
            }
            // No need to insert 'section' since what's already stored has a column offset that's
            // less than 'section'
            return ;
        } else if (section->_line_off < cur_section->_line_off) {
            // Insert this section before current index
            InsertBefore(i, section) ;
            return ;
        }
    }

    // Need to check corner-case when i == _last_idx
    if (i == _last_idx) {
        cur_section = (FileMod*)At(i) ;
        if (section->_line_off == cur_section->_line_off) {
            if (section->_col_off < cur_section->_col_off) {
                // Since line offsets are the same, but column offset of 'inserting' section
                // is less than what's already there, overwrite stored section.
                Insert(i, section) ;
            }
            return ;
        }
    }

    // Insert this section after this index.
    InsertAfter(i, section) ;
}

////////////////////////////////////////////////////////////////////////////
////////////          FileMod Implementation           ////////////
////////////////////////////////////////////////////////////////////////////

FileMod::FileMod(unsigned line, unsigned column)
  : _insert_before(0),
    _insert_after(0),
    _print_before(1),
    _print_after(1),
    _line_off(line),
    _col_off(column),
    _next(0)
{
}

/* -------------------------------------------------------------------------- */

FileMod::~FileMod()
{
    char *text ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_insert_before, i, text) Strings::free(text) ;
    FOREACH_ARRAY_ITEM(_insert_after, i, text) Strings::free(text) ;

    delete _insert_before ;
    delete _insert_after ;

    // Do not delete it, will be deleted from the container:
    _next = 0 ;
}

/* -------------------------------------------------------------------------- */

unsigned
FileMod::InsertBefore(const char *text)
{
    if (!text) return 0 ;

    if (!_insert_before) _insert_before = new Array() ;
    _insert_before->InsertLast(text) ; // String is absorbed

    return 1 ;
}

/* -------------------------------------------------------------------------- */

unsigned
FileMod::InsertAfter(const char *text)
{
    if (!text) return 0 ;

    if (!_insert_after) _insert_after = new Array() ;
    _insert_after->InsertFirst(text) ; // String is absorbed

    return 1 ;
}

/* -------------------------------------------------------------------------- */

unsigned
FileMod::WriteToStream(ostream &os) const
{
    unsigned i ;
    char *text ;

    // First output any "insert_before" text strings
    if (PrintBefore()) {
        FOREACH_ARRAY_ITEM(_insert_before, i, text) {
            os << text ;
        }
    }

    // Now output any "insert_after" text strings
    if (PrintAfter()) {
        FOREACH_ARRAY_ITEM(_insert_after, i, text) {
            os << text ;
        }
    }

    // Return now if we're not supposed to print anything else
    return 1 ;
}

/* -------------------------------------------------------------------------- */
